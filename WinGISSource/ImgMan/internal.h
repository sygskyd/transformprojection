///////////////////////////////////////////////////////////////////////////
//
// internal.h
//
// Contains all declarations required for development of an import or
// an export library (DIL or DEL). Must be included _before_ any other
// header files, as it also sets up special defines for and then imports
// the Microsoft Windows headers.
//
// Copyright (C) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
///////////////////////////////////////////////////////////////////////////
// $History: internal.h $
// 
// *****************  Version 9  *****************
// User: Sygsky       Date: 18-APR-2001 Time: 16:03 of Moscow time (Russia)
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Added new function "DLL_scale_img" to be exported from DIL/DEL 
// *****************  Version 9  *****************
// User: Sygsky       Date: 02-OCT-2000 Time: 14:58
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Added new name SET_ERRORIDPTR to be exported from DIL/DEL 
//
// *****************  Version 9  *****************
// User: Ericj        Date: 8/21/99    Time: 4:40a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// C:\imageman\imgman.cpp
// 
// *****************  Version 8  *****************
// User: Johnd        Date: 7/15/98    Time: 8:48a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Made ImgFromPDIB an externally visible function
// 
// *****************  Version 7  *****************
// User: Ericw        Date: 4/07/98    Time: 4:28p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Changed plug-in host name to imhost32.dll
// 
// *****************  Version 6  *****************
// User: Ericw        Date: 4/02/98    Time: 4:41p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// SUPPORT FOR PLUG-IN HOSTING !!!!!
// 
// *****************  Version 5  *****************
// User: Johnd        Date: 3/20/98    Time: 9:05a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Updated  internal error handling. Added error string info to TLS data
// instead of the global buffers where it had been stored. Also added
// SetError() func to set the internal error state.
// 
// *****************  Version 4  *****************
// User: Ericw        Date: 2/03/98    Time: 12:30p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Needed to have the save dialog structure since it's used for 2
// different modules - imgman.cpp and cdialog.cpp.
// 
// *****************  Version 3  *****************
// User: Ericw        Date: 1/19/98    Time: 11:36a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Moved ERRBUFSIZE in here to help with common dialog support.
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 7/16/97    Time: 3:13p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Added CLAMPED #define for the contrast feature.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:04p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 10  *****************
// User: Johnd        Date: 9/18/96    Time: 5:52p
// Updated in $/ImageMan 16-32/DLL
// Updated MAX_PATH define
// 
// *****************  Version 9  *****************
// User: Johnd        Date: 4/11/96    Time: 3:27p
// Updated in $/ImageMan 16-32/DLL
// Added OutputDebugString defines
// 
// *****************  Version 8  *****************
// User: Timk         Date: 2/01/96    Time: 10:47a
// Updated in $/ImageMan 16-32/DILS
// Changes to support debug dialog.
// 
// *****************  Version 7  *****************
// User: Johnd        Date: 9/25/95    Time: 3:03p
// Updated in $/ImageMan 16-32/DLL
// Chgd SETSTATUS macro to always set status
// 
// *****************  Version 6  *****************
// User: Johnd        Date: 9/21/95    Time: 10:50a
// Updated in $/ImageMan 16-32/DLL
// Changed STATUS structure long members to DWORD to avoid overflows on
// large images.
// 
// *****************  Version 5  *****************
// User: Timk         Date: 7/25/95    Time: 11:02p
// Updated in $/ImageMan 16-32/DILS
// Added writing using IO function blocks.
// 
// *****************  Version 4  *****************
// User: Timk         Date: 6/30/95    Time: 12:08p
// Updated in $/ImageMan 16-32/DLL
// Part 1 of massive changes for read/write using function blocks.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/18/95    Time: 3:59p
// Created in $/ImageMan 16-32/DLL
// ImageMan 4.0 Beta 1

// The Win32 version may use some Microsoft Win32 SDK includes even when using
// a non-MS compiler... we need to fake out those header files.
// We also need to ensure that the default 'char' type is unsigned.

#ifdef _MSC_VER
#ifndef _CHAR_UNSIGNED
#error Default char type must be unsigned. Use "/J".
#endif
#else
#define _MSC_VER 800 // fake out SDK headers
#endif // #ifdef _MSC_VER

// Let Windows' includes know we're making a Windows DLL
#ifndef _WINDOWS
#define _WINDOWS
#endif
#ifndef _WINDLL
#define _WINDLL
#endif
#define STRICT
//#define WIN32_LEAN_AND_MEAN
#define NOSERVICE
#include <windows.h>
#include <windowsx.h>
#include <commdlg.h>

#define ERRBUFSIZE    200
#include "TLSbyJHC.h" // Sygsky

#ifdef _WIN32

#define LOADDS
#define HUGE
typedef LPSTR HPSTR;                              
typedef unsigned char *HPUSTR;
typedef long *HPLONG;
#define MemFill(D,C,V) memset((D),(V),(C))
#include <shellapi.h>

#else // #ifdef WIN32

#define MemFill(D,C,V) _fmemset((D),(V),(C))
#define LOADDS _loadds

// supplied in winnt.h for Win32 development
#define UNREFERENCED_PARAMETER(P) (P)

// supplied in windef.h for Win32 development
#ifdef _WIN32
#define MAX_PATH 512
#else
#define MAX_PATH 128
#endif
#define HUGE _huge
typedef unsigned char huge *HPSTR;
typedef unsigned char huge *HPUSTR;
typedef unsigned char UCHAR;
typedef unsigned short USHORT;
typedef short SHORT;
typedef unsigned long ULONG;
typedef long huge *HPLONG;


#endif // #ifdef WIN32 #else

#define DLLHND  (GHND|GMEM_DDESHARE)    // DLL memory attributes

typedef int NEAR *NPINT;

#ifndef DEBUG
#define DEBUG 0
#endif

// Bring in "end-user" declarations.
#include "imgman.h"

#ifdef __cplusplus

#include "dib.h"

extern      "C" {

#else

typedef LPSTR PDIB;

#endif


	typedef struct tagSTATUS {
		STATUSPROC  lpfnStat;
		DWORD        lInterval;    // How often to call statusproc
		DWORD        lTotal;               // # of bytes total
		DWORD        lDone;                // # of bytes completed
		DWORD        lAcc;                         // # of bytes done since last call to lpfnStat
		DWORD       dwUserInfo;   // info to be passed on to status proc
		HANDLE      hImage;               // ImageMan image handle
	}           STATUS, FAR * LPSTATUS;


#define GET_EXT       MAKEINTRESOURCE(1)
#define SET_EXT       MAKEINTRESOURCE(2)
#define INIT_IMAGE    MAKEINTRESOURCE(3)
#define CLOSE_IMAGE   MAKEINTRESOURCE(4)
#define OPEN          MAKEINTRESOURCE(5)
#define CLOSE_KEEP    MAKEINTRESOURCE(6)
#define READ_ROWS     MAKEINTRESOURCE(7)
#define SET_ROW       MAKEINTRESOURCE(8)
#define BUILDDIB      MAKEINTRESOURCE(9)
#define PRINT_SELF    MAKEINTRESOURCE(10)
#define PRINT         MAKEINTRESOURCE(11)
#define RENDER_SELF   MAKEINTRESOURCE(12)
#define RENDER        MAKEINTRESOURCE(13)
#define ERROR_STRING  MAKEINTRESOURCE(14)
#define GET_VER       MAKEINTRESOURCE(15)
#define IMG_NAME      MAKEINTRESOURCE(16)
#define LOAD_WMF      MAKEINTRESOURCE(19)
#define SET_PAGE      MAKEINTRESOURCE(20)
#define GET_PAGE      MAKEINTRESOURCE(21)
#define VERIFY_IMG    MAKEINTRESOURCE(22)
//++Sygsky: to use internally supported images (as ECW for example)
#define SCALE_SELF    MAKEINTRESOURCE(23)	

typedef struct tagINTERNALINFO {
		char        sig[4];		// Signature = 'IM20'
		LPBITMAPINFO lpbi;		// bitmapinfo struct + palette
		UINT        bFlags;		//
		LONG        lOrgWid;		// width of original image
		LONG        lOrgHi;		// height of original image
		HANDLE      hFile;		// handle to image returned from DLL
		int         nExt;			// EXTList array entry for this extension
		HGLOBAL     hDIB;			// handle to DIB (if loaded)
		HBITMAP     hBitmap;		// handle to bitmap (if loaded)
		HMETAFILE   hWMF;			// handle to metafile (if vector)
		RECT        wmfBBOX;		// bounding box of WMF
		DWORD       dwROP;		// ROP to use when blitting the image
		LPVOID      lpHandle;	// handle to file (if used)
		LPIMG_IO	lpIO;			//io handlers for this image
		STATUSPROC  lpfnStat;	// pointer to status proc
		DWORD       dwUserInfo;	// whatever the user wants to see in his status proc
		LONG        lInterval;	// call status proc after processing lInterval bytes
		int         nPages;		// pages in this file
		int         nCurPage;	// current page in this file
		LPSTR		lpAlias;		// name of file, or alias of files not on disk
		LPVOID		lpOpenInfo;	// our copy of info needed to open this image

		PDIB 		pDIB;
	}           INTERNALINFO, FAR * LPINTERNALINFO;


#ifdef _WIN32

// stuff for locating DILs and DELs in directory structure
typedef int (*LPFNDODLLDIR) (LPCSTR, LPSTR);
extern BOOL findDLL(LPFNDODLLDIR, LPSTR FAR *);

// Use standard DLL loader for NT version... removes need for 'windos' module.
#define LoadLibraryPath LoadLibrary

// Information specific to each thread (task) of execution.
// This includes information for both import and export routines.
typedef struct tagThreadInfo {
	DWORD       dwDefaultUserInfo;  // default user info
	STATUSPROC  lpfnDefaultStat;    // default status proc
	long        lDefaultInterval;   // default interval between notifications
	int         status;               // import status
	LPSTR		lpError;			// * to Error info 
#ifdef _WIN32
	HINSTANCE   hPlugInHost ;
	BOOL			  bPlugInAvailable ;
#endif
} 	THREADINFO, *LPTHREADINFO;

#define GETSTATUS       ImgGetStatus()
#define SETSTATUS(S)    ImgSetStatus(S)
#define VSETSTATUS(P,S) if (P) (P)->status = (S)

extern void IMAPI ImgSetStatus(int);
extern void IMAPI ImgXSetStatus(int);
extern LPTHREADINFO GetThreadInfo(void);

#else // WIN32

typedef struct tagThreadInfo {  // structure which holds task-specific globals
		HTASK       hTask;                // task this junk belongs to
		DWORD       dwDefaultUserInfo;  // default user info
		STATUSPROC  lpfnDefaultStat;    // default status proc
		long        lDefaultInterval;   // default interval between notifications
		LPSTR		lpError;			// * to Error info 
} 	THREADINFO, FAR * LPTHREADINFO;

#define GETSTATUS       (img_status)
#define SETSTATUS(S)    (img_status = (S))
#define VSETSTATUS(P,S) img_status = (S)
#define GETXSTATUS      (img_status)
#define SETXSTATUS(S)   img_status = (S)

#define DeleteFile(S) (!_funlink((LPSTR)(S)))

#endif // WIN32

// Following is used for the ImgXFileDialog common dialog
typedef struct tagSAVEDIALOG {
	HANDLE hOptBlk ;
	long   lOptions[10] ;
	int		 iSaveWindowHeight ;
	int		 iColors ;
	char   szFileType[5] ;
} SAVEDIALOG, FAR * LPSAVEDIALOG ;

//
// typedefs useful for accessing DIL functions
//
typedef int (IMAPI * DLLclose_image) (HANDLE);
typedef int (IMAPI * DLLinit_image) (LPCSTR, LPHANDLE, LPVOID, long, long, LPIMG_IO);
typedef int (IMAPI * DLLopen) (HANDLE, LPVOID);
typedef int (IMAPI * DLLclose_keep) (HANDLE);
typedef int (IMAPI * DLLread_rows) (HANDLE, LPSTR, UINT, UINT, UINT, LPSTATUS);
typedef int (IMAPI * DLLset_row) (HANDLE, UINT);
typedef int (IMAPI * DLLbuildDIBhead) (HANDLE, LPINTERNALINFO);
typedef int (IMAPI * DLLprint) (HANDLE, HDC, LPRECT, LPRECT);
typedef int (IMAPI * DLLrender) (HANDLE, HDC, LPRECT, LPRECT);
typedef int (IMAPI * DLLerror_string) (LPSTR, int);
typedef int (IMAPI * DLLimg_name) (HANDLE, HANDLE FAR *);
typedef int (IMAPI * DLLescape) (HANDLE, int, int, LPVOID, LPVOID);
typedef int (IMAPI * DLLload_wmf) (HANDLE, HMETAFILE FAR *, LPRECT, LPIMG_IO);
typedef int (IMAPI * DLLset_page) (HANDLE, int);
typedef int (IMAPI * DLLget_page) (HANDLE, LPINT);
typedef     LPSTR(IMAPI * DLLget_ext) (void);
typedef     LPSTR(IMAPI * DLLget_ver) (void);
typedef     BOOL(IMAPI * DLLverify_img) (LPSTR, LPVOID, LPIMG_IO);
typedef int	(IMAPI * DLLscale_img) (HANDLE, UINT, UINT, LPRECT); //++Sygsky: for internal scaling

typedef struct _tagDILFUNCS {
	DLLinit_image init_image;
	DLLclose_image close_image;
	DLLopen     open;
	DLLclose_keep close_keep;
	DLLread_rows read_rows;
	DLLset_row  set_row;
	DLLbuildDIBhead buildDIBHead;
	DLLprint    print;
	DLLrender   render;
	DLLerror_string error_string;
	DLLimg_name img_name;
	DLLload_wmf load_wmf;
	DLLset_page set_page;
	DLLget_page get_page;
	DLLverify_img verify_img;
	DLLscale_img scale_img;	//++Sygsky: for ECW internal scaling
}           DILFUNCS;

LPSTR IMAPI IMDLget_ext(void);
int IMAPI   ImgXInit(void);
int IMAPI   IMDLset_ext(HGLOBAL, LPSTR);
int IMAPI   IMDLinit_image(LPCSTR, LPHANDLE, LPVOID, long, long, LPIMG_IO);
int IMAPI   IMDLclose_image(HGLOBAL);
int IMAPI   IMDLimg_name(HGLOBAL, LPHANDLE);
int IMAPI   IMDLopen(HGLOBAL, LPVOID);
int IMAPI   IMDLclose_keep(HGLOBAL);
int IMAPI   IMDLread_rows(HGLOBAL, LPSTR, UINT, UINT, UINT, LPSTATUS);
int IMAPI   IMDLset_row(HGLOBAL, UINT);
int IMAPI   IMDLload_wmf(HANDLE, HMETAFILE FAR *, LPRECT, LPIMG_IO);
int IMAPI   IMDLbuildDIBhead(HGLOBAL, LPINTERNALINFO);
int IMAPI   IMDLprint(HGLOBAL, HDC, LPRECT, LPRECT);
int IMAPI   IMDLrender(HGLOBAL, HDC, LPRECT, LPRECT);
int IMAPI   IMDLerror_string(LPSTR, int);
int IMAPI   IMDLset_page(HGLOBAL, int);
int IMAPI   IMDLget_page(HGLOBAL, LPINT);
BOOL IMAPI  IMDLverify_img(LPSTR, LPVOID, LPIMG_IO);
BOOL IMAPI  IMDLset_erroridptr(LPDWORD); // Sygsky
LPSTR IMAPI IMDLget_ver(void);

HANDLE IMAPI ImgLoadLibraryExt(LPSTR lpExt);
HANDLE IMAPI ImgFromPDIB( PDIB pNewDIB );

// misc functions
extern void PASCAL SetError(LPCSTR, int, int, HANDLE, HINSTANCE);

int         InternalLoad(LPINTERNALINFO, LPRECT, PDIB FAR *, HMETAFILE FAR *);

LPSTR       MemCopy(void HUGE *, void HUGE *, DWORD);
LPTHREADINFO GetThreadInfo(void);

// ////////////////////////////////////////////////////////////////////////////
//
// ImageMan/X stuff
//
// ///////////////////////////////////////////////////////////////////////////

#define XGET_EXT     MAKEINTRESOURCE(1)
#define XBEGIN_WRITE MAKEINTRESOURCE(2)
#define XWRITE_BLOCK MAKEINTRESOURCE(3)
#define XEND_WRITE   MAKEINTRESOURCE(4)
#define XGET_VER     MAKEINTRESOURCE(5)

// setup an error-handling macro
#define XSETERROR(a,b,c,d) if (XSetError) (*XSetError)(a,b,c,d)

//
// typedefs useful for accessing DEL functions
//
typedef int (IMAPI * DLLxerror_string) (LPSTR, int);
typedef int (IMAPI * DLLximg_name) (HANDLE, HANDLE FAR *);
typedef     LPSTR(IMAPI * DLLxget_ver) (void);
typedef     LPSTR(IMAPI * DLLxget_ext) (LPXINITSTRUCT);
typedef     HANDLE(IMAPI * DLLxbegin_write) (LPVOID, LPIMG_IO, LPCSTR, LPBITMAPINFO, HANDLE, LPXINFO);
typedef int (IMAPI * DLLxwrite_block) (HANDLE, int, int, LPSTR, LPXINFO);
typedef int (IMAPI * DLLxend_write) (HANDLE, LPXINFO);
void __cdecl FAR Debug(UINT, LPSTR, ...);
// definitions for DEBUGOUT call
// 1 - ImageMan API calls
// 2 - ImageMan Internals
// 4 - DIL API calls
// 8 - DIL internals
// 16 - Image details
//
#define IMGMANAPI       0x01
#define IMGMANINT       0x02
#define DILAPI          0x04
#define DILINT          0x08
#define IMGDETAIL       0x10
#define DILSETLINE      0x20
#define DILERR          0x40
#define IMGMANERR       0x80

typedef void (__cdecl FAR * DEBUGPROC) (UINT, LPSTR, ...);
#define DEBUGOUT Debug
#if DEBUG
#define DEBUGGER _asm int 3
#else
#define DEBUGGER ((void)0)
#endif


#ifdef __cplusplus
}
#endif

#ifdef DEBUG
#include <stdarg.h>
#endif

#ifdef NDEBUG
#undef OutputDebugString
#define OutputDebugString(S) ((void)0)
#endif

// Image processing tools
#define CLAMPED(x) (x>255)?255:(x<0)?0:x

#define PLUGINHOSTSPEC "IMHOST32.DLL"

// WinGIS licensing string
#define WINGISLICENSEATOM "dewolla WCE"

#ifndef WIN32
extern int  img_status;
#endif


