/////////////////////////////////////////////////////////////////////////////
//
//    CPiPL.h
//
//    ImageMan Plug-In Host PiPL Class Header
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1998 Data Techniques, Inc.
//    All Rights Reserved
//	 
/////////////////////////////////////////////////////////////////////////////
//$Header: /ImageMan 16-32/DLL-API Level 2/CPiPL.h 2     4/24/98 10:58a Ericw $History: CPiPL.h $
// 

typedef struct tagPROPS {
	PIType vendorID       ;
	PIType propertyKey    ;
	int32  propertyID     ;
	int32  propertyLength ;
	char * propertyData   ;
	} PROPS, * LPPROPS ;

class CPiPL {

	private:
		int  iId    ;		// resource id in the filter itself;
		long lVers  ;
		long lErr   ;
		long lCount ;

		LPPROPS pProps ;

	public:
	
		CPiPL  ( HMODULE hPlugIn ) ;
		~CPiPL () ;

		LPSTR Category() { return GetValue( PICategoryProperty ); } ;
		LPSTR LongName() { return GetValue( PINameProperty     ); } ;
		LPSTR EntryProc() { return GetValue( PIWin32X86CodeProperty ); } ;

		long PiPLGetLastError () { return lErr ; } ;

	private:
		long LoadPiPL     ( HMODULE hPlugIn ) ;
		long BuildPiPL    ( LPBYTE lppipldata, BOOL bIsPiMI ) ;
		int  LocatePiPLID ( HMODULE hPlugIn, BOOL * bIsPiMI ) ;

		LPSTR GetValue ( PIType propKey ) ;
} ;
