/////////////////////////////////////////////////////////////////////////////
//
// ImgFX.cpp
//
// ImageMan Effects Module:
//   prototypes & user structures
//
// Copyright (c) 1996 Data Techniques, Inc.
// All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: imgfx.cpp $
// 
// *****************  Version 6  *****************
// User: Johnd        Date: 8/19/98    Time: 10:52a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Misc Chgs for 16 bit compiles (Clean up warnings)
// 
// *****************  Version 5  *****************
// User: Johnd        Date: 7/15/98    Time: 8:47a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed a 16 bit compile problem (Getcliprgn16 was left out)
// 
// *****************  Version 4  *****************
// User: Johnd        Date: 4/10/98    Time: 5:06p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Chgd Error Returns to be consistent with other ImageMan Defines
// 
// *****************  Version 3  *****************
// User: Johnd        Date: 3/05/98    Time: 12:09p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// removed _CHAR_UNSIGNED define which caused warning and was extraneous.
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 10/09/97   Time: 2:00p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Cleaned up some warning related to unused vars
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:03p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
//
#ifdef WIN32
#define IMFXDECL __declspec(dllexport)
#else
#define IMFXDECL __export
#endif
#include "internal.h"
#include "fxparam.h"

extern int doBlur(HDC,PEFFECTBLK);
extern int doWipe(HDC,PEFFECTBLK);
extern int doMosaic(HDC,PEFFECTBLK);
extern int doCreepX(HDC,PEFFECTBLK);
extern int doCreepY(HDC,PEFFECTBLK);
extern int doShape(HDC,PEFFECTBLK);
extern int doCurtain(HDC,PEFFECTBLK);
extern int doBlinds(HDC,PEFFECTBLK);


static int WINAPI GetClipRgn16(HDC,HRGN);

static int (WINAPI *pfnGetClipRgn)(HDC,HRGN) ;

static PFNEFFECT pfnEffect[IMFX_ENUM] = {
	NULL,
	doWipe,doWipe,doWipe,doWipe,  // left,right,up,down
	doCurtain,doCurtain,doCurtain,doCurtain,
	doMosaic,
	doCreepX,doCreepX,
	doCreepY,doCreepY,
	doBlur,doBlur,
	doShape,doShape,
	doBlinds,doBlinds,doBlinds,doBlinds // left,right,up,down
};


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//
// ImgDrawImageFX
//
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

int  IMAPI ImgDrawImageFX(HANDLE hImg,
	HDC hDC,LPRECT pDstBounds,LPRECT pSrcBounds,
	UINT nEffect,DWORD dwFlags,DWORD dwExtra,
	IMFXCALLBACK pfnCallback,UINT nInterval,DWORD dwUserInfo, HPALETTE hPal)
{
	LPINTERNALINFO lpII;
	EFFECTBLK eb;
	HPEN hPrevPen;
	UINT lastpercent = 0;
	int result = IMG_OK;

#ifdef WIN32
	pfnGetClipRgn = GetClipRgn;
#else
	pfnGetClipRgn = GetClipRgn16;
#endif
	// check parameters
	if (nEffect >= IMFX_ENUM || !hDC || !pDstBounds) {
		return (IMGFX_INVALIDPARAM);
	}
	eb.rDst = *pDstBounds;

	// handle no-effect displays and metafiles
	if (nEffect == IMFX_NONE) {
		if (hImg) {
simpledraw:
			eb.rDst.right--; eb.rDst.bottom--; // adjust 'cause ImgDrawImage is weird
			result = (ImgDrawImage(hImg,hDC,&eb.rDst,pSrcBounds) == IMG_OK)
				? IMG_OK : IMGFX_INVALIDIMAGE;
		}
		else { // clear entire area
			hPrevPen = SelectPen(hDC,GetStockObject(NULL_PEN));
			Rectangle(hDC,eb.rDst.left,eb.rDst.top,eb.rDst.right+1,eb.rDst.bottom+1);
			SelectPen(hDC,hPrevPen);
		}
		return (pfnCallback ? (*pfnCallback)(hImg,100,dwUserInfo) : result);
	}

	// initialize the effects parameter block
	eb.pdib = NULL;
	eb.hMemDC = NULL;
	eb.nEffect = nEffect;
	eb.dwFlags = dwFlags;
	eb.dwExtra = dwExtra;
	eb.nStepsDone = eb.nStepsTotal = 0;
	eb.hPal = hPal;

	if (nInterval > 100) nInterval = 100;

	if (hImg) {
		if (!(lpII = (LPINTERNALINFO)GlobalLock(hImg))) 
			return (IMGFX_INVALIDIMAGE);
		
		// Load DIB if its not already loaded
		if( !lpII->pDIB )
			ImgLoad( hImg, NULL );

		eb.pdib = (LPBITMAPINFOHEADER) lpII->pDIB->GetDIBPtr();

		if ( !eb.pdib ) {
			GlobalUnlock(hImg);
			goto simpledraw;
		}
		
		eb.pbits = lpII->pDIB->GetDataPtr();

		GlobalUnlock(hImg);

		SetRect(&eb.rSrc,0,0,(int)eb.pdib->biWidth,(int)eb.pdib->biHeight);
	
		// clip to supplied source bounds
		if (pSrcBounds) IntersectRect(&eb.rSrc,&eb.rSrc,pSrcBounds);
	}
	else {
		// remove pen from dst DC so I can use Rectangle to fill background
		hPrevPen = SelectPen(hDC,GetStockObject(NULL_PEN));
	}
	// notice a 1:1 mapping
	eb.b121 = eb.rDst.right-eb.rDst.left == eb.rSrc.right-eb.rSrc.left
		&& eb.rDst.bottom-eb.rDst.top == eb.rSrc.bottom-eb.rSrc.top;
	// get a copy of any currently selected clip region
	eb.hClipRgn = CreateRectRgn(0,0,0,0);
	if ((*pfnGetClipRgn)(hDC,eb.hClipRgn) < 1) {
		DeleteRgn(eb.hClipRgn);
		eb.hClipRgn = NULL;
	}

	// call effect's core routine until complete or interrupted
	do {
		result = (*pfnEffect[nEffect])(hDC, (PEFFECTBLK)&eb);
		// Invoke callback at interval
		if (pfnCallback && result == IMG_OK && eb.nStepsTotal > 0) {
			register UINT percent = (UINT)((eb.nStepsDone*100UL)/eb.nStepsTotal);
			if (percent-lastpercent >= nInterval) {
				lastpercent = percent;
				result = !((*pfnCallback)(hImg,percent,dwUserInfo));
				
			}
		}
	} while (result == IMG_OK && eb.nStepsDone < eb.nStepsTotal);

	// force effect routine to clean up, even if effect aborted
	eb.nStepsDone = eb.nStepsTotal;
	(*pfnEffect[nEffect])(hDC, (PEFFECTBLK)&eb);

	// clean up
	if (eb.hClipRgn) {
		SelectClipRgn(hDC,eb.hClipRgn);
		DeleteRgn(eb.hClipRgn);
	}
	if (hImg) {
		if (eb.hMemDC) {
			DeleteDC(eb.hMemDC);
		}
	}
	else {
		SelectPen(hDC,hPrevPen);
	}

	return (result);
}

/////////////////////////////////////////////////////////////////////////////
//
// GetClipRgn16
//
// Create a copy of a DC's clip region under 16-bit Windows via GDI's
// undocumented "GetClipRgn" call. Note that this region is in screen
// coordinates, and must be translated to client coordinates before
// returning.
//
// Parameters:
//   hdc  - DC we're interested in
//   hrgn - valid region to receive copy of DC's clip region
// Returns:
//   1 - valid copy of clip region returned in 'hrgn'
//   0 - no clip region available in DC
//  -1 -  occured obtaining clip region
//
/////////////////////////////////////////////////////////////////////////////

static int WINAPI GetClipRgn16(HDC hdc,HRGN hrgn)
{
	typedef HRGN (FAR PASCAL *PFNGETCLIP)(HDC);
	static PFNGETCLIP pfnGetClippage;
	HRGN hDCRgn;
	POINT ptOrg;

	if (!pfnGetClippage) {
		HMODULE hmod;

		if (!(hmod = GetModuleHandle("GDI"))) {
			if (!(hmod = LoadLibrary("GDI"))) return (-1);
		}
		if (!(pfnGetClippage = (PFNGETCLIP)GetProcAddress(hmod,"GETCLIPRGN"))) return (-1);
		if ((PFNGETCLIP)GetProcAddress(hmod,MAKEINTRESOURCE(173)) != pfnGetClippage) return (-1);
	}
	if (!(hDCRgn = (*pfnGetClippage)(hdc))) return (0); 
	if (CombineRgn(hrgn,hDCRgn,NULL,RGN_COPY) != ERROR) {
#ifdef WIN32
		GetDCOrgEx(hdc,&ptOrg);
#else
		*(DWORD NEAR *)&ptOrg = GetDCOrg(hdc);
#endif
		OffsetRgn(hrgn,-ptOrg.x,-ptOrg.y);
		return (1);
	}
	return (-1); // indicate an error
}

/////////////////////////////////////////////////////////////////////////////
//
// clipblt
//
// Transfer a portion of the source image onto the destination surface
// thru the given clipping rectangle 'prClip'.
// Attempts to clip without using regions.
//
/////////////////////////////////////////////////////////////////////////////

void clipblt(HDC hDC,PEFFECTBLK peb,LPRECT prClip)
{
	BOOL usergn;

	if (!RectVisible(hDC,prClip)) return;
	if (peb->pdib || peb->hMemDC) {
		if (peb->b121) {
			// Ratio is 1:1. Blt only the pixels needed w/out using a region.
			if (peb->hMemDC) {
				BitBlt(hDC,prClip->left,prClip->top,
					prClip->right-prClip->left,prClip->bottom-prClip->top,
					peb->hMemDC,
					peb->rSrc.left+(prClip->left-peb->rDst.left),
					peb->rSrc.top+(prClip->top-peb->rDst.top),
					SRCCOPY);
			}
			else {
				StretchDIBits(hDC,prClip->left,prClip->top,
					prClip->right-prClip->left,prClip->bottom-prClip->top,
					peb->rSrc.left+(prClip->left-peb->rDst.left),
					(int)peb->pdib->biHeight-peb->rSrc.top+peb->rDst.top-prClip->bottom,
					prClip->right-prClip->left,prClip->bottom-prClip->top,
					peb->pbits,(LPBITMAPINFO)peb->pdib,DIB_RGB_COLORS,SRCCOPY);
			}
		}
		else {
			// Ratio is not 1:1. Blt entire image through a clip region.
			usergn = !EqualRect(prClip,&peb->rDst);
			if (usergn) {
				IntersectClipRect(hDC,prClip->left,prClip->top,prClip->right,prClip->bottom);
			}
			if (peb->hMemDC) {
				StretchBlt(hDC,peb->rDst.left,peb->rDst.top,
					peb->rDst.right-peb->rDst.left,peb->rDst.bottom-peb->rDst.top,
					peb->hMemDC,peb->rSrc.left,peb->rSrc.top,
					peb->rSrc.right-peb->rSrc.left,
					peb->rSrc.bottom-peb->rSrc.top,SRCCOPY);
			}
			else {
				StretchDIBits(hDC,peb->rDst.left,peb->rDst.top,
					peb->rDst.right-peb->rDst.left,peb->rDst.bottom-peb->rDst.top,
					peb->rSrc.left,(int)peb->pdib->biHeight-peb->rSrc.bottom,
					peb->rSrc.right-peb->rSrc.left,peb->rSrc.bottom-peb->rSrc.top,
					peb->pbits,(LPBITMAPINFO)peb->pdib,DIB_RGB_COLORS,SRCCOPY);
			}
			if (usergn) {
				SelectClipRgn(hDC,peb->hClipRgn);
			}
		}
	}
	else {
		// no image, so erase clipped area
		Rectangle(hDC,prClip->left,prClip->top,prClip->right+1,prClip->bottom+1);
	}
}

