/////////////////////////////////////////////////////////////////////////////
//
// fxcurtn.c
//
// ImageMan Effects Module: Curtain and reverse Curtain
// Exports:
//   doCurtain
//
// Version 1.0
// Copyright (c) 1996 Data Techniques, Inc.
// Copyright (c) 1996 Chris Roueche
// All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: fxcurtn.cpp $
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 4/10/98    Time: 5:06p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Chgd Error Returns to be consistent with other ImageMan Defines
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:02p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
//

#define STRICT
#define WIN32_LEAN_AND_MEAN
#define NOSERVICE
#include <windows.h>
#include <windowsx.h>
#include "imgman.h"
#include "fxparam.h"

// Curtain-specific storage can be found in fxparam.h:tag_Curtain

/////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////

static void NEAR initcurtain(PEFFECTBLK peb)
{
	int x;

	// initialize curtain
	peb->dwExtra = LOWORD(peb->dwExtra);
	
	if( !peb->dwExtra )
		  peb->dwExtra = 1;

	peb->curtain.step.cx = peb->curtain.step.cy = (int)peb->dwExtra;
	peb->curtain.pinc = NULL;
	peb->curtain.bbox = peb->rDst;
	peb->curtain.bkbox1 = peb->curtain.bkbox2 = peb->rDst;
	switch (peb->nEffect) {
	case IMFX_VCURTAIN_OUT:
	case IMFX_VCURTAIN_IN:
		peb->curtain.step.cy = 0;
		if (peb->dwFlags&IMDF_CURTAIN_IMGEXPOSE) {
			peb->curtain.pinc = &peb->curtain.step.cx;
		}
		x = (peb->rDst.right+peb->rDst.left)/2; // center
		peb->curtain.bkbox1.right = x;
		peb->curtain.bkbox2.left = x;
		peb->nStepsTotal = peb->rDst.right-x+peb->curtain.step.cx-1;
		if (peb->nEffect == IMFX_VCURTAIN_IN) {
			peb->curtain.step.cx = -peb->curtain.step.cx;
			peb->curtain.pedge1 = &peb->curtain.bkbox1.left;
			peb->curtain.pedge2 = &peb->curtain.bkbox2.right;
		}
		else {
			peb->curtain.bbox.right = peb->curtain.bbox.left = x;
			peb->curtain.pedge1 = &peb->curtain.bkbox1.right;
			peb->curtain.pedge2 = &peb->curtain.bkbox2.left;
		}
		break;
	case IMFX_HCURTAIN_OUT:
	case IMFX_HCURTAIN_IN:
		peb->curtain.step.cx = 0;
		if (peb->dwFlags&IMDF_CURTAIN_IMGEXPOSE) {
			peb->curtain.pinc = &peb->curtain.step.cy;
		}
		x = (peb->rDst.bottom+peb->rDst.top)/2; // center
		peb->curtain.bkbox1.bottom = x;
		peb->curtain.bkbox2.top = x;
		peb->nStepsTotal = peb->rDst.bottom-x+peb->curtain.step.cy-1;
		if (peb->nEffect == IMFX_HCURTAIN_IN) {
			peb->curtain.step.cy = -peb->curtain.step.cy;
			peb->curtain.pedge1 = &peb->curtain.bkbox1.top;
			peb->curtain.pedge2 = &peb->curtain.bkbox2.bottom;
		}
		else {
			peb->curtain.bbox.bottom = peb->curtain.bbox.top = x;
			peb->curtain.pedge1 = &peb->curtain.bkbox1.bottom;
			peb->curtain.pedge2 = &peb->curtain.bkbox2.top;
		}
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//
// doCurtain
//
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

int doCurtain(HDC hDC,PEFFECTBLK peb)
{
	RECT rOrig,rClip;

	if (INITIALIZE(peb)) initcurtain(peb);
	if (CLEANUP(peb)) return (IMG_OK);

	if ((peb->nStepsDone += (UINT)peb->dwExtra) < peb->nStepsTotal) {
		// Move background before messing with image's box
		if (!(peb->dwFlags&IMDF_CURTAIN_OVERBKGND)) {
			ScrollDC(hDC,-peb->curtain.step.cx,-peb->curtain.step.cy,
				&peb->curtain.bkbox1,&peb->curtain.bkbox1,NULL,NULL);
			ScrollDC(hDC,peb->curtain.step.cx,peb->curtain.step.cy,
				&peb->curtain.bkbox2,&peb->curtain.bkbox2,NULL,NULL);
		}
		if (peb->curtain.pinc) {
			// resize both background boxes
			*peb->curtain.pedge1 -= *peb->curtain.pinc;
			*peb->curtain.pedge2 += *peb->curtain.pinc;
		}
	}
	InflateRect(&peb->curtain.bbox,peb->curtain.step.cx,peb->curtain.step.cy);

	// now update the image	
	switch (peb->nEffect) {
	case IMFX_VCURTAIN_OUT:
	case IMFX_HCURTAIN_OUT:
		if (peb->dwFlags&IMDF_CURTAIN_IMGEXPOSE) {
			// expose more of the image
			clipblt(hDC,peb,&peb->curtain.bbox);
		}
		else {
			// image slides out of center axis
			rOrig = peb->rDst;
			IntersectRect(&rClip,&peb->curtain.bbox,&peb->curtain.bkbox1);
			OffsetRect(&peb->rDst,rClip.left-rOrig.left,rClip.top-rOrig.top);
			clipblt(hDC,peb,&rClip);
			peb->rDst = rOrig;
			IntersectRect(&rClip,&peb->curtain.bbox,&peb->curtain.bkbox2);
			OffsetRect(&peb->rDst,rClip.right-rOrig.right,rClip.bottom-rOrig.bottom);
			clipblt(hDC,peb,&rClip);
			peb->rDst = rOrig;
		}
		break;
	case IMFX_VCURTAIN_IN:
	case IMFX_HCURTAIN_IN:
		if (peb->dwFlags&IMDF_CURTAIN_IMGEXPOSE) {
			// expose image in two boxes by excluding area inbetween them
			if (peb->curtain.bbox.left < peb->curtain.bbox.right 
				&& peb->curtain.bbox.top < peb->curtain.bbox.bottom) {
				ExcludeClipRect(hDC,peb->curtain.bbox.left,peb->curtain.bbox.top,
					peb->curtain.bbox.right,peb->curtain.bbox.bottom);
			}
			clipblt(hDC,peb,&peb->rDst);
			SelectClipRgn(hDC,peb->hClipRgn);
		}
		else {
			// image slides in from either side
			rOrig = peb->rDst;
			SubtractRect(&rClip,&peb->curtain.bkbox1,&peb->curtain.bbox);
			OffsetRect(&peb->rDst,rClip.right-peb->curtain.bkbox1.right,rClip.bottom-peb->curtain.bkbox1.bottom);
			clipblt(hDC,peb,&rClip);
			peb->rDst = rOrig;

			SubtractRect(&rClip,&peb->curtain.bkbox2,&peb->curtain.bbox);
			OffsetRect(&peb->rDst,rClip.left-peb->curtain.bkbox2.left,rClip.top-peb->curtain.bkbox2.top);
			clipblt(hDC,peb,&rClip);
			peb->rDst = rOrig;
		}
		break;
	}
	return (IMG_OK);
}

