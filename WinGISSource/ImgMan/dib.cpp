//////////////////////////////////////////////////////////////////////////////
//
// 	DIB.cpp: General purpose DIB handling class
//
//	Copyright(c) 1995 Data Techniques, Inc.
//
//////////////////////////////////////////////////////////////////////////////
//$Header: /ImageMan 16-32/DLL - V 6.xx API/dib.cpp 12    8/22/00 1:54p Johnd $
//$History: dib.cpp $
// 
// *****************  Version 12  *****************
// User: Johnd        Date: 8/22/00    Time: 1:54p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed a problem in the hDIBSection constructor in that it didnt set the
// hDIB member var to NULL.
// 
// *****************  Version 11  *****************
// User: Ericj        Date: 6/07/99    Time: 4:17p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Problem in 16 bit constructor (hDIB, True) where palette size was
// calculated incorrectly.
// 
// *****************  Version 10  *****************
// User: Ericj        Date: 6/07/99    Time: 3:23p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Changed the Destructor to work with the constructor (hNewDib, False).
// 
// *****************  Version 9  *****************
// User: Ericw        Date: 6/01/99    Time: 11:25a
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed bug I introduced on the last checkin.  My mistake.  :-(
// 
// *****************  Version 8  *****************
// User: Ericw        Date: 5/19/99    Time: 2:40p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// As part of bug #161 and a new found issue with copying an image from
// the clipboard.  This was regarding bmps that are 24 bit _and_ also had
// a palette with the biClrUsed value set.
// 
// *****************  Version 7  *****************
// User: Ericw        Date: 5/18/99    Time: 12:56p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// 
// *****************  Version 6  *****************
// User: Ericj        Date: 11/13/98   Time: 12:04p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed 16 bit Dib Constructor in 16 bits used in ImgFromClipboard.
// Fixed 16 bit Data Offset in ImgToClipboard.
// 
// *****************  Version 5  *****************
// User: Johnd        Date: 11/09/98   Time: 12:46p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Misc Optimizations and Fix in DIB.cpp for unitialized hDIB when using
// width/height ctor.
// 
// *****************  Version 4  *****************
// User: Johnd        Date: 11/04/98   Time: 5:10p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Updates to properly update DIBSECTION color table when calling
// GethBitmap()
// 
// *****************  Version 3  *****************
// User: Johnd        Date: 11/02/98   Time: 4:42p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Misc 6.02 Updates
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 10/09/97   Time: 5:15p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// GethDIB now looks at hDIBSection before returning a new DIB.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 7/10/97    Time: 2:02p
// Created in $/imageman 16-32/dll-api level 2
// Initial Lvl 2 Checkin
// 
// *****************  Version 2  *****************
// User: Johnd        Date: 8/08/95    Time: 3:09p
// Updated in $/FaxMan/DLL
// Chgd code to avoid internal compiler error.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 4/18/95    Time: 11:33a
// Created in $/FaxMan/DLL
// Initial Release
// 
#define STRICT

#include <windows.h>
#include <windowsx.h>
#include <math.h>

#include "dib.h"

DIB::DIB( HBITMAP hNewDIBSection )
{
	long nColors;
	long lDIBSize;
	HDC hDC;
	HBITMAP hOldBitmap;

	hDIBSection = NULL;
	lpDIB = NULL;
	lpBMI = NULL;
	hDIB = NULL;

#ifdef WIN32
	DIBSECTION ds;

	if( !GetObject( hNewDIBSection, sizeof(DIBSECTION), &ds ) )
		return;

	lpDIB = ds.dsBm.bmBits;

	if( !lpDIB )
		return;

	nColors = 1 << ds.dsBmih.biBitCount;
	lDIBSize = sizeof(BITMAPINFOHEADER);
	if( nColors <= 256 )
		lDIBSize += sizeof(RGBQUAD) * nColors;


	lpBMI = (LPBITMAPINFO) GlobalAllocPtr(GHND, lDIBSize );


	memcpy( lpBMI, &ds.dsBmih, sizeof(BITMAPINFOHEADER) );

	if( nColors <= 256 ) {
		LPSTR lpRGB;

		hDC = CreateCompatibleDC( NULL );
		if( hDC ) {
			hOldBitmap = (HBITMAP) SelectObject( hDC, hNewDIBSection );

			lpRGB = (LPSTR)lpBMI + sizeof(BITMAPINFOHEADER);
			GetDIBColorTable( hDC, 0, nColors, (RGBQUAD *)lpRGB );

			SelectObject( hDC, hOldBitmap );
			DeleteDC( hDC );
		}

	}
	
	hDIBSection = hNewDIBSection;

#endif

}


DIB::DIB( HANDLE hNewDIB, BOOL bCopy )
{
	long lDIBSize;
	long nColors;

	hDIB = hDIBSection = NULL;
	lpDIB = NULL;


	if( bCopy ) { 
		LPBITMAPINFOHEADER lpbi;

		lpbi = (LPBITMAPINFOHEADER) GlobalLock( hNewDIB );

		if( !lpbi )
			return;

 #ifdef WIN32			
		// Create our DIBSection
		lpBMI = (LPBITMAPINFO) GlobalAllocPtr(GHND, sizeof(BITMAPINFOHEADER) + (256 * sizeof(RGBQUAD)) );
		nColors = GetColors(lpbi) ;

		lDIBSize = sizeof(BITMAPINFOHEADER);
	
		if( nColors <= 256 )
			lDIBSize += sizeof(RGBQUAD) * nColors;

		memcpy( lpBMI, lpbi, lDIBSize );

		// Now lets create a DIB Section

		hDIBSection = CreateDIBSection( NULL, lpBMI, DIB_RGB_COLORS, &lpDIB, NULL, 0 );

		if( !hDIBSection ) {
			lpDIB = NULL;
			GlobalFreePtr( lpBMI );
			lpBMI = NULL;
		}

		// Now copy the DIB info into it

		hmemcpy( lpDIB, (LPSTR)lpbi+lDIBSize, GetImageMemSize() );

 #else
		long lDIBSize;
		long nColors;
		long nHeight;
		long nWidth;

		lpDIB = NULL;
		lpBMI = NULL;
		hDIB = hDIBSection = NULL;

		nColors = GetColors (lpbi) ;

		nHeight = lpbi->biHeight;
		nWidth = lpbi->biWidth;

		lDIBSize = sizeof(BITMAPINFOHEADER) + (sizeof(RGBQUAD) * (nColors < 257 ? nColors : 0));

		// Add size of raster data to lDIBSize
		lDIBSize += ((lpbi->biWidth * lpbi->biBitCount + 31) / 32 * 4) * nHeight;		// RowSize * nHeight
			
		// now Allocate the memory

		hDIB = GlobalAlloc(GHND,  lDIBSize + 16);
		lpDIB = (LPSTR) GlobalLock( hDIB );

		if( !hDIB || !lpDIB)
			return;

		lpBMI = (LPBITMAPINFO)lpDIB;

		hmemcpy( lpBMI, lpbi, lDIBSize );

 #endif
		GlobalUnlock( hNewDIB );
	} else {

		hDIB = hNewDIB;

		lpDIB = (LPSTR) GlobalLock( hDIB );
		lpBMI = (LPBITMAPINFO)lpDIB;

		if( !lpDIB )
			return;
#ifdef WIN32
		lpDIB = (LPSTR)lpDIB + GetBitsOffset();
#else
		lpDIB = (LPSTR)lpDIB;
#endif
	}
}


DIB::DIB( LONG nWidth, LONG nHeight, WORD nBitsPerPixel, BOOL bDIBSection, LPBITMAPINFO lpInitBMI )
{
	long lDIBSize;
	long lHeaderSize;
	long nColors;

	lpDIB = NULL;
	lpBMI = NULL;
	hDIB = hDIBSection = NULL;

	nColors = 1L << nBitsPerPixel;
	lDIBSize = sizeof(BITMAPINFOHEADER);
	if( nColors <= 256 )
		lDIBSize += sizeof(RGBQUAD) * nColors;

	lHeaderSize = lDIBSize;

#ifdef WIN32

	lpBMI = (LPBITMAPINFO) GlobalAllocPtr(GHND, lDIBSize );

#else	// 16 Bit code

	// Add size of raster data to lDIBSize
	lDIBSize += ((nWidth * nBitsPerPixel + 31) / 32 * 4) * nHeight;		// RowSize * nHeight

	lDIBSize += 16;
		
	// now Allocate the memory

	hDIB = GlobalAlloc(GHND,  lDIBSize);
	lpDIB = (LPSTR) GlobalLock( hDIB );

	if( !hDIB || !lpDIB)
		return;

	lpBMI = (LPBITMAPINFO)lpDIB;

#endif

	if( lpInitBMI ) {
		hmemcpy( lpBMI, lpInitBMI, lHeaderSize );

		//Reset the width just in case the Init values are different
		lpBMI->bmiHeader.biWidth = nWidth;
		lpBMI->bmiHeader.biHeight = nHeight;
	} else {

		lpBMI->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		lpBMI->bmiHeader.biWidth = nWidth;
		lpBMI->bmiHeader.biHeight = nHeight;
		lpBMI->bmiHeader.biPlanes = 1;
		lpBMI->bmiHeader.biBitCount = nBitsPerPixel;
		lpBMI->bmiHeader.biSizeImage = 0l;
		lpBMI->bmiHeader.biCompression = BI_RGB;
		lpBMI->bmiHeader.biClrImportant = 0;
		lpBMI->bmiHeader.biClrUsed = 0;
		lpBMI->bmiHeader.biXPelsPerMeter = 0;
		lpBMI->bmiHeader.biYPelsPerMeter =  0;

		// If its B/W then init the default colors
		if( nBitsPerPixel == 1 ) {
			lpBMI->bmiColors[0].rgbRed = 0x0;
			lpBMI->bmiColors[0].rgbGreen = 0x0;
			lpBMI->bmiColors[0].rgbBlue = 0x0;

			lpBMI->bmiColors[1].rgbRed = 0x0ff;
			lpBMI->bmiColors[1].rgbGreen = 0x0ff;
			lpBMI->bmiColors[1].rgbBlue = 0x0ff;
		}
	}

#ifdef WIN32
	if( bDIBSection ) {
		// Now lets create a DIB Section
		hDIBSection = CreateDIBSection( NULL, lpBMI, DIB_RGB_COLORS, &lpDIB, NULL, 0 );
	}
#endif

}

DIB::~DIB(void)
{
	if( hDIB ) {
		GlobalUnlock( hDIB );
		GlobalFree( hDIB );
	}

#ifdef WIN32
	if ( hDIBSection ) {
		DeleteObject( hDIBSection );

		if( lpBMI != lpDIB )
			GlobalFreePtr( lpBMI );
	}
   
#endif
}


LPSTR DIB::GetDataPtr(void)
{
#ifdef WIN32
	return (LPSTR)lpDIB;
#else
	return (LPSTR)lpDIB + GetBitsOffset();
#endif
}



int DIB::GetRowPadding( void )
{
	return (GetRowSize() - (lpBMI->bmiHeader.biWidth * lpBMI->bmiHeader.biBitCount + 7 ) /8);
}

DWORD DIB::GetBitsOffset( void )
{
	DWORD dwSize;

	dwSize = sizeof(BITMAPINFOHEADER);

	if ( GetColors(&lpBMI->bmiHeader)<= 256 )
		dwSize += GetColors(&lpBMI->bmiHeader) * sizeof(RGBQUAD) ;

	return dwSize;
}


DWORD DIB::GetMemSize( void )
{
	DWORD dwSize;

	dwSize = GetBitsOffset() + lpBMI->bmiHeader.biHeight * GetRowSize();	

	return dwSize;
}

DWORD DIB::GetImageMemSize( void )
{
	DWORD dwSize;

	dwSize = lpBMI->bmiHeader.biHeight * GetRowSize();

	return dwSize;
}

DWORD DIB::GetRowSize( void )
{
	return ((lpBMI->bmiHeader.biWidth)*(lpBMI->bmiHeader.biBitCount)+31) / 32 * 4;
}


DWORD DIB::GetPalEntries( void )
{
//	return lpBMI->bmiHeader.biClrUsed ? lpBMI->bmiHeader.biClrUsed : ((1 << lpBMI->bmiHeader.biBitCount) & 0x01ff);
	return GetColors(&lpBMI->bmiHeader ) ;
}


HANDLE DIB::GethDIB( BOOL bNewDIB )
{
	HANDLE hNewDIB;

	if( bNewDIB ) {
		hNewDIB = GlobalAlloc( GHND, GetMemSize() );

		if( hNewDIB ) {
			LPSTR lpNewDIB;
			long lSize;

			lpNewDIB = (LPSTR) GlobalLock( hNewDIB );

			lSize = GetMemSize() - GetImageMemSize();

			hmemcpy( lpNewDIB, lpBMI, lSize );
			
			hmemcpy( lpNewDIB+lSize, GetDataPtr(), GetImageMemSize() );

			GlobalUnlock( hNewDIB );
		}

	} else {
#ifdef WIN32
		if ( hDIBSection ) {
			// Update Color Table if we are palettized
			if( GetBitCount() <= 8 ) {
				HDC hDC;
				HBITMAP hOldBitmap;


				hDC = CreateCompatibleDC( NULL );
				if( hDC ) {
					hOldBitmap = (HBITMAP) SelectObject( hDC, hDIBSection );

					SetDIBColorTable( hDC, 0, GetPalEntries(), lpBMI->bmiColors );

					SelectObject( hDC, hOldBitmap );
					DeleteDC( hDC );
				}
			}

			hNewDIB = hDIBSection ;
		} else
#endif
		  hNewDIB = hDIB ;
	}

	return hNewDIB;
}

LONG DIB::GetColors ( LPBITMAPINFOHEADER lpbmi )
{
	LONG lCols = 0 ;
	if ( lpbmi->biBitCount == 24 )
		lCols = ( 1L << lpbmi->biBitCount ) ;
	else {
		if( lpbmi->biBitCount <= 8 )
			lCols = lpbmi->biClrUsed ? lpbmi->biClrUsed : (1L << lpbmi->biBitCount) ;
	}
	return lCols ;
}
