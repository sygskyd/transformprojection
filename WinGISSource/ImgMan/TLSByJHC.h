///////////////////////////////////////////////////////////////////////////////////////////////
//
// TLSByJHC.h
//
// This is a special header to help new TLS handling without real descriptors preservation
//
//
// mailto:sygsky@poboxes.com
//
///////////////////////////////////////////////////////////////////////////////////////////////
//#include "internal.h"
#include <windows.h>
//#ifndef __TLS_BY_JHC__ 
//#define __TLS_BY_JHC__

#define WINGIS // declare "our handling" of TLS, not system one!!!
#define MAXTHREADNUM 16 //Sygsky: Define from how many threads ImageMan can be loaded in one process

BOOL  bJHCTLSClear( VOID );
BOOL  bJHCTLSFree( VOID );
BOOL  bJHCTLSSetValue( DWORD value);
DWORD nJHCTLSAlloc( VOID );
DWORD nJHCTLSGetValue( VOID);

//#endif // __TLS_BY_JHC__