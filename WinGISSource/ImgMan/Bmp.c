////////////////////////////////////////////////////////////////////////////
//
// bmp.c
//
// Windows Bitmap Format DIL Module
//
// ImageMan Image Processing Library
//
// Copyright (c) 1991-5 Data Techniques, Inc.
// All Rights Reserved
//
/////////////////////////////////////////////////////////////////////////////
// $History: BMP.C $
// 
// *****************  Version 4  *****************
// User: Ericw        Date: 5/19/99    Time: 2:43p
// Updated in $/ImageMan 16-32/DILS
// Fixed bug #161.  Added protection from trying to open 32 bit bitmaps.
// 
// *****************  Version 3  *****************
// User: Johnd        Date: 3/20/98    Time: 8:47a
// Updated in $/ImageMan 16-32/DILS
// Updated to new Version Info Scheme
// 
// *****************  Version 2  *****************
// User: Timk         Date: 6/30/95    Time: 12:04p
// Updated in $/ImageMan 16-32/DILS
// Part 1 of massive changes for read/write using function pointer blocks.
// 
// *****************  Version 1  *****************
// User: Johnd        Date: 1/17/95    Time: 4:02p
// Created in $/ImageMan 16-32/DILS
// ImageMan 4.0 Beta 1
// 

#include "internal.h"
#pragma hdrstop
#include <assert.h>
#include <string.h>
#include "bmp.h"     // stringtable defines

#define VERSION "Bitmap File Reader V1.0"
#define WBUFSIZE  1024
#define _LREADSIZE 32000L

#ifdef NDEBUG
#undef OutputDebugString
#define OutputDebugString(S) ((void)0)
#endif

typedef struct tagFileStruct {
	LPVOID		lpOpenInfo;			// info needed to reopen the image
	LPVOID		lpHandle;		  // handle to file particulars for IMG_IO block functions
	LPIMG_IO		lpIO;				  // IO block 
	long        offset;			  // offset of image in file
	DWORD       rowSize;			  // size of 1 row of the image
	UINT        rows;				  // # of rows in image
	UINT        nCompress;		  // compression type
	BITMAPFILEHEADER bmp;		  // standard BMP file header
}           FileStruct;

typedef int (NEAR * LPFNRLEREADER) (FileStruct FAR *, HPSTR);
int NEAR    ReadRLE4Row(FileStruct FAR * fs, HPSTR lpBuf);
int NEAR    ReadRLE8Row(FileStruct FAR * fs, HPSTR lpBuf);


#ifdef WIN32
#ifdef WINGIS // so the file TLSByJHC.h was included

#define SETERROR(E) bJHCTLSSetValue((DWORD)(E))
#define GETERROR    nJHCTLSGetValue()

#else
// Since we only need to store the error value on a per-thread basis, I'm
// just gonna place it in our DWORD TLS slot.
static DWORD TlsIndex;
#define SETERROR(E) TlsSetValue(TlsIndex,(LPVOID)(DWORD)(E))
#define GETERROR    ((DWORD)TlsGetValue(TlsIndex))

#endif

#else
static int  nErrID;				  // id of latest error
#define SETERROR(E) nErrID = (E)
#define GETERROR    (nErrID)
#endif

static HINSTANCE bmpInstance;
#if DEBUG
static DEBUGPROC DebugStringOut;
#endif

#ifdef WIN32
// /////////////////////////////////////////////////////////////////////////
//
// DllMain
//
// This is the Win32 'DllEntryPoint'. It is similar in function to LibMain
// and the WEP in 16-bit land, but is much more thorough. LibMain is
// called whenever a process or thread attaches or detaches to the
// BMP DIL
//
// Entry:
// hinstDLL  - handle to the ImageMan DLL
// fdwReason - notification code (attach/detach)
// DLL_PROCESS_ATTACH
// DLL_PROCESS_DETACH
// DLL_THREAD_ATTACH
// DLL_THREAD_DETACH
// lpvReserved - NULL during dynamic process attach/detach, non-NULL
// during static attach/detach.
// Exit:
// DLL_PROCESS_ATTACH - TRUE for success, FALSE for failure
// all others         - return code ignored
// /////////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason) {
 		// The DLL is attaching to a process, due to LoadLibrary.
 		case DLL_PROCESS_ATTACH:
			assert(!lpvReserved);  // must always be loaded via LoadLibrary!

//			OutputDebugString("IM2BMP: Process++\n");
			bmpInstance = hinstDLL;// save our identity
#ifndef WINGIS
			// Allocate a "Thread Local Storage" (TLS) index right away.
			if ((TlsIndex = TlsAlloc()) == 0xFFFFFFFF)
				return (FALSE);
#else
			bJHCTLSClear();
#endif
			// Fall thru: Initialize the index for first thread.

 		// The attached process created a new thread.
		case DLL_THREAD_ATTACH:
			OutputDebugString("IM2BMP: Thread++\n");
#ifndef WINGIS
			// Initialize the TLS index for this thread... it's actually 'nErrID'
			if (!TlsSetValue(TlsIndex, (LPVOID) 0))
				return (FALSE);
#else
			nJHCTLSAlloc(); // The same as to nJHCTLSAlloc()
#endif
			break;

 		// The thread of the attached process terminates.
		case DLL_THREAD_DETACH:
			OutputDebugString("IM2BMP: Thread--\n");
#ifdef WINGIS
			bJHCTLSFree(); // Free this thread value
#endif
			break;

 		// The DLL is detaching from a process, due to a call to FreeLibrary.
		case DLL_PROCESS_DETACH:
			OutputDebugString("IM2BMP: Process--\n");
#ifndef WINGIS
			// Release the TLS index.
			TlsFree(TlsIndex);
#else
			bJHCTLSClear();
#endif
			break;
	}

	OutputDebugString("IM2BMP: OK\n");
	return (TRUE);

	UNREFERENCED_PARAMETER(lpvReserved);
}

#else // WIN32

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int WINAPI  LibMain(HANDLE hInstance, WORD wDataSeg, WORD wHeapSize, LPSTR lpCmdLine)
{
	if (!wHeapSize)
		return (0);					  // no heap indicates fatal error

#if DEBUG
	{
		HANDLE      hMod = GetModuleHandle("IMGMAN2.DLL");
		if (hMod)
			DebugStringOut = (DEBUGPROC) GetProcAddress(hMod, "DebugStringOut");
	}
#endif

	bmpInstance = hInstance;
	return (1);

	UNREFERENCED_PARAMETER(wDataSeg);
	UNREFERENCED_PARAMETER(lpCmdLine);
}
#endif // WIN32

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
void WINAPI WEP(int bSystemExit)
{
	return;
	UNREFERENCED_PARAMETER(bSystemExit);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
LPSTR IMAPI IMDLget_ver(void)
{
	return ((LPSTR) (VERSION));
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLescape(HANDLE hImage, int nInCnt, LPVOID lpIn, LPVOID lpOut)
{
	return (IMG_NSUPPORT);
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(nInCnt);
	UNREFERENCED_PARAMETER(lpIn);
	UNREFERENCED_PARAMETER(lpOut);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
BOOL IMAPI  IMDLverify_img(LPSTR lpVerfBuf, LPVOID lpHandle, LPIMG_IO lpIO)
{
	return ((lpVerfBuf[0] == 'B' && lpVerfBuf[1] == 'M'));
	UNREFERENCED_PARAMETER(lpHandle);
	UNREFERENCED_PARAMETER(lpIO);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLinit_image(LPCSTR lpOpenInfo, LPHANDLE hFile, LPVOID lpHandle, long offset, long lLen, LPIMG_IO lpIO)
{
	FileStruct FAR *fs;
	BITMAPFILEHEADER bmp;
	BITMAPINFOHEADER bi;
	int         retval;

//	DEBUGOUT(DILAPI, "\t%s-->init_image(%s,%x:%x,%d,%ld,%ld)", (LPSTR) __FILE__, (LPSTR) file, HIWORD(hFile), LOWORD(hFile), offset, lLen);

	// read in the bmp header - abort if can't read it
	lpIO->seek(lpHandle, offset, 0);

	if ( (lpIO->read(lpHandle, (LPSTR)&bmp, sizeof(bmp)) != sizeof(bmp))) {
		lpIO->close(lpHandle);
		SETERROR(IDS_READERR);
		return (IMG_FILE_ERR);
	}
	retval = IMG_OK;

	// check the header info
	if ((((LPSTR) & bmp.bfType)[0] != 'B') && (((LPSTR) & bmp.bfType)[1] != 'M')) {
		retval = IMG_FILE_ERR;
		SETERROR(IDS_BADMAN);
	}
	// check for valid compression scheme
	lpIO->seek(lpHandle, offset+sizeof(BITMAPFILEHEADER), 0);

	lpIO->read(lpHandle, (LPSTR)&bi, sizeof(BITMAPINFOHEADER));

	if (bi.biSize != sizeof(BITMAPINFOHEADER) && bi.biSize != sizeof(BITMAPCOREHEADER)) {
		retval = IMG_FILE_ERR;
		SETERROR(IDS_BADVER);
	}
	if (retval != IMG_OK) {
		lpIO->close(lpHandle);
		return (retval);
	}
	// create new internal structure
	if (!(*hFile = GlobalAlloc(DLLHND, sizeof(FileStruct)))) {
		lpIO->close(lpHandle);
		SETERROR(IDS_MEMERR);
		return (IMG_NOMEM);
	}
	// fill in the structure
	fs = (FileStruct FAR *) GlobalLock(*hFile);
	fs->lpOpenInfo = (LPVOID)lpOpenInfo;
	fs->lpHandle = lpHandle;
	fs->lpIO = lpIO;
	fs->offset = offset;
	// fs->imbedlen = lLen;
	fs->bmp = bmp;
	fs->rowSize = 0;				  // these are not known 'till buildDIBhead

	fs->rows = 0;

	GlobalUnlock(*hFile);

	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_ext(HGLOBAL hFile, LPSTR ext)
{
	DEBUGOUT(DILAPI, "\t%s-->set_ext(%d,%s)", (LPSTR) __FILE__, hFile, (LPSTR) ext);
	// no extensions to differentiate between - just return OK
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLclose_image(HGLOBAL hFile)
{
	FileStruct FAR *fs;

	DEBUGOUT(DILAPI, "\t%s-->close_image(%d)", (LPSTR) __FILE__, hFile);
	// check to see if file is still open - if it is, close it
	if ((fs = (FileStruct FAR *) GlobalLock(hFile)) == NULL) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (fs->lpHandle) {
		fs->lpIO->close(fs->lpHandle);
		fs->lpHandle = NULL;
	}

	GlobalUnlock(hFile);
	GlobalFree(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLimg_name(HGLOBAL hFile, HANDLE FAR * lpHand)
{
	FileStruct FAR *fs;

	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
//	*lpHand = fs->fName;

	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLopen(HGLOBAL hFile, LPVOID lpHandle)
{
	FileStruct FAR *fs;

	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (!fs->lpHandle) {
		if (fs->lpIO->open(fs->lpOpenInfo, OF_READ, &(fs->lpHandle)) != IMG_OK) {
			GlobalUnlock(hFile);
			SETERROR(IDS_NOOPEN);
			return (IMG_NOFILE);
		}
	}
	fs->lpIO->seek(fs->lpHandle, fs->offset+fs->bmp.bfOffBits+fs->rowSize*(fs->rows-1), 0);

	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLclose_keep(HANDLE hFile)
{
	FileStruct FAR *fs;

	DEBUGOUT(DILAPI, "\t%s-->close_keep(%d)", (LPSTR) __FILE__, hFile);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
//		fs->fHand = -1;
//	}
	if (fs->lpHandle) {
		fs->lpIO->close(fs->lpHandle);
		fs->lpHandle = NULL;
	}

	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLread_rows(HGLOBAL hFile, LPSTR buf, UINT rows, UINT rowSize, UINT start, LPSTATUS lpStat)
{
	FileStruct FAR *fs;
	long        fpos;
	HPSTR       tmpBuf = (HPSTR) buf;

	DEBUGOUT(DILAPI, "\t%s-->%s(%d, %x:%x, %u, %u, %u, %x:%x)", (LPSTR) __FILE__, (LPSTR) "read_rows", hFile, HIWORD(buf), LOWORD(buf), rows, rowSize, start, HIWORD(lpStat), LOWORD(lpStat));
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	// do a quickie if we're reading the entire image
	if (rows == fs->rows && fs->nCompress == BI_RGB) {
		fs->lpIO->seek(fs->lpHandle, fs->offset+fs->bmp.bfOffBits, 0);
		fs->lpIO->read(fs->lpHandle, tmpBuf, (LONG)rows*rowSize);

	} else {
		if (fs->nCompress == BI_RGB) {	// no compression

			tmpBuf += (rows - 1) * (DWORD) rowSize;
			while (rows--) {
				fpos = fs->lpIO->seek(fs->lpHandle, 0, 1);
				if (start) {
					fs->lpIO->seek(fs->lpHandle, start, 1);
				}
				

				fs->lpIO->read(fs->lpHandle, tmpBuf, rowSize);
				tmpBuf -= rowSize;
				fs->lpIO->seek(fs->lpHandle, fpos-fs->rowSize, 0);

				lpStat->lAcc += rowSize;
				if (lpStat->lpfnStat && lpStat->lAcc > lpStat->lInterval) {
					lpStat->lDone += lpStat->lAcc;
					lpStat->lDone = min(lpStat->lDone, lpStat->lTotal);

					if( !(*lpStat->lpfnStat) (lpStat->hImage, (int) ((lpStat->lDone * 100) / lpStat->lTotal), lpStat->dwUserInfo) ) {
						GlobalUnlock(hFile);
						return IMG_CANCELLED;
					}
					lpStat->lAcc %= lpStat->lInterval;
				}
			}
		} else {						  // RLE4 and RLE8

			BOOL        usescratch, rleok;
			LPFNRLEREADER lpfnRead;
			HPSTR       lpScratch;

			usescratch = (fs->rowSize != rowSize);

			if (!(lpScratch = (HPSTR) GlobalAllocPtr(GHND, fs->rowSize + 16))) {
				SETERROR(IDS_MEMERR);
				return (IMG_NOMEM);
			}
			lpfnRead = fs->nCompress == BI_RLE8 ? ReadRLE8Row : ReadRLE4Row;

			while (rows--) {
				// If we're reading a whole line then decompress into the output
				// buffer otherwise decompress into a temp buf then copy the required
				// portion into the output buf.
				if (usescratch) {
					rleok = (*lpfnRead) (fs, lpScratch);
					hmemcpy(tmpBuf, lpScratch + start, rowSize);
				} else
					rleok = (*lpfnRead) (fs, tmpBuf);

				if (!rleok)
					break;

				tmpBuf += rowSize;

				lpStat->lAcc += rowSize;
				if (lpStat->lpfnStat && lpStat->lAcc > lpStat->lInterval) {
					lpStat->lDone += lpStat->lAcc;
					lpStat->lDone = min(lpStat->lDone, lpStat->lTotal);

					if( !(*lpStat->lpfnStat) (lpStat->hImage, (int) ((lpStat->lDone * 100) / lpStat->lTotal), lpStat->dwUserInfo) ) {
						GlobalFreePtr( lpScratch );
						GlobalUnlock(hFile);
						return IMG_CANCELLED;
					}

					lpStat->lAcc %= lpStat->lInterval;
				}
			}
			GlobalFreePtr(lpScratch);

			if (!rleok) {
				SETERROR(IDS_READERR);
				return (IMG_FILE_ERR);
			}
		}
	}
	GlobalUnlock(hFile);
	return (IMG_OK);
}


// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_row(HGLOBAL hFile, UINT row)
{
	FileStruct FAR *fs;
	HPSTR       lpScratch;

	DEBUGOUT(DILAPI, "\t%s-->set_row(%d,%d)", (LPSTR) __FILE__, hFile, row);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	if (fs->nCompress == BI_RGB) {
		fs->lpIO->seek(fs->lpHandle, fs->offset + fs->bmp.bfOffBits + fs->rowSize * (fs->rows - row - 1), 0);
	} else {
		BOOL        rleok = TRUE;
		LPFNRLEREADER lpfnRead;

		lpScratch = GlobalAllocPtr(GHND, fs->rowSize);

		lpfnRead = fs->nCompress == BI_RLE8 ? ReadRLE8Row : ReadRLE4Row;
		fs->lpIO->seek(fs->lpHandle, fs->offset + fs->bmp.bfOffBits, 0);

		while (row--) {
			if (!(rleok = (*lpfnRead) (fs, lpScratch)))
				break;
		}

		GlobalFreePtr(lpScratch);

		if (!rleok) {
			SETERROR(IDS_READERR);
			return (IMG_FILE_ERR);
		}
	}
	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLbuildDIBhead(HGLOBAL hFile, LPINTERNALINFO lpii)
{
	LPBITMAPINFOHEADER lpbi;
	LPBITMAPCOREHEADER lpci;
	BITMAPCOREHEADER bc;
	RGBTRIPLE FAR *lpTrip;
	LPRGBQUAD   lpQuad;
	int         nColors, j;
	FileStruct FAR *fs;
	HGLOBAL     hMem;

	DEBUGOUT(DILAPI, "\t%s-->buildDIBhead(%d)", (LPSTR) __FILE__, hFile);
	if (!(fs = (FileStruct FAR *) GlobalLock(hFile))) {
		SETERROR(IDS_INVHAND);
		return (IMG_INV_HAND);
	}
	//
	// when we allcate space for the BITMAPINFOHEADER struct, we allocate
	// a little extra on the end. This simplifies dealing with OS/2
	// bitmap formats by assuring that we allocate enough space to
	// hold the (larger) BITMAPINFOHEADER and the given palette information.
	// We also allocate an extra 256 bytes to allow for maximum # of extra
	// bytes due to expanded palette entries (BITMAPINFOHEADER uses RGBQUAD,
	// BITMAPCOREHEADER uses RGBTRIPLE).
	//
	if ((hMem = GlobalAlloc(GHND,
									fs->bmp.bfOffBits - sizeof(BITMAPFILEHEADER) + 256 + (sizeof(BITMAPINFOHEADER) - sizeof(BITMAPCOREHEADER)))) == NULL) {
		SETERROR(IDS_MEMERR);
		return (IMG_NOMEM);
	}
	lpbi = (LPBITMAPINFOHEADER) GlobalLock(hMem);

	// read in the header
	fs->lpIO->seek(fs->lpHandle, fs->offset + sizeof(BITMAPFILEHEADER), 0);
	fs->lpIO->read(fs->lpHandle, (HPSTR)lpbi, (int) fs->bmp.bfOffBits - sizeof(BITMAPFILEHEADER));

	if ( (lpbi->biBitCount == 16) ||
		   (lpbi->biBitCount == 32) ) {
		GlobalUnlock( hFile ) ;
		SETERROR( IDS_BADVER ) ;
		return ( IMG_NSUPPORT ) ;
	}
	if ( lpbi->biBitCount > 8 )
		lpbi->biClrUsed = 0 ;
	if (lpbi->biSize == sizeof(BITMAPCOREHEADER)) {
		lpci = (LPBITMAPCOREHEADER) lpbi;
		bc = *lpci;
		//
		// first we need to move the palette info down to correct location
		// in the BITMAPINFOHEADER, 'cause in a BITMAPCOREHEADER the
		// palette is nearer to the top of the struct.
		//
		nColors = (1 << bc.bcBitCount) & 0x01FF;
		//
		// start at last RGBxxxx entry and work backwards...
		//
		lpTrip = (RGBTRIPLE FAR *) ((LPSTR) lpbi + sizeof(BITMAPCOREHEADER));
		lpQuad = (LPRGBQUAD) ((LPSTR) lpbi + sizeof(BITMAPINFOHEADER));
		lpTrip += nColors - 1;
		lpQuad += nColors - 1;

		for (j = 0; j < nColors; j++) {
			lpQuad->rgbBlue = lpTrip->rgbtBlue;
			lpQuad->rgbRed = lpTrip->rgbtRed;
			lpQuad->rgbGreen = lpTrip->rgbtGreen;
			lpQuad->rgbReserved = 0;
			lpQuad--;
			lpTrip--;
		}

		lpbi->biSize = sizeof(BITMAPINFOHEADER);
		lpbi->biWidth = bc.bcWidth;
		lpbi->biHeight = bc.bcHeight;
		lpbi->biPlanes = 1;
		lpbi->biBitCount = bc.bcBitCount;
		lpbi->biCompression = BI_RGB;
		lpbi->biSizeImage = ROWSIZE(lpbi->biWidth, lpbi->biBitCount) * lpbi->biHeight;
		lpbi->biXPelsPerMeter = 0;
		lpbi->biYPelsPerMeter = 0;
		lpbi->biClrUsed = 0;
		lpbi->biClrImportant = 0;
	}
	lpii->lOrgWid = lpbi->biWidth;	// set height&wid for Imgmgr

	lpii->lOrgHi = lpbi->biHeight;
	lpii->bFlags = 0;

	lpbi->biSizeImage = 0;
	fs->nCompress = (UINT) lpbi->biCompression;
	lpbi->biCompression = BI_RGB;
	fs->rowSize = ROWSIZE(lpbi->biWidth, lpbi->biBitCount);
	fs->rows = (UINT) lpbi->biHeight;

	lpii->lpbi = (LPBITMAPINFO) lpbi;

	GlobalUnlock(hFile);
	return (IMG_OK);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLprint(HGLOBAL hFile, HDC hPrnDC, LPRECT lpDest, LPRECT lpSrc)
{
	return (IMG_NOTAVAIL);
	UNREFERENCED_PARAMETER(hFile);
	UNREFERENCED_PARAMETER(hPrnDC);
	UNREFERENCED_PARAMETER(lpDest);
	UNREFERENCED_PARAMETER(lpSrc);
}


// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLrender(HGLOBAL hFile, HDC hDC, LPRECT lpDest, LPRECT lpSrc)
{
	return (IMG_ERR);
	UNREFERENCED_PARAMETER(hFile);
	UNREFERENCED_PARAMETER(hDC);
	UNREFERENCED_PARAMETER(lpDest);
	UNREFERENCED_PARAMETER(lpSrc);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLload_wmf(HANDLE hImage, HMETAFILE FAR * lpmeta, LPRECT lpbbox, LPIMG_IO lpio)
{
	return (IMG_ERR);
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(lpmeta);
	UNREFERENCED_PARAMETER(lpbbox);
	UNREFERENCED_PARAMETER(lpio);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLget_page(HANDLE hImage, LPINT nPage)
{
	return (IMG_ERR);
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(nPage);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLset_page(HANDLE hImage, int nPage)
{
	return (IMG_ERR);
	UNREFERENCED_PARAMETER(hImage);
	UNREFERENCED_PARAMETER(nPage);
}

// /////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////
int IMAPI   IMDLerror_string(LPSTR lpszBuf, int nMaxLen)
{
	LoadString(bmpInstance, GETERROR, lpszBuf, nMaxLen);
	return (IMG_OK);
}

int NEAR    ReadRLE8Row(FileStruct FAR * fs, HPSTR lpBuf)
{
	int         count;
	int         data;
	int         i;
	unsigned char HUGE *lpOut;

	lpOut = lpBuf;

	while (1) {
		count = fs->lpIO->getchar(fs->lpHandle);
		data = fs->lpIO->getchar(fs->lpHandle);

		if (count != 0) {			  // Encoded Run

			while (count--)
				*lpOut++ = data;
		} else {
			if (data == 0)			  // End of Line

				break;
			else if (data == 1)	  // End of Bitmap

				break;
			else if (data == 2) {  // Delta - Not currently supported

				OutputDebugString("\r\nDelta encountered in RLE Bitmap");
				break;
			} else {					  // Absolute encoding

				for (i = 0; i < data; i++) {
					*lpOut++ = fs->lpIO->getchar(fs->lpHandle);
				}

				if (data & 1)		  // Always Padded to word boundary

					fs->lpIO->getchar(fs->lpHandle);
			}
		}
	}
	return TRUE;
}

int NEAR    ReadRLE4Row(FileStruct FAR * fs, HPSTR lpBuf)
{
	int         count;
	unsigned int data;
	unsigned char HUGE *lpOut;
	int         xpos;
	char        SrcShft;
	char        DstShft;
	unsigned char tmp;
	char        OddLen;

	lpOut = lpBuf;

	xpos = 0;
	DstShft = 4;

	while (1) {
		count = fs->lpIO->getchar(fs->lpHandle);
		data = fs->lpIO->getchar(fs->lpHandle);

		if (count != 0) {			  // Encoded Run

			SrcShft = 4;

			while (count--) {

				if (xpos && !(xpos % 2))
					lpOut++;

				tmp = (data >> SrcShft);
				tmp &= 0x0f;
				tmp <<= DstShft;
				*lpOut |= tmp;

				DstShft = DstShft ? 0 : 4;
				SrcShft = SrcShft ? 0 : 4;

				xpos++;
			}

		} else {
			if (data == 0)			  // End of Line

				break;
			else if (data == 1)	  // End of Bitmap

				break;
			else if (data == 2) {  // Delta - Not currently supported
				// Eat the two delta coordinates

				fs->lpIO->getchar(fs->lpHandle);
				fs->lpIO->getchar(fs->lpHandle);

				break;
			} else {					  // Absolute encoding

				count = data;

				if (((count + 1) / 2) % 2)	// Always Padded to word boundary

					OddLen = 1;
				else
					OddLen = 0;

				SrcShft = 4;

				while (count--) {
					if (SrcShft)
						data = fs->lpIO->getchar(fs->lpHandle);

					if (xpos && !(xpos % 2))
						lpOut++;

					tmp = (data >> SrcShft);
					tmp &= 0x0f;
					tmp <<= DstShft;
					*lpOut |= tmp;

					DstShft = DstShft ? 0 : 4;
					SrcShft = SrcShft ? 0 : 4;

					xpos++;
				}

				if (OddLen)			  // Always Padded to word boundary

					fs->lpIO->getchar(fs->lpHandle);
			}
		}
	}

	return TRUE;
}

#if DEBUG
void __cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	va_list     vaArgs;

	if (DebugStringOut) {
		va_start(vaArgs, lpFormat);
		(*DebugStringOut) (wCategory, lpFormat, vaArgs);
		va_end(vaArgs);
	}
}
#else
void __cdecl FAR Debug(UINT wCategory, LPSTR lpFormat,...)
{
	UNREFERENCED_PARAMETER(wCategory);
	UNREFERENCED_PARAMETER(lpFormat);
}
#endif
