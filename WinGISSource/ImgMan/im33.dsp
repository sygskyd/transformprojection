# Microsoft Developer Studio Project File - Name="im33" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) External Target" 0x0106

CFG=im33 - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "im33.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "im33.mak" CFG="im33 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "im33 - Win32 Release" (based on "Win32 (x86) External Target")
!MESSAGE "im33 - Win32 Debug" (based on "Win32 (x86) External Target")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""

!IF  "$(CFG)" == "im33 - Win32 Release"

# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Cmd_Line "NMAKE /f im32.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "im32.exe"
# PROP BASE Bsc_Name "im32.bsc"
# PROP BASE Target_Dir ""
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Cmd_Line "NMAKE /f im32.mak RELEASE=1 USELZW=1"
# PROP Rebuild_Opt "/a"
# PROP Target_File "im33.exe"
# PROP Bsc_Name "im33.bsc"
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "im33 - Win32 Debug"

# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Cmd_Line "NMAKE /f im32.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "im32.exe"
# PROP BASE Bsc_Name "im32.bsc"
# PROP BASE Target_Dir ""
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Cmd_Line "NMAKE /f im32.mak DEBUG=1 USELZW=1"
# PROP Rebuild_Opt "/a"
# PROP Target_File "im33.exe"
# PROP Bsc_Name "im33.bsc"
# PROP Target_Dir ""

!ENDIF 

# Begin Target

# Name "im33 - Win32 Release"
# Name "im33 - Win32 Debug"

!IF  "$(CFG)" == "im33 - Win32 Release"

!ELSEIF  "$(CFG)" == "im33 - Win32 Debug"

!ENDIF 

# Begin Source File

SOURCE=.\im32.mak
# End Source File
# End Target
# End Project
