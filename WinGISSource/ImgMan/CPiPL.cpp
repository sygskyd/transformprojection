/////////////////////////////////////////////////////////////////////////////
//
//    CPiPL.cpp
//
//    ImageMan Plug-In Host PiPL Class module
//
//    ImageMan Image Processing Library
//
//    Copyright (c) 1998 Data Techniques, Inc.
//    All Rights Reserved
//	 
/////////////////////////////////////////////////////////////////////////////
//$Header: /ImageMan 16-32/DLL - V 6.xx API/CPiPL.cpp 4     1/05/00 3:19p Ericw $
//
//$History: CPiPL.cpp $
// 
// *****************  Version 4  *****************
// User: Ericw        Date: 1/05/00    Time: 3:19p
// Updated in $/ImageMan 16-32/DLL - V 6.xx API
// Fixed problem when loading Filter Meister plug-ins.
// 
// *****************  Version 3  *****************
// User: Ericw        Date: 4/24/98    Time: 10:58a
// Updated in $/ImageMan 16-32/DLL-API Level 2
// PiMI emulation for the older style filters that don't have PiPL.
// 
// *****************  Version 2  *****************
// User: Ericw        Date: 4/08/98    Time: 3:54p
// Updated in $/ImageMan 16-32/DLL-API Level 2
// Fixed the comment header - no code.
// 

#define STRICT

#include <stdlib.h>
#include "internal.h"
#pragma hdrstop
#include "pigeneral.h"

typedef void (*ENTRYPROC)(short, LPVOID, LPLONG, short*);

#include "cpipl.h"

extern "C" {
	BOOL CALLBACK EnumNameProc ( HANDLE hMod, LPCTSTR lpType, LPTSTR lpId, LPINT lParam )
	{
		*lParam = (int)lpId ;
		return TRUE ;
//		return FALSE ;
	}
}

CPiPL :: CPiPL( HMODULE hPlugIn )
{
	pProps = NULL ;
	lCount = 0 ;
	lErr = 0 ;
	lErr = LoadPiPL ( hPlugIn ) ;
}

CPiPL :: ~CPiPL( )
{
	long lxx ;
	if ( pProps )
		{
			for ( lxx = 0 ; lxx < lCount ; lxx ++ )
				if ( pProps[lxx].propertyData )
					GlobalFreePtr(pProps[lxx].propertyData) ;
			GlobalFreePtr ( pProps ) ;
			pProps = NULL ;
		}
}

int CPiPL :: LocatePiPLID ( HMODULE hPlugIn, BOOL * bIsPiMI )
{
	int iId = 0 ;

	if ( ! (EnumResourceNames ( hPlugIn, "PIPL", (ENUMRESNAMEPROC)EnumNameProc, (LONG)&iId )) ) {
		// If the PiPL fails go for an old PiMI resource (pre-Photoshop 3.0 hosts).
		if ( ! (EnumResourceNames ( hPlugIn, "PIMI", (ENUMRESNAMEPROC)EnumNameProc, (LONG)&iId )) )
			lErr = GetLastError() ;
		else
			*bIsPiMI = TRUE ;
	} else
		*bIsPiMI = FALSE ;

	return iId ;
}

long CPiPL :: LoadPiPL ( HMODULE hPlugIn )
{
	BOOL bPiMI ;
	int iPIPLID = LocatePiPLID ( hPlugIn, & bPiMI ) ;
	HRSRC   hRes  ;
	HGLOBAL hData ;
	LPBYTE  lpData = NULL ;
	LPSTR lpResType ;

	if ( iPIPLID == 0 )
		return ( IMG_ERR ) ;
	lpResType = bPiMI ? "PIMI" : "PIPL" ;
	if ( hRes = FindResource ( hPlugIn, MAKEINTRESOURCE(iPIPLID), lpResType ) ) {
		if ( hData = LoadResource( hPlugIn, hRes ) ) {
			lpData = (LPBYTE)LockResource( hData ) ;
		}
		else
			lErr = GetLastError () ;
	}
	else
		lErr = GetLastError() ;
	if ( lpData ) {
		BuildPiPL( lpData, bPiMI ) ;
	}
	return ( IMG_OK ) ;
}

long CPiPL :: BuildPiPL ( LPBYTE lpData, BOOL bIsPiMI )
{
	long lxx ;
	long lLen ;
	int  iValLen ;
	LPBYTE lpValue ;

		lpData += 2 ;
	if ( ! bIsPiMI ) {
		lVers  = ((LPLONG)lpData)[0] ;
		lCount = ((LPLONG)lpData)[1] ;
	} else
		lCount = 3 ;

	pProps = ( LPPROPS )GlobalAllocPtr ( GHND, lCount * sizeof(PIProperty) ) ;
	if ( bIsPiMI ) {
		LPSTR lpEntry = "ENTRYPOINT1" ;
		lxx = -1 ;
		while ( lpData[++lxx] ) ;
		if ( lxx ) {
			pProps[0].propertyKey = PINameProperty ;
			pProps[1].propertyKey = PICategoryProperty ;
			pProps[0].propertyData = (LPSTR)GlobalAllocPtr(GHND, lxx + 1 );
			pProps[1].propertyData = (LPSTR)GlobalAllocPtr(GHND, lxx + 1 );
			strcpy ( pProps[0].propertyData, (LPSTR)lpData ) ;
			strcpy ( pProps[1].propertyData, (LPSTR)lpData ) ;
		}
		pProps[2].propertyKey = PIWin32X86CodeProperty ;
		pProps[2].propertyData = (LPSTR)GlobalAllocPtr(GHND, strlen(lpEntry) + 1 );
		strcpy ( pProps[2].propertyData, lpEntry ) ;
		return ( IMG_OK ) ;
	}
	// otherwise let the real PiPL get built

	lpData += 8 ;
	for ( lxx = 0 ; lxx < lCount ; lxx++ )
		{
			pProps[lxx].vendorID    = ((LPLONG)lpData)[0] ;
			pProps[lxx].propertyKey = ((LPLONG)lpData)[1] ;
			pProps[lxx].propertyID  = ((LPLONG)lpData)[2] ;
			lLen = ((LPLONG)lpData)[3] ;		// always a multiple of four
			lpValue = lpData + ( sizeof(long) * 4 ) ;
			switch ( pProps[lxx].propertyKey )
				{
					// special case for the code property, note there is no length
					// embedded in the data
					case PIWin32X86CodeProperty :
					/*
						iValLen = (int) *lpValue ;
						pProps[lxx].propertyData = (LPSTR)GlobalAllocPtr(GHND, iValLen + 1 );
						memcpy ( pProps[lxx].propertyData, lpValue, iValLen ) ;
						pProps[lxx].propertyData[iValLen] = '\0'  ;
					*/
						// According to Alex Hunter, author of Filter Meister, the above code
						// should be like this.....
						pProps[lxx].propertyData = (LPSTR)GlobalAllocPtr(GHND, strlen((char*)lpValue)+1);
						strcpy ( pProps[lxx].propertyData, (char*)lpValue ) ;
						break ;

					case PINameProperty :
					case PICategoryProperty :
						iValLen = (int) *lpValue ;
						pProps[lxx].propertyData = (LPSTR)GlobalAllocPtr(GHND, iValLen + 1 );
						memcpy ( pProps[lxx].propertyData, (lpValue+1), iValLen ) ;
						pProps[lxx].propertyData[iValLen] = '\0'  ;
						break ;

					default :		// do nothing
						break ;
				}
			lpData += ( sizeof(int32) * 4 ) + lLen ;
		}
	return IMG_OK ;
}

LPSTR CPiPL :: GetValue ( PIType propKey )
{
	long lxx = 0 ;
	if ( ! pProps )
		return NULL ;
	do
		{
			if ( propKey == pProps[lxx].propertyKey )
				return pProps[lxx].propertyData ;
		}
	while
		( ++lxx < lCount ) ;
	return NULL ;
}
