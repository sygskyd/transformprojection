unit gissrvmain;

interface

uses
  Windows, SysUtils, Classes, HTTPApp, SyncObjs;

type
  TGisServerWebModule = class(TWebModule)
    procedure WebModuleException(Sender: TObject; E: Exception; var Handled: Boolean);
    procedure WebModule1WebActionItemAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
  end;

var
  GisServerWebModule: TGisServerWebModule;
  CriticalSection: TCriticalSection;

const ColumnSeparator = '|';
const SyncFileExt = '.scl';

implementation

{$R *.DFM}

function BuildUrl(Request: TWebRequest): AnsiString;
var
i,n,APos: Integer;
s: AnsiString;
begin
        s:=Request.URL;
        APos:=1;
        n:=StrLen(PChar(s));
        for i:=1 to n do begin
                if s[i] = '/' then begin
                        APos:=i;
                end;
        end;
        s:=Copy(s,1,APos);
        Result:='http://' + Request.Host + s;
end;

function GetFileDate(AFileName: AnsiString): Double;
begin
     try
        Result:=FileDateToDateTime(FileAge(AFileName));
     except
        Result:=0;
     end;
end;

function OpGetList(AUrl,ADir: AnsiString): AnsiString;
var
ASearch: TSearchRec;
begin
        Result:='RES_OK|';
        if FindFirst(ADir + '\*.amp', faAnyFile, ASearch) = 0 then begin
                Result:=Result + AUrl + 'data/' + ASearch.Name;
                while FindNext(ASearch) = 0 do begin
                        Result:=Result + '|' + AUrl + 'data/' + ASearch.Name;
                end;
                FindClose(ASearch);
        end;
end;

function OpNewLog(Request: TWebRequest; ADataDir: AnsiString): AnsiString;
var
ALine,ASyncFileName: AnsiString;
F: TextFile;
AHandle: Integer;
begin
        Result:='RES_ERR_SYNC_UNKNOWN';

        ASyncFileName:=Trim(Request.QueryFields.Values['syncfile']);
        if ASyncFileName = '' then begin
                Result:='RES_ERR_SYNC_NO_FILENAME';
                exit;
        end;

        ALine:=Trim(Request.Content);
        if ALine = '' then begin
                Result:='RES_ERR_SYNC_NO_CONTENT';
                exit;
        end;

        ASyncFileName:=ADataDir + ASyncFileName;

        if not FileExists(ASyncFileName) then begin
                AHandle:=FileCreate(ASyncFileName);
                if AHandle <> 0 then begin
                        FileClose(AHandle);
                end
                else begin
                        Result:='RES_ERR_SYNC_CREATE_FILE';
                end;
        end;

        if FileExists(ASyncFileName) then begin
                CriticalSection.Enter;
                try
                        Result:='RES_ERR_SYNC_WRITE_FILE';
                        AssignFile(F,ASyncFileName);
                        Append(F);
                        WriteLn(F,ALine);
                        CloseFile(F);
                        Result:='RES_OK';
                finally
                        CriticalSection.Leave;
                end;
        end
        else begin
                Result:='RES_ERR_SYNC_NO_FILE';
        end;
end;

function OpGetFileDate(Request: TWebRequest; ADataDir: AnsiString): AnsiString;
var
AFileName: AnsiString;
s: AnsiString;
begin
        Result:='RES_ERR_FILE_DATE';
        AFileName:=ADataDir + Request.QueryFields.Values['file'];
        if FileExists(AFileName) then begin
                s:=FloatToStr(GetFileDate(AFileName));
                s:=StringReplace(s,',','.',[]);
                Result:='RES_OK|' + s;
        end;
end;

procedure TGisServerWebModule.WebModuleException(Sender: TObject; E: Exception;
  var Handled: Boolean);
begin
        Response.Content:='RES_EXCEPTION|' + E.Message;
        Response.SendResponse;
end;

procedure TGisServerWebModule.WebModule1WebActionItemAction(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
Buffer: Array[0..255] of Char;
ModuleFileName: AnsiString;
ModuleDir,DataDir: AnsiString;
Op: AnsiString;
AUrl: AnsiString;
begin
        Response.ContentType:='text/plain';
        Response.Content:='RES_UNKNOWN';

        DecimalSeparator:='.';

        AUrl:=BuildUrl(Request);

        ZeroMemory(@Buffer,Sizeof(Buffer));
        GetModuleFileName(hInstance,Buffer,Sizeof(Buffer));
        ModuleFileName:=StrPas(Buffer);
        ModuleFileName:=StringReplace(ModuleFileName,'\\?\','',[]);

        ModuleDir:=ExtractFilePath(ModuleFileName);
        if ModuleDir[Length(ModuleDir)] <> '\' then begin
                ModuleDir:=ModuleDir + '\';
        end;

        DataDir:=ModuleDir + 'data\';

        if not DirectoryExists(DataDir) then begin
                CreateDir(DataDir);
        end;

        if DirectoryExists(DataDir) then begin
                Op:=LowerCase(Request.QueryFields.Values['op']);
                if Op = 'getlist' then begin
                        Response.Content:=OpGetList(AUrl,DataDir);
                end
                else if Op = 'newlog' then begin
                        Response.Content:=OpNewLog(Request,DataDir);
                end
                else if Op = 'getfiledate' then begin
                        Response.Content:=OpGetFileDate(Request,DataDir);
                end
                else begin
                        Response.Content:='RES_ERR_INVALID_OP';
                end;
        end
        else begin
                Response.Content:='RES_ERR_NO_DATADIR';
        end;
        Response.SendResponse;
end;

initialization

CriticalSection:=TCriticalSection.Create;

finalization

CriticalSection.Free;

end.
