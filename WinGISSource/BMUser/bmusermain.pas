unit bmusermain;

interface

uses
  Windows, SysUtils, Classes, HTTPApp, DB, ADODB, ActiveX, Math;

type
  TVEUserWebModule = class(TWebModule)
    procedure VEUserWebModuleWebActionItemAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
  private
    Procedure DoLog(AUserId,ACount: Integer; AWingisVersion,AHardwareID: AnsiString; AConnectionString: AnsiString);
  end;

var
  VEUserWebModule: TVEUserWebModule;

implementation

const ConnectionStringTemplate = 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=%1;Persist Security Info=False';

{$R *.DFM}

procedure TVEUserWebModule.VEUserWebModuleWebActionItemAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
var
Buffer: Array[0..255] of Char;
UserTable: TADOTable;
DBFileName: AnsiString;
Op: AnsiString;
Username: AnsiString;
Password: AnsiString;
Transactions: Integer;
WingisVersion: AnsiString;
HardwareID: AnsiString;
RecUpdated: Boolean;
Count: Integer;
UserId: Integer;
BingMapsKey: AnsiString;
Cmd: AnsiString;
UntilDate: AnsiString;
HasOp: Boolean;
HasUserName: Boolean;
HasPassword: Boolean;
HasCount: Boolean;
HasBingMapsKey: Boolean;
HasCmd: Boolean;
HasUntilDate: Boolean;
s: AnsiString;
i: Integer;
begin
        ZeroMemory(@Buffer,Sizeof(Buffer));
        GetModuleFileName(hInstance,Buffer,Sizeof(Buffer));
        DBFileName:=StrPas(Buffer);
        DBFileName:=StringReplace(DBFileName,'\\?\','',[]);
        DBFileName:=ChangeFileExt(DBFileName,'.mdb');

        Response.ContentType:='text/plain';
        Response.Content:='RES_UNKNOWN';

        if Pos('{',Request.QueryFields.Text) > 0 then begin
                Response.Content:='RES_ERROR_PARAM';
                exit;
        end;

        Op:='';
        HasOp:=False;
        Username:='';
        HasUsername:=False;
        Password:='';
        HasPassword:=False;
        Count:=0;
        HasCount:=False;
        BingMapsKey:='';
        HasBingMapsKey:=False;
        Cmd:='';
        HasCmd:=False;
        UntilDate:='';
        HasUntilDate:=False;
        WingisVersion:='';
        HardwareID:='';

        if Request.QueryFields.IndexOfName('op') > -1 then begin
                Op:=Trim(Request.QueryFields.Values['op']);
                HasOp:=True;
        end;
        if Request.QueryFields.IndexOfName('user') > -1 then begin
                Username:=Trim(Request.QueryFields.Values['user']);
                HasUsername:=True;
        end;
        if Request.QueryFields.IndexOfName('pwd') > -1 then begin
                Password:=Trim(Request.QueryFields.Values['pwd']);
                HasPassword:=True;
        end;
        if Request.QueryFields.IndexOfName('n') > -1 then begin
                Count:=StrToIntDef(Request.QueryFields.Values['n'],0);
                HasCount:=True;
        end;
        if Request.QueryFields.IndexOfName('bmk') > -1 then begin
                BingMapsKey:=Trim(Request.QueryFields.Values['bmk']);
                HasBingMapsKey:=True;
        end;
        if Request.QueryFields.IndexOfName('cmd') > -1 then begin
                Cmd:=Trim(Request.QueryFields.Values['cmd']);
                HasCmd:=True;
        end;
        if Request.QueryFields.IndexOfName('until') > -1 then begin
                UntilDate:=Trim(Request.QueryFields.Values['until']);
                HasUntilDate:=True;
                if UntilDate = '0' then begin
                        UntilDate:='';
                end;
        end;
        if Request.QueryFields.IndexOfName('v') > -1 then begin
                WingisVersion:=Trim(Request.QueryFields.Values['v']);
        end;
        if Request.QueryFields.IndexOfName('hid') > -1 then begin
                HardwareID:=Trim(Request.QueryFields.Values['hid']);
        end;

        CoInitialize(nil);

        UserTable:=TADOTable.Create(Self);
        UserTable.TableDirect:=True;
        UserTable.ConnectionString:=StringReplace(ConnectionStringTemplate,'%1',DBFileName,[]);
        UserTable.TableName:='USER';
        try
                UserTable.Open;
                if UserTable.Active then begin
                        if (HasOp) and (HasUsername) and (Username <> '') then begin
                                if UserTable.Locate('User',Username,[loCaseInsensitive]) then begin
                                        if Op = 'get' then begin
                                                s:='';
                                                for i:=0 to UserTable.Fields.Count-1 do begin
                                                        s:=s + UserTable.Fields[i].AsString;
                                                        if i < UserTable.Fields.Count-1 then begin
                                                                s:=s + '|';
                                                        end;
                                                end;
                                                Response.Content:=s;
                                        end
                                        else if (Op = 'login') and (HasPassword) and (Password <> '') then begin
                                                if UserTable.FieldByName('Password').AsString = Password then begin
                                                        Transactions:=UserTable.FieldByName('Transactions').AsInteger;
                                                        if Transactions <> 0 then begin
                                                                Response.Content:='RES_OK'
                                                                + '|' + UserTable.FieldByName('Transactions').AsString
                                                                + '|' + IntToStr(Trunc(UserTable.FieldByName('Until').AsFloat))
                                                                + '|' + Trim(UserTable.FieldByName('BingMapsKey').AsString)
                                                                + '|' + Trim(UserTable.FieldByName('Cmd').AsString);
                                                        end
                                                        else begin
                                                                Response.Content:='RES_ZERO';
                                                        end;
                                                end
                                                else begin
                                                        Response.Content:='RES_ERROR_USER';
                                                end;
                                        end
                                        else if Op = 'count' then begin
                                                RecUpdated:=True;
                                                UserId:=UserTable.FieldByName('UserID').AsInteger;
                                                Transactions:=UserTable.FieldByName('Transactions').AsInteger;
                                                if Transactions > 0 then begin
                                                        Dec(Transactions,Count);
                                                        Transactions:=Max(Transactions,0);
                                                        try
                                                                UserTable.Edit;
                                                                UserTable.FieldByName('Transactions').AsInteger:=Transactions;
                                                                UserTable.Post;
                                                        except
                                                                on E: Exception do begin
                                                                        RecUpdated:=False;
                                                                        Response.Content:='RES_ERROR_UPDATE|' + E.Message;
                                                                end;
                                                        end;
                                                end;
                                                if RecUpdated then begin
                                                        if Transactions <> 0 then begin
                                                                Response.Content:='RES_OK|' + UserTable.FieldByName('Transactions').AsString;
                                                        end
                                                        else begin
                                                                Response.Content:='RES_ZERO';
                                                        end;
                                                end;
                                                try
                                                        DoLog(UserId,Count,WingisVersion,HardwareID,UserTable.ConnectionString);
                                                except
                                                        on E: Exception do begin
                                                                Response.Content:=Response.Content + '|' + E.Message;
                                                        end;
                                                end;
                                        end
                                        else if Op = 'upd' then begin
                                                UserTable.Edit;
                                                if (HasPassword) and (Password <> '') then begin
                                                        UserTable.FieldByName('Password').AsString:=Password;
                                                end;
                                                if HasCount then begin
                                                        Transactions:=UserTable.FieldByName('Transactions').AsInteger;
                                                        if Transactions >= 0 then begin
                                                                if Count = -1 then begin
                                                                        Transactions:=-1;
                                                                end
                                                                else begin
                                                                        Transactions:=Transactions + Count;
                                                                end;
                                                        end
                                                        else begin
                                                                Transactions:=Count;
                                                        end;
                                                        if Transactions < -1 then begin
                                                                Transactions:=0;
                                                        end;
                                                        UserTable.FieldByName('Transactions').AsInteger:=Transactions;
                                                end;
                                                if HasBingMapsKey then begin
                                                        UserTable.FieldByName('BingMapsKey').AsString:=BingMapsKey;
                                                end;
                                                if HasCmd then begin
                                                        UserTable.FieldByName('Cmd').AsString:=Cmd;
                                                end;
                                                if HasUntilDate then begin
                                                        UserTable.FieldByName('Until').AsString:=UntilDate;
                                                end;
                                                UserTable.Post;
                                                Response.Content:='RES_OK'
                                                + '|' + UserTable.FieldByName('User').AsString
                                                + '|' + UserTable.FieldByName('Password').AsString
                                                + '|' + UserTable.FieldByName('Transactions').AsString
                                                + '|' + UserTable.FieldByName('Until').AsString
                                                + '|' + Trim(UserTable.FieldByName('BingMapsKey').AsString)
                                                + '|' + Trim(UserTable.FieldByName('Cmd').AsString);
                                        end
                                        else begin
                                                Response.Content:='RES_ERROR_PARAM';
                                        end;
                                end
                                else if (Op = 'upd') and (Username <> '') and (Password <> '') then begin
                                                UserTable.Append;
                                                UserTable.FieldByName('User').AsString:=Username;
                                                UserTable.FieldByName('Password').AsString:=Password;
                                                UserTable.FieldByName('Transactions').AsInteger:=Count;
                                                UserTable.FieldByName('Until').AsString:=UntilDate;
                                                UserTable.FieldByName('BingMapsKey').AsString:=BingMapsKey;
                                                UserTable.FieldByName('Cmd').AsString:=Cmd;
                                                UserTable.Post;
                                                Response.Content:='RES_OK'
                                                + '|' + UserTable.FieldByName('User').AsString
                                                + '|' + UserTable.FieldByName('Password').AsString
                                                + '|' + UserTable.FieldByName('Transactions').AsString
                                                + '|' + UserTable.FieldByName('Until').AsString
                                                + '|' + Trim(UserTable.FieldByName('BingMapsKey').AsString)
                                                + '|' + Trim(UserTable.FieldByName('Cmd').AsString);
                                end
                                else if (Op = 'login') then begin
                                        Response.Content:='RES_ERROR_USER';
                                end
                                else begin
                                        Response.Content:='RES_ERROR_PARAM';
                                end;
                        end
                        else begin
                                Response.Content:='RES_ERROR_PARAM';
                        end;
                end
                else begin
                        Response.Content:='RES_ERROR_TABLE|' + UserTable.ConnectionString;
                end;
        finally
                if UserTable.Active then begin
                        UserTable.Close;
                end;
                UserTable.Free;
                CoUninitialize;
        end;
end;

procedure TVEUserWebModule.DoLog(AUserId,ACount: Integer; AWingisVersion,AHardwareID: AnsiString; AConnectionString: AnsiString);
var
LogTable: TADOTable;
ADate: AnsiString;
RecFound: Boolean;
ATransactions: Integer;
begin
        LogTable:=TADOTable.Create(Self);
        LogTable.TableDirect:=True;
        LogTable.ConnectionString:=AConnectionString;
        LogTable.TableName:='LOG';
        try
                LogTable.Open;
                ADate:=FormatDateTime('yyyy-mm-dd',Now);
                LogTable.Filter:='UserID=' + IntToStr(AUserId) + ' AND SessionDate=' + '''' + ADate + '''';
                try
                        RecFound:=LogTable.FindFirst;
                except
                        RecFound:=False;
                end;
                if RecFound then begin
                        ATransactions:=LogTable.FieldByName('Transactions').AsInteger;
                        Inc(ATransactions,ACount);
                        LogTable.Edit;
                        LogTable.FieldByName('Transactions').AsInteger:=ATransactions;
                        LogTable.FieldByName('Version').AsString:=AWingisVersion;
                        LogTable.FieldByName('HardwareID').AsString:=AHardwareID;
                        LogTable.Post;
                end
                else begin
                        LogTable.Append;
                        LogTable.FieldByName('UserID').AsInteger:=AUserId;
                        LogTable.FieldByName('SessionDate').AsString:=ADate;
                        LogTable.FieldByName('Transactions').AsInteger:=ACount;
                        LogTable.FieldByName('Version').AsString:=AWingisVersion;
                        LogTable.FieldByName('HardwareID').AsString:=AHardwareID;
                        LogTable.Post;
                end;
        finally
                if LogTable.Active then begin
                        LogTable.Close;
                end;
                LogTable.Free;
        end;
end;

end.
