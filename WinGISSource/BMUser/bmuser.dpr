library bmuser;

uses
  WebBroker,
  ISAPIApp,
  bmusermain in 'bmusermain.pas' {VEUserWebModule: TWebModule};

{$R *.RES}

exports
  GetExtensionVersion,
  HttpExtensionProc,
  TerminateExtension;

begin
  Application.Initialize;
  Application.CreateForm(TVEUserWebModule, VEUserWebModule);
  Application.Run;
end.
