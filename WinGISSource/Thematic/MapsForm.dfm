object MapsDlg: TMapsDlg
  Left = 408
  Top = 219
  BorderStyle = bsDialog
  Caption = 'MapsDlg'
  ClientHeight = 200
  ClientWidth = 257
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object ButtonOk: TButton
    Left = 32
    Top = 168
    Width = 75
    Height = 25
    Caption = 'ButtonOk'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 0
  end
  object ButtonCancel: TButton
    Left = 152
    Top = 168
    Width = 75
    Height = 25
    Caption = 'ButtonCancel'
    ModalResult = 2
    TabOrder = 1
  end
  object LbMaps: TListBox
    Left = 8
    Top = 8
    Width = 241
    Height = 153
    ExtendedSelect = False
    ItemHeight = 13
    TabOrder = 2
    OnClick = LbMapsClick
    OnDblClick = LbMapsDblClick
  end
end
