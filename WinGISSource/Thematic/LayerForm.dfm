object LayersDlg: TLayersDlg
  Left = 348
  Top = 200
  BorderStyle = bsDialog
  Caption = 'LayersDlg'
  ClientHeight = 124
  ClientWidth = 264
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object LabelLayer: TLabel
    Left = 8
    Top = 8
    Width = 52
    Height = 13
    Caption = 'LabelLayer'
  end
  object ComboBoxLayers: TComboBox
    Left = 8
    Top = 32
    Width = 249
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    Text = 'ComboBoxLayers'
  end
  object ButtonOk: TButton
    Left = 48
    Top = 88
    Width = 75
    Height = 25
    Caption = 'ButtonOk'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object ButtonCancel: TButton
    Left = 144
    Top = 88
    Width = 75
    Height = 25
    Caption = 'ButtonCancel'
    ModalResult = 2
    TabOrder = 2
  end
  object CheckBoxSepLayers: TCheckBox
    Left = 8
    Top = 64
    Width = 121
    Height = 17
    Caption = 'CheckBoxSepLayers'
    TabOrder = 3
  end
  object CheckBoxLegendOnOff: TCheckBox
    Left = 136
    Top = 64
    Width = 121
    Height = 17
    Caption = 'CheckBoxLegendOnOff'
    TabOrder = 4
  end
end
