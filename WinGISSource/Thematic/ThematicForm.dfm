object ThematicMainForm: TThematicMainForm
  Left = 260
  Top = 164
  BorderStyle = bsDialog
  ClientHeight = 454
  ClientWidth = 499
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDefault
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnMouseMove = PageControlMouseMove
  OnShow = FormShow
  DesignSize = (
    499
    454)
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButtonSaveSchema: TSpeedButton
    Left = 8
    Top = 355
    Width = 20
    Height = 20
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888880000000000000880330000008803088033000000880308803300000088
      0308803300000000030880333333333333088033000000003308803088888888
      0308803088888888030880308888888803088030888888880308803088888888
      0008803088888888080880000000000000088888888888888888}
    ParentShowHint = False
    ShowHint = True
    Spacing = 0
    OnClick = SpeedButtonSaveSchemaClick
  end
  object SpeedButtonDeleteSchema: TSpeedButton
    Left = 32
    Top = 355
    Width = 20
    Height = 20
    Enabled = False
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888888888888888888888808888888888088800088888888888880000888888
      8088888000888888088888880008888008888888800088008888888888000008
      8888888888800088888888888800000888888888800088008888888000088880
      0888880000888888008888000888888888088888888888888888}
    ParentShowHint = False
    ShowHint = True
    Spacing = 0
    OnClick = SpeedButtonDeleteSchemaClick
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 435
    Width = 499
    Height = 19
    Panels = <
      item
        Alignment = taCenter
        Width = 200
      end
      item
        Alignment = taCenter
        Width = 50
      end>
    SimplePanel = False
  end
  object GroupBoxAutoColor: TGroupBox
    Left = 184
    Top = 8
    Width = 121
    Height = 113
    Caption = 'GroupBoxAutoColor'
    TabOrder = 2
    object LabelFillColor: TLabel
      Left = 8
      Top = 20
      Width = 62
      Height = 13
      Caption = 'LabelFillColor'
    end
    object LabelLineColor: TLabel
      Left = 8
      Top = 56
      Width = 70
      Height = 13
      Caption = 'LabelLineColor'
    end
    object PanelFillColorStart: TPanel
      Left = 8
      Top = 36
      Width = 17
      Height = 17
      BevelOuter = bvLowered
      TabOrder = 0
      OnClick = PanelFillColorStartClick
    end
    object PanelFillColorEnd: TPanel
      Left = 24
      Top = 36
      Width = 17
      Height = 17
      BevelOuter = bvLowered
      TabOrder = 1
      OnClick = PanelFillColorEndClick
    end
    object CheckBoxAutoFillColor: TCheckBox
      Left = 48
      Top = 36
      Width = 65
      Height = 17
      TabStop = False
      Caption = 'CheckBoxAutoFillColor'
      TabOrder = 2
      OnClick = CheckBoxAutoFillColorClick
    end
    object PanelLineColorStart: TPanel
      Left = 8
      Top = 72
      Width = 17
      Height = 17
      BevelOuter = bvLowered
      TabOrder = 3
      OnClick = PanelLineColorStartClick
    end
    object PanelLineColorEnd: TPanel
      Left = 24
      Top = 72
      Width = 17
      Height = 17
      BevelOuter = bvLowered
      TabOrder = 4
      OnClick = PanelLineColorEndClick
    end
    object CheckBoxAutoLineColor: TCheckBox
      Left = 48
      Top = 72
      Width = 65
      Height = 17
      TabStop = False
      Caption = 'CheckBoxAutoLineColor'
      TabOrder = 5
      OnClick = CheckBoxAutoLineColorClick
    end
    object CheckBoxNoColors: TCheckBox
      Left = 8
      Top = 92
      Width = 105
      Height = 17
      Caption = 'CheckBoxNoColors'
      TabOrder = 6
      OnClick = CheckBoxNoColorsClick
    end
  end
  object GroupBoxGrouping: TGroupBox
    Left = 312
    Top = 8
    Width = 177
    Height = 113
    Caption = 'GroupBoxGrouping'
    TabOrder = 4
    object LabelGrouping: TLabel
      Left = 8
      Top = 24
      Width = 69
      Height = 13
      Caption = 'LabelGrouping'
    end
    object LabelGroups: TLabel
      Left = 104
      Top = 64
      Width = 60
      Height = 13
      Caption = 'LabelGroups'
    end
    object LabelRounding: TLabel
      Left = 9
      Top = 64
      Width = 72
      Height = 13
      Caption = 'LabelRounding'
    end
    object SpeedButtonSaveGrouping: TSpeedButton
      Left = 8
      Top = 40
      Width = 20
      Height = 20
      Enabled = False
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888880000000000000880330000008803088033000000880308803300000088
        0308803300000000030880333333333333088033000000003308803088888888
        0308803088888888030880308888888803088030888888880308803088888888
        0008803088888888080880000000000000088888888888888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = SpeedButtonSaveGroupingClick
    end
    object SpeedButtonDeleteGrouping: TSpeedButton
      Left = 32
      Top = 40
      Width = 20
      Height = 20
      Enabled = False
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888808888888888088800088888888888880000888888
        8088888000888888088888880008888008888888800088008888888888000008
        8888888888800088888888888800000888888888800088008888888000088880
        0888880000888888008888000888888888088888888888888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = SpeedButtonDeleteGroupingClick
    end
    object ComboBoxMethod: TComboBox
      Left = 56
      Top = 40
      Width = 113
      Height = 21
      Style = csDropDownList
      DropDownCount = 15
      Enabled = False
      ItemHeight = 13
      TabOrder = 0
      TabStop = False
      OnChange = ComboBoxMethodChange
      Items.Strings = (
        '0'
        '1'
        '2'
        '3')
    end
    object SpinEditGroups: TSpinEdit
      Left = 104
      Top = 80
      Width = 65
      Height = 22
      TabStop = False
      MaxValue = 10000
      MinValue = 1
      TabOrder = 1
      Value = 9
      OnChange = SpinEditGroupsChange
    end
    object ComboBoxRounding: TComboBox
      Left = 8
      Top = 80
      Width = 65
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      TabStop = False
      OnChange = ComboBoxRoundingChange
      Items.Strings = (
        '1000'
        '100'
        '10'
        '1'
        '.1'
        '.01'
        '.001'
        '.0001')
    end
  end
  object GroupBoxFields: TGroupBox
    Left = 8
    Top = 8
    Width = 169
    Height = 113
    Caption = 'GroupBoxFields'
    TabOrder = 5
    object LabelFields: TLabel
      Left = 8
      Top = 24
      Width = 53
      Height = 13
      Caption = 'LabelFields'
    end
    object ComboBoxFields: TComboBox
      Left = 8
      Top = 40
      Width = 153
      Height = 21
      Style = csDropDownList
      DropDownCount = 25
      ItemHeight = 13
      TabOrder = 0
      TabStop = False
      OnChange = ComboBoxFieldsChange
    end
    object CheckBoxNumericOnly: TCheckBox
      Left = 8
      Top = 72
      Width = 153
      Height = 17
      TabStop = False
      Caption = 'CheckBoxNumericOnly'
      TabOrder = 1
      OnClick = CheckBoxNumericOnlyClick
    end
  end
  object PageControl: TPageControl
    Left = 8
    Top = 128
    Width = 481
    Height = 217
    ActivePage = TabSheetGroups
    MultiLine = True
    TabIndex = 0
    TabOrder = 6
    TabStop = False
    OnChange = PageControlChange
    OnMouseMove = PageControlMouseMove
    object TabSheetGroups: TTabSheet
      Caption = 'TabSheetGroups'
      OnMouseMove = PageControlMouseMove
      object Grid: TStringGrid
        Left = 8
        Top = 24
        Width = 457
        Height = 156
        TabStop = False
        ColCount = 6
        DefaultColWidth = 73
        DefaultRowHeight = 16
        FixedCols = 0
        RowCount = 9
        FixedRows = 0
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing]
        TabOrder = 0
        OnDrawCell = GridDrawCell
        OnMouseMove = GridMouseMove
        OnMouseUp = GridMouseUp
        OnSelectCell = GridSelectCell
      end
      object HeaderControl: THeaderControl
        Left = 8
        Top = 6
        Width = 457
        Height = 20
        Align = alNone
        DragReorder = False
        Sections = <
          item
            ImageIndex = -1
            MinWidth = 60
            Text = 'Fillstyle'
            Width = 76
          end
          item
            ImageIndex = -1
            MinWidth = 60
            Text = 'Linestyle'
            Width = 74
          end
          item
            AllowClick = False
            ImageIndex = -1
            Text = 'From'
            Width = 74
          end
          item
            AllowClick = False
            ImageIndex = -1
            Text = 'To'
            Width = 74
          end
          item
            AllowClick = False
            ImageIndex = -1
            Text = 'Count'
            Width = 74
          end
          item
            AllowClick = False
            ImageIndex = -1
            MinWidth = 30
            Text = 'Active'
            Width = 74
          end>
        OnSectionClick = HeaderControlSectionClick
        OnSectionResize = HeaderControlSectionResize
        OnMouseMove = PageControlMouseMove
      end
    end
    object TabSheetStatistics: TTabSheet
      Caption = 'TabSheetStatistics'
      ImageIndex = 1
      object GroupBoxStatistics: TGroupBox
        Left = 8
        Top = 8
        Width = 169
        Height = 169
        TabOrder = 0
        object LabelMax: TLabel
          Left = 8
          Top = 56
          Width = 46
          Height = 13
          Caption = 'LabelMax'
        end
        object LabelMin: TLabel
          Left = 8
          Top = 16
          Width = 43
          Height = 13
          Caption = 'LabelMin'
        end
        object LabelAvg: TLabel
          Left = 8
          Top = 96
          Width = 45
          Height = 13
          Caption = 'LabelAvg'
        end
        object LabelMinValue: TLabel
          Left = 153
          Top = 16
          Width = 3
          Height = 13
          Alignment = taRightJustify
          Caption = '-'
        end
        object LabelMaxValue: TLabel
          Left = 153
          Top = 56
          Width = 3
          Height = 13
          Alignment = taRightJustify
          Caption = '-'
        end
        object LabelAvgValue: TLabel
          Left = 153
          Top = 96
          Width = 3
          Height = 13
          Alignment = taRightJustify
          Caption = '-'
        end
        object LabelMedian: TLabel
          Left = 8
          Top = 136
          Width = 61
          Height = 13
          Caption = 'LabelMedian'
        end
        object LabelMedianValue: TLabel
          Left = 153
          Top = 136
          Width = 3
          Height = 13
          Alignment = taRightJustify
          Caption = '-'
        end
      end
      object GroupBoxGraph: TGroupBox
        Left = 184
        Top = 8
        Width = 281
        Height = 169
        TabOrder = 1
        object PanelGraph: TPanel
          Left = 8
          Top = 16
          Width = 265
          Height = 121
          BevelOuter = bvLowered
          Color = clWhite
          TabOrder = 0
          DesignSize = (
            265
            121)
          object PaintBox: TPaintBox
            Left = 8
            Top = 8
            Width = 249
            Height = 105
            Anchors = [akLeft, akTop, akRight, akBottom]
            OnPaint = PaintBoxPaint
          end
        end
        object CheckBoxLogar: TCheckBox
          Left = 8
          Top = 144
          Width = 153
          Height = 17
          TabStop = False
          Caption = 'CheckBoxLogar'
          TabOrder = 1
          OnClick = CheckBoxLogarClick
        end
      end
    end
  end
  object ComboBoxFillStyle: TComboBox
    Left = 312
    Top = 128
    Width = 33
    Height = 20
    Style = csOwnerDrawFixed
    DropDownCount = 20
    ItemHeight = 14
    TabOrder = 7
    TabStop = False
    Visible = False
    OnChange = ComboBoxFillStyleChange
    OnDrawItem = ComboBoxFillStyleDrawItem
  end
  object ComboBoxLineStyle: TComboBox
    Left = 368
    Top = 128
    Width = 33
    Height = 20
    Style = csOwnerDrawFixed
    DropDownCount = 20
    ItemHeight = 14
    TabOrder = 9
    TabStop = False
    Visible = False
    OnChange = ComboBoxLineStyleChange
    OnDrawItem = ComboBoxLineStyleDrawItem
  end
  object ButtonColor: TButton
    Left = 418
    Top = 130
    Width = 16
    Height = 16
    Caption = 'C'
    TabOrder = 8
    TabStop = False
    Visible = False
    OnClick = ButtonColorClick
  end
  object MaskEdit: TMaskEdit
    Left = 344
    Top = 128
    Width = 25
    Height = 21
    TabStop = False
    TabOrder = 10
    Visible = False
    OnKeyDown = MaskEditKeyDown
    OnKeyUp = MaskEditKeyUp
  end
  object ProgressBar: TProgressBar
    Left = 0
    Top = 417
    Width = 499
    Height = 18
    Align = alBottom
    Min = 0
    Max = 100
    TabOrder = 12
  end
  object ComboBoxSchemas: TComboBox
    Left = 56
    Top = 355
    Width = 113
    Height = 21
    Style = csDropDownList
    DropDownCount = 15
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    Sorted = True
    TabOrder = 14
    OnChange = ComboBoxSchemasChange
  end
  object SpinButtonLineWidth: TSpinButton
    Left = 400
    Top = 128
    Width = 15
    Height = 18
    DownGlyph.Data = {
      BA000000424DBA00000000000000420000002800000009000000060000000100
      1000030000007800000000000000000000000000000000000000007C0000E003
      00001F0000000042004200420042004200420042004200420000004200420042
      0042000000420042004200420000004200420042000000000000004200420042
      0000004200420000000000000000000000420042000000420000000000000000
      000000000000004200000042004200420042004200420042004200420000}
    TabOrder = 15
    UpGlyph.Data = {
      BA000000424DBA00000000000000420000002800000009000000060000000100
      1000030000007800000000000000000000000000000000000000007C0000E003
      00001F0000000042004200420042004200420042004200420000004200000000
      0000000000000000000000420000004200420000000000000000000000420042
      0000004200420042000000000000004200420042000000420042004200420000
      004200420042004200000042004200420042004200420042004200420000}
    Visible = False
    OnDownClick = SpinButtonLineWidthDownClick
    OnUpClick = SpinButtonLineWidthUpClick
  end
  object CheckBoxFillStyle: TCheckBox
    Left = 316
    Top = 132
    Width = 12
    Height = 12
    Checked = True
    ParentShowHint = False
    ShowHint = True
    State = cbChecked
    TabOrder = 16
    Visible = False
    OnClick = CheckBoxFillStyleClick
  end
  object CheckBoxLineStyle: TCheckBox
    Left = 372
    Top = 132
    Width = 12
    Height = 12
    Checked = True
    ParentShowHint = False
    ShowHint = True
    State = cbChecked
    TabOrder = 17
    Visible = False
    OnClick = CheckBoxLineStyleClick
  end
  object CheckBoxUseSchema: TCheckBox
    Left = 176
    Top = 356
    Width = 17
    Height = 17
    TabOrder = 18
    OnClick = CheckBoxUseSchemaClick
  end
  object ButtonCreate: TButton
    Left = 8
    Top = 384
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'ButtonCreate'
    Enabled = False
    TabOrder = 1
    TabStop = False
    OnClick = ButtonCreateClick
  end
  object ButtonSave: TButton
    Left = 88
    Top = 384
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'ButtonSave'
    Enabled = False
    TabOrder = 19
    OnClick = ButtonSaveClick
  end
  object ButtonReset: TButton
    Left = 168
    Top = 384
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'ButtonReset'
    TabOrder = 11
    TabStop = False
    OnClick = ButtonResetClick
  end
  object ButtonStop: TButton
    Left = 248
    Top = 384
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'ButtonStop'
    Enabled = False
    ParentShowHint = False
    ShowHint = False
    TabOrder = 13
    TabStop = False
    OnClick = ButtonStopClick
  end
  object ButtonClose: TButton
    Left = 416
    Top = 384
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'ButtonClose'
    TabOrder = 3
    TabStop = False
    OnClick = ButtonCloseClick
  end
  object ADODataSet: TADODataSet
    AutoCalcFields = False
    Connection = ADOConnection
    CursorType = ctOpenForwardOnly
    LockType = ltReadOnly
    MarshalOptions = moMarshalModifiedOnly
    MaxRecords = 100
    ParamCheck = False
    Parameters = <>
    Left = 64
    Top = 200
  end
  object ColorDialog: TColorDialog
    Ctl3D = True
    Options = [cdFullOpen]
    Left = 472
    Top = 120
  end
  object FontDialog: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MinFontSize = 0
    MaxFontSize = 0
    Left = 476
    Top = 144
  end
  object ADOConnection: TADOConnection
    LoginPrompt = False
    Left = 32
    Top = 200
  end
end
