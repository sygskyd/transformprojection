library Thematic;

uses
  Forms,
  Windows,
  SysUtils,
  Classes,
  Math,
  WinOSInfo,
  Wingis_TLB,
  ThematicForm in 'ThematicForm.pas' {ThematicMainForm};

{$R *.RES}

function Create (Handle: Integer; Doc: Variant; Lng: PChar; Connection: PChar; Command: PChar; ProgisIDField: PChar; ThematicField: PChar; Grouping: Integer; GroupCount: Integer; GroupingName: PChar; SchemaName: PChar; DestLayerName: PChar; AllowDialog: Boolean; MaxTMClasses: Integer; AutoClose: Boolean): Integer; stdcall;
var
   i: Integer;
   Disp: IDispatch;
begin
   Result := 0;
   if MaxTMClasses < 10 then begin
        MaxTMCLasses:=10;
   end;
   if MaxTMClasses > 1000 then begin
        MaxTMClasses:=1000;
   end;
   ThematicForm.MaxGroups:=MaxTMClasses;
   if (ThematicMainForm = nil) or (not IsWindowVisible (ThematicMainForm.Handle)) then
   begin
      Application.Handle := Handle;
      if ThematicMainForm = nil then
      begin
         ThematicMainForm := TThematicMainForm.Create (Application);
      end;

      if ThematicMainForm <> nil then with ThematicMainForm do begin
         AutoCloseAfterCreate:=AutoClose;

         Disp:=Doc;
         Document := Disp as IDocument;
         Application.ProcessMessages;

         ProgisIdFieldName := ProgisIdField;

         if ADODataset.Active then begin
                ADODataset.Close;
         end;
         ADODataSet.CommandText := Command;

         if ADOConnection.Connected then begin
                ADOConnection.Close;
         end;
         ADOConnection.ConnectionString := Connection;

         if (Connection <> nil) and (Trim(Connection) <> '') and (Command <> nil) and (Trim (Command) <> '') and (RefreshDS) then
         begin
            ProgisIdFieldIndex := -1;
            for i := 0 to ADODataset.Fields.Count - 1 do
            begin
               if Lowercase (ADODataset.Fields[i].FieldName) = Lowercase (ProgisIdFieldName) then
               begin
                  ProgisIdFieldIndex := i;
               end;
            end;
            if ProgisIdFieldIndex >= 0 then
            begin
               try
                  LangList := TStringList.Create;
                  LangList.LoadFromFile (ChangeFileExt(ThematicMainForm.ModuleFileName,'.' + Lng));
                  SetCompactMode (False);
                  CompactMode := not AllowDialog;
                  Show;
                  Application.ProcessMessages;
                  if (GroupCount > 0) then
                  begin
                     SpinEditGroups.Value := Min (GroupCount, MAXGROUPS);
                  end;
                  ComboBoxFields.ItemIndex := ComboBoxFields.Items.IndexOf (ThematicField);
                  if ComboBoxFields.ItemIndex <> -1 then
                  begin
                     ComboBoxFieldsChange (nil);
                     if (Trim (GroupingName) = '') and (IsNumeric) then
                     begin
                        ComboBoxMethod.ItemIndex := Grouping;
                        if ComboBoxMethod.ItemIndex <> -1 then
                        begin
                           ComboBoxMethodChange (nil);
                        end;
                     end;
                  end;
                  if (IsNumeric) and (Trim (GroupingName) <> '') then
                  begin
                     SetGrouping (Trim (GroupingName));
                  end;
                  if Trim (SchemaName) <> '' then
                  begin
                     ComboBoxSchemas.ItemIndex := ComboBoxSchemas.Items.IndexOf (Trim (SchemaName));
                     ComboBoxSchemasChange (nil);
                  end;
                  DLayerName := Trim (DestLayerName);
                  if (not AllowDialog) and (DLayerName <> '') and (ButtonCreate.Enabled) then
                  begin
                     ButtonCreateClick (nil);
                     if Visible then begin
                        Close;
                     end;
                  end
                  else
                     if (not AllowDialog) then
                     begin
                        SetCompactMode (False);
                     end;
               except
                  Result := 4;
               end;
            end
            else
            begin
               Result := 3;
            end;
         end
         else
         begin
            Result := 2;
         end;
      end;
   end
   else
   begin
      Result := 1;
   end;
end;

//******************************************************************************

function Apply (Handle: Integer; Doc: Variant; Lng: PChar; MaxTMClasses: Integer): Integer; stdcall;
var
i: Integer;
Disp: IDispatch;
begin
   Result := 0;

   if MaxTMClasses < 10 then begin
        MaxTMCLasses:=10;
   end;
   if MaxTMClasses > 1000 then begin
        MaxTMClasses:=1000;
   end;

   ThematicForm.MaxGroups:=MaxTMClasses;

   if (ThematicMainForm = nil) or (not IsWindowVisible (ThematicMainForm.Handle)) then begin
      Application.Handle := Handle;
      if ThematicMainForm = nil then
      begin
         ThematicMainForm := TThematicMainForm.Create (Application);
      end;

      if ThematicMainForm <> nil then with ThematicMainForm do begin
         AutoCloseAfterCreate:=True;

         Disp:=Doc;
         Document := Disp as IDocument;

         //ProgisIdFieldName := ProgisIdField;

         if ADODataset.Active then begin
                ADODataset.Close;
         end;
         //ADODataSet.CommandText := Command;

         if ADOConnection.Connected then begin
                ADOConnection.Close;
         end;
         //ADOConnection.ConnectionString := Connection;

//         if (Connection <> nil) and (Trim(Connection) <> '') and (Command <> nil) and (Trim (Command) <> '') and (RefreshDS) then begin
            ProgisIdFieldIndex := -1;
            for i := 0 to ADODataset.Fields.Count - 1 do
            begin
               if Lowercase (ADODataset.Fields[i].FieldName) = Lowercase (ProgisIdFieldName) then
               begin
                  ProgisIdFieldIndex := i;
               end;
            end;
            //if ProgisIdFieldIndex >= 0 then begin
               try
                  LangList := TStringList.Create;
                  LangList.LoadFromFile (ChangeFileExt(ThematicMainForm.ModuleFileName,'.' + Lng));
                  SetCompactMode (False);
                  CompactMode := True;

                  AutoCloseAfterCreate:=False;

                  if not DoApplyMap then begin
                        Result:=3;
                  end;

                  {
                  if (GroupCount > 0) then
                  begin
                     SpinEditGroups.Value := Min (GroupCount, MAXGROUPS);
                  end;
                  }
                  //ComboBoxFields.ItemIndex := ComboBoxFields.Items.IndexOf (ThematicField);
                  {
                  if ComboBoxFields.ItemIndex <> -1 then
                  begin
                     ComboBoxFieldsChange (nil);
                     if (Trim (GroupingName) = '') and (IsNumeric) then begin
                        ComboBoxMethod.ItemIndex := Grouping;
                        if ComboBoxMethod.ItemIndex <> -1 then
                        begin
                           ComboBoxMethodChange (nil);
                        end;
                     end;
                  end;
                  }
                  {
                  if (IsNumeric) and (Trim (GroupingName) <> '') then
                  begin
                     SetGrouping (Trim (GroupingName));
                  end;
                  }
                  {
                  if Trim (SchemaName) <> '' then
                  begin
                     ComboBoxSchemas.ItemIndex := ComboBoxSchemas.Items.IndexOf (Trim (SchemaName));
                     ComboBoxSchemasChange (nil);
                  end;
                  }
                  //DLayerName := Trim (DestLayerName);
               except
                  Result := 2;
               end;
            //end
         end
         else
         begin
            Result := 1;
         end;
      end;
end;

exports Create,Apply;

begin
end.

