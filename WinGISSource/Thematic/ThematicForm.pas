unit ThematicForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, StdCtrls, Grids, DBGrids, ComCtrls, Spin, ExtCtrls,
  Math, Menus, Mask, Buttons, Wingis_TLB, LayerForm, MapsForm, WinOSInfo;

const
  MAXCOLORS          = 9;
  GRPFILEEXT         = '.tgm';
  SCHFILEEXT         = '.tsc';
  TMFILEEXT          = '.tms';
  MAXLINEWIDTH       = 100;
  MINLINEWIDTH       = 0;
  LASTFILLSTYLE      = 7;
  LASTLINESTYLE      = 4;
  THEMDIR            = 'Thematics\';   

const
  fiFill    = 0;
  fiLine    = 1;
  fiStart   = 2;
  fiEnd     = 3;
  fiCount   = 4;
  fiActive  = 5;

  miCustom          = 0;
  miEqualInterval   = 1;
  miQuantile        = 2;
  miClassification  = 3;
  miSaved           = 4;

const
  GisFillStyles: Array[0..7] of Integer = (7,0,5,6,1,4,2,3);

const
  DefaultPalette: Array[0..MAXCOLORS-1] of Integer = (
      clRed,
      clYellow,
      clLime,
      clAqua,
      clFuchsia,
      clTeal,
      clSilver,
      clGreen,
      clBlue);

type TGroup = record
     Active: Boolean;
     StartVal: Double;
     EndVal: Double;
     Cls: String;
     Count: Integer;
     FillColor: TColor;
     FillStyle: Integer;
     LineColor: TColor;
     LineStyle: Integer;
     LineWidth: Integer;
end;

type PEntry = ^TEntry;
     TEntry = record
     Id: Integer;
     Text: String;
     Value: Double;
     Group: Integer;
     Next: PEntry;
end;

type
  TThematicMainForm = class(TForm)
    ADODataSet: TADODataSet;
    StatusBar: TStatusBar;
    ColorDialog: TColorDialog;
    ButtonCreate: TButton;
    GroupBoxAutoColor: TGroupBox;
    PanelFillColorStart: TPanel;
    PanelFillColorEnd: TPanel;
    CheckBoxAutoFillColor: TCheckBox;
    PanelLineColorStart: TPanel;
    PanelLineColorEnd: TPanel;
    CheckBoxAutoLineColor: TCheckBox;
    LabelFillColor: TLabel;
    LabelLineColor: TLabel;
    ButtonClose: TButton;
    GroupBoxGrouping: TGroupBox;
    LabelGrouping: TLabel;
    ComboBoxMethod: TComboBox;
    LabelGroups: TLabel;
    SpinEditGroups: TSpinEdit;
    LabelRounding: TLabel;
    ComboBoxRounding: TComboBox;
    GroupBoxFields: TGroupBox;
    LabelFields: TLabel;
    ComboBoxFields: TComboBox;
    CheckBoxNumericOnly: TCheckBox;
    PageControl: TPageControl;
    TabSheetGroups: TTabSheet;
    TabSheetStatistics: TTabSheet;
    Grid: TStringGrid;
    HeaderControl: THeaderControl;
    GroupBoxStatistics: TGroupBox;
    LabelMax: TLabel;
    LabelMin: TLabel;
    LabelAvg: TLabel;
    LabelMinValue: TLabel;
    LabelMaxValue: TLabel;
    LabelAvgValue: TLabel;
    ComboBoxFillStyle: TComboBox;
    ButtonColor: TButton;
    ComboBoxLineStyle: TComboBox;
    GroupBoxGraph: TGroupBox;
    PanelGraph: TPanel;
    PaintBox: TPaintBox;
    CheckBoxLogar: TCheckBox;
    MaskEdit: TMaskEdit;
    ButtonReset: TButton;
    ProgressBar: TProgressBar;
    ButtonStop: TButton;
    SpeedButtonSaveSchema: TSpeedButton;
    ComboBoxSchemas: TComboBox;
    SpeedButtonDeleteSchema: TSpeedButton;
    LabelMedian: TLabel;
    LabelMedianValue: TLabel;
    SpeedButtonSaveGrouping: TSpeedButton;
    SpeedButtonDeleteGrouping: TSpeedButton;
    SpinButtonLineWidth: TSpinButton;
    CheckBoxFillStyle: TCheckBox;
    CheckBoxLineStyle: TCheckBox;
    FontDialog: TFontDialog;
    CheckBoxNoColors: TCheckBox;
    ADOConnection: TADOConnection;
    CheckBoxUseSchema: TCheckBox;
    ButtonSave: TButton;
    procedure ComboBoxFieldsChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ComboBoxMethodChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpinEditGroupsChange(Sender: TObject);
    procedure GridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure ComboBoxRoundingChange(Sender: TObject);
    procedure ButtonCreateClick(Sender: TObject);
    procedure HeaderControlSectionResize(HeaderControl: THeaderControl;
      Section: THeaderSection);
    procedure PanelFillColorStartClick(Sender: TObject);
    procedure PanelFillColorEndClick(Sender: TObject);
    procedure CheckBoxAutoFillColorClick(Sender: TObject);
    procedure CheckBoxAutoLineColorClick(Sender: TObject);
    procedure PanelLineColorStartClick(Sender: TObject);
    procedure PanelLineColorEndClick(Sender: TObject);
    procedure ButtonCloseClick(Sender: TObject);
    procedure CheckBoxNumericOnlyClick(Sender: TObject);
    procedure GridMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ComboBoxFillStyleDrawItem(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure PageControlMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ComboBoxFillStyleChange(Sender: TObject);
    procedure ButtonColorClick(Sender: TObject);
    procedure ComboBoxLineStyleChange(Sender: TObject);
    procedure ComboBoxLineStyleDrawItem(Control: TWinControl;
      Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure PaintBoxPaint(Sender: TObject);
    procedure CheckBoxLogarClick(Sender: TObject);
    procedure GridMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure GridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure MaskEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PageControlChange(Sender: TObject);
    procedure MaskEditKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ButtonResetClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ButtonStopClick(Sender: TObject);
    procedure SpeedButtonSaveSchemaClick(Sender: TObject);
    procedure SpeedButtonDeleteSchemaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure HeaderControlSectionClick(HeaderControl: THeaderControl;
      Section: THeaderSection);
    procedure SpeedButtonSaveGroupingClick(Sender: TObject);
    procedure SpeedButtonDeleteGroupingClick(Sender: TObject);
    procedure SpinButtonLineWidthDownClick(Sender: TObject);
    procedure SpinButtonLineWidthUpClick(Sender: TObject);
    procedure CheckBoxFillStyleClick(Sender: TObject);
    procedure CheckBoxLineStyleClick(Sender: TObject);
    procedure CheckBoxNoColorsClick(Sender: TObject);
    procedure CheckBoxUseSchemaClick(Sender: TObject);
    procedure ButtonSaveClick(Sender: TObject);
    procedure ButtonTestClick(Sender: TObject);
    procedure ComboBoxSchemasChange(Sender: TObject);
  public
    LangList: TStringList;
    ProgisIdFieldName: String;
    ProgisIdFieldIndex: Integer;
    ThematicFieldIndex: Integer;
    IsNumeric: Boolean;
    MaxValue: Double;
    MinValue: Double;
    StartColor: Integer;
    EndColor: Integer;
    StyleMode: Integer;
    StyleGroup: Integer;
    EditGroup: Integer;
    Modified: Boolean;
    AllowEditing: Boolean;
    Stopped: Boolean;
    OrigGroupCount: Integer;
    FirstEntry: PEntry;
    CurEntry: PEntry;
    EntryCount: Integer;
    DLayerName: String;
    CompactMode: Boolean;
    Groups: Array of TGroup;
    StdFillColors: Array of Integer;
    StdLineColors: Array of Integer;
    AutoCloseAfterCreate: Boolean;
    ModuleFileName: AnsiString;
    procedure WndProc(var Msg: TMessage); override;
    procedure DoException(Sender: TObject; E: Exception);
    function LangText(i: Integer): String;
    function CalculateEqualInterval: Boolean;
    function CalculateQuantile: Boolean;
    function CalculateCls: Boolean;
    function GetColor(var Color: TColor): Boolean;
    function CalculateColorSteps(Index: Integer): Boolean;
    function IsNumField(Field: TField): Boolean;
    function RoundVal(Val: Double): Double;
    function FillFields(OnlyNum: Boolean): Boolean;
    function ResetColors: Boolean;
    function ResetFillStyles: Boolean;
    function ResetLineStyles: Boolean;
    function SetModified: Boolean;
    function ResetGridSelection: Boolean;
    function AssignValue(ACol,ARow: Integer; Text: String): Boolean;
    function SetInputMask(ACol,ARow: Integer): Boolean;
    function GetCounts: Boolean;
    function SetEditPos(ACol,ARow: Integer): Boolean;
    function SetGridLayout(Layout: Integer): Boolean;
    function AnalyseValues: Boolean;
    function RefreshDS: Boolean;
    function ResetThematicList: Boolean;
    function LoadSchemas: Boolean;
    function LoadTMFiles: Boolean;
    function AssignPalette: Boolean;
    function LoadGroupings: Boolean;
    function SetGrouping(GroupingName: String): Boolean;
    function SetCompactMode(IsOn: Boolean): Boolean;
    function GetLineStyles: Integer;
    function GetFillStyles: Integer;
    procedure SaveGroupsToTxt;
    function DoApplyMap: Boolean;
    function LoadSchema(SchemaName: String): Boolean;
    function DoCreateLegend(AName: String): Boolean;
  end;

var
  ThematicMainForm: TThematicMainForm = nil;
  MaxGroups: Integer;
  Document: IDocument;

implementation

{$R *.DFM}

procedure TThematicMainForm.WndProc(var Msg: TMessage);
begin
     inherited;
     case Msg.Msg of
          WM_NCLBUTTONDBLCLK:
          begin
               SetCompactMode(not CompactMode);
          end;
     end;
end;

procedure TThematicMainForm.DoException(Sender: TObject; E: Exception);
begin
        ShowMessage(E.Message);
end;

procedure TThematicMainForm.FormCreate(Sender: TObject);
var
ABuffer: Array[0..1023] of Char;

begin
     Scaleby(Trunc(9600000/(PixelsPerInch)),100000);

     ZeroMemory(@ABuffer,Sizeof(ABuffer));
     GetModuleFileName(hInstance,ABuffer,Sizeof(ABuffer));
     ModuleFileName:=StrPas(ABuffer);

     SetLength(Groups,MaxGroups);
     SetLength(StdFillColors,MaxGroups);
     SetLength(StdLineColors,MaxGroups);

     Application.OnException:=DoException;
end;

procedure TThematicMainForm.FormShow(Sender: TObject);
begin
     ThematicFieldIndex:=-1;
     IsNumeric:=False;
     StyleMode:=-1;
     StyleGroup:=-1;
     AllowEditing:=False;
     Stopped:=False;
     OrigGroupCount:=-1;
     DLayerName:='';

     FirstEntry:=nil;
     CurEntry:=nil;
     EntryCount:=0;

     Left:=(Screen.Width div 2) - (Width div 2);
     Top:=(Screen.Height div 2) - (Height div 2);
     SetCompactMode(CompactMode);

     ComboBoxMethod.Enabled:=True;
     ComboBoxRounding.Enabled:=True;
     SpinEditGroups.Enabled:=True;

     PageControl.ActivePage:=TabSheetGroups;

     AssignPalette;

     Caption:=LangText(0);
     LabelMin.Caption:=LangText(2);
     LabelMax.Caption:=LangText(3);
     LabelGroups.Caption:=LangText(4);
     ButtonCreate.Caption:=LangText(5);
     ButtonClose.Caption:=LangText(6);
     ButtonStop.Caption:=LangText(39);

     ComboBoxMethod.Items[miCustom]:=LangText(7);
     ComboBoxMethod.Items[miEqualInterval]:=LangText(8);
     ComboBoxMethod.Items[miQuantile]:=LangText(9);
     ComboBoxMethod.Items[miClassification]:=LangText(36);

     GroupBoxAutoColor.Caption:=LangText(13);
     LabelFillColor.Caption:=LangText(14);
     LabelLineColor.Caption:=LangText(15);

     HeaderControl.Sections[fiFill].Text:=LangText(16);
     HeaderControl.Sections[fiLine].Text:=LangText(17);
     HeaderControl.Sections[fiStart].Text:=LangText(18);
     HeaderControl.Sections[fiEnd].Text:=LangText(19);
     HeaderControl.Sections[fiCount].Text:=LangText(20);
     HeaderControl.Sections[fiActive].Text:=LangText(21);

     LabelRounding.Caption:=LangText(22);
     LabelFields.Caption:=LangText(23);
     LabelGrouping.Caption:=LangText(24);
     GroupBoxGrouping.Caption:=LangText(25);
     CheckBoxAutoFillColor.Caption:=LangText(26);
     CheckBoxAutoLineColor.Caption:=LangText(26);
     GroupBoxFields.Caption:=LangText(27);
     CheckBoxNumericOnly.Caption:=LangText(28);
     TabSheetGroups.Caption:=LangText(29);
     TabSheetStatistics.Caption:=LangText(30);
     LabelAvg.Caption:=LangText(31);
     LabelMedian.Caption:=LangText(47);
     CheckBoxLogar.Caption:=LangText(32);
     ButtonReset.Caption:=LangText(33);
     SpeedButtonSaveSchema.Hint:=LangText(43);
     ComboBoxSchemas.Hint:=LangText(44);
     SpeedButtonDeleteSchema.Hint:=LangText(45);
     SpeedButtonSaveGrouping.Hint:=LangText(51);
     SpeedButtonDeleteGrouping.Hint:=LangText(52);
     CheckBoxFillStyle.Hint:=LangText(60);
     CheckBoxLineStyle.Hint:=LangText(60);
     CheckBoxNoColors.Caption:=LangText(65);

     ButtonSave.Caption:=LangText(68);

     SpinEditGroups.Value:=9;
     SpinEditGroups.MaxValue:=MAXGROUPS;

     GetCounts;

     FillFields(CheckBoxNumericOnly.Checked);

     ComboBoxRounding.ItemIndex:=5;

     ResetColors;
     ResetFillStyles;
     ResetLineStyles;

     PanelFillColorStart.Color:=Groups[0].FillColor;
     PanelFillColorEnd.Color:=Groups[Grid.RowCount-1].FillColor;
     PanelLineColorStart.Color:=Groups[0].LineColor;
     PanelLineColorEnd.Color:=Groups[Grid.RowCount-1].LineColor;

     CalculateColorSteps(0);
     CalculateColorSteps(1);

     ComboBoxMethod.ItemIndex:=miCustom;
     ResetGridSelection;

     if not DirectoryExists(OSInfo.DocDataDir + THEMDIR) then begin
        CreateDir(OSInfo.DocDataDir + THEMDIR);
     end;

     LoadSchemas;
     LoadGroupings;

     ButtonResetClick(nil);

     GetFillStyles;
     GetLineStyles;
end;

procedure TThematicMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     Stopped:=True;
end;

procedure TThematicMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     LangList.Free;
     ResetThematicList;
     if ADODataset.Active then begin
        ADODataSet.Close;
        ADOConnection.Close;
     end;
     Document:=nil;
     Application.ProcessMessages;
end;

function TThematicMainForm.AssignPalette: Boolean;
var
i,Page: Integer;
R,G,B: Integer;
begin
     for i:=0 to MAXGROUPS-1 do begin
        StdFillColors[i]:=DefaultPalette[i mod MAXCOLORS];
        Page:=(i div MAXCOLORS)+1;
        if Page > 1 then begin
           R:=GetRValue(StdFillColors[i]);
           G:=GetGValue(StdFillColors[i]);
           B:=GetBValue(StdFillColors[i]);
           R:=R - (R*Page*10) div 100;
           G:=G - (G*Page*10) div 100;
           B:=B - (B*Page*10) div 100;
           StdFillColors[i]:=RGB(R,G,B);
        end;
        StdLineColors[i]:=clBlack;
     end;
     Result:=True;
end;

function TThematicMainForm.ResetThematicList: Boolean;
begin
     CurEntry:=FirstEntry;
     try
        while CurEntry <> nil do begin
                FirstEntry:=CurEntry^.Next;
                Dispose(CurEntry);
                CurEntry:=FirstEntry;
        end;
     finally
        CurEntry:=nil;                           
        FirstEntry:=nil;
        EntryCount:=0;
        Result:=True;
     end;
end;

function TThematicMainForm.LangText(i: Integer): String;
begin
     Result:=LangList.Values[IntToStr(i)];
     if Result = '' then begin
        Result:='Unknown' + IntToStr(i);
     end;
end;

procedure TThematicMainForm.ButtonResetClick(Sender: TObject);
var
i: Integer;
begin
     for i:=0 to MAXGROUPS-1 do begin
         Groups[i].Active:=False;
         Groups[i].StartVal:=0;
         Grid.Cells[fiStart,i]:='';
         Groups[i].EndVal:=0;
         Grid.Cells[fiEnd,i]:='';
         Groups[i].Count:=0;
         Grid.Cells[fiCount,i]:='';
     end;
     ComboBoxMethod.ItemIndex:=miCustom;
     Grid.Repaint;
     SetModified;
end;

function TThematicMainForm.ResetColors: Boolean;
var
i: Integer;
begin
     for i:=0 to MAXGROUPS-1 do begin
         if not CheckBoxAutoFillColor.Checked then begin
            Groups[i].FillColor:=StdFillColors[i];
         end;
         if not CheckBoxAutoLineColor.Checked then begin
            Groups[i].LineColor:=StdLineColors[i];
         end;
     end;
     Result:=True;
end;

function TThematicMainForm.ResetFillStyles: Boolean;
var
i: Integer;
begin
     for i:=0 to MAXGROUPS-1 do begin
         Groups[i].FillStyle:=Integer(bsSolid);
     end;
     Result:=True;
end;

function TThematicMainForm.ResetLineStyles: Boolean;
var
i: Integer;
begin
     for i:=0 to MAXGROUPS-1 do begin
         Groups[i].LineStyle:=Integer(psSolid);
         Groups[i].LineWidth:=0;
     end;
     Result:=True;
end;

procedure TThematicMainForm.ComboBoxFieldsChange(Sender: TObject);
begin
     if ComboBoxFields.ItemIndex > -1 then begin
        StatusBar.Panels[1].Text:=LangText(40);
        Application.ProcessMessages;
        ThematicFieldIndex:=ADODataset.FieldByName(ComboBoxFields.Items[ComboBoxFields.ItemIndex]).Index;
        if not RefreshDS then begin
           exit;
        end;
     end
     else begin
        ThematicFieldIndex:=-1;
     end;
     IsNumeric:=(ThematicFieldIndex > -1) and (IsNumField(ADODataset.Fields[ThematicFieldIndex]));
     if ThematicFieldIndex <> -1 then begin
        ButtonResetClick(nil);
        AnalyseValues;
        if IsNumeric then begin
           ComboBoxMethod.Enabled:=True;
        end
        else begin
           ComboBoxMethod.ItemIndex:=miClassification;
           ComboBoxMethod.Enabled:=False;
        end;
        ComboBoxMethodChange(nil);
     end;
     if (not IsNumeric) or (ThematicFieldIndex = ProgisIdFieldIndex) then begin
        LabelMinValue.Caption:='-';
        LabelMaxValue.Caption:='-';
        LabelAvgValue.Caption:='-';
        LabelMedianValue.Caption:='-';
        StatusBar.Panels[1].Text:='';
     end;
     if ComboBoxSchemas.ItemIndex <> -1 then begin
        ComboBoxSchemasChange(Self);
     end;
     SetModified;
end;

procedure TThematicMainForm.SpinEditGroupsChange(Sender: TObject);
begin
     if SpinEditGroups.Value > Grid.RowCount then begin
        if CheckBoxAutoFillColor.Checked then begin
           Groups[Grid.RowCount].FillColor:=Groups[Grid.RowCount-1].FillColor;
        end;
        if CheckBoxAutoLineColor.Checked then begin
           Groups[Grid.RowCount].LineColor:=Groups[Grid.RowCount-1].LineColor;
        end;
     end
     else if SpinEditGroups.Value < Grid.RowCount then begin
        if CheckBoxAutoFillColor.Checked then begin
           Groups[Grid.RowCount-2].FillColor:=Groups[Grid.RowCount-1].FillColor;
        end;
        if CheckBoxAutoLineColor.Checked then begin
           Groups[Grid.RowCount-2].LineColor:=Groups[Grid.RowCount-1].LineColor;
        end;
     end;
     Grid.RowCount:=Max(Min(SpinEditGroups.Value,MAXGROUPS),1);

     CalculateColorSteps(0);
     CalculateColorSteps(1);

     if ComboBoxMethod.ItemIndex <> miClassification then begin
        ComboBoxMethodChange(nil);
     end;

     Grid.Repaint;
     SetModified;
end;

procedure TThematicMainForm.ComboBoxMethodChange(Sender: TObject);
begin
     if (EntryCount > 0) and (FirstEntry <> nil) then begin
        SpinEditGroups.MaxValue:=MAXGROUPS;
        SpinEditGroups.Value:=Min(SpinEditGroups.Value,SpinEditGroups.MaxValue);
        AllowEditing:=False;
        SetGridLayout(ComboBoxMethod.ItemIndex);
        if IsNumeric then begin
           SpinEditGroups.Enabled:=True;
           ComboBoxRounding.Enabled:=False;
           if (OrigGroupCount <> -1) and (OrigGroupCount <> SpinEditGroups.Value) and (ComboBoxMethod.ItemIndex <> miClassification) then begin
              SpinEditGroups.Value:=OrigGroupCount;
              OrigGroupCount:=-1;
           end;
           case ComboBoxMethod.ItemIndex of
              miCustom: AllowEditing:=True;
              miEqualInterval: CalculateEqualInterval;
              miQuantile: CalculateQuantile;
           end;
           if ComboBoxMethod.ItemIndex >= miSaved then begin
              SetGrouping(ComboBoxMethod.Text);
              AllowEditing:=True;
           end;
           Grid.Repaint;
        end;
        if (ComboBoxFields.ItemIndex <> -1) and (ComboBoxMethod.ItemIndex = miClassification) then begin
           CalculateCls;
           AllowEditing:=True;
        end;
        SetModified;
     end;
end;

function TThematicMainForm.SetGridLayout(Layout: Integer): Boolean;
begin
     with HeaderControl do begin
          if Layout = miClassification then begin
             Sections[fiStart].Text:=LangText(37);
             Sections[fiStart].Width:=Sections[fiStart].Width + Sections[fiEnd].Width;
             Sections[fiEnd].MaxWidth:=0;
          end
          else begin
             Sections[fiStart].Text:=LangText(18);
             Sections[fiEnd].MaxWidth:=10000;
             if Sections[fiEnd].Width = 0 then begin
                Sections[fiStart].Width:=Sections[fiStart].Width div 2;
                Sections[fiEnd].Width:=Sections[fiStart].Width;
             end;
          end;
     end;
     HeaderControlSectionResize(HeaderControl,HeaderControl.Sections[fiStart]);
     HeaderControlSectionResize(HeaderControl,HeaderControl.Sections[fiEnd]);
     Result:=True;
end;

function TThematicMainForm.AssignValue(ACol, ARow: Integer;
  Text: String): Boolean;
var
i: Integer;

     function Convert(s: String): Double;
     begin
          Result:=StrToFloat(s);
     end;

     function Recalculate: Boolean;
     var
     i,k: Integer;
     CurValue: Double;
     begin
          for i:=0 to Grid.RowCount-1 do begin
              Groups[i].Count:=0;
          end;
          CurEntry:=FirstEntry;
          while CurEntry <> nil do begin
              for k:=0 to Grid.RowCount-1 do begin
                  CurValue:=CurEntry^.Value;
                  if (CurValue >= Groups[k].StartVal) and (CurValue <= Groups[k].EndVal) then begin
                     CurEntry^.Group:=k;
                     Inc(Groups[k].Count);
                     break;
                  end;
              end;
              CurEntry:=CurEntry^.Next;
          end;
          for i:=0 to Grid.RowCount-1 do begin
              Grid.Cells[fiStart,i]:=FloatToStr(Groups[i].StartVal);
              Grid.Cells[fiEnd,i]:=FloatToStr(Groups[i].EndVal);
              Grid.Cells[fiCount,i]:=IntToStr(Groups[i].Count);
              Groups[i].Active:=(Groups[i].Count <> 0);
          end;
          Grid.Repaint;
          SetModified;
          Result:=True;
     end;

     function RecalculateCls: Boolean;
     var
     i,k: Integer;
     CurValue: String;
     begin
          for i:=0 to Grid.RowCount-1 do begin
              Groups[i].Count:=0;
          end;
          CurEntry:=FirstEntry;
          while CurEntry <> nil do begin
              for k:=0 to Grid.RowCount-1 do begin
                  CurValue:=CurEntry^.Text;
                  if (CurValue = Groups[k].Cls) or ((Pos(';',Groups[k].Cls) > 0) and (Pos(CurValue,Groups[k].Cls) >0))  then begin
                     CurEntry^.Group:=k;
                     Inc(Groups[k].Count);
                     break;
                  end;
              end;
              CurEntry:=CurEntry^.Next;
          end;
          for i:=0 to Grid.RowCount-1 do begin
              Grid.Cells[fiStart,i]:=Groups[i].Cls;
              Grid.Cells[fiCount,i]:=IntToStr(Groups[i].Count);
              Groups[i].Active:=(Groups[i].Count <> 0) and (Groups[i].Cls <> '');
          end;
          Grid.Repaint;
          SetModified;
          Result:=True;
     end;

begin
     Result:=False;
     Text:=Trim(Text);
     if ComboBoxMethod.ItemIndex = miClassification then begin
        if Text <> Grid.Cells[ACol,ARow] then begin
           Groups[ARow].Cls:=Text;
           RecalculateCls;
        end;
     end
        else begin
        if (Text = '') then begin
           Text:='0';
        end;
        if Text <> Grid.Cells[ACol,ARow] then begin
           if ACol = fiStart then begin
              Groups[ARow].StartVal:=Convert(Text);
              if ARow > 0 then begin
                 Groups[ARow-1].EndVal:=Groups[ARow].StartVal;
              end;
           end
           else if ACol = fiEnd then begin
              Groups[ARow].EndVal:=Convert(Text);
              if ARow < Grid.RowCount-1 then begin
                 Groups[ARow+1].StartVal:=Groups[ARow].EndVal;
              end;
           end;

           for i:=ARow to Grid.RowCount-1 do begin
               Groups[i+1].StartVal:=Groups[i].EndVal;
               if Groups[i+1].StartVal > Groups[i+1].EndVal then begin
                  Groups[i+1].EndVal:=Groups[i+1].StartVal;
               end
               else begin
                  break;
               end;
           end;

           for i:=ARow downto 1 do begin
               Groups[i-1].EndVal:=Groups[i].StartVal;
               if Groups[i-1].EndVal < Groups[i-1].StartVal then begin
                  Groups[i-1].StartVal:=Groups[i-1].EndVal;
               end
               else begin
                  break;
               end;
           end;

           Recalculate;
           Result:=True;
        end;
     end;
end;

function TThematicMainForm.CalculateCls: Boolean;
var
i: Integer;
LastVal,CurVal: String;
CurGroup: Integer;
begin
     CurGroup:=0;
     Groups[0].Count:=0;
     CurEntry:=FirstEntry;
     LastVal:=CurEntry^.Text;
     while CurEntry <> nil do begin
         CurVal:=CurEntry^.Text;
         if CurGroup < MAXGROUPS then begin
            Groups[CurGroup].Cls:=LastVal;
         end
         else begin
            MessageBox(Handle,PChar(Format(LangText(38),[MAXGROUPS])),PChar(Caption),MB_OK or MB_ICONINFORMATION or MB_APPLMODAL);
            break;
         end;
         if Uppercase(CurVal) = Uppercase(LastVal) then begin
            Inc(Groups[CurGroup].Count);
         end
         else begin
            LastVal:=CurVal;
            Inc(CurGroup);
            if CurGroup < MAXGROUPS then begin
                Groups[CurGroup].Count:=1;
                Groups[CurGroup].Cls:=LastVal;
            end;
         end;
         CurEntry^.Group:=CurGroup;
         CurEntry:=CurEntry^.Next;
     end;
     if EntryCount <= CurGroup+1 then begin
        Groups[CurGroup].Cls:=CurVal;
     end;
     if OrigGroupCount = -1 then begin
         OrigGroupCount:=SpinEditGroups.Value;
     end;
     SpinEditGroups.Enabled:=True;
     SpinEditGroups.Value:=Min(CurGroup+1,MAXGROUPS);
     ComboBoxRounding.Enabled:=False;
     for i:=0 to Grid.RowCount-1 do begin
         Grid.Cells[fiStart,i]:=Groups[i].Cls;
         Grid.Cells[fiCount,i]:=IntToStr(Groups[i].Count);
         Groups[i].Active:=Trim(Groups[i].Cls) <> '';
     end;
     Grid.Repaint;
     Result:=True;
end;

function TThematicMainForm.CalculateEqualInterval: Boolean;
var
i,k: Integer;
CurValue,Step: Double;
begin
     ComboBoxRounding.Enabled:=True;
     Step:=(MaxValue - MinValue) / Grid.RowCount;
     CurValue:=MinValue;
     for i:=0 to Grid.RowCount-1 do begin
         Groups[i].Count:=0;
         Groups[i].StartVal:=RoundVal(CurValue);
         CurValue:=CurValue + Step;
         Groups[i].EndVal:=RoundVal(CurValue);
     end;
     CurEntry:=FirstEntry;
     while CurEntry <> nil do begin
         for k:=0 to Grid.RowCount-1 do begin
             CurValue:=CurEntry^.Value;
             if (CurValue >= Groups[k].StartVal) and (CurValue <= Groups[k].EndVal) then begin
                CurEntry^.Group:=k;
                Inc(Groups[k].Count);
                break;
             end;
         end;
         CurEntry:=CurEntry^.Next;
     end;
     for i:=0 to Grid.RowCount-1 do begin
         Grid.Cells[fiStart,i]:=FloatToStr(Groups[i].StartVal);
         Grid.Cells[fiEnd,i]:=FloatToStr(Groups[i].EndVal);
         Grid.Cells[fiCount,i]:=IntToStr(Groups[i].Count);
         Groups[i].Active:=(Groups[i].Count <> 0);
     end;
     Grid.Repaint;
     Result:=True;
end;

function TThematicMainForm.CalculateQuantile: Boolean;
var
i: Integer;
ItemsPerGroup: Integer;
CurGroup: Integer;
begin
     ItemsPerGroup:=(EntryCount div Grid.RowCount)+1;
     CurEntry:=FirstEntry;
     CurGroup:=0;
     Groups[CurGroup].Count:=0;
     Groups[CurGroup].StartVal:=CurEntry^.Value;
     i:=1;
     while CurEntry <> nil do begin
         CurEntry^.Group:=CurGroup;
         Inc(Groups[CurGroup].Count);
         Groups[CurGroup].EndVal:=CurEntry^.Value;
         if (i mod ItemsPerGroup) = 0 then begin
            Inc(CurGroup);
            Groups[CurGroup].Count:=0;
            Groups[CurGroup].StartVal:=CurEntry^.Value;
         end;
         CurEntry:=CurEntry^.Next;
         Inc(i);
     end;
     for i:=0 to Grid.RowCount-1 do begin
         Grid.Cells[fiStart,i]:=FloatToStr(Groups[i].StartVal);
         Grid.Cells[fiEnd,i]:=FloatToStr(Groups[i].EndVal);
         Grid.Cells[fiCount,i]:=IntToStr(Groups[i].Count);
         Groups[i].Active:=(Groups[i].Count <> 0);
     end;
     Grid.Repaint;
     Result:=True;
end;

function TThematicMainForm.GetColor(var Color: TColor): Boolean;
begin
     Result:=ColorDialog.Execute;
     if Result then begin
        Color:=ColorDialog.Color;
     end;
end;

procedure TThematicMainForm.GridDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
OldBrushStyle: TBrushStyle;
OldPenStyle: TPenStyle;
OldPenWidth: Integer;
Region: THandle;
ClipBox: TRect;
RealWidth: Double;
s: String;
x,y: Integer;
begin
     if ACol = fiFill then begin
        OldBrushStyle:=Grid.Canvas.Brush.Style;
        OldPenStyle:=Grid.Canvas.Pen.Style;
        if CheckBoxFillStyle.Checked then begin
           if Groups[ARow].FillStyle <= LASTFILLSTYLE then begin
              if CheckBoxNoColors.Checked then begin
                 Grid.Canvas.Brush.Color:=clBlack;
              end
              else begin
                 Grid.Canvas.Brush.Color:=Groups[ARow].FillColor;
              end;
              Grid.Canvas.Brush.Style:=TBrushStyle(Groups[ARow].FillStyle);
              Grid.Canvas.Pen.Style:=psClear;
              Grid.Canvas.Rectangle(Rect);
           end
           else begin
              Grid.Canvas.Font.Name:='Small Fonts';
              Grid.Canvas.Font.Size:=7;
              if CheckBoxNoColors.Checked then begin
                  Grid.Canvas.Font.Color:=clBlack;
              end
              else begin
                  Grid.Canvas.Font.Color:=Groups[ARow].FillColor;
              end;
              Grid.Canvas.TextOut(Rect.Left+2,Rect.Top+2,ComboBoxFillStyle.Items[Groups[ARow].FillStyle]);
           end;
        end
        else begin
           Grid.Canvas.Brush.Color:=clGray;
           Grid.Canvas.Brush.Style:=bsSolid;
           Grid.Canvas.Pen.Style:=psClear;
           Grid.Canvas.Rectangle(Rect);
        end;
        Grid.Canvas.Brush.Style:=OldBrushStyle;
        Grid.Canvas.Pen.Style:=OldPenStyle;
     end
     else if ACol = fiLine then begin
        if CheckBoxLineStyle.Checked then begin
           GetClipBox(Grid.Canvas.Handle,ClipBox);
           IntersectClipRect(Grid.Canvas.Handle,Rect.Left,Rect.Top,Rect.Right,Rect.Bottom);
           if Groups[ARow].LineStyle <= LASTLINESTYLE then begin
              OldPenStyle:=Grid.Canvas.Pen.Style;
              OldPenWidth:=Grid.Canvas.Pen.Width;
              Grid.Canvas.Pen.Style:=TPenStyle(Groups[ARow].LineStyle);
              if CheckBoxNoColors.Checked then begin
                  Grid.Canvas.Pen.Color:=clBlack;
              end
              else begin
                  Grid.Canvas.Pen.Color:=Groups[ARow].LineColor;
              end;
              Grid.Canvas.Pen.Width:=Min(Groups[ARow].LineWidth div 2,(Rect.Bottom-Rect.Top));;
              Grid.Canvas.MoveTo(Rect.Left,(Rect.Top + Rect.Bottom) div 2);
              Grid.Canvas.LineTo(Rect.Right,(Rect.Top + Rect.Bottom) div 2);
              Grid.Canvas.Pen.Style:=OldPenStyle;
              Grid.Canvas.Pen.Width:=OldPenWidth;
           end
           else begin
              Grid.Canvas.Font.Name:='Small Fonts';
              Grid.Canvas.Font.Size:=7;
              if CheckBoxNoColors.Checked then begin
                  Grid.Canvas.Font.Color:=clBlack;
              end
              else begin
                  Grid.Canvas.Font.Color:=Groups[ARow].LineColor;
              end;
              Grid.Canvas.TextOut(Rect.Left+2,Rect.Top+2,ComboBoxLineStyle.Items[Groups[ARow].LineStyle]);
           end;
           if Groups[ARow].LineWidth > 0 then begin
              RealWidth:=Groups[ARow].LineWidth;
              RealWidth:=RealWidth/10;
              s:=' ' + FloatToStr(RealWidth) + ' mm ';
              x:=Rect.Left+5;
              y:=(Rect.Top + Rect.Bottom) div 2;
              y:=y - Grid.Canvas.TextHeight(s) div 2;
              Grid.Canvas.Font.Name:='Small Fonts';
              Grid.Canvas.Font.Size:=7;
              Grid.Canvas.TextOut(x,y,s);
           end;
           Region:=CreateRectRgn(ClipBox.Left,ClipBox.Top,ClipBox.Right,ClipBox.Bottom);
           SelectClipRgn(Grid.Canvas.Handle,Region);
           DeleteObject(Region);
        end
        else begin
           OldBrushStyle:=Grid.Canvas.Brush.Style;
           OldPenStyle:=Grid.Canvas.Pen.Style;
           Grid.Canvas.Brush.Color:=clGray;
           Grid.Canvas.Brush.Style:=bsSolid;
           Grid.Canvas.Pen.Style:=psClear;
           Grid.Canvas.Rectangle(Rect);
           Grid.Canvas.Brush.Style:=OldBrushStyle;
           Grid.Canvas.Pen.Style:=OldPenStyle;
        end;
     end
     else if ACol = fiActive then begin
        Grid.Canvas.Pen.Color:=clBlack;
        if Groups[ARow].Active then begin
           Grid.Canvas.Brush.Color:=clLime;
        end
        else begin
           Grid.Canvas.Brush.Color:=clRed;
        end;
        Rect.Top:=Rect.Top + 2;
        Rect.Bottom:=Rect.Bottom - 2;
        Rect.Left:=((Rect.Left + Rect.Right) div 2) - ((Rect.Bottom - Rect.Top) div 2);
        Rect.Right:=Rect.Left + (Rect.Bottom - Rect.Top);
        Grid.Canvas.Ellipse(Rect);
     end;
end;

function TThematicMainForm.CalculateColorSteps(Index: Integer): Boolean;
var
i: Integer;
R,G,B: Integer;
REnd,GEnd,BEnd: Integer;
RStep,GStep,BStep: Integer;
begin
     if Index = 0 then begin
        R:=GetRValue(PanelFillColorStart.Color);
        G:=GetGValue(PanelFillColorStart.Color);
        B:=GetBValue(PanelFillColorStart.Color);

        REnd:=GetRValue(PanelFillColorEnd.Color);
        GEnd:=GetGValue(PanelFillColorEnd.Color);
        BEnd:=GetBValue(PanelFillColorEnd.Color);
     end
     else begin
        R:=GetRValue(PanelLineColorStart.Color);
        G:=GetGValue(PanelLineColorStart.Color);
        B:=GetBValue(PanelLineColorStart.Color);

        REnd:=GetRValue(PanelLineColorEnd.Color);
        GEnd:=GetGValue(PanelLineColorEnd.Color);
        BEnd:=GetBValue(PanelLineColorEnd.Color);
     end;

     RStep:=(REnd - R) div Max((Grid.RowCount-1),1);
     GStep:=(GEnd - G) div Max((Grid.RowCount-1),1);
     BStep:=(BEnd - B) div Max((Grid.RowCount-1),1);

     for i:=0 to Grid.RowCount-1 do begin
         if (Index = 0) and (CheckBoxAutoFillColor.Checked) then begin
            Groups[i].FillColor:=RGB(R,G,B);
         end
         else if (Index = 1) and (CheckBoxAutoLineColor.Checked) then begin
            Groups[i].LineColor:=RGB(R,G,B);
         end;
         R:=R + RStep;
         G:=G + GStep;
         B:=B + BStep;
     end;

     Result:=True;
end;

function TThematicMainForm.IsNumField(Field: TField): Boolean;
begin
     Result:=Field.DataType in [ftSmallint,ftInteger,ftWord,ftFloat,ftAutoInc,ftLargeint];
end;

function TThematicMainForm.RoundVal(Val: Double): Double;
begin
     case ComboBoxRounding.ItemIndex of
          0: Result:=Round(Val/1000)*1000;
          1: Result:=Round(Val/100)*100;
          2: Result:=Round(Val/10)*10;
          3: Result:=Round(Val);
          4: Result:=Round(Val*10)/10;
          5: Result:=Round(Val*100)/100;
          6: Result:=Round(Val*1000)/1000;
          7: Result:=Round(Val*10000)/10000;
          else Result:=Val;
     end;
end;

procedure TThematicMainForm.ComboBoxRoundingChange(Sender: TObject);
begin
     ComboBoxMethodChange(nil);
end;

procedure TThematicMainForm.ButtonCreateClick(Sender: TObject);
var
s,Layername: String;
DestLayer: ILayer;
i,d: Integer;
ObjDisp: IDispatch;
Obj: OleVariant;
ObjIndex: Integer;
GroupIndex: Integer;
UserStyleName: WideString;
UserStyle: Integer;
SeperateLayers: Boolean;
SeperateLayerName: WideString;
CreateLegend: Boolean;

     function LayerExists(s: String): Boolean;
     var
     Dispatch: IDispatch;
     begin
          Dispatch:=Document.Layers.GetLayerByName(s);
          Result:=(Dispatch <> nil);
     end;

     function GetLayerName: Boolean;
     var
     i: Integer;
     s: String;
     begin
          Result:=False;
          LayersDlg:=TLayersDlg.Create(Self);
          LayersDlg.Caption:=LangText(34);
          LayersDlg.LabelLayer.Caption:=LangText(35);
          LayersDlg.ButtonOk.Caption:=LangText(61);
          LayersDlg.ButtonCancel.Caption:=LangText(62);
          LayersDlg.CheckBoxSepLayers.Caption:=LangText(63);
          LayersDlg.CheckBoxLegendOnOff.Caption:=LangText(58);
          for i:=0 to Document.Layers.Count-1 do begin
              s:=Document.Layers.Items[i].Layername;
              LayersDlg.ComboBoxLayers.Items.Add(s);
          end;
          LayersDlg.ComboBoxLayers.Text:=Layername;
          if LayersDlg.ShowModal = mrOK then begin
             LayerName:=LayersDlg.ComboBoxLayers.Text;
             SeperateLayers:=LayersDlg.CheckBoxSepLayers.Checked;
             CreateLegend:=LayersDlg.CheckBoxLegendOnOff.Checked;
             Result:=True;
          end;
          LayersDlg.Free;
     end;

begin
  try
     SeperateLayers:=False;
     CreateLegend:=False;
     Stopped:=False;
     if DLayerName = '' then begin
        Layername:=ComboBoxFields.Items[ComboBoxFields.ItemIndex];
        i:=0;
        s:=Layername;
        while LayerExists(s) do begin
           Inc(i);
           s:=Layername + ' (' + IntToStr(i) + ')';
        end;
        Layername:=s;
     end
     else begin
        LayerName:=DLayerName;
     end;
     if (DLayerName <> '') or (GetLayerName) then begin
        Layername:=Trim(Layername);
        if Layername = '' then begin
           DestLayer:=Document.Layers.ActiveLayer;
           Layername:=DestLayer.Layername;
        end
        else if LayerExists(Layername) then begin
           if not SeperateLayers then begin
              DestLayer:=Document.Layers.GetLayerByName(Layername);
           end;
        end
        else begin
           if not SeperateLayers then begin
              DestLayer:=Document.Layers.InsertLayerByName(Layername,True);
           end;
        end;
        if LayerExists(Layername) or (SeperateLayers) then begin
           ProgressBar.Max:=EntryCount;
           d:=EntryCount div 100;
           d:=Max(d,1);
           CurEntry:=FirstEntry;
           i:=0;
           ButtonStop.Enabled:=True;
           StatusBar.Panels[1].Text:=LangText(59);

           //DestLayer.Clear;

           while CurEntry <> nil do begin
               Application.ProcessMessages;
               if Stopped then begin
                  ProgressBar.Position:=0;
                  ButtonStop.Enabled:=False;
                  exit;
               end;
               if (i mod d) = 0 then begin
                  ProgressBar.Position:=i;
               end;
               ObjIndex:=CurEntry^.Id;
               GroupIndex:=CurEntry^.Group;
               if (GroupIndex <> -1) and (Groups[GroupIndex].Active) then begin
                  if SeperateLayers then begin
                     if ComboBoxMethod.ItemIndex = miClassification then begin
                        SeperateLayerName:=Layername + ' ' + Groups[GroupIndex].Cls;
                     end
                     else begin
                        SeperateLayerName:=Layername + ' ' + Format('%.0f',[Groups[GroupIndex].StartVal]) + '-' + Format('%.0f',[Groups[GroupIndex].EndVal]);
                     end;
                     if LayerExists(SeperateLayername) then begin
                        DestLayer:=Document.Layers.GetLayerByName(SeperateLayername);
                     end
                     else begin
                          DestLayer:=Document.Layers.InsertLayerByName(SeperateLayername,True);
                     end;
                     if CheckBoxLineStyle.Checked then begin
                        if not CheckBoxNoColors.Checked then begin
                           DestLayer.LineStyle.Color:=Groups[GroupIndex].LineColor;
                        end;
                        if Groups[GroupIndex].LineStyle > LASTLINESTYLE then begin
                           if not CheckBoxNoColors.Checked then begin
                              DestLayer.LineStyle.FillColor:=Groups[GroupIndex].LineColor;
                           end;
                           UserStyleName:=ComboBoxLineStyle.Items[Groups[GroupIndex].LineStyle];
                           UserStyle:=Document.GetUserLineStyleNumberByName(UserStyleName);
                           DestLayer.LineStyle.Style:=UserStyle;
                        end
                        else begin
                           DestLayer.LineStyle.Style:=Groups[GroupIndex].LineStyle;
                        end;
                        DestLayer.LineStyle.WidthType:=1;
                        DestLayer.LineStyle.Width:=Integer(Groups[GroupIndex].LineWidth);
                     end;
                     if CheckBoxFillStyle.Checked then begin
                        if not CheckBoxNoColors.Checked then begin
                           DestLayer.FillStyle.ForeColor:=Groups[GroupIndex].FillColor;
                        end;
                        if Groups[GroupIndex].FillStyle > LASTFILLSTYLE then begin
                           UserStyleName:=ComboBoxFillStyle.Items[Groups[GroupIndex].FillStyle];
                           UserStyle:=Document.GetUserFillStyleNumberByName(UserStyleName);
                           DestLayer.FillStyle.Pattern:=UserStyle;
                        end
                        else begin
                           DestLayer.FillStyle.Pattern:=GisFillStyles[Groups[GroupIndex].FillStyle];
                        end;
                     end;
                  end;
                  ObjDisp:=Document.GetObjectByIndex(ObjIndex);
                  if ObjDisp <> nil then begin
                     Obj:=ObjDisp;
                     try
                        ObjDisp:=Obj.ObjectStyle;
                        if not SeperateLayers then begin
                           if (ObjDisp = nil) then begin
                                Obj.CreateObjectStyle;
                           end;
                           if CheckBoxLineStyle.Checked then begin
                              if not CheckBoxNoColors.Checked then begin
                                 Obj.ObjectStyle.LineStyle.Color:=Groups[GroupIndex].LineColor;
                              end;
                              if Groups[GroupIndex].LineStyle > LASTLINESTYLE then begin
                                 if not CheckBoxNoColors.Checked then begin
                                    Obj.ObjectStyle.LineStyle.FillColor:=Groups[GroupIndex].LineColor;
                                 end;
                                 UserStyleName:=ComboBoxLineStyle.Items[Groups[GroupIndex].LineStyle];
                                 UserStyle:=Document.GetUserLineStyleNumberByName(UserStyleName);
                                 Obj.ObjectStyle.LineStyle.Style:=UserStyle;
                              end
                              else begin
                                 Obj.ObjectStyle.LineStyle.Style:=Groups[GroupIndex].LineStyle;
                              end;
                              Obj.ObjectStyle.LineStyle.WidthType:=1;
                              Obj.ObjectStyle.LineStyle.Width:=Integer(Groups[GroupIndex].LineWidth);
                           end;
                           if CheckBoxFillStyle.Checked then begin
                              if not CheckBoxNoColors.Checked then begin
                                 Obj.ObjectStyle.FillStyle.ForeColor:=Groups[GroupIndex].FillColor;
                              end;
                              if Groups[GroupIndex].FillStyle > LASTFILLSTYLE then begin
                                 UserStyleName:=ComboBoxFillStyle.Items[Groups[GroupIndex].FillStyle];
                                 UserStyle:=Document.GetUserFillStyleNumberByName(UserStyleName);
                                 Obj.ObjectStyle.FillStyle.Pattern:=UserStyle;
                              end
                              else begin
                                 Obj.ObjectStyle.FillStyle.Pattern:=GisFillStyles[Groups[GroupIndex].FillStyle];
                              end;
                           end;
                        end
                        else begin
                                ObjDisp:=Obj.ObjectStyle;
                                if (ObjDisp <> nil) then begin
                                        Obj.ObjectStyle.FillStyle.Pattern:=-1;
                                        Obj.ObjectStyle.FillStyle.ForeColor:=-2;
                                        Obj.ObjectStyle.FillStyle.BackColor:=-2;

                                        Obj.ObjectStyle.LineStyle.Style:=-2;
                                        Obj.ObjectStyle.LineStyle.Color:=-2;
                                end;
                        end;
                     finally
                        if Obj.Index > 0 then begin
                                DestLayer.DuplicateObject(Obj);
                        end;
                     end;
                  end;
               end;
               CurEntry:=CurEntry^.Next;
               Inc(i);
           end;
           ButtonStop.Enabled:=False;

           if CreateLegend then begin
                DoCreateLegend('TM_' + LayerName);
           end;

           Document.Redraw;

           SaveGroupsToTxt;

           if AutoCloseAfterCreate then begin
                Close;
           end;
        end;
     end;
  finally
     StatusBar.Panels[1].Text:='';
     ProgressBar.Position:=0;
  end;
end;

function TThematicMainForm.DoCreateLegend(AName: String): Boolean;
var
i: Integer;
SymbolDef: ISymbolDef;
L,T,R,B: Integer;
ASize: Double;
x1,x2: Double;
ARect: IRect;

        procedure InsertRect;
        var
        APoly: IPolygon;
        APoint: IPoint;
        AText: IText;
        SText: WideString;
        begin
                APoly:=Document.CreatePolygon;
                APoint:=Document.CreatePoint;

                APoint.x:=L;
                APoint.y:=T;
                APoly.InsertPoint(APoint);

                APoint.x:=L;
                APoint.y:=B;
                APoly.InsertPoint(APoint);

                APoint.x:=R;
                APoint.y:=B;
                APoly.InsertPoint(APoint);

                APoint.x:=R;
                APoint.y:=T;
                APoly.InsertPoint(APoint);

                APoly.CreateObjectStyle;

                if CheckBoxFillStyle.Checked then begin
                        APoly.ObjectStyle.FillStyle.ForeColor:=Groups[i].FillColor;
                        APoly.ObjectStyle.FillStyle.Pattern:=GisFillStyles[Groups[i].FillStyle];
                end
                else begin
                        APoly.ObjectStyle.FillStyle.ForeColor:=Groups[i].LineColor;
                        APoly.ObjectStyle.FillStyle.Pattern:=7;
                end;

                APoly.ObjectStyle.LineStyle.Style:=Groups[i].LineStyle;
                APoly.ObjectStyle.LineStyle.WidthType:=1;
                APoly.ObjectStyle.LineStyle.Width:=Integer(Groups[i].LineWidth);
                APoly.ObjectStyle.LineStyle.Color:=Groups[i].LineColor;

                SymbolDef.InsertSymbolItem(APoly);


                if IsNumeric then begin
                        SText:=Format('%.0f',[Groups[i].StartVal]) + '-' + Format('%.0f',[Groups[i].EndVal]);
                end
                else begin
                        SText:=Groups[i].Cls;
                end;

                if SText <> '' then begin
                        AText:=Document.CreateText;
                        AText.Text:=SText;
                        AText.FontName:='Arial';
                        AText.FontHeight:=Abs(B-T);
                        AText.FontStyle:=0;
                        AText.Position.x:=R + 5000;
                        AText.Position.y:=T;

                        SymbolDef.InsertSymbolItem(AText);
                end;
        end;

begin
        SymbolDef:=Document.CreateSymbolDef;

        SymbolDef.Name:=AName;
        SymbolDef.DefaultSizeType:=0;

        Document.ReArrangeBounds;
        ARect:=Document.Bounds;
        x1:=ARect.x1;
        x2:=ARect.x2;

        ASize:=Abs(x1-x2);
        ASize:=Log10(ASize);
        ASize:=Trunc(ASize);
        ASize:=Max(ASize,1);
        ASize:=Power(10,ASize);

        //ASize:=ASize/100;

        SymbolDef.DefaultSize:=ASize;

        L:=0;
        T:=0;
        R:=10000;
        B:=T-10000;

        for i:=0 to Grid.RowCount-1 do begin
                InsertRect;
                T:=T-15000;
                B:=B-15000;
        end;

        Document.Symbols.InsertSymbolDef(SymbolDef);

        Result:=True;
end;

procedure TThematicMainForm.HeaderControlSectionResize(HeaderControl: THeaderControl; Section: THeaderSection);
begin
     if Section.Index = 0 then begin
        Grid.ColWidths[Section.Index]:=Section.Width-3;
     end
     else begin
        Grid.ColWidths[Section.Index]:=Section.Width-1;
     end;
     SetEditPos(MaskEdit.Tag,EditGroup);

end;

procedure TThematicMainForm.PanelFillColorStartClick(Sender: TObject);
var
Col: TColor;
begin
     GetColor(Col);
     PanelFillColorStart.Color:=Col;
     CalculateColorSteps(0);
     Grid.Repaint;
     SetModified;
end;

procedure TThematicMainForm.PanelFillColorEndClick(Sender: TObject);
var
Col: TColor;
begin
     GetColor(Col);
     PanelFillColorEnd.Color:=Col;
     CalculateColorSteps(0);
     Grid.Repaint;
     SetModified;
end;

procedure TThematicMainForm.CheckBoxAutoFillColorClick(Sender: TObject);
begin
     CalculateColorSteps(0);
     ResetColors;
     Grid.Repaint;
     SetModified;
end;

procedure TThematicMainForm.PanelLineColorStartClick(Sender: TObject);
var
Col: TColor;
begin
     GetColor(Col);
     PanelLineColorStart.Color:=Col;
     CalculateColorSteps(1);
     Grid.Repaint;
     SetModified;
end;

procedure TThematicMainForm.PanelLineColorEndClick(Sender: TObject);
var
Col: TColor;
begin
     GetColor(Col);
     PanelLineColorEnd.Color:=Col;
     CalculateColorSteps(1);
     Grid.Repaint;
     SetModified;
end;

procedure TThematicMainForm.CheckBoxAutoLineColorClick(Sender: TObject);
begin
     CalculateColorSteps(1);
     ResetColors;
     Grid.Repaint;
     SetModified;
end;

procedure TThematicMainForm.ButtonCloseClick(Sender: TObject);
begin
     Close;
end;

function TThematicMainForm.FillFields(OnlyNum: Boolean): Boolean;
var
i: Integer;
begin
     Result:=False;
     if ADODataSet.Active then begin
        ComboBoxFields.Clear;
        for i:=0 to ADODataset.Fields.Count-1 do begin
            if OnlyNum then begin
               if IsNumField(ADODataset.Fields[i]) then begin
                  ComboBoxFields.Items.Add(ADODataset.Fields[i].FieldName);
               end;
            end
            else begin
               ComboBoxFields.Items.Add(ADODataset.Fields[i].FieldName);
            end;
        end;
        SetModified;
        Result:=True;
     end;
end;

procedure TThematicMainForm.CheckBoxNumericOnlyClick(Sender: TObject);
begin
     FillFields(CheckBoxNumericOnly.Checked);
end;

function TThematicMainForm.RefreshDS: Boolean;
var
s,OrderFieldName: AnsiString;
t: Array [0..8000] of Char;
n: Integer;
begin
     try
        if ADODataset.Active then begin
           if ThematicFieldIndex > -1 then begin
              OrderFieldName:=ADODataset.Fields[ThematicFieldIndex].FieldName;
              ADODataset.Close;
              s:=ADODataset.CommandText;
              n:=Pos(' order by',s);
              if n <> 0 then begin
                 StrLCopy(t,PChar(s),n-1);
                 s:=t;
              end;
              s:=s + ' order by [' + OrderFieldName + ']';
              ADODataset.CommandText:=s;
           end;
        end;
        if not ADODataset.Active then begin
           if ADOConnection.Connected then begin
                ADOConnection.Close;
           end;
           ADOConnection.Open;
           ADODataset.Open;
        end;
        Result:=ADODataset.Active;
     except
        Result:=False;
     end;
end;

function TThematicMainForm.AnalyseValues: Boolean;
var
Sum,Avg,Median: Double;
i,d: Integer;
begin
     Stopped:=False;
     ResetThematicList;
     Sum:=0;
     ProgressBar.Max:=ADODataset.RecordCount;
     d:=ProgressBar.Max div 100;
     d:=Max(d,1);
     ADODataset.First;
     ButtonStop.Enabled:=True;
     for i:=0 to ADODataset.RecordCount-1 do begin
         if (i mod d) = 0 then begin
            GetCounts;
            ProgressBar.Position:=i;
         end;
         Application.ProcessMessages;
         if Stopped then begin
            ProgressBar.Position:=0;
            ButtonStop.Enabled:=False;
            break;
         end;
         if (IsNumeric) and (ThematicFieldIndex <> ProgisIdFieldIndex) then begin
            Sum:=Sum+ADODataset.Fields[ThematicFieldIndex].AsFloat;
         end;
         if CurEntry <> nil then begin
            New(CurEntry^.Next);
            CurEntry:=CurEntry^.Next;
            CurEntry^.Next:=nil;
         end
         else begin
            New(CurEntry);
            CurEntry^.Next:=nil;
         end;
         CurEntry^.Id:=ADODataset.Fields[ProgisIdFieldIndex].AsInteger;
         if IsNumeric then begin
            CurEntry^.Value:=ADODataset.Fields[ThematicFieldIndex].AsFloat;
         end
         else begin
            CurEntry^.Value:=-1;
         end;
         CurEntry^.Text:=ADODataset.Fields[ThematicFieldIndex].AsString;
         CurEntry^.Group:=-1;
         CurEntry^.Next:=nil;
         Inc(EntryCount);
         if FirstEntry = nil then begin
            FirstEntry:=CurEntry;
         end;
         ADODataset.Next;
     end;
     ProgressBar.Position:=0;
     ButtonStop.Enabled:=False;
     if IsNumeric then begin
        Avg:=Sum/EntryCount;
        if Avg >= 1 then begin
           Avg:=Round(Avg*100)/100;
        end;
        if EntryCount > 0 then begin
           ADODataset.RecNo:=1;
           MinValue:=ADODataset.Fields[ThematicFieldIndex].AsFloat;
           ADODataset.RecNo:=(EntryCount div 2)+1;
           Median:=ADODataset.Fields[ThematicFieldIndex].AsFloat;
           ADODataset.RecNo:=EntryCount;
           MaxValue:=ADODataset.Fields[ThematicFieldIndex].AsFloat;
           if ThematicFieldIndex <> ProgisIdFieldIndex then begin
              LabelMinValue.Caption:=FloatToStr(MinValue);
              LabelMaxValue.Caption:=FloatToStr(MaxValue);
              LabelAvgValue.Caption:=FloatToStr(Avg);
              LabelMedianValue.Caption:=FloatToStr(Median);
           end;
        end;
        StatusBar.Panels[1].Text:=ADODataset.Fields[ThematicFieldIndex].FieldName + ': ' + FloatToStr(MinValue) + ' -> ' + FloatToStr(MaxValue);
     end;
     Result:=True;
end;

procedure TThematicMainForm.GridMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
ACol,ARow: Integer;
Rect: TRect;
begin
     Grid.MouseToCell(X,Y,ACol,ARow);
     Rect:=Grid.CellRect(ACol,ARow);
     StyleMode:=ACol;
     StyleGroup:=ARow;
     ComboBoxFillStyle.Tag:=0;
     ComboBoxLineStyle.Tag:=0;
     CheckBoxFillStyle.Visible:=False;
     CheckBoxLineStyle.Visible:=False;

     if (ACol = fiFill) then begin
        ComboBoxFillStyle.Width:=Rect.Right-Rect.Left;
        ComboBoxFillStyle.Left:=Rect.Left+Grid.Left+TabSheetGroups.Left+PageControl.Left+2;
        ComboBoxFillStyle.Top:=Rect.Top+Grid.Top+TabSheetGroups.Top+PageControl.Top+2;
        ComboBoxFillStyle.ItemIndex:=Integer(Groups[StyleGroup].FillStyle);
        ComboBoxFillStyle.Visible:=True;
        ComboBoxFillStyle.Repaint;
        if not CheckBoxAutoFillColor.Checked then begin
           ButtonColor.Left:=ComboBoxFillStyle.Left+2;
           ButtonColor.Top:=ComboBoxFillStyle.Top+2;
           ButtonColor.Visible:=True;
        end
        else begin
           ButtonColor.Visible:=False;
        end;
        ComboBoxLineStyle.Visible:=False;
        SpinButtonLineWidth.Visible:=False;
     end
     else if (ACol = fiLine) then begin
        ComboBoxLineStyle.Width:=Rect.Right-Rect.Left;
        ComboBoxLineStyle.Left:=Rect.Left+Grid.Left+TabSheetGroups.Left+PageControl.Left+2;
        ComboBoxLineStyle.Top:=Rect.Top+Grid.Top+TabSheetGroups.Top+PageControl.Top+2;
        ComboBoxLineStyle.ItemIndex:=Integer(Groups[StyleGroup].LineStyle);
        if TPenStyle(Groups[StyleGroup].LineStyle) = psSolid then begin
           ComboBoxLineStyle.Width:=ComboBoxLineStyle.Width-SpinButtonLineWidth.Width;
           SpinButtonLineWidth.Left:=ComboBoxLineStyle.Left+ComboBoxLineStyle.Width;
           SpinButtonLineWidth.Top:=ComboBoxLineStyle.Top;
           SpinButtonLineWidth.Visible:=True;
        end
        else begin
           SpinButtonLineWidth.Visible:=False;
        end;
        ComboBoxLineStyle.Visible:=True;
        ComboBoxLineStyle.Repaint;
        if not CheckBoxAutoLineColor.Checked then begin
           ButtonColor.Left:=ComboBoxLineStyle.Left+2;
           ButtonColor.Top:=ComboBoxLineStyle.Top+2;
           ButtonColor.Visible:=True;
        end
        else begin
           ButtonColor.Visible:=False;
        end;
        ComboBoxFillStyle.Visible:=False;
     end
     else begin
        ComboBoxFillStyle.Visible:=False;
        ComboBoxLineStyle.Visible:=False;
        ButtonColor.Visible:=False;
        SpinButtonLineWidth.Visible:=False;
     end;
end;

procedure TThematicMainForm.HeaderControlSectionClick(
  HeaderControl: THeaderControl; Section: THeaderSection);
begin
     if Section.Index = 0 then begin
        ComboBoxFillStyle.Width:=Section.Width;
        ComboBoxFillStyle.Left:=HeaderControl.Left+TabSheetGroups.Left+PageControl.Left;
        ComboBoxFillStyle.Top:=HeaderControl.Top+TabSheetGroups.Top+PageControl.Top;
        ComboBoxFillStyle.ItemIndex:=0;
        ComboBoxFillStyle.Tag:=1;
        ComboBoxFillStyle.Visible:=True;
        ComboBoxFillStyle.Repaint;
        ButtonColor.Visible:=False;
        ComboBoxLineStyle.Visible:=False;
        CheckBoxFillStyle.Left:=ComboBoxFillStyle.Left+3;
        CheckBoxFillStyle.Top:=ComboBoxFillStyle.Top+4;
        CheckBoxFillStyle.Visible:=True;
     end
     else if Section.Index = 1 then begin
        ComboBoxLineStyle.Width:=Section.Width;
        ComboBoxLineStyle.Left:=HeaderControl.Sections[0].Width+HeaderControl.Left+TabSheetGroups.Left+PageControl.Left;
        ComboBoxLineStyle.Top:=HeaderControl.Top+TabSheetGroups.Top+PageControl.Top;
        ComboBoxLineStyle.ItemIndex:=0;
        ComboBoxLineStyle.Tag:=1;
        ComboBoxLineStyle.Visible:=True;
        ComboBoxLineStyle.Repaint;
        ButtonColor.Visible:=False;
        ComboBoxFillStyle.Visible:=False;
        CheckBoxLineStyle.Left:=ComboBoxLineStyle.Left+3;
        CheckBoxLineStyle.Top:=ComboBoxLineStyle.Top+4;
        CheckBoxLineStyle.Visible:=True;
     end;
end;

procedure TThematicMainForm.ComboBoxFillStyleDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
     ComboBoxFillStyle.Canvas.Pen.Style:=psSolid;
     ComboBoxFillStyle.Canvas.Pen.Color:=clGray;
     ComboBoxFillStyle.Canvas.Brush.Style:=TBrushStyle(Index);
     if Index = Integer(bsClear) then begin
        ComboBoxFillStyle.Canvas.Brush.Color:=ComboBoxFillStyle.Color;
     end
     else begin
        if ComboBoxFillStyle.Tag = 0 then begin
           ComboBoxFillStyle.Canvas.Brush.Color:=Groups[StyleGroup].FillColor;
        end
        else begin
           ComboBoxFillStyle.Canvas.Brush.Color:=clBlack;
        end;
     end;
     if Index > LASTFILLSTYLE then begin
        ComboBoxFillStyle.Canvas.Font.Name:='Small Fonts';
        ComboBoxFillStyle.Canvas.Font.Size:=7;
        ComboBoxFillStyle.Canvas.Font.Color:=ComboBoxFillStyle.Canvas.Brush.Color;
        ComboBoxFillStyle.Canvas.Brush.Style:=bsClear;
        ComboBoxFillStyle.Canvas.Rectangle(Rect);
        ComboBoxFillStyle.Canvas.TextOut(Rect.Left+2,Rect.Top+2,ComboBoxFillStyle.Items[Index]);
     end
     else begin
        ComboBoxFillStyle.Canvas.Rectangle(Rect);          
     end;
end;

procedure TThematicMainForm.PageControlMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
     ComboBoxFillStyle.Visible:=False;
     ComboBoxLineStyle.Visible:=False;
     CheckBoxFillStyle.Visible:=False;
     CheckBoxLineStyle.Visible:=False;
     ButtonColor.Visible:=False;
     SpinButtonLineWidth.Visible:=False;
end;

procedure TThematicMainForm.ComboBoxFillStyleChange(Sender: TObject);
var
i: Integer;
begin
     if ComboBoxFillStyle.Tag = 0 then begin
        if ComboBoxFillStyle.ItemIndex > -1 then begin
           Groups[StyleGroup].FillStyle:=ComboBoxFillStyle.ItemIndex;
        end;
     end
     else begin
        for i:=0 to MAXGROUPS-1 do begin
            Groups[i].FillStyle:=ComboBoxFillStyle.ItemIndex;
        end;
     end;
     Grid.Repaint;
     ComboBoxFillStyle.Visible:=False;
     ButtonColor.Visible:=False;
     SetModified;
end;

procedure TThematicMainForm.ButtonColorClick(Sender: TObject);
begin
     if (StyleMode = 0) and (not CheckBoxAutoFillColor.Checked) then begin
        GetColor(Groups[StyleGroup].FillColor);
        StdFillColors[StyleGroup]:=Groups[StyleGroup].FillColor;
     end
     else if (StyleMode = 1) and (not CheckBoxAutoLineColor.Checked) then begin
        GetColor(Groups[StyleGroup].LineColor);
        StdLineColors[StyleGroup]:=Groups[StyleGroup].LineColor;
     end;
     ButtonColor.Visible:=False;
     SetModified;
end;

procedure TThematicMainForm.ComboBoxLineStyleChange(Sender: TObject);
var
i: Integer;
begin
     if ComboBoxLineStyle.Tag = 0 then begin
        if ComboBoxLineStyle.ItemIndex > -1 then begin
           Groups[StyleGroup].LineStyle:=ComboBoxLineStyle.ItemIndex;
        end;
     end
     else begin
        for i:=0 to MAXGROUPS-1 do begin
            Groups[i].LineStyle:=ComboBoxLineStyle.ItemIndex;
            if TPenStyle(Groups[i].LineStyle) <> psSolid then begin
               Groups[i].LineWidth:=0;
            end;
        end;
     end;
     if TPenStyle(Groups[StyleGroup].LineStyle) <> psSolid then begin
        Groups[StyleGroup].LineWidth:=0;
     end;
     Grid.Repaint;
     ComboBoxLineStyle.Visible:=False;
     ButtonColor.Visible:=False;
     SetModified;
end;

procedure TThematicMainForm.ComboBoxLineStyleDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
     ComboBoxLineStyle.Canvas.Pen.Style:=psSolid;
     ComboBoxLineStyle.Canvas.Pen.Color:=clGray;
     ComboBoxLineStyle.Canvas.Brush.Style:=bsSolid;
     ComboBoxLineStyle.Canvas.Brush.Color:=clWhite;
     ComboBoxLineStyle.Canvas.Rectangle(Rect);

     if Index > LASTLINESTYLE then begin
        ComboBoxLineStyle.Canvas.Font.Name:='Small Fonts';
        ComboBoxLineStyle.Canvas.Font.Size:=7;
        ComboBoxLineStyle.Canvas.Font.Color:=Groups[StyleGroup].LineColor;
        ComboBoxLineStyle.Canvas.TextOut(Rect.Left+2,Rect.Top+2,ComboBoxLineStyle.Items[Index]);
     end
     else begin
        ComboBoxLineStyle.Canvas.Pen.Style:=TPenStyle(Index);
        if ComboBoxLineStyle.Tag = 0 then begin
           ComboBoxLineStyle.Canvas.Pen.Color:=Groups[StyleGroup].LineColor;
        end
        else begin
           ComboBoxLineStyle.Canvas.Pen.Color:=clBlack;
        end;
        ComboBoxLineStyle.Canvas.MoveTo(Rect.Left,(Rect.Top + Rect.Bottom) div 2);
        ComboBoxLineStyle.Canvas.LineTo(Rect.Right,(Rect.Top + Rect.Bottom) div 2);
     end;
end;

procedure TThematicMainForm.PaintBoxPaint(Sender: TObject);
var
BarWidth: Double;
MaxVal,MaxHeight,BarHeight: Double;
BarCount: Integer;
i: Integer;
Rect: TRect;
Text: String;
x,y: Integer;

     function ScaledVal(Val: Double): Double;
     begin
          if (Val > 0.0) and (CheckBoxLogar.Checked) then begin
             Val:=Log10(Val*10);
          end;
          Result:=Val;
     end;

begin
     MaxVal:=0;
     BarCount:=0;
     PaintBox.Canvas.Refresh;
     for i:=0 to Grid.RowCount-1 do begin
         if Groups[i].Active then begin
            MaxVal:=Max(MaxVal,ScaledVal(Groups[i].Count));
            Inc(BarCount);
         end;
     end;
     if BarCount > 0 then begin
        MaxHeight:=PaintBox.Height-15;
        BarWidth:=(PaintBox.Width div BarCount);
        BarCount:=0;
        for i:=0 to Grid.RowCount-1 do begin
            if Groups[i].Active then begin
               PaintBox.Canvas.Brush.Color:=Groups[i].FillColor;
               PaintBox.Canvas.Brush.Style:=TBrushStyle(Groups[i].FillStyle);
               PaintBox.Canvas.Pen.Color:=Groups[i].LineColor;
               PaintBox.Canvas.Pen.Style:=TPenStyle(Groups[i].LineStyle);
               BarHeight:=(ScaledVal(Groups[i].Count) * MaxHeight) / MaxVal;

               Rect.Left:=Trunc((BarWidth*BarCount))+2;
               Rect.Right:=Rect.Left+Trunc(BarWidth)-2;
               Rect.Top:=PaintBox.Height-Trunc(BarHeight);
               Rect.Bottom:=Rect.Top+Trunc(BarHeight);

               PaintBox.Canvas.Rectangle(Rect);

               Text:=' ' + IntToStr(Groups[i].Count) + ' ';
               PaintBox.Canvas.Brush.Color:=clWhite;
               PaintBox.Canvas.Pen.Color:=clBlack;
               PaintBox.Canvas.Font.Size:=7;
               PaintBox.Canvas.Font.Name:='Small Fonts';
               X:=(Rect.Left + Rect.Right) div 2;
               X:=X - PaintBox.Canvas.TextWidth(Text) div 2;
               Y:=Rect.Top - PaintBox.Canvas.TextHeight(Text) - 1;
               PaintBox.Canvas.TextOut(X,Y,Text);

               Inc(BarCount);
            end;
        end;
     end;
end;

function TThematicMainForm.SetModified: Boolean;
var
i: Integer;
begin
     Modified:=True;
     GetCounts;
     PaintBox.Repaint;
     MaskEdit.Visible:=False;
     ButtonCreate.Enabled:=False;
     ButtonSave.Enabled:=False;
     for i:=0 to Grid.RowCount-1 do begin
         if Groups[i].Active then begin
            ButtonCreate.Enabled:=True;
            if ComboBoxSchemas.ItemIndex > -1 then begin
                ButtonSave.Enabled:=True;
            end;
            break;
         end;
     end;
     SpeedButtonSaveGrouping.Enabled:=True;
     for i:=0 to Grid.RowCount-1 do begin
         if (Grid.Cells[fiStart,i] = '') or (Grid.Cells[fiEnd,i] = '') then begin
            SpeedButtonSaveGrouping.Enabled:=False;
            break;
         end;
     end;
     SpeedButtonDeleteGrouping.Enabled:=(ComboBoxMethod.ItemIndex >= miSaved);
     ComboBoxMethod.Enabled:=(ComboBoxFields.ItemIndex > -1) and (IsNumeric);
     Result:=True;
end;

procedure TThematicMainForm.CheckBoxLogarClick(Sender: TObject);
begin
     PaintBox.Repaint;
end;

procedure TThematicMainForm.GridMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
ACol,ARow: Integer;
AllowAssign: Boolean;
begin
     Grid.MouseToCell(X,Y,ACol,ARow);
     AllowAssign:=MaskEdit.Visible;
     MaskEdit.Visible:=False;
     if (ACol = fiStart) or (ACol = fiEnd) then begin
        if ComboBoxMethod.ItemIndex <> miClassification then begin
           ComboBoxMethod.ItemIndex:=miCustom;
           ComboBoxMethodChange(nil);
        end;
        if AllowAssign then begin
           AssignValue(MaskEdit.Tag,EditGroup,MaskEdit.Text);
        end;
        SetInputMask(ACol,ARow);
     end
     else if ACol = fiActive then begin
        Groups[ARow].Active:=not Groups[ARow].Active;
        if (IsNumeric)  and ((Trim(Grid.Cells[fiStart,ARow]) = '') or (Trim(Grid.Cells[fiEnd,ARow]) = '')) then begin
           Groups[ARow].Active:=False;
        end;
        SetModified;
     end;
     Grid.Repaint;
end;

procedure TThematicMainForm.GridSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
     CanSelect:=False;
end;

function TThematicMainForm.ResetGridSelection: Boolean;
var
Rect: TGridRect;
begin
     Rect.Left:=-1;
     Rect.Top:=-1;
     Rect.Right:=-1;
     Rect.Bottom:=-1;
     Grid.Selection:=Rect;
     Result:=True;
end;

procedure TThematicMainForm.MaskEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if Key = VK_ESCAPE then begin
        MaskEdit.Visible:=False;
     end
     else if (Key = VK_RETURN) or (Key = VK_TAB) then begin
        AssignValue(MaskEdit.Tag,EditGroup,MaskEdit.Text);
        MaskEdit.Visible:=False;
     end
end;

procedure TThematicMainForm.MaskEditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if Key = VK_TAB then begin
        AssignValue(MaskEdit.Tag,EditGroup,MaskEdit.Text);
        MaskEdit.Visible:=False;
        if (MaskEdit.Tag = fiStart) and (IsNumeric) then begin
           SetInputMask(fiEnd,EditGroup);
        end
        else {if MaskEdit.Tag = fiEnd then} begin
           if EditGroup < Grid.RowCount-1 then begin
              SetInputMask(fiStart,EditGroup+1);
           end
           else begin
              SetInputMask(fiStart,0);
           end;
        end;
     end;
end;

procedure TThematicMainForm.PageControlChange(Sender: TObject);
begin
     MaskEdit.Visible:=False;
end;

function TThematicMainForm.SetInputMask(ACol, ARow: Integer): Boolean;
begin
     Result:=False;
     if AllowEditing then begin
        SetEditPos(ACol,ARow);
        MaskEdit.Text:=Grid.Cells[ACol,ARow];
        MaskEdit.Visible:=True;
        MaskEdit.SetFocus;
        MaskEdit.Tag:=ACol;
        EditGroup:=ARow;
        Result:=True;
     end;
end;

function TThematicMainForm.SetEditPos(ACol,ARow: Integer): Boolean;
var
Rect: TRect;
begin
     Rect:=Grid.CellRect(ACol,ARow);
     MaskEdit.Width:=Rect.Right-Rect.Left;
     MaskEdit.Left:=Rect.Left+Grid.Left+TabSheetGroups.Left+PageControl.Left+2;
     MaskEdit.Top:=Rect.Top+Grid.Top+TabSheetGroups.Top+PageControl.Top+2;
     Result:=True;
end;

function TThematicMainForm.GetCounts: Boolean;
var
i,n: Integer;
begin
     Result:=False;
     n:=0;
     for i:=0 to Grid.RowCount-1 do begin
         if Groups[i].Active then begin
            n:=n+Groups[i].Count;
            Result:=True;
         end;
     end;
     StatusBar.Panels[0].Text:=LangText(1) + ' ' + IntToStr(n) + '/' + IntToStr(EntryCount);
end;

procedure TThematicMainForm.ButtonStopClick(Sender: TObject);
begin
     Stopped:=True;
end;

procedure TThematicMainForm.SpeedButtonSaveSchemaClick(Sender: TObject);
var
SchemaName: String;
FileName: String;
i,n: Integer;
FileHandle: Integer;
IsCls: Boolean;
Buffer: Array[0..255] of Char;
begin
     try
        IsCls:=(ComboBoxMethod.ItemIndex = miClassification);
        SchemaName:=ComboBoxSchemas.Text;
        if InputQuery(LangText(41),LangText(42),SchemaName) and (Trim(SchemaName) <> '') then begin
           SchemaName:=Trim(SchemaName);
           SchemaName:=StringReplace(SchemaName,'/','',[rfReplaceAll]);
           SchemaName:=StringReplace(SchemaName,'\','',[rfReplaceAll]);
           SchemaName:=StringReplace(SchemaName,'*','',[rfReplaceAll]);
           SchemaName:=StringReplace(SchemaName,'?','',[rfReplaceAll]);
           SchemaName:=StringReplace(SchemaName,'"','',[rfReplaceAll]);
           SchemaName:=StringReplace(SchemaName,'<','',[rfReplaceAll]);
           SchemaName:=StringReplace(SchemaName,'>','',[rfReplaceAll]);
           SchemaName:=StringReplace(SchemaName,'|','',[rfReplaceAll]);
           FileName:=OSInfo.DocDataDir + THEMDIR + SchemaName + SCHFILEEXT;
           if not FileExists(FileName) then begin
              FileHandle:=FileCreate(FileName);
              if FileHandle <> -1 then begin
                 FileClose(FileHandle);
              end;
           end;
           FileHandle:=FileOpen(FileName,fmOpenWrite);
           if FileHandle <> -1 then begin
              n:=Grid.RowCount;
              if IsCls then begin
                 n:=-n;
              end;
              FileWrite(FileHandle,n,SizeOf(Integer));
              for i:=0 to MAXGROUPS-1 do begin
                  FileWrite(FileHandle,Groups[i].FillColor,SizeOf(Integer));
                  FileWrite(FileHandle,Groups[i].FillStyle,SizeOf(Integer));
                  FileWrite(FileHandle,Groups[i].LineColor,SizeOf(Integer));
                  FileWrite(FileHandle,Groups[i].LineStyle,SizeOf(Integer));
                  FileWrite(FileHandle,Groups[i].LineWidth,SizeOf(Integer));
                  if IsCls then begin
                     StrCopy(Buffer,PChar(Groups[i].Cls));
                     FileWrite(FileHandle,Buffer,SizeOf(Buffer));
                  end;
              end;
              FileClose(FileHandle);
              SetModified;
           end;
        end;
     finally
        LoadSchemas;
        ComboBoxSchemas.ItemIndex:=ComboBoxSchemas.Items.IndexOf(SchemaName);
        SpeedButtonDeleteSchema.Enabled:=(ComboBoxSchemas.ItemIndex <> -1);
     end;
end;

function TThematicMainForm.LoadSchemas: Boolean;
var
Search: TSearchRec;
SearchRes: Integer;
SchemaName: String;
begin
     ComboBoxSchemas.Clear;
     SearchRes:=FindFirst(OSInfo.DocDataDir + THEMDIR + '*' + SCHFILEEXT,faAnyFile,Search);
     while SearchRes = 0 do begin
           SchemaName:=Search.Name;
           SchemaName:=StringReplace(SchemaName,SCHFILEEXT,'',[rfIgnoreCase]);
           ComboBoxSchemas.Items.Add(SchemaName);
           SearchRes:=FindNext(Search);
     end;
     FindClose(Search);
     Result:=True;
end;

function TThematicMainForm.LoadTMFiles: Boolean;
var
Search: TSearchRec;
SearchRes: Integer;
TMName: String;
begin
     SearchRes:=FindFirst(OSInfo.DocDataDir + THEMDIR + '*' + TMFILEEXT,faAnyFile,Search);
     while SearchRes = 0 do begin
           TMName:=Search.Name;
           TMName:=StringReplace(TMName,TMFILEEXT,'',[rfIgnoreCase]);

           if (MapsDlg <> nil) then begin
                MapsDlg.LbMaps.Items.Add(TMName);
           end;

           SearchRes:=FindNext(Search);
     end;
     FindClose(Search);
     Result:=True;
end;

function TThematicMainForm.LoadSchema(SchemaName: String): Boolean;
var
FileName: String;
i: Integer;
FileHandle: Integer;
Value: Integer;
n: Integer;
IsCls: Boolean;
Buffer: Array[0..255] of Char;
begin
     Result:=True;
     IsCls:=False;
     if ComboBoxSchemas.Items.IndexOf(SchemaName) > -1 then begin
        SpeedButtonDeleteSchema.Enabled:=(ComboBoxSchemas.Items.IndexOf(SchemaName) > -1);
        FileName:=OSInfo.DocDataDir + THEMDIR + SchemaName + SCHFILEEXT;
        FileHandle:=FileOpen(FileName,fmOpenRead);
        if FileHandle <> -1 then begin
           if FileRead(FileHandle,n,SizeOf(Integer)) > 0 then begin
              if n < 0 then begin
                 n:=-n;
                 IsCls:=True;
              end;
              SpinEditGroups.Value:=n;
              for i:=0 to n-1 do begin
                  if FileRead(FileHandle,Value,SizeOf(Integer)) > 0 then begin
                     Groups[i].FillColor:=TColor(Value);
                  end;
                  if FileRead(FileHandle,Value,SizeOf(Integer)) > 0 then begin
                     Groups[i].FillStyle:=Value;
                  end;
                  if FileRead(FileHandle,Value,SizeOf(Integer)) > 0 then begin
                     Groups[i].LineColor:=TColor(Value);
                  end;
                  if FileRead(FileHandle,Value,SizeOf(Integer)) > 0 then begin
                     Groups[i].LineStyle:=Value;
                  end;
                  if FileRead(FileHandle,Value,SizeOf(Integer)) > 0 then begin
                     Groups[i].LineWidth:=Value;
                  end;
                  if IsCls then begin
                     if FileRead(FileHandle,Buffer,SizeOf(Buffer)) > 0 then begin
                        Groups[i].Cls:=StrPas(Buffer);
                     end;
                  end;
                  StdFillColors[i]:=Groups[i].FillColor;
                  StdLineColors[i]:=Groups[i].LineColor;
              end;
           end;
           FileClose(FileHandle);
           Result:=True;
        end;
        CheckBoxAutoFillColor.Checked:=False;
        CheckBoxAutoLineColor.Checked:=False;
        if (IsCls) and (not IsNumeric) then begin
           for i:=0 to n-1 do begin
               AssignValue(fiStart,i,Groups[i].Cls);
           end;
           CheckBoxUseSchema.Checked:=True;
        end;
        Grid.Repaint;
        SetModified;
     end;
end;

procedure TThematicMainForm.ComboBoxSchemasChange(Sender: TObject);
var
AName: String;
begin
        if ComboBoxSchemas.ItemIndex > -1 then begin
                AName:=ComboBoxSchemas.Text;
                if AName <> '' then begin
                        LoadSchema(AName);
                end;
        end;
end;

procedure TThematicMainForm.SpeedButtonDeleteSchemaClick(Sender: TObject);
var
SchemaName: String;
begin
     SchemaName:=ComboBoxSchemas.Text;
     if Trim(SchemaName) <> '' then begin
        if MessageBox(Handle,PChar(Format(LangText(46),[SchemaName])),PChar(Caption),MB_YESNO or MB_ICONQUESTION or MB_APPLMODAL) = ID_YES then begin
           try
              DeleteFile(OSInfo.DocDataDir + THEMDIR + SchemaName + GRPFILEEXT);
           finally
              LoadSchemas;
              ComboBoxSchemas.ItemIndex:=-1;
              SpeedButtonDeleteSchema.Enabled:=False;
           end;
        end;
     end;
end;

procedure TThematicMainForm.SpeedButtonSaveGroupingClick(Sender: TObject);
var
GroupingName: String;
FileName: String;
i: Integer;
FileHandle: Integer;
n: Integer;
begin
     try
        if ComboBoxMethod.ItemIndex >= miSaved then begin
           GroupingName:=ComboBoxMethod.Text;
        end
        else begin
           GroupingName:='';
        end;
        if InputQuery(LangText(48),LangText(49),GroupingName) and (Trim(GroupingName) <> '') then begin
           GroupingName:=Trim(GroupingName);
           GroupingName:=StringReplace(GroupingName,'/','',[rfReplaceAll]);
           GroupingName:=StringReplace(GroupingName,'\','',[rfReplaceAll]);
           GroupingName:=StringReplace(GroupingName,'*','',[rfReplaceAll]);
           GroupingName:=StringReplace(GroupingName,'?','',[rfReplaceAll]);
           GroupingName:=StringReplace(GroupingName,'"','',[rfReplaceAll]);
           GroupingName:=StringReplace(GroupingName,'<','',[rfReplaceAll]);
           GroupingName:=StringReplace(GroupingName,'>','',[rfReplaceAll]);
           GroupingName:=StringReplace(GroupingName,'|','',[rfReplaceAll]);
           FileName:=OSInfo.DocDataDir + THEMDIR + GroupingName + GRPFILEEXT;
           if not FileExists(FileName) then begin
              FileHandle:=FileCreate(FileName);
              if FileHandle <> -1 then begin
                 FileClose(FileHandle);
              end;
           end;
           FileHandle:=FileOpen(FileName,fmOpenWrite);
           if FileHandle <> -1 then begin
              n:=Grid.RowCount;
              FileWrite(FileHandle,n,SizeOf(Integer));
              for i:=0 to Grid.RowCount-1 do begin
                  FileWrite(FileHandle,Groups[i].StartVal,SizeOf(Double));
                  FileWrite(FileHandle,Groups[i].EndVal,SizeOf(Double));
              end;
              FileClose(FileHandle);
           end;
        end;
     finally
        if GroupingName <> '' then begin
           LoadGroupings;
           ComboBoxMethod.ItemIndex:=ComboBoxMethod.Items.IndexOf(GroupingName);
           SpeedButtonDeleteGrouping.Enabled:=(ComboBoxMethod.ItemIndex <> -1) and (ComboBoxMethod.ItemIndex >= miSaved);
        end;
     end;
end;

function TThematicMainForm.LoadGroupings: Boolean;
var
i: Integer;
Search: TSearchRec;
SearchRes: Integer;
GroupingName: String;
begin
     for i:=miSaved to ComboBoxMethod.Items.Count-1 do begin
         ComboBoxMethod.Items.Delete(i);
     end;
     SearchRes:=FindFirst(OSInfo.DocDataDir + THEMDIR + '*' + GRPFILEEXT,faAnyFile,Search);
     while SearchRes = 0 do begin
           GroupingName:=Search.Name;
           GroupingName:=StringReplace(GroupingName,GRPFILEEXT,'',[rfIgnoreCase]);
           ComboBoxMethod.Items.Add(GroupingName);
           SearchRes:=FindNext(Search);
     end;
     FindClose(Search);
     Result:=True;
end;

procedure TThematicMainForm.SpeedButtonDeleteGroupingClick(
  Sender: TObject);
var
GroupingName: String;
begin
     GroupingName:=ComboBoxMethod.Text;
     if Trim(GroupingName) <> '' then begin
        if MessageBox(Handle,PChar(Format(LangText(50),[GroupingName])),PChar(Caption),MB_YESNO or MB_ICONQUESTION or MB_APPLMODAL) = ID_YES then begin
           try
              DeleteFile(OSInfo.DocDataDir + THEMDIR + GroupingName + GRPFILEEXT);
           finally
              LoadGroupings;
              ComboBoxMethod.ItemIndex:=miCustom;
              SpeedButtonDeleteGrouping.Enabled:=False;
           end;
        end;
     end;
end;

function TThematicMainForm.SetGrouping(GroupingName: String): Boolean;
var
FileName: String;
i: Integer;
FileHandle: Integer;
Value: Double;
n: Integer;
begin
     Result:=False;
     if ComboBoxMethod.ItemIndex <> -1 then begin
        SpeedButtonDeleteGrouping.Enabled:=(ComboBoxMethod.ItemIndex >= miSaved);
        FileName:=OSInfo.DocDataDir + THEMDIR + GroupingName + GRPFILEEXT;
        FileHandle:=FileOpen(FileName,fmOpenRead);
        if FileHandle <> -1 then begin
           if FileRead(FileHandle,n,SizeOf(Integer)) > 0 then begin
              SpinEditGroups.Value:=n;
              for i:=0 to n-1 do begin
                  if FileRead(FileHandle,Value,SizeOf(Double)) > 0 then begin
                     AssignValue(fiStart,i,FloatToStr(Value));
                  end;
                  if FileRead(FileHandle,Value,SizeOf(Double)) > 0 then begin
                     AssignValue(fiEnd,i,FloatToStr(Value));
                  end;
              end;
           end;
           FileClose(FileHandle);
        end
        else begin
           i:=ComboBoxMethod.Items.IndexOf(GroupingName);
           if (i >= 0) and (i <> ComboBoxMethod.ItemIndex) then begin
                ComboBoxMethod.ItemIndex:=i;
                ComboBoxMethodChange(nil);
           end;
        end;
        SetModified;
     end;
end;

procedure TThematicMainForm.SpinButtonLineWidthDownClick(Sender: TObject);
begin
     Dec(Groups[StyleGroup].LineWidth);
     Groups[StyleGroup].LineWidth:=Max(Groups[StyleGroup].LineWidth,MINLINEWIDTH);
     ComboBoxLineStyle.Visible:=False;
     ButtonColor.Visible:=False;
     Grid.Repaint;
end;

procedure TThematicMainForm.SpinButtonLineWidthUpClick(Sender: TObject);
begin
     Inc(Groups[StyleGroup].LineWidth);
     Groups[StyleGroup].LineWidth:=Min(Groups[StyleGroup].LineWidth,MAXLINEWIDTH);
     ComboBoxLineStyle.Visible:=False;
     ButtonColor.Visible:=False;
     Grid.Repaint;
end;

function TThematicMainForm.SetCompactMode(IsOn: Boolean): Boolean;
begin
     CompactMode:=IsOn;
     PageControl.Visible:=not CompactMode;
     GroupBoxFields.Visible:=not CompactMode;
     GroupBoxAutoColor.Visible:=not CompactMode;
     GroupBoxGrouping.Visible:=not CompactMode;
     ButtonReset.Enabled:=not CompactMode;
     if CompactMode then begin
        Height:=104;
     end
     else begin
        Height:=482;
     end;
     Result:=True;
end;

procedure TThematicMainForm.CheckBoxFillStyleClick(Sender: TObject);
begin
     Grid.Repaint;
end;

procedure TThematicMainForm.CheckBoxLineStyleClick(Sender: TObject);
begin
     Grid.Repaint;
end;

function TThematicMainForm.GetLineStyles: Integer;
var
i: Integer;
s: WideString;
begin
     Result:=0;
     ComboBoxLineStyle.Items.Clear;
     for i:=0 to LASTLINESTYLE do begin
         ComboBoxLineStyle.Items.Add('');
     end;
     for i:=1000 to 1100 do begin
         s:=Document.GetUserLineStyleNameByNumber(i);
         if s <> '' then begin
            ComboBoxLineStyle.Items.Add(s);
            Inc(Result);
         end;
     end;
end;

function TThematicMainForm.GetFillStyles: Integer;
var
i: Integer;
s: WideString;
begin
     Result:=0;
     ComboBoxFillStyle.Items.Clear;
     for i:=0 to LASTFILLSTYLE do begin
         ComboBoxFillStyle.Items.Add('');
     end;
     for i:=8 to 100 do begin
         s:=Document.GetUserFillStyleNameByNumber(i);
         if s <> '' then begin
            ComboBoxFillStyle.Items.Add(s);
            Inc(Result);
         end;
     end;
end;

procedure TThematicMainForm.CheckBoxNoColorsClick(Sender: TObject);
begin
     Grid.Repaint;
end;

procedure TThematicMainForm.SaveGroupsToTxt;
var
AList: TStringList;
AFileName: String;
i: Integer;
s: String;
begin
        AFileName:=OSInfo.DocDataDir + THEMDIR + 'Thematic.txt';

        AList:=TStringList.Create;

        s:='Fillcolor|Fillstyle|Linecolor|Linestyle|LineWidth|StartValue|EndValue|Class|Count|Active';
        AList.Add(s);

        try
                for i:=0 to Grid.RowCount-1 do begin
                        s:='';

                        if CheckBoxFillStyle.Checked then begin
                                s:=s + IntToStr(Groups[i].FillColor) + '|' + IntToStr(Groups[i].FillStyle) + '|';
                        end
                        else begin
                                s:=s + '-1|-1|';
                        end;

                        if CheckBoxLineStyle.Checked then begin
                                s:=s + IntToStr(Groups[i].LineColor) + '|' + IntToStr(Groups[i].LineStyle) + '|' + IntToStr(Groups[i].LineWidth) + '|';
                        end
                        else begin
                                s:=s + '-1|-1|-1|';
                        end;

                        s:=s + FloatToStr(Groups[i].StartVal) + '|';

                        s:=s + FloatToStr(Groups[i].EndVal) + '|';

                        s:=s + Groups[i].Cls + '|';

                        s:=s + IntToStr(Groups[i].Count) + '|';

                        s:=s + IntToStr(Ord(Groups[i].Active));

                        AList.Add(s);
                end;

                AList.SaveToFile(AFileName);
        finally
                AList.Free;
        end;
end;

procedure TThematicMainForm.CheckBoxUseSchemaClick(Sender: TObject);
begin
        if not CheckBoxUseSchema.Checked then begin
                ComboBoxSchemas.ItemIndex:=-1;
        end;
end;

function TThematicMainForm.DoApplyMap: Boolean;
var
AList: TStringList;
FileName: String;
s: String;
begin
        Result:=False;

        MapsDlg:=TMapsDlg.Create(Self);
        try
                MapsDlg.Caption:=LangText(72);
                MapsDlg.ButtonOk.Caption:=LangText(73);
                MapsDlg.ButtonCancel.Caption:=LangText(74);

                if LoadTMFiles then begin
                        if MapsDlg.ShowModal = mrOK then begin

                                AList:=TStringList.Create;
                                try
                                        FileName:=OSInfo.DocDataDir + THEMDIR + MapsDlg.LbMaps.Items[MapsDlg.LbMaps.ItemIndex] + TMFILEEXT;
                                        AList.LoadFromFile(FileName);

                                        s:=AList.Values['ADOConnection'];
                                        ADOConnection.ConnectionString:=s;

                                        s:=AList.Values['ADODataset'];
                                        ADODataset.CommandText:=s;

                                        if RefreshDS then begin

                                                Show;
                                                Application.ProcessMessages;

                                                s:=AList.Values['ProgisIdFieldIndex'];
                                                ProgisIdFieldIndex:=StrToInt(s);

                                                s:=AList.Values['ThematicFieldIndex'];
                                                ThematicFieldIndex:=StrToInt(s);

                                                ComboBoxFields.ItemIndex := ThematicFieldIndex;
                                                ComboBoxFieldsChange (nil);

                                                s:=AList.Values['Schema'];
                                                LoadSchema(s);

                                                s:=AList.Values['Grouping'];
                                                if (IsNumeric) and (s <> '') then begin
                                                        SetGrouping(s);
                                                end;
                                                ButtonCreateClick(nil);
                                        end;
                                finally
                                        AList.Free;
                                end;

                                Result:=True;
                        end;
                end;
        finally
                MapsDlg.Close;
                MapsDlg.Free;
                MapsDlg:=nil;

                Close;
        end;
end;

procedure TThematicMainForm.ButtonSaveClick(Sender: TObject);
var
TMName: String;
FileName: String;
AList: TStringList;
bOverwrite: Boolean;
begin
        AList:=TStringList.Create;
        try
                if InputQuery(LangText(69),LangText(70),TMName) and (Trim(TMName) <> '') then begin
                        TMName:=Trim(TMName);
                        TMName:=StringReplace(TMName,'/','',[rfReplaceAll]);
                        TMName:=StringReplace(TMName,'\','',[rfReplaceAll]);
                        TMName:=StringReplace(TMName,'*','',[rfReplaceAll]);
                        TMName:=StringReplace(TMName,'?','',[rfReplaceAll]);
                        TMName:=StringReplace(TMName,'"','',[rfReplaceAll]);
                        TMName:=StringReplace(TMName,'<','',[rfReplaceAll]);
                        TMName:=StringReplace(TMName,'>','',[rfReplaceAll]);
                        TMName:=StringReplace(TMName,'|','',[rfReplaceAll]);

                        FileName:=OSInfo.DocDataDir + THEMDIR + TMName + TMFILEEXT;

                        AList.Add('ADOConnection=' + ADOConnection.ConnectionString);
                        AList.Add('ADODataset=' + ADODataset.CommandText);
                        AList.Add('ProgisIdFieldIndex=' + IntToStr(ProgisIdFieldIndex));
                        AList.Add('ThematicFieldIndex=' + IntToStr(ThematicFieldIndex));
                        AList.Add('Schema=' + ComboBoxSchemas.Items[ComboBoxSchemas.ItemIndex]);
                        AList.Add('Grouping=' + ComboBoxMethod.Items[ComboBoxMethod.ItemIndex]);

                        bOverwrite:=True;

                        if FileExists(FileName) then begin
                                if MessageBox(Handle,PChar(Format(LangText(71),[FileName])),PChar(Caption),MB_ICONQUESTION or MB_APPLMODAL or MB_YESNO) <> ID_YES then begin
                                        bOverwrite:=False;
                                end;
                        end;

                        if bOverwrite then begin
                                AList.SaveToFile(FileName);
                        end;
                end;
        finally
                AList.Free;
        end;
end;

procedure TThematicMainForm.ButtonTestClick(Sender: TObject);
begin
        DoApplyMap;
end;

end.


