unit LayerForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TLayersDlg = class(TForm)
    LabelLayer: TLabel;
    ComboBoxLayers: TComboBox;
    ButtonOk: TButton;
    ButtonCancel: TButton;
    CheckBoxSepLayers: TCheckBox;
    CheckBoxLegendOnOff: TCheckBox;
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  LayersDlg: TLayersDlg;

implementation

{$R *.DFM}

end.
