unit MapsForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TMapsDlg = class(TForm)
    ButtonOk: TButton;
    ButtonCancel: TButton;
    LbMaps: TListBox;
    procedure LbMapsClick(Sender: TObject);
    procedure LbMapsDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MapsDlg: TMapsDlg;

implementation

{$R *.dfm}

procedure TMapsDlg.LbMapsClick(Sender: TObject);
begin
        if LBMaps.ItemIndex > -1 then begin
                ButtonOk.Enabled:=True;
        end;
end;

procedure TMapsDlg.LbMapsDblClick(Sender: TObject);
begin
        ModalResult:=mrOK;
end;

end.
