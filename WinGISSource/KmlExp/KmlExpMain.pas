unit KmlExpMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  WINGIS_TLB, StdCtrls, OleCtrls, AxGisPro_TLB, ComCtrls, Dialogs, IniFiles,
  WinOSInfo, Math;

type
  TGisPoint = record
        X,Y,Z: Double;
  end;

type
  TKMLForm = class(TForm)
    GisPro: TAxGisProjection;
    Pb: TProgressBar;
    Sb: TStaticText;
    SaveDialog: TSaveDialog;
  public
    Doc: IDocument;
    KML: TStringList;
    Ini: TIniFile;
    AreaFact: Integer;
    procedure CheckLine(var s: String);     
    procedure Line(s: String); overload;
    procedure Line(s: String; Args: Array of Const); overload;
    procedure SetProjection;
    function ConvertPoint(APoint: IPoint): TGisPoint;
    procedure DoProject;
    procedure DoLayer(ALayer: ILayer);
    procedure DoObject(ABase: IBase; ALayer: ILayer);
    procedure DoPolyline(APolyline: IPolyline);
    procedure DoPolygon(APolygon: IPolygon);
    procedure DoPixel(APixel: IPixel);
    procedure DoSymbol(ASymbol: ISymbol);
    procedure DoText(AText: IText);
  end;

var
  KMLForm: TKMLForm;

const
  ot_Pixel              = 1;
  ot_Poly               = 2;
  ot_CPoly              = 4;
  ot_Text               = 7;
  ot_Symbol             = 8;
  ot_Image              = 10;
  ot_Circle             = 12;
  ot_Arc                = 13;

const
  SupportedObjectTypes = [ot_Poly,ot_CPoly,ot_Pixel,ot_Symbol,ot_Text];

const
  wtProjectUnits        = 0;
  wtScaleIndependent    = 1;
  wtUnknownUnits        = 2;

  stProjectUnits        = 0;
  stScaleIndependent    = 1;
  stDefaultSize         = 2;

  FillStyleMap : Array[0..7] of TBrushStyle = (bsClear,bsFDiagonal,bsCross,bsDiagCross,bsBDiagonal,bsHorizontal,bsVertical,bsSolid);

  dblTolerance    = 1E-10;

implementation

{$R *.dfm}

procedure TKMLForm.CheckLine(var s: String);
begin
        s:=StringReplace(s,'�','ae',[rfReplaceAll]);
        s:=StringReplace(s,'�','Ae',[rfReplaceAll]);

        s:=StringReplace(s,'�','oe',[rfReplaceAll]);
        s:=StringReplace(s,'�','Oe',[rfReplaceAll]);

        s:=StringReplace(s,'�','ue',[rfReplaceAll]);
        s:=StringReplace(s,'�','Ue',[rfReplaceAll]);

        s:=StringReplace(s,'�','ss',[rfReplaceAll]);

        s:=StringReplace(s,'&','-',[rfReplaceAll]);
end;

procedure TKMLForm.Line(s: String);
begin
        CheckLine(s);
        KML.Add(s);
end;

procedure TKMLForm.Line(s: String; Args: Array of Const);
begin
        s:=Format(s,Args);
        CheckLine(s);
        KML.Add(s);
end;

procedure TKMLForm.SetProjection;
begin
   GisPro.WorkingPath:=OSInfo.WingisDir;

   GisPro.SourceCoordinateSystem:=Doc.Projection;
   GisPro.SourceProjSettings:=Doc.ProjectProjSettings;
   GisPro.SourceProjection:=Doc.ProjectProjection;
   GisPro.SourceDate:=Doc.ProjectDate;
   GisPro.SourceXOffset:=Doc.ProjectionXOffset;
   GisPro.SourceYOffset:=Doc.ProjectionYOffset;
   GisPro.SourceScale:=Doc.ProjectionScale;

   GisPro.TargetCoordinateSystem:=0;
   GisPro.TargetProjSettings:='Geodetic Phi/Lamda';
   GisPro.TargetProjection:='Phi/Lamda values';
   GisPro.TargetDate:='WGS84';
   GisPro.TargetXOffset:=0;
   GisPro.TargetYOffset:=0;
   GisPro.TargetScale:=1;

   GisPro.ShowOnlySource:=False;
   GisPro.LngCode:='044';

   if Doc.ProjectProjection = 'NONE' then begin
        GisPro.SetupByDialog;
   end;

end;

function TKMLForm.ConvertPoint(APoint: IPoint): TGisPoint;
var
DX,DY: Double;
begin
        DX:=APoint.x;
        DY:=APoint.y;

        DX:=DX/100;
        DY:=DY/100;

        GisPro.Calculate(DX,DY);

        Result.X:=DX;
        Result.Y:=DY;
        Result.Z:=0;
end;

procedure TKMLForm.DoProject;
var
i: Integer;
KMLFilename: String;
begin
        DecimalSeparator:='.';

        AreaFact:=1;
        Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
        try
                AreaFact:=Ini.ReadInteger('SETTINGS','AreaDivider',1);
        finally
                Ini.Free;
        end;

        if Doc <> nil then begin

                SetProjection;

                KML:=TStringList.Create;
                try
                        Line('<?xml version="1.0" encoding="UTF-8"?>');
                        Line('<kml xmlns="http://earth.google.com/kml/2.0">');
                        Line('<Folder>');
                        Line('<name>%s</name>',[ChangeFileExt(ExtractFileName(KMLFilename),'')]);

                        for i:=0 to Doc.Layers.Count-1 do begin
                                DoLayer(Doc.Layers.Items[i]);
                        end;

                        Line('</Folder>');
                        Line('</kml>');

                        KMLFilename:=SaveDialog.FileName;

                        KML.SaveToFile(KMLFilename);
                        KML.Clear;
                finally
                        KML.Free;
                end;

        end;
end;

procedure TKMLForm.DoLayer(ALayer: ILayer);
var
i,n,Step: Integer;
AnyObject: Boolean;
begin
        AnyObject:=False;
        n:=ALayer.Count;
        for i:=0 to n-1 do begin
                if ALayer.Items[i].ObjectType in SupportedObjectTypes then begin
                        AnyObject:=True;
                        break;
                end;
        end;

        if (ALayer.Visible) and (AnyObject) then begin

                Sb.Caption:='Layer: ' + ALayer.LayerName;

                Line('<Folder>');
                Line('<name>%s</name>',[ALayer.LayerName]);

                n:=ALayer.Count;
                for i:=0 to n-1 do begin

                        Step:=((i+1)*100) div n;
                        if Step <> Pb.Position then begin
                                Pb.Position:=Step;
                                Application.ProcessMessages;
                        end;

                        DoObject(ALayer.Items[i],ALayer);
                end;

                Line('</Folder>');

                Pb.Position:=0;
                Sb.Caption:='';
        end;
end;

procedure TKMLForm.DoObject(ABase: IBase; ALayer: ILayer);
begin
        if ABase.ObjectType in SupportedObjectTypes then begin

                Line('<Placemark>');

                Line('<Style>');
                if ABase.ObjectType = ot_CPoly then begin

                        if ABase.OtherStyle then begin
                                Line('<LineStyle>');
                                Line('<color>FF%s</color>',[IntToHex(ABase.ObjectStyle.LineStyle.Color,6)]);
                                Line('<width>%d</width>',[Trunc(Max(ABase.ObjectStyle.LineStyle.Width/10,1))]);
                                Line('</LineStyle>');
                                Line('<PolyStyle>');
                                if ALayer.FillStyle.Pattern = 0 then begin
                                        Line('<color>00FFFFFF</color>');
                                        Line('<fill>0</fill>');
                                end
                                else if ALayer.FillStyle.Pattern = 7 then begin
                                        Line('<color>7F%s</color>',[IntToHex(ALayer.FillStyle.ForeColor,6)]);
                                end
                                else begin
                                        Line('<color>40%s</color>',[IntToHex(ALayer.FillStyle.ForeColor,6)]);
                                end;
                                Line('</PolyStyle>');
                        end
                        else begin
                                Line('<LineStyle>');
                                Line('<color>FF%s</color>',[IntToHex(ALayer.LineStyle.Color,6)]);
                                Line('<width>%d</width>',[Trunc(Max(ALayer.LineStyle.Width/10,1))]);
                                Line('</LineStyle>');
                                Line('<PolyStyle>');
                                if ALayer.FillStyle.Pattern = 0 then begin
                                        Line('<color>00FFFFFF</color>');
                                        Line('<fill>0</fill>');
                                end
                                else if ALayer.FillStyle.Pattern = 7 then begin
                                        Line('<color>7F%s</color>',[IntToHex(ALayer.FillStyle.ForeColor,6)]);
                                end
                                else begin
                                        Line('<color>40%s</color>',[IntToHex(ALayer.FillStyle.ForeColor,6)]);
                                end;
                                Line('</PolyStyle>');
                        end;
                end
                else begin
                        if ABase.OtherStyle then begin
                                Line('<geomColor>FF%s</geomColor>',[IntToHex(ABase.ObjectStyle.LineStyle.Color,6)]);
                        end
                        else begin
                                Line('<geomColor>FF%s</geomColor>',[IntToHex(ALayer.LineStyle.Color,6)]);
                        end;
                end;

                if ABase.ObjectType = ot_Text then begin
                        Line('<IconStyle>');
			Line('<scale>0.5</scale>');
                        if ABase.OtherStyle then begin
                                Line('<color>000000ff</color>');
                                //Line('<color>FF%s</color>',[IntToHex(ABase.ObjectStyle.LineStyle.Color,6)]);
                        end
                        else begin
                                Line('<color>000000ff</color>');
                                //Line('<color>FF%s</color>',[IntToHex(ALayer.LineStyle.Color,6)]);
                        end;
		        Line('</IconStyle>');
		        Line('<LabelStyle>');
                        if ABase.OtherStyle then begin
                                Line('<color>FF%s</color>',[IntToHex(ABase.ObjectStyle.LineStyle.Color,6)]);
                        end
                        else begin
                                Line('<color>FF%s</color>',[IntToHex(ALayer.LineStyle.Color,6)]);
                        end;
		        Line('</LabelStyle>');
                end;

                if ABase.ObjectType = ot_Pixel then begin
		        Line('<LabelStyle>');
			Line('<scale>0</scale>');
		        Line('</LabelStyle>');
                end;

                if ABase.ObjectType = ot_Poly then begin
                        Line('<LineStyle>');
                        if ABase.OtherStyle then begin
                                Line('<color>FF%s</color>',[IntToHex(ABase.ObjectStyle.LineStyle.Color,6)]);
                        end
                        else begin
                                Line('<color>FF%s</color>',[IntToHex(ALayer.LineStyle.Color,6)]);
                        end;
                        if ABase.OtherStyle then begin
                                Line('<width>%d</width>',[Trunc(Max(ABase.ObjectStyle.LineStyle.Width/10,1))]);
                        end
                        else begin
                                Line('<width>%d</width>',[Trunc(Max(ALayer.LineStyle.Width/10,1))]);
                        end;
                        Line('</LineStyle>');
                end;

                Line('</Style>');

                case ABase.ObjectType of
                        ot_Pixel: DoPixel(ABase as IPixel);
                        ot_Symbol: DoSymbol(ABase as ISymbol);
                        ot_Poly: DoPolyline(ABase as IPolyline);
                        ot_CPoly: DoPolygon(ABase as IPolygon);
                        ot_Text: DoText(ABase as IText);
                end;

                Line('</Placemark>');
        end;
end;

procedure TKMLForm.DoPolyline(APolyline: IPolyline);
var
i,n: Integer;
APoint: IPoint;
GP: TGisPoint;
s,u: AnsiString;
d: Double;
begin
        Line('<LineString>');
        Line('<coordinates>');

        s:='';
        n:=APolyline.Count;
        for i:=0 to n-1 do begin
                APoint:=APolyline.Points[i];
                GP:=ConvertPoint(APoint);
                s:=s + FloatToStr(GP.X) + ',' + FloatToStr(GP.Y) + ',' + FloatToStr(GP.Z) + ' ';
        end;

        Line(s);

        Line('</coordinates>');
        Line('<tesselate>1</tesselate>');
        Line('</LineString>');

        u:='m';
        d:=APolyline.Length;
        if d >= 5000 then begin
                d:=d/1000;
                u:='km';
        end;

        //Line('<name>%.2f %s</name>',[d,u]);

        Line('<name>%d</name>',[APolyline.Index]);
end;

procedure TKMLForm.DoPolygon(APolygon: IPolygon);
var
i,Start,Stop: Integer;
APoint: IPoint;
GP: TGisPoint;
s,u: AnsiString;
a: Double;

        Procedure DoPoints(AStart, AStop: Integer; AIsland: Boolean);
        var
        k: Integer;
        begin
                if AIsland then begin
                        Line('<innerBoundaryIs>');
                end
                else begin
                        Line('<outerBoundaryIs>');
                end;

                s:='';

                Line('<LinearRing>');
                Line('<coordinates>');
                for k:=AStart to AStop do begin
                        APoint:=APolygon.Points[k];
                        GP:=ConvertPoint(APoint);
                        s:=s + FloatToStr(GP.X) + ',' + FloatToStr(GP.Y) + ',' + FloatToStr(GP.Z) + ' ';
                end;
                Line(s);

                Line('</coordinates>');
                Line('</LinearRing>');

                if AIsland then begin
                        Line('</innerBoundaryIs>');
                end
                else begin
                        Line('</outerBoundaryIs>');
                end;
        end;

begin
        Line('<Polygon>');
        if APolygon.IslandArea then begin

                Line('<extrude>1</extrude>');

                Start:=APolygon.IslandInfo[0];
                DoPoints(0,Start-1,False);
                for i:=1 to APolygon.IslandCount-1 do begin
                        Stop:=Start+APolygon.IslandInfo[i];
                        DoPoints(Start,Stop-1,True);
                        Start:=Start + APolygon.IslandInfo[i] + 1;
                end;
        end
        else begin
                DoPoints(0,APolygon.Count-1,False);
        end;

        Line('<tesselate>1</tesselate>');
        Line('</Polygon>');

        if AreaFact = 10000 then begin
                u:='ha';
        end
        else if AreaFact = 1000000 then begin
                u:='km�';
        end
        else begin
                u:='m�';
        end;

        //a:=APolygon.Area;
        //Line('<name>%.2f %s</name>',[a,u]);

        Line('<name>%d</name>',[APolygon.Index]);
end;

procedure TKMLForm.DoPixel(APixel: IPixel);
var
APoint: IPoint;
GP: TGisPoint;
s: String;
AText: String;
begin
        Line('<Point>');
        Line('<coordinates>');

        APoint:=APixel.Position;
        GP:=ConvertPoint(APoint);

        s:=FloatToStr(GP.X) + ',' + FloatToStr(GP.Y) + ',' + FloatToStr(GP.Z);
        Line(s);

        Line('</coordinates>');
        Line('</Point>');

        AText:=Doc.GetAttachedText(APixel.Index);

        if AText = '' then begin
                //Line('<name>%.6f / %.6f</name>',[GP.X,GP.Y]);
                Line('<name>%d</name>',[APixel.Index]);
        end
        else begin
                Line('<name>%s</name>',[AText]);
        end;

end;

procedure TKMLForm.DoSymbol(ASymbol: ISymbol);
var
APoint: IPoint;
GP: TGisPoint;
s: String;
begin
        Line('<Point>');
        Line('<coordinates>');

        APoint:=ASymbol.Position;
        GP:=ConvertPoint(APoint);

        s:=FloatToStr(GP.X) + ',' + FloatToStr(GP.Y) + ',' + FloatToStr(GP.Z);
        Line(s);

        Line('</coordinates>');
        Line('</Point>');

        Line('<name>%s</name>',[ASymbol.SymbolName]);
end;

procedure TKMLForm.DoText(AText: IText);
var
APoint: IPoint;
GP: TGisPoint;
s: String;
begin
        Line('<Point>');
        Line('<coordinates>');

        APoint:=AText.Position;
        GP:=ConvertPoint(APoint);

        s:=FloatToStr(GP.X) + ',' + FloatToStr(GP.Y) + ',' + FloatToStr(GP.Z);
        Line(s);

        Line('</coordinates>');

        Line('</Point>');

        Line('<name>%s</name>',[AText.Text]);
end;

end.
