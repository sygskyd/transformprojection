library KmlExp;

uses
  Forms,
  SysUtils,
  WINGIS_TLB,
  KmlExpMain;

{$R *.res}

function ExportKml(AHandle: Integer; ADoc: Variant): PChar; stdcall;
begin
        Result:='';

        Application.Handle:=AHandle;
        KMLForm:=TKMLForm.Create(Application);
        try
                KMLForm.Doc:=IUnknown(ADoc) as IDocument;
                if KMLForm.Doc <> nil then begin
                        KMLForm.SaveDialog.FileName:=ChangeFileExt(KMLForm.Doc.Name,'.kml');
                        if KMLForm.SaveDialog.Execute then begin
                                KMLForm.Show;
                                KMLForm.GisPro.Visible:=False;
                                KMLForm.DoProject;
                                KMLForm.Close;
                                Result:=PChar(KMLForm.SaveDialog.FileName);
                        end;
                end;
        finally
                Application.Handle:=0;
                KMLForm.Free;
        end;
end;

function ExportKmlWithoutDlg(AHandle: Integer; ADoc: Variant): HRESULT; stdcall;
begin
        Result:=S_FALSE;

        Application.Handle:=AHandle;
        KMLForm:=TKMLForm.Create(Application);
        try
                KMLForm.Doc:=IUnknown(ADoc) as IDocument;
                if KMLForm.Doc <> nil then begin
                        KMLForm.SaveDialog.FileName:=ChangeFileExt(KMLForm.Doc.Name,'.kml');
                        KMLForm.Show;
                        KMLForm.GisPro.Visible:=False;
                        KMLForm.DoProject;
                        KMLForm.Close;
                        Result:=S_OK;
                end;
        finally
                Application.Handle:=0;
                KMLForm.Free;
        end;
end;

exports ExportKml, ExportKmlWithoutDlg;

begin
end.
