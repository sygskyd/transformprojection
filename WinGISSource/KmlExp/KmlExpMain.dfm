object KMLForm: TKMLForm
  Left = 750
  Top = 157
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'WinGIS KML Export'
  ClientHeight = 56
  ClientWidth = 241
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Pb: TProgressBar
    Left = 8
    Top = 32
    Width = 225
    Height = 16
    Min = 0
    Max = 100
    Smooth = True
    TabOrder = 1
  end
  object Sb: TStaticText
    Left = 8
    Top = 8
    Width = 225
    Height = 17
    AutoSize = False
    BorderStyle = sbsSunken
    TabOrder = 2
  end
  object GisPro: TAxGisProjection
    Left = 16
    Top = 16
    Width = 50
    Height = 25
    ParentFont = False
    TabOrder = 0
    ControlData = {
      54504630065450616E656C00044C656674021003546F70021005576964746802
      320648656967687402190743617074696F6E060647697350726F0000}
  end
  object SaveDialog: TSaveDialog
    DefaultExt = '.kml'
    Filter = 'KML Files|*.kml|All Files|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 80
    Top = 16
  end
end
