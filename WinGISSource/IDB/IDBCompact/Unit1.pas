unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    CloseButton: TButton;
    Label1: TLabel;
    Panel3: TPanel;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    FileNamePanel: TPanel;
    Panel5: TPanel;
    Memo1: TMemo;
    StartButton: TButton;
    OD: TOpenDialog;
    FileNameLabel: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure StartButtonClick(Sender: TObject);
    procedure CloseButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
    bIAmWorking: Boolean;
    Procedure Report(sMessage: AnsiString);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

Uses 
     JRO_TLB;

Procedure TForm1.Report(sMessage: AnsiString);
Begin
     Memo1.Lines.Add(sMessage);
End;

procedure TForm1.SpeedButton1Click(Sender: TObject);
begin
     If OD.Execute Then Begin
        If FileExists(OD.FileName) Then Begin
           FileNameLabel.Caption := OD.FileName;
           StartButton.Enabled := TRUE;
           Report('');
           Report('You have selected file: ''' + OD.FileName + '''');
           End;
        End
     Else Begin
        FileNameLabel.Caption := '';
        StartButton.Enabled := FALSE;
        End;
end;

procedure TForm1.FormCreate(Sender: TObject);
Var
   AnEngine: IJetEngine;
begin
     bIAmWorking := FALSE;
     
     Memo1.Lines.Clear;
     Memo1.Lines.Add('WinGIS Internal database file packer is ready to work.');
     StartButton.Enabled := FALSE;
     FileNameLabel.Caption := '';

     AnEngine := CoJetEngine.Create;
     If AnEngine = NIL Then
        Report('Packing engine cannot be found!');
end;

procedure TForm1.StartButtonClick(Sender: TObject);
Var
   AnEngine: IJetEngine;
   sSourceConn,
   sDestinationConn,
   sDBFileName,
   sTempFileName: AnsiString;
   ABuffer: array[0..1023] of Char;
begin
     AnEngine := CoJetEngine.Create;

     sDBFileName := FileNameLabel.Caption;
     sSourceConn := 'Provider=Microsoft.Jet.OLEDB.4.0;' +
                    'Data Source=' + sDBFileName + ';';

     SetString(sTempFileName, ABuffer, GetTempPath(SizeOf(ABuffer), ABuffer));
     sTempFileName := sTempFileName + 'WG_IDB_TEMP.wgi';
     If FileExists(sTempFileName) Then
        If NOT DeleteFile(sTempFileName) Then Begin
           Report('Temp file cannot be created!');
           EXIT;
           End;

     sDestinationConn := 'Provider=Microsoft.Jet.OLEDB.4.0;' +
                         'Data Source=' + sTempFileName + ';';

     Report('Packing has started. Please wait. It can take some minutes...');

     If AnEngine <> NIL Then Begin
        bIAmWorking := TRUE;
        TRY
           AnEngine.CompactDatabase(sSourceConn, sDestinationConn);
        FINALLY
           bIAmWorking := FALSE;
        END;
        Report('Packing has complited successfully');

        DeleteFile(sDBFileName);
        MoveFile(PChar(sTempFileName), PChar(FileNameLabel.Caption));
        DeleteFile(sTempFileName);
        End
     Else
        Report('Packing engine cannot be found!');
end;

procedure TForm1.CloseButtonClick(Sender: TObject);
begin
     SELF.Close;
end;

procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     If bIAmWorking Then Begin
        If MessageBox(Handle, 'The packing is proceeding. Do you want to interrupt it and exit?', 'WinGIS', MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrNO Then
           CanClose := FALSE;
        End;
end;

end.
