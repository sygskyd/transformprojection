unit IDB_Progress;
{
Internal database. Ver. II.
This module is purposed to show done percent of any operation in IDB..

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 25-05-2001
}
interface

uses
  Windows, Messages, Classes, Graphics, Controls, Forms, Dialogs,
  DTables, Db, DDB, DMaster, StdCtrls, Gauges,
  IDB_Access2DB, IDB_StructMan, ExtCtrls;

type
  TIDB_ProgressForm = class(TForm)
    BottomPanel: TPanel;
    Panel2: TPanel;
    StatusLabel: TLabel;
    GlobalGauge: TGauge;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    bUserWantsToCancel: Boolean;
  public
    { Public declarations }
    Procedure ShowTheBottomPanel;
    Property UserWantsToCancel: Boolean Read bUserWantsToCancel;
  end;

implementation
{$R *.DFM}
Uses
     IDB_LanguageManager, IDB_Consts;

procedure TIDB_ProgressForm.FormCreate(Sender: TObject);
begin
     GlobalGauge.Progress := 0;
     bUserWantsToCancel := FALSE;
     ALangManager.MakeReplacement(SELF);
     SELF.Height := SELF.Height - BottomPanel.Height;
end;

procedure TIDB_ProgressForm.Button1Click(Sender: TObject);
begin
     // 306=Do You want to cancel this operation?
     If MessageBox(Handle, PChar(ALangManager.GetValue(306)), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrNo Then EXIT;
     bUserWantsToCancel := TRUE;     
end;

Procedure TIDB_ProgressForm.ShowTheBottomPanel;
Begin
     SELF.Height := SELF.Height + BottomPanel.Height;
     BottomPanel.Visible := TRUE;
End;

end.
