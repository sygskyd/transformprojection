unit IDB_WG_Func;
{
Internal Database. Ver II.
Classes that supports WinGis functionality (the annotations producing,
the charts producing).

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 07-06-2000

Corrections:
      31-07-2000 - Function TIDB_AnnotMaker.MakeAnnotationInProject by Ivanoff.
}

{
10.05.2000 - Ivanoff.
Now I finished the annotatiion maker only.
And I made a half the DBCharts maker. (It works without inserting in the project.
The inserting will be make later.)
}
interface

uses Objects, SysUtils, Windows;

type
    { This declaration was taken from AM_Def.pas }
   PAnnot = ^TAnnot;
   TAnnot = record
      LineName: PChar;
      LineValue: PChar;
      HasValue: Boolean;
   end;

   PChartRec = ^TChartRec;
   TChartRec = record
      LineName: PChar;
      LineValue: PChar;
      HasValue: Boolean;
   end;

   TIDB_WG_Func = class
   private
      AProj: Pointer;
   public
      constructor Create (AProject: Pointer);
   end;

   TIDB_AnnotMaker = class (TIDB_WG_Func)
   private
      AnAnnotData: PCollection;
   public
      constructor Create (AProject: Pointer);
      destructor Free;
      function Find (sFieldName: AnsiString): Integer;
      procedure AddInAnnotData (sFieldName: AnsiString);
      procedure ClearAnnotData;
      procedure DeleteAnnotData;
      procedure CorrectAnnotData (sFieldName, sAnnotValue: AnsiString);
      function AnnotationSettingsDialogExecute (ACurrentLayer: Pointer): Boolean;
      function MakeAnnotationInProject (AnObjectLayer: Pointer; iItemID: Integer): Boolean;
// ++ Cadmensky version 2.3.5
      function GetAnnotationFieldNames: AnsiString;
// -- Cadmensky version 2.3.5
   end;

   TIDB_ChartMaker = class (TIDB_WG_Func)
   private
      AChartData: PCollection;
   public
      constructor Create (AProject: Pointer);
      destructor Free;
      function Find (sFieldName: AnsiString): Integer;
      procedure AddInChartData (sFieldName: AnsiString);
      procedure ClearChartData;
      procedure DeleteChartData;
      procedure CorrectChartData (sFieldName, sChartValue: AnsiString);
      function ChartSettingsDialogExecute (ACurrentLayer: Pointer): Boolean;
      function MakeChartInProject (AnObjectLayer: Pointer; iItemID: Integer): Boolean;
       {brovak}
      procedure MakeSpecialChart (AProject, Alayer: Pointer);
      procedure SendSpecialforChart (AProject: Pointer; PseudoDDEString: Ansistring);
       {brovak}
   end;

implementation

uses
   Classes, IDB_CallbacksDef;

constructor TIDB_WG_Func.Create (AProject: Pointer);
begin
   SELF.AProj := AProject;
end;

/////////  A N N O T A T I O N     M A K E R  /////////////////////////////////

constructor TIDB_AnnotMaker.Create (AProject: Pointer);
begin
   inherited Create (AProject);
   AnAnnotData := New (PCollection, Init (5, 5));
end;

destructor TIDB_AnnotMaker.Free;
begin
   SELF.DeleteAnnotData;
   Dispose (AnAnnotData, Done);
end;

function TIDB_AnnotMaker.Find (sFieldName: AnsiString): Integer;
var
   iI: Integer;
   AnAnnotRec: PAnnot;
begin
   RESULT := -1;
   for iI := 0 to SELF.AnAnnotData.Count - 1 do
   begin
      AnAnnotRec := AnAnnotData.At (iI);
      if AnAnnotRec.LineName = sFieldName then
      begin
         RESULT := iI;
         EXIT;
      end;
   end;
end;

procedure TIDB_AnnotMaker.AddInAnnotData (sFieldName: AnsiString);
var
   iI: Integer;
   AnAnnotRec: PAnnot;
begin
   iI := SELF.Find (sFieldName);
   if iI > -1 then EXIT; // Already exists...
   AnAnnotRec := New (PAnnot);
   AnAnnotRec.LineName := StrNew (PChar (sFieldName));
   AnAnnotRec.LineValue := nil;
   AnAnnotRec.HasValue := FALSE;
   AnAnnotData.Insert (AnAnnotRec);
end;

procedure TIDB_AnnotMaker.ClearAnnotData;
var
   iI: Integer;
   AnAnnotRec: PAnnot;
begin
   for iI := 0 to AnAnnotData.Count - 1 do
   begin
      AnAnnotRec := AnAnnotData.At (iI);
// ++ Cadmensky Version 2.3.5
      StrDispose (AnAnnotRec.LineValue);
// -- Cadmensky Version 2.3.5
      AnAnnotRec.LineValue := nil;
      AnAnnotRec.HasValue := FALSE;
   end;
end;

procedure TIDB_AnnotMaker.DeleteAnnotData;
var
   i: integer;
   Annotation: PAnnot;
begin
   for i := 0 to AnAnnotData.Count - 1 do
   begin;
      Annotation := AnAnnotData.At (i);
      StrDispose (Annotation.LineName);
// ++ Cadmensky Version 2.3.5
      StrDispose (Annotation.LineValue);
// -- Cadmensky Version 2.3.5
      Dispose (Annotation);
   end;
   AnAnnotData.DeleteAll; // . DeleteAll;//   FreeAll;
//  for I := 0 to AnAnnotData.Count - 1 do AnAnnotData.Free(AnAnnotData.At(I));
 // Count := 0;
end;

procedure TIDB_AnnotMaker.CorrectAnnotData (sFieldName, sAnnotValue: AnsiString);
var
   iP: Integer;
   AnAnnotRec: PAnnot;
begin
   if sAnnotValue = '' then EXIT;
   iP := SELF.Find (sFieldName);
   if iP > -1 then
   begin
      AnAnnotRec := AnAnnotData.At (iP);
      StrDispose (AnAnnotRec.LineValue);
      AnAnnotRec.LineValue := StrNew (PChar (sAnnotValue));
      AnAnnotRec.HasValue := TRUE;
   end;
end;

function TIDB_AnnotMaker.AnnotationSettingsDialogExecute (ACurrentLayer: Pointer): Boolean;
begin
   RESULT := AnnotationSettingsDialogExecuteFunc (SELF.AProj, ACurrentLayer, AnAnnotData);
end;

function TIDB_AnnotMaker.MakeAnnotationInProject (AnObjectLayer: Pointer; iItemID: Integer): Boolean;
var
   iI: Integer;
   pcAnnotData: PChar;
   sAnnotData: AnsiString;
   AnAnnotRec: PAnnot;
begin
// ++ Cadmensky Version 2.3.6
   sAnnotData := '';
   for iI := 0 to SELF.AnAnnotData.Count - 1 do
   begin
      AnAnnotRec := SELF.AnAnnotData.At (iI);
      if AnAnnotRec.HasValue then
         sAnnotData := sAnnotData + AnAnnotRec.LineValue + #13;
   end;
   if sAnnotData <> '' then
      sAnnotData := Copy (sAnnotData, 1, Length (sAnnotData) - 1)
   else
      Exit;
   pcAnnotData := StrNew (PChar (sAnnotData));
//   RESULT := MakeAnnotationInProjectFunc (SELF.AProj, AnObjectLayer, iItemID, SELF.AnAnnotData);
   RESULT := MakeAnnotationInProjectFunc (SELF.AProj, AnObjectLayer, iItemID, pcAnnotData);
   StrDispose (pcAnnotData);
// ++ Cadmensky Version 2.3.6
end;

// ++ Cadmensky version 2.3.5
function TIDB_AnnotMaker.GetAnnotationFieldNames: AnsiString;
var
   slAnnotationFieldNames: TStringList;
   iI: Integer;
   AnAnnotRec: PAnnot;
begin
   slAnnotationFieldNames := TStringList.Create;
   for iI := 0 to SELF.AnAnnotData.Count - 1 do
   begin
      AnAnnotRec := SELF.AnAnnotData.At (iI);
      if AnAnnotRec.LineName <> '' then
         slAnnotationFieldNames.Add (AnAnnotRec.LineName);
   end;
   RESULT := slAnnotationFieldNames.Text;
   slAnnotationFieldNames.Free;
end;
// -- Cadmensky version 2.3.5

/////////  C H A R T S     M A K E R  /////////////////////////////////

constructor TIDB_ChartMaker.Create (AProject: Pointer);
begin
   inherited Create (AProject);
   AChartData := New (PCollection, Init (5, 5));
end;

destructor TIDB_ChartMaker.Free;
begin
   SELF.DeleteChartData;
   Dispose (AChartData, Done);
end;

function TIDB_ChartMaker.Find (sFieldName: AnsiString): Integer;
var
   iI: Integer;
   AChartRec: PChartRec;
begin
   RESULT := -1;
   for iI := 0 to SELF.AChartData.Count - 1 do
   begin
      AChartRec := AChartData.At (iI);
      if AChartRec.LineName = sFieldName then
      begin
         RESULT := iI;
         EXIT;
      end;
   end;
end;

procedure TIDB_ChartMaker.AddInChartData (sFieldName: AnsiString);
var
   iI: Integer;
   AChartRec: PChartRec;
begin
   iI := SELF.Find (sFieldName);
   if iI > -1 then EXIT; // Already exists...
   AChartRec := New (PChartRec);
   AChartRec.LineName := StrNew (PChar (sFieldName));
   AChartRec.LineValue := nil;
   AChartRec.HasValue := FALSE;
   AChartData.Insert (AChartRec);
end;

procedure TIDB_ChartMaker.ClearChartData;
var
   iI: Integer;
   AChartRec: PChartRec;
begin
   for iI := 0 to AChartData.Count - 1 do
   begin
      AChartRec := AChartData.At (iI);
// ++ Cadmensky Version 2.3.5
      StrDispose (AChartRec.LineValue);
// -- Cadmensky Version 2.3.5
      AChartRec.LineValue := nil;
      AChartRec.HasValue := FALSE;
   end;
end;

procedure TIDB_ChartMaker.DeleteChartData;
var
   iI: Integer;
   AChartRec: PChartRec;
begin
// ++ Cadmensky Version 2.3.5
   for iI := 0 to SELF.AChartData.Count - 1 do
   begin;
      AChartRec := AChartData.At (iI);
      StrDispose (AChartRec.LineName);
      StrDispose (AChartRec.LineValue);
      Dispose (AChartRec);
   end;
//   AnChartData.FreeAll;
   AChartData.DeleteAll;
// -- Cadmensky Version 2.3.5
end;

procedure TIDB_ChartMaker.CorrectChartData (sFieldName, sChartValue: AnsiString);
var
   iP: Integer;
   AChartRec: PChartRec;
begin
   if sChartValue = '' then EXIT;
   iP := SELF.Find (sFieldName);
   if iP > -1 then
   begin
      AChartRec := AChartData.At (iP);
      StrDispose (AChartRec.LineValue);
      AChartRec.LineValue := StrNew (PChar (sChartValue));
      AChartRec.HasValue := TRUE;
   end;
end;

function TIDB_ChartMaker.ChartSettingsDialogExecute (ACurrentLayer: Pointer): Boolean;
begin
   RESULT := ChartSettingsDialogExecuteFunc (SELF.AProj, ACurrentLayer, AChartData);
end;

function TIDB_ChartMaker.MakeChartInProject (AnObjectLayer: Pointer; iItemID: Integer): Boolean;
begin
   RESULT := MakeChartInProjectFunc (SELF.AProj, AnObjectLayer, iItemID, SELF.AChartData);
end;

{brovak}

procedure TIDB_ChartMaker.MakeSpecialChart (AProject, Alayer: Pointer);
begin
   MakeSpecialChartProc (AProject, Alayer);
end;

procedure TIDB_ChartMaker.SendSpecialforChart (AProject: Pointer; PseudoDDEString: Ansistring);
begin
   SendSpecialforChartProc (AProject, PseudoDDEString);
end;
//

{brovak}

end.

