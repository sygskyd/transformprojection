unit IDB_UserFieldProps;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, IDB_UserFieldsMan, IDB_ExternalTablesMan;

type
   TIDB_UserFieldPropsForm = class(TForm)
      Panel1: TPanel;
      cbAvailableFields: TComboBox;
      cbAvailableFields2: TComboBox;
      Label1: TLabel;
      Label2: TLabel;
      OKButton: TButton;
      Button2: TButton;
      Edit1: TEdit;
      Edit2: TEdit;
      CheckBox1: TCheckBox;
      CheckBox2: TCheckBox;
    Edit3: TEdit;
    Label3: TLabel;
      procedure OKButtonClick(Sender: TObject);
      procedure FormShow(Sender: TObject);
      procedure CheckBox1Click(Sender: TObject);
      procedure CheckBox2Click(Sender: TObject);
      procedure Edit1KeyPress(Sender: TObject; var Key: Char);
   private
      AProject: Pointer;
      AnUserFieldsMan: TIDB_UserFieldsManager;
      AnExtTablesMan: TIDB_ExternalTablesMan;
//      TempUserFieldsMan: TIDB_BaseUserFieldsManager;
      ALayer: Pointer;
      iItemIndex: integer;
   public
      constructor Create(AProject: Pointer; AnUserFieldsManager: TIDB_UserFieldsManager; {AnExtTablesManager: TIDB_ExternalTablesMan;} ALayer: Pointer; iItemIndex: integer);
  end;

implementation

uses IDB_LanguageManager, IDB_Consts, IDB_CallBacksDef, IDB_Utils, IDB_MainManager;

{$R *.DFM}

constructor TIDB_UserFieldPropsForm.Create (AProject: Pointer; AnUserFieldsManager: TIDB_UserFieldsManager; {AnExtTablesManager: TIDB_ExternalTablesMan;} ALayer: Pointer; iItemIndex: integer);
begin
   inherited Create (nil);
   SELF.AProject := AProject;
   Self.AnUserFieldsMan := AnUserFieldsManager;
   Self.AnExtTablesMan := AnExtTablesManager;
   Self.ALayer := ALayer;
   Self.iItemIndex := iItemIndex;

   ALangManager.MakeReplacement (Self);

end;

procedure TIDB_UserFieldPropsForm.FormShow(Sender: TObject);
var sFormula: AnsiString;
    sField1, sField2: AnsiString;
begin
   cbAvailableFields2.Items.Text := cbAvailableFields.Items.Text;

   sFormula := AnUserFieldsMan.GetUserCalcFieldFormula (iItemIndex);
   sField1 := Copy (sFormula, 1, Pos ('/', sFormula) - 1);
   try
      Edit1.Text := FloatToStr (StrToFloat (sField1));
      CheckBox1.Checked := true;
      Edit1.Visible := true;
      cbAvailableFields.Visible := false;
   except
      sField1 := Copy (sField1, Pos ('.', sField1) + 2, Length (sField1) - Pos ('.', sField1) - 2);
      cbAvailableFields.ItemIndex := cbAvailableFields.Items.IndexOf (sField1);
   end;
   if cbAvailableFields.ItemIndex = -1 then
      cbAvailableFields.ItemIndex := 0;

   sField2 := Copy (sFormula, Pos ('/', sFormula) + 1, Length (sFormula));
   try
      Edit2.Text := FloatToStr (StrToFloat (sField2));
      CheckBox2.Checked := true;
      Edit2.Visible := true;
      cbAvailableFields2.Visible := false;
   except
      sField2 := Copy (sField2, Pos ('.', sField2) + 2, Length (sField2) - Pos ('.', sField2) - 2);
      cbAvailableFields2.ItemIndex := cbAvailableFields2.Items.IndexOf (sField2);
   end;
   if cbAvailableFields2.ItemIndex = -1 then
      cbAvailableFields2.ItemIndex := 0;
   Edit3.Text := Self.AnUserFieldsMan.GetUserCalcFieldPrecision (Self.iItemIndex);
end;

procedure TIDB_UserFieldPropsForm.OKButtonClick(Sender: TObject);
var //sFormula: AnsiString;
    sLayerTableName, sLinkName: AnsiString;
    sField1, sField2: AnsiString;
    sExtTableName: AnsiString;
    iExtTableNumber: integer;
    dValue: Extended;
begin
   if not AnIDBMainMan.UseNewATTNames (AProject) then
      sLayerTableName := cs_AttributeTableNamePrefix + IntToStr(GetLayerIndexFunc(SELF.ALayer))
   else
      sLayerTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, SELF.ALayer);

   if Self.CheckBox1.Checked then
      try
         dValue := StrToFloat (Edit1.Text);
         sField1 := Edit1.Text;
      except
         sField1 := '1';
      end
   else
   begin
      if Pos ('_', cbAvailableFields.Text) > 1 then
         try
            iExtTableNumber := StrToInt (Copy (cbAvailableFields.Text, 1, Pos ('_', cbAvailableFields.Text) - 1));
            sLinkName := AnExtTablesMan.GetLinkName (iExtTableNumber - 1);
            if sLinkName <> '' then
            begin
               sExtTableName := Copy (cbAvailableFields.Text, Pos ('_', cbAvailableFields.Text) + 1, Length (cbAvailableFields.Text));
               sField1 := '[' + sLinkName + '].[' + sExtTableName + ']';
            end;
         except
         end;
      if sField1 = '' then
         sField1 := '[' + sLayerTableName + '].[' + cbAvailableFields.Text + ']';
   end;
   if Self.CheckBox2.Checked then
   begin
      try
         dValue := StrToFloat (Edit1.Text);
      except
         sField2 := '1';
      end;
      if dValue = 0 then
         sField2 := '1'
      else
         sField2 := Edit1.Text;
   end
   else
   begin
      if Pos ('_', cbAvailableFields2.Text) > 1 then
         try
            iExtTableNumber := StrToInt (Copy (cbAvailableFields2.Text, 1, Pos ('_', cbAvailableFields2.Text) - 1));
            sLinkName := AnExtTablesMan.GetLinkName (iExtTableNumber - 1);
            if sLinkName <> '' then
            begin
               sExtTableName := Copy (cbAvailableFields2.Text, Pos ('_', cbAvailableFields2.Text) + 1, Length (cbAvailableFields2.Text));
               sField2 := '[' + sLinkName + '].[' + sExtTableName + ']';
            end;
         except
         end;
         if sField2 = '' then
         sField2 := '[' + sLayerTableName + '].[' + cbAvailableFields2.Text + ']';
   end;
   Self.AnUserFieldsMan.SetUserCalcFieldFormula (Self.iItemIndex, sField1 + '/' + sField2);
   try
      dValue := StrToInt (Edit3.Text);
      if dValue <= 22 then
         Self.AnUserFieldsMan.SetUserCalcFieldPrecision (Self.iItemIndex, Edit3.Text)
      else
         Self.AnUserFieldsMan.SetUserCalcFieldPrecision (Self.iItemIndex, '22');
   except
      Self.AnUserFieldsMan.SetUserCalcFieldPrecision (Self.iItemIndex, '');
   end;
end;

procedure TIDB_UserFieldPropsForm.CheckBox1Click(Sender: TObject);
begin
   Edit1.Visible := CheckBox1.Checked;
   cbAvailableFields.Visible := not CheckBox1.Checked;
end;

procedure TIDB_UserFieldPropsForm.CheckBox2Click(Sender: TObject);
begin
   Edit2.Visible := CheckBox2.Checked;
   cbAvailableFields2.Visible := not CheckBox2.Checked;
end;

procedure TIDB_UserFieldPropsForm.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
   if ((Key < '0') or (Key > '9')) and (Key <> ',') then
      Key := #0;
end;



end.
