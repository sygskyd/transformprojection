unit IDB_DescMan;
{
Internal database. Ver. II.
Layer's description table manager.
This form is used for definition which fields from the attribute table are accordinable
to fields from descpription table.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 08-02-2000

GeoTextID field ALWAYS is 'ProgisID'.
}
interface

uses
    Classes, Controls, Forms, DB,
    DTables,
    IDB_Access2DB, IDB_StructMan;

type
  TParamsToBeRead = (pbrAll, pbrGeoText);

  TIDB_DescMan = class
    private
       AMode: TParamsToBeRead;
       IDA: TIDB_Access2DB_Level2;
       AStructMan: TIDB_StructMan;
       AWorkTable: TDTable;
       AWorkQuery: TDQuery;
       sAttribTableName,
       sOldDescTableName,
       sLayerIndex,
       sXCoordFieldName,
       sYCoordFieldName,
       sSymNumFieldName,
       sSymNameFieldName,
       sSymSizeFieldName,
//       sGeoTextIDFieldName,
       sGeoTextFieldName,
       sMonitoredFieldName: AnsiString;

       bXCoordFieldHasBeenChanged,
       bYCoordFieldHasBeenChanged,
       bSymNumFieldNameHasBeenChanged,
       bSymNameFieldNameHasBeenChanged,
       bSymSizeFieldNameHasBeenChanged,
       bGeoTextFieldNameHasBeenChanged,
       bMonitoredFieldNameHasBeenChanged,
       bAnnotationListHasBeenChanged,
       bChartListHasBeenChanged,
// ++ Cadmensky
       bOverlayListHasBeenChanged,
       bInfoListHasBeenChanged,
       bQueryListHasBeenChanged: Boolean;
// -- Cadmensky

       bUseSymName: Boolean;
       listFieldsOfAttribTable,
       listAnnotationFieldNames,
       listChartFieldNames,
// ++ Cadmensky
       listOverlayFieldNames,
       listInfoFieldNames,
       listQueryNames: TStringList;
       bNeedToFillInfoFields: boolean;
// -- Cadmensky

       AnOldScreenCursor: TCursor;

       function ReadAttribTableFieldsNames: Boolean;
       function ReadOldDescriptionTable: Boolean;
       procedure CheckAndCorrectDescTableStructure (sDescTableName: AnsiString);

       procedure SetScreenCursor;
       procedure RestoreScreenCursor;
    public
       constructor Create(AProject: Pointer; AIDB_DA: TIDB_Access2DB_Level2; ALayer: Pointer; AParametersMode: TParamsToBeRead = pbrAll);
       destructor Free;

       function ReadDescriptionTable(AParametersMode: TParamsToBeRead = pbrAll): Boolean;
       procedure SetUserChoice;

       function GetCountOfAnnotTableField: Integer;
       function GetFieldNameOfAnnotTable(iFieldPos: Integer): AnsiString;

       procedure CorrectSelectedFieldsList;

       function FieldIsNumberType(sFieldName: AnsiString): Boolean;

       function GetAnnotationFieldNames: AnsiString;
       function GetChartFieldNames: AnsiString;
       function GetXCoordFieldName: AnsiString;
       function GetYCoordFieldName: AnsiString;
       function GetSymNumFieldName: AnsiString;
       function GetSymNameFieldName: AnsiString;
       function GetSymSizeFieldName: AnsiString;
       function GetUseSymName: Boolean;
//       function GetGeoTextIDFieldName: AnsiString;
       function GetGeoTextFieldName: AnsiString;
       function GetMonitoredFieldName: AnsiString;
// ++ Cadmensky
       function GetOverlayFieldNames: AnsiString;
       function GetInfoFieldNames: AnsiString;

       function GetQueryNames: AnsiString;
       function GetQueryAsString (sQueryName: AnsiString): AnsiString;
// -- Cadmensky

       procedure SetAnnotationFieldNames(sAnnotFldNames: AnsiString);
       procedure SetChartFierldNames(sChartFldNames: AnsiString);
       procedure SetXCoordFieldName(sXCoordFldName: AnsiString);
       procedure SetYCoordFieldName(sYcoordFldName: AnsiString);
       procedure SetSymNumFieldName(sSymbolNumberFldName: AnsiString);
       procedure SetSymNameFieldName(sSymbolNameFldName: AnsiString);
       procedure SetSymSizeFieldName(sSymbolSizeFldName: AnsiString);
       procedure SetUseSymName(bUseSymbolName: Boolean);
//       procedure SetGeoTextIDFieldName(sGeoTextIDFldName: AnsiString);
       procedure SetGeoTextFieldName(sGeoTextFldName: AnsiString);
       procedure SetMonitoredFieldName(sMonitoredFieldName: AnsiString);
// ++ Cadmensky
       procedure SetOverlayFieldNames(sOverlayFieldNames: AnsiString);
       procedure SetInfoFieldNames(sInfoFieldNames: AnsiString);

       procedure SetQueryNames (sQueryNames: AnsiString);
       procedure SetQueryAsString (sQueryName, sQueryText: AnsiString);
       procedure DeleteQueryByName (sQueryName: AnsiString);
// -- Cadmensky
    end;

implementation

uses DDB, SysUtils,
     IDB_Consts, IDB_Utils, IDB_LanguageManager, IDB_CallBacksDef, IDB_MainManager;

constructor TIDB_DescMan.Create(AProject: Pointer; AIDB_DA: TIDB_Access2DB_Level2; ALayer: Pointer; AParametersMode: TParamsToBeRead = pbrAll);
begin
     SetScreenCursor;
     try
        inherited Create;
        SELF.AMode := AParametersMode;
        SELF.IDA := AIDB_DA;

        AWorkTable := TDTable.Create(nil);
        AWorkTable.Master := AIDB_DA.DMaster;
        AWorkQuery := TDQuery.Create(nil);
        AWorkQuery.Master := AIDB_DA.DMaster;

        SELF.sLayerIndex := IntToStr (GetLayerIndexFunc (ALayer));

        if not AnIDBMainMan.UseNewATTNames (AProject) then
           SELF.sAttribTableName := cs_AttributeTableNamePrefix + sLayerIndex
        else
           SELF.sAttribTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

        SELF.sOldDescTableName := cs_DescriptionTableNamePrefix + '_' + sLayerIndex;

        AStructMan := TIDB_StructMan.Create(AProject, IDA);
        listFieldsOfAttribTable := TStringList.Create;
        listAnnotationFieldNames := TStringList.Create;
        listChartFieldNames := TStringList.Create;
// ++ Cadmensky
        listOverlayFieldNames := TStringList.Create;
        listInfoFieldNames := TStringList.Create;
        listQueryNames := TStringList.Create;
// -- Cadmensky
        // Now I read all field's names of the attribute layer's table.
        ShowPercentAtWinGISProc (1, 1455, 39);
        SELF.ReadAttribTableFieldsNames;
        ShowPercentAtWinGISProc (1, 1455, 41);
// ++ Cadmensky
        if not SELF.IDA.ADbItemsMan.TableExists(cs_CommonDescTableName) then
           IDA.ADbItemsMan.CreateCommonDescTable
        else
           SELF.CheckAndCorrectDescTableStructure (cs_CommonDescTableName);
// -- Cadmensky
        // Now I read data from the description table of the attribute layer's table.
        SELF.ReadDescriptionTable;
        if SELF.AMode <> pbrGeoText then
           SELF.CorrectSelectedFieldsList;
     finally
        RestoreScreenCursor;
     end;
end;

destructor TIDB_DescMan.Free;
var i: integer;
begin
     AWorkTable.Free;
     AWorkQuery.Free; // Cadmensky
     AStructMan.Free; // Cadmensky
     listAnnotationFieldNames.Free;
     listChartFieldNames.Free;
// ++ Cadmensky
     listOverlayFieldNames.Free;
     listInfoFieldNames.Free;
     for i := 0 to listQueryNames.Count - 1 do
        if listQueryNames.Objects[i] <> nil then
           TStringList (listQueryNames.Objects[i]).Free;
     listQueryNames.Free;
// -- Cadmensky
     listFieldsOfAttribTable.Free;
end;

procedure TIDB_DescMan.SetScreenCursor;
begin
     AnOldScreenCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
end;

procedure TIDB_DescMan.RestoreScreenCursor;
begin
     Screen.Cursor := AnOldScreenCursor;
end;

{In this procedure I read field's names of the layer's attributive table.}
function TIDB_DescMan.ReadAttribTableFieldsNames: Boolean;
var
   iI: Integer;
begin
     RESULT := FALSE;
     if not SELF.IDA.ADbItemsMan.TableExists(SELF.sAttribTableName) then EXIT;
     if AWorkTable.Active then AWorkTable.Close;
     AWorkTable.TableName := SELF.sAttribTableName;

     AWorkTable.Open;
     for iI:=0 to AWorkTable.Fields.Count-1 do begin
         if FieldIsInternalUsageField(AWorkTable.Fields[iI].FieldName) then CONTINUE;
         listFieldsOfAttribTable.Add(AWorkTable.Fields[iI].FieldName);
         end;
     SELF.AStructMan.DetectStructure(AWorkTable);
     AWorkTable.Close;
     RESULT := TRUE;
end;

{Read settings from layer's description table.}
function TIDB_DescMan.ReadOldDescriptionTable: Boolean;
var
   S: TStream;
   i: integer;
   iDataLength: integer;
   pBuff: Pointer;
begin
     RESULT := FALSE;

     if not SELF.IDA.ADbItemsMan.TableExists(SELF.sOldDescTableName) then
        EXIT
     else
        SELF.CheckAndCorrectDescTableStructure (SELF.sOldDescTableName);

     if AWorkTable.Active then AWorkTable.Close;
     AWorkTable.TableName := SELF.sOldDescTableName;
     AWorkTable.Open;

     try
        SELF.SetXCoordFieldName(AWorkTable.FieldByName('XCOORDINATE').AsString);
        SELF.SetYCoordFieldName(AWorkTable.FieldByName('YCOORDINATE').AsString);
        SELF.SetSymNumFieldName(AWorkTable.FieldByName('SYMBOLNUMBER').AsString);
        SELF.SetSymNameFieldName(AWorkTable.FieldByName('SYMBOLNAME').AsString);
        SELF.SetSymSizeFieldName(AWorkTable.FieldByName('SYMBOLSIZE').AsString);
        SELF.SetUseSymName(AWorkTable.FieldByName('USESYMBOLNAME').AsBoolean);
        SELF.SetMonitoredFieldName(AWorkTable.FieldByName('MONITORID').AsString);
        SELF.SetGeoTextFieldName(AWorkTable.FieldByName('GEOTEXT').AsString);

        S := AWorkTable.CreateBlobStream(AWorkTable.FieldByName('ANNOTATIONFIELDS'), bmRead);
        try
           SELF.listAnnotationFieldNames.LoadFromStream(S);
        finally
           S.Free;
        end;
        bAnnotationListHasBeenChanged := TRUE;

        S := AWorkTable.CreateBlobStream(AWorkTable.FieldByName('CHARTFIELDS'), bmRead);
        try
           SELF.listChartFieldNames.LoadFromStream(S);
        finally
           S.Free;
        end;
        bChartListHasBeenChanged := TRUE;

// ++ Cadmensky
        S := AWorkTable.CreateBlobStream(AWorkTable.FieldByName('OVERLAYFIELDS'), bmRead);
        try
           SELF.listOverlayFieldNames.LoadFromStream(S);
        finally
           S.Free;
        end;
        bOverlayListHasBeenChanged := TRUE;

        if bNeedToFillInfoFields then
        begin
           SELF.listInfoFieldNames.Text := SELF.listFieldsOfAttribTable.Text;
           bNeedToFillInfoFields := false;
        end
        else
        begin
           S := AWorkTable.CreateBlobStream(AWorkTable.FieldByName('INFOFIELDS'), bmRead);
           try
              SELF.listInfoFieldNames.LoadFromStream(S);
           finally
             S.Free;
           end;
        end;
        bInfoListHasBeenChanged := TRUE;

        S := AWorkTable.CreateBlobStream(AWorkTable.FieldByName('QUERYNAMES'), bmRead);
        try
           SELF.listQueryNames.LoadFromStream(S);
        finally
           S.Free;
        end;
        bQueryListHasBeenChanged := TRUE;

        S := AWorkTable.CreateBlobStream(AWorkTable.FieldByName('QUERYTEXTS'), bmRead);
        try
           for i := 0 to listQueryNames.Count - 1 do
           begin
              S.Read (Pointer (iDataLength), SizeOf (integer));
              GetMem(pBuff, iDataLength);
              S.Read (pBuff^, iDataLength);
              SELF.listQueryNames.Objects[i] := pBuff;
           end;
//              SELF.listQueryTexts.LoadFromStream(S);
        finally
           S.Free;
        end;
// -- Cadmensky
     finally
        AWorkTable.Close;
     end;
     RESULT := TRUE;
end;

function TIDB_DescMan.ReadDescriptionTable(AParametersMode: TParamsToBeRead = pbrAll): Boolean;
var
   S: TDBlobStream;

   function ReadInteger: Integer;
   begin
      S.Read (RESULT, SizeOf (RESULT));
   end;

   function ReadString: AnsiString;
   var
      iL: Integer;
   begin
      iL := ReadInteger;
      if il> 10000 then il:=1;
      SetLength (RESULT, iL);
      S.Read (Pointer (RESULT)^, iL);
   end;

var iRecordCount: Integer;
    i: integer;
//    iDataLength: integer;
//    pBuff: Pointer;
begin
     RESULT := FALSE;
{     if not SELF.IDA.ADbItemsMan.TableExists(cs_CommonDescTableName) then
        IDA.ADbItemsMan.CreateCommonDescTable
     else
        SELF.CheckAndCorrectDescTableStructure (cs_CommonDescTableName);}

     SELF.AWorkQuery.Close;
     SELF.AWorkQuery.SQL.Text := 'SELECT * FROM [' + cs_CommonDescTableName + '] ' +
                                 'WHERE [' + cs_CDT_LayerIndexFieldName + '] = ' + SELF.sLayerIndex;
     try
        SELF.AWorkQuery.Open;
        iRecordCount := SELF.AWorkQuery.RecordCount;
     except
        iRecordCount := 0;
     end;
     // if there is not any record in new description table - try to read the old description table.
     if iRecordCount < 1 then begin
        SELF.AWorkQuery.Close;
        SELF.ReadOldDescriptionTable;
        EXIT;
        end;
     try
        if AParametersMode = pbrAll then begin
           SELF.sXCoordFieldName := AWorkQuery.FieldByName('XCOORDINATE').AsString;
           SELF.bXCoordFieldHasBeenChanged := FALSE;

           SELF.sYCoordFieldName :=  AWorkQuery.FieldByName('YCOORDINATE').AsString;
           SELF.bYCoordFieldHasBeenChanged := FALSE;

           SELF.sSymNumFieldName :=  AWorkQuery.FieldByName('SYMBOLNUMBER').AsString;
           SELF.bSymNumFieldNameHasBeenChanged := FALSE;

           SELF.sSymNameFieldName := AWorkQuery.FieldByName('SYMBOLNAME').AsString;
           SELF.bSymNameFieldNameHasBeenChanged := FALSE;

           SELF.sSymSizeFieldName := AWorkQuery.FieldByName('SYMBOLSIZE').AsString;
           SELF.bSymSizeFieldNameHasBeenChanged := FALSE;

           SELF.bUseSymName := AWorkQuery.FieldByName('USESYMBOLNAME').AsBoolean;

           //        SELF.sGeoTextIDFieldName := AWorkQuery.FieldByName('GEOTEXTID').AsString;

           SELF.sMonitoredFieldName := AWorkQuery.FieldByName('MONITORID').AsString;
           SELF.bMonitoredFieldNameHasBeenChanged := FALSE;

           S := TDBlobStream (AWorkQuery.CreateBlobStream(AWorkQuery.FieldByName('ANNOTATIONFIELDS'), bmRead));
           try
              SELF.listAnnotationFieldNames.LoadFromStream(S);
           finally
              S.Free;
           end;
           bAnnotationListHasBeenChanged := FALSE;

           S := TDBlobStream (AWorkQuery.CreateBlobStream(AWorkQuery.FieldByName('CHARTFIELDS'), bmRead));
           try
              SELF.listChartFieldNames.LoadFromStream(S);
           finally
              S.Free;
           end;
           bChartListHasBeenChanged := FALSE;

// ++ Cadmensky
           S := TDBlobStream (AWorkQuery.CreateBlobStream(AWorkQuery.FieldByName('OVERLAYFIELDS'), bmRead));
           try
              SELF.listOverlayFieldNames.LoadFromStream(S);
           finally
              S.Free;
           end;
           bOverlayListHasBeenChanged := FALSE;

           S := TDBlobStream (AWorkQuery.CreateBlobStream(AWorkQuery.FieldByName('INFOFIELDS'), bmRead));
           try
              SELF.listInfoFieldNames.LoadFromStream(S);
              if SELF.listInfoFieldNames.Text = '' then
                 SELF.listInfoFieldNames.Add ('ProgisID');
           finally
              S.Free;
           end;

           if bNeedToFillInfoFields then
           begin
              SELF.listInfoFieldNames.Text := SELF.listFieldsOfAttribTable.Text;
              bNeedToFillInfoFields := false;
              S := TDBlobStream(AWorkQuery.CreateBlobStream(AWorkQuery.FieldByName('INFOFIELDS'), bmWrite));
              try
                 listInfoFieldNames.SaveToStream(S);
                 S.Flush;
              finally
                 S.Free;
              end;
           end;
           bInfoListHasBeenChanged := FALSE;

           S := TDBlobStream (AWorkQuery.CreateBlobStream(AWorkQuery.FieldByName('QUERYNAMES'), bmRead));
           try
              SELF.listQueryNames.LoadFromStream(S);
           finally
              S.Free;
           end;
           bQueryListHasBeenChanged := FALSE;

           S := TDBlobStream (AWorkQuery.CreateBlobStream(AWorkQuery.FieldByName('QUERYTEXTS'), bmRead));
           try
              S.Position := 0;
              for i := 0 to listQueryNames.Count - 1 do
              begin
                 SELF.listQueryNames.Objects[i] := TStringList.Create;
                 TStringList (SELF.listQueryNames.Objects[i]).Text := ReadString;
              end;
//              SELF.listQueryTexts.LoadFromStream(S);
           finally
              S.Free;
           end;
           bQueryListHasBeenChanged := FALSE;
           end;
// -- Cadmensky

        SELF.sGeoTextFieldName := AWorkQuery.FieldByName('GEOTEXT').AsString;
        SELF.bGeoTextFieldNameHasBeenChanged := FALSE;

        RESULT := TRUE;
     finally
        AWorkQuery.Close;
     end;
end;

{Put field's names that have the special meaning into the description table.}
procedure TIDB_DescMan.SetUserChoice;
var
   S: TDBlobStream;
   iRecordCount: Integer;
   i: integer;
   pBuff: AnsiString;
   iDataLength: integer;
begin
     SetScreenCursor;
//     SELF.CheckAndCorrectDescTableStructure (cs_CommonDescTableName);

     SELF.AWorkQuery.Close;
     SELF.AWorkQuery.SQL.Text := 'SELECT * FROM [' + cs_CommonDescTableName + '] ' +
                                 'WHERE [' + cs_CDT_LayerIndexFieldName + '] = ' + SELF.sLayerIndex;
     try
        SELF.AWorkQuery.Open;
        iRecordCount := SELF.AWorkQuery.RecordCount;
     except
        iRecordCount := 0;
     end;

     if iRecordCount < 1 then begin
        try
           SELF.AWorkQuery.Close;
           SELF.AWorkQuery.ExecSQL('INSERT INTO [' + cs_CommonDescTableName + '] ([' + cs_CDT_LayerIndexFieldName + ']) ' +
                                    'VALUES(' + SELF.sLayerIndex + ')');
           SELF.AWorkQuery.SQL.Text := 'SELECT * FROM [' + cs_CommonDescTableName + '] ' +
                                       'WHERE [' + cs_CDT_LayerIndexFieldName + '] = ' + SELF.sLayerIndex;
           SELF.AWorkQuery.Open;
        except
           RestoreScreenCursor;
           EXIT;
        end;
        end;

     try
        AWorkQuery.Edit;

        if SELF.AMode = pbrAll then begin
           if bXCoordFieldHasBeenChanged then AWorkQuery.FieldByName('XCOORDINATE').AsString := SELF.sXCoordFieldName;
           if bYCoordFieldHasBeenChanged then AWorkQuery.FieldByName('YCOORDINATE').AsString := SELF.sYCoordFieldName;
           if bSymNumFieldNameHasBeenChanged then AWorkQuery.FieldByName('SYMBOLNUMBER').AsString := SELF.sSymNumFieldName;
           if bSymNameFieldNameHasBeenChanged then AWorkQuery.FieldByName('SYMBOLNAME').AsString := SELF.sSymNameFieldName;
           if bSymSizeFieldNameHasBeenChanged then AWorkQuery.FieldByName('SYMBOLSIZE').AsString := SELF.sSymSizeFieldName;
//           AWorkQuery.FieldByName('USESYMBOLNAME').AsBoolean := SELF.bUseSymName;
//           AWorkQuery.FieldByName('GEOTEXTID').AsString := SELF.sGeoTextIDFieldName;
           if bMonitoredFieldNameHasBeenChanged then AWorkQuery.FieldByName('MONITORID').AsString := SELF.sMonitoredFieldName;
           if bAnnotationListHasBeenChanged then begin
              S := TDBlobStream(AWorkQuery.CreateBlobStream(AWorkQuery.FieldByName('ANNOTATIONFIELDS'), bmWrite));
              try
                 listAnnotationFieldNames.SaveToStream(S);
                 S.Flush;
              finally
                 S.Free;
              end;
              end;
           if bChartListHasBeenChanged then begin
              S := TDBlobStream(AWorkQuery.CreateBlobStream(AWorkQuery.FieldByName('CHARTFIELDS'), bmWrite));
              try
                 listChartFieldNames.SaveToStream(S);
                 S.Flush;
              finally
                 S.Free;
              end;
              end;

// ++ Cadmensky
           if bOverlayListHasBeenChanged then
           begin
              S := TDBlobStream(AWorkQuery.CreateBlobStream(AWorkQuery.FieldByName('OVERLAYFIELDS'), bmWrite));
              try
                 listOverlayFieldNames.SaveToStream(S);
                 S.Flush;
              finally
                 S.Free;
              end;
           end;

           if bInfoListHasBeenChanged then
           begin
              S := TDBlobStream(AWorkQuery.CreateBlobStream(AWorkQuery.FieldByName('INFOFIELDS'), bmWrite));
              try
                 listInfoFieldNames.SaveToStream(S);
                 S.Flush;
              finally
                 S.Free;
              end;
           end;

           if bQueryListHasBeenChanged then
           begin
              S := TDBlobStream(AWorkQuery.CreateBlobStream(AWorkQuery.FieldByName('QUERYNAMES'), bmWrite));
              try
                 listQueryNames.SaveToStream(S);
                 S.Flush;
              finally
                 S.Free;
              end;

              S := TDBlobStream(AWorkQuery.CreateBlobStream(AWorkQuery.FieldByName('QUERYTEXTS'), bmWrite));
              try
                 S.Position := 0;
                 for i := 0 to listQueryNames.Count - 1 do
                 begin
                    pBuff := TStringList (listQueryNames.Objects[i]).Text;
                    iDataLength := Length (pBuff);
                    S.Write (Pointer (iDataLength), SizeOf (integer));
                    S.Write (Pointer (pBuff)^, iDataLength);
                 end;
//                 listQueryTexts.SaveToStream(S);
                 S.Flush;
              finally
                 S.Free;
              end;
           end;
// -- Cadmensky
        end;
        if bGeoTextFieldNameHasBeenChanged then AWorkQuery.FieldByName('GEOTEXT').AsString := SELF.sGeoTextFieldName;
        AWorkQuery.Post;
        SELF.IDA.ADbItemsMan.DeleteOldDescriptionTable(SELF.sLayerIndex);
     finally
        AWorkQuery.Close;
        RestoreScreenCursor;
     end;
end;

function TIDB_DescMan.GetCountOfAnnotTableField: Integer;
begin
     RESULT := listFieldsOfAttribTable.Count;
end;

function TIDB_DescMan.GetFieldNameOfAnnotTable(iFieldPos: Integer): AnsiString;
begin
     RESULT := '';
     if (iFieldPos < 0) OR (iFieldPos > listFieldsOfAttribTable.Count-1) then EXIT;
     RESULT := listFieldsOfAttribTable[iFieldPos];
end;

function TIDB_DescMan.FieldIsNumberType(sFieldName: AnsiString): Boolean;
var
   iFT: Integer;
begin
     RESULT := FALSE;
     iFT := SELF.AStructMan.GetFieldType(sFieldName);
     if iFT < 0 then EXIT; // This field was not found in the structure data.
{
1 - String
2 - SmallInt
3 - Integer
5 - Boolean
6 - Float
7 - Currency
11 - DateTime
12 - Byte
}
     Case iFT Of
     2..3, 6..7, 12: RESULT := TRUE;
     end
end;

function TIDB_DescMan.GetAnnotationFieldNames: AnsiString;
begin
     RESULT := SELF.listAnnotationFieldNames.Text;
end;

function TIDB_DescMan.GetChartFieldNames: AnsiString;
begin
     RESULT := listChartFieldNames.Text;
end;

// ++ Cadmensky
function TIDB_DescMan.GetOverlayFieldNames: AnsiString;
begin
     RESULT := listOverlayFieldNames.Text;
end;

function TIDB_DescMan.GetInfoFieldNames: AnsiString;
begin
     RESULT := listInfoFieldNames.Text;
end;

function TIDB_DescMan.GetQueryNames: AnsiString;
begin
     RESULT := listQueryNames.Text;
end;

function TIDB_DescMan.GetQueryAsString (sQueryName: AnsiString): AnsiString;
var iIndexOfQueryName: integer;
begin
   RESULT := '';
   try
      iIndexOfQueryName := SELF.listQueryNames.IndexOf (sQueryName);
      if (iIndexOfQueryName <> - 1) and (listQueryNames.Objects[iIndexOfQueryName] <> nil) then
         RESULT := TStringList (listQueryNames.Objects[iIndexOfQueryName]).Text;
   except
   end;
end;
// -- Cadmensky

function TIDB_DescMan.GetXCoordFieldName: AnsiString;
begin
     RESULT := SELF.sXCoordFieldName
end;

function TIDB_DescMan.GetYCoordFieldName: AnsiString;
begin
     RESULT := SELF.sYCoordFieldName;
end;

function TIDB_DescMan.GetSymNumFieldName: AnsiString;
begin
     RESULT := SELF.sSymNumFieldName;
end;

function TIDB_DescMan.GetSymNameFieldName: AnsiString;
begin
     RESULT := SELF.sSymNameFieldName;
end;

function TIDB_DescMan.GetSymSizeFieldName: AnsiString;
begin
     RESULT := SELF.sSymSizeFieldName;
end;

function TIDB_DescMan.GetUseSymName: Boolean;
begin
     RESULT := SELF.bUseSymName;
end;
{
function TIDB_DescMan.GetGeoTextIDFieldName: AnsiString;
begin
     RESULT := SELF.sGeoTextIDFieldName;
end;
}
function TIDB_DescMan.GetGeoTextFieldName: AnsiString;
begin
     RESULT := SELF.sGeoTextFieldName;
     if RESULT = '' then RESULT := 'ProgisID';
end;

function TIDB_DescMan.GetMonitoredFieldName: AnsiString;
begin
     RESULT := SELF.sMonitoredFieldName;
     if RESULT = '' then RESULT := 'ProgisID';
end;

procedure TIDB_DescMan.SetAnnotationFieldNames(sAnnotFldNames: AnsiString);
begin
     if SELF.listAnnotationFieldNames.Text = sAnnotFldNames then EXIT;
     SELF.listAnnotationFieldNames.Text := sAnnotFldNames;
     bAnnotationListHasBeenChanged := TRUE;
end;

procedure TIDB_DescMan.SetChartFierldNames(sChartFldNames: AnsiString);
begin
     if SELF.listChartFieldNames.Text = sChartFldNames then EXIT;
     SELF.listChartFieldNames.Text := sChartFldNames;
     bChartListHasBeenChanged := TRUE;
end;

// ++ Cadmensky
procedure TIDB_DescMan.SetOverlayFieldNames(sOverlayFieldNames: AnsiString);
begin
     bOverlayListHasBeenChanged := TRUE;
     if SELF.listOverlayFieldNames.Text = sOverlayFieldNames then
        EXIT;
     SELF.listOverlayFieldNames.Text := sOverlayFieldNames;
end;

procedure TIDB_DescMan.SetInfoFieldNames(sInfoFieldNames: AnsiString);
begin
     bInfoListHasBeenChanged := TRUE;
     if SELF.listInfoFieldNames.Text = sInfoFieldNames then
        EXIT;
     SELF.listInfoFieldNames.Text := sInfoFieldNames;
end;

procedure TIDB_DescMan.SetQueryNames (sQueryNames: AnsiString);
begin
     bQueryListHasBeenChanged := TRUE;
     if SELF.listQueryNames.Text = sQueryNames then
        EXIT;
     SELF.listQueryNames.Text := sQueryNames;
end;

procedure TIDB_DescMan.SetQueryAsString (sQueryName, sQueryText: AnsiString);
var iIndexOfQueryName: integer;
begin
     bQueryListHasBeenChanged := true;

     iIndexOfQueryName := SELF.listQueryNames.IndexOf (sQueryName);

     if (iIndexOfQueryName <> -1) then
     begin
        if (listQueryNames.Objects[iIndexOfQueryName] = nil) then
           SELF.listQueryNames.Objects[iIndexOfQueryName] := TStringList.Create;
        TStringList (SELF.listQueryNames.Objects[iIndexOfQueryName]).Text := sQueryText
     end
     else
     begin
        SELF.listQueryNames.Add (sQueryName);
        SetQueryAsString (sQueryName, sQueryText);
     end;

end;
// -- Cadmensky

procedure TIDB_DescMan.SetXCoordFieldName(sXCoordFldName: AnsiString);
begin
     if SELF.sXCoordFieldName = sXCoordFldName then EXIT;
     SELF.sXCoordFieldName := sXCoordFldName;
     bXCoordFieldHasBeenChanged := TRUE;
end;

procedure TIDB_DescMan.SetYCoordFieldName(sYcoordFldName: AnsiString);
begin
     if SELF.sYCoordFieldName = sYCoordFldName then EXIT;
     SELF.sYCoordFieldName := sYCoordFldName;
     bYCoordFieldHasBeenChanged := TRUE;
end;

procedure TIDB_DescMan.SetSymNumFieldName(sSymbolNumberFldName: AnsiString);
begin
     if SELF.sSymNumFieldName = sSymbolNumberFldName then EXIT;
     SELF.sSymNumFieldName := sSymbolNumberFldName;
     bSymNumFieldNameHasBeenChanged := TRUE;
end;

procedure TIDB_DescMan.SetSymNameFieldName(sSymbolNameFldName: AnsiString);
begin
     if SELF.sSymNameFieldName = sSymbolNameFldName then EXIT;
     SELF.sSymNameFieldName := sSymbolNameFldName;
     bSymNameFieldNameHasBeenChanged := TRUE;
end;

procedure TIDB_DescMan.SetSymSizeFieldName(sSymbolSizeFldName: AnsiString);
begin
     if SELF.sSymSizeFieldName = sSymbolSizeFldName then EXIT;
     SELF.sSymSizeFieldName := sSymbolSizeFldName;
     bSymSizeFieldNameHasBeenChanged := TRUE;
end;

procedure TIDB_DescMan.SetUseSymName(bUseSymbolName: Boolean);
begin
     SELF.bUseSymName := bUseSymbolName;
end;
{
procedure TIDB_DescMan.SetGeoTextIDFieldName(sGeoTextIDFldName: AnsiString);
begin
     SELF.sGeoTextIDFieldName := sGeoTextIDFldName;
end;
}
procedure TIDB_DescMan.SetGeoTextFieldName(sGeoTextFldName: AnsiString);
begin
     if SELF.sGeoTextFieldName = sGeoTextFldName then EXIT;
     SELF.sGeoTextFieldName := sGeoTextFldName;
     bGeoTextFieldNameHasBeenChanged := TRUE;
end;

procedure TIDB_DescMan.SetMonitoredFieldName(sMonitoredFieldName: AnsiString);
begin
     if SELF.sMonitoredFieldName = sMonitoredFieldName then EXIT;
     SELF.sMonitoredFieldName := sMonitoredFieldName;
     bMonitoredFieldNameHasBeenChanged := TRUE;
end;

{ This procedure deletes names of fields from list of selected fields that isn't present
  in a real attribute table.}
procedure TIDB_DescMan.CorrectSelectedFieldsList;
var
   iI: Integer;
begin
     iI:=0;
     while iI < listAnnotationFieldNames.Count do
         if listFieldsOfAttribTable.IndexOf(listAnnotationFieldNames[iI]) < 0 then
            listAnnotationFieldNames.Delete(iI)
         else
            Inc(iI);
end;

{Check present of required fields and create their if they doesn't exist.}
procedure TIDB_DescMan.CheckAndCorrectDescTableStructure (sDescTableName: AnsiString);
var
   bFieldIsPresent: Boolean;
begin

{ Text field type = 1
  Integer type = 3
  Logical field type = 5
  Memo field type = 16
  BLOB type = 15 }
// ++ Cadmensky Version 2.3.2
   if IDA.ADBItemsMan.GetReadyToCheckTableStructure (sDescTableName) <> ci_All_Ok then
   begin
      IDA.ADBItemsMan.EndOfCheckingTable;
      Exit;
   end;

     if not IDA.ADbItemsMan.FieldExists('_USER_ID') then
        IDA.ADbItemsMan.AddField(sDescTableName, '_USER_ID', 1, 38);

     if not IDA.ADbItemsMan.FieldExists('_PROJECT_ID') then
        IDA.ADbItemsMan.AddField(sDescTableName, '_PROJECT_ID', 1, 38);

     if not IDA.ADbItemsMan.FieldExists(cs_CDT_LayerIndexFieldName) then
        IDA.ADbItemsMan.AddField(sDescTableName, cs_CDT_LayerIndexFieldName, 3, -1);

     if not IDA.ADbItemsMan.FieldExists('ANNOTATIONFIELDS') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'ANNOTATIONFIELDS', 16, -1);

     if not IDA.ADbItemsMan.FieldExists('CHARTFIELDS') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'CHARTFIELDS', 16, -1);

     if not IDA.ADbItemsMan.FieldExists('OVERLAYFIELDS') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'OVERLAYFIELDS', 16, -1);

     bNeedToFillInfoFields := false;
     if not IDA.ADbItemsMan.FieldExists('INFOFIELDS') then
     begin
        IDA.ADbItemsMan.AddField(sDescTableName, 'INFOFIELDS', 16, -1);
        bNeedToFillInfoFields := true;
     end;

     if not IDA.ADbItemsMan.FieldExists('QUERYNAMES') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'QUERYNAMES', 16, -1);

     if not IDA.ADbItemsMan.FieldExists('QUERYTEXTS') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'QUERYTEXTS', 16, -1);

     if not IDA.ADbItemsMan.FieldExists('XCoordinate') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'XCoordinate', 1, 64);

     if not IDA.ADbItemsMan.FieldExists('YCoordinate') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'YCoordinate', 1, 64);

     if not IDA.ADbItemsMan.FieldExists('SymbolNumber') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'SymbolNumber', 1, 64);

     if not IDA.ADbItemsMan.FieldExists('SymbolSize') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'SymbolSize', 1, 64);

     if not IDA.ADbItemsMan.FieldExists('UseSymbolName') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'UseSymbolName', 1, 64);

     if not IDA.ADbItemsMan.FieldExists('GeoText') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'GeoText', 1, 64);

     if not IDA.ADbItemsMan.FieldExists('DBGridProps') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'DBGridProps', 15, -1);

     if not IDA.ADbItemsMan.FieldExists('DBGridPosX') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'DBGridPosX', 3, -1);

     if not IDA.ADbItemsMan.FieldExists('DBGridPosY') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'DBGridPosY', 3, -1);

     if not IDA.ADbItemsMan.FieldExists('DBGridWidth') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'DBGridWidth', 3, -1);

     if not IDA.ADbItemsMan.FieldExists('DBGridHeight') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'DBGridHeight', 3, -1);

     if not IDA.ADbItemsMan.FieldExists('MonitorID') then
        IDA.ADbItemsMan.AddField(sDescTableName, 'MonitorID', 1, 64);

     IDA.ADBItemsMan.EndOfCheckingTable;
// -- Cadmensky Version 2.3.2

// ++ Commented by Cadmensky Version 2.3.2
{     if IDA.ADbItemsMan.FieldExists(sDescTableName, '_USER_ID', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, '_USER_ID', 1, 38);

     if IDA.ADbItemsMan.FieldExists(sDescTableName, '_PROJECT_ID', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, '_PROJECT_ID', 1, 38);

     if IDA.ADbItemsMan.FieldExists(sDescTableName, cs_CDT_LayerIndexFieldName, bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, cs_CDT_LayerIndexFieldName, 3, -1);

     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'ANNOTATIONFIELDS', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'ANNOTATIONFIELDS', 16, -1);

     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'CHARTFIELDS', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'CHARTFIELDS', 16, -1);

// ++ Cadmensky
     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'OVERLAYFIELDS', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'OVERLAYFIELDS', 16, -1);

     bNeedToFillInfoFields := false;
     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'INFOFIELDS', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then
     begin
        IDA.ADbItemsMan.AddField(sDescTableName, 'INFOFIELDS', 16, -1);
        bNeedToFillInfoFields := true;
     end;

     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'QUERYNAMES', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'QUERYNAMES', 16, -1);

     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'QUERYTEXTS', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'QUERYTEXTS', 16, -1);
// -- Cadmensky

     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'XCoordinate', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'XCoordinate', 1, 64);

     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'YCoordinate', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'YCoordinate', 1, 64);

     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'SymbolNumber', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'SymbolNumber', 1, 64);

     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'SymbolSize', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'SymbolSize', 1, 64);

     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'UseSymbolName', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'UseSymbolName', 1, 64);

//     if IDA.ADbItemsMan.FieldExists(cs_CommonDescTableName, 'GeoTextID', bFieldIsPresent) <> ci_All_Ok then EXIT;
//     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(cs_CommonDescTableName, 'GeoTextID', 1, 64);

     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'GeoText', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'GeoText', 1, 64);

     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'DBGridProps', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'DBGridProps', 15, -1);

     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'DBGridPosX', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'DBGridPosX', 3, -1);

     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'DBGridPosY', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'DBGridPosY', 3, -1);

     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'DBGridWidth', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'DBGridWidth', 3, -1);

     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'DBGridHeight', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'DBGridHeight', 3, -1);

     if IDA.ADbItemsMan.FieldExists(sDescTableName, 'MonitorID', bFieldIsPresent) <> ci_All_Ok then EXIT;
     if not bFieldIsPresent then IDA.ADbItemsMan.AddField(sDescTableName, 'MonitorID', 1, 64);}
// -- Commented by Cadmensky Version 2.3.2

end;

procedure TIDB_DescMan.DeleteQueryByName(sQueryName: AnsiString);
var iItemIndex: integer;
begin
   iItemIndex := listQueryNames.IndexOf (sQueryName);
   if iItemIndex <> -1 then
   begin
      if listQueryNames.Objects[iItemIndex] <> nil then
         TStringList (listQueryNames.Objects[iItemIndex]).Free;
      listQueryNames.Delete (iItemIndex);
   end;
end;

end.
