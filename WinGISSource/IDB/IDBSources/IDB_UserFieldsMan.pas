unit IDB_UserFieldsMan;

{
Internal database. Ver. II.

Author: Cadmensky Fedor, Progis Russia, Moscow.
Date: 03.04.2002
Modified:

This is an object to manage on user defined calculated fields.
}


interface

uses Forms, Controls, SysUtils, Classes,
     IDB_Access2DB;

type

     PUserCalcField = ^TUserCalcField;
     TUserCalcField = record
        sFieldName: AnsiString;
        sFormula  : AnsiString;
        sPrecision: AnsiString;
     end;

     TIDB_BaseUserFieldsManager = class
     private
        iLayerIndex: Integer;
        listUserCalcFields: TList; // Cadmensky

        bUserFieldsStateWasSuccessfullyLoaded: Boolean;

        AnOldScreenCursor: TCursor;

        procedure RestoreScreenCursor;
        procedure SetScreenCursor;
        function GetFieldsCount: integer;
     public
        property FieldsCount: integer read GetFieldsCount;
        constructor Create(iLayerIndex: Integer);
        destructor Free;
        procedure AddUserCalcField(sFieldName, sFormula: AnsiString);
        procedure DeleteUserCalcField (iItemIndex: integer); overload;
        procedure DeleteUserCalcField (sFieldName: AnsiString); overload;
        function GetUserCalcFieldName(iItemNumber: integer): AnsiString;
        function GetUserCalcFieldFormula(iItemNumber: integer): AnsiString;
        function GetUserCalcFieldsNames: AnsiString;
        function GetUserCalcFieldsSQLString: AnsiString;
        procedure SetUserCalcFieldFormula(iItemNumber: integer; sNewFormula: AnsiString);
        function GetUserCalcFieldPrecision(iItemNumber: integer): AnsiString;
        procedure SetUserCalcFieldPrecision(iItemNumber: integer; sNewPrecision: AnsiString);
        function FieldIsUserDefined(sFieldName: AnsiString): boolean;
     end;

     TIDB_UserFieldsManager = class (TIDB_BaseUserFieldsManager)
     private
        IDA: TIDB_Access2DB_Level2;
        function LoadData: Boolean;
        procedure SaveData;
        procedure CheckAndCorrectDescTableStructure;
     public
        constructor Create(IDB_DataAccessModule: TIDB_Access2DB_Level2; iLayerIndex: Integer);
        destructor Free;
     end;

implementation

uses IDB_Consts, DTables, DDB, DB;

const cs_FieldName = 'UserFieldsProps';

// ===================== TIDB_BaseUserFieldsManager ===============================

constructor TIDB_BaseUserFieldsManager.Create(iLayerIndex: Integer);
begin
   SELF.iLayerIndex := iLayerIndex;
   SELF.listUserCalcFields := TList.Create;
end;

destructor TIDB_BaseUserFieldsManager.Free;
var i: integer;
begin
   if SELF.listUserCalcFields.Count > 0 then
      for i := 0 to SELF.listUserCalcFields.Count - 1 do
         FreeMem (SELF.listUserCalcFields[i]);
   SELF.listUserCalcFields.Free;
end;

function TIDB_BaseUserFieldsManager.GetFieldsCount: integer;
begin
   RESULT := Self.listUserCalcFields.Count;
end;

Procedure TIDB_BaseUserFieldsManager.SetScreenCursor;
Begin
     AnOldScreenCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
end;

Procedure TIDB_BaseUserFieldsManager.RestoreScreenCursor;
Begin
     Screen.Cursor := AnOldScreenCursor;
end;

procedure TIDB_BaseUserFieldsManager.AddUserCalcField (sFieldName: AnsiString; sFormula: AnsiString);
var pNewUsercalcField: PUserCalcField;
begin
   New (pNewUsercalcField);
   pNewUsercalcField.sFieldName := sFieldName;
   pNewUsercalcField.sFormula := sFormula;
   listUserCalcFields.Add (pNewUsercalcField);
end;

procedure TIDB_BaseUserFieldsManager.DeleteUserCalcField (iItemIndex: integer);
begin
   try
   listUserCalcFields.Delete (iItemIndex);
   except
   end;
end;

procedure TIDB_BaseUserFieldsManager.DeleteUserCalcField (sFieldName: AnsiString);
var i: integer;
    pUCF: PUserCalcField;
begin
   try
   for i := 0 to listUserCalcFields.Count - 1 do
   begin
      pUCF := Self.listUserCalcFields[i];
      if pUCF.sFieldName = sFieldName then
      begin
         listUserCalcFields.Delete (i);
         exit;
      end;
   end;
   except
   end;
end;

function TIDB_BaseUserFieldsManager.GetUserCalcFieldsSQLString: AnsiString;
var pUCF: PUserCalcField;
    i: integer;
//    sAttrTableName: string;
begin
   try
      if Self.listUserCalcFields.Count > 0 then
         for i := 0 to Self.listUserCalcFields.Count - 1 do
         begin
            pUCF := Self.listUserCalcFields[i];
            if pUCF.sFormula <> '' then
               if pUCF.sPrecision <> '' then
//               RESULT := RESULT + ', SUBSTRING (' + pUCF.sFormula +
//                         ', 0, CHARINDEX (''.'', 2 + ' + pUCF.sFormula + ')) AS ' + pUCF.sFieldName;
                  RESULT := RESULT + ', ROUND (' + pUCF.sFormula + ', ' + pUCF.sPrecision + ') AS ' + pUCF.sFieldName
               else
                  RESULT := RESULT + ', ' + pUCF.sFormula + ' AS ' + pUCF.sFieldName;
         end;
    except
       Result := '';
    end;
end;

function TIDB_BaseUserFieldsManager.GetUserCalcFieldsNames: AnsiString;
var pUCF: PUserCalcField;
    i: integer;
    SL: TStringList;
begin
   RESULT := '';
   try
      SL := TStringList.Create;

      if Self.listUserCalcFields.Count > 0 then
         for i := 0 to Self.listUserCalcFields.Count - 1 do
         begin
            pUCF := Self.listUserCalcFields[i];
            SL.Add (pUCF.sFieldName);
         end;
   finally
      RESULT := SL.Text;
      SL.Free;
   end;
end;

function TIDB_BaseUserFieldsManager.GetUserCalcFieldName (iItemNumber: integer): AnsiString;
var pUCF: PUserCalcField;
begin
   RESULT := '';
   try
      pUCF := Self.listUserCalcFields[iItemNumber];
      RESULT := pUCF.sFieldName;
   except
   end;
end;

function TIDB_BaseUserFieldsManager.GetUserCalcFieldFormula (iItemNumber: integer): AnsiString;
var pUCF: PUserCalcField;
begin
   RESULT := '';
   try
      pUCF := Self.listUserCalcFields[iItemNumber];
      RESULT := pUCF.sFormula;
   except
   end;
end;

function TIDB_BaseUserFieldsManager.GetUserCalcFieldPrecision (iItemNumber: integer): AnsiString;
var pUCF: PUserCalcField;
begin
   RESULT := '';
   try
      pUCF := Self.listUserCalcFields[iItemNumber];
      RESULT := pUCF.sPrecision;
   except
   end;
end;

procedure TIDB_BaseUserFieldsManager.SetUserCalcFieldFormula (iItemNumber: integer; sNewFormula: AnsiString);
var pUCF: PUserCalcField;
begin
   try
      pUCF := Self.listUserCalcFields[iItemNumber];
      pUCF.sFormula := sNewFormula;
   except
   end;
end;

procedure TIDB_BaseUserFieldsManager.SetUserCalcFieldPrecision (iItemNumber: integer; sNewPrecision: AnsiString);
var pUCF: PUserCalcField;
begin
   try
      pUCF := Self.listUserCalcFields[iItemNumber];
      pUCF.sPrecision := sNewPrecision;
   except
   end;
end;

function TIDB_BaseUserFieldsManager.FieldIsUserDefined (sFieldName: AnsiString): boolean;
var i: integer;
    pUCF: PUserCalcField;
begin
   RESULT := false;
   for i := 0 to listUserCalcFields.Count - 1 do
   begin
      pUCF := listUserCalcFields[i];
      if sFieldName = pUCF.sFieldName then
         RESULT := true;
   end;
//   if slUserFields.IndexOf (sFieldName) <> -1 then
//   slUserFields.Free;
end;

// ===================== TIDB_UserFieldsManager ===============================

constructor TIDB_UserFieldsManager.Create(IDB_DataAccessModule: TIDB_Access2DB_Level2; iLayerIndex: Integer);
begin
   SELF.IDA := IDB_DataAccessModule;

   inherited Create (iLayerIndex);


   SELF.bUserFieldsStateWasSuccessfullyLoaded := LoadData;
end;

destructor TIDB_UserFieldsManager.Free;
begin
   SaveData;
   inherited Free;
end;

Procedure TIDB_UserFieldsManager.CheckAndCorrectDescTableStructure;
Var
   bFieldIsPresent: Boolean;
Begin
     If IDA.ADbItemsMan.FieldExists(cs_CommonDescTableName, '_USER_ID', bFieldIsPresent) <> ci_All_Ok Then EXIT;
     If NOT bFieldIsPresent Then IDA.ADbItemsMan.AddField(cs_CommonDescTableName, '_USER_ID', 1, 38);

     If IDA.ADbItemsMan.FieldExists(cs_CommonDescTableName, '_PROJECT_ID', bFieldIsPresent) <> ci_All_Ok Then EXIT;
     If NOT bFieldIsPresent Then IDA.ADbItemsMan.AddField(cs_CommonDescTableName, '_PROJECT_ID', 1, 38);

     If IDA.ADbItemsMan.FieldExists(cs_CommonDescTableName, cs_CDT_LayerIndexFieldName, bFieldIsPresent) <> ci_All_Ok Then EXIT;
     If NOT bFieldIsPresent Then IDA.ADbItemsMan.AddField(cs_CommonDescTableName, cs_CDT_LayerIndexFieldName, 3, -1);

     If IDA.ADbItemsMan.FieldExists(cs_CommonDescTableName, cs_FieldName, bFieldIsPresent) <> ci_All_OK Then EXIT;
     If NOT bFieldIsPresent Then IDA.ADbItemsMan.AddField(cs_CommonDescTableName, cs_FieldName, 15, -1); // 15th type is BLOB
End;

Procedure TIDB_UserFieldsManager.SaveData;
var iI, iLen: Integer;
    pUCF: PUserCalcField;
    AQuery: TDQuery;
    AStream: TDBlobStream;
    sLayerIndex: AnsiString;
begin
     SetScreenCursor;

     sLayerIndex := intToStr(SELF.iLayerIndex);
     IDA.ADbItemsMan.CreateCommonDescTable;
     SELF.CheckAndCorrectDescTableStructure;

     AQuery := TDQuery.Create(nil);
     AQuery.Master := IDA.DMaster;
     AQuery.SQL.Text := 'SELECT [' + cs_FieldName + '], [' + cs_CDT_LayerIndexFieldName + '] ' +
                        'FROM [' + cs_CommonDescTableName + '] ' +
                        'WHERE [' + cs_CDT_LayerIndexFieldName + '] = ' + sLayerIndex;
     try
        AQuery.Open;
        if AQuery.RecordCount < 1 then
        begin
           AQuery.Close;
           AQuery.ExecSQL('INSERT INTO [' + cs_CommonDescTableName + '] ([' + cs_CDT_LayerIndexFieldName + ']) ' +
                          'VALUES(' + sLayerIndex + ')');
           AQuery.SQL.Text := 'SELECT [' + cs_FieldName + '], [' + cs_CDT_LayerIndexFieldName + '] ' +
                              'FROM [' + cs_CommonDescTableName + '] ' +
                              'WHERE [' + cs_CDT_LayerIndexFieldName + '] = ' + sLayerIndex;
           AQuery.Open;
        end;
        AQuery.Edit;
        AStream := TDBlobStream(AQuery.CreateBlobStream(AQuery.FieldByName(cs_FieldName), bmWrite));
        if (AStream = nil) then
           raise Exception.Create('A stream to saving data cannot be created.');
     except
        AQuery.Cancel;
        AQuery.Free;
        RestoreScreenCursor;
        EXIT;
     end;
     try
        AStream.Truncate;
        // First of all write number of entries.
        iLen := listUserCalcFields.Count;
        AStream.Write(iLen, SizeOf(iLen));
        for iI := 0 to listUserCalcFields.Count - 1 do
        begin
            pUCF := listUserCalcFields[iI];
            // Write field name.
            iLen := Length(pUCF.sFieldName);
            AStream.Write(iLen, SizeOf(iLen));
            AStream.Write(Pointer(pUCF.sFieldName)^, iLen);
            // Write fields formula
            iLen := Length(pUCF.sFormula);
            AStream.Write(iLen, SizeOf(iLen));
            AStream.Write(Pointer(pUCF.sFormula)^, iLen);
            // Write field precision
            iLen := Length(pUCF.sPrecision);
            AStream.Write(iLen, SizeOf(iLen));
            AStream.Write(Pointer(pUCF.sPrecision)^, iLen);
        end;
        AStream.Flush;
     finally
        AStream.Free;

        AQuery.Post;
        AQuery.Free;
        RestoreScreenCursor;
     end;
end;

function TIDB_UserFieldsManager.LoadData: Boolean;
var iCount, iI, iLen: Integer;
    pUCF: PUserCalcField;
    AQuery: TDQuery;
    AStream: TDBlobStream;
    sLayerIndex: AnsiString;
begin
     RESULT := FALSE;

     SetScreenCursor;

     sLayerIndex := IntToStr(SELF.iLayerIndex);
     IDA.ADbItemsMan.CreateCommonDescTable;
     SELF.CheckAndCorrectDescTableStructure;

     AQuery := TDQuery.Create(nil);
     AQuery.Master := IDA.DMaster;
     AQuery.SQL.Text := 'SELECT [' + cs_FieldName + '] FROM [' + cs_CommonDescTableName + '] ' +
                        'WHERE [' + cs_CDT_LayerIndexFieldName + '] = ' + sLayerIndex;
     try
        AQuery.Open;
        if AQuery.RecordCount < 1 then
           raise Exception.Create('The data are missing.');
        AStream := TDBlobStream(AQuery.CreateBlobStream(AQuery.FieldByName(cs_FieldName), bmRead));
        if (AStream = nil) or (AStream.Size < 1) then
           raise Exception.Create('A stream to loading data cannot be created.');
     EXCEPT
        AQuery.Close;
        AQuery.Free;
        RestoreScreenCursor;
        exit;
     end;
     try
        AStream.Position := 0;
        // Read number of entires.
        AStream.Read(iCount, SizeOf(iCount));
        for iI := 0 to iCount - 1 do
        begin
            New(pUCF);
            // Read field name.
            AStream.Read(iLen, SizeOf(iLen));
            SetLength(pUCF.sFieldName, iLen);
            AStream.Read(Pointer(pUCF.sFieldName)^, iLen);
            // Read fields formula
            AStream.Read(iLen, SizeOf(iLen));
            SetLength(pUCF.sFormula, iLen);
            AStream.Read(Pointer(pUCF.sFormula)^, iLen);
            // Read fields precision
            AStream.Read(iLen, SizeOf(iLen));
            SetLength(pUCF.sPrecision, iLen);
            AStream.Read(Pointer(pUCF.sPrecision)^, iLen);
            // Add entry
            listUserCalcFields.Add(pUCF);
        end;
        RESULT := TRUE;
     finally
        AStream.Free;
        AQuery.Close;
        AQuery.Free;
        RestoreScreenCursor;
     end;
end;



end.
