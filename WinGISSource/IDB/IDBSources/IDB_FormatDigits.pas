unit IDB_FormatDigits;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, ExtCtrls, IDB_LanguageManager;

type
  TFormatfieldForm = class(TForm)
    Button1: TButton;
    CanelButton: TButton;
    Panel1: TPanel;
    CheckBoxSeperator: TCheckBox;
    SpinEditStellen: TSpinEdit;
    RadioButtonStellen: TRadioButton;
    RadioButtonDefault: TRadioButton;
    procedure RadioButtonDefaultClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Panel1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormatfieldForm: TFormatfieldForm;

implementation

{$R *.DFM}

procedure TFormatfieldForm.RadioButtonDefaultClick(Sender: TObject);
begin
if RadioButtonDefault.Checked then
   begin
   RadioButtonStellen.Checked:=False;
   SpineditStellen.Enabled:=False;
   end
   else
   begin
   SpineditStellen.Enabled:=True;
   end;
end;

procedure TFormatfieldForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
if modalresult= mrOK then
   begin
   modalresult:=10000;
   if self.CheckBoxSeperator.Checked then modalresult:=modalresult+1000;
   if spineditstellen.Enabled then modalresult:=modalresult + spineditstellen.Value
      else
      modalresult:=modalresult+ 100;
   end
   else modalresult:=-1;
end;

procedure TFormatfieldForm.FormCreate(Sender: TObject);
begin
ALangManager.MakeReplacement (SELF);
end;

procedure TFormatfieldForm.FormMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
if  (ssLeft in Shift)   then
    if (x > self.SpinEditStellen.left) and (y > self.spineditstellen.top) and
       (x < self.SpineditStellen.Left+self.SpinEditStellen.Width) and (y < self.SpinEditStellen.top+self.SpinEditStellen.Height) then
    begin
    self.RadioButtonStellen.Checked:=True;
    self.RadioButtonDefault.Checked:=False;
    self.RadioButtonDefaultClick(sender);
    end;
end;

procedure TFormatfieldForm.Panel1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
if  (ssLeft in Shift)   then
    if (x > self.SpinEditStellen.left) and (y > self.spineditstellen.top) and
       (x < self.SpineditStellen.Left+self.SpinEditStellen.Width) and (y < self.SpinEditStellen.top+self.SpinEditStellen.Height) then
    begin
    self.RadioButtonStellen.Checked:=True;
    self.RadioButtonDefault.Checked:=False;
    self.RadioButtonDefaultClick(sender);
    end;
end;

end.
