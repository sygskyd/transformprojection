unit IDB_UBuilder;
{
Internal database. Ver. II.
Update builder.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 05-03-2001
}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls, ExtPage, Grids, IDB_C_StringGrid, Buttons,
  IDB_Access2DB, IDB_StructMan, Validate, IDB_Grids;

type
  TIDB_UBForm = class(TForm)
    BottomPanel: TPanel;
    OKButton: TButton;
    CancelButton: TButton;
    ExtPageControl1: TExtPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Memo1: TMemo;
    SG: TIDB_StringGrid;
    ComboBox1: TComboBox;
    HeaderControl1: THeaderControl;
    AButton: TBitBtn;
    IntValidator1: TIntValidator;
    FloatValidator1: TFloatValidator;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SGMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure HeaderControl1SectionResize(HeaderControl: THeaderControl;
      Section: THeaderSection);
    procedure SGTopLeftChanged(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure AButtonClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure SGClick(Sender: TObject);
  private
    { Private declarations }
    AStructureManager: TIDB_StructMan;
    sLayerName,
    sAttTableName: AnsiString;
    Procedure DrawComboBox(bShowIt: Boolean = TRUE);
    Function GenerateSQLText(Var sSQL: AnsiString): Boolean;
    Procedure DisconnectAllValidators;    
  public
    { Public declarations }
    Constructor Create(AOwner: TComponent; AStructureManager: TIDB_StructMan; sLayerName: AnsiString; iLayerIndex: Integer);

    Procedure InitializeGrid(sCurrentFieldName: AnsiString);
    Function MakeIt(sFieldName, sValue: AnsiString): AnsiString;
  end;

implementation
{$R *.DFM}
Uses DTables,
     IDB_LanguageManager, IDB_ExpressionBuilder, IDB_Consts, IDB_Utils, IDB_MainManager;

Constructor TIDB_UBForm.Create(AOwner: TComponent; AStructureManager: TIDB_StructMan; sLayerName: AnsiString; iLayerIndex: Integer);
Begin
     Inherited Create(AOwner);

     SELF.sLayerName := sLayerName;
     if not AnIDBMainMan.UseNewATTNames (AProject) then
        SELF.sAttTableName := cs_AttributeTableNamePrefix + IntToStr(iLayerIndex)
     else
        SELF.sAttTableName := cs_AttributeTableNamePrefix + '_' + sLayerName;

     SELF.AStructureManager := AStructureManager;
End;

procedure TIDB_UBForm.FormCreate(Sender: TObject);
begin
     InitializeGrid('');
     ALangManager.MakeReplacement(SELF);
end;

Procedure TIDB_UBForm.InitializeGrid(sCurrentFieldName: AnsiString);
Var
   iI, iJ: Integer;
Begin
     SG.RowCount := 2;
     SG.Row := 0;
     For iI:=0 To SG.RowCount-1 Do
         For iJ:=0 To SG.ColCount-1 Do
             SG.Cells[iJ, iI] := '';

     iI := ComboBox1.Items.IndexOf(sCurrentFieldName);
     If iI < 0 Then iI := 0;
     ComboBox1.ItemIndex := iI;
     SG.Cells[0, 0] := ComboBox1.Items[ComboBox1.ItemIndex]; 
End;

procedure TIDB_UBForm.FormShow(Sender: TObject);
Var
   iWidth: Integer;
begin
     SG.DefaultColWidth := (SG.ClientWidth DIV 2) - 2;
     iWidth := SG.DefaultColWidth + 2;
     HeaderControl1.Sections[0].Width := iWidth;
     HeaderControl1.Sections[1].Width := iWidth;
     DrawComboBox;
end;

Procedure TIDB_UBForm.DrawComboBox(bShowIt: Boolean = TRUE);
Var
   ARect: TRect;
   iP: Integer;
Begin
     If bShowIt Then Begin
        If ComboBox1.DroppedDown Then ComboBox1.DroppedDown := FALSE;
        ARect := SG.CellRect(0, SG.Row);
        // Draw the ComboBox at the left column of the StringGrid.
        ComboBox1.Left := ARect.Left + 2;
        ComboBox1.Top := SG.Top + ARect.Top;
        ComboBox1.Width := ARect.Right - ARect.Left;
        ComboBox1.Height := ARect.Bottom - ARect.Top;

        SG.Invalidate;
        Application.ProcessMessages;

        iP := ComboBox1.Items.IndexOf(SG.Cells[0, SG.Row]);
        If iP < 0 Then Begin
           iP := 0;
           ComboBox1.DroppedDown := TRUE;
           End;
        ComboBox1.ItemIndex := iP;
        ComboBox1.Visible := TRUE;

        ARect := SG.CellRect(1, SG.Row);
        AButton.Height := ARect.Bottom - ARect.Top ; //ComboBox1.Height;
        AButton.Left := ARect.Right - AButton.Height;
        AButton.Top := ComboBox1.Top;
        AButton.Width := AButton.Height;

        AButton.Visible := TRUE;
        End
     Else Begin
        ComboBox1.Visible := FALSE;
        AButton.Visible := FALSE;
        End;
End;

procedure TIDB_UBForm.SGMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
Var
   ACol, ARow: Integer;
begin
     HeaderControl1.Sections[0].Width := SG.ColWidths[0] + 2;
     HeaderControl1.Sections[1].Width := SG.ColWidths[1] + 2;
     SG.MouseToCell(X, Y, ACol, ARow);
     If (ACol > -1) And (ARow > -1) Then Begin
        SG.Row := ARow;
        DrawComboBox;
        End;
end;

procedure TIDB_UBForm.HeaderControl1SectionResize(HeaderControl: THeaderControl; Section: THeaderSection);
begin
     If HeaderControl1.Sections[0].Width > 0 Then
        SG.ColWidths[0] := HeaderControl1.Sections[0].Width - 2;
     SG.ColWidths[1] := HeaderControl1.Sections[1].Width - 2;
     DrawComboBox;
end;

procedure TIDB_UBForm.SGTopLeftChanged(Sender: TObject);
begin
     If SG.LeftCol = 0 Then Begin
        HeaderControl1.Sections[0].Width := SG.ColWidths[0] + 2;
        DrawComboBox;
        End
     Else Begin
        HeaderControl1.Sections[0].Width := 0;
        DrawComboBox(FALSE);
        End;
end;

procedure TIDB_UBForm.ComboBox1Change(Sender: TObject);
Var
   bThereWasNothing: Boolean;
   bThereIsAText: Boolean;
   iI, iFieldType: Integer;
   sData, sCurrentChar: AnsiString;
begin
     bThereWasNothing := Trim(SG.Cells[0, SG.Row]) = '';
     iFieldType := AStructureManager.GetFieldType(ComboBox1.Items[ComboBox1.ItemIndex]);
     If (bThereWasNothing) Then
        SG.RowCount := SG.RowCount + 1
     Else Begin
        bThereIsAText := FALSE;
        sData := SG.Cells[1, SG.Row];
        For iI:=1 To Length(sData) Do Begin
            sCurrentChar := Copy(sData, iI, 1);
            If (sCurrentChar < '0') OR (sCurrentChar > '9') Then Begin
               bThereIsAText := TRUE;
               BREAK;
               End;
            End;
        If bThereIsAText AND (iFieldType <> 1) Then
           SG.Cells[1, SG.Row] := '';
        End;
     SG.Cells[0, SG.Row] := ComboBox1.Items[ComboBox1.ItemIndex];
end;

procedure TIDB_UBForm.AButtonClick(Sender: TObject);
Var
   EBF: TIDB_ExpressionBuilderForm;
   sFieldName, sAddFieldName: AnsiString;
   iI, iFieldType, iAddFieldType: Integer;
   sData: AnsiString;
begin
     sFieldName := SG.Cells[0, SG.Row];
     iFieldType := AStructureManager.GetFieldType(sFieldName);
     EBF := TIDB_ExpressionBuilderForm.Create(SELF, sFieldName, iFieldType);
     // Put all fields in the list box.
     For iI:=0 To ComboBox1.Items.Count-1 Do Begin
         sAddFieldName := ComboBox1.Items[iI];
         iAddFieldType := AStructureManager.GetFieldType(sAddFieldName);
         If (iFieldType <> 1) And (iAddFieldType = 1) Then CONTINUE;
         EBF.FieldsListBox.Items.Add(sAddFieldName);
         End;
     EBF.FieldsListBox.ItemIndex := 0;
     EBF.FunctionsListBox.ItemIndex := 0;

     EBF.Memo1.Text := SG.Cells[1, SG.Row];
     If EBF.ShowModal = mrOk Then Begin
        sData := EBF.Memo1.Lines.Text;
        sData := DeleteChars(sData, #13);
        sData := DeleteChars(sData, #10);
        SG.Cells[1, SG.Row] := sData;
        End;
     EBF.Free;
end;

Function TIDB_UBForm.GenerateSQLText(Var sSQL: AnsiString): Boolean;
Var
   slSQL: TStringList;

   Procedure TruncateLastComma;
   Begin
        slSQL[slSQL.Count-1] := Copy(slSQL[slSQL.Count-1], 1, Length(slSQL[slSQL.Count-1])-2);
   End;

Var
   iI: Integer;
   sTableName,
   sFieldName,
   sValue: AnsiString;
   bThereIsData: Boolean;
Begin
     RESULT := FALSE;
     sSQL := '';
     // 212 = Table of the layer '' %s''
     sTableName := Format(ALangManager.GetValue(212), [SELF.sLayerName]);
     // And now I start the work.
     slSQL := TStringList.Create;
     slSQL.Clear;

     slSQL.Add('UPDATE [' + sTableName + ']');
     slSQL.Add('');
     slSQL.Add('SET ');
     bThereIsData := FALSE;
     For iI:=0 To SG.RowCount-1 Do Begin
         sFieldName := SG.Cells[0, iI];
         If Trim(sFieldName) = '' Then CONTINUE;

         // The text can be in the inplcae editor, not in the grid yet.
         If (SG.Row = iI) AND (SG.InplaceEditor <> NIL) Then
            sValue := SG.InplaceEditor.Text
         Else
            sValue := SG.Cells[1, iI];

         If (AStructureManager.GetFieldType(sFieldName) <> 1) AND (Trim(sValue) = '') Then CONTINUE;
         bThereIsData := TRUE;
         slSQL.Add('[' + sFieldName + '] = ' + MakeIt(sFieldName, sValue) + ', ');
         End;
     If bThereIsData Then Begin
        TruncateLastComma;
        sSQL := slSQL.Text;
        RESULT := TRUE;
        End;
     slSQL.Clear;
     slSQL.Free;
End;

procedure TIDB_UBForm.TabSheet2Show(Sender: TObject);
Var
   sSQL: AnsiString;
begin
     If SELF.GenerateSQLText(sSQL) Then
        Memo1.Lines.Text := sSQL
     Else
        Memo1.Lines.Clear;  
end;

Function TIDB_UBForm.MakeIt(sFieldName, sValue: AnsiString): AnsiString;
Const
     cs_SpecialSign     = '$';
     cs_CancelSign      = '\';
     cs_UseFieldName    = 'F';
     cs_UseCurrentValue = 'C';
     cs_StBracket       = '(';
     cs_EndBracket      = ')';
Type
     TMode = (wmUnknown, wmSpecialSignWasAdded, wmOrdinarySignWasAdded);
Var
   iPStBr, iPEndBr: Integer;
   sResult,
   sCurrentChar: AnsiString;
   bFollowingSignHasPower: Boolean;
   AMode: TMode;
Begin
     RESULT := '';
     sResult := '';

     bFollowingSignHasPower := TRUE;
     AMode := wmUnknown;
//// STRING TYPE
     If AStructureManager.GetFieldType(sFieldName) = 1 Then Begin
        While Length(sValue) > 0 Do Begin
              sCurrentChar := Copy(sValue, 1, 1);
              If sCurrentChar = cs_CancelSign Then Begin
                 bFollowingSignHasPower := FALSE;
                 sValue := Copy(sValue, 2, Length(sValue));
                 End
              Else Begin
                 If (sCurrentChar = cs_SpecialSign) AND bFollowingSignHasPower Then Begin
                    sCurrentChar := AnsiUppercase(Copy(sValue, 2, 1));
                    If (sCurrentChar = cs_UseCurrentValue) Then Begin
                       If AMode = wmOrdinarySignWasAdded Then sResult := sResult + ''' + ';
                       sResult := sResult + '[' + sFieldName + '] + ';
                       sValue := Copy(sValue, 3, Length(sValue));
                       AMode := wmSpecialSignWasAdded;
                       End
                    Else Begin
                      If (sCurrentChar = cs_UseFieldName) Then Begin
                         iPStBr := Pos(cs_StBracket, sValue);
                         If iPStBr = 0 Then Begin
                            sValue := Copy(sValue, 1, 2) + cs_StBracket + Copy(sValue, 3, Length(sValue));
                            iPStBr := 3;
                            End;
                         iPEndBr := Pos(cs_EndBracket, sValue);
                         If iPEndBr = 0 Then
                            If Length(Copy(sValue, iPStBr+1, Length(sValue))) = 0 Then Begin
                               sValue := Copy(sValue, 1, iPStBr) + cs_EndBracket + Copy(sValue, iPStBr+1, Length(sValue));
                               iPEndBr := iPStBr + 1;
                               End
                            Else Begin
                               iPEndBr := Length(sValue) + 1;
                               sValue := sValue + cs_EndBracket;
                               End;
                         If iPEndBr-iPStBr-1 > 0 Then Begin
                            If AMode = wmOrdinarySignWasAdded Then sResult := sResult + ''' + ';
                            sResult := sResult + '[' + AnsiUpperCase(Copy(sValue, iPStBr+1, iPEndBr-iPStBr-1)) + '] + ';
                            End;
                         sValue := Copy(sValue, iPEndBr+1, Length(sValue));
                         AMode := wmSpecialSignWasAdded;
                         End;
                      End;
                    End
                 Else Begin
                    If sResult = '' Then sResult := ''''
                    Else
                       If AMode = wmSpecialSignWasAdded Then
                          sResult := sResult + '''';
                    sResult := sResult + sCurrentChar;
                    sValue := Copy(sValue, 2, Length(sValue));
                    AMode := wmOrdinarySignWasAdded;
                    End;
                 bFollowingSignHasPower := TRUE;
                 End;
              End; // While end

        Case AMode Of
        wmUnknown: sResult := '''''';
        wmOrdinarySignWasAdded: sResult := sResult + '''';
        wmSpecialSignWasAdded: sResult := Copy(sResult, 1, Length(sResult) - 3);
        End; // Case end
        End
//// INTEGER TYPE
     Else Begin
        While Length(sValue) > 0 Do Begin
              sCurrentChar := Copy(sValue, 1, 1);
              If (sCurrentChar = cs_SpecialSign) AND bFollowingSignHasPower Then Begin
                 sCurrentChar := AnsiUppercase(Copy(sValue, 2, 1));
                 If (sCurrentChar = cs_UseCurrentValue) Then Begin
                    sResult := sResult + ' [' + sFieldName + '] ';
                    sValue := Copy(sValue, 3, Length(sValue));
                    End
                 Else Begin
                    If (sCurrentChar = cs_UseFieldName) Then Begin
                       iPStBr := Pos(cs_StBracket, sValue);
                       If iPStBr = 0 Then Begin
                          sValue := Copy(sValue, 1, 2) + cs_StBracket + Copy(sValue, 3, Length(sValue));
                          iPStBr := 3;
                          End;
                       iPEndBr := Pos(cs_EndBracket, sValue);
                       If iPEndBr = 0 Then
                          If Length(Copy(sValue, iPStBr+1, Length(sValue))) = 0 Then Begin
                             sValue := Copy(sValue, 1, iPStBr) + cs_EndBracket + Copy(sValue, iPStBr+1, Length(sValue));
                             iPEndBr := iPStBr + 1;
                             End
                          Else Begin
                             iPEndBr := Length(sValue) + 1;
                             sValue := sValue + cs_EndBracket;
                             End;
                       If iPEndBr-iPStBr-1 > 0 Then
                          sResult := sResult + ' [' + AnsiUpperCase(Copy(sValue, iPStBr+1, iPEndBr-iPStBr-1)) + '] ';
                       sValue := Copy(sValue, iPEndBr+1, Length(sValue));
                       End;
                    End;
                 End
              Else Begin
                 sResult := sResult + sCurrentChar;
                 sValue := Copy(sValue, 2, Length(sValue));                  
                 End;
              End; // While end
        sResult := ReplaceChars(sResult, ',', '.');
        End;
     RESULT := sResult;
End;

procedure TIDB_UBForm.OKButtonClick(Sender: TObject);
Var
   bThereIsData: Boolean;
   iI: Integer;
   sFieldName: AnsiString;
   iFieldType: Integer;
begin
     bThereIsData := FALSE;
     For iI:=0 To SG.RowCount-1 Do Begin
         sFieldName := SG.Cells[0, iI];
         If (Trim(sFieldName) = '') Then CONTINUE;
         iFieldType := SELF.AStructureManager.GetFieldType(sFieldName);
         If (iFieldType <> 1) AND (Trim(SG.Cells[1, iI]) = '') Then CONTINUE;
         bThereIsData := TRUE;
         BREAK;
         End; // For iI end
     If NOT bThereIsData Then CancelButton.Click;
end;

Procedure TIDB_UBForm.DisconnectAllValidators;
Begin
     IntValidator1.Edit := NIL;
     FloatValidator1.Edit := NIL;
End;

procedure TIDB_UBForm.SGClick(Sender: TObject);
begin
        DisconnectAllValidators;
        Case AStructureManager.GetFieldType(SG.Cells[0, SG.Row]) Of
        1: Begin End; // String
        2,  // SmallInt
        3,  // Integer
        12: // Byte
            IntValidator1.Edit := SG.InplaceEditor;
        6,  // Float
        7:  // Currency
            FloatValidator1.Edit := SG.InplaceEditor;
        End; // Case end
end;

end.
