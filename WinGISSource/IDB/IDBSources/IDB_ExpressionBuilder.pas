unit IDB_ExpressionBuilder;
{
Internal database. Ver. II.
This module implenets the builder of user's expressions. These expressions will be used later.
For example, in Update builder.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 27-06-2001
}
interface

uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   ExtCtrls, StdCtrls, Buttons, ComCtrls, ExtPage;

type
   TIDB_ExpressionBuilderForm = class (TForm)
      Panel1: TPanel;
      Panel2: TPanel;
      OKButton: TButton;
      CancelButton: TButton;
      Memo1: TMemo;
      SpeedButton1: TSpeedButton;
      SpeedButton2: TSpeedButton;
      SpeedButton3: TSpeedButton;
      SpeedButton4: TSpeedButton;
      SpeedButton5: TSpeedButton;
      SpeedButton6: TSpeedButton;
      AddButton: TButton;
      ClearButton: TButton;
      ExtPageControl1: TExtPageControl;
      FieldsTS: TTabSheet;
      FunctionsTS: TTabSheet;
      FieldsListBox: TListBox;
      FunctionsListBox: TListBox;
      SpeedButton7: TSpeedButton;
      procedure FormCreate (Sender: TObject);
      procedure SpeedButton1Click (Sender: TObject);
      procedure SpeedButton2Click (Sender: TObject);
      procedure SpeedButton3Click (Sender: TObject);
      procedure SpeedButton4Click (Sender: TObject);
      procedure SpeedButton5Click (Sender: TObject);
      procedure SpeedButton6Click (Sender: TObject);
      procedure ClearButtonClick (Sender: TObject);
      procedure AddButtonClick (Sender: TObject);
      procedure Memo1KeyPress (Sender: TObject; var Key: Char);
      procedure SpeedButton7Click (Sender: TObject);
   private
    { Private declarations }
      sFieldName: AnsiString;
      iFieldType: Integer;
      procedure MakeToolBarForStringField;
      procedure AddSign (sWhatMustBeAdded: AnsiString);
   public
    { Public declarations }
      constructor Create (AOwner: TComponent; sFieldName: AnsiString; iFieldType: Integer);

   end;

implementation
{$R *.DFM}
uses
   IDB_LanguageManager;

constructor TIDB_ExpressionBuilderForm.Create (AOwner: TComponent; sFieldName: AnsiString; iFieldType: Integer);
begin
   inherited Create (AOwner);
   SELF.sFieldName := sFieldName;
   SELF.iFieldType := iFieldType;
   case iFieldType of
      1:
         begin // String type. Disable all buttons exclude '+'.
            MakeToolBarForStringField;
            FunctionsTS.Enabled := FALSE;
         end;
   else
      begin
      end;
   end; // Case end
     { It is under construction yet... }
   FunctionsTS.Enabled := FALSE;
end;

procedure TIDB_ExpressionBuilderForm.FormCreate (Sender: TObject);
begin
   ALangManager.MakeReplacement (SELF);
   ExtPageControl1.ActivePage := FieldsTS;
end;

procedure TIDB_ExpressionBuilderForm.AddSign (sWhatMustBeAdded: AnsiString);
var
   iPL, iPC, iSS: Integer;
   sData: AnsiString;
begin
     // I check what line and what place in this line I am positioned.
   iSS := Memo1.SelStart;
   iPL := SendMessage (Memo1.Handle, EM_LINEFROMCHAR, Memo1.SelStart, 0);
   iPC := SendMessage (Memo1.Handle, EM_LINEINDEX, iPL, 0);
   iPC := Memo1.SelStart - iPC + 1;
     // Insert new items.
   sData := Memo1.Lines[iPL];
   sData := Copy (sData, 1, iPC - 1) + sWhatMustBeAdded + Copy (sData, iPC, Length (sData));
   Memo1.Lines[iPL] := sData;
   Memo1.SelStart := iSS + Length (sWhatMustBeAdded);
   Memo1.SelLength := 0;
end;

procedure TIDB_ExpressionBuilderForm.SpeedButton1Click (Sender: TObject);
begin
   AddSign ('+');
end;

procedure TIDB_ExpressionBuilderForm.SpeedButton2Click (Sender: TObject);
begin
   AddSign ('-');
end;

procedure TIDB_ExpressionBuilderForm.SpeedButton3Click (Sender: TObject);
begin
   AddSign ('/');
end;

procedure TIDB_ExpressionBuilderForm.SpeedButton4Click (Sender: TObject);
begin
   AddSign ('*');
end;

procedure TIDB_ExpressionBuilderForm.SpeedButton5Click (Sender: TObject);
begin
   AddSign ('(');
end;

procedure TIDB_ExpressionBuilderForm.SpeedButton6Click (Sender: TObject);
begin
   AddSign (')');
end;

procedure TIDB_ExpressionBuilderForm.MakeToolBarForStringField;
begin
{
     SpeedButton1.Enabled := FALSE; // '+'
     SpeedButton2.Enabled := FALSE; // '-'
     SpeedButton3.Enabled := FALSE; // '/'
     SpeedButton4.Enabled := FALSE; // '*'
     SpeedButton5.Enabled := FALSE; // '('
     SpeedButton6.Enabled := FALSE; // ')'
}
end;

procedure TIDB_ExpressionBuilderForm.ClearButtonClick (Sender: TObject);
begin
   Memo1.Lines.Clear;
end;

procedure TIDB_ExpressionBuilderForm.AddButtonClick (Sender: TObject);
begin
   case ExtPageControl1.ActivePage.Tag of
      1:
         begin // List of fields
            if FieldsListBox.ItemIndex < 0 then EXIT;
            if sFieldName <> FieldsListBox.Items[FieldsListBox.ItemIndex] then
               AddSign ('$F(' + FieldsListBox.Items[FieldsListBox.ItemIndex] + ')')
            else
               AddSign ('$C');
         end;
      2:
         begin // List of functions
            if FunctionsListBox.ItemIndex < 0 then EXIT;
            AddSign (' ' + FunctionsListBox.Items[FunctionsListBox.ItemIndex] + ' ( ) ')
         end;
   end; // Case end
end;

procedure TIDB_ExpressionBuilderForm.Memo1KeyPress (Sender: TObject; var Key: Char);
var
   iCharNum: Integer;
begin
   iCharNum := Ord (Key);
   if (iCharNum = VK_BACK)
      or
      (iCharNum = VK_SPACE) then
      EXIT;
   case iFieldType of
      2, // SmallInt
         3, // Integer
         4, // Word
         7, // Currency
         12, // Byte
         25: // LargeInt
         // For non-String field we can use only number values.
         if (Key < '0') or (Key > '9') then Key := #0;
      6: // Float
         // For non-String field we can use only number values.
         if ((Key < '0') or (Key > '9')) and (Key <> ',') then Key := #0;
      1, // Text
         24: // WideString
         begin
         // For String field we can write all. We need not to correct the user.
         end;
   else
      begin
         // For all other field types we cannot write anything.
         Key := #0;
      end;
   end; // Case end
end;

procedure TIDB_ExpressionBuilderForm.SpeedButton7Click (Sender: TObject);
begin
   AddSign ('NULL');
end;

end.

