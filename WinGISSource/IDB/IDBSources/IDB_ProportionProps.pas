unit IDB_ProportionProps;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, ExtCtrls, IDB_PolygonOverlayManager;

type
  TIDB_ProportionPropsForm = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Panel1: TPanel;
    Edit1: TEdit;
    CheckBox1: TCheckBox;
    RadioGroup1: TRadioGroup;
    Label1: TLabel;
    Edit2: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
  private
    { Private declarations }
  public
     AOverlayField: TOverlayField;
     Formula: AnsiString;
    constructor Create(AField: TOverlayField);
    { Public declarations }
  end;

implementation

uses IDB_LanguageManager;

{$R *.DFM}

constructor TIDB_ProportionPropsForm.Create (AField: TOverlayField);
begin
   inherited Create (nil);
   SELF.AOverlayField := AField;
end;



procedure TIDB_ProportionPropsForm.FormCreate(Sender: TObject);
begin
   ALangManager.MakeReplacement (Self);
end;

procedure TIDB_ProportionPropsForm.BitBtn1Click(Sender: TObject);
//var Buffer: AnsiString;
begin
{   Buffer := StaticText1.Caption;
   StaticText1.Caption := StaticText2.Caption;
   StaticText2.Caption := Buffer;}
end;


procedure TIDB_ProportionPropsForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
//   Self.Formula :=
end;

procedure TIDB_ProportionPropsForm.FormShow(Sender: TObject);
begin
   if AOverlayField.Field1 <> '' then
   begin
      RadioGroup1.Items.Add (AOverlayField.Field1 + ' / ' + AOverlayField.Field2);
      RadioGroup1.Items.Add (AOverlayField.Field2 + ' / ' + AOverlayField.Field1);
   end;
   RadioGroup1.Items.Add (AOverlayField.Field2);
   if AOverlayField.Field1 <> '' then
      RadioGroup1.Items.Add ('Clear field');
   RadioGroup1.ItemIndex := 0;
end;

procedure TIDB_ProportionPropsForm.Button1Click(Sender: TObject);
var sBuffer: AnsiString;
begin
   if RadioGroup1.Items.Count > 2 then
   begin

      if CheckBox1.Checked then
         try
            AOverlayField.Coefficient := StrToFloat (Edit1.Text);
         except
            AOverlayField.Coefficient := 1;
         end;

      if Edit2.Text = '' then
         AOverlayField.Precision := -1
      else
         try
         AOverlayField.Precision := StrToInt (Edit2.Text);
         except
            AOverlayField.Precision := -1;
         end;

      case RadioGroup1.ItemIndex of
      0: begin
            AOverlayField.Calculated := true;
         end;
      1: begin
            AOverlayField.Calculated := true;
            sBuffer := AOverlayField.Field1;
            AOverlayField.Field1 := AOverlayField.Field2;
            AOverlayField.Field2 := sBuffer;
         end;
      2: begin
            AOverlayField.Field1 := AOverlayField.Field2;
            AOverlayField.Field2 := '';
            AOverlayField.Calculated := false;
         end;
      3: begin
            AOverlayField.Field1 := '';
            AOverlayField.Field2 := '';
            AOverlayField.Calculated := false;
         end;
      end;
   end
   else
      case RadioGroup1.ItemIndex of
      0: begin
            AOverlayField.Field1 := AOverlayField.Field2;
            AOverlayField.Field2 := '';
            AOverlayField.Calculated := false;
         end;
      1: begin
            AOverlayField.Field1 := '';
            AOverlayField.Calculated := false;
         end;
      end;
end;

procedure TIDB_ProportionPropsForm.RadioGroup1Click(Sender: TObject);
begin
   if RadioGroup1.ItemIndex < 2 then
   begin
      CheckBox1.Enabled := true;
      Edit1.Enabled := true;
   end
   else
   begin
      CheckBox1.Enabled := false;
      Edit1.Enabled := false;
   end
end;

end.
