unit IDB_ParamsOfSave;
{
Internal database. Ver. II.
Module sets parameters for saving data table information.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 02-02-2001
}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TIDB_SaveResultForm = class(TForm)
    Panel1: TPanel;
    OKButton: TButton;
    CancelButton: TButton;
    Label1: TLabel;
    CheckBox1: TCheckBox;
    ComboBox1: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

Uses
     IDB_LanguageManager;

{$R *.DFM}

procedure TIDB_SaveResultForm.FormShow(Sender: TObject);
begin
     ComboBox1.ItemIndex := 0;
end;

procedure TIDB_SaveResultForm.FormCreate(Sender: TObject);
begin
     ALangManager.MakeReplacement(SELF);
end;

end.
