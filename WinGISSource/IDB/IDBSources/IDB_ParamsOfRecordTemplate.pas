unit IDB_ParamsOfRecordTemplate;
{
Internal database. ver. II.
In this form an user can set some values that will be put into attribute fields.

This form is used for reconciling. In this case, when we will have find that an item
of project doesn't have a record in the attribute table, such record will be added and
values from this form will be put into the fields of this record .

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 14-06-2001
}

interface

uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   ExtCtrls, StdCtrls, Grids, IDB_C_StringGrid, Gauges,
   DTables, Db, DDB, Validate,
   IDB_Access2DB, IDB_StructMan, {IDB_ExternalTablesMan,} IDB_Grids;

type
   TIDB_ParamsOfRecordTemplateForm = class (TForm)
      Panel1: TPanel;
      BottomPanel: TPanel;
      OKButton: TButton;
      SG: TIDB_StringGrid;
      CancelButton: TButton;
      TopPanel: TPanel;
      Label1: TLabel;
      AND_RB: TRadioButton;
      OR_RB: TRadioButton;
      UseExistingValuesCheckBox: TCheckBox;
      ComboBox1: TComboBox;
      StatusPanel: TPanel;
      Gauge1: TGauge;
      Label2: TLabel;
      ADataQuery: TDQuery;
// ++ Cadmensky Version 2.3.4
      UpdateExistedRecordsCB: TCheckBox;
// -- Cadmensky Version 2.3.4
      procedure FormCreate (Sender: TObject);
      procedure FormShow (Sender: TObject);
      procedure FormResize (Sender: TObject);
      procedure SGClick (Sender: TObject);
      procedure ComboBox1Change (Sender: TObject);
      procedure UseExistingValuesCheckBoxClick (Sender: TObject);
      procedure FormKeyUp (Sender: TObject; var Key: Word;
         Shift: TShiftState);
      procedure ComboBox1KeyPress (Sender: TObject; var Key: Char);
      procedure OKButtonClick (Sender: TObject);
   private
    { Private declarations }
      AProject: Pointer;
      AnOldEventHandler: TNotifyEvent;
      sAttTableName: AnsiString;
      bIAmSelecting: Boolean;
      bTheUserWantsToCancel: Boolean;

      IntValidator: TIntValidator;
      FloatValidator: TFloatValidator;

      //AnExtTablesMan: TIDB_ExternalTablesMan;

      procedure RestoreApp (SENDER: TObject);
      procedure MinimizeApp (var AMsg: TWMSysCommand); message WM_SYSCommand;

      procedure DrawComboBox (bShowIt: Boolean = TRUE);
      procedure DisconnectAllValidators;
   public
    { Public declarations }
      IDA: TIDB_Access2DB_Level2;

      constructor Create (AProject: Pointer; AOwner: TComponent; ALayer: Pointer; ADatabaseAccessObject: TIDB_Access2DB_Level2{; AnExtTablesMan: TIDB_ExternalTablesMan = nil});
   end;

implementation
{$R *.DFM}

uses ADOBase,
   IDB_LanguageManager, IDB_Consts, IDB_View, IDB_CallbacksDef, IDB_Utils, IDB_MainManager;

procedure TIDB_ParamsOfRecordTemplateForm.MinimizeApp (var AMsg: TWMSysCommand);
begin
   if AMsg.CmdType = SC_MINIMIZE then
   begin
      AnOldEventHandler := Application.OnRestore;
      Application.OnRestore := SELF.RestoreApp;
      Application.Minimize;
   end
   else
      inherited;
end;

procedure TIDB_ParamsOfRecordTemplateForm.RestoreApp (SENDER: TObject);
begin
   SELF.Show;
   Application.OnRestore := AnOldEventHandler;
end;

constructor TIDB_ParamsOfRecordTemplateForm.Create (AProject: Pointer; AOwner: TComponent; ALayer: Pointer; ADatabaseAccessObject: TIDB_Access2DB_Level2{; AnExtTablesMan: TIDB_ExternalTablesMan = nil});
begin
   inherited Create (AOwner);
   SELF.AProject := AProject;

   if not AnIDBMainMan.UseNewATTNames (AProject) then
      SELF.sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
   else
      SELF.sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

   SELF.bIAmSelecting := FALSE;
   SELF.bTheUserWantsToCancel := FALSE;

   SELF.IDA := ADatabaseAccessObject;

   ADataQuery.Master := ADatabaseAccessObject.DMaster;
   ADataQuery.ADOCursorType := ctOpenForwardOnly;

   //SELF.AnExtTablesMan := AnExtTablesMan;
end;

procedure TIDB_ParamsOfRecordTemplateForm.FormCreate (Sender: TObject);
begin
   ALangManager.MakeReplacement (SELF);

   IntValidator := TIntValidator.Create (SELF);

   IntValidator.FormatWhenExit := FALSE;
   IntValidator.ValidateOnExit := FALSE;
   IntValidator.MaxLength := 100;

   FloatValidator := TFloatValidator.Create (SELF);
   FloatValidator.FormatWhenExit := FALSE;
   FloatValidator.ValidateOnExit := FALSE;
   FloatValidator.MaxLength := 100;
   FloatValidator.Commas := 20;
end;

procedure TIDB_ParamsOfRecordTemplateForm.DisconnectAllValidators;
begin
   IntValidator.Edit := nil;
   FloatValidator.Edit := nil;
end;

procedure TIDB_ParamsOfRecordTemplateForm.DrawComboBox (bShowIt: Boolean = TRUE);
   procedure GoIn;
   begin
      SELF.bIAmSelecting := TRUE;
      SELF.bTheUserWantsToCancel := FALSE;
      Gauge1.Progress := 0;
        // 326=Loading data from field '%s'... Press ESC to stop
      Label2.Caption := Format (ALangManager.GetValue (326), [SG.Cells[0, SG.Row]]);
      StatusPanel.Visible := TRUE;
      BottomPanel.Enabled := FALSE;
      SG.Enabled := FALSE;
      Application.ProcessMessages;
   end;

   procedure GoOut;
   begin
      SELF.bIAmSelecting := FALSE;
      StatusPanel.Visible := FALSE;
      BottomPanel.Enabled := TRUE;
      SG.Enabled := TRUE;
   end;
var
   ARect: TRect;
   iI: Integer;
// ++ Cadmensky
//   sLinkName,
// -- Cadmensky
      sTableName,
      sFieldName: AnsiString;
begin
   if bShowIt then
   begin
      ARect := SG.CellRect (1, SG.Row);
      ComboBox1.Text := '';
      ComboBox1.Left := ARect.Left + 1;
      ComboBox1.Top := SG.Top + ARect.Top;
      ComboBox1.Width := ARect.Right - ARect.Left + 5;
      ComboBox1.Height := ARect.Bottom - ARect.Top;
      Application.ProcessMessages;
      ComboBox1.Items.Clear;

      GoIn;

      sFieldName := SG.Cells[0, SG.Row];
      if ADataQuery.Active then ADataQuery.Close;
      //if AnExtTablesMan = nil then
         ADataQuery.SQL.Text := 'SELECT DISTINCT [' + sFieldName + '] ' +
            'FROM [' + SELF.sAttTableName + '] ' +
            'WHERE ' + cs_ShowAllWithoutDeletedObjectsFilter;
      {else
      begin
         iI := Pos ('_', sFieldName);
         if iI < 1 then
            sTableName := sAttTableName
         else
         begin
// ++ Cadmensky
            sTableName := AnExtTablesMan.GetLinkName (StrToInt (Copy (sFieldName, 1, iI - 1)) - 1);
//            sLinkName := AnExtTablesMan.GetLinkName (StrToInt (Copy (sFieldName, 1, iI - 1)) - 1);
//            sTableName := AnExtTablesMan.GetLinkDestTableName(sLinkName);
// ++ Cadmensky
            sFieldName := Copy (sFieldName, iI + 1, Length (sFieldName));
         end;
         ADataQuery.SQL.Text := 'SELECT DISTINCT [' + sTableName + '].[' + sFieldName + '] ' +
                                'FROM ' + AnExtTablesMan.JoinClause + ' ' +
                                'WHERE ' + cs_ShowAllWithoutDeletedObjectsFilter;
      end; }
      try
         ADataQuery.Open;
      except
         GoOut;
         EXIT;
      end;
      try
         Gauge1.MaxValue := ADataQuery.RecordCount;
         for iI := 0 to ADataQuery.RecordCount - 1 do
         begin
            ComboBox1.Items.Add (ADataQuery.Fields[0].AsString);
            ADataQuery.Next;
            Gauge1.Progress := Gauge1.Progress + 1;
            Application.ProcessMessages;
            if bTheUserWantsToCancel then BREAK;
         end;
      finally
         ADataQuery.Close;
         GoOut;
      end;
      ComboBox1.Visible := TRUE;
      ComboBox1.Text := SG.Cells[1, SG.Row];
   end
   else
      ComboBox1.Visible := FALSE;
end;

procedure TIDB_ParamsOfRecordTemplateForm.FormShow (Sender: TObject);
var
   iI, iCurrLen, iMaxLen: Integer;
begin
   iMaxLen := SG.Canvas.TextWidth (SG.Cells[0, 0]);
   for iI := 1 to SG.RowCount - 1 do
   begin
      iCurrLen := SG.Canvas.TextWidth (SG.Cells[0, iI]);
      if iCurrLen > iMaxLen then
         iMaxLen := iCurrLen;
   end; // For iI end
   SG.ColWidths[0] := iMaxLen + 10;
   SG.ColWidths[1] := SG.ClientWidth - SG.ColWidths[0];
   if SG.ColWidths[1] < 10 then SG.ColWidths[1] := SG.ClientWidth div 2;

   SG.ActivateEditor (1, 1);
   SGClick (SENDER);
end;

procedure TIDB_ParamsOfRecordTemplateForm.FormResize (Sender: TObject);
begin
   Label1.Width := TopPanel.ClientWidth - Label1.Left;
end;

procedure TIDB_ParamsOfRecordTemplateForm.SGClick (Sender: TObject);
var
   AnOldCursor: TCursor;
   AStructureManager: TIDB_StructMan;
   ATable: TDTable;
// ++ Cadmensky
//   sLinkName,
// -- Cadmensky
   sTableName,
      sFieldName: AnsiString;
   iP: Integer;
begin
   DisconnectAllValidators;

   sTableName := SELF.sAttTableName;
   sFieldName := SG.Cells[0, SG.Row];
   {if AnExtTablesMan <> nil then
   begin
      iP := Pos ('_', sFieldName);
      if iP > 0 then
      begin
// ++ Cadmensky
         sTableName := AnExtTablesMan.GetLinkName (StrToInt (Copy (sFieldName, 1, iP - 1)) - 1);
//           sLinkName := AnExtTablesMan.GetLinkName(StrToInt(Copy(sFieldName, 1, iP-1))-1);
//           sTableName := AnExtTablesMan.GetLinkDestTableName(sLinkName);
// -- Cadmensky
         sFieldName := Copy (sFieldName, iP + 1, Length (sFieldName));
      end;
   end; }

   AStructureManager := TIDB_StructMan.Create (AProject, SELF.IDA);
   ATable := TDTable.Create (nil);
   try
      ATable.Master := SELF.ADataQuery.Master;
      ATable.TableName := sTableName;

      AStructureManager.DetectStructure (ATable);
   finally
      ATable.Close;
      ATable.Free;
   end;

   case AStructureManager.GetFieldType (sFieldName) of
      1:
         begin
         end; // String
      2, // SmallInt
      3, // Integer
      12: // Byte
         begin
            IntValidator.Edit := SG.InplaceEditor;
            SG.Cells[1, SG.Row] := '';
         end;
      6, // Float
      7: // Currency
         begin
            FloatValidator.Edit := SG.InplaceEditor;
            SG.Cells[1, SG.Row] := '';
         end;
   end; // Case end
// ++ Cadmensky Version 2.3.2
   AStructureManager.Free;
// -- Cadmensky Version 2.3.2

   if not UseExistingValuesCheckBox.Checked then EXIT;
   AnOldCursor := Screen.Cursor;
   Screen.Cursor := crHourGlass;
   Application.ProcessMessages;
   try
      DrawComboBox;
   finally
      Screen.Cursor := AnOldCursor;
   end;
end;

procedure TIDB_ParamsOfRecordTemplateForm.ComboBox1Change (Sender: TObject);
begin
   SG.Cells[1, SG.Row] := ComboBox1.Text;
end;

procedure TIDB_ParamsOfRecordTemplateForm.UseExistingValuesCheckBoxClick (Sender: TObject);
begin
   if UseExistingValuesCheckBox.Checked then
   begin
      SG.Options := SG.Options - [goEditing, goAlwaysShowEditor];
      SG.HideEditor;
      DrawComboBox;
   end
   else
   begin
      SG.Options := SG.Options + [goEditing, goAlwaysShowEditor];
      DrawComboBox (FALSE);
      Sg.ActivateEditor (1, SG.Row);
   end;
end;

procedure TIDB_ParamsOfRecordTemplateForm.FormKeyUp (Sender: TObject; var Key: Word; Shift: TShiftState);
begin
   bTheUserWantsToCancel := bIAmSelecting and (Key = vk_Escape);
end;

procedure TIDB_ParamsOfRecordTemplateForm.ComboBox1KeyPress (Sender: TObject; var Key: Char);
var
   iFieldType: Integer;
   AStructureManager: TIDB_StructMan;
   ATable: TDTable;
// ++ Cadmensky
//   sLinkName,
// -- Cadmensky
   sTableName,
      sFieldName: AnsiString;
   iP: Integer;
begin
   sTableName := SELF.sAttTableName;
   sFieldName := SG.Cells[0, SG.Row];
   {if AnExtTablesMan <> nil then
   begin
      iP := Pos ('_', sFieldName);
      if iP > 0 then
      begin
// ++ Cadmensky
         sTableName := AnExtTablesMan.GetLinkName (StrToInt (Copy (sFieldName, 1, iP - 1)) - 1);
//           sLinkName := AnExtTablesMan.GetLinkName(StrToInt(Copy(sFieldName, 1, iP-1))-1);
//           sTableName := AnExtTablesMan.GetLinkDestTableName(sLinkName);
// -- Cadmensky
         sFieldName := Copy (sFieldName, iP + 1, Length (sFieldName));
      end;
   end;   }

   AStructureManager := TIDB_StructMan.Create (AProject, SELF.IDA);
   ATable := TDTable.Create (nil);
   try
      ATable.Master := SELF.ADataQuery.Master;
      ATable.TableName := sTableName;

      AStructureManager.DetectStructure (ATable);
   finally
      ATable.Close;
      ATable.Free;
   end;

   iFieldType := AStructureManager.GetFieldType (sFieldName);
   case iFieldType of
      2, // SmallInt
         3, // Integer
         4, // Word
         6, // Float
         7, // Currency
         12, // Byte
         25: // LargeInt
         begin
         // For non-String field we can use only number values.
            if (Key < '0') or (Key > '9') then Key := #0;
         end;
      1, // Text
         24: // WideString
         begin
         // For String field we can write all. We need not to correct the user.
         end;
   else
      begin
         // For all other field types we cannot write anything.
         Key := #0;
      end;
   end; // Case end
end;

procedure TIDB_ParamsOfRecordTemplateForm.OKButtonClick (Sender: TObject);
begin
   if not (UseExistingValuesCheckBox.Checked {AND (SG.Row = 1)}) then
      SG.Cells[1, SG.Row] := SG.InplaceEditor.Text;
end;

end.

