unit IDB_Access2DB;
{
Internal database. Ver. II.
This module provides the access to databases via MS ADO.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 22-01-2001
}
interface

uses
   DMaster, DTables,
   IDB_DatabaseItemsManager, IDB_Consts;

type
   TIDB_Access2DB_Level1 = class
   private
   public
      DMaster: TDMaster;
      constructor Create; virtual;
      destructor Free; virtual;

      function Connect (sDBFileName, sUserName, sPassword: AnsiString; iTypeOfDatabase: Integer = 1; sInitialCatalogName: AnsiString = ''): Boolean;
      procedure Disconnect;

      function UnLoadEmptyDBFile (sDBFileName: AnsiString; AVersion: TDBVersion = dbv2000): Boolean;

      function GetDBFileName: AnsiString;
   end;

   TIDB_Access2DB_Level2 = class (TIDB_Access2DB_Level1)
   public
      ADBItemsMan: TIDB_DatabaseItemsManager;

      constructor Create (AProject: Pointer);
      destructor Free; override;

      function GetStartDatabaseGUIDInformation: AnsiString;
      procedure SetStartDatabaseGUIDInformation (sGUID: AnsiString);
      function GetCurrentDatabaseGUIDInformation: AnsiString;
      procedure SetCurrentDatabaseGUIDInformation (sGUID: AnsiString);
   end;

implementation

uses Classes, Windows, Forms;

////////////////////       TIDB_Access2DB_Level1         ///////////////////////

constructor TIDB_Access2DB_Level1.Create;
begin
   DMaster := TDMaster.Create (nil);
end;

destructor TIDB_Access2DB_Level1.Free;
begin
   SELF.Disconnect;
   DMaster.Free;
end;

{ This procedure connetcts the IDB to the database file. }

function TIDB_Access2DB_Level1.Connect (sDBFileName, sUserName, sPassword: AnsiString; iTypeOfDatabase: Integer = 1; sInitialCatalogName: AnsiString = ''): Boolean;
begin
   RESULT := FALSE;
   Disconnect;
   case iTypeOfDatabase of
      1:
         DMaster.Connection := 'Provider=Microsoft.Jet.OLEDB.4.0;' +
            'Password=' + sPassword + ';' +
            'User ID=' + sUserName + ';' +
            'Data Source=' + sDBFileName + ';' +
            'Mode=ReadWrite|Share Deny None;' +
            'Persist Security Info=True';
      4:
      begin

//         'Provider=SQLOLEDB.1;Data Source=ODISSEY\PROGIS;Initial Catalog=Northwind', 'sa', '_qwerty_', 0);
         DMaster.Connection := 'Provider=SQLOLEDB.1;' +
            'Password=' + sPassword + ';' +
            'User ID=' + sUserName + ';' +
            'Data Source=' + sDBFileName + ';' +
//            'Mode=ReadWrite|Share Deny None;' +
            'Persist Security Info=True';
            if sInitialCatalogName <> '' then
               DMaster.Connection := DMaster.Connection + ';Initial Catalog=' + sInitialCatalogName;
         end;
   end;
   try
      DMaster.Connected := TRUE;
   except
      EXIT;
   end;
   RESULT := TRUE;
end;

procedure TIDB_Access2DB_Level1.Disconnect;
begin
   DMaster.Connected := FALSE;
end;

{ This procedure unload empty database file (mdb) from resources. }

function TIDB_Access2DB_Level1.UnLoadEmptyDBFile (sDBFileName: AnsiString; AVersion: TDBVersion = dbv2000): Boolean;
var
   fsFileStream: TFileStream;
   HRsrc, HMem: THandle;
   Size: DWORD;
   Point: Pointer;
   sResName: AnsiString;
begin
   RESULT := FALSE;

   if AVersion = dbv97 then
      sResName := cs_ResName97
   else
      sResName := cs_ResName2000;

   HRsrc := FindResource (HInstance, PChar (sResName), RT_RCDATA);
   if HRsrc = 0 then
   begin
      Application.MessageBox (PChar ('Resource ''' + sResName + ''' not found.'), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1);
      EXIT;
   end;
   Size := SizeofResource (HInstance, HRsrc);
   if Size = 0 then
   begin
      Application.MessageBox (PChar ('Size of resource ''' + sResName + ''' is zero.'), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1);
      EXIT;
   end;
   HMem := LoadResource (HInstance, HRsrc);
   if HMem = 0 then
   begin
      Application.MessageBox (PChar ('Resource ''' + sResName + ''' cannot be loaded.'), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1);
      EXIT;
   end;
   Point := LockResource (HMem);
   if Point = nil then
   begin
      Application.MessageBox (PChar ('Resource ''' + sResName + ''' cannot be locked.'), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1);
      EXIT;
   end;
   try
      fsFileStream := TFileStream.Create (sDBFileName, fmCreate);
      fsFileStream.Position := 0;
      fsFileStream.Write (Point^, Size);
      fsFileStream.Free;
   except
      EXIT;
   end;
   RESULT := TRUE;
end;

{ This procedure returns the name of database file to that the IDB is currently connected. }

function TIDB_Access2DB_Level1.GetDBFileName: AnsiString;
begin
// ++ Cadmensky
   RESULT := DMaster.DataSource;
//   RESULT := DMaster.Database;
// -- Cadmensky
end;

////////////////////       TIDB_Access2DB_Level2         ///////////////////////

constructor TIDB_Access2DB_Level2.Create (AProject: Pointer);
begin
   inherited Create;
   ADbItemsMan := TIDB_DatabaseItemsManager.Create (AProject, SELF.DMaster);
end;

destructor TIDB_Access2DB_Level2.Free;
begin
   ADbItemsMan.Free;
   inherited Free;
end;

function TIDB_Access2DB_Level2.GetStartDatabaseGUIDInformation: AnsiString;
var
   AnExecQuery: TDQuery;
begin
   RESULT := '';
   SELF.ADbItemsMan.CreateInternalTables;
   AnExecQuery := TDQuery.Create (nil);
   AnExecQuery.Master := SELF.DMaster;
   try
      AnExecQuery.SQL.Clear;
      AnExecQuery.SQL.Add ('SELECT * FROM [' + cs_IT_ST_TextInfo + ']');
      AnExecQuery.SQL.Add ('WHERE SPID = 0');
      AnExecQuery.Open;
      try
         RESULT := AnExecQuery.FieldByName (cs_SPFieldName).AsString;
      finally
         AnExecQuery.Close;
      end;
   finally
      AnExecQuery.Free;
   end;
end;

procedure TIDB_Access2DB_Level2.SetStartDatabaseGUIDInformation (sGUID: AnsiString);
var
   AnExecQuery: TDQuery;
begin
   SELF.ADbItemsMan.CreateInternalTables;
   AnExecQuery := TDQuery.Create (nil);
   AnExecQuery.Master := SELF.DMaster;
   // Is there the record with GUID information?
   try
      AnExecQuery.SQL.Clear;
      AnExecQuery.SQL.Add ('SELECT * FROM [' + cs_IT_ST_TextInfo + ']');
      AnExecQuery.SQL.Add ('WHERE SPID = 0');
      AnExecQuery.Open;
      if AnExecQuery.RecordCount > 0 then
      begin
         AnExecQuery.Close;
         AnExecQuery.SQL.Clear;
         // UPDATE table_name SET field_name = new_value WHERE key_field = key_value
         AnExecQuery.SQL.Add ('UPDATE [' + cs_IT_ST_TextInfo + ']');
         AnExecQuery.SQL.Add ('SET [' + cs_SPFieldName + '] = ''' + sGUID + '''');
         AnExecQuery.SQL.Add ('WHERE SPID = 0');
      end
      else
      begin
         AnExecQuery.Close;
         AnExecQuery.SQL.Clear;
         // INSERT INTO table_name (field_name_1, field_name_2) VALUES (value_1, value_2)
         AnExecQuery.SQL.Add ('INSERT INTO [' + cs_IT_ST_TextInfo + '] (SPID, [' + cs_SPFieldName + '])');
         AnExecQuery.SQL.Add ('VALUES (0, ''' + sGUID + ''')');
      end;
      AnExecQuery.ExecSQL;
   finally
      AnExecQuery.Free;
   end;
end;

function TIDB_Access2DB_Level2.GetCurrentDatabaseGUIDInformation: AnsiString;
var
   AnExecQuery: TDQuery;
begin
   RESULT := '';
   SELF.ADbItemsMan.CreateInternalTables;
   AnExecQuery := TDQuery.Create (nil);
   AnExecQuery.Master := SELF.DMaster;
   try
      AnExecQuery.SQL.Clear;
      AnExecQuery.SQL.Add ('SELECT * FROM [' + cs_IT_ST_TextInfo + ']');
      AnExecQuery.SQL.Add ('WHERE SPID = 1');
      AnExecQuery.Open;
      try
         RESULT := AnExecQuery.FieldByName (cs_SPFieldName).AsString;
      finally
         AnExecQuery.Close;
      end;
   finally
      AnExecQuery.Free;
   end;
end;

procedure TIDB_Access2DB_Level2.SetCurrentDatabaseGUIDInformation (sGUID: AnsiString);
var
   AnExecQuery: TDQuery;
begin
   SELF.ADbItemsMan.CreateInternalTables;
   // Is there the record with GUID information?
   AnExecQuery := TDQuery.Create (nil);
   AnExecQuery.Master := SELF.DMaster;
   try
      AnExecQuery.SQL.Clear;
      AnExecQuery.SQL.Add ('SELECT * FROM [' + cs_IT_ST_TextInfo + ']');
      AnExecQuery.SQL.Add ('WHERE SPID = 1');
      AnExecQuery.Open;
      if AnExecQuery.RecordCount > 0 then
      begin
         AnExecQuery.Close;
         AnExecQuery.SQL.Clear;
         // UPDATE table_name SET field_name = new_value WHERE key_field = key_value
         AnExecQuery.SQL.Add ('UPDATE [' + cs_IT_ST_TextInfo + ']');
         AnExecQuery.SQL.Add ('SET [' + cs_SPFieldName + '] = ''' + sGUID + '''');
         AnExecQuery.SQL.Add ('WHERE SPID = 1');
      end
      else
      begin
         AnExecQuery.Close;
         AnExecQuery.SQL.Clear;
         // INSERT INTO table_name (field_name_1, field_name_2) VALUES (value_1, value_2)
         AnExecQuery.SQL.Add ('INSERT INTO [' + cs_IT_ST_TextInfo + '] (SPID, [' + cs_SPFieldName + '])');
         AnExecQuery.SQL.Add ('VALUES (1, ''' + sGUID + ''')');
      end;
      AnExecQuery.ExecSQL;
   finally
      AnExecQuery.Free;
   end;
end;

end.

