unit IDB_ParamsOfSaveLoad;

interface

uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   ExtCtrls, StdCtrls, Gauges, ComCtrls, Grids, Menus, Buttons,
   IDB_DataMan, IDB_Access2DB, IDB_StructMan, IDB_Grids, IDB_C_StringGrid;

type
   TDialogWorkModeNew = (wmLoad, wmSave);
   TIDB_ParamsOfSaveLoadForm = class (TForm)
      PageControl1: TPageControl;
      TabSheet1: TTabSheet;
      GroupBox1: TGroupBox;
      DelimTypeComboBox: TComboBox;
      DelimEdit: TEdit;
      RadioButton1: TRadioButton;
      RadioButton2: TRadioButton;
      UseTheFirstLineAsFieldNamesCheckBox: TCheckBox;
      ScanFileCheckBox: TCheckBox;
      RewriteDataCheckBox: TCheckBox;
      InsertRecordCheckBox: TCheckBox;
      UseConvertingCheckBox: TCheckBox;
      TabSheet2: TTabSheet;
      Panel1: TPanel;
      Label1: TLabel;
      AddFieldsSB: TSpeedButton;
      ProgisIDSourceColumnComboBox: TComboBox;
      TabSheet3: TTabSheet;
      SpeedButton1: TSpeedButton;
      SpeedButton2: TSpeedButton;
      SpeedButton3: TSpeedButton;
      SpeedButton4: TSpeedButton;
      AvailableFieldsListBox: TListBox;
      SelectedFieldsListBox: TListBox;
      PopupMenu1: TPopupMenu;
      Add_PUMenu: TMenuItem;
      Gauge1: TGauge;
      OKButton: TButton;
      CancelButton: TButton;
      Delete_PUMenu: TMenuItem;
    StringGrid1: TIDB_StringGrid;
    SourceComboBox: TComboBox;
    LinkIDComboBox: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
      procedure AddFieldsSBClick (Sender: TObject);
      procedure FormShow (Sender: TObject);
      procedure FormCreate (Sender: TObject);
      procedure StringGrid1DrawCell (Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
      procedure FormDestroy (Sender: TObject);
      procedure Add_PUMenuClick (Sender: TObject);
      procedure SourceComboBoxChange (Sender: TObject);
      procedure Delete_PUMenuClick (Sender: TObject);
      procedure SpeedButton1Click (Sender: TObject);
      procedure SpeedButton2Click (Sender: TObject);
      procedure SpeedButton3Click (Sender: TObject);
      procedure SpeedButton4Click (Sender: TObject);
      procedure OKButtonClick (Sender: TObject);
      procedure StringGrid1GetEditText (Sender: TObject; ACol, ARow: Integer;
         var Value: string);
      procedure StringGrid1SetEditText (Sender: TObject; ACol, ARow: Integer;
         const Value: string);
      procedure StringGrid1SelectCell (Sender: TObject; ACol, ARow: Integer;
         var CanSelect: Boolean);
      procedure ProgisIDSourceColumnComboBoxChange (Sender: TObject);
      procedure UseTheFirstLineAsFieldNamesCheckBoxClick (Sender: TObject);
      procedure ScanFileCheckBoxClick (Sender: TObject);
      procedure RewriteDataCheckBoxClick (Sender: TObject);
      procedure InsertRecordCheckBoxClick (Sender: TObject);
      procedure RadioButton1Click (Sender: TObject);
      procedure DelimTypeComboBoxChange (Sender: TObject);
      procedure RadioButton2Click (Sender: TObject);
      procedure DelimEditChange (Sender: TObject);
      procedure UseConvertingCheckBoxClick (Sender: TObject);
      procedure StringGrid1TopLeftChanged (Sender: TObject);
      procedure SourceComboBoxClick (Sender: TObject);
    procedure LinkIDComboBoxChange(Sender: TObject);
   private
      ADataManager: TIDB_DBDataManager;
      wmWorkMode: TDialogWorkModeNew;
      slSourceFields, slDestFields: TStringList;
      sOldFieldName: AnsiString;
      procedure FillGridWithDestinationAndSourcesFieldNames; // (listTheIDBTableColumns: TStringList);
      procedure GoAnalyse;
      procedure ClearLinkedDataDisplaying;
   public
      constructor Create (AOwner: TComponent; ADataMan: TIDB_DBDataManager; wmDialogWorkMode: TDialogWorkModeNew; IDA: TIDB_Access2DB_Level2; sSourceTableName, sDestTableName: AnsiString);
      procedure MakeWindowForLoadingFromTextFile;
      procedure MakeWindowForLoadingFromDBTable (bWeAreUsingMSAccessFileAsSource: Boolean);
      procedure MakeWindowForSavingToDBTable (bWeAreUsingMSAccessFileAsDestination: Boolean);
      procedure MakeWindowForSavingInTextFile;
   end;

implementation

{$R *.DFM}

uses
   IDB_Utils, IDB_Consts, IDB_2ParamsDialog, IDB_LanguageManager, DTables,
   IDB_SelectInListBox;

constructor TIDB_ParamsOfSaveLoadForm.Create (AOwner: TComponent; ADataMan: TIDB_DBDataManager; wmDialogWorkMode: TDialogWorkModeNew; IDA: TIDB_Access2DB_Level2; sSourceTableName, sDestTableName: AnsiString);
var
   ATable: TDTable;
//   pLC: PRecLinkedColumns;
   iI: integer;
begin
   inherited Create (AOwner);
   SELF.ADataManager := ADataMan;
   SELF.wmWorkMode := wmDialogWorkMode;
     // Set caption of the form.
   case wmDialogWorkMode of
      wmLoad: SELF.Caption := ALangManager.GetValue (66); // Parameters of loading
      wmSave: SELF.Caption := ALangManager.GetValue (63); // Parameters of saving
   end; // Case end

   slSourceFields := TStringList.Create;
   slDestFields := TStringList.Create;

   if SELF.wmWorkMode = wmSave then
   begin
      slSourceFields.Text := SELF.ADataManager.GetTheIDBTableColumnsNames (true);
   end;
   if SELF.wmWorkMode = wmLoad then
   begin
      ATable := TDTable.Create (nil);
      ATable.Master := IDA.DMaster;

      if not (SELF.ADataManager.wmWorkMode = wmTextFile) then
         try
            ATable.TableName := sSourceTableName;
            ATable.Open;
            for iI := 0 to ATable.Fields.Count - 1 do
               if not FieldIsInternalUsageField (ATable.Fields[iI].FieldName) then
                  slSourceFields.Add (ATable.Fields[iI].FieldName);

            ATable.Close;
         except
         end;

      try
         ATable.TableName := sDestTableName;
         ATable.Open;

         for iI := 0 to ATable.Fields.Count - 1 do
         begin
            SELF.ADataManager.AddLinkedData ('', ATable.Fields[iI].FieldName);
            if (AnsiUpperCase (ATable.Fields[iI].FieldName) <> 'PROGISID') and
               (AnsiUpperCase (ATable.Fields[iI].FieldName) <> 'GUID') and
               not FieldIsInternalUsageField (ATable.Fields[iI].FieldName) and
               not FieldIsCalculatedField (ATable.Fields[iI].FieldName) then
               slDestFields.Add (ATable.Fields[iI].FieldName);
         end;
         ATable.Close;
      except
      end;
      ATable.Free;
   end;
end;

procedure TIDB_ParamsOfSaveLoadForm.FormDestroy (Sender: TObject);
begin
   slSourceFields.Free;
   slDestFields.Free;
end;

procedure TIDB_ParamsOfSaveLoadForm.AddFieldsSBClick (Sender: TObject);
var
   SILBF: TIDB_SelectInListBoxForm;
//   Dialog: TIDB_2ParamsDialogForm;
//   listTheIDBTableColumns: TStringList;
//   bRes: Boolean;
   iI: integer;
//   iIndex: integer;
//   pLC: ^TrecLinkedColumns;
   sSelectedFieldName: AnsiString;
begin
//   if SELF.wmWorkMode <> wmTextFile then
//   begin
   SILBF := TIDB_SelectInListBoxForm.Create (Application);
   SILBF.Label1.Caption := ALangManager.GetValue (108);
   SILBF.ListBox1.OnDblClick := nil;
   SILBF.ListBox1.MultiSelect := TRUE;
   SILBF.OKButton.Enabled := FALSE;
   SILBF.ListBox1.Items.Clear;
   SILBF.ListBox1.Items.Text := SELF.slSourceFields.Text;
   if SILBF.ListBox1.Items.Count < 1 then
   begin // There isn't any suitable field in the data source...
      MessageBox (Application.Handle, PChar (ALangManager.GetValue (109)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
      EXIT;
   end;
   if SILBF.ShowModal <> mrOK then
   begin
      SILBF.Free;
      EXIT;
   end;

   for iI := 0 to SILBF.ListBox1.Items.Count - 1 do
   begin
      if SILBF.ListBox1.Selected[iI] then
      begin
         sSelectedFieldName := SILBF.ListBox1.Items[iI];
         if (AnsiUpperCase (sSelectedFieldName) <> 'PROGISID') and
            (AnsiUpperCase (sSelectedFieldName) <> 'GUID') and
            not FieldIsInternalUsageField (sSelectedFieldName) and
            not FieldIsCalculatedField (sSelectedFieldName) then
            SELF.ADataManager.AddLinkedData (sSelectedFieldName, sSelectedFieldName)
         else
            SELF.ADataManager.AddLinkedData (sSelectedFieldName, SELF.ADataManager.GenerateUniqueFieldName (sSelectedFieldName));
      end;
{      iIndex := SELF.FindDestinationColumn (sSelectedFieldName);
//         SELF.slSourceFields.IndexOf (SELF.slDestFields[iI]);
      if iIndex <> -1 then
      begin
         pLC := listLinkedColumns[iIndex];
         pLC.sSourceColumnName := sSelectedFieldName;
      end
      else
      begin
//         StringGrid1.RowCount := StringGrid1.RowCount + 1;
         SELF.AddLinkedData (sSelectedFieldName, AStructMan.GenerateFieldName (sSelectedFieldName));
      end;}
   end;
{      Dialog := TIDB_2ParamsDialogForm.Create (SELF);
      Dialog.Label1.Caption := ALangManager.GetValue (83); // Adding of new field
      Dialog.RadioButton1.Caption := ALangManager.GetValue (84); // Run table structure editor
      Dialog.RadioButton2.Caption := ALangManager.GetValue (85); // Create new field as based on existed in data source
      if Dialog.ShowModal <> mrOK then EXIT;
      if Dialog.RadioButton1.Checked then
           // Run Structure Manager of destination table.
         bRes := SELF.ADataManager.ShowTableStructure
      else
           // Create a new field using data about existing field in the source.
         bRes := SELF.ADataManager.WorkWithSourceTableStructure;
      Dialog.Free;}
//   end
//   else
        // Run Structure Manager of destination table. For a text file data fields doesn't exist. :-ro)
//      bRes := SELF.ADataManager.ShowTableStructure;
//   if bRes then
//   begin
//      listTheIDBTableColumns := TStringList.Create;
//      listTheIDBTableColumns.Text := ADataManager.GetTheIDBTableColumnsNames;
//      StringGrid1.RowCount := listLinkedColumns.Count + 1;
   SELF.FillGridWithDestinationAndSourcesFieldNames; // (SELF.slDestFields);
//   end;
end;

{ This procedure is called when I have added some new fields in an attribute table
  and want to display them to an user. As I can add new fields by 2 ways (by calling
  the attribute table structure manager and by using data of fields in a source table)
  I have to call this procedure in both case. Besides, when I've added new fields
  using data of fields in the source, I set links between destination and sources fields.
  I also have to display these links.}

procedure TIDB_ParamsOfSaveLoadForm.FillGridWithDestinationAndSourcesFieldNames; // (listTheIDBTableColumns: TStringList);
var
   iP, iI: Integer;
   pLC: PRecLinkedColumns;
begin
   iP := 1;
   for iI := 0 to SELF.ADataManager.GetLinkedDataCount - 1 do
   begin
      pLC := SELF.ADataManager.GetLinkedData (iI);
         // We haven't a necessity in a showing the special fields for an user.
      if not (AnsiUpperCase (pLC.sDestinationColumnName) = 'PROGISID') and
         not (AnsiUpperCase (pLC.sDestinationColumnName) = 'GUID') and
         not (FieldIsInternalUsageField (pLC.sDestinationColumnName)) and
         not (FieldIsCalculatedField (pLC.sDestinationColumnName)) then
      begin
         Inc (iP);
      end;
   end;
   if StringGrid1.RowCount > iP then
   begin
      SourceComboBox.Visible := false;
      StringGrid1.RowCount := 2;
   end;
   if iP > 1 then
      StringGrid1.RowCount := iP;
     // Display data about fields of the destination attribute table.
   iP := 1;
//   iI := 0;
   for iI := 0 to SELF.ADataManager.GetLinkedDataCount - 1 do

{   pLC := SELF.ADataManager.GetLinkedData (iI);
   while pLC <> nil do}
   begin
      pLC := SELF.ADataManager.GetLinkedData (iI);
         // We haven't a necessity in a showing the special fields for an user.
      if not (AnsiUpperCase (pLC.sDestinationColumnName) = 'PROGISID') and
         not (AnsiUpperCase (pLC.sDestinationColumnName) = 'GUID') and
         not (FieldIsInternalUsageField (pLC.sDestinationColumnName)) and
         not (FieldIsCalculatedField (pLC.sDestinationColumnName)) then
      begin
{         if StringGrid1.RowCount < iP then
            StringGrid1.RowCount := iP;}
         StringGrid1.Cells[0, iP] := pLC.sDestinationColumnName;
         StringGrid1.Cells[1, iP] := pLC.sSourceColumnName;
         Inc (iP);
      end;
//      Inc (iI);
//      pLC := SELF.ADataManager.GetLinkedData (iI);
   end;
// ++ Cadmensky Version 2.3.2
   if iP < 2 then
      SourceComboBox.Visible := false;
// -- Cadmensky Version 2.3.2
//   StringGrid1.RowCount := iP;
     // Display data about links between source and destination fields.
{   for iI := 1 to StringGrid1.RowCount - 1 do
      StringGrid1.Cells[1, iI] := SELF.GetSourceColumnName (StringGrid1.Cells[0, iI]);}
   iP := SELF.SourceComboBox.Items.IndexOf (StringGrid1.Cells[1, StringGrid1.Row]);
   if iP < 0 then iP := 0;
   SELF.SourceComboBox.ItemIndex := iP;
// ++ Cadmensky Version 2.3.8
   StringGrid1.Invalidate;
// -- Cadmensky Version 2.3.8
end;

procedure TIDB_ParamsOfSaveLoadForm.MakeWindowForLoadingFromTextFile;
begin
   SELF.TabSheet3.TabVisible := FALSE;
   SELF.UseConvertingCheckBox.Caption := ALangManager.GetValue (74); // Use converting data from OEM code page to ANSI
     // Set visible properties only. Positions are developed.
   SELF.GroupBox1.Visible := TRUE;
   SELF.UseTheFirstLineAsFieldNamesCheckBox.Visible := TRUE;
   SELF.ScanFileCheckBox.Visible := TRUE;
   SELF.RewriteDataCheckBox.Visible := TRUE;
   SELF.InsertRecordCheckBox.Visible := TRUE;
   SELF.UseConvertingCheckBox.Visible := TRUE;
   SELF.PageControl1.ActivePage := TabSheet2;
end;

procedure TIDB_ParamsOfSaveLoadForm.MakeWindowForLoadingFromDBTable (bWeAreUsingMSAccessFileAsSource: Boolean);
var
   iP: Integer;
begin
   SELF.TabSheet3.TabVisible := FALSE;
   SELF.UseConvertingCheckBox.Caption := ALangManager.GetValue (74); // Use converting from OEM to ANSI.
     // Set visible properties.
   SELF.GroupBox1.Visible := FALSE;
   SELF.UseTheFirstLineAsFieldNamesCheckBox.Visible := FALSE;
   SELF.ScanFileCheckBox.Visible := FALSE;
   SELF.RewriteDataCheckBox.Visible := true;
// ++ Cadmensky
   SELF.RewriteDataCheckBox.Checked := true;
   SELF.RewriteDataCheckBox.Enabled := false;
// -- Cadmensky
   SELF.InsertRecordCheckBox.Visible := TRUE;
// ++ Cadmensky
   SELF.InsertRecordCheckBox.Checked := TRUE;
// -- Cadmensky
   SELF.UseConvertingCheckBox.Visible := not bWeAreUsingMSAccessFileAsSource;
   SELF.PageControl1.ActivePage := TabSheet2;
     // Calculate new positions.
   iP := ScanFileCheckBox.Top - UseTheFirstLineAsFieldNamesCheckBox.Top;
   RewriteDataCheckBox.Top := GroupBox1.Top;
   InsertRecordCheckBox.Top := RewriteDataCheckBox.Top + iP;
   UseConvertingCheckBox.Top := InsertRecordCheckBox.Top + 2 * iP;
   TabSheet1.Caption := ALangManager.GetValue (79);
end;

procedure TIDB_ParamsOfSaveLoadForm.MakeWindowForSavingToDBTable (bWeAreUsingMSAccessFileAsDestination: Boolean);
begin
   SELF.TabSheet2.TabVisible := FALSE;
   SELF.UseConvertingCheckBox.Caption := ALangManager.GetValue (80); // Use converting data from ANSI code page to OEM
     // Set visible properties.
   if not bWeAreUsingMSAccessFileAsDestination then
   begin
      SELF.GroupBox1.Visible := FALSE;
      SELF.UseTheFirstLineAsFieldNamesCheckBox.Visible := FALSE;
      SELF.ScanFileCheckBox.Visible := FALSE;
      SELF.RewriteDataCheckBox.Visible := FALSE;
      SELF.InsertRecordCheckBox.Visible := FALSE;
      SELF.UseConvertingCheckBox.Visible := TRUE;
        // Calcaulate new positions.
      SELF.UseConvertingCheckBox.Top := SELF.GroupBox1.Top;
   end
   else
      SELF.TabSheet1.TabVisible := FALSE;
   SELF.PageControl1.ActivePage := TabSheet3;
end;

procedure TIDB_ParamsOfSaveLoadForm.MakeWindowForSavingInTextFile;
begin
   SELF.TabSheet2.TabVisible := FALSE;
   SELF.UseConvertingCheckBox.Caption := ALangManager.GetValue (80); // Use converting data from ANSI code page to OEM
     // Set visible properties.
   SELF.GroupBox1.Visible := TRUE;
   SELF.UseTheFirstLineAsFieldNamesCheckBox.Visible := TRUE;
   SELF.ScanFileCheckBox.Visible := FALSE;
   SELF.RewriteDataCheckBox.Visible := FALSE;
   SELF.InsertRecordCheckBox.Visible := FALSE;
   SELF.UseConvertingCheckBox.Visible := TRUE;
   SELF.PageControl1.ActivePage := TabSheet3;
     // Calculate new positions.
   SELF.UseTheFirstLineAsFieldNamesCheckBox.Top := SELF.InsertRecordCheckBox.Top;
end;

procedure TIDB_ParamsOfSaveLoadForm.FormShow (Sender: TObject);
//var
//   iI: integer;
begin
   SELF.Gauge1.Visible := FALSE;
   DelimTypeComboBox.ItemIndex := 0;
   if ADataManager <> nil then
      UseConvertingCheckBox.Checked := SELF.ADataManager.GetCodePage <> cpANSI;
   case SELF.wmWorkMode of
      wmLoad:
         begin
            StringGrid1.DefaultColWidth := ((StringGrid1.ClientWidth - 1) div 2) - 1;

//            SELF.slSourceFields.Text := ADataManager.GetTheIDBTableColumnsNames;
            StringGrid1.Cells[0, 0] := ALangManager.GetValue (81); // Destination columns
            StringGrid1.Cells[1, 0] := ALangManager.GetValue (82); // Source columns
            StringGrid1.RowCount := SELF.slDestFields.Count + 1;
            SELF.FillGridWithDestinationAndSourcesFieldNames; // (SELF.slSourceFields);
// ++ Cadmensky
            if StringGrid1.RowCount < 2 then
               StringGrid1.RowCount := 2;
// -- Cadmensky
            if StringGrid1.RowCount > 1 then
               StringGrid1.FixedRows := 1;
{            else
            begin
               Panel1.Enabled := FALSE;
               StringGrid1.Enabled := FALSE;
               OKButton.Enabled := FALSE;
            end;}
//            StringGrid1.DefaultColWidth := (StringGrid1.ClientWidth div 2) - 1;
            StringGrid1.Col := 1;
            StringGrid1.Row := 1;
            SELF.GoAnalyse;
         end;
      wmSave:
         begin
            SELF.AvailableFieldsListBox.Items.Text := SELF.slSourceFields.Text;
            SELF.AvailableFieldsListBox.ItemIndex := 0;
            SELF.OKButton.Enabled := FALSE;
         end;
   end;
end;

procedure TIDB_ParamsOfSaveLoadForm.FormCreate (Sender: TObject);
begin
   ALangManager.MakeReplacement (SELF);
end;

procedure TIDB_ParamsOfSaveLoadForm.GoAnalyse;
var
//   iI: Integer;
//   listSourceColumns: TStringList;
   sProgisIDFieldName: AnsiString;
   pLC: PRecLinkedColumns;
   iI : Integer;
begin
   PageControl1.Enabled := FALSE;
   SourceComboBox.Items.Clear;
// SELF.ClearLinkedDataDisplaying;
   try
     // Before I shall do the analysis, I clear all previous data.
//      ADataManager.ClearLinkedData;
     // Do analysis.
      if SELF.ADataManager.wmWorkMode = wmTextFile then
      begin
         ADataManager.AnalyseDataFile (Gauge1);
         slSourceFields.Text := SELF.ADataManager.GetGuaranteedExistingColumnsNames;
         ProgisIDSourceColumnComboBox.Items.Text := slSourceFields.Text;

      end
// ++ Cadmensky Version 2.3.2
      else
         ProgisIDSourceColumnComboBox.Items.Text := slSourceFields.Text;//SELF.ADataManager.GetIntegerTypeFieldNames (slSourceFields.Text);

   for iI := 0 to SELF.ADataManager.GetLinkedDataCount - 1 do
   begin
      pLC := SELF.ADataManager.GetLinkedData (iI);
         // We haven't a necessity in a showing the special fields for an user.
      if not (FieldIsInternalUsageField (pLC.sDestinationColumnName)) and
         not (FieldIsCalculatedField (pLC.sDestinationColumnName)) then
         self.LinkIDComboBox.Items.Add(pLC.sDestinationColumnName);
   end;


// -- Cadmensky Version 2.3.2
// ++ Commented by Cadmensky Version 2.3.2
//      ProgisIDSourceColumnComboBox.Items.Text := slSourceFields.Text;
// -- Commented by Cadmensky Version 2.3.2
//      ProgisIDSourceColumnComboBox.Items.Text := SELF.ADataManager.GetGuaranteedExistingColumnsNames;
      sProgisIDFieldName := SELF.ADataManager.GetColumnWithProgisIDData;
     // I try set TComboBox on position with name of required column.
      ProgisIDSourceColumnComboBox.ItemIndex := ProgisIDSourceColumnComboBox.Items.IndexOf (sProgisIDFieldName);
      LinkIDComboBox.ItemIndex:= LinkIDComboBox.Items.IndexOf ('PROGISID');
     // If it failed, I try to find in TComboBox 'ProgisID' column.
      if ProgisIDSourceColumnComboBox.ItemIndex < 0 then
         ProgisIDSourceColumnComboBox.ItemIndex := ProgisIDSourceColumnComboBox.Items.IndexOf ({'ProgisID'}'PROGISID');
     // If it failed, I set TCombox on first position.
      if ProgisIDSourceColumnComboBox.ItemIndex < 0 then
         ProgisIDSourceColumnComboBox.ItemIndex := 0;
     if LinkIDComboBox.ItemIndex < 0 then
          LinkIDComboBox.ItemIndex :=0 ;
     // And I set column that contain ProgisID data. If this isn't required column, user has a possibilty
     // to change this column later.
//      sColumnWithProgisIDData := ProgisIDSourceColumnComboBox.Items[ProgisIDSourceColumnComboBox.ItemIndex];
      sProgisIDFieldName := ProgisIDSourceColumnComboBox.Items[ProgisIDSourceColumnComboBox.ItemIndex];
      SELF.ADataManager.SetColumnWithProgisIDData (sProgisIDFieldName);
//      SourceComboBox.Items.Clear;
//      slDestFields.Text := ADataManager.GetSourceColumnsNames;
//      SourceComboBox.Items.Add ('');
      SourceComboBox.Items.Text := slSourceFields.Text;
      SourceComboBox.Items.Insert (0, '');
   finally
      PageControl1.Enabled := TRUE;
   end;
end;

{Procedure clears all data about linked columns in right column of StringGrid1.}

procedure TIDB_ParamsOfSaveLoadForm.ClearLinkedDataDisplaying;
var
   iI: Integer;
begin
     // Clear data in second column. First column contains data about columns of
     // a destination table. First line (index 0) contains header and is fixed line.
   for iI := 1 to StringGrid1.RowCount - 1 do
      StringGrid1.Cells[1, iI] := '';
end;

procedure TIDB_ParamsOfSaveLoadForm.StringGrid1DrawCell (Sender: TObject;
   ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
   iP: Integer;
   ComboBoxRect: TRect;
begin
   if StringGrid1.DefaultRowHeight * StringGrid1.RowCount > StringGrid1.Height then
      StringGrid1.ScrollBars := ssVertical
   else
      StringGrid1.ScrollBars := ssNone;

   if StringGrid1.ScrollBars = ssVertical then
      StringGrid1.ColWidths[1] := StringGrid1.DefaultColWidth - GetSystemMetrics (SM_CXVSCROLL)
   else
      StringGrid1.ColWidths[1] := StringGrid1.DefaultColWidth;

   if ACol = 0 then
      EXIT;
   if ARow <> StringGrid1.Row then
      EXIT;

   if ARow > 0 then
   begin
      ComboBoxRect := SELF.StringGrid1.CellRect (1, StringGrid1.Row);
      SourceComboBox.Left := StringGrid1.Left + ComboBoxRect.Left;
      SourceComboBox.Top := StringGrid1.Top + ComboBoxRect.Top + 1;
      SourceComboBox.Width := ComboBoxRect.Right - ComboBoxRect.Left + 4;
      if SourceComboBox.Left + SourceComboBox.Width > StringGrid1.ClientWidth + StringGrid1.Left then
         SourceComboBox.Width := StringGrid1.ClientWidth + StringGrid1.Left - SourceComboBox.Left;
      SourceComboBox.Height := ComboBoxRect.Bottom - ComboBoxRect.Top;
   end;

   iP := SourceComboBox.Items.IndexOf (StringGrid1.Cells[1, ARow]);
   if iP < 0 then
      iP := 0;
   SourceComboBox.ItemIndex := iP;
end;

procedure TIDB_ParamsOfSaveLoadForm.Add_PUMenuClick (Sender: TObject);
begin
   SELF.StringGrid1.RowCount := SELF.StringGrid1.RowCount + 1;
   SELF.StringGrid1.Cells[0, SELF.StringGrid1.RowCount - 1] := SELF.ADataManager.GenerateUniqueFieldName (ALangManager.GetValue (122));
   SELF.StringGrid1.Cells[1, SELF.StringGrid1.RowCount - 1] := '';
   SELF.ADataManager.AddLinkedData ('', SELF.StringGrid1.Cells[0, SELF.StringGrid1.RowCount - 1]);
end;

procedure TIDB_ParamsOfSaveLoadForm.SourceComboBoxChange (Sender: TObject);
var
   sSourceColumnName, sDestinationColumnName: AnsiString;
//   pLC: PRecLinkedColumns;
//   iP: integer;
begin
   sSourceColumnName := SourceComboBox.Items[SourceComboBox.ItemIndex];
   sDestinationColumnName := StringGrid1.Cells[0, StringGrid1.Row];
   StringGrid1.Cells[1, StringGrid1.Row] := sSourceColumnName;
   if SourceComboBox.ItemIndex > 0 then
      SELF.ADataManager.AddLinkedData (sSourceColumnName, sDestinationColumnName)
   else
      if SELF.slDestFields.IndexOf (sDestinationColumnName) = -1 then
         SELF.ADataManager.DeleteDestinationLinkedData (sDestinationColumnName);
end;

procedure TIDB_ParamsOfSaveLoadForm.Delete_PUMenuClick (Sender: TObject);
var
   sFieldName: AnsiString;
//   iP: integer;
begin
// ++ Cadmensky Version 2.3.9
   if StringGrid1.RowCount < 3 then
      Exit;
// -- Cadmensky Version 2.3.9
   sFieldName := SELF.StringGrid1.Cells[0, StringGrid1.Row];
   SELF.ADataManager.DeleteDestinationLinkedData (sFieldName);
   if SELF.slDestFields.IndexOf (sFieldName) <> -1 then
      SELF.slDestFields.Delete (SELF.slDestFields.IndexOf (sFieldName));
   SELF.FillGridWithDestinationAndSourcesFieldNames;
end;

procedure TIDB_ParamsOfSaveLoadForm.SpeedButton1Click (Sender: TObject);
begin
   AddOneValue (SELF.AvailableFieldsListBox, SELF.SelectedFieldsListBox);
   OKButton.Enabled := SELF.SelectedFieldsListBox.Items.Count > 0;
end;

procedure TIDB_ParamsOfSaveLoadForm.SpeedButton2Click (Sender: TObject);
begin
   AddAllValues (SELF.AvailableFieldsListBox, SELF.SelectedFieldsListBox);
   OKButton.Enabled := SELF.SelectedFieldsListBox.Items.Count > 0;
end;

procedure TIDB_ParamsOfSaveLoadForm.SpeedButton3Click (Sender: TObject);
begin
   AddOneValue (SELF.SelectedFieldsListBox, SELF.AvailableFieldsListBox);
   OKButton.Enabled := SELF.SelectedFieldsListBox.Items.Count > 0;
end;

procedure TIDB_ParamsOfSaveLoadForm.SpeedButton4Click (Sender: TObject);
begin
   AddAllValues (SELF.SelectedFieldsListBox, SELF.AvailableFieldsListBox);
   OKButton.Enabled := SELF.SelectedFieldsListBox.Items.Count > 0;
end;

procedure TIDB_ParamsOfSaveLoadForm.OKButtonClick (Sender: TObject);
var
   iI: Integer;
begin
   if SELF.wmWorkMode = wmSave then
   begin
        // Put all names of selected fields into DataManager.
      SELF.ADataManager.ClearLinkedData;
      for iI := 0 to SELF.SelectedFieldsListBox.Items.Count - 1 do
         SELF.ADataManager.AddLinkedData (SelectedFieldsListBox.Items[iI], SelectedFieldsListBox.Items[iI]);
   end;
   if SELF.wmWorkMode = wmLoad then
      for iI := 1 to StringGrid1.RowCount - 1 do
         if (StringGrid1.Cells[0, iI] = '') and (slDestFields.IndexOf (StringGrid1.Cells[0, iI]) = -1) then
            SELF.ADataManager.DeleteDestinationLinkedData (StringGrid1.Cells[0, iI]);
end;

procedure TIDB_ParamsOfSaveLoadForm.StringGrid1GetEditText (Sender: TObject; ACol, ARow: Integer; var Value: string);
begin
   SELF.sOldFieldName := Value;
end;

procedure TIDB_ParamsOfSaveLoadForm.StringGrid1SetEditText (Sender: TObject; ACol, ARow: Integer; const Value: string);
begin
   SELF.ADataManager.DeleteDestinationLinkedData (SELF.sOldFieldName);
   SELF.ADataManager.AddLinkedData (StringGrid1.Cells[1, ARow], Value);
   SELF.sOldFieldName := Value;
end;

procedure TIDB_ParamsOfSaveLoadForm.StringGrid1SelectCell (Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
var
   ComboBoxRect: TRect;
   iP: integer;
begin
// ++ Cadmensky Version 2.3.2
   if ARow = 0 then
      CanSelect := false;
// -- Cadmensky Version 2.3.2

   if (ACol = 0) and (ARow > 0) {and
      (slDestFields.IndexOf (StringGrid1.Cells[0, ARow]) = -1)}then
   begin
      StringGrid1.Options := StringGrid1.Options + [goEditing];
      SELF.Delete_PUMenu.Enabled := true;
   end
   else
   begin
      StringGrid1.Options := StringGrid1.Options - [goEditing];
      SELF.Delete_PUMenu.Enabled := false;
   end;
   if (ARow > 0) then
   begin
      iP := SELF.SourceComboBox.Items.IndexOf (StringGrid1.Cells[1, ARow]);
      if iP < 0 then
         iP := 0;
      SELF.SourceComboBox.ItemIndex := iP;

      SourceComboBox.Visible := true;
      ComboBoxRect := SELF.StringGrid1.CellRect (1, ARow);
      SourceComboBox.Left := StringGrid1.Left + ComboBoxRect.Left;
      SourceComboBox.Top := StringGrid1.Top + ComboBoxRect.Top + 1;
      SourceComboBox.Width := ComboBoxRect.Right - ComboBoxRect.Left + 4;
      if SourceComboBox.Left + SourceComboBox.Width > StringGrid1.ClientWidth + StringGrid1.Left then
         SourceComboBox.Width := StringGrid1.ClientWidth + StringGrid1.Left - SourceComboBox.Left;
      SourceComboBox.Height := ComboBoxRect.Bottom - ComboBoxRect.Top;
   end
   else
   begin
      SourceComboBox.Visible := false;
      CanSelect := false;
   end;

end;

procedure TIDB_ParamsOfSaveLoadForm.ProgisIDSourceColumnComboBoxChange (Sender: TObject);
var
   sSourceColumnName: AnsiString;
begin
   sSourceColumnName := ProgisIDSourceColumnComboBox.Items[ProgisIDSourceColumnComboBox.ItemIndex];
   ADataManager.SetColumnWithProgisIDData (sSourceColumnName);
end;

procedure TIDB_ParamsOfSaveLoadForm.LinkIDComboBoxChange(Sender: TObject);
var sLinkColumnName: AnsiString;
begin
   sLinkColumnName := LinkIDComboBox.Items[LinkIDCombobox.itemIndex];
   ADataManager.SetColumnWithLinkIDData(sLinkColumnName);
end;

procedure TIDB_ParamsOfSaveLoadForm.UseTheFirstLineAsFieldNamesCheckBoxClick (Sender: TObject);
begin
   SELF.ADataManager.SetFirstFileLineIsFieldNameSign (UseTheFirstLineAsFieldNamesCheckBox.Checked);
     // I have a necessity in the analysis data file only if I'm loading data, when I'm saving data
     // there isn't a necessity in the analysis.
   if SELF.wmWorkMode = wmLoad then
      SELF.GoAnalyse;
end;

procedure TIDB_ParamsOfSaveLoadForm.ScanFileCheckBoxClick (Sender: TObject);
begin
   SELF.ADataManager.SetScanWholeFileSign (ScanFileCheckBox.Checked);
   SELF.GoAnalyse;
end;

procedure TIDB_ParamsOfSaveLoadForm.RewriteDataCheckBoxClick (Sender: TObject);
begin
   SELF.ADataManager.SetExistingDataMustBeRewritedSign (RewriteDataCheckBox.Checked);
end;

procedure TIDB_ParamsOfSaveLoadForm.InsertRecordCheckBoxClick (Sender: TObject);
begin
   ADataManager.SetInsertNewRecordSign (InsertRecordCheckBox.Checked);
end;

procedure TIDB_ParamsOfSaveLoadForm.RadioButton1Click (Sender: TObject);
begin
   SELF.DelimTypeComboBox.Enabled := TRUE;
   SELF.DelimEdit.Enabled := FALSE;
   DelimTypeComboBoxChange (SENDER);
end;

procedure TIDB_ParamsOfSaveLoadForm.DelimTypeComboBoxChange (Sender: TObject);
begin
   SELF.ADataManager.SetSeparator (DelimTypeComboBox.ItemIndex);
   if SELF.wmWorkMode = wmLoad then
      SELF.GoAnalyse;
end;

procedure TIDB_ParamsOfSaveLoadForm.RadioButton2Click (Sender: TObject);
begin
   SELF.DelimTypeComboBox.Enabled := FALSE;
   SELF.DelimEdit.Enabled := TRUE;
   DelimEditChange (SENDER);
end;

procedure TIDB_ParamsOfSaveLoadForm.DelimEditChange (Sender: TObject);
var
   iI: Integer;
begin // Set Separator by Edit (the user do it by his hand).
   SELF.ADataManager.SetSeparator (DelimEdit.Text);
   if SELF.wmWorkMode = wmSave then
      EXIT;
     // Clear the old settings of linked columns.
   for iI := 1 to StringGrid1.RowCount - 1 do
      StringGrid1.Cells[1, iI] := '';
   if DelimEdit.Text = '' then
      SourceComboBox.Items.Clear
   else
      SELF.GoAnalyse;
   SELF.DelimEdit.SetFocus;
end;

procedure TIDB_ParamsOfSaveLoadForm.UseConvertingCheckBoxClick (Sender: TObject);
begin
   if UseConvertingCheckBox.Checked then
      ADataManager.SetCodePage (cpOEM)
   else
      ADataManager.SetCodePage (cpANSI);
end;

procedure TIDB_ParamsOfSaveLoadForm.StringGrid1TopLeftChanged (Sender: TObject);
var
   ComboBoxRect: TRect;
begin
   if SELF.SourceComboBox.Visible then
   begin
      ComboBoxRect := SELF.StringGrid1.CellRect (1, StringGrid1.Row);
      SourceComboBox.Left := StringGrid1.Left + ComboBoxRect.Left;
      SourceComboBox.Top := StringGrid1.Top + ComboBoxRect.Top + 1;
      SourceComboBox.Width := ComboBoxRect.Right - ComboBoxRect.Left + 4;
      if SourceComboBox.Left + SourceComboBox.Width > StringGrid1.ClientWidth + StringGrid1.Left then
         SourceComboBox.Width := StringGrid1.ClientWidth + StringGrid1.Left - SourceComboBox.Left;
      SourceComboBox.Height := ComboBoxRect.Bottom - ComboBoxRect.Top;
   end;
end;

procedure TIDB_ParamsOfSaveLoadForm.SourceComboBoxClick (Sender: TObject);
var
   sSourceColumnName, sDestinationColumnName: AnsiString;
//   pLC: PRecLinkedColumns;
//   iP: integer;
begin
   sSourceColumnName := SourceComboBox.Items[SourceComboBox.ItemIndex];
   sDestinationColumnName := StringGrid1.Cells[0, StringGrid1.Row];
   StringGrid1.Cells[1, StringGrid1.Row] := sSourceColumnName;
   if SourceComboBox.ItemIndex > 0 then
      SELF.ADataManager.AddLinkedData (sSourceColumnName, sDestinationColumnName)
   else
      if SELF.slDestFields.IndexOf (sDestinationColumnName) = -1 then
         SELF.ADataManager.DeleteDestinationLinkedData (sDestinationColumnName);
end;



end.

