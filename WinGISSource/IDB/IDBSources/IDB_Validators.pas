unit IDB_Validators;
{
Internal database. Ver. II.
This module implements 2 descendant of validators that we can find in CommonLib library.
I need to check the contents of validators neither only when an user enters some value
nor when he exits from this component. I need to check the contents of validator at any time.

The check function is declared under Protected section of validators. 

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 27-06-2001
}
interface

Uses Classes, Validate;

Type
    TIDB_IntValidator = Class(TIntValidator)
        Private
           sBasicCharacterSet: AnsiString;
        Public
           Constructor Create(AOwner: TComponent); override;
           Function IsValidInput(var S:String;var CaretPos:Integer):Boolean; override;
        End;

    TIDB_FloatValidator = Class(TFloatValidator)
        Private
           sBasicCharacterSet: AnsiString;
        Public
           Constructor Create(AOwner: TComponent); override;
           Function IsValidInput(var S:String;var CaretPos:Integer):Boolean; override;
        End;

implementation

Const // I can have these symbols in text but I don't want to admit to an user
      // to enter them directly.  
      cs_AdditionalCharacterSet = ',''F'',''f'',''C'',''c'',''$'',''('','')''';

Constructor TIDB_IntValidator.Create(AOwner: TComponent);
Begin
     Inherited Create(AOwner);
     sBasicCharacterSet := SELF.ValidChars;
     SELF.FormatWhenExit := FALSE;
     SELF.ValidateOnExit := FALSE;
     SELF.MaxLength := 100;
End;

Function TIDB_IntValidator.IsValidInput(var S:String;var CaretPos:Integer):Boolean;
Begin
     SELF.ValidChars := sBasicCharacterSet + cs_AdditionalCharacterSet;
     RESULT := Inherited IsValidInput(S, CaretPos);
     SELF.ValidChars := sBasicCharacterSet;
End;

Constructor TIDB_FloatValidator.Create(AOwner: TComponent);
Begin
     Inherited Create(AOwner);
     sBasicCharacterSet := SELF.ValidChars;
     SELF.FormatWhenExit := FALSE;
     SELF.ValidateOnExit := FALSE;
     SELF.MaxLength := 100;
     SELF.Commas := 20;
End;

Function TIDB_FloatValidator.IsValidInput(var S:String;var CaretPos:Integer):Boolean;
Begin
     SELF.ValidChars := sBasicCharacterSet + cs_AdditionalCharacterSet;
     RESULT := Inherited IsValidInput(S, CaretPos);
     SELF.ValidChars := sBasicCharacterSet;
End;

end.
