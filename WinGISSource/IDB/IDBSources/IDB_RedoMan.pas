unit IDB_RedoMan;
{
Internal database. ver. II.
Redo manager.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 10-05-2001
}
interface

Uses DTables, DMaster,
     IDB_Access2DB, IDB_CallBacksDef, Dialogs, Classes, IDB_UndoMan;

Type
    TIDB_RedoManager = Class
       Private
          AProject: Pointer;

          iRedoStepCount: Integer;
          iMaxRedoStreamSizeConst: integer;
          bThereAreSomeProblemsWithRedoTable: Boolean;
          bWeAreUsingRedoFile: boolean;
          bDataForSavingInFile: boolean;
          sRedoFileName, sRedoDataFileName: AnsiString;

          RedoStream: TMemoryStream;
          iCurrentRedoObjectCount,
          iTheTypeOfTheOneObject: Integer;
          ADataForSaving: AnsiString;

       Public
          Constructor Create(AProject: Pointer; ADataAccessObject: TIDB_Access2DB_Level2);
          Destructor Free;

          Procedure ClearRedoData;
          Procedure GetReadyToReceiveRedoData;
          Function PutDataInRedo(AData: Pointer; iSize: Integer): Boolean;
          Procedure SaveRedoData(ARedoType: TRedoType);

          Function ThereAreDataForRedoing: Boolean;
          Function GetNextRedoDescription: AnsiString;
          Function GetRedoData(Var ABuffer: Pointer; Var ASize: Integer; Var ARedoType: TRedoType): Boolean;
       End;

implementation

Uses DB, Windows, SysUtils,
     DDB,
     IDB_Consts, IDB_Languagemanager;

Constructor TIDB_RedoManager.Create(AProject: Pointer; ADataAccessObject: TIDB_Access2DB_Level2);
Begin
     Self.AProject := AProject;

     iMaxRedoStreamSizeConst := GetProjectFileSizeFunc (AProject) div 10;
     if iMaxRedoStreamSizeConst = 0 then
        iMaxRedoStreamSizeConst := 1024;

     bWeAreUsingRedoFile := false;
     bDataForSavingInFile := false;

     if not bWeAreUsingRedoFile then
        RedoStream := TMemoryStream.Create;

     sRedoFileName := GenerateUniqueName ('', '', '');

     ClearRedoData;

     SELF.iRedoStepCount := 0;
     SELF.ADataForSaving := '';
     SELF.bThereAreSomeProblemsWithRedoTable := FALSE;
End;

Destructor TIDB_RedoManager.Free;
Begin
     SELF.ClearRedoData;
     if Self.RedoStream <> nil then
        Self.RedoStream.Free;
End;

Procedure TIDB_RedoManager.ClearRedoData;
Begin
     if bWeAreUsingRedoFile then
        DeleteFile (sRedoFileName)
     else
        RedoStream.Clear;

     SELF.ADataForSaving := '';
     SELF.bDataForSavingInFile := false;
     SELF.sRedoDataFileName := '';
     SELF.iRedoStepCount := 0;

End;

Procedure TIDB_RedoManager.GetReadyToReceiveRedoData;
Begin
     If bThereAreSomeProblemsWithRedoTable Then EXIT;
     iCurrentRedoObjectCount := 0;
     ADataForSaving := '';
End;

Function TIDB_RedoManager.ThereAreDataForRedoing: Boolean;
Begin
     If bThereAreSomeProblemsWithRedoTable Then
        RESULT := FALSE
     Else
        RESULT := SELF.iRedoStepCount > 0;
End;

Function TIDB_RedoManager.PutDataInRedo(AData: Pointer; iSize: Integer): Boolean;
Var
   iLen: Integer;
   ibObjectType: Byte;
   iObjectTypeHeaderLength: Integer;
   RedoDataFile: TFileStream;
Begin
     { A notice: below I used SizeOf(Byte) instead of 1 to avoid the situation when
       the length of the header of stream, which contains type of object in the stream,
       will be changed. Then I'll simply change the type of ibObjectType variable. }
     // The first byte of the stream is the type of the object that was put into the stream.
     iObjectTypeHeaderLength := SizeOf(ibObjectType);
     CopyMemory(@ibObjectType, AData, iObjectTypeHeaderLength);
     // If there is the one object in REDO stream, store the type of this object.
     If iCurrentRedoObjectCount < 1 Then
        iTheTypeOfTheOneObject := ibObjectType;

     if (Length (ADataForSaving) + iSize > iMaxRedoStreamSizeConst)
         or bDataForSavingInFile then
     begin
        if not bDataForSavingInFile then
           sRedoDataFileName := GenerateUniqueName ('', '', '');
        bDataForSavingInFile := true;
        if FileExists (sRedoDataFileName) then
           RedoDataFile := TFileStream.Create (sRedoDataFileName, fmOpenReadWrite)
        else
        begin
           RedoDataFile := TFileStream.Create (sRedoDataFileName, fmCreate);
           RedoDataFile.Free;
           RedoDataFile := TFileStream.Create (sRedoDataFileName, fmOpenReadWrite);
        end;

        RedoDataFile.Position := RedoDataFile.Size;

        RedoDataFile.Write (Pointer(PChar(AData) + iObjectTypeHeaderLength)^, iSize - iObjectTypeHeaderLength);
        RedoDataFile.Free;
     end
     else
     begin
        iLen := Length(ADataForSaving);
        SetLength(ADataForSaving, iLen + iSize - iObjectTypeHeaderLength);
        CopyMemory(@ADataForSaving[iLen+1], PChar(AData) + iObjectTypeHeaderLength, iSize - iObjectTypeHeaderLength);
     end;

     Inc(iCurrentRedoObjectCount);
     RESULT := TRUE;
End;

Procedure TIDB_RedoManager.SaveRedoData(ARedoType: TRedoType);
var iDataLength: Integer;
    RedoFile, RedoDataFile: TFileStream;
    iRedoType, iRedoObjectCount: integer;
    iDataBlockLength: integer;
    pBuff: Pointer;
    iBufferSize: integer;
    i: integer;
begin
   try
   iRedoType := integer(Ord(ARedoType));
   // Save the number of objects in the stream. To understand how this value is created and interpreted,
   // see the comments to procedure TIDB_UndoManager.SaveUndoData in IDB_UndoMan.pas.
   if iCurrentRedoObjectCount < 2 then
      iRedoObjectCount := -iTheTypeOfTheOneObject
   else
      iRedoObjectCount := iCurrentRedoObjectCount;
   // Save the data.

   if bDataForSavingInFile then
   begin
      RedoDataFile := TFileStream.Create (sRedoDataFileName, fmOpenReadWrite);
      iDataLength := Length(ADataForSaving) + RedoDataFile.Size;
      RedoDataFile.Free;
   end
   else
      iDataLength := Length(ADataForSaving);
   iDataBlockLength := iDataLength + sizeof (integer) * 3;

   if bWeAreUsingRedoFile then
   begin
      if FileExists (sRedoFileName) then
         RedoFile := TFileStream.Create (sRedoFileName, fmOpenReadWrite)
      else
      begin
         RedoFile := TFileStream.Create (sRedoFileName, fmCreate);
         RedoFile.Free;
         RedoFile := TFileStream.Create (sRedoFileName, fmOpenReadWrite);
      end;

      try

         RedoFile.Position := RedoFile.Size;

         RedoFile.Write (iRedoType, sizeof (integer));
         RedoFile.Write (iRedoObjectCount, sizeof (integer));
         if bDataForSavingInFile then
         begin
            RedoDataFile := TFileStream.Create (sRedoDataFileName, fmOpenReadWrite);

            RedoFile.Write (Pointer(ADataForSaving)^, Length (ADataForSaving));

            for i := 0 to RedoDataFile.Size div iMaxRedoStreamSizeConst do
            begin
               if (i + 1) * iMaxRedoStreamSizeConst < RedoDataFile.Size then
                  iBufferSize := iMaxRedoStreamSizeConst
               else
                  iBufferSize := RedoDataFile.Size - i * iMaxRedoStreamSizeConst;
               GetMem(pBuff, iBufferSize);
               RedoDataFile.Read (pBuff^, iBufferSize);
               RedoFile.Write (Pointer (pBuff)^, iBufferSize);
               FreeMem (pBuff);
            end;
            RedoDataFile.Free;
            DeleteFile (sRedoDataFileName);
            bDataForSavingInFile := false;
            sRedoDataFileName := '';
         end
         else
         begin
            RedoFile.Write (Pointer(ADataForSaving)^, iDataLength);
         end;

         RedoFile.Write (iDataBlockLength, sizeof (integer));

         Inc (SELF.iRedoStepCount);
      finally
         RedoFile.Free;
         SELF.GetReadyToReceiveRedoData;
      end;
   end
   else
   begin
      if RedoStream.Size + iDataBlockLength >= iMaxRedoStreamSizeConst then
      begin
         try
            RedoStream.SaveToFile (sRedoFileName);
            RedoStream.Free;
            Self.bWeAreUsingRedoFile := true;
            Self.SaveRedoData (ARedoType);
            exit;
         except
         end;
      end;

      try
         RedoStream.Position := RedoStream.Size;

         RedoStream.Write (iRedoType, sizeof (integer));
         RedoStream.Write (iRedoObjectCount, sizeof (integer));
         RedoStream.Write (Pointer(ADataForSaving)^, iDataLength);
         RedoStream.Write (iDataBlockLength, sizeof (integer));

         Inc (SELF.iRedoStepCount);
      finally
         SELF.GetReadyToReceiveRedoData;
      end;
   end;
   except
      if RedoFile <> nil then
         RedoFile.Free;
   end;
end;

Function TIDB_RedoManager.GetNextRedoDescription: AnsiString;
Var
   iNextRedoType,
   iRedoObjectCount: Integer;
   RedoFile: TFileStream;
   iLength: integer;
Begin
   try
     RESULT := '';
     iNextRedoType := Ord(rtUnknownREDO);
     iRedoObjectCount := 0;
     if bWeAreUsingRedoFile then
     begin
        try
           RedoFile := TFileStream.Create (sRedoFileName, fmOpenReadWrite);
        except
           bThereAreSomeProblemsWithRedoTable := true;
           iRedoStepCount := 0;
           exit;
        end;

        if RedoFile.Size = 0 then
           exit;

        try
           RedoFile.Seek (-sizeof (integer), soFromEnd);
           RedoFile.Read (Pointer (iLength), sizeof (integer));
           RedoFile.Seek (-iLength, soFromEnd);

           RedoFile.Read (Pointer (iNextRedoType), sizeof (integer));
           RedoFile.Read (Pointer (iRedoObjectCount), sizeof (integer));
        finally
           RedoFile.Free;
        end;
     end
     else
     begin
        try
           RedoStream.Seek (-sizeof (integer), soFromEnd);
           RedoStream.Read (Pointer (iLength), sizeof (integer));
           RedoStream.Seek (-iLength, soFromEnd);

           RedoStream.Read (Pointer (iNextRedoType), sizeof (integer));
           RedoStream.Read (Pointer (iRedoObjectCount), sizeof (integer));
        except
           exit;
        end;
     end;

     Case TRedoType(iNextRedoType) Of
     rtInsert: RESULT := ALangManager.GetValue(297);
     rtDelete: RESULT := ALangManager.GetValue(298);
     rtChange: RESULT := ALangManager.GetValue(299);
     rtMove: RESULT := ALangManager.GetValue(300);
     rtResize: RESULT := ALangManager.GetValue(301);
     rtTransform: RESULT := ALangManager.GetValue(302);
     Else RESULT := ALangManager.GetValue(303); // 'Unknown REDO' will be returned.
     End; // Case end

     // If the number of objects is a negative value, it means that there is the one object
     // in REDO stream and this value is the type of this object.
     If iRedoObjectCount < 0 Then Begin
        Case iRedoObjectCount Of
        -1:  RESULT := RESULT + ALangManager.GetValue(282); // ' of pixel'             - TPixel
        -2:  RESULT := RESULT + ALangManager.GetValue(283); // ' of polyline'          - TPoly
        -4:  RESULT := RESULT + ALangManager.GetValue(284); // ' of polygon'           - TCPoly
        -5:  RESULT := RESULT + ALangManager.GetValue(285); // ' of group'             - TGroup
        -7:  RESULT := RESULT + ALangManager.GetValue(286); // ' of text'              - TText
        -8:  RESULT := RESULT + ALangManager.GetValue(287); // ' of Symbol'            - TSymbol
        -9:  RESULT := RESULT + ALangManager.GetValue(288); // ' of spline'            - TSpline
        -10: RESULT := RESULT + ALangManager.GetValue(289); // ' of image'            - TImage
        -11: RESULT := RESULT + ALangManager.GetValue(290); // ' of measure line'     - TMesLine
        -12: RESULT := RESULT + ALangManager.GetValue(291); // ' of circle'           - TCircle
        -13: RESULT := RESULT + ALangManager.GetValue(292); // ' of arc'              - TArc
        -14: RESULT := RESULT + ALangManager.GetValue(293); // ' of OLE object'       - TOLEObj
        -16: RESULT := RESULT + ALangManager.GetValue(294); // ' of text'             - TRText
        -17: RESULT := RESULT + ALangManager.GetValue(295); // ' of business graphic' - TBusGraph';
        -18: RESULT := RESULT + ALangManager.GetValue(296); // ' of layer'            - TLayer';
        End; // Case end
        End
     Else
     // If the number of objects in the next redo is more than 1, this number will be added to description.
        If iRedoObjectCount > 1 Then
           RESULT := RESULT + ' (' + IntToStr(iRedoObjectCount) + ')';
   except
      if RedoFile <> nil then
         RedoFile.Free;
      Result := '';   
   end;

End;

Function TIDB_RedoManager.GetRedoData(Var ABuffer: Pointer; Var ASize: Integer; Var ARedoType: TRedoType): Boolean;
var iLength: Integer;
    pBuff: Pointer;
    RedoFile: TFileStream;
    iRedoType, iRedoObjectCount: integer;
begin
   try
   RESULT := FALSE;
   ABuffer := nil;
   ASize := 0;
   ARedoType := rtUnknownREDO;

   if bThereAreSomeProblemsWithRedoTable then
      exit;

   if bWeAreUsingRedoFile then
   begin
      try
         RedoFile := TFileStream.Create (sRedoFileName, fmOpenReadWrite);
      except
         bThereAreSomeProblemsWithRedoTable := true;
         iRedoStepCount := 0;
         exit;
      end;

      if RedoFile.Size = 0 then
         exit;

      try
         RedoFile.Seek (-sizeof (integer), soFromEnd);
         RedoFile.Read (Pointer (iLength), sizeof (integer));
         RedoFile.Seek (-iLength, soFromEnd);

         iLength := iLength - sizeof (integer) * 3;
         GetMem(pBuff, iLength);
         RedoFile.Read (Pointer (iRedoType), sizeof (integer));
         RedoFile.Read (Pointer (iRedoObjectCount), sizeof (integer));
         RedoFile.Read (pBuff^, iLength);
         RedoFile.Seek (-iLength - sizeof (integer) * 3, soFromEnd);
         RedoFile.Size := RedoFile.Position;
         Dec (iRedoStepCount);
      finally
            if RedoFile.Size < iMaxRedoStreamSizeConst then
            begin
               RedoFile.Free;
               RedoStream := TMemoryStream.Create;
               RedoStream.LoadFromFile (sRedoFileName);
               DeleteFile (sRedoFileName);
               Self.bWeAreUsingRedoFile := false;
            end
            else
               RedoFile.Free;
      end;
   end
   else
   begin
      try
      RedoStream.Seek (-sizeof (integer), soFromEnd);
      RedoStream.Read (Pointer (iLength), sizeof (integer));
      RedoStream.Seek (-iLength, soFromEnd);

      iLength := iLength - sizeof (integer) * 3;
      GetMem(pBuff, iLength);
      RedoStream.Read (Pointer (iRedoType), sizeof (integer));
      RedoStream.Read (Pointer (iRedoObjectCount), sizeof (integer));
      RedoStream.Read (pBuff^, iLength);
      RedoStream.Seek (-iLength - sizeof (integer) * 3, soFromEnd);
      RedoStream.Size := RedoStream.Position;
      Dec (iRedoStepCount);
      except
         exit;
      end;
   end;
   try
      ARedoType := TRedoType (iRedoType);
   except
      ARedoType := rtUnknownREDO;
   end;
   ABuffer := Pointer (pBuff);
   ASize := iLength;
   RESULT := TRUE;
   except
      if RedoFile <> nil then
         RedoFile.Free;
      Result := false;
   end;
end;

end.
