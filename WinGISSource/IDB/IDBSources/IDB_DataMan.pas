unit IDB_DataMan;
{
Internal database. Ver II.
Data manager. This module provides possibilities to save/load data from/in DB table or a string grid.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 07-06-2000
}
interface

uses DB, SysUtils, Dialogs, Windows, Forms, Controls, Grids, Gauges, Classes,
   DTables, DDB,
   IDB_Consts, IDB_Access2DB, IDB_StructMan;

type

   TWorkMode = (wmUnknown, wmTextFile, wmAccessTable, wmDBase5Table, wmDBase3Table,
      wmParadox5Table, wmParadox7Table);

   TAccessExportMode = (aemReplace, aemUse);

   // I use the next Record in 2 ways. First: as link data between source and destination
   // columns (fields) during loading operation. Second: I use only sDestinationColumnName
   // during saving operation. In this case I store in that member of record which fields
   // were selected by an user to export.
   TRecLinkedColumns = record
      sSourceColumnName: AnsiString;
      sDestinationColumnName: AnsiString;
      bThisFieldIsNormalField: Boolean;
   end;
   PRecLinkedColumns = ^TRecLinkedColumns;

   TIDB_BaseDataManager = class
   private
      AProject: Pointer;
      AGauge: TGauge; // A TGauge shows complete percent during the execution.
      sDataFileName: AnsiString;
      sSeparator: AnsiString;
      bFirstFileLineIsFieldNames: Boolean;
      cpCodePage: TCodePage;

      emAccessExportMode: TAccessExportMode;

      listLinkedColumns: TList; // Data about linked source and destination columns.
      sProgisIDColumnName: AnsiString; // Which source column is column with ProgisID data.
      sLinkIDColumnName: AnsiString; // Feldname der PROGISID in der Tabelle oder anderer zu Feldname als verbindung beim importieren

      AdditionalIDA: TIDB_Access2DB_Level2; // The object that I create to access to source (during loading)
      // or destination data (during saving) (ONLY for MS Access).
      ADestinationTable: TDTable;

      function OemToAnsiStr (sOemStr: AnsiString): AnsiString;
      function AnsiToOemStr (sOemStr: AnsiString): AnsiString;
      function TranscodeFromOemToAnsi (sData: AnsiString): AnsiString;
      function TranscodeFromAnsiToOem (sData: AnsiString): AnsiString;

      function FindSourceColumn (sSourceColumnName: AnsiString): Integer;
      function FindDestinationColumn (sDestinationColumnName: AnsiString): Integer;

      function DoSaveDialog: Boolean;

      procedure CorrectFieldNamesUnderDriverRestriction;
      function DeleteLastChar (sValue: AnsiString): AnsiString;

      function CreateConnectionStringToDesktopFile: AnsiString;

      function GetDesktopCodePage: TCodePage;
      procedure SetDesktopCodePage (ANewCodePage: TCodePage);

   public
      wmWorkMode: TWorkMode;
      constructor Create (AProject: Pointer; AGauge: TGauge);
      destructor Free;

      procedure SetSeparator (sNewSeparator: AnsiString); overload;
      procedure SetSeparator (iSeparatorPos: Integer); overload;
      procedure SetFirstFileLineIsFieldNameSign (bNewValue: Boolean);
      procedure SetCodePage (cpPage: TCodePage);
      function GetCodePage: TCodePage;

      function GetWorkMode: TWorkMode;

      procedure ClearLinkedData;
      procedure DeleteDestinationLinkedData (sDestinationColumnName: AnsiString);
      procedure DeleteSourceLinkedData (sSourceColumnName: AnsiString);
      function GetLinkedDataCount: Integer;
      function GetLinkedData (iItemIndex: integer): PRecLinkedColumns;
      function GetSourceColumnName (sDestinationColumnName: AnsiString): AnsiString;

      procedure SetColumnWithProgisIDData (sColumnName: AnsiString);
      function GetColumnWithProgisIDData: AnsiString;
      procedure SetColumnWithLinkIDData (sColumnName: AnsiString);
      function GetColumnWithLinkIDData: AnsiString;
      // Abstract methods.
      { I think it is very important thing: some of this methods are implemented as in TDBDataManager call
        as well in TGridDataManager and some of those - only in TDBGridManager. It was made so because I have
        one dialog for loading_from / saving_to actions and it is the dialog for both calsses. But some methods
        are rerquired only for TDBDataManager. }
      function GetTheIDBTableColumnsNames (bShowCalculatedFields: Boolean = FALSE): AnsiString; virtual; abstract;
      procedure AnalyseDataFile (AGauge: TGauge = nil); virtual; abstract;
      function GetGuaranteedExistingColumnsNames: AnsiString; virtual; abstract;
      function GetSourceColumnsNames: AnsiString; virtual; abstract;
      procedure SetScanWholeFileSign (bNewValue: Boolean); virtual; abstract;
      procedure SetExistingDataMustBeRewritedSign (bNewValue: Boolean); virtual; abstract;
      procedure AddLinkedData (sSourceColumnName, sDestinationColumnName: AnsiString);
      procedure AddIDField;
      procedure SetInsertNewRecordSign (bNewValue: Boolean); virtual; abstract;
      function ShowTableStructure: Boolean; virtual; abstract;
      function WorkWithSourceTableStructure: Boolean; virtual; abstract;
   end;

   TIDB_DBDataManager = class (TIDB_BaseDataManager)
   private
//      sLayerName: AnsiString;
//      iLayerIndex: Integer;
      ALayer: Pointer;
      ASQL: AnsiString;

      MainIDA: TIDB_Access2DB_Level2; // Already exists data access object
      ADataSet: TDADODataSet; // A data set that contains data for the saving (It can be as TTable as TQuery).
      ASourceTable: TDTable; // A table that contains data for the loading.

      bScanWholeFile: Boolean; // Scan all lines fo file. If I'll detect in any line the quantity of
      // columns more than in the first line of file, I'll add this colums
      // in available and name they as 'Column_X' (X - the digital from
      // sequence 1, 2, 3,..).
      bExistingDataMustBeRewrited: Boolean; // If this sign is TRUE, I rewrite existsing table data during execution import operation.
      bInsertRecordIfItDoesNotExist: Boolean; // Insert new record in an attribute table, if record for an object does not exist.
      listSurelyExistingColumns, // The list of colums that surely exist in the source file.
      //  (They are enumerated in the first line of source file).
      listUnSurelyExistingColumns: TStringList; // The list of colums that can be exist in any lines but in
      //  the other lines of source file they can be unexist.
      AStructureManager: TIDB_StructMan;
      function DoOpenDialog: Boolean;

      procedure AnalyseTextFile (AGauge: TGauge);
      procedure AnalyseTable;
   public
      constructor Create (IDB_DataAccessModule: TIDB_Access2DB_Level2; AProject, ALayer: Pointer; ADataSet: TDADODataSet; sSQL: AnsiString; AGauge: TGauge = nil);
      destructor Free;

      procedure SaveResultsFromDBTableToFile;
      function LoadData (wmMode: TWorkMode = wmUnknown; sDBFileName: AnsiString = ''; sTableName: AnsiString = ''): Boolean;

      procedure AnalyseDataFile (AGauge: TGauge = nil); override;
      procedure SetScanWholeFileSign (bNewValue: Boolean); override;
      procedure SetExistingDataMustBeRewritedSign (bNewValue: Boolean); override;
      procedure SetInsertNewRecordSign (bNewValue: Boolean); override;

      function GetGuaranteedExistingColumnsNames: AnsiString; override;
// ++ Cadmensky Version 2.3.2
      function GetIntegerTypeFieldNames (sFieldNames: AnsiString): AnsiString;
// -- Cadmensky Version 2.3.2

      function GetTheIDBTableColumnsNames (bShowCalculatedFields: Boolean = FALSE): AnsiString; override;
      function GetSourceColumnsNames: AnsiString; override;

      function ShowTableStructure: Boolean; override; // This procedure is used for start of Structure Manager
      // when I need to do it from dialog of loading parameters.
      function WorkWithSourceTableStructure: Boolean; override; // This procedure I use when the user requires
      // from me add field based on any existed field in a source
      // table.
      function GenerateUniqueFieldName (sBaseFieldName: AnsiString): AnsiString;
   end;

implementation

uses
   Registry,
   IDB_MainManager, IDB_View, IDB_ParamsOfSaveLoad, IDB_ParamsOfSaveLoad_TableSelect,
   IDB_Utils, IDB_2ParamsDialog, IDB_SelectInListBox, IDB_LanguageManager,
   IDB_StructureAndProperties, IDB_CallbacksDef, IDB_LayerManager;
var
   arSeparators: array[0..3] of AnsiString = (';', ',', #09, ' ');

constructor TIDB_BaseDataManager.Create (AProject: Pointer; AGauge: TGauge);
begin
   SELF.AProject := AProject;
   SELF.AGauge := AGauge;
   SELF.sDataFileName := '';
   SELF.sSeparator := ';';
   SELF.bFirstFileLineIsFieldNames := FALSE;
   SELF.cpCodePage := cpANSI;

   SELF.sProgisIDColumnName := '';
   SELF.sLinkIDColumnName :='PROGISID';
   SELF.listLinkedColumns := TList.Create;

   SELF.AdditionalIDA := TIDB_Access2DB_Level2.Create (AProject);
   SELF.ADestinationTable := TDTable.Create (nil);
end;

destructor TIDB_BaseDataManager.Free;
begin
   ClearLinkedData;
   SELF.listLinkedColumns.Free;
   if SELF.AdditionalIDA <> nil then
   begin
      AdditionalIDA.Disconnect;
      AdditionalIDA.Free;
   end;

   SELF.ADestinationTable.Free;
end;

function TIDB_BaseDataManager.OemToAnsiStr (sOemStr: AnsiString): AnsiString;
begin
   SetLength (Result, Length (sOemStr));
   if Length (Result) > 0 then OemToCharBuff (PChar (sOemStr), PChar (Result), Length (Result));
end;

function TIDB_BaseDataManager.TranscodeFromOemToAnsi (sData: AnsiString): AnsiString;
begin
   if SELF.cpCodePage = cpANSI then
      RESULT := sData
   else
      RESULT := OemToAnsiStr (sData);
end;

function TIDB_BaseDataManager.AnsiToOemStr (sOemStr: AnsiString): AnsiString;
begin
   SetLength (Result, Length (sOemStr));
   if Length (Result) > 0 then CharToOemBuff (PChar (sOemStr), PChar (Result), Length (Result));
end;

function TIDB_BaseDataManager.TranscodeFromAnsiToOem (sData: AnsiString): AnsiString;
begin
   if SELF.cpCodePage = cpANSI then
      RESULT := sData
   else
      RESULT := AnsiToOemStr (sData);
end;

procedure TIDB_BaseDataManager.SetSeparator (sNewSeparator: AnsiString);
begin
   if sNewSeparator = '' then EXIT;
   SELF.sSeparator := sNewSeparator;
end;

procedure TIDB_BaseDataManager.SetSeparator (iSeparatorPos: Integer);
begin
   if iSeparatorPos > High (arSeparators) then iSeparatorPos := 0;
   SELF.SetSeparator (arSeparators[iSeparatorpos]);
end;

procedure TIDB_BaseDataManager.SetFirstFileLineIsFieldNameSign (bNewValue: Boolean);
begin
   SELF.bFirstFileLineIsFieldNames := bNewValue;
end;

procedure TIDB_BaseDataManager.SetCodePage (cpPage: TCodePage);
begin
   SELF.cpCodePage := cpPage;
end;

function TIDB_BaseDataManager.GetCodePage: TCodePage;
begin
   RESULT := SELF.cpCodePage;
end;

procedure TIDB_BaseDataManager.ClearLinkedData;
var
   iI: Integer;
   pLC: PRecLinkedColumns;
begin
   for iI := 0 to SELF.listLinkedColumns.Count - 1 do
   begin
      pLC := listLinkedColumns[iI];
      if pLC <> nil then Dispose (pLC);
   end;
   listLinkedColumns.Clear;
end;

procedure TIDB_BaseDataManager.AddLinkedData (sSourceColumnName, sDestinationColumnName: AnsiString);
var
   iP: Integer;
   pLC: PRecLinkedColumns;
   {I think this's important to understanding. A destination column can be filled with data from
   the only a source column. But the source column can be the source of data for some destination columns.
   Thereby in my work I consider a destination column in my data array is more important than
   a source column.}
begin
   iP := SELF.FindDestinationColumn (sDestinationColumnName);
   if iP < 0 then
   begin
      New (pLC);
      pLC.sSourceColumnName := sSourceColumnName;
      pLC.sDestinationColumnName := sDestinationColumnName;
      pLC.bThisFieldIsNormalField := TRUE;
      listLinkedColumns.Add (pLC);
   end
   else
   begin
      pLC := listLinkedColumns[iP];
      pLC.sSourceColumnName := sSourceColumnName;
   end;
end;

procedure TIDB_BaseDataManager.AddIDField;
var
   pLC: PRecLinkedColumns;
begin
   New (pLC);
   pLC.sSourceColumnName := 'ROW_ID';
   pLC.sDestinationColumnName := 'ROW_ID';
   pLC.bThisFieldIsNormalField := FALSE;
   listLinkedColumns.Insert (0, pLC);
end;

function TIDB_BaseDataManager.FindSourceColumn (sSourceColumnName: AnsiString): Integer;
var
   iI: Integer;
   pLC: PRecLinkedColumns;
begin
   RESULT := -1;
   sSourceColumnName := AnsiUpperCase (sSourceColumnName);
   for iI := 0 to listLinkedColumns.Count - 1 do
   begin
      pLC := listLinkedColumns[iI];
      if AnsiUpperCase (pLC.sSourceColumnName) = sSourceColumnName then
      begin
         RESULT := iI;
         EXIT;
      end;
   end;
end;

function TIDB_BaseDataManager.FindDestinationColumn (sDestinationColumnName: AnsiString): Integer;
var
   iI: Integer;
   pLC: PRecLinkedColumns;
begin
   RESULT := -1;
   sDestinationColumnName := AnsiUpperCase (sDestinationColumnName);
   for iI := 0 to listLinkedColumns.Count - 1 do
   begin
      pLC := listLinkedColumns[iI];
      if AnsiUpperCase (pLC.sDestinationColumnName) = sDestinationColumnName then
      begin
         RESULT := iI;
         EXIT;
      end;
   end;
end;

procedure TIDB_BaseDataManager.DeleteDestinationLinkedData (sDestinationColumnName: AnsiString);
var
   iDCP: Integer;
   pLC: PRecLinkedColumns;
begin
   iDCP := SELF.FindDestinationColumn (sDestinationColumnName);
   if iDCP < 0 then EXIT;
   pLC := listLinkedColumns[iDCP];
   Dispose (pLC);
   listLinkedColumns.Delete (iDCP);
end;

procedure TIDB_BaseDataManager.DeleteSourceLinkedData (sSourceColumnName: AnsiString);
var
   iSCP: Integer;
   pLC: PRecLinkedColumns;
begin
   iSCP := SELF.FindSourceColumn (sSourceColumnName);
   while iSCP > -1 do
   begin
      pLC := listLinkedColumns[iSCP];
      Dispose (pLC);
      listLinkedColumns.Delete (iSCP);
      iSCP := SELF.FindSourceColumn (sSourceColumnName);
   end;
end;

function TIDB_BaseDataManager.GetSourceColumnName (sDestinationColumnName: AnsiString): AnsiString;
var
   iDCP: Integer;
   pLC: PRecLinkedColumns;
begin
   RESULT := '';
   iDCP := SELF.FindDestinationColumn (sDestinationColumnName);
   if iDCP < 0 then EXIT;
   pLC := listLinkedColumns[iDCP];
   RESULT := pLC.sSourceColumnName;
end;

// ++ Cadmensky Version 2.3.9
function TIDB_BaseDataManager.GetLinkedDataCount: Integer;
begin
   RESULT := listLinkedColumns.Count;
end;
// -- Cadmensky Version 2.3.9

function TIDB_BaseDataManager.GetLinkedData (iItemIndex: integer): PRecLinkedColumns;
begin
   RESULT := nil;
   if iItemIndex < listLinkedColumns.Count then
      RESULT := listLinkedColumns[iItemIndex];
end;

procedure TIDB_BaseDataManager.SetColumnWithProgisIDData (sColumnName: AnsiString);
begin
   if Trim (sColumnName) = '' then EXIT;
   SELF.sProgisIDColumnName := sColumnName;
end;

function TIDB_BaseDataManager.GetColumnWithProgisIDData: AnsiString;
begin
   RESULT := SELF.sProgisIDColumnName;
end;

procedure TIDB_BaseDataManager.SetColumnWithLinkIDData (sColumnName: AnsiString);
begin
   if Trim (sColumnName) = '' then EXIT;
   SELF.sLinkIDColumnName := sColumnName;
end;

function TIDB_BaseDataManager.GetColumnWithLinkIDData: AnsiString;
begin
   RESULT := SELF.sLinkIDColumnName ;
end;


{Create and execute TSaveDialog for user.}

function TIDB_BaseDataManager.DoSaveDialog: Boolean;
var
   sProjectName,
      sPrjName: AnsiString;
   SaveDialog: TSaveDialog;
   DF: TIDB_2ParamsDialogForm;
   bExecuted: Boolean;
begin
   RESULT := FALSE;

   sProjectName := GetProjectFullFileNameFunc (AProject);
   sPrjName := GetFileNameWithoutExt (sProjectName);

   bExecuted := FALSE;
   SaveDialog := TSaveDialog.Create (Application);
   try
      SaveDialog.InitialDir := ExtractFilePath (sProjectName);
      // Set parameters of dialog for an export from Grid.
//        SaveDialog.Filter := ALangManager.GetValue(93); // 'MS Access 97 database files (*.mdb)|*.mdb|dBase 5 files (*.dbf)|*.dbf|dBase 3 files (*.dbf)|*.dbf|Paradox 3.5 files (*.db)|*.db|Paradox 7 files (*.db)|*.db|Text files (*.txt)|*.txt'
      SaveDialog.Filter := ALangManager.GetValue (408); // 408=MS Access 97 database files (*.mdb)|*.mdb|dBase 5 files (*.dbf)|*.dbf|Paradox 5 files (*.db)|*.db|Paradox 7 files (*.db)|*.db|Text files (*.txt)|*.txt

      SaveDialog.InitialDir := ExtractFilePath (sProjectName);
      if Trim (sPrjName) <> '' then
         SaveDialog.FileName := sPrjName
      else
         SaveDialog.FileName := ALangManager.GetValue (110); // 'Noname';
      // Show SaveDialog.
      bExecuted := SaveDialog.Execute;
      if not bExecuted then
         EXIT;
      // Get name of selectd file and for the export from DB I have to check the file extension.
      SELF.sDataFileName := SaveDialog.FileName;
      case SaveDialog.FilterIndex of
         1:
            begin
               TIDB_DBDataManager (SELF).wmWorkMode := wmAccessTable;
               if ExtractFileExt (SELF.sDataFileName) = '' then SELF.sDataFileName := SELF.sDataFileName + '.mdb';
            end;
         2:
            begin
               TIDB_DBDataManager (SELF).wmWorkMode := wmDBase5Table;
               if ExtractFileExt (SELF.sDataFileName) = '' then SELF.sDataFileName := SELF.sDataFileName + '.dbf';
            end;
         {
                 3: Begin
                    TIDB_DBDataManager(SELF).wmWorkMode := wmDBase3Table;
                    If ExtractFileExt(SELF.sDataFileName) = '' Then SELF.sDataFileName := SELF.sDataFileName + '.dbf';
                    End;
         }
         3:
            begin
               TIDB_DBDataManager (SELF).wmWorkMode := wmParadox5Table;
               if ExtractFileExt (SELF.sDataFileName) = '' then SELF.sDataFileName := SELF.sDataFileName + '.db';
            end;
         4:
            begin
               TIDB_DBDataManager (SELF).wmWorkMode := wmParadox7Table;
               if ExtractFileExt (SELF.sDataFileName) = '' then SELF.sDataFileName := SELF.sDataFileName + '.db';
            end;
         5:
            begin
               TIDB_DBDataManager (SELF).wmWorkMode := wmTextFile;
               if ExtractFileExt (SELF.sDataFileName) = '' then SELF.sDataFileName := SELF.sDataFileName + '.txt';
            end;
      else
         TIDB_DBDataManager (SELF).wmWorkMode := wmUnknown;
      end; // Case end
   finally
      SaveDialog.Free;
   end;
   if bExecuted then
   begin
      // Ask what the user wants to do with the already existsing file.
      if FileExists (SELF.sDataFileName) then
         if SELF.wmWorkMode = wmAccessTable then
         begin
            // File '...' already exists. Do you want to export in it?
            DF := TIDB_2ParamsDialogForm.Create (Application);
            DF.Label1.Caption := Format (ALangManager.GetValue (94), [SELF.sDataFileName]); // Database file ''%s'' already exists
            DF.RadioButton1.Caption := ALangManager.GetValue (95); // Replace it.
            DF.RadioButton2.Caption := ALangManager.GetValue (96); // Use already existed file.
            if DF.ShowModal <> mrOK then EXIT;
            if DF.RadioButton1.Checked then
               TIDB_DBDataManager (SELF).emAccessExportMode := aemReplace
            else
               TIDB_DBDataManager (SELF).emAccessExportMode := aemUse;
            DF.Free;
         end
         else
            // File '...' already exists
            if MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (88), [SELF.sDataFileName])), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrNO then
               EXIT;
      RESULT := TRUE;
   end;
end;

// This procedure corrects names of selected fields 'cause dBase and Paradox drivers have
// a restriction on length of field names. For dBase - 10 letters and for Paradox - 25 letters.
// Meanwhile for MS Access this length can be equal 255 letters.

procedure TIDB_BaseDataManager.CorrectFieldNamesUnderDriverRestriction;
var
   iI, iJ, iP, iLen: Integer;
   sFieldName, sPostfix: AnsiString;
   pLC: PRecLinkedColumns;
begin
   case wmWorkMode of
      wmDBase5Table, wmDBase3Table: iLen := 10;
      wmParadox5Table, wmParadox7Table:
         begin
            iLen := 25;
            if SELF.FindDestinationColumn ('PROGISID') < 0 then
               AddIDField;
         end;
   else
      EXIT;
   end; // Case end

   for iI := 0 to SELF.listLinkedColumns.Count - 1 do
   begin
      pLC := listLinkedColumns[iI];
      // Replace ' ' with '_'.
      sFieldName := ReplaceChars (pLC.sDestinationColumnName);
      if Length (sFieldName) > iLen then
         sFieldName := Copy (sFieldName, 1, iLen);

      iJ := 1;
      iP := SELF.FindDestinationColumn (sFieldName);
      while (iP > -1) and (iP <> iI) do
      begin
         sPostFix := IntToStr (iJ);
         sFieldName := Copy (sFieldName, 1, Length (sFieldName) - Length (sPostfix)) + sPostfix;
         Inc (iJ);
      end;

      if (FieldIsCalculatedField (pLC.sSourceColumnName)) then
         sFieldName := Copy (sFieldName, 2, Length (sFieldName)) + '_';

      pLC.sDestinationColumnName := sFieldName;
   end; // For iI end
end;

// This function deletes the last Separator char in a string.

function TIDB_BaseDataManager.DeleteLastChar (sValue: AnsiString): AnsiString;
begin
   RESULT := Copy (sValue, 1, Length (sValue) - Length (sSeparator));
end;

function TIDB_BaseDataManager.GetWorkMode: TWorkMode;
begin
   RESULT := SELF.wmWorkMode;
end;

function TIDB_BaseDataManager.CreateConnectionStringToDesktopFile: AnsiString;
var
   sLevel: AnsiString;
begin
   case SELF.wmWorkMode of
      wmDBase5Table: sLevel := 'dBase 5.0';
      wmDBase3Table: sLevel := 'dBase III';
      wmParadox5Table: sLevel := 'Paradox 5.x';
      wmParadox7Table: sLevel := 'Paradox 7.x';
   end;
   RESULT := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + ExtractFilePath (SELF.sDataFileName) + ';' +
      'Extended Properties=' + sLevel + ';' +
      'Mode=ReadWrite|Share Deny None;Persist Security Info=False';
end;

function TIDB_BaseDataManager.GetDesktopCodePage: TCodePage;
var
   ARegistry: TRegistry;
   sPath, sKeyValue: AnsiString;
begin
   RESULT := cpANSI;
   ARegistry := TRegistry.Create;
   try
      case wmWorkMode of
         wmDBase5Table, wmDBase3Table:
            sPath := '\SOFTWARE\Microsoft\Jet\4.0\Engines\Xbase';
         wmParadox5Table, wmParadox7Table:
            sPath := '\SOFTWARE\Microsoft\Jet\4.0\Engines\Paradox';
      else
         EXIT;
      end; // Case end
      ARegistry.RootKey := HKEY_LOCAL_MACHINE;
      ARegistry.OpenKey (sPath, FALSE);
      sKeyValue := ARegistry.ReadString ('DataCodePage');
      if AnsiUpperCase (sKeyValue) = 'OEM' then
         RESULT := cpOEM;
   finally
      ARegistry.CloseKey;
      ARegistry.Free;
   end;
end;

procedure TIDB_BaseDataManager.SetDesktopCodePage (ANewCodePage: TCodePage);
var
   ARegistry: TRegistry;
   sPath, sKeyValue: AnsiString;
begin
   ARegistry := TRegistry.Create;
   try
      case wmWorkMode of
         wmDBase5Table, wmDBase3Table:
            sPath := '\SOFTWARE\Microsoft\Jet\4.0\Engines\Xbase';
         wmParadox5Table, wmParadox7Table:
            sPath := '\SOFTWARE\Microsoft\Jet\4.0\Engines\Paradox';
      else
         EXIT;
      end; // Case end
      case ANewCodePage of
         cpANSI: sKeyValue := 'ANSI';
         cpOEM: sKeyValue := 'OEM';
      else
         EXIT;
      end; // Case end
      ARegistry.RootKey := HKEY_LOCAL_MACHINE;
      ARegistry.OpenKey (sPath, TRUE);
      ARegistry.WriteString ('DataCodePage', sKeyValue);
   finally
      ARegistry.CloseKey;
      ARegistry.Free;
   end;
end;

/////////   D B   D A T A   M A N A G E R  /////////////////////////////////////

constructor TIDB_DBDataManager.Create (IDB_DataAccessModule: TIDB_Access2DB_Level2; AProject, ALayer: Pointer; ADataSet: TDADODataSet; sSQL: AnsiString; AGauge: TGauge = nil);
begin
   inherited Create (AProject, AGauge);

   SELF.MainIDA := IDB_DataAccessModule;

   SELF.ALayer := ALayer;

   SELF.ASQL := sSQL;
   SELF.ADataSet := ADataSet;
   SELF.bScanWholeFile := FALSE;
   SELF.bExistingDataMustBeRewrited := FALSE;
   SELF.bInsertRecordIfItDoesNotExist := TRUE;
   SELF.ClearLinkedData;
   SELF.listSurelyExistingColumns := TStringList.Create;
   SELF.listUnSurelyExistingColumns := TStringList.Create;

   SELF.AStructureManager := TIDB_StructMan.Create (AProject, IDB_DataAccessModule);

   SELF.ASourceTable := TDTable.Create (nil);
   // Detect stucture of the source table.
   SELF.ASourceTable.Master := MainIDA.DMaster;

   if not AnIDBMainMan.UseNewATTNames (AProject) then
      SELF.ASourceTable.TableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (SELF.ALayer))
   else
      SELF.ASourceTable.TableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (SELF.AProject, SELF.ALayer);

   if ADataSet = nil then
      AStructureManager.DetectStructure (ASourceTable)
   else
      AStructureManager.DetectStructure (ADataSet, SELF.ASourceTable.TableName);

   if SELF.ASourceTable.Active then
      ASourceTable.Close;
   SELF.ASourceTable.Master := nil;
end;

destructor TIDB_DBDataManager.Free;
begin
   SELF.listSurelyExistingColumns.Free;
   SELF.listUnSurelyExistingColumns.Free;

   SELF.AStructureManager.Free;

   SELF.ASourceTable.Free;

// ++ Cadmensky Version 2.3.2
   inherited Free;
// -- Cadmensky Version 2.3.2
end;

{Create and execute TOpenDialog for user.}

function TIDB_DBDataManager.DoOpenDialog: Boolean;
var
   sProjectName,
      sPrjName: AnsiString;
   OpenDialog: TOpenDialog;
begin
   RESULT := FALSE;
   sProjectName := GetProjectFullFileNameFunc (AProject);
   sPrjName := GetFileNameWithoutExt (sProjectName);
   OpenDialog := TOpenDialog.Create (Application);
   OpenDialog.Filter := ALangManager.GetValue (92); //'MS Access 97 database files (*.mdb)|*.mdb|Paradox files (*.db)|*.db|XBase files (*.dbf)|*.dbf|Text files (*.txt)|*.txt'

   OpenDialog.InitialDir := ExtractFilePath (sProjectName);
   RESULT := OpenDialog.Execute;
   if RESULT then
      if Trim (OpenDialog.FileName) = '' then
         RESULT := FALSE
      else
      begin
         SELF.sDataFileName := OpenDialog.FileName;
         case OpenDialog.FilterIndex of
            1: SELF.wmWorkMode := wmAccessTable;
            2: SELF.wmWorkMode := wmParadox7Table;
            3: SELF.wmWorkMode := wmDBase5Table;
            4: SELF.wmWorkMode := wmTextFile;
         else
            SELF.wmWorkMode := wmUnknown;
         end; // case end
      end;
   OpenDialog.Free;
end;

function TIDB_DBDataManager.GetTheIDBTableColumnsNames (bShowCalculatedFields: Boolean = FALSE): AnsiString;
var
   iI: Integer;
   listData: TStringList;
   sFieldName: AnsiString;
   ALayerManager: TIDB_LayerManager;
   sCalcFields: AnsiString;
begin
   RESULT := '';
   listData := TStringList.Create;

   for iI := 0 to SELF.AStructureManager.GetFieldCount - 1 do
   begin
      sFieldName := AStructureManager.GetFieldName (iI);
      if (not FieldIsInternalUsageField (sFieldName)) and
         (not FieldIsCalculatedField (sFieldName)) and
         CheckFieldTypeSuitability (AStructureManager.GetFieldType (sFieldName)) then
         listData.Add (sFieldName);
   end;
   if bShowCalculatedFields then
   begin
      ALayerManager := AnIDBMainMan.GetProjectLayerManager (SELF.AProject);
      if ALayerManager <> nil then
      begin
         sCalcFields := ALayerManager.GetUsedCalcFieldsNames (SELF.ALayer);
         sCalcFields := Copy (sCalcFields, 1, Length (sCalcFields) - 2);
         listData.Add (sCalcFields);
      end;
   end;
   RESULT := listData.Text;
   listData.Free;
end;

function TIDB_DBDataManager.GetSourceColumnsNames: AnsiString;
begin
   RESULT := SELF.listSurelyExistingColumns.Text;
   if SELF.bScanWholeFile then
      RESULT := RESULT + SELF.listUnSurelyExistingColumns.Text;
end;

procedure TIDB_DBDataManager.SetScanWholeFileSign (bNewValue: Boolean);
begin
   SELF.bScanWholeFile := bNewValue;
end;

procedure TIDB_DBDataManager.SetExistingDataMustBeRewritedSign (bNewValue: Boolean);
begin
   SELF.bExistingDataMustBeRewrited := bNewValue;
end;

procedure TIDB_DBDataManager.SetInsertNewRecordSign (bNewValue: Boolean);
begin
   SELF.bInsertRecordIfItDoesNotExist := bNewValue;
end;

function TIDB_DBDataManager.GetGuaranteedExistingColumnsNames: AnsiString;
begin
   RESULT := SELF.listSurelyExistingColumns.Text;
end;

// ++ Cadmensky Version 2.3.2
function TIDB_DBDataManager.GetIntegerTypeFieldNames (sFieldNames: AnsiString): AnsiString;
var
   ASourceStructMan: TIDB_StructMan;
   ATable: TDTable;
   slFieldNames, slIntFieldNames: TStringList;
   iI: Integer;
begin
   ATable := TDTable.Create (nil);
   ATable.Master := SELF.MainIDA.DMaster;
   ATable.TableName := cs_IT_TempLink;
   ASourceStructMan := TIDB_StructMan.Create (SELF.AProject, SELF.MainIDA);
   ASourceStructMan.DetectStructure (ATable);

   slFieldNames := TStringList.Create;
   slIntFieldNames := TStringList.Create;
   slFieldNames.Text := sFieldNames;
   for iI := 0 to slFieldNames.Count - 1 do
         if ASourceStructMan.GetFieldType (slFieldNames[iI]) = 3 then
            slIntFieldNames.Add (slFieldNames[iI]);
   RESULT := slIntFieldNames.Text;

   ATable.Free;
   ASourceStructMan.Free;
   slFieldNames.Free;
   slIntFieldNames.Free;
end;
// -- Cadmensky Version 2.3.2

{This procedure saves data from database table in Separatored text file.}

procedure TIDB_DBDataManager.SaveResultsFromDBTableToFile;

// This function creates table in MS Access database file.

   function CreateAccessTable (sTableName: AnsiString): Boolean;
   var
      iI: Integer;
      pLC: PRecLinkedColumns;
   begin
      RESULT := FALSE;
      // Create table with one temp field.
      // Then I shall add all required fields and delete this temp field.
      try
         AdditionalIDA.ADbItemsMan.CreateTableWithoutAnyField (sTableName);
      except
         EXIT;
      end;
      if SELF.listLinkedColumns.Count < 1 then EXIT;
      try
         for iI := 0 to SELF.listLinkedColumns.Count - 1 do
         begin
            pLC := listLinkedColumns[iI];
            //if not FieldIsCalculatedField (pLC.sSourceColumnName) then
               AdditionalIDA.ADbItemsMan.AddField (sTableName,
                  pLC.sDestinationColumnName,
                  SELF.AStructureManager.GetFieldType (pLC.sSourceColumnName),
                  SELF.AStructureManager.GetFieldSize (pLC.sSourceColumnName))
           { else
               AdditionalIDA.ADbItemsMan.AddField (sTableName,
                  pLC.sDestinationColumnName,
                  6, // << Field type = 'DOUBLE', Floating 8 bytes
                  4) // << Field size  }
         end; // For iI end
      except
         EXIT;
      end;
      RESULT := TRUE;
   end;

   // This function creates dBase database file of required format.

   function CreateDBaseFile (sConnection: AnsiString): Boolean;
   var
      iI, iFieldType, iFieldSize: Integer;
      sSQL: AnsiString;
      pLC: PRecLinkedColumns;
      AQuery: TDQuery;
   begin
      RESULT := FALSE;
      try
         sSQL := 'CREATE TABLE [' + ExtractFileName (SELF.sDataFileName) + '] (';
         for iI := 0 to SELF.listLinkedColumns.Count - 1 do
         begin
            pLC := listLinkedColumns[iI];
            if not FieldIsCalculatedField (pLC.sSourceColumnName) then
            begin
               iFieldType := SELF.AStructureManager.GetFieldType (pLC.sSourceColumnName);
               if not CheckFieldTypeSuitability (iFieldType) then CONTINUE;
               case iFieldType of
                  1:
                     begin // Size of text field for dBase van be 1-254 (for MS Access 1-255).
                        iFieldSize := AStructureManager.GetFieldSize (pLC.sSourceColumnName);
                        if iFieldSize > 254 then iFieldSize := 254;
                        sSQL := sSQL + pLC.sDestinationColumnName + ' CHAR (' + IntToStr (iFieldSize) + '), ';
                     end;
                  2, 12: sSQL := sSQL + pLC.sDestinationColumnName + ' NUMERIC(16,0), ';
                  3: sSQL := sSQL + pLC.sDestinationColumnName + ' NUMERIC(16,0), ';
                  5: sSQL := sSQL + pLC.sDestinationColumnName + ' LOGICAL, ';
                  6, 7: sSQL := sSQL + pLC.sDestinationColumnName + ' FLOAT, ';
                  11: sSQL := sSQL + pLC.sDestinationColumnName + ' DATE, ';
               end; // Case end
            end
            else
               sSQL := sSQL + pLC.sDestinationColumnName + ' FLOAT, ';
         end; // For iI end
         sSQL := DeleteLastChar (sSQL); // Delete last ' '
         sSQL := DeleteLastChar (sSQL); // Delete last ','
         sSQl := sSQL + ')';

         AQuery := TDQuery.Create (nil);
         AQuery.Connection := sConnection;

         if FileExists (SELF.sDataFileName) then
            AQuery.ExecSQL ('DROP TABLE [' + ExtractFileName (SELF.sDataFileName) + ']');

         AQuery.ExecSQL (sSQL);

         AQuery.Free;
      except
         try
            if AQuery <> nil then AQuery.Free;
         except
         end;
         EXIT;
      end;
      RESULT := TRUE;
   end;

   // This function creates PARADOX databse file of required format.

   function CreateParadoxFile (sConnection: AnsiString): Boolean;
   var
      iI, iFieldType: Integer;
      sSQL: AnsiString;
      pLC: PRecLinkedColumns;
      AQuery: TDQuery;
      sIndexFieldName: AnsiString;
   begin
      RESULT := FALSE;
      try
         sSQL := 'CREATE TABLE [' + ExtractFileName (SELF.sDataFileName) + '] (';
         for iI := 0 to SELF.listLinkedColumns.Count - 1 do
         begin
            pLC := listLinkedColumns[iI];
            if not PLC.bThisFieldIsNormalField then
               sSQL := sSQL + pLC.sDestinationColumnName + ' AUTOINCREMENT, '
            else
               if not FieldIsCalculatedField (pLC.sSourceColumnName) then
               begin
                  iFieldType := SELF.AStructureManager.GetFieldType (pLC.sSourceColumnName);
                  if not CheckFieldTypeSuitability (iFieldType) then CONTINUE;
                  case iFieldType of // Size of text field for Paradox can be 1-255.
                     1: sSQL := sSQL + pLC.sDestinationColumnName + ' CHAR (' + IntToStr (AStructureManager.GetFieldSize (pLC.sSourceColumnName)) + '), ';
                     2, 12: sSQL := sSQL + pLC.sDestinationColumnName + ' SMALLINT, ';
                     3: sSQL := sSQL + pLC.sDestinationColumnName + ' INTEGER, ';
                     5: sSQL := sSQL + pLC.sDestinationColumnName + ' LOGICAL, ';
                     6, 7: sSQL := sSQL + pLC.sDestinationColumnName + ' FLOAT, ';
                     11: sSQL := sSQL + pLC.sDestinationColumnName + ' DATE, ';
                  end; // Case end
               end
               else
                  sSQL := sSQL + pLC.sDestinationColumnName + ' FLOAT, ';
         end; // For iI end
         sSQL := DeleteLastChar (sSQL); // Delete last ' '
         sSQL := DeleteLastChar (sSQL); // Delete last ','
         sSQl := sSQL + ')';
         {
                    If FileExists(SELF.sDataFileName) Then
                       If NOT SysUtils.DeleteFile(SELF.sDataFileName) Then EXIT;
         }
         AQuery := TDQuery.Create (nil);
         AQuery.Connection := sConnection;

         if FileExists (SELF.sDataFileName) then
            AQuery.ExecSQL ('DROP TABLE [' + ExtractFileName (SELF.sDataFileName) + ']');

         AQuery.ExecSQL (sSQL);

         if SELF.FindSourceColumn ('PROGISID') > -1 then
            sIndexFieldName := 'PROGISID'
         else
            sIndexFieldName := 'ROW_ID';
         AQuery.ExecSQL ('CREATE UNIQUE INDEX [' + GetFileNameWithoutExt (ExtractFileName (SELF.sDataFileName)) + '] ' +
            'ON [' + ExtractFileName (SELF.sDataFileName) + '] (' + sIndexFieldName + ')');
         AQuery.Free;
      except
         try
            if AQuery <> nil then AQuery.Free;
         except
         end;
         EXIT;
      end;
      RESULT := TRUE;
   end;

   procedure DeleteParadoxPrimaryKey;
   var
      sPXFileName: AnsiString;
   begin
      sPXFileName := ExtractFilePath (SELF.sDataFileName) +
         GetFileNameWithoutExt (ExtractFileName (SELF.sDataFileName)) +
         '.px';
      SysUtils.DeleteFile (sPXFileName);
   end;

   // This procedure copies data from an attribute table into destination table
   // (as MS Access table as well Standard table (xBase or Paradox).

   procedure ExportData;
   var
      iI: integer;
      AQuery: TDQuery;
      pLC: PRecLinkedColumns;
      sSQL: AnsiString;
   begin
      sSQL := 'INSERT INTO ' + cs_IT_TempLink + ' SELECT ';
      for iI := 0 to SELF.listLinkedColumns.Count - 1 do
      begin
         pLC := listLinkedColumns[iI];
         if (pLC.sSourceColumnName <> '') and (pLC.sDestinationColumnName <> '') then
            sSQL := sSQL + '[' + pLC.sSourceColumnName + '] AS [' + pLC.sDestinationColumnName + '], ';
      end;
      sSQL := Copy (sSQL, 1, Length (sSQL) - 2);
      sSQL := sSQL + ' FROM (' + SELF.ASQL + ')';

      AQuery := TDQuery.Create (nil);
      AQuery.Master := MainIDA.DMaster;

      AQuery.SQL.Text := sSQL;
      try
         AQuery.ExecSQL;
      except
         ShowMessage ('Error export data');
      end;

      AQuery.Free;
   end;

var
   iI, iJ: Integer;
   iMark: Integer;
   sTextData,
      sResult: AnsiString;
   tfResultFile: TextFile;
   PSF: TIDB_ParamsOfSaveLoadForm;
   PSFTS: TIDB_TableSelect; // Form for selecting destination table in MS Access file.
   sNewTableName: AnsiString; // Use when we are working with MS Access table
   pLC: PRecLinkedColumns;
   sConnection: AnsiString;
   sAttTableName: AnsiString;
   bSuccess: boolean;
begin
{   if SELF.AGauge <> nil then
   begin
      AGauge.MaxValue := ADataSet.RecordCount;
      AGauge.Progress := 0;
   end;}
   Application.ProcessMessages;
   // Ask an user about destination file.
   wmWorkMode := wmUnknown;
   if not DoSaveDialog then EXIT;
   // Now I know wmWorkMode (in which format I shall do the export) and emAccessExportMode (for wmWorkMode = wmAccessTable only).
   if wmWorkMode = wmUnknown then EXIT;
   // If the user has selected destination as mdb-file, we have to ask him some question
   // about mdb file.
   sNewTableName := ALangManager.GetValue (97);
   if SELF.wmWorkMode = wmAccessTable then
   begin
      PSFTS := TIDB_TableSelect.Create (Application);
      if FileExists (SELF.sDataFileName) then
      begin
         // Try to connect to destination file. If it isn't possible, set work mode as 'Replace Mode'.
         if AdditionalIDA.Connect (SELF.sDataFileName, '', '') then
            AdditionalIDA.ADbItemsMan.GetTablesNames (PSFTS.ListBox2, FALSE)
         else
            SELF.emAccessExportMode := aemReplace;
      end;
      if (emAccessExportMode = aemUse) and (PSFTS.ListBox2.Items.Count > 0) then
      begin
         // There are at least one existing table.
         PSFTS.ListBox2.ItemIndex := 0;
         PSFTS.UseExistingRadioButton.Checked := TRUE;
         PSFTS.Notebook1.PageIndex := 1; // ActivePage := 'SAVE';
         // Create name for new table.
         PSFTS.Edit1.Text := sNewTableName;
         if PSFTS.ListBox2.Items.IndexOf (PSFTS.Edit1.Text) > -1 then
         begin
            iI := 1;
            while PSFTS.ListBox2.Items.IndexOf (PSFTS.Edit1.Text) > -1 do
            begin
               PSFTS.Edit1.Text := sNewTableName + '_' + IntToStr (iI);
               Inc (iI);
            end;
         end;
         // Show the list of tables and ask the user either to select existing table
         // or to type new table name.
         if PSFTS.ShowModal <> mrOK then
         begin
            PSFTS.Free;
            EXIT;
         end;
         if PSFTS.UseExistingRadioButton.Checked then
            sNewTableName := PSFTS.ListBox2.Items[PSFTS.ListBox2.ItemIndex]
         else
            sNewTableName := Trim (PSFTS.Edit1.Text);
      end
      else
      begin
         // There isnt't any table.
         if not InputQuery (cs_ApplicationName, ALangManager.GetValue (98), sNewTableName) then
         begin
            PSFTS.Free;
            EXIT;
         end;
      end;
      if Trim (sNewTableName) = '' then
      begin
         // Table name cannot be empty.
         MessageBox (Application.Handle, PChar (ALangManager.GetValue (99)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
         PSFTS.Free;
         EXIT;
      end;
      PSFTS.Free;
   end; // If SELF.wmWorkMode = wmAccessTable end

   // Ask the user about parameters of destination.
   // Here I ask from the user the parameters of saving in the database file.
   if not AnIDBMainMan.UseNewATTNames (AProject) then
      sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (SELF.ALayer))
   else
      sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (SELF.AProject, SELF.ALayer);

   PSF := TIDB_ParamsOfSaveLoadForm.Create (Application, SELF, wmSave, SELF.MainIDA, sAttTableName, '');
   if SELF.wmWorkMode = wmTextFile then
      PSF.MakeWindowForSavingInTextFile
   else
   begin
      SELF.AnalyseTable;
      PSF.MakeWindowForSavingToDBTable (TRUE); // wmWorkMode = wmAccessTable);
   end;
   bSuccess := PSF.ShowModal = mrOK;
   PSF.Free;

   if bSuccess then
   begin
   // Make names of selected fields under driver restrictions.
      CorrectFieldNamesUnderDriverRestriction;

      case SELF.wmWorkMode of
         wmTextFile:
            begin
               try
                  iMark := ADataSet.FieldByName (cs_InternalID_FieldName).AsInteger;
               except
                  iMark := -1;
               end;
               ADataSet.DisableControls;
               // Write in file
               ADataSet.First;
               try
                  AssignFile (tfResultFile, SELF.sDataFileName);
                  ReWrite (tfResultFile);
               except // Error of creating of file '...'
                  MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (89), [SELF.sDataFileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
                  EXIT;
               end;
               if SELF.bFirstFileLineIsFieldNames then
               begin
                  // If first file line is fields names
                  sResult := '';
                  for iI := 0 to SELF.listLinkedColumns.Count - 1 do
                  begin
                     pLC := listLinkedColumns[iI];
                     sTextData := SELF.TranscodeFromAnsiToOem (ADataSet.FieldByName (pLC.sDestinationColumnName).FieldName);
                     sTextData := AnsiQuotedStr (sTextData, '"');
                     sResult := sResult + sTextData + sSeparator;
                  end;
                  sResult := DeleteLastChar (sResult);
                  try
                     WriteLn (tfResultFile, sResult);
                  except
                     // Error of writing in file '...'
                     CloseFile (tfResultFile);
                     MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (90), [SELF.sDataFileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
                     EXIT;
                  end;
               end; // If end
               // Now I'll save data from required field.
               for iI := 0 to ADataSet.RecordCount - 1 do
               begin
                  sResult := '';
                  for iJ := 0 to SELF.listLinkedColumns.Count - 1 do
                  begin
                     pLC := listLinkedColumns[iJ];
                     sTextData := SELF.TranscodeFromAnsiToOem (ADataSet.FieldByName (pLC.sDestinationColumnName).AsString);
                     sTextData := AnsiQuotedStr (sTextData, '"');
                     sResult := sResult + sTextData + sSeparator;
                  end;
                  sResult := DeleteLastChar (sResult);
                  try
                     WriteLn (tfResultFile, sResult);
                  except
                  // Error of writing in file '...'
                     CloseFile (tfResultFile);
                     MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (90), [SELF.sDataFileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
                     EXIT;
                  end;
                  ADataSet.Next;
                  if AGauge <> nil then AGauge.Progress := AGauge.Progress + 1;
                  Application.ProcessMessages;
               end; // For iI end
            // For a text file the work is ended.
               CloseFile (tfResultFile);
               try
                  if not ADataSet.Search (cs_InternalID_FieldName, IntToStr (iMark)) then
                     ADataSet.First;
               except
                  ADataSet.First;
               end;
            end;
         wmAccessTable:
            begin
            // Try to use or replace (create) *.mdb file.
               case SELF.emAccessExportMode of
                  aemUse:
                     begin // Check delete table
                        if not AdditionalIDA.ADbItemsMan.DeleteTable (sNewTableName) then
                        begin
                        // Table cannot be replaced.
                           MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (100), [sNewTableName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
                           EXIT;
                        end;
                     end; // aemUse end
                  aemReplace:
                     begin // Check the replacing existsing file.
                        if FileExists (SELF.sDataFileName) then
                        begin
                           AdditionalIDA.Disconnect;
                           Application.ProcessMessages;
                           if not SysUtils.DeleteFile (SELF.sDataFileName) then
                           begin
                           // File '...' cannot be deleted.
                              MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (101), [SELF.sDataFileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
                              EXIT;
                           end;
                        end;
                        if not AdditionalIDA.UnLoadEmptyDBFile (SELF.sDataFileName, dbv97) then
                        begin
                        // Error of creating of file '...'
                           MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (89), [SELF.sDataFileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
                           EXIT;
                        end;
                        if not AdditionalIDA.Connect (SELF.sDataFileName, '', '') then
                        begin
                           MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (102), [SELF.sDataFileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
                           EXIT;
                        end;
                     end; // aemReplcae end
               end; // Inside Case end
            // This part will be executed if the user selected to use already existing file.
               if not CreateAccessTable (sNewTableName) then
               begin
               // Error of creating of table '...'
                  MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (103), [sNewTableName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
                  EXIT;
               end;
            // Create TTable to access to destination table.
//               if ADestinationTable.Active then ADestinationTable.Close;
// ++ Cadmensky Version 2.3.7
               SELF.MainIDA.ADbItemsMan.DeleteLinkTable (cs_IT_TempLink);
// -- Cadmensky Version 2.3.7
// ++ Cadmensky
               if SELF.MainIDA.ADbItemsMan.CreateLinkTable (AdditionalIDA.GetDBFileName, sNewTableName, cs_IT_TempLink, Integer (SELF.wmWorkMode)) then
                  ExportData;
// -- Cadmensky
            end;
         wmDBase5Table, wmDBase3Table,
            wmParadox5Table, wmParadox7Table:
            begin
               sConnection := SELF.CreateConnectionStringToDesktopFile;
               if (wmWorkMode = wmDBase5Table) or (wmWorkMode = wmDBase3Table) then
               begin
                  if not CreateDBaseFile (sConnection) then
                  begin
                  // Error of creating of file '...'
                     MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (89), [SELF.sDataFileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
                     EXIT;
                  end;
               end
               else
               begin // wmParadox5Table, wmParadox7Table
                  if not CreateParadoxFile (sConnection) then
                  begin
                  // Error of creating of file '...'
                     MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (89), [SELF.sDataFileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
                     EXIT;
                  end;
               end;
// ++ Cadmensky Version 2.3.7
               SELF.MainIDA.ADbItemsMan.DeleteLinkTable (cs_IT_TempLink);
// -- Cadmensky Version 2.3.7
               if SELF.MainIDA.ADbItemsMan.CreateLinkTable (SELF.sDataFileName, '', cs_IT_TempLink, Integer (SELF.wmWorkMode)) then
            // Copy (export) data.
                  ExportData;
               if (wmWorkMode = wmParadox5Table) or (wmWorkMode = wmParadox7Table) then
                  DeleteParadoxPrimaryKey;
            end;
      end; // Case end

   // Return to old position in table
{   try
      if not ADataSet.Search (cs_InternalID_FieldName, IntToStr (iMark)) then
         ADataSet.First;
   except
      ADataSet.First;
   end;}
// ++ Cadmensky Version 2.3.1
      MainIDA.ADbItemsMan.DeleteLinkTable (cs_IT_TempLink);
// -- Cadmensky Version 2.3.1

      if ASourceTable.Active then ASourceTable.Close;
      if ADestinationTable.Active then ADestinationTable.Close;
      AdditionalIDA.Disconnect;

//   ADataSet.EnableControls;
      MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (91), [SELF.sDataFileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION);
   end;
end;

{This procedure can do a loading data from text file, from MS Access table and from a standard
database files - Paradox and xBase format. Also this procedure can work as with an user interface
as without one.
If you want to do the loading with manual settings of source and destination columns/fields,
you can run it with default parameters. In this case a work mode of the loader is
wmUnknow mode, database file name and table name are empty.
If you want to make the loading without user interface when you already know and preset how
is your columns/fields linked each to other, you can run this procedure with this parameters.
In this case you have to transfer work mode of the loader - wmTextFile, wmAccessTable, wmStandardTable.
Also before a calling this procedure you have to set loading options.
I'll count them:
1. SetExistingDataMustBeRewritedSign(True of False);
2. SetInsertNewRecordSign(True of False);
3. For the loading from text file, you also have to set whether first line of text file
   contains names of columns or not.
   SetFirstFileLineIsFieldNameSign(True or False);
4. SetColumnWithProgisIDData(Column with ProgisID data in the source);
5. ADataManager.AddLinkedData(Column with data in the source,
                              Column with data in the destination database table);
Only after it you can call this procedure.
If you do the loading from a text file, you can don't give other parameters.
If you do the loading from a standard database file (Paradox or xBase), you have to give
   the work mode and a database file name.
If you do the loading from MS Access table, You have to give the work mode, a database file name
   and a table name.}

function TIDB_DBDataManager.LoadData (wmMode: TWorkMode = wmUnknown; sDBFileName: AnsiString = ''; sTableName: AnsiString = ''): Boolean;
var
   arData: array of AnsiString;

   procedure PutDataIntoArray (sData: AnsiString);
   var
      iI: Integer;
      sTemp: AnsiString;
      pcTemp: PChar;
   begin
      if SELF.bScanWholeFile then // All data must be readed
         SetLength (arData, listSurelyExistingColumns.Count + listUnSurelyExistingColumns.Count)
      else // Data must be read according the first line of the data file.
         SetLength (arData, listSurelyExistingColumns.Count);
      // Put data from the data string into array's cells.
      for iI := 0 to High (arData) do
         arData[iI] := '';

      pcTemp := PChar (sData);
      while pcTemp^ in [#1..' '] do
         pcTemp := CharNext (pcTemp);
      for iI := 0 to High (arData) do
      begin
         sTemp := AnsiExtractQuotedStr (pcTemp, '"');
         if (sTemp = '') and (StrLen (pcTemp) = 0) then BREAK;
         arData[iI] := TranscodeFromOemToAnsi (sTemp);
         pcTemp := CharNext (pcTemp);
         while pcTemp^ in [#1..' '] do
            pcTemp := CharNext (pcTemp);
         while (pcTemp^ <> '"') and (pcTemp^ <> #0) do
            pcTemp := CharNext (pcTemp);
      end; // For iI end
   end;

var
   PLF: TIDB_ParamsOfSaveLoadForm; // Form for setting parameters of loading.
   PLFTS: TIDB_TableSelect; // Form for selecting source table in MS Access file.
   listData: TStringList;
   iStartDataLine, iDataLine, iI, iP, iPID: Integer;
   pLC: PRecLinkedColumns;
   sData {, sProgisIDSourceFieldName}: AnsiString;
   bThereIsNecessityInShowInterface,
      bWeAreReadyToDoTheLoading: Boolean;
//   iSourceRecordCount: Integer;
//   sLoadedProgisID, sLoadedFieldData: AnsiString;
   bDataIsLoadingFromUnsure: Boolean;
   AnOldCodePage: TCodePage;
// ++ Cadmensky
   AQuery: TDQuery;
   sAttTableName, sSQL, sSQL_2: AnsiString;
//   bFieldExists: boolean;
// -- Cadmensky
   sTemp: String;
begin
   RESULT := FALSE;
   // I detect if there is a necessity in a showing an user interface or not
   bThereIsNecessityInShowInterface := wmMode = wmUnknown;
   // If there is such necessity, I show OpenDialog,
   if bThereIsNecessityInShowInterface then
   begin
      if not SELF.DoOpenDialog then EXIT;
      if SELF.wmWorkMode = wmUnknown then EXIT;
   end
   else
   begin
      SELF.wmWorkMode := wmMode;
      SELF.sDataFileName := sDBFileName;
   end;
   if not FileExists (SELF.sDataFileName) then
   begin
      // File ''%s'' does not exist.
      MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (105), [SELF.sDataFileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
      EXIT;
   end;

   case wmWorkMode of
      wmAccessTable:
         begin
            // If there is the necesstity in the showing an user inteface, I ask an user to select
            // a table in MS Access database file. If there isn't this necessity, I think I already have this parameter.
            if bThereIsNecessityInShowInterface then
            begin
               // Determine database's table as a source.
               AdditionalIDA.Connect (SELF.sDataFileName, '', '');
               PLFTS := TIDB_TableSelect.Create (Application);
               PLFTS.Notebook1.PageIndex := 0; //ActivePage := 'LOAD';
               AdditionalIDA.ADbItemsMan.GetTablesNames (PLFTS.ListBox1, FALSE);
               if PLFTS.ListBox1.Items.Count < 1 then
               begin
                  PLFTS.Free;
                  // There isn''t any table in database file ''%s''.
                  MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (106), [SELF.sDataFileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
                  EXIT;
               end;
               PLFTS.ListBox1.ItemIndex := 0;
               if PLFTS.ShowModal <> mrOK then
               begin
                  PLFTS.Free;
                  EXIT;
               end;
               sTableName := PLFTS.ListBox1.Items[PLFTS.ListBox1.ItemIndex];
               PLFTS.Free;
            end;
// ++ Cadmensky Version 2.3.7
            SELF.MainIDA.ADbItemsMan.DeleteLinkTable (cs_IT_TempLink);
// -- Cadmensky Version 2.3.7
// ++ Cadmensky
            if not SELF.MainIDA.ADbItemsMan.CreateLinkTable (SELF.sDataFileName, sTableName, cs_IT_TempLink, Integer (SELF.wmWorkMode)) then
               exit;

            SELF.MainIDA.ADbItemsMan.DeleteTable (cs_IT_TempTable);
            if not AnIDBMainMan.UseNewATTNames (AProject) then
               sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (SELF.ALayer))
            else
               sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (SELF.AProject, SELF.ALayer);

// ++ Cadmensky
            AQuery := TDQuery.Create (nil);
            AQuery.Master := SELF.MainIDA.DMaster;
// -- Cadmensky

            AQuery.ExecSQL ('SELECT * INTO [' + cs_IT_TempTable + '] FROM [' + sAttTableName + ']');
// -- Cadmensky
         end;
      wmParadox7Table, wmDBase5Table {wmStandardTable}:
         begin
// ++ Cadmensky Version 2.3.7
            SELF.MainIDA.ADbItemsMan.DeleteLinkTable (cs_IT_TempLink);
// -- Cadmensky Version 2.3.7
// ++ Cadmensky
            if not SELF.MainIDA.ADbItemsMan.CreateLinkTable (SELF.sDataFileName, '', cs_IT_TempLink, Integer (SELF.wmWorkMode)) then
               exit;

            SELF.MainIDA.ADbItemsMan.DeleteTable (cs_IT_TempTable);
            if not AnIDBMainMan.UseNewATTNames (AProject) then
               sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (SELF.ALayer))
            else
               sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (SELF.AProject, SELF.ALayer);

// ++ Cadmensky
            AQuery := TDQuery.Create (nil);
            AQuery.Master := SELF.MainIDA.DMaster;
// -- Cadmensky

            AQuery.ExecSQL ('SELECT * INTO [' + cs_IT_TempTable + '] FROM [' + sAttTableName + ']');
// -- Cadmensky
         end;
      wmTextFile:
         begin
            if ADestinationTable.Active then ADestinationTable.Close;
            ADestinationTable.Master := MainIDA.DMaster;

            if not AnIDBMainMan.UseNewATTNames (AProject) then
               ADestinationTable.TableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (SELF.ALayer))
            else
               ADestinationTable.TableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (SELF.AProject, SELF.ALayer);

         end;
   end; // case end

   // Run user interface, if it is necessity.
   if bThereIsNecessityInShowInterface then
   begin
      if wmWorkMode = wmTextFile then
      begin
         PLF := TIDB_ParamsOfSaveLoadForm.Create (Application, SELF, wmLoad, SELF.MainIDA, '', ADestinationTable.TableName);
         PLF.MakeWindowForLoadingFromTextFile;
      end
      else
      begin
         PLF := TIDB_ParamsOfSaveLoadForm.Create (Application, SELF, wmLoad, SELF.MainIDA, cs_IT_TempLink, cs_IT_TempTable);
         PLF.MakeWindowForLoadingFromDBTable (wmWorkMode = wmAccessTable);
      end;
      bWeAreReadyToDoTheLoading := PLF.ShowModal = mrOK;
   end
   else
   begin
      // And now for a following loading I have to analyse data file.
      SELF.AnalyseDataFile;
      bWeAreReadyToDoTheLoading := TRUE;
   end;

   // And now I am ready to do it.
   if bWeAreReadyToDoTheLoading then
   begin
// ++ Cadmensky
      Screen.Cursor := crHourGlass;
      Application.ProcessMessages;
// -- Cadmensky
      // Now I start the execution of load data.
      case wmWorkMode of
         wmAccessTable, wmParadox7Table, wmDBase5Table {, wmStandardTable}:
            begin // A source is a database's table.
// ++ Cadmensky
               if bThereIsNecessityInShowInterface then
               begin
                  AnOldCodePage := SELF.GetDesktopCodePage;
                  SetDesktopCodePage (SELF.cpCodePage);
                  sSQL := 'SELECT ';
                  for iI := 0 to SELF.listLinkedColumns.Count - 1 do
                  begin
                     pLC := SELF.listLinkedColumns[iI];
                     if pLC.sSourceColumnName = '' then
                        sSQL := sSQL + '[' + cs_IT_TempTable + '].[' + pLC.sDestinationColumnName + '] AS [' + pLC.sDestinationColumnName + '], '
                     else
                        sSQL := sSQL + '[' + cs_IT_TempLink + '].[' + pLC.sSourceColumnName + '] AS [' + pLC.sDestinationColumnName + '], ';
                  end; // For iI end

                  sSQL := Copy (sSQL, 1, Length (sSQL) - 2);
                  sSQL := sSQL +
                     ' FROM [' + cs_IT_TempTable + ']' +
                     ' LEFT JOIN [' + cs_IT_TempLink + '] ON [' + cs_IT_TempTable + '].['+ self.sLinkIDColumnName + '] = [' + cs_IT_TempLink + '].[' + SELF.sProgisIDColumnName + ']';

                  if SELF.bInsertRecordIfItDoesNotExist then
                  begin
                     sSQL_2 := 'SELECT ';
                     for iI := 0 to SELF.listLinkedColumns.Count - 1 do
                     begin
                        pLC := SELF.listLinkedColumns[iI];
                        if pLC.sSourceColumnName = '' then
                        begin
                           if AnsiUpperCase (pLC.sDestinationColumnName) = 'PROGISID' then
                              sSQL_2 := sSQL_2 + '[' + cs_IT_TempLink + '].[' + SELF.sProgisIDColumnName + '] AS [' + pLC.sDestinationColumnName + '], '
                           else
                              sSQL_2 := sSQL_2 + '[' + cs_IT_TempTable + '].[' + pLC.sDestinationColumnName + '] AS [' + pLC.sDestinationColumnName + '], '
                        end
                        else
                           sSQL_2 := sSQL_2 + '[' + cs_IT_TempLink + '].[' + pLC.sSourceColumnName + '] AS [' + pLC.sDestinationColumnName + '], ';
                     end; // For iI end

                     sSQL_2 := Copy (sSQL_2, 1, Length (sSQL_2) - 2);
                     sSQL_2 := sSQL_2 +
                        ' FROM [' + cs_IT_TempTable + ']' +
                        ' RIGHT JOIN [' + cs_IT_TempLink + '] ON [' + cs_IT_TempTable + '].' + self.sLinkIDColumnName + ' = [' + cs_IT_TempLink + '].[' + SELF.sProgisIDColumnName + ']';
                  end;

                  {if SELF.bInsertRecordIfItDoesNotExist then
                     sSQL := 'SELECT * INTO [' + sAttTableName + '] FROM (' + sSQL + ' UNION ' + sSQL_2 + ')'
                  else}
                     sSQL := 'SELECT * INTO [' + sAttTableName + '] FROM (' + sSQL + ')';

                  AQuery.SQL.Text := sSQL;

                  SELF.MainIDA.ADbItemsMan.DeleteTable (sAttTableName);
                  try
                     AQuery.ExecSQL;
                     if SELF.bInsertRecordIfItDoesNotExist then
                        sTemp:='UPDATE [' + sAttTableName + '] SET ' + cs_StateFieldName + ' = ' + cs_ObjectIsNormal;
                        AQuery.ExecSQL (sTemp);
                  except
                     AQuery.ExecSQL ('SELECT * INTO [' + sAttTableName + '] FROM [' + cs_IT_TempTable + ']');
                  end;
                  AQuery.Free;
                  SELF.MainIDA.ADbItemsMan.DeleteTable (cs_IT_TempTable);
// ++ Cadmensky Version 2.3.2
                  SELF.MainIDA.ADbItemsMan.DeleteLinkTable (cs_IT_TempLink);
//                  SELF.MainIDA.ADbItemsMan.DeleteTable (cs_IT_TempLink);
// -- Cadmensky Version 2.3.2
// -- Cadmensky
                  SetDesktopCodePage (AnOldCodePage);
                  Application.ProcessMessages;
               end;
            end;
         wmTextFile:
            begin // A source is a text file.
               iPID := listSurelyExistingColumns.IndexOf (SELF.sProgisIDColumnName);
               if iPID < 0 then
                  EXIT;

// ++ Cadmensky Version 2.3.2
               if SELF.MainIDA.ADBItemsMan.GetReadyToCheckTableStructure (ADestinationTable.TableName) <> ci_All_Ok then
               begin
                  SELF.MainIDA.ADBItemsMan.EndOfCheckingTable;
                  Exit;
               end;
               for iI := 0 to listLinkedColumns.Count - 1 do
               begin
                  pLC := listLinkedColumns[iI];

                  if not SELF.MainIDA.ADBItemsMan.FieldExists (pLC.sDestinationColumnName) then
                     SELF.MainIDA.ADbItemsMan.AddField (ADestinationTable.TableName,
                        SELF.AStructureManager.GenerateFieldName (pLC.sDestinationColumnName), 1, 50);
               end;
               SELF.MainIDA.ADBItemsMan.EndOfCheckingTable;
// -- Cadmensky Version 2.3.2

// ++ Commented by Cadmensky Version 2.3.2
{               for iI := 0 to listLinkedColumns.Count - 1 do
               begin
                  pLC := listLinkedColumns[iI];

                  SELF.MainIDA.ADbItemsMan.FieldExists (ADestinationTable.TableName,
                     pLC.sDestinationColumnName,
                     bFieldExists);

                  if not bFieldExists then
                     SELF.MainIDA.ADbItemsMan.AddField (ADestinationTable.TableName,
                        SELF.AStructureManager.GenerateFieldName (pLC.sDestinationColumnName), 1, 50);
               end;}
// -- Commented by Cadmensky Version 2.3.2

               if not ADestinationTable.Active then
                  ADestinationTable.Open;
               listData := TStringList.Create;
               listData.LoadFromFile (SELF.sDataFileName);
               if SELF.AGauge <> nil then
               begin
                  AGauge.Progress := 0;
                  AGauge.MaxValue := listData.Count;
               end;
               if SELF.bFirstFileLineIsFieldNames then
                  iStartDataLine := 1
               else
                  iStartDataLine := 0;
               for iDataLine := iStartDataLine to listData.Count - 1 do
               begin
                  if SELF.AGauge <> nil then AGauge.Progress := AGauge.Progress + 1;
                  Application.ProcessMessages;
                  sData := listData[iDataLine];
                  // If it is necessary, I make a convertion data from OEM code page to ANSI one during this operation.
                  PutDataIntoArray (sData);
                  // If I haven't ProgisID data, I go to the next data line.
                  if Trim (arData[iPID]) = '' then CONTINUE;
                  // I try to find already existing value for current ProgisD. If I shall find it, I set
                  // tblDestination in Edit mode else I shall append new record into table.
                  if ADestinationTable.Locate ('PROGISID', arData[iPID], []) then
                     ADestinationTable.Edit
                  else
                     // If an user doesn't want to insert records for absent objects, I go to the next data line.
                     if SELF.bInsertRecordIfItDoesNotExist then
                     begin
                        ADestinationTable.Append;
                        ADestinationTable.FieldByName (cs_StateFieldName).AsString := cs_ObjectIsJustAdded;
                        ADestinationTable.FieldByName ('PROGISID').AsString := arData[iPID];
                        ///////////////////////////
                     end;
                  // Now I fill required fields with source's data.
                  for iI := 0 to listLinkedColumns.Count - 1 do
                  begin
                     pLC := listLinkedColumns[iI];
                     iP := SELF.listSurelyExistingColumns.IndexOf (pLC.sSourceColumnName);
                     if iP < 0 then
                     begin
                        bDataIsLoadingFromUnsure := TRUE;
                        iP := SELF.listUnSurelyExistingColumns.IndexOf (pLC.sSourceColumnName);
                     end
                     else
                        bDataIsLoadingFromUnsure := FALSE;
                     if (iP < 0) then CONTINUE; // Just in case... :)
                     // And now I like to move it - move it... :)
                     if arData[iP] = '' then CONTINUE;
                     // If there is a data in a field and the user don't want to rewrite existing data,
                     // I leave this field and go to the next field.
                     if (ADestinationTable.FieldByName (pLC.sDestinationColumnName).AsString <> '') and
                        (not SELF.bExistingDataMustBeRewrited) then
                        CONTINUE;
                     if bDataIsLoadingFromUnsure then
                        ADestinationTable.FieldByName (pLC.sDestinationColumnName).AsString := arData[listSurelyExistingColumns.Count + iP]
                     else
                        ADestinationTable.FieldByName (pLC.sDestinationColumnName).AsString := arData[iP];
                  end; // iI end
                  SELF.ADestinationTable.Post;
               end; // iDataLine end
// ++ Cadmensky
               listData.Free;
// -- Cadmensky
               Application.ProcessMessages;
            end;
      end; // case end
// ++ Cadmensky
      Screen.Cursor := crDefault;
// -- Cadmensky
   end;
   ASourceTable.Close;
   if bThereIsNecessityInShowInterface then
      PLF.Free;

// ++ Cadmensky
   RESULT := bWeAreReadyToDoTheLoading; //TRUE;
// -- Cadmensky
end;

procedure TIDB_DBDataManager.AnalyseDataFile (AGauge: TGauge = nil);
begin
   listSurelyExistingColumns.Clear;
   listUnSurelyExistingColumns.Clear;
   if wmWorkMode = wmTextFile then
      AnalyseTextFile (AGauge)
   else
      SELF.AnalyseTable;
end;

procedure TIDB_DBDataManager.AnalyseTextFile (AGauge: TGauge);
var
   listData: TStringList;
   iI, iC {, iP}: Integer;
   sData: AnsiString;
   ADispGauge: TGauge;
   sTemp: AnsiString;
   pcTemp: PChar;
   {I used listData for loading data from the data text file.
    At first I analyse the first line of data file. As result I have list of columns
    that surely exist in the data file. Then, if it's required, I analyse whole
    data file and have list of columns that surely and not surely exist in data file.}
begin
   // I receive TGauge as parameter 'cause there can be a necessity to show the progress
   // of comleting in an another form. If I receive NIL instead TGauge, I try to use TGauge that
   // was be transmitted to me in Create procedure.
   if AGauge <> nil then
      ADispGauge := AGauge
   else
      ADispGauge := SELF.AGauge;
   listData := TStringList.Create;
   listData.LoadFromFile (SELF.sDataFileName);
   // Here I analyse only a first line of source file.
   sData := listData[0];
   iC := 0;
   SELF.listSurelyExistingColumns.Clear;
   SELF.listUnSurelyExistingColumns.Clear;

   pcTemp := PChar (sData);
   while pcTemp^ in [#1..' '] do
      pcTemp := CharNext (pcTemp);

   while TRUE do
   begin
      sTemp := AnsiExtractQuotedStr (pcTemp, '"');
      if (sTemp = '') and (StrLen (pcTemp) = 0) then BREAK;
      Inc (iC);
      if SELF.bFirstFileLineIsFieldNames then
         listSurelyExistingColumns.Add (sTemp)
      else
         listSurelyExistingColumns.Add ('Column ' + IntToStr (iC));
      pcTemp := CharNext (pcTemp);
      while pcTemp^ in [#1..' '] do
         pcTemp := CharNext (pcTemp);
      while (pcTemp^ <> '"') and (pcTemp^ <> #0) do
         pcTemp := CharNext (pcTemp);
   end;
   if SELF.bScanWholeFile then
   begin
      // Here I analyse a whole file.
      if ADispGauge <> nil then
      begin
         ADispGauge.Progress := 0;
         ADispGauge.MaxValue := listData.Count - 2; // 2 - because I have just already analysed first line.
         ADispGauge.Visible := TRUE;
         Application.ProcessMessages;
      end;
      for iI := 1 to listData.Count - 1 do
      begin
         sData := listData[iI];
         iC := 0;
         pcTemp := PChar (sData);
         while pcTemp^ in [#1..' '] do
            pcTemp := CharNext (pcTemp);
         while TRUE do
         begin
            sTemp := AnsiExtractQuotedStr (pcTemp, '"');
            if (sTemp = '') and (StrLen (pcTemp) = 0) then BREAK;
            Inc (iC);
            if iC > listSurelyExistingColumns.Count then listUnSurelyExistingColumns.Add ('Column ' + IntToStr (iC));
            pcTemp := CharNext (pcTemp);
            while pcTemp^ in [#1..' '] do
               pcTemp := CharNext (pcTemp);
            while (pcTemp^ <> '"') and (pcTemp^ <> '#') do
               pcTemp := CharNext (pcTemp);
         end; // While end
         if ADispGauge <> nil then
         begin
            ADispGauge.Progress := ADispGauge.Progress + 1;
            Application.ProcessMessages;
         end;
      end; // For iI end
   end;
   if ADispGauge <> nil then
   begin
      ADispGauge.Visible := FALSE;
      Application.ProcessMessages;
   end;
   listData.Free;
end;

procedure TIDB_DBDataManager.AnalyseTable;
var
   iI: Integer;
begin
   SELF.listSurelyExistingColumns.Clear;
   if SELF.ADataSet = nil then
   begin
      if SELF.ASourceTable.Active then
         for iI := 0 to SELF.ASourceTable.Fields.Count - 1 do
            if CheckFieldTypeSuitability (ASourceTable.Fields[iI].DataType) then
               listSurelyExistingColumns.Add (ASourceTable.Fields[iI].FieldName);
   end
   else
   begin
      for iI := 0 to SELF.ADataSet.Fields.Count - 1 do
         if CheckFieldTypeSuitability (ADataSet.Fields[iI].DataType) then
            listSurelyExistingColumns.Add (ADataSet.Fields[iI].FieldName);
   end;
end;

{ This procedure is used for start of Structure Manager when I need to do it
  from dialog of loading parameters.}

function TIDB_DBDataManager.ShowTableStructure: Boolean;
var
   SPF: TIDB_StructureAndPropertiesForm;
   AStructMan: TIDB_StructMan;
   iI: Integer;
begin
   AStructMan := TIDB_StructMan.Create (AProject, MainIDA);
   AStructMan.DetectStructure (SELF.ADestinationTable);

   SPF := TIDB_StructureAndPropertiesForm.Create (Application, SELF.AProject, GetLayerIndexFunc (SELF.ALayer), AStructMan);
   try
      SPF.Position := poDesktopCenter;
      SPF.Caption := Format (ALangManager.GetValue (107), [GetLayerNameFunc (SELF.ALayer)]);
      SPF.SG.RowCount := AStructMan.GetFieldCount + 2;
      SPF.SG.ColCount := 4;
      for iI := 0 to AStructMan.GetFieldCount - 1 do
      begin
         SPF.SG.Cells[SPF.FieldNameCol, iI + 1] := AStructMan.GetFieldName (iI);
         SPF.SG.Objects[SPF.FieldNameCol, iI + 1] := Pointer (1);
      end;
      SPF.SG.Objects[SPF.FieldNameCol, SPF.SG.RowCount - 1] := Pointer (1);
      SPF._ReSize;
      RESULT := SPF.ShowModal = mrOK;
      if RESULT then
      begin
         SELF.ADestinationTable.Close;
         SELF.AStructureManager.DetectStructure (SELF.ADestinationTable);
      end;
   finally
      SPF.Free;
      AStructMan.Free;
   end;
end;

{ This procedure I use when the user requires from me add field based on any
  existed field in a source table.}

function TIDB_DBDataManager.WorkWithSourceTableStructure: Boolean;
var
   SILBF: TIDB_SelectInListBoxForm;
   ASource_StructMan: TIDB_StructMan;
   iI: Integer;
   sSelectedFieldName, sNewFieldName: AnsiString;
   bResultWasSuccessful: Boolean;
begin
   RESULT := FALSE;

   ASource_StructMan := TIDB_StructMan.Create (AProject, SELF.MainIDA);
   ASource_StructMan.DetectStructure (SELF.ASourceTable);

   SILBF := TIDB_SelectInListBoxForm.Create (Application);
   SILBF.Label1.Caption := ALangManager.GetValue (108);
   SILBF.ListBox1.OnDblClick := nil;
   SILBF.ListBox1.MultiSelect := TRUE;
   SILBF.OKButton.Enabled := FALSE;
   SILBF.ListBox1.Items.Clear;
   for iI := 0 to ASource_StructMan.GetFieldCount - 1 do
   begin
      if CheckFieldTypeSuitability (ASource_StructMan.GetFieldType (iI)) then
         SILBF.ListBox1.Items.Add (ASource_StructMan.GetFieldName (iI));
   end;
   if SILBF.ListBox1.Items.Count < 1 then
   begin // There isn't any suitable field in the data source...
      MessageBox (Application.Handle, PChar (ALangManager.GetValue (109)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
      EXIT;
   end;
   if SILBF.ShowModal <> mrOK then
   begin
      SILBF.Free;
      EXIT;
   end;
   for iI := 0 to SILBF.ListBox1.Items.Count - 1 do
   begin
      if not SILBF.ListBox1.Selected[iI] then CONTINUE;
      sSelectedFieldName := SILBF.ListBox1.Items[iI];
      sNewFieldName := SELF.AStructureManager.GenerateFieldName (sSelectedFieldName);
      bResultWasSuccessful := SELF.AStructureManager.AddNewField (sNewFieldName);
      if not bResultWasSuccessful then CONTINUE;
      bResultWasSuccessful := SELF.AStructureManager.ChangeFieldTypeAndSize (sNewFieldName, // << Name of just added field.
         ASource_StructMan.GetFieldType (sSelectedFieldName), // << Set type of just added field.
         ASource_StructMan.GetFieldSize (sSelectedFieldName)); // << Set size of just added field.
      if bResultWasSuccessful then
         SELF.AddLinkedData (sSelectedFieldName, sNewFieldName);
   end; // For iI end
   SILBF.Free;
   ADestinationTable.Close;
   SELF.AStructureManager.DoChangeStructure;
   ASource_StructMan.Free;
   RESULT := TRUE;
end;

function TIDB_DBDataManager.GenerateUniqueFieldName (sBaseFieldName: AnsiString): AnsiString;
var
   iI: Integer;
begin
   RESULT := sBaseFieldName;
   iI := 1;
   while SELF.FindDestinationColumn (RESULT) > -1 do
   begin
      RESULT := sBaseFieldName + '_' + IntToStr (iI);
      Inc (iI);
   end;
end;


end.

