unit IDB_ParamsOfExternalTables;
{
Internal database. Ver. II.
This form provides user interface for settings of external tables linking.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 23-08-2001
}
interface

uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   StdCtrls, ComCtrls, ExtCtrls, Buttons, CheckLst, Menus, ImgList,
   IDB_ExternalTablesMan, IDB_LayerManager, IDB_TempExternalTablesMan,
   ADODB_TLB, ADOX_TLB, Db, DDB, DTables;

type
   TIDB_ParamsOfExternalTablesForm = class (TForm)
      Panel1: TPanel;
      Panel2: TPanel;
      PageControl1: TPageControl;
      TableParamsTS: TTabSheet;
      Label6: TLabel;
      Label3: TLabel;
      Label4: TLabel;
      Label5: TLabel;
      FieldsTS: TTabSheet;
      LayerNamesComboBox: TComboBox;
      Label2: TLabel;
      Label1: TLabel;
      OKButton: TButton;
      CancelButton: TButton;
      AddNewLinkSB: TSpeedButton;
      DeleteLinkSB: TSpeedButton;
      StaticText1: TStaticText;
      StaticText2: TStaticText;
      StaticText3: TStaticText;
      CodePageComboBox: TComboBox;
      Label7: TLabel;
      Label8: TLabel;
      SelectedFieldsListBox: TListBox;
      AvailableFieldsListBox: TListBox;
      SpeedButton3: TSpeedButton;
      SpeedButton4: TSpeedButton;
      SpeedButton5: TSpeedButton;
      SpeedButton6: TSpeedButton;
      OpenDialog: TOpenDialog;
      LinkedTableCheckListBox: TCheckListBox;
      CheckBox1: TCheckBox;
      Label9: TLabel;
      ComboBox3: TComboBox;
      Label10: TLabel;
      Label11: TLabel;
      UserNameEdit: TEdit;
      PasswordEdit: TEdit;
      TestConnectionButton: TButton;
      RenameLinkSB: TSpeedButton;
      PopupMenu1: TPopupMenu;
      AddLinkPopUp: TMenuItem;
      DeleteLinkPopUp: TMenuItem;
      RenameLinkPopUp: TMenuItem;
      N1: TMenuItem;
      SelectAllPopUp: TMenuItem;
      DeselectAllPopUp: TMenuItem;
      InvertSelectionPopUp: TMenuItem;
      ImageList1: TImageList;
      UpSB: TSpeedButton;
      DownSB: TSpeedButton;
      SQLTablesComboBox: TComboBox;
      StaticText4: TStaticText;
      procedure FormCreate (Sender: TObject);
      procedure LayerNamesComboBoxChange (Sender: TObject);
      procedure AddNewLinkSBClick (Sender: TObject);
      procedure DeleteLinkSBClick (Sender: TObject);
      procedure LinkedTableCheckListBoxClick (Sender: TObject);
      procedure LinkedTableCheckListBoxDblClick (Sender: TObject);
      procedure RenameLinkSBClick (Sender: TObject);
      procedure SelectAllPopUpClick (Sender: TObject);
      procedure DeselectAllPopUpClick (Sender: TObject);
      procedure InvertSelectionPopUpClick (Sender: TObject);
      procedure PopupMenu1Popup (Sender: TObject);
      procedure TestConnectionButtonClick (Sender: TObject);
      procedure UserNameEditChange (Sender: TObject);
      procedure PasswordEditChange (Sender: TObject);
      procedure CodePageComboBoxChange (Sender: TObject);
      procedure FieldsTSShow (Sender: TObject);
      procedure OKButtonClick (Sender: TObject);
      procedure SpeedButton3Click (Sender: TObject);
      procedure SpeedButton4Click (Sender: TObject);
      procedure SpeedButton5Click (Sender: TObject);
      procedure SpeedButton6Click (Sender: TObject);
      procedure SelectedFieldsListBoxClick (Sender: TObject);
      procedure AvailableFieldsListBoxClick (Sender: TObject);
      procedure ComboBox3Change (Sender: TObject);
      procedure UpSBClick (Sender: TObject);
      procedure DownSBClick (Sender: TObject);
      procedure FormDestroy (Sender: TObject);
      procedure SQLTablesComboBoxChange (Sender: TObject);
   private
    { Private declarations }
      AProject: Pointer;
// ++ Cadmensky
      iLayerIndex: integer;
// -- Cadmensky
      ALayerManager: TIDB_LayerManager;

      TempExtTablesMan: TIDB_TempExtTablesMan;

      sWorkLinkName: AnsiString;

      procedure DisableVisualization;
      procedure ReadData;
   public
    { Public declarations }
      constructor Create (AOwner: TComponent; AProject: Pointer; ALayerManager: TIDB_LayerManager; iLayerIndex: integer);
      procedure SetCurrentLayer (ALayer: Pointer);
   end;

implementation
{$R *.DFM}

uses
   IDB_LanguageManager, IDB_CallbacksDef, IDB_Consts, IDB_ParamsOfSaveLoad_TableSelect,
   IDB_Access2DB, IDB_Utils;

constructor TIDB_ParamsOfExternalTablesForm.Create (AOwner: TComponent; AProject: Pointer; ALayerManager: TIDB_LayerManager; iLayerIndex: integer);
begin
   inherited Create (AOwner);
   SELF.AProject := AProject;
   SELF.ALayerManager := ALayerManager;
// ++ Cadmensky
   SELF.iLayerIndex := iLayerIndex;
// -- Cadmensky
end;

procedure TIDB_ParamsOfExternalTablesForm.FormCreate (Sender: TObject);
//var
//   {iI,} iP: Integer;
//   ALayer: Pointer;
//   AnExtTablesMan,
//      ATempMan: TIDB_BaseExternalTablesMan;
begin
   ALangManager.MakeReplacement (SELF);

   PageControl1.ActivePage := TableParamsTS;
   SelectedFieldsListBox.ItemIndex := 0;
   AvailableFieldsListBox.ItemIndex := 0;

   TempExtTablesMan := TIDB_TempExtTablesMan.Create (SELF.AProject, SELF.ALayerManager.DataAccessObject);

   LayerNamesComboBox.Items.Clear;
// Commented by Cadmensky
//     for iI:=1 to GetProjectLayerNumberFunc(AProject) -1 do begin
//         ALayer := GetLayerPointerByPositionFunc(AProject, iI);

{         ALayer := GetLayerPointerByPositionFunc(AProject, iLayerIndex);
         iP := LayerNamesComboBox.Items.Add(GetLayerNameFunc(ALayer));
// ++ Cadmensky
         StaticText4.Caption := ' ' + GetLayerNameFunc(ALayer);
         StaticText4.Color := clWindow;
// -- Cadmensky
         LayerNamesComboBox.Items.Objects[iP] := ALayer;

         AnExtTablesMan := ALayerManager.GetLayerExtTablesMan(ALayer);
         ATempMan := TempExtTablesMan.AddTempExtTablesMan(ALayer);
         TempExtTablesMan.Assign(AnExtTablesMan, ATempMan);
// Commented by Cadmensky
//         end; // for iI end
     TempExtTablesMan.DropModifiedSign;}
     // 357=MS Access database files (*.mdb)|*.mdb|Paradox files (*.db)|*.db|XBase files (*.dbf)|*.dbf
//     OD.Filter := ALangManager.GetValue(357);
   OpenDialog.Filter := 'MS Access database files (*.mdb)|*.mdb|Paradox database files (*.dbf)|*.dbf'; //|dBASE database files (*.db)|*.db';
   OpenDialog.InitialDir := ExtractFilePath (Application.ExeName);
end;

procedure TIDB_ParamsOfExternalTablesForm.FormDestroy (Sender: TObject);
begin
   TempExtTablesMan.Free;
end;

procedure TIDB_ParamsOfExternalTablesForm.SetCurrentLayer (ALayer: Pointer);
var
   sLayerName,
      sLinkName: AnsiString;
   sDisplayLinkName: AnsiString;
   iP: Integer;
   AnExtTablesMan,
      ATempMan: TIDB_BaseExternalTablesMan;
//   bOldModifiedSign: boolean;
begin
   sLayerName := GetLayerNameFunc (ALayer);
   iP := LayerNamesComboBox.Items.IndexOf (sLayerName);
   if iP < 0 then
   begin
      iP := LayerNamesComboBox.Items.Add (sLayerName);
      LayerNamesComboBox.Items.Objects[iP] := ALayer;
   end;
   LayerNamesComboBox.ItemIndex := iP;

// ++ Cadmensky
   StaticText4.Caption := ' ' + GetLayerNameFunc (ALayer);
   StaticText4.Color := clWindow;
// -- Cadmensky

   AnExtTablesMan := ALayerManager.GetLayerExtTablesMan (ALayer);
   ATempMan := TempExtTablesMan.AddTempExtTablesMan (ALayer);
   TempExtTablesMan.Assign (AnExtTablesMan, ATempMan);
   TempExtTablesMan.DropModifiedSign;

     // Get a manager of external tables and use it to fill a list with link names..
   LinkedTableCheckListBox.Items.Clear;
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);
   for iP := 0 to ATempMan.GetLinksCount - 1 do
   begin
      sLinkName := ATempMan.GetLinkName (iP);
      if sLinkName <> '' then
      begin
            // ++ Cadmensky
         sDisplayLinkName := Copy (sLinkName, Pos ('_', sLinkName) + 1, Length (sLinkName) - Pos ('_', sLinkName));
         LinkedTableCheckListBox.Items.Add (sDisplayLinkName);
            // -- Cadmensky
      end;
   end;

   for iP := 0 to LinkedTableCheckListBox.Items.Count - 1 do
   begin
         // ++ Cadmensky
      sDisplayLinkName := IntToStr (GetLayerIndexFunc (ALayer)) + '_' + LinkedTableCheckListBox.Items[iP];
      LinkedTableCheckListBox.Checked[iP] := ATempMan.GetLinkIsUsedState (sDisplayLinkName);
         // -- Cadmensky
   end;

   DeleteLinkSB.Enabled := LinkedTableCheckListBox.Items.Count > 0;
   RenameLinkSB.Enabled := LinkedTableCheckListBox.Items.Count > 0;
   UpSB.Enabled := LinkedTableCheckListBox.Items.Count > 1;
   DownSB.Enabled := LinkedTableCheckListBox.Items.Count > 1;
   LinkedTableCheckListBox.ItemIndex := 0;

   ReadData;
{     bOldModifiedSign := ATempMan.Modified;
     if not bOldModifiedSign then}
   ATempMan.DropModifiedSign;

end;

procedure TIDB_ParamsOfExternalTablesForm.LayerNamesComboBoxChange (Sender: TObject);
var
   ALayer: Pointer;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   SELF.SetCurrentLayer (ALayer);
end;

procedure TIDB_ParamsOfExternalTablesForm.DisableVisualization;
begin
   PageControl1.Enabled := FALSE;
   StaticText1.Caption := ''; // Database type
   StaticText2.Caption := ''; // Database file name
   StaticText3.Caption := ''; // Table name
   UserNameEdit.Text := '';
   UserNameEdit.Enabled := FALSE;
   PasswordEdit.Text := '';
   PasswordEdit.Enabled := FALSE;
   CodePageComboBox.ItemIndex := 0; // ANSI code page
   CodePageComboBox.Enabled := FALSE;
   TestConnectionButton.Enabled := FALSE;
   CheckBox1.Checked := FALSE;
   CheckBox1.Enabled := FALSE;
end;

procedure TIDB_ParamsOfExternalTablesForm.ReadData;
var
   ADatabaseType: TDatabaseType;
   sLinkName: AnsiString;
   ATempMan: TIDB_BaseExternalTablesMan;
// ++ Cadmensky Version 2.3.1
   AdditionalIDA: TIDB_Access2DB_Level2;
   iTableNumber: Integer;
// -- Cadmensky Version 2.3.1
   ALayer: Pointer;
begin
{$O-}
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);
   if ATempMan = nil then
   begin
      DisableVisualization;
      EXIT;
   end;
   if LinkedTableCheckListBox.ItemIndex < 0 then
   begin
      DisableVisualization;
      EXIT;
   end;
   try
       // Cadmensky
      sLinkName := IntToStr (GetLayerIndexFunc (ALayer)) + '_' +
         LinkedTableCheckListBox.Items[LinkedTableCheckListBox.ItemIndex];
   except
      sLinkName := '';
   end;

   ATempMan.SetLinkIsUsedState (sLinkName, LinkedTableCheckListBox.Checked[LinkedTableCheckListBox.ItemIndex]);

   PageControl1.ActivePage := TableParamsTS;
   Label4.Visible := TRUE;
   StaticText3.Visible := TRUE;
   ADatabaseType := ATempMan.GetLinkType (sLinkName);

   CheckBox1.Enabled := TRUE;
   CheckBox1.Checked := ATempMan.GetLinkKeepActive (sLinkName);
   case ADatabaseType of
      dtMSAccess:
         begin
            PageControl1.Enabled := TRUE;
            StaticText1.Caption := ALangManager.GetValue (350); // Database type = MS Access
            StaticText2.Caption := ' ' + ATempMan.GetLinkDatabaseFileName (sLinkName); // Database file name
            StaticText3.Caption := ' ' + ATempMan.GetLinkTableName (sLinkName); // Table name
            UserNameEdit.Enabled := TRUE;
            UserNameEdit.Text := ATempMan.GetLinkUserName (sLinkName);
            PasswordEdit.Enabled := TRUE;
            PasswordEdit.Text := ATempMan.GetLinkPassword (sLinkName);
(* !
          CodePageComboBox.Enabled := TRUE;
*)
            CodePageComboBox.Items.Text := ALangManager.GetValue (348); // MS Access can have only ANSI code page
            CodePageComboBox.ItemIndex := 0;
            TestConnectionButton.Enabled := TRUE;
// ++ Cadmensky Version 2.3.1
            SQLTablesComboBox.Clear;

            AdditionalIDA := TIDB_Access2DB_Level2.Create (SELF.AProject);
            if AdditionalIDA.Connect (ATempMan.GetLinkDatabaseFileName (sLinkName),
                                      ATempMan.GetLinkUserName (sLinkName),
                                      ATempMan.GetLinkPassword (sLinkName)) then
            begin
               AdditionalIDA.ADbItemsMan.GetTablesNames (SQLTablesComboBox.Items, false);
               AdditionalIDA.Disconnect;
               iTableNumber := SQLTablesComboBox.Items.IndexOf (ATempMan.GetLinkTableName (sLinkName));
               if iTableNumber <> -1 then
                  SQLTablesComboBox.ItemIndex := iTableNumber
               else
                  SQLTablesComboBox.ItemIndex := 0;

               ATempMan.SetLinkTableName (sLinkName, SQLTablesComboBox.Items[iTableNumber]);

               SQLTablesComboBox.Visible := true;
               StaticText3.Visible := false;
            end
            else
            begin
               SQLTablesComboBox.Visible := false;
               StaticText3.Visible := true;
            end;
            AdditionalIDA.Free;
// -- Cadmensky Version 2.3.1
         end;
      dtSQLServer:
         begin
            PageControl1.Enabled := TRUE;
            StaticText1.Caption := ALangManager.GetValue (357); // Database type = SQL Sever
            StaticText2.Caption := ' ' + ATempMan.GetLinkDatabaseFileName (sLinkName); // Database file name
            StaticText3.Caption := ' ' + ATempMan.GetLinkTableName (sLinkName); // Table name
            UserNameEdit.Enabled := TRUE;
            UserNameEdit.Text := ATempMan.GetLinkUserName (sLinkName);
            PasswordEdit.Enabled := TRUE;
            PasswordEdit.Text := ATempMan.GetLinkPassword (sLinkName);
(* !
          CodePageComboBox.Enabled := TRUE;
*)
            CodePageComboBox.Items.Text := ALangManager.GetValue (348); // MS Access can have only ANSI code page
            CodePageComboBox.ItemIndex := 0;
            TestConnectionButton.Enabled := TRUE;
         end;
      dtxBase, dtParadox:
         begin
            PageControl1.Enabled := TRUE;
            if ADatabaseType = dtParadox then
               StaticText1.Caption := ALangManager.GetValue (358) // Database type = Paradox
            else
               StaticText1.Caption := ALangManager.GetValue (359); // Database type = xBase

            StaticText2.Caption := ' ' + ATempMan.GetLinkDatabaseFileName (sLinkName); // Database file name

            StaticText3.Caption := ' ' + ATempMan.GetLinkTableName (sLinkName); // Table name
// ++ Cadmensky Version 2.3.1
            StaticText3.Visible := true;
            SQLTablesComboBox.Visible := false;
// -- Cadmensky Version 2.3.1
//          Label4.Visible := FALSE;

            UserNameEdit.Enabled := TRUE;
            UserNameEdit.Text := ATempMan.GetLinkUserName (sLinkName);
            PasswordEdit.Enabled := TRUE;
            PasswordEdit.Text := ATempMan.GetLinkPassword (sLinkName);
(* !
          CodePageComboBox.Enabled := TRUE;
*)
            CodePageComboBox.Items.Text := ALangManager.GetValue (348) + #13#10 + ALangManager.GetValue (349); // ANSI + OEM
            if ATempMan.GetLinkCodePage (sLinkName) = cpOEM then
               CodePageComboBox.ItemIndex := 1 // OEM code page
            else
               CodePageComboBox.ItemIndex := 0; // ANSI code page
            TestConnectionButton.Enabled := TRUE;
         end;
   else
      begin
         DisableVisualization;
      end;
   end; // case end
end;

procedure TIDB_ParamsOfExternalTablesForm.AddNewLinkSBClick (Sender: TObject);
var
   sNewLinkName: AnsiString;
   iP: Integer;
   bDummy: Boolean;
   PLFTS: TIDB_TableSelect;
   sTableName: AnsiString;
   AdditionalIDA: TIDB_Access2DB_Level2;
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
   iTypeOfDataBase: integer;
//   LinksOrder: TStringList; // Cadmensky
   sInitialCatalog: AnsiString;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);
  // 352=New_link
   sNewLinkName := ATempMan.GenerateLinkName (ALangManager.GetValue (352));
  // 351=Input new link name
   if not InputQuery (cs_ApplicationName, ALangManager.GetValue (351), sNewLinkName) then
   begin
      bDummy := false;
      exit;
   end;
   if Trim (sNewLinkName) = '' then
   begin
      bDummy := false;
      exit;
   end;

   PLFTS := TIDB_TableSelect.Create (Application);
   PLFTS.Notebook1.PageIndex := 0; //ActivePage := 'LOAD';
   PLFTS.ListBox1.Items.Add ('Microsoft Access Database');
   PLFTS.ListBox1.Items.Add ('dBASE table');
   PLFTS.ListBox1.Items.Add ('Paradox table');
// ++ Cadmensky Version 2.3.4
//   PLFTS.ListBox1.Items.Add ('SQL Server Table');
// -- Cadmensky Version 2.3.4
   PLFTS.ListBox1.ItemIndex := 0;
   if PLFTS.ShowModal <> mrOK then
   begin
      PLFTS.Free;
      exit;
   end;                                                            
   iTypeOfDataBase := PLFTS.ListBox1.ItemIndex + 1;
   PLFTS.Free;

   if (iTypeOfDataBase = 1) or
      (iTypeOfDataBase = 2) or
      (iTypeOfDataBase = 3) then
   begin
      OpenDialog.Filter := ALangManager.GetValue (412 + iTypeOfDataBase);

      if not OpenDialog.Execute then
         exit;
      if not FileExists (OpenDialog.FileName) then
      begin
        // 105=File '%s' does not exist.
         MessageBox (Handle, PChar (Format (ALangManager.GetValue (105), [OpenDialog.FileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
         exit;
      end;
   end; // if iTypeOfDatabase = 1, 2, 3

   if iTypeOfDataBase = 1 then
  // Microsoft Access table
   begin

// ++ Cadmensky Version 2.3.1
      AdditionalIDA := TIDB_Access2DB_Level2.Create (SELF.AProject);
      if not AdditionalIDA.Connect (OpenDialog.FileName, '', '') then
      begin
         AdditionalIDA.Free;
         exit;
      end;
// -- Cadmensky Version 2.3.1

      PLFTS := TIDB_TableSelect.Create (Application);
      PLFTS.Notebook1.PageIndex := 0; //ActivePage := 'LOAD';

// ++ Cadmensky Version 2.3.1
      AdditionalIDA.ADbItemsMan.GetTablesNames (PLFTS.ListBox1, FALSE);

      AdditionalIDA.Disconnect;
      AdditionalIDA.Free;
// -- Cadmensky Version 2.3.1

      if PLFTS.ListBox1.Items.Count < 1 then
      begin
         PLFTS.Free;
        // There isn''t any table in database file ''%s''.
         MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (106), [OpenDialog.FileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
         exit;
      end;
      PLFTS.ListBox1.ItemIndex := 0;
      if PLFTS.ShowModal <> mrOK then
      begin
         PLFTS.Free;
         exit;
      end;
// ++ Cadmensky Version 2.3.1
      SQLTablesComboBox.Items.Text := PLFTS.ListBox1.Items.Text;
      SQLTablesComboBox.ItemIndex := PLFTS.ListBox1.ItemIndex;
// -- Cadmensky Version 2.3.1
      sTableName := PLFTS.ListBox1.Items[PLFTS.ListBox1.ItemIndex];
      PLFTS.Free;
   end;
// ++ Cadmensky Version 2.3.4
{  if iTypeOfDataBase = 4 then
  // SQL Server Table
  begin
// ++ Cadmensky Version 2.3.1
      AdditionalIDA := TIDB_Access2DB_Level2.Create (SELF.AProject);
      if not AdditionalIDA.Connect ('ODISSEY\PROGIS', 'sa', '_qwerty_', 4) then
      begin
         AdditionalIDA.Free;
         exit;
      end;
// -- Cadmensky Version 2.3.1
      PLFTS := TIDB_TableSelect.Create (Application);
      PLFTS.Notebook1.PageIndex := 0; //ActivePage := 'LOAD';

      AdditionalIDA.DMaster.CentralData.GetInitialCatalogNames (PLFTS.ListBox1.Items);

      if PLFTS.ListBox1.Items.Count < 1 then
      begin
         PLFTS.Free;
        // There isn''t any table in database file ''%s''.
         MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (106), [OpenDialog.FileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
         exit;
      end;
      PLFTS.ListBox1.ItemIndex := 0;
      if PLFTS.ShowModal <> mrOK then
      begin
         PLFTS.Free;
         exit;
      end;

      sInitialCatalog := PLFTS.ListBox1.Items[PLFTS.ListBox1.ItemIndex];
      if not AdditionalIDA.Connect ('ODISSEY\PROGIS', 'sa', '_qwerty_', 4, sInitialCatalog) then
      begin
         AdditionalIDA.Free;
         exit;
      end;

// ++ Cadmensky Version 2.3.1
      AdditionalIDA.ADbItemsMan.GetTablesNames (PLFTS.ListBox1, FALSE);

      AdditionalIDA.Disconnect;
      AdditionalIDA.Free;
// -- Cadmensky Version 2.3.1

      if PLFTS.ListBox1.Items.Count < 1 then
      begin
         PLFTS.Free;
        // There isn''t any table in database file ''%s''.
         MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (106), [OpenDialog.FileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
         exit;
      end;
      PLFTS.ListBox1.ItemIndex := 0;
      if PLFTS.ShowModal <> mrOK then
      begin
         PLFTS.Free;
         exit;
      end;
// ++ Cadmensky Version 2.3.1
      SQLTablesComboBox.Items.Text := PLFTS.ListBox1.Items.Text;
      SQLTablesComboBox.ItemIndex := PLFTS.ListBox1.ItemIndex;
// -- Cadmensky Version 2.3.1
      sTableName := PLFTS.ListBox1.Items[PLFTS.ListBox1.ItemIndex];
      PLFTS.Free;
  end;}
   if ATempMan.LinkExists (OpenDialog.FileName, sTableName) then
   begin
      if OpenDialog.FilterIndex = 1 then
           // 360=There is alredy a link to database '%s' (table name '%s')
         MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (360), [OpenDialog.FileName, sTableName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP)
      else
        // 361=There is alredy a link to database file '%s'
         MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (361), [OpenDialog.FileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
      EXIT;
   end;
   iP := LinkedTableCheckListBox.Items.Add (sNewLinkName);
   sNewLinkName := ATempMan.AddLink (IntToStr (GetLayerIndexFunc (ALayer)) + '_' + sNewLinkName);
   LinkedTableCheckListBox.ItemIndex := iP;
   LinkedTableCheckListBoxClick (SENDER);
   case iTypeOfDataBase of
      1:
         begin // MS Access
            ATempMan.SetLinkType (sNewLinkName, dtMSAccess);
            ATempMan.SetLinkDatabaseFileName (sNewLinkName, OpenDialog.FileName);
            ATempMan.SetLinkTableName (sNewLinkName, sTableName);
            ATempMan.SetLinkUserName (sNewLinkName, '');
            ATempMan.SetLinkPassword (sNewLinkName, '');
            ATempMan.SetLinkCodePage (sNewLinkName, cpANSI);
         end;
      2:
         begin // xBase files
            ATempMan.SetLinkType (sNewLinkName, dtxBase);
            ATempMan.SetLinkDatabaseFileName (sNewLinkName, ExtractFilePath (OpenDialog.FileName));
            ATempMan.SetLinkTableName (sNewLinkName, ExtractFileName (OpenDialog.FileName));
            ATempMan.SetLinkUserName (sNewLinkName, '');
            ATempMan.SetLinkPassword (sNewLinkName, '');
            ATempMan.SetLinkCodePage (sNewLinkName, cpANSI);
         end;
      3:
         begin // Paradox files
            ATempMan.SetLinkType (sNewLinkName, dtParadox);
            ATempMan.SetLinkDatabaseFileName (sNewLinkName, ExtractFilePath (OpenDialog.FileName));
            ATempMan.SetLinkTableName (sNewLinkName, ExtractFileName (OpenDialog.FileName));
            ATempMan.SetLinkUserName (sNewLinkName, '');
            ATempMan.SetLinkPassword (sNewLinkName, '');
            ATempMan.SetLinkCodePage (sNewLinkName, cpANSI);
         end;
      4:
         begin // SQL Server tables
            ATempMan.SetLinkType (sNewLinkName, dtSQLServer);
            ATempMan.SetLinkDatabaseFileName (sNewLinkName, sInitialCatalog); // Initial catalog
            ATempMan.SetLinkTableName (sNewLinkName, sTableName);
            ATempMan.SetLinkServerName (sNewLinkName, 'ODISSEY\PROGIS');
            ATempMan.SetLinkUserName (sNewLinkName, 'sa');
            ATempMan.SetLinkPassword (sNewLinkName, '_qwerty_');
            ATempMan.SetLinkCodePage (sNewLinkName, cpANSI);
         end;
   end; // case end
   ATempMan.SetLinkIsUsedState (sNewLinkName, TRUE);
   LinkedTableCheckListBox.Checked[LinkedTableCheckListBox.ItemIndex] := TRUE;
   LinkedTableCheckListBoxClick (SENDER);
   DeleteLinkSB.Enabled := TRUE;
   RenameLinkSB.Enabled := TRUE;
   UpSB.Enabled := LinkedTableCheckListBox.Items.Count > 1;
   DownSB.Enabled := LinkedTableCheckListBox.Items.Count > 1;

     // ++ Cadmensky
// ++ Commented by Cadmensky Version 2.3.4
{   LinksOrder := TStringList.Create;
   for iP := 0 to LinkedTableCheckListBox.Items.Count - 1 do
   begin
      LinksOrder.Add (IntToStr (GetLayerIndexFunc (ALayer)) + '_' + LinkedTableCheckListBox.Items[iP]);
   end;
//     ATempMan.SetLinksOrder(LinkedTableCheckListBox.Items.Text);
   ATempMan.SetLinksOrder (LinksOrder.Text);
   LinksOrder.Free;}
// -- Commented by Cadmensky Version 2.3.4
     // -- Cadmensky
end;

procedure TIDB_ParamsOfExternalTablesForm.DeleteLinkSBClick (Sender: TObject);
var
   iLinkNum: Integer;
   sLinkName: AnsiString;
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

   iLinkNum := LinkedTableCheckListBox.ItemIndex;
// Cadmensky
//     sLinkName := LinkedTableCheckListBox.Items[iLinkNum];
   sLinkName := IntToStr (GetLayerIndexFunc (ALayer)) + '_' +
      LinkedTableCheckListBox.Items[iLinkNum];
     // 353=do you want to delete link '%s'?
   if MessageBox (Handle, PChar (Format (ALangManager.GetValue (353), [LinkedTableCheckListBox.Items[iLinkNum]])), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrYes then
   begin
      ATempMan.DeleteLink (sLinkName);
      LinkedTableCheckListBox.Items.Delete (iLinkNum);
      if iLinkNum >= LinkedTableCheckListBox.Items.Count then
         LinkedTableCheckListBox.ItemIndex := LinkedTableCheckListBox.Items.Count - 1
      else
         if LinkedTableCheckListBox.ItemIndex < 0 then
            LinkedTableCheckListBox.ItemIndex := 0;
   end;

   DeleteLinkSB.Enabled := ATempMan.GetLinksCount > 0;
   RenameLinkSB.Enabled := ATempMan.GetLinksCount > 0;
   UpSB.Enabled := LinkedTableCheckListBox.Items.Count > 1;
   DownSB.Enabled := LinkedTableCheckListBox.Items.Count > 1;
// ++ Commented by Cadmensky Version 2.3.4
//   ATempMan.SetLinksOrder (LinkedTableCheckListBox.Items.Text);
// -- Commented by Cadmensky Version 2.3.4
end;

procedure TIDB_ParamsOfExternalTablesForm.LinkedTableCheckListBoxClick (Sender: TObject);
begin
   ReadData;
end;

procedure TIDB_ParamsOfExternalTablesForm.LinkedTableCheckListBoxDblClick (Sender: TObject);
var
   sOldLinkName,
      sNewLinkName: AnsiString;
   iP: Integer;
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

   iP := LinkedTableCheckListBox.ItemIndex;
   if iP > -1 then
   begin
      sOldLinkName := LinkedTableCheckListBox.Items[iP];
      sNewLinkName := sOldLinkName;
        // 354=Input link name
      if InputQuery (cs_ApplicationName, ALangManager.GetValue (354), sNewLinkName) then
      begin
         if Trim (sNewLinkName) <> '' then
         begin
            ATempMan.SetLinkName (sOldLinkName, sNewLinkName);
            LinkedTableCheckListBox.Items[iP] := sNewLinkName;
// ++ Commented by Cadmensky Version 2.3.4
//            ATempMan.SetLinksOrder (LinkedTableCheckListBox.Items.Text);
// -- Commented by Cadmensky Version 2.3.4
         end;
      end;
   end;
end;

procedure TIDB_ParamsOfExternalTablesForm.RenameLinkSBClick (Sender: TObject);
begin
   LinkedTableCheckListBoxDblClick (SENDER);
end;

procedure TIDB_ParamsOfExternalTablesForm.SelectAllPopUpClick (Sender: TObject);
var
   iI: Integer;
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

   for iI := 0 to LinkedTableCheckListBox.Items.Count - 1 do
   begin
      LinkedTableCheckListBox.Checked[iI] := TRUE;
      ATempMan.SetLinkIsUsedState (LinkedTableCheckListBox.Items[iI], TRUE);
   end;
end;

procedure TIDB_ParamsOfExternalTablesForm.DeselectAllPopUpClick (Sender: TObject);
var
   iI: Integer;
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

   for iI := 0 to LinkedTableCheckListBox.Items.Count - 1 do
   begin
      LinkedTableCheckListBox.Checked[iI] := FALSE;
      ATempMan.SetLinkIsUsedState (LinkedTableCheckListBox.Items[iI], FALSE);
   end;
end;

procedure TIDB_ParamsOfExternalTablesForm.InvertSelectionPopUpClick (Sender: TObject);
var
   iI: Integer;
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

   for iI := 0 to LinkedTableCheckListBox.Items.Count - 1 do
   begin
      LinkedTableCheckListBox.Checked[iI] := not LinkedTableCheckListBox.Checked[iI];
      ATempMan.SetLinkIsUsedState (LinkedTableCheckListBox.Items[iI], LinkedTableCheckListBox.Checked[iI]);
   end;
end;

procedure TIDB_ParamsOfExternalTablesForm.PopupMenu1Popup (Sender: TObject);
var
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

   SelectAllPopUp.Enabled := LinkedTableCheckListBox.Items.Count > 1;
   DeselectAllPopUp.Enabled := LinkedTableCheckListBox.Items.Count > 1;
   InvertSelectionPopUp.Enabled := LinkedTableCheckListBox.Items.Count > 1;

   DeleteLinkPopUp.Enabled := ATempMan.GetLinksCount > 0;
   RenameLinkPopUp.Enabled := ATempMan.GetLinksCount > 0;
end;

procedure TIDB_ParamsOfExternalTablesForm.TestConnectionButtonClick (Sender: TObject);
var
   AdditionalIDA: TIDB_Access2DB_Level2;
   sLinkName, sDBFileName: AnsiString;
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
   dtLinkType: TDatabaseType;
   ATable: TDTable;
   sDBType: AnsiString;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

// Cadmensky
//     sLinkName := LinkedTableCheckListBox.Items[LinkedTableCheckListBox.ItemIndex];
   sLinkName := IntToStr (GetLayerIndexFunc (ALayer)) + '_' +
      LinkedTableCheckListBox.Items[LinkedTableCheckListBox.ItemIndex];
   dtLinkType := ATempMan.GetLinkType (sLinkName);
   sDBFileName := ATempMan.GetLinkDatabaseFileName (sLinkName);
// ++ Commented by Cadmensky Version 2.3.1
//   SQLTablesComboBox.Clear;
// -- Commented by Cadmensky Version 2.3.1

// ++ Cadmensky Version 2.3.1
   case dtLinkType of
      dtMSAccess, dtSQLServer:
         begin
            AdditionalIDA := TIDB_Access2DB_Level2.Create (SELF.AProject);
            if not AdditionalIDA.Connect (sDBFileName, ATempMan.GetLinkUserName (sLinkName), ATempMan.GetLinkPassword (sLinkName)) then
               MessageBox (Handle, PChar (ALangManager.GetValue (363)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION)
            else
            begin
               AdditionalIDA.Disconnect;
               // 356=Test connection succeeded
               MessageBox (Handle, PChar (ALangManager.GetValue (356)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION);
            end;
            AdditionalIDA.Free;
         end;
      dtxBase, dtParadox:
         begin
            ATable := TDTable.Create (nil);
            if dtLinkType = dtxBase then
               sDBType := 'dBase 5.0'
            else
               sDBType := 'Paradox 5.x';
            ATable.Connection := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + sDBFileName +
               ';' + 'Extended Properties=' + sDBType + ';' +
               'Mode=ReadWrite|Share Deny None;Persist Security Info=False';
            ATable.TableName := ATempMan.GetLinkTableName (sLinkName);
            try
               ATable.Open;
               // 356=Test connection succeeded
               MessageBox (Handle, PChar (ALangManager.GetValue (356)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION);
               ATable.Free;
            except
               ATable.Free;
               MessageBox (Handle, PChar (ALangManager.GetValue (363)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION);
            end;
         end;
   end;
// -- Cadmensky Version 2.3.1

end;

procedure TIDB_ParamsOfExternalTablesForm.UserNameEditChange (Sender: TObject);
var
   sLinkName: AnsiString;
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

// Cadmensky
//     sLinkName := LinkedTableCheckListBox.Items[LinkedTableCheckListBox.ItemIndex];
   sLinkName := IntToStr (GetLayerIndexFunc (ALayer)) + '_' +
      LinkedTableCheckListBox.Items[LinkedTableCheckListBox.ItemIndex];
   ATempMan.SetLinkUserName (sLinkName, UserNameEdit.Text);
end;

procedure TIDB_ParamsOfExternalTablesForm.PasswordEditChange (Sender: TObject);
var
   sLinkName: AnsiString;
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

// Cadmensky
//     sLinkName := LinkedTableCheckListBox.Items[LinkedTableCheckListBox.ItemIndex];
   sLinkName := IntToStr (GetLayerIndexFunc (ALayer)) + '_' +
      LinkedTableCheckListBox.Items[LinkedTableCheckListBox.ItemIndex];
   ATempMan.SetLinkPassword (sLinkName, PasswordEdit.Text);
end;

procedure TIDB_ParamsOfExternalTablesForm.CodePageComboBoxChange (Sender: TObject);
var
   sLinkName: AnsiString;
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

// Cadmensky
//     sLinkName := LinkedTableCheckListBox.Items[LinkedTableCheckListBox.ItemIndex];
   sLinkName := IntToStr (GetLayerIndexFunc (ALayer)) + '_' +
      LinkedTableCheckListBox.Items[LinkedTableCheckListBox.ItemIndex];
   case ATempMan.GetLinkType (sLinkName) of
      dtMSAccess: ATempMan.SetLinkCodePage (sLinkName, cpANSI);
      dtxBase, dtParadox:
         if CodePageComboBox.ItemIndex = 1 then
            ATempMan.SetLinkCodePage (sLinkName, cpOEM)
         else
            ATempMan.SetLinkCodePage (sLinkName, cpANSI);
   end; // case end
end;

procedure TIDB_ParamsOfExternalTablesForm.FieldsTSShow (Sender: TObject);
var
   sLinkName, sFieldName: AnsiString;
   iI: Integer;
   bF: Boolean;
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

   if LinkedTableCheckListBox.ItemIndex > -1 then
   begin
// Cadmensky
//        sLinkName := LinkedTableCheckListBox.Items[LinkedTableCheckListBox.ItemIndex];
      sLinkName := IntToStr (GetLayerIndexFunc (ALayer)) + '_' +
         LinkedTableCheckListBox.Items[LinkedTableCheckListBox.ItemIndex];
      sWorkLinkName := sLinkName;
      SelectedFieldsListBox.Items.Text := ATempMan.GetLinkSelectedFields (sLinkName);
      AvailableFieldsListBox.Items.Text := ATempMan.GetLinkAllFieldNames (sLinkName);
      // Fill the combobox that contains fields and shows the field with ProgisID data.
      ComboBox3.Items.Text := ATempMan.GetLinkIntegerFieldNames (sLinkName);
      iI := 0;
      while iI < AvailableFieldsListBox.Items.Count do
      begin
         sFieldName := AvailableFieldsListBox.Items[iI];
         if SelectedFieldsListBox.Items.IndexOf (sFieldName) > -1 then
            AvailableFieldsListBox.Items.Delete (iI)
         else
            Inc (iI);
      end; // While end
        // Show field with ProgisID information.
      iI := ComboBox3.Items.IndexOf (ATempMan.GetLinkProgisIDFieldName (sLinkName));
      if iI > -1 then
      begin
         bF := FALSE;
         for iI := 0 to ComboBox3.Items.Count - 1 do
            if AnsiUpperCase (ComboBox3.Items[iI]) = 'PROGISID' then
            begin
               bF := TRUE;
               BREAK;
            end;
         if not bF then
            iI := 0;
      end;
      ComboBox3.ItemIndex := iI;
   end;
end;

procedure TIDB_ParamsOfExternalTablesForm.OKButtonClick (Sender: TObject);
var
   iI: Integer;
   ALayer: Pointer;
   bF: Boolean;
   AnExtTablesMan: TIDB_ExternalTablesMan;
   ATempMan: TIDB_BaseExternalTablesMan;
begin
   bF := FALSE;
   for iI := 0 to LayerNamesComboBox.Items.Count - 1 do
   begin
      ALayer := LayerNamesComboBox.Items.Objects[iI];
      if ALayer <> nil then
      begin
//         AnExtTablesMan := ALayerManager.GetLayerExtTablesMan (ALayer);
         ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);
         if ATempMan.Modified then
         begin
            bF := TRUE;
            BREAK;
         end;
      end;
   end; // for iI end
   if bF then
        // 371=Connection parameters of some layers were modified. Apply them?
      bF := MessageBox (Handle, PChar (ALangManager.GetValue (371)), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrYes;
   if bF then
   begin
      for iI := 0 to LayerNamesComboBox.Items.Count - 1 do
      begin
         ALayer := LayerNamesComboBox.Items.Objects[iI];
         if ALayer <> nil then
         begin
            AnExtTablesMan := ALayerManager.GetLayerExtTablesMan (ALayer);
            ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);
               // Transmit data to external tables manager.
            AnExtTablesMan.ClearData;
            TempExtTablesMan.Assign (ATempMan, AnExtTablesMan);
         end;
      end; // for iI end;
   end
   else
      SELF.ModalResult := mrCancel;
end;

procedure TIDB_ParamsOfExternalTablesForm.SpeedButton3Click (Sender: TObject);
var
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

   AddOneValue (SelectedFieldsListBox, AvailableFieldsListBox);
   ATempMan.SetLinkSelectedFields (sWorkLinkName, SelectedFieldsListBox.Items.Text);
end;

procedure TIDB_ParamsOfExternalTablesForm.SpeedButton4Click (Sender: TObject);
var
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

   AddAllValues (SelectedFieldsListBox, AvailableFieldsListBox);
   ATempMan.SetLinkSelectedFields (sWorkLinkName, '');
end;

procedure TIDB_ParamsOfExternalTablesForm.SpeedButton5Click (Sender: TObject);
var
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

   AddOneValue (AvailableFieldsListBox, SelectedFieldsListBox);
   ATempMan.SetLinkSelectedFields (sWorkLinkName, SelectedFieldsListBox.Items.Text);
end;

procedure TIDB_ParamsOfExternalTablesForm.SpeedButton6Click (Sender: TObject);
var
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

   AddAllValues (AvailableFieldsListBox, SelectedFieldsListBox);
   ATempMan.SetLinkSelectedFields (sWorkLinkName, SelectedFieldsListBox.Items.Text);
end;

procedure TIDB_ParamsOfExternalTablesForm.SelectedFieldsListBoxClick (Sender: TObject);
begin
   SpeedButton3.Click;
end;

procedure TIDB_ParamsOfExternalTablesForm.AvailableFieldsListBoxClick (Sender: TObject);
begin
   SpeedButton5.Click;
end;

procedure TIDB_ParamsOfExternalTablesForm.ComboBox3Change (Sender: TObject);
var
   sLinkName: AnsiString;
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   if LinkedTableCheckListBox.ItemIndex > -1 then
   begin
      ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
      ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

// Cadmensky
//        sLinkName := LinkedTableCheckListBox.Items[LinkedTableCheckListBox.ItemIndex];
      sLinkName := IntToStr (GetLayerIndexFunc (ALayer)) + '_' +
         LinkedTableCheckListBox.Items[LinkedTableCheckListBox.ItemIndex];
      ATempMan.SetLinkProgisIDFieldName (sLinkName, ComboBox3.Items[ComboBox3.ItemIndex]);
   end;
end;

procedure TIDB_ParamsOfExternalTablesForm.UpSBClick (Sender: TObject);
var
   iP: Integer;
   sTemp: AnsiString;
   bTemp: Boolean;
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   iP := LinkedTableCheckListBox.ItemIndex;
   if iP > 0 then
   begin
      ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
      ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

      sTemp := LinkedTableCheckListBox.Items[iP - 1];
      bTemp := LinkedTableCheckListBox.Checked[iP - 1];

      LinkedTableCheckListBox.Items[iP - 1] := LinkedTableCheckListBox.Items[iP];
      LinkedTableCheckListBox.Checked[iP - 1] := LinkedTableCheckListBox.Checked[iP];

      LinkedTableCheckListBox.Items[iP] := sTemp;
      LinkedTableCheckListBox.Checked[iP] := bTemp;

      LinkedTableCheckListBox.ItemIndex := LinkedTableCheckListBox.ItemIndex - 1;

// ++ Commented by Cadmensky Version 2.3.4
//      ATempMan.SetLinksOrder (LinkedTableCheckListBox.Items.Text);
// -- Commented by Cadmensky Version 2.3.4
   end;
end;

procedure TIDB_ParamsOfExternalTablesForm.DownSBClick (Sender: TObject);
var
   iP: Integer;
   sTemp: AnsiString;
   bTemp: Boolean;
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   iP := LinkedTableCheckListBox.ItemIndex;
   if iP < LinkedTableCheckListBox.Items.Count - 1 then
   begin
      ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
      ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

      sTemp := LinkedTableCheckListBox.Items[iP + 1];
      bTemp := LinkedTableCheckListBox.Checked[iP + 1];

      LinkedTableCheckListBox.Items[iP + 1] := LinkedTableCheckListBox.Items[iP];
      LinkedTableCheckListBox.Checked[iP + 1] := LinkedTableCheckListBox.Checked[iP];

      LinkedTableCheckListBox.Items[iP] := sTemp;
      LinkedTableCheckListBox.Checked[iP] := bTemp;

      LinkedTableCheckListBox.ItemIndex := LinkedTableCheckListBox.ItemIndex + 1;

// ++ Commented by Cadmensky Version 2.3.4
//      ATempMan.SetLinksOrder (LinkedTableCheckListBox.Items.Text);
// -- Commented by Cadmensky Version 2.3.4
   end;
end;

procedure TIDB_ParamsOfExternalTablesForm.SQLTablesComboBoxChange (Sender: TObject);
var
   sLinkName: AnsiString;
   ATempMan: TIDB_BaseExternalTablesMan;
   ALayer: Pointer;
begin
   ALayer := LayerNamesComboBox.Items.Objects[LayerNamesComboBox.ItemIndex];
   ATempMan := TempExtTablesMan.GetTempExtTablesMan (ALayer);

// Cadmensky
//     sLinkName := LinkedTableCheckListBox.Items[LinkedTableCheckListBox.ItemIndex];
   sLinkName := IntToStr (GetLayerIndexFunc (ALayer)) + '_' +
      LinkedTableCheckListBox.Items[LinkedTableCheckListBox.ItemIndex];
   ATempMan.SetLinkTableName (sLinkName, SQLTablesComboBox.Items[SQLTablesComboBox.ItemIndex]);
   ATempMan.SetLinkSelectedFields (sLinkName, '');
end;

end.

