unit IDB_ParamsOfSearch;
{
Internal database. Ver. II.
Form for set search parameters by user.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 02-02-2001

A one big problem for search on index is that the search by partial key matches
is possible only for initial parts of field values.
}
interface

uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   DB, StdCtrls, ExtCtrls;

type
   TIDB_SearchParamsForm = class (TForm)
      OKButton: TButton;
      CancelButton: TButton;
      Panel1: TPanel;
      Label1: TLabel;
      Label2: TLabel;
      SearchValueEdit: TEdit;
      FieldsForSearchComboBox: TComboBox;
      CaseSensitiveCheckBox: TCheckBox;
      WholeKeyCheckBox: TCheckBox;
      procedure FormActivate (Sender: TObject);
      procedure FormCreate (Sender: TObject);
      procedure FieldsForSearchComboBoxKeyDown (Sender: TObject; var Key: Word; Shift: TShiftState);
      procedure SearchValueEditKeyDown (Sender: TObject; var Key: Word; Shift: TShiftState);
   private
    { Private declarations }
   public
    { Public declarations }
      loLocateOption: TLocateOptions;
      procedure SetSearchParams (sSearchField, sSearchValue: AnsiString; loLocOpt: TLocateOptions);
      procedure GetSearchParams (var sSearchField: AnsiString; var sSearchValue: AnsiString; var loLocOpt: TLocateOptions);
   end;

implementation

uses
   IDB_LanguageManager;

{$R *.DFM}

procedure TIDB_SearchParamsForm.SetSearchParams (sSearchField, sSearchValue: AnsiString; loLocOpt: TLocateOptions);
var
   iP: Integer;
begin
     // Move ComboBox on current field name
   iP := SELF.FieldsForSearchComboBox.Items.IndexOf (sSearchField);
   if iP < 0 then iP := 0;
   FieldsForSearchComboBox.ItemIndex := iP;
     // Set value for a searching.
   SELF.SearchValueEdit.Text := sSearchValue;
     // Set TCheckBoxes
   CaseSensitiveCheckBox.Checked := (loCaseInsensitive in loLocOpt);
   WholeKeyCheckBox.Checked := (not (loPartialKey in loLocOpt));
end;

procedure TIDB_SearchParamsForm.GetSearchParams (var sSearchField: AnsiString; var sSearchValue: AnsiString; var loLocOpt: TLocateOptions);
begin
   sSearchField := SELF.FieldsForSearchComboBox.Items[FieldsForSearchComboBox.ItemIndex];
   sSearchValue := SELF.SearchValueEdit.Text;
   loLocOpt := [];
   if CaseSensitiveCheckBox.Checked then
      loLocOpt := loLocOpt + [loCaseInsensitive];
   if not WholeKeyCheckBox.Checked then
      loLocOpt := loLocOpt + [loPartialKey];
end;

procedure TIDB_SearchParamsForm.FormActivate (Sender: TObject);
begin
   SearchValueEdit.SetFocus;
end;

procedure TIDB_SearchParamsForm.FormCreate (Sender: TObject);
begin
   ALangManager.MakeReplacement (SELF);
end;

procedure TIDB_SearchParamsForm.FieldsForSearchComboBoxKeyDown (Sender: TObject; var Key: Word; Shift: TShiftState);
begin
   if not (SENDER is TCombobox) then EXIT; // Check just in case.
     // If ESC key has been pressed on dropped ComboBox, let it itself do what it have to.
   if FieldsForSearchComboBox.DroppedDown then EXIT;
     // If ESC key has been pressed and Combobox is not dropped, check our behavior.
   case Key of
      vk_Return:
         begin
            Key := 0;
            OKButton.Click;
         end;
      vk_Escape:
         begin
            Key := 0;
            CancelButton.Click;
         end;
   end; // case end
end;

procedure TIDB_SearchParamsForm.SearchValueEditKeyDown (Sender: TObject; var Key: Word; Shift: TShiftState);
begin
   if not (SENDER is TEdit) then EXIT; // Check just in case.
     // If ESC key has been pressed on Edit, check our behavior.
   case Key of
      vk_Return:
         begin
            Key := 0;
            OKButton.Click;
         end;
      vk_Escape:
         begin
            Key := 0;
            CancelButton.Click;
         end;
   end; // case end
end;

end.

