unit IDB_ListOfQueryWindows;
{
Internal Database. Ver.II.
Form for selecting by user which window a query result must be displayed in.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 05-03-2001
}
interface

uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   StdCtrls, ExtCtrls, Buttons;

type
   TIDB_ListQueryWindowForm = class (TForm)
      Panel2: TPanel;
      Panel1: TPanel;
      RadioButton1: TRadioButton;
      RadioButton2: TRadioButton;
      ListBox1: TListBox;
      Edit1: TEdit;
      OKButton: TButton;
      CancelButton: TButton;
      Label2: TLabel;
      Label3: TLabel;
      Label4: TLabel;
      DeleteSB: TSpeedButton;
    Label1: TLabel;
      procedure RadioButton1Click (Sender: TObject);
      procedure RadioButton2Click (Sender: TObject);
      procedure FormShow (Sender: TObject);
      procedure Edit1Change (Sender: TObject);
      procedure ListBox1DblClick (Sender: TObject);
      procedure FormCreate (Sender: TObject);
    procedure DeleteSBClick(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
      procedure MakeSaveQueryUI;
   end;

implementation
{$R *.DFM}

uses
   IDB_LanguageManager;

procedure TIDB_ListQueryWindowForm.RadioButton1Click (Sender: TObject);
begin
   ListBox1.Enabled := TRUE;
   DeleteSB.Enabled := true;
   if ListBox1.ItemIndex < 0 then ListBox1.ItemIndex := 0;
   Edit1.Enabled := FALSE;
   OKButton.Enabled := TRUE;
end;

procedure TIDB_ListQueryWindowForm.RadioButton2Click (Sender: TObject);
begin
   ListBox1.Enabled := FALSE;
   DeleteSB.Enabled := false;
   Edit1.Enabled := TRUE;
   if Trim (Edit1.Text) = '' then
      OKButton.Enabled := FALSE
   else
      OKButton.Enabled := TRUE;
   Edit1.SetFocus;
end;

procedure TIDB_ListQueryWindowForm.FormShow (Sender: TObject);
begin
   if (Listbox1.ItemIndex< 0) and (listbox1.Items.Count>0) then ListBox1.ItemIndex := 0;
   // New result
   Edit1.Text := ALangManager.GetValue (217);
end;

procedure TIDB_ListQueryWindowForm.Edit1Change (Sender: TObject);
begin
   if Trim (Edit1.Text) = '' then
      OKButton.Enabled := FALSE
   else
      OKButton.Enabled := TRUE;
end;

procedure TIDB_ListQueryWindowForm.ListBox1DblClick (Sender: TObject);
begin
   OKButton.Click;
end;

procedure TIDB_ListQueryWindowForm.FormCreate (Sender: TObject);
begin
   ALangManager.MakeReplacement (SELF);
end;


procedure TIDB_ListQueryWindowForm.DeleteSBClick(Sender: TObject);
begin
   SELF.ListBox1.Items.Delete (SELF.ListBox1.ItemIndex);
   SELF.ListBox1.ItemIndex := 0;
end;

procedure TIDB_ListQueryWindowForm.MakeSaveQueryUI;
var iDY: integer;
begin
   SELF.Caption := ALangManager.GetValue (497);
   SELF.RadioButton1.Caption := ALangManager.GetValue (435);
   SELF.RadioButton2.Caption := ALangManager.GetValue (436);
   SELF.Edit1.Left := SELF.ListBox1.Left;
   SELF.Edit1.Width := SELF.ListBox1.Width;
   SELF.Label1.Visible := false;
   SELF.Label2.Visible := false;
   SELF.Label3.Visible := false;
   SELF.Label4.Visible := false;

   iDY := SELF.ListBox1.Top - SELF.RadioButton1.Top;
   SELF.ListBox1.Height := SELF.ListBox1.Height + iDY;
   SELF.DeleteSB.Top := SELF.DeleteSB.Top - iDY;

   SELF.ListBox1.Top := SELF.RadioButton1.Top;
   SELF.RadioButton1.Top := SELF.Label1.Top;

   SELF.DeleteSB.Visible := true;
end;

end.

