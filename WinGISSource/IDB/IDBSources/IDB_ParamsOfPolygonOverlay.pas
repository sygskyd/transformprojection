unit IDB_ParamsOfPolygonOverlay;
{
Internal database. Ver. II.
The form of parameters for merging attribute data during polygon overlaying operation.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 06-12-2001
}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, IDB_Grids, IDB_C_StringGrid, Menus, ImgList, Buttons, Grids,
  IDB_StructMan, IDB_Validators, IDB_PolygonOverlayManager;

type
  TIDB_ParamsOfPolygonOverlayForm = class(TForm)
    BottomPanel: TPanel;
    Panel2: TPanel;
    OKButton: TButton;
    CancelButton: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    PopupMenu1: TPopupMenu;
    ImageList1: TImageList;
    CopyPopUp: TMenuItem;
    CutPopUp: TMenuItem;
    PastePopUp: TMenuItem;
    N1: TMenuItem;
    ShowStructurePopUp: TMenuItem;
    RunBuilderBB: TBitBtn;
    ListBox1: TListBox;
    ListBox2: TListBox;
    SG: TIDB_StringGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ShowStructurePopUpClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure SGDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure SGMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SGMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure RunBuilderBBClick(Sender: TObject);
    procedure SGDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure SGDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure SGSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: String);
  private
    { Private declarations }
    AProject: Pointer;
    AStructMan: TIDB_StructMan;
    iFieldNameCol,
    iFieldTypeCol,
    iFieldValueCol: Integer;

    IntValidator: TIDB_IntValidator;
    FloatValidator: TIDB_FloatValidator;
    procedure SwitchToEditMode;
    procedure SwitchToBrowseMode;
//    procedure DisconnectAllValidators;

    procedure FillTheGrid;
    procedure SetFieldTypeAndSizeByFullName(sFullFieldName: AnsiString; AOverlayField: TOverlayField);
    function FieldHasStringType(ALayer: Pointer; sFieldName: AnsiString): boolean;
  public
    { Public declarations }
    ASourceLayer1, ASourceLayer2: Pointer;
    constructor Create(AOwner: TComponent; AProject: Pointer; AStructMan: TIDB_StructMan);
  end;

implementation

{$R *.DFM}

uses
     IDB_LanguageManager, IDB_Utils, IDB_StructureAndProperties, IDB_MainManager,
     IDB_ExpressionBuilder, IDB_ProportionProps,
     IDB_Access2DB, IDB_CallbacksDef;

constructor TIDB_ParamsOfPolygonOverlayForm.Create(AOwner: TComponent; AProject: Pointer; AStructMan: TIDB_StructMan);
begin
     inherited Create(AOwner);
     SELF.AProject := AProject;
     SELF.AStructMan := AStructMan;
     SELF.IntValidator := TIDB_IntValidator.Create(SELF);
     SELF.FloatValidator := TIDB_FloatValidator.Create(SELF);
end;

procedure TIDB_ParamsOfPolygonOverlayForm.FormCreate(Sender: TObject);
begin
     ALangManager.MakeReplacement(SELF);

     iFieldNameCol := 0;
     iFieldTypeCol := 1;
     iFieldValueCol := 2;

     SG.Cells[iFieldNameCol, 0] := ALangManager.GetValue(310); // 310=Field name
     SG.Cells[iFieldTypeCol, 0] := ALangManager.GetValue(116); // 116=Field type
     SG.Cells[iFieldValueCol, 0] := ALangManager.GetValue(311); // 311=Value
end;

{procedure TIDB_ParamsOfPolygonOverlayForm.DisconnectAllValidators;
begin
     IntValidator.Edit := NIL;
     FloatValidator.Edit := NIL;
end;}

procedure TIDB_ParamsOfPolygonOverlayForm.FillTheGrid;
var
   iI: Integer;
   sFieldName: AnsiString;
   iFieldType, iFieldSize: Integer;
   AOverlayField: TOverlayField;
begin
     SG.RowCount := 1;
     for iI := 0 to AStructMan.GetFieldCount - 1 do
     begin
         sFieldName := AStructMan.GetFieldName(iI);
         if (AnsiUpperCase(sFieldName) <> 'PROGISID') and
            (AnsiUpperCase(sFieldName) <> 'GUID') and
            (not FieldIsInternalUsageField(sFieldName)) and
            (not FieldIsCalculatedField(sFieldName)) then
         begin
               iFieldType := AStructMan.GetFieldType(sFieldName);
               iFieldSize := AStructMan.GetFieldSize(sFieldName);
               SG.RowCount := SG.RowCount + 1;
               SG.Cells[iFieldNameCol, SG.RowCount-1] := sFieldName;
               SG.Cells[iFieldTypeCol, SG.RowCount-1] := GetTypeNameByNumber(iFieldType);
               SG.Objects[iFieldTypeCol, SG.RowCount-1] := Pointer(iFieldType);

               AOverlayField := TOverlayField.Create;
               AOverlayField.FieldName := SG.Cells[iFieldNameCol, SG.RowCount - 1];
               AOverlayField.Field1 := SG.Cells[iFieldValueCol, SG.RowCount - 1];
               AOverlayField.Calculated := false;
               AOverlayField.FieldType := iFieldType;
               AOverlayField.FieldSize := iFieldSize;
               AOverlayField.Coefficient := 1;
               AOverlayField.Precision := -1;
               SG.Objects[iFieldValueCol, SG.RowCount - 1] := AOverlayField;
         end;
     end; // For iI end
     if SG.RowCount < 2 then
     begin
        SG.RowCount := 2;
        SG.Enabled := FALSE;
     end;
     SG.FixedRows := 1;
end;

procedure TIDB_ParamsOfPolygonOverlayForm.FormShow(Sender: TObject);
begin
     SELF.Label4.Caption := GetLayerNameFunc (Self.ASourceLayer1);
     SELF.Label5.Caption := GetLayerNameFunc (Self.ASourceLayer2);
     SELF.FillTheGrid;
end;

procedure TIDB_ParamsOfPolygonOverlayForm.ShowStructurePopUpClick(Sender: TObject);
var
   SPF: TIDB_StructureAndPropertiesForm;
   iI: Integer;
   sLayerName: AnsiString;
   iCurrRow: Integer;
begin
     SPF := TIDB_StructureAndPropertiesForm.Create(Application, SELF.AProject, -1, SELF.AStructMan);
     try
        SPF.ApplyToAllCB.Enabled := FALSE;
        // Delete the first and the last apostrophe (') signs.
        sLayerName := Copy(Label6.Caption, 2, Length(Label6.Caption));
        sLayerName := Copy(sLayerName, 1, Length(sLayerName)-1);
        SPF.Caption := Format(ALangManager.GetValue(107), [sLayerName]);
        // Fill the grid.
        SPF.SG.RowCount := AStructMan.GetFieldCount+2;
        SPF.SG.ColCount := 4;
        for iI := 0 to AStructMan.GetFieldCount - 1 do
        begin
           SPF.SG.Cells[SPF.FieldNameCol, iI+1] := AStructMan.GetFieldName(iI);
           SPF.SG.Objects[SPF.FieldNameCol, iI+1] := Pointer(1); // Non-Calculated field
        end;
        SPF.SG.Objects[SPF.FieldNameCol, SPF.SG.RowCount-1] := Pointer(1);
        SPF._ReSize;

        if SPF.ShowModal = mrOK then
        begin
           iCurrRow := SG.Row;
           SELF.FillTheGrid;
           SG.Row := iCurrRow;
        end;
     finally
        SPF.Free;
     end;
end;

procedure TIDB_ParamsOfPolygonOverlayForm.PopupMenu1Popup(Sender: TObject);
var
   bValue: Boolean;
begin
     bValue := SG.InplaceEditor <> NIL;
     CopyPopUp.Enabled := bValue;
     CutPopUp.Enabled := bValue;
     PastePopUp.Enabled := bValue;
end;

procedure TIDB_ParamsOfPolygonOverlayForm.SwitchToEditMode;
begin
     SG.Options := SG.Options + [goEditing, goAlwaysShowEditor];
end;

procedure TIDB_ParamsOfPolygonOverlayForm.SwitchToBrowseMode;
begin
     SG.Options := SG.Options - [goEditing, goAlwaysShowEditor];
     if SG.InplaceEditor <> NIL then
        SG.HideEditor;
end;

procedure TIDB_ParamsOfPolygonOverlayForm.SGDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
{     if (ARow = SG.Row) and (ACol = iFieldValueCol) then
     begin
        RunBuilderBB.Height := SG.RowHeights[ARow];
        RunBuilderBB.Width := RunBuilderBB.Height;
        RunBuilderBB.Top := SG.Top + Rect.Top + 1;
        RunBuilderBB.Left := SG.ClientWidth - RunBuilderBB.Width + 1;
     end;}
end;

procedure TIDB_ParamsOfPolygonOverlayForm.SGMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
   ACol, ARow: Integer;
begin
     SG.MouseToCell(X, Y, ACol, ARow);
     SG.Row := ARow;
     SG.Col := ACol;

     if (ARow > 0) and (ACol = iFieldNameCol) then
        SELF.SwitchToEditMode
     else
        SELF.SwitchToBrowseMode;
end;

procedure TIDB_ParamsOfPolygonOverlayForm.SGMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     SwitchToBrowseMode;
end;

procedure TIDB_ParamsOfPolygonOverlayForm.RunBuilderBBClick(Sender: TObject);
var
   EBF: TIDB_ExpressionBuilderForm;
   sFieldName: AnsiString;
   iFieldType: Integer;
begin
     sFieldName := SG.Cells[iFieldNameCol, SG.Row];
     iFieldType := SELF.AStructMan.GetFieldType(sFieldName);
     EBF := TIDB_ExpressionBuilderForm.Create(SELF, sFieldName, iFieldType);
     try
        if EBF.ShowModal = mrOK then begin

           end;
     finally
        EBF.Free;
     end;
end;

procedure TIDB_ParamsOfPolygonOverlayForm.SGDragDrop(Sender, Source: TObject; X, Y: Integer);
var ACol, ARow: integer;
    PPF: TIDB_ProportionPropsForm;
    AOverlayField: TOverlayField;
    ALayer: Pointer;
    sFullFieldName: AnsiString;
begin
   if (Sender is TIDB_StringGrid) and (Source is TListBox) then
   begin
      SG.MouseToCell (X, Y, ACol, ARow);

      if Source = ListBox1 then
         ALayer := ASourceLayer1
//         sFullFieldName := GetLayerNameFunc (ASourceLayer1) + '.' + (Source as TListBox).Items.Strings[(Source as TListBox).ItemIndex]
      else
         ALayer := ASourceLayer2;
      sFullFieldName := GetLayerNameFunc (ALayer) + '.' + (Source as TListBox).Items.Strings[(Source as TListBox).ItemIndex];

      if ARow = -1 then
      begin
         SG.RowCount := SG.RowCount + 1;

         ARow := SG.RowCount - 1;
         SG.Cells[iFieldNameCol, ARow] := (Source as TListBox).Items.Strings[(Source as TListBox).ItemIndex];
         AOverlayField := TOverlayField.Create;
         AOverlayField.FieldName := (Source as TListBox).Items.Strings[(Source as TListBox).ItemIndex];
         AOverlayField.Calculated := false;
         AOverlayField.Coefficient := 1;
         AOverlayField.Precision := -1;
         SG.Objects[iFieldValueCol, ARow] := AOverlayField;
      end
      else
      begin
         AOverlayField := TOverlayField (SG.Objects[iFieldValueCol, ARow]);
      end;

      if (AOverlayField.Field1 = '') or (FieldHasStringType (ALayer, (Source as TListBox).Items.Strings[(Source as TListBox).ItemIndex])) then
      begin
         SG.Cells[iFieldValueCol, ARow] := sFullFieldName;
         AOverlayField.Field1 := sFullFieldName;
         SetFieldTypeAndSizeByFullName (sFullFieldName, AOverlayField);
      end
      else
      begin
         AOverlayField.Field2 := sFullFieldName;
         PPF := TIDB_ProportionPropsForm.Create (AOverlayField);
         if PPF.ShowModal = mrOK then
         begin
            if AOverlayField.Calculated then
            begin
               AOverlayField.FieldType := 6;
               AOverlayField.FieldSize := 8;
               if AOverlayField.Coefficient = 1 then
                  SG.Cells[iFieldValueCol, ARow] := AOverlayField.Field1 + ' / ' + AOverlayField.Field2
               else
                  SG.Cells[iFieldValueCol, ARow] := FloatToStr (AOverlayField.Coefficient) + ' * ' + AOverlayField.Field1 + ' / ' + AOverlayField.Field2;
            end
            else
            begin
               SetFieldTypeAndSizeByFullName (sFullFieldName, AOverlayField);
               SG.Cells[iFieldValueCol, ARow] := AOverlayField.Field1;
            end;
         end;
      end;
      SG.Cells[iFieldTypeCol, ARow] := GetTypeNameByNumber(AOverlayField.FieldType);
   end;

end;

procedure TIDB_ParamsOfPolygonOverlayForm.SGDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
   Accept := Source is TListBox;
end;


procedure TIDB_ParamsOfPolygonOverlayForm.SGSetEditText(Sender: TObject;
  ACol, ARow: Integer; const Value: String);
begin
   TOverlayField (SG.Objects[iFieldValueCol, ARow]).FieldName := SG.Cells[iFieldNameCol, ARow];
end;

procedure TIDB_ParamsOfPolygonOverlayForm.SetFieldTypeAndSizeByFullName (sFullFieldName: AnsiString; AOverlayField: TOverlayField);
var BStructMan: TIDB_StructMan;
    ADataAccessItem: TIDB_Access2DB_Level2;
    sLayerName: AnsiString;
    sFieldName: AnsiString;
begin
   ADataAccessItem := AnIDBMainMan.GetProjectLayerManager (AProject).DataAccessObject;
   BStructMan := TIDB_StructMan.Create (ADataAccessItem);

   sLayerName := Copy (sFullFieldName, 1, Pos ('.', sFullFieldName) - 1);
   sFieldName := Copy (sFullFieldName, Pos ('.', sFullFieldName) + 1, Length (sFullFieldName) - Pos ('.', sFullFieldName));
   if sLayerName = GetLayerNameFunc (Self.ASourceLayer1) then
      BStructMan.DetectStructure (ADataAccessItem.DMaster, GetLayerIndexFunc (ASourceLayer1))
   else
      if sLayerName = GetLayerNameFunc (Self.ASourceLayer2) then
      begin
         BStructMan.DetectStructure (ADataAccessItem.DMaster, GetLayerIndexFunc (ASourceLayer2));
      end
      else
         exit;
   AOverlayField.FieldType := BStructMan.GetFieldType (sFieldName);
   AOverlayField.FieldSize := BStructMan.GetFieldSize (sFieldName);

   BStructMan.Free;
end;

function TIDB_ParamsOfPolygonOverlayForm.FieldHasStringType (ALayer: Pointer; sFieldName: AnsiString): boolean;
var BStructMan: TIDB_StructMan;
    ADataAccessItem: TIDB_Access2DB_Level2;
    iFieldType: integer;
begin
   ADataAccessItem := AnIDBMainMan.GetProjectLayerManager (AProject).DataAccessObject;
   BStructMan := TIDB_StructMan.Create (ADataAccessItem);

   BStructMan.DetectStructure (ADataAccessItem.DMaster, GetLayerIndexFunc (ALayer));
   iFieldType := BStructMan.GetFieldType (sFieldName);

   case iFieldType of
      2, 3, 4, 6, 12:
         RESULT := false
      else
         RESULT := true;
   end;
   BStructMan.Free;
end;

end.
