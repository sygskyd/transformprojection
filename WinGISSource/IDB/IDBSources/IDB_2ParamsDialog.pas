unit IDB_2ParamsDialog;
{
Internal database. Ver II.
Some of parameters that must be selected by the user and cannot be selected
with help of standard Windows' dialogs.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 02-02-2001
}
interface

uses
  Windows, Messages, Classes, Graphics, Controls, Forms, 
  ExtCtrls, StdCtrls;

type
  TIDB_2ParamsDialogForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    OKButton: TButton;
    RadioButton2: TRadioButton;
    RadioButton1: TRadioButton;
    Label1: TLabel;
    CancelButton: TButton;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation
{$R *.DFM}

Uses SysUtils,
     IDB_MainManager, IDB_LanguageManager, IDB_Consts;

procedure TIDB_2ParamsDialogForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     If SELF.ModalResult = mrCancel Then
        // 383=If you will select OK, the Internal database will be switched off for this project. Do you want to continue?
        If MessageBox(AWinGISMainForm.Handle, PChar(ALangManager.GetValue(383)), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrNO Then Begin
           CanClose := FALSE;
           SELF.Show;
           End;
end;

procedure TIDB_2ParamsDialogForm.FormCreate(Sender: TObject);
begin
     ALangManager.MakeReplacement(SELF);
end;

end.
