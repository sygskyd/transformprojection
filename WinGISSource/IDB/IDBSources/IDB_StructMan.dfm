’
 TIDB_STRUCTMANFORM 0c  TPF0TIDB_StructManFormIDB_StructManFormLeft  TopEBorderStylebsDialogCaption!112ClientHeight¢ClientWidthxColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
KeyPreview	OldCreateOrderPositionpoDesktopCenterScaledOnCreate
FormCreate	OnDestroyFormDestroyOnKeyUp	FormKeyUpOnShowFormShowPixelsPerInchx
TextHeight TPanelPanel1Left TopWidthxHeight#AlignalBottom
BevelOuterbvNoneTabOrder  TBitBtnCancelBBLeft&TopWidthKHeightAnchorsakRightakBottom Cancel	Caption!50Default	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style ModalResult
ParentFontTabOrder 	NumGlyphs  TBitBtnOKBBLeftÖTopWidthKHeightAnchorsakRightakBottom Caption!49Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style ModalResult
ParentFontTabOrderOnClick	OKBBClick	NumGlyphs   TPageControlPageControl1Left Top WidthyHeight
ActivePageFieldsDefsTabSheetFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style HotTrack	
ParentFontTabOrder 	TTabSheetFieldsDefsTabSheetCaption!330Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
ParentFont TPanelPanel3Leftš Top#WidthHeight@
BevelInner	bvLoweredFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TLabel	TypeLabelLeftÓ Top
WidthHeight	AlignmenttaRightJustifyCaption!116Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabel
Size1LabelLeftÓ Top"WidthHeight	AlignmenttaRightJustifyCaption!117Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelIndexedLabelLeftÓ Top<WidthHeight	AlignmenttaRightJustifyCaption!118Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TComboBoxTypeComboBoxTagLeftš TopWidth HeightStylecsDropDownListFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
ItemHeightItems.StringsStringByteInteger (2 bytes)Single (4 bytes)Long (4 bytes)Double (8 bytes)DateTimeCurrencyBoolean 
ParentFontTabOrder OnChangeTypeComboBoxChange  	TComboBoxIndexedComboBoxTagLeftš Top7Width HeightStylecsDropDownListFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
ItemHeightItems.Strings!125!126 
ParentFontTabOrderOnChangeIndexedComboBoxChange  TEdit	Size1EditTagLeftš TopWidth HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderText	Size1EditOnChangeSize1EditChange
OnKeyPressSize1EditKeyPress   TPanelPanel4Left Top WidthqHeightAlignalTop
BevelInner	bvLoweredParentShowHintShowHint	TabOrder TSpeedButton
AddFieldSBLeftTopWidthHeightHint!113Flat	
Glyph.Data
ź   ę   BMę       v   (               p                                    ĄĄĄ    ’  ’   ’’ ’   ’ ’ ’’  ’’’ 3333333 3333333 3333333 330 333 330 333 330 333 3    3 3
ŖŖŖ3 3    3 330 333 330 333 330 333 3333333 3333333 OnClickAddFieldSBClick  TSpeedButtonDeleteFieldSBTagLeftTopWidthHeightHint!114Flat	
Glyph.Data
ź   ę   BMę       v   (               p                                    ĄĄĄ    ’  ’   ’’ ’   ’ ’ ’’  ’’’ 3333333 3333333 3333333 3333333 3333333 3333333 3     3 3	3 3     3 3333333 3333333 3333333 3333333 3333333 OnClickDeleteFieldSBClick  TSpeedButtonChangeNameSBTagLeft7TopWidthHeightHint!115Flat	
Glyph.Data
ś   ö   BMö       v   (                                                   ĄĄĄ    ’  ’   ’’ ’   ’ ’ ’’  ’’’ 333     333’’’š 0  ’ šąūū’’šąææ ’ššąūūūšššąææ  šąūūūūš’šąæ   ’š ū °’ š30 ’’š330°’’  33šš30°’’3	’’ 3303   3OnClickChangeNameSBClick   TListBoxFieldsListBoxLeft Top#Widthģ HeightPFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
ItemHeightMultiSelect	
ParentFont	PopupMenu
PopupMenu1TabOrderOnClickFieldsListBoxClick
OnDblClickFieldsListBoxDblClick   	TTabSheetFieldsVisibilityTabSheetCaption!331Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
ImageIndex
ParentFontOnShowFieldsVisibilityTabSheetShow TSpeedButtonUpSBLeft,Top¾ WidthHeightFlat	
Glyph.Data
z  v  BMv      v   (                                                    ĄĄĄ    ’  ’   ’’ ’   ’ ’ ’’  ’’’ 333 333333333333	333333333333	333333333333	333333333333	333333333333	33333’’ó330 	  3338ó3303338?3383333	3333333333	3333ó333330333338ó8ó33330333338?8333333	333333333333	333333333333033333338ó3333330333333383333	NumGlyphsParentShowHintShowHint	OnClick	UpSBClick  TSpeedButtonDownSBLeft,Top× WidthHeightFlat	
Glyph.Data
z  v  BMv      v   (                                                    ĄĄĄ    ’  ’   ’’ ’   ’ ’ ’’  ’’’ 333033333338ó333333033333338?333333	333333333333	333333ó33330333338ó8ó3333033333838?3333	3333333333	333333ó3303338’’?ųó330 	  333833333	333333333333	333333333333	333333333333	333333333333	333333333333 333333333	NumGlyphsParentShowHintShowHint	OnClickDownSBClick  TSpeedButtonSpeedButton1Left,Top7WidthHeightCaption>Flat	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontOnClickSpeedButton1Click  TSpeedButtonSpeedButton2Left,TopPWidthHeightCaption>>Flat	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontOnClickSpeedButton2Click  TSpeedButtonSpeedButton3Left,TopiWidthHeightCaption<Flat	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontOnClickSpeedButton3Click  TSpeedButtonSpeedButton4Left,Top WidthHeightCaption<<Flat	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontOnClickSpeedButton4Click  TLabelLabel1Left
TopWidth
Height	AlignmenttaCenterAutoSizeCaption!132Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel2LeftTTopWidth
Height	AlignmenttaCenterAutoSizeCaption!133Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
ParentFont  TListBoxVisibleFieldsListBoxLeft
Top-Width
Height#
ItemHeightTabOrder 
OnDblClickSpeedButton1Click  TListBoxInvisibleFieldsListBoxLeftYTop(Width
Height#
ItemHeightSorted	TabOrder
OnDblClickSpeedButton3Click    
TPopupMenu
PopupMenu1Leftž TopÓ  	TMenuItemAddNewFieldPopUpCaption!113OnClickAddFieldSBClick  	TMenuItemDeleteFieldPopUpCaption!114OnClickDeleteFieldSBClick  	TMenuItemChangeFieldNamePopUpCaption!115OnClickChangeNameSBClick    