unit IDB_Monitor1;
{
Internal database. Ver. II.
This module provides the functionality of monitoring of 1 object of WinGIS.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 25-10-2001
}
interface

uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
   IDB_C_Split, ExtCtrls, StdCtrls, Menus,
   DMaster, Db, DDB, DTables, Buttons,
   IDB_LayerManager, IDB_Messages, {IDB_ExternalTablesMan,} IDB_Access2DB;

type
   TSelItemInfo = record
      iLayerIndex,
         iItemID: Integer;
   end;

   PSelItemInfo = ^TSelItemInfo;

   TIDB_Monitor1Form = class (TForm)
      AQuery: TDQuery;
      DataSource1: TDataSource;
      TopPanel: TPanel;
      ProjectNameLabel: TLabel;
      PopupMenu1: TPopupMenu;
      FieldsStatesPopUp: TMenuItem;
      DataScrollBox: TScrollBox;
      DataPanel: TPanel;
      CalcDataScrollBox: TScrollBox;
      CalcDataPanel: TPanel;
      CalcData_Splitter: TIDB_Splitter;
      Data_Splitter: TIDB_Splitter;
      Bevel1: TBevel;
      Bevel2: TBevel;
      Panel1: TPanel;
      Bevel3: TBevel;
      BackwardSB: TSpeedButton;
      ForwardSB: TSpeedButton;
      procedure FormCreate (Sender: TObject);
      procedure FormClose (Sender: TObject; var Action: TCloseAction);
      procedure FieldsStatesPopUpClick (Sender: TObject);
      procedure FormResize (Sender: TObject);
      procedure CalcData_SplitterPosChanged (Sender: TObject);
      procedure Data_SplitterPosChanged (Sender: TObject);
      procedure FormShow (Sender: TObject);
      procedure ForwardSBClick (Sender: TObject);
      procedure BackwardSBClick (Sender: TObject);
    procedure FormDestroy(Sender: TObject);
   private
      { Private declarations }
      AProject: Pointer;
      ALayerManager: TIDB_LayerManager;
      iLastLayerIndex,
         iLastItemID: Integer;

      FItemNumber: integer;

      Data: TList;

      sOriginalProjectName: AnsiString;

      procedure DeleteAllDataLabels;
      procedure RefreshDataWindow (var AMsg: TMessage); message WM_IDB_Message;
      procedure ApplyFieldVisibilityChanges;

      procedure MoveLabels;
      procedure MakeProjectLabel;

      procedure SwitchPopUpMenu (bPopupOn: Boolean);
      function ChangeFieldsStates: boolean;
   public
      { Public declarations }
      constructor Create (AOwner: TComponent);
      procedure Init (AProject: Pointer; ALayerManager: TIDB_LayerManager; AMaster: TDMaster);
      procedure ClearData;
      procedure ShowData (iItemNumber: integer);
      procedure ClearItemsForMonitoring;
      procedure AddLayerItem (iLayerIndex, iLayerItemID: Integer; ADataAccessObject: TIDB_Access2DB_Level2);
      procedure SetButtons (bValue: boolean);
      property Project: Pointer read AProject;
   end;

implementation

{$R *.DFM}

uses
   IDB_LanguageManager, IDB_FieldsVisibilityMan, IDB_MainManager, IDB_Consts,
   IDB_Utils, IDB_CallbacksDef, IDB_StructureAndProperties, IDB_StructMan, IDB_View,
   IDB_DescMan;

// ++ Cadmensky Version 2.3.2
constructor TIDB_Monitor1Form.Create (AOwner: TComponent);
begin
   inherited Create (AOwner);
   SELF.Data := TList.Create;
end;
// -- Cadmensky Version 2.3.2

procedure TIDB_Monitor1Form.Init (AProject: Pointer; ALayerManager: TIDB_LayerManager; AMaster: TDMaster);
begin
   SELF.AProject := AProject;
   SELF.ALayerManager := ALayerManager;
   AQuery.Master := AMaster;
   sOriginalProjectName := GetProjectFullFileNameFunc (AProject);
   SELF.MakeProjectLabel;
   FItemNumber := 0;
end;

procedure TIDB_Monitor1Form.ClearData;
begin
   SELF.SwitchPopUpMenu (FALSE);
   SELF.AQuery.Close;
   SELF.AQuery.Master := nil;
   SELF.ALayerManager := nil;
   SELF.AProject := nil;
   SELF.DeleteAllDataLabels;
   SELF.Caption := ALangManager.GetValue (375);
   // 376=Project
   SELF.ProjectNameLabel.Caption := ALangManager.GetValue (376);
end;

procedure TIDB_Monitor1Form.DeleteAllDataLabels;
var
   iI: Integer;
   AComponent: TComponent;
begin
   iI := 0;
   while iI < SELF.ComponentCount do
   begin
      AComponent := SELF.Components[iI];
      if AComponent.Tag > 0 then
      begin
         if AComponent is TLabel then
         begin
            AComponent.Free;
            CONTINUE;
         end;
      end;
      Inc (iI);
   end;
end;

procedure TIDB_Monitor1Form.FormCreate (Sender: TObject);
begin
   ALangManager.MakeReplacement (SELF);
end;

procedure TIDB_Monitor1Form.ShowData (iItemNumber: integer);
var
   AList: TStringList;
   iI, iPC, iPD: Integer;
   sTableName,
      sFieldName,
      sUFN,
      sFieldNames,
      sItemID,
      sData: AnsiString;
   ADescMan: TIDB_DescMan;
   ALayer: Pointer;
   ALabel: TLabel;
   AField: TField;

   //AnExtTablesMan: TIDB_ExternalTablesMan;

   pLI: PSelItemInfo;
   iLayerIndex, iItemID: integer;

   sAttTableName: AnsiString;
   slCalcFields: TStringList;

begin
// ++ Cadmensky
   SetButtons (false);
// -- Cadmensky

   pLI := Self.Data[FItemNumber];
   iLayerIndex := pLI.iLayerIndex;
   iItemID := pLI.iItemID;
   ALayer := GetLayerPointerByIndexFunc (SELF.AProject, iLayerIndex);

   try
      sItemID := IntToStr (iItemID);
   except
      EXIT;
   end;
   AQuery.Close;

   SELF.iLastLayerIndex := iLayerIndex;
   SELF.iLastItemID := iItemID;

   SELF.DeleteAllDataLabels;

   if ALayer <> nil then
   begin
      SELF.SwitchPopUpMenu (TRUE);
      ADescMan := ALayerManager.GetLayerDescMan (ALayer);

      AList := TStringList.Create;
      AList.Text := ADescMan.GetInfoFieldNames;

// ++ Cadmensky
      if AList.Count < 1 then
      begin
         SELF.Hide;
         Application.MessageBox (PChar (ALangManager.GetValue (463)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION);
         if SELF.ChangeFieldsStates then
         begin
            AList.Text := ADescMan.GetInfoFieldNames;
            if AList.Count < 1 then
            begin
               AList.Free;
               SELF.Close;
               exit;
            end;
         end
         else
         begin
            AList.Free;
            SELF.Close;
            exit;
         end;
      end;

      slCalcFields := TStringList.Create;
      slCalcFields.Text := SELF.ALayerManager.GetUsedCalcFieldsNames (ALayer);
// -- Cadmensky

      {AnExtTablesMan := Self.ALayerManager.GetLayerExtTablesMan (ALayer);
      if AnExtTablesMan.IAmInUse then
      begin
         AnExtTablesMan.SetParamsToExtTablePool;

         if not AnIDBMainMan.UseNewATTNames (AProject) then
            sAttTableName := cs_AttributeTableNamePrefix + IntToStr (iLayerIndex)
         else
            sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (SELF.AProject, ALayer);

         //AQuery.SQL.Text := AnExtTablesMan.JoiningSQL + ' AND [' + sAttTableName + '].[PROGISID] = ' + sItemID;

         try
            AQuery.Open;
         except
         end;
         if AQuery.RecordCount < 1 then
         begin
            if not AnIDBMainMan.UseNewATTNames (SELF.AProject) then
               sTableName := cs_AttributeTableNamePrefix + IntToStr (iLayerIndex)
            else
               sTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (SELF.AProject, ALayer);

            sFieldNames := '';
            for iI := 0 to AList.Count - 1 do
            begin
               sFieldName := AList[iI];
// ++ Cadmensky Version 2.3.2
               if (sFieldName <> '') and
// -- Cadmensky Version 2.3.2
               not FieldIsLinked (sFieldName) and
                  (not FieldIsCalculatedField (sFieldName) or
                  (slCalcFields.IndexOf (sFieldName) <> -1)) then
                  sFieldNames := sFieldNames + '[' + sFieldName + '], ';
            end; // for iI end

            sFieldNames := Copy (sFieldNames, 1, Length (sFieldNames) - 2);
// ++ Cadmensky Version 2.3.2
            if sFieldNames = '' then
               sFieldNames := '[PROGISID]';
// -- Cadmensky Version 2.3.2
            AQuery.SQL.Text := 'SELECT ' + sFieldNames + ' FROM [' + sTableName + '] ' +
               'WHERE PROGISID = ' + sItemID;
         end;
      end
      else}
      begin

         if not AnIDBMainMan.UseNewATTNames (SELF.AProject) then
            sTableName := cs_AttributeTableNamePrefix + IntToStr (iLayerIndex)
         else
            sTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (SELF.AProject, ALayer);

         sFieldNames := '';
         for iI := 0 to AList.Count - 1 do
         begin
            sFieldName := AList[iI];
// ++ Cadmensky Version 2.3.2
            if (sFieldName <> '') and
// -- Cadmensky Version 2.3.2
            (not FieldIsCalculatedField (sFieldName) or (slCalcFields.IndexOf (sFieldName) <> -1)) then
               sFieldNames := sFieldNames + '[' + sFieldName + '], ';
         end; // for iI end

         sFieldNames := Copy (sFieldNames, 1, Length (sFieldNames) - 2);
// ++ Cadmensky Version 2.3.2
         if sFieldNames = '' then
            sFieldNames := '[PROGISID]';
// -- Cadmensky Version 2.3.2
         AQuery.SQL.Text := 'SELECT ' + sFieldNames + ' FROM [' + sTableName + '] ' +
            'WHERE PROGISID = ' + sItemID;
      end; 
      try
         AQuery.Open;
         try
            // Create new components for showing attribute data.
            iPD := 0;
            iPC := 0;
            for iI := 0 to AList.Count - 1 do
            begin
               sFieldName := AList[iI];
               sUFN := AnsiUpperCase (sFieldName);
               sData := '';
               if (not FieldIsCalculatedField (sFieldName)) and (sUFN <> 'PROGISID') and (sUFN <> 'GUID') then
               begin
                  AField := AQuery.FindField (sFieldName);
                  if AField <> nil then
                     sData := AField.AsString
// ++ Cadmensky Version 2.3.2
                  else
                     Continue;
// -- Cadmensky Version 2.3.2
                  // FieldName
                  ALabel := TLabel.Create (SELF);
                  ALabel.Tag := 1;
                  ALabel.Parent := DataPanel;
                  ALabel.Alignment := taRightJustify;
                  ALabel.Left := DataPanel.ClientWidth - ALabel.Width - 5;
                  ALabel.Caption := sFieldName;
                  ALabel.Top := (iPD * (ALabel.Height + 1));
                  // FieldData
                  ALabel := TLabel.Create (SELF);
                  ALabel.Tag := 2;
                  ALabel.Parent := DataScrollBox;
                  ALabel.Caption := sData;
                  ALabel.Left := DataPanel.Width + 5;
                  ALabel.Top := (iPD * (ALabel.Height + 1));
                  Inc (iPD);
               end
               else
               begin
                  if sUFN = 'PROGISID' then
                     sData := sItemID
                  else
                  begin
                     AField := AQuery.FindField (sFieldName);
                     if AField <> nil then
                        sData := AField.AsString
                     else
                        sData := GetCalculatedFieldValueByName (SELF.AProject, ALayer, iItemID, sFieldName);
                  end;
                  if sData <> '' then
                  begin
                     // FieldName
                     ALabel := TLabel.Create (SELF);
                     ALabel.Tag := 1;
                     ALabel.Parent := CalcDataPanel;
                     ALabel.Alignment := taRightJustify;
                     ALabel.Left := DataPanel.ClientWidth - ALabel.Width - 5;
                     if (sUFN <> 'PROGISID') and (sUFN <> 'GUID') then
                        sFieldName := Copy (sFieldName, 2, Length (sFieldName));
                     ALabel.Caption := sFieldName;
                     ALabel.Top := (iPC * (ALabel.Height + 1));
                     // FieldData
                     ALabel := TLabel.Create (SELF);
                     ALabel.Tag := 2;
                     ALabel.Parent := CalcDataScrollBox;
                     ALabel.Caption := sData;
                     ALabel.Left := CalcDataPanel.Width + 5;
                     ALabel.Top := (iPC * (ALabel.Height + 1));
                     Inc (iPC);
                  end;
               end;
            end; // for iI end
            CalcDataScrollBox.Height := (iPC * (ALabel.Height + 1)) + 1;

         finally
            Self.Caption := ALangManager.GetValue (375) + ' - ' + Format (ALangManager.GetValue (379), [GetLayerNameFunc (ALayer)]);
            DeselectAllObjectsProc (Self.AProject);
            ShowSelectedItemsInProjectProc (SELF.AProject, ALayer, iItemID, true, false);
// ++ Cadmensky
            if AnIDBMainMan.AViewForm <> nil then
            begin
               if (GetLayerIndexFunc (AnIDBMainMan.AViewForm.ALayer) <> iLayerIndex) or
                  (AnIDBMainMan.AViewForm.AProject <> AProject) then
                  Self.ALayerManager.ShowAttributeWindow (ALayer, nil)
               else
                  if AnIDBMainMan.AViewForm <> nil then
                     AnIDBMainMan.AViewForm.ShowRecordsForSelectedObjects;
            end
            else
               AnIDBMainMan.ShowRecordsForSelectedObjects (SELF.AProject);
// -- Cadmensky
            AQuery.Close;
            Application.ProcessMessages;
         end;
      except
// ++ Cadmensky
         AList.Free;
         SELF.Close;
// -- Cadmensky
         EXIT;
      end;
// ++ Cadmensky Version 2.3.2
      slCalcFields.Free;
// -- Cadmensky Version 2.3.2
      AList.Clear;
// ++ Cadmensky
      AList.Free;
      SELF.Show;
      SetButtons (true);
// -- Cadmensky
   end;
end;

procedure TIDB_Monitor1Form.FormClose (Sender: TObject; var Action: TCloseAction);
//var
//   i: integer;
begin
   AnIDBMainMan.DetectInfoFormState (SELF);
   ReverseUsingIDBInfoProc;

// ++ Cadmensky
//   for i := 0 to SELF.Data.Count - 1 do
//      Dispose (SELF.Data[i]);
// -- Cadmensky

//   Self.Data.Free;
   Action := caFree;
end;

function TIDB_Monitor1Form.ChangeFieldsStates: boolean;
var
   ALayer: Pointer;
   iT, iL, iW, iH: Integer;
begin
   RESULT := false;
   ALayer := GetLayerPointerByIndexFunc (SELF.AProject, SELF.iLastLayerIndex);
   if ALayer <> nil then
   begin
      iT := SELF.Top;
      iL := SELF.Left;
      iW := SELF.Width;
      iH := SELF.Height;
      SELF.Hide;
      try
         RESULT := ALayerManager.ShowFieldVisibility (ALayer, iT, iL, iW, iH);
{         if ALayerManager.ShowFieldVisibility (ALayer, iT, iL, iW, iH) then
            SELF.ShowData (FItemNumber);}
      finally
{         SELF.Show;}
      end;
      SELF.Top := iT;
      SELF.Left := iL;
      SELF.Width := iW;
      SELF.Height := iH;
   end;
end;

procedure TIDB_Monitor1Form.RefreshDataWindow (var AMsg: TMessage);
begin
   case AMsg.WParam of
      IDB_RefreshDataWindow:
         case AMsg.LParam of
            1: SELF.ApplyFieldVisibilityChanges;
         end;
   end; // case end
end;

procedure TIDB_Monitor1Form.ApplyFieldVisibilityChanges;
begin
   SELF.ShowData (FItemNumber);
end;

procedure TIDB_Monitor1Form.FormResize (Sender: TObject);
begin
   ProjectNameLabel.Width := SELF.ClientWidth - ProjectNameLabel.Left - 5;
   SELF.MakeProjectLabel;
end;

procedure TIDB_Monitor1Form.MoveLabels;
var
   iI: Integer;
   ALabel: TLabel;
begin
   for iI := 0 to SELF.ComponentCount - 1 do
      if SELF.Components[iI] is TLabel then
      begin
         ALabel := TLabel (SELF.Components[iI]);
         if ALabel.Tag = 1 then
            ALabel.Left := DataPanel.ClientWidth - ALabel.Width - 5
         else
            if ALabel.Tag = 2 then
               ALabel.Left := DataPanel.Width + 5;
      end;
end;

procedure TIDB_Monitor1Form.CalcData_SplitterPosChanged (Sender: TObject);
begin
   DataPanel.Width := CalcDataPanel.Width;
   MoveLabels;
end;

procedure TIDB_Monitor1Form.Data_SplitterPosChanged (Sender: TObject);
begin
   CalcDataPanel.Width := DataPanel.Width;
   MoveLabels;
end;

procedure TIDB_Monitor1Form.FormShow (Sender: TObject);
begin
   SELF.FormResize (SENDER);
end;

procedure TIDB_Monitor1Form.MakeProjectLabel;
var
   iP: Integer;
   sResult: AnsiString;
begin
   sResult := SELF.sOriginalProjectName;
   iP := 4;
   while SELF.Canvas.TextWidth (sResult) > ProjectNameLabel.Width do
   begin
      sResult := Copy (sOriginalProjectName, 1, 3) + '...' + Copy (sOriginalProjectName, iP, Length (sOriginalProjectName));
      Inc (iP);
   end;
   ProjectNameLabel.Caption := sResult;
end;

procedure TIDB_Monitor1Form.SwitchPopUpMenu (bPopupOn: Boolean);
begin
   if bPopupOn then
   begin
      TopPanel.PopupMenu := PopupMenu1;
      CalcDataPanel.PopupMenu := PopupMenu1;
      CalcDataScrollBox.PopupMenu := PopupMenu1;
      DataPanel.PopupMenu := PopupMenu1;
      DataScrollBox.PopupMenu := PopupMenu1;
   end
   else
   begin
      TopPanel.PopupMenu := nil;
      CalcDataPanel.PopupMenu := nil;
      CalcDataScrollBox.PopupMenu := nil;
      DataPanel.PopupMenu := nil;
      DataScrollBox.PopupMenu := nil;
   end;
end;

procedure TIDB_Monitor1Form.ForwardSBClick (Sender: TObject);
//var
//   iLayerIndex, iItemID: Integer;
//   pLI: PSelItemInfo;
begin
   if (FItemNumber >= 0) and (FItemNumber < Self.Data.Count - 1) then
   begin
      Inc (FItemNumber);
//      pLI := Self.Data[FItemNumber];
//      iLayerIndex := pLI.iLayerIndex;
//      iItemID := pLI.iItemID;
      ShowData (FItemNumber);
   end;
end;

procedure TIDB_Monitor1Form.BackwardSBClick (Sender: TObject);
//var
//   iLayerIndex, iItemID: Integer;
//   pLI: PSelItemInfo;
begin
   if (FItemNumber > 0) and (FItemNumber < Self.Data.Count) then
   begin
      Dec (FItemNumber);
//      pLI := Self.Data[FItemNumber];
//      iLayerIndex := pLI.iLayerIndex;
//      iItemID := pLI.iItemID;
      ShowData (FItemNumber);
   end;
end;

{Add new layer's item in objects for monitoring.}

procedure TIDB_Monitor1Form.AddLayerItem (iLayerIndex: Integer; iLayerItemID: Integer; ADataAccessObject: TIDB_Access2DB_Level2);
var
   pLI: PSelItemInfo;
begin
   new (pLI);
   pLI.iLayerIndex := iLayerIndex;
   pLI.iItemID := iLayerItemID;
   SELF.Data.Add (pLI);
end;

{ Clear array with items of layers of project. }

procedure TIDB_Monitor1Form.ClearItemsForMonitoring;
var
   iI: integer;
begin
// ++ Cadmensky
   for iI := 0 to SELF.Data.Count - 1 do
      Dispose (PSelItemInfo (SELF.Data[iI]));
// -- Cadmensky
   SELF.Data.Clear;
end;

procedure TIDB_Monitor1Form.FieldsStatesPopUpClick (Sender: TObject);
begin
   if SELF.ChangeFieldsStates then
      SELF.ShowData (SELF.FItemNumber)
   else
      SELF.Show;
end;

procedure TIDB_Monitor1Form.SetButtons (bValue: boolean);
begin
   if bValue then
   begin
      Panel1.Visible := SELF.Data.Count > 1;
{      ForwardSB.Visible := SELF.Data.Count > 1;
      BackwardSB.Visible := ForwardSB.Visible;}
      BackwardSB.Enabled := (FItemNumber > 0);
      ForwardSB.Enabled := (FItemNumber < Self.Data.Count - 1);
   end
   else
   begin
      BackwardSB.Enabled := false;
      ForwardSB.Enabled := false;
   end;
end;

// ++ Cadmensky Version 2.3.2
procedure TIDB_Monitor1Form.FormDestroy(Sender: TObject);
begin
   SELF.ClearItemsForMonitoring;
   SELF.Data.Clear;
   SELF.Data.Free;
end;
// -- Cadmensky Version 2.3.2

end.

