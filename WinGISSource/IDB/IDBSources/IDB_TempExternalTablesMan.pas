unit IDB_TempExternalTablesMan;

interface

uses Classes,
   IDB_ExternalTablesMan, IDB_Access2DB;

type
   TIDB_TempExtTablesMan = class
   private
      TempExtTablesMansList: TList;
      AProject: Pointer;
      IDA: TIDB_Access2DB_Level2;
   public
      constructor Create (AProject: Pointer; MainIDA: TIDB_Access2DB_Level2);
      destructor Free;

      function AddTempExtTablesMan (ALayer: Pointer): TIDB_BaseExternalTablesMan;
      function GetTempExtTablesMan (ALayer: Pointer): TIDB_BaseExternalTablesMan;
      function GetMansCount: Integer;

      procedure Assign (ASource, ADest: TIDB_BaseExternalTablesMan);
      procedure DropModifiedSign;
   end;

implementation

constructor TIDB_TempExtTablesMan.Create (AProject: Pointer; MainIDA: TIDB_Access2DB_Level2);
begin
   SELF.TempExtTablesMansList := TList.Create;
   SELF.AProject := AProject;
   SELF.IDA := MainIDA;
end;

destructor TIDB_TempExtTablesMan.Free;
var
   iI: Integer;
   ATempMan: TIDB_BaseExternalTablesMan;
begin
   for iI := 0 to TempExtTablesMansList.Count - 1 do
   begin
      ATempMan := TempExtTablesMansList[iI];
      ATempMan.Free;
   end;
   TempExtTablesMansList.Clear;
   TempExtTablesMansList.Free;
end;

function TIDB_TempExtTablesMan.AddTempExtTablesMan (ALayer: Pointer): TIDB_BaseExternalTablesMan;
begin
   RESULT := TIDB_BaseExternalTablesMan.Create (SELF.AProject, ALayer, SELF.IDA);
   SELF.TempExtTablesMansList.Add (RESULT);
end;

function TIDB_TempExtTablesMan.GetTempExtTablesMan (ALayer: Pointer): TIDB_BaseExternalTablesMan;
var
   iI: Integer;
   TempExtTablesMan: TIDB_BaseExternalTablesMan;
begin
   RESULT := nil;
   for iI := 0 to TempExtTablesMansList.Count - 1 do
   begin
      TempExtTablesMan := TempExtTablesMansList[iI];
      if TempExtTablesMan.Layer = ALayer then
      begin
         RESULT := TempExtTablesMan;
         BREAK;
      end;
   end; // For iI end
end;

procedure TIDB_TempExtTablesMan.Assign (ASource, ADest: TIDB_BaseExternalTablesMan);
var
   iI: Integer;
   sLinkName: AnsiString;
// ++ Commented by Cadmensky Version 2.3.4
//   slOrder: TStringList;
// -- Commented by Cadmensky Version 2.3.4
begin
// ++ Commented by Cadmensky Version 2.3.4
//   slOrder := TStringList.Create;
// -- Commented by Cadmensky Version 2.3.4
   ADest.ClearData;
   for iI := 0 to ASource.GetLinksCount - 1 do
   begin
      sLinkName := ASource.GetLinkName (iI);
// ++ Cadmensky Version 2.3.4
      if sLinkName = '' then
         Continue;
// -- Cadmensky Version 2.3.4
      sLinkName := ADest.AddLink (sLinkName);
// ++ Commented by Cadmensky Version 2.3.4
//      slOrder.Add (sLinkName);
// -- Commented by Cadmensky Version 2.3.4
      ADest.SetLinkType (sLinkName, ASource.GetLinkType (sLinkName));
      ADest.SetLinkDatabaseFileName (sLinkName, ASource.GetLinkDatabaseFileName (sLinkName));
      ADest.SetLinkUserName (sLinkName, ASource.GetLinkUserName (sLinkName));
      ADest.SetLinkPassword (sLinkName, ASource.GetLinkPassword (sLinkName));
      ADest.SetLinkTableName (sLinkName, ASource.GetLinkTableName (sLinkName));
// ++ Cadmensky Version 2.3.4
      ADest.SetLinkServerName (sLinkName, ASource.GetLinkServerName (sLinkName));
// -- Cadmensky Version 2.3.4
      ADest.SetLinkCodePage (sLinkName, ASource.GetLinkCodePage (sLinkName));
      ADest.SetLinkKeepActive (sLinkName, ASource.GetLinkKeepActive (sLinkName));
      ADest.SetLinkProgisIDFieldName (sLinkName, ASource.GetLinkProgisIDFieldName (sLinkName));
      ADest.SetLinkSelectedFields (sLinkName, ASource.GetLinkSelectedFields (sLinkName));
      ADest.SetLinkIsUsedState (sLinkName, ASource.GetLinkIsUsedState (sLinkName));
   end;
// ++ Commented by Cadmensky Version 2.3.4
//   ADest.SetLinksOrder (slOrder.Text);
//   slOrder.Free; // Cadmensky
// -- Commented by Cadmensky Version 2.3.4
end;

function TIDB_TempExtTablesMan.GetMansCount: Integer;
begin
   RESULT := SELF.TempExtTablesMansList.Count;
end;

procedure TIDB_TempExtTablesMan.DropModifiedSign;
var
   iI: Integer;
   TempExtTablesMan: TIDB_BaseExternalTablesMan;
begin
   for iI := 0 to TempExtTablesMansList.Count - 1 do
   begin
      TempExtTablesMan := TempExtTablesMansList[iI];
      TempExtTablesMan.DropModifiedSign;
   end;
end;

end.

