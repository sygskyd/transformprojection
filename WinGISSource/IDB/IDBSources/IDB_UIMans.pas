unit IDB_UIMans;
{
Internal database. Ver. II.
These objects provide an ability of tuning of menus (as main menu as well popup menu)
and toolbar from confguration file.

Notice:
  All these classes can be used onl for main window of IDB.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 26-07-2001
}
interface

uses Menus, Classes, Forms, Buttons,
   IDB_C_ToolBar;

type
   ///////////////////// MENU MANAGER /////////////////////////////////////////////
   PRecMenuItem = ^TrecMenuItem;
   TRecMenuItem = record
      sMenuItemName: AnsiString;
      bPrevEnableStatus: Boolean;

      AnOriginalMainMenuItem: TMenuItem;
      ARealMainMenuItem: TMenuItem;

      AnOriginalPopUpMenuItem: TMenuItem;
      ARealPopupMenuItem: TMenuItem;
   end;

   TIDB_MenuManager = class
   private
      AMenuList: TList;
      AOwnerForm: TForm;
      procedure Clear;
      function FindMenuItem (sMenuItemName: AnsiString): Integer;

      procedure Assign (ASource, ADestination: TMenuItem);

      function GetMenuItemCaption (sMenuItemName: AnsiString): AnsiString;
      procedure SetMenuItemCaption (sMenuItemName: AnsiString; sValue: AnsiString);

      function GetMenuItemVisible (sMenuItemName: AnsiString): Boolean;
      procedure SetMenuItemVisible (sMenuItemName: AnsiString; bValue: Boolean);

      function GetMenuItemEnabled (sMenuItemName: AnsiString): Boolean;
      procedure SetMenuItemEnabled (sMenuItemName: AnsiString; bValue: Boolean);

      function GetMenuItemChecked (sMenuItemName: AnsiString): Boolean;
      procedure SetMenuItemChecked (sMenuItemName: AnsiString; bValue: Boolean);

      function GetMainMenuItem (sMenuItemName: AnsiString): TMenuItem;
      function GetPopupMenuItem (sMenuItemName: AnsiString): TMenuItem;
   public
      constructor Create (AOwner: TForm);
      destructor Free;

      procedure RegisterItem (sMenuItemName: AnsiString; AnOriginalMainMenuItem, AnOriginalPopupMenuItem: TMenuItem);
      function Make (sMenuIniFileName: AnsiString; AMainMenuParent: TMainMenu; APopupMenuParent: TPopupMenu): Boolean;

      procedure MakeMenuEnabled;
      procedure MakeMenuDisabled;

      property MenuCaption[sMenuItemName: AnsiString]: AnsiString read GetMenuItemCaption write SetMenuItemCaption;
      property MenuVisible[sMenuItemName: AnsiString]: Boolean read GetMenuItemVisible write SetMenuItemVisible;
      property MenuEnabled[sMenuItemName: AnsiString]: Boolean read GetMenuItemEnabled write SetMenuItemEnabled;
      property MenuChecked[sMenuItemName: AnsiString]: Boolean read GetMenuItemChecked write SetMenuItemChecked;
      property MainMenuItem[sMenuItemName: AnsiString]: TMenuItem read GetMainMenuItem;
      property PopupMenuItem[sMenuItemName: AnsiString]: TMenuItem read GetPopupMenuItem;
   end;

   ///////////////////// TOOLBAR MANAGER //////////////////////////////////////////
   PrecButtonDesc = ^TrecButtonDesc;
   TrecButtonDesc = record
      sButtonName: AnsiString;
      AButton: TSpeedButton;
      iBitmapIndex: Integer;
      iTag: Integer;
      AProcedure: TNotifyEvent;
   end;

   TIDB_ToolbarManager = class
   private
      AnOwnerForm: TForm;
      AButtonsList: TList;

      procedure Clear;
      function FindButton (sButtonName: AnsiString): Integer;

      function GetButtonVisible (sButtonName: AnsiString): Boolean;
      procedure SetButtonVisible (sButtonName: AnsiString; bValue: Boolean);

      function GetButtonEnabled (sButtonName: AnsiString): Boolean;
      procedure SetButtonEnabled (sButtonName: AnsiString; bValue: Boolean);

      function GetButtonDown (sButtonName: AnsiString): Boolean;
      procedure SetButtonDown (sButtonName: AnsiString; bValue: Boolean);

      function GetButtonHint (sButtonName: AnsiString): AnsiString;
      procedure SetButtonHint (sButtonName: AnsiString; sValue: AnsiString);
   public
      constructor Create (AOwner: TForm);
      destructor Free;

      procedure RegisterButton (sButtonName: AnsiString; AButton: TSpeedButton);

      function Make (sToolbarIniFileName: AnsiString; ADefaultToolbar: TIDB_Toolbar): TIDB_ToolBar;

      property ButtonVisible[sButtonName: AnsiString]: Boolean read GetButtonVisible write SetButtonVisible;
      property ButtonEnabled[sButtonName: AnsiString]: Boolean read GetButtonEnabled write SetButtonEnabled;
      property ButtonDown[sButtonName: AnsiString]: Boolean read GetButtonDown write SetButtonDown;
      property ButtonHint[sButtonName: AnsiString]: AnsiString read GetButtonHint write SetButtonHint;
   end;

implementation

uses SysUtils,
   IDB_Consts;

///////////////////// MENU MANAGER /////////////////////////////////////////////

constructor TIDB_MenuManager.Create (AOwner: TForm);
begin
   SELF.AOwnerForm := AOwner;
   AMenuList := TList.Create;
end;

destructor TIDB_MenuManager.Free;
begin
   SELF.Clear;
   AMenuList.Free;
end;

procedure TIDB_MenuManager.Clear;
var
   pMI: PRecMenuItem;
   iI: Integer;
begin
   for iI := 0 to AMenuList.Count - 1 do
   begin
      pMI := AMenuList[iI];
      Dispose (pMI);
   end;
   AMenuList.Clear;
end;

function TIDB_MenuManager.FindMenuItem (sMenuItemName: AnsiString): Integer;
var
   pMI: PRecMenuItem;
   iI: Integer;
begin
   sMenuItemName := AnsiUpperCase (sMenuItemName);
   RESULT := -1;
   for iI := 0 to AMenuList.Count - 1 do
   begin
      pMI := AMenuList[iI];
      if pMI.sMenuItemName = sMenuItemName then
      begin
         RESULT := iI;
         BREAK;
      end;
   end;
end;

procedure TIDB_MenuManager.RegisterItem (sMenuItemName: AnsiString; AnOriginalMainMenuItem, AnOriginalPopupMenuItem: TMenuItem);
var
   pMI: PRecMenuItem;
begin
   if FindMenuItem (sMenuItemName) > -1 then EXIT;
   New (pMI);
   pMI.sMenuItemName := AnsiUpperCase (sMenuItemName);
   pMI.AnOriginalMainMenuItem := AnOriginalMainMenuItem;
   pMI.ARealMainMenuItem := AnOriginalMainMenuItem;
   pMI.AnOriginalPopUpMenuItem := AnOriginalPopupMenuItem;
   pMI.ARealPopupMenuItem := AnOriginalPopupMenuItem;
   AMenuList.Add (pMI);
end;

function TIDB_MenuManager.Make (sMenuIniFileName: AnsiString; AMainMenuParent: TMainMenu; APopupMenuParent: TPopupMenu): Boolean;
var
   AFile: TextFile;
   AString, AnUpperedString: AnsiString;
   sMenuItemName: AnsiString;
   bTheStartSignWasPassedBy: Boolean;
   iMenuListPos, iP: Integer;
   pMI: PRecMenuItem;
   ARealMenuItem: TMenuItem;
   AParentList: TList;
   ACurrentParent,
      ACandidatInParent: Pointer;

   bDummyVariable: Boolean; // In some places without this variable the compilator does not compile
   // CONTINUE line. I don't know why.
begin
{$O-}
   RESULT := FALSE;
   AssignFile (AFile, sMenuIniFileName);
   AParentList := TList.Create;
   try
      if AMainMenuParent <> nil then
      begin
         Reset (AFile);
         bTheStartSignWasPassedBy := FALSE;
         // Pass over all languages entries till the start menu sign will be reached.
         while not EOF (AFile) do
         begin
            ReadLn (AFile, AString);
            AnUpperedString := AnsiUpperCase (Trim (AString));
            if AnUpperedString = cs_MenuListStartSign then
            begin // '[MenuBar]'
               bTheStartSignWasPassedBy := TRUE;
               BREAK;
            end;
         end; // While end
         if bTheStartSignWasPassedBy then
         begin
            ACurrentParent := AMainMenuParent;
            while not EOF (AFile) do
            begin
               ReadLn (AFile, AString);
               AnUpperedString := AnsiUpperCase (Trim (AString));
               if (AnUpperedString = cs_MenuListEndSign) // ''
               or
                  (AnUpperedString = cs_PopupMenuListStartSign) // '[PopupMenu]'
               or
                  (AnUpperedString = cs_ToolbarStartSign) then
               begin // '[ToolBar]';
                  bDummyVariable := FALSE;
                  BREAK;
               end;
               if AnUpperedString = cs_SeparatorSign then
               begin // '<SEPARATOR>'
                  ARealMenuItem := TMenuItem.Create (AOwnerForm);
                  ARealMenuItem.Caption := '-';
                  if AParentList.Count = 0 then
                     TMainMenu (ACurrentParent).Items.Add (ARealMenuItem)
                  else
                     TMenuItem (ACurrentParent).Add (ARealMenuItem);
                  CONTINUE;
               end;
               if AnUpperedString = cs_StartChildMenuSign then
               begin // '('
                  AParentList.Add (ACurrentParent);
                  ACurrentParent := ACandidatInParent;
                  CONTINUE;
               end;
               if AnUpperedString = cs_EndChildMenuSign then
               begin // ')'
                  iP := AParentList.Count - 1;
                  ACurrentParent := AParentList[iP];
                  AParentList.Delete (iP);
                  CONTINUE;
               end;
               sMenuItemName := Trim (AString);
               iMenuListPos := SELF.FindMenuItem (sMenuItemName);
               if iMenuListPos < 0 then
               begin
                  bDummyVariable := FALSE;
                  CONTINUE;
               end;
               pMI := AMenuList[iMenuListPos];
               if pMI.AnOriginalMainMenuItem <> nil then
               begin
                  ARealMenuItem := TMenuItem.Create (AOwnerForm);
                  pMI.ARealMainMenuItem := ARealMenuItem;

                  Assign (pMI.AnOriginalMainMenuItem, ARealMenuItem);

                  if AParentList.Count = 0 then
                     TMainMenu (ACurrentParent).Items.Add (ARealMenuItem)
                  else
                     TMenuItem (ACurrentParent).Add (ARealMenuItem);
                  ACandidatInParent := ARealMenuItem;
               end;
            end; // While end

            iMenuListPos := SELF.FindMenuItem ('WindowsMenu');
            if iMenuListPos > -1 then
            begin
               pMI := AMenuList[iMenuListPos];
               if pMI.AnOriginalMainMenuItem <> nil then
               begin
                  ARealMenuItem := TMenuItem.Create (AOwnerForm);
                  pMI.ARealMainMenuItem := ARealMenuItem;

                  Assign (pMI.AnOriginalMainMenuItem, ARealMenuItem);

                  TMainMenu (ACurrentParent).Items.Add (ARealMenuItem);
                  ACurrentParent := ARealMenuItem;

                  iMenuListPos := SELF.FindMenuItem ('CascadeWindows');
                  if iMenuListPos > -1 then
                  begin
                     pMI := AMenuList[iMenuListPos];
                     if pMI.AnOriginalMainMenuItem <> nil then
                     begin
                        ARealMenuItem := TMenuItem.Create (AOwnerForm);
                        pMI.ARealMainMenuItem := ARealMenuItem;

                        Assign (pMI.AnOriginalMainMenuItem, ARealMenuItem);

                        TMenuItem (ACurrentParent).Add (ARealMenuItem);
                     end;
                  end;
                  ARealMenuItem := TMenuItem.Create (AOwnerForm);
                  ARealMenuItem.Caption := '-';
                  ARealMenuItem.Enabled := FALSE;
                  ARealMenuItem.Visible := FALSE;
                  TMenuItem (ACurrentParent).Add (ARealMenuItem);

                  iMenuListPos := SELF.FindMenuItem ('TileWindows');
                  if iMenuListPos > -1 then
                  begin
                     pMI := AMenuList[iMenuListPos];
                     if pMI.AnOriginalMainMenuItem <> nil then
                     begin
                        ARealMenuItem := TMenuItem.Create (AOwnerForm);
                        pMI.ARealMainMenuItem := ARealMenuItem;

                        Assign (pMI.AnOriginalMainMenuItem, ARealMenuItem);

                        TMenuItem (ACurrentParent).Add (ARealMenuItem);
                     end;
                  end;
                  ARealMenuItem := TMenuItem.Create (AOwnerForm);
                  ARealMenuItem.Caption := '-';
                  TMenuItem (ACurrentParent).Add (ARealMenuItem);
               end;
            end;
         end;
      end; // If AMainMenuParent <> NIL Then end
      if APopupMenuParent <> nil then
      begin
         // Pass over all languages entries till the start popup menu sign will be reached.
         Reset (AFile);
         bTheStartSignWasPassedBy := FALSE;
         AParentList.Clear;
         while not EOF (AFile) do
         begin
            ReadLn (AFile, AString);
            AnUpperedString := AnsiUpperCase (Trim (AString));
            if AnUpperedString = cs_PopupMenuListStartSign then
            begin // '[PopupMenu]'
               bTheStartSignWasPassedBy := TRUE;
               BREAK;
            end;
         end; // While end
         if bTheStartSignWasPassedBy then
         begin
            ACurrentParent := APopupMenuParent;
            while not EOF (AFile) do
            begin
               ReadLn (AFile, AString);
               AnUpperedString := AnsiUpperCase (Trim (AString));
               if (AnUpperedString = cs_PopupMenuListEndSign) // ''
               or
                  (AnUpperedString = cs_MenuListStartSign) // '[MenuBar]'
               or
                  (AnUpperedString = cs_ToolbarStartSign) then
               begin // '[ToolBar]';
                  bDummyVariable := FALSE;
                  BREAK;
               end;
               if AnUpperedString = cs_SeparatorSign then
               begin // '<SEPARATOR>'
                  ARealMenuItem := TMenuItem.Create (AOwnerForm);
                  ARealMenuItem.Caption := '-';
                  if AParentList.Count = 0 then
                     TMainMenu (ACurrentParent).Items.Add (ARealMenuItem)
                  else
                     TMenuItem (ACurrentParent).Add (ARealMenuItem);
                  CONTINUE;
               end;
               if AnUpperedString = cs_StartChildMenuSign then
               begin // '('
                  AParentList.Add (ACurrentParent);
                  ACurrentParent := ACandidatInParent;
                  CONTINUE;
               end;
               if AnUpperedString = cs_EndChildMenuSign then
               begin // ')'
                  iP := AParentList.Count - 1;
                  ACurrentParent := AParentList[iP];
                  AParentList.Delete (iP);
                  CONTINUE;
               end;
               sMenuItemName := Trim (AString);
               iMenuListPos := SELF.FindMenuItem (sMenuItemName);
               if iMenuListPos < 0 then
               begin
                  bDummyVariable := FALSE;
                  CONTINUE;
               end;
               pMI := AMenuList[iMenuListPos];
               if pMI.AnOriginalPopUpMenuItem <> nil then
               begin
                  ARealMenuItem := TMenuItem.Create (AOwnerForm);
                  pMI.ARealPopUpMenuItem := ARealMenuItem;

                  Assign (pMI.AnOriginalPopUpMenuItem, ARealMenuItem);

                  if AParentList.Count = 0 then
                     TPopupMenu (ACurrentParent).Items.Add (ARealMenuItem)
                  else
                     TMenuItem (ACurrentParent).Add (ARealMenuItem);
                  ACandidatInParent := ARealMenuItem;
               end;
            end; // While end
         end;
      end; // If APopupMenuParent <> NIL Then end
      RESULT := TRUE;
   finally
      CloseFile (AFile);
      AParentList.Clear;
      AParentList.Free;
   end;
end;

function TIDB_MenuManager.GetMenuItemCaption (sMenuItemName: AnsiString): AnsiString;
var
   pMI: PrecMenuItem;
   iP: Integer;
begin
   RESULT := '';
   iP := SELF.FindMenuItem (sMenuItemName);
   if iP < 0 then
      EXIT;
   pMI := AMenuList[iP];
   if pMI.AnOriginalMainMenuItem <> nil then
      RESULT := pMI.ARealMainMenuItem.Caption
   else
      if pMI.ARealPopupMenuItem <> nil then
         RESULT := pMI.ARealPopupMenuItem.Caption;
end;

procedure TIDB_MenuManager.SetMenuItemCaption (sMenuItemName: AnsiString; sValue: AnsiString);
var
   pMI: PrecMenuItem;
   iP: Integer;
begin
   iP := SELF.FindMenuItem (sMenuItemName);
   if iP < 0 then
      EXIT;
   pMI := AMenuList[iP];
   if pMI.ARealMainMenuItem <> nil then
      pMI.ARealMainMenuItem.Caption := sValue;
   if pMI.ARealPopupMenuItem <> nil then
      pMI.ARealPopupMenuItem.Caption := sValue;
end;

function TIDB_MenuManager.GetMenuItemVisible (sMenuItemName: AnsiString): Boolean;
var
   pMI: PrecMenuItem;
   iP: Integer;
begin
   RESULT := FALSE;
   iP := SELF.FindMenuItem (sMenuItemName);
   if iP < 0 then
      EXIT;
   pMI := AMenuList[iP];
   if pMI.ARealMainMenuItem <> nil then
      RESULT := pMI.ARealMainMenuItem.Visible
   else
      if pMI.ARealPopupMenuItem <> nil then
         RESULT := pMI.ARealPopupMenuItem.Visible;
end;

procedure TIDB_MenuManager.SetMenuItemVisible (sMenuItemName: AnsiString; bValue: Boolean);
var
   pMI: PrecMenuItem;
   iP: Integer;
begin
   iP := SELF.FindMenuItem (sMenuItemName);
   if iP < 0 then
      EXIT;
   pMI := AMenuList[iP];
   if pMI.ARealMainMenuItem <> nil then
      pMI.ARealMainMenuItem.Visible := bValue;
   if pMI.ARealPopupMenuItem <> nil then
      pMI.ARealPopupMenuItem.Visible := bValue;
end;

function TIDB_MenuManager.GetMenuItemEnabled (sMenuItemName: AnsiString): Boolean;
var
   pMI: PrecMenuItem;
   iP: Integer;
begin
   RESULT := FALSE;
   iP := SELF.FindMenuItem (sMenuItemName);
   if iP < 0 then
      EXIT;
   pMI := AMenuList[iP];
   if pMI.ARealMainMenuItem <> nil then
      RESULT := pMI.ARealMainMenuItem.Enabled
   else
      if pMI.ARealPopupMenuItem <> nil then
         RESULT := pMI.ARealPopupMenuItem.Enabled;
end;

procedure TIDB_MenuManager.SetMenuItemEnabled (sMenuItemName: AnsiString; bValue: Boolean);
var
   pMI: PrecMenuItem;
   iP: Integer;
begin
   iP := SELF.FindMenuItem (sMenuItemName);
   if iP < 0 then
      EXIT;
   pMI := AMenuList[iP];
   if pMI.ARealMainMenuItem <> nil then
      pMI.ARealMainMenuItem.Enabled := bValue;
   if pMI.ARealPopupMenuItem <> nil then
      pMI.ARealPopupMenuItem.Enabled := bValue;
end;

function TIDB_MenuManager.GetMenuItemChecked (sMenuItemName: AnsiString): Boolean;
var
   pMI: PrecMenuItem;
   iP: Integer;
begin
   RESULT := FALSE;
   iP := SELF.FindMenuItem (sMenuItemName);
   if iP < 0 then
      EXIT;
   pMI := AMenuList[iP];
   if pMI.ARealMainMenuItem <> nil then
      RESULT := pMI.ARealMainMenuItem.Checked
   else
      if pMI.ARealPopupMenuItem <> nil then
         RESULT := pMI.ARealPopupMenuItem.Checked;
end;

procedure TIDB_MenuManager.SetMenuItemChecked (sMenuItemName: AnsiString; bValue: Boolean);
var
   pMI: PrecMenuItem;
   iP: Integer;
begin
   iP := SELF.FindMenuItem (sMenuItemName);
   if iP < 0 then
      EXIT;
   pMI := AMenuList[iP];
   if pMI.ARealMainMenuItem <> nil then
      pMI.ARealMainMenuItem.Checked := bValue;
   if pMI.ARealPopupMenuItem <> nil then
      pMI.ARealPopupMenuItem.Checked := bValue;
end;

function TIDB_MenuManager.GetMainMenuItem (sMenuItemName: AnsiString): TMenuItem;
var
   iP: Integer;
   pMI: PrecMenuItem;
begin
   RESULT := nil;
   iP := SELF.FindMenuItem (sMenuItemName);
   if iP < 0 then
      EXIT;
   pMI := AMenuList[iP];
   RESULT := pMI.ARealMainMenuItem;
end;

function TIDB_MenuManager.GetPopupMenuItem (sMenuItemName: AnsiString): TMenuItem;
var
   iP: Integer;
   pMI: PrecMenuItem;
begin
   RESULT := nil;
   iP := SELF.FindMenuItem (sMenuItemName);
   if iP < 0 then
      EXIT;
   pMI := AMenuList[iP];
   RESULT := pMI.ARealPopupMenuItem;
end;

procedure TIDB_MenuManager.Assign (ASource, ADestination: TMenuItem);
begin
   if (ASource = nil) or (ADestination = nil) then
      EXIT;
   ADestination.Caption := ASource.Caption;
   ADestination.OnClick := ASource.OnClick;
   ADestination.Enabled := ASource.Enabled;
   ADestination.Visible := ASource.Visible;
   ADestination.Checked := ASource.Checked;
   ADestination.ShortCut := ASource.ShortCut;
   ADestination.ImageIndex := ASource.ImageIndex;
   ADestination.Tag := ASource.Tag;
end;

procedure TIDB_MenuManager.MakeMenuEnabled;
var
   iI: Integer;
   pMI: PrecMenuItem;
begin
   for iI := 0 to AMenuList.Count - 1 do
   begin
      try
         pMI := AMenuList[iI];
         if pMI.ARealMainMenuItem <> nil then
            pMI.ARealMainMenuItem.Enabled := pMI.bPrevEnableStatus;
         if pMI.ARealPopupMenuItem <> nil then
            pMI.ARealPopupMenuItem.Enabled := pMI.bPrevEnableStatus;
      except
      end;
   end; // For iI end
end;

procedure TIDB_MenuManager.MakeMenuDisabled;
var
   iI: Integer;
   pMI: PrecMenuItem;
begin
   for iI := 0 to AMenuList.Count - 1 do
   begin
      pMI := AMenuList[iI];
      if pMI.ARealMainMenuItem <> nil then
         try
            pMI.bPrevEnableStatus := pMI.ARealMainMenuItem.Enabled
         except
         end
      else
      begin
         if pMI.ARealPopupMenuItem <> nil then
            try
               pMI.bPrevEnableStatus := pMI.ARealPopupMenuItem.Enabled
            except
            end
         else
         begin
            pMI := nil; // The compilator does not compile next line without this line.
            CONTINUE;
         end;
      end;
      if pMI.ARealMainMenuItem <> nil then
         try
            pMI.ARealMainMenuItem.Enabled := FALSE;
         except
         end;
      if pMI.ARealPopupMenuItem <> nil then
         try
            pMI.ARealPopupMenuItem.Enabled := FALSE;
         except
         end;
   end; // For iI end
end;

///////////////////// TOOLBAR MANAGER //////////////////////////////////////////

constructor TIDB_ToolbarManager.Create (AOwner: TForm);
begin
   SELF.AnOwnerForm := AOwner;
   SELF.AButtonsList := TList.Create;
end;

destructor TIDB_ToolbarManager.Free;
begin
   SELF.Clear;
   SELF.AButtonsList.Free;
end;

procedure TIDB_ToolbarManager.Clear;
var
   pB: PrecButtonDesc;
   iI: Integer;
begin
   for iI := 0 to SELF.AButtonsList.Count - 1 do
   begin
      pB := AButtonsList[iI];
      Dispose (pB);
   end;
   AButtonsList.Clear;
end;

function TIDB_ToolbarManager.FindButton (sButtonName: AnsiString): Integer;
var
   pB: PrecButtonDesc;
   iI: Integer;
begin
   RESULT := -1;
   sButtonName := AnsiUpperCase (sButtonName);
   for iI := 0 to SELF.AButtonsList.Count - 1 do
   begin
      pB := AButtonsList[iI];
      if pB.sButtonName = sButtonName then
      begin
         RESULT := iI;
         BREAK;
      end;
   end;
end;

procedure TIDB_ToolbarManager.RegisterButton (sButtonName: AnsiString; AButton: TSpeedButton);
var
   pB: PrecButtonDesc;
begin
   if SELF.FindButton (sButtonName) < 0 then
   begin
      New (pB);
      pB.sButtonName := AnsiUpperCase (sButtonName);
      pB.AButton := AButton;
      SELF.AButtonsList.Add (pB);
   end;
end;

function TIDB_ToolbarManager.Make (sToolbarIniFileName: AnsiString; ADefaultToolbar: TIDB_Toolbar): TIDB_ToolBar;
var
   AFile: TextFile;
   bTheStartSignWasPassedBy: Boolean;
   AString,
      AnUpperedString: AnsiString;
   iI, iP: Integer;
   bDummyVariable: Boolean;
   pB: PrecButtonDesc;
   AnIniButtonsList: TStringList;
begin
   RESULT := TIDB_ToolBar.Create (AnOwnerForm);
   RESULT.Parent := AnOwnerForm;

   RESULT.ButtonsWidth := ADefaultToolbar.ButtonsWidth;
   RESULT.ButtonsHeight := ADefaultToolbar.ButtonsHeight;
   RESULT.ButtonsHorzSpacing := ADefaultToolbar.ButtonsHorzSpacing;
   RESULT.ButtonsSeparatorWidth := ADefaultToolbar.ButtonsSeparatorWidth;
   RESULT.ButtonsVertSpacing := ADefaultToolbar.ButtonsVertSpacing;

   RESULT.OnMouseMove := ADefaultToolbar.OnMouseMove;

   RESULT.Flat := TRUE;

   AnIniButtonsList := TStringList.Create;

   AssignFile (AFile, sToolbarIniFileName);
   try
      Reset (AFile);
      bTheStartSignWasPassedBy := FALSE;
      // Pass over all languages entries till the start buttons sign will be reached.
      while not EOF (AFile) do
      begin
         ReadLn (AFile, AString);
         AnUpperedString := AnsiUpperCase (Trim (AString));
         if AnUpperedString = cs_ToolbarStartSign then
         begin // '[Toolbar]'
            bTheStartSignWasPassedBy := TRUE;
            BREAK;
         end;
      end; // While end
      if bTheStartSignWasPassedBy then
      begin
         while not EOF (AFile) do
         begin
            ReadLn (AFile, AString);
            AnUpperedString := AnsiUpperCase (Trim (AString));
            if (AnUpperedString = cs_ToolbarEndSign) // ''
            or
               (AnUpperedString = cs_MenuListStartSign) // '[MENUBAR]'
            or
               (AnUpperedString = cs_PopupMenuListStartSign) then
            begin // '[POPUPMENU]'
               bDummyVariable := FALSE;
               BREAK;
            end;
            AnIniButtonsList.Add (AnUpperedString);
         end;
         // I go down from List.Count-1 To 0 because when I make an insert into the TToolbar,
         // new button will be added into the left position of buttons of the TToolbar.
         for iI := 0 to AnIniButtonsList.Count - 1 do
         begin
            AnUpperedString := AnIniButtonsList[iI];
            if AnUpperedString = cs_SeparatorSign then // '<SEPARATOR>'
               RESULT.AddButton (tbSeparator)
            else
            begin
               iP := SELF.FindButton (AnUpperedString);
               if iP > -1 then
               begin
                  pB := AButtonsList[iP];
                  RESULT.AddButton (TSpeedButton (pB.AButton));
               end;
            end;
         end; // For iI end
      end;
   finally
      CloseFile (AFile);
      if RESULT.ButtonsCount = 0 then
      begin
         RESULT.Free;
         RESULT := nil;
      end;
   end;
// ++ Cadmensky
   AnIniButtonsList.Clear;
   AnIniButtonsList.Free;
// -- Cadmensky
end;

function TIDB_ToolbarManager.GetButtonVisible (sButtonName: AnsiString): Boolean;
var
   iP: Integer;
   pB: PrecButtonDesc;
begin
   RESULT := FALSE;
   iP := SELF.FindButton (sButtonName);
   if iP < 0 then EXIT;
   pB := AButtonsList[iP];
   if pB.AButton <> nil then
      RESULT := pB.AButton.Visible;
end;

procedure TIDB_ToolbarManager.SetButtonVisible (sButtonName: AnsiString; bValue: Boolean);
var
   iP: Integer;
   pB: PrecButtonDesc;
begin
   iP := SELF.FindButton (sButtonName);
   if iP < 0 then EXIT;
   pB := AButtonsList[iP];
   if pB.AButton <> nil then
      pB.AButton.Visible := bValue;
end;

function TIDB_ToolbarManager.GetButtonEnabled (sButtonName: AnsiString): Boolean;
var
   iP: Integer;
   pB: PrecButtonDesc;
begin
   RESULT := FALSE;
   iP := SELF.FindButton (sButtonName);
   if iP < 0 then EXIT;
   pB := AButtonsList[iP];
   if pB.AButton <> nil then
      RESULT := pB.AButton.Enabled;
end;

procedure TIDB_ToolbarManager.SetButtonEnabled (sButtonName: AnsiString; bValue: Boolean);
var
   iP: Integer;
   pB: PrecButtonDesc;
begin
   iP := SELF.FindButton (sButtonName);
   if iP < 0 then EXIT;
   pB := AButtonsList[iP];
   if pB.AButton <> nil then
      pB.AButton.Enabled := bValue;
end;

function TIDB_ToolbarManager.GetButtonDown (sButtonName: AnsiString): Boolean;
var
   iP: Integer;
   pB: PrecButtonDesc;
begin
   RESULT := FALSE;
   iP := SELF.FindButton (sButtonName);
   if iP < 0 then EXIT;
   pB := AButtonsList[iP];
   if pB.AButton <> nil then
      RESULT := pB.AButton.Down;
end;

procedure TIDB_ToolbarManager.SetButtonDown (sButtonName: AnsiString; bValue: Boolean);
var
   iP: Integer;
   pB: PrecButtonDesc;
begin
   iP := SELF.FindButton (sButtonName);
   if iP < 0 then EXIT;
   pB := AButtonsList[iP];
   if pB.AButton <> nil then
      pB.AButton.Down := bValue;
end;

function TIDB_ToolbarManager.GetButtonHint (sButtonName: AnsiString): AnsiString;
var
   iP: Integer;
   pB: PrecButtonDesc;
begin
   RESULT := '';
   iP := SELF.FindButton (sButtonName);
   if iP < 0 then EXIT;
   pB := AButtonsList[iP];
   if pB.AButton <> nil then
      RESULT := pB.AButton.Hint;
end;

procedure TIDB_ToolbarManager.SetButtonHint (sButtonName: AnsiString; sValue: AnsiString);
var
   iP: Integer;
   pB: PrecButtonDesc;
begin
   iP := SELF.FindButton (sButtonName);
   if iP < 0 then EXIT;
   pB := AButtonsList[iP];
   if pB.AButton <> nil then
      pB.AButton.Hint := sValue;
end;

end.

