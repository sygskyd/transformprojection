unit IDB_InsertRecordsManager;
{
Internal database. Ver. II.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 22-05-2001
}
interface

uses Classes,
   DMaster, DTables;

type
   TrecData = record
      AProject: Pointer;
      ALayer: Pointer;
      sLayerTableName: AnsiString;
      AQuery: TDQuery;
      bLayerTableWasEmpty: Boolean;
      ARecordTemplate: TList;
   end;

   TIDB_InsertRecordsManager = class
   private
      AList: TList;

      procedure ClearData;
      procedure Delete (iPosition: Integer);
      function Find (AProject, ALayer: Pointer): Integer;
   public
      constructor Create;
      destructor Free;

      function AddLayer (AProject, ALayer: Pointer; AMaster: TDMaster; ARecordTemplate: TList): Integer;
      procedure DeleteLayer (iPosition: Integer);

      function InsertRecord (iPosition, iLayerItemID: Integer): Boolean;
   end;

implementation

uses SysUtils,
   IDB_Consts, IDB_MainManager, IDB_CallbacksDef, IDB_Utils;

constructor TIDB_InsertRecordsManager.Create;
begin
   AList := TList.Create;
end;

destructor TIDB_InsertRecordsManager.Free;
begin
   SELF.ClearData;
   AList.Free;
end;

procedure TIDB_InsertRecordsManager.ClearData;
var
   iI: Integer;
   pD: ^TrecData;
begin
   for iI := 0 to AList.Count - 1 do
   begin
      pD := AList[iI];
      if pD = nil then CONTINUE;
      pd.AQuery.Free;
      Dispose (pD);
   end;
   AList.Clear;
end;

procedure TIDB_InsertRecordsManager.Delete (iPosition: Integer);
var
   pD: ^TrecData;
begin
   if (iPosition < 0) or (iPosition >= AList.Count) then EXIT;
   pD := AList[iPosition];
   if pD = nil then EXIT;
   pD.AQuery.Free;
   Dispose (pD);
   AList[iPosition] := nil;
end;

function TIDB_InsertRecordsManager.Find (AProject, ALayer: Pointer): Integer;
var
   iI: Integer;
   pD: ^TrecData;
begin
   RESULT := -1;
   for iI := 0 to AList.Count - 1 do
   begin
      pD := AList[iI];
      if pD = nil then CONTINUE;
      if (pD.AProject = AProject) and (pD.ALayer = ALayer) then
      begin
         RESULT := iI;
         EXIT;
      end;
   end;
end;

function TIDB_InsertRecordsManager.AddLayer (AProject, ALayer: Pointer; AMaster: TDMaster; ARecordTemplate: TList): Integer;
var
   iP: Integer;
   pD: ^TrecData;
begin
   iP := SELF.Find (AProject, ALayer);
   if iP > -1 then
   begin
      RESULT := iP;
      EXIT;
   end;
   New (pD);
   pD.AProject := AProject;
   pD.ALayer := ALayer;
   pD.ARecordTemplate := ARecordTemplate;
   pD.AQuery := TDQuery.Create (nil);
   pD.AQuery.Master := AMaster;

   if not AnIDBMainMan.UseNewATTNames (AProject) then
      pD.sLayerTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
   else
      pD.sLayerTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

   pD.AQuery.SQL.Text := 'SELECT [' + cs_InternalID_FieldName + '] FROM [' + pD.sLayerTableName + ']';
   try
      pD.AQuery.Open;
      pD.bLayerTableWasEmpty := pD.AQuery.RecordCount < 1;
   except
      pD.bLayerTableWasEmpty := FALSE;
   end;
   pD.AQuery.Close;

   AList.Add (pD);
   RESULT := AList.Count - 1;
end;

procedure TIDB_InsertRecordsManager.DeleteLayer (iPosition: Integer);
begin
   SELF.Delete (iPosition);
end;

function TIDB_InsertRecordsManager.InsertRecord (iPosition, iLayerItemID: Integer): Boolean;
var
   pD: ^TrecData;
   sLayerItemID: AnsiString;
   iRecordCount: Integer;
   sFieldNames,
      sValues: AnsiString;
   iI: Integer;
   pRecordTemplateItem: ^TrecRecordTemplateItem;
begin
   RESULT := FALSE;
   if (iPosition < 0) or (iPosition >= AList.Count) then EXIT;

   try
      sLayerItemID := IntToStr (iLayerItemID);
   except
      EXIT;
   end;
   pD := AList[iPosition];
   if not pD.bLayerTableWasEmpty then
   begin
        // If the attribute table has some records (may be the only record - it's enough)
        // before reconciling operation.
      pD.AQuery.SQL.Text := 'SELECT [' + cs_StateFieldName + '] FROM [' + pD.sLayerTableName + '] WHERE PROGISID = ' + sLayerItemID;
      try
         pD.AQuery.Open;
         iRecordCount := pD.AQuery.RecordCount;
         if (iRecordCount > 0) and (pD.AQuery.Fields[0].AsString <> cs_ObjectIsDeleted) then
         begin
            pD.AQuery.Close;
            EXIT;
         end;
         pD.AQuery.Close;
      except
         pD.AQuery.Close;
         EXIT;
      end;
   end
   else
        // If the attribute table doesn't have any record before reconciling operation.
      iRecordCount := 0;

   if iRecordCount = 0 then
   begin // If such record does not exist or there was not any record before
                                    // reconciling, add it.
      try // INSERT INTO table_name (field_name_1, field_name_2) VALUES (value_1, value_2)
         sFieldNames := ' ProgisID, ' + cs_StateFieldName;
         sValues := sLayerItemID + ', ' + cs_ObjectIsJustAdded;
         for iI := 0 to pD.ARecordTemplate.Count - 1 do
         begin
            pRecordTemplateItem := pD.ARecordTemplate[iI];
            if Trim (pRecordTemplateItem.sValue) <> '' then
            begin
               sFieldNames := sFieldNames + ', ' + pRecordTemplateItem.sFieldName;
               if pRecordTemplateItem.iFieldType = 1 then
                  sValues := sValues + ', ''' + pRecordTemplateItem.sValue + ''''
               else
                  sValues := sValues + ', ' + pRecordTemplateItem.sValue;
            end;
         end; // For iI end
         pD.AQuery.ExecSQL ('INSERT INTO [' + pD.sLayerTableName + '] (' + sFieldNames + ') ' +
            'VALUES (' + sValues + ')');
         RESULT := TRUE;
      except
      end;
   end
   else
   begin // If such record exists, mark it as 'just added'.
      sValues := cs_StateFieldName + ' = ' + cs_ObjectIsJustAdded;
      for iI := 0 to pD.ARecordTemplate.Count - 1 do
      begin
         pRecordTemplateItem := pD.ARecordTemplate[iI];
         sValues := sValues + ',  ' + pRecordTemplateItem.sFieldName + ' = ';
         if pRecordTemplateItem.iFieldType = 1 then
            sValues := sValues + '''' + pRecordTemplateItem.sValue + ''''
         else
            sValues := sValues + pRecordTemplateItem.sValue;
      end; // For iI end
      try // UPDATE table_name SET field_name_1 = value_1, field_name_2 = value_2 WHERE key_field = key_value
         pD.AQuery.ExecSQL ('UPDATE [' + pD.sLayerTableName + '] ' +
            'SET ' + sValues + ' ' +
            'WHERE PROGISID = ' + sLayerItemID);
         RESULT := TRUE;
      except
      end;
   end;
end;

end.

