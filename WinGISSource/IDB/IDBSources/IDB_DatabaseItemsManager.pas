unit IDB_DatabaseItemsManager;
{
Internal database. Ver. II.
This module provides the management of the database items (managenet of index, fieds, tables, etc).

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 05-02-2001
}
interface

uses DMaster, DTables, Classes, StdCtrls,
  IDB_Consts;

type
  TIDB_DatabaseItemsManager = class
  private
    AProject: Pointer;
    ExecQuery: TDQuery;
  public
    constructor Create(AProject: Pointer; AMaster: TDMaster);
    destructor Free;

      { Checking of the version of database file. }
    function CheckDBFileVersion: TDBVersion;

      { A management of tables. }
    function TableExists(sTableName: AnsiString): Boolean;

    function CreateAttributeTable(sNewTableName: AnsiString; ALayer: Pointer): Boolean;
    function CheckGUIDField(sTableName: AnsiString): Boolean;
//      procedure CreatePatternTable (sNewTableName: AnsiString);
//      procedure CreateDescTable (sNewTableName: AnsiString);
    procedure CreateCommonDescTable;
      //          Function CreateBlobSpecialTable: Boolean;
    procedure CreateTextSpecialTable;
    procedure CreateInternalTables;
    function CreateTableWithoutAnyField(sNewTableName: AnsiString): Boolean;

    function DeleteTable(sTableName: AnsiString): Boolean;

    function CreateAllLayerTables(ALayer: Pointer): boolean;
    procedure DeleteAllLayerTables(sLayerIndex: AnsiString);
    procedure DeleteOldDescriptionTable(sLayerIndex: AnsiString);

    procedure GetTablesNames(slTables: TStrings; bDetectSytemTables: Boolean); overload;
    procedure GetTablesNames(lbTables: TListBox; bDetectSytemTables: Boolean); overload;
    procedure GetTablesNames(sMask: AnsiString; var slTables: TStringList); overload;

      {A management of table indicies.}
    function AddIndex(sTableName, sFieldName: AnsiString; bIndexIsUnique: Boolean = FALSE): boolean;
    procedure DeleteIndex(sTableName, sFieldName: AnsiString);

    procedure AddPrimaryKey(sTableName, sFieldName: AnsiString);
    procedure AlterField(sTableName, sFieldName: AnsiString; iFieldType, iFieldSize: Integer);

      { A management of table fields. }
// ++ Cadmensky Version 2.3.2
    function GetReadyToCheckTableStructure(sTableName: AnsiString): Integer;
    procedure EndOfCheckingTable;
//      function FieldExists (sTableName, sFieldName: AnsiString; var bFieldIsPresent: Boolean): Integer;
    function FieldExists(sFieldName: AnsiString): Boolean;
// -- Cadmensky Version 2.3.2
    procedure AddField(sTableName, sFieldName: AnsiString; iFieldType, iFieldSize: Integer);
    procedure DeleteField(sTableName, sFieldName: AnsiString);
    procedure DeleteTempField(sTableName: AnsiString);
// ++ Cadmensky
    procedure CreateLayerNamesSysTable;
    function CreateLinkTable(sSourceDatabaseFileName, sSourceTableName, sLinkName: AnsiString; iWorkMode: integer; sServerName: AnsiString = ''; sUserID: AnsiString = ''; sPassword: AnsiString = ''): Boolean;
    procedure DeleteLinkTable(sLinkName: AnsiString);
// -- Cadmensky
  end;

implementation

uses SysUtils, Windows, Forms, DBTables, Controls,
  IDB_MainManager, IDB_LanguageManager, IDB_Utils, IDB_CallBacksDef,
  Dialogs, ADOX_TLB, ADODB_TLB;

constructor TIDB_DatabaseItemsManager.Create(AProject: Pointer; AMaster: TDMaster);
begin
  SELF.AProject := AProject;
  SELF.ExecQuery := TDQuery.Create(nil);
  SELF.ExecQuery.Master := AMaster;
end;

destructor TIDB_DatabaseItemsManager.Free;
begin
  SELF.ExecQuery.Free;
end;

///////////////////          A management of tables.          //////////////////

function TIDB_DatabaseItemsManager.TableExists(sTableName: AnsiString): Boolean;
var
  slTables: TStringList;
  iI: Integer;
begin
  RESULT := FALSE;
  slTables := TStringList.Create;
  GetTablesNames(slTables, false);
  sTableName := AnsiUpperCase(sTableName);
  for iI := 0 to slTables.Count - 1 do
    if AnsiUpperCase(slTables[iI]) = sTableName then
    begin
      RESULT := TRUE;
      BREAK;
    end;
  slTables.Free;
end;

function TIDB_DatabaseItemsManager.CreateAttributeTable(sNewTableName: AnsiString; ALayer: Pointer): Boolean;
var
  sqlText, tpltable: string;

begin
  RESULT := FALSE;
   // If the table is already exists to create it it is not necessary.
  if SELF.TableExists(sNewTableName) then
  begin
    CheckGUIDField(sNewTableName);
    RESULT := TRUE;
    exit;
  end;
  showtablepropertiesdlg := True;
  sqlText := ('CREATE TABLE [' + sNewTableName + '] ' +
    '([ProgisID] Integer NULL, ' +
    '[GUID] TEXT(50), ' +
    '[' + cs_StateFieldName + '] BYTE NULL, ' +
    '[' + cs_InternalID_FieldName + '] AUTOINCREMENT, ' +
         // ++ Cadmensky
//         '[' + cs_CalcFieldAreaName + '] FLOAT, ' +
//         '[' + cs_CalcFieldSizeName + '] FLOAT, ' +
//         '[' + cs_CalcFieldLengthName + '] FLOAT, ' +
//         '[' + cs_CalcFieldPerimeterName + '] FLOAT, ' +
//         '[' + cs_CalcFieldXName + '] FLOAT, ' +
//         '[' + cs_CalcFieldYName + '] FLOAT, ' +
//         '[' + cs_CalcFieldVerticesCount + '] Integer, ' +
//         '[' + cs_CalcFieldLinkIDName + '] Integer, ' +
//         '[' + cs_CalcFieldTextName + '] TEXT (50), ' +
//         '[' + cs_CalcFieldSymbolName + '] TEXT (50), ' +
         // -- Cadmensky
    'Field1 TEXT (50) NULL, ' +
    'CONSTRAINT IdIndex UNIQUE (ProgisID))');
  try
    if SELF.TableExists('USYS_WGIDB_DT') then
    begin // Check: gibt es eine Vorlage?
      try
        self.AddField('USYS_WGIDB_DT', 'isTemplate', 1, 255);
        self.ExecQuery.Text := 'SELECT [isTemplate] FROM [USYS_WGIDB_DT] WHERE [isTemplate] LIKE "ATT_%"';
        self.ExecQuery.Open;
        tpltable := self.ExecQuery.Fieldbyname('isTemplate').AsString;
        self.ExecQuery.Close;
        if self.TableExists(tpltable) then
        begin
          self.ExecQuery.ExecSQL('INSERT INTO [USYS_WGIDB_DT] ( AnnotationFields, ChartFields, OverlayFields, InfoFields, DBGridProps, DBGridPosX, DBGridPosY, DBGridWidth, DBGridHeight, MonitorID, DBButtonDown, LI )' +
            ' SELECT AnnotationFields, ChartFields, OverlayFields, InfoFields, DBGridProps, DBGridPosX, DBGridPosY, DBGridWidth, DBGridHeight, MonitorID, DBButtonDown, ' + inttostr(GetLayerIndexFunc(ALayer)) +
            ' FROM [USYS_WGIDB_DT] WHERE isTemplate LIKE "ATT_%"');
          sqlText := ('SELECT * INTO [' + sNewTableName + '] FROM [' + tpltable + '] WHERE [' + cs_InternalID_FieldName + '] = -1 ');
          showtablepropertiesdlg := False;
        end;
      except
      end;
    end;
    self.ExecQuery.ExecSQL(sqlText);

  except
      // Error of creating a table ''%s''.
    MessageBox(Application.Handle, PChar(Format(ALangManager.GetValue(103), [sNewTableName])), PChar(cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
    showtablepropertiesdlg := False;
    EXIT;
  end;
  RESULT := TRUE;
end;

{ Since version 2 I think "pattern table" is usefulless. }

{procedure TIDB_DatabaseItemsManager.CreatePatternTable (sNewTableName: AnsiString);
begin

end;}
{ Now we have a common description table for all layers. }

{procedure TIDB_DatabaseItemsManager.CreateDescTable (sNewTableName: AnsiString);
begin

end;}

procedure TIDB_DatabaseItemsManager.CreateCommonDescTable;
begin
   // If the table is already exists to create it it is not necessary.
  if SELF.TableExists(cs_CommonDescTableName) then
    EXIT;
  try
      // Lengths of fields equal 64 because it is the max length of MSAccess field's name.
    SELF.ExecQuery.ExecSQL('CREATE TABLE [' + cs_CommonDescTableName + '] ' + // '_WG_DT'
      '([_USER_ID] TEXT (38), ' +
      '[_PROJECT_ID] TEXT (38), ' +
      '[' + cs_CDT_LayerIndexFieldName + '] Integer, ' + // 'LI'
      'XCoordinate TEXT (64), ' +
      'YCoordinate TEXT (64), ' +
      'SymbolNumber TEXT (64), ' +
      'SymbolName TEXT (64), ' +
      'SymbolSize TEXT (64), ' +
      'UseSymbolName LOGICAL, ' +
      'GeoText TEXT (64), ' +
      'AnnotationFields MEMO, ' +
      'ChartFields MEMO, ' +
         // ++ Cadmensky
      'OverlayFields MEMO, ' +
      'InfoFields MEMO, ' +
      'QueryNames MEMO,' +
      'QueryTexts MEMO,' +
         // -- Cadmensky
      'DBGridProps LONGBINARY, ' +
      'DBGridPosX Integer, ' +
      'DBGridPosY Integer, ' +
      'DBGridWidth Integer, ' +
      'DBGridHeight Integer, ' +
      'MonitorID TEXT (64), ' +
      cs_InternalID_FieldName + ' AUTOINCREMENT' +
      ')');
  except
      // Error of creating a table ''%s''.
//        MessageBox(AWinGISMainForm.Handle, PChar(Format(ALangManager.GetValue(103), [cs_CommonDescTableName])), cs_ApplicationName, MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
  end;
end;

{Function TIDB_DatabaseItemsManager.CreateBlobSpecialTable: Boolean;
Begin
     RESULT := TRUE;
     If NOT SELF.TableExists(cs_IT_ST_BlobInfo) Then Begin
        TRY
           SELF.ExecQuery.ExecSQL('CREATE TABLE [' + cs_IT_ST_BlobInfo + '] ' +
                                  '(SPID BYTE, ' +
                                  cs_SPFieldName + ' LONGBINARY' +
                                  ')');
        EXCEPT
           RESULT := FALSE;
           // Error of creating a table ''%s''.
           MessageBox(AWinGISMainForm.Handle, PChar(Format(ALangManager.GetValue(103), [cs_IT_ST_BlobInfo])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
        END;
        End;
End;}

procedure TIDB_DatabaseItemsManager.CreateTextSpecialTable;
begin
  if not SELF.TableExists(cs_IT_ST_TextInfo) then
  begin
    try
      SELF.ExecQuery.ExecSQL('CREATE TABLE [' + cs_IT_ST_TextInfo + '] ' +
        '(SPID BYTE, ' +
        cs_SPFieldName + ' TEXT (38)' +
        ')');
    except
         // Error of creating a table ''%s''.
      MessageBox(Application.Handle, PChar(Format(ALangManager.GetValue(103), [cs_IT_ST_TextInfo])), PChar(cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
      EXIT;
    end;
    try
         // INSERT INTO table_name (field_name_1, field_name_2) VALUES (value_1, value_2)
      SELF.ExecQuery.ExecSQl('INSERT INTO [' + cs_IT_ST_TextInfo + '] (SPID, ' + cs_SPFieldName + ') ' +
        'VALUES (1, ''CG'')');
    except
    end;
    try
      SELF.ExecQuery.ExecSQl('INSERT INTO [' + cs_IT_ST_TextInfo + '] (SPID, ' + cs_SPFieldName + ') ' +
        'VALUES (0, ''SG'')');
    except
    end;
  end;
end;

procedure TIDB_DatabaseItemsManager.CreateInternalTables;
begin
   //     SELF.CreateBlobSpecialTable;  //<< For thematic map.
  SELF.CreateTextSpecialTable; //<< Since 25.07.2000 this table has been containing GUIDs (start and current values) of DB file.
  SELF.CreateCommonDescTable; //<< There is a new methodology of description table since 24.10.2001.
end;

function TIDB_DatabaseItemsManager.CreateTableWithoutAnyField(sNewTableName: AnsiString): Boolean;
begin
  RESULT := FALSE;
   // If the table is already exists to create it it is not necessary.
  if SELF.TableExists(sNewTableName) then
    EXIT;
  try
      // For the creating of table without fields, I create ProgisID field and at once I delete it.
    SELF.ExecQuery.ExecSQL('CREATE TABLE [' + sNewTableName + '] ' +
      '(ProgisID Integer)');
    SELF.DeleteField(sNewTableName, 'ProgisID');
  except
      // Error of creating a table ''%s''.
    MessageBox(AWinGISMainForm.Handle, PChar(Format(ALangManager.GetValue(103), [sNewTableName])), PChar(cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
    EXIT;
  end;
  RESULT := TRUE;
end;

function TIDB_DatabaseItemsManager.DeleteTable(sTableName: AnsiString): Boolean;
begin
  RESULT := FALSE;
  if not TableExists(sTableName) then
  begin
    RESULT := TRUE;
    EXIT;
  end;
  try
    SELF.ExecQuery.ExecSQL('DROP TABLE [' + sTableName + ']');
  except
      // Error of deleting the table ''%s''.
//        MessageBox(AWinGISMainForm.Handle, PChar(Format(ALangManager.GetValue(111), [sTableName])), cs_ApplicationName, MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
    EXIT;
  end;
  RESULT := TRUE;
end;

function TIDB_DatabaseItemsManager.CreateAllLayerTables(ALayer: Pointer): boolean;
var
  sAttTableName: AnsiString;
  sLayerIndex: AnsiString;
begin
  sLayerIndex := IntToStr(GetLayerIndexFunc(ALayer));
   // Attribute table and pattern table always have the same structure. We shall always try to reach it!
  if not AnIDBMainMan.UseNewATTNames(AProject) then
    RESULT := SELF.CreateAttributeTable(cs_AttributeTableNamePrefix + sLayerIndex, ALayer)
  else
  begin
    sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx(AProject, ALayer);
    RESULT := SELF.CreateAttributeTable(sAttTableName, ALayer);
  end;
// ++ Cadmensky
  if not RESULT then
    exit;
// -- Cadmensky

//   SELF.CreatePatternTable (cs_PatternTableNamePrefix + sLayerIndex);
   // The description table has a structure that are distinguished from other layer's tables.
//   SELF.CreateDescTable (cs_DescriptionTableNamePrefix + sLayerIndex);
  SELF.CreateCommonDescTable;

// ++ Cadmensky Version 2.2.8
  SELF.ExecQuery.Master.Connected := false;
  SELF.ExecQuery.Master.Connected := true;
// -- Cadmensky Version 2.2.8
end;

procedure TIDB_DatabaseItemsManager.DeleteAllLayerTables(sLayerIndex: AnsiString);
begin
  SELF.DeleteOldDescriptionTable(sLayerIndex); // << Old table. Now it isn't used.
  SELF.DeleteTable(cs_PatternTableNamePrefix + sLayerIndex); // << Old table. Now it isn't used.
  try // New description table. Delete all data about the layer from it.
    SELF.ExecQuery.ExecSQL('DELETE FROM [' + cs_CommonDescTableName + '] ' +
      'WHERE [' + cs_CDT_LayerIndexFieldName + '] = ' + sLayerIndex);
  except
  end;
end;

procedure TIDB_DatabaseItemsManager.DeleteOldDescriptionTable(sLayerIndex: AnsiString);
begin
  SELF.DeleteTable(cs_DescriptionTableNamePrefix + sLayerIndex);
end;

procedure TIDB_DatabaseItemsManager.GetTablesNames(slTables: TStrings; bDetectSytemTables: Boolean);
begin
  SELF.ExecQuery.Master.CentralData.GetADOTableNames(slTables);
end;

procedure TIDB_DatabaseItemsManager.GetTablesNames(lbTables: TListBox; bDetectSytemTables: Boolean);
begin
  SELF.ExecQuery.Master.CentralData.GetADOTableNames(lbTables.Items);
end;

procedure TIDB_DatabaseItemsManager.GetTablesNames(sMask: AnsiString; var slTables: TStringList);
var
  slNames: TStringList;
  iI: Integer;
  sTableName: AnsiString;
begin
  slTables.Clear;
  slNames := TStringList.Create;
  sMask := AnsiUpperCase(sMask);
  SELF.GetTablesNames(slNames, FALSE);
  for iI := 0 to slNames.Count - 1 do
  begin
    sTableName := AnsiUpperCase(slNames[iI]);
    if Copy(sTableName, 1, Length(sMask)) = sMask then
      slTables.Add(slNames[iI]);
  end;
  slNames.Free;
end;

///////////////////     A management of table indicies.     ////////////////////

function TIDB_DatabaseItemsManager.AddIndex(sTableName, sFieldName: AnsiString; bIndexIsUnique: Boolean = FALSE): boolean;
begin
  RESULT := true;
  try
    if not bIndexIsUnique then
      //        SELF.ExecQuery.ExecSQL('CREATE INDEX [' + ReplaceChars(sFieldName) + '_INDEX] ON [' + sTableName + '] ' + '([' + sFieldName + '] ASC)')
      // Cadmensky
      SELF.ExecQuery.ExecSQL('CREATE INDEX [' + ReplaceChars(sFieldName) + '] ON [' + sTableName + '] ' + '([' + sFieldName + '] ASC)')
    else
      //        SELF.ExecQuery.ExecSQL('ALTER TABLE [' + sTableName + '] ADD CONSTRAINT [' + ReplaceChars(sFieldName) + '_INDEX UNIQUE ([' + sFieldName + ']))');
      // Cadmensky
      SELF.ExecQuery.ExecSQL('ALTER TABLE [' + sTableName + '] ADD CONSTRAINT [' + ReplaceChars(sFieldName) + '] UNIQUE ([' + sFieldName + ']))');
  except
    RESULT := false;
  end;
end;

procedure TIDB_DatabaseItemsManager.AddPrimaryKey(sTableName, sFieldName: AnsiString);
begin
   // Constraint name is the following: _WG_PK_TableName_FieldName
  SELF.ExecQuery.ExecSQL('ALTER TABLE [' + sTableName + '] ' +
      //                            'ADD CONSTRAINT [_WG_PK_' + sTableName + '_' + sFieldName + '] ' +
      // Cadmensky
    'ADD CONSTRAINT [' + sFieldName + '] ' +
    'PRIMARY KEY ([' + sFieldName + '])');
end;

procedure TIDB_DatabaseItemsManager.DeleteIndex(sTableName, sFieldName: AnsiString);
begin
  try
      //        SELF.ExecQuery.ExecSQL('DROP INDEX [' + ReplaceChars(sFieldName) + '_INDEX] ON [' + sTableName + ']');
      // Cadmensky
    SELF.ExecQuery.ExecSQL('DROP INDEX [' + ReplaceChars(sFieldName) + '] ON [' + sTableName + ']');
  except
  end;
end;

///////////////////         A management of table fields.   ////////////////////

// ++ Cadmensky Version 2.3.2

procedure TIDB_DatabaseItemsManager.EndOfCheckingTable;
begin
  if SELF.ExecQuery.Active then
    SELF.ExecQuery.Close;
end;

function TIDB_DatabaseItemsManager.GetReadyToCheckTableStructure(sTableName: AnsiString): Integer;
var
  iI: Integer;
begin
  RESULT := ci_All_Ok;

  if not SELF.TableExists(sTableName) then
  begin
    RESULT := ci_TableNotExists;
    EXIT;
  end;

  if SELF.ExecQuery.Active then
    ExecQuery.Close;

  ExecQuery.SQL.Text := 'SELECT * FROM [' + sTableName + ']';
  try
    ExecQuery.Open;
  except
    on E: EDBEngineError do
    begin
      for iI := 0 to E.ErrorCount - 1 do
            // This error was caused because there is noone field in a table. All is OK.
        if E.Errors[iI].NativeError = ci_ThereIsNoFieldInTable then
          Exit;
         // And thi is unknow error (general SQL). We show the error's message to user and exit from the function.
      RESULT := ci_GeneralSQLError;
      MessageBox(Application.Handle, PChar(E.Message), PChar(cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
      Exit;
    end;
  end;
end;
// -- Cadmensky Version 2.3.2

// ++ Cadmensky Version 2.3.2

function TIDB_DatabaseItemsManager.FieldExists(sFieldName: AnsiString): Boolean;
//function TIDB_DatabaseItemsManager.FieldExists (sTableName, sFieldName: AnsiString; var bFieldIsPresent: Boolean): Integer;
// -- Cadmensky Version 2.3.2
var
  iI: Integer;
  sTN: AnsiString;
begin
// ++ Cadmensky Version 2.3.2
  RESULT := false;
// -- Cadmensky Version 2.3.2

// ++ Commented Cadmensky Version 2.3.2
{   RESULT := ci_All_Ok;
   bFieldIsPresent := FALSE;

   if not SELF.TableExists (sTableName) then
   begin
      RESULT := ci_TableNotExists;
      EXIT;
   end;

   sTN := sTableName;
   if SELF.ExecQuery.Active then ExecQuery.Close;
   sTN := '[' + sTN + ']';
   ExecQuery.SQL.Clear;
   ExecQuery.SQL.Add ('SELECT * FROM ' + sTN);
   try
      ExecQuery.Open;
   except
      on E: EDBEngineError do
      begin
         for iI := 0 to E.ErrorCount - 1 do
            // This error was caused because there is noone field in a table. All is OK.
            if E.Errors[iI].NativeError = ci_ThereIsNoFieldInTable then EXIT;
         // And thi is unknow error (general SQL). We show the error's message to user and exit from the function.
         RESULT := ci_GeneralSQLError;
         MessageBox (Application.Handle, PChar (E.Message), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
         EXIT;
      end;
   end;
   // The table was opened without any error and we try to find field with required name in the table.}
// -- Commented Cadmensky Version 2.3.2
  sFieldName := AnsiUpperCase(sFieldName);
  for iI := 0 to ExecQuery.Fields.Count - 1 do
    if AnsiUpperCase(ExecQuery.Fields[iI].FieldName) = sFieldName then
    begin
// ++ Cadmensky Version 2.3.2
      RESULT := TRUE;
// -- Cadmensky Version 2.3.2
// ++ Commented Cadmensky Version 2.3.2
{         bFieldIsPresent := TRUE;
         ExecQuery.Close;}
// -- Commented Cadmensky Version 2.3.2
      EXIT;
    end;
// ++ Commented Cadmensky Version 2.3.2
//   ExecQuery.Close;
// -- Commented Cadmensky Version 2.3.2
end;

procedure TIDB_DatabaseItemsManager.AddField(sTableName, sFieldName: AnsiString; iFieldType, iFieldSize: Integer);
var
  sFieldType: AnsiString;
begin
  if (iFieldType = 1) and (iFieldSize > 255) then
    iFieldsize := 255;
  try
    case iFieldType of
      5: sFieldType := 'LOGICAL'; // Boolean
      6:
        if iFieldSize = 4 then
          sFieldType := 'SINGLE' // Floating point 4 bytes
        else
          sFieldType := 'DOUBLE'; // Floating 8 bytes
      15: sFieldType := 'LONGBINARY';
    else
      sFieldType := GetTypeNameByNumber(iFieldType);
    end; // Case end
    if iFieldSize > -1 then
    begin
      if iFieldType = 1 then
        SELF.ExecQuery.ExecSQL('ALTER TABLE [' + sTableName + '] ADD COLUMN [' + sFieldName + '] ' + sFieldType + ' (' + IntToStr(iFieldSize) + ') NULL')
      else
        SELF.ExecQuery.ExecSQL('ALTER TABLE [' + sTableName + '] ADD COLUMN [' + sFieldName + '] ' + sFieldType + ' NULL');
    end
    else
      SELF.ExecQuery.ExecSQL('ALTER TABLE [' + sTableName + '] ADD COLUMN [' + sFieldName + '] ' + sFieldType + ' NULL');
  except
  end;
end;

procedure TIDB_DatabaseItemsManager.DeleteField(sTableName, sFieldName: AnsiString);
begin
  try // Before delete the field we need to delete index.
    DeleteIndex(sTableName, sFieldName);
  except
  end;
  try
    SELF.ExecQuery.ExecSQL('ALTER TABLE [' + sTableName + '] DROP COLUMN [' + sFieldName + ']');
  except
  end;
end;

procedure TIDB_DatabaseItemsManager.DeleteTempField(sTableName: AnsiString);
begin
  SELF.ExecQuery.ExecSQL('ALTER TABLE [' + sTableName + '] DROP COLUMN [' + cs_TempFieldName + ']');
end;

{ This function returns which format has a database file - MS Access 97 or MS Access 2000.
  To detect it I create a new table with autoincrement field in it then I add 2 new records.
  If the information in this autoincrement field equals zero, I can affirm that this file has
  MS Access 97 format. }

function TIDB_DatabaseItemsManager.CheckDBFileVersion: TDBVersion;

  procedure DeleteTempTable;
  begin
// ++ Cadmensky Version 2.2.9
    if TableExists(cs_IT_CheckVersionTable) then
// -- Cadmensky Version 2.2.9
      ExecQuery.ExecSQL('DROP TABLE ' + cs_IT_CheckVersionTable);
  end;

var
  ATable: TDTable;
  iI: Integer;
begin
  ATable := TDTable.Create(nil);
  ATable.Master := ExecQuery.Master;
  ATable.TableName := cs_IT_CheckVersionTable;

  DeleteTempTable;

  try
    ExecQuery.ExecSQL('CREATE TABLE ' + cs_IT_CheckVersionTable + ' (' +
      '_AutoIncField AutoIncrement, ' +
      '_TempValue Integer' +
      ')');
    ATable.Open;
    for iI := 0 to 1 do
    begin
      ATable.Insert;
      ATable.FieldByName('_TempValue').AsInteger := 0;
      ATable.Post;
    end; // For iI end
    if ATable.FieldByName('_AutoIncField').AsInteger <> 0 then
      RESULT := dbv2000
    else
      RESULT := dbv97;
    ATable.Close;
    ATable.Free;
    DeleteTempTable;
  except
    ATable.Close;
    ATable.Free;

    DeleteTempTable;
  end;
end;

procedure TIDB_DatabaseItemsManager.CreateLayerNamesSysTable;
begin
   // If the table is already exists to create it it is not necessary.
  try
      // Lengths of fields equal 64 because it is the max length of MSAccess field's name.
    SELF.ExecQuery.ExecSQL('CREATE TABLE [' + cs_IT_LayerNamesTable + '] ' + // '_WG_DT'
      '([INDEX] Integer, ' +
      '[TABLENAME] TEXT (64), ' +
      '[LAYERNAME] TEXT (64))');
  except
      // Error of creating a table ''%s''.
//        MessageBox(AWinGISMainForm.Handle, PChar(Format(ALangManager.GetValue(103), [cs_CommonDescTableName])), cs_ApplicationName, MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
  end;
end;

function TIDB_DatabaseItemsManager.CreateLinkTable(sSourceDatabaseFileName,
  sSourceTableName,
  sLinkName: AnsiString;
  iWorkMode: integer;
  sServerName: AnsiString = '';
  sUserID: AnsiString = '';
  sPassword: AnsiString = ''): Boolean;
var
// ++ Commented by Cadmensky Version 2.3.1
//   FConnection: _Connection;
// -- Commented by Cadmensky Version 2.3.1
  FCatalog: _Catalog;
  FTable: _Table;
  sValue: AnsiString;
begin
  RESULT := false;

  try
// ++ Commented by Cadmensky Version 2.3.1
{      FConnection := CoConnection.Create;
      try
         sConnection := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + SELF.ExecQuery.Master.DataSource;
         FConnection.Open ('Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + SELF.ExecQuery.Master.DataSource, '', '', 0);
      except
         exit;
      end;}
// -- Commented by Cadmensky Version 2.3.1
    FCatalog := CoCatalog.Create;
// ++ Commented by Cadmensky Version 2.3.1
    FCatalog.Set_ActiveConnection(SELF.ExecQuery.CentralData.ADO);
//      FCatalog.Set_ActiveConnection (FConnection);
// -- Commented by Cadmensky Version 2.3.1

// ++ Commented by Cadmensky Version 2.3.7
{      try
         FCatalog.Tables.Delete (sLinkName);
      except
      end;}
// -- Commented by Cadmensky Version 2.3.7

    FTable := CoTable.Create;
    FTable.Name := sLinkName;
    FTable.ParentCatalog := FCatalog; // Set the properties to create the linkend;
    FTable.Properties.Item[8].Set_Value(true); //'Create Link'
    case iWorkMode of
      2 {wmAccessTable}: FTable.Properties.Item[5].Set_Value(';Pwd=password');
      5 {wmParadox5Table}: FTable.Properties.Item[5].Set_Value('Paradox 5.x;HDR=NO;IMEX=2;');
      6 {wmParadox7Table}: FTable.Properties.Item[5].Set_Value('Paradox 7;HDR=NO;IMEX=2;');
      4 {wmDBase3Table}: FTable.Properties.Item[5].Set_Value('dBase III;HDR=NO;IMEX=2;');
      3 {wmDBase5Table}: FTable.Properties.Item[5].Set_Value('dBase 5.0;HDR=NO;IMEX=2;');
      8 {SQL Server}:
        begin
//         sValue := 'SQLOLEDB;Data Source=' + sServerName + ';Initial Catalog=' + sSourceDatabaseFileName + ';User ID=' + sUserID + ';Password=' + sPassword + ';';
          sValue := 'ODBC;DSN=SQL Server 7;UID=' + sUserID + ';PWD=' + sPassword + ';DATABASE=' + sSourceDatabaseFileName + ';';
          FTable.Properties.Item[5].Set_Value(sValue);
        end;
    end;

// ++ Commented by Cadmensky Version 2.3.1
{      if iWorkMode = 2 then
      begin}
// -- Commented by Cadmensky Version 2.3.1
    if iWorkMode = 8 then
    begin
//         FTable.Properties.Item[4].Set_Value (sSourceTableName);
      FTable.Properties.Item[4].Set_Value('dbo.' + sSourceTableName);
//         FTable.Properties.Item[6].Set_Value (sServerName);
    end
    else
    begin
         //'Jet OLEDB:Remote Table Name'
      FTable.Properties.Item[4].Set_Value(sSourceTableName);
         //'Jet OLEDB:Link Datasource'
      FTable.Properties.Item[6].Set_Value(sSourceDatabaseFileName);
    end;
// ++ Commented by Cadmensky Version 2.3.1
{      end
      else
      begin
         FTable.Properties.Item[4].Set_Value (ExtractFileName (sSourceDatabaseFileName));
         FTable.Properties.Item[6].Set_Value (ExtractFilePath (sSourceDatabaseFileName));
      end;}
// -- Commented by Cadmensky Version 2.3.1

      // Append the table to the collection
    try
      FCatalog.Tables.Append(FTable);
      RESULT := TRUE;
    except
    end;
  finally
  end;
end;

// ++ Cadmensky Version 2.3.1

procedure TIDB_DatabaseItemsManager.DeleteLinkTable(sLinkName: AnsiString);
var
  FCatalog: _Catalog;
begin
  FCatalog := CoCatalog.Create;
  FCatalog.Set_ActiveConnection(SELF.ExecQuery.CentralData.ADO);
  try
    FCatalog.Tables.Delete(sLinkName);
  except
  end;
end;
// ++ Cadmensky Version 2.3.1

procedure TIDB_DatabaseItemsManager.AlterField(sTableName, sFieldName: AnsiString; iFieldType, iFieldSize: Integer);
var
  sFieldType: AnsiString;
begin
  try
    case iFieldType of
      5: sFieldType := 'LOGICAL'; // Boolean
      6:
        if iFieldSize = 4 then
          sFieldType := 'SINGLE' // Floating point 4 bytes
        else
          sFieldType := 'DOUBLE'; // Floating 8 bytes
      15: sFieldType := 'LONGBINARY';
    else
      sFieldType := GetTypeNameByNumber(iFieldType);
    end; // Case end
    if iFieldSize > -1 then
    begin
      if iFieldType = 1 then
        SELF.ExecQuery.ExecSQL('ALTER TABLE [' + sTableName + '] ALTER COLUMN [' + sFieldName + '] ' + sFieldType + ' (' + IntToStr(iFieldSize) + ') NULL')
      else
        SELF.ExecQuery.ExecSQL('ALTER TABLE [' + sTableName + '] ALTER COLUMN [' + sFieldName + '] ' + sFieldType + ' NULL');
    end
    else
      SELF.ExecQuery.ExecSQL('ALTER TABLE [' + sTableName + '] ALTER COLUMN [' + sFieldName + '] ' + sFieldType + ' NULL');
  except
  end;
end;

function TIDB_DatabaseItemsManager.CheckGUIDField(sTableName: AnsiString): Boolean;
var
  s: AnsiString;
begin
  Result := True;
  try
    AddField(sTableName, 'GUID', 1, 50);
  except
    Result := False;
  end;
end;

end.

