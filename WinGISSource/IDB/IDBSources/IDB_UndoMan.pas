Unit IDB_UndoMan;
{
Internal database. ver. II.
Undo manager.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 16-02-2001

It makes the following operations:
     - un-insert of new object (184),
     - un-delete of all objects (include layers) (185),
     - un-change of object properties (I mean properties that can be changed through a dialog
       that appers on mouse right-click or were changed through DDE-command [SOS]) (186),
     - un-move of any object (187),
     - un-size of any object (188),
     - un-transform of images (189).
}
Interface

Uses Classes,
     DBTables,
     DTables, DMaster,
     IDB_Access2DB, IDB_CallBacksDef;

function GenerateUniqueName(const Path, Prefix, Extension: AnsiString): string;

Type
   { This is an object that provided mapping of symbol to be restored.
     This mapper belongs a project. Each project has according mapper.
     Why it is needed? That's why:
     When we restore a symbol from EXTERNAL symbol library there can be
     the following situation when an user has switched off required library.
     In this case I shall restore the symbol into the project library but
     in the library symbol with such name can already exist. I suppose to do
     the following way: I need the mapper; I shall always to know what name
     the symbol has to have and what name it has indeed.
     Information about symbols is entered when these symbols are deleted from
     external symbol library and put into project library.
     When I create new symbol in external or project library (it doesn't matter)
     I always check information in this mapper. }
   PrecSymbolNamesMap = ^TrecSymbolNamesMap;
   TrecSymbolNamesMap = Record
         sSymbolLibraryName,
         sSymbolVirtualName,
         sSymbolActualName: AnsiString;
   End;

   TIDB_SymbolRemapperManager = Class
   Private
      listNames: TList;
      Function FindMap(sSymbolLibraryName, sSymbolName: AnsiString): Integer;
   Public
      Constructor Create;
      Destructor Free;

      Procedure ClearMapData;
      Procedure AddSymbolName(sSymbolLibraryName, sSymbolName, sActualSymbolName: AnsiString);
      Function GetSymbolActualName(sSymbolLibraryName, sSymbolName: AnsiString): AnsiString;
   End;

   TIDB_UndoManager = Class
   Private
      AProject: Pointer;
//      IDA: TIDB_Access2DB_Level2;

      UndoStream: TMemoryStream;

      AIDB_SymNameMapMan: TIDB_SymbolRemapperManager;
      bThereAreSomeProblemsWithUndoTable: Boolean;
//      bIAmReadyToReceiveUndoData: Boolean;
      iCurrentUndoObjectCount, // Count of objects in Undo stream. It is increased when new object is added.
                               // It is set as zero value when undo data are stored (SaveUndoData procedure is called).
      iTheTypeOfTheOneObject,
      iUndoStepCount: Integer;
      iHideObjectsCount: integer;
      iMaxUndoStreamSizeConst: integer;
      bWeAreUsingUndoFile: boolean;
      bDataForSavingInFile: boolean;

      sUndoFileName: AnsiString;
      sUndoDataFileName: AnsiString;

      ADataForSaving: AnsiString;
//      Procedure CheckTableStructure;
   Public
      Constructor Create(AProject: Pointer; ADataAccessObject: TIDB_Access2DB_Level2);
      Destructor Free;

      Procedure ClearUndoData;
      Procedure GetReadyToReceiveUndoData;
      Function PutDataInUndo(AData: Pointer; iSize: Integer): Boolean;
      Procedure SaveUndoData(AnUndoType: TUndoType);

      Function ThereAreDataForUndoing: Boolean;
      Function GetNextUndoDescription: AnsiString;
      Function GetUndoData(Var ABuffer: Pointer; Var ASize: Integer; Var AnUndoType: TUndoType): Boolean;

      procedure OnChangeUndoStepsCount(iNewUndoStepsCount: integer);

      Procedure AddSymbolMappingEntry(sSymLibName, sSymName, sNewSymName: AnsiString);
      Function SymbolNameExists(sSymbolLibraryName, sSymbolName: AnsiString): Boolean;

      Procedure AddSymbolName(sSymbolLibraryName, sSymbolName, sActualSymbolName: AnsiString);
      Function GetSymbolActualName(sSymbolLibraryName, sSymbolName: AnsiString): AnsiString;
   End;

Implementation

Uses Windows, Controls, DB, SysUtils, Dialogs, Forms,
     DDB,
     IDB_LanguageManager, IDB_Consts;

function GenerateUniqueName(const Path, Prefix, Extension: AnsiString): string;

  function IntToBase32(Number: Integer): AnsiString;
  const
    Table: array[0..31] of Char = '0123456789ABCDEFGHIJKLMNOPQRSTUV';
  var
    I: Integer;
  begin
    Result := '';
    for I := 0 to 4 do
    begin
      Insert(Table[Number and 31], Result, 1);
      Number := Number shr 5;
    end;
  end;

var
  Rand, Len: Longint;
  Path1, Pre1, Ext1: AnsiString;
const
  DefExt = '.TMP';
  DefPre = 'JHC-'; // I alone know what it is
begin
  Path1 := Path;
  if Path1 = '' then
  begin
    SetLength(Path1, 256);
    Len := GetTempPath(255, PChar(Path1));
    if Len = 0 then
      Len := GetSystemDirectory(PChar(Path1), 255);
    SetLength(Path1, Len);
  end;
  if Path1[Length(Path1)] <> '\' then
    Path1 := Path1 + '\';

  Pre1 := Prefix;
  if Pre1 = '' then
    Pre1 := DefPre;

  Ext1 := ExtractFileExt(Extension);
  if Ext1 = '' then
  begin
    if Extension <> '' then
      Ext1 := '.' + Extension
    else
      Ext1 := DefExt;
  end;

  Rand := Random($2000000);
  Path1 := Path1 + Pre1;
  repeat
    Inc(Rand);
    if Rand > $1FFFFFF then Rand := 0;
    { Generate a random name }
    Result := Path1 + IntToBase32(Rand) + Ext1;
  until not FileExists(Result);
end;

//////////////////    R E M A P P E R     M A N A G E R    /////////////////////

Constructor TIDB_SymbolRemapperManager.Create;
Begin
     SELF.listNames := TList.Create;
End;

Destructor TIDB_SymbolRemapperManager.Free;
Begin
     SELF.ClearMapData;
     listNames.Free;
End;

Procedure TIDB_SymbolRemapperManager.ClearMapData;
Var
   iI: Integer;
   pSNM: PrecSymbolNamesMap;
Begin
     For iI := 0 To SELF.listNames.Count - 1 Do Begin
         pSNM := listNames[iI];
         pSNM.sSymbolLibraryName := '';
         pSNM.sSymbolVirtualName := '';
         pSNM.sSymbolActualName := '';
         Dispose(pSNM);
         End;
     listNames.Clear;
End;

{ This function checks whteher an etry for a symbol exists. The function returns index of this entry
  if it exists or -1 otherwise. }
Function TIDB_SymbolRemapperManager.FindMap(sSymbolLibraryName, sSymbolName: AnsiString): Integer;
Var
   iP: Integer;
   pSNM: PrecSymbolNamesMap;
Begin
     RESULT := -1;
     sSymbolLibraryName := AnsiUpperCase(sSymbolLibraryName);
     sSymbolName := AnsiUppercase(sSymbolName);
     For iP := 0 To listNames.Count - 1 Do Begin
         pSNM := listNames[iP];
         If (pSNM.sSymbolLibraryName = sSymbolLibraryName)
            And
            (pSNM.sSymbolVirtualName = sSymbolName) Then Begin
               RESULT := iP;
               EXIT;
               End;
         End; // For iP end
End;

{ This function seraches and returns actual name for the symbol. We give it
  the restored symbol's name. If such name already exists in the mapper,
  the function returns it, otherwise it returns a string with zero length.}
Function TIDB_SymbolRemapperManager.GetSymbolActualName(sSymbolLibraryName, sSymbolName: AnsiString): AnsiString;
Var
   iP: Integer;
   pSNM: PrecSymbolNamesMap;
Begin
     RESULT := '';
     sSymbolLibraryName := AnsiUpperCase(sSymbolLibraryName);
     sSymbolName := AnsiUpperCase(sSymbolName);
     iP := SELF.FindMap(sSymbolLibraryName, sSymbolName);
     If iP > -1 Then Begin
        pSNM := listNames[iP];
        RESULT := pSNM.sSymbolActualName;
        End;
End;

{ This function checks whether an entry for a symbol exists and adds new entry for the symbol
  if the entry doesn't exist. This function is called during delete operation. }
Procedure TIDB_SymbolRemapperManager.AddSymbolName(sSymbolLibraryName, sSymbolName, sActualSymbolName: AnsiString);
Var
   iP: Integer;
   pSNM: PrecSymbolNamesMap;
   sSymLibUpperName, sSymUpperName: AnsiString;
Begin
     sSymLibUpperName := AnsiUpperCase(sSymbolLibraryName);
     sSymUpperName := AnsiUpperCase(sSymbolName);
     iP := SELF.FindMap(sSymLibUpperName, sSymUpperName);
     If iP < 0 Then
        New(pSNM)
     Else
        pSNM := listNames[iP];
     pSNM.sSymbolLibraryName := sSymLibUpperName;
     pSNM.sSymbolVirtualName := sSymUpperName;
     pSNM.sSymbolActualName := sActualSymbolName;
     If iP < 0 Then listNames.Add(pSNM);
End;

//------------------------------------------------------------------------------
//------------------    U N D O     M A N A G E R    ---------------------------
//------------------------------------------------------------------------------

Constructor TIDB_UndoManager.Create(AProject: Pointer; ADataAccessObject: TIDB_Access2DB_Level2);
begin
     Self.AProject := AProject;

     bWeAreUsingUndoFile := false;
     bDataForSavingInFile := false;

     iMaxUndoStreamSizeConst := GetProjectFileSizeFunc (AProject) div 10;
     if iMaxUndoStreamSizeConst = 0 then
        iMaxUndoStreamSizeConst := 1024;
     sUndoFileName := GenerateUniqueName ('', '', '');

     if not bWeAreUsingUndoFile then
        UndoStream := TMemoryStream.Create;

     ClearUndoData;
     SELF.iUndoStepCount := 0;
     SELF.ADataForSaving := '';
     SELF.iCurrentUndoObjectCount := 0;

     SELF.AIDB_SymNameMapMan := TIDB_SymbolRemapperManager.Create;
End;

Destructor TIDB_UndoManager.Free;
Begin
     SELF.ClearUndoData;
     SELF.AIDB_SymNameMapMan.Free;
     if UndoStream <> nil then
       UndoStream.Free;
End;

Procedure TIDB_UndoManager.ClearUndoData;
begin
     if bWeAreUsingUndoFile then
        DeleteFile (sUndoFileName)
     else
        UndoStream.Clear;

     SELF.ADataForSaving := '';
     SELF.bDataForSavingInFile := false;
     SELF.sUndoDataFileName := '';
     SELF.iUndoStepCount := 0;
End;

{ This procedure clears te buffer for the storing undo data in it. }
Procedure TIDB_UndoManager.GetReadyToReceiveUndoData;
Begin
     If bThereAreSomeProblemsWithUndoTable Then EXIT;
     iCurrentUndoObjectCount := 0;
     ADataForSaving := '';
End;

{ This function adds data to be stored for undo to the undo data buffer. }
Function TIDB_UndoManager.PutDataInUndo(AData: Pointer; iSize: Integer): Boolean;
Var
   iLen: Integer;
   ibObjectType: Byte;
   iObjectTypeHeaderLength: Integer;
   UndoDataFile: TFileStream;
Begin
     { A notice: Below I used SizeOf(Byte) instead of 1 to avoid the situation when
       the length of the header of stream, which contains type of object in the stream,
       will be changed. Then I'll simply change the type of ibObjectType variable. }

     // The first byte of the stream is the type of the object that was put into the stream.
     iObjectTypeHeaderLength := SizeOf(ibObjectType);
     CopyMemory(@ibObjectType, AData, iObjectTypeHeaderLength);

     // If there is the one object in UNDO stream, store the type of this object.
     if iCurrentUndoObjectCount < 1 then
        iTheTypeOfTheOneObject := ibObjectType;

     if (Length (ADataForSaving) + iSize > iMaxUndoStreamSizeConst)
         or bDataForSavingInFile then
     begin
        if not bDataForSavingInFile then
           sUndoDataFileName := GenerateUniqueName ('', '', '');
        bDataForSavingInFile := true;
        if FileExists (sUndoDataFileName) then
           UndoDataFile := TFileStream.Create (sUndoDataFileName, fmOpenReadWrite)
        else
        begin
           UndoDataFile := TFileStream.Create (sUndoDataFileName, fmCreate);
           UndoDataFile.Free;
           UndoDataFile := TFileStream.Create (sUndoDataFileName, fmOpenReadWrite);
        end;

        UndoDataFile.Position := UndoDataFile.Size;

        UndoDataFile.Write (Pointer(PChar(AData) + iObjectTypeHeaderLength)^, iSize - iObjectTypeHeaderLength);
        UndoDataFile.Free;
     end
     else
     begin
        iLen := Length(ADataForSaving);
        SetLength(ADataForSaving, iLen + iSize - iObjectTypeHeaderLength);
        CopyMemory(@ADataForSaving[iLen+1], PChar(AData) + iObjectTypeHeaderLength, iSize - iObjectTypeHeaderLength);
     end;

     Inc(iCurrentUndoObjectCount);
     RESULT := TRUE;
End;

{ And this procedure stores data into database's table. }
Procedure TIDB_UndoManager.SaveUndoData(AnUndoType: TUndoType);
var iDataLength: Integer;
    UndoFile, UndoDataFile: TFileStream;
    iUndoType, iUndoObjectCount: integer;
    iDataBlockLength: integer;
    pBuff: Pointer;
    iBufferSize: integer;
    i: integer;
begin
   iUndoType := integer(Ord(AnUndoType));
   // Save the number of objects in the stream. To understand how this value is created and interpreted,
   // see the comments to procedure TIDB_UndoManager.SaveUndoData in IDB_UndoMan.pas.
   if iCurrentUndoObjectCount > 1 then
      iUndoObjectCount := -iTheTypeOfTheOneObject
   else
      iUndoObjectCount := iCurrentUndoObjectCount;
   // Save the data.

   if bDataForSavingInFile then
   begin
      UndoDataFile := TFileStream.Create (sUndoDataFileName, fmOpenReadWrite);
      iDataLength := Length(ADataForSaving) + UndoDataFile.Size;
      UndoDataFile.Free;
   end
   else
      iDataLength := Length(ADataForSaving);
   iDataBlockLength := iDataLength + sizeof (integer) * 3;

   if bWeAreUsingUndoFile then
      try
         if FileExists (sUndoFileName) then
            UndoFile := TFileStream.Create (sUndoFileName, fmOpenReadWrite)
         else
         begin
            UndoFile := TFileStream.Create (sUndoFileName, fmCreate);
            UndoFile.Free;
            UndoFile := TFileStream.Create (sUndoFileName, fmOpenReadWrite);
         end;

         if Self.iUndoStepCount = GetProjectUndoStepsCountFunc (AProject) then
         begin
            Inc (Self.iHideObjectsCount);
            Dec (Self.iUndoStepCount);
         end;

         UndoFile.Position := UndoFile.Size;

         UndoFile.Write (iUndoType, sizeof (integer));
         UndoFile.Write (iUndoObjectCount, sizeof (integer));
         if bDataForSavingInFile then
         begin
            UndoDataFile := TFileStream.Create (sUndoDataFileName, fmOpenReadWrite);

            UndoFile.Write (Pointer(ADataForSaving)^, Length (ADataForSaving));

            for i := 0 to UndoDataFile.Size div iMaxUndoStreamSizeConst do
            begin
               if (i + 1) * iMaxUndoStreamSizeConst < UndoDataFile.Size then
                  iBufferSize := iMaxUndoStreamSizeConst
               else
                  iBufferSize := UndoDataFile.Size - i * iMaxUndoStreamSizeConst;
               GetMem(pBuff, iBufferSize);
               UndoDataFile.Read (pBuff^, iBufferSize);
               UndoFile.Write (Pointer (pBuff)^, iBufferSize);
               FreeMem (pBuff);
            end;
            UndoDataFile.Free;
            DeleteFile (sUndoDataFileName);
            bDataForSavingInFile := false;
            sUndoDataFileName := '';
         end
         else
         begin
            UndoFile.Write (Pointer(ADataForSaving)^, iDataLength);
         end;
         UndoFile.Write (iDataBlockLength, sizeof (integer));

         Inc (SELF.iUndoStepCount);
      finally
         UndoFile.Free;
         SELF.GetReadyToReceiveUndoData;
      end
   else
   begin
      if UndoStream.Size + iDataBlockLength >= iMaxUndoStreamSizeConst then
         try
            UndoStream.SaveToFile (sUndoFileName);
            UndoStream.Free;
            Self.bWeAreUsingUndoFile := true;
            Self.SaveUndoData (AnUndoType);
            exit;
         except
         end;

      if Self.iUndoStepCount = GetProjectUndoStepsCountFunc (AProject) then
      begin
{         UndoStream.Position := UndoStream.Size;
         for i := 1 to Self.iUndoStepCount - 1 do
         begin
            UndoStream.Seek (-sizeof (integer), soFromCurrent);
            UndoStream.Read (Pointer (iDataLength), sizeof (integer));
            UndoStream.Seek (-iDataLength, soFromCurrent);
         end;
//        UndoStream.Seek (iDataLength - sizeof (integer), soFromCurrent);
         iBufferSize := UndoStream.Size - UndoStream.Position;
         GetMem(pBuff, iBufferSize);
         UndoStream.Read (pBuff^, iBufferSize);
         UndoStream.Free;
         UndoStream := TMemoryStream.Create;
         UndoStream.Write (pBuff^, iBufferSize);
         FreeMem (pBuff);}
//        Inc (Self.iUndoTopPointer, iDataLength);
         Inc (Self.iHideObjectsCount);
         Dec (Self.iUndoStepCount);
//        UndoFile.Free;
//        SaveUndoData (AnUndoType);
//        exit;
      end;
      try
         UndoStream.Position := UndoStream.Size;

         UndoStream.Write (iUndoType, sizeof (integer));
         UndoStream.Write (iUndoObjectCount, sizeof (integer));
         UndoStream.Write (Pointer(ADataForSaving)^, iDataLength);
         UndoStream.Write (iDataBlockLength, sizeof (integer));


         Inc (SELF.iUndoStepCount);
      finally
         SELF.GetReadyToReceiveUndoData;
      end;
   end;
End;

{ This function returns TRUE if there are data to restore operation.
  The user interface of WinGIS asks about this information to set buttons in Enabled
  or Disabled state. }
Function TIDB_UndoManager.ThereAreDataForUndoing: Boolean;
Begin
     If bThereAreSomeProblemsWithUndoTable Then
        RESULT := FALSE
     Else
        RESULT := SELF.iUndoStepCount > 0;
End;

Function TIDB_UndoManager.GetNextUndoDescription: AnsiString;
var iNextUndoType,
    iUndoObjectCount: Integer;
    iLength: integer;
    UndoFile: TFileStream;
begin
     RESULT := '';
     iNextUndoType := Ord(utUnknownUNDO);
     iUndoObjectCount := 0;
     if bWeAreUsingUndoFile then
     begin
        try
           UndoFile := TFileStream.Create (sUndoFileName, fmOpenReadWrite);
        except
           bThereAreSomeProblemsWithUndoTable := true;
           iUndoStepCount := 0;
           exit;
        end;

        if UndoFile.Size = 0 then
           exit;

        try
           UndoFile.Seek (-sizeof (integer), soFromEnd);
           UndoFile.Read (Pointer (iLength), sizeof (integer));
           UndoFile.Seek (-iLength, soFromEnd);

           UndoFile.Read (Pointer (iNextUndoType), sizeof (integer));
           UndoFile.Read (Pointer (iUndoObjectCount), sizeof (integer));
        finally
           UndoFile.Free;
        end;
     end
     else
     begin
        try
           UndoStream.Seek (-sizeof (integer), soFromEnd);
           UndoStream.Read (Pointer (iLength), sizeof (integer));
           UndoStream.Seek (-iLength, soFromEnd);

           UndoStream.Read (Pointer (iNextUndoType), sizeof (integer));
           UndoStream.Read (Pointer (iUndoObjectCount), sizeof (integer));
        except
           exit;
        end;
     end;

     Case TUndoType(iNextUndoType) Of
     utUnInsert: RESULT := ALangManager.GetValue(184);
     utUnDelete: RESULT := ALangManager.GetValue(185);
     utUnChange: RESULT := ALangManager.GetValue(186);
     utUnMove: RESULT := ALangManager.GetValue(187);
     utUnResize: RESULT := ALangManager.GetValue(188);
     utUnTransform: RESULT := ALangManager.GetValue(189);
     Else RESULT := ALangManager.GetValue(190); // 'Unknown UNDO' will be returned.
     End; // Case end

     // If the number of objects is a negative value, it means that there is the one object
     // in UNDO stream and this value is the type of this object.
     If iUndoObjectCount < 0 Then Begin
        Case iUndoObjectCount Of
        -1:  RESULT := RESULT + ALangManager.GetValue(282); // ' of pixel'             - TPixel
        -2:  RESULT := RESULT + ALangManager.GetValue(283); // ' of polyline'          - TPoly
        -4:  RESULT := RESULT + ALangManager.GetValue(284); // ' of polygon'           - TCPoly
        -5:  RESULT := RESULT + ALangManager.GetValue(285); // ' of group'             - TGroup
        -7:  RESULT := RESULT + ALangManager.GetValue(286); // ' of text'              - TText
        -8:  RESULT := RESULT + ALangManager.GetValue(287); // ' of Symbol'            - TSymbol
        -9:  RESULT := RESULT + ALangManager.GetValue(288); // ' of spline'            - TSpline
        -10: RESULT := RESULT + ALangManager.GetValue(289); // ' of image'            - TImage
        -11: RESULT := RESULT + ALangManager.GetValue(290); // ' of measure line'     - TMesLine
        -12: RESULT := RESULT + ALangManager.GetValue(291); // ' of circle'           - TCircle
        -13: RESULT := RESULT + ALangManager.GetValue(292); // ' of arc'              - TArc
        -14: RESULT := RESULT + ALangManager.GetValue(293); // ' of OLE object'       - TOLEObj
        -16: RESULT := RESULT + ALangManager.GetValue(294); // ' of text'             - TRText
        -17: RESULT := RESULT + ALangManager.GetValue(295); // ' of business graphic' - TBusGraph';
        -18: RESULT := RESULT + ALangManager.GetValue(296); // ' of layer'            - TLayer';
        End; // Case end
        End
     Else
     // If the number of objects in the next undo is more than 1, this number will be added to description.
        If iUndoObjectCount > 1 Then
           RESULT := RESULT + ' (' + IntToStr(iUndoObjectCount) + ')';
End;

{Procedure TIDB_UndoManager.CheckTableStructure;
Var
   bFieldIsPresented: Boolean;
Begin
     // Check undo data field.
     If SELF.IDA.ADbItemsMan.FieldExists(cs_IT_UndoTableName, 'UndoData', bFieldIsPresented) <> ci_All_Ok Then Begin
        bThereAreSomeProblemsWithUndoTable := TRUE;
        EXIT;
        End;
     If NOT bFieldIsPresented Then
        SELF.IDA.ADbItemsMan.AddField(cs_IT_UndoTableName, 'UndoData', 15, -1); // 15 = Blob type

     // Check undo type field.
     If SELF.IDA.ADbItemsMan.FieldExists(cs_IT_UndoTableName, 'UndoType', bFieldIsPresented) <> ci_All_Ok Then Begin
        bThereAreSomeProblemsWithUndoTable := TRUE;
        EXIT;
        End;
     If NOT bFieldIsPresented Then
        SELF.IDA.ADbItemsMan.AddField(cs_IT_UndoTableName, 'UndoType', 12, -1); // 12 = Byte type

     // Check undo object count field.
     If SELF.IDA.ADbItemsMan.FieldExists(cs_IT_UndoTableName, 'UndoObjectCount', bFieldIsPresented) <> ci_All_Ok Then Begin
        bThereAreSomeProblemsWithUndoTable := TRUE;
        EXIT;
        End;
     If NOT bFieldIsPresented Then
        SELF.IDA.ADbItemsMan.AddField(cs_IT_UndoTableName, 'UndoObjectCount', 3, -1); // 3 = Integer type

     // Check internal ID field,
     If SELF.IDA.ADbItemsMan.FieldExists(cs_IT_UndoTableName, cs_InternalID_FieldName, bFieldIsPresented) <> ci_All_Ok Then Begin
        bThereAreSomeProblemsWithUndoTable := TRUE;
        EXIT;
        End;
     If NOT bFieldIsPresented Then
        SELF.IDA.ADbItemsMan.AddField(cs_IT_UndoTableName, cs_InternalID_FieldName, 14, -1); // 14 = AutoIncrement type
End;}

function TIDB_UndoManager.GetUndoData(var ABuffer: Pointer; var ASize: Integer; var AnUndoType: TUndoType): Boolean;
var iLength: Integer;
    pBuff: Pointer;
    UndoFile: TFileStream;
    iUndoType, iUndoObjectCount: integer;
begin

   RESULT := FALSE;
   ABuffer := nil;
   ASize := 0;
   AnUndoType := utUnknownUNDO;

   if bThereAreSomeProblemsWithUndoTable then
      exit;

   if bWeAreUsingUndoFile then
   begin
      try
         UndoFile := TFileStream.Create (sUndoFileName, fmOpenReadWrite);
      except
         bThereAreSomeProblemsWithUndoTable := true;
         iUndoStepCount := 0;
         exit;
      end;

      if UndoFile.Size = 0 then
         exit;

      try
         UndoFile.Seek (-sizeof (integer), soFromEnd);
         UndoFile.Read (Pointer (iLength), sizeof (integer));
         UndoFile.Seek (-iLength, soFromEnd);

         iLength := iLength - sizeof (integer) * 3;
         GetMem(pBuff, iLength);
         UndoFile.Read (Pointer (iUndoType), sizeof (integer));
         UndoFile.Read (Pointer (iUndoObjectCount), sizeof (integer));
         UndoFile.Read (pBuff^, iLength);
         UndoFile.Seek (-iLength - sizeof (integer) * 3, soFromEnd);
         UndoFile.Size := UndoFile.Position;
         Dec (iUndoStepCount);
      finally
            if UndoFile.Size < iMaxUndoStreamSizeConst then
            begin
               UndoFile.Free;
               UndoStream := TMemoryStream.Create;
               UndoStream.LoadFromFile (sUndoFileName);
               try
                  DeleteFile (sUndoFileName);
               except
               end;
               Self.bWeAreUsingUndoFile := false;
            end
            else
               UndoFile.Free;
      end;
   end
   else
   begin
      try
      UndoStream.Seek (-sizeof (integer), soFromEnd);
      UndoStream.Read (Pointer (iLength), sizeof (integer));
      UndoStream.Seek (-iLength, soFromEnd);

      iLength := iLength - sizeof (integer) * 3;
      GetMem(pBuff, iLength);
      UndoStream.Read (Pointer (iUndoType), sizeof (integer));
      UndoStream.Read (Pointer (iUndoObjectCount), sizeof (integer));
      UndoStream.Read (pBuff^, iLength);
      UndoStream.Seek (-iLength - sizeof (integer) * 3, soFromEnd);
      UndoStream.Size := UndoStream.Position;
      Dec (iUndoStepCount);
      except
         exit;
      end;
   end;
   try
      AnUndoType := TUndoType (iUndoType);
   except
      AnUndoType := utUnknownUNDO;
   end;
   ABuffer := Pointer (pBuff);
   ASize := iLength;
   RESULT := TRUE;
end;

procedure TIDB_UndoManager.OnChangeUndoStepsCount(iNewUndoStepsCount: integer);
var iTotalUndoStepsCount: integer;
begin
   iTotalUndoStepsCount := Self.iUndoStepCount + Self.iHideObjectsCount;
   if iTotalUndoStepsCount > iNewUndoStepsCount then
   begin
      Self.iHideObjectsCount := iTotalUndoStepsCount - iNewUndoStepsCount;
      Self.iUndoStepCount := iNewUndoStepsCount;
   end
   else
   begin
      Self.iHideObjectsCount := 0;
      Self.iUndoStepCount := iTotalUndoStepsCount;
   end;
end;



{ This function adds new entry for symbol during delete operation of a symbol from
  external library. }
Procedure TIDB_UndoManager.AddSymbolMappingEntry(sSymLibName, sSymName, sNewSymName: AnsiString);
Begin
     SELF.AIDB_SymNameMapMan.AddSymbolName(sSymLibName, sSymName, sNewSymName);
End;

{ This function returns TRUE if information about a symbol already contained in the mapper. }
Function TIDB_UndoManager.SymbolNameExists(sSymbolLibraryName, sSymbolName: AnsiString): Boolean;
Begin
     RESULT := SELF.AIDB_SymNameMapMan.FindMap(sSymbolLibraryName, sSymbolName) > -1;
End;

Procedure TIDB_UndoManager.AddSymbolName(sSymbolLibraryName, sSymbolName, sActualSymbolName: AnsiString);
Begin
     SELF.AIDB_SymNameMapMan.AddSymbolName(sSymbolLibraryName, sSymbolName, sActualSymbolName);
End;

Function TIDB_UndoManager.GetSymbolActualName(sSymbolLibraryName, sSymbolName: AnsiString): AnsiString;
Begin
     RESULT := SELF.AIDB_SymNameMapMan.GetSymbolActualName(sSymbolLibraryName, sSymbolName);
End;

End.

