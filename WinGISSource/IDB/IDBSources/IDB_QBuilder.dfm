ÿ
 TIDB_QBFORM 0z+  TPF0TIDB_QBForm
IDB_QBFormLeftTop Anchors BorderStylebsSingleCaption!315ClientHeightÐClientWidthColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightõ	Font.NameMS Sans Serif
Font.Style 	FormStylefsStayOnTopOldCreateOrderPositionpoDesktopCenterScaledOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel2Left TopªWidthHeight&AlignalBottom
BevelOuterbvNoneTabOrder  TLabelLabel4LeftTop
WidthHeightCaption!273Visible  TButtonOKButtonLeftáTopWidthKHeightAnchorsakRightakBottom Caption!49EnabledModalResultTabOrder OnClickOKButtonClick  TButtonCancelButtonLeft1TopWidthKHeightAnchorsakRightakBottom Cancel	Caption!50ModalResultTabOrder   TExtPageControlExtPageControl1Left Top WidthHeightª
ActivePage	TabSheet4AlignalClientHottrack.TabInactiveColor	clBtnFace	TabHeight TabOrderTabWidth OnChangeExtPageControl1ChangeInitialPage TabActiveColor	clBtnFaceTabActiveFont.CharsetDEFAULT_CHARSETTabActiveFont.ColorclWindowTextTabActiveFont.HeightòTabActiveFont.NameMS Sans SerifTabActiveFont.Style TabInactiveColor	clBtnFaceTabInactiveFont.CharsetDEFAULT_CHARSETTabInactiveFont.ColorclWindowTextTabInactiveFont.HeightòTabInactiveFont.NameMS Sans SerifTabInactiveFont.Style  	TTabSheet	TabSheet1Caption!202 TSpeedButtonUpSBLeft)Top WidthHeight
Glyph.Data
z  v  BMv      v   (                                                    ÀÀÀ    ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ 333 333333333333	333333333333	333333333333	333333333333	333333333333	33333ÿÿó330 	  3338ó3303338?3383333	3333333333	3333ó333330333338ó8ó33330333338?8333333	333333333333	333333333333033333338ó3333330333333383333	NumGlyphsOnClick	UpSBClick  TSpeedButtonDownSBLeft)Top´ WidthHeight
Glyph.Data
z  v  BMv      v   (                                                    ÀÀÀ    ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ 333033333338ó333333033333338?333333	333333333333	333333ó33330333338ó8ó3333033333838?3333	3333333333	333333ó3303338ÿÿ?øó330 	  333833333	333333333333	333333333333	333333333333	333333333333	333333333333 333333333	NumGlyphsOnClickDownSBClick  TListBoxAvailableTableFieldsListBoxLeftTopWidthHeightAnchorsakLeftakTopakBottom 
ItemHeightSorted	TabOrder 
OnDblClick#AvailableTableFieldsListBoxDblClick  TListBoxSelectedTableFieldsListBoxLeftJTopWidth+HeightAnchorsakLeftakTopakRightakBottom 
ItemHeightTabOrder
OnDblClick"SelectedTableFieldsListBoxDblClick  TButtonButton1Left)Top(WidthHeightCaption>Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightñ	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickButton1Click  TButtonButton2Left)TopAWidthHeightCaption>>Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightñ	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickButton2Click  TButtonButton3Left)TopZWidthHeightCaption<Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightñ	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickButton3Click  TButtonButton4Left)TopsWidthHeightCaption<<Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightñ	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickButton4Click   	TTabSheet	TabSheet5Caption!202
ImageIndex TIDB_StringGridSGLeft TopWidthyHeightwAlignalClientColCountDefaultColWidthú DefaultRowHeight	FixedCols 	FixedRows OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoColSizing	goEditinggoAlwaysShowEditor TabOrderOnClickSGClick	OnMouseUp	SGMouseUpOnTopLeftChangedSGTopLeftChangedAllowMovingFromToTheFirstLine	AllowMovingFromToTheLastLine	  	TComboBox	ComboBox1LeftKTop  Width HeightStylecsDropDownList
ItemHeight Sorted	TabOrder OnChangeComboBox1Change  TBitBtnAButtonLeftú Top¢ WidthCHeightTabOrderOnClickAButtonClick
Glyph.Data
ú   ö   BMö       v   (                                                    ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ ffffffffffffffffffffffffffffffffffffffffffffffff`f`f`  f  f    f  f  `f`f`ffffffffffffffffffffffffffffffffffffffffffffffff  THeaderControlHeaderControl1Left Top WidthyHeightSections
AllowClickText!310Widthü  
AllowClickText!311Widthü   OnSectionResizeHeaderControl1SectionResize   	TTabSheet	TabSheet2Caption!203Enabled
ImageIndexOnShowTabSheet2Show TLabelLabel3TagÈ Left½Top$WidthHeightCaption!208Visible  TLabelLabel1TagdLeft½Top$WidthHeightCaption!207  TLabelLabel2TagÈ Left½TopMWidthHeightCaption!209Visible  TListBoxValuesFieldsListBoxLeftTop$Widthà Height Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder OnClickValuesFieldsListBoxClick  	TGroupBox	GroupBox1Leftõ TopWidthÀ Height Caption Conditions TabOrder TRadioButtonRadioButton1TagLeftTopWidth(HeightCaption=Checked	TabOrder TabStop	OnClickRadioButton1Click  TRadioButtonRadioButton2TagLeftTop-Width(HeightCaption<=TabOrderOnClickRadioButton1Click  TRadioButtonRadioButton3TagLeftTop_Width(HeightCaption<TabOrderOnClickRadioButton1Click  TRadioButtonRadioButton4TagLeftTopFWidth(HeightCaption>=TabOrderOnClickRadioButton1Click  TRadioButtonRadioButton5TagLeftTopxWidth(HeightCaption>TabOrderOnClickRadioButton1Click  TRadioButtonRadioButton6TagLeftWTopWidthDHeightCaptionLIKETabOrderOnClickRadioButton1Click  TRadioButtonRadioButton7TagLeftWTop-Width`HeightCaptionIS NULLTabOrderOnClickRadioButton1Click  TRadioButtonRadioButton9TagLeftWTopFWidth[HeightCaptionBETWEENTabOrderOnClickRadioButton1Click   	TCheckBox	CheckBox2LeftTop
Width HeightCaptionSELECT DISTINCTTabOrder  	TComboBoxBoolValComboBoxLeft¼Top3Width¸ HeightStylecsDropDownListFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ItemHeightItems.Strings!210!211 
ParentFontTabOrderVisible  TMemoMemo2LeftTopÜ Width3Height¨ AnchorsakLeftakTopakRightakBottom Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
ParentFont
ScrollBarsssBothTabOrder  TButtonAdd_NewStatement_ButtonLeft Top¾ Width\HeightCaption!324TabOrderOnClickAdd_NewStatement_ButtonClickOnEnterAdd_NewStatement_ButtonEnter  TButtonClearAll_ButtonLeftTop¾ Width\HeightCaption!327TabOrderOnClickClearAll_ButtonClick  TButtonAdd_AND_ButtonLeftõ Top¾ Width<HeightCaptionANDTabOrderOnClickAdd_AND_ButtonClick  TButtonAdd_OR_ButtonLeft1Top¾ Width<HeightCaptionORTabOrderOnClickAdd_OR_ButtonClick  TButtonAdd_L_ButtonLeftmTop¾ Width$HeightCaption(TabOrder	OnClickAdd_L_ButtonClick  TButtonAdd_R_ButtonLeftTop¾ Width$HeightCaption)TabOrder
OnClickAdd_R_ButtonClick  	TCheckBox	CheckBox1Leftö Top	WidthaHeightCaptionNOTEnabledTabOrder  	TComboBoxFirstComboBoxLeft¼Top3Width¸ Height
ItemHeight Sorted	TabOrderVisibleOnEnterFirstComboBoxEnterOnExitFirstComboBoxExit  	TComboBoxSecondComboBoxLeft¾TopaWidth¹ Height
ItemHeight Sorted	TabOrderOnEnterSecondComboBoxEnter
OnKeyPressSecondComboBoxKeyPress  	TCheckBox	CheckBox3Left¼TopWidth· HeightCaption!206TabOrderOnClickCheckBox3Click   	TTabSheet	TabSheet3Caption!204Enabled
ImageIndex TListBoxAvailableSelectedFieldsListBoxLeftTopWidth"HeightyAnchorsakLeftakTopakBottom Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder 
OnDblClick&AvailableSelectedFieldsListBoxDblClick  TListBoxOrdersFieldsListBoxLeftNTopWidth'HeightyAnchorsakLeftakTopakRightakBottom Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontTabOrder
OnDblClickOrdersFieldsListBoxDblClick  TButtonButton5Left/Top(WidthHeightCaption>Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightñ	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickButton5Click  TButtonButton6Left/TopAWidthHeightCaption>>Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightñ	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickButton6Click  TButtonButton7Left/TopZWidthHeightCaption<Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightñ	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickButton7Click  TButtonButton8Left/TopsWidthHeightCaption<<Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightñ	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickButton8Click   	TTabSheet	TabSheet4Caption!205Enabled
ImageIndexOnShowTabSheet4Show TMemoMemo1Left TopWidthyHeightoAlignalBottomColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	
ScrollBarsssBothTabOrder   	TCheckBox	CheckBox4LeftTopWidthå HeightCaption!2TabOrderOnClickCheckBox4Click  	TCheckBoxCheckBoxDuplicatesLeftTopWidthHeightCaptionFind duplicate recordsTabOrderOnClickTabSheet4Show   	TTabSheet	TabSheet6Caption!380
ImageIndexOnShowTabSheet6Show TIDB_DBGridIDB_DBGrid1Left Top WidthyHeightAlignalClientColor	clBtnFace
DataSourceDataSource1Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightõ	Font.NameMS Sans Serif
Font.Style OptionsdgTitlesdgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit 
ParentFontReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.HeightóTitleFont.NameMS Sans SerifTitleFont.Style OnCellClickIDB_DBGrid1CellClickOnDrawDataCellIDB_DBGrid1DrawDataCell	OnKeyDownIDB_DBGrid1KeyUpOnKeyUpIDB_DBGrid1KeyUpFixedColumnNamePROGISID    TDataSourceDataSource1DataSetDQuery1Left Top¬   TDQueryDQuery1	FieldDefs 
MaxRecords SubList 
UpdateMode
upWhereAllOptionsopDialogopFilteropConnection LeftrTop¬    