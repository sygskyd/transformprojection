unit IDB_ExternalTablesMan;
{
Internal database. Ver. II.
This class provides a control over linked external tables.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 23-08-2001
}
interface

uses Classes, Controls, Gauges,
   DDB, DTables, DB,
   IDB_Consts, IDB_Access2DB, ADOX_TLB, ADODB_TLB, StdCtrls, IDB_C_StringGrid;

type
   TDatabaseType = (dtUnknown, dtMSAccess, dtxBase, dtParadox, dtSQLServer);

   PrecExtTableLinkProps = ^TrecExtTableLinkProps;
   TrecExtTableLinkProps = record
      sLinkName: AnsiString;
      ATypeOfDatabase: TDatabaseType;
// ++ Cadmensky Version 2.3.4
      sServerName: AnsiString;
// -- Cadmensky Version 2.3.4
      sSourceDatabaseFileName: AnsiString;
      sUserName: AnsiString;
      sPassword: AnsiString;
      sSourceTableName: AnsiString;
      cpPage: TCodePage;
      bKeepLinkActive: Boolean;
      sProgisIDFieldInExtTable: AnsiString;
      sProgisIDFieldInDestinationTable: AnsiString;
      AFieldsList: TStringList;
      sSelectedFields: AnsiString;
      bLinkIsUsed: Boolean;
   end;

   PrecDataForUpdate = ^TrecDataForUpdate;
   TrecDataForUpdate = record
      sFieldName: AnsiString;
      bFieldHasStringType: Boolean;
      sDataForUpdate: AnsiString;
   end;

   TIDB_BaseExternalTablesMan = class
   private
      AProject: Pointer;
      ALayer: Pointer;
      ExtTablesList: TList;
// ++ Commented by Cadmensky Version 2.3.4
//      ExtTablesLinkOrder: TStringList;
//      sExtTablePoolFileName: AnsiString;
// -- Commented by Cadmensky Version 2.3.4
      sMainLayerTableName: AnsiString; // The name of the layer attribute table in IDB database file.

      IDA: TIDB_Access2DB_Level2;

      AnOldScreenCursor: TCursor;

      bIAmInUse: Boolean;
      bModified: Boolean;

      procedure SetScreenCursor;
      procedure RestoreScreenCursor;

      procedure SetModifiedSign;

      function FindLink (iLinkNum: Integer): PrecExtTableLinkProps; overload;
      function FindLink (sLinkName: AnsiString): PrecExtTableLinkProps; overload;
      procedure DeleteLink (iLinkNum: Integer); overload;
   public
      constructor Create (AProject, ALayer: Pointer; MainIDA: TIDB_Access2DB_Level2);
      destructor Free;

      procedure DropModifiedSign;

      property Layer: Pointer read ALayer;
      property Modified: Boolean read bModified;
      property IAmInUse: Boolean read bIAmInUse;

      procedure ClearData;

      function AddLink (sNewLinkName: AnsiString): AnsiString;
      procedure DeleteLink (sLinkName: AnsiString); overload;
      function LinkExists (sDatabaseFileName, sTableName: AnsiString): Boolean;

      function GenerateLinkName (sLinkName: AnsiString): AnsiString;

      function GetLinksCount: Integer;
      function GetUsedLinksCount: Integer;
      function GetLinkName (iLinkNum: Integer): AnsiString;
      function GetLinkType (sLinkName: AnsiString): TDatabaseType;
      function GetLinkDatabaseFileName (sLinkName: AnsiString): AnsiString;
      function GetLinkUserName (sLinkName: AnsiString): AnsiString;
      function GetLinkPassword (sLinkName: AnsiString): AnsiString;
      function GetLinkTableName (sLinkName: AnsiString): AnsiString;
      function GetLinkCodePage (sLinkName: AnsiString): TCodePage;
      function GetLinkKeepActive (sLinkName: AnsiString): Boolean;
      function GetLinkProgisIDFieldName (sLinkName: AnsiString): AnsiString;
      function GetLinkSelectedFields (sLinkName: AnsiString): AnsiString;
      function GetLinkIsUsedState (sLinkName: AnsiString): Boolean;
// ++ Cadmensky Version 2.3.4
      function GetLinkServerName (sLinkName: AnsiString): AnsiString;
      function GetLinkIntegerFieldNames (sLinkName: AnsiString): AnsiString;
// -- Cadmensky Version 2.3.4

      function GetLinkAllFieldNames (sLinkname: AnsiString): AnsiString;

      procedure SetLinkName (sOldLinkName: AnsiString; sValue: AnsiString);
      procedure SetLinkType (sLinkName: AnsiString; dtValue: TDatabaseType);
      procedure SetLinkDatabaseFileName (sLinkName: AnsiString; sValue: AnsiString);
      procedure SetLinkUserName (sLinkName: AnsiString; sValue: AnsiString);
      procedure SetLinkPassword (sLinkName: AnsiString; sValue: AnsiString);
      procedure SetLinkTableName (sLinkName: AnsiString; sValue: AnsiString);
      procedure SetLinkCodePage (sLinkName: AnsiString; cpValue: TCodePage);
      procedure SetLinkKeepActive (sLinkName: AnsiString; bValue: Boolean);
      procedure SetLinkProgisIDFieldName (sLinkName: AnsiString; sValue: AnsiString);
      procedure SetLinkSelectedFields (sLinkName: AnsiString; sValue: AnsiString);
      procedure SetLinkIsUsedState (sLinkName: AnsiString; bValue: Boolean);
// ++ Cadmensky Version 2.3.4
      procedure SetLinkServerName (sLinkName: AnsiString; sValue: AnsiString);
// -- Cadmensky Version 2.3.4

// ++ Commented by Cadmensky Version 2.3.4
//      procedure SetLinksOrder (sValue: AnsiString);
// -- Commented by Cadmensky Version 2.3.4
   end;

   TIDB_ExternalTablesMan = class (TIDB_BaseExternalTablesMan)
   private
      LinksForUpdate: TStringList;
      sJoinClauseText,
         sJoiningSQLText: AnsiString;
   public
      constructor Create (AProject, ALayer: Pointer; MainIDA: TIDB_Access2DB_Level2);
      destructor Free;

      procedure SaveData;
      function LoadData: Boolean;

      function MakeExtTablePool (bNeedToReconnect: Boolean; AGauge: TGauge = nil): Boolean;
      procedure SetParamsToExtTablePool;
      procedure Disconnect;

      procedure GetReadyToReceiveDataForUpdate;
      procedure ReceiveDataForUpdate (sFieldName, sData: AnsiString; dtFieldType: TFieldType);
      procedure ExecuteUpdate (iProgisIDValue: Integer);
// ++ Commented by Cadmensky Version 2.3.9
//      function SelectDistinctFieldsForProgisID (sFieldname: AnsiString; iProgisID: Integer): AnsiString;
// -- Commented by Cadmensky Version 2.3.9
      (*
      The first variant. Now it is replaced with 3 procedures above:
      1. GetReadyToReceiveDataForUpdate
      2. ReceiveDataForUpdate
      3. ExecuteUpdate

                procedure UpdateTableData(sProgisIDValue, sFieldName, sData: AnsiString);
      *)
      procedure ClearDataForUpdate;

      property JoinClause: AnsiString read sJoinClauseText;
      property JoiningSQL: AnsiString read sJoiningSQLText;
   end;

implementation

uses SysUtils, Forms, Windows,
   IDB_Utils, IDB_StructMan, IDB_CallbacksDef, IDB_LanguageManager, IDB_MainManager
   ;

constructor TIDB_BaseExternalTablesMan.Create (AProject, ALayer: Pointer; MainIDA: TIDB_Access2DB_Level2);
begin
   SELF.AProject := AProject;
   SELF.ALayer := ALayer;

   if not AnIDBMainMan.UseNewATTNames (AProject) then
      SELF.sMainLayerTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (SELF.ALayer))
   else
      SELF.sMainLayerTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, SELF.ALayer);

// ++ Commented by Cadmensky Version 2.3.4
//   SELF.sExtTablePoolFileName := '';
// -- Commented by Cadmensky Version 2.3.4
   SELF.ExtTablesList := TList.Create;
// ++ Commented by Cadmensky Version 2.3.4
//   SELF.ExtTablesLinkOrder := TStringList.Create;
// -- Commented by Cadmensky Version 2.3.4
   SELF.IDA := MainIDA;
   SELF.bIAmInUse := FALSE;

   SELF.DropModifiedSign;
end;

destructor TIDB_BaseExternalTablesMan.Free;
begin
   SELF.ClearData;
   SELF.ExtTablesList.Free;
// ++ Commented by Cadmensky Version 2.3.4
//   SELF.ExtTablesLinkOrder.Clear;
//   SELF.ExtTablesLinkOrder.Free;
// -- Commented by Cadmensky Version 2.3.4
end;

procedure TIDB_BaseExternalTablesMan.SetModifiedSign;
begin
   SELF.bModified := TRUE;
end;

procedure TIDB_BaseExternalTablesMan.DropModifiedSign;
begin
   SELF.bModified := FALSE;
end;

procedure TIDB_BaseExternalTablesMan.ClearData;
begin
   while SELF.ExtTablesList.Count > 0 do
      DeleteLink (0);
end;

procedure TIDB_BaseExternalTablesMan.SetScreenCursor;
begin
   SELF.AnOldScreenCursor := Screen.Cursor;
   Screen.Cursor := crHourGlass;
   Application.ProcessMessages;
   Application.HandleMessage;
end;

procedure TIDB_BaseExternalTablesMan.RestoreScreenCursor;
begin
   Screen.Cursor := SELF.AnOldScreenCursor;
end;

function TIDB_BaseExternalTablesMan.AddLink (sNewLinkName: AnsiString): AnsiString;
var
   ALink: PrecExtTableLinkProps;
begin
   New (ALink);
   ALink.sLinkName := GenerateLinkName (sNewLinkName); // to avoid some same names of links.
   ALink.ATypeOfDatabase := dtUnknown;
   ALink.sSourceDatabaseFileName := '';
   ALink.sUserName := '';
   ALink.sPassword := '';
   ALink.sSourceTableName := '';
   ALink.cpPage := cpANSI;
   ALink.bKeepLinkActive := FALSE;
   ALink.sProgisIDFieldInExtTable := 'ProgisID';
   ALink.sSelectedFields := '';
   ExtTablesList.Add (ALink);

   SELF.SetModifiedSign;
   RESULT := ALink.sLinkName;
end;

procedure TIDB_BaseExternalTablesMan.DeleteLink (iLinkNum: Integer);
var
   ALink: PrecExtTableLinkProps;
begin
   ALink := SELF.FindLink (iLinkNum);
   if ALink <> nil then
   begin
      try
         ALink.AFieldsList.Clear;
         ALink.AFieldsList.Free;
      except
      end;
      Dispose (ALink);
      ExtTablesList.Delete (iLinkNum);
   end;
end;

procedure TIDB_BaseExternalTablesMan.DeleteLink (sLinkName: AnsiString);
var
   ALink: PrecExtTableLinkProps;
   iP: Integer;
// ++ Commented by Cadmensky Version 2.3.1
//   FConnection: _Connection;
//   FCatalog: _Catalog;
// -- Commented by Cadmensky Version 2.3.1
begin
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
   begin

      iP := ExtTablesList.IndexOf (ALink);
      Dispose (ALink);
      ExtTablesList.Delete (iP);
      SELF.SetModifiedSign;

// ++ Cadmensky Version 2.3.1
      SELF.IDA.ADbItemsMan.DeleteLinkTable (sLinkName);
// -- Cadmensky Version 2.3.1

// ++ Commented by Cadmensky Version 2.3.1
{      FConnection := CoConnection.Create;
      try
         FConnection.Open ('Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + MakeDBFileNameFromProjectFileName (GetProjectFullFileNameFunc (SELF.AProject)), '', '', 0);
      except
         exit;
      end;
      FCatalog := CoCatalog.Create;
      FCatalog.Set_ActiveConnection (FConnection);

      try
         FCatalog.Tables.Delete (sLinkName);
      except
      end;}
// -- Commented by Cadmensky Version 2.3.1

   end;
end;

function TIDB_BaseExternalTablesMan.FindLink (iLinkNum: Integer): PrecExtTableLinkProps;
begin
   RESULT := nil;
   if (iLinkNum > -1) and (iLinkNum < SELF.ExtTablesList.Count) then
      RESULT := ExtTablesList[iLinkNum];
end;

function TIDB_BaseExternalTablesMan.FindLink (sLinkName: AnsiString): PrecExtTableLinkProps;
var
   ALink: PrecExtTableLinkProps;
   iI: Integer;
begin
   RESULT := nil;
   if Trim (sLinkName) <> '' then
      for iI := 0 to ExtTablesList.Count - 1 do
      begin
         ALink := ExtTablesList[iI];
         if ALink.sLinkName = sLinkName then
         begin
            RESULT := ALink;
            EXIT;
         end;
      end;
end;

function TIDB_BaseExternalTablesMan.GetLinksCount: Integer;
begin
   RESULT := SELF.ExtTablesList.Count;
end;

function TIDB_BaseExternalTablesMan.GetUsedLinksCount: Integer;
var
   ALink: PrecExtTableLinkProps;
   iI: Integer;
begin
   RESULT := 0;
   for iI := 0 to ExtTablesList.Count - 1 do
   begin
      ALink := ExtTablesList[iI];
      if ALink.bLinkIsUsed then
         Inc (RESULT);
   end;
end;

{ This function returns names of links not according to order in ExtTablesList
  but according to their visible order. This order is set by the user at the left
  part of "params" form with the help of red up and down arrows. }

function TIDB_BaseExternalTablesMan.GetLinkName (iLinkNum: Integer): AnsiString;
var
   ALink: PrecExtTableLinkProps;
begin
// ++ Cadmensky Version 2.3.4
   RESULT := '';
   ALink := SELF.FindLink (iLinkNum);
   if ALink <> nil then
      RESULT := ALink.sLinkName;
// -- Cadmensky Version 2.3.4
// ++ Commented by Cadmensky Version 2.3.4
{   try
      RESULT := ExtTablesLinkOrder[iLinkNum];
   except
      RESULT := '';
   end;}
// -- Commented by Cadmensky Version 2.3.4
end;

function TIDB_BaseExternalTablesMan.GetLinkType (sLinkName: AnsiString): TDatabaseType;
var
   ALink: PrecExtTableLinkProps;
begin
   RESULT := dtUnknown;
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
      RESULT := ALink.ATypeOfDatabase;
end;

function TIDB_BaseExternalTablesMan.GetLinkDatabaseFileName (sLinkName: AnsiString): AnsiString;
var
   ALink: PrecExtTableLinkProps;
begin
   RESULT := '';
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
      RESULT := ALink.sSourceDatabaseFileName;
end;

function TIDB_BaseExternalTablesMan.GetLinkUserName (sLinkName: AnsiString): AnsiString;
var
   ALink: PrecExtTableLinkProps;
begin
   RESULT := '';
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
      RESULT := ALink.sUserName;
end;

function TIDB_BaseExternalTablesMan.GetLinkPassword (sLinkName: AnsiString): AnsiString;
var
   ALink: PrecExtTableLinkProps;
begin
   RESULT := '';
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
      RESULT := ALink.sPassword;
end;

function TIDB_BaseExternalTablesMan.GetLinkTableName (sLinkName: AnsiString): AnsiString;
var
   ALink: PrecExtTableLinkProps;
begin
   RESULT := '';
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
      RESULT := ALink.sSourceTableName;
end;

function TIDB_BaseExternalTablesMan.GetLinkCodePage (sLinkName: AnsiString): TCodePage;
var
   ALink: PrecExtTableLinkProps;
begin
   RESULT := cpANSI;
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
      RESULT := ALink.cpPage;
end;

function TIDB_BaseExternalTablesMan.GetLinkKeepActive (sLinkName: AnsiString): Boolean;
var
   ALink: PrecExtTableLinkProps;
begin
   RESULT := FALSE;
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
      RESULT := ALink.bKeepLinkActive;
end;

function TIDB_BaseExternalTablesMan.GetLinkProgisIDFieldName (sLinkName: AnsiString): AnsiString;
var
   ALink: PrecExtTableLinkProps;
begin
   RESULT := 'ProgisID';
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
      RESULT := ALink.sProgisIDFieldInExtTable;
end;

function TIDB_BaseExternalTablesMan.GetLinkSelectedFields (sLinkName: AnsiString): AnsiString;
var
   ALink: PrecExtTableLinkProps;
begin
   RESULT := '';
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
      RESULT := ALink.sSelectedFields;
end;

// ++ Cadmensky Version 2.3.4

function TIDB_BaseExternalTablesMan.GetLinkServerName (sLinkName: AnsiString): AnsiString;
var
   ALink: PrecExtTableLinkProps;
begin
   RESULT := '';
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
      RESULT := ALink.sServerName;
end;
// -- Cadmensky Version 2.3.4

function TIDB_BaseExternalTablesMan.GetLinkIsUsedState (sLinkName: AnsiString): Boolean;
var
   ALink: PrecExtTableLinkProps;
begin
   RESULT := FALSE;
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
      RESULT := ALink.bLinkIsUsed;
end;

function TIDB_BaseExternalTablesMan.GetLinkAllFieldNames (sLinkName: AnsiString): AnsiString;
var
   ALink: PrecExtTableLinkProps;
   ATable: TDTable;
   iI: Integer;
   AList: TStringList;
begin
   RESULT := '';
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
   begin
      ATable := TDTable.Create (nil);
      ATable.TableName := ALink.sSourceTableName;

      case ALink.ATypeOfDatabase of
         dtMSAccess:
            ATable.Connection := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + ALink.sSourceDatabaseFileName;
         dtParadox:
            ATable.Connection := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + ALink.sSourceDatabaseFileName +
               ';' + 'Extended Properties=Paradox 5.x;' +
               'Mode=ReadWrite|Share Deny None;Persist Security Info=False';
         dtxBase:
            ATable.Connection := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + ALink.sSourceDatabaseFileName +
               ';' + 'Extended Properties=dBase 5.0;' +
               'Mode=ReadWrite|Share Deny None;Persist Security Info=False';
         dtSQLServer:
            ATable.Connection := 'Provider=SQLOLEDB.1;' +
               'Data Source=' + ALink.sServerName + ';' +
               'Initial Catalog=' + ALink.sSourceDatabaseFileName + ';' +
               'Persist Security Info=False;' +
               'User ID=' + ALink.sUserName + ';' +
               'Password=' + ALink.sPassword + ';';
      end;
      AList := TStringList.Create;
      SetScreenCursor;
      try
         ATable.Open;
         for iI := 0 to ATable.FieldCount - 1 do
            AList.Add (ATable.Fields[iI].FieldName);
         RESULT := AList.Text;
      finally
         AList.Clear;
         AList.Free;
         ATable.Close;
         ATable.Free;
         RestoreScreenCursor;
      end;
   end;
end;

// ++ Cadmensky Version 2.3.4

function TIDB_BaseExternalTablesMan.GetLinkIntegerFieldNames (sLinkName: AnsiString): AnsiString;
var
   ALink: PrecExtTableLinkProps;
   ATable: TDTable;
   iI: Integer;
   AList: TStringList;
   AStructMan: TIDB_StructMan;
   sFieldName: AnsiString;
begin
   RESULT := '';
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
   begin
      ATable := TDTable.Create (nil);
      ATable.TableName := ALink.sSourceTableName;

      case ALink.ATypeOfDatabase of
         dtMSAccess: ATable.Connection := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + ALink.sSourceDatabaseFileName;
         dtParadox:
            ATable.Connection := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + ALink.sSourceDatabaseFileName +
               ';' + 'Extended Properties=Paradox 5.x;' +
               'Mode=ReadWrite|Share Deny None;Persist Security Info=False';
         dtxBase:
            ATable.Connection := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + ALink.sSourceDatabaseFileName +
               ';' + 'Extended Properties=dBase 5.0;' +
               'Mode=ReadWrite|Share Deny None;Persist Security Info=False';
         dtSQLServer:
            ATable.Connection := 'Provider=SQLOLEDB.1;' +
               'Data Source=' + ALink.sServerName + ';' +
               'Initial Catalog=' + ALink.sSourceDatabaseFileName + ';' +
               'Persist Security Info=False;' +
               'User ID=' + ALink.sUserName + ';' +
               'Password=' + ALink.sPassword + ';';
      end;
      AList := TStringList.Create;
      SetScreenCursor;
      try
         ATable.Open;
         AStructMan := TIDB_StructMan.Create (SELF.AProject, SELF.IDA);
         AStructMan.DetectStructure (ATable);
         for iI := 0 to ATable.FieldCount - 1 do
         begin
            sFieldName := ATable.Fields[iI].FieldName;
            if (AStructMan.GetFieldType (sFieldName) = 2) or
               (AStructMan.GetFieldType (sFieldName) = 3) or
               (AStructMan.GetFieldType (sFieldName) = 6) then
               AList.Add (ATable.Fields[iI].FieldName);
         end;
         RESULT := AList.Text;
      finally
         AList.Clear;
         AList.Free;
         ATable.Close;
         ATable.Free;
         AStructMan.Free;
         RestoreScreenCursor;
      end;
   end;
end;
// -- Cadmensky Version 2.3.4

procedure TIDB_BaseExternalTablesMan.SetLinkName (sOldLinkName: AnsiString; sValue: AnsiString);
var
   ALink: PrecExtTableLinkProps;
// ++ Commented by Cadmensky Version 2.3.1
//   FConnection: _Connection;
// -- Commented by Cadmensky Version 2.3.1
// ++ Commented by Cadmensky Version 2.3.4
{   FCatalog: _Catalog;
   FTable: _Table;}
// -- Commented by Cadmensky Version 2.3.4
   sLinkName: AnsiString;
begin
// ++ Cadmensky Version 2.3.4
   sLinkName := IntToStr (GetLayerIndexFunc (SELF.ALayer)) + '_' + sOldLinkName;
// -- Cadmensky Version 2.3.4

   if Trim (sValue) <> '' then
   begin
      ALink := SELF.FindLink (sLinkName);
      if ALink <> nil then
      begin
// ++ Commented by Cadmensky Version 2.3.4
{         sDBFileName := MakeDBFileNameFromProjectFileName (GetProjectFullFileNameFunc (SELF.AProject));
// ++ Commented by Cadmensky Version 2.3.1
//         FConnection := CoConnection.Create;
//         try
//            FConnection.Open ('Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + MakeDBFileNameFromProjectFileName (GetProjectFullFileNameFunc (SELF.AProject)), '', '', 0);
//         except
            //                  MessageBox(Handle, PChar(Format(ALangManager.GetValue(102), [ALink.sSourceDataBaseFileName])), cs_ApplicationName, MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
//            exit;
//         end;
// -- Commented by Cadmensky Version 2.3.1
         FCatalog := CoCatalog.Create;
// ++ Cadmensky Version 2.3.1
         FCatalog.Set_ActiveConnection (SELF.IDA.DMaster.CentralData.ADO);
//         FCatalog.Set_ActiveConnection (FConnection);
// -- Cadmensky Version 2.3.1

         FTable := CoTable.Create;
         FTable.Name := sValue;
         FTable.ParentCatalog := FCatalog; // Set the properties to create the linkend;
         FTable.Properties.Item[8].Set_Value (true); //'Create Link'
         FTable.Properties.Item[5].Set_Value (';Pwd=password');
         //'Jet OLEDB:Link Datasource'
         FTable.Properties.Item[6].Set_Value (ALink.sSourceDatabaseFileName);
         //'Jet OLEDB:Remote Table Name'
         FTable.Properties.Item[4].Set_Value (ALink.sSourceTableName);
         // Append the table to the collection
         try
            FCatalog.Tables.Append (FTable);
            FCatalog.Tables.Delete (sOldLinkName);
         except
         end; }
// -- Commented by Cadmensky Version 2.3.4
         SELF.IDA.ADBItemsMan.DeleteLinkTable (sLinkName);
         ALink.sLinkName := IntToStr (GetLayerIndexFunc (SELF.ALayer)) + '_' + sValue;
      end;
   end;
end;

procedure TIDB_BaseExternalTablesMan.SetLinkType (sLinkName: AnsiString; dtValue: TDatabaseType);
var
   ALink: PrecExtTableLinkProps;
begin
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
   begin
      ALink.ATypeOfDatabase := dtValue;
      SELF.SetModifiedSign;
   end;
end;

procedure TIDB_BaseExternalTablesMan.SetLinkDatabaseFileName (sLinkName: AnsiString; sValue: AnsiString);
var
   ALink: PrecExtTableLinkProps;
begin
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
   begin
      ALink.sSourceDatabaseFileName := sValue;
      SELF.SetModifiedSign;
   end;
end;

// ++ Cadmensky Version 2.3.4

procedure TIDB_BaseExternalTablesMan.SetLinkServerName (sLinkName: AnsiString; sValue: AnsiString);
var
   ALink: PrecExtTableLinkProps;
begin
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
   begin
      ALink.sServerName := sValue;
      SELF.SetModifiedSign;
   end;
end;
// -- Cadmensky Version 2.3.4

procedure TIDB_BaseExternalTablesMan.SetLinkUserName (sLinkName: AnsiString; sValue: AnsiString);
var
   ALink: PrecExtTableLinkProps;
begin
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
   begin
      ALink.sUserName := sValue;
      SELF.SetModifiedSign;
   end;
end;

procedure TIDB_BaseExternalTablesMan.SetLinkPassword (sLinkName: AnsiString; sValue: AnsiString);
var
   ALink: PrecExtTableLinkProps;
begin
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
   begin
      ALink.sPassword := sValue;
      SELF.SetModifiedSign;
   end;
end;

procedure TIDB_BaseExternalTablesMan.SetLinkTableName (sLinkName: AnsiString; sValue: AnsiString);
var
   ALink: PrecExtTableLinkProps;
begin
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
   begin
      ALink.sSourceTableName := sValue;
      SELF.SetModifiedSign;
   end;
end;

procedure TIDB_BaseExternalTablesMan.SetLinkCodePage (sLinkName: AnsiString; cpValue: TCodePage);
var
   ALink: PrecExtTableLinkProps;
begin
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
      ALink.cpPage := cpValue;
end;

procedure TIDB_BaseExternalTablesMan.SetLinkKeepActive (sLinkName: AnsiString; bValue: Boolean);
var
   ALink: PrecExtTableLinkProps;
begin
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
      ALink.bKeepLinkActive := bValue;
end;

procedure TIDB_BaseExternalTablesMan.SetLinkProgisIDFieldName (sLinkName: AnsiString; sValue: AnsiString);
var
   ALink: PrecExtTableLinkProps;
begin
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
   begin
      ALink.sProgisIDFieldInExtTable := sValue;
      SELF.SetModifiedSign;
   end;
end;

procedure TIDB_BaseExternalTablesMan.SetLinkSelectedFields (sLinkName: AnsiString; sValue: AnsiString);
var
   ALink: PrecExtTableLinkProps;
begin
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
   begin
      ALink.sSelectedFields := sValue;
      SELF.SetModifiedSign;
   end;
end;

procedure TIDB_BaseExternalTablesMan.SetLinkIsUsedState (sLinkName: AnsiString; bValue: Boolean);
var
   ALink: PrecExtTableLinkProps;
begin
   ALink := SELF.FindLink (sLinkName);
   if ALink <> nil then
   begin
      ALink.bLinkIsUsed := bValue;
      SELF.SetModifiedSign;
   end;
end;

// ++ Commented by Cadmensky Version 2.3.4
{procedure TIDB_BaseExternalTablesMan.SetLinksOrder (sValue: AnsiString);
begin
   SELF.ExtTablesLinkOrder.Text := sValue;
   SELF.SetModifiedSign;
end;}
// -- Commented by Cadmensky Version 2.3.4

function TIDB_BaseExternalTablesMan.GenerateLinkName (sLinkName: AnsiString): AnsiString;
var
   iI: Integer;
   sRealLinkName: AnsiString;
begin
   RESULT := sLinkName;
// ++ Cadmensky Version 2.3.1
   sRealLinkName := IntToStr (GetLayerIndexFunc (SELF.ALayer)) + '_' + sLinkName;
// -- Cadmensky Version 2.3.1
   iI := 1;
   while SELF.FindLink (sRealLinkName) <> nil do
   begin
      RESULT := sLinkName + '_' + IntToStr (iI);
      sRealLinkName := IntToStr (GetLayerIndexFunc (SELF.ALayer)) + '_' + sLinkName + '_' + IntToStr (iI);
      Inc (iI);
   end;
end;

{ This procedure checks if the link to the database file and to the table in this database file
  exists or not. if the link exists, the function returns TRUE and FALSE otherwise. }

function TIDB_BaseExternalTablesMan.LinkExists (sDatabaseFileName, sTableName: AnsiString): Boolean;
var
   ALink: PrecExtTableLinkProps;
   iI: Integer;
begin
   RESULT := FALSE;
   sDatabaseFileName := AnsiUpperCase (sDatabaseFileName);
   sTableName := AnsiUpperCase (sTableName);
   for iI := 0 to ExtTablesList.Count - 1 do
   begin
      ALink := ExtTablesList[iI];
      if (AnsiUpperCase (ALink.sSourceDatabaseFileName) = sDatabaseFileName)
         and
         (AnsiUpperCase (ALink.sSourceTableName) = sTableName) then
      begin
         RESULT := TRUE;
         BREAK;
      end;
   end;
end;

////////////////////////////////////////////////////////////////////////////////

constructor TIDB_ExternalTablesMan.Create (AProject, ALayer: Pointer; MainIDA: TIDB_Access2DB_Level2);
begin
   inherited Create (AProject, ALayer, MainIDA);
   SELF.LinksForUpdate := TStringList.Create;
   SELF.sJoinClauseText := '';
   SELF.sJoiningSQLText := '';

{$IFDEF EXT_TAB_POOL}
   LoadData;
{$ENDIF}
   DropModifiedSign;
end;

destructor TIDB_ExternalTablesMan.Free;
begin
{$IFDEF EXT_TAB_POOL}
   SaveData;
{$ENDIF}
   SELF.Disconnect;
   SELF.ClearDataForUpdate; // Just in case
   Self.LinksForUpdate.Free;
   inherited Free;
end;

procedure TIDB_ExternalTablesMan.SaveData;
var
   AStream: TDBlobStream;

   procedure WriteInteger (iData: Integer);
   begin
      AStream.Write (iData, SizeOf (iData));
   end;

   procedure WriteString (sData: AnsiString);
   var
      iData: Integer;
   begin
      iData := Length (sData);
      WriteInteger (iData);
      AStream.Write (Pointer (sData)^, iData);
   end;

var
   iI, iCount: Integer;
   ALink: PrecExtTableLinkProps;
//   AnAdditionalIDA: TIDB_Access2DB_Level2;
   AQuery: TDQuery;
   sTableName: AnsiString;
   sFieldName: AnsiString;
   bF: Boolean;
begin
{   AnAdditionalIDA := TIDB_Access2DB_Level2.Create (AProject);
   AnAdditionalIDA.Connect (MakeDBFileNameFromProjectFileName (GetProjectFullFileNameFunc (SELF.AProject)), '', '');}

   sTableName := cs_CommonDescTableName;
   sFieldName := 'ExtTabLinksProps';
{   AnAdditionalIDA.ADbItemsMan.CreateCommonDescTable;
   if AnAdditionalIDA.ADbItemsMan.FieldExists (sTableName, sFieldName, bF) <> ci_All_OK then EXIT;
   if not bF then AnAdditionalIDA.ADbItemsMan.AddField (sTableName, sFieldName, 15, -1); // 15th type is BLOB}
   SELF.IDA.ADbItemsMan.CreateCommonDescTable;

// ++ Cadmensky Version 2.3.2
   if SELF.IDA.ADBItemsMan.GetReadyToCheckTableStructure (sTableName) <> ci_All_Ok then
   begin
      SELF.IDA.ADBItemsMan.EndOfCheckingTable;
      Exit;
   end;

   if not SELF.IDA.ADbItemsMan.FieldExists (sFieldName) then
      SELF.IDA.ADbItemsMan.AddField (sTableName, sFieldName, 15, -1); // 15th type is BLOB

   SELF.IDA.ADBItemsMan.EndOfCheckingTable;
// -- Cadmensky Version 2.3.2

// ++ Commented by Cadmensky Version 2.3.2
{   if SELF.IDA.ADbItemsMan.FieldExists (sTableName, sFieldName, bF) <> ci_All_OK then
      EXIT;
   if not bF then
      SELF.IDA.ADbItemsMan.AddField (sTableName, sFieldName, 15, -1); // 15th type is BLOB}
// -- Commented by Cadmensky Version 2.3.2

   AQuery := TDQuery.Create (nil);
   AQuery.Master := SELF.IDA.DMaster;
   AQuery.SQL.Text := 'SELECT * FROM [' + cs_CommonDescTableName + '] ' +
      'WHERE [' + cs_CDT_LayerIndexFieldName + '] = ' + IntToStr (GetLayerIndexFunc (SELF.ALayer));
   try
      AQuery.Open;
      if AQuery.RecordCount < 1 then
         AQuery.Insert
      else
      begin
         AQuery.First;
         AQuery.Edit;
      end;
      AStream := TDBlobStream (AQuery.CreateBlobStream (AQuery.FieldByName (sFieldName), bmWrite));
      if AStream = nil then
         raise Exception.Create ('The stream cannot be created');
   except
      AQuery.Close;
      AQuery.Free;
{      AnAdditionalIDA.Disconnect;
      AnAdditionalIDA.Free;}
      EXIT;
   end;

   try
      AStream.Truncate;
      WriteString (cs_VerisonOfLinkStorage);
      WriteInteger (Integer (bIAmInUse));
// ++ Cadmensky Version 2.3.4
      WriteString ('');
//      WriteString (ExtTablesLinkOrder.Text);
// -- Cadmensky Version 2.3.4
      iCount := 0;
      for iI := 0 to ExtTablesList.Count - 1 do
      begin
         ALink := ExtTablesList[iI];
         if ALink.sLinkName <> cs_LinkToLayerTable then
            Inc (iCount);
      end;
      WriteInteger (iCount);
      for iI := 0 to ExtTablesList.Count - 1 do
      begin
         ALink := ExtTablesList[iI];
         if ALink.sLinkName <> cs_LinkToLayerTable then
         begin
            WriteString (ALink.sLinkName);
            WriteInteger (Ord (ALink.ATypeOfDatabase));
            WriteString (ALink.sSourceDatabaseFileName);
            WriteString (ALink.sUserName);
            WriteString (ALink.sPassword);
            WriteString (ALink.sSourceTableName);
            WriteString ('' {ALink.sDestinationTableName});
            WriteInteger (Ord (ALink.cpPage));
            WriteString (ALink.sProgisIDFieldInExtTable);
            WriteString (ALink.sSelectedFields);
            WriteInteger (Integer (ALink.bLinkIsUsed));
         end;
      end; // for iI end
   finally
      AStream.Flush;
      AStream.Free;
      AQuery.Post;
      AQuery.Close;
   end;
   AQuery.Free;
{   AnAdditionalIDA.Disconnect;
   AnAdditionalIDA.Free;}
end;

function TIDB_ExternalTablesMan.LoadData: Boolean;
var
   AStream: TDBlobStream;

   function ReadInteger: Integer;
   begin
      AStream.Read(RESULT, SizeOf (RESULT));
   end;

   function ReadString: AnsiString;
   var
      iL: Integer;
   begin
      iL := ReadInteger;
      SetLength (RESULT, iL);
      AStream.Read (Pointer (RESULT)^, iL);
   end;

var
   iI, iD: Integer;
   ALink: PrecExtTableLinkProps;
//   AnAdditionalIDA: TIDB_Access2DB_Level2;
   AQuery: TDQuery;
   sVersionMark: AnsiString;
begin
   RESULT := FALSE;
   ID:=0;
{   AnAdditionalIDA := TIDB_Access2DB_Level2.Create (AProject);
   AnAdditionalIDA.Connect (MakeDBFileNameFromProjectFileName (GetProjectFullFileNameFunc (SELF.AProject)), '', '');}
   AQuery := TDQuery.Create (nil);
   AQuery.Master := SELF.IDA.DMaster;
   AQuery.SQL.Text := 'SELECT * FROM [' + cs_CommonDescTableName + '] ' +
      'WHERE [' + cs_CDT_LayerIndexFieldName + '] = ' + IntToStr (GetLayerIndexFunc (SELF.ALayer));
   try
      AQuery.Open;
      if AQuery.RecordCount < 1 then raise Exception.Create ('There is no data about links to external tables.');
      AStream := TDBlobStream (AQuery.CreateBlobStream (AQuery.FieldByName ('ExtTabLinksProps'), bmRead));
      if AStream = nil then raise Exception.Create ('The stream cannot be created');
   except
      AQuery.Close;
      AQuery.Free;
{      AnAdditionalIDA.Disconnect;
      AnAdditionalIDA.Free;}
      EXIT;
   end;

   if AStream.Size < 1 then
   begin
      AStream.Free;
      AQuery.Close;
      AQuery.Free;
{      AnAdditionalIDA.Disconnect;
      AnAdditionalIDA.Free;}
      EXIT;
   end;

   try
      AStream.Position := 0;

      sVersionMark := ReadString; // It is not used yet. for future use.

      bIAmInUse := Boolean (ReadInteger);
// ++ Cadmensky Version 2.3.4
      ReadString;
//      ExtTablesLinkOrder.Text := ReadString;
// -- Cadmensky Version 2.3.4
      SELF.ClearData;
      iD := ReadInteger;
      if iD> 9999000 then ID:= 1; // WAI falls fehler im Blob
      for iI := 0 to iD - 1 do
      begin
         New (ALink);
         ALink.sLinkName := ReadString;
         ALink.ATypeOfDatabase := TDatabaseType (ReadInteger);
         ALink.sSourceDatabaseFileName := ReadString;
         ALink.sUserName := ReadString;
         ALink.sPassword := ReadString;
         ALink.sSourceTableName := ReadString;
         {ALink.sDestinationTableName := } ReadString;
         ALink.cpPage := TCodePage (ReadInteger);
         ALink.sProgisIDFieldInExtTable := ReadString;
         ALink.sSelectedFields := ReadString;
         ALink.bLinkIsUsed := Boolean (ReadInteger);
         ExtTablesList.Add (ALink);
      end;
      RESULT := TRUE;
   finally
      AStream.Free;
      AQuery.Close;
      AQuery.Free;
{      AnAdditionalIDA.Disconnect;
      AnAdditionalIDA.Free;}
   end;
end;

{ This function creates new database file and move into it all required tables.
  This function creates external tabes pool, creates all required tables in it
  and copy data from external tables into this pool. }

function TIDB_ExternalTablesMan.MakeExtTablePool (bNeedToReconnect: Boolean; AGauge: TGauge = nil): Boolean;
var
   iI: Integer;
   ALink: PrecExtTableLinkProps;
   iDBType: Integer;
   sDatabaseFileName: AnsiString;
begin
   RESULT := false;

   SetScreenCursor;
   try
      if AGauge <> nil then
      begin
         AGauge.MaxValue := ExtTablesList.Count;
         Application.ProcessMessages;
         Application.HandleMessage;
      end;

      for iI := 0 to ExtTablesList.Count - 1 do
      begin
         ALink := ExtTablesList[iI];
         if ALink.bLinkIsUsed then
// ++ Cadmensky
         begin
            sDatabaseFileName := '';
// ++ Cadmensky Version 2.3.1
            case ALink.ATypeOfDatabase of
               dtMSAccess:
                  begin
                     sDatabaseFileName := ALink.sSourceDatabaseFileName;
                     iDBType := 2;
                  end;
               dtxBase:
                  begin
                     sDatabaseFileName := ALink.sSourceDatabaseFileName + ALink.sSourceTableName;
                     iDBType := 3;
                  end;
               dtParadox:
                  begin
                     sDatabaseFileName := ALink.sSourceDatabaseFileName + ALink.sSourceTableName;
                     iDBType := 5;
                  end;
               dtSQLServer: iDBType := 8;
            else
               Continue;
            end;
// -- Cadmensky Version 2.3.1

// ++ Cadmensky Version 2.3.7
            SELF.IDA.ADBItemsMan.DeleteLinkTable (ALink.sLinkName);
            if (sDatabaseFileName <> '') and not FileExists (sDatabaseFileName) then
            begin
               MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (105), [sDatabaseFileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
               ALink.bLinkIsUsed := false;
               Continue;
            end;
// -- Cadmensky Version 2.3.7
            SELF.IDA.ADBItemsMan.CreateLinkTable (ALink.sSourceDatabaseFileName,
               ALink.sSourceTableName,
               ALink.sLinkName,
               iDBType,
               ALink.sServerName,
               ALink.sUserName,
               ALink.sPassword);
         end;
// -- Cadmensky

         if AGauge <> nil then
         begin
            AGauge.Progress := AGauge.Progress + 1;
            Application.ProcessMessages;
            Application.HandleMessage;
         end;

      end; // for iI end

// ++ Cadmensky Version 2.3.8
      if bNeedToReconnect then
      begin
         SELF.IDA.DMaster.Connected := false;
         SELF.IDA.DMaster.Connected := true;
      end;
// -- Cadmensky Version 2.3.8

      RESULT := TRUE;
   finally
      RestoreScreenCursor;
   end;
end;

{ This function creates SQL statement and connect AQuery to external tables pool. }

procedure TIDB_ExternalTablesMan.SetParamsToExtTablePool;
var
   iI, iJ: Integer;
   ALink: PrecExtTableLinkProps;

   slSelectedFieldsNames: TStringList;
   sFieldName,
      sSelectedFieldsNames,
      sSelectedTablesNames,
      sJoinClause: AnsiString;
   slSQL: TStringList;
begin
   SELF.bIAmInUse := FALSE;
//   SetScreenCursor;
   try
      slSQL := TStringList.Create;
      slSelectedFieldsNames := TStringList.Create;
      sSelectedTablesNames := '';
      sSelectedFieldsNames := '';
      sJoinClause := '';

// ++ Cadmensky Version 2.3.4
      for iI := 0 to ExtTablesList.Count - 1 do
//      for iI := 0 to ExtTablesLinkOrder.Count - 1 do
      begin
//         ALink := FindLink (ExtTablesLinkOrder[iI]);
         ALink := FindLink (iI);
// -- Cadmensky Version 2.3.4
         if ALink.bLinkIsUsed then
         begin
            slSelectedFieldsNames.Text := ALink.sSelectedFields;
            if slSelectedFieldsNames.Count = 0 then
               continue;
            sSelectedTablesNames := sSelectedTablesNames + '[' + ALink.sLinkName + '], ';

            for iJ := 0 to slSelectedFieldsNames.Count - 1 do
            begin
               sFieldName := slSelectedFieldsNames[iJ];
               sSelectedFieldsNames := sSelectedFieldsNames + '[' + ALink.sLinkName + '].[' + sFieldName + '] AS [' + IntToStr (iI + 1) + '_' + sFieldName + '], ';
            end;
         end;
      end; // for iI end

// ++ Cadmensky Version 2.3.4
      ALink := SELF.FindLink (0);
//      ALink := SELF.FindLink (ExtTablesLinkOrder[0]);
// -- Cadmensky Version 2.3.4

      if ALink <> nil then
      begin
         slSelectedFieldsNames.Text := ALink.sSelectedFields;
         if (ALink.bLinkIsUsed) and (slSelectedFieldsNames.Count > 0) then
            sJoinClause := '[' + SELF.sMainLayerTableName + '] LEFT JOIN [' + ALink.sLinkName + '] ' +
               'ON [' + SELF.sMainLayerTableName + '].[PROGISID] = [' + ALink.sLinkName + '].[' + ALink.sProgisIDFieldInExtTable + ']'
         else
            sJoinClause := '[' + SELF.sMainLayerTableName + ']';
      end
      else
         sJoinClause := '[' + SELF.sMainLayerTableName + ']';

// ++ Cadmensky Version 2.3.4
      for iI := 1 to ExtTablesList.Count - 1 do
//      for iI := 1 to ExtTablesLinkOrder.Count - 1 do
      begin
         ALink := SELF.FindLink (iI);
//         ALink := SELF.FindLink (ExtTablesLinkOrder[iI]);
// -- Cadmensky Version 2.3.4
         slSelectedFieldsNames.Text := ALink.sSelectedFields;
         if (ALink.bLinkIsUsed) and (slSelectedFieldsNames.Count > 0) then
            sJoinClause := '(' + sJoinClause + ') LEFT JOIN [' + ALink.sLinkName + '] ON [' + SELF.sMainLayerTableName + '].[PROGISID] = [' + ALink.sLinkName + '].[' + ALink.sProgisIDFieldInExtTable + ']';
      end;

      //        sSelectedTablesNames := Copy(sSelectedTablesNames, 1, Length(sSelectedTablesNames)-2); // Delete two last ', ' symbols.
      if sSelectedFieldsNames <> '' then
         sSelectedFieldsNames := ', ' + Copy (sSelectedFieldsNames, 1, Length (sSelectedFieldsNames) - 2); // The same.
      slSQL.Add ('SELECT [' + SELF.sMainLayerTableName + '].*' + sSelectedFieldsNames);
      slSQL.Add ('FROM (' + sJoinClause + ')');
      // And we have to show only 'normal' or 'just added' objects but not 'deleted' objects.
      slSQL.Add ('WHERE (([' + sMainLayerTableName + '].[' + cs_StateFieldName + '] = ' + cs_ObjectIsNormal + ') OR ([' + sMainLayerTableName + '].[' + cs_StateFieldName + '] = ' + cs_ObjectIsJustAdded + '))');
      SELF.sJoinClauseText := sJoinClause;
      SELF.sJoiningSQLText := slSQL.Text;

      SELF.bIAmInUse := SELF.GetUsedLinksCount > 0;
      SELF.DropModifiedSign;
   finally
      slSelectedFieldsNames.Clear;
      slSelectedFieldsNames.Free;
      slSQL.Free;
//      RestoreScreenCursor;
   end;
end;

procedure TIDB_ExternalTablesMan.Disconnect;
begin
// ++ Cadmensky
//   SELF.IDA.Disconnect;
// -- Cadmensky
   SELF.bIAmInUse := FALSE;
   SELF.sJoiningSQLText := '';
end;

procedure TIDB_ExternalTablesMan.ClearDataForUpdate;
var
   iI, iJ: Integer;
   AList: TList;
   AData: PrecDataForUpdate;
begin
   for iI := 0 to LinksForUpdate.Count - 1 do
   begin
      AList := TList (LinksForUpdate.Objects[iI]);
      for iJ := 0 to AList.Count - 1 do
      begin
         AData := AList[iJ];
         Dispose (AData);
      end;
      AList.Clear;
      AList.Free;
   end;
   LinksForUpdate.Clear;
end;

procedure TIDB_ExternalTablesMan.GetReadyToReceiveDataForUpdate;
begin
   ClearDataForUpdate;
end;

procedure TIDB_ExternalTablesMan.ReceiveDataForUpdate (sFieldName, sData: AnsiString; dtFieldType: TFieldType);
var
   AList: TList;
   iI, iP, iLP: Integer;
   sLinkName: AnsiString;
   bF: Boolean;
   AData: PrecDataForUpdate;
   ALink: PrecExtTableLinkProps;
   iLinkNumber: integer;
   sRealFieldName: AnsiString;
begin
   if FieldIsCalculatedField (sFieldName) then
      exit;

   iLinkNumber := GetLinkedFieldLinkNo (sFieldName, sRealFieldName);
   if iLinkNumber = -1 then
      sLinkName := sMainLayerTableName
   else
   begin
// ++ Commented by Cadmensky Version 2.3.4
//      sLinkName := SELF.ExtTablesLinkOrder[iLinkNumber];
//      ALink := SELF.FindLink (sLinkName);
// -- Commented by Cadmensky Version 2.3.4
// ++ Cadmensky Version 2.3.4
      ALink := SELF.FindLink (iLinkNumber);
      if ALink = nil then
         exit;
      sLinkName := ALink.sLinkName;
// -- Cadmensky Version 2.3.4
//      sFieldName := Copy (sFieldName, iP + 1, Length (sFieldName));
      if AnsiUpperCase (sRealFieldName) = AnsiUpperCase (ALink.sProgisIDFieldInExtTable) then
         exit;

   end;
   bF := FALSE;
   for iI := 0 to SELF.LinksForUpdate.Count - 1 do
      if LinksForUpdate[iI] = sLinkName then
      begin
         bF := TRUE;
         BREAK;
      end;
   if not bF then
   begin
      AList := TList.Create;
      iP := LinksForUpdate.Add (sLinkName);
      LinksForUpdate.Objects[iP] := AList;
   end
   else
      AList := TList (LinksForUpdate.Objects[iI]);

   bF := FALSE;
   for iI := 0 to AList.Count - 1 do
   begin
      AData := AList[iI];
      if AData.sFieldName = sRealFieldName then
      begin
         bF := TRUE;
         BREAK;
      end;
   end;
   if not bF then
   begin
      New (AData);
      AData.sFieldName := sRealFieldName;
      AData.sDataForUpdate := MakeCorrectString (sData);

      case dtFieldType of
         ftSmallInt,
            ftInteger,
            ftWord,
            ftFloat,
            ftCurrency,
            ftLargeint:
            AData.bFieldHasStringType := false
      else
         AData.bFieldHasStringType := true;
      end;
      AList.Add (AData);
   end
   else
      AData.sDataForUpdate := sData;
   // Replace ',' with '.' for correct SQL statement.
   if not AData.bFieldHasStringType then
      AData.sDataForUpdate := ReplaceChars (AData.sDataForUpdate, DecimalSeparator, '.');
end;

procedure TIDB_ExternalTablesMan.ExecuteUpdate (iProgisIDValue: Integer);
var
   iI, iJ: Integer;
   sLinkName: AnsiString;
   sSQL: AnsiString;
   iLayerIndex: Integer;
   sTableName: AnsiString;
   AQuery: TDQuery;
   AList: TList;
   AData: PrecDataForUpdate;
   iCount: Integer;
   ALink: PrecExtTableLinkProps;
begin
   AQuery := TDQuery.Create (nil);
   AQuery.Master := SELF.IDA.DMaster;
   try
      for iI := 0 to LinksForUpdate.Count - 1 do
      begin
         sLinkName := LinksForUpdate[iI];
         AList := TList (LinksForUpdate.Objects[iI]);
         iCount := 0;
         for iJ := 0 to AList.Count - 1 do
         begin
            AData := AList[iJ];
            if (AData.bFieldHasStringType) or
               ((not AData.bFieldHasStringType) and (Trim (AData.sDataForUpdate) <> '')) then
               Inc (iCount);
         end;
         if iCount > 0 then
         begin
            sSQL := '';
            for iJ := 0 to AList.Count - 1 do
            begin
               AData := AList[iJ];
               // The string field can be updated to empty value, but the other cannot.
               if (AData.bFieldHasStringType) then
                  sSQL := sSQL + ' [' + AData.sFieldName + '] = ''' + AData.sDataForUpdate + ''''
               else
               begin
                  if Trim (AData.sDataForUpdate) <> '' then
                     sSQL := sSQL + ' [' + AData.sFieldName + '] = ' + AData.sDataForUpdate
                  else
                     sSQL := sSQL + ' [' + AData.sFieldName + '] = NULL';
               end;
               if iJ < AList.Count - 1 then
                  sSQL := sSQL + ', ';
            end; // for iJ end

            if sLinkName = sMainLayerTableName then
            begin
               // Link to Layer Attribute Table processing start.
               iLayerIndex := GetLayerIndexFunc (SELF.ALayer);

               if not AnIDBMainMan.UseNewATTNames (AProject) then
                  sTableName := cs_AttributeTableNamePrefix + IntToStr (iLayerIndex)
               else
                  sTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (SELF.AProject, SELF.ALayer);

               // Update data in IDB file.
// ++ Commented by Cadmensky Version 2.3.3
               AQuery.SQL.Text := 'SELECT * FROM [' + sTableName + '] ' +
                  'WHERE [PROGISID] = ' + IntToStr (iProgisIDValue);
               AQuery.Active := true;
               if AQuery.RecordCount < 1 then
// -- Commented by Cadmensky Version 2.3.3
// ++ Cadmensky Version 2.3.3
                  try
// -- Cadmensky Version 2.3.3
                     AQuery.ExecSQL ('INSERT INTO [' + sTableName + '] ' +
                        '([PROGISID], [' + cs_StateFieldName + ']) ' +
                        'VALUES (' + IntToStr (iProgisIDValue) + ', ' + cs_ObjectIsJustAdded + ')');
// ++ Cadmensky Version 2.3.3
                  except
                  end;
// -- Cadmensky Version 2.3.3
// ++ Commented by Cadmensky Version 2.3.3
               AQuery.Active := false;
// -- Commented by Cadmensky Version 2.3.3
               try
                  AQuery.ExecSQL ('UPDATE [' + sTableName + '] SET ' + sSQL + ' WHERE [PROGISID] = ' + IntToStr (iProgisIDValue));
               except
               end;
            end // Link to Layer Attribute Table processing end.
            else
            begin
               // Link to External Table processing start.
               ALink := FindLink (sLinkName);
// ++ Commented by Cadmensky Version 2.3.4
//               iJ := SELF.ExtTablesLinkOrder.IndexOf (sLinkName);
// -- Commented by Cadmensky Version 2.3.4

               AQuery.SQL.Text := 'SELECT * FROM [' + ALink.sLinkName + '] ' +
                  'WHERE [' + ALink.sProgisIDFieldInExtTable + '] = ' + IntToStr (iProgisIDValue);
               AQuery.Open;
               if AQuery.RecordCount < 1 then
                  try
                     AQuery.ExecSQL ('INSERT INTO [' + ALink.sLinkName + '] ' +
                        '([' + ALink.sProgisIDFieldInExtTable + ']) ' +
                        'VALUES (' + IntToStr (iProgisIDValue) + ')');
                     AQuery.ReOpen;
                  except

                  end;
//               AQuery.Active := false;
               try
                  AQuery.ExecSQL ('UPDATE [' + ALink.sLinkName + '] SET ' + sSQL + ' ' +
                     'WHERE [' + ALink.sProgisIDFieldInExtTable + '] = ' + IntToStr (iProgisIDValue));
               except
               end;
               AQuery.Close;
            end; // Link to external table processing end.
         end; // if iCount > 0 end
      end; // for iI end
   finally
      AQuery.Free;
      ClearDataForUpdate;
   end;
end;

// ++ Commented by Cadmensky Version 2.3.9
{function TIDB_ExternalTablesMan.SelectDistinctFieldsForProgisID (sFieldName: AnsiString; iProgisID: integer): AnsiString;
var
   AQuery: TDQuery;
   AField: TField;
   ALink: PrecExtTableLinkProps;
   sRealFieldName: AnsiString;
   iLinkNumber: integer;
   slResult: TStringList;
begin
   Result := '';
   iLinkNumber := GetLinkedFieldLinkNo (sFieldName, sRealFieldName);

   if iLinkNumber = -1 then
      exit;
// ++ Commented by Cadmensky Version 2.3.4
//   try
//      ALink := FindLink (ExtTablesLinkOrder[iLinkNumber]);
//   except
// -- Commented by Cadmensky Version 2.3.4
// ++ Cadmensky Version 2.3.4
   ALink := FindLink (iLinkNumber);
   if ALink = nil then
      exit;
//   end;
// -- Cadmensky Version 2.3.4
   if ALink.bLinkIsUsed then
   begin
      AQuery := TDQuery.Create (nil);
      AQuery.Master := SELF.IDA.DMaster;
      AQuery.SQL.Text := 'SELECT DISTINCT [' + sRealFieldName + '] FROM [' + ALink.sLinkName + ']' +
         ' WHERE [' + ALink.sProgisIDFieldInExtTable + ']=' + IntToStr (iProgisID);
      try
         AQuery.Open;

         if AQuery.RecordCount > 1 then
         begin
            slResult := TStringList.Create;
            AQuery.First;
            while not AQuery.EOF do
            begin
               AField := AQuery.FindField (sRealFieldName);
               if AField <> nil then
                  slResult.Add (AField.AsString);
               AQuery.Next;
            end;
            Result := slResult.Text;
         end;
      finally
         AQuery.Free;
      end;
   end;
end;}
// -- Commented by Cadmensky Version 2.3.9

end.

