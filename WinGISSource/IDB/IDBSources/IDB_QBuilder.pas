unit IDB_QBuilder;
{
Internal database. Ver. II.
Query builder.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 05-03-2001
}
interface

uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   ExtCtrls, StdCtrls, ComCtrls, Buttons, DBTables, ExtPage,
   DMaster, DTables, Grids, Validate,
   IDB_StructMan, IDB_Access2DB, IDB_C_StringGrid, IDB_Validators, IDB_Grids,
   IDB_dbgrids, IDB_C_DBGrid, Db, DDB;

type
   TWorkMode = (wmSQLBuilder, wmUpdateBuilder);
   {
    The TRaioButtons have a preset property Tag that equals index of SQL operator (for example,
    0 - 'Nothing', 1 - '=', 2 - '<=' etc). These properties are preset from 0 to 8.
   }
   TIDB_QBForm = class (TForm)
      Panel2: TPanel;
      OKButton: TButton;
      CancelButton: TButton;
      ExtPageControl1: TExtPageControl;
      TabSheet1: TTabSheet;
      TabSheet2: TTabSheet;
      TabSheet3: TTabSheet;
      AvailableTableFieldsListBox: TListBox;
      SelectedTableFieldsListBox: TListBox;
      Button1: TButton;
      Button2: TButton;
      Button3: TButton;
      Button4: TButton;
      ValuesFieldsListBox: TListBox;
      AvailableSelectedFieldsListBox: TListBox;
      OrdersFieldsListBox: TListBox;
      Button5: TButton;
      Button6: TButton;
      Button7: TButton;
      Button8: TButton;
      Label1: TLabel;
      Label2: TLabel;
      Label3: TLabel;
      GroupBox1: TGroupBox;
      RadioButton1: TRadioButton;
      RadioButton2: TRadioButton;
      RadioButton3: TRadioButton;
      RadioButton4: TRadioButton;
      RadioButton5: TRadioButton;
      RadioButton6: TRadioButton;
      RadioButton7: TRadioButton;
      RadioButton9: TRadioButton;
      CheckBox2: TCheckBox;
      TabSheet4: TTabSheet;
      BoolValComboBox: TComboBox;
      Memo2: TMemo;
      Add_NewStatement_Button: TButton;
      ClearAll_Button: TButton;
      Add_AND_Button: TButton;
      Add_OR_Button: TButton;
      Add_L_Button: TButton;
      Add_R_Button: TButton;
      Memo1: TMemo;
      CheckBox1: TCheckBox;
      FirstComboBox: TComboBox;
      SecondComboBox: TComboBox;
      CheckBox3: TCheckBox;
      UpSB: TSpeedButton;
      DownSB: TSpeedButton;
      Label4: TLabel;
      TabSheet5: TTabSheet;
      ComboBox1: TComboBox;
      AButton: TBitBtn;
      SG: TIDB_StringGrid;
      HeaderControl1: THeaderControl;
      TabSheet6: TTabSheet;
      IDB_DBGrid1: TIDB_DBGrid;
      DataSource1: TDataSource;
      DQuery1: TDQuery;
    CheckBox4: TCheckBox;
    CheckBoxDuplicates: TCheckBox;
      procedure Button1Click (Sender: TObject);
      procedure Button2Click (Sender: TObject);
      procedure Button3Click (Sender: TObject);
      procedure Button4Click (Sender: TObject);
      procedure FormShow (Sender: TObject);
      procedure Button5Click (Sender: TObject);
      procedure Button6Click (Sender: TObject);
      procedure Button7Click (Sender: TObject);
      procedure Button8Click (Sender: TObject);
      procedure RadioButton1Click (Sender: TObject);
      procedure ValuesFieldsListBoxClick (Sender: TObject);
      procedure TabSheet2Show (Sender: TObject);
      procedure TabSheet4Show (Sender: TObject);
      procedure ClearAll_ButtonClick (Sender: TObject);
      procedure Add_AND_ButtonClick (Sender: TObject);
      procedure Add_OR_ButtonClick (Sender: TObject);
      procedure Add_L_ButtonClick (Sender: TObject);
      procedure Add_R_ButtonClick (Sender: TObject);
      procedure Add_NewStatement_ButtonClick (Sender: TObject);
      procedure RadioButton8Click (Sender: TObject);
      procedure CheckBox3Click (Sender: TObject);
      procedure SecondComboBoxKeyPress (Sender: TObject; var Key: Char);
      procedure AvailableTableFieldsListBoxDblClick (Sender: TObject);
      procedure SelectedTableFieldsListBoxDblClick (Sender: TObject);
      procedure AvailableSelectedFieldsListBoxDblClick (Sender: TObject);
      procedure OrdersFieldsListBoxDblClick (Sender: TObject);
      procedure UpSBClick (Sender: TObject);
      procedure DownSBClick (Sender: TObject);
      procedure FormCreate (Sender: TObject);
      procedure SGMouseUp (Sender: TObject; Button: TMouseButton;
         Shift: TShiftState; X, Y: Integer);
      procedure HeaderControl1SectionResize (HeaderControl: THeaderControl;
         Section: THeaderSection);
      procedure SGTopLeftChanged (Sender: TObject);
      procedure ComboBox1Change (Sender: TObject);
      procedure AButtonClick (Sender: TObject);
      procedure SGClick (Sender: TObject);
      procedure OKButtonClick (Sender: TObject);
      procedure FirstComboBoxEnter (Sender: TObject);
      procedure SecondComboBoxEnter (Sender: TObject);
      procedure Button9Click (Sender: TObject);
      procedure Add_NewStatement_ButtonEnter (Sender: TObject);
      procedure FirstComboBoxExit (Sender: TObject);
      procedure TabSheet6Show (Sender: TObject);
      procedure ExtPageControl1Change (Sender: TObject);
      procedure IDB_DBGrid1KeyUp (Sender: TObject; var Key: Word;
         Shift: TShiftState);
      procedure IDB_DBGrid1CellClick (Column: TColumn);
      {brovak}
      procedure StatusPreview;
      procedure GiveMeStatus;
      procedure IDB_DBGrid1DrawDataCell (Sender: TObject; const Rect: TRect;
         Field: TField; State: TGridDrawState);
    procedure CheckBox4Click(Sender: TObject);
      {brovak}
   private
      { Private declarations }
      IntValidator: TIDB_IntValidator;
      FloatValidator: TIDB_FloatValidator;
      IntValidator1: TIntValidator;
      FloatValidator1: TFloatValidator;

      AProject: Pointer;
      AWorkMode: TWorkMode;
      sAttTableName: AnsiString;
      sLayerName: AnsiString;
      AQuery: TDQuery;
      AStructMan: TIDB_StructMan;
      iOperator: Integer;
      {brovak}
      RecordsStatusDummy: AnsiString;
      {brovak}
      procedure ShowOtherParts (bShow: Boolean);
      procedure MakeUIForWrongType;
      procedure MakeUIForIntegerType;
      procedure MakeUIForStringType;
      procedure MakeUIForBooleanType;
      procedure MakeUIForRange (bRange: Boolean); // If bRange equals TRUE, the user interface must be made for BETWEEN condition.
      procedure AddInWhereClause (sWhatMustbeAdded: AnsiString);
      procedure FillCombosWithExistsingValues (sFieldName: AnsiString);
      function GenerateSQLText (bGenerateForUser: Boolean): AnsiString; overload;
      function GenerateSQLText (var sSQL: AnsiString): Boolean; overload;
      function MakeQueryPreview: Boolean;
   public
      { Public declarations }
      constructor Create (AProject: Pointer; AOwner: TComponent; ADataAccessObject: TIDB_Access2DB_Level2; sLayerName, sAttributeTableName: AnsiString); overload;
      constructor Create (AProject: Pointer; AOwner: TComponent; AMaster: TDMaster; AStructureManager: TIDB_StructMan; sLayerName: AnsiString; iLayerIndex: Integer); overload;
      destructor Free;
      function GetSQLText: AnsiString;
      function GetWhereClause: AnsiString;
      function GetTableNames: AnsiString;

      procedure InitializeGrid (sCurrentFieldName: AnsiString);
      function MakeIt (sFieldName, sValue: AnsiString): AnsiString;
      procedure DisconnectAllValidators;
      procedure DrawComboBox (bShowIt: Boolean = TRUE);
   end;

implementation
{$R *.DFM}

uses
   IDB_Utils, IDB_LanguageManager, IDB_Consts, IDB_ExpressionBuilder, IDB_MainManager;
var
   arOperators: array[1..6] of AnsiString = ('=', '<=', '<', '>=', '>', 'LIKE');
   ////////////////////////////////////////////////////////////////////////////////

constructor TIDB_QBForm.Create (AProject: Pointer; AOwner: TComponent; ADataAccessObject: TIDB_Access2DB_Level2; sLayerName, sAttributeTableName: AnsiString);
var
   ATable: TDTable;
begin
   inherited Create (AOwner);
   SELF.AProject := AProject;
   SELF.AWorkMode := wmSQLBuilder;
   SELF.sLayerName := sLayerName;
   SELF.sAttTableName := sAttributeTableName;
   AQuery := TDQuery.Create (nil);
   AQuery.Master := ADataAccessObject.DMaster;
   SELF.AStructMan := TIDB_StructMan.Create (AProject, ADataAccessObject);
   ATable := TDTable.Create (nil);
   ATable.Master := ADataAccessObject.DMaster;
   ATable.TableName := sAttributeTableName;
   AStructMan.DetectStructure (ATable);
   ATable.Close;
   ATable.Free;

   TabSheet5.TabVisible := FALSE;
   ExtPageControl1.ActivePage := TabSheet1;

   DQuery1.Master := ADataAccessObject.DMaster;
end;

constructor TIDB_QBForm.Create (AProject: Pointer; AOwner: TComponent; AMaster: TDMaster; AStructureManager: TIDB_StructMan; sLayerName: AnsiString; iLayerIndex: Integer);
begin
   inherited Create (AOwner);
   SELF.AProject := AProject;
   SELF.AWorkMode := wmUpdateBuilder;
   SELF.sLayerName := sLayerName;

   if not AnIDBMainMan.UseNewATTNames (AProject) then
      SELF.sAttTableName := cs_AttributeTableNamePrefix + IntToStr (iLayerIndex)
   else
      SELF.sAttTableName := cs_AttributeTableNamePrefix + '_' + sLayerName;

   SELF.AStructMan := AStructureManager;
   AQuery := TDQuery.Create (nil);
   AQuery.Master := AMaster;
   InitializeGrid ('');

   TabSheet1.TabVisible := FALSE;
   TabSheet2.Enabled := TRUE;
   TabSheet3.TabVisible := FALSE;
   TabSheet4.Enabled := TRUE;

   {Query preview}
   TabSheet6.TabVisible := FALSE;
   //  Button9.Visible := FALSE;

   ExtPageControl1.ActivePage := TabSheet5;
   OKButton.Enabled := TRUE;
end;

// ++ Cadmensky Version 2.3.2

destructor TIDB_QBForm.Free;
begin
   SELF.AQuery.Free;
   SELF.AStructMan.Free;
   inherited Free;
end;
// -- Cadmensky Version 2.3.2

procedure TIDB_QBForm.FormCreate (Sender: TObject);
begin
   ALangManager.MakeReplacement (SELF);

   IntValidator := TIDB_IntValidator.Create (SELF);
   FloatValidator := TIDB_FloatValidator.Create (SELF);

   IntValidator1 := TIntValidator.Create (SELF);
   IntValidator1.FormatWhenExit := FALSE;
   IntValidator1.ValidateOnExit := FALSE;
   IntValidator1.MaxLength := 100;

   FloatValidator1 := TFloatValidator.Create (SELF);
   FloatValidator1.FormatWhenExit := FALSE;
   FloatValidator1.ValidateOnExit := FALSE;
   FloatValidator1.MaxLength := 100;
   FloatValidator1.Commas := 20;
end;

procedure TIDB_QBForm.ShowOtherParts (bShow: Boolean);
begin
   TabSheet2.Enabled := bShow;
   TabSheet3.Enabled := bShow;
   TabSheet4.Enabled := bShow;
   ExtPageControl1.Invalidate;
   OKButton.Enabled := bShow;
   UpSB.Enabled := bShow;
   DownSB.Enabled := bShow;
end;

procedure TIDB_QBForm.AddInWhereClause (sWhatMustbeAdded: AnsiString);
var
   iPL, iPC, iSS: Integer;
   sData: AnsiString;
begin
   // I check what line and what place in this line I am positioned.
   iSS := Memo2.SelStart;
   iPL := SendMessage (Memo2.Handle, EM_LINEFROMCHAR, Memo2.SelStart, 0);
   iPC := SendMessage (memo2.Handle, EM_LINEINDEX, iPL, 0);
   iPC := Memo2.SelStart - iPC + 1;
   // Insert new items.
   sData := Memo2.Lines[iPL];
   sData := Copy (sData, 1, iPC - 1) + sWhatMustBeAdded + Copy (sData, iPC, Length (sData));
   Memo2.Lines[iPL] := sData;
   Memo2.SelStart := iSS + Length (sWhatMustBeAdded);
   Memo2.SelLength := 0;
end;

procedure TIDB_QBForm.MakeUIForWrongType;
begin
   RadioButton1.Enabled := FALSE; // =
   RadioButton2.Enabled := FALSE; // <=
   RadioButton3.Enabled := FALSE; // <
   RadioButton4.Enabled := FALSE; // >=
   RadioButton5.Enabled := FALSE; // >
   RadioButton6.Enabled := FALSE; // LIKE
   RadioButton7.Enabled := FALSE; // IS NULL
   RadioButton9.Enabled := FALSE; // BETWEEN
   BoolValComboBox.Visible := FALSE;
end;

// I make UserInterface for field that is Integer type (the same for byte type, currency type, etc.

procedure TIDB_QBForm.MakeUIForIntegerType;
begin
   RadioButton1.Enabled := TRUE; // =
   RadioButton2.Enabled := TRUE; // <=
   RadioButton3.Enabled := TRUE; // <
   RadioButton4.Enabled := TRUE; // >=
   RadioButton5.Enabled := TRUE; // >
   RadioButton6.Enabled := FALSE; // LIKE
   RadioButton7.Enabled := TRUE; // IS NULL
   RadioButton9.Enabled := TRUE; // BETWEEN
   BoolValComboBox.Visible := FALSE;
   FirstComboBox.Visible := TRUE;
end;

// I make UserInterface for field that is String type.

procedure TIDB_QBForm.MakeUIForStringType;
begin
   RadioButton1.Enabled := TRUE; // =
   RadioButton2.Enabled := TRUE; // <=
   RadioButton3.Enabled := TRUE; // <
   RadioButton4.Enabled := TRUE; // >=
   RadioButton5.Enabled := TRUE; // >
   RadioButton6.Enabled := TRUE; // LIKE
   RadioButton7.Enabled := TRUE; // IS NULL
   RadioButton9.Enabled := TRUE; // BETWEEN
   BoolValComboBox.Visible := FALSE;
   FirstComboBox.Visible := TRUE;
end;

// I make UserInterface for field that is Logical (boolean) type.

procedure TIDB_QBForm.MakeUIForBooleanType;
begin
   RadioButton1.Enabled := TRUE; // =
   RadioButton2.Enabled := FALSE; // <=
   RadioButton3.Enabled := FALSE; // <
   RadioButton4.Enabled := FALSE; // >=
   RadioButton5.Enabled := FALSE; // >
   RadioButton6.Enabled := FALSE; // LIKE
   RadioButton7.Enabled := TRUE; // IS NULL
   RadioButton9.Enabled := FALSE; // BETWEEN
   BoolValComboBox.Visible := TRUE;
   FirstComboBox.Visible := FALSE;
end;

procedure TIDB_QBForm.MakeUIForRange (bRange: Boolean);
begin
   if bRange then
   begin
      Label1.Visible := FALSE; // 'Value'
      Label3.Visible := TRUE; // 'Start of range'
      Label2.Visible := TRUE; // 'End of range'
      SecondComboBox.Visible := TRUE;
   end
   else
   begin
      Label1.Visible := TRUE; // 'Value'
      Label3.Visible := FALSE; // 'Start of range'
      Label2.Visible := FALSE; // 'End of range'
      SecondComboBox.Visible := FALSE;
   end;
end;

procedure TIDB_QBForm.Button1Click (Sender: TObject);
begin
   if AvailableTableFieldsListBox.Items.Count > 0 then
   begin
      // Add it in list for condition's setting.
      AddOneValue (AvailableTableFieldsListBox, ValuesFieldsListBox, FALSE);
      if ValuesFieldsListBox.ItemIndex < 0 then ValuesFieldsListBox.ItemIndex := 0;
      // And add it in list for order's setting.
      AddOneValue (AvailableTableFieldsListBox, AvailableSelectedFieldsListBox, FALSE);
      if AvailableSelectedFieldsListBox.ItemIndex < 0 then AvailableSelectedFieldsListBox.ItemIndex := 0;
   end;
   AddOneValue (AvailableTableFieldsListBox, SelectedTableFieldsListBox);
   ShowOtherParts (TRUE);
end;

procedure TIDB_QBForm.Button2Click (Sender: TObject);
begin
   // Add it in list for condition's setting.
   AddAllValues (AvailableTableFieldsListBox, ValuesFieldsListBox, FALSE);
   if ValuesFieldsListBox.ItemIndex < 0 then ValuesFieldsListBox.ItemIndex := 0;
   // And add it in list for order's setting.
   AddAllValues (AvailableTableFieldsListBox, AvailableSelectedFieldsListBox, FALSE);
   if AvailableSelectedFieldsListBox.ItemIndex < 0 then AvailableSelectedFieldsListBox.ItemIndex := 0;
   AddAllValues (AvailableTableFieldsListBox, SelectedTableFieldsListBox);
   ShowOtherParts (TRUE);
end;

procedure TIDB_QBForm.Button3Click (Sender: TObject);
var
   iP: Integer;
begin
   if SelectedTableFieldsListBox.Items.Count > 0 then
   begin
      // Mark this field as unselected.
      // Delete if from list for condition's setting.
      iP := ValuesFieldsListBox.Items.IndexOf (SelectedTableFieldsListBox.Items[SelectedTableFieldsListBox.ItemIndex]);
      if iP > -1 then ValuesFieldsListBox.Items.Delete (iP);
      if ValuesFieldsListBox.ItemIndex < 0 then ValuesFieldsListBox.ItemIndex := 0;
      // Delete it from list for order's setting
      iP := AvailableSelectedFieldsListBox.Items.IndexOf (SelectedTableFieldsListBox.Items[SelectedTableFieldsListBox.ItemIndex]);
      if iP > -1 then AvailableSelectedFieldsListBox.Items.Delete (iP);
      if AvailableSelectedFieldsListBox.ItemIndex < 0 then AvailableSelectedFieldsListBox.ItemIndex := 0;
      iP := OrdersFieldsListBox.Items.IndexOf (SelectedTableFieldsListBox.Items[SelectedTableFieldsListBox.ItemIndex]);
      if iP > -1 then
      begin
         OrdersFieldsListBox.Items.Delete (iP);
      end;
      if OrdersFieldsListBox.ItemIndex < 0 then OrdersFieldsListBox.ItemIndex := 0;
   end;
   AddOneValue (SelectedTableFieldsListBox, AvailableTableFieldsListBox);
   if SelectedTableFieldsListBox.Items.Count < 1 then
      ShowOtherParts (FALSE);
end;

procedure TIDB_QBForm.Button4Click (Sender: TObject);
begin
   ValuesFieldsListBox.Items.Clear;
   AvailableSelectedFieldsListBox.Items.Clear;
   OrdersFieldsListBox.Items.Clear;
   AddAllValues (SelectedTableFieldsListBox, AvailableTableFieldsListBox);
   ShowOtherParts (FALSE);
end;

procedure TIDB_QBForm.FormShow (Sender: TObject);
var
   iI, iWidth: Integer;
   sFieldName: AnsiString;
begin
   SELF.iOperator := 1;
   case AWorkMode of
      wmSQLBuilder:
         begin
            for iI := 0 to SELF.AStructMan.GetFieldCount - 1 do
            begin
               sFieldName := AStructMan.GetFieldName (iI);
               if FieldIsInternalUsageField (sFieldName) then CONTINUE;
               SELF.AvailableTableFieldsListBox.Items.Add (sFieldName);
            end;
            for iI := 0 to AvailableTableFieldsListBox.Items.Count - 1 do
               if (AnsiUpperCase (AvailableTableFieldsListBox.Items[iI]) = 'PROGISID') then
               begin
                  AvailableTableFieldsListBox.ItemIndex := iI;
                  Button1.Click;
                  BREAK;
               end;
            AvailableTableFieldsListBox.ItemIndex := 0;
            SELF.ShowOtherParts (TRUE);
            if checkbox4.Visible=False then ExtPageControl1.ActivePage:=TabSheet4;
         end;
      wmUpdateBuilder:
         begin
            SG.DefaultColWidth := (SG.ClientWidth div 2) - 2;
            iWidth := SG.DefaultColWidth + 2;
            HeaderControl1.Sections[0].Width := iWidth;
            HeaderControl1.Sections[1].Width := iWidth;
            DrawComboBox;
            SGClick (SENDER);
         end;
   end; // Case end

end;

procedure TIDB_QBForm.Button5Click (Sender: TObject);
begin
   AddOneValue (AvailableSelectedFieldsListBox, OrdersFieldsListBox);
end;

procedure TIDB_QBForm.Button6Click (Sender: TObject);
begin
   AddAllValues (AvailableSelectedFieldsListBox, OrdersFieldsListBox);
end;

procedure TIDB_QBForm.Button7Click (Sender: TObject);
begin
   AddOneValue (OrdersFieldsListBox, AvailableSelectedFieldsListBox);
end;

procedure TIDB_QBForm.Button8Click (Sender: TObject);
begin
   AddAllValues (OrdersFieldsListBox, AvailableSelectedFieldsListBox);
end;

procedure TIDB_QBForm.RadioButton1Click (Sender: TObject);
begin
   if not (SENDER is TRadioButton) then EXIT;
   SELF.iOperator := TRadioButton (SENDER).Tag;
   Label4.Visible := (SELF.iOperator = 6); // 'LIKE' operator
   {     brovak}
   if label4.visible = true then
      Label4.Caption := ALangManager.GetValue (273);
   {brovak}
   if SELF.AStructMan.GetFieldType (ValuesFieldsListBox.Items[ValuesFieldsListBox.ItemIndex]) = 5 then
      BoolValComboBox.Visible := TRUE
   else
      BoolValComboBox.Visible := FALSE;
   FirstComboBox.Visible := TRUE;
   Label1.Visible := TRUE;
   Label2.Visible := FALSE;
   Label3.Visible := FALSE;
   CheckBox1.Enabled := TRadioButton (SENDER).Tag >= 6;
   SELF.MakeUIForRange (TRadioButton (SENDER).Tag = 8);
end;

procedure TIDB_QBForm.FillCombosWithExistsingValues (sFieldName: AnsiString);
var
   iI: Integer;
   iFieldType: Integer;
   bFieldHasFloatType: Boolean;
   sData: AnsiString;
begin
   if AQuery.Active then AQuery.Close;
   AQuery.SQL.Clear;
   AQuery.SQL.Add ('SELECT DISTINCT [' + sFieldName + ']');
   AQuery.SQL.Add ('FROM [' + SELF.sAttTableName + ']');
   AQuery.Open;
   {
   1 - String
   2 - SmallInt
   3 - Integer
   5 - Boolean
   6 - Float
   7 - Currency
   11 - DateTime
   12 - Byte
   }
   iFieldType := SELF.AStructMan.GetFieldType (ValuesFieldsListBox.Items[ValuesFieldsListBox.ItemIndex]);
   bFieldHasFloatType := (iFieldType = 6);

   for iI := 0 to AQuery.RecordCount - 1 do
   begin
      try
         sData := AQuery.Fields[0].AsString;
         if bFieldHasFloatType then sData := ReplaceChars (sData, DecimalSeparator, '.');
         FirstComboBox.Items.Add (sData);
         SecondComboBox.Items.Add (sData);
      except
      end;
      AQuery.Next;
   end;
   AQuery.Close;
end;

procedure TIDB_QBForm.RadioButton8Click (Sender: TObject);
begin
   SELF.FillCombosWithExistsingValues (ValuesFieldsListBox.Items[ValuesFieldsListBox.ItemIndex]);
   BoolValComboBox.Visible := FALSE;
   Label1.Visible := TRUE;
   Label2.Visible := FALSE;
   Label3.Visible := FALSE;
   FirstComboBox.Visible := TRUE;
end;

procedure TIDB_QBForm.ValuesFieldsListBoxClick (Sender: TObject);
var
   iOpIx: Integer;
   iI: Integer;
begin
   {
   1 - String
   2 - SmallInt
   3 - Integer
   5 - Boolean
   6 - Float
   7 - Currency
   11 - DateTime
   12 - Byte
   }
        // Make the user interface for BETWEEN operator
   SELF.MakeUIForRange (iOperator = 8);
   for iI := 0 to SELF.ComponentCount - 1 do
      if (SELF.Components[iI] is TRadioButton) and (TRadioButton (SELF.Components[iI]).Tag = iOpIx) then
      begin
         TRadioButton (SELF.Components[iI]).Checked := TRUE;
         BREAK;
      end;
   // Here I make the user interface for different field's type.
   SELF.FirstComboBox.Visible := FALSE;
   case SELF.AStructMan.GetFieldType (ValuesFieldsListBox.Items[ValuesFieldsListBox.ItemIndex]) of
      1: SELF.MakeUIForStringType;
      2, 3, 6, 7, 12: SELF.MakeUIForIntegerType;
      5: SELF.MakeUIForBooleanType;
   else
      SELF.MakeUIForWrongType;
   end; // case end

   FirstComboBox.Text := '';
   SecondComboBox.Text := '';
   FirstComboBox.Items.Clear;
   SecondComboBox.Items.Clear;
   if CheckBox3.Checked then
      SELF.FillCombosWithExistsingValues (ValuesFieldsListBox.Items[ValuesFieldsListBox.ItemIndex]);

   RadioButton1.Checked := TRUE;
end;

procedure TIDB_QBForm.TabSheet2Show (Sender: TObject);
begin
   ValuesFieldsListBoxClick (SENDER);
   RadioButton1Click (SENDER);
end;

procedure TIDB_QBForm.TabSheet4Show (Sender: TObject);
var
   sSQL: AnsiString;
begin
   checkbox4click(sender);
   if not checkbox4.checked then Memo1.Lines.Clear;
   checkboxDuplicates.Visible:= checkbox4.Visible;
   case AWorkMode of
      wmSQLBuilder: if not checkbox4.checked then Memo1.Lines.Text := SELF.GenerateSQLText (TRUE);
      wmUpdateBuilder:
         begin
            if SELF.GenerateSQLText (sSQL) then
               Memo1.Lines.Text := sSQL
         end;
   end; // Case end
end;

// I filter user's Key In this procedure. If field has integer type of any kind, the user can press only
// numeric keys!!! The user can press any keys with string type field. This handling won't work with
// boolean type field because the user works with TComboBox in this case.

function TIDB_QBForm.GetSQLText: AnsiString;
begin
   if checkbox4.checked then RESULT:= memo1.lines.text else
      RESULT := SELF.GenerateSQLText (FALSE);
end;

procedure TIDB_QBForm.ClearAll_ButtonClick (Sender: TObject);
begin
   Memo2.Clear;
end;

procedure TIDB_QBForm.Add_AND_ButtonClick (Sender: TObject);
begin
   SELF.AddInWhereClause (' AND ');
end;

procedure TIDB_QBForm.Add_OR_ButtonClick (Sender: TObject);
begin
   SELF.AddInWhereClause (' OR ');
end;

procedure TIDB_QBForm.Add_L_ButtonClick (Sender: TObject);
begin
   SELF.AddInWhereClause (' ( ');
end;

procedure TIDB_QBForm.Add_R_ButtonClick (Sender: TObject);
begin
   SELF.AddInWhereClause (' ) ');
end;

procedure TIDB_QBForm.Add_NewStatement_ButtonEnter (Sender: TObject);
var
   iI: Integer;
begin
   iI := 0;
   if iI = 0 then
      Inc (iI);
end;

procedure TIDB_QBForm.Add_NewStatement_ButtonClick (Sender: TObject);
var
   sData, sFieldName: AnsiString;
   iFieldType: Integer;
   bThisFieldIsOfStringType,
      bThisFieldIsOfBooleanType: Boolean;
begin
   if ValuesFieldsListBox.ItemIndex < 0 then EXIT;
   sFieldName := ValuesFieldsListBox.Items[ValuesFieldsListBox.ItemIndex];
   sData := '[' + sFieldName + '] ';
   iFieldType := AStructMan.GetFieldType (sFieldName);
   bThisFieldIsOfStringType := (iFieldType = 1);
   bThisFieldIsOfBooleanType := (iFieldType = 5);
   case iOperator of
      1..6:
         begin // =, <, > etc
            if (iOperator = 6) and (CheckBox1.Checked) then sData := sData + 'NOT ';
            sData := sData + ' ' + arOperators[iOperator] + ' ';
            {CHANGED BY BROVAK}
            if bThisFieldIsOfStringType then
            begin;
               if (iOperator <> 6) then
                  sData := sData + '"' + TRIM (FirstComboBox.Text) + '" '
               else
                  sData := sData + '"' + TRIM (FirstComboBox.Text) + '%' + '" '
            end
               {BROVAK}
            else
               if not bThisFieldIsOfBooleanType then
                  sData := sData + ReplaceChars (FirstComboBox.Text, DecimalSeparator, '.')
               else
                  if BoolValComboBox.ItemIndex = 0 then
                     sData := sData + 'FALSE '
                  else
                     sData := sData + 'TRUE ';
         end;
      7:
         begin // 'Not null'/'Is not null' SQL statement
            if CheckBox1.Checked then
               sData := sData + ' IS NOT NULL '
            else
               sData := sData + ' IS NULL ';
         end;
      8:
         begin // Between
            if CheckBox1.Checked then sData := sData + 'NOT ';
            if bThisFieldIsOfStringType then
               sData := sData + ' BETWEEN ''' + FirstComboBox.Text + ''' AND ''' + SecondComboBox.Text + ''''
            else
               sData := sData + ' BETWEEN ' + FirstComboBox.Text + ' AND ' + SecondComboBox.Text;
         end;
   end; // case end;
   SELF.AddInWhereClause ('(' + sData + ')');
end;

procedure TIDB_QBForm.CheckBox3Click (Sender: TObject);
begin
   FirstComboBox.Items.Clear;
   SecondComboBox.Items.Clear;
   SELF.FillCombosWithExistsingValues (ValuesFieldsListBox.Items[ValuesFieldsListBox.ItemIndex]);
end;

procedure TIDB_QBForm.SecondComboBoxKeyPress (Sender: TObject; var Key: Char);
begin

end;

{ This procedure generates SQL text fro Query builder. }

function TIDB_QBForm.GenerateSQLText (bGenerateForUser: Boolean): AnsiString;
var
   slSQL: TStringList;

   procedure TruncateLastComma;
   begin
      slSQL[slSQL.Count - 1] := Copy (slSQL[slSQL.Count - 1], 1, Length (slSQL[slSQL.Count - 1]) - 1);
   end;

var
   sTableName: AnsiString;
   iI: Integer;
   bb: boolean;
begin
   RESULT := '';
   case AWorkMode of
      wmSQLBuilder:
         begin
            // 212 = Table of the layer '' %s''
            if bGenerateForUser then
               sTableName := Format (ALangManager.GetValue (212), [SELF.sLayerName])
            else
               sTableName := SELF.sAttTableName;
            // And now I start the work.
            slSQL := TStringList.Create;
            slSQL.Clear;
            if SELF.CheckBox2.Checked then
               slSQL.Add ('SELECT DISTINCT')
            else
               slSQL.Add ('SELECT');
            // Now I'm going to write down all selected field names.
            for iI := 0 to SelectedTableFieldsListBox.Items.Count - 1 do
               slSQL.Add ('[' + SelectedTableFieldsListBox.Items[iI] + '],');

            if ((not bGenerateForUser) and (not CheckBox2.Checked)) or (checkbox4.Checked) then
             begin
             slSQL.Add ('[' + cs_InternalID_FieldName + '],');
             bb:=False;
             for iI:= 0 to SelectedTableFieldsListbox.Items.count -1 do
                 if uppercase(SelectedTableFieldsListbox.Items[iI])='PROGISID' then bb:=True;
             if not bb then slSQL.Add('[PROGISID],');
             end;
            //                                 ^^^ SELECT DISTINCT
            TruncateLastComma;
            slSQL.Add ('');
            // Now I write down all table's names (FROM statement of SQL).
            slSQL.Add ('FROM');
            slSQL.Add ('[' + sTableName + ']');
            slSQL.Add ('');

            // I try to detect whether there are fields with conditions or not.
            // If there are fields with conditions, I'm going to create WHERE statement of SQL.
            if checkboxDuplicates.Checked then
            begin //HAVING  -> for Duplcate records
            slSQL.Add('WHERE [' + SelectedTableFieldsListBox.Items[0] + '] IN ');
            slSQL.Add('(SELECT [' + SelectedTableFieldsListBox.Items[0] + '] FROM [' + sTableName + '] As Tmp' );
            slSQL.Add('GROUP BY ');
            for iI := 0 to SelectedTableFieldsListBox.Items.Count - 1 do
               slSQL.Add ('[' + SelectedTableFieldsListBox.Items[iI] + '],');
            TruncateLastComma;
            slSQL.Add('HAVING Count(*)>1  AND [' + SelectedTableFieldsListBox.Items[0] + '] = [' + sTableName + '].[' + SelectedTableFieldsListBox.Items[0]+ '])');


           { for iI := 0 to SelectedTableFieldsListBox.Items.Count - 1 do
               slSQL.Add ('[' + SelectedTableFieldsListBox.Items[iI] + '],');
            //if (not bGenerateForUser) and (not CheckBox2.Checked) then slSQL.Add ('[' + cs_InternalID_FieldName + '],');
            TruncateLastComma;
            slSQL.Add ('HAVING ');
            for iI := 0 to SelectedTableFieldsListBox.Items.Count - 1 do
               slSQL.Add ('COUNT ([' + SelectedTableFieldsListBox.Items[iI] + ']) > 1 AND ');
            slSQl[slSQl.Count-1]:= stringreplace(slSQl[slSQl.Count-1],' AND ',' ',[]); }
            slSQL.Add ('');
            end else

            begin //WHERE
            if Trim (Memo2.Text) <> '' then
            begin
               slSQL.Add ('WHERE');
               for iI := 0 to Memo2.Lines.Count - 1 do
                  slSQL.Add (Memo2.Lines[iI]);
               slSQL.Add ('');
            end;
            end;
            // I try to detect whether there are fields that must be used for the sorting of query result or not.
            // If there are fields that must be used for the sorting of query result, I'm going to create ORDER BY statement of SQL.

            if OrdersFieldsListBox.Items.Count > 0 then
            begin
               slSQL.Add ('ORDER BY');
               for iI := 0 to OrdersFieldsListBox.Items.Count - 1 do
                  slSQL.Add ('[' + OrdersFieldsListBox.Items[iI] + '],');
               TruncateLastComma;
               slSQL.Add ('');
            end;

            RESULT := slSQL.Text;
            slSQL.Free;
         end;
      wmUpdateBuilder:
         begin
         end;
   end; // Case end
end;

{ This procedure generates SQL text for Update builder }

function TIDB_QBForm.GenerateSQLText (var sSQL: AnsiString): Boolean;
var
   slSQL: TStringList;

   procedure TruncateLastComma;
   begin
      slSQL[slSQL.Count - 1] := Copy (slSQL[slSQL.Count - 1], 1, Length (slSQL[slSQL.Count - 1]) - 2);
   end;

var
   iI: Integer;
   sTableName,
      sFieldName,
      sValue: AnsiString;
   bThereIsData: Boolean;
begin
   RESULT := FALSE;
   sSQL := '';
   // 212 = Table of the layer '' %s''
   sTableName := Format (ALangManager.GetValue (212), [SELF.sLayerName]);
   // And now I start the work.
   slSQL := TStringList.Create;
   slSQL.Clear;

   slSQL.Add ('UPDATE [' + sTableName + ']');
   slSQL.Add ('');
   slSQL.Add ('SET ');
   bThereIsData := FALSE;
   for iI := 0 to SG.RowCount - 1 do
   begin
      sFieldName := SG.Cells[0, iI];
      if Trim (sFieldName) = '' then CONTINUE;

      // The text can be in the inplcae editor, not in the grid yet.
      if (SG.Row = iI) and (SG.InplaceEditor <> nil) then
         sValue := SG.InplaceEditor.Text
      else
         sValue := SG.Cells[1, iI];

      if (AStructMan.GetFieldType (sFieldName) <> 1) and (Trim (sValue) = '') then CONTINUE;
      bThereIsData := TRUE;
      slSQL.Add ('[' + sFieldName + '] = ' + MakeIt (sFieldName, sValue) + ', ');
   end;
   if bThereIsData then
   begin
      TruncateLastComma;

      // I try to detect whether there are fields with conditions or not.
      // If there are fields with conditions, I'm going to create WHERE statement of SQL.
      if checkboxDuplicates.Checked then
            begin //HAVING  -> for Duplcate records
            slSQL.Add('GROUP BY ');
            for iI := 0 to SelectedTableFieldsListBox.Items.Count - 1 do
               slSQL.Add ('[' + SelectedTableFieldsListBox.Items[iI] + '],');
            TruncateLastComma;
            slSQL.Add ('HAVING ');
            for iI := 0 to SelectedTableFieldsListBox.Items.Count - 1 do
               slSQL.Add ('COUNT ([' + SelectedTableFieldsListBox.Items[iI] + ']) > 1 AND ');
            slSQl[slSQl.Count-1]:= stringreplace(slSQl[slSQl.Count-1],' AND ',' ',[]);
            slSQL.Add ('');
            end else
            begin
            if Trim (Memo2.Text) <> '' then
            begin
               slSQL.Add (#13#10 + 'WHERE');
               for iI := 0 to Memo2.Lines.Count - 1 do
                  slSQL.Add (Memo2.Lines[iI]);
            end;
            end;
      sSQL := slSQL.Text;
      RESULT := TRUE;
   end;
   slSQL.Clear;
   slSQL.Free;
end;

{ This function returns the WHERE clause of generated SQL statement. This data will be able to use
  to update data not in the atrribute table as whole but only in result of the query. }

function TIDB_QBForm.GetWhereClause: AnsiString;
var
   iI: Integer;
begin
   // I try to detect whether there are fields with conditions or not.
   // If there are fields with conditions, I'm going to create WHERE statement of SQL.
   RESULT := '';
   if Trim (Memo2.Text) = '' then EXIT;
   for iI := 0 to Memo2.Lines.Count - 1 do
      RESULT := RESULT + ' ' + Memo2.Lines[iI];
end;

{These 4 procedures simulated add or remove one value button click.}

procedure TIDB_QBForm.AvailableTableFieldsListBoxDblClick (Sender: TObject);
begin
   Button1.Click;
end;

procedure TIDB_QBForm.SelectedTableFieldsListBoxDblClick (Sender: TObject);
begin
   Button3.Click;
end;

procedure TIDB_QBForm.AvailableSelectedFieldsListBoxDblClick (Sender: TObject);
begin
   Button5.Click;
end;

procedure TIDB_QBForm.OrdersFieldsListBoxDblClick (Sender: TObject);
begin
   Button7.Click;
end;

procedure TIDB_QBForm.UpSBClick (Sender: TObject);
var
   sTemp: AnsiString;
begin
   if SELF.SelectedTableFieldsListBox.ItemIndex < 1 then EXIT;
   sTemp := SelectedTableFieldsListBox.Items[SelectedTableFieldsListBox.ItemIndex - 1];
   SelectedTableFieldsListBox.Items[SelectedTableFieldsListBox.ItemIndex - 1] := SelectedTableFieldsListBox.Items[SelectedTableFieldsListBox.ItemIndex];
   SelectedTableFieldsListBox.Items[SelectedTableFieldsListBox.ItemIndex] := sTemp;
   SelectedTableFieldsListBox.ItemIndex := SelectedTableFieldsListBox.ItemIndex - 1;
end;

procedure TIDB_QBForm.DownSBClick (Sender: TObject);
var
   sTemp: AnsiString;
begin
   if SelectedTableFieldsListBox.ItemIndex = SelectedTableFieldsListBox.Items.Count - 1 then EXIT;
   sTemp := SelectedTableFieldsListBox.Items[SelectedTableFieldsListBox.ItemIndex + 1];
   SelectedTableFieldsListBox.Items[SelectedTableFieldsListBox.ItemIndex + 1] := SelectedTableFieldsListBox.Items[SelectedTableFieldsListBox.ItemIndex];
   SelectedTableFieldsListBox.Items[SelectedTableFieldsListBox.ItemIndex] := sTemp;
   SelectedTableFieldsListBox.ItemIndex := SelectedTableFieldsListBox.ItemIndex + 1;
end;

function TIDB_QBForm.GetTableNames: AnsiString;
begin
   RESULT := sAttTableName;
end;

procedure TIDB_QBForm.InitializeGrid (sCurrentFieldName: AnsiString);
var
   iI, iJ: Integer;
begin
   SG.RowCount := 2;
   SG.Row := 0;
   for iI := 0 to SG.RowCount - 1 do
      for iJ := 0 to SG.ColCount - 1 do
         SG.Cells[iJ, iI] := '';

   iI := ComboBox1.Items.IndexOf (sCurrentFieldName);
   if iI < 0 then iI := 0;
   ComboBox1.ItemIndex := iI;
   SG.Cells[0, 0] := ComboBox1.Items[ComboBox1.ItemIndex];
end;

function TIDB_QBForm.MakeIt (sFieldName, sValue: AnsiString): AnsiString;
const
   cs_SpecialSign = '$';
   cs_CancelSign = '\';
   cs_UseFieldName = 'F';
   cs_UseCurrentValue = 'C';
   cs_StBracket = '(';
   cs_EndBracket = ')';
type
   TMode = (wmUnknown, wmSpecialSignWasAdded, wmOrdinarySignWasAdded);
var
   iPStBr, iPEndBr: Integer;
   sResult,
      sCurrentChar: AnsiString;
   bFollowingSignHasPower: Boolean;
   AMode: TMode;
begin
   RESULT := '';
   sResult := '';

   bFollowingSignHasPower := TRUE;
   AMode := wmUnknown;
   //// STRING TYPE
   if AStructMan.GetFieldType (sFieldName) = 1 then
   begin
      while Length (sValue) > 0 do
      begin
         sCurrentChar := Copy (sValue, 1, 1);
         if sCurrentChar = cs_CancelSign then
         begin
            bFollowingSignHasPower := FALSE;
            sValue := Copy (sValue, 2, Length (sValue));
         end
         else
         begin
            if (sCurrentChar = cs_SpecialSign) and bFollowingSignHasPower then
            begin
               sCurrentChar := AnsiUppercase (Copy (sValue, 2, 1));
               if (sCurrentChar = cs_UseCurrentValue) then
               begin
                  if AMode = wmOrdinarySignWasAdded then sResult := sResult + ''' + ';
                  sResult := sResult + '[' + sFieldName + '] + ';
                  sValue := Copy (sValue, 3, Length (sValue));
                  AMode := wmSpecialSignWasAdded;
               end
               else
               begin
                  if (sCurrentChar = cs_UseFieldName) then
                  begin
                     iPStBr := Pos (cs_StBracket, sValue);
                     if iPStBr = 0 then
                     begin
                        sValue := Copy (sValue, 1, 2) + cs_StBracket + Copy (sValue, 3, Length (sValue));
                        iPStBr := 3;
                     end;
                     iPEndBr := Pos (cs_EndBracket, sValue);
                     if iPEndBr = 0 then
                        if Length (Copy (sValue, iPStBr + 1, Length (sValue))) = 0 then
                        begin
                           sValue := Copy (sValue, 1, iPStBr) + cs_EndBracket + Copy (sValue, iPStBr + 1, Length (sValue));
                           iPEndBr := iPStBr + 1;
                        end
                        else
                        begin
                           iPEndBr := Length (sValue) + 1;
                           sValue := sValue + cs_EndBracket;
                        end;
                     if iPEndBr - iPStBr - 1 > 0 then
                     begin
                        if AMode = wmOrdinarySignWasAdded then sResult := sResult + ''' + ';
                        sResult := sResult + '[' + AnsiUpperCase (Copy (sValue, iPStBr + 1, iPEndBr - iPStBr - 1)) + '] + ';
                     end;
                     sValue := Copy (sValue, iPEndBr + 1, Length (sValue));
                     AMode := wmSpecialSignWasAdded;
                  end;
               end;
            end
            else
            begin
               if sResult = '' then
                  sResult := ''''
               else
                  if AMode = wmSpecialSignWasAdded then
                     sResult := sResult + '''';
               sResult := sResult + sCurrentChar;
               sValue := Copy (sValue, 2, Length (sValue));
               AMode := wmOrdinarySignWasAdded;
            end;
            bFollowingSignHasPower := TRUE;
         end;
      end; // While end

      case AMode of
         wmUnknown: sResult := '''''';
         wmOrdinarySignWasAdded: sResult := sResult + '''';
         wmSpecialSignWasAdded: sResult := Copy (sResult, 1, Length (sResult) - 3);
      end; // Case end
   end
      //// INTEGER TYPE
   else
   begin
      while Length (sValue) > 0 do
      begin
         sCurrentChar := Copy (sValue, 1, 1);
         if (sCurrentChar = cs_SpecialSign) and bFollowingSignHasPower then
         begin
            sCurrentChar := AnsiUppercase (Copy (sValue, 2, 1));
            if (sCurrentChar = cs_UseCurrentValue) then
            begin
               sResult := sResult + ' [' + sFieldName + '] ';
               sValue := Copy (sValue, 3, Length (sValue));
            end
            else
            begin
               if (sCurrentChar = cs_UseFieldName) then
               begin
                  iPStBr := Pos (cs_StBracket, sValue);
                  if iPStBr = 0 then
                  begin
                     sValue := Copy (sValue, 1, 2) + cs_StBracket + Copy (sValue, 3, Length (sValue));
                     iPStBr := 3;
                  end;
                  iPEndBr := Pos (cs_EndBracket, sValue);
                  if iPEndBr = 0 then
                     if Length (Copy (sValue, iPStBr + 1, Length (sValue))) = 0 then
                     begin
                        sValue := Copy (sValue, 1, iPStBr) + cs_EndBracket + Copy (sValue, iPStBr + 1, Length (sValue));
                        iPEndBr := iPStBr + 1;
                     end
                     else
                     begin
                        iPEndBr := Length (sValue) + 1;
                        sValue := sValue + cs_EndBracket;
                     end;
                  if iPEndBr - iPStBr - 1 > 0 then
                     sResult := sResult + ' [' + AnsiUpperCase (Copy (sValue, iPStBr + 1, iPEndBr - iPStBr - 1)) + '] ';
                  sValue := Copy (sValue, iPEndBr + 1, Length (sValue));
               end;
            end;
         end
         else
         begin
            sResult := sResult + sCurrentChar;
            sValue := Copy (sValue, 2, Length (sValue));
         end;
      end; // While end
      sResult := ReplaceChars (sResult, DecimalSeparator, '.');
   end;
   RESULT := sResult;
end;

procedure TIDB_QBForm.DisconnectAllValidators;
begin
   if IntValidator.Control <> nil then
      if IntValidator.Control is TCustomEdit then
         IntValidator.Edit := nil
      else
         IntValidator.ComboBox := nil;
   if FloatValidator.Control <> nil then
      if FloatValidator.Control is TCustomEdit then
         FloatValidator.Edit := nil
      else
         FloatValidator.ComboBox := nil;

   if IntValidator1.Control <> nil then
      if IntValidator1.Control is TCustomEdit then
         IntValidator1.Edit := nil
      else
         IntValidator1.ComboBox := nil;
   if FloatValidator1.Control <> nil then
      if FloatValidator1.Control is TCustomEdit then
         FloatValidator1.Edit := nil
      else
         FloatValidator1.ComboBox := nil;
end;

procedure TIDB_QBForm.DrawComboBox (bShowIt: Boolean = TRUE);
var
   ARect: TRect;
   iP: Integer;
begin
   if bShowIt then
   begin
      if ComboBox1.DroppedDown then ComboBox1.DroppedDown := FALSE;
      ARect := SG.CellRect (0, SG.Row);
      // Draw the ComboBox at the left column of the StringGrid.
      ComboBox1.Left := ARect.Left + 2;
      ComboBox1.Top := SG.Top + ARect.Top;
      ComboBox1.Width := ARect.Right - ARect.Left + 2;
      ComboBox1.Height := ARect.Bottom - ARect.Top;

      SG.Invalidate;
      Application.ProcessMessages;

      iP := ComboBox1.Items.IndexOf (SG.Cells[0, SG.Row]);
      if iP < 0 then
      begin
         iP := 0;
         ComboBox1.DroppedDown := TRUE;
      end;
      ComboBox1.ItemIndex := iP;
      ComboBox1.Visible := TRUE;

      ARect := SG.CellRect (1, SG.Row);
      AButton.Height := ARect.Bottom - ARect.Top + 2; //ComboBox1.Height;
      AButton.Left := ARect.Right - AButton.Height + 2;
      AButton.Top := ComboBox1.Top + 1;
      AButton.Width := AButton.Height;

      AButton.Visible := TRUE;
   end
   else
   begin
      ComboBox1.Visible := FALSE;
      AButton.Visible := FALSE;
   end;
end;

procedure TIDB_QBForm.SGMouseUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
   ACol, ARow: Integer;
begin
   HeaderControl1.Sections[0].Width := SG.ColWidths[0] + 2;
   HeaderControl1.Sections[1].Width := SG.ColWidths[1] + 2;
   SG.MouseToCell (X, Y, ACol, ARow);
   if (ACol > -1) and (ARow > -1) then
   begin
      SG.Row := ARow;
      DrawComboBox;
   end;
end;

procedure TIDB_QBForm.HeaderControl1SectionResize (HeaderControl: THeaderControl; Section: THeaderSection);
begin
   if HeaderControl1.Sections[0].Width > 0 then
      SG.ColWidths[0] := HeaderControl1.Sections[0].Width - 2;
   SG.ColWidths[1] := HeaderControl1.Sections[1].Width - 2;
   DrawComboBox;
end;

procedure TIDB_QBForm.SGTopLeftChanged (Sender: TObject);
begin
   if SG.LeftCol = 0 then
   begin
      HeaderControl1.Sections[0].Width := SG.ColWidths[0] + 2;
      DrawComboBox;
   end
   else
   begin
      HeaderControl1.Sections[0].Width := 0;
      DrawComboBox (FALSE);
   end;
end;

procedure TIDB_QBForm.ComboBox1Change (Sender: TObject);
var
   bThereWasNothing: Boolean;
   iI, iFieldType: Integer;
   sData {, sCurrentChar}: AnsiString;
begin
// ++ Cadmensky Version 2.3.4
   for iI := 0 to SG.RowCount - 1 do
      if (iI <> SG.Row) and
         (SG.Cells[0, iI] = ComboBox1.Items[ComboBox1.ItemIndex]) then
      begin
         ComboBox1.Visible := false;
         Exit;
      end;
// -- Cadmensky Version 2.3.4
   bThereWasNothing := Trim (SG.Cells[0, SG.Row]) = '';
   iFieldType := AStructMan.GetFieldType (ComboBox1.Items[ComboBox1.ItemIndex]);
   DisconnectAllValidators;
   if bThereWasNothing and
// ++ Cadmensky Version 2.3.4
      (SG.RowCount < AStructMan.GetFieldCount - 1) then
// -- Cadmensky Version 2.3.4
      SG.RowCount := SG.RowCount + 1
   else
   begin
      sData := SG.InplaceEditor.Text; // Cells[1, SG.Row];
      iI := 1;
      case iFieldType of
         1:
            begin
            end; // String
         2, // SmallInt
            3, // Integer
            12: // Byte
            begin
               IntValidator.Edit := SG.InplaceEditor;
               if not IntValidator.IsValidInput (sData, iI) then
               begin
                  SG.InplaceEditor.Text := '';
                  SG.Cells[1, SG.Row] := '';
               end;
            end;
         6, // Float
            7: // Currency
            begin
               FloatValidator.Edit := SG.InplaceEditor;
               if not FloatValidator.IsValidInput (sData, iI) then
               begin
                  SG.InplaceEditor.Text := '';
                  SG.Cells[1, SG.Row] := '';
               end;
            end;
      end; // Case end
   end;
   SG.Cells[0, SG.Row] := ComboBox1.Items[ComboBox1.ItemIndex];
end;

procedure TIDB_QBForm.AButtonClick (Sender: TObject);
var
   EBF: TIDB_ExpressionBuilderForm;
   sFieldName, sAddFieldName: AnsiString;
   iI, iFieldType, iAddFieldType: Integer;
   sData: AnsiString;
begin
   sFieldName := SG.Cells[0, SG.Row];
   iFieldType := AStructMan.GetFieldType (sFieldName);
   EBF := TIDB_ExpressionBuilderForm.Create (SELF, sFieldName, iFieldType);
   // Put all fields in the list box.
   for iI := 0 to ComboBox1.Items.Count - 1 do
   begin
      sAddFieldName := ComboBox1.Items[iI];
      iAddFieldType := AStructMan.GetFieldType (sAddFieldName);
      if (iFieldType <> 1) and (iAddFieldType = 1) then CONTINUE;
      EBF.FieldsListBox.Items.Add (sAddFieldName);
   end;
   EBF.FieldsListBox.ItemIndex := 0;
   EBF.FunctionsListBox.ItemIndex := 0;

   EBF.Memo1.Text := SG.Cells[1, SG.Row];
   if EBF.ShowModal = mrOk then
   begin
      sData := EBF.Memo1.Lines.Text;
      sData := DeleteChars (sData, #13);
      sData := DeleteChars (sData, #10);
      SG.Cells[1, SG.Row] := sData;
   end;
   EBF.Free;
end;

procedure TIDB_QBForm.SGClick (Sender: TObject);
begin
   case AStructMan.GetFieldType (SG.Cells[0, SG.Row]) of
      2, // SmallInt
         3, // Integer
         12: // Byte
         begin
            IntValidator.Edit := SG.InplaceEditor;
            //         SG.Cells[1, SG.Row] := '';
         end;
      6, // Float
         7: // Currency
         begin
            FloatValidator.Edit := SG.InplaceEditor;
            //         SG.Cells[1, SG.Row] := '';
         end;
   else
      DisconnectAllValidators;
   end; // Case end
end;

procedure TIDB_QBForm.OKButtonClick (Sender: TObject);
var
   bThereIsData: Boolean;
   iI: Integer;
   sFieldName,
      sFieldValue: AnsiString;
   iFieldType: Integer;
begin
   if AWorkMode = wmUpdateBuilder then
   begin
      bThereIsData := FALSE;
      for iI := 0 to SG.RowCount - 1 do
      begin
         sFieldName := SG.Cells[0, iI];
         if (Trim (sFieldName) = '') then CONTINUE;
         iFieldType := SELF.AStructMan.GetFieldType (sFieldName);

         // The text can be in the inplcae editor, not in the grid yet.
         if (SG.Row = iI) and (SG.InplaceEditor <> nil) then
            sFieldValue := SG.InplaceEditor.Text
         else
            sFieldValue := SG.Cells[1, iI];

         if (iFieldType <> 1) and (Trim (sFieldValue) = '') then CONTINUE;
         bThereIsData := TRUE;
         BREAK;
      end; // For iI end
      if not bThereIsData then CancelButton.Click;
   end;
end;

procedure TIDB_QBForm.FirstComboBoxEnter (Sender: TObject);
begin
   (*
   {
   1 - String
   2 - SmallInt
   3 - Integer
   5 - Boolean
   6 - Float
   7 - Currency
   11 - DateTime
   12 - Byte
   }
        // An user can input any values for String type field and can input only number values for the others type field.
        If SELF.AStructMan.GetFieldType(ValuesFieldsListBox.Items[ValuesFieldsListBox.ItemIndex]) = 1 Then EXIT;
        If Key=#8 Then EXIT; // BackSpace was pressed.
        If (Key<'0') OR (Key>'9') Then Key := #0;
   *)
   DisconnectAllValidators;
   case AStructMan.GetFieldType (ValuesFieldsListBox.Items[ValuesFieldsListBox.ItemIndex]) of
      1:
         begin
         end; // String
      2, // SmallInt
         3, // Integer
         12: // Byte
         begin
            IntValidator1.ComboBox := FirstComboBox;
         end;
      6, // Float
         7: // Currency
         begin
            FloatValidator1.ComboBox := FirstComboBox;
         end;
   end; // Case end
end;

procedure TIDB_QBForm.FirstComboBoxExit (Sender: TObject);
begin
   DisconnectAllValidators;
end;

procedure TIDB_QBForm.SecondComboBoxEnter (Sender: TObject);
begin
   (*
   {
   1 - String
   2 - SmallInt
   3 - Integer
   5 - Boolean
   6 - Float
   7 - Currency
   11 - DateTime
   12 - Byte
   }
        // An user can input any values for String type field and can input only number values for the others type field.
        If SELF.AStructMan.GetFieldType(ValuesFieldsListBox.Items[ValuesFieldsListBox.ItemIndex]) = 1 Then EXIT;
        If Key=#8 Then EXIT; // BackSpace was pressed.
        If (Key<'0') OR (Key>'9') Then Key := #0;
   *)
   DisconnectAllValidators;
   case AStructMan.GetFieldType (ValuesFieldsListBox.Items[ValuesFieldsListBox.ItemIndex]) of
      1:
         begin
         end; // String
      2, // SmallInt
         3, // Integer
         12: // Byte
         begin
            IntValidator1.ComboBox := SecondComboBox;
         end;
      6, // Float
         7: // Currency
         begin
            FloatValidator1.ComboBox := SecondComboBox;
         end;
   end; // Case end
end;

function TIDB_QBForm.MakeQueryPreview: Boolean;
var
   iI: Integer;
   AField: TField;
begin
   RESULT := FALSE;
   DQuery1.Close;
   DQuery1.SQL.Text := SELF.GetSQLText;
   try
      DQuery1.Open;
      RESULT := TRUE;
   except on E: Exception do
      // 382=The query cannot be executed.
      begin
      MessageBox (Handle, PChar (ALangManager.GetValue (382)+#13 + e.message), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
      EXIT;
      end;
   end;
   for iI := 0 to DQuery1.Fields.Count - 1 do
   begin
      AField := DQuery1.Fields[iI];
      if FieldIsInternalUsageField (AField.FieldName) then
         AField.Visible := FALSE;
   end; // For iI end
   DQuery1.Next;
   IDB_DBGrid1.Perform (WM_VSCROLL, SB_LINEUP, 0);
   DQuery1.First;
   //brovak
//    IDB_DBGrid1.Invalidate;
   label4.Visible := true;
   RecordsStatusDummy := ALangManager.GetValue (500);
   label4.Caption := Format (RecordsStatusDummy, [DQuery1.RecNo, DQuery1.RecordCount]);
end;

procedure TIDB_QBForm.Button9Click (Sender: TObject);
begin
   //     If MakeQueryPreview Then
      //     ExtPageControl1.ActivePage := TabSheet6;
end;

procedure TIDB_QBForm.TabSheet6Show (Sender: TObject);
begin
   MakeQueryPreview;
end;

procedure TIDB_QBForm.ExtPageControl1Change (Sender: TObject);
begin
   //Label4.Visible:=(ExtPageControl1.ActivePage.Name='TabSheet6')
   // or ((ExtPageControl1.ActivePage.Name='TabSheet2') and (IOperator=6));
   // if Label4.Visible then begin;
   if ExtPageControl1.ActivePage.Name = 'TabSheet2' then
      Label4.Caption := ALangManager.GetValue (273)
   else
      if ExtPageControl1.ActivePage.Name = 'TabSheet6' then
      begin;
         StatusPreview;
         Label4.Caption := Format (RecordsStatusDummy, [DQuery1.RecNo, DQuery1.RecordCount]);
      end
      else
         Label4.Visible := false;
end;

procedure TIDB_QBForm.IDB_DBGrid1KeyUp (Sender: TObject; var Key: Word;
   Shift: TShiftState);
begin
   GiveMeStatus;
end;

procedure TIDB_QBForm.IDB_DBGrid1CellClick (Column: TColumn);
begin
   GiveMeStatus;
end;

{brovak}

procedure TIDB_QBForm.StatusPreview;
begin;
   if (SelectedTableFieldsListBox.Items.Count > 0) and (ExtPageControl1.ActivePage.Name = 'TabSheet6') then
   begin;
      IDB_DBGrid1.Visible := true;
      Label4.Visible := true;
   end
   else
   begin;
      IDB_DBGrid1.Visible := false;
      Label4.Visible := false;
   end;
end;

{brovak}

procedure TIDB_QBForm.GiveMeStatus;
begin;
   Label4.Caption := Format (RecordsStatusDummy, [DQuery1.RecNo, DQuery1.RecordCount]);
   IDB_DBGrid1.UpdateScrollBar;

end;

procedure TIDB_QBForm.IDB_DBGrid1DrawDataCell (Sender: TObject;
   const Rect: TRect; Field: TField; State: TGridDrawState);
begin
   GiveMeStatus;
end;

procedure TIDB_QBForm.CheckBox4Click(Sender: TObject);
var rt  : string;
    //i   : integer;
    sSQL: AnsiString;
begin
if CheckBox4.Checked then
 begin
  memo1.ReadOnly:=False;
  memo1.Color:= clWhite;
  rt:=Format(ALangManager.GetValue (212),['']) ;
  rt:=stringreplace(rt,'''','',[rfReplaceAll]);
  memo1.lines.text:=Stringreplace(memo1.lines.text, rt,'ATT_',[rfReplaceAll, rfIgnoreCase]);
  memo1.lines.text:=stringreplace(memo1.lines.text,'''','',[rfReplaceAll]);
  tabsheet1.enabled:=False;
  tabsheet2.enabled:=False;
  tabsheet3.enabled:=False;
  tabsheet1.enabled:=False;
  tabsheet5.enabled:=False;
 end
 else
 begin
  memo1.color:=clBtnFace;
  memo1.ReadOnly:=True;
  tabsheet1.enabled:=True;
  tabsheet2.enabled:=True;
  tabsheet3.enabled:=True;
  tabsheet1.enabled:=True;
  tabsheet5.enabled:=True;
 end;
extPageControl1.Repaint;
end;

end.

