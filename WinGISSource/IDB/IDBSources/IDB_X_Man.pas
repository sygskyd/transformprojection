unit IDB_X_Man;
{
Internal database. Ver. II.
Internal database AX-Connections manager.
It's purposed to allow for external object to get
an access to attribute information in IDB.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 11-03-2001
Modified:
}
interface

uses DBTables, Classes,
  DMaster, DTables {, IDB_ExternalTablesMan};

type
  TIDB_X_Manager = class
  private
 //     AnExtTablesMan: TIDB_ExternalTablesMan;
//      ATable: TDTable;
    AQuery: TDQuery;
    slFieldNames: TStringList;

    procedure FillFieldNamesList;
  public
    constructor Create(AMasterObject: TDMaster; {AnExtTablesMan: TIDB_ExternalTablesMan;} sAttTableName: AnsiString);
    destructor Free;

    procedure StopConnection;
    procedure StartConnection;

    function GotoObjectRecord(iItemIndex: Integer): Boolean;
    function AddRecord(iItemIndex: Integer): Boolean;

    function GetFieldCount: Integer;
    function GetFieldName(iFieldIndex: Integer): AnsiString;

    function GetFieldValue(sFieldName: AnsiString): Variant; overload;
    function GetFieldValue(iFieldIndex: Integer): Variant; overload;

    procedure SetFieldValue(sFieldName: AnsiString; FieldValue: Variant); overload;
    procedure SetFieldValue(iFieldIndex: Integer; FieldValue: Variant); overload;
  end;

implementation

uses DB,
  IDB_Utils, IDB_Consts, Variants;

constructor TIDB_X_Manager.Create(AMasterObject: TDMaster; {AnExtTablesMan: TIDB_ExternalTablesMan;} sAttTableName: AnsiString);
begin
  slFieldNames := TStringList.Create;
   //SELF.AnExtTablesMan := AnExtTablesMan;
  SELF.AQuery := TDQuery.Create(nil);
  SELF.AQuery.Master := AMasterObject;
   //if SELF.AnExtTablesMan.JoiningSQL = '' then
   //   SELF.AnExtTablesMan.SetParamsToExtTablePool;

//   if SELF.AnExtTablesMan.IAmInUse then
//   SELF.AQuery.SQL.Text := SELF.AnExtTablesMan.JoiningSQL;
//   else
  SELF.AQuery.SQL.Text := 'SELECT * FROM [' + sAttTableName + ']';

//   SELF.ATable := TDTable.Create (nil);
//   ATable.Master := AMasterObject;
//   ATable.TableName := sAttTableName;
  try
    SELF.AQuery.Open;
//      ATable.Open;
    SELF.FillFieldNamesList;
  except
  end;
end;

destructor TIDB_X_Manager.Free;
begin
  StopConnection;
  slFieldNames.Free;
//   ATable.Free;
  AQuery.Free;
end;

procedure TIDB_X_Manager.StopConnection;
begin
  AQuery.Close;
//   ATable.Close;
end;

procedure TIDB_X_Manager.StartConnection;
begin
  try
//      ATable.Open;
    AQuery.Open;
  except
  end;
end;

procedure TIDB_X_Manager.FillFieldNamesList;
var
  iI: Integer;
  sFieldName: AnsiString;
begin
  if not AQuery.Active then
//   if not ATable.Active then
  begin
    slFieldNames.Clear;
    EXIT;
  end;
  slFieldNames.Clear;
  for iI := 0 to AQuery.FieldCount - 1 do
//   for iI := 0 to ATable.FieldCount - 1 do
  begin
    sFieldName := AQuery.Fields[iI].FieldName;
//      sFieldName := ATable.Fields[iI].FieldName;
    if FieldIsInternalUsageField(sFieldName) then
      CONTINUE;
    slFieldNames.Add(AQuery.Fields[iI].FieldName);
//      slFieldNames.Add (ATable.Fields[iI].FieldName);
  end; // For iI end
end;

{ This function goes to required item attribute record.
  It returns TRUE if such record exists.
  It returns false if such record doesnt exists and goes to the previuos record.}

function TIDB_X_Manager.GotoObjectRecord(iItemIndex: Integer): Boolean;
var
  iMark: Integer;
begin
  RESULT := FALSE;
  if AQuery.Active then
//   if ATable.Active then
//      iMark := ATable.FieldByName (cs_InternalID_FieldName).AsInteger
    iMark := AQuery.FieldByName(cs_InternalID_FieldName).AsInteger
  else
    iMark := -1;
  try
//      ATable.Close;
//      ATable.Open;
    AQuery.Reopen;
//      RESULT := ATable.Locate ('PROGISID', iItemIndex, []);
    RESULT := AQuery.Locate('PROGISID', iItemIndex, []);
    if not RESULT then
    begin
      if iMark < 0 then
        AQuery.First
//            ATable.First
      else
        AQuery.Locate(cs_InternalID_FieldName, iMark, []);
//            ATable.Locate (cs_InternalID_FieldName, iMark, []);
    end;
    FillFieldNamesList;
  except
  end;
end;

function TIDB_X_Manager.AddRecord(iItemIndex: Integer): Boolean;
begin
  RESULT := FALSE;
  if not AQuery.Active then
    EXIT;
// if not ATable.Active then EXIT;
  try
    if not SELF.GotoObjectRecord(iItemIndex) then
    begin
      AQuery.Insert;
      AQuery.FieldByName('PROGISID').AsInteger := iItemIndex;
//         ATable.Insert;
//         ATable.FieldByName ('PROGISID').AsInteger := iItemIndex;
    end
    else
      AQuery.Edit;
    AQuery.FieldByName(cs_StateFieldName).AsString := cs_ObjectIsJustAdded;
    AQuery.Post;
//         ATable.Edit;
//      ATable.FieldByName (cs_StateFieldName).AsString := cs_ObjectIsJustAdded;
//      ATable.Post;
  except
    EXIT;
  end;
  RESULT := TRUE;
end;

function TIDB_X_Manager.GetFieldCount: Integer;
begin
  RESULT := slFieldNames.Count;
end;

function TIDB_X_Manager.GetFieldName(iFieldIndex: Integer): AnsiString;
begin
  RESULT := 'ProgisID';
  if (iFieldIndex > -1) and (iFieldIndex < slFieldNames.Count) then
    RESULT := slFieldNames[iFieldIndex];
end;

function TIDB_X_Manager.GetFieldValue(sFieldName: AnsiString): Variant;
var
  AField: TField;
begin
// ++ Cadmensky Version 2.3.7
  RESULT := NULL;
//   RESULT := 0;
// -- Cadmensky Version 2.3.7
//   if not ATable.Active then EXIT;
//   AField := ATable.FindField (sFieldName);
  if not AQuery.Active then
    EXIT;
  AField := AQuery.FindField(sFieldName);
  if AField <> nil then
  try
    if AField.Value <> NULL then
      RESULT := AField.Value;
  except
  end;
end;

function TIDB_X_Manager.GetFieldValue(iFieldIndex: Integer): Variant;
var
  sFieldName: AnsiString;
begin
// ++ Cadmensky Version 2.3.7
  RESULT := NULL;
//   RESULT := 0;

 {  if SELF.AnExtTablesMan.JoiningSQL = '' then
      SELF.AnExtTablesMan.SetParamsToExtTablePool;

   if SELF.AQuery.SQL.Text <> SELF.AnExtTablesMan.JoiningSQL then
   begin
      SELF.AQuery.Close;
      SELF.AQuery.SQL.Text := SELF.AnExtTablesMan.JoiningSQL;
      try
         SELF.AQuery.Open
      except
      end;
   end; }
// -- Cadmensky Version 2.3.7
  if not AQuery.Active then
    EXIT;

//   if not ATable.Active then EXIT;
  if (iFieldIndex > -1) and (iFieldIndex < slFieldNames.Count) then
    sFieldName := slFieldNames[iFieldIndex]
  else
    sFieldName := 'ProgisID';
  RESULT := SELF.GetFieldValue(sFieldName);
end;

procedure TIDB_X_Manager.SetFieldValue(sFieldName: AnsiString; FieldValue: Variant);
var
  AField: TField;
begin
  if not AQuery.Active then
    EXIT;
  AField := AQuery.FindField(sFieldName);
//   if not ATable.Active then EXIT;
//   AField := ATable.FindField (sFieldName);
  if AField <> nil then
  begin
    try
      AQuery.Edit;
//         ATable.Edit;
      AField.Value := FieldValue;
      AQuery.Post;
//         ATable.Post;
    except
    end;
  end;
end;

procedure TIDB_X_Manager.SetFieldValue(iFieldIndex: Integer; FieldValue: Variant);
var
  sFieldName: AnsiString;
begin
  if not AQuery.Active then
    EXIT;
//   if not ATable.Active then EXIT;
  if (iFieldIndex > -1) and (iFieldIndex < slFieldNames.Count) then
    sFieldName := slFieldNames[iFieldIndex]
  else
    EXIT;
  SELF.SetFieldValue(sFieldName, FieldValue);
end;

end.

