unit IDB_View;
{
Internal database. Ver. II.
This module provides the window for displaying database information
     from concrete database table.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 22-01-2001

----
Brovak Vladimir, 11-04-2002

At this and anothers modules we are now using Power Broker.
It is a easy copy of Current DataSet; We work with it for faster
making some operations

---
}
interface

uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
   ExtCtrls, DTables, Db, DDB, StdCtrls, Gauges, DBCtrls,
   Buttons, Menus, ImgList, DMaster, IDB_C_DBGrid, Grids, DBGrids, IDB_dbgrids,
   WCtrls, IDB_C_DBNavigator, IDB_C_Toolbar, IDB_FormatDigits,
   IDB_LayerManager, IDB_Messages, IDB_UIMans, IDB_Grids, Dialogs, lists, AdoApi,
   IDB_C_StringGrid, Shellapi, IDB_Statistic, IDB_PrintDlg,  Qrctrls, Inifiles;

type
   TIDB_ViewForm = class (TForm)
      StatusPanel: TPanel;
      ViewDataSource: TDataSource;
      StatusLabel: TLabel;
      Gauge: TGauge;
      MainMenu1: TMainMenu;
      Project1: TMenuItem;
      ShowItemProjectMenu: TMenuItem;
      ShowSelectItemsInProjectMenu: TMenuItem;
      ShowSelectedItemsWithZoom1: TMenuItem;
      N6: TMenuItem;
      AllExistingItemsInProject1: TMenuItem;
      ExistingItemsInTable1: TMenuItem;
      N3: TMenuItem;
      Edit1: TMenuItem;
      Copy1: TMenuItem;
      Cut1: TMenuItem;
      Paste1: TMenuItem;
      Functions1: TMenuItem;
      AnnotationMenuItem: TMenuItem;
      AllRecAnnot1: TMenuItem;
      SelRecAnnot1: TMenuItem;
      DBCharts1: TMenuItem;
      AllRecChart1: TMenuItem;
      SelRecChart1: TMenuItem;
      ThematicMap1: TMenuItem;
      TableMenu: TMenuItem;
      Record1: TMenuItem;
      AddRecordItemTableMenu: TMenuItem;
      DeleteRecordItemTableMenu: TMenuItem;
      N1: TMenuItem;
      EditRecordItemTableMenu: TMenuItem;
      SaveChangesItemTableMenu: TMenuItem;
      CancelChangesItemTableMenu: TMenuItem;
      Navigate1: TMenuItem;
      FirstRecordItemTableMenu: TMenuItem;
      PrevRecordItemTableMenu: TMenuItem;
      NextRecordItemTableMenu: TMenuItem;
      LastRecordItemTableMenu: TMenuItem;
      RefreshItemTableMenu: TMenuItem;
      UpdateRecords1: TMenuItem;
      N4: TMenuItem;
      SaveResult1: TMenuItem;
      LoadData1: TMenuItem;
      N2: TMenuItem;
      StructureTable1: TMenuItem;
      QueryMenu: TMenuItem;
      RunQueryBuilder1: TMenuItem;
      PopupMenu1: TPopupMenu;
      MakeFieldAsInvisiblePopUp: TMenuItem;
      N8: TMenuItem;
      SelRecAnnotPopUp: TMenuItem;
      DataQuery: TDQuery;
      Ahaa1: TMenuItem;
      SortAscPopUp: TMenuItem;
      SortDescPopUp: TMenuItem;
      N11: TMenuItem;
      ViewDBGrid: TIDB_DBGrid;
      MDI_SDI_SwitchPopUp: TMenuItem;
      SelectedExistingItemsInProject1: TMenuItem;
      W1: TMenuItem;
      N13: TMenuItem;
      IDB_Toolbar1: TIDB_Toolbar;
      CutSB: TSpeedButton;
      CopySB: TSpeedButton;
      PasteSB: TSpeedButton;
      TablePropertiesSB: TSpeedButton;
      FindItSB: TSpeedButton;
      FindNextSB: TSpeedButton;
      QueryBuilerSB: TSpeedButton;
      Create_Disable_FilterSB: TSpeedButton;
      SortAscSB: TSpeedButton;
      SortDescSB: TSpeedButton;
      DeleteSB: TSpeedButton;
      ShowSelItemsWithZoomSB: TSpeedButton;
      T2: TMenuItem;
      C2: TMenuItem;
      N14: TMenuItem;
      N15: TMenuItem;
      MonitoringSB: TSpeedButton;
      AutoAddToSelectedSB: TSpeedButton;
      LinksSB: TSpeedButton;
      SelRecChartPopUp: TMenuItem;
      LoadQuery: TMenuItem;
      SaveQuery: TMenuItem;
      AllRecTM1: TMenuItem;
      SelRecTM1: TMenuItem;
      SelRecTMPopup: TMenuItem;
      N12: TMenuItem;
      SelectAll1: TMenuItem;
      DeselectAll1: TMenuItem;
      InvertSelection1: TMenuItem;
      N16: TMenuItem;
      Replace1: TMenuItem;
      FindItemTableMenu: TMenuItem;
      FindNext1: TMenuItem;
      AlignVertical: TMenuItem;
      AlignHorizontal: TMenuItem;
      UnhideColumnPopup: TMenuItem;
      ShowSelectedPopup: TMenuItem;
      ShowSelectedWithZoomPopup: TMenuItem;
      FilterPanel: TPanel;
      Usecheck: TCheckBox;
      AppBtn: TButton;
      CancFiltBtn: TButton;
      ShowSelectedRecordsSB: TSpeedButton;
      StringGrid1: TIDB_StringGrid;
      ComboBox1: TComboBox;
    N5: TMenuItem;
    Exitmenue: TMenuItem;
    N3841: TMenuItem;
    N4401: TMenuItem;
    N4411: TMenuItem;
    N4421: TMenuItem;
    Help: TMenuItem;
    Helpmenue: TMenuItem;
    Infomenue: TMenuItem;
    N31: TMenuItem;
    N9: TMenuItem;
    Filtermenue: TMenuItem;
    N10: TMenuItem;
    N17: TMenuItem;
    ShowSelectedOnlyMenue: TMenuItem;
    N18: TMenuItem;
    LinkRecordwithObject: TMenuItem;
    N7: TMenuItem;
    Datensatz1: TMenuItem;
    Lschen1: TMenuItem;
    Neu1: TMenuItem;
    N19: TMenuItem;
    N5161: TMenuItem;
    ZoomOrSelect: TSpeedButton;
    PopupMenuZoomOrSelect: TPopupMenu;
    AutoZoom: TMenuItem;
    AutoSelect: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    Statistic1: TMenuItem;
    druck1: TMenuItem;
    ShowSelectedLinkID: TSpeedButton;
    Button1: TButton;
    FormatField: TMenuItem;
    N22: TMenuItem;
    EditQuery1: TMenuItem;
      procedure FormClose (Sender: TObject; var Action: TCloseAction);
      procedure FindItSBClick (Sender: TObject);
      procedure FormResize (Sender: TObject);
      procedure UpdateRecords1Click (Sender: TObject);
      procedure FindNextSBClick (Sender: TObject);
      procedure SaveResult1Click (Sender: TObject);
      procedure LoadData1Click (Sender: TObject);
      procedure MakeFieldAsInvisiblePopUpClick (Sender: TObject);
      procedure StatesOfFieldsPopUpClick (Sender: TObject);
      procedure Copy1Click (Sender: TObject);
      procedure Cut1Click (Sender: TObject);
      procedure Paste1Click (Sender: TObject);
      procedure PopupMenu1Popup (Sender: TObject);
      procedure AddSBClick (Sender: TObject);
      procedure FirstRecordItemTableMenuClick (Sender: TObject);
      procedure PrevRecordItemTableMenuClick (Sender: TObject);
      procedure NextRecordItemTableMenuClick (Sender: TObject);
      procedure LastRecordItemTableMenuClick (Sender: TObject);
      procedure RefreshItemTableMenuClick (Sender: TObject);
      procedure DeleteSBClick (Sender: TObject);
      procedure TablePropertiesPopUpClick (Sender: TObject);
      procedure Ahaa1Click (Sender: TObject);
      procedure ShowProjectSBClick (Sender: TObject);
      procedure AllRecInsertPointsClick (Sender: TObject);
      procedure SelRecInsertPointsClick (Sender: TObject);
      procedure ViewDTableAfterInsert (DataSet: TDataSet);
      procedure ViewDataSourceStateChange (Sender: TObject);
      procedure AllRecInsertSymbolsClick (Sender: TObject);
      procedure SelRecInsertSymbolsClick (Sender: TObject);
      procedure ShowSelItemsWithZoomSBClick (Sender: TObject);
      procedure ShowSelectItemsInProjectMenuClick (Sender: TObject);
      procedure AllExistingItemsInProject1Click (Sender: TObject);
      procedure FormCreate (Sender: TObject);
      procedure ExistingItemsInTable1Click (Sender: TObject);
      procedure RunQueryBuilder1Click (Sender: TObject);
      procedure AllRecAnnot1Click (Sender: TObject);
      procedure SelRecAnnot1Click (Sender: TObject);

      procedure AllRecChart1Click (Sender: TObject);
      procedure SelRecChart1Click (Sender: TObject);

      procedure AllRecTM1Click (Sender: TObject);
      procedure SelRecTM1Click (Sender: TObject);

      procedure SortAscPopUpClick (Sender: TObject);
      procedure SortDescPopUpClick (Sender: TObject);
      procedure MDI_SDI_SwitchPopUpClick (Sender: TObject);
      procedure ViewDBGridMouseUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
      procedure ViewDBGridKeyDown (Sender: TObject; var Key: Word; Shift: TShiftState);
//      procedure ShowAllQueriedItemsMenuItemClick (Sender: TObject);
      procedure FormDestroy (Sender: TObject);
      procedure FormActivate (Sender: TObject);
      procedure FormDeactivate (Sender: TObject);
      procedure SelectedExistingItemsInProject1Click (Sender: TObject);
      procedure TableStructurePopUpClick (Sender: TObject);
      procedure ViewDTableAfterScroll (DataSet: TDataSet);
      procedure ViewDBGridKeyUp (Sender: TObject; var Key: Word; Shift: TShiftState);
      procedure T2Click (Sender: TObject);
      procedure C2Click (Sender: TObject);
      procedure FormKeyPress (Sender: TObject; var Key: Char);
      procedure Create_Disable_FilterSBClick (Sender: TObject);
      procedure CutSBMouseMove (Sender: TObject; Shift: TShiftState; X, Y: Integer);
      procedure IDB_Toolbar1MouseMove (Sender: TObject; Shift: TShiftState; X, Y: Integer);
      procedure W1Click (Sender: TObject);
      procedure DataQueryAfterOpen (DataSet: TDataSet);
      procedure MonitoringSBClick (Sender: TObject);

//      procedure ExtTablesParamsPopUpClick (Sender: TObject);
//      procedure ExtTablesDisconnectPopUpClick (Sender: TObject);
//      procedure ExtTablesConnectPopUpClick (Sender: TObject);

//      procedure DataQueryBeforePost (DataSet: TDataSet);
      procedure ResizeGridCellPopUpClick (Sender: TObject);
      procedure DataQueryCalcFields (DataSet: TDataSet);
      procedure DataQueryBeforeOpen (DataSet: TDataSet);
      procedure CloseSBClick (Sender: TObject);
      procedure DataQueryAfterPost (DataSet: TDataSet);
      procedure ViewDBGridCellClick (Column: TColumn);
      procedure ViewDBGridDblClick (Sender: TObject);
      procedure IDB_Toolbar1Exit (Sender: TObject);
      procedure ViewDBGridMouseMove (Sender: TObject; Shift: TShiftState; X, Y: Integer);
      procedure ViewDBGridExit (Sender: TObject);
      procedure FormHide (Sender: TObject);
      procedure LoadQueryClick (Sender: TObject);
      procedure SaveQueryClick (Sender: TObject);
      procedure SelectAll1Click (Sender: TObject);

      procedure AlignVerticalClick (Sender: TObject);
      procedure AlignHorizontalClick (Sender: TObject);
      procedure UnhideColumnPopupClick (Sender: TObject);
      procedure DeselectAll1Click (Sender: TObject);
      procedure InvertSelection1Click (Sender: TObject);
      procedure StringGrid1MouseDown (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
      procedure ComboBox1Exit (Sender: TObject);
      procedure StringGrid1TopLeftChanged (Sender: TObject);
      procedure StringGrid1KeyDown (Sender: TObject; var Key: Word; Shift: TShiftState);
      procedure AppBtnClick (Sender: TObject);
      procedure UsecheckClick (Sender: TObject);
      procedure StringGrid1SelectCell (Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
      procedure ComboBox1Change (Sender: TObject);
      procedure ComboBox1DropDown (Sender: TObject);
      procedure CancFiltBtnClick (Sender: TObject);
      procedure ComboBox1KeyDown (Sender: TObject; var Key: Word; Shift: TShiftState);
      procedure StringGrid1MouseMove (Sender: TObject; Shift: TShiftState; X, Y: Integer);
      procedure ComboBox1KeyUp (Sender: TObject; var Key: Word; Shift: TShiftState);
      procedure StringGrid1KeyUp (Sender: TObject; var Key: Word; Shift: TShiftState);

      procedure FilterPanelResize (Sender: TObject);
      procedure StringGrid1RowMoved (Sender: TObject; FromIndex, ToIndex: Integer);
      procedure ShowSelectedRecordsSBClick (Sender: TObject);
      procedure AutoAddToSelectedSBClick (Sender: TObject);
    procedure HelpmenueClick(Sender: TObject);
    procedure InfomenueClick(Sender: TObject);
    procedure ShowSelectedOnlyMenueClick(Sender: TObject);
    procedure ExitmenueClick(Sender: TObject);
    procedure Replace1Click(Sender: TObject);
    procedure LinkRecordwithObjectClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AutoZoomClick(Sender: TObject);
    procedure AutoSelectClick(Sender: TObject);
    procedure ZoomOrSelectClick(Sender: TObject);
    procedure EnableSort;
    procedure Statistic1Click(Sender: TObject);
    procedure druck1Click(Sender: TObject);
    procedure ShowSelectedLinkIDClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormatFieldClick(Sender: TObject);
   private
      { Private declarations }
      ACurrentSpeedButtonUnderMouse: TSpeedButton;
      // This variables are required to the operation that changes the state
      // of the window - MDI to SDI and back.
      iMyTop, iMyLeft,
         iMyHeight, iMyWidth,
         iHeightOfToolBarsOfMainMDIForm: Integer;

      FProject: Pointer;
      FLayer: Pointer;
      FALayer: Pointer;
      // Search parameters
      sSearchValue, sSearchColumn: AnsiString;
      loOptions: TLocateOptions;

      bIAmQueryResultWindow: Boolean; // if this window isn't the window of an attribute table,
      // we haven't to save parameters of its size and placement.
      
//      bIAmResultOfExternalTablesJoining: Boolean;

      bIAmUnderFilter: Boolean;
      bIAmWorking: Boolean;
      bTheUserWantsToCancel: Boolean;

      bParametersAlreadyWereSaved: Boolean;

      bCheckFieldPresentBeforeInsert: Boolean; // Check all required fields of the attribute table before inserting of new record.

      bROMode: Boolean;

      sQueryTableNames,
         sWhereClause: AnsiString; // if the window is purposed to show the query result, this variable storages WHERE clause
      // of SQL statement if this clause exists in the statement and empty string otherwise.

      sOldStatusText: AnsiString;
      sRecordsStatusDummy: AnsiString;

      AToolbar: TIDB_Toolbar;

      AMenuManager: TIDB_MenuManager;
      AToolbarManager: TIDB_ToolbarManager;

      AnOldScreenCursor: TCursor;

      ProgisIDList: TIntList;
      LinkIDList: TIntList;
      InternalIDList: TIntList;
      StartWidthComboList: integer;

      FHintWin: THintWindow;
      FHintX, FHintY, FHintCol, FHintRow: integer;

      ARow, ACol: integer;
      AlreadyResized, Querysaved: boolean;

      procedure CMMouseLeave (var Msg: TMessage); message CM_MOUSELEAVE;
      procedure CMMouseENTER (var Msg: TMessage); message CM_MOUSEENTER;
      procedure Formmove (var Msg: TMessage); message WM_MOVE;
      function FilterMachineAnalyzer (ASourceCell: ansistring; FieldType: integer): ansistring;

      procedure SetScreenCursor;
      procedure RestoreScreenCursor;

      procedure SetMyCaption;
      procedure SetMyCaptionFiltered;
      procedure GoToPrevPosition (AMark: Integer); overload;

      procedure ShowStatus; // Show status bar.
      procedure HideStatus; // Hide status bar.
      // This bar is not WinGIS status bar but the status bar at the botton of this window.

      procedure MakeRecordsStatus;
      procedure FillWindowMenu;
      procedure OnWindowMenuItemClick (Sender: TObject);

      procedure InsertPoints (bMakeItForAllRecords: Boolean);
      procedure InsertSymbols (bMakeItForAllRecords: Boolean);
      procedure MakeAnnotations (bMakeItForAllRecords: Boolean);
      procedure MakeCharts (bMakeItForAllrecords: boolean);
      procedure MakeThematicMap (MakeItForAllRecords: Boolean);

      procedure PutExistingItemsInProjectIntoTheTable (bPutAllLayerItem: Boolean; showinfo: Boolean = True);

      procedure RefreshDataWindow (var AMsg: TMessage); message WM_IDB_Message;

      procedure ShowSelectedItemsInProject (bNeedZoom: Boolean);

      procedure RegisterToolbarButtons;
      procedure RegisterMenuItems;

      function ThematicMapDllExists: Boolean;

      procedure CreateCalculatedFields;
      procedure ApplyFieldVisibilityChanges;
      procedure SaveFieldVisibilityChanges;

      procedure WP (var WPMsg: TWMSysCommand); message WM_SYSCommand;
//      procedure WMKeyDown (var WPMsg: TMessage); message WM_KEYDOWN;
      procedure MakeBookmarksList (Flag: boolean);
      procedure FillingFilterCombo (sFieldName: string);
// ++ Commented by Cadmensky Version 2.3.7
//      function FieldIsNumberType (sFieldName: AnsiString): Boolean;
//      procedure appIdle (Sender: TObject; var Done: boolean);
// -- Commented by Cadmensky Version 2.3.7
      procedure ClosePowerBroker;

      procedure PrepareGauge (AText: AnsiString);

      function FieldNoByNameEx (sFieldName: AnsiString; TmpRecordset: RecordSet): Integer;
      function Find: Boolean;
      function CompareSearchValues (sSearchValue, sDataValue: AnsiString): Boolean;
// ++ Cadmensky Version 2.3.8
      procedure RestoreUIAfterFilterSettingsDialog;
// -- Cadmensky Version 2.3.8
      procedure ShowAllWindows(isvisible : Boolean);
   public
      ALayerManager: TIDB_LayerManager;
      StartRow: integer;
      ARecordSet: RecordSet;
      sAttTableName: AnsiString;
      CPB_SortField: string;
      dblclicksortorder: boolean;
      CPB_Order: string;
      FilterField: string;

      { Public declarations }
      property AProject: Pointer read FProject;
      property ALayer: Pointer read FLayer write FLayer;
      property ActiveLayer: Pointer read FALayer write FALayer;
      property IAmQueryResultWindow: Boolean read bIAmQueryResultWindow;
      property TheUserWantsToCancel: Boolean read bTheUserWantsToCancel;

      constructor Create (AOwner: TComponent; ALayerManager: TIDB_LayerManager; AProject, ALayer, AALayer: Pointer);
      function FieldNoByName (sFieldName: AnsiString): Integer;
      procedure MakeWindowForDisplayingQueryResult (sTableNames, sWhereClause: AnsiString); // Just after creating, the window is purposed to display attribute data from the attribute table.
      procedure MakeReadOnlyUI (bROMode: Boolean);
//      procedure MakeExtTablesUI (bUseExtTableJoining: Boolean);

      procedure MakeMyWindowState (AValue: TWindowState);

//      function MakeExtTablesLinking (bDetectFieldStatesBefore: Boolean = true): Boolean;
//      procedure MakeExtTablesDisconnect;

      function Create_Power_Broker (bNeedToClearRowBits: boolean = true): boolean;

      procedure GotoSelectRecord (iItemID: integer);
      procedure ShowRecordsForSelectedObjects;
      procedure SetAutoAddToSelectedButtonDown (bValue: boolean);
   end;

implementation
{$R *.DFM}
{$R Thumbtack.res}

uses ClipBrd,
   ADOBase,
   IDB_LanguageManager, IDB_ParamsOfSearch, IDB_ParamsOfUpdates, IDB_Utils, IDB_Consts,
   IDB_DataMan, IDB_FieldsVisibilityMan, IDB_DescMan, IDB_MainManager, IDB_CallbacksDef,
   IDB_2ParamsDialog, IDB_StructMan, IDB_QBuilder, IDB_ListOfQueryWindows,
   IDB_WG_Func, IDB_ParamsOfRecordTemplate, {IDB_ParamsOfExternalTables,}
   {IDB_ExternalTablesMan,} IDB_Access2DB, IDB_Monitor, IDB_Monitor1,
   IDB_StructureAndProperties, IDB_ParamsOfSaveLoad_TableSelect, Variants;

function ExtractValue (AValue: OLEVariant): AnsiString;
begin
   Result := '';
   if aValue <> NULL then
      Result := AValue;
end;

function ExtractValue1 (AValue: OLEVariant): longint;
begin
   Result := 0;
   if AValue <> NULL then
      Result := AValue;
end;

constructor TIDB_ViewForm.Create (AOwner: TComponent; ALayerManager: TIDB_LayerManager; AProject, ALayer, AALayer: Pointer);
begin
   inherited Create (AOwner);
   ACurrentSpeedButtonUnderMouse := nil;
   SELF.bTheUserWantsToCancel := false;
   SELF.ALayerManager := ALayerManager;
   SELF.FProject := AProject;
   SELF.FLayer := ALayer;
   SELF.FALayer := AALayer;
   SELF.StatusLabel.Caption := '';
   SELF.bIAmQueryResultWindow := false;
   //SELF.bIAmResultOfExternalTablesJoining := false;
   SELF.bIAmUnderFilter := false;
   // 321=Create and apply filter
   Create_Disable_FilterSB.Hint := ALangManager.GetValue (321);
   self.Filtermenue.Caption:=Create_Disable_FilterSB.Hint;
   Filtermenue.Caption:=Create_Disable_FilterSB.Hint;
   bParametersAlreadyWereSaved := false;
   sSearchColumn := '';
   // Attribute information of layer ''%s''
   SELF.SetMyCaption;
   {brovak}
   ViewDBGrid.RowBitsClear;
   ProgisIDList := TIntList.Create;
   InternalIDList := TIntList.Create;
   LinkIDList:= TIntList.Create;
   {brovak}
end;

procedure TIDB_ViewForm.FormCreate (Sender: TObject);
begin
   // 281=Record %d from %d (%d)
   sRecordsStatusDummy := ALangManager.GetValue (281);
   ALangManager.MakeReplacement (SELF);
   // 306=do You want to cancel this operation?
   SELF.ViewDBGrid.CancelMessage := ALangManager.GetValue (306);
   SELF.AMenuManager := TIDB_MenuManager.Create (SELF);
   SELF.AToolbarManager := TIDB_ToolbarManager.Create (SELF);
   RegisterToolbarButtons;
   RegisterMenuItems;
   ZoomOrSelect.Width:=10;
   FHintWin := THintWindow.Create (SELF);
   ShowSelectedLinkID.Visible:=False;
end;

procedure TIDB_ViewForm.FormClose (Sender: TObject; var Action: TCloseAction);
begin
   if not SELF.bIAmQueryResultWindow then
   begin
      ALayerManager.StoreParametersOfViewForm (SELF.FLayer, SELF);
      bParametersAlreadyWereSaved := true;
   end else if self.Querysaved=False then  SaveQueryClick(Sender);   
//   AnIDBMainMan.LastWasAttributeWindow[SELF.FProject] := true;
   Action := caFree;
end;

procedure TIDB_ViewForm.WP (var WPMsg: TWMSysCommand);
begin
   if (WPMsg.CmdType = SC_RESTORE) or
      (WPMsg.CmdType = SC_MAXIMIZE) then
      SELF.MDI_SDI_SwitchPopUpClick (SELF)
   else
      inherited;
end;

{procedure TIDB_ViewForm.WMKeyDown (var WPMsg: TMessage);
begin
   if (WPMsg.WParam = 68) then
      if (GetKeyState (VK_LCONTROL) > 0) or
         (GetKeyState (VK_RCONTROL) > 0) then
         SELF.DeleteSBClick (SELF)
   else
      inherited;
end;}

procedure TIDB_ViewForm.CreateCalculatedFields;
var
   ANewFloatField: TFloatField;
   ANewIntegerField: TIntegerField;
   ANewStringField: TStringField;
   slCalcFields: TStringList;
begin
   slCalcFields := TStringList.Create;
   slCalcFields.Text := SELF.ALayerManager.GetUsedCalcFieldsNames (SELF.FLayer);
   // Area
   if slCalcFields.IndexOf (cs_CalcFieldAreaName) = -1 then
   begin
      ANewFloatField := TFloatField.Create (DataQuery);
      ANewFloatField.Visible := false;
      ANewFloatField.FieldKind := fkCalculated;
      ANewFloatField.ReadOnly := true;
      ANewFloatField.FieldName := cs_CalcFieldAreaName;
      ANewFloatField.DataSet := DataQuery;
   end;
   // Size
   if slCalcFields.IndexOf (cs_CalcFieldSizeName) = -1 then
   begin
      ANewFloatField := TFloatField.Create (DataQuery);
      ANewFloatField.Visible := false;
      ANewFloatField.FieldKind := fkCalculated;
      ANewFloatField.ReadOnly := true;
      ANewFloatField.FieldName := cs_CalcFieldSizeName;
      ANewFloatField.DataSet := DataQuery;
   end;

   if slCalcFields.IndexOf (cs_CalcFieldRotationName) = -1 then
   begin
      ANewFloatField := TFloatField.Create (DataQuery);
      ANewFloatField.Visible := false;
      ANewFloatField.FieldKind := fkCalculated;
      ANewFloatField.ReadOnly := true;
      ANewFloatField.FieldName := cs_CalcFieldRotationName;
      ANewFloatField.DataSet := DataQuery;
   end;
   // X
   if slCalcFields.IndexOf (cs_CalcFieldXName) = -1 then
   begin
      ANewFloatField := TFloatField.Create (DataQuery);
      ANewFloatField.Visible := false;
      ANewFloatField.FieldKind := fkCalculated;
      ANewFloatField.ReadOnly := true;
      ANewFloatField.FieldName := cs_CalcFieldXName;
      ANewFloatField.DataSet := DataQuery;
   end;
   // Y
   if slCalcFields.IndexOf (cs_CalcFieldYName) = -1 then
   begin
      ANewFloatField := TFloatField.Create (DataQuery);
      ANewFloatField.Visible := false;
      ANewFloatField.FieldKind := fkCalculated;
      ANewFloatField.ReadOnly := true;
      ANewFloatField.FieldName := cs_CalcFieldYName;
      ANewFloatField.DataSet := DataQuery;
   end;
   // Vertices
   if slCalcFields.IndexOf (cs_CalcFieldVerticesCount) = -1 then
   begin
      ANewIntegerField := TIntegerField.Create (DataQuery);
      ANewIntegerField.Visible := false;
      ANewIntegerField.FieldKind := fkCalculated;
      ANewIntegerField.ReadOnly := true;
      ANewIntegerField.FieldName := cs_CalcFieldVerticesCount;
      ANewIntegerField.DataSet := DataQuery;
   end;
   // SymbolName
   if slCalcFields.IndexOf (cs_CalcFieldSymbolName) = -1 then
   begin
      ANewStringField := TStringField.Create (DataQuery);
      ANewStringField.Size := 100;
      ANewStringField.Visible := false;
      ANewStringField.FieldKind := fkCalculated;
      ANewStringField.ReadOnly := true;
      ANewStringField.FieldName := cs_CalcFieldSymbolName;
      ANewStringField.DataSet := DataQuery;
   end;
   // Text
   if slCalcFields.IndexOf (cs_CalcFieldTextName) = -1 then
   begin
      ANewStringField := TStringField.Create (DataQuery);
      ANewStringField.Size := 100;
      ANewStringField.Visible := false;
      ANewStringField.FieldKind := fkCalculated;
      ANewStringField.ReadOnly := true;
      ANewStringField.FieldName := cs_CalcFieldTextName;
      ANewStringField.DataSet := DataQuery;
   end;
   // LinkID
   if slCalcFields.IndexOf (cs_CalcFieldLinkIDName) = -1 then
   begin
      ANewIntegerField := TIntegerField.Create (DataQuery);
      ANewIntegerField.Visible := false;
      ANewIntegerField.FieldKind := fkCalculated;
      ANewIntegerField.ReadOnly := true;
      ANewIntegerField.FieldName := cs_CalcFieldLinkIDName;
      ANewIntegerField.DataSet := DataQuery;
   end;
   slCalcFields.Free;
end;

procedure TIDB_ViewForm.SetScreenCursor;
begin
   SELF.AnOldScreenCursor := Screen.Cursor;
   Screen.Cursor := crHourGlass;
   Application.ProcessMessages;
   Application.HandleMessage;
end;

procedure TIDB_ViewForm.RestoreScreenCursor;
begin
   Screen.Cursor := SELF.AnOldScreenCursor;
end;

procedure TIDB_ViewForm.RefreshDataWindow (var AMsg: TMessage);
begin
   case AMsg.WParam of
      IDB_RefreshDataWindow:
         case AMsg.LParam of
            0: SELF.RefreshItemTableMenuClick (SELF);
            1: SELF.ApplyFieldVisibilityChanges;
         end;
   end; // Case end
end;

procedure TIDB_ViewForm.SetMyCaption;
begin
   // Attribute information of layer ''%s''
   SELF.Caption := Format (ALangManager.GetValue (58), [GetLayerNameFunc (SELF.FLayer)]);
end;

procedure TIDB_ViewForm.SetMyCaptionFiltered;
begin
   // 323=Attribute information of layer ''%s'' (filtered)
   SELF.Caption := Format (ALangManager.GetValue (323), [GetLayerNameFunc (SELF.FLayer)]);
end;

procedure TIDB_ViewForm.GoToPrevPosition (AMark: Integer);
begin
   try
      if not SELF.DataQuery.Search (cs_InternalID_FieldName, IntToStr (AMark)) then
         SELF.DataQuery.First;
   except
      SELF.DataQuery.First;
   end;
end;

procedure TIDB_ViewForm.FindItSBClick (Sender: TObject);
var
   iI: Integer;
   iMark: Integer;
   SPF: TIDB_SearchParamsForm;
   bValueWasFound: Boolean;
begin
   if SELF.DataQuery.RecordCount < 1 then EXIT;
   // if there isn't any attempt of searching, get the pushed column name as field name to search in.
   if SELF.sSearchColumn = '' then SELF.sSearchColumn := ViewDBGrid.PushedColumnName;
   SPF := TIDB_SearchParamsForm.Create (AWinGISMainForm);
   // Put fields names in ComboBox
   for iI := 0 to ViewDBGrid.Columns.Count - 1 do
   begin
      SPF.FieldsForSearchComboBox.Items.Add (ViewDBGrid.Columns[iI].FieldName);
   end;
// ++ Cadmensky Version 2.3.3
   SPF.SetSearchParams (ViewDBGrid.PushedColumnName, SELF.sSearchValue, SELF.loOptions);
//   SPF.SetSearchParams (SELF.sSearchColumn, SELF.sSearchValue, SELF.loOptions);
// -- Cadmensky Version 2.3.3
   if SPF.ShowModal <> mrOk then
   begin
      SPF.Free;
      EXIT;
   end;
   // I get from the search parameters form in which field search should be and search parameters.
   SPF.GetSearchParams (SELF.sSearchColumn, SELF.sSearchValue, SELF.loOptions);
   // The searching itself
   try
      iMark := SELF.DataQuery.FieldByName (cs_InternalID_FieldName).AsInteger;
   except
      iMark := -1;
   end;
   try
      bValueWasFound := SELF.DataQuery.Locate (sSearchColumn, sSearchValue, loOptions);
// ++ Cadmensky Version 2.3.3
      if bValueWasFound then
         bValueWasFound := SELF.CompareSearchValues (sSearchValue, SELF.DataQuery.FieldByName (sSearchColumn).AsString);
// -- Cadmensky Version 2.3.3
   except
      bValueWasFound := false;
   end;

   if not bValueWasFound then
   begin
      SELF.GoToPrevPosition (iMark);
      // 'Value ''' + sSearchValue + ''' in field ''' + sSearchColumn + ''' does not exists.'
      MessageBox (Handle, PChar (Format (ALangManager.GetValue (51), [sSearchValue, sSearchColumn])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
// ++ Cadmensky Version 2.3.3
      AToolbarManager.ButtonEnabled['FindNextRecords'] := false;
      AMenuManager.MenuEnabled['FindNextRecords'] := false;
// -- Cadmensky Version 2.3.3
   end
   else
   begin
// ++ Cadmensky Version 2.3.3
      SELF.GoToPrevPosition (SELF.DataQuery.FieldByName (cs_InternalID_FieldName).AsInteger);
      SELF.MakeRecordsStatus;
// -- Cadmensky Version 2.3.3
      SELF.ViewDBGrid.SelectedRows.Clear;
      AToolbarManager.ButtonEnabled['FindNextRecords'] := true;
      AMenuManager.MenuEnabled['FindNextRecords'] := true;
   end;
   SPF.Free;
end;

procedure TIDB_ViewForm.FormResize (Sender: TObject);
var
   iI: Integer;
begin
   self.MDI_SDI_SwitchPopUp.Checked:=self.FormStyle=fsStayOnTop;
   // if any field has display width that is wider than grid width, this field should have
   // width less than grid width.
   for iI := 0 to ViewDBGrid.Columns.Count - 1 do
      if (ViewDBGrid.Columns[iI].Width > SELF.ClientWidth - 10)
         and
         (ViewDBGrid.Columns[iI].Width > 20) then
         ViewDBGrid.Columns[iI].Width := SELF.ClientWidth - 10;
   // Change the progress bar width.
   Gauge.Width := SELF.ClientWidth - Gauge.Left - 5;
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;

   {brovak}
   if SELF.Width > 310 then
   begin
      FilterPanel.Width := SELF.Width - 8;
   end;
   Combobox1.Visible := false;
   {brovak}
   SELF.AToolbarManager.ButtonDown['AutoAddToSelected'] := SELF.AToolbarManager.ButtonDown['AutoAddToSelected'] and (SELF.FormStyle<>fsMDIChild);
   self.AutoAddToSelectedSB.Enabled:= SELF.FormStyle <> fsMDIChild;
end;

procedure TIDB_ViewForm.MakeWindowForDisplayingQueryResult (sTableNames, sWhereClause: AnsiString);

   procedure HideMenuItems (iTag: Integer);
   var
      iI: Integer;
   begin
      for iI := 0 to SELF.ComponentCount - 1 do
         if (SELF.Components[iI] is TMenuItem)
            and
            (TMenuItem (SELF.Components[iI]).Tag = iTag) then
            TMenuItem (SELF.Components[iI]).Visible := false;
   end;

   procedure DisableButtons (iTag: Integer);
   var
      iI: Integer;
   begin
      for iI := 0 to SELF.ComponentCount - 1 do
         if (SELF.Components[iI] is TSpeedButton)
            and
            (TSpeedButton (SELF.Components[iI]).Tag = iTag) then
            TSpeedButton (SELF.Components[iI]).Enabled := false;
   end;

var
   iI: Integer;
   AField: TField;
begin
   SELF.bIAmQueryResultWindow := true;

   SELF.sQueryTableNames := sTableNames;
   SELF.sWhereClause := sWhereClause;
   // Hide menus that maybe used only for table not for query result. (Add, delete, table structure etc.)
   HideMenuItems (1);
   // Disable same buttons (see comment above)
   DisableButtons (1);
   // if there isn't field with name 'ProgisID' in query, I hide also the menu items
   // that call work with wingis project based on this field (show selected items,
   // show selected items with zoom etc).
   if (SELF.DataQuery.FindField ('PROGISID') = nil) or (SELF.DataQuery.FindField (cs_InternalID_FieldName) = nil) then
   begin
      HideMenuItems (2);
      SELF.ViewDBGrid.Options := SELF.ViewDBGrid.Options - [dgMultiSelect];
   end;

   SELF.ViewDataSource.DataSet := SELF.DataQuery;
   SELF.ViewDBGrid.ReadOnly := False;

   for iI := 0 to ViewDataSource.DataSet.Fields.Count - 1 do
   begin
      AField := ViewDataSource.DataSet.Fields[iI];
      {if FieldIsCalculatedField (AField.FieldName) then
         AField.Visible := false }  //WAI why not?
   end;
   AMenuManager.MenuCaption['TableMenu'] := ALangManager.GetValue (52); // Menu 'Table' has to be named as 'Result of query'
   AMenuManager.MenuVisible['ShowAllQueriedItems'] := true;
   AMenuManager.MenuVisible['SaveQuery'] := true;
   AMenuManager.MenuVisible['EditQuery1'] := true;
   AMenuManager.MenuVisible['LoadData'] := false;
   AMenuManager.MenuVisible['ExternalTables'] := false;
   AMenuManager.MenuVisible['UpdateRecords'] := True;
   AToolBarManager.ButtonEnabled['ShowSelectedRecords'] := false;
   ShowSelectedOnlyMenue.Enabled:= AToolbarManager.ButtonEnabled['ShowSelectedRecords'];
   AToolBarManager.ButtonEnabled['Links'] := false;
end;

procedure TIDB_ViewForm.ShowStatus;
begin
   sOldStatusText := StatusLabel.Caption;
   SELF.Gauge.Progress := 0;

   SELF.Gauge.Visible := true;
   SELF.ViewDBGrid.Enabled := false;
   SELF.AToolbar.Enabled := false;

   AMenuManager.MakeMenuDisabled;

   SELF.bIAmWorking := true;

   Application.ProcessMessages;
end;

procedure TIDB_ViewForm.HideStatus;
begin
   StatusLabel.Caption := sOldStatusText;

   SELF.Gauge.Visible := false;
   SELF.ViewDBGrid.Enabled := true;
   SELF.AToolbar.Enabled := true;

   AMenuManager.MakeMenuEnabled;

   SELF.bIAmWorking := false;
   SELF.bTheUserWantsToCancel := false;

   Application.ProcessMessages;
end;

procedure TIDB_ViewForm.UpdateRecords1Click (Sender: TObject);
var
   iI, iJ: Integer;
   UF: TIDB_UpdateForm;
   sLayerName,
// ++ Commented by Cadmensky Version 2.3.6
{      sTableName, }
// -- Commented by Cadmensky Version 2.3.6
   sFieldName: AnsiString;
   AFieldType: TFieldType;
   AnExecQuery: TDQuery;
   sSQL: AnsiString;
   bSQLHasToBeExecuted: Boolean;
// ++ Cadmensky Version 2.3.4
   iInternalIDField, iProgisIDField: Integer;
   iMark: Integer;
   sIDs: AnsiString;
   isReplace: Boolean;
   tmpstr: string;
// -- Cadmensky Version 2.3.4
begin
   isReplace:=  Sender=Replace1;
   UF := TIDB_UpdateForm.Create (AWinGISMainForm, ALayerManager.DataAccessObject, SELF.FProject, GetLayerIndexFunc (SELF.FLayer));
   sLayerName := GetLayerNameFunc (SELF.FLayer);
   if isReplace then
      begin
      UF.Caption := Format (ALangManager.GetValue (95), [sLayerName]);
      UF.Label2.Caption:= Format (ALangManager.GetValue (511), [sLayerName]);
      UF.ReplaceEdit.Visible:=True;
      UF.CheckBoxGesamt.Visible:=True;
      UF.Label3.Visible:=True;
      UF.AdvancedButton.Visible:=False;
      UF.OkButton.Caption:= Format (ALangManager.GetValue (513), [sLayerName]);
      end else
      begin
      UF.Caption := Format (ALangManager.GetValue (56), [sLayerName]);
      UF.SearchEdit.Top:=34;
      UF.Label2.Top:=39;
      UF.Height:=133;
      end;
   if SELF.ViewDBGrid.SelectedCount > 0 then
      UF.Caption:= UF.Caption + ' (' + inttostr(SELF.ViewDBGrid.SelectedCount)+' '+ ALangManager.GetValue(514)+')'
      else if self.bIAmUnderFilter then SelectAll1Click(Sender);
   // Put all field's names in ComboBox exclude field with name 'ProgisID'.
   // This field cannot be updated.
   UF.FieldNamesComboBox.Items.Text := AnIDBMainMan.GetTableFieldsNamesList (SELF.FProject, SELF.FLayer, false);
   // Set ComboBox on position that shows current field's name. if current field
   // has name 'ProgisID', set first position in ComboBox.
   iI := UF.FieldNamesComboBox.Items.IndexOf (ViewDBGrid.PushedColumnName);
   if iI < 0 then
      UF.FieldNamesComboBox.ItemIndex := 0
   else
      UF.FieldNamesComboBox.ItemIndex := iI;
   UF.FieldNamesComboBoxChange (SENDER);
   // Set DataModule a DataSet for TIDB_UpdateForm. It will be use its for check
   // type of field that will be updated.
   UF.ADataSet := ViewDataSource.DataSet;
   tmpstr:= format(ALangManager.GetValue (521), [UF.FieldNamesComboBox.Items[UF.FieldNamesComboBox.ItemIndex]] ) + #13;
   if SELF.ViewDBGrid.SelectedCount > 0 then tmpstr:=tmpstr + inttostr(SELF.ViewDBGrid.SelectedCount) + ' ' + ALangManager.GetValue (21)
      else tmpstr:=tmpstr + ALangManager.GetValue (20) + ' ('+ inttostr(ViewDataSource.DataSet.RecordCount) + ')';
   if UF.ShowModal = mrOK then
   if MessageBox (Handle, PChar(tmpstr), PChar (cs_ApplicationName), MB_YESNO + MB_ICONQUESTION)= IDYES then
   begin
      if not UF.WeHaveToDoAdvancedSQL then
      begin
         // if user has pressed OK button, we are doing update.
         sFieldName := UF.FieldNamesComboBox.Items[UF.FieldNamesComboBox.ItemIndex];

// ++ Commented by Cadmensky Version 2.3.6
{         if not AnIDBMainMan.UseNewATTNames (SELF.FProject) then
            sTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (SELF.FLayer))
         else
            sTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (SELF.FProject, SELF.FLayer);}
// -- Commented by Cadmensky Version 2.3.6

         AFieldType := SELF.DataQuery.FieldByName (sFieldName).DataType;
         // Check field type. Can we make update for this field?
         bSQLHasToBeExecuted := CheckFieldTypeSuitability (AFieldType);
      end
      else
         bSQLHasToBeExecuted := true;

      if bSQLHasToBeExecuted then
      begin
         self.SetScreenCursor;
         AnExecQuery := TDQuery.Create (SELF);
         AnExecQuery.Master := SELF.ALayerManager.DataAccessObject.DMaster;
         SELF.ShowStatus;
         StatusLabel.Caption := ALangManager.GetValue (277);
         Gauge.Progress := 0;
         Gauge.MaxValue := SELF.DataQuery.RecordCount;
         Application.ProcessMessages;
         try
            sSQL := UF.GenerateClause;
// ++ Cadmensky Version 2.3.4
            if SELF.ViewDBGrid.SelectedCount > 0 then
            begin
               iInternalIDField := FieldNoByName (cs_InternalID_FieldName);
               iProgisIDField := FieldNoByName ('PROGISID');
               if (iInternalIDField < 0) or (iProgisIDField < 0) then
               begin
                  AnExecQuery.Free;
                  UF.Free;
                  Exit;
               end;

               if SELF.sWhereClause <> '' then
               begin
                  if Pos ('WHERE', AnsiUpperCase (sSQL)) < 1 then
                     sSQL := sSQL + ' WHERE (' + SELF.sWhereClause + ') AND '
                  else
                     sSQL := sSQL + ' AND (' + SELF.sWhereClause + ') AND ';
               end
               else
                  if Pos ('WHERE', AnsiUpperCase (sSQL)) < 1 then
                     sSQL := sSQL + ' WHERE '
                  else
                     sSQL := sSQL + ' AND ';

               iJ := 0;
               sIDs := '';
               SELF.ARecordSet.MoveFirst;
               for iI := 0 to SELF.ARecordSet.RecordCount - 1 do
               begin
                  iMark := ExtractValue1 (SELF.ARecordSet.Fields[iInternalIDField].Value) - 1;
                  if SELF.ViewDBGrid.RowBits[iMark] then
                  begin
                     sIDs := sIDs + 'PROGISID = ' + ExtractValue (SELF.ARecordSet.Fields[iProgisIDField].Value) + ' OR ';
                     Inc (iJ);
                  end;
                  if iI mod 50 = 0 then gauge.Progress:=iI;

                  SELF.ARecordSet.MoveNext;
                  if iJ = ci_PackLength then
                  begin
                     if sIDs <> '' then
                     begin
                        sIDs := '(' + Copy (sIDs, 1, Length (sIDs) - 4) + ')';
                       AnExecQuery.ExecSQL (sSQL + sIDs);
                       gauge.Progress:=iI;
                     end;
                     iJ := 0;
                     sIDs := '';
                  end;
               end;
               if sIDs <> '' then
               begin
                  sIDs := '(' + Copy (sIDs, 1, Length (sIDs) - 4) + ')';
                  AnExecQuery.ExecSQL (sSQL + sIDs);
               end;
            end
            else
            begin
// -- Cadmensky Version 2.3.4
               if SELF.sWhereClause <> '' then
                  if Pos ('WHERE', AnsiUpperCase (sSQL)) < 1 then
                     sSQL := sSQL + ' WHERE ' + SELF.sWhereClause
                  else
                     sSQL := sSQL + ' AND (' + SELF.sWhereClause + ')';
               AnExecQuery.ExecSQL (sSQL);
               end;
         except
            self.RestoreScreenCursor;
            if not UF.WeHaveToDoAdvancedSQL then
               // 57=Update field '%s' of table of layer '%s' is not completed.
               MessageBox (Handle, PChar (Format (ALangManager.GetValue (57), [sFieldName, sLayerName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP)
            else
               // 328=Update of table of layer '%s' is not completed
               MessageBox (Handle, PChar (Format (ALangManager.GetValue (328), [sLayerName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
         end;
         self.RestoreScreenCursor;
         self.HideStatus;
         AnExecQuery.Free;
         ALayerManager.RefreshAllDataWindows;
      end;
   end;
   UF.Free;
end;

procedure TIDB_ViewForm.FindNextSBClick (Sender: TObject);
var
   iMark: Integer;
begin
   try
      iMark := SELF.DataQuery.FieldByName (cs_InternalID_FieldName).AsInteger;
   except
      iMark := -1;
   end;
   SELF.DataQuery.DisableControls;
   if not SELF.Find then
   begin
      SELF.GoToPrevPosition (iMark);
   // The end of attribute table of layer ''%s'' was reached. New record with value ''%s'' wasn''t found.
      MessageBox (Handle, PChar (Format (ALangManager.GetValue (60), [GetLayerNameFunc (SELF.FLayer), SELF.sSearchValue])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION);
      AToolbarManager.ButtonEnabled['FindNextRecords'] := false;
      AMenuManager.MenuEnabled['FindNextRecords'] := false;

   end
   else
      SELF.MakeRecordsStatus;

   SELF.DataQuery.EnableControls;
end;

function TIDB_ViewForm.Find: Boolean;
var
   sDataValue: AnsiString;
begin
   RESULT := false;

   SetScreenCursor;

   ShowStatus;
   // 329=Finding next match...
   StatusLabel.Caption := ALangManager.GetValue (329);
   Gauge.Progress := 0;
   Gauge.MaxValue := SELF.DataQuery.RecordCount - SELF.DataQuery.RecNo;
   Application.ProcessMessages;

   // And now I go in the cycle till I reach the end of an attribute table.
   while not SELF.DataQuery.EOF do
   begin
      SELF.DataQuery.Next;

      Gauge.Progress := Gauge.Progress + 1;
      Application.ProcessMessages;

      sDataValue := SELF.DataQuery.FieldByName (SELF.sSearchColumn).AsString;
      RESULT := SELF.CompareSearchValues (SELF.sSearchValue, sDataValue);
      if RESULT then
         try
            SELF.GoToPrevPosition (SELF.DataQuery.FieldByName (cs_InternalID_FieldName).AsInteger);
            Break;
         except
         end;

   end; // While do end
//   GoOut;
// ++ Cadmensky Version 2.3.3
   RestoreScreenCursor;
   HideStatus;
   Application.ProcessMessages;
// -- Cadmensky Version 2.3.3
end;

function TIDB_ViewForm.CompareSearchValues (sSearchValue, sDataValue: AnsiString): Boolean;
var
   sTempDataValue, sTempSearchValue: AnsiString;
begin
   RESULT := false;
   if not (loCaseInsensitive in SELF.loOptions) then
   begin
      sTempDataValue := AnsiUpperCase (sDataValue);
      sTempSearchValue := AnsiUpperCase (sSearchValue);
   end;

   if loPartialKey in SELF.loOptions then
   begin
         // if the user has seleted loPartialKey in the search parameters,
         // I go this way.
      if not (loCaseInsensitive in SELF.loOptions) then
      begin
         if Pos (sTempSearchValue, sTempDataValue) > 0 then
            RESULT := true;
      end
      else
         if Pos (sSearchValue, sDataValue) > 0 then
            RESULT := true;
   end
   else
   begin
         // if the user hasn't selected lo PartialKey in the search parameters,
         // I go this way.
      if not (loCaseInsensitive in SELF.loOptions) then
      begin
         if sTempSearchValue = sTempDataValue then
            RESULT := true;
      end
      else
         if sSearchValue = sDataValue then
            RESULT := true;
   end;
end;

procedure TIDB_ViewForm.SaveResult1Click (Sender: TObject);
begin
   // Save result:
   try
      StatusLabel.Caption := ALangManager.GetValue (127);
      Application.ProcessMessages;
      SELF.ALayerManager.SaveResults (SELF.FLayer, SELF.DataQuery, SELF.DataQuery.SQL.Text, SELF.Gauge);
   finally
   end;
end;

procedure TIDB_ViewForm.LoadData1Click (Sender: TObject);
var
   iMark: Integer;
   bLoadData: Boolean;
begin
   // Data loading...
   SELF.ALayerManager.DetectFieldsStates (SELF.FLayer, SELF.ViewDBGrid);

   Application.ProcessMessages;
   try
      try
         iMark := SELF.DataQuery.FieldByName (cs_InternalID_FieldName).AsInteger;
      except
         iMark := -1;
      end;
      // Save state of fields in DBGrid and then close the dataset.
      ALayerManager.DetectFieldsStates (SELF.FLayer, SELF.ViewDBGrid);
//      SELF.ClosePowerBroker;

      bLoadData := SELF.ALayerManager.LoadData (SELF.FLayer, SELF.DataQuery, SELF.Gauge);

      if bLoadData then
      begin
         try
            SELF.DataQuery.Open;
            SELF.GoToPrevPosition (iMark);
         except
         end;
         Create_Power_Broker (false);

      // Restore state of all fields and then refresh all data windows.
         ALayerManager.RestoreFieldsStates (SELF.FLayer, SELF.ViewDBGrid);
         SELF.ALayerManager.RefreshAllDataWindows;
         SELF.ApplyFieldVisibilityChanges;
         ALayerManager.DetectFieldsStates (SELF.FLayer, SELF.ViewDBGrid);
      end;
   finally
   end;
end;

procedure TIDB_ViewForm.MakeFieldAsInvisiblePopUpClick (Sender: TObject);
var
   Coord: TGridCoord;
   ACol: Integer;
   sFieldName: AnsiString;
begin
   Coord := ViewDBGrid.MouseCoord (ViewDBGrid.GetLastMouseX, ViewDBGrid.GetLastMouseY);
   ACol := Coord.X;
   if ViewDBGrid.FieldCount > 1 then
   begin
      sFieldName := ViewDBGrid.Fields[ACol].FieldName;
      SELF.DataQuery.FieldByName (sFieldName). Visible := false;
      SELF.ALayerManager.DetectFieldsStates (SELF.FLayer, SELF.ViewDBGrid);
      ShowRecordsForSelectedObjects;
   end;
end;

procedure TIDB_ViewForm.ApplyFieldVisibilityChanges;
var
   AFieldsVisibilityMan: TIDB_FieldsVisibilityManager;
begin
   try
      AFieldsVisibilityMan := SELF.ALayerManager.GetLayerFieldsVisibilityManager (SELF.FLayer);
      AFieldsVisibilityMan.RestoreFieldsStates (SELF.ViewDBGrid);
   finally
   end;
end;

procedure TIDB_ViewForm.StatesOfFieldsPopUpClick (Sender: TObject);
begin
end;

{ These 3 procedures do edit operations such as CopyToClipboard, CutToClipboard, PasteFromClipboard. }

procedure TIDB_ViewForm.Copy1Click (Sender: TObject);
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;

   if ViewDBGrid.DataSource.DataSet.State in [dsEdit, dsInsert] then
      if ViewDBGrid.InplaceEditor <> nil then
      begin
         ClipBoard.Clear; // Cadmensky
         SendMessage (ViewDBGrid.InplaceEditor.Handle, WM_COPY, 0, 0);
      end;
end;

procedure TIDB_ViewForm.Cut1Click (Sender: TObject);
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;

   if ViewDBGrid.DataSource.DataSet.State in [dsEdit, dsInsert] then
      if ViewDBGrid.InplaceEditor <> nil then
         SendMessage (ViewDBGrid.InplaceEditor.Handle, WM_CUT, 0, 0);
end;

procedure TIDB_ViewForm.Paste1Click (Sender: TObject);
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;

   if ViewDBGrid.DataSource.DataSet.State in [dsEdit, dsInsert] then
      if ViewDBGrid.InplaceEditor <> nil then
         SendMessage (ViewDBGrid.InplaceEditor.Handle, WM_PASTE, 0, 0);
end;

procedure TIDB_ViewForm.PopupMenu1Popup (Sender: TObject);
var
   Coord: TGridCoord;
   ACol, ARow: Integer;
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;

   // This is menu item that makes a field in DBGrid invisible.
   // But at least one field in the DBGrid always must be visible.
   // Also this item has to be disabled when the user clicka abroad DBGrid.
   Coord := ViewDBGrid.MouseCoord (ViewDBGrid.GetLastMouseX, ViewDBGrid.GetLastMouseY);
   ACol := Coord.X;
   ARow := Coord.Y;
   AMenuManager.MenuEnabled['SetFieldInvisible'] := (ViewDBGrid.FieldCount > 1) and (ACol > -1) and (ARow > -1);
   AMenuManager.MenuVisible['ResizeGridCell'] := SELF.ViewDBGrid.HorzSplitterPosition > -1;
   // Check RO state and make the UI.
   Formatfield.Enabled:= ViewDBGrid.Fields[ACol] is TFloatField;
   SELF.FormActivate (SENDER);
end;

procedure TIDB_ViewForm.AddSBClick (Sender: TObject);
begin
   DeselectAll1Click(Sender);
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
   try
   SELF.DataQuery.Append;
   SELF.DataQuery.Post;
   except end;
   Application.ProcessMessages;
end;

procedure TIDB_ViewForm.FirstRecordItemTableMenuClick (Sender: TObject);
begin
   ViewDBGrid.DataSource.DataSet.First;
end;

procedure TIDB_ViewForm.PrevRecordItemTableMenuClick (Sender: TObject);
begin
   ViewDBGrid.DataSource.DataSet.Prior;
end;

procedure TIDB_ViewForm.NextRecordItemTableMenuClick (Sender: TObject);
begin
   ViewDBGrid.DataSource.DataSet.Next;
end;

procedure TIDB_ViewForm.LastRecordItemTableMenuClick (Sender: TObject);
begin
   ViewDBGrid.DataSource.DataSet.Last;
end;

procedure TIDB_ViewForm.RefreshItemTableMenuClick (Sender: TObject);
var
   iMark: Integer;
begin
   SELF.DataQuery.DisableControls;
   try
      try
         iMark := SELF.DataQuery.FieldByName (cs_InternalID_FieldName).AsInteger;
      except
         iMark := -1;
      end;
      ALayerManager.StoreParametersOfViewForm (SELF.FLayer, SELF);

      Create_Power_Broker (false);

      SELF.DataQuery.ReOpen;
      SELF.GoToPrevPosition (iMark);
      ALayerManager.RestoreFieldsStates (SELF.FLayer, SELF.ViewDBGrid);
   finally
      SELF.DataQuery.EnableControls;
      SELF.ViewDBGrid.Invalidate;
   end;
end;

procedure TIDB_ViewForm.DeleteSBClick (Sender: TObject);
var
   iJ, iP: Integer;
   iMark: Integer;
   bDeleteConfirmed: Boolean; // User confirmed his wish to delete object from project.
// ++ Commented by Cadmensky Version 2.3.2
{   sAttTableName: AnsiString;
   bF: Boolean;}
// -- Commented by Cadmensky Version 2.3.2
   sIDs: AnsiString;
   MyLayer: Pointer;
begin
   if SELF.DataQuery.RecordCount < 1 then EXIT;

   if SELF.StringGrid1.Visible then
      Exit;

   if dgEditing in SELF.ViewDBGrid.Options then
      Exit;

   MyLayer := SELF.FLayer;

// ++ Commented by Cadmensky Version 2.3.2
{   if not AnIDBMainMan.UseNewATTNames (SELF.FProject) then
      sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (SELF.FLayer))
   else
      sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (SELF.FProject, SELF.FLayer);

   // Check whether required field of state exists or not.
   if ALayerManager.DataAccessObject.ADbItemsMan.FieldExists (sAttTableName, cs_StateFieldName, bF) <> ci_All_Ok then EXIT;
   // if it doesn't exist, create it.
   if not bF then
   begin
      try
         ALayerManager.DataAccessObject.ADbItemsMan.AddField (sAttTableName, cs_StateFieldName, 12, -1); // Type '12' is a byte type.
      except
         EXIT;
      end;
   end;}
// -- Commented by Cadmensky Version 2.3.2

   if ViewDBGrid.SelectedCount < 1 then
   begin
      if Trim (SELF.DataQuery.FieldByName ('PROGISID').AsString) <> '' then
      begin
         // do you want to delete record that has ProgisID = ''%s''?
         if MessageBox (Handle, PChar (Format (ALangManager.GetValue (153), [SELF.DataQuery.FieldByName ('PROGISID').AsString])), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrNO then
            EXIT;
         // do you want to delete this object from project?
         if MessageBox (Handle, PChar (ALangManager.GetValue (154)), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrYes then
         begin
            SELF.ShowSelectedItemsInProject (false);
            ALayerManager.DeleteObjectsInProject_FromDB (SELF.FLayer);
         end;
         // Mark it as deleted
         SELF.DataQuery.DisableControls;
         try
            SELF.DataQuery.Next;
            try
               iMark := SELF.DataQuery.FieldByName (cs_InternalID_FieldName).AsInteger;
            except
               iMark := -1;
            end;
            SELF.DataQuery.Prior;

            SELF.DataQuery.Edit;
            SELF.DataQuery.FieldByName (cs_StateFieldName).AsString := cs_ObjectIsDeleted;
            SELF.DataQuery.Post;

            SELF.DataQuery.Close;
            SELF.DataQuery.Open;
            ViewDataSourceStateChange (SENDER);
            GoToPrevPosition (iMark);
         finally
            SELF.DataQuery.EnableControls;
         end;
      end
      else
      begin
         // do you want to delete this record?
         if MessageBox (Handle, PChar (ALangManager.GetValue (155)), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrNO then
            EXIT;
         // Mark it as table deleted
         SELF.DataQuery.DisableControls;
         try
            SELF.DataQuery.Next;
            try
               iMark := SELF.DataQuery.FieldByName (cs_InternalID_FieldName).AsInteger;
            except
               iMark := -1;
            end;
            SELF.DataQuery.Prior;

            SELF.DataQuery.Edit;
            SELF.DataQuery.FieldByName (cs_StateFieldName).AsString := cs_ObjectIsDeleted;
            SELF.DataQuery.Post;

            SELF.DataQuery.Close;
            SELF.DataQuery.Open;
            ViewDataSourceStateChange (SENDER);
            GoToPrevPosition (iMark);
         finally
            SELF.DataQuery.EnableControls;
         end;
      end;
   end
   else
   begin
      // do you want to delete %d records?
      if MessageBox (Handle, PChar (Format (ALangManager.GetValue (156), [ViewDBGrid.SelectedCount])), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrNO then
         EXIT;
      // do you want to delete these objects from project?
      MakeBookmarksList (true);
      bDeleteConfirmed:=False;
      for iMark := 0 to ProgisIDList.count -1 do
          if ProgisIDList[iMark] > 0 then
             begin
             bDeleteConfirmed:=True;
             break;
             end;
      if  bDeleteConfirmed then if MessageBox (Handle, PChar (ALangManager.GetValue (157)), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrYes then
         bDeleteConfirmed := true
      else
         bDeleteConfirmed := false else bDeleteConfirmed:=False;

      SetScreenCursor;

      try
         iMark := SELF.DataQuery.FieldByName (cs_InternalID_FieldName).AsInteger;
      except
         iMark := -1;
      end;

      //bDeleteConfirmed);

      PrepareGauge ('Deleting...');
      iP := 0;
      ij := 0;
      while iP <= InternalIDList.Count - 1 do
      begin
         sIDs := '(';
         for iJ := 0 to ci_PackLength do
         begin
            sIDs := sIDs + ' ['+cs_InternalID_FieldName+'] = ' + IntToStr (INTERNALIDList[iP + iJ]) + ' OR ';
            if iP + iJ = InternalIDList.Count - 1 then
               BREAK;
         end; // For iJ end
         Inc (iP, ci_PackLength + 1);
         sIDs := Copy (sIDs, 1, Length (sIDs) - 4) + ')';
         try
            DataQuery.ExecSQL ('UPDATE [' + sAttTableName + '] ' +
               'SET [' + cs_StateFieldName + '] = ' + cs_ObjectIsDeleted +
               ' WHERE ' + sIDs);
            Gauge.Progress := ip;
         except
         end;
      end;

      if bDeleteConfirmed then
      begin
         SELF.ShowSelectedItemsInProject (false);
         ALayerManager.DeleteObjectsInProject_FromDB (SELF.FLayer);
      end;

      SELF.GoToPrevPosition (iMark);
   end; // else end

   Gauge.Visible := false;
   Create_Power_Broker (false);
   Application.ProcessMessages;
   SELF.ALayerManager.RefreshAllDataWindows;
   ViewDBGrid.Perform (WM_VSCROLL, SB_LINEUP, 0);
   ViewDBGrid.Invalidate;
   DeselectAll1Click(sender);
   RestoreScreenCursor;
end;

procedure TIDB_ViewForm.TablePropertiesPopUpClick (Sender: TObject);
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;

   TableStructurePopUpClick (SENDER);
end;

{ This procedure shows properties of the attribute table. I named it such strange name :)) due to
  some strange thing with name 'TablePropertiesMenuItem'. }

procedure TIDB_ViewForm.Ahaa1Click (Sender: TObject);
begin
   TablePropertiesPopUpClick (SENDER);
end;

procedure TIDB_ViewForm.ShowProjectSBClick (Sender: TObject);
begin
   if Assigned (FHintWin) then
      FHintWin.ReleaseHandle;

   ShowProjectProc (SELF.FProject);
end;

{ These 3 procedures insert a point or some points in a WinGIS project. }

procedure TIDB_ViewForm.InsertPoints (bMakeItForAllRecords: Boolean);
begin
   //
end;

procedure TIDB_ViewForm.AllRecInsertPointsClick (Sender: TObject);
begin
   SELF.InsertPoints (true);
end;

procedure TIDB_ViewForm.SelRecInsertPointsClick (Sender: TObject);
begin
   SELF.InsertPoints (false);
end;

procedure TIDB_ViewForm.ViewDTableAfterInsert (DataSet: TDataSet);
begin
   SELF.DataQuery.FieldByName (cs_StateFieldName).AsString := cs_ObjectIsJustAdded;
end;

procedure TIDB_ViewForm.ViewDataSourceStateChange (Sender: TObject);
var
   AField: TField;
   iI: Integer;
begin
   try
      if SELF.DataQuery.State = dsInActive then EXIT;
   except
      EXIT;
   end;

   for iI := 0 to SELF.DataQuery.Fields.Count - 1 do
   begin
      AField := SELF.DataQuery.Fields[iI];
      if FieldIsInternalUsageField (AField.FieldName) then
         AField.Visible := false;
   end;

   if SELF.bROMode then EXIT;
   if SELF.bIAmQueryResultWindow then EXIT;

   if (SELF.DataQuery.State = dsBrowse) or (SELF.DataQuery.State = dsInActive) then
   begin
      // Buttons

      //Menu items
      AMenuManager.MenuEnabled['EditRecord'] := true;
      AMenuManager.MenuEnabled['SaveChanges'] := false;
      AMenuManager.MenuEnabled['CancelChanges'] := false;
      // Edit buttons and menu items
      AToolbarManager.ButtonEnabled['Copy'] := false;
      AToolbarManager.ButtonEnabled['Cut'] := false;
      AToolbarManager.ButtonEnabled['Paste'] := false;

      AMenuManager.MenuEnabled['Copy'] := false;
      AMenuManager.MenuEnabled['Cut'] := false;
      AMenuManager.MenuEnabled['Paste'] := false;
   end
   else
   begin
      // Buttons

      //Menu items
      AMenuManager.MenuEnabled['EditRecord'] := false;
      AMenuManager.MenuEnabled['SaveChanges'] := true;
      AMenuManager.MenuEnabled['CancelChanges'] := true;
      // Edit buttons and menu items
      AToolbarManager.ButtonEnabled['Copy'] := true;
      AToolbarManager.ButtonEnabled['Cut'] := true;
      AToolbarManager.ButtonEnabled['Paste'] := Clipboard.HasFormat (CF_TEXT);

      AMenuManager.MenuEnabled['Copy'] := true;
      AMenuManager.MenuEnabled['Cut'] := true;
      AMenuManager.MenuEnabled['Paste'] := Clipboard.HasFormat (CF_TEXT);
   end;
end;

{ These 3 procedures insert a symbol or some symbols in a WinGIS project. }

procedure TIDB_ViewForm.InsertSymbols (bMakeItForAllRecords: Boolean);
begin

end;

procedure TIDB_ViewForm.AllRecInsertSymbolsClick (Sender: TObject);
begin
   InsertSymbols (true);
end;

procedure TIDB_ViewForm.SelRecInsertSymbolsClick (Sender: TObject);
begin
   InsertSymbols (false);
end;

{optimized by Brovak}

procedure TIDB_ViewForm.ShowSelectedItemsInProject (bNeedZoom: Boolean);
var
   iI, iJ: Integer;
   bInternalIDFieldIsPresented, bProgisIDFieldIsPresented: Boolean;
   sSQL: AnsiString;
   AField: TField;
   APB_Recordset: RecordSet;
   AForm: TForm;
   iProgisIDValue: integer;
var
   iInternalIDField, iProgisIDField, iMark: Integer;
//   OldRowBits: TBits;
//   iRowBitsSize: Integer;
begin
   SetScreenCursor;

   if AToolbarManager.ButtonDown['ShowSelectedRecords'] then
   begin
      iProgisIDValue := SELF.DataQuery.FieldByName ('PROGISID').AsInteger;
      ShowItemInProjectWithZoomProc (SELF.FProject, SELF.FLayer, iProgisIDValue);
      RestoreScreenCursor;
      exit;
   end;
   SELF.ALayerManager.GetReadyToReceiveIDsOfObjectsForMonitoring;

   if SELF.DataQuery.RecordCount < 1 then
      exit;

   SELF.ViewDBGrid.Enabled := false;

   RestoreScreenCursor;

   DeselectAllObjectsProc (SELF.FProject);

   SetScreenCursor;

   bInternalIDFieldIsPresented := SELF.DataQuery.FindField (cs_InternalID_FieldName) <> nil;
   bProgisIDFieldIsPresented := SELF.DataQuery.FindField ('PROGISID') <> nil;

   if bInternalIDFieldIsPresented and bProgisIDFieldIsPresented then
   begin
      // if the query result contains the internal ID field. Each record easy can be recognized.
      // The same way is when we use this form to displaying data of an attribute table. In this case
      // ADataSet will always contain as InternalID field as ProgisID field. Each record in this case
      // also can be easy recognized one from other.

      iInternalIDField := FieldNoByName (cs_InternalID_FieldName);
      iProgisIDField := FieldNoByName ('PROGISID');
      if (iInternalIDField = -1) or (iProgisIDField = -1) then
      begin
         RestoreScreenCursor;
         exit;
      end;

      SELF.ViewDBGrid.SelectedCount := 0;
      ARecordSet.MoveFirst;
      iProgisIDValue := -1;
      for iI := 0 to ARecordSet.RecordCount - 1 do
      begin
         iMark := ExtractValue1 (ARecordSet.Fields[iInternalIDField].Value) - 1;
         if SELF.ViewDBGrid.RowBits[iMark] then
         begin
            iProgisIDValue := ExtractValue1 (ARecordSet.Fields[iProgisIDField].Value);
            Inc (SELF.ViewDBGrid.SelectedCount);
            ShowSelectedItemsInProjectProc (SELF.FProject, SELF.FLayer, iProgisIDValue, true, false);
         end;
         ARecordSet.MoveNext;
      end;
      if bNeedZoom and (iProgisIDValue <> -1) then
         ShowSelectedItemsInProjectProc (SELF.FProject, SELF.FLayer, iProgisIDValue, true, bNeedZoom);
   end
   else
   begin
      if bProgisIDFieldIsPresented then
      begin
      // if the query result doesn't contain the internal ID field. It can be, for example, if the user has made
      // 'SELECT DISTINCT' query. In this case I create a temp query to select all possible records.
      // This way always will be done only with result of the query.
      // We need to get values from only ProgisID field.

         sSQL := 'SELECT PROGISID FROM ' + sQueryTableNames + ' WHERE ';
         for iI := 0 to SELF.DataQuery.FieldCount - 1 do
         begin
            AField := SELF.DataQuery.Fields[iI];
            if FieldIsInternalUsageField (AField.FieldName) or
               FieldIsCalculatedField (AField.FieldName) or
               FieldIsLinked (AField.FieldName) then
               CONTINUE;
            sSQL := sSQL + ' ([' + AField.FieldName + '] = ';
            if AField.DataType = ftString then
               sSQL := sSQL + '''' + AField.AsString + ''')'
            else
               sSQL := sSQL + AField.AsString + ')';
            sSQL := sSQL + ' AND ';
         end; // for iI end
         sSQL := Copy (sSQL, 1, Length (sSQL) - 5);
         APB_RecordSet := CoRecordSet.Create;
         APB_RecordSet.Open (sSQL, SELF.DataQuery.CentralData.ADO, adOpenKeyset, adLockReadOnly, adCmdText);
         if APB_RecordSet.State <> adStateOpen then
         begin
            raise Exception.Create (' TMP Recordset cannot be opened.');
         end;

         try
            iProgisIDField := FieldNoByNameEx ('PROGISID', APB_RecordSet);
            // Show object in WinGIS except the last object without zoom.
            if APB_RecordSet.RecordCount > 0 then
            begin
               APB_Recordset.MoveFirst;
               for iI := 0 to APB_RecordSet.RecordCount - 2 do
               begin
                  try
                     iProgisIDValue := ExtractValue1 (APB_RecordSet.Fields[iProgisIDField].Value);
                     ShowSelectedItemsInProjectProc (SELF.FProject, SELF.FLayer, iProgisIDValue, true, false);
                  except
                  end;
                  APB_RecordSet.MoveNext;
               end; // for iI end
         // Show in WinGIS the last object with zoom if it is required.
               try
                  iProgisIDValue := ExtractValue1 (APB_RecordSet.Fields[iProgisIDField].Value);
                  ShowSelectedItemsInProjectProc (SELF.FProject, SELF.FLayer, iProgisIDValue, true, bNeedZoom);
               except
               end;
            end;
         finally
            APB_RecordSet.Close;
         end;
      end;
   end;

   if bNeedZoom = true then
   begin
      for iJ := 0 to Screen.FormCount - 1 do
      begin
         AForm := Screen.Forms[iJ];
         if (AForm is TIDB_Monitor1Form) then
         begin
            RunMonitoringProc (SELF.FProject, false);
            AnIDBMainMan.ShowInfoWindow (SELF.FProject);
         end;
      end;
   end;

   SELF.AToolbarManager.ButtonEnabled['Monitoring'] := GetLayerItemsCountFunc (SELF.FProject, nil) > 0;
   SELF.ViewDBGrid.Enabled := true;
   RestoreScreenCursor;
   Application.ProcessMessages;
end;

{ then menu item and the button, which require to show seleted items with zoom, handler. }

procedure TIDB_ViewForm.ShowSelItemsWithZoomSBClick (Sender: TObject);
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;

   SELF.ShowSelectedItemsInProject (true);
   SELF.ViewDBGrid.Repaint;
end;
{ The button, which require to show seleted items without zoom, handler. }

procedure TIDB_ViewForm.ShowSelectItemsInProjectMenuClick (Sender: TObject);
begin
   SELF.ShowSelectedItemsInProject (false);
end;

procedure TIDB_ViewForm.PutExistingItemsInProjectIntoTheTable (bPutAllLayerItem: Boolean; showinfo : Boolean = True);
var
   iI, iInsertedItemQuantity: Integer;
   sMessage: AnsiString;
   PRTF: TIDB_ParamsOfRecordTemplateForm;
   IDA: TIDB_Access2DB_Level2;
   AStructureManager: TIDB_StructMan;
   sFieldName: AnsiString;
   pRecordTemplateItem: ^TrecRecordTemplateItem;
   ATemplateList: TList;
   iItemsCount, iItemIndex: Integer;
begin
   // Create the form in which the user will make a template of new records.
   PRTF := TIDB_ParamsOfRecordTemplateForm.Create (SELF.FProject, SELF, SELF.FLayer, SELF.ALayerManager.DataAccessObject);
   PRTF.SG.RowCount := 1;
   PRTF.SG.Cells[0, 0] := ALangManager.GetValue (310); // 310=Field name
   PRTF.SG.Cells[1, 0] := ALangManager.GetValue (311); // 311=Value
   // Fill the grid with field names of the attribute table.
   for iI := 0 to SELF.DataQuery.FieldCount - 1 do
   begin
      sFieldName := SELF.DataQuery.Fields[iI].FieldName;
      if FieldIsInternalUsageField (sFieldName) then CONTINUE;
      if FieldIsCalculatedField (sFieldName) then CONTINUE;
      if AnsiUpperCase (sFieldName) = 'PROGISID' then CONTINUE;
      if AnsiUpperCase (sFieldName) = 'GUID' then CONTINUE;

      PRTF.SG.RowCount := PRTF.SG.RowCount + 1;
      PRTF.SG.Cells[0, PRTF.SG.RowCount - 1] := sFieldName;
   end;
   if PRTF.SG.RowCount > 1 then PRTF.SG.FixedRows := 1;
   {if PRTF.ShowModal <> mrOk then
   begin
      PRTF.Free;
      EXIT;
   end;
   }
   // I'll insert many records and I haven't any necessary in checking of required fields each time.
   SELF.bCheckFieldPresentBeforeInsert := false;

   SetScreenCursor;

   SELF.DataQuery.DisableControls;
   ViewDBGrid.Visible := false;

   IDA := SELF.ALayerManager.DataAccessObject;
   AStructureManager := TIDB_StructMan.Create (SELF.FProject, IDA);
   AStructureManager.DetectStructure (IDA.DMaster, SELF.FLayer);
   try
      // Reconciling...
      ShowStatus;
      StatusLabel.Caption := ALangManager.GetValue (195);
      Application.ProcessMessages;

      SELF.KeyPreview := true;

      ATemplateList := TList.Create;
      for iI := 1 to PRTF.SG.RowCount - 1 do
      begin
         New (pRecordTemplateItem);
         pRecordTemplateItem.sFieldName := PRTF.SG.Cells[0, iI];
         pRecordTemplateItem.iFieldType := AStructureManager.GetFieldType (pRecordTemplateItem.sFieldName);
         pRecordTemplateItem.sValue := PRTF.SG.Cells[1, iI];
         ATemplateList.Add (pRecordTemplateItem);
      end;

      if bPutAllLayerItem then
         iItemsCount := GetLayerItemsCountFunc (SELF.FProject, ALayer)
      else
         iItemsCount := GetLayerItemsCountFunc (SELF.FProject, nil);
      Gauge.Progress := 0;
      Gauge.MaxValue := iItemsCount;
      ShowPercentAtWinGISProc (0, 11208,0);
      ALayerManager.GetReadyToInsertRecords (ALayer, ATemplateList);
      try
         iInsertedItemQuantity := 0;
         for iI := 0 to iItemsCount - 1 do
         begin

            if bPutAllLayerItem then
               iItemIndex := GetLayerItemIndexByNumberFunc (SELF.FProject, ALayer, iI, false)
            else
               iItemIndex := GetLayerItemIndexByNumberFunc (SELF.FProject, ALayer, iI, true);

            if iItemIndex <> -1 then
               if ALayerManager.InsertRecord (ALayer, iItemIndex, {PRTF.UpdateExistedRecordsCB.Checked}False) then
                  Inc (iInsertedItemQuantity);
            Gauge.Progress := Gauge.Progress + 1;
            ShowPercentAtWinGISProc (1, 11208, round(100/(iItemsCount+1)*iI));
            Application.ProcessMessages;
            if SELF.TheUserWantsToCancel then
               break;
         end; // for iI end
      except
      end;

   finally
      PRTF.Free;
      for iI := 0 to ATemplateList.Count - 1 do
      begin
         pRecordTemplateItem := ATemplateList[iI];
         Dispose (pRecordTemplateItem);
      end;
      ATemplateList.Clear;
      ATemplateList.Free;
// ++ Cadmensky Version 2.3.2
      AStructureManager.Free;
// -- Cadmensky Version 2.3.2
      // Now I am going to insert records one by one and I have to check required field each time.
      SELF.bCheckFieldPresentBeforeInsert := true;
      SELF.HideStatus;

// ++ Commented by Cadmensky Version 2.3.4
//      if iInsertedItemQuantity > 0 then
// -- Commented by Cadmensky Version 2.3.4
      SELF.RefreshItemTableMenuClick (SELF);

      ViewDBGrid.Visible := true;
      SELF.DataQuery.EnableControls;
      RestoreScreenCursor;
      SELF.KeyPreview := false;
   end;

   Create_Power_Broker;
   if AToolBarManager.ButtonDown['AutoAddToSelected'] then
      SELF.ShowRecordsForSelectedObjects;

   if (iInsertedItemQuantity > 0) and showinfo then
   begin
      // Make the project status as 'modified'.
      SetProjectModifiedProc (SELF.FProject);
      // All objects from layer ''%s'' of project ''%s'' which hasn''t attribute records in database table were added. Number of new added records: %d.
      sMessage := Format (ALangManager.GetValue (196), [GetLayerNameFunc (SELF.FLayer), GetProjectFullFileNameFunc (SELF.FProject), iInsertedItemQuantity]);
      MessageBox (SELF.Handle, PChar (sMessage), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION);
   end;
end;

procedure TIDB_ViewForm.AllExistingItemsInProject1Click (Sender: TObject);
begin
   SELF.PutExistingItemsInProjectIntoTheTable (true);
end;

procedure TIDB_ViewForm.SelectedExistingItemsInProject1Click (Sender: TObject);
begin
   if ViewDBGrid.SelectedCount<1 then begin
      self.AllExistingItemsInProject1Click(Sender);
      exit;
      end;
   if GetLayerItemsCountFunc (SELF.FProject, nil) > 0 then
      SELF.PutExistingItemsInProjectIntoTheTable (false)
   else
      // 333=There is no selected item in the project.
      self.AllExistingItemsInProject1Click(Sender);
      //MessageBox (Handle, PChar (ALangManager.GetValue (333)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
end;

procedure TIDB_ViewForm.ExistingItemsInTable1Click (Sender: TObject);
var
   iMark: Integer;
   iI, iNumberOfDeletedRecords: Integer;
   bTheDeleteActionTookPlace: Boolean;
   sMessage: AnsiString;
begin
   // 'Reconciling...'
   SELF.ShowStatus;
   StatusLabel.Caption := ALangManager.GetValue (195);
   SELF.DataQuery.DisableControls;
   try
      iMark := SELF.DataQuery.FieldByName (cs_InternalID_FieldName).AsInteger;
   except
      iMark := -1;
   end;
   SELF.DataQuery.First;
   SELF.Gauge.Progress := 0;
   Gauge.MaxValue := SELF.DataQuery.RecordCount;
   // Now we almost are ready.
   try
      iNumberOfDeletedRecords := 0;
      for iI := 0 to SELF.DataQuery.RecordCount - 1 do
      begin
         bTheDeleteActionTookPlace := false;
         // if ProgusID value isn't present or items with this ID does not present in the project, I move this record into the pattern table.
         if not ItemExistsInProjectFunc (SELF.FProject, SELF.FLayer, SELF.DataQuery.FieldByName ('PROGISID').AsInteger) then
         begin
            // And now delete record.
            SELF.DataQuery.Delete;
            bTheDeleteActionTookPlace := true;
            Inc (iNumberOfDeletedRecords);
         end;
         // if we didn't the moving, we must go on next record.
         if not bTheDeleteActionTookPlace then
            SELF.DataQuery.Next;
         Gauge.Progress := Gauge.Progress + 1;
         Application.ProcessMessages;
      end;
   finally
      SELF.GoToPrevPosition (iMark); // Return to the old position in table.
      SELF.DataQuery.EnableControls;
      SELF.HideStatus;
   end;
   SELF.PutExistingItemsInProjectIntoTheTable (true, False);
   if iNumberOfDeletedRecords > 0 then
   begin
      // All records from database''s table of layer ''%s'' of project ''%s'' which haven''t corresponding items in the project were deleted. Number of deleted records: %d.
      sMessage := Format (ALangManager.GetValue (201), [GetLayerNameFunc (SELF.FLayer), GetProjectFullFileNameFunc (SELF.FProject), iNumberOfDeletedRecords]);
      MessageBox (SELF.Handle, PChar (sMessage), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION);
   end;

   Create_Power_Broker;
end;

procedure TIDB_ViewForm.RunQueryBuilder1Click (Sender: TObject);
var
   QBF: TIDB_QBForm;
   sNewWindowCaption, sSQLText: AnsiString;
   listQueryWindows: TStringList;
   LQWF: TIDB_ListQueryWindowForm;
   iI: Integer;
   bQueryResultWindowWasShown: Boolean;
   AQueryResultForm: TIDB_ViewForm;
// ++ Cadmensky Version 2.3.2
   AForm: TForm;
// -- Cadmensky Version 2.3.2
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;

   QBF := TIDB_QBForm.Create (SELF.FProject, Application, ALayerManager.DataAccessObject, GetLayerNameFunc (SELF.FLayer), SELF.sAttTableName);
   if sender=EditQuery1 then
      begin
      qbf.CheckBox4.Checked:=True;
      qbf.CheckBox4.Visible:=False;
      qbf.Memo1.Lines.Text:= self.DataQuery.Text;
      end;
   QBF.ShowModal;

   if QBF.ModalResult <> mrOK then
   begin
      QBF.Free;
      EXIT;
   end;

   try
      sSQLText := QBF.GetSQLText;
// ++ Commented by Cadmensky Version 2.3.2
//      listQueryWindows := ALayerManager.GetQueryResultWindows (SELF.FProject);
// -- Commented by Cadmensky Version 2.3.2
// ++ Cadmensky Version 2.3.2
      listQueryWindows := TStringList.Create;
      for iI := 0 to Screen.FormCount - 1 do
      begin
         AForm := Screen.Forms[iI];
         if (AForm is TIDB_ViewForm) and
            (TIDB_ViewForm (AForm).AProject = AProject) and
            (TIDB_ViewForm (AForm).IAmQueryResultWindow) then
         begin
            listQueryWindows.Add (AForm.Caption);
            listQueryWindows.Objects[listQueryWindows.Count - 1] := AForm;
         end;
      end;
// -- Cadmensky Version 2.3.2
      if listQueryWindows.Count < 1 then
      begin
         // if we haven't window, we ask the user to type a caption of result window and make it.
         // New result
         sNewWindowCaption := ALangManager.GetValue (217);
         // Please type a name of result
         {if not InputQuery (cs_ApplicationName, ALangManager.GetValue (218), sNewWindowCaption) then
         begin
// ++ Cadmensky Version 2.3.2
            listQueryWindows.Clear;
// -- Cadmensky Version 2.3.2
            listQueryWindows.Free;
            EXIT;
         end; } //WAI not needed to insert an Queryname here
         AQueryResultForm := nil;
         bQueryResultWindowWasShown := ALayerManager.ShowQueryResultWindow (SELF.FProject, FLayer, TForm (AQueryResultForm), sNewWindowCaption, sSQLText);
      end
      else
      begin
         // if we have any result window, we ask user either to type a caption of new window
         // and create it or to select the existing window and use it.
         LQWF := TIDB_ListQueryWindowForm.Create (SELF);
         // I fill list with captions of window for dispalying result of a query.

         for iI := 0 to listQueryWindows.Count - 1 do
            LQWF.ListBox1.Items.Add (listQueryWindows[iI]);
         if LQWF.ShowModal = mrOk then
         begin
            if LQWF.RadioButton1.Checked then
            begin // The user selected 'Existing window'
               self.Querysaved:=True;
               AQueryResultForm := TIDB_ViewForm (listQueryWindows.Objects[LQWF.ListBox1.ItemIndex]);
               bQueryResultWindowWasShown := ALayerManager.ShowQueryResultWindow (SELF.FProject, FLayer, TForm (AQueryResultForm), '', sSQLText);
            end
            else
            begin // The user selected 'New window'
               AQueryResultForm := nil;
               bQueryResultWindowWasShown := ALayerManager.ShowQueryResultWindow (SELF.FProject, FLayer, TForm (AQueryResultForm), LQWF.Edit1.Text, sSQLText);
            end;
         end;
      end;
   finally
// ++ Cadmensky Version 2.3.2
      listQueryWindows.Clear;
      listQueryWindows.Free;
// -- Cadmensky Version 2.3.2
      // Set for the query result window the WHERE clause of the query.
      if bQueryResultWindowWasShown then
      begin
         // I placed the call of this procedure here 'cause I have different set of menus on the form
         // depending on whether exists 'ProgisID' field in the query result or not.
         AQueryResultForm.MakeWindowForDisplayingQueryResult (QBF.GetTableNames, QBF.GetWhereClause);
      end;
      QBF.Free;
   end;
end;

procedure TIDB_ViewForm.MakeThematicMap (MakeItForAllRecords: boolean);
var
   sSQL: AnsiString;
   iI: integer;
   sFieldName, sSourceTable: AnsiString;
   myQuery: TDQuery;
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
   if SELF.DataQuery.RecordCount < 1 then EXIT;

   if not ThematicMapDllExists then
   begin
      // 332=Thematic map dll (TM.dll) does not exist.
      MessageBox (Handle, PChar (ALangManager.GetValue (332)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
      EXIT;
   end;
   Application.ShowHint := false;
   SELF.ShowStatus;
   // 264=Thematic map making...
   StatusLabel.Caption := ALangManager.GetValue (264);

   //if not selected then all
   if not MakeItForAllRecords then
      MakeBookmarksList (true)
      else
      if (self.bIAmQueryResultWindow) or (self.bIAmUnderFilter) then
         begin
         SelectAll1Click(self);
         MakeItForAllRecords:=False;
         MakeBookmarksList (true);
         end;
   if (MakeItForAllRecords = false) and (ViewDBGrid.SelectedCount = 0) then
      Exit;
   sSourceTable:=sAttTableName;
   if (MakeItForAllRecords=FALSE) and (ProgisIDList.Count>0) and (ProgisIDLIst.Count<10000) then
     begin
      self.ALayerManager.DeleteATable('tmpthemmap');
      sSQL:='SELECT * INTO [tmpthemmap] FROM [' + sAttTableName + '] WHERE PROGISID=-1';
      myQuery:=TDQuery.Create(Self);
      try
       myQuery.Master := SELF.ALayerManager.DataAccessObject.DMaster;
       myQuery.ExecSQL(sSQL);
       gauge.MaxValue:= ProgisIDList.count -1;
       FOR iI:= 0 to ProgisIDList.count -1 do
            begin
            if  ProgisIDList[iI]< 1 then continue;
            sSQL:='INSERT INTO [tmpthemmap] SELECT * FROM [' + sAttTableName + '] WHERE PROGISID=' +  inttostr(ProgisIDList[iI]);
            myQuery.ExecSQL(sSql);
            if iI mod 10 = 0 then gauge.Progress:= iI;
            end;
       sSourceTable:='tmpthemmap';
      finally
       myQuery.free;
       if sSourceTable<>'tmpthemmap' then showmessage('Error creating temporary table, use all records!');
      end;
     end;
   SELF.HideStatus;
   Screen.Cursor := crDefault;
   Gauge.Visible := false;
   MakeRecordsStatus;

   //if not selected then all
   SendToTMSelectedIDProc (SELF.FProject, ProgisIDList);
   sSQL := 'SELECT ';
   for iI := 0 to SELF.DataQuery.Fields.Count - 1 do
   begin
      sFieldName := SELF.DataQuery.Fields[iI].FieldName;
      if not FieldIsInternalUsageField (sFieldName) and
         not (FieldIsCalculatedField (sFieldName) and
         not SELF.ALayerManager.GetCalcFieldIsUsed (SELF.FLayer, sFieldName)) and
         not FieldIsLinked (sFieldName) then
         sSQL := sSQL + '[' + SELF.DataQuery.Fields[iI].FieldName + '], ';
   end;

   sSQL := Copy (sSQL, 1, Length (sSQL) - 2);
   sSQL := sSQL + ' FROM [' + sSourceTable + ']';


   try
      ShowNewThematicMapDialogProc (SELF.FProject,
         SELF.DataQuery.Connection,
         sSQL,
         'PROGISID',
         SELF.ViewDBGrid.PushedColumnName);

      RedrawProjectWindowProc (SELF.FProject);

   finally
      Application.ShowHint := true;

   end;
end;

{ These 3 procedures make annotations in a project. }
//modified by Brovak

procedure TIDB_ViewForm.MakeAnnotations (bMakeItForAllRecords: Boolean);
var
   AnAnnotMaker: TIDB_AnnotMaker;
   slAnnotFieldNames: TStringList;
   iInternalIDFieldNo, iProgisIDFieldNo: Integer;
   PrePostList: TStringList;
   procedure DoIt1;
   var
      iI: Integer;
      iObjectID: Integer;
      sAnnotFieldName: AnsiString;
      iAnnotFieldNo: Integer;
      davor, danach : String;
   begin
      iObjectID := ExtractValue1 (SELF.ARecordSet.Fields[iProgisIDFieldNo].Value);
      if iObjectID > 0 then
         try
            AnAnnotMaker.ClearAnnotData;
            for iI := 0 to slAnnotFieldNames.Count - 1 do
            begin

               sAnnotFieldName := slAnnotFieldNames[iI];
               iAnnotFieldNo := FieldNoByName (sAnnotFieldName);
               davor:=''; danach:='';
               if prepostlist.Count = slAnnotFieldNames.Count*2 then
                  begin
                  davor:= stringreplace(prepostlist[iI*2],'~|','',[]);
                  danach:= stringreplace(prepostlist[(iI*2)+1],'~|','',[]);
                  end;
               AnAnnotMaker.CorrectAnnotData (sAnnotFieldName,ansistring(davor+ ExtractValue (SELF.ARecordSet.Fields[iAnnotFieldNo].Value)+danach));
            end; // for iI end

            AnAnnotMaker.MakeAnnotationInProject (SELF.FLayer, iObjectID);
         except;
         end;
   end;

var
   AnIDBDescMan: TIDB_DescMan;
   iI: Integer;
   iMark: Integer;
   //tmplist : TSTringList;
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
   if (bMakeItForAllRecords = false) and
      (ViewDBGrid.SelectedCount = 0) then
      Exit;

   if SELF.DataQuery.RecordCount < 1 then EXIT;

   iInternalIDFieldNo := FieldNoByName (cs_InternalID_FieldName);
   iProgisIDFieldNo := FieldNoByName ('PROGISID');
   if (iInternalIDFieldNo < 0) or (iProgisIDFieldNo < 0) then
      Exit;

   AnIDBDescMan := ALayerManager.GetLayerDescMan (SELF.FLayer);

   slAnnotFieldNames := TStringList.Create;
   slAnnotFieldNames.Text := AnIDBDescMan. GetAnnotationFieldNames;
   // Check the fields in the annotation's fields list and in the attribute table.
   CheckSpecialFieldsAndAttTableStruct (SELF.FProject, ALayerManager.DataAccessObject, SELF.FLayer, slAnnotFieldNames);
   // if the list of the annotation's fields is empty, I require of an user to select them.
   if slAnnotFieldNames.Count < 1 then
   begin
      // 409=There is no any selected field as a source for annotations. Please, select the source fields!
      MessageBox (Handle, PChar (ALangManager.GetValue (409)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION);
      SELF.TableStructurePopUpClick (nil);

      slAnnotFieldNames.Text := AnIDBDescMan.GetAnnotationFieldNames;

      // I again try to check what user has selected and the attribute table structure.
      // if after that the list will already be empty, I go out in this case.
      if slAnnotFieldNames.Count < 1 then
      begin
// ++ Cadmensky Version 2.3.5
         slAnnotFieldNames.Free;
// -- Cadmensky Version 2.3.5
         Exit;
      end;
   end;
   // Put field's names in Annotation's maker.
   AnAnnotMaker := TIDB_AnnotMaker.Create (SELF.FProject);

   for iI := 0 to slAnnotFieldNames.Count - 1 do
      AnAnnotMaker.AddInAnnotData (slAnnotFieldNames[iI]);
   // Ask user about annotation's patameters.
   if not AnAnnotMaker.AnnotationSettingsDialogExecute (SELF.FLayer) then
   begin
// ++ Cadmensky Version 2.3.5
      AnAnnotMaker.Free;
//      AnAnnotMaker.DeleteAnnotData;
// -- Cadmensky Version 2.3.5
      slAnnotFieldNames.Free;
      Exit;
   end;

// ++ Cadmensky Version 2.3.5
   SELF.SetScreenCursor;
   slAnnotFieldNames.Text := AnAnnotMaker.GetAnnotationFieldNames;
   {tmplist:=TStringList.create;
   tmplist.Text:=slAnnotFieldNames.Text;
   for iI:=0 to tmplist.count-1 do tmplist[iI]:=copy(tmplist[iI],1,pos('��',tmplist[iI])-1);}
   //I will not overwrite my Lable-Settings form the table properties:
   //AnIDBDescMan.SetAnnotationFieldNames (SlAnnotFieldnames.Text);
   //tmplist.free;
   AnIDBDescMan.SetUserChoice;
// -- Cadmensky Version 2.3.5

   // And now we start our works.
   AnIDBMainMan.Set_IAmGeneratingObjectsByMyself_Sign (SELF.FProject, true);
   SetNessesitySaveUndoDataSignProc (SELF.FProject, false);

   // Make annotations...
   SELF.ShowStatus;
   Gauge.MaxValue := SELF.ARecordSet.RecordCount;
   StatusLabel.Caption := ALangManager.GetValue (265);
   Application.ProcessMessages;

   SELF.ARecordSet.MoveFirst;
   PrePostlist:=TStringList.Create;
   if Fileexists(ExtractFilePath (Application.ExeName) + 'prepost.tmp') then
      PrePostlist.LoadFromFile(ExtractFilePath (Application.ExeName) + 'prepost.tmp');
   for iI := 0 to SELF.ARecordSet.RecordCount - 1 do
   begin
      if not bMakeItForAllRecords then
      begin
         iMark := ExtractValue1 (SELF.ARecordSet.Fields[iInternalIDFieldNo].Value) - 1;
         if SELF.ViewDBGrid.RowBits[iMark] then
            DoIt1;
      end
      else
         DoIt1;
      SELF.ARecordSet.MoveNext;
      Gauge.Progress := Gauge.Progress + 1;
      Application.ProcessMessages;
      if SELF.bTheUserWantsToCancel then
         Break;
   end;
   PrePostlist.free;
   SELF.HideStatus;

   AnIDBMainMan.Set_IAmGeneratingObjectsByMyself_Sign (SELF.FProject, false);

   SetNessesitySaveUndoDataSignProc (SELF.FProject, true);
   SaveUndoDataProc (SELF.FProject, utUnInsert);
   slAnnotFieldNames.Clear;
   slAnnotFieldNames.Free;
   AnAnnotMaker.Free;

   Screen.Cursor := crDefault;
   Gauge.Visible := false;
   MakeRecordsStatus;
   Application.ProcessMessages;

// ++ Cadmensky Version 2.3.5
   SELF.ViewDBGrid.Invalidate;
   SELF.RestoreScreenCursor;
// -- Cadmensky Version 2.3.5
end;

procedure TIDB_ViewForm.AllRecAnnot1Click (Sender: TObject);
begin
   MakeAnnotations (true);
end;

procedure TIDB_ViewForm.SelRecAnnot1Click (Sender: TObject);
begin
   MakeAnnotations (false);
end;

(*********************)
{ These 3 procedures make charts in a project. }

procedure TIDB_ViewForm.MakeCharts (bMakeItForAllRecords: Boolean);
var
   AnChartMaker: TIDB_ChartMaker;
   slChartFieldNames: TStringList;
   iInternalIDFieldNo, iProgisIDFieldNo: Integer;
   ini : TIniFile;
   ddespeperatorsign : String;
   procedure DoIt1;
   var
      iI: Integer;
      iObjectID: Integer;
      sChartFieldName: AnsiString;
      sChartFieldValue: AnsiString;
      iChartFieldNo: Integer;
      SendString: AnsiString;
   begin
      try
      ini:= TIniFile.Create(stringreplace(Application.ExeName,'.exe','.ini',[rfIgnoreCase]));
      ddespeperatorsign:=ini.ReadString('Settings','DDESeparateSign',',');
      if ddespeperatorsign=',' then ddespeperatorsign:=ini.ReadString('Settings','DDESeperateSign',',');
      ini.free;
      finally
      iObjectID := ExtractValue1 (SELF.ARecordSet.Fields[iProgisIDFieldNo].Value);
      if iObjectID > 0 then
      begin
         AnChartMaker.ClearChartData;
         SendString := '[' + IntToStr (iObjectID) + '][';

         for iI := 0 to slChartFieldNames.Count - 1 do
         begin
            sChartFieldName := slChartFieldNames[iI];
            iChartFieldNo := FieldNoByName (sChartFieldName);
            if iChartFieldNo < 0 then
               Continue;
            sChartFieldValue := ExtractValue (SELF.ARecordSet.Fields[iChartFieldNo].Value);
            AnChartMaker.CorrectChartData (sChartFieldName, sChartFieldValue);
            SendString := SendString + sChartFieldName + ddespeperatorsign + sChartFieldValue + ddespeperatorsign;
         end; // for iI end

         SendString := Copy (Sendstring, 1, Length (SendString) - 1) + ']';
         AnChartMaker.SendSpecialForChart (SELF.FProject, SendString);
      end;
      end;
   end;

var
   ADescMan: TIDB_DescMan;
   iI: Integer;
   iMark: Integer;
   TempState: TWindowState;
   DestLayer: Pointer;
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;

   if (bMakeItForAllRecords = false) and
      (ViewDBGrid.SelectedCount = 0) then
      Exit;

   ADescMan := ALayerManager.GetLayerDescMan (SELF.FLayer);

   slChartFieldNames := TStringList.Create;
   slChartFieldNames.Text := ADescMan.GetChartFieldNames;
   // Check the fields in the chart's fields list and in the attribute table.
   CheckSpecialFieldsAndAttTableStruct (SELF.FProject, ALayerManager.DataAccessObject, SELF.FLayer, slChartFieldNames);
   // if the list of the chart's fields is empty, I require of an user to select them.
   if slChartFieldNames.Count < 1 then
   begin
      // 409=There is no any selected field  for charts. Please, select the source fields!
      MessageBox (Handle, PChar (ALangManager.GetValue (501)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION);
      SELF.TableStructurePopUpClick (nil);
      slChartFieldNames.Text := ADescMan.GetChartFieldNames;
      // I again try to check what user has selected and the attribute table structure.
      // if after that the list will already be empty, I go out in this case.
      if slChartFieldNames.Count < 1 then
      begin
         slChartFieldNames.Free;
         Exit;
      end;
   end;

// ++ Cadmensky Version 2.3.5
   SELF.SetScreenCursor;
// -- Cadmensky Version 2.3.5

   iInternalIDFieldNo := FieldNoByName (cs_InternalID_FieldName);
   iProgisIDFieldNo := FieldNoByName ('PROGISID');
   if (iInternalIDFieldNo < 0) or (iProgisIDFieldNo < 0) then
      Exit;
   // Put field's names in Annotation's maker.
   AnChartMaker := TIDB_ChartMaker.Create (SELF.FProject);
   AnChartMaker.DeleteChartData;

   for iI := 0 to slChartFieldNames.Count - 1 do
      AnChartMaker.AddInChartData (slChartFieldNames[iI]);
   DestLayer := LayerForInsertFunc (SELF.FProject, SELF.FLayer);

   AnIDBMainMan.Set_IAmGeneratingObjectsByMyself_Sign (SELF.FProject, true);

   SetNessesitySaveUndoDataSignProc (SELF.FProject, false);

   // Insert annotations for all records.
//   SELF.Gauge.Progress := 0;
   Gauge.MaxValue := SELF.DataQuery.RecordCount;
   // Make annotations...
   SELF.ShowStatus;
   StatusLabel.Caption := ALangManager.GetValue (504);

   SELF.ARecordSet.MoveFirst;
   for iI := 0 to SELF.ARecordSet.RecordCount - 1 do
   begin
      if not bMakeItForAllrecords then
      begin
         iMark := ExtractValue1 (SELF.ARecordSet.Fields[iInternalIDFieldNo].Value);
         if SELF.ViewDBGrid.RowBits[iMark] then
            DoIt1;
      end
      else
         DoIt1;

      SELF.ARecordSet.MoveNext;
      Gauge.Progress := Gauge.Progress + 1;
      Application.ProcessMessages;
      if bTheUserWantsToCancel then
         Break;
   end;
   SELF.HideStatus;
   // Now we end our work.
   TempState := SELF.WindowState;
   SELF.WindowState := wsMinimized;
   AnChartMaker.MakeSpecialChart (SELF.FProject, DestLayer);
   SELF.WindowState := TempState;
   AnIDBMainMan.Set_IAmGeneratingObjectsByMyself_Sign (SELF.FProject, false);

   SetNessesitySaveUndoDataSignProc (SELF.FProject, true);
   SaveUndoDataProc (SELF.FProject, utUnInsert);

   slChartFieldNames.Free;
   AnChartMaker.Free;

//   Screen.Cursor := crDefault;
   SELF.HideStatus;
   Gauge.Visible := false;
   MakeRecordsStatus;
   Application.ProcessMessages;
// ++ Cadmensky Version 2.3.5
   SELF.RestoreScreenCursor;
   SELF.ViewDBGrid.Invalidate;
// -- Cadmensky Version 2.3.5
end;

procedure TIDB_ViewForm.AllRecChart1Click (Sender: TObject);
begin
   MakeCharts (true);
end;

procedure TIDB_ViewForm.SelRecChart1Click (Sender: TObject);
begin
   MakeCharts (false);
end;

procedure TIDB_ViewForm.AllRecTM1Click (Sender: TObject);
begin
   MakeThematicMap (true);
end;

procedure TIDB_ViewForm.SelRecTM1Click (Sender: TObject);
begin
   MakeThematicMap (false);
end;

(********************)

procedure TIDB_ViewForm.SortAscPopUpClick (Sender: TObject);
var
   iMark: Integer;
   sTableName: AnsiString;
begin
if  FieldNoByName(ViewdbGrid.PushedColumnName)< 0 then exit;
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;

   SELF.DataQuery.DisableControls;
   try
      try
         iMark := SELF.DataQuery.FieldByName (cs_InternalID_FieldName).AsInteger;
      except
         iMark := -1;
      end;
      ViewDBGrid.SelectedRows.Clear;
      SELF.DataQuery.Sort := '[' + ViewDBGrid.PushedColumnName + '] ASC' + ', [' + cs_InternalID_FieldName + '] DESC';
      SELF.GoToPrevPosition (iMark);
   finally
      SELF.DataQuery.EnableControls;
   end;
   CPB_Order := ' ASC';

  { CPB_SortField := ViewDBGrid.PushedColumnName;
   sTableName := ALayerManager.GetTableName (SELF.FLayer, CPB_SortField);
   if sTableName <> '' then
      CPB_SortField := '[' + sTableName + '].[' + CPB_SortField + ']';
   CPB_SortField := CPB_SortField + CPB_Order;

   if AnsiUpperCase (ViewDBGrid.PushedColumnName) <> 'PROGISID' then
      CPB_SortField := CPB_SortField + ', [' + SELF.sAttTableName + '].[PROGISID] ASC'; }
   TDADODataSet (ViewDataSource.DataSet).Sort:= SELF.DataQuery.Sort;
   CPB_SortField:=SELF.DataQuery.Sort;
   Create_Power_Broker;
   ShowRecordsForSelectedObjects;
end;

procedure TIDB_ViewForm.SortDescPopUpClick (Sender: TObject);
var
   iMark: Integer;
   sTableName: AnsiString;
begin
   if FieldNoByName(ViewdbGrid.PushedColumnName)< 0 then exit;
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
   {brovak}

   SELF.DataQuery.DisableControls;
   try
      try
         iMark := SELF.DataQuery.FieldByName (cs_InternalID_FieldName).AsInteger;
      except
         iMark := -1;
      end;
      ViewDBGrid.SelectedRows.Clear;
      SELF.DataQuery.Sort := '[' + ViewDBGrid.PushedColumnName + '] DESC'+ ', [' + cs_InternalID_FieldName + '] ASC';
      SELF.GoToPrevPosition (iMark);
   finally
      ViewDataSource.DataSet.EnableControls;
   end;
   CPB_Order := ' DESC';
   {
   CPB_SortField := ViewDBGrid.PushedColumnName;
   sTableName := ALayerManager.GetTableName (SELF.FLayer, CPB_SortField);
   if sTableName <> '' then
      CPB_SortField := '[' + sTableName + '].[' + CPB_SortField + ']';
   CPB_SortField := CPB_SortField + CPB_Order;

   if AnsiUpperCase (ViewDBGrid.PushedColumnName) <> 'PROGISID' then
      CPB_SortField := CPB_SortField + ', [' + SELF.sAttTableName + '].[PROGISID] DESC';  }
   TDADODataSet (ViewDataSource.DataSet).Sort:= SELF.DataQuery.Sort;
   CPB_SortField := SELF.DataQuery.Sort;
   Create_Power_Broker (false);

   ShowRecordsForSelectedObjects;
end;

procedure TIDB_ViewForm.MDI_SDI_SwitchPopUpClick (Sender: TObject);
var
   AState: TWindowState;
{   iI: Integer;
   AForm: TForm;}
begin
// ++ Commented by Cadmensky Version 2.3.8
{   if SELF.WindowState <> wsMaximized then
   begin
      iMyHeight := SELF.Height;
      iMyWidth := SELF.Width;
   end;}
// -- Commented by Cadmensky Version 2.3.8

   if SELF.FormStyle = fsMDIChild then
   begin
      // Make it as 'Stay on top'.
      SELF.FormStyle := fsStayOnTop;
      SELF.BorderStyle := bsSizeable;

      SELF.Top := iMyTop;
      SELF.Left := iMyLeft;
      SELF.Width := iMyWidth;
      SELF.Height := iMyHeight;

      AMenuManager.MenuChecked['StayOnTop'] := true;
      AMenuManager.MenuVisible['WindowsMenu'] := false;
      AMenuManager.MenuEnabled['ShowProject'] := false;
      AToolbarManager.ButtonVisible['ShowProject'] := false;
      SELF.AToolbar.RepositionButtons;
      AMenuManager.MenuEnabled['VertAlign'] := true;
      AMenuManager.MenuEnabled['HorzAlign'] := true;
   end
   else
   begin
      // Make it as not 'Stay on top' but as 'Maximized'.
      AState := GetMDIChildWindowStateFunc;

      iMyTop := SELF.Top;
      iMyLeft := SELF.Left;
// ++ Cadmensky Version 2.3.8
      iMyHeight := SELF.Height;
      iMyWidth := SELF.Width;
// -- Cadmensky Version 2.3.8

      AMenuManager.MenuVisible['WindowsMenu'] := true;
      SELF.FormStyle := fsMDIChild;
      SELF.BorderStyle := bsSizeable;
      SELF.WindowState := AState;

      AMenuManager.MenuChecked['StayOnTop'] := false;
      AMenuManager.MenuEnabled['ShowProject'] := true;
      AToolbarManager.ButtonVisible['ShowProject'] := true;
      SELF.AToolbar.RepositionButtons;
      AMenuManager.MenuEnabled['VertAlign'] := false;
      AMenuManager.MenuEnabled['HorzAlign'] := false;
   end;
end;

procedure TIDB_ViewForm.ViewDBGridMouseUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
   SELF.MakeRecordsStatus;
   EnableSort;
   if Button <> mbRight then EXIT;
   if SELF.FormStyle <> fsMDIChild then EXIT;
   if SELF.WindowState = wsMaximized then EXIT;
   // Count the coordinates of the upper-left corner of the window in the ablsolute screen coordinates.
   // then count the height of the WinGIS window toolbars.
   iMyTop := Mouse.CursorPos.Y - Y - (SELF.Height - SELF.ClientHeight - GetSystemMetrics (SM_CYFRAME));
   iMyLeft := Mouse.CursorPos.X - X - GetSystemMetrics (SM_CXFRAME);
   iHeightOfToolBarsOfMainMDIForm := iMyTop - SELF.Top - AWinGISMainForm.Top;
end;

procedure TIDB_ViewForm.ViewDBGridKeyDown (Sender: TObject; var Key: Word; Shift: TShiftState);
begin
   if ssCTRL in Shift then
   begin
      case Key of // Key with Control button
         Ord ('D'), Ord ('d'): DeleteSB.Click; // Ctrl+D or Ctrl+d - Delete current record
// ++ Commented by Cadmensky Version 2.3.8
//         vk_Insert: AddSB.Click; // Ctrl+Insert - Add new record
// -- Commented by Cadmensky Version 2.3.8
         vk_Prior, vk_Next, // Ctrl+PageUp, Ctrl+PageDown
            Ord ('F'), Ord ('f'), // Find record in table
            Ord ('T'), Ord ('t'), // Show table structure
            Ord ('S'), Ord ('s'), // Save result
            Ord ('L'), Ord ('l'), // Load data from external source
            Ord ('Q'), Ord ('q'), // QueryBuilder
            Ord ('U'), Ord ('u'): // Update table's data by user's wish
            begin
            end;
      else
         if (ViewDataSource.DataSet.State = dsBrowse) // if we not in edit or insert mode then key = 0
            or
            (ViewDataSource.DataSet.State = dsInactive) then
            Key := 0;
      end; // Case end
   end
   else
   begin
      case Key of // Key without Control button
         vk_F7, // Show project
            vk_Tab,
            vk_Up, vk_Down:
            begin
            end; // Up and Down arrows key - previous or next record
      else
         if (ViewDataSource.DataSet.State = dsBrowse) // if we not in edit or insert mode then key = 0
            or
            (ViewDataSource.DataSet.State = dsInactive) then
            Key := 0;
      end; // case end
   end;
end;

{procedure TIDB_ViewForm.ShowAllQueriedItemsMenuItemClick (Sender: TObject);
var
   iI, iMark, iJ, iItemID: Integer;
   ADataSet: TDADODataSet;
   bItIsTheLastRecord,
      bInternalIDFiledIsPresented,
      bProgisIDFieldIsPresented: Boolean;
   sSQL: AnsiString;
   AQuery: TDQuery;
   AField: TField;
begin
   ADataSet := TDADODataSet (ViewDataSource.DataSet);
   if ADataSet.RecordCount < 1 then EXIT;

   MakeBookmarksList (true);

   ADataSet.DisableControls;
   try
      iMark := ADataSet.FieldbyName (cs_InternalID_FieldName).AsInteger;
   except
      iMark := -1;
   end;
   ShowStatus;
   // 384=Selecting...
   StatusLabel.Caption := ALangManager.GetValue (384);
   Gauge.Progress := 0;
   Gauge.MaxValue := ADataSet.RecordCount;
   Application.ProcessMessages;
   try
      bInternalIDFiledIsPresented := ADataSet.FindField (cs_InternalID_FieldName) <> nil;
      bProgisIDFieldIsPresented := ADataSet.FindField ('PROGISID') <> nil;

      AQuery := TDQuery.Create (SELF);
      AQuery.Master := ADataSet.Master;

      ADataSet.First;
      for iI := 0 to ADataSet.RecordCount - 1 do
      begin
         bItIsTheLastRecord := iI = ADataSet.RecordCount - 1;
         if bInternalIDFiledIsPresented then
         begin
            if bProgisIDFieldIsPresented then
               iItemID := ViewDataSource.DataSet.FieldByName ('PROGISID').AsInteger
            else
            begin
               if AQuery.Active then AQuery.Close;
               AQuery.SQL.Text := 'SELECT PROGISID FROM ' + sQueryTableNames + ' WHERE [' + cs_InternalID_FieldName + '] = ' + ADataSet.FieldByName (cs_InternalID_FieldName).AsString;
               try
                  AQuery.Open;
                  iItemID := AQuery.Fields[0].AsInteger;
               except
                  iItemID := 0;
               end;
               AQuery.Close;
            end;

            // if the query result contains the internal ID field. Each record easy can be recognized.
            if not bItIsTheLastRecord then // Show in winGIS all object except the last without zooming.
               ShowSelectedItemsInProjectProc (SELF.FProject, SELF.FLayer, iItemID, true, false)
            else // Show in WinGIS the last object with zoom if it is required.
               ShowSelectedItemsInProjectProc (SELF.FProject, SELF.FLayer, iItemID, true, true);
         end
         else
         begin
            // if the query result doesn't contain the internal ID field. It can be, for example, if the user has made
            // 'SELECT DISTINCT' query. In this case I create a temp query to select all possible records.
            // We need to get values from only ProgisID field.
            sSQL := 'SELECT PROGISID FROM ' + sQueryTableNames + ' WHERE ';
            for iJ := 0 to ADataSet.FieldCount - 1 do
            begin
               AField := ADataSet.Fields[iJ];
               if AField.Visible then
               begin
                  if FieldIsInternalUsageField (AField.FieldName) then CONTINUE;
                  sSQL := sSQL + ' ([' + AField.FieldName + '] = ';
                  if AField.DataType = ftString then
                     sSQL := sSQL + '''' + AField.AsString + ''')'
                  else
                     sSQL := sSQL + AField.AsString + ')';
                  sSQL := sSQL + ' and ';
               end;
            end; // for iJ end
            sSQL := Copy (sSQL, 1, Length (sSQL) - 5);
            try
               if AQuery.Active then AQuery.Close;
               AQuery.SQL.Text := sSQL;
               AQuery.Open;
               // Show object in WinGIS except the last object without zoom.
               for iJ := 0 to AQuery.RecordCount - 2 do
               begin
                  ShowSelectedItemsInProjectProc (SELF.FProject, SELF.FLayer, AQuery.Fields[0].AsInteger, true, false);
                  AQuery.Next;
               end; // for iI end
               // Show in WinGIS the last object with zoom if it is required.
               ShowSelectedItemsInProjectProc (SELF.FProject, SELF.FLayer, AQuery.Fields[0].AsInteger, true, true);
            finally
               AQuery.Close;
            end;
         end;
         ADataSet.Next;
         Gauge.Progress := Gauge.Progress + 1;
         Application.ProcessMessages;
      end; // for iI end;
   finally
      HideStatus;
      if AQuery <> nil then
      begin
         if AQuery.Active then AQuery.Close;
         AQuery.Free;
      end;
      GoToPrevPosition (iMark);
      ADataSet.EnableControls;
   end;
   Screen.Cursor := crDefault;
end;  }

procedure TIDB_ViewForm.FormDestroy (Sender: TObject);
begin
   SELF.AMenuManager.Free;
   SELF.AToolbarManager.Free;
   ViewDBGrid.RowBitsClear;

   ProgisIDList.Clear;
   ProgisIDList.Free;

   InternalIDList.Clear;
   InternalIDLIst.free;
   LinkIDList.clear;
   LinkIDList.free;
   if ARecordSet.State = adStateOpen then
      ARecordSet.Close;

   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
   FHintWin.Free;

   if not SELF.bIAmQueryResultWindow then
      AnIDBMainMan.AViewForm := nil;

end;

{ This procedure chnages UI. There are 2 possibilities: Read-Only mode and full mode.
  Read-Only mode is required, e.g., if the layer is fixed. In this case the user is
  not allowed to make any changes with attribute information as well as with any
  object that belongs to this layer. }

procedure TIDB_ViewForm.MakeReadOnlyUI (bROMode: Boolean);
begin
   if bCommonReadOnlyMode then bROMode := true;

   SELF.bROMode := bROMode;
   SELF.ViewDBGrid.ReadOnly := bROMode;
   // Buttons
   AToolbarManager.ButtonEnabled['AddRecord'] := not bROMode;
   AToolbarManager.ButtonEnabled['DeleteRecord'] := not bROMode;
   AToolbarManager.ButtonEnabled['EditRecord'] := not bROMode;
   AToolbarManager.ButtonEnabled['SaveChanges'] := false; // not bROMode;
   AToolbarManager.ButtonEnabled['CancelChanges'] := false; // not bROMode;
   AToolbarManager.ButtonVisible['Monitoring'] := @RunMonitoringProc <> nil;

   // Main menu items
   // 'Project' menu
   AMenuManager.MenuVisible['PutLayerItemsIntoTable'] := not bROMode;
   AMenuManager.MenuVisible['PutSelectedLayerItemsIntoTable'] := not bROMode;
   AMenuManager.MenuVisible['ConcileTableRecordsWithLayerItems'] := not bROMode;
   // 'Table' menu
   AMenuManager.MenuVisible['Records'] := not bROMode; // This menu items also have accelerators keys. They have to be disabled as well as invisible.
   AMenuManager.MenuVisible['UpdateRecords'] := not bROMode;
   AMenuManager.MenuVisible['LoadData'] := not bROMode;
   AMenuManager.MenuVisible['TableStructure'] := not bROMode;
   AMenuManager.MenuVisible['TableProperties'] := false; // not bROMode;
   // 'Edit' menu
   AMenuManager.MenuVisible['EditMenu'] := not bROMode;
   // PopUp menu
   AMenuManager.MenuVisible['Copy'] := not bROMode;
   AMenuManager.MenuVisible['Paste'] := not bROMode;
   AMenuManager.MenuVisible['Cut'] := not bROMode;
   // 'Functions' menu
   AMenuManager.MenuVisible['FunctionsMenu'] := not bCommonReadOnlyMode;
end;
{
procedure TIDB_ViewForm.MakeExtTablesUI (bUseExtTableJoining: Boolean);
begin
   // Commented by Cadmensky
//   AMenuManager.MenuEnabled['TableProperties'] := false; // not bUseExtTableJoining;
// ++ Commented by Cadmensky Version 2.3.6
//   AMenuManager.MenuEnabled['UpdateRecords'] := not bUseExtTableJoining;
// -- Commented by Cadmensky Version 2.3.6
   AMenuManager.MenuEnabled['AddRecord'] := not bUseExtTableJoining;

   AToolbarManager.ButtonEnabled['AddRecord'] := not bUseExtTableJoining;
end;   }

procedure TIDB_ViewForm.FormActivate (Sender: TObject);
var
   ADescMan: TIDB_DescMan;
begin
   { When the user activates the window, check the user interface for
     Read-Only mode of the layer or WinGIS as whole. }

   if not SELF.bIAmQueryResultWindow then
   begin
      MakeReadOnlyUI (LayerIsFixedFunc (FLayer) or bCommonReadOnlyMode);
      //MakeExtTablesUI (ALayerManager.GetLayerExtTablesMan (SELF.FLayer).IAmInUse);
   end;

   ADescMan := SELF.ALayerManager.GetLayerDescMan (SELF.FLayer);
   AMenuManager.MenuVisible['LoadQuery'] := ADescMan.GetQueryNames <> '';

   //AMenuManager.MenuEnabled['ConnectExternalTables'] := not bIAmResultOfExternalTablesJoining;
   //AMenuManager.MenuEnabled['DisconnectExternalTables'] := bIAmResultOfExternalTablesJoining;
   AToolbarManager.ButtonEnabled['Monitoring'] := GetLayerItemsCountFunc (SELF.FProject, nil) > 0;
// ++ Cadmensky IDB Version 2.3.8
   AnIDBMainMan.LastWasAttributeWindow[SELF.AProject] := true;
// -- Cadmensky IDB Version 2.3.8
  self.ShowItemProjectMenu.Enabled:= self.FormStyle<>fsStayOnTop;
  // Wai im Projectmenue unsichtbar schalten
  //self.ExistingItemsInTable1.Visible:=False;
  self.SelectedExistingItemsInProject1.Visible:=False;
  EnableSort;
end;

procedure TIDB_ViewForm.FormDeactivate (Sender: TObject);
begin
   { When the user goes to another window, switch the grid in browse mode
     and save data in the data source. }
   if ViewDataSource.DataSet.State in [dsEdit, dsInsert] then
   begin
      SELF.ViewDataSource.DataSet.Post;
      ViewDBGrid.SwitchToBrowseMode;
   end;
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;

end;

procedure TIDB_ViewForm.TableStructurePopUpClick (Sender: TObject);
var
   bResult: Boolean;
   iT, iL, iW, iH: Integer;
// ++ Cadmensky Version 2.3.7
   iMark: Integer;
// -- Cadmensky Version 2.3.7
begin
// ++ Cadmensky Version 2.3.7
   try
      iMark := SELF.DataQuery.FieldByName (cs_InternalID_FieldName).AsInteger;
   except
      iMark := -1;
   end;
// -- Cadmensky Version 2.3.7

   SELF.ViewDBGrid.Visible := false;
   try
      ALayerManager.DetectFieldsStates (SELF.FLayer, SELF.ViewDBGrid);
      if SELF.WindowState <> wsMaximized then
      begin
         iT := SELF.Top;
         iL := SELF.Left;
         iH := SELF.Height;
         iW := SELF.Width;
         SELF.Hide;
      end
      else
      begin
         iT := -1;
         iL := -1;
         iW := -1;
         iH := -1;
      end;

// ++ Cadmensky Version 2.3.2
//      SELF.ClosePowerBroker;
// -- Cadmensky Version 2.3.2

      bResult := not ALayerManager.ShowTableStructure (SELF.FLayer, {TDADODataSet (SELF.ViewDataSource.DataSet),} iT, iL, iW, iH, 1);

      if not bResult then
      begin
         try
            SELF.DataQuery.Reopen;
// ++ Cadmensky Version 2.3.2
            Create_Power_Broker (false);
// -- Cadmensky Version 2.3.2
         except
            SELF.DataQuery.Sort := 'PROGISID ASC';
            SELF.CPB_SortField := 'PROGISID ASC';
            //SELF.DataQuery.Reopen;
// ++ Cadmensky Version 2.3.2
            Create_Power_Broker (false);
// -- Cadmensky Version 2.3.2
         end;
// ++ Cadmensky Version 2.3.7
         SELF.GoToPrevPosition (iMark);
// -- Cadmensky Version 2.3.7
      end;
// ++ Commented by Cadmensky Version 2.3.2
//      Create_Power_Broker (false);
// -- Commented by Cadmensky Version 2.3.2
   finally
      if (SELF.WindowState <> wsMaximized) then
      begin
         if (iT <> -1) and
            (iL <> -1) and
            (iH <> -1) and
            (iW <> -1) then
         begin
            SELF.Top := iT;
            SELF.Left := iL;
            SELF.Height := iH;
            SELF.Width := iW;
         end;
         SELF.Show;
      end;

      if (SELF.WindowState <> wsMaximized) then
      // Hide all internal fields that musn't be shown to an user. do not restore fields visibility and visible order.
         ALayerManager.RestoreFieldsStates (SELF.FLayer, SELF.ViewDBGrid, bResult, bResult);
// ++ Commented by Cadmensky Version 2.3.2
//      SELF.ShowRecordsForSelectedObjects;
// -- Commented by Cadmensky Version 2.3.2
      SELF.ViewDBGrid.Visible := true;
      SELF.ViewDBGrid.SetFocus;

// ++ Commented by Cadmensky Version 2.3.2
//      ViewDBGrid.Perform (WM_VSCROLL, SB_LINEUP, 0);
// -- Commented by Cadmensky Version 2.3.2
//      SELF.ViewDBGrid.Invalidate;

      ALayerManager.DetectFieldsStates (SELF.FLayer, SELF.ViewDBGrid);
      if not bResult then
         begin
         SaveFieldVisibilityChanges;
         ALayerManager.StoreParametersOfViewForm (SELF.FLayer, SELF);
         end;
   end;

end;

procedure TIDB_ViewForm.MakeRecordsStatus;
var
iRecordCount,
iCurrentRecordNumber,
iSelectedRecordsCount: Integer;
begin
   iRecordCount := ViewDataSource.DataSet.RecordCount;
   iCurrentRecordNumber := TDADODataSet (ViewDataSource.DataSet).RecNo;
   {brovak}
   iSelectedRecordsCount := ViewDBGrid.SelectedCount;
   {brovak}
   if iRecordCount > 0 then
      // 281=Record %d from %d (%d)
      SELF.StatusLabel.Caption := Format (sRecordsStatusDummy, [iCurrentRecordNumber, iRecordCount, iSelectedRecordsCount])
   else
      // 305=No records in the table
      SELF.StatusLabel.Caption := ALangManager.GetValue (305);
   AToolbarManager.ButtonEnabled['Monitoring'] := GetLayerItemsCountFunc (SELF.FProject, nil) > 0;
   AToolbarManager.ButtonEnabled['ShowSelectedItemsWithZoom'] := ViewDBGrid.SelectedCount > 0;
   AToolbarManager.ButtonEnabled['ShowSelectedRecords'] := (ViewDBGrid.SelectedCount > 0) or (AToolbarManager.ButtonDown['ShowSelectedRecords']=True) ;
   ShowSelectedOnlyMenue.Enabled:= AToolbarManager.ButtonEnabled['ShowSelectedRecords'];
end;

procedure TIDB_ViewForm.ViewDTableAfterScroll (DataSet: TDataSet);
begin
   if bIAmWorking then EXIT;
   SELF.MakeRecordsStatus;
end;

procedure TIDB_ViewForm.ViewDBGridKeyUp (Sender: TObject; var Key: Word; Shift: TShiftState);
begin
   MakeRecordsStatus;
end;

{ This procedure fills 'Window' menu with captions of existed windows of WinGIS. }

procedure TIDB_ViewForm.FillWindowMenu;
var
   iI: Integer;
   WindowsMenuItem,
      MI: TMenuItem;
   AForm: TForm;
begin
   WindowsMenuItem := AMenuManager.MainMenuItem['WindowsMenu'];
   if WindowsMenuItem = nil then
      EXIT;
   // Delete all menu of existed windows.
   while WindowsMenuItem.Count > 4 do
      WindowsMenuItem.Delete (4);
   // Create new menu for existed windows.
   for iI := 0 to AWinGISMainForm.MDIChildCount - 1 do
   begin
      AForm := AWinGISMainForm.MDIChildren[iI];
      MI := TMenuItem.Create (SELF);
      MI.Caption := IntToStr (iI + 1) + ' ' + AForm.Caption;
      if AForm = SELF then MI.Checked := true;
      MI.Tag := Integer (AForm); // Pointer to the window will be required to show
      // this window when the user clicks on the menu item
      // with name of this window in 'Window' menu.
      MI.OnClick := SELF.OnWindowMenuItemClick;
      WindowsMenuItem.Add (MI);
   end;
end;

procedure TIDB_ViewForm.T2Click (Sender: TObject);
begin
   AWinGISMainForm.Tile;
end;

procedure TIDB_ViewForm.C2Click (Sender: TObject);
begin
   AWinGISMainForm.Cascade;
end;

{ This procedure shows the window from 'Window' menu. The pointer to the window
  is storaged in TAG property of menu item. }

procedure TIDB_ViewForm.OnWindowMenuItemClick (Sender: TObject);
begin
   TForm (TMenuItem (SENDER).Tag).Show;
end;

procedure TIDB_ViewForm.FormKeyPress (Sender: TObject; var Key: Char);
begin
   if (Key = #27) and bIAmWorking then
      // 306=do You want to cancel this operation?
      if MessageBox (Handle, PChar (ALangManager.GetValue (306)), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrYes then
      begin
         bTheUserWantsToCancel := true; // ESC key has been pressed and the user has agreed to cancel an operation.
         Application.ProcessMessages;
      end;
end;

procedure TIDB_ViewForm.Create_Disable_FilterSBClick (Sender: TObject);

// ++ Cadmensky Version 2.3.8

   procedure MakeUIForFilterSettingsDialog;
   begin
      SELF.AToolbarManager.ButtonEnabled['Monitoring'] := false;
      SELF.AToolbarManager.ButtonEnabled['TableProperties'] := false;
      SELF.AToolbarManager.ButtonEnabled['FindRecords'] := false;
      SELF.AToolbarManager.ButtonEnabled['FindNextRecords'] := false;
      SELF.AToolbarManager.ButtonEnabled['RunQueryBuilder'] := false;
      SELF.AToolbarManager.ButtonEnabled['SortAsc'] := false;
      SELF.AToolbarManager.ButtonEnabled['SortDesc'] := false;
      SELF.AToolbarManager.ButtonEnabled['DeleteRecord'] := false;
      SELF.AToolbarManager.ButtonEnabled['ShowSelectedItemsWithZoom'] := false;
      SELF.AToolbarManager.ButtonEnabled['AutoAddToSelected'] := false;
      SELF.AToolbarManager.ButtonEnabled['Links'] := false;
      SELF.AToolbarManager.ButtonEnabled['ShowSelectedRecords'] := false;
      ShowSelectedOnlyMenue.Enabled:= AToolbarManager.ButtonEnabled['ShowSelectedRecords'];
      SELF.AMenuManager.MakeMenuDisabled;
      self.QueryMenu.Enabled:=True;
   end;
// -- Cadmensky Version 2.3.8

var
   i, iMark: integer;
begin
   if SELF.StringGrid1.Visible then
   begin
      SELF.AppBtnClick (Sender);
// ++ Cadmensky Version 2.3.8
      SELF.RestoreUIAfterFilterSettingsDialog;
// -- Cadmensky Version 2.3.8
      exit;
   end;

   if SELF.bIAmUnderFilter = false then
   begin

      SELF.bIAmUnderFilter := true;

      MakeUIForFilterSettingsDialog;

      ACol := -1;
      ARow := -1;
      StringGrid1.DefaultRowHeight := ViewDBGrid.GridRowDefaultHeight;
      {�������� combobox}
      ViewDBGrid.Visible := false;
      Combobox1.Visible := false;
      StringGrid1.Visible := true;
      StringGrid1.ColCount := ViewDBGrid.Columns.Count + 1;
      FilterPanel.Visible := true;
      FilterField := ViewDBGrid.Columns[0].FieldName;

      StringGrid1.ColWidths[0] := 30;
      for i := 1 to ViewDBGrid.Columns.Count do
      begin
         StringGrid1.ColWidths[i] := ViewDBGrid.Columns.Items[i - 1].Width;
         StringGrid1.Cells[i, 0] := ViewDBGrid.Columns.Items[i - 1].FieldName;
      end;
      AToolbarManager.ButtonHint['Filter'] := ALangManager.GetValue (464);
      self.Filtermenue.Caption:=Create_Disable_FilterSB.Hint;
   end

   else //return
   begin
{      if not AnIDBMainMan.UseNewATTNames (SELF.FProject) then
         sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (SELF.FLayer))
      else
         sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (SELF.FProject, SELF.FLayer);}
// ++ Cadmensky
      try
         iMark := ViewDataSource.DataSet.FieldByName (cs_InternalID_FieldName).AsInteger;
      except
         iMark := -1;
      end;
      SELF.ALayerManager.DetectFieldsStates (SELF.FLayer, SELF.ViewDBGrid);
      SELF.DataQuery.Close;
      SELF.ClosePowerBroker;

      {if bIAmResultOfExternalTablesJoining then
         SELF.DataQuery.SQL.Text := SELF.ALayerManager.GetLayerExtTablesMan (SELF.FLayer).JoiningSQL
      else }
         SELF.DataQuery.SQL.Text := 'SELECT * FROM [' + sAttTableName + '] ' +
            'WHERE (' + cs_ShowAllWithoutDeletedObjectsFilter + ')';
      SELF.DataQuery.Open;
//      SELF.ApplyFieldVisibilityChanges;
      SELF.GoToPrevPosition (iMark);

      CPB_SortField := '[' + sAttTableName + '].[PROGISID] ASC';
      Create_Power_Broker;
// ++ Cadmensky Version 2.3.6
      ShowRecordsForSelectedObjects;
// -- Cadmensky Version 2.3.6

      SELF.bIAmUnderFilter := false;

      AToolbarManager.ButtonEnabled['AddRecord'] := true;
      AMenuManager.MenuEnabled['AddRecord'] := true;
      AToolbarManager.ButtonDown['Filter'] := false;
      // 321=Create and apply filter
      AToolbarManager.ButtonHint['Filter'] := ALangManager.GetValue (321);
      self.Filtermenue.Caption:=Create_Disable_FilterSB.Hint;
      Combobox1.Visible := false;
      StringGrid1.Visible := false;
      FilterPanel.Visible := false;
      ViewDBGrid.Visible := true;

// ++ Commented by Cadmensky Version 2.3.6
{      for i := 0 to StringGrid1.ColCount - 2 do
         ViewDBGrid.Columns.Items[i].Width := StringGrid1.ColWidths[i + 1];}
// -- Commented by Cadmensky Version 2.3.6

      SELF.SetMyCaption;
   end;
end;

// ++ Cadmensky Version 2.3.8

procedure TIDB_ViewForm.RestoreUIAfterFilterSettingsDialog;
begin
   SELF.AToolbarManager.ButtonEnabled['Monitoring'] := GetLayerItemsCountFunc (SELF.FProject, nil) > 0;
   SELF.AToolbarManager.ButtonEnabled['TableProperties'] := true;
   SELF.AToolbarManager.ButtonEnabled['FindRecords'] := true;
   SELF.AToolbarManager.ButtonEnabled['FindNextRecords'] := false;
   SELF.AToolbarManager.ButtonEnabled['RunQueryBuilder'] := true;
   SELF.AToolbarManager.ButtonEnabled['SortAsc'] := true;
   SELF.AToolbarManager.ButtonEnabled['SortDesc'] := true;
   SELF.AToolbarManager.ButtonEnabled['DeleteRecord'] := true;
   SELF.AToolbarManager.ButtonEnabled['ShowSelectedItemsWithZoom'] := SELF.ViewDBGrid.SelectedCount > 0;
   SELF.AToolbarManager.ButtonEnabled['AutoAddToSelected'] := true;
   SELF.AToolbarManager.ButtonEnabled['Links'] := true;
   SELF.AToolbarManager.ButtonEnabled['ShowSelectedRecords'] := SELF.ViewDBGrid.SelectedCount > 0;
   ShowSelectedOnlyMenue.Enabled:= AToolbarManager.ButtonEnabled['ShowSelectedRecords'];
   SELF.AMenuManager.MakeMenuEnabled;
end;
// -- Cadmensky Version 2.3.8

procedure TIDB_ViewForm.CutSBMouseMove (Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
   HintString: Ansistring;
   NeedRect: TRect;
   ClickPoint: TPoint;
   OldButton: TSpeedButton;
begin
   {brovak}
   if ACurrentSpeedButtonUnderMouse <> TSpeedButton (SENDER) then
   begin
      FHintX := X;
      FHintY := Y;
      if Assigned (FHintWin) then FHintWin.ReleaseHandle;
   end;
   {brovak}

   if not (SENDER is TSpeedButton) then
   begin
      if Assigned (FHintWin) then FHintWin.ReleaseHandle;
      EXIT;
   end;
   if (ACurrentSpeedButtonUnderMouse <> nil) and (ACurrentSpeedButtonUnderMouse <> SENDER) then
      ACurrentSpeedButtonUnderMouse.Perform (CM_MOUSELEAVE, 0, 0);
   OldButton := ACurrentSpeedButtonUnderMouse;
   ACurrentSpeedButtonUnderMouse := TSpeedButton (SENDER);
   {brovak}
   HintString := TSpeedButton (ACurrentSpeedButtonUnderMouse).Hint;
   if HintString <> '' then
   begin
      NeedRect := FHintWin.CalcHintRect (100, HintString, nil);
      GetCursorPos (ClickPoint);
      NeedRect.Left := ClickPoint.X + ACurrentSpeedButtonUnderMouse.Width div 3;
      NeedRect.Top := ClickPoint.Y + 3 * ACurrentSpeedButtonUnderMouse.Height div 4;
      NeedRect.Right := NeedRect.Left + Canvas.TextWidth (HintString) + 14;
      NeedRect.Bottom := NeedRect.Top + Canvas.TextHeight (HintString) + 4;
      FHintWin.Color := clInfoBk;
      FHintWin.ActivateHint (NeedRect, HintString);

   end
   else
      if Assigned (FHintWin) then FHintWin.ReleaseHandle;
   {brovak}
end;

procedure TIDB_ViewForm.IDB_Toolbar1MouseMove (Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
   ACurrentSpeedButtonUnderMouse.Perform (CM_MOUSELEAVE, 0, 0);
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;

end;

procedure TIDB_ViewForm.RegisterToolbarButtons;
//var    ANewToolBar: TIDB_Toolbar;
begin
   // Firts step.
        // Add all required buttons to default toolbar in order it can reposition all this buttons on some rows.
        // Besides, these arrangement will be used as default.
   IDB_Toolbar1.AddButton (MonitoringSB);
   IDB_Toolbar1.AddButton (TablePropertiesSB);

   IDB_Toolbar1.AddButton (tbSeparator);

   IDB_Toolbar1.AddButton (ShowSelItemsWithZoomSB);
   IDB_Toolbar1.AddButton (AutoAddToSelectedSB);
   IDB_Toolbar1.AddButton (ZoomOrSelect);
      IDB_Toolbar1.AddButton (tbSeparator);
   IDB_Toolbar1.AddButton (LinksSB);
   IDB_Toolbar1.AddButton (ShowSelectedRecordsSB);

   IDB_Toolbar1.AddButton (tbSeparator);
   IDB_Toolbar1.AddButton (CutSB);
   IDB_Toolbar1.AddButton (CopySB);
   IDB_Toolbar1.AddButton (PasteSB);
   IDB_Toolbar1.AddButton (tbSeparator);

   IDB_Toolbar1.AddButton (SortAscSB);
   IDB_Toolbar1.AddButton (SortDescSB);

   IDB_Toolbar1.AddButton (tbSeparator);

   IDB_Toolbar1.AddButton (FindItSB);
   IDB_Toolbar1.AddButton (FindNextSB);

   IDB_Toolbar1.AddButton (tbSeparator);

   IDB_Toolbar1.AddButton (QueryBuilerSB);
   IDB_Toolbar1.AddButton (Create_Disable_FilterSB);


   IDB_Toolbar1.AddButton (tbSeparator);

   IDB_Toolbar1.AddButton (DeleteSB);
   IDB_Toolbar1.AddButton(ShowSelectedLinkID);


   // Second step
        // Register all buttons in the toolbar manager.
   AToolbarManager.RegisterButton ('Monitoring', MonitoringSB);
   AToolbarManager.RegisterButton ('TableProperties', TablePropertiesSB);
   AToolbarManager.RegisterButton ('Cut', CutSB);
   AToolbarManager.RegisterButton ('Copy', CopySB);
   AToolbarManager.RegisterButton ('Paste', PasteSB);
   AToolbarManager.RegisterButton ('FindRecords', FindItSB);
   AToolbarManager.RegisterButton ('FindNextRecords', FindNextSB);
   AToolbarManager.RegisterButton ('RunQueryBuilder', QueryBuilerSB);
   AToolbarManager.RegisterButton ('Filter', Create_Disable_FilterSB);
   AToolbarManager.RegisterButton ('SortAsc', SortAscSB);
   AToolbarManager.RegisterButton ('SortDesc', SortDescSB);
   AToolbarManager.RegisterButton ('DeleteRecord', DeleteSB);
   AToolbarManager.RegisterButton ('ShowSelectedItemsWithZoom', ShowSelItemsWithZoomSB);
   AToolbarManager.RegisterButton ('AutoAddToSelected', AutoAddToSelectedSB);
   AToolbarManager.RegisterButton ('Links', LinksSB);
   AToolbarManager.RegisterButton ('ShowSelectedRecords', ShowSelectedRecordsSB);
   AToolbarManager.RegisterButton ('ShowSelectedLinkID', ShowSelectedLinkID);
   // Third step
        // Try to create a new toolbar basing on data from configuration file.
        // if new toolbar won't be created, the default toolbar will be used.

// ++ Commented by Cadmensky Version 2.3.8
{   ANewToolBar := AToolbarManager.Make (sToolbarIniFileName, IDB_Toolbar1);
   if ANewToolBar <> nil then
   begin
      SELF.AToolbar := ANewToolBar;
      IDB_Toolbar1.Free;
   end
   else}
// -- Commented by Cadmensky Version 2.3.8
   SELF.AToolbar := IDB_Toolbar1;
end;

procedure TIDB_ViewForm.RegisterMenuItems;
{var
   ANewMenu: TMainMenu;
   ANewPopupMenu: TPopupMenu;}
begin
   // Parameters:
   //    1. Name of function. It must be equal the name you want to link in menu ini file
   //    2. Main menu item that corresponds to the function from parameter 1.
   //    3. Popup menu item that corresponds to the function from parameter 1.

   // Register main menu items. They can have corresponded popup menu item as well they can not have it.
   AMenuManager.RegisterItem ('ProjectMenu', Project1, nil);
   AMenuManager.RegisterItem ('ShowProject', ShowItemProjectMenu, nil);
   AMenuManager.RegisterItem ('ShowSelectedItems', ShowSelectItemsInProjectMenu, ShowSelectedPopup);
   AMenuManager.RegisterItem ('ShowSelectedItemsWithZoom', ShowSelectedItemsWithZoom1, ShowSelectedWithZoomPopup);
   AMenuManager.RegisterItem ('PutLayerItemsIntoTable', AllExistingItemsInProject1, nil);
   AMenuManager.RegisterItem ('PutSelectedLayerItemsIntoTable', SelectedExistingItemsInProject1, nil);
   AMenuManager.RegisterItem ('ConcileTableRecordsWithLayerItems', ExistingItemsInTable1, nil);
// ++ Commented by Cadmensky Version 2.3.6
//   AMenuManager.RegisterItem ('ShowAllQueriedItems', ShowAllQueriedItemsMenuItem, nil);
// -- Commented by Cadmensky Version 2.3.6
   AMenuManager.RegisterItem ('VertAlign', AlignVertical, nil);
   AMenuManager.RegisterItem ('HorzAlign', AlignHorizontal, nil);

   AMenuManager.RegisterItem ('EditMenu', Edit1, nil);
   AMenuManager.RegisterItem ('Copy', Copy1, nil);
   AMenuManager.RegisterItem ('Cut', Cut1, nil);
   AMenuManager.RegisterItem ('Paste', Paste1, nil);
   AMenuManager.RegisterItem ('SelectAll', SelectAll1, nil);
   AMenuManager.RegisterItem ('DeselectAll', DeselectAll1, nil);
   AMenuManager.RegisterItem ('InvertSelection', InvertSelection1, nil);
   AMenuManager.RegisterItem ('FindRecords', FindItemTableMenu, nil);
   AMenuManager.RegisterItem ('FindNextRecords', FindNext1, nil);
   AMenuManager.RegisterItem ('Replace', Replace1, nil);

   AMenuManager.RegisterItem ('FunctionsMenu', Functions1, nil);

   AMenuManager.RegisterItem ('MakeAnnotations', AnnotationMenuItem, nil);
   AMenuManager.RegisterItem ('MakeAnnotationsForAllRecords', AllRecAnnot1, nil);
   AMenuManager.RegisterItem ('MakeAnnotationsForSelectedRecords', SelRecAnnot1, SelRecAnnotPopUp);

   AMenuManager.RegisterItem ('MakeCharts', DBCharts1, nil);
   AMenuManager.RegisterItem ('MakeChartsForAllRecords', AllRecChart1, nil);
   AMenuManager.RegisterItem ('MakeChartsForSelectedRecords', SelRecChart1, SelRecChartPopUp);

   AMenuManager.RegisterItem ('MakeThematicMap', ThematicMap1, nil);
   AMenuManager.RegisterItem ('MakeTMForAllRecords', AllRecTM1, nil);
   AMenuManager.RegisterItem ('MakeTMForSelectedRecords', SelRecTM1, SelRecTMPopUp);

   AMenuManager.RegisterItem ('TableMenu', TableMenu, nil);
   AMenuManager.RegisterItem ('Records', Record1, nil);
   AMenuManager.RegisterItem ('AddRecord', AddRecordItemTableMenu, nil);
   AMenuManager.RegisterItem ('DeleteRecord', DeleteRecordItemTableMenu, nil);
   AMenuManager.RegisterItem ('Navigate', Navigate1, nil);
   AMenuManager.RegisterItem ('FirstRecord', FirstRecordItemTableMenu, nil);
   AMenuManager.RegisterItem ('PrevRecord', PrevRecordItemTableMenu, nil);
   AMenuManager.RegisterItem ('NextRecord', NextRecordItemTableMenu, nil);
   AMenuManager.RegisterItem ('LastRecord', LastRecordItemTableMenu, nil);
   AMenuManager.RegisterItem ('RefreshData', RefreshItemTableMenu, nil);
   AMenuManager.RegisterItem ('UpdateRecords', UpdateRecords1, nil);
   AMenuManager.RegisterItem ('SaveResult', SaveResult1, nil);
   AMenuManager.RegisterItem ('LoadData', LoadData1, nil);
   AMenuManager.RegisterItem ('TableProperties', Ahaa1, nil);
   AMenuManager.RegisterItem ('TableStructure', StructureTable1, nil);
   AMenuManager.RegisterItem ('SaveQuery', SaveQuery, nil);
   AMenuManager.RegisterItem ('EditQuery1', EditQuery1, nil);
   AMenuManager.RegisterItem ('QueryBuilderMenu', QueryMenu, nil);
   AMenuManager.RegisterItem ('RunQueryBuilder', RunQueryBuilder1, nil);
   AMenuManager.RegisterItem ('LoadQuery', LoadQuery, nil);

   // Register popup menu items that don't have corresponded main menu items.
   AMenuManager.RegisterItem ('SortAsc', nil, SortAscPopUp);
   AMenuManager.RegisterItem ('SortDesc', nil, SortDescPopUp);
   AMenuManager.RegisterItem ('SetFieldInvisible', nil, MakeFieldAsInvisiblePopUp);
   AMenuManager.RegisterItem ('UnhideColumn', nil, UnhideColumnPopup);

   // Temporary commented by Cadmensky
   AMenuManager.RegisterItem ('StayOnTop', nil, MDI_SDI_SwitchPopUp);
// ++ Commented by Cadmensky Version 2.3.6
//   AMenuManager.RegisterItem ('ResizeGridCell', nil, ResizeGridCellPopUp);
// -- Commented by Cadmensky Version 2.3.6

{$IFDEF EXT_TAB_POOL}
   //AMenuManager.RegisterItem ('ExternalTablesParams', nil, ExtTablesParamsPopUp);
   //AMenuManager.RegisterItem ('ConnectExternalTables', nil, ExtTablesConnectPopUp);
   //AMenuManager.RegisterItem ('DisconnectExternalTables', nil, ExtTablesDisconnectPopUp);
{$ENDIF}

   // Register 'Windows' menu. It always will appear as the rightest menu of main menu.
   AMenuManager.RegisterItem ('WindowsMenu', W1, nil);
   AMenuManager.RegisterItem ('CascadeWindows', C2, nil);
   AMenuManager.RegisterItem ('TileWindows', T2, nil);
   AmenuManager.RegisterItem('Help',Help,nil);
   AmenuManager.RegisterItem('Helpmenue',Helpmenue,nil);
   AMenuManager.RegisterItem( 'Infomenue',Infomenue,nil);
   AMenuManager.RegisterItem( 'Exitmenue',Exitmenue,nil);
   AMenuManager.RegisterItem( 'LinkRecordwithObject',LinkRecordwithObject,nil);

// ++ Commented by Cadmensky Version 2.3.8
{   if AMenuManager.Make (sMenuIniFileName, RealMainMenu, RealPopupMenu) then
   begin
      SELF.Menu := RealMainMenu;
      ViewDBGrid.PopupMenu := RealPopupMenu;
      RealPopupMenu.OnPopup := PopupMenu1.OnPopup;
      // Delete default main and popup menus.
      MainMenu1.Free;
      PopupMenu1.Free;
   end;}
// -- Commented by Cadmensky Version 2.3.8

end;

procedure TIDB_ViewForm.W1Click (Sender: TObject);
begin
   FillWindowMenu;
end;

function TIDB_ViewForm.ThematicMapDllExists: Boolean;
var
   sThematicMapDllFileName: AnsiString;
begin
   sThematicMapDllFileName := ExtractFilePath (Application.ExeName) + 'Thematic.dll';

   RESULT := FileExists (sThematicMapDllFileName);
end;

procedure TIDB_ViewForm.DataQueryAfterOpen (DataSet: TDataSet);
var
   iI: Integer;
   AField: TField;
begin
   for iI := 0 to ViewDataSource.DataSet.FieldCount - 1 do
   begin
      AField := ViewDataSource.DataSet.Fields[iI];
      if FieldIsInternalUsageField (AField.FieldName) then
         AField.Visible := false;

      if (AnsiUpperCase (AField.FieldName) = 'PROGISID') or (AnsiUpperCase (AField.FieldName) = 'GUID') or FieldIsCalculatedField (AField.FieldName) then
         AField.ReadOnly := true;
   end;
   {brovak}
   try
      SELF.ApplyFieldVisibilityChanges;
   except
   end;
   SELF.MakeRecordsStatus;
   {brovak}

   DataQuery.Refresh;
end;

procedure TIDB_ViewForm.MonitoringSBClick (Sender: TObject);
//var
//   bFound: Boolean;
//   iI: Integer;
//   AForm: TForm;
begin
   {brovak}
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
   {brovak}
   if @RunMonitoringProc <> nil then
   begin
      if GetLayerItemsCountFunc (SELF.FProject, nil) = 0 then
         exit;
      AToolbarManager.ButtonEnabled['Monitoring'] := false;
      RunMonitoringProc (SELF.FProject, false);
      AnIDBMainMan.ShowMonitoringWindow (SELF.FProject, false);
      SELF.Close;

   end;
end;

///////////////////  EXTERNAL TABLES POOL CONNECTION / DISCONNECTION  //////////
{
procedure TIDB_ViewForm.ExtTablesParamsPopUpClick (Sender: TObject);
var
   //PETF: TIDB_ParamsOfExternalTablesForm;
   //AnExtTablesMan: TIDB_ExternalTablesMan;
   bResult: Boolean;
begin
   //brovak
   if Assigned (FHintWin) then
      FHintWin.ReleaseHandle;
   //brovak
   SELF.ALayerManager.DetectFieldsStates (SELF.FLayer, SELF.ViewDBGrid);
   try
      //PETF := TIDB_ParamsOfExternalTablesForm.Create (SELF, SELF.FProject, SELF.ALayerManager, GetLayerIndexFunc (SELF.FLayer));
      //PETF.SetCurrentLayer (SELF.FLayer);

      //AnExtTablesMan := SELF.ALayerManager.GetLayerExtTablesMan (SELF.FLayer);

      bResult := PETF.ShowModal = mrOK;
// ++ Cadmensky Version 2.3.8
      if (not AnExtTablesMan.MakeExtTablePool (bResult)) or
//      if (not AnExtTablesMan.MakeExtTablePool (SELF.Gauge)) or
      (AnExtTablesMan.GetUsedLinksCount < 1) then
         MakeExtTablesDisconnect
      else
// -- Cadmensky Version 2.3.8
         if bResult then
         begin
            AnExtTablesMan.SetParamsToExtTablePool;
            MakeExtTablesLinking (false);
            // for layer of which external parameters has been changed.
            AMenuManager.MenuEnabled['ConnectExternalTables'] := false;
            AMenuManager.MenuEnabled['DisconnectExternalTables'] := true;
// ++ DEBUG
            ShowRecordsForSelectedObjects;
// -- DEBUG
         end;
   finally
      PETF.Free;
   end;
// ++ DEBUG
//   SELF.ApplyFieldVisibilityChanges;
//   ShowRecordsForSelectedObjects;
// -- DEBUG
end;    }

{ This parameter is used when I show attribute window. This procedure makes external tables pool
  and before wants to detect all fields state. But if the window is being created, the field states
  don't exists. So we need to use previous state information instead. }
{
function TIDB_ViewForm.MakeExtTablesLinking (bDetectFieldStatesBefore: Boolean = true): Boolean;
var
   iMark: Integer;
//   sAttTableName: AnsiString;
//   AnExtTableMan: TIDB_ExternalTablesMan;
begin
   RESULT := false;
   try
      if ViewDataSource.DataSet.Active then
         iMark := ViewDataSource.DataSet.FieldByName (cs_InternalID_FieldName).AsInteger
      else
         iMark := -1;
   except
      iMark := -1;
   end;

   // Detect the current row and field states in case if the external linking will fail.
   if bDetectFieldStatesBefore then
      SELF.ALayerManager.DetectFieldsStates (SELF.FLayer, SELF.ViewDBGrid);

   AnExtTableMan := ALayerManager.GetLayerExtTablesMan (SELF.FLayer);

// ++ Commented by Cadmensky Version 2.3.8
//   SELF.ShowStatus;
   // 364=Joining...
 //  StatusLabel.Caption := ALangManager.GetValue (364);
//   Application.ProcessMessages;
// -- Commented by Cadmensky Version 2.3.8
   try
      SELF.ClosePowerBroker;

      // Create the external tables pool and set connection parameters of TDQuery.
      if AnExtTableMan.JoiningSQL = '' then
         AnExtTableMan.SetParamsToExtTablePool;

      SELF.DataQuery.Close;
      SELF.DataQuery.SQL.Text := AnExtTableMan.JoiningSQL;
         // Try to open the pool.
      try
         SELF.DataQuery.Open;
         SELF.bIAmResultOfExternalTablesJoining := true;
         RESULT := true;
         MakeExtTablesUI (true);
      except
         // 362=Result from external tables cannot be obtained (layer '%s')
         MessageBox (SELF.Handle, PChar (Format (ALangManager.GetValue (362), [GetLayerNameFunc (SELF.FLayer)])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
         RESULT := false;
      end;
      // if the attempt of opening has failed, show only layer attribute table.
      if not RESULT then
      begin
         SELF.bIAmResultOfExternalTablesJoining := false;
         // Restore previous result if the linking has failed.

         SELF.DataQuery.Close;
// ++ Commented by Cadmensky Version 2.3.8
//         SELF.DataQuery.Master := ALayerManager.DataAccessObject.DMaster;
// -- Commented by Cadmensky Version 2.3.8
         SELF.DataQuery.SQL.Text := 'SELECT * FROM [' + SELF.sAttTableName + '] ' +
            'WHERE (' + cs_ShowAllWithoutDeletedObjectsFilter + ')';
         try
            SELF.DataQuery.Open;
         except
         end;

// ++ Cadmensky Version 2.3.9
         try SELF.MakeExtTablesDisconnect;
         except end;
// -- Cadmensky Version 2.3.9
         SELF.GoToPrevPosition (iMark);
         MakeExtTablesUI (false);
      end;
   finally
      SELF.Create_Power_Broker;
      SELF.ALayerManager.RestoreFieldsStates (FLayer, ViewDBGrid, true); // Cadmensky
// ++ Commented by Cadmensky Version 2.3.8
//      SELF.HideStatus;
// -- Commented by Cadmensky Version 2.3.8
      SELF.MakeRecordsStatus;
   end;
end;   }
{
procedure TIDB_ViewForm.MakeExtTablesDisconnect;
var
   iMark: Integer;
   //AnExtTableMan: TIDB_ExternalTablesMan;
begin
   // Detect the current row and field states in case if the external linking will fail.
   try
      if ViewDataSource.DataSet.Active then
         iMark := ViewDataSource.DataSet.FieldByName (cs_InternalID_FieldName).AsInteger
      else
         iMark := -1;
   except
      iMark := -1;
   end;

   SELF.ALayerManager.DetectFieldsStates (SELF.FLayer, SELF.ViewDBGrid);
   AnExtTableMan := ALayerManager.GetLayerExtTablesMan (SELF.FLayer);

   SELF.ShowStatus;
   // 370=Disconnection...
   StatusLabel.Caption := ALangManager.GetValue (370);
   Application.ProcessMessages;

   AnExtTableMan.Disconnect;

   SELF.bIAmResultOfExternalTablesJoining := false;

   SELF.DataQuery.Close;
   SELF.DataQuery.Master := ALayerManager.DataAccessObject.DMaster;
   SELF.DataQuery.SQL.Text := 'SELECT *' + //AnUserFieldsMan.GetUserCalcFieldsSQLString +
      ' FROM [' + SELF.sAttTableName + '] ' +
      'WHERE ' + cs_ShowAllWithoutDeletedObjectsFilter;
   try
      SELF.DataQuery.Open;
      SELF.ALayerManager.RestoreFieldsStates (SELF.FLayer, SELF.ViewDBGrid);
      SELF.GoToPrevPosition (iMark);
   finally
      SELF.HideStatus;
      SELF.MakeRecordsStatus;
   end;

   MakeExtTablesUI (false);
end;   }
{
procedure TIDB_ViewForm.ExtTablesConnectPopUpClick (Sender: TObject);
var
   //AnExtTablesMan: TIDB_ExternalTablesMan;
begin
   AnExtTablesMan := SELF.ALayerManager.GetLayerExtTablesMan (SELF.FLayer);
   if AnExtTablesMan.GetUsedLinksCount < 1 then
      ExtTablesParamsPopUpClick (SENDER)
   else
      // 368=do you want to make connection to external tables for layer '%s'?
      if MessageBox (Handle, PChar (Format (ALangManager.GetValue (368), [GetLayerNameFunc (SELF.FLayer)])), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrYes then
         if SELF.MakeExtTablesLinking (false) then
         begin
            AMenuManager.MenuEnabled['ConnectExternalTables'] := false;
            AMenuManager.MenuEnabled['DisconnectExternalTables'] := true;
         end;
end;      }
{
procedure TIDB_ViewForm.ExtTablesDisconnectPopUpClick (Sender: TObject);
begin
   // 369=do you want to make disconnect for layer '%s'?
   if MessageBox (Handle, PChar (Format (ALangManager.GetValue (369), [GetLayerNameFunc (SELF.FLayer)])), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrYes then
   begin
      SELF.MakeExtTablesDisconnect;
      AMenuManager.MenuEnabled['ConnectExternalTables'] := true;
      AMenuManager.MenuEnabled['DisconnectExternalTables'] := false;
   end;
end;
}
{procedure TIDB_ViewForm.DataQueryBeforePost (DataSet: TDataSet);
var
   //AnExtTablesMan: TIDB_ExternalTablesMan;
   iI: Integer;
   iProgisIDValue: Integer;
   sFieldName, sFieldData: AnsiString;
   iMark: Integer;
   dtFieldType: TFieldType;
begin
   SELF.ALayerManager.DetectFieldsStates (SELF.FLayer, SELF.ViewDBGrid);
   try
      iProgisIDValue := DataQuery.FieldByName ('PROGISID').AsInteger;
   except
      iProgisIDValue := 0;
   end;
   ViewDBGrid.Enabled := false;
   if iProgisIDValue <> 0 then
   begin
   //   AnExtTablesMan := SELF.ALayerManager.GetLayerExtTablesMan (SELF.FLayer);
   //   AnExtTablesMan.GetReadyToReceiveDataForUpdate;

      for iI := 0 to SELF.DataQuery.Fields.Count - 1 do
      begin
         sFieldName := DataQuery.Fields[iI].FieldName;
         if not FieldIsInternalUsageField (sFieldName) and
            not FieldIsCalculatedField (sFieldName)
            and not (AnsiUpperCase (sFieldName) = 'PROGISID') then
         begin
            sFieldData := DataQuery.Fields[iI].AsString;
            dtFieldType := DataQuery.Fields[iI].DataType;

          //  AnExtTablesMan.ReceiveDataForUpdate (sFieldName, sFieldData, dtFieldType);
         end;
      end; // for iI end

    //  AnExtTablesMan.ExecuteUpdate (iProgisIDValue);
   end;
   DataQuery.Cancel;

   try
      iMark := DataQuery.FieldByName (cs_InternalID_FieldName).AsInteger;
   except
      iMark := -1;
   end;
   DataQuery.ReOpen;
   SELF.GoToPrevPosition (iMark);
   ViewDBGrid.Enabled := true;
end; }

procedure TIDB_ViewForm.ResizeGridCellPopUpClick (Sender: TObject);
var
   Cell: TGridCoord;
   sFieldName: AnsiString;
   iFieldNo: integer;
   sValue: AnsiString;
//   iMark: integer;
   iI: integer;
   iItemID: integer;
   iFieldWidth, iFieldValueWidth: Integer;
begin
   Cell := ViewDBGrid.MouseCoord (ViewDBGrid.GetLastMouseX, ViewDBGrid.GetLastMouseY);

   if AlreadyResized = true then
   begin;
      ViewDBGrid.Invalidate;
      Exit;
   end;
   SetScreenCursor;
   sFieldName := ViewDBGrid.Columns[Cell.X].FieldName;
   iFieldWidth := ViewDBGrid.Canvas.TextWidth (sFieldName);

   ARecordSet.MoveFirst;
   iFieldNo := FieldNoByName (sFieldName);
   if iFieldNo = -1 then
   begin
      for iI := 0 to SELF.ARecordSet.RecordCount - 1 do
      begin
         iFieldNo := FieldNoByName ('PROGISID');
         iItemID := StrToInt (ExtractValue (SELF.ARecordSet.Fields[iFieldNo].Value));
         sValue := GetCalculatedFieldValueByName (SELF.FProject, SELF.FLayer, iItemID, sFieldName);
         iFieldValueWidth := ViewDBGrid.Canvas.TextWidth (sValue);
         if iFieldValueWidth > iFieldWidth then
            iFieldWidth := iFieldValueWidth;
         ARecordSet.MoveNext;
      end;
   end
   else
   begin
      for iI := 0 to SELF.ARecordSet.RecordCount - 1 do
      begin;
         sValue := ExtractValue (SELF.ARecordSet.Fields[iFieldNo].Value);
         iFieldValueWidth := ViewDBGrid.Canvas.TextWidth (sValue);
         if iFieldValueWidth > iFieldWidth then
            iFieldWidth := iFieldValueWidth;
         ARecordSet.MoveNext;
      end;
   end;
   ViewDBGrid.Columns.Items[Cell.x].Width := iFieldWidth + 5;
   ViewDBGrid.Repaint;
   SELF.ALayerManager.DetectFieldsStates (SELF.FLayer, SELF.ViewDBGrid);
   AlreadyResized := true;
   RestoreScreenCursor;
end;

procedure TIDB_ViewForm.DataQueryCalcFields (DataSet: TDataSet);
var
   ASourceField, ADestField: TField;
   iItemID: Integer;
   slCalcFields: TStringList;
   sFieldValue: AnsiString;
begin
   ASourceField := ViewDBGrid.DataSource.DataSet.FindField ('PROGISID');
   if ASourceField <> nil then
   begin
      iItemID := ASourceField.AsInteger;

      slCalcFields := TStringList.Create;
      slCalcFields.Text := SELF.ALayerManager.GetUsedCalcFieldsNames (SELF.FLayer);

      // Area
      if slCalcFields.IndexOf (cs_CalcFieldAreaName) = -1 then
      begin
         sFieldValue := GetObjectPropsEx (SELF.FProject, SELF.FLayer, iItemID, dtArea);
         ADestField := ViewDBGrid.DataSource.DataSet.FindField (cs_CalcFieldAreaName);
         if (ADestField <> nil) and (sFieldValue <> '') then
            ADestField.AsFloat := StrToFloat (ReplaceChars (sFieldValue, '.', DecimalSeparator));
      end;
      // Size
      if slCalcFields.IndexOf (cs_CalcFieldSizeName) = -1 then
      begin
         sFieldValue := GetObjectPropsEx (SELF.FProject, SELF.FLayer, iItemID, dtSize);
         ADestField := ViewDBGrid.DataSource.DataSet.FindField (cs_CalcFieldSizeName);
         if (ADestField <> nil) and (sFieldValue <> '') then
            ADestField.AsFloat := StrToFloat (ReplaceChars (sFieldValue, '.', DecimalSeparator));
      end;
      // X
      // Size
      if slCalcFields.IndexOf (cs_CalcFieldRotationName) = -1 then
      begin
         sFieldValue := GetObjectPropsEx (SELF.FProject, SELF.FLayer, iItemID, dtRotation);
         ADestField := ViewDBGrid.DataSource.DataSet.FindField (cs_CalcFieldRotationName);
         if (ADestField <> nil) and (sFieldValue <> '') then
            ADestField.AsFloat := StrToFloat (ReplaceChars (sFieldValue, '.', DecimalSeparator));
      end;

      if slCalcFields.IndexOf (cs_CalcFieldXName) = -1 then
      begin
         sFieldValue := GetObjectPropsEx (SELF.FProject, SELF.FLayer, iItemID, dtX);
         ADestField := ViewDBGrid.DataSource.DataSet.FindField (cs_CalcFieldXName);
         if (ADestField <> nil) and (sFieldValue <> '') then
            ADestField.AsFloat := StrToFloat (ReplaceChars (sFieldValue, '.', DecimalSeparator));
      end;
      // Y
      if slCalcFields.IndexOf (cs_CalcFieldYName) = -1 then
      begin
         sFieldValue := GetObjectPropsEx (SELF.FProject, SELF.FLayer, iItemID, dtY);
         ADestField := ViewDBGrid.DataSource.DataSet.FindField (cs_CalcFieldYName);
         if (ADestField <> nil) and (sFieldValue <> '') then
            ADestField.AsFloat := StrToFloat (ReplaceChars (sFieldValue, '.', DecimalSeparator));
      end;
      // VerticesCount
      if slCalcFields.IndexOf (cs_CalcFieldVerticesCount) = -1 then
      begin
         ADestField := ViewDBGrid.DataSource.DataSet.FindField (cs_CalcFieldVerticesCount);
         if (ADestField <> nil) and (sFieldValue <> '') then
            ADestField.AsString := GetObjectPropsEx (SELF.FProject, SELF.FLayer, iItemID, dtVerticesCount);
      end;
      // Symbol name
      if slCalcFields.IndexOf (cs_CalcFieldSymbolName) = -1 then
      begin
         ADestField := ViewDBGrid.DataSource.DataSet.FindField (cs_CalcFieldSymbolName);
         if ADestField <> nil then
            ADestField.AsString := GetObjectPropsEx (SELF.FProject, SELF.FLayer, iItemID, dtSymbolName);
      end;
      // Text
      if slCalcFields.IndexOf (cs_CalcFieldTextName) = -1 then
      begin
         ADestField := ViewDBGrid.DataSource.DataSet.FindField (cs_CalcFieldTextName);
         if ADestField <> nil then
            ADestField.AsString := GetObjectPropsEx (SELF.FProject, SELF.FLayer, iItemID, dtText);
      end;
      // LinkID
      if slCalcFields.IndexOf (cs_CalcFieldLinkIDName) = -1 then
      begin
         ADestField := ViewDBGrid.DataSource.DataSet.FindField (cs_CalcFieldLinkIDName);
         if (ADestField <> nil) and (sFieldValue <> '') then
            ADestField.AsString := GetObjectPropsEx (SELF.FProject, SELF.FLayer, iItemID, dtParent);
      end;
      slCalcFields.Free;
   end;
end;

procedure TIDB_ViewForm.DataQueryBeforeOpen (DataSet: TDataSet);
begin
   CreateCalculatedFields;
end;

procedure TIDB_ViewForm.MakeMyWindowState (AValue: TWindowState);
begin
   if SELF.WindowState <> AValue then
      MDI_SDI_SwitchPopUpClick (SELF);
end;

procedure TIDB_ViewForm.CloseSBClick (Sender: TObject);
begin
   SELF.Close;
end;

procedure TIDB_ViewForm.DataQueryAfterPost (DataSet: TDataSet);
begin
//   SELF.ApplyFieldVisibilityChanges;
end;

procedure TIDB_ViewForm.PrepareGauge (AText: AnsiString);
begin
   Gauge.MinValue := 0;
   Gauge.MaxValue := ViewDBGrid.RowBits.Size - 1;
   Gauge.Progress := 0;
   Gauge.Visible := true;
   Statuslabel.Caption := AText;
   Application.ProcessMessages;
end;

procedure TIDB_ViewForm.ViewDBGridCellClick (Column: TColumn);
var
   EndRow, i: integer;
   bCurrentSelected, bLinkID: boolean;
   iMark, iProgisIDFieldValue, iRecNo: integer;
   iInternalIDFieldNo, iProgisIDFieldNo, iLinkIdNo: integer;
begin
   if SELF.DataQuery.RecordCount < 1 then
      exit;
   if GetKeyState (VK_LMenu) < 0 then
   begin;
      ResizeGridCellPopUpClick (nil);
      Exit;
   end;
   if AToolbarManager.ButtonDown['ShowSelectedRecords'] then
      exit;

   SetScreenCursor;
   bLinkID:=False;
   LinkIDList.clear;
   SELF.ViewDBGrid.Enabled := false;
   Application.ProcessMessages;

   iRecNo := SELF.DataQuery.RecNo;
   iInternalIDFieldNo := FieldNoByName (cs_InternalID_FieldName);
   if iInternalIDFieldNo <> -1 then
   begin

      if GetKeyState (VK_Shift) < 0 then
      begin
         if iRecNo - 1 <= StartRow then
         begin
            EndRow := StartRow;
            StartRow := iRecNo - 1;
         end
         else
            EndRow := iRecNo - 1;

// ++ Commented by Cadmensky Version 2.3.2
//         ViewDBGrid.SelectedCount := Abs (EndRow - StartRow) + 1;
// -- Commented by Cadmensky Version 2.3.2
         iLinkIdNo:= FieldNoByName(cs_CalcFieldLinkIDName);
         iProgisIDFieldNo := FieldNoByName ('PROGISID');
         SELF.ARecordSet.MoveFirst;
         SELF.ARecordSet.Move (StartRow, 0);
         //viewdbgrid.DataSource.DataSet.recno:=StartRow;
         ViewDBGrid.RowBitsClear;
         ViewDBGrid.SelectedCount := 0;
         for i := StartRow to EndRow do
         begin
            iMark := ExtractValue1 (SELF.ARecordSet.Fields[iInternalIDFieldNo].Value) - 1;
            ViewDBGrid.RowBits[iMark] := true;
            Inc (ViewDBGrid.SelectedCount);
            if iLinkIdNo > -1 then if ExtractValue1 (SELF.ARecordSet.Fields[cs_CalcFieldLinkIDName].Value) > 0 then
                begin
                bLinkID:=True;
                LinkIDlist.Add(ExtractValue1 (SELF.ARecordSet.Fields[cs_CalcFieldLinkIDName].Value));
                end;
            if AToolbarManager.ButtonDown['AutoAddToSelected'] then
            begin
               iProgisIDFieldValue := ExtractValue1 (SELF.ARecordSet.Fields[iProgisIDFieldNo].Value);
               ShowSelectedItemsInProjectProc (SELF.FProject, SELF.FLayer, iProgisIDFieldValue, true, AutoZoom.Checked);
            end;
            SELF.ARecordSet.MoveNext;
            //ViewDBGRid.DataSource.DataSet.Next;
         end;
         StartRow := iRecNo - 1;
      end //shift
      else
      begin
         iMark := SELF.DataQuery.FieldByName (cs_InternalID_FieldName).AsInteger - 1;
         if AToolbarManager.ButtonDown['AutoAddToSelected'] then
            iProgisIDFieldValue := SELF.DataQuery.FieldByName ('PROGISID').AsInteger;
         bCurrentSelected := ViewDBGrid.RowBits[iMark];
         if bCurrentSelected then
            Dec (ViewDBGrid.SelectedCount);
         if not (GetKeyState (VK_CONTROL) < 0) then
         begin
            ViewDBGrid.RowBitsClear;
            ViewDBGrid.SelectedCount := 0;
            if AToolbarManager.ButtonDown['AutoAddToSelected'] then
               DeselectAllObjectsProc (SELF.FProject);
         end;
         if not bCurrentSelected then
            begin
            Inc (ViewDBGrid.SelectedCount);
            if iLinkIdNo > -1 then if SELF.DataQuery.FieldByName(cs_CalcFieldLinkIDName).asInteger > 0 then
                begin
                bLinkID:=True;
                LinkIDlist.Add(SELF.DataQuery.FieldByName(cs_CalcFieldLinkIDName).asInteger);
                end;
            end;
         ViewDBGrid.RowBits[iMark] := not bCurrentSelected;
         if AToolbarManager.ButtonDown['AutoAddToSelected'] then
            ShowSelectedItemsInProjectProc (SELF.FProject, SELF.FLayer, iProgisIDFieldValue, not bCurrentSelected, AutoZoom.Checked);
         StartRow := iRecNo - 1;
      end;
   end;

// ++ Cadmensky Version 2.3.2
   AToolbarManager.ButtonEnabled['Monitoring'] := GetLayerItemsCountFunc (SELF.FProject, nil) > 0;
// -- Cadmensky Version 2.3.2
   AToolbarManager.ButtonEnabled['ShowSelectedItemsWithZoom'] := ViewDBGrid.SelectedCount > 0;
   AToolbarManager.ButtonEnabled['ShowSelectedRecords'] := (ViewDBGrid.SelectedCount > 0) or (AToolbarManager.ButtonDown['ShowSelectedRecords']) ;
   AMenuManager.MenuEnabled['ShowSelectedItems'] := ViewDBGrid.SelectedCount > 0;
   AToolbarManager.Buttonvisible['ShowSelectedLinkID'] := bLinkID;
   ShowSelectedOnlyMenue.Enabled:= AToolbarManager.ButtonEnabled['ShowSelectedRecords'];
   AMenuManager.MenuEnabled['ShowSelectedItemsWithZoom'] := ViewDBGrid.SelectedCount > 0;
   AMenuManager.MenuEnabled['MakeAnnotationsForSelectedRecords'] := ViewDBGrid.SelectedCount > 0;
   AMenuManager.MenuEnabled['MakeChartsForSelectedRecords'] := ViewDBGrid.SelectedCount > 0;
   AMenuManager.MenuEnabled['MakeTMForSelectedRecords'] := ViewDBGrid.SelectedCount > 0;
   SELF.ViewDBGrid.SelectedRows.Clear;
   SELF.ViewDBGrid.Enabled := true;
   Application.ProcessMessages;

   RestoreScreenCursor;
   Screen.Cursor := crDefault;
   Application.ProcessMessages;

   if iInternalIDFieldNo = -1 then
   begin
      ViewDBGrid.SelectedCount := 1;
      SELF.ShowSelectedItemsInProject (AutoZoom.Checked);
   end;

   ViewDBGrid.Repaint;
   MakeRecordsStatus;
try
  ViewDBGrid.SetFocus;
except end;
end;

procedure TIDB_ViewForm.ViewDBGridDblClick (Sender: TObject);
var
   //sSortString: string;
   Cell: TGridCoord;
   sTableName: AnsiString;
begin
   if SELF.ViewDBGrid.HorzSplitterPosition > -1 then
   begin
      ResizeGridCellPopUpClick (nil);
      exit;
   end;

   Cell := ViewDBGrid.MouseCoord (ViewDBGrid.GetLastMouseX, ViewDBGrid.GetLastMouseY);
   if Cell.Y = 0 then
   begin
      //sSortString := Trim (AnsiUpperCase (TDADODataSet (ViewDataSource.DataSet).Sort));
      //if Pos (' ASC',SELF.DataQuery.Sort{ sSortString}) < 1 then
      if dblclicksortorder then
         SortAscPopUpClick(Sender) //CPB_Order := ' DESC'
      else
         SortDescPopUpClick(Sender);//CPB_Order := ' ASC';
      dblclicksortorder:= not dblclicksortorder;
     { CPB_SortField := ViewDBGrid.SortField;

      sTableName := ALayerManager.GetTableName (SELF.FLayer, CPB_SortField);
      if sTableName <> '' then
         CPB_SortField := '[' + sTableName + '].[' + CPB_SortField + ']';
      CPB_SortField := CPB_SortField + CPB_Order;
      if CPB_Order = ' ASC' then  CPB_SortField := CPB_SortField + ', [' + cs_InternalID_FieldName + '] DESC'
         else  CPB_SortField := CPB_SortField + ', [' + cs_InternalID_FieldName + '] ASC';
      if AnsiUpperCase (ViewDBGrid.SortField) <> 'PROGISID' then
         CPB_SortField := CPB_SortField + ', [' + SELF.sAttTableName + '].[PROGISID] ASC'; // + CPB_Order;

      Create_Power_Broker (false); }
   end;
end;

function TIDB_ViewForm.Create_Power_Broker (bNeedToClearRowBits: boolean = true): boolean;
var
   TmpString: AnsiString;
   iValue: Integer;
begin
   Result := false;

   ViewDBGrid.SelectedRows.Clear;

   TmpString := 'SELECT MAX([' + cs_InternalID_FieldName + ']) AS ROWBITS_SIZE FROM [' + SELF.sAttTableName + ']';
   ARecordSet := CoRecordSet.Create;

   ARecordSet.Open (TmpString,
      SELF.DataQuery.CentralData.ADO,
      adOpenKeyset,
      adOpenKeyset,
      adCmdText);

   if ARecordSet.RecordCount > 0 then
   begin
      iValue := ExtractValue1 (ARecordSet.Fields[0].Value);
      ViewDBGrid.RowBits.Size := iValue;
   end;
   ARecordSet.Close;
   if bNeedToClearRowBits then
   begin
      ViewDBGrid.SelectedCount := 0;
      ViewDBGrid.RowBitsClear;
   end;

   TmpString := DataQuery.SQL.Text;
   if CPB_SortField <> '' then
      TmpString := TmpString + ' ORDER BY ' + CPB_SortField; // + ' ' + CPB_Order;

   ARecordSet := CoRecordSet.Create;
   ShowPercentAtWinGISProc (1, 1453, 90);
   ARecordSet.Open (TmpString,
      SELF.DataQuery.CentralData.ADO,
      adOpenKeyset,
      adOpenKeyset,
      adCmdText);
   ShowPercentAtWinGISProc (1, 1453, 91);
   if ARecordSet.State <> adStateOpen then
      raise Exception.Create ('The Power Broker recordset cannot be opened.');
   ShowPercentAtWinGISProc (2, 0, 0);
end;

procedure TIDB_ViewForm.ClosePowerBroker;
begin
   try
      if ARecordSet.State = adStateOpen then
         ARecordSet.Close;
   except
   end;
end;

procedure TIDB_ViewForm.MakeBookmarksList (Flag: boolean);
var
   i, last: integer;
   PROGISID: integer;
   INTNUMBER: integer;
   iValue: integer;
begin
   PROGISID := FieldNoByName ('PROGISID');
   INTNUMBER := FieldNoByName (cs_InternalID_FieldName);
   Last := 0;
   ProgisIDList.Clear;
   InternalIDList.Clear;
   ARecordSet.MoveFirst;
   ViewDBGrid.SelectedCount := 0;

   try
      ARecordSet.MoveFirst;
      for i := 0 to ARecordSet.RecordCount - 1 do
      begin
         iValue := ExtractValue1 (ARecordSet.Fields[INTNUMBER].Value) - 1;
         if ViewDbGrid.RowBits[iValue] = true then
         begin
            if Flag = true then
               begin
               ProgisIDList.Add (ExtractValue1 (ARecordSet.Fields[PROGISID].Value));
               InternalIDList.Add (ExtractValue1 (ARecordSet.Fields[INTNUMBER].Value));
               end;
            Last := i;
            Inc (ViewDBGrid.SelectedCount);
         end;
         if Frac (i / 100) = 0 then
         begin
            Gauge.Progress := i;
         end;
         ARecordSet.MoveNext;
      end;

   except
      ShowMessage ('Error !');
   end;
end;

function TIDB_ViewForm.FieldNoByName (sFieldName: AnsiString): Integer;
var
   iI: integer;
begin
   RESULT := -1;
   for iI := 0 to SELF.ARecordSet.Fields.Count - 1 do
      if AnsiCompareText (SELF.ARecordSet.Fields[iI].Name, sFieldName) = 0 then
      begin
         RESULT := iI;
         break;
      end;
end;

function TIDB_ViewForm.FieldNoByNameEx (sFieldName: AnsiString; TmpRecordSet: RecordSet): Integer;
var
   iI: integer;
begin
   RESULT := -1;
   for iI := 0 to TmpRecordSet.Fields.Count - 1 do
      if AnsiCompareText (TmpRecordSet.Fields[iI].Name, sFieldName) = 0 then
      begin
         RESULT := iI;
         break;
      end;
end;

procedure TIDB_ViewForm.CMMouseLeave (var Msg: TMessage);
begin
   if Assigned (FHintWin) then
      FHintWin.ReleaseHandle;
   FHintX := -1;
   FHintY := -1;
   FHintCol := -1;
   FHintRow := -1;
   inherited;
end;

procedure TIDB_ViewForm.CMMouseENTER (var Msg: TMessage);
begin
   if Assigned (FHintWin) then
      FHintWin.ReleaseHandle;
   inherited;
end;

procedure TIDB_ViewForm.IDB_Toolbar1Exit (Sender: TObject);
begin
   if Assigned (FHintWin) then
      FHintWin.ReleaseHandle;
end;

procedure TIDB_ViewForm.ViewDBGridMouseMove (Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
   if Assigned (FHintWin) then
      FHintWin.ReleaseHandle;
   AlreadyResized := false;
end;

procedure TIDB_ViewForm.FormMove (var Msg: TMessage);
begin
   if Assigned (FHintWin) then
      FHintWin.ReleaseHandle;
   inherited;
end;

procedure TIDB_ViewForm.GotoSelectRecord (iItemID: integer);
begin
   try
      DataQuery.Locate ('PROGISID', iItemID, []);
   except
   end;
end;

procedure TIDB_ViewForm.ViewDBGridExit (Sender: TObject);
begin
   if Assigned (FHintWin) then
      FHintWin.ReleaseHandle;
end;

procedure TIDB_ViewForm.FormHide (Sender: TObject);
begin
   if Assigned (FHintWin) then   FHintWin.ReleaseHandle;
end;

procedure TIDB_ViewForm.LoadQueryClick (Sender: TObject);
var
   bQueryResultWindowWasShown: Boolean;
   AQueryResultForm: TIDB_ViewForm;
//   LQWF: TIDB_ListQueryWindowForm;
   PLFTS: TIDB_TableSelect; // Form for selecting source table in MS Access file.
   ADescMan: TIDB_DescMan;
   sQueryName, sSQLText: AnsiString;
begin
   try
      ADescMan := SELF.ALayerManager.GetLayerDescMan (SELF.FLayer);

      PLFTS := TIDB_TableSelect.Create (Application);
      PLFTS.Notebook1.PageIndex := 0; //ActivePage := 'LOAD';
      PLFTS.ListBox1.Items.Text := ADescMan.GetQueryNames;
      if PLFTS.ListBox1.Items.Count < 1 then
      begin
         PLFTS.Free;
         MessageBox (SELF.Handle, PChar (ALangManager.GetValue (432)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
         EXIT;
      end;
      PLFTS.ListBox1.ItemIndex := 0;
      PLFTS.Caption := ALangManager.GetValue (433);
      PLFTS.Panel3.Caption := ALangManager.GetValue (434);
      if PLFTS.ShowModal <> mrOK then
      begin
         PLFTS.Free;
         EXIT;
      end;
      sQueryName := PLFTS.ListBox1.Items[PLFTS.ListBox1.ItemIndex];
      PLFTS.Free;
      sSQLText := ADescMan.GetQueryAsString (sQueryName);
      AQueryResultForm := nil;
      bQueryResultWindowWasShown := ALayerManager.ShowQueryResultWindow (SELF.FProject, FLayer, TForm (AQueryResultForm), sQueryName, sSQLText);

   finally
      if bQueryResultWindowWasShown then
         AQueryResultForm.MakeWindowForDisplayingQueryResult (SELF.sAttTableName, '');
   end;

end;

procedure TIDB_ViewForm.SaveQueryClick (Sender: TObject);
var
   ADescMan: TIDB_DescMan;
   LQWF: TIDB_ListQueryWindowForm;
   sQueryName: AnsiString;
   listQueryNames: TStringList;
   i: integer;
   tmpstr: string;
begin
   ADescMan := SELF.ALayerManager.GetLayerDescMan (SELF.FLayer);

   listQueryNames := TStringList.Create;
   listQueryNames.Text := ADescMan.GetQueryNames;

   if listQueryNames.Count < 1 then
   begin
      sQueryName := ALangManager.GetValue (217);
      if not InputQuery (cs_ApplicationName, ALangManager.GetValue (218), sQueryName) then
         EXIT;
   end
   else
   begin
      LQWF := TIDB_ListQueryWindowForm.Create (SELF);
      LQWF.MakeSaveQueryUI;
      LQWF.ListBox1.Items.Text := listQueryNames.Text;

      tmpstr:= copy(self.Caption,pos('''',self.caption)+1,length(self.caption));
      tmpstr:=stringreplace(tmpstr,'''','',[rfReplaceAll]);
      for i:= 0 to LQWF.ListBox1.Items.Count-1 do
             if tmpstr= LQWF.ListBox1.Items[i] then  LQWF.ListBox1.ItemIndex:=i;
      if LQWF.ShowModal = mrOk then
      begin
         for i := 0 to listQueryNames.Count - 1 do
            if LQWF.ListBox1.Items.IndexOf (listQueryNames.Strings[i]) = -1 then
               ADescMan.DeleteQueryByName (listQueryNames.Strings[i]);

         if LQWF.RadioButton1.Checked then
            sQueryName := LQWF.ListBox1.Items[LQWF.ListBox1.ItemIndex]
         else
            sQueryName := LQWF.Edit1.Text;
      self.querysaved:=True;
      end;
      LQWF.Free;
   end;
   listQueryNames.Free;
   ADescMan.SetQueryAsString (sQueryName, SELF.DataQuery.SQL.Text);
   ADescMan.SetUserChoice;
end;

procedure TIDB_ViewForm.SelectAll1Click (Sender: TObject);
var
   iI: integer;
begin
   if ViewDBGrid.Rowbits.Size<= 2 then exit;
   for iI := 0 to ViewDBGrid.RowBits.Size - 1 do
   begin
      ViewDBGrid.RowBits[iI] := true;
   end;

   if AToolbarManager.ButtonDown['AutoAddToSelected'] then
      SELF.ShowSelectedItemsInProject (AutoZoom.Checked);
   ViewDBGrid.SelectedCount := ViewDBGrid.DataSource.DataSet.RecordCount;
   ViewDBGrid.Repaint;
   MakeRecordsStatus;
end;

procedure TIDB_ViewForm.AlignVerticalClick (Sender: TObject);
var
   ifLeft: Integer;
begin
   if (SELF.FormStyle = fsMDIChild) or
      (AWinGISMainForm.Width > Screen.DesktopWidth - 100) then
// ++ Cadmensky Version 2.3.8
   begin
      AWinGISMainForm.WindowState := wsNormal;
      if self.width> round(Screen.Desktopwidth*0.7) then AWinGISMainForm.Width := Round (Screen.DesktopWidth * 0.6)
         else AWinGISMainForm.Width := Screen.Desktopwidth-self.width;
//      exit;
   end;
// -- Cadmensky Version 2.3.8

   AWinGISMainForm.Align := alLeft;
   AWinGISMainForm.Align := alNone;

   ifLeft := AWinGISMainForm.Left + AWinGISMainForm.Width;

   SELF.Left := ifLeft;
   SELF.Width := Screen.DesktopWidth - ifLeft;
   SELF.Top := AWinGISMainForm.Top;
   SELF.Height := AWinGISMainForm.Height;
   if not SELF.bIAmQueryResultWindow then
   begin
      ALayerManager.StoreParametersOfViewForm (SELF.FLayer, SELF);
      bParametersAlreadyWereSaved := true;
   end;
end;

procedure TIDB_ViewForm.AlignHorizontalClick (Sender: TObject);
var
   iTop: Integer;
   iBottom: Integer;
begin
   if (SELF.FormStyle = fsMDIChild) or
      (AWinGISMainForm.Height > Screen.DesktopHeight - 100) then
// ++ Cadmensky Version 2.3.8
   begin
      AWinGISMainForm.WindowState := wsNormal;
      AWinGISMainForm.Height := Round (Screen.DesktopHeight * 0.7);
//      exit;
   end;
// -- Cadmensky Version 2.3.8

   iTop := AWinGISMainForm.Height;

   SELF.Height := Screen.DesktopHeight - AWinGISMainForm.Height;
   SELF.Align := alBottom;
   iBottom := SELF.Height + SELF.Top;

   SELF.Height := iBottom - AWinGISMainForm.Height;

   SELF.Align := alNone;

   AWinGISMainForm.Left := SELF.Left;
   AWinGISMainForm.Width := SELF.Width;
   AWinGISMainForm.Top := Screen.DesktopTop;
   self.ALayerManager.StoreParametersOfViewForm(aLayer,self);
   if not SELF.bIAmQueryResultWindow then
   begin
      ALayerManager.StoreParametersOfViewForm (SELF.FLayer, SELF);
      bParametersAlreadyWereSaved := true;
   end;
end;

procedure TIDB_ViewForm.UnhideColumnPopupClick (Sender: TObject);
var
   PLFTS: TIDB_TableSelect;
   AStringList: TStringList;
   AFieldsVisibilityMan: TIDB_FieldsVisibilityManager;
   iI: integer;
   sFieldName: AnsiString;
begin
   AStringList := TStringList.Create;
   PLFTS := TIDB_TableSelect.Create (Application);
   PLFTS.Notebook1.PageIndex := 0; //ActivePage := 'LOAD';

   AFieldsVisibilityMan := SELF.ALayerManager.GetLayerFieldsVisibilityManager (SELF.FLayer);
   AFieldsVisibilityMan.DetectFieldsStates (SELF.ViewDBGrid);
   AStringList.Text := AFieldsVisibilityMan.GetAllAttributeFieldNames;
   for iI := 0 to AStringList.Count - 1 do
   begin
      sFieldName := AStringList[iI];
      if not AFieldsVisibilityMan.GetFieldVisibleState (sFieldName) then
         PLFTS.ListBox1.Items.Add (sFieldName);
   end;
   AStringList.Free;

   if PLFTS.ListBox1.Items.Count < 1 then
   begin
      PLFTS.Free;
      MessageBox (SELF.Handle, PChar (ALangManager.GetValue (449)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
      EXIT;
   end
   else
   begin
      PLFTS.ListBox1.ItemIndex := 0;
      PLFTS.ListBox1.MultiSelect := true;
      PLFTS.Caption := ALangManager.GetValue (448);
      PLFTS.Panel3.Caption := ALangManager.GetValue (453);
      if PLFTS.ShowModal <> mrOK then
      begin
         PLFTS.Free;
         EXIT;
      end;

      for iI := 0 to PLFTS.ListBox1.Items.Count - 1 do
         if PLFTS.ListBox1.Selected[iI] then
         begin
            sFieldName := PLFTS.ListBox1.Items[iI];
            AFieldsVisibilityMan.SetFieldVisibleState (sFieldName, true);
         end;
      AFieldsVisibilityMan.RestoreFieldsStates (SELF.ViewDBGrid);
      PLFTS.Free;

      ShowRecordsForSelectedObjects;
   end;
end;

procedure TIDB_ViewForm.ShowRecordsForSelectedObjects;
var
   iI: integer;
   iProgisIDField: Integer;
   iInternalIDField: Integer;
   iMark: Integer;
   sIDs, sSQL: AnsiString;
   iProgisIDFieldValue, iItemID: Integer;
   iSelectedCount, iObjectCount, iP: Integer;
   iFirstSelectedItemID: Integer;
   //AnExtTablesMan: TIDB_ExternalTablesMan;
begin
   iSelectedCount := GetLayerItemsCountFunc (SELF.FProject, ALayer, true);
   AToolbarManager.ButtonEnabled['Monitoring'] := iSelectedCount > 0;

   if not AToolbarManager.ButtonDown['AutoAddToSelected'] then  exit;

   SetScreenCursor;
   try

      AToolbarManager.ButtonEnabled['ShowSelectedItemsWithZoom'] := iSelectedCount > 0;
      AMenuManager.MenuEnabled['ShowSelectedItems'] := iSelectedCount > 0;
      AMenuManager.MenuEnabled['ShowSelectedItemsWithZoom'] := iSelectedCount > 0;
      AMenuManager.MenuEnabled['MakeAnnotationsForSelectedRecords'] := iSelectedCount > 0;
      AMenuManager.MenuEnabled['MakeChartsForSelectedRecords'] := iSelectedCount > 0;
      AMenuManager.MenuEnabled['MakeTMForSelectedRecords'] := iSelectedCount > 0;

      ViewDBGrid.RowBitsClear;
      ViewDBGrid.SelectedCount := 0;

      if AToolbarManager.ButtonDown['ShowSelectedRecords'] then
      begin
// ++ Cadmensky Version 2.3.9
  //       AnExtTablesMan := SELF.ALayerManager.GetLayerExtTablesMan (SELF.FLayer);
// -- Cadmensky Version 2.3.9
         if (iSelectedCount < 1) or 
            (iSelectedCount > 253) or
            (ARecordSet.RecordCount < 1) then
         begin
// ++ Cadmensky Version 2.3.9
       {     if AnExtTablesMan.IAmInUse then
               SELF.DataQuery.SQL.Text := AnExtTablesMan.JoiningSQL
            else }
// -- Cadmensky Version 2.3.9
               SELF.DataQuery.SQL.Text := 'SELECT * FROM [' + SELF.sAttTableName + '] WHERE (' + cs_ShowAllWithoutDeletedObjectsFilter + ')';
            SELF.DataQuery.Reopen;
            AToolbarManager.ButtonDown['ShowSelectedRecords'] := false;
            RestoreScreenCursor;
            ShowRecordsForSelectedObjects;
            exit;
         end
         else
         begin
            SELF.DataQuery.Close;
            sSQL := '';
            iP := 0;
            iObjectCount := GetLayerItemsCountFunc (SELF.FProject, SELF.FLayer);
            for iI := 0 to iObjectCount - 1 do
            begin
               iItemID := GetLayerItemIndexByNumberFunc (SELF.FProject, ALayer, iI, true);
               if iItemID > 0 then
               begin
//                  Continue;
// ++ Cadmensky Version 2.3.9
                  Inc (iP);

                 { if AnExtTablesMan.IAmInUse then
                     sIDs := sIDs + '[' + SELF.sAttTableName + '].[PROGISID] = ' + IntToStr (iItemID) + ' OR '
                  else }
// -- Cadmensky Version 2.3.9
                     sIDs := sIDs + 'PROGISID = ' + IntToStr (iItemID) + ' OR ';
               end;
               if ((iP > 0) and (iP mod 253 = 0)) or (iI = iObjectCount - 1) then
               begin
                  sIDs := Copy (sIDs, 1, Length (sIDs) - 4);
// ++ Cadmensky Version 2.3.9
                 { if AnExtTablesMan.IAmInUse then
                     sSQL := sSQL + AnExtTablesMan.JoiningSQL + ' AND (' + sIDs + ') UNION '
                  else }
// -- Cadmensky Version 2.3.9
                     sSQL := sSQL + 'SELECT * FROM [' + SELF.sAttTableName + '] WHERE (' + cs_ShowAllWithoutDeletedObjectsFilter + ')' +
                        ' AND (' + sIDs + ') UNION ';
               end;
            end;
            sSQL := Copy (sSQL, 1, Length (sSQL) - 7);

            SELF.DataQuery.SQL.Text := sSQL;
            try
               SELF.DataQuery.Open;
            except
// ++ Cadmensky Version 2.3.9
               {if AnExtTablesMan.IAmInUse then
                  SELF.DataQuery.SQL.Text := AnExtTablesMan.JoiningSQL
               else  }
// -- Cadmensky Version 2.3.9
                  SELF.DataQuery.SQL.Text := 'SELECT * FROM [' + SELF.sAttTableName + '] WHERE (' + cs_ShowAllWithoutDeletedObjectsFilter + ')';
               AToolbarManager.ButtonDown['ShowSelectedRecords'] := false;
               SELF.DataQuery.Open;
               SELF.Create_Power_Broker (false);
               RestoreScreenCursor;
               ShowRecordsForSelectedObjects;
               Exit;
            end;

            SELF.Create_Power_Broker (false);
         end;
      end
      else
      begin
         if (iSelectedCount < 1) or (ARecordSet.RecordCount < 1) then
         begin
            RestoreScreenCursor;
            ViewDBGrid.Repaint;
            MakeRecordsStatus;
            exit;
         end;

         iProgisIDField := FieldNoByName ('PROGISID');
         iInternalIDField := FieldNoByName (cs_InternalID_FieldName);

         if (iProgisIDField = -1) or (iInternalIDField = -1) then
         begin
            RestoreScreenCursor;
            exit;
         end;

         SELF.ViewDBGrid.Enabled := false;
         SELF.AToolbar.Enabled := false;
         SELF.bIAmWorking := true;

         if iSelectedCount > 1 then
         begin
            PrepareGauge ('Selecting...');
            SELF.Gauge.MaxValue := iSelectedCount;
         end;

         if iSelectedCount > 1 then
         begin
            ARecordSet.MoveFirst;
            for iI := 0 to ARecordSet.RecordCount - 1 do
            begin
               try
                  iProgisIDFieldValue := ExtractValue1 (ARecordSet.Fields[iProgisIDField].Value);
                  SELF.Gauge.Progress := SELF.Gauge.Progress + 1;
                  iMark := ExtractValue1 (ARecordSet.Fields[iInternalIDField].Value) - 1;
                  ViewDBGrid.RowBits[iMark] := IsObjectSelectedFunc (SELF.FProject, iProgisIDFieldValue);
                  if ViewDBGrid.RowBits[iMark] then
                     Inc (ViewDBGrid.SelectedCount);
               except
               end;
               ARecordSet.MoveNext;
            end;
         end;

         iFirstSelectedItemID := -1;
         for iI := 0 to iSelectedCount - 1 do
         begin
            iFirstSelectedItemID := GetLayerItemIndexByNumberFunc (SELF.FProject, ALayer, iI, true);

            if iFirstSelectedItemID <> -1 then
            begin
// ++ Cadmensky Version 2.3.6
               ARecordSet.MoveFirst;
               ARecordSet.Find ('PROGISID = ' + IntToStr (iFirstSelectedItemID), 0, adSearchForward, EmptyParam);
               if not ARecordSet.EOF then
                  Break
               else
                  iFirstSelectedItemID := -1;
// -- Cadmensky Version 2.3.6
            end;
         end;

         if iFirstSelectedItemID > 0 then
            if ViewDBGrid.DataSource.DataSet.Locate ('ProgisID', IntToStr (iFirstSelectedItemID), []) then
               if iSelectedCount = 1 then
               begin
                  iMark := ViewDBGrid.DataSource.DataSet.FieldByName (cs_InternalID_FieldName).AsInteger - 1;
                  ViewDBGrid.RowBits[iMark] := true;
                  ViewDBGrid.SelectedCount := 1;
               end;

         SELF.Gauge.Visible := false;
         SELF.ViewDBGrid.Enabled := true;
         SELF.AToolbar.Enabled := true;
         SELF.bIAmWorking := false;
         SELF.bTheUserWantsToCancel := false;
         SELF.MakeRecordsStatus;

         ViewDBGrid.Repaint;
      end;
   finally
      RestoreScreenCursor;
   end;
end;

procedure TIDB_ViewForm.DeselectAll1Click (Sender: TObject);
begin
   ViewDBGrid.RowBitsClear;
   if AToolbarManager.ButtonDown['AutoAddToSelected'] then
      SELF.ShowSelectedItemsInProject (false);
   ViewDBGrid.SelectedCount := 0;
   ViewDBGrid.Repaint;
   MakeRecordsStatus;
end;

procedure TIDB_ViewForm.InvertSelection1Click (Sender: TObject);
var
   iI: integer;
begin
   ViewDBGrid.SelectedCount := 0;
   for iI := 0 to ViewDBGrid.RowBits.Size - 1 do
   begin;
      ViewDBGrid.RowBits[iI] := not ViewDBGrid.RowBits[iI];
      if ViewDBGrid.RowBits[iI] = true then
         Inc (ViewDBGrid.SelectedCount);
   end;
   if AToolbarManager.ButtonDown['AutoAddToSelected'] then
      SELF.ShowSelectedItemsInProject (AutoZoom.Checked);
   ViewDBGrid.Repaint;
   MakeRecordsStatus;
end;

procedure TIDB_ViewForm.SetAutoAddToSelectedButtonDown (bValue: boolean);
begin
   SELF.AToolbarManager.ButtonDown['AutoAddToSelected'] := bValue and (SELF.FormStyle <> fsMDIChild);
   self.AutoAddToSelectedSB.Enabled:= (SELF.FormStyle <> fsMDIChild);
   end;

procedure TIDB_ViewForm.AppBtnClick (Sender: TObject);
var
   FilterString, TmpString: AnsiString;
   iI, iJ, iP: integer;
   //AnExtTablesMan: TIDB_ExternalTablesMan;
   ATable: TDTable;
   sTableName: AnsiString;
   sFieldName: AnsiString;
   AStructMan: TIDB_StructMan;
   bFieldIsLinked: Boolean;

begin
   SELF.ALayerManager.DetectFieldsStates (SELF.FLayer, SELF.ViewDBGrid);
   //AnExtTablesMan := SELF.ALayerManager.GetLayerExtTablesMan (SELF.FLayer);
   AStructMan := nil;

   ComboBox1.Visible := false;
   StringGrid1.Visible := false;
   FilterPanel.Visible := false;
   ViewDBGrid.Visible := true;

   SELF.bIAmUnderFilter := true;

   AToolbarManager.ButtonEnabled['AddRecord'] := false;
   AMenuManager.MenuEnabled['AddRecord'] := false;

   FilterString := '';
   for iI := 1 to StringGrid1.RowCount - 1 do
   begin
      TmpString := '';
      for iJ := 1 to StringGrid1.ColCount - 1 do
         if Trim (StringGrid1.Cells[iJ, iI]) <> '' then
         begin
            bFieldIsLinked := false;

            {if SELF.bIAmResultOfExternalTablesJoining then
            begin
               sFieldName := StringGrid1.Cells[iJ, 0];
// ++ Cadmensky Version 2.3.6
               iP := GetLinkedFieldLinkNo (sFieldName, sFieldName);
               if iP < 0 then
                  sTableName := SELF.sAttTableName
               else
               begin
                  bFieldIsLinked := true;
                  //sTableName := AnExtTablesMan.GetLinkName (iP);
                  AStructMan := TIDB_StructMan.Create (SELF.FProject, SELF.ALayerManager.DataAccessObject);
                  ATable := TDTable.Create (nil);
                  try
                     ATable.Master := SELF.ALayerManager.DataAccessObject.DMaster;
                     ATable.TableName := sTableName;
                     AStructMan.DetectStructure (ATable);
                  finally
                     ATable.Close;
                     ATable.Free;
                  end;
               end;
// -- Cadmensky Version 2.3.6

            end //with linked
            else }
            begin
               sTableName := SELF.sAttTableName;
               sFieldName := StringGrid1.Cells[iJ, 0];
            end;

            if TmpString <> '' then
               TmpString := TmpString + ' AND ';

            if bFieldIsLinked then
               TmpString := TmpString + '([' + sTableName + '].[' + sFieldName + '] ' + FilterMachineAnalyzer (StringGrid1.Cells[iJ, iI], AStructMan.GetFieldType (sFieldName)) + ')'
            else
               TmpString := TmpString + '([' + sTableName + '].[' + sFieldName + '] ' + FilterMachineAnalyzer (StringGrid1.Cells[iJ, iI], Ord (DataQuery.FieldByName (sFieldName).DataType)) + ')';

// ++ Cadmensky Version 2.3.6
            if AStructMan <> nil then
               AStructMan.Free;
// -- Cadmensky Version 2.3.6
         end;
      if TmpString <> '' then
         if iI = 1 then
            FilterString := FilterString + ' (' + TmpString + ') '
         else
            FilterString := FilterString + StringGrid1.Cells[0, iI] + ' (' + TmpString + ') ';
   end; // for i

   if FilterString <> '' then
      FilterString := ' AND (' + FilterString + ')';

   SELF.DataQuery.Close;

   {if bIAmResultOfExternalTablesJoining then
      SELF.DataQuery.SQL.Text := AnExtTablesMan.JoiningSQL + FilterString
   else   }
      SELF.DataQuery.SQL.Text := 'SELECT * FROM [' + sAttTableName + '] ' +
         'WHERE (' + cs_ShowAllWithoutDeletedObjectsFilter + ')' + FilterString;

   FilterString := SELF.DataQuery.SQL.Text;
   try
      SELF.ClosePowerBroker;
      SELF.DataQuery.Open;
      Create_Power_Broker;
// ++ Cadmensky Version 2.3.6
      ShowRecordsForSelectedObjects;
// -- Cadmensky Version 2.3.6
   except
      MessageBox (SELF.Handle, PChar (ALangManager.GetValue (454)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONERROR);
      SELF.ApplyFieldVisibilityChanges;
      SELF.Create_Disable_FilterSBClick (nil);
      SELF.bIAmUnderFilter := false;
      exit;
   end;
   SELF.bIAmUnderFilter := true;
//   SELF.ApplyFieldVisibilityChanges;

   AToolbarManager.ButtonDown['Filter'] := true;
   // 322=Disable filter
   AToolbarManager.ButtonHint['Filter'] := ALangManager.GetValue (322);
   self.Filtermenue.Caption:=ALangManager.GetValue (322);
   SELF.SetMyCaptionFiltered;

   for iI := 0 to StringGrid1.ColCount - 2 do
      ViewDBGrid.Columns.Items[iI].Width := StringGrid1.ColWidths[iI + 1];
end;

procedure TIDB_ViewForm.FillingFilterCombo (sFieldName: string);
var
   APB_Recordset: RecordSet;
   i, j: integer;
   //AnExtTablesMan: TIDB_ExternalTablesMan;
   sTableName: AnsiString;
   sSQLText: AnsiString;
   iLinkNo: Integer;
   sRealFieldName: AnsiString;
begin
   if (StringGrid1.Row > 0) and (StringGrid1.Col > 0) then
   begin
      ComboBox1.Items.Clear;

      Combobox1.Items.Add ('IS NULL');
      Combobox1.Items.Add ('IS NOT NULL');
      StartWidthComboList := 80;

// ++ Commented by Cadmensky Version 2.3.2
//      ComboBox1.Text := StringGrid1.Cells[StringGrid1.Col, StringGrid1.Row];
// -- Commented by Cadmensky Version 2.3.2

      if UseCheck.Checked = true then
      begin
         Screen.Cursor := crHourGlass;

        // AnExtTablesMan := SELF.ALayerManager.GetLayerExtTablesMan (SELF.FLayer);

         StatusLabel.Caption := ALangManager.GetValue (460);
// ++ Commeneted by Cadmensky version 2.3.3
//         Application.ProcessMessages;
// -- Commeneted by Cadmensky version 2.3.3

        // if not bIAmResultOfExternalTablesJoining then
            sSQLText := 'SELECT DISTINCT [' + sFieldName + '] ' +
               'FROM [' + SELF.sAttTableName + '] ' +
               'WHERE (' + cs_ShowAllWithoutDeletedObjectsFilter + ')';
         {else
         begin
            iLinkNo := GetLinkedFieldLinkNo (sFieldName, sRealFieldName);
            if iLinkNo < 0 then
               sSQLText := 'SELECT DISTINCT [' + sFieldName + '] ' +
                  'FROM [' + SELF.sAttTableName + '] ' +
                  'WHERE (' + cs_ShowAllWithoutDeletedObjectsFilter + ')'
            else
            begin
               //sTableName := AnExtTablesMan.GetLinkName (iLinkNo);
               sSQLText := 'SELECT DISTINCT [' + sTableName + '].[' + sRealFieldName + '] ' +
                  'FROM ' + AnExtTablesMan.JoinClause + ' ' +
                  'WHERE (' + cs_ShowAllWithoutDeletedObjectsFilter + ')';
            end;  }
{            iI := Pos ('_', sFieldName);
            if iI < 1 then
               sTableName := sAttTableName
            else
            begin
               try
                  sTableName := AnExtTablesMan.GetLinkName (StrToInt (Copy (sFieldName, 1, iI - 1)) - 1);
                  sFieldName := Copy (sFieldName, iI + 1, Length (sFieldName));
               except
                  sTableName := sAttTableName;
               end;
            end;
            sSQLText := 'SELECT DISTINCT [' + sTableName + '].[' + sFieldName + '] ' +
               'FROM ' + AnExtTablesMan.JoinClause + ' ' +
               'WHERE ' + cs_ShowAllWithoutDeletedObjectsFilter;}
     //    end;

         APB_RecordSet := CoRecordSet.Create;
         try
            APB_RecordSet.Open (sSQLText,
               SELF.DataQuery.CentralData.ADO,
               adOpenKeyset,
               adLockReadOnly,
               adCmdText);
         except
         end;

// ++ Cadmensky Version 2.3.6
         if APB_RecordSet.State = adStateOpen then
{         if APB_RecordSet.State <> adStateOpen then
            raise Exception.Create (' TMP Recordset cannot be opened.')
         else}
// -- Cadmensky Version 2.3.6
         begin
            APB_RecordSet.MoveFirst;
            ComboBox1.Items.Add (ExtractValue (APB_RecordSet.Fields[0].Value));
            Gauge.Visible := true;
            Gauge.Progress := 0;
            Gauge.MaxValue := APB_RecordSet.RecordCount - 1;
            for i := 0 to APB_RecordSet.RecordCount - 2 do
            begin
               APB_RecordSet.Move (1, 0);
               Gauge.Progress := Gauge.Progress + 1;
               ComboBox1.Items.Add (ExtractValue (APB_RecordSet.Fields[0].Value));
               j := Length (Combobox1.Items[i + 2]) * 7;
               if J > StartWidthComboList then StartWidthComboList := j;
            end;
         end;
         StatusLabel.Caption := ALangManager.GetValue (461);
// ++ Commeneted by Cadmensky version 2.3.3
//         Application.ProcessMessages;
// -- Commeneted by Cadmensky version 2.3.3
         Gauge.Visible := false;
// ++ Cadmensky version 2.3.6
         if APB_RecordSet.State = adStateOpen then
// -- Cadmensky version 2.3.6
            APB_RecordSet.Close;
//         Screen.Cursor := crDefault;
      end; //usecheck
   end; //work with
   Screen.Cursor := crDefault;
end;

procedure TIDB_ViewForm.UseCheckClick (Sender: TObject);
begin
   if ComboBox1.Visible then
   begin
      FillingFilterCombo (FilterField);
      Combobox1.DroppedDown := true;
   end;
end;

procedure TIDB_ViewForm.StringGrid1TopLeftChanged (Sender: TObject);
begin
   ComboBox1.Visible := false;
end;

procedure TIDB_ViewForm.StringGrid1KeyDown (Sender: TObject; var Key: Word; Shift: TShiftState);
begin

   if Key = VK_DOWN then
   begin;
      if (StringGrid1.RowCount) = (StringGrid1.Row + 1) then
      begin
         StringGrid1.RowCount := StringGrid1.RowCount + 1;
         StringGrid1.Row := StringGrid1.Row + 1;
         Stringgrid1.Cells[0, StringGrid1.Row] := 'AND'
      end
      else
         StringGrid1.Row := StringGrid1.Row + 1;
      ComboBox1.Visible := false;
   end;
   if Key in [VK_DOWN, VK_UP, VK_LEFT, VK_RIGHT, VK_HOME, VK_END] then
   begin;
      Combobox1.Visible := false;
      inherited;
   end
end;

procedure TIDB_ViewForm.StringGrid1SelectCell (Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
var
   R: TRect;
begin
   if ComboBox1.DroppedDown = true then
   begin
      ComboBox1.DroppedDown := false;
//      ComboBox1.Text := '';
      StringGrid1.Invalidate;
// ++ Cadmensky Version 2.3.2
      Application.ProcessMessages;
   end;
// -- Cadmensky Version 2.3.2

// ++ Cadmensky Version 2.3.2
   if ARow = 0 then
      Combobox1.Visible := false;
// -- Cadmensky Version 2.3.2

   if (ACol = 0) and (ARow > 1) then
   begin
      if StringGrid1.Cells[ACol, ARow] = '' then
         StringGrid1.Cells[ACol, ARow] := 'AND'
      else
         if StringGrid1.Cells[ACol, ARow] = 'AND' then
            StringGrid1.Cells[ACol, ARow] := 'OR'
         else
            if StringGrid1.Cells[ACol, ARow] = 'OR' then
               StringGrid1.Cells[ACol, ARow] := 'AND';
      Exit;
   end;

   if ((ACol > 0) and (ARow > 0)) then
   begin
// ������ � ��������� ComboBox ������ ��������������� ������ StringGrid
      R := StringGrid1.CellRect (ACol, ARow);
      R.Left := R.Left + StringGrid1.Left;
      R.Right := R.Right + StringGrid1.Left;
      R.Top := R.Top + StringGrid1.Top;
      R.Bottom := R.Bottom + StringGrid1.Top;
      ComboBox1.Left := R.Left + 1;
      ComboBox1.Top := R.Top;
      ComboBox1.Width := (R.Right + 1) - R.Left + 1;
      ComboBox1.Height := (R.Bottom + 1) - R.Top + 1;
// ������� combobox
      ComboBox1.Text := StringGrid1.Cells[ACol, ARow];
      FilterField := StringGrid1.Cells[ACol, 0];
      FillingFilterCombo (FilterField);
      ComboBox1.Visible := True;
      ComboBox1.SetFocus;
   end;
   CanSelect := true;
end;

procedure TIDB_ViewForm.StringGrid1MouseDown (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
{var
   ACol, ARow: Integer;
   R: TRect;}
begin
{   StringGrid1.MouseToCell (x, y, ACol, ARow);
   if ComboBox1.DroppedDown = true then
   begin
      ComboBox1.DroppedDown := false;
      ComboBox1.Text := '';
      StringGrid1.Invalidate;
   end;
   Application.ProcessMessages;

// ++ Cadmensky Version 2.3.2
   if ARow = 0 then
      Combobox1.Visible := false;
// -- Cadmensky Version 2.3.2

   if (ACol = 0) and (ARow > 1) then
   begin
      if StringGrid1.Cells[ACol, ARow] = '' then
         StringGrid1.Cells[ACol, ARow] := 'AND'
      else
         if StringGrid1.Cells[ACol, ARow] = 'AND' then
            StringGrid1.Cells[ACol, ARow] := 'OR'
         else
            if StringGrid1.Cells[ACol, ARow] = 'OR' then
               StringGrid1.Cells[ACol, ARow] := 'AND';
      Exit;
   end;

   if ((ACol > 0) and (ARow > 0)) then
   begin
// ������ � ��������� ComboBox ������ ��������������� ������ StringGrid
      StringGrid1.MouseToCell (x, y, ACol, Arow);
      R := StringGrid1.CellRect (ACol, ARow);
      R.Left := R.Left + StringGrid1.Left;
      R.Right := R.Right + StringGrid1.Left + 1;
      R.Top := R.Top + StringGrid1.Top;
      R.Bottom := R.Bottom + StringGrid1.Top (*+1*);
      ComboBox1.Left := R.Left + 1;
      ComboBox1.Top := R.Top;
      ComboBox1.Width := (R.Right + 1) - R.Left + 1;
      ComboBox1.Height := (R.Bottom + 1) - R.Top + 1;
// ������� combobox
      ComboBox1.Text := StringGrid1.Cells[ACol, ARow];
      FilterField := StringGrid1.Cells[ACol, 0];
      FillingFilterCombo (FilterField);
      ComboBox1.Visible := True;
      Application.ProcessMessages;
      if ComboBox1.Text <> '' then
         ComboBox1.SetFocus;
      if ComboBox1.Items.Count > 2 then
         ComboBox1.DroppedDown;
   end;}
end;

procedure TIDB_ViewForm.StringGrid1MouseMove (Sender: TObject;
   Shift: TShiftState; X, Y: Integer);
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
end;

// ++ Commented by Cadmensky Version 2.3.7
{procedure TIDB_ViewForm.appIdle (Sender: TObject; var Done: boolean);
var
   pt: TPoint;
   wnd: HWND;
   buf: array[0..128] of Char;
//   i: Integer;
   ARect: TRect;
begin
   if ComboBox1.Visible = false then Exit;

   GetCursorPos (pt);
   wnd := WindowFromPoint (pt);
   buf[0] := #0;
   if wnd <> 0 then
   begin
      GetClassName (wnd, buf, sizeof (buf));
      if StrIComp (buf, 'ComboLBox') = 0 then
      begin
         GetWindowRect (wnd, ARect);
         SetWindowPos (wnd, HWND_TOPMOST, ARect.Left, ARect.Top, 200, ARect.Bottom - ARect.Top, SWP_SHOWWINDOW);
      end;
   end;
end;}
// -- Commented by Cadmensky Version 2.3.7

procedure TIDB_ViewForm.CancFiltBtnClick (Sender: TObject);
begin
   StringGrid1.Visible := false;
   FilterPanel.Visible := false;
   ViewDBGrid.Visible := true;
   Combobox1.Visible := false;
// ++ Cadmensky
   AToolbarManager.ButtonDown['Filter'] := false;
   AToolbarManager.ButtonHint['Filter'] := ALangManager.GetValue (321);
   self.Filtermenue.Caption:=Create_Disable_FilterSB.Hint;
// -- Cadmensky
// ++ Cadmensky Version 2.3.8
   SELF.RestoreUIAfterFilterSettingsDialog;
// -- Cadmensky Version 2.3.8
end;

procedure TIDB_ViewForm.ComboBox1Change (Sender: TObject);
begin
   StringGrid1.Cells[StringGrid1.Col, StringGrid1.Row] := ComboBox1.Text;
end;

procedure TIDB_ViewForm.ComboBox1DropDown (Sender: TObject);
//var
//   d: boolean;
begin
   ComboBox1.Perform (CB_SETDROPPEDWIDTH, StartWidthComboList, 0);
end;

procedure TIDB_ViewForm.ComboBox1Exit (Sender: TObject);
//var
//   APos: TPoint;
begin

   if ComboBox1.DroppedDown = true then
      ComboBox1.DroppedDown := false;
   ComboBox1.Visible := False;
   ComboBox1.Text := '';
   StringGrid1.Invalidate;
   StringGrid1.SetFocus;

end;

procedure TIDB_ViewForm.ComboBox1KeyDown (Sender: TObject; var Key: Word; Shift: TShiftState);
begin

   if Key = VK_DOWN then
   begin;
      if StringGrid1.RowCount = StringGrid1.Row + 1 then
      begin
         StringGrid1.RowCount := StringGrid1.RowCount + 1;
         StringGrid1.Row := StringGrid1.Row + 1;
         StringGrid1.Cells[0, StringGrid1.Row] := 'AND'
      end
      else
         StringGrid1.Row := StringGrid1.Row + 1;
         ComboBox1.Visible := false;
   end;

end;

// ++ Commented by Cadmensky Version 2.3.7
(*function TIDB_ViewForm.FieldIsNumberType (sFieldName: AnsiString): Boolean;
var
   iFT: Integer;
   TF: TFieldType;
begin
   RESULT := false;
   TF := DataQuery.FieldByName (sFieldName).DataType;
   iFT := Ord (TF);
   if iFT < 0 then EXIT; // This field was not found in the structure data.
   {
   1 - String
   2 - SmallInt
   3 - Integer
   5 - Boolean
   6 - Float
   7 - Currency
   11 - DateTime
   12 - Byte
   }
   case iFT of
      2..3, 6..7, 12: RESULT := true;
   end
end;*)

procedure TIDB_ViewForm.ComboBox1KeyUp (Sender: TObject; var Key: Word;
   Shift: TShiftState);
begin
   if (Key = vk_tab) and (Shift = []) then
   begin;
      if Stringgrid1.Col + 1 < Stringgrid1.Colcount then Stringgrid1.Col := Stringgrid1.Col + 1;
      Combobox1.Visible := false;
   end;
end;

procedure TIDB_ViewForm.StringGrid1KeyUp (Sender: TObject; var Key: Word;
   Shift: TShiftState);
begin
   if (Key = vk_tab) and (Shift = []) then
   begin;
      if Stringgrid1.Col + 1 < Stringgrid1.Colcount then Stringgrid1.Col := Stringgrid1.Col + 1;
      Combobox1.Visible := false;
   end;
end;

function TIDB_ViewForm.FilterMachineAnalyzer (ASourceCell: AnsiString; FieldType: integer): AnsiString;

   procedure ReplaceChars1 (var StringStroka: AnsiString; A, B: char);
   var
      i: integer;
      TmpString: ansistring;
   begin;
      TmpString := '';
      for i := 1 to Length (StringStroka) do
         if StringStroka[i] = A then
            TmpString := TmpString + B
         else
            TmpString := TmpString + StringStroka[i];
      StringStroka := TmpString;
   end;

var
//   i: integer;
   Stroka, TmpStroka: ansistring;
   IsNOT, IsLIKE: boolean;
   iPos: integer;
begin
   if (Trim (Uppercase (ASourceCell)) = 'IS NULL') or (Trim (Uppercase (ASourceCell)) = 'IS NOT NULL') then
   begin;
      Result := Trim (Uppercase (ASourceCell));
      Exit;
   end;
   Stroka := Trim (Uppercase (ASourceCell));
   IsNOT := false;
   iPos := Pos ('NOT', Stroka);
   if iPos > 0 then
   begin;
      IsNOT := true;
      Stroka := Copy (Stroka, iPos + 3, Length (Stroka) - 3);
      TmpStroka := Copy (Stroka, 1, 1);
      if TmpStroka = '(' then
         Stroka := Copy (Stroka, 2, Length (Stroka) - 1);

      TmpStroka := Copy (Stroka, Length (Stroka), 1);
      if TmpStroka = ')' then
         Stroka := Copy (Stroka, 1, Length (Stroka) - 1);
      Stroka := Trim (Stroka);
   end;

   IsLIKE := false;
   if FieldType = 1 then
   begin;
      //if ? or * then I'm working only as with Like
      if (Pos ('*', Stroka) > 0) then
      begin;
         ReplaceChars1 (Stroka, '*', '%');
         IsLIKE := true;
      end;
      if (Pos ('?', Stroka) > 0) then
      begin;
         ReplaceChars1 (Stroka, '?', '_');
         IsLIKE := true;
      end;
      if IsLIKE then
      begin
         iPos := Pos ('LIKE ', Stroka);
         if iPos > 0 then
            Stroka := Copy (Stroka, iPos + 5, Length (Stroka) - 5);
         Stroka := 'LIKE "' + Stroka + '"'
      end;

   end;

   if (IsLike = false) then
   begin;
      if FieldType <> 1 then
      begin;
         ReplaceChars1 (Stroka, DecimalSeparator, '.');
         if (Pos ('<>', Stroka) = 0) and (Pos ('<=', Stroka) = 0) and (Pos ('>=', Stroka) = 0)
            and (Pos ('>', Stroka) = 0) and (Pos ('<', Stroka) = 0) and (Pos ('=', Stroka) = 0) then Stroka := '=' + Stroka;
      end
      else
      begin;
         if (Pos ('<>', Stroka) > 0) or (Pos ('<=', Stroka) > 0) or (Pos ('>=', Stroka) > 0) then
            Stroka := Copy (Stroka, 1, 2) + '''' + Copy (Stroka, 3, Length (Stroka) - 2) + ''''
         else
            if (Pos ('>', Stroka) > 0) or (Pos ('<', Stroka) > 0) or (Pos ('=', Stroka) > 0) then
               Stroka := Copy (Stroka, 1, 2) + '''' + Copy (Stroka, 2, Length (Stroka) - 1) + ''''
            else
               Stroka := '=' + '''' + Stroka + ''''
      end; //stroka
   end; // like false

   Result := Stroka;
   //    else
     // I'm don't know, what is this stupid user want !!
   if isNot = true then
      Result := ' NOT ' + Result + ' ';
end;

procedure TIDB_ViewForm.FilterPanelResize (Sender: TObject);
begin
   if SELF.Width > 310 then
   begin
      FilterPanel.Width := SELF.Width - 8;
   end
end;

procedure TIDB_ViewForm.StringGrid1RowMoved (Sender: TObject; FromIndex,
   ToIndex: Integer);
begin
   Combobox1.Visible := false;
end;

procedure TIDB_ViewForm.ShowSelectedRecordsSBClick (Sender: TObject);
var sSql: String;
   iSelectedCount: Integer;
   //AnExtTablesMan: TIDB_ExternalTablesMan;
begin
   if not AToolbarManager.ButtonDown['ShowSelectedRecords'] then
   begin
      SetScreenCursor;
      SELF.DataQuery.Close;
// ++ Cadmensky Version 2.3.9
//      AnExtTablesMan := SELF.ALayerManager.GetLayerExtTablesMan (SELF.FLayer);
  {    if AnExtTablesMan.IAmInUse then
         SELF.DataQuery.SQL.Text := AnExtTablesMan.JoiningSQL
      else     }
// -- Cadmensky Version 2.3.9
         SELF.DataQuery.SQL.Text := 'SELECT * FROM [' + SELF.sAttTableName + '] WHERE (' + cs_ShowAllWithoutDeletedObjectsFilter + ')';
      SELF.DataQuery.Open;
      SELF.Create_Power_Broker (false);
      RestoreScreenCursor;
   end
   else
   begin
      ShowSelectedOnlyMenue.Checked:=False;
      {iSelectedCount := GetLayerItemsCountFunc (SELF.FProject, SELF.FLayer, true);
      if (iSelectedCount < 1) or
         (iSelectedCount > 253) or
         not AToolbarManager.ButtonDown['AutoAddToSelected'] then
      begin
         AToolbarManager.ButtonDown['ShowSelectedRecords'] := false;
         exit;
      end;    }
   //SELF.ShowRecordsForSelectedObjects;
   Self.MakeBookmarksList (true);
   if InternalIDList.Count>0 then
   begin
            SELF.DataQuery.Close;
            sSQL := 'SELECT * FROM [' + SELF.sAttTableName + '] WHERE ';
            for iSelectedCount:= 0 to InternalIDList.Count -1 do
                sSQL:=sSQL+ '['+cs_InternalID_FieldName+']=' + inttostr(internalIDList[iSelectedCount]) + ' OR ';
            sSQL := Copy (sSQL, 1, Length (sSQL) - 4);
            SELF.DataQuery.SQL.Text := sSQL;
            try
               SELF.DataQuery.Open;
            except
// ++ Cadmensky Version 2.3.9
               {if AnExtTablesMan.IAmInUse then
                  SELF.DataQuery.SQL.Text := AnExtTablesMan.JoiningSQL
               else  }
// -- Cadmensky Version 2.3.9
               SELF.DataQuery.SQL.Text := 'SELECT * FROM [' + SELF.sAttTableName + '] WHERE (' + cs_ShowAllWithoutDeletedObjectsFilter + ')';
               AToolbarManager.ButtonDown['ShowSelectedRecords'] := false;
               SELF.DataQuery.Open;
               SELF.Create_Power_Broker (false);
               RestoreScreenCursor;
               ShowRecordsForSelectedObjects;
            end;
            SELF.Create_Power_Broker (false);
         end;
   end;
   ShowSelectedOnlyMenue.Checked:=AToolbarManager.ButtonDown['ShowSelectedRecords'];
   Restorescreencursor;
end;

procedure TIDB_ViewForm.AutoAddToSelectedSBClick (Sender: TObject);
begin
try
   if AToolbarManager.ButtonDown['AutoAddToSelected'] then
   begin
      if SELF.ViewDBGrid.SelectedCount > 0 then //= GetLayerItemsCountFunc (SELF.FProject, SELF.FLayer, true) then
         SELF.ShowSelectedItemsInProject (AutoZoom.Checked)
      else
         if GetLayerItemsCountFunc (SELF.FProject, SELF.FLayer, true)>0 then SELF.ShowRecordsForSelectedObjects;
      SELF.ViewDBGrid.Repaint;
   end;
except end;   
end;


procedure TIDB_ViewForm.HelpmenueClick(Sender: TObject);
var helpfile,lang: string;
begin
// Hilfe Aufrufen
Helpmenue.Enabled:=False;
application.ProcessMessages;
try
lang:=stringReplace(extractfileext(sLanguageFilename),'.','',[]);
helpfile:= ExtractFilePath (Application.ExeName)+ 'IDB_' + lang + '.chm';
if not fileexists(helpfile) then
 helpfile:= ExtractFilePath (Application.ExeName)+ 'WINGIS2000_' + lang + '.chm';
shellexecute(0,'open',pchar(helpfile),'','',1);
sleep(100);
finally
application.ProcessMessages;
Helpmenue.Enabled:=True;
end;
end;

procedure TIDB_ViewForm.InfomenueClick(Sender: TObject);
begin
// Info Fenster anzeigen
ExecuteWinGISMenuFuncion(1);
end;

procedure TIDB_ViewForm.ShowSelectedOnlyMenueClick(Sender: TObject);
begin
self.ShowSelectedRecordsSB.Down:=not self.ShowSelectedRecordsSB.Down;
ShowSelectedOnlyMenue.Checked:=self.ShowSelectedRecordsSB.Down;
ShowSelectedRecordsSBClick(Sender);
end;

procedure TIDB_ViewForm.ExitmenueClick(Sender: TObject);
begin
close;
end;

procedure TIDB_ViewForm.Replace1Click(Sender: TObject);
begin
UpdateRecords1Click(Sender);
end;


procedure TIDB_ViewForm.LinkRecordwithObjectClick(Sender: TObject);
var sSQL : String;
   AnExecQuery: TDQuery;
begin
if (ViewDBGrid.SelectedCount <> 1) or (GetLayerItemsCountFunc (SELF.FProject, ALayer,True)<>1) then
   begin
   MessageBox (Handle, PChar (ALangManager.GetValue(517)), PChar (cs_ApplicationName), MB_OK + MB_ICONSTOP);
   AToolbarManager.ButtonDown['AutoAddToSelected']:=False;
   exit;
   end;
 MakeBookmarksList (true);
 AnExecQuery := TDQuery.Create (SELF);
try
 DataQuery.Close;
 AnExecQuery.Master := SELF.ALayerManager.DataAccessObject.DMaster;
 try
   sSql:='UPDATE [' + sAttTableName + '] ' +
               'SET [PROGISID] = ' + inttostr(GetLayerItemIndexByNumberFunc (SELF.FProject, ALayer, 0, true))  +
               ' WHERE [' + cs_InternalID_FieldName + '] = ' + inttostr(internalidlist[0]);
   AnExecQuery.ExecSQL (sSql);
  except
  if MessageBox (Handle, PChar (Format(ALangManager.GetValue(518),[GetLayerItemIndexByNumberFunc(SELF.FProject,ALayer,0,true)])), PChar (cs_ApplicationName), MB_YESNO + MB_ICONQUESTION) = IDYES then
   begin
   sSql:='DELETE [PROGISID] FROM [' + sAttTableName + '] ' +
               'WHERE [PROGISID] = ' + inttostr(GetLayerItemIndexByNumberFunc (SELF.FProject, ALayer, 0, true));
   AnExecQuery.ExecSQL (sSql);
   sSql:='UPDATE [' + sAttTableName + '] ' +
               'SET [PROGISID] = ' + inttostr(GetLayerItemIndexByNumberFunc (SELF.FProject, ALayer, 0, true))  +
               ' WHERE [' + cs_InternalID_FieldName + '] = ' + inttostr(internalidlist[0]);
   AnExecQuery.ExecSQL (sSql);
   end;
  end;
finally
DataQuery.Open;
AnExecQuery.free;
end;
end;

procedure TIDB_ViewForm.FormShow(Sender: TObject);
begin

if (self.DataQuery.RecordCount< 1) and (self.bIAmQueryResultWindow=False)
   then if  GetLayerItemsCountFunc (SELF.FProject, ALayer,False)>0
        then if Messagebox(Handle, PChar (ALangManager.GetValue(10)+'?'), PChar (cs_ApplicationName), MB_YESNO + MB_ICONQUESTION) = mrYES then
                  self.AllExistingItemsInProject1Click(Sender);
end;

procedure TIDB_ViewForm.SaveFieldVisibilityChanges;
var
   AFieldsVisibilityMan: TIDB_FieldsVisibilityManager;
begin
   try
      AFieldsVisibilityMan := SELF.ALayerManager.GetLayerFieldsVisibilityManager (SELF.FLayer);
      AFieldsVisibilityMan.SaveData;
   finally
   end;
end;


procedure TIDB_ViewForm.AutoZoomClick(Sender: TObject);
begin
AutoSelect.Checked:=False;
AutoZoom.Checked:=True;
   if not SELF.bIAmQueryResultWindow then
   begin
      ALayerManager.StoreParametersOfViewForm (SELF.FLayer, SELF);
      bParametersAlreadyWereSaved := true;
   end;
end;

procedure TIDB_ViewForm.AutoSelectClick(Sender: TObject);
begin
AutoSelect.Checked:=True;
AutoZoom.Checked:=False;
   if not SELF.bIAmQueryResultWindow then
   begin
      ALayerManager.StoreParametersOfViewForm (SELF.FLayer, SELF);
      bParametersAlreadyWereSaved := true;
   end;
end;

procedure TIDB_ViewForm.ZoomOrSelectClick(Sender: TObject);
var p : TPoint;
begin
getcursorpos(p);
self.PopupMenuZoomOrSelect.Popup(p.x,p.y);
end;

procedure TIDB_ViewForm.EnableSort;
var bv : Boolean;
begin
bv:=   (FieldNoByName(ViewdbGrid.PushedColumnName)<> -1) and (Self.DataQuery.RecordCount>0) ;
SortAscSB.Enabled :=bv;
SortDescSB.enabled :=bv;
SortAscPopUp.Enabled :=bv;
SortDescPopUp.enabled :=bv;
end;

procedure TIDB_ViewForm.Statistic1Click(Sender: TObject);
var sf : TStatisticForm;
begin
sf:= TStatisticForm.Create(self);
try
 if self.bIAmQueryResultWindow then sf.Caption:= sf.Caption + ' - ' + AlangManager.GetValue(5);
  sf.Caption:= sf.Caption + ' - ' + GetLayerNameFunc (SELF.FLayer);
 sf.ShowModal;
finally
 sf.Free;
 end;
end;

procedure TIDB_ViewForm.druck1Click(Sender: TObject);
var rf : TRepForm;
    i  : Integer;
    ld  : Array of TQRDBText;
    l   : Array of TQRLabel;
    lt  : String;
begin
 lt:=Statuslabel.Caption;
 statuslabel.caption:= AlangManager.GetValue(506)+ '...';
 Application.ProcessMessages;
 rf:= TRepForm.Create(nil);
try
 statuslabel.caption:=statuslabel.caption + '.';
 Application.ProcessMessages;
 rf.QuickRep1.DataSet:= viewdbgrid.DataSource.DataSet;
 rf.Titellabel.Caption:= self.Caption;
 rf.summarytext.Text:= AlangManager.GetValue(651)+' ';
 setlength(ld,viewdbgrid.Columns.Count);
 setlength(l, viewdbgrid.Columns.Count);
try
 for i:= 0 to viewdbgrid.Columns.Count -1  do
   begin
   statuslabel.caption:=statuslabel.caption + '.';
   Application.ProcessMessages;
   l[i]:= TQRLabel.Create(rf);
   l[i].parent:= rf.PageHeaderBand1;
   l[i].Autosize:=False;
   l[i].font.Assign(viewdbgrid.font);
   if i=0 then l[i].left:=0 else
      l[i].left:= l[i-1].left + l[i-1].width;
   l[i].width:=viewdbgrid.Columns[i].width;
   if l[i].width<1 then l[i].width:=1;
   l[i].caption:= viewdbgrid.Fields[i].fieldname;
   l[i].top:= rf.PageHeaderBand1.Height-l[i].height;
   l[i].color:=clltgray;
   l[i].frame.DrawTop:=True;
   l[i].frame.DrawBottom:=True;
   l[i].frame.Drawleft:=True;
   l[i].frame.Drawright:=True;
   l[i].frame.Color:=clgray;
   end;
except end;
try
   for i:= 0 to viewdbgrid.Columns.Count-1  do
   begin
   ld[i]:= TQRDBText.Create(rf);
   ld[i].parent:= rf.DetailBand1;
   ld[i].Autosize:=False;
   ld[i].font.Assign(viewdbgrid.font);
   if i=0 then ld[i].left:=0 else
      ld[i].left:= ld[i-1].left + ld[i-1].width;
   ld[i].width:=viewdbgrid.Columns[i].width;
   if ld[i].width<1 then ld[i].width:=1;
   ld[i].top:=1;
   ld[i].DataSet:=viewdbgrid.DataSource.DataSet;
   ld[i].DataField:= viewdbgrid.Fields[i].fieldname;

   ld[i].frame.DrawTop:=True;
   ld[i].frame.DrawBottom:=True;
   ld[i].frame.Drawleft:=True;
   ld[i].frame.Drawright:=True;
   ld[i].frame.Color:=clgray;
   statuslabel.caption:=statuslabel.caption + '.';
   Application.ProcessMessages;
   end;
except end;
 statuslabel.Caption:='Please wait...';
 application.ProcessMessages;
 if self.WindowState<> wsMaximized then showallwindows(false);
 rf.QuickRep1.Preview;
 finally
  Self.visible:=True;
  showallwindows(true);
  statuslabel.caption:=lt;
  for i:= 0 to length(l)-1 do l[i].free;
  for i:= 0 to length(ld)-1 do ld[i].free;
  rf.free;
 end;
end;


procedure TIDB_ViewForm.ShowAllWindows(isvisible: Boolean);
var iI: integer;
    AForm: TForm;
begin
iI := 0;
   while iI < Screen.FormCount do
   begin
      AForm := Screen.Forms[iI];
      if (AForm is TIDB_ViewForm) then
      begin
         try
            AForm.Visible:=isvisible;
            //Application.ProcessMessages;
         except
         end;
      end;
      Inc (iI);
   end; // while end
end;

procedure TIDB_ViewForm.ShowSelectedLinkIDClick(Sender: TObject);
var i : Integer;
begin
for i:= 0 to LinkIDList.Count -1 do
  ShowSelectedItemsInProjectProc (SELF.FProject, Nil, LinkIDList[i], true, true);
RunMonitoringProc (SELF.FProject, false);
end;

procedure TIDB_ViewForm.Button1Click(Sender: TObject);
var i, iMark,iInternalIDField, iProgisIDField : Integer;
begin
iProgisIDField := FieldNoByName ('PROGISID');
iInternalIDField := FieldNoByName (cs_InternalID_FieldName);
self.ARecordSet.MoveFirst;
for i := 0 to SELF.ARecordSet.RecordCount - 1 do
               begin
                  iMark := ExtractValue1 (SELF.ARecordSet.Fields[iInternalIDField].Value) - 1;
                  if SELF.ViewDBGrid.RowBits[iMark] then
                  begin
                  makeannotationupdateproc(self.FLayer,  strtoint( SELF.ARecordSet.Fields[iProgisIDField].Value) );
                  end;
               Self.ARecordSet.MoveNext;
               end;   
end;

procedure TIDB_ViewForm.FormatFieldClick(Sender: TObject);
var
   Coord: TGridCoord;
   ACol: Integer;
   sFieldName: AnsiString;
   ff: TFormatfieldForm;
   mr,i: Integer;
   myformat: string;
begin
   Coord := ViewDBGrid.MouseCoord (ViewDBGrid.GetLastMouseX, ViewDBGrid.GetLastMouseY);
   ACol := Coord.X;
   if ViewDBGrid.FieldCount > 1 then
   begin
      sFieldName := ViewDBGrid.Fields[ACol].FieldName;
      ff:= TFormatfieldForm.Create(self);
      ff.Caption:= ff.Caption + ' - ' + sFieldName;
      myformat:=TFloatfield(SELF.DataQuery.FieldByName (sFieldName)). DisplayFormat;
      if (pos('#.#',myformat)> 0) or (myformat='') then ff.RadioButtonDefault.Checked:=True else ff.RadioButtonStellen.Checked:=True;
      if pos(',',myformat)> 0 then ff.CheckBoxSeperator.Checked:=True;
      mr:=0;
      for i := 1 to length(myformat) do if myformat[i]='0' then mr:=mr+1;
      ff.SpinEditStellen.Value:=mr;
      ff.RadioButtonDefaultClick(sender);
      mr:= ff.ShowModal;
      if (mr>0) then
      begin
        mr:=mr-10000;
        myformat:='#.';
        if mr >= 1000 then
         begin
         myformat:=',' + myformat;
         mr:=mr-1000;
         end;
        if mr <100 then for i:= 1 to mr do myformat:=myformat+'0';
        if mr = 100 then myformat:=myformat+'#';
      TFloatfield(SELF.DataQuery.FieldByName (sFieldName)). DisplayFormat := myformat;
      SELF.ALayerManager.DetectFieldsStates (SELF.FLayer, SELF.ViewDBGrid);
      ShowRecordsForSelectedObjects;
      end;
      ff.free;
   end;
end;

end.


