�
 TIDB_PARAMSOFRECORDTEMPLATEFORM 0<
  TPF0TIDB_ParamsOfRecordTemplateFormIDB_ParamsOfRecordTemplateFormLeft,Top� Width�HeightTCaptionWinGISColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
KeyPreview	OldCreateOrderPositionpoScreenCenterScaledOnCreate
FormCreateOnKeyUp	FormKeyUpOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top Width�Height� AlignalClientTabOrder  TIDB_StringGridSGLeftTop*Width�Height� AlignalClientColCountDefaultColWidth� DefaultRowHeightRowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoColSizing	goEditinggoAlwaysShowEditor TabOrder OnClickSGClickAllowMovingFromToTheFirstLine	AllowMovingFromToTheLastLine		ColWidths� W 
RowHeights   TPanelTopPanelLeftTopWidth�Height)AlignalTop
BevelOuterbvNoneTabOrder TLabelLabel1LeftTopWidth�Height	AlignmenttaCenterAutoSizeCaption!312WordWrap	   	TComboBox	ComboBox1Left� Top� Width� Height
ItemHeightTabOrderVisibleOnChangeComboBox1Change
OnKeyPressComboBox1KeyPress  TPanelStatusPanelLeftTop� Width�Height"AlignalBottom
BevelOuterbvNoneTabOrderVisible TGaugeGauge1Left TopWidth�HeightAlignalBottom	ForeColorclBlueProgress2ShowText  TLabelLabel2LeftTopWidth�HeightAutoSizeCaption!326    TPanelBottomPanelLeft Top� Width�Height5AlignalBottom
BevelOuterbvNoneTabOrder TButtonOKButtonLeft2TopWidthKHeightAnchorsakRightakBottom Caption!49Default	ModalResultTabOrder OnClickOKButtonClick  TButtonCancelButtonLeft�TopWidthKHeightAnchorsakRightakBottom Cancel	Caption!50ModalResultTabOrder  TRadioButtonAND_RBLeftTopWidth8HeightCaption!318Checked	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTabStop	Visible  TRadioButtonOR_RBLeftATopWidth3HeightCaption!319Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible  	TCheckBoxUseExistingValuesCheckBoxLeftTopWidth#HeightCaption!206TabOrderOnClickUseExistingValuesCheckBoxClick  	TCheckBoxUpdateExistedRecordsCBLeftTopWidth!HeightCaption!470TabOrder   TDQuery
ADataQuery	FieldDefs 
MaxRecords SubList 
UpdateMode
upWhereAllOptionsopDialogopFilteropConnection Left)Top   