unit IDB_StructMan;
{
Internal database. Ver II.
Table structure manager.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 28-11-2001
}
interface

uses Classes, DBTables, SysUtils, Controls,
   DMaster, DTables, DDB,
   IDB_Consts, IDB_Access2DB;

type
   TrecFieldDefs = record
      bTheFieldHasBeenJustAdded: Boolean;
      sName: AnsiString;
      sOldName: AnsiString;
      iType: Integer;
      iOldType: Integer;
      iSize1: Integer;
      iOldSize1: Integer;
      bIndexedField: Boolean;
      bOldIndexedField: Boolean;
   end;

   TWPSortType = (stAscending, stDescending);
   TrecIndexDefs = record
      sIndexName: string[50];
      sFieldFromIndex: AnsiString;
   end;

   TIDB_StructMan = class
   private
      AProject: Pointer;
      IDB_DA: TIDB_Access2DB_Level2;
      listFieldsDefs: TList;
//      listIndexesDefs: TList;
      arDeletedFields: array of AnsiString;
      sTableName: AnsiString;
      bModified: Boolean;

      AnOldScreenCursor: TCursor;
      procedure SetScreenCursor;
      procedure RestoreScreenCursor;

      function FindFieldByName (sFieldName: AnsiString): Integer;
      procedure SetTablesNames (sTableName: AnsiSTring);

      function ChangeFieldName (iFieldPos: Integer; sNewFieldName: AnsiString): Boolean; overload;
      function ChangeFieldTypeAndSize (iFieldPos, iFieldType, iFieldSize: Integer): Boolean; overload;
      procedure ChangeFieldSize (iFieldPos, iFieldSize: Integer); overload;
      procedure ChangeFieldIndex (iFieldPos: Integer; bFieldIsIndexed: Boolean); overload;
   public
      constructor Create (AProject: Pointer; IDB_DA: TIDB_Access2DB_Level2);
      destructor Free;

      procedure DetectStructure (ADataSet: TDADODataSet; sTableName: AnsiString); overload;
      procedure DetectStructure (ATable: TDTable); overload; // ADO Table structure detection
      procedure DetectStructure (AMaster: TDMaster; ALayer: Pointer); overload; // Query structure detection

      procedure DoChangeStructure;

      procedure ClearData;

      procedure DeleteField (sFieldNameToBeDeleted: AnsiString); overload;
      procedure DeleteField (iFieldPos: Integer); overload;

      function GenerateFieldName (sBaseFieldName: AnsiString): AnsiString;
      function AddNewField (sNewFieldName: AnsiString): Boolean; overload;
      function AddNewField (sNewFieldName: AnsiString; iFieldType, iFieldSize: Integer): Boolean; overload;

      function ChangeFieldName (sOldFieldName: AnsiString; sNewFieldName: AnsiString): Boolean; overload;
      function ChangeFieldTypeAndSize (sFieldName: AnsiString; iFieldType, iFieldSize: Integer): Boolean; overload;
      procedure ChangeFieldSize (sFieldName: AnsiString; iFieldSize: Integer); overload;
      procedure ChangeFieldIndex (sFieldName: AnsiString; bFieldIsIndexed: Boolean); overload;

      function GetFieldCount: Integer;
      function GetFieldName (iFieldPos: Integer): AnsiString;
      function GetFieldType (iFieldPos: Integer): Integer; overload;
      function GetFieldType (sFieldName: AnsiString): Integer; overload;
      function GetFieldSize (iFieldPos: Integer): Integer; overload;
      function GetFieldSize (sFieldName: AnsiString): Integer; overload;
      function GetFieldIndexIsPresent (iFieldPos: Integer): Boolean; overload;
      function GetFieldIndexIsPresent (sFieldName: AnsiString): Boolean; overload;
      function FieldExists (sFieldName: AnsiString): Boolean;

      procedure ApplyStructureToAllTables;

// ++ Cadmensky
      procedure SetModified;
      function FieldIsNumberType (sFieldName: AnsiString): Boolean;
// -- Cadmensky

   end;

implementation

uses Windows, Forms,
   ADOBase,
   IDB_Utils, IDB_LanguageManager, IDB_CallbacksDef, IDB_MainManager,
   Dialogs;

constructor TIDB_StructMan.Create (AProject: Pointer; IDB_DA: TIDB_Access2DB_Level2);
begin
   SELF.AProject := AProject;
   SELF.IDB_DA := IDB_DA;
   listFieldsDefs := TList.Create; // List of fields data
   SetLength (arDeletedFields, 0);
   bModified := FALSE;
end;

destructor TIDB_StructMan.Free;
var
   iI: Integer;
   pFD: ^TrecFieldDefs;
begin
   // Delete list with information about fields.
   for iI := 0 to listFieldsDefs.Count - 1 do
   begin
      pFD := listFieldsDefs.Items[iI];
      Dispose (pFD);
   end;
   listFieldsDefs.Free;
end;

procedure TIDB_StructMan.SetScreenCursor;
begin
   SELF.AnOldScreenCursor := Screen.Cursor;
   Screen.Cursor := crHourGlass;
   Application.ProcessMessages;
   Application.HandleMessage;
end;

procedure TIDB_StructMan.RestoreScreenCursor;
begin
   Screen.Cursor := SELF.AnOldScreenCursor;
end;

procedure TIDB_StructMan.ClearData;
var
   pFD: ^TrecFieldDefs;
begin
   while SELF.listFieldsDefs.Count > 0 do
   begin
      pFD := listFieldsDefs[0];
      Dispose (pFD);
      listFieldsDefs.Delete (0);
   end;
end;

procedure TIDB_StructMan.SetTablesNames (sTableName: AnsiSTring);
begin
   SELF.sTableName := sTableName;
end;

procedure TIDB_StructMan.SetModified;
begin
   SELF.bModified := TRUE;
end;

// ++ Cadmensky

procedure TIDB_StructMan.DetectStructure (ADataSet: TDADODataSet; sTableName: AnsiString);
var
   iI: Integer;
   pFD: ^TrecFieldDefs;
   slIndexFieldNames: TStringList;
begin
   if ADataSet = nil then EXIT;
   try
      if not ADataSet.Active then ADataSet.Open;
   except
      EXIT;
   end;

   SELF.SetTablesNames (sTableName);
   SELF.ClearData;

   slIndexFieldNames := TStringList.Create;

   try
      ADataSet.Records.Connection.GetIndexInfo (sTableName, slIndexFieldNames, itIndexFieldNames);
   except
   end;

   for iI := 0 to ADataSet.FieldCount - 1 do
   begin
      if not FieldIsInternalUsageField (ADataSet.Fields[iI].FieldName)
      { and not FieldIsCalculatedField (ADataSet.Fields[iI].FieldName)}then
      begin
         New (pFD);
         pFD.sName := ADataSet.Fields[iI].FieldName;
         pFD.sOldName := pFD.sName;
         pFD.iType := Ord (ADataSet.Fields[iI].DataType);
         if pFD.iType = 4 then pFD.iType := 12; // Word type is Byte type indeed.
         pFD.iOldType := pFD.iType;
         pFD.iSize1 := ADataSet.Fields[iI].DataSize;
         if pFD.iType = 1 then
            pFD.iSize1 := pFD.iSize1 - 1; // I don't know why it returns the size that was increased on one.
         pFD.iOldSize1 := pFD.iSize1;
         // Is this field indexed field or not?
         if slIndexFieldNames.IndexOf (AnsiUpperCase (pFD.sName)) > -1 then
            pFD.bIndexedField := TRUE
         else
            pFD.bIndexedField := FALSE;
         pFD.bOldIndexedField := pFD.bIndexedField;

         pFD.bTheFieldHasBeenJustAdded := FALSE;
         listFieldsDefs.Add (pFD);
      end;
   end;
   slIndexFieldNames.Clear;
   slIndexFieldNames.Free;
end;
// -- Cadmensky

procedure TIDB_StructMan.DetectStructure (ATable: TDTable);
begin
   SELF.DetectStructure (TDADODataSet (ATable), ATable.TableName);
end;

procedure TIDB_StructMan.DetectStructure (AMaster: TDMaster; ALayer: Pointer);
var
   ATable: TDTable;
begin
   if AMaster = nil then EXIT;

   ATable := TDTable.Create (nil);
   ATable.Master := AMaster;
   if not AnIDBMainMan.UseNewATTNames (AProject) then
      ATable.TableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
   else
      ATable.TableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

   SELF.DetectStructure (TDADODataSet (ATable), ATable.TableName);
   ATable.Free;
end;

{This procedure changes the structure of attribute and pattern table.}

procedure TIDB_StructMan.DoChangeStructure;
var
   iI: Integer;
   pFD: ^TrecFieldDefs;
   bFieldIsPresent: Boolean;
   ExecQuery: TDQuery;

   procedure WorkWithField (sTableName, sOldFieldName, sNewFieldName: AnsiString; iFieldtype, iFieldSize: Integer);
//   var
//      bF: Boolean;
   begin
// ++ Cadmensky Version 2.3.2
      // Delete field that has name of temp field (on any case).
      if IDB_DA.ADbItemsMan.FieldExists (cs_TempFieldName) then
         IDB_DA.ADbItemsMan.DeleteTempField (sTableName);
// -- Cadmensky Version 2.3.2

// ++ Commented by Cadmensky Version 2.3.2
{      // Delete field that has name of temp field (on any case).
      if IDB_DA.ADbItemsMan.FieldExists (sTableName, cs_TempFieldName, bF) <> ci_All_Ok then
         EXIT;
      try
         if bF then IDB_DA.ADbItemsMan.DeleteTempField (sTableName);
      except
      end;}
// -- Commented by Cadmensky Version 2.3.2

      // Add temp field with necessary type and size.
      IDB_DA.ADbItemsMan.AddField (sTableName, cs_TempFieldName, iFieldType, iFieldSize);
      // Write into temp field data from field that must be changed.

      try
      if iFieldType=1 then  ExecQuery.ExecSQL ('UPDATE [' + sTableName + '] SET [' + cs_TempFieldName + '] = left([' + sOldFieldName + '],' + inttostr(iFieldsize)+ ')') else
         ExecQuery.ExecSQL ('UPDATE [' + sTableName + '] SET [' + cs_TempFieldName + '] = [' + sOldFieldName + ']');
      except
      end;
      // Delete information field.
      IDB_DA.ADbItemsMan.DeleteField (sTableName, sOldFieldName);
      // Add new field with necessary name, type and size.
      IDB_DA.ADbItemsMan.AddField (sTableName, sNewFieldName, iFieldType, iFieldSize);
      // Write data into new field from temp field.
      try
         ExecQuery.ExecSQL ('UPDATE [' + sTableName + '] SET [' + sNewFieldName + '] = [' + cs_TempFieldName + ']');
      except
      end;
      // Delete temp field.
      IDB_DA.ADbItemsMan.DeleteTempField (sTableName);
   end;

begin
   if not SELF.bModified then EXIT;

   ExecQuery := TDQuery.Create (nil);
   SetScreenCursor;
   try
      ExecQuery.Master := IDB_DA.DMaster;

// ++ Cadmensky Version 2.3.2
      if IDB_DA.ADBItemsMan.GetReadyToCheckTableStructure (SELF.sTableName) <> ci_All_Ok then
      begin
         IDB_DA.ADBItemsMan.EndOfCheckingTable;
         Exit;
      end;

   // Delete fields that is selected to deleting.
   // Before deleting we need to check is required field exists or not.
      for iI := 0 to High (arDeletedFields) do
         if IDB_DA.ADbItemsMan.FieldExists (arDeletedFields[iI]) then
            IDB_DA.ADbItemsMan.DeleteField (SELF.sTableName, arDeletedFields[iI]);
// -- Cadmensky Version 2.3.2

// ++ Commented by Cadmensky Version 2.3.2
{      // Delete fields that is selected to deleting.
      // Before deleting we need to check is required field exists or not.
      for iI := 0 to High (arDeletedFields) do
      begin
         if IDB_DA.ADbItemsMan.FieldExists (SELF.sTableName, arDeletedFields[iI], bFieldIsPresent) <> ci_All_Ok then
            EXIT;
         if bFieldIsPresent then
            IDB_DA.ADbItemsMan.DeleteField (SELF.sTableName, arDeletedFields[iI]);
      end; // for iI end}
// -- Commented by Cadmensky Version 2.3.2

      for iI := 0 to listFieldsDefs.Count - 1 do
      begin
         pFD := listFieldsDefs.Items[iI];
         pFD.sName:=stringreplace(pFD.sName,'.','_',[rfReplaceAll]);
         pFD.sName:=stringreplace(pFD.sName,'[','_',[rfReplaceAll]);
         pFD.sName:=stringreplace(pFD.sName,']','_',[rfReplaceAll]);
         pFD.sName:=stringreplace(pFD.sName,'!','_',[rfReplaceAll]);
         // if user has added new field, we create this field.
         if pFD.bTheFieldHasBeenJustAdded then
         begin
            IDB_DA.ADbItemsMan.AddField (SELF.sTableName, pFD.sName, pFD.iType, pFD.iSize1);
            // if new field has index, we create this index.
            if pFD.bIndexedField then
               IDB_DA.ADbItemsMan.AddIndex (SELF.sTableName, pFD.sName, true);
            CONTINUE;
         end;
         // Actions below will be executed when user has changed name, type or size of field.
         // I add temp field, write into it all data from the former field, delete the former field,
         // create new field with user parameters, write into it data from the temp field and delete
         // the temp field.
         // IMPORTANT - I don't know and cann't say if such converting will be successful or not.
         // if not - data in the field will be lost.
         if (pFD.sName <> pFD.sOldName) or
            (pFD.iType <> pFD.iOldType) or
            (pFD.iSize1 <> pFD.iOldSize1) then
         begin
            WorkWithField (SELF.sTableName, pFD.sOldName, pFD.sName, pFD.iType, pFD.iSize1);
            if pFD.bIndexedField then
               IDB_DA.ADbItemsMan.AddIndex (SELF.sTableName, pFD.sName, true);
         end;
         // Actions below is executed if user has changed necessity of present of index for a field.
         if pFD.bIndexedField <> pFD.bOldIndexedField then
            if pFD.bIndexedField then
               IDB_DA.ADbItemsMan.AddIndex (SELF.sTableName, pFD.sName, true)
            else
               IDB_DA.ADbItemsMan.DeleteIndex (SELF.sTableName, pFD.sName);
      end; // for iI end
   finally
// ++ Cadmensky Version 2.3.2
      IDB_DA.ADBItemsMan.EndOfCheckingTable;
// -- Cadmensky Version 2.3.2
      RestoreScreenCursor;
      ExecQuery.Free;
   end;
end;

{Find the field's descriptions in definition array.}

function TIDB_StructMan.FindFieldByName (sFieldName: AnsiString): Integer;
var
   iI: Integer;
   pFD: ^TrecFieldDefs;
begin
   RESULT := -1;
   sFieldName := AnsiUpperCase (sFieldName);
   for iI := 0 to SELF.listFieldsDefs.Count - 1 do
   begin
      pFD := listFieldsDefs[iI];
      if AnsiUpperCase (pFD.sName) = sFieldName then
      begin
         RESULT := iI;
         EXIT;
      end;
   end;
   if FieldIsInternalUsageField (sFieldName) {or FieldIsCalculatedField (sFieldName)} then
      //        RESULT := listFieldsDefs.Count;
      RESULT := listFieldsDefs.Count - 1; // Cadmensky
end;

{Move field from definition array into the array that contains data about fields to delete. A field is searched by it's name.}

procedure TIDB_StructMan.DeleteField (sFieldNameToBeDeleted: AnsiString);
var
   iP: Integer;
begin
   if AnsiUpperCase (sFieldNameToBeDeleted) = 'PROGISID' then EXIT;
   if AnsiUpperCase (sFieldNameToBeDeleted) = 'GUID' then EXIT;
   if FieldIsInternalUsageField (sFieldNameToBeDeleted) then EXIT;
   if FieldIsPresettedTypeField (sFieldNameToBeDeleted) then EXIT;
   iP := SELF.FindFieldByName (sFieldNameToBeDeleted);
   if iP > -1 then SELF.DeleteField (iP);
end;

{Move field from definition array into the array that contains data about fields to delete. A field is searched by it's position in the definition array.}

procedure TIDB_StructMan.DeleteField (iFieldPos: Integer);
var
   iL: Integer;
   pFD: ^TrecFieldDefs;
begin
   pFD := SELF.listFieldsDefs[iFieldPos];
   if not pFD.bTheFieldHasBeenJustAdded then
   begin
      iL := Length (arDeletedFields);
      SetLength (arDeletedFields, iL + 1);
      arDeletedFields[iL] := pFD.sName;
   end;
   // Delete definition of field.
   Dispose (pFD);
   listFieldsDefs.Delete (iFieldPos);
   SELF.SetModified;
end;

function TIDB_StructMan.GenerateFieldName (sBaseFieldName: AnsiString): AnsiString;
var
   iI: Integer;
begin
   RESULT := sBaseFieldName;
   iI := 1;
   while SELF.FindFieldByName (RESULT) > -1 do
   begin
      RESULT := sBaseFieldName + '_' + IntToStr (iI);
      Inc (iI);
   end;
end;

function TIDB_StructMan.AddNewField (sNewFieldName: AnsiString): Boolean;
var
   iP: Integer;
   pFD: ^TrecFieldDefs;
begin
   RESULT := FALSE;
   if sNewFieldName = '' then EXIT;
   // Check present of field with the same name.
   iP := SELF.FindFieldByName (sNewFieldName);
   if iP > -1 then
      // Field ''%s'' already exists.
      MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (119), [sNewFieldName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP)
   else
   begin
      // Add it.
      New (pFD);
      pFD.sName := sNewFieldName;
      pFD.sOldName := sNewFieldName;
      pFD.iType := 1; // String field type
      pFD.iSize1 := 50;
      pFD.bIndexedField := FALSE;
      pFD.bTheFieldHasBeenJustAdded := TRUE;
      listFieldsDefs.Add (pFD);

      RESULT := TRUE;
      SELF.SetModified;
   end;
end;

function TIDB_StructMan.AddNewField (sNewFieldName: AnsiString; iFieldType, iFieldSize: Integer): Boolean;
var
   iP: Integer;
   pFD: ^TrecFieldDefs;
begin
   RESULT := FALSE;
   if sNewFieldName = '' then EXIT;
   // Check present of field with the same name.
   iP := SELF.FindFieldByName (sNewFieldName);
   if iP > -1 then
   begin
      // Field ''%s'' alredy exists.
      MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (119), [sNewFieldName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
      EXIT;
   end;
   // Add it.
   New (pFD);
   pFD.sName := sNewFieldName;
   pFD.sOldName := sNewFieldName;
   pFD.iType := iFieldType;
   pFD.iSize1 := iFieldSize;
   pFD.bIndexedField := FALSE;
   listFieldsDefs.Add (pFD);
   pFD.bTheFieldHasBeenJustAdded := TRUE;
   RESULT := TRUE;
   SELF.SetModified;
end;

function TIDB_StructMan.ChangeFieldName (iFieldPos: Integer; sNewFieldName: AnsiString): Boolean;
var
   iP: Integer;
   pFD: ^TrecFieldDefs;
begin
   RESULT := FALSE;
   if Trim (sNewFieldName) = '' then
   begin
      // 397=Field name must not be empty
      MessageBox (Application.Handle, PChar (ALangManager.GetValue (397)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
      EXIT;
   end;
   iP := SELF.FindFieldByName (sNewFieldName);
   if iP = iFieldPos then EXIT;
   if (iP > -1) then
   begin
      //Field ''%s'' alredy exists.
      MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (119), [sNewFieldName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
      EXIT;
   end;
   // Now change field's name.
   try
      pFD := listFieldsDefs.Items[iFieldPos];
   except
      EXIT;
   end;
   // Change the field name if it is really required.
   if AnsiUpperCase (pFD.sName) <> AnsiUpperCase (sNewFieldName) then
   begin
      pFD.sName := sNewFieldName;
      SELF.SetModified;
   end;
   RESULT := TRUE;
end;

function TIDB_StructMan.ChangeFieldName (sOldFieldName: AnsiString; sNewFieldName: AnsiString): Boolean;
var
   iP: Integer;
begin
   RESULT := FALSE;
   iP := SELF.FindFieldByName (sOldFieldName);
   if iP > -1 then
      RESULT := SELF.ChangeFieldName (iP, sNewFieldName);
end;

function TIDB_StructMan.ChangeFieldTypeAndSize (iFieldPos, iFieldType, iFieldSize: Integer): Boolean;
var
   pFD: ^TrecFieldDefs;
begin
   RESULT := FALSE;
   try
      pFD := SELF.listFieldsDefs[iFieldPos];
   except
      EXIT;
   end;
   pFD.iType := iFieldType;
   pFD.iSize1 := iFieldSize;

   SELF.SetModified;
   RESULT := TRUE;
end;

function TIDB_StructMan.ChangeFieldTypeAndSize (sFieldName: AnsiString; iFieldType, iFieldSize: Integer): Boolean;
var
   iP: Integer;
begin
   RESULT := FALSE;
   if AnsiUpperCase (sFieldName) = 'PROGISID' then EXIT;
   if AnsiUpperCase (sFieldName) = 'GUID' then EXIT;
   if FieldIsInternalUsageField (sFieldName) then EXIT;
   if AnsiUpperCase (sFieldName) = 'GEOTEXTINFO' then
   begin
      if iFieldSize < 1 then EXIT;
      iP := SELF.FindFieldByName (sFieldName);
      if iP > -1 then
         RESULT := SELF.ChangeFieldTypeAndSize (iP, 1, iFieldSize);
   end
   else
   begin
      iP := SELF.FindFieldByName (sFieldName);
      if iP > -1 then
         RESULT := SELF.ChangeFieldTypeAndSize (iP, iFieldType, iFieldSize);
   end;
end;

procedure TIDB_StructMan.ChangeFieldSize (iFieldPos, iFieldSize: Integer);
var
   pFD: ^TrecFieldDefs;
begin
   if iFieldSize > 255 then iFieldSize := 255;
   try
      pFD := SELF.listFieldsDefs[iFieldPos];
   except
      EXIT;
   end;
   pFD.iSize1 := iFieldSize;
   SELF.SetModified;
end;

procedure TIDB_StructMan.ChangeFieldSize (sFieldName: AnsiString; iFieldSize: Integer);
var
   iP: Integer;
begin
   iP := SELF.FindFieldByName (sFieldName);
   if iP > -1 then
      SELF.ChangeFieldSize (iP, iFieldSize);
end;

procedure TIDB_StructMan.ChangeFieldIndex (iFieldPos: Integer; bFieldIsIndexed: Boolean);
var
   pFD: ^TrecFieldDefs;
begin
   try
      pFD := SELF.listFieldsDefs[iFieldPos];
   except
      EXIT;
   end;
   pFD.bIndexedField := bFieldIsIndexed;
   SELF.SetModified;
end;

procedure TIDB_StructMan.ChangeFieldIndex (sFieldName: AnsiString; bFieldIsIndexed: Boolean);
var
   iP: Integer;
begin
   if AnsiUpperCase (sFieldName) = 'PROGISID' then EXIT;
   if AnsiUpperCase (sFieldName) = 'GUID' then EXIT;
   iP := SELF.FindFieldByName (sFieldName);
   if iP > -1 then
      SELF.ChangeFieldIndex (iP, bFieldIsIndexed);
end;

function TIDB_StructMan.GetFieldCount: Integer;
begin
   RESULT := SELF.listFieldsDefs.Count;
end;

function TIDB_StructMan.GetFieldName (iFieldPos: Integer): AnsiString;
var
   pFD: ^TrecFieldDefs;
begin
   RESULT := '';
   try
      pFD := SELF.listFieldsDefs[iFieldPos];
   except
      EXIT;
   end;
   RESULT := pFD.sName;
end;

function TIDB_StructMan.GetFieldType (iFieldPos: Integer): Integer;
var
   pFD: ^TrecFieldDefs;
begin
   RESULT := -1;
   try
      pFD := SELF.listFieldsDefs[iFieldPos];
   except
      EXIT;
   end;
   RESULT := pFD.iType;
end;

function TIDB_StructMan.GetFieldType (sFieldName: AnsiString): Integer;
var
   iP: Integer;
begin
   RESULT := -1;
   iP := SELF.FindFieldByName (sFieldName);
   if iP > -1 then
      RESULT := GetFieldType (iP);
end;

function TIDB_StructMan.GetFieldSize (iFieldPos: Integer): Integer;
var
   pFD: ^TrecFieldDefs;
begin
   RESULT := 0;
   try
      pFD := SELF.listFieldsDefs[iFieldPos];
   except
      EXIT;
   end;
   RESULT := pFD.iSize1;
end;

function TIDB_StructMan.GetFieldSize (sFieldName: AnsiString): Integer;
var
   iP: Integer;
begin
   RESULT := 0;
   iP := SELF.FindFieldByName (sFieldName);
   if iP > -1 then
      RESULT := SELF.GetFieldSize (iP);
end;

function TIDB_StructMan.GetFieldIndexIsPresent (iFieldPos: Integer): Boolean;
var
   pFD: ^TrecFieldDefs;
begin
   RESULT := FALSE;
   try
      pFD := SELF.listFieldsDefs[iFieldPos];
   except
      EXIT;
   end;
   RESULT := pFD.bIndexedField;
end;

function TIDB_StructMan.GetFieldIndexIsPresent (sFieldName: AnsiString): Boolean;
var
   iP: Integer;
begin
   RESULT := FALSE;
   iP := SELF.FindFieldByName (sFieldName);
   if iP > -1 then
      RESULT := SELF.GetFieldIndexIsPresent (iP);
end;

function TIDB_StructMan.FieldExists (sFieldName: AnsiString): Boolean;
begin
   RESULT := SELF.FindFieldByName (sFieldName) > -1;
end;

procedure TIDB_StructMan.ApplyStructureToAllTables;

   function TheChangeCanBeMade (iNewType, iOldType: Integer): Boolean;
   begin
      { 1  - String
        2  - SmallInt
        3  - Integer
        5  - Boolean
        6  - Float
        7  - Currency
        11 - DateTime
        12 - Byte     }
         // Text field cannot be converted into nontext field.
      if ((iOldType = 1) and (iNewType <> 1)) then
         RESULT := FALSE
      else
         // Date field cannot be converted into other types exclude Text field.
         if (iOldType = 11) and ((iNewType <> 11) and (iNewType <> 1)) then
            RESULT := FALSE
         else
            // Any field could be converted into text field.
            if (iOldType <> 1) and (iNewType = 1) then
               RESULT := TRUE
            else
               // Float cannot converted into other numeric fields.
               if (iOldType = 6) and (iNewType <> 6) then
                  RESULT := FALSE
               else
                  // Integer field could be converted only into Float or Currency.
                  if (iOldType = 3) and (iNewType <> 6) and (iNewType <> 7) then
                     RESULT := FALSE
                  else
                     // SmallInt could be converted only into Integer, Float or Currency
                     if (iOldType = 2) and (iNewType <> 3) and (iNewType <> 6) and (iNewType <> 7) then
                        RESULT := FALSE
                           // Byte and Boolean could be coverted in every fomat.
                        // And I think the other cases are allowed.
                     else
                        RESULT := TRUE;
   end;

var
   AList: TStringList;
   iI, iJ: Integer;
   sTableName: AnsiString;
   AnExtraStructMan: TIDB_StructMan;
   ATable: TDTable;
   pFD: ^TrecFieldDefs;
   iNewType, iOldType: Integer;
   iNewSize, iOldSize: Integer;
begin
   AList := TStringList.Create;
   ATable := TDTable.Create (nil);
   ATable.Master := SELF.IDB_DA.DMaster;
   AnExtraStructMan := TIDB_StructMan.Create (AProject, SELF.IDB_DA);
   SetScreenCursor;
   try
      SELF.IDB_DA.ADbItemsMan.GetTablesNames (cs_AttributeTableNamePrefix, AList);
      for iI := 0 to AList.Count - 1 do
      begin
         sTableName := AList[iI];
         if AnsiUpperCase (sTableName) <> AnsiUpperCase (SELF.sTableName) then
         begin
            ATable.Close;
            ATable.TableName := sTableName;
            AnExtraStructMan.DetectStructure (ATable);
            for iJ := 0 to SELF.listFieldsDefs.Count - 1 do
            begin
               pFD := listFieldsDefs[iJ];
               iNewType := pFD.iType;
               iNewSize := pFD.iSize1;
               if not AnExtraStructMan.FieldExists (pFD.sName) then
                  // if there is not such field - simply add it to the table.
                  AnExtraStructMan.AddNewField (pFD.sName, iNewType, iNewSize)
               else
               begin
                  // if there is not such field, I must make some checks.
                  iOldType := AnExtraStructMan.GetFieldType (pFD.sName);
                  iOldSize := AnExtraStructMan.GetFieldSize (pFD.sName);
                  if (iNewType = iOldType) and (iNewSize <> iOldSize) then
                     AnExtraStructMan.ChangeFieldSize (pFD.sName, iNewSize)
                  else
                     if (iNewType <> iOldType) and TheChangeCanBeMade (iNewType, iOldType) then
                        AnExtraStructMan.ChangeFieldTypeAndSize (pFD.sName, iNewType, iNewSize);
               end;
               // Set indexed state.
               AnExtraStructMan.ChangeFieldIndex (pFD.sName, pFD.bIndexedField);
            end; // for iJ end
            AnExtraStructMan.DoChangeStructure;
         end;
      end; // for iI end
   finally
      RestoreScreenCursor;
      ATable.Close;
      ATable.Free;
      AnExtraStructMan.Free;
      AList.Clear;
      AList.Free;
   end;
end;

function TIDB_StructMan.FieldIsNumberType (sFieldName: AnsiString): Boolean;
var
   iFT: integer;
begin
   RESULT := false;
   iFT := SELF.GetFieldType (sFieldName);
   if iFT < 0 then
      exit; // This field was not found in the structure data.
{
1 - String
2 - SmallInt
3 - Integer
5 - Boolean
6 - Float
7 - Currency
11 - DateTime
12 - Byte
}
   case iFT of
      2..3, 6..7, 12: RESULT := true;
   end
end;

end.

