unit AM_Def;
{
Internal database. ver. II.
Object types for the UNDO manager.

These declarations were gotten from AM_Def.pas of WinGIS.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 04-06-2001
}
interface

Const
      ot_Pixel     = 1;
      ot_Poly      = 2;
      ot_CPoly     = 4;
      ot_Group     = 5;
      ot_Proj      = 6;
      ot_Text      = 7;
      ot_Symbol    = 8;
      ot_Spline    = 9;
      ot_Image     = 10;
      ot_MesLine   = 11;
      ot_Circle    = 12;
      ot_Arc       = 13;
      ot_OLEObj    = 14;
      ot_RText     = 16;
      ot_BusGraph  = 17;
      ot_Layer     = 18;

      mi_Text      =  000000000;
      mi_RText     =  090000000;
      mi_BusGraph  =  095000000;
      mi_MesLine   =  100000000;
      mi_Symbol    =  200000000;
      mi_Circle    =  250000000;
      mi_Arc       =  300000000;
      mi_Pixel     =  400000000;
      mi_Group     =  600000000;
      mi_ChartKind =  650000000;
      mi_Spline    =  700000000;
      mi_Poly      =  800000000;
      mi_CPoly     = 1000000000;
      mi_Image     = 1200000000;
      mi_OLEObj    = 1250000000;

implementation
begin
end.



