unit IDB_Monitor;
{
Internal database. Ver. II.
This module provides the functionality of monitoring of WinGIS.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 14-02-2001
}
interface

uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
   ExtCtrls, ComCtrls, Grids, Db, DBTables, ImgList, Buttons, Menus,
   Gauges, StdCtrls, WCtrls, TreeView,
   DMaster, DDB, DTables,
   IDB_C_StringGrid, IDB_C_Split, IDB_Grids, IDB_C_Toolbar,
   IDB_MonData, IDB_DataMan, IDB_LayerManager, IDB_Access2DB, IDB_TreeView,
   IDB_Messages, IDB_UIMans;

type
   TIDB_MonitorForm = class (TForm)
      LeftPanel: TPanel;
      RightPanel: TPanel;
      MonitorImageList: TImageList;
      MainMenu1: TMainMenu;
      File1: TMenuItem;
      MakeGridFromDataItemDataMenu: TMenuItem;
      Project1: TMenuItem;
      Edit1: TMenuItem;
      Cut1: TMenuItem;
      Copy1: TMenuItem;
      Paste1: TMenuItem;
      StringGrid1: TIDB_StringGrid;
      GridPopupMenu: TPopupMenu;
      CopyPopUp: TMenuItem;
      CutPopUp: TMenuItem;
      PastePopUp: TMenuItem;
      N1: TMenuItem;
      MakeGridFromDataPopUp: TMenuItem;
      N2: TMenuItem;
      N3: TMenuItem;
      EditItemEditMemu: TMenuItem;
      CancelChanges1: TMenuItem;
      SaveChanges1: TMenuItem;
      N4: TMenuItem;
      SaveChangesPopUp: TMenuItem;
      CancelChangesPopUp: TMenuItem;
      BottomPanel: TPanel;
      Gauge1: TGauge;
      StatusLabel: TLabel;
      ShowSelectedItem1: TMenuItem;
      ShowSelectedItemPopUp: TMenuItem;
      TreeViewPopupMenu: TPopupMenu;
      ShowSelectedItemTVPopup: TMenuItem;
      N5: TMenuItem;
      StayOnTopTVPopUp: TMenuItem;
      N6: TMenuItem;
      StayOnTopGridPopUp: TMenuItem;
      UpdateRecordsTVPopUp: TMenuItem;
      N8: TMenuItem;
      UpdateRecordsGridPopUp: TMenuItem;
      N7: TMenuItem;
      UpdateRecords1: TMenuItem;
      TreeListView1: TTreeListView;
      IDB_Splitter1: TIDB_Splitter;
      N9: TMenuItem;
      FieldsStatesGridPopUp: TMenuItem;
      ResizeGridCellGridPopUp: TMenuItem;
      IDB_Toolbar1: TIDB_Toolbar;
      ShowAttributeTableSB: TSpeedButton;
      TablePropertiesSB: TSpeedButton;
      ShowHideTreeViewSB: TSpeedButton;
      ShowSelectedItemSB: TSpeedButton;
      Image1: TImage;
      N11: TMenuItem;
      GatherSelectedItemsInProject1: TMenuItem;
      GatherSelectedItemsInProjectForCurrentLayer1: TMenuItem;
      UpdateRecordsInAllLayers1: TMenuItem;
      BackwardSB: TSpeedButton;
      ForwardSB: TSpeedButton;
      N12: TMenuItem;
      AlignVertical1: TMenuItem;
      AlignHorizontal1: TMenuItem;
      W1: TMenuItem;
      T1: TMenuItem;
      C1: TMenuItem;
    N10: TMenuItem;
    Nclose: TMenuItem;
      procedure FormClose (Sender: TObject; var Action: TCloseAction);
      procedure TreeView1MouseUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
      procedure StringGrid1DrawCell (Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
      procedure TreeView1KeyUp (Sender: TObject; var Key: Word; Shift: TShiftState);
      procedure ShowHideTreeViewSBClick (Sender: TObject);
      procedure ShowProjectMenuClick (Sender: TObject);
      procedure StringGrid1DblClick (Sender: TObject);
      procedure StringGrid1Click (Sender: TObject);
      procedure StringGrid1KeyUp (Sender: TObject; var Key: Word; Shift: TShiftState);
      procedure FormCreate (Sender: TObject);
      procedure StringGrid1MouseUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
      procedure SaveChangesSBClick (Sender: TObject);
      procedure CancelChangesSBClick (Sender: TObject);
      procedure EditSBClick (Sender: TObject);
      procedure Cut1Click (Sender: TObject);
      procedure Copy1Click (Sender: TObject);
      procedure Paste1Click (Sender: TObject);
      procedure Edit1Click (Sender: TObject);
      procedure GridPopupMenuPopup (Sender: TObject);
      procedure ShowSelectedItemSBClick (Sender: TObject);
      procedure TreeViewPopupMenuPopup (Sender: TObject);
      procedure FormResize (Sender: TObject);
      procedure StayOnTopTVPopUpClick (Sender: TObject);
      procedure UpdateRecordsTVPopUpClick (Sender: TObject);
      procedure C1Click (Sender: TObject);
      procedure T1Click (Sender: TObject);
      procedure FormActivate (Sender: TObject);
      procedure FormKeyPress (Sender: TObject; var Key: Char);
      procedure StringGrid1KeyPress (Sender: TObject; var Key: Char);
      procedure ShowHideTreeViewSBMouseMove (Sender: TObject; Shift: TShiftState; X, Y: Integer);
      procedure FieldsStatesGridPopUpClick (Sender: TObject);
      procedure IDB_Splitter1MouseUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
      procedure ResizeGridCellGridPopUpClick (Sender: TObject);
      procedure ShowAttributeTableSBClick (Sender: TObject);
      procedure CloseSBClick (Sender: TObject);
      procedure TablePropertiesSBClick (Sender: TObject);
      procedure IDB_Toolbar1MouseMove (Sender: TObject; Shift: TShiftState; X, Y: Integer);
      procedure GatherSelectedItemsInProject1Click (Sender: TObject);
      procedure Project1Click (Sender: TObject);
      procedure GatherSelectedItemsInProjectForCurrentLayer1Click (Sender: TObject);
      procedure UpdateRecordsInAllLayers1Click (Sender: TObject);
      procedure RightPanelCanResize (Sender: TObject; var NewWidth, NewHeight: Integer; var Resize: Boolean);
      procedure BackwardSBClick (Sender: TObject);
      procedure ForwardSBClick (Sender: TObject);
      procedure AlignHorizontalClick (Sender: TObject);
      procedure AlignVerticalClick (Sender: TObject);
      procedure FormDestroy (Sender: TObject);
      procedure StringGrid1MouseMove (Sender: TObject; Shift: TShiftState; X, Y: Integer);
      procedure TreeListView1MouseMove (Sender: TObject; Shift: TShiftState; X, Y: Integer);
      procedure IDB_Toolbar1Resize (Sender: TObject);
      procedure FormHide (Sender: TObject);
      procedure FormDeactivate (Sender: TObject);
      procedure IDB_Toolbar1Exit (Sender: TObject);
    procedure NcloseClick(Sender: TObject);
   private
      { Private declarations }
      ACurrentSpeedButtonUnderMouse: TSpeedButton;
      IDA: TIDB_Access2DB_Level2;
      // This variables are required to the operation that changes the state
      // of the window - MDI to SDI and back.
      iMyTop, iMyLeft, iMyHeight, iMyWidth: Integer;
      iHeightOfToolBarsOfMainMDIForm: Integer;

      ACurrRow: Integer;
      bItWasDblClick, bItWasSmpClick, bModified: Boolean;
      iCurrentProjectLayerID, iCurrentProjectObjectID: Integer;

      Data: TIDB_MonData;

      bIAmWorking: Boolean;
      bTheUserWantsToCancel: Boolean;

      {brovak}
      FHintWin: THintWindow;
      FHintX, FHintY: integer;
      {brovak}

      AToolbarManager: TIDB_ToolbarManager;

      procedure ShowDataForCurrentTreeViewItem (ndCurrent: TIDB_TreeEntry);
      // These procedures set and drop sign of modify state.
      procedure SetModified;
      procedure DropModified;
      // These procedures switch TStringGrid of monitoring form between browse and edit modes.
      // They also switch TSpeedButton on the upper panel on the monitoring form.
      procedure SwitchToEdit;
      procedure SwitchToBrowse;
      // These procedures switch on or off the buttons on the upper panel.
      // These procedures do the saving or the rollback of information in TStringGrid.
      procedure SaveMonitoringData;
      procedure ReloadMonitoringData;

      procedure Count_MDI_SDI_Parameters (X, Y: Integer);

      procedure OnWindowMenuItemClick (Sender: TObject);

      procedure ApplyFieldVisibilityChanges;
      procedure RefreshDataWindow (var AMsg: TMessage); message WM_IDB_Message;

      procedure RegisterToolbarButtons;

      procedure GatherSelectedItemsInProject (bMakeItForAllLayers: Boolean);
      procedure UpdateRecords (bAllLayers: Boolean);
      procedure WP (var WPMsg: TWMSysCommand); message WM_SYSCommand;
   public
      { Public declarations }
      AProject: Pointer;
      ALayerManager: TIDB_LayerManager;

      constructor Create (AOwner: TComponent; ALayerManager: TIDB_LayerManager; AProject: Pointer; ADBAccessObject: TIDB_Access2DB_Level2);

      procedure ClearItemsForMonitoring;
      procedure AddLayerItem (iLayerIndex: Integer; iLayerItemID: Integer; ADataAccessObject: TIDB_Access2DB_Level2);
      function MakeTreeFromItemsData: Integer;

      procedure MakeMyWindowState (AValue: TWindowState);

      procedure FillWindowMenu;

      procedure ResizeGridColumns;
      procedure DetectTreeVisible;

      procedure ShowStatus (sStatusCaption: AnsiString);
      procedure HideStatus;
      procedure SetTreeVisible (bTreeWillBeVisible: Boolean);
      procedure SetButtonsEnabled;
    {brovak}
      procedure CMMouseLeave (var Msg: TMessage); message CM_MOUSELEAVE;
      procedure CMMouseENTER (var Msg: TMessage); message CM_MOUSEENTER;
      procedure FormMove (var Msg: TMessage); message WM_MOVE;
    {brovak}

      property TheUserWantsToCancel: Boolean read bTheUserWantsToCancel;
   end;

implementation
{$R *.DFM}
uses ClipBrd,
   IDB_LanguageManager, IDB_ParamsOfUpdates, IDB_Consts,
   IDB_FieldsVisibilityMan, IDB_CallbacksDef, IDB_Utils,
   IDB_View, IDB_MainManager, Dialogs;

constructor TIDB_MonitorForm.Create (AOwner: TComponent; ALayerManager: TIDB_LayerManager; AProject: Pointer; ADBAccessObject: TIDB_Access2DB_Level2);
begin
   inherited Create (AOwner);
   SELF.ALayerManager := ALayerManager;
   SELF.AProject := AProject;
   SELF.IDA := ADBAccessObject;
   SELF.Data := TIDB_MonData.Create (AProject, SELF.ALayerManager, ADBAccessObject.DMaster);
   SELF.bItWasDblClick := FALSE;
   SELF.bItWasSmpClick := FALSE;
   SELF.iCurrentProjectLayerID := -1;
   SELF.iCurrentProjectObjectID := -1;
   SELF.bIAmWorking := FALSE;
   SELF.bTheUserWantsToCancel := FALSE;
end;

procedure TIDB_MonitorForm.FormClose (Sender: TObject; var Action: TCloseAction);
var
   i, j: integer;
begin
   if SELF.bModified then
      SELF.SaveMonitoringData;
   SELF.ClearItemsForMonitoring;
   //AnIDBMainMan.DetectMonitoringFormState (SELF.AProject, SELF);
// ++ Cadmensky
   SELF.Data.Free;

   for i := 0 to StringGrid1.ColCount - 1 do
      for j := 0 to StringGrid1.RowCount - 1 do
         if StringGrid1.Objects[i, j] <> nil then
            TComboBox (StringGrid1.Objects[i, j]).Free;
//   AnIDBMainMan.LastWasAttributeWindow[SELF.AProject] := false;
// -- Cadmensky
   Action := caFree;
end;

procedure TIDB_MonitorForm.ShowStatus (sStatusCaption: AnsiString);
begin
// ++ Cadmensky
   StatusLabel.Caption := sStatusCaption;
   Gauge1.Progress := 0;
//   BottomPanel.Visible := TRUE;
   Gauge1.Visible := true;
// -- Cadmensky
   Application.ProcessMessages;
   SELF.KeyPreview := TRUE;
   bIAmWorking := TRUE;
end;

procedure TIDB_MonitorForm.HideStatus;
begin
// ++ Cadmensky
   StatusLabel.Caption := Format (ALangManager.GetValue (304), [SELF.Data.GetAllItemsCount]);
//   BottomPanel.Visible := FALSE;
   Gauge1.Visible := false;
// -- Cadmensky
   SELF.KeyPreview := FALSE;
   bIAmWorking := FALSE;
   bTheUserWantsToCancel := FALSE;
end;

{ Clear array with items of layers of project. }

procedure TIDB_MonitorForm.ClearItemsForMonitoring;
var
   pItemInfo: ^TrecItemInfo;
   TI: TIDB_TreeEntry;
   iI: Integer;
begin
   TreeListView1.BeginUpdate;
   if Data.GetLayersCount = 1 then
   begin
      pItemInfo := TIDB_TreeEntry (TreeListView1.Tree.Items[0]).Data;
      Dispose (pItemInfo);
   end
   else
   begin
      iI := 0;
      while iI < TreeListView1.Tree.Count do
      begin
         TI := TIDB_TreeEntry (TreeListView1.Tree.Items[iI]);
         if TI.Level < 2 then
         begin
            pItemInfo := TI.Data;
            if pItemInfo <> nil then Dispose (pItemInfo);
         end;
         Inc (iI);
      end;
   end;

// ++ Cadmensky
   TreeListView1.Tree.FreeAll;
// TreeListView1.Tree.Clear;
// -- Cadmensky
   TreeListView1.EndUpdate;
   SELF.Data.ClearData;
end;

procedure TIDB_MonitorForm.SetModified;
begin
   SELF.bModified := TRUE;
end;

procedure TIDB_MonitorForm.DropModified;
begin
   SELF.bModified := FALSE;
   SELF.SwitchToBrowse;
end;

procedure TIDB_MonitorForm.SwitchToEdit;
begin
   StringGrid1.Options := StringGrid1.Options + [goEditing, goAlwaysShowEditor] - [goRowSelect];
   SELF.SetModified;
end;

procedure TIDB_MonitorForm.SwitchToBrowse;
begin
   StringGrid1.Options := StringGrid1.Options - [goEditing, goAlwaysShowEditor] + [goRowSelect];
end;

{Add new layer's item in objects for monitoring.}

procedure TIDB_MonitorForm.AddLayerItem (iLayerIndex: Integer; iLayerItemID: Integer; ADataAccessObject: TIDB_Access2DB_Level2);
begin
   SELF.Data.AddLayerItem (iLayerIndex, iLayerItemID, ADataAccessObject);
end;

{Transmit data from array to imaging in tree.}

function TIDB_MonitorForm.MakeTreeFromItemsData: Integer;
var
   iI: Integer;
begin
   // Save all menu items' enable state and make all these items as disabled.
   for iI := 0 to SELF.ComponentCount - 1 do
      if SELF.Components[iI] is TMenuItem then
      begin
         TMenuItem (SELF.Components[iI]).Tag := Integer (TMenuItem (SELF.Components[iI]).Enabled);
         TMenuItem (SELF.Components[iI]).Enabled := FALSE;
      end;
   SELF.StringGrid1.Enabled := FALSE;
   SELF.IDB_Toolbar1.Enabled := FALSE;
   Gauge1.Progress := 0;
   SELF.ShowStatus ('');
   try
      SELF.Data.MakeTreeViewRepresentation (TreeListView1, @bTheUserWantsToCancel, Gauge1, StatusLabel);
   finally
      SELF.HideStatus;
      SELF.StringGrid1.Enabled := TRUE;
      SELF.IDB_Toolbar1.Enabled := TRUE;
      // Restore state of all menu items.
      for iI := 0 to SELF.ComponentCount - 1 do
         if SELF.Components[iI] is TMenuItem then
            TMenuItem (SELF.Components[iI]).Enabled := Boolean (TMenuItem (SELF.Components[iI]).Tag);
   end;

   RESULT := SELF.Data.GetAllItemsCount;
end;

{When user clicks on tree's item, we should put in TStringGrid all data for this layer's item
from database or we should write on right part of window name of layer.}

procedure TIDB_MonitorForm.TreeView1MouseUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
   ndCurrent: TIDB_TreeEntry;
begin
   ndCurrent := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]);
   if ndCurrent <> nil then
      SELF.ShowDataForCurrentTreeViewItem (ndCurrent);
   if Button = mbRight then
   begin
      if SELF.FormStyle = fsMDIChild then
         SELF.Count_MDI_SDI_Parameters (X, Y);
   end;
   SetButtonsEnabled;
end;

procedure TIDB_MonitorForm.StringGrid1DrawCell (Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
   sText: AnsiString;
   CB: TComboBox;
begin
   if StringGrid1.Objects[ACol, ARow] <> nil then
   begin
      CB := TComboBox (StringGrid1.Objects[ACol, ARow]);
      CB.Top := StringGrid1.Top + StringGrid1.ClientRect.Top + Rect.Top;
      CB.Left := StringGrid1.Left + Rect.Left + 1;
      if Rect.Right < StringGrid1.ClientWidth then
         CB.Width := StringGrid1.Left + Rect.Right - Rect.Left + 1
      else
         CB.Width := StringGrid1.ClientWidth - StringGrid1.Left - CB.Left + 2;
      CB.Height := Rect.Bottom - Rect.Top;
   end;

   //     if gdFixed in State then EXIT;
   if gdSelected in State then
      StringGrid1.Canvas.Brush.Color := clYellow
   else
      StringGrid1.Canvas.Brush.Color := clWhite;
   if gdFixed in State then //EXIT;
      StringGrid1.Canvas.Brush.Color := clBtnFace;
   StringGrid1.Canvas.Font.Color := clBlack;
   if StringGrid1.Objects[ACol, ARow] = nil then
   begin
      sText := StringGrid1.Cells[ACol, ARow];
      ExtTextOut (StringGrid1.Canvas.Handle, Rect.Left + 2, Rect.Top + 2, ETO_CLIPPED or ETO_OPAQUE, @Rect, PChar (sText), Length (sText), nil);
   end;
end;

procedure TIDB_MonitorForm.ShowDataForCurrentTreeViewItem (ndCurrent: TIDB_TreeEntry);
var
   iLayerIndex, iLayerItemID: Integer;
   pItemInfo: ^TrecItemInfo;
   iL: Integer;
   SelTI: TIDB_TreeEntry;
begin
   Screen.Cursor := crHourGlass;
   if SELF.bModified then
   begin
      SELF.SaveMonitoringData;
      SELF.DropModified;
   end;
   iL := Data.GetLayersCount;
   if iL = 0 then
      EXIT;
   SelTI := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]);
   if iL = 1 then
   begin
      pItemInfo := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[0]).Data;
      iLayerIndex := pItemInfo.iLayerIndexToThatTheItemBelongs;
      if SelTI = TreeListView1.Tree.ExpandedItems[0] then
         iLayerItemID := pItemInfo.iItemID
      else
         iLayerItemID := Integer (SelTI.Data);
   end
   else
   begin
      if ndCurrent.Level < 2 then
      begin
// ++ Cadmensky Version 2.3.9
         if not SELF.LeftPanel.Visible then
         begin
            if not ndCurrent.Expanded then
            begin
               ndCurrent.Expanded := true;
               TreeListView1.UpdateView;
            end;
            TreeListView1.ItemIndex := TreeListView1.ItemIndex + 1;
            ShowDataForCurrentTreeViewItem (TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]));
            Exit;
         end;
// -- Cadmensky Version 2.3.9
         // if user clicked on layer (root) item of TTreeView, we should hide TStringGrid
         // and show the name of selected layer on right panel.
         StringGrid1.Visible := FALSE;

         AToolbarManager.ButtonEnabled['ShowSelectedItem'] := FALSE;
         SELF.ShowSelectedItem1.Enabled := FALSE;

         PItemInfo := ndCurrent.Data;
         iLayerIndex := pItemInfo.iLayerIndexToThatTheItemBelongs;

         // Layer ''%s''. Selected items count: %d
         RightPanel.Caption := Format (ALangManager.GetValue (177), [Data.GetLayerNameByIndex (iLayerIndex), ndCurrent.Count]);
// ++ Cadmensky IDB Version 2.3.6
         Screen.Cursor := crDefault;
// -- Cadmensky IDB Version 2.3.6
         EXIT; // In this case we only show layer's name and nothing more.
      end
      else
      begin
         pItemInfo := TIDB_TreeEntry (ndCurrent.Parent).Data;
         iLayerIndex := pItemInfo.iLayerIndexToThatTheItemBelongs;
         iLayerItemID := Integer (ndCurrent.Data); //pItemInfo.iItemID;
      end;
   end;

// ++ Cadmensky
   if iLayerIndex <> SELF.iCurrentProjectLayerID then
      SELF.Data.OpenDataForLayer (iLayerIndex);
// -- Cadmensky
   SELF.iCurrentProjectLayerID := iLayerIndex;
   SELF.iCurrentProjectObjectID := iLayerItemID;
   if not SELF.Data.GetDataFromDatabase (SELF.iCurrentProjectLayerID, SELF.iCurrentProjectObjectID, StringGrid1) then
   begin
      // if we have some problems with data in the database, I hide right part of the form, show
      // a message about absence data and hide editor buttons.
      SELF.StringGrid1.Visible := FALSE;
      // No data about object with ProgisID = %d
      RightPanel.Caption := Format (ALangManager.GetValue (178), [iCurrentProjectObjectID]);
      AToolbarManager.ButtonEnabled['ShowSelectedItem'] := TRUE;
      SELF.ShowSelectedItem1.Enabled := TRUE;
// ++ Cadmensky IDB Version 2.3.6
      Screen.Cursor := crDefault;
// -- Cadmensky IDB Version 2.3.6
      EXIT;
   end
   else
   begin
      AToolbarManager.ButtonEnabled['ShowSelectedItem'] := TRUE;
      SELF.ShowSelectedItem1.Enabled := TRUE;
   end;
   SetButtonsEnabled;
   Screen.Cursor := crDefault;
end;

procedure TIDB_MonitorForm.TreeView1KeyUp (Sender: TObject; var Key: Word; Shift: TShiftState);
begin
   if (Key = vk_Up) or (Key = vk_Down)
      or (Key = vk_Prior) or (Key = vk_Next)
      or (Key = vk_Home) or (Key = vk_End) then
      SELF.ShowDataForCurrentTreeViewItem (TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]));
end;

procedure TIDB_MonitorForm.ShowHideTreeViewSBClick (Sender: TObject);
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
   LeftPanel.Visible := AToolbarManager.ButtonDown['ShowHideTreeView'];
   // if it is not visible - show it.
   if (LeftPanel.Visible) and (LeftPanel.Width < 10) then
      IDB_Splitter1.DblClick;
   if AToolbarManager.ButtonDown['ShowHideTreeView'] then
      AToolbarManager.ButtonHint['ShowHideTreeView'] := ALangManager.GetValue (372) // 372=Hide objects treeview
   else
      AToolbarManager.ButtonHint['ShowHideTreeView'] := ALangManager.GetValue (373); // 373=Show objects treeview
end;

procedure TIDB_MonitorForm.ShowProjectMenuClick (Sender: TObject);
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
   ShowProjectProc (SELF.AProject);
end;

procedure TIDB_MonitorForm.StringGrid1DblClick (Sender: TObject);
begin
   SELF.bItWasDblClick := TRUE;
end;

procedure TIDB_MonitorForm.StringGrid1Click (Sender: TObject);
begin
   SELF.bItWasSmpClick := TRUE;
end;

procedure TIDB_MonitorForm.StringGrid1KeyUp (Sender: TObject; var Key: Word; Shift: TShiftState);
begin
   case Key of
      vk_Up, vk_Down, vk_Prior, vk_Next, vk_Return: SELF.SwitchToBrowse;
   else
      if (goEditing in StringGrid1.Options) then SELF.SetModified;
   end;
end;

procedure TIDB_MonitorForm.FormCreate (Sender: TObject);
begin
   SELF.ACurrRow := -1;
   TreeListView1.AddBitmaps (Image1.Picture.Bitmap, 0);
   ALangManager.MakeReplacement (SELF);

   SELF.AToolbarManager := TIDB_ToolbarManager.Create (SELF);
   SELF.RegisterToolbarButtons;
   IDB_Toolbar1.RepositionButtons;
   IDB_Toolbar1.Invalidate;

   {brovak}
   FHintWin := THintWindow.Create (Self);
   {brovak}
//   StayOnTopTVPopUpClick (SENDER);
end;

procedure TIDB_MonitorForm.Count_MDI_SDI_Parameters (X, Y: Integer);
begin
   // Count the coordinates of the upper-left corner of the window in the ablsolute screen coordinates.
   // then count the height of the WinGIS window toolbars.
   iMyTop := Mouse.CursorPos.Y - Y - (SELF.Height - SELF.ClientHeight - GetSystemMetrics (SM_CYFRAME));
   iMyLeft := Mouse.CursorPos.X - X - GetSystemMetrics (SM_CXFRAME);
   iHeightOfToolBarsOfMainMDIForm := iMyTop - SELF.Top - AWinGISMainForm.Top;
end;

procedure TIDB_MonitorForm.StringGrid1MouseUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
   ACol, ARow: Integer;
   i, j: integer;
   sFieldName: AnsiString;
begin
   StringGrid1.MouseToCell (X, Y, ACol, ARow);
   try
      for j := 0 to StringGrid1.ColCount do
         for i := 0 to StringGrid1.RowCount do
         begin
            if StringGrid1.Objects[j, i] <> nil then
               if StringGrid1.ColWidths[j] + TComboBox (StringGrid1.Objects[j, i]).Left <= StringGrid1.Width then
               begin
                  if TComboBox (StringGrid1.Objects[j, i]).Width >= StringGrid1.ColWidths[j] then
                     TComboBox (StringGrid1.Objects[j, i]).Width := StringGrid1.ColWidths[j];
               end
               else
                  TComboBox (StringGrid1.Objects[j, i]).Width := StringGrid1.Width - TComboBox (StringGrid1.Objects[j, i]).Left;
         end;
   except
   end;

   if Button = mbRight then
   begin
      if SELF.FormStyle = fsMDIChild then
      begin
         SELF.Count_MDI_SDI_Parameters (X, Y);
         EXIT;
      end;
   end;
   if SELF.bItWasDblClick then
   begin
      bItWasDblClick := FALSE;
      bItWasSmpClick := FALSE;
      StringGrid1.MouseToCell (X, Y, ACol, ARow);
// ++ Cadmensky Version 2.3.6
      if ARow < 0 then
         Exit;
// -- Cadmensky Version 2.3.6
      sFieldName := StringGrid1.Cells[0, ARow];
      if (ACol > 0) and (AnsiUpperCase (sFieldName) <> 'PROGISID')
                    and (AnsiUpperCase (sFieldName) <> 'GUID')
                    and (not FieldIsCalculatedField (sFieldName)) then
      begin
           SELF.SwitchToEdit;
           ACurrRow := ARow;
      end;
   end;
   if SELF.bItWasSmpClick then
   begin
      bItWasSmpClick := FALSE;
      StringGrid1.MouseToCell (X, Y, ACol, ARow);
      if ACol = 0 then
         StringGrid1.Row := ARow;
      if StringGrid1.Row <> ACurrRow then
         SELF.SwitchToBrowse;
   end;
end;

procedure TIDB_MonitorForm.SaveMonitoringData;
begin
   SELF.Data.SetDataToDatabase (SELF.iCurrentProjectLayerID, SELF.iCurrentProjectObjectID, StringGrid1);
   SELF.DropModified;
   AnIDBMainMan.RefreshAllDataWindows (SELF.AProject);
end;

procedure TIDB_MonitorForm.ReloadMonitoringData;
var
   SelTI: TIDB_TreeEntry;
begin
   SelTI := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]);
   if SelTI <> nil then
      SELF.ShowDataForCurrentTreeViewItem (SelTI);
end;

procedure TIDB_MonitorForm.SaveChangesSBClick (Sender: TObject);
var
   pItemInfo: ^TrecItemInfo;
   iCurrentRowNumber: Integer;
   SelTI: TIDB_TreeEntry;
begin
   if not SELF.bModified then EXIT;
   // Put changes in the attribute table.
   SaveMonitoringData;

   // if we show in the right part of the monitoring window data not from 'ProgisID' field
   // but from another field, we have to correct information in this part.

   // Which field I show data in the TTreeView from?
   SelTI := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]);
   if SelTI.Level < 2 then
      pItemInfo := SelTI.Data
   else
      pItemInfo := TIDB_TreeEntry (SelTI.Parent).Data;
   iCurrentRowNumber := StringGrid1.Row;
   if SelTI.Level < 2 then
      SELF.Data.GetDataFromDatabase (pItemInfo.iLayerIndexToThatTheItemBelongs, pItemInfo.iItemID, StringGrid1)
   else
      SELF.Data.GetDataFromDatabase (pItemInfo.iLayerIndexToThatTheItemBelongs, Integer (SelTI.Data), StringGrid1);

   StringGrid1.Row := iCurrentRowNumber;

   DropModified;
end;

procedure TIDB_MonitorForm.CancelChangesSBClick (Sender: TObject);
begin
   // do you want to reload data?
   if MessageBox (Handle, PChar (ALangManager.GetValue (180)), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrNO then EXIT;
   SELF.DropModified;
   SELF.ReloadMonitoringData;
end;

procedure TIDB_MonitorForm.EditSBClick (Sender: TObject);
var
   sFieldName: AnsiString;
begin
   sFieldName := StringGrid1.Cells[0, StringGrid1.Row];
   if (AnsiUpperCase (sFieldName) <> 'PROGISID') and (AnsiUpperCase (sFieldName) <> 'GUID') and (not FieldIsCalculatedField (sFieldName)) then
      SELF.SwitchToEdit;
end;

{These 3 procedures do editing operations such as CopyToClipboard, CutToClipboard, PasteFromClipboard.}

procedure TIDB_MonitorForm.Cut1Click (Sender: TObject);
begin
   if StringGrid1.InplaceEditor <> nil then
      SendMessage (StringGrid1.InplaceEditor.Handle, WM_CUT, 0, 0);
end;

procedure TIDB_MonitorForm.Copy1Click (Sender: TObject);
begin
   if StringGrid1.InplaceEditor <> nil then
      SendMessage (StringGrid1.InplaceEditor.Handle, WM_COPY, 0, 0);
end;

procedure TIDB_MonitorForm.Paste1Click (Sender: TObject);
begin
   if StringGrid1.InplaceEditor <> nil then
      SendMessage (StringGrid1.InplaceEditor.Handle, WM_PASTE, 0, 0);
end;

procedure TIDB_MonitorForm.Edit1Click (Sender: TObject);
begin
   if not (goEditing in StringGrid1.Options) then
   begin
      Copy1.Enabled := FALSE;
      CopyPopUp.Enabled := FALSE;
      Cut1.Enabled := FALSE;
      CutPopUp.Enabled := FALSE;
      Paste1.Enabled := FALSE;
      PastePopUp.Enabled := FALSE;
   end
   else
   begin
      Copy1.Enabled := TRUE;
      CopyPopUp.Enabled := TRUE;
      Cut1.Enabled := TRUE;
      CutPopUp.Enabled := TRUE;
      if Clipboard.HasFormat (CF_TEXT) then
      begin
         Paste1.Enabled := TRUE;
         PastePopUp.Enabled := TRUE;
      end
      else
      begin
         Paste1.Enabled := FALSE;
         PastePopUp.Enabled := FALSE;
      end;
   end;
   UpdateRecordsInAllLayers1.Visible := Data.GetLayersCount > 1;
end;

procedure TIDB_MonitorForm.GridPopupMenuPopup (Sender: TObject);
begin
   SELF.Edit1.Click;
   ResizeGridCellGridPopUp.Visible := StringGrid1.HorzSplitterPosition > -1;
end;

{ This procedure shows selected item (in TreeView) with zoom it. Only one item can be selected.}

procedure TIDB_MonitorForm.ShowSelectedItemSBClick (Sender: TObject);
var
   pItemInfo: ^TrecItemInfo;
   iItemID: Integer;
   SelTI: TIDB_TreeEntry;
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
   SelTI := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]);
   if SelTI = nil then EXIT;
   if SELF.Data.GetLayersCount = 1 then
   begin
      pItemInfo := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[0]).Data;
      if SelTI = TreeListView1.Tree.ExpandedItems[0] then
         iItemID := pItemInfo.iItemID
      else
         iItemID := Integer (SelTI.Data);
   end
   else
   begin
      pItemInfo := TIDB_TreeEntry (SelTI.Parent).Data;
      iItemID := Integer (SelTI.Data);
   end;

   if pItemInfo = nil then EXIT;
   ShowItemInProjectWithZoomProc (SELF.AProject, // << The project pointer
      GetLayerPointerByIndexFunc (SELF.AProject, pItemInfo.iLayerIndexToThatTheItemBelongs), // << The layer pointer
      iItemID); // The item to be shown.
end;

procedure TIDB_MonitorForm.TreeViewPopupMenuPopup (Sender: TObject);
var
   iItemID: Integer;
   pItemInfo: ^TrecItemInfo;
   SelTI: TIDB_TreeEntry;
begin
   SelTI := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]);
   if SelTI = nil then EXIT;
   if SELF.Data.GetLayersCount = 1 then
   begin
      if SelTI = TreeListView1.Tree.ExpandedItems[0] then
      begin
         pItemInfo := SelTI.Data;
         if pItemInfo = nil then EXIT;
         iItemID := pItemInfo.iItemID;
      end
      else
         iItemID := Integer (SelTI.Data);
   end
   else
      if SelTI.Level = 0 then
         iItemID := -1
      else
         iItemID := Integer (SelTI.Data);
   // if pItemInfo.iItemID = -1, it means the user has selected the node of TTreeView that contains data
   // about layer but not about the item. This situation can appear when we have selected items from
   // some layers at one time.
   ShowSelectedItemTVPopup.Visible := iItemID <> -1;
end;

procedure TIDB_MonitorForm.FormResize (Sender: TObject);
begin
   // Change the progress bar width.
{   Gauge1.Width := SELF.ClientWidth - Gauge1.Left - 5;
   Application.ProcessMessages;}
end;

procedure TIDB_MonitorForm.StayOnTopTVPopUpClick (Sender: TObject);
var
   AState: TWindowState;
begin
// ++ Cadmensky Version 2.3.8
{   if SELF.WindowState <> wsMaximized then
   begin
      iMyHeight := SELF.Height;
      iMyWidth := SELF.Width;
   end;}
// -- Cadmensky Version 2.3.8

   if SELF.FormStyle = fsMDIChild then
   begin
      // Make it as 'Stay on top'.
      SELF.FormStyle := fsStayOnTop;
//      SELF.BorderStyle := bsSizeToolWin;
      SELF.BorderStyle := bsSizeable;

      SELF.Top := iMyTop;
      SELF.Left := iMyLeft;
      SELF.Width := iMyWidth;
      SELF.Height := iMyHeight;

      StayOnTopTVPopUp.Checked := TRUE;
      StayOnTopGridPopUp.Checked := TRUE;
      W1.Visible := FALSE;
      AToolbarManager.ButtonVisible['ShowProject'] := FALSE;
//      ShowProjectMenu.Enabled := FALSE;

{      StayOnTopSB.Glyph.LoadFromResourceName (HInstance, 'MAXIMIZE_IT');
      AToolbarManager.ButtonHint['StayOnTop'] := ALangManager.GetValue (406); // 406=Maximize}

      IDB_Toolbar1.RepositionButtons;
   end
   else
   begin
      // Make it as not 'Stay on top' but as 'Maximized'.
      AState := GetMDIChildWindowStateFunc;

      iMyTop := SELF.Top;
      iMyLeft := SELF.Left;
// ++ Cadmensky Version 2.3.8
      iMyHeight := SELF.Height;
      iMyWidth := SELF.Width;
// -- Cadmensky Version 2.3.8

      W1.Visible := TRUE;
      SELF.FormStyle := fsMDIChild;
      SELF.BorderStyle := bsSizeable;
      (*
              SELF.Top := iMyAbsTop - (AWinGISMainForm.Top + iHeightOfToolBarsOfMainMDIForm);
              if SELF.Top < 0 then SELF.Top := 0;
              SELF.Left := iMyAbsLeft - AWinGISMainForm.Left - AWinGISMainForm.BorderWidth - 6; // I don't know how this number can be counted.
              if SELF.Left < 0 then SELF.Left := 0;
              SELF.Width := iMyWidth;
              SELF.Height := iMyHeight;
      *)
      SELF.WindowState := wsMaximized;

      StayOnTopTVPopUp.Checked := FALSE;
      StayOnTopGridPopUp.Checked := FALSE;
//      ShowProjectMenu.Enabled := TRUE;
      AToolbarManager.ButtonVisible['ShowProject'] := TRUE;

{      StayOnTopSB.Glyph.LoadFromResourceName (HInstance, 'RESTORE_IT');
      AToolbarManager.ButtonHint['StayOnTop'] := ALangManager.GetValue (407); // 407=Restore}

      IDB_Toolbar1.RepositionButtons;
   end;
end;

procedure TIDB_MonitorForm.UpdateRecords (bAllLayers: Boolean);

   function DoDialog (iLayerIndex: Integer; var sSelectedFieldName: AnsiString; var sEnteredValue: AnsiString): Boolean;
   var
      UF: TIDB_UpdateForm;
      ALayer: Pointer;
   begin
      RESULT := FALSE;
      sSelectedFieldName := '';
      sEnteredValue := '';
      // Show the replacing parameters dialog.
      UF := TIDB_UpdateForm.Create (AWinGISMainForm, SELF.IDA, SELF.AProject, iLayerIndex);
      ALayer := GetLayerPointerByIndexFunc (SELF.AProject, iLayerIndex);
      UF.Caption := Format (ALangManager.GetValue (56), [GetLayerNameFunc (ALayer)]);
      UF.DeleteConditionsSheet;
      // Put all field's names in ComboBox except the field with name 'ProgisID'.
      // This field cannot be updated.
      UF.FieldNamesComboBox.Items.Text := AnIDBMainMan.GetTableFieldsNamesList (AProject, ALayer, FALSE);
      if UF.FieldNamesComboBox.Items.Count < 1 then
      begin
         MessageBox (Handle, PChar (ALangManager.GetValue (274)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
         EXIT;
      end;

      UF.FieldNamesComboBox.ItemIndex := 0;
      RESULT := UF.ShowModal = mrOK;
      if RESULT then
      begin
         sSelectedFieldName := UF.FieldNamesComboBox.Items[UF.FieldNamesComboBox.ItemIndex];
         sEnteredValue := UF.GenerateClause (false); // UF.Edit1.Text;
      end;
      UF.Free;
   end;

var
   iLayersCount: Integer;
   pItemInfo: ^TrecItemInfo;
   sFieldName, sValue: AnsiString;
   AnOldCursor: TCursor;
   SelTI: TIDB_TreeEntry;
   iI: Integer;
   ATI: TIDB_TreeEntry;
begin
   iLayersCount := SELF.Data.GetLayersCount;
   if iLayersCount = 0 then EXIT;

   SelTI := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]);
   if SELF.Data.GetLayersCount = 1 then
      pItemInfo := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[0]).Data
   else
      if SelTI.Level = 1 then
         pItemInfo := SelTI.Data
      else
         pItemInfo := TIDB_TreeEntry (SelTI.Parent).Data;

   if not DoDialog (pItemInfo.iLayerIndexToThatTheItemBelongs, sFieldName, sValue) then EXIT;
   if Trim (sFieldName) = '' then EXIT;

   AnOldCursor := Screen.Cursor;
   Screen.Cursor := crHourGlass;

   SELF.Gauge1.Progress := 0;
   SELF.ShowStatus (ALangManager.GetValue (277));
   try
      if iLayersCount = 1 then
      begin
         pItemInfo := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[0]).Data;
         SELF.Data.UpdateDataInOneField (pItemInfo.iLayerIndexToThatTheItemBelongs,
            sFieldName,
            sValue,
            @bTheUserWantsToCancel,
            SELF.Gauge1);
      end
      else
      begin
         if not bAllLayers then
         begin
            // if there are more than 1 layer, a layer as well as an item can be selected in TTreeView.
            if SelTI.Level = 1 then // The layer has been selected in TTreeView.
               pItemInfo := SelTI.Data
            else // The item has been selected in TTreeView.
               pItemInfo := TIDB_TreeEntry (SelTI.Parent).Data;
            SELF.Data.UpdateDataInOneField (pItemInfo.iLayerIndexToThatTheItemBelongs,
               sFieldName,
               sValue,
               @bTheUserWantsToCancel,
               SELF.Gauge1);
         end
         else
            for iI := 0 to TreeListView1.Tree.ExpandedCount - 1 do
            begin
               ATI := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[iI]);
               if ATI.Level = 1 then
               begin
                  pItemInfo := ATI.Data;
                  SELF.Data.UpdateDataInOneField (pItemInfo.iLayerIndexToThatTheItemBelongs,
                     sFieldName,
                     sValue,
                     @bTheUserWantsToCancel,
                     SELF.Gauge1);
               end;
            end; // for iI end
      end;
      AnIDBMainMan.RefreshAllDataWindows (SELF.AProject);

      // Display the corrected data in TStringGrid
      SELF.ShowDataForCurrentTreeViewItem (SelTI);
   finally
      SELF.HideStatus;
      Screen.Cursor := AnOldCursor;
   end;
end;

{ This procedure fill one of the attribute fields with a value that will be selected
  by the user. The filling will be applied obly to objects that are monitored and
  only for one layer. }

procedure TIDB_MonitorForm.UpdateRecordsTVPopUpClick (Sender: TObject);
begin
   SELF.UpdateRecords (FALSE);
end;

procedure TIDB_MonitorForm.UpdateRecordsInAllLayers1Click (Sender: TObject);
begin
   UpdateRecords (TRUE);
end;

procedure TIDB_MonitorForm.C1Click (Sender: TObject);
begin
   AWinGISMainForm.Cascade;
end;

procedure TIDB_MonitorForm.T1Click (Sender: TObject);
begin
   AWinGISMainForm.Tile;
end;

procedure TIDB_MonitorForm.FillWindowMenu;
var
   iI: Integer;
   MI: TMenuItem;
   AForm: TForm;
begin
   // Delete all menu of existed windows.
   while W1.Count > 3 do
      W1.Delete (3);
   // Create new menu for existed windows.
   for iI := 0 to AWinGISMainForm.MDIChildCount - 1 do
   begin
      AForm := AWinGISMainForm.MDIChildren[iI];
      MI := TMenuItem.Create (SELF);
      MI.Caption := IntToStr (iI + 1) + ' ' + AForm.Caption;
      if AForm = SELF then MI.Checked := TRUE;
      MI.Tag := Integer (AForm); // Pointer to the window will be required to show
      // this window when the user clicks on the menu item
      // with name of this window in 'Window' menu.
      MI.OnClick := SELF.OnWindowMenuItemClick;
      W1.Add (MI);
   end;
   SetButtonsEnabled;
end;

procedure TIDB_MonitorForm.ResizeGridColumns;
var
   iI: Integer;
begin
   for iI := 0 to StringGrid1.ColCount - 1 do
      StringGrid1.ResizeColumn (iI);
end;

{ This procedure shows the window from 'Window' menu. The pointer to the window
  is storaged in TAG property of menu item. }

procedure TIDB_MonitorForm.OnWindowMenuItemClick (Sender: TObject);
begin
   TForm (TMenuItem (SENDER).Tag).Show;
end;

procedure TIDB_MonitorForm.FormActivate (Sender: TObject);
begin
   FillWindowMenu;
end;

procedure TIDB_MonitorForm.FormKeyPress (Sender: TObject; var Key: Char);
begin
   if (Key = #27) and bIAmWorking then
      // 306=do You want to cancel this operation?
      if MessageBox (Handle, PChar (ALangManager.GetValue (306)), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrYes then
      begin
         bTheUserWantsToCancel := TRUE; // ESC key has been pressed and the user has agreed to cancel an operation.
         Application.ProcessMessages;
      end;
end;

procedure TIDB_MonitorForm.StringGrid1KeyPress (Sender: TObject; var Key: Char);
var
   pItemInfo: ^TrecItemInfo;
   ndSelected: TIDB_TreeEntry;
   iFieldType: Integer;
begin
   if (SELF.StringGrid1.InplaceEditor <> nil)
      and
      (SELF.StringGrid1.InplaceEditor.Visible) then
   begin
      // Now the user is editing data of monitored objects and we have to allow this to him
      // according to type of data field.
      if Data.GetLayersCount = 1 then
         pItemInfo := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[0]).Data
      else
      begin
         ndSelected := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]);
         if ndSelected.Level < 2 then
            pItemInfo := ndSelected.Data
         else
            pItemInfo := TIDB_TreeEntry (ndSelected.Parent).Data;
      end;
      if Pos ('_', StringGrid1.Cells[0, StringGrid1.Row]) <= 1 then
      begin
         iFieldType := Data.GetFieldType (pItemInfo.iLayerIndexToThatTheItemBelongs, StringGrid1.Cells[0, StringGrid1.Row]);
         case iFieldType of
            2, // SmallInt
               3, // Integer
               4, // Word
               6, // Float
               7, // Currency
               12, // Byte
               25: // LargeInt
               begin
                  // for non-String field we can use only number values.
                  if (Key < '0') or (Key > '9') then Key := #0;
               end;
            1, // Text
               24: // WideString
               begin
                  // for String field we can write all. We need not to correct the user.
               end;
         else
            begin
               // for all other field types we cannot write anything.
               Key := #0;
            end;
         end; // Case end
      end;
   end;
end;

procedure TIDB_MonitorForm.ShowHideTreeViewSBMouseMove (Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
   HintString: Ansistring;
   NeedRect: TRect;
   ClickPoint: Tpoint;
begin
   {brovak}
   if ACurrentSpeedButtonUnderMouse <> TSpeedButton (SENDER) then
   begin
      FHintX := X;
      FHintY := Y;
      if Assigned (FHintWin) then FHintWin.ReleaseHandle;
   end;
   {brovak}

   if not (Sender is TSpeedButton) then
   begin
      if Assigned (FHintWin) then FHintWin.ReleaseHandle;
      EXIT;
   end;

   if (ACurrentSpeedButtonUnderMouse <> nil) and (ACurrentSpeedButtonUnderMouse <> SENDER) then
      ACurrentSpeedButtonUnderMouse.Perform (CM_MOUSELEAVE, 0, 0);
   ACurrentSpeedButtonUnderMouse := TSpeedButton (SENDER);

   HintString := TSpeedButton (ACurrentSpeedButtonUnderMouse).Hint;
   if {(} (HintString <> '') { and (Self.FormStyle = fsStayOnTop)) } (*and (OldButton<>ACurrentSpeedButtonUnderMouse)*) then
   begin
      //  FHintWin.Canvas.Font:=TSpeedButton(ACurrentSpeedButtonUnderMouse).Hint.

      NeedRect := FHintWin.CalcHintRect (100, HintString, nil);
      GetCursorPos (ClickPoint);
      NeedRect.Left := ClickPoint.X + ACurrentSpeedButtonUnderMouse.Width div 3;
      NeedRect.Top := ClickPoint.Y + 3 * ACurrentSpeedButtonUnderMouse.Height div 4;
      NeedRect.Right := NeedRect.Left + Canvas.TextWidth (HintString) + 11;
      NeedRect.Bottom := NeedRect.Top + Canvas.TextHeight (HintString) + 4;
      FHintWin.Color := clInfoBk;
      FHintWin.ActivateHint (NeedRect, HintString);

   end
   else
      if Assigned (FHintWin) then FHintWin.ReleaseHandle;
   {brovak}
end;

procedure TIDB_MonitorForm.DetectTreeVisible;
var
   bTreeWillBeVisible: Boolean;
begin
   bTreeWillBeVisible := (Data.GetLayersCount > 1) or (TreeListView1.Tree.AllCount > 1);
   AToolbarManager.ButtonDown['ShowHideTreeView'] := bTreeWillBeVisible;
   ShowHideTreeViewSBClick (SELF);
end;

procedure TIDB_MonitorForm.SetTreeVisible (bTreeWillBeVisible: Boolean);
begin
   AToolbarManager.ButtonDown['ShowHideTreeView'] := bTreeWillBeVisible;
   ShowHideTreeViewSBClick (SELF);
end;

procedure TIDB_MonitorForm.FieldsStatesGridPopUpClick (Sender: TObject);
begin
   TablePropertiesSB.Click;
end;

procedure TIDB_MonitorForm.IDB_Splitter1MouseUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
   if LeftPanel.ClientWidth < 2 then
      AToolbarManager.ButtonDown['ShowHideTreeView'] := FALSE
   else
   begin
      AToolbarManager.ButtonDown['ShowHideTreeView'] := TRUE;
      TreeListView1.Invalidate;
   end;
end;

procedure TIDB_MonitorForm.ResizeGridCellGridPopUpClick (Sender: TObject);
begin
   StringGrid1.ResizeColumn (StringGrid1.HorzSplitterPosition);
end;

procedure TIDB_MonitorForm.ApplyFieldVisibilityChanges;
begin
   SELF.ShowDataForCurrentTreeViewItem (TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]));
end;

procedure TIDB_MonitorForm.RefreshDataWindow (var AMsg: TMessage);
begin
   case AMsg.WParam of
      IDB_RefreshDataWindow:
         case AMsg.LParam of
            1: SELF.ApplyFieldVisibilityChanges;
         end;
   end; // Case end
end;

procedure TIDB_MonitorForm.ShowAttributeTableSBClick (Sender: TObject);
var
   SelTI: TIDB_TreeEntry;
   iL: Integer;
   pItemInfo: ^TrecItemInfo;
   iLayerIndex: Integer;
   AForm: TForm;
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
   iL := Data.GetLayersCount;
   if iL = 0 then EXIT;
   SelTI := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]);
   if iL = 1 then
   begin
      pItemInfo := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[0]).Data;
      iLayerIndex := pItemInfo.iLayerIndexToThatTheItemBelongs;
   end
   else
   begin
      if SelTI.Level < 2 then
      begin
         pItemInfo := SelTI.Data;
         iLayerIndex := pItemInfo.iLayerIndexToThatTheItemBelongs;
      end
      else
      begin
         pItemInfo := TIDB_TreeEntry (SelTI.Parent).Data;
         iLayerIndex := pItemInfo.iLayerIndexToThatTheItemBelongs;
      end;
   end;
   // Show the attribute table at the same place when the monitoring window is situated
   // with the same window state and close the monitoring window.
   AForm := SELF.ALayerManager.ShowAttributeWindow (GetLayerPointerByIndexFunc (SELF.AProject, iLayerIndex), nil);
{   if AForm <> nil then
   begin
      if SELF.WindowState <> wsMaximized then
      begin
         AForm.Top := SELF.Top;
         AForm.Left := SELF.Left;
         AForm.Width := SELF.Width;
         AForm.Height := SELF.Height;
      end;
      TIDB_ViewForm (AForm).MakeMyWindowState (SELF.WindowState);
      SELF.Close;
   end;}
end;

procedure TIDB_MonitorForm.MakeMyWindowState (AValue: TWindowState);
begin
   if SELF.WindowState <> AValue then
      StayOnTopTVPopUpClick (SELF);
end;

procedure TIDB_MonitorForm.WP (var WPMsg: TWMSysCommand);
begin
   if (WPMsg.CmdType = SC_RESTORE) or
      (WPMsg.CmdType = SC_MAXIMIZE) then
      Self.StayOnTopTVPopUpClick (Self)
   else
      inherited;
end;

procedure TIDB_MonitorForm.CloseSBClick (Sender: TObject);
begin
   SELF.Close;
end;

procedure TIDB_MonitorForm.TablePropertiesSBClick (Sender: TObject);
var
   SelTI: TIDB_TreeEntry;
   iL: Integer;
   pItemInfo: ^TrecItemInfo;
   iLayerIndex: Integer;
   iT, iW, iH: Integer;
   ALayer: Pointer;
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
   iL := Data.GetLayersCount;
   if iL = 0 then
      EXIT;
   SelTI := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]);
   if iL = 1 then
   begin
      pItemInfo := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[0]).Data;
      iLayerIndex := pItemInfo.iLayerIndexToThatTheItemBelongs;
   end
   else
   begin
      if SelTI.Level < 2 then
      begin
         pItemInfo := SelTI.Data;
         iLayerIndex := pItemInfo.iLayerIndexToThatTheItemBelongs;
      end
      else
      begin
         pItemInfo := TIDB_TreeEntry (SelTI.Parent).Data;
         iLayerIndex := pItemInfo.iLayerIndexToThatTheItemBelongs;
      end;
   end;
   if SELF.WindowState <> wsMaximized then
   begin
      iT := SELF.Top;
      iL := SELF.Left;
      iH := SELF.Height;
      iW := SELF.Width;
      SELF.Hide;
   end
   else
   begin
      iT := -1;
      iL := -1;
      iW := -1;
      iH := -1;
   end;
   ALayer := GetLayerPointerByIndexFunc (SELF.AProject, iLayerIndex);
   /////
   if SELF.ALayerManager.ShowTableStructure (ALayer, iT, iL, iW, iH, 1) then
   begin
// ++ Cadmensky Version 2.3.9
      SELF.Data.OpenDataForLayer (iLayerIndex);
// -- Cadmensky Version 2.3.9
      SELF.ShowDataForCurrentTreeViewItem (SelTI);
      // There is no need in the next line because now we have the only IDB window - either attribute window or monitoring window.
      //        ALayerManager.ApplyFieldsVisibilityChanges(ALayer);
   end;
   /////
   if SELF.WindowState <> wsMaximized then
   begin
      SELF.Top := iT;
      SELF.Left := iL;
      SELF.Height := iH;
      SELF.Width := iW;
      SELF.Show;
   end;
end;

procedure TIDB_MonitorForm.IDB_Toolbar1MouseMove (Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
   ACurrentSpeedButtonUnderMouse.Perform (CM_MOUSELEAVE, 0, 0);
end;

procedure TIDB_MonitorForm.RegisterToolbarButtons;
begin
   IDB_Toolbar1.AddButton (ShowAttributeTableSB);
   IDB_Toolbar1.AddButton (TablePropertiesSB);
   IDB_Toolbar1.AddButton (tbSeparator);
   IDB_Toolbar1.AddButton (ShowHideTreeViewSB);
   IDB_Toolbar1.AddButton (tbSeparator);
//   IDB_Toolbar1.AddButton (ShowProjectSB);
   IDB_Toolbar1.AddButton (ShowSelectedItemSB);
   IDB_Toolbar1.AddButton (tbSeparator);
   IDB_Toolbar1.AddButton (BackwardSB);
   IDB_Toolbar1.AddButton (ForwardSB);
//   IDB_Toolbar1.AddButton (tbSeparator);
//   IDB_Toolbar1.AddButton (StayOnTopSB);       
//   IDB_Toolbar1.AddButton (CloseSB);

   AToolbarManager.RegisterButton ('ShowAttributeTable', ShowAttributeTableSB);
   AToolbarManager.RegisterButton ('TableProperties', TablePropertiesSB);
   AToolbarManager.RegisterButton ('ShowHideTreeView', ShowHideTreeViewSB);
//   AToolbarManager.RegisterButton ('ShowProject', ShowProjectSB);
   AToolbarManager.RegisterButton ('ShowSelectedItems', ShowSelectedItemSB);
//   AToolbarManager.RegisterButton ('StayOnTop', StayOnTopSB);
   AToolbarManager.RegisterButton ('Backward', BackwardSB);
   AToolbarManager.RegisterButton ('Forward', ForwardSB);
end;

procedure TIDB_MonitorForm.GatherSelectedItemsInProject (bMakeItForAllLayers: Boolean);
var
   iI, iJ, iL: Integer;
   pItemInfo: ^TrecItemInfo;
   CornerTI, TI, SelectedTI: TIDB_TreeEntry;
   iLayerIndex: Integer;
   iItemID: Integer;
   iPosition: Integer;
   AList: TList;
   ALayer: Pointer;
begin
   iL := SELF.Data.GetLayersCount;
   if iL = 0 then EXIT;
   AList := TList.Create;
   if iL = 1 then
   begin
      TI := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[0]);
      pItemInfo := TI.Data;
      iLayerIndex := pItemInfo.iLayerIndexToThatTheItemBelongs;
      iItemID := pItemInfo.iItemID;

      ALayer := GetLayerPointerByIndexFunc (AProject, iLayerIndex);

      iPosition := AnIDBMainMan.GetReadyToInsertRecords (AProject, ALayer, AList);
      try
         // ++ Cadmensky
         AnIDBMainMan.InsertRecord (AProject, ALayer, iItemID);
         //           AnIDBMainMan.InsertRecord(iPosition, iItemID);
         // -- Cadmensky
         for iI := 1 to TreeListView1.Tree.AllCount - 1 do
         begin
            TI := TIDB_TreeEntry (TreeListView1.Tree.Items[iI]);
            iItemID := Integer (TI.Data);
            // ++ Cadmensky
            AnIDBMainMan.InsertRecord (AProject, ALayer, iItemID);
            //           AnIDBMainMan.InsertRecord(iPosition, iItemID);
            // -- Cadmensky
         end; // for iI end
      finally
         // Commented by Cadmensky
         //           AnIDBMainMan.EndInserting(iPosition);
      end;
   end
   else
   begin
      if bMakeItForAllLayers then
      begin
         iI := 0;
         while iI < TreeListView1.Tree.ExpandedCount do
         begin
            CornerTI := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[iI]);
            if CornerTI.Level < 2 then
            begin
               pItemInfo := CornerTI.Data;
               iLayerIndex := pItemInfo.iLayerIndexToThatTheItemBelongs;
               ALayer := GetLayerPointerByIndexFunc (AProject, iLayerIndex);
               iPosition := AnIDBMainMan.GetReadyToInsertRecords (AProject, ALayer, AList);
               try
                  for iJ := 0 to CornerTI.Count - 1 do
                  begin
                     TI := TIDB_TreeEntry (CornerTI.Items[iJ]);
                     iItemID := Integer (TI.Data);
                     // ++ Cadmensky
                     AnIDBMainMan.InsertRecord (AProject, ALayer, iItemID);
                     //                         AnIDBMainMan.InsertRecord(iPosition, iItemID);
                     // -- Cadmensky
                  end; // for iJ end
               finally
                  // Commented by Cadmensky
                  //                       AnIDBMainMan.EndInserting(iPosition);
               end;
            end;
            Inc (iI);
         end; // while end
      end
      else
      begin
         SelectedTI := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]);
         if SelectedTI.Level > 1 then
            CornerTI := TIDB_TreeEntry (SelectedTI.Parent)
         else
            CornerTI := SelectedTI;
         pItemInfo := TIDB_TreeEntry (CornerTI).Data;
         iLayerIndex := pItemInfo.iLayerIndexToThatTheItemBelongs;
         ALayer := GetLayerPointerByIndexFunc (AProject, iLayerIndex);
         iPosition := AnIDBMainMan.GetReadyToInsertRecords (AProject, ALayer, AList);
         try
            for iI := 0 to CornerTI.Count - 1 do
            begin
               TI := TIDB_TreeEntry (CornerTI.Items[iI]);
               iItemID := Integer (TI.Data);
               // ++ Cadmensky
               AnIDBMainMan.InsertRecord (AProject, ALayer, iItemID);
               //                  AnIDBMainMan.InsertRecord(iPosition, iItemID);
               // -- Cadmensky
            end;
         finally
            // Commented by Cadmensky
            //              AnIDBMainMan.EndInserting(iPosition);
         end;
      end;
   end;
end;

procedure TIDB_MonitorForm.GatherSelectedItemsInProject1Click (Sender: TObject);
begin
   SELF.GatherSelectedItemsInProject (TRUE);
end;

procedure TIDB_MonitorForm.GatherSelectedItemsInProjectForCurrentLayer1Click (Sender: TObject);
begin
   GatherSelectedItemsInProject (FALSE);
   SELF.ALayerManager.RefreshAllDataWindows;
end;

procedure TIDB_MonitorForm.Project1Click (Sender: TObject);
begin
   if Data.GetLayersCount > 1 then
   begin
      GatherSelectedItemsInProjectForCurrentLayer1.Visible := TRUE;
      GatherSelectedItemsInProject1.Caption := ALangManager.GetValue (411); // 411=Insert selected layer items into the tables of all layers
   end
   else
   begin
      GatherSelectedItemsInProjectForCurrentLayer1.Visible := FALSE;
      GatherSelectedItemsInProject1.Caption := ALangManager.GetValue (276); // 276=Insert selected layer items into the table
   end;
end;

procedure TIDB_MonitorForm.RightPanelCanResize (Sender: TObject; var NewWidth, NewHeight: Integer; var Resize: Boolean);
var //ACol, ARow: Integer;
   i, j: integer;
//   sFieldName: AnsiString;
   CB: TComboBox;
begin
   try
      for j := 0 to StringGrid1.ColCount do
         for i := 0 to StringGrid1.RowCount do
         begin
            CB := TComboBox (StringGrid1.Objects[j, i]);
            if CB <> nil then
               if StringGrid1.ColWidths[j] + CB.Left > StringGrid1.ClientWidth then
                  CB.Width := StringGrid1.ClientWidth - CB.Left + 4;
         end;
   except
   end;
end;

procedure TIDB_MonitorForm.BackwardSBClick (Sender: TObject);
var
   ndCurrent: TIDB_TreeEntry;
begin
   if TreeListView1.ItemIndex > 0 then
   begin
      TreeListView1.ItemIndex := TreeListView1.ItemIndex - 1;
      ndCurrent := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]);
      if (ndCurrent.Level = 1) and (Data.GetLayersCount > 1) then
      begin
         if not ndCurrent.Expanded then
         begin
            ndCurrent.Expanded := true;
            TreeListView1.UpdateView;
            TreeListView1.ItemIndex := TreeListView1.ItemIndex + ndCurrent.Count + 1;
         end;
         BackwardSBClick (nil);
         exit;
      end;
      if ndCurrent <> nil then
         SELF.ShowDataForCurrentTreeViewItem (ndCurrent);
   end;
   SetButtonsEnabled;
end;

procedure TIDB_MonitorForm.ForwardSBClick (Sender: TObject);
var
   ndCurrent: TIDB_TreeEntry;
begin
   ndCurrent := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]);
   if (ndCurrent.Level = 1) and (Data.GetLayersCount > 1) then
      ndCurrent.Expanded := true;
   Application.ProcessMessages;
   TreeListView1.UpdateView;

   if TreeListView1.ItemIndex < TreeListView1.Items.Count - 1 then
   begin
      TreeListView1.ItemIndex := TreeListView1.ItemIndex + 1;
      ndCurrent := TIDB_TreeEntry (TreeListView1.Tree.ExpandedItems[TreeListView1.ItemIndex]);
      if (ndCurrent.Level = 1) and (Data.GetLayersCount > 1) then
      begin
         ForwardSBClick (nil);
         exit;
      end;
      if ndCurrent <> nil then
         SELF.ShowDataForCurrentTreeViewItem (ndCurrent);
   end;
   SetButtonsEnabled;
end;

procedure TIDB_MonitorForm.SetButtonsEnabled;
begin
   ForwardSB.Enabled := (TreeListView1.ItemIndex < TreeListView1.Items.Count - 1);
// ++ Cadmensky Version 2.3.9
   if Data.GetLayersCount > 1 then
      BackwardSB.Enabled := TreeListView1.ItemIndex > 1
   else
// -- Cadmensky Version 2.3.9
      BackwardSB.Enabled := TreeListView1.ItemIndex > 0;
end;

procedure TIDB_MonitorForm.AlignVerticalClick (Sender: TObject);
var
   iLeft: Integer;
begin
   if (AWinGISMainForm.WindowState = wsMaximized) or
      (AWinGISMainForm.Width > Screen.DesktopWidth - 100) then
// ++ Cadmensky Version 2.3.9
   begin
      AWinGISMainForm.WindowState := wsNormal;
      AWinGISMainForm.Width := Round (Screen.DesktopWidth * 0.6);
//      exit;
   end;
// -- Cadmensky Version 2.3.9

   AWinGISMainForm.Align := alLeft;
   AWinGISMainForm.Align := alNone;

   iLeft := AWinGISMainForm.Left + AWinGISMainForm.Width;

   SELF.Left := iLeft;
   SELF.Width := Screen.DesktopWidth - iLeft;
   SELF.Top := AWinGISMainForm.Top;
   SELF.Height := AWinGISMainForm.Height;
end;

procedure TIDB_MonitorForm.AlignHorizontalClick (Sender: TObject);
var
   iBottom: Integer;
begin
   if (AWinGISMainForm.WindowState = wsMaximized) or
      (AWinGISMainForm.Height > Screen.DesktopHeight - 100) then
// ++ Cadmensky Version 2.3.9
   begin
      AWinGISMainForm.WindowState := wsNormal;
      AWinGISMainForm.Height := Round (Screen.DesktopHeight * 0.7);
//      exit;
   end;
// -- Cadmensky Version 2.3.9

   SELF.Height := Screen.DesktopHeight - AWinGISMainForm.Height;
   SELF.Align := alBottom;
   iBottom := SELF.Height + SELF.Top;

   SELF.Height := iBottom - AWinGISMainForm.Height;

   SELF.Align := alNone;

   AWinGISMainForm.Left := SELF.Left;
   AWinGISMainForm.Width := SELF.Width;
   AWinGISMainForm.Top := Screen.DesktopTop;
end;

procedure TIDB_MonitorForm.CMMouseLeave (var Msg: TMessage);
begin
   if Assigned (FHintWin) then
      FHintWin.ReleaseHandle;
   FHintX := -1;
   FHintY := -1;
//   FHintCol := -1;
//   FHintRow := -1;
   inherited;
end;

procedure TIDB_MonitorForm.CMMouseENTER (var Msg: TMessage);
begin
   if Assigned (FHintWin) then
      FHintWin.ReleaseHandle;
   inherited;
end;

procedure TIDB_MonitorForm.FormMove (var Msg: TMessage);
begin
   if Assigned (FHintWin) then
      FHintWin.ReleaseHandle;
   inherited;
end;

procedure TIDB_MonitorForm.FormDestroy (Sender: TObject);
begin
// ++ Cadmensky
   AToolbarManager.Free;
   AnIDBMainMan.AMonitoringForm := nil;
// -- Cadmensky
{brovak}
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
   FHintWin.Free;
{brovak}
end;

procedure TIDB_MonitorForm.StringGrid1MouseMove (Sender: TObject;
   Shift: TShiftState; X, Y: Integer);
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
end;

procedure TIDB_MonitorForm.TreeListView1MouseMove (Sender: TObject;
   Shift: TShiftState; X, Y: Integer);
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
end;

procedure TIDB_MonitorForm.IDB_Toolbar1Resize (Sender: TObject);
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
end;

procedure TIDB_MonitorForm.FormHide (Sender: TObject);
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
end;

procedure TIDB_MonitorForm.FormDeactivate (Sender: TObject);
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
end;

procedure TIDB_MonitorForm.IDB_Toolbar1Exit (Sender: TObject);
begin
   if Assigned (FHintWin) then FHintWin.ReleaseHandle;
end;

procedure TIDB_MonitorForm.NcloseClick(Sender: TObject);
begin
close;
end;

end.

