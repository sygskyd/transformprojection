unit IDB_SelectInListBox;
{
Internal database. Ver. II.
This form is purposed for selecting by user some data in TListBox.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 10-08-2000

23.08.2000
Now I use this form in the following places:
1. For selecting which attribure table the user wants to see.
2. For selecting which fields in a data source will be used as base for adding
   new fields before the loading from external data source operation.   
}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Menus, ExtCtrls;

type
  TIDB_SelectInListBoxForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    CancelButton: TButton;
    OKButton: TButton;
    Label1: TLabel;
    ListBox1: TListBox;
    PopupMenu1: TPopupMenu;
    SelectAllPopUp: TMenuItem;
    DeselectAllPopUp: TMenuItem;
    InvertSelectionPopUp: TMenuItem;
    procedure ListBox1DblClick(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure SelectAllPopUpClick(Sender: TObject);
    procedure DeselectAllPopUpClick(Sender: TObject);
    procedure InvertSelectionPopUpClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

Uses
     IDB_LanguageManager;

{$R *.DFM}

procedure TIDB_SelectInListBoxForm.ListBox1DblClick(Sender: TObject);
begin
     SELF.OKButton.Click;
end;

procedure TIDB_SelectInListBoxForm.ListBox1Click(Sender: TObject);
begin
     If ListBox1.MultiSelect Then
        OKButton.Enabled := ListBox1.SelCount > 0;
end;

procedure TIDB_SelectInListBoxForm.SelectAllPopUpClick(Sender: TObject);
Var
   iI: Integer;
begin
     For iI:=0 To SELF.ListBox1.Items.Count-1 Do
         ListBox1.Selected[iI] := TRUE;
end;

procedure TIDB_SelectInListBoxForm.DeselectAllPopUpClick(Sender: TObject);
Var
   iI: Integer;
begin
     For iI:=0 To SELF.ListBox1.Items.Count-1 Do
         ListBox1.Selected[iI] := FALSE;
end;

procedure TIDB_SelectInListBoxForm.InvertSelectionPopUpClick(Sender: TObject);
Var
   iI: Integer;
begin
     For iI:=0 To SELF.ListBox1.Items.Count-1 Do
         ListBox1.Selected[iI] := NOT ListBox1.Selected[iI];
end;

procedure TIDB_SelectInListBoxForm.FormCreate(Sender: TObject);
begin
     ALangManager.MakeReplacement(SELF);
end;

end.
