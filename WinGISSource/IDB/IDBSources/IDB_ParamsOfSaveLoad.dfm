�
 TIDB_PARAMSOFSAVELOADFORM 0g  TPF0TIDB_ParamsOfSaveLoadFormIDB_ParamsOfSaveLoadFormLeftTop� BorderStylebsDialogCaption!66ClientHeightkClientWidthcColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoDesktopCenterScaledOnCreate
FormCreate	OnDestroyFormDestroyOnShowFormShowPixelsPerInch`
TextHeight TGaugeGauge1Left�TopUWidth"HeightAnchorsakRightakBottom 	ForeColorclBlueProgress2ShowTextVisible  TPageControlPageControl1Left Top WidthaHeightE
ActivePage	TabSheet2TabOrder  	TTabSheet	TabSheet1Caption!67 	TGroupBox	GroupBox1TagLeft
Top
Width� Height`Caption!68Ctl3D	ParentCtl3DTabOrder  	TComboBoxDelimTypeComboBoxTagLeft_TopWidthjHeightStylecsDropDownList
ItemHeightItems.StringsSemicolon (;)	Comma (,)TABSpace TabOrder OnChangeDelimTypeComboBoxChange  TEdit	DelimEditTagLeft_Top9WidthjHeight
AutoSelectEnabledTabOrderOnChangeDelimEditChange  TRadioButtonRadioButton1TagLeft
TopWidthPHeightCaption!69Checked	TabOrderTabStop	OnClickRadioButton1Click  TRadioButtonRadioButton2TagLeft
Top<WidthPHeightCaption!70TabOrderOnClickRadioButton2Click   	TCheckBox#UseTheFirstLineAsFieldNamesCheckBoxTagLeft
ToprWidthXHeightCaption!64TabOrderOnClick(UseTheFirstLineAsFieldNamesCheckBoxClick  	TCheckBoxScanFileCheckBoxTagLeft
Top� WidthXHeightCaption!71TabOrderOnClickScanFileCheckBoxClick  	TCheckBoxRewriteDataCheckBoxLeft
Top� WidthXHeightCaption!72TabOrderOnClickRewriteDataCheckBoxClick  	TCheckBoxInsertRecordCheckBoxLeft
Top� WidthXHeightCaption!73Checked	State	cbCheckedTabOrderOnClickInsertRecordCheckBoxClick  	TCheckBoxUseConvertingCheckBoxLeft
Top� WidthXHeightCaption!74TabOrderVisibleOnClickUseConvertingCheckBoxClick   	TTabSheet	TabSheet2Caption!75
ImageIndex TPanelPanel1Left Top WidthYHeight.AlignalTop
BevelInner	bvLowered
BevelOuterbvNoneTabOrder  TLabelLabel1LeftBTopWidthHeightCaption!523  TSpeedButtonAddFieldsSBLeftTopWidthHeightHint!83Flat	
Glyph.Data
�   �   BM�       v   (               p                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 3333333 3333333 3333333 330 333 330�333 330�333 3  � 3 3
���3 3  � 3 330�333 330�333 330 333 3333333 3333333 ParentShowHintShowHint	OnClickAddFieldsSBClick  TLabelLabel2LeftTopWidthUHeightAutoSizeCaption!113Transparent	  TLabelLabel3LeftfTopWidthHeightCaption!522  	TComboBoxProgisIDSourceColumnComboBoxLeft@TopWidth� HeightStylecsDropDownList
ItemHeightTabOrder OnChange"ProgisIDSourceColumnComboBoxChange  	TComboBoxLinkIDComboBoxLeftdTopWidth� HeightStylecsDropDownList
ItemHeightTabOrderOnChangeLinkIDComboBoxChange   TIDB_StringGridStringGrid1Left Top.WidthYHeight� AlignalClientColCountDefaultRowHeight	FixedCols RowCount	PopupMenu
PopupMenu1
ScrollBarsssNoneTabOrder
OnDrawCellStringGrid1DrawCellOnGetEditTextStringGrid1GetEditTextOnSelectCellStringGrid1SelectCellOnSetEditTextStringGrid1SetEditTextOnTopLeftChangedStringGrid1TopLeftChangedAllowMovingFromToTheFirstLine	AllowMovingFromToTheLastLine		ColWidths-�   	TComboBoxSourceComboBoxLeft4TophWidth%HeightStylecsDropDownList
ItemHeightSorted	TabOrderOnChangeSourceComboBoxChangeOnClickSourceComboBoxClick   	TTabSheet	TabSheet3Caption!75
ImageIndex TSpeedButtonSpeedButton1LeftTop(WidthHeightCaption>Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontOnClickSpeedButton1Click  TSpeedButtonSpeedButton2LeftTopAWidthHeightCaption>>Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontOnClickSpeedButton2Click  TSpeedButtonSpeedButton3LeftTopZWidthHeightCaption<Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontOnClickSpeedButton3Click  TSpeedButtonSpeedButton4LeftTopsWidthHeightCaption<<Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontOnClickSpeedButton4Click  TListBoxAvailableFieldsListBoxLeftTopWidthHeight
ItemHeightTabOrder   TListBoxSelectedFieldsListBoxLeft<TopWidthHeight
ItemHeightTabOrder    TButtonOKButtonLeft�TopKWidthKHeightAnchorsakRightakBottom Caption!49ModalResultTabOrderOnClickOKButtonClick  TButtonCancelButtonLeftTopKWidthKHeightAnchorsakRightakBottom Cancel	Caption!50ModalResultTabOrder  
TPopupMenu
PopupMenu1Left� Top�  	TMenuItem
Add_PUMenuCaption!113OnClickAdd_PUMenuClick  	TMenuItemDelete_PUMenuCaption!114OnClickDelete_PUMenuClick    