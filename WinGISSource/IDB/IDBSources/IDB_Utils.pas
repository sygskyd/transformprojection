unit IDB_Utils;
{
Internal database. Ver. II.
This module provides some functions that are useful for Internal database.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 01-02-2001
}
interface

uses DB, StdCtrls, Classes, Messages,
   IDB_Access2DB, IDB_DatabaseItemsManager, IDB_LayerManager, IDB_PolygonOverlayManager, DTables, IDB_MainManager,
   IDB_CallbacksDef;

function MakeDBFileNameFromProjectFileName (sProjectFileName: AnsiString): AnsiString;
function MakeTempDBFileName (sReallyFileName: AnsiString): AnsiString;
function GetFileNameWithoutExt (sFileName: AnsiString): AnsiString;
function ReplaceChars (sValue: AnsiString; chOldChar: Char = ' '; chNewChar: Char = '_'): AnsiString;
function DeleteChars (sValue: AnsiString; sCharsToBeDeleted: AnsiString): AnsiString;

function SaveTempDatabaseFile (AProject: Pointer): AnsiString;

function CheckFieldTypeSuitability (AFieldType: TFieldType): Boolean; Overload;
function CheckFieldTypeSuitability (iFieldType: Integer): Boolean; Overload;

function FieldIsInternalUsageField (sFieldName: AnsiString): Boolean;
function FieldIsPresettedTypeField (sFieldName: AnsiString): Boolean;
function FieldIsCalculatedField (sFieldName: AnsiString): Boolean;
// ++ Cadmensky
function GetCalcFieldTypeAndSize (sFieldName: AnsiString; var iFieldType, iFieldSize: integer): boolean;
function GetDataPropertyByCalcFieldName (sFieldName: AnsiString): TDataProps;
// -- Cadmensky

function GetTypeNameByNumber (iFieldType: Integer): AnsiString;

function CreateGUID: AnsiString;

procedure AddOneValue (lbSource, lbDest: TListBox; bDeleteFromSourceListBox: Boolean = TRUE); Overload;
procedure AddOneValue (lbSource: TListBox; cbDest: TComboBox; bDeleteFromSourceListBox: Boolean = TRUE); Overload;
procedure AddAllValues (lbSource, lbDest: TListBox; bClearSource: Boolean = TRUE); Overload;
procedure AddAllValues (lbSource: TListBox; cbDest: TComboBox; bClearSource: Boolean = TRUE); Overload;

procedure CheckSpecialFieldsAndAttTableStruct (AProject: Pointer; IDA: TIDB_Access2DB_Level2; ALayer: Pointer; var slFieldNames: TStringList);

function CheckRequiredFields (ALayerManager: TIDB_LayerManager; ALayer: Pointer; sAttTableName: AnsiString; bNeedToFillCalculatedFields: boolean = false): Boolean;
// ++ Commented by Cadmensky Version 2.3.2
//function CheckOverlayField (ALayerManager: TIDB_LayerManager; sAttTableName: AnsiString; AOverlayField: TOverlayField): Boolean;
// -- Commented by Cadmensky Version 2.3.2

function GetTempDir: Ansistring;

function GetTypeNameByProgisID (iProgisID: Integer): AnsiString;
function GetTypeBitmapNumberByProgisID (iProgisID: Integer): Integer;

// ++ Cadmensky
procedure ReadLayerTablesNames (AProject: Pointer; AList: TList; IDA: TIDB_Access2DB_Level2);
procedure FillLayerNamesList (AProject: Pointer; AList: TList);
procedure SaveLayerTablesNames (AProject: Pointer; AList: TList; IDA: TIDB_Access2DB_Level2);
function GetLayerNameEx (AProject, ALayer: Pointer; listLayerNames: TList = nil): AnsiString;
function GetLayerIndexByNameEx (AProject: Pointer; sLayerName: AnsiString; listLayerNames: TList = nil): Integer;
function FieldIsLinked (sFieldName: AnsiString): Boolean;
//function GetLinkedFieldLinkNo (sFieldName: AnsiString; var sRealFieldName: AnsiString): integer;
function GetCalculatedFieldValueByName (AProject, ALayer: Pointer; iItemID: integer; sFieldName: AnsiString): AnsiString;
// -- Cadmensky
// ++ Cadmensky IDB Version 2.3.8
function MakeCorrectString (sValue: AnsiString): AnsiString;
function GetObjectPropsEx (AProject, ALayer: Pointer; iItemID: Integer; ADataPropsType: TDataProps): AnsiString;

procedure ExecuteWinGISMenuFuncion(Funcnr: Integer);


const
   mi_Text = 000000000;
   mi_RText = 090000000;
   mi_BusGraph = 095000000;
   mi_MesLine = 100000000;
   mi_Symbol = 200000000;
   mi_Circle = 250000000; {0504F}
   mi_Arc = 300000000; {0504F}
   mi_Pixel = 400000000;
   mi_Group = 600000000;
   mi_ChartKind = 650000000; //++ Moskaliov Business Graphics BUILD#150 17.01.01
   mi_Spline = 700000000;
   mi_Poly = 800000000;
   mi_CPoly = 1000000000;
   mi_Image = 1200000000;
   mi_OLEObj = 1250000000; {0707Ray}

implementation

uses SysUtils, ActiveX, ComObj, Windows,
//   AM_Def,
   IDB_Consts, IDB_LanguageManager, Forms;


function MakeDBFileNameFromProjectFileName (sProjectFileName: AnsiString): AnsiString;
var
   iP: Integer;
   bF: Boolean;
begin
     // I search the last point.
   bF := FALSE;
   for iP := Length (sProjectFileName) downto 1 do
      if Copy (sProjectFileName, iP, 1) = '.' then
      begin
         bF := TRUE;
         BREAK;
      end;
     // if I didn't find any point in file name, I'll use file name as whole.
   if not bF then iP := Length (sProjectFileName);
   RESULT := Copy (sProjectFileName, 1, iP) + cs_IDB_DatabaseFileExt;
end;

function MakeTempDBFileName (sReallyFileName: AnsiString): AnsiString;
var
   iP: Integer;
   bF: Boolean;
begin
     // I search the last point.
   bF := FALSE;
   for iP := Length (sReallyFileName) downto 1 do
      if Copy (sReallyFileName, iP, 1) = '.' then
      begin
         bF := TRUE;
         BREAK;
      end;
     // if I didn't find any point in file name, I'll use file name as whole.
   if not bF then iP := Length (sReallyFileName);
   RESULT := Copy (sReallyFileName, 1, iP) + cs_IDB_TemporarilyStoragedDataBaseFileExt;
end;

function GetFileNameWithoutExt (sFileName: AnsiString): AnsiString;
var
   iP: Integer;
   sPN: AnsiString;
   iExtLength: Integer;
begin
   RESULT := ALangManager.GetValue (110);
   sPN := ExtractFileName (sFileName);
   iExtLength := Length (ExtractFileExt (sPN));
   iP := Length (sPN) - iExtLength;
   RESULT := Copy (sPN, 1, iP);
end;

function ReplaceChars (sValue: AnsiString; chOldChar: Char = ' '; chNewChar: Char = '_'): AnsiString;
var
   iLen: Integer;
   iP: Integer;
begin
   iP := Pos (chOldChar, sValue);
   if iP < 1 then
   begin
      RESULT := sValue;
      exit;
   end;
   iLen := Length (sValue);
   for iP := 1 to iLen do
      if sValue[iP] = chOldChar then sValue[iP] := chNewChar;
   RESULT := sValue;
end;

function DeleteChars (sValue: AnsiString; sCharsToBeDeleted: AnsiString): AnsiString;
var
   iL, iP: Integer;
begin
   RESULT := sValue;
   iP := Pos (sCharsToBeDeleted, RESULT);
   if iP < 0 then exit;
   iL := Length (sCharsToBeDeleted);
   while iP > 0 do
   begin
      RESULT := Copy (RESULT, 1, iP - 1) + Copy (RESULT, iP + iL, Length (RESULT));
      iP := Pos (sCharsToBeDeleted, RESULT);
   end;
end;

function CheckFieldTypeSuitability (AFieldType: TFieldType): Boolean;
begin
   RESULT := CheckFieldTypeSuitability (Ord (AFieldType));
end;

function CheckFieldTypeSuitability (iFieldType: Integer): Boolean;
begin
     {
     1 - String
     2 - SmallInt
     3 - Integer
     5 - Boolean
     6 - Float
     7 - Currency
     11 - DateTime
     12 - Byte
     }
   case iFieldType of
      1..3, 5..7, 11..12: RESULT := TRUE;
   else
      RESULT := FALSE;
   end; // case end
end;

{ This procedure checks whether a field is special field or not. An user shouldn't see such fields. }

function FieldIsInternalUsageField (sFieldName: AnsiString): Boolean;
begin
   RESULT := FALSE;
   sFieldName := AnsiUpperCase (sFieldName);
   if (sFieldName = cs_TempFieldName)
      or
      (sFieldName = cs_StateFieldName)
      or
      (sFieldName = cs_InternalID_FieldName) then
      RESULT := TRUE;
end;

function FieldIsPresettedTypeField (sFieldName: AnsiString): Boolean;
begin
   RESULT := FALSE;
   sFieldName := AnsiUpperCase (sFieldName);
   if (sFieldName = 'GEOTEXTINFO') then
      RESULT := TRUE;
end;

function FieldIsCalculatedField (sFieldName: AnsiString): Boolean;
begin
   RESULT := FALSE;
   sFieldName := AnsiUpperCase (sFieldName);
   if (sFieldName = AnsiUpperCase (cs_CalcFieldAreaName)) // '_Area';
      or
//      (sFieldName = AnsiUpperCase (cs_CalcFieldLengthName)) // '_Length';
//      or
//      (sFieldName = AnsiUpperCase (cs_CalcFieldPerimeterName)) // '_Perimeter';
//      or
   (sFieldName = AnsiUpperCase (cs_CalcFieldXName)) // '_X';
      or
      (sFieldName = AnsiUpperCase (cs_CalcFieldYName)) // '_Y';
      or
      (sFieldName = AnsiUpperCase (cs_CalcFieldVerticesCount)) // '_Vertices';
      or
      (sFieldName = AnsiUpperCase (cs_CalcFieldSymbolName)) // '_Symbol Name';
      or
      (sFieldName = AnsiUpperCase (cs_CalcFieldRotationName))
      or
      (sFieldName = AnsiUpperCase (cs_CalcFieldSizeName)) // '_Size';
      or
      (sFieldName = AnsiUpperCase (cs_CalcFieldTextName)) // '_Text';
      or
      (sFieldName = AnsiUpperCase (cs_CalcFieldLinkIDName)) then // '_LinkID';
      RESULT := TRUE;
end;

// ++ Cadmensky

function GetCalcFieldTypeAndSize (sFieldName: AnsiString; var iFieldType, iFieldSize: integer): boolean;
begin
   RESULT := false;
   if (sFieldName = cs_CalcFieldAreaName) or
      (sFieldName = cs_CalcFieldSizeName) or
      (sFieldName = cs_CalcFieldRotationName) or
      (sFieldName = cs_CalcFieldXName) or
      (sFieldName = cs_CalcFieldY1Name) or
      (sFieldName = cs_CalcFieldX1Name) or
      (sFieldName = cs_CalcFieldYName)then
   begin
      iFieldType := 6;
      iFieldSize := 8;
      RESULT := true;
   end;

   if (sFieldName = cs_CalcFieldTextName) or
      (sFieldName = cs_CalcFieldSymbolName) then
   begin
      iFieldType := 1;
      iFieldSize := 255;
      RESULT := true;
   end;

   if (sFieldName = cs_CalcFieldVerticesCount) or
      (sFieldName = cs_CalcFieldLinkIDName) then
   begin
      iFieldType := 3;
      iFieldSize := 8;
      RESULT := true;
   end;
end;

function GetDataPropertyByCalcFieldName (sFieldName: AnsiString): TDataProps;
begin
   if sFieldName = cs_CalcFieldAreaName then RESULT := dtArea;
   if sFieldName = cs_CalcFieldSizeName then RESULT := dtSize;
   if sFieldName = cs_CalcFieldXName then RESULT := dtX;
   if sFieldName = cs_CalcFieldYName then RESULT := dtY;
   if sFieldName = cs_CalcFieldX1Name then RESULT := dtX1;
   if sFieldName = cs_CalcFieldY1Name then RESULT := dtY1;
   if sFieldName = cs_CalcFieldTextName then RESULT := dtText;
   if sFieldName = cs_CalcFieldSymbolName then RESULT := dtSymbolName;
   if sFieldName = cs_CalcFieldRotationName then RESULT := dtRotation;
   if sFieldName = cs_CalcFieldVerticesCount then RESULT := dtVerticesCount;
   if sFieldName = cs_CalcFieldLinkIDName then RESULT := dtParent;
end;
// -- Cadmensky

function GetTypeNameByNumber (iFieldType: Integer): AnsiString;
begin
   case iFieldtype of
      0: RESULT := 'Unknown';
//     1: RESULT := 'String';
      1: RESULT := 'Text';
      2: RESULT := 'SmallInt';
      3: RESULT := 'Integer';
      4: RESULT := 'Word';
      5: RESULT := 'Boolean';
      6: RESULT := 'Float';
      7: RESULT := 'Currency';
      8: RESULT := 'BCD';
      9: RESULT := 'Date';
      10: RESULT := 'Time';
      11: RESULT := 'DateTime';
      12: RESULT := 'Byte';
      13: RESULT := 'VarBytes';
//     14: RESULT := 'AutoInc';
      14: RESULT := 'AutoIncrement';
      15: RESULT := 'Blob';
      16: RESULT := 'Memo';
      17: RESULT := 'Graphic';
      18: RESULT := 'FmtMemo';
      19: RESULT := 'ParadoxOle';
      20: RESULT := 'dBaseOle';
      21: RESULT := 'TypedBinary';
      22: RESULT := 'Cursor';
      23: RESULT := 'FixedChar';
      24: RESULT := 'WideString';
      25: RESULT := 'LargeInt';
      26: RESULT := 'ADT';
      27: RESULT := 'array';
      28: RESULT := 'Reference';
      29: RESULT := 'DataSet';
   end; // case end
end;

function CreateGUID: AnsiString;
var
   Value: TGUID;
begin
   CoCreateGUID (Value);
   RESULT := GuidToString (Value);
end;

{This procedure moves one string from source TListBox into destination TListBox.}

procedure AddOneValue (lbSource, lbDest: TListBox; bDeleteFromSourceListBox: Boolean = TRUE);
var
   iI: Integer;
begin
   iI := lbSource.ItemIndex;
   if lbSource.Items.Count < 1 then exit;
   lbDest.Items.Add (lbSource.Items[iI]);
   if bdeleteFromSourceListBox then
      lbSource.Items.Delete (iI);
   if iI < lbSource.Items.Count then
      lbSource.ItemIndex := iI
   else
      lbSource.ItemIndex := lbSource.Items.Count - 1;
   if lbDest.ItemIndex < 0 then
      lbDest.ItemIndex := 0;
end;

{This is the same procedure but workes with TComboBox as source and TCombBox as destination.}

procedure AddOneValue (lbSource: TListBox; cbDest: TComboBox; bDeleteFromSourceListBox: Boolean = TRUE);
var
   iI: Integer;
begin
   iI := lbSource.ItemIndex;
   if lbSource.Items.Count < 1 then exit;
   cbDest.Items.Add (lbSource.Items[iI]);
   if bdeleteFromSourceListBox then
      lbSource.Items.Delete (iI);
   if iI < lbSource.Items.Count then
      lbSource.ItemIndex := iI
   else
      lbSource.ItemIndex := lbSource.Items.Count - 1;
   if cbDest.ItemIndex < 0 then cbDest.ItemIndex := 0;
end;

{This procedure moves all strings from source TListBox into destination TListBox.}

procedure AddAllValues (lbSource, lbDest: TListBox; bClearSource: Boolean = TRUE);
var
   iI: Integer;
begin
   for iI := 0 to lbSource.Items.Count - 1 do
      lbDest.Items.Add (lbSource.Items[iI]);
   if bClearSource then lbSource.Items.Clear;
   if lbDest.ItemIndex < 0 then lbDest.ItemIndex := 0;
end;

{This is the same procedure but workes with TComboBox as source and TCombBox as destination.}

procedure AddAllValues (lbSource: TListBox; cbDest: TComboBox; bClearSource: Boolean = TRUE);
var
   iI: Integer;
begin
   for iI := 0 to lbSource.Items.Count - 1 do
      cbDest.Items.Add (lbSource.Items[iI]);
   if bClearSource then lbSource.Items.Clear;
   if cbDest.ItemIndex < 0 then cbDest.ItemIndex := 0;
end;

{This procedure checks whether fields exists or not in a attribure table. This operation
is necessity before we shall start insert various objects in a WinGIS project.}

procedure CheckSpecialFieldsAndAttTableStruct (AProject: Pointer; IDA: TIDB_Access2DB_Level2; ALayer: Pointer; var slFieldNames: TStringList);
var
   iI: Integer;
   sFieldName: AnsiString;
//   bFieldIsPresent: Boolean;
   sAttTableName: AnsiString;
begin
// ++ Cadmensky Version 2.3.2
   if not AnIDBMainMan.UseNewATTNames (AProject) then
      sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
   else
      sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

   if IDA.ADBItemsMan.GetReadyToCheckTableStructure (sAttTableName) <> ci_All_Ok then
   begin
      // if I have any problem here, I simply clear the list.
      slFieldNames.Clear;
      IDA.ADBItemsMan.EndOfCheckingTable;
      Exit;
   end;
// -- Cadmensky Version 2.3.2

    // if I have an any errors during detecting the field's presenting, I shall go out.
   iI := 0;
   while iI < slFieldNames.Count do
   begin
      sFieldName := slFieldNames[iI];

// ++ Commented by Cadmensky Version 2.3.2
{      if not AnIDBMainMan.UseNewATTNames (AProject) then
      begin
         if IDA.ADbItemsMan.FieldExists (cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer)), sFieldName, bFieldIsPresent) <> ci_All_Ok then
         begin
                 // if I have any problem here, I simply clear the list.
            slFieldNames.Clear;
            exit;
         end;
      end
      else
      begin
         if IDA.ADbItemsMan.FieldExists (cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer), sFieldName, bFieldIsPresent) <> ci_All_Ok then
         begin
                 // if I have any problem here, I simply clear the list.
            slFieldNames.Clear;
            exit;
         end;
      end;

      if not bFieldIsPresent then}
// -- Commented by Cadmensky Version 2.3.2
// ++ Cadmensky Version 2.3.2
      if not IDA.ADBItemsMan.FieldExists (sFieldName) then
// -- Cadmensky Version 2.3.2
              // if the field does not exist in the attribute table, I delete it from the list.
         slFieldNames.Delete (iI)
      else
              // if the field exists in the attribute table, I walk to the next field in the list.
         Inc (iI);

   end; // while iI end
// ++ Cadmensky Version 2.3.2
   IDA.ADBItemsMan.EndOfCheckingTable;
// -- Cadmensky Version 2.3.2
end;

function CheckRequiredFields (ALayerManager: TIDB_LayerManager; ALayer: Pointer; sAttTableName: AnsiString; bNeedToFillCalculatedFields: boolean = false): Boolean;
var
//   bF: Boolean;
   ADBItemsManager: TIDB_DatabaseItemsManager;
begin
   RESULT := FALSE;

   ADBItemsManager := ALayerManager.DataAccessObject.ADBItemsMan;
   // Check whether required fields exists or not.

// ++ Cadmensky Version 2.3.2
   if ADBItemsManager.GetReadyToCheckTableStructure (sAttTableName) <> ci_All_Ok then
   begin
      ADBItemsManager.EndOfCheckingTable;
      Exit;
   end;

   if not ADBItemsManager.FieldExists (cs_StateFieldName) then
      ADBItemsManager.AddField (sAttTableName, cs_StateFieldName, 12, -1); // Type '12' is a byte type.

   if not ADBItemsManager.FieldExists (cs_InternalID_FieldName) then
      ADBItemsManager.AddField (sAttTableName, cs_InternalID_FieldName, 14, -1); // 14 = AutoIncrement type

   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldAreaName, ADBItemsManager.FieldExists (cs_CalcFieldAreaName));
   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldXName, ADBItemsManager.FieldExists (cs_CalcFieldXName));
   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldYName, ADBItemsManager.FieldExists (cs_CalcFieldYName));
   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldVerticesCount, ADBItemsManager.FieldExists (cs_CalcFieldVerticesCount));
   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldSymbolName, ADBItemsManager.FieldExists (cs_CalcFieldSymbolName));
   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldRotationName, ADBItemsManager.FieldExists (cs_CalcFieldRotationName));
   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldSizeName, ADBItemsManager.FieldExists (cs_CalcFieldSizeName));
   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldTextName, ADBItemsManager.FieldExists (cs_CalcFieldTextName));
   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldLinkIDName, ADBItemsManager.FieldExists (cs_CalcFieldLinkIDName));
// -- Cadmensky Version 2.3.2
   if   ADBItemsManager.FieldExists (cs_CalcFieldTextName) then ADBItemsManager.AlterField(sAttTableName,cs_CalcFieldTextName,1,255);
// ++ Commented by Cadmensky Version 2.3.2
{   if ADBItemsManager.FieldExists (sAttTableName, cs_StateFieldName, bF) <> ci_All_Ok then exit;
     // if it doesn't exist, add it.
   if not bF then
      try
         ADBItemsManager.AddField (sAttTableName, cs_StateFieldName, 12, -1); // Type '12' is a byte type.
      except
         exit;
      end;

   if ADBItemsManager.FieldExists (sAttTableName, cs_InternalID_FieldName, bF) <> ci_All_Ok then exit;
   if not bF then
      try
         ADBItemsManager.AddField (sAttTableName, cs_InternalID_FieldName, 14, -1); // 14 = AutoIncrement type
      except
         exit;
      end;

// ++ Cadmensky
   if ADBItemsManager.FieldExists (sAttTableName, cs_CalcFieldAreaName, bF) <> ci_All_Ok then
      exit;
   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldAreaName, bF);
   if ADBItemsManager.FieldExists (sAttTableName, cs_CalcFieldXName, bF) <> ci_All_Ok then
      exit;
   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldXName, bF);

   if ADBItemsManager.FieldExists (sAttTableName, cs_CalcFieldYName, bF) <> ci_All_Ok then
      exit;
   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldYName, bF);

   if ADBItemsManager.FieldExists (sAttTableName, cs_CalcFieldVerticesCount, bF) <> ci_All_Ok then
      exit;
   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldVerticesCount, bF);

   if ADBItemsManager.FieldExists (sAttTableName, cs_CalcFieldSymbolName, bF) <> ci_All_Ok then
      exit;
   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldSymbolName, bF);

   if ADBItemsManager.FieldExists (sAttTableName, cs_CalcFieldSizeName, bF) <> ci_All_Ok then
      exit;
   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldSizeName, bF);

   if ADBItemsManager.FieldExists (sAttTableName, cs_CalcFieldTextName, bF) <> ci_All_Ok then
      exit;
   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldTextName, bF);

   if ADBItemsManager.FieldExists (sAttTableName, cs_CalcFieldLinkIDName, bF) <> ci_All_Ok then
      exit;
   ALayerManager.SetCalcFieldIsUsed (ALayer, cs_CalcFieldLinkIDName, bF);}
// -- Cadmensky
// ++ Commented by Cadmensky Version 2.3.2

   try // I suppose that index on this field may be deleted by somebody. There by I try to create it again.
      ADBItemsManager.DeleteIndex (sAttTableName, cs_InternalID_FieldName);
   except
   end;
                // WAI Internal ID add
   try
      ADBItemsManager.AddPrimaryKey (sAttTableName, cs_InternalID_FieldName);
   except
// ++ Cadmensky Version 2.3.0
      ADBItemsManager.DeleteField (sAttTableName, cs_InternalID_FieldName);
      ADBItemsManager.AddField (sAttTableName, cs_InternalID_FieldName, 14, -1); // 14 = AutoIncrement type
      try
         ADBItemsManager.AddPrimaryKey (sAttTableName, cs_InternalID_FieldName);
      except
      end;
// -- Cadmensky Version 2.3.0
   end;

// ++ Cadmensky
   //try // I suppose that index on this field may be deleted by somebody. There by I try to create it again.
  //    ADBItemsManager.DeleteIndex (sAttTableName, 'PROGISID');
 //  except
 //  end;

   {try
      if not ADBItemsManager.AddIndex (sAttTableName, 'PROGISID', true) then
      begin
         MessageBox (Application.Handle, PChar (ALangManager.GetValue (462)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
// ++ Cadmensky Version 2.3.2
         Exit;
      end;
// -- Cadmensky Version 2.3.2
   except
   end;    }

   if bNeedToFillCalculatedFields then
      try
         ALayerManager.FillCalculatedFields (ALayer);
      except
      end;

// -- Cadmensky
   RESULT := TRUE;

// ++ Cadmensky Version 2.3.2
   ADBItemsManager.EndOfCheckingTable;
// -- Cadmensky Version 2.3.2
end;

// ++ Commented by Cadmensky Version 2.3.2
{function CheckOverlayField (ALayerManager: TIDB_LayerManager; sAttTableName: AnsiString; AOverlayField: TOverlayField): Boolean;
var
   bF: Boolean;
   ADBItemsManager: TIDB_DatabaseItemsManager;
begin
   RESULT := FALSE;

   ADBItemsManager := ALayerManager.DataAccessObject.ADbItemsMan;
     // Check whether required fields exists or not.

   if ADBItemsManager.FieldExists (sAttTableName, AOverlayField.FieldName, bF) <> ci_All_Ok then
      exit;
     // if it doesn't exist, add it.
   if not bF then
      try
         ADBItemsManager.AddField (sAttTableName, AOverlayField.FieldName, AOverlayField.FieldType, AOverlayField.FieldSize); // Type '12' is a byte type.
      except
         exit;
      end;

// -- Cadmensky

   RESULT := TRUE;
end;}
// -- Commented by Cadmensky Version 2.3.2

function SaveTempDatabaseFile (AProject: Pointer): AnsiString;
var
   sProjectFileName,
      sSourceFileName: AnsiString;
begin
     // Save already existed file as temp file.
   sProjectFileName := GetProjectFullFileNameFunc (AProject);
   sSourceFileName := MakeDBFileNameFromProjectFileName (sProjectFileName);
   RESULT := MakeTempDBFileName (sProjectFileName);
   if FileExists (RESULT) then DeleteFile (PChar (RESULT));
   CopyFile (PChar (sSourceFileName), PChar (RESULT), FALSE);
end;

function GetTempDir: Ansistring;
var
   ABuffer: array[0..1023] of Char;
begin
   SetString (RESULT, ABuffer, GetTempPath (SizeOf (ABuffer), ABuffer));
end;

function GetTypeNameByProgisID (iProgisID: Integer): AnsiString;
begin
   if iProgisID < mi_RText then
      RESULT := 'Text'
   else
      if iProgisID < mi_BusGraph then
         RESULT := 'Annotation'
      else
         if iProgisID < mi_MesLine then
            RESULT := 'Business chart'
         else
            if iProgisID < mi_Symbol then
               RESULT := 'Measure line'
            else
               if iProgisID < mi_Circle then
                  RESULT := 'Symbol'
               else
                  if iProgisID < mi_Arc then
                     RESULT := 'Circle'
                  else
                     if iProgisID < mi_Pixel then
                        RESULT := 'Arc'
                     else
                        if iProgisID < mi_Group then
                           RESULT := 'Pixel'
                        else
                           if iProgisID < mi_Spline then
                              RESULT := 'Group'
                           else
                              if iProgisID < mi_Poly then
                                 RESULT := 'Spline'
                              else
                                 if iProgisID < mi_CPoly then
                                    RESULT := 'Polyline'
                                 else
                                    if iProgisID < mi_Image then
                                       RESULT := 'Polygon'
                                    else
                                       if iProgisID < mi_OleObj then
                                          RESULT := 'Image'
                                       else
                                          RESULT := 'OLE Object';
end;

function GetTypeBitmapNumberByProgisID (iProgisID: Integer): Integer;
begin
   RESULT := 1;
   if iProgisID < mi_RText then
      RESULT := 8 //'Text'
   else
      if iProgisID < mi_BusGraph then
         RESULT := 8 //'Annotation'
      else
         if iProgisID < mi_MesLine then
            RESULT := 9 //'Business chart'
         else
            if iProgisID < mi_Symbol then
               RESULT := 6 //'Measure line'
            else
               if iProgisID < mi_Circle then
                  RESULT := 7 //'Symbol'
               else
                  if iProgisID < mi_Arc then
                     RESULT := 3 //'Circle'
                  else
                     if iProgisID < mi_Pixel then
                        RESULT := 2 //'Arc'
                     else
                        if iProgisID < mi_Group then
                           RESULT := 4 //'Pixel'
                        else
                           if iProgisID < mi_Spline then
                              RESULT := 1 //'Group'
                           else
                              if iProgisID < mi_Poly then
                                 RESULT := 6 //'Spline'
                              else
                                 if iProgisID < mi_CPoly then
                                    RESULT := 6 //'Polyline'
                                 else
                                    if iProgisID < mi_Image then
                                       RESULT := 5 //'Polygon'
                                    else
                                       if iProgisID < mi_OleObj then
                                          RESULT := 1 //'Image'
                                       else
                                          RESULT := 11 //'OLE Object';
end;

procedure ReadLayerTablesNames (AProject: Pointer; AList: TList; IDA: TIDB_Access2DB_Level2);
var
   ATable: TDTable;
   ALayerNameItem: PLayerNameItem;
begin
   ATable := TDTable.Create (nil);
   ATable.Master := IDA.DMaster;
   ATable.TableName := cs_IT_LayerNamesTable;
   try
      ATable.Open;
   except
      ATable.Free;
//      FillLayerNamesListProc (AProject, AList);
      FillLayerNamesList (AProject, AList);
      exit;
   end;

   if ATable.RecordCount > 0 then
   begin
      ATable.First;
      while not ATable.Eof do
      begin
         New (ALayerNameItem);
         ALayerNameItem.iLayerIndex := ATable.FieldByName ('Index').AsInteger;
         ALayerNameItem.sLayerTableName := ATable.FieldByName ('TableName').AsString;
         if pos('ATT_',ALayernameItem.sLayerTableName)=1 then ALayernameItem.sLayerTableName:=Stringreplace(ALayernameItem.sLayerTableName,'ATT_','',[]);
         AList.Add (ALayerNameItem);
         ATable.Next;
      end;
   end;
   ATable.Free;

end;

procedure FillLayerNamesList (AProject: Pointer; AList: TList);
var
   iI: Integer;
   ALayer: Pointer;
   ALayerNameItem: PLayerNameItem;
begin
   if AList = nil then
      exit;
   AList.Clear;
   for iI := 1 to GetProjectLayerNumberFunc (AProject) - 1 do
   begin
      ALayer := GetLayerPointerByPositionFunc (AProject, iI);
      if ALayer <> nil then
      begin
         New (ALayerNameItem);
         ALayerNameItem.iLayerIndex := GetLayerIndexFunc (ALayer);
         ALayerNameItem.sLayerTableName := GetLayerNameFunc (ALayer);
         AList.Add (ALayerNameItem);
      end;
   end;
end;

procedure SaveLayerTablesNames (AProject: Pointer; AList: TList; IDA: TIDB_Access2DB_Level2);
var
   AQuery: TDQuery;
   ALayerNameItem: PLayerNameItem;
   i: Integer;
   sSQL: AnsiString;
   ALayer: Pointer;
   sAttTableName, sNewAttTableName, sLayerName: AnsiString;
begin
   IDA.ADbItemsMan.DeleteTable (cs_IT_LayerNamesTable);
   IDA.ADbItemsMan.CreateLayerNamesSysTable;

   AQuery := TDQuery.Create (nil);
   AQuery.Master := IDA.DMaster;
   for i := 0 to AList.Count - 1 do
   begin
      ALayerNameItem := AList[i];
      try
         ALayer := GetLayerPointerByIndexFunc (AProject, ALayerNameItem.iLayerIndex);
// ++ Cadmensky Version 2.3.3
         sLayerName := GetLayerNameFunc (ALayer);
         if ALayerNameItem.sLayerTableName <> sLayerName then
            try
               sAttTableName := cs_AttributeTableNamePrefix + '_' + ALayerNameItem.sLayerTableName;
               sNewAttTableName := cs_AttributeTableNamePrefix + '_' + sLayerName;

               AQuery.ExecSQL ('SELECT * INTO [' + sNewAttTableName + '] FROM [' + sAttTableName + ']');
               AQuery.ExecSQL ('DROP TABLE [' + sAttTableName + ']');
               ALayerNameItem.sLayerTableName := sLayerName;
               AQuery.ExecSQL ('UPDATE [USYS_WGIDB_DT] SET isTemplate = "' + sNewAttTableName + '" WHERE isTemplate = "' + sAttTableName + '"');
            except
            end;
// -- Cadmensky Version 2.3.3
         sSQL := 'INSERT INTO [' + cs_IT_LayerNamesTable + '] ([INDEX], [TABLENAME], [LAYERNAME]) ' +
            'VALUES (' + IntToStr (ALayerNameItem.iLayerIndex) + ', ''ATT_' + ALayerNameItem.sLayerTableName + ''', ''' + GetLayerNameFunc (ALayer) + ''')';
         AQuery.ExecSQL (sSQL);
      except
      end;
   end;
   try
   aQuery.close;
   aQuery.Text:='SELECT [INDEX] FROM [' + cs_IT_LayerNamesTable + ']';
   aQuery.Open;
   aQuery.First;
   sSql:='';
   for i:= 0 to aQuery.RecordCount -1 do
     begin
     sSql:=sSql + 'LI<>' + aQuery.fieldbyname('INDEX').AsString + ' AND ';
     aQuery.Next;
     end;
   //sSql:=copy(sSql,1,length(sSql)-5);
   sSql:= sSql + 'LI<>-1';
   aQuery.ExecSQL('DELETE LI FROM [USYS_WGIDB_DT] WHERE ' + sSql);
   except
   end;
   AQuery.Free;
end;

function GetLayerNameEx (AProject, ALayer: Pointer; listLayerNames: TList = nil): AnsiString;
var
   iLayerIndex: integer;
   ALayerNamesList: TList;
   ALayerNameItem: PLayerNameItem;
   i: integer;
begin
   RESULT := '';

   if listLayerNames = nil then
      ALayerNamesList := AnIDBMainMan.GetLayerNamesList (AProject)
   else
      ALayerNamesList := listLayerNames;

   if ALayerNamesList = nil then
      exit;

   iLayerIndex := GetLayerIndexFunc (ALayer);

   for i := 0 to ALayerNamesList.Count - 1 do
   begin
      ALayerNameItem := ALayerNamesList[i];
      if ALayerNameItem.iLayerIndex = iLayerIndex then
      begin
         RESULT := ALayerNameItem.sLayerTableName;
         break;
      end;
   end;

   if RESULT = '' then
   begin
      FillLayerNamesList (AProject, ALayerNamesList);
      for i := 0 to ALayerNamesList.Count - 1 do
      begin
         ALayerNameItem := ALayerNamesList[i];
         if ALayerNameItem.iLayerIndex = iLayerIndex then
         begin
            RESULT := ALayerNameItem.sLayerTableName;
            break;
         end;
      end;
   end;
end;

function GetLayerIndexByNameEx (AProject: Pointer; sLayerName: AnsiString; listLayerNames: TList = nil): Integer;
var
//   iLayerIndex: integer;
   ALayerNamesList: TList;
   ALayerNameItem: PLayerNameItem;
   i: integer;
begin
   RESULT := -1;
   if listLayerNames = nil then
      ALayerNamesList := AnIDBMainMan.GetLayerNamesList (AProject)
   else
      ALayerNamesList := listLayerNames;

   if ALayerNamesList = nil then
      exit;
   for i := 0 to ALayerNamesList.Count - 1 do
   begin
      ALayerNameItem := ALayerNamesList[i];
      if ALayerNameItem.sLayerTableName = sLayerName then
         RESULT := ALayerNameItem.iLayerIndex;
   end;
   if RESULT = -1 then
   begin
      FillLayerNamesList (AProject, ALayerNamesList);
      for i := 0 to ALayerNamesList.Count - 1 do
      begin
         ALayerNameItem := ALayerNamesList[i];
         if ALayerNameItem.sLayerTableName = sLayerName then
            RESULT := ALayerNameItem.iLayerIndex;
      end;
   end;

end;

{function GetLinkedFieldLinkNo (sFieldName: AnsiString; var sRealFieldName: AnsiString): Integer;
var
   iPos: integer;
   sLinkNumber: AnsiString;
begin
   RESULT := -1;
   sRealFieldName := sFieldName;
   iPos := Pos ('_', sFieldName);
   if iPos >= 2 then
      try
         sLinkNumber := Copy (sFieldName, 1, iPos - 1);
         RESULT := StrToInt (sLinkNumber) - 1;
         sRealFieldName := Copy (sFieldName, iPos + 1, Length (sFieldName) - iPos);
      except
      end;
end;   }

function FieldIsLinked (sFieldName: AnsiString): Boolean;
var
   sRealFieldName: AnsiString;
begin
  RESULT :=FALSE;  // GetLinkedFieldLinkNo (sFieldName, sRealFieldName) <> -1;
end;

function GetCalculatedFieldValueByName (AProject, ALayer: Pointer; iItemID: integer; sFieldName: AnsiString): AnsiString;
begin
   if sFieldName = cs_CalcFieldAreaName then
      RESULT := GetObjectPropsEx (AProject, ALayer, iItemID, dtArea)
   else
      if sFieldName = cs_CalcFieldXName then
         RESULT := GetObjectPropsEx (AProject, ALayer, iItemID, dtX)
      else
         if sFieldName = cs_CalcFieldYName then
            RESULT := GetObjectPropsEx (AProject, ALayer, iItemID, dtY)
         else
            if sFieldName = cs_CalcFieldSizeName then
               RESULT := GetObjectPropsEx (AProject, ALayer, iItemID, dtSize)
            else
               if sFieldName = cs_CalcFieldVerticesCount then
                  RESULT := GetObjectPropsEx (AProject, ALayer, iItemID, dtVerticesCount)
               else
                  if sFieldName = cs_CalcFieldSymbolName then
                     RESULT := GetObjectPropsEx (AProject, ALayer, iItemID, dtSymbolName)
                  else
                    if sFieldName = cs_CalcFieldRotationName then
                     RESULT := GetObjectPropsEx (AProject, ALayer, iItemID, dtRotation)
                    else
                     if sFieldName = cs_CalcFieldTextName then
                        RESULT := GetObjectPropsEx (AProject, ALayer, iItemID, dtText)
                     else
                        if sFieldName = cs_CalcFieldLinkIDName then
                           RESULT := GetObjectPropsEx (AProject, ALayer, iItemID, dtParent);
end;

// ++ Cadmensky IDB Version 2.3.8
function MakeCorrectString (sValue: AnsiString): AnsiString;
var
   iI: Integer;
begin
   if Pos ('''', sValue) > 0 then
   begin
      RESULT := '';
      for iI := 1 to Length (sValue) do
      begin
         if sValue[iI] = '''' then
            RESULT := RESULT + '''' + ''''
         else
            RESULT := RESULT + sValue[iI];
      end;
   end
   else
      RESULT := sValue;
end;

function GetObjectPropsEx (AProject, ALayer: Pointer; iItemID: Integer; ADataPropsType: TDataProps): AnsiString;
var
   pcData: PChar;
begin
   pcData := GetObjectPropsFunc (AProject, ALayer, iItemID, ADataPropsType);
   RESULT := pcData;
   FreeMem (pcData);
end;
// -- Cadmensky IDB Version 2.3.8
procedure ExecuteWinGISMenuFuncion(Funcnr: integer);
var handle: THAndle;
begin
handle:=findwindow('TWinGISMainForm',NIL);
postmessage(handle,15005,Funcnr,0);
end;




end.

