library IDB;
{
Internal database. Ver. II.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 30-01-2001
}
uses
  SHAREMEM,
  Forms,
  Windows,
  SysUtils,
  Classes,
  Objects,
  IDB_Consts,
  IDB_Access2DB,
  IDB_View {IDB_ViewForm},
  IDB_MainManager,
  IDB_Utils,
  IDB_LanguageManager,
  IDB_ParamsOfSearch {IDB_SearchParamsForm},
  IDB_ParamsOfUpdates {IDB_UpdateForm},
  IDB_ParamsOfSaveLoad_TableSelect {IDB_TableSelect},
  IDB_DataMan,
  IDB_SelectInListBox {IDB_SelectInListBoxForm},
  IDB_DatabaseItemsManager,
  IDB_Messages,
  IDB_CallBacksDef,
  IDB_Monitor {IDB_MonitorForm},
  IDB_MonData,
  IDB_2ParamsDialog {IDB_2ParamsDialogForm},
  IDB_QBuilder {IDB_QBForm},
  IDB_WG_Func,
  IDB_X_Man,
  IDB_Progress {IDB_ProgressForm},
  IDB_MovementManager {IDB_MovementForm},
  IDB_TreeView,
  IDB_ParamsOfRecordTemplate {IDB_ParamsOfRecordTemplateForm},
  IDB_UIMans,
  IDB_FormPositionMan,
  IDB_Monitor1 {IDB_Monitor1Form},
  IDB_Validators,
  IDB_StructureAndProperties {IDB_StructureAndPropertiesForm},
  IDB_StructMan,
  IDB_PolygonOverlayManager,
  IDB_FieldsVisibilityMan,
  IDB_DescMan,
  JRO_TLB,
  IDB_LayerManager,
  IDB_ParamsOfSaveLoad {IDB_ParamsOfSaveLoadForm},
  Registry,
  ADOApi,
//  ADOX_TLB,
  IDB_Statistic,
  IDB_PrintDlg,
  IDB_FormatDigits,
  ADODB_TLB in '..\..\..\Program Files\Borland\Delphi4\Imports\ADODB_TLB.pas',
  DesignIntf in '..\..\..\..\..\..\..\program files (x86)\borland\delphi7\Source\ToolsAPI\DesignIntf.pas',
  DesignMenus in '..\..\..\..\..\..\..\Program Files (x86)\Borland\Delphi7\Source\ToolsAPI\DesignMenus.pas';

var
   AnOldApplication: TApplication;
   AnOldScreen: TScreen;

{$R *.res} // This resource contains data of IDB version.
{$R IDB_EDBF_97.res} // This resource contains an empty MS Access 97 database file (mdb format).
{$R IDB_EDBF_2000.res} // This resource contains an empty MS Access 2000 database file (mdb format).
{$R IDB_EnglishLangFile.res} // This resource contains english language file.

//********     Initialization and finalization routnes of the DLL.  **********//

function InitLibrary (AnApplication: Integer; AScreen: Integer; AMainForm: Integer; ALanguageFileExtension: PChar): Boolean; stdcall;
var
// ++ Cadmensky Version 2.3.2
//   ARegistry: TRegistry;
// -- Cadmensky Version 2.3.2
   sMDACVersion, sTemp: AnsiString;
// ++ Cadmensky Version 2.3.3
   fMDACVersion: Double;
   FConnection: _Connection;
// -- Cadmensky Version 2.3.3
begin
   RESULT := false;

   // Save the current TApplication and TScreen.
   AnOldApplication := Application;
   AnOldScreen := Screen;

   // Use the received TApplication and TScreen. We need it to correct making of imported windows
   // that will be imported from dll (as well MDI as SDI windows).
   Application := TApplication (AnApplication);

   Screen := TScreen (AScreen);

   AWinGISMainForm := TForm (AMainForm);
   bCommonReadOnlyMode := false;

   sLanguageFileName := ExtractFilePath (Application.ExeName) + 'IDB.' + ALanguageFileExtension;
   if not FileExists (sLanguageFileName) then
      sLanguageFileName := ExtractFilePath (Application.ExeName) + 'IDB.044';

   sMenuIniFileName := sLanguageFileName;
   sToolbarIniFileName := sLanguageFileName;

   // Create the Language manager.
   ALangManager := TLanguageManager.Create (sLanguageFileName);
   cs_ApplicationName := ALangManager.GetValue (499);

// ++ Cadmensky Version 2.3.3
   try
      FConnection := CoConnection.Create;
      sMDACVersion := FConnection.Version;
      sTemp := ReplaceChars (sMDACVersion, '.', DecimalSeparator);
      fMDACVersion := StrToFloat (sTemp);
      if fMDACVersion < 2.5 then
      begin
            // ++ Your MDAC Version (%s) is less than 2.7
         MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (469), [sMDACVersion])), PChar (cs_ApplicationName), MB_DEFBUTTON1 + MB_ICONERROR);
         Exit;
      end;
   except
      // ++ MDAC is not installed
      MessageBox (Application.Handle, PChar (ALangManager.GetValue (468)), PChar (cs_ApplicationName), MB_DEFBUTTON1 + MB_ICONERROR);
      Exit;
   end;
// -- Cadmensky Version 2.3.3

// ++ Cadmensky Version 2.3.2
{   ARegistry := TRegistry.Create;
   ARegistry.RootKey := HKEY_LOCAL_MACHINE;
   if ARegistry.OpenKeyReadOnly ('\SOFTWARE\Microsoft\DataAccess') then
   begin
      sMDACVersion := ARegistry.ReadString ('FullInstallVer');
      try
         sTemp := ReplaceChars (Copy (sMDACVersion, 1, 3), '.', DecimalSeparator);
         fMDACVersion := StrToFloat (sTemp);
         if fMDACVersion < 2.7 then
         begin
            // ++ Your MDAC Version (%s) is less than 2.7
            MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (469), [sMDACVersion])), PChar (cs_ApplicationName), MB_DEFBUTTON1 + MB_ICONERROR);
            ARegistry.Free;
            Exit;
         end;
      except
         ARegistry.Free;
         Exit;
      end;
   end
   else
   begin
      // ++ MDAC is not installed
      MessageBox (Application.Handle, PChar (ALangManager.GetValue (468)), PChar (cs_ApplicationName), MB_DEFBUTTON1 + MB_ICONERROR);
      ARegistry.Free;
      Exit;
   end;

   ARegistry.Free;}
// -- Cadmensky Version 2.3.2

     // Try to create main IDB manager. It it fails, we'll not be able to work with the IDB.
     // AWinGISMainForm variable will be used as AOwner variable for creating some of VCL components.
   try
      AnIDBMainMan := TIDB_MainManager.Create;
   except
      EXIT;
   end;

   RESULT := true;
{     if MemCheckLogFileName <> ExtractFilePath(Application.ExeName) + 'ChkMem.txt' then
     begin
     MemCheckLogFileName := ExtractFilePath(Application.ExeName) + 'ChkMem.txt';
     MemChk;
     end;}

end;

procedure FinLibrary; stdcall;
begin
     // Delete the language manager.
   try
      if ALangManager <> nil then
         ALangManager.Free;
   except
   end;
     // Delete the IDB main manager.
   try
      if AnIDBMainMan <> nil then
         AnIDBMainMan.Free;
   except
   end;
     // Restore the old TScreen and TApplication.
   if AnOldApplication <> nil then
      Application := AnOldApplication;
   if AnOldScreen <> nil then
      Screen := AnOldScreen;
end;

function GetVersion: PChar; stdcall;
begin
   GetMem (RESULT, Length (cs_IDB_NormalVersion) + 1);
   StrPCopy (RESULT, cs_IDB_NormalVersion);
end;

procedure SetReadOnlyState (bReadOnlyState: Boolean); stdcall;
begin
   bCommonReadOnlyMode := bReadOnlyState;
end;

//****************             GeoText routines.           *******************//
{ This function returns information for the layer's item from GeoText field.
  Returned value has a string type. }

function GetGeoText (AProject: Pointer; iLayerIndex, iItemID: Integer {; pcGeoTextIDFieldName, pcGeoTextValueFieldName: PChar}): PChar; stdcall;
var
   sData: AnsiString;
begin
   sData := '';
   if AnIDBMainMan <> nil then
      sData := AnIDBMainMan.GetGeoText (AProject, iLayerIndex, iItemID {, pcGeoTextIDFieldName, pcGeoTextValueFieldName});
   GetMem (RESULT, Length (sData) + 1);
   StrPCopy (RESULT, sData);
end;

procedure SetGeoText (AProject: Pointer; iLayerIndex, iItemID: Integer; {pcGeoTextIDFieldName, pcGeoTextValueFieldName,} pcGeoTextValue: PChar); stdcall;
begin
   if AnIDBMainMan <> nil then
   begin
      if AnIDBMainMan.SetGeoText (AProject, iLayerIndex, iItemID, {pcGeoTextIDFieldName, pcGeoTextValueFieldName,} pcGeoTextValue) then
      begin
         Application.HandleMessage;
         AnIDBMainMan.RefreshAllDataWindows (AProject);
      end;
   end;
end;

procedure SetLayerForInsertCallback (AFunction: TLayerForInsert); stdcall;
begin
   LayerForInsertFunc := AFunction;
end;

function GetTableFieldsNamesList (AProject: Pointer; ALayer: Pointer): PChar; stdcall;
var
   sData: AnsiString;
begin
   sData := '';
   if AnIDBMainMan <> nil then
      sData := AnIDBMainMan.GetTableFieldsNamesList (AProject, ALayer);
   GetMem (RESULT, Length (sData) + 1);
   StrPCopy (RESULT, sData);
end;

function GetTableVisibleFieldsNamesList (AProject: Pointer; ALayer: Pointer): PChar; stdcall;
var
   sData: AnsiString;
begin
   sData := '';
   if AnIDBMainMan <> nil then
      sData := AnIDBMainMan.GetTableVisibleFieldsNamesList (AProject, ALayer);
   GetMem (RESULT, Length (sData) + 1);
   StrPCopy (RESULT, sData);
end;

//****************            IDB core routines.           *********************
{ This procedure adds a project in the project list of the IDB manager. }

function AddProject (AProjectFormHandle: Integer; AProject: Pointer; bProjectFileHasReadOnlyState: Boolean): Boolean; stdcall;
begin
   RESULT := false;
   if AnIDBMainMan <> nil then
   begin
      RESULT := AnIDBMainMan.AddProject (AProjectFormHandle, AProject, bProjectFileHasReadOnlyState);
      if not RESULT then
         PostMessage (AWinGISMainForm.Handle, WM_IDB_Message, IDB_MustBeDisconnected, Integer (AProject));
   end;
end;

{ This procedure deletes a project from the project list of the IDB manager. }

procedure DeleteProject (AProject: Pointer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.DeleteProject (AProject);
end;

{ This procedure shows an attribute window of the layer of the project. }

procedure ShowAttributeWindow (AProject, ALayer, AALayer: Pointer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.ShowAttributeWindow (AProject, ALayer, AALayer);
end;

{brovak}

function IsOpenedWindow (AProject: Pointer): integer; stdcall
begin
   if AnIDBMainMan <> nil then
      Result := AnIDBMainMan.IsOpenedWindow (AProject);
end;
{brovak}

procedure SetMakeSpecialChartCallBack (AFunction: TMakeSpecialChart); stdcall;
begin;
   MakeSpecialChartProc := AFunction;
end;

procedure SetSendSpecialforChartCallBack (AFunction: TSendSpecialforChart); stdcall;
begin;
   SendSpecialforChartProc := AFunction;
end;

procedure SetShowNewThematicMapDialogCallBack (AFunction: TShowNewThematicMapDialog); stdcall;
begin;
   ShowNewThematicMapDialogProc := AFunction;
end;

procedure SetShowPercentAtWinGISCallBack (AFunction: TShowPercentAtWinGIS); stdcall;
begin;
   ShowPercentAtWinGISProc := AFunction;
end;

procedure SetSendToTMSelectedIDCallBack (AFunction: TSendToTMSelectedID); stdcall;
begin;
   SendToTMSelectedIDProc := AFunction;
end;
{brovak}

{ This procedure closes an attribute window of the layer of the project. }

procedure CloseAttributeWindow (AProject, ALayer: Pointer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.CloseAttributeWindow (AProject, ALayer);
end;

{ This procedure closes all database windows of the project. }

procedure CloseAllWindowsOfProject (AProject: Pointer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.CloseAllWindowsOfProject (AProject);
end;

procedure CloseAllWindowsOfAllProjects; stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.CloseAllWindowsOfAllProjects;
end;

{ This procedure shows a monitoring window of the project. }

procedure ShowMonitoringWindow (AProject: Pointer; bAlwaysShowMonitoring: Boolean); stdcall;
var
   ALayer: Pointer;
begin
   if AnIDBMainMan <> nil then
// ++ Cadmensky
      if not bAlwaysShowMonitoring and AnIDBMainMan.LastWasAttributeWindow[AProject] then
      begin
         ALayer := GetActiveLayerFunc (AProject);
         if ALayer <> nil then
            ShowAttributeWindow (AProject, ALayer, nil);
      end
      else
// -- Cadmensky
         AnIDBMainMan.ShowMonitoringWindow (AProject);
end;

procedure ShowInfoWindow (AProject: Pointer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.ShowInfoWindow (AProject);
end;

procedure SetReverseUsingIDBInfoCallback (AProcedure: TReverseUsingIDBInfo); stdcall;
begin
   ReverseUsingIDBInfoProc := AProcedure;
end;

procedure ClearInfoWindow (AProject: Pointer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.ClearInfoWindow (AProject);
end;

procedure CloseInfoWindow; stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.CloseInfoWindow;
end;

procedure HideInfoWindow; stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.HideInfoWindow;
end;

procedure RefreshAllDataWindows (AProject: Pointer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.RefreshAllDataWindows (AProject);
end;

//****************   WinGIS <-> IDB interaction routines.   ********************

procedure SetShowProjectCallback (AProcedure: TShowProject); stdcall;
begin
   ShowProjectProc := AProcedure;
end;

procedure SetShowSelectedItemsInProjectCallback (AProcedure: TShowSelectedItemsInProject); stdcall;
begin
   ShowSelectedItemsInProjectProc := AProcedure;
end;

procedure SetShowItemInProjectWithZoomCallback (AProcedure: TShowItemInProjectWithZoom); stdcall;
begin
   ShowItemInProjectWithZoomProc := AProcedure;
end;

procedure SetCheckItemExistingCallback (AFunction: TItemExistsInProject); stdcall;
begin
   ItemExistsInProjectFunc := AFunction;
end;

procedure SetItemToGuidCallback (AFunction: TItemToGuid); stdcall;
begin
   ItemToGuidFunc := AFunction;
end;

procedure SetSetProjectModifiedCallback (AProcedure: TSetProjectModified); stdcall;
begin
   SetProjectModifiedProc := AProcedure;
end;

procedure SetSetStartProjectGUIDCallback (AProcedure: TSetStartProjectGUID); stdcall;
begin
   SetStartProjectGUIDProc := AProcedure;
end;

procedure SetSetCurrentProjectGUIDCallback (AProcedure: TSetCurrentProjectGUID); stdcall;
begin
   SetCurrentProjectGUIDProc := AProcedure;
end;

procedure SetGetStartProjectGUIDCallback (AFunction: TGetStartProjectGUID); stdcall;
begin
   GetStartProjectGUIDFunc := AFunction;
end;

procedure SetGetCurrentProjectGUIDCallback (AFunction: TGetCurrentProjectGUID); stdcall;
begin
   GetCurrentProjectGUIDFunc := AFunction;
end;

procedure SetGetProjectIsNewAndNotSavedCallback (AFunction: TGetProjectIsNewAndNotSaved); stdcall;
begin
   GetProjectIsNewAndNotSavedStateFunc := AFunction;
end;

procedure SetGetProjectFullFileNameCallback (AFunction: TGetProjectFullFileName); stdcall;
begin
   GetProjectFullFileNameFunc := AFunction;
end;

// ++ Cadmensky Version 2.2.8

procedure SetGetLayerItemsCountCallback (AFunction: TGetLayerItemsCount); stdcall;
begin
   GetLayerItemsCountFunc := AFunction;
end;

procedure SetGetLayerItemIndexByNumberCallback (AFunction: TGetLayerItemIndexByNumber); stdcall;
begin
   GetLayerItemIndexByNumberFunc := AFunction;
end;

procedure SetIsObjectSelectedCallback (AFunction: TIsObjectSelected); stdcall;
begin
   IsObjectSelectedFunc := AFunction;
end;
// -- Cadmensky Version 2.2.8

procedure SetGetLayerNameCallback (AFunction: TGetLayerName); stdcall;
begin
   GetLayerNameFunc := AFunction;
end;

procedure SetGetLayerIndexCallback (AFunction: TGetLayerIndex); stdcall;
begin
   GetLayerIndexFunc := AFunction;
end;

procedure SetGetAllProjectWindowsNumberCallback (AFunction: TGetAllProjectWindowsNumber); stdcall;
begin
   GetAllProjectWindowsNumberFunc := AFunction;
end;

procedure SetGetProjectLayerNumberCallback (AFunction: TGetProjectLayerNumber); stdcall;
begin
   GetProjectLayerNumberFunc := AFunction;
end;

procedure SetGetLayerPointerByPositionCallback (AFunction: TGetLayerPointerByPosition); stdcall;
begin
   GetLayerPointerByPositionFunc := AFunction;
end;

procedure SetGetLayerPointerByIndexCallback (AFunction: TGetLayerPointerByIndex); stdcall;
begin
   GetLayerPointerByIndexFunc := AFunction;
end;

procedure SetGetActiveLayerCallback (AFunction: TGetActiveLayer); stdcall;
begin
   GetActiveLayerFunc := AFunction;
end;

procedure SetLayerIsFixedCallback (AFunction: TLayerIsFixed); stdcall;
begin
   LayerIsFixedFunc := AFunction;
end;

procedure SetGetWinGISINIFileNameCallback (AFunction: TGetWinGISINIFileName); stdcall;
begin
   GetWinGISINIFileNameFunc := AFunction;
end;

procedure SetGetDefaultProjectFileNameCallback (AFunction: TGetDefaultProjectFileName); stdcall;
begin
   GetDefaultProjectFileNameFunc := AFunction;
end;

procedure SetGeneratePointCallback (AFunction: TGeneratePoint); stdcall;
begin
   GeneratePointFunc := AFunction;
end;

procedure SetGenerateSymbolCallback (AFunction: TGenerateSymbol); stdcall;
begin
   GenerateSymbolFunc := AFunction;
end;

procedure SetAnnotationSettingsDialogExecuteCallback (AFunction: TAnnotationSettingsDialogExecute); stdcall;
begin
   AnnotationSettingsDialogExecuteFunc := AFunction;
end;

procedure SetMakeAnnotationInProjectCallback (AFunction: TMakeAnnotationInProject); stdcall;
begin
   MakeAnnotationInProjectFunc := AFunction;
end;
{   brovak}

procedure SetChartSettingsDialogExecuteCallback (AFunction: TChartSettingsDialogExecute); stdcall;
begin
   ChartSettingsDialogExecuteFunc := AFunction;
end;

procedure SetMakeChartInProjectCallback (AFunction: TMakeChartInProject); stdcall;
begin
   MakeChartInProjectFunc := AFunction;
end;

//
(*procedure MakeSpecialChartCallback(AFunction: TMakeSpecialChart);stdcall;
 begin;
   MakeSpecialChartProc:=AFunction;
 end;

procedure SendSpecialforChartCallback(AFunction:TSendSpecialforChart);stdcall;
 begin;
   SendSpecialforChartProc:=AFunction;
 end; *)
{brovak}

procedure SetShowDonePercentCallback (AFunction: TShowDonePercent); stdcall;
begin
   ShowDonePercentProc := AFunction;
end;

// ++ Commented by Cadmensky Version 2.2.8
{procedure SetGatherGarbageInProjectCallback (AFunction: TGatherGarbageInProject); stdcall;
begin
   GatherGarbageInProjectFunc := AFunction;
end;

procedure SetGatherSelectedGarbageInProjectCallback (AFunction: TGatherGarbageInProject); stdcall;
begin
   GatherSelectedGarbageInProjectFunc := AFunction;
end;}
// -- Commented by Cadmensky Version 2.2.8

procedure SetRedrawProjectWindowCallback (AProcedure: TRedrawProjectWindow); stdcall;
begin
   RedrawProjectWindowProc := AProcedure;
end;

procedure SetDeselectAllObjectsCallback (AProcedure: TDeselectAllObjects); stdcall;
begin
   DeselectAllObjectsProc := AProcedure;
end;

procedure SetChangeGeoTextParametersCallback (AProcedure: TChangeGeoTextParameters); stdcall;
begin
   ChangeGeoTextParametersProc := AProcedure;
end;

procedure SetRunThematicMapBuilderCallback (AFunction: TRunThematicMapBuilder); stdcall;
begin
   RunThematicMapBuilderFunc := AFunction;
end;

procedure SetRunMonitoringCallback (AProcedure: TRunMonitoring); stdcall;
begin
   RunMonitoringProc := AProcedure;
end;

procedure SetGetObjectPropsCallback (AFunction: TGetObjectProps); stdcall;
begin
   GetObjectPropsFunc := AFunction;
end;

procedure SetGetMDIChildWindowStateCallback (AFunction: TGetMDIChildWindowState); stdcall;
begin
   GetMDIChildWindowStateFunc := AFunction;
end;

procedure SetSetNessesitySaveUndoDataSignCallback (AProcedure: TSetNessesitySaveUndoDataSign); stdcall;
begin
   SetNessesitySaveUndoDataSignProc := AProcedure;
end;

procedure SetSaveUndoDataCallback (AProcedure: TSaveUndoData); stdcall;
begin
   SaveUndoDataProc := AProcedure;
end;

{ This function returns ID of an object that was deleted from IDB user interface.
  if there isn't such project or such layer the function returns a negative value.
  if there isn't any object to be deleted in ALayer of AProject, the function
  returns the negative value, too. }

function GetItemIDToBeDeleted_FromIDB (AProject: Pointer; ALayer: Pointer): Integer; stdcall;
begin
   RESULT := -1;
   if AnIDBMainMan <> nil then
      RESULT := AnIDBMainMan.GetItemIDToBeDeleted_FromIDB (AProject, ALayer);
end;

{ These functions provide deleting data from IDB of objects that were deleted from WinGIS
  user interface. This interaction goes in three steps:
  1. We need to command to get ready to receive IDs of the deleted objects.
  2. We transmite IDs.
  3. We command to make deleteing operations itself. }

function GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG (AProject: Pointer): Boolean; stdcall;
begin
   RESULT := false;
   if AnIDBMainMan <> nil then
      RESULT := AnIDBMainMan.GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG (AProject);
end;

procedure ReceiveIDOfObjectToBeDeleted_FromWG (AProject, ALayer: Pointer; iObjectID: Integer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.ReceiveIDOfObjectToBeDeleted_FromWG (AProject, ALayer, iObjectID);
end;

procedure DeleteObjects_FromWG (AProject: Pointer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.DeleteObjects_FromWG (AProject);
end;

procedure ProjectIsBeingSaved (AProject: Pointer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.ProjectIsBeingSaved (AProject);
end;

procedure SaveDatabaseAs (AProject: Pointer; pcNewProjectName: PChar); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.SaveDatabaseAs (AProject, pcNewProjectName);
end;

procedure DeleteProjectAndDatabaseFiles (pcProjectFileName: PChar); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.DeleteProjectAndDatabaseFiles (pcProjectFileName);
end;

function GetReadyToReceiveIDsOfObjectsForMonitoring (AProject: Pointer): Boolean; stdcall;
begin
   RESULT := false;
   if AnIDBMainMan <> nil then
      RESULT := AnIDBMainMan.GetReadyToReceiveIDsOfObjectsForMonitoring (AProject);
end;

procedure ReceiveIDOfObjectForMonitoring (AProject, ALayer: Pointer; iObjectID: Integer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.ReceiveIDOfObjectForMonitoring (AProject, ALayer, iObjectID);
end;

procedure SetGeoTextFieldNames (AProject, ALayer: Pointer; pcGeotextIDFieldname, pcGeoTextFieldName: PChar); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.SetGeoTextFieldNames (AProject, ALayer, pcGeotextIDFieldname, pcGeoTextFieldName);
end;

function GetGeoTextIDFieldName (AProject, ALayer: Pointer): PChar; stdcall;
var
   sData: AnsiString;
begin
   sData := '';
   if AnIDBMainMan <> nil then
   begin
{
        sData := AnIDBMainMan.GetGeoTextIDFieldName(AProject, ALayer);
        if Trim(sData) = '' then sData := 'ProgisID';}
      sData := 'ProgisID';
   end;
   GetMem (RESULT, Length (sData) + 1);
   StrPCopy (RESULT, sData);
end;

function GetGeoTextFieldName (AProject, ALayer: Pointer): PChar; stdcall;
var
   sData: AnsiString;
begin
   if AnIDBMainMan <> nil then
   begin
      sData := AnIDBMainMan.GetGeoTextFieldName (AProject, ALayer);
      if Trim (sData) = '' then sData := 'GeoTextInfo';
   end;
   GetMem (RESULT, Length (sData) + 1);
   StrPCopy (RESULT, sData);
end;

procedure InsertItem (AProject, ALayer: Pointer; iNewItemIndex: Integer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.InsertItem (AProject, ALayer, iNewItemIndex);
end;

// ++ Cadmensky

procedure OverlayInsertItem (AProject, ALayer: Pointer; iNewItemIndex, iAItemIndex, iBItemIndex: Integer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.OverlayInsertItem (AProject, ALayer, iNewItemIndex, iAItemIndex, iBItemIndex);
end;
// -- Cadmensky

// ++ Cadmensky Version 2.2.8

function MoveAttributeInfo (AProject, ADestLayer: Pointer): Integer; stdcall;
begin
   if AnIDBMainMan <> nil then     
      RESULT := AnIDBMainMan.MoveAttributeInfo (AProject, ADestLayer);
end;
// -- Cadmensky Version 2.2.8

function CopyAttributeInfo (AProject, ASourceLayer, ADestLayer: Pointer; iSourceItemID, iDestItemID: Integer): integer; stdcall;
begin
   if AnIDBMainMan <> nil then
      RESULT := AnIDBMainMan.CopyAttributeInfo (AProject, ASourceLayer, ADestLayer, iSourceItemID, iDestItemID);
end;

// ++ Commented by Cadmensky IDB Version 2.2.8
{function GetReadyToInsertRecords (AProject, ALayer: Pointer; ARecordTemplate: TList): Integer; stdcall;
begin
   RESULT := -1;
   if AnIDBMainMan <> nil then
      RESULT := AnIDBMainMan.GetReadyToInsertRecords (AProject, ALayer, ARecordTemplate);
end;

// ++ Cadmensky

function InsertRecord (AProject, ALayer: Pointer; iLayerItemID: Integer): Boolean; stdcall;
//function InsertRecord(iPosition, iLayerItemID: Integer): Boolean; stdcall;
begin
   RESULT := false;
   if AnIDBMainMan <> nil then
      RESULT := AnIDBMainMan.InsertRecord (AProject, ALayer, iLayerItemID);
//        RESULT := AnIDBMainMan.InsertRecord(iPosition, iLayerItemID);
end;
// ++ Cadmensky

procedure EndInserting (iPosition: Integer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.EndInserting (iPosition);
end;}
// -- Commented by Cadmensky IDB Version 2.2.8

function GetReadyToPolygonOverlaying (AProject, ASourceLayer1, ASourceLayer2, ADestinationLayer: Pointer): Boolean; stdcall;
begin
   if AnIDBMainMan <> nil then
      RESULT := AnIDBMainMan.GetReadyToPolygonOverlaying (AProject, ASourceLayer1, ASourceLayer2, ADestinationLayer);
end;

procedure EndOfPolygonOverlaying (AProject: Pointer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.EndOfPolygonOverlaying (AProject);
end;

procedure Set_DoNotAddAttributeRecords_Sign (AProject: Pointer; bValue: Boolean); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.Set_DoNotAddAttributeRecords_Sign (AProject, bValue);
end;

procedure DeleteLayerTables (AProject, ALayer: Pointer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.DeleteLayerTables (AProject, ALayer);
end;

function LoadGeoTextData (AProject, ALayer: Pointer; pcSourceDBFileName, pcSourceFieldName, pcDestinationFieldName, pcProgisIDFieldName: PChar): Boolean; stdcall;
begin
   RESULT := false;
   if AnIDBMainMan <> nil then
      RESULT := AnIDBMainMan.LoadGeoTextData (AProject, ALayer, pcSourceDBFileName, pcSourceFieldName, pcDestinationFieldName, pcProgisIDFieldName);
end;

function GetReadyToReceiveIDsOfObjectsToBeMade (AProject: Pointer): Boolean; stdcall;
begin
   RESULT := false;
   if AnIDBMainMan <> nil then
      RESULT := AnIDBMainMan.GetReadyToReceiveIDsOfObjectsToBeMade (AProject);
end;

procedure ReceiveIDOfObjectToBeMade (AProject, ALayer: Pointer; iItemID: Integer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.ReceiveIDOfObjectToBeMade (AProject, ALayer, iItemID);
end;

procedure MakeAnnotations (AProject: Pointer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.MakeAnnotations (AProject);
end;

procedure UpdateAnnotationData (AProject: Pointer; iLayerIndex, iItemID: Integer; AnAnnotData: PCollection); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.UpdateAnnotationData (AProject, iLayerIndex, iItemID, AnAnnotData);
end;
{brovak}

procedure MakeChart (AProject: Pointer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.MakeChart (AProject);
end;

procedure UpdateChartData (AProject: Pointer; iLayerIndex, iItemID: Integer; AnChartData: PCollection); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.UpdateChartData (AProject, iLayerIndex, iItemID, AnChartData);
end;
{brovak}

procedure SetMakeAnnotationUpdateCallback (AProcedure: TMakeAnnotationUpdate); stdcall;
begin
   MakeAnnotationUpdateProc := AProcedure;
end;
{brovak}

procedure SetMakeChartUpdateCallback (AProcedure: TMakeChartUpdate); stdcall;
begin
   MakeChartUpdateProc := AProcedure;
end;
{brovak}

function GetReadyToReceiveIDsOfObjectsToBeRestored (AProject: Pointer): Boolean; stdcall;
begin
   RESULT := false;
   if AnIDBMainMan <> nil then
      RESULT := AnIDBMainMan.GetReadyToReceiveIDsOfObjectsToBeRestored (AProject);
end;

procedure ReceiveIDOfObjectToBeRestored (AProject: Pointer; ALayer: Pointer; iObjectID: Integer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.ReceiveIDOfObjectToBeRestored (AProject, ALayer, iObjectID);
end;

// ++ Cadmensky

procedure UpdateCalcFields (AProject: Pointer; ALayer: Pointer; iObjectID: Integer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.UpdateCalcFields (AProject, ALayer, iObjectID);
end;
// -- Cadmensky

procedure ExecuteRestoring (AProject: Pointer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.ExecuteRestoring (AProject);
end;

// ++ Commented by Cadmensky Version 2.3.0
{function GetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDrop (AProject: Pointer): Boolean; stdcall;
begin
   RESULT := false;
   if AnIDBMainMan <> nil then
      RESULT := AnIDBMainMan.GetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDrop (AProject);
end;

procedure ReceiveIDOfObjectToBeUsedForDragAndDrop (AProject, ALayer: Pointer; iObjectID: Integer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.ReceiveIDOfObjectToBeUsedForDragAndDrop (AProject, ALayer, iObjectID);
end;

procedure ExecuteDragAndDrop (AProject: Pointer; ASourceGridOfDragging: Pointer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.ExecuteDragAndDrop (AProject, ASourceGridOfDragging);
end;}
// -- Commented by Cadmensky Version 2.3.0

function GetActiveWindowProject: Pointer; stdcall;
var
   AForm: TForm;
begin
   RESULT := nil;
   AForm := AWinGISMainForm.ActiveMDIChild;
   if AForm is TIDB_ViewForm then
   begin
      RESULT := TIDB_ViewForm (AForm).AProject;
      EXIT;
   end;
   if AForm is TIDB_MonitorForm then
      RESULT := TIDB_MonitorForm (AForm).AProject;
end;

// AX connection

function X_GotoObjectRecord (AProject, ALayer: Pointer; iItemIndex: Integer): Boolean; stdcall;
begin
   RESULT := false;
   if AnIDBMainMan <> nil then
      RESULT := AnIDBMainMan.X_GotoObjectRecord (AProject, ALayer, iItemIndex);
end;

function X_AddRecord (AProject, ALayer: Pointer; iItemIndex: Integer): Boolean; stdcall;
begin
   RESULT := false;
   if AnIDBMainMan <> nil then
      RESULT := AnIDBMainMan.X_AddRecord (AProject, ALayer, iItemIndex);
end;

function X_GetFieldCount (AProject, ALayer: Pointer): Integer; stdcall;
begin
   RESULT := 0;
   if AnIDBMainMan <> nil then
      RESULT := AnIDBMainMan.X_GetFieldCount (AProject, ALayer);
end;

function X_GetFieldName (AProject, ALayer: Pointer; iFieldIndex: Integer): PChar; stdcall;
var
   sData: AnsiString;
begin
   sData := 'ProgisID';
   if AnIDBMainMan <> nil then
   begin;
      sData := AnIDBMainMan.X_GetFieldName (AProject, ALayer, iFieldIndex);
      if Trim (sData) = '' then sData := 'ProgisID';
   end;
   GetMem (RESULT, Length (sData) + 1);
   StrPCopy (RESULT, sData);
end;

function X_GetFieldValueByFieldName (AProject, ALayer: Pointer; pcFieldName: PChar): Variant; stdcall;
begin
   RESULT := 0;
   if AnIDBMainMan <> nil then
      RESULT := AnIDBMainMan.X_GetFieldValue (AProject, ALayer, pcFieldName);
end;

function X_GetFieldValueByFieldIndex (AProject, ALayer: Pointer; iFieldIndex: Integer): Variant; stdcall;
begin
   RESULT := 0;
   if AnIDBMainMan <> nil then
      RESULT := AnIDBMainMan.X_GetFieldValue (AProject, ALayer, iFieldIndex);
end;

procedure X_SetFieldValueByFieldName (AProject, ALayer: Pointer; pcFieldName: PChar; FieldValue: Variant); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.X_SetFieldValue (AProject, ALayer, pcFieldName, FieldValue);
end;

procedure X_SetFieldValueByFieldIndex (AProject, ALayer: Pointer; iFieldIndex: Integer; FieldValue: Variant); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.X_SetFieldValue (AProject, ALayer, iFieldIndex, FieldValue);
end;

{ This procedure returns true if the user wants to cancel an operation in the database window
  (reconciling, for example) and false otherwise. }

function TheUserWantsToCancel (AWindow: TForm): Boolean; stdcall;
begin
   if AWindow is TIDB_ViewForm then
      RESULT := TIDB_ViewForm (AWindow).TheUserWantsToCancel
   else
      if AWindow is TIDB_MonitorForm then
         RESULT := TIDB_MonitorForm (AWindow).TheUserWantsToCancel
      else
         RESULT := false;
end;

procedure ShowRecordsForSelectedObjects (AProject: Pointer); stdcall;
begin
   if AnIDBMainMan <> nil then
      AnIDBMainMan.ShowRecordsForSelectedObjects (AProject);
end;

procedure SaveAnnotationSettings(AProject: Pointer;
  iLayerindex: Integer; ASettingsList: ansiString); stdcall;
begin
if AnIDBMainMan <> nil then
      AnIDBMainMan.SaveAnnotationSettings(AProject, iLayerIndex, ASettingsList);
end;

function GetAnnotationSettings(AProject: Pointer; iLayerindex: Integer): AnsiString; stdcall;
begin
if AnIDBMainMan <> nil then
      Result:= AnIDBMainMan.GetAnnotationSettings(AProject, iLayerIndex);
end;

//Facilityman
function F_GetLinesPerLayer(AProject: Pointer; Layername: Pchar): Pchar; StdCall;
begin
if AnIDBMainMan <> nil then
      Result:= AnIDBMainMan.F_GetLinesPerLayer(AProject, Layername);
end;

function F_GetSymbolsPerLayer(AProject: Pointer; Layername: Pchar): Pchar; StdCall;
begin
if AnIDBMainMan <> nil then
      Result:= AnIDBMainMan.F_GetSymbolsPerLayer(AProject, Layername);
      end;

function F_SymbolAllowed(AProject: Pointer; Symbolname, LineStyleName: Pchar): Pchar; StdCall;
begin
if AnIDBMainMan <> nil then
      Result:= AnIDBMainMan.F_Symbolallowed(AProject, Symbolname, LineStylename);
end;

exports
   //********     Initialization and finalization routnes of the DLL.  **********//
   InitLibrary,
   FinLibrary,

   GetVersion,
   SetReadOnlyState,
   //****************             GeoText routines.           *******************//
   GetGeoText,
   SetGeoText,
   GetTableFieldsNamesList,
   GetTableVisibleFieldsNamesList,
   //****************            IDB core routines.           *******************//
   AddProject,
   DeleteProject,
   SetMakeSpecialChartCallback,
   SetSendSpecialforChartCallBack,
   ShowAttributeWindow,
   CloseAttributeWindow,
   ShowMonitoringWindow,
   ShowInfoWindow,
   ClearInfoWindow,
   CloseInfoWindow,
   HideInfoWindow,
   RefreshAllDataWindows,
   CloseAllWindowsOfProject,
   CloseAllWindowsOfAllProjects,
{brovak}
   IsOpenedWindow,
{brovak}
   ProjectIsBeingSaved,
   DeleteLayerTables,

   LoadGeoTextData,

   GetActiveWindowProject,

   X_GotoObjectRecord,
   X_AddRecord,
   X_GetFieldCount,
   X_GetFieldName,
   X_GetFieldValueByFieldName,
   X_GetFieldValueByFieldIndex,
   X_SetFieldValueByFieldName,
   X_SetFieldValueByFieldIndex,

   UpdateAnnotationData,
   SetMakeAnnotationUpdateCallback,
{brovak}
   UpdateChartData,
   SetMakeChartUpdateCallback,
{brovak}
   SetShowProjectCallback,
   SetShowSelectedItemsInProjectCallback,
   SetShowItemInProjectWithZoomCallback,
   SetDeselectAllObjectsCallback,
   SetCheckItemExistingCallback,
   SetItemToGuidCallback,
   SetSetProjectModifiedCallback,

   SetSetStartProjectGUIDCallback,
   SetSetCurrentProjectGUIDCallback,
   SetGetStartProjectGUIDCallback,
   SetGetCurrentProjectGUIDCallback,
   SetGetProjectIsNewAndNotSavedCallback,
   SetGetProjectFullFileNameCallback,
// ++ Cadmensky Version 2.2.8
   SetGetLayerItemsCountCallback,
   SetGetLayerItemIndexByNumberCallback,
   SetIsObjectSelectedCallback,
// -- Cadmensky Version 2.2.8
   SetGetLayerNameCallback,
   SetGetLayerIndexCallback,
   SetGetAllProjectWindowsNumberCallback,
   SetGetProjectLayerNumberCallback,
   SetGetLayerPointerByPositionCallback,
   SetGetLayerPointerByIndexCallback,
// ++ Cadmensky
   SetGetActiveLayerCallback,
// -- Cadmensky
   SetLayerIsFixedCallback,
   SetGetWinGISINIFileNameCallback,
   SetGetDefaultProjectFileNameCallback,

   SetGeneratePointCallback,
   SetGenerateSymbolCallback,

   SetAnnotationSettingsDialogExecuteCallback,
   SetMakeAnnotationInProjectCallback,
// ++ Cadmensky
// ++ UndoRedo
   SetSetNessesitySaveUndoDataSignCallback,
   SetSaveUndoDataCallback,
// -- UndoRedo
// -- Cadmensky
   {brovak}
   SetChartSettingsDialogExecuteCallback,
   SetMakeChartInProjectCallback,
   SetLayerForInsertCallback,

   {brovak}
   SetShowDonePercentCallback,
   SetShowNewThematicMapDialogCallBack,
   SetShowPercentAtWinGISCallBack,
   SetSendToTMSelectedIDCallBack,
// ++ Commented by Cadmensky Version 2.2.8
{   SetGatherGarbageInProjectCallback,
   SetGatherSelectedGarbageInProjectCallback,}
// -- Commented by Cadmensky Version 2.2.8
   SetRedrawProjectWindowCallback,
   SetChangeGeoTextParametersCallback,
   SetRunThematicMapBuilderCallback,
   SetRunMonitoringCallback,
   SetGetObjectPropsCallback,
   SetReverseUsingIDBInfoCallback,
   SetGetMDIChildWindowStateCallback,

   GetItemIDToBeDeleted_FromIDB,
   GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG,
   ReceiveIDOfObjectToBeDeleted_FromWG,
   DeleteObjects_FromWG,

   GetReadyToReceiveIDsOfObjectsForMonitoring,
   ReceiveIDOfObjectForMonitoring,

   SaveDatabaseAs,
   DeleteProjectAndDatabaseFiles,

   SetGeoTextFieldNames,
   GetGeotextIDFieldName,
   GetGeoTextFieldName,

   InsertItem,
// ++ Cadmensky
   OverlayInsertItem,
// -- Cadmensky
// ++ Cadmensky Version 2.2.8
   MoveAttributeInfo,
// -- Cadmensky Version 2.2.8
   CopyAttributeInfo,

// ++ Commented by Cadmensky IDB Version 2.2.8
{   GetReadyToInsertRecords,
   InsertRecord,
   EndInserting,}
// -- Commented by Cadmensky IDB Version 2.2.8

   GetReadyToPolygonOverlaying,
   EndOfPolygonOverlaying,

   Set_DoNotAddAttributeRecords_Sign,

   GetReadyToReceiveIDsOfObjectsToBeRestored,
   ReceiveIDOfObjectToBeRestored,
   ExecuteRestoring,

   UpdateCalcFields, // Cadmensky

// ++ Commented by Cadmensky Version 2.3.0
{   GetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDrop,
   ReceiveIDOfObjectToBeUsedForDragAndDrop,
   ExecuteDragAndDrop,}
// -- Commented by Cadmensky Version 2.3.0

   GetReadyToReceiveIDsOfObjectsToBeMade,
   ReceiveIDOfObjectToBeMade,
   MakeAnnotations,
{brovak}
   MakeChart,
{brovak}
   TheUserWantsToCancel,
// ++ Cadmensky
   ShowRecordsForSelectedObjects,
   SaveAnnotationSettings, //Annotations
   GetAnnotationSettings,
   //Facility Management
   F_GetLinesPerLayer,
   F_GetSymbolsPerLayer,
   F_Symbolallowed;

begin

end.

