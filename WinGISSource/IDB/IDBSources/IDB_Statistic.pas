unit IDB_Statistic;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, DTables, IDB_Consts, IDB_LanguageManager;

type
  TStatisticForm = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Label6: TLabel;
    Label7: TLabel;
    Bevel3: TBevel;
    Button1: TButton;
    Datensaetze: TEdit;
    Spaltenanzahl: TEdit;
    Minimalwert: TEdit;
    durchschnitt: TEdit;
    maximalwert: TEdit;
    summe: TEdit;
    ohnewert: TEdit;
    Label8: TLabel;
    ComboBoxColumn: TComboBox;
    StatusLabel: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ComboBoxColumnChange(Sender: TObject);
    procedure progress(stufe: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  StatisticForm: TStatisticForm;
  wherestring: string;

implementation

{$R *.DFM}

uses IDB_View, Variants;

function ExtractValue(AValue: OLEVariant): AnsiString;
begin
  Result := '';
  if aValue <> NULL then
    Result := AValue;
end;

function ExtractValue1(AValue: OLEVariant): longint;
begin
  Result := 0;
  if AValue <> NULL then
    Result := AValue;
end;

procedure TStatisticForm.Button1Click(Sender: TObject);
begin
  close;
end;

procedure TStatisticForm.FormCreate(Sender: TObject);
var
  i: integer;
  s: string;
begin
  ALangManager.MakeReplacement(SELF);
  Datensaetze.Text := inttostr(TIDB_ViewForm(self.Owner).Dataquery.RecordCount);
  for i := 0 to TIDB_ViewForm(self.Owner).Dataquery.FieldList.Count - 1 do
    if (TIDB_ViewForm(self.Owner).FieldNoByName(TIDB_ViewForm(self.Owner).Dataquery.FieldList.Strings[i]) <> -1)
      and (TIDB_ViewForm(self.Owner).Dataquery.FieldList.Strings[i] <> cs_InternalID_FieldName)
      and (TIDB_ViewForm(self.Owner).Dataquery.FieldList.Strings[i] <> cs_StateFieldName) then
      ComboboxColumn.Items.Add(TIDB_ViewForm(self.Owner).Dataquery.FieldList.Strings[i]);
  Spaltenanzahl.Text := inttostr(ComboboxColumn.Items.Count);
  s := stringreplace(TIDB_ViewForm(self.Owner).Dataquery.SQL.Text, #12, ' ', [rfReplaceAll]);
  i := pos('WHERE', uppercase(s));
  if i < 10 then
    wherestring := ''
  else
    wherestring := {' ' + } copy(s, i, length(s)) {+ ' '};
end;

procedure TStatisticForm.ComboBoxColumnChange(Sender: TObject);
var
  columnname, sSQL: ansistring;
  myQuery: TDQuery;
begin
  if ComboBoxColumn.ItemIndex < 0 then
    exit;
  columnname := '[' + ComboBoxColumn.Text + ']';
  sSQL := ' FROM [' + TIDB_ViewForm(self.Owner).sAttTableName + '] ' + wherestring;
  myQuery := TDQuery.create(self);
  myQuery.Master := TIDB_ViewForm(self.Owner).ALayerManager.DataAccessObject.DMaster;
// max
  try
    progress(1);
    myQuery.Sql.Text := 'SELECT MAX(' + columnname + ')' + sSQL;
    myQuery.Open;
    if myQuery.RecordCount > 0 then
      Maximalwert.text := extractvalue(myQuery.Fields[0].Value)
    else
      Maximalwert.text := '-';
    myQuery.Close;
  except Maximalwert.Text := '-';
  end;
//min
  try
    progress(2);
    myQuery.Sql.Text := 'SELECT MIN(' + columnname + ')' + sSQL;
    myQuery.Open;
    if myQuery.RecordCount > 0 then
      minimalwert.text := extractvalue(myQuery.Fields[0].Value)
    else
      minimalwert.text := '-';
    myQuery.Close;
  except minimalwert.text := '-';
  end;
// durchschn.
  try
    progress(3);
    myQuery.Sql.Text := 'SELECT AVG(' + columnname + ')' + sSQL;
    myQuery.Open;
    if myQuery.RecordCount > 0 then
      Durchschnitt.text := extractvalue(myQuery.Fields[0].Value)
    else
      Durchschnitt.text := '-';
    myQuery.Close;
  except Durchschnitt.text := '-'
  end;
// summe
  try
    progress(4);
    myQuery.Sql.Text := 'SELECT SUM(' + columnname + ')' + sSQL;
    myQuery.Open;
    if myQuery.RecordCount > 0 then
      Summe.text := extractvalue(myQuery.Fields[0].Value)
    else
      Summe.text := '-';
    myQuery.Close;
  except Summe.text := '-';
  end;
// NUll
  try
    progress(5);
    myQuery.Sql.Text := 'SELECT COUNT(' + columnname + ')' + sSQL;
    if pos(' WHERE ', myQuery.SQL.Text) > 5 then
      myQuery.Sql.Text := myQuery.Sql.Text + ' AND (' + columnname + ' IS NOT NULL)'
    else
      myQuery.Sql.Text := myQuery.Sql.Text + ' WHERE (' + columnname + ' IS NOT NULL)';
    myQuery.Open;
    if myQuery.RecordCount > 0 then
      ohnewert.text := inttostr(strtointdef(datensaetze.text, 0) - extractvalue1(myQuery.Fields[0].Value))
    else
      ohnewert.text := '-';
    myQuery.Close;
  except ohnewert.text := '-';
  end;
  progress(0);
  myQuery.Free;
end;

procedure TStatisticForm.progress(stufe: Integer);
var
  i: Integer;
begin
  statuslabel.caption := '';
  for i := 1 to stufe do
    statuslabel.caption := statuslabel.caption + '. ';
  application.ProcessMessages;
end;

end.

