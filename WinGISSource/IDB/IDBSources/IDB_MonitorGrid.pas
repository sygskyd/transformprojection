unit IDB_MonitorGrid;
{
Internal database.
Form is used for the imaging monitoring data into TStringGrid component.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 21-03-2001
}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Grids, ComCtrls, Buttons, Gauges, Menus,
  IDB_DataMan, IDB_MainManager;

type
  TSortType = (stAscending, stDescending);

  TIDB_MonitorGridForm = class(TForm)
    Panel2: TPanel;
    BottomPanel: TPanel;
    Button1: TButton;
    StringGrid1: TStringGrid;
    HeaderPanel: TPanel;
    SBSave: TSpeedButton;
    SaveDialog1: TSaveDialog;
    Gauge1: TGauge;
    SBSortAsc: TSpeedButton;
    SBSortDesc: TSpeedButton;
    PopupMenu1: TPopupMenu;
    SavePopUp: TMenuItem;
    N1: TMenuItem;
    SortAscPopUp: TMenuItem;
    SortDescPopUp: TMenuItem;
    procedure StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure StringGrid1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SBSortAscClick(Sender: TObject);
    procedure SBSortDescClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    iSelectedColumn: Integer;
    AMainManager: TIDB_MainManager;
    AProject: Pointer;

    AnOldEventHandler: TNotifyEvent;
    Procedure RestoreApp(SENDER: TObject);

    Procedure Sort(SortType: TSortType; iColumnsIndex: Integer; iLo, iHi: Integer);

    Procedure MinimizeApp(var AMsg: TWMSysCommand); message WM_SYSCommand;
  public
    { Public declarations }
    Constructor Create(AOwner: TComponent; AMainManager: TIDB_MainManager; AProject: Pointer);
  end;

implementation
{$R *.DFM}

Uses
     IDB_LanguageManager, IDB_CallbacksDef;

Procedure TIDB_MonitorGridForm.RestoreApp(SENDER: TObject);
Begin
     SELF.Show;
     Application.OnRestore := AnOldEventHandler;
End;

Constructor TIDB_MonitorGridForm.Create(AOwner: TComponent; AMainManager: TIDB_MainManager; AProject: Pointer);
Begin
     Inherited Create(AOwner);
     SELF.AMainManager := AMainManager;
     SELF.AProject := AProject;
End;

Procedure TIDB_MonitorGridForm.MinimizeApp(var AMsg: TWMSysCommand);
Begin
     If AMsg.CmdType = SC_MINIMIZE Then Begin
        AnOldEventHandler := Application.OnRestore;
        Application.OnRestore := SELF.RestoreApp;
        Application.Minimize;
        End
     Else
        Inherited;
End;

procedure TIDB_MonitorGridForm.StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  Procedure DrawDownBtn;
  Begin
       DrawFrameControl(StringGrid1.Canvas.Handle, Rect, DFC_Button, DFCS_ButtonPush OR DFCS_AdjustRect OR DFCS_Pushed);
  End;
Var
   sText: AnsiString;
begin
     If ARow > 0 Then Begin
        If gdSelected in State Then StringGrid1.Canvas.Brush.Color := clYellow
        Else StringGrid1.Canvas.Brush.Color := clWhite;
        StringGrid1.Canvas.Font.Color := clBlack;
        sText := StringGrid1.Cells[ACol, ARow];
        ExtTextOut(StringGrid1.Canvas.Handle, Rect.Left, Rect.Top, ETO_CLIPPED or ETO_OPAQUE, @Rect, PChar(sText), Length(sText), nil);
        End
     Else Begin
        StringGrid1.Canvas.Brush.Color := clBtnFace;
        If ACol = iSelectedColumn Then Begin
           DrawDownBtn;
           DrawDownBtn;
           StringGrid1.Canvas.TextRect(Rect, Rect.Left, Rect.Top-3, StringGrid1.Cells[ACol, ARow]);
           End;
        End;
end;

procedure TIDB_MonitorGridForm.FormCreate(Sender: TObject);
begin
     iSelectedColumn := 0;
     ALangManager.MakeReplacement(SELF);
end;

procedure TIDB_MonitorGridForm.StringGrid1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
Var
   ACol, ARow: Integer;
begin
     TStringGrid(Sender).MouseToCell(X, Y, ACol, ARow);
     If ARow = 0 Then Begin
        iSelectedColumn := ACol;
        StringGrid1.Invalidate;
        End;
end;

Procedure TIDB_MonitorGridForm.Sort(SortType: TSortType; iColumnsIndex: Integer; iLo, iHi: Integer);
Var
   iI, Lo, Hi: Integer;
   arTemp: Array Of AnsiString;
   Mid: AnsiString;
Begin
     SetLength(arTemp, StringGrid1.ColCount);
     Lo := iLo;
     Hi := iHi;
     Mid := StringGrid1.Cells[iColumnsIndex, (Lo + Hi) div 2];
     Repeat
           If SortType = stAscending Then Begin
              While StringGrid1.Cells[iColumnsIndex, Lo] < Mid Do Inc(Lo);
              While StringGrid1.Cells[iColumnsIndex, Hi] > Mid Do Dec(Hi);
              End
           Else Begin
              While StringGrid1.Cells[iColumnsIndex, Lo] > Mid Do Inc(Lo);
              While StringGrid1.Cells[iColumnsIndex, Hi] < Mid Do Dec(Hi);
              End;
           If Lo <= Hi Then Begin
              For iI:=0 To StringGrid1.ColCount-1 Do
                  arTemp[iI] := StringGrid1.Cells[iI, Lo];
              For iI:=0 To StringGrid1.ColCount-1 Do
                  StringGrid1.Cells[iI, Lo] := StringGrid1.Cells[iI, Hi];
              For iI:=0 To StringGrid1.ColCount-1 Do
                  StringGrid1.Cells[iI, Hi] := arTemp[iI];
              Inc(Lo);
              Dec(Hi);
              End;
     Until Lo > Hi;
     If Hi > iLo Then Sort(SortType, iColumnsIndex, iLo, Hi);
     If Lo < iHi Then Sort(SortType, iColumnsIndex, Lo, iHi);
End;

procedure TIDB_MonitorGridForm.SBSortAscClick(Sender: TObject);
begin
     SELF.Sort(stAscending, SELF.iSelectedColumn, 1, StringGrid1.RowCount-1);
end;

procedure TIDB_MonitorGridForm.SBSortDescClick(Sender: TObject);
begin
     SELF.Sort(stDescending, SELF.iSelectedColumn, 1, StringGrid1.RowCount-1);
end;

procedure TIDB_MonitorGridForm.FormActivate(Sender: TObject);
begin
     If StringGrid1.RowCount < 2 Then Begin
        SBSave.Enabled := FALSE;
        SBSortAsc.Enabled := FALSE;
        SBSortDesc.Enabled := FALSE;
        End;
end;

procedure TIDB_MonitorGridForm.FormResize(Sender: TObject);
begin
     // Change the progress bar width. 
     Gauge1.Width := Button1.Left - 10;
end;

end.
