unit IDB_ParamsOfSaveLoad_TableSelect;
{
Internal database. Ver. II.

This form purpose is that when an user determines a source of loading data as
MS Access table, I need to know which table in *.mdb file is required to use.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 02-02-2001
}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TIDB_TableSelect = class(TForm)
    Panel2: TPanel;
    OKButton: TButton;
    CancelButton: TButton;
    Notebook1: TNotebook;
    ListBox1: TListBox;
    Panel3: TPanel;
    UseExistingRadioButton: TRadioButton;
    CreateNewRadioButton: TRadioButton;
    Edit1: TEdit;
    ListBox2: TListBox;
    procedure ListBox1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

Uses
     IDB_LanguageManager;

{$R *.DFM}

procedure TIDB_TableSelect.ListBox1DblClick(Sender: TObject);
begin
     OKButton.Click;
end;

procedure TIDB_TableSelect.FormCreate(Sender: TObject);
begin
     ALangManager.MakeReplacement(SELF);
end;

end.
