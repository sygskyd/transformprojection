unit IDB_FormPositionMan;
{
Internal database. Ver. II.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 18.10.2001
Modified:

This is an object to manage on positon and size of form.
}
interface

uses Forms, Controls,
   IDB_Access2DB;

type
   TIDB_FormPositionManager = class
   private
      IDA: TIDB_Access2DB_Level2;
      iLayerIndex: Integer;
      iFormLeft,
         iFormTop,
         iFormWidth,
         iFormHeight: Integer;
      bAutoZoom: Boolean;
      bFormStateWasSuccessfullyLoaded: Boolean;
      bButtonDown: boolean;
// ++ Cadmensky Version 2.3.7
      iSplitterPos: Integer;
      iLeftColWidth: Integer;
      iRightColWidth: Integer;
      bTreeWillBeVisible: Boolean;
      bGridStateWasSuccessfullyLoaded: Boolean;
// -- Cadmensky Version 2.3.7

      AnOldScreenCursor: TCursor;

      procedure CheckAndCorrectDescTableStructure;
      function LoadFormState: Boolean;
// ++ Cadmensky Version 2.3.7
      function LoadGridState: Boolean;
// -- Cadmensky Version 2.3.7

      procedure SetScreenCursor;
      procedure RestoreScreenCursor;
   public
      constructor Create (IDB_DataAccessModule: TIDB_Access2DB_Level2; iLayerIndex: Integer);

      procedure StoreFormState (AForm: TForm; bButtonDown: boolean; AutoZoomOrSelect: Boolean); virtual;
      procedure GetFormState (var iLeft: Integer; var iTop: Integer; var iWidth: Integer; var iHeight: Integer; var bButtonDown: boolean; var bZoomOrSelect: boolean);
      procedure GetGridState (var iSplitterPos: Integer; var iLeftColWidth: Integer; var iRightColWidth: Integer; var bTreeWillBeVisible: Boolean);
      procedure SaveData;
   end;

   TIDB_InfoFormPositionManager = class
   private
      iFormLeft,
         iFormTop,
         iFormWidth,
         iFormHeight,
         iSplitterPos: Integer;
      bFormStateWasSuccessfullyLoaded: Boolean;

      AnOldScreenCursor: TCursor;
      procedure SetScreenCursor;
      procedure RestoreScreenCursor;

      procedure SaveData;
   public
      constructor Create;
      destructor Free;

      procedure StoreFormState (AForm: TForm);
      procedure GetFormState (var iLeft: Integer; var iTop: Integer; var iWidth: Integer; var iHeight: Integer; var iSplitterPos: Integer);
      function LoadData: Boolean;
   end;

implementation

uses SysUtils, DB, IniFiles,
   DTables, DDB,
   IDB_Consts, IDB_Monitor, IDB_Monitor1, IDB_CallbacksDef;

const
   cs_FieldName = 'DBGridProps';
   cs_FieldNameX = 'DBGridPosX';
   cs_FieldNameY = 'DBGridPosY';
   cs_FieldNameW = 'DBGridWidth';
   cs_FieldNameH = 'DBGridHeight';
   cs_FieldButtonDown = 'DBButtonDown';

   ////////////////// F O R M   P O S I T I O N   M A N A G E R ///////////////////

constructor TIDB_FormPositionManager.Create (IDB_DataAccessModule: TIDB_Access2DB_Level2; iLayerIndex: Integer);
begin
   SELF.IDA := IDB_DataAccessModule;
   SELF.iLayerIndex := iLayerIndex;
   SELF.bFormStateWasSuccessfullyLoaded := FALSE;
// ++ Cadmensky Version 2.3.7
   SELF.bGridStateWasSuccessfullyLoaded := FALSE;
   SELF.iSplitterPos := -1;
   SELF.iLeftColWidth := -1;
   SELF.iRightColWidth := -1;
// -- Cadmensky Version 2.3.7
end;

procedure TIDB_FormPositionManager.SetScreenCursor;
begin
   AnOldScreenCursor := Screen.Cursor;
   Screen.Cursor := crHourGlass;
   Application.ProcessMessages;
end;

procedure TIDB_FormPositionManager.RestoreScreenCursor;
begin
   Screen.Cursor := AnOldScreenCursor;
   Application.ProcessMessages;
end;

procedure TIDB_FormPositionManager.GetFormState (var iLeft: Integer; var iTop: Integer; var iWidth: Integer; var iHeight: Integer; var bButtonDown: boolean; var bZoomOrSelect: boolean);
begin
   // Try to get data from DB, if it is necessary.
   if not SELF.bFormStateWasSuccessfullyLoaded then
      SELF.bFormStateWasSuccessfullyLoaded := SELF.LoadFormState;
   // If I have loaded data, apply it to the form.
   if SELF.bFormStateWasSuccessfullyLoaded then
   begin
      iLeft := SELF.iFormLeft;
      iTop := SELF.iFormTop;
      iWidth := SELF.iFormWidth;
      iHeight := SELF.iFormHeight;
      bButtonDown := SELF.bButtonDown;
      bZoomOrSelect:= SELF.bAutoZoom;
   end;
end;

procedure TIDB_FormPositionManager.CheckAndCorrectDescTableStructure;
var
   bFieldIsPresent: Boolean;
begin
// ++ Cadmensky Version 2.3.2
   if IDA.ADBItemsMan.GetReadyToCheckTableStructure (cs_CommonDescTableName) <> ci_All_Ok then
   begin
      IDA.ADBItemsMan.EndOfCheckingTable;
      Exit;
   end;

   if not IDA.ADbItemsMan.FieldExists ('_USER_ID') then
      IDA.ADbItemsMan.AddField (cs_CommonDescTableName, '_USER_ID', 1, 38);

   if not IDA.ADbItemsMan.FieldExists ('_PROJECT_ID') then
      IDA.ADbItemsMan.AddField (cs_CommonDescTableName, '_PROJECT_ID', 1, 38);

   if not IDA.ADbItemsMan.FieldExists (cs_CDT_LayerIndexFieldName) then
      IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_CDT_LayerIndexFieldName, 3, -1);

   if not IDA.ADbItemsMan.FieldExists (cs_FieldNameX) then
      IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_FieldNameX, 3, -1); // 3rd type is integer.

   if not IDA.ADbItemsMan.FieldExists (cs_FieldNameY) then
      IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_FieldNameY, 3, -1); // 3rd type is integer.

   if not IDA.ADbItemsMan.FieldExists (cs_FieldNameW) then
      IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_FieldNameW, 3, -1); // 3rd type is integer.

   if not IDA.ADbItemsMan.FieldExists (cs_FieldNameH) then
      IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_FieldNameH, 3, -1); // 3rd type is integer.

   if not IDA.ADbItemsMan.FieldExists (cs_FieldButtonDown) then
      IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_FieldButtonDown, 5, -1); // 3rd type is integer.

   if not IDA.ADbItemsMan.FieldExists (cs_InternalID_FieldName) then
      IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_InternalID_FieldName, 14, -1); // 14 = AutoIncrement type

   if not IDA.ADbItemsMan.FieldExists ('isTemplate') then
      IDA.ADbItemsMan.AddField (cs_CommonDescTableName, 'isTemplate', 1, 255);

   IDA.ADBItemsMan.EndOfCheckingTable;
// -- Cadmensky Version 2.3.2

// ++ Commented by Cadmensky Version 2.3.2
{   if IDA.ADbItemsMan.FieldExists (cs_CommonDescTableName, '_USER_ID', bFieldIsPresent) <> ci_All_Ok then EXIT;
   if not bFieldIsPresent then IDA.ADbItemsMan.AddField (cs_CommonDescTableName, '_USER_ID', 1, 38);

   if IDA.ADbItemsMan.FieldExists (cs_CommonDescTableName, '_PROJECT_ID', bFieldIsPresent) <> ci_All_Ok then EXIT;
   if not bFieldIsPresent then IDA.ADbItemsMan.AddField (cs_CommonDescTableName, '_PROJECT_ID', 1, 38);

   if IDA.ADbItemsMan.FieldExists (cs_CommonDescTableName, cs_CDT_LayerIndexFieldName, bFieldIsPresent) <> ci_All_Ok then EXIT;
   if not bFieldIsPresent then IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_CDT_LayerIndexFieldName, 3, -1);

   if IDA.ADbItemsMan.FieldExists (cs_CommonDescTableName, cs_FieldNameX, bFieldIsPresent) <> ci_All_OK then EXIT;
   if not bFieldIsPresent then IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_FieldNameX, 3, -1); // 3rd type is integer.

   if IDA.ADbItemsMan.FieldExists (cs_CommonDescTableName, cs_FieldNameY, bFieldIsPresent) <> ci_All_OK then EXIT;
   if not bFieldIsPresent then IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_FieldNameY, 3, -1); // 3rd type is integer.

   if IDA.ADbItemsMan.FieldExists (cs_CommonDescTableName, cs_FieldNameW, bFieldIsPresent) <> ci_All_OK then EXIT;
   if not bFieldIsPresent then IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_FieldNameW, 3, -1); // 3rd type is integer.

   if IDA.ADbItemsMan.FieldExists (cs_CommonDescTableName, cs_FieldNameH, bFieldIsPresent) <> ci_All_OK then EXIT;
   if not bFieldIsPresent then IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_FieldNameH, 3, -1); // 3rd type is integer.

   if IDA.ADbItemsMan.FieldExists (cs_CommonDescTableName, cs_FieldButtonDown, bFieldIsPresent) <> ci_All_OK then EXIT;
   if not bFieldIsPresent then IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_FieldButtonDown, 5, -1); // 3rd type is integer.

   if IDA.ADbItemsMan.FieldExists (cs_CommonDescTableName, cs_InternalID_FieldName, bFieldIsPresent) <> ci_All_Ok then EXIT;
   if not bFieldIsPresent then IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_InternalID_FieldName, 14, -1); // 14 = AutoIncrement type}
// -- Commented by Cadmensky Version 2.3.2

end;

{ This procedure loads data about form's position and form's sizes from DB.}

function TIDB_FormPositionManager.LoadFormState: Boolean;
var
   AQuery: TDQuery;
   sLayerIndex,
      sTemp: AnsiString;
      var ini : TINIFile;
begin
   RESULT := FALSE;

   SetScreenCursor;
   try
      sLayerIndex := IntToStr (SELF.iLayerIndex);
      IDA.ADbItemsMan.CreateCommonDescTable;
      SELF.CheckAndCorrectDescTableStructure;

      AQuery := TDQuery.Create (nil);
      AQuery.Master := IDA.DMaster;
      AQuery.SQL.Text := 'SELECT * FROM [' + cs_CommonDescTableName + '] ' +
         'WHERE [' + cs_CDT_LayerIndexFieldName + '] = ' + sLayerIndex;
      try
         AQuery.Open;
         if AQuery.RecordCount < 1 then raise Exception.Create ('The data are missing.');
      except
         AQuery.Close;
         AQuery.Free;
         RestoreScreenCursor;
         EXIT;
      end;
      try
         sTemp := IntToStr (AQuery.FieldByName (cs_FieldNameX).AsInteger);
         SELF.iFormLeft := AQuery.FieldByName (cs_FieldNameX).AsInteger;

         sTemp := IntToStr (AQuery.FieldByName (cs_FieldNameY).AsInteger);
         SELF.iFormTop := AQuery.FieldByName (cs_FieldNameY).AsInteger;

         sTemp := IntToStr (AQuery.FieldByName (cs_FieldNameW).AsInteger);
         SELF.iFormWidth := AQuery.FieldByName (cs_FieldNameW).AsInteger;

         sTemp := IntToStr (AQuery.FieldByName (cs_FieldNameH).AsInteger);
         SELF.iFormHeight := AQuery.FieldByName (cs_FieldNameH).AsInteger;

         SELF.bButtonDown := AQuery.FieldByName (cs_FieldButtonDown).AsBoolean;

         if (iFormHeight > 0) and (iFormWidth > 0) then
            RESULT := TRUE;

         ini:=TIniFile.Create(GetWinGISINIFileNameFunc);
         SELF.bAutoZoom:=ini.ReadBool ('InternalDatabase', 'IDB_ZoomOrSelect', False);
         ini.free;
      except
      end;
      AQuery.Close;
      AQuery.Free;
   finally
      RestoreScreenCursor;
   end;
end;

procedure TIDB_FormPositionManager.GetGridState (var iSplitterPos: Integer; var iLeftColWidth: Integer; var iRightColWidth: Integer; var bTreeWillBeVisible: Boolean);
begin
   if not bGridStateWasSuccessfullyLoaded then
      bGridStateWasSuccessfullyLoaded := LoadGridState;
   if bGridStateWasSuccessfullyLoaded then
   begin
      iSplitterPos := SELF.iSplitterPos;
      iLeftColWidth := SELF.iLeftColWidth;
      iRightColWidth := SELF.iRightColWidth;
// ++ Cadmensky Version 2.3.7
      bTreeWillBeVisible := SELF.bTreeWillBeVisible;
// -- Cadmensky Version 2.3.7
   end;
end;

function TIDB_FormPositionManager.LoadGridState: Boolean;
var
   AQuery: TDQuery;
   AStream: TDBLOBStream;
   sVersionSign: AnsiString;
   iVersionOfStorage: Integer;
// ++ Cadmensky Version 2.3.7
   iTreeWillBeVisible: Integer;
// -- Cadmensky Version 2.3.7
begin
   RESULT := FALSE;

   SetScreenCursor;

   IDA.ADbItemsMan.CreateCommonDescTable;
   SELF.CheckAndCorrectDescTableStructure;

   AQuery := TDQuery.Create (nil);
   AQuery.Master := IDA.DMaster;
   AQuery.SQL.Text := 'SELECT * FROM [' + cs_CommonDescTableName + '] ' +
      'WHERE [' + cs_CDT_LayerIndexFieldName + '] = -1';
   try
      AQuery.Open;
      if AQuery.RecordCount < 1 then raise Exception.Create ('The data are missing.');
      AStream := TDBlobStream (AQuery.CreateBlobStream (AQuery.FieldByName (cs_FieldName), bmRead));
      if (AStream = nil) or (AStream.Size < 1) then raise Exception.Create ('A stream to loading data cannot be created.');
   except
      RestoreScreenCursor;
      AQuery.Close;
      AQuery.Free;
      EXIT;
   end;
   try
      SetLength (sVersionSign, Length (cs_VerisonOfDBFormStorage));
      try
         AStream.Position := 0;
         AStream.Read (Pointer (sVersionSign)^, Length (sVersionSign));
         iVersionOfStorage := StrToInt (Copy (sVersionSign, 5, 4)); // '#WG#____#WG#';
         //      ^^^^ - Version number
      except
         iVersionOfStorage := 0;
         AStream.Position := 0;
      end;
      AStream.Read (SELF.iSplitterPos, SizeOf (SELF.iSplitterPos));
      AStream.Read (SELF.iLeftColWidth, SizeOf (SELF.iLeftColWidth));
      AStream.Read (SELF.iRightColWidth, SizeOf (SELF.iRightColWidth));
// ++ Cadmensky Version 2.3.7
      try
         AStream.Read (iTreeWillBeVisible, iTreeWillBeVisible);
         SELF.bTreeWillBeVisible := iTreeWillBeVisible = 1;
      except
         SELF.bTreeWillBeVisible := false;
      end;
// -- Cadmensky Version 2.3.7
      RESULT := TRUE;
   finally
      RestoreScreenCursor;
      AStream.Free;
      AQuery.Close;
      AQuery.Free;
   end;
end;

procedure TIDB_FormPositionManager.StoreFormState (AForm: TForm; bButtonDown: boolean; AutoZoomOrSelect: Boolean );
begin
   if AForm is TIDB_MonitorForm then
   begin
      SELF.iSplitterPos := TIDB_MonitorForm (AForm).IDB_Splitter1.Left;
      SELF.iLeftColWidth := TIDB_MonitorForm (AForm).StringGrid1.ColWidths[0];
      SELF.iRightColWidth := TIDB_MonitorForm (AForm).StringGrid1.ColWidths[1];
   //     SELF.bButtonDown := TIDB_MonitorForm(AForm).LeftPanel.Visible;
      SELF.bTreeWillBeVisible := TIDB_MonitorForm (AForm).LeftPanel.Visible;
      SELF.bGridStateWasSuccessfullyLoaded := TRUE;
   end;
// ++ Cadmensky Version 2.3.7
//   inherited StoreFormState (AForm, bButtonDown);
// ++ Cadmensky Version 2.3.8
   if AForm.FormStyle <> fsMDIChild then
//   if AForm.WindowState <> wsMaximized then
// -- Cadmensky Version 2.3.8
   begin
      SELF.iFormLeft := AForm.Left;
      SELF.iFormTop := AForm.Top;
      SELF.iFormWidth := AForm.Width;
      SELF.iFormHeight := AForm.Height;
// ++ Cadmensky Version 2.3.7
      if not (AForm is TIDB_MonitorForm) then
         begin
         SELF.bButtonDown := bButtonDown;
         self.bAutoZoom:= AutoZoomOrSelect;
         end;
      SELF.bFormStateWasSuccessfullyLoaded := TRUE;
      Self.SaveData;
   end;
// -- Cadmensky Version 2.3.7
end;

procedure TIDB_FormPositionManager.SaveData;
var
   AQuery: TDQuery;
   AStream: TDBLOBStream;
   sVersionSign: AnsiString;
   sLayerIndex: AnsiString;
   iTreeWillBeVisible: Integer;
   ini: TIniFile;
begin

// ++ Cadmensky Version 2.3.7
   if not bFormStateWasSuccessfullyLoaded then
      Exit;
//   inherited SaveData;
// -- Cadmensky Version 2.3.7

   SetScreenCursor;

   IDA.ADBItemsMan.CreateCommonDescTable;
   SELF.CheckAndCorrectDescTableStructure;

   sLayerIndex := IntToStr (SELF.iLayerIndex);
   AQuery := TDQuery.Create (nil);
   AQuery.Master := IDA.DMaster;

   AQuery.SQL.Text := 'SELECT * ' +
      'FROM [' + cs_CommonDescTableName + '] ' +
      'WHERE [' + cs_CDT_LayerIndexFieldName + '] = ' + sLayerIndex;
   try
      AQuery.Open;
      if AQuery.RecordCount < 1 then
      begin
         AQuery.Close;
         AQuery.ExecSQL ('INSERT INTO [' + cs_CommonDescTableName + '] ([' + cs_CDT_LayerIndexFieldName + ']) ' +
            'VALUES(' + sLayerIndex + ')');
         AQuery.SQL.Text := 'SELECT * ' +
            'FROM [' + cs_CommonDescTableName + '] ' +
            'WHERE [' + cs_CDT_LayerIndexFieldName + '] = ' + sLayerIndex;
         AQuery.Open;
      end;
   except
      AQuery.Free;
      RestoreScreenCursor;
      Exit;
   end;

   try
      AQuery.Edit;
// ++ Cadmensky Version 2.3.7
      AQuery.FieldByName (cs_FieldNameX).AsInteger := SELF.iFormLeft;
      AQuery.FieldByName (cs_FieldNameY).AsInteger := SELF.iFormTop;
      AQuery.FieldByName (cs_FieldNameW).AsInteger := SELF.iFormWidth;
      AQuery.FieldByName (cs_FieldNameH).AsInteger := SELF.iFormHeight;
      AQuery.FieldByName (cs_FieldButtonDown).AsBoolean := SELF.bButtonDown;
      AQuery.Post;
      ini:=TIniFile.Create(GetWinGISINIFileNameFunc);
      ini.WriteBool ('InternalDatabase', 'IDB_ZoomOrSelect', SELF.bAutoZoom);
      ini.free;
   except
      AQuery.Cancel;
      AQuery.Close;
      AQuery.Free;

      RestoreScreenCursor;
      Exit;
   end;
// -- Cadmensky Version 2.3.7

   if not bGridStateWasSuccessfullyLoaded then
   begin
// ++ Cadmensky Version 2.3.8
      AQuery.Close;
      AQuery.Free;
// -- Cadmensky Version 2.3.8
      RestoreScreenCursor;
      Exit;
   end;

   try
      AQuery.Edit;
      AStream := TDBlobStream (AQuery.CreateBlobStream (AQuery.FieldByName (cs_FieldName), bmWrite));
      if (AStream = nil) then
         raise Exception.Create ('A stream to saving data cannot be created.');

      AStream.Truncate;

      sVersionSign := cs_VerisonOfMonitoringFormStorage;
      AStream.Write (Pointer (sVersionSign)^, Length (sVersionSign));

      AStream.Write (SELF.iSplitterPos, SizeOf (SELF.iSplitterPos));
      AStream.Write (SELF.iLeftColWidth, SizeOf (SELF.iLeftColWidth));
      AStream.Write (SELF.iRightColWidth, SizeOf (SELF.iRightColWidth));
// ++ Cadmensky Version 2.3.7
      if SELF.bTreeWillBeVisible then
         iTreeWillBeVisible := 1
      else
         iTreeWillBeVisible := 0;
      AStream.Write (iTreeWillBeVisible, SizeOf (iTreeWillBeVisible));
// -- Cadmensky Version 2.3.7

      AStream.Flush;
      AStream.Free;
   finally
      AQuery.Post;
      AQuery.Close;
      AQuery.Free;
   end;
   RestoreScreenCursor;
end;

//// I N F O   W I N D O W    F O R M   P O S I T I O N   M A N A G E R ////////

constructor TIDB_InfoFormPositionManager.Create;
begin
   SELF.bFormStateWasSuccessfullyLoaded := FALSE;
end;

destructor TIDB_InfoFormPositionManager.Free;
var
   AForm: TForm;
   iI: Integer;
   bFound: Boolean;
begin
   bFound := FALSE;
   for iI := 0 to Screen.FormCount - 1 do
   begin
      AForm := Screen.Forms[iI];
      if (AForm is TIDB_Monitor1Form) then
      begin
         bFound := TRUE;
         BREAK;
      end;
   end; // For iI end
   if bFound then
   begin
      SELF.StoreFormState (AForm);
   end;
   SELF.SaveData;
end;

procedure TIDB_InfoFormPositionManager.SetScreenCursor;
begin
   AnOldScreenCursor := Screen.Cursor;
   Screen.Cursor := crHourGlass;
   Application.ProcessMessages;
end;

procedure TIDB_InfoFormPositionManager.RestoreScreenCursor;
begin
   Screen.Cursor := AnOldScreenCursor;
   Application.ProcessMessages;
end;

procedure TIDB_InfoFormPositionManager.StoreFormState (AForm: TForm);
begin
   SELF.iFormLeft := AForm.Left;
   SELF.iFormTop := AForm.Top;
   SELF.iFormWidth := AForm.Width;
   SELF.iFormHeight := AForm.Height;
   SELF.iSplitterPos := TIDB_Monitor1Form (AForm).DataPanel.Width;
   SELF.bFormStateWasSuccessfullyLoaded := TRUE;
end;

procedure TIDB_InfoFormPositionManager.GetFormState (var iLeft: Integer; var iTop: Integer; var iWidth: Integer; var iHeight: Integer; var iSplitterPos: Integer);
begin
   if not bFormStateWasSuccessfullyLoaded then
      bFormStateWasSuccessfullyLoaded := LoadData;
   if bFormStateWasSuccessfullyLoaded then
   begin
      iLeft := SELF.iFormLeft;
      iTop := SeLF.iFormTop;
      iWidth := SELF.iFormWidth;
      iHeight := SELF.iFormHeight;
      iSplitterPos := SELF.iSplitterPos;
   end;
end;

procedure TIDB_InfoFormPositionManager.SaveData;
var
   sIniFileName: AnsiString;
   AnIniFile: TIniFile;
begin
   if not bFormStateWasSuccessfullyLoaded then EXIT;

   SetScreenCursor;
   sIniFileName := GetWinGISINIFileNameFunc;
   AnIniFile := TIniFile.Create (sIniFileName);
   try
      AnIniFile.WriteInteger ('InternalDatabase', 'IW_L', SELF.iFormLeft);
      AnIniFile.WriteInteger ('InternalDatabase', 'IW_T', SELF.iFormTop);
      AnIniFile.WriteInteger ('InternalDatabase', 'IW_W', SELF.iFormWidth);
      AnIniFile.WriteInteger ('InternalDatabase', 'IW_H', SELF.iFormHeight);
      AnIniFile.WriteInteger ('InternalDatabase', 'IW_SP', SELF.iSplitterPos);
   finally
      RestoreScreenCursor;
      AnIniFile.Free;
   end;
end;

function TIDB_InfoFormPositionManager.LoadData: Boolean;
var
   sIniFileName: AnsiString;
   AnIniFile: TIniFile;
begin
   RESULT := FALSE;
   SetScreenCursor;

   sIniFileName := GetWinGISINIFileNameFunc;
   AnIniFile := TIniFile.Create (sIniFileName);
   try
      SELF.iFormLeft := AnIniFile.ReadInteger ('InternalDatabase', 'IW_L', -1);
      SELF.iFormTop := AnIniFile.ReadInteger ('InternalDatabase', 'IW_T', -1);
      SELF.iFormWidth := AnIniFile.ReadInteger ('InternalDatabase', 'IW_W', -1);
      if SELF.iFormWidth < 1 then SELF.iFormWidth := -1;
      SELF.iFormHeight := AnIniFile.ReadInteger ('InternalDatabase', 'IW_H', -1);
      if SELF.iFormHeight < 1 then SELF.iFormHeight := -1;
      SELF.iSplitterPos := AnIniFile.ReadInteger ('InternalDatabase', 'IW_SP', -1);
      if SELF.iSplitterPos < 1 then SELF.iSplitterPos := -1;
      RESULT := TRUE;
   finally
      RestoreScreenCursor;
      AnIniFile.Free;
   end;
end;

end.

