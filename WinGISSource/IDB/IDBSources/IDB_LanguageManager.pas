unit IDB_LanguageManager;
{
Internal database. Ver. II.
This module provides the functionality of the langauge localization.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 01-02-2001
}

interface

uses Classes;

type
   TLanguageManager = class
   private
      listEntries: TStringList;
   public
      constructor Create (sLanguageFileName: AnsiString);
      destructor Free;

      function GetValue (iValue: Integer): AnsiString; overload;
      function GetValue (sValue: AnsiString): AnsiString; overload;

      procedure MakeReplacement (AComponent: TObject);
   end;
var
   ALangManager: TLanguageManager;

implementation

uses Forms, SysUtils, TypInfo, Windows,

   Graphics, IDB_Consts;

constructor TLanguageManager.Create (sLanguageFileName: AnsiString);
var
   fsFileStream: TFileStream;
   HRsrc, HMem: THandle;
   Size: DWORD;
   Point: Pointer;
   AFile: TextFile;
   AString,
      AnUpperedString: AnsiString;
begin
   listEntries := TStringList.Create;
      // Try to find such file.
   if not FileExists (sLanguageFileName) then
   begin
        // If the english file also does not exist, create it.
      HRsrc := FindResource (HInstance, PChar (cs_ResNameOfEnglishLngFile), RT_RCDATA);
      Size := SizeofResource (HInstance, HRsrc);
      HMem := LoadResource (HInstance, HRsrc);
      Point := LockResource (HMem);
      try
         fsFileStream := TFileStream.Create (sLanguageFileName, fmCreate);
         fsFileStream.Position := 0;
         fsFileStream.Write (Point^, Size);
         fsFileStream.Free;
      except
         EXIT;
      end;
   end;
   try
        // Load language file.
      AssignFile (AFile, sLanguageFileName);
      try
         Reset (AFile);
         while not EOF (AFile) do
         begin
            ReadLn (AFile, AString);
            if Trim (AString) = '' then
            begin
               AString := ''; // Without this line the compilator does not compile next line.
               CONTINUE;
            end;
            AnUpperedString := AnsiUpperCase (AString);
            if (AnUpperedString = cs_MenuListStartSign)
               or
               (AnUpperedString = cs_PopupMenuListStartSign)
               or
               (AnUpperedString = cs_ToolbarStartSign) then
            begin
               AString := ''; // Without this line the compilator does not compile next line.
               BREAK;
            end;
            if Pos ('=', AString) > 0 then
               listEntries.Add (AString);
         end; // While end
      finally
         CloseFile (AFile);
      end;
   except
      listEntries.Clear;
   end;
end;

destructor TLanguageManager.Free;
begin
   listEntries.Clear;
   listEntries.Free;
end;

{ This function returns a string by a ID value. It looks like Multilanguage of WinGIS. }

function TLanguageManager.GetValue (iValue: Integer): AnsiString;
begin
   try
      RESULT := SELF.GetValue (IntToStr (iValue));
   except
      RESULT := '';
   end;
end;

function TLanguageManager.GetValue (sValue: AnsiString): AnsiString;
var
   iP: Integer;
begin
   RESULT := 'Unknown ' + sValue;
   iP := listEntries.IndexOfName (sValue);
   if (iP < 0) or (iP >= listEntries.Count) then EXIT;
   RESULT := listEntries[iP];
   iP := Pos ('=', RESULT);
   if iP > 0 then
      RESULT := Copy (RESULT, iP + 1, Length (RESULT));
end;

procedure TLanguageManager.MakeReplacement (AComponent: TObject);

   procedure SetStringProps (AComponent: TObject);
   var
      bReplaced: Boolean;

      function Replace (Text: AnsiString): AnsiString;
      var
         iP: Integer;
         sValue: AnsiString;
      begin
         RESULT := '';
         bReplaced := FALSE;
         if Text = '' then
            EXIT
         else
         begin
            Text := Trim (Text);
            iP := Pos ('!', Text);
            if iP < 1 then EXIT;
            sValue := Copy (Text, iP + 1, Length (Text));
            RESULT := SELF.GetValue (sValue);
            bReplaced := TRUE;
         end;
      end;

   var
      APropInfo: PPropInfo;
      APropList: PPropList;
      iPropCount, iI, iJ: Integer;
      AObject: TObject;
      sCaption: AnsiString;
   begin
      if AComponent = nil then EXIT;
      New (APropList);
      try
         iPropCount := GetPropList (AComponent.ClassInfo, [tkClass, tkString, tkLString], APropList);
         for iI := 0 to iPropCount - 1 do
         begin
            APropInfo := APropList[iI];
            if APropInfo.PropType^.Kind in [tkString, tkLString] then
            begin
               sCaption := Replace (GetStrProp (AComponent, APropInfo));
               if bReplaced then
                  SetStrProp (AComponent, APropInfo, sCaption);
            end
            else
               if APropInfo.PropType^.Kind = tkClass then
               begin
                  AObject := Pointer (GetOrdProp (AComponent, APropInfo));
                  if AObject is TStrings then
                  begin
                     TStrings (AObject).BeginUpdate;
                     for iJ := 0 to TStrings (AObject).Count - 1 do
                        if Copy (Trim (TStrings (AObject).Strings[iJ]), 1, 1) = '!' then
                           TStrings (AObject).Strings[iJ] := Replace (TStrings (AObject).Strings[iJ]);
                     TStrings (AObject).EndUpdate;
                  end
                  else
                     if AObject is TCollection then
                        for iJ := 0 to TCollection (AObject).Count - 1 do
                           SetStringProps (TCollection (AObject).Items[iJ])
                     else
                        if AObject is TComponent then
                           for iJ := 0 to TComponent (AObject).ComponentCount - 1 do
                              SetStringProps (TComponent (AObject).Components[iJ])

                        else
                           if AObject is TFont then
                           begin
                              TFont (AObject).Name := 'MS Sans Serif';
                              TFont (AObject).Size := 8;
                           end
                           else
                              SetStringProps (AObject);
               end;
         end; // For iI end
      finally
         Dispose (APropList);
      end;
   end;

var
   iI: Integer;
begin
   SetStringProps (AComponent);
   if AComponent is TComponent then
      for iI := 0 to TComponent (AComponent).ComponentCount - 1 do
         SetStringProps (TComponent (AComponent).Components[iI]);
end;

end.

