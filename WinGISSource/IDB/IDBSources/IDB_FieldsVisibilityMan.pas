unit IDB_FieldsVisibilityMan;
{
Internal database. Ver. II.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 18.10.2001
Modified:

This module provides a management on field's states (visible states and
widths) and a form that allows to the user to select which fields are visible and
which are not.
}
interface

uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   ExtCtrls, StdCtrls, Buttons,
   IDB_Access2DB, IDB_DBGrids;

type

   TrecFieldsParameters = record
      sFieldName: AnsiString;
      iFieldsDisplayWidth: Integer;
      bFieldIsVisible: Boolean;
      iFieldVisibleIndex: Integer;
      iFieldCommas: Integer;
      sFieldFormat: AnsiString;
   end;

   TIDB_FieldsVisibilityManager = class
   private
      listFields: TList;
      IDA: TIDB_Access2DB_Level2;
      iLayerIndex: Integer;
      bFieldsDataWereSuccessfullyLoaded: Boolean;

      AnOldScreenCursor: TCursor;

      procedure ClearAllData;
      function FindField (sFieldName: AnsiString): Integer;
      procedure CorrectExistedOrAddNewField (sFieldName: AnsiString; iFieldWidth: Integer; bFieldIsVisible: Boolean; iFieldVisibleIndex: Integer; sFieldFormat: AnsiString);

      procedure CheckAndCorrectDescTableStructure;
      procedure SaveFieldsState;
      function LoadFieldsState: Boolean;

      procedure SetScreenCursor;
      procedure RestoreScreenCursor;
   public
      constructor Create (IDB_DataAccessModule: TIDB_Access2DB_Level2; iLayerIndex: Integer);
      destructor Free;

      procedure DetectFieldsStates (ADBGrid: TIDBPCDBGrid);
      procedure RestoreFieldsStates (AGrid: TIDBPCDBGrid; bRestoreFieldsVisibleState: Boolean = TRUE; bRestoreFieldVisibleIndex: Boolean = TRUE);

      function GetFieldVisibleState (sFieldName: AnsiString): Boolean;
      function GetFieldFormat (sFieldName: AnsiString): AnsiString;

      procedure SetFieldVisibleState (sFieldName: AnsiString; bState: Boolean);

      function GetFieldDisplayWidth (sFieldName: AnsiString): Integer;

      function GetFieldVisibleIndex (sFieldName: AnsiString): Integer;
      procedure SetFieldVisibleIndex (sFieldName: AnsiString; iValue: Integer);

      function GetAllAttributeFieldNames (bUsingVisibleOrder: Boolean = FALSE): AnsiString;
      function GetVisibleAttributeFieldNames (bUsingVisibleOrder: Boolean = FALSE): AnsiString;

      procedure LoadData;
      procedure SaveData;
   end;

implementation

uses DB,
   DTables, DDB,
   IDB_LanguageManager, IDB_Utils, IDB_Consts;

const
   cs_FieldName = 'DBGridProps';

//////////////////////// F I E L D S     M A N A G E R /////////////////////////

constructor TIDB_FieldsVisibilityManager.Create (IDB_DataAccessModule: TIDB_Access2DB_Level2; iLayerIndex: Integer);
begin
   SELF.listFields := TList.Create;
   SELF.IDA := IDB_DataAccessModule;
   SELF.iLayerIndex := iLayerIndex;
   SELF.bFieldsDataWereSuccessfullyLoaded := FALSE;
   SELF.LoadData;
end;

destructor TIDB_FieldsVisibilityManager.Free;
begin
   SELF.ClearAllData;
   SELF.listFields.Free;
end;

procedure TIDB_FieldsVisibilityManager.SetScreenCursor;
begin
   AnOldScreenCursor := Screen.Cursor;
   Screen.Cursor := crHourGlass;
end;

procedure TIDB_FieldsVisibilityManager.RestoreScreenCursor;
begin
   Screen.Cursor := AnOldScreenCursor;
end;

{ This procedure restores data about fields in the DBGrid and position and sizes of
  the form to that this DBGrid belongs. if these data were not loaded from DB into
  internal structure, I try to read it. if these data were already successfully loaded,
  I use them from internal structure.}

procedure TIDB_FieldsVisibilityManager.RestoreFieldsStates (AGrid: TIDBPCDBGrid; bRestoreFieldsVisibleState: Boolean = TRUE; bRestoreFieldVisibleIndex: Boolean = TRUE);
var
   iI, iCountOfVisibleFields: Integer;
   AField: TField;
   pFP: ^TrecFieldsParameters;
begin
     // Try to get data from DB, if it is necessary.
   if not SELF.bFieldsDataWereSuccessfullyLoaded then
      SELF.bFieldsDataWereSuccessfullyLoaded := SELF.LoadFieldsState;
     // if data were loaded, use it.
   if SELF.bFieldsDataWereSuccessfullyLoaded then
   begin
        // Restore fields visibility.
      if bRestoreFieldsVisibleState then
      begin
           // Hide all fields.
         for iI := 0 to AGrid.DataSource.DataSet.FieldCount - 1 do
            AGrid.DataSource.DataSet.Fields[iI].Visible := FALSE;
      end;
        // Show all required fields.
      for iI := 0 to AGrid.DataSource.DataSet.FieldCount - 1 do
      begin
         AField := AGrid.DataSource.DataSet.Fields[iI];
         if (AField <> nil)
            and
            (not FieldIsInternalUsageField (AField.FieldName)) then
         begin
            if bRestoreFieldsVisibleState then
               AField.Visible := SELF.GetFieldVisibleState (AField.FieldName);
            AField.DisplayWidth := SELF.GetFieldDisplayWidth (AField.FieldName);
            if AField is TFloatField then TFloatField(AField).Displayformat:=self.GetFieldformat(AField.Fieldname);
         end;
      end; // for iI end
        // Restore fields order in the grid.
      if bRestoreFieldVisibleIndex then
         for iI := 0 to listFields.Count - 1 do
         begin
            pFP := listFields[iI];
            AField := AGrid.DataSource.DataSet.FindField (pFP.sFieldName);
            if AField <> nil then
               AField.Index := pFP.iFieldVisibleIndex;
         end; // for iI end
        // Check how many fields are visible. if there isn't any visible field,
        // set the field with name ProgisID as visible field.
      iCountOfVisibleFields := 0;
      for iI := 0 to AGrid.DataSource.DataSet.FieldCount - 1 do
         if AGrid.DataSource.DataSet.Fields[iI].Visible then
            Inc (iCountOfVisibleFields);
      if iCountOfVisibleFields = 0 then
// ++ Cadmesky Version 2.3.9
      begin
         AField := AGrid.DataSource.DataSet.FieldByName ('PROGISID');
         if AField <> nil then
            AField.Visible := true;
//         AGrid.DataSource.DataSet.FieldByName ('PROGISID').Visible := TRUE;
      end;
// -- Cadmesky Version 2.3.9
   end
     // if I haven't data about field's states, I will just in case set Visible property in FALSE
     // state for fields that are only purposed to internal usage.
   else
   begin
      for iI := 0 to AGrid.DataSource.DataSet.FieldCount - 1 do
      begin
         AField := AGrid.DataSource.DataSet.Fields[iI];
         if FieldIsInternalUsageField (AField.FieldName) or
            FieldIsCalculatedField (AField.FieldName) then
            AField.Visible := FALSE;
      end; // for iI end
        // 'ProgisID' field has to be shown as invisible by default.
      if AGrid.FieldCount > 1 then begin
         if AGrid.DataSource.DataSet.FieldByName('PROGISID') <> nil then begin
            AGrid.DataSource.DataSet.FieldByName ('PROGISID').Visible := FALSE;
         end;
         if AGrid.DataSource.DataSet.FieldByName('GUID') <> nil then begin
            AGrid.DataSource.DataSet.FieldByName ('GUID').Visible := FALSE;
         end;
      end;
   end;
end;

{ This procedure deletes all data in internal structure.}

procedure TIDB_FieldsVisibilityManager.ClearAllData;
var
   pFP: ^TrecFieldsParameters;
begin
   while SELF.listFields.Count > 0 do
   begin
      pFP := listFields[0];
      Dispose (pFP);
      listFields.Delete (0);
   end;
end;

function TIDB_FieldsVisibilityManager.FindField (sFieldName: AnsiString): Integer;
var
   pFP: ^TrecFieldsParameters;
   iI: Integer;
begin
   RESULT := -1;
   sFieldName := AnsiUpperCase (sFieldName);
   for iI := 0 to SELF.listFields.Count - 1 do
   begin
      pFP := listFields[iI];
      if AnsiUpperCase (pFP.sFieldName) = sFieldName then
      begin
         RESULT := iI;
         EXIT;
      end;
   end; // for iI end
end;

{ This procedure corrects information about field's state. if there is such field,
  data will be corrected. if there isn't such field, new record will be added and
  the information will be put.}

procedure TIDB_FieldsVisibilityManager.CorrectExistedOrAddNewField (sFieldName: AnsiString; iFieldWidth: Integer; bFieldIsVisible: Boolean; iFieldVisibleIndex: Integer; sFieldFormat: AnsiString);
var
   pFP: ^TrecFieldsParameters;
   iP: Integer;
begin
   iP := SELF.FindField (sFieldName);
   if iP < 0 then
   begin
      New (pFP);
      pFP.sFieldName := sFieldName;
      pFP.iFieldsDisplayWidth := iFieldWidth;
      pFP.bFieldIsVisible := bFieldIsVisible;
      pFP.iFieldVisibleIndex := iFieldVisibleIndex;
      if (sFieldFormat<>' ')  then pFP.sFieldFormat:=sFieldFormat;
      SELF.listFields.Add (pFP);
   end
   else
   begin
      pFP := listFields[iP];
      pFP.iFieldsDisplayWidth := iFieldWidth;
      pFP.bFieldIsVisible := bFieldIsVisible;
      pFP.iFieldVisibleIndex := iFieldVisibleIndex;
      if (sFieldFormat<>' ')  then pFP.sFieldFormat:=sFieldFormat;
   end;
end;

{ This procedure store required information about fields in DBGrid.
  First of all I delete previous information and put new information
  to all fields in a data source.}

procedure TIDB_FieldsVisibilityManager.DetectFieldsStates (ADBGrid: TIDBPCDBGrid);
var
   iI: Integer;
   ProgisIDField, AField: TField;
begin
   SELF.ClearAllData;
// ++ Cadmensky Version 2.3.2
   try
      ProgisIDField := ADBGrid.DataSource.DataSet.FieldByName ('PROGISID');
      if ProgisIDField <> nil then
         if ProgisIDField.Index > 0 then
         begin
            for iI := 0 to ProgisIDField.Index - 1 do
            begin
               AField := ADBGrid.DataSource.DataSet.Fields[iI];
               AField.Index := AField.Index + 1;
            end;
            ProgisIDField.Index := 0;
         end;
   except
   end;
// -- Cadmensky Version 2.3.2

   for iI := 0 to ADBGrid.DataSource.DataSet.FieldCount - 1 do
   begin
      AField := ADBGrid.DataSource.DataSet.Fields[iI];
      if FieldIsInternalUsageField (AField.FieldName) then CONTINUE;
      if (AField is TFloatfield) then
      SELF.CorrectExistedOrAddNewField (AField.FieldName,
         AField.DisplayWidth,
         AField.Visible,
         AField.Index, TFloatfield(AField).DisplayFormat)
       else
       SELF.CorrectExistedOrAddNewField (AField.FieldName,
         AField.DisplayWidth,
         AField.Visible,
         AField.Index,' ' );
   end;
   SELF.bFieldsDataWereSuccessfullyLoaded := TRUE;
end;

{ This procedure returns visible state for a field.}

function TIDB_FieldsVisibilityManager.GetFieldVisibleState (sFieldName: AnsiString): Boolean;
var
   pFP: ^TrecFieldsParameters;
   iP: Integer;
begin
     // if the field is used to internal aims, the field must be invisible to the user.
   RESULT := FALSE;
   if FieldIsInternalUsageField (sFieldName) then EXIT;
     // Try to find information about the field. if such information will not be found,
     // I think this field has to be shown to the user. Except calculated fields.
   if not FieldIsCalculatedField (sFieldName) then
      RESULT := TRUE;
   iP := SELF.FindField (sFieldName);
   if iP > -1 then
   begin
      pFP := listFields[iP];
      RESULT := pFP.bFieldIsVisible;
   end;
end;

procedure TIDB_FieldsVisibilityManager.SetFieldVisibleState (sFieldName: AnsiString; bState: Boolean);
var
   pFP: ^TrecFieldsParameters;
   iP: Integer;
begin
   if FieldIsInternalUsageField (sFieldName) then EXIT;
   iP := SELF.FindField (sFieldName);
   if iP > -1 then
   begin
      pFP := listFields[iP];
      pFP.bFieldIsVisible := bState;
   end
// ++ Cadmensky
   else
      SELF.CorrectExistedOrAddNewField (sFieldName, 50, bState, listFields.Count - 1,'');
// -- Cadmensky
end;

{ This procedure returns display width for a field.}

function TIDB_FieldsVisibilityManager.GetFieldDisplayWidth (sFieldName: AnsiString): Integer;
var
   pFP: ^TrecFieldsParameters;
   iP: Integer;
begin
     // if the field is used to internal aims, the field must be invisible to the user
     // or its display width equals to zero.
   RESULT := 0;
   if FieldIsInternalUsageField (sFieldName) then EXIT;
   iP := FindField (sFieldName);
     // if the information was not found, I think the field must be shown with its
     // default width.
   if iP < 0 then EXIT;
   pFP := listFields[iP];
   RESULT := pFP.iFieldsDisplayWidth;
end;

{ This function returns an index of TField in Fields collection. }

function TIDB_FieldsVisibilityManager.GetFieldVisibleIndex (sFieldName: AnsiString): Integer;
var
   pFP: ^TrecFieldsParameters;
   iMaxIndex, iP: Integer;
begin
   iMaxIndex := 0;
   for iP := 0 to SELF.listFields.Count - 1 do
   begin
      pFP := listFields[iP];
      if pFP.iFieldVisibleIndex > iMaxIndex then
         iMaxIndex := pFP.iFieldVisibleIndex;
   end;
   RESULT := iMaxIndex + 1;
   if not FieldIsInternalUsageField (sFieldName) then
   begin
      iP := FindField (sFieldName);
      if iP > -1 then
      begin
         pFP := listFields[iP];
         RESULT := pFP.iFieldVisibleIndex;
      end;
   end;
end;

procedure TIDB_FieldsVisibilityManager.SetFieldVisibleIndex (sFieldName: AnsiString; iValue: Integer);
var
   pFP: ^TrecFieldsParameters;
   iP: Integer;
begin
   if FieldIsInternalUsageField (sFieldName) then EXIT;
   iP := FindField (sFieldName);
   if iP < 0 then EXIT;
   pFP := listFields[iP];
   pFP.iFieldVisibleIndex := iValue;
end;

procedure TIDB_FieldsVisibilityManager.CheckAndCorrectDescTableStructure;
var
   bFieldIsPresent: Boolean;
begin
// ++ Cadmensky Version 2.3.2
   if IDA.ADBItemsMan.GetReadyToCheckTableStructure (cs_CommonDescTableName) <> ci_All_Ok then
   begin
      IDA.ADBItemsMan.EndOfCheckingTable;
      Exit;
   end;

   if not IDA.ADbItemsMan.FieldExists ('_USER_ID') then
      IDA.ADbItemsMan.AddField (cs_CommonDescTableName, '_USER_ID', 1, 38);

   if not IDA.ADbItemsMan.FieldExists ('_PROJECT_ID') then
      IDA.ADbItemsMan.AddField (cs_CommonDescTableName, '_PROJECT_ID', 1, 38);

   if not IDA.ADbItemsMan.FieldExists (cs_CDT_LayerIndexFieldName) then
      IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_CDT_LayerIndexFieldName, 3, -1);

   if not IDA.ADbItemsMan.FieldExists (cs_FieldName) then
      IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_FieldName, 15, -1); // 15th type is BLOB

   IDA.ADBItemsMan.EndOfCheckingTable;
// -- Cadmensky Version 2.3.2

// ++ Commented by Cadmensky Version 2.3.2
{   if IDA.ADbItemsMan.FieldExists (cs_CommonDescTableName, '_USER_ID', bFieldIsPresent) <> ci_All_Ok then EXIT;
   if not bFieldIsPresent then IDA.ADbItemsMan.AddField (cs_CommonDescTableName, '_USER_ID', 1, 38);

   if IDA.ADbItemsMan.FieldExists (cs_CommonDescTableName, '_PROJECT_ID', bFieldIsPresent) <> ci_All_Ok then EXIT;
   if not bFieldIsPresent then IDA.ADbItemsMan.AddField (cs_CommonDescTableName, '_PROJECT_ID', 1, 38);

   if IDA.ADbItemsMan.FieldExists (cs_CommonDescTableName, cs_CDT_LayerIndexFieldName, bFieldIsPresent) <> ci_All_Ok then EXIT;
   if not bFieldIsPresent then IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_CDT_LayerIndexFieldName, 3, -1);

   if IDA.ADbItemsMan.FieldExists (cs_CommonDescTableName, cs_FieldName, bFieldIsPresent) <> ci_All_OK then EXIT;
   if not bFieldIsPresent then IDA.ADbItemsMan.AddField (cs_CommonDescTableName, cs_FieldName, 15, -1); // 15th type is BLOB}
// -- Commented by Cadmensky Version 2.3.2

end;

{ This procedure saves data about field's states (visible state and display width)
  in DB.}

procedure TIDB_FieldsVisibilityManager.SaveFieldsState;
var
   iI, iLen: Integer;
   pFP: ^TrecFieldsParameters;
   AQuery: TDQuery;
   AStream: TDBlobStream;
   sLayerIndex: AnsiString;
   sVersionSign: AnsiString;
begin
   SetScreenCursor;

   sLayerIndex := intToStr (SELF.iLayerIndex);
   IDA.ADbItemsMan.CreateCommonDescTable;
   SELF.CheckAndCorrectDescTableStructure;

   AQuery := TDQuery.Create (nil);
   AQuery.Master := IDA.DMaster;
   AQuery.SQL.Text := 'SELECT [' + cs_FieldName + '], [' + cs_CDT_LayerIndexFieldName + '] ' +
      'FROM [' + cs_CommonDescTableName + '] ' +
      'WHERE [' + cs_CDT_LayerIndexFieldName + '] = ' + sLayerIndex;
   try
      AQuery.Open;
      if AQuery.RecordCount < 1 then
      begin
         AQuery.Close;
         AQuery.ExecSQL ('INSERT INTO [' + cs_CommonDescTableName + '] ([' + cs_CDT_LayerIndexFieldName + ']) ' +
            'VALUES(' + sLayerIndex + ')');
         AQuery.SQL.Text := 'SELECT [' + cs_FieldName + '], [' + cs_CDT_LayerIndexFieldName + '] ' +
            'FROM [' + cs_CommonDescTableName + '] ' +
            'WHERE [' + cs_CDT_LayerIndexFieldName + '] = ' + sLayerIndex;
         AQuery.Open;
      end;
      AQuery.Edit;
      AStream := TDBlobStream (AQuery.CreateBlobStream (AQuery.FieldByName (cs_FieldName), bmWrite));
      if (AStream = nil) then raise Exception.Create ('A stream to saving data cannot be created.');
   except
      AQuery.Cancel;
      AQuery.Free;
      RestoreScreenCursor;
      EXIT;
   end;
   try
      AStream.Truncate;

      sVersionSign := cs_VerisonOfDBFormStorage;
      AStream.Write (Pointer (sVersionSign)^, Length (sVersionSign));

        // First of all write number of entries.
      iLen := listFields.Count;
      AStream.Write (iLen, SizeOf (iLen));
      for iI := 0 to listFields.Count - 1 do
      begin
         pFP := listFields[iI];
            // Write field name.
         iLen := Length (pFP.sFieldName);
         AStream.Write (iLen, SizeOf (iLen));
         AStream.Write (Pointer (pFP.sFieldName)^, iLen);
            // Write field size
         AStream.Write (pFP.iFieldsDisplayWidth, SizeOf (pFP.iFieldsDisplayWidth));
            // Write visible state of field
         AStream.Write (pFP.bFieldIsVisible, SizeOf (pFP.bFieldIsVisible));
            // Write visible index of the field.
         AStream.Write (pFP.iFieldVisibleIndex, SizeOf (pFP.iFieldVisibleIndex));
         iLen := Length (pFP.sFieldFormat);
         AStream.Write (iLen, SizeOf (iLen));
         AStream.Write (Pointer (pFP.sFieldFormat)^, iLen);
      end;
      AStream.Flush;
   finally
      AStream.Free;

      AQuery.Post;
      AQuery.Free;
      RestoreScreenCursor;
   end;
end;

{ This procedure loads data about field's states (visible state and display width)
  from DB.}

function TIDB_FieldsVisibilityManager.LoadFieldsState: Boolean;
var
   iCount, iI, iLen: Integer;
   pFP: ^TrecFieldsParameters;
   AQuery: TDQuery;
   AStream: TDBlobStream;
   sVersionSign: AnsiString;
   sLayerIndex: AnsiString;
   iVersionOfStorage: Integer;
begin
   RESULT := FALSE;

   SetScreenCursor;

   sLayerIndex := IntToStr (SELF.iLayerIndex);
   IDA.ADbItemsMan.CreateCommonDescTable;
   SELF.CheckAndCorrectDescTableStructure;

   AQuery := TDQuery.Create (nil);
   AQuery.Master := IDA.DMaster;
   AQuery.SQL.Text := 'SELECT [' + cs_FieldName + '] FROM [' + cs_CommonDescTableName + '] ' +
      'WHERE [' + cs_CDT_LayerIndexFieldName + '] = ' + sLayerIndex;
   try
      AQuery.Open;
      if AQuery.RecordCount < 1 then raise Exception.Create ('The data are missing.');
      AStream := TDBlobStream (AQuery.CreateBlobStream (AQuery.FieldByName (cs_FieldName), bmRead));
      if (AStream = nil) or (AStream.Size < 1) then raise Exception.Create ('A stream to loading data cannot be created.');
   except
      AQuery.Close;
      AQuery.Free;
      RestoreScreenCursor;
      EXIT;
   end;
   try
      SetLength (sVersionSign, Length (cs_VerisonOfDBFormStorage));
      try
         AStream.Position := 0;
         AStream.Read (Pointer (sVersionSign)^, Length (sVersionSign));
         iVersionOfStorage := StrToInt (Copy (sVersionSign, 5, 4)); // '#WG#____#WG#';
                                                                    //      ^^^^ - Version number
      except
         iVersionOfStorage := 0;
         AStream.Position := 0;
      end;
      ClearAllData;
        // Read number of entires.
      AStream.Read (iCount, SizeOf (iCount));
      if iCount > 9000000 then icount:=1;
      for iI := 0 to iCount - 1 do
      begin
         New (pFP);
            // Read field name.
         AStream.Read (iLen, SizeOf (iLen));
         SetLength (pFP.sFieldName, iLen);
         AStream.Read (Pointer (pFP.sFieldName)^, iLen);
            // Read field size
         AStream.Read (pFP.iFieldsDisplayWidth, SizeOf (pFP.iFieldsDisplayWidth));
            // Read visible state of field
         AStream.Read (pFP.bFieldIsVisible, SizeOf (pFP.bFieldIsVisible));
            // Visible index of the field.
         if iVersionOfStorage = 0 then
            pFP.iFieldVisibleIndex := iI
         else
            AStream.Read (pFP.iFieldVisibleIndex, SizeOf (pFP.iFieldVisibleIndex));
           // Format of the field
         if iVersionOfStorage >1 then
            begin
            AStream.Read (iLen, SizeOf (iLen));
            SetLength (pFP.sFieldFormat, iLen);
            AStream.Read (Pointer (pFP.sFieldFormat)^, iLen);
            end;
            // Add entry
         listFields.Add (pFP);
      end;
      RESULT := TRUE;
   finally
      AStream.Free;
      AQuery.Close;
      AQuery.Free;
      RestoreScreenCursor;
   end;
end;

procedure TIDB_FieldsVisibilityManager.LoadData;
begin
   if not SELF.bFieldsDataWereSuccessfullyLoaded then
      SELF.bFieldsDataWereSuccessfullyLoaded := SELF.LoadFieldsState;
end;

{ This procedure saves all required data in DB. }

procedure TIDB_FieldsVisibilityManager.SaveData;
begin
   SELF.SaveFieldsState;
end;

function TIDB_FieldsVisibilityManager.GetAllAttributeFieldNames (bUsingVisibleOrder: Boolean = FALSE): AnsiString;
var
   iI, iP: Integer;
   pFP: ^TrecFieldsParameters;
   AList: TStringList;
begin
   RESULT := '';
   AList := TStringList.Create;
   for iI := 0 to SELF.listFields.Count - 1 do
   begin
      pFP := listFields[iI];
      if not FieldIsInternalUsageField (pFP.sFieldName) then
         if not bUsingVisibleOrder then
              // if I have not to worry about order - simply add.
            AList.Add (pFP.sFieldName)
         else
         begin
              // if I have to worry about fields visible order - add in sorted list.
            for iP := 0 to AList.Count - 1 do
               if Integer (AList.Objects[iP]) > pFP.iFieldVisibleIndex then
                  BREAK;
            if iP < AList.Count then
            begin
               AList.Insert (iP, pFP.sFieldName);
               AList.Objects[iP] := Pointer (pFP.iFieldVisibleIndex);
            end
            else
            begin
               iP := AList.Add (pFP.sFieldName);
               AList.Objects[iP] := Pointer (pFP.iFieldVisibleIndex);
            end;
         end;

   end;
   RESULT := AList.Text;
   AList.Clear;
   Alist.Free;
end;

function TIDB_FieldsVisibilityManager.GetVisibleAttributeFieldNames (bUsingVisibleOrder: Boolean = FALSE): AnsiString;
var
   iI, iP: Integer;
   pFP: ^TrecFieldsParameters;
   AList: TStringList;
begin
   RESULT := '';
   AList := TStringList.Create;
   for iI := 0 to SELF.listFields.Count - 1 do
   begin
      pFP := listFields[iI];
      if (not FieldIsInternalUsageField (pFP.sFieldName)) and
         (pFP.bFieldIsVisible) then
      begin
         if not bUsingVisibleOrder then
              // if I have not to worry about order - simply add.
            AList.Add (pFP.sFieldName)
         else
         begin
              // if I have to worry about fields visible order - add in sorted list.
            for iP := 0 to AList.Count - 1 do
               if Integer (AList.Objects[iP]) > pFP.iFieldVisibleIndex then
                  BREAK;
            if iP < AList.Count then
            begin
               AList.Insert (iP, pFP.sFieldName);
               AList.Objects[iP] := Pointer (pFP.iFieldVisibleIndex);
            end
            else
            begin
               iP := AList.Add (pFP.sFieldName);
               AList.Objects[iP] := Pointer (pFP.iFieldVisibleIndex);
            end;
         end;
      end;
   end;
   if AList.Count < 1 then begin
      AList.Add ('ProgisID');
      AList.Add ('GUID');
   end;
   RESULT := AList.Text;
   AList.Clear;
   AList.Free;
end;

function TIDB_FieldsVisibilityManager.GetFieldFormat(
  sFieldName: AnsiString): AnsiString;
var
   pFP: ^TrecFieldsParameters;
   iP: Integer;
begin
   RESULT := '';
   iP := SELF.FindField (sFieldName);
   if iP > -1 then
   begin
      pFP := listFields[iP];
      RESULT := pFP.sFieldFormat;
   end;
end;

end.

