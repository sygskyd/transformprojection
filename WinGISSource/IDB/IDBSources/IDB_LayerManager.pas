unit IDB_LayerManager;
{
Internal database. Ver. II.
This module provides the functionality of the layer that providess a management
  of components that needed to each layer of each project of WinGIS.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 30-01-2001

----
Brovak Vladimir, 11-04-2002

At this and anothers modules we are now using Power Broker.
It is a easy copy of Current DataSet; We work with it for faster
making some operations

---
}
interface

uses Classes, Forms, Gauges, Controls,
   DMaster, DTables, DDB, IDB_DBGrids,
   IDB_Access2DB, IDB_X_Man, //IDB_ExternalTablesMan,
   IDB_FieldsVisibilityMan, IDB_FormPositionMan,
   IDB_DescMan, AdoApi, Dialogs;

type
   TLayerNameItem = record
      iLayerIndex: integer;
      sLayerTableName: AnsiString;
   end;
   PLayerNameItem = ^TLayerNameItem;

   PLayerDef = ^TLayerDef;

   TLayerDef = record
      ALayer: Pointer;
      AX_Man: TIDB_X_Manager;
// ++ Commented by Cadmensky Version 2.3.7
//      AnAttFormStateMan: TIDB_FormPositionManager;
// -- Commented by Cadmensky Version 2.3.7
      AFieldsVisibilityMan: TIDB_FieldsVisibilityManager;
//        AnExtTablesMan: TIDB_ExternalTablesMan;
//      AnUserFieldsMan: TIDB_UserFieldsManager;
      ADescMan: TIDB_DescMan;
      listIDOfObjectsToBeDeletedFromDB: TList;
      listIDOfObjectsToBeDeletedFromWG: TList;
      listIDOfObjectsToBeMonitored: TList;
      listIDOfObjectsToBeRestored: TList;
// ++ Commented by Cadmensky Version 2.3.0
//      listIDOfObjectsToBeUsedForDragAndDrop: TList;
// -- Commented by Cadmensky Version 2.3.0
      listIDOfObjectToBeMade: TList;
      listRecordTemplate: TList;
      slCalcFields: TStringList;
   end;

   TIDB_LayerManager = class
   private
      AProjectFormHandle: Integer; // Here I store Handle of MDIChild window of a project.
      // It is required when I send a message of interaction between
      // IDB dll and WinGIS.
      AProject: Pointer;
      ADBAccessObject: TIDB_Access2DB_Level2;
      listLayerDefs: TList;

      AnOldScreenCursor: TCursor;

//      bUserCalcFields: boolean;
// ++ Cadmensky
      listLayersNotAdded: TList;
// -- Cadmensky

      procedure SetScreenCursor;
      procedure RestoreScreenCursor;

      procedure ClearData;

      procedure DeleteLayer (iLayerPos: Integer); overload;
      procedure CloseAttributeWindow (iLayerPos: Integer); overload;
   public

      constructor Create (AProjectFormHandle: Integer; AProject: Pointer; AnAccess2DBObject: TIDB_Access2DB_Level2);
      destructor Free;

      // ***************     INSIDE  IDB     ***************

      function AddLayer (ALayer: Pointer; bNeedToAsk: boolean = false): Integer;
      procedure DeleteLayer (ALayer: Pointer); overload;
      function FindLayer (ALayer: Pointer): Integer;

      function ShowAttributeWindow (ALayer, AALayer: Pointer): TForm;
      procedure CloseAttributeWindow (ALayer: Pointer); overload;
      procedure CloseAllWindows;

      procedure RefreshAllDataWindows;
      procedure ApplyFieldsVisibilityChanges (ALayer: Pointer);

//      function ShowTableStructure (ALayer: Pointer; ADataSet: TDADODataSet; var iTop: Integer; var iLeft: Integer; var iWidth: Integer; var iHeight: Integer; iStartTabNumber: Integer): Boolean; overload;
      function ShowTableStructure (ALayer: Pointer; var iTop: Integer; var iLeft: Integer; var iWidth: Integer; var iHeight: Integer; iStartTabNumber: Integer): Boolean; overload;
      function ShowFieldVisibility (ALayer: Pointer; var iTop: Integer; var iLeft: Integer; var iWidth: Integer; var iHeight: Integer): Boolean;

      procedure SaveResults (ALayer: Pointer; ADataSet: TDADODataSet; sSQL: AnsiString; AGauge: TGauge = nil);
      function LoadData (ALayer: Pointer; ADataSet: TDADODataSet; AGauge: TGauge = nil): Boolean;

      procedure StoreParametersOfViewForm (ALayer: Pointer; AForm: TForm);
      procedure RestoreFieldsStates (ALayer: Pointer; ADBGrid: TIDBPCDBGrid; bRestoreFieldsVisibleState: Boolean = TRUE; bRestoreFieldVisibleIndex: Boolean = TRUE);
      procedure DetectFieldsStates (ALayer: Pointer; ADBGrid: TIDBPCDBGrid);

      property DataAccessObject: TIDB_Access2DB_Level2 read ADBAccessObject;
      function GetLayerFieldsVisibilityManager (ALayer: Pointer): TIDB_FieldsVisibilityManager;

      procedure DeleteLayerTables (ALayer: Pointer; sAttTableName: AnsiString);

      // By help of this procedure WinGIS asks ID of all objects that are deleted by the user
      // from DB user inaterface.
      function GetItemIDToBeDeleted_FromIDB (ALayer: Pointer): Integer;

      // ***************    FROM  WINGIS     ***************
                // These procedures do action of deleting in IDB of objects that were deleted by the user
                // through WinGIS user interface.
      function GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG: Boolean;
      procedure ReceiveIDOfObjectToBeDeleted_FromWG (ALayer: Pointer; iObjectID: Integer);
      procedure DeleteObjects_FromWG;
      // These procedures do action of monitoring of objects that were selected from WinGIS user interface.
      function GetReadyToReceiveIDsOfObjectsForMonitoring: Boolean;
      procedure ReceiveIDOfObjectForMonitoring (ALayer: Pointer; iObjectID: Integer);
      procedure FillMonitoringData (AForm: TForm);
      procedure GetInfoObjectParams (ItemNumber: integer; var iLayerIndex: Integer; var iItemID: Integer);
      // These procedures do action of restoring deleted data in an attribute table.
      // I mean that during deleting operation I didn't delete data indeed but marked them as 'Deleted'.
      // Now I will mark them as 'Normal'.
      function GetReadyToReceiveIDsOfObjectsToBeRestored: Boolean;
      procedure ReceiveIDOfObjectToBeRestored (ALayer: Pointer; iObjectID: Integer);
      procedure ExecuteRestoring;
// ++ Commented by Cadmensky Version 2.3.0
{      // These procedures do action of copying data from selected data row into database rows of selected
      // objects. The source string is selected by the user through IDB interface. The objects the user
      // selects through WinGIS interface.
      function GetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDrop: Boolean;
      procedure ReceiveIDOfObjectToBeUsedForDragAndDrop (ALayer: Pointer; iObjectID: Integer);
      procedure ExecuteDragAndDrop (ASourceGridOfDragging: Pointer);}
// -- Commented by Cadmensky Version 2.3.0
      // These procedures do action of making the annotations for objects that were selected from
      // WinGIS interface.
      function GetReadyToReceiveIDsOfObjectsToBeMade: Boolean;
      procedure ReceiveIDOfObjectToBeMade (ALayer: Pointer; iItemID: Integer);
      procedure MakeAnnotations;
      {brovak}
      procedure MakeChart;
      {brovak}

      procedure InsertItem (ALayer: Pointer; iNewItemIndex: Integer; bAnExternalDBIsWorking: Boolean);
      function GetReadyToInsertRecords (ALayer: Pointer; ARecordTemplate: TList): Integer;
      function InsertRecord (ALayer: Pointer; iItemIndex: Integer; bNeedToUpdateRecordAnyway: Boolean = false): Boolean;

      // AX connection
      // This is the interface that allows to an external object to get an access to attribute data.
      function X_GotoObjectRecord (ALayer: Pointer; iItemIndex: Integer): Boolean;
      function X_AddRecord (ALayer: Pointer; iItemIndex: Integer): Boolean;
      function X_GetFieldCount (ALayer: Pointer): Integer;
      function X_GetFieldName (ALayer: Pointer; iFieldIndex: Integer): AnsiString;
      function X_GetFieldValue (ALayer: Pointer; sFieldName: AnsiString): Variant; overload;
      function X_GetFieldValue (ALayer: Pointer; iFieldIndex: Integer): Variant; overload;
      procedure X_SetFieldValue (ALayer: Pointer; sFieldName: AnsiString; FieldValue: Variant); overload;
      procedure X_SetFieldValue (ALayer: Pointer; iFieldIndex: Integer; FieldValue: Variant); overload;

      // ***************     IN   WINGIS     ***************
      function GetReadyToReceiveIDsOfObjectToBeDeleted_FromDB (ALayer: Pointer): Boolean;
      procedure ReceiveIDOfObjectToBeDeleted_FromDB (ALayer: Pointer; iObjectID: Integer);
      procedure DeleteObjectsInProject_FromDB (ALayer: Pointer);

//      function GetLayerExtTablesMan (ALayer: Pointer): TIDB_ExternalTablesMan;
//      function GetLayerUserFieldsMan (ALayer: Pointer): TIDB_UserFieldsManager;
      function GetLayerDescMan (ALayer: Pointer): TIDB_DescMan;

// ++ Commented by Cadmensky Version 2.3.2
//      function GetQueryResultWindows (AProject: Pointer): TStringList;
// ++ Commented by Cadmensky Version 2.3.2
      function ShowQueryResultWindow (AProject, ALayer: Pointer; var AWindow: TForm; sCaption, sSQLText: AnsiString): Boolean;

//      procedure MakeExtTablesLinks;
      // ++ Cadmensky
// ++ Commented by Cadmensky Version 2.3.7
//      procedure RefreshView (ALayer: Pointer);
// -- Commented by Cadmensky Version 2.3.7
      procedure FillCalculatedFields (ALayer: Pointer);
      procedure FillCalculatedField (ALayer: Pointer; iItemID: integer);
      function GetSelectedObjectsIDs (ALayer: Pointer): AnsiString;
      function GetTableName (ALayer: Pointer; var sFieldName: AnsiString): AnsiString;
// ++ Cadmensky Version 2.2.9
      function GetCalcFieldIsUsed (ALayer: Pointer; sFieldName: AnsiString): Boolean;
// -- Cadmensky Version 2.2.9
      procedure SetCalcFieldIsUsed (ALayer: Pointer; sFieldName: AnsiString; bValue: boolean);
      function GetUsedCalcFieldsNames (ALayer: Pointer): AnsiString;
      procedure DeleteATable(stablename: AnsiString);
      // -- Cadmensky
   end;

implementation

uses SysUtils, DB, Windows, Messages,
    IDB_Consts, IDB_Messages, IDB_View, IDB_StructMan, IDB_LanguageManager,
   IDB_DataMan, IDB_Monitor, IDB_Utils, IDB_WG_Func, IDB_StructureAndProperties,
   IDB_CallbacksDef, IDB_Monitor1, IDB_Progress,IDB_MainManager, ComObj;

constructor TIDB_LayerManager.Create (AProjectFormHandle: Integer; AProject: Pointer; AnAccess2DBObject: TIDB_Access2DB_Level2);
begin
   SELF.AProjectFormHandle := AProjectFormHandle;
   SELF.AProject := AProject;
   SELF.ADBAccessObject := AnAccess2DBObject;
   SELF.listLayerDefs := TList.Create;
   SELF.listLayersNotAdded := TList.Create;
end;

destructor TIDB_LayerManager.Free;
begin
   SELF.ClearData;
   SELF.listLayerDefs.Free;
   SELF.listLayersNotAdded.Free;
   self.DeleteATable('tmpthemmap');
end;

procedure TIDB_LayerManager.SetScreenCursor;
begin
   SELF.AnOldScreenCursor := Screen.Cursor;
   Screen.Cursor := crHourGlass;
   Application.ProcessMessages;
   Application.HandleMessage;
end;

procedure TIDB_LayerManager.RestoreScreenCursor;
begin
   Screen.Cursor := SELF.AnOldScreenCursor;
end;

procedure TIDB_LayerManager.ClearData;
begin
   // Delete information about all layers.
   while listLayerDefs.Count > 0 do
      SELF.DeleteLayer (0);
end;

function TIDB_LayerManager.FindLayer (ALayer: Pointer): Integer;
var
   iP: Integer;
   pLD: PLayerDef;
begin
   RESULT := -1;
   for iP := 0 to listLayerDefs.Count - 1 do
   begin
      pLD := listLayerDefs[iP];
      if pLD.ALayer = ALayer then
      begin
         RESULT := iP;
         EXIT;
      end;
   end;
end;

function TIDB_LayerManager.AddLayer (ALayer: Pointer; bNeedToAsk: boolean = false): Integer;
var
   iI, iP, iLayerIndex: Integer;
   pLD: PLayerDef;
   sAtttableName: AnsiString;
   bFound: boolean;
begin
   RESULT := -1;  

   iP := SELF.FindLayer (ALayer);
   if iP > -1 then
   begin
      RESULT := iP;
      EXIT; // We needn't to add this layer. There is already here.
   end;

   if not AnIDBMainMan.UseNewATTNames (AProject) then
      sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
   else
      sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

   if not ADBAccessObject.ADbItemsMan.TableExists (sAttTableName) then
   begin
      bFound := false;
      for iI := 0 to SELF.listLayersNotAdded.Count - 1 do
         if SELF.listLayersNotAdded[iI] = ALayer then
         begin
            bFound := true;
            break;
         end;
      if bFound then
      begin
         if bNeedToAsk then
            SELF.listLayersNotAdded.Delete (iI)
         else
            exit;
      end;
      if MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (465), [GetLayerNameFunc (ALayer)])), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) <> mrYes then
      begin
         SELF.listLayersNotAdded.Add (ALayer);
         exit;
      end;
   end;

   if not ADBAccessObject.ADbItemsMan.CreateAllLayerTables (ALayer) then
      exit;

   // Otherwise, add it.
   New (pLD);
   pLD.ALayer := ALayer;

   iLayerIndex := GetLayerIndexFunc (ALayer);


// ++ Commented by Cadmensky Version 2.3.7
//   pLD.AnAttFormStateMan := TIDB_FormPositionManager.Create (ADBAccessObject, iLayerIndex);
// -- Commented by Cadmensky Version 2.3.7
   pLD.AFieldsVisibilityMan := TIDB_FieldsVisibilityManager.Create (ADBAccessObject, iLayerIndex);
//   pLD.AnExtTablesMan := TIDB_ExternalTablesMan.Create (SELF.AProject, ALayer, ADBAccessObject);
// ++ Cadmensky Version 2.3.8
 {  if pLD.AnExtTablesMan.IAmInUse then
   begin
      if pLD.AnExtTablesMan.MakeExtTablePool (true) then
         pLD.AnExtTablesMan.SetParamsToExtTablePool;
   end;     }
   ShowPercentAtWinGISProc (1, 1455, 36);
   pLD.AX_Man := TIDB_X_Manager.Create (SELF.ADBAccessObject.DMaster, {pLD.AnExtTablesMan,} sAttTableName);
   pLD.AX_Man.StartConnection;
// -- Cadmensky Version 2.3.8
   ShowPercentAtWinGISProc (1, 1455, 37);
   pLD.ADescMan := TIDB_DescMan.Create (AProject, ADBAccessObject, ALayer);
   ShowPercentAtWinGISProc (1, 1455, 45);
   pLD.listIDOfObjectsToBeDeletedFromDB := TList.Create;
   pLD.listIDOfObjectsToBeDeletedFromWG := TList.Create;
   pLD.listIDOfObjectsToBeMonitored := TList.Create;
   pLD.listIDOfObjectsToBeRestored := TList.Create;
   pLD.listIDOfObjectToBeMade := TList.Create;
   pLD.slCalcFields := TStringList.Create;
   // Add data about the layer in the list.
   listLayerDefs.Add (pLD);
   ShowPercentAtWinGISProc (1, 1455, 50);
   CheckRequiredFields (Self, ALayer, sAttTableName);

   RESULT := listLayerDefs.Count - 1;
end;

{ This procedure deletes layer's entry by layer's position in the list. }

procedure TIDB_LayerManager.DeleteLayer (iLayerPos: Integer);
var
   pLD: PLayerDef;
begin
   if (iLayerPos < 0) or (iLayerPos > listLayerDefs.Count - 1) then EXIT;
   pLD := listLayerDefs[iLayerPos];
// ++ Commented by Cadmensky Version 2.3.7
{   if pLD.AnAttFormStateMan <> nil then
   begin
      CloseAttributeWindow (iLayerPos);
      pLD.AnAttFormStateMan.SaveData;
      pLD.AnAttFormStateMan.Free;
   end;}
// -- Commented by Cadmensky Version 2.3.7
   if pLD.AFieldsVisibilityMan <> nil then
   begin
      pLD.AFieldsVisibilityMan.SaveData;
      pLD.AFieldsVisibilityMan.Free;
   end;
   pLD.AX_Man.Free;
   //pLD.AnExtTablesMan.Free;
   pLD.ADescMan.Free;
   // Clear the list that contains data of obkects to be deleted from DB.
   pLD.listIDOfObjectsToBeDeletedFromDB.Clear;
   pLD.listIDOfObjectsToBeDeletedFromDB.Free;
   // Clear the list that contains data of objects to be deleted from WinGIS.
   pLD.listIDOfObjectsToBeDeletedFromWG.Clear;
   pLD.listIDOfObjectsToBeDeletedFromWG.Free;
   // Clear the list that contains data of objects for monitoring.
   pLD.listIDOfObjectsToBeMonitored.Clear;
   pLD.listIDOfObjectsToBeMonitored.Free;

   pLD.listIDOfObjectsToBeRestored.Clear;
   pLD.listIDOfObjectsToBeRestored.Free;

   pLD.listIDOfObjectToBeMade.Clear;
   pLD.listIDOfObjectToBeMade.Free;

   pLD.slCalcFields.Clear;
   pLD.slCalcFields.Free;

   Dispose (pLD);
   listLayerDefs.Delete (iLayerPos);
end;

{ This procedure deletes layer's entry by layer's pointer. It finds the layer position
  in the list and calls the deleting by the layer's position in the list. }

procedure TIDB_LayerManager.DeleteLayer (ALayer: Pointer);
var
   iP: Integer;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then EXIT; // There isn't such layer.
   SELF.DeleteLayer (iP);
end;

function TIDB_LayerManager.ShowAttributeWindow (ALayer, AALayer: Pointer): TForm;
var
   pLD: PLayerDef;
   iP, iLayerIndex: Integer;
   iLeft, iTop, iWidth, iHeight: Integer;
   AForm: TForm;
   sLayerIndex,
      sAttTableName: AnsiString;
   AnOldCursor: TCursor;
   bFound: boolean;
   bButtonDown, bZoomOrSelect: boolean;
// ++ Cadmensky Version 2.3.7
   AnAttFormStateMan: TIDB_FormPositionManager;
// -- Cadmensky Version 2.3.7
begin
   RESULT := nil;

   iLeft := -1;
   iTop := -1;
   iWidth := -1;
   iHeight := -1;

   ShowPercentAtWinGISProc (0, 1450, 0);

   bFound := false;
   if AnIDBMainMan.AViewForm <> nil then
   begin
      if AnIDBMainMan.AViewForm.AProject = SELF.AProject then
         bFound := true
      else
         AnIDBMainMan.AViewForm.Close;
   end;

   if bFound then
   begin
      AForm := AnIDBMainMan.AViewForm;
      if TIDB_ViewForm (AForm).ALayer = ALayer then
      begin
         if AForm.FormStyle = fsStayOnTop then
            begin
            AForm.Show;
            end;
         RESULT := AForm;
         ShowPercentAtWinGISProc (2, 0, 0);
         //AForm.BringtoFront;
         exit;
      end
      else
      begin
         StoreParametersOfViewForm (TIDB_ViewForm (AForm).ALayer, TIDB_ViewForm (AForm));
         TIDB_ViewForm (AForm).ALayer := ALayer;
         TIDB_ViewForm (AForm).ActiveLayer := AALayer;
      end;
   end;

   ShowPercentAtWinGISProc (1, 1451, 15);

   iP := SELF.FindLayer (ALayer);
   if iP < 0 then
   begin;
      ShowPercentAtWinGISProc (1, 1455, 35);
      iP := AddLayer (ALayer, true);
      if iP < 0 then
      begin
         if AnIDBMainMan.AViewForm <> nil then
            AnIDBMainMan.AViewForm.Close;
         ShowPercentAtWinGISProc (2, 0, 0);
         exit;
      end;
   end;
   ShowPercentAtWinGISProc (1, 1452, 70);

   AnOldCursor := Screen.Cursor;
   Screen.Cursor := crHourGlass;
   try
      // if there isn't such window, create it.
      pLD := listLayerDefs[iP];
      iLayerIndex := GetLayerIndexFunc (pLD.ALayer);
      sLayerIndex := IntToStr (iLayerIndex);
      if not AnIDBMainMan.UseNewATTNames (AProject) then
         sAttTableName := cs_AttributeTableNamePrefix + sLayerIndex
      else
         sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, pLD.ALayer);

//      if not bFound then    // WAI load always form state
// ++ Cadmensky Version 2.3.7
//      begin
         AnAttFormStateMan := AnIDBMainMan.GetFormPositionManager (SELF.AProject);
         if AnAttFormStateMan <> nil then
            AnAttFormStateMan.GetFormState (iLeft, iTop, iWidth, iHeight, bButtonDown, bZoomOrSelect);
//         pLD.AnAttFormStateMan.GetFormState (iLeft, iTop, iWidth, iHeight, bButtonDown);
//      end;
// -- Cadmensky Version 2.3.7
      ShowPercentAtWinGISProc (1, 1453, 85);

      if not bFound then
      begin
         TIDB_ViewForm (AForm) := TIDB_ViewForm.Create (AWinGISMainForm, SELF, SELF.AProject, ALayer, AALayer);
         AnIDBMainMan.AViewForm := TIDB_ViewForm (AForm);
      end;

      RESULT := AForm;
      Application.ProcessMessages;
      ShowPercentAtWinGISProc (1, 1453, 86);
      // I use TDQuery as a source.
      if not bFound then
      begin
         TIDB_ViewForm (AForm).ViewDataSource.DataSet := TIDB_ViewForm (AForm).DataQuery;
         TDADODataSet (TIDB_ViewForm (AForm).ViewDataSource.DataSet).Master := SELF.ADBAccessObject.DMaster;
      end;

      // The old way to show attribute data. It is used when we have only to show attribute data of IDB.

      TIDB_ViewForm (AForm).sAttTableName := sAttTableName;
      TIDB_ViewForm (AForm).CPB_SortField := '[' + sAttTableName + '].[PROGISID] ASC';
      TIDB_ViewForm (AForm).CPB_Order := ' ASC';

      TDADODataSet (TIDB_ViewForm (AForm).ViewDataSource.DataSet).SortAdd ('ProgisID', smASC);
      TIDB_ViewForm (AForm).ViewDBGrid.SortField := 'PROGISID';
      ShowPercentAtWinGISProc (1, 1453, 87);
   {   if pLD.AnExtTablesMan.IAmInUse then
         TIDB_ViewForm (AForm).MakeExtTablesLinking (FALSE)
      else  }
      begin
         TIDB_ViewForm (AForm).DataQuery.SQL.Text := 'SELECT * FROM [' + sAttTableName + '] WHERE (' + cs_ShowAllWithoutDeletedObjectsFilter + ')';
         // I try open the layer atribute table
         try
            ShowPercentAtWinGISProc (1, 1453, 88);
            TIDB_ViewForm (AForm).ViewDataSource.DataSet.Open;
            ShowPercentAtWinGISProc (1, 1453, 89);
         except
         if showtablepropertiesdlg then TIDB_ViewForm (AForm).TableStructurePopUpClick(Nil);
         showtablepropertiesdlg:=False;
         end;

         TIDB_ViewForm (AForm).Create_Power_Broker;

      end;
      ShowPercentAtWinGISProc (1, 1453, 94);
      pLD.AFieldsVisibilityMan.RestoreFieldsStates (TIDBPCDBGrid (TIDB_ViewForm (AForm).ViewDBGrid));
   finally
      Screen.Cursor := AnOldCursor;
   end;

   if bFound then
      TIDB_ViewForm (AForm).Caption := Format (ALangManager.GetValue (58), [GetLayerNameFunc (ALayer)]);

   ShowPercentAtWinGISProc (1, 1454, 95);
try
   if AnIDBMainMan.AMonitoringForm <> nil then
   begin
      if AnIDBMainMan.AMonitoringForm.WindowState <> wsMaximized then
      begin
         AForm.Top := AnIDBMainMan.AMonitoringForm.Top;
         AForm.Left := AnIDBMainMan.AMonitoringForm.Left;
         AForm.Width := AnIDBMainMan.AMonitoringForm.Width;
         AForm.Height := AnIDBMainMan.AMonitoringForm.Height;
      end;
      TIDB_ViewForm (AForm).MakeMyWindowState (AnIDBMainMan.AMonitoringForm.WindowState);
      AnIDBMainMan.AMonitoringForm.Close;
   end;
   Application.ProcessMessages;
   ShowPercentAtWinGISProc (1, 1454, 96);
   if AForm.FormStyle = fsStayOnTop then
   AForm.Show;
   ShowPercentAtWinGISProc (1, 1454, 97);
   //      AForm.WindowState := wsNormal;
      if iLeft > -1 then AForm.Left := iLeft;
      if iTop > -1 then AForm.Top := iTop;
      if iWidth > -1 then AForm.Width := iWidth;
      if iHeight > -1 then AForm.Height := iHeight;

      try
         TIDB_ViewForm (AForm).AutoZoom.Checked:=bZoomOrSelect;
         TIDB_ViewForm (AForm).AutoSelect.Checked:=not bZoomOrSelect;
         TIDB_ViewForm (AForm).SetAutoAddToSelectedButtonDown (bButtonDown);
      except
      end;
   TIDB_ViewForm (AForm).ShowRecordsForSelectedObjects;
finally
   ShowPercentAtWinGISProc (2, 0, 0);
end;
end;

procedure TIDB_LayerManager.CloseAttributeWindow (iLayerPos: Integer);
var
   pLD: PLayerDef;
   iI: Integer;
   AForm: TForm;
begin
   if (iLayerPos < 0) or (iLayerPos > listLayerDefs.Count - 1) then EXIT;
   pLD := listLayerDefs[iLayerPos];
   iI := 0;
   while iI < Screen.FormCount do
   begin
      AForm := Screen.Forms[iI];
      if (AForm is TIDB_ViewForm) and (TIDB_ViewForm (AForm).ALayer = pLD.ALayer) then
      begin
         try
            AForm.Close;
            Application.ProcessMessages;
         except
         end;
         iI := 0; // Try to find again.
      end
      else
         Inc (iI);
   end; // while end
end;

procedure TIDB_LayerManager.CloseAttributeWindow (ALayer: Pointer);
var
   iP: Integer;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then EXIT; // There isn't such layer.
   SELF.CloseAttributeWindow (iP);
end;

procedure TIDB_LayerManager.CloseAllWindows;
var
   iI: Integer;
begin
   for iI := 0 to listLayerDefs.Count - 1 do
      SELF.CloseAttributeWindow (iI);
end;

procedure TIDB_LayerManager.RefreshAllDataWindows;
var
   iI: Integer;
   AForm: TForm;
begin
{   if AnIDBMainMan.AViewForm <> nil then
   begin
      SendMessage (AnIDBMainMan.AViewForm.Handle, WM_IDB_Message, IDB_RefreshDataWindow, 0);
      Application.ProcessMessages;
   end;}
   for iI := 0 to Screen.FormCount - 1 do
   begin
      AForm := Screen.Forms[iI];
      if (AForm is TIDB_ViewForm) and (TIDB_ViewForm (AForm).AProject = SELF.AProject) then
      begin
         { I have to send a message due to the following cause:
           When we change a GeoText information we have to refresh also all data windows that
           belongs to the concrete WinGIS project. But the project's and GeoText's handlings work
           in two different threads. And when I demand to refresh data table from GeoText thread,
           such refreshing doesn't appear. to avoid this problem I send a message and the data window
           receives it and refreshes data table successfully. to avoid to write two different procedures
           that will do the same task (for project's and GoeText's handling) I decided to send the message
           in both cases. }
         SendMessage (AForm.Handle, WM_IDB_Message, IDB_RefreshDataWindow, 0);
         Application.ProcessMessages;
      end;
   end; // for iI end
end;

procedure TIDB_LayerManager.ApplyFieldsVisibilityChanges (ALayer: Pointer);
var
   iI: Integer;
   AForm: TForm;
   bFound: Boolean;
begin
   bFound := FALSE;
   for iI := 0 to Screen.FormCount - 1 do
   begin
      AForm := Screen.Forms[iI];
      if (AForm is TIDB_ViewForm) and (TIDB_ViewForm (AForm).ALayer = ALayer) then
      begin
         bFound := TRUE;
         BREAK;
      end;
   end; // for iI end
   if bFound then
   begin
      SendMessage (AForm.Handle, WM_IDB_Message, IDB_RefreshDataWindow, 1);
      Application.ProcessMessages;
   end;
end;

{function TIDB_LayerManager.ShowTableStructure (ALayer: Pointer; ADataSet: TDADODataSet; var iTop: Integer; var iLeft: Integer; var iWidth: Integer; var iHeight: Integer; iStartTabNumber: Integer): Boolean;
var
   SPF: TIDB_StructureAndPropertiesForm;
   AStructMan: TIDB_StructMan;
   ADescMan: TIDB_DescMan;
   iP, iI: Integer;
   pLD: PLayerDef;
   iMark: Integer;
   AField: TField;
   AFieldsList: TStringList;
   iLayerIndex: integer;
   sAttTableName: AnsiString;
begin
   RESULT := FALSE;
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then EXIT; // There isn't such layer.
   pLD := listLayerDefs[iP];

   iLayerIndex := GetLayerIndexFunc (ALayer);

   AStructMan := TIDB_StructMan.Create (AProject, SELF.ADBAccessObject);
   //     ADescMan := TIDB_DescMan.Create(SELF.ADBAccessObject, IntToStr(GetLayerIndexFunc(pLD.ALayer)));
   ADescMan := pLD.ADescMan;
   SPF := TIDB_StructureAndPropertiesForm.Create (Application, SELF.AProject, iLayerIndex, AStructMan);
   AFieldsList := TStringList.Create; // This list will contain only 'user field' - without calculated fields and without internal field (e.g. 'state' field).
   AFieldsList.Add ('ProgisID');
// ++ Cadmensky
   AFieldslist.Add (cs_CalcFieldAreaName);
   AFieldslist.Add (cs_CalcFieldXName);
   AFieldslist.Add (cs_CalcFieldYName);
   AFieldslist.Add (cs_CalcFieldSizeName);
   AFieldslist.Add (cs_CalcFieldTextName);
   AFieldslist.Add (cs_CalcFieldVerticesCount);
   AFieldslist.Add (cs_CalcFieldSymbolName);
   AFieldslist.Add (cs_CalcFieldLinkIDName);
// -- Cadmensky
   try
      SPF.Caption := Format (ALangManager.GetValue (107), [GetlayerNameFunc (pLD.ALayer)]);

      //        AStructMan.DetectStructure(ADataSet.Master, GetLayerIndexFunc(pLD.ALayer));
      if not AnIDBMainMan.UseNewATTNames (AProject) then
         sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (pLD.ALayer))
      else
         sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, pLD.ALayer);
      AStructMan.DetectStructure (ADataSet, sAttTableName);
      for iI := 0 to ADataSet.Fields.Count - 1 do
      begin
         AField := ADataSet.Fields[iI];
         if (AnsiUpperCase (AField.FieldName) <> 'PROGISID') and
            (not FieldIsInternalUsageField (AField.FieldName)) and
            (not FieldIsCalculatedField (AField.FieldName)) then
            AFieldsList.Add (AField.FieldName);
      end; // for iI end

              // Set the grid line count
      SPF.SG.RowCount := AFieldsList.Count + 2; // 1 line - for the title and 1 line - for the new field that can be added.
      // Fill the grid with the field names and set states of visibility of these fields.
      for iI := 0 to AFieldsList.Count - 1 do
      begin
         SPF.SG.Cells[SPF.FieldNameCol, iI + 1] := AFieldsList[iI];
         AField := ADataSet.FindField (AFieldsList[iI]);
         if (AField <> nil) then
         begin
            SPF.SG.Cells[SPF.FieldNameCol, iI + 1] := AField.FieldName;
            //               if AField.FieldKind = fkCalculated then
            if FieldIsCalculatedField (AField.FieldName) then
               SPF.SG.Objects[SPF.FieldNameCol, iI + 1] := Pointer (0)
            else
            begin
               SPF.SG.Objects[SPF.FieldNameCol, iI + 1] := Pointer (1);

             (*     SPF.AnnotAvailableListBox.Items.Add (AField.FieldName);
// ++ brovak
                  if (ADescman.FieldIsNumberType (AField.FieldName))
                     //                     if (AStructMan.FieldIsNumberType(AField.FieldName))
                  and (AnsiUpperCase (AField.FieldName) <> 'PROGISID') then
                     SPF.ChartAvailableListBox.Items.Add (AField.FieldName);
                     *)
// -- brovak
//               end;
            end;
            // Field visibility
            if AField.Visible then
               SPF.SG.Objects[SPF.FieldVisibleCol, iI + 1] := Pointer (1)
            else
               SPF.SG.Objects[SPF.FieldVisibleCol, iI + 1] := Pointer (0);
         end // if AField <> nil end
         else
         begin
            SPF.SG.Cells[SPF.FieldNameCol, iI + 1] := AFieldsList[iI];
            SPF.SG.Objects[SPF.FieldNameCol, iI + 1] := Pointer (2);
            SPF.SG.Objects[SPF.FieldVisibleCol, iI + 1] := Pointer (0);
         end;
      end; // for iI end
      SPF.SG.Objects[SPF.FieldNameCol, SPF.SG.RowCount - 1] := Pointer (1);
      // Set 'Geotext field' mark
      SPF.GeoTextFieldName := ADescMan.GetGeoTextFieldName;
      // Set 'Monitored ID field' mark
      SPF.MonitoredFieldName := ADescMan.GetMonitoredFieldName;
      // Set Annotations source fields
      SPF.AnnotationsSourceFieldNames := ADescMan.GetAnnotationFieldNames;

// ++ brovak
      SPF.ChartSourceFieldNames := ADescman.GetChartFieldNames;
// -- brovak
// ++ Cadmensky
      SPF.OverlaySourceFieldNames := ADescman.GetOverlayFieldNames;
      SPF.InfoSourceFieldNames := ADescman.GetInfoFieldNames;
// -- Cadmensky

      AFieldsList.Clear;
      AFieldsList.Free;

      // Which record I'm positioned on?
      try
         iMark := ADataSet.FieldByName (cs_InternalID_FieldName).AsInteger;
      except
         iMark := -1;
      end;
      ADataSet.Close;
      pLD.AX_Man.StopConnection;

      if (iTop < 0) and (iLeft < 0) and (iWidth < 0) and (iHeight < 0) then
         SPF.Position := poDesktopCenter
      else
      begin
         if iTop > -1 then SPF.Top := iTop;
         if iLeft > -1 then SPF.Left := iLeft;
         if iWidth > -1 then SPF.Width := iWidth;
         if iHeight > -1 then SPF.Height := iHeight;
      end;
     (* case iStartTabNumber of
         1: SPF.PageControl1.ActivePage := SPF.StructureTS;
         2: SPF.PageControl1.ActivePage := SPF.AnnotationsTS;
         3: SPF.PageControl1.ActivePage := SPF.ChartsTS;
      else *)
      SPF.PageControl1.ActivePage := SPF.StructureTS;
    //  end;
      try
         RESULT := SPF.ShowModal = mrOK;
         if SPF.WindowState <> wsMaximized then
         begin
            iTop := SPF.Top;
            iLeft := SPF.Left;
            iWidth := SPF.Width;
            iHeight := SPF.Height;
         end
         else
         begin
            iTop := -1;
            iLeft := -1;
            iWidth := -1;
            iHeight := -1;
         end
      finally
// ++ Cadmensky
         if RESULT then
            if SPF.bNeedToFillCalculatedFields then
               SELF.FillCalculatedFields (ALayer);
// -- Cadmensky
         pLD.AX_Man.StartConnection;
         try
            ADataSet.Open;
         except
         end;
         // Return to the old position
         if ADataSet.Active then
            if not ADataSet.Locate (cs_InternalID_FieldName, IntToStr (iMark), []) then
               ADataSet.First;
      end;

      if RESULT then
      begin
         SetScreenCursor;
         try
            // Set fields visibility
            for iI := 0 to ADataSet.Fields.Count - 1 do
               ADataSet.Fields[iI].Visible := FALSE;
            for iI := 1 to SPF.SG.RowCount - 1 do
            begin
               AField := ADataSet.FindField (SPF.SG.Cells[SPF.FieldNameCol, iI]);
               if AField <> nil then
               begin
                  AField.Visible := Integer (SPF.SG.Objects[SPF.FieldVisibleCol, iI]) = 1;
                  AField.Index := iI - 1;
               end;
            end; // for iI end
            // Geotext field
            ADescMan.SetGeoTextFieldName (SPF.GeoTextFieldName);
            // Monitored ID field
            ADescMan.SetMonitoredFieldName (SPF.MonitoredFieldName);
            // Get Annotations source fields
            ADescMan.SetAnnotationFieldNames (SPF.AnnotationsSourceFieldNames);
            // Get Charts source fields
            ADescMan.SetChartFierldNames (SPF.ChartSourceFieldNames);

            // ++ Cadmensky
            ADescMan.SetOverlayFieldNames (SPF.OverlaySourceFieldNames);
            ADescMan.SetInfoFieldNames (SPF.InfoSourceFieldNames);
            // -- Cadmensky

                          // Save them
            ADescMan.SetUserChoice;

            if SPF.ApplyToAllCB.Checked then
               SELF.RefreshAllDataWindows;
         finally
            RestoreScreenCursor;
         end;
      end;
      if AStructMan <> nil then
         AStructMan.Free;
      if SPF <> nil then
         SPF.Free;
   except
      if AStructMan <> nil then
         AStructMan.Free;
      if SPF <> nil then
         SPF.Free;
   end;
end;}

{function TIDB_LayerManager.ShowTableStructure (ALayer: Pointer; var iTop: Integer; var iLeft: Integer; var iWidth: Integer; var iHeight: Integer; iStartTabNumber: Integer): Boolean;
var
   ATable: TDTable;
begin
   ATable := TDTable.Create (nil);
   try
      ATable.Master := SELF.ADBAccessObject.DMaster;
      if not AnIDBMainMan.UseNewATTNames (AProject) then
         ATable.TableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
      else
         ATable.TableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);
      ATable.Open;
      ShowTableStructure (ALayer, TDADODataSet (ATable), iTop, iLeft, iWidth, iHeight, iStartTabNumber);
   finally
      ATable.Free;
   end;
end;}

function TIDB_LayerManager.ShowTableStructure (ALayer: Pointer; var iTop: Integer; var iLeft: Integer; var iWidth: Integer; var iHeight: Integer; iStartTabNumber: Integer): Boolean;
var
   iP, iI: Integer;
   pLD: PLayerDef;
   SPF: TIDB_StructureAndPropertiesForm;
   AStructMan: TIDB_StructMan;
   ADescMan: TIDB_DescMan;
//   AnExtTablesMan: TIDB_ExternalTablesMan;
   AQuery: TDQuery;
   sFieldName, sAttTableName: AnsiString;
   ACommonFieldsList, AVisibleFieldsList: TStringList;
begin
   RESULT := FALSE;
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then EXIT; // There isn't such layer.
   pLD := listLayerDefs[iP];

   if not AnIDBMainMan.UseNewATTNames (AProject) then
      sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
   else
      sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

   AStructMan := TIDB_StructMan.Create (AProject, SELF.ADBAccessObject);

   AQuery := TDQuery.Create (nil);
   AQuery.Master := SELF.ADBAccessObject.DMaster;

  { if pLD.AnExtTablesMan.GetUsedLinksCount > 0 then
   begin
      if pLD.AnExtTablesMan.JoiningSQL = '' then
         pLD.AnExtTablesMan.SetParamsToExtTablePool;
      AQuery.SQL.Text := pLD.AnExtTablesMan.JoiningSQL;
   end
   else  }
      AQuery.SQL.Text := 'SELECT * FROM [' + sAttTableName + ']';

   try
      AQuery.Open;
      AStructMan.DetectStructure (AQuery, sAttTableName);
   except
      AQuery.Free;
      exit;
   end;

   ADescMan := pLD.ADescMan;
   //     ADescMan := TIDB_DescMan.Create(SELF.ADBAccessObject, IntToStr(GetLayerIndexFunc(pLD.ALayer)));
   //     SPF := TIDB_StructureAndPropertiesForm.Create(Application, SELF.AProject, AStructMan);
   SPF := TIDB_StructureAndPropertiesForm.Create (Application, SELF.AProject, GetLayerIndexFunc (ALayer), AStructMan);
   try
      SPF.Caption := Format (ALangManager.GetValue (107), [GetLayerNameFunc (ALayer)]);

      ACommonFieldsList := TStringList.Create;
      AVisibleFieldsList := TStringList.Create;

{      ACommonFieldsList.Text := pLD.AFieldsVisibilityMan.GetAllAttributeFieldNames (TRUE);
      if ACommonFieldsList.Count < 1 then}
// ++ Cadmensky
      ACommonFieldsList.Add ('PROGISID');
      ACommonFieldsList.Add ('GUID');
      ACommonFieldsList.Add (cs_CalcFieldAreaName);
      ACommonFieldsList.Add (cs_CalcFieldXName);
      ACommonFieldsList.Add (cs_CalcFieldYName);
      ACommonFieldsList.Add (cs_CalcFieldSizeName);
      ACommonFieldsList.Add (cs_CalcFieldTextName);
      ACommonFieldsList.Add (cs_CalcFieldVerticesCount);
      ACommonFieldsList.Add (cs_CalcFieldSymbolName);
      ACommonFieldsList.Add (cs_CalcFieldRotationName);
      ACommonFieldsList.Add (cs_CalcFieldLinkIDName);
// -- Cadmensky
      for iI := 0 to AStructMan.GetFieldCount - 1 do
      begin
         sFieldName := AStructMan.GetFieldName (iI);
         if (AnsiUpperCase (sFieldName) <> 'PROGISID') and
            (AnsiUpperCase (sFieldName) <> 'GUID') and
            not FieldIsCalculatedField (sFieldName) then
//               ACommonFieldsList.Insert (0, sFieldName)
//            else
            ACommonFieldsList.Add (AStructMan.GetFieldName (iI));
      end;

      AVisibleFieldsList.Text := pLD.AFieldsVisibilityMan.GetVisibleAttributeFieldNames (FALSE);

      SPF.SG.RowCount := ACommonFieldsList.Count + 2;
      for iI := 0 to ACommonFieldsList.Count - 1 do
      begin
         sFieldName := ACommonFieldsList[iI];
         SPF.SG.Cells[SPF.FieldNameCol, iI + 1] := sFieldName;
         if FieldIsCalculatedField (sFieldName) then
         begin
            if pLD.slCalcFields.IndexOf (sFieldName) = -1 then
               SPF.SG.Objects[SPF.FieldNameCol, iI + 1] := Pointer (2)
            else
               SPF.SG.Objects[SPF.FieldNameCol, iI + 1] := Pointer (0);
         end
         else
         begin
            SPF.SG.Objects[SPF.FieldNameCol, iI + 1] := Pointer (1);
// ++ Cadmensky
(*brovak            SPF.AnnotAvailableListBox.Items.Add (sFieldName);
            if (ADescman.FieldIsNumberType (sFieldName))
               and (AnsiUpperCase (sFieldName) <> 'PROGISID') then
               SPF.ChartAvailableListBox.Items.Add (sFieldName);
*)
// -- Cadmensky
         end;

         if AVisibleFieldsList.IndexOf (sFieldName) > -1 then
            SPF.SG.Objects[SPF.FieldVisibleCol, iI + 1] := Pointer (1)
         else
            SPF.SG.Objects[SPF.FieldVisibleCol, iI + 1] := Pointer (0);
      end;

      ACommonFieldsList.Clear;
      ACommonFieldsList.Free;
      AVisibleFieldsList.Clear;
      AVisibleFieldsList.Free;

      SPF.SG.Objects[SPF.FieldNameCol, SPF.SG.RowCount - 1] := Pointer (1);

      // Set 'Geotext field' mark
      SPF.GeoTextFieldName := ADescMan.GetGeoTextFieldName;
      // Set 'Monitored ID field' mark
      SPF.MonitoredFieldName := ADescMan.GetMonitoredFieldName;
      // Set Annotations source fields
      SPF.AnnotationsSourceFieldNames := ADescMan.GetAnnotationFieldNames;

      {brovak}
      SPF.ChartSourceFieldNames := ADescman.GetChartFieldNames;
      {brovak}
      // ++ Cadmensky
      SPF.OverlaySourceFieldNames := ADescman.GetOverlayFieldNames;
      SPF.InfoSourceFieldNames := ADescman.GetInfoFieldNames;
      // -- Cadmensky

      SPF._ReSize;

      if (iTop < 0) and (iLeft < 0) and (iWidth < 0) and (iHeight < 0) then
         SPF.Position := poDesktopCenter
      else
      begin
         if iTop > -1 then SPF.Top := iTop;
         if iLeft > -1 then SPF.Left := iLeft;
         if iWidth > -1 then SPF.Width := iWidth;
         if iHeight > -1 then SPF.Height := iHeight;
      end;

    (*  case iStartTabNumber of
         1: SPF.PageControl1.ActivePage := SPF.StructureTS;
         2: SPF.PageControl1.ActivePage := SPF.AnnotationsTS;
      else
      *)
      SPF.PageControl1.ActivePage := SPF.StructureTS;
     // end;
      try      //is talbe template for new tables?
      aQuery.Close;
      aQuery.Text:='SELECT * FROM [USYS_WGIDB_DT] WHERE [isTemplate] LIKE "' + sAttTableName + '"';
      aQuery.Open;
      SPF.IsTemplateCheckBox.Checked:=aQuery.RecordCount>0;
      except end;
      SPF.OKButton.Enabled:= showtablepropertiesdlg;
      RESULT := SPF.ShowModal = mrOK;

      iTop := SPF.Top;
      iLeft := SPF.Left;
      iWidth := SPF.Width;
      iHeight := SPF.Height;

      if RESULT then
      begin
         SetScreenCursor;
         try
// ++ Cadmensky
            if SPF.bNeedToFillCalculatedFields then
               SELF.FillCalculatedFields (ALayer);
// -- Cadmensky
            // Set fields visibility
            for iI := 1 to SPF.SG.RowCount - 1 do
            begin
               sFieldName := SPF.SG.Cells[SPF.FieldNameCol, iI];
               pLD.AFieldsVisibilityMan.SetFieldVisibleState (sFieldName, Integer (SPF.SG.Objects[SPF.FieldVisibleCol, iI]) = 1);
            end; // for iI end

            // Geotext field
            ADescMan.SetGeoTextFieldName (SPF.GeoTextFieldName);
            // Monitored ID field
            ADescMan.SetMonitoredFieldName (SPF.MonitoredFieldName);
            // Get Annotations source fields
            ADescMan.SetAnnotationFieldNames (SPF.AnnotationsSourceFieldNames);
            // Get Charts source fields
            ADescMan.SetChartFierldNames (SPF.ChartSourceFieldNames);

            // ++ Cadmensky
            ADescMan.SetOverlayFieldNames (SPF.OverlaySourceFieldNames);
            ADescMan.SetInfoFieldNames (SPF.InfoSourceFieldNames);
            // -- Cadmensky

            // Save them
            ADescMan.SetUserChoice;
            try
            aQuery.ExecSQL('UPDATE [USYS_WGIDB_DT] SET isTemplate = " "');
            if SPF.IsTemplateCheckBox.Checked then
                begin
                aQuery.ExecSQL('UPDATE [USYS_WGIDB_DT] SET isTemplate = "' + sAttTableName + '" WHERE LI = ' + inttostr(GetLayerIndexFunc(ALayer)));
                end;
            except end;
            if SPF.ApplyToAllCB.Checked then
               SELF.RefreshAllDataWindows;
         finally
            RestoreScreenCursor;
         end;
      end;
 AQuery.Free;
   finally
      SPF.Free;
      if AStructMan <> nil then AStructMan.Free;
   end;
end;

function TIDB_LayerManager.ShowFieldVisibility (ALayer: Pointer; var iTop: Integer; var iLeft: Integer; var iWidth: Integer; var iHeight: Integer): Boolean;
var
   iP, iI: Integer;
   pLD: PLayerDef;
   SPF: TIDB_StructureAndPropertiesForm;
   AStructMan: TIDB_StructMan;
   AQuery: TDQuery;
//   ATable: TDTable;
   sFieldName, sAttTableName: AnsiString;
   ACommonFieldsList, AVisibleFieldsList: TStringList;
   iLayerIndex: integer;
begin
   RESULT := FALSE;
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then EXIT; // There isn't such layer.
   pLD := listLayerDefs[iP];

// -- Cadmensky
   if not AnIDBMainMan.UseNewATTNames (AProject) then
      sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
   else
      sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

   AStructMan := TIDB_StructMan.Create (AProject, SELF.ADBAccessObject);

   AQuery := TDQuery.Create (nil);
   AQuery.Master := SELF.ADBAccessObject.DMaster;

   {if pLD.AnExtTablesMan.IAmInUse then
   begin
      if pLD.AnExtTablesMan.JoiningSQL = '' then
         pLD.AnExtTablesMan.SetParamsToExtTablePool;
      AQuery.SQL.Text := pLD.AnExtTablesMan.JoiningSQL;
   end
   else  }
      AQuery.SQL.Text := 'SELECT * FROM [' + sAttTableName + ']';

   try
      AQuery.Open;
      AStructMan.DetectStructure (AQuery, sAttTableName);
   except
      AQuery.Free;
      exit;
   end;
   AQuery.Free;
{   ATable := TDTable.Create (nil);
   try
      ATable.Master := SELF.ADBAccessObject.DMaster;
      if not AnIDBMainMan.UseNewATTNames (AProject) then
         ATable.TableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
      else
         ATable.TableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

      AStructMan := TIDB_StructMan.Create (AProject, SELF.ADBAccessObject);
      AStructMan.DetectStructure (ATable);
   finally
      ATable.Free;
   end;}
// -- Cadmensky

   iLayerIndex := GetLayerIndexFunc (ALayer);

   SPF := TIDB_StructureAndPropertiesForm.Create (Application, SELF.AProject, iLayerIndex, AStructMan, TRUE);
   try
      SPF.Caption := Format (ALangManager.GetValue (107), [GetLayerNameFunc (ALayer)]);
      SPF.Left := iLeft;
      SPF.Top := iTop;
      SPF.Height := iHeight;

      ACommonFieldsList := TStringList.Create;
      AVisibleFieldsList := TStringList.Create;

// ++ Cadmensky
      ACommonFieldsList.Add ('PROGISID');
      ACommonFieldsList.Add ('GUID');
      ACommonFieldsList.Add (cs_CalcFieldAreaName);
      ACommonFieldsList.Add (cs_CalcFieldXName);
      ACommonFieldsList.Add (cs_CalcFieldYName);
      ACommonFieldsList.Add (cs_CalcFieldSizeName);
      ACommonFieldsList.Add (cs_CalcFieldTextName);
      ACommonFieldsList.Add (cs_CalcFieldVerticesCount);
      ACommonFieldsList.Add (cs_CalcFieldSymbolName);
      ACommonFieldsList.Add (cs_CalcFieldRotationName);
      ACommonFieldsList.Add (cs_CalcFieldLinkIDName);
// -- Cadmensky
//      ACommonFieldsList.Text := pLD.AFieldsVisibilityMan.GetAllAttributeFieldNames (TRUE);
//      if ACommonFieldsList.Count < 1 then
      for iI := 0 to AStructMan.GetFieldCount - 1 do
      begin
         sFieldName := AStructMan.GetFieldName (iI);
         if not (AnsiUpperCase (sFieldName) = 'PROGISID') and
            not (AnsiUpperCase (sFieldName) = 'GUID') and
            not FieldIsCalculatedField (sFieldName) then
{               ACommonFieldsList.Insert (0, sFieldName)
            else}
            ACommonFieldsList.Add (AStructMan.GetFieldName (iI));
      end;

      AVisibleFieldsList.Text := pLD.ADescMan.GetInfoFieldNames;
      //      AVisibleFieldsList.Text := pLD.AFieldsVisibilityMan.GetVisibleAttributeFieldNames (FALSE);

      SPF.SG.RowCount := ACommonFieldsList.Count + 1;
      for iI := 0 to ACommonFieldsList.Count - 1 do
      begin
         sFieldName := ACommonFieldsList[iI];
         SPF.SG.Cells[SPF.FieldNameCol, iI + 1] := sFieldName;
         if FieldIsCalculatedField (sFieldName) then
            SPF.SG.Objects[SPF.FieldNameCol, iI + 1] := Pointer (0)
         else
            SPF.SG.Objects[SPF.FieldNameCol, iI + 1] := Pointer (1);

         if AVisibleFieldsList.IndexOf (sFieldName) > -1 then
            SPF.SG.Objects[SPF.FieldInfoVisibleCol, iI + 1] := Pointer (1)
         else
            SPF.SG.Objects[SPF.FieldInfoVisibleCol, iI + 1] := Pointer (0);
      end;

      ACommonFieldsList.Clear;
      ACommonFieldsList.Free;
      AVisibleFieldsList.Clear;
      AVisibleFieldsList.Free;

      SPF.SG.Objects[SPF.FieldNameCol, SPF.SG.RowCount - 1] := Pointer (1);

      SPF._ReSize;
      SPF.Width := iWidth;

      RESULT := SPF.ShowModal = mrOK;

      iTop := SPF.Top;
      iLeft := SPF.Left;
      iWidth := SPF.Width;
      iHeight := SPF.Height;

      if RESULT then
      begin
         SetScreenCursor;
         try
            pLD.ADescMan.SetInfoFieldNames (SPF.InfoSourceFieldNames);
            // Set fields visibility
{            for iI := 1 to SPF.SG.RowCount - 1 do
            begin
               sFieldName := SPF.SG.Cells[SPF.FieldNameCol, iI];
               pLD.AFieldsVisibilityMan.SetFieldVisibleState (sFieldName, Integer (SPF.SG.Objects[SPF.FieldVisibleCol, iI]) = 1);
            end; // for iI end}
         finally
            RestoreScreenCursor;
         end;
      end;
   finally
      SPF.Free;
      if AStructMan <> nil then AStructMan.Free;
   end;
end;

procedure TIDB_LayerManager.SaveResults (ALayer: Pointer; ADataSet: TDADODataSet; sSQL: AnsiString; AGauge: TGauge = nil);
var
   iP: Integer;
   ADataMan: TIDB_DBDataManager;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then EXIT; // There isnt' such layer.

   ADataMan := TIDB_DBDataManager.Create (ADBAccessObject, SELF.AProject, ALayer, ADataSet, sSQL, AGauge);
   try
      ADataMan.SaveResultsFromDBTableToFile;
   finally
      ADataMan.Free;
   end;
end;

function TIDB_LayerManager.LoadData (ALayer: Pointer; ADataSet: TDADODataSet; AGauge: TGauge = nil): Boolean;
var
   ADataMan: TIDB_DBDataManager;
   iP: Integer;
   // ++ Cadmensky
   sAttTableName: AnsiString;
   // -- Cadmensky
//   bLoadData: boolean;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then EXIT; // There isn't such layer.

   RESULT := false;
   ADataMan := TIDB_DBDataManager.Create (ADBAccessObject, SELF.AProject, ALayer, nil, '', AGauge);
//   try
   RESULT := ADataMan.LoadData;
//   finally
   ADataMan.Free;
//   end;

   // ++ Cadmensky
   if RESULT then
   begin
      if not AnIDBMainMan.UseNewATTNames (AProject) then
         sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
      else
         sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);
      CheckRequiredFields (SELF, ALayer, sAttTableName);
   end;
   // -- Cadmensky
end;

procedure TIDB_LayerManager.StoreParametersOfViewForm (ALayer: Pointer; AForm: TForm);
var
   iP: Integer;
   pLD: PLayerDef;
   AnAttFormPositionManager: TIDB_FormPositionManager;
begin
   iP := FindLayer (ALayer);
   if iP < 0 then
      Exit; // There isn't such layer.
   pLD := listLayerDefs[iP];
// ++ Cadmensky Version 2.3.7
   AnAttFormPositionManager := AnIDBMainMan.GetFormPositionManager (SELF.AProject);
   if AnAttFormPositionManager <> nil then
      AnAttFormPositionManager.StoreFormState (AForm, TIDB_ViewForm (AForm).AutoAddToSelectedSB.Down, TIDB_ViewForm(AForm).AutoZoom.checked );
//   pLD.AnAttFormStateMan.StoreFormState (AForm, TIDB_ViewForm (AForm).AutoAddToSelectedSB.Down);
// -- Cadmensky Version 2.3.7
   pLD.AFieldsVisibilityMan.DetectFieldsStates (TIDB_ViewForm (AForm).ViewDBGrid);
end;

procedure TIDB_LayerManager.RestoreFieldsStates (ALayer: Pointer; ADBGrid: TIDBPCDBGrid; bRestoreFieldsVisibleState: Boolean = TRUE; bRestoreFieldVisibleIndex: Boolean = TRUE);
var
   iP: Integer;
   pLD: PLayerDef;
begin
   iP := FindLayer (ALayer);
   if iP < 0 then EXIT; // There isn't such layer.
   pLD := listLayerDefs[iP];
   pLD.AFieldsVisibilityMan.RestoreFieldsStates (ADBGrid, bRestoreFieldsVisibleState, bRestoreFieldVisibleIndex);
end;

procedure TIDB_LayerManager.DetectFieldsStates (ALayer: Pointer; ADBGrid: TIDBPCDBGrid);
var
   iP: Integer;
   pLD: PLayerDef;
begin
   iP := FindLayer (ALayer);
   if iP < 0 then EXIT; // There isn't such layer.
   pLD := listLayerDefs[iP];
   pLD.AFieldsVisibilityMan.DetectFieldsStates (ADBGrid);
end;

function TIDB_LayerManager.GetReadyToReceiveIDsOfObjectToBeDeleted_FromDB (ALayer: Pointer): Boolean;
var
   iP: Integer;
   pLD: PLayerDef;
begin
   RESULT := FALSE;
   iP := FindLayer (ALayer);
   if iP < 0 then
      iP := SELF.AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky

   pLD := listLayerDefs[iP];
   pLD.listIDOfObjectsToBeDeletedFromDB.Clear;
end;

procedure TIDB_LayerManager.ReceiveIDOfObjectToBeDeleted_FromDB (ALayer: Pointer; iObjectID: Integer);
var
   iP: Integer;
   pLD: PLayerDef;
begin
   iP := FindLayer (ALayer);
   if iP < 0 then iP := AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];
   pLD.listIDOfObjectsToBeDeletedFromDB.Add (Pointer (iObjectID));
end;

procedure TIDB_LayerManager.DeleteObjectsInProject_FromDB (ALayer: Pointer);
{var
   iP: Integer;
   pLD: PLayerDef;}
begin
{   iP := FindLayer (ALayer);
   if iP < 0 then iP := AddLayer (ALayer);
   pLD := listLayerDefs[iP];
   if pLD.listIDOfObjectsToBeDeletedFromDB.Count < 1 then EXIT; // Why we need to call deleted if there}
   // isn't any object to be deleted?
// Send a message to the window which is the parent of the project to that the layer belongs.
   SendMessage (SELF.AProjectFormHandle, WM_IDB_Message, IDB_DeleteObjectsAction, Integer (ALayer));
end;

function TIDB_LayerManager.GetItemIDToBeDeleted_FromIDB (ALayer: Pointer): Integer;
var
   pLD: PLayerDef;
   iP: Integer;
begin
   RESULT := -1;
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then iP := AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];
   if pLD.listIDOfObjectsToBeDeletedFromDB.Count < 1 then EXIT;
   RESULT := Integer (pLD.listIDOfObjectsToBeDeletedFromDB[0]);
   pLD.listIDOfObjectsToBeDeletedFromDB.Delete (0);
end;

function TIDB_LayerManager.GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG: Boolean;
var
   iI: Integer;
   pLD: PLayerDef;
begin
   for iI := 0 to listLayerDefs.Count - 1 do
   begin
      pLD := listLayerDefs[iI];
      pLD.listIDOfObjectsToBeDeletedFromWG.Clear;
   end;
   RESULT := TRUE;
end;

procedure TIDB_LayerManager.ReceiveIDOfObjectToBeDeleted_FromWG (ALayer: Pointer; iObjectID: Integer);
var
   pLD: PLayerDef;
   iP: Integer;
// ++ Cadmensky Version 2.3.4
   sAttTableName: AnsiString;
// -- Cadmensky Version 2.3.4
begin
// ++ Cadmensky Version 2.3.4
   if not AnIDBMainMan.UseNewATTNames (SELF.AProject) then
      sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
   else
      sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (SELF.AProject, ALayer);
// -- Cadmensky Version 2.3.4

   iP := SELF.FindLayer (ALayer);
   if iP < 0 then
// ++ Cadmensky Version 2.3.4
      if ADBAccessObject.ADBItemsMan.TableExists (sAttTableName) then
// -- Cadmensky Version 2.3.4
         iP := AddLayer (ALayer);

// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];
   pLD.listIDOfObjectsToBeDeletedFromWG.Add (Pointer (iObjectID));
end;

procedure TIDB_LayerManager.DeleteObjects_FromWG;
var
   iI, iP, iJ,
      iQuantityToBeDone,
      iQuantityOfDone: Integer;

   pLD: PLayerDef;
   sAttTableName,
      sIDs: AnsiString;

   AnExecuteQuery: TDQuery;
begin
   AnExecuteQuery := TDQuery.Create (nil);
   AnExecuteQuery.Master := ADBAccessObject.DMaster;
   try
      // Count how many objects are to be deleted.
      iQuantityOfDone := 0;
      iQuantityToBeDone := 0;
      for iI := 0 to listLayerDefs.Count - 1 do
      begin
         pLD := listLayerDefs[iI];
         if pLD.listIDOfObjectsToBeDeletedFromWG.Count > 0 then
            Inc (iQuantityToBeDone, pLD.listIDOfObjectsToBeDeletedFromWG.Count);
      end;
      // if there isn't data to be deleted, go out.
      if iQuantityToBeDone = 0 then
         // Hide the progress bar at the left-bootom corner of WinGIS window.
         // I can do it if I will show 100% of done percent.
         ShowDonePercentProc (100)
      else
      begin
         // Show zero percent of done.
         ShowDonePercentProc (0);
         // Go by all layers and delete all objects.
         for iI := 0 to listLayerDefs.Count - 1 do
         begin
            pLD := listLayerDefs[iI];
            if pLD.listIDOfObjectsToBeDeletedFromWG.Count < 1 then CONTINUE;

            if not AnIDBMainMan.UseNewATTNames (AProject) then
               sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (pLD.ALayer))
            else
               sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, pLD.ALayer);
// ++ Commented by Cadmensky Version 2.3.4
{            // if the attribute table doesn't exist, increase percent of done, clear the list of IDs and go to the next layer.
            if not ADBAccessObject.ADbItemsMan.TableExists (sAttTableName) then
            begin
               iQuantityOfDone := iQuantityOfDone + pLD.listIDOfObjectsToBeDeletedFromWG.Count;
               pLD.listIDOfObjectsToBeDeletedFromWG.Clear;

               ShowDonePercentProc (Round (100 * iQuantityOfDone / iQuantityToBeDone));
               CONTINUE;
            end;}
// -- Commented by Cadmensky Version 2.3.4
            iP := 0;
            try
               while iP <= pLD.listIDOfObjectsToBeDeletedFromWG.Count - 1 do
               begin
                  sIDs := '(';
                  for iJ := 0 to ci_PackLength do
                  begin
                     sIDs := sIDs + 'PROGISID = ' + IntToStr (Integer (pLD.listIDOfObjectsToBeDeletedFromWG[iP + iJ])) + ' OR ';
                     if iP + iJ = pLD.listIDOfObjectsToBeDeletedFromWG.Count - 1 then BREAK;
                  end; // for iJ end
                  Inc (iP, ci_PackLength + 1);
                  sIDs := Copy (sIDs, 1, Length (sIDs) - 4) + ')';
                  try // Mark objects as deleted.
                     AnExecuteQuery.ExecSQL ('UPDATE [' + sAttTableName + '] ' +
                        'SET [' + cs_StateFieldName + '] = ' + cs_ObjectIsDeleted + ' ' +
                        'WHERE ' + sIDs);
                  except
                  end;
                  Inc (iQuantityOfDone, ci_PackLength + 1);
                  // Show done percent in the main WinGIS window.
                  ShowDonePercentProc (Round (100 * iQuantityOfDone / iQuantityToBeDone));
               end; // while end
            finally
               pLD.listIDOfObjectsToBeDeletedFromWG.Clear;
            end;
         end; // for iI end
      end;
   finally
      AnExecuteQuery.Free;
   end;
   SELF.RefreshAllDataWindows;
end;

function TIDB_LayerManager.GetReadyToReceiveIDsOfObjectsForMonitoring: Boolean;
var
   iI: Integer;
   pLD: PLayerDef;
begin
   for iI := 0 to listLayerDefs.Count - 1 do
   begin
      pLD := listLayerDefs[iI];
      pLD.listIDOfObjectsToBeMonitored.Clear;
   end;
   RESULT := TRUE;
end;

procedure TIDB_LayerManager.ReceiveIDOfObjectForMonitoring (ALayer: Pointer; iObjectID: Integer);
var
   iP: Integer;
   pLD: PLayerDef;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then iP := AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];
   pLD.listIDOfObjectsToBeMonitored.Add (Pointer (iObjectID));
end;

procedure TIDB_LayerManager.FillMonitoringData (AForm: TForm);
var
   iI, iJ, iItemsCount: Integer;
   pLD: PLayerDef;
   sLayerName: AnsiString;
   iLayerIndex: Integer;
begin
   if (AForm is TIDB_MonitorForm) then
      TIDB_MonitorForm (AForm).ClearItemsForMonitoring;
   if (AForm is TIDB_Monitor1Form) then
      TIDB_Monitor1Form (AForm).ClearItemsForMonitoring;

   iItemsCount := 0;
   for iI := 0 to listLayerDefs.Count - 1 do
   begin                          
      pLD := listLayerDefs[iI];
      if pLD.listIDOfObjectsToBeMonitored.Count > 0 then
         Inc (iItemsCount, pLD.listIDOfObjectsToBeMonitored.Count);
   end; // for iI end
   if iItemsCount = 0 then EXIT;

   // Monitoring. Step 1.
   if (AForm is TIDB_MonitorForm) then
   begin
      TIDB_MonitorForm (AForm).Gauge1.MaxValue := iItemsCount;
      TIDB_MonitorForm (AForm).ShowStatus (ALangManager.GetValue (182));
   end;
   try
      for iI := 0 to listLayerDefs.Count - 1 do
      begin
         pLD := listLayerDefs[iI];
         if pLD.listIDOfObjectsToBeMonitored.Count < 1 then
            Continue;
         sLayerName := GetLayerNameFunc (pLD.ALayer);
         iLayerIndex := GetLayerIndexFunc (pLD.ALayer);
         for iJ := 0 to pLD.listIDOfObjectsToBeMonitored.Count - 1 do
         begin
            if (AForm is TIDB_MonitorForm) then
            begin
               TIDB_MonitorForm (AForm).AddLayerItem (iLayerIndex,
                  Integer (pLD.listIDOfObjectsToBeMonitored[iJ]),
                  SELF.ADBAccessObject);
               TIDB_MonitorForm (AForm).Gauge1.Progress := TIDB_MonitorForm (AForm).Gauge1.Progress + 1;
            end;
            if (AForm is TIDB_Monitor1Form) then
               TIDB_Monitor1Form (AForm).AddLayerItem (iLayerIndex,
                  Integer (pLD.listIDOfObjectsToBeMonitored[iJ]),
                  SELF.ADBAccessObject);
            Application.ProcessMessages;
         end; // for iJ end
      end; // for iI end
   finally
      if (AForm is TIDB_MonitorForm) then
         TIDB_MonitorForm (AForm).HideStatus;
   end;
end;

function TIDB_LayerManager.GetSelectedObjectsIDs (ALayer: Pointer): AnsiString;
var
   pLD: PLayerDef;
   iI: integer;
   AStringList: TStringList;
   iP: Integer;
begin
   iP := FindLayer (ALayer);
   if iP < 0 then
      iP := SELF.AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];

   AStringList := TStringList.Create;
   for iI := 0 to pLD.listIDOfObjectsToBeMonitored.Count - 1 do
   begin
      AStringList.Add (IntToStr (Integer (pLD.listIDOfObjectsToBeMonitored[iI])));
   end;
   RESULT := AStringList.Text;
   AStringList.Free;
end;

procedure TIDB_LayerManager.GetInfoObjectParams (ItemNumber: integer; var iLayerIndex: Integer; var iItemID: Integer);
var
   iI: Integer;
   pLD: PLayerDef;
begin
   iLayerIndex := -1;
   iItemID := -1;
   for iI := 0 to listLayerDefs.Count - 1 do
   begin
      pLD := listLayerDefs[iI];
      if pLD.listIDOfObjectsToBeMonitored.Count > ItemNumber then
      begin
         iLayerIndex := GetLayerIndexFunc (pLD.ALayer);
         iItemID := Integer (pLD.listIDOfObjectsToBeMonitored[ItemNumber]);
      end; // for iJ end
   end; // for iI end
end;

function TIDB_LayerManager.GetReadyToReceiveIDsOfObjectsToBeRestored: Boolean;
var
   iI: Integer;
   pLD: PLayerDef;
begin
   for iI := 0 to listLayerDefs.Count - 1 do
   begin
      pLD := listLayerDefs[iI];
      pLD.listIDOfObjectsToBeRestored.Clear;
   end;
   RESULT := TRUE;
end;

procedure TIDB_LayerManager.ReceiveIDOfObjectToBeRestored (ALayer: Pointer; iObjectID: Integer);
var
   iP: Integer;
   pLD: PLayerDef;
// ++ Cadmensky Version 2.3.4
   sAttTableName: AnsiString;
// -- Cadmensky Version 2.3.4
begin
// ++ Cadmensky Version 2.3.4
   if not AnIDBMainMan.UseNewATTNames (SELF.AProject) then
      sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
   else
      sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (SELF.AProject, ALayer);
// -- Cadmensky Version 2.3.4
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then
// ++ Cadmensky Version 2.3.4
      if ADBAccessObject.ADBItemsMan.TableExists (sAttTableName) then
// -- Cadmensky Version 2.3.4
         iP := SELF.AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];
   pLD.listIDOfObjectsToBeRestored.Add (Pointer (iObjectID));
end;

procedure TIDB_LayerManager.ExecuteRestoring;
var
   AnExecQuery: TDQuery;
   iI, iJ, iP: Integer;
   iAllObjectsCount, iRestoredObjectsCount: Integer;
   pLD: PLayerDef;
   sIDs: AnsiString;
   sAttTableName: AnsiString;
begin
   AnExecQuery := TDQuery.Create (nil);
   AnExecQuery.Master := SELF.ADBAccessObject.DMaster;
   try
      iAllObjectsCount := 0;
      iRestoredObjectsCount := 0;
      for iI := 0 to listLayerDefs.Count - 1 do
      begin
         pLD := listLayerDefs[iI];
         if pLD.listIDOfObjectsToBeRestored.Count > 0 then
            Inc (iAllObjectsCount, pLD.listIDOfObjectsToBeRestored.Count);
      end; // for iI end

      if iAllObjectsCount = 0 then EXIT;

      ShowDonePercentProc (0);
      Application.ProcessMessages;
      Application.HandleMessage;
      // I'll do it by 90 items pack.
      for iI := 0 to listLayerDefs.Count - 1 do
      begin
         pLD := listLayerDefs[iI];
         iP := 0;
         while iP <= pLD.listIDOfObjectsToBeRestored.Count - 1 do
         begin
            sIDs := '(';
            for iJ := 0 to ci_PackLength do
            begin
               sIDs := sIDs + 'PROGISID = ' + IntToStr (Integer (pLD.listIDOfObjectsToBeRestored[iP + iJ])) + ' OR ';
               if iP + iJ = pLD.listIDOfObjectsToBeRestored.Count - 1 then BREAK;
            end; // for iJ end
            Inc (iP, ci_PackLength + 1);
            sIDs := Copy (sIDs, 1, Length (sIDs) - 4) + ')';

            if not AnIDBMainMan.UseNewATTNames (AProject) then
               sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (pLD.ALayer))
            else
               sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, pLD.ALayer);

            try
               AnExecQuery.ExecSQL ('UPDATE [' + sAttTableName + '] ' +
                  'SET [' + cs_StateFieldName + '] = ' + cs_ObjectIsNormal + ' ' +
                  'WHERE ' + sIDs)
            except
            end;
            Inc (iRestoredObjectsCount, ci_PackLength + 1);
            ShowDonePercentProc (Round (100 * iRestoredObjectsCount / iAllObjectsCount));
            Application.ProcessMessages;
         end; // while iP end
         pLD.listIDOfObjectsToBeRestored.Clear;
      end; // for iI end
   finally
      AnExecQuery.Free;
      Application.ProcessMessages;
   end;
end;

{ Insert new record in layer attribute table. }

procedure TIDB_LayerManager.InsertItem (ALayer: Pointer; iNewItemIndex: Integer; bAnExternalDBIsWorking: Boolean);
var
   iI: Integer;
   sLayerIndex, sNewItemIndex,
   sAttTableName,
   sSQL: AnsiString;
   AQuery: TDQuery;
   AForm: TForm;
   iP: integer;
   AGUID: TGUID;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then
      iP := SELF.AddLayer (ALayer);
   if iP < 0 then
      exit;

   sLayerIndex := IntToStr (GetLayerIndexFunc (ALayer));
   sNewItemIndex := IntToStr (iNewItemIndex);
   if not AnIDBMainMan.UseNewATTNames (AProject) then
      sAttTableName := cs_AttributeTableNamePrefix + sLayerIndex
   else
      sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

   AQuery := TDQuery.Create (nil);
   AQuery.Master := ADBAccessObject.DMaster;

   AGUID:=ItemToGuidFunc(AProject,ALayer,iNewItemIndex);

   sSQL := 'INSERT INTO [' + sAttTableName + '] ([PROGISID],[GUID],[' + cs_StateFieldName + ']) ' +
      'VALUES (' + sNewItemIndex + ',' + '''' + GuidToString(AGUID) + '''' + ','+ cs_ObjectIsJustAdded + ')';

   try // Just in case...
      AQuery.SQL.Text:='SELECT PROGISID FROM [' + sAttTableName + '] WHERE [PROGISID] = ' + sNewItemIndex;
      AQuery.Open;
      if AQuery.RecordCount>0 then
         AQuery.ExecSQL ('UPDATE [' + sAttTableName + '] ' +
            'SET [' + cs_StateFieldName + '] = ' + cs_ObjectIsNormal + ' ' +
            'WHERE [PROGISID] = ' + sNewItemIndex)
         else AQuery.ExecSQL (sSQL);
      FillCalculatedField (ALayer, iNewItemIndex);
   except
      try
         AQuery.ExecSQL ('UPDATE [' + sAttTableName + '] ' +
            'SET [' + cs_StateFieldName + '] = ' + cs_ObjectIsNormal + ' ' +
            'WHERE [PROGISID] = ' + sNewItemIndex);

         FillCalculatedField (ALayer, iNewItemIndex);
      except
      end;
   end;

   // ++ Cadmensky
   if AQuery.Active then
      AQuery.Close;
   AQuery.Free;
   // -- Cadmensky

   // Move on new record.
   SELF.RefreshAllDataWindows;
   for iI := 0 to Screen.FormCount - 1 do
   begin
      AForm := Screen.Forms[iI];
      if (AForm is TIDB_ViewForm)
         and (TIDB_ViewForm (AForm).AProject = AProject)
         and (TIDB_ViewForm (AForm).ALayer = ALayer) then
         TIDB_ViewForm (AForm).ViewDataSource.DataSet.Locate ('PROGISID', iNewItemIndex, []);
   end; // for iI end
end;

// ++ Cadmensky

function TIDB_LayerManager.GetReadyToInsertRecords (ALayer: Pointer; ARecordTemplate: TList): Integer;
var
   pLD: PLayerDef;
   iP: Integer;
begin
   iP := FindLayer (ALayer);
   if iP < 0 then
      iP := SELF.AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];
   pLD.listRecordTemplate := ARecordTemplate;
end;
// -- Cadmensky

function TIDB_LayerManager.InsertRecord (ALayer: Pointer; iItemIndex: Integer; bNeedToUpdateRecordAnyway: Boolean = false): Boolean;
var
   AQuery: TDQuery;
   sTableName, sItemIndex, sSQL: AnsiString;
// ++ Cadmensky
   pLD: PLayerDef;
   iP, iI: Integer;
   sFieldNames, sValues: AnsiString;
   pRecordTemplateItem: ^TrecRecordTemplateItem;
// -- Cadmensky
// ++ Cadmensky Version 2.3.4
   bObjectIsDeleted: Boolean;
// -- Cadmensky Version 2.3.4
   AGUID: TGUID;
begin
// ++ Cadmensky
   iP := FindLayer (ALayer);
   if iP < 0 then
      iP := SELF.AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];
// -- Cadmensky

   RESULT := FALSE;
   if not AnIDBMainMan.UseNewATTNames (AProject) then
      sTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
   else
      sTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);
   sItemIndex := IntToStr (iItemIndex);
   AQuery := TDQuery.Create (nil);
   AQuery.Master := SELF.ADBAccessObject.DMaster;
   AQuery.SQL.Text := 'SELECT [PROGISID],[' + cs_StateFieldName + '] ' +
      'FROM [' + sTableName + '] ' +
      'WHERE PROGISID = ' + sItemIndex;
   try
      AQuery.Open;
   except
      AQuery.Free;
      EXIT;
   end;
   if AQuery.RecordCount > 0 then
   begin
// ++ Cadmensky Version 2.3.4
      bObjectIsDeleted := AQuery.FieldByName (cs_StateFieldName).AsString = cs_ObjectIsDeleted;
      if bObjectIsDeleted or
         bNeedToUpdateRecordAnyway then
// -- Cadmensky Version 2.3.4
// ++ Cadmensky
      begin
// ++ Cadmensky Version 2.3.4
         if bObjectIsDeleted then
// -- Cadmensky Version 2.3.4
            sValues := cs_StateFieldName + ' = ' + cs_ObjectIsJustAdded + ', ';
         for iI := 0 to pLD.listRecordTemplate.Count - 1 do
         begin
            pRecordTemplateItem := pLD.listRecordTemplate[iI];
            if Trim (pRecordTemplateItem.sValue) <> '' then
            begin
               sValues := sValues + pRecordTemplateItem.sFieldName + ' = ';
               if pRecordTemplateItem.iFieldType = 1 then
                  sValues := sValues + '''' + pRecordTemplateItem.sValue + ''''
               else
                  sValues := sValues + pRecordTemplateItem.sValue;
               sValues := sValues + ', ';
            end;
         end; // For iI end
         sValues := Copy (sValues, 1, Length (sValues) - 2);
         sSQL := 'UPDATE [' + sTableName + '] ' +
            'SET ' + sValues + ' ' +
            'WHERE PROGISID = ' + sItemIndex;
// ++ Cadmensky Version 2.3.4
         if not bObjectIsDeleted then
         begin
            try
               AQuery.ExecSQL (sSQL);
            except
            end;
            AQuery.Free;
         end;
// -- Cadmensky Version 2.3.4

// -- Cadmensky
{      sSQL := 'UPDATE TABLE [' + sTableName + '] ' +
         'SET [' + cs_StateFieldName + '] = ' + cs_ObjectIsJustAdded + ' ' +
         'WHERE PROGISID = ' + sItemIndex }
      end
      else
      begin
         AGUID:=ItemToGuidFunc(AProject,ALayer,iItemIndex);

         sSQL := 'UPDATE [' + sTableName + '] ' +
         'SET [GUID]=' + '''' + GuidToString(AGUID) + '''' + ' ' +
         'WHERE [PROGISID]=' + sItemIndex;

         try
            AQuery.ExecSQL(sSQL);
         finally
            AQuery.Close;
            AQuery.Free;
         end;
         exit;
      end;
   end
   else
   begin
// ++ Cadmensky
      sFieldNames := ' ProgisID, ' + cs_StateFieldName;
      sValues := sItemIndex + ', ' + cs_ObjectIsJustAdded;

      for iI := 0 to pLD.listRecordTemplate.Count - 1 do
      begin
         pRecordTemplateItem := pLD.listRecordTemplate[iI];
         if Trim (pRecordTemplateItem.sValue) <> '' then
         begin
            sFieldNames := sFieldNames + ', ' + pRecordTemplateItem.sFieldName;
            if pRecordTemplateItem.iFieldType = 1 then
               sValues := sValues + ', ''' + pRecordTemplateItem.sValue + ''''
            else
               sValues := sValues + ', ' + pRecordTemplateItem.sValue;
         end;
      end; // For iI end
      sSQL := 'INSERT INTO [' + sTableName + '] (' + sFieldNames + ') ' +
         'VALUES (' + sValues + ')';
// -- Cadmensky
   end;
{      sSQL := 'INSERT INTO [' + sTableName + '] (PROGISID, [' + cs_StateFieldName + ']) ' +
         'VALUES (' + sItemIndex + ', ' + cs_ObjectIsJustAdded + ')';}
   AQuery.Close;
   try
      AQuery.ExecSQL (sSQL);
      FillCalculatedField (ALayer, iItemIndex); // Cadmensky
      RESULT := TRUE;
   except
   end;
   AQuery.Free;
end;

// ++ Commented by Cadmensky Version 2.3.2
{function TIDB_LayerManager.GetQueryResultWindows (AProject: Pointer): TStringList;
var
   iI: Integer;
   AForm: TForm;
begin
   RESULT := TStringList.Create;
   for iI := 0 to Screen.FormCount - 1 do
   begin
      AForm := Screen.Forms[iI];
      if (AForm is TIDB_ViewForm) and
         (TIDB_ViewForm (AForm).AProject = AProject) and
         (TIDB_ViewForm (AForm).IAmQueryResultWindow) then
      begin
         RESULT.Add (AForm.Caption);
         RESULT.Objects[RESULT.Count - 1] := AForm;
      end;
   end;
end;}
// -- Commented by Cadmensky Version 2.3.2

{ This procedure makes dispalying a result of a query either in existing window for this purpose
  or creates the same window. }

function TIDB_LayerManager.ShowQueryResultWindow (AProject, ALayer: Pointer; var AWindow: TForm; sCaption, sSQLText: AnsiString): Boolean;
var
   AForm: TIDB_ViewForm;
   sTmpSQL: AnsiString;
//   i: integer;
begin
   RESULT := FALSE;
   // Next line is a dummy line. Below you can see 'AForm.DataQuery.Open;'. I need to have a negative result
   // if this operator causes an exception (if it contatins non-correct SQL for example). But the compiler
   // don't give me to have a preset negative RESULT, I think due to its very clever behavior.
   if RESULT then EXIT;

//   AForm.DataQuery.Master := ADBAccessObject.DMaster;
   AForm := TIDB_ViewForm.Create (AWinGISMainForm, SELF, SELF.AProject, ALayer, nil);
   AForm.Constraints.MinWidth := 150;
//   AForm.DataQuery.Master := ADBAccessObject.DMaster;
   if AWindow = nil then
      // Result of query ''%s''
      AForm.Caption := Format (ALangManager.GetValue (219), [sCaption])
   else
   begin // And I use existing window here.
      AForm.Left := AWindow.Left+5;
      AForm.Top := AWindow.Top+5;
      AForm.Height := AWindow.Height;
      AForm.Width := AWindow.Width;
      AForm.Caption := AWindow.Caption;
      AWindow.Close;
   end;
   AWindow := AForm;
   // And now I shall make this window to execute query and to display the result of this query.
   AForm.ViewDataSource.DataSet := AForm.DataQuery;
   AForm.DataQuery.Master := ADBAccessObject.DMaster;
   AForm.DataQuery.SQL.Text := sSQLText;
   AForm.DataQuery.Open;

{   AForm.DataQuery.Next;
   AForm.ViewDBGrid.Perform (WM_VSCROLL, SB_LINEUP, 0);}

{BROVAK}
   with TIDB_ViewForm (AForm) do
   begin
      ViewDBGrid.SelectedRows.Clear;
// ++ Commented by Cadmensky Version 2.3.1
{      AConnection := CoConnection.Create;
      AConnection.Open (TDADODataSet (ViewDataSource.DataSet).Connection, '', '', adConnectUnspecified);
      if AConnection.State <> adStateOpen then
      begin;
         raise Exception.Create ('The SQL Power Broker initialaze failed !');
         Exit;
      end;}
// -- Commented by Cadmensky Version 2.3.1
      ARecordSet := CoRecordSet.Create;
// ++ Cadmensky
      if not AnIDBMainMan.UseNewATTNames (AProject) then
         sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
      else
         sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

      sTmpSQL := 'SELECT MAX([' + cs_InternalID_FieldName + ']) AS ROWBITS_SIZE FROM [' + sAttTableName + ']';
      ARecordSet := CoRecordSet.Create;
      ARecordSet.Open (sTmpSQL,
// ++ Cadmensky Version 2.3.1
//         AConnection,
         DataQuery.CentralData.ADO,
// -- Cadmensky Version 2.3.1
         adOpenKeyset,
         adOpenKeyset,
         adCmdText);
      ViewDBGrid.RowBits.Size := StrToInt (ARecordSet.Fields[0].Value);
      ViewDBGrid.RowBitsClear;
      ARecordSet.Close;
// -- Cadmensky
      ARecordSet.Open (sSQLText,
// ++ Cadmensky Version 2.3.1
//         AConnection,
         DataQuery.CentralData.ADO,
// -- Cadmensky Version 2.3.1
         adOpenKeyset,
        adLockOptimistic, // adLockReadOnly,
         adCmdText);
      if ARecordSet.State <> adStateOpen then
      begin
// ++ Commented by Cadmensky Version 2.3.1
//         AConnection.Close;
// -- Commented by Cadmensky Version 2.3.1
         raise Exception.Create ('The Power Broker recordset cannot be opened.');
      end;

//      ViewDBGrid.RowBits.Size := TDADODataSet (ViewDataSource.DataSet).RecordCount;
   end;
{BROVAK}
   //     TIDB_ViewForm(AForm).sAttTableName:=sAttTableName;
   //            TIDB_ViewForm(AForm).CPB_SortField:='PROGISID';
   //            TIDB_ViewForm(AForm).CPB_Order:=' ASC';
   //            TIDB_ViewForm(AForm).Create_Power_Broker;

// ++ Cadmensky Version 2.2.8
   AForm.Show;
// -- Cadmensky Version 2.2.8
   RESULT := true;
end;

// ++ Commented by Cadmensky Version 2.3.0
{function TIDB_LayerManager.GetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDrop: Boolean;
var
   pLD: PLayerDef;
   iI: Integer;
begin
   for iI := 0 to SELF.listLayerDefs.Count - 1 do
   begin
      pLD := listLayerDefs[iI];
      pLD.listIDOfObjectsToBeUsedForDragAndDrop.Clear;
   end; // for iI end
   RESULT := TRUE;
end;

procedure TIDB_LayerManager.ReceiveIDOfObjectToBeUsedForDragAndDrop (ALayer: Pointer; iObjectID: Integer);
var
   pLD: PLayerDef;
   iP: Integer;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then iP := AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];
   pLD.listIDOfObjectsToBeUsedForDragAndDrop.Add (Pointer (iObjectID));
end;}

{ This procedure copies all attribute data from the current record to record of destination item. }

{procedure TIDB_LayerManager.ExecuteDragAndDrop (ASourceGridOfDragging: Pointer);
var
   pLD: PLayerDef;
   ALayer: Pointer;
   iI, iJ, iP: Integer;
   ADestTable: TDTable;
   iDestItemID: Integer;
   sFN: AnsiString;
begin
   // Create temp TTable. This object will work with the same attribute table.
   if not TIDBPCDBGrid (ASourceGridOfDragging).DataSource.DataSet.Active then EXIT;

   ALayer := TIDB_ViewForm (TIDBPCDBGrid (ASourceGridOfDragging).Parent).ALayer;
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then EXIT;
   pLD := listLayerDefs[iP];

   ADestTable := TDTable.Create (nil);
   ADestTable.Master := SELF.ADBAccessObject.DMaster;
   if not AnIDBMainMan.UseNewATTNames (AProject) then
      ADestTable.TableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (pLD.ALayer))
   else
      ADestTable.TableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, pLD.ALayer);
   ADestTable.Open;

   try
      for iI := 0 to pLD.listIDOfObjectsToBeUsedForDragAndDrop.Count - 1 do
      begin
         iDestItemID := Integer (pLD.listIDOfObjectsToBeUsedForDragAndDrop[iI]);
         // if it is the same object, I'll continue. ProgisID field can has an empty value.
         try
            if iDestItemID = TIDBPCDBGrid (ASourceGridOfDragging).DataSource.DataSet.FieldByName ('PROGISID').AsInteger then CONTINUE;
         except
         end;
         // if the attribute record of required object doesn't exist - add it, otherwise
         // switch TTable in edit mode.
         if not ADestTable.Locate ('PROGISID', iDestItemID, []) then
            ADestTable.Insert
         else
            ADestTable.Edit;
         // Check state field.
         if ADestTable.FieldByName (cs_StateFieldName).AsString <> cs_ObjectIsNormal then
            ADestTable.FieldByName (cs_StateFieldName).AsString := cs_ObjectIsJustAdded;
         // Copy all attribute data from the currecnt record of the attribute table to required object record.
         for iJ := 0 to TIDBPCDBGrid (ASourceGridOfDragging).DataSource.DataSet.FieldCount - 1 do
         begin
            sFN := AnsiUpperCase (TIDBPCDBGrid (ASourceGridOfDragging).DataSource.DataSet.Fields[iJ].FieldName);
            if FieldIsInternalUsageField (sFN) then CONTINUE;
            if sFN = 'PROGISID' then CONTINUE;
            try
               ADestTable.FieldByName (sFN).Value := TIDBPCDBGrid (ASourceGridOfDragging).DataSource.DataSet.Fields[iJ].Value;
            except
            end;
         end; // for iJ
         // Save data.
         ADestTable.Post;
      end; // for iI end
   finally
      ADestTable.Close;
      ADestTable.Free;
   end;
end;}
// -- Commented by Cadmensky Version 2.3.0

function TIDB_LayerManager.X_GotoObjectRecord (ALayer: Pointer; iItemIndex: Integer): Boolean;
var
   iP: Integer;
   pLD: PLayerDef;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then iP := SELF.AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];
   RESULT := pLD.AX_Man.GotoObjectRecord (iItemIndex);
end;

function TIDB_LayerManager.X_AddRecord (ALayer: Pointer; iItemIndex: Integer): Boolean;
var
   iP: Integer;
   pLD: PLayerDef;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then iP := SELF.AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];
   RESULT := pLD.AX_Man.AddRecord (iItemIndex);
end;

function TIDB_LayerManager.X_GetFieldCount (ALayer: Pointer): Integer;
var
   iP: Integer;
   pLD: PLayerDef;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then iP := SELF.AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];
   RESULT := pLD.AX_Man.GetFieldCount;
end;

function TIDB_LayerManager.X_GetFieldName (ALayer: Pointer; iFieldIndex: Integer): AnsiString;
var
   iP: Integer;
   pLD: PLayerDef;
begin
   RESULT := 'ProgisID';
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then iP := SELF.AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];
   RESULT := pLD.AX_Man.GetFieldName (iFieldIndex);
end;

function TIDB_LayerManager.X_GetFieldValue (ALayer: Pointer; sFieldName: AnsiString): Variant;
var
   iP: Integer;
   pLD: PLayerDef;
begin
   RESULT := 0;
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then iP := SELF.AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];
   RESULT := pLD.AX_Man.GetFieldValue (sFieldName);
end;

function TIDB_LayerManager.X_GetFieldValue (ALayer: Pointer; iFieldIndex: Integer): Variant;
var
   iP: Integer;
   pLD: PLayerDef;
begin
   RESULT := 0;
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then iP := SELF.AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];
   RESULT := pLD.AX_Man.GetFieldValue (iFieldIndex);
end;

procedure TIDB_LayerManager.X_SetFieldValue (ALayer: Pointer; sFieldName: AnsiString; FieldValue: Variant);
var
   iP: Integer;
   pLD: PLayerDef;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then iP := SELF.AddLayer (ALayer);
   pLD := listLayerDefs[iP];
   pLD.AX_Man.SetFieldValue (sFieldName, FieldValue);
end;

procedure TIDB_LayerManager.X_SetFieldValue (ALayer: Pointer; iFieldIndex: Integer; FieldValue: Variant);
var
   iP: Integer;
   pLD: PLayerDef;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then iP := SELF.AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];
   pLD.AX_Man.SetFieldValue (iFieldIndex, FieldValue);
end;

procedure TIDB_LayerManager.DeleteLayerTables (ALayer: Pointer; sAttTableName: AnsiString);
var
   iP: Integer;
begin
   iP := SELF.FindLayer (ALayer);
   if iP > -1 then
      SELF.DeleteLayer (iP);
// ++ Cadmensky Version 2.3.3
   ADBAccessObject.ADbItemsMan.DeleteTable (sAttTableName);
// -- Cadmensky Version 2.3.3
   ADBAccessObject.ADbItemsMan.DeleteAllLayerTables (IntToStr (GetLayerIndexFunc (ALayer)));
end;

function TIDB_LayerManager.GetReadyToReceiveIDsOfObjectsToBeMade: Boolean;
var
   iI: Integer;
   pLD: PLayerDef;
begin
   for iI := 0 to listLayerDefs.Count - 1 do
   begin
      pLD := listLayerDefs[iI];
      pLD.listIDOfObjectToBeMade.Clear;
   end;
   RESULT := TRUE;
end;

procedure TIDB_LayerManager.ReceiveIDOfObjectToBeMade (ALayer: Pointer; iItemID: Integer);
var
   iP: Integer;
   pLD: PLayerDef;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then iP := SELF.AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := listLayerDefs[iP];
   pLD.listIDOfObjectToBeMade.Add (Pointer (iItemID));
end;

procedure TIDB_LayerManager.MakeAnnotations;
var
   slAnnotFieldNames: TStringList;
   AnAnnotMaker: TIDB_AnnotMaker;
   AQuery: TDQuery;
   iI, iJ, iP: Integer;
   iT, iL, iW, iH: Integer;
   pLD: PLayerDef;
   sLayerIndex, sAttTableName, sTemp: AnsiString;
   AnIDBDescMan: TIDB_DescMan;
begin
// ++ Cadmensky Version 2.3.5
   SELF.SetScreenCursor;
// -- Cadmensky Version 2.3.5
   // And now I have to check all information about fields that contains special information
   // in an attribute table.
   slAnnotFieldNames := TStringList.Create;
   AnAnnotMaker := TIDB_AnnotMaker.Create (AProject);
   AQuery := TDQuery.Create (nil);
   AQuery.Master := SELF.ADBAccessObject.DMaster;
   try
      // for each layers that contains selected items.
      for iI := 0 to listLayerDefs.Count - 1 do
      begin
         pLD := listLayerDefs[iI];
         if pLD.listIDOfObjectToBeMade.Count < 1 then CONTINUE;
         sLayerIndex := IntToStr (GetLayerIndexFunc (pLD.ALayer));

         if not AnIDBMainMan.UseNewATTNames (AProject) then
            sAttTableName := cs_AttributeTableNamePrefix + sLayerIndex
         else
            sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, pLD.ALayer);

         if not SELF.ADBAccessObject.ADbItemsMan.TableExists (sAttTableName) then CONTINUE;
         slAnnotFieldNames.Clear;
         //            AnIDBDescMan := TIDB_DescMan.Create(SELF.ADBAccessObject, sLayerIndex);
         AnIDBDescMan := pLD.ADescMan;
         // Get fields that contains annotation information for this layer.
         slAnnotFieldNames.Text := AnIDBDescMan.GetAnnotationFieldNames;
         // Check structure of the layer's attribute table and these field's names
         CheckSpecialFieldsAndAttTableStruct (AProject, SELF.ADBAccessObject, pLD.ALayer, slAnnotFieldNames);
         // if noone from these fields does not exist in the attribute table, I have to ask
         // an user to select once from existing fields.
         if slAnnotFieldNames.Count < 1 then
         begin
            // 409=There is no any selected field as a source for annotations. Please, select the source fields!
            MessageBox (Application.Handle, PChar (ALangManager.GetValue (409)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION);
            iL := -1;
            iT := -1;
            iW := -1;
            iH := -1;
            if not SELF.ShowTableStructure (pLD.ALayer, iT, iL, iW, iH, 2) then
               Continue;
//            AnIDBDescMan.ReadDescriptionTable;
            // Get the user choice.
            slAnnotFieldNames.Text := AnIDBDescMan.GetAnnotationFieldNames;
            // And check it again.
            CheckSpecialFieldsAndAttTableStruct (AProject, SELF.ADBAccessObject, pLD.ALayer, slAnnotFieldNames);
            // I again try to check what user has selected and the attribute table structure.
            // if after that the list will already be empty, I go out in this case.
            if slAnnotFieldNames.Count < 1 then
               Continue;
         end;
         AnAnnotMaker.DeleteAnnotData;
         // Put all field's name's into an annotation maker.
         for iJ := 0 to slAnnotFieldNames.Count - 1 do
            AnAnnotMaker.AddInAnnotData (slAnnotFieldNames[iJ]);
         // Ask user about parameters of annotations object on this layer.
         if not AnAnnotMaker.AnnotationSettingsDialogExecute (pLD.ALayer) then
         begin
            slAnnotFieldNames.Clear;
            AnAnnotMaker.DeleteAnnotData;
            Continue;
         end;

// ++ Cadmensky Version 2.3.5
         slAnnotFieldNames.Text := AnAnnotMaker.GetAnnotationFieldNames;
         AnIDBDescMan.SetAnnotationFieldNames (slAnnotFieldNames.Text);
         AnIDBDescMan.SetUserChoice;
// -- Cadmensky Version 2.3.5

         // Now I'm starting to make anntotaions on the layer.
         for iJ := 0 to pLD.listIDOfObjectToBeMade.Count - 1 do
         begin
            // Get data for the current item.
            if AQuery.Active then AQuery.Close;
            AQuery.SQL.Clear;
            AQuery.SQL.Add ('SELECT');
            for iP := 0 to slAnnotFieldNames.Count - 1 do
               AQuery.SQL.Add ('[' + slAnnotFieldNames[iP] + '], ');
            sTemp := AQuery.SQL[AQuery.SQL.Count - 1];
            AQuery.SQL[AQuery.SQL.Count - 1] := Copy (sTemp, 1, Length (sTemp) - 2);
            AQuery.SQL.Add ('FROM [' + sAttTableName + ']');
            AQuery.SQL.Add ('WHERE PROGISID = ' + IntToStr (Integer (pLD.listIDOfObjectToBeMade[iJ])));
            try
               AQuery.Open;
            except
               CONTINUE;
            end;
            if AQuery.RecordCount < 1 then CONTINUE;
            // Put these data into the annotation maker.
            for iP := 0 to slAnnotFieldNames.Count - 1 do
               AnAnnotMaker.CorrectAnnotData (slAnnotFieldNames[iP], AQuery.FieldByName (slAnnotFieldNames[iP]).AsString);
            AQuery.Close;
            // Make the annotation.
            AnAnnotMaker.MakeAnnotationInProject (pLD.ALayer, Integer (pLD.listIDOfObjectToBeMade[iJ]));
         end; // for iJ end
         // Here I'm finishing work with this layer and go to the next layer.
         slAnnotFieldNames.Clear;
         AnAnnotmaker.DeleteAnnotData;
      end; // for iI end
   finally
// ++ Cadmensky Version 2.3.5
      slAnnotFieldNames.Free;
// -- Cadmensky Version 2.3.5
      AQuery.Close;
      AQuery.Free;
      AnAnnotMaker.Free;
// ++ Cadmensky Version 2.3.5
      SELF.RestoreScreenCursor;
// -- Cadmensky Version 2.3.5
   end;
end;

procedure TIDB_LayerManager.MakeChart;
var
   slChartFieldNames: TStringList;
   AnChartMaker: TIDB_ChartMaker;
   AQuery: TDQuery;
   iI, iJ, iP: Integer;
   iT, iL, iW, iH: Integer;
   pLD: PLayerDef;
   sLayerIndex, sAttTableName, sTemp: AnsiString;
   AnIDBDescMan: TIDB_DescMan;
begin
// ++ Cadmensky Version 2.3.5
   SELF.SetScreenCursor;
// -- Cadmensky Version 2.3.5

   // And now I have to check all information about fields that contains special information
   // in an attribute table.
   slChartFieldNames := TStringList.Create;
   AnChartMaker := TIDB_ChartMaker.Create (AProject);
   AQuery := TDQuery.Create (nil);
   AQuery.Master := SELF.ADBAccessObject.DMaster;
   try
      // for each layers that contains selected items.
      for iI := 0 to listLayerDefs.Count - 1 do
      begin
         pLD := listLayerDefs[iI];
         if pLD.listIDOfObjectToBeMade.Count < 1 then CONTINUE;
         sLayerIndex := IntToStr (GetLayerIndexFunc (pLD.ALayer));

         if not AnIDBMainMan.UseNewATTNames (AProject) then
            sAttTableName := cs_AttributeTableNamePrefix + sLayerIndex
         else
            sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, pLD.ALayer);

         if not SELF.ADBAccessObject.ADbItemsMan.TableExists (sAttTableName) then CONTINUE;
         slChartFieldNames.Clear;
         //            AnIDBDescMan := TIDB_DescMan.Create(SELF.ADBAccessObject, sLayerIndex);
         AnIDBDescMan := pLD.ADescMan;
         // Get fields that contains chart information for this layer.
         slChartFieldNames.Text := AnIDBDescMan.GetChartFieldNames;
         // Check structure of the layer's attribute table and these field's names
         CheckSpecialFieldsAndAttTableStruct (AProject, SELF.ADBAccessObject, pLD.ALayer, slChartFieldNames);
         // if noone from these fields does not exist in the attribute table, I have to ask
         // an user to select once from existing fields.
         if slChartFieldNames.Count < 1 then
         begin
            // 501=There is no any selected field as a source for CHARTS. Please, select the source fields!
            MessageBox (Application.Handle, PChar (ALangManager.GetValue (501)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION);
            iL := -1;
            iT := -1;
            iW := -1;
            iH := -1;
            if not SELF.ShowTableStructure (pLD.ALayer, iT, iL, iW, iH, 3) then
               Continue;
            AnIDBDescMan.ReadDescriptionTable;
            // Get the user choice.
            slChartFieldNames.Text := AnIDBDescMan.GetChartFieldNames;
            // And check it again.
            CheckSpecialFieldsAndAttTableStruct (AProject, SELF.ADBAccessObject, pLD.ALayer, slChartFieldNames);
            // I again try to check what user has selected and the attribute table structure.
            // if after that the list will already be empty, I go out in this case.
            if slChartFieldNames.Count < 1 then Continue;
         end;
         AnChartMaker.DeleteChartData;
         // Put all field's name's into an chart maker.
         for iJ := 0 to slChartFieldNames.Count - 1 do
            AnChartMaker.AddInChartData (slChartFieldNames[iJ]);
         // Ask user about parameters of chart object on this layer.
         if not AnChartMaker.ChartSettingsDialogExecute (pLD.ALayer) then
         begin
            slChartFieldNames.Clear;
            AnChartmaker.DeleteChartData;
            CONTINUE;
         end;
         // Now I'm starting to make anntotaions on the layer.
         for iJ := 0 to pLD.listIDOfObjectToBeMade.Count - 1 do
         begin
            // Get data for the current item.
            if AQuery.Active then AQuery.Close;
            AQuery.SQL.Clear;
            AQuery.SQL.Add ('SELECT');
            for iP := 0 to slChartFieldNames.Count - 1 do
               AQuery.SQL.Add ('[' + slChartFieldNames[iP] + '], ');
            sTemp := AQuery.SQL[AQuery.SQL.Count - 1];
            AQuery.SQL[AQuery.SQL.Count - 1] := Copy (sTemp, 1, Length (sTemp) - 2);
            AQuery.SQL.Add ('FROM [' + sAttTableName + ']');
            AQuery.SQL.Add ('WHERE PROGISID = ' + IntToStr (Integer (pLD.listIDOfObjectToBeMade[iJ])));
            try
               AQuery.Open;
            except
               CONTINUE;
            end;
            if AQuery.RecordCount < 1 then CONTINUE;
            // Put these data into the chart maker.
            for iP := 0 to slChartFieldNames.Count - 1 do
               AnChartMaker.CorrectChartData (slChartFieldNames[iP], AQuery.FieldByName (slChartFieldNames[iP]).AsString);
            AQuery.Close;
            // Make the CHART ! YES !!!
            AnChartMaker.MakeChartInProject (pLD.ALayer, Integer (pLD.listIDOfObjectToBeMade[iJ]));
         end; // for iJ end
         // Here I'm finishing work with this layer and go to the next layer.
         slChartFieldNames.Clear;
         AnChartmaker.DeleteChartData;
      end; // for iI end
   finally
// ++ Cadmensky Version 2.3.5
      slChartFieldNames.Free;
// -- Cadmensky Version 2.3.5
      AQuery.Close;
      AQuery.Free;
      AnChartMaker.Free;
// ++ Cadmensky Version 2.3.5
      SELF.RestoreScreenCursor;
// -- Cadmensky Version 2.3.5
   end;
end;

{function TIDB_LayerManager.GetLayerExtTablesMan (ALayer: Pointer): TIDB_ExternalTablesMan;
var
   iP: Integer;
   pLD: PLayerDef;
begin
   RESULT := nil;
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then
      iP := AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
   pLD := SELF.listLayerDefs[iP];
   RESULT := pLD.AnExtTablesMan;
end;   }

{function TIDB_LayerManager.GetLayerUserFieldsMan (ALayer: Pointer): TIDB_UserFieldsManager;
var
   iP: Integer;
   pLD: PLayerDef;
begin
   RESULT := nil;
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then
      exit;
   pLD := SELF.listLayerDefs[iP];
   RESULT := pLD.AnUserFieldsMan;
end;}

function TIDB_LayerManager.GetLayerDescMan (ALayer: Pointer): TIDB_DescMan;
var
   iP: Integer;
   pLD: PLayerDef;
begin
   RESULT := nil;
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then
// ++ Cadmensky
      iP := AddLayer (ALayer);
// ++ Cadmensky
   if iP < 0 then
      exit;
// -- Cadmensky
// -- Cadmensky
   pLD := SELF.listLayerDefs[iP];
   RESULT := pLD.ADescMan;
end;
{
procedure TIDB_LayerManager.MakeExtTablesLinks;
var
   iI, iJ: Integer;
   pLD: PLayerDef;
begin
   for iI := 0 to SELF.listLayerDefs.Count - 1 do
   begin
      pLD := listLayerDefs[iI];
      if pLD.AnExtTablesMan.Modified then
         for iJ := 0 to Screen.FormCount - 1 do
            if (Screen.Forms[iJ] is TIDB_ViewForm) and
               (TIDB_ViewForm (Screen.Forms[iJ]).ALayer = pLD.ALayer) then
            begin
               TIDB_ViewForm (Screen.Forms[iJ]).MakeExtTablesLinking (false);
               break;
            end;
   end; // for iI end
end;  }

function TIDB_LayerManager.GetLayerFieldsVisibilityManager (ALayer: Pointer): TIDB_FieldsVisibilityManager;
var
   iP: Integer;
   pLD: PLayerDef;
begin
   RESULT := nil;
   iP := SELF.FindLayer (ALayer);
   if iP > -1 then
   begin
      pLD := SELF.listLayerDefs[iP];
      RESULT := pLD.AFieldsVisibilityMan;
   end;
end;

// ++ Commented by Cadmensky Version 2.3.7
(*procedure TIDB_LayerManager.RefreshView (ALayer: Pointer);
var
   bF: boolean;
   pLD: PLayerDef;
   iP, iI, iLayerIndex: Integer;
   AForm: TForm;
   sLayerIndex, sAttTableName: AnsiString;
//   sUserCalcFields: AnsiString;
   AnOldCursor: TCursor;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then
      exit;
   // Try to find required window.
   bF := false;
   for iI := 0 to Screen.FormCount - 1 do
   begin
      AForm := Screen.Forms[iI];
      if AForm is TIDB_ViewForm then
         if TIDB_ViewForm (AForm).ALayer = ALayer then
         begin
            bF := true;
            break;
         end
   end; // for iI end
   AnOldCursor := Screen.Cursor;
   Screen.Cursor := crHourGlass;
   try
      // if there isn't such window, create it.
      pLD := listLayerDefs[iP];
      iLayerIndex := GetLayerIndexFunc (pLD.ALayer);
      // Get position and size for new window.

      Application.ProcessMessages;
      sLayerIndex := IntToStr (iLayerIndex);
      if not AnIDBMainMan.UseNewATTNames (AProject) then
         sAttTableName := cs_AttributeTableNamePrefix + sLayerIndex
      else
         sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, pLD.ALayer);

      // The old way to show attribute data. It is used when we have only to show attribute data of IDB.
//      sUserCalcFields := pLD.AnUserFieldsMan.GetUserCalcFieldsSQLString;
      TIDB_ViewForm (AForm).DataQuery.SQL.Text := 'SELECT *' + {sUserCalcFields +} ' FROM [' + sAttTableName + '] WHERE ' + cs_ShowAllWithoutDeletedObjectsFilter;

{$IFDEF EXT_TAB_POOL}
      if pLD.AnExtTablesMan.IAmInUse then
      begin
         TIDB_ViewForm (AForm).MakeExtTablesLinking (FALSE);
         pLD.AFieldsVisibilityMan.RestoreFieldsStates (TIDBPCDBGrid (TIDB_ViewForm (AForm).ViewDBGrid));
      end
      else
      begin
{$ENDIF}
         // I try open the layer atribute table
         try
            TIDB_ViewForm (AForm).ViewDataSource.DataSet.Open;
         except
            TIDB_ViewForm (AForm).DataQuery.SQL.Text := 'SELECT * FROM [' + sAttTableName + '] WHERE ' + cs_ShowAllWithoutDeletedObjectsFilter;
            try
               TIDB_ViewForm (AForm).ViewDataSource.DataSet.Open;
               MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (417), [GetLayerNameFunc (ALayer)])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
            except
            end;
         end;

         pLD.AFieldsVisibilityMan.RestoreFieldsStates (TIDB_ViewForm (AForm).ViewDBGrid);

         TDADODataSet (TIDB_ViewForm (AForm).ViewDataSource.DataSet).SortAdd ('ProgisID', smASC);

         //           TIDB_ViewForm(AForm).ViewDataSource.DataSet.Next;
         //           TIDB_ViewForm(AForm).ViewDBGrid.Perform(WM_VSCROLL, SB_LINEUP, 0);
         //           TIDB_ViewForm(AForm).ViewDataSource.DataSet.EnableControls;
{$IFDEF EXT_TAB_POOL}
      end;
{$ENDIF}
   finally
      Screen.Cursor := AnOldCursor;
   end;
end;*)

procedure TIDB_LayerManager.FillCalculatedFields (ALayer: Pointer);
var
   sAttTableName: AnsiString;
   AQuery: TDQuery;
   iItemID: integer;
   AProgressForm: TIDB_ProgressForm;
begin
   if not AnIDBMainMan.UseNewATTNames (AProject) then
      sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
   else
      sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

   AQuery := TDQuery.Create (nil);
   try
      AQuery.Master := ADBAccessObject.DMaster;
      AQuery.SQL.Text := 'SELECT * FROM [' + sAttTableName + '] WHERE ' + cs_ShowAllWithoutDeletedObjectsFilter;
      AQuery.Open;
      AQuery.First;

      AProgressForm := TIDB_ProgressForm.Create (AWinGISMainForm);
      AProgressForm.StatusLabel.Caption := Format (ALangManager.GetValue (426), [GetLayerNameFunc (ALayer)]);
      AProgressForm.GlobalGauge.Progress := 0;

      //      AProgressForm.ShowTheBottomPanel;
      AProgressForm.Show;
      Application.ProcessMessages;
      AProgressForm.GlobalGauge.MaxValue := AQuery.RecordCount;

      while not AQuery.EOF do
      begin
         AProgressForm.GlobalGauge.Progress := AProgressForm.GlobalGauge.Progress + 1;
         iItemID := AQuery.FieldByName ('PROGISID').AsInteger;
         FillCalculatedField (ALayer, iItemID);

         if AProgressForm.UserWantsToCancel then
            break;
         Application.ProcessMessages;
         AQuery.Next;
      end;
   finally
      AProgressForm.Hide;
      AProgressForm.Free;

      AQuery.Free;
   end;
end;

procedure TIDB_LayerManager.FillCalculatedField (ALayer: Pointer; iItemID: integer);
var
   sAttTableName: AnsiString;
   sValue: AnsiString;
   sSQL: AnsiString;
   AQuery: TDQuery;
   iI: integer;
   slCalcFields: TStringList;
   iType, iSize: integer;
   bAdded: boolean;
begin
   if not AnIDBMainMan.UseNewATTNames (AProject) then
      sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
   else
      sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

   slCalcFields := TStringList.Create;
   slCalcFields.Text := SELF.GetUsedCalcFieldsNames (ALayer);
   if slCalcFields.Count < 1 then
   begin
      slCalcFields.Free;
      exit;
   end;

   try
      AQuery := TDQuery.Create (nil);
      AQuery.Master := ADBAccessObject.DMaster;
      // Wenn die Felder _X1 und _Y1 existieren, f�ge sie hinzu.
      try
        AQuery.SQL.Text:='SELECT * FROM [' + sAttTableName + '] WHERE  [' + cs_InternalID_FieldName + ']= -1';
        AQuery.Open;
        for iI := 0 to AQuery.Fields.Count - 1 do
        if AnsiCompareText (AQuery.Fields[iI].Fieldname , '_X1') = 0 then
        begin
           slCalcFields.Add('_X1');
           break;
        end;
        for iI := 0 to AQuery.Fields.Count - 1 do
        if AnsiCompareText (AQuery.Fields[iI].fieldName, '_Y1') = 0 then
        begin
         slCalcFields.Add('_Y1');
         break;
        end;
      except end;

      sSQL := 'UPDATE [' + sAttTableName + '] SET ';
      bAdded := false;
      for iI := 0 to slCalcFields.Count - 1 do
      begin
         if GetCalcFieldTypeAndSize (slCalcFields[iI], iType, iSize) then
         begin
            bAdded := true;
            sValue := GetObjectPropsEx (AProject, ALayer, iItemID, GetDataPropertyByCalcFieldName (slCalcFields[iI]));
            if iType = 1 then
               begin
               if length(sValue)> 254 then sValue:=copy(sValue,length(sValue)-254,length(sValue));
               if sValue='' then sValue:=' ';
               sValue := '''' + sValue + '''';
               end
            else
               if sValue = '' then
                  sValue := 'NULL';
            sSQL := sSQL + '[' + slCalcFields[iI] + '] = ' + sValue + ', ';
         end;
      end;
      sSQL := Copy (sSQL, 1, Length (sSQL) - 2);
      sSQL := sSQL + ' WHERE [PROGISID] = ' + IntToStr (iItemID);
      if bAdded then
         AQuery.ExecSQL (sSQL);
   finally
      slCalcFields.Free;
      AQuery.Free;
   end;

{      sAreaValue := GetObjectPropsEx (AProject, ALayer, iItemID, dtArea);
      if sAreaValue = '' then
         sAreaValue := 'NULL';

      sLengthValue := GetObjectPropsEx (AProject, ALayer, iItemID, dtLength);
      if sLengthValue = '' then
         sLengthValue := 'NULL';

//      sPerimeterValue := GetObjectPropsEx (AProject, ALayer, iItemID, dtPerimeter);
//      if sPerimeterValue = '' then
//         sPerimeterValue := 'NULL';

      sXValue := GetObjectPropsEx (AProject, ALayer, iItemID, dtX);
      if sXValue = '' then
         sXValue := 'NULL';

      sYValue := GetObjectPropsEx (AProject, ALayer, iItemID, dtY);
      if sYValue = '' then
         sYValue := 'NULL';

      sVertices := GetObjectPropsEx (AProject, ALayer, iItemID, dtVerticesCount);
      if sVertices = '' then
         sVertices := 'NULL';

      sSymbolName := GetObjectPropsEx (AProject, ALayer, iItemID, dtSymbolName);

      sSQL := 'UPDATE [' + sAttTableName + '] ' +
         'SET [' + cs_CalcFieldAreaName + '] = ' + sAreaValue +
         ', [' + cs_CalcFieldLengthName + '] = ' + sLengthValue +
//         ', [' + cs_CalcFieldPerimeterName + '] = ' + sPerimeterValue +
      ', [' + cs_CalcFieldXName + '] = ' + sXValue +
         ', [' + cs_CalcFieldYName + '] = ' + sYValue +
         ', [' + cs_CalcFieldVerticesCount + '] = ' + sVertices;
      if sSymbolName <> '' then
         sSQL := sSQL + ', [' + cs_CalcFieldSymbolName + '] = ''' + sSymbolName + '''';
      sSQL := sSQL + ' WHERE [PROGISID] = ' + IntToStr (iItemID);
      AQuery.ExecSQL (sSQL);
   finally
      AQuery.Free;
   end;}
end;

function TIDB_LayerManager.GetTableName (ALayer: Pointer; var sFieldName: AnsiString): AnsiString;
var
   iP: integer;
   pLD: PLayerDef;
   iLinkNumber: integer;
   //AnExtTablesMan: TIDB_ExternalTablesMan;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then
      exit;
   pLD := SELF.listLayerDefs[iP];

 {  iLinkNumber := GetLinkedFieldLinkNo (sFieldName, sFieldName);
   if iLinkNumber < 0 then
   begin   }
      if not AnIDBMainMan.UseNewATTNames (AProject) then
         RESULT := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
      else
         RESULT := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);
{   end
   else
   begin
     // AnExtTablesMan := pLD.AnExtTablesMan;
    //  RESULT := AnExtTablesMan.GetLinkName (iLinkNumber);
   end;   }
end;

// ++ Cadmensky Version 2.2.9

function TIDB_LayerManager.GetCalcFieldIsUsed (ALayer: Pointer; sFieldName: AnsiString): Boolean;
var
   iP: Integer;
   pLD: PLayerDef;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then
      exit;
   pLD := SELF.listLayerDefs[iP];
   RESULT := pLD.slCalcFields.IndexOf (sFieldName) <> -1;
end;
// -- Cadmensky Version 2.2.9

procedure TIDB_LayerManager.SetCalcFieldIsUsed (ALayer: Pointer; sFieldName: AnsiString; bValue: boolean);
var
   iP: integer;
   pLD: PLayerDef;
   iIndex: integer;
begin
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then
      exit;
   pLD := SELF.listLayerDefs[iP];
   if bValue then
   begin
      if pLD.slCalcFields.IndexOf (sFieldName) = -1 then
         pLD.slCalcFields.Add (sFieldName);
   end
   else
   begin
      iIndex := pLD.slCalcFields.IndexOf (sFieldName);
      if pLD.slCalcFields.IndexOf (sFieldName) <> -1 then
         pLD.slCalcFields.Delete (iIndex);
   end;
end;

function TIDB_LayerManager.GetUsedCalcFieldsNames (ALayer: Pointer): AnsiString;
var
   iP: integer;
   pLD: PLayerDef;
begin
   RESULT := '';
   iP := SELF.FindLayer (ALayer);
   if iP < 0 then
      exit;
   pLD := SELF.listLayerDefs[iP];
   RESULT := pLD.slCalcFields.Text;
end;

procedure TIDB_LayerManager.DeleteATable(stablename: AnsiString);
begin
ADBAccessObject.ADbItemsMan.DeleteTable (stablename);
end;

end.

