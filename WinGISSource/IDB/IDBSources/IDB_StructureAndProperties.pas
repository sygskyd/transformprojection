unit IDB_StructureAndProperties;
{
Internal database. Ver. II.
This form will replace two different dialog - table structure dialog and
     table properties dialog.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 22-11-2001
}
interface

uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   IDB_Grids, IDB_C_StringGrid, Grids, StdCtrls, Menus, ImgList,
   IDB_StructMan, IDB_Access2DB, ExtCtrls, IDB_C_Panel, ComCtrls, Buttons,
   IDB_LayerManager; //, IDB_UserFieldsMan;

type
   TIDB_StructureAndPropertiesForm = class (TForm)
      PopupMenu1: TPopupMenu;
      AddFieldPopUp: TMenuItem;
      DeleteFieldPopUp: TMenuItem;
      ImageList1: TImageList;
      CopyPopUp: TMenuItem;
      CutPopUp: TMenuItem;
      PastePopUp: TMenuItem;
      N1: TMenuItem;
      BottomPanel: TIDB_Panel;
      ApplyToAllCB: TCheckBox;
      OKButton: TButton;
      CancelButton: TButton;
      PageControl1: TPageControl;
      StructureTS: TTabSheet;
      SG: TIDB_StringGrid;
      FieldTypesComboBox: TComboBox;
    IsTemplateCheckBox: TCheckBox;
      procedure FormCreate (Sender: TObject);
      procedure FormDestroy (Sender: TObject);
      procedure SGDrawCell (Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
      procedure FormCloseQuery (Sender: TObject; var CanClose: Boolean);
      procedure FieldTypesComboBoxClick (Sender: TObject);
      procedure SGDeSelectCell (Sender: TObject; ACol, ARow: Integer;
         var CanDeSelect: Boolean);
      procedure SGSelectCell (Sender: TObject; ACol, ARow: Integer;
         var CanSelect: Boolean);
      procedure SGSetEditText (Sender: TObject; ACol, ARow: Integer;
         const Value: string);
      procedure AddFieldPopUpClick (Sender: TObject);
      procedure DeleteFieldPopUpClick (Sender: TObject);
      procedure PopupMenu1Popup (Sender: TObject);
      procedure CopyPopUpClick (Sender: TObject);
      procedure CutPopUpClick (Sender: TObject);
      procedure PastePopUpClick (Sender: TObject);
      procedure SGRowMoved (Sender: TObject; FromIndex, ToIndex: Integer);
      procedure AnnotationsTSShow (Sender: TObject);
      procedure AnnotationsTSResize (Sender: TObject);
      procedure AnnotAvailableListBoxDblClick (Sender: TObject);
      procedure AnnotSelectedListBoxDblClick (Sender: TObject);
      procedure AnnotAddOneSBClick (Sender: TObject);
      procedure AnnotAddAllSBClick (Sender: TObject);
      procedure AnnotRemoveOneSBClick (Sender: TObject);
      procedure AnnotRemoveAllSBClick (Sender: TObject);
      procedure AnnotUpSBClick (Sender: TObject);
      procedure AnnotDownSBClick (Sender: TObject);
      procedure SpeedButton1Click (Sender: TObject);
      procedure ChartAddOneSBClick (Sender: TObject);
      procedure ChartAddAllSBClick (Sender: TObject);
      procedure ChartRemoveOneSBClick (Sender: TObject);
      procedure ChartRemoveAllSBClick (Sender: TObject);
      procedure ChartAvailableListBoxDblClick (Sender: TObject);
      procedure ChartSelectedListBoxDblClick (Sender: TObject);
      procedure ChartsTSShow (Sender: TObject);
      procedure ChartUpSBClick (Sender: TObject);
      procedure ChartDownSBUpSBClick (Sender: TObject);
      procedure ChartsTSResize (Sender: TObject);
      procedure OKButtonClick (Sender: TObject);
      procedure CancelButtonClick (Sender: TObject);
      procedure FormActivate (Sender: TObject);
      function FieldIsNumberType (sFieldName: AnsiString): Boolean;
      procedure SGTopLeftChanged (Sender: TObject);
      procedure checkboxbitmap(aCol, aRow : Integer; checked : Boolean);
    procedure IsTemplateCheckBoxClick(Sender: TObject);
    procedure ApplyToAllCBClick(Sender: TObject);
   private
      { Private declarations }
      ACheckBitmap,
         AnIndicatorBitmap: TBitmap;
      bModified: Boolean;

      bIShowOnlyFieldsVisibility,
         bInitializationSettings: Boolean;

      iFieldNameCol,
         iFieldTypeCol,
         iFieldSizeCol,
         iFieldVisibleCol,
         iFieldIndexCol,
         iGeotextCol,
         iMonitoredIDCol,
         {+++brovak}
      iAnnotCol,
         iChartCol,
          {---brovak}
         // ++ Cadmensky
      iPolygonOverlayCol,
         iInfoVisibleCol: integer;
      // -- Cadmensky

      AProject: Pointer;
      AStructMan: TIDB_StructMan;
      iLayerIndex: integer;
      sCurrentFieldName: AnsiString;
      iCurrentCol, iCurrentRow: integer;

      bNewFieldIsBeingAdded: Boolean;

      AnOldScreenCursor: TCursor;

      procedure SetScreenCursor;
      procedure RestoreScreenCursor;

      procedure SetModified;
      function GetFieldTypeName (sFieldName: AnsiString): AnsiString;
      function GetFieldTypeIndex (sFieldName: AnsiString): Integer;

      procedure SwitchToEditMode;
      procedure SwitchToBrowseMode;

      procedure SetGeoTextFieldName (sFieldName: AnsiString);
      function GetGeoTextFieldName: AnsiString;

      procedure SetMonitoredFieldName (sFieldName: AnsiString);
      function GetMonitoredFieldName: AnsiString;

      procedure SetAnnotationsSourceFields (sFieldNames: AnsiString);
      function GetAnnotationsSourceFields: AnsiString;
      {brovak}
      procedure SetChartSourceFields (sFieldNames: AnsiString);
      function GetChartSourceFields: AnsiString;
      {brovak}
      procedure MakeNewFieldAdding;
      procedure MakeFieldDeleting;

      procedure WP (var WPMsg: TWMSysCommand); message WM_SYSCommand;
//      procedure SetUserCalcFieldProperties (sFieldName: AnsiString);
//      procedure DeleteUserField (sFieldName: AnsiString);
//      function AddNewUserField (sFieldName: AnsiString): AnsiString;
      // ++ Cadmensky
      function GetOverlaySourceFields: AnsiString;
      procedure SetOverlaySourceFields (sFieldNames: AnsiString);
      procedure SetInfoSourceFields (sFieldNames: AnsiString);
      function GetInfoSourceFields: AnsiString;
      // -- Cadmensky
   public
      { Public declarations }
      bNeedToFillCalculatedFields: boolean;
      constructor Create (AOwner: TComponent; AProject: Pointer; iLayerIndex: integer; AStructureManager: TIDB_StructMan; bTheWindowWillShowOnlyFieldsVisibility: Boolean = FALSE);

      procedure _ReSize; // It is named so (starting from '_' sign beacuse the compilator said 'Resize method hides the same nethods of TControl')

      property FieldNameCol: Integer read iFieldNameCol;
      property FieldTypeCol: Integer read iFieldTypeCol;
      property FieldSizeCol: Integer read iFieldSizeCol;
      property FieldVisibleCol: Integer read iFieldVisibleCol;
      property FieldIndexCol: Integer read iFieldIndexCol;
      property FieldInfoVisibleCol: Integer read iInfoVisibleCol;
      property GeoTextFieldName: AnsiString read GetGeoTextFieldName write SetGeoTextFieldName;
      property MonitoredFieldName: AnsiString read GetMonitoredFieldName write SetMonitoredFieldName;
      property AnnotationsSourceFieldNames: AnsiString read GetAnnotationsSourceFields write SetAnnotationsSourceFields;
      {brovak}
      property ChartSourceFieldNames: AnsiString read GetChartSourceFields write SetChartSourceFields;
      {brovak}
  // ++ Cadmensky
      property OverlaySourceFieldNames: AnsiString read GetOverlaySourceFields write SetOverlaySourceFields;
      property InfoSourceFieldNames: AnsiString read GetInfoSourceFields write SetInfoSourceFields;
      // -- Cadmensky
   end;

implementation
{$R *.DFM}
{$R CheckBox.res}

uses Clipbrd,
   IDB_LanguageManager, IDB_CallbacksDef, IDB_Utils,
   {IDB_ExternalTablesMan,} IDB_MainManager,
{   IDB_UserFieldProps,} IDB_Consts;

procedure TIDB_StructureAndPropertiesForm.WP (var WPMsg: TWMSysCommand);
begin
   case WPMsg.CmdType of
      SC_Minimize: Application.Minimize;
   else
      inherited;
   end;
end;

constructor TIDB_StructureAndPropertiesForm.Create (AOwner: TComponent; AProject: Pointer; iLayerIndex: integer; AStructureManager: TIDB_StructMan; bTheWindowWillShowOnlyFieldsVisibility: Boolean = FALSE);
begin
   inherited Create (AOwner);
   SELF.bInitializationSettings := TRUE;
   SELF.bNeedToFillCalculatedFields := false;
   SELF.AProject := AProject;
   SELF.iLayerIndex := iLayerIndex;
   SELF.AStructMan := AStructureManager;
   SELF.bIShowOnlyFieldsVisibility := bTheWindowWillShowOnlyFieldsVisibility;
end;

procedure TIDB_StructureAndPropertiesForm.FormCreate (Sender: TObject);
var
   iI, iJ, iTop: Integer;
begin
   ALangManager.MakeReplacement (SELF);

   SELF.bModified := FALSE;
   SELF.bNewFieldIsBeingAdded := FALSE;
   ACheckBitmap := TBitmap.Create;

   AnIndicatorBitmap := TBitmap.Create;
   AnIndicatorBitmap.LoadFromResourceName (HINSTANCE, 'INDICATOR');

   SG.AllowMovingFromToTheFirstLine := FALSE;
   SG.ColWidths[0] := AnIndicatorBitmap.Width + 2;

   PageControl1.Anchors := [];

   if not bIShowOnlyFieldsVisibility then
   begin
      SG.AllowMovingFromToTheLastLine := FALSE;
      SG.ColCount := 11; {+++ Brovak}

      iFieldNameCol := 1;
      iFieldTypeCol := 2;
      iFieldSizeCol := 3;
      iFieldVisibleCol := 4;
      iFieldIndexCol := 5;
      iGeotextCol := 6;
  (*    iMonitoredIDCol := 9;
      // ++ Cadmensky
      iPolygonOverlayCol := 7;
      iInfoVisibleCol := 8;
      // -- Cadmensky
                       *)
      iPolygonOverlayCol := 7;
      iAnnotCol := 8;
      IChartCol := 9;
      iInfoVisibleCol := 10;
      iMonitoredIDCol := 11;

 //     AnnotationsTS.TabVisible := TRUE;
 //     ChartsTS.TabVisible := true;
      iTop := 0;
   end
   else
   begin
      SG.AllowMovingFromToTheLastLine := TRUE;
      SG.ColCount := 3;

      iFieldNameCol := 1;
      iFieldTypeCol := 100; // 100 and more - this only to avoid settings in existing cells.
      iFieldSizeCol := 101;
      iFieldVisibleCol := 105;
      iFieldIndexCol := 102;
      iGeotextCol := 103;
      iMonitoredIDCol := 106;
      // ++ Cadmensky
      iPolygonOverlayCol := 104;
      iInfoVisibleCol := 2;
      // -- Cadmensky
      {brovak}
      iAnnotCol := 118;
      IChartCol := 119;
      {brovak}
      SG.PopupMenu := nil;

//      AnnotationsTS.TabVisible := FALSE;
//      ChartsTS.TabVisible := FALSE;
      iTop := -StructureTS.Top + 2;
   end;

   PageControl1.Left := 0;
   PageControl1.Width := SELF.ClientWidth;
   PageControl1.Top := iTop;
   PageControl1.Height := SELF.ClientHeight - iTop - BottomPanel.Height;
   PageControl1.Height := SELF.ClientHeight - iTop - BottomPanel.Height;

   PageControl1.Anchors := [akLeft, akTop, akRight, akBottom];

   PageControl1.ActivePage := StructureTS;

   SG.Cells[iFieldNameCol, 0] := ALangManager.GetValue (310); // 310=Field name
   SG.Cells[iFieldTypeCol, 0] := ALangManager.GetValue (116); // 116=Field type
   SG.Cells[iFieldSizeCol, 0] := ALangManager.GetValue (117); // 117=Field size
   SG.Cells[iFieldVisibleCol, 0] := ALangManager.GetValue (385); // 385=Visible
   SG.Cells[iFieldIndexCol, 0] := ALangManager.GetValue (399); // 399=Indexed
   SG.Cells[iGeotextCol, 0] :=ALangManager.GetValue (140); // 140=Geotext field
   SG.Cells[iMonitoredIDCol, 0] := ALangManager.GetValue (141); // 141=Displayed field
   // ++ Cadmensky
   SG.Cells[iPolygonOverlayCol, 0] := ALangManager.GetValue (424); // 424=Use for overlay
   SG.Cells[iInfoVisibleCol, 0] := ALangManager.GetValue (427); // 424=Info visible
   // -- Cadmensky

   {brovak}
   SG.Cells[iAnnotCol, 0] := ALangManager.GetValue (600); // 424=Info visible
   SG.Cells[iChartCOl, 0] := ALangManager.GetValue (601); // 424=Info visible
   {brovak}

   SELF._ReSize;

   for iI := 0 to SG.RowCount - 1 do
      for iJ := 0 to SG.ColCount - 1 do
         SG.Objects[iJ, iI] := Pointer (0);
//   SpeedButton1.Parent := SG; // Cadmensky
end;

procedure TIDB_StructureAndPropertiesForm.FormDestroy (Sender: TObject);
begin
   ACheckBitmap.Free;
   AnIndicatorBitmap.Free;
end;

procedure TIDB_StructureAndPropertiesForm.SetModified;
begin
   SELF.bModified := TRUE;
   SELF.OKButton.Enabled := TRUE;
end;

procedure TIDB_StructureAndPropertiesForm.SetScreenCursor;
begin
   SELF.AnOldScreenCursor := Screen.Cursor;
   Screen.Cursor := crHourGlass;
   Application.ProcessMessages;
   Application.HandleMessage;
end;

procedure TIDB_StructureAndPropertiesForm.RestoreScreenCursor;
begin
   Screen.Cursor := SELF.AnOldScreenCursor;
end;

procedure TIDB_StructureAndPropertiesForm._ReSize;
var
   iI, iJ, iK, iMax, iCurr: Integer;
begin
   iJ := SG.ColWidths[0];
   for iI := 1 to SG.ColCount - 1 do
   begin
      if iI <> iFieldTypeCol then
         SG.ResizeColumn (iI)
      else
      begin
         iMax := SG.Canvas.TextWidth (FieldTypesComboBox.Items[0]);
         for iK := 1 to FieldTypesComboBox.Items.Count - 1 do
         begin
            iCurr := SG.Canvas.TextWidth (FieldTypesComboBox.Items[iI]);
            if iCurr > iMax then iMax := iCurr;
         end;
         SG.ColWidths[iFieldTypeCol] := iMax + 10;
      end;
      iJ := iJ + SG.ColWidths[iI];
   end;
   SELF.ClientWidth := iJ + 15;
end;

procedure TIDB_StructureAndPropertiesForm.SGDrawCell (Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
   iX, iY, iLastRow: Integer;
   sText: AnsiString;
   ComboBoxRect: TRect;
begin
   { +++ Brovak}
   if ACheckBitmap <> nil then
   begin
      ACheckBitmap.Free;
      ACheckBitmap := TBitmap.Create;
   end;
   {--Brovak}

   if bIShowOnlyFieldsVisibility then
      iLastRow := SG.RowCount
   else
      iLastRow := SG.RowCount - 1;

   // Navigator column - sign '>' at the very left column.
   if ACol = 0 then
   begin
      if ARow = SG.Row then
      begin
         iY := (Rect.Bottom - Rect.Top) div 2 - AnIndicatorBitmap.Height div 2 + Rect.Top;
         iX := (Rect.Right - Rect.Left) div 2 - AnIndicatorBitmap.Width div 2 + Rect.Left;
         SG.Canvas.Draw (iX, iY, AnIndicatorBitmap);
      end;
   end
   else
      // CheckBox imitator in 'Visible', 'GeoText', and 'Monitored ID' columns.
      if ((ARow > 0) and (ARow < iLastRow {SG.RowCount-1})) and
         ((ACol = iFieldVisibleCol) or (ACol = iFieldIndexCol) or (ACol = iGeotextCol) or (ACol = iMonitoredIDCol) or (ACol = iInfoVisibleCol) or (ACol = iAnnotCol) or (ACol = iChartCol)) then
      begin
         {brovak}
         if (ACol = iChartCol) and
            (not FieldIsNumberType (SG.Cells[iFieldNameCol, ARow])) then
            ACheckBitmap.LoadFromResourceName (HINSTANCE, 'GRAYSTATE')
         else
         {brovak}
            if (ARow = 1) and (ACol = iFieldIndexCol) then
            // ProgisID field is always indexed field. The user cannot make it non-indexed field.
               ACheckBitmap.LoadFromResourceName (HINSTANCE, 'CHECKEDGRAYSTATE')
            else
            // Only for noncalculated fields or if the column is fields visibility state column.
               if ((Integer (SG.Objects[iFieldNameCol, ARow]) = 1) or (ACol = iFieldVisibleCol) or (ACol = iInfoVisibleCol) or (((ACol = iAnnotCol) or (ACol = iChartCol)) and (Integer (SG.Objects[iFieldNameCol, ARow])<>2 )))  then
               begin
                  if (FieldIsLinked (SG.Cells[iFieldNameCol, ARow])) or
                     ((ACol <> iFieldIndexCol) and (ACol <> iGeotextCol)) then
                  begin
                     if Integer (SG.Objects[ACol, ARow]) <> 0 then
                        checkboxbitmap(ACol, ARow, True)
                        //ACheckBitmap.LoadFromResourceName (HINSTANCE, 'CHECKED')
                     else
                        checkboxbitmap(ACol, ARow, False);
                        //ACheckBitmap.LoadFromResourceName (HINSTANCE, 'UNCHECKED');
                  end
                  else
                  begin
                     ACheckBitmap.LoadFromResourceName (HINSTANCE, 'GRAYSTATE');
                  end;
               end
               else
                  ACheckBitmap.LoadFromResourceName (HINSTANCE, 'GRAYSTATE');
         iY := (Rect.Bottom - Rect.Top) div 2 - ACheckBitmap.Height div 2 + Rect.Top;
         iX := (Rect.Right - Rect.Left) div 2 - ACheckBitmap.Width div 2 + Rect.Left;
         SG.Canvas.Draw (iX, iY, ACheckBitmap);
      end
      else
         // Combobox in 'Field Type' column
         if ACol = iFieldTypeCol then
         begin
            if ARow = SG.Row then
            begin
               if (Integer (SG.Objects[iFieldNameCol, ARow]) = 1) and (FieldTypesComboBox.Visible) then
               begin
                  // for noncalculated fields I must show the ComboBox.
                  ComboBoxRect := SELF.SG.CellRect (iFieldTypeCol, ARow);
                  FieldTypesComboBox.Left := {Rect.Left + } SG.Left + ComboBoxRect.Left + 1;
                  FieldTypesComboBox.Top := {Rect.Top + } SG.Top + ComboBoxRect.Top + 1;
                  FieldTypesComboBox.Width := Rect.Right - Rect.Left + 1;
                  FieldTypesComboBox.Height := Rect.Bottom - Rect.Top;
               end
               else
               begin
                  // for calculated fields I mustn't show the ComboBox - the user cannot change the type of such fields.
                  if Integer (SG.Objects[iFieldNameCol, ARow]) = 1 then
                     sText := GetFieldTypeName (SG.Cells[iFieldNameCol, ARow])
                  else
                     if Integer (SG.Objects[iFieldNameCol, ARow]) = 2 then
                        sText := ALangManager.GetValue (418) // 418=Proportion
                     else
                        begin
                        sText := ALangManager.GetValue (401); // 401=Calculated
                        end;
                  iY := (Rect.Bottom - Rect.Top) div 2 - SG.Canvas.TextHeight (sText) div 2 + Rect.Top;
                  SG.Canvas.TextOut (Rect.Left + 2, iY, sText);
               end;
            end
            else
               if (ARow > 0) and (ARow < SG.RowCount - 1) then
               begin
                  if Integer (SG.Objects[iFieldNameCol, ARow]) = 1 then
                     sText := GetFieldTypeName (SG.Cells[iFieldNameCol, ARow])
                  else
                     if Integer (SG.Objects[iFieldNameCol, ARow]) = 2 then
                        sText := ALangManager.GetValue (418) // 418=Not used
                     else
                        sText := ALangManager.GetValue (401); // 401=Calculated
                  iY := (Rect.Bottom - Rect.Top) div 2 - SG.Canvas.TextHeight (sText) div 2 + Rect.Top;
                  SG.Canvas.TextOut (Rect.Left + 2, iY, sText);
               end;
         end;

   // ++ Cadmensky
   if (ACol = iPolygonOverlayCol) and (ARow > 0) and (ARow < SG.RowCount - 1) then
   begin
      if (Integer (SG.Objects[iFieldNameCol, ARow]) <> 2) then
      begin
         if Integer (SG.Objects[ACol, ARow]) <> 0 then
         begin
            checkboxbitmap(ACol, ARow,True);
            //ACheckBitmap.LoadFromResourceName (HINSTANCE, 'CHECKED');
            if Integer (SG.Objects[ACol, ARow]) = 1 then
               sText := ALangManager.GetValue (428)
            else
               sText := ALangManager.GetValue (429);
         end
         else checkboxbitmap(ACol, ARow,False);//ACheckBitmap.LoadFromResourceName (HINSTANCE, 'UNCHECKED');
         iX := Rect.Left + 10;
         iY := (Rect.Bottom - Rect.Top) div 2 - SG.Canvas.TextHeight (sText) div 2 + Rect.Top;
         SG.Canvas.TextOut (iX + ACheckBitmap.Width + 5, iY, sText);
      end
      else
         ACheckBitmap.LoadFromResourceName (HINSTANCE, 'GRAYSTATE');

      iX := Rect.Left + 10;
      iY := (Rect.Bottom - Rect.Top) div 2 - ACheckBitmap.Height div 2 + Rect.Top;
      SG.Canvas.Draw (iX, iY, ACheckBitmap);
   end;
{   if (Integer (SG.Objects[iFieldNameCol, ARow]) = 2) and
      (ACol <> 0) and
      (ACol <> iFieldNameCol) and
      (ACol <> iFieldSizeCol) and
      (ACol <> iPolygonOverlayCol) and
      (ACol <> iFieldTypeCol) then
   begin
      ACheckBitmap.LoadFromResourceName (HINSTANCE, 'GRAYSTATE');
      iY := (Rect.Bottom - Rect.Top) div 2 - ACheckBitmap.Height div 2 + Rect.Top;
      iX := (Rect.Right - Rect.Left) div 2 - ACheckBitmap.Width div 2 + Rect.Left;
      SG.Canvas.Draw (iX, iY, ACheckBitmap);
   end;}
// -- Cadmensky

end;

procedure TIDB_StructureAndPropertiesForm.FormCloseQuery (Sender: TObject; var CanClose: Boolean);
begin
// ++ Cadmensky
//   if SELF.ModalResult <> mrOK then EXIT;

   if (SELF.ModalResult = mrOK) or (SELF.bModified and
      // 395=Apply changes?
      (MessageBox (Handle, PChar (ALangManager.GetValue (395)), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrYes)) then
   begin
      SELF.ModalResult := mrOK;
// -- Cadmensky
      if not bIShowOnlyFieldsVisibility then
      begin
         SetScreenCursor;
         try
            AStructMan.DoChangeStructure;
         finally
            RestoreScreenCursor;
         end;
      end;
   end
   else
   begin
      SELF.ModalResult := mrCancel;
      EXIT;
   end;
   if ApplyToAllCB.Checked
      and
      (not bIShowOnlyFieldsVisibility)
      and
      // 402=Are you sure to apply the structure of the table to all the tables? Some data might be lost because of the converting.
   (MessageBox (Handle, PChar (ALangManager.GetValue (402)), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrYes) then
   begin
      SetScreenCursor;
      try
         AStructMan.ApplyStructureToAllTables;
      finally
         RestoreScreenCursor;
      end;
   end
end;

procedure TIDB_StructureAndPropertiesForm.FieldTypesComboBoxClick (Sender: TObject);

   procedure SetTypeAndSize (iFieldType, iFieldSize: Integer);
   begin
      SELF.AStructMan.ChangeFieldTypeAndSize (sCurrentFieldName, iFieldType, iFieldSize);
      SG.Objects[iFieldTypeCol, SG.Row] := Pointer (iFieldType);
      if iFieldTypeCol<>1 then SG.Cells[iFieldSizeCol,SG.Row]:='';
   end;

var
   iOldFieldType,
      iOldFieldSize: Integer;
begin
   iOldFieldType := Integer (SG.Objects[iFieldTypeCol, SG.Row]);
   if FieldTypesComboBox.ItemIndex = 0 then
   begin
      // String field type
      iOldFieldSize := Integer (SG.Objects[iFieldSizeCol, SG.Row]);
      if (iOldFieldType <> 1) and (iOldFieldSize < 1) then
         iOldFieldSize := 50;
      SetTypeAndSize (1, iOldFieldSize);
      SG.Cells[iFieldSizeCol, SG.Row] := IntToStr (iOldFieldSize);
   end
   else
   begin
      if iOldFieldType = 1 then
      try
         SG.Objects[iFieldSizeCol, SG.Row] := Pointer (StrToInt (SG.Cells[iFieldSizeCol, SG.Row]));
      except
      end;
      case FieldTypesComboBox.ItemIndex of
         1: SetTypeAndSize (12, -1); // Byte field type
         2: SetTypeAndSize (2, -1); // Integer field type
         3: SetTypeAndSize (3, -1); // Single field type
         4: SetTypeAndSize (6, 4); // Long field type
         5: SetTypeAndSize (6, 8); // Double field type
         6: SetTypeAndSize (11, -1); // DateTime field type
         7: SetTypeAndSize (7, -1); // Currency field type
         8: SetTypeAndSize (5, -1); // Boolean field type
         // ++ Cadmensky
         9:
            begin
{               SG.Objects[iFieldNameCol, SG.Row] := Pointer (2); // Proportion field type
               SG.Objects[iFieldSizeCol, SG.Row] := Pointer (0); // Proportion field type
               SetTypeAndSize (6, -1);
               SG.Cells[iFieldSizeCol, SG.Row] := '';
               SG.Cells[iFieldNameCol, SG.Row] := AddNewUserField (SG.Cells[iFieldNameCol, SG.Row]);
               FieldTypesComboBox.Visible := false;}
            end;
         // -- Cadmensky
      end; // case end
   end;
   SELF.SetModified;
end;

function TIDB_StructureAndPropertiesForm.GetFieldTypeIndex (sFieldName: AnsiString): Integer;
var
   iFS, iFT: Integer;
begin
   iFT := AStructMan.GetFieldType (sFieldName);
   iFS := AStructMan.GetFieldSize (sFieldName);

   RESULT := -1;
   if (iFT = 1) then // String field type
      RESULT := 0
   else
      if (iFT = 12) then // Byte field type
         RESULT := 1
      else
         if (iFT = 2) then // Integer field type
            RESULT := 2
         else
            if (iFT = 3) then // Single field type
               RESULT := 3
            else
               if (iFT = 6) and (iFS = 4) then // Long field type (Float 4 bytes)
                  RESULT := 4
               else
                  if (iFT = 6) and (iFS = 8) then // Double field type (Float 8 bytes)
                     RESULT := 5
                  else
                     if (iFT = 11) then // DateTime field type
                        RESULT := 6
                     else
                        if (iFT = 7) then // Currency field type
                           RESULT := 7
                        else
                           if (iFT = 5) then // Boolean field type
                              RESULT := 8;
end;

function TIDB_StructureAndPropertiesForm.GetFieldTypeName (sFieldName: AnsiString): AnsiString;
var
   iIndex: Integer;
begin
   iIndex := GetFieldTypeIndex (sFieldName);
   if iIndex < 0 then
      RESULT := ALangManager.GetValue (396) // 396=Unknown type
   else
      RESULT := FieldTypesComboBox.Items[iIndex];
end;

procedure TIDB_StructureAndPropertiesForm.SwitchToEditMode;
begin
   SG.Options := SG.Options + [goEditing];
end;

procedure TIDB_StructureAndPropertiesForm.SwitchToBrowseMode;
begin
   SG.Options := SG.Options - [goEditing];
   if SG.InplaceEditor <> nil then
      SG.HideEditor;
end;

procedure TIDB_StructureAndPropertiesForm.SetGeoTextFieldName (sFieldName: AnsiString);
var
   iI: Integer;
   sUFN: AnsiString;
begin
   sUFN := AnsiUpperCase (sFieldName);
   for iI := 1 to SG.RowCount - 1 do
      if sUFN = AnsiUpperCase (SG.Cells[iFieldNameCol, iI]) then
         SG.Objects[iGeotextCol, iI] := Pointer (1)
      else
         SG.Objects[iGeotextCol, iI] := Pointer (0);
end;

function TIDB_StructureAndPropertiesForm.GetGeoTextFieldName: AnsiString;
var
   iI: Integer;
begin
   RESULT := SG.Cells[iFieldNameCol, 1]; // ProgisID
   for iI := 1 to SG.RowCount - 1 do
      if Integer (SG.Objects[iGeotextCol, iI]) = 1 then
      begin
         RESULT := SG.Cells[iFieldNameCol, iI];
         EXIT;
      end;
end;

procedure TIDB_StructureAndPropertiesForm.SetMonitoredFieldName (sFieldName: AnsiString);
var
   iI: Integer;
   sUFN: AnsiString;
begin
   sUFN := AnsiUpperCase (sFieldName);
   for iI := 1 to SG.RowCount - 1 do
      if sUFN = AnsiUpperCase (SG.Cells[iFieldNameCol, iI]) then
         SG.Objects[iMonitoredIDCol, iI] := Pointer (1)
      else
         SG.Objects[iMonitoredIDCol, iI] := Pointer (0);
end;

function TIDB_StructureAndPropertiesForm.GetMonitoredFieldName: AnsiString;
var
   iI: Integer;
begin
   RESULT := SG.Cells[iFieldNameCol, 1]; // ProgisID
   for iI := 1 to SG.RowCount - 1 do
      if Integer (SG.Objects[iMonitoredIDCol, iI]) = 1 then
      begin
         RESULT := SG.Cells[iFieldNameCol, iI];
         EXIT;
      end;
end;

procedure TIDB_StructureAndPropertiesForm.SGDeSelectCell (Sender: TObject; ACol, ARow: Integer; var CanDeSelect: Boolean);
var
   sNewFieldName: AnsiString;
   iFieldSize: Integer;
begin
   if not bIShowOnlyFieldsVisibility then
   begin
      if (ACol = iFieldNameCol) and (ARow > 2) then
      begin
// ++ Cadmensky
         if FieldIsCalculatedField (sCurrentFieldName) or
            FieldIsCalculatedField (sNewFieldName) or
            FieldIsLinked (sCurrentFieldName) or
            FieldIsLinked (SG.Cells[iFieldNameCol, ARow]) then
            exit;
// -- Cadmensky
         sNewFieldName := SG.Cells[iFieldNameCol, ARow];

         if (Trim (sNewFieldName) = '') and
            (ARow < SG.RowCount - 1) then
         begin
            CanDeSelect := FALSE;
            // 397=Field name must not be empty
            MessageBox (Application.Handle, PChar (ALangManager.GetValue (397)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
            EXIT;
         end;
         if AnsiUpperCase (sCurrentFieldName) <> AnsiUpperCase (sNewFieldName) then
            if bNewFieldIsBeingAdded then
            begin
               if AStructMan.AddNewField (sNewFieldName) then
                  bNewFieldIsBeingAdded := FALSE
               else
                  CanDeSelect := FALSE;
            end
            else
               if not AStructMan.ChangeFieldName (sCurrentFieldName, sNewFieldName) then
               begin
                  SG.Cells[iFieldNameCol, ARow] := sCurrentFieldName;
                  CanDeSelect := FALSE;
               end;
      end
      else
         if (ACol = iFieldSizeCol) and (Integer (SG.Objects[iFieldTypeCol, ARow]) = 1) then
         begin
            try
               iFieldSize := StrToInt (SG.Cells[iFieldSizeCol, ARow]);
            except
               iFieldSize := 0;
            end;
            if (iFieldSize < 1) or (iFieldSize > 255) then
            begin
               CanDeSelect := FALSE;
               // 398=Field size must be between 1 and 255
               MessageBox (Application.Handle, PChar (ALangManager.GetValue (398)), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);
               EXIT;
            end;
         end;
   end;
end;

procedure TIDB_StructureAndPropertiesForm.SGSelectCell (Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
var
   iI: Integer;
   bFieldIsUsedCalcField: Boolean;
   bFieldIsNotUsedCalcField: boolean; // Cadmensky
   bFieldIsLinkedField: boolean; // Cadmensky
   ComboBoxRect: TRect;
   iFieldType, iFieldSize: integer;
   ALayerManager: TIDB_LayerManager;
//   sFieldName: AnsiString;
begin
   iCurrentCol := ACol;
   iCurrentRow := ARow;
   if ARow > 2 then
      sCurrentFieldName := SG.Cells[iFieldNameCol, ARow];

   SwitchToBrowseMode;
   FieldTypesComboBox.Visible := FALSE;
   bFieldIsUsedCalcField := (Integer (SG.Objects[iFieldNameCol, ARow]) = 0);
   // ++ Cadmensky
   bFieldIsNotUsedCalcField := (Integer (SG.Objects[iFieldNameCol, ARow]) = 2);
   bFieldIsLinkedField := FieldIsLinked (SG.Cells[iFieldNameCol, ARow]);

   if bFieldIsNotUsedCalcField and (ACol = iFieldTypeCol) then
   begin
      SG.Objects[iFieldNameCol, ARow] := Pointer (0);
      FieldTypesComboBox.Visible := false;
      SELF.SetModified;
      if GetCalcFieldTypeAndSize (SG.Cells[iFieldNameCol, ARow], iFieldType, iFieldSize) then
      begin
         ALayerManager := AnIDBMainMan.GetProjectLayerManager (SELF.AProject);
         if ALayerManager <> nil then
            begin
            ALayerManager.SetCalcFieldIsUsed (GetLayerPointerByIndexFunc (SELF.AProject, SELF.ilayerIndex),
               SG.Cells[iFieldNameCol, ARow],
               true);
            //wai hier visible auch einschalten!
            SG.Objects[IFieldVisibleCol,ARow]:=Pointer(1);
            end;
         AStructMan.AddNewField (SG.Cells[iFieldNameCol, ARow], iFieldType, iFieldSize);
         SELF.bNeedToFillCalculatedFields := true;
      end;
      SELF.SG.Invalidate;
      exit;
   end;

   if bFieldIsUsedCalcField and (ACol = iFieldTypeCol) then
   begin
      SG.Objects[iFieldNameCol, ARow] := Pointer (2);
{      SG.Objects[iFieldVisibleCol, ARow] := Pointer (0);
      SG.Objects[iInfoVisibleCol, ARow] := Pointer (0);}
      FieldTypesComboBox.Visible := false;
      ALayerManager := AnIDBMainMan.GetProjectLayerManager (SELF.AProject);
      if ALayerManager <> nil then
         ALayerManager.SetCalcFieldIsUsed (GetLayerPointerByIndexFunc (SELF.AProject, SELF.ilayerIndex),
            SG.Cells[iFieldNameCol, ARow],
            false);
      SELF.SetModified;
      AStructMan.DeleteField (SG.Cells[iFieldNameCol, ARow]);
      SELF.SG.Invalidate;
      exit;
   end;
   // -- Cadmensky
   if (bFieldIsNotUsedCalcField) and
      (ACol <> iFieldVisibleCol) and
      (ACol <> iInfoVisibleCol) then
   begin
      SG.Invalidate;
      EXIT;
   end;
   if (not bFieldIsUsedCalcField) and
      (not bFieldIsNotUsedCalcField) and
      (not bFieldIsLinkedField) and
      (ARow > 2) and
      (ARow < SG.RowCount - 1) and
      (not bIShowOnlyFieldsVisibility) then
   begin
      ComboBoxRect := SELF.SG.CellRect (iFieldTypeCol, ARow);
      FieldTypesComboBox.Left := SG.Left + ComboBoxRect.Left + 1;
      FieldTypesComboBox.Top := SG.Top + ComboBoxRect.Top + 1;
      FieldTypesComboBox.Width := ComboBoxRect.Right - ComboBoxRect.Left + 1;
      FieldTypesComboBox.Height := ComboBoxRect.Bottom - ComboBoxRect.Top;
      FieldTypesComboBox.Visible := TRUE;
   end;
   // Geotext and Monitored field
   if (not bFieldIsUsedCalcField) and
      (not bFieldIsNotUsedCalcField) and
      (not bFieldIsLinkedField) and
      (ACol = iGeotextCol) or (ACol = iMonitoredIDCol) then
   begin
      if Trim (SG.Cells[iFieldNameCol, ARow]) <> '' then
      begin
         // Only if the name of the field exists. It could not exists for the last line of the grid.
         for iI := 0 to SG.RowCount - 1 do
            SG.Objects[ACol, iI] := Pointer (0);
         SG.Objects[ACol, ARow] := Pointer (1);
         SELF.SetModified;
      end;
   end
   else
{      if not bFieldIsNotUsedCalcField then
      begin}
      // Field visible state
         if (ACol = iFieldVisibleCol) then
         begin
            if not bInitializationSettings then
            begin
               if Integer (SG.Objects[ACol, ARow]) = 0 then
                  SG.Objects[ACol, ARow] := Pointer (1)
               else
                  SG.Objects[ACol, ARow] := Pointer (0);
               SELF.SetModified;
            end;
         end
         else
         // Field indexed state
            if (ARow > 2) and
               (ACol = iFieldIndexCol) and
               (not bFieldIsLinkedField) and
               (not bFieldIsUsedCalcField) and
               (not bFieldIsNotUsedCalcField) then
            begin
               if Integer (SG.Objects[ACol, ARow]) = 0 then
                  SG.Objects[ACol, ARow] := Pointer (1)
               else
                  SG.Objects[ACol, ARow] := Pointer (0);
               AStructMan.ChangeFieldIndex (SG.Cells[iFieldNameCol, ARow], Integer (SG.Objects[iFieldIndexCol, ARow]) <> 0);
            //                          ^^^ The field name.            ^^^ The field should be indexed.
               SELF.SetModified;
            end;
//      end;

   if ARow <= 2 then
   begin
      // The first row with 'ProgisID' field cannot be selected because this field type, name, size or index state
      // cannot be changed by the user.
      SG.Row := 3;
      CanSelect := FALSE;
   end
   else
   begin
      FieldTypesComboBox.ItemIndex := GetFieldTypeIndex (SG.Cells[iFieldNameCol, ARow]);
      if (not bFieldIsUsedCalcField) and
         (not bFieldIsNotUsedCalcField) and
         (not bFieldIsLinkedField) and
         (not bIShowOnlyFieldsVisibility) and
         ((ACol = iFieldNameCol) or ((ACol = iFieldSizeCol) and (Integer (SG.Objects[iFieldTypeCol, ARow]) = 1))) then
         SwitchToEditMode;
      // GeoText and MonitoredID columns can have the only row in selected state.
   end;

   // ++ Cadmensky
   if ACol = iPolygonOverlayCol then
   begin
      if Integer (SG.Objects[ACol, ARow]) >= 2 then
         SG.Objects[ACol, ARow] := Pointer (0)
      else
         SG.Objects[ACol, ARow] := Pointer (Integer (SG.Objects[ACol, ARow]) + 1);
      SELF.SetModified;
   end;

   (*brovak*)
   if (ACol = iAnnotCol) or (ACol = iChartCol) then
   begin
      if Integer (SG.Objects[ACol, ARow]) = 0 then
         SG.Objects[ACol, ARow] := Pointer (1)
      else
         SG.Objects[ACol, ARow] := Pointer (0);
      SELF.SetModified;
   end;
     (*brovak*)

   if ACol = iInfoVisibleCol then
   begin
      if Integer (SG.Objects[ACol, ARow]) > 0 then
         SG.Objects[ACol, ARow] := Pointer (0)
      else
         SG.Objects[ACol, ARow] := Pointer (1);
      SELF.SetModified;
   end;
   // -- Cadmensky

   SG.Invalidate;
end;

procedure TIDB_StructureAndPropertiesForm.MakeNewFieldAdding;
var
   iI: Integer;
begin
   bNewFieldIsBeingAdded := TRUE;
   SG.Row := SG.RowCount - 1;
   SG.RowCount := SG.RowCount + 1;
   SG.Objects[iFieldNameCol, SG.RowCount - 1] := Pointer (1); // Field kind - data field, not a calculated field
   for iI := 0 to SG.ColCount - 1 do
      SG.Cells[iI, SG.RowCount - 1] := '';
   SG.Objects[iFieldNameCol, SG.Row] := Pointer (1); // Field kind - data field, not a calculated field
   SG.Objects[iFieldTypeCol, SG.Row] := Pointer (1); // Field type - String
   FieldTypesComboBox.ItemIndex := 0;
   SG.Cells[iFieldSizeCol, SG.Row] := '50'; // Field size = 50
   SG.Objects[iFieldVisibleCol, SG.Row] := Pointer (1); // Field is visible by default
   SG.Objects[iInfoVisibleCol, SG.Row] := Pointer (1); // Field is visible by default
end;

procedure TIDB_StructureAndPropertiesForm.MakeFieldDeleting;
var
   iI, iP: Integer;
begin
   iP := SG.Row;
   for iI := 0 to SG.ColCount - 1 do
      SG.Cells[iI, SG.RowCount - 1] := '';
   SwitchToBrowseMode;
   SG.DeleteRow (SG.Row);
   if iP < SG.RowCount - 1 then
      SG.Row := iP
   else
      SG.Row := SG.RowCount - 1;
   FieldTypesComboBox.ItemIndex := GetFieldTypeIndex (SG.Cells[iFieldNameCol, SG.Row]);
   SG.Cells[iFieldSizeCol, SG.RowCount - 1] := '';
   SG.HideEditor;
   SG.Selectcell (SG.Col, SG.Row);
   SG.Invalidate;
end;

procedure TIDB_StructureAndPropertiesForm.SGSetEditText (Sender: TObject; ACol, ARow: Integer; const Value: string);
begin
   if not bIShowOnlyFieldsVisibility then
   begin
      if (ARow = SG.RowCount - 1) then
      begin
         if (Trim (Value) <> '') then
         begin
            MakeNewFieldAdding;
            SG.Invalidate;
         end;
      end
      else
         if bNewFieldIsBeingAdded then
         begin
            if (Trim (Value) = '') then
            begin
               bNewFieldIsBeingAdded := FALSE;
               MakeFieldDeleting;
               SG.Invalidate;
            end
            else
               SELF.SetModified;
         end
         else
         begin
            if (ACol = iFieldSizeCol) and (Integer (SG.Objects[iFieldTypeCol, ARow]) = 1) then
            begin
               try
                  AStructMan.ChangeFieldSize (SG.Cells[iFieldNameCol, ARow], StrToInt (Value));
                  SELF.SetModified;
               except
               end;
            end;
            if (ACol = iFieldNameCol) {and (Integer (SG.Objects[iFieldTypeCol, ARow]) = 1)} then
            begin
               try
                  iCurrentCol := ACol;
                  AStructMan.SetModified;
                  SELF.SetModified;
               except
               end;
            end;
         end;
   end;
end;

procedure TIDB_StructureAndPropertiesForm.AddFieldPopUpClick (Sender: TObject);
var i,n,c : integer;
    sn : string;
    e : boolean;
begin
   // 122=New_field
   e:=False;
   sn:=ALangManager.GetValue (122);
   for n:=1 to 9 do
    begin
     for i:= 0 to SG.RowCount-1 do  if SG.Cells[iFieldNameCol,i]=sn then
       begin
       c:=1+ strtointdef(sn[length(sn)],-1);
       if c=0 then sn:=sn+'_1' else sn[length(sn)]:= char(c+48);
       e:=True;
       break;
       end;
     if not e then break;
     e:=False;
    end;
   SG.Cells[iFieldNameCol, SG.RowCount - 1] := sn;
   AStructMan.AddNewField (sn, 1, 50);
   MakeNewFieldAdding;
end;

procedure TIDB_StructureAndPropertiesForm.DeleteFieldPopUpClick (Sender: TObject);
begin
   // 121=do you want to delete field '%s'?
   if MessageBox (Handle, PChar (Format (ALangManager.GetValue (121), [SG.Cells[iFieldNameCol, SG.Row]])), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrYes then
   begin
{      if Integer (SG.Objects[iFieldNameCol, SG.Row]) = 2 then
         DeleteUserField (SG.Cells[iFieldNameCol, SG.Row]);}
      AStructMan.DeleteField (SG.Cells[iFieldNameCol, SG.Row]);
      MakeFieldDeleting;
      SG.Invalidate;
// ++ Cadmensky Version 2.3.5
      SELF.SetModified;
// -- Cadmensky Version 2.3.5
   end;
end;

procedure TIDB_StructureAndPropertiesForm.PopupMenu1Popup (Sender: TObject);
var
   bF: Boolean;
begin
   bf := (SG.Col = iFieldNameCol) and (Clipboard.HasFormat (CF_TEXT));
   CopyPopUp.Enabled := bF;
   CutPopUp.Enabled := bF;
   PastePopUp.Enabled := bF;

   bF := (AnsiUpperCase (SG.Cells[iFieldNameCol, SG.Row]) <> 'PROGISID') and
         (AnsiUpperCase (SG.Cells[iFieldNameCol, SG.Row]) <> 'GUID') and
         (Integer (SG.Objects[iFieldNameCol, SG.Row]) = 1) and
         not FieldIsLinked (SG.Cells[iFieldNameCol, SG.Row]) and
         not FieldIsPresettedTypeField (SG.Cells[iFieldNameCol, SG.Row]);
   DeleteFieldPopUp.Enabled := bF;
end;

procedure TIDB_StructureAndPropertiesForm.CopyPopUpClick (Sender: TObject);
begin
   if SG.InplaceEditor <> nil then
      SendMessage (SG.InplaceEditor.Handle, WM_COPY, 0, 0);
end;

procedure TIDB_StructureAndPropertiesForm.CutPopUpClick (Sender: TObject);
begin
   if SG.InplaceEditor <> nil then
      SendMessage (SG.InplaceEditor.Handle, WM_CUT, 0, 0);
end;

procedure TIDB_StructureAndPropertiesForm.PastePopUpClick (Sender: TObject);
begin
   if SG.InplaceEditor <> nil then
      SendMessage (SG.InplaceEditor.Handle, WM_PASTE, 0, 0);
end;

procedure TIDB_StructureAndPropertiesForm.SGRowMoved (Sender: TObject; FromIndex, ToIndex: Integer);
begin
 //  SELF.SetModified;
end;

procedure TIDB_StructureAndPropertiesForm.AnnotationsTSShow (Sender: TObject);
//var
//   iWidth: Integer;
begin
(*   iWidth := (AnnotationsTS.ClientWidth - AnnotationButtonsPanel.Width) div 2;
   AnnotAvailableListBox.Width := iWidth;
   AnnotSelectedListBox.Width := iWidth;
   Label8.Left := AnnotAvailableListBox.Left + 10;
   Label1.Left := AnnotSelectedListBox.Left + 10;

   if AnnotAvailableListBox.ItemIndex < 0 then AnnotAvailableListBox.ItemIndex := 0;
   if AnnotSelectedListBox.ItemIndex < 0 then AnnotSelectedListBox.ItemIndex := 0;
*)
end;

procedure TIDB_StructureAndPropertiesForm.AnnotationsTSResize (Sender: TObject);
begin
   AnnotationsTSShow (SENDER);
end;

procedure TIDB_StructureAndPropertiesForm.SetAnnotationsSourceFields (sFieldNames: AnsiString);
(*var
   AList: TStringList;
   iI, iP: Integer;
   sFieldName: AnsiString;
begin
   AList := TStringList.Create;
   try
      AList.Text := sFieldNames;
      for iI := 0 to AList.Count - 1 do
      begin
         sFieldName := AList[iI];
         iP := AnnotAvailableListBox.Items.IndexOf (sFieldName);
         if iP > -1 then
         begin
            AnnotAvailableListBox.Items.Delete (iP);
            AnnotSelectedListBox.Items.Add (sFieldName);
         end;
      end;
   finally
      AList.Clear;
      AList.Free;
   end;*)
var
   AList: TStringList;
   iI, iJ {, iP}: Integer;
   sFieldName: AnsiString;
begin
   AList := TStringList.Create;
   try
      AList.Text := sFieldNames;
      for iI := 0 to AList.Count - 1 do
      begin
         sFieldName := AList[iI];
         for iJ := 1 to SG.RowCount - 1 do
            if AnsiUpperCase (sFieldName) = AnsiUpperCase (SG.Cells[iFieldNameCol, iJ]) then
            begin
               SG.Objects[iAnnotCol, iJ] := Pointer (1);
               break;
            end;
      end;
   finally
      AList.Clear;
      AList.Free;
   end;
end;

{brovak}

procedure TIDB_StructureAndPropertiesForm.SetChartSourceFields (sFieldNames: AnsiString);
(*var
   AList: TStringList;
   iI, iP: Integer;
   sFieldName: AnsiString;
begin
   AList := TStringList.Create;
   try
      AList.Text := sFieldNames;
      for iI := 0 to AList.Count - 1 do
      begin
         sFieldName := AList[iI];
         iP := ChartAvailableListBox.Items.IndexOf (sFieldName);
         if iP > -1 then
         begin
            ChartAvailableListBox.Items.Delete (iP);
            ChartSelectedListBox.Items.Add (sFieldName);
         end;
      end;
   finally
      AList.Clear;
      AList.Free;
   end;
*)
var
   AList: TStringList;
   iI, iJ {, iP}: Integer;
   sFieldName: AnsiString;
begin
   AList := TStringList.Create;
   try
      AList.Text := sFieldNames;
      for iI := 0 to AList.Count - 1 do
      begin
         sFieldName := AList[iI];
         for iJ := 1 to SG.RowCount - 1 do
            if AnsiUpperCase (sFieldName) = AnsiUpperCase (SG.Cells[iFieldNameCol, iJ]) then
            begin
               SG.Objects[iChartCol, iJ] := Pointer (1);
               break;
            end;
      end;
   finally
      AList.Clear;
      AList.Free;
   end;
end;

(*function TIDB_StructureAndPropertiesForm.GetChartSourceFields: AnsiString;
begin
   RESULT := ChartSelectedListBox.Items.Text;
end;*)
{brovak}

// ++ Cadmensky

procedure TIDB_StructureAndPropertiesForm.SetOverlaySourceFields (sFieldNames: AnsiString);
var
   AList: TStringList;
   iI, iJ {, iP}: Integer;
   sFieldName: AnsiString;
   bFieldIsProportion: boolean;
begin
   AList := TStringList.Create;
   try
      AList.Text := sFieldNames;
      for iI := 0 to AList.Count - 1 do
      begin
         sFieldName := AList[iI];
         bFieldIsProportion := Pos ('/', sFieldName) > 0;
         if bFieldIsProportion then
            sFieldName := Trim (Copy (sFieldName, 1, Pos ('/', sFieldName) - 1));
         for iJ := 1 to SG.RowCount - 1 do
            if AnsiUpperCase (sFieldName) = AnsiUpperCase (SG.Cells[iFieldNameCol, iJ]) then
            begin
               if bFieldIsProportion then
                  SG.Objects[iPolygonOverlayCol, iJ] := Pointer (2)
               else
                  SG.Objects[iPolygonOverlayCol, iJ] := Pointer (1);
               break;
            end;
      end;
   finally
      AList.Clear;
      AList.Free;
   end;
end;

function TIDB_StructureAndPropertiesForm.GetOverlaySourceFields: AnsiString;
var
   i: integer;
   AList: TStringList;
begin
   AList := TStringList.Create;
   for i := 1 to SG.RowCount - 1 do
   begin
      if Integer (SG.Objects[iPolygonOverlayCol, i]) > 0 then
      begin
         if Integer (SG.Objects[iPolygonOverlayCol, i]) = 1 then
            AList.Add (SG.Cells[iFieldNameCol, i])
         else
            AList.Add (SG.Cells[iFieldNameCol, i] + ' / ' + cs_CalcFieldAreaName);
      end;
   end;
   RESULT := AList.Text;
   AList.Free;
end;
{brovak}

function TIDB_StructureAndPropertiesForm.GetAnnotationsSourceFields: AnsiString;
var
   i: integer;
   AList: TStringList;
begin
   AList := TStringList.Create;
   for i := 1 to SG.RowCount - 1 do
   begin
      if Integer (SG.Objects[iAnnotCol, i]) > 0 then
      begin
         if Integer (SG.Objects[iAnnotCol, i]) = 1 then
            AList.Add (SG.Cells[iFieldNameCol, i])
   //      else
     //       AList.Add (SG.Cells[iFieldNameCol, i] + ' / ' + cs_CalcFieldAreaName);
      end;
   end;
   RESULT := AList.Text;
   AList.Free;
end;

function TIDB_StructureAndPropertiesForm.GetChartSourceFields: AnsiString;
var
   i: integer;
   AList: TStringList;
begin
   AList := TStringList.Create;
   for i := 1 to SG.RowCount - 1 do
   begin
      if Integer (SG.Objects[iChartCol, i]) > 0 then
      begin
         if Integer (SG.Objects[iChartCol, i]) = 1 then
            AList.Add (SG.Cells[iFieldNameCol, i])
//         else
 //           AList.Add (SG.Cells[iFieldNameCol, i] + ' / ' + cs_CalcFieldAreaName);
      end;
   end;
   RESULT := AList.Text;
   AList.Free;
end;
{brovak}

procedure TIDB_StructureAndPropertiesForm.SetInfoSourceFields (sFieldNames: AnsiString);
var
   AList: TStringList;
   iI, iJ {, iP}: Integer;
   sFieldName: AnsiString;
begin
   AList := TStringList.Create;
   try
      AList.Text := sFieldNames;
      for iI := 0 to AList.Count - 1 do
      begin
         sFieldName := AList[iI];
         for iJ := 1 to SG.RowCount - 1 do
            if AnsiUpperCase (sFieldName) = AnsiUpperCase (SG.Cells[iFieldNameCol, iJ]) then
            begin
               SG.Objects[iInfoVisibleCol, iJ] := Pointer (1);
               break;
            end;
      end;
   finally
      AList.Clear;
      AList.Free;
   end;
end;

function TIDB_StructureAndPropertiesForm.GetInfoSourceFields: AnsiString;
var
   i: integer;
   AList: TStringList;
begin
   AList := TStringList.Create;
   for i := 1 to SG.RowCount - 1 do
      if Integer (SG.Objects[iInfoVisibleCol, i]) > 0 then
         AList.Add (SG.Cells[iFieldNameCol, i]);
   RESULT := AList.Text;
   AList.Free;
end;
// -- Cadmensky

(*function TIDB_StructureAndPropertiesForm.GetAnnotationsSourceFields: AnsiString;
begin
   RESULT := AnnotSelectedListBox.Items.Text;
end; *)

procedure TIDB_StructureAndPropertiesForm.AnnotAvailableListBoxDblClick (Sender: TObject);
begin
//   AnnotAddOneSB.Click;
end;

procedure TIDB_StructureAndPropertiesForm.AnnotSelectedListBoxDblClick (Sender: TObject);
begin
//   AnnotRemoveOneSB.Click;
end;

procedure TIDB_StructureAndPropertiesForm.AnnotAddOneSBClick (Sender: TObject);
begin
(*   AddOneValue (AnnotAvailableListBox, AnnotSelectedListBox);
   SetModified;*)
end;

procedure TIDB_StructureAndPropertiesForm.AnnotAddAllSBClick (Sender: TObject);
begin
(*   AddAllValues (AnnotAvailableListBox, AnnotSelectedListBox);
   SetModified;*)
end;

procedure TIDB_StructureAndPropertiesForm.AnnotRemoveOneSBClick (Sender: TObject);
begin
(*   AddOneValue (AnnotSelectedListBox, AnnotAvailableListBox);
   SetModified;*)
end;

procedure TIDB_StructureAndPropertiesForm.AnnotRemoveAllSBClick (Sender: TObject);
begin
(*   AddAllValues (AnnotSelectedListBox, AnnotAvailableListBox);
   SetModified;*)
end;

procedure TIDB_StructureAndPropertiesForm.AnnotUpSBClick (Sender: TObject);
//var
//   sTemp: AnsiString;
begin
(*   if AnnotSelectedListBox.ItemIndex > -1 then
   begin
      sTemp := AnnotSelectedListBox.Items[AnnotSelectedListBox.ItemIndex - 1];
      AnnotSelectedListBox.Items[AnnotSelectedListBox.ItemIndex - 1] := AnnotSelectedListBox.Items[AnnotSelectedListBox.ItemIndex];
      AnnotSelectedListBox.Items[AnnotSelectedListBox.ItemIndex] := sTemp;
      AnnotSelectedListBox.ItemIndex := AnnotSelectedListBox.ItemIndex - 1;
   end;
   *)
end;

procedure TIDB_StructureAndPropertiesForm.AnnotDownSBClick (Sender: TObject);
//var
//   sTemp: AnsiString;
begin
(*   if AnnotSelectedListBox.ItemIndex < AnnotSelectedListBox.Items.Count - 1 then
   begin
      sTemp := AnnotSelectedListBox.Items[AnnotSelectedListBox.ItemIndex + 1];
      AnnotSelectedListBox.Items[AnnotSelectedListBox.ItemIndex + 1] := AnnotSelectedListBox.Items[AnnotSelectedListBox.ItemIndex];
      AnnotSelectedListBox.Items[AnnotSelectedListBox.ItemIndex] := sTemp;
      AnnotSelectedListBox.ItemIndex := AnnotSelectedListBox.ItemIndex + 1;
   end;
   *)
end;

{procedure TIDB_StructureAndPropertiesForm.SetUserCalcFieldProperties (sFieldName: AnsiString);
var
   pUFP: TIDB_UserFieldPropsForm;
   ALayerMan: TIDB_LayerManager;
   ALayer: Pointer;
//   AnUserFieldsMan: TIDB_UserFieldsManager;
   AnExtTablesMan: TIDB_ExternalTablesMan;
   i: integer;
   bF: boolean;
   sField: AnsiString;
begin
   ALayerMan := AnIDBMainMan.GetProjectLayerManager (Self.AProject);
   ALayer := GetLayerPointerByIndexFunc (SELF.AProject, iLayerIndex);

//   AnUserFieldsMan := ALayerMan.GetLayerUserFieldsMan (ALayer);
   AnExtTablesMan := ALayerMan.GetLayerExtTablesMan (ALayer);

   bF := false;

   for i := 0 to AnUserFieldsMan.FieldsCount - 1 do
      if AnUserFieldsMan.GetUserCalcFieldName (i) = sFieldName then
      begin
         pUFP := TIDB_UserFieldPropsForm.Create (AProject, AnUserFieldsMan, AnExtTablesMan, ALayer, i);
         bF := true;
      end;

   if not bF then
   begin
      AnUserFieldsMan.AddUserCalcField (sFieldName, '');
      pUFP := TIDB_UserFieldPropsForm.Create (AProject, AnUserFieldsMan, AnExtTablesMan, ALayer, AnUserFieldsMan.FieldsCount - 1);
   end;

   //   for i := 1 to SG.RowCount - 2 do
   for i := 0 to SELF.AStructMan.GetFieldCount - 1 do
   begin
      //      sTestFieldName := SG.Cells [iFieldNameCol, i];
      sField := SELF.AStructMan.GetFieldName (i);
      //      if (not AnUserFieldsMan.FieldIsUserDefined (sTestFieldName)) and (Pos ('_', sTestFieldName) <> 1) then
      if AnsiUpperCase (sField) <> 'PROGISID' then
         case SELF.AStructMan.GetFieldType (sField) of
            2, 3, 4, 7, 12, 25, 6: pUFP.cbAvailableFields.Items.Add (sField);
         end;
   end;

   pUFP.ShowModal;

   if pUFP.ModalResult = mrOk then
   begin
      ALayerMan.RefreshView (ALayer);
      AStructMan.DoChangeStructure;
   end;
end;}

{function TIDB_StructureAndPropertiesForm.AddNewUserField (sFieldName: AnsiString): AnsiString;
var
   sNewFieldName: AnsiString;
begin
   sNewFieldName := sFieldName;
   Result := '';
   if not InputQuery (cs_ApplicationName, ALangManager.GetValue (416), sNewFieldName) then
      exit;
   if Trim (sNewFieldName) = '' then
      exit;
   SetUserCalcFieldProperties (sNewFieldName);
   Result := sNewFieldName;
end;}

{procedure TIDB_StructureAndPropertiesForm.DeleteUserField (sFieldName: AnsiString);
var
   ALayerMan: TIDB_LayerManager;
   ALayer: Pointer;
   AnUserFieldsMan: TIDB_UserFieldsManager;
   //   AnExtTablesMan: TIDB_ExternalTablesMan;
begin
   ALayerMan := AnIDBMainMan.GetProjectLayerManager (Self.AProject);
   ALayer := GetLayerPointerByIndexFunc (SELF.AProject, iLayerIndex);

   AnUserFieldsMan := ALayerMan.GetLayerUserFieldsMan (ALayer);
   AnUserFieldsMan.DeleteUserCalcField (sFieldName);
   ALayerMan.RefreshView (ALayer);
end;}

// ++ Cadmensky

procedure TIDB_StructureAndPropertiesForm.SpeedButton1Click (Sender: TObject);
begin
end;

// -- Cadmensky

procedure TIDB_StructureAndPropertiesForm.ChartAddOneSBClick (
   Sender: TObject);
begin
(*   AddOneValue (ChartAvailableListBox, ChartSelectedListBox);
   SetModified;
   *)
end;

procedure TIDB_StructureAndPropertiesForm.ChartAddAllSBClick (
   Sender: TObject);
begin
(*   AddAllValues (ChartAvailableListBox, ChartSelectedListBox);
   SetModified;
   *)
end;

procedure TIDB_StructureAndPropertiesForm.ChartRemoveOneSBClick (
   Sender: TObject);
begin
(*   AddOneValue (ChartSelectedListBox, ChartAvailableListBox);
   SetModified;
   *)
end;

procedure TIDB_StructureAndPropertiesForm.ChartRemoveAllSBClick (
   Sender: TObject);
begin
(*   AddAllValues (ChartSelectedListBox, ChartAvailableListBox);
   SetModified;
   *)
end;

procedure TIDB_StructureAndPropertiesForm.ChartAvailableListBoxDblClick (
   Sender: TObject);
begin
//   ChartAddOneSB.Click;
end;

procedure TIDB_StructureAndPropertiesForm.ChartSelectedListBoxDblClick (
   Sender: TObject);
begin
//   ChartRemoveOneSB.Click;
end;

procedure TIDB_StructureAndPropertiesForm.ChartsTSShow (Sender: TObject);
//var
//   iWidth: Integer;
begin
(*   iWidth := (ChartsTS.ClientWidth - ChartButtonsPanel.Width) div 2;
   ChartAvailableListBox.Width := iWidth;
   ChartSelectedListBox.Width := iWidth;
   Label2.Left := ChartAvailableListBox.Left + 10;
   Label3.Left := ChartSelectedListBox.Left + 10;

   if ChartAvailableListBox.ItemIndex < 0 then ChartAvailableListBox.ItemIndex := 0;
   if ChartSelectedListBox.ItemIndex < 0 then ChartSelectedListBox.ItemIndex := 0;
*)
end;

procedure TIDB_StructureAndPropertiesForm.ChartUpSBClick (Sender: TObject);
//var
//   sTemp: AnsiString;
begin
(*   if ChartSelectedListBox.ItemIndex > -1 then
   begin
      sTemp := ChartSelectedListBox.Items[ChartSelectedListBox.ItemIndex - 1];
      ChartSelectedListBox.Items[ChartSelectedListBox.ItemIndex - 1] := ChartSelectedListBox.Items[ChartSelectedListBox.ItemIndex];
      ChartSelectedListBox.Items[ChartSelectedListBox.ItemIndex] := sTemp;
      ChartSelectedListBox.ItemIndex := ChartSelectedListBox.ItemIndex - 1;
   end;
*)
end;

procedure TIDB_StructureAndPropertiesForm.ChartDownSBUpSBClick (Sender: TObject);
//var
//   sTemp: AnsiString;
begin
(*   if ChartSelectedListBox.ItemIndex < ChartSelectedListBox.Items.Count - 1 then
   begin
      sTemp := ChartSelectedListBox.Items[ChartSelectedListBox.ItemIndex + 1];
      ChartSelectedListBox.Items[ChartSelectedListBox.ItemIndex + 1] := ChartSelectedListBox.Items[ChartSelectedListBox.ItemIndex];
      ChartSelectedListBox.Items[ChartSelectedListBox.ItemIndex] := sTemp;
      ChartSelectedListBox.ItemIndex := ChartSelectedListBox.ItemIndex + 1;
   end;
*)
end;

procedure TIDB_StructureAndPropertiesForm.ChartsTSResize (Sender: TObject);
begin
   ChartsTSShow (SENDER);
end;

procedure TIDB_StructureAndPropertiesForm.OKButtonClick (Sender: TObject);
var
   b: boolean;
begin
   if SG.Col = iFieldNameCol then
      SGDeSelectCell (Sender, iCurrentCol, iCurrentRow, b);
end;

procedure TIDB_StructureAndPropertiesForm.CancelButtonClick (Sender: TObject);
begin
   SELF.bModified := false;
end;

procedure TIDB_StructureAndPropertiesForm.FormActivate (Sender: TObject);
var
   iI: Integer;
   sFieldName: AnsiString;
   iFieldType,
      iFieldSize: Integer;
   //   ACheckBox: TCheckBox;
begin
   try
      // Commented by Cadmensky
      {        SG.Row := 2;
              if bIShowOnlyFieldsVisibility then
                 SG.Col := iFieldVisibleCol
              else
                 SG.Col := iFieldTypeCol;}
      FieldTypesComboBox.ItemIndex := GetFieldTypeIndex (SG.Cells[iFieldNameCol, SG.Row]);
      sCurrentFieldName := SG.Cells[iFieldNameCol, SG.Row];
      for iI := 1 to SG.RowCount - 1 do
      begin
         sFieldName := SG.Cells[iFieldNameCol, iI];
         if not FieldIsCalculatedField (sFieldName) then
         begin
            iFieldType := AStructMan.GetFieldType (sFieldName);
            if iFieldType = 1 then
            begin
               iFieldSize := AStructMan.GetFieldSize (sFieldName);
               try
                  SG.Cells[iFieldSizeCol, iI] := IntToStr (iFieldSize);
               except
               end;
               SG.Objects[iFieldSizeCol, iI] := Pointer (iFieldSize);
            end
            else
               SG.Objects[iFieldSizeCol, iI] := Pointer (0);
            // Field type
            SG.Objects[iFieldTypeCol, iI] := Pointer (iFieldType);
            // Indexed
         end;
         if AStructMan.GetFieldIndexIsPresent (sFieldName) then
            SG.Objects[iFieldIndexCol, iI] := Pointer (1)
         else
            SG.Objects[iFieldIndexCol, iI] := Pointer (0);
      end; // for iI end
      if not bIShowOnlyFieldsVisibility then
         // Try to avoid the situation when the buttons will be placed over CheckBox on the bottom panel.
         ApplyToAllCB.Width := BottomPanel.GetCanvas.TextWidth (ApplyToAllCB.Caption) + ApplyToAllCB.Height
      else
         begin
         ApplyToAllCB.Visible := FALSE;
         IsTemplateCheckBox.Visible:= False;
         end;
      OKButton.Left := SELF.ClientWidth - OKButton.Width - CancelButton.Width - 10;
      CancelButton.Left := OKButton.Left + OKButton.Width + 5;

      if not bIShowOnlyFieldsVisibility then
      begin
         while OKButton.Left < ApplyToAllCB.Width + ApplyToAllCB.Left + 5 do
            SELF.Width := SELF.Width + 5;
         // Show the CheckBox on the bottom panel if only there is more than one layer in the project. As the function will
         //    return all layer count in the project including the layer of selected object, I check number 2.
         ApplyToAllCB.Enabled := GetProjectLayerNumberFunc (SELF.AProject) > 2;
      end;
   finally
      SELF.bInitializationSettings := FALSE;
      sg.ColWidths[5]:=-1;
      sg.ColWidths[6]:=-1;

   end;
end;

function TIDB_StructureAndPropertiesForm.FieldIsNumberType (sFieldName: AnsiString): Boolean;
var
   iFT: Integer;
begin
   RESULT := FALSE;
 // IFT:=   GetFieldTypeName (SG.Cells[iFieldNameCol, ARow]);
   IFT := AStructMan.GetFieldType (sFieldName);
//   TF := DataQuery.FieldByname (sFieldName).DataType;
//   iFT := Ord (TF); // .DataType; //DataQuery.FieldB  .T .GetFieldType(sFieldName);
   if iFT < 0 then EXIT; // This field was not found in the structure data.
   {
   1 - String
   2 - SmallInt
   3 - Integer
   5 - Boolean
   6 - Float
   7 - Currency
   11 - DateTime
   12 - Byte
   }
   case iFT of
      2..3, 6..7, 12: RESULT := TRUE;
   end
end;

procedure TIDB_StructureAndPropertiesForm.SGTopLeftChanged (Sender: TObject);
var
   ComboBoxRect: TRect;
begin
   if SELF.FieldTypesComboBox.Visible then
   begin
      ComboBoxRect := SELF.SG.CellRect (iFieldTypeCol, SG.Row);
      FieldTypesComboBox.Left := SG.Left + ComboBoxRect.Left + 1;
      FieldTypesComboBox.Top := SG.Top + ComboBoxRect.Top + 1;
      FieldTypesComboBox.Width := ComboBoxRect.Right - ComboBoxRect.Left + 1;
      FieldTypesComboBox.Height := ComboBoxRect.Bottom - ComboBoxRect.Top;
   end;
end;

procedure TIDB_StructureAndPropertiesForm.checkboxbitmap(aCol,
  aRow: Integer; checked : Boolean);
var cbitmap: Ansistring;
begin
if checked then
 begin
 case aCol of
   4 : cbitmap:='VISIBLE';
   6 : cbitmap:='CHECKED';
   7 : cbitmap:='CHECKED';
   8 : cbitmap:='LABEL';
   9 : cbitmap:='CHART';
   10 : cbitmap:='INFO';
   else  cbitmap:='CHECKED';
   end;
 end
 else
 begin
 case aCol of
   4 : cbitmap:='VISIBLEUNCHECKED';
   6 : cbitmap:='UNCHECKED';
   7 : cbitmap:='UNCHECKED';
   8 : cbitmap:='LABELUNCHECKED';
   9 : cbitmap:='CHARTUNCHECKED';
   10 : cbitmap:='INFOUNCHECKED';
   else  cbitmap:='UNCHECKED';
   end;
end;
ACheckBitmap.LoadFromResourceName (HINSTANCE, cbitmap);
end;

procedure TIDB_StructureAndPropertiesForm.IsTemplateCheckBoxClick(
  Sender: TObject);
begin
SELF.SetModified;
end;

procedure TIDB_StructureAndPropertiesForm.ApplyToAllCBClick(
  Sender: TObject);
begin
SELF.SetModified;
end;

end.

