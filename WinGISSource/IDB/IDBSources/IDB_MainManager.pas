unit IDB_MainManager;
{
Internal database. Ver. II.
This module provides the functionality of the main manager of the all objects of IDB.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 30-01-2001
}
interface

uses Classes, Forms, Controls, extctrls,
   Objects, IDB_LayerManager,
   IDB_Access2DB,  IDB_CallbacksDef, //IDB_UndoMan, IDB_RedoMan,
// ++ Cadmensky
   {IDB_InsertRecordsManager,}
// -- Cadmensky
   IDB_FieldsVisibilityMan, IDB_FormPositionMan,
   IDB_PolygonOverlayManager, JRO_TLB, IDB_View, IDB_Monitor;

type
 {  TTimerThread = class(TThread)
   Private
   procedure execute; override;
   end;    }

   PProjectDef = ^TProjectDef;
   TProjectDef = record
      AProject: Pointer;
      AProjectFormHandle: Integer;

      IDB_DA: TIDB_Access2DB_Level2;
      ALayerManager: TIDB_LayerManager;
// ++ Cadmensky Version 2.3.7
//      AMonitoringFormPosManager: TIDB_MonitorFormPositionManager;
      AFormPosManager: TIDB_FormPositionManager;
// -- Cadmensky Version 2.3.7
      APolygonOverlayManager: TIDB_PolygonOverlayManager;

      bThereIsNoNecessityToPutDataIntoUndo: Boolean;
      bIAmGeneratingTheItemsByMyself: Boolean;
      bAnExternalDBIsWorking: Boolean;

      bTheDatabaseFileWas97thFormat: Boolean;
      bProjectFileHasReadOnlyState: Boolean;

// ++ Cadmensky
      bUseNewATTNames: boolean;
      bLastWasAttributeWindow: boolean;
      listLayerNames: TList;
// -- Cadmensky
   end;

   TIDB_MainManager = class
   private
      listProjectDefs: TList;
      bIHaveToBeDisconnected: Boolean;

      // Cadmensky
      //      AnInsertManager: TIDB_InsertRecordsManager;

      AnInfoFormPositionManager: TIDB_InfoFormPositionManager;

      AnOldScreenCursor: TCursor;
      procedure ClearData;
      function FindProject (AProject: Pointer): Integer;
      procedure DeleteProject (iProjectPos: Integer); overload;
      procedure CloseAllWindowsOfProject (iProjectPos: Integer); overload;
      procedure CloseMonitoringWindow (AProject: Pointer);

      function CheckVersionOfProjectAndDBFiles (pPD: PProjectDef): Boolean;
      procedure SyncProjectAndDB (pPD: PProjectDef);
      procedure DeleteTablesOfLayersThatCanBeNotExist (pPD: PProjectDef);
      procedure MakeBakUpOfDatabaseFile (pPD: PProjectDef);
// ++ Commented by Cadmensky Version 2.3.2
//      procedure KillAllExternalTablesPool;
// -- Commented by Cadmensky Version 2.3.2
      procedure MigrateAllInternalTablesIntoHiddenTables (pPD: PProjectDef);

      procedure SetScreenCursor;
      procedure RestoreScreenCursor;
      function RenameATTTablesIfItNeed (AProject: Pointer; IDA: TIDB_Access2DB_Level2): boolean;

      function GetLastWasAttributeWindow (AProject: Pointer): boolean;
      procedure SetLastWasAttributeWindow (AProject: Pointer; bValue: boolean);
// ++ Cadmensky Version 2.2.8
      function GetReadyToCopyAttributeInfo (AProject, ASourceLayer, ADestLayer: Pointer; var bUseSourceFields: Boolean): Boolean;
      function CopyAttributeInfoSuperNew (AProject, ASourceLayer, ADestLayer: Pointer; bUseSourceFields: Boolean): Boolean;
// -- Cadmensky Version 2.2.8
   public
      AViewForm: TIDB_ViewForm;
      AMonitoringForm: TIDB_MonitorForm;
      AInfoForm: TIDB_MonitorForm;
      constructor Create;
      destructor Free;

      // ***************     FROM WINGIS     ***************
      // Project management.
      function AddProject (AProjectFormHandle: Integer; AProject: Pointer; bProjectFileHasReadOnlyState: Boolean): Boolean;
      procedure DeleteProject (AProject: Pointer); overload;
      // The IDB functions
      procedure ShowAttributeWindow (AProject, ALayer, AALayer: Pointer);
      procedure CloseAttributeWindow (AProject, ALayer: Pointer);
      procedure ShowMonitoringWindow (AProject: Pointer; bNeedToCloseViewForm: Boolean = true);
      procedure ShowInfoWindow (AProject: Pointer);
      procedure ClearInfoWindow (AProject: Pointer);
      procedure RefreshAllDataWindows (AProject: Pointer);
      procedure RefreshInfoWindow;
      procedure RefreshMonitoringWindow (AProject: Pointer);
      procedure CloseInfoWindow;
      procedure HideInfoWindow;
      procedure CloseAllWindowsOfProject (AProject: Pointer); overload;
      procedure CloseAllWindowsOfAllProjects;
            
      procedure ProjectIsBeingSaved (AProject: Pointer);
      procedure SaveDatabaseAs (AProject: Pointer; sNewProjectName: AnsiString);
      procedure DeleteProjectAndDatabaseFiles (sProjectFileName: AnsiString);

      procedure DeleteLayerTables (AProject, ALayer: Pointer);
      // By help of this procedure, WinGIS asks ID of all objects that were deleted by the user
      // from DB user inaterface.
      function GetItemIDToBeDeleted_FromIDB (AProject: Pointer; ALayer: Pointer): Integer;
      // These procedures do action of deleting in IDB of objects that were deleted by the user
      // from WinGIS user interface.
      function GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG (AProject: Pointer): Boolean;
      procedure ReceiveIDOfObjectToBeDeleted_FromWG (AProject, ALayer: Pointer; iObjectID: Integer);
      procedure DeleteObjects_FromWG (AProject: Pointer);
      // These procedures do action of monitoring of objects that were selected from WinGIS user interface.
      function GetReadyToReceiveIDsOfObjectsForMonitoring (AProject: Pointer): Boolean;
      procedure ReceiveIDOfObjectForMonitoring (AProject, ALayer: Pointer; iObjectID: Integer);

      function GetReadyToReceiveIDsOfObjectsToBeRestored (AProject: Pointer): Boolean;
      procedure ReceiveIDOfObjectToBeRestored (AProject: Pointer; ALayer: Pointer; iObjectID: Integer);
      procedure UpdateCalcFields (AProject: Pointer; ALayer: Pointer; iObjectID: Integer);
      procedure ExecuteRestoring (AProject: Pointer);

// ++ Commented by Cadmensky Version 2.3.0
{      function GetReadyToReceiveIDsOfObjectsToBeUsedForDragAndDrop (AProject: Pointer): Boolean;
      procedure ReceiveIDOfObjectToBeUsedForDragAndDrop (AProject, ALayer: Pointer; iObjectID: Integer);
      procedure ExecuteDragAndDrop (AProject: Pointer; ASourceGridOfDragging: Pointer);}
// -- Commented by Cadmensky Version 2.3.0

      function GetReadyToReceiveIDsOfObjectsToBeMade (AProject: Pointer): Boolean;
      procedure ReceiveIDOfObjectToBeMade (AProject, ALayer: Pointer; iItemID: Integer);
      procedure MakeAnnotations (AProject: Pointer);
      procedure MakeChart (AProject: Pointer);

{brovak}
      procedure MakeSpecialChart (AProject, Alayer: Pointer);
      procedure SendSpecialforChart (AProject: Pointer; PseudoDDEString: Ansistring);
{brovak}
      function GetReadyToPolygonOverlaying (AProject, ASourceLayer1, ASourceLayer2, ADestinationLayer: Pointer): Boolean;
      procedure EndOfPolygonOverlaying (AProject: Pointer);

      procedure SetGeoTextFieldNames (AProject, ALayer: Pointer; sGeoTextIDFieldName, sGeoTextFieldName: AnsiString);
      function GetGeoTextIDFieldName (AProject, ALayer: Pointer): AnsiString;
      function GetGeoTextFieldName (AProject, ALayer: Pointer): AnsiString;

      function LoadGeoTextData (AProject, ALayer: Pointer; sSourceDBFileName, sSourceFieldName, sDestinationFieldName, sProgisIDFieldName: AnsiString): Boolean;

      function GetTableFieldsNamesList (AProject: Pointer; ALayer: Pointer; bInclude_ProgisID_Field: Boolean = TRUE): AnsiString;
      function GetTableVisibleFieldsNamesList (AProject: Pointer; ALayer: Pointer): AnsiString;

      procedure InsertItem (AProject, ALayer: Pointer; iNewItemIndex: Integer);
// ++ Cadmensky
      procedure OverlayInsertItem (AProject, ALayer: Pointer; iNewItemIndex, iAItemIndex, iBItemIndex: Integer);
// ++ Cadmensky

// ++ Cadmensky Version 2.2.8
      function MoveAttributeInfo (AProject, ADestLayer: Pointer): integer;
// -- Cadmensky Version 2.2.8
      function CopyAttributeInfo (AProject, ASourceLayer, ADestLayer: Pointer; iSourceItemID, iDestItemID: Integer): integer;

      function InsertRecord (AProject, ALayer: Pointer; iItemIndex: Integer): Boolean;
      function GetReadyToInsertRecords (AProject, ALayer: Pointer; ARecordTemplate: TList): Integer;
      //      function InsertRecord(iPosition, iLayerItemID: Integer): Boolean;
// ++ Commented by Cadmensky Version 2.2.8
//      procedure EndInserting (iPosition: Integer);
// -- Commented by Cadmensky Version 2.2.8

      function GetGeoText (AProject: Pointer; iLayerIndex, iItemID: Integer {; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString}): AnsiString;
      function SetGeoText (AProject: Pointer; iLayerIndex, iItemID: Integer; {sGeoTextIDFieldName, sGeoTextValueFieldName,} sGeoTextValue: AnsiString): Boolean;

// ++ Commented by Cadmensky Version 2.3.3
//      procedure CopyData (AProject, ASourceLayer, ADestLayer: Pointer; iItemID: Integer);
// -- Commented by Cadmensky Version 2.3.3

      procedure Set_DoNotAddAttributeRecords_Sign (AProject: Pointer; bValue: Boolean);
      procedure Set_ThereIsNoNecessityToPutDataInUndo_Sign (AProject: Pointer; bValue: Boolean);
      procedure Set_AnExternalDBIsWorking_Sign (AProject: Pointer; bValue: Boolean);

      // AX connection
      function X_GotoObjectRecord (AProject, ALayer: Pointer; iItemIndex: Integer): Boolean;
      function X_AddRecord (AProject, ALayer: Pointer; iItemIndex: Integer): Boolean;
      function X_GetFieldCount (AProject, ALayer: Pointer): Integer;
      function X_GetFieldName (AProject, ALayer: Pointer; iFieldIndex: Integer): AnsiString;
      function X_GetFieldValue (AProject, ALayer: Pointer; sFieldName: AnsiString): Variant; overload;
      function X_GetFieldValue (AProject, ALayer: Pointer; iFieldIndex: Integer): Variant; overload;
      procedure X_SetFieldValue (AProject, ALayer: Pointer; sFieldName: AnsiString; FieldValue: Variant); overload;
      procedure X_SetFieldValue (AProject, ALayer: Pointer; iFieldIndex: Integer; FieldValue: Variant); overload;

      // Annotation service
      procedure UpdateAnnotationData (AProject: Pointer; iLayerIndex, iItemID: Integer; AnAnnotData: PCollection);
      procedure UpdateChartData (AProject: Pointer; iLayerIndex, iItemID: Integer; AnAnnotData: PCollection);


      // ***************     INSIDE     ***************
      procedure Set_IAmGeneratingObjectsByMyself_Sign (AProject: Pointer; bValue: Boolean);
      procedure DetectMonitoringFormState (AProject: Pointer; AForm: TForm; AutoZoomOrSelect: Boolean);
      procedure DetectInfoFormState (AForm: TForm);
// ++ Cadmensky Version 2.3.7
      function GetFormPositionManager (AProject: Pointer): TIDB_FormPositionManager;
// -- Cadmensky Version 2.3.7

// ++ Cadmensky
      function GetProjectLayerManager (AProject: Pointer): TIDB_LayerManager;
      procedure PackDataBase (sProjectFileName: AnsiString);
      function UseNewATTNames (AProject: Pointer): boolean;
      function GetLayerNamesList (AProject: Pointer): TList;
      procedure ShowRecordsForSelectedObjects (AProject: Pointer);
      procedure SaveAnnotationSettings(AProject: Pointer; iLayerindex: Integer; ASettingsList: AnsiString);
      function GetAnnotationSettings(AProject: Pointer; iLayerindex: Integer): AnsiString;
// Facilitymanagement
      function   F_GetLinesPerLayer(AProject: Pointer; Layername: Pchar): Pchar; 
      function   F_GetSymbolsPerLayer(AProject: Pointer; Layername: Pchar): Pchar;
      function   F_Symbolallowed(AProject: Pointer; Symbolname, LineStyleName: Pchar): Pchar;
      {brovak}
      function IsOpenedWindow (AProject: Pointer): integer;
      {brovak}
      property LastWasAttributeWindow[AProject: Pointer]: boolean read GetLastWasAttributeWindow write SetLastWasAttributeWindow;

   end;

var
   AnIDBMainMan: TIDB_MainManager;
   AWinGISMainForm: TForm;
   bCommonReadOnlyMode, createnewfields: Boolean;

   sLanguageFileName,
      sMenuIniFileName,
      sToolbarIniFileName: AnsiString;
   lastlayername : Pointer;

implementation

uses Dialogs, SysUtils, Windows, DB,
   DTables, DUtils,
   IDB_Utils, IDB_2paramsDialog, IDB_LanguageManager, IDB_Messages,
   IDB_DescMan, IDB_StructMan, IDB_Consts, IDB_DataMan,
   IDB_DatabaseItemsManager, IDB_MovementManager, IDB_Progress,
   IDB_Monitor1, IDB_WG_Func;

constructor TIDB_MainManager.Create;
begin
   listProjectDefs := TList.Create;
   // Cadmensky
   //     AnInsertManager := TIDB_InsertRecordsManager.Create;
   AnInfoFormPositionManager := TIDB_InfoFormPositionManager.Create;

// ++ Commented by Cadmensky Version 2.3.2
//   KillAllExternalTablesPool;
// -- Commented by Cadmensky Version 2.3.2
end;

destructor TIDB_MainManager.Free;
begin
   ClearData;
   listProjectDefs.Free;
   // Cadmensky
   //     AnInsertManager.Free;
   AnInfoFormPositionManager.Free;
end;

procedure TIDB_MainManager.ClearData;
begin
// ++ Cadmensky Version 2.3.8
      if SELF.AViewForm <> nil then
         SELF.AViewForm.Close;
      SELF.AViewForm := nil;

      if SELF.AMonitoringForm <> nil then
         SELF.AMonitoringForm.Close;
      SELF.AMonitoringForm := nil;

      if SELF.AInfoForm <> nil then
         SELF.AInfoForm.Close;
      SELF.AInfoForm := nil;
// -- Cadmensky Version 2.3.8
   // Delete information about all projects.
   while listProjectDefs.Count > 0 do
      SELF.DeleteProject (0);
end;

procedure TIDB_MainManager.SetScreenCursor;
begin
   SELF.AnOldScreenCursor := Screen.Cursor;
   Screen.Cursor := crHourGlass;
   Application.ProcessMessages;
   Application.HandleMessage;
end;

procedure TIDB_MainManager.RestoreScreenCursor;
begin
   Screen.Cursor := SELF.AnOldScreenCursor;
end;

function TIDB_MainManager.FindProject (AProject: Pointer): Integer;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := -1;
   for iP := 0 to listProjectDefs.Count - 1 do
   begin
      pPD := listProjectDefs[iP];
      if pPD.AProject = AProject then
      begin
         RESULT := iP;
         EXIT;
      end;
   end; // for iP end
end;

function TIDB_MainManager.AddProject (AProjectFormHandle: Integer; AProject: Pointer; bProjectFileHasReadOnlyState: Boolean): Boolean;
var
   pPD: PProjectDef;
   MMD: TIDB_MovementManager;
begin
   RESULT := TRUE; // if this procedures was called for already added project, it must return TRUE.
   if SELF.FindProject (AProject) > -1 then EXIT; // We needn't to add this project. There is it already.
   RESULT := FALSE; // Now we try to add new project and as default result must be set in FALSE state.
   // Otherwise add it.
   SetScreenCursor;
   try
      New (pPD);
      pPD.AProject := AProject;
      pPD.bProjectFileHasReadOnlyState := bProjectFileHasReadOnlyState;
      pPD.AProjectFormHandle := AProjectFormHandle;
// ++ Cadmensky
      pPD.bLastWasAttributeWindow := false;

      if SELF.AViewForm <> nil then
         SELF.AViewForm.Close;
      SELF.AViewForm := nil;

      if SELF.AMonitoringForm <> nil then
         SELF.AMonitoringForm.Close;
      SELF.AMonitoringForm := nil;

      if SELF.AInfoForm <> nil then
         SELF.AInfoForm.Close;
      SELF.AInfoForm := nil;
// -- Cadmensky
      // Create data access objects.
      pPD.IDB_DA := TIDB_Access2DB_Level2.Create (AProject);

// ++ Cadmensky
{      if not FileExists (MakeDBFileNameFromProjectFileName (GetProjectFullFileNameFunc (pPD.AProject))) then
         if SELF.CheckVersionOfProjectAndDBFiles (pPD) then
            SELF.SyncProjectAndDB (pPD);}
// -- Cadmensky

      pPD.IDB_DA.Connect (MakeDBFileNameFromProjectFileName (GetProjectFullFileNameFunc (pPD.AProject)), '', '');
      // Before I'll make a synchronization, I have to check version of the project and DB files.
      bIHaveToBeDisconnected := FALSE;

      pPD.bTheDatabaseFileWas97thFormat := FALSE;

// ++ Cadmensky
      try
         pPD.bUseNewATTNames := True;
         RenameATTTablesIfItNeed (AProject, pPD.IDB_DA);
      except
      end;
      pPD.listLayerNames := TList.Create;
      ReadLayerTablesNames (AProject, pPD.listLayerNames, pPD.IDB_DA);
// -- Cadmensky
      // Now I'm going to make a synchronization between project and database file.
      // The synchronization I'll do only if the DB file is the same version and this fact
      // was detected automatically without user help. if database file and project have different
      // version value, the synchronization and reconciling should be done by the user himself later.

      if SELF.CheckVersionOfProjectAndDBFiles (pPD) then
         SELF.SyncProjectAndDB (pPD);

      if bIHaveToBeDisconnected then
      begin
         pPD.bTheDatabaseFileWas97thFormat := FALSE; // This line was added only for the next line will be executed.
         // The compiler passed it if I delete this line.
         RestoreScreenCursor;
         EXIT;
      end;

      { if the database file is a previous version (MS Access 97) we need to change its format to MS Access 2000 format. }
      pPD.bTheDatabaseFileWas97thFormat := pPD.IDB_DA.ADbItemsMan.CheckDBFileVersion = dbv97;
      if pPD.bTheDatabaseFileWas97thFormat then
      begin
         MMD := TIDB_MovementManager.Create (pPD.AProject, pPD.IDB_DA);
         try
            MMD.MigrateIt;
         finally
            MMD.Free;
         end;
      end
      else
         if pPD.bProjectFileHasReadOnlyState then
            // Save the temp file if the database file has MS Access 2000 format and the project has Read-Only state.
            SaveTempDatabaseFile (pPD.AProject);

      try
         SELF.MakeBakUpOfDatabaseFile (pPD);
      except
      end;
      // Create manager of layers of this project.
      pPD.ALayerManager := TIDB_LayerManager.Create (AProjectFormHandle, AProject, pPD.IDB_DA);
      pPD.bThereIsNoNecessityToPutDataIntoUndo := FALSE;
      pPD.bIAmGeneratingTheItemsByMyself := FALSE;
      pPD.bAnExternalDBIsWorking := FALSE;
      // Create UNDO and REDO managers.
//      pPD.AnUndoManager := TIDB_UndoManager.Create (AProject, pPD.IDB_DA);
//      pPD.ARedoManager := TIDB_RedoManager.Create (AProject, pPD.IDB_DA);

      pPD.APolygonOverlayManager := nil; // This object will be created only if it will be required.

// ++ Cadmensky Version 2.3.7
      pPD.AFormPosManager := TIDB_FormPositionManager.Create (pPD.IDB_DA, -1);
//      pPD.AMonitoringFormPosManager := TIDB_MonitorFormPositionManager.Create (pPD.IDB_DA, -1);
// -- Cadmensky Version 2.3.7
      // Add the information about the project in the list.
      listProjectDefs.Add (pPD);
      RESULT := TRUE;
   finally
      RestoreScreenCursor;
   end;
end;

{ This procedure deletes project's entry by project's position in the list. }

procedure TIDB_MainManager.DeleteProject (iProjectPos: Integer);
var
   pPD: PProjectDef;
   sProjectFileName,
      sTempFileName,
      sReallyDatabaseFileName: AnsiString;
   iI: Integer;
   inilist : TSTringList;
  // timer: TTimerThread;
begin
   if (iProjectPos < 0) or (iProjectPos > listProjectDefs.Count - 1) then EXIT;
   SetScreenCursor;
   try
   ShowPercentAtWinGISProc (0, 1457, 8);
      pPD := listProjectDefs[iProjectPos];
// ++ Cadmensky Version 2.3.3
    //  if pPD.bUseNewATTNames then
         SaveLayerTablesNames (pPD.AProject, pPD.listLayerNames, pPD.IDB_DA);
// -- Cadmensky Version 2.3.3

      if pPD.APolygonOverlayManager <> nil then
      begin
         try
            pPD.APolygonOverlayManager.Free;
         except
         end;
      end;

// ++ Commented by Cadmensky Version 2.3.7
//      pPD.AMonitoringFormPosManager.SaveData;
//      pPD.AMonitoringFormPosManager.Free;
// -- Commented by Cadmensky Version 2.3.7
      ShowPercentAtWinGISProc (1, 1457, 10);
      pPD.ALayerManager.Free;
      ShowPercentAtWinGISProc (1, 1457, 20);
// ++ Cadmensky Version 2.3.7
      pPD.AFormPosManager.Free;
// -- Cadmensky Version 2.3.7
      ShowPercentAtWinGISProc (1, 1457, 30);
      pPD.IDB_DA.Free; // The disconnect will be made in Free method.
      { if the database file had MS Access 97 format, it was transformed to MS Access 2000 format
        but not deleted immediately. if this flag is in TRUE state, the project was not saved by
        the user. In this case, we have to delete file of MS Access 2000 format and restore the file
        of MS Access 97 format. }
       ShowPercentAtWinGISProc (1, 1457, 35);
      if pPD.bProjectFileHasReadOnlyState or pPD.bTheDatabaseFileWas97thFormat then
      begin
         sProjectFileName := GetProjectFullFileNameFunc (pPD.AProject);
         sReallyDatabaseFileName := MakeDBFileNameFromProjectFileName (sProjectFileName);
         sTempFileName := MakeTempDBFileName (sProjectFileName);
         // Move the temporarily saved database file to the really database file.
         MoveFileEx (PChar (sTempFileName), PChar (sReallyDatabaseFileName), MOVEFILE_REPLACE_EXISTING);
      end
      else
         begin
         try
            ShowPercentAtWinGISProc (1, 1458, 40);
              if fileexists(stringreplace(Application.ExeName,'.exe','.ini',[rfIgnoreCase])) then
                 begin
                 inilist:=TSTringList.Create;
                 inilist.LoadFromFile(stringreplace(Application.ExeName,'.exe','.ini',[rfIgnoreCase]));
                 if (inilist.Values['IDB_Compact']<>'0') or (lowercase(inilist.Values['IDB_Compact'])<>'false') then
                         begin
                         sProjectFileName := GetProjectFullFileNameFunc (pPD.AProject);
                         SELF.PackDatabase (sProjectFileName);
                         end;
                 inilist.free;
                 end else
                 begin
                   sProjectFileName := GetProjectFullFileNameFunc (pPD.AProject);
                   SELF.PackDatabase (sProjectFileName);
                 end;
         except
         end;
         end;
      ShowPercentAtWinGISProc (1, 1457, 80);
      for iI := 0 to pPD.listLayerNames.Count - 1 do
         Dispose (PLayerNameItem (pPD.listLayerNames[iI]));
      pPD.listLayerNames.Clear;
      pPD.listLayerNames.Free;

      Dispose (pPD);
      listProjectDefs.Delete (iProjectPos);
   finally
      ShowDonePercentProc(0);
      ShowPercentAtWinGISProc (2, 0, 0);
      RestoreScreenCursor;
   end;
end;

{ This procedure deletes project's entry by project's pointer. It finds the project position
  in the list and calls the deleting by the project's position in the list. }

procedure TIDB_MainManager.DeleteProject (AProject: Pointer);
var
   iP: Integer;
//   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   SetScreenCursor;
   try
//      pPD := listProjectDefs[iP];
      ShowPercentAtWinGISProc (0, 1457, 0);
      Application.ProcessMessages;
      SELF.CloseAllWindowsOfProject (iP);
      ShowPercentAtWinGISProc (1, 1457, 5);
      SELF.DeleteProject (iP);
   finally
      RestoreScreenCursor;
   end;
end;

procedure TIDB_MainManager.ShowAttributeWindow (AProject, ALayer, AAlayer: Pointer);
var
   pPD: PProjectDef;
   iP: Integer;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   SetScreenCursor;
   try
      pPD := listProjectDefs[iP];
      pPD.ALayerManager.ShowAttributeWindow (ALayer, AALayer);
   finally
      RestoreScreenCursor;
   end;
end;
{brovak}

function TIDB_MainManager.IsOpenedWindow (AProject: Pointer): integer;
//var
//   iI: integer;
//   AForm: TForm;
begin;
   RESULT := 0;
   if (SELF.AViewForm <> nil) and
      (SELF.AViewForm.AProject = AProject) then
      RESULT := RESULT + 1;
   if (SELF.AMonitoringForm <> nil) and
      (SELF.AMonitoringForm.AProject = AProject) then
      RESULT := RESULT + 2;
{   for iI := 0 to Screen.FormCount - 1 do
   begin
      AForm := Screen.Forms[iI];
      if AForm is TIDB_ViewForm then
         if //(TIDB_ViewForm (AForm).ActiveLayer = ALayer) and
            (AForm.FormStyle = fsStayOnTop) and
            (TIDB_ViewForm (AForm).AProject = AProject) then
         begin
            RESULT := RESULT + 1;
//            Exit;
         end;
      if AForm is TIDB_MonitorForm then
         if TIDB_MonitorForm (AForm).AProject = AProject then
         begin
            RESULT := RESULT + 2;
//          Exit;
         end
   end;}
//   Result := false;
end;
{brovak}

(*procedure TIDB_MainManager.MakeSpecialChart(AProject: Pointer);
 begin;

 end;*)

(* procedure TIDB_MainManager.SendSpecialforChart(AProject:Pointer;PseudoDDEString:Ansistring);
 begin;

 end;
  *)

procedure TIDB_MainManager.CloseAttributeWindow (AProject, ALayer: Pointer);
var
   pPD: PProjectDef;
   iP: Integer;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   SetScreenCursor;
   try
      pPD := listProjectDefs[iP];
      pPD.ALayerManager.CloseAttributeWindow (ALayer);
   finally
      RestoreScreenCursor;
   end;
end;

procedure TIDB_MainManager.CloseMonitoringWindow (AProject: Pointer);
var
   iI: Integer;
   AForm: TForm;
begin
   SetScreenCursor;
   try
      for iI := 0 to Screen.FormCount - 1 do
      begin
         AForm := Screen.Forms[iI];
         if (AForm is TIDB_MonitorForm) and (TIDB_MonitorForm (AForm).AProject = AProject) then
         begin
            AForm.Close;
            BREAK;
         end;
      end; // for iI end
   finally
      RestoreScreenCursor;
   end;
end;

procedure TIDB_MainManager.ClearInfoWindow (AProject: Pointer);
var
   iI: Integer;
   AForm: TForm;
begin
   for iI := 0 to Screen.FormCount - 1 do
   begin
      AForm := Screen.Forms[iI];
      if (AForm is TIDB_Monitor1Form) and (TIDB_Monitor1Form (AForm).Project = AProject) then
      begin
         TIDB_Monitor1Form (AForm).ClearData;
         BREAK;
      end;
   end;
end;

procedure TIDB_MainManager.CloseInfoWindow;
var
   iI: Integer;
   AForm: TForm;
begin
   for iI := 0 to Screen.FormCount - 1 do
   begin
      AForm := Screen.Forms[iI];
      if AForm is TIDB_Monitor1Form then
      begin
         AForm.Close;
         AForm.Free;
         BREAK;
      end;
   end;
end;

procedure TIDB_MainManager.HideInfoWindow;
var
   iI: Integer;
   AForm: TForm;
begin
   for iI := 0 to Screen.FormCount - 1 do
   begin
      AForm := Screen.Forms[iI];
      if AForm is TIDB_Monitor1Form then
      begin
         AForm.Hide;
         BREAK;
      end;
   end;
end;

procedure TIDB_MainManager.CloseAllWindowsOfProject (iProjectPos: Integer);
var
   pPD: PProjectDef;
begin
   if (iProjectPos < 0) or (iProjectPos > listProjectDefs.Count - 1) then EXIT;
   pPD := listProjectDefs[iProjectPos];
   SetScreenCursor;
   try
      // Close all attribute windows.
      pPD.ALayerManager.CloseAllWindows;
   finally
      RestoreScreenCursor;
   end;
   // Close monitoring window.
   SELF.CloseMonitoringWindow (pPD.AProject);

   SELF.CloseInfoWindow;
end;

procedure TIDB_MainManager.CloseAllWindowsOfProject (AProject: Pointer);
var
   iP: Integer;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   SELF.CloseAllWindowsOfProject (iP);
end;

procedure TIDB_MainManager.CloseAllWindowsOfAllProjects;
var
   iI: Integer;
begin
   for iI := 0 to listProjectDefs.Count - 1 do
      SELF.CloseAllWindowsOfProject (iI);
end;

function TIDB_MainManager.GetItemIDToBeDeleted_FromIDB (AProject: Pointer; ALayer: Pointer): Integer;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := -1;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   RESULT := pPD.ALayerManager.GetItemIDToBeDeleted_FromIDB (ALayer);
end;

function TIDB_MainManager.GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG (AProject: Pointer): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := FALSE;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   RESULT := pPD.ALayerManager.GetReadyToReceiveIDsOfObjectToBeDeleted_FromWG;
end;

procedure TIDB_MainManager.ReceiveIDOfObjectToBeDeleted_FromWG (AProject, ALayer: Pointer; iObjectID: Integer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.ALayerManager.ReceiveIDOfObjectToBeDeleted_FromWG (ALayer, iObjectID);
end;

procedure TIDB_MainManager.DeleteObjects_FromWG (AProject: Pointer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.ALayerManager.DeleteObjects_FromWG;
end;

{ These procedures are linked to synchronization between a project and a DB file.}

function TIDB_MainManager.CheckVersionOfProjectAndDBFiles (pPD: PProjectDef): Boolean;
var
   sDBFullName,
      sGUID: AnsiString;

   procedure SetTheSameGUIDsInProjectAndDB;
   begin
      sGUID := CreateGUID;
      SetStartProjectGUIDProc (pPD.AProject, PChar (sGUID));
      pPD.IDB_DA.SetStartDatabaseGUIDInformation (sGUID);
      SetCurrentProjectGUIDProc (pPD.AProject, PChar (sGUID));
      pPD.IDB_DA.SetCurrentDatabaseGUIDInformation (sGUID);
   end;

   procedure Unload;
   begin
      pPD.IDB_DA.Disconnect;
      pPD.IDB_DA.UnLoadEmptyDBFile (sDBFullName);
// ++ Cadmensky
      SELF.RenameATTTablesIfItNeed (pPD.AProject, pPD.IDB_DA);
// -- Cadmensky
      pPD.IDB_DA.Connect (sDBFullName, '', '');
      pPD.IDB_DA.ADbItemsMan.CreateInternalTables;
      SetTheSameGUIDsInProjectAndDB;
   end;

   procedure DeleteOldLayersTables (sMask: AnsiString);
   var
      iI: Integer;
      slTablesNames: TStringList;
   begin
      slTablesNames := TStringList.Create;
      pPD.IDB_DA.ADbItemsMan.GetTablesNames (sMask, slTablesNames);
      for iI := 0 to slTablesNames.Count - 1 do
         pPD.IDB_DA.ADbItemsMan.DeleteTable (slTablesNames[iI]);
      slTablesNames.Free;
   end;

var
   sReallyProjectFullName,
      sReallyDBFullName,
      sProjectFullName,
      sProjectPath,
      sStartProjectGUID, sStartDBGUID,
      sCurrentProjectGUID, sCurrentDBGUID: AnsiString;
   ISPF: TIDB_2ParamsDialogForm;
   OD: TOpenDialog;
   slTablesNames: TStringList;
   iI: Integer;
   bFileAlreadyExisted: Boolean;
   AnExecQuery: TDQuery;
begin
   SetScreenCursor;
   { This function returns TRUE if the current GUID in the project and in the DB file is the same.
     if these GUIDs are different, I'll inform an user about this problem and request from him to select
     where required file is placed. When the user has selected one, I request from him what I have
     to do with this file: we have two ways - simply copy this file or move it. if the user has selected
     DB file with name that is different from required one, I will also rename the DB file.}
   RESULT := FALSE;

   sReallyProjectFullName := GetProjectFullFileNameFunc (pPD.AProject);
   sReallyDBFullName := GetADOPart (pPD.IDB_DA.DMaster.Connection, 'Data Source');
   sProjectPath := ExtractFilePath (sReallyProjectFullName);

   if GetProjectIsNewAndNotSavedStateFunc (pPD.AProject) then
   begin
      // At first we have to check about DEFAULT project and DEFAULT database.
      sProjectFullName := Trim (GetDefaultProjectFileNameFunc);
      sDBFullName := MakeDBFileNameFromProjectFileName (sProjectFullName);

      if FileExists (sProjectFullName) and FileExists (sDBFullName) then
      begin
         pPD.IDB_DA.Disconnect;
         CopyFile (PChar (sDBFullName), PChar (MakeDBFileNameFromProjectFileName (GetProjectFullFileNameFunc (pPD.AProject))), FALSE);
         pPD.IDB_DA.Connect (sDBFullName, '', '');
         RESULT := TRUE;
         EXIT;
      end;
   end;

   sProjectFullName := sReallyProjectFullName;
   sDBFullName := sReallyDBFullName;

   sCurrentProjectGUID := GetCurrentProjectGUIDFunc (pPD.AProject);
   if pPD.IDB_DA.Connect (sDBFullName, '', '') then
      sCurrentDBGUID := pPD.IDB_DA.GetCurrentDatabaseGUIDInformation
   else
      sCurrentDBGUID := '';
   bFileAlreadyExisted := FileExists (sDBFullName);

   if (sCurrentProjectGUID = sCurrentDBGUID) and (sCurrentProjectGUID <> '') and (sCurrentDBGUID <> '') then
   begin
      // All is OK.
      RESULT := TRUE;
      EXIT;
   end;
   if (not bFileAlreadyExisted) and GetProjectIsNewAndNotSavedStateFunc (pPD.AProject) then
   begin
      // if an user has just created the project and before it I didn't find 'default' project
      // and ' default' db file, I need to create the db file.
      Unload;
      EXIT;
   end;
   if (bFileAlreadyExisted) and GetProjectIsNewAndNotSavedStateFunc (pPD.AProject) then
   begin
      // In this case, I have found the project was just created but the db file already exists.
      DeleteOldLayersTables (cs_AttributeTableNamePrefix);
      DeleteOldLayersTables (cs_DescriptionTableNamePrefix);
      DeleteOldLayersTables (cs_PatternTableNamePrefix);
      DeleteOldLayersTables (cs_TempTableNamePrefix);
      EXIT;
   end;

   // Now check that GUIDs simply are different but both are existed or the project GUID exists
   // but the DB GUID doesn't.
   if ((sCurrentProjectGUID <> '') and (sCurrentDBGUID <> '')) or (bFileAlreadyExisted) then
   begin
      // At this time the screen cursor has HourGlass shape. to allow the user select somewhat in the
      // question dialogs I have to make the screen cursor shape as Normal and then return it to
      // HourGlass again.
      Screen.Cursor := crDefault;
      try
         // This file is not the last version of the db file. Anyway to use it??
         if MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (158), [sDBFullName, sProjectFullName])), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrYES then
         begin
            SetTheSameGUIDsInProjectAndDB;
            SetProjectModifiedProc (pPD.AProject);
            RESULT := TRUE;
         end
         else
            Unload;
      finally
      end;
      // The next line was placed here not under finally block in order to allow
      // EXIT line to be compiled and executed.
      RestoreScreenCursor;
      EXIT;
   end;

   sStartProjectGUID := GetStartProjectGUIDFunc (pPD.AProject);
   ISPF := TIDB_2ParamsDialogForm.Create (Application);
   ISPF.Label1.Caption := Format (ALangManager.GetValue (159), [sProjectFullName]); // The db file of the project is missing.
   ISPF.RadioButton1.Caption := ALangManager.GetValue (160); // to create a new db file.
   ISPF.RadioButton2.Caption := ALangManager.GetValue (161); // to select an existsing db file.
   ISPF.RadioButton1.Checked := TRUE;
   ISPF.OnCloseQuery := ISPF.FormCloseQuery; // Add the handler that will ask about furthet actions just after
   // ESC key has been pressed or Cancel button has been clicked.
// At this time the screen cursor has HourGlass shape. to allow the user select somewhat in the
// question dialog I have to make the screen cursor shape as Normal and then return it to
// HourGlass again.
   Screen.Cursor := crDefault;

   if ISPF.ShowModal = mrOk then
   begin
      if ISPF.RadioButton1.Checked then // The user has selected to create new file.
         Unload
      else
      begin // The user has selected to selected already existed DB file.
         OD := TOpenDialog.Create (Application);
         // OD.Filter := '(*.mdb)|*.mdb|(*.md~)|*.md~';
         // 'WinGIS database files (*.wgi)|*.wgi|WinGIS backup database files (*.wg~)|*.wg~'
         OD.Filter := Format (ALangManager.GetValue (162), [cs_ApplicationName, cs_ApplicationName]);
         OD.InitialDir := ExtractFilePath (sProjectFullName);
         if not OD.Execute then // if the user didn't select DB file, I create new file.
            Unload
         else
         begin
            if not FileExists (OD.FileName) then
            begin
               // The file ''%s'' does not exist. The new database file will be created.
               MessageBox (AWinGISMainForm.Handle, PChar (Format (ALangManager.GetValue (163), [OD.FileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION);
               Unload;
            end
            else
            begin
               // The user has selected another DB file. What have I to do with it?
               ISPF.Label1.Caption := Format (ALangManager.GetValue (164), [OD.FileName]); //  What to do with the file ''%s''?
               ISPF.RadioButton1.Caption := ALangManager.GetValue (165); // do not delete the selected file after copying
               ISPF.RadioButton2.Caption := ALangManager.GetValue (166); // Delete the selected file after copying
               ISPF.RadioButton1.Checked := TRUE;
               ISPF.CancelButton.Visible := FALSE;
               ISPF.ShowModal;
               // Copy database file
               pPD.IDB_DA.Connect (OD.FileName, '', '');
               sStartDBGUID := pPD.IDB_DA.GetStartDatabaseGUIDInformation;
               if (sStartProjectGUID <> sStartDBGUID)
                  and
                  // The database file ''%s'' was created for a different project. All the same do you want to use it?
               (MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (167), [OD.FileName])), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) <> mrYES) then
               begin
                  Unload;
                  pPD.IDB_DA.Connect (sDBFullName, '', '');
               end
               else
               begin
                  CopyFile (PChar (OD.FileName), PChar (sDBFullName), FALSE);
                  if ISPF.RadioButton2.Checked then
                  begin
                     // if the user wishes to delete the source file, I do it.
                     pPD.IDB_DA.Disconnect;
                     Application.ProcessMessages;
                     DeleteFile (PChar (OD.FileName));
                  end;
                  // and now I have to set the same GUID in the both files (project and DB once).
                  pPD.IDB_DA.Connect (sDBFullName, '', '');
                  SetTheSameGUIDsInProjectAndDB;
                  // and now I have to set all STATE flags in the normal state.
                  slTablesNames := TStringList.Create;

                  pPD.IDB_DA.ADbItemsMan.GetTablesNames (cs_AttributeTableNamePrefix, slTablesNames);

                  AnExecQuery := TDQuery.Create (nil);
                  AnExecQuery.Master := pPD.IDB_DA.DMaster;
                  try
                     for iI := 0 to slTablesNames.Count - 1 do
                     begin
                        try
                           // UPDATE table_name SET field_name = new_value WHERE key_field = key_value
                           AnExecQuery.ExecSQL ('UPDATE [' + slTablesNames[iI] + '] ' +
                              'SET [' + cs_StateFieldName + '] = ' + cs_ObjectIsNormal +
                              'WHERE [' + cs_StateFieldName + '] <> ' + cs_ObjectIsNormal);
                        except
                        end;
                     end; // for iI end
                  finally
                     AnExecQuery.Free;
                  end;
                  slTablesNames.Free;
                  // The database file ''%s'' was copied to the project. You should to reconcile data contained in the project and in attribute tables in the database file.
                  MessageBox (Application.Handle, PChar (Format (ALangManager.GetValue (168), [OD.FileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION);
               end;
            end;
         end;
      end;
      RestoreScreenCursor;
      RESULT := TRUE;
   end
   else
   begin
      RestoreScreenCursor;
      bIHaveToBeDisconnected := TRUE;
      RESULT := FALSE;
   end;
   ISPF.Free;
end;

procedure TIDB_MainManager.DeleteTablesOfLayersThatCanBeNotExist (pPD: PProjectDef);
var
   iI, iLayerIndex: Integer;
   slTablesNames: TStringList;
   sLayerIndex: AnsiString;
   ALayer: Pointer;
begin
   slTablesNames := TStringList.Create;
   // Get all tables names that started form ATT.
   pPD.IDB_DA.ADbItemsMan.GetTablesNames (cs_AttributeTableNamePrefix, slTablesNames);
   for iI := 0 to slTablesNames.Count - 1 do
   begin
      if not pPD.bUseNewATTNames then
      begin
         sLayerIndex := Copy (slTablesNames[iI], 4, Length (slTablesNames[iI]));
         try
            iLayerIndex := StrToInt (sLayerIndex);
         except
            CONTINUE;
         end;
      end
      else
      begin
//         slTablesNames.Free;
//         exit;
         sLayerIndex := Copy (slTablesNames[iI], 5, Length (slTablesNames[iI]));
         iLayerIndex := GetLayerIndexByNameEx (pPD.AProject, sLayerIndex, pPD.listLayerNames);
         sLayerIndex := '_' + sLayerIndex;
      end;
      ALayer := GetLayerPointerByIndexFunc (pPD.AProject, iLayerIndex);
      if ALayer = nil then
      begin
         pPD.IDB_DA.ADBItemsMan.DeleteTable (cs_AttributeTableNamePrefix + sLayerIndex);
         pPD.IDB_DA.ADBItemsMan.DeleteAllLayerTables (IntToStr (iLayerIndex));
      end;
   end; // for iI end
   slTablesNames.Free;
end;

procedure TIDB_MainManager.SyncProjectAndDB (pPD: PProjectDef);
var
   iI, iProgisID, iP, iJ: Integer;
   sAttTableName: AnsiString;
//   bFieldExists,
      bTheProjecthasToBeSetAsModified: Boolean;
   AQuery: TDQuery;
   iLayerCount: Integer;
   ALayer: Pointer;

   sMainStatus, sSlaveStatus: AnsiString;
   AProgressForm: TIDB_ProgressForm;
   sIDs: AnsiString;
   sDelIDs: AnsiString;
begin
   { How I do it.
     I.
     The user during his work could delete some layers, but I didn't delete according attribute tables.
     When the project is being loaded, I check if the layer exists. if the layer doesn't exist, I delete according tables.
     II.
     We have a special field in an attribute table. Name of this field is _WG_STATE.
     This field has 3 possible values:
     - zero value - nothing was happened with object in the project;
     - negative value - an object was deleted from the project;
     - positive value - an object was added in the project.

     1. I set these values during WinGIS works.
     2. When an user saves the project, I set state values that are positive as zero, and I delete
        all records that contain negative state value.
     3. When the user loads the project, I check STATE value, if this value doesn't equal zero and
        this object doesn't exist in the project, I delete this record from the table. Otherwise,
        if this object exists, I set STATE value to zero.
     }
   //////////// P A R T   I.
   SELF.DeleteTablesOfLayersThatCanBeNotExist (pPD);
   //////////// P A R T   II.
   // Here I create a progress form and set for it some text parameters.
   // The form will be show if it is required. if isn't, the form won't be show at all.
   AProgressForm := TIDB_ProgressForm.Create (AWinGISMainForm);
   // Synchronization of project and database files
   sMainStatus := ALangManager.GetValue (278);
   // Layer %d from %d
   sSlaveStatus := ALangManager.GetValue (279);
   AProgressForm.StatusLabel.Caption := sMainStatus;
   AProgressForm.ShowTheBottomPanel;

   AQuery := TDQuery.Create (nil);
   AQuery.Master := pPD.IDB_DA.DMaster;

   iLayerCount := GetProjectLayerNumberFunc (pPD.AProject);
   bTheProjecthasToBeSetAsModified := FALSE;
   for iI := 1 to iLayerCount - 1 do
   begin // From 1 because zero layer is the layer of selected items.
      AProgressForm.StatusLabel.Caption := sMainStatus + Format (sSlaveStatus, [iI, iLayerCount - 1]);
      AProgressForm.GlobalGauge.Progress := 0;
      Application.ProcessMessages;
      ALayer := GetLayerPointerByPositionFunc (pPD.AProject, iI);
      // Get an attribute table name.
      if not pPD.bUseNewATTNames then
         sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
      else
         sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (pPD.AProject, ALayer, pPD.listLayerNames);

// ++ Cadmensky Version 2.3.2
      if pPD.IDB_DA.ADBItemsMan.GetReadyToCheckTableStructure (sAttTableName) <> ci_All_Ok then
      begin
         pPD.IDB_DA.ADBItemsMan.EndOfCheckingTable;
         Continue;
      end;

      if not pPD.IDB_DA.ADbItemsMan.FieldExists (cs_StateFieldName) then
         pPD.IDB_DA.ADbItemsMan.AddField (sAttTableName, cs_StateFieldName, 12, -1); // Type '12' is a byte type.

      pPD.IDB_DA.ADBItemsMan.EndOfCheckingTable;
// -- Cadmensky Version 2.3.2

// ++ Commented by Cadmensky Version 2.3.2
{      // if the layer attribute table doesn't exist, go to the next project layer.
      if not pPD.IDB_DA.ADbItemsMan.TableExists (sAttTableName) then
         CONTINUE;
      // Check whether required field exists or not.
      try
         if pPD.IDB_DA.ADbItemsMan.FieldExists (sAttTableName, cs_StateFieldName, bFieldExists) <> ci_All_Ok then
            CONTINUE;
      except
         CONTINUE;
      end;
      // if it doesn't exist, create it.
      if not bFieldExists then
      begin
         try
            pPD.IDB_DA.ADbItemsMan.AddField (sAttTableName, cs_StateFieldName, 12, -1); // Type '12' is a byte type.
         except
            CONTINUE;
         end;
      end; }
// ++ Commented by Cadmensky Version 2.3.2

      // Get data from the attribute table where STATE value doesn't equal zero.
      if AQuery.Active then
         AQuery.Close;
      AQuery.SQL.Clear;
      AQuery.SQL.Add ('SELECT [PROGISID], [' + cs_StateFieldName + '] FROM [' + sAttTableName + '] WHERE [' + cs_StateFieldName + '] <> ' + cs_ObjectIsNormal + ' OR [' + cs_StateFieldName + '] IS NULL');
      try
         AQuery.Open;
      except
         CONTINUE;
      end;
      if AQuery.RecordCount > 0 then
      begin
         if AWinGISMainForm.Enabled then
            AWinGISMainForm.Enabled := FALSE;
         AProgressForm.Show;
         AProgressForm.GlobalGauge.MaxValue := AQuery.RecordCount;
         AQuery.First;
         bTheProjecthasToBeSetAsModified := TRUE;

         iP := 0;
         while not AQuery.EOF do
         begin
            sIDs := '(';
            sDelIDs := '(';
            for iJ := 0 to ci_PackLength do
            begin
               // try to get ProgisID value. if it is empty or contains incorrect data, I delete this record.
               if (iP + iJ = AQuery.RecordCount - 1) or AQuery.EOF then
                  break;
               try
                  iProgisID := AQuery.Fields[0].AsInteger;
               except
                  AQuery.Delete;
                  CONTINUE;
               end;
               // I have now ProgisID value and I start to find an object with such ID in the project.
               if not ItemExistsInProjectFunc (pPD.AProject, ALayer, iProgisID) then
               begin
// ++ Cadmensky
                  sDelIDs := sDelIDs + ' [PROGISID] = ' + IntToStr (iProgisID) + ' OR ';
{                  AQuery.Delete;
                  CONTINUE;}
// -- Cadmensky
               end
               else
               begin
                  sIDs := sIDs + ' [PROGISID] = ' + IntToStr (iProgisID) + ' OR ';
                  {                     try
                                          AQuery.Edit;
                                          AQuery.Fields[1].AsString := cs_ObjectIsNormal;
                                          AQuery.Post;
                                       finally
                                          AQuery.Next;
                                       end;}
               end;
               AQuery.Next;
               AProgressForm.GlobalGauge.Progress := AProgressForm.GlobalGauge.Progress + 1;
               Application.ProcessMessages;

               if AProgressForm.UserWantsToCancel then
                  Break;
            end; // for iJ end

// ++ Cadmensky
            if AProgressForm.UserWantsToCancel then
               Break;
// -- Cadmensky

            Inc (iP, ci_PackLength + 1);
            if sIDs <> '(' then
            begin
               sIDs := Copy (sIDs, 1, Length (sIDs) - 4) + ')';
               try
                  AQuery.ExecSQL ('UPDATE [' + sAttTableName + '] ' +
                     'SET [' + cs_StateFieldName + '] = ' + cs_ObjectIsNormal +
                     ' WHERE ' + sIDs);
               except
                //                  ShowMessage('Mraki');
               end;
            end;
// ++ Cadmensky
            if sDelIDs <> '(' then
            begin
               sDelIDs := Copy (sDelIDs, 1, Length (sDelIDs) - 4) + ')';
               try
                  AQuery.ExecSQL ('DELETE FROM [' + sAttTableName + '] ' +
                     'WHERE ' + sDelIDs);
               except
               end;
            end;
// -- Cadmensky

         end; // While end
      end;
   end; // for iI end

   // All internal tables except attribute tables should be hidden. This task can be solved by set a prefix for them.
   // The prefix is 'USYS'.
   SELF.MigrateAllInternalTablesIntoHiddenTables (pPD);

   if bTheProjecthasToBeSetAsModified then
      SetProjectModifiedProc (pPD.AProject);
   if not AWinGISMainForm.Enabled then
      AWinGISMainForm.Enabled := TRUE;
   AProgressForm.Hide;
   AProgressForm.Free;

   AQuery.Close;
   AQuery.Free;
end;

procedure TIDB_MainManager.MigrateAllInternalTablesIntoHiddenTables (pPD: PProjectDef);
var
   AQuery: TDQuery;
begin
   // Delete old Undo and Redo tables. They will be created again just before using.
//   pPD.IDB_DA.ADbItemsMan.DeleteTable (cs_Old_IT_UndoTableName);
//   pPD.IDB_DA.ADbItemsMan.DeleteTable (cs_Old_IT_RedoTableName);

   AQuery := TDQuery.Create (nil);
   AQuery.Master := pPD.IDB_DA.DMaster;

   // Special TEXT table
   pPD.IDB_DA.ADbItemsMan.CreateTextSpecialTable;
// ++ Cadmensky Version 2.2.9
   if pPD.IDB_DA.ADbItemsMan.TableExists (cs_Old_IT_ST_TextInfo) then
   begin
// -- Cadmensky Version 2.2.9
      AQuery.ExecSQL ('INSERT INTO [' + cs_IT_ST_TextInfo + '] ' +
         'SELECT * FROM [' + cs_Old_IT_ST_TextInfo + ']');
      pPD.IDB_DA.ADbItemsMan.DeleteTable (cs_Old_IT_ST_TextInfo);
   end;

   // Description table.
   pPD.IDB_DA.ADbItemsMan.CreateCommonDescTable;
// ++ Cadmensky Version 2.2.9
   if pPD.IDB_DA.ADbItemsMan.TableExists (cs_OldCommonDescTableName) then
   begin
// -- Cadmensky Version 2.2.9
      AQuery.ExecSQL ('INSERT INTO [' + cs_CommonDescTableName + '] ' +
         'SELECT * FROM [' + cs_OldCommonDescTableName + ']');
      pPD.IDB_DA.ADbItemsMan.DeleteTable (cs_OldCommonDescTableName);
   end;

   AQuery.Free;
end;

procedure TIDB_MainManager.MakeBakUpOfDatabaseFile (pPD: PProjectDef);
var
   iFileAge: Integer;
   dtFileAge: TDateTime;
   sDBFileName, sBackUpFileName: AnsiString;
   iwNowYear, iwNowMonth, iwNowDay,
      iwFileYear, iwFileMonth, iwFileDay: Word;
begin
   sDBFileName := MakeDBFileNameFromProjectFileName (GetProjectFullFileNameFunc (pPD.AProject));
   // Get date of create the project file.
   iFileAge := FileAge (sDBFileName);
   if iFileAge < 0 then EXIT;
   dtFileAge := FileDateToDateTime (iFileAge);
   // Decode now date and date of the file.
   DecodeDate (DATE, iwNowYear, iwNowMonth, iwNowDay);
   DecodeDate (dtFileAge, iwFileYear, iwFileMonth, iwFileDay);
   // if dates are different one from other, make backuo file.
   if (iwFileYear <> iwNowYear) or (iwFileMonth <> iwNowMonth) or (iwFileDay <> iwNowDay) then
   begin
      // Define backup file name.
      sBackUpFileName := Copy (sDBFileName, 1, Length (sDBFileName) - 1) + '~';
      // Copy database file.
      CopyFile (PChar (sDBFileName), PChar (sBackUpFileName), FALSE);
   end;
end;

procedure TIDB_MainManager.ProjectIsBeingSaved (AProject: Pointer);
var
   pPD: PProjectDef;
   iI, iP, iLayersCount: Integer;
   sAttTableName, sGUID: AnsiString;
   ALayer: Pointer;
   AnExecQuery: TDQuery;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.

   // First step: I set new values of GUID in the DB and in the project.
   sGUID := CreateGUID;
   SetCurrentProjectGUIDProc (AProject, PChar (sGUID));
   pPD := listProjectDefs[iP];
   pPD.IDB_DA.SetCurrentDatabaseGUIDInformation (sGUID);
   try SaveLayerTablesNames (pPD.AProject, pPD.listLayerNames, pPD.IDB_DA);
   except end;
   // Second step: I delete tables of layers that can be not exist (their layers were deleted).
   SELF.DeleteTablesOfLayersThatCanBeNotExist (pPD);
   // Third step: I work with existed layers and I mark all just added objects as 'normal' objects
   //             and delete all objects that were marked as 'deleted'.
   AnExecQuery := TDQuery.Create (nil);
   try
      AnExecQuery.Master := pPD.IDB_DA.DMaster;
      iLayersCount := GetProjectLayerNumberFunc (pPD.AProject);
      for iI := 1 to iLayersCount - 1 do
      begin
         ALayer := GetLayerPointerByPositionFunc (pPD.AProject, iI);

         if not UseNewATTNames (AProject) then
            sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
         else
            sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

// ++ Cadmensky Version 2.3.4
         if pPD.IDB_DA.ADBItemsMan.TableExists (sAttTableName) then
         begin
// -- Cadmensky Version 2.3.4
//            try
            // UPDATE table_name SET field_name = new_value WHERE key_field = key_value
            AnExecQuery.ExecSQL ('UPDATE [' + sAttTableName + '] ' +
               'SET [' + cs_StateFieldName + '] = ' + cs_ObjectIsNormal + ' ' +
               'WHERE [' + cs_StateFieldName + '] = ' + cs_ObjectIsJustAdded);
//            except
//            end;

//            try
            // DELETE FROM table_name WHERE key_field = key_value
            AnExecQuery.ExecSQL ('DELETE FROM [' + sAttTableName + '] ' +
               'WHERE [' + cs_StateFieldName + '] = ' + cs_ObjectIsDeleted);
//            except
//            end;
         end;
      end; // for iI end
  SaveLayerTablesNames (pPD.AProject, pPD.listLayerNames, pPD.IDB_DA);

   finally
      AnExecQuery.Free;
   end;

   // As the project is being saved by the user, the old version of the database of MS Access 97 format
   // is not required any more.
   if pPD.bTheDatabaseFileWas97thFormat then
   begin
      DeleteFile (PChar (MakeTempDBFileName (GetProjectFullFileNameFunc (pPD.AProject))));
      pPD.bTheDatabaseFileWas97thFormat := FALSE;
   end;
end;

procedure TIDB_MainManager.SaveDatabaseAs (AProject: Pointer; sNewProjectName: AnsiString);
var
   pPD: PProjectDef;
   iP: Integer;
   sOldDBFileName,
      sNewDBFileName: AnsiString;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project
   // Close for this project all data windows
   CloseAllWindowsOfProject (iP);
   pPD := listProjectDefs[iP];
   // Define file names (old and new)
   sOldDBFileName := GetADOPart (pPD.IDB_DA.DMaster.Connection, 'Data Source');
   sNewDBFileName := MakeDBFileNameFromProjectFileName (sNewProjectName);
   if sOldDBFileName <> sNewDBFileName then
   begin
      pPD.IDB_DA.Disconnect;
      // Copy database file.
      CopyFile (PChar (sOldDBFileName), PChar (sNewDBFileName), FALSE);

      if GetProjectIsNewAndNotSavedStateFunc (AProject) then
         DeleteFile (PChar (sOldDBFileName));

      if pPD.bProjectFileHasReadOnlyState or pPD.bTheDatabaseFileWas97thFormat then
      begin
         MoveFileEx (PChar (MakeTempDBFileName (sOldDBFileName)), PChar (sOldDBFileName), MOVEFILE_REPLACE_EXISTING);

         pPD.bProjectFileHasReadOnlyState := FALSE;
         pPD.bTheDatabaseFileWas97thFormat := FALSE;
      end;

      // Correct path to the new database file.
      pPD.IDB_DA.Connect (sNewDBFileName, '', '');
   end;
end;

{ When the project was just created, was never saved and is being closed, it is necessary
  to delete files of project and database. }

procedure TIDB_MainManager.DeleteProjectAndDatabaseFiles (sProjectFileName: AnsiString);
var
   sDBFileName: AnsiString;
begin
   sDBFileName := MakeDBFileNameFromProjectFileName (sProjectFileName);
   try
      if FileExists (sProjectFileName) then DeleteFile (PChar (sProjectFileName));
   except
   end;
   try
      if FileExists (sDBFileName) then DeleteFile (PChar (sDBFileName));
   except
   end;
end;

function TIDB_MainManager.GetReadyToReceiveIDsOfObjectsForMonitoring (AProject: Pointer): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := FALSE;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   RESULT := pPD.ALayerManager.GetReadyToReceiveIDsOfObjectsForMonitoring;
end;

procedure TIDB_MainManager.ReceiveIDOfObjectForMonitoring (AProject, ALayer: Pointer; iObjectID: Integer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.ALayerManager.ReceiveIDOfObjectForMonitoring (ALayer, iObjectID);
end;

procedure TIDB_MainManager.ShowMonitoringWindow (AProject: Pointer; bNeedToCloseViewForm: Boolean = true);
var
   iP, iSelectedItemsNumber: Integer;
   iwKey: Word;
   pPD: PProjectDef;
   AForm: TForm;
   bFound: Boolean;
   iLeft, iTop, iHeight, iWidth: Integer;
   iSplitterPos, iLeftColWidth, iRightColWidth: Integer;
   bTreeWillBeVisible: boolean;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   SetScreenCursor;

   try
      bFound := false;

      if AnIDBMainMan.AMonitoringForm <> nil then
      begin
         if AnIDBMainMan.AMonitoringForm.AProject = AProject then
         begin
            bFound := true;
            AForm := SELF.AMonitoringForm;
         end
         else
            AnIDBMainMan.AMonitoringForm.Close;
      end;
      Application.ProcessMessages;

      iLeft := -1;
      iTop := -1;
      iWidth := -1;
      iHeight := -1;
      // if the monitoring window wasn't found, I have to create it.
      if not bFound then
      begin
         // Get position and size for new window.
// ++ Cadmensky Version 2.3.7
         pPD.AFormPosManager.GetFormState (iLeft, iTop, iWidth, iHeight, bTreeWillBeVisible, bTreeWillBeVisible);
//         pPD.AMonitoringFormPosManager.GetFormState (iLeft, iTop, iWidth, iHeight, bTreeWillBeVisible);
// -- Cadmensky Version 2.3.7
         iSplitterPos := -1;
         iLeftColWidth := -1;
         iRightColWidth := -1;
// ++ Cadmensky Version 2.3.7
         pPD.AFormPosManager.GetGridState (iSplitterPos, iLeftColWidth, iRightColWidth, bTreeWillBeVisible);
//         pPD.AMonitoringFormPosManager.GetGridState (iSplitterPos, iLeftColWidth, iRightColWidth);
// -- Cadmensky Version 2.3.7

         AForm := TIDB_MonitorForm.Create (AWinGISMainForm, pPD.ALayerManager, AProject, pPD.IDB_DA);
         // Monitoring of project ''%s''
         AForm.Caption := Format (ALangManager.GetValue (181), [GetProjectFullFileNameFunc (AProject)]);
         AForm.WindowState := wsNormal;
         if iLeft > -1 then AForm.Left := iLeft;
         if iTop > -1 then AForm.Top := iTop;
         if iWidth > -1 then AForm.Width := iWidth;
         if iHeight > -1 then AForm.Height := iHeight;

         if iSplitterPos > -1 then TIDB_MonitorForm (AForm).LeftPanel.Width := iSplitterPos;
         if iLeftColWidth > -1 then TIDB_MonitorForm (AForm).StringGrid1.ColWidths[0] := iLeftColWidth;
         if iRightColWidth > -1 then TIDB_MonitorForm (AForm).StringGrid1.ColWidths[1] := iRightColWidth;
      end;
      // Monitoring step 1.
      pPD.ALayerManager.FillMonitoringData (AForm);
      // Monitoring step 2. View creating.
      iSelectedItemsNumber := TIDB_MonitorForm (AForm).MakeTreeFromItemsData;

      if not bFound then
         TIDB_MonitorForm (AForm).SetTreeVisible (bTreeWillBeVisible);

      TIDB_MonitorForm (AForm).TreeListView1.ItemIndex := 0;
      iwKey := VK_UP;
      TIDB_MonitorForm (AForm).TreeView1KeyUp (SELF, iwKey, []); // I call this procedure in order to fill information grid in the right panel

      // Monitoring of project ''%s'' (selected items count: %d)
      TIDB_MonitorForm (AForm).StatusLabel.Caption := Format (ALangManager.GetValue (304), [iSelectedItemsNumber]);

      if (not bFound) and (iLeftColWidth < 0) and (iRightColWidth < 0) then
         TIDB_MonitorForm (AForm).ResizeGridColumns;
      TIDB_MonitorForm (AForm).SetButtonsEnabled;

      if AnIDBMainMan.AViewForm <> nil then
      begin
         if SELF.AViewForm.WindowState <> wsMaximized then
         begin
            iTop := SELF.AViewForm.Top;
            iLeft := SELF.AViewForm.Left;
            AForm.Width := SELF.AViewForm.Width;
            AForm.Height := SELF.AViewForm.Height;
         end;
         TIDB_MonitorForm (AForm).MakeMyWindowState (SELF.AViewForm.WindowState);
//         SELF.AViewForm.Close;
         Application.ProcessMessages;
      end;
      AForm.Show;
      if iLeft > -1 then AForm.Left := iLeft;
      if iTop > -1 then AForm.Top := iTop;

      SELF.AMonitoringForm := TIDB_MonitorForm (AForm);

// ++ Cadmensky IDB Version 2.3.8
      AnIDBMainMan.LastWasAttributeWindow[AProject] := false;
      if (AnIDBMainMan.AViewForm <> nil) and bNeedToCloseViewForm then
         SELF.AViewForm.Close;
// -- Cadmensky IDB Version 2.3.8
   finally
      RestoreScreenCursor;
   end;
end;

procedure TIDB_MainManager.ShowInfoWindow (AProject: Pointer);
var
   iP, iI: Integer;
   pPD: PProjectDef;
   AForm: TForm;
   bFound, bZoomorSelect: Boolean;
   ALayer: Pointer;
   iLayerIndex, iItemID: Integer;
   iLeft, iTop, iHeight, iWidth, iSplitterPos: Integer;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];

   SetScreenCursor;
   try
      pPD.ALayerManager.GetInfoObjectParams (0, iLayerIndex, iItemID);

      if (iLayerIndex = -1) or (iItemID = -1) then
      begin
         RestoreScreenCursor;
         exit;
      end;

      bFound := FALSE;
      for iI := 0 to Screen.FormCount - 1 do
      begin
         AForm := Screen.Forms[iI];
         if (AForm is TIDB_Monitor1Form) then
         begin
            bFound := TRUE;
            AForm.Visible := true;
            BREAK;
         end;
      end; // for iI end
      // if the monitoring window wasn't found, I have to create it.
      if not bFound then
      begin
         // Get position and size for new window.
         iLeft := -1;
         iTop := -1;
         iWidth := -1;
         iHeight := -1;
         iSplitterPos := -1;
         AnInfoFormPositionManager.GetFormState (iLeft, iTop, iWidth, iHeight, iSplitterPos);
         AForm := TIDB_Monitor1Form.Create (Application);
         // 375=Info window
         AForm.Caption := ALangManager.GetValue (375);
         AForm.WindowState := wsNormal;
         
         if iLeft > -1 then AForm.Left := iLeft;
         if iTop > -1 then AForm.Top := iTop;
         if iWidth > -1 then AForm.Width := iWidth;
         if iHeight > -1 then AForm.Height := iHeight;
         if iSplitterPos > -1 then
         begin
            TIDB_Monitor1Form (AForm).CalcDataPanel.Width := iSplitterPos;
            TIDB_Monitor1Form (AForm).DataPanel.Width := iSplitterPos;
         end;
      end;
      TIDB_Monitor1Form (AForm).TopPanel.Visible := GetAllProjectWindowsNumberFunc > 1;
      // 379=Layer '%s'
      ALayer := GetLayerPointerByIndexFunc (AProject, iLayerIndex);
      AForm.Caption := ALangManager.GetValue (375);
      if ALayer <> nil then
         AForm.Caption := AForm.Caption + ' - ' + Format (ALangManager.GetValue (379), [GetLayerNameFunc (ALayer)]);

      TIDB_Monitor1Form (AForm).Init (AProject, pPD.ALayerManager, pPD.IDB_DA.DMaster);
      pPD.ALayerManager.FillMonitoringData (AForm);
      // Show either data or empty window.
      TIDB_Monitor1Form (AForm).ShowData (0);

      Application.ProcessMessages;
   finally
      RestoreScreenCursor;
   end;
end;

procedure TIDB_MainManager.RefreshAllDataWindows (AProject: Pointer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.ALayerManager.RefreshAllDataWindows;
end;

procedure TIDB_MainManager.RefreshInfoWindow;
var
   iI: Integer;
   AForm: TForm;
   bFound: Boolean;
begin
   bFound := FALSE;
   for iI := 0 to Screen.FormCount - 1 do
   begin
      AForm := Screen.Forms[iI];
      if AForm is TIDB_Monitor1Form then
      begin
         bFound := TRUE;
         BREAK;
      end;
   end; // for iI end
   if bFound then
   begin
      SendMessage (AForm.Handle, WM_IDB_Message, IDB_RefreshDataWindow, 1);
      Application.ProcessMessages;
   end;
end;

procedure TIDB_MainManager.RefreshMonitoringWindow (AProject: Pointer);
var
   bFound: Boolean;
   AForm: TForm;
   iI: Integer;
begin
   bFound := FALSE;
   for iI := 0 to Screen.FormCount - 1 do
   begin
      AForm := Screen.Forms[iI];
      if (AForm is TIDB_MonitorForm) and (TIDB_MonitorForm (AForm).AProject = AProject) then
      begin
         bFound := TRUE;
         BREAK;
      end;
   end; // for iI end
   if bFound then
   begin
      SendMessage (AForm.Handle, WM_IDB_Message, IDB_RefreshDataWindow, 1);
      Application.ProcessMessages;
   end;
end;

procedure TIDB_MainManager.SetGeoTextFieldNames (AProject, ALayer: Pointer; sGeoTextIDFieldName, sGeoTextFieldName: AnsiString);
var
   iP: Integer;
   pPD: PProjectDef;
   ADescMan: TIDB_DescMan;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];

   ADescMan := pPD.ALayerManager.GetLayerDescMan (ALayer);
   try
      ADescMan.SetGeoTextFieldName (sGeoTextFieldName);
      ADescMan.SetUserChoice;
   except
   end;
end;

{ Now I consider GeoTextID field only as ProgisID field in attribute table.
  So, this function is not called from anywhere. }

function TIDB_MainManager.GetGeoTextIDFieldName (AProject, ALayer: Pointer): AnsiString;
{var
   iP: Integer;
   pPD: PProjectDef;
   ADescMan: TIDB_DescMan;}
begin
{   RESULT := '';
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];

   ADescMan := pPD.ALayerManager.GetLayerDescMan (ALayer);

   try
      // GeoTextIDFieldName always is 'ProgisID'.
      //        RESULT := ADescMan.GetGeoTextIDFieldName;
   finally
      //        ADescMan.Free;
   end;}
end;

function TIDB_MainManager.GetGeoTextFieldName (AProject, ALayer: Pointer): AnsiString;
var
   iP: Integer;
   pPD: PProjectDef;
   ADescMan: TIDB_DescMan;
begin
   RESULT := '';
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];

   ADescMan := pPD.ALayerManager.GetLayerDescMan (ALayer);

   try
      RESULT := ADescMan.GetGeoTextFieldName;
   except
   end;
end;

{ This functions returns a list of names of field's names. It is used in Create Tooltips routines. }

function TIDB_MainManager.GetTableFieldsNamesList (AProject: Pointer; ALayer: Pointer; bInclude_ProgisID_Field: Boolean = TRUE): AnsiString;
var
   AStructMan: TIDB_StructMan;
   sLayerIndex: AnsiString;
   ATempTable: TDTable;
   slFieldNames: TStringList;
   iI, iP: Integer;
   pPD: PProjectDef;
   sFieldName: AnsiString;
begin
   RESULT := '';
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   sLayerIndex := IntToStr (GetLayerIndexFunc (ALayer));
   AStructMan := TIDB_StructMan.Create (AProject, pPD.IDB_DA);
   ATempTable := TDTable.Create (nil);
   try
      ATempTable.Master := pPD.IDB_DA.DMaster;

      if not UseNewATTNames (AProject) then
         ATempTable.TableName := cs_AttributeTableNamePrefix + sLayerIndex
      else
         ATempTable.TableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

      AStructMan.DetectStructure (ATempTable);
      slFieldNames := TStringList.Create;
      for iI := 0 to AStructMan.GetFieldCount - 1 do
      begin
         sFieldName := AStructMan.GetFieldName (iI);
         if (not FieldIsInternalUsageField (sFieldName)) and
            {(not FieldIsCalculatedField (sFieldName)) and }
            CheckFieldTypeSuitability (AStructMan.GetFieldType (sFieldName)) then
            if AnsiUpperCase (sFieldName) = 'PROGISID' then
            begin
               if bInclude_ProgisID_Field then
                  slFieldNames.Add (sFieldName)
            end
            else
               slFieldNames.Add (sFieldName);
      end; // for iI end
   finally
      ATempTable.Close;
      ATempTable.Free;
      AStructMan.Free;
   end;
   RESULT := slFieldNames.Text;
   slFieldNames.Free;
end;

function TIDB_MainManager.GetTableVisibleFieldsNamesList (AProject: Pointer; ALayer: Pointer): AnsiString;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := '';
   iP := SELF.FindProject (AProject);
   if iP > -1 then
   begin
      pPD := SELF.listProjectDefs[iP];
      RESULT := pPD.ALayerManager.GetLayerFieldsVisibilityManager (ALayer).GetVisibleAttributeFieldNames (TRUE);
   end;
end;

{ This function sets the sign which means that when the following put_in_undo_data routines
  will be called, the putting won't be done indeed. Such action can be caused when I make
  UnInsert operation. In this case I haven't put data of deleted objects in Undo. }

procedure TIDB_MainManager.Set_ThereIsNoNecessityToPutDataInUndo_Sign (AProject: Pointer; bValue: Boolean);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.bThereIsNoNecessityToPutDataIntoUndo := bValue;
end;

procedure TIDB_MainManager.Set_DoNotAddAttributeRecords_Sign (AProject: Pointer; bValue: Boolean);
begin
   SELF.Set_IAmGeneratingObjectsByMyself_Sign (AProject, bValue); // This flag is purposed for the same task.
end;

procedure TIDB_MainManager.Set_AnExternalDBIsWorking_Sign (AProject: Pointer; bValue: Boolean);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.bAnExternalDBIsWorking := bValue;
end;

function TIDB_MainManager.GetReadyToReceiveIDsOfObjectsToBeRestored (AProject: Pointer): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := FALSE;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   RESULT := pPD.ALayerManager.GetReadyToReceiveIDsOfObjectsToBeRestored;
end;

procedure TIDB_MainManager.ReceiveIDOfObjectToBeRestored (AProject: Pointer; ALayer: Pointer; iObjectID: Integer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.ALayerManager.ReceiveIDOfObjectToBeRestored (ALayer, iObjectID);
end;

procedure TIDB_MainManager.UpdateCalcFields (AProject: Pointer; ALayer: Pointer; iObjectID: Integer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.ALayerManager.FillCalculatedField (ALayer, iObjectID);
   pPD.ALayerManager.RefreshAllDataWindows;
end;

procedure TIDB_MainManager.ExecuteRestoring (AProject: Pointer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.ALayerManager.ExecuteRestoring;
end;

procedure TIDB_MainManager.Set_IAmGeneratingObjectsByMyself_Sign (AProject: Pointer; bValue: Boolean);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.bIAmGeneratingTheItemsByMyself := bValue;
end;

procedure TIDB_MainManager.InsertItem (AProject, ALayer: Pointer; iNewItemIndex: Integer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   if pPD.bIAmGeneratingTheItemsByMyself then EXIT;
   pPD.ALayerManager.InsertItem (ALayer, iNewItemIndex, pPD.bAnExternalDBIsWorking);
end;

procedure TIDB_MainManager.OverlayInsertItem (AProject, ALayer: Pointer; iNewItemIndex, iAItemIndex, iBItemIndex: Integer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   if pPD.bIAmGeneratingTheItemsByMyself then EXIT;
   if pPD.APolygonOverlayManager = nil then
      pPD.ALayerManager.InsertItem (ALayer, iNewItemIndex, pPD.bAnExternalDBIsWorking)
   else
      pPD.APolygonOverlayManager.InsertItem (ALayer, iNewItemIndex, iAItemIndex, iBItemIndex);
end;

function TIDB_MainManager.GetReadyToCopyAttributeInfo (AProject, ASourceLayer, ADestLayer: Pointer; var bUseSourceFields: Boolean): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
   ASourceQuery, ADestQuery: TDQuery;
   sSourceTableName, sDestTableName: AnsiString;
   bAddField: boolean;
   iI: Integer;
   ASourceField, ADestField: TField;
   AStructman: TIDB_Structman;
begin
   RESULT := false;
   if ASourceLayer = ADestLayer then
      EXIT;
   if lastlayername<>ADestLayer then createnewfields:=True;
   iP := SELF.FindProject (AProject);
   if iP > -1 then
   begin
      pPD := listProjectDefs[iP];
      AStructMan := TIDB_StructMan.Create (AProject, pPD.IDB_DA);
      iP := pPD.ALayerManager.FindLayer (ASourceLayer);
      if iP < 0 then
         iP := pPD.ALayerManager.AddLayer (ASourceLayer);
      if iP < 0 then
      begin
         RESULT := false;
         exit;
      end;

      iP := pPD.ALayerManager.FindLayer (ADestLayer);
      if iP < 0 then
         iP := pPD.ALayerManager.AddLayer (ADestLayer);
      if iP < 0 then
      begin
         RESULT := false;
         exit;
      end;

      if not UseNewATTNames (AProject) then
         sSourceTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ASourceLayer))
      else
         sSourceTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ASourceLayer);

      ASourceQuery := TDQuery.Create (nil);
      ASourceQuery.Master := pPD.IDB_DA.DMaster;
      ASourceQuery.SQL.Text := 'SELECT * FROM [' + sSourceTableName + '] ';

      try
         ASourceQuery.Open;
      except
         ASourceQuery.Free;
         RESULT := false;
         exit;
      end;

      if not UseNewATTNames (AProject) then
         sDestTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ADestLayer))
      else
         sDestTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ADestLayer);

      ADestQuery := TDQuery.Create (nil);
      ADestQuery.Master := pPD.IDB_DA.DMaster;
      ADestQuery.SQL.Text := 'SELECT * FROM [' + sDestTableName + '] ';
      try
         ADestQuery.Open;
      except
         ASourceQuery.Close;
         ASourceQuery.Free;
         ADestQuery.Free;
         RESULT := false;
         EXIT;
      end;

      bAddField := false;
      bUseSourceFields := true;
      for iI := 0 to ASourceQuery.Fields.Count - 1 do
      begin
         ASourceField := ASourceQuery.Fields[iI];
         ADestField := ADestQuery.FindField (ASourceField.FieldName);
         if (AnsiUpperCase (ASourceField.FieldName) <> 'PROGISID')
            and (AnsiUpperCase (ASourceField.FieldName) <> 'GUID')
            and (not FieldIsInternalUsageField (ASourceField.FieldName))
            and (not FieldIsCalculatedField (ASourceField.FieldName)) then
         begin
            ADestField := ADestQuery.FindField (ASourceField.FieldName);
            if ADestField = nil then
            begin
               if (not bAddField) and createnewfields then
               begin
                  if MessageBox (Application.Handle, PChar (ALangManager.GetValue (425)), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrYES then
                     bAddField := true
                  else
                  begin
                     bUseSourceFields := false;
                     break;
                  end;
               end;
               if bAddField then
               begin
                  ADestQuery.Close;
                  pPD.IDB_DA.ADbItemsMan.AddField (sDestTableName, ASourceField.FieldName, Integer (ASourceField.DataType), ASourceField.DataSize);
                  ADestQuery.Open;
               end;
            end;
         end;
      end;
      AStructman.Free;
      ASourceQuery.Free;
      ADestQuery.Free;

      RESULT := true;
   end;
lastlayername:=ADestLayer;
end;

function TIDB_MainManager.MoveAttributeInfo (AProject, ADestLayer: Pointer): Integer;
var
   iP: Integer;
//   pPD: PProjectDef;
   ASourceLayer: Pointer;
   iI: Integer;
   iSelectedItemsCount: Integer;
   bUseSourceFields: Boolean;
begin
   Result := 0;

   iP := SELF.FindProject (AProject);
   if iP > -1 then
   begin
//      pPD := listProjectDefs[iP];

      ShowPercentAtWinGISProc (0, 1456, 0);
      for iI := 1 to GetProjectLayerNumberFunc (AProject) - 1 do
      begin
         ASourceLayer := GetLayerPointerByPositionFunc (AProject, iI);
         if (ASourceLayer <> nil) then
         begin
            iSelectedItemsCount := GetLayerItemsCountFunc (AProject, ASourceLayer, true);
            if iSelectedItemsCount > 0 then
            begin
               if not GetReadyToCopyAttributeInfo (AProject, ASourceLayer, ADestLayer, bUseSourceFields) then
                  Continue;
               CopyAttributeInfoSuperNew (AProject, ASourceLayer, ADestLayer, bUseSourceFields);
            end;
         end;          
      end;

      if SELF.AViewForm <> nil then
         SELF.AViewForm.Create_Power_Broker;

      ShowPercentAtWinGISProc (2, 0, 0);

   end;
end;

function TIDB_MainManager.CopyAttributeInfoSuperNew (AProject, ASourceLayer, ADestLayer: Pointer; bUseSourceFields: Boolean): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
   AQuery: TDQuery;
   sSourceTableName, sDestTableName: AnsiString;
   sFieldName: AnsiString;
   slFieldNames: TStringList;
   sDestFieldNames: AnsiString;
   sIDs: AnsiString;
   iI: Integer;
   iItemID: Integer;
   iObjectCount, Type1, Type2: Integer;
   AStructman: TIDB_Structman;
   ATempTable:TDTAble;
   hs: string;
begin
   RESULT := false;
   iP := SELF.FindProject (AProject);
   if iP > -1 then
   begin
      pPD := listProjectDefs[iP];

      AQuery := TDQuery.Create (nil);
      AQuery.Master := pPD.IDB_DA.DMaster;

      if not UseNewATTNames (AProject) then
      begin
         sSourceTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ASourceLayer));
         sDestTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ADestLayer));
      end
      else
      begin
         sSourceTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ASourceLayer);
         sDestTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ADestLayer);
      end;

      AQuery.SQL.Text := 'SELECT * FROM [' + sSourceTableName + ']';

      try
         AQuery.Open;
      except
         AQuery.Free;
         exit;
      end;

      slFieldNames := TStringList.Create;
      for iI := 0 to AQuery.Fields.Count - 1 do
      begin
         sFieldName := AQuery.Fields[iI].FieldName;
         if (not FieldIsCalculatedField (sFieldName)) and
            (not FieldIsInternalUsageField (sFieldName) or (sFieldName = cs_StateFieldName)) then
            slFieldNames.Add (sFieldName);
      end;
      hs:=slFieldnames.text;
      if bUseSourceFields then
         for iI := 0 to slFieldNames.Count - 1 do
            begin
            sFieldName := AQuery.Fields[iI].FieldName;
            AStructMan := TIDB_StructMan.Create (AProject, pPD.IDB_DA);
            ATempTable := TDTable.Create (nil);
            ATempTable.Master := pPD.IDB_DA.DMaster;
            ATempTable.TableName:=sSourceTableName;
            AStructMan.DetectStructure (ATempTable);
            type1:=AStructman.GetFieldtype(slFieldNames[iI]);
            AStructMan.free;
            slFieldnames.text:=hs;
            AStructMan := TIDB_StructMan.Create (AProject, pPD.IDB_DA);
            ATempTable.TableName:=sDestTableName;
            AStructMan.DetectStructure (ATempTable);
            type2:=AStructman.GetFieldtype(slFieldNames[iI]);
            ATempTable.free;
            AStructMan.Free;
            slFieldnames.text:=hs;
            if (type1=type2) or (slFieldNames[iI]=cs_StateFieldName) then
               sDestFieldNames := sDestFieldNames + '[' + sSourceTableName + '].[' + slFieldNames[iI] + '], ';
            end
      else
      begin
         AQuery.SQL.Text := 'SELECT * FROM [' + sDestTableName + ']';

         try
            AQuery.Open;
         except
            AQuery.Free;
            slFieldNames.Free;
            exit;
         end;

         for iI := 0 to AQuery.Fields.Count - 1 do
         begin
            sFieldName := AQuery.Fields[iI].FieldName;
            AStructMan := TIDB_StructMan.Create (AProject, pPD.IDB_DA);
            ATempTable := TDTable.Create (nil);
            ATempTable.Master := pPD.IDB_DA.DMaster;
            ATempTable.TableName:=sSourceTableName;
            AStructMan.DetectStructure (ATempTable);
            type1:=AStructman.GetFieldType(sFieldName);
            ATempTable.TableName:=sDestTableName;
            AStructMan.DetectStructure (ATempTable);
            type2:=AStructman.GetFieldType(sFieldName);
            ATempTable.free;
           AStructMan.Free;

            if (not FieldIsCalculatedField (sFieldName)) and
               (not FieldIsInternalUsageField (sFieldName) or (sFieldName = cs_StateFieldName)) and
               (slFieldNames.IndexOf (sFieldName) <> -1)
               and ((type1=type2) or (slFieldNames[iI]=cs_StateFieldName)) then
               sDestFieldNames := sDestFieldNames + '[' + sSourceTableName + '].[' + sFieldName + '], ';
         end;
      end;
      sDestFieldNames := Copy (sDestFieldNames, 1, Length (sDestFieldNames) - 2);
      slFieldNames.Free;

      iObjectCount := GetLayerItemsCountFunc (AProject, nil, true);

      for iI := 0 to iObjectCount - 1 do
      begin

         iItemID := GetLayerItemIndexByNumberFunc (AProject, nil, iI, true);
         if iItemID = -1 then
            Continue;
         try
            sIDs := sIDs + '[PROGISID] = ' + IntToStr (iItemID) + ' OR ';
         except
         end;

         if ((iI > 0) and (iI mod ci_PackLength = 0)) or (iI = iObjectCount - 1) then
         begin
            ShowPercentAtWinGISProc (1, 1456, Round (iI * 100 / iObjectCount));
            if sIDs = '' then
               Continue;
            sIDs := Copy (sIDs, 1, Length (sIDs) - 4);

            AQuery.SQL.Text := 'DELETE FROM [' + sDestTableName + '] ' +
               'WHERE (' + sIDs + ')';
            try
               AQuery.ExecSQL;
            except
            end;

            AQuery.SQL.Text := 'INSERT INTO [' + sDestTableName + '] ' +
               'SELECT ' + sDestFieldNames + ' FROM [' + sSourceTableName + '] ' +
               'WHERE (' + sIDs + ')';
            try
               AQuery.ExecSQL;
               RESULT := true;
            except
            end;
            sIDs := '';
         end;
      end;

      AQuery.Free;

   end;
end;

function TIDB_MainManager.CopyAttributeInfo (AProject, ASourceLayer, ADestLayer: Pointer; iSourceItemID, iDestItemID: Integer): integer;
var
   iP: Integer;
   pPD: PProjectDef;
   ASourceQuery, ADestQuery: TDQuery;
   sSourceItemID, sDestItemID,
      sSourceTableName, sDestTableName: AnsiString;
   ASourceField, ADestField: TField;
   iI: Integer;
   bAllTheFieldsArePresented, bThereAreConverisonErrors: Boolean;
   bAddField: boolean;
//   sMessage: AnsiString;
begin
   Result := 0;
   if (ASourceLayer = ADestLayer) and (iSourceItemID = iDestItemID) then
      EXIT;
   if lastlayername<>ADestLayer then createnewfields:=True;
   iP := SELF.FindProject (AProject);
   if iP > -1 then
   begin
      pPD := listProjectDefs[iP];

      iP := pPD.ALayerManager.FindLayer (ASourceLayer);
      if iP < 0 then
         iP := pPD.ALayerManager.AddLayer (ASourceLayer);
      if iP < 0 then
      begin
         Result := 3;
         exit;
      end;

      iP := pPD.ALayerManager.FindLayer (ADestLayer);
      if iP < 0 then
         iP := pPD.ALayerManager.AddLayer (ADestLayer);
      if iP < 0 then
      begin
         Result := 3;
         exit;
      end;

      try
         sSourceItemID := IntToStr (iSourceItemID);
         sDestItemID := IntToStr (iDestItemID);
      except
         Result := 3;
         EXIT;
      end;

      if not UseNewATTNames (AProject) then
         sSourceTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ASourceLayer))
      else
         sSourceTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ASourceLayer);

      ASourceQuery := TDQuery.Create (nil);
      ASourceQuery.Master := pPD.IDB_DA.DMaster;
      ASourceQuery.SQL.Text := 'SELECT * FROM [' + sSourceTableName + '] ' +
         'WHERE PROGISID = ' + sSourceItemID;
      try
         ASourceQuery.Open;
      except
         ASourceQuery.Free;
         Result := 3;
         exit;
      end;

      if not UseNewATTNames (AProject) then
         sDestTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ADestLayer))
      else
         sDestTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ADestLayer);

      ADestQuery := TDQuery.Create (nil);
      ADestQuery.Master := pPD.IDB_DA.DMaster;
      ADestQuery.SQL.Text := 'SELECT * FROM [' + sDestTableName + '] ' +
         'WHERE PROGISID = ' + sDestItemID;
      try
         ADestQuery.Open;
      except
         ASourceQuery.Close;
         ASourceQuery.Free;
         ADestQuery.Free;
         Result := 3;
         EXIT;
      end;
      if ADestQuery.RecordCount < 1 then
      begin
         ADestQuery.Close;
         try
            ADestQuery.ExecSQL ('INSERT INTO [' + sDestTableName + '] (PROGISID, [' + cs_StateFieldName + ']) ' +
               'VALUES (' + sDestItemID + ', ' + cs_ObjectIsJustAdded + ')');
         except
         end;
         ADestQuery.SQL.Text := 'SELECT * FROM [' + sDestTableName + '] ' +
            'WHERE PROGISID = ' + sDestItemID;
         try
            ADestQuery.Open;
         except
            ASourceQuery.Close;
            ASourceQuery.Free;
            ADestQuery.Free;
            Result := 3;
            EXIT;
         end;
      end;

      bAddField := false;
      for iI := 0 to ASourceQuery.Fields.Count - 1 do
      begin
         ASourceField := ASourceQuery.Fields[iI];
         if (AnsiUpperCase (ASourceField.FieldName) <> 'PROGISID')
            and (AnsiUpperCase (ASourceField.FieldName) <> 'GUID')
            and (not FieldIsInternalUsageField (ASourceField.FieldName))
            and (not FieldIsCalculatedField (ASourceField.FieldName)) then
         begin
            ADestField := ADestQuery.FindField (ASourceField.FieldName);
            if ADestField = nil then
            begin
               if (not bAddField) and createnewfields then
               begin
                  if MessageBox (Application.Handle, PChar (ALangManager.GetValue (425)), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrYES then
                     bAddField := true
                  else
                     begin
                     createnewfields:=False;
                     break;
                     end;
               end;
               if bAddField then
               begin
                  ADestQuery.Close;
                  pPD.IDB_DA.ADbItemsMan.AddField (sDestTableName, ASourceField.FieldName, Integer (ASourceField.DataType), ASourceField.DataSize);
                  ADestQuery.Open;
               end;
            end;
         end;
      end;

      ADestQuery.Edit;

      bAllTheFieldsArePresented := TRUE;
      bThereAreConverisonErrors := FALSE;
      for iI := 0 to ASourceQuery.Fields.Count - 1 do
      begin
         ASourceField := ASourceQuery.Fields[iI];
         if (AnsiUpperCase (ASourceField.FieldName) <> 'PROGISID')
            and (AnsiUpperCase (ASourceField.FieldName) <> 'GUID')
            and (not FieldIsInternalUsageField (ASourceField.FieldName))
            and (not FieldIsCalculatedField (ASourceField.FieldName)) then
         begin
            ADestField := ADestQuery.FindField (ASourceField.FieldName);
            if ADestField <> nil then
            begin
               try
                  ADestField.AsString := ASourceField.AsString;
               except
                  bThereAreConverisonErrors := TRUE;
               end;
            end
            else
            begin
               bAllTheFieldsArePresented := FALSE;
            end;
         end;
      end; // for iI end
      ADestQuery.Post;

      pPD.ALayerManager.FillCalculatedField (ADestLayer, iDestItemID);

      if not bAllTheFieldsArePresented then
         Result := 1;
      if bThereAreConverisonErrors then
         Result := 2;
      if (not bAllTheFieldsArePresented) and (bThereAreConverisonErrors) then
         Result := 3;

      ASourceQuery.Free;
      ADestQuery.Free;
   end;
lastlayername:=ADestLayer;
end;

function TIDB_MainManager.InsertRecord (AProject, ALayer: Pointer; iItemIndex: Integer): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := FALSE;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   RESULT := pPD.ALayerManager.InsertRecord (ALayer, iItemIndex);
end;

function TIDB_MainManager.X_GotoObjectRecord (AProject, ALayer: Pointer; iItemIndex: Integer): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := FALSE;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
   RESULT := pPD.ALayerManager.X_GotoObjectRecord (ALayer, iItemIndex);
end;

function TIDB_MainManager.X_AddRecord (AProject, ALayer: Pointer; iItemIndex: Integer): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := FALSE;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
   RESULT := pPD.ALayerManager.X_AddRecord (ALayer, iItemIndex);
end;

function TIDB_MainManager.X_GetFieldCount (AProject, ALayer: Pointer): Integer;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := 0;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
   RESULT := pPD.ALayerManager.X_GetFieldCount (ALayer);
end;

function TIDB_MainManager.X_GetFieldName (AProject, ALayer: Pointer; iFieldIndex: Integer): AnsiString;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := 'ProgisID';
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
   RESULT := pPD.ALayerManager.X_GetFieldName (ALayer, iFieldIndex);
end;

function TIDB_MainManager.X_GetFieldValue (AProject, ALayer: Pointer; sFieldName: AnsiString): Variant;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := 0;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
   RESULT := pPD.ALayerManager.X_GetFieldValue (ALayer, sFieldName);
end;

function TIDB_MainManager.X_GetFieldValue (AProject, ALayer: Pointer; iFieldIndex: Integer): Variant;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := 0;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
   RESULT := pPD.ALayerManager.X_GetFieldValue (ALayer, iFieldIndex);
end;

procedure TIDB_MainManager.X_SetFieldValue (AProject, ALayer: Pointer; sFieldName: AnsiString; FieldValue: Variant);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
   pPD.ALayerManager.X_SetFieldValue (ALayer, sFieldName, FieldValue);
end;

procedure TIDB_MainManager.X_SetFieldValue (AProject, ALayer: Pointer; iFieldIndex: Integer; FieldValue: Variant);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
   pPD.ALayerManager.X_SetFieldValue (ALayer, iFieldIndex, FieldValue);
end;

procedure TIDB_MainManager.DeleteLayerTables (AProject, ALayer: Pointer);
var
   iP: Integer;
   pPD: PProjectDef;
// ++ Cadmensky Version 2.3.3
   sAttTableName: AnsiString;
// -- Cadmensky Version 2.3.3
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
// ++ Cadmensky Version 2.3.3
   if not pPD.bUseNewATTNames then
      sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
   else
      sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);
// -- Cadmensky Version 2.3.3
   pPD.ALayerManager.DeleteLayerTables (ALayer, sAttTableName);
end;

function TIDB_MainManager.LoadGeoTextData (AProject, ALayer: Pointer; sSourceDBFileName, sSourceFieldName, sDestinationFieldName, sProgisIDFieldName: AnsiString): Boolean;
var
   pPD: PProjectDef;
   iP: Integer;
   ADataManager: TIDB_DBDataManager;
   sExtension: AnsiString;
begin
   RESULT := FALSE;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
   ADataManager := TIDB_DBDataManager.Create (pPD.IDB_DA, AProject, ALayer, nil, '');
   try
      ADataManager.SetExistingDataMustBeRewritedSign (TRUE);
      ADataManager.SetInsertNewRecordSign (TRUE);
      ADataManager.SetColumnWithProgisIDData (sProgisIDFieldName);
      ADataManager.ClearLinkedData;
      ADataManager.AddLinkedData (sSourceFieldName, sDestinationFieldName);

      sExtension := AnsiUpperCase (ExtractFileExt (sSourceDBFileName));
      if sExtension = '.DBF' then
         RESULT := ADataManager.LoadData (wmDBase5Table, sSourceDBFileName)
      else
         if sExtension = '.DB' then
            RESULT := ADataManager.LoadData (wmParadox7Table, sSourceDBFileName);
   finally
      ADataManager.Free;
      SELF.RefreshAllDataWindows (AProject);
   end;
end;

function TIDB_MainManager.GetReadyToReceiveIDsOfObjectsToBeMade (AProject: Pointer): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := FALSE;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
   RESULT := pPD.ALayerManager.GetReadyToReceiveIDsOfObjectsToBeMade;
end;

procedure TIDB_MainManager.ReceiveIDOfObjectToBeMade (AProject, ALayer: Pointer; iItemID: Integer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
   pPD.ALayerManager.ReceiveIDOfObjectToBeMade (ALayer, iItemID);
end;

procedure TIDB_MainManager.MakeAnnotations (AProject: Pointer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
   pPD.bIAmGeneratingTheItemsByMyself := TRUE;
   pPD.ALayerManager.MakeAnnotations;
   pPD.bIAmGeneratingTheItemsByMyself := FALSE;
end;

procedure TIDB_MainManager.MakeSpecialChart (AProject, Alayer: Pointer);
{var
   iP: Integer;
   pPD: PProjectDef;}
begin;
{   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
   //  pPD.ALayerManager.MakeSpecialChart(Aproject);}
end;

procedure TIDB_MainManager.SendSpecialforChart (AProject: Pointer; PseudoDDEString: Ansistring);
{
var
   iP: Integer;
   pPD: PProjectDef;}
begin;
{   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
   //   pPD.ALayerManager.SendSpecialforChart(AProject,PseudoDDEString);}
end;

procedure TIDB_MainManager.MakeChart (AProject: Pointer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
   pPD.bIAmGeneratingTheItemsByMyself := TRUE;
   pPD.ALayerManager.MakeChart;
   pPD.bIAmGeneratingTheItemsByMyself := FALSE;
end;

function TIDB_MainManager.GetGeoText (AProject: Pointer; iLayerIndex, iItemID: Integer {; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString}): AnsiString;
var
   iP: Integer;
   pPD: PProjectDef;
   AQuery: TDQuery;
   ALayer: Pointer;
   ADescMan: TIDB_DescMan;
begin
   RESULT := '';
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;

   pPD := SELF.listProjectDefs[iP];
   ALayer := GetLayerPointerByIndexFunc (AProject, iLayerIndex);
   ADescMan := pPD.ALayerManager.GetLayerDescMan (ALayer);
   AQuery := TDQuery.Create (nil);
   try
      AQuery.Master := pPD.IDB_DA.DMaster;
      AQuery.SQL.Clear;
//      AQuery.SQL.Add ('SELECT [' + sGeoTextValueFieldName + ']');
      AQuery.SQL.Add ('SELECT [' + ADescMan.GetGeoTextFieldName + ']');

      if not UseNewATTNames (AProject) then
         AQuery.SQL.Add ('FROM [' + cs_AttributeTableNamePrefix + IntToStr (iLayerIndex) + ']')
      else
         AQuery.SQL.Add ('FROM [' + cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer) + ']');
      AQuery.SQL.Add ('WHERE [PROGISID] = ' + IntToStr (iItemID));
//      AQuery.SQL.Add ('WHERE [' + sGeoTextIDFieldName + '] = ' + IntToStr (iItemID));
      try
         AQuery.Open;
         RESULT := AQuery.Fields[0].AsString;
      except
      end;
   finally
      AQuery.Close;
      AQuery.Free;
   end;
end;

function TIDB_MainManager.SetGeoText (AProject: Pointer; iLayerIndex, iItemID: Integer; {sGeoTextIDFieldName, sGeoTextValueFieldName,} sGeoTextValue: AnsiString): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
   sTableName, sItemID, sSQLText: AnsiString;
   AQuery: TDQuery;
   ALayer: Pointer;
   ADescMan: TIDB_DescMan;
begin
   RESULT := false;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];

   ALayer := GetLayerPointerByIndexFunc (AProject, iLayerIndex);
   ADescMan := pPD.ALayerManager.GetLayerDescMan (ALayer);

   if AnsiUpperCase (ADescMan.GetGeoTextFieldName) = 'PROGISID' then
      exit;

   AQuery := TDQuery.Create (nil);
   AQuery.Master := pPD.IDB_DA.DMaster;

   if not UseNewATTNames (AProject) then
      sTableName := cs_AttributeTableNamePrefix + IntToStr (iLayerIndex)
   else
      sTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

   sItemID := IntToStr (iItemID);
   sGeoTextValue := ReplaceChars (sGeoTextValue, '''', '"');

   try
      if AQuery.Active then AQuery.Close;
      AQuery.SQL.Clear;
      // I want to know is a record for this object exist or not.
      AQuery.SQL.Add ('SELECT [' + ADescMan.GetGeoTextFieldName + ']');
      AQuery.SQL.Add ('FROM [' + sTableName + ']');
      AQuery.SQL.Add ('WHERE [PROGISID] = ' + sItemID);
//      AQuery.SQL.Add ('WHERE [' + sGeoTextIDFieldName + '] = ' + sItemID);
      AQuery.Open;
      if AQuery.RecordCount > 0 then
         // UPDATE table_name SET field_name = new_value WHERE key_field = key_value
         sSQLText := 'UPDATE [' + sTableName + '] ' +
            'SET [' + ADescMan.GetGeoTextFieldName + '] = ''' + sGeoTextValue + ''' ' +
            'WHERE [PROGISID] = ' + sItemID
//            'WHERE [' + sGeoTextIDFieldName + '] = ' + sItemID
      else
         // INSERT INTO table_name (field_name_1, field_name_2) VALUES (value_1, value_2)
         sSQLText := 'INSERT INTO [' + sTableName + '] ' +
            '([PROGISID], [' + ADescMan.GetGeoTextFieldName + ']) ' +
//            '([' + sGeoTextIDFieldName + '], [' + sGeoTextValueFieldName + ']) ' +
         'VALUES (' + sItemID + ', ''' + sGeoTextValue + ''')';
      AQuery.Close;
      try
         AQuery.ExecSQL (sSQLText);
         RESULT := true;
      except
      end;
   finally
      AQuery.Free;
   end;
end;

function TIDB_MainManager.GetReadyToInsertRecords (AProject, ALayer: Pointer; ARecordTemplate: TList): Integer;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := -1;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
   // ++ Cadmensky
   //     RESULT := SELF.AnInsertManager.AddLayer(AProject, ALayer, pPD.IDB_DA.DMaster, ARecordTemplate);
   RESULT := pPD.ALayerManager.GetReadyToInsertRecords (ALayer, ARecordTemplate);
   // -- Cadmensky
end;

// ++ Commented by Cadmensky Version 2.2.8
{procedure TIDB_MainManager.EndInserting (iPosition: Integer);
begin
   // Commented by Cadmensky
   //     SELF.AnInsertManager.DeleteLayer(iPosition);
end;}
// -- Commented by Cadmensky Version 2.2.8

// ++ Commented by Cadmensky Version 2.3.3
{procedure TIDB_MainManager.CopyData (AProject, ASourceLayer, ADestLayer: Pointer; iItemID: Integer);
var
   ASourceTable, ADestTable: TDTable;

   procedure ExitActions;
   begin
      ASourceTable.Close;
      ASourceTable.Free;

      ADestTable.Close;
      ADestTable.Free;
   end;

var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];

   iP := GetLayerIndexFunc (ASourceLayer);
   ASourceTable := TDTable.Create (nil);
   ASourceTable.Master := pPD.ALayerManager.DataAccessObject.DMaster;
   if not UseNewATTNames (AProject) then
      ASourceTable.TableName := cs_AttributeTableNamePrefix + IntToStr (iP)
   else
      ASourceTable.TableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ASourceLayer);

   iP := GetLayerIndexFunc (ADestLayer);
   ADestTable := TDTable.Create (nil);
   ADestTable.Master := ASourceTable.Master;

   if not UseNewATTNames (AProject) then
      ADestTable.TableName := cs_AttributeTableNamePrefix + IntToStr (iP)
   else
      ADestTable.TableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ADestLayer);

   try
      ASourceTable.Open;
   except
      ExitActions;
      EXIT;
   end;

   try
      ADestTable.Open;
   except
      ExitActions;
      EXIT;
   end;

   try
      // We should only work if the source table has data about layer item.
      if ASourceTable.Locate ('PROGISID', iItemID, []) then
      begin
         // Check if the destination table already has data about such item.
         if ADestTable.Locate ('PROGISID', iItemID, []) then
         begin
         end
         else
         begin
         end;
      end;
   finally
      ExitActions;
   end;
end;}
// -- Commented by Cadmensky Version 2.3.3

// ++ Commented by Cadmensky Version 2.3.2
{ This procedure finds all files of external tables pools that could be left in the TEMP
  directory when WinGIS crashes and such files cannot be deleted in normal way.
  if extarnal tables pools are found, they will be deleted. Also I will try to delete
  lock (*.ldb) files of MS Access. }

{procedure TIDB_MainManager.KillAllExternalTablesPool;
var
   sTempDir: AnsiString;
   ASearchRec: TSearchRec;
   slFileNames: TStringList;
   iI: Integer;
begin
   slFileNames := TStringList.Create;
   sTempDir := GetTempDir;
   try
      if FindFirst (sTempDir + '*.' + cs_IDB_ExtTablesPoolFileExt, faAnyFile, ASearchRec) = 0 then
      begin
         slFileNames.Add (sTempDir + ASearchRec.Name);
         while FindNext (ASearchRec) = 0 do
            slFileNames.Add (sTempDir + ASearchRec.Name);
      end;
   finally
      SysUtils.FindClose (ASearchRec);
   end;
   for iI := 0 to slFileNames.Count - 1 do
   begin
      DeleteFile (PChar (Copy (slFileNames[iI], 1, Length (slFileNames[iI]) - 3) + 'ldb')); // *.ldb file
      DeleteFile (PChar (slFileNames[iI])); // *.etp file
   end;
   slFileNames.Clear;
   slFileNames.Free;
end;}
// -- Commented by Cadmensky Version 2.3.2

procedure TIDB_MainManager.DetectMonitoringFormState (AProject: Pointer; AForm: TForm;AutoZoomOrSelect: Boolean);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
   pPD := listProjectDefs[iP];
// ++ Cadmensky Version 2.3.7
   pPD.AFormPosManager.StoreFormState (AForm, TIDB_MonitorForm (AForm).LeftPanel.Visible,AutoZoomOrSelect);
//   pPD.AMonitoringFormPosManager.StoreFormState (AForm, TIDB_MonitorForm (AForm).LeftPanel.Visible);
// -- Cadmensky Version 2.3.7
end;

function TIDB_MainManager.GetFormPositionManager (AProject: Pointer): TIDB_FormPositionManager;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := nil;
   iP := SELF.FindProject (AProject);
   if iP < 0 then
      Exit;
   pPD := listProjectDefs[iP];
   RESULT := pPD.AFormPosManager;
end;

procedure TIDB_MainManager.DetectInfoFormState (AForm: TForm);
begin
   SELF.AnInfoFormPositionManager.StoreFormState (AForm);
end;

function TIDB_MainManager.GetReadyToPolygonOverlaying (AProject, ASourceLayer1, ASourceLayer2, ADestinationLayer: Pointer): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := FALSE;
   iP := SELF.FindProject (AProject);
   if iP > -1 then
   begin
      pPD := SELF.listProjectDefs[iP];
      pPD.APolygonOverlayManager := TIDB_PolygonOverlayManager.Create (pPD.IDB_DA, AProject, ASourceLayer1, ASourceLayer2, ADestinationLayer);
      RESULT := pPD.APolygonOverlayManager.MakeParams;
      if not RESULT then
      begin
         pPD.APolygonOverlayManager.Free;
         pPD.APolygonOverlayManager := nil;
      end
   end;
end;

procedure TIDB_MainManager.EndOfPolygonOverlaying (AProject: Pointer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP > -1 then
   begin
      pPD := SELF.listProjectDefs[iP];
// ++ Cadmensky Version 2.3.5
      pPD.ALayerManager.RefreshAllDataWindows;
// -- Cadmensky Version 2.3.5
      if pPD.APolygonOverlayManager <> nil then
      begin
         try
            pPD.APolygonOverlayManager.Free;
         except
         end;
         pPD.APolygonOverlayManager := nil;
      end;
   end;
end;

procedure TIDB_MainManager.UpdateAnnotationData (AProject: Pointer; iLayerIndex, iItemID: Integer; AnAnnotData: PCollection);
var
   AQuery: TDQuery;
   iI, iP: Integer;
   AnAnnot: PAnnot;
   pPD: PProjectDef;
   sFieldNames: AnsiString;
   AField: TField;
   sData: AnsiString;
   ALayer: Pointer;
begin
   iP := SELF.FindProject (AProject);
   if (iP > -1) and (iLayerIndex > 0) then
   begin
      pPD := listProjectDefs[iP];
      AQuery := TDQuery.Create (nil);
      try
         AQuery.Master := pPD.IDB_DA.DMaster;

         sFieldNames := '';
         for iI := 0 to AnAnnotData.Count - 1 do
         begin
            AnAnnot := AnAnnotData.At (iI);
            sFieldNames := sFieldNames + '[' + AnAnnot.LineName + '], ';
         end; // for iI end
         sFieldNames := Copy (sFieldNames, 1, Length (sFieldNames) - 2);

         if not UseNewATTNames (AProject) then
            AQuery.SQL.Text := 'SELECT ' + sFieldNames + ' ' +
               'FROM ' + cs_AttributeTableNamePrefix + IntToStr (iLayerIndex) + ' ' +
               'WHERE PROGISID = ' + IntToStr (iItemID)
         else
         begin

            ALayer := GetLayerPointerByIndexFunc (AProject, iLayerIndex);
            AQuery.SQL.Text := 'SELECT ' + sFieldNames + ' ' +
               'FROM ' + cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer) + ' ' +
               'WHERE PROGISID = ' + IntToStr (iItemID);
         end;

         try
            AQuery.Open;
         except
            EXIT;
         end;
         for iI := 0 to AnAnnotData.Count - 1 do
         begin
            AnAnnot := AnAnnotData.At (iI);
            AField := AQuery.FindField (AnAnnot.LineName);
            if AField <> nil then
            begin
               sData := AField.AsString;
               if sData <> '' then
               begin
                  StrDispose (AnAnnot.LineValue);
                  AnAnnot.LineValue := StrNew (PChar (sData));;
                  AnAnnot.HasValue := TRUE;
               end
               else
               begin
                  StrDispose (AnAnnot.LineValue);
                  AnAnnot.HasValue := FALSE;
               end;
            end
            else
            begin
               StrDispose (AnAnnot.LineValue);
               AnAnnot.HasValue := FALSE;
            end;
         end; // for iI end
      finally
         AQuery.Close;
         AQuery.Free;
      end;
   end;
end;

procedure TIDB_MainManager.UpdateChartData (AProject: Pointer; iLayerIndex, iItemID: Integer; AnAnnotData: PCollection);
var
   AQuery: TDQuery;
   iI, iP: Integer;
   AnAnnot: PAnnot;
   pPD: PProjectDef;
   sFieldNames: AnsiString;
   AField: TField;
   sData: AnsiString;
   ALayer: Pointer;
begin
   iP := SELF.FindProject (AProject);
   if (iP > -1) and (iLayerIndex > 0) then
   begin
      pPD := listProjectDefs[iP];
      AQuery := TDQuery.Create (nil);
      try
         AQuery.Master := pPD.IDB_DA.DMaster;

         sFieldNames := '';
         for iI := 0 to AnAnnotData.Count - 1 do
         begin
            AnAnnot := AnAnnotData.At (iI);
            sFieldNames := sFieldNames + '[' + AnAnnot.LineName + '], ';
         end; // for iI end
         sFieldNames := Copy (sFieldNames, 1, Length (sFieldNames) - 2);

         if not UseNewATTNames (AProject) then
            AQuery.SQL.Text := 'SELECT ' + sFieldNames + ' ' +
               'FROM ' + cs_AttributeTableNamePrefix + IntToStr (iLayerIndex) + ' ' +
               'WHERE PROGISID = ' + IntToStr (iItemID)
         else
         begin
            ALayer := GetLayerPointerByIndexFunc (AProject, iLayerIndex);
            AQuery.SQL.Text := 'SELECT ' + sFieldNames + ' ' +
               'FROM ' + cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer) + ' ' +
               'WHERE PROGISID = ' + IntToStr (iItemID);
         end;
         try
            AQuery.Open;
         except
            EXIT;
         end;
         for iI := 0 to AnAnnotData.Count - 1 do
         begin
            AnAnnot := AnAnnotData.At (iI);
            AField := AQuery.FindField (AnAnnot.LineName);
            if AField <> nil then
            begin
               sData := AField.AsString;
               if sData <> '' then
               begin
                  StrDispose (AnAnnot.LineValue);
                  AnAnnot.LineValue := StrNew (PChar (sData));
                  AnAnnot.HasValue := TRUE;
               end
               else
               begin
                  StrDispose (AnAnnot.LineValue);
                  AnAnnot.HasValue := FALSE;
               end;
            end
            else
            begin
               StrDispose (AnAnnot.LineValue);
               AnAnnot.HasValue := FALSE;
            end;
         end; // foriI end
      finally
         AQuery.Close;
         AQuery.Free;
      end;
   end;
end;

function TIDB_MainManager.GetProjectLayerManager (AProject: Pointer): TIDB_LayerManager;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then
      exit;
   pPD := listProjectDefs[iP];
   RESULT := pPD.ALayerManager;
end;

procedure TIDB_MainManager.PackDataBase (sProjectFileName: AnsiString);
var
   AnEngine: IJetEngine;
   sSourceConn,
      sDestinationConn,
      sDBFileName,
      sTempFileName: AnsiString;
   ABuffer: array[0..1023] of Char;
begin

   AnEngine := CoJetEngine.Create;

   sDBFileName := ChangeFileExt (sProjectFileName, '.wgi');
   sSourceConn := 'Provider=Microsoft.Jet.OLEDB.4.0;' +
      'Data Source=' + sDBFileName + ';';

   SetString (sTempFileName, ABuffer, GetTempPath (SizeOf (ABuffer), ABuffer));
   sTempFileName := sTempFileName + 'WG_IDB_TEMP.wgi';
   if FileExists (sTempFileName) then
      if not DeleteFile (PChar (sTempFileName)) then
         exit;

   sDestinationConn := 'Provider=Microsoft.Jet.OLEDB.4.0;' +
      'Data Source=' + sTempFileName + ';';

   if AnEngine <> nil then
   begin
      try

         AnEngine.CompactDatabase (sSourceConn, sDestinationConn);
         DeleteFile (PChar (sDBFileName));
         MoveFile (PChar (sTempFileName), PChar (sDBFileName));
         DeleteFile (PChar (sTempFileName));
      except
      end;
   end;
end;

function TIDB_MainManager.RenameATTTablesIfItNeed (AProject: Pointer; IDA: TIDB_Access2DB_Level2): boolean;
var
   slTablesList: TStringList;
   i: integer;
   AQuery: TDQuery;
   sOldTableName, sNewTableName: AnsiString;
   iLayerIndex: integer;
   ALayer: Pointer;
   sNumber: AnsiString;
   bNewVersion: boolean;
begin
   slTablesList := TStringList.Create;
   AQuery := TDQuery.Create (nil);
   AQuery.Master := IDA.DMaster;
   IDA.ADbItemsMan.GetTablesNames (cs_AttributeTableNamePrefix, slTablesList);

   bNewVersion := true;
   for i := 0 to slTablesList.Count - 1 do
      if Pos ('_', slTablesList.Strings[i]) < 1 then
      begin
         bNewVersion := false;
         break;
      end;

   RESULT := bNewVersion;
   if not bNewVersion then
   begin

      if MessageBox (Application.Handle, PChar (ALangManager.GetValue (438)), PChar (cs_ApplicationName), MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrNO then
         exit;

      for i := 0 to slTablesList.Count - 1 do
         if Pos ('_', slTablesList.Strings[i]) < 1 then
            try
               sNumber := Copy (slTablesList.Strings[i], 4, Length (slTablesList.Strings[i]) - 3);
               sOldTableName := cs_AttributeTableNamePrefix + sNumber;
               iLayerIndex := StrToInt (sNumber);
               ALayer := GetLayerPointerByIndexFunc (AProject, ilayerIndex);
               sNewTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameFunc (ALayer);

               try
                  AQuery.ExecSQL ('SELECT * INTO [' + sNewTableName + '] FROM [' + sOldTableName + ']');
               except
               end;
               try
                  IDA.ADbItemsMan.DeleteTable (sOldTableName);
               except
               end;
            except
            end;
      RESULT := true;
   end;
   slTablesList.Free;
   AQuery.Free;

end;

function TIDB_MainManager.UseNewATTNames (AProject: Pointer): boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := false;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   RESULT := pPD.bUseNewATTNames;
end;

function TIDB_MainManager.GetLayerNamesList (AProject: Pointer): TList;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := nil;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   RESULT := pPD.listLayerNames;
end;

procedure TIDB_MainManager.ShowRecordsForSelectedObjects (AProject: Pointer);
begin
   if SELF.AViewForm <> nil then
      if SELF.AViewForm.AProject = AProject then
      begin
// ++ Commented by Cadmensky Version 2.2.8
//         SELF.AViewForm.MonitoringSB.Enabled := GetLayerItemsCountFunc (AProject, nil) > 0;
// -- Commented by Cadmensky Version 2.2.8
         SELF.AViewForm.ShowRecordsForSelectedObjects;
         exit;
      end;
   if (SELF.AMonitoringForm <> nil) and (SELF.AMonitoringForm.AProject = AProject) then
      if GetLayerItemsCountFunc (AProject, nil) > 0 then
      begin
         if @RunMonitoringProc <> nil then
         begin
            RunMonitoringProc (AProject, false);
            SELF.ShowMonitoringWindow (AProject);
         end;
      end
      else
         SELF.AMonitoringForm.Close;
end;

function TIDB_MainManager.GetLastWasAttributeWindow (AProject: Pointer): boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   RESULT := pPD.bLastWasAttributeWindow;
end;

procedure TIDB_MainManager.SetLastWasAttributeWindow (AProject: Pointer; bValue: boolean);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.bLastWasAttributeWindow := bValue;
end;

procedure TIDB_MainManager.SaveAnnotationSettings(AProject: Pointer;
  iLayerindex: Integer; ASettingsList: String);
var AQuery: TDQuery;
    iP,i : Integer;
    pPD : PProjectDef;
    bExists: Boolean;
    sSQL: String;
begin
iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
bExists:=False;
pPD := listProjectDefs[iP];
AQuery := TDQuery.Create (nil);
AQuery.Master := pPD.IDB_DA.DMaster;
//pPD.IDB_DA.SetCurrentDatabaseGUIDInformation (sGUID);
try SaveLayerTablesNames (pPD.AProject, pPD.listLayerNames, pPD.IDB_DA);
except end;
AQuery.SQL.Text:= 'SELECT * FROM [USYS_WGIDB_DT]';
try
  Aquery.Open;  
  for i := 0 to AQuery.Fields.Count - 1 do
      if AnsiUpperCase (AQuery.Fields[i].FieldName) = 'ANNOTSETTINGS' then bExists:=True;
  if not bExists then
     pPD.IDB_DA.ADbItemsMan.AddField (cs_CommonDescTableName, 'AnnotSettings', 16, -1);
except end;    
try
sSQL:='UPDATE [' + cs_CommonDescTableName +
                 '] SET [AnnotSettings] = "' + ASettingsList +
                 '" WHERE ['+ cs_CDT_LayerIndexFieldName + '] = ' + inttostr(iLayerIndex);
AQuery.ExecSQL(sSQL);
except end;
AQuery.free;
end;

function TIDB_MainManager.GetAnnotationSettings(AProject: Pointer;
  iLayerindex: Integer): String;
var AQuery : TDQuery;
    iP,i : Integer;
    pPD : PProjectDef;
begin
iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
pPD := listProjectDefs[iP];
AQuery := TDQuery.Create (nil);
AQuery.Master := pPD.IDB_DA.DMaster;
AQuery.SQL.Text:= 'SELECT * FROM [USYS_WGIDB_DT] WHERE ['+ cs_CDT_LayerIndexFieldName + '] = ' + inttostr(iLayerIndex);
try
  Aquery.open;
  for i := 0 to AQuery.Fields.Count - 1 do
      if AnsiUpperCase (AQuery.Fields[i].FieldName) = 'ANNOTSETTINGS' then
        begin
        Result:=AQuery.Fields[i].AsString;
        break;
        end;
except end;
AQuery.free;
end;



{ TTooltipThread }
 {
procedure TTimerThread.execute;
var timercounter: integer;
begin
while not terminated do begin
       if (timercounter>99) or (timercounter<41) then timercounter:=41;
       ShowPercentAtWinGISProc (1, 1458, timercounter);
       Application.ProcessMessages;
       sleep(300);
       inc(timercounter)
      end;
end;      }

function TIDB_MainManager.F_GetLinesPerLayer(AProject: Pointer;
  Layername: Pchar): Pchar;
var AQuery : TDQuery;
    iP,i : Integer;
    pPD : PProjectDef;
    tmpstr : String;
    tmplist : TSTringList;
begin
iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
pPD := listProjectDefs[iP];
AQuery := TDQuery.Create (nil);
AQuery.Master := pPD.IDB_DA.DMaster;
AQuery.SQL.Text:= 'SELECT [Linename], [Layernames] FROM [USYS_F_Lines]';
try
  Aquery.open;
  Aquery.First;
  tmplist:=TStringList.Create;
  tmpstr:='';
  For i:= 0  to AQuery.RecordCount - 1 do
        begin
        tmplist.Text:=stringReplace(AQuery.Fields[1].AsString,';', #13,[rfReplaceall]);
        for ip:= 0 to tmplist.Count -1 do
           if uppercase(string(layername))= uppercase(tmplist[ip]) then
              begin
              tmpstr:= tmpstr + AQuery.Fields[0].AsString + #13;
              break;
              end;
        AQuery.Next;
        end;
  tmplist.free;
except end;
Result:=pchar(tmpstr);
AQuery.free;
end;

function TIDB_MainManager.F_GetSymbolsPerLayer(AProject: Pointer;
  Layername: Pchar): Pchar;
var AQuery : TDQuery;
    iP,i : Integer;
    pPD : PProjectDef;
    tmpstr : String;
    tmplist : TSTringList;
begin
iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
pPD := listProjectDefs[iP];
AQuery := TDQuery.Create (nil);
AQuery.Master := pPD.IDB_DA.DMaster;
AQuery.SQL.Text:= 'SELECT [Layernames],[Symbolname] FROM [USYS_F_Symbols]';
try
  Aquery.open;
  Aquery.First;
  tmplist:=TStringList.Create;
  tmpstr:='';
  For i:= 0  to AQuery.RecordCount - 1 do
        begin
        tmplist.Text:=stringReplace(AQuery.Fields[0].AsString,';', #13,[rfReplaceall]);
        for ip:= 0 to tmplist.Count -1 do
           if uppercase(string(layername))= uppercase(tmplist[ip]) then
              begin
              tmpstr:= tmpstr + AQuery.Fields[1].AsString + #13;
              break;
              end;
        AQuery.Next;
        end;
  tmplist.free;
except end;
Result:=pchar(tmpstr);
AQuery.free;
end;

function TIDB_MainManager.F_Symbolallowed(AProject: Pointer; Symbolname,
  LineStyleName: Pchar): Pchar;
var AQuery : TDQuery;
    iP,i : Integer;
    pPD : PProjectDef;
    tmpstr : String;
    tmplist : TSTringList;
begin
iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT;
pPD := listProjectDefs[iP];
AQuery := TDQuery.Create (nil);
AQuery.Master := pPD.IDB_DA.DMaster;
AQuery.SQL.Text:= 'SELECT [Linenames], [Lineparam] FROM [USYS_F_Symbols] WHERE [Symbolname] LIKE ' + string(symbolname);
try
  Aquery.open;
  tmplist:=TStringList.Create;
  tmpstr:='FALSE';
  tmplist.Text:=stringReplace(AQuery.Fields[0].AsString,';', #13,[rfReplaceall]);
  for ip:= 0 to tmplist.Count -1 do
      if uppercase(string(Linestylename))= uppercase(tmplist[ip]) then
            begin
            tmpstr:= AQuery.Fields[0].AsString;
            break;
            end;

  tmplist.free;
except end;
Result:=pchar(tmpstr);
AQuery.free;
end;

end.

