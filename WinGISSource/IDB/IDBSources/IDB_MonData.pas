unit IDB_MonData;
{
Internal database. Ver II.
Module is used for storing monitoring data.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 14-02-2001
}
interface

uses SysUtils, DBTables, Grids, ComCtrls, Classes, Gauges, StdCtrls, Forms,
   DTables, DMaster, TreeView,
   IDB_C_StringGrid,
   IDB_DescMan, IDB_Access2DB, IDB_StructMan, IDB_LayerManager,
   {IDB_ExternalTablesMan,} Dialogs, DB;

type
   TPointerToBoolean = ^Boolean;

   TrecItemInfo = record
      iLayerIndexToThatTheItemBelongs,
      iItemID: Integer;
   end;

   TrecLayersItems = record
      ADBAccessObject: TIDB_Access2DB_Level2;
      iLayerIndex: Integer; // Layer index for access to layer attribute table.
      listObjectIDs: TList; // List with IDs of layer's objects.
      listDispInfo: TStringList; // List with data that will be shown at the left part
      //    of the monitoring window (in the TTreeView).
      AStructManager: TIDB_StructMan;
   end;

   TIDB_MonData = class //(TObject)
   private
      AProject: Pointer;
      ALayerMan: TIDB_LayerManager;
      Data: TList; // The list of TrecLayersItems;
      AQuery: TDQuery;
      bIsDataOpen: boolean;
      function FindLayerInArray (iLayerIndex: Integer): Integer;
//      function FindLayerItemInArray (iLayerPosition: Integer; iLayerItemID: Integer): Integer;
//      function OpenData (iLayerIndex, iLayerItemID: Integer; sFieldsNames: AnsiString): integer;
   public
      constructor Create (AProject: Pointer; ALayerMan: TIDB_LayerManager; AMaster: TDMaster);
      destructor Free;
      procedure ClearData;
      procedure AddLayerItem (iLayerIndex: Integer; iLayerItemID: Integer; ADataAccessObject: TIDB_Access2DB_Level2);

      function GetLayersCount: Integer;
      function GetAllItemsCount: Integer;
      function GetLayerNameByIndex (iLayerIndex: Integer): AnsiString;
// ++ Cadmensky
      procedure OpenDataForLayer (iLayerIndex: integer);
// -- Cadmensky
      function GetDataFromDatabase (iLayerIndex, iLayerItemID: Integer; var SG: TIDB_StringGrid): Boolean;
      procedure SetDataToDatabase (iLayerIndex, iLayerItemID: Integer; SG: TIDB_StringGrid);

      function GetFieldType (iLayerIndex: Integer; sFieldName: AnsiString): Integer;

      procedure UpdateDataInOneField (iLayerIndex, iLayerItemID: Integer; sFieldNameToBeFilledWith, sValueToBeFilledWith: AnsiString); overload;
      procedure UpdateDataInOneField (iLayerIndex: Integer; sFieldNameToBeFilledWith, sValueToBeFilledWith: AnsiString; pbTheUserWantsToCancel: TPointerToBoolean; AGauge: TGauge = nil); overload;

      procedure MakeTreeViewRepresentation (var TV: TTreeListView; pbTheUserWantsToCancel: TPointerToBoolean; AGauge: TGauge = nil; ALabel: TLabel = nil);
      procedure MakeGridRepresentation (var SG: TStringGrid; pbTheUserWantsToCancel: TPointerToBoolean; AGauge: TGauge = nil);
   end;

implementation

uses
   IDB_Consts, IDB_Utils, IDB_LanguageManager, IDB_TreeView, IDB_Monitor,
   IDB_FieldsVisibilityMan, IDB_MainManager, IDB_CallbacksDef;

constructor TIDB_MonData.Create (AProject: Pointer; ALayerMan: TIDB_LayerManager; AMaster: TDMaster);
begin
//   inherited Create;
   SELF.AProject := AProject;
   SELF.ALayerMan := ALayerMan;
   SELF.Data := TList.Create;
   AQuery := TDQuery.Create (nil);
   AQuery.Master := AMaster;
end;

destructor TIDB_MonData.Free;
begin
   SELF.ClearData;
   Data.Free;
   AQuery.Free;
//   inherited Free;
end;

procedure TIDB_MonData.ClearData;
var
   iI: Integer;
   pLI: ^TrecLayersItems;
begin
   for iI := 0 to SELF.Data.Count - 1 do
   begin
      pLI := Data[iI];
      pLI.listObjectIDs.Clear;
      pLI.listObjectIDs.Free;
      pLI.listDispInfo.Clear;
      pLI.listDispInfo.Free;
      pLI.AStructManager.Free;
      Dispose (pLI);
   end; // for iI end
   Data.Clear;
end;

{Find layer in array by its index.}

function TIDB_MonData.FindLayerInArray (iLayerIndex: Integer): Integer;
var
   iI: Integer;
   pLI: ^TrecLayersItems;
begin
   RESULT := -1;
   for iI := 0 to SELF.Data.Count - 1 do
   begin
      pLI := Data[iI];
      if pLI.iLayerIndex = iLayerIndex then
      begin
         RESULT := iI;
         EXIT;
      end;
   end;
end;

{Find layer's item in array by its ID and layer's index.}

{function TIDB_MonData.FindLayerItemInArray (iLayerPosition: Integer; iLayerItemID: Integer): Integer;
var
   iI: Integer;
   pLI: ^TrecLayersItems;
begin
   RESULT := -1;
   pLI := SELF.Data[iLayerPosition];
   for iI := 0 to pLI.listObjectIDs.Count - 1 do
      if Integer (pLI.listObjectIDs[iI]) = iLayerItemID then
      begin
         RESULT := iI;
         EXIT;
      end;
end;}

{ Add new layer's item in objects for monitoring. }

procedure TIDB_MonData.AddLayerItem (iLayerIndex: Integer; iLayerItemID: Integer; ADataAccessObject: TIDB_Access2DB_Level2);
var
   iP: Integer;
   pLI: ^TrecLayersItems;
   sLayerIndex: AnsiString;
//   ADescMan: TIDB_DescMan;
   ATable: TDTable;
   ALayer: Pointer;
begin
   sLayerIndex := IntToStr (iLayerIndex);
   // Try to find layer in array by its index.
   iP := SELF.FindLayerInArray (iLayerIndex);
   if iP < 0 then
   begin // if layer wasn't found, add it.
      New (pLI);
      pLI.ADBAccessObject := ADataAccessObject;
      pLI.iLayerIndex := iLayerIndex;
      pLI.listObjectIDs := TList.Create;
      pLI.listDispInfo := TStringList.Create;
      //
      ATable := TDTable.Create (nil);
      ATable.Master := pLI.ADBAccessObject.DMaster;

      if not AnIDBMainMan.UseNewATTNames (AProject) then
         ATable.TableName := cs_AttributeTableNamePrefix + IntToStr (iLayerIndex)
      else
      begin
         ALayer := GetLayerPointerByIndexFunc (AProject, iLayerIndex);
         ATable.TableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);
      end;

      pLI.AStructManager := TIDB_StructMan.Create (AProject, pLI.ADBAccessObject);
      pLI.AStructManager.DetectStructure (ATable);
      ATable.Close;
      ATable.Free;
      //
      SELF.Data.Add (pLI);
   end
   else
      pLI := SELF.Data[iP];
   pLI.listObjectIDs.Add (Pointer (iLayerItemID));
end;

{Get count different layers that is stored into array}

function TIDB_MonData.GetLayersCount: Integer;
begin
   RESULT := SELF.Data.Count;
end;

{Return layer's name if we know Wingis index of layer's}

function TIDB_MonData.GetLayerNameByIndex (iLayerIndex: Integer): AnsiString;
begin
   RESULT := GetLayerNameFunc (GetLayerPointerByIndexFunc (SELF.AProject, iLayerIndex));
end;

{function TIDB_MonData.OpenData (iLayerIndex, iLayerItemID: Integer; sFieldsNames: AnsiString): integer;
//var pLD: PLayerDef;
//   iP, iI: Integer;
var
   ALayer: Pointer;
begin
   RESULT := 0;

   if SELF.AQuery.Active then
      AQuery.Close;
   // to get data about concrete object if only this object didn't marked as deleted one.
   AQuery.SQL.Clear;

   if not AnIDBMainMan.UseNewATTNames (AProject) then
      AQuery.SQL.Add ('SELECT ' + sFieldsNames + ' FROM [' + cs_AttributeTableNamePrefix + IntToStr (iLayerIndex) + ']')
   else
   begin
      ALayer := GetLayerPointerByIndexFunc (AProject, iLayerIndex);
      AQuery.SQL.Add ('SELECT ' + sFieldsNames + ' FROM [' + cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer) + ']');
   end;

   AQuery.SQL.Add ('WHERE (PROGISID = ' + IntToStr (iLayerItemID) + ')');
   AQuery.SQL.Add ('  AND ([' + cs_StateFieldName + '] <> ' + cs_ObjectIsDeleted + ')');

   try
      AQuery.Open;
      RESULT := AQuery.RecordCount;
   except
   end;
end;}

// ++ Cadmensky

procedure TIDB_MonData.OpenDataForLayer (iLayerIndex: integer);
var
   ALayer: Pointer;
   //AnExtTablesMan: TIDB_ExternalTablesMan;
   sAttTableName: AnsiString;
begin
   SELF.bIsDataOpen := FALSE;

   ALayer := GetLayerPointerByIndexFunc (SELF.AProject, iLayerIndex);
   if ALayer <> nil then
   begin
      if SELF.AQuery.Active then
         SELF.AQuery.Close;

      //AnExtTablesMan := SELF.ALayerMan.GetLayerExtTablesMan (ALayer);

      {if (AnExtTablesMan <> nil) and AnExtTablesMan.IAmInUse then
      begin
//         if AnExtTablesMan.MakeExtTablePool then
         AnExtTablesMan.SetParamsToExtTablePool;
//         else
//            exit;
         SELF.AQuery.SQL.Text := AnExtTablesMan.JoiningSQL;
      end
      else }
      begin
         if not AnIDBMainMan.UseNewATTNames (AProject) then
            sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (ALayer))
         else
            sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (SELF.AProject, ALayer);

         SELF.AQuery.SQL.Text := 'SELECT * FROM [' + sAttTableName + '] WHERE ' + cs_ShowAllWithoutDeletedObjectsFilter;
      end;

      try
         SELF.AQuery.Open;
      except
         exit;
      end;

      SELF.bIsDataOpen := true;
   end;
end;
// -- Cadmensky

{Get data for concrete layer's object from database and put it into TStringGrid}

function TIDB_MonData.GetDataFromDatabase (iLayerIndex, iLayerItemID: Integer; var SG: TIDB_StringGrid): Boolean;
var
   iI: Integer;
   ALayer: Pointer;
   slFields: TStringList;
   AFieldsVisibilityMan: TIDB_FieldsVisibilityManager;
//   AnExtTablesMan: TIDB_ExternalTablesMan;
   sFieldName, sData: AnsiString;
   AField: TField;
//   bDataExists: integer;
//   CB1: TComboBox;
//   sComboBoxData: AnsiString;
//   iCBTop: integer;
begin
   RESULT := FALSE;

   ALayer := GetLayerPointerByIndexFunc (SELF.AProject, iLayerIndex);
   if ALayer <> nil then
   begin
      if not SELF.AQuery.Locate ('PROGISID', iLayerItemID, []) then
         exit;
      slFields := TStringList.Create;
      AFieldsVisibilityMan := ALayerMan.GetLayerFieldsVisibilityManager (ALayer);
      slFields.Text := AFieldsVisibilityMan.GetVisibleAttributeFieldNames (TRUE);
      SG.RowCount := slFields.Count + 1;
      SG.FixedRows := 1;
      SG.RowHeights[0] := 10;
      for iI := 0 to slFields.Count - 1 do
      begin
         sFieldName := slFields[iI];
         sData := '';
         if AnsiUpperCase (sFieldName) = 'PROGISID' then
            sData := IntToStr (iLayerItemID)
         else
         begin
            AField := SELF.AQuery.FindField (sFieldName);
            if AField <> nil then
               sData := AField.AsString
// ++ Cadmensky Version 2.3.9
            else
               if FieldIsCalculatedField (sFieldName) then
                  sData := GetCalculatedFieldValueByName (SELF.AProject, ALayer, iLayerItemID, sFieldName);
// -- Cadmensky Version 2.3.9
         end;

         SG.Cells[0, iI + 1] := sFieldName;
         SG.Cells[1, iI + 1] := sData;

// ++ Commented by Cadmensky Version 2.3.9
{         AnExtTablesMan := Self.ALayerMan.GetLayerExtTablesMan (ALayer);
         if (AnExtTablesMan <> nil) and AnExtTablesMan.IAmInUse then
         begin
            if SG.Objects[1, iI + 1] <> nil then
               SG.Objects[1, iI + 1].Free;
            sComboBoxData := AnExtTablesMan.SelectDistinctFieldsForProgisID (sFieldName, iLayerItemID, SG);
            if sComboBoxData <> '' then
            begin
               CB1 := TComboBox.Create (SG);
               CB1.Parent := SG.Parent;
               CB1.Style := csDropDown;
               CB1.Items.Text := sComboBoxData;
               CB1.Text := IntToStr (CB1.Items.Count) + ' record''s';

               iCBTop := SG.Top;
               for iJ := 0 to iI do
               begin
                  Inc (iCBTop, SG.RowHeights[iJ] + 1);
               end;
               CB1.Top := iCBTop;
               CB1.Height := SG.RowHeights[iI];
               CB1.Left := SG.Left + SG.ColWidths[0] + 2;
               CB1.Width := SG.ColWidths[1] + 3;
               SG.Objects[1, iI + 1] := CB1;
            end;
         end;}
// ++ Commented by Cadmensky Version 2.3.9
      end; // for iI end
// ++ Cadmensky
      slFields.Clear;
      slFields.Free;
// -- Cadmensky
      SG.Visible := TRUE;
      RESULT := TRUE;
   end; // if ALayer <> nil end
end;

procedure TIDB_MonData.SetDataToDatabase (iLayerIndex, iLayerItemID: Integer; SG: TIDB_StringGrid);
var
   iI: Integer;
//   ATable: TDTable;
   AField: TField;
//   sState: AnsiString;
//   iLinkNumber: integer;
//   sFieldName: string;
//   ALayer: Pointer;
//   AnExtTablesMan: TIDB_ExternalTablesMan;
   ALayer: Pointer;
   sFieldName, sFieldData: AnsiString;
begin
// ++ Cadmensky Version 2.3.9
   ALayer := GetLayerPointerByIndexFunc (SELF.AProject, iLayerIndex);
   if ALayer = nil then
      Exit;

   //AnExtTablesMan := SELF.ALayerMan.GetLayerExtTablesMan (ALayer);

   {if AnExtTablesMan.IAmInUse then
   begin
      AnExtTablesMan.GetReadyToReceiveDataForUpdate;

      for iI := 2 to SG.RowCount - 1 do
      begin
         sFieldName := SG.Cells[0, iI];
         sFieldData := SG.Cells[1, iI];
         if not FieldIsCalculatedField (sFieldName) and
            not (AnsiUpperCase (sFieldName) = 'PROGISID') then
         begin
            AField := SELF.AQuery.FindField (sFieldName);
            if AField <> nil then
               AnExtTablesMan.ReceiveDataForUpdate (sFieldName, sFieldData, AField.DataType);
         end;
      end;

      AnExtTablesMan.ExecuteUpdate (iLayerItemID);
      SELF.AQuery.Reopen;
   end
   else}
      try
         AQuery.Edit;
// -- Cadmensky Version 2.3.9
         for iI := 2 to SG.RowCount - 1 do
         begin
            sFieldName := SG.Cells[0, iI];
            sFieldData := SG.Cells[1, iI];
            if not FieldIsCalculatedField (sFieldName) and
               not (AnsiUpperCase (sFieldName) = 'PROGISID') and
               not (AnsiUpperCase (sFieldName) = 'GUID') then
            begin
               AField := AQuery.FindField (sFieldName);
               if AField <> nil then
                  AField.AsString := SG.Cells[1, iI];
            end;
         end;
// ++ Cadmensky Version 2.3.9
         AQuery.Post;
      except
         AQuery.Cancel;
      end;
// -- Cadmensky Version 2.3.9

end;

procedure TIDB_MonData.UpdateDataInOneField (iLayerIndex: Integer; sFieldNameToBeFilledWith, sValueToBeFilledWith: AnsiString; pbTheUserWantsToCancel: TPointerToBoolean; AGauge: TGauge = nil);
var
   pLI: ^TrecLayersItems;
   iP, iJ: Integer;
   sIDs: AnsiString;
   ALayer: Pointer;
begin
   if AnsiUpperCase (sFieldNameToBeFilledWith) = 'PROGISID' then EXIT;
   if AnsiUpperCase (sFieldNameToBeFilledWith) = 'GUID' then EXIT;
   // Try to find layer in array by its index.
   iP := SELF.FindLayerInArray (iLayerIndex);
   if iP < 0 then EXIT;
   pLI := SELF.Data[iP];

   AGauge.Progress := 0;
   AGauge.MaxValue := pLI.listObjectIDs.Count;
   Application.ProcessMessages;

   iP := 0;
   while iP <= pLI.listObjectIDs.Count - 1 do
   begin
      sIDs := '(';
      for iJ := 0 to ci_PackLength do
      begin
         sIDs := sIDs + 'PROGISID = ' + IntToStr (Integer (pLI.listObjectIDs[iP + iJ])) + ' or ';
         if iP + iJ = pLI.listObjectIDs.Count - 1 then BREAK;
      end; // for iJ end
      Inc (iP, ci_PackLength + 1);
      sIDs := Copy (sIDs, 1, Length (sIDs) - 4) + ')';
      try
         if not AnIDBMainMan.UseNewATTNames (AProject) then
            AQuery.ExecSQL ('UPDATE [' + cs_AttributeTableNamePrefix + IntToStr (iLayerIndex) + '] ' +
               sValueToBeFilledWith + ' WHERE ' + sIDs)
         else
         begin
            ALayer := GetLayerPointerByIndexFunc (AProject, iLayerIndex);
            AQuery.ExecSQL ('UPDATE [' + cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer) + '] ' +
               sValueToBeFilledWith + ' WHERE ' + sIDs);
         end;
      except
      end;
      AGauge.Progress := iP;
      Application.ProcessMessages;
      if pbTheUserWantsToCancel^ then
         break;
   end; // while end
end;

procedure TIDB_MonData.UpdateDataInOneField (iLayerIndex, iLayerItemID: Integer; sFieldNameToBeFilledWith, sValueToBeFilledWith: AnsiString);
var
   ALayer: Pointer;
begin
   if AnsiUpperCase (sFieldNameToBeFilledWith) = 'PROGISID' then EXIT;
   if AnsiUpperCase (sFieldNameToBeFilledWith) = 'GUID' then EXIT;
   try
      if not AnIDBMainMan.UseNewATTNames (AProject) then
         AQuery.ExecSQL ('UPDATE [' + cs_AttributeTableNamePrefix + IntToStr (iLayerIndex) + '] ' +
            'SET [' + sFieldNameToBeFilledWith + '] = ' + sValueToBeFilledWith + ' ' +
            'WHERE PROGISID = ' + IntToStr (iLayerItemID))
      else
      begin
         ALayer := GetLayerPointerByIndexFunc (AProject, iLayerIndex);
         AQuery.ExecSQL ('UPDATE [' + cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer) + '] ' +
            'SET [' + sFieldNameToBeFilledWith + '] = ' + sValueToBeFilledWith + ' ' +
            'WHERE PROGISID = ' + IntToStr (iLayerItemID));
      end;
   except
   end;
end;

{ Transform data from array into TTreeView. }

procedure TIDB_MonData.MakeTreeViewRepresentation (var TV: TTreeListView; pbTheUserWantsToCancel: TPointerToBoolean; AGauge: TGauge = nil; ALabel: TLabel = nil);
var
   pLI: ^TrecLayersItems;
   bDisplayInfoIsProgisIDField: Boolean;
   sStatusText: AnsiString;

   procedure FillDispInfo;
   var
      iI {, iJ}: Integer;
//      sIDs: AnsiString;
   begin
      pLI.listDispInfo.Clear;
      for iI := 0 to pLI.listObjectIDs.Count - 1 do
         pLI.listDispInfo.Add (IntToStr (Integer (pLI.listObjectIDs[iI])));
   end;

var
   iI, iJ: Integer;
   pItemInfo: ^TrecItemInfo;
   ndCorner, ndChild: TIDB_TreeEntry;
begin
   TV.BeginUpdate;
   for iI := 0 to TV.Items.Count - 1 do
   begin
      pItemInfo := TIDB_TreeEntry (TV.Tree.ExpandedItems[iI]).Data;
      if pItemInfo <> nil then
         Dispose (pItemInfo);
   end; // for iI end
   TV.Tree.Clear;
   if Data.Count = 0 then
   begin
      TV.EndUpdate;
      EXIT;
   end;

   if ALabel <> nil then
      // Create view: %d from %d
      sStatusText := ALangManager.GetValue (172);

   if Data.Count = 1 then
   begin
      // if there is data only for objects from one layer, these data displayed as root of trees.
      pLI := Data[0];

      bDisplayInfoIsProgisIDField := TRUE; // AnsiUpperCase(pLI.sMonitoredFieldForThisLayer) = 'PROGISID';
      if AGauge <> nil then
         if bDisplayInfoIsProgisIDField then
            AGauge.MaxValue := pLI.listObjectIDs.Count
         else
            AGauge.MaxValue := 2 * pLI.listObjectIDs.Count;

      FillDispInfo;

      New (pItemInfo);
      pItemInfo.iLayerIndexToThatTheItemBelongs := pLI.iLayerIndex;

      for iI := 0 to pLI.listObjectIDs.Count - 1 do
      begin
         if (pbTheUserWantsToCancel^) and (iI > 0) then
         begin
            pLI := nil; // if there is not this line, the compiler does not compile next line. I don't know why.
            BREAK;
         end;
         ndCorner := TIDB_TreeEntry.Create;
         ndCorner.Expanded := FALSE;
         (*
                     // We haven't bitmap for items from the same layer.
         //            ndCorner.Bitmap := 2;
         *)
         ndCorner.Bitmap := GetTypeBitmapNumberByProgisID (Integer (pLI.listObjectIDs[iI]));
         ndCorner.Caption := GetTypeNameByProgisID (Integer (pLI.listObjectIDs[iI]));
         TV.Tree.Add (ndCorner);
         ndCorner.Data := pLI.listObjectIDs[iI];

         if AGauge <> nil then AGauge.Progress := AGauge.Progress + 1;
         if ALabel <> nil then ALabel.Caption := Format (sStatusText, [AGauge.Progress, AGauge.MaxValue]);
         Application.ProcessMessages;
      end; // for iI end
      pItemInfo.iItemID := Integer (TIDB_TreeEntry (TV.Tree.ExpandedItems[0]).Data);
      TIDB_TreeEntry (TV.Tree.ExpandedItems[0]).Data := pItemInfo;
   end
   else
   begin
      // if there are data for objects from some layers, names of these layers displayed as roots of tree.
      // Items of these layers are displayed as leafs of tree.
      if AGauge <> nil then
      begin
         iJ := 0;
         for iI := 0 to Data.Count - 1 do
         begin
            pLI := Data[iI];
            iJ := iJ + pLI.listObjectIDs.Count;
         end; // for iJ end
         if bDisplayInfoIsProgisIDField then
            AGauge.MaxValue := iJ
         else
            AGauge.MaxValue := 2 * iJ;
      end; // if end

      for iI := 0 to Data.Count - 1 do
      begin
         pLI := Data[iI];
         bDisplayInfoIsProgisIDField := TRUE; // AnsiUpperCase(pLI.sMonitoredFieldForThisLayer) = 'PROGISID';
         FillDispInfo;

         ndCorner := TIDB_TreeEntry.Create;
         ndCorner.Caption := GetLayerNameByIndex (pLI.iLayerIndex);
         ndCorner.Expanded := FALSE;
         ndCorner.Bitmap := 0;
         TV.Tree.Add (ndCorner);

         New (pItemInfo);
         pItemInfo.iLayerIndexToThatTheItemBelongs := pLI.iLayerIndex;
         pItemInfo.iItemID := -1;
         ndCorner.Data := pItemInfo;

         // Set bitmap for root items of TTreeView (for representation of layers).
         ndCorner.Bitmap := 0;
         for iJ := 0 to pLI.listObjectIDs.Count - 1 do
         begin
            ndChild := TIDB_TreeEntry.Create;
            ndChild.Expanded := FALSE;
            (*
                            // We haven't bitmap for items from the same layer.
            //                ndChild.Bitmap := 2;
            *)
            ndChild.Bitmap := GetTypeBitmapNumberByProgisID (Integer (pLI.listObjectIDs[iJ]));
            ndChild.Caption := GetTypeNameByProgisID (Integer (pLI.listObjectIDs[iJ]));
            ndCorner.Add (ndChild);

            ndChild.Data := pLI.listObjectIDs[iJ];

            if AGauge <> nil then AGauge.Progress := AGauge.Progress + 1;
            if ALabel <> nil then ALabel.Caption := Format (sStatusText, [AGauge.Progress, AGauge.MaxValue]);
            Application.ProcessMessages;

            if pbTheUserWantsToCancel^ then
            begin
               pLI := nil; // if there is not this line, the compiler does not compile next line. I don't know why.
               BREAK;
            end;
         end;
         if pbTheUserWantsToCancel^ then
         begin
            pLI := nil; // if there is not this line, the compiler does not compile next line. I don't know why.
            BREAK;
         end;
      end; // for iI end
   end;
   TV.EndUpdate;
end;

{ Transform data from array to grid. }

procedure TIDB_MonData.MakeGridRepresentation (var SG: TStringGrid; pbTheUserWantsToCancel: TPointerToBoolean; AGauge: TGauge = nil);

// This function is detects whether column with concrete name exists in grid or not.

   function ColumnIsPresent (sColumnName: AnsiString; var iP: Integer): Boolean;
   var
      iI: Integer;
   begin
      RESULT := FALSE;
      for iI := 0 to SG.ColCount - 1 do
         if SG.Cells[iI, 0] = sColumnName then
         begin
            iP := iI;
            RESULT := TRUE;
            EXIT;
         end;
   end;
var
   iI, iJ, iK, iP, iP1: Integer;
   pLI: ^TrecLayersItems;
   sIDs: AnsiString;
   ALayer: Pointer;
begin
   if Data.Count < 1 then
   begin
      SG.RowCount := 0;
      SG.ColCount := 0;
      EXIT;
   end;
   if AGauge <> nil then
   begin
      AGauge.Progress := 0;
      iK := 0;
      for iI := 0 to Data.Count - 1 do
      begin
         pLI := Data[ii];
         Inc (iK, pLI.listObjectIDs.Count);
      end; // for iI end
      AGauge.MaxValue := iK;
   end;
   SG.Visible := FALSE;
   // First column - 'Layer'.
   SG.ColCount := 1;
   SG.RowCount := 1;
   SG.Cells[0, 0] := 'Layer';
   for iI := 0 to Data.Count - 1 do
   begin
      pLI := Data[iI];
      iP1 := 0;
      while iP1 <= pLI.listObjectIDs.Count - 1 do
      begin
         sIDs := '(';
         for iJ := 0 to ci_PackLength do
         begin
            sIDs := sIDs + 'PROGISID = ' + IntToStr (Integer (pLI.listObjectIDs[iP1 + iJ])) + ' or ';
            if iP1 + iJ = pLI.listObjectIDs.Count - 1 then BREAK;
         end; // for iJ end
         Inc (iP1, ci_PackLength + 1);
         sIDs := Copy (sIDs, 1, Length (sIDs) - 4) + ')';
         try
            if AQuery.Active then
               AQuery.Close;

            if not AnIDBMainMan.UseNewATTNames (AProject) then
               AQuery.SQL.Text := 'SELECT * FROM [' + cs_AttributeTableNamePrefix + IntToStr (pLI.iLayerIndex) + '] ' +
                  'WHERE ([' + cs_StateFieldName + '] <> ' + cs_ObjectIsDeleted + ') and (' + sIDs + ')'
            else
            begin
               ALayer := GetLayerPointerByIndexFunc (AProject, pLI.iLayerIndex);
               AQuery.SQL.Text := 'SELECT * FROM [' + cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer) + '] ' +
                  'WHERE ([' + cs_StateFieldName + '] <> ' + cs_ObjectIsDeleted + ') and (' + sIDs + ')';
            end;

            AQuery.Open;
         except
            CONTINUE;
         end;
         for iJ := 0 to AQuery.RecordCount - 1 do
         begin
            SG.RowCount := SG.RowCount + 1;
            for iK := 0 to AQuery.Fields.Count - 1 do
            begin
               // Here I try to avoid special field of the Internal Database.
               // Data that contained in this field mustn't be dispalyed for an user.
               if FieldIsInternalUsageField (AQuery.Fields[iK].FieldName) then CONTINUE;
               SG.Cells[0, SG.RowCount - 1] := GetLayerNameByIndex (pLI.iLayerIndex); // pLI.sLayerName;
               if not ColumnIsPresent (AQuery.Fields[iK].FieldName, iP) then
               begin
                  // if filed not exist in Grid, we must add it into Grid.
                  iP := SG.ColCount;
                  SG.ColCount := iP + 1;
                  SG.Cells[iP, 0] := AQuery.Fields[iK].FieldName;
               end;
               SG.Cells[iP, SG.RowCount - 1] := AQuery.Fields[iK].AsString;
            end; // for iK end
            AQuery.Next;

            if AGauge <> nil then
            begin
               AGauge.Progress := AGauge.Progress + 1;
               Application.ProcessMessages;
            end;

            if pbTheUserWantsToCancel^ then BREAK;

         end; // for iJ end
         AQuery.Close;
         if pbTheUserWantsToCancel^ then BREAK;
      end; // while end
      if pbTheUserWantsToCancel^ then BREAK;
   end; // for iI end
   if SG.RowCount > 1 then SG.FixedRows := 1;
   SG.Visible := TRUE;
end;

function TIDB_MonData.GetAllItemsCount: Integer;
var
   pLI: ^TrecLayersItems;
   iI: Integer;
begin
   RESULT := 0;
   for iI := 0 to SELF.Data.Count - 1 do
   begin
      pLI := Data[iI];
      RESULT := RESULT + pLI.listObjectIDs.Count;
   end;
end;

function TIDB_MonData.GetFieldType (iLayerIndex: Integer; sFieldName: AnsiString): Integer;
var
   pLI: ^TrecLayersItems;
   iP: Integer;
begin
   RESULT := -1;
   iP := SELF.FindLayerInArray (iLayerIndex);
   if iP < 0 then EXIT;
   pLI := Data[iP];
   RESULT := pLI.AStructManager.GetFieldType (sFieldName);
end;

end.

