unit IDB_Consts;
{
Internal database. Ver. II.
Various constants that I use in other parts of Internal Database.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 22-01-2001
}
interface

Type
    TDBVersion = (dbv97, dbv2000);

var cs_ApplicationName: AnsiString;
    showtablepropertiesdlg: Boolean;


Const
     cs_ResName97 = 'EMPTYDB97';     // Empty *.mdb file of MS Access 97 format in resource file.
     cs_ResName2000 = 'EMPTYDB2000'; // Empty *.mdb file of MS Access 2000 format in resource file.
     cs_ResNameOfEnglishLngFile = 'IDB_ENGLISH_LANGUAGE_FILE'; // Default language file of the IDB (English version).

     cs_IDB_NormalVersion = '2.3.9'; // Current version of the IDB. It is used when WinGIS is loading IDB.dll.
                                      // WinGIS compares this value with the value it needs.

     cs_IDB_DatabaseFileExt = 'wgi';
     cs_IDB_ExtTablesPoolFileExt = 'etp';
     cs_IDB_TemporarilyStoragedDataBaseFileExt = 'tsw';
     cs_TempDatabaseFileName = 'WG_IDB_TEMP.wgi';  //<< This name is used when Internal database makes either changing of database file
                                                   //   format or packing of database.
//     cs_IT_ST_BlobInfo = 'USYS_WGIDB_SPTB';        //<< Table that contains special information of BLOB type.
                                                   //   The record with SPID = 0 contains range schema information (for thematic maps).
//     cs_Old_IT_ST_BlobInfo = 'WGIDB_SPTB';
// ++ Cadmensky
     cs_IT_LayerNamesTable = 'USYS_WGIDB_LTN';     //<< Table that contains layer's names/indexes and attribute tables names
// -- Cadmensky

     cs_IT_ST_TextInfo = 'USYS_WGIDB_SPTT';        //<< Table that contains special information of text type.
                                                   //   The record with SPID = 0 contains start GUID of the database file.
                                                   //   The record with SPID = 1 contains current GUID of the database file.
     cs_Old_IT_ST_TextInfo = 'WGIDB_SPTT';

     cs_TempFieldName = '_WG_TEMP';                //<< This field is used during a table restructure.
     cs_StateFieldName = '_WG_STATE';              //<< This field contains state of object in an attribute
                                                   //   table (object was marked as deleted, object was just added etc).
     cs_SPFieldName = '_WG_SPF';                   //<< This field are placed in the table of special information.

     cs_InternalID_FieldName = '_WG_INTERNAL_ID';  //<< Internal ID of each record in an attribute table. It was added to avoid many
                                                   //   problems tied to ADO.
     cs_IT_CheckVersionTable = '_WG_CVT';          //<< This table is used to check the database file format. It is created every time
                                                   //   a project is being loaded and is deleted just after the checking is completed.
     cs_IT_TempLink = 'USYS_WGIDB_TEMP_LINK';      // Temporary link to table
     cs_IT_TempTable = 'USYS_WGIDB_TEMP';          // Temporary table
{ States of objects in an attribute table. }
     cs_ObjectIsDeleted = '0';
     cs_ObjectIsNormal = '1';
     cs_ObjectIsJustAdded = '2';
{ Filters. }
     // This filter is used to show all item attribute recodrds exlude deleted objects.
     cs_ShowAllWithoutDeletedObjectsFilter = '[' + cs_StateFieldName + '] = ' + cs_ObjectIsNormal + ' OR [' + cs_StateFieldName + '] = ' + cs_ObjectIsJustAdded;
{ These are names of database's tables: an attributive table, a table that contains data about fields
  of the attributive table which have the special means and a pattern table that contains values for
  the attributive table. }
     cs_AttributeTableNamePrefix = 'ATT';   // ATT + LayerIndex
     cs_DescriptionTableNamePrefix = 'DES'; // DES + LayerIndex
     cs_PatternTableNamePrefix = 'PAT';     // PAT + LayerIndex
     cs_TempTableNamePrefix = 'TMP';        // TMP + LayerIndex

     cs_CommonDescTableName = 'USYS_WGIDB_DT';
     cs_OldCommonDescTableName = '_WGIDB_DT';

     cs_CDT_LayerIndexFieldName = 'LI';
{ These are errors of BDE. I use TEDBEngineError and Errors.NativeError to get an access to error's number
  for recognizig the error.
  All errors were named by me, when I found them in different situations. }
     ci_All_Ok = -1;
     ci_TableNotExists = -2;
     ci_GeneralSQLError = 0;
     ci_ThereIsNoFieldInTable = 3066;
{ Before I'll do an Undo operations I have to put deleting data into the Undo table and Redo one in
  a database. As I can do now not only for items of project but for a layer, I have to put
  in the Undo table also sign what I had put in this table - a layer or a project item.
  These constants are determine that. }
     ci_LayerItem = 1;    // TIndex
     ci_SymbolsGroup = 2; // TSGroup
     ci_ProjectItem = 3;  // TView
     ci_Layer = 4;        // TLayer
     ci_NewObject = 5;    // An object was added and now the user wants to delete it as Undo.
     ci_DeletedObject = 6;// An object was restored during Undo and needs to be deleted during Redo.
{ These parameters determines key and parameter name in the WinGIS project registry. }
     cs_ProjectRegistry_IDB_KeyName = '\Project\IDB\GUID';
     // Here I storage the start value of GUID. This value is created during creating a new project
     // and is continually all life time of the project.
     cs_ProjectRegistry_IDB_StartGUID_ParameterName = 'StartGUIDValue';
     // Here I storage the current value of GUID. This value changes during each saving of the project.
     cs_ProjectRegistry_IDB_GUID_ParameterName = 'GUIDValue';
{ These constants I use in SetObjectStyle procedure in TIDB object. }
     ci_DoNotChange = -1;
     ci_UseLayerSettings = -2;
{ I can make operations on records in attribute tables in database files and count some of ProgisID for such
  operations (I ment SQL clause Where ProgisID = first OR ProgisID = second ... etc.) I found out that I can
  count only less than 90 ProgisID in such situations.
  Value is required value minus 1 because of a count starts from zero. }
     ci_PackLength = 89;
{ The size of grid's field less which the field is considered as invisible one. }
     ci_InvisibleFieldBorder = 2;
{ The following symbols are used in the language file for tuning of the user interface. }     
     cs_MenuListStartSign = '[MENUBAR]';
     cs_MenuListEndSign = '';
     cs_PopupMenuListStartSign = '[POPUPMENU]';
     cs_PopupMenuListEndSign = '';

     cs_ToolbarStartSign = '[TOOLBAR]';
     cs_ToolbarEndSign = '';

     cs_SeparatorSign = '<SEPARATOR>';
     cs_StartChildMenuSign = '(';
     cs_EndChildMenuSign = ')';

     cs_VerisonOfDBFormStorage = '#WG#0002#WG#';
     cs_VerisonOfMonitoringFormStorage = '#WG#0001#WG#';

     cs_LinkToLayerTable = '_WG_LinkToMainProject';
     cs_VerisonOfLinkStorage = '#WG#0001#WG#';

     cs_CalcFieldAreaName = '_Area';
//     cs_CalcFieldLengthName = '_Length';
//     cs_CalcFieldPerimeterName = '_Perimeter';
     cs_CalcFieldXName = '_X';
     cs_CalcFieldYName = '_Y';
     cs_CalcFieldX1Name = '_X1';
     cs_CalcFieldY1Name = '_Y1';
     cs_CalcFieldVerticesCount = '_Vertices';
     cs_CalcFieldSymbolName = '_Symbol Name';
     cs_CalcFieldSizeName = '_Size';
     cs_CalcFieldRotationName = '_Rotation';
     cs_CalcFieldTextName = '_Text';
     cs_CalcFieldLinkIDName = '_LinkID';

     cs_CalcFieldAreaNameForXBase = 'Area_';
//     cs_CalcFieldLengthNameForXBase = 'Length_';
//     cs_CalcFieldPerimeterNameForXBase = 'Perimeter_';
     cs_CalcFieldXNameForXBase = 'X_';
     cs_CalcFieldYNameForXBase = 'Y_';

Type
{ These code page settings are used in the TDataManager during loading or saving data operations. }
     TCodePage = (cpANSI, cpOEM);

implementation

end.
