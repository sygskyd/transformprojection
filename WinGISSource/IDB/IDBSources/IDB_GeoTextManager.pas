unit IDB_GeoTextManager;
{
Internal database. Ver. II.
This module provides the functionality of the GeoText of WinGIS.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 30-01-2001
}
interface

Uses DTables,
     IDB_Access2DB;

Type
    TIDB_GeoTextManager = Class
       Private
          IDB_DA: TIDB_Access2DB_Level1;
          DQuery: TDQuery;
       Public
          Constructor Create;
          Destructor Free;

          Procedure ChangeProjectFile(sProjectFileName, sUserName, sPassword: AnsiString);
          Function GetGeoText(iLayerIndex, iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): AnsiString;
          Procedure StopConnection(sProjectFileName: AnsiString);
       End;

Var
   AGeoTextManager: TIDB_GeoTextManager;

implementation

Uses SysUtils,
     DUtils,
     IDB_Consts, IDB_Utils;

Constructor TIDB_GeoTextManager.Create;
Begin
     SELF.IDB_DA := TIDB_Access2DB_Level1.Create;
     SELF.DQuery := TDQuery.Create(NIL);
     SELF.DQuery.Master := SELF.IDB_DA.DMaster;
End;

Destructor TIDB_GeoTextManager.Free;
Begin
     If SELF.DQuery.Active Then SELF.DQuery.Close;
     SELF.DQuery.Free;
     SELF.IDB_DA.Free;
End;

Procedure TIDB_GeoTextManager.ChangeProjectFile(sProjectFileName, sUserName, sPassword: AnsiString);
Begin
     SELF.IDB_DA.Connect(MakeDBFileNameFromProjectFileName(sProjectFileName), sUserName, sPassword);
End;

Function TIDB_GeoTextManager.GetGeoText(iLayerIndex, iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): AnsiString;
var ALayer: Ponter;
Begin
     RESULT := '';
     If NOT SELF.IDB_DA.DMaster.Connected Then Begin
        TRY
           SELF.IDB_DA.DMaster.Connected := TRUE;
        EXCEPT
        END;
        End;
     If SELF.DQuery.Active Then SELF.DQuery.Close;
     DQuery.SQL.Clear;
     DQuery.SQL.Add('SELECT [' + sGeoTextValueFieldName + ']');
     if not AnIDBMainMan.UseNewATTNames (AProject) then
        DQuery.SQL.Add('FROM [' + cs_AttributeTableNamePrefix + IntToStr(iLayerIndex) + ']')
     else
     begin
        ALayer := GetLayerPointerByIndexFunc (iLayerIndex)
        DQuery.SQL.Add('FROM [' + cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer) + ']');
     end;

     DQuery.SQL.Add('WHERE [' + sGeoTextIDFieldName + '] = ' + IntToStr(iItemID));
     TRY
        DQuery.Open;
     EXCEPT
        EXIT;
     END;
     RESULT := DQuery.Fields[0].AsString;
     DQuery.Close;
End;

Procedure TIDB_GeoTextManager.StopConnection(sProjectFileName: AnsiString);
Var
   sStoppedDBFileName,
   sCurrentConnectionDBFileName: AnsiString;
Begin
     sStoppedDBFileName := AnsiUpperCase(MakeDBFileNameFromProjectFileName(sProjectFileName));
     sCurrentConnectionDBFileName := AnsiUpperCase(GetADOPart(SELF.IDB_DA.DMaster.Connection, 'Data Source'));

     If sCurrentConnectionDBFileName = sStoppedDBFileName Then
        SELF.IDB_DA.Disconnect;
End;

end.
