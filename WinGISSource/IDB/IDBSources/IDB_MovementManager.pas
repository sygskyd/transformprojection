unit IDB_MovementManager;
{
Internal database. Ver. II.
This module implenets an operation that makes a movement all data from source database file
     into new database file and this new file has MS Access 2000 format.
     I use this module to migrate all data from database file of MS Access 97 format
     and to make pack operation of a database.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 16-03-2001
}
interface

uses
    DTables, Db, DMaster, 
    IDB_Access2DB, IDB_StructMan, IDB_Progress;

type
    TIDB_MovementManager = Class
       Private
          AProgressForm: TIDB_ProgressForm;
          AProject: Pointer;
          SourceIDA,
          DestIDA: TIDB_Access2DB_Level2;
          AStructMan: TIDB_StructMan;
          sSourceFileName,
          sDestFileName: AnsiString;
          ATable: TDTable;
          AQuery: TDQuery;
       Public
          Constructor Create(AProject: Pointer; ASourceAccess: TIDB_Access2DB_Level2);
          Destructor Free;
          Procedure MigrateIt;
       End;

implementation

Uses SysUtils, DUtils, Forms, Classes, Windows,
     IDB_Utils, IDB_Consts, IDB_MainManager, IDB_LanguageManager;

Constructor TIDB_MovementManager.Create(AProject: Pointer; ASourceAccess: TIDB_Access2DB_Level2);
Begin
     SELF.AProgressForm := TIDB_ProgressForm.Create(Application.MainForm);
     // Change database file structrure
     AProgressForm.StatusLabel.Caption := ALangManager.GetValue(266);
     AProgressForm.Show;
     Application.ProcessMessages;

     SELF.AProject := AProject;
     SourceIDA := ASourceAccess;
     DestIDA := TIDB_Access2DB_Level2.Create (AProject);

     sSourceFileName := GetADOPart(ASourceAccess.DMaster.Connection, 'Data Source');
     sDestFileName := cs_TempDatabaseFileName;

     // Delete file if such file already exists and create new empty database file.
     If FileExists(sDestFileName) Then SysUtils.DeleteFile(sDestFileName);
     DestIDA.UnLoadEmptyDBFile(sDestFileName);
     DestIDA.Connect(sDestFileName, '', '');

     AStructMan := TIDB_StructMan.Create(AProject, ASourceAccess);

     ATable := TDTable.Create(NIL);
     AQuery := TDQuery.Create(NIL);
     AQuery.Master := ASourceAccess.DMaster;
End;

Destructor TIDB_MovementManager.Free;
Begin
     ATable.Close;
     ATable.Free;
     AQuery.Close;
     AQuery.Free;

     AStructMan.Free;
     DestIDA.Free;
     
     AProgressForm.Hide;
     AProgressForm.Free;
End;

Procedure TIDB_MovementManager.MigrateIt;
Var
   iI, iJ: Integer;
   slTables: TStringList;
   sTableName, sFieldName: AnsiString;
   AField: TField;
   sFieldNames: AnsiString;
   sTempFileName: AnsiString;
Begin
     AWinGISMainForm.Enabled := FALSE;
     TRY
        // Detect names of all tables in the database.
        slTables := TStringList.Create;
        SourceIDA.DMaster.CentralData.GetADOTableNames(slTables);

        AProgressForm.GlobalGauge.Progress := 0;
        AProgressForm.GlobalGauge.MaxValue := slTables.Count;

        For iI:=0 To slTables.Count-1 Do Begin
            sTableName := slTables[iI];
            // Detect structure of the source table (in the old database).
            If ATable.Active Then ATable.Close;
            ATable.Master := SourceIDA.DMaster;
            ATable.TableName := sTableName;
            AStructMan.DetectStructure(ATable);
            // Create the same destination table (in the  new database).
            DestIDA.ADbItemsMan.CreateTableWithoutAnyField(sTableName);
            For iJ:=0 To AStructMan.GetFieldCount-1 Do Begin
                sFieldName := AStructMan.GetFieldName(iJ);
                If sFieldName = cs_InternalID_FieldName Then
                   DestIDA.ADbItemsMan.AddField(sTableName,
                                                sFieldName,
                                                14,         // << AutoIncrement type
                                                -1)
                Else
                   If sFieldName = cs_StateFieldName Then
                      DestIDA.ADbItemsMan.AddField(sTableName,
                                                   sFieldName,
                                                   12,      // << Byte type
                                                   -1)
                   Else
                      DestIDA.ADbItemsMan.AddField(sTableName,
                                                   sFieldName,
                                                   AStructMan.GetFieldType(iJ),
                                                   AStructMan.GetFieldSize(iJ));
                End; // For iJ end
            { Get names of all fields that were successfully created in the new table. }
            ATable.Close;
            ATable.Master := DestIDA.DMaster;
            ATable.TableName := sTableName;
            ATable.Open;
            sFieldNames := '';
            For iJ:=0 To ATable.Fields.Count-1 Do Begin
                AField := ATable.Fields[iJ];
                If AField.FieldName = cs_InternalID_FieldName Then CONTINUE;
                sFieldNames := sFieldNames + '[' + AField.FieldName + '], ';
                End; // For iJ end
            sFieldNames := Copy(sFieldNames, 1, Length(sFieldNames)-2);
            ATable.Close;
            
            // Move data.
{           INSERT INTO TableName (Field1, Field2, ...) IN 'FullPathToDatabaseFile'
            SELECT Field1, Field2, ... FROM TableName;}
            TRY
               AQuery.ExecSQL('INSERT INTO [' + sTableName + '] (' + sFieldNames + ') IN ''' + sDestFileName + ''' ' +
                               'SELECT ' + sFieldNames + ' FROM [' + sTableName + ']');
            EXCEPT
            END;
            AProgressForm.GlobalGauge.Progress := AProgressForm.GlobalGauge.Progress + 1;
            Application.ProcessMessages;
            End; // For iI end
        slTables.Free;
     FINALLY
        AProgressForm.Hide;
     END;

     SourceIDA.Disconnect;
     DestIDA.Disconnect;
     TRY
        sTempFileName := SaveTempDatabaseFile(SELF.AProject);
        MoveFileEx(cs_TempDatabaseFileName, PChar(sSourceFileName), MOVEFILE_REPLACE_EXISTING);
        DeleteFile(cs_TempDatabaseFileName); // ?? Sometimes it is required to delete the temp file... I don't understand why.
     EXCEPT
     END;
     
     AWinGISMainForm.Enabled := TRUE;

     SourceIDA.Connect(sSourceFileName, '', '');
     AProgressForm.FormStyle := fsNormal;
     // Show the warning for the user.
     // The database file structure has been changed. The old version of the file was saved as '%s'. If You will save the project the old version of the database file will be deleted.
     MessageBox(AWinGISMainForm.Handle, PChar(Format(ALangManager.GetValue(275), [sTempFileName])), PChar (cs_ApplicationName), MB_OK + MB_DEFBUTTON1 + MB_ICONINFORMATION);
End;

end.
