unit IDB_ParamsOfUpdates;
{
Internal database. Ver.II
Form for update parameters for table's field by user.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 02-02-2001
}
interface

uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   ExtCtrls, StdCtrls, DB, DBTables,
   IDB_Access2DB, IDB_StructMan, IDB_QBuilder, IDB_Validators;

type
   TIDB_UpdateForm = class (TForm)
      Panel1: TPanel;
      Panel2: TPanel;
      OkButton: TButton;
      CancelButton: TButton;
      Label1: TLabel;
      Label2: TLabel;
      FieldNamesComboBox: TComboBox;
    SearchEdit: TEdit;
      AdvancedButton: TButton;
    ReplaceEdit: TEdit;
    Label3: TLabel;
    CheckBoxGesamt: TCheckBox;
      procedure FormShow (Sender: TObject);
      procedure SearchEditKeyDown (Sender: TObject; var Key: Word;
         Shift: TShiftState);
      procedure FormCreate (Sender: TObject);
      procedure FieldNamesComboBoxKeyDown (Sender: TObject; var Key: Word;
         Shift: TShiftState);
      procedure AdvancedButtonClick (Sender: TObject);
      procedure FormDestroy (Sender: TObject);
      procedure FieldNamesComboBoxChange (Sender: TObject);
   private
    { Private declarations }
      IntValidator: TIDB_IntValidator;
      FloatValidator: TIDB_FloatValidator;

      IDA: TIDB_Access2DB_Level2;
      AStructureManager: TIDB_StructMan;
      AProject: Pointer;
      iLayerIndex: Integer;
      bWeHaveToDoAdvancedSQL: Boolean;
      UBForm: TIDB_QBForm; // TIDB_UBForm;
      procedure DisconnectAllValidators;
   public
    { Public declarations }
      ADataSet: TDataSet;
      constructor Create (AOwner: TComponent; ADBAccessObject: TIDB_Access2DB_Level2; AProject: Pointer; iLayerIndex: Integer);
      procedure DeleteConditionsSheet;
      function GenerateClause (bCreateFullUpdateClause: Boolean = TRUE): AnsiString;
      property WeHaveToDoAdvancedSQL: Boolean read bWeHaveToDoAdvancedSQL;
   end;

implementation

uses DTables,
   IDB_LanguageManager, IDB_Consts, IDB_Utils, IDB_CallbacksDef, IDB_MainManager;

{$R *.DFM}

constructor TIDB_UpdateForm.Create (AOwner: TComponent; ADBAccessObject: TIDB_Access2DB_Level2; AProject: Pointer; iLayerIndex: Integer);
var
   ATable: TDTable;
   ALayer: Pointer;
begin
   inherited Create (AOwner);
   IDA := ADBAccessObject;
   SELF.AProject := AProject;
   SELF.iLayerIndex := iLayerIndex;
   SELF.bWeHaveToDoAdvancedSQL := FALSE;

   ATable := TDTable.Create (nil);
   ATable.Master := ADBAccessObject.DMaster;

   if not AnIDBMainMan.UseNewATTNames (AProject) then
      ATable.TableName := cs_AttributeTableNamePrefix + IntToStr (iLayerIndex)
   else
   begin
      ALayer := GetLayerPointerByIndexFunc (AProject, iLayerIndex);
      ATable.TableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameFunc (ALayer);
   end;

   SELF.AStructureManager := TIDB_StructMan.Create (AProject, ADBAccessObject);
   SELF.AStructureManager.DetectStructure (ATable);
   if ATable.Active then ATable.Close;
   ATable.Free;

   UBForm := TIDB_QBForm.Create (AProject, Application, ADBAccessObject.DMaster, AStructureManager, GetLayerNameFunc (GetLayerPointerByIndexFunc (AProject, iLayerIndex)), iLayerIndex);
     // 316=Update builder
   UBForm.Caption := ALangManager.GetValue (316);

   IntValidator := TIDB_IntValidator.Create (SELF);
   FloatValidator := TIDB_FloatValidator.Create (SELF);
end;

procedure TIDB_UpdateForm.FormShow (Sender: TObject);
begin
 if ReplaceEdit.Visible then ReplaceEdit.SetFocus else  SearchEdit.SetFocus;
end;

procedure TIDB_UpdateForm.DisconnectAllValidators;
begin
   IntValidator.Edit := nil;
   FloatValidator.Edit := nil;
end;

procedure TIDB_UpdateForm.SearchEditKeyDown (Sender: TObject; var Key: Word; Shift: TShiftState);
begin
   if not (SENDER is TEdit) then EXIT; // Check just in case.
     // If ESC key has been pressed on Edit, check our behavior.
   case Key of
      vk_Return:
         begin
            Key := 0;
            OKButton.Click;
         end;
      vk_Escape:
         begin
            Key := 0;
            CancelButton.Click;
         end;
   end; // case end
end;

procedure TIDB_UpdateForm.FormCreate (Sender: TObject);
begin
   ALangManager.MakeReplacement (SELF);
end;

procedure TIDB_UpdateForm.FieldNamesComboBoxKeyDown (Sender: TObject; var Key: Word; Shift: TShiftState);
begin
   if not (SENDER is TCombobox) then EXIT; // Check just in case.
     // If ESC key has been pressed on dropped ComboBox, let it itself do what it have to.
   if FieldNamesComboBox.DroppedDown then EXIT;
     // If ESC key has been pressed and Combobox is not dropped, check our behavior.
   case Key of
      vk_Return:
         begin
            Key := 0;
            OKButton.Click;
         end;
      vk_Escape:
         begin
            Key := 0;
            CancelButton.Click;
         end;
   end; // case end
end;

function TIDB_UpdateForm.GenerateClause (bCreateFullUpdateClause: Boolean = TRUE): AnsiString;
var
   sFieldName, sFieldValue, sResult: AnsiString;
   sAttTableName: AnsiString;
   iI: Integer;
   ALayer: Pointer;
begin
   RESULT := '';

   if not AnIDBMainMan.UseNewATTNames (AProject) then
      sAttTableName := cs_AttributeTableNamePrefix + IntToStr (SELF.iLayerIndex)
   else
   begin
      ALayer := GetLayerPointerByIndexFunc (AProject, iLayerIndex);
      sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);
   end;

   if bCreateFullUpdateClause then
      sResult := 'UPDATE [' + sAttTableName + '] ';

   if bWeHaveToDoAdvancedSQL then
   begin
      sResult := sResult + 'SET ';
      for iI := 0 to UBForm.SG.RowCount - 1 do
      begin
         sFieldName := UBForm.SG.Cells[0, iI];
         if (Trim (sFieldName) = '') then CONTINUE;
            // The text can be in the inplace editor, not in the grid yet.
{            If (UBForm.SG.Row = iI) AND (UBForm.SG.InplaceEditor <> NIL) Then
               sFieldValue := UBForm.SG.InplaceEditor.Text
            Else }
                       (* I ws wrong?? Ivanoff. 09.01.2002 *)
         sFieldValue := UBForm.SG.Cells[1, iI];

         if (AStructureManager.GetFieldType (sFieldName) <> 1) and (Trim (sFieldValue) = '') then CONTINUE;
         sResult := sResult + '[' + sFieldName + '] = ' + UBForm.MakeIt (sFieldName, sFieldValue) + ', ';
      end;
        // Truncate last comma.
      sResult := Copy (sResult, 1, Length (sResult) - 2);

      if Trim (UBForm.Memo2.Text) <> '' then
      begin
         sResult := sResult + ' WHERE (';
         for iI := 0 to UBForm.Memo2.Lines.Count - 1 do
            sResult := sResult + ' ' + UBForm.Memo2.Lines[iI] + ' ';
         sResult := sResult + ')';
      end;
   end
   else
   begin
      sFieldName := FieldNamesComboBox.Items[FieldNamesComboBox.ItemIndex];
      sResult := sResult + 'SET [' + sFieldName + '] = ' + UBForm.MakeIt (sFieldName, SearchEdit.Text);
      if self.CheckBoxGesamt.Visible then
         begin
         if self.CheckBoxGesamt.Checked then
          begin
          if ReplaceEdit.Text='' then
            sReSult:= sResult + ' WHERE [' + sFieldName + '] is Null or [' + sFieldName + '] like ""' else     // or like ""
            sReSult:= sResult + ' WHERE [' + sFieldName + '] LIKE '''+ ReplaceEdit.Text + '''';
          end
          else
          sReSult:= sResult + ' WHERE [' + sFieldName + '] LIKE ''%'+ ReplaceEdit.Text + '%''';          
         end;
   end;
   RESULT := sResult;
end;

procedure TIDB_UpdateForm.AdvancedButtonClick (Sender: TObject);
begin
   UBForm.ComboBox1.Items.Assign (SELF.FieldNamesComboBox.Items);
   UBForm.ValuesFieldsListBox.Items.Assign (SELF.FieldNamesComboBox.Items);
     // 'ProgisID' cannot be updated but it can take part in the setting of conditions.
   UBForm.ValuesFieldsListBox.Items.Insert (0, 'ProgisID');
   UBForm.ValuesFieldsListBox.ItemIndex := 0;
   UBForm.InitializeGrid (FieldNamesComboBox.Items[FieldNamesComboBox.ItemIndex]);
   UBForm.SG.Cells[1, 0] := SearchEdit.Text;
   bWeHaveToDoAdvancedSQL := UBForm.ShowModal = mrOK;
   if bWeHaveToDoAdvancedSQL then OkButton.Click;
end;

procedure TIDB_UpdateForm.FormDestroy (Sender: TObject);
begin
   AStructureManager.Free;
   UBForm.Free;
end;

procedure TIDB_UpdateForm.FieldNamesComboBoxChange (Sender: TObject);
var
   sText: AnsiString;
   iI: Integer;
begin
   DisconnectAllValidators;
   sText := SearchEdit.Text;
   iI := 1;
   case AStructureManager.GetFieldType (FieldNamesComboBox.Items[FieldNamesComboBox.ItemIndex]) of
      1:
         begin
         end; // String
      2, // SmallInt
         3, // Integer
         12: // Byte
         begin
            IntValidator.Edit := SearchEdit;
            if not IntValidator.IsValidInput (sText, iI) then SearchEdit.Text := '';
         end;
      6, // Float
         7: // Currency
         begin
            FloatValidator.Edit := SearchEdit;
            if not FloatValidator.IsValidInput (sText, iI) then SearchEdit.Text := '';
         end;
   end; // Case end
   // 2. Feld
if ReplaceEdit.Visible then
begin
   sText := ReplaceEdit.Text;
   iI := 1;
   case AStructureManager.GetFieldType (FieldNamesComboBox.Items[FieldNamesComboBox.ItemIndex]) of
      1:
         begin
         end; // String
      2, // SmallInt
         3, // Integer
         12: // Byte
         begin
            IntValidator.Edit := ReplaceEdit;
            if not IntValidator.IsValidInput (sText, iI) then ReplaceEdit.Text := '';
         end;
      6, // Float
         7: // Currency
         begin
            FloatValidator.Edit := ReplaceEdit;
            if not FloatValidator.IsValidInput (sText, iI) then ReplaceEdit.Text := '';
         end;
   end; // Case end
end;
end;

procedure TIDB_UpdateForm.DeleteConditionsSheet;
begin
   SELF.UBForm.TabSheet2.TabVisible := FALSE;
end;

end.

