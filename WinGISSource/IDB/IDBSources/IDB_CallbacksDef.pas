unit IDB_CallBacksDef;
{
Internal database. Ver. II.
This unit contains declarations of callbacks function of IDB that I use for
interaction between WinGIS and IDB and also some constants that have to be knowon
in WinGIS.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 09-02-2001
}
interface

uses Gauges, Forms, Classes,
   Objects, Lists;

type
   TUndoType = (utUnInsert,
      utUnDelete,
      utUnChange,
      utUnMove,
      utUnResize,
      utUnTransform,
      utUnknownUNDO);
   TRedoType = (rtInsert,
      rtDelete,
      rtChange,
      rtMove,
      rtResize,
      rtTransform,
      rtUnknownREDO);

   TrecRecordTemplateItem = record
      sFieldName: AnsiString;
      iFieldType: Integer;
      sValue: AnsiString;
   end;

   TShowProject = procedure (AProject: Pointer); stdcall;
   TShowSelectedItemsInProject = procedure (AProject, ALayer: Pointer; iItemID: Integer; bSelected, bNeedZoom: Boolean); stdcall;
   TShowItemInProjectWithZoom = procedure (AProject, ALayer: Pointer; iItemID: Integer); stdcall;
   TDeselectAllObjects = procedure (AProject: Pointer); stdcall;
   TGetProjectUndoStepsCount = function (AProject: Pointer): integer; stdcall;
   TGetProjectFileSize = function (AProject: Pointer): integer; stdcall;

   TItemExistsInProject = function (AProject: Pointer; ALayer: Pointer; iItemID: Integer): Boolean; stdcall;
   TItemToGUID = function (AProject: Pointer; ALayer: Pointer; iItemID: Integer): TGUID; stdcall;
   TSetProjectModified = procedure (AProject: Pointer); stdcall;

   TSetStartProjectGUID = procedure (AProject: Pointer; pcGUID: PChar); stdcall;
   TSetCurrentProjectGUID = procedure (AProject: Pointer; pcGUID: PChar); stdcall;
   TGetStartProjectGUID = function (AProject: Pointer): PChar; stdcall;
   TGetCurrentProjectGUID = function (AProject: Pointer): PChar; stdcall;

   TGetProjectIsNewAndNotSaved = function (AProject: Pointer): Boolean; stdcall;
   TGetProjectFullFileName = function (AProject: Pointer): PChar; stdcall;

// ++ Cadmensky Version 2.2.8
   TGetLayerItemsCount = function (AProject, ALayer: Pointer; bSelected: Boolean = false): Integer; stdcall;
   TGetLayerItemIndexByNumber = function (AProject, ALayer: Pointer; iItemNumber: Integer; bSelected: Boolean): Integer; stdcall;
   TIsObjectSelected = function (AProject: Pointer; iItemID: Integer): Boolean; stdcall;
// -- Cadmensky Version 2.2.8

   TGetLayerName = function (ALayer: Pointer): PChar; stdcall;
   TGetLayerIndex = function (ALayer: Pointer): Integer; stdcall;
   TLayerIsFixed = function (ALayer: Pointer): Boolean; stdcall;

   TChangeGeoTextParameters = procedure (AProject, ALayer: Pointer; pcGeoTextIDFieldName, pcGeoTextDataFieldName: PChar); stdcall;

   TGetAllProjectWindowsNumber = function: Integer; stdcall;
   TGetProjectLayerNumber = function (AProject: Pointer): Integer; stdcall; // The funstion returns all number of layers including the layer of selected objects.
   TGetLayerPointerByPosition = function (AProject: Pointer; iLayerPos: Integer): Pointer; stdcall;
   TGetLayerPointerByIndex = function (AProject: Pointer; iLayerIndex: Integer): Pointer; stdcall;
   TGetActiveLayer = function (AProject: Pointer): Pointer; stdcall;

   TGetWinGISINIFileName = function: PChar; stdcall;
   TGetDefaultProjectFileName = function: PChar; stdcall;

   TGetNecessityInMakeDatabaseFileBackup = function (AProject: Pointer): Boolean; stdcall;

   TGeneratePoint = function (AProject: Pointer; ALayer: Pointer; ilXCoord, ilYCoord: LongInt; iExistedProgisID: Integer; var iNewProgisID: Integer): Boolean; stdcall;
   TGenerateSymbol = function (AProject: Pointer; ALayer: Pointer; ilXCoord, ilYCoord: LongInt; iSymbolNumber: Integer; dblSymbolSize: Double; iExistedProgisID: Integer; var iNewProgisID: Integer): Boolean; stdcall;
// ++ Cadmensky Version 2.3.6
   TMakeAnnotationInProject = function (AProject, AnObjectLayer: Pointer; iItemID: Integer; sAnnotData: PChar): Boolean; stdcall;
//   TMakeAnnotationInProject = function (AProject, AnObjectLayer: Pointer; iItemID: Integer; AnAnnotData: PCollection): Boolean; stdcall;
// -- Cadmensky Version 2.3.6
   TMakeAnnotationUpdate = procedure (ALayer: Pointer; iItemID: LongInt); stdcall;
{brovak}
   TMakeChartInProject = function (AProject, AnObjectLayer: Pointer; iItemID: Integer; AnChartData: PCollection): Boolean; stdcall;
   TMakeChartUpdate = procedure (ALayer: Pointer; iItemID: LongInt); stdcall;
    //
   TLayerForInsert = function (Aproject, Alayer: pointer): Pointer; stdcall;
   TMakeSpecialChart = procedure (AProject, Alayer: Pointer); stdcall;
   TSendSpecialforChart = procedure (AProject: Pointer; PseudoDDEString: Ansistring); stdcall;
   TShowNewThematicMapDialog = procedure (AProject: Pointer; sConnection, sSQLText, sProgisIDFieldName, sThematicFieldName: Ansistring); stdcall;
   TSendToTMSelectedID = procedure (AProject: Pointer; AProgisIDList: TIntList); stdcall;

   TShowPercentAtWinGIS = procedure (Flag, IDMessage, IDCurrent: integer); stdcall;
{brovak}

   TShowDonePercent = procedure (iDonePercent: Integer); stdcall;

// ++ Commented by Cadmensky Version 2.2.8
//   TGatherGarbageInProject = function (AProject, ALayer: Pointer; AGauge: Integer; ADatabaseWindow: TForm; ARecordTemplate: TList): Integer; stdcall;
// -- Commented by Cadmensky Version 2.2.8

   TSetObjectStyle = function (AProject, ALayer: Pointer; iItemID, iLineColor, iLineStyle, iLineWidth, iPatternColor, iPatternStyle, iTransparency: Integer): Boolean; stdcall;
   TRedrawProjectWindow = procedure (AProject: Pointer); stdcall;

   TAnnotationSettingsDialogExecute = function (AProject, ACurrentLayer: Pointer; AnAnnotData: PCollection): Boolean; stdcall;
{brovak}
   TChartSettingsDialogExecute = function (AProject, ACurrentLayer: Pointer; AnChartData: PCollection): Boolean; stdcall;
{brovak}
   TRunThematicMapBuilder = function (AProject, ALayer: Pointer; pcAttTableName, pcDefaultDataSourceField, pcListOfFieldsThatAreNotToBeVisible: PChar; AGauge: TGauge): Boolean; stdcall;

   TRunMonitoring = procedure (AProject: Pointer; bNeedToShowMonitoringWindow: Boolean); stdcall;

   TDataProps = ({dtLength, }dtArea, {dtPerimeter, }dtX, dtY, dtX1, dtY1, dtVerticesCount, dtSymbolName, dtSize, dtText, dtParent, dtRotation);
   TGetObjectProps = function (AProject, ALayer: Pointer; iItemID: Integer; ADataPropsType: TDataProps): PChar; stdcall;

   TReverseUsingIDBInfo = procedure;
   TGetMDIChildWindowState = function: TWindowState;

// ++ Cadmensky
   TSetNessesitySaveUndoDataSign = procedure (AProject: Pointer; bValue: boolean);
   TSaveUndoData = procedure (AProject: Pointer; AnUndoType: TUndoType);
// -- Cadmensky

var
   ShowProjectProc: TShowProject;
   ShowSelectedItemsInProjectProc: TShowSelectedItemsInProject;
   ShowItemInProjectWithZoomProc: TShowItemInProjectWithZoom;
   DeselectAllObjectsProc: TDeselectAllObjects;
   GetProjectUndoStepsCountFunc: TGetProjectUndoStepsCount;
   GetProjectFileSizeFunc: TGetProjectUndoStepsCount;

   ItemExistsInProjectFunc: TItemExistsInProject;
   ItemToGUIDFunc: TItemToGUID;
   SetProjectModifiedProc: TSetProjectModified;

   SetStartProjectGUIDProc: TSetStartProjectGUID;
   SetCurrentProjectGUIDProc: TSetCurrentProjectGUID;
   GetStartProjectGUIDFunc: TGetStartProjectGUID;
   GetCurrentProjectGUIDFunc: TGetCurrentProjectGUID;

   GetProjectIsNewAndNotSavedStateFunc: TGetProjectIsNewAndNotSaved;
   GetProjectFullFileNameFunc: TGetProjectFullFileName;

// ++ Cadmensky Version 2.2.8
   GetLayerItemsCountFunc: TGetLayerItemsCount;
   GetLayerItemIndexByNumberFunc: TGetLayerItemIndexByNumber;
   IsObjectSelectedFunc: TIsObjectSelected;
// -- Cadmensky Version 2.2.8

   GetLayerNameFunc: TGetLayerName;
   GetLayerIndexFunc: TGetLayerIndex;

   ChangeGeoTextParametersProc: TChangeGeoTextParameters;

   GetAllProjectWindowsNumberFunc: TGetAllProjectWindowsNumber;
   GetProjectLayerNumberFunc: TGetProjectLayerNumber;
   GetLayerPointerByPositionFunc: TGetLayerPointerByPosition;
   GetLayerPointerByIndexFunc: TGetLayerPointerByIndex;
   GetActiveLayerFunc: TGetActiveLayer;
   LayerIsFixedFunc: TLayerIsFixed;

   GetWinGISINIFileNameFunc: TGetWinGISINIFileName;
   GetDefaultProjectFileNameFunc: TGetDefaultProjectFileName;

   GeneratePointFunc: TGeneratePoint;
   GenerateSymbolFunc: TGenerateSymbol;

   AnnotationSettingsDialogExecuteFunc: TAnnotationSettingsDialogExecute;
   MakeAnnotationInProjectFunc: TMakeAnnotationInProject;
   MakeAnnotationUpdateProc: TMakeAnnotationUpdate;
{brovak}
   LayerForInsertFunc: TLayerForInsert;
   ChartSettingsDialogExecuteFunc: TChartSettingsDialogExecute;
   MakeChartInProjectFunc: TMakeChartInProject;
   MakeChartUpdateProc: TMakeChartUpdate;
   //

   MakeSpecialChartProc: TMakeSpecialChart;
   SendSpecialforChartProc: TSendSpecialforChart;

   ShowNewThematicMapDialogProc: TShowNewThematicMapDialog;
   SendToTMSelectedIDProc: TSendToTMSelectedID;

   ShowPercentAtWinGISProc: TShowPercentAtWinGIS;
{brovak}

   ShowDonePercentProc: TShowDonePercent;

// ++ Commented by Cadmensky Version 2.2.8
//   GatherGarbageInProjectFunc: TGatherGarbageInProject;
//   GatherSelectedGarbageInProjectFunc: TGatherGarbageInProject;
// -- Commented by Cadmensky Version 2.2.8

   RedrawProjectWindowProc: TRedrawProjectWindow;

   RunThematicMapBuilderFunc: TRunThematicMapBuilder;
   RunMonitoringProc: TRunMonitoring;

   GetObjectPropsFunc: TGetObjectProps;

   ReverseUsingIDBInfoProc: TReverseUsingIDBInfo;
   GetMDIChildWindowStateFunc: TGetMDIChildWindowState;
// ++ Cadmensky
//   FillLayerNamesListProc: TFillLayerNamesList;
   SetNessesitySaveUndoDataSignProc: TSetNessesitySaveUndoDataSign;
   SaveUndoDataProc: TSaveUndoData;
// -- Cadmensky
implementation

end.

