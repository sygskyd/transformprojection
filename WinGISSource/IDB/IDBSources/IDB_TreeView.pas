unit IDB_TreeView;

interface

Uses TreeView;

Type
    TIDB_TreeEntry = Class(TTreeEntry)
       Private
          FData: Pointer;
       Public
          Property Data: Pointer Read FData Write FData;
       End;

implementation

end.
