unit IDB_PolygonOverlayManager;
{
Internal database. Ver. II.
The manager for merging attribute data during polygon overlaying operation.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 06-12-2001
}
interface

uses
   IDB_Access2DB, IDB_StructMan, IDB_DescMan, Classes, DTables;

type
   TOverlayField = class (TObject)
   public
      FieldName: AnsiString;
      FieldType: integer;
      FieldSize: integer;
      Calculated: boolean;
      Field1, Field2: AnsiString;
      Coefficient: double;
      Precision: integer;
   end;

   TIDB_PolygonOverlayManager = class
   private
      IDA: TIDB_Access2DB_Level2;
      AStructMan: TIDB_StructMan;
      AProject,
         ASourceLayer1,
         ASourceLayer2,
         ADestinationLayer: Pointer;
      OverlayFieldsList: TList;
      function GetFieldValue (SourceQuery1, SourceQuery2: TDQuery; AOverlayField: TOverlayField; iDestItemID, iSourceItemID1, iSourceItemID2: Integer): AnsiString;
      procedure SetFieldTypeAndSizeByFullName (sFullFieldName: AnsiString; AOverlayField: TOverlayField);
   public
      constructor Create (ADataAccessItem: TIDB_Access2DB_Level2; AProject, ASourceLayer1, ASourceLayer2, ADestinationLayer: Pointer);
      destructor Free;

      function MakeParams: Boolean;
      procedure InsertItem (ALayer: Pointer; iItemID, iAItemID, iBItemID: Integer);
   end;

implementation

uses Forms, Controls, SysUtils,
//     DTables,
//     IDB_ParamsOfPolygonOverlay,
   IDB_CallbacksDef, IDB_Consts,
   IDB_Utils, IDB_MainManager, IDB_View, IDB_LayerManager, IDB_DatabaseItemsManager,ComObj;

constructor TIDB_PolygonOverlayManager.Create (ADataAccessItem: TIDB_Access2DB_Level2; AProject, ASourceLayer1, ASourceLayer2, ADestinationLayer: Pointer);
begin
   SELF.IDA := ADataAccessItem;
   SELF.AStructMan := TIDB_StructMan.Create (AProject, ADataAccessItem);
   SELF.AProject := AProject;
   SELF.ASourceLayer1 := ASourceLayer1;
   SELF.ASourceLayer2 := ASourceLayer2;
   SELF.ADestinationLayer := ADestinationLayer;
   SELF.OverlayFieldsList := TList.Create;
end;

destructor TIDB_PolygonOverlayManager.Free;
var
   i: integer;
begin
   SELF.AStructMan.Free;
   for i := 0 to SELF.OverlayFieldsList.Count - 1 do
      TOverlayField (SELF.OverlayFieldsList.Items[i]).Free;
   SELF.OverlayFieldsList.Free;
end;

function TIDB_PolygonOverlayManager.MakeParams: Boolean;
var
   ATable: TDTable;
   ALayerManager: TIDB_LayerManager;
//   PPOF: TIDB_ParamsOfPolygonOverlayForm;

   procedure FillOverlayFieldsList (ALayer: Pointer);
   var
      AOverlayField: TOverlayField;
      ADescMan: TIDB_DescMan;
      AStringList: TStringList;
      sLayerName: AnsiString;
      i: integer;
   begin
      AStringList := TStringList.Create;

      ADescMan := ALayerManager.GetLayerDescMan (ALayer);
      if ADescMan = nil then
      begin
         ADescMan := TIDB_DescMan.Create (AProject, Self.IDA, ALayer);
         AStringList.Text := ADescMan.GetOverlayFieldNames;
         ADescMan.Free;
      end
      else
         AStringList.Text := ADescMan.GetOverlayFieldNames;

      sLayerName := GetLayerNameFunc (ALayer);
//        PPOF.ListBox1.Items.Text := ADescMan.GetOverlayFieldNames;
      for i := 0 to AStringList.Count - 1 do
      begin
         AOverlayField := TOverlayField.Create;
         AOverlayField.Precision := -1;
         AOverlayField.Coefficient := 1;
         if Pos ('/', AStringList.Strings[i]) > 0 then
         begin
            AOverlayField.FieldName := ReplaceChars (sLayerName) + '_' + Trim (Copy (AStringList.Strings[i], 1, Pos ('/', AStringList.Strings[i]) - 1));
            AOverlayField.Field1 := sLayerName + '.' + Trim (Copy (AStringList.Strings[i], 1, Pos ('/', AStringList.Strings[i]) - 1));
            AOverlayField.Field2 := sLayerName + '.' + cs_CalcFieldAreaName;
            AOverlayField.Calculated := true;
            AOverlayField.FieldType := 6;
            AOverlayField.FieldSize := 8;
         end
         else
         begin
            AOverlayField.FieldName := ReplaceChars (sLayerName) + '_' + AStringList.Strings[i];
            AOverlayField.Field1 := sLayerName + '.' + AStringList.Strings[i];
            AOverlayField.Calculated := false;
            SetFieldTypeAndSizeByFullName (AOverlayField.Field1, AOverlayField);
         end;
         Self.OverlayFieldsList.Add (AOverlayField);
      end;
      AStringList.Free;
   end;
var
   iI, iP: Integer;
   sAttTableName: AnsiString;
   ADBItemsManager: TIDB_DatabaseItemsManager;
   AOverlayField: TOverlayField;
begin
// ++ Cadmensky Version 2.3.2
   RESULT := false;

   ALayerManager := AnIDBMainMan.GetProjectLayerManager (AProject);
   if ALayerManager <> nil then
   begin
      iP := ALayerManager.FindLayer (SELF.ASourceLayer1);
      if iP < 0 then
         iP := ALayerManager.AddLayer (SELF.ASourceLayer1);
      if iP < 0 then
         exit;

      iP := ALayerManager.FindLayer (SELF.ASourceLayer2);
      if iP < 0 then
         iP := ALayerManager.AddLayer (SELF.ASourceLayer2);
      if iP < 0 then
         exit;

      iP := ALayerManager.FindLayer (SELF.ADestinationLayer);
      if iP < 0 then
         iP := ALayerManager.AddLayer (SELF.ADestinationLayer);
      if iP < 0 then
         exit;
   end
   else
      exit;
// -- Cadmensky Version 2.3.2

   ATable := TDTable.Create (nil);
   try
      ATable.Master := SELF.IDA.DMaster;

      if not AnIDBMainMan.UseNewATTNames (AProject) then
         sAttTableName := cs_AttributeTableNamePrefix + IntToStr (GetLayerIndexFunc (SELF.ADestinationLayer))
      else
         sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, SELF.ADestinationLayer);

      ATable.TableName := sAttTableName;
      AStructMan.DetectStructure (ATable);
      ATable.Close;
      ATable.Free;
   except
      ATable.Close;
      ATable.Free;
      EXIT;
   end;
//     PPOF := TIDB_ParamsOfPolygonOverlayForm.Create(Application, SELF.AProject, SELF.AStructMan);
   try
{        PPOF.ASourceLayer1 := SELF.ASourceLayer1;
        PPOF.ASourceLayer2 := SELF.ASourceLayer2;
        PPOF.Label6.Caption := '''' + GetLayerNameFunc(SELF.ADestinationLayer) + '''';}

// ++ Cadmensky
      Self.OverlayFieldsList.Clear;
      FillOverlayFieldsList (ASourceLayer1);
      FillOverlayFieldsList (ASourceLayer2);

// ++ Cadmensky Version 2.3.2
      ADBItemsManager := ALayerManager.DataAccessObject.ADBItemsMan;

      if ADBItemsManager.GetReadyToCheckTableStructure (sAttTableName) <> ci_All_Ok then
      begin
         ADBItemsManager.EndOfCheckingTable;
         Exit;
      end;

      for iI := 0 to SELF.OverlayFieldsList.Count - 1 do
      begin
         AOverlayField := TOverlayField (OverlayFieldsList.Items[iI]);
         if not ADBItemsManager.FieldExists (AOverlayField.FieldName) then
            ADBItemsManager.AddField (sAttTableName, AOverlayField.FieldName, AOverlayField.FieldType, AOverlayField.FieldSize);
      end;

      ADBItemsManager.EndOfCheckingTable;
      RESULT := true;
// -- Cadmensky Version 2.3.2

{        RESULT := PPOF.ShowModal = mrOK;
        if RESULT then
        begin
           Self.OverlayFieldsList.Clear;
           for i := 1 to PPOF.SG.RowCount - 1 do
              Self.OverlayFieldsList.Add (PPOF.SG.Objects[2, i]);
        end;}
   finally
//        PPOF.Free;
   end;
end;

procedure TIDB_PolygonOverlayManager.InsertItem (ALayer: Pointer; iItemID, iAItemID, iBItemID: Integer);

   procedure CreateAndOpenQuery (sLayerTableName, sItemIndex: AnsiString; var AAQuery: TDQuery);
   begin
      AAQuery := TDQuery.Create (nil);
      AAQuery.Master := IDA.DMaster;
      AAQuery.SQL.Clear;
      AAQuery.SQL.Add ('SELECT * FROM [' + sLayerTableName + ']');
      AAQuery.SQL.Add ('WHERE [PROGISID] = ' + sItemIndex);
      try
         AAQuery.Open;
      except
      end;
   end;

var
   iI: integer;
   ALayerManager: TIDB_LayerManager;
   sLayerIndex, sNewItemIndex,
      sAttTableName, sSourceLayerAttTableName, sSQL: AnsiString;
   sFieldValue: AnsiString;
   DestQuery, SourceQuery1, SourceQuery2: TDQuery;
   AForm: TForm;
   sSQL1, sSQL2: AnsiString;
   AOverlayField: TOverlayField;
   AGUID: TGUID;
begin

   if not AnIDBMainMan.UseNewATTNames (AProject) then
   begin
      sLayerIndex := IntToStr (GetLayerIndexFunc (ALayer));
      sAttTableName := cs_AttributeTableNamePrefix + sLayerIndex;
   end
   else
      sAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ALayer);

   sNewItemIndex := IntToStr (iItemID);
// ++ Cadmensky
     // Check whether the attribute table is exists or not.
{     if not SELF.IDA.ADbItemsMan.TableExists(sAttTableName) then
        IDA.ADbItemsMan.CreateAttributeTable(sAttTableName)
     else // if it exists, check all required fields in it.
        if not CheckRequiredFields(AnIDBMainMan.GetProjectLayerManager (AProject), ALayer, sAttTableName) then
        begin
           sLayerIndex := ''; // Foolish compilator. I want to get out, but it does not compile next line without this one.
           EXIT;
        end;}
   ALayerManager := AnIDBMainMan.GetProjectLayerManager (AProject);
   if ALayerManager = nil then
      Exit;
// ++ Commented by Cadmensky Version 2.3.2
{   ALayerManager := AnIDBMainMan.GetProjectLayerManager (AProject);
   if ALayerManager <> nil then
   begin
      iP := ALayerManager.FindLayer (ALayer);
      if iP < 0 then
         iP := ALayerManager.AddLayer (ALayer);
      if iP < 0 then
         exit;
   end
   else
      exit;}
// -- Commented by Cadmensky Version 2.3.2
// -- Cadmensky

     // When we move object from one layer to another we can find a situation
     // when we already have data about inserted object but this data is marked as
     // deleted. In this case instead of inserting new record, I mark this data as
     // normal existing.
   CreateAndOpenQuery (sAttTableName, sNewItemIndex, DestQuery);

   sLayerIndex := IntToStr (GetLayerIndexFunc (ASourceLayer1));

   if not AnIDBMainMan.UseNewATTNames (AProject) then
      sSourceLayerAttTableName := cs_AttributeTableNamePrefix + sLayerIndex
   else
      sSourceLayerAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ASourceLayer1);

   CreateAndOpenQuery (sSourceLayerAttTableName, IntToStr (iAItemID), SourceQuery1);

   sLayerIndex := IntToStr (GetLayerIndexFunc (ASourceLayer2));

   if not AnIDBMainMan.UseNewATTNames (AProject) then
      sSourceLayerAttTableName := cs_AttributeTableNamePrefix + sLayerIndex
   else
      sSourceLayerAttTableName := cs_AttributeTableNamePrefix + '_' + GetLayerNameEx (AProject, ASourceLayer2);

   CreateAndOpenQuery (sSourceLayerAttTableName, IntToStr (iBItemID), SourceQuery2);

   if DestQuery.RecordCount > 0 then
   begin
        // This way I go when I have such data marked as deleted.
      DestQuery.Close;
      sSQL := 'UPDATE [' + sAttTableName + '] ' +
         'SET [' + cs_StateFieldName + '] = ' + cs_ObjectIsNormal;
      for iI := 0 to OverlayFieldsList.Count - 1 do
      begin
         AOverlayField := TOverlayField (OverlayFieldsList.Items[iI]);
         sFieldValue := GetFieldValue (SourceQuery1, SourceQuery2, AOverlayField, iItemID, iAItemID, iBItemID);
         if sFieldValue <> '' then
         begin
// ++ Commented by Cadmensky Version 2.3.2
//            if CheckOverlayField (AnIDBMainMan.GetProjectLayerManager (AProject), sAttTableName, TOverlayField (OverlayFieldsList.Items[iI])) then
// -- Commented by Cadmensky Version 2.3.2
            if (AOverlayField.FieldType = 2) or
               (AOverlayField.FieldType = 3) or
               (AOverlayField.FieldType = 4) or
               (AOverlayField.FieldType = 6) or
               (AOverlayField.FieldType = 12) then
               sSQL := sSQL + ', [' + AOverlayField.FieldName + '] = ' + sFieldValue
            else
               sSQL := sSQL + ', [' + AOverlayField.FieldName + '] = ''' + sFieldValue + '''';
         end;
      end;
      sSQL := sSQL + ' WHERE [PROGISID] = ' + sNewItemIndex;
        // UPDATE table_name SET field_name = new_value WHERE key_field = key_value
      try
         DestQuery.ExecSQL (sSQL);
      except
      end;
   end
   else
   begin
      DestQuery.Close;
      for iI := 0 to OverlayFieldsList.Count - 1 do
      begin
         AOverlayField := TOverlayField (OverlayFieldsList.Items[iI]);
         sFieldValue := GetFieldValue (SourceQuery1, SourceQuery2, AOverlayField, iItemID, iAItemID, iBItemID);
         if sFieldValue <> '' then
         begin
// ++ Commented by Cadmensky Version 2.3.2
//            if CheckOverlayField (AnIDBMainMan.GetProjectLayerManager (AProject), sAttTableName, TOverlayField (OverlayFieldsList.Items[iI])) then
//            begin
// -- Commented by Cadmensky Version 2.3.2
            sSQL1 := sSQL1 + ', [' + AOverlayField.FieldName + ']';
            {if (AOverlayField.FieldType = 2) or
               (AOverlayField.FieldType = 3) or
               (AOverlayField.FieldType = 4) or
               (AOverlayField.FieldType = 6) or
               (AOverlayField.FieldType = 12) then
               sSQL2 := sSQL2 + ', ' + sFieldValue
            else}
               sSQL2 := sSQL2 + ', ''' + sFieldValue + '''';
//            end;
         end;
      end;
      DestQuery.Open;

      AGUID:=ItemToGuidFunc(AProject,ALayer,iItemID);

        // This way I go if there is no data about just inserted object.
      sSQL := 'INSERT INTO [' + sAttTableName + '] ([PROGISID],[GUID],[' + cs_StateFieldName + ']' + sSQL1 + ') ' +
         'VALUES (' + sNewItemIndex + ',' + '''' + GuidToString(AGUID) + '''' + ',' + cs_ObjectIsJustAdded + sSQL2 + ')';
        // Insert it.
      try // Just in case...
         DestQuery.ExecSQL (sSQL);
      except
      end;
   end;
// ++ Cadmensky Version 2.3.1
   DestQuery.Free;
   SourceQuery1.Free;
   SourceQuery2.Free;
// -- Cadmensky Version 2.3.1

   ALayerManager.FillCalculatedField (ALayer, iItemID);
     // Move on new record.
// ++ Commented by Cadmensky Version 2.3.5
{   ALayerManager.RefreshAllDataWindows;

   if (AnIDBMainMan.AViewForm <> nil) and
      (AnIDBMainMan.AViewForm.AProject = AProject) and
      (AnIDBMainMan.AViewForm.ALayer = ALayer) then
      AnIDBMainMan.AViewForm.ViewDataSource.DataSet.Locate ('PROGISID', iItemID, []);}
// -- Commented by Cadmensky Version 2.3.5
{     for iI := 0 to Screen.FormCount - 1 do
     begin
         AForm := Screen.Forms[iI];
         if (AForm is TIDB_ViewForm)
            and (TIDB_ViewForm(AForm).AProject = AProject)
            and (TIDB_ViewForm(AForm).ALayer = ALayer) then
               TIDB_ViewForm(AForm).ViewDataSource.DataSet.Locate('PROGISID', iItemID, []);
     end; // for iI end}

end;

function TIDB_PolygonOverlayManager.GetFieldValue (SourceQuery1, SourceQuery2: TDQuery; AOverlayField: TOverlayField; iDestItemID, iSourceItemID1, iSourceItemID2: Integer): AnsiString;
var
   sLayer1Name, sLayer2Name: AnsiString;
   sFieldName: AnsiString;
   fFieldValue: double;
   fSourceArea, fDestArea: Double;
   sValue: AnsiString;
   sFieldValue: AnsiString;
begin
   RESULT := '';
   try
      if AOverlayField.Calculated then
      begin
         sLayer1Name := GetLayerNameFunc (ASourceLayer1);
         sLayer2Name := GetLayerNameFunc (ASourceLayer2);
         sFieldName := Copy (AOverlayField.Field1, Pos ('.', AOverlayField.Field1) + 1, Length (AOverlayField.Field1) - Pos ('.', AOverlayField.Field1));

         if sLayer1Name = Copy (AOverlayField.Field1, 1, Pos ('.', AOverlayField.Field1) - 1) then
            try
               fFieldValue := StrToFloat (SourceQuery1.FieldByName (sFieldName).AsString);
               sValue := GetObjectPropsEx (SELF.AProject, ASourceLayer1, iSourceItemID1, dtArea);
               fSourceArea := StrToFloat (ReplaceChars (sValue, '.', DecimalSeparator));
            except
            end
         else
            if sLayer2Name = Copy (AOverlayField.Field1, 1, Pos ('.', AOverlayField.Field1) - 1) then
               try
                  fFieldValue := StrToFloat (SourceQuery2.FieldByName (sFieldName).AsString);
                  sValue := GetObjectPropsEx (SELF.AProject, ASourceLayer2, iSourceItemID2, dtArea);
                  fSourceArea := StrToFloat (ReplaceChars (sValue, '.', DecimalSeparator));
               except
               end
            else
               exit;

         sValue := GetObjectPropsEx (SELF.AProject, ADestinationLayer, iDestItemID, dtArea);
         fDestArea := StrToFloat (ReplaceChars (sValue, '.', DecimalSeparator));
{         sFieldName := Copy (AOverlayField.Field2, Pos ('.', AOverlayField.Field2) + 1, Length (AOverlayField.Field2) - Pos ('.', AOverlayField.Field2));
         if sLayer1Name = Copy (AOverlayField.Field2, 1, Pos ('.', AOverlayField.Field2) - 1) then
            fFieldValue2 := StrToFloat (SourceQuery1.FieldByName (sFieldName).AsString)
         else
            if sLayer2Name = Copy (AOverlayField.Field2, 1, Pos ('.', AOverlayField.Field2) - 1) then
               fFieldValue2 := StrToFloat (SourceQuery2.FieldByName (sFieldName).AsString)
            else
               exit;}
         if AOverlayField.Precision = -1 then
            sFieldValue := FloatToStrF (AOverlayField.Coefficient * fFieldValue * fDestArea / fSourceArea, ffFixed, 20, 15)
//            sFieldValue := FloatToStrF (AOverlayField.Coefficient * fFieldValue1 / fFieldValue2, ffFixed, 20, 15)
         else
            sFieldValue := FloatToStrF (AOverlayField.Coefficient * fFieldValue * fDestArea / fSourceArea, ffFixed, 20, AOverlayField.Precision);
//            sFieldValue := FloatToStrF (AOverlayField.Coefficient * fFieldValue1 / fFieldValue2, ffFixed, 20, AOverlayField.Precision);
         sFieldValue := ReplaceChars (sFieldValue, DecimalSeparator, '.');
      end
      else
      begin
         sLayer1Name := GetLayerNameFunc (ASourceLayer1);
         sLayer2Name := GetLayerNameFunc (ASourceLayer2);
         sFieldName := Copy (AOverlayField.Field1, Pos ('.', AOverlayField.Field1) + 1, Length (AOverlayField.Field1) - Pos ('.', AOverlayField.Field1));

         if sLayer1Name = Copy (AOverlayField.Field1, 1, Pos ('.', AOverlayField.Field1) - 1) then
            sFieldValue := SourceQuery1.FieldByName (sFieldName).AsString
         else
         begin
            if sLayer2Name = Copy (AOverlayField.Field1, 1, Pos ('.', AOverlayField.Field1) - 1) then
               sFieldValue := SourceQuery2.FieldByName (sFieldName).AsString
            else
               exit;
         end;
      end;
      RESULT := sFieldValue;
   except
   end;
end;

procedure TIDB_PolygonOverlayManager.SetFieldTypeAndSizeByFullName (sFullFieldName: AnsiString; AOverlayField: TOverlayField);
var
   BStructMan: TIDB_StructMan;
   ADataAccessItem: TIDB_Access2DB_Level2;
   sLayerName: AnsiString;
   sFieldName: AnsiString;
begin
   ADataAccessItem := AnIDBMainMan.GetProjectLayerManager (AProject).DataAccessObject;
   BStructMan := TIDB_StructMan.Create (AProject, ADataAccessItem);

   sLayerName := Copy (sFullFieldName, 1, Pos ('.', sFullFieldName) - 1);
   sFieldName := Copy (sFullFieldName, Pos ('.', sFullFieldName) + 1, Length (sFullFieldName) - Pos ('.', sFullFieldName));
   if sLayerName = GetLayerNameFunc (Self.ASourceLayer1) then
      BStructMan.DetectStructure (ADataAccessItem.DMaster, ASourceLayer1)
   else
      if sLayerName = GetLayerNameFunc (Self.ASourceLayer2) then
      begin
         BStructMan.DetectStructure (ADataAccessItem.DMaster, ASourceLayer2);
      end
      else
         exit;
   AOverlayField.FieldType := BStructMan.GetFieldType (sFieldName);
   AOverlayField.FieldSize := BStructMan.GetFieldSize (sFieldName);

   BStructMan.Free;
end;

end.

