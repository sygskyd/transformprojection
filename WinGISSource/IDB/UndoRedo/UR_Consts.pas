unit UR_Consts;
{
Internal database. Ver. II.
Various constants that I use in other parts of Internal Database.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 22-01-2001
}
interface

type
   TDBVersion = (dbv97, dbv2000);

var
   cs_ApplicationName: AnsiString;

const
   cs_ResNameOfEnglishLngFile = 'IDB_ENGLISH_LANGUAGE_FILE'; // Default language file of the IDB (English version).

   cs_IDB_NormalVersion = '1.0.2'; // Current version of the IDB. It is used when WinGIS is loading IDB.dll.
                                      // WinGIS compares this value with the value it needs.

{ Before I'll do an Undo operations I have to put deleting data into the Undo table and Redo one in
  a database. As I can do now not only for items of project but for a layer, I have to put
  in the Undo table also sign what I had put in this table - a layer or a project item.
  These constants are determine that. }
   ci_LayerItem = 1; // TIndex
   ci_SymbolsGroup = 2; // TSGroup
   ci_ProjectItem = 3; // TView
   ci_Layer = 4; // TLayer
   ci_NewObject = 5; // An object was added and now the user wants to delete it as Undo.
   ci_DeletedObject = 6; // An object was restored during Undo and needs to be deleted during Redo.
{ These parameters determines key and parameter name in the WinGIS project registry. }
   cs_ProjectRegistry_IDB_KeyName = '\Project\IDB\GUID';
     // Here I storage the start value of GUID. This value is created during creating a new project
     // and is continually all life time of the project.
   cs_ProjectRegistry_IDB_StartGUID_ParameterName = 'StartGUIDValue';
     // Here I storage the current value of GUID. This value changes during each saving of the project.
   cs_ProjectRegistry_IDB_GUID_ParameterName = 'GUIDValue';
{ These constants I use in SetObjectStyle procedure in TIDB object. }
   ci_DoNotChange = -1;
   ci_UseLayerSettings = -2;
{ I can make operations on records in attribute tables in database files and count some of ProgisID for such
  operations (I ment SQL clause Where ProgisID = first OR ProgisID = second ... etc.) I found out that I can
  count only less than 90 ProgisID in such situations.
  Value is required value minus 1 because of a count starts from zero. }
   ci_PackLength = 89;
{ The size of grid's field less which the field is considered as invisible one. }
   ci_InvisibleFieldBorder = 2;
    { The following symbols are used in the language file for tuning of the user interface. }

   cs_VerisonOfDBFormStorage = '#WG#0001#WG#';
   cs_VerisonOfMonitoringFormStorage = '#WG#0001#WG#';

   cs_LinkToLayerTable = '_WG_LinkToMainProject';
   cs_VerisonOfLinkStorage = '#WG#0001#WG#';

implementation

end.

