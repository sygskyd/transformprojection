unit UR_CallBacksDef;
{
Internal database. Ver. II.
This unit contains declarations of callbacks function of IDB that I use for
interaction between WinGIS and IDB and also some constants that have to be knowon
in WinGIS.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 09-02-2001
}
interface

type
   TUndoType = (utUnInsert,
      utUnDelete,
      utUnChange,
      utUnMove,
      utUnResize,
      utUnTransform,
      utUnknownUNDO);
   TRedoType = (rtInsert,
      rtDelete,
      rtChange,
      rtMove,
      rtResize,
      rtTransform,
      rtUnknownREDO);

   TGetProjectUndoStepsCount = function (AProject: Pointer): integer; stdcall;
   TGetProjectFileSize = function (AProject: Pointer): integer; stdcall;

implementation

end.

