unit UR_MainManager;

interface

uses Classes, Forms, Controls,
   UR_CallbacksDef, UR_UndoMan, UR_RedoMan;

type
   PProjectDef = ^TProjectDef;
   TProjectDef = record
      AProject: Pointer;
      AnUndoManager: TUR_UndoManager;
      ARedoManager: TUR_RedoManager;

      bThereIsNoNecessityToPutDataIntoUndo: Boolean;
//      bIAmGeneratingTheItemsByMyself: Boolean;
   end;

   TUR_MainManager = class
   private
      listProjectDefs: TList;

      AnOldScreenCursor: TCursor;
      procedure ClearData;
      function FindProject (AProject: Pointer): Integer;
      procedure DeleteProject (iProjectPos: Integer); overload;

      procedure SetScreenCursor;
      procedure RestoreScreenCursor;
   public

      constructor Create;
      destructor Free;

      // ***************     FROM WINGIS     ***************
      // Project management.
      function AddProject (AProjectFormHandle: Integer; AProject: Pointer; bProjectFileHasReadOnlyState: Boolean): Boolean;
      procedure DeleteProject (AProject: Pointer); overload;
      // Undo functionality
      procedure ClearUndoData (AProject: Pointer);
      procedure GetReadyToReceiveUndoData (AProject: Pointer);
      function PutDataInUndo (AProject: Pointer; AData: Pointer; iSize: Integer): Boolean;
      procedure SaveUndoData (AProject: Pointer; AnUndoType: TUndoType; bClearRedoData: Boolean);
      function ThereAreDataForUndoing (AProject: Pointer): Boolean;
      function GetNextUndoDescription (AProject: Pointer): AnsiString;
      function GetUndoData (AProject: Pointer; var ABuffer: Pointer; var ASize: Integer; var AnUndoType: TUndoType): Boolean;
      procedure OnChangeUndoStepsCount (AProject: Pointer; iNewUndoStepsCount: integer);

      procedure AddSymbolMappingEntry (AProject: Pointer; sSymLibName, sSymName, sNewSymName: AnsiString);
      function SymbolNameExists (AProject: Pointer; sSymbolLibraryName, sSymbolName: AnsiString): Boolean;
      function GetSymbolActualName (AProject: Pointer; sSymbolLibraryName, sSymbolName: AnsiString): AnsiString;

      // Redo functionality
      procedure ClearRedoData (AProject: Pointer);
      procedure GetReadyToReceiveRedoData (AProject: Pointer);
      function PutDataInRedo (AProject: Pointer; AData: Pointer; iSize: Integer; bNeedToIncObjCount: Boolean): Boolean;
      procedure SaveRedoData (AProject: Pointer; ARedoType: TRedoType);
      function ThereAreDataForRedoing (AProject: Pointer): Boolean;
      function GetNextRedoDescription (AProject: Pointer): AnsiString;
      function GetRedoData (AProject: Pointer; var ABuffer: Pointer; var ASize: Integer; var ARedoType: TRedoType): Boolean;

      procedure Set_ThereIsNoNecessityToPutDataInUndo_Sign (AProject: Pointer; bValue: Boolean);

      // ***************     INSIDE     ***************
   end;

var
   AnURMainMan: TUR_MainManager;
   sLanguageFileName: AnsiString;

implementation

constructor TUR_MainManager.Create;
begin
   listProjectDefs := TList.Create;
end;

destructor TUR_MainManager.Free;
begin
   ClearData;
   listProjectDefs.Free;
end;

procedure TUR_MainManager.ClearData;
begin
   // Delete information about all projects.
   while listProjectDefs.Count > 0 do
      SELF.DeleteProject (0);
end;

procedure TUR_MainManager.SetScreenCursor;
begin
   SELF.AnOldScreenCursor := Screen.Cursor;
   Screen.Cursor := crHourGlass;
   Application.ProcessMessages;
   Application.HandleMessage;
end;

procedure TUR_MainManager.RestoreScreenCursor;
begin
   Screen.Cursor := SELF.AnOldScreenCursor;
end;

function TUR_MainManager.FindProject (AProject: Pointer): Integer;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := -1;
   for iP := 0 to listProjectDefs.Count - 1 do
   begin
      pPD := listProjectDefs[iP];
      if pPD.AProject = AProject then
      begin
         RESULT := iP;
         EXIT;
      end;
   end; // for iP end
end;

function TUR_MainManager.AddProject (AProjectFormHandle: Integer; AProject: Pointer; bProjectFileHasReadOnlyState: Boolean): Boolean;
var
   pPD: PProjectDef;
begin
   RESULT := TRUE; // if this procedures was called for already added project, it must return TRUE.
   if SELF.FindProject (AProject) > -1 then EXIT; // We needn't to add this project. There is it already.
   RESULT := FALSE; // Now we try to add new project and as default result must be set in FALSE state.
   // Otherwise add it.
   SetScreenCursor;
   try
      New (pPD);
      pPD.AProject := AProject;

      // Create UNDO and REDO managers.
      pPD.AnUndoManager := TUR_UndoManager.Create (AProject);
      pPD.ARedoManager := TUR_RedoManager.Create (AProject);
      pPD.bThereIsNoNecessityToPutDataIntoUndo := FALSE;

      // Add the information about the project in the list.
      listProjectDefs.Add (pPD);
      RESULT := TRUE;
   finally
      RestoreScreenCursor;
   end;
end;

{ This procedure deletes project's entry by project's position in the list. }

procedure TUR_MainManager.DeleteProject (iProjectPos: Integer);
var
   pPD: PProjectDef;
   sProjectFileName,
      sTempFileName,
      sReallyDatabaseFileName: AnsiString;
begin
   if (iProjectPos < 0) or (iProjectPos > listProjectDefs.Count - 1) then EXIT;
   SetScreenCursor;
   try
      pPD := listProjectDefs[iProjectPos];

      pPD.AnUndoManager.Free;
      pPD.ARedoManager.Free;
      Dispose (pPD);
      listProjectDefs.Delete (iProjectPos);
   finally
      RestoreScreenCursor;
   end;
end;

{ This procedure deletes project's entry by project's pointer. It finds the project position
  in the list and calls the deleting by the project's position in the list. }

procedure TUR_MainManager.DeleteProject (AProject: Pointer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   SetScreenCursor;
   try
      pPD := listProjectDefs[iP];
      SELF.DeleteProject (iP);
   finally
      RestoreScreenCursor;
   end;
end;

procedure TUR_MainManager.ClearUndoData (AProject: Pointer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.AnUndoManager.ClearUndoData;
end;

procedure TUR_MainManager.GetReadyToReceiveUndoData (AProject: Pointer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.AnUndoManager.GetReadyToReceiveUndoData;
end;

function TUR_MainManager.PutDataInUndo (AProject: Pointer; AData: Pointer; iSize: Integer): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := FALSE;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   if pPD.bThereIsNoNecessityToPutDataIntoUndo then EXIT;
   RESULT := pPD.AnUndoManager.PutDataInUndo (AData, iSize);
end;

procedure TUR_MainManager.SaveUndoData (AProject: Pointer; AnUndoType: TUndoType; bClearRedoData: Boolean);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   if pPD.bThereIsNoNecessityToPutDataIntoUndo then EXIT;
//   if pPD.bIAmGeneratingTheItemsByMyself then EXIT;
   pPD.AnUndoManager.SaveUndoData (AnUndoType);

   // if we put data into UNDO data, all REDO data have to be deleted. Otherwise, we'll be
   // able to have some interesting situations...
   // Usually this flag is set in TRUE state but if if I fill undo data from redo operation, I don't set this flag in TRUE.
   if bClearRedoData then
      pPD.ARedoManager.ClearRedoData;
end;

function TUR_MainManager.ThereAreDataForUndoing (AProject: Pointer): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := FALSE;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   RESULT := pPD.AnUndoManager.ThereAreDataForUndoing;
end;

function TUR_MainManager.GetNextUndoDescription (AProject: Pointer): AnsiString;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := '';
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   RESULT := pPD.AnUndoManager.GetNextUndoDescription;
end;

function TUR_MainManager.GetUndoData (AProject: Pointer; var ABuffer: Pointer; var ASize: Integer; var AnUndoType: TUndoType): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := FALSE;
   ABuffer := nil;
   ASize := 0;
   AnUndoType := utUnknownUNDO;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   RESULT := pPD.AnUndoManager.GetUndoData (ABuffer, ASize, AnUndotype);
end;

procedure TUR_MainManager.AddSymbolMappingEntry (AProject: Pointer; sSymLibName, sSymName, sNewSymName: AnsiString);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.AnUndoManager.AddSymbolMappingEntry (sSymLibName, sSymName, sNewSymName);
end;

function TUR_MainManager.SymbolNameExists (AProject: Pointer; sSymbolLibraryName, sSymbolName: AnsiString): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := FALSE;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   RESULT := pPD.AnUndoManager.SymbolNameExists (sSymbolLibraryName, sSymbolName);
end;

function TUR_MainManager.GetSymbolActualName (AProject: Pointer; sSymbolLibraryName, sSymbolName: AnsiString): AnsiString;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := '';
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project in the list.
   pPD := listProjectDefs[iP];
   RESULT := pPD.AnUndoManager.GetSymbolActualName (sSymbolLibraryName, sSymbolName);
end;

procedure TUR_MainManager.OnChangeUndoStepsCount (AProject: Pointer; iNewUndoStepsCount: integer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then
      exit; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.AnUndoManager.OnChangeUndoStepsCount (iNewUndoStepsCount);
end;

procedure TUR_MainManager.ClearRedoData (AProject: Pointer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.ARedoManager.ClearRedoData;
end;

procedure TUR_MainManager.GetReadyToReceiveRedoData (AProject: Pointer);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.ARedoManager.GetReadyToReceiveRedoData;
end;

function TUR_MainManager.PutDataInRedo (AProject: Pointer; AData: Pointer; iSize: Integer; bNeedToIncObjCount: Boolean): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := FALSE;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   RESULT := pPD.ARedoManager.PutDataInRedo (AData, iSize, bNeedToIncObjCount);
end;

procedure TUR_MainManager.SaveRedoData (AProject: Pointer; ARedoType: TRedoType);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.ARedoManager.SaveRedoData (ARedoType);
end;

function TUR_MainManager.ThereAreDataForRedoing (AProject: Pointer): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := FALSE;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   RESULT := pPD.ARedoManager.ThereAreDataForRedoing;
end;

function TUR_MainManager.GetNextRedoDescription (AProject: Pointer): AnsiString;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := '';
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   RESULT := pPD.ARedoManager.GetNextRedoDescription;
end;

function TUR_MainManager.GetRedoData (AProject: Pointer; var ABuffer: Pointer; var ASize: Integer; var ARedoType: TRedoType): Boolean;
var
   iP: Integer;
   pPD: PProjectDef;
begin
   RESULT := FALSE;
   ABuffer := nil;
   ASize := 0;
   ARedoType := rtUnknownREDO;
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   RESULT := pPD.ARedoManager.GetRedoData (ABuffer, ASize, ARedoType);
end;

procedure TUR_MainManager.Set_ThereIsNoNecessityToPutDataInUndo_Sign (AProject: Pointer; bValue: Boolean);
var
   iP: Integer;
   pPD: PProjectDef;
begin
   iP := SELF.FindProject (AProject);
   if iP < 0 then EXIT; // There isn't such project.
   pPD := listProjectDefs[iP];
   pPD.bThereIsNoNecessityToPutDataIntoUndo := bValue;
end;

end.

