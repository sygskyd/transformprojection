library UndoRedo;
{
UndoRedo . Ver. I.

Author: Fedor Cadmensky, Progis Russia, Moscow.
Date: 08-09-2002
}
uses
   SHAREMEM,
   Forms,
   Windows,
   SysUtils,
   Classes,
   UR_UndoMan in 'UR_UndoMan.pas',
   UR_LanguageManager in 'UR_LanguageManager.pas',
   UR_Mainmanager in 'UR_Mainmanager.pas',
   UR_RedoMan in 'UR_RedoMan.pas',
   UR_CallBacksDef in 'UR_CallBacksDef.pas',
   UR_Consts in 'UR_Consts.pas';

var
   AnOldApplication: TApplication;
   AnOldScreen: TScreen;

{$R URVersionResource.res}  // This resource contains data of IDB version.
{$R UR_EnglishLangFile.res} // This resource contains english language file.


//********     Initialization and finalization routnes of the DLL.  **********//

function InitLibrary (AnApplication: Integer; ALanguageFileExtension: PChar): Boolean; stdcall;
begin
   RESULT := false;

     // Save the current TApplication and TScreen.
   AnOldApplication := Application;
   Application := TApplication (AnApplication);
     // Try to create main UR manager. It it fails, we'll not be able to work with the IDB.
     // AWinGISMainForm variable will be used as AOwner variable for creating some of VCL components.
   try
      AnURMainMan := TUR_MainManager.Create;
   except
      EXIT;
   end;

   sLanguageFileName := ExtractFilePath (Application.ExeName) + 'IDB.' + ALanguageFileExtension;
   if not FileExists (sLanguageFileName) then
      sLanguageFileName := ExtractFilePath (Application.ExeName) + 'IDB.044';

     // Create the Language manager.
   ALangManager := TLanguageManager.Create (sLanguageFileName);
   cs_ApplicationName := ALangManager.GetValue (499);
   RESULT := true;
end;

procedure FinLibrary; stdcall;
begin
     // Delete the language manager.
   try
      if ALangManager <> nil then
         ALangManager.Free;
   except
   end;
     // Delete the IDB main manager.
   try
      if AnURMainMan <> nil then
         AnURMainMan.Free;
   except
   end;
     // Restore the old TScreen and TApplication.
   if AnOldApplication <> nil then
      Application := AnOldApplication;
   if AnOldScreen <> nil then
      Screen := AnOldScreen;
end;

function GetVersion: PChar; stdcall;
begin
   GetMem (RESULT, Length (cs_IDB_NormalVersion) + 1);
   StrPCopy (RESULT, cs_IDB_NormalVersion);
end;

//****************            IDB core routines.           *********************
{ This procedure adds a project in the project list of the IDB manager. }

function AddProject (AProjectFormHandle: Integer; AProject: Pointer; bProjectFileHasReadOnlyState: Boolean): Boolean; stdcall;
begin
   RESULT := false;
   if AnURMainMan <> nil then
   begin
      RESULT := AnURMainMan.AddProject (AProjectFormHandle, AProject, bProjectFileHasReadOnlyState);
   end;
end;

{ This procedure deletes a project from the project list of the IDB manager. }

procedure DeleteProject (AProject: Pointer); stdcall;
begin
   if AnURMainMan <> nil then
      AnURMainMan.DeleteProject (AProject);
end;

procedure GetProjectUndoStepsCountCallback (AFunction: TGetProjectUndoStepsCount); stdcall;
begin
//     GetProjectUndoStepsCountFunc := AFunction;
end;

procedure GetProjectFileSizeCallback (AFunction: TGetProjectFileSize); stdcall;
begin
//     GetProjectFileSizeFunc := AFunction;
end;

//*****************************   UNDO routines.   *****************************

procedure ClearUndoData (AProject: Pointer); stdcall;
begin
   if AnURMainMan <> nil then
      AnURMainMan.ClearUndoData (AProject);
end;

procedure GetReadyToReceiveUndoData (AProject: Pointer); stdcall;
begin
   if AnURMainMan <> nil then
      AnURMainMan.GetReadyToReceiveUndoData (AProject);
end;

procedure Set_ThereIsNoNecessityToPutDataInUndo_Sign (AProject: Pointer; bValue: Boolean); stdcall;
begin
   if AnURMainMan <> nil then
      AnURMainMan.Set_ThereIsNoNecessityToPutDataInUndo_Sign (AProject, bValue);
end;

function PutDataInUndo (AProject: Pointer; AData: Pointer; iSize: Integer): Boolean; stdcall;
begin
   RESULT := false;
   if AnURMainMan <> nil then
      RESULT := AnURMainMan.PutDataInUndo (Aproject, AData, iSize);
end;

procedure SaveUndoData (AProject: Pointer; AnUndoType: TUndoType; bClearRedoData: Boolean); stdcall;
begin
   if AnURMainMan <> nil then
      AnURMainMan.SaveUndoData (AProject, AnUndoType, bClearRedoData);
end;

function ThereAreDataForUndoing (AProject: Pointer): Boolean; stdcall;
begin
   RESULT := false;
   if AnURMainMan <> nil then
      RESULT := AnURMainMan.ThereAreDataForUndoing (AProject);
end;

function GetNextUndoDescription (AProject: Pointer): PChar; stdcall;
var
   sData: AnsiString;
begin
   sData := '';
   if AnURMainMan <> nil then
      sData := AnURMainMan.GetNextUndoDescription (AProject);
   GetMem (RESULT, Length (sData) + 1);
   StrPCopy (RESULT, sData);
end;

function GetUndoData (AProject: Pointer; var ABuffer: Pointer; var ASize: Integer; var AnUndoType: TUndoType): Boolean; stdcall;
begin
   RESULT := false;
   ABuffer := nil;
   ASize := 0;
   AnUndoType := utUnknownUNDO;
   if AnURMainMan <> nil then
      RESULT := AnURMainMan.GetUndoData (AProject, ABuffer, ASize, AnUndoType);
end;

procedure AddSymbolMappingEntry (AProject: Pointer; pcSymLibName, pcSymName, pcNewSymName: PChar); stdcall;
begin
   if AnURMainMan <> nil then
      AnURMainMan.AddSymbolMappingEntry (AProject, pcSymLibName, pcSymName, pcNewSymName);
end;

function SymbolNameExists (AProject: Pointer; pcSymbolLibraryName, pcSymbolName: PChar): Boolean; stdcall;
begin
   RESULT := false;
   if AnURMainMan <> nil then
      RESULT := AnURMainMan.SymbolNameExists (AProject, pcSymbolLibraryName, pcSymbolName);
end;

function GetSymbolActualName (AProject: Pointer; pcSymbolLibraryName, pcSymbolName: PChar): PChar; stdcall;
var
   sData: AnsiString;
begin
   sData := '';
   if AnURMainMan <> nil then
      sData := AnURMainMan.GetSymbolActualName (AProject, pcSymbolLibraryName, pcSymbolName);
   GetMem (RESULT, Length (sData) + 1);
   StrPCopy (RESULT, sData);
end;

//*****************************   REDO routines.   *****************************

procedure ClearRedoData (AProject: Pointer); stdcall;
begin
   if AnURMainMan <> nil then
      AnURMainMan.ClearRedoData (AProject);
end;

procedure GetReadyToReceiveRedoData (AProject: Pointer); stdcall;
begin
   if AnURMainMan <> nil then
      AnURMainMan.GetReadyToReceiveRedoData (AProject);
end;

function PutDataInRedo (AProject: Pointer; AData: Pointer; iSize: Integer; bNeedToIncObjCount: Boolean = true): Boolean; stdcall;
begin
   RESULT := false;
   if AnURMainMan <> nil then
      RESULT := AnURMainMan.PutDataInRedo (AProject, AData, iSize, bNeedToIncObjCount);
end;

procedure SaveRedoData (AProject: Pointer; AnRedoType: TRedoType); stdcall;
begin
   if AnURMainMan <> nil then
      AnURMainMan.SaveRedoData (AProject, AnRedoType);
end;

procedure OnChangeUndoStepsCount (AProject: Pointer; iNewUndoStepsCount: integer); stdcall;
begin
   if AnURMainMan <> nil then
      AnURMainMan.OnChangeUndoStepsCount (AProject, iNewUndoStepsCount);
end;

function ThereAreDataForRedoing (AProject: Pointer): Boolean; stdcall;
begin
   RESULT := false;
   if AnURMainMan <> nil then
      RESULT := AnURMainMan.ThereAreDataForRedoing (AProject);
end;

function GetNextRedoDescription (AProject: Pointer): PChar; stdcall;
var
   sData: AnsiString;
begin
   sData := '';
   if AnURMainMan <> nil then
      sData := AnURMainMan.GetNextRedoDescription (AProject);
   GetMem (RESULT, Length (sData) + 1);
   StrPCopy (RESULT, sData);
end;

function GetRedoData (AProject: Pointer; var ABuffer: Pointer; var ASize: Integer; var ARedoType: TRedoType): Boolean; stdcall;
begin
   RESULT := false;
   ABuffer := nil;
   ASize := 0;
   ARedoType := rtUnknownREDO;
   if AnURMainMan <> nil then
      RESULT := AnURMainMan.GetRedoData (AProject, ABuffer, ASize, ARedoType);
end;

exports
   //********     Initialization and finalization routnes of the DLL.  **********//
   InitLibrary,
   FinLibrary,

   GetVersion,
   //****************            IDB core routines.           *******************//
   AddProject,
   DeleteProject,
   GetProjectUndoStepsCountCallback,
   GetProjectFileSizeCallback,

   ClearUndoData,
   GetReadyToReceiveUndoData,
   Set_ThereIsNoNecessityToPutDataInUndo_Sign,

   PutDataInUndo,
   SaveUndoData,
   ThereAreDataForUndoing,
   GetNextUndoDescription,
   GetUndoData,

   ClearRedoData,
   GetReadyToReceiveRedoData,
   PutDataInRedo,
   SaveRedoData,
   ThereAreDataForRedoing,
   GetNextRedoDescription,
   GetRedoData,
   OnChangeUndoStepsCount,
   AddSymbolMappingEntry,
   SymbolNameExists,
   GetSymbolActualName;

begin

end.

