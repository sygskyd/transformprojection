unit IDB_C_StringGrid;
{
Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 12.07.2000

This component is used instead of standard TStringGrid in forms that display
database data.
Cause of using this component instead of standard is that the standard TStringGrid has
TInplaceEditor in Protected section in declare class section. I need to send messages
to TInplaceEditor during user's work.
}

interface

Uses Messages, Classes, Windows,
     IDB_Grids;

Type
    TDeselectCellEvent = Procedure(Sender: TObject; ACol, ARow: Integer; Var CanDeSelect: Boolean) Of Object;

    TIDB_StringGrid = Class(TIDB_CustomStringGrid)
        Private
           bAllowMovingFromToTheFirstLine: Boolean;
           bAllowMovingFromToTheLastLine: Boolean;
           FOnDeSelectCell: TDeselectCellEvent;
        Protected
           procedure DblClick; override;
           function EndRowDrag(var Origin, Destination: Integer; const MousePt: TPoint): Boolean; Override;
        Public
           Constructor Create(AOwner: TComponent); Override;

           Procedure ActivateEditor(iColumn, iRow: Integer);
           Procedure HideEditor;
           Procedure DeleteRow(ARow: Longint); Override;
           Procedure ResizeColumn(ACol: Integer);
           Function SelectCell(ACol, ARow: Longint): Boolean; Override;

           Property InplaceEditor;
           Property HorzSplitterPosition: Integer Read iHorzSplitterPosition;
        Published
           Property AllowMovingFromToTheFirstLine: Boolean Read bAllowMovingFromToTheFirstLine Write bAllowMovingFromToTheFirstLine;
           Property AllowMovingFromToTheLastLine: Boolean Read bAllowMovingFromToTheLastLine Write bAllowMovingFromToTheLastLine;
           Property OnDeSelectCell: TDeselectCellEvent Read FOnDeSelectCell Write FOnDeSelectCell;
        End;

procedure Register;

implementation

Uses Controls, Grids;

procedure Register;
begin
     RegisterComponents('Progis IDB', [TIDB_StringGrid]);
end;

Constructor TIDB_StringGrid.Create(AOwner: TComponent);
Begin
     Inherited Create(AOwner);
     SELF.bAllowMovingFromToTheFirstLine := TRUE;
     SELF.bAllowMovingFromToTheLastLine := TRUE;
End;

Procedure TIDB_StringGrid.ActivateEditor(iColumn, iRow: Integer);
Var
   ARect: TRect;
   iX, iY: Integer;
Begin
     // Imitate left-mouse click on the first line of string grid.
     ARect := SELF.CellRect(iColumn, iRow);
     iX := (ARect.Right - ARect.Left) DIV 2;
     iY := (ARect.Bottom - ARect.Top) DIV 2;
     // Then show inplace editor. I simulate left mouse click.
     SELF.MouseDown(mbLeft, [ssLeft], iX, iY);
     SELF.MouseUp(mbLeft, [ssLeft], iX, iY);
End;

Procedure TIDB_StringGrid.HideEditor;
Begin
     Inherited HideEditor;
End;

Procedure TIDB_StringGrid.DeleteRow(ARow: Longint);
Begin
     Inherited DeleteRow(ARow);
End;

Procedure TIDB_StringGrid.ResizeColumn(ACol: Integer);
Var
   iI: Integer;
   iMaxLength,
   iLength: Integer;
Begin
     If ACol > -1 Then Begin
        iMaxLength := -1;
        For iI:=0 To SELF.RowCount-1 Do Begin
            iLength := SELF.Canvas.TextWidth(SELF.Cells[ACol, iI]);
            If iLength > iMaxLength Then
               iMaxLength := iLength;
            End; // For iI end
        If iMaxLength > -1 Then
           SELF.ColWidths[ACol] := iMaxLength + 10;
        End;
End;

procedure TIDB_StringGrid.DblClick;
Begin
     inherited DblClick;
     If SELF.iHorzSplitterPosition > -1 Then
        SELF.ResizeColumn(iHorzSplitterPosition);
End;

function TIDB_StringGrid.EndRowDrag(var Origin, Destination: Integer; const MousePt: TPoint): Boolean;
begin
     If ((Origin = 0) OR (Destination = 0)) AND (NOT bAllowMovingFromToTheFirstLine) Then
        RESULT := FALSE
     Else
     If ((Origin = SELF.RowCount-1) OR (Destination = SELF.RowCount-1)) AND (NOT bAllowMovingFromToTheLastLine) Then
        RESULT := FALSE
     Else
     If Origin = SELF.FixedRows Then
        RESULT := FALSE
     Else
{
 The first unfixed line index equals FixedRow property. 
}
     If Destination = SELF.FixedRows Then Begin
        Destination := SELF.FixedRows + 1;
        RESULT := TRUE;
        End;
end;

Function TIDB_StringGrid.SelectCell(ACol, ARow: Longint): Boolean;
Begin
     RESULT := TRUE;
     If Assigned(FOnDeSelectCell) Then FOnDeSelectCell(SELF, SELF.Selection.Left, SELF.Selection.Top, RESULT);
     If RESULT Then
        RESULT := Inherited SelectCell(ACol, ARow);
End;

end.

