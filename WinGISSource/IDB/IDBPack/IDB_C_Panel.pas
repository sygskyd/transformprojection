unit IDB_C_Panel;
{
Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 19.12.2001

This component is used instead of standard TPanel. The only reason is the following:
TPanel has Canvas property declared as Protected. I want to have an access to Canvas
of TPanel (for example, to know length of text on it).
}
interface

Uses ExtCtrls, Graphics, Controls;

Type
    TIDB_Panel = Class(TPanel)
        Private
        Public
           Function GetCanvas: TCanvas;
        End;

procedure Register;

implementation

Uses Classes;

procedure Register;
begin
     RegisterComponents('Progis IDB', [TIDB_Panel]);
end;

Function TIDB_Panel.GetCanvas: TCanvas;
Begin
     RESULT := SELF.Canvas;
End;

end.
