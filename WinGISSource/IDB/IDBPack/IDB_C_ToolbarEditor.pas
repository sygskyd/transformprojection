unit IDB_C_ToolbarEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DesignIntf, DesignEditors,
  IDB_C_Toolbar, ExtCtrls, StdCtrls;

type
  TEditorForm = class(TForm)
    Panel1: TPanel;
    BottomPanel: TPanel;
    OKButton: TButton;
    RadioGroup1: TRadioGroup;
    CancelButton: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TIDB_ToolbarButtonsEditor = class(TComponentEditor)
  private
    AForm: TEditorForm;
  public
    function GetVerbCount: Integer; override;
    function GetVerb(AIndex: Integer): AnsiString; override;
    procedure ExecuteVerb(AIndex: Integer); override;
  end;

implementation
{$R *.DFM}

function TIDB_ToolbarButtonsEditor.GetVerbCount: Integer;
begin
  RESULT := inherited GetVerbCount + 1;
end;

function TIDB_ToolbarButtonsEditor.GetVerb(AIndex: Integer): AnsiString;
begin
  if AIndex = GetVerbCount - 1 then
    RESULT := 'Add button'
  else
    RESULT := inherited GetVerb(AIndex);
end;

procedure TIDB_ToolbarButtonsEditor.ExecuteVerb(AIndex: Integer);
begin
  if AIndex = GetVerbCount - 1 then
  begin
    AForm := TEditorForm.Create(Application);
    if AForm.ShowModal = mrOK then
    begin
      if AForm.RadioGroup1.ItemIndex = 1 then
        TIDB_Toolbar(Component).AddButton(tbSeparator)
      else
        TIDB_Toolbar(Component).AddButton(tbButton);
      ShowMessage(IntToStr(TIDB_Toolbar(Component).ButtonsCount));
    end;
    AForm.Free;
  end
  else
    inherited ExecuteVerb(AIndex);
end;

end.

