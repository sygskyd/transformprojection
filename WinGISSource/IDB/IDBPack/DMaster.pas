unit DMaster;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DesignIntf, DesignEditors, DB, DUtils, ADOBase, DEditors;

type
  TDMasterConnectionProperty = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure Edit; override;

  end;

type
  TDMasterEditor = class(TComponentEditor)
    function GetVerbCount: Integer; override;
    function GetVerb(Index: Integer): string; override;
    procedure ExecuteVerb(Index: Integer); override;

  end;

type
  TSourceType = (stUnknown,
    stAccess,
    stDbase,
    stExcel,
    stFoxPro,
    stParadox,
    stText,
    stVisualFoxPro,
    stOracle,
    stMsSQL,
    stSybase,
    stInformix,
    stIngres,
    stDB2);
  TConnectMode = (cmNone, cmSystem, cmFile, cmServer);
  TOdbcDialogFilter = function(const DriverName: string): string of object;

  TDMaster = class(TComponent)
  private
    { Private declarations }
    FConnect: string;
    FSourceType: TSourceType;
    FDataSets: TList;
    FConnection: TDConnection;
    FMachine: string;

    // Handle Properties
    function GetConnected: Boolean;
    procedure SetConnected(Value: Boolean);
    function GetSourceType: TSourceType;
    function GetData: TDConnection;
    function GetDataSet(Index: Integer): TDataSet;
    function GetFileDSN: string;
    procedure SetFileDSN(const Value: string);
    function GetSysDSN: string;
    procedure SetSysDSN(const Value: string);
    function GetDriver: string;
    procedure SetDriver(const Value: string);
    function GetServer: string;
    procedure SetServer(const Value: string);
    function GetDatabase: string;
    procedure SetDatabase(const Value: string);
    function GetUsername: string;
    procedure SetUsername(const Value: string);
    function GetPassword: string;
    procedure SetPassword(const Value: string);
    function GetProvider: string;
    procedure SetProvider(const Value: string);
    function GetConnectMode: TConnectMode;
    procedure SetConnect(const Value: string);
    function GetBaseDir: string;
    function GetSystemDir: string;
    procedure SetMachine(const Value: string);
// ++ Cadmensky
    function GetDataSource: string;
    procedure SetDataSource(const Value: string);
// -- Cadmensky

  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;

    procedure Assign(Source: TPersistent); override;
    function Editor(StartPage: Byte = 0): string; virtual;

    // Child DataSets
    procedure InsertDataSet(DataSet: TDataSet);
    procedure RemoveDataSet(DataSet: TDataSet);

    // Load UDL file
    function LoadUDLFile(const FileName: string; UserName: string = ''; Password: string = ''): string;
    procedure UDLEditor(const FileName: string);

    property CentralData: TDConnection read GetData;
    property FileName: string read GetFileDSN write SetFileDSN;
    property System: string read GetSysDSN write SetSysDSN;
    property Driver: string read GetDriver write SetDriver;
    property Server: string read GetServer write SetServer;
    property Database: string read GetDatabase write SetDatabase;
    property DataSource: string read GetDataSource write SetDataSource;
    property Password: string read GetPassword write SetPassword;
    property Username: string read GetUsername write SetUsername;
    property Provider: string read GetProvider write SetProvider;
    property Mode: TConnectMode read GetConnectMode;
    property SystemDir: string read GetSystemDir;
    property BaseDir: string read GetBaseDir;
    property SourceType: TSourceType read GetSourceType;
    property DataSets[Index: Integer]: TDataSet read GetDataSet;

  published
    { Published declarations }
    property Connection: string read FConnect write SetConnect;
    property Connected: Boolean read GetConnected write SetConnected;
    property MachineName: string read FMachine write SetMachine;

  end;

procedure Register;

implementation

uses DDB;

{$R *.dcr}

procedure Register;
begin
  RegisterPropertyEditor(TypeInfo(string), TDMaster, 'Connection', TDMasterConnectionProperty);
  RegisterComponentEditor(TDMaster, TDMasterEditor);
end;

{******************************************************************************}
{***                         Propery Editors                                ***}
{******************************************************************************}

function TDMasterConnectionProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paDialog];
end;

procedure TDMasterConnectionProperty.Edit;
begin
  SetStrValue(TDMaster(GetComponent(0)).Editor);
  Modified;
end;

function TDMasterEditor.GetVerbCount: Integer;
begin
  Result := 1;
end;

function TDMasterEditor.GetVerb(Index: Integer): string;
begin
  Result := 'Editor';
end;

procedure TDMasterEditor.ExecuteVerb(Index: Integer);
begin
  TDMaster(Component).Connection := TDMaster(Component).Editor;
  Designer.Modified;
end;

{******************************************************************************}
{***                         Central Component                              ***}
{******************************************************************************}

constructor TDMaster.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FDataSets := TList.Create;
  FSourceType := stUnknown;
  FConnect := '';
  FConnection := TDConnection.Create(False);
end;

destructor TDMaster.Destroy;
begin
  FDataSets.Free;
  FConnection.Free;
  FConnection := nil;
  inherited Destroy;
end;

procedure TDMaster.Assign(Source: TPersistent);
begin
  if Source is TDMaster then
  begin
    Connection := TDMaster(Source).Connection;
  end
  else
    inherited Assign(Source);
end;

procedure TDMaster.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) and (AComponent is TDataSet) then
    RemoveDataSet(TDataSet(AComponent));
end;

{******************************************************************************}

function TDMaster.Editor(StartPage: Byte = 0): string;
var
  oEditor: TDConnectionEditor;
begin
  oEditor := TDConnectionEditor.Create(Self);
  try
    begin
      if StartPage = 1 then
        oEditor.Connection := ''
      else
        oEditor.Connection := FConnect;
      if oEditor.Execute then
      begin
        Connection := oEditor.Connection;
        FConnection.DefaultTable := oEditor.DefaultTable;
      end
      else
        FConnection.DefaultTable := '';
    end;
  finally
    oEditor.Free;
  end;
  FConnection.Connection := FConnect;
  Result := FConnect;
end;

function TDMaster.GetSourceType: TSourceType;
var
  sTmp: string;
begin
  sTmp := AnsiUpperCase(FConnect);
  Result := stUnknown;
  if IsSub('ACCESS', sTmp) then
    Result := stAccess;
  if IsSub('DBASE', sTmp) then
    Result := stDbase;
  if IsSub('EXCEL', sTmp) then
    Result := stExcel;
  if IsSub('FOXPRO', sTmp) then
    Result := stFoxPro;
  if IsSub('PARADOX', sTmp) then
    Result := stParadox;
  if IsSub('TEXT', sTmp) then
    Result := stText;
  if IsSub('VISUAL FOXPRO', sTmp) then
    Result := stVisualFoxPro;
  if IsSub('ORACLE', sTmp) then
    Result := stOracle;
  if IsSub('SQL Server', sTmp) then
    Result := stMsSQL;
  if IsSub('SYBASE', sTmp) then
    Result := stSybase;
  if IsSub('INFORMIX', sTmp) then
    Result := stInformix;
  if IsSub('INGRES', sTmp) then
    Result := stIngres;
  if IsSub('DB2', sTmp) then
    Result := stDB2;
end;

function TDMaster.GetData: TDConnection;
begin
  if FConnection.Connection <> FConnect then
    FConnection.Connection := FConnect;
  Result := FConnection;
end;

function TDMaster.GetConnected: Boolean;
begin
  Result := FConnection.Active;
end;

procedure TDMaster.SetConnected(Value: Boolean);
var
  i: Integer;
begin
  if not Value then
  begin
    for i := 0 to FDataSets.Count - 1 do
    begin
      try
{++ Ivanoff}
        if FDataSets.Items[i] <> nil then
{-- Ivanoff}
          TDataSet(FDataSets.Items[i]).Close;
      except
      end;
    end;
  end;
  if FConnection.Connection <> FConnect then
    FConnection.Connection := FConnect;
  FConnection.Active := Value;
end;

function TDMaster.GetDataSet(Index: Integer): TDataSet;
begin
  Result := nil;
  if (Index >= 0) and (Index < FDataSets.Count) then
  begin
    Result := TDataSet(FDataSets.Items[Index]);
  end;
end;

function TDMaster.GetFileDSN: string;
begin
  Result := GetADOPart(FConnect, cnsADOFileDSN);
end;

procedure TDMaster.SetFileDSN(const Value: string);
begin
  FConnect := SetADOPart(FConnect, cnsADOFileDSN, Value);
end;

function TDMaster.GetSysDSN: string;
begin
  Result := GetADOPart(FConnect, cnsADOSystemDSN);
end;

procedure TDMaster.SetSysDSN(const Value: string);
begin
  FConnect := SetADOPart(FConnect, cnsADOSystemDSN, Value);
end;

function TDMaster.GetDriver: string;
begin
  Result := GetADOPart(FConnect, cnsADODriver);
end;

procedure TDMaster.SetDriver(const Value: string);
begin
  FConnect := SetADOPart(FConnect, cnsADODriver, Value);
end;

function TDMaster.GetServer: string;
begin
  Result := GetADOPart(FConnect, cnsADOServer);
end;

procedure TDMaster.SetServer(const Value: string);
begin
  FConnect := SetADOPart(FConnect, cnsADOServer, Value);
end;

function TDMaster.GetDatabase: string;
begin
// ++ Cadmensky
//     Result := GetADOPart(FConnect, cnsADOServer);
  Result := GetADOPart(FConnect, cnsADODatabase);
// -- Cadmensky
end;

procedure TDMaster.SetDatabase(const Value: string);
begin
  FConnect := SetADOPart(FConnect, cnsADODatabase, Value);
end;

// ++ Cadmensky

function TDMaster.GetDataSource: string;
begin
  Result := GetADOPart(FConnect, cnsADODataSource);
end;

procedure TDMaster.SetDataSource(const Value: string);
begin
  FConnect := SetADOPart(FConnect, cnsADODataSource, Value);
end;
// -- Cadmensky

function TDMaster.GetUsername: string;
begin
  Result := GetADOPart(FConnect, cnsADOUsername);
end;

procedure TDMaster.SetUsername(const Value: string);
begin
  FConnect := SetADOPart(FConnect, cnsADOUsername, Value);
end;

function TDMaster.GetPassword: string;
begin
  Result := GetADOPart(FConnect, cnsADOPassword);
end;

procedure TDMaster.SetPassword(const Value: string);
begin
  FConnect := SetADOPart(FConnect, cnsADOPassword, Value);
end;

function TDMaster.GetProvider: string;
begin
  Result := GetADOPart(FConnect, cnsADOProvider);
end;

procedure TDMaster.SetProvider(const Value: string);
begin
  if Value = '' then
    FConnect := SetADOPart(FConnect, cnsADOProvider, cnsDefProvider)
  else
    FConnect := SetADOPart(FConnect, cnsADOProvider, Value);
end;

function TDMaster.GetConnectMode: TConnectMode;
begin
  Result := cmNone;
  if System <> '' then
    Result := cmSystem;
  if Driver <> '' then
    Result := cmFile;
  if Server <> '' then
    Result := cmServer;
end;

procedure TDMaster.SetConnect(const Value: string);
var
  i: Integer;
begin
  if FConnect <> Value then
  begin
    FConnect := Value;
    for i := 0 to FDataSets.Count - 1 do
    begin
      if Assigned(FDataSets.Items[i]) then
        TDADODataSet(FDataSets.Items[i]).Connection := FConnect;
    end;
    FConnection.Close;
    FConnection.Connection := FConnect;
  end;
end;

function TDMaster.GetBaseDir: string;
begin
  Result := IIF((csDesigning in ComponentState), GetWorkDir, GetExeDir);
end;

function TDMaster.GetSystemDir: string;
begin
  Result := BaseDir + 'System\';
end;

procedure TDMaster.InsertDataSet(DataSet: TDataSet);
begin
  if FDataSets.IndexOf(DataSet) = -1 then
    FDataSets.Add(DataSet);
end;

procedure TDMaster.RemoveDataSet(DataSet: TDataSet);
var
  iPos: Integer;
begin
  iPos := FDataSets.IndexOf(DataSet);
  if iPos > -1 then
    FDataSets.Remove(DataSet);
end;

function TDMaster.LoadUDLFile(const FileName: string; UserName: string = ''; Password: string = ''): string;
begin
  Result := FConnection.LoadUDLFile(FileName, UserName, Password);
end;

procedure TDMaster.UDLEditor(const FileName: string);
begin
  FConnection.UDLEditor(FileName);
end;

procedure TDMaster.SetMachine(const Value: string);
begin
  if FMachine <> Value then
  begin
    FMachine := Value;
    FConnection.MachineName := FMachine;
  end;
end;

end.

