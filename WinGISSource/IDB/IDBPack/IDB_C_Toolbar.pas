unit IDB_C_Toolbar;
{
This component was created to replace a TToolbar of Delphi VCL.
Standard components has some understandable features that linked to
repositionng of toolbar buttons. So, we decided to create our own
component to avoid some problems than will be able to appear in future.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 20.07.2001
}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, DesignIntf, Buttons;

type
  TButtonType = (tbButton, tbSeparator);

  PrecButtonDesc = ^TrecButtonDesc;
  TrecButtonDesc = Record
      AType: TButtonType;
      AButton: TSpeedButton;
      End; 

  TIDB_Toolbar = class(TPanel)
  private
    { Private declarations }
    bButtonsHaveToBeFlat: Boolean;
    iButtonsWidth: Integer;
    iButtonsHeight: Integer;
    iHorzSpacing: Integer;
    iVertSpacing: Integer;
    iSeparatorWidth: Integer;
    iRowCount: Integer;
    AButtonsList: TList;
    bMouseIsHoldedDown: Boolean;

    Function GetButton(AIndex: Integer): TSpeedButton;
    Procedure SetFlatState(AValue: Boolean);
    Function GetButtonsCount: Integer;
  protected
    { Protected declarations }
    Procedure Paint; Override;
    Procedure Resize; Override;
    Procedure WriteButtons(AStream: TStream); Virtual;
    Procedure ReadButtons(AStream: TStream); Virtual;
    Procedure DefineProperties(AFiler: TFiler); Override;
  public
    { Public declarations }
    Constructor Create(AOwner: TComponent); override;
    Destructor Destroy; override;

    Function AddButton(AType: TButtonType): TSpeedButton; Overload;
    Procedure AddButton(AButton: TSpeedButton); Overload;
    Procedure RepositionButtons;

    Property Buttons[AIndex: Integer]: TSpeedButton Read GetButton;
    Property ButtonsCount: Integer Read GetButtonsCount;
    Property RowCount: Integer Read iRowCount;
  published
    { Published declarations }
    Property Flat: Boolean Read bButtonsHaveToBeFlat Write SetFlatState;
    Property ButtonsWidth: Integer Read iButtonsWidth Write iButtonsWidth;
    Property ButtonsHeight: Integer Read iButtonsHeight Write iButtonsHeight;
    Property ButtonsHorzSpacing: Integer Read iHorzSpacing Write iHorzSpacing;
    Property ButtonsSeparatorWidth: Integer Read iSeparatorWidth Write iSeparatorWidth;
    Property ButtonsVertSpacing: Integer Read iVertSpacing Write iVertSpacing;
  end;

procedure Register;

implementation
{$R IDB_Buttons.res}
Uses
     IDB_C_ToolbarEditor;

procedure Register;
begin
     RegisterComponents('Progis IDB', [TIDB_Toolbar]);

     RegisterComponentEditor(TIDB_Toolbar, TIDB_ToolbarButtonsEditor);
end;

Constructor TIDB_Toolbar.Create(AOwner: TComponent);
Begin
     Inherited Create(AOwner);
     SELF.AButtonsList := TList.Create;

     SELF.bMouseIsHoldedDown := FALSE;

     SELF.Caption := ' ';
     SELF.Align := alTop;
     SELF.FullRepaint := FALSE;

     SELF.BevelInner := bvLowered;
     SELF.BevelOuter := bvRaised;

     SELF.Height := 30;

     SELF.bButtonsHaveToBeFlat := TRUE;
     SELF.iButtonsWidth := 25;
     SELF.iButtonsHeight := 25;
     SELF.iSeparatorWidth := 5;
     SELF.iHorzSpacing := 0;
     SELF.iVertSpacing := 0;

     SELF.iRowCount := 1;

     If AOwner IS TForm Then
        TForm(AOwner).Invalidate;
End;

Destructor TIDB_Toolbar.Destroy;
var i: integer;
    pB: PrecButtonDesc;
Begin
// ++ Cadmensky
     for i := 0 to AButtonsList.Count - 1 do
     begin
        pB := AButtonsList[i];
        FreeMem (pB);
     end;   
// -- Cadmensky
     AButtonsList.Free;
     Inherited Destroy;
End;

Procedure TIDB_Toolbar.SetFlatState(AValue: Boolean);
Var
   AButton: TSpeedButton;
   iI: Integer;
Begin
     bButtonsHaveToBeFlat := AValue;
     For iI:=0 To AButtonsList.Count-1 Do Begin
         AButton := aButtonsList[iI];
         AButton.Flat := AValue;
         End;
     SELF.Invalidate;
End;

Function TIDB_Toolbar.GetButton(AIndex: Integer): TSpeedButton;
Var
   pB: PrecButtonDesc;
   iI, iP: Integer;
Begin
     If (AIndex < 0) OR (AIndex >= AButtonsList.Count) Then Begin
        RESULT := NIL;
        EXIT;
        End
     Else Begin
        RESULT := NIL;     
        iP := AIndex;
        For iI:=0 To AButtonsList.Count-1 Do Begin
            pB := AButtonsList[iI];
            If pB.AType = tbButton Then
               Dec(iP);
            If iP = -1 Then Begin
               RESULT := pB.AButton;
               BREAK;
               End;
            End;
        End;
End;

Function TIDB_Toolbar.GetButtonsCount: Integer;
Var
   iI: Integer;
   pB: PrecButtonDesc;
Begin
     RESULT := 0;
     For iI:=0 To AButtonsList.Count-1 Do Begin
         pB := AButtonsList[iI];
         If pB.AType = tbButton Then
            Inc(RESULT);
         End;
End;

Function TIDB_Toolbar.AddButton(AType: TButtonType): TSpeedButton;
Var
   ANewButton,
   AnExistingButton: TSpeedButton;
   iI, iCurr,
   iTop, iLeft: Integer;
   sName: AnsiString;
   ATypeOfTheLastButton: TButtonType;
   pB: PrecButtonDesc;
Begin
     iI := 1;
     sName := 'IDB_ToolButton' + IntToStr(iI);
     While SELF.Owner.FindComponent(sName) <> NIL Do Begin
           Inc(iI);
           sName := 'IDB_ToolButton' + IntToStr(iI);
           End;

     ANewButton := TSpeedButton.Create(SELF.Owner);
     ANewButton.Parent := SELF;
     ANewButton.Name := sName;

     RESULT := ANewButton;

// Only for tests.     
//     ANewButton.OnClick := OnButtonClick;

     If AType = tbButton Then Begin
        ANewButton.Flat := bButtonsHaveToBeFlat;
        ANewButton.Width := iButtonsWidth;
        ANewButton.Height := iButtonsHeight;
        End
     Else Begin
        ANewButton.Glyph.LoadFromResourceName(HInstance, 'SEPARATORBUTTON');
        ANewButton.NumGlyphs := 2;
        ANewButton.Caption := '';
        ANewButton.Flat := TRUE;
        ANewButton.Height := iButtonsHeight + iVertSpacing; //iButtonsHeight;
        ANewButton.Width := iSeparatorWidth;
        ANewButton.Enabled := FALSE;
        End;

     iLeft := 5;
     iTop := 2;

     For iI:=0 To AButtonsList.Count-1 Do Begin
         pB := AButtonsList[iI];
         AnExistingButton := pB.AButton;
         ATypeOfTheLastButton := pB.AType;
         iCurr := AnExistingButton.Left;
         If iCurr > iLeft Then
            iLeft := iCurr;
         If AnExistingButton.Top > iTop Then
             iTop := AnExistingButton.Top;
         End;

     If AButtonsList.Count < 1 Then
        ANewButton.Left := iLeft
     Else Begin
        If ATypeOfTheLastButton = tbSeparator Then
           ANewButton.Left := iLeft + iSeparatorWidth + iHorzSpacing
        Else
           ANewButton.Left := iLeft + iButtonsWidth + iHorzSpacing;
        End;
     ANewButton.Top := iTop;

     New(pB);
     pB.AType := AType;
     pB.AButton := ANewButton;
     AButtonsList.Add(pB);
End;

Procedure TIDB_Toolbar.AddButton(AButton: TSpeedButton);
Var
   pB: PrecButtonDesc;
   iI,
   iLeft, iTop: Integer;
   AnExistingButton: TSpeedButton;
   ATypeOfTheLastButton: TButtonType;
Begin
     AButton.Parent := SELF;

// Only for tests.     
//     AButton.OnClick := OnButtonClick;

     AButton.Flat := bButtonsHaveToBeFlat;
     AButton.Width := iButtonsWidth;
     AButton.Height := iButtonsHeight;

     iLeft := 5;
     iTop := 2;

     For iI:=0 To AButtonsList.Count-1 Do Begin
         pB := AButtonsList[iI];
         AnExistingButton := pB.AButton;
         ATypeOfTheLastButton := pB.AType;
         If AnExistingButton.Left > iLeft Then
            iLeft := AnExistingButton.Left;
         If AnExistingButton.Top > iTop Then
             iTop := AnExistingButton.Top;
         End;

     If AButtonsList.Count < 1 Then
        AButton.Left := 5
     Else Begin
        If ATypeOfTheLastButton = tbSeparator Then
           AButton.Left := iLeft + iSeparatorWidth + iHorzSpacing
        Else
           AButton.Left := iLeft + iButtonsWidth + iHorzSpacing;
        End;
     AButton.Top := iTop;

     New(pB);
     pB.AType := tbButton;
     pB.AButton := AButton;
     AButtonsList.Add(pB);

     SELF.RepositionButtons;
     SELF.Invalidate;
End;

Procedure TIDB_Toolbar.RepositionButtons;
Var
   iI: Integer;
   pB: PrecButtonDesc;
   iLeft,
   iTop: Integer;
   bStartANewRow: Boolean;
Begin
{$O-}
     If AButtonsList.Count < 1 Then Begin
        iRowCount := 1;
        EXIT;
        End;

     iRowCount := 1;
     pB := AButtonsList[0];
     pB.AButton.Left := SELF.BorderWidth + 2;
     iTop := pB.AButton.Top;
     iLeft := pB.AButton.Left + pB.AButton.Width + iHorzSpacing;

     For iI:=1 To AButtonsList.Count-1 Do Begin
         pB := AButtonsList[iI];

         If pB.AButton.Visible Then Begin
            bStartANewRow := FALSE;
            If iLeft + pB.AButton.Width + iHorzSpacing > SELF.ClientWidth Then Begin
               bStartANewRow := TRUE;
               Inc(iRowCount);
               End;
            If bStartANewRow Then Begin
               bStartANewRow := FALSE;
               iLeft := SELF.BorderWidth + 2;
               iTop := iTop + iButtonsHeight + iVertSpacing;
               End;
            pB.AButton.Left := iLeft;
            pB.AButton.Top := iTop;

            iLeft := iLeft + pB.AButton.Width + iHorzSpacing;
            End;
         End; // For iI end
     SELF.ClientHeight := 2*2 + iRowCount * (iButtonsHeight + iVertSpacing);
End;

{ This procedure was created based on original TPanel.Paint.
  The original procedure forces all buttons of toolbar to be repainted every time
  when Resize event goes. I don't want to see such a blinked buttons on my toolbar.
  It can be useful only when button repositioning can be done but it is fully
  overmore when I have the only row of buttons. }
Procedure TIDB_Toolbar.Paint;
Var
   TopColor, BottomColor: TColor;

   Procedure AdjustColors(Bevel: TPanelBevel);
   Begin
        TopColor := clBtnHighlight;
        If Bevel = bvLowered Then TopColor := clBtnShadow;
        BottomColor := clBtnShadow;
        If Bevel = bvLowered Then BottomColor := clBtnHighlight;
   End;

Var
   Rect: TRect;
   iI,
   iCurr, iMax: Integer;
   pB: PrecButtonDesc;
Begin
     If (NOT bMouseIsHoldedDown) OR (iRowCount > 1) Then
        Inherited Paint
     Else Begin
        Rect := GetClientRect;
        If BevelOuter <> bvNone Then Begin
           AdjustColors(BevelOuter);
           Frame3D(Canvas, Rect, TopColor, BottomColor, BevelWidth);
           End;
        Frame3D(Canvas, Rect, Color, Color, BorderWidth);
        If BevelInner <> bvNone Then Begin
           AdjustColors(BevelInner);
           Frame3D(Canvas, Rect, TopColor, BottomColor, BevelWidth);
           End;

        iMax := 0;
        For iI:=0 To AButtonsList.Count-1 Do Begin
            pB := AButtonsList[iI];
            iCurr := pB.AButton.Left + pB.AButton.Width + iHorzSpacing;
            If iCurr > iMax Then
               iMax := iCurr;
            End;

        Rect.Left := iMax;
        Canvas.Brush.Color := Color;
        Canvas.FillRect(Rect);

        bMouseIsHoldedDown := FALSE;
        End;
End;

Procedure TIDB_Toolbar.Resize;
Begin
     SELF.bMouseIsHoldedDown := TRUE;
     Inherited Resize;
     RepositionButtons;
End;

Procedure TIDB_Toolbar.WriteButtons(AStream: TStream);
Var
   iI: Integer;
   pB: PrecButtonDesc;
   sData: AnsiString;
   iL: Integer;
Begin
     iL := AButtonsList.Count;
     AStream.Write(iL, Sizeof(iL));
     For iI:=0 To AButtonsList.Count-1 Do Begin
         pB := AButtonsList[iI];
         sData := pB.AButton.Name;
         iL := Length(sData);
         AStream.Write(pB.AType, SizeOf(pB.AType));
         AStream.Write(iL, SizeOf(iL));
         AStream.Write(Pointer(sData)^, iL);
         End;
End;

Procedure TIDB_Toolbar.ReadButtons(AStream: TStream);
Var
   iI, iC, iL: Integer;
   AType: TButtonType;
   sData: AnsiString;
   AComponent: TComponent;
Begin
     AStream.Read(iC, SizeOf(iC));
     For iI:=0 To iC-1 Do Begin
         AStream.Read(AType, SizeOf(AType));
         AStream.Read(iL, SizeOf(iL));
         SetLength(sData, iL);
         AStream.Read(Pointer(sData)^, iL);
         AComponent := SELF.Owner.FindComponent(sData);
         If (AComponent <> NIL) AND (AComponent IS TSpeedButton) Then
            SELF.AddButton(TSpeedButton(AComponent)); 
         End;
End;

Procedure TIDB_Toolbar.DefineProperties(AFiler: TFiler);
Begin
     Inherited DefineProperties(AFiler);
     AFiler.DefineBinaryProperty('AButtonsList', ReadButtons, WriteButtons, TRUE); 
End;

end.
