unit IDB_C_Shape;
{
Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 29.06.2001

This component is used instead of standard TShape.
}

interface

Uses ExtCtrls;

Type
    TIDB_Shape = Class(TShape)
       Published
          property OnDblClick;
       End;

procedure Register;

implementation

Uses Classes;

procedure Register;
begin
     RegisterComponents('Progis IDB', [TIDB_Shape]);
end;

end.
