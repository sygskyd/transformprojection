unit IDB_C_DBNavigator;
{
Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 12.07.2000

This component is used instead of standard TDBNavigator in forms that display
database data.
Cause of using this component instead of standard is that the standard TDBNavigator
button sometimes don't receive CM_MOUSELEAVE message.
}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, DBCtrls;

type
  TIDB_DBNavigator = class(TDBNavigator)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    Procedure MouseLeave;
    Procedure SetOnMouseMoveForButtons(AProcedure: TMouseMoveEvent);
  published
    { Published declarations }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Progis IDB', [TIDB_DBNavigator]);
end;

Procedure TIDB_DBNavigator.MouseLeave;
Var
   AButton: TNavigateBtn;
Begin
     For AButton:=Low(Buttons) To High(Buttons) Do
         Buttons[AButton].Perform(CM_MOUSELEAVE, 0, 0);
End;

Procedure TIDB_DBNavigator.SetOnMouseMoveForButtons(AProcedure: TMouseMoveEvent);
Var
   AButton: TNavigateBtn;
Begin
     For AButton:=Low(Buttons) To High(Buttons) Do
         Buttons[AButton].OnMouseMove := AProcedure;
End;

end.
