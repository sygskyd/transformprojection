unit IDB_C_DBGrid;
{
Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 15.06.2000
Modified: 06.07.2000, 12.07.2000

This component is used instead of standard TDBGrid in forms that display
database data.
Cause of using this component instead of standard is:
1. I need to paint selected record in a diffrerent color in compare with the
   standard component.
2. I had a necessity in the rewriting of standard handlers of KeyDown, MouseDown etc events.
3. I improved the user interface. (I added 'pushed' title of column, etc).
4. The standard TDBGrid has TInplaceEditor in Protected section in declare class
   section. I need to send messages to TInplaceEditor during an user work.

May be important:
- Pushed column - I mean we can have a current field in a table and we can want
                  for this field to make some operations like a search, an update etc.
                  This field in my component will be drawn with pushed title.
- Fixed column - I mean we can have a field in a table that must be uneditable field.
                  For example - ProgisID field. This field can't be abled to move
                  in the grid or to edit.
}
interface

uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
   Grids, IDB_DBGrids;

const
{ This message is sent to a form that contains TDBGrid with an attribute information.
  This message means the TDBGrid must end Drag-and-Drop operation.
  Warning!
  This message is also declared in Am_Def.pas. If you want to change its number, you must
  also change it there! }
   WM_IDB_EndDrag = WM_USER + $2742;

type
   TIDB_DBGrid = class (TIDBPCDBGrid)
   private
    { Private declarations }
      FX,
         FY: Integer;
      FiPushedColumnName: Integer;
      FsFixedColumnName: AnsiString;

      FbDragAndDropIsAllowed: Boolean;
      FbDragAndDropMustBeBegun: Boolean;

      bUpdateScrollBars: Boolean;
      bIAmDoingColumnResizing: Boolean;
      sCancelMessage: AnsiString;
      bTheUserWantsToCancel: Boolean;

      function GetPushedColumnName: AnsiString;
      procedure SetFixedColumnName (sFixedColumnName: AnsiString);
//    Function GetFixedColumnPosition: Integer;

      procedure EndDragging (var AMessage: TWMNoParams); message WM_IDB_EndDrag;
   protected
    { Protected declarations }
      procedure MouseUp (Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
      procedure MouseMove (Shift: TShiftState; X, Y: Integer); override;
      procedure DblClick; override;
   public
    { Public declarations }
     {brovak}
      SortField: string;
     {brovak}
      constructor Create (AOwner: TComponent); override;
      procedure DrawCell (ACol, ARow: LongInt; ARect: TRect; AState: TGridDrawState); override;
      procedure DrawColumnCell (const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState); override;
      procedure MouseDown (Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
      procedure ColumnMoved (FromIndex, ToIndex: Longint); override;
      procedure KeyDown (var Key: Word; Shift: TShiftState); override;
      procedure KeyPress (var Key: Char); override;

      procedure SwitchToBrowseMode;
      procedure SwitchToEditMode;

      procedure AllowDragAndDrop;

      function GetLastMouseX: Integer;
      function GetLastMouseY: Integer;

      procedure UpdateScrollBar;
      procedure ResizeColumn (ACol: Integer);

      property Col;
      property Row;
      property InplaceEditor;
      property PushedColumnName: AnsiString read GetPushedColumnName; // << The current column in the DBGrid. It has pressed caption.
      property HorzSplitterPosition: Integer read iHorzSplitterPosition;
      property CancelMessage: AnsiString read sCancelMessage write sCancelMessage;
   published
    { Published declarations }
      property FixedColumnName: AnsiString read FsFixedColumnName write SetFixedColumnName;
      property OnMouseUp;
    {brovak}
      property OnMouseMove;
    {brovak}
   end;

procedure Register;

implementation

uses
   DDB;

const
   cs_DefaultFixedColumnName = 'PROGISID';
   RowMovementKeys = [VK_UP, VK_PRIOR, VK_DOWN, VK_NEXT];
                       {Arrow_Up, Page_Up, Arrow_Down, Page_Next}
   HomeEndMovementKeys = [VK_HOME, VK_END];
                       {Home, End}
   ci_ValueOfMinDisplayWidth = 5;
   cs_InternalID_FieldName = '_WG_INTERNAL_ID';

procedure Register;
begin
   RegisterComponents ('Progis IDB', [TIDB_DBGrid]);
end;

constructor TIDB_DBGrid.Create (AOwner: TComponent);
begin
   inherited Create (AOwner);
   Options := [dgTitles,
      dgColumnResize,
      dgColLines,
      dgRowLines,
      dgTabs,
      dgRowSelect,
      dgConfirmDelete,
      dgCancelOnExit,
      dgMultiSelect,
      dgAlwaysShowSelection];
   FiPushedColumnName := 0;
   FsFixedColumnName := cs_DefaultFixedColumnName;
   FbDragAndDropIsAllowed := FALSE;
   FbDragAndDropMustBeBegun := FALSE;
   bUpdateScrollBars := TRUE;
   bIAmDoingColumnResizing := FALSE;
   bTheUserWantsToCancel := FALSE;
end;

procedure TIDB_DBGrid.SwitchToBrowseMode;
var
   ACol: Integer;
begin
   ACol := SELF.LeftCol;
   Options := Options - [dgEditing, dgAlwaysShowEditor];
   Options := Options + [dgRowSelect];
   SELF.LeftCol := ACol;
end;

procedure TIDB_DBGrid.SwitchToEditMode;
var
   ACol: Integer;
begin
   if SELF.ReadOnly then
   begin
      SELF.SwitchToBrowseMode;
      EXIT;
   end;
   ACol := SELF.LeftCol;
   Options := Options - [dgRowSelect];
   Options := Options + [dgEditing, dgAlwaysShowEditor];
   SELF.LeftCol := ACol;
end;

function TIDB_DBGrid.GetPushedColumnName: AnsiString;
begin
   RESULT := SELF.Columns.Items[FiPushedColumnName].Field.FieldName;
end;

procedure TIDB_DBGrid.SetFixedColumnName (sFixedColumnName: AnsiString);
begin
   if Trim (sFixedColumnName) = '' then sFixedColumnName := cs_DefaultFixedColumnName;
   SELF.FsFixedColumnName := AnsiUpperCase (sFixedColumnName);
end;
{
Function TIDB_DBGrid.GetFixedColumnPosition: Integer;
Var
   iI: Integer;
Begin
     RESULT := -1;
     For iI:=0 To Columns.Count-1 Do
         If AnsiUpperCase(Columns[iI].Field.FieldName) = SELF.FsFixedColumnName Then Begin
            RESULT := iI;
            EXIT;
            End;
End;
}
{This procedure draws the title of DBGrid and 'pushed' title.}

procedure TIDB_DBGrid.DrawCell (ACol, ARow: LongInt; ARect: TRect; AState: TGridDrawState);

   procedure DrawBtn (bPressed: Boolean);
   var
      iPush: Integer;
   begin
      if bPressed then
         iPush := DFCS_PUSHED
      else
         iPush := 0;
      DrawFrameControl (SELF.Canvas.Handle, ARect, DFC_BUTTON, DFCS_BUTTONPUSH or DFCS_ADJUSTRECT or iPush);
   end;
begin
   inherited DrawCell (ACol, ARow, ARect, AState);
   if csDesigning in SELF.ComponentState then EXIT;
   if SELF.DataSource = nil then EXIT;
   if SELF.DataSource.DataSet = nil then EXIT;
   if SELF.DataSource.DataSet.State = dsInactive then EXIT;

   if (ACol = FiPushedColumnName) and (ARow = 0) then
   begin
      DrawBtn (TRUE);
      if SELF.DataSource.DataSet.State <> dsInactive then
         SELF.Canvas.TextRect (ARect, ARect.Left, ARect.Top, SELF.Columns.Items[ACol].Field.FieldName);
   end;
end;

{This procedure draws DBGrid cells with data.}

procedure TIDB_DBGrid.DrawColumnCell (const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
   clOldBrushColor,
      clOldPenColor {,
      clOldFontColor}: TColor;
begin
   clOldBrushColor := Canvas.Brush.Color;
//   clOldFontColor := Canvas.Font.Color;
   clOldPenColor := Canvas.Pen.Color;
   if AnsiUpperCase (Column.Field.FieldName) = FsFixedColumnName then
   begin // << ProgisID field.
      Canvas.Brush.Color := clBtnFace;
      Canvas.Pen.Color := clBlack;
// ++ Cadmensky
{      Canvas.MoveTo (Rect.Left, Rect.Top);
      Canvas.LineTo (Rect.Right, Rect.Top);
      Canvas.LineTo (Rect.Right, Rect.Bottom);
      Canvas.LineTo (Rect.Left, Rect.Bottom);}
// -- Cadmensky
      Canvas.MoveTo (Rect.Right, Rect.Top);
      Canvas.LineTo (Rect.Right, Rect.Bottom);
      Canvas.LineTo (Rect.Left, Rect.Bottom);
   end;
   DefaultDrawColumnCell (Rect, DataCol, Column, State);
   Canvas.Brush.Color := clOldBrushColor;
//   Canvas.Font.Color := clOldFontColor;
   Canvas.Pen.Color := clOldPenColor;
  { if (column.field is TFloatfield) then
       begin
       if (dgEditing in Options) then TFloatField(column.field).DisplayFormat :='#.#'
       else TFloatField(column.field).DisplayFormat := '#0.00';
       end;    }
end;

{ This procedure tryes to move column from one place to other.
  I don't like to move or replace fixed column. }

procedure TIDB_DBGrid.ColumnMoved (FromIndex, ToIndex: Longint);
begin
     // Don't move a fixed column from its place!
   if AnsiUpperCase (Fields[FromIndex].FieldName) = FsFixedColumnName then
      Columns[FromIndex].Index := FromIndex
   else
        // Don't move also column on selected column place!
      if AnsiUpperCase (Fields[ToIndex].FieldName) = FsFixedColumnName then
         Columns[FromIndex].Index := FromIndex
      else
           // In other way we can move whatever we need to anywhere.
         inherited ColumnMoved (FromIndex, ToIndex);
end;

procedure TIDB_DBGrid.KeyDown (var Key: Word; Shift: TShiftState);
begin
   if (Key in RowMovementKeys)
      or
      ((Key in HomeEndMovementKeys) and (ssCtrl in Shift))
      or
      (Key = vk_Tab)
      or
      (Key = vk_Escape)
      or
      (Key = vk_Delete) then
   begin
      inherited KeyDown (Key, Shift);
      if (Key <> vk_Tab) and (Key <> vk_Escape) then
         SELF.SwitchToBrowseMode;
   end;
end;

procedure TIDB_DBGrid.KeyPress (var Key: Char);
begin
   if (DataSource.DataSet.State = dsInActive) then EXIT;
   if (Key = #27) then
   begin // Esc
      if SELF.bIAmDoingColumnResizing then
           // 306=Do You want to cancel this operation?
         if MessageBox (Handle, PChar (sCancelMessage), 'WinGIS', MB_YESNO + MB_DEFBUTTON1 + MB_ICONQUESTION) = mrYes then
         begin
            bTheUserWantsToCancel := TRUE; // ESC key has been pressed and the user has agreed to cancel an operation.
            Application.ProcessMessages;
         end;
   end
   else
      SELF.SwitchToEditMode;
end;

{ In this procedure I make some important things. I'll count them:
  - which field now will be 'pushed' field;
  - switching data source in edit mode;
  - switching in read-only mode for fixed column;
  - switching grid between string-select mode and each-field-select-separately mode;
  - moving focus into a necessary cell.}

procedure TIDB_DBGrid.MouseDown (Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
   Coord: TGridCoord;
   ACol, ARow: Integer;
begin
   if DataSource.DataSet.RecordCount < 1 then EXIT;

   Coord := MouseCoord (X, Y);
   ACol := Coord.X;

   ARow := Coord.Y;
   if ACol < 0 then EXIT;
   if ARow < 0 then EXIT;
   if ARow = 0 then
   begin
      FiPushedColumnName := ACol;
// ++ Cadmensky
      SELF.Invalidate;
   end;
//   SELF.Invalidate;
// -- Cadmensky

   SELF.FbDragAndDropMustBeBegun := (SELF.SelectedRows.Count = 1) and
      FbDragAndDropIsAllowed and (ARow > 0);

   inherited MouseDown (Button, Shift, X, Y);

   if DataSource.DataSet.CanModify and (not (ssCtrl in Shift)) then
      if (ARow > 0) and (ssDouble in Shift) and (Button = mbLeft) then
      begin
         if DataSource.DataSet.Fields.Count > 1 then
         begin
            if DataSource.DataSet.FindField (FsFixedColumnName) <> nil then
               DataSource.DataSet.FieldByName (FsFixedColumnName).ReadOnly := TRUE;
            DataSource.DataSet.Edit;
         end;
      end;
   if (DataSource.DataSet.State = dsBrowse) or (DataSource.DataSet.State = dsInActive) then
      SELF.SwitchToBrowseMode
   else
   begin
      SELF.SwitchToEditMode;
      SelectedField := Columns[ACol].Field;
   end;
end;

procedure TIDB_DBGrid.MouseUp (Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
   iI: Integer;
begin
   SELF.FbDragAndDropMustBeBegun := FALSE;

   inherited MouseUp (Button, Shift, X, Y);

   if not (dgColumnResize in SELF.Options) then EXIT;
   if SELF.DataSource = nil then EXIT;
   if SELF.DataSource.DataSet = nil then EXIT;
   if not SELF.DataSource.DataSet.Active then EXIT;
     // For each field, which has diplay width less than ci_ValueOfMinDisplayWidth,
     // I have to set state of visibility in FALSE.
   for iI := 0 to SELF.Columns.Count - 1 do
      if SELF.Columns.Items[iI].Width < ci_ValueOfMinDisplayWidth then
         SELF.Columns.Items[iI].Field.Visible := FALSE;
end;

procedure TIDB_DBGrid.MouseMove (Shift: TShiftState; X, Y: Integer);
begin
   SELF.FX := X;
   SELF.FY := Y;

   if SELF.FbDragAndDropMustBeBegun then
   begin
      BeginDrag (FALSE, -1);
      SELF.FbDragAndDropMustBeBegun := FALSE;
   end;

   inherited MouseMove (Shift, X, Y);
end;

function TIDB_DBGrid.GetLastMouseX: Integer;
begin
   RESULT := SELF.FX;
end;

function TIDB_DBGrid.GetLastMouseY: Integer;
begin
   RESULT := SELF.FY;
end;

procedure TIDB_DBGrid.EndDragging (var AMessage: TWMNoParams);
begin
   SELF.EndDrag (FALSE);
end;

procedure TIDB_DBGrid.AllowDragAndDrop;
begin
   FbDragAndDropIsAllowed := TRUE;
end;

{ Sometimes I have to call it myself. I can't explain why and when... }

procedure TIDB_DBGrid.UpdateScrollBar;
begin
   if SELF.bUpdateScrollBars then
      inherited UpdateScrollBar;
end;

procedure TIDB_DBGrid.DblClick;
var
   AGridCoord: TGridCoord;
   AField: TField;
   iMark: Integer;
   sSortString,
      sFieldName: AnsiString;
   bSortItAsAsc: Boolean;
   sOldSortString, sOldSortField: AnsiString;
begin
   if SELF.DataSource.DataSet is TDADODataSet then
   begin

      if SELF.iHorzSplitterPosition > -1 then
      begin
// ++ Cadmensky
//           SELF.ResizeColumn(iHorzSplitterPosition);
// -- Cadmensky
         inherited DblClick;
         EXIT;
      end;

      sSortString := Trim (AnsiUpperCase (TDADODataSet (SELF.DataSource.DataSet).Sort));
      AGridCoord := MouseCoord (FX, FY);
      if AGridCoord.Y = 0 then
      begin
         AField := SELF.GetColField (AGridCoord.X);
         sFieldName := AnsiUpperCase (AField.FieldName);

         if sSortString = '' then
            bSortItAsAsc := TRUE
         else
            if Pos (sFieldName, sSortString) > 0 then
               bSortItAsAsc := Pos (' ASC', sSortString) < 1
            else
               bSortItAsAsc := TRUE;

         SELF.DataSource.DataSet.DisableControls;
         try
            try
               iMark := SELF.DataSource.DataSet.FieldByName (cs_InternalID_FieldName).AsInteger;
            except
               iMark := -1;
            end;

            SELF.SelectedRows.Clear;
{brovak}
//            RowBitsClear;
{brovak}
            try
// ++ Cadmensky IDB Version 2.3.9
               sOldSortString := TDADODataSet (SELF.DataSource.DataSet).Sort;
               sOldSortField := SortField;
// -- Cadmensky IDB Version 2.3.9
               if bSortItAsAsc then
                  TDADODataSet (SELF.DataSource.DataSet).Sort := '[' + AField.FieldName + '] ASC'
               else
                  TDADODataSet (SELF.DataSource.DataSet).Sort := '[' + AField.FieldName + '] DESC';
             {brovak}
               SortField := AField.FieldName;
             {brovak}
            except
// ++ Cadmensky IDB Version 2.3.9
               TDADODataSet (SELF.DataSource.DataSet).Sort := sOldSortString;
               SortField := sOldSortField;
// -- Cadmensky IDB Version 2.3.9
            end;
            try
               if not TDADODataSet (SELF.DataSource.DataSet).Search (cs_InternalID_FieldName, IntToStr (iMark)) then
                  SELF.DataSource.DataSet.First;
            except
               SELF.DataSource.DataSet.First;
            end;
         finally
            SELF.DataSource.DataSet.EnableControls;
         end;
      end;
   end;
   inherited DblClick;
end;

procedure TIDB_DBGrid.ResizeColumn (ACol: Integer);
var
   iI: Integer;
   iMaxLength,
      iLength: Integer;
   AField: TField;
   iMark: Integer;
begin
   if ACol > -1 then
   begin
      iMaxLength := -1;
      try
         iMark := SELF.DataSource.DataSet.FieldByName (cs_InternalID_FieldName).AsInteger;
      except
         iMark := -1;
      end;
      SELF.DataSource.DataSet.DisableControls;
      SELF.bUpdateScrollBars := FALSE;
      SELF.bIAmDoingColumnResizing := TRUE;
      AField := SELF.Columns[ACol].Field;
      SELF.DataSource.DataSet.First;
      try
         for iI := 0 to SELF.DataSource.DataSet.RecordCount - 1 do
         begin
            iLength := SELF.Canvas.TextWidth (AField.AsString);
            if iLength > iMaxLength then
               iMaxLength := iLength;
            SELF.DataSource.DataSet.Next;
            Application.ProcessMessages;
            if SELF.bTheUserWantsToCancel then
            begin
               SELF.bTheUserWantsToCancel := FALSE;
               BREAK;
            end;
         end; // For iI end
         if iMaxLength > -1 then
         begin
            iMaxLength := iMaxLength + 10;
            AField.DisplayWidth := iMaxLength;
            SELF.Columns[ACol].Width := iMaxLength;
         end;
      finally
         SELF.bIAmDoingColumnResizing := FALSE;
         SELF.bUpdateScrollBars := TRUE;
         try
            if not TDADODataSet (SELF.DataSource.DataSet).Search (cs_InternalID_FieldName, IntToStr (iMark)) then
               SELF.DataSource.DataSet.First;
         except
            SELF.DataSource.DataSet.First;
         end;
         SELF.DataSource.DataSet.EnableControls;
      end;
   end;
end;

end.

