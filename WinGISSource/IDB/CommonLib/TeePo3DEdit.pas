{**********************************************}
{   TPoint3DSeries Component Editor Dialog     }
{   Copyright (c) 1998-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeePo3DEdit;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, ExtCtrls, Chart, Series, TeePoin3, ComCtrls,
  TeCanvas, TeePenDlg;

type
  TPoint3DSeriesEditor = class(TForm)
    GPLine: TGroupBox;
    BColor: TButtonColor;
    CBColorEach: TCheckBox;
    Button1: TButtonPen;
    Label4: TLabel;
    SEPointDepth: TEdit;
    UDPointDepth: TUpDown;
    procedure FormShow(Sender: TObject);
    procedure CBColorEachClick(Sender: TObject);
    procedure SEPointDepthChange(Sender: TObject);
  private
    { Private declarations }
    TheSeries    : TPoint3DSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
uses TeEngine, TeePoEdi, TeeProcs, TeeConst;

procedure TPoint3DSeriesEditor.FormShow(Sender: TObject);
begin
  TheSeries:=TPoint3DSeries(Tag);
  With TheSeries do
  Begin
    UDPointDepth.Position :=Round(DepthSize);
    CBColorEach.Checked:=ColorEachPoint;
    Button1.LinkPen(LinePen);
  end;
  BColor.LinkProperty(TheSeries,'SeriesColor');
  BColor.Enabled:=not TheSeries.ColorEachPoint;
  TeeInsertPointerForm(Parent,TheSeries.Pointer,TeeMsg_GalleryPoint);
end;

procedure TPoint3DSeriesEditor.CBColorEachClick(Sender: TObject);
begin
  TheSeries.ColorEachPoint:=CBColorEach.Checked;
  BColor.Enabled:=not TheSeries.ColorEachPoint;
end;

procedure TPoint3DSeriesEditor.SEPointDepthChange(Sender: TObject);
begin
  if Showing then TheSeries.DepthSize:=UDPointDepth.Position;
end;

initialization
  RegisterClass(TPoint3DSeriesEditor);
end.
