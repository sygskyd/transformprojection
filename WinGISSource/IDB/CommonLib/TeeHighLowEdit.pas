{**********************************************}
{   TeeChart HighLow Series Editor             }
{   Copyright (c) 1999-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeHighLowEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ErrorBar, StdCtrls, TeCanvas, TeePenDlg;

type
  THighLowEditor = class(TForm)
    BPen: TButtonPen;
    BHighPen: TButtonPen;
    BLowPen: TButtonPen;
    BBrush: TButton;
    CBColorEach: TCheckBox;
    BLowBrush: TButton;
    BColor: TButtonColor;
    procedure FormShow(Sender: TObject);
    procedure BBrushClick(Sender: TObject);
    procedure BLowBrushClick(Sender: TObject);
    procedure CBColorEachClick(Sender: TObject);
  private
    { Private declarations }
    HighLow : THighLowSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

Uses TeeBrushDlg;

procedure THighLowEditor.FormShow(Sender: TObject);
begin
  HighLow:=THighLowSeries(Tag);
  With HighLow do
  begin
    CBColorEach.Checked:=ColorEachPoint;
    BLowPen.LinkPen(LowPen);
    BHighPen.LinkPen(HighPen);
    BPen.LinkPen(Pen);
  end;
  BColor.LinkProperty(HighLow,'SeriesColor');
end;

procedure THighLowEditor.BBrushClick(Sender: TObject);
begin
  EditChartBrush(Self,HighLow.HighBrush);
end;

procedure THighLowEditor.BLowBrushClick(Sender: TObject);
begin
  EditChartBrush(Self,HighLow.LowBrush);
end;

procedure THighLowEditor.CBColorEachClick(Sender: TObject);
begin
  HighLow.ColorEachPoint:=CBColorEach.Checked;
end;

initialization
  RegisterClass(THighLowEditor);
end.
