{******************************************}
{    TGanttSeries Editor Dialog            }
{ Copyright (c) 1996-2000 by David Berneda }
{******************************************}
{$I teedefs.inc}
unit TeeGanttEdi;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls, QComCtrls,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, ComCtrls,
     {$ENDIF}
     Chart, GanttCh, TeCanvas, TeePenDlg;

type
  TGanttSeriesEditor = class(TForm)
    Label2: TLabel;
    SEPointVertSize: TEdit;
    BConnLines: TButtonPen;
    UDPointVertSize: TUpDown;
    GPLine: TGroupBox;
    BColor: TButtonColor;
    CBColorEach: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure SEPointVertSizeChange(Sender: TObject);
    procedure CBColorEachClick(Sender: TObject);
    procedure BColorClick(Sender: TObject);
  private
    { Private declarations }
    TheSeries    : TGanttSeries;
    procedure RefreshShape;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
uses TeEngine, Series, TeePoEdi, TeeProcs, TeeConst;

procedure TGanttSeriesEditor.RefreshShape;
begin
  BColor.Enabled:=not TheSeries.ColorEachPoint;
end;

procedure TGanttSeriesEditor.FormShow(Sender: TObject);
begin
  TheSeries:=TGanttSeries(Tag);
  With TheSeries do
  begin
    UDPointVertSize.Position := Pointer.VertSize;
    CBColorEach.Checked      := ColorEachPoint;
    BConnLines.LinkPen(ConnectingPen);
  end;
  BColor.LinkProperty(TheSeries,'SeriesColor');
  RefreshShape;
  TeeInsertPointerForm(Parent,TheSeries.Pointer,TeeMsg_GalleryGantt);
end;

procedure TGanttSeriesEditor.SEPointVertSizeChange(Sender: TObject);
begin
  if Showing then TheSeries.Pointer.VertSize:=UDPointVertSize.Position;
end;

procedure TGanttSeriesEditor.CBColorEachClick(Sender: TObject);
begin
  TheSeries.ColorEachPoint:=CBColorEach.Checked;
  RefreshShape;
end;

procedure TGanttSeriesEditor.BColorClick(Sender: TObject);
begin
  TheSeries.ColorEachPoint:=False;
end;

initialization
  RegisterClass(TGanttSeriesEditor);
end.
