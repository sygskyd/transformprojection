{******************************************************************************+
  Unit PropCollect
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  Release: 1
--------------------------------------------------------------------------------
+******************************************************************************}

Unit PropCollect;

Interface

Uses Classes,Graphics;

Const intNoChange       = -40;
      intNotSet         = -50;

      dblNoChange       = -4E301;
      dblNotSet         = -5E301;

      clNoChange        = TColor(intNoChange);
      clNotSet          = TColor(intNotSet);

      sNotSet           = #2'#%$NOTSET$%#';
      sNoChange         = #1'#%$NOCHANGE$%#';

      bsYes             = 1;
      bsNo              = 0;
      bsNoChange        = intNoChange;
      bsNotSet          = intNotSet;

      soNoChange        = 255;

Type {*************************************************************************+
       Optionen f�r Auswahllisten
     +*************************************************************************}
     TSelectOptions     = Set of Byte;

     {*************************************************************************+
      Type TPropertyMode
     ---------------------------------------------------------------------------
       Gibt an, wie sich die Stil-Objekte bei Zuweisung an ihre Properties
       verhalten.
       - pmSet = der zugewiesene Wert �berschreibt den aktuellen Wert
       - pmCollect = ist der Eigenschaftswert NotSet, so wird der Wert kopiert,
           ist der Eigenschaftswert ungleich dem zugewiesenen, so wird der
           Eigenschaftswert NoChange gesetzt, sonst erfolgt keine �nderung
       - pmUpdate = der aktuelle Wert wird �berschrieben, sofern der
           zugewiesene Wert nicht NoChange ist
     +*************************************************************************}
     TPropertyMode      = (pmSet,pmCollect,pmUpdate);

     PColorArray        = ^TColorArray;
     TColorArray        = Array[0..0] of TColor;

     PDoubleArray       = ^TDoubleArray;
     TDoubleArray       = Array[0..0] of Double;

     PLongIntArray      = ^TLongIntArray;
     TLongIntArray      = Array[0..0] of LongInt;

     PShortIntArray     = ^TShortIntArray;
     TShortIntArray     = Array[0..0] of ShortInt;

     PStringArray       = ^TStringArray;
     TStringArray       = Array[0..0] of AnsiString;

     TSelectOptionList = Class;

     PSelectOptionListArray = ^TSelectOptionListArray;
     TSelectOptionListArray = Array[0..MaxInt Div 4-1] of TSelectOptionList; 

     TPropertyCollector = Class(TPersistent)
      Private
       FBooleanProps    : Integer;
       FBooleanVals     : PShortIntArray;
       FColorProps      : Integer;
       FColorVals       : PColorArray;
       FDoubleProps     : Integer;
       FDoubleVals      : PDoubleArray;
       FEnabled         : Boolean;
       FIntProps        : Integer;
       FIntVals         : PLongIntArray;
       FOptionProps     : Integer;
       FOptionVals      : PSelectOptionListArray;
       FOptionCount     : Integer;
       FModified        : Boolean;
       FPropertyMode    : TPropertyMode;
       FStringProps     : Integer;
       FStringVals      : PStringArray;
       Function    CollectBoolean(var FBoolean:ShortInt;Const ABoolean:ShortInt):Boolean;
       Function    CollectColor(var FColor:TColor;Const AColor:TColor):Boolean;
       Function    CollectDouble(var FDouble:Double;Const ADouble:Double):Boolean;
       Function    CollectLongInt(var FLongInt:LongInt;Const ALongInt:LongInt):Boolean;
       Function    CollectString(var FString:AnsiString;Const AString:AnsiString):Boolean;
       Function    CollectSelectOptions(var FSelectOptions:TSelectOptions;Const ASelectOptions:TSelectOptions):Boolean;
       Procedure   SetBooleanProps(AProps:Integer);
       Procedure   SetColorProps(AProps:Integer);
       Procedure   SetDoubleProps(AProps:Integer);
       Procedure   SetIntProps(AProps:Integer);
       Procedure   SetOptionCount(ACount:Integer);
       Procedure   SetOptionProps(AProps:Integer);
       Procedure   SetStringProps(AProps:Integer);
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
       Property    BooleanProps:Integer read FBooleanProps write SetBooleanProps;
       Property    ColorProps:Integer read FColorProps write SetColorProps;
       Property    DoubleProps:Integer read FDoubleProps write SetDoubleProps;
       Function    GetBooleanVal(AIndex:Integer):ShortInt;
       Function    GetBooleanOptionList(AIndex:Integer):TSelectOptionList;
       Function    GetColorVal(AIndex:Integer):TColor;
       Function    GetColorOptionList(AIndex:Integer):TSelectOptionList;
       Function    GetDoubleVal(AIndex:Integer):Double;
       Function    GetDoubleOptionList(AIndex:Integer):TSelectOptionList;
       Function    GetIntVal(AIndex:Integer):LongInt;
       Function    GetIntOptionList(AIndex:Integer):TSelectOptionList;
       Function    GetStringVal(AIndex:Integer):AnsiString;
       Function    GetStringOptionList(AIndex:Integer):TSelectOptionList;
       Function    GetOptionList(AIndex:Integer):TSelectOptionList;
       Property    IntProps:Integer read FIntProps write SetIntProps;
       Property    OptionProps:Integer read FOptionProps write SetOptionProps;
       Procedure   SetBooleanVal(AIndex:Integer;AValue:ShortInt);
       Procedure   SetColorVal(AIndex:Integer;AValue:TColor);
       Procedure   SetDoubleVal(AIndex:Integer;Const AValue:Double);
       Procedure   SetIntVal(AIndex:Integer;AValue:LongInt);
       Procedure   SetPropertyMode(AMode:TPropertyMode); virtual;
       Property    StringProps:Integer read FStringProps write SetStringProps;
       Procedure   SetStringVal(AIndex:Integer;Const AValue:AnsiString);
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Property    UpdatesEnabled:Boolean read FEnabled write FEnabled default TRUE;
       Property    Modified:Boolean read FModified write FModified;
       Property    PropertyMode:TPropertyMode read FPropertyMode write SetPropertyMode;
       Function    ValidValues:Boolean; virtual;
     end;

     TGetSelectionValues = Procedure (AType,AOption:Integer;var LongName,
         ShortName:String;var Value:Double);

     TSelectOptionList = Class(TPropertyCollector)
      Private
       FCount      : Integer;
       FItems      : Array[0..255] of Byte;
       FOptions    : TSelectOptions;
       FGetValues  : TGetSelectionValues;
       FType       : Integer;
       Function    GetItem(AIndex:Integer):Byte;
       Procedure   SetOptions(AOptions:TSelectOptions);
       {++brovak}
       Function LimitToLong(AValue:Double):LongInt;
       {brovak}
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
       Function    GetDblValues(AIndex:Integer):Double; virtual;
       Function    GetLongNames(AIndex:Integer):String; virtual;
       Function    GetShortNames(AIndex:Integer):String; virtual;
       Function    GetValues(AIndex:Integer):LongInt; virtual;
       Procedure   SetPropertyMode(AMode:TPropertyMode); override;
      Public
       Procedure   AddLongNamesToList(AList:TStrings);
       Procedure   AddShortNamesToList(AList:TStrings);
       Property    Count:Integer read FCount;
       Property    DblValues[AIndex:Integer]:Double read GetDblValues;
       Property    GetSelectionValues:TGetSelectionValues read FGetValues write FGetValues;
       Function    IndexOf(AOption:Byte):Integer;
       Function    IndexOfValue(AValue:LongInt):Integer;
       Function    IndexOfDblValue(Const AValue:Double):Integer;
       Property    Items[AIndex:Integer]:Byte read GetItem; default;
       Property    ListType:Integer read FType write FType;
       Property    LongNames[AIndex:Integer]:String read GetLongNames;
       Property    Options:TSelectOptions read FOptions write SetOptions;
       Property    ShortNames[AIndex:Integer]:String read GetShortNames;
       Property    Values[AIndex:Integer]:LongInt read GetValues;
     end;

Function BooleanStyle(ABool:Boolean):ShortInt;

Function InvertBooleanStyle(ABool:ShortInt;ThreeState:Boolean):ShortInt;

Implementation

Uses NumTools;

Function BooleanStyle
   (
   ABool           : Boolean
   )
   : ShortInt;
  begin
    if ABool then Result:=bsYes
    else Result:=bsNo;
  end;

Function InvertBooleanStyle
   (
   ABool           : ShortInt;
   ThreeState      : Boolean
   )
   : ShortInt;
  begin
    if ABool=bsNo then Result:=bsYes
    else if (ABool=bsYes) and ThreeState then Result:=bsNoChange
    else Result:=bsNo;
  end;

{==============================================================================+
  TPropertyCollector
+==============================================================================}

Constructor TPropertyCollector.Create;
  begin
    inherited Create;
    FEnabled:=TRUE;
  end;
  
Destructor TPropertyCollector.Destroy;
  var Cnt          : Integer;
  begin
    for Cnt:=0 to FOptionCount-1 do FOptionVals^[Cnt].Free;
    if FOptionCount>0 then FreeMem(FOptionVals,FOptionCount*SizeOf(Pointer));
    if FIntProps>0 then FreeMem(FIntVals,FIntProps*SizeOf(Integer));
    if FColorProps>0 then FreeMem(FColorVals,FColorProps*SizeOf(TColor));
    if FDoubleProps>0 then FreeMem(FDoubleVals,FDoubleProps*SizeOf(Double));
    if FBooleanProps>0 then FreeMem(FBooleanVals,FBooleanProps*SizeOf(ShortInt));
    if FStringProps>0 then FreeMem(FStringVals,FStringProps*SizeOf(AnsiString));
    inherited Destroy;
  end;

Function TPropertyCollector.CollectDouble
   (
   var FDouble     : Double;
   Const ADouble   : Double
   )
   : Boolean;
  begin
    Result:=FALSE;
    case PropertyMode of
      pmSet    : FDouble:=ADouble;
      pmCollect:
        if FDouble<dblNotSet+Tolerance then begin
          FDouble:=ADouble;
          FModified:=TRUE;
        end
        else if Abs(FDouble-ADouble)>Tolerance then begin
          FDouble:=dblNoChange;
          FModified:=TRUE;
          Result:=TRUE;
        end;
      pmUpdate: if (DblCompare(ADouble,dblNoChange)<>0)
            and (DblCompare(ADouble,FDouble)<>0) then begin
          FDouble:=ADouble;
          FModified:=TRUE;
        end;
    end;
  end;

Function TPropertyCollector.CollectBoolean
   (
   var FBoolean    : ShortInt;
   Const ABoolean  : ShortInt
   )
   : Boolean;
  begin
    Result:=FALSE;
    case PropertyMode of
      pmSet    : FBoolean:=ABoolean;
      pmCollect: if FBoolean=bsNotSet then begin
          FBoolean:=ABoolean;
          FModified:=TRUE;
        end
        else if FBoolean<>ABoolean then begin
          FBoolean:=bsNoChange;
          FModified:=TRUE;
          Result:=TRUE;
        end;
      pmUpdate: if (ABoolean<>bsNoChange) and (ABoolean<>FBoolean) then begin
          FBoolean:=ABoolean;
          FModified:=TRUE;
        end;
    end;
  end;

Function TPropertyCollector.CollectString
   (
   var FString    : AnsiString;
   Const AString  : AnsiString
   )
   : Boolean;
  begin
    Result:=FALSE;
    case PropertyMode of
      pmSet    : FString:=AString;
      pmCollect: if FString=sNotSet then begin
          FString:=AString;
          FModified:=TRUE;
        end
        else if FString<>AString then begin
          FString:=sNoChange;
          FModified:=TRUE;
          Result:=TRUE;
        end;
      pmUpdate: if (AString<>sNoChange) and (FString<>AString) then begin
          FString:=AString;
          FModified:=TRUE;
        end;
    end;
  end;

Function TPropertyCollector.CollectLongInt
   (
   var FLongInt    : LongInt;
   Const ALongInt  : LongInt
   )
   : Boolean;
  begin
    Result:=FALSE;
    case PropertyMode of
      pmSet    : FLongInt:=ALongInt;
      pmCollect: if FLongInt=intNotSet then begin
          FLongInt:=ALongInt;
          FModified:=TRUE;
        end
        else if FLongInt<>ALongInt then begin
          FLongInt:=intNoChange;
          FModified:=TRUE;
          Result:=TRUE;
        end;
      pmUpdate: if (ALongInt<>intNoChange) and (ALongInt<>FLongInt) then begin
          FLongInt:=ALongInt;
          FModified:=TRUE;
        end;
    end;
  end;

Function TPropertyCollector.CollectColor
   (
   var FColor      : TColor;
   Const AColor    : TColor
   )
   : Boolean;
  begin
    Result:=FALSE;
    case PropertyMode of
      pmSet    : FColor:=AColor;
      pmCollect: if FColor=clNotSet then begin
          FColor:=AColor;
          FModified:=TRUE;
        end
        else if FColor<>AColor then begin
          FColor:=clNoChange;
          FModified:=TRUE;
          Result:=TRUE;
        end;
      pmUpdate: if (AColor<>clNoChange) and (FColor<>AColor) then begin
          FColor:=AColor;
          FModified:=TRUE;
        end;
    end;
  end;

Function TPropertyCollector.CollectSelectOptions
   (
   var FSelectOptions      : TSelectOptions;
   Const ASelectOptions    : TSelectOptions
   )
   : Boolean;
  begin
    Result:=FALSE;
    if FPropertyMode=pmSet then FSelectOptions:=ASelectOptions
    else FSelectOptions:=FSelectOptions+ASelectOptions;
  end;

Procedure TPropertyCollector.AssignTo
   (
   Dest            : TPersistent
   );
  var Cnt          : Integer;
  begin
    if not(Dest is TPropertyCollector) then inherited AssignTo(Dest)
    else with Dest as TPropertyCollector do begin    
      if FPropertyMode<>pmUpdate then for Cnt:=0 to FOptionCount-1 do FOptionVals^[Cnt].Options:=Self.FOptionVals^[Cnt].Options;
      for Cnt:=0 to FColorProps-1 do SetColorVal(Cnt,Self.FColorVals^[Cnt]);
      for Cnt:=0 to FDoubleProps-1 do SetDoubleVal(Cnt,Self.FDoubleVals^[Cnt]);
      for Cnt:=0 to FIntProps-1 do SetIntVal(Cnt,Self.FIntVals^[Cnt]);
      for Cnt:=0 to FBooleanProps-1 do SetBooleanVal(Cnt,Self.FBooleanVals^[Cnt]);
      for Cnt:=0 to FStringProps-1 do SetStringVal(Cnt,Self.FStringVals^[Cnt]);
      FModified:=Self.FModified;
      FEnabled:=Self.FEnabled;
    end;
  end;

Procedure TPropertyCollector.SetPropertyMode
   (
   AMode           : TPropertyMode
   );
  var Cnt          : Integer;
  begin
    FPropertyMode:=AMode;
    for Cnt:=0 to FOptionCount-1 do FOptionVals^[Cnt].PropertyMode:=AMode;
    if FPropertyMode=pmCollect then begin
      for Cnt:=0 to FColorProps-1 do FColorVals^[Cnt]:=clNotSet;
      for Cnt:=0 to FDoubleProps-1 do FDoubleVals^[Cnt]:=dblNotSet;
      for Cnt:=0 to FIntProps-1 do FIntVals^[Cnt]:=intNotSet;
      for Cnt:=0 to FBooleanProps-1 do FBooleanVals^[Cnt]:=bsNotSet;
      for Cnt:=0 to FStringProps-1 do FStringVals^[Cnt]:=sNotSet;
    end;
  end;

Procedure TPropertyCollector.SetOptionProps
   (
   AProps          : Integer
   );
  begin
    SetOptionCount(FOptionCount-FOptionProps+AProps);
    FOptionProps:=AProps;
  end;

Procedure TPropertyCollector.SetOptionCount
   (
   ACount          : Integer
   );
  var Cnt          : Integer;
  begin
    if FOptionCount>0 then begin
      for Cnt:=0 to FOptionCount-1 do FOptionVals^[Cnt].Free;
      FreeMem(FOptionVals,FOptionCount*SizeOf(Pointer));
    end;
    FOptionCount:=ACount;
    GetMem(FOptionVals,FOptionCount*SizeOf(Pointer));
    for Cnt:=0 to FOptionCount-1 do FOptionVals^[Cnt]:=TSelectOptionList.Create;
  end;

Function TPropertyCollector.GetOptionList
   (
   AIndex          : Integer
   )
   : TSelectOptionList;
  begin
    Result:=FOptionVals^[AIndex];
  end;

Procedure TPropertyCollector.SetDoubleProps
   (
   AProps          : Integer
   );
  begin
    SetOptionCount(FOptionCount-FDoubleProps+AProps);
    if FDoubleProps<>0 then FreeMem(FDoubleVals,FDoubleProps*SizeOf(Double));
    FDoubleProps:=AProps;
    GetMem(FDoubleVals,FDoubleProps*SizeOf(Double));
    for AProps:=0 to FDoubleProps-1 do FDoubleVals^[AProps]:=dblNotSet;
  end;

Function TPropertyCollector.GetDoubleVal
   (
   AIndex          : Integer
   )
   : Double;
  begin
   try
    if AIndex>=FDoubleProps then Result:=dblNoChange
    else Result:=FDoubleVals^[AIndex];
   except
    Result:=dblNoChange;
   end;
  end;

Procedure TPropertyCollector.SetDoubleVal
   (
   AIndex          : Integer;
   Const AValue    : Double
   );
  begin
   try
    if AIndex<FDoubleProps then if CollectDouble(FDoubleVals^[AIndex],AValue) then
        GetDoubleOptionList(AIndex).Options:=[soNoChange];
    except
    end;
  end;

Function TPropertyCollector.GetDoubleOptionList
   (
   AIndex          : Integer
   )
   : TSelectOptionList;
  begin
    Result:=GetOptionList(FOptionProps+AIndex);
  end;

Procedure TPropertyCollector.SetColorProps
   (
   AProps          : Integer
   );
  begin
    SetOptionCount(FOptionCount-FColorProps+AProps);
    if FColorProps<>0 then FreeMem(FColorVals,FColorProps*SizeOf(TColor));
    FColorProps:=AProps;
    GetMem(FColorVals,FColorProps*SizeOf(TColor));
    for AProps:=0 to FColorProps-1 do FColorVals^[AProps]:=clNotSet;
  end;

Function TPropertyCollector.GetColorVal
   (
   AIndex          : Integer
   )
   : TColor;
  begin
    if AIndex>=FColorProps then Result:=clNoChange
    else Result:=FColorVals^[AIndex];
  end;

Procedure TPropertyCollector.SetColorVal
   (
   AIndex          : Integer;
   AValue          : TColor
   );
  begin
    if AIndex<FColorProps then if CollectColor(FColorVals^[AIndex],AValue) then
        GetColorOptionList(AIndex).Options:=[soNoChange];
  end;

Function TPropertyCollector.GetColorOptionList
   (
   AIndex          : Integer
   )
   : TSelectOptionList;
  begin
    Result:=GetOptionList(FOptionProps+FDoubleProps+FIntProps+AIndex);
  end;

Procedure TPropertyCollector.SetIntProps
   (
   AProps          : Integer
   );
  begin
    SetOptionCount(FOptionCount-FIntProps+AProps);
    if FIntProps<>0 then FreeMem(FIntVals,FIntProps*SizeOf(LongInt));
    FIntProps:=AProps;
    GetMem(FIntVals,FIntProps*SizeOf(LongInt));
    for AProps:=0 to FIntProps-1 do FIntVals^[AProps]:=intNotSet;
  end;

Function TPropertyCollector.GetIntVal
   (
   AIndex          : Integer
   )
   : LongInt;
  begin
    if AIndex>=FIntProps then Result:=intNoChange
    else Result:=FIntVals^[AIndex];
  end;

Procedure TPropertyCollector.SetIntVal
   (
   AIndex          : Integer;
   AValue          : LongInt
   );
  begin
    if AIndex<FIntProps then if CollectLongInt(FIntVals^[AIndex],AValue) then
        GetIntOptionList(AIndex).Options:=[soNoChange];
  end;

Function TPropertyCollector.GetIntOptionList
   (
   AIndex          : Integer
   )
   : TSelectOptionList;
  begin
    Result:=GetOptionList(FOptionProps+FDoubleProps+AIndex);
  end;

Procedure TPropertyCollector.SetBooleanProps
   (
   AProps          : Integer
   );
  begin
    SetOptionCount(FOptionCount-FBooleanProps+AProps);
    if FBooleanProps<>0 then FreeMem(FBooleanVals,FBooleanProps*SizeOf(ShortInt));
    FBooleanProps:=AProps;
    GetMem(FBooleanVals,FBooleanProps*SizeOf(ShortInt));
    for AProps:=0 to FBooleanProps-1 do FBooleanVals^[AProps]:=bsNotSet;
  end;

Function TPropertyCollector.GetBooleanVal
   (
   AIndex          : Integer
   )
   : ShortInt;
  begin
    if AIndex>=FBooleanProps then Result:=bsNoChange
    else Result:=FBooleanVals^[AIndex];
  end;

Procedure TPropertyCollector.SetBooleanVal
   (
   AIndex          : Integer;
   AValue          : ShortInt
   );
  begin
    if AIndex<FBooleanProps then if CollectBoolean(FBooleanVals^[AIndex],AValue) then
        GetBooleanOptionList(AIndex).Options:=[soNoChange];
  end;

Function TPropertyCollector.GetBooleanOptionList
   (
   AIndex          : Integer
   )
   : TSelectOptionList;
  begin
    Result:=GetOptionList(FOptionProps+FDoubleProps+FIntProps+FColorProps+AIndex);
  end;

Procedure TPropertyCollector.SetStringProps
   (
   AProps          : Integer
   );
  begin
    SetOptionCount(FOptionCount-FStringProps+AProps);
    if FStringProps>0 then FreeMem(FStringVals,FStringProps*SizeOf(AnsiString));
    FStringProps:=AProps;
    GetMem(FStringVals,FStringProps*SizeOf(AnsiString));
    // fill array with zeroes, otherwise delphi gets confused when
    // assigning a new string to the elements
    FillChar(FStringVals^,FStringProps*SizeOf(AnsiString),#0);
    for AProps:=0 to FStringProps-1 do FStringVals^[AProps]:=sNotSet;
  end;

Function TPropertyCollector.GetStringVal
   (
   AIndex          : Integer
   )
   : AnsiString;
  begin
    if AIndex>=FStringProps then Result:=sNoChange
    else Result:=FStringVals^[AIndex];
  end;

Procedure TPropertyCollector.SetStringVal
   (
   AIndex          : Integer;
   Const AValue    : AnsiString
   );
  begin
    if AIndex<FStringProps then if CollectString(FStringVals^[AIndex],AValue) then
        GetStringOptionList(AIndex).Options:=[soNoChange];
  end;

Function TPropertyCollector.GetStringOptionList
   (
   AIndex          : Integer
   )
   : TSelectOptionList;
  begin
    Result:=GetOptionList(FOptionProps+FDoubleProps+FIntProps+FColorProps+FBooleanProps+AIndex);
  end;

Function TPropertyCollector.ValidValues
   : Boolean;
  var Cnt          : Integer; 
  begin
    Result:=FALSE;
    for Cnt:=0 to FIntProps-1 do if FIntVals^[Cnt]<=intNoChange then Exit;
    for Cnt:=0 to FDoubleProps-1 do if DblCompare(FDoubleVals^[Cnt],dblNoChange)<=0 then Exit;
    for Cnt:=0 to FColorProps-1 do if FColorVals^[Cnt]<=clNoChange then Exit;
    for Cnt:=0 to FBooleanProps-1 do if FBooleanVals^[Cnt]<=bsNoChange then Exit;
    for Cnt:=0 to FStringProps-1 do if FStringVals^[Cnt][1]<=sNoChange[1] then Exit;
    Result:=TRUE;
  end;

{==============================================================================}
{ TSelectOptionList                                                            }
{==============================================================================}

Function TSelectOptionList.GetItem
   (
   AIndex          : Integer
   )
   : Byte;
  begin
    Result:=FItems[AIndex];
  end;

Procedure TSelectOptionList.SetOptions
   (
   AOptions        : TSelectOptions
   );
  var Cnt          : Byte;
  begin
    if PropertyMode=pmSet then FOptions:=AOptions
    else FOptions:=FOptions+AOptions;
    FCount:=0;
    for Cnt:=0 to 255 do if Cnt in FOptions then begin
      FItems[FCount]:=Cnt;
      Inc(FCount);
    end;
  end;

Function TSelectOptionList.IndexOf
   (
   AOption         : Byte
   )
   : Integer;
  var Cnt          : Integer;
  begin
    for Cnt:=0 to FCount-1 do if FItems[Cnt]=AOption then begin
      Result:=Cnt;
      Exit;
    end;
    Result:=-1;
  end;
                                                                   
Function TSelectOptionList.IndexOfValue
   (
   AValue          : LongInt 
   )
   : Integer;
  var Cnt          : Integer;
  begin
    for Cnt:=0 to FCount-1 do if Values[Cnt]=AValue then begin
      Result:=Cnt;
      Exit;
    end;
    Result:=-1;
  end;

Function TSelectOptionList.IndexOfDblValue
   (
   Const AValue    : Double
   )
   : Integer;
  var Cnt          : Integer;
  begin
    for Cnt:=0 to FCount-1 do if DblValues[Cnt]=AValue then begin
      Result:=Cnt;
      Exit;
    end;
    Result:=-1;
  end;

Function TSelectOptionList.GetLongNames
   (
   AIndex          : Integer
   )
   : String;
  var LongName     : String;
      ShortName    : String;
      AValue       : Double; 
  begin
    if not Assigned(FGetValues) then Result:=''
    else begin
      FGetValues(FType,FItems[AIndex],LongName,ShortName,AValue);
      Result:=LongName;
    end;  
  end;

Function TSelectOptionList.GetShortNames
   (
   AIndex          : Integer
   )
   : String;
  var LongName     : String;
      ShortName    : String;
      AValue       : Double; 
  begin
    if not Assigned(FGetValues) then Result:=''
    else begin
      FGetValues(FType,FItems[AIndex],LongName,ShortName,AValue);
      Result:=ShortName;
    end;  
  end;

Function TSelectOptionList.GetValues
   (
   AIndex          : Integer
   )
   : LongInt;
  var LongName     : String;
      ShortName    : String;
      AValue       : Double; 
  begin
    if not Assigned(FGetValues) then Result:=0
    else begin
      FGetValues(FType,FItems[AIndex],LongName,ShortName,AValue);
      Result:=Round(LimitToLong(AValue)); // brovak , added condition LimitToLong to AValue for correct work

    end;
  end;

Function TSelectOptionList.GetDblValues
   (
   AIndex          : Integer
   )
   : Double;
  var LongName     : String;
      ShortName    : String;
      AValue       : Double;
  begin
    if not Assigned(FGetValues) then Result:=0
    else begin
      FGetValues(FType,FItems[AIndex],LongName,ShortName,AValue);
      Result:=AValue;
    end;
  end;

Procedure TSelectOptionList.AssignTo
   (
   Dest            : TPersistent
   );
  begin
    inherited AssignTo(Dest);
    with Dest as TSelectOptionList do begin
      Options:=Self.FOptions;
      FType:=Self.FType;
      FGetValues:=Self.FGetValues;
    end;
  end;

Procedure TSelectOptionList.AddLongNamesToList
   (
   AList           : TStrings
   );
  var Cnt          : Integer;
  begin
    for Cnt:=0 to Count-1 do AList.Add(LongNames[Cnt]);
  end;

Procedure TSelectOptionList.AddShortNamesToList
   (
   AList           : TStrings
   );
  var Cnt          : Integer;
  begin
    for Cnt:=0 to Count-1 do AList.Add(ShortNames[Cnt]);
  end;

Procedure TSelectOptionList.SetPropertyMode
   (
   AMode           : TPropertyMode
   );
  begin
    inherited SetPropertyMode(AMode);
    if PropertyMode=pmCollect then FCount:=0;
  end;

  {+++brovak copyed from AM_DEF}
Function TSelectOptionList.LimitToLong
   (
   AValue          : Double
   )
   : LongInt;
  begin
    if AValue>MaxLongInt then LimitToLong:=MaxLongInt
    else if AValue<-MaxLongInt then LimitToLong:=-MaxLongInt
    else LimitToLong:=Round(AValue);
  end;
  {--brovak }
end.
