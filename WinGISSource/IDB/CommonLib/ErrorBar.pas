{**********************************************}
{   TErrorSeries                               }
{   TErrorBarSeries (derived from TBarSeries)  }
{                                              }
{   Copyright (c) 1995-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit ErrorBar;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, TeEngine, Series, TeCanvas;

type
  TErrorSeriesStyle=( essLeft,essRight,essLeftRight,
                      essTop,essBottom,essTopBottom);

  TErrorWidthUnits=(ewuPercent,ewuPixels);

  TCustomErrorSeries = class(TBarSeries)
  private
    { Private declarations }
    FErrorPen        : TChartPen;
    FErrorStyle      : TErrorSeriesStyle;
    FErrorValues     : TChartValueList;
    FErrorWidth      : Integer;
    FErrorWidthUnits : TErrorWidthUnits;
    { internal }
    IDrawBar         : Boolean;
    Function GetErrorValue(Index:Integer):Double;
    Procedure PrepareErrorPen(ValueIndex:Integer);
    Procedure SetErrorStyle(Value:TErrorSeriesStyle);
    Procedure SetErrorValue(Index:Integer; Const Value:Double);
    Procedure SetErrorValues(Value:TChartValueList);
    Procedure SetErrorWidthUnits(Value:TErrorWidthUnits);
    Procedure SetErrorWidth(Value:Integer);
    procedure SetErrorPen(const Value: TChartPen);
  protected
    { Protected declarations }
    Procedure AddSampleValues(NumValues:Integer); override;
    Procedure CalcHorizMargins(Var LeftMargin,RightMargin:Integer); override;
    Procedure CalcVerticalMargins(Var TopMargin,BottomMargin:Integer); override;
    Procedure DrawError(X,Y,AWidth,AHeight:Integer; Draw3D:Boolean);
    Procedure DrawLegendShape(ValueIndex:Integer; Const Rect:TRect); override;
    class Function GetEditorClass:String; override;
    Procedure PrepareForGallery(IsEnabled:Boolean); override;
    Procedure SetSeriesColor(AColor:TColor); override;
  public
    { Public declarations }
    Constructor Create(AOwner: TComponent); override;
    Destructor Destroy; override;

    Function AddErrorBar(Const AX,AY,AError:Double;
                         Const AXLabel:String{$IFDEF D4}=''{$ENDIF};
                         AColor:TColor{$IFDEF D4}=clTeeColor{$ENDIF}):Integer;
    Procedure Assign(Source:TPersistent); override;
    Procedure DrawBar(BarIndex,StartPos,EndPos:Integer); override;
    Function MinYValue:Double; override;
    Function MaxYValue:Double; override;
    property ErrorValue[Index:Integer]:Double read GetErrorValue
                                              write SetErrorValue;

    { To be published declarations }
    property ErrorPen:TChartPen read FErrorPen write SetErrorPen;
    property ErrorStyle:TErrorSeriesStyle read FErrorStyle write SetErrorStyle
                                          default essTopBottom;
    property ErrorValues:TChartValueList read FErrorValues write SetErrorValues;
    property ErrorWidth:Integer read FErrorWidth write SetErrorWidth default 100;
    property ErrorWidthUnits:TErrorWidthUnits read FErrorWidthUnits
                                              write SetErrorWidthUnits default ewuPercent;
  end;

  TErrorSeries=class(TCustomErrorSeries)
  published
    property ErrorPen;
    property ErrorStyle;
    property ErrorValues;
    property ErrorWidth;
    property ErrorWidthUnits;
  end;

  TErrorBarSeries=class(TCustomErrorSeries)
  protected
    Procedure PrepareForGallery(IsEnabled:Boolean); override;
  public
    Constructor Create(AOwner:TComponent); override;
  published
    property ErrorPen;
    property ErrorValues;
    property ErrorWidth;
    property ErrorWidthUnits;
  end;

  THighLowSeries=class(TChartSeries)
  private
    FHighPen : TChartPen;
    FLow     : TChartValueList;
    FLowPen  : TChartPen;
    OldX     : Integer;
    OldY0    : Integer;
    OldY1    : Integer;
    FLowBrush: TChartBrush;
    function GetHigh: TChartValueList;
    procedure SetHigh(const Value: TChartValueList);
    procedure SetHighPen(const Value: TChartPen);
    procedure SetLow(const Value: TChartValueList);
    procedure SetLowPen(const Value: TChartPen);
    function GetHighBrush: TChartBrush;
    procedure SetHighBrush(const Value: TChartBrush);
    procedure SetLowBrush(const Value: TChartBrush);
  protected
    Procedure AddSampleValues(NumValues:Integer); override;
    class Procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); override;
    class Function GetEditorClass:String; override;
    class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); override;
  public
    Constructor Create(AOwner: TComponent); override;

    Procedure Assign(Source:TPersistent); override;

    Function AddHighLow(Const AX,AHigh,ALow:Double;
                        Const AXLabel:String{$IFDEF D4}=''{$ENDIF};
                         AColor:TColor{$IFDEF D4}=clTeeColor{$ENDIF}):Integer;
    Procedure DrawValue(ValueIndex:Integer); override;
    Function IsValidSourceOf(Value:TChartSeries):Boolean; override;
    Function MaxYValue:Double; override;
    Function MinYValue:Double; override;
    destructor Destroy; override;
  published
    property HighBrush:TChartBrush read GetHighBrush write SetHighBrush;
    property HighPen:TChartPen read FHighPen write SetHighPen;
    property HighValues:TChartValueList read GetHigh write SetHigh;
    property LowBrush:TChartBrush read FLowBrush write SetLowBrush;
    property LowPen:TChartPen read FLowPen write SetLowPen;
    property LowValues:TChartValueList read FLow write SetLow;
    property Pen;

    property Active;
    property ColorEachPoint;
    property ColorSource;
    property Cursor;
    property Depth;
    property HorizAxis;
    property Marks;
    property ParentChart;
    { datasource below parentchart }
    property DataSource;
    property PercentFormat;
    property SeriesColor;
    property ShowInLegend;
    property Title;
    property ValueFormat;
    property VertAxis;
    property XLabelsSource;

    { events }
    property AfterDrawValues;
    property BeforeDrawValues;
    property OnAfterAdd;
    property OnBeforeAdd;
    property OnClearValues;
    property OnClick;
    property OnDblClick;
    property OnGetMarkText;
  end;

implementation

Uses Chart, TeeProCo, TeeConst;

{ TCustomErrorSeries }
Constructor TCustomErrorSeries.Create(AOwner: TComponent);
Begin
  inherited;
  IDrawBar:=False;
  FErrorPen:=CreateChartPen;
  FErrorValues:=TChartValueList.Create(Self,TeeMsg_ValuesStdError); { <-- Std Error storage }
  FErrorStyle:=essTopBottom;
  FErrorWidth:=100;
  FErrorWidthUnits:=ewuPercent;
  Marks.Visible:=False;
end;

Destructor TCustomErrorSeries.Destroy;
begin
  FErrorPen.Free;
  inherited;
end;

Procedure TCustomErrorSeries.CalcHorizMargins(Var LeftMargin,RightMargin:Integer);
begin
  inherited;
  if (FErrorStyle=essLeft) or (FErrorStyle=essLeftRight) then
     LeftMargin  :=MaxLong(LeftMargin, ErrorPen.Width);
  if (FErrorStyle=essRight) or (FErrorStyle=essLeftRight) then
     RightMargin :=MaxLong(RightMargin, ErrorPen.Width);
end;

Procedure TCustomErrorSeries.CalcVerticalMargins(Var TopMargin,BottomMargin:Integer);
begin
  inherited;
  if (FErrorStyle=essTop) or (FErrorStyle=essTopBottom) then
     TopMargin    :=MaxLong(TopMargin, ErrorPen.Width);
  if (FErrorStyle=essBottom) or (FErrorStyle=essTopBottom) then
     BottomMargin :=MaxLong(BottomMargin, ErrorPen.Width);
end;

Procedure TCustomErrorSeries.SetErrorPen(Const Value:TChartPen);
Begin
  FErrorPen.Assign(Value);
  if not IDrawBar then SeriesColor:=FErrorPen.Color;
End;

Procedure TCustomErrorSeries.PrepareErrorPen(ValueIndex:Integer);
begin
  With ParentChart.Canvas do
  begin
    if (ValueIndex<>TeeAllValues) and (not IDrawBar) then
       AssignVisiblePenColor(ErrorPen,ValueColor[ValueIndex])
    else
       AssignVisiblePen(ErrorPen);

    BackMode:=cbmTransparent;
  end;
end;

Procedure TCustomErrorSeries.SetErrorWidth(Value:Integer);
Begin
  SetIntegerProperty(FErrorWidth,Value);
End;

Procedure TCustomErrorSeries.DrawError(X,Y,AWidth,AHeight:Integer; Draw3D:Boolean);

  Procedure DrawHoriz(XPos:Integer);
  begin
    With ParentChart.Canvas do
    begin
      if Draw3D then
      begin
        HorizLine3D(X,XPos,Y,MiddleZ);
        VertLine3D(XPos,Y-AHeight,Y+AHeight,MiddleZ);
      end
      else
      begin
        DoHorizLine(X,XPos,Y);
        DoVertLine(XPos,Y-AHeight,Y+AHeight);
      end;
    end;
  end;

  Procedure DrawVert(YPos:Integer);
  begin
    With ParentChart.Canvas do
    begin
      if Draw3D then
      begin
        VertLine3D(X,Y,YPos,MiddleZ);
        HorizLine3D(X-(AWidth div 2),X+(AWidth div 2),YPos,MiddleZ);
      end
      else
      begin
        DoVertLine(X,Y,YPos);
        DoHorizLine(X-(AWidth div 2),X+(AWidth div 2),YPos);
      end;
    end;
  end;

begin
  Case FErrorStyle of
    essLeft     : DrawHoriz(X-(AWidth div 2));
    essRight    : DrawHoriz(X+(AWidth div 2));
    essLeftRight: begin
                    DrawHoriz(X-(AWidth div 2));
                    DrawHoriz(X+(AWidth div 2));
                  end;
    essTop      : DrawVert(Y-AHeight);
    essBottom   : DrawVert(Y+AHeight);
    essTopBottom: begin
                    DrawVert(Y-AHeight);
                    DrawVert(Y+AHeight);
                  end;
  end;
end;

Procedure TCustomErrorSeries.DrawBar(BarIndex,StartPos,EndPos:Integer);
Var tmp         : Integer;
    tmpWidth    : Integer;
    tmpBarWidth : Integer;
    ErrorTop    : Integer;
    tmpError    : Double;
Begin
  if IDrawBar then inherited;
  if ErrorPen.Visible then
  Begin
    tmpError:=FErrorValues.Value[BarIndex];
    if tmpError<>0 then
    Begin
      tmpBarWidth:=BarBounds.Right-BarBounds.Left;

      if FErrorWidth=0 then tmpWidth:=tmpBarWidth
      else
      if FErrorWidthUnits=ewuPercent then
         tmpWidth:=Round(1.0*FErrorWidth*tmpBarWidth*0.01)
      else
         tmpWidth:=FErrorWidth;

      if IDrawBar and (YValues.Value[BarIndex]<YOrigin) then tmpError:=-tmpError;
      ErrorTop:=CalcYPosValue(YValues.Value[BarIndex]+tmpError);
      tmp:=CalcYPosValue(YValues.Value[BarIndex]);

      PrepareErrorPen(BarIndex);
      DrawError((BarBounds.Right+BarBounds.Left) div 2,tmp,
                 tmpWidth,tmp-ErrorTop,ParentChart.View3D);
    end;
  end;
End;

Procedure TCustomErrorSeries.SetErrorWidthUnits(Value:TErrorWidthUnits);
Begin
  if FErrorWidthUnits<>Value then
  Begin
    FErrorWidthUnits:=Value;
    Repaint;
  end;
end;

Procedure TCustomErrorSeries.SetErrorStyle(Value:TErrorSeriesStyle);
begin
  if FErrorStyle<>Value then
  begin
    FErrorStyle:=Value;
    Repaint;
  end;
end;

Procedure TCustomErrorSeries.SetErrorValues(Value:TChartValueList);
Begin
  SetChartValueList(FErrorValues,Value); { standard method }
End;

Function TCustomErrorSeries.AddErrorBar( Const AX,AY,AError:Double;
                                         Const AXLabel:String;
                                         AColor:TColor):Integer;
Begin
  FErrorValues.TempValue:=AError;
  result:=AddXY(AX,AY,AXLabel,AColor);
End;

Procedure TCustomErrorSeries.AddSampleValues(NumValues:Integer);
Var t : Integer;
Begin
  with RandomBounds(NumValues) do
  for t:=1 to NumValues do
  Begin
    AddErrorBar( tmpX,
                 System.Random(Round(DifY)),
                 DifY/(20+System.Random(4))
                 {$IFNDEF D4},'', clTeeColor{$ENDIF});
    tmpX:=tmpX+StepX;
  end;
end;

Function TCustomErrorSeries.MaxYValue:Double;
Var t      : Integer;
    tmp    : Double;
    tmpErr : Double;
Begin
  if IDrawBar then result:=inherited MaxYValue else result:=0;
  for t:=0 to Count-1 do
  if IDrawBar then
  Begin
    tmpErr:=FErrorValues.Value[t];
    tmp:=YValues.Value[t];
    if tmp<0 then tmp:=tmp-tmpErr else tmp:=tmp+tmpErr;
    if tmp>result then result:=tmp;
  end
  else
  begin
    tmp:=YValues.Value[t]+FErrorValues.Value[t];
    if t=0 then result:=tmp else result:=MaxDouble(result,tmp);
  end;
End;

Function TCustomErrorSeries.MinYValue:Double;
Var t      : Integer;
    tmp    : Double;
    tmpErr : Double;
Begin
  if IDrawBar then result:=inherited MinYValue else result:=0;
  for t:=0 to Count-1 do
  if IDrawBar then
  Begin
    tmpErr:=FErrorValues.Value[t];
    tmp:=YValues.Value[t];
    if tmp<0 then tmp:=tmp-tmpErr else tmp:=tmp+tmpErr;
    if tmp<result then result:=tmp;
  end
  else
  begin
    tmp:=YValues.Value[t]-FErrorValues.Value[t];
    if t=0 then result:=tmp else result:=MinDouble(result,tmp);
  end;
End;

Function TCustomErrorSeries.GetErrorValue(Index:Integer):Double;
Begin
  result:=FErrorValues.Value[Index];
End;

Procedure TCustomErrorSeries.SetErrorValue(Index:Integer; Const Value:Double);
Begin
  FErrorValues.Value[Index]:=Value;
End;

class Function TCustomErrorSeries.GetEditorClass:String;
Begin
  result:='TErrorSeriesEditor';
end;

Procedure TCustomErrorSeries.Assign(Source:TPersistent);
begin
  if Source is TCustomErrorSeries then
  With TCustomErrorSeries(Source) do
  begin
    Self.FErrorPen       :=FErrorPen;
    Self.FErrorStyle     :=FErrorStyle;
    Self.FErrorWidth     :=FErrorWidth;
    Self.FErrorWidthUnits:=FErrorWidthUnits;
  end;
  inherited;
end;

Procedure TCustomErrorSeries.PrepareForGallery(IsEnabled:Boolean);
Const Colors:Array[Boolean] of TColor=(clSilver,clBlue);
      ErrorColors:Array[Boolean] of TColor=(clWhite,clRed);
begin
  inherited;
  ErrorPen.Color:=ErrorColors[IsEnabled];
  SeriesColor:=Colors[IsEnabled];
end;

Procedure TCustomErrorSeries.SetSeriesColor(AColor:TColor);
begin
  inherited;
  if not IDrawBar then ErrorPen.Color:=AColor;
end;

Procedure TCustomErrorSeries.DrawLegendShape(ValueIndex:Integer; Const Rect:TRect);
begin
  PrepareErrorPen(ValueIndex);
  With Rect do
    DrawError( (Left+Right) shr 1,(Top+Bottom) shr 1,
               Right-Left,((Bottom-Top) div 2)-1,False);
end;

{ TErrorBarSeries }
Constructor TErrorBarSeries.Create(AOwner: TComponent);
Begin
  inherited;
  IDrawBar:=True;
  FErrorStyle:=essTop;
end;

Procedure TErrorBarSeries.PrepareForGallery(IsEnabled:Boolean);
begin
  ErrorPen.Width:=2;
  inherited;
end;

{ THighLowSeries }
Constructor THighLowSeries.Create(AOwner: TComponent);
begin
  inherited;
  CalcVisiblePoints:=False;
  Pen.Color:=clTeeColor;
  FLow:=TChartValueList.Create(Self,TeeMsg_ValuesLow);
  FHighPen:=CreateChartPen;
  FLowPen:=CreateChartPen;
  FLowBrush:=TChartBrush.Create(CanvasChanged);
  LowBrush.Style:=bsClear;
  HighBrush.Style:=bsClear;
end;

Destructor THighLowSeries.Destroy;
begin
  FHighPen.Free;
  FLowPen.Free;
  FLowBrush.Free;
  inherited;
end;

function THighLowSeries.AddHighLow(const AX, AHigh, ALow: Double;
                                   Const AXLabel:String{$IFDEF D4}=''{$ENDIF};
                          AColor:TColor{$IFDEF D4}=clTeeColor{$ENDIF}): Integer;
begin
  FLow.TempValue:=ALow;
  result:=AddXY(AX,AHigh{$IFNDEF D4},'',clTeeColor{$ENDIF});
end;

procedure THighLowSeries.Assign(Source: TPersistent);
begin
  if Source is THighLowSeries then
  With THighLowSeries(Source) do
  begin
    Self.HighPen :=HighPen;
    Self.LowPen  :=LowPen;
    Self.LowBrush:=LowBrush;
  end;
  inherited;
end;

procedure THighLowSeries.DrawValue(ValueIndex: Integer);
Var X : Integer;

  Procedure DrawLine(APen:TChartPen; BeginY,EndY:Integer);
  begin
    if APen.Visible then
    With ParentChart,Canvas do
    begin
      AssignVisiblePen(APen);
      if View3D then
      begin
        MoveTo3D(OldX,BeginY,MiddleZ);
        LineTo3D(X,EndY,MiddleZ);
      end
      else
      begin
        MoveTo(OldX,BeginY);
        LineTo(X,EndY);
      end;
    end;
  end;

var Y0   : Integer;
    Y1   : Integer;
    tmp  : TColor;
    tmpX : Integer;
begin
  With ParentChart,Canvas do
  begin
    x:=CalcXPos(ValueIndex);
    y0:=CalcYPos(ValueIndex);
    y1:=CalcYPosValue(FLow.Value[ValueIndex]);
    if ValueIndex<>FirstValueIndex then
    begin
      if FLow.Value[ValueIndex]<HighValues.Value[ValueIndex] then
         AssignBrush(Self.HighBrush,ValueColor[ValueIndex])
      else
         AssignBrush(Self.LowBrush,ValueColor[ValueIndex]);
      if Brush.Style<>bsClear then
      begin
        Pen.Style:=psClear;
        tmpX:=OldX;
        if Self.Pen.Visible then Inc(tmpX,Self.Pen.Width);
        PlaneWithZ(Point(tmpX,OldY0),Point(tmpX,OldY1),Point(X,Y1),Point(X,Y0),MiddleZ);
      end;
      DrawLine(HighPen,OldY0,Y0);
      DrawLine(LowPen,OldY1,Y1);
    end;
    if Self.Pen.Visible then
    begin
      tmp:=Self.Pen.Color;
      if tmp=clTeeColor then tmp:=ValueColor[ValueIndex];
      AssignVisiblePenColor(Self.Pen,tmp);
      if View3D then VertLine3D(x,y0,y1,MiddleZ)
                else DoVertLine(x,y0,y1);
    end;
  end;
  OldX:=X;
  OldY0:=Y0;
  OldY1:=Y1;
end;

function THighLowSeries.GetHigh: TChartValueList;
begin
  result:=YValues;
end;

procedure THighLowSeries.SetHigh(const Value: TChartValueList);
begin
  SetChartValueList(YValues,Value);
end;

procedure THighLowSeries.SetLow(const Value: TChartValueList);
begin
  FLow.Assign(Value);
end;

Function THighLowSeries.IsValidSourceOf(Value:TChartSeries):Boolean;
begin
  result:=Value is THighLowSeries;
end;

Function THighLowSeries.MaxYValue:Double;
Begin
  result:=MaxDouble(inherited MaxYValue,FLow.MaxValue);
End;

Function THighLowSeries.MinYValue:Double;
Begin
  result:=MinDouble(inherited MinYValue,FLow.MinValue);
End;

procedure THighLowSeries.SetHighPen(const Value: TChartPen);
begin
  FHighPen.Assign(Value);
end;

procedure THighLowSeries.SetLowPen(const Value: TChartPen);
begin
  FLowPen.Assign(Value);
end;

class function THighLowSeries.GetEditorClass: String;
begin
  result:='THighLowEditor';
end;

function THighLowSeries.GetHighBrush: TChartBrush;
begin
  result:=Brush;
end;

procedure THighLowSeries.SetHighBrush(const Value: TChartBrush);
begin
  Brush:=Value;
end;

procedure THighLowSeries.SetLowBrush(const Value: TChartBrush);
begin
  FLowBrush.Assign(Value);
end;

procedure THighLowSeries.AddSampleValues(NumValues: Integer);
Var t   : Integer;
    tmp : Double;
Begin
  With RandomBounds(NumValues) do
  begin
    tmp:=System.Random(Round(DifY));
    for t:=1 to NumValues do
    Begin
      tmp:=tmp+System.Random(Round(DifY/5.0))-(DifY/10.0);
      AddHighLow(tmpX,{ X }
                 tmp, { High }
                 tmp-System.Random(Round(DifY/5.0)) { Low }
                 {$IFNDEF D4},'', clTeeColor{$ENDIF});
      tmpX:=tmpX+StepX;
    end;
  end;
end;

class procedure THighLowSeries.CreateSubGallery(
  AddSubChart: TChartSubGalleryProc);
begin
  inherited;
  AddSubChart(TeeMsg_Filled);
  AddSubChart(TeeMsg_NoLines);
  AddSubChart(TeeMsg_NoHigh);
  AddSubChart(TeeMsg_NoLow);
end;

class procedure THighLowSeries.SetSubGallery(ASeries: TChartSeries;
  Index: Integer);
begin
  With THighLowSeries(ASeries) do
  Case Index of
    1: Brush.Style:=bsSolid;
    2: Pen.Visible:=False;
    3: HighPen.Visible:=False;
    4: LowPen.Visible:=False;
  else inherited;
  end;
end;

initialization
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{
   RegisterTeeSeries(TErrorBarSeries,TeeMsg_GalleryErrorBar,TeeMsg_GalleryStats,1);
   RegisterTeeSeries(TErrorSeries,TeeMsg_GalleryError,TeeMsg_GalleryStats,1);
   RegisterTeeSeries(THighLowSeries,TeeMsg_GalleryHighLow,TeeMsg_GalleryStats,1);
}
   RegisterTeeSeries(TErrorBarSeries,TeeMsg_GalleryErrorBar,TeeMsg_GalleryStats,1,stErrorBar);
   RegisterTeeSeries(TErrorSeries,TeeMsg_GalleryError,TeeMsg_GalleryStats,1,stError);
   RegisterTeeSeries(THighLowSeries,TeeMsg_GalleryHighLow,TeeMsg_GalleryStats,1,stHighLow);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
finalization
  UnRegisterTeeSeries( [TErrorBarSeries,TErrorSeries,THighLowSeries]);
end.
