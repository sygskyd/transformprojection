{*********************************************}
{ TeeChart Delphi Component Library           }
{    Polar and Radar Series Components        }
{ Copyright (c) 1995-2000 by David Berneda    }
{ All rights reserved                         }
{*********************************************}
{$I teedefs.inc}
unit TeePolar;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     {$IFNDEF CLX}
     Windows, Messages,
     {$ENDIF}
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, Types,
     {$ELSE}
     Graphics, Controls, Forms,
     {$ENDIF}
     TeEngine, Chart, Series, TeCanvas;

Const TeePolarDegreeSymbol : Char = '�';

type
  TCustomPolarSeries = class(TCircledSeries)
  private
    FCircleLabels     : Boolean;
    FCircleLabelsFont : TTeeFont;
    FCircleLabelsInside : Boolean;
    FCircleLabelsRot  : Boolean;
    FCirclePen        : TChartPen;
    FClockWiseLabels  : Boolean;
    FCloseCircle      : Boolean;
    FPointer          : TSeriesPointer;

    { Private declarations }
    OldX              : Integer;
    OldY              : Integer;
    IMaxValuesCount   : Integer;
    Procedure CalcXYPos( ValueIndex:Integer;
                         Const ARadius:Double; Var X,Y:Integer);
    Procedure DrawAxis;
    Procedure FillTriangle(Const A,B:TPoint; Z:Integer);
    Function GetAngleIncrement:Double;
    Function GetAngleValues:TChartValueList;
    Function GetRadiusIncrement:Double;
    Function GetRadiusValues:TChartValueList;
    Function IsRadiusStored:Boolean;
    Procedure SetAngleIncrement(Const Value:Double);
    Procedure SetAngleValues(Value:TChartValueList);
    Procedure SetCircleLabels(Value:Boolean);
    Procedure SetCircleLabelsFont(Value:TTeeFont);
    procedure SetCircleLabelsInside(const Value: Boolean);
    Procedure SetCirclePen(Value:TChartPen);
    procedure SetClockWiseLabels(const Value: Boolean);
    Procedure SetCloseCircle(Value:Boolean);
    Procedure SetLabelsRotated(Value:Boolean);
    Procedure SetPointer(Value:TSeriesPointer);
    Procedure SetRadiusIncrement(Const Value:Double);
    Procedure SetRadiusValues(Value:TChartValueList);
  protected
    { Protected declarations }
    class Procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); override;
    Procedure DoAfterDrawValues; override;
    Procedure DoBeforeDrawValues; override;
    Procedure DrawAllValues; override;
    Procedure DrawLegendShape(ValueIndex:Integer; Const Rect:TRect); override;
    Procedure DrawMark( ValueIndex:Integer; Const St:String;
                        APosition:TSeriesMarkPosition); override;
    Procedure DrawPolarCircle(HalfWidth,HalfHeight,Z:Integer);
    procedure DrawValue(ValueIndex:Integer); override;
    Function  GetCircleLabel(Const Angle:Double; Index:Integer):String; virtual;
    class Function  GetEditorClass:String; override;
    procedure LinePrepareCanvas(ValueIndex:Integer);
    Procedure PrepareForGallery(IsEnabled:Boolean); override;
    Procedure SetParentChart(Const Value:TCustomAxisPanel); override;
    Procedure SetSeriesColor(AColor:TColor); override;
    class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); override;
  public
    { Public declarations }
    Constructor Create(AOwner: TComponent); override;
    Destructor Destroy; override;

    Procedure Assign(Source:TPersistent); override;
    Function CalcXPos(ValueIndex:Integer):Integer; override;
    Function CalcYPos(ValueIndex:Integer):Integer; override;
    Function Clicked(x,y:Integer):Integer; override;
    Procedure DrawRing(Const Value:Double; Z:Integer);

    { to be Published declarations }
    property AngleIncrement:Double read GetAngleIncrement write SetAngleIncrement;
    property AngleValues:TChartValueList read GetAngleValues write SetAngleValues;
    property Brush;
    property CircleLabels:Boolean read FCircleLabels write SetCircleLabels default False;
    property CircleLabelsFont:TTeeFont read FCircleLabelsFont write SetCircleLabelsFont;
    property CircleLabelsInside:Boolean read FCircleLabelsInside write SetCircleLabelsInside default False;
    property CircleLabelsRotated:Boolean read FCircleLabelsRot
                                         write SetLabelsRotated default False;
    property CirclePen:TChartPen read FCirclePen write SetCirclePen;
    property ClockWiseLabels:Boolean read FClockWiseLabels write SetClockWiseLabels default False;
    property CloseCircle:Boolean read FCloseCircle
                                 write SetCloseCircle default True;
    property Pen;
    property Pointer:TSeriesPointer read FPointer write SetPointer;
    property RadiusIncrement:Double read GetRadiusIncrement write SetRadiusIncrement;
    property RadiusValues:TChartValueList read GetRadiusValues write SetRadiusValues
                                          stored IsRadiusStored;
  end;

  TPolarSeries = class(TCustomPolarSeries)
  protected
    Procedure AddSampleValues(NumValues:Integer); override;
  public
    Function AddPolar( Const Angle,Value:Double;
                       Const ALabel:String{$IFDEF D4}=''{$ENDIF};
                       AColor:TColor{$IFDEF D4}=clTeeColor{$ENDIF}):Integer;
  published
    { Published declarations }
    property Active;
    property ColorEachPoint;
    property HorizAxis;
    property SeriesColor;
    property VertAxis;

    property AngleIncrement;
    property AngleValues;
    property Brush;
    property CircleBackColor;
    property CircleLabels;
    property CircleLabelsFont;
    property CircleLabelsInside;
    property CircleLabelsRotated;
    property CirclePen;
    property ClockWiseLabels;
    property CloseCircle;
    property Pen;
    property Pointer;
    property RadiusIncrement;
    property RadiusValues;
    property RotationAngle;
  end;

  TRadarSeries=class(TCustomPolarSeries)
  protected
    Procedure AddSampleValues(NumValues:Integer); override;
    Procedure DoBeforeDrawChart; override;
    Function GetCircleLabel(Const Angle:Double; Index:Integer):String; override;
    Procedure PrepareForGallery(IsEnabled:Boolean); override;
  public
    Function NumSampleValues:Integer; override;
  published
    { Published declarations }
    property Active;
    property ColorEachPoint;
    property HorizAxis;
    property SeriesColor;
    property VertAxis;

    property Brush;
    property CircleBackColor;
    property CircleLabels;
    property CircleLabelsFont;
    property CircleLabelsRotated;
    property CirclePen;
    property CloseCircle;
    property Pen;
    property Pointer;
    property RadiusIncrement;
    property RadiusValues;
  end;

implementation

Uses TeeProCo, TeeConst, TeeProcs;

Const TeeMsg_RadiusValues='Radius'; { <-- dont translate ! }

{ TCustomPolarSeries }
Constructor TCustomPolarSeries.Create(AOwner: TComponent);
begin
  inherited;
  IMaxValuesCount:=0;
  FClockWiseLabels:=False;
  FCloseCircle := True;
  FPointer     := TSeriesPointer.Create(Self);
  YValues.Name := TeeMsg_RadiusValues;
  Brush.Style  :=bsClear;
  FCircleLabels:= False;
  FCircleLabelsRot:=False;
  FCircleLabelsInside:=False;
  FCircleLabelsFont:=TTeeFont.Create(CanvasChanged);
  FCirclePen:=CreateChartPen;
end;

Destructor TCustomPolarSeries.Destroy;
Begin
  FCircleLabelsFont.Free;
  FCirclePen.Free;
  FreeAndNil(FPointer);
  inherited;
end;

type TPointerAccess=class(TSeriesPointer);

Procedure TCustomPolarSeries.SetParentChart(Const Value:TCustomAxisPanel);
Begin
  inherited;
  if Assigned(FPointer) then Pointer.ParentChart:=Value;
//  if Assigned(Value) then TPointerAccess(Pointer).Canvas:=Value.Canvas;
  if Assigned(ParentChart) and (csDesigning in ComponentState) then
     ParentChart.View3D:=False;
end;

Procedure TCustomPolarSeries.SetCloseCircle(Value:Boolean);
begin
  SetBooleanProperty(FCloseCircle,Value);
end;

Procedure TCustomPolarSeries.SetLabelsRotated(Value:Boolean);
begin
  SetBooleanProperty(FCircleLabelsRot,Value);
end;

Procedure TCustomPolarSeries.DrawPolarCircle(HalfWidth,HalfHeight,Z:Integer);
var OldP : TPoint;
    P    : TPoint;
    tmp  : Double;
    t    : Integer;
begin
  With ParentChart.Canvas do
  begin
    if IMaxValuesCount=0 then
       EllipseWithZ( CircleXCenter-HalfWidth,CircleYCenter-HalfHeight,
                     CircleXCenter+HalfWidth,CircleYCenter+HalfHeight, Z)
    else
    begin
      tmp:=piDegree*360.0/IMaxValuesCount;
      AngleToPos(0,HalfWidth,HalfHeight,OldP.X,OldP.Y);
      MoveTo3D(OldP.X,OldP.Y,Z);
      for t:=0 to IMaxValuesCount do
      begin
        AngleToPos(t*tmp,HalfWidth,HalfHeight,P.X,P.Y);
        if Brush.Style<>bsClear then FillTriangle(OldP,P,Z);
        LineTo3D(P.X,P.Y,Z);
        OldP:=P;
      end;
    end;
  end;
end;

Function TCustomPolarSeries.GetCircleLabel(Const Angle:Double; Index:Integer):String;
var tmp : Double;
begin
  if FClockWiseLabels then tmp:=360-Angle else tmp:=Angle;
  if tmp=360 then tmp:=0;
  result:=FloatToStr(tmp)+TeePolarDegreeSymbol;
end;

type TAxisAccess=class(TChartAxis);

Procedure TCustomPolarSeries.DrawAxis;
var tmpValue     : Double;
    tmpIncrement : Double;

  Procedure SetGridCanvas(Axis:TChartAxis);
  Begin
    with ParentChart,Canvas do
    begin
      Brush.Style:=bsClear;
      BackMode:=cbmTransparent;
      AssignVisiblePen(Axis.Grid);
      CheckPenWidth(Pen);
      if Pen.Color=clTeeColor then Pen.Color:=clGray;
    end;
  end;

  Procedure DrawYGrid;

    Procedure InternalDrawGrid;
    begin
      DrawRing(tmpValue,EndZ);
      tmpValue:=tmpValue-tmpIncrement;
    end;

  Begin
    With GetVertAxis do
    if Grid.Visible then
    begin
      tmpIncrement:=CalcIncrement;
      if tmpIncrement>0 then
      begin
        SetGridCanvas(GetVertAxis);
        tmpValue:=Maximum/tmpIncrement;
        if (Abs(tmpValue)<MaxLongint) and
           (Abs((Maximum-Minimum)/tmpIncrement)<10000) then
        Begin
          if RoundFirstLabel then tmpValue:=tmpIncrement*Trunc(tmpValue)
                             else tmpValue:=Maximum;
          if LabelsOnAxis then
          begin
            While tmpValue>Maximum do tmpValue:=tmpValue-tmpIncrement;
            While tmpValue>=Minimum do InternalDrawGrid;
          end
          else
          begin
            While tmpValue>=Maximum do tmpValue:=tmpValue-tmpIncrement;
            While tmpValue>Minimum do InternalDrawGrid;
          end;
        end;
      end;
    end;
  end;

  Procedure DrawXGrid;

    Procedure DrawAngleLabel(Angle:Double; AIndex:Integer);
    var X         : Integer;
        Y         : Integer;
        tmpHeight : Integer;
        tmpWidth  : Integer;
        tmp       : Double;
        tmp2      : Double;
        tmpSt     : String;
        tmpXRad   : Integer;
        tmpYRad   : Integer;
    begin
      With ParentChart.Canvas do
      begin
        AssignFont(FCircleLabelsFont);
        tmpHeight:=FontHeight;
        if Angle>=360 then Angle:=Angle-360;
        tmp:=Angle;
        if FCircleLabelsRot then
        begin
          if (tmp>90) and (tmp<270) then tmp2:=Angle-3
                                    else tmp2:=Angle+2;
        end
        else tmp2:=Angle;

        tmpSt:=GetCircleLabel(tmp,AIndex);

        tmpXRad:=XRadius;
        tmpYRad:=YRadius;

        if FCircleLabelsInside then
        begin
          Dec(tmpXRad,TextWidth('   '));
          Dec(tmpYRad,TextHeight(tmpSt));
        end;

        AngleToPos((tmp2)*PiDegree,tmpXRad+2,tmpYRad+2,X,Y);

        Angle:=Angle+RotationAngle;
        if Angle>=360 then Angle:=Angle-360;
        if FCircleLabelsRot then
        begin
          if (Angle>90) and (Angle<270) then
          begin
            TextAlign:=ta_Right;
            Angle:=Angle+180;
          end
          else TextAlign:=ta_Left;
          RotateLabel3D(X,Y,EndZ,tmpSt,Round(Angle));
        end
        else
        begin
          if (Angle=0) or (Angle=180) then Dec(Y,tmpHeight div 2)
          else
          if (Angle>0) and (Angle<180) then Dec(Y,tmpHeight);
          if (Angle=90) or (Angle=270) then TextAlign:=ta_Center
          else
          begin
            if FCircleLabelsInside then
               if (Angle>90) and (Angle<270) then TextAlign:=ta_Left
                                             else TextAlign:=ta_Right
            else
               if (Angle>90) and (Angle<270) then TextAlign:=ta_Right
                                             else TextAlign:=ta_Left;
          end;

          tmpWidth:=TextWidth('0') div 2;

          if Angle=0 then Inc(x,tmpWidth) else
          if Angle=180 then Dec(x,tmpWidth);

          TextOut3D(X,Y,EndZ,tmpSt);
        end;
      end;
    end;

  var tmpX     : Integer;
      tmpY     : Integer;
      tmpIndex : Integer;
  Begin
    With GetHorizAxis do
    if Grid.Visible or Self.FCircleLabels then
    begin
      tmpIncrement:=Increment;
      if tmpIncrement<=0 then tmpIncrement:=10.0;
      SetGridCanvas(GetHorizAxis);
      tmpIndex:=0;
      tmpValue:=0;
      While tmpValue<360 do
      begin
        if FCircleLabels then DrawAngleLabel(tmpValue,tmpIndex);
        if Grid.Visible then
        begin
          AngleToPos(piDegree*tmpValue,XRadius,YRadius,tmpX,tmpY);
          ParentChart.Canvas.LineWithZ(CircleXCenter,CircleYCenter,tmpX,tmpY,EndZ);
        end;
        tmpValue:=tmpValue+tmpIncrement;
        Inc(tmpIndex);
      end;
    end;
  end;

Var tmp : Integer;
begin
  DrawXGrid;
  DrawYGrid;
  With ParentChart do
  if AxisVisible then
  begin
    With TAxisAccess(RightAxis) do
    if Visible then
    begin
      tmp:=CircleXCenter+SizeTickAxis;
      CustomDrawMinMaxStartEnd( tmp,tmp+SizeLabels,CircleXCenter,False,
                                LeftAxis.Minimum,LeftAxis.Maximum,LeftAxis.Increment,
                                CircleYCenter-YRadius,CircleYCenter);
    end;
    if IMaxValuesCount=0 then
    begin
      With TAxisAccess(LeftAxis) do
      if Visible then
      begin
        InternalSetInverted(True);
        tmp:=CircleXCenter-SizeTickAxis;
        CustomDrawStartEnd( tmp,tmp-SizeLabels,CircleXCenter,False,
                            CircleYCenter,CircleYCenter+YRadius);
        InternalSetInverted(False);
      end;

      With TAxisAccess(TopAxis) do
      if Visible then
      begin
        InternalSetInverted(True);
        tmp:=CircleYCenter-SizeTickAxis;
        CustomDrawMinMaxStartEnd( tmp,tmp-SizeLabels,CircleYCenter,False,
                                  LeftAxis.Minimum,LeftAxis.Maximum,LeftAxis.Increment,
                                  CircleXCenter-XRadius,CircleXCenter);
        InternalSetInverted(False);
      end;

      With TAxisAccess(BottomAxis) do
      if Visible then
      begin
        tmp:=CircleYCenter+SizeTickAxis;
        CustomDrawMinMaxStartEnd( tmp,tmp+SizeLabels,CircleYCenter,False,
                                  LeftAxis.Minimum,LeftAxis.Maximum,LeftAxis.Increment,
                                  CircleXCenter,CircleXCenter+XRadius);
      end;
    end;
  end;
end;

Procedure TCustomPolarSeries.DoBeforeDrawValues;

  Procedure DrawCircle;
  begin
    With ParentChart.Canvas do
    Begin
      if CircleBackColor=clTeeColor then Brush.Style:=bsClear
      else
      begin
        Brush.Style:=bsSolid;
        Brush.Color:=CalcCircleBackColor;
      end;
      AssignVisiblePen(CirclePen);
      DrawPolarCircle(CircleWidth div 2,CircleHeight div 2,EndZ);
    end;
  end;

var t   : Integer;
    tmp : Integer;
    First : Boolean;
Begin
  First:=False;
  With ParentChart do
  for t:=0 to SeriesCount-1 do
  if (Series[t].Active) and (Series[t] is Self.ClassType) then
  begin
    if Series[t]=Self then
    begin
      if not First then
      begin
         if FCircleLabels and (not FCircleLabelsInside) then
         begin
           Canvas.AssignFont(FCircleLabelsFont);
           With ChartRect do
           begin
             tmp:=Canvas.FontHeight+2;
             Inc(Top,tmp);
             Dec(Bottom,tmp);
             tmp:=Canvas.TextWidth('360');
             Inc(Left,tmp);
             Dec(Right,tmp);
           end;
         end;
      end;
      break;
    end;
    First:=True;
  end;
  inherited;
  First:=False;
  With ParentChart do
  for t:=0 to SeriesCount-1 do
  if (Series[t].Active) and (Series[t] is Self.ClassType) then
  begin
    if Series[t]=Self then
    begin
      if not First then
      begin
        DrawCircle;
        if ParentChart.AxisBehind then DrawAxis;
      end;
      break;
    end;
    First:=True;
  end;
end;

Procedure TCustomPolarSeries.CalcXYPos( ValueIndex:Integer;
                                        Const ARadius:Double;
                                        Var X,Y:Integer);
var tmp       : Double;
    tmpRadius : Double;
    tmpDif    : Double;
begin
  With GetVertAxis do tmp:=Maximum-Minimum;
  tmpDif:=YValues.Value[ValueIndex]-GetVertAxis.Minimum;
  if (tmp=0) or (tmpDif<0) then
  begin
    X:=CircleXCenter;
    Y:=CircleYCenter;
  end
  else
  begin
    tmpRadius:=tmpDif*ARadius/tmp;
    AngleToPos( piDegree*XValues.Value[ValueIndex],tmpRadius,tmpRadius,X,Y );
  end;
end;

Function TCustomPolarSeries.CalcYPos(ValueIndex:Integer):Integer;
var tmpX : Integer;
begin
  CalcXYPos(ValueIndex,YRadius,tmpX,result);
end;

Function TCustomPolarSeries.CalcXPos(ValueIndex:Integer):Integer;
var tmpY : Integer;
begin
  CalcXYPos(ValueIndex,XRadius,result,tmpY);
end;

procedure TCustomPolarSeries.LinePrepareCanvas(ValueIndex:Integer);
begin
  With ParentChart.Canvas do
  begin
    if Self.Pen.Visible then
    begin
      if ValueIndex=-1 then AssignVisiblePenColor(Self.Pen,SeriesColor)
                       else AssignVisiblePenColor(Self.Pen,ValueColor[ValueIndex]);
    end
    else Pen.Style:=psClear;
    BackMode:=cbmTransparent;
  end;
end;

Procedure TCustomPolarSeries.FillTriangle(Const A,B:TPoint; Z:Integer);
var tmpStyle : TPenStyle;
begin
  With ParentChart.Canvas do
  begin
    tmpStyle:=Pen.Style;
    Pen.Style:=psClear;
    TriangleWithZ(A,B,Point(CircleXCenter,CircleYCenter),Z);
    Pen.Style:=tmpStyle;
  end;
end;

procedure TCustomPolarSeries.DrawValue(ValueIndex:Integer);
var X : Integer;
    Y : Integer;

  Procedure TryFillTriangle;
  begin
    if Brush.Style<>bsClear then
    begin
      if Brush.Color=clTeeColor then
         ParentChart.Canvas.AssignBrushColor(Brush,clBlack,ValueColor[ValueIndex])
      else
         ParentChart.Canvas.AssignBrushColor(Brush,Brush.Color,ValueColor[ValueIndex]);
      FillTriangle(Point(OldX,OldY),Point(X,Y),StartZ);
    end;
  end;

begin
  X:=CalcXPos(ValueIndex);
  Y:=CalcYPos(ValueIndex);
  LinePrepareCanvas(ValueIndex);
  With ParentChart.Canvas do
  if ValueIndex=FirstValueIndex then MoveTo3D(X,Y,StartZ)  { <-- first point }
  else
  begin
    if (X<>OldX) or (Y<>OldY) then
    begin
      TryFillTriangle;
      LineTo3D(X,Y,StartZ);
    end;
    if (ValueIndex=LastValueIndex) and FCloseCircle then
    begin
      Pen.Color:=ValueColor[0];
      OldX:=X;
      OldY:=Y;
      X:=CalcXPos(0);
      Y:=CalcYPos(0);
      TryFillTriangle;
      LineTo3D(X,Y,StartZ);
      X:=OldX;
      Y:=OldY;
    end;
  end;
  OldX:=X;
  OldY:=Y;
end;

Procedure TCustomPolarSeries.DrawAllValues;
var t        : Integer;
    tmpColor : TColor;
begin
  inherited;
  With FPointer do
  if Visible then
    for t:=FirstValueIndex to LastValueIndex do
    begin
      tmpColor:=ValueColor[t];
      TPointerAccess(FPointer).PrepareCanvas(ParentChart.Canvas,tmpColor);
      Draw(CalcXPos(t),CalcYPos(t),tmpColor,Style);
    end;
end;

Procedure TCustomPolarSeries.SetSeriesColor(AColor:TColor);
begin
  inherited;
  Pen.Color:=AColor;
end;

Procedure TCustomPolarSeries.SetPointer(Value:TSeriesPointer);
Begin
  FPointer.Assign(Value);
end;

Procedure TCustomPolarSeries.SetCircleLabels(Value:Boolean);
Begin
  SetBooleanProperty(FCircleLabels,Value);
end;

Procedure TCustomPolarSeries.SetCircleLabelsFont(Value:TTeeFont);
begin
  FCircleLabelsFont.Assign(Value);
end;

class Function TCustomPolarSeries.GetEditorClass:String;
Begin
  result:='TPolarSeriesEditor'; { <-- dont translate ! }
end;

{ The BottomAxis is used in Angle axis }
Function TCustomPolarSeries.GetAngleIncrement:Double;
Const MinAngle = 10;
begin
  if ParentChart=nil then result:=MinAngle
  else
  begin
    result:=GetHorizAxis.Increment;
    if result=0 then result:=MinAngle;
  end;
end;

{ The BottomAxis is used in Angle axis }
Procedure TCustomPolarSeries.SetAngleIncrement(Const Value:Double);
begin
  if Assigned(ParentChart) then GetHorizAxis.Increment:=Value;
end;

{ The LeftAxis is used in Radius axis }
Function TCustomPolarSeries.GetRadiusIncrement:Double;
begin
  if ParentChart=nil then result:=0
                     else result:=GetVertAxis.Increment;
end;

{ The LeftAxis is used in Radius axis }
Procedure TCustomPolarSeries.SetRadiusIncrement(Const Value:Double);
begin
  if Assigned(ParentChart) then GetVertAxis.Increment:=Value;
end;

Function TCustomPolarSeries.Clicked(x,y:Integer):Integer;
var t : Integer;
begin
  if Assigned(ParentChart) then ParentChart.Canvas.Calculate2DPosition(x,y,StartZ);
  result:=inherited Clicked(x,y);
  if (result=-1) and (FirstValueIndex>-1) and (LastValueIndex>-1) then
    if FPointer.Visible then
    for t:=FirstValueIndex to LastValueIndex do
        if (Abs(CalcXPos(t)-X)<FPointer.HorizSize) and
           (Abs(CalcYPos(t)-Y)<FPointer.VertSize) then
        begin
          result:=t;
          break;
        end;
end;

Procedure TCustomPolarSeries.DrawLegendShape(ValueIndex:Integer; Const Rect:TRect);
var tmpColor : TColor;
begin
  if Pen.Visible then
  begin
    LinePrepareCanvas(ValueIndex);
    With Rect do ParentChart.Canvas.DoHorizLine(Left,Right,(Top+Bottom) div 2);
  end;
  if FPointer.Visible then
  begin
    if ValueIndex=-1 then tmpColor:=SeriesColor
                     else tmpColor:=ValueColor[ValueIndex];
    TPointerAccess(FPointer).DrawLegendShape(tmpColor,Rect,Pen.Visible);
  end
  else
  if not Pen.Visible then inherited
end;

{ Used to draw inside circles (grid) (or radar grid lines) }
{ Can be used also to custom draw circles at specific values }
Procedure TCustomPolarSeries.DrawRing(Const Value:Double; Z:Integer);
Var tmp : Double;
begin
  with GetVertAxis do
  begin
    tmp:=Maximum-Minimum;
    if tmp<>0 then
    begin
      tmp :=(Value-Minimum)/tmp;
      DrawPolarCircle(Round(tmp*XRadius),Round(tmp*YRadius),Z);
    end;
  end;
end;

Procedure TCustomPolarSeries.Assign(Source:TPersistent);
begin
  if Source is TCustomPolarSeries then
  With TCustomPolarSeries(Source) do
  begin
    Self.FCircleLabels   := FCircleLabels;
    Self.CircleLabelsFont:= FCircleLabelsFont;
    Self.FCircleLabelsInside:=FCircleLabelsInside;
    Self.FCircleLabelsRot:= FCircleLabelsRot;
    Self.CirclePen       := FCirclePen;
    Self.FClockWiseLabels:= FClockWiseLabels;
    Self.FCloseCircle    := FCloseCircle;
    Self.Pointer         := FPointer;
  end;
  inherited;
end;

Function TCustomPolarSeries.GetRadiusValues:TChartValueList;
begin
  result:=YValues;
end;

Procedure TCustomPolarSeries.SetRadiusValues(Value:TChartValueList);
begin
  SetYValues(Value); { overrides the default YValues }
end;

Function TCustomPolarSeries.IsRadiusStored:Boolean;
begin
  With YValues do
  result:=(Name<>TeeMsg_RadiusValues) or DateTime or
          {$IFDEF TEEMULTIPLIER}(Multiplier<>1) or {$ENDIF}
          (Order<>loNone) or (ValueSource<>'');
end;

Function TCustomPolarSeries.GetAngleValues:TChartValueList;
begin
  result:=XValues;
end;

Procedure TCustomPolarSeries.SetAngleValues(Value:TChartValueList);
begin
  SetXValues(Value); { overrides the default XValues }
end;

Procedure TCustomPolarSeries.PrepareForGallery(IsEnabled:Boolean);
Begin
  inherited;
  Circled:=True;
  GetHorizAxis.Increment:=90;
  With ParentChart do
  begin
    Chart3DPercent:=5;
    RightAxis.Labels:=False;
    TopAxis.Labels:=False;
    With View3DOptions do
    begin
      Orthogonal:=False;
      Elevation:=360;
      Zoom:=90;
    end;
  end;
end;

procedure TCustomPolarSeries.SetClockWiseLabels(const Value: Boolean);
begin
  SetBooleanProperty(FClockWiseLabels,Value);
end;

procedure TCustomPolarSeries.SetCirclePen(Value: TChartPen);
begin
  FCirclePen.Assign(Value);
end;

class procedure TCustomPolarSeries.CreateSubGallery(
  AddSubChart: TChartSubGalleryProc);
begin
  inherited;
  AddSubChart(TeeMsg_NoPoint);
  AddSubChart(TeeMsg_NoLine);
  AddSubChart(TeeMsg_Filled);
  AddSubChart(TeeMsg_Labels);
  AddSubChart(TeeMsg_NoBorder);
  AddSubChart(TeeMsg_NoCircle);
end;

class procedure TCustomPolarSeries.SetSubGallery(ASeries: TChartSeries;
  Index: Integer);
begin
  With TCustomPolarSeries(ASeries) do
  Case Index of
    1: Pointer.Visible:=False;
    2: Pen.Visible:=False;
    3: Brush.Style:=bsSolid;
    4: CircleLabels:=True;
    5: Pointer.Pen.Visible:=False;
    6: CirclePen.Visible:=False;
  else inherited;
  end;
end;

procedure TCustomPolarSeries.SetCircleLabelsInside(const Value: Boolean);
begin
  SetBooleanProperty(FCircleLabelsInside,Value);
end;

procedure TCustomPolarSeries.DoAfterDrawValues;
var tmp : Boolean;
    t   : Integer;
begin
  With ParentChart do
  if not AxisBehind then
  begin
    tmp:=False;
    for t:=SeriesList.IndexOf(Self)+1 to SeriesCount-1 do
        if Series[t] is Self.ClassType then
        begin
          tmp:=True;
          break;
        end;
    if not tmp then DrawAxis;
  end;
  inherited;
end;

Procedure TCustomPolarSeries.DrawMark( ValueIndex:Integer; Const St:String;
                                       APosition:TSeriesMarkPosition);
begin
  Marks.ApplyArrowLength(APosition);
  inherited;
end;

{ TPolarSeries }
Function TPolarSeries.AddPolar( Const Angle,Value:Double;
                                Const ALabel:String;
                                AColor:TColor):Integer;
begin
  result:=AddXY(Angle,Value,ALabel,AColor);
end;

Procedure TPolarSeries.AddSampleValues(NumValues:Integer);
var t   : Integer;
    tmp : Double;
Begin
  tmp:=360.0/NumValues;
  for t:=1 to NumValues do
      AddPolar( t*tmp,                      { <-- Angle }
                1+System.Random(ChartSamplesMax)  { <-- Value (Radius) }
                {$IFNDEF D4},'', clTeeColor{$ENDIF});
end;

{ TRadarSeries }
Procedure TRadarSeries.DoBeforeDrawChart;
var t   : Integer;
    tmp : Double;
begin
  inherited;
  SetRotationAngle(90);
  IMaxValuesCount:=ParentChart.GetMaxValuesCount;
  if IMaxValuesCount>0 then
  begin
    tmp:=360.0/IMaxValuesCount;
    AngleIncrement:=tmp;
    for t:=0 to Count-1 do
        if XValues.Value[t]<>t*tmp then XValues.Value[t]:=t*tmp;
  end;
end;

Procedure TRadarSeries.AddSampleValues(NumValues:Integer);
Const RadarSampleStr:Array[0..4] of String=
         ( TeeMsg_PieSample1,TeeMsg_PieSample2,TeeMsg_PieSample3,
           TeeMsg_PieSample4,TeeMsg_PieSample5);
var t : Integer;
begin
  for t:=0 to NumValues-1 do
      Add(System.Random(1000),RadarSampleStr[t mod 5]{$IFNDEF D4},clTeeColor{$ENDIF});
end;

Procedure TRadarSeries.PrepareForGallery(IsEnabled:Boolean);
Begin
  inherited;
  FillSampleValues(NumSampleValues);
end;

Function TRadarSeries.GetCircleLabel(Const Angle:Double; Index:Integer):String;
begin
  result:=XLabel[Index];
  if result='' then Str(Index,result);
end;

Function TRadarSeries.NumSampleValues:Integer;
begin
  result:=5;
end;

initialization
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{
   RegisterTeeSeries(TPolarSeries,TeeMsg_GalleryPolar,TeeMsg_GalleryExtended,2);
   RegisterTeeSeries(TRadarSeries,TeeMsg_GalleryRadar,TeeMsg_GalleryExtended,2);
}
   RegisterTeeSeries(TPolarSeries,TeeMsg_GalleryPolar,TeeMsg_GalleryExtended,2,stPolar);
   RegisterTeeSeries(TRadarSeries,TeeMsg_GalleryRadar,TeeMsg_GalleryExtended,2,stRadar);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
finalization
   UnRegisterTeeSeries([TPolarSeries,TRadarSeries]);
end.
