{**********************************************}
{   TeeChart PCX format related functions      }
{   Copyright (c) 2000 by David Berneda        }
{   Portions Copyright (c) Davie Reed,         }
{   January 1999. E-Mail:  davie@smatters.com  }
{**********************************************}
{$I teedefs.inc}
unit TeePCX;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeExport;

type
  TPCXOptions = class(TForm)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TPCXExportFormat=class(TTeeExportFormat)
  private
    FProperties : TPCXOptions;
    Procedure CheckProperties;
  protected
    Procedure DoCopyToClipboard; override;
  public
    function Description:String; override;
    function FileExtension:String; override;
    function FileFilter:String; override;
    Function Options:TForm; override;
    Procedure SaveToStream(Stream:TStream); override;
  end;

implementation

{$R *.DFM}

Uses Clipbrd, PCX, TeeImageConstants;

function TPCXExportFormat.Description:String;
begin
  result:=TeeMsg_AsPCX;
end;

function TPCXExportFormat.FileFilter:String;
begin
  result:=TeeMsg_PCXFilter;
end;

function TPCXExportFormat.FileExtension:String;
begin
  result:='pcx';
end;

Procedure TPCXExportFormat.CheckProperties;
begin
  if not Assigned(FProperties) then FProperties:=TPCXOptions.Create(nil)
end;

Function TPCXExportFormat.Options:TForm;
begin
  CheckProperties;
  result:=FProperties;
end;

procedure TPCXExportFormat.SaveToStream(Stream:TStream);
var b : TBitmap;
begin
  CheckProperties;
  CheckSize;
  b:=Panel.TeeCreateBitmap(Panel.Color,Rect(0,0,Width,Height));
  try
    b.PixelFormat:=pf24Bit;
    TeePCXToStream(Stream,b,4);
  finally
    b.Free;
  end;
end;

procedure TPCXExportFormat.DoCopyToClipboard;
var tmp : TBitmap;
begin
  tmp:=Panel.TeeCreateBitmap(Panel.Color,Rect(0,0,Width,Height));
  try
    Clipboard.Assign(tmp);
  finally
    tmp.Free;
  end;
end;

initialization
  RegisterTeeExportFormat(TPCXExportFormat);
finalization
  UnRegisterTeeExportFormat(TPCXExportFormat);
end.
