{******************************************************************************+
  Unit RegDB
--------------------------------------------------------------------------------
  Autor: Martin Forst
--------------------------------------------------------------------------------
  Registierungsdatenbank
+******************************************************************************}
Unit RegDB;

Interface

{$H+}

Uses
Classes,WinTypes,Objects;

Const MaxKeyLength      = 30;

      rdbeNoError       = 0;
      rdbeIllegalType   = 1;
      rdbeUnknownEntry  = 2;

Type TRegistryHeap      = Class
      Private
       FFreeList        : LongInt;
       FGlobalSize      : LongInt;
       FGlobalHandle    : THandle;
       FGlobalPtr       : PChar;
       FOnChangeSize    : TNotifyEvent;
       Function    GetBlockPointer(BlockNumber:LongInt):Pointer;
       Function    GetNumber(Ptr:Pointer):LongInt;
       Procedure   SetGlobalSize(ASize:LongInt);
      Protected
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Function    Allocate(Size:LongInt):LongInt;
       Procedure   DeAllocate(BlockNumber:LongInt);
       Procedure   LoadFromStream(S:TReader); overload;
       Property    Numbers[Ptr:Pointer]:LongInt read GetNumber;
       Property    OnChangeSize:TNotifyEvent read FOnChangeSize write FOnChangeSize;
       Property    Ptrs[BlockNumber:LongInt]:Pointer read GetBlockPointer; default;
       Function    ReAllocate(BlockNumber:LongInt;Size:LongInt):LongInt;
       Procedure   StoreToStream(S:TWriter); overload;
       Procedure   LoadFromStream(S:POldStream); overload;
       Procedure   StoreToStream(var S:TOldStream); overload;
     end;

     TRegDataType       = (rtUnknown,rtKey,rtInteger,rtBool,rtFloat,rtText,rtBinary,rtDateTime);

     TRegistryData      = Packed Record
       case Integer of
         1: (Ptr   : Pointer;);
         2: (Long  : LongInt;);
         3: (Float : Double;);
         4: (Binary: Array[0..7] of Char;);
     end;

     TRegistryEntryInfo = Class
      Private
       FName            : String;
       FDataType        : TRegDataType;
       FDataSize        : Integer;
      Public
       Property    Name:String read FName;
       Property    DataType:TRegDataType read FDataType;
       Property    DataSize:Integer read FDataSize;
     end;

     PRegistryEntry     = ^TRegistryEntry;
     TRegistryEntry     = Packed Record
       Name             : LongInt;
       DataType         : TRegDataType;
       DataSize         : Word;
       DataPtr          : TRegistryData;
       Next             : LongInt;
       Children         : LongInt;
     end;

     TCustomRegistry    = Class
      Private
       FAliasList       : TStringList;
       FCurrentPath     : String;
       FLastError       : Integer;
      Protected
       Function    GetData(Const EntryName:String;var Buffer;Offset,
                       BufSize:Integer;var Copied:Integer):TRegDataType; virtual; abstract;
       Function    GetDataSize(Const ValueName:String):Integer; virtual; abstract;
       Function    GetDataType(Const ValueName:String):TRegDataType; virtual; abstract;
       Procedure   SetCurrentPath(Const APath:String); virtual;
       Procedure   SetData(Const Name:String;Const Buffer;Size:Integer;ADataType:TRegDataType); virtual; abstract;
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Function    AddAlias(Const KeyName,AliasName:String):Boolean;
       Property    CurrentPath:String read FCurrentPath write SetCurrentPath;
       Property    DataSize[Const ValueName:String]:Integer read GetDataSize;
       Property    DataType[Const ValueName:String]:TRegDataType read GetDataType;
       Procedure   GetSubKeys(const KeyName:String;SubKeys:TStrings); virtual;
       Procedure   GetValues(const KeyName:String;Values:TStrings); virtual;
       Function    GetEntryInfo(const Name:String):TregistryEntryInfo; virtual;
       Property    LastError:Integer read FLastError;
       Function    OpenKey(Const KeyName:String;CanCreate:Boolean):Boolean; virtual; abstract;
       Function    ReadBinary(Const ValueName:String;var Buffer;BufSize:Integer):Integer;
       Function    ReadBinaryOffset(Const ValueName:String;var Buffer;Offset,BufSize:Integer):Integer;
       Function    ReadBool(Const ValueName:String):Boolean;
       Function    ReadDateTime(Const ValueName:String):TDateTime;
       Function    ReadFloat(Const ValueName:String):Double;
       Function    ReadInteger(Const ValueName:String):LongInt;
       Function    ReadString(Const ValueName:String):String;
       Function    ReadText(Const ValueName:String;Value:PChar;ValueLen:Integer):Integer;
       Procedure   WriteBinary(Const ValueName:String;Const Buffer;BufSize:Integer);
       Procedure   WriteBool(Const ValueName:String;Value:Boolean);
       Procedure   WriteDateTime(Const ValueName:String;Value:TDateTime);
       Procedure   WriteFloat(Const ValueName:String;Value:Double);
       Procedure   WriteInteger(Const ValueName:String;Value:LongInt);
       Procedure   WriteString(Const ValueName:String;Value:String);
       Procedure   WriteText(Const ValueName:String;Value:PChar);
     end;

     TRegistryDataBase  = Class(TCustomRegistry)
      Private
       FHeap            : TRegistryHeap;
       FCurrentEntry    : PRegistryEntry;
       FNameTable       : LongInt;
       FNameTableSize   : LongInt;
       FRootEntry       : LongInt;    
       Function    AddSubEntry(Entry:PRegistryEntry;Const KeyName:String):PRegistryEntry;
       Procedure   DeleteSubEntry(Entry:PRegistryEntry;Const KeyName:String);
       Procedure   DoChangeSize(Sender:TObject);
       Function    FindCreateEntry(Const EntryName:String;ADataType:TRegDataType):PRegistryEntry;
       Function    FindEntry(Const KeyName:String):PRegistryEntry;
       Function    FindCreateName(Const Name:ShortString):LongInt;
       Function    FindSubEntry(Entry:PRegistryEntry;Const KeyName:String):PRegistryEntry;
       Procedure   ParseKeyName(KeyName:String;Strings:TStringList);
      Protected
       Function    GetData(Const EntryName:String;var Buffer;Offset,BufSize:Integer;var Copied:Integer):TRegDataType; override;
       Function    GetDataSize(Const ValueName:String):Integer; override;
       Function    GetDataType(Const ValueName:String):TRegDataType; override;
       Procedure   SetCurrentPath(Const APath:String); override;
       Procedure   SetData(Const Name:String;Const Buffer;Size:Integer;ADataType:TRegDataType); override;
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Function    CreateKey(Const KeyName:String):Boolean;
       Function    DeleteKey(Const KeyName:String):Boolean;
       Function    DeleteValue(Const ValueName:String):Boolean;
       Function    GetEntryInfo(const Name:String):TRegistryEntryInfo; override;
       Procedure   GetSubKeys(const KeyName:String;SubKeys:TStrings); override;
       Procedure   GetValues(const KeyName:String;Values:TStrings); override;
       Function    HasSubKeys:Boolean;
       Function    KeyExists(Const KeyName:String):Boolean;
       Procedure   LoadFromStream(Reader:TReader); overload;
       Function    OpenKey(Const KeyName:String;CanCreate:Boolean):Boolean; override;
       Function    RenameValue(Const OldName,NewName:String):Boolean;
       Procedure   StoreToStream(Writer:TWriter); overload;
       Function    ValueExists(Const ValueName:String):Boolean;
       Procedure   LoadFromStream(S:POldStream); overload;
       Procedure   StoreToStream(var S:TOldStream); overload;
     end;

     TWindowsRegistry   = Class(TCustomRegistry)
     end;

     TIniFileRegistry   = Class(TCustomRegistry)
     end;

Implementation

Uses SysUtils,WinProcs;

Const PageSize          = 1024;
      NameTablePageSize = 128;

Type PFreeBlock  = ^TFreeBlock;
     TFreeBlock  = Record
       Size        : LongInt;
       Next        : LongInt;
     end;

     PNameEntry  = ^TNameEntry;
     TNameEntry  = Record
       UseCount  : Word;
       case Integer of
         0 : (Name      : ShortString;);
         1 : (Length    : Byte;);
     end;

{==============================================================================}
{ TCustomRegistry                                                              }
{==============================================================================}

Constructor TCustomRegistry.Create;
begin
  inherited Create;
  FLastError:=rdbeNoError;
  FAliasList:=TStringList.Create;
  FAliasList.Sorted:=TRUE;
  FAliasList.Duplicates:=dupIgnore;
end;

Destructor TCustomRegistry.Destroy;
var Cnt          : Integer;
begin
  for Cnt:=0 to FAliasList.Count-1 do
    DisposeStr(PString(FAliasList.Objects[Cnt]));
  FAliasList.Free;
  inherited Destroy;
end;    

Function TCustomRegistry.ReadBinary(Const ValueName:String;var Buffer;
    BufSize:Integer):Integer;
begin
  FLastError:=rdbeNoError;
  GetData(ValueName,Buffer,0,BufSize,Result);
end;
   
Function TCustomRegistry.ReadBinaryOffset(Const ValueName:String;var Buffer;
    Offset:Integer;BufSize:Integer):Integer;
begin
  FLastError:=rdbeNoError;
  GetData(ValueName,Buffer,Offset,BufSize,Result);
end;

Function TCustomRegistry.ReadBool(Const ValueName:String):Boolean;
var Copied       : Integer;
begin
  FLastError:=rdbeNoError;
  if GetData(ValueName,Result,0,SizeOf(Result),Copied)<>rtBool then begin
    if FLastError=rdbeNoError then FLastError:=rdbeIllegalType;
    Result:=FALSE;
  end;
end;

Function TCustomRegistry.ReadFloat(Const ValueName:String):Double;
var Copied       : Integer;
    EntryType    : TRegDataType;
begin
  FLastError:=rdbeNoError;
  EntryType:=GetData(ValueName,Result,0,SizeOf(Result),Copied);
  if EntryType=rtInteger then Result:=PLongInt(@Result)^
  else if EntryType<>rtFloat then begin
    if FLastError=rdbeNoError then FLastError:=rdbeIllegalType;
    Result:=0;
  end;
end;

Function TCustomRegistry.ReadInteger(Const ValueName:String):LongInt;
var Copied       : Integer;
    EntryType    : TRegDataType;
    Value        : Double;
begin
  FLastError:=rdbeNoError;
  EntryType:=GetData(ValueName,Value,0,SizeOf(Value),Copied);
  if EntryType=rtFloat then Result:=Round(Value)
  else if EntryType=rtInteger then Result:=PLongInt(@Value)^
  else begin
    if FLastError=rdbeNoError then FLastError:=rdbeIllegalType;
    Result:=0;
  end;
end;

Function TCustomRegistry.ReadString(Const ValueName:String):String;
var Copied       : Integer;
begin
  FLastError:=rdbeNoError;
  SetLength(Result,512);
  if GetData(ValueName,PChar(Result)^,0,512,Copied)<>rtText then
      Result:=''
  else SetLength(Result,Copied);
end;

Function TCustomRegistry.ReadText(Const ValueName:String;Value:PChar;
    ValueLen:Integer):Integer;
begin
  FLastError:=rdbeNoError;
  if (GetData(ValueName,Value,0,ValueLen,Result)<>rtText)
      or (Result=0) then begin
    if FLastError=rdbeNoError then FLastError:=rdbeIllegalType;
    StrCopy(Value,'');
  end
  else Value[Result]:=#0;
end;

Function TCustomRegistry.ReadDateTime(Const ValueName:String):TDateTime;
var Copied       : Integer;
begin
  FLastError:=rdbeNoError;
  if GetData(ValueName,Result,0,SizeOf(Result),Copied)<>rtDateTime then begin
    if FLastError=rdbeNoError then FLastError:=rdbeIllegalType;
    Result:=Now;
  end;
end;

Procedure TCustomRegistry.WriteBinary(Const ValueName:String;Const Buffer;
    BufSize:Integer);
begin
  FLastError:=rdbeNoError;
  SetData(ValueName,Buffer,BufSize,rtBinary);
end;

Procedure TCustomRegistry.WriteBool(Const ValueName:String;Value:Boolean);
begin
  FLastError:=rdbeNoError;
  SetData(ValueName,Value,SizeOf(Value),rtBool);
end;

Procedure TCustomRegistry.WriteFloat(Const ValueName:String;Value:Double);
begin
  FLastError:=rdbeNoError;
  SetData(ValueName,Value,SizeOf(Value),rtFloat);
end;

Procedure TCustomRegistry.WriteInteger(Const ValueName:String;Value:LongInt);
begin
  FLastError:=rdbeNoError;
  SetData(ValueName,Value,SizeOf(Value),rtInteger);
end;
               
Procedure TCustomRegistry.WriteString(Const ValueName:String;Value:String);
begin
  FLastError:=rdbeNoError;
  SetData(ValueName,PChar(Value)^,Length(Value),rtText);
end;

Procedure TCustomRegistry.WriteDateTime(Const ValueName:String;Value:TDateTime);
begin
  FLastError:=rdbeNoError;
  SetData(ValueName,Value,SizeOf(Value),rtDateTime);
end;

Procedure TCustomRegistry.WriteText(Const ValueName:String;Value:PChar);
begin
  FLastError:=rdbeNoError;
  SetData(ValueName,Value^,StrLen(Value),rtText);
end;

Procedure TCustomRegistry.SetCurrentPath(Const APath:String);
begin
  if APath<>'' then begin
    if APath[1]='\' then FCurrentPath:=APath
    else FCurrentPath:=FCurrentPath+APath;
  end;
end;

Function TCustomRegistry.AddAlias(Const KeyName,AliasName:String):Boolean;
begin
  if FAliasList.IndexOf(KeyName)>=0 then Result:=FALSE
  else begin
    FAliasList.AddObject(KeyName,Pointer(NewStr(AliasName)));
    Result:=TRUE;
  end;
end;

{==============================================================================+
  TRegistryDataBase
+==============================================================================}

Constructor TRegistryDataBase.Create;
  begin
    inherited Create;
    FHeap:=TRegistryHeap.Create;
    FHeap.OnChangeSize:=DoChangeSize;
    FNameTable:=FHeap.Allocate(NameTablePageSize);
    FNameTableSize:=NameTablePageSize;
    FRootEntry:=FHeap.Allocate(SizeOf(TRegistryEntry));
    FCurrentEntry:=FHeap[FRootEntry];
    with FCurrentEntry^ do DataType:=rtKey;
    AddAlias('\Users\.Default','\User');
  end;

Destructor TRegistryDataBase.Destroy;
  begin
    FHeap.Free;
    inherited Destroy;
  end;

Procedure TRegistryDataBase.DoChangeSize;
  begin
    FCurrentEntry:=FindEntry(CurrentPath);
  end;  

Function TRegistryDataBase.FindCreateName(Const Name:ShortString):LongInt;
var Source       : PNameEntry;
    Position     : LongInt;
    PossiblyFree : LongInt;
    CurLength    : Integer;
Procedure GetPosition;
  begin
    Source:=FHeap[Position];
    CurLength:=Source^.Length;
    if CurLength and $80<>0 then CurLength:=-(CurLength and $7F);
  end;
begin
  Position:=FNameTable;     
  GetPosition;
  PossiblyFree:=-1;
  while CurLength<>0 do begin
    if (CurLength>0)
        and (CompareText(Name,Source^.Name)=0) then begin
      Inc(Source^.UseCount);
      Result:=Position-FNameTable;
      Exit;
    end
    else if (CurLength<0)
        and (Abs(CurLength)>=Length(Name)+3) then
      PossiblyFree:=Position;
    Position:=Position+Abs(CurLength)+3;
    GetPosition;
  end;
  if PossiblyFree>=0 then begin
    Position:=PossiblyFree;
    GetPosition;
    Move(Name,Source^,Length(Name)+1);
    if Length(Name)<Abs(CurLength) then begin
      Position:=Position+Length(Name);
      GetPosition;
      Source^.Length:=(Abs(CurLength)-Length(Name)-1) or $80;
    end;
    Source^.UseCount:=1;
    Result:=PossiblyFree-FNameTable;
  end
  else begin
    // need palace for name, use-count, name-length and a blank entry at the end
    if FNameTableSize-Position+FNameTable<=Length(Name)+6 then begin
      Dec(Position,FNameTable);
      FNameTable:=FHeap.ReAllocate(FNameTable,FNameTableSize+NameTablePageSize);
      Inc(FNameTableSize,NameTablePageSize);
      Inc(Position,FNameTable);
      Source:=FHeap[Position];
    end;
    Move(Name,Source^.Name,Length(Name)+1);
    Source^.UseCount:=1;
    Result:=Position-FNameTable;
  end;
end;

Procedure TRegistryDataBase.ParseKeyName
   (
   KeyName         : String;
   Strings         : TStringList
   );
  var APos         : Integer;
  begin
    APos:=Pos('\',KeyName);
    while APos<>0 do begin
      if APos=1 then Delete(KeyName,1,1)
      else begin
        if APos>MaxKeyLength then Strings.Add(Copy(KeyName,1,MaxKeyLength))
        else Strings.Add(Copy(KeyName,1,APos-1));
        Delete(KeyName,1,APos);
      end;
      APos:=Pos('\',KeyName);
    end;
    if KeyName<>'' then Strings.Add(KeyName);
  end;

Function TRegistryDataBase.CreateKey
   (
   Const KeyName   : String
   )
   : Boolean;
  begin
    FLastError:=rdbeNoError;
    FindCreateEntry(KeyName,rtKey);
    Result:=TRUE;
  end;

Function TRegistryDataBase.FindEntry
   (
   Const KeyName   : String
   )
   : PRegistryEntry;
  var KeyList      : TStringList;
      Cnt          : Integer;
  begin
    if KeyName='' then Result:=FCurrentEntry
    else begin
      KeyList:=TStringList.Create;
      try
        ParseKeyName(KeyName,KeyList);
        if KeyName[1]='\' then Result:=FHeap[FRootEntry]
        else Result:=FCurrentEntry;
        for Cnt:=0 to KeyList.Count-1 do begin
          Result:=FindSubEntry(Result,KeyList[Cnt]);
          if Result=NIL then Exit;
        end; 
      finally
        KeyList.Free;
      end;
    end;  
  end;

Function TRegistryDataBase.FindCreateEntry
   (
   Const EntryName : String;
   ADataType       : TRegDataType
   )
   : PRegistryEntry;
  var KeyList      : TStringList;
      Entry        : PRegistryEntry;
      Cnt          : Integer;
      Cnt1         : Integer;
  begin
    if EntryName='' then Result:=FCurrentEntry
    else begin
      KeyList:=TStringList.Create;
      try
        ParseKeyName(EntryName,KeyList);
        if EntryName[1]='\' then Result:=FHeap[FRootEntry]
        else begin
          if FCurrentEntry=NIL then FCurrentEntry:=FindCreateEntry(CurrentPath,rtKey);
          Result:=FCurrentEntry;
        end;  
        for Cnt:=0 to KeyList.Count-1 do begin
          Entry:=FindSubEntry(Result,KeyList[Cnt]);
          if Entry=NIL then begin
            for Cnt1:=Cnt to KeyList.Count-1 do begin
              Result:=AddSubEntry(Result,KeyList[Cnt1]);
              Result^.DataType:=rtKey;
            end;
            Result^.DataType:=ADataType;
            Break;
          end
          else Result:=Entry;
        end;
      finally
        KeyList.Free;
      end;
    end;  
  end;

Function TRegistryDataBase.DeleteKey
   (
   Const KeyName   : String
   )
   : Boolean;
  var Entry        : PRegistryEntry;
  begin
    FLastError:=rdbeNoError;
    Result:=FALSE;
    Entry:=FindEntry(KeyName);
    if Entry=NIL then FLastError:=rdbeUnknownEntry
    else if Entry^.DataType<>rtKey then FLastError:=rdbeIllegalType
    else begin
{      DeleteSubEntry(FHeap[Entry^.Parent],Entry^.Name);}
      Result:=TRUE;
    end;
  end;

Function TRegistryDataBase.DeleteValue
   (
   Const ValueName : String
   )
   : Boolean;
  var Entry        : PRegistryEntry;
  begin
    Result:=FALSE;
    FLastError:=rdbeNoError;
    Entry:=FindEntry(ValueName);
    if Entry=NIL then FLastError:=rdbeUnknownEntry
    else if Entry^.DataType in [rtUnknown,rtKey] then FLastError:=rdbeIllegalType
    else begin
{      DeleteSubEntry(FHeap[Entry^.Parent],Entry^.Name);}
      Result:=TRUE;
    end;
  end;

Function TRegistryDataBase.GetDataSize
   (
   Const ValueName : String
   )
   : Integer;
  var Entry        : PRegistryEntry;
  begin
    FLastError:=rdbeNoError;
    Result:=0;
    Entry:=FindEntry(ValueName);
    if Entry=NIL then FLastError:=rdbeUnknownEntry
    else if Entry^.DataType in [rtUnknown,rtKey] then FLastError:=rdbeIllegalType
    else Result:=Entry^.DataSize;
  end;

Function TRegistryDataBase.GetDataType
   (
   Const ValueName : String
   )
   : TRegDataType;
  var Entry        : PRegistryEntry; 
  begin
    FLastError:=rdbeNoError;
    Result:=rtUnknown;
    Entry:=FindEntry(ValueName);
    if Entry=NIL then FLastError:=rdbeUnknownEntry
    else Result:=Entry^.DataType;
  end;

Function TRegistryDataBase.HasSubKeys
   : Boolean;
  begin
    FLastError:=rdbeNoError;
    Result:=(FCurrentEntry<>NIL) and (FCurrentEntry^.Children<>0);
  end;

Function TRegistryDataBase.KeyExists
   (
   Const KeyName   : String
   )
   : Boolean;
  var Entry        : PRegistryEntry;
  begin
    FLastError:=rdbeNoError;
    Result:=FALSE;
    Entry:=FindEntry(KeyName);
    if Entry=NIL then FLastError:=rdbeUnknownEntry
    else if Entry^.DataType<>rtKey then FLastError:=rdbeIllegalType
    else Result:=TRUE;
  end;

Function TRegistryDataBase.OpenKey
   (
   Const KeyName   : String;
   CanCreate       : Boolean
   )
   : Boolean;
  var Entry        : PRegistryEntry;
  begin
    FLastError:=rdbeNoError;
    if KeyName='' then Result:=TRUE
    else begin
      FCurrentEntry:=NIL;
      Result:=FALSE;
      if KeyName[1]='\' then FCurrentPath:=KeyName
      else FCurrentPath:=FCurrentPath+'\';
      if CanCreate then Entry:=FindCreateEntry(KeyName,rtKey)
      else Entry:=FindEntry(KeyName);
      if Entry=NIL then FLastError:=rdbeUnknownEntry
      else if Entry^.DataType<>rtKey then FLastError:=rdbeIllegalType
      else begin
        FCurrentEntry:=Entry;
        Result:=TRUE;
      end;
    end;  
  end;

Function TRegistryDataBase.GetData
   (
   Const EntryName : String;
   var Buffer      ;
   Offset          : Integer;
   BufSize         : Integer;
   var Copied      : Integer
   )
   : TRegDataType;
  var Source       : PChar;
      Entry        : PRegistryEntry;
  begin
    Copied:=0;
    Entry:=FindEntry(EntryName);
    if Entry=NIL then begin
      FLastError:=rdbeUnknownEntry;
      Result:=rtUnknown;
    end
    else if Entry^.DataType=rtKey then Result:=rtKey
    else begin
      if Entry^.DataSize<=8 then Source:=@Entry^.DataPtr
      else Source:=FHeap[Entry^.DataPtr.Long];
      if BufSize+Offset<Entry^.DataSize then Copied:=BufSize
      else if Offset>=BufSize then Copied:=0
      else Copied:=Entry^.DataSize-Offset;
      Move(Source[Offset],Buffer,Copied);
      Result:=Entry^.DataType;
    end;
  end;

Function TRegistryDataBase.RenameValue
   (
   Const OldName   : String;
   Const NewName   : String
   )
   : Boolean;
  begin
    Result:=FALSE;
  end;
   
Function TRegistryDataBase.ValueExists
   (
   Const ValueName : String
   )
   : Boolean;
  var Entry        : PRegistryEntry; 
  begin
    FLastError:=rdbeNoError;
    Result:=FALSE;
    Entry:=FindEntry(ValueName);
    if Entry=NIL then FLastError:=rdbeUnknownEntry
    else if Entry^.DataType in [rtKey,rtUnknown] then FLastError:=rdbeIllegalType
    else Result:=TRUE;
  end;
   
Procedure TRegistryDataBase.SetData
   (
   Const Name      : String;
   Const Buffer    ;
   Size            : Integer;
   ADataType       : TRegDataType
   );
  var Entry        : PRegistryEntry;
      Dest         : PChar;
      EntryNumber  : LongInt;
      Data         : LongInt;
  begin
    Entry:=FindCreateEntry(Name,rtUnknown);
    if Entry^.DataType=rtKey then FLastError:=rdbeIllegalType
    else begin
      Entry^.DataType:=ADataType;
      if Size<=8 then begin
        if Entry^.DataSize>8 then FHeap.DeAllocate(Entry^.DataPtr.Long); 
        Dest:=@Entry^.DataPtr;
      end
      else begin
        if Entry^.DataSize<=8 then Entry^.DataPtr.Long:=0; 
        EntryNumber:=FHeap.Numbers[Entry];
        if Entry^.DataPtr.Long=0 then Data:=FHeap.Allocate(Size)
        else Data:=FHeap.ReAllocate(Entry^.DataPtr.Long,Size);
        Entry:=FHeap[EntryNumber];
        Entry^.DataPtr.Long:=Data;
        Dest:=FHeap[Data];
      end;
      Entry^.DataSize:=Size;
      if Size>0 then Move(Buffer,Dest^,Size);
    end;
  end;

Procedure TRegistryDataBase.SetCurrentPath
   (
   Const APath     : String
   );
  begin
    inherited SetCurrentPath(APath);
    FCurrentEntry:=FindEntry(APath);
  end;

Function TRegistryDataBase.FindSubEntry(Entry:PRegistryEntry;Const KeyName:String):PRegistryEntry;
var SubNumber    : LongInt;
begin
  if Entry<>NIL then begin
    SubNumber:=Entry^.Children;
    while SubNumber<>0 do begin
      Result:=FHeap[SubNumber];
      if CompareText(PNameEntry(FHeap[FNameTable+Result^.Name])^.Name,KeyName)=0 then Exit;
      SubNumber:=Result^.Next;
    end;
  end;
  Result:=NIL;
end;

Function TRegistryDataBase.AddSubEntry
   (
   Entry           : PRegistryEntry;
   Const KeyName   : String
   )
   : PRegistryEntry;
  var ANumber      : LongInt;
      EntryNumber  : LongInt;
  begin
    if Entry^.Children=0 then begin
      ANumber:=FHeap.Allocate(SizeOf(TRegistryEntry));
      Entry^.Children:=ANumber;
    end
    else begin
      Entry:=FHeap[Entry^.Children];
      while Entry^.Next<>0 do Entry:=FHeap[Entry^.Next];
      ANumber:=FHeap.Allocate(SizeOf(TRegistryEntry));
      Entry^.Next:=ANumber
    end;
    EntryNumber:=FindCreateName(KeyName);
    Result:=FHeap[ANumber];
    Result^.Name:=EntryNumber;
  end;

Procedure TRegistryDataBase.DeleteSubEntry
   (
   Entry           : PRegistryEntry;
   Const KeyName   : String
   );
  begin
  end;

Procedure TRegistryDataBase.LoadFromStream(S:POldStream);
begin
  FHeap.LoadFromStream(S);
  S.Read(FNameTable,SizeOf(FNameTable));
  S.Read(FNameTableSize,SizeOf(FNameTableSize));
  S.Read(FRootEntry,SizeOf(FRootEntry));
  FCurrentEntry:=FHeap[FRootEntry];
  FCurrentPath:='';
end;

Procedure TRegistryDataBase.StoreToStream(var S:TOldStream);
begin
  FHeap.StoreToStream(S);
  S.Write(FNameTable,SizeOf(FNameTable));
  S.Write(FNameTableSize,SizeOf(FNameTableSize));
  S.Write(FRootEntry,SizeOf(FRootEntry));
end;

Procedure TRegistryDataBase.LoadFromStream(Reader:TReader);
begin
  FHeap.LoadFromStream(Reader);
  Reader.Read(FNameTable,SizeOf(FNameTable));
  Reader.Read(FNameTableSize,SizeOf(FNameTableSize));
  Reader.Read(FRootEntry,SizeOf(FRootEntry));
  FCurrentEntry:=FHeap[FRootEntry];
  FCurrentPath:='';
end;

Procedure TRegistryDataBase.StoreToStream(Writer:TWriter);
begin
  FHeap.StoreToStream(Writer);
  Writer.Write(FNameTable,SizeOf(FNameTable));
  Writer.Write(FNameTableSize,SizeOf(FNameTableSize));
  Writer.Write(FRootEntry,SizeOf(FRootEntry));
end;

{==============================================================================+
  TRegistryHeap
+==============================================================================}

Constructor TRegistryHeap.Create;
  begin
    inherited Create;
    FFreeList:=-1;    
  end;

Destructor TRegistryHeap.Destroy;
  begin
    if FGlobalHandle<>0 then begin
      GlobalUnlock(FGlobalHandle);
      GlobalFree(FGlobalHandle);
    end;
    inherited Destroy;
  end;

Function TRegistryHeap.GetBlockPointer
   (
   BlockNumber     : LongInt
   )
   : Pointer;
  begin
    Result:=FGlobalPtr+BlockNumber;
  end;

Function TRegistryHeap.GetNumber
   (
   Ptr             : Pointer
   )
   : LongInt;
  begin
    Result:=PChar(Ptr)-FGlobalPtr;
  end;
   
Function TRegistryHeap.Allocate
   (
   Size            : LongInt
   )
   : LongInt;
  var BlockNumber  : LongInt;
      BlockPtr     : PFreeBlock;
      NewBlockPtr  : PFreeBlock;
      PrevBlockPtr : PFreeBlock;
  Procedure DoAllocate;
    begin
      if PrevBlockPtr=NIL then FFreeList:=BlockPtr^.Next
      else PrevBlockPtr^.Next:=BlockPtr^.Next;
      FillChar(BlockPtr^,Size,#0);
      BlockPtr^.Size:=Size;
      Result:=BlockNumber+4;
    end;
  begin
    Inc(Size,4);
    if Size<8 then Size:=8;
    while TRUE do begin
      PrevBlockPtr:=NIL;
      BlockNumber:=FFreeList;
      while BlockNumber>-1 do begin
        BlockPtr:=Ptrs[BlockNumber];             
        if BlockPtr^.Size=Size then begin
          DoAllocate;
          Exit;
        end
        else if BlockPtr^.Size-Size>=8 then begin
          NewBlockPtr:=Ptrs[BlockNumber+Size];
          NewBlockPtr^.Size:=BlockPtr^.Size-Size;
          NewBlockPtr^.Next:=BlockPtr^.Next;
          BlockPtr^.Next:=BlockNumber+Size;              
          DoAllocate;
          Exit;
        end
        else BlockNumber:=BlockPtr^.Next;
        PrevBlockPtr:=BlockPtr;
      end;
      SetGlobalSize(FGlobalSize+Size);
    end;
  end;

Procedure TRegistryHeap.SetGlobalSize
   (
   ASize           : LongInt
   );
  var BlockPtr     : PFreeBlock;
  begin
    if ASize Mod PageSize<>0 then ASize:=(ASize Div PageSize+1)*PageSize;
    if ASize<>FGlobalSize then begin
      if FGlobalHandle<>0 then begin
        GlobalUnlock(FGlobalHandle);
        FGlobalHandle:=GlobalRealloc(FGlobalHandle,ASize,GMEM_MOVEABLE or GMEM_ZEROINIT);
      end
      else FGlobalHandle:=GlobalAlloc(GMEM_MOVEABLE or GMEM_ZEROINIT,ASize);
      FGlobalPtr:=GlobalLock(FGlobalHandle);
      BlockPtr:=Ptrs[FGlobalSize];
      BlockPtr^.Size:=ASize-FGlobalSize;
      BlockPtr^.Next:=FFreeList;
      FFreeList:=FGlobalSize;
      FGlobalSize:=ASize;
      if Assigned(FOnChangeSize) then OnChangeSize(Self);
    end;
  end;

Procedure TRegistryHeap.DeAllocate
   (
   BlockNumber     : LongInt
   );
  var BlockPtr     : PFreeBlock;
  begin
    BlockPtr:=Ptrs[BlockNumber-4];
    FillChar(Ptrs[BlockNumber]^,BlockPtr^.Size-4,#0);
    BlockPtr^.Next:=FFreeList;
    FFreeList:=BlockNumber-4;
  end;

Function TRegistryHeap.ReAllocate
   (
   BlockNumber     : LongInt;
   Size            : LongInt
   )
   : LongInt;
  var BlockPtr     : PFreeBlock;
      NewPtr       : PFreeBlock;
  begin
    if Size=PFreeBlock(Ptrs[BlockNumber-4])^.Size-4 then Result:=BlockNumber
    else begin
      Result:=Allocate(Size);
      BlockPtr:=Ptrs[BlockNumber-4];
      NewPtr:=Ptrs[Result-4];
      if NewPtr^.Size<BlockPtr^.Size then Move(Ptrs[BlockNumber]^,Ptrs[Result]^,NewPtr^.Size-4)
      else Move(Ptrs[BlockNumber]^,Ptrs[Result]^,BlockPtr^.Size-4);
      DeAllocate(BlockNumber);
    end;  
  end;

Procedure TRegistryHeap.LoadFromStream(S:POldStream);
var ASize        : LongInt;
    Position     : LongInt;
    Count        : LongInt;
begin
  S.Read(ASize,SizeOf(ASize));
  SetGlobalSize(ASize);
  S.Read(FFreeList,SizeOf(FFreeList));
  Position:=0;
  for Count:=1 to FGlobalSize Div PageSize do begin
    S.Read(Ptrs[Position]^,PageSize);
    Inc(Position,PageSize);
  end;
end;

Procedure TRegistryHeap.StoreToStream(var S:TOldStream);
var Position     : LongInt;
    Count        : LongInt;
begin
  S.Write(FGlobalSize,SizeOf(FGlobalSize));
  S.Write(FFreeList,SizeOf(FFreeList));
  Position:=0;
  for Count:=1 to FGlobalSize Div PageSize do begin
    S.Write(Ptrs[Position]^,PageSize);
    Inc(Position,PageSize);
  end;
end;

Procedure TRegistryHeap.LoadFromStream(S: TReader);
var ASize        : LongInt;
    Position     : LongInt;
    Count        : LongInt;
begin
  S.Read(ASize,SizeOf(ASize));
  SetGlobalSize(ASize);
  S.Read(FFreeList,SizeOf(FFreeList));
  Position:=0;
  for Count:=1 to FGlobalSize Div PageSize do begin
    S.Read(Ptrs[Position]^,PageSize);
    Inc(Position,PageSize);
  end;
end;

Procedure TRegistryHeap.StoreToStream(S: TWriter);
var Position     : LongInt;
    Count        : LongInt;
begin
  S.Write(FGlobalSize,SizeOf(FGlobalSize));
  S.Write(FFreeList,SizeOf(FFreeList));
  Position:=0;
  for Count:=1 to FGlobalSize Div PageSize do begin
    S.Write(Ptrs[Position]^,PageSize);
    Inc(Position,PageSize);
  end;
end;

Procedure TCustomRegistry.GetSubKeys(const KeyName:String;SubKeys:TStrings);
begin
end;

Procedure TCustomRegistry.GetValues(const KeyName:String;Values:TStrings);
begin
end;

Procedure TRegistryDataBase.GetSubKeys(const KeyName:String;SubKeys:TStrings);
var Entry          : PRegistryEntry;
    SubNumber    : LongInt;
begin
  Entry:=FindEntry(KeyName);
  if Entry<>NIL then begin
    SubNumber:=Entry^.Children;
    while SubNumber<>0 do begin
      Entry:=FHeap[SubNumber];      
      if Entry^.DataType=rtKey then SubKeys.Add(PNameEntry( 
          FHeap[FNameTable+Entry^.Name])^.Name);
      SubNumber:=Entry^.Next;
    end;
  end;
end;

Procedure TRegistryDataBase.GetValues(const KeyName:String;Values:TStrings);
var Entry          : PRegistryEntry;
    SubNumber    : LongInt;
begin
  Entry:=FindEntry(KeyName);
  if Entry<>NIL then begin
    SubNumber:=Entry^.Children;
    while SubNumber<>0 do begin
      Entry:=FHeap[SubNumber];
      if Entry^.DataType<>rtKey then Values.Add(PNameEntry(
          FHeap[FNameTable+Entry^.Name])^.Name);
      SubNumber:=Entry^.Next;
    end;
  end;
end;

function TCustomRegistry.GetEntryInfo(const Name:String):TRegistryEntryInfo;
begin
  Result:=TRegistryEntryInfo.Create;
  Result.FName:=Name;
  Result.FDataType:=GetDataType(Name);
  Result.FDataSize:=GetDataSize(Name);
end;

function TRegistryDataBase.GetEntryInfo(const Name: String):TRegistryEntryInfo;
var Entry          : PRegistryEntry;
begin
  Entry:=FindEntry(Name);
  if Entry=NIL then Result:=NIL
  else begin
    Result:=TRegistryEntryInfo.Create;
    Result.FName:=Name;
    Result.FDataType:=Entry^.DataType;
    Result.FDataSize:=Entry^.DataSize;
  end;
end;

end.
