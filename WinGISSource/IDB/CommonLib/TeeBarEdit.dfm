�
 TBARSERIESEDITOR 0y	  TPF0TBarSeriesEditorBarSeriesEditorLeft3TopActiveControl
CBBarStyleBorderIcons BorderStylebsNoneClientHeight� ClientWidth9Color	clBtnFace
ParentFont	OldCreateOrder	Position	poDefaultOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1Left� Top,WidthMHeight	AlignmenttaRightJustifyAutoSizeCaption!93FocusControl
SEBarwidth  TLabelLStyleLeft Top
Width%Height	AlignmenttaRightJustifyAutoSizeCaption!88FocusControl
CBBarStyle  TLabelLabel3Left� TopHWidthQHeight	AlignmenttaRightJustifyAutoSizeCaption!94FocusControlSEBarOffset  TEdit
SEBarwidthLeftTop(Width HeightHelpContextbTabOrder Text1OnChangeSEBarwidthChange  	TComboBox
CBBarStyleLeft(TopWidthaHeightHelpContext`StylecsDropDownList
ItemHeightItems.Strings	RectanglePyramidInvert. PyramidCylinderEllipseArrowRect. GradientCone TabOrderOnChangeCBBarStyleChange  TButton	BBarBrushLeft� TopWidthKHeightHelpContext]Caption!90TabOrderOnClickBBarBrushClick  TEditSEBarOffsetLeftTopDWidth HeightHelpContext3TabOrderText0OnChangeSEBarOffsetChange  	TGroupBox	GroupBox1Left� TopdWidth� HeightITabOrder 	TCheckBox	CBDarkBarLeftTopWidth� HeightHelpContext� Caption!96TabOrder OnClickCBDarkBarClick  	TCheckBoxCBBarSideMarginsLeftTop Width� HeightHelpContext�Caption!97TabOrderOnClickCBBarSideMarginsClick  	TCheckBoxCBMarksAutoPositionLeftTop4Width� HeightHelpContextNCaption!98TabOrderOnClickCBMarksAutoPositionClick   	TGroupBox	GroupBox2Left$TopfWidtheHeightATabOrderVisible 	TCheckBoxCBColorEachLeftTopWidthQHeightHelpContext� Caption!91TabOrder OnClickCBColorEachClick  TButtonColor	BBarColorLeftTop HelpContext�Caption!92TabOrder   TUpDown
UDBarWidthLeft$Top(WidthHeight	Associate
SEBarwidthMin	IncrementPositionTabOrderWrap  TUpDownUDBarOffsetLeft$TopDWidthHeight	AssociateSEBarOffsetMin�	IncrementPosition TabOrderWrap  TButton	BGradientLeft(Top(WidthKHeightHelpContext\Caption!95TabOrderOnClickBGradientClick  
TButtonPenBBarPenLeft� TopHelpContext_Caption!89TabOrder	  TMlgSectionMlgSection1SectionBusinessGraphicsLeftTop�    