{*********************************************}
{    TeePreviewPanelEditor                    }
{   Copyright (c) 1999-2000 by David Berneda  }
{*********************************************}
{$I teedefs.inc}
unit TeePreviewPanelEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls, TeePreviewPanel, TeCanvas, TeePenDlg;

type
  TFormPreviewPanelEditor = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    CBAllowMove: TCheckBox;
    CBAllowResize: TCheckBox;
    CBDragImage: TCheckBox;
    CBAsBitmap: TCheckBox;
    CBShowImage: TCheckBox;
    BPaperColor: TButtonColor;
    RGOrientation: TRadioGroup;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButtonPen;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Edit1: TEdit;
    UDShadowSize: TUpDown;
    BShadowColor: TButtonColor;
    procedure FormShow(Sender: TObject);
    procedure CBAllowMoveClick(Sender: TObject);
    procedure CBAllowResizeClick(Sender: TObject);
    procedure CBDragImageClick(Sender: TObject);
    procedure CBAsBitmapClick(Sender: TObject);
    procedure CBShowImageClick(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure RGOrientationClick(Sender: TObject);
  private
    { Private declarations }
    TeePreviewPanel1 : TTeePreviewPanel;
  public
    { Public declarations }
    Constructor CreatePanel(AOwner:TComponent; APanel:TTeePreviewPanel);
  end;

implementation

{$R *.dfm}
Uses TeeEdiPane;

Constructor TFormPreviewPanelEditor.CreatePanel(AOwner:TComponent; APanel:TTeePreviewPanel);
var Page:TTabSheet;
begin
  inherited Create(AOwner);
  TeePreviewPanel1:=APanel;
  Page:=TTabSheet.Create(Self);
  Page.Caption:='Panel';
  Page.PageControl:=PageControl1;
  AddFormTo(TFormTeePanel.CreatePanel(Self,TeePreviewPanel1),Page,0);
end;

procedure TFormPreviewPanelEditor.FormShow(Sender: TObject);
begin
  With TeePreviewPanel1 do
  begin
    CBAllowMove.Checked   :=AllowMove;
    CBAllowResize.Checked :=AllowResize;
    CBDragImage.Checked   :=DragImage;
    CBAsBitmap.Checked    :=AsBitmap;
    CBShowImage.Checked   :=ShowImage;
    UDShadowSize.Position :=ShadowSize;
    RGOrientation.ItemIndex:=Ord(Orientation);
    Button2.LinkPen(Margins);
  end;
  BPaperColor.LinkProperty(TeePreviewPanel1,'PaperColor');
  BShadowColor.LinkProperty(TeePreviewPanel1,'ShadowColor');
end;

procedure TFormPreviewPanelEditor.CBAllowMoveClick(Sender: TObject);
begin
  TeePreviewPanel1.AllowMove:=CBAllowMove.Checked
end;

procedure TFormPreviewPanelEditor.CBAllowResizeClick(Sender: TObject);
begin
  TeePreviewPanel1.AllowResize:=CBAllowResize.Checked
end;

procedure TFormPreviewPanelEditor.CBDragImageClick(Sender: TObject);
begin
  TeePreviewPanel1.DragImage:=CBDragImage.Checked
end;

procedure TFormPreviewPanelEditor.CBAsBitmapClick(Sender: TObject);
begin
  TeePreviewPanel1.AsBitmap:=CBAsBitmap.Checked
end;

procedure TFormPreviewPanelEditor.CBShowImageClick(Sender: TObject);
begin
  TeePreviewPanel1.ShowImage:=CBShowImage.Checked
end;

procedure TFormPreviewPanelEditor.Edit1Change(Sender: TObject);
begin
  if Showing then TeePreviewPanel1.ShadowSize:=UDShadowSize.Position
end;

procedure TFormPreviewPanelEditor.RGOrientationClick(Sender: TObject);
begin
  With TeePreviewPanel1 do
  Case RGOrientation.ItemIndex of
    0: Orientation:=ppoDefault;
    1: Orientation:=ppoPortrait;
  else
    Orientation:=ppoLandscape;
  end;
end;

end.

