�
 TTEEGRADIENTEDITOR 0U  TPF0TTeeGradientEditorTeeGradientEditorLeftTop�BorderStylebsDialogCaption!174ClientHeight� ClientWidth� Color	clBtnFace
ParentFont	OldCreateOrder	PositionpoScreenCenter	OnDestroyFormDestroyOnShowFormShowPixelsPerInch`
TextHeight 	TGroupBoxGB4Left Top Width� Height)HelpContext�Caption!140TabOrder  TLabelLabel1LeftETopWidth7Height	AlignmenttaRightJustifyAutoSizeCaption!142FocusControlCBDirection  	TCheckBox	CBVisibleLeft	TopWidth8HeightHelpContext�Caption!141TabOrder OnClickCBVisibleClick  	TComboBoxCBDirectionLeft� TopWidthqHeightHelpContext� StylecsDropDownList
ItemHeightItems.Strings
Top Bottom
Bottom Top
Left Right
Right LeftFrom CenterFrom Top LeftFrom Bottom Left TabOrderOnChangeCBDirectionChange   TButtonBOkLeftVTop� WidthKHeightCaption!8Default	ModalResultTabOrder  TButtonBCancelLeft� Top� WidthKHeightCancel	Caption!9ModalResultTabOrderOnClickBCancelClick  	TGroupBox	GroupBox1Left Top,Width� HeightQCaption!143TabOrder TButtonColorBStartLeftHTopHelpContext�Caption!145TabOrder  TButtonColorBMidLeft� TopHelpContextVCaption!146TabOrderOnClick	BMidClick  TButtonColorBEndLeftHTop/HelpContext� Caption!147TabOrder  TButtonBSwapLeftTopWidth1HeightCaption!144TabOrder OnClick
BSwapClick  	TCheckBoxCBMidLeft� Top4WidthQHeightCaption!148TabOrderOnClick
CBMidClick   TMlgSectionMlgSection1SectionBusinessGraphicsLeftTop�    