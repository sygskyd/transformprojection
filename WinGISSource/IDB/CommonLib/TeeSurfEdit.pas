{**********************************************}
{   TSurfaceSeries Editor Dialog               }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeSurfEdit;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, ExtCtrls, Chart, TeeSurfa, TeCanvas, TeePenDlg;

type
  TSurfaceSeriesEditor = class(TForm)
    Button2: TButtonPen;
    Button3: TButton;
    RadioGroup1: TRadioGroup;
    CBSmooth: TCheckBox;
    Button1: TButton;
    procedure FormShow(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure CBSmoothClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    Surface : TSurfaceSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeeBrushDlg, TeeProcs, TeeGriEd;

procedure TSurfaceSeriesEditor.FormShow(Sender: TObject);
begin
  Surface:=TSurfaceSeries(Tag);
  With Surface do
  begin
    if WireFrame then RadioGroup1.ItemIndex:=1 else
    if DotFrame then RadioGroup1.ItemIndex:=2 else
                     RadioGroup1.ItemIndex:=0;
    CBSmooth.Checked:=SmoothPalette;
    Button2.LinkPen(Pen);
  end;
  TeeInsertGrid3DForm(Parent,Surface);
end;

procedure TSurfaceSeriesEditor.Button3Click(Sender: TObject);
begin
  EditChartBrush(Self,Surface.Brush);
end;

procedure TSurfaceSeriesEditor.RadioGroup1Click(Sender: TObject);
begin
  if Showing then
  Case RadioGroup1.ItemIndex of
    0: { solid }
       begin
         Surface.DotFrame:=False;
         Surface.WireFrame:=False;
       end;
    1: Surface.WireFrame:=True;
    2: Surface.DotFrame:=True;
  end;
end;

procedure TSurfaceSeriesEditor.CBSmoothClick(Sender: TObject);
begin
  Surface.SmoothPalette:=CBSmooth.Checked
end;

procedure TSurfaceSeriesEditor.Button1Click(Sender: TObject);
begin
  EditChartBrush(Self,Surface.SideBrush);
end;

initialization
  RegisterClass(TSurfaceSeriesEditor);
end.
