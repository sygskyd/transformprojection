{********************************************}
{  TeeChart Wizard                           }
{  Copyright (c) 1995-2000 by David Berneda  }
{  All Rights Reserved                       }
{********************************************}
{$I TeeDefs.inc}
unit TeeExpForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls, TeEngine, Chart, DBChart, ToolIntf,
  DB, TeeGalleryPanel, DBCtrls, TeeProcs, TeeConst, DBTables, FileCtrl,
  TeeSelectList;

type
  TTeeDlgWizard = class(TForm)
    PageControl: TPageControl;
    TabStyle: TTabSheet;
    TableName: TTabSheet;
    Label3: TLabel;
    Buttons: TTabSheet;
    Label2: TLabel;
    TabSheet1: TTabSheet;
    Label4: TLabel;
    TabSheet2: TTabSheet;
    Table1: TTable;
    Panel1: TPanel;
    Panel2: TPanel;
    Label6: TLabel;
    cbLabelsFields: TComboBox;
    PageControl1: TPageControl;
    TabPreviewChart: TTabSheet;
    PreviewChart: TDBChart;
    Panel7: TPanel;
    CB3DOption: TCheckBox;
    CBShowLegendOption: TCheckBox;
    CBShowMarksOption: TCheckBox;
    Panel8: TPanel;
    Panel4: TPanel;
    Image2: TImage;
    LabelURL: TLabel;
    RG3D: TRadioGroup;
    Panel5: TPanel;
    Button1: TButton;
    PrevButton: TButton;
    NextButton: TButton;
    CancelBtn: TButton;
    tmpGallery: TChartGalleryPanel;
    RGDatabase: TRadioGroup;
    Label1: TLabel;
    CBAlias: TComboBox;
    Button6: TButton;
    Label5: TLabel;
    CBTables: TComboBox;
    DataSource1: TDataSource;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure CancelClick(Sender: TObject);
    procedure PrevClick(Sender: TObject);
    procedure NextClick(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure CB3DOptionClick(Sender: TObject);
    procedure CBShowLegendOptionClick(Sender: TObject);
    procedure CBShowMarksOptionClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LabelURLClick(Sender: TObject);
    procedure RG3DClick(Sender: TObject);
    procedure tmpGallerySelectedChart(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button6Click(Sender: TObject);
    procedure CBTablesChange(Sender: TObject);
    procedure CBAliasChange(Sender: TObject);
  private
    { Private declarations }
    SourceBuffer : PChar;
    BackupDir    : String;
    IFields      : TSelectListForm;

    Procedure AddControls(AForm:TCustomForm);
    procedure ChangedList(Sender:TObject);
    Procedure CreateSeries( AOwner:TComponent;
                            AChart:TCustomChart;
                            ATable:TTable;
                            ShowData:Boolean );
    {$IFDEF C3}
    function CreateCppSource(const FormIdent: string): TMemoryStream;
    function CreateHeader(const UnitIdent, FormIdent: string): TMemoryStream;
    {$ENDIF}
    function CreateSource(const UnitIdent, FormIdent: string): TMemoryStream;
    function CreateForm(const FormIdent: string): TMemoryStream;
    function DoFormCreation(const FormIdent: string): TForm;
    procedure FmtWrite(Stream: TStream; Const Fmt: String; const Args: array of const);
    procedure RefreshButtons;
  public
    { Public declarations }
  end;

procedure TeeChartWizard(ToolServices: TIToolServices);

implementation

uses Proxies, VirtIntf, IStreams, TeeAbout, Series, TeePenDlg, TeCanvas;

{$R *.DFM}

{ page numbers }
const
  pgStyle       = 0;  { Database vs. Non-Database }
  pgTable       = 1;  { Table name }
  pgFields      = 2;  { Fields }
  pgGallery     = 3;  { Chart Type }
  pgPreviewChart= 4;  { Preview }

  TeeMsg_WizardTable1   = 'Table1'; {  <-- dont translate }

  SourceBufferSize = 1024;

{ TTeeDlgWizard }
procedure TTeeDlgWizard.FormCreate(Sender: TObject);
begin
  GetDir(0,BackupDir);
  CBAlias.Text:=BackupDir;
  PageControl.ActivePage:=TabStyle;
  RefreshButtons;
  With tmpGallery do
  begin
    FunctionsVisible:=False;
    CreateChartList([ TLineSeries,
                      TBarSeries,
                      THorizBarSeries,
                      TAreaSeries,
                      TPointSeries,
                      TPieSeries,
                      TFastLineSeries ]);
  end;
  With TTeeAboutForm.Create(Self) do
  try
    Self.Image1.Picture.Assign(Image2.Picture);
    Self.Image2.Picture.Assign(Image1.Picture);
  finally
    Free;
  end;
end;

procedure TTeeDlgWizard.CancelClick(Sender: TObject);
begin
  Close;
end;

procedure TTeeDlgWizard.PrevClick(Sender: TObject);
begin
  With PageControl do
  case ActivePage.PageIndex of
    pgStyle  : Exit;
    pgTable  : ActivePage := Pages[pgStyle];
    pgFields : ActivePage := Pages[pgTable];
    pgGallery: if RGDatabase.ItemIndex=0 then ActivePage := Pages[pgFields]
                                         else ActivePage := Pages[pgStyle];
    pgPreviewChart: ActivePage := Pages[pgGallery];
  end;
  RefreshButtons;
end;

procedure TTeeDlgWizard.NextClick(Sender: TObject);

  Procedure ActiveGallery;
  begin
    PageControl.ActivePage:=PageControl.Pages[pgGallery];
    if PreviewChart.View3D then RG3D.ItemIndex:=1
                           else RG3D.ItemIndex:=0;
  end;

var t : Integer;
begin
  case PageControl.ActivePage.PageIndex of
    pgStyle:  if RGDatabase.ItemIndex=0 then { init table selection }
              begin
                PageControl.ActivePage := PageControl.Pages[pgTable];
                Session.GetAliasNames(CBAlias.Items);
                CBAliasChange(Self);
              end
              else ActiveGallery;
    pgTable:  begin
                PageControl.ActivePage := PageControl.Pages[pgFields];
                Table1.Close;
                Table1.DatabaseName:=CBAlias.Text;
                Table1.TableName:=CBTables.Text;
                Table1.FieldDefs.Update;
                if not Assigned(IFields) then
                begin
                  IFields:=TSelectListForm.Create(Self);
                  IFields.OnChange:=ChangedList;
                  AddFormTo(IFields,Panel1,0);
                end;
                IFields.FromList.Clear;
                IFields.ToList.Clear;
                CBLabelsFields.Clear;
                for t:=0 to Table1.FieldDefs.Count-1 do
                With Table1.FieldDefs[t] do
                Case TeeFieldType(DataType) of
                  tftNumber,
                  tftDateTime: begin
                               IFields.FromList.Items.Add(Name);
                               CBLabelsFields.Items.Add(Name);
                             end;
                  tftText: CBLabelsFields.Items.Add(Name);
                end;
                IFields.EnableButtons;
                for t:=0 to PreviewChart.SeriesCount-1 do
                    PreviewChart.Series[t].DataSource:=nil;
                Table1.Open;
              end;
    pgFields :  ActiveGallery;
    pgGallery:  tmpGallerySelectedChart(Self);
    pgPreviewChart: begin
                      ModalResult := mrOK;
                      Exit;
                    end;
  end;
  RefreshButtons;
end;

procedure TTeeDlgWizard.ChangedList(Sender:TObject);
begin
  NextButton.Enabled:=TSelectListForm(Sender).ToList.Items.Count>0;
end;

procedure TTeeDlgWizard.RefreshButtons;
begin
  PrevButton.Enabled := PageControl.ActivePage.PageIndex > 0;
  With NextButton do
  begin
    Case PageControl.ActivePage.PageIndex of
      pgTable:  Enabled:=CBTables.Text<>'';
      pgFields: Enabled:=IFields.ToList.Items.Count>0;
    else
      Enabled:=True;
    end;
    if PageControl.ActivePage.PageIndex=pgPreviewChart then
       Caption := TeeMsg_ExpFinish
    else
       Caption := TeeMsg_ExpNext;
  end;
end;

Procedure TTeeDlgWizard.CreateSeries( AOwner:TComponent;
                                      AChart:TCustomChart;
                                      ATable:TTable;
                                      ShowData:Boolean );
var  t         : Integer;
     tmpSeries : TChartSeries;
     theField  : TField;
     tmpSeriesClass: TChartSeriesClass;
     NumSeries : Integer;
begin
  if Assigned(ATable) then
     NumSeries:=MaxLong(1,IFields.ToList.Items.Count)
  else
     NumSeries:=2;
  tmpSeriesClass:= TChartSeriesClass(tmpGallery.SelectedChart.Series[0].ClassType);
  if tmpSeriesClass=TPieSeries then NumSeries:=1;

  AChart.FreeAllSeries;
  for t:=0 to NumSeries-1 do
  begin
    tmpSeries:=tmpSeriesClass.Create(AOwner);
    With tmpSeries do
    begin
      ParentChart:=AChart;
      if Assigned(ATable) then
      begin
        if AChart is TDBChart then TDBChart(AChart).AutoRefresh:=False;
        DataSource:=ATable;
        MandatoryValueList.ValueSource:=IFields.ToList.Items[t];
        TheField:=ATable.FieldByName(MandatoryValueList.ValueSource);
        if (t=0) and (cbLabelsFields.ItemIndex>=0) then
           XLabelsSource:=cbLabelsFields.Items[cbLabelsFields.ItemIndex];
        case TheField.DataType of
          ftDate,
          ftTime,
          ftDateTime: MandatoryValueList.DateTime:=True;
        else
          MandatoryValueList.DateTime:=False;
        end;
        GetHorizAxis.Title.Caption:='';
        GetVertAxis.Title.Caption:='';
        if YMandatory then
        begin
          XValues.DateTime:=False;
          if NumSeries=1 then  GetVertAxis.Title.Caption:=YValues.ValueSource;
          if XLabelsSource<>'' then GetHorizAxis.Title.Caption:=XLabelsSource;
        end
        else
        begin
          YValues.DateTime:=False;
          if NumSeries=1 then GetHorizAxis.Title.Caption:=XValues.ValueSource;
          if XLabelsSource<>'' then GetVertAxis.Title.Caption:=XLabelsSource;
        end;
        Title:=MandatoryValueList.ValueSource;
      end
      else FillSampleValues(NumSampleValues);
      Name:=TeeMsg_DefaultSeriesName+IntToStr(AChart.SeriesCount);
      ColorEachPoint:=NumSeries=1;
      Marks.Style:=smsValue;
      Marks.Visible:=CBShowMarksOption.Checked;
      if AChart is TDBChart then
        TDBChart(AChart).AutoRefresh:=True;
    end;
  end;
  AChart.View3D:=tmpGallery.SelectedChart.View3D;
  AChart.Title.Text.Clear;
  if Assigned(ATable) then
  begin
    AChart.Title.Text.Add(ATable.TableName);
    if ShowData and (AChart is TDBChart) then TDBChart(AChart).RefreshData;
  end
  else AChart.Title.Text.Add('TChart');  { <-- do not translate }
end;

Procedure TTeeDlgWizard.AddControls(AForm:TCustomForm);
var tmpChart : TCustomChart;
    tmpTable : TTable;
begin
  if RGDatabase.ItemIndex=0 then
  begin
    tmpTable:=TTable.Create(AForm);
    With tmpTable do
    begin
      Left:=12;
      Top:=8;
      Name:=TeeMsg_WizardTable1;
      DatabaseName:=CBAlias.Text;
      TableName:=CBTables.Text;
      Open;
    end;
    tmpChart:=TDBChart.Create(AForm);
  end
  else
  begin
    tmpTable:=nil;
    tmpChart:=TChart.Create(AForm);
  end;
  With tmpChart do
  begin
    Name:=TeeGetUniqueName(AForm,Copy(ClassName,2,Length(ClassName)));
    Left:=48;
    Top:=8;
    Parent:=AForm;
    Assign(PreviewChart as TCustomChart);
  end;
  CreateSeries(AForm,tmpChart,tmpTable,False);
end;

{ Create the dialog defined by the user }
function TTeeDlgWizard.DoFormCreation(const FormIdent: string): TForm;
begin
  Result := TForm.Create(nil);
  Proxies.CreateSubClass(Result, 'T' + FormIdent, TForm);  { <-- dont translate }
  with Result do
  begin
    Name    :=FormIdent;
    Caption :=FormIdent;
    Width:=470;
    Height:=300;
    with Font do
    begin
      Name := GetDefaultFontName;
      Size := GetDefaultFontSize;
    end;
  end;
  AddControls(result);
end;

procedure TTeeDlgWizard.FmtWrite(Stream: TStream; Const Fmt: String;
  const Args: array of const);
begin
  if Assigned(Stream) and Assigned(SourceBuffer) then
  begin
    StrLFmt(SourceBuffer, SourceBufferSize, @Fmt[1], Args);
    Stream.Write(SourceBuffer[0], StrLen(SourceBuffer));
  end;
end;

{$IFDEF C3}
const
  CRLF = #13#10;
  DashLine =
  '//----------------------------------------------------------------------------';

function TTeeDlgWizard.CreateHeader(const UnitIdent, FormIdent:                                         string): TMemoryStream;
var t: Integer;
begin
  SourceBuffer := StrAlloc(SourceBufferSize);
  try
    Result := TMemoryStream.Create;
    try
      FmtWrite(Result,
        DashLine + CRLF +
        '#ifndef %0:sH' + CRLF +
        '#define %0:sH' + CRLF +
        DashLine + CRLF +
        '#include <vcl\Classes.hpp>' + CRLF +
        '#include <vcl\Controls.hpp>' + CRLF +
        '#include <vcl\StdCtrls.hpp>' + CRLF +
        '#include <vcl\Forms.hpp>' + CRLF +
        '#include <vcl\TeEngine.hpp>' + CRLF +
        '#include <vcl\TeeProcs.hpp>' + CRLF +
        '#include <vcl\Chart.hpp>' + CRLF, [UnitIdent]);
      if rbDatabase.Checked then
        FmtWrite(Result,
          '#include <vcl\DBChart.hpp>' + CRLF +
          '#include <vcl\DB.hpp>' + CRLF +
          '#include <vcl\DBTables.hpp>' + CRLF, [nil]);

      FmtWrite(Result, DashLine + CRLF, [nil]);

      FmtWrite(Result,
        'class T%s : public TForm' + CRLF +
        '{' + CRLF +
        '__published:' + CRLF, [FormIdent]);

      if rbDatabase.Checked then
      begin
         FmtWrite(Result,'TTable *Table1;' + CRLF, [nil]);
         FmtWrite(Result,'TDBChart *DBChart1;' + CRLF, [nil]);
      end
      else
         FmtWrite(Result,'TChart *Chart1;' + CRLF, [nil]);

      for t:=0 to PreviewChart.SeriesCount-1 do
      begin
        FmtWrite(Result,
           '%s *%s;'+CRLF,
           [PreviewChart[t].ClassName,PreviewChart[t].Name]);
      end;

      FmtWrite(Result,'private:'+CRLF +
               'public:' +CRLF +
               '  __fastcall T%0:s::T%0:s(TComponent* Owner);' + CRLF +
               '};' + CRLF ,[FormIdent]);
      FmtWrite(Result,
        DashLine + CRLF +
        'extern  T%0:s *%0:s;' + CRLF +
        DashLine + CRLF +
        '#endif', [FormIdent]);
      Result.Position := 0;
    except
      Result.Free;
      raise;
    end;
  finally
    StrDispose(SourceBuffer);
  end;
end;

function TTeeDlgWizard.CreateCppSource(const FormIdent: string): TMemoryStream;
begin
  SourceBuffer := StrAlloc(SourceBufferSize);
  try
    Result := TMemoryStream.Create;
    try
      FmtWrite(Result,
        DashLine + CRLF +
        '#include <vcl\vcl.h>' + CRLF +
        '#pragma hdrstop' + CRLF +
        CRLF +
        '#include "%0:s.h"' + CRLF +
        DashLine + CRLF +
        '#pragma resource "*.dfm"' + CRLF +
        'T%1:s *%1:s;' + CRLF +
        DashLine + CRLF +
        '__fastcall T%1:s::T%1:s(TComponent* Owner)' + CRLF +
        '    : TForm(Owner)' + CRLF +
        '{' + CRLF +
        '}' + CRLF +
        DashLine, [FormIdent, FormIdent]);
      Result.Position := 0;
    except
      Result.Free;
      raise;
    end;
  finally
    StrDispose(SourceBuffer);
  end;
end;
{$ENDIF}

function TTeeDlgWizard.CreateSource(const UnitIdent, FormIdent: string): TMemoryStream;
const  CRLF = #13#10;
var  t: Integer;
begin
  SourceBuffer := StrAlloc(SourceBufferSize);
  try
    Result := TMemoryStream.Create;
    try
      { unit header and uses clause }
      FmtWrite(Result,
        'unit %s;' + CRLF + CRLF +
        'interface' + CRLF + CRLF +
        'uses'+CRLF +
        '  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,'+CRLF+
        '  StdCtrls, ExtCtrls, TeEngine, TeeProcs, Chart', [UnitIdent]);

      { additional units that may be needed }
      if RGDatabase.ItemIndex=0 then FmtWrite(Result, ', DBChart, DB, DBTables', [nil]);

      FmtWrite(Result, ';' + CRLF + CRLF, [nil]);

      { begin the class declaration }
      FmtWrite(Result,
        'type'+CRLF +
        '  T%s = class(TForm)'+CRLF, [FormIdent]);

      { add variable declarations }

      if RGDatabase.ItemIndex=0 then
      begin
        FmtWrite(Result,'    Table1 : TTable;' + CRLF, [nil]);
        FmtWrite(Result,'    DBChart1 : TDBChart;' + CRLF, [nil]);
      end
      else
         FmtWrite(Result,'    Chart1 : TChart;' + CRLF, [nil]);

      for t:=0 to PreviewChart.SeriesCount-1 do
      With PreviewChart.Series[t] do
           FmtWrite(Result,'    %s: %s;'+CRLF,[Name,ClassName]);

      FmtWrite(Result,
        '  private'+CRLF+
        '    '+TeeMsg_PrivateDeclarations+CRLF+
        '  public'+CRLF+
        '    '+TeeMsg_PublicDeclarations+CRLF+
        '  end;' + CRLF + CRLF +
        'var' + CRLF +
        '  %s: T%s;' + CRLF + CRLF +
        'implementation' + CRLF + CRLF +
        '{$R *.DFM}' + CRLF + CRLF, [FormIdent, FormIdent]);

      FmtWrite(Result, 'end.' + CRLF, [nil]);
      Result.Position := 0;
    except
      Result.Free;
      raise;
    end;
  finally
    StrDispose(SourceBuffer);
  end;
end;

function TTeeDlgWizard.CreateForm(const FormIdent: string): TMemoryStream;
var ChartForm : TForm;
begin
  ChartForm:=DoFormCreation(FormIdent);
  try
    Result:=TMemoryStream.Create;
    Result.WriteComponentRes(FormIdent, ChartForm);
    Result.Position:=0;
  finally
    ChartForm.Free;
  end;
end;

procedure TTeeDlgWizard.Image1Click(Sender: TObject);
begin
  TeeShowAboutBox;
end;

procedure TeeChartWizard(ToolServices: TIToolServices);
var D             : TTeeDlgWizard;
    {$IFDEF C3}
    IHeaderStream : IStream;
    ISourceStream : IStream;
    IFormStream   : IStream;
    {$ELSE}
    ISourceStream : TIMemoryStream;
    IFormStream   : TIMemoryStream;
    {$ENDIF}
    UnitIdent     : String;
    FormIdent     : String;
    FileName      : String;
begin
  if ToolServices = nil then Exit;
  if ToolServices.GetNewModuleName(UnitIdent, FileName) then
  begin
    D := TTeeDlgWizard.Create(Application);
    try
      if D.ShowModal = mrOK then
      begin
        UnitIdent := LowerCase(UnitIdent);
        UnitIdent[1] := Upcase(UnitIdent[1]);
        FormIdent := 'Form' + Copy(UnitIdent, 5, 255);

        IFormStream := TIMemoryStream.Create(D.CreateForm(FormIdent));
        {$IFDEF C3}
          IHeaderStream := TIMemoryStream.Create(D.CreateHeader(UnitIdent,FormIdent), soOwned);
          ISourceStream := TIMemoryStream.Create(D.CreateCppSource(UnitIdent,FormIdent), soOwned);
          ToolServices.CreateCppModule(FileName, '', '', '', IHeaderStream,
            ISourceStream, IFormStream, [cmAddToProject, cmShowSource,
            cmShowForm, cmUnNamed, cmMarkModified]);
        {$ELSE}
          try
            {$IFNDEF D4}
            IFormStream.AddRef;
            {$ENDIF}
            ISourceStream := TIMemoryStream.Create(D.CreateSource(UnitIdent,FormIdent));
            try
              {$IFNDEF D4}
              ISourceStream.AddRef;
              {$ENDIF}
              ToolServices.CreateModule(FileName, ISourceStream, IFormStream,
                [ cmAddToProject, cmShowSource, cmShowForm, cmUnNamed,
                  cmMarkModified]);
            finally
              {$IFNDEF D4}
              ISourceStream.OwnStream := True;
              ISourceStream.Free;
              {$ENDIF}
            end;
          finally
            {$IFNDEF D4}
            IFormStream.OwnStream := True;
            IFormStream.Free;
            {$ENDIF}
          end;
        {$ENDIF}
      end;
    finally
      D.Free;
    end;
  end;
end;

procedure TTeeDlgWizard.CB3DOptionClick(Sender: TObject);
begin
  PreviewChart.View3D:=CB3DOption.Checked;
  if CB3DOption.Checked then RG3D.ItemIndex:=1
                        else RG3D.ItemIndex:=0;
end;

procedure TTeeDlgWizard.CBShowLegendOptionClick(Sender: TObject);
begin
  PreviewChart.Legend.Visible:=CBShowLegendOption.Checked;
end;

procedure TTeeDlgWizard.CBShowMarksOptionClick(Sender: TObject);
var t : Integer;
begin
  for t:=0 to PreviewChart.SeriesCount-1 do
     PreviewChart.Series[t].Marks.Visible:=CBShowMarksOption.Checked;
end;

procedure TTeeDlgWizard.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose:=(ModalResult<>mrCancel) or TeeYesNo(TeeMsg_WizardSureToClose);
end;

procedure TTeeDlgWizard.Button1Click(Sender: TObject);
begin
  Application.HelpJump('TeeChart_Wizard');  { <-- don't translate }
end;

procedure TTeeDlgWizard.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  {$I-}
  ChDir(BackupDir);
  if IOResult<>0 then ;
  {$I+}
end;

procedure TTeeDlgWizard.LabelURLClick(Sender: TObject);
begin
  GotoURL(Handle,LabelURL.Caption);
end;

procedure TTeeDlgWizard.RG3DClick(Sender: TObject);
begin
  tmpGallery.View3D:=RG3D.ItemIndex=1;
end;

procedure TTeeDlgWizard.tmpGallerySelectedChart(Sender: TObject);
begin
  PageControl.ActivePage := PageControl.Pages[pgPreviewChart];
  if RGDatabase.ItemIndex=0 then
     CreateSeries(Self,PreviewChart,Self.Table1,True)
  else
     CreateSeries(Self,PreviewChart,nil,True);
  CB3DOption.Checked:=PreviewChart.View3D;
  CBShowLegendOption.Checked:=PreviewChart.Legend.Visible;
  CBShowMarksOption.Visible:=PreviewChart.SeriesCount>0;
  if PreviewChart.SeriesCount>0 then
     CBShowMarksOption.Checked:=PreviewChart.Series[0].Marks.Visible;
  PageControl1.ActivePage:=TabPreviewChart;
  RefreshButtons;
end;

procedure TTeeDlgWizard.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  tmpGallery.KeyDown(Key,Shift);
end;

procedure TTeeDlgWizard.Button6Click(Sender: TObject);
Var tmpDir : String;
begin
  tmpDir:=CBAlias.Text;
  if not DirectoryExists(tmpDir) then tmpDir:='';
  if SelectDirectory(tmpDir,[],0) then
  begin
    CBAlias.Text:=tmpDir;
    CBAliasChange(Self);
  end;
end;

procedure TTeeDlgWizard.CBTablesChange(Sender: TObject);
begin
  NextButton.Enabled:=True;
end;

procedure TTeeDlgWizard.CBAliasChange(Sender: TObject);
begin
  try
    Session.GetTableNames(CBAlias.Text,'',True,False,CBTables.Items);
  except
    CBAlias.Text:='';
    CBTables.Clear;
  end;
end;

end.



