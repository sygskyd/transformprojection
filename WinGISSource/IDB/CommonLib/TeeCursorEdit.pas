{******************************************}
{ TCursorTool Editor Dialog                }
{ Copyright (c) 1999-2000 by David Berneda }
{******************************************}
{$I teedefs.inc}
unit TeeCursorEdit;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Chart, TeeTools, ComCtrls,
  TeCanvas, TeePenDlg, TeeToolSeriesEdit;

type
  TCursorToolEditor = class(TSeriesToolEditor)
    BPen: TButtonPen;
    Label2: TLabel;
    CBStyle: TComboBox;
    CBSnap: TCheckBox;
    CBFollow: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure CBSeriesChange(Sender: TObject);
    procedure CBStyleChange(Sender: TObject);
    procedure CBSnapClick(Sender: TObject);
    procedure CBFollowClick(Sender: TObject);
  private
    { Private declarations }
    CursorTool : TCursorTool;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeEngine, TeeConst, TeeProco;

procedure TCursorToolEditor.FormShow(Sender: TObject);
begin
  inherited;
  CursorTool:=TCursorTool(Tag);
  With CursorTool do
  begin
    CBStyle.ItemIndex :=Ord(Style);
    CBSnap.Checked    :=Snap;
    CBSnap.Enabled    :=Assigned(Series);
    CBFollow.Checked  :=FollowMouse;
    BPen.LinkPen(Pen);
  end;
end;

procedure TCursorToolEditor.CBSeriesChange(Sender: TObject);
begin
  inherited;
  CBSnap.Enabled:=Assigned(CursorTool.Series);
end;

procedure TCursorToolEditor.CBStyleChange(Sender: TObject);
begin
  CursorTool.Style:=TCursorToolStyle(CBStyle.ItemIndex);
end;

procedure TCursorToolEditor.CBSnapClick(Sender: TObject);
begin
  CursorTool.Snap:=CBSnap.Checked
end;

procedure TCursorToolEditor.CBFollowClick(Sender: TObject);
begin
  CursorTool.FollowMouse:=CBFollow.Checked
end;

initialization
  RegisterClass(TCursorToolEditor);
end.
