unit TeeWaterFallEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeeSurfEdit, StdCtrls, ExtCtrls, TeCanvas, TeePenDlg;

type
  TWaterFallEditor = class(TSurfaceSeriesEditor)
    BLines: TButtonPen;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

Uses TeeSurfa;

procedure TWaterFallEditor.FormShow(Sender: TObject);
begin
  inherited;
  BLines.LinkPen(TWaterFallSeries(Tag).WaterLines);
end;

initialization
  RegisterClass(TWaterFallEditor);
end.
