{**********************************************}
{  TCustomChart (or derived) Editor Dialog     }
{  Copyright (c) 1996-2000 by David Berneda    }
{**********************************************}
{$I teedefs.inc}
unit TeeEdiPage;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QComCtrls, QExtCtrls,
     TeePenDlg,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls,
     {$ENDIF}
     TeeProcs, Chart, TeeNavigator;

type
  TFormTeePage = class(TForm)
    L17: TLabel;
    SEPointsPerPage: TEdit;
    CBScaleLast: TCheckBox;
    LabelPages: TLabel;
    UDPointsPerPage: TUpDown;
    CBPageLegend: TCheckBox;
    ChartPageNavigator1: TChartPageNavigator;
    procedure SEPointsPerPageChange(Sender: TObject);
    procedure CBScaleLastClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CBPageLegendClick(Sender: TObject);
    procedure ChartPageNavigator1ButtonClicked(Index: TChartNavigateBtn);
  private
    { Private declarations }
  public
    { Public declarations }
    Constructor CreateChart(Owner:TComponent; AChart:TCustomChart);
  end;

implementation

{$R *.dfm}

Uses TeeConst;

{ TFormTeePage }
Constructor TFormTeePage.CreateChart(Owner:TComponent; AChart:TCustomChart);
begin
  inherited Create(Owner);
  ChartPageNavigator1.Chart:=AChart;
end;

procedure TFormTeePage.SEPointsPerPageChange(Sender: TObject);
begin
  if Showing then
  begin
    With ChartPageNavigator1.Chart do
    if SEPointsPerPage.Text='' then MaxPointsPerPage:=0
                               else MaxPointsPerPage:=UDPointsPerPage.Position;
    ChartPageNavigator1ButtonClicked(nbCancel);
  end;
end;

procedure TFormTeePage.CBScaleLastClick(Sender: TObject);
begin
  ChartPageNavigator1.Chart.ScaleLastPage:=CBScaleLast.Checked;
end;

procedure TFormTeePage.FormShow(Sender: TObject);
begin
  With ChartPageNavigator1.Chart do
  begin
    UDPointsPerPage.Position :=MaxPointsPerPage;
    CBScaleLast.Checked      :=ScaleLastPage;
    CBPageLegend.Checked     :=Legend.CurrentPage;
    ChartPageNavigator1ButtonClicked(nbCancel);
  end;
end;

procedure TFormTeePage.CBPageLegendClick(Sender: TObject);
begin
  ChartPageNavigator1.Chart.Legend.CurrentPage:=CBPageLegend.Checked;
end;

procedure TFormTeePage.ChartPageNavigator1ButtonClicked(
  Index: TChartNavigateBtn);
begin
  With ChartPageNavigator1.Chart do
      LabelPages.Caption:=Format(TeeMsg_PageOfPages,[Page,NumPages]);
end;

end.
