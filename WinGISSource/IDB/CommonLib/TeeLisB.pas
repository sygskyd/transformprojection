{***************************************}
{ TeeChart Pro - TChartListBox class    }
{ Copyright (c) 1995-2000 David Berneda }
{     Component Registration Unit       }
{***************************************}
{$I teedefs.inc}
unit TeeLisB;

interface

Uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     Classes, Chart, TeeProcs, TeCanvas, TeEngine,
     {$IFDEF CLX}
     QStdCtrls, QGraphics, QForms, QControls, QButtons
     {$ELSE}
     StdCtrls, Graphics, Forms, Controls, Buttons
     {$ENDIF}
     {$IFDEF D6}, Types{$ENDIF};

type TChartListBox=class;

     TDblClickSeriesEvent=procedure(Sender:TChartListBox; Index:Integer) of object;

     TListBoxSection=Packed record
       Width   : Integer;
       Visible : Boolean;
     end;

     TListBoxSections=Array[0..3] of TListBoxSection;

     TChartListBox=class(TCustomListBox,ITeeEventListener)
     private
       FAllowAdd         : Boolean;
       FAllowDelete      : Boolean;
       FAskDelete        : Boolean;
       FChart            : TCustomChart;
       FEnableChangeColor: Boolean;
       FEnableDragSeries : Boolean;
       FEnableChangeType : Boolean;
       {$IFNDEF CLX}
       FHitTest          : TPoint;
       {$ENDIF}
       FOnEditSeries     : TDblClickSeriesEvent;
       FOtherItems       : TStrings;
       FOtherItemsChange : TNotifyEvent;
       FRefresh          : TNotifyEvent;
       ComingFromDoubleClick:Boolean;

       procedure DoRefresh;
       Function GetSelectedSeries:TChartSeries;
       Function GetShowActive:Boolean;
       Function GetShowIcon:Boolean;
       Function GetShowColor:Boolean;
       Function GetShowTitle:Boolean;
       function GetSeries(Index: Integer): TChartSeries;
       procedure LBSeriesClick(Sender: TObject);
       {$IFDEF CLX}
       procedure LBSeriesDrawItem(Sender: TObject; Index: Integer;
        Rect: TRect; State: TOwnerDrawState; var Handled: Boolean);
       {$ELSE}
       procedure LBSeriesDrawItem(Control: TWinControl; Index: Integer;
                                           Rect: TRect; State: TOwnerDrawState);
       {$ENDIF}
       procedure LBSeriesDragOver( Sender, Source: TObject; X,
                                   Y: Integer; State: TDragState; var Accept: Boolean);
       Procedure RefreshDesigner;
       Function SectionLeft(ASection:Integer):Integer;
       procedure SetChart(Value:TCustomChart);
       Procedure SetSelectedSeries(Value:TChartSeries);
       Procedure SetShowActive(Value:Boolean);
       Procedure SetShowIcon(Value:Boolean);
       Procedure SetShowColor(Value:Boolean);
       Procedure SetShowTitle(Value:Boolean);

       procedure TeeEvent(Event: TTeeEvent);  { interface }
     protected
       procedure DblClick; override;
       procedure KeyUp(var Key: Word; Shift: TShiftState); override;
       procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
                           X, Y: Integer); override;
       procedure Notification(AComponent: TComponent;
                              Operation: TOperation); override;
       {$IFNDEF CLX}
       procedure WMNCHitTest(var Msg: TWMNCHitTest); message WM_NCHITTEST;
       procedure WMSetCursor(var Msg: TWMSetCursor); message WM_SETCURSOR;
       {$ENDIF}
       {$IFNDEF CLX}
       {$IFNDEF D4}
       { IUnknown }
       function QueryInterface(const IID: TGUID; out Obj): Integer; stdcall;
       function _AddRef: Integer; stdcall;
       function _Release: Integer; stdcall;
       {$ENDIF}
       {$ENDIF}
     public
       Sections : TListBoxSections;

       Constructor Create(AOwner:TComponent); override;
       Destructor Destroy; override;

       Function AddSeriesGallery:TChartSeries;
       procedure ChangeTypeSeries(Sender: TObject);
       procedure ClearItems;
       Procedure CloneSeries;
       Procedure DeleteSeries;
       procedure DragDrop(Source: TObject; X, Y: Integer); override;
       procedure FillSeries(OldSeries:TChartSeries);
       Procedure MoveCurrentDown;
       Procedure MoveCurrentUp;
       property OtherItems:TStrings read FOtherItems write FOtherItems;
       Function PointInSection(Const P:TPoint; ASection:Integer):Boolean;
       Procedure RenameSeries;
       Procedure SelectAll; {$IFNDEF CLX}{$IFDEF D6} override; {$ENDIF}{$ENDIF}
       property Series[Index:Integer]:TChartSeries read GetSeries;
       Function SeriesAtMousePos(Var p:TPoint):Integer;
       property SelectedSeries:TChartSeries read GetSelectedSeries
                                            write SetSelectedSeries;
       procedure SwapSeries(tmp1,tmp2:Integer);
       procedure UpdateSeries;
     published
       property AllowAddSeries : Boolean read FAllowAdd
                                  write FAllowAdd default True;
       property AllowDeleteSeries : Boolean read FAllowDelete
                                  write FAllowDelete default True;
       property AskDelete:Boolean read FAskDelete write FAskDelete
                                  default True;
       property Chart:TCustomChart read FChart write SetChart;
       property EnableChangeColor:Boolean read FEnableChangeColor
                                          write FEnableChangeColor default True;
       property EnableDragSeries:Boolean read FEnableDragSeries
                                            write FEnableDragSeries default True;
       property EnableChangeType:Boolean read FEnableChangeType
                                         write FEnableChangeType default True;
       property OnOtherItemsChange:TNotifyEvent read FOtherItemsChange
                                              write FOtherItemsChange;
       property OnDblClickSeries:TDblClickSeriesEvent read FOnEditSeries
                                                  write FOnEditSeries;
       property OnRefresh:TNotifyEvent read FRefresh write FRefresh;
       property ShowActiveCheck:Boolean read GetShowActive
                                        write SetShowActive default True;
       property ShowSeriesColor:Boolean read GetShowColor
                                           write SetShowColor default True;
       property ShowSeriesIcon:Boolean read GetShowIcon
                                           write SetShowIcon default True;
       property ShowSeriesTitle:Boolean read GetShowTitle
                                           write SetShowTitle default True;

       property Align;
       property BorderStyle;
       property Color;
       {$IFNDEF CLX}
       property Ctl3D;
       {$ENDIF}
       property Enabled;
       property ExtendedSelect;
       property Font;
       {$IFNDEF CLX}
       property ImeMode;
       property ImeName;
       {$ENDIF}
       property MultiSelect;
       property ParentColor;
       {$IFNDEF CLX}
       property ParentCtl3D;
       {$ENDIF}
       property ParentFont;
       property ParentShowHint;
       {$IFNDEF TEEOCX}
       property PopupMenu;
       {$ENDIF}
       property ShowHint;
       property TabOrder;
       property TabStop;
       property Visible;
       property OnClick;
       property OnEnter;
       property OnExit;
       property OnKeyDown;
       property OnKeyPress;
       property OnKeyUp;
       property OnMouseDown;
       property OnMouseMove;
       property OnMouseUp;
       {$IFNDEF CLX}
       {$IFDEF D4}
       property OnStartDock;
       {$ENDIF}
       {$ENDIF}
       property OnStartDrag;
     end;

implementation

{$R TeeBmps.res}

Uses TeePenDlg, TeeGally,
     {$IFDEF CLX}
     QDialogs,
     {$ELSE}
     Dialogs,
     {$ENDIF}
     TeeConst, SysUtils;

{ TChartListBox }
Constructor TChartListBox.Create(AOwner:TComponent);
begin
  inherited;
  ComingFromDoubleClick:=False;

  FEnableChangeColor:=True;
  FEnableDragSeries:=True;
  FEnableChangeType:=True;

  Sections[0].Width:=26;  Sections[0].Visible:=True;
  Sections[1].Width:=16;  Sections[1].Visible:=True;
  Sections[2].Width:=26;  Sections[2].Visible:=True;
  Sections[3].Width:=216; Sections[3].Visible:=True;

  OnDrawItem:=LBSeriesDrawItem;
  OnDragOver:=LBSeriesDragOver;
  OnClick:=LBSeriesClick;

  Style:=lbOwnerDrawFixed;
  ItemHeight:=24;
  Sorted:=False;
  MultiSelect:=True;
  FAskDelete:=True;
  FAllowDelete:=True;
  FAllowAdd:=True;
end;

Destructor TChartListBox.Destroy;
begin
  Chart:=nil;
  inherited;
end;

procedure TChartListBox.DragDrop(Source: TObject; X,Y: Integer);
var tmp1 : Integer;
    tmp2 : Integer;
begin
  if ItemIndex<>-1 then
  begin
    tmp1:=ItemIndex;
    tmp2:=ItemAtPos(Point(X,Y){$IFNDEF CLX},True{$ENDIF});
    if (tmp2<>-1) and (tmp1<>tmp2) then SwapSeries(tmp1,tmp2);
  end;
end;

procedure TChartListBox.DoRefresh;
begin
  if Assigned(FRefresh) then FRefresh(Self);
end;

{$IFNDEF CLX}
{$IFNDEF D4}
function TChartListBox.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if VCLComObject = nil then
  begin
    if GetInterface(IID, Obj) then Result := S_OK
    else Result := E_NOINTERFACE
  end
  else
    Result := IVCLComObject(VCLComObject).QueryInterface(IID, Obj);
end;

function TChartListBox._AddRef: Integer;
begin
  if VCLComObject = nil then
    Result := -1   // -1 indicates no reference counting is taking place
  else
    Result := IVCLComObject(VCLComObject)._AddRef;
end;

function TChartListBox._Release: Integer;
begin
  if VCLComObject = nil then
    Result := -1   // -1 indicates no reference counting is taking place
  else
    Result := IVCLComObject(VCLComObject)._Release;
end;
{$ENDIF}
{$ENDIF}

type TTeePanelAccess=class(TCustomTeePanel);

procedure TChartListBox.SetChart(Value:TCustomChart);
begin
  if Assigned(Chart) and (not (csDestroying in Chart.ComponentState)) then
     TTeePanelAccess(Chart).Listeners.Remove(Self);
  FChart:=Value;
  if Assigned(Chart) then
  begin
    Chart.FreeNotification(Self);
    TTeePanelAccess(Chart).Listeners.Add(Self);
    FillSeries(nil);
  end
  else ClearItems;
end;

procedure TChartListBox.ClearItems;
begin
  if not (csDestroying in ComponentState) then
  begin
    Items.Clear;
    if Assigned(FOtherItems) then FOtherItems.Clear;
  end;
end;

procedure TChartListBox.SwapSeries(tmp1,tmp2:Integer);
var tmp : TCustomForm;
begin
  Items.Exchange(tmp1,tmp2);
  if Assigned(FOtherItems) then FOtherItems.Exchange(tmp1,tmp2);
  if Assigned(Chart) then Chart.ExchangeSeries(tmp1,tmp2);
  tmp:=GetParentForm(Self);
  if Assigned(tmp) then tmp.ActiveControl:=Self;
  Selected[tmp2]:=True;
  DoRefresh;
  RefreshDesigner;
end;

procedure TChartListBox.LBSeriesClick(Sender: TObject);
begin
  DoRefresh;
end;

Function TChartListBox.SectionLeft(ASection:Integer):Integer;
var t : Integer;
begin
  result:=0;
  for t:=0 to ASection-1 do
  if Sections[t].Visible then Inc(result,Sections[t].Width);
end;

type TSeriesAccess=class(TChartSeries);

{$IFDEF CLX}
procedure TChartListBox.LBSeriesDrawItem(Sender: TObject; Index: Integer;
     Rect: TRect; State: TOwnerDrawState; var Handled: Boolean);
{$ELSE}
procedure TChartListBox.LBSeriesDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
{$ENDIF}
Const BrushColors : Array[Boolean] of TColor=(clWindow,clHighLight);
      FontColors  : Array[Boolean] of TColor=(clWindowText,clHighlightText);
var tmp       : Integer;
    tmpSeries : TChartSeries;
    tmpR      : TRect;
    CBRect    : TRect;
    tmpCanvas : TCanvas;
    tmpCanvas2 : TCanvas3D;
    tmpBitmap : TBitmap;
begin
  tmpCanvas:=Canvas;
  With tmpCanvas do
  begin
    if odSelected in State then Brush.Color:=clHighLight
                           else Brush.Color:=Self.Color;
    FillRect(Rect);

    Brush.Color:=Self.Color;
    Brush.Style:=bsSolid;
    tmpR:=Rect;
    tmpR.Right:=SectionLeft(3)-2;
    FillRect(tmpR);

    tmpSeries:=Series[Index];

    if ShowSeriesIcon then
    begin
      tmpBitmap:=TBitmap.Create;
      try
        TSeriesAccess(tmpSeries).GetBitmapEditor(tmpBitmap);
        {$IFNDEF CLX}
        tmpBitmap.Transparent:=True;
        {$ENDIF}
        Draw(SectionLeft(0),Rect.Top,tmpBitmap);
      finally
        tmpBitmap.Free;
      end;
    end;

    if ShowSeriesColor and
       (TSeriesAccess(Series[Index]).IUseSeriesColor) then
    begin
      tmp:=SectionLeft(2)-2;
      tmpR:=Classes.Rect(tmp,Rect.Top,tmp+Sections[2].Width,Rect.Bottom);
      InflateRect(tmpR,-4,-4);
      PaintSeriesLegend(Series[Index],tmpCanvas,tmpR);
    end;

    if ShowActiveCheck then
    begin
      tmp:=SectionLeft(1);
      CBRect:=Classes.Rect(tmp+2,Rect.Top+6,tmp+12,Rect.Top+18);
      tmpCanvas2:=TTeeCanvas3D.Create;
      try
        tmpCanvas2.ReferenceCanvas:=tmpCanvas;
        TeeDrawCheckBox(CBRect.Left,CBRect.Top,tmpCanvas2,tmpSeries.Active,Self.Color);
      finally
        tmpCanvas2.Free;
      end;
    end;

    Brush.Style:=bsClear;
    if ShowSeriesTitle then
    begin
      if odSelected in State then Font.Color:=clHighlightText
                             else Font.Color:=Self.Font.Color;
      TextOut(SectionLeft(3)+1,Rect.Top+((ItemHeight-TextHeight('W')) div 2),Items[Index]);
    end
    else TextOut(0,0,'');
  end;
end;

Function TChartListBox.GetSelectedSeries:TChartSeries;
begin
  result:=Series[ItemIndex];
end;

Procedure TChartListBox.SetSelectedSeries(Value:TChartSeries);
begin
  ItemIndex:=Items.IndexOfObject(Value);
end;

procedure TChartListBox.LBSeriesDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept:=FEnableDragSeries and (Sender=Source);
end;

Function TChartListBox.SeriesAtMousePos(Var p:TPoint):Integer;
begin
  {$IFDEF D4}
  p:=ScreenToClient(Mouse.CursorPos);
  {$ELSE}
  Windows.GetCursorPos(P);
  P:=ScreenToClient(P);
  {$ENDIF}
  result:=ItemAtPos(p{$IFNDEF CLX},True{$ENDIF});
end;

Function TChartListBox.PointInSection(Const P:TPoint; ASection:Integer):Boolean;
Var tmpPos : Integer;
begin
  if Sections[ASection].Visible then
  begin
    tmpPos:=SectionLeft(ASection);
    result:=(p.x>tmpPos) and (p.x<tmpPos+Sections[ASection].Width);
  end
  else result:=False;
end;

procedure TChartListBox.FillSeries(OldSeries:TChartSeries);
var tmp : Integer;
Begin
  ClearItems;
  if Assigned(Chart) then FillSeriesItems(Items,Chart);
  if Assigned(FOtherItems) then FOtherItems.Assign(Items);

  tmp:=Items.IndexOfObject(OldSeries);
  if (tmp=-1) and (Items.Count>0) then tmp:=0;

  if tmp<>-1 then Selected[tmp]:=True;
  if Assigned(FOtherItemsChange) then FOtherItemsChange(Self);
  DoRefresh;
end;

procedure TChartListBox.ChangeTypeSeries(Sender: TObject);
var tmpSeries : TChartSeries;
    NewClass  : TChartSeriesClass;
    t         : Integer;
    tmp       : Integer;
    FirstTime : Boolean;
    tmpList   : TChartSeriesList;
begin
  if (SelCount>0) and Assigned(Chart) then
    begin
      FirstTime:=True;
      NewClass:=nil;
      tmpList:=TChartSeriesList.Create;
      try
        { get selected list of series }
        for t:=0 to Items.Count-1 do
            if Selected[t] then
               tmpList.Add(Series[t]);
        { change all selected... }
        for t:=0 to tmpList.Count-1 do
            begin
              tmpSeries:=tmpList[t];
              if FirstTime then
                 begin
                   ChangeSeriesTypeGallery(tmpSeries.Owner,tmpSeries);
                   NewClass:=TChartSeriesClass(tmpSeries.ClassType);
                   FirstTime:=False;
                 end
              else
                 ChangeSeriesType(tmpSeries,NewClass);
              tmpList[t]:=tmpSeries;
            end;
        { reset the selected list... }
        for t:=0 to tmpList.Count-1 do
            begin
              tmp:=Items.IndexOfObject(tmpList[t]);
              if tmp<>-1 then
                 Selected[tmp]:=True;
            end;
      finally
        tmpList.Free;
      end;
      { refill and repaint list }
      DoRefresh;
      { tell the series designer we have changed... }
      RefreshDesigner;
    end;
end;

procedure TChartListBox.DblClick;
var p   : TPoint;
    tmp : Integer;
begin
  ComingFromDoubleClick:=True;
  tmp:=SeriesAtMousePos(p);
  if (tmp<>-1) and Selected[tmp] then
  begin
    if PointInSection(p,0) and FEnableChangeType then ChangeTypeSeries(Self)
    else
    if PointInSection(p,2) and FEnableChangeColor then
    begin
      if TSeriesAccess(Series[tmp]).IUseSeriesColor then
      With Series[tmp] do
      begin
        SeriesColor:=EditColor(Self,SeriesColor);
        ColorEachPoint:=False;
      end;
    end
    else
    if PointInSection(p,3) then
       if Assigned(FOnEditSeries) then FOnEditSeries(Self,tmp);
  end;
end;

procedure TChartListBox.MouseDown(Button: TMouseButton; Shift: TShiftState;
                                  X, Y: Integer);
var tmp : Integer;
    p   : TPoint;
begin
  if ItemIndex<>-1 then
  begin
    tmp:=SeriesAtMousePos(p);
    if (tmp<>-1) and (PointInSection(p,1)) then
    begin
      with Series[tmp] do Active:=not Active;
      if not Assigned(Series[tmp].ParentChart) then Self.Repaint;
    end
    else
    if FEnableDragSeries and
       PointInSection(p,3) and (not ComingFromDoubleClick) and
       (not (ssShift in Shift)) and
       (not (ssCtrl in Shift)) and
       ( ( ssLeft in Shift) ) then
            BeginDrag(False);
    ComingFromDoubleClick:=False;
    if Assigned(FOtherItemsChange) then FOtherItemsChange(Self);
  end;
end;

Procedure TChartListBox.MoveCurrentUp;
begin
  if ItemIndex>0 then SwapSeries(ItemIndex,ItemIndex-1);
end;

Procedure TChartListBox.MoveCurrentDown;
begin
  if (ItemIndex<>-1) and (ItemIndex<Items.Count-1) then
     SwapSeries(ItemIndex,ItemIndex+1);
end;

Procedure TChartListBox.DeleteSeries;

  Procedure DoDelete;
  Var t : Integer;
  begin
    t:=0;
    While t<Items.Count do
    if Selected[t] then Series[t].Free
                   else Inc(t);
    if Items.Count>0 then Selected[0]:=True;
    DoRefresh;
    RefreshDesigner;
  end;

var tmpSt : String;
begin
  if SelCount>0 then
    if FAskDelete then
    begin
      if SelCount=1 then tmpSt:=SeriesTitleOrName(SelectedSeries)
                    else tmpSt:=TeeMsg_SelectedSeries;
      if TeeYesNo(Format(TeeMsg_SureToDelete,[tmpSt])) then DoDelete;
    end
    else DoDelete;
end;

Procedure TChartListBox.CloneSeries;
begin
  if ItemIndex<>-1 then
  begin
    CloneChartSeries(SelectedSeries);
    RefreshDesigner;
  end;
end;

procedure TChartListBox.KeyUp(var Key: Word; Shift: TShiftState);
begin
  Case Key of
    VK_INSERT: if FAllowAdd then AddSeriesGallery;
    VK_DELETE: if FAllowDelete then DeleteSeries;
  end;
end;

Procedure TChartListBox.RenameSeries;
var tmp       : Integer;
    tmpSeries : TChartSeries;
    tmpSt     : String;
begin
  tmp:=ItemIndex;
  if tmp<>-1 then
  begin
    tmpSeries:=SelectedSeries;
    tmpSt:=SeriesTitleOrName(tmpSeries);
    if InputQuery( TeeMsg_ChangeSeriesTitle,
                   TeeMsg_NewSeriesTitle,tmpSt) then
    if tmpSt<>'' then
       if tmpSt=tmpSeries.Name then tmpSeries.Title:='' else tmpSeries.Title:=tmpSt;
    Selected[tmp]:=True;
  end;
end;

Procedure TChartListBox.RefreshDesigner;
begin
  if Assigned(Chart) and Assigned(Chart.Designer) then Chart.Designer.Refresh;
end;

Function TChartListBox.AddSeriesGallery:TChartSeries;
var SubIndex : Integer;
    t        : Integer;
begin
   result:=nil;
   if Assigned(Chart) then
      begin
         result:=CreateNewSeriesGallery(Chart.Owner,SelectedSeries,Chart,True,False,SubIndex);
         if Assigned(result) then
            begin
              for t:=0 to Items.Count-1 do
                  Selected[t]:=False;
              Selected[Items.IndexOfObject(result)]:=True;
              DoRefresh;
              RefreshDesigner;
            end;
      end;
end;

procedure TChartListBox.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if Operation=opRemove then
     if Assigned(Chart) and (AComponent=Chart) then
        Chart:=nil;
end;

{$IFNDEF CLX}
procedure TChartListBox.WMNCHitTest(var Msg: TWMNCHitTest);
begin
  inherited;
  FHitTest:=SmallPointToPoint(Msg.Pos);
end;

procedure TChartListBox.WMSetCursor(var Msg: TWMSetCursor);
var I: Integer;
begin
  if csDesigning in ComponentState then Exit;

  if (Items.Count>0) and (SeriesAtMousePos(FHitTest)<>-1) and
     (Msg.HitTest=HTCLIENT) then

     for I:=0 to 2 do  { don't count last section }
     begin
       if PointInSection(FHitTest,I) then
       begin
         if ((I=0) and FEnableChangeType) or
            ((I=2) and FEnableChangeColor) then
         begin
           SetCursor(Screen.Cursors[crHandPoint]);
           Exit;
         end
         else break;
       end;
     end;
  inherited;
end;
{$ENDIF}

procedure TChartListBox.UpdateSeries;
begin
  FillSeries(SelectedSeries);
end;

Function TChartListBox.GetShowActive:Boolean;
begin
  result:=Sections[1].Visible;
end;

procedure TChartListBox.SetShowActive(Value:Boolean);
begin
  Sections[1].Visible:=Value; Repaint;
end;

Function TChartListBox.GetShowIcon:Boolean;
begin
  result:=Sections[0].Visible;
end;

procedure TChartListBox.SetShowIcon(Value:Boolean);
begin
  Sections[0].Visible:=Value; Repaint;
end;

Function TChartListBox.GetShowColor:Boolean;
begin
  result:=Sections[2].Visible;
end;

procedure TChartListBox.SetShowColor(Value:Boolean);
begin
  Sections[2].Visible:=Value; Repaint;
end;

Function TChartListBox.GetShowTitle:Boolean;
begin
  result:=Sections[3].Visible;
end;

procedure TChartListBox.SetShowTitle(Value:Boolean);
begin
  Sections[3].Visible:=Value; Repaint;
end;

procedure TChartListBox.SelectAll;
{$IFNDEF D6}
var t : Integer;
{$ENDIF}
begin
  {$IFDEF D6}
  inherited;
  {$ELSE}
  for t:=0 to Items.Count-1 do Selected[t]:=True;
  {$ENDIF}
  RefreshDesigner;
end;

Function TChartListBox.GetSeries(Index:Integer):TChartSeries;
begin
  if (Index<>-1) and (Index<Items.Count) then
     result:=TChartSeries(Items.Objects[Index])
  else
     result:=nil;
end;

procedure TChartListBox.TeeEvent(Event:TTeeEvent);
var tmp : Integer;
begin
  if not (csDestroying in ComponentState) then
  if Event is TTeeSeriesEvent then
  With TTeeSeriesEvent(Event) do
  Case Event of
    seChangeColor,
    seChangeActive: Repaint;
    seChangeTitle: begin
                tmp:=Items.IndexOfObject(Series);
                if tmp<>-1 then Items[tmp]:=SeriesTitleOrName(Series);
              end;
    seRemove: begin
                tmp:=Items.IndexOfObject(Series);
                if tmp<>-1 then
                begin
                  Items.Delete(tmp);
                  if Assigned(FOtherItems) then FOtherItems.Delete(tmp);
                end;
              end;
      seAdd,
      seSwap: UpdateSeries;
  end;
end;

end.

