Unit TreeView;

Interface

Uses WinProcs,WinTypes,Classes,Controls,Forms,Graphics,Menus,Messages,StdCtrls,
     TreeList,WCtrls;

Const biPlus       = -2;
      biMinus      = -1;
      biCollapsed  = 0;
      biExpanded   = 1;

Type TTreeEntry    = Class(TTreeList)
      Private
       FBitmap     : Integer;
       FCaption    : String;
      Protected 
       Function    GetCaption:String; virtual;
       Procedure   SetCaption(Const ACaption:String); virtual;
      Public
       Property    Bitmap:Integer read FBitmap write FBitmap;
       Property    Caption:String read GetCaption write SetCaption;
       Function    Find(Const AName:String):TTreeEntry;
{$ifdef SUPERFAST_INSERTION}
       Function    FindWhereInsertSorted(Const AName: String): Integer;
{$endif}
       Procedure   SortByCaption(SortSubEntries:Boolean);
     end;

     TCustomTreeListView = Class(TWCustomListbox)
      Private
       FBitmapOffset    : Integer;
       FBitmaps         : Pointer;
       FImages          : TWImageList;
       FItemWidth       : Integer;
       FOnDrawBitmap    : TDrawItemEvent;
       FOnDrawCaption   : TDrawItemEvent;
       FTree            : TTreeEntry;
       FUpdates         : Integer;
       Function    GetExpanded(AIndex:Integer):Boolean;
       Function    GetItemHeight:Integer;
       Procedure   SetBitmap(AIndex:Integer;ABitmap:TBitmap);
       Procedure   SetExpanded(AIndex:Integer;ASet:Boolean);
       Procedure   SetItemHeight(AHeight:Integer);
       Procedure   SetItemWidth(AWidth:Integer);
       Procedure   WMKeyDown(var Msg:TWMKeyDown); message wm_KeyDown;
       Procedure   WMLButtonDblClk(var Msg:TWMLButtonDblClk); message wm_LButtonDblClk;
      Protected
       Procedure   ChangeScale(M,D:Integer); override;
       Procedure   DoDrawBitmap(Index:Integer;ARect:TRect;State:TOwnerDrawState); virtual;
       Procedure   DoDrawCaption(Index:Integer;ARect:TRect;State:TOwnerDrawState); virtual;
       Procedure   DoUpdate; virtual;
       Procedure   DrawItem(Index:Integer;ItemRect:TRect;State:TOwnerDrawState); override;
       Function    GetImageIndex(ABitmap:Integer):Integer; virtual;
       Property    Images:TWImageList read FImages write FImages;
       Property    ItemWidth:Integer read FItemWidth write SetItemWidth;
       Procedure   MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
       Property    OnDrawBitmap:TDrawItemEvent read FOnDrawBitmap write FOnDrawBitmap;
       Property    OnDrawCaption:TDrawItemEvent read FOnDrawCaption write FOnDrawCaption;
       Property    Tree:TTreeEntry read FTree;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Procedure   AddBitmaps(ABitmaps:TBitmap;StartIndex:Integer);
       Procedure   BeginUpdate;
       Property    Bitmaps[AIndex:Integer]:TBitmap write SetBitmap;
       Procedure   DeleteItem(AIndex:Integer);
       Procedure   EndUpdate;
       Property    Expanded[AIndex:Integer]:Boolean read GetExpanded write SetExpanded;
       Property    ExpandedBitmapOffset:Integer read FBitmapOffset write FBitmapOffset default 1;
       Property    ItemHeight:Integer read GetItemHeight write SetItemHeight default 16;
       Procedure   UpdateView;
     end;

     TTreeListView = Class(TCustomTreeListView)
      Public
       Property    Tree;
      Published
       Property    Align;
       Property    BorderStyle;
       Property    Color;
       Property    Columns;
       Property    Ctl3D;
       Property    DragCursor;
       Property    DragMode;
       Property    Enabled;
       Property    ExpandedBitmapOffset;
       Property    ExtendedSelect;
       Property    Font;
       {$IFDEF WIN32}
       Property    ImeMode;
       Property    ImeName;
       {$ENDIF}
       Property    IntegralHeight;
       Property    ItemHeight;
       Property    MultiSelect;
       Property    ParentColor;
       Property    ParentCtl3D;
       Property    ParentFont;
       Property    ParentShowHint;
       Property    PopupMenu;
       Property    ShowHint;
       Property    Sorted;
       Property    TabOrder;
       Property    TabStop;
       Property    Visible;
       Property    OnClick;
       Property    OnDblClick;
       Property    OnDragDrop;
       Property    OnDragOver;
       Property    OnDrawBitmap;
       Property    OnDrawCaption;
       Property    OnEndDrag;
       Property    OnEnter;
       Property    OnExit;
       Property    OnKeyDown;
       Property    OnKeyPress;
       Property    OnKeyUp;
       Property    OnMeasureItem;
       Property    OnMouseDown;
       Property    OnMouseMove;
       Property    OnMouseUp;
       {$IFDEF Win32}
       Property    OnStartDrag;
       {$ENDIF}
       Property    ItemWidth;
       Property    Scrollbars;
     end;

Implementation

Uses NumTools,SysUtils;

{$IFDEF WIN32}
{$R *.R32}
{$ELSE}
{$R *.RES}
{$ENDIF}

var PatternBrush   : TBrush;

Type PBitmapList   = ^TBitmapList;
     TBitmapList   = Record
       BitmapIndex      : Integer;
       ImageListIndex   : Integer;
       Next             : PBitmapList;
     end;

Procedure CreatePatternBrush;
  var X            : Integer;
      Y            : Integer;
      Pattern      : TBitmap;
  begin
    Pattern:=TBitmap.Create;
    try
      Pattern.Width:=8;
      Pattern.Height:=8;
      with Pattern.Canvas do begin
        Brush.Style:=bsSolid;
        Brush.Color:=clWindow;
        FillRect(Rect(0,0,Pattern.Width,Pattern.Height));
        for Y:=0 to 7 do for X:=0 to 7 do
          if (Y mod 2)=(X mod 2) then
              Pixels[X,Y]:=clBtnShadow;
      end;
      PatternBrush:=TBrush.Create;
      PatternBrush.Bitmap:=Pattern;
    finally
{!?!?!      Pattern.Free;}
    end;    
  end;

Procedure DrawDottedLine
   (
   Canvas          : TCanvas;
   X1              : Integer;
   Y1              : Integer;
   X2              : Integer;
   Y2              : Integer
   );
  begin
    FrameRect(Canvas.Handle,Rect(X1,Y1,X2+1,Y2+1),PatternBrush.Handle);
  end;

{===============================================================================
| Class TCustomTreeListView
+==============================================================================}

Function TTreeEntry.Find
   (
   Const AName     : String
   )
   : TTreeEntry;
  var L            : Integer;
      H            : Integer;
      I            : Integer;
      C            : Integer;
  begin
    Result:=NIL;
    L:=0;
    H:=Count-1;
    while L<=H do begin
      I:=(L+H) Shr 1;
      C:=AnsiCompareText(TTreeEntry(Items[I]).Caption,AName);
      if C<0 then L:=I+1
      else begin
        H:=I-1;
        if C=0 then begin
          Result:=TTreeEntry(Items[I]);
          L:=I;
        end;
      end;
    end;
  end;

{$ifdef SUPERFAST_INSERTION}
Function TTreeEntry.FindWhereInsertSorted(Const AName: String): Integer;
var
  L : Integer;
  H : Integer;
  I : Integer;
  C : Integer;
begin
  L:=0;  { Initial search diapason low  bound}
  H:=Count-1; { ... and high one}
  while L<=H do begin
    I:=(L+H) Shr 1;  { Generate middle index  to compare }
    C:=AnsiCompareText(TTreeEntry(Items[I]).Caption,AName);
    if C<0 then L:=I+1 { Searched item in lower part of diapason }
    else begin  { Searched item in high part}
      H:=I-1;
      if C=0 then begin  { Found first coincidence }
        L:=I;
      end;
    end;
  end;
  if C >= 0 then  { if .GE. then insert before this element }
    Result := I - 1;
  else            { If .LT. insert before next element }
    Result := I + 1;
end;
{$endif}

Procedure TTreeEntry.SetCaption
   (
   Const ACaption  : String
   );
  begin
    FCaption:=ACaption;
  end;

Function TTreeEntry.GetCaption
   : String;
  begin
	  Result:=FCaption;
  end;

Function SortTreeEntries
   (
   Item1           : TTreeList;
   Item2           : TTreeList
   )
   : Integer; Far;
{$IFDEF SORT_BY_EXT_FIRST} {++Sygsky}
{ It adds some good time to a window showing delay, so use it on your own risk }
var
  Cap1, Cap2 : String;
  Ext1, Ext2 : String;
  function ExtractFileName1(Str:String):String;
  var
    P1 : Integer;
  begin
    Result := ExtractFileName(Str);
    P1 := Pos('.', Result);
    if P1 <> 0 then
      Result := Copy(Result, 1, P1 - 1);
  end;
  begin
    Cap1 := TTreeEntry(Item1).Caption;
    Cap2 := TTreeEntry(Item2).Caption;
    Ext1 := ExtractFileExt(Cap1);
    Ext2 := ExtractFileExt(Cap2);
    if Ext1[1] = '.' then
      Delete( Ext1,1,1);
    if Ext2[1] = '.' then
      Delete( Ext2,1,1);
    if Ext1 <> '' or Ext2 <> '' Then
    begin
      Result := AnsiCompareText(Ext1,Ext2);   { Compare pure extensions }
      if Result <> 0 then { Comparison is done }
        Exit;
    end;
    Result := AnsiCompareText(ExtractFileName1(Cap1),ExtractFileName1(Cap2));
{$ELSE}
  begin
    Result:=AnsiCompareText(TTreeEntry(Item1).Caption,TTreeEntry(Item2).Caption);
{$ENDIF}
  end;

Procedure TTreeEntry.SortByCaption
   (
   SortSubEntries  : Boolean
   );
  begin
    Sort(SortTreeEntries,SortSubEntries);
  end;

{===============================================================================
| Class TCustomTreeListView
+==============================================================================}

Constructor TCustomTreeListView.Create
   (
   AOwner          : TComponent
   );
  var ABitmap      : TBitmap;
  begin
    inherited Create(AOwner);
    FTree:=TTreeEntry.Create;
    Style:=lbOwnerDrawFixed;
    HasStrings:=FALSE;
    ItemHeight:=16;
    ItemWidth:=20;
    FImages:=TWImageList.CreateSize(16,16);
    ABitmap:=TBitmap.Create;
    ABitmap.Handle:=LoadBitmap(hInstance,'TREEVIEW');
    try
      AddBitmaps(ABitmap,-2);
    finally
      ABitmap.Free;
    end;
    FBitmapOffset:=1;
  end;

Destructor TCustomTreeListView.Destroy;
  var Bitmap       : PBitmapList;
  begin
    Bitmap:=FBitmaps;
    while Bitmap<>NIL do begin
      FBitmaps:=Bitmap^.Next;
      Dispose(Bitmap);
      Bitmap:=FBitmaps;
    end;
    FTree.Free;
    FImages.Free;
    inherited Destroy;
  end;

Procedure TCustomTreeListView.MouseDown
   (
   Button          : TMouseButton;
   Shift           : TShiftState;
   X               : Integer;
   Y               : Integer
   );
  var AItem        : TTreeList;
      AIndex       : Integer;
  begin
    inherited MouseDown(Button,Shift,X,Y);
    if Button=mbLeft then begin
      AIndex:=ItemAtPos(Point(X,Y),True);
      if AIndex>=0 then begin
        AItem:=FTree.ExpandedItems[AIndex];
        if (AItem<>NIL) and (X>=(AItem.Level-1)*FItemWidth)
            and (X<=AItem.Level*FItemWidth) then begin
          Expanded[AIndex]:=not Expanded[AIndex];
          DoInherited:=FALSE;
        end;  
      end;
    end;
  end;

Procedure TCustomTreeListView.ChangeScale
   (
   M               : Integer;
   D               : Integer
   );
  begin
    inherited ChangeScale(M,D);
    ItemWidth:=MulDiv(ItemWidth,M,D);
  end;

Procedure TCustomTreeListView.BeginUpdate;
  begin
    LockUpdates;
    Inc(FUpdates);
  end;

Procedure TCustomTreeListView.EndUpdate;
  begin
    if FUpdates>0 then begin
      Dec(FUpdates);
      if FUpdates=0 then begin
        DoUpdate;
        Count:=FTree.ExpandedCount;
        UnlockUpdates;
        Invalidate;
      end
      else UnlockUpdates;
    end
    else UnlockUpdates;
  end;

Procedure TCustomTreeListView.SetItemWidth
   (
   AWidth          : Integer
   );
  begin
    if AWidth<>FItemWidth then begin
      FItemWidth:=AWidth;
      Invalidate;
    end;
  end;

Procedure TCustomTreeListView.DoUpdate;
  begin
  end;  

Procedure TCustomTreeListView.SetBitmap
   (
   AIndex          : Integer;
   ABitmap         : TBitmap
   );
  begin
  end;
   
Procedure TCustomTreeListView.UpdateView;
  begin
    BeginUpdate;
    EndUpdate;
  end;

Procedure TCustomTreeListView.DrawItem
   (
   Index           : Integer;
   ItemRect        : TRect;
   State           : TOwnerDrawState
   );
  var TreeItem     : TTreeEntry;
      X1           : Integer;
      Y2           : Integer;
      ARect        : TRect;
      AItem        : TTreeList;
  begin
    TreeItem:=TTreeEntry(FTree.ExpandedItems[Index]);
    if TreeItem<>NIL then with TreeItem,Canvas,ARect do begin
      with ItemRect do ARect:=Rect(Left+Level*FItemWidth,Top,Left+(Level+1)*FItemWidth,Bottom);
      if oaDrawEntire in DrawAction then begin
        Pen.Style:=psSolid;
        Pen.Color:=clGray;
        X1:=Left-FItemWidth Div 2;
        Y2:=Top+ItemHeight Div 2;
        DrawDottedLine(Canvas,X1,Y2,Left-1,Y2);
        if Next<>NIL then DrawDottedLine(Canvas,X1,Top,X1,Bottom)
        else DrawDottedLine(Canvas,X1,Top,X1,Y2);
        AItem:=TreeItem.Parent;
        while AItem.Parent<>NIL do begin
          if AItem.Next<>NIL then begin
            DrawDottedLine(Canvas,ItemRect.Left+AItem.Level*FItemWidth-FItemWidth Div 2,ItemRect.Top,
            ItemRect.Left+AItem.Level*FItemWidth-FItemWidth Div 2,ItemRect.Bottom);
          end;
          AItem:=AItem.Parent;
        end;
        if Count<>0 then begin
          if not Expanded then FImages.Draw(Canvas,X1-7,Y2-7,GetImageIndex(biPlus))
          else FImages.Draw(Canvas,X1-7,Y2-7,GetImageIndex(biMinus));
        end;
        if Assigned(FOnDrawBitmap) then OnDrawBitmap(Self,Index,ARect,State)
        else DoDrawBitmap(Index,ARect,State);
      end;
      if odSelected in State then begin
        Brush.Color:=clHighlight;
        Font.Color:=clHighlightText;
      end
      else Font.Color:=clWindowText;
      with ARect do if GetImageIndex(Bitmap)>=0 then ARect:=Rect(Right,Top,ItemRect.Right,Bottom)
      else ARect:=Rect(Left,Top,ItemRect.Right,Bottom);
      if Assigned(FOnDrawCaption) then OnDrawCaption(Self,Index,ARect,State)
      else DoDrawCaption(Index,ARect,State);
    end;
  end;

Procedure TCustomTreeListView.DoDrawBitmap
   (
   Index           : Integer;
   ARect           : TRect;
   State           : TOwnerDrawState
   );
  var AItem        : TTreeEntry;
      AIndex       : Integer;
  begin
    AItem:=TTreeEntry(Tree.ExpandedItems[Index]);
    with AItem,ARect do begin
      AIndex:=GetImageIndex(Bitmap);
      if AIndex>=0 then begin
        if (Count>0) and Expanded then FImages.Draw(Canvas,Left+(FItemWidth-15) Div 2,
            Top+(ItemHeight-15) Div 2,AIndex+FBitmapOffset)
        else FImages.Draw(Canvas,Left+(FItemWidth-15) Div 2,
            Top+(ItemHeight-15) Div 2,AIndex);
      end;
    end;
  end;

Procedure TCustomTreeListView.DoDrawCaption
   (
   Index           : Integer;
   ARect           : TRect;
   State           : TOwnerDrawState
   );
  var AItem        : TTreeEntry;
  begin
    AItem:=TTreeEntry(Tree.ExpandedItems[Index]);
    with ARect,Canvas do begin
      ARect:=Rect(Left,Top,Left+4+TextWidth(AItem.Caption),Bottom);
      TextRect(ARect,Left+2,(Top+Bottom-TextHeight(AItem.Caption)) Div 2,
          AItem.Caption);
      if odFocused in State then DrawFocusRect(ARect);
    end;
  end;

Procedure TCustomTreeListView.AddBitmaps
   (
   ABitmaps        : TBitmap;
   StartIndex      : Integer
   );
  var BitmapList   : PBitmapList;
      ARect        : TRect;
      BRect        : TRect;
      ABitmap      : TBitmap;
  begin
    ARect:=Rect(0,0,16,16);  
    BRect:=ARect;
    ABitmap:=TBitmap.Create;
    try
      ABitmap.Width:=16;
      ABitmap.Height:=16;
      while BRect.Right<=ABitmaps.Width do begin
        New(BitmapList);
        with BitmapList^ do begin
          Next:=FBitmaps;
          BitmapIndex:=StartIndex;
          FBitmaps:=BitmapList;
          ABitmap.Canvas.CopyRect(ARect,ABitmaps.Canvas,BRect);
          ImageListIndex:=FImages.AddMasked(ABitmap,ABitmaps.TransparentColor);
          OffsetRect(BRect,16,0);
        end;
        Inc(StartIndex);
      end;
    finally
      ABitmap.Free;
    end;
  end;

Function TCustomTreeListView.GetImageIndex
   (
   ABitmap         : Integer
   )
   : Integer;
  var BitmapList   : PBitmapList;
  begin
    BitmapList:=FBitmaps;
    while (BitmapList<>NIL) and (BitmapList^.BitmapIndex<>ABitmap) do
        BitmapList:=BitmapList^.Next;
    if BitmapList=NIL then Result:=-1
    else Result:=BitmapList^.ImageListIndex;
  end;

Procedure TCustomTreeListView.WMKeyDown
   (
   var Msg         : TWMKeyDown
   );
  begin
    case Msg.CharCode of
      vk_Subtract : Expanded[ItemIndex]:=FALSE;
      vk_Add      : Expanded[ItemIndex]:=TRUE;
    end;
    inherited;
  end;

Procedure TCustomTreeListView.WMLButtonDblClk
   (
   var Msg         : TWMLButtonDblClk
   );
  var Index        : Integer;
      {$IFNDEF WIN32}
      AOnDblClick  : TNotifyEvent;
      {$ENDIF}
  begin
    {$IFDEF WIN32}
    if not Assigned(OnDblClick) then begin
    {$ELSE}
    AOnDblClick:=OnDblClick;
    if not Assigned(AOnDblClick) then begin
    {$ENDIF}
      Index:=ItemAtPos(Point(Msg.XPos,Msg.YPos),TRUE);
      if Index>=0 then Expanded[Index]:=not Expanded[Index]
      else inherited;
    end
    else inherited;
  end;

Function TCustomTreeListView.GetExpanded
   (
   AIndex          : Integer
   )
   : Boolean;
  begin
    Result:=FTree.ExpandedItems[AIndex].Expanded;
  end;

Procedure TCustomTreeListView.SetExpanded
   (
   AIndex          : Integer;
   ASet            : Boolean
   );
  var AItem        : TTreeList; 
  begin
    AItem:=FTree.ExpandedItems[AIndex];
    if (AItem.Parent<>NIL)
        and (AItem.Count>0)
        and (AItem.Expanded<>ASet) then begin
      LockUpdates;
      AItem.Expanded:=ASet;
      InvalidateItems(AIndex,FTree.ExpandedCount-1);
      Count:=FTree.ExpandedCount;
      UnlockUpdates;
    end;
  end;

Procedure TCustomTreeListView.DeleteItem
   (
   AIndex          : Integer
   );
  var AItem        : TTreeList;
      BIndex       : Integer;
  begin
    LockUpdates;
    AItem:=FTree.ExpandedItems[AIndex];
    BIndex:=AItem.Parent.IndexOf(AItem);
    if AIndex+AItem.ExpandedCount=Tree.ExpandedCount then begin
      if BIndex=0 then AIndex:=Tree.ExpandedIndexOf(AItem.Parent)
      else AIndex:=Tree.ExpandedIndexOf(AItem.Parent[BIndex-1]);
    end;  
    AItem.Parent.Delete(BIndex);
    InvalidateItems(AIndex,AIndex+AItem.ExpandedCount);
    Count:=FTree.ExpandedCount;
    UnlockUpdates;
  end;

Function TCustomTreeListView.GetItemHeight
   : Integer;
  begin
    Result:=inherited ItemHeight;
  end;

Procedure TCustomTreeListView.SetItemHeight
   (
   AHeight         : Integer
   );
  begin
    AHeight:=(AHeight Div 2)*2;
    if AHeight<>ItemHeight then inherited ItemHeight:=AHeight;
  end;        
   
{===============================================================================
| Initialisierungs- und Terminierungscode                                      
+==============================================================================}

{$IFNDEF WIN32}
Procedure ExitProc; Far; 
  begin
    PatternBrush.Free;
  end;
{$ENDIF}  

Initialization
  begin
    CreatePatternBrush;
    {$IFNDEF WIN32}
    AddExitProc(ExitProc);
    {$ENDIF}
  end;

{$IFDEF WIN32}
Finalization
  begin
    PatternBrush.Free;
  end;
{$ENDIF}

end.
