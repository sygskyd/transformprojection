{******************************************}
{   TDraw3D component                      }
{ Copyright (c) 1995-2000 by David Berneda }
{        All Rights Reserved               }
{******************************************}
{$I teedefs.inc}
unit TeeDraw3D;

interface

Uses Windows, Classes, TeeProcs;

type
  TDraw3DPaintEvent=procedure(Sender:TObject; Const ARect:TRect) of object;

  TDraw3D=class(TCustomTeePanelExtended)
  private
    FOnPaint : TDraw3DPaintEvent;
  protected
    Procedure InternalDraw(Const UserRectangle:TRect); override;
  published
    { TCustomTeePanelExtended properties }
    property BackImage;
    property BackImageMode;
    property Gradient;
    property OnAfterDraw;

    { TCustomTeePanel properties }
    property BufferedDisplay;
    property MarginLeft;
    property MarginTop;
    property MarginRight;
    property MarginBottom;
    property Monochrome;
    property PrintProportional;
    property PrintResolution;
    property View3D;
    property View3DOptions;
    property OnPaint:TDraw3DPaintEvent read FOnPaint write FOnPaint;

    { TPanel properties }
    property Align;
    property BevelInner;
    property BevelOuter;
    property BevelWidth;
    property BorderWidth;
    property BorderStyle;
    property Color;
    {$IFNDEF CLX}
    property DragCursor;
    {$ENDIF}
    property DragMode;
    property Enabled;
    property ParentColor;
    property ParentShowHint;
    {$IFNDEF TEEOCX}
    property PopupMenu;
    {$ENDIF}
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    {$IFDEF D4}
    property Anchors;
    {$IFNDEF CLX}
    property AutoSize;
    {$ENDIF}
    {$IFNDEF TEEOCX}
    property Constraints;
    {$ENDIF}
    {$IFNDEF CLX}
    property DragKind;
    {$ENDIF}
    property Locked;
    {$ENDIF}

    { TPanel events }
    property OnClick;
    {$IFDEF D5}
    property OnContextPopup;
    {$ENDIF}
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    {$IFDEF D4}
    property OnMouseWheel;
    property OnMouseWheelDown;
    property OnMouseWheelUp;
    {$ENDIF}
    property OnStartDrag;
    {$IFDEF D4}
    {$IFNDEF CLX}
    property OnCanResize;
    {$ENDIF}
    {$IFNDEF TEEOCX}
    property OnConstrainedResize;
    {$IFNDEF CLX}
    property OnDockDrop;
    property OnDockOver;
    property OnEndDock;
    property OnGetSiteInfo;
    property OnStartDock;
    property OnUnDock;
    {$ENDIF}
    {$ENDIF}
    {$ENDIF}
  end;

implementation

{ TDraw3D }
Procedure TDraw3D.InternalDraw(Const UserRectangle:TRect);
begin
  PanelPaint(UserRectangle);
  RecalcWidthHeight;
  Width3D:=100;
  InternalCanvas.Projection(Width3D,ChartBounds,ChartRect);
  Canvas.ResetState;
  if Assigned(FOnPaint) then FOnPaint(Self,UserRectangle);
  if Zoom.Active then DrawZoomRectangle;
  Canvas.ResetState;
  if Assigned(FOnAfterDraw) then FOnAfterDraw(Self);
end;

end.
