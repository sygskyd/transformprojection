{**********************************************}
{  TeeChart Pro                                }
{                                              }
{  TCustom3DSeries                             }
{     TSurfaceSeries                           }
{     TContourSeries                           }
{     TWaterFallSeries                         }
{     TColorGridSeries                         }
{                                              }
{  Copyright (c) 1995-2000 by David Berneda    }
{**********************************************}
{$I teedefs.inc}
unit TeeSurfa;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     {$IFNDEF CLX}
     Windows, Messages,
     {$ENDIF}
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, Types,
     {$ELSE}
     Graphics,
     {$ENDIF}
     TeEngine, TeCanvas, Chart;

Const MaxAllowedCells=2000; { max 2000 x 2000 cells }

type
  TChartSurfaceGetColor=Procedure( Sender:TChartSeries;
                                   ValueIndex:Integer;
                                   Var Color:TColor) of object;

  TCustom3DSeries=class(TChartSeries)
  private
    FTimesZOrder : Integer;
    FZValues     : TChartValueList;
    Function GetZValue(Index:Integer):Double;
    Procedure SetTimesZOrder(Const Value:Integer);
    Procedure SetZValue(Index:Integer; Const Value:Double);
    Procedure SetZValues(Const Value:TChartValueList);
  protected
    Procedure CalcZOrder; override;
    Procedure DrawMark( ValueIndex:Integer; Const St:String;
                        APosition:TSeriesMarkPosition); override;
    Procedure PrepareLegendCanvas( ValueIndex:Integer; Var BackColor:TColor;
                                   Var BrushStyle:TBrushStyle); override;
  public
    Constructor Create(AOwner: TComponent); override;
    Procedure Assign(Source:TPersistent); override;

    Function AddXYZ( Const AX,AY,AZ:Double;
                     Const AXLabel:String{$IFDEF D4}=''{$ENDIF};
                     AColor:TColor{$IFDEF D4}=clTeeColor{$ENDIF}):Integer; virtual;
    Function CalcZPos(ValueIndex:Integer):Integer;
    Function IsValidSourceOf(Value:TChartSeries):Boolean; override;
    Function MaxZValue:Double; override;
    Function MinZValue:Double; override;
    property ZValue[Index:Integer]:Double read GetZValue write SetZValue;

    { to be published }
    property TimesZOrder:Integer read FTimesZOrder write SetTimesZOrder default 3;
    property ZValues:TChartValueList read FZValues write SetZValues;
  end;

  {$IFNDEF D4}
  PGridPalette=^TGridPalette;
  {$ENDIF}
  TGridPalette=packed record
    UpToValue : Double;
    Color     : TColor;
  end;

  {$IFDEF D4}
  TCustom3DPalette=Array of TGridPalette;
  {$ELSE}
  TCustom3DPalette=class(TList)
  private
    Function Get(Index:Integer):PGridPalette;
  public
    property Items[Index:Integer]:PGridPalette read Get; default;
  end;
  {$ENDIF}

  TTeePaletteStyle=(psPale,psStrong);

  TCustom3DPaletteSeries=class(TCustom3DSeries)
  private
    FEndColor     : TColor;
    FMidColor     : TColor;
    FPalette      : TCustom3DPalette;
    FPaletteSteps : Integer;
    FPaletteStyle : TTeePaletteStyle;
    FStartColor   : TColor;
    FUseColorRange: Boolean;
    FUsePalette   : Boolean;

    FOnGetColor   : TChartSurfaceGetColor;

    { internal }
    IRangeRed    : Integer;
    IEndRed      : Integer;
    IMidRed      : Integer;
    IRangeMidRed : Integer;
    IRangeGreen  : Integer;
    IEndGreen    : Integer;
    IMidGreen    : Integer;
    IRangeMidGreen: Integer;
    IRangeBlue   : Integer;
    IEndBlue     : Integer;
    IMidBlue     : Integer;
    IRangeMidBlue: Integer;
    Procedure CalcColorRange;
    Function RangePercent(Const Percent:Double):TColor;
    Procedure SetEndColor(Const Value:TColor);
    Procedure SetMidColor(Const Value:TColor);
    Procedure SetPaletteSteps(Const Value:Integer);
    procedure SetPaletteStyle(const Value: TTeePaletteStyle);
    Procedure SetStartColor(Const Value:TColor);
    Procedure SetUseColorRange(Const Value:Boolean);
    Procedure SetUsePalette(Const Value:Boolean);
  protected
    PaletteRange : Double;
    class Procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); override;
    Procedure DoBeforeDrawChart; override;
    Procedure GalleryChanged3D(Is3D:Boolean); override;
    class Function GetEditorClass:String; override;
    Function GetValueColor(ValueIndex:Integer):TColor; override;
    Function GetValueColorValue(Const AValue:Double):TColor;
    Procedure PrepareForGallery(IsEnabled:Boolean); override;
    class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); override;
  public
    RedFactor    : Double;
    GreenFactor  : Double;
    BlueFactor   : Double;
    Constructor Create(AOwner: TComponent); override;
    Destructor Destroy; override;
    Function AddPalette(Const AValue:Double; AColor:TColor):Integer;
    Procedure Assign(Source:TPersistent); override;
    Procedure Clear; override;
    Procedure ClearPalette;
    Function CountLegendItems:Integer; override;
    Procedure CreateDefaultPalette(NumSteps:Integer);
    Procedure CreateRangePalette;
    Function GetSurfacePaletteColor(Const Y:Double):TColor;
    Function LegendItemColor(LegendIndex:Integer):TColor; override;
    Function LegendString( LegendIndex:Integer;
                           LegendTextStyle:TLegendTextStyle ):String; override;

    property EndColor:TColor read FEndColor write SetEndColor default clWhite;
    property MidColor:TColor read FMidColor write SetMidColor default clNone;
    property Palette:TCustom3DPalette read FPalette;
    property PaletteSteps:Integer read FPaletteSteps write SetPaletteSteps default 32;
    property PaletteStyle:TTeePaletteStyle read FPaletteStyle write SetPaletteStyle default psPale;
    property StartColor:TColor read FStartColor write SetStartColor default clNavy;
    property UseColorRange:Boolean read FUseColorRange write SetUseColorRange default True;
    property UsePalette:Boolean read FUsePalette write SetUsePalette default False;
    { events }
    property OnGetColor:TChartSurfaceGetColor read FOnGetColor write FOnGetColor;
  end;

  { Grid 3D series }
  TTeeCellsRow=Array[1..MaxAllowedCells] of Integer;
  PTeeCellsRow=^TTeeCellsRow;

  TChartSurfaceGetY=Function(Sender:TChartSeries; X,Z:Integer):Double of object;

  TCustom3DGridSeries=class(TCustom3DPaletteSeries)
  private
    FGridIndex     : Array[1..MaxAllowedCells] of PTeeCellsRow;
    FIrregularGrid : Boolean;
    FNumXValues    : Integer;
    FNumZValues    : Integer;
    FOnGetYValue   : TChartSurfaceGetY;

    { internal }
    IInGallery     : Boolean;
    ValueIndex0    : Integer;
    ValueIndex1    : Integer;
    ValueIndex2    : Integer;
    ValueIndex3    : Integer;
    INextXCell     : Integer;
    INextZCell     : Integer;
    Procedure ClearGridIndex;
    Function ExistFourGridIndex(X,Z:Integer):Boolean;
    Function GetGridIndex(X,Z:Integer):Integer;
    Procedure InternalSetGridIndex(X,Z,Value:Integer);
    Procedure SetGridIndex(X,Z,Value:Integer);
    Procedure SetIrregularGrid(Const Value:Boolean);
    Procedure SetNumXValues(Value:Integer);
    Procedure SetNumZValues(Value:Integer);
  protected
    Procedure AddSampleValues(NumValues:Integer); override;
    Procedure AddValues(Source:TChartSeries); override;
    Function CanCreateValues:Boolean;
    Procedure DoBeforeDrawChart; override;
  public
    Constructor Create(AOwner: TComponent); override;
    Destructor Destroy; override;

    Procedure Assign(Source:TPersistent); override;
    Procedure Clear; override;
    Procedure CreateValues(NumX,NumZ:Integer); virtual;
    Procedure FillGridIndex;
    Function GetXZValue(X,Z:Integer):Double; virtual;
    Function IsValidSeriesSource(Value:TChartSeries):Boolean; override;
    Function NumSampleValues:Integer; override;
    Procedure ReCreateValues;

    property GridIndex[X,Z:Integer]:Integer read GetGridIndex write SetGridIndex;
    property IrregularGrid:Boolean read FIrregularGrid write SetIrregularGrid default False;

    property NumXValues:Integer read FNumXValues write SetNumXValues default 10;
    property NumZValues:Integer read FNumZValues write SetNumZValues default 10;
    { events }
    property OnGetYValue:TChartSurfaceGetY read FOnGetYValue write FOnGetYValue;
  end;

  TSurfaceSeries=class(TCustom3DGridSeries)
  private
    { Private declarations }
    FDotFrame       : Boolean;
    FSideBrush      : TChartBrush;
    FSmoothPalette  : Boolean;
    FWaterFall      : Boolean;
    FWaterLines     : TChartPen;
    FWireFrame      : Boolean;

    { internal }
    FSameBrush      : Boolean;
    Function FourGridIndex(x,z:Integer):Boolean;
    Procedure SetDotFrame(Value:Boolean);
    Procedure SetSideBrush(Value:TChartBrush);
    Procedure SetSmoothPalette(Value:Boolean);
    Procedure SetWaterFall(Value:Boolean);
    Procedure SetWireFrame(Value:Boolean);
    procedure SetWaterLines(const Value: TChartPen);
  protected
    { Protected declarations }
    Points : TFourPoints;
    Function CalcPointPos(Index:Integer):TPoint;
    class Procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); override;
    Procedure DrawAllValues; override;
    Procedure DrawCell(x,z:Integer); virtual;
    Function FastCalcPoints( x,z:Integer;
                             Var P0,P1:TPoint3D;
                             Var Color0,Color1:TColor):Boolean;
    class Function GetEditorClass:String; override;
    Procedure PrepareForGallery(IsEnabled:Boolean); override;
    class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); override;
  public
    { Public declarations }
    Constructor Create(AOwner: TComponent); override;
    Destructor Destroy; override;

    Procedure Assign(Source:TPersistent); override;
    Function Clicked(x,y:Integer):Integer; override;
    property WaterFall:Boolean read FWaterFall write SetWaterFall default False;
    property WaterLines:TChartPen read FWaterLines write SetWaterLines;
  published
    { Published declarations }
    property Active;
    property ColorSource;
    property Cursor;
    property HorizAxis;
    property Marks;
    property ParentChart;
    property DataSource;
    property PercentFormat;
    property SeriesColor;
    property ShowInLegend;
    property Title;
    property ValueFormat;
    property VertAxis;
    property XLabelsSource;

    { events }
    property AfterDrawValues;
    property BeforeDrawValues;
    property OnAfterAdd;
    property OnBeforeAdd;
    property OnClearValues;
    property OnClick;
    property OnDblClick;
    property OnGetMarkText;

    property Brush;
    property DotFrame:Boolean read FDotFrame write SetDotFrame default False;
    property EndColor;
    property IrregularGrid;
    property MidColor;
    property NumXValues;
    property NumZValues;
    property Pen;
    property PaletteSteps;
    property PaletteStyle;
    property SideBrush:TChartBrush read FSideBrush write SetSideBrush;
    property SmoothPalette:Boolean read FSmoothPalette write SetSmoothPalette default False;
    property StartColor;
    property UseColorRange;
    property UsePalette;
    property WireFrame:Boolean read FWireFrame write SetWireFrame default False;
    property TimesZOrder;
    property XValues;
    property YValues;
    property ZValues;
    { events }
    property OnGetYValue;
    property OnGetColor;
  end;

  TContourSeries=class;

  TOnGetLevelEvent=procedure( Sender:TContourSeries; LevelIndex:Integer;
                              Var Value:Double; Var Color:TColor) of object;

  TContourLevel=class(TCollectionItem)
  private
    FColor : TColor;
    FUpTo  : Double;

    ISeries : TContourSeries;
    Procedure CheckAuto;
    procedure SetColor(const Value: TColor);
    procedure SetUpTo(const Value: Double);
  public
    Constructor Create(Collection:TCollection); override;
    Destructor Destroy; override;
  published
    property Color:TColor read FColor write SetColor;
    property UpToValue:Double read FUpTo write SetUpTo;
  end;

  TContourLevels=class(TOwnedCollection)
  private
    Function Get(Index:Integer):TContourLevel;
    Procedure Put(Index:Integer; Const Value:TContourLevel);
  public
    property Items[Index:Integer]:TContourLevel read Get write Put; default;
  end;

  TContourSeries=class(TCustom3DGridSeries)
  private
    FAutomaticLevels: Boolean;
    FLevels         : TContourLevels;
    FNumLevels      : Integer;
    FYPosition      : Double;
    FYPositionLevel : Boolean;
    FOnGetLevel     : TOnGetLevelEvent;

    IModifyingLevels : Boolean;
    function GetNumLevels: Integer;
    function IsLevelsStored: Boolean;
    procedure SetAutomaticLevels(const Value: Boolean);
    procedure SetLevels(const Value: TContourLevels);
    Procedure SetNumLevels(Value:Integer);
    Procedure SetYPosition(Const Value:Double);
    Procedure SetYPositionLevel(Value:Boolean);
  protected
    class Procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); override;
    procedure AddSampleValues(NumValues:Integer); override;
    Procedure DoBeforeDrawChart; override;
    procedure DrawAllValues; override;
    class Function GetEditorClass:String; override;
    Procedure PrepareForGallery(IsEnabled:Boolean); override;
    class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); override;
  public
    Constructor Create(AOwner: TComponent); override;
    Destructor Destroy; override;

    Procedure Assign(Source:TPersistent); override;
    Function CountLegendItems:Integer; override;
    Procedure CreateAutoLevels; { calc Level values and colors }
    Function LegendItemColor(LegendIndex:Integer):TColor; override;
    Function LegendString( LegendIndex:Integer;
                           LegendTextStyle:TLegendTextStyle ):String; override;
    Function MaxYValue:Double; override;
    Function MinYValue:Double; override;
  published
    property Active;
    property AutomaticLevels:Boolean read FAutomaticLevels write SetAutomaticLevels default True;
    property ColorEachPoint default True;
    property ColorSource;
    property Cursor;
    property HorizAxis;
    property Marks;
    property ParentChart;
    property DataSource;
    property PercentFormat;
    property SeriesColor;
    property ShowInLegend;
    property Title;
    property ValueFormat;
    property VertAxis;
    property XLabelsSource;

    { events }
    property AfterDrawValues;
    property BeforeDrawValues;
    property OnAfterAdd;
    property OnBeforeAdd;
    property OnClearValues;
    property OnClick;
    property OnDblClick;
    property OnGetMarkText;

    property EndColor;
    property Levels:TContourLevels read FLevels write SetLevels stored IsLevelsStored;
    property MidColor;
    property NumLevels:Integer read GetNumLevels write SetNumLevels default 10;
    property NumXValues;
    property NumZValues;
    property PaletteSteps;
    property PaletteStyle;
    property Pen;
    property StartColor;
    property TimesZOrder;
    property UseColorRange;
    property UsePalette;
    property XValues;
    property YPosition:Double read FYPosition write SetYPosition;
    property YPositionLevel:Boolean read FYPositionLevel write SetYPositionLevel default False;
    property YValues;
    property ZValues;
  { events }
    property OnGetYValue;
    property OnGetLevel:TOnGetLevelEvent read FOnGetLevel write FOnGetLevel;
  end;

  TWaterFallSeries=class(TSurfaceSeries)
  protected
    class Procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); override;
    Procedure GalleryChanged3D(Is3D:Boolean); override;
    class Function GetEditorClass:String; override;
    class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); override;
  public
    Constructor Create(AOwner: TComponent); override;
  published
    property WaterFall default True;
    property WaterLines;
  end;

  TColorGridSeries=class(TCustom3DGridSeries)
  private
    FBitmap : TBitmap;
  protected
    class Procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); override;
    procedure DrawAllValues; override;
    Procedure GalleryChanged3D(Is3D:Boolean); override;
    class Function GetEditorClass:String; override;
    Procedure PrepareForGallery(IsEnabled:Boolean); override;
    class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); override;
  public
    Constructor Create(AOwner:TComponent); override;
    Destructor Destroy; override;
    Function MaxXValue:Double; override;
    Function MaxYValue:Double; override;
    Function MinYValue:Double; override;

    property Bitmap:TBitmap read FBitmap;
  published
    { Published declarations }
    property Active;
    property ColorSource;
    property Cursor;
    property HorizAxis;
    property Marks;
    property ParentChart;
    property DataSource;
    property PercentFormat;
    property SeriesColor;
    property ShowInLegend;
    property Title;
    property ValueFormat;
    property VertAxis;
    property XLabelsSource;

    { events }
    property AfterDrawValues;
    property BeforeDrawValues;
    property OnAfterAdd;
    property OnBeforeAdd;
    property OnClearValues;
    property OnClick;
    property OnDblClick;
    property OnGetMarkText;

    property Brush;
    property EndColor;
    property IrregularGrid;
    property MidColor;
    property NumXValues;
    property NumZValues;
    property Pen;
    property PaletteSteps;
    property PaletteStyle;
    property StartColor;
    property UseColorRange;
    property UsePalette;
    property TimesZOrder;
    property XValues;
    property YValues;
    property ZValues;
    { events }
    property OnGetYValue;
    property OnGetColor;
  end;

implementation

Uses TeeProcs, TeeProCo, TeeConst;

{ TCustom3DSeries }
Constructor TCustom3DSeries.Create(AOwner: TComponent);
Begin
  inherited;
  HasZValues:=True;
  CalcVisiblePoints:=False;
  FZValues:=TChartValueList.Create(Self,'Z'); { <-- dont translate ! }
  XValues.Order:=loNone;
  FTimesZOrder:=3;
end;

Procedure TCustom3DSeries.SetZValues(Const Value:TChartValueList);
Begin
  SetChartValueList(FZValues,Value); { standard method }
End;

Procedure TCustom3DSeries.CalcZOrder;
Begin
  inherited;
  ParentChart.MaxZOrder:=FTimesZOrder;
end;

Procedure TCustom3DSeries.DrawMark( ValueIndex:Integer; Const St:String;
                                    APosition:TSeriesMarkPosition);
begin
  Marks.ZPosition:=CalcZPos(ValueIndex);
  inherited;
end;

Function TCustom3DSeries.AddXYZ( Const AX,AY,AZ:Double;
                                 Const AXLabel:String; AColor:TColor):Integer;
Begin
  ZValues.TempValue:=AZ;
  result:=AddXY(AX,AY,AXLabel,AColor);
end;

Function TCustom3DSeries.IsValidSourceOf(Value:TChartSeries):Boolean;
begin
  result:=Value is TCustom3DSeries;
end;

Procedure TCustom3DSeries.SetTimesZOrder(Const Value:Integer);
Begin
  SetIntegerProperty(FTimesZOrder,Value);
End;

Function TCustom3DSeries.MaxZValue:Double;
begin
  result:=FZValues.MaxValue;
end;

Function TCustom3DSeries.MinZValue:Double;
begin
  result:=FZValues.MinValue;
end;

Procedure TCustom3DSeries.Assign(Source:TPersistent);
begin
  if Source is TCustom3DSeries then
  With TCustom3DSeries(Source) do
  begin
    Self.FZValues.Assign(FZValues);
    Self.FTimesZOrder  :=FTimesZOrder;
  end;
  inherited;
end;

Procedure TCustom3DSeries.SetZValue(Index:Integer; Const Value:Double);
Begin
  ZValues.Value[Index]:=Value;
End;

Function TCustom3DSeries.GetZValue(Index:Integer):Double;
Begin
  result:=ZValues.Value[Index];
End;

function TCustom3DSeries.CalcZPos(ValueIndex: Integer): Integer;
begin
  result:=ParentChart.DepthAxis.CalcYPosValue(ZValues.Value[ValueIndex]);
end;

procedure TCustom3DSeries.PrepareLegendCanvas(ValueIndex: Integer;
  var BackColor: TColor; var BrushStyle: TBrushStyle);
begin
  inherited;
  if TCustomChart(ParentChart).Legend.Symbol.Continuous then
     ParentChart.Canvas.Pen.Style:=psClear;
end;

{ TCustom3DPaletteSeries }
Constructor TCustom3DPaletteSeries.Create(AOwner: TComponent);
Begin
  inherited;
  FUseColorRange:=True;
  {$IFNDEF D4}
  FPalette:=TCustom3DPalette.Create;
  {$ENDIF}
  FPaletteSteps:=32;
  FStartColor:=clNavy;
  FEndColor:=clWhite;
  FMidColor:=clNone;
  { Palette Modifiers }
  RedFactor:=2.0;
  GreenFactor:=1;
  BlueFactor:=1;
  CalcColorRange;
End;

Destructor TCustom3DPaletteSeries.Destroy;
begin
  ClearPalette;
  {$IFNDEF D4}
  FPalette.Free;
  FPalette:=nil;
  {$ENDIF}
  inherited;
end;

Procedure TCustom3DPaletteSeries.CalcColorRange;
Begin
  IEndRed    :=GetRValue(EndColor);
  IEndGreen  :=GetGValue(EndColor);
  IEndBlue   :=GetBValue(EndColor);
  if MidColor<>clNone then
  begin
    IMidRed    :=GetRValue(MidColor);
    IMidGreen  :=GetGValue(MidColor);
    IMidBlue   :=GetBValue(MidColor);
    IRangeMidRed  :=Integer(GetRValue(StartColor))-IMidRed;
    IRangeMidGreen:=Integer(GetGValue(StartColor))-IMidGreen;
    IRangeMidBlue :=Integer(GetBValue(StartColor))-IMidBlue;
    IRangeRed  :=IMidRed-IEndRed;
    IRangeGreen:=IMidGreen-IEndGreen;
    IRangeBlue :=IMidBlue-IEndBlue;
  end
  else
  begin
    IRangeRed  :=Integer(GetRValue(StartColor))-IEndRed;
    IRangeGreen:=Integer(GetGValue(StartColor))-IEndGreen;
    IRangeBlue :=Integer(GetBValue(StartColor))-IEndBlue;
  end;
end;

Procedure TCustom3DPaletteSeries.SetStartColor(Const Value:TColor);
Begin
  SetColorProperty(FStartColor,Value);
  CalcColorRange;
End;

Procedure TCustom3DPaletteSeries.SetMidColor(Const Value:TColor);
Begin
  SetColorProperty(FMidColor,Value);
  CalcColorRange;
End;

Procedure TCustom3DPaletteSeries.SetEndColor(Const Value:TColor);
Begin
  SetColorProperty(FEndColor,Value);
  CalcColorRange;
End;

Function TCustom3DPaletteSeries.AddPalette(Const AValue:Double; AColor:TColor):Integer;
var t   : Integer;
    {$IFDEF D4}
    tt  : Integer;
    {$ELSE}
    tmp : PGridPalette;
    {$ENDIF}
Begin
  {$IFNDEF D4}
  New(tmp);
  tmp^.UpToValue:=AValue;
  tmp^.Color:=AColor;
  {$ENDIF}
  for t:=0 to {$IFDEF D4}Length(FPalette){$ELSE}FPalette.Count{$ENDIF}-1 do
  begin
    if AValue<FPalette[t].UpToValue then
    begin
      {$IFDEF D4}
      SetLength(FPalette,Length(FPalette)+1);
      for tt:=Length(FPalette)-1 downto t+1 do
          FPalette[tt]:=FPalette[tt-1];
      With FPalette[t] do
      begin
        UpToValue:=AValue;
        Color:=AColor;
      end;
      {$ELSE}
      FPalette.Insert(t,tmp);
      {$ENDIF}
      result:=t;
      exit;
    end;
  end;
  {$IFDEF D4}
  result:=Length(FPalette);
  SetLength(FPalette,result+1);
  With FPalette[result] do
  begin
    UpToValue:=AValue;
    Color:=AColor;
  end;
  {$ELSE}
  result:=FPalette.Add(tmp);
  {$ENDIF}
End;

Procedure TCustom3DPaletteSeries.ClearPalette;
{$IFNDEF D4}
var t : Integer;
{$ENDIF}
Begin
  {$IFDEF D4}
  FPalette:=nil;
  {$ELSE}
  if Assigned(FPalette) then
  begin
    for t:=0 to FPalette.Count-1 do Dispose(PGridPalette(FPalette[t]));
    FPalette.Clear;
  end;
  {$ENDIF}
end;

Procedure TCustom3DPaletteSeries.CreateDefaultPalette(NumSteps:Integer);
Const Delta=127.0;
var t          : Integer;
    tmp        : Double;
    Scale      : Double;
    ScaleValue : Double;
Begin
  ClearPalette;
  {$IFNDEF D4}
  FPalette.Capacity:=NumSteps+1;
  {$ENDIF}
  Scale:=Pi/NumSteps;
  if PaletteStyle=psStrong then Scale:=2.0*Scale;
  if PaletteRange=0 then
     ScaleValue:=MandatoryValueList.Range/NumSteps
  else
     ScaleValue:=PaletteRange/NumSteps;
  for t:=1 to NumSteps do
  begin
    tmp:=Scale*t;
    AddPalette(MandatoryValueList.MinValue+ScaleValue*t,
                        RGB( Trunc(Delta * (Sin(tmp/RedFactor)+1)) ,
                             Trunc(Delta * (Sin(tmp/GreenFactor)+1)),
                             Trunc(Delta * (Cos(tmp/BlueFactor)+1))));
  end;
end;

Procedure TCustom3DPaletteSeries.SetUseColorRange(Const Value:Boolean);
Begin
  SetBooleanProperty(FUseColorRange,Value);
  if Value then ColorEachPoint:=False;
End;

Procedure TCustom3DPaletteSeries.SetUsePalette(Const Value:Boolean);
Begin
  SetBooleanProperty(FUsePalette,Value);
  if Value then
  begin
    ColorEachPoint:=False;
    if (Count>0) and (({$IFDEF D4}Length(FPalette){$ELSE}FPalette.Count{$ENDIF}=0)) then
       CreateDefaultPalette(FPaletteSteps);
  end;
End;

Function TCustom3DPaletteSeries.GetSurfacePaletteColor(Const Y:Double):TColor;
Var t        : Integer;
    tmpCount : Integer;
Begin
  {$IFDEF D4}
  tmpCount:=Length(FPalette)-1;
  {$ELSE}
  tmpCount:=FPalette.Count-1;
  {$ENDIF}
  for t:=0 to tmpCount do
  With FPalette[t]{$IFNDEF D4}^{$ENDIF} do
  if UpToValue>Y then
  begin
    result:=Color;
    exit;
  end;
  result:=FPalette[tmpCount].Color;
End;

Function TCustom3DPaletteSeries.RangePercent(Const Percent:Double):TColor;
begin
  if MidColor=clNone then
     result:=RGB( IEndRed  +Round(Percent*IRangeRed),
                  IEndGreen+Round(Percent*IRangeGreen),
                  IEndBlue +Round(Percent*IRangeBlue))
  else
  if Percent<0.5 then
     result:=RGB( IEndRed  +Round((2.0*Percent)*IRangeRed),
                  IEndGreen+Round((2.0*Percent)*IRangeGreen),
                  IEndBlue +Round((2.0*Percent)*IRangeBlue))
  else
     result:=RGB( IMidRed  +Round(2.0*(Percent-0.5)*IRangeMidRed),
                  IMidGreen+Round(2.0*(Percent-0.5)*IRangeMidGreen),
                  IMidBlue +Round(2.0*(Percent-0.5)*IRangeMidBlue))
end;

Function TCustom3DPaletteSeries.GetValueColorValue(Const AValue:Double):TColor;
begin
  if UseColorRange then
  With MandatoryValueList do
  if Range=0 then result:=EndColor
             else result:=RangePercent((AValue-MinValue)/Range)
  else
  if UsePalette and ({$IFDEF D4}Length(FPalette){$ELSE}FPalette.Count{$ENDIF}>0) then
     result:=GetSurfacePaletteColor(AValue)
  else
     result:=SeriesColor;
end;

Function TCustom3DPaletteSeries.GetValueColor(ValueIndex:Integer):TColor;
Begin
  result:=inherited GetValueColor(ValueIndex);
  if (result<>clNone) and (not ColorEachPoint) then
     if FUseColorRange or FUsePalette then
        result:=GetValueColorValue(MandatoryValueList.Value[ValueIndex])
     else
     if result=clTeeColor then result:=SeriesColor;
  if Assigned(FOnGetColor) then FOnGetColor(Self,ValueIndex,result);
End;

Function TCustom3DPaletteSeries.CountLegendItems:Integer;
begin
  if UseColorRange or UsePalette then
     result:={$IFDEF D4}Length(FPalette){$ELSE}FPalette.Count{$ENDIF}
  else
     result:=inherited CountLegendItems;
end;

Function TCustom3DPaletteSeries.LegendString( LegendIndex:Integer;
                                              LegendTextStyle:TLegendTextStyle
                                              ):String;
var tmp : Double;
begin
  if UseColorRange or UsePalette then
  begin
    if CountLegendItems>LegendIndex then
    begin
      tmp:=FPalette[{$IFDEF D4}Length(FPalette){$ELSE}FPalette.Count{$ENDIF}-LegendIndex-1].UpToValue;
      result:=FormatFloat(ValueFormat,tmp);
    end
    else
       result:='';
  end
  else result:=inherited LegendString(LegendIndex,LegendTextStyle);
end;

Function TCustom3DPaletteSeries.LegendItemColor(LegendIndex:Integer):TColor;
begin
  if UseColorRange or UsePalette then
     result:=GetValueColorValue(FPalette[{$IFDEF D4}Length(FPalette){$ELSE}FPalette.Count{$ENDIF}-LegendIndex-1].UpToValue)
  else
     result:=inherited LegendItemColor(LegendIndex);
end;

Procedure TCustom3DPaletteSeries.SetPaletteSteps(Const Value:Integer);
Begin
  FPaletteSteps:=Value;
  CreateDefaultPalette(FPaletteSteps);
End;

Procedure TCustom3DPaletteSeries.PrepareForGallery(IsEnabled:Boolean);
begin
  inherited;
  UseColorRange:=False;
  if IsEnabled then Pen.Color:=clBlack
               else Pen.Color:=clGray;
  UsePalette:=IsEnabled;
end;

Procedure TCustom3DPaletteSeries.Assign(Source:TPersistent);
{$IFNDEF D4}
var t : Integer;
{$ENDIF}
begin
  if Source is TCustom3DPaletteSeries then
  With TCustom3DPaletteSeries(Source) do
  begin
    Self.FUsePalette   :=FUsePalette;
    Self.FUseColorRange:=FUseColorRange;
    Self.FStartColor   :=FStartColor;
    Self.FEndColor     :=FEndColor;
    Self.FMidColor     :=FMidColor;
    Self.FPaletteSteps :=FPaletteSteps;
    {$IFDEF D4}
    Self.FPalette      :=FPalette;
    {$ELSE}
    Self.ClearPalette;
    for t:=0 to FPalette.Count-1 do
    With FPalette[t]{$IFNDEF D4}^{$ENDIF} do Self.AddPalette(UpToValue,Color);
    {$ENDIF}
  end;
  inherited;
end;

Procedure TCustom3DPaletteSeries.DoBeforeDrawChart;
begin
  inherited;
  if (Count>0) and
     ({$IFDEF D4}Length(FPalette){$ELSE}FPalette.Count{$ENDIF}=0) then
        CreateDefaultPalette(FPaletteSteps);
end;

class Function TCustom3DPaletteSeries.GetEditorClass:String;
Begin
  result:='TGrid3DSeriesEditor'; { <-- dont translate ! }
end;

Procedure TCustom3DPaletteSeries.Clear;
begin
  inherited;
  if FUsePalette then ClearPalette;
end;

{$IFNDEF D4}
Function TCustom3DPalette.Get(Index:Integer):PGridPalette;
begin
  result:=inherited Items[Index];
end;
{$ENDIF}

class procedure TCustom3DPaletteSeries.CreateSubGallery(
  AddSubChart: TChartSubGalleryProc);
begin
  inherited;
  AddSubChart(TeeMsg_ColorRange);
end;

class procedure TCustom3DPaletteSeries.SetSubGallery(ASeries: TChartSeries;
  Index: Integer);
begin
  With TCustom3DPaletteSeries(ASeries) do
  Case Index of
    1: begin UseColorRange:=True; UsePalette:=False; end;
  else inherited;
  end;
end;

procedure TCustom3DPaletteSeries.CreateRangePalette;
var tmp     : Double;
    Delta   : Double;
    t       : Integer;
begin
  Delta:=MandatoryValueList.Range;
  if Delta>0 then
  begin
    ClearPalette;
    tmp:=Delta/PaletteSteps;
    for t:=1 to PaletteSteps do
        AddPalette(MandatoryValueList.MinValue+tmp*t,RangePercent(tmp*t/Delta));
  end;
end;

Procedure TCustom3DPaletteSeries.GalleryChanged3D(Is3D:Boolean);
Const Rots:Array[Boolean] of Integer=(0,335);
      Elev:Array[Boolean] of Integer=(270,340);
      Pers:Array[Boolean] of Integer=(0,60);
begin
  With ParentChart.View3DOptions do
  begin
    Zoom:=60;
    VertOffset:=-2;
    Rotation:=Rots[Is3D];
    Elevation:=Elev[Is3D];
    Perspective:=Pers[Is3D];
  end;
end;

procedure TCustom3DPaletteSeries.SetPaletteStyle(
  const Value: TTeePaletteStyle);
begin
  if FPaletteStyle<>Value then
  begin
    FPaletteStyle:=Value;
    ClearPalette;
    Repaint;
  end;
end;

{ TCustom3DGridSeries }
Constructor TCustom3DGridSeries.Create(AOwner: TComponent);
Begin
  inherited;
  FNumXValues:=10;
  FNumZValues:=10;
  Clear;
End;

Destructor TCustom3DGridSeries.Destroy;
begin
  ClearGridIndex;
  inherited;
end;

Procedure TCustom3DGridSeries.ClearGridIndex;
var t : Integer;
begin
  for t:=1 to MaxAllowedCells do
  if Assigned(FGridIndex[t]) then
  begin
    Dispose(FGridIndex[t]);
    FGridIndex[t]:=nil;
  end;
end;

Procedure TCustom3DGridSeries.Clear;
begin
  inherited;
  XValues.Order:=loNone;
  ClearGridIndex;
end;

Function TCustom3DGridSeries.GetGridIndex(X,Z:Integer):Integer;
begin
  if Assigned(FGridIndex[x]) then result:=FGridIndex[x]^[z]
                             else result:=-1;
end;

Procedure TCustom3DGridSeries.SetGridIndex(X,Z,Value:Integer);
begin
  if (X>=1) and (X<=MaxAllowedCells) and
     (Z>=1) and (Z<=MaxAllowedCells) then InternalSetGridIndex(x,z,value);
end;

Procedure TCustom3DGridSeries.InternalSetGridIndex(X,Z,Value:Integer);
var t : Integer;
begin
  if not Assigned(FGridIndex[x]) then
  begin
    New(FGridIndex[x]);
    for t:=1 to MaxAllowedCells do FGridIndex[x]^[t]:=-1;
  end;
  FGridIndex[x]^[z]:=Value;
end;

Procedure TCustom3DGridSeries.FillGridIndex;
Var MinX   : Double;
    MinZ   : Double;
    XCount : Integer;
    ZCount : Integer;

  Procedure FillIrregularGrid;
  type TIrregValues=Array[0..MaxAllowedCells-1] of Double;

    Procedure SearchValue(Var ACount:Integer; Var Values:TIrregValues; Const AValue:Double);
    var t : Integer;
    begin
      t:=0;
      while t<ACount do
      begin
        if Values[t]=AValue then Break
        else
        begin
          Inc(t);
          if t=ACount then
          begin
            Values[t]:=AValue;
            Inc(ACount);
          end;
        end;
      end;
    end;

    Function ValuePosition(ACount:Integer; Const Values:TIrregValues; Const AValue:Double):Integer;
    begin
      result:=0;
      while (AValue<>Values[result]) and (result<ACount) do Inc(result);
      Inc(result);
    end;

    Procedure SortValues(ACount:Integer; Var Values:TIrregValues);
    var t        : Integer;
        tt       : Integer;
        tmpMin   : Double;
        tmpIndex : Integer;
    begin
      for t:=1 to ACount-2 do {min already at 0}
      begin
        tmpMin:=Values[t];
        tmpIndex:=t;
        for tt:=t+1 to ACount-1 do  {get minimum }
        begin
          if Values[tt]<tmpMin then
          begin
            tmpMin:=Values[tt];
            tmpIndex:=tt;
          end;
          if tmpIndex<>t then
          begin
            Values[tmpIndex]:=Values[t];
            Values[t]:=tmpMin;
          end;
        end;
      end;
     end;

  Var XVals  : TIrregValues;
      ZVals  : TIrregValues;
      t      : Integer;
  begin
    XCount:=1;
    XVals[0]:=MinX;
    ZCount:=1;
    ZVals[0]:=MinZ;
    for t:=0 to Count-1 do
    begin
      SearchValue(XCount,XVals,XValues.Value[t]);
      SearchValue(ZCount,ZVals,ZValues.Value[t]);
    end;

    SortValues(XCount,XVals);
    SortValues(ZCount,ZVals);

    { use sorted xvals and zvals to index Mandatory ValueList in grid}
    for t:=0 to Count-1 do
      InternalSetGridIndex( ValuePosition(XCount,XVals,XValues.Value[t]),
                            ValuePosition(ZCount,ZVals,ZValues.Value[t]),
                            t);

  end;

  Procedure FillRegularGrid;
  var t : Integer;
  begin
    for t:=0 to Count-1 do
      InternalSetGridIndex(1+Round(XValues.Value[t]-MinX),1+Round(ZValues.Value[t]-MinZ),t);
    XCount:=1+Round(XValues.MaxValue-MinX);
    ZCount:=1+Round(ZValues.MaxValue-MinZ);
  end;

begin
  MinX:=XValues.MinValue;
  MinZ:=ZValues.MinValue;
  if FIrregularGrid then FillIrregularGrid
                    else FillRegularGrid;
  if XCount>FNumXValues then FNumXValues:=XCount;
  if ZCount>FNumZValues then FNumZValues:=ZCount;
end;

Function TCustom3DGridSeries.GetXZValue(X,Z:Integer):Double;
Begin
  if Assigned(FOnGetYValue) then result:=FOnGetYValue(Self,X,Z)
  else  { default sample random surface value formula }
  if (csDesigning in ComponentState) or
     (IInGallery) then
     result:=0.5*sqr(Cos(x/(FNumXValues*0.2)))+
                 sqr(Cos(z/(FNumZValues*0.2)))-
                 Cos(z/(FNumZValues*0.5))
  else
     result:=0;
end;

Function TCustom3DGridSeries.NumSampleValues:Integer;
begin
  result:=FNumXValues;
end;

Procedure TCustom3DGridSeries.ReCreateValues;
Begin
  CreateValues(FNumXValues,FNumZValues);
end;

Procedure TCustom3DGridSeries.SetNumXValues(Value:Integer);
Begin
  if Value<>FNumXValues then
  begin
    FNumXValues:=Value;
    ReCreateValues;
  end;
End;

Procedure TCustom3DGridSeries.SetNumZValues(Value:Integer);
Begin
  if Value<>FNumZValues then
  begin
    FNumZValues:=Value;
    ReCreateValues;
  end;
End;

Procedure TCustom3DGridSeries.AddValues(Source:TChartSeries);
Begin
  if Source is TCustom3DGridSeries then
  With TCustom3DGridSeries(Source) do
  begin
    Self.FNumXValues:=FNumXValues;
    Self.FNumZValues:=FNumZValues;
  end;
  inherited;
  FillGridIndex;
  Repaint;
end;

Procedure TCustom3DGridSeries.Assign(Source:TPersistent);
begin
  if Source is TCustom3DGridSeries then
  With TCustom3DGridSeries(Source) do
  begin
    Self.FNumXValues   :=FNumXValues;
    Self.FNumZValues   :=FNumZValues;
    Self.FIrregularGrid:=FIrregularGrid;
  end;
  inherited;
end;

Procedure TCustom3DGridSeries.SetIrregularGrid(Const Value:Boolean);
begin
  SetBooleanProperty(FIrregularGrid,Value);
end;

Function TCustom3DGridSeries.CanCreateValues:Boolean;
begin
  result:= Assigned(FOnGetYValue) or (csDesigning in ComponentState)
           or IInGallery;
end;

Procedure TCustom3DGridSeries.CreateValues(NumX,NumZ:Integer);
var x           : Integer;
    z           : Integer;
    OldCapacity : Integer;
Begin
  if CanCreateValues then
  begin
    FNumXValues:=NumX;
    FNumZValues:=NumZ;
    OldCapacity:=TeeDefaultCapacity;
    TeeDefaultCapacity:=NumX*NumZ;
    try
      Clear;
      for z:=1 to NumZ do
          for x:=1 to NumX do AddXYZ(X,GetXZValue(X,Z),Z {$IFNDEF D4},'',clTeeColor{$ENDIF});
      RefreshSeries;
    finally
      TeeDefaultCapacity:=OldCapacity;
    end;
    CreateDefaultPalette(FPaletteSteps);
  end;
End;

Procedure TCustom3DGridSeries.AddSampleValues(NumValues:Integer);
var OldGallery : Boolean;
    tmp        : Integer;
Begin
  if NumValues>0 then
  begin
    OldGallery:=IInGallery;
    IInGallery:=True;
    try
      tmp:=MinLong(MaxAllowedCells,NumValues);
      CreateValues(tmp,tmp);
    finally
      IInGallery:=OldGallery;
    end;
  end;
End;

Procedure TCustom3DGridSeries.DoBeforeDrawChart;
begin
  inherited;
  if Count>0 then FillGridIndex;
end;

Function TCustom3DGridSeries.ExistFourGridIndex(X,Z:Integer):Boolean;
begin
  result:=False;
  ValueIndex0:=GetGridIndex(X,Z);
  if ValueIndex0>-1 then
  begin
    ValueIndex1:=GetGridIndex(X+INextXCell,Z);
    if ValueIndex1>-1 then
    begin
      ValueIndex2:=GetGridIndex(X+INextXCell,Z+INextZCell);
      if ValueIndex2>-1 then
      begin
        ValueIndex3:=GetGridIndex(X,Z+INextZCell);
        result:=ValueIndex3>-1;
      end;
    end;
  end;
end;

function TCustom3DGridSeries.IsValidSeriesSource(Value:TChartSeries):Boolean;
begin
  result:=Value is TCustom3DGridSeries;
end;

{ TSurfaceSeries }
Constructor TSurfaceSeries.Create(AOwner: TComponent);
Begin
  inherited;
  INextXCell:=-1;
  INextZCell:=-1;
  FSideBrush:=TChartBrush.Create(CanvasChanged);
  FSideBrush.Style:=bsClear;
  FWaterLines:=CreateChartPen;
End;

Destructor TSurfaceSeries.Destroy;
Begin
  FSideBrush.Free;
  FWaterLines.Free;
  inherited;
End;

class Function TSurfaceSeries.GetEditorClass:String;
Begin
  result:='TSurfaceSeriesEditor'; { <-- dont translate ! }
end;

Procedure TSurfaceSeries.PrepareForGallery(IsEnabled:Boolean);
begin
  inherited;
  IInGallery:=True;
  CreateValues(10,10);
end;

Function TSurfaceSeries.CalcPointPos(Index:Integer):TPoint;
begin
  result.x:=GetHorizAxis.CalcXPosValue(XValues.Value[Index]);
  result.y:=GetVertAxis.CalcYPosValue(YValues.Value[Index]);
end;

Function TSurfaceSeries.FourGridIndex(x,z:Integer):Boolean;
begin
  result:=ExistFourGridIndex(x,z);
  if result then
  begin
    Points[0]:=CalcPointPos(ValueIndex0);
    Points[1]:=CalcPointPos(ValueIndex1);
    Points[2]:=CalcPointPos(ValueIndex2);
    Points[3]:=CalcPointPos(ValueIndex3);
  end;
end;

Function TSurfaceSeries.Clicked(x,y:Integer):Integer;
var tmpX : Integer;
    tmpZ : Integer;
begin
  result:=TeeNoPointClicked;
  if Count>0 then
  begin
    INextXCell:=-1;
    INextZCell:=-1;
    for tmpX:=2 to FNumXValues do
      for tmpZ:=2 to FNumZValues do { front to back... }
        if FourGridIndex(tmpX,tmpZ) and
           PointInPolygon(Point(x,y),Points) then
        begin
          result:=ValueIndex0;
          exit;
        end;
  end;
end;

Procedure TSurfaceSeries.SetSideBrush(Value:TChartBrush);
Begin
  FSideBrush.Assign(Value);
End;

Procedure TSurfaceSeries.DrawAllValues;

  Procedure DrawCells;
  var x   : Integer;
      z   : Integer;
      tmp : Integer;
  begin
    if FWaterFall then tmp:=0 else tmp:=1;
    if GetHorizAxis.Inverted then INextXCell:=1 else INextXCell:=-1;
    if ParentChart.DepthAxis.Inverted then
    begin
      INextZCell:=1;
      if GetHorizAxis.Inverted then
         for x:=FNumXValues-1 downto 1 do
             for z:=1 to FNumZValues-tmp do DrawCell(x,z)
      else
         for x:=2 to FNumXValues do
             for z:=1 to FNumZValues-tmp do DrawCell(x,z);
    end
    else
    begin
      INextZCell:=-1;
      if GetHorizAxis.Inverted then
         for x:=FNumXValues-1 downto 1 do
             for z:=FNumZValues downto 1+tmp do DrawCell(x,z)
      else
         for x:=2 to FNumXValues do
             for z:=FNumZValues downto 1+tmp do DrawCell(x,z);
    end;
  end;

  Procedure FastDraw;
  var tmpStyle : TTeeCanvasSurfaceStyle;
  begin
    if FWireFrame then tmpStyle:=tcsWire else
    if FDotFrame then tmpStyle:=tcsDot else tmpStyle:=tcsSolid;
    ParentChart.Canvas.Surface3D(tmpStyle,FSameBrush,FNumXValues,
                                 FNumZValues,FastCalcPoints);
  end;

  Procedure DrawSides;
  Var tmpYOrigin : Integer;

    Function CalcOnePoint(tmpRow,t:Integer; Var P0,P1:TPoint):Integer;
    Var tmpIndex : Integer;
    begin
      tmpIndex:=GetGridIndex(tmpRow,t);
      P0:=CalcPointPos(tmpIndex);
      P1.x:=P0.x;
      P1.y:=tmpYOrigin;
      result:=CalcZPos(tmpIndex);
    end;

  var tmpPoints  : TFourPoints;
      t          : Integer;
      z0         : Integer;
      z1         : Integer;
      tmpRow     : Integer;
      tmpP       : TPoint;
  begin
    if GetHorizAxis.Inverted then tmpRow:=1
                             else tmpRow:=FNumXValues;
    With GetVertAxis do
    if Inverted then tmpYOrigin:=CalcYPosValue(YValues.MaxValue)
                else tmpYOrigin:=CalcYPosValue(YValues.MinValue);
    With ParentChart.Canvas do
    begin
      AssignBrush(FSideBrush,FSideBrush.Color);
      Pen.Style:=psClear;
    end;
    for t:=FNumZValues downto 2 do
    begin
      Z0:=CalcOnePoint(tmpRow,t,tmpPoints[0],tmpPoints[1]);
      Z1:=CalcOnePoint(tmpRow,t-1,tmpPoints[3],tmpPoints[2]);
      ParentChart.Canvas.PlaneFour3D(tmpPoints,Z0,Z1);
    end;

    if ParentChart.DepthAxis.Inverted then tmpRow:=FNumZValues
                                      else tmpRow:=1;

    z0:=0;
    for t:=2 to FNumXValues do
    begin
      Z0:=CalcOnePoint(t,tmpRow,tmpPoints[0],tmpPoints[1]);
      Z1:=CalcOnePoint(t-1,tmpRow,tmpPoints[3],tmpPoints[2]);
      if t=FNumXValues then tmpP:=tmpPoints[0];
      ParentChart.Canvas.PlaneFour3D(tmpPoints,Z0,Z1);
    end;
    With ParentChart.Canvas do
    begin
      Pen.Style:=psSolid;
      VertLine3D(tmpP.X,tmpP.Y,tmpYOrigin,Z0);
    end;
  end;

  Procedure PrepareCanvas;
  begin
    With ParentChart.Canvas do
    begin
      if (not Self.Pen.Visible) and
         (not FWireFrame)   and
         (not FDotFrame) then Pen.Style:=psClear
                         else AssignVisiblePen(Self.Pen);
      AssignBrush(Self.Brush,SeriesColor);
      FSameBrush:=((not FUseColorRange) and (not FUsePalette)) or
                  Assigned(Self.Brush.Image.Graphic);
      if FWireFrame or FDotFrame then Brush.Style:=bsClear;
    end;
  end;

begin
  if Count>0 then
  begin
    PrepareCanvas;
    if ParentChart.Canvas.SupportsFullRotation then FastDraw
                                               else DrawCells;
    if FSideBrush.Style<>bsClear then DrawSides;
  end;
end;

Function TSurfaceSeries.FastCalcPoints( x,z:Integer;
                                        Var P0,P1:TPoint3D;
                                        Var Color0,Color1:TColor):Boolean;
var tmp0 : Double;
    tmp1 : Double;
begin
  result:=False;
  ValueIndex0:=GetGridIndex(x-1,z);
  if ValueIndex0<>-1 then
  begin
    ValueIndex1:=GetGridIndex(x,z);
    if ValueIndex1<>-1 then
    begin
      result:=True;
      With GetHorizAxis do
      begin
        P0.x:=CalcXPosValue(XValues.Value[ValueIndex0]);
        P1.x:=CalcXPosValue(XValues.Value[ValueIndex1]);
      end;
      P0.z:=CalcZPos(ValueIndex0);
      P1.z:=CalcZPos(ValueIndex1);
      tmp0:=YValues.Value[ValueIndex0];
      tmp1:=YValues.Value[ValueIndex1];
      With GetVertAxis do
      begin
        P0.y:=CalcYPosValue(tmp0);
        P1.y:=CalcYPosValue(tmp1);
      end;
      if not FSameBrush then
      begin
        Color0:=GetValueColorValue(tmp0);
        Color1:=GetValueColorValue(tmp1);
      end;
    end;
  end;
end;

Procedure TSurfaceSeries.DrawCell(X,Z:Integer);
var tmpColor : TColor;
    Z0       : Integer;
    Z1       : Integer;

  Procedure DrawTheCell;
  var tmp : Integer;
  begin
    With ParentChart.Canvas do
    begin
      if FWaterFall then
      begin
        tmp:=GetVertAxis.IEndPos;
        if not FWireFrame then
        begin
           Pen.Style:=psClear;
           PlaneWithZ(Points[0],Points[1],
                      Point(Points[1].X,tmp),Point(Points[0].X,tmp),
                      Z0);
        end;
        AssignVisiblePen(Self.Pen);
        LineWithZ(Points[0].X,Points[0].Y,Points[1].X,Points[1].Y,Z0);
        if WaterLines.Visible then
        begin
          AssignVisiblePen(WaterLines);
          VertLine3D(Points[0].X,Points[0].Y,tmp,Z0);
          VertLine3D(Points[1].X,Points[1].Y,tmp,Z0);
        end;
      end
      else
      if FDotFrame then
      begin
        With Points[0] do Pixels3D[X,Y,Z0]:=ValueColor[ValueIndex0];
        With Points[1] do Pixels3D[X,Y,Z0]:=ValueColor[ValueIndex1];
        With Points[2] do Pixels3D[X,Y,Z1]:=ValueColor[ValueIndex2];
        With Points[3] do Pixels3D[X,Y,Z1]:=ValueColor[ValueIndex3];
      end
      else PlaneFour3D(Points,Z0,Z1);
    end;
  end;

var tmp : Boolean;
Begin
  tmp:=False;
  if FWaterFall then
  begin
    ValueIndex0:=GetGridIndex(x,Z);
    if ValueIndex0>-1 then
    begin
      ValueIndex1:=GetGridIndex(x+INextXCell,Z);
      if ValueIndex1>-1 then
      begin
        Points[0]:=CalcPointPos(ValueIndex0);
        Points[1]:=CalcPointPos(ValueIndex1);
        tmp:=True;
      end;
    end;
  end
  else
  if FourGridIndex(X,Z) then
  begin
    Z1:=CalcZPos(ValueIndex2);
    tmp:=True;
  end;

  if tmp then
  begin
    tmpColor:=ValueColor[ValueIndex0];
    if tmpColor<>clNone then
    begin
      Z0:=CalcZPos(ValueIndex0);
      if FSameBrush then DrawTheCell
      else
      begin
        if FSmoothPalette then
           tmpColor:=GetValueColorValue(( YValues.Value[ValueIndex0]+
                                          YValues.Value[ValueIndex1]+
                                          YValues.Value[ValueIndex2]+
                                          YValues.Value[ValueIndex3])*0.25);

        if FWireFrame then ParentChart.Canvas.Pen.Color:=tmpColor
                      else ParentChart.Canvas.Brush.Color:=tmpColor;
        DrawTheCell;
      end;
    end;
  end;
end;

Procedure TSurfaceSeries.SetDotFrame(Value:Boolean);
Begin
  if Value then
  begin
    Pen.Visible:=True;
    FWireFrame:=False;
  end;
  SetBooleanProperty(FDotFrame,Value);
End;

Procedure TSurfaceSeries.SetWaterFall(Value:Boolean);
Begin
  SetBooleanProperty(FWaterFall,Value);
End;

Procedure TSurfaceSeries.SetWireFrame(Value:Boolean);
Begin
  if Value then
  begin
    Pen.Visible:=True;
    FDotFrame:=False;
  end;
  SetBooleanProperty(FWireFrame,Value);
End;

Procedure TSurfaceSeries.SetSmoothPalette(Value:Boolean);
begin
  SetBooleanProperty(FSmoothPalette,Value);
end;

Procedure TSurfaceSeries.Assign(Source:TPersistent);
begin
  if Source is TSurfaceSeries then
  With TSurfaceSeries(Source) do
  begin
    Self.FDotFrame     := FDotFrame;
    Self.SideBrush     := FSideBrush;
    Self.FSmoothPalette:= FSmoothPalette;
    Self.FWaterFall    := FWaterFall;
    Self.WaterLines    := WaterLines;
    Self.FWireFrame    := FWireFrame;
  end;
  inherited;
end;

class procedure TSurfaceSeries.CreateSubGallery(
  AddSubChart: TChartSubGalleryProc);
begin
  inherited;
  AddSubChart(TeeMsg_WireFrame);
  AddSubChart(TeeMsg_DotFrame);
  AddSubChart(TeeMsg_Sides);
  AddSubChart(TeeMsg_NoBorder);
end;

class procedure TSurfaceSeries.SetSubGallery(ASeries: TChartSeries;
  Index: Integer);
begin
  With TSurfaceSeries(ASeries) do
  Case Index of
    2: WireFrame:=True;
    3: DotFrame:=True;
    4: SideBrush.Style:=bsSolid;
    5: Pen.Visible:=False;
  else inherited;
  end;
end;

procedure TSurfaceSeries.SetWaterLines(const Value: TChartPen);
begin
  FWaterLines.Assign(Value);
end;

{ TContourSeries }
Constructor TContourSeries.Create(AOwner: TComponent);
Begin
  inherited;
  FLevels:=TContourLevels.Create(Self,TContourLevel);
  FAutomaticLevels:=True;
  ColorEachPoint:=True;
  FNumLevels:=10;
End;

Destructor TContourSeries.Destroy;
begin
  IModifyingLevels:=True;
  FLevels.Free;
  IModifyingLevels:=False;
  inherited;
end;

Procedure TContourSeries.Assign(Source:TPersistent);
begin
  if Source is TContourSeries then
  With TContourSeries(Source) do
  begin
    Self.FAutomaticLevels:=FAutomaticLevels;
    Self.Levels          :=FLevels;
    Self.FNumLevels      :=FNumLevels;
    Self.FYPosition      :=FYPosition;
    Self.FYPositionLevel :=FYPositionLevel;
  end;
  inherited;
end;

Procedure TContourSeries.SetNumLevels(Value:Integer);
begin
  SetIntegerProperty(FNumLevels,Value);
  if AutomaticLevels then
  begin
    Levels.Clear;
    AutomaticLevels:=True;
  end;
end;

Procedure TContourSeries.SetYPosition(Const Value:Double);
begin
  SetDoubleProperty(FYPosition,Value);
end;

Procedure TContourSeries.SetYPositionLevel(Value:Boolean);
begin
  SetBooleanProperty(FYPositionLevel,Value);
end;

procedure TContourSeries.DrawAllValues;
Var DifY  : Array[0..4] of Double;
    CellX : Array[0..4] of Integer;
    CellZ : Array[0..4] of Integer;
    m1    : Integer;
    m3    : Integer;
    x1    : Integer;
    x2    : Integer;
    z1    : Integer;
    z2    : Integer;

  Procedure CalcLinePoints(Side:Integer);

    Procedure PointSect(p1,p2:Integer; Var Ax,Az:Integer);
    Var tmp : Double;
    begin
      tmp:=DifY[p2]-DifY[p1];
      if tmp<>0 then
      begin
        Ax:=Round((DifY[p2]*CellX[p1]-DifY[p1]*CellX[p2])/tmp);
        Az:=Round((DifY[p2]*CellZ[p1]-DifY[p1]*CellZ[p2])/tmp);
      end
      else
      begin
        Ax:=Round(CellX[p2]-CellX[p1]);
        Az:=Round(CellZ[p2]-CellZ[p1]);
      end;
    end;

  begin
    case Side of
       1: begin
            x1:=CellX[m1]; z1:=CellZ[m1];
            x2:=CellX[0];  z2:=CellZ[0];
          end;
       2: begin
            x1:=CellX[0];  z1:=CellZ[0];
            x2:=CellX[m3]; z2:=CellZ[m3];
          end;
       3: begin
            x1:=CellX[m3]; z1:=CellZ[m3];
            x2:=CellX[m1]; z2:=CellZ[m1];
          end;
       4: begin
            x1:=CellX[m1]; z1:=CellZ[m1];
            PointSect(0,m3,x2,z2);
          end;
       5: begin
            x1:=CellX[0];  z1:=CellZ[0];
            PointSect(m3,m1,x2,z2);
          end;
       6: begin
            x1:=CellX[m3]; z1:=CellZ[m3];
            PointSect(m1,0,x2,z2);
          end;
       7: begin PointSect(m1,0,x1,z1); PointSect(0,m3,x2,z2); end;
       8: begin PointSect(0,m3,x1,z1); PointSect(m3,m1,x2,z2); end;
       9: begin PointSect(m3,m1,x1,z1); PointSect(m1,0,x2,z2); end;
    end;
  end;

  Function ContourCalcZPos(AIndex:Integer):Integer;
  var tmp : TChartAxis;
  begin
    if ParentChart.View3D then tmp:=ParentChart.DepthAxis
                          else tmp:=GetVertAxis;
       result:=tmp.CalcYPosValue(ZValues.Value[AIndex])
  end;

var tmpYPosition : Integer;

  Procedure DrawLevel(TheLevel:Integer);
  Const Sides : Array[-1..1,-1..1,-1..1] of Integer=
                   ( ( (0,0,8),(0,2,5),(7,6,9) ),
                     ( (0,3,4),(1,3,1),(4,3,0) ),
                     ( (9,6,7),(5,2,0),(8,0,0) )  );
  var Corner      : Integer;
      SignHeights : Array[0..4] of Integer;
      Side        : Integer;
  Begin
    { calc screen coordinates }
    CellZ[1]:=ContourCalcZPos(ValueIndex0);
    CellZ[3]:=ContourCalcZPos(ValueIndex3);
    CellZ[2]:=CellZ[1];
    CellZ[4]:=CellZ[3];
    CellZ[0]:=(CellZ[1]+CellZ[3]) div 2;  { mid Z pos }

    CellX[1]:=CalcXPos(ValueIndex0);
    CellX[2]:=CalcXPos(ValueIndex1);
    CellX[3]:=CellX[2];
    CellX[4]:=CellX[1];
    CellX[0]:=(CellX[1]+CellX[2]) div 2;  { mid X pos }

    for Corner:=0 to 4 do
    Begin
      if DifY[Corner]>0 then SignHeights[Corner]:= 1 else
      if DifY[Corner]<0 then SignHeights[Corner]:=-1 else
                             SignHeights[Corner]:= 0;
    End;

    With ParentChart.Canvas do
    for Corner:=1 to 4 do
    Begin
      m1:=Corner;
      if Corner=4 then m3:=1 else m3:=Succ(Corner);
      Side:=Sides[SignHeights[m1],SignHeights[0],SignHeights[m3]];
      if Side<>0 then
      Begin
        CalcLinePoints(Side);
        if ParentChart.View3D then
        begin
          MoveTo3D(x1,tmpYPosition,z1);
          LineTo3D(x2,tmpYPosition,z2);
        end
        else Line(x1,z1,x2,z2);
      end;
    end;
  end;

Var x,z      : Integer;
    DiffMin  : Double;
    DiffMax  : Double;
    TheLevel : Integer;
    tmp1     : Double;
    tmp2     : Double;
    tmp3     : Double;
    tmp4     : Double;
begin
  if Count>0 then
  begin
    ParentChart.Canvas.BackMode:=cbmTransparent;
    INextXCell:=1;
    INextZCell:=1;
    tmpYPosition:=GetVertAxis.CalcYPosValue(FYPosition);
    for z:=1 to FNumZValues-1 do { for each cell }
      for x:=1 to FNumXValues-1 do
      if ExistFourGridIndex(x,z) then  { if xz exists }
      begin
        With YValues do
        begin
          tmp1:=Value[ValueIndex0];
          tmp2:=Value[ValueIndex3];
          tmp3:=Value[ValueIndex1];
          tmp4:=Value[ValueIndex2];
        end;
        { calc min and max of 4 cell corners }
        DiffMin:=MinDouble(MinDouble(tmp1,tmp2),MinDouble(tmp3,tmp4));
        DiffMax:=MaxDouble(MaxDouble(tmp1,tmp2),MaxDouble(tmp3,tmp4));

        if (DiffMax>=FLevels[0].UpToValue) and
           (DiffMin<=FLevels[NumLevels-1].UpToValue) then { if inside levels }
        for TheLevel:=0 to NumLevels-1 do { for each level }
        With FLevels[TheLevel] do
        if (UpToValue>=DiffMin) and (UpToValue<=DiffMax) then { if inside level }
        begin
          if FYPositionLevel then
             tmpYPosition:=GetVertAxis.CalcYPosValue(FLevels[TheLevel].UpToValue);
          { corners dif Y to level value }
          DifY[1]:=tmp1-UpToValue;
          DifY[2]:=tmp3-UpToValue;
          DifY[3]:=tmp4-UpToValue;
          DifY[4]:=tmp2-UpToValue;
          { center point dif Y }
          DifY[0]:=0.25*( DifY[1]+DifY[2]+DifY[3]+DifY[4] );
          ParentChart.Canvas.AssignVisiblePenColor(Pen,Color);
          DrawLevel(TheLevel); { draw level }
        end;
      end;
  end;
end;

procedure TContourSeries.AddSampleValues(NumValues:Integer);
begin
  inherited;
  FYPosition:=0.5*(YValues.MaxValue+YValues.MinValue); { mid vertical pos }
end;

Procedure TContourSeries.PrepareForGallery(IsEnabled:Boolean);
begin
  inherited;
  if not IsEnabled then SeriesColor:=clGray;
end;

class Function TContourSeries.GetEditorClass:String;
Begin
  result:='TContourSeriesEditor'; { <-- dont translate ! }
end;

Function TContourSeries.CountLegendItems:Integer;
begin
  result:=FLevels.Count;
end;

Function TContourSeries.LegendItemColor(LegendIndex:Integer):TColor;
begin  { inverted legend }
  result:=FLevels[NumLevels-LegendIndex-1].Color;
end;

Function TContourSeries.LegendString( LegendIndex:Integer;
                                      LegendTextStyle:TLegendTextStyle ):String;
begin { inverted legend }
  result:=FormatFloat(ValueFormat,FLevels[NumLevels-LegendIndex-1].UpToValue);
end;

Function TContourSeries.MaxYValue:Double;
begin
  if ParentChart.View3D then result:=inherited MaxYValue
                        else result:=ZValues.MaxValue;
end;

Function TContourSeries.MinYValue:Double;
begin
  if ParentChart.View3D then result:=inherited MinYValue
                        else result:=ZValues.MinValue;
end;

Procedure TContourSeries.CreateAutoLevels; { calc Level values and colors }
Var t   : Integer;
    tmp : Double;
begin
  if AutomaticLevels and (Count>0) and (NumLevels>0) then
  begin
    IModifyingLevels:=True;
    ParentChart.AutoRepaint:=False;
    tmp:=YValues.Range/NumLevels;
    FLevels.Clear;
    for t:=0 to NumLevels-1 do
    With TContourLevel(FLevels.Add) do
    begin
      FUpTo:=YValues.MinValue+tmp*t;
      if ColorEachPoint then FColor:=ValueColor[t]
                        else FColor:=GetValueColorValue(FUpTo);
      if Assigned(FOnGetLevel) then FOnGetLevel(Self,t,FUpTo,FColor);
    end;
    IModifyingLevels:=False;
    ParentChart.AutoRepaint:=True;
  end;
end;

Procedure TContourSeries.DoBeforeDrawChart;
begin
  inherited;
  CreateAutoLevels;
end;

function TContourSeries.IsLevelsStored: Boolean;
begin
  result:=not AutomaticLevels;
end;

procedure TContourSeries.SetLevels(const Value: TContourLevels);
begin
  FLevels.Assign(Value);
end;

procedure TContourSeries.SetAutomaticLevels(const Value: Boolean);
begin
  SetBooleanProperty(FAutomaticLevels,Value);
end;

function TContourSeries.GetNumLevels: Integer;
begin
  if AutomaticLevels then result:=FNumLevels else result:=Levels.Count;
end;

class procedure TContourSeries.CreateSubGallery(
  AddSubChart: TChartSubGalleryProc);
begin
  inherited;
  AddSubChart(TeeMsg_Colors);
  AddSubChart(TeeMsg_Positions);
end;

class procedure TContourSeries.SetSubGallery(ASeries: TChartSeries;
  Index: Integer);
begin
  with TContourSeries(ASeries) do
  Case Index of
    2: ColorEachPoint:=True;
    3: YPositionLevel:=True;
  else inherited;
  end
end;

{ TContourLevel }
type TOwnedCollectionAccess=class(TOwnedCollection);

Constructor TContourLevel.Create(Collection: TCollection);
begin
  inherited;
  ISeries:=TContourSeries(TOwnedCollectionAccess(Collection).GetOwner);
  CheckAuto;
end;

Destructor TContourLevel.Destroy;
begin
  CheckAuto;
  With ISeries do if not FAutomaticLevels then Repaint;
  inherited;
end;

Procedure TContourLevel.CheckAuto;
begin
  With ISeries do
  if not IModifyingLevels then FAutomaticLevels:=False;
end;

procedure TContourLevel.SetColor(const Value: TColor);
begin
  ISeries.SetColorProperty(FColor,Value);
  CheckAuto;
end;

procedure TContourLevel.SetUpTo(const Value: Double);
begin
  ISeries.SetDoubleProperty(FUpTo,Value);
  CheckAuto;
end;

{ TContourLevels }
function TContourLevels.Get(Index: Integer): TContourLevel;
begin
  result:=TContourLevel(inherited Items[Index]);
end;

procedure TContourLevels.Put(Index: Integer; Const Value: TContourLevel);
begin
  Items[Index].Assign(Value);
end;

{ TWaterFallSeries }
Constructor TWaterFallSeries.Create(AOwner: TComponent);
Begin
  inherited;
  FWaterFall:=True;
end;

procedure TWaterFallSeries.GalleryChanged3D(Is3D: Boolean);
begin
  inherited;
  ParentChart.View3D:=Is3D;
end;

class function TWaterFallSeries.GetEditorClass: String;
begin
  result:='TWaterFallEditor';
end;

class procedure TWaterFallSeries.SetSubGallery(ASeries: TChartSeries;
  Index: Integer);
begin
  with TWaterFallSeries(ASeries) do
  Case Index of
    6: WaterLines.Visible:=False;
  else inherited;
  end;
end;

class procedure TWaterFallSeries.CreateSubGallery(
  AddSubChart: TChartSubGalleryProc);
begin
  inherited;
  AddSubChart(TeeMsg_NoLines);
end;

{ TColorGridSeries }
Constructor TColorGridSeries.Create(AOwner:TComponent);
begin
  inherited;
  FBitmap:=TBitmap.Create;
  {$IFNDEF CLX}
  FBitmap.PixelFormat:=pf24bit;
  {$ENDIF}
end;

Destructor TColorGridSeries.Destroy;
begin
  FBitmap.Free;
  inherited;
end;

procedure TColorGridSeries.DrawAllValues;

  Procedure FillBitmap;
  {$IFNDEF CLX}
  type RGBColor=packed record Blue,Green,Red:Byte; end;
       TColors=Array[0..MaxAllowedCells] of RGBColor;
  {$ENDIF}
  var z        : Integer;
      x        : Integer;
      {$IFNDEF CLX}
      P        : ^TColors;
      {$ENDIF}
      tmpIndex : Integer;
      tmp      : TColor;
  begin
    for z:=1 to NumZValues do
    begin
      {$IFNDEF CLX}
      P:=FBitmap.ScanLine[z-1];
      {$ENDIF}
      for x:=1 to NumXValues do
      begin
        tmpIndex:=GridIndex[x,z];
        if tmpIndex<>-1 then
        {$IFNDEF CLX}With P^[x-1] do{$ENDIF}
        begin
          tmp:=ValueColor[tmpIndex];
          {$IFDEF CLX}
          FBitmap.Canvas.Pen.Color:=tmp;
          FBitmap.Canvas.DrawPoint(x-1,z-1);
          {$ELSE}
          Red  :=GetRValue(tmp);
          Green:=GetGValue(tmp);
          Blue :=GetBValue(tmp);
          {$ENDIF}
        end;
      end;
    end;
  end;

Var R : TRect;

  Procedure DrawBitmap;
  begin
    With GetHorizAxis do
    begin
      R.Left:=CalcPosValue(XValues.MinValue);
      R.Right:=CalcPosValue(XValues.MaxValue+1);
    end;
    With GetVertAxis do
    begin
      R.Top:=CalcPosValue(ZValues.MinValue);
      R.Bottom:=CalcPosValue(ZValues.MaxValue+1);
    end;
    With ParentChart.Canvas do
         StretchDraw(CalcRect3D(R,MiddleZ),FBitmap);
  end;

  Procedure DrawGrid;
  var z : Integer;
      x : Integer;
  begin
    With ParentChart.Canvas do
    begin
      BackMode:=cbmTransparent;
      AssignVisiblePen(Self.Pen);
      for z:=2 to NumZValues do
        DoHorizLine(R.Left,R.Right,
                 GetVertAxis.CalcPosValue(ZValues.Value[GridIndex[1,z]]));
      for x:=2 to NumXValues do
        DoVertLine(GetHorizAxis.CalcPosValue(XValues.Value[GridIndex[x,1]]),
                                     R.Top,R.Bottom);
    end;
  end;

begin
  if Count>0 then
  begin
    FBitmap.Width:=NumXValues;
    FBitmap.Height:=NumZValues;
    FillBitmap;
    DrawBitmap;
    if Pen.Visible then DrawGrid;
  end;
end;

Function TColorGridSeries.MaxXValue:Double;
begin
  result:=XValues.MaxValue+1;
end;

Function TColorGridSeries.MaxYValue:Double;
begin
  result:=ZValues.MaxValue+1;
end;

Function TColorGridSeries.MinYValue:Double;
begin
  result:=ZValues.MinValue;
end;

Procedure TColorGridSeries.GalleryChanged3D(Is3D:Boolean);
begin
  ParentChart.View3D:=False;
  ParentChart.ClipPoints:=True;
end;

class function TColorGridSeries.GetEditorClass: String;
begin
  result:='TColorGridEditor';
end;

procedure TColorGridSeries.PrepareForGallery(IsEnabled: Boolean);
begin
  inherited;
  IInGallery:=True;
  CreateValues(8,4);
end;

class procedure TColorGridSeries.CreateSubGallery(
  AddSubChart: TChartSubGalleryProc);
begin
  inherited;
  AddSubChart(TeeMsg_NoGrid);
end;

class procedure TColorGridSeries.SetSubGallery(ASeries: TChartSeries;
  Index: Integer);
begin
  Case Index of
    2: ASeries.Pen.Visible:=False;
  else inherited
  end;
end;

initialization
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{
  RegisterTeeSeries( TSurfaceSeries, TeeMsg_GallerySurface,
                     TeeMsg_GalleryExtended,1);
  RegisterTeeSeries( TContourSeries, TeeMsg_GalleryContour,
                     TeeMsg_GalleryExtended,1);
  RegisterTeeSeries( TWaterFallSeries,TeeMsg_GalleryWaterFall,
                     TeeMsg_GalleryStats,1);
  RegisterTeeSeries( TColorGridSeries,TeeMsg_GalleryColorGrid,
                     TeeMsg_GalleryStats,1);
}
  RegisterTeeSeries( TSurfaceSeries, TeeMsg_GallerySurface,
                     TeeMsg_GalleryExtended,1,stSurface);
  RegisterTeeSeries( TContourSeries, TeeMsg_GalleryContour,
                     TeeMsg_GalleryExtended,1,stContour);
  RegisterTeeSeries( TWaterFallSeries,TeeMsg_GalleryWaterFall,
                     TeeMsg_GalleryStats,1,stWaterFall);
  RegisterTeeSeries( TColorGridSeries,TeeMsg_GalleryColorGrid,
                     TeeMsg_GalleryStats,1,stColorGrid);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
finalization
  UnRegisterTeeSeries([TSurfaceSeries,TContourSeries,TWaterFallSeries,
                       TColorGridSeries]);
end.
