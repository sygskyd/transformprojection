{**********************************************}
{   TSeriesPointer Component Editor Dialog     }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeePoEdi;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls, QComCtrls,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, ComCtrls,
     {$ENDIF}
     Chart, Series, TeCanvas, TeePenDlg, MultiLng;

type
  TSeriesPointerEditor = class(TForm)
    GPPoint: TGroupBox;
    CBDrawPoint: TCheckBox;
    CB3dPoint: TCheckBox;
    CBInflate: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    SEPointHorizSize: TEdit;
    SEPointVertSize: TEdit;
    CBStyle: TComboBox;
    Label3: TLabel;
    BPoinPenCol: TButtonPen;
    UDPointHorizSize: TUpDown;
    UDPointVertSize: TUpDown;
    CBPoDark: TCheckBox;
    GroupBox1: TGroupBox;
    BPointFillColor: TButton;
    CBDefBrushColor: TCheckBox;
    CBColorEach: TCheckBox;
    MlgSection1: TMlgSection;
    procedure FormShow(Sender: TObject);
    procedure CBDrawPointClick(Sender: TObject);
    procedure CB3dPointClick(Sender: TObject);
    procedure SEPointHorizSizeChange(Sender: TObject);
    procedure BPointFillColorClick(Sender: TObject);
    procedure CBStyleChange(Sender: TObject);
    procedure SEPointVertSizeChange(Sender: TObject);
    procedure CBInflateClick(Sender: TObject);
    procedure CBPoDarkClick(Sender: TObject);
    procedure CBDefBrushColorClick(Sender: TObject);
    {$IFNDEF CLX}
    procedure CBStyleDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    {$ENDIF}
    procedure CBColorEachClick(Sender: TObject);
    procedure BPoinPenColClick(Sender: TObject);
  private
    { Private declarations }
    ThePointer : TSeriesPointer;
    procedure SetPointerVisible(Value:Boolean);
    Procedure Enable3DPoint;
  public
    { Public declarations }
  end;

{ Adds a new sub-tab Form into the Series tab at EditChart dialog }
Procedure TeeInsertPointerForm( AParent:TControl;
                                APointer:TSeriesPointer;
                                Const Title:String);

implementation

{$R *.dfm}
uses TeeBrushDlg, TeEngine, TeeProcs, TeeConst, TeeEdiSeri;

Procedure TeeInsertPointerForm( AParent:TControl;
                                APointer:TSeriesPointer;
                                Const Title:String);

begin
  if Assigned(AParent) and (AParent.Owner is TFormTeeSeries) then
    TFormTeeSeries(AParent.Owner).InsertSeriesForm( TSeriesPointerEditor,
                                                    1,Title,
                                                    APointer);
end;

Procedure TSeriesPointerEditor.Enable3DPoint;
begin
  With ThePointer do
  CB3DPoint.Enabled:= ParentSeries.ParentChart.View3D and
                      ( (Style=psRectangle) or
                        (Style=psTriangle) or
                        (Style=psDownTriangle) or
                        (ParentSeries.ParentChart.Canvas.SupportsFullRotation
                         and (Style=psCircle))
                      );
  CBPoDark.Enabled:=CB3DPoint.Enabled;
end;


type TPointerAccess=class(TSeriesPointer);

procedure TSeriesPointerEditor.FormShow(Sender: TObject);
begin
  if TPersistent(Tag) is TSeriesPointer then
  begin
    ThePointer:=TSeriesPointer(Tag);
    CBColorEach.Visible:=False;
  end
  else
  begin
    ThePointer:=TCustomSeries(Tag).Pointer;
    CBColorEach.Visible:=True;
    CBColorEach.Checked:=TCustomSeries(Tag).ColorEachPoint;
  end;
  With ThePointer do
  Begin
    SetPointerVisible(Visible);
    CBDrawPoint.Checked:=Visible;
    CB3DPoint.Checked:=Draw3D;
    Enable3DPoint;
    UDPointHorizSize.Position:=HorizSize;
    UDPointVertSize.Position :=VertSize;
    CBStyle.ItemIndex:=Ord(Style);
    CBInflate.Checked:=InflateMargins;
    CBPoDark.Checked:=Dark3D;
    CBDefBrushColor.Checked:=Brush.Color=clTeeColor;
    BPoinPenCol.LinkPen(Pen);

    if not TPointerAccess(ThePointer).AllowChangeSize then
       ShowControls(False,[ Label1,Label2,SEPointHorizSize,
                            SEPointVertSize,UDPointHorizSize,UDPointVertSize]);
  end;
end;

procedure TSeriesPointerEditor.SetPointerVisible(Value:Boolean);
begin
  ThePointer.Visible:=Value;
  if Value then Enable3DPoint
  else
  begin
    CB3DPoint.Enabled:=False;
    CBPoDark.Enabled:=False;
  end;
  EnableControls(Value,[ CBInflate,SEPointHorizSize,SEPointVertSize,
                         UDPointHorizSize,UDPointVertSize,BPointFillColor,
                         CBStyle,BPoinPenCol,CBDefBrushColor] );
end;

procedure TSeriesPointerEditor.CBDrawPointClick(Sender: TObject);
begin
  SetPointerVisible(CBDrawPoint.Checked);
end;

procedure TSeriesPointerEditor.CB3dPointClick(Sender: TObject);
begin
  ThePointer.Draw3D:=CB3DPoint.Checked;
end;

procedure TSeriesPointerEditor.SEPointHorizSizeChange(Sender: TObject);
begin
  if Showing then ThePointer.HorizSize:=UDPointHorizSize.Position;
end;

procedure TSeriesPointerEditor.BPointFillColorClick(Sender: TObject);
begin
  EditChartBrush(Self,ThePointer.Brush);
  CBDefBrushColor.Checked:=ThePointer.Brush.Color=clTeeColor;
  CBStyle.Repaint;
end;

procedure TSeriesPointerEditor.CBStyleChange(Sender: TObject);
begin
  ThePointer.Style:=TSeriesPointerStyle(CBStyle.ItemIndex);
  Enable3DPoint;
end;

procedure TSeriesPointerEditor.SEPointVertSizeChange(Sender: TObject);
begin
  if Showing then ThePointer.VertSize:=UDPointVertSize.Position;
end;

procedure TSeriesPointerEditor.CBInflateClick(Sender: TObject);
begin
  ThePointer.InflateMargins:=CBInflate.Checked;
end;

procedure TSeriesPointerEditor.CBPoDarkClick(Sender: TObject);
begin
  ThePointer.Dark3D:=CBPoDark.Checked;
end;

procedure TSeriesPointerEditor.CBDefBrushColorClick(Sender: TObject);
begin
  if CBDefBrushColor.Checked then ThePointer.Brush.Color:=clTeeColor;
end;

{$IFNDEF CLX}
procedure TSeriesPointerEditor.CBStyleDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var tmp : TColor;
    ACanvas: TTeeCanvas3D;
begin
  CBStyle.Canvas.FillRect(Rect);
  With TPointerAccess(ThePointer) do
  begin
    ACanvas:=TTeeCanvas3D.Create;
    try
      ACanvas.ReferenceCanvas:=CBStyle.Canvas;
      tmp:=Brush.Color;
      if tmp=clTeeColor then tmp:=ParentSeries.SeriesColor;
      PrepareCanvas(ACanvas,tmp);
      DrawPointer(ACanvas,False,Rect.Left+6,Rect.Top+6+2,4,4,tmp,TSeriesPointerStyle(Index));
    finally
      ACanvas.Free;
    end;
  end;
  With CBStyle,Canvas do
  begin
    Brush.Style:=bsClear;
    {$IFNDEF CLX}
    {$IFDEF D4}
    TControlCanvas(CBStyle.Canvas).UpdateTextFlags;
    {$ENDIF}
    {$ENDIF}
    TextOut(Rect.Left+14,Rect.Top+2,Items[Index]);
  end;
end;
{$ENDIF}

procedure TSeriesPointerEditor.CBColorEachClick(Sender: TObject);
begin
  TCustomSeries(Tag).ColorEachPoint:=CBColorEach.Checked;
end;

procedure TSeriesPointerEditor.BPoinPenColClick(Sender: TObject);
begin
  CBStyle.Repaint;
end;

initialization
  RegisterClass(TSeriesPointerEditor);
end.
