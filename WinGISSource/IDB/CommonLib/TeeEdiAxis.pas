{**********************************************}
{  TCustomChart (or derived) Editor Dialog     }
{  Copyright (c) 1996-2000 by David Berneda    }
{**********************************************}
{$I teedefs.inc}
unit TeeEdiAxis;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QComCtrls, QExtCtrls, QButtons,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls, Buttons,
     {$ENDIF}
     TeEngine, TeeEdiFont, TeCanvas, TeeProcs, TeePenDlg;

type
  TFormTeeAxis = class(TForm)
    PageAxis: TPageControl;
    TabScales: TTabSheet;
    L5: TLabel;
    L8: TLabel;
    LAxisMax: TLabel;
    LAxisMin: TLabel;
    L6: TLabel;
    LAxisIncre: TLabel;
    CBAutomatic: TCheckBox;
    CBLogarithmic: TCheckBox;
    CBInverted: TCheckBox;
    CBAutoMax: TCheckBox;
    CBAutoMin: TCheckBox;
    BAxisMax: TButton;
    BAxisMin: TButton;
    BIncre: TButton;
    TabTitle: TTabSheet;
    TabLabels: TTabSheet;
    TabTicks: TTabSheet;
    L28: TLabel;
    L29: TLabel;
    BAxisPen: TButtonPen;
    BTickPen: TButtonPen;
    BTickInner: TButtonPen;
    SEAxisTickLength: TEdit;
    SEInnerTicksLength: TEdit;
    BAxisGrid: TButtonPen;
    CBTickOnLabels: TCheckBox;
    CBGridCentered: TCheckBox;
    UDInnerTicksLength: TUpDown;
    UDAxisTickLength: TUpDown;
    TabPositions: TTabSheet;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    EPos: TEdit;
    EStart: TEdit;
    EEnd: TEdit;
    UDPos: TUpDown;
    UDStart: TUpDown;
    UDEnd: TUpDown;
    TabMinorTicks: TTabSheet;
    BTickMinor: TButtonPen;
    L30: TLabel;
    SEAxisMinorTickLen: TEdit;
    UDAxisMinorTickLen: TUpDown;
    L31: TLabel;
    SEMinorCount: TEdit;
    UDMinorCount: TUpDown;
    BMinorGrid: TButtonPen;
    CBVisible: TCheckBox;
    CBOtherSide: TCheckBox;
    CBHorizAxis: TCheckBox;
    Label2: TLabel;
    ELogBase: TEdit;
    UDLogBase: TUpDown;
    PageTitle: TPageControl;
    TabTitleStyle: TTabSheet;
    L14: TLabel;
    ETitle: TEdit;
    L3: TLabel;
    SETitleAngle: TEdit;
    UDTitleAngle: TUpDown;
    L9: TLabel;
    SETitleSize: TEdit;
    UDTitleSize: TUpDown;
    CBTitleVisible: TCheckBox;
    TabTitleFont: TTabSheet;
    PageLabels: TPageControl;
    TabLabStyle: TTabSheet;
    CBLabels: TCheckBox;
    CBMultiline: TCheckBox;
    CBOnAxis: TCheckBox;
    CBRoundFirst: TCheckBox;
    SELabelsSize: TEdit;
    L26: TLabel;
    L23: TLabel;
    SELabelsAngle: TEdit;
    UDLabelsAngle: TUpDown;
    UDLabelsSize: TUpDown;
    L20: TLabel;
    SESepar: TEdit;
    UDSepar: TUpDown;
    RGLabelStyle: TRadioGroup;
    TabLabelsFont: TTabSheet;
    Panel1: TPanel;
    TabLabFormat: TTabSheet;
    LabelAxisFormat: TLabel;
    CBFormat: TComboBox;
    CBExpo: TCheckBox;
    CBLabelAlign: TCheckBox;
    Splitter1: TSplitter;
    Panel2: TPanel;
    BAdd: TSpeedButton;
    BDelete: TSpeedButton;
    Panel4: TPanel;
    CBShow: TCheckBox;
    CBAxisBeh: TCheckBox;
    Label1: TLabel;
    LBAxes: TListBox;
    procedure FormShow(Sender: TObject);
    procedure CBVisibleClick(Sender: TObject);
    procedure SEAxisTickLengthChange(Sender: TObject);
    procedure CBAutomaticClick(Sender: TObject);
    procedure ETitleChange(Sender: TObject);
    procedure BAxisMaxClick(Sender: TObject);
    procedure BAxisMinClick(Sender: TObject);
    procedure BIncreClick(Sender: TObject);
    procedure CBLogarithmicClick(Sender: TObject);
    procedure SEInnerTicksLengthChange(Sender: TObject);
    procedure SEAxisMinorTickLenChange(Sender: TObject);
    procedure SEMinorCountChange(Sender: TObject);
    procedure CBAutoMaxClick(Sender: TObject);
    procedure CBInvertedClick(Sender: TObject);
    procedure SETitleAngleChange(Sender: TObject);
    procedure SETitleSizeChange(Sender: TObject);
    procedure CBLabelsClick(Sender: TObject);
    procedure SELabelsAngleChange(Sender: TObject);
    procedure RGLabelStyleClick(Sender: TObject);
    procedure SELabelsSizeChange(Sender: TObject);
    procedure CBOnAxisClick(Sender: TObject);
    procedure SESeparChange(Sender: TObject);
    procedure CBRoundFirstClick(Sender: TObject);
    procedure CBTickOnLabelsClick(Sender: TObject);
    procedure CBGridCenteredClick(Sender: TObject);
    procedure CBMultilineClick(Sender: TObject);
    procedure PageAxisChange(Sender: TObject);
    procedure CBShowClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PageAxisChanging(Sender: TObject; var AllowChange: Boolean);
    procedure EPosChange(Sender: TObject);
    procedure EStartChange(Sender: TObject);
    procedure EEndChange(Sender: TObject);
    procedure LBAxesClick(Sender: TObject);
    procedure BAddClick(Sender: TObject);
    procedure CBOtherSideClick(Sender: TObject);
    procedure BDeleteClick(Sender: TObject);
    procedure CBHorizAxisClick(Sender: TObject);
    procedure ELogBaseChange(Sender: TObject);
    procedure CBTitleVisibleClick(Sender: TObject);
    procedure CBFormatChange(Sender: TObject);
    procedure CBExpoClick(Sender: TObject);
    procedure CBAxisBehClick(Sender: TObject);
    procedure CBLabelAlignClick(Sender: TObject);
  private
    { Private declarations }
    CreatingForm : Boolean;
    ILabelsFont  : TTeeFontEditor;
    ITitleFont   : TTeeFontEditor;
    Procedure SetAxisIndex;
    Procedure SetAxisScales;
    Procedure EnableLogBaseControls;
    procedure EditAxisMaxMin(Const ATitle:String; ACheckBox:TCheckBox;
                             IsMax:Boolean);
  public
    { Public declarations }
    TheAxis      : TChartAxis;
    Constructor CreateAxis(Owner:TComponent; AAxis:TChartAxis);
  end;

procedure TeeAddAxes(AChart:TCustomAxisPanel; AList:TStrings; AddDepth:Boolean);

implementation

{$R *.dfm}
Uses TeeConst, TeeAxisIncr, TeeBrushDlg, TeeAxMaxMin, Chart;

procedure TeeAddAxes(AChart:TCustomAxisPanel; AList:TStrings; AddDepth:Boolean);
var t : Integer;
begin
  With AList do
  begin
    Clear;
    Add(TeeMsg_LeftAxis);
    Add(TeeMsg_RightAxis);
    Add(TeeMsg_TopAxis);
    Add(TeeMsg_BottomAxis);
    if AddDepth then Add(TeeMsg_DepthAxis);
    for t:=5 to AChart.Axes.Count-1 do
        Add(TeeMsg_CustomAxesEditor+IntToStr(t-5));
  end;
end;

Constructor TFormTeeAxis.CreateAxis(Owner:TComponent; AAxis:TChartAxis);
begin
  inherited Create(Owner);
  TheAxis:=AAxis;
  ILabelsFont:=InsertTeeFontEditor(TabLabelsFont);
  ITitleFont:=InsertTeeFontEditor(TabTitleFont);
end;

Procedure TFormTeeAxis.SetAxisIndex;
begin
  With TheAxis.ParentChart,LBAxes do
  if TheAxis=LeftAxis   then ItemIndex:=0 else
  if TheAxis=RightAxis  then ItemIndex:=1 else
  if TheAxis=TopAxis    then ItemIndex:=2 else
  if TheAxis=BottomAxis then ItemIndex:=3 else
  if TheAxis=DepthAxis  then ItemIndex:=4 else
     ItemIndex:=Axes.IndexOf(TheAxis);
end;

procedure TFormTeeAxis.FormShow(Sender: TObject);
begin
  TeeAddAxes(TheAxis.ParentChart,LBAxes.Items,True);
  SetAxisIndex;

  CBShow.Checked:=TheAxis.ParentChart.AxisVisible;
  CBAxisBeh.Checked:=TheAxis.ParentChart.AxisBehind;
  PageAxis.ActivePage:=TabScales;
  PageLabels.ActivePage:=TabLabStyle;
  PageTitle.ActivePage:=TabTitleStyle;
  PageAxisChange(Self);
end;

procedure TFormTeeAxis.CBVisibleClick(Sender: TObject);
begin
  TheAxis.Visible:=CBVisible.Checked;
end;

procedure TFormTeeAxis.SEAxisTickLengthChange(Sender: TObject);
begin
  if not CreatingForm then TheAxis.TickLength:=UDAxisTickLength.Position;
end;

procedure TFormTeeAxis.CBAutomaticClick(Sender: TObject);
begin
  With TheAxis do
  begin
    Automatic:=CBAutomatic.Checked;
    if Automatic then AdjustMaxMin
    else
    begin
      AutomaticMaximum:=False;
      AutomaticMinimum:=False;
    end;
  end;
  SetAxisScales;
end;

procedure TFormTeeAxis.ETitleChange(Sender: TObject);
begin
  TheAxis.Title.Caption:=ETitle.Text;
end;

procedure TFormTeeAxis.EditAxisMaxMin(Const ATitle:String; ACheckBox:TCheckBox;
                                      IsMax:Boolean);
begin
  With TAxisMaxMin.Create(Self) do
  try
    Caption:=ATitle+' '+(TheAxis.ParentChart as TCustomChart).AxisTitleOrName(TheAxis);
    IsDateTime:=TheAxis.IsDateTime;
    if IsMax then MaxMin:=TheAxis.Maximum
             else MaxMin:=TheAxis.Minimum;
    if ShowModal=mrOk then
    Begin
      if IsMax then TheAxis.Maximum:=MaxMin
               else TheAxis.Minimum:=MaxMin;
      ACheckBox.Checked:=False;
      SetAxisScales;
    end;
  finally
    Free;
  end;
end;

procedure TFormTeeAxis.BAxisMaxClick(Sender: TObject);
begin
  EditAxisMaxMin(TeeMsg_Maximum,CBAutoMax,True);
end;

procedure TFormTeeAxis.BAxisMinClick(Sender: TObject);
begin
  EditAxisMaxMin(TeeMsg_Minimum,CBAutoMin,False);
end;

procedure TFormTeeAxis.BIncreClick(Sender: TObject);
begin
  With TAxisIncrement.Create(Self) do
  try
    Caption:=Format(TeeMsg_DesiredIncrement,
                   [(TheAxis.ParentChart as TCustomChart).AxisTitleOrName(TheAxis)]);
    IsDateTime:=TheAxis.IsDateTime;
    IsExact:=TheAxis.ExactDateTime;
    Increment:=TheAxis.Increment;
    IStep:=FindDateTimeStep(Increment);
    if ShowModal=mrOk then
    Begin
      TheAxis.Increment:=Increment;
      TheAxis.ExactDateTime:=IsExact;
      SetAxisScales;
    end;
  finally
    Free;
  end;
end;

procedure TFormTeeAxis.CBLogarithmicClick(Sender: TObject);
begin
  try
    try
      TheAxis.Logarithmic:=CBLogarithmic.Checked;
    except
      on AxisException do
      Begin
        TheAxis.Logarithmic:=False;
        CBLogarithmic.Checked:=False;
        Raise;
      end;
    end;
  finally
    EnableLogBaseControls;
  end;
end;

Procedure TFormTeeAxis.EnableLogBaseControls;
begin
  ELogBase.Enabled:=CBLogarithmic.Checked and CBLogarithmic.Enabled;
  UDLogBase.Enabled:=ELogBase.Enabled;
end;

procedure TFormTeeAxis.SEInnerTicksLengthChange(Sender: TObject);
begin
  if not CreatingForm then
     TheAxis.TickInnerLength:=UDInnerTicksLength.Position;
end;

procedure TFormTeeAxis.SEAxisMinorTickLenChange(Sender: TObject);
begin
  if not CreatingForm then TheAxis.MinorTickLength:=UDAxisMinorTickLen.Position;
end;

procedure TFormTeeAxis.SEMinorCountChange(Sender: TObject);
begin
  if not CreatingForm then TheAxis.MinorTickCount:=UDMinorCount.Position;
end;

procedure TFormTeeAxis.CBAutoMaxClick(Sender: TObject);
begin
  With TheAxis do
  if Sender=CBAutoMax then AutomaticMaximum:=CBAutoMax.Checked
                      else AutomaticMinimum:=CBAutoMin.Checked;
  TheAxis.AdjustMaxMin;
  SetAxisScales;
end;

procedure TFormTeeAxis.CBInvertedClick(Sender: TObject);
begin
  TheAxis.Inverted:=CBInverted.Checked;
end;

procedure TFormTeeAxis.SETitleAngleChange(Sender: TObject);
begin
  if not CreatingForm then TheAxis.Title.Angle:=UDTitleAngle.Position;
end;

procedure TFormTeeAxis.SETitleSizeChange(Sender: TObject);
begin
  if not CreatingForm then TheAxis.TitleSize:=UDTitleSize.Position;
end;

procedure TFormTeeAxis.CBLabelsClick(Sender: TObject);
begin
  TheAxis.Labels:=CBLabels.Checked;
end;

procedure TFormTeeAxis.SELabelsAngleChange(Sender: TObject);
begin
  if not CreatingForm then TheAxis.LabelsAngle:=UDLabelsAngle.Position;
end;

procedure TFormTeeAxis.RGLabelStyleClick(Sender: TObject);
begin
  TheAxis.LabelStyle:=TAxisLabelStyle(RGLabelStyle.ItemIndex);
end;

procedure TFormTeeAxis.SELabelsSizeChange(Sender: TObject);
begin
  if not CreatingForm then TheAxis.LabelsSize:=UDLabelsSize.Position;
end;

procedure TFormTeeAxis.CBOnAxisClick(Sender: TObject);
begin
  TheAxis.LabelsOnAxis:=CBOnAxis.Checked;
end;

procedure TFormTeeAxis.SESeparChange(Sender: TObject);
begin
  if not CreatingForm then TheAxis.LabelsSeparation:=UDSepar.Position;
end;

procedure TFormTeeAxis.CBRoundFirstClick(Sender: TObject);
begin
  TheAxis.RoundFirstLabel:=CBRoundFirst.Checked;
end;

procedure TFormTeeAxis.CBTickOnLabelsClick(Sender: TObject);
begin
  TheAxis.TickOnLabelsOnly:=CBTickOnLabels.Checked;
end;

procedure TFormTeeAxis.CBGridCenteredClick(Sender: TObject);
begin
  TheAxis.GridCentered:=CBGridCentered.Checked;
end;

procedure TFormTeeAxis.CBMultilineClick(Sender: TObject);
begin
  TheAxis.LabelsMultiLine:=CBMultiline.Checked;
end;

Procedure TFormTeeAxis.SetAxisScales;
Begin
  With TheAxis do
  Begin
    LAxisIncre.Caption:=GetIncrementText(Self,Increment,IsDateTime,ExactDateTime,AxisValuesFormat);
    if IsDateTime then
    begin
      if Minimum>=1 then LAxisMin.Caption:=DateTimeToStr(Minimum)
                    else LAxisMin.Caption:=TimeToStr(Minimum);
      if Maximum>=1 then LAxisMax.Caption:=DateTimeToStr(Maximum)
                    else LAxisMax.Caption:=TimeToStr(Maximum);
    end
    else
    begin
      LAxisMin.Caption:=FormatFloat(AxisValuesFormat,Minimum);
      LAxisMax.Caption:=FormatFloat(AxisValuesFormat,Maximum);
    end;
    CBAutomatic.Checked:=Automatic;
    CBAutoMax.Checked:=AutomaticMaximum;
    CBAutoMin.Checked:=AutomaticMinimum;
    CBLogarithmic.Checked:=Logarithmic;
    CBLogarithmic.Enabled:=not IsDepthAxis;
    UDLogBase.Position:=LogarithmicBase;
    EnableLogBaseControls;
    CBInverted.Checked:=Inverted;
    CBVisible.Checked:=Visible;
    { enable controls... }
    BAxisMax.Enabled:=(not Automatic) and (not AutomaticMaximum);
    BAxisMin.Enabled:=(not Automatic) and (not AutomaticMinimum);
    EnableControls(not Automatic,[CBAutoMax,CBAutoMin]);
  end;
end;

procedure TFormTeeAxis.PageAxisChange(Sender: TObject);

  Procedure SetAxisLabels;
  var tmp : String;
  begin  { Axis Labels  }
    With TheAxis do
    Begin
      CBLabels.Checked:=Labels;
      RGLabelStyle.ItemIndex:=Ord(LabelStyle);
      CBOnAxis.Checked:=LabelsOnAxis;
      CBRoundFirst.Checked:=RoundFirstLabel;
      UDLabelsAngle.Position:=LabelsAngle;
      UDSepar.Position:=LabelsSeparation;
      UDLabelsSize.Position:=LabelsSize;
      CBMultiline.Checked:=LabelsMultiLine;
      CBExpo.Checked:=LabelsExponent;
      CBFormat.Items.Clear;
      if IsDateTime then
      begin
        With CBFormat.Items do
        if Maximum-Minimum<=1 then
        begin
          Add(ShortTimeFormat);
          Add(LongTimeFormat);
          Add('hh:mm');
          Add('hh:mm:ss');
          Add('hh:mm:ss.zzz');
        end
        else
        begin
          Add(ShortDateFormat);
          Add(LongDateFormat);
          Add('yyyy-mm-dd');
          Add('mm-dd-yyyy');
          Add('dd-mm-yyyy');
          Add('ddd');
          Add('dddd');
          Add('mmm');
          Add('mmmm');
        end;

        LabelAxisFormat.Caption:=TeeMsg_DateTimeFormat;
        tmp:=DateTimeFormat;
        if tmp='' then tmp:=DateTimeDefaultFormat(Maximum-Minimum);
      end
      else
      begin
        AddDefaultValueFormats(CBFormat.Items);
        LabelAxisFormat.Caption:=TeeMsg_ValuesFormat;
        tmp:=AxisValuesFormat;
        if tmp='' then tmp:=TeeMsg_DefValueFormat;
        tmp:=DelphiToLocalFormat(tmp);
      end;
      With CBFormat.Items do if IndexOf(tmp)=-1 then Add(tmp);
      CBFormat.Text:=tmp;
      ILabelsFont.RefreshControls(LabelsFont);
    end;
  end;

  Procedure SetAxisTicks;
  begin
    With TheAxis do
    Begin
      UDAxisTickLength.Position:=TickLength;
      UDInnerTicksLength.Position:=TickInnerLength;
      CBTickOnLabels.Checked:=TickOnLabelsOnly;
      CBGridCentered.Checked:=GridCentered;
    end;
  end;

  Procedure SetAxisMinorTicks;
  begin
    With TheAxis do
    Begin
      UDAxisMinorTickLen.Position:=MinorTickLength;
      UDMinorCount.Position:=MinorTickCount;
    end;
  end;

  Procedure SetAxisTitle;
  begin
    With TheAxis do
    Begin
      ETitle.Text:=Title.Caption;
      UDTitleAngle.Position:=Title.Angle;
      UDTitleSize.Position:=TitleSize;
      CBTitleVisible.Checked:=Title.Visible;
      ITitleFont.RefreshControls(Title.Font);
    end;
  end;

  Procedure SetAxisPositions;
  begin
    With TheAxis do
    Begin
      UDPos.Position:=Round(PositionPercent);
      UDStart.Position:=Round(StartPosition);
      UDEnd.Position:=Round(EndPosition);
      CBOtherSide.Checked:=OtherSide;
      CBOtherSide.Enabled:=LBAxes.ItemIndex>4;
      CBHorizAxis.Checked:=Horizontal;
      CBHorizAxis.Enabled:=LBAxes.ItemIndex>4;
      EPos.Enabled:=not IsDepthAxis;
      EStart.Enabled:=not IsDepthAxis;
      EEnd.Enabled:=not IsDepthAxis;
    end;
  end;

begin
  With TheAxis do
  begin
    BAxisGrid.LinkPen(Grid);
    BTickMinor.LinkPen(MinorTicks);
    BTickInner.LinkPen(TicksInner);
    BTickPen.LinkPen(Ticks);
    BAxisPen.LinkPen(Axis);
    BMinorGrid.LinkPen(MinorGrid);
  end;

  With PageAxis do
  if ActivePage=TabLabels then SetAxisLabels else
  if ActivePage=TabScales then SetAxisScales else
  if ActivePage=TabTitle then SetAxisTitle else
  if ActivePage=TabTicks then SetAxisTicks else
  if ActivePage=TabMinorTicks then SetAxisMinorTicks else
  if ActivePage=TabPositions then SetAxisPositions;

  BDelete.Enabled:=LBAxes.ItemIndex>4;
  CBVisible.Checked:=TheAxis.Visible;
  
  CBLabelAlign.Checked:=TheAxis.LabelsAlign=alDefault;
  CBLabelAlign.Enabled:=(not TheAxis.Horizontal) and (not TheAxis.IsDepthAxis);

  CreatingForm:=False;
end;

procedure TFormTeeAxis.CBShowClick(Sender: TObject);
begin
  TheAxis.ParentChart.AxisVisible:=CBShow.Checked;
end;

procedure TFormTeeAxis.FormCreate(Sender: TObject);
begin
  CreatingForm:=True;
end;

procedure TFormTeeAxis.PageAxisChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  CreatingForm:=True;
end;

procedure TFormTeeAxis.EPosChange(Sender: TObject);
begin
  if not CreatingForm then TheAxis.PositionPercent:=UDPos.Position;
end;

procedure TFormTeeAxis.EStartChange(Sender: TObject);
begin
  if not CreatingForm then
  begin
    if UDStart.Position<TheAxis.EndPosition then
       TheAxis.StartPosition:=UDStart.Position
    else
       UDStart.Position:=Round(TheAxis.StartPosition);
  end;
end;

procedure TFormTeeAxis.EEndChange(Sender: TObject);
begin
  if not CreatingForm then
  begin
    if UDEnd.Position>TheAxis.StartPosition then
       TheAxis.EndPosition:=UDEnd.Position
    else
       UDEnd.Position:=Round(TheAxis.EndPosition);
  end;
end;

procedure TFormTeeAxis.LBAxesClick(Sender: TObject);
begin
  With TheAxis.ParentChart do
  Case LBAxes.ItemIndex of
    0: TheAxis:=LeftAxis;
    1: TheAxis:=RightAxis;
    2: TheAxis:=TopAxis;
    3: TheAxis:=BottomAxis;
    4: TheAxis:=DepthAxis;
  else TheAxis:=Axes[LBAxes.ItemIndex];
  end;
  CreatingForm:=True;
  PageAxisChange(Self);
end;

procedure TFormTeeAxis.BAddClick(Sender: TObject);
begin
  TheAxis:=TChartAxis.Create(TheAxis.ParentChart.CustomAxes);
  TeeAddAxes(TheAxis.ParentChart,LBAxes.Items,True);
  SetAxisIndex;
  PageAxisChange(Self);
end;

procedure TFormTeeAxis.CBOtherSideClick(Sender: TObject);
begin
  TheAxis.OtherSide:=CBOtherSide.Checked;
end;

procedure TFormTeeAxis.BDeleteClick(Sender: TObject);
var tmp : Integer;
begin
  TheAxis.Free;
  With LBAxes do
  begin
    tmp:=ItemIndex;
    Items.Delete(tmp);
    ItemIndex:=tmp-1;
  end;
  LBAxesClick(Self);
end;

procedure TFormTeeAxis.CBHorizAxisClick(Sender: TObject);
begin
  TheAxis.Horizontal:=CBHorizAxis.Checked;
end;

procedure TFormTeeAxis.ELogBaseChange(Sender: TObject);
begin
  if not CreatingForm then TheAxis.LogarithmicBase:=UDLogBase.Position;
end;

procedure TFormTeeAxis.CBTitleVisibleClick(Sender: TObject);
begin
  TheAxis.Title.Visible:=CBTitleVisible.Checked
end;

procedure TFormTeeAxis.CBFormatChange(Sender: TObject);
begin
  With TheAxis do
  if IsDateTime then DateTimeFormat:=CBFormat.Text
                else AxisValuesFormat:=LocalToDelphiFormat(CBFormat.Text);
end;

procedure TFormTeeAxis.CBExpoClick(Sender: TObject);
begin
  TheAxis.LabelsExponent:=CBExpo.Checked;
end;

procedure TFormTeeAxis.CBAxisBehClick(Sender: TObject);
begin
  TheAxis.ParentChart.AxisBehind:=CBAxisBeh.Checked;
end;

procedure TFormTeeAxis.CBLabelAlignClick(Sender: TObject);
begin
  if CBLabelAlign.Checked then
     TheAxis.LabelsAlign:=alDefault
  else
     TheAxis.LabelsAlign:=alOpposite;
end;

end.
