{******************************************************************************+
  Unit WPrinter
--------------------------------------------------------------------------------
  Author: Martin Forst
--------------------------------------------------------------------------------
  Unit to handle printing. Works like Delphi's printers-unit but has an object
  for each printer.Settings made for a printer are save even if the printer
  is changed. Supplies some additional mehtods an properties like the
  pritning-area, page-size, printer-status, default-printer-flag ...
+******************************************************************************}
Unit WPrinter;

Interface

Uses WinTypes,WinProcs,Classes,Graphics;

{$H+}

Type TPaperFormat       = Class(TPersistent)
      Private
       FHeight          : Integer;
       FName            : String;
       FWindowsID       : Integer;
       FWidth           : Integer;
       function    GetPageExtent:TRect;
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Property    Extent:TRect read GetPageExtent;
       Property    Height:Integer read FHeight write FHeight;
       Property    Name:String read FName write FName;
       Property    WindowsID:Integer read FWindowsID write FWindowsID;
       Property    Width:Integer read FWidth write FWidth;
     end;

     TWPrinterState        = (wpsNoHandle, wpsHandleIC, wpsHandleDC);
     TWPrinterOrientation  = (wpoPortrait, wpoLandscape);
     TWPrinterCapability   = (wpcCopies, wpcOrientation, wpcCollation);
     TWPrinterCapabilities = set of TWPrinterCapability;
     
     TPaperBin          = Class
      Private
       FWindowsID       : Integer;
       FName            : String;
      Public
       Property    Name:String read FName write FName;
       Property    WindowsID:Integer read FWindowsID write FWindowsID;
     end;

     TWPrinter      = Class(TObject)
     Private
       FAborted         : Boolean;
       FCanvas          : TCanvas;
       FCapabilities    : TWPrinterCapabilities;
       FFonts           : TStrings;
       FPageNumber      : Integer;
       FTitle           : String;
       FPrinting        : Boolean;
       State            : TWPrinterState;
       DC               : HDC;
       {$IFDEF WIN32}
       DevMode          : PDeviceMode;
       {$ELSE}
       DevMode          : PDevMode;
       FExtDeviceMode   : TExtDeviceMode;
       {$ENDIF}
       DeviceMode       : THandle;
       FPrinterHandle   : THandle;
       FPrintToFile     : Boolean;
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
       FFileName        : String;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
       FName            : String;
       FDriver          : String;
       FPort            : String;
       FLocation        : String;
       FComments        : String;
       FPaperFormats    : TStringList;
       FPaperBins       : TStringList;
       Procedure   CheckPrinting(Value: Boolean);
       Procedure   CreatePrinterHandle;
       Procedure   FreePaperBins;
       Procedure   FreePaperFormats;
       Procedure   FreeFonts;
       Function    GetCanvas:TCanvas;
       Function    GetCollate:Boolean;
       Function    GetFonts:TStrings;
       Function    GetHandle:HDC;
       Function    GetIsDefault:Boolean;
       Function    GetNumCopies:Integer;
       Function    GetOrientation:TWPrinterOrientation;
       Function    GetPaperBin(AIndex:Integer):TPaperBin;
       Function    GetPaperBinCount:Integer;
       Function    GetPaperBins:TStringList;
       Function    GetPaperFormat(AIndex:Integer):TPaperFormat;
       Function    GetPaperFormatCount:Integer;
       Function    GetPaperFormatID:Integer;
       Function    GetPaperFormatIndex:Integer;
       Function    GetPaperFormats:TStringList;
       Function    GetPageHeight:Integer;
       Function    GetPageWidth: Integer;
       Function    GetPixelsPerInch:TPoint;
       Function    GetPrinterStatus:LongInt;
       Function    GetStatusText:String;
       Function    GetWaitingDocuments:Integer;
       Procedure   SetNumCopies(Value: Integer);
       Procedure   SetOrientation(Value: TWPrinterOrientation);
       Procedure   SetPaperFormatID(AId:Integer);
       Procedure   SetPaperFormatIndex(AIndex:Integer);
       Procedure   SetPrinterCapabilities(Value: Integer);
       Procedure   SetPrintToFile(APrintToFile:Boolean);
       Procedure   SetCollate(ACollate:Boolean);
       Procedure   SetState(Value: TWPrinterState);
     Public
       Constructor Create(const AName,ATyp,APort,ALocation,AComment:String);
       Destructor  Destroy; override;
       Property    Aborted: Boolean read FAborted;
       Procedure   Abort;
//++ Glukhov PrinterOptimization Build#175 25.01.02
        // Returns True when DeviceMode<>0
        Function   GotDocProperties : Boolean;
//-- Glukhov PrinterOptimization Build#175 25.01.02
       Function    AdvancedProperties(AParent:THandle):Boolean;
       Procedure   BeginDoc;
       Property    Canvas: TCanvas read GetCanvas;
       Property    Capabilities:TWPrinterCapabilities read FCapabilities;
       Property    Collate:Boolean read GetCollate write SetCollate;
       Property    Comments:String read FComments write FComments;
       Property    Copies: Integer read GetNumCopies write SetNumCopies;
       Procedure   EndDoc;
       Property    Fonts: TStrings read GetFonts;
       Property    Handle: HDC read GetHandle;
       Property    IsDefaultPrinter:Boolean read GetIsDefault;
       Property    Location:String read FLocation;
       Property    Name:String read FName;
       Procedure   NewPage;
       Property    Printing: Boolean read FPrinting;
       // paper-bin support
       Property    PaperBinCount:Integer read GetPaperBinCount;
       Property    PaperBins[AIndex:Integer]:TPaperBin read GetPaperBin;
       Property    PaperBinNames:TStringList read GetPaperBins;
       // paper-format support
       Function    IndexOfPaperFormat(AFormat:TPaperFormat):Integer; overload;
       Function    IndexOfPaperFormat(AFormat:Integer):Integer; overload;
       Property    Orientation: TWPrinterOrientation read GetOrientation write SetOrientation;
       Property    PixelsPerInch:TPoint read GetPixelsPerInch;
       Property    PaperFormatCount:Integer read GetPaperFormatCount;
       Property    PaperFormat:Integer read GetPaperFormatID write SetPaperFormatID;
       Property    PaperFormats[AIndex:Integer]:TPaperFormat read GetPaperFormat;
       Property    PaperFormatIndex:Integer read GetPaperFormatIndex write SetPaperFormatIndex;
       Property    PaperFormatNames:TStringList read GetPaperFormats;
       // page-sizes and printable area given in (1/10 mm)
       Property    PageHeight: Integer read GetPageHeight;
       Property    PageNumber: Integer read FPageNumber;
       Function    PageSize:TRect;
       Property    PageWidth: Integer read GetPageWidth;
       Function    PrintableArea:TRect;
       // print to file or to port?
       Property    PrintToFile:Boolean read FPrintToFile write SetPrintToFile;
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
       Property    FileName:String read FFileName write FFileName;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
       // function to display setup-dialog
       Function    Properties(AParent:THandle):Boolean;
       // port, actual status, printer-type and number of waiting documents
       Property    Port:String read FPort;
       Property    Status:LongInt read GetPrinterStatus;
       Property    StatusText:String read GetStatusText;
       Property    Title: String read FTitle write FTitle;
       Property    Typ:String read FDriver;
       Property    WaitingDocuments:Integer read GetWaitingDocuments;
     end;

     TWPrinters    = Class
      Private
//++ Glukhov PrinterOptimization Build#175 25.01.02
        State       : Integer;  // 0 - initial; >0 - initialized
//-- Glukhov PrinterOptimization Build#175 25.01.02
        FPrinters   : TStringList;
        FPrinter    : TWPrinter;
        Procedure   CreatePrinterList;
//++ Glukhov PrinterOptimization Build#175 25.01.02
        Procedure   InitPrinters;
//-- Glukhov PrinterOptimization Build#175 25.01.02
        Function    GetDefaultPrinter:TWPrinter;
        Function    GetPrinter(AIndex:Integer):TWPrinter;
        Function    GetPrinters:TStrings;
      Public
        Constructor Create;
        Destructor  Destroy; override;
        Property    DefaultPrinter:TWPrinter read GetDefaultPrinter;
        Property    Printer:TWPrinter read FPrinter write FPrinter;
        Property    PrinterObjects[AIndex:Integer]:TWPrinter read GetPrinter; default;
        Property    Printers:TStrings read GetPrinters;
        Procedure   Update;
     end;

var WPrinters      : TWPrinters;

Implementation

Uses Consts,Forms,MultiLng,SysUtils,WinSpool;

Procedure RaiseError(const AError:String);
begin
end;

Function FetchStr(var Str:PChar):PChar;
var P              : PChar;
begin
  Result:=Str;
  if Str=nil then Exit;
  P:=Str;
  while P^=' ' do Inc(P);
  Result:=P;
  while (P^<>#0) and (P^<>',') do Inc(P);
  if P^=',' then begin
    P^:=#0;
    Inc(P);
  end;
  Str:=P;
end;

{$IFDEF WIN32}
function AbortProc(Prn: HDC; Error: Integer): Bool; stdcall;
{$ELSE}
function AbortProc(Prn: HDC; Error: Integer): Bool; export;
{$ENDIF}
var Cnt            : Integer;
begin
  Application.ProcessMessages;
  for Cnt:=0 to WPrinters.FPrinters.Count-1 do if WPrinters.PrinterObjects[Cnt].
      DC=Prn then begin
    Result:=not WPrinters.PrinterObjects[Cnt].Aborted;
    Exit;
  end;
  Result:=FALSE;
end;

Type TPrinterCanvas = class(TCanvas)
       Printer: TWPrinter;
       constructor Create(APrinter: TWPrinter);
       procedure CreateHandle; override;
       procedure Changing; override;
       procedure UpdateFont;
     end;

constructor TPrinterCanvas.Create(APrinter: TWPrinter);
begin
  inherited Create;
  Printer:=APrinter;
end;

procedure TPrinterCanvas.CreateHandle;
begin
  Printer.SetState(wpsHandleIC);
  UpdateFont;
  Handle:= Printer.DC;
end;

procedure TPrinterCanvas.Changing;
begin
  Printer.CheckPrinting(True);
  inherited Changing;
  UpdateFont;
end;

procedure TPrinterCanvas.UpdateFont;
var
  FontSize: Integer;
begin
  FontSize := Font.Size;
  Font.PixelsPerInch := GetDeviceCaps(Printer.DC, LOGPIXELSY);
  Font.Size := FontSize;
end;

{===============================================================================
  TWPrinter
+==============================================================================}

Constructor TWPrinter.Create(const AName,ATyp,APort,ALocation,AComment:String);
begin
  inherited Create;
  FName:=AName;
  FDriver:=ATyp;
  FPort:=APort;
  FLocation:=ALocation;
  FComments:=AComment;
end;

Destructor TWPrinter.Destroy;
begin
  if Printing then EndDoc;
  SetState(wpsNoHandle);
  FreeFonts;
  FreePaperBins;
  FreePaperFormats;
  FCanvas.Free;
  if DeviceMode<>0 then begin
    GlobalUnlock(DeviceMode);
    GlobalFree(DeviceMode);
  end;  
  {$IFDEF WIN32}
  if FPrinterHandle<>0 then ClosePrinter(FPrinterHandle);
  {$ELSE}
  if FPrinterHandle<>0 then FreeLibrary(FPrinterHandle);
  {$ENDIF}
  inherited Destroy;
end;

Procedure TWPrinter.SetState(Value: TWPrinterState);
{$IFDEF WIN32}
type
  TCreateHandleFunc =
      Function (DriverName, DeviceName, Output: PChar; InitData: PDeviceMode): HDC stdcall;
{$ELSE}
type
  TCreateHandleFunc =
      Function (DriverName, DeviceName, Output: PChar; InitData: Pointer): HDC;
{$ENDIF}
var CreateHandleFunc    : TCreateHandleFunc;
begin
    if Value<>State then begin
      CreateHandleFunc:=NIL;
      case Value of
      wpsNoHandle:
        begin
          CheckPrinting(FALSE);
          if Assigned(FCanvas) then FCanvas.Handle:=0;
        end;
      wpsHandleIC:
        if State<>wpsHandleDC
        then  CreateHandleFunc:=CreateIC
        else  Exit;
      wpsHandleDC:
        begin
          if FCanvas<>NIL then FCanvas.Handle:=0;
          CreateHandleFunc:=CreateDC;
        end;
      end;

      if DC<>0 then begin
        DeleteDC(DC);
        DC:=0;
      end;
      if Assigned(CreateHandleFunc) then begin
        if FPrintToFile
        then  DC:=CreateHandleFunc(PChar(FDriver),PChar(FName),'FILE:',DevMode)
        else  DC:=CreateHandleFunc(PChar(FDriver),PChar(Fname),PChar(FPort),DevMode);
        if DC=0 then RaiseError(SInvalidPrinter);
        if FCanvas<>NIL then FCanvas.Handle:=DC;
      end;
      State:=Value;
    end;
end;

Procedure TWPrinter.CheckPrinting(Value:Boolean);
begin
  if Printing<>Value then
    if Value
    then  RaiseError(SNotPrinting)
    else  RaiseError(SPrinting);
end;

Procedure TWPrinter.Abort;
begin
  CheckPrinting(TRUE);
  AbortDoc(Canvas.Handle);
  FAborted:=TRUE;
  EndDoc;
end;

Procedure TWPrinter.BeginDoc;
var CTitle         : Array[0..255] of Char;
    DocInfo        : TDocInfo;
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
    CFileName      : Array[0..255] of Char;
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
begin
   CheckPrinting(FALSE);
   SetState(wpsHandleDC);
   Canvas.Refresh;
   FPrinting:=TRUE;
   FAborted:=FALSE;
   FPageNumber:=1;
   StrPLCopy(CTitle,Title,SizeOf(CTitle)-1);
   FillChar(DocInfo,SizeOf(DocInfo),0);
   with DocInfo do
      begin
         cbSize:=SizeOf(DocInfo);
         lpszDocName:=CTitle;
{++ Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
         if FPrintToFile and (FFileName <> '') then
            begin
               StrPLCopy(CFileName,FFileName,SizeOf(CFileName)-1);
               lpszOutput:=CFileName;
            end
         else
{-- Moskaliov AXWinGIS Printing Methods BUILD#133 25.09.00}
            lpszOutput:=NIL;
      end;
   SetAbortProc(DC,AbortProc);
   StartDoc(DC,DocInfo);
   StartPage(DC);
end;

Procedure TWPrinter.EndDoc;
begin
   CheckPrinting(TRUE);
   EndPage(DC);
   if not Aborted then
      WinProcs.EndDoc(DC);
   FPrinting:=FALSE;
   FAborted:=FALSE;
   FPageNumber:=0;
end;

Procedure TWPrinter.NewPage;
begin
   CheckPrinting(TRUE);
   EndPage(DC);
   StartPage(DC);
   Inc(FPageNumber);
   Canvas.Refresh;
end;

Procedure TWPrinter.SetPrinterCapabilities(Value: Integer);
begin
  FCapabilities:=[];
  if Value and DM_ORIENTATION<>0 then Include(FCapabilities,wpcOrientation);
  if Value and DM_COPIES<>0 then Include(FCapabilities,wpcCopies);
  {$IFDEF WIN32}
  if Value and DM_COLLATE<>0 then Include(FCapabilities,wpcCollation);
  {$ENDIF}
end;

//++ Glukhov PrinterOptimization Build#175 25.01.02
// Returns True when DeviceMode<>0
Function TWPrinter.GotDocProperties : Boolean;
begin
  Result:= (DeviceMode<>0);
end;
//-- Glukhov PrinterOptimization Build#175 25.01.02

Procedure TWPrinter.CreatePrinterHandle;
var {$IFDEF WIN32}
    StubDevMode    : TDeviceMode;
    {$ELSE}
    StubDevMode    : TDevMode;
    P              : PDevMode;
    DriverName     : array[0..255] of Char;
    {$ENDIF}
begin
  CheckPrinting(FALSE);
  {$IFDEF WIN32}                    
  if OpenPrinter(PChar(FName),FPrinterHandle,NIL) then begin
    if DeviceMode=0 then begin
      DeviceMode:=GlobalAlloc( GHND,
          DocumentProperties(0,FPrinterHandle,PChar(Fname), StubDevMode,StubDevMode,0) );
      if DeviceMode<>0 then begin
        DevMode:= GlobalLock(DeviceMode);
        if DocumentProperties(0,FPrinterHandle,PChar(Fname),
              DevMode^,DevMode^, DM_OUT_BUFFER)<0
        then begin
          GlobalUnlock(DeviceMode);
          GlobalFree(DeviceMode);
          DeviceMode:=0;
        end;
      end;
    end;
    if DeviceMode<>0 then SetPrinterCapabilities(DevMode^.dmFields);
  end;
  {$ELSE}
  StrCat(StrCopy(DriverName, ADriver), '.DRV');
  FPrinterHandle:=LoadLibrary(DriverName);
  if FPrinterHandle <= 16 then FPrinterHandle:=0;
  if FPrinterHandle <> 0 then
  begin
    @FExtDeviceMode:=GetProcAddress(FPrinterHandle, 'ExtDeviceMode');
    if Assigned(FExtDeviceMode) and (DeviceMode = 0) then
    begin
      DeviceMode:=GlobalAlloc(HeapAllocFlags or GMEM_ZEROINIT,
        FExtDeviceMode(0, FPrinterHandle, StubDevMode, ADevice, APort,
        StubDevMode, NIL, 0));
      if DeviceMode <> 0 then
      begin
        DevMode:=GlobalLock(DeviceMode);
        if FExtDeviceMode(0, FPrinterHandle, DevMode^, ADevice, APort, DevMode^, NIL,
          DM_OUT_BUFFER) < 0 then
        begin
          GlobalUnlock(DeviceMode);
          GlobalFree(DeviceMode);
          DeviceMode:=0;
        end
        else SetPrinterCapabilities(DevMode^.dmFields);
      end;
    end;
  end;
  {$ENDIF}
end;

Function TWPrinter.GetCanvas:TCanvas;
begin
  if FCanvas=NIL then FCanvas:=TPrinterCanvas.Create(Self);
  Result:=FCanvas;
end;

Function EnumFontsProc(var LogFont:TLogFont;var TextMetric:TTextMetric;
  FontType:Integer;Data:Pointer):Integer;
  {$IFDEF WIN32}
  stdcall;
  {$ELSE}
  export;
  {$ENDIF}
begin
  TStrings(Data).Add(LogFont.lfFaceName);
  Result:=1;
end;

Function TWPrinter.GetFonts:TStrings;
begin
  if FFonts=NIL then
  try
    SetState(wpsHandleIC);
    FFonts:=TStringList.Create;
    EnumFonts(DC,NIL,@EnumFontsProc,Pointer(FFonts));
  except
    FFonts.Free;
    FFonts:=NIL;
    Raise;
  end;
  Result:=FFonts;
end;

Function TWPrinter.GetHandle:HDC;
begin
  SetState(wpsHandleIC);
  Result:=DC;
end;

Function TWPrinter.GetNumCopies:Integer;
begin
  if DeviceMode=0 then RaiseError(SInvalidPrinterOp);
  Result:=DevMode^.dmCopies;
end;                      

Procedure TWPrinter.SetNumCopies(Value:Integer);
begin
  CheckPrinting(FALSE);
  if DeviceMode=0 then RaiseError(SInvalidPrinterOp);
  SetState(wpsNoHandle);
  DevMode^.dmCopies:=Value;
end;

Function TWPrinter.GetOrientation:TWPrinterOrientation;
begin
  if DeviceMode = 0 then RaiseError(SInvalidPrinterOp);
  if DevMode^.dmOrientation = DMORIENT_PORTRAIT then Result:=wpoPortrait
  else Result:=wpoLandscape;
end;

Procedure TWPrinter.SetOrientation(Value:TWPrinterOrientation);
const Orientations : array [TWPrinterOrientation] of Integer = (
    DMORIENT_PORTRAIT, DMORIENT_LANDSCAPE);
begin
  CheckPrinting(FALSE);
  if DeviceMode=0 then RaiseError(SInvalidPrinterOp);
  SetState(wpsNoHandle);
  DevMode^.dmOrientation:=Orientations[Value];
end;

Function TWPrinter.GetPageHeight:Integer;
begin
  SetState(wpsHandleIC);
  Result:=GetDeviceCaps(DC,VertRes);
end;

Function TWPrinter.GetPageWidth:Integer;
begin
  SetState(wpsHandleIC);
  Result:=GetDeviceCaps(DC,HorzRes);
end;

Procedure TWPrinter.FreeFonts;
begin
  FFonts.Free;
  FFonts:=NIL;
end;

{******************************************************************************+
  Function TWPrinter.Properties
--------------------------------------------------------------------------------
  Opens the printer-setup-dialog.
+******************************************************************************}
//++ Glukhov PrinterOptimization Build#175 25.01.02
Function TWPrinter.Properties(AParent:THandle):Boolean;
var RetValue     : Integer;
begin
{$IFDEF WIN32}
  CheckPrinting(FALSE);
  CreatePrinterHandle;
//  if DeviceMode=0 then RaiseError(SInvalidPrinterOp);
  if DeviceMode=0
  then begin
  // YG: I set result False when DeviceMode=0
    RaiseError(SInvalidPrinterOp);
    Result:= False;
  end
  else begin
    RetValue:=DocumentProperties(
      AParent,FPrinterHandle,PChar(SELF.FName),DevMode^,DevMode^,
      DM_IN_PROMPT or DM_IN_BUFFER or DM_OUT_BUFFER);
    if RetValue<0 then begin
      if DeviceMode<>0 then begin
        GlobalUnlock(DeviceMode);
        GlobalFree(DeviceMode);
        DeviceMode:=0;
      end;
    end
    else  SetState(wpsNoHandle);
    Result:= (RetValue=id_OK);
  end;
{$ELSE}
  CheckPrinting(FALSE);
//  if DeviceMode=0 then RaiseError(SInvalidPrinterOp);
  if DeviceMode=0 then begin
  // YG: I set result False when DeviceMode=0
    RaiseError(SInvalidPrinterOp);
    Result:= False;
  end
  else begin
    with TWPrinterDevice(FPrinters.Objects[FPrinterIndex])
    do FExtDeviceMode(
        AParent,FPrinterHandle,DevMode^,Device,Port,DevMode^,
        NIL,DM_IN_PROMPT or DM_IN_BUFFER or DM_OUT_BUFFER);
  // YG: I set result True to set something
    Result:= True;
  end;
{$ENDIF}
end;
//-- Glukhov PrinterOptimization Build#175 25.01.02

{******************************************************************************+
  Function TWPrinter.AdvancedProperties
--------------------------------------------------------------------------------
  Opens the advanced-printer-setup-dialog.
+******************************************************************************}
Function TWPrinter.AdvancedProperties(AParent:THandle):Boolean;
var RetValue     : Integer;
begin
{$IFDEF WIN32}
   CheckPrinting(FALSE);
   if DeviceMode=0 then
      RaiseError(SInvalidPrinterOp);
   RetValue:=AdvancedDocumentProperties(AParent,FPrinterHandle,NIL,DevMode,DevMode);
   if RetValue<0 then
      begin
         GlobalUnlock(DeviceMode);
         GlobalFree(DeviceMode);
         DeviceMode:=0;
      end
   else
      SetState(wpsNoHandle);
   Result:=RetValue=id_OK;
{$ELSE}
   CheckPrinting(FALSE);
   if DeviceMode=0 then
      RaiseError(SInvalidPrinterOp);
   with TWPrinterDevice(FPrinters.Objects[FPrinterIndex]) do
      FExtDeviceMode(AParent,FPrinterHandle,DevMode^,Device,Port,DevMode^,
                     NIL,DM_IN_PROMPT or DM_IN_BUFFER or DM_OUT_BUFFER);
{$ENDIF}
end;

{*******************************************************************************
| Function TWPrinter.GetPrinterStatus
|-------------------------------------------------------------------------------
| Gets the status of the currently selected printer.
+******************************************************************************}
Function TWPrinter.GetPrinterStatus: LongInt;
{$IFDEF WIN32}
var PrinterInfo  : PPrinterInfo2;
    Size         : LongInt;
    SizeCopied   : LongInt;
begin
   Result:=PRINTER_STATUS_ERROR;
   WinSpool.GetPrinter(FPrinterHandle,2,NIL,0,@Size);
   GetMem(PrinterInfo,Size);
   try
      if WinSpool.GetPrinter(FPrinterHandle,2,PrinterInfo,Size,@SizeCopied) then
         Result:=PrinterInfo^.Status;
   finally
      FreeMem(PrinterInfo,Size);
   end;
end;
{$ELSE}
begin
   Result:=0;
end;
  {$ENDIF}

{*******************************************************************************
| Function TWPrinter.GetWaitingDocuments
|-------------------------------------------------------------------------------
| Liefert die Anzahl der im Druckerspooler wartenden Dokumente.
+******************************************************************************}
Function TWPrinter.GetWaitingDocuments: Integer;
{$IFDEF WIN32}
var PrinterInfo  : PPrinterInfo2;
    Size         : LongInt;
    SizeCopied   : LongInt;
begin
   Result:=0;
   WinSpool.GetPrinter(FPrinterHandle,2,NIL,0,@Size);
   GetMem(PrinterInfo,Size);
   try
      if WinSpool.GetPrinter(FPrinterHandle,2,PrinterInfo,Size,@SizeCopied) then
         Result:=PrinterInfo^.cJobs;
   finally
      FreeMem(PrinterInfo,Size);
   end;
end;
{$ELSE}
begin
   Result:=0;
end;
{$ENDIF}

{*******************************************************************************
| Function TWPrinter.PageSize
|-------------------------------------------------------------------------------
| Liefert die Gr��e der Seite in virtuellen Einheiten.
+******************************************************************************}
Function TWPrinter.PageSize: TRect;
{$IFNDEF WIN32}
var Point        : TPoint;
{$ENDIF}
begin
{$IFDEF WIN32}
   Result:=Rect(0,0,GetDeviceCaps(Handle,PHYSICALWIDTH),GetDeviceCaps(Handle,PHYSICALHEIGHT));
{$ELSE}
   Escape(Handle,GETPHYSPAGESIZE,0,NIL,@Point);
   Result:=Rect(0,0,Point.X,Point.Y);
{$ENDIF}
   DPToLP(Handle,Result,2);
end;

{******************************************************************************+
 Function TWPrinter.PrintableArea
--------------------------------------------------------------------------------
  Liefert die Gr��e des druckbaren Bereichs in virtuellen Einheiten.
+******************************************************************************}
Function TWPrinter.PrintableArea: TRect;
{$IFNDEF WIN32}
var Point        : TPoint;
{$ENDIF}
begin
   Result:=Rect(0,0,GetDeviceCaps(Handle,HORZRES),GetDeviceCaps(Handle,VERTRES));
{$IFDEF WIN32}
   OffsetRect(Result,GetDeviceCaps(Handle,PHYSICALOFFSETX),
              GetDeviceCaps(Handle,PHYSICALOFFSETY));
{$ELSE}
   Escape(Handle,GETPRINTINGOFFSET,0,NIL,@Point);
   OffsetRect(Result,Point.X,Point.Y);
{$ENDIF}
   DPToLP(Handle,Result,2);
end;

Function TWPrinter.GetIsDefault:Boolean;
begin
   Result:=AnsiCompareText(FName,WPrinters.DefaultPrinter.Name)=0;
end;

Procedure TWPrinter.SetPrintToFile(APrintToFile:Boolean);
begin
   if FPrintToFile<>APrintToFile then
      begin
         CheckPrinting(FALSE);
         FPrintToFile:=APrintToFile;
         SetState(wpsNoHandle);
      end;
end;

Function TWPrinter.GetCollate:Boolean;
begin
   if DeviceMode=0 then
      RaiseError(SInvalidPrinterOp);
   Result:=DevMode^.dmCollate=DMCOLLATE_TRUE;
end;

Procedure TWPrinter.SetCollate(ACollate:Boolean);
begin
   if DeviceMode=0 then
      RaiseError(SInvalidPrinterOp);
   if ACollate then
      DevMode^.dmCollate:=DMCOLLATE_TRUE
   else
      DevMode^.dmCollate:=DMCOLLATE_FALSE;
end;

{===============================================================================
  TWPrinters
+==============================================================================}

Constructor TWPrinters.Create;
begin
  inherited Create;
//++ Glukhov PrinterOptimization Build#175 25.01.02
//  CreatePrinterList;
//  FPrinter:=DefaultPrinter;
  State:= 0;
//-- Glukhov PrinterOptimization Build#175 25.01.02
end;

Destructor TWPrinters.Destroy;
var Cnt            : Integer;
begin
//++ Glukhov PrinterOptimization Build#175 25.01.02
//  for Cnt:=0 to FPrinters.Count-1 do TWPrinter(FPrinters.Objects[Cnt]).Free;
//  FPrinters.Free;
  if State > 0 then begin
    for Cnt:=0 to FPrinters.Count-1
    do  TWPrinter(FPrinters.Objects[Cnt]).Free;
    FPrinters.Free;
  end;
//-- Glukhov PrinterOptimization Build#175 25.01.02
  inherited Destroy;
end;

//++ Glukhov PrinterOptimization Build#175 25.01.02
Procedure TWPrinters.InitPrinters;
begin
  CreatePrinterList;
  State:= 1;
  FPrinter:= DefaultPrinter;
end;
//-- Glukhov PrinterOptimization Build#175 25.01.02

Function TWPrinters.GetPrinter(AIndex:Integer):TWPrinter;
begin
//++ Glukhov PrinterOptimization Build#175 25.01.02
  if State = 0 then InitPrinters;
//-- Glukhov PrinterOptimization Build#175 25.01.02
  if AIndex<0 then Result:=DefaultPrinter
  else Result:=TWPrinter(FPrinters.Objects[AIndex]);
  if (Result<>NIL) and (Result.FPrinterHandle=0) then Result.CreatePrinterHandle;
end;

Function TWPrinters.GetPrinters:TStrings;
begin
//++ Glukhov PrinterOptimization Build#175 25.01.02
  if State = 0 then InitPrinters;
//-- Glukhov PrinterOptimization Build#175 25.01.02
  Result:=FPrinters;
end;

{$IFDEF WIN32}
Procedure TWPrinters.CreatePrinterList;
var Buffer         : PChar;
    PrinterInfo    : PChar;
    I              : Integer;
    Count          : LongWord;
    NumInfo        : LongWord;
    Flags          : Integer;
    Level          : Byte;
    NewPrinter     : TWPrinter;
begin
  if FPrinters=NIL then begin
    FPrinters:=TStringList.Create;
    try
      if Win32Platform<>VER_PLATFORM_WIN32_NT then Flags:=PRINTER_ENUM_LOCAL
      else Flags:=PRINTER_ENUM_CONNECTIONS or PRINTER_ENUM_LOCAL;
      Level:=2;
      EnumPrinters(Flags,NIL,Level,NIL,0,Count,NumInfo);
      GetMem(Buffer,Count);
      try
        if not EnumPrinters(Flags,NIL,Level,PByte(Buffer),Count,Count,NumInfo)
        then  Exit;
        PrinterInfo:=Buffer;
        for I:=0 to NumInfo-1 do begin
          with PPrinterInfo2(PrinterInfo)^ do
          if StrIComp(pPortName,'PUB:')<>0 then begin
            NewPrinter:=TWPrinter.Create(pPrinterName,pDriverName,pPortName, pLocation,pComment);
            FPrinters.AddObject(pPrinterName,NewPrinter);
          end;
          Inc(PrinterInfo,sizeof(TPrinterInfo2));
        end;
      finally
        FreeMem(Buffer, Count);
      end;
    except
      FPrinters.Free;
      FPrinters:=NIL;
      Raise;
    end;
  end;
end;
{$ELSE}
Function TWPrinters.GetPrinters:TStrings;
const
  DevicesSize = 4096;
var
  Devices: PChar;
  LineCur: PChar;
  DriverLine: array[0..79] of Char;
  DriverStr: array[0..79] of Char;
  Device, Driver, Port: PChar;
begin
//++ Glukhov PrinterOptimization Build#175 25.01.02
  if State = 0 then InitPrinters;
//-- Glukhov PrinterOptimization Build#175 25.01.02
  if FPrinters = NIL then
  begin
    FPrinters:=TStringList.Create;
    try
      GetMem(Devices, DevicesSize);
      try
        { Get a list of devices from WIN.INI.  Stored in the form of
          <device 1>#0<device 2>#0...<driver n>#0#0 }
        GetProfileString('devices', NIL, '', Devices, DevicesSize);
        Device:=Devices;
        while Device^ <> #0 do
        begin
          GetProfileString('devices', Device, '', DriverLine, SizeOf(DriverLine));
          { Get driver portion of DeviceLine }
          LineCur:=DriverLine;
          Driver:=FetchStr(LineCur);
          { Copy the port information from the line }
          { This code is complicated because the device line is of the form:
                <device name> = <driver name> , <port> [ , <port> ]
              where port (in []) can be repeated. }
          Port:=FetchStr(LineCur);
          while Port^ <> #0 do
          begin
            FPrinters.AddObject(StrPas(Device),
              TWPrinterDevice.Create(Driver, Device, Port, NIL));
            Port:=FetchStr(LineCur);
          end;
          Inc(Device, StrLen(Device) + 1);
        end;
      finally
        FreeMem(Devices, DevicesSize);
      end;
    except
      FPrinters.Free;
      FPrinters:=NIL;
      raise;
    end;
  end;
  Result:=FPrinters;
end;
{$ENDIF}

{******************************************************************************+
  Procedure TWPrinters.Update
--------------------------------------------------------------------------------
  Updates the printer-list.
+******************************************************************************}
Procedure TWPrinters.Update;
begin
//++ Glukhov PrinterOptimization Build#175 25.01.02
  if State = 0 then InitPrinters;
//-- Glukhov PrinterOptimization Build#175 25.01.02
end;

Function TWPrinters.GetDefaultPrinter:TWPrinter;
var Cnt            : Integer;
    Buffer         : Array[0..255] of Char;
    Cur            : PChar;
    Device         : PChar;
    ByteCnt        : DWORD;
    StructCnt      : DWORD;
    PrinterInfo    : PPrinterInfo5;
begin
//++ Glukhov PrinterOptimization Build#175 25.01.02
  if State = 0 then InitPrinters;
//-- Glukhov PrinterOptimization Build#175 25.01.02
  ByteCnt:=0;
  StructCnt:=0;
  // first try EnumPrinters, is supported by Windows 95 only
{++ Ivanoff BUG#423 BUILD#95}
{Flag PRINTER_ENUM_DEFAULT was replaced with PRINTER_ENUM_LOCAL.
Call of EnumPrinters with PRINTER_ENUM_DEFAULT will cause false result return value
of GetLastError equal to 123 - "The filename, directory name, or volume label
syntax is incorrect."
}
  if not EnumPrinters(PRINTER_ENUM_LOCAL,NIL,5,NIL,0,ByteCnt,
    StructCnt) and (GetLastError<>ERROR_INSUFFICIENT_BUFFER) then
    RaiseLastWin32Error;
{-- Ivanoff }

  PrinterInfo:=AllocMem(ByteCnt);
  try
    EnumPrinters(PRINTER_ENUM_DEFAULT,NIL,5,PrinterInfo,ByteCnt,ByteCnt,StructCnt);
    if StructCnt>0 then Device:=PrinterInfo.pPrinterName
    else begin
      // EnumPrinters not successfull, read info from the registry
      GetProfileString('windows','device','',Buffer,SizeOf(Buffer)-1);
      Cur:=Buffer;
      Device:=FetchStr(Cur);
    end;
    // search for the printer in the list
    for Cnt:=0 to FPrinters.Count-1 do
      if AnsiCompareText(TWPrinter(FPrinters.Objects[Cnt]).Name,Device)=0
      then begin
        Result:=TWPrinter(FPrinters.Objects[Cnt]);
        if Result.FPrinterHandle=0 then Result.CreatePrinterHandle;
        Exit;
      end;
    // no default-printer found, return first printer or NIL if no printers installed
    if FPrinters.Count>0 then begin
      Result:=TWPrinter(FPrinters.Objects[0]);
      if Result.FPrinterHandle=0 then Result.CreatePrinterHandle;
    end
    else Result:=NIL;
  finally
    FreeMem(PrinterInfo);
  end;
end;

Function TWPrinter.GetPaperFormat(AIndex:Integer):TPaperFormat;
begin
  Result:=TPaperFormat(PaperFormatNames.Objects[AIndex]);
end;

Function TWPrinter.GetPaperFormatCount:Integer;
begin
  Result:=PaperFormatNames.Count;
end;

Procedure TWPrinter.FreePaperFormats;
var Cnt            : Integer;
begin
  if FPaperFormats<>NIL then begin
    for Cnt:=0 to FPaperFormats.Count-1 do FPaperFormats.Objects[Cnt].Free;
    FPaperFormats.Free;
    FPaperFormats:=NIL;
  end;
end;

Function TWPrinter.GetPaperBin(AIndex:Integer):TPaperBin;
begin
  Result:=TPaperBin(PaperBinNames.Objects[AIndex]);
end;

Function TWPrinter.GetPaperBinCount:Integer;
begin
  Result:=PaperBinNames.Count;
end;

Procedure TWPrinter.FreePaperBins;
var Cnt            : Integer;
begin
  if FPaperBins<>NIL then begin
    for Cnt:=0 to FPaperBins.Count-1 do FPaperBins.Objects[Cnt].Free;
    FPaperBins.Free;
    FPaperBins:=NIL;
  end;
end;

Type TBinName      = Array[0..63] of char;

Function TWPrinter.GetPaperBins:TStringList;
var Count          : Integer;
    Cnt            : Integer;
    PaperBinNames  : Array of TBinName;
    PaperBinIDs    : Array of Word;
  Procedure NewPaperBin(const Name:String;WindowsID:Integer);
  var PaperBin     : TPaperBin;
  begin
    PaperBin:=TPaperBin.Create;
    PaperBin.Name:=Name;
    PaperBin.WindowsID:=WindowsID;
    FPaperBins.AddObject(Name,PaperBin);
  end;
begin
  if FPaperBins=NIL then
  try
    FPaperBins:=TStringList.Create;
    Count:=DeviceCapabilities(PChar(FName),PChar(FPort),DC_PAPERNAMES,NIL,NIL);
    SetLength(PaperBinNames,Count);      
    SetLength(PaperBinIDs,Count);
    DeviceCapabilities(PChar(FName),PChar(FPort),DC_BINNAMES,@PaperBinNames,NIL);
    DeviceCapabilities(PChar(FName),PChar(FPort),DC_BINS,@PaperBinIDs,NIL);
    for Cnt:=0 to Count-1 do NewPaperBin(PaperBinNames[Cnt],PaperBinIDs[Cnt]);
  except
    FPaperBins.Free;
    FPaperBins:=NIL;
    Raise;
  end;
  Result:=FPaperBins;
end;

Type TPaperName      = Array[0..63] of char;

Function TWPrinter.GetPaperFormats:TStringList;
  var
    Count          : Integer;
    Cnt            : Integer;
    PaperNames     : Array of TPaperName;
    PaperIDs       : Array of Word;
    PaperSizes     : Array of TPoint;

  Procedure NewPageSize(const Name:String;WindowsID:Integer;Width,Height:Integer);
  var PageSize     : TPaperFormat;
  begin
    PageSize:=TPaperFormat.Create;
    PageSize.Name:=Name;
    PageSize.WindowsID:=WindowsID;
    PageSize.Width:=Width+1;
    PageSize.Height:=Height+1;
    FPaperFormats.AddObject(Name,PageSize);
  end;

begin
  if FPaperFormats=NIL then
  try
    FPaperFormats:=TStringList.Create;
    Count:=DeviceCapabilities(PChar(FName),PChar(FPort),DC_PAPERNAMES,NIL,NIL);
    if Count <> -1 then begin
       SetLength(PaperNames,Count);
       SetLength(PaperSizes,Count);
       SetLength(PaperIDs,Count);
       DeviceCapabilities(PChar(FName),PChar(FPort),DC_PAPERNAMES,@PaperNames[0],NIL);
       DeviceCapabilities(PChar(FName),PChar(FPort),DC_PAPERSIZE,@PaperSizes[0],NIL);
       DeviceCapabilities(PChar(FName),PChar(FPort),DC_PAPERS,@PaperIDs[0],NIL);
       for Cnt:=0 to Count-1 do
         NewPageSize(PaperNames[Cnt], PaperIDs[Cnt], PaperSizes[Cnt].X, PaperSizes[Cnt].Y);
    end;
  except
    FPaperFormats.Free;
    FPaperFormats:=NIL;
    Raise;
  end;
  Result:=FPaperFormats;
end;

{ TPageSize }

function TPaperFormat.GetPageExtent:TRect;
begin
  Result:=Rect(0,0,Width,Height);
end;

Function TWPrinter.GetPaperFormatIndex:Integer;
begin
  Result:=0;
  while Result<PaperFormatCount do begin
    if PaperFormats[Result].WindowsID=DevMode^.dmPaperSize then break;
    Inc(Result);
  end;
  if Result>=PaperFormatCount then Result:=0;
end;

Procedure TWPrinter.SetPaperFormatIndex(AIndex:Integer);
var Format         : TPaperFormat;
begin
  Format:=PaperFormats[AIndex];
  DevMode^.dmPaperSize:=Format.WindowsID;
  DevMode^.dmPaperWidth:=Format.Width;
  DevMode^.dmPaperLength:=Format.Height;
end;

Function TWPrinter.GetPaperFormatID:Integer;
begin
  Result:=DevMode^.dmPaperSize;
end;
  
Procedure TWPrinter.SetPaperFormatID(AID:Integer);
begin
  DevMode^.dmPaperSize:=AID;
end;

Function TWPrinter.IndexOfPaperFormat(AFormat:TPaperFormat):Integer;
begin
  for Result:=0 to PaperFormatCount-1 do
      if PaperFormats[Result].WindowsID=AFormat.WindowsID then
         Exit;
  Result:=-1;
end;

Function TWPrinter.IndexOfPaperFormat(AFormat:Integer):Integer;
begin
  for Result:=0 to PaperFormatCount-1 do if PaperFormats[Result].WindowsID=AFormat
      then Exit;
  Result:=-1;
end;

Procedure TPaperFormat.AssignTo(Dest:TPersistent);
begin
  with Dest as TPaperFormat do begin
    FHeight:=Self.FHeight;
    FName:=Self.FName;
    FWindowsID:=Self.WindowsID;
    FWidth:=Self.FWidth;
  end;
end;

Function TWPrinter.GetStatusText:String;
var StatusNumber   : Integer;
begin
  case Status of
    0                                : StatusNumber:=100;
    PRINTER_STATUS_PAUSED            : StatusNumber:=101;
    PRINTER_STATUS_PENDING_DELETION  : StatusNumber:=103;
    PRINTER_STATUS_BUSY              : StatusNumber:=110;
    PRINTER_STATUS_DOOR_OPEN         : StatusNumber:=123;
    PRINTER_STATUS_INITIALIZING      : StatusNumber:=116;
    PRINTER_STATUS_IO_ACTIVE         : StatusNumber:=109;
    PRINTER_STATUS_MANUAL_FEED       : StatusNumber:=106;
    PRINTER_STATUS_NO_TONER          : StatusNumber:=119;
    PRINTER_STATUS_OFFLINE           : StatusNumber:=108;
    PRINTER_STATUS_OUT_OF_MEMORY     : StatusNumber:=122;
    PRINTER_STATUS_OUTPUT_BIN_FULL   : StatusNumber:=112;
    PRINTER_STATUS_PAGE_PUNT         : StatusNumber:=120;
    PRINTER_STATUS_PAPER_JAM         : StatusNumber:=104;
    PRINTER_STATUS_PAPER_OUT         : StatusNumber:=105;
    PRINTER_STATUS_PAPER_PROBLEM     : StatusNumber:=107;
    PRINTER_STATUS_PRINTING          : StatusNumber:=111;
    PRINTER_STATUS_PROCESSING        : StatusNumber:=115;
    PRINTER_STATUS_TONER_LOW         : StatusNumber:=118;
    PRINTER_STATUS_NOT_AVAILABLE     : StatusNumber:=113;
    PRINTER_STATUS_USER_INTERVENTION : StatusNumber:=121;
    PRINTER_STATUS_WAITING           : StatusNumber:=114;
    PRINTER_STATUS_WARMING_UP        : StatusNumber:=117;
    else StatusNumber:=102;
  end;
  Result:=MlgStringList['Printers',StatusNumber];
end;

Function TWPrinter.GetPixelsPerInch:TPoint;
begin
  Result.X:=GetDeviceCaps(Handle,LOGPIXELSX);
  Result.Y:=GetDeviceCaps(Handle,LOGPIXELSY);
end;

{===============================================================================
  Initialization and Termination-Code
+==============================================================================}

Initialization
begin
  WPrinters:=TWPrinters.Create;
end;

Finalization
begin
  WPrinters.Free;
end;  

end.
