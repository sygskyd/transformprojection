Unit ImagEdit;

Interface

Uses WinProcs,WinTypes,Classes,Controls,Graphics;

Type TDrawMode     = (dmNone,dmFreehand,dmLine,dmRect,dmFilledRect,dmEllipse,dmFilledEllipse,dmSelect,
                      dmFloodFill,dmText);
                      
     TImageEditor  = Class(TGraphicControl)
      Private
       FBackgroundColor      : TColor;
       FCurDisplay           : TBitmap;
       FDrawColor            : TColor;
       FDrawMode             : TDrawMode;
       FGraphic              : TBitmap;
       FGridColor            : TColor;
       FGridMask             : TBitmap;
       FLineMask             : TBitmap;
       FOnUndoRedoChanged    : TNotifyEvent;
       FOnUpdateEnabled      : TNotifyEvent;
       FOutputBmp            : TBitmap;
       FSelection            : TBitmap;
       FSelMoving            : Boolean;
       FSelMoved             : Boolean;
       FSelRect              : TRect;
       FSelValid             : Boolean;
       FUndoList             : TStringList;
       FRedoList             : TStringList;
       FGridVisible          : Boolean;
       LastCtrlState         : Boolean;
       LastPos               : TPoint;
       LineStart             : TPoint;
       FModified             : Boolean;
       FPixelSize            : Integer;
       Procedure   CreateLineMask;
       Procedure   CreateSelection(P1,P2:TPoint);
       Procedure   DoDrawFillRect(L1,L2:TPoint;FromData:Integer);
       Procedure   DoDrawEllipse(L1,L2:TPoint;FromData:Integer;Filled:Boolean;AsCircle:Boolean);
       Procedure   DoDrawLine(L1,L2:TPoint;FromData:Integer);
       Procedure   DoDrawRect(L1,L2:TPoint;FromData:Integer;RestrictSize:Boolean);
       Procedure   DoUndoRedo(AList:TStringList;ATitle:Integer;Const AMenuFn:String);
       Procedure   DrawSelRect(FromData:Integer);
       Procedure   Draw;
       Function    GetBitmapSize:TPoint;
       Procedure   SaveSelection(AUndoText:Integer);
       Procedure   SaveToUndoRedo(AUndo:Boolean;Const AText:String);
       Procedure   SetBitmap(ABitmap:TBitmap);
       Procedure   SetBitmapSize(ASize:TPoint);
       Procedure   SetGridColor(AColor:TColor);
       Procedure   SetFGridVisible(AVisible:Boolean);
       Procedure   SetDisplayToGraphic;
       Procedure   SetDrawMode(AMode:TDrawMode);
       Procedure   SetGraphSize;
       Procedure   SetPixelSize(ASize:Integer);
       Procedure   UpdateMenus;
      Protected
       Procedure   Loaded; override;
       Procedure   MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
       Procedure   MouseMove(Shift:TShiftState;X,Y:Integer); override;
       Procedure   MouseUp(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
       Procedure   Paint; override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Property    Modified:Boolean read FModified write FModified;
       Procedure   Redo;
       Property    RedoList:TStringList read FRedoList;
       Procedure   Undo;
       Property    UndoList:TStringList read FUndoList;
      Published
       Property    BackgroundColor:TColor read FBackgroundColor write FBackgroundColor default clBlack;
       Property    Bitmap:TBitmap read FGraphic write SetBitmap;
       Property    BitmapSize:TPoint read GetBitmapSize write SetBitmapSize;
       Property    Color;
       Property    DrawColor:TColor read FDrawColor write FDrawColor default clBlack;
       Property    DrawMode:TDrawMode read FDrawMode write SetDrawMode default dmFreehand;
       Property    GridColor:TColor read FGridColor write SetGridColor default clSilver;
       Property    Height default 32;
       Property    OnUndoRedoChanged:TNotifyEvent read FOnUndoRedoChanged write FOnUndoRedoChanged;
       Property    OnUpdateEnabled:TNotifyEvent read FOnUpdateEnabled write FOnUpdateEnabled;
       Property    ParentColor;
       Property    PixelSize:Integer read FPixelSize write SetPixelSize default 1;
       Property    ShowGrid:Boolean read FGridVisible write SetFGridVisible default False;
       Property    Width default 32;
     end;

Implementation

Uses NumTools;

Procedure SwapValues
  (
  {$IFDEF WIN32}
  var V1           : LongInt;
  var V2           : LongInt
  {$ELSE}
  var V1           : Integer;
  var V2           : Integer
  {$ENDIF}
  );
 var Save          : Integer;
 begin
   Save:=V1;
   V1:=V2;
   V2:=Save;
 end;

Constructor TImageEditor.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    ControlStyle:=ControlStyle+[csOpaque];
    ParentColor:=TRUE;
    Height:=32;
    Width:=32;
    FDrawMode:=dmFreehand;
    FGridColor:=clSilver;
    FGraphic:=TBitmap.Create;
    FCurDisplay:=TBitmap.Create;
    FLineMask:=TBitmap.Create;
    FGridMask:=TBitmap.Create;
    FOutputBmp:=TBitmap.Create;
    FSelection:=TBitmap.Create;
    FUndoList:=TStringList.Create;
    FRedoList:=TStringList.Create;
    FGraphic.Width:=32;
    FGraphic.Height:=32;
    FPixelSize:=1;
    SetGraphSize;
  end;

Destructor TImageEditor.Destroy;
  var Cnt          : Integer;
  begin
    FGraphic.Free;
    FOutputBmp.Free;
    FLineMask.Free;
    FGridMask.Free;
    FCurDisplay.Free;
    FSelection.Free;
    for Cnt:=0 to FUndoList.Count-1 do
      FUndoList.Objects[Cnt].Free;
    for Cnt:=0 to FRedoList.Count-1 do
      FRedoList.Objects[Cnt].Free;
    FUndoList.Free;
    FRedoList.Free;  
    inherited Destroy;
  end;    

Procedure TImageEditor.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  begin
    LineStart.X:=X Div FPixelSize;
    LineStart.Y:=Y Div FPixelSize;
    LastPos:=LineStart;
    if DrawMode=dmFloodFill then with FGraphic.Canvas do begin
      SaveToUndoRedo(TRUE,''{MlgSection[7]});
      Brush.Color:=FDrawColor;
      FloodFill(LineStart.X,LineStart.Y,Pixels[LineStart.X,LineStart.Y],fsSurface);
      SetDisplayToGraphic;
      Self.Draw;
      FModified:=TRUE;
      UpdateMenus;
    end
    else if DrawMode=dmFreehand then begin
      SaveToUndoRedo(TRUE,''{MlgSection[6]});
      DoDrawLine(LastPos,LastPos,2);
      SetDisplayToGraphic;
      Self.Draw;
      FModified:=TRUE;
      UpdateMenus;
    end
    else if DrawMode=dmSelect then begin
      if ptInRect(FSelRect,LastPos) then FSelMoving:=TRUE
      else SaveSelection(14);
    end
    else if DrawMode=dmText then begin
{      CreateCaret(Handle,0,0,50);
      ShowCaret(Handle);}
    end;
  end;

Procedure TImageEditor.DoDrawLine
   (
   L1              : TPoint;
   L2              : TPoint;
   FromData        : Integer
   );
  begin
    if FromData=0 then with FCurDisplay.Canvas do begin
      Pen.Color:=FDrawColor;
      MoveTo(L1.X,L1.Y);
      LineTo(L2.X,L2.Y);
      Pixels[L2.X,L2.Y]:=FDrawColor;
    end
    else if FromData=1 then SetDisplayToGraphic
    else with FGraphic.Canvas do begin
      Pen.Color:=FDrawColor;
      MoveTo(L1.X,L1.Y);
      LineTo(L2.X,L2.Y);
      Pixels[L2.X,L2.Y]:=Pen.Color;
      SetDisplayToGraphic;
    end;
  end;

Procedure TImageEditor.DoDrawRect
   (
   L1              : TPoint;
   L2              : TPoint;
   FromData        : Integer;
   RestrictSize    : Boolean
   );
  begin
    if L1.X>L2.X then SwapValues(L1.X,L2.X);
    if L1.Y>L2.Y then SwapValues(L1.Y,L2.Y);
    if RestrictSize then begin
      L1.X:=Max(0,Min(L1.X,FGraphic.Width-1));
      L2.X:=Max(0,Min(L2.X,FGraphic.Width-1));
      L1.Y:=Max(0,Min(L1.Y,FGraphic.Height-1));
      L2.Y:=Max(0,Min(L2.Y,FGraphic.Height-1));
    end;
    if FromData=0 then with FCurDisplay.Canvas do begin
      Brush.Color:=FDrawColor;
      FrameRect(Rect(L1.X,L1.Y,L2.X+1,L2.Y+1));
    end
    else if FromData=1 then SetDisplayToGraphic
    else with FGraphic.Canvas do begin
      Brush.Color:=FDrawColor;
      FrameRect(Rect(L1.X,L1.Y,L2.X+1,L2.Y+1));
      SetDisplayToGraphic;
    end;
  end;

Procedure TImageEditor.DoDrawFillRect
   (
   L1              : TPoint;
   L2              : TPoint;
   FromData        : Integer
   );
  begin
    if L1.X>L2.X then SwapValues(L1.X,L2.X);
    if L1.Y>L2.Y then SwapValues(L1.Y,L2.Y);
    if FromData=0 then with FCurDisplay.Canvas do begin
      if FBackgroundColor<0 then Brush.Style:=bsClear
      else Brush.Color:=FBackgroundColor;
      Pen.Color:=FDrawColor;
      Rectangle(L1.X,L1.Y,L2.X+1,L2.Y+1);
      Brush.Style:=bsSolid;
    end
    else if FromData=1 then SetDisplayToGraphic
    else with FGraphic.Canvas do begin
      if FBackgroundColor<0 then Brush.Style:=bsClear
      else Brush.Color:=FBackgroundColor;
      Pen.Color:=FDrawColor;
      Rectangle(L1.X,L1.Y,L2.X+1,L2.Y+1);
      SetDisplayToGraphic;
    end;
  end;

Procedure TImageEditor.MouseMove(Shift: TShiftState; X, Y: Integer);
  var NewPos       : TPoint;
  begin
    NewPos.X:=X Div FPixelSize;
    NewPos.Y:=Y Div FPixelSize;
    if (ssLeft in Shift)
        and ((NewPos.X<>LastPos.X)
            or (NewPos.Y<>LastPos.Y)) then begin
      if DrawMode=dmLine then begin
        DoDrawLine(LineStart,LastPos,1);
        DoDrawLine(LineStart,NewPos,0);
      end
      else if DrawMode=dmRect then begin
        DoDrawRect(LineStart,LastPos,1,FALSE);
        DoDrawRect(LineStart,NewPos,0,FALSE);
      end
      else if DrawMode=dmFilledRect then begin
        DoDrawFillRect(LineStart,LastPos,1);
        DoDrawFillRect(LineStart,NewPos,0);
      end
      else if DrawMode=dmEllipse then begin
        DoDrawEllipse(LineStart,LastPos,1,FALSE,LastCtrlState);
        LastCtrlState:=ssCtrl in Shift;
        DoDrawEllipse(LineStart,NewPos,0,FALSE,LastCtrlState);
      end
      else if DrawMode=dmFilledEllipse then begin
        DoDrawEllipse(LineStart,LastPos,1,TRUE,LastCtrlState);
        LastCtrlState:=ssCtrl in Shift;
        DoDrawEllipse(LineStart,NewPos,0,TRUE,LastCtrlState);
      end
      else if DrawMode=dmSelect then begin
        if not FSelValid then begin
          DoDrawRect(LineStart,LastPos,1,TRUE);
          DoDrawRect(LineStart,NewPos,0,TRUE);
        end
        else if FSelMoving then begin
          DrawSelRect(1);
          LastPos:=NewPos;
          DrawSelRect(0);
          FSelMoved:=TRUE;
        end;
      end
      else if (DrawMode=dmFreehand)
          and (ssLeft in Shift) then begin
            DoDrawLine(LastPos,NewPos,2);
          end;
      LastPos:=NewPos;
      Draw;
    end;
  end;

Procedure TImageEditor.DrawSelRect
   (
   FromData        : Integer
   );
  var ARect        : TRect;
      BRect        : TRect;
  begin
    ARect:=Rect(0,0,FSelection.Width,FSelection.Height);
    BRect:=ARect;
    OffsetRect(BRect,FSelRect.Left+LastPos.X-LineStart.X,FSelRect.Top+LastPos.Y-LineStart.Y);
    if FromData=0 then begin
      FCurDisplay.Canvas.CopyRect(BRect,FSelection.Canvas,ARect);
      Dec(BRect.Right); Dec(BRect.Bottom);
      DoDrawRect(BRect.TopLeft,BRect.BottomRight,0,FALSE);
    end
    else if FromData=1 then SetDisplayToGraphic
    else begin
      FGraphic.Canvas.CopyRect(FSelRect,FSelection.Canvas,ARect);
      SetDisplayToGraphic;
    end;
  end;

Procedure TImageEditor.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  begin
    LastPos.X:=X Div FPixelSize;
    LastPos.Y:=Y Div FPixelSize;
    if (LastPos.X<>LineStart.X)
        or (LastPos.Y<>LineStart.Y) then begin
      if DrawMode=dmLine then begin
        SaveToUndoRedo(TRUE,''{MlgSection[8]});
        DoDrawLine(LineStart,LastPos,2);
      end
      else if DrawMode=dmRect then begin
        SaveToUndoRedo(TRUE,''{MlgSection[9]});
        DoDrawRect(LineStart,LastPos,2,FALSE)
      end
      else if DrawMode=dmFilledRect then begin
        SaveToUndoRedo(TRUE,''{MlgSection[10]});
        DoDrawFillRect(LineStart,LastPos,2);
      end
      else if DrawMode=dmEllipse then begin
        SaveToUndoRedo(TRUE,''{MlgSection[11]});
        DoDrawEllipse(LineStart,LastPos,2,FALSE,LastCtrlState);
      end
      else if DrawMode=dmFilledEllipse then begin
        SaveToUndoRedo(TRUE,''{MlgSection[12]});
        DoDrawEllipse(LineStart,LastPos,2,TRUE,LastCtrlState);
      end;
      FModified:=TRUE;
      UpdateMenus;
    end
    else if DrawMode<>dmSelect then SetDisplayToGraphic;
    if (DrawMode=dmSelect) then begin
      if not FSelValid then CreateSelection(LineStart,LastPos)
      else OffsetRect(FSelRect,LastPos.X-LineStart.X,LastPos.Y-LineStart.Y);
      FSelMoving:=FALSE;
      UpdateMenus;
    end;
    Draw;
  end;

Procedure TImageEditor.CreateSelection
   (
   P1              : TPoint;
   P2              : TPoint
   );
  var ARect        : TRect;
  begin
    if (P1.X=P2.X)
        and (P1.Y=P2.Y) then FSelValid:=FALSE
    else begin
      if P1.X>P2.X then SwapValues(P1.X,P2.X);
      if P1.Y>P2.Y then SwapValues(P1.Y,P2.Y);
      P1.X:=Max(0,Min(P1.X,FGraphic.Width-1));
      P2.X:=Max(0,Min(P2.X,FGraphic.Width-1));
      P1.Y:=Max(0,Min(P1.Y,FGraphic.Height-1));
      P2.Y:=Max(0,Min(P2.Y,FGraphic.Height-1));
      FSelRect:=Rect(P1.X,P1.Y,P2.X+1,P2.Y+1);
      with FSelRect do ARect:=Rect(0,0,Right-Left,Bottom-Top);
      FSelection.Width:=ARect.Right;
      FSelection.Height:=ARect.Bottom;
      FSelection.Canvas.CopyRect(ARect,FGraphic.Canvas,FSelRect);
      FSelValid:=TRUE;
    end;
    FSelMoved:=FALSE;
  end;

Procedure TImageEditor.Paint;
begin
  Draw;
end;

Procedure TImageEditor.UpdateMenus;
begin
  if Assigned(FOnUpdateEnabled) then OnUpdateEnabled(Self);
end;

Procedure TImageEditor.DoDrawEllipse(L1:TPoint;L2:TPoint;FromData:Integer;
   Filled:Boolean;AsCircle:Boolean);
var AWidth       : Integer;
    AHeight      : Integer;
begin
  AWidth:=Abs(L2.X-L1.X)+1;
  AHeight:=Abs(L2.Y-L1.Y)+1;
  if AsCircle then begin
    if AHeight<AWidth then AWidth:=AHeight
    else AHeight:=AWidth;
  end;
  if L2.X<L1.X then AWidth:=-AWidth;
  if L2.Y<L1.Y then AHeight:=-AHeight;
  if FromData=0 then with FCurDisplay.Canvas do begin
    Pen.Color:=FDrawColor;
    if not Filled or (FBackgroundColor<0) then Brush.Style:=bsClear
    else Brush.Color:=FBackgroundColor;
    Ellipse(L1.X,L1.Y,L1.X+AWidth,L1.Y+AHeight);
    Brush.Style:=bsSolid;
  end
  else if FromData=1 then SetDisplayToGraphic
  else with FGraphic.Canvas do begin
    Pen.Color:=FDrawColor;
    if not Filled or (FBackgroundColor<0) then Brush.Style:=bsClear
    else Brush.Color:=FBackgroundColor;
    Ellipse(L1.X,L1.Y,L1.X+AWidth,L1.Y+AHeight);
    Brush.Style:=bsSolid;
    SetDisplayToGraphic;
  end;
end;

Procedure TImageEditor.DoUndoRedo
   (
   AList           : TStringList;
   ATitle          : Integer;
   Const AMenuFn   : String
   );
  var AIndex       : Integer;
      AGraphic     : TBitmap;
      SizeChange   : Boolean;
  begin
    AIndex:=AList.Count-1;
    SaveToUndoRedo(AList<>FUndoList,AList[AIndex]);
    AGraphic:=TBitmap(AList.Objects[AIndex]);
    SizeChange:=(AGraphic.Width<>FGraphic.Width) or (AGraphic.Height<>FGraphic.Height);
    FGraphic.Assign(AGraphic);
    AList.Delete(AIndex);
{    if AList.Count=0 then MenuFunctions[AMenuFn].Caption:=MlgSection[ATitle]
    else MenuFunctions[AMenuFn].Caption:=MlgSection[ATitle]+': '+AList[AIndex-1];}
    if SizeChange then SetGraphSize
    else begin
      SetDisplayToGraphic;
      Draw;
    end;
    UpdateMenus;
  end;

Procedure TImageEditor.Draw;
var ARect        : TRect;
    BRect        : TRect;
begin
  ARect:=Rect(0,0,FGraphic.Width,FGraphic.Height);
  BRect:=Rect(0,0,FGraphic.Width*FPixelSize+1,FGraphic.Height*FPixelSize+1);
  with FOutputBmp.Canvas do begin
    CopyMode:=cmSrcCopy;
    CopyRect(BRect,FCurDisplay.Canvas,ARect);
    if (FPixelSize>2) and FGridVisible then begin
      CopyMode:=cmSrcAnd;
      CopyRect(BRect,FLineMask.Canvas,BRect);
      CopyMode:=cmSrcPaint;
      CopyRect(BRect,FGridMask.Canvas,BRect);
    end;
  end;
  Canvas.CopyRect(BRect,FOutputBmp.Canvas,BRect);
  Canvas.Brush.Color:=Color;
  Canvas.FillRect(Rect(BRect.Right,0,ClientWidth,BRect.Bottom));
  Canvas.FillRect(Rect(0,BRect.Bottom,ClientWidth,ClientHeight));
end;

Procedure TImageEditor.SaveSelection
   (
   AUndoText       : Integer
   );
  begin
    if FSelValid then begin
      if FSelMoved then begin
        SaveToUndoRedo(TRUE,''{MlgSection[AUndoText]});
        DrawSelRect(2);
      end
      else DrawSelRect(1);
      FSelValid:=FALSE;
      FModified:=TRUE;
      Draw;
    end;
  end;

Procedure TImageEditor.SaveToUndoRedo
   (
   AUndo           : Boolean;
   Const AText     : String
   );
  var ABitmap      : TBitmap;
  begin
    ABitmap:=TBitmap.Create;
    ABitmap.Assign(FGraphic);
    if AUndo then begin
      FUndoList.AddObject(AText,ABitmap);
      if Assigned(FOnUndoRedoChanged) then OnUndoRedoChanged(Self);
    end
    else begin
      FRedoList.AddObject(AText,ABitmap);
      if Assigned(FOnUndoRedoChanged) then OnUndoRedoChanged(Self);
    end;
    UpdateMenus;
  end;

Procedure TImageEditor.SetDisplayToGraphic;
  var ARect        : TRect;
  begin
    ARect:=Rect(0,0,FGraphic.Width,FGraphic.Height);
    FCurDisplay.Canvas.CopyRect(ARect,FGraphic.Canvas,ARect)
  end;

Procedure TImageEditor.SetDrawMode
   (
   AMode           : TDrawMode
   );
  begin
    FDrawMode:=AMode;
    SaveSelection(14);
    UpdateMenus;
  end;

Procedure TImageEditor.SetGraphSize;
  begin
    FCurDisplay.Width:=FGraphic.Width;
    FCurDisplay.Height:=FGraphic.Height;
    SetDisplayToGraphic;
    CreateLineMask;
  end;

Procedure TImageEditor.CreateLineMask;
  var Line         : Integer;
  begin
    FOutputBmp.Width:=FGraphic.Width*FPixelSize+1;
    FOutputBmp.Height:=FGraphic.Height*FPixelSize+1;
    with FLineMask,Canvas do begin
      Width:=FGraphic.Width*FPixelSize+1;
      Height:=FGraphic.Height*FPixelSize+1;
      Brush.Style:=bsSolid;
      Brush.Color:=clWhite;
      FillRect(Rect(0,0,Width+1,Height+1));
      Pen.Color:=clBlack;
      for Line:=Width downto 0 do begin
        MoveTo(Line*FPixelSize,0);
        LineTo(Line*FPixelSize,Height);
      end;
      for Line:=Height downto 0 do begin
        MoveTo(0,Line*FPixelSize);
        LineTo(Width,Line*FPixelSize);
      end;
    end;
    with FGridMask,Canvas do begin
      Width:=FGraphic.Width*FPixelSize+1;
      Height:=FGraphic.Height*FPixelSize+1;
      Brush.Style:=bsSolid;
      Brush.Color:=clBlack;
      FillRect(Rect(0,0,Width+1,Height+1));
      Pen.Color:=FGridColor;
      for Line:=Width downto 0 do begin
        MoveTo(Line*FPixelSize,0);
        LineTo(Line*FPixelSize,Height);
      end;
      for Line:=Height downto 0 do begin
        MoveTo(0,Line*FPixelSize);
        LineTo(Width,Line*FPixelSize);
      end;
    end;
  end;

Procedure TImageEditor.SetFGridVisible
   (
   AVisible        : Boolean
   );
  begin
    if AVisible<>FGridVisible then begin
      FGridVisible:=AVisible;
      CreateLineMask;
      Invalidate;
    end;
  end;

Procedure TImageEditor.SetBitmap
   (
   ABitmap         : TBitmap
   );
  begin
    FModified:=FALSE;
    FGraphic.Assign(ABitmap);
    SetGraphSize;
    SetDisplayToGraphic;
    Invalidate;
  end;

Procedure TImageEditor.SetPixelSize
   (
   ASize           : Integer
   );
  begin
    if ASize<>FPixelSize then begin
      FPixelSize:=ASize;
      CreateLineMask;
      Invalidate;
    end;
  end;

Procedure TImageEditor.SetGridColor
   (
   AColor          : TColor
   );
  begin
    if AColor<>FGridColor then begin
      FGridColor:=AColor;
      CreateLineMask;
      if FGridVisible then Invalidate;
    end;
  end;

Procedure TImageEditor.Loaded;
  begin
    inherited Loaded;
    SetDisplayToGraphic;
  end;

Procedure TImageEditor.Undo;
  begin
    if FUndoList.Count>0 then DoUndoRedo(FUndoList,4,'EditUndo');
  end;

Procedure TImageEditor.Redo;
  begin
    if FRedoList.Count>0 then DoUndoRedo(FRedoList,5,'EditRedo');
  end;

Function TImageEditor.GetBitmapSize:TPoint;
begin
  Result.X:=FGraphic.Width;
  Result.Y:=FGraphic.Height;
end;

Procedure TImageEditor.SetBitmapSize(ASize:TPoint);
begin
  FGraphic.Width:=ASize.X;
  FGraphic.Height:=ASize.Y;
  SetGraphSize;
  SetDisplayToGraphic;
  Invalidate;
end;

end.
