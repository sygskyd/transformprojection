�
 TSERIESTEXTEDITOR 0�  TPF0TSeriesTextEditorSeriesTextEditorLeftTop� BorderStylebsDialogCaptionSeries DataSource Text EditorClientHeight� ClientWidthqColor	clBtnFace
ParentFont	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TButtonButton1Left� Top� WidthKHeightCaptionOKDefault	ModalResultTabOrder OnClickButton1Click  TButtonButton2LeftTop� WidthKHeightCancel	CaptionCancelModalResultTabOrder  TPageControlPageControl1Left Top WidthqHeight� 
ActivePage	TabSheet1AlignalTopHotTrack	TabOrder 	TTabSheet	TabSheet1CaptionFields TLabelLabel1LeftTopSWidthrHeightCaptionNumber of &Header lines:FocusControlEdit1  TLabelLabel2Left0Top2Width1HeightCaption&Separator:FocusControlCBSep  TLabelLabel3LeftTop
Width HeightCaptionS&eries:  TEditEdit1Left� TopPWidth)HeightTabOrder Text0  TUpDownUpDown1Left� TopPWidthHeight	AssociateEdit1Min Position TabOrderWrap  TStringGridStringGrid1Left� TopWidth� HeightqColCountDefaultRowHeightOptions
goVertLine
goHorzLine	goEditing TabOrder
RowHeights   	TComboBoxCBSeriesLeft0TopWidth� HeightStylecsDropDownList
ItemHeightTabOrderOnChangeCBSeriesChange  	TComboBoxCBSepLefthTop0WidthQHeight
ItemHeightTabOrderTextCommaItems.StringsCommaSpaceTab    	TTabSheet	TabSheet2CaptionSource TSpeedButtonBBrowseLeftJTopWidthHeightCaption...OnClickSpeedButton1Click  TRadioButtonRBFileLeftTopWidthRHeightCaption&File:Checked	TabOrder TabStop	OnClickRBFileClick  TEditEFileLeft^TopWidth� HeightTabOrderTextc:\test.txt  TRadioButtonRBWebLeftTop<WidthRHeightCaption	&Web URL:TabOrderOnClickRBFileClick  TEditEWebLeft^Top8Width� HeightEnabledTabOrderTexthttp://www.steema.com/test.txt  TButtonButton4Left^Top\WidthKHeightCaption&Load TabOrderOnClickButton4Click    TOpenDialogOpenDialog1
DefaultExttxtFilterText files|*.txtOptions
ofReadOnlyofHideReadOnlyofEnableSizing LeftTop@   