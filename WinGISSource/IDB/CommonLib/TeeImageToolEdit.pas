unit TeeImageToolEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeeToolSeriesEdit, TeeTools, StdCtrls, ExtCtrls;

type
  TChartImageToolEditor = class(TSeriesToolEditor)
    GroupBox2: TGroupBox;
    Image1: TImage;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    Image : TChartImageTool;
    Procedure CheckImageButton;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

Uses TeeBrushDlg, TeeConst;

procedure TChartImageToolEditor.Button1Click(Sender: TObject);
var tmp : String;
begin
  if Button1.Tag=1 then Image.Picture:=nil
  else
  begin
    tmp:=TeeGetPictureFileName(Self);
    if tmp<>'' then
    begin
      Image.Picture.LoadFromFile(tmp);
      Image.Repaint;
    end;
  end;
  CheckImageButton;
end;

Procedure TChartImageToolEditor.CheckImageButton;
begin
  if Assigned(Image.Picture.Graphic) then
  begin
    Button1.Tag:=1;
    Button1.Caption:=TeeMsg_ClearImage;
    Image1.Picture.Assign(Image.Picture);
  end
  else
  begin
    Button1.Tag:=0;
    Button1.Caption:=TeeMsg_BrowseImage;
    Image1.Picture:=nil;
  end;
end;

procedure TChartImageToolEditor.FormShow(Sender: TObject);
begin
  inherited;
  Image:=TChartImageTool(Tag);
  CheckImageButton;
end;

initialization
  RegisterClass(TChartImageToolEditor);
end.
