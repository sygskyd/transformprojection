Unit Combobtn;

Interface

Uses SysUtils,WinTypes,WinProcs,Classes,Graphics,Controls,
     Forms,Menus,Messages,WCtrls;

Type TPaintButtonEvent  = Procedure(Sender:TObject;ARect:TRect) of object;

     TArrowDirection    = (adDown,adRight,adUp,adLeft);

     TCustomComboBtn    = Class(TCustomControl)
      Private
       FArrowDirection  : TArrowDirection;
       FAlignment       : TAlignment;
       FComboWidth      : Integer;
       FDown            : Boolean;
       FFlat            : Boolean;
       FMouseInControl  : Boolean;
       FOnComboClick    : TNotifyEvent;
       FOnPaintButton   : TPaintButtonEvent;
       FSeparate        : Boolean;
       Function    ButtonRect:TRect;
       Procedure   CMCaptionChanged(var Message:TMessage); message cm_TextChanged;
       Procedure   CMEnabledChanged(var Message:TMessage); message cm_EnabledChanged;
       Function    ComboButtonRect:TRect;
       Procedure   DoPaintFocus;
       Procedure   SetAlignment(AAlignment:TAlignment);
       Procedure   SetArrowDirection(AArrowDirection:TArrowDirection);
       Procedure   SetComboWidth(AWidth:Integer);
       Procedure   SetDown(ADown:Boolean);
       Procedure   SetFlat(AFlat:Boolean);
       Procedure   SetSeparate(ASeparate:Boolean);
      Protected
       Property    Alignment:TAlignment read FAlignment write SetAlignment default taLeftJustify;
       Property    ArrowDirection:TArrowDirection read FArrowDirection write SetArrowDirection default adDown;
       Property    ButtonDown:Boolean read FDown write SetDown;
       Procedure   CMMouseEnter(var Message: TMessage); message CM_MOUSEENTER;
       Procedure   CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
       Property    ComboBtnWidth:Integer read FComboWidth write SetComboWidth default 13;
       Procedure   DoPaintBtn(ARect:TRect); virtual;
       Property    Flat:Boolean read FFlat write SetFlat default False;
       Procedure   MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
       Procedure   MouseMove(Shift:TShiftState;X,Y:Integer); override;
       Procedure   MouseUp(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
       Property    OnComboBtnClick:TNotifyEvent read FOnComboClick write FOnComboClick;
       Property    OnPaintBtn:TPaintButtonEvent read FOnPaintButton write FOnPaintButton;
       Procedure   Paint; override;
       Property    SeparateBtns:Boolean read FSeparate write SetSeparate default True;
       Property    Tabstop default TRUE;
       Procedure   WMKillFocus(var Msg:TWMKillFocus); message wm_KillFocus;
       Procedure   WMSetFocus(var Msg:TWMSetFocus); message wm_SetFocus;
      Public
       Constructor Create(AOwner:TComponent); override;
       Property    Canvas;
      Published
       Property    Height default 22;
       Property    Width default 37;
     end;

     TComboBtn     = Class(TCustomComboBtn)
      Published
       Property    Alignment;
       Property    Anchors;
       Property    ArrowDirection;
       Property    Caption;
       Property    ComboBtnWidth;
       Property    Enabled;
       Property    Flat;
       Property    Font;
       Property    Hint;
       Property    OnClick;
       Property    OnComboBtnClick;
       Property    OnPaintBtn;
       Property    ParentFont;
       Property    ParentShowHint;
       Property    SeparateBtns;
       Property    ShowHint;
       Property    TabOrder;
       Property    TabStop;
     end;

     TDropDownForm = Class(TWForm)
      Protected
       Procedure   CreateParams(var Params:TCreateParams); override;
       Procedure   WMActivate(var Message:TWMActivate); message wm_Activate;
       Procedure   WMNCActivate(var Message:TWMNCActivate); message wm_NCActivate;
     end;

     TCustomDropDownBtn = Class(TCustomComboBtn)
      Private
       FDropDown   : TWinControl;
       FOnDropDown : TNotifyEvent;
       Property    SeparateBtns stored False default False;
      Protected
       Procedure   Click; override;
       Property    DropDown:TWinControl read FDropDown write FDropDown;
       Procedure   KeyDown(var Key:Word;Shift:TShiftState); override;
       Property    OnDropDown:TNotifyEvent read FOnDropDown write FOnDropDown;
      Public
       Constructor Create(AOwner:TComponent); override;
     end;

     TDropDownBtn  = Class(TCustomDropDownBtn)
      Public
       Property    DropDown;
      Published
       Property    Alignment;
       Property    Anchors;
       Property    Caption;
       Property    Flat;
       Property    Font;
       Property    Hint;
       Property    OnPaintBtn;
       Property    ParentFont;
       Property    ParentShowHint;
       Property    ShowHint;
       Property    TabOrder;
       Property    TabStop;
     end;

     TPopupMenuBtn = Class(TCustomComboBtn)
      Protected
       Property    SeparateBtns stored False default False;
       Procedure   MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
      Public
       Constructor Create(AOwner:TComponent); override;
      Published
       Property    Alignment;
       Property    Anchors;
       Property    ArrowDirection;
       Property    Caption;
       Property    ComboBtnWidth;
       Property    Flat;
       Property    Font;
       Property    Hint;
       Property    ParentFont;
       Property    ParentShowHint;
//       Property    PopuMenu:TPopupMenu read FPopupMenu write FPopupMenu;
       Property    PopupMenu;
       Property    ShowHint;
       Property    TabOrder;
       Property    TabStop;
     end;

Implementation

{$IFDEF WIN32}
{$R *.r32}
{$ELSE}
{$R *.res}
{$ENDIF}

Uses ExtCtrls;

var FDownButton    : Integer;
    FComboBitmap   : TBitmap;

Constructor TCustomComboBtn.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    ControlStyle:=ControlStyle+[csOpaque];
    FComboWidth:=13;
    FSeparate:=TRUE;
    Height:=22;
    Width:=37;
    TabStop:=TRUE;
  end;

Procedure TCustomComboBtn.Paint;
  Procedure Frame(var Rect:TRect;OnLeft:Boolean;Color1,Color2,Color3:TColor);
  begin
    with Canvas,Rect do begin
      Dec(Right);
      Dec(Bottom);
      Pen.Color:=Color1;
      if OnLeft then Polyline([Point(Right,Top),Point(Left,Top),Point(Left,Bottom)])
      else Polyline([Point(Left,Top),Point(Right,Top)]);
      Pen.Color:=Color2;
      if OnLeft then Polyline([Point(Left,Bottom),Point(Right+1,Bottom)])
      else Polyline([Point(Left,Bottom),Point(Right,Bottom),Point(Right,Top)]);
      if OnLeft then Pen.Color:=Color3
      else Pen.Color:=clBtnFace;
      if OnLeft then Polyline([Point(Right,Bottom-1),Point(Right,Top)])
      else Polyline([Point(Left,Top+1),Point(Left,Bottom)]);
      Inc(Left);
      Inc(Top);
    end;
  end;
  Procedure PaintFrameSeparate(var Rect:TRect;Left,ADown:Boolean);
  begin
    if ADown and FDown then begin
      Frame(Rect,Left,clBlack,clBtnHighlight,clBtnShadow); 
      Frame(Rect,Left,clBtnShadow,clBtnFace,clBtnFace);
    end
    else begin
      Frame(Rect,Left,clBtnHighlight,clBlack,clBtnShadow);
      Frame(Rect,Left,clBtnFace,clBtnShadow,clBtnFace);
    end;
    Canvas.FillRect(Rect);
    InflateRect(Rect,-1,-1);
    if ADown and FDown then OffsetRect(Rect,1,1);
  end;
  Procedure PaintFrame(ARect:TRect;ADown:Boolean);
  begin
    if FFlat then begin
      if FDown and ADown then Frame3D(Canvas,ARect,clBtnShadow,clBtnHighlight,1)
      else if FMouseInControl then Frame3D(Canvas,ARect,clWhite,clBtnShadow,1)
      else if csDesigning in ComponentState then Frame3D(Canvas,ARect,clWhite,clBtnShadow,1)
      else Frame3D(Canvas,ARect,clBtnFace,clBtnFace,1);
      InflateRect(ARect,-1,-1);
    end
    else if ADown and FDown then begin
      Frame3D(Canvas,ARect,cl3dDkShadow,clBtnHighlight,1);
      Frame3D(Canvas,ARect,clBtnShadow,cl3dLight,1);
    end
    else begin
      Frame3D(Canvas,ARect,clBtnHighlight,cl3dDkShadow,1);
      Frame3D(Canvas,ARect,cl3dLight,clBtnShadow,1);
    end;
    Canvas.FillRect(ARect);
    ARect:=Rect(ARect.Left+2,ARect.Top+2,ARect.Right-ComboBtnWidth,ARect.Bottom-2);
    if ADown and FDown then OffsetRect(ARect,1,1);
    DoPaintBtn(ARect);
  end;         
var ARect        : TRect;
    BRect        : TRect;
begin
  with Canvas do begin  
    Pen.Style:=psSolid;
    Brush.Style:=bsSolid;
    Brush.Color:=clBtnFace;
    if not FSeparate then PaintFrame(ClientRect,FDownButton=1)
    else begin
      ARect:=ComboButtonRect; 
      PaintFrameSeparate(ARect,False,FDownButton=3);
      ARect:=ButtonRect;
      PaintFrameSeparate(ARect,True,FDownButton=2);
      DoPaintBtn(ARect);
    end;
    ARect:=Rect(0,0,7,7);
    OffsetRect(ARect,Integer(FArrowDirection)*7,0);
    BRect:=Bounds(Width-FComboWidth-1+(FComboWidth-7) Div 2,
        (Height-7) Div 2,7,7);
    if (FDownButton<>2) and FDown then OffsetRect(BRect,1,1);
    Brush.Color:=clBtnFace;
    SetBKMode(Handle,TRANSPARENT);
    SetBKColor(Handle,ColorToRGB(clBtnFace));
    if Enabled then Font.Color:=clBlack
    else Font.Color:=clBtnShadow;
    CopyRect(BRect,FComboBitmap.Canvas,ARect);
    if Focused then DoPaintFocus;
  end;
end;
  
Procedure TCustomComboBtn.DoPaintFocus;
var ARect        : TRect;
begin
  with Canvas do begin
    if FSeparate then ARect:=ButtonRect
    else ARect:=ClientRect;
    InflateRect(ARect,-3,-3);
    if Assigned(FOnPaintButton) then begin
      Dec(ARect.Right,ComboBtnWidth-2);
      if FDown then OffsetRect(ARect,1,1);
    end;
    DrawFocusRect(ARect);
  end;
end;

Procedure TCustomComboBtn.MouseDown
   (
   Button          : TMouseButton;
   Shift           : TShiftState;
   X               : Integer;
   Y               : Integer
   );
  begin
    if Button=mbLeft then begin
      if TabStop and not Focused then SetFocus;
      if not FSeparate then FDownButton:=1
      else if ptInRect(ButtonRect,Point(X,Y)) then FDownButton:=2
      else FDownButton:=3;
      ButtonDown:=TRUE;
    end;
  end;

Procedure TCustomComboBtn.MouseMove
   (
   Shift           : TShiftState;
   X               : Integer;
   Y               : Integer
   );
  begin
    if (ssLeft in Shift) and (FDownButton<>0) then begin
      case FDownButton of
        1: ButtonDown:=ptInRect(ClientRect,Point(X,Y));
        2: ButtonDown:=ptInRect(ButtonRect,Point(X,Y));
        3: ButtonDown:=ptInRect(ComboButtonRect,Point(X,Y));
      end;
    end;
  end;

Procedure TCustomComboBtn.MouseUp
   (
   Button          : TMouseButton;
   Shift           : TShiftState;
   X               : Integer;
   Y               : Integer
   );
  begin
    if Button=mbLeft then begin
      FDownButton:=0;
      ButtonDown:=FALSE;
    end;  
  end;

Function TCustomComboBtn.ButtonRect
   : TRect;
  begin
    Result:=ClientRect;
    Dec(Result.Right,FComboWidth);
  end;

Function TCustomComboBtn.ComboButtonRect
   : TRect;
  begin
    Result:=ClientRect;
    Result.Left:=Result.Right-FComboWidth;
  end;

Procedure TCustomComboBtn.SetDown
   (
   ADown           : Boolean
   );
  begin
    if ADown<>FDown then begin
      FDown:=ADown;
      Repaint;
    end;
  end;

Procedure TCustomComboBtn.SetComboWidth
   (
   AWidth          : Integer
   );
  begin
    if AWidth<>FComboWidth then begin
      FComboWidth:=AWidth;
      Invalidate;
    end;
  end;

Procedure TCustomComboBtn.WMKillFocus
   (
   var Msg         : TWMKillFocus
   );
  begin
    DoPaintFocus;
    inherited;
  end;

Procedure TCustomComboBtn.WMSetFocus
   (
   var Msg         : TWMSetFocus
   );
  begin
    DoPaintFocus;
    inherited;
  end;

Procedure TCustomComboBtn.SetSeparate
   (
   ASeparate       : Boolean
   );                             
  begin
    if ASeparate<>FSeparate then begin
      FSeparate:=ASeparate;
      Invalidate;
    end;
  end;                    

Procedure TCustomComboBtn.DoPaintBtn(ARect:TRect);
var Buffer       : Array[0..128] of Char;
    Format       : Word;
begin
  Canvas.Font:=Self.Font;
  if not Enabled then Canvas.Font.Color:=clBtnShadow;
  if Caption<>'' then with Canvas do begin
    if FAlignment=taCenter then Format:=DT_CENTER
    else if FAlignment=taRightJustify then Format:=DT_RIGHT                
    else Format:=DT_LEFT;
    DrawText(Handle,StrPCopy(Buffer,Caption),Length(Caption),ARect,
        Format or DT_VCENTER or DT_SINGLELINE);
  end
  else if Assigned(OnPaintBtn) then OnPaintBtn(Self,ARect);
end;

Procedure TCustomComboBtn.SetAlignment
   (
   AAlignment      : TAlignment
   );
  begin
    if AAlignment<>FAlignment then begin
      FAlignment:=AAlignment;
      Invalidate;
    end;
  end;

Procedure TCustomComboBtn.CMCaptionChanged(var Message:TMessage);
begin
  inherited;
  Invalidate;
end;

Procedure TCustomComboBtn.CMEnabledChanged(var Message:TMessage);
begin
  inherited;
  Invalidate;
end;

Procedure TCustomComboBtn.SetArrowDirection
   (
   AArrowDirection      : TArrowDirection
   );
  begin
    if AArrowDirection<>FArrowDirection then begin
      FArrowDirection:=AArrowDirection;
      Invalidate;
    end;
  end;

Procedure TCustomComboBtn.SetFlat(AFlat:Boolean);
begin
  if AFlat<>FFlat then begin
    FFlat:=AFlat;
    Invalidate;
  end;
end;

Procedure TCustomComboBtn.CMMouseEnter(var Message: TMessage);
begin
  if FFlat then begin
    FMouseInControl:=TRUE;
    Repaint;
  end;
end;

Procedure TCustomComboBtn.CMMouseLeave(var Message: TMessage);
begin
  if FFlat then begin
    FMouseInControl:=FALSE;
    Repaint;
  end;
end;

{==============================================================================}
{ TCustomDropDownBtn                                                           } 
{==============================================================================}

Constructor TCustomDropDownBtn.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    SeparateBtns:=FALSE;
  end;

Procedure TCustomDropDownBtn.Click;
  begin
    if Assigned(FDropDown) then begin
      FDropDown.Left:=ClientOrigin.X;
      FDropDown.Top:=ClientOrigin.Y+Height;
      if FDropDown.ClientOrigin.Y+FDropDown.Height>=Screen.Height then
          FDropDown.Top:=ClientOrigin.Y-FDropDown.Height;
      if Assigned(FOnDropDown) then OnDropDown(Self);
      FDropDown.Show;
    end;
  end;

Procedure TCustomDropDownBtn.KeyDown
   (
   var Key         : Word;
   Shift           : TShiftState
   );
  begin
    inherited KeyDown(Key,Shift);
    case Key of
      vk_Down  : if ssAlt in Shift then Click;
      vk_Space : Click;
    end;
  end;

{==============================================================================}
{ TPopupMenuBtn                                                                }
{==============================================================================}

Constructor TPopupMenuBtn.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    SeparateBtns:=FALSE;
  end;

Procedure TPopupMenuBtn.MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer);
  var Align        : Integer;
  begin
    inherited MouseDown(Button,Shift,X,Y);
    if not(csDesigning in ComponentState) and ButtonDown then begin
    //and Assigned(FPopupMenu) then begin
      X:=ClientOrigin.X;
      Y:=ClientOrigin.Y;
      Align:=TPM_LEFTALIGN;
      case ArrowDirection of
        adLeft  : Align:=TPM_RIGHTALIGN;
        adDown  : Inc(Y,Height);
        adUp,
        adRight : Inc(X,Width);
      end;
      PopupMenu.Popup(X,Y);
//      TrackPopupMenu(FPopupMenu.Handle,Align or TPM_LEFTBUTTON,
//          X,Y,0,Handle,NIL);
      FDownButton:=0;
      ButtonDown:=FALSE;
    end;
  end;

{==============================================================================}
{ TDropDownForm
{==============================================================================}

Procedure TDropDownForm.CreateParams
   (
   var Params      : TCreateParams
   );
  begin
    inherited CreateParams(Params);
    Params.Style:=Params.Style and not (ws_Child+ws_Caption) or ws_Popup;
  end;

Procedure TDropDownForm.WMActivate
   (
   var Message     : TWMActivate
   );
  begin
    if Message.Active=wa_InActive then Hide;
    inherited;
  end;

Procedure TDropDownForm.WMNCActivate
   (
   var Message     : TWMNCActivate
   );
  begin
    if not Message.Active then Hide;
    inherited;
  end;

{$IFNDEF WIN32}  
Procedure ExitUnit; Far;
  begin
    FComboBitmap.Free;
  end;
{$ENDIF}  

Initialization
  begin
    FDownButton:=0;
    FComboBitmap:=NIL;
    {$IFNDEF WIN32}
    AddExitProc(ExitUnit);
    {$ENDIF}
    FComboBitmap:=TBitmap.Create;
    FComboBitmap.Handle:=LoadBitmap(HInstance,'COMBOBTN');
  end;

{$IFDEF WIN32}
Finalization
  begin
    FComboBitmap.Free;
  end;
{$ENDIF}

end.
