{******************************************}
{ TeeChart Tools Gallery                   }
{ Copyright (c) 2000 by David Berneda      }
{******************************************}
{$I teedefs.inc}
unit TeeToolsGallery;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls,
     {$ENDIF}
     TeEngine, Chart, TeCanvas;

type
  TTeeToolsGallery = class(TForm)
    P1: TPanel;
    LBTool: TListBox;
    Panel1: TPanel;
    BOk: TButtonColor;
    BCan: TButtonColor;
    procedure FormShow(Sender: TObject);
    procedure LBToolDblClick(Sender: TObject);
    {$IFNDEF CLX}
    procedure LBToolDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    {$ENDIF}
  private
    { Private declarations }
    Function ToolAtIndex(AIndex:Integer):TTeeCustomToolClass;
  public
    { Public declarations }
    Function SelectedTool:TTeeCustomToolClass;
  end;

{$IFNDEF CLX}
procedure TeeDrawTool(AList:TListBox;
                      Index: Integer; Rect: TRect; State: TOwnerDrawState;
                      ATool:TTeeCustomTool);
{$ENDIF}

implementation

{$R *.dfm}
{$R TeeTools.res}

procedure TTeeToolsGallery.FormShow(Sender: TObject);
var t : Integer;
begin
  LBTool.Clear;
  for t:=0 to TeeToolTypes.Count-1 do
      LBTool.Items.AddObject(TeeToolTypes[t].Description,TObject(TeeToolTypes[t]));
end;

Function TTeeToolsGallery.SelectedTool:TTeeCustomToolClass;
begin
  result:=ToolAtIndex(LBTool.ItemIndex);
end;

procedure TTeeToolsGallery.LBToolDblClick(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

Function TTeeToolsGallery.ToolAtIndex(AIndex:Integer):TTeeCustomToolClass;
begin
  result:=TTeeCustomToolClass(LBTool.Items.Objects[AIndex]);
end;

type TTeeToolAccess=class(TTeeCustomTool);

{$IFNDEF CLX}
procedure TeeDrawTool(AList:TListBox;
  Index: Integer; Rect: TRect; State: TOwnerDrawState; ATool:TTeeCustomTool);
var tmp : TBitmap;
begin
  tmp:=TBitmap.Create;
  try
    TTeeToolAccess(ATool).GetBitmapEditor(tmp);
    With AList.Canvas do
    begin
      if odSelected in State then Brush.Color:=clHighLight
                             else Brush.Color:=AList.Color;
      FillRect(Rect);
      Draw(Rect.Left,Rect.Top+1,tmp);

      if odSelected in State then Font.Color:=clHighlightText
                             else Font.Color:=AList.Font.Color;
      Brush.Style:=bsClear;
      TextOut(Rect.Left+18,Rect.Top+2,AList.Items[Index]);
    end;
  finally
    tmp.Free;
  end;
end;
{$ENDIF}

{$IFNDEF CLX}
procedure TTeeToolsGallery.LBToolDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var tmpTool : TTeeCustomTool;
begin
  tmpTool:=ToolAtIndex(Index).Create(nil);
  try
    TeeDrawTool(LBTool,Index,Rect,State,tmpTool);
  finally
    tmpTool.Free;
  end;
end;
{$ENDIF}

end.
