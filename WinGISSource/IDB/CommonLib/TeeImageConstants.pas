unit TeeImageConstants;

interface

resourcestring
  TeeMsg_AsJPEG        ='as &JPEG';
  TeeMsg_JPEGFilter    ='JPEG files (*.jpg)|*.jpg';
  TeeMsg_AsGIF         ='as &GIF';
  TeeMsg_GIFFilter     ='GIF files (*.gif)|*.gif';
  TeeMsg_AsPNG         ='as &PNG';
  TeeMsg_PNGFilter     ='PNG files (*.png)|*.png';
  TeeMsg_AsPCX         ='as PC&X';
  TeeMsg_PCXFilter     ='PCX files (*.pcx)|*.pcx';

implementation

end.
 