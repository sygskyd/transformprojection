{********************************************}
{     TeeChart Pro Charting Library          }
{ Copyright (c) 1995-2000 by David Berneda   }
{         All Rights Reserved                }
{********************************************}
{$I teedefs.inc}
unit TeeFuncEdit;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QButtons, QExtCtrls, QComCtrls,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls,
     {$ENDIF}
     TeeSelectList, TeEngine, TeeEdiPeri, TeeSourceEdit;

type
  TTeeFuncEditor = class(TBaseSourceEditor)
    PageControl1: TPageControl;
    TabSource: TTabSheet;
    TabOptions: TTabSheet;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure CBSourcesChange(Sender: TObject);
    procedure BApplyClick(Sender: TObject);
  private
    { Private declarations }
    IOptions : TFormPeriod;
    ISources : TSelectListForm;
    TheSeries: TChartSeries;
    Function FunctionClass:TTeeFunctionClass;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeeConst, Chart, TeePenDlg;

{ Helper functions }
procedure FillTeeFunctions(ACombo:TComboBox);
var t : Integer;
begin
  With ACombo.Items do
  begin
    BeginUpdate;
    Clear;
    Add(TeeMsg_FunctionNone);
    With TeeSeriesTypes do
    for t:=0 to Count-1 do
      With Items[t] do
      if Assigned(FunctionClass) and
         (not Assigned(SeriesClass)) and
         (IndexOfObject(TObject(FunctionClass))=-1) then
             AddObject(Description,TObject(FunctionClass));
    EndUpdate;
  end;
end;

procedure TTeeFuncEditor.FormCreate(Sender: TObject);
begin
  inherited;
  {$IFNDEF CLX}
  CBSources.Sorted:=True;
  {$ENDIF}
  FillTeeFunctions(CBSources);
end;

Function TTeeFuncEditor.FunctionClass:TTeeFunctionClass;
begin
  With CBSources do result:=TTeeFunctionClass(Items.Objects[ItemIndex]);
end;

procedure TTeeFuncEditor.FormShow(Sender: TObject);

  Procedure FillSources(AItems:TStrings; AddCurrent:Boolean);

    Procedure AddSource(AComponent:TComponent);
    begin
      if AddCurrent or (TheSeries.DataSources.IndexOf(AComponent)=-1) then
         if (AComponent is TChartSeries) and
            TheSeries.ParentChart.IsValidDataSource(TheSeries,AComponent) then
               AItems.AddObject(SeriesTitleOrName(TChartSeries(AComponent)),AComponent);
    end;

  var t : Integer;
  begin
    if Assigned(TheSeries.Owner) then
    With TheSeries.Owner do
         for t:=0 to ComponentCount-1 do AddSource(Components[t]);
  end;

var t : Integer;
begin
  inherited;
  PageControl1.ActivePage:=TabSource;
  TheSeries:=TChartSeries(Tag);

  ISources:=TSelectListForm.Create(Self);
  With ISources do
  begin
    FromList.Height:=FromList.Height-5;
    ToList.Height:=ToList.Height-5;
    Height:=Height-8;
    Align:=alClient;
    OnChange:=CBSourcesChange;
  end;
  AddFormTo(ISources,TabSource,Tag);

  With ISources do
  begin
    FromList.Items.Clear;
    FillSources(FromList.Items,False);
    With ToList do
    begin
      Items.BeginUpdate;
      Clear;
      With TheSeries do
      if Assigned(DataSource) then
      for t:=0 to DataSources.Count-1 do
        if TComponent(DataSources[t]) is TChartSeries then
           Items.AddObject(SeriesTitleOrName(TChartSeries(DataSources[t])),TChartSeries(DataSources[t]));
      Items.EndUpdate;
    end;
  end;

  With CBSources do
  if Assigned(TheSeries.FunctionType) then
     ItemIndex:=Items.IndexOfObject(TObject(TheSeries.FunctionType.ClassType))
  else
     ItemIndex:=Items.IndexOf(TeeMsg_FunctionNone);

  TabOptions.TabVisible:=FunctionClass<>nil;
  ISources.EnableButtons;
end;

type TSeriesAccess=class(TChartSeries);

procedure TTeeFuncEditor.PageControl1Change(Sender: TObject);
begin
  if (PageControl1.ActivePage=TabOptions) then
  begin
    if BApply.Enabled then BApplyClick(Self);
    if TabOptions.ControlCount=0 then
    begin
      IOptions:=TFormPeriod.Create(Self);
      IOptions.Caption:=CBSources.Text;
      IOptions.BApply:=BApply;
      AddFormTo(IOptions,TabOptions,Integer(TheSeries.FunctionType));
      BApply.Enabled:=False;
    end;
  end;
end;

procedure TTeeFuncEditor.CBSourcesChange(Sender: TObject);
begin
  inherited;
  TabOptions.TabVisible:=FunctionClass<>nil;
end;

procedure TTeeFuncEditor.BApplyClick(Sender: TObject);

  procedure DoApply;
  var t        : Integer;
      tmp      : TChartSeries;
      tmpClass : TTeeFunctionClass;
  begin
    { Set datasources... }
    if ISources.ToList.Items.Count=0 then TheSeries.DataSource:=nil
    else
    begin
      With ISources.ToList.Items do
         for t:=0 to Count-1 do TheSeries.CheckOtherSeries(TChartSeries(Objects[t]));
      With TheSeries.DataSources do
      begin
        for t:=0 to Count-1 do
            TSeriesAccess(Items[t]).RemoveLinkedSeries(TheSeries);
        Clear;
        With ISources.ToList.Items do
        for t:=0 to Count-1 do
        begin
          tmp:=TChartSeries(Objects[t]);
          TheSeries.DataSources.Add(tmp);
          TSeriesAccess(tmp).AddLinkedSeries(TheSeries);
        end;
      end;
    end;
    { Set function... }
    tmpClass:=FunctionClass;
    if Assigned(tmpClass) then
    begin
      if (not Assigned(TheSeries.FunctionType)) or
         (TheSeries.FunctionType.ClassType<>tmpClass) then
         CreateNewTeeFunction(TheSeries,tmpClass)
      else
         TheSeries.CheckDataSource;
    end
    else TheSeries.SetFunction(nil);
  end;

begin
  inherited;
  if Assigned(IOptions) then IOptions.Apply;
  DoApply;
  BApply.Enabled:=False;
end;

end.
