{**********************************************}
{   THistogramSeries Component Editor Dialog   }
{   Copyright (c) 2000 by David Berneda        }
{**********************************************}
{$I teedefs.inc}
unit TeeHistoEdit;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Chart, ExtCtrls, TeCanvas, TeePenDlg, StatChar;

type
  THistogramSeriesEditor = class(TForm)
    GroupBox1: TGroupBox;
    CBColorEach: TCheckBox;
    BAreaColor: TButtonColor;
    BAreaLinesPen: TButtonPen;
    BAreaLinePen: TButtonPen;
    Button1: TButton;
    procedure FormShow(Sender: TObject);
    procedure CBColorEachClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    TheSeries : THistogramSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeEngine, TeeBrushDlg, TeePoEdi, TeeProcs;

procedure THistogramSeriesEditor.FormShow(Sender: TObject);
begin
  TheSeries:=THistogramSeries(Tag);
  With TheSeries do
  begin
    CBColorEach.Checked   :=ColorEachPoint;
    BAreaLinesPen.LinkPen(LinesPen);
    BAreaLinePen.LinkPen(Pen);
  end;
  With BAreaColor do
  begin
    LinkProperty(TheSeries,'SeriesColor');
    Enabled:=not TheSeries.ColorEachPoint;
  end;
end;

procedure THistogramSeriesEditor.CBColorEachClick(Sender: TObject);
begin
  TheSeries.ColorEachPoint:=CBColorEach.Checked;
  BAreaColor.Enabled:=not TheSeries.ColorEachPoint;
end;

procedure THistogramSeriesEditor.Button1Click(Sender: TObject);
begin
  EditChartBrush(Self,TheSeries.Brush);
end;

initialization
  RegisterClass(THistogramSeriesEditor);
end.
