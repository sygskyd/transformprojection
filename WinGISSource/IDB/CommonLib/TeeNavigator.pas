{******************************************}
{   TCustomChartNavigator                  }
{   TChartPageNavigator Components         }
{   Copyright (c) 2000 by David Berneda    }
{   All Rights Reserved                    }
{******************************************}
{$I teedefs.inc}
unit TeeNavigator;

interface

Uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     {$IFDEF CLX}
     QExtCtrls, QControls, QButtons,
     {$ELSE}
     ExtCtrls, Controls, Buttons,
     {$ENDIF}
     Classes, TeeProcs, Chart;

type
  TChartNavGlyph = (ngEnabled, ngDisabled);
  TChartNavigateBtn = (nbFirst, nbPrior, nbNext, nbLast,
                      nbInsert, nbDelete, nbEdit, nbPost, nbCancel);

  TChartButtonSet = set of TChartNavigateBtn;

  TChartNavButton = class(TSpeedButton)
  private
    FIndex       : TChartNavigateBtn;
    FRepeatTimer : TTimer;
    procedure TimerExpired(Sender: TObject);
  protected
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure Paint; override;
  public
    Destructor Destroy; override;
    property Index : TChartNavigateBtn read FIndex write FIndex;
  end;

  TNotifyButtonClickedEvent=Procedure(Index:TChartNavigateBtn) of object;

  TCustomChartNavigator=class(TCustomPanel)
  private
    FHints        : TStrings;
    FDefHints     : TStrings;
    ButtonWidth   : Integer;
    MinBtnSize    : TPoint;
    FocusedButton : TChartNavigateBtn;
    FOnButtonClicked : TNotifyButtonClickedEvent;
    procedure BtnMouseDown (Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ClickHandler(Sender: TObject);
    procedure HintsChanged(Sender: TObject);
    procedure InitHints;
    procedure CheckSize;
    procedure SetSize(var W: Integer; var H: Integer);
    {$IFNDEF CLX}
    procedure WMSize(var Message: TWMSize);  message WM_SIZE;
    procedure WMSetFocus(var Message: TWMSetFocus); message WM_SETFOCUS;
    procedure WMKillFocus(var Message: TWMKillFocus); message WM_KILLFOCUS;
    procedure WMGetDlgCode(var Message: TWMGetDlgCode); message WM_GETDLGCODE;
    {$ENDIF}
    {$IFNDEF CLX}
    procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
    {$ENDIF}
  protected
    Buttons: Array[TChartNavigateBtn] of TChartNavButton;
    procedure BtnClick(Index: TChartNavigateBtn); dynamic;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure InitButtons; virtual;
    procedure Loaded; override;
    {$IFNDEF CLX}
    {$IFNDEF D4}
    { IUnknown }
    function QueryInterface(const IID: TGUID; out Obj): Integer; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    {$ENDIF}
    {$ENDIF}
  public
    Constructor Create(AOwner: TComponent); override;
    Destructor Destroy; override;
    procedure EnableButtons; virtual;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    property OnButtonClicked:TNotifyButtonClickedEvent read FOnButtonClicked
                                   write FOnButtonClicked;
  published
    { TPanel properties }
    property Align;
    property BorderStyle;
    property Color;
    {$IFNDEF CLX}
    {$IFDEF D4}
    {$IFNDEF TEEOCX}
    property UseDockManager default True;
    property DockSite;
    {$ENDIF}
    {$ENDIF}
    {$ENDIF}
    property DragMode;
    {$IFNDEF CLX}
    property DragCursor;
    {$ENDIF}
    property Enabled;
    property ParentColor;
    property ParentShowHint;
    {$IFNDEF TEEOCX}
    property PopupMenu;
    {$ENDIF}
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    {$IFDEF D4}
    property Anchors;
    {$IFNDEF TEEOCX}
    property Constraints;
    {$ENDIF}
    {$IFNDEF CLX}
    property DragKind;
    {$ENDIF}
    property Locked;
    {$ENDIF}

    { TPanel events }
    property OnClick;
    property OnDblClick;
    {$IFNDEF CLX}
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    {$ENDIF}
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    {$IFDEF D4}
    property OnMouseWheel;
    property OnMouseWheelDown;
    property OnMouseWheelUp;
    {$ENDIF}
    property OnResize;
    {$IFDEF D4}
    {$IFNDEF CLX}
    property OnCanResize;
    {$ENDIF}
    {$IFNDEF TEEOCX}
    property OnConstrainedResize;
    {$IFNDEF CLX}
    property OnDockDrop;
    property OnDockOver;
    property OnEndDock;
    property OnGetSiteInfo;
    property OnStartDock;
    property OnUnDock;
    {$ENDIF}
    {$ENDIF}
    {$ENDIF}
  end;

  TChartPageNavigator=class(TCustomChartNavigator,ITeeEventListener)
  private
    FChart : TCustomChart;
    procedure TeeEvent(Event: TTeeEvent);
    procedure SetChart(AChart:TCustomChart);
  protected
    procedure BtnClick(Index: TChartNavigateBtn); override;
    procedure InitButtons; override;
    procedure Notification( AComponent: TComponent;
                            Operation: TOperation); override;
  public
    Destructor Destroy; override;
    procedure EnableButtons; override;
  published
    property Chart:TCustomChart read FChart write SetChart;
    property OnButtonClicked;
  end;

implementation

Uses SysUtils, TeEngine,
     {$IFDEF CLX}
     QForms,
     {$ELSE}
     Forms,
     {$ENDIF}
     TeeConst;

Const
  InitRepeatPause = 400;  { pause before repeat timer (ms) }
  RepeatPause     = 100;  { pause before hint window displays (ms)}
  SpaceSize       =  5;   { size of space between special buttons }

  BtnTypeName: array[TChartNavigateBtn] of PChar = ('FIRST', 'PRIOR', 'NEXT',
    'LAST', 'INSERT', 'DELETE', 'EDIT', 'POST', 'CANCEL');
  BtnHintId: array[TChartNavigateBtn] of String = (
     TeeMsg_First,
     TeeMsg_Prior,
     TeeMsg_Next,
     TeeMsg_Last,
     TeeMsg_Insert,
     TeeMsg_Delete,
     TeeMsg_Edit,
     TeeMsg_Post,
     TeeMsg_Cancel);

{ TCustomChartNavigator }
Constructor TCustomChartNavigator.Create(AOwner: TComponent);
begin
  inherited;
  ControlStyle:=ControlStyle-[csAcceptsControls,csSetCaption] + [csOpaque];
  {$IFNDEF CLX}
  if not NewStyleControls then ControlStyle:=ControlStyle + [csFramed];
  {$ENDIF}
  FHints:=TStringList.Create;
  TStringList(FHints).OnChange:=HintsChanged;
  InitButtons;
  InitHints;
  BevelOuter:=bvNone;
  BevelInner:=bvNone;
  Width:=241;
  Height:=25;
  ButtonWidth:=0;
  FocusedButton:=nbFirst;
  FullRepaint:=False;
end;

Destructor TCustomChartNavigator.Destroy;
begin
  FDefHints.Free;
  FHints.Free;
  inherited;
end;

procedure TCustomChartNavigator.InitHints;
var I : Integer;
    J : TChartNavigateBtn;
begin
  if not Assigned(FDefHints) then
  begin
    FDefHints := TStringList.Create;
    for J := Low(Buttons) to High(Buttons) do
      FDefHints.Add(BtnHintId[J]);
  end;
  for J:=Low(Buttons) to High(Buttons) do Buttons[J].Hint:=FDefHints[Ord(J)];
  J := Low(Buttons);
  for I := 0 to (FHints.Count - 1) do
  begin
    if FHints.Strings[I] <> '' then Buttons[J].Hint := FHints.Strings[I];
    if J = High(Buttons) then Exit;
    Inc(J);
  end;
end;

procedure TCustomChartNavigator.HintsChanged(Sender: TObject);
begin
  InitHints;
end;

procedure TCustomChartNavigator.SetSize(var W: Integer; var H: Integer);
var Count  : Integer;
    MinW   : Integer;
    I      : TChartNavigateBtn;
    Space  : Integer;
    Temp   : Integer;
    Remain : Integer;
    X      : Integer;
    ButtonH: Integer;
begin
  if (csLoading in ComponentState) then Exit;
  if Buttons[nbFirst] = nil then Exit;

  Count := 0;
  for I := Low(Buttons) to High(Buttons) do
  begin
    if Buttons[I].Visible then
    begin
      Inc(Count);
    end;
  end;
  if Count = 0 then Inc(Count);

  MinW := Count * MinBtnSize.X;
  if W < MinW then W := MinW;
  if H < MinBtnSize.Y then H := MinBtnSize.Y;
  ButtonH:=H;
  if BorderStyle=bsSingle then Dec(ButtonH,4);

  ButtonWidth := W div Count;
  Temp := Count * ButtonWidth;
  if Align = alNone then W := Temp;

  X := 0;
  Remain := W - Temp;
  Temp := Count div 2;
  for I := Low(Buttons) to High(Buttons) do
  begin
    if Buttons[I].Visible then
    begin
      Space := 0;
      if Remain <> 0 then
      begin
        Dec(Temp, Remain);
        if Temp < 0 then
        begin
          Inc(Temp, Count);
          Space := 1;
        end;
      end;
      Buttons[I].SetBounds(X, 0, ButtonWidth + Space, ButtonH);
      Inc(X, ButtonWidth + Space);
    end
    else
      Buttons[I].SetBounds (Width + 1, 0, ButtonWidth, Height);
  end;
end;

procedure TCustomChartNavigator.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
var W : Integer;
    H : Integer;
begin
  W:=AWidth;
  H:=AHeight;
  if not HandleAllocated then SetSize(W,H);
  inherited SetBounds(ALeft,ATop,W,H);
end;

procedure TCustomChartNavigator.CheckSize;
var W : Integer;
    H : Integer;
begin
  { check for minimum size }
  W:=Width;
  H:=Height;
  SetSize(W,H);
  if (W<>Width) or (H<>Height) then inherited SetBounds(Left,Top,W,H);
end;

{$IFNDEF CLX}
procedure TCustomChartNavigator.WMSize(var Message: TWMSize);
begin
  inherited;
  CheckSize;
  Message.Result := 0;
end;
{$ENDIF}

procedure TCustomChartNavigator.BtnMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var OldFocus: TChartNavigateBtn;
begin
  OldFocus:=FocusedButton;
  FocusedButton:=TChartNavButton(Sender).Index;
  if TabStop {$IFNDEF CLX}and (GetFocus<>Handle){$ENDIF} and CanFocus then
  begin
    SetFocus;
    {$IFNDEF CLX}if (GetFocus<>Handle) then Exit;{$ENDIF}
  end
  else if TabStop {$IFNDEF CLX}and (GetFocus=Handle){$ENDIF} and (OldFocus<>FocusedButton) then
  begin
    Buttons[OldFocus].Invalidate;
    Buttons[FocusedButton].Invalidate;
  end;
end;

{$IFNDEF CLX}
procedure TCustomChartNavigator.WMSetFocus(var Message: TWMSetFocus);
begin
  Buttons[FocusedButton].Invalidate;
end;

procedure TCustomChartNavigator.WMKillFocus(var Message: TWMKillFocus);
begin
  Buttons[FocusedButton].Invalidate;
end;
{$ENDIF}

procedure TCustomChartNavigator.KeyDown(var Key: Word; Shift: TShiftState);
var NewFocus : TChartNavigateBtn;
    OldFocus : TChartNavigateBtn;
begin
  OldFocus:=FocusedButton;
  case Key of
    VK_RIGHT:
      begin
        NewFocus := FocusedButton;
        repeat
          if NewFocus < High(Buttons) then NewFocus := Succ(NewFocus);
        until (NewFocus = High(Buttons)) or (Buttons[NewFocus].Visible);
        if NewFocus <> FocusedButton then
        begin
          FocusedButton := NewFocus;
          Buttons[OldFocus].Invalidate;
          Buttons[FocusedButton].Invalidate;
        end;
      end;
    VK_LEFT:
      begin
        NewFocus := FocusedButton;
        repeat
          if NewFocus > Low(Buttons) then
            NewFocus := Pred(NewFocus);
        until (NewFocus = Low(Buttons)) or (Buttons[NewFocus].Visible);
        if NewFocus <> FocusedButton then
        begin
          FocusedButton := NewFocus;
          Buttons[OldFocus].Invalidate;
          Buttons[FocusedButton].Invalidate;
        end;
      end;
    VK_SPACE: if Buttons[FocusedButton].Enabled then
                 Buttons[FocusedButton].Click;
  end;
end;

{$IFNDEF CLX}
procedure TCustomChartNavigator.WMGetDlgCode(var Message: TWMGetDlgCode);
begin
  Message.Result:=DLGC_WANTARROWS;
end;
{$ENDIF}

procedure TCustomChartNavigator.ClickHandler(Sender: TObject);
begin
  BtnClick(TChartNavButton(Sender).Index);
end;

{$IFNDEF CLX}
procedure TCustomChartNavigator.CMEnabledChanged(var Message: TMessage);
begin
  inherited;
  if not (csLoading in ComponentState) then EnableButtons;
end;
{$ENDIF}

procedure TCustomChartNavigator.BtnClick(Index: TChartNavigateBtn);
begin
  EnableButtons;
  if Assigned(FOnButtonClicked) then FOnButtonClicked(Index);
end;

procedure TCustomChartNavigator.Loaded;
begin
  inherited;
  CheckSize;
  InitHints;
  EnableButtons;
end;

procedure TCustomChartNavigator.EnableButtons;
begin
end;

{$R TeeNavig.res}

procedure TCustomChartNavigator.InitButtons;
var I       : TChartNavigateBtn;
    X       : Integer;
    ResName : string;
begin
  MinBtnSize:=Point(20,18);
  X:=0;
  for I:=Low(Buttons) to High(Buttons) do
  begin
    Buttons[I]:=TChartNavButton.Create(Self);
    With Buttons[I] do
    begin
      Flat:=True;
      Index:=I;
      Visible:=True;
      Enabled:=False;
      SetBounds(X, 0, MinBtnSize.X, MinBtnSize.Y);
      FmtStr(ResName, 'TeeNav_%s', [BtnTypeName[I]]);
      Glyph.LoadFromResourceName(HInstance, ResName);
      NumGlyphs:=2;
      OnClick:=ClickHandler;
      OnMouseDown:=BtnMouseDown;
      Parent:=Self;
      Inc(X,MinBtnSize.X);
    end;
  end;
end;

{$IFNDEF D4}
function TCustomChartNavigator.QueryInterface(const IID: TGUID; out Obj): Integer; stdcall;
begin
  if VCLComObject = nil then
  begin
    if GetInterface(IID, Obj) then Result := S_OK
    else Result := E_NOINTERFACE
  end
  else
    Result := IVCLComObject(VCLComObject).QueryInterface(IID, Obj);
end;

function TCustomChartNavigator._AddRef: Integer; stdcall;
begin
  if VCLComObject = nil then
    Result := -1   // -1 indicates no reference counting is taking place
  else
    Result := IVCLComObject(VCLComObject)._AddRef;
end;

function TCustomChartNavigator._Release: Integer; stdcall;
begin
  if VCLComObject = nil then
    Result := -1   // -1 indicates no reference counting is taking place
  else
    Result := IVCLComObject(VCLComObject)._Release;
end;
{$ENDIF}

{ TChartNavButton }
Destructor TChartNavButton.Destroy;
begin
  FRepeatTimer.Free;
  inherited;
end;

procedure TChartNavButton.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited;
  if FRepeatTimer = nil then
     FRepeatTimer := TTimer.Create(Self);

  FRepeatTimer.OnTimer := TimerExpired;
  FRepeatTimer.Interval := InitRepeatPause;
  FRepeatTimer.Enabled  := True;
end;

procedure TChartNavButton.MouseUp(Button: TMouseButton; Shift: TShiftState;
                                  X, Y: Integer);
begin
  inherited;
  if Assigned(FRepeatTimer) then FRepeatTimer.Enabled:=False;
end;

procedure TChartNavButton.TimerExpired(Sender: TObject);
begin
  FRepeatTimer.Interval:=RepeatPause;
  if (FState=bsDown) and MouseCapture then
  begin
    try
      Click;
    except
      FRepeatTimer.Enabled:=False;
      raise;
    end;
  end;
end;

procedure TChartNavButton.Paint;
var R : TRect;
begin
  inherited;
  if {$IFNDEF CLX}(GetFocus=Parent.Handle) and{$ENDIF}
     (FIndex=TCustomChartNavigator (Parent).FocusedButton) then
  begin
    R:=Rect(3,3,Width-3,Height-3);
    if FState=bsDown then OffsetRect(R,1,1);
    {$IFDEF CLX}
    Canvas.DrawFocusRect(R);
    {$ELSE}
    DrawFocusRect(Canvas.Handle,R);
    {$ENDIF}
  end;
end;

{ TChartPageNavigator }
Destructor TChartPageNavigator.Destroy;
begin
  Chart:=nil;
  inherited;
end;

procedure TChartPageNavigator.BtnClick(Index: TChartNavigateBtn);
begin
  if Assigned(FChart) then
  with FChart do
  case Index of
    nbPrior : if Page>1 then Page:=Page-1;
    nbNext  : if Page<NumPages then Page:=Page+1;
    nbFirst : if Page>1 then Page:=1;
    nbLast  : if Page<NumPages then Page:=NumPages;
  end;
  EnableButtons;
  inherited;
end;

procedure TChartPageNavigator.EnableButtons;
begin
  inherited;
  if Assigned(FChart) then
  begin
    Buttons[nbFirst].Enabled:=FChart.Page>1;
    Buttons[nbPrior].Enabled:=Buttons[nbFirst].Enabled;
    Buttons[nbNext].Enabled:=FChart.Page<FChart.NumPages;
    Buttons[nbLast].Enabled:=Buttons[nbNext].Enabled;
  end;
end;

procedure TChartPageNavigator.InitButtons;
begin
  inherited;
  Buttons[nbInsert].Visible:=False;
  Buttons[nbDelete].Visible:=False;
  Buttons[nbEdit].Visible:=False;
  Buttons[nbPost].Visible:=False;
  Buttons[nbCancel].Visible:=False;
end;

procedure TChartPageNavigator.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if Operation=opRemove then
     if Assigned(FChart) and (AComponent=FChart) then Chart:=nil;
end;

type TTeePanelAccess=class(TCustomTeePanel);

procedure TChartPageNavigator.SetChart(AChart: TCustomChart);
begin
  if Assigned(FChart) then TTeePanelAccess(FChart).Listeners.Remove(Self);
  FChart:=AChart;
  if Assigned(FChart) then
  begin
    FChart.FreeNotification(Self);
    TTeePanelAccess(FChart).Listeners.Add(Self);
  end;
  EnableButtons;
end;

procedure TChartPageNavigator.TeeEvent(Event: TTeeEvent);
begin
  if (Event is TChartChangePage) or
     ( (Event is TTeeSeriesEvent) and
       (TTeeSeriesEvent(Event).Event=seDataChanged)
      ) then
        EnableButtons;
end;

end.
 