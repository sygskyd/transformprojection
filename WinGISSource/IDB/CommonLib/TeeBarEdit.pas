{**********************************************}
{   TBarSeries Component Editor Dialog         }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
Unit TeeBarEdit;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls, QComCtrls,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, ComCtrls,
     {$ENDIF}
     Chart, Series, TeCanvas, TeePenDlg, MultiLng;

type
  TBarSeriesEditor = class(TForm)
    SEBarwidth: TEdit;
    Label1: TLabel;
    CBBarStyle: TComboBox;
    LStyle: TLabel;
    BBarBrush: TButton;
    Label3: TLabel;
    SEBarOffset: TEdit;
    GroupBox1: TGroupBox;
    CBDarkBar: TCheckBox;
    CBBarSideMargins: TCheckBox;
    GroupBox2: TGroupBox;
    CBColorEach: TCheckBox;
    BBarColor: TButtonColor;
    CBMarksAutoPosition: TCheckBox;
    UDBarWidth: TUpDown;
    UDBarOffset: TUpDown;
    BGradient: TButton;
    BBarPen: TButtonPen;
    MlgSection1: TMlgSection;
    procedure FormShow(Sender: TObject);
    procedure SEBarwidthChange(Sender: TObject);
    procedure CBBarStyleChange(Sender: TObject);
    procedure BBarBrushClick(Sender: TObject);
    procedure CBColorEachClick(Sender: TObject);
    procedure CBDarkBarClick(Sender: TObject);
    procedure CBBarSideMarginsClick(Sender: TObject);
    procedure SEBarOffsetChange(Sender: TObject);
    procedure CBMarksAutoPositionClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BGradientClick(Sender: TObject);
  private
    { Private declarations }
    CreatingForm : Boolean;
    Bar          : TCustomBarSeries;
    Procedure CheckGradientButton;
    Procedure RefreshShape;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeeBrushDlg, TeeEdiGrad, TeeProcs, TeeStackBarEdit, TeeEdiSeri, TeeConst;

procedure TBarSeriesEditor.FormShow(Sender: TObject);
begin
  Bar:=TCustomBarSeries(Tag);
  With Bar do
  begin
    CBBarStyle.ItemIndex :=Ord(BarStyle);
    UDBarWidth.Position  :=BarWidthPercent;
    UDBarOffset.Position :=OffsetPercent;
    CBColorEach.Checked  :=ColorEachPoint;
    CBMarksAutoPosition.Checked:=AutoMarkPosition;
    CBDarkBar.Checked    :=Dark3D;
    CBBarSideMargins.Checked:=SideMargins;
    BBarPen.LinkPen(BarPen);
  end;
  CheckGradientButton;
  BBarColor.LinkProperty(Bar,'SeriesColor');
  RefreshShape;
  if Assigned(Parent) then 
  TFormTeeSeries(Parent.Owner).InsertSeriesForm( TStackBarSeriesEditor,
                                                 1,TeeMsg_Stack,
                                                 Bar);
  CreatingForm:=False;
end;

Procedure TBarSeriesEditor.RefreshShape;
Begin
  BBarColor.Enabled:=not Bar.ColorEachPoint;
  if BBarColor.Enabled then BBarColor.Repaint;
end;

procedure TBarSeriesEditor.SEBarWidthChange(Sender: TObject);
begin
  if not CreatingForm then Bar.BarWidthPercent:=UDBarWidth.Position;
end;

Procedure TBarSeriesEditor.CheckGradientButton;
begin
  BGradient.Enabled:=Bar.BarStyle=bsRectGradient;
end;

procedure TBarSeriesEditor.CBBarStyleChange(Sender: TObject);
begin
  if not CreatingForm then
  begin
    Bar.BarStyle:=TBarStyle(CBBarStyle.ItemIndex);
    CheckGradientButton;
  end;
end;

procedure TBarSeriesEditor.BBarBrushClick(Sender: TObject);
begin
  EditChartBrush(Self,Bar.BarBrush);
end;

procedure TBarSeriesEditor.CBColorEachClick(Sender: TObject);
begin
  if not CreatingForm then
  begin
    Bar.ColorEachPoint:=CBColorEach.Checked;
    RefreshShape;
  end;
end;

procedure TBarSeriesEditor.CBDarkBarClick(Sender: TObject);
begin
  if not CreatingForm then Bar.Dark3D:=CBDarkBar.Checked;
end;

procedure TBarSeriesEditor.CBBarSideMarginsClick(Sender: TObject);
begin
  if not CreatingForm then Bar.SideMargins:=CBBarSideMargins.Checked;
end;

procedure TBarSeriesEditor.SEBarOffsetChange(Sender: TObject);
begin
  if not CreatingForm then Bar.OffsetPercent:=UDBarOffset.Position;
end;

procedure TBarSeriesEditor.CBMarksAutoPositionClick(Sender: TObject);
begin
  Bar.AutoMarkPosition:=CBMarksAutoPosition.Checked;
end;

procedure TBarSeriesEditor.FormCreate(Sender: TObject);
begin
  CreatingForm:=True;
end;

procedure TBarSeriesEditor.BGradientClick(Sender: TObject);
begin
  {$IFNDEF D4}
  EditTeeGradientCustom(Self,Bar.Gradient,True);
  {$ELSE}
  EditTeeGradient(Self,Bar.Gradient,True);
  {$ENDIF}
end;

initialization
  RegisterClass(TBarSeriesEditor);
end.
