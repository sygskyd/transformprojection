unit TeeColorLineEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeeAxisToolEdit, StdCtrls, TeCanvas, TeeTools, TeePenDlg;

type
  TColorLineToolEditor = class(TAxisToolEditor)
    Label2: TLabel;
    EValue: TEdit;
    CBAllowDrag: TCheckBox;
    procedure EValueChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CBAllowDragClick(Sender: TObject);
  private
    { Private declarations }
    ColorLineTool : TColorLineTool;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TColorLineToolEditor.EValueChange(Sender: TObject);
var tmp : Double;
begin
  inherited;
  try
    tmp:=StrToFloat(EValue.Text);
  except
    exit;
  end;
  ColorLineTool.Value:=tmp;
end;

procedure TColorLineToolEditor.FormShow(Sender: TObject);
begin
  inherited;
  ColorLineTool:=TColorLineTool(Tag);
  EValue.Text:=FloatToStr(ColorLineTool.Value);
  CBAllowDrag.Checked:=ColorLineTool.AllowDrag;
end;

procedure TColorLineToolEditor.CBAllowDragClick(Sender: TObject);
begin
  ColorLineTool.AllowDrag:=CBAllowDrag.Checked
end;

initialization
  RegisterClass(TColorLineToolEditor);
end.
