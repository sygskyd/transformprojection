{**********************************************}
{   TDonutSeries                               }
{   Copyright (c) 1999-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeDonut;

interface

Uses Windows, SysUtils, Classes, Graphics, Teengine, Chart, TeCanvas, Series;

{  TDonutSeries ->  Pie Series with a hole in the center. }

Const TeeDefaultDonutPercent = 50;

Type TDonutSeries=class(TPieSeries)
     protected
       Procedure GalleryChanged3D(Is3D:Boolean); override;
       class Function GetEditorClass:String; override;
     public
       Constructor Create(AOwner: TComponent); override;
     published
       property DonutPercent default TeeDefaultDonutPercent;
     end;

implementation

Uses TeeConst,TeeProco;

{ TDonutSeries }
Constructor TDonutSeries.Create(AOwner: TComponent);
begin
  inherited;
  SetDonutPercent(TeeDefaultDonutPercent);
end;

Procedure TDonutSeries.GalleryChanged3D(Is3D:Boolean);
begin
  inherited;
  ParentChart.View3D:=False;
  Circled:=True;
end;

class Function TDonutSeries.GetEditorClass:String;
begin
  result:='TDonutSeriesEditor';
end;

initialization
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{
   RegisterTeeSeries(TDonutSeries,TeeMsg_GalleryDonut,TeeMsg_GalleryExtended,1);
}
   RegisterTeeSeries(TDonutSeries,TeeMsg_GalleryDonut,TeeMsg_GalleryExtended,1,stDonut);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
finalization
   UnRegisterTeeSeries([TDonutSeries]);
end.
