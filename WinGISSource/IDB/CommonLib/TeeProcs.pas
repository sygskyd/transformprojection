{******************************************}
{   Base types and Procedures              }
{ Copyright (c) 1995-2000 by David Berneda }
{        All Rights Reserved               }
{******************************************}
{$I teedefs.inc}
unit TeeProcs;

interface

Uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     {$IFDEF CLX}
     Qt, QClipbrd, QGraphics, QStdCtrls, QExtCtrls, QControls, QForms,
     {$ELSE}
     Printers, Clipbrd, ExtCtrls, Controls, Graphics, Forms,
     {$IFNDEF D6}
     Buttons,
     {$ENDIF}
     {$ENDIF}
     Classes, SysUtils, TeCanvas
     {$IFDEF D6}
     , Types
     {$ENDIF}
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
     ,Objects
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
     ;

{$IFDEF CLX}
type
  TMetafile=class(TBitmap)
  private
    FEnhanced: Boolean;
  published
    property Enhanced:Boolean read FEnhanced write FEnhanced;
  end;

  TMetafileCanvas=class(TCanvas)
  public
    Constructor Create(Meta:TMetafile; Ref:Integer);
  end;

  TPrinterOrientation = (poPortrait, poLandscape);

  TPrinters=TStrings;

  TPrinter=Object
    Orientation : TPrinterOrientation;
    Canvas      : TCanvas;
    Title       : String;
    Printing    : Boolean;
    Handle      : TTeeCanvasHandle;
    PageWidth   : Integer;
    PageHeight  : Integer;
    Printers    : TPrinters;
    PrinterIndex: Integer;
    Procedure BeginDoc;
    Procedure EndDoc;
    Procedure Abort;
    Procedure NewPage;
  end;

Var Printer:TPrinter;
{$ENDIF}

Const
   TeeDefVerticalMargin = 4;
   TeeDefHorizMargin    = 3;

   crTeeHand              = TCursor(2020); { Hand cursor }
   TeeMsg_TeeHand         = 'crTeeHand'; { string cursor name (dont translate) }

   TeeNormalPrintDetail   =    0;
   TeeHighPrintDetail     = -100;

   TeeDefault_PrintMargin =   15;
   TeeDef3DPercent        =   15;

Var
  TeeCheckPenWidth      : Boolean=True;  { for HP Laserjet printers... }
  TeeClipWhenPrinting   : Boolean=True;  { Apply clipping when printing }
  TeeClipWhenMetafiling : Boolean=True;  { Apply clipping when creating metafiles }
  TeeEraseBack          : Boolean=False; { erase background before repainting panel }
  { Should Panel background to be printed ? Default: False }
  PrintTeePanel         : Boolean=False;

type
  TDateTimeStep=(  dtOneMillisecond,
                   dtOneSecond,
                   dtFiveSeconds,
                   dtTenSeconds,
                   dtFifteenSeconds,
                   dtThirtySeconds,
                   dtOneMinute,
                   dtFiveMinutes,
                   dtTenMinutes,
                   dtFifteenMinutes,
                   dtThirtyMinutes,
                   dtOneHour,
                   dtTwoHours,
                   dtSixHours,
                   dtTwelveHours,
                   dtOneDay,
                   dtTwoDays,
                   dtThreeDays,
                   dtOneWeek,
                   dtHalfMonth,
                   dtOneMonth,
                   dtTwoMonths,
                   dtThreeMonths,
                   dtFourMonths,
                   dtSixMonths,
                   dtOneYear,
                   dtNone );

Const
    DateTimeStep:Array[TDateTimeStep] of Double=
      ( 1.0/(1000.0*86400.0), 1.0/86400.0,  5.0/86400.0,  10.0/86400.0,
        0.25/1440.0,  0.5/1440.0,    1.0/1440.0,
        5.0/1440.0,  10.0/1440.0,    0.25/24.0,
        0.5/24.0 ,    1.0/24.0 ,  2.0/24.0 ,  6.0/24.0 ,
        12.0/24.0,    1, 2, 3, 7, 15, 30, 60, 90, 120, 182, 365, {none:} 1
      );

type
  TCustomPanelNoCaption=class(TCustomPanel)
  public
    Constructor Create(AOwner: TComponent); override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TComponent);
    Procedure   Store(var S: TOldStream);
    Procedure   Assign(Source: TPersistent); override;
    Function    OnCompareItems(Item: TComponent): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
  end;

  TCustomTeePanel=class;

  TZoomPanning=class(TPersistent)
  private
    FActive     : Boolean;
  public
    X0,Y0,X1,Y1 : Integer;
    Procedure Check;
    Procedure Activate(x,y:Integer);
    
    property Active:Boolean read FActive write FActive;
  end;

  TTeeEvent=class
  public
    Sender : TCustomTeePanel;
  end;

  ITeeEventListener=interface
    procedure TeeEvent(Event:TTeeEvent);
  end;

  TTeeEventListeners=class(TList)
  private
    Function Get(Index:Integer):ITeeEventListener;
  public
    function Add(Const Item: ITeeEventListener): Integer;
    function Remove(Item: ITeeEventListener): Integer;
    property Items[Index:Integer]:ITeeEventListener read Get; default;
  end;

  TTeeMouseEventKind=(meDown,meUp,meMove);

  TTeeMouseEvent=class(TTeeEvent)
  public
    Event  : TTeeMouseEventKind;
    Button : TMouseButton;
    Shift  : TShiftState;
    X      : Integer;
    Y      : Integer;
  end;

  TTeeView3DEvent=class(TTeeEvent);

  TCustomTeePanel=class(TCustomPanelNoCaption)
  private
    F3DPercent        : Integer;
    FApplyZOrder      : Boolean;
    FAutoRepaint      : Boolean;  { when False, it does not refresh }
    FCancelMouse      : Boolean;  { when True, it does not finish mouse events }
    FChartBounds      : TRect;
    FChartWidth       : Integer;
    FChartHeight      : Integer;
    FChartXCenter     : Integer;
    FChartYCenter     : Integer;
    FDelphiCanvas     : TCanvas;
    FHeight3D         : Integer;
    FMargins          : TRect;
    FOriginalCursor   : TCursor;
    FPanning          : TZoomPanning;
    FPrinting         : Boolean;
    FPrintMargins     : TRect;
    FPrintProportional: Boolean;
    FPrintResolution  : Integer;
    FView3D           : Boolean;
    FView3DOptions    : TView3DOptions;
    FWidth3D          : Integer;

    IEventListeners   : TTeeEventListeners;

    Procedure BroadcastMouseEvent(Kind:TTeeMouseEventKind;
                                  Button: TMouseButton;
                                  Shift: TShiftState; X, Y: Integer);
    Function GetBufferedDisplay:Boolean;
    Function GetMargin(Index:Integer):Integer;
    Function GetMonochrome:Boolean;
    Procedure Set3DPercent(Value:Integer);
    Procedure SetBufferedDisplay(Value:Boolean);
    Procedure SetMargin(Index,Value:Integer);
    Procedure SetMonochrome(Value:Boolean);
    procedure SetView3D(Value:Boolean);
    procedure SetView3DOptions(Value:TView3DOptions);
  protected
    InternalCanvas : TCanvas3D;

    procedure AssignTo(Dest: TPersistent); override;
    Function BroadcastTeeEvent(Event:TTeeEvent):TTeeEvent;
    {$IFNDEF CLX}
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    procedure CMSysColorChange(var Message: TMessage); message CM_SYSCOLORCHANGE;
    procedure CreateParams(var Params: TCreateParams); override;
    {$ELSE}
    procedure MouseLeave(AControl: TControl); override;
    {$ENDIF}
    Function GetBackColor:TColor; virtual;
    Procedure InternalDraw(Const UserRectangle:TRect); virtual; abstract;
    property Listeners:TTeeEventListeners read IEventListeners;
    Procedure Loaded; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure Paint; override;
    Procedure SetBooleanProperty(Var Variable:Boolean; Value:Boolean);
    Procedure SetColorProperty(Var Variable:TColor; Value:TColor);
    Procedure SetDoubleProperty(Var Variable:Double; Const Value:Double);
    Procedure SetIntegerProperty(Var Variable:Integer; Value:Integer);
    Procedure SetStringProperty(Var Variable:String; Const Value:String);
    {$IFNDEF CLX}
    procedure WMEraseBkgnd(var Message: TWmEraseBkgnd); message WM_ERASEBKGND;
    procedure WMGetDlgCode(var Message: TWMGetDlgCode); message WM_GETDLGCODE;
    {$ENDIF}
  public
    ChartRect    : TRect;    { the rectangle bounded by axes in pixels }

    Constructor Create(AOwner: TComponent); override;
    Destructor  Destroy; override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TComponent);
    Procedure   Store(var S: TOldStream);
    Function    OnCompareItems(Item: TComponent): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    Procedure Assign(Source: TPersistent); override;
    Procedure CalcMetaBounds( Var R:TRect; Const AChartRect:TRect;
                              Var WinWidth,WinHeight,ViewWidth,ViewHeight:Integer);
    Function CalcProportionalMargins:TRect;
    Function CanClip:Boolean;
    Procedure CanvasChanged(Sender:TObject); virtual;
    Function ChartPrintRect:TRect;
    Procedure CheckPenWidth(APen:TPen);
    Procedure CopyToClipboardBitmap; {$IFDEF D4}overload;{$ENDIF}
    {$IFDEF D4}
    Procedure CopyToClipboardBitmap(Const R:TRect); overload;
    {$ELSE}
    Procedure CopyToClipboardBitmapRect(Const R:TRect);
    {$ENDIF}
    Procedure CopyToClipboardMetafile(Enhanced:Boolean); {$IFDEF D4}overload;{$ENDIF}
    {$IFDEF D4}
    Procedure CopyToClipboardMetafile(Enhanced:Boolean; Const R:TRect); overload;
    {$ELSE}
    Procedure CopyToClipboardMetafileRect(Enhanced:Boolean; Const R:TRect);
    {$ENDIF}
    Function TeeCreateBitmap(ABackColor:TColor; Const Rect:TRect
                             {$IFNDEF CLX}
                             {$IFDEF D4}; APixelFormat:TPixelFormat=pfDevice{$ENDIF}
                             {$ENDIF}):TBitmap;
    Procedure Draw(UserCanvas:TCanvas; Const UserRect:TRect); virtual;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Procedure DrawDirectly(ABitmapDC: HDC; Const UserRect:TRect);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    Procedure DrawPanelBevels(Rect:TRect); dynamic;
    Procedure DrawToMetaCanvas(ACanvas:TCanvas; Const Rect:TRect);

    Function GetCursorPos:TPoint;
    Function GetRectangle:TRect; virtual;
    procedure Invalidate; override;
    Function IsScreenHighColor:Boolean;

    Procedure Print;
    Procedure PrintLandscape;
    Procedure PrintOrientation(AOrientation:TPrinterOrientation);
    Procedure PrintPartial(Const PrinterRect:TRect);
    Procedure PrintPartialCanvas( PrintCanvas:TCanvas;
                                  Const PrinterRect:TRect);
    Procedure PrintPortrait;
    Procedure PrintRect(Const R:TRect);
    Procedure SaveToBitmapFile(Const FileName:String); {$IFDEF D4} overload; {$ENDIF}
    {$IFDEF D4}
    Procedure SaveToBitmapFile(Const FileName:String; Const R:TRect); overload;
    {$ELSE}
    Procedure SaveToBitmapFileRect(Const FileName:String; Const R:TRect);
    {$ENDIF}
    Procedure SaveToMetafile(Const FileName:String);
    Procedure SaveToMetafileEnh(Const FileName:String);
    Procedure SaveToMetafileRect( Enhanced:Boolean; Const FileName:String;
                                  Const Rect:TRect );
    Procedure SetBrushCanvas( AColor:TColor; ABrush:TChartBrush;
                              ABackColor:TColor);
    Procedure SetInternalCanvas(NewCanvas:TCanvas3D);
    Procedure ReCalcWidthHeight;
    Function  TeeCreateMetafile(Enhanced:Boolean; Const Rect:TRect):TMetafile;

    { public properties }
    property ApplyZOrder:Boolean read FApplyZOrder write FApplyZOrder;
    property AutoRepaint:Boolean read FAutoRepaint write FAutoRepaint;  { when False, it does not refresh }
    property BufferedDisplay:Boolean read GetBufferedDisplay
                                     write SetBufferedDisplay;
    property CancelMouse:Boolean read FCancelMouse write FCancelMouse; { when True, it does not finish mouse events }
    property Canvas:TCanvas3D read InternalCanvas write SetInternalCanvas;
    property ChartBounds:TRect read FChartBounds;
    property ChartHeight:Integer read FChartHeight;
    property ChartWidth:Integer read FChartWidth;
    property ChartXCenter:Integer read FChartXCenter;
    property ChartYCenter:Integer read FChartYCenter;
    property DelphiCanvas:TCanvas read FDelphiCanvas;
    property Height3D:Integer read FHeight3D write FHeight3D;
    property IPanning:TZoomPanning read FPanning;
    property OriginalCursor:TCursor read FOriginalCursor write FOriginalCursor;
    property Printing:Boolean read FPrinting write FPrinting;
    property Width3D:Integer read FWidth3D write FWidth3D;

    property PrintResolution:Integer read FPrintResolution
                                     write FPrintResolution default TeeNormalPrintDetail;
    { to be published properties }
    property Chart3DPercent:Integer read F3DPercent write Set3DPercent
                                    default TeeDef3DPercent;
    property MarginLeft:Integer   index 0 read GetMargin write SetMargin default TeeDefHorizMargin;
    property MarginTop:Integer    index 1 read GetMargin write SetMargin default TeeDefVerticalMargin;
    property MarginRight:Integer  index 2 read GetMargin write SetMargin default TeeDefHorizMargin;
    property MarginBottom:Integer index 3 read GetMargin write SetMargin default TeeDefVerticalMargin;
    property Monochrome:Boolean read GetMonochrome write SetMonochrome default False;
    property PrintMargins:TRect read FPrintMargins write FPrintMargins;    { the percent of paper printer margins }
    property PrintProportional:Boolean read FPrintProportional
                                       write FPrintProportional default True;
    property View3D:Boolean read FView3D write SetView3D default True;
    property View3DOptions:TView3DOptions read FView3DOptions write SetView3DOptions;

    { TPanel properties }
    property Align;
    property BevelInner;
    property BevelOuter;
    property BevelWidth;
    property BorderWidth;
    property BorderStyle;
    property Color;
    {$IFNDEF CLX}
    property DragCursor;
    {$ENDIF}
    property DragMode;
    property Enabled;
    property ParentColor;
    property ParentShowHint;
    {$IFNDEF TEEOCX}
    property PopupMenu;
    {$ENDIF}
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;

    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
  published
  end;

  TCustomTeeGradient=class(TPersistent)
  private
    FDirection : TGradientDirection;
    FEndColor  : TColor;
    FMidColor  : TColor;
    FStartColor: TColor;
    FVisible   : Boolean;

    IChanged   : TNotifyEvent;
    Procedure SetColorProperty(Var AColor:TColor; Value:TColor);
    Procedure SetDirection(Value:TGradientDirection);
    Procedure SetEndColor(Value:TColor);
    Procedure SetMidColor(Value:TColor);
    Procedure SetStartColor(Value:TColor);
    Procedure SetVisible(Value:Boolean);
  public
    Constructor Create(ChangedEvent: TNotifyEvent); virtual;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; ChangedEvent: TNotifyEvent);
    Procedure   Store(var S: TOldStream);
    Function    OnCompareItems(Item: TPersistent): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    Procedure Assign(Source:TPersistent); override;
    Procedure Draw(Canvas:TTeeCanvas; Const Rect:TRect; RoundRectSize:Integer{$IFDEF D4}=0{$ENDIF});
    property Changed:TNotifyEvent read IChanged;
    { to be published }
    property Direction:TGradientDirection read FDirection write SetDirection default gdTopBottom;
    property EndColor:TColor read FEndColor write SetEndColor default clYellow;
    property MidColor:TColor read FMidColor write SetMidColor default clNone;
    property StartColor:TColor read FStartColor write SetStartColor default clWhite;
    property Visible:Boolean read FVisible write SetVisible default False;
  end;

  TChartGradient=class(TCustomTeeGradient)
  published
    property Direction;
    property EndColor;
    property MidColor;
    property StartColor;
    property Visible;
  end;

  TChartGradientClass=class of TChartGradient;

  TPanningMode=(pmNone,pmHorizontal,pmVertical,pmBoth);

  TTeeBackImageMode=(pbmStretch,pbmTile,pbmCenter);

  TTeeZoomPen=class(TChartPen)
  published
    property Color default clWhite;
  end;

  TTeeZoomBrush=class(TChartBrush)
  published
    property Color default clWhite;
    property Style default bsClear;
  end;

  TTeeZoomDirection=(tzdHorizontal, tzdVertical, tzdBoth);

  TTeeZoom=class(TZoomPanning)
  private
    FAllow         : Boolean;
    FAnimated      : Boolean;
    FAnimatedSteps : Integer;
    FBrush         : TTeeZoomBrush;
    FDirection     : TTeeZoomDirection;
    FKeyShift      : TShiftState;
    FMinimumPixels : Integer;
    FMouseButton   : TMouseButton;
    FPen           : TTeeZoomPen;
    Procedure SetBrush(Value:TTeeZoomBrush);
    Procedure SetPen(Value:TTeeZoomPen);
  public
    Constructor Create;
    Destructor Destroy; override;
    Procedure Assign(Source:TPersistent); override;
  published
    property Allow:Boolean read FAllow write FAllow default True;
    property Animated:Boolean read FAnimated write FAnimated default False;
    property AnimatedSteps:Integer read FAnimatedSteps write FAnimatedSteps default 8;
    property Brush:TTeeZoomBrush read FBrush write SetBrush;
    property Direction:TTeeZoomDirection read FDirection write FDirection default tzdBoth;
    property KeyShift:TShiftState read FKeyShift write FKeyShift default [];
    property MinimumPixels:Integer read FMinimumPixels write FMinimumPixels default 16;
    property MouseButton:TMouseButton read FMouseButton write FMouseButton default mbLeft;
    property Pen:TTeeZoomPen read FPen write SetPen;
  end;

  TCustomTeePanelExtended=class(TCustomTeePanel)
  private
    FAllowPanning      : TPanningMode;
    FBackImage         : TPicture;
    FBackImageInside   : Boolean;
    FBackImageMode     : TTeeBackImageMode;
    FGradient          : TChartGradient;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    FTransparent       : Boolean;     // not completely realised in this class - used in child classes
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    FZoom              : TTeeZoom;
    FZoomed            : Boolean;

    { for compatibility with Tee4 }
    Function  GetAllowZoom: Boolean;
    Function  GetAnimatedZoom: Boolean;
    Function  GetAnimatedZoomSteps: Integer;
    procedure ReadAnimatedZoomSteps(Reader: TReader);
    procedure ReadAnimatedZoom(Reader: TReader);
    procedure ReadAllowZoom(Reader: TReader);
    procedure ReadPrintMargins(Reader: TReader);
    procedure SavePrintMargins(Writer: TWriter);
    Procedure SetAllowZoom(Value: Boolean);
    Procedure SetAnimatedZoom(Value: Boolean);
    Procedure SetAnimatedZoomSteps(Value: Integer);

    procedure SetBackImage(Value: TPicture);
    procedure SetBackImageInside(Value: Boolean);
    procedure SetBackImageMode(Value: TTeeBackImageMode);
    Procedure SetGradient(Value: TChartGradient);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Procedure SetTransparent(Value: Boolean);
    Procedure OnBackImageChange(Sender: TObject);
    Procedure OnGradientChange(Sender: TObject);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    Procedure SetZoom(Value: TTeeZoom);
  protected
    FOnAfterDraw : TNotifyEvent;
    FOnScroll    : TNotifyEvent;
    FOnUndoZoom  : TNotifyEvent;
    FOnZoom      : TNotifyEvent;
    Procedure DefineProperties(Filer:TFiler); override;
    procedure DrawBitmap(Rect:TRect; Z:Integer);
    procedure FillPanelRect(Const Rect:TRect); virtual;
    {$IFNDEF CLX}
    function GetPalette: HPALETTE; override;    { override the method }
    {$ENDIF}
    procedure PanelPaint(Const UserRect:TRect); virtual;
  public
    Constructor Create(AOwner:TComponent); override;
    Destructor  Destroy; override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TComponent);
    Procedure   Store(var S: TOldStream);
    Function    OnCompareItems(Item: TComponent): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    Procedure Assign(Source: TPersistent); override;
    Procedure DrawZoomRectangle;
    procedure UndoZoom; dynamic;

    property Zoomed:Boolean read FZoomed write FZoomed;

    property AllowPanning:TPanningMode read FAllowPanning
				       write FAllowPanning default pmBoth;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    // not completely realised in this class - used in child classes
    property Transparent: Boolean read FTransparent write SetTransparent;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    { for compatibility with Tee4 }
    property AllowZoom:Boolean read GetAllowZoom write SetAllowZoom default True;
    property AnimatedZoom:Boolean read GetAnimatedZoom
				  write SetAnimatedZoom default False;
    property AnimatedZoomSteps:Integer read GetAnimatedZoomSteps
				       write SetAnimatedZoomSteps default 8;
    {}
    
    property BackImage:TPicture read FBackImage write SetBackImage;
    property BackImageInside:Boolean read FBackImageInside
				     write SetBackImageInside default False;
    property BackImageMode:TTeeBackImageMode read FBackImageMode
					     write SetBackImageMode
					     default pbmStretch;
    property Gradient:TChartGradient read FGradient write SetGradient;
    property Zoom:TTeeZoom read FZoom write SetZoom;

    { events }
    property OnAfterDraw:TNotifyEvent read FOnAfterDraw write FOnAfterDraw;
    property OnScroll:TNotifyEvent read FOnScroll write FOnScroll;
    property OnUndoZoom:TNotifyEvent read FOnUndoZoom write FOnUndoZoom;
    property OnZoom:TNotifyEvent read FOnZoom write FOnZoom;
  end;

  TChartBrushClass=class of TChartBrush;

  TTeeCustomShapeBrushPen=class(TPersistent)
  private
    FBrush   : TChartBrush;
    FPen     : TChartPen;
    FVisible : Boolean;
    Procedure SetBrush(Value:TChartBrush);
    Procedure SetPen(Value:TChartPen);
    Procedure SetVisible(Value:Boolean);
  protected
    Procedure CanvasChanged(Sender:TObject);
    Function GetBrushClass: TChartBrushClass; dynamic;
  public
    ParentChart : TCustomTeePanel;
    Constructor Create;
    Destructor  Destroy; override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream);
    Procedure   Store(var S: TOldStream);
    Function    OnCompareItems(Item: TPersistent): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    Procedure Assign(Source:TPersistent); override;
    Procedure Repaint;

    property Brush:TChartBrush read FBrush write SetBrush;
    property Frame:TChartPen read FPen write SetPen;
    property Pen:TChartPen read FPen write SetPen;
    property Visible:Boolean read FVisible write SetVisible;
  end;

  TChartObjectShapeStyle=(fosRectangle,fosRoundRectangle);

  TTeeCustomShape=class(TTeeCustomShapeBrushPen)
  private
    FBevel        : TPanelBevel;
    FBevelWidth   : TBevelWidth;
    FColor        : TColor;        
    FFont         : TTeeFont;      
    FGradient     : TChartGradient;
    FShadowColor  : TColor;        
    FShadowSize   : Integer;
    FShapeStyle   : TChartObjectShapeStyle;
    FTransparency : TTeeTransparency;
    FTransparent  : Boolean;
    Procedure SetBevel(Value:TPanelBevel);
    procedure SetBevelWidth(Value: TBevelWidth);
    Procedure SetColor(Value:TColor);
    Procedure SetFont(Value:TTeeFont);
    procedure SetGradient(Value:TChartGradient);
    Procedure SetShadowColor(Value:TColor);
    Procedure SetShadowSize(Value:Integer);
    Procedure SetShapeStyle(Value:TChartObjectShapeStyle);
    procedure SetTransparency(Value:TTeeTransparency);
    procedure SetTransparent(Value:Boolean);
  protected
    Function GetGradientClass:TChartGradientClass; dynamic;
    property Transparency:TTeeTransparency read FTransparency
                                           write SetTransparency default 0;
  public
    ShapeBounds : TRect;

    Constructor Create(AOwner: TCustomTeePanel);
    Destructor  Destroy; override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TCustomTeePanel);
    Procedure   Store(var S: TOldStream);
    Function    OnCompareItems(Item: TPersistent): Boolean;
    Function    CreateDeterminant(Determinant: String): String;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    Procedure Assign(Source:TPersistent); override;
    Procedure Draw;
    Procedure DrawRectRotated(Const Rect:TRect; Angle:Integer; AZ:Integer);

    { to be published }
    property Bevel:TPanelBevel read FBevel write SetBevel default bvNone;
    property BevelWidth:TBevelWidth read FBevelWidth write SetBevelWidth default 2;
    property Color:TColor read FColor write SetColor default clWhite;
    property Font:TTeeFont read FFont write SetFont;
    property Gradient:TChartGradient read FGradient write SetGradient;
    property ShadowColor:TColor read FShadowColor write SetShadowColor default clBlack;
    property ShadowSize:Integer read FShadowSize write SetShadowSize default 3;
    property ShapeStyle:TChartObjectShapeStyle read FShapeStyle
                                   write SetShapeStyle default fosRectangle;
    property Transparent:Boolean read FTransparent write SetTransparent default False;
  end;

  TTeeShape=class(TTeeCustomShape)
  published
    property Bevel;
    property BevelWidth;
    property Color;
    property Font;
    property Gradient;
    property ShadowColor;
    property ShadowSize;
    property ShapeStyle;
    property Transparent;
  end;

Function TeeStr(Num:Integer):String; // same as IntToStr but a bit faster

{ returns the appropiate string date or time format according to "step" }
Function DateTimeDefaultFormat(Const AStep:Double):String;

{ returns the number of days of month-year }
Function DaysInMonth(Year,Month:Word):Word;

{ Given a "step", return the corresponding set element }
Function FindDateTimeStep(Const StepValue:Double):TDateTimeStep;

{ Returns the next "step" in ascending order. (eg: TwoDays follows OneDay }
Function NextDateTimeStep(Const AStep:Double):Double;

{ Returns True if point T is over line:  P --> Q }
Function PointInLine(Const P:TPoint; px,py,qx,qy:Integer):Boolean;

{ Returns True if point T is over (more or less "Tolerance" pixels)
  line:  P --> Q }
Function PointInLineTolerance(Const P:TPoint; px,py,qx,qy,TolerancePixels:Integer):Boolean;

{ Returns True if point P is inside Poly polygon }
Function PointInPolygon(Const P:TPoint; Const Poly:Array of TPoint):Boolean;

{ Returns True if point P is inside the vert triangle of x0y0, midxY1, x1y0 }
Function PointInTriangle(Const P:TPoint; X0,X1,Y0,Y1:Integer):Boolean;

{ Returns True if point P is inside the horiz triangle of x0y0, x1midY, x0y0 }
Function PointInHorizTriangle(Const P:TPoint; Y0,Y1,X0,X1:Integer):Boolean;

{ Returns True if point P is inside the ellipse bounded by Rect }
Function PointInEllipse(Const P:TPoint; Const Rect:TRect):Boolean;

{ This functions try to solve locale problems with formatting numbers }
Function DelphiToLocalFormat(Const Format:String):String;
Function LocalToDelphiFormat(Const Format:String):String;

{ For all controls in the array, set the Enabled property }
Procedure EnableControls(Enable:Boolean; Const ControlArray:Array of TControl);

{ Round "ADate" to the nearest "AStep" value }
Function TeeRoundDate(Const ADate:TDateTime; AStep:TDateTimeStep):TDateTime;

{ Increment or Decrement "Value", for DateTime and not-DateTime }
Procedure TeeDateTimeIncrement( IsDateTime:Boolean;
                                Increment:Boolean;
                                Var Value:Double;
                                Const AnIncrement:Double;
                                tmpWhichDateTime:TDateTimeStep);


{ Generic "QuickSort" sorting algorithm }
type TTeeSortCompare=Function(a,b:Integer):Integer of object;
     TTeeSortSwap=Procedure(a,b:Integer) of object;

Procedure TeeSort( StartIndex,EndIndex:Integer;
                   CompareFunc:TTeeSortCompare;
                   SwapFunc:TTeeSortSwap);

{ Returns a valid component name }
Function TeeGetUniqueName(AOwner:TComponent; Const AStartName:String):TComponentName;

{ Delimited field routines }
Const TeeFieldsSeparator:String=';';

{ Returns the "index" item in string, using "TeeFieldsSeparator" character }
Function TeeExtractField(St:String; Index:Integer):String;
{ Returns the number of fields in string, using "TeeFieldsSeparator" character }
Function TeeNumFields(St:String):Integer;

{ Routines to support the custom "crTeeHand" cursor type }
Function TeeIdentToCursor(Const AName:String; Var ACursor:Integer):Boolean;
Function TeeCursorToIdent(ACursor:Integer; Var AName:String):Boolean;

{ Try to find a resource bitmap and load it }
Function TryFindResource(AInstance:Integer; Const ResName:String; ABitmap:TBitmap):Boolean;

{ returns one of the sample colors in ColorPalette constant array }
Const MaxDefaultColors=20;
Function GetDefaultColor(Const Index:Integer):TColor;

Const ColorPalette:Array[1..MaxDefaultColors] of TColor=
        ( clRed,
          clGreen,
          clYellow,
          clBlue,
          clWhite,
          clGray,
          clFuchsia,
          clTeal,
          clNavy,
          clMaroon,
          clLime,
          clOlive,
          clPurple,
          clSilver,
          clAqua,
          clBlack,
          clMoneyGreen,
          clSkyBlue,
          clCream,
          clMedGray
          );

implementation

Uses {$IFNDEF D5}
     {$IFNDEF TEEOCX}
     DsgnIntf,
     {$ENDIF}
     {$ENDIF}
     TeeConst
     {$IFDEF TEETRIAL}
     , TeeAbout
     {$ENDIF}
     ;

{.$DEFINE MONITOR_REDRAWS}

{$IFDEF MONITOR_REDRAWS}
Const RedrawCount:Integer=0;
{$ENDIF}

{$R TeeResou.res}

{$IFDEF CLX}
Const
  LOGPIXELSX = 0;
  LOGPIXELSY = 1;
{$ENDIF}

{ Same as IntToStr but faster }
Function TeeStr(Num:Integer):String;
begin
  Str(Num,Result);
end;

{ Returns one of the sample colors in ColorPalette constant array }
Function GetDefaultColor(Const Index:Integer):TColor;
Begin
  result:=ColorPalette[1+(Index mod MaxDefaultColors)];
end;

{$IFDEF D5}
Function DaysInMonth(Year,Month:Word):Word;
begin
  result:=MonthDays[IsLeapYear(Year),Month]
end;
{$ELSE}
Function DaysInMonth(Year,Month:Word):Word;
Const DaysMonths:Array[1..12] of Byte=(31,28,31,30,31,30,31,31,30,31,30,31);
Begin
  result:=DaysMonths[Month];
  if (Month=2) and
  {$IFDEF D4}
     IsLeapYear(Year)
  {$ELSE}
     (Year mod 4=0) and
     ( (Year mod 100<>0) or (Year mod 400=0) )
  {$ENDIF}
     then Inc(result);
End;
{$ENDIF}

Function DateTimeDefaultFormat(Const AStep:Double):String;
Begin
  if AStep<=1 then result:=ShortTimeFormat
              else result:=ShortDateFormat;
end;

Function NextDateTimeStep(Const AStep:Double):Double;
var t : TDateTimeStep;
Begin
  for t:=Pred(dtOneYear) downto dtOneMilliSecond do
  if AStep>=DateTimeStep[t] then
  Begin
    result:=DateTimeStep[Succ(t)];
    exit;
  end;
  result:=DateTimeStep[dtOneYear];
end;

Function FindDateTimeStep(Const StepValue:Double):TDateTimeStep;
var t : TDateTimeStep;
begin
  for t:=Pred(High(DateTimeStep)) downto Low(DateTimeStep) do
  begin
    if Abs(DateTimeStep[t]-StepValue)<DateTimeStep[dtOneMilliSecond] then
    begin
      result:=t;
      exit;
    end;
  end;
  result:=dtNone;
end;

{ TCustomPanelNoCaption }
Constructor TCustomPanelNoCaption.Create(AOwner: TComponent);
begin
   inherited;
   ControlStyle:=ControlStyle-[csSetCaption];
end;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TCustomPanelNoCaption.Load(var S: TOldStream; AOwner: TComponent);
var intValue  : Integer;
    setValue  : TControlStyle;
    aliValue  : TAlignment;
    bevValue  : TPanelBevel;
    bevwValue : TBevelWidth;
    bdstValue : TBorderStyle;
    bdwValue  : TBorderWidth;
    colValue  : TColor;
    boolValue : Boolean;
begin
   inherited Create(AOwner);
   S.Read(setValue,SizeOf(TControlStyle));
   ControlStyle:=setValue;
   S.Read(intValue,SizeOf(Integer));
   Width:=intValue;
   S.Read(intValue,SizeOf(Integer));
   Height:=intValue;
   S.Read(intValue,SizeOf(Integer));
   Left:=intValue;
   S.Read(intValue,SizeOf(Integer));
   Top:=intValue;

   S.Read(aliValue,SizeOf(TAlignment));
   Alignment:=aliValue;
   S.Read(bevValue,SizeOf(TPanelBevel));
   BevelInner:=bevValue;
   S.Read(bevValue,SizeOf(TPanelBevel));
   BevelOuter:=bevValue;
   S.Read(bevwValue,SizeOf(TBevelWidth));
   BevelWidth:=bevwValue;
   S.Read(bdstValue,SizeOf(TBorderStyle));
   BorderStyle:=bdstValue;
   S.Read(bdwValue,SizeOf(TBorderWidth));
   BorderWidth:=bdwValue;
   S.Read(colValue,SizeOf(Color));
   Color:=colValue;
   S.Read(boolValue,SizeOf(Boolean));
   FullRepaint:=boolValue;
   S.Read(boolValue,SizeOf(Boolean));
   Locked:=boolValue;
   S.Read(boolValue,SizeOf(Boolean));
   ParentColor:=boolValue;
   S.Read(boolValue,SizeOf(Boolean));
   UseDockManager:=boolValue;
end;

Procedure TCustomPanelNoCaption.Store(var S: TOldStream);
begin
   S.Write(ControlStyle,SizeOf(ControlStyle));
   S.Write(Width,SizeOf(Width));
   S.Write(Height,SizeOf(Height));
   S.Write(Left,SizeOf(Integer));
   S.Write(Top,SizeOf(Integer));

   S.Write(Alignment,SizeOf(Alignment));
   S.Write(BevelInner,SizeOf(BevelInner));
   S.Write(BevelOuter,SizeOf(BevelOuter));
   S.Write(BevelWidth,SizeOf(BevelWidth));
   S.Write(BorderStyle,SizeOf(BorderStyle));
   S.Write(BorderWidth,SizeOf(BorderWidth));
   S.Write(Color,SizeOf(Color));
   S.Write(FullRepaint,SizeOf(Boolean));
   S.Write(Locked,SizeOf(Boolean));
   S.Write(ParentColor,SizeOf(Boolean));
   S.Write(UseDockManager,SizeOf(Boolean));
end;

Procedure TCustomPanelNoCaption.Assign(Source: TPersistent);
begin
   if Source is TCustomPanelNoCaption then
      with TCustomPanelNoCaption(Source) do
         begin
            Self.ControlStyle   := ControlStyle;
            Self.Width          := Width;
            Self.Height         := Height;
            Self.Left           := Left;
            Self.Top            := Top;
            Self.Alignment      := Alignment;
            Self.BevelInner     := BevelInner;
            Self.BevelOuter     := BevelOuter;
            Self.BevelWidth     := BevelWidth;
            Self.BorderStyle    := BorderStyle;
            Self.BorderWidth    := BorderWidth;
            Self.Color          := Color;
            Self.FullRepaint    := FullRepaint;
            Self.Locked         := Locked;
            Self.ParentColor    := ParentColor;
            Self.UseDockManager := UseDockManager;
         end
   else
      inherited Assign(Source);
end;

Function TCustomPanelNoCaption.OnCompareItems(Item: TComponent): Boolean;
var Diff1      : Integer;
    Diff2      : Integer;
    PermisDiff : Integer;
begin
   Result:=FALSE;
   if Item is TCustomPanelNoCaption then
      with TCustomPanelNoCaption(Item) do
         if Self.ControlStyle = ControlStyle then
            if Self.Alignment = Alignment then
               if Self.BevelInner = BevelInner then
                  if Self.BevelOuter = BevelOuter then
                     if Self.BevelWidth = BevelWidth then
                        if Self.BorderStyle = BorderStyle then
                           if Self.BorderWidth = BorderWidth then
                              if Self.Color = Color then
                                 if Self.FullRepaint = FullRepaint then
                                    if Self.Locked = Locked then
                                       if Self.ParentColor = ParentColor then
                                          if Self.UseDockManager = UseDockManager then
                                             begin
                                                Diff1:=Abs(Self.Width-Width);
                                                Diff2:=Abs(Self.Height-Height);
                                                if (Diff1 > 1) or (Diff2 > 1) then
                                                   begin
                                                      PermisDiff:=Self.Width;
                                                      if Self.Height > PermisDiff then
                                                         PermisDiff:=Self.Height;
                                                      PermisDiff:=Round(PermisDiff*co_PermisPercent);
                                                      if (Diff1 <= PermisDiff) and (Diff2 <= PermisDiff) then
                                                         Result:=TRUE;
                                                   end
                                                else
                                                   Result:=TRUE;
                                             end;
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{ TCustomTeePanel }
Constructor TCustomTeePanel.Create(AOwner: TComponent);
begin
  inherited;
  IEventListeners:=TTeeEventListeners.Create;

  Width := 400;
  Height:= 250;
  F3DPercent   :=TeeDef3DPercent;
  FApplyZOrder :=True;
  FDelphiCanvas:=inherited Canvas;
  FView3D      :=True;
  FView3DOptions:=TView3DOptions.Create(Self);
  InternalCanvas:=TTeeCanvas3D.Create;
  InternalCanvas.ReferenceCanvas:=FDelphiCanvas;
  FMargins:= {$IFDEF D6}Types.{$ENDIF}Rect( TeeDefHorizMargin,TeeDefVerticalMargin,
                   TeeDefHorizMargin,TeeDefVerticalMargin);
  FPrintProportional:=True;
  FPrintResolution:=TeeNormalPrintDetail;
  PrintMargins:={$IFDEF D6}Types.{$ENDIF}Rect(TeeDefault_PrintMargin,TeeDefault_PrintMargin,
                     TeeDefault_PrintMargin,TeeDefault_PrintMargin);
  FOriginalCursor:=Cursor;
  FPanning:=TZoomPanning.Create;
  if TeeEraseBack then TeeEraseBack:=not (csDesigning in ComponentState);
  AutoRepaint:=True;
  {$IFDEF CLX}
  QWidget_setBackgroundMode(Handle,QWidgetBackgroundMode_NoBackground);
  {$ENDIF}
end;

Destructor TCustomTeePanel.Destroy;
Begin
  FreeAndNil(InternalCanvas);
  FView3DOptions.Free;
  FPanning.Free;
  IEventListeners.Free;
  inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TCustomTeePanel.Load(var S: TOldStream; AOwner: TComponent);
begin
   inherited;
   IEventListeners:=TTeeEventListeners.Create;

   S.Read(F3DPercent,SizeOf(F3DPercent));
   S.Read(FApplyZOrder,SizeOf(Boolean));
   FDelphiCanvas:=inherited Canvas;
   S.Read(FView3D,SizeOf(Boolean));
   FView3DOptions:=TView3DOptions.Load(S,Self);

   InternalCanvas:=TTeeCanvas3D.Create;
   InternalCanvas.ReferenceCanvas:=FDelphiCanvas;

   S.Read(FMargins,SizeOf(FMargins));
   S.Read(FPrintProportional,SizeOf(Boolean));
   S.Read(FPrintResolution,SizeOf(FPrintResolution));
   S.Read(FPrintMargins,SizeOf(FPrintMargins));

   FOriginalCursor:=Cursor;
   FPanning:=TZoomPanning.Create;

   if TeeEraseBack then
      TeeEraseBack:=not (csDesigning in ComponentState);

   AutoRepaint:=FALSE;
end;

Procedure TCustomTeePanel.Store(var S: TOldStream);
begin
   inherited Store(S);
   S.Write(F3DPercent,SizeOf(F3DPercent));
   S.Write(FApplyZOrder,SizeOf(Boolean));
   S.Write(FView3D,SizeOf(Boolean));
   FView3DOptions.Store(S);
   S.Write(FMargins,SizeOf(FMargins));
   S.Write(FPrintProportional,SizeOf(Boolean));
   S.Write(FPrintResolution,SizeOf(FPrintResolution));
   S.Write(FPrintMargins,SizeOf(FPrintMargins));
end;

Function TCustomTeePanel.OnCompareItems(Item: TComponent): Boolean;
begin
   Result:=FALSE;
   if Item is TCustomTeePanel then
      with TCustomTeePanel(Item) do
         if Self.FView3D = FView3D then
            if Self.FApplyZOrder = FApplyZOrder then
               if EqualRect(Self.FMargins,FMargins) then
                  if Self.FView3D then
                     begin
                        if Self.F3DPercent = F3DPercent then
                           if Self.FView3DOptions.OnCompareItems(FView3DOptions) then
                              Result := inherited OnCompareItems(Item);
                     end
                  else
                     Result := inherited OnCompareItems(Item);
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TCustomTeePanel.CanvasChanged(Sender:TObject);
Begin
  Invalidate;
end;

{$IFNDEF CLX}
procedure TCustomTeePanel.CreateParams(var Params: TCreateParams);
begin
  inherited;
  InternalCanvas.View3DOptions:=nil;
end;
{$ENDIF}

procedure TCustomTeePanel.SetView3DOptions(Value:TView3DOptions);
begin
  FView3DOptions.Assign(Value);
end;

procedure TCustomTeePanel.SetView3D(Value:Boolean);
Begin
  SetBooleanProperty(FView3D,Value);
  BroadcastTeeEvent(TTeeView3DEvent.Create).Free;
end;

Procedure TCustomTeePanel.Draw(UserCanvas:TCanvas; Const UserRect:TRect);
Var tmpW : Integer;
    tmpH : Integer;
Begin
  FChartBounds:=InternalCanvas.InitWindow(UserCanvas,FView3DOptions,Color,FView3D,UserRect);
  RectSize(FChartBounds,tmpW,tmpH);
  With FChartBounds do
       ChartRect:={$IFDEF D6}Types.{$ENDIF}Rect( Left  + BorderWidth + MulDiv(MarginLeft,tmpW,100),
                        Top   + BorderWidth + MulDiv(MarginTop,tmpH,100),
                        Right - BorderWidth - MulDiv(MarginRight,tmpW,100),
                        Bottom- BorderWidth - MulDiv(MarginBottom,tmpH,100) );
  RecalcWidthHeight;
  InternalDraw(FChartBounds);

{$IFDEF MONITOR_REDRAWS}
  Inc(RedrawCount);
  InternalCanvas.TextAlign:=TA_LEFT;
  InternalCanvas.Font.Size:=8;
  InternalCanvas.TextOut(0,0,TeeStr(RedrawCount));
{$ENDIF}
  InternalCanvas.ShowImage(UserCanvas,FDelphiCanvas,UserRect);
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Procedure TCustomTeePanel.DrawDirectly(ABitmapDC: HDC; Const UserRect:TRect);
var tmpW    : Integer;
    tmpH    : Integer;
    BmpRect : TRect;
begin
   BmpRect:=UserRect;
   LPtoDP(ABitmapDC,BmpRect,2);
   BmpRect.Right:=Abs(BmpRect.Right-BmpRect.Left);
   BmpRect.Left:=0;
   BmpRect.Bottom:=Abs(BmpRect.Bottom-BmpRect.Top);
   BmpRect.Top:=0;
   FChartBounds:=InternalCanvas.InitWindow(FDelphiCanvas,FView3DOptions,Color,FView3D,BmpRect);
   RectSize(FChartBounds,tmpW,tmpH);
   with FChartBounds do
      ChartRect:={$IFDEF D6}Types.{$ENDIF}Rect( Left  + BorderWidth + MulDiv(MarginLeft,tmpW,100),
                         Top   + BorderWidth + MulDiv(MarginTop,tmpH,100),
                         Right - BorderWidth - MulDiv(MarginRight,tmpW,100),
                         Bottom- BorderWidth - MulDiv(MarginBottom,tmpH,100) );
   RecalcWidthHeight;
   InternalDraw(FChartBounds);

   InternalCanvas.DrawImage(ABitmapDC,UserRect);
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

procedure TCustomTeePanel.Paint;

  {$IFDEF TEEOCX}
  procedure TeeFpuInit;
  asm
    FNINIT
    FWAIT
    FLDCW   Default8087CW
  end;
  {$ENDIF}

begin
  {$IFDEF TEEOCX}
  TeeFPUInit;
  {$ENDIF}
  if (not FPrinting) and (not InternalCanvas.ReDrawBitmap) then
     Draw(FDelphiCanvas,GetClientRect);
end;

Function TCustomTeePanel.TeeCreateMetafile( Enhanced:Boolean; Const Rect:TRect ):TMetafile;
var tmpCanvas : TMetafileCanvas;
begin
  result:=TMetafile.Create;
  { bug in Delphi 3.02 : graphics.pas metafile reduces width/height.
    Fixed in Delphi 4.0x and BCB4.  }
  result.Width :=MaxLong(1,(Rect.Right-Rect.Left)-1);
  result.Height:=MaxLong(1,(Rect.Bottom-Rect.Top)-1);
  result.Enhanced:=Enhanced;
  tmpCanvas:=TMetafileCanvas.Create(result,0);
  try
    DrawToMetaCanvas(tmpCanvas,Rect);
  finally
    tmpCanvas.Free;
  end;
end;

Procedure TCustomTeePanel.SetBrushCanvas( AColor:TColor; ABrush:TChartBrush;
                                          ABackColor:TColor);
begin
  if (ABrush.Style<>bsSolid) and (AColor=ABackColor) then
     if ABackColor=clBlack then AColor:=clWhite else AColor:=clBlack;
  Canvas.AssignBrushColor(ABrush,AColor,ABackColor);
end;

Function TeeGetDeviceCaps(Handle:TTeeCanvasHandle; Cap:Integer):Integer;
begin
  {$IFDEF CLX}
  result:=1;
  {$ELSE}
  result:=GetDeviceCaps(Handle,Cap);
  {$ENDIF}
end;

Function TCustomTeePanel.IsScreenHighColor:Boolean;
Begin
  {$IFNDEF CLX}
  With InternalCanvas do
    result:= SupportsFullRotation
             or
             (TeeGetDeviceCaps(Handle,BITSPIXEL)*
              TeeGetDeviceCaps(Handle,PLANES)>=15);
  {$ELSE}
  result:=True;
  {$ENDIF}
End;

Function TCustomTeePanel.CanClip:Boolean;
begin
  result:=(not Canvas.SupportsFullrotation) and
          (
            ((not Printing) and (not Canvas.Metafiling)) or
            (Printing and TeeClipWhenPrinting) or
            (Canvas.Metafiling and TeeClipWhenMetafiling)
          )
end;

Procedure TCustomTeePanel.Set3DPercent(Value:Integer);
Const Max3DPercent = 100;
      Min3DPercent = 1;
Begin
  if (Value<Min3DPercent) or (Value>Max3DPercent) then
     Raise Exception.CreateFmt(TeeMsg_3DPercent,[Min3DPercent,Max3DPercent])
  else
    SetIntegerProperty(F3DPercent,Value);
end;

Procedure TCustomTeePanel.SetStringProperty(Var Variable:String; Const Value:String);
Begin
  if Variable<>Value then
  begin
    Variable:=Value; Invalidate;
  end;
end;

Procedure TCustomTeePanel.SetDoubleProperty(Var Variable:Double; Const Value:Double);
begin
  if Variable<>Value then
  begin
    Variable:=Value; Invalidate;
  end;
end;

Procedure TCustomTeePanel.SetColorProperty(Var Variable:TColor; Value:TColor);
Begin
  if Variable<>Value then
  begin
    Variable:=Value; Invalidate;
  end;
end;

Procedure TCustomTeePanel.SetBooleanProperty(Var Variable:Boolean; Value:Boolean);
Begin
  if Variable<>Value then
  begin
    Variable:=Value; Invalidate;
  end;
end;

Procedure TCustomTeePanel.SetIntegerProperty(Var Variable:Integer; Value:Integer);
Begin
  if Variable<>Value then
  begin
    Variable:=Value; Invalidate;
  end;
end;

Function TCustomTeePanel.GetBackColor:TColor;
begin
  result:=Color;
end;

Procedure TCustomTeePanel.Loaded;
begin
  inherited;
  FOriginalCursor:=Cursor;
end;

{$IFDEF CLX}
procedure TCustomTeePanel.MouseLeave(AControl: TControl);
{$ELSE}
procedure TCustomTeePanel.CMMouseLeave(var Message: TMessage);
{$ENDIF}
begin
  Cursor:=FOriginalCursor;
  FPanning.Active:=False;
  inherited;
end;

{$IFNDEF CLX}
procedure TCustomTeePanel.CMSysColorChange(var Message: TMessage);
begin
  inherited;
  Invalidate;
end;

procedure TCustomTeePanel.WMGetDlgCode(var Message: TWMGetDlgCode);
begin
  inherited;
  Message.Result:=Message.Result or DLGC_WANTARROWS;
end;

procedure TCustomTeePanel.WMEraseBkgnd(var Message: TWmEraseBkgnd);
Begin
  if TeeEraseBack then Inherited;
  Message.Result:=1;
End;
{$ENDIF}

procedure TCustomTeePanel.Invalidate;
begin
  if AutoRepaint then
     begin
        if Assigned(InternalCanvas) then
           InternalCanvas.Invalidate;
        inherited;
     end;
end;

procedure TCustomTeePanel.AssignTo(Dest: TPersistent);
var tmp : TBitmap;
begin
  if Dest is TPicture then
  begin
    tmp:=TeeCreateBitmap(Color,ClientRect);
    try
      Dest.Assign(tmp);
    finally
      tmp.Free;
    end;
  end
  else inherited;
end;

Function TCustomTeePanel.BroadcastTeeEvent(Event:TTeeEvent):TTeeEvent;
var t   : Integer;
    tmp : ITeeEventListener;
begin
  result:=Event;
  if not (csDestroying in ComponentState) then
  begin
    Event.Sender:=Self;
    for t:=0 to Listeners.Count-1 do
    begin
      tmp:=Listeners[t];
      tmp.TeeEvent(Event);
      if CancelMouse then break;
    end;
  end;
end;

procedure TCustomTeePanel.BroadcastMouseEvent(Kind:TTeeMouseEventKind;
                                           Button: TMouseButton;
                                           Shift: TShiftState; X, Y: Integer);
var tmp : TTeeMouseEvent;
begin
  tmp:=TTeeMouseEvent.Create;
  try
    tmp.Event:=Kind;
    tmp.Button:=Button;
    tmp.Shift:=Shift;
    tmp.X:=X;
    tmp.Y:=Y;
    BroadcastTeeEvent(tmp);
  finally
    tmp.Free;
  end;
end;

procedure TCustomTeePanel.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CancelMouse:=False;
  inherited;
  BroadcastMouseEvent(meDown,Button,Shift,X,Y);
end;

procedure TCustomTeePanel.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  CancelMouse:=False;
  inherited;
  BroadcastMouseEvent(meUp,Button,Shift,X,Y);
end;

procedure TCustomTeePanel.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  CancelMouse:=False;
  inherited;
  if not (csDesigning in ComponentState) then
     BroadcastMouseEvent(meMove,mbLeft,Shift,X,Y);
end;

type TRectArray=array[0..3] of Integer;

Function TCustomTeePanel.GetMargin(Index:Integer):Integer;
Begin
  result:=TRectArray(FMargins)[Index];
end;

Procedure TCustomTeePanel.SetMargin(Index,Value:Integer);
Begin
  SetIntegerProperty(TRectArray(FMargins)[Index],Value);
end;

Function TCustomTeePanel.GetBufferedDisplay:Boolean;
begin
  result:=InternalCanvas.UseBuffer;
end;

Procedure TCustomTeePanel.SetBufferedDisplay(Value:Boolean);
begin
  InternalCanvas.UseBuffer:=Value;
end;

Procedure TCustomTeePanel.SetInternalCanvas(NewCanvas:TCanvas3D);
begin
  if Assigned(NewCanvas) then
     begin
       NewCanvas.ReferenceCanvas:=FDelphiCanvas;
       if Assigned(InternalCanvas) then
          begin
            AutoRepaint:=False;
            NewCanvas.Assign(InternalCanvas);
            AutoRepaint:=True;
            InternalCanvas.Free;
          end;
       InternalCanvas:=NewCanvas;
       Repaint;
     end;
end;

procedure TCustomTeePanel.RecalcWidthHeight;
Begin
  With ChartRect do
  begin
    if Left<FChartBounds.Left then Left:=FChartBounds.Left;
    if Top<FChartBounds.Top then Top:=FChartBounds.Top;
    if Right<Left then Right:=Left+1 else
    if Right=Left then Right:=Left+Width;
    if Bottom<Top then Bottom:=Top+1 else
    if Bottom=Top then Bottom:=Top+Height;
    FChartWidth  :=Right-Left;
    FChartHeight :=Bottom-Top;
  end;
  RectCenter(ChartRect,FChartXCenter,FChartYCenter);
end;

Function TCustomTeePanel.GetCursorPos:TPoint;
{$IFNDEF D4}
Var P : TPoint;
{$ENDIF}
Begin
  {$IFNDEF D4}
  Windows.GetCursorPos(P);
  result:=ScreenToClient(P);
  {$ELSE}
  result:=ScreenToClient(Mouse.CursorPos);
  {$ENDIF}
end;

Function TCustomTeePanel.GetRectangle:TRect;
begin
  result:=GetClientRect;
end;

Procedure TCustomTeePanel.DrawToMetaCanvas(ACanvas:TCanvas; Const Rect:TRect);
Var Old : Boolean;
begin  { assume the acanvas is in MM_ANISOTROPIC mode }
  InternalCanvas.Metafiling:=True;
  Old:=BufferedDisplay;
  BufferedDisplay:=False;
  try
    Draw(ACanvas,Rect);
  finally
    BufferedDisplay:=Old;
    InternalCanvas.Metafiling:=False;
  end;
end;

Function TCustomTeePanel.GetMonochrome:Boolean;
Begin
  result:=InternalCanvas.Monochrome;
end;

Procedure TCustomTeePanel.SetMonochrome(Value:Boolean);
Begin
  InternalCanvas.Monochrome:=Value;
end;

Procedure TCustomTeePanel.Assign(Source: TPersistent);
begin
   if Source is TCustomTeePanel then
      with TCustomTeePanel(Source) do
         begin
           Self.BufferedDisplay    := BufferedDisplay;
           Self.PrintMargins       := PrintMargins;
           Self.FPrintProportional := FPrintProportional;
           Self.FPrintResolution   := FPrintResolution;
           Self.FMargins           := FMargins;
           Self.Monochrome         := Monochrome;
           Self.FView3D            := FView3D;
           Self.View3DOptions      := FView3DOptions;
           Self.F3DPercent         := F3DPercent;
           Self.Color              := Color;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
           inherited Assign(Source);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
         end
   else
      inherited Assign(Source);
end;

Procedure TCustomTeePanel.SaveToMetafile(Const FileName:String);
begin
  SaveToMetaFileRect(False,FileName,GetRectangle);
end;

Procedure TCustomTeePanel.SaveToMetafileEnh(Const FileName:String);
begin
  SaveToMetaFileRect(True,FileName,GetRectangle);
end;

{ Enhanced:Boolean }
Procedure TCustomTeePanel.SaveToMetafileRect( Enhanced:Boolean;
                                              Const FileName:String;
                                              Const Rect:TRect);
Var tmpStream : TFileStream;
    Meta      : TMetafile;
Begin
  Meta:=TeeCreateMetafile(Enhanced,Rect);
  With Meta do
  try
    tmpStream:=TFileStream.Create(FileName,fmCreate);
    try
      SaveToStream(tmpStream);
    finally
      tmpStream.Free;
    end;
  finally
    Free;
  end;
End;

Function TCustomTeePanel.TeeCreateBitmap(ABackColor:TColor; Const Rect:TRect
                                         {$IFNDEF CLX}
                                         {$IFDEF D4}; APixelFormat:TPixelFormat=pfDevice{$ENDIF}
                                         {$ENDIF}):TBitmap;
var Old : Boolean;
begin
  result:=TBitmap.Create;
  With result do
  begin
    {$IFNDEF CLX}
    {$IFDEF D4}
    PixelFormat:=APixelFormat;
    {$ENDIF}
    {$ENDIF}
    Width :=Rect.Right-Rect.Left;
    Height:=Rect.Bottom-Rect.Top;
    if ABackColor<>clWhite then
    begin
      Canvas.Brush.Color:=ABackColor;
      Canvas.FillRect(Rect);
    end;
    Old:=BufferedDisplay;
    BufferedDisplay:=False;
    Self.Draw(Canvas,Rect);
    BufferedDisplay:=Old;
  end;
end;

Procedure TCustomTeePanel.SaveToBitmapFile(Const FileName:String);
Begin
{$IFDEF D4}SaveToBitmapFile{$ELSE}SaveToBitmapFileRect{$ENDIF}(FileName,GetRectangle);
End;

{$IFDEF D4}
Procedure TCustomTeePanel.SaveToBitmapFile(Const FileName:String; Const R:TRect);
{$ELSE}
Procedure TCustomTeePanel.SaveToBitmapFileRect(Const FileName:String; Const R:TRect);
{$ENDIF}
begin
  With TeeCreateBitmap(clWhite,R) do
  try
    SaveToFile(FileName);
  finally
    Free;
  end;
end;

Procedure TCustomTeePanel.PrintPortrait;
Begin
  PrintOrientation(poPortrait);
end;

Procedure TCustomTeePanel.PrintLandscape;
Begin
  PrintOrientation(poLandscape);
end;

Procedure TCustomTeePanel.PrintOrientation(AOrientation:TPrinterOrientation);
Var OldOrientation : TPrinterOrientation;
Begin
  OldOrientation:=Printer.Orientation;
  Printer.Orientation:=AOrientation;
  try
    Print;
  finally
    Printer.Orientation:=OldOrientation;
  end;
end;

{$IFDEF D4}
Procedure TCustomTeePanel.CopyToClipboardBitmap(Const R:TRect);
{$ELSE}
Procedure TCustomTeePanel.CopyToClipboardBitmapRect(Const R:TRect);
{$ENDIF}
var tmpBitmap : TBitmap;
begin
  tmpBitmap:=TeeCreateBitmap(clWhite,R);
  try
    ClipBoard.Assign(tmpBitmap);
  finally
    tmpBitmap.Free;
  end;
end;

Procedure TCustomTeePanel.CopyToClipboardBitmap;
begin
{$IFDEF D4}CopyToClipboardBitmap{$ELSE}CopyToClipboardBitmapRect{$ENDIF}(GetRectangle);
end;

{$IFDEF D4}
Procedure TCustomTeePanel.CopyToClipboardMetafile( Enhanced:Boolean;
                                                   Const R:TRect);
{$ELSE}
Procedure TCustomTeePanel.CopyToClipboardMetafileRect( Enhanced:Boolean;
                                                       Const R:TRect);
{$ENDIF}
Var tmpMeta : TMetaFile;
begin
  tmpMeta:=TeeCreateMetafile(Enhanced,R);
  try
    ClipBoard.Assign(tmpMeta);
  finally
    tmpMeta.Free;
  end;
end;

Procedure TCustomTeePanel.CopyToClipboardMetafile(Enhanced:Boolean);
begin
  {$IFDEF D4}CopyToClipboardMetafile{$ELSE}CopyToClipboardMetafileRect{$ENDIF}(Enhanced,GetRectangle);
end;

Procedure TCustomTeePanel.CalcMetaBounds( Var R:TRect;
                                          Const AChartRect:TRect;
                                          Var WinWidth,WinHeight,
                                          ViewWidth,ViewHeight:Integer);
Var tmpRectWidth  : Integer;
    tmpRectHeight : Integer;
begin  { apply PrintResolution to the desired rectangle coordinates }
  RectSize(R,ViewWidth,ViewHeight);
  RectSize(AChartRect,tmpRectWidth,tmpRectHeight);
  WinWidth :=tmpRectWidth -MulDiv(tmpRectWidth, PrintResolution,100);
  WinHeight:=tmpRectHeight-MulDiv(tmpRectHeight,PrintResolution,100);
  With R do
  begin
    Left  :=MulDiv(Left  ,WinWidth,ViewWidth);
    Right :=MulDiv(Right ,WinWidth,ViewWidth);
    Top   :=MulDiv(Top   ,WinHeight,ViewHeight);
    Bottom:=MulDiv(Bottom,WinHeight,ViewHeight);
  end;
end;

Procedure TCustomTeePanel.PrintPartialCanvas( PrintCanvas:TCanvas;
                                              Const PrinterRect:TRect);
Var ViewWidth   : Integer;
    ViewHeight  : Integer;
    WinWidth    : Integer;
    WinHeight   : Integer;
    tmpR        : TRect;
    {$IFNDEF CLX}
    OldMapMode  : Integer;
    {$ENDIF}

  Procedure SetAnisotropic; { change canvas/windows metafile mode }
  {$IFNDEF CLX}
  Var DC : HDC;
  {$ENDIF}
  begin
    {$IFNDEF CLX}
    DC:=PrintCanvas.Handle;
    OldMapMode:=GetMapMode(DC);
    SetMapMode(DC, MM_ANISOTROPIC);
    SetWindowOrgEx(  DC, 0, 0, nil);
    SetWindowExtEx(  DC, WinWidth, WinHeight, nil);
    SetViewportExtEx(DC, ViewWidth,ViewHeight, nil);
    SetViewportOrgEx(DC, 0, 0, nil);
    {$ENDIF}
  end;

Begin
  tmpR:=PrinterRect;
  { check if margins inverted }
  if tmpR.Left>tmpR.Right then SwapInteger(tmpR.Left,tmpR.Right);
  if tmpR.Top>tmpR.Bottom then SwapInteger(tmpR.Top,tmpR.Bottom);

  { apply PrintResolution to dimensions }
  CalcMetaBounds(tmpR,GetRectangle,WinWidth,WinHeight,ViewWidth,ViewHeight);

  {//SaveDC}
  SetAnisotropic;
  FPrinting:=True;
  try
    if CanClip then ClipCanvas(PrintCanvas,tmpR);
    DrawToMetaCanvas(PrintCanvas,tmpR);
    UnClipCanvas(PrintCanvas);
  finally
    FPrinting:=False;
  end;
  {$IFNDEF CLX}
  SetMapMode(PrintCanvas.Handle,OldMapMode);
  {$ENDIF}
  {//RestoreDC}
end;

Procedure TCustomTeePanel.PrintPartial(Const PrinterRect:TRect);
Begin
  PrintPartialCanvas(Printer.Canvas,PrinterRect);
End;

Procedure TCustomTeePanel.PrintRect(Const R:TRect);
Begin
  Printer.Title:=Name;
  Printer.BeginDoc;
  try
    PrintPartial(R);
    Printer.EndDoc;
  except
    on Exception do
    begin
      Printer.Abort;
      if Printer.Printing then Printer.EndDoc;
      Raise;
    end;
  end;
end;

Function TCustomTeePanel.CalcProportionalMargins:TRect;

  Function CalcMargin(Size1,Size2:Integer; Const ARatio:Double):Integer;
  Var tmpPrinterRatio : Double;
  begin
    tmpPrinterRatio:= TeeGetDeviceCaps(Printer.Handle,LOGPIXELSX)/
                      TeeGetDeviceCaps(Printer.Handle,LOGPIXELSY);
    result:=Round(100.0*(Size2-((Size1-(40.0*Size1*0.01))*ARatio*tmpPrinterRatio))/Size2) div 2;
  end;

Var tmp      : Integer;
    tmpWidth : Integer;
    tmpHeight: Integer;
begin
  With GetRectangle do
  begin
    tmpWidth:=Right-Left;
    tmpHeight:=Bottom-Top;
  end;
  if tmpWidth > tmpHeight then
  begin
    tmp:=CalcMargin(Printer.PageWidth,Printer.PageHeight,tmpHeight/tmpWidth);
    Result:={$IFDEF D6}Types.{$ENDIF}Rect(TeeDefault_PrintMargin,tmp,TeeDefault_PrintMargin,tmp);
  end
  else
  begin
    tmp:=CalcMargin(Printer.PageHeight,Printer.PageWidth,tmpWidth/tmpHeight);
    Result:={$IFDEF D6}Types.{$ENDIF}Rect(tmp,TeeDefault_PrintMargin,tmp,TeeDefault_PrintMargin);
  end;
end;

Function TCustomTeePanel.ChartPrintRect:TRect;
Var tmpLog : Array[Boolean] of Integer;

  Function InchToPixel(Horizontal:Boolean; Const Inch:Double):Integer;
  begin
    result:=Round(Inch*tmpLog[Horizontal]);
  end;

Var tmp : Double;
Begin
  if FPrintProportional then PrintMargins:=CalcProportionalMargins;
  { calculate margins in pixels and calculate the remaining rectangle in pixels }
  tmpLog[True] :=TeeGetDeviceCaps(Printer.Handle,LOGPIXELSX);
  tmpLog[False]:=TeeGetDeviceCaps(Printer.Handle,LOGPIXELSY);
  With result do
  Begin
    tmp   :=0.01*Printer.PageWidth/(1.0*tmpLog[True]);
    Left  :=InchToPixel(True,1.0*PrintMargins.Left*tmp);
    Right :=Printer.PageWidth-InchToPixel(True,1.0*PrintMargins.Right*tmp);

    tmp   :=0.01*Printer.PageHeight/(1.0*tmpLog[False]);
    Top   :=InchToPixel(False,1.0*PrintMargins.Top*tmp);
    Bottom:=Printer.PageHeight-InchToPixel(False,1.0*PrintMargins.Bottom*tmp);
  end;
end;

Procedure TCustomTeePanel.Print;
Begin
  PrintRect(ChartPrintRect);
end;

Procedure TCustomTeePanel.CheckPenWidth(APen:TPen);
begin
  if Printing and TeeCheckPenWidth and (APen.Style<>psSolid) and (APen.Width=1) then
     APen.Width:=0;  { <-- fixes some printer's bug (HP Laserjets?) }
end;

Procedure TCustomTeePanel.DrawPanelBevels(Rect:TRect);

  Procedure DrawBevel(ABevel:TPanelBevel);
  Const Colors:Array[Boolean] of TColor=(clBtnHighlight,clBtnShadow);
  begin
    if ABevel<>bvNone then
       Frame3D(Canvas.ReferenceCanvas,Rect,Colors[ABevel=bvLowered],
               Colors[ABevel=bvRaised],BevelWidth);
  end;

begin
  if (not Printing) or PrintTeePanel then
  begin
    With Canvas.Pen do
    begin
      Style:=psSolid;
      Width:=1;
      Mode:=pmCopy;
    end;
    Canvas.Brush.Style:=bsClear;
    DrawBevel(BevelOuter);
    Frame3D(Canvas.ReferenceCanvas, Rect, Color, Color, BorderWidth);
    DrawBevel(BevelInner);
  end;
end;

{ TTeeZoom }
Constructor TTeeZoom.Create;
begin
  inherited;
  FAnimatedSteps:=8;
  FAllow:=True;
  FBrush:=TTeeZoomBrush.Create(nil);
  FBrush.Color:=clWhite;
  FBrush.Style:=bsClear;
  FDirection:=tzdBoth;
  FMinimumPixels:=16;
  FMouseButton:=mbLeft;
  FPen:=TTeeZoomPen.Create(nil);
  FPen.Color:=clWhite;
end;

Destructor TTeeZoom.Destroy;
Begin
  FPen.Free;
  FBrush.Free;
  inherited;
end;

Procedure TTeeZoom.Assign(Source:TPersistent);
begin
  if Source is TTeeZoom then
  With TTeeZoom(Source) do
  begin
    Self.FAllow        := FAllow;
    Self.FAnimated     := FAnimated;
    Self.FAnimatedSteps:= FAnimatedSteps;
    Self.Brush         := FBrush;
    Self.FDirection    := FDirection;
    Self.FKeyShift     := FKeyShift;
    Self.FMouseButton  := FMouseButton;
    Self.Pen           := Pen;
  end;
end;

procedure TTeeZoom.SetBrush(Value:TTeeZoomBrush);
begin
  FBrush.Assign(Value);
end;

procedure TTeeZoom.SetPen(Value:TTeeZoomPen);
begin
  FPen.Assign(Value);
end;

{ TCustomTeePanelExtended }
Constructor TCustomTeePanelExtended.Create(AOwner: TComponent);
begin
   inherited;
   {$IFDEF TEETRIAL}
   if Assigned(TeeAboutBoxProc) and not (csDesigning in ComponentState) then
      TeeAboutBoxProc;
   {$ENDIF}
   FGradient:=TChartGradient.Create(CanvasChanged);
   FBackImageMode:=pbmStretch;
   FBackImage:=TPicture.Create;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
// FBackImage.OnChange:=CanvasChanged;
   FBackImage.OnChange:=OnBackImageChange;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
   FAllowPanning:=pmBoth;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
   FTransparent:=FALSE;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
   FZoom:=TTeeZoom.Create;
end;

Destructor TCustomTeePanelExtended.Destroy;
begin
   FZoom.Free;
   FGradient.Free;
   FBackImage.Free;
   inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TCustomTeePanelExtended.Load(var S: TOldStream; AOwner: TComponent);
begin
   inherited;
   FGradient:=TChartGradient.Load(S,OnGradientChange);
   S.Read(FBackImageMode,SizeOf(FBackImageMode));

   FBackImage:=TPicture.Create;
   FBackImage.OnChange:=OnBackImageChange;

   S.Read(FAllowPanning,SizeOf(FAllowPanning));
   S.Read(FTransparent,SizeOf(Boolean));

   FZoom:=TTeeZoom.Create;
end;

Procedure TCustomTeePanelExtended.Store(var S: TOldStream);
begin
   inherited Store(S);
   FGradient.Store(S);
   S.Write(FBackImageMode,SizeOf(FBackImageMode));
   S.Write(FAllowPanning,SizeOf(FAllowPanning));
   S.Write(FTransparent,SizeOf(Boolean));
end;

Function TCustomTeePanelExtended.OnCompareItems(Item: TComponent): Boolean;
begin
   Result:=FALSE;
   if Item is TCustomTeePanelExtended then
      with TCustomTeePanelExtended(Item) do
         if Self.FTransparent = FTransparent then
            if Self.FTransparent then
               Result := inherited OnCompareItems(Item)
            else
               if Self.FBackImageMode = FBackImageMode then
                  if Self.FAllowPanning = FAllowPanning then
                        if Self.FGradient.OnCompareItems(FGradient) then
                           Result := inherited OnCompareItems(Item);
end;

Procedure TCustomTeePanelExtended.SetTransparent(Value: Boolean);
begin
   if Value <> FTransparent then
      begin
         FTransparent:=Value;
         if FTransparent then
            FGradient.Visible:=FALSE;
      end;
end;

Procedure TCustomTeePanelExtended.OnBackImageChange(Sender: TObject);
begin
   CanvasChanged(Sender);
end;

Procedure TCustomTeePanelExtended.OnGradientChange(Sender: TObject);
begin
   if FGradient.Visible then
      FTransparent:=FALSE;
   CanvasChanged(Sender);
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TCustomTeePanelExtended.SetAnimatedZoom(Value:Boolean);
Begin
  FZoom.Animated:=Value;
end;

Procedure TCustomTeePanelExtended.SetAnimatedZoomSteps(Value:Integer);
Begin
  FZoom.AnimatedSteps:=Value;
end;

procedure TCustomTeePanelExtended.SetBackImage(Value:TPicture);
begin
  FBackImage.Assign(Value);
end;

procedure TCustomTeePanelExtended.SetBackImageInside(Value:Boolean);
begin
  SetBooleanProperty(FBackImageInside,Value);
end;

procedure TCustomTeePanelExtended.SetBackImageMode(Value:TTeeBackImageMode);
Begin
  if FBackImageMode<>Value then
  begin
    FBackImageMode:=Value;
    Invalidate;
  end;
End;

procedure TCustomTeePanelExtended.UndoZoom;
begin
  if Assigned(FOnUndoZoom) then FOnUndoZoom(Self);
  Invalidate;
  FZoomed:=False;
end;

procedure TCustomTeePanelExtended.SetGradient(Value:TChartGradient);
begin
  FGradient.Assign(Value);
end;

Procedure TCustomTeePanelExtended.Assign(Source: TPersistent);
begin
   if Source is TCustomTeePanelExtended then
      with TCustomTeePanelExtended(Source) do
         begin
            Self.BackImage        := FBackImage;
            Self.FBackImageInside := FBackImageInside;
            Self.FBackImageMode   := FBackImageMode;
            Self.Gradient         := FGradient;
            Self.FAllowPanning    := FAllowPanning;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
            Self.FTransparent     := FTransparent;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
            Self.Zoom             := Zoom;
         end;
   inherited Assign(Source);
end;

{$IFNDEF CLX}
function TCustomTeePanelExtended.GetPalette: HPALETTE;
begin
  Result:=0;	{ default result is no palette }
  if Assigned(FBackImage) and Assigned(FBackImage.Graphic) and
     (FBackImage.Graphic is TBitmap) then	{ only bitmaps have palettes }
         Result := TBitmap(FBackImage.Graphic).Palette;	{ use it if available }
end;
{$ENDIF}

procedure TCustomTeePanelExtended.DrawBitmap(Rect:TRect; Z:Integer);
var RectH : Integer;
    RectW : Integer;

    Procedure TileBitmap;
    Var tmpWidth  : Integer;
	tmpHeight : Integer;
	tmpX      : Integer;
	tmpY      : Integer;
    begin
      tmpWidth :=BackImage.Width;
      tmpHeight:=BackImage.Height;
      { keep "painting" the wall with tiled small images... }
      if (tmpWidth>0) and (tmpHeight>0) then
      Begin
	tmpY:=0;
	while tmpY<RectH do
	begin
	  tmpX:=0;
	  while tmpX<RectW do
	  begin
	    Canvas.Draw(Rect.Left+tmpX,Rect.Top+tmpY,BackImage.Graphic);
	    Inc(tmpX,tmpWidth);
	  end;
	  Inc(tmpY,tmpHeight);
	end;
      end;
    end;

    Procedure Calc3DRect;
    begin
      Rect:=Canvas.CalcRect3D(Rect,Z);
      if not View3D then Inc(Rect.Top);
    end;

Var ShouldClip : Boolean;
Begin
  if (Z=0) or ((not View3D) or (View3DOptions.Orthogonal)) then
  if Assigned(FBackImage.Graphic) then
  begin
    if FBackImageMode=pbmStretch then
    begin
      if Z>0 then Calc3DRect;
      Canvas.StretchDraw(Rect,FBackImage.Graphic);
    end
    else
    begin
      ShouldClip:=CanClip;
      if ShouldClip then
	 if Z=0 then Canvas.ClipRectangle(Rect)
		else Canvas.ClipCube(Rect,0,Width3D);
      if Z>0 then Calc3DRect;
      RectSize(Rect,RectW,RectH);
      if FBackImageMode=pbmTile then TileBitmap
      else { draw centered }
      Begin
	FillPanelRect(Rect);
	Canvas.Draw( Rect.Left+((RectW-BackImage.Width) div 2),
		     Rect.Top +((RectH-BackImage.Height) div 2),
		     BackImage.Graphic);
      end;
      if ShouldClip then Canvas.UnClipRectangle;
    end;
  end;
end;

procedure TCustomTeePanelExtended.PanelPaint(Const UserRect:TRect);
Var Rect : TRect;
begin
  Rect:=UserRect;
  FillPanelRect(Rect);
  if Assigned(BackImage.Graphic) and (not FBackImageInside) then
     DrawBitmap(Rect,0);
  if not Canvas.SupportsFullRotation then DrawPanelBevels(Rect);
end;

procedure TCustomTeePanelExtended.FillPanelRect(Const Rect:TRect);
Begin
  With FGradient do if Visible then Draw(Canvas,Rect,0)
  else
  { PrintTeePanel is a "trick" to paint Chart background also when printing }
  if ((not Printing) or PrintTeePanel) and (Self.Color<>clNone) then
  With Canvas do
  begin
    Brush.Color:=Self.Color;
    Brush.Style:=bsSolid;
    EraseBackground(Rect);
  end;
end;

procedure TCustomTeePanelExtended.DrawZoomRectangle;
var tmp:TColor;
Begin
  tmp:=ColorToRGB(GetBackColor);
  SetBrushCanvas((clWhite-tmp) xor Zoom.Brush.Color,Zoom.Brush,tmp);
  Zoom.Pen.Mode:=pmNotXor;
  With InternalCanvas do
  Begin
    AssignVisiblePenColor(Zoom.Pen,(clWhite-tmp) xor Zoom.Pen.Color);
    if Zoom.Brush.Style=bsClear then BackMode:=cbmTransparent;
    With Zoom do Rectangle(X0,Y0,X1,Y1);
  end;
end;

function TCustomTeePanelExtended.GetAllowZoom: Boolean;
begin
  result:=Zoom.Allow;
end;

function TCustomTeePanelExtended.GetAnimatedZoom: Boolean;
begin
  result:=Zoom.Animated;
end;

function TCustomTeePanelExtended.GetAnimatedZoomSteps: Integer;
begin
  result:=Zoom.AnimatedSteps;
end;

procedure TCustomTeePanelExtended.SetAllowZoom(Value: Boolean);
begin
  Zoom.Allow:=Value;
end;

procedure TCustomTeePanelExtended.SetZoom(Value: TTeeZoom);
begin
  FZoom.Assign(Value);
end;

procedure TCustomTeePanelExtended.ReadAllowZoom(Reader: TReader);
begin
  Zoom.Allow:=Reader.ReadBoolean;
end;

procedure TCustomTeePanelExtended.ReadAnimatedZoom(Reader: TReader);
begin
  Zoom.Animated:=Reader.ReadBoolean;
end;

procedure TCustomTeePanelExtended.ReadAnimatedZoomSteps(Reader: TReader);
begin
  Zoom.AnimatedSteps:=Reader.ReadInteger;
end;

procedure TCustomTeePanelExtended.ReadPrintMargins(Reader: TReader);
begin
  Reader.ReadListBegin;
  with PrintMargins do
  begin
    Left  :=Reader.ReadInteger;
    Top   :=Reader.ReadInteger;
    Right :=Reader.ReadInteger;
    Bottom:=Reader.ReadInteger;
  end;
  Reader.ReadListEnd;
end;

procedure TCustomTeePanelExtended.SavePrintMargins(Writer: TWriter);
begin
  Writer.WriteListBegin;
  with PrintMargins do
  begin
    Writer.WriteInteger(Left);
    Writer.WriteInteger(Top);
    Writer.WriteInteger(Right);
    Writer.WriteInteger(Bottom);
  end;
  Writer.WriteListEnd;
end;

procedure TCustomTeePanelExtended.DefineProperties(Filer:TFiler);

  Function NotDefaultPrintMargins:Boolean;
  begin
    With PrintMargins do
    result:= (Left<>TeeDefault_PrintMargin) or
             (Top<>TeeDefault_PrintMargin) or
             (Right<>TeeDefault_PrintMargin) or
             (Bottom<>TeeDefault_PrintMargin);
  end;

begin
  inherited;
  Filer.DefineProperty('AllowZoom',ReadAllowZoom,nil,False);
  Filer.DefineProperty('AnimatedZoom',ReadAnimatedZoom,nil,False);
  Filer.DefineProperty('AnimatedZoomSteps',ReadAnimatedZoomSteps,nil,False);
  Filer.DefineProperty('PrintMargins',ReadPrintMargins,SavePrintMargins,NotDefaultPrintMargins);
end;

{ TCustomTeeGradient }
Constructor TCustomTeeGradient.Create(ChangedEvent: TNotifyEvent);
begin
   inherited Create;
   IChanged    :=ChangedEvent;
   FDirection  :=gdTopBottom;
   FStartColor :=clWhite;
   FEndColor   :=clYellow;
   FMidColor   :=clNone;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TCustomTeeGradient.Load(var S: TOldStream; ChangedEvent: TNotifyEvent);
begin
   inherited Create;
   IChanged:=ChangedEvent;
   S.Read(FDirection,SizeOf(FDirection));
   S.Read(FStartColor,SizeOf(FStartColor));
   S.Read(FEndColor,SizeOf(FEndColor));
   S.Read(FMidColor,SizeOf(FMidColor));
   S.Read(FVisible,SizeOf(Boolean));
end;

Procedure TCustomTeeGradient.Store(var S: TOldStream);
begin
   S.Write(FDirection,SizeOf(FDirection));
   S.Write(FStartColor,SizeOf(FStartColor));
   S.Write(FEndColor,SizeOf(FEndColor));
   S.Write(FMidColor,SizeOf(FMidColor));
   S.Write(FVisible,SizeOf(Boolean));
end;

Function TCustomTeeGradient.OnCompareItems(Item: TPersistent): Boolean;
begin
   Result:=FALSE;
   if Item is TCustomTeeGradient then
      with TCustomTeeGradient(Item) do
         if Self.FVisible = FVisible then
            if Self.FVisible then
               begin
                  if Self.FDirection = FDirection then
                     if Self.FStartColor = FStartColor then
                        if Self.FEndColor = FEndColor then
                           Result := Self.FMidColor = FMidColor;
               end
            else
               Result:=TRUE;
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TCustomTeeGradient.SetVisible(Value:Boolean);
Begin
  if FVisible<>Value then
  begin
    FVisible:=Value;
    IChanged(Self);
  end;
End;

Procedure TCustomTeeGradient.SetDirection(Value:TGradientDirection);
Begin
  if FDirection<>Value then
  Begin
    FDirection:=Value;
    IChanged(Self);
  end;
End;

Procedure TCustomTeeGradient.SetColorProperty(Var AColor:TColor; Value:TColor);
begin
  if AColor<>Value then
  begin
    AColor:=Value;
    IChanged(Self);
  end;
end;

Procedure TCustomTeeGradient.SetStartColor(Value:TColor);
Begin
  SetColorProperty(FStartColor,Value);
End;

Procedure TCustomTeeGradient.SetEndColor(Value:TColor);
Begin
  SetColorProperty(FEndColor,Value);
End;

Procedure TCustomTeeGradient.SetMidColor(Value:TColor);
Begin
  SetColorProperty(FMidColor,Value);
End;

Procedure TCustomTeeGradient.Assign(Source:TPersistent);
Begin
  if Source is TCustomTeeGradient then
  With TCustomTeeGradient(Source) do
  Begin
    Self.FDirection :=FDirection;
    Self.FEndColor  :=FEndColor;
    Self.FMidColor  :=FMidColor;
    Self.FStartColor:=FStartColor;
    Self.FVisible   :=FVisible;
  end
  else inherited;
end;

Procedure TCustomTeeGradient.Draw( Canvas:TTeeCanvas; Const Rect:TRect;
                                   RoundRectSize:Integer{$IFDEF D4}=0{$ENDIF});
var R : TRect;

  Procedure DoVert(C0,C1,C2,C3:TColor);
  begin
    R.Bottom:=(Rect.Bottom+Rect.Top) div 2;
    Canvas.GradientFill(R,C0,C1,Direction);
    R.Top:=R.Bottom;
    R.Bottom:=Rect.Bottom;
    Canvas.GradientFill(R,C2,C3,Direction);
  end;

  Procedure DoHoriz(C0,C1,C2,C3:TColor);
  begin
    R.Right:=(Rect.Left+Rect.Right) div 2;
    Canvas.GradientFill(R,C0,C1,Direction);
    R.Left:=R.Right;
    R.Right:=Rect.Right;
    Canvas.GradientFill(R,C2,C3,Direction);
  end;

begin
  if RoundRectSize>0 then ClipRoundRectangle(Canvas,Rect,RoundRectSize);
  if MidColor=clNone then
     Canvas.GradientFill(Rect,StartColor,EndColor,Direction)
  else
  begin
    R:=Rect;
    Case Direction of
    gdTopBottom: DoVert(MidColor,EndColor,StartColor,MidColor);
    gdBottomTop: DoVert(StartColor,MidColor,MidColor,EndColor);
    gdLeftRight: DoHoriz(MidColor,EndColor,StartColor,MidColor);
    gdRightLeft: DoHoriz(StartColor,MidColor,MidColor,EndColor);
    else
       Canvas.GradientFill(Rect,StartColor,EndColor,Direction)
    end;
  end;
  if RoundRectSize>0 then Canvas.UnClipRectangle;
end;

{ TTeeCustomShapeBrushPen }
Constructor TTeeCustomShapeBrushPen.Create;
begin
   inherited;
   FBrush:=GetBrushClass.Create(CanvasChanged);
   FPen:=TChartPen.Create(CanvasChanged);
   FVisible:=True;
end;

Destructor TTeeCustomShapeBrushPen.Destroy;
begin
   FBrush.Free;
   FPen.Free;
   inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TTeeCustomShapeBrushPen.Load(var S: TOldStream);
begin
   inherited Create;
   FBrush:=GetBrushClass.Load(S,CanvasChanged);
   FPen:=TChartPen.Load(S,CanvasChanged);
   S.Read(FVisible,SizeOf(Boolean));
end;

Procedure TTeeCustomShapeBrushPen.Store(var S: TOldStream);
begin
   FBrush.Store(S);
   FPen.Store(S);
   S.Write(FVisible,SizeOf(Boolean));
end;

Function TTeeCustomShapeBrushPen.OnCompareItems(Item: TPersistent): Boolean;
begin
   Result:=FALSE;
   if Item is TTeeCustomShapeBrushPen then
      with TTeeCustomShapeBrushPen(Item) do
         if Self.FVisible = FVisible then
            if Self.FVisible then
               begin
                  if Self.FBrush.OnCompareItems(FBrush) then
                     Result:=Self.FPen.OnCompareItems(FPen);
               end
            else
               Result:=TRUE;
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TTeeCustomShapeBrushPen.Assign(Source:TPersistent);
Begin
  if Source is TTeeCustomShapeBrushPen then
  With TTeeCustomShapeBrushPen(Source) do
  begin
    Self.Brush   :=FBrush;
    Self.Pen     :=FPen;
    Self.FVisible:=FVisible;
  end
  else inherited;
end;

function TTeeCustomShapeBrushPen.GetBrushClass: TChartBrushClass;
begin
  result:=TChartBrush;
end;

Procedure TTeeCustomShapeBrushPen.SetBrush(Value:TChartBrush);
begin
  FBrush.Assign(Value);
end;

Procedure TTeeCustomShapeBrushPen.SetPen(Value:TChartPen);
Begin
  FPen.Assign(Value);
end;

Procedure TTeeCustomShapeBrushPen.Repaint;
begin
  if Assigned(ParentChart) then ParentChart.Invalidate;
end;

Procedure TTeeCustomShapeBrushPen.CanvasChanged(Sender:TObject);
begin
  Repaint;
end;

procedure TTeeCustomShapeBrushPen.SetVisible(Value:Boolean);
begin
  if Assigned(ParentChart) then ParentChart.SetBooleanProperty(FVisible,Value)
                           else FVisible:=Value; 
end;

{ TTeeCustomShape }
Constructor TTeeCustomShape.Create(AOwner: TCustomTeePanel);
begin
   ParentChart:=AOwner;
   inherited Create;
   FBevel:=bvNone;
   FBevelWidth:=2;
   FColor:=clWhite;
   FFont:=TTeeFont.Create(CanvasChanged);
   FGradient:=GetGradientClass.Create(CanvasChanged);
   FShapeStyle:=fosRectangle;
   FShadowColor:=clBlack;
   FShadowSize:=3;
end;

Destructor TTeeCustomShape.Destroy;
begin
   FFont.Free;
   FGradient.Free;
   inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TTeeCustomShape.Load(var S: TOldStream; AOwner: TCustomTeePanel);
var intValue    : Integer;
    smlValue    : SmallInt;
begin
   ParentChart:=AOwner;
   inherited Load(S);

   // Upack values (this method slightly limits the maximum value of FBevelWidth)
   S.Read(intValue,SizeOf(Integer));
   FTransparency:=intValue and $0000007F;
   intValue:=intValue shr 7;
   FTransparent:=(intValue and $00000001) <> 0;
   intValue:=intValue shr 1;
   FShapeStyle:=TChartObjectShapeStyle(intValue and $00000001);
   intValue:=intValue shr 1;
   FBevel:=TPanelBevel(intValue and $00000003);
   FBevelWidth:=intValue shr 2;

   S.Read(FColor,SizeOf(TColor));
   FFont:=TTeeFont.Load(S,CanvasChanged);
   FGradient:=GetGradientClass.Load(S,CanvasChanged);
   S.Read(FShadowColor,SizeOf(TColor));
   S.Read(smlValue,SizeOf(SmallInt));
   FShadowSize:=smlValue;
   S.Read(ShapeBounds,SizeOf(TRect));
end;

Procedure TTeeCustomShape.Store(var S: TOldStream);
var intValue    : Integer;
    smlValue    : SmallInt;
begin
   inherited Store(S);

   // Pack values (this method slightly limits the maximum value of FBevelWidth)
   intValue:=FBevelWidth shl 2;
   intValue:=intValue or Ord(FBevel);
   intValue:=intValue shl 1;
   intValue:=intValue or Ord(FShapeStyle);
   intValue:=intValue shl 1;
   if FTransparent then
      intValue:=intValue or 1;
   intValue:=intValue shl 7;
   intValue:=intValue or FTransparency;
   S.Write(intValue,SizeOf(Integer));

   S.Write(FColor,SizeOf(TColor));
   FFont.Store(S);
   FGradient.Store(S);
   S.Write(FShadowColor,SizeOf(TColor));
   // Pack values (this method slightly limits the maximum value of FShadowSize)
   smlValue:=FShadowSize;
   S.Write(smlValue,SizeOf(SmallInt));
   S.Write(ShapeBounds,SizeOf(TRect));
end;

Function TTeeCustomShape.CreateDeterminant(Determinant: String): String;
var Size        : Integer;
    intValue    : Integer;
    smlValue    : SmallInt;
begin
   Result:=FFont.CreateDeterminant(Determinant);
   Size:=Length(Result);
   SetLength(Result,Size+SizeOf(intValue)+2*SizeOf(TColor)+SizeOf(SmallInt));
   Inc(Size);

   // Pack values (this method slightly limits the maximum value of FBevelWidth)
   intValue:=FBevelWidth shl 2;
   intValue:=intValue or Ord(FBevel);
   intValue:=intValue shl 1;
   intValue:=intValue or Ord(FShapeStyle);
   intValue:=intValue shl 1;
   if FTransparent then
      intValue:=intValue or 1;
   intValue:=intValue shl 7;
   intValue:=intValue or FTransparency;
   CopyMemory(@Result[Size],@intValue,SizeOf(intValue));
   Inc(Size,SizeOf(intValue));

   CopyMemory(@Result[Size],@FColor,SizeOf(TColor));
   Inc(Size,SizeOf(TColor));
   CopyMemory(@Result[Size],@FShadowColor,SizeOf(TColor));
   Inc(Size,SizeOf(TColor));
   // Pack values (this method slightly limits the maximum value of FShadowSize)
   smlValue:=FShadowSize;
   CopyMemory(@Result[Size],@smlValue,SizeOf(SmallInt));
end;

Function TTeeCustomShape.OnCompareItems(Item: TPersistent): Boolean;
begin
   Result:=FALSE;
   if Item is TTeeCustomShape then
      with TTeeCustomShape(Item) do
         if Self.FBevel = FBevel then
            if Self.FBevelWidth = FBevelWidth then
               if Self.FColor = FColor then
                  if Self.FShapeStyle = FShapeStyle then
                     if Self.FShadowColor = FShadowColor then
                        if Self.FShadowSize = FShadowSize then
                           if Self.FTransparent = FTransparent then
                              if Self.FTransparency = FTransparency then
                                 if Self.FFont.OnCompareItems(FFont) then
                                    if Self.FGradient.OnCompareItems(FGradient) then
                                       Result:=inherited OnCompareItems(Item);
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TTeeCustomShape.Assign(Source: TPersistent);
begin
   if Source is TTeeCustomShape then
      with TTeeCustomShape(Source) do
         begin
            Self.FBevel       :=FBevel;
            Self.FBevelWidth  :=FBevelWidth;
            Self.FColor       :=FColor;
            Self.Font         :=FFont;
            Self.Gradient     :=FGradient;
            Self.FShapeStyle  :=FShapeStyle;
            Self.FShadowColor :=FShadowColor;
            Self.FShadowSize  :=FShadowSize;
            Self.FTransparent :=FTransparent;
         end;
   inherited;
end;

Procedure TTeeCustomShape.Draw;
var tmpBlend : TTeeBlend;
begin
  if (Transparency>0) and (ParentChart.Canvas is TTeeCanvas3D) then
  begin
    tmpBlend:=TTeeBlend.Create(TTeeCanvas3D(ParentChart.Canvas),ShapeBounds);
    DrawRectRotated(ShapeBounds,0,0);
    tmpBlend.DoBlend(Transparency);
    tmpBlend.Free;
  end
  else DrawRectRotated(ShapeBounds,0,0);
end;

Procedure TTeeCustomShape.DrawRectRotated(Const Rect:TRect; Angle:Integer; AZ:Integer);
Const TeeDefaultRoundSize=16;

  Procedure InternalDrawShape(Const ARect:TRect);
  Var tmpSin    : Extended;
      tmpCos    : Extended;
      tmpCenter : TPoint;

    Procedure RotatePoint(Var P:TPoint; AX,AY:Integer);
    begin
      P.X:=tmpCenter.x+Round(  AX*tmpCos + AY*tmpSin );
      P.Y:=tmpCenter.y+Round( -AX*tmpSin + AY*tmpCos );
    end;

  Var tmp : Double;
      P   : TFourPoints;
      tmpRect : TRect;
  begin
    if Angle>0 then
    begin
      RectCenter(ARect,tmpCenter.X,tmpCenter.Y);
      tmp:=Angle*TeePiStep;
      SinCos(tmp,tmpSin,tmpCos);
      tmpRect:=ARect;
      With tmpRect do
      begin
        Dec(Left,tmpCenter.X);
        Dec(Right,tmpCenter.X);
        Dec(Top,tmpCenter.Y);
        Dec(Bottom,tmpCenter.Y);
        RotatePoint(P[0],Left,Top);
        RotatePoint(P[1],Right,Top);
        RotatePoint(P[2],Right,Bottom);
        RotatePoint(P[3],Left,Bottom);
      end;
      ParentChart.Canvas.PolygonWithZ(P,AZ);
    end
    else
    With ParentChart.Canvas do
    if SupportsFullRotation then RectangleWithZ(ARect,AZ)
    else
    Case FShapeStyle of
      fosRectangle: DoRectangle(ARect);
    else
      With ARect do RoundRect(Left,Top,Right,Bottom,TeeDefaultRoundSize,TeeDefaultRoundSize)
    end;
  end;

  Procedure DrawShadow;
  var tmp : Integer;
  begin
    tmp:=FShadowSize;
    if (tmp>0) and Pen.Visible then Inc(tmp,Pen.Width);
    With ParentChart.Canvas do
    begin
      Pen.Style:=psClear;
      Brush.Style:=bsSolid;
      Brush.Color:=FShadowColor;
      With Rect do
        InternalDrawShape({$IFDEF D6}Types.{$ELSE}Classes.{$ENDIF}Rect(Left+tmp,
                                 Top+tmp,Right+tmp,Bottom+tmp));
    end;
  end;

  Procedure DrawBevel;
  Const Colors:Array[Boolean] of TColor=(clBtnHighlight,clBtnShadow);
  Var tmpR : TRect;
  begin
    if FBevel<>bvNone then
    begin
      tmpR:=Rect;
      Frame3D(ParentChart.Canvas.ReferenceCanvas,tmpR,Colors[FBevel=bvLowered],
              Colors[FBevel=bvRaised],FBevelWidth);
    end;
  end;

var tmp : Integer;
begin
  if not FTransparent then
  begin
    if (FShadowSize<>0) and (FBrush.Style<>bsClear) then
       if not ParentChart.Canvas.SupportsFullRotation then DrawShadow;

    With ParentChart.Canvas do
    begin
      With FGradient do
      if Visible and (Angle=0) then
      begin
        if Self.FShapeStyle=fosRoundRectangle then
           tmp:=TeeDefaultRoundSize
        else
           tmp:=0;
        Draw(ParentChart.Canvas,Rect,tmp);
        Brush.Style:=bsClear;
      end
      else AssignBrush(Self.Brush,Self.Color);
      AssignVisiblePen(Self.Pen);
      InternalDrawShape(Rect);
    end;
    DrawBevel;
  end;
end;

function TTeeCustomShape.GetGradientClass: TChartGradientClass;
begin
  result:=TChartGradient;
end;

procedure TTeeCustomShape.SetBevel(Value: TPanelBevel);
begin
  if FBevel<>Value then
  begin
    FBevel:=Value;
    Repaint;
  end;
end;

procedure TTeeCustomShape.SetBevelWidth(Value: TBevelWidth);
begin
  if FBevelWidth<>Value then
  begin
    FBevelWidth:=Value;
    if Assigned(ParentChart) then ParentChart.Invalidate;
  end;
end;

Procedure TTeeCustomShape.SetColor(Value:TColor);
begin
  if Assigned(ParentChart) then ParentChart.SetColorProperty(FColor,Value)
                           else FColor:=Value;
end;

Procedure TTeeCustomShape.SetFont(Value:TTeeFont);
begin
  FFont.Assign(Value);
end;

procedure TTeeCustomShape.SetGradient(Value:TChartGradient);
begin
  FGradient.Assign(Value);
end;

Procedure TTeeCustomShape.SetShadowColor(Value:TColor);
begin
  if Assigned(ParentChart) then ParentChart.SetColorProperty(FShadowColor,Value)
                           else FShadowColor:=Value;
end;

Procedure TTeeCustomShape.SetShadowSize(Value:Integer);
begin
  if Assigned(ParentChart) then ParentChart.SetIntegerProperty(FShadowSize,Value)
                           else FShadowSize:=Value;
end;

Procedure TTeeCustomShape.SetShapeStyle(Value:TChartObjectShapeStyle);
Begin
  if FShapeStyle<>Value then
  begin
    FShapeStyle:=Value;
    Repaint;
  end;
end;

procedure TTeeCustomShape.SetTransparency(Value: TTeeTransparency);
begin
  if FTransparency<>Value then
  begin
    FTransparency:=Value;
    if Assigned(ParentChart) then ParentChart.Invalidate;
  end;
end;

procedure TTeeCustomShape.SetTransparent(Value:Boolean);
begin
  if Assigned(ParentChart) then ParentChart.SetBooleanProperty(FTransparent,Value)
                           else FTransparent:=Value;
end;

{ Zoom & Scroll (Panning) }
Procedure TZoomPanning.Check;
begin
  if X0>X1 Then SwapInteger(X0,X1);
  if Y0>Y1 Then SwapInteger(Y0,Y1);
end;

Procedure TZoomPanning.Activate(x,y:Integer);
Begin
  X0:=x;  Y0:=y;
  X1:=x;  Y1:=y;
  Active:=True;
End;

{ TTeeMouseEventListeners }
function TTeeEventListeners.Add(Const Item: ITeeEventListener): Integer;
begin
  result:=inherited Add(nil);
  inherited Items[result]:=Pointer(Item);
end;

function TTeeEventListeners.Get(Index: Integer): ITeeEventListener;
begin
  result:=ITeeEventListener(inherited Items[Index]);
end;

function TTeeEventListeners.Remove(Item: ITeeEventListener): Integer;
begin
  result:=IndexOf(Pointer(Item));
  if result>-1 then
  begin
    inherited Items[result]:=nil;
    Delete(result);
  end;
end;

{ Returns True if point T is over (more or less "Tolerance" pixels)
  line:  P --> Q }
Function PointInLineTolerance(Const P:TPoint; px,py,qx,qy,TolerancePixels:Integer):Boolean;

  Function Dif(a,b:Integer):Boolean;
  begin
    result:=(a+TolerancePixels)<b;
  end;

begin
  if ( Abs((qy-py)*(P.x-px)-(P.y-py)*(qx-px)) >= (MaxLong(Abs(qx-px),Abs(qy-py)))) then
      result:=False
  else
  if ((Dif(qx,px) and Dif(px,P.x)) or (Dif(qy,py) and Dif(py,P.y))) then result:=False else
  if ((Dif(P.x,px) and Dif(px,qx)) or (Dif(P.y,py) and Dif(py,qy))) then result:=False else
  if ((Dif(px,qx) and Dif(qx,P.x)) or (Dif(py,qy) and Dif(qy,P.y))) then result:=False else
  if ((Dif(P.x,qx) and Dif(qx,px)) or (Dif(P.y,qy) and Dif(qy,py))) then result:=False else
     result:=True;
end;

Function PointInLine(Const P:TPoint; px,py,qx,qy:Integer):Boolean;
Begin
  result:=PointInLineTolerance(P,px,py,qx,qy,0);
end;

Function PointInPolygon(Const P:TPoint; Const Poly:Array of TPoint):Boolean;
{ slow...
Var Region:HRGN;
begin
  Region:=CreatePolygonRgn(Poly,1+High(Poly),0);
  result:=(Region>0) and PtInRegion(Region,p.x,p.y);
  DeleteObject(Region);
end;
}
Var i,j : Integer;
begin
  result:=False;
  j:=High(Poly);
  for i:=0 to High(Poly) do
  begin
    if (((( Poly[i].Y <= P.Y ) and ( P.Y < Poly[j].Y ) ) or
         (( Poly[j].Y <= P.Y ) and ( P.Y < Poly[i].Y ) )) and
          ( P.X < ( Poly[j].X - Poly[i].X ) * ( P.Y - Poly[i].Y )
          / ( Poly[j].Y - Poly[i].Y ) + Poly[i].X )) then
             result:=not result;
    j:=i;
  end;
end;

Function PointInTriangle(Const P:TPoint; X0,X1,Y0,Y1:Integer):Boolean;
begin
  result:=PointInPolygon(P,[ {$IFDEF D6}Types.{$ENDIF}Point(X0,Y0),
                             {$IFDEF D6}Types.{$ENDIF}Point((X0+X1) div 2,Y1),
                             {$IFDEF D6}Types.{$ENDIF}Point(X1,Y0) ]);
end;

Function PointInHorizTriangle(Const P:TPoint; Y0,Y1,X0,X1:Integer):Boolean;
begin
  result:=PointInPolygon(P,[ {$IFDEF D6}Types.{$ENDIF}Point(X0,Y0),
                             {$IFDEF D6}Types.{$ENDIF}Point(X1,(Y0+Y1) div 2),
                             {$IFDEF D6}Types.{$ENDIF}Point(X0,Y1) ]);
end;

Function PointInEllipse(Const P:TPoint; Const Rect:TRect):Boolean;
var tmpWidth   : Integer;
    tmpHeight  : Integer;
    tmpXCenter : Integer;
    tmpYCenter : Integer;
begin
  RectCenter(Rect,tmpXCenter,tmpYCenter);
  With Rect do
  begin
    tmpWidth :=Sqr(tmpXCenter-Left);
    tmpHeight:=Sqr(tmpYCenter-Top);
  end;
  result:=(tmpWidth<>0) and (tmpHeight<>0) and
          ( (Sqr(1.0*P.X-tmpXCenter) / tmpWidth ) +
            (Sqr(1.0*P.Y-tmpYCenter) / tmpHeight) <= 1 );
end;

Function DelphiToLocalFormat(Const Format:String):String;
var t : Integer;
begin
  result:=Format;
  for t:=1 to Length(result) do
      if result[t]=',' then result[t]:=ThousandSeparator else
      if result[t]='.' then result[t]:=DecimalSeparator;
end;

Function LocalToDelphiFormat(Const Format:String):String;
var t : Integer;
begin
  result:=Format;
  for t:=1 to Length(result) do
      if result[t]=ThousandSeparator then result[t]:=',' else
      if result[t]=DecimalSeparator then result[t]:='.';
end;

Procedure EnableControls(Enable:Boolean; Const ControlArray:Array of TControl);
var t : Integer;
begin
  for t:=Low(ControlArray) to High(ControlArray) do
  if Assigned(ControlArray[t]) then
     ControlArray[t].Enabled:=Enable;
end;

Function TeeRoundDate(Const ADate:TDateTime; AStep:TDateTimeStep):TDateTime;
var Year  : Word;
    Month : Word;
    Day   : Word;
begin
  if ADate<=EncodeDate(1900,1,1) then result:=ADate
  else
  begin
    DecodeDate(ADate,Year,Month,Day);
    Case AStep of
       dtHalfMonth   : if Day>=15 then Day:=15
                                  else Day:=1;
       dtOneMonth,
       dtTwoMonths,
       dtThreeMonths,
       dtFourMonths,
       dtSixMonths   : Day:=1;
       dtOneYear     : begin
                         Day:=1;
                         Month:=1;
                       end;
    end;
    result:=EncodeDate(Year,Month,Day);
  end;
end;

Procedure TeeDateTimeIncrement( IsDateTime:Boolean;
                                Increment:Boolean;
                                Var Value:Double;
                                Const AnIncrement:Double;
                                tmpWhichDateTime:TDateTimeStep);
var Year  : Word;
    Month : Word;
    Day   : Word;

 Procedure DecMonths(HowMany:Word);
 begin
   Day:=1;
   if Month>HowMany then Dec(Month,HowMany)
   else
   begin
     Dec(Year);
     Month:=12-(HowMany-Month);
   end;
 end;

 Procedure IncMonths(HowMany:Word);
 begin
   Day:=1;
   Inc(Month,HowMany);
   if Month>12 then
   begin
     Inc(Year);
     Month:=Month-12;
   end;
 end;

 Procedure IncDecMonths(HowMany:Word);
 begin
   if Increment then IncMonths(HowMany)
                else DecMonths(HowMany);
 end;

begin
  if IsDateTime then
  begin
    DecodeDate(Value,year,month,day);
    Case tmpWhichDateTime of
       dtHalfMonth   : Begin
                         if Day>15 then Day:=15
                         else
                         if Day>1 then Day:=1
                         else
                         begin
                           IncDecMonths(1);
                           Day:=15;
                         end;
                       end;
       dtOneMonth    : IncDecMonths(1);
       dtTwoMonths   : IncDecMonths(2);
       dtThreeMonths : IncDecMonths(3);
       dtFourMonths  : IncDecMonths(4);
       dtSixMonths   : IncDecMonths(6);
       dtOneYear     : if Increment then Inc(year) else Dec(year);
    else
    begin
      if Increment then Value:=Value+AnIncrement
                   else Value:=Value-AnIncrement;
      Exit;
    end;
    end;
    Value:=EncodeDate(year,month,day);
  end
  else
  begin
    if Increment then Value:=Value+AnIncrement
                 else Value:=Value-AnIncrement;
  end;
end;

Procedure TeeSort( StartIndex,EndIndex:Integer; CompareFunc:TTeeSortCompare;
                   SwapFunc:TTeeSortSwap);

  procedure PrivateSort(l,r:Integer);
  var i : Integer;
      j : Integer;
      x : Integer;
  begin
    i:=l;
    j:=r;
    x:=(i+j) shr 1;
    while i<j do
    begin
      while CompareFunc(i,x)<0 do inc(i);
      while CompareFunc(x,j)<0 do dec(j);
      if i<j then
      begin
        SwapFunc(i,j);
        if i=x then x:=j else if j=x then x:=i;
      end;
      if i<=j then
      begin
        inc(i);
        dec(j)
      end;
    end;
    if l<j then PrivateSort(l,j);
    if i<r then PrivateSort(i,r);
  end;

begin
  PrivateSort(StartIndex,EndIndex);
end;

{ Helper functions }
Function TeeGetUniqueName(AOwner:TComponent; Const AStartName:String):TComponentName;

  Function FindNameLoop:String;
  var tmp : Integer;
  begin
    tmp:={$IFDEF TEEOCX}0{$ELSE}1{$ENDIF};
    if Assigned(AOwner) then
    while Assigned(AOwner.FindComponent(AStartName+TeeStr(tmp))) do
         Inc(tmp);
    result:=AStartName+TeeStr(tmp);
  end;

{$IFNDEF CLX}

{$IFNDEF TEEOCX}
var tmpForm : TCustomForm;
    Designer : {$IFDEF D5}IDesigner{$ELSE}{$IFDEF D4}IFormDesigner{$ELSE}TFormDesigner{$ENDIF}{$ENDIF};
{$ENDIF}
begin
  result:='';
  if Assigned(AOwner) then
  begin
    {$IFNDEF TEEOCX}
    if AOwner is TCustomForm then
    begin
      tmpForm:=TCustomForm(AOwner);
      if Assigned(tmpForm.Designer) then
      begin
        Designer:=nil;
        {$IFNDEF D4}
        if tmpForm.Designer is TFormDesigner then
           Designer:=TFormDesigner(tmpForm.Designer);
        {$ELSE}
        tmpForm.Designer.QueryInterface({$IFDEF D5}IDesigner{$ELSE}IFormDesigner{$ENDIF},Designer);
        {$ENDIF}
        if Assigned(Designer) then result:=Designer.UniqueName(AStartName);
      end;
    end;
    {$ENDIF}
    if result='' then result:=FindNameLoop;
  end;
{$ELSE}
begin
  result:=FindNameLoop;
{$ENDIF}
end;

{ returns number of sections in St string separated by ";" }
Function TeeNumFields(St:String):Integer;
var i : Integer;
begin
  if St='' then result:=0
  else
  begin
    result:=1;
    i:=AnsiPos(TeeFieldsSeparator,St);
    While i>0 do
    begin
      Inc(result);
      Delete(St,1,i);
      i:=AnsiPos(TeeFieldsSeparator,St);
    end;
  end;
end;

{ returns the index-th section in St string separated by ";" }
Function TeeExtractField(St:String; Index:Integer):String;
var i : Integer;
begin
  result:='';
  if St<>'' then
  begin
    i:=AnsiPos(TeeFieldsSeparator,St);
    While i>0 do
    begin
      Dec(Index);
      if Index=0 then
      begin
        result:=Copy(St,1,i-1);
        exit;
      end;
      Delete(St,1,i);
      i:=AnsiPos(TeeFieldsSeparator,St);
    end;
    result:=St;
  end;
end;

{ Routines to support "crTeeHand" cursor type }
Function TeeIdentToCursor(Const AName:String; Var ACursor:Integer):Boolean;
begin
  if AName=TeeMsg_TeeHand then
  begin
    ACursor:=crTeeHand;
    result:=True;
  end
  else result:=IdentToCursor(AName,ACursor);
end;

Function TeeCursorToIdent(ACursor:Integer; Var AName:String):Boolean;
begin
  if ACursor=crTeeHand then
  begin
    AName:=TeeMsg_TeeHand;
    result:=True;
  end
  else result:=CursorToIdent(ACursor,AName);
end;

{ Try to find a resource bitmap and load it }
Function TryFindResource(AInstance:Integer; Const ResName:String; ABitmap:TBitmap):Boolean;
var tmpSt : Array[0..255] of Char;
begin
  StrPCopy(tmpSt,ResName);
  if FindResource(AInstance, tmpSt, RT_BITMAP)<>0 then
  begin
    ABitmap.LoadFromResourceName(AInstance,ResName);
    result:=True;
  end
  else result:=False;
end;

{$IFDEF CLX}
{ TMetafileCanvas }
constructor TMetafileCanvas.Create(Meta: TMetafile; Ref: Integer);
begin
  inherited Create;
end;

{ TPrinter }
procedure TPrinter.Abort;
begin
  EndDoc;
end;

procedure TPrinter.BeginDoc;
begin
  Printing:=True;
end;

procedure TPrinter.EndDoc;
begin
  Printing:=False;
end;

Procedure TPrinter.NewPage;
begin
end;
{$ENDIF}

initialization
  Screen.Cursors[crTeeHand]:={$IFDEF CLX}QCursorH(0){$ELSE}LoadCursor(HInstance,'TEE_CURSOR_HAND'){$ENDIF};
finalization
  Screen.Cursors[crTeeHand]:={$IFDEF CLX}nil{$ELSE}0{$ENDIF};
end.
