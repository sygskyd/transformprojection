{**********************************************}
{   TeeChart BarJoin Series Editor             }
{   Copyright (c) 1999-2000 by David Berneda   }
{**********************************************}
unit TeeBarJoinEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeeBarEdit, ComCtrls, TeCanvas, StdCtrls, TeePenDlg;

type
  TBarJoinEditor = class(TBarSeriesEditor)
    BJoin: TButtonPen;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

Uses MyPoint;

procedure TBarJoinEditor.FormShow(Sender: TObject);
begin
  inherited;
  BJoin.LinkPen(TBarJoinSeries(Tag).JoinPen);
end;

initialization
  RegisterClass(TBarJoinEditor);
end.
