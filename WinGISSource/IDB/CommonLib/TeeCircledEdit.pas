{****************************************}
{    TCircledSeries Editor Dialog        }
{ Copyright (c) 2000 by David Berneda    }
{****************************************}
{$I teedefs.inc}
unit TeeCircledEdit;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls, QComCtrls,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, ComCtrls,
     {$ENDIF}
     Chart, Series, TeEngine, TeCanvas, TeePenDlg, MultiLng;

type
  TCircledSeriesEditor = class(TForm)
    Label1: TLabel;
    CBCircled: TCheckBox;
    CB3D: TCheckBox;
    Edit1: TEdit;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Edit2: TEdit;
    CBAutoXR: TCheckBox;
    Edit3: TEdit;
    CBAutoYR: TCheckBox;
    UDY: TUpDown;
    UDX: TUpDown;
    UDRot: TUpDown;
    BBack: TButtonColor;
    MlgSection1: TMlgSection;
    procedure FormShow(Sender: TObject);
    procedure CBCircledClick(Sender: TObject);
    procedure SERotationChange(Sender: TObject);
    procedure SEXRadiusChange(Sender: TObject);
    procedure SEYRadiusChange(Sender: TObject);
    procedure CBAutoXRClick(Sender: TObject);
    procedure CBAutoYRClick(Sender: TObject);
    procedure CB3DClick(Sender: TObject);
  private
    { Private declarations }
    Circled : TCircledSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TCircledSeriesEditor.FormShow(Sender: TObject);
begin
  Circled:=TCircledSeries(Tag);
  With Circled do
  begin
    CBCircled.Checked:=Circled;
    CB3D.Checked     :=ParentChart.View3D;
    UDRot.Position   :=RotationAngle;
    UDX.Position     :=CustomXRadius;
    UDY.Position     :=CustomYRadius;
  end;
  BBack.LinkProperty(Circled,'CircleBackColor');
end;

procedure TCircledSeriesEditor.CBCircledClick(Sender: TObject);
begin
  Circled.Circled:=CBCircled.Checked;
end;

procedure TCircledSeriesEditor.SERotationChange(Sender: TObject);
begin
  if Showing then Circled.RotationAngle:=UDRot.Position;
end;

procedure TCircledSeriesEditor.SEXRadiusChange(Sender: TObject);
begin
  if Showing then
  begin
    Circled.CustomXRadius:=UDX.Position;
    CBAutoXR.Checked:=UDX.Position=0;
  end;
end;

procedure TCircledSeriesEditor.SEYRadiusChange(Sender: TObject);
begin
  if Showing then
  begin
    Circled.CustomYRadius:=UDY.Position;
    CBAutoYR.Checked:=UDY.Position=0;
  end;
end;

procedure TCircledSeriesEditor.CBAutoXRClick(Sender: TObject);
begin
  if CBAutoXR.Checked then UDX.Position:=0;
end;

procedure TCircledSeriesEditor.CBAutoYRClick(Sender: TObject);
begin
  if CBAutoYR.Checked then UDY.Position:=0;
end;

procedure TCircledSeriesEditor.CB3DClick(Sender: TObject);
begin
  Circled.ParentChart.View3D:=CB3D.Checked
end;

initialization
  RegisterClass(TCircledSeriesEditor);
end.
