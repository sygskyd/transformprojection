{**********************************************}
{   TeeChart GIF Export format                 }
{   Copyright (c) 2000 by David Berneda        }
{**********************************************}
{$I teedefs.inc}
unit TeeGIF;

interface

Uses Windows, Classes, Graphics, Forms, GIFImage, TeeProcs, TeExport,
     Controls, StdCtrls, ExtCtrls, TeCanvas;

type
  TTeeGIFOptions = class(TForm)
    RGCompression: TRadioGroup;
    Label1: TLabel;
    CBDither: TComboBox;
    Label2: TLabel;
    CBReduction: TComboBox;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TGIFExportFormat=class(TTeeExportFormat)
  private
    FProperties: TTeeGIFOptions;
    Procedure CheckProperties;
  protected
    Procedure DoCopyToClipboard; override;
  public
    function Description:String; override;
    function FileExtension:String; override;
    function FileFilter:String; override;
    Function GIF:TGIFImage;
    Function Options:TForm; override;
    Procedure SaveToStream(Stream:TStream); override;
  end;

implementation

{$R *.DFM}

Uses Clipbrd, TeeImageConstants;

function TGIFExportFormat.Description:String;
begin
  result:=TeeMsg_AsGIF;
end;

function TGIFExportFormat.FileFilter:String;
begin
  result:=TeeMsg_GIFFilter;
end;

function TGIFExportFormat.FileExtension:String;
begin
  result:='gif';
end;

Function TGIFExportFormat.GIF:TGIFImage;
var tmp : TGIFImage;
begin
  CheckProperties;
  CheckSize;
  tmp:=TGIFImage.Create;
  With tmp do
  begin
    Compression:=TGIFCompression(FProperties.RGCompression.ItemIndex);
    DitherMode:=TDitherMode(FProperties.CBDither.ItemIndex);
    ColorReduction:=TColorReduction(FProperties.CBReduction.ItemIndex);
    Assign(Panel.TeeCreateBitmap(Panel.Color,Rect(0,0,Self.Width,Self.Height)));
  end;
  result:=tmp;
end;

Procedure TGIFExportFormat.CheckProperties;
begin
  if not Assigned(FProperties) then
  begin
    FProperties:=TTeeGIFOptions.Create(nil);
    FProperties.FormShow(Self);
  end;
end;

Function TGIFExportFormat.Options:TForm;
begin
  CheckProperties;
  result:=FProperties;
end;

procedure TGIFExportFormat.DoCopyToClipboard;
var tmp : TGIFImage;
begin
  tmp:=GIF;
  try
    Clipboard.Assign(tmp);
  finally
    tmp.Free;
  end;
end;

procedure TGIFExportFormat.SaveToStream(Stream:TStream);
begin
  With GIF do
  try
    SaveToStream(Stream);
  finally
    Free;
  end;
end;

procedure TTeeGIFOptions.FormShow(Sender: TObject);
begin
  CBDither.ItemIndex:=Ord(GIFImageDefaultDitherMode);
  RGCompression.ItemIndex:=Ord(GIFImageDefaultCompression);
  CBReduction.ItemIndex:=Ord(rmNone);
end;

initialization
  RegisterTeeExportFormat(TGIFExportFormat);
finalization
  UnRegisterTeeExportFormat(TGIFExportFormat);
end.
