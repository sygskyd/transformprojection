{**********************************************}
{   TeeChart Wizard                            }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeChartExp;

interface

uses Forms, Windows, ExptIntf, ToolIntf, VirtIntf, SysUtils, TeeExpForm,
     TeeConst;

type
  TTeeChartWizard = class(TIExpert)
    function GetName: string; override;
    function GetAuthor: string; override;
    function GetComment: string; override;
    function GetGlyph: HICON; override;
    function GetStyle: TExpertStyle; override;
    function GetState: TExpertState; override;
    function GetIDString: string; override;
    function GetPage: string; override;
    procedure Execute; override;
    function GetMenuText: string; override;
  end;

Procedure Register;

implementation

{$R TeeChaEx.res}

{ TTeeChartWizard }
function TTeeChartWizard.GetName: string;
begin
  Result:=TeeMsg_TeeChartWizard;
end;

function TTeeChartWizard.GetComment: string;
begin
  Result:=TeeMsg_TeeChartWizard;
end;

function TTeeChartWizard.GetGlyph: HICON;
begin
  result:=0;
  try
    Result:=LoadIcon(HInstance, 'TEEEXPICON');
  except
    ToolServices.RaiseException(ReleaseException);
  end;
end;

function TTeeChartWizard.GetStyle: TExpertStyle;
begin
  Result:=esForm;
end;

function TTeeChartWizard.GetState: TExpertState;
begin
  Result:=[esEnabled];
end;

function TTeeChartWizard.GetIDString: string;
begin
  Result:=TeeMsg_TeeChartIDWizard;
end;

function TTeeChartWizard.GetAuthor: string;
begin
  Result:=TeeMsg_TeeChartSL;
end;

function TTeeChartWizard.GetPage: string;
begin
  Result:=TeeMsg_WizardTab;
end;

procedure TTeeChartWizard.Execute;
begin
  try
    TeeChartWizard(ToolServices);
  except
    ToolServices.RaiseException(ReleaseException);
  end;
end;

function TTeeChartWizard.GetMenuText: string;
begin
  result:='';
end;

Procedure Register;
begin
  RegisterLibraryExpert(TTeeChartWizard.Create);
end;

end.
