unit TeeSeriesTextEd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Grids, TeeURL, ExtCtrls, Buttons, TeEngine;

type
  TSeriesTextEditor = class(TForm)
    OpenDialog1: TOpenDialog;
    Button1: TButton;
    Button2: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Edit1: TEdit;
    UpDown1: TUpDown;
    StringGrid1: TStringGrid;
    CBSeries: TComboBox;
    CBSep: TComboBox;
    TabSheet2: TTabSheet;
    BBrowse: TSpeedButton;
    RBFile: TRadioButton;
    EFile: TEdit;
    RBWeb: TRadioButton;
    EWeb: TEdit;
    Button4: TButton;
    procedure FormShow(Sender: TObject);
    procedure CBSeriesChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure RBFileClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    Function SelectedSeries:TChartSeries;
    Procedure SetOptions;
  public
    { Public declarations }
    DataSource:TSeriesTextSource;
  end;

Procedure TeeEditSeriesTextSource(ASource:TSeriesTextSource);

implementation

{$R *.DFM}

Uses TeeConst, TeeProCo;

Procedure TeeEditSeriesTextSource(ASource:TSeriesTextSource);
begin
  With TSeriesTextEditor.Create(nil) do
  try
    Tag:=Integer(ASource);
    ShowModal;
  finally
    Free;
  end
end;

procedure TSeriesTextEditor.FormShow(Sender: TObject);
var tmp : TChartSeries;
    t   : Integer;
begin
  DataSource:=TSeriesTextSource(Tag);
  with DataSource do
  begin
    UpDown1.Position:=HeaderLines;

    if FieldSeparator=',' then CBSep.ItemIndex:=0
    else
    if FieldSeparator=' ' then CBSep.ItemIndex:=1
    else
    if FieldSeparator=#9 then CBSep.ItemIndex:=2
    else
    begin
      CBSep.ItemIndex:=-1;
      CBSep.Text:=FieldSeparator;
    end;

    if Assigned(Owner) then
    begin
      With Owner do
      for t:=0 to ComponentCount-1 do
      if Components[t] is TChartSeries then
      begin
        tmp:=TChartSeries(Components[t]);
        CBSeries.Items.AddObject(SeriesTitleOrName(tmp),tmp);
      end;
      CBSeries.ItemIndex:=CBSeries.Items.IndexOfObject(Series);
      if CBSeries.ItemIndex<>-1 then CBSeriesChange(Self)
      else
      begin
        StringGrid1.Enabled:=False;
        Button4.Enabled:=False;
      end;
    end;
    if FileName<>'' then
      if Copy(Uppercase(FileName),1,7)='HTTP://' then
      begin
        EWeb.Text:=FileName;
        RBWeb.Checked:=True;
        RBFileClick(RBWeb);
      end
      else
      begin
        EFile.Text:=FileName;
        RBFile.Checked:=True;
        RBFileClick(RBFile);
      end;
  end;
end;

procedure TSeriesTextEditor.CBSeriesChange(Sender: TObject);

  Function GetFieldIndexSt(Const AName:String):String;
  var t : Integer;
  begin
    result:='';
    With DataSource do
    for t:=0 to Fields.Count-1 do
    if Uppercase(Fields[t].FieldName)=Uppercase(AName) then
    begin
      Str(Fields[t].FieldIndex,result);
      Exit;
    end;
  end;

var t   : Integer;
    tmp : TChartSeries;
begin
  if CBSeries.ItemIndex<>-1 then
  begin
    StringGrid1.Enabled:=True;
    Button4.Enabled:=True;
    tmp:=SelectedSeries;
    if Assigned(tmp) then
    begin
      StringGrid1.RowCount:=2+tmp.ValuesList.Count;
      StringGrid1.Cells[1,0]:=TeeMsg_Column;
      StringGrid1.Cells[0,1]:=TeeMsg_Text;
      StringGrid1.Cells[1,1]:=GetFieldIndexSt(TeeMsg_Text);
      With tmp do
      for t:=0 to ValuesList.Count-1 do
      begin
        StringGrid1.Cells[0,2+t]:=ValuesList[t].Name;
        StringGrid1.Cells[1,2+t]:=GetFieldIndexSt(ValuesList[t].Name);
      end;
    end;
  end;
end;

Procedure TSeriesTextEditor.SetOptions;
var t: Integer;
begin
  With DataSource do
  begin
    if RBWeb.Checked then FileName:=EWeb.Text else FileName:=EFile.Text;
    HeaderLines:=UpDown1.Position;
    Case CBSep.ItemIndex of
      0: FieldSeparator:=',';
      1: FieldSeparator:=' ';
      2: FieldSeparator:=#9;
    else  FieldSeparator:=CBSep.Text;
    end;
    Fields.Clear;
    if CBSeries.ItemIndex<>-1 then
    begin
      Series:=SelectedSeries;
      for t:=1 to StringGrid1.RowCount-1 do
      begin
        if StringGrid1.Cells[1,t]<>'' then
           AddField(StringGrid1.Cells[0,t],StrToInt(StringGrid1.Cells[1,t]));
      end;
    end;
  end;
end;

Function TSeriesTextEditor.SelectedSeries:TChartSeries;
begin
  With CBSeries do
  if ItemIndex=-1 then result:=nil
                  else result:=TChartSeries(Items.Objects[ItemIndex])
end;

procedure TSeriesTextEditor.Button1Click(Sender: TObject);
begin
  SetOptions;
end;

procedure TSeriesTextEditor.Button4Click(Sender: TObject);
begin
  Screen.Cursor:=crHourGlass;
  try
    SetOptions;
    DataSource.Open;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

procedure TSeriesTextEditor.RBFileClick(Sender: TObject);
begin
  EFile.Enabled:=RBFile.Checked;
  RBWeb.Checked:=not RBFile.Checked;
  EWeb.Enabled:=not EFile.Enabled;
  BBrowse.Enabled:=EFile.Enabled;
end;

procedure TSeriesTextEditor.SpeedButton1Click(Sender: TObject);
begin
  if OpenDialog1.Execute then EFile.Text:=OpenDialog1.FileName;
end;

end.
