{**********************************************}
{   TeeChart Clock Series Editor               }
{   Copyright (c) 1999-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeClockEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeePolarEditor, StdCtrls, ComCtrls, TeCanvas, TeePenDlg;

type
  TClockEditor = class(TPolarSeriesEditor)
    CBRoman: TCheckBox;
    BHour: TButtonPen;
    BMinute: TButtonPen;
    BSecond: TButtonPen;
    BRadius: TButtonPen;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CBRomanClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

Uses TeeRose;

procedure TClockEditor.FormCreate(Sender: TObject);
begin
  inherited;
  ShowControls(False,[CBClockWise,CBClose,UDRadiusInc,SERadiusInc,Label8,BPen]);
end;

procedure TClockEditor.FormShow(Sender: TObject);
begin
  inherited;
  With TClockSeries(Tag) do
  begin
    CBRoman.Checked:=Style=cssRoman;
    BHour.LinkPen(PenHours);
    BMinute.LinkPen(PenMinutes);
    BSecond.LinkPen(PenSeconds);
    BRadius.LinkPen(GetHorizAxis.Grid);
  end;
end;

procedure TClockEditor.CBRomanClick(Sender: TObject);
begin
  with TClockSeries(Tag) do
  if CBRoman.Checked then Style:=cssRoman else Style:=cssDecimal;
end;

initialization
  RegisterClass(TClockEditor);
end.
