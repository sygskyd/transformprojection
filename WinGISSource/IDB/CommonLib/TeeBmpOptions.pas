{*******************************************}
{ TeeChart Pro Bitmap exporting             }
{ Copyright (c) 1995-2000 by David Berneda  }
{         All Rights Reserved               }
{*******************************************}
{$I teedefs.inc}
unit TeeBmpOptions;

interface

uses
  {$IFDEF LINUX}
  LibC,
  {$ELSE}
  Windows, Messages,
  {$ENDIF}
  SysUtils, Classes,
  {$IFDEF CLX}
  QGraphics, QControls, QForms, QDialogs, QStdCtrls,
  {$ELSE}
  Graphics, Controls, Forms, Dialogs, StdCtrls,
  {$ENDIF}
  TeExport;

type
  TBMPOptions = class(TForm)
    CBMono: TCheckBox;
    Label1: TLabel;
    CBColors: TComboBox;
    procedure CBMonoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TBMPExportFormat=class(TTeeExportFormat)
  private
    FProperties: TBMPOptions;
    Procedure CheckProperties;
  protected
    Procedure DoCopyToClipboard; override;
  public
    Function Bitmap:TBitmap;
    function Description:String; override;
    function FileExtension:String; override;
    function FileFilter:String; override;
    Function Options:TForm; override;
    Procedure SaveToStream(Stream:TStream); override;
  end;

implementation

{$R *.dfm}

Uses TeeConst, Clipbrd {$IFDEF CLX}, Types{$ENDIF};

procedure TBMPOptions.CBMonoClick(Sender: TObject);
begin
  CBColors.Enabled:=not CBMono.Checked;
end;

function TBMPExportFormat.Description:String;
begin
  result:=TeeMsg_AsBMP;
end;

function TBMPExportFormat.FileFilter:String;
begin
  result:=TeeMsg_BMPFilter;
end;

function TBMPExportFormat.FileExtension:String;
begin
  result:='bmp';
end;

Function TBMPExportFormat.Bitmap:TBitmap;
var tmp : TBitmap;
    R   : TRect;
    Old : Boolean;
begin
  CheckProperties;
  tmp:=TBitmap.Create;
  tmp.Monochrome:=FProperties.CBMono.Checked;
  {$IFNDEF CLX}
  tmp.PixelFormat:=TPixelFormat(FProperties.CBColors.ItemIndex);
  {$ENDIF}
  CheckSize;
  tmp.Width:=Width;
  tmp.Height:=Height;
  R:=Rect(0,0,tmp.Width,tmp.Height);
  With tmp do
  begin
    Canvas.Brush.Color:=Panel.Color;
    Canvas.FillRect(R);
    Old:=Panel.BufferedDisplay;
    Panel.BufferedDisplay:=False;
    Panel.Draw(Canvas,R);
    Panel.BufferedDisplay:=Old;
  end;
  result:=tmp;
end;

Procedure TBMPExportFormat.CheckProperties;
begin
  if not Assigned(FProperties) then FProperties:=TBMPOptions.Create(nil)
end;

Function TBMPExportFormat.Options:TForm;
begin
  CheckProperties;
  result:=FProperties;
end;

procedure TBMPExportFormat.DoCopyToClipboard;
var tmp : TBitmap;
begin
  tmp:=Bitmap;
  try
    Clipboard.Assign(tmp);
  finally
    tmp.Free;
  end;
end;

procedure TBMPExportFormat.SaveToStream(Stream:TStream);
begin
  with Bitmap do
  try
    SaveToStream(Stream);
  finally
    Free;
  end;
end;

procedure TBMPOptions.FormShow(Sender: TObject);
begin
  CBColors.ItemIndex:=0;
end;

initialization
  RegisterTeeExportFormat(TBMPExportFormat);
finalization
  UnRegisterTeeExportFormat(TBMPExportFormat);
end.
