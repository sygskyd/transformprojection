{*****************************************}
{   TeeChart Pro                          }
{   Component Registration Unit           }
{   Copyright (c) 1995-2000 David Berneda }
{*****************************************}
unit TeeChart;

interface

Procedure Register;

implementation

Uses ChartReg,   { <-- Basic Series and Functions }
     ChartPro;   { <-- Extended Series and Functions }

Procedure Register;
Begin
  ChartReg.Register;
  ChartPro.Register;
End;

end.
