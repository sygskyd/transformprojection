{**********************************************}
{   TCandleSeries (derived from OHLCSeries)    }
{   TVolumeSeries (derived from TCustomSeries) }
{   TADXFunction                               }
{                                              }
{   Copyright (c) 1995-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit CandleCh;

interface

{
  Financial TCandleSeries derives from TOHLCSeries (Open, High, Low & Close).
  See OHLChart.pas unit for TOHLCSeries source code.

  TCandleSeries overrides the TChartSeries.DrawValue method to paint its
  points in several financial styles (CandleStick, Bars, OpenClose, etc).

  TVolumeSeries overrides the TChartSeries.DrawValue method to paint
  points as thin vertical bars.

  TADXFunction is a commonly used financial indicator. It requires a
  OHLC (financial) series as the datasource.
}
Uses Windows, Classes, Graphics, Chart, Series, OHLChart, TeEngine,
     TeCanvas;

Const DefCandleWidth = 4;  { 2 + 1 + 2  Default width for Candle points }

type
   TCandleStyle=(csCandleStick,csCandleBar,csOpenClose);

   TCandleSeries=class(TOHLCSeries)
   private
     FCandleStyle    : TCandleStyle;
     FCandleWidth    : Integer;
     FDownCloseColor : TColor;
     FShowCloseTick  : Boolean;
     FShowOpenTick   : Boolean;
     FUpCloseColor   : TColor;
     Function GetDark3D:Boolean;
     Function GetDraw3D:Boolean;
     Function GetPen:TChartPen;
     procedure SetCandlePen(Value:TChartPen);
     Procedure SetCandleStyle(Value:TCandleStyle);
     Procedure SetCandleWidth(Value:Integer);
     procedure SetDark3D(Value:Boolean);
     Procedure SetDownColor(Value:TColor);
     procedure SetDraw3D(Value:Boolean);
     Procedure SetShowCloseTick(Value:Boolean);
     Procedure SetShowOpenTick(Value:Boolean);
     Procedure SetUpColor(Value:TColor);
   protected
     Function CalculateColor(ValueIndex:Integer):TColor;
     class Procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); override;
     procedure DrawValue(ValueIndex:Integer); override;
     class Function  GetEditorClass:String; override;
     Procedure PrepareForGallery(IsEnabled:Boolean); override;
     class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); override;
   public
     Constructor Create(AOwner: TComponent); override;

     Procedure Assign(Source:TPersistent); override;
     Function AddCandle( Const ADate:TDateTime;
                         Const AOpen,AHigh,ALow,AClose:Double):Integer;
   published
     property Active;
     property ColorEachPoint;
     property ColorSource;
     property Cursor;
     property Depth;
     property HorizAxis;
     property Marks;
     property ParentChart;
     property DataSource;
     property PercentFormat;
     property SeriesColor;
     property ShowInLegend;
     property Title;
     property ValueFormat;
     property VertAxis;
     property XLabelsSource;

     { events }
     property AfterDrawValues;
     property BeforeDrawValues;
     property OnAfterAdd;
     property OnBeforeAdd;
     property OnClearValues;
     property OnClick;
     property OnDblClick;
     property OnGetMarkText;

     property CandleStyle:TCandleStyle read FCandleStyle write SetCandleStyle
                                       default csCandleStick;
     property CandleWidth:Integer read FCandleWidth write SetCandleWidth
                                  default DefCandleWidth;
     property Draw3D:Boolean read GetDraw3D write SetDraw3D default False;
     property Dark3D:Boolean read GetDark3D write SetDark3D default True;
     property DownCloseColor:TColor read FDownCloseColor write SetDownColor
                                    default clRed;
     property ShowCloseTick:Boolean read FShowCloseTick write SetShowCloseTick
                                    default True;
     property ShowOpenTick:Boolean read FShowOpenTick write SetShowOpenTick
                                   default True;
     property UpCloseColor:TColor read FUpCloseColor write SetUpColor
                                  default clWhite;
     property Pen:TChartPen read GetPen write SetCandlePen;
   end;

   { Used in financial charts for Volume quantities (or OpenInterest) }
   { Overrides FillSampleValues to create random POSITIVE values }
   { Overrides DrawValue to paint a thin vertical bar }
   { Declares VolumeValues (same like YValues) }
   TVolumeSeries=class(TCustomSeries)
   private
     Function GetVolumeValues:TChartValueList;
     Procedure SetVolumeValues(Value:TChartValueList);
   protected
     Procedure AddSampleValues(NumValues:Integer); override;
     class Procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); override;
     procedure DrawValue(ValueIndex:Integer); override;
     class Function GetEditorClass:String; override;
     Procedure PrepareForGallery(IsEnabled:Boolean); override;
     Procedure SetSeriesColor(AColor:TColor); override;
     class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); override;
   public
     OriginZero : Boolean;
     Constructor Create(AOwner: TComponent); override;
     Function NumSampleValues:Integer; override;
   published
     property Active;
     property ColorEachPoint;
     property ColorSource;
     property Cursor;
     property HorizAxis;
     property Marks;
     property ParentChart;
     property DataSource;
     property PercentFormat;
     property SeriesColor;
     property ShowInLegend;
     property Title;
     property ValueFormat;
     property VertAxis;
     property XLabelsSource;

     { events }
     property AfterDrawValues;
     property BeforeDrawValues;
     property OnAfterAdd;
     property OnBeforeAdd;
     property OnClearValues;
     property OnClick;
     property OnDblClick;
     property OnGetMarkText;

     property LinePen;
     property VolumeValues:TChartValueList read GetVolumeValues write SetVolumeValues;
     property XValues;
   end;

   { Financial A.D.X function }
   TADXFunction=class(TTeeFunction)
   private
     IDMDown : TFastLineSeries;
     IDMUp   : TFastLineSeries;
   public
     Constructor Create(AOwner:TComponent); override;
     procedure AddPoints(Source:TChartSeries); override;
     property DMDown:TFastLineSeries read IDMDown;
     property DMUp:TFastLineSeries read IDMUp;
   end;

implementation

Uses SysUtils, TeeProCo, TeeConst;

{ TCandleSeries }
Constructor TCandleSeries.Create(AOwner: TComponent);
Begin
  inherited;
  FUpCloseColor  :=clWhite;
  FDownCloseColor:=clRed;
  FCandleWidth   :=DefCandleWidth;
  FCandleStyle   :=csCandleStick;
  FShowOpenTick  :=True;
  FShowCloseTick :=True;
  Pointer.Draw3D :=False;
end;

Procedure TCandleSeries.SetShowOpenTick(Value:Boolean);
Begin
  SetBooleanProperty(FShowOpenTick,Value);
End;

Procedure TCandleSeries.SetShowCloseTick(Value:Boolean);
Begin
  SetBooleanProperty(FShowCloseTick,Value);
End;

Function TCandleSeries.CalculateColor(ValueIndex:Integer):TColor;
Begin
  result:=ValueColor[ValueIndex];
  if result=SeriesColor then
  With ParentChart.Canvas do
  begin
    if OpenValues.Value[ValueIndex]>CloseValues.Value[ValueIndex] then
       result:=FUpCloseColor
    else
    if OpenValues.Value[ValueIndex]<CloseValues.Value[ValueIndex] then
       result:=FDownCloseColor
    else
    Begin
      { color algorithm when open is equal to close }
      if ValueIndex=0 then
         result:=FUpCloseColor  { <-- first point }
      else
      if CloseValues.Value[ValueIndex-1]>CloseValues.Value[ValueIndex] then
         result:=FDownCloseColor
      else
      if CloseValues.Value[ValueIndex-1]<CloseValues.Value[ValueIndex] then
         result:=FUpCloseColor
      else
         result:=ValueColor[ValueIndex-1];
    end;
  end;
end;

type TPointerAccess=class(TSeriesPointer);

procedure TCandleSeries.DrawValue(ValueIndex:Integer);
var yOpen  : Integer;
    yClose : Integer;
    yHigh  : Integer;
    yLow   : Integer;
    x             : Integer;
    tmpLeftWidth  : Integer;
    tmpRightWidth : Integer;
    tmpTop        : Integer;
    tmpBottom     : Integer;
Begin
  TPointerAccess(Pointer).PrepareCanvas(ParentChart.Canvas,clTeeColor); { Pointer Pen and Brush styles }
  With ParentChart,Canvas do
  Begin
    X:=CalcXPosValue(DateValues.Value[ValueIndex]); { The horizontal position }

    { Vertical positions of Open, High, Low & Close values for this point }
    YOpen :=CalcYPosValue(OpenValues.Value[ValueIndex]);
    YHigh :=CalcYPosValue(HighValues.Value[ValueIndex]);
    YLow  :=CalcYPosValue(LowValues.Value[ValueIndex]);
    YClose:=CalcYPosValue(CloseValues.Value[ValueIndex]);

    tmpLeftWidth:=FCandleWidth div 2; { calc half Candle Width }
    tmpRightWidth:=FCandleWidth-tmpLeftWidth;

    if (FCandleStyle=csCandleStick) or (FCandleStyle=csOpenClose) then
    Begin { draw Candle Stick }

      if View3D and Pointer.Draw3D then
      begin
        tmpTop:=yClose;
        tmpBottom:=yOpen;
        if tmpTop>tmpBottom then SwapInteger(tmpTop,tmpBottom);

        { Draw Candle Vertical Line from bottom to Low }
        if FCandleStyle=csCandleStick then VertLine3D(x,tmpBottom,yLow,MiddleZ);

        { Draw 3D Candle }
        Brush.Color:=CalculateColor(ValueIndex);
        if yOpen=yClose then Pen.Color:=CalculateColor(ValueIndex);
        Cube( x-tmpLeftWidth,x+tmpRightWidth,tmpTop,tmpBottom,
              StartZ,EndZ,Pointer.Dark3D);

        { Draw Candle Vertical Line from Top to High }
        if FCandleStyle=csCandleStick then VertLine3D(x,tmpTop,yHigh,MiddleZ);
      end
      else
      begin
        { Draw Candle Vertical Line from High to Low }
        if FCandleStyle=csCandleStick then VertLine3D(x,yLow,yHigh,MiddleZ);
        { remember that Y coordinates are inverted }
        if yOpen=yClose then
        begin
          Pen.Color:=CalculateColor(ValueIndex);
          HorizLine3D(x-tmpLeftWidth,x+tmpRightWidth,yOpen,MiddleZ);
        end
        else
        begin
          Brush.Color:=CalculateColor(ValueIndex);
          RectangleWithZ(Rect(x-tmpLeftWidth,yOpen,x+tmpRightWidth,yClose),MiddleZ);
        end;
      end;
    end
    else
    Begin { draw Candle bar }
      AssignVisiblePenColor(Self.Pen,CalculateColor(ValueIndex));
      BackMode:=cbmTransparent;
      { Draw Candle Vertical Line from High to Low }
      VertLine3D(x,yLow,yHigh,MiddleZ);
      if FShowOpenTick then HorizLine3D(x,x-tmpLeftWidth,yOpen,MiddleZ);
      if FShowCloseTick then HorizLine3D(x,x+tmpRightWidth,yClose,MiddleZ);
    end;
  end;
end;

Procedure TCandleSeries.SetUpColor(Value:TColor);
Begin
  SetColorProperty(FUpCloseColor,Value);
end;

Procedure TCandleSeries.SetDownColor(Value:TColor);
Begin
  SetColorProperty(FDownCloseColor,Value);
end;

Procedure TCandleSeries.SetCandleWidth(Value:Integer);
Begin
  SetIntegerProperty(FCandleWidth,Value);
end;

Procedure TCandleSeries.SetCandleStyle(Value:TCandleStyle);
Begin
  if FCandleStyle<>Value then
  begin
    FCandleStyle:=Value;
    Repaint;
  end;
end;

class Function TCandleSeries.GetEditorClass:String;
Begin
  result:='TCandleEditor';  { <-- do not translate }
End;

Procedure TCandleSeries.PrepareForGallery(IsEnabled:Boolean);
Begin
  inherited;
  FillSampleValues(4);
  ColorEachPoint:=True;
  if IsEnabled then
     UpCloseColor:=clBlue
  else
  begin
    UpCloseColor:=clSilver;
    DownCloseColor:=clSilver;
    Pointer.Pen.Color:=clGray;
  end;
  Pointer.Pen.Width:=2;
  CandleWidth:=12;
end;

Procedure TCandleSeries.Assign(Source:TPersistent);
begin
  if Source is TCandleSeries then
  With TCandleSeries(Source) do
  begin
    Self.FCandleWidth   :=FCandleWidth;
    Self.FCandleStyle   :=FCandleStyle;
    Self.FUpCloseColor  :=FUpCloseColor;
    Self.FDownCloseColor:=FDownCloseColor;
    Self.FShowOpenTick  :=FShowOpenTick;
    Self.FShowCloseTick :=FShowCloseTick;
  end;
  inherited;
end;

Function TCandleSeries.GetDraw3D:Boolean;
begin
  result:=Pointer.Draw3D;
end;

procedure TCandleSeries.SetDraw3D(Value:Boolean);
begin
  Pointer.Draw3D:=Value;
end;

Function TCandleSeries.GetDark3D:Boolean;
begin
  result:=Pointer.Dark3D;
end;

procedure TCandleSeries.SetDark3D(Value:Boolean);
begin
  Pointer.Dark3D:=Value;
end;

Function TCandleSeries.GetPen:TChartPen;
begin
  result:=Pointer.Pen;
end;

procedure TCandleSeries.SetCandlePen(Value:TChartPen);
begin
  Pointer.Pen.Assign(Value);
end;

Function TCandleSeries.AddCandle( Const ADate:TDateTime;
                                  Const AOpen,AHigh,ALow,AClose:Double):Integer;
begin
  result:=AddOHLC(ADate,AOpen,AHigh,ALow,AClose);
end;

class procedure TCandleSeries.CreateSubGallery(
  AddSubChart: TChartSubGalleryProc);
begin
  inherited;
  AddSubChart(TeeMsg_CandleBar);
  AddSubChart(TeeMsg_CandleNoOpen);
  AddSubChart(TeeMsg_CandleNoClose);
  AddSubChart(TeeMsg_NoBorder);
end;

class procedure TCandleSeries.SetSubGallery(ASeries: TChartSeries;
  Index: Integer);
begin
  With TCandleSeries(ASeries) do
  Case Index of
    1: CandleStyle:=csCandleBar;
    2: begin Pen.Visible:=True; CandleStyle:=csCandleBar; ShowOpenTick:=False; end;
    3: begin Pen.Visible:=True; CandleStyle:=csCandleBar; ShowCloseTick:=False; end;
    4: begin CandleStyle:=csCandleStick; Pen.Visible:=False; end;
  else inherited;
  end;
end;

{ TVolumeSeries }
Constructor TVolumeSeries.Create(AOwner: TComponent);
begin
  inherited;
  DrawArea:=False;
  Pointer.Visible:=False;
  OriginZero:=False;
end;

Function TVolumeSeries.GetVolumeValues:TChartValueList;
Begin
  result:=YValues;
end;

Procedure TVolumeSeries.SetSeriesColor(AColor:TColor);
begin
  inherited;
  LinePen.Color:=AColor;
end;

Procedure TVolumeSeries.SetVolumeValues(Value:TChartValueList);
Begin
  SetYValues(Value);
end;

procedure TVolumeSeries.DrawValue(ValueIndex:Integer);
var tmpY : Integer;
Begin
  With ParentChart.Canvas do
  Begin
    AssignVisiblePenColor(LinePen,ValueColor[ValueIndex]);
    { moves to x,y coordinates and draws a vertical bar to top or bottom,
      depending on the vertical Axis.Inverted property }
    BackMode:=cbmTransparent;
    if OriginZero then
       tmpY:=CalcYPos(0)
    else
    With GetVertAxis do
       if Inverted then tmpY:=IStartPos else tmpY:=IEndPos;

    VertLine3D(CalcXPos(ValueIndex),CalcYPos(ValueIndex),tmpY,MiddleZ);
  end;
end;

Function TVolumeSeries.NumSampleValues:Integer;
Begin
  result:=40;
end;

Procedure TVolumeSeries.AddSampleValues(NumValues:Integer);
Var t : Integer;
begin
  With RandomBounds(NumValues) do
  for t:=1 to NumValues do
  Begin
    AddXY(tmpX,System.Random(Round(DifY) div 15){$IFNDEF D4},'', clTeeColor{$ENDIF});
    tmpX:=tmpX+StepX;
  end;
end;

Procedure TVolumeSeries.PrepareForGallery(IsEnabled:Boolean);
begin
  inherited;
  FillSampleValues(26);
  Pointer.InflateMargins:=True;
end;

class Function TVolumeSeries.GetEditorClass:String;
Begin
  result:='TVolumeSeriesEditor';
End;

class procedure TVolumeSeries.CreateSubGallery(
  AddSubChart: TChartSubGalleryProc);
begin
  inherited;
  AddSubChart(TeeMsg_Dotted);
  AddSubChart(TeeMsg_Colors);
end;

class procedure TVolumeSeries.SetSubGallery(ASeries: TChartSeries;
  Index: Integer);
begin
  With TVolumeSeries(ASeries) do
  Case Index of
    1: Pen.SmallDots:=True;
    2: ColorEachPoint:=True;
  else inherited
  end;
end;

type TChartSeriesAccess=class(TChartSeries);

{ TADXFunction }
Constructor TADXFunction.Create(AOwner: TComponent);

  Procedure HideSeries(ASeries:TChartSeries);
  begin
    ASeries.ShowInLegend:=False;
    TChartSeriesAccess(ASeries).InternalUse:=True;
  end;

begin
  inherited;
  InternalSetPeriod(14);

  IDMDown:=TFastLineSeries.Create(Self);
  HideSeries(IDMDown);
  IDMDown.SeriesColor:=clRed;

  IDMUp:=TFastLineSeries.Create(Self);
  HideSeries(IDMUp);
  IDMUp.SeriesColor:=clGreen;
end;

procedure TADXFunction.AddPoints(Source: TChartSeries);

  Procedure PrepareSeries(ASeries:TChartSeries);
  begin
    With ASeries do
    begin
      ParentChart:=ParentSeries.ParentChart;
      CustomVertAxis:=ParentSeries.CustomVertAxis;
      VertAxis:=ParentSeries.VertAxis;
      XValues.DateTime:=ParentSeries.XValues.DateTime;
      AfterDrawValues:=ParentSeries.AfterDrawValues;
      BeforeDrawValues:=ParentSeries.BeforeDrawValues;
    end;
  end;

  Function CalcADX(Index:Integer):Double;
  begin
    Index:=Index-Round(Period);
    result:=(100*Abs(IDMUp.YValues.Value[Index]-IDMDown.YValues.Value[Index])/
                    (IDMUp.YValues.Value[Index]+IDMDown.YValues.Value[Index]));
  end;

var tmpTR,
    tmpDMUp,
    tmpDMDown : Array{$IFNDEF D4}[0..10000]{$ENDIF} of Double;
    t         : Integer;
    tt        : Integer;
    tmpClose  : Double;
    Closes,
    Highs,
    Lows      : TChartValueList;
    tmpTR2,
    tmpUp2,
    tmpDown2  : Double;
    tmpX      : Double;
    tmp       : Integer;
    tmpADX    : Double;
begin
  ParentSeries.Clear;
  IDMUp.Clear;
  IDMDown.Clear;
  PrepareSeries(IDMUp);
  PrepareSeries(IDMDown);
  if Source.Count>=(2*Period) then
  begin
    With TOHLCSeries(Source) do
    begin
      Closes:=CloseValues;
      Highs:=HighValues;
      Lows:=LowValues;
    end;

    {$IFDEF D4}
    SetLength(tmpTR, Source.Count);
    SetLength(tmpDMUp, Source.Count);
    SetLength(tmpDMDown, Source.Count);
    {$ENDIF}
    for t:=1 to Source.Count-1 do
    begin
      tmpClose:=Closes.Value[t-1];
      tmpTR[t]:=Highs.Value[t]-Lows.Value[t];
      tmpTR[t]:=MaxDouble(tmpTR[t],Abs(Highs.Value[t]-tmpClose));
      tmpTR[t]:=MaxDouble(tmpTR[t],Abs(Lows.Value[t]-tmpClose));

      if (Highs.Value[t]-Highs.Value[t-1])>(Lows.Value[t-1]-Lows.Value[t]) then
          tmpDMUp[t]:=MaxDouble(0,Highs.Value[t]-Highs.Value[t-1])
      else
          tmpDMUp[t]:=0;

      if (Lows.Value[t-1]-Lows.Value[t])>(Highs.Value[t]-Highs.Value[t-1]) then
          tmpDMDown[t]:=MaxDouble(0,Lows.Value[t-1]-Lows.Value[t])
      else
          tmpDMDown[t]:=0;
    end;

    tmpTR2:=0;
    tmpUp2:=0;
    tmpDown2:=0;

    tmp:=Round(Period);
    for t:=tmp to Source.Count-1 do
    begin
      if t=tmp then
      begin
        for tt:=1 to Round(Period) do
        begin
          tmpTR2  :=tmpTR2+tmpTR[tt];
          tmpUp2  :=tmpUp2+tmpDMUp[tt];
          tmpDown2:=tmpDown2+tmpDMDown[tt];
        end;
      end
      else
      begin
        tmpTR2:=tmpTR2-(tmpTR2/Period)+tmpTR[t];
        tmpUp2:=tmpUp2-(tmpUp2/Period)+tmpDMUp[t];
        tmpDown2:=tmpDown2-(tmpDown2/Period)+tmpDMDown[t];
      end;
      tmpX:=Source.XValues[t];
      IDMUp.AddXY( tmpX, 100*(tmpUp2/tmpTR2) {$IFNDEF D4},'',clTeeColor{$ENDIF});
      IDMDown.AddXY( tmpX, 100*(tmpDown2/tmpTR2){$IFNDEF D4},'',clTeeColor{$ENDIF});
    end;

    {$IFDEF D4}
    tmpTR:=nil;
    tmpDMUp:=nil;
    tmpDMDown:=nil;
    {$ENDIF}

    tmpADX:=0;

    tmp:=Round((2*Period)-2);
    for t:=tmp to Source.Count-1 do
    begin
      if t=tmp then
      begin
        tmpADX:=0;
        for tt:=Round(Period) to tmp do tmpADX:=tmpADX+CalcADX(tt);
        tmpADX:=tmpADX/(Period-1);
      end
      else
      begin
        tmpADX:=((tmpADX*(Period-1))+(CalcADX(t)))/Period;
      end;
      ParentSeries.AddXY(Source.XValues[t],tmpADX{$IFNDEF D4},'',clTeeColor{$ENDIF});
    end;
  end;
end;

initialization
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{
   RegisterTeeSeries( TCandleSeries, TeeMsg_GalleryCandle,
                      TeeMsg_GalleryFinancial, 1);
   RegisterTeeSeries( TVolumeSeries, TeeMsg_GalleryVolume,
                      TeeMsg_GalleryFinancial, 1);
   RegisterTeeFunction( TADXFunction, TeeMsg_FunctionADX, TeeMsg_GalleryFinancial, 1 );
}
   RegisterTeeSeries( TCandleSeries, TeeMsg_GalleryCandle,
                      TeeMsg_GalleryFinancial, 1, stCandle);
   RegisterTeeSeries( TVolumeSeries, TeeMsg_GalleryVolume,
                      TeeMsg_GalleryFinancial, 1, stVolume);
   RegisterTeeFunction( TADXFunction, TeeMsg_FunctionADX, TeeMsg_GalleryFinancial, 1 , ftADX );
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
finalization
  UnRegisterTeeSeries([TCandleSeries,TVolumeSeries]);
  UnRegisterTeeFunctions([ TADXFunction ]);
end.
