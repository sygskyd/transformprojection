�
 TFORMTEEPANEL 0�  TPF0TFormTeePanelFormTeePanelLeft� TopfBorderIconsbiSystemMenu BorderStylebsDialogCaptionPanel EditorClientHeight� ClientWidth+Color	clBtnFace
ParentFont	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TPageControlPageControl1Left Top Width+Height� 
ActivePage
TabBordersAlignalClientHotTrack	TabOrder  	TTabSheetTabBackCaption
Background TButtonColorBPanelColorLeftTopWidthkHelpContext�Caption&Panel Color...TabOrder OnClickBPanelColorClick  	TGroupBoxGB6LeftTop,Width� Height]HelpContextYCaption
Back ImageTabOrder TRadioGroupRGBitmapLeftTTopWidthIHeightIHelpContext[CaptionSt&yle	ItemIndex Items.StringsStretchTileCenter TabOrder OnClickRGBitmapClick  TButtonBBrowseImageLeftTopWidthEHeightHelpContext�Caption
B&rowse...TabOrderOnClickBBrowseImageClick  	TCheckBoxCBImageInsideLeftTop<WidthAHeightHelpContextZCaptionI&nsideTabOrderOnClickCBImageInsideClick    	TTabSheet
TabBordersCaptionBorders TLabelL19LeftTopdWidthHeight	AlignmenttaRightJustifyAutoSizeCaption&Width:FocusControl
SEPanelBor  TLabelL2Left^TopdWidthHeight	AlignmenttaRightJustifyAutoSizeCaptionWidt&h:FocusControl	SEPanelWi  TRadioGroup	RGBevelInLeftTopWidthOHeightQHelpContext�CaptionBevel &Inner:Items.StringsNoneLoweredRaised TabOrder OnClickRGBevelInClick  TRadioGroup
RGBevelOutLeftbTopWidthKHeightQHelpContext�CaptionBevel &Outer:Items.StringsNoneLoweredRaised TabOrderOnClickRGBevelOutClick  TEdit
SEPanelBorLeft*Top_WidthHeightHelpContext�ColorclWindowTabOrderText0OnChangeSEPanelBorChange  TEdit	SEPanelWiLeft� Top_WidthHeightHelpContext�ColorclWindowTabOrderText1OnChangeSEPanelWiChange  TUpDown	UDPanelWiLeft� Top_WidthHeight	Associate	SEPanelWiMin PositionTabOrderWrap  TUpDown
UDPanelBorLeftHTop_WidthHeight	Associate
SEPanelBorMin Position TabOrderWrap  	TCheckBoxCBPanelBorderLeft� TopWidth7HeightHelpContext�Caption&BorderTabOrderOnClickCBPanelBorderClick   	TTabSheetTabGradientCaptionGradient    