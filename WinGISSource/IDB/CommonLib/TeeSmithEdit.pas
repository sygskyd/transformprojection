{******************************************}
{ TeeChart. Smith Series Editor            }
{ Copyright (c) 1996-2000 by David Berneda }
{******************************************}
{$I teedefs.inc}
unit TeeSmithEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeeSmith, StdCtrls, TeCanvas, TeePenDlg;

type
  TSmithSeriesEdit = class(TForm)
    CBC: TCheckBox;
    CBR: TCheckBox;
    BCPen: TButtonPen;
    BRPen: TButtonPen;
    BCircle: TButtonPen;
    CBColorEach: TCheckBox;
    BBorder: TButtonPen;
    BCFont: TButton;
    BRFont: TButton;
    procedure FormShow(Sender: TObject);
    procedure CBCClick(Sender: TObject);
    procedure CBRClick(Sender: TObject);
    procedure CBColorEachClick(Sender: TObject);
    procedure BCFontClick(Sender: TObject);
    procedure BRFontClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Smith : TSmithSeries;
  end;

implementation

{$R *.dfm}
Uses TeeEdiSeri, TeePoEdi, TeeCircledEdit, TeeConst, TeeProcs, TeeBrushDlg;

procedure TSmithSeriesEdit.FormShow(Sender: TObject);
begin
  Smith:=TSmithSeries(Tag);
  With Smith do
  begin
    CBC.Checked:=CLabels;
    CBR.Checked:=RLabels;
    BCPen.LinkPen(CCirclePen);
    BRPen.LinkPen(RCirclePen);
    BCircle.LinkPen(CirclePen);
    BBorder.LinkPen(Pen);
    CBColorEach.Checked:=ColorEachPoint;
  end;
  TFormTeeSeries(Parent.Owner).InsertSeriesForm( TCircledSeriesEditor,
                                                 1,TeeMsg_GalleryCircled,
                                                 Smith);
  TeeInsertPointerForm(Parent,Smith.Pointer,TeeMsg_GalleryPoint);
end;

procedure TSmithSeriesEdit.CBCClick(Sender: TObject);
begin
  Smith.CLabels:=CBC.Checked;
end;

procedure TSmithSeriesEdit.CBRClick(Sender: TObject);
begin
  Smith.RLabels:=CBR.Checked;
end;

procedure TSmithSeriesEdit.CBColorEachClick(Sender: TObject);
begin
  Smith.ColorEachPoint:=CBColorEach.Checked;
end;

procedure TSmithSeriesEdit.BCFontClick(Sender: TObject);
begin
  EditTeeFont(Self,Smith.CLabelsFont);
end;

procedure TSmithSeriesEdit.BRFontClick(Sender: TObject);
begin
  EditTeeFont(Self,Smith.RLabelsFont);
end;

initialization
  RegisterClass(TSmithSeriesEdit);
end.
