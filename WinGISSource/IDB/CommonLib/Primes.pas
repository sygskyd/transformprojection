const  N = 30000000;
var a: array[1..N] of Boolean;

procedure TForm1.FormCreate(Sender: TObject);
var Buffer : Array[0..20] of Char;
    Read:Integer;
 i,j,k,l:Integer;
 f:TextFile;
 last:integer;
 Diff:Integer;
begin
  assignfile(f,'primes.inc');
  rewrite(f);
  writeln(f,'Const Primes : Array[0..] of Integer = (');
  for i:=1 to N do a[i]:=true;
  a[2]:=FALSE;
  for i:=2 to N Div 2 do
    for j:=2 to N div i do a[i*j]:=false;
  k:=0;
  l:=0;
  last:=1;
  for i:=1 to N do if a[i] then begin
    Inc(k);
    if i < 100 then Diff:=20
    else if i < 1000 then Diff:=50
    else if i < 10000 then Diff:=1000
    else if i < 100000 then Diff:=5000
    else if i < 1000000 then Diff:=10000
    else if i < 10000000 then Diff:=50000
    else Diff:=100000;
    if i-last > Diff then begin
      Last:=i;
      write(f,i:8,', ');
      inc(l);
      if l Mod 10 = 0 then Writeln(f);
    end;
  end;
  writeln(f,l);
  CloseFile(f);
end;

