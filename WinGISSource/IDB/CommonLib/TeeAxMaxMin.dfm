�
 TAXISMAXMIN 0�  TPF0TAxisMaxMin
AxisMaxMinLeft� Top(ActiveControlEMaximumBorderIconsbiSystemMenu BorderStylebsDialogCaptionAxis Maximum and MinimumClientHeightOClientWidth
ParentFont	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTopWidth)Height	AlignmenttaRightJustifyAutoSizeCaption&Date:FocusControlEMaximum  TLabelLabel2LeftTop0Width5Height	AlignmenttaRightJustifyAutoSizeCaption&Time:FocusControlEMinimum  TButtonBitBtn1Left� TopWidthKHeightCaptionOKDefault	TabOrderOnClickBitBtn1Click  TButtonBitBtn2Left� Top+WidthKHeightCancel	CaptionCancelModalResultTabOrder  TEditEMaximumLeftATopWidthlHeightHint
Enter DateHelpContext� ParentShowHintShowHint	TabOrder   TEditEMinimumLeftATop,WidthlHeightHint
Enter TimeHelpContext� TabOrder   