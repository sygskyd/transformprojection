{**********************************************}
{  TCustomChart (or derived) Editor Dialog     }
{  Copyright (c) 1996-2000 by David Berneda    }
{**********************************************}
{$I teedefs.inc}
unit TeeEditCha;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QStdCtrls, QExtCtrls, QComCtrls, QButtons,
     {$ELSE}
     Graphics, Controls, Forms, StdCtrls, ExtCtrls, ComCtrls, Buttons,
     {$ENDIF}
     Chart, TeEngine, TeeProcs, TeeEdiSeri, TeeLisB, TeeChartGrid, MultiLng;

Const teeEditMainPage    =0;
      teeEditGeneralPage =1;
      teeEditAxisPage    =2;
      teeEditTitlePage   =3;
      teeEditLegendPage  =4;
      teeEditPanelPage   =5;
      teeEditPagingPage  =6;
      teeEditWallsPage   =7;
      teeEdit3DPage      =8;

type
  TChartEditorOption=( ceAdd,
                       ceDelete,
                       ceChange,
                       ceClone,
                       ceDataSource,
                       ceTitle,
                       ceHelp  );

Const eoAll=[Low(TChartEditorOption)..High(TChartEditorOption)];

type
  TChartEditorTab=( cetMain,
                    cetGeneral,
                    cetAxis,
                    cetTitles,
                    cetLegend,
                    cetPanel,
                    cetPaging,
                    cetWalls,
                    cet3D,
                    cetSeriesGeneral,
                    cetSeriesMarks,
                    cetAllSeries,
                    cetSeriesData,
                    cetExport,
                    cetTools,
                    cetPrintPreview
                   );

  TChartEditorOptions=set of TChartEditorOption;
  TChartEditorHiddenTabs=set of TChartEditorTab;

  TChartEditForm = class(TForm)
    MainPage           : TPageControl;
    TabChart           : TTabSheet;
    SubPage            : TPageControl;
    TabSeriesList      : TTabSheet;
    TabAxis            : TTabSheet;
    TabGeneral         : TTabSheet;
    TabTitle           : TTabSheet;
    TabLegend          : TTabSheet;
    TabPanel           : TTabSheet;
    TabPaging          : TTabSheet;
    TabWalls           : TTabSheet;
    TabSeries          : TTabSheet;
    Tab3D              : TTabSheet;
    LBSeries           : TChartListBox;
    TabData            : TTabSheet;
    TabExport          : TTabSheet;
    PanBottom          : TPanel;
    LabelWWW           : TLabel;
    TabTools           : TTabSheet;
    TabPrint           : TTabSheet;
    PanRight           : TPanel;
    BMoveUP            : TSpeedButton;
    BMoveDown          : TSpeedButton;
    BAddSeries         : TButton;
    BDeleteSeries      : TButton;
    BRenameSeries      : TButton;
    BCloneSeries       : TButton;
    PanTop             : TPanel;
    PanBot             : TPanel;
    PanLeft            : TPanel;
    BClose: TButton;
    Panel1: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    MlgSection1: TMlgSection;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SubPageChange(Sender: TObject);
    procedure BCloseClick(Sender: TObject);
    procedure BMoveUPClick(Sender: TObject);
    procedure BMoveDownClick(Sender: TObject);
    procedure BAddSeriesClick(Sender: TObject);
    procedure BDeleteSeriesClick(Sender: TObject);
    procedure BRenameSeriesClick(Sender: TObject);
    procedure BCloneSeriesClick(Sender: TObject);
    procedure MainPageChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure LBSeriesEditSeries(Sender: TChartListBox; Index: Integer);
    procedure LBSeriesOtherItemsChange(Sender: TObject);
    procedure LBSeriesRefreshButtons(Sender: TObject);
    procedure BChangeTypeSeriesClick(Sender: TObject);
    procedure LabelWWWClick(Sender: TObject);
    procedure ButtonHelpClick(Sender: TObject);
    procedure MainPageChanging(Sender: TObject; var AllowChange: Boolean);
  private
    { Private declarations }
    TheChart           : TCustomChart;
    FGrid              : TChartGrid;
    procedure SetChart(Value:TCustomChart);
  protected
    {$IFNDEF CLX}
    procedure WMHelp(var Message: TWMHelp); message WM_HELP;
    {$ENDIF}
  public
    { Public declarations }
    TheAxis            : TChartAxis;
    TheTitle           : TChartTitle;
    TheTool            : TTeeCustomTool;
    TheWall            : TChartWall;
    TheSeries          : TChartSeries;
    TheEditSeries      : TChartSeries;
    TheActivePageIndex : Integer;
    TheHiddenTabs      : TChartEditorHiddenTabs;

    EditorOptions      : TChartEditorOptions;
    IsDssGraph         : Boolean;
    TheFormSeries      : TFormTeeSeries;
    Procedure CheckHelpFile;
    procedure SetTabSeries;
    property Chart     : TCustomChart read TheChart write SetChart;
  end;

Function GetTeeChartHelpFile:String;

type TTeeOnShowEditor=Procedure(Editor:TChartEditForm);

var TeeOnShowEditor    : TTeeOnShowEditor=nil;

implementation

{$R *.dfm}

uses TeePenDlg, TeeConst, Series, TeExport, TeeFunci,
     TeeEdiAxis, TeeEdiLege, TeeEdiPane, TeeEdiTitl, TeeEdiWall, TeeEdiGene,
     TeeEdiPage, TeeEdi3D,
     {$IFNDEF CLX}
     Registry,
     {$ENDIF}
     TeeEditTools, TeePrevi, TeeAbout;

Function GetRegistryHelpPath(Const HelpFile:String):String;
Const WindowsHelp='SOFTWARE\Microsoft\Windows\Help';
begin
  result:='';
  {$IFNDEF CLX}
  With TRegistry.Create do
  try
    RootKey:=HKEY_LOCAL_MACHINE;
    if {$IFDEF D4}OpenKeyReadOnly{$ELSE}OpenKey{$ENDIF}(WindowsHelp{$IFNDEF D4},False{$ENDIF}) then
       result:=ReadString(HelpFile)+'\'+HelpFile;
  finally
    Free;
  end;
  {$ENDIF}
end;

Function GetTeeChartHelpFile:String;
begin
  result:=GetRegistryHelpPath({$IFDEF TEEOCX}'TeeChartX5.hlp'{$ELSE}'TeeChart5.hlp'{$ENDIF}); // <- do not translate
end;

Function GetTeeChartUserHelpFile:String;
begin
  result:=GetRegistryHelpPath({$IFDEF TEEOCX}'TeeUserX5.hlp'{$ELSE}'TeeUser5.hlp'{$ENDIF}); // <- do not translate
end;

{ TChartEditForm }
Procedure TChartEditForm.CheckHelpFile;
begin
  if ceHelp in EditorOptions then
  begin
    if csDesigning in Chart.ComponentState then
       HelpFile:=GetTeeChartHelpFile
    else
       HelpFile:=GetTeeChartUserHelpFile;
    if HelpFile<>'' then BorderIcons:=BorderIcons+[biHelp];
  end
  else HelpFile:='';
//  ButtonHelp.Visible:=ceHelp in EditorOptions;
end;

procedure TChartEditForm.FormShow(Sender: TObject);

  Procedure HideTabs;
  Const Margin=26;
        HorizMargin=10;
        HorizButtonMargin=5;
  begin
    if cetMain in TheHiddenTabs then TabSeriesList.TabVisible:=False;
    if cetGeneral in TheHiddenTabs then TabGeneral.TabVisible:=False;
    if cetAxis in TheHiddenTabs then TabAxis.TabVisible:=False;
    if cetTitles in TheHiddenTabs then TabTitle.TabVisible:=False;
    if cetLegend in TheHiddenTabs then TabLegend.TabVisible:=False;
    if cetPanel in TheHiddenTabs then TabPanel.TabVisible:=False;
    if cetPaging in TheHiddenTabs then TabPaging.TabVisible:=False;
    if cetWalls in TheHiddenTabs then TabWalls.TabVisible:=False;
    if cet3D in TheHiddenTabs then Tab3D.TabVisible:=False;
    if cetSeriesData in TheHiddenTabs then TabData.TabVisible:=False;
    if cetExport in TheHiddenTabs then TabExport.TabVisible:=False;
    if cetTools in TheHiddenTabs then TabTools.TabVisible:=False;
    if cetPrintPreview in TheHiddenTabs then TabPrint.TabVisible:=False;
    if cetAllSeries in TheHiddenTabs then
    begin
      SubPage.Parent:=Self;
      MainPage.Visible:=False;
      Height:=Height-Margin;
      Width:=Width-HorizMargin;
    end;
    MainPage.ActivePage:=TabSeries;
    SetTabSeries;
  end;

var t  : Integer;
    St : String;
begin
  TeeLoadArrowBitmaps(BMoveUp.Glyph,BMoveDown.Glyph);
  {$IFDEF TEETRIAL}
  LabelWWW.Cursor:=crHandPoint;
  {$ELSE}
  LabelWWW.Visible:=False;
  {$ENDIF}

  if TeeToolTypes.Count=0 then TabTools.TabVisible:=False;

  LBSeries.Chart:=Chart;

  if (Caption='') and Assigned(Chart) then
  begin
    FmtStr(St,TeeMsg_Editing,[Chart.Name]);
    Caption:=St;
  end;

  HideTabs;

  if TheActivePageIndex<>-1 then
  begin
    t:=TheActivePageIndex;
    While not SubPage.Pages[t].TabVisible do
    if t<SubPage.PageCount then
       Inc(t)
    else
    begin
      t:=TheActivePageIndex;
      While not SubPage.Pages[t].TabVisible do
      if t>0 then
         Dec(t)
      else
         Break;
    end;
    TheActivePageIndex:=t;
    if SubPage.Pages[TheActivePageIndex].TabVisible then
    begin
      SubPage.ActivePage:=SubPage.Pages[TheActivePageIndex];
      SubPageChange(Self);
    end;
  end;

  if Assigned(TeeOnShowEditor) then TeeOnShowEditor(Self);

  if Assigned(TheEditSeries) then
  begin
    LBSeries.SelectedSeries:=TheEditSeries;
    LBSeriesEditSeries(LBSeries,0);
  end
  else
  if Assigned(TheTool) and TabTools.TabVisible then
  begin
    MainPage.ActivePage:=TabTools;
    MainPageChange(Self);
  end;
end;

procedure TChartEditForm.FormCreate(Sender: TObject);
begin
  Chart:=nil;
  Caption:='';
  Left:=Screen.Width-Width-20;
  Top:=20;
  TheActivePageIndex:=-1;
  EditorOptions:=eoAll;
  MainPage.ActivePage:=TabChart;
  SubPage.ActivePage:=TabSeriesList;
end;

procedure TChartEditForm.SubPageChange(Sender: TObject);
var tmpForm   : TForm;
    tmpSeries : TChartSeries;
begin
  With SubPage.ActivePage do
  if ControlCount=0 then
  begin
    Case {$IFNDEF CLX}PageIndex{$ELSE}TabIndex{$ENDIF} of
      teeEditGeneralPage: tmpForm:=TFormTeeGeneral.CreateChart(Self,Chart);
      teeEditAxisPage   : begin
                            if not Assigned(TheAxis) then TheAxis:=Chart.LeftAxis;
                            tmpForm:=TFormTeeAxis.CreateAxis(Self,TheAxis);
                          end;
      teeEditTitlePage  : begin
                            if not Assigned(TheTitle) then TheTitle:=Chart.Title;
                            tmpForm:=TFormTeeTitle.CreateTitle(Self,Chart,TheTitle);
                          end;
      teeEditLegendPage : tmpForm:=TFormTeeLegend.CreateLegend(Self,Chart.Legend);
      teeEditPanelPage  : tmpForm:=TFormTeePanel.CreatePanel(Self,Chart);
      teeEditPagingPage : tmpForm:=TFormTeePage.CreateChart(Self,Chart);
      teeEditWallsPage  : begin
                            if not Assigned(TheWall) then TheWall:=Chart.LeftWall;
                            tmpForm:=TFormTeeWall.CreateWall(Self,TheWall);
                          end;
    else
       tmpForm:=TFormTee3D.CreateChart(Self,Chart);
    end;

    { show the form... }
    With tmpForm do
    begin
      BorderIcons:=[]; 
      BorderStyle:={$IFDEF CLX}fbsNone{$ELSE}bsNone{$ENDIF};
      Align:=alClient;
      Parent:=Self.SubPage.ActivePage;
      Show;
    end;
  end;
  if SubPage.ActivePage=Tab3D then
  begin
    tmpSeries:=Chart.GetASeries;
    With TFormTee3D(Tab3D.Controls[0]) do
    begin
      AllowRotation:=TheChart.Canvas.SupportsFullRotation or
                     (not Assigned(tmpSeries)) or
                     (not (tmpSeries is TPieSeries));
      CheckRotation;
    end;
  end;
end;

procedure TChartEditForm.BCloseClick(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TChartEditForm.BMoveUPClick(Sender: TObject);
begin
  LBSeries.MoveCurrentUp;
end;

procedure TChartEditForm.BMoveDownClick(Sender: TObject);
begin
  LBSeries.MoveCurrentDown;
end;

procedure TChartEditForm.BAddSeriesClick(Sender: TObject);
var tmpSeries : TChartSeries;
begin
  tmpSeries:=LBSeries.AddSeriesGallery;
  if Assigned(tmpSeries) and Assigned(tmpSeries.FunctionType) then
  begin
    TheSeries:=tmpSeries;
    MainPage.ActivePage:=TabSeries;
    SetTabSeries;
    With TheFormSeries do
    begin
      PageSeries.ActivePage:=TabDataSource;
      PageSeriesChange(Self);
      CBDataSourcestyleChange(Self);
    end;
  end;
end;

procedure TChartEditForm.BDeleteSeriesClick(Sender: TObject);
begin
  LBSeries.DeleteSeries;
  if Assigned(TheFormSeries) then TheFormSeries.DestroySeriesForms;
end;

procedure TChartEditForm.BRenameSeriesClick(Sender: TObject);
begin
  LBSeries.RenameSeries;
  Self.ActiveControl:=LBSeries;
end;

procedure TChartEditForm.BCloneSeriesClick(Sender: TObject);
begin
  LBSeries.CloneSeries;
end;

procedure TChartEditForm.SetTabSeries;
begin
  if Assigned(TheFormSeries) then
     TheFormSeries.TheSeries:=LBSeries.SelectedSeries
  else
  begin
    TheFormSeries:=TFormTeeSeries.Create(Self);
    With TheFormSeries do
    begin
      Self.LBSeries.OtherItems:=CBSeries.Items;
      BorderIcons:=[];
      BorderStyle:={$IFDEF CLX}fbsNone{$ELSE}bsNone{$ENDIF};
      Parent:=TabSeries;
      Align:=alClient;
      ShowTabDataSource:=ceDataSource in EditorOptions;
      ShowTabGeneral:=not (cetSeriesGeneral in TheHiddenTabs);
      ShowTabMarks:=not (cetSeriesMarks in TheHiddenTabs);
      IsDssGraph:=Self.IsDssGraph;
      TheSeries:=Self.LBSeries.SelectedSeries;
      Self.LBSeries.FillSeries(TheSeries);
      Show;
    end;
  end;
  TheFormSeries.SetCBSeries;
end;

procedure TChartEditForm.MainPageChange(Sender: TObject);

  Procedure SetTabPrint;
  var tmp : TChartPreview;
  begin
    if TabPrint.ControlCount=0 then
    begin
      tmp:=TChartPreview.Create(Self);
      with tmp do
      begin
        GBMargins.Visible:=False;
        PanelMargins.Visible:=False;
        BClose.Visible:=False;
        Align:=alClient;
      end;
      AddFormTo(tmp,TabPrint,Integer(Chart));
    end;
  end;

  Procedure SetTabTools;
  var tmp     : TFormTeeTools;
      tmpTool : Integer;
  begin
    if TabTools.ControlCount=0 then
    begin
      tmp:=TFormTeeTools.Create(Self);
      tmp.Align:=alClient;
      AddFormTo(tmp,TabTools,Integer(Chart));
      if Assigned(TheTool) then
      begin
        tmpTool:=tmp.LBTools.Items.IndexOfObject(TheTool);
        if tmpTool<>-1 then
        begin
          tmp.LBTools.ItemIndex:=tmpTool;
          tmp.LBToolsClick(Self);
        end;
        TheTool:=nil;
      end;
    end;
  end;

  Procedure SetTabExport;
  begin
    if TabExport.ControlCount=0 then
       AddFormTo(TTeeExportForm.Create(Self),TabExport,Integer(Chart));
    With TTeeExportForm(TabExport.Controls[0]) do
    begin
      Align:=alClient;
      BClose.Visible:=False;
      EnableButtons;
    end;
  end;

  Procedure SetTabData;
  begin
    if TabData.ControlCount=0 then
    begin
      FGrid:=TChartGrid.Create(Self);
      FGrid.Align:=alClient;

      With TChartGridNavigator.Create(Self) do
      begin
        Align:=alBottom;
        Grid:=FGrid;
        ShowHint:=True;
        TabStop:=True;
        Parent:=TabData;
      end;
      FGrid.Parent:=TabData;
    end;
    FGrid.Chart:=Chart;
    FGrid.RecalcDimensions;
    FGrid.SetFocus;
  end;

begin
  if MainPage.ActivePage=TabSeries then SetTabSeries
  else
  if MainPage.ActivePage=TabData then SetTabData
  else
  if MainPage.ActivePage=TabTools then SetTabTools
  else
  if MainPage.ActivePage=TabExport then SetTabExport
  else
  if MainPage.ActivePage=TabPrint then SetTabPrint
end;

{$IFNDEF CLX}
procedure TChartEditForm.WMHelp(var Message: TWMHelp);
var Control : TWinControl;
begin
  if biHelp in BorderIcons then
  with Message.HelpInfo^ do
  begin
    if iContextType = HELPINFO_WINDOW then
    begin
      Control := FindControl(hItemHandle);
      while Assigned(Control) and (Control.HelpContext = 0) do
        Control := Control.Parent;
      if Assigned(Control) then
         Application.HelpContext(Control.HelpContext);
    end;
  end
  else inherited;
end;
{$ENDIF}

procedure TChartEditForm.SetChart(Value:TCustomChart);
begin
  TheChart:=Value;
  LBSeries.Chart:=TheChart;
  MainPage.Enabled:=Assigned(TheChart);
  SubPage.Enabled:=Assigned(TheChart);
end;

procedure TChartEditForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if Assigned(TheFormSeries) then
  begin
    TheFormSeries.FormCloseQuery(Sender,CanClose);
    if CanClose then Close;
  end;
end;

procedure TChartEditForm.LBSeriesEditSeries(Sender: TChartListBox;
  Index: Integer);
begin
  if LBSeries.ItemIndex<>-1 then
  begin
    MainPage.ActivePage:=TabSeries;
    SetTabSeries;
  end;
end;

procedure TChartEditForm.LBSeriesOtherItemsChange(Sender: TObject);
begin
  if Assigned(TheFormSeries) then
     TheFormSeries.CBSeries.ItemIndex:=LBSeries.ItemIndex;
end;

procedure TChartEditForm.LBSeriesRefreshButtons(Sender: TObject);
var tmp          : Boolean;
    tmpInherited : Boolean;
    tmpSeries    : TChartSeries;
begin
  tmp:=(Chart.SeriesCount>0) and (LBSeries.ItemIndex<>-1);
  if tmp then tmpSeries:=Chart.Series[LBSeries.ItemIndex]
         else tmpSeries:=nil;
  tmpInherited:=tmp and (csAncestor in tmpSeries.ComponentState);
  BAddSeries.Enabled:=(ceAdd in EditorOptions);
  BDeleteSeries.Enabled:= tmp and
                          (not tmpInherited) and
                          (ceDelete in EditorOptions)
                          and
                          (not (tssIsTemplate in tmpSeries.Style)) and
                          (not (tssDenyDelete in tmpSeries.Style)) ;
  BRenameSeries.Enabled:=tmp and (LBSeries.SelCount<2) and (ceTitle in EditorOptions);
{  BChangeTypeSeries.Enabled:= tmp and
                              (not tmpInherited) and
                              (ceChange in EditorOptions)
                              and
                              (not (tssDenyChangeType in tmpSeries.Style));}
  BCloneSeries.Enabled:= tmp and
                         (LBSeries.SelCount<2) and
                         (ceClone in EditorOptions)
                         and
                         (not (tssIsTemplate in tmpSeries.Style)) and
                         (not (tssDenyClone in tmpSeries.Style));

  if tmp and (LBSeries.SelCount<=1) then
  begin
    BMoveDown.Enabled:=LBSeries.ItemIndex<LBSeries.Items.Count-1;
    BMoveUp.Enabled:=LBSeries.ItemIndex>0;
  end
  else
  begin
    BMoveDown.Enabled:=False;
    BMoveUp.Enabled:=False;
  end;
end;

procedure TChartEditForm.BChangeTypeSeriesClick(Sender: TObject);
begin
  LBSeries.ChangeTypeSeries(Self);
  if Assigned(TheFormSeries) then TheFormSeries.DestroySeriesForms;
end;

procedure TChartEditForm.LabelWWWClick(Sender: TObject);
begin
  GotoURL(Handle,LabelWWW.Caption);
end;

procedure TChartEditForm.ButtonHelpClick(Sender: TObject);
begin
  {$IFNDEF CLX}
  Application.HelpCommand(HELP_CONTEXT, HelpContext);
  {$ENDIF}
end;

procedure TChartEditForm.MainPageChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  if Assigned(TheFormSeries) then
     TheFormSeries.PageSeriesChanging(Self,AllowChange)
  else
     AllowChange:=True;
end;

initialization
  RegisterClass(TChartEditForm);
end.


  