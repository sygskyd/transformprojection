unit TeePyramidEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, TeCanvas, TeePenDlg, TeePyramid;

type
  TPyramidSeriesEditor = class(TForm)
    BPen: TButtonPen;
    CBColorEach: TCheckBox;
    BColor: TButtonColor;
    Button1: TButton;
    Label1: TLabel;
    Edit1: TEdit;
    UDSize: TUpDown;
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure CBColorEachClick(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
  private
    { Private declarations }
    Pyramid : TPyramidSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

Uses TeeBrushDlg;

procedure TPyramidSeriesEditor.FormShow(Sender: TObject);
begin
  Pyramid:=TPyramidSeries(Tag);
  With Pyramid do
  begin
    BPen.LinkPen(Pen);
    UDSize.Position:=SizePercent;
    CBColorEach.Checked:=ColorEachPoint;
    BColor.Enabled:=not ColorEachPoint;
  end;
  BColor.LinkProperty(Pyramid,'SeriesColor');
end;

procedure TPyramidSeriesEditor.Button1Click(Sender: TObject);
begin
  EditChartBrush(Self,Pyramid.Brush);
end;

procedure TPyramidSeriesEditor.CBColorEachClick(Sender: TObject);
begin
  Pyramid.ColorEachPoint:=CBColorEach.Checked;
  BColor.Enabled:=not Pyramid.ColorEachPoint;
end;

procedure TPyramidSeriesEditor.Edit1Change(Sender: TObject);
begin
  if Showing then Pyramid.SizePercent:=UDSize.Position;
end;

initialization
  RegisterClass(TPyramidSeriesEditor);
end.
