Unit MSConvert;

Interface  

Uses Classes,DsgnIntf,SysUtils,Windows;

Const fRegAppPreview         = $0004;
      fRegAppSupportNonOem   = $0008;

      fceNoErr          = 0;
      fceOpenInFileErr	= -1;
      fceReadErr	= -2;
      fceOpenConvErr	= -3;
      fceWriteErr	= -4;
      fceInvalidFile	= -5;
      fceOpenExceptErr	= -6;
      fceWriteExceptErr	= -7;
      fceNoMemory	= -8; 
      fceInvalidDoc	= -9; 
      fceDiskFull	= -10;
      fceDocTooLarge	= -11;
      fceOpenOutFileErr	= -12;
      fceUserCancel	= -13;
      fceWrongFileType	= -14;

Type ETextConverter     = Class(Exception)
     end;

     TOutputCallback    = Function(Count:Integer;PercentComplete:Integer):Integer; stdcall;
     TInputCallback     = Function(Flags:Integer;PercentComplete:Integer):Integer; stdcall;
     TForeignToRTF      = Function(FileName:THandle;Param:Pointer;Buffer,Description,
                              SubSet:THandle;OutputCallback:TOutputCallback):Integer; stdcall;
     TRTFToForeign      = Function(FileName:THandle;Param:Pointer;Buffer,
                              ClassName:THandle;InputCallback:TInputCallback):Integer; stdcall;
     TInitConverter     = Function(Window:THandle;ApplicationName:PChar):Integer; stdcall;
     TUninitConverter   = Procedure; stdcall;
     TIsFormatCorrect   = Function(FileName:THandle;ClassName:THandle):Integer; stdcall;
     TRegisterApp       = Function(Flags:LongInt;Params:Pointer):HGLOBAL;
     TCompletionNotify  = Procedure(Sender:TObject;Percentage:Integer;var Continue:Boolean) of object;
     TFetchErrorText    = Function(ErrorCode:Integer;Buffer:PChar;BufferSize:Integer):Integer; stdcall;

     TTextConverter         = Class(TComponent)
      Private
       FBufferPtr       : PChar;
       FDLLHandle       : THandle;
       FExportConverter : String;
       FFetchErrorText  : TFetchErrorText;
       FFileName        : String;
       FForeignToRTF    : TForeignToRTF;
       FImportConverter : String;
       FInitConverter   : TInitConverter;
       FIsFormatCorrect : TIsFormatCorrect;
       FNotify          : TCompletionNotify;
       FRegisterApp     : TRegisterApp;
       FRTFToForeign    : TRTFToForeign;
       FStream          : TStream;
       FTextOnly        : Boolean;
       FUninitConverter : TUninitConverter;
       Function    DoRead(Count:Integer;PercentComplete:Integer):Integer;
       Function    GetExportConverter:String;
       Class Function GetExportConverters:TStringList;
       Function    GetImportConverter:String;
       Class Function GetImportConverters:TStringList;
       Procedure   LoadConverter(const DLLName:String;Import:Boolean);
       Procedure   SetExportConverter(const Name:String);
       Procedure   SetImportConverter(const Name:String);
       Procedure   ThrowConverterError(ErrorCode:Integer);
       Procedure   UnloadCOnverter;
      Public
       Property    ExportConverters:TStringList read GetExportConverters;
       Property    ImportConverters:TStringList read GetImportConverters;
       Function    IsFormatCorrect(const FileName:String):Boolean;
       Procedure   Read(OutputTo:TStream);
       Procedure   Write(OutputFrom:TStream);
      Published
       Property    CompletionNotify:TCompletionNotify read FNotify write FNotify;
       Property    ImportConverter:String read GetImportConverter write SetImportConverter;
       Property    ExportConverter:String read GetExportConverter write SetExportConverter;
       Property    FileName:String read FFileName write FFileName;
       Property    TextOnly:Boolean read FTextOnly write FTextOnly default FALSE;
     end;

     TExportConverterProperty = Class(TStringProperty)
       function GetAttributes: TPropertyAttributes; override;
       procedure GetValues(Proc: TGetStrProc); override;
     end;

     TImportConverterProperty = Class(TStringProperty)
       function GetAttributes: TPropertyAttributes; override;
       procedure GetValues(Proc: TGetStrProc); override;
     end;

Procedure Register;

Implementation

Uses Controls,Forms,ActiveX,MultiLng,Registry;

Const BufferSize        = 8192;

var ActiveConverter     : TTextConverter;
    ImportConverters    : TStringList;
    ExportConverters    : TStringList;

Type PConverterInfo     = ^TConverterInfo;
     TConverterInfo     = Record
       ClassName        : String;
       DLLName          : String;
       Name             : String;
     end;

Function StringToHGLOBAL(const Str:String):HGLOBAL;
var Dest           : PChar;
begin
  // allocate global memory-block
  Result:=GlobalAlloc(GHND,Length(Str)*2+1);
  // copy characters
  Dest:=GlobalLock(Result);
  StrCopy(Dest,PChar(Str));
  GlobalUnlock(Result);
end;

Procedure ThrowError(const Text:String);
var MsgText        : PChar;
    Exception      : ETextConverter;
begin
  FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER or FORMAT_MESSAGE_FROM_SYSTEM, NIL,
     GetLastError(),SUBLANG_DEFAULT Shl 10 or LANG_NEUTRAL,@MsgText,0,NIL);
  Exception:=ETextConverter.Create(Text+' '+MsgText);
  LocalFree(Integer(MsgText));
  Raise Exception;
end;

Procedure ClearConverterList(AList:TStringList);
var Cnt            : Integer;
begin
  for Cnt:=0 to AList.Count-1 do Dispose(PConverterInfo(AList.Objects[Cnt]));
  AList.Clear;
end;

Function InfoFromClass(AList:TStringList;const ClassName:String):PConverterInfo;
var Cnt            : Integer;
begin
  for Cnt:=0 to AList.Count-1 do if AnsiCompareText(ClassName,
      PConverterInfo(AList.Objects[Cnt])^.ClassName)=0 then begin
    Result:=PConverterInfo(AList.Objects[Cnt]);
    Exit;
  end;
  Result:=NIL;
end;

Procedure CreateConverterList;
var Registry       : TRegistry;
  Procedure AddConverters(const RegName:String);
  var Keys         : TStringList;
      Info         : PConverterInfo;
      Cnt          : Integer;
  begin
    Keys:=TStringList.Create;
    try
      // read import-filters
      if Registry.OpenKeyReadOnly(RegName+'\Import') then begin
        Registry.GetKeyNames(Keys);
        // create a new converter-info if there does not already exist one
        for Cnt:=0 to Keys.Count-1 do if InfoFromClass(ImportConverters,
            Keys[Cnt])=NIL then if Registry.OpenKeyReadOnly(RegName+'\Import\'
            +Keys[Cnt]) then begin
          Info:=New(PConverterInfo);
          Info^.ClassName:=Keys[Cnt];
          Info^.DLLName:=Registry.ReadString('Path');
          Info^.Name:=Registry.ReadString('Name');
          ImportConverters.AddObject(Info^.Name,Pointer(Info));
        end;
      end;
      // read export-filters
      Keys.Clear;
      if Registry.OpenKeyReadOnly(RegName+'\Export') then begin
        Registry.GetKeyNames(Keys);
        // create a new converter-info if there does not already exist one
        for Cnt:=0 to Keys.Count-1 do if InfoFromClass(ExportConverters,
            Keys[Cnt])=NIL then if Registry.OpenKeyReadOnly(RegName+'\Export\'
            +Keys[Cnt]) then begin
          Info:=New(PConverterInfo);
          Info^.ClassName:=Keys[Cnt];
          Info^.DLLName:=Registry.ReadString('Path');
          Info^.Name:=Registry.ReadString('Name');
          ExportConverters.AddObject(Info^.Name,Pointer(Info));
        end;
      end;
    finally
      Keys.Free;
    end;
  end;
begin
  if ImportConverters=NIL then begin
    ImportConverters:=TStringList.Create;
    ExportConverters:=TStringList.Create;
    Registry:=TRegistry.Create;
    try
      Registry.RootKey:=HKEY_LOCAL_MACHINE;
      AddConverters('\Software\Microsoft\Shared Tools\Text Converters');
      Registry.RootKey:=HKEY_CURRENT_USER;
      AddConverters('\Software\Microsoft\Office\8.0\Word\Text Converters');
      AddConverters('\Software\Microsoft\Word\7.0\Text Converters');
    finally
      Registry.Free;
    end;
  end;
end;

{==============================================================================+
  TTextConverter
+==============================================================================}

{******************************************************************************+
  Procedure TTextConverter.LoadConverter
--------------------------------------------------------------------------------
  Loads the text-converter-dll and initializes the function-pointers. Checks
  if all necessary functions are exported by the dll.
+******************************************************************************}
Procedure TTextConverter.LoadConverter(const DLLName:String;Import:Boolean);
var OwnerHandle    : THandle;
begin
  FDLLHandle:=LoadLibrary(PChar(DLLName));
  try
    if FDLLHandle=0 then ThrowError(MlgStringList['TextConverter',1]);
    FInitConverter:=GetProcAddress(FDLLHandle,'InitConverter32');
    FUninitConverter:=GetProcAddress(FDLLHandle,'UninitConverter');
    FForeignToRTF:=GetProcAddress(FDLLHandle,'ForeignToRtf32');
    FRTFToForeign:=GetProcAddress(FDLLHandle,'RtfToForeign32');
    FIsFormatCorrect:=GetProcAddress(FDLLHandle,'IsFormatCorrect32');
    FRegisterApp:=GetProcAddress(FDLLHandle,'RegisterApp');
    FFetchErrorText:=GetProcAddress(FDLLHandle,'CchFetchLpszError');
    if @FInitConverter=NIL then ThrowError(MlgStringList['TextConverter',2]);
    if Import and (@FForeignToRTF=NIL) then ThrowError(MlgStringList['TextConverter',2]);
    if not Import and (@FRTFToForeign=NIL) then ThrowError(MlgStringList['TextConverter',2]);
    if Owner is TWinControl then OwnerHandle:=TWinControl(Owner).Handle
    else OwnerHandle:=Application.Handle;
    if FInitConverter(OwnerHandle,PChar(ExtractFileName(Application.ExeName)))<>1 then
        ThrowError(MlgStringList['TextConverter',3]);
//    if @FRegisterApp<>NIL then FRegisterApp(fRegAppSupportNonOem,NIL);
  except
    UnloadConverter;
  end;
end;

{******************************************************************************+
  Procedure TTextConverter.UnloadConverter
--------------------------------------------------------------------------------
  Unloads the converter-dll and resets all internal variables. 
+******************************************************************************}
Procedure TTextConverter.UnloadConverter;
begin
  if @FUninitConverter<>NIL then FUninitConverter;
  if FDLLHandle<>0 then begin
    FreeLibrary(FDLLHandle);
    FDLLHandle:=0;
  end;  
  FUninitConverter:=NIL;
  OleUninitialize;
end;

Function ReadCallback(Count:Integer;PercentComplete:Integer):Integer; stdcall;
begin
  if ActiveConverter<>NIL then Result:=ActiveConverter.DoRead(Count,PercentComplete);
  Result:=0;
end;

Function TTextConverter.DoRead(Count:Integer;PercentComplete:Integer):Integer;
var Continue       : Boolean;
begin
  FStream.Write(FBufferPtr^,Count);
  if Assigned(FNotify) then begin
    Continue:=TRUE;
    FNotify(Self,PercentComplete,Continue);
    if Continue then Result:=0
    else Result:=-1;
  end
  else Result:=0;
end;

Procedure TTextConverter.ThrowConverterError(ErrorCode:Integer);
var Text           : Array[0..255] of Char;
begin
  // did an error occur?
  if ErrorCode>=0 then Exit;
  Text:='';
  // does the converter supply error-messages?
  if @FFetchErrorText<>NIL then FFetchErrorText(ErrorCode,Text,SizeOf(Text));
  // use standard error-messages if the converter procides none
  if (Text='') and (ErrorCode>=-14) then StrPCopy(Text,MlgStringList['TextConverter',20+Abs(ErrorCode)]);
  Raise ETextConverter.CreateFmt(MlgStringList['TextConverter',6]+' '+Text,[FFileName]);
end;

Procedure TTextConverter.Read(OutputTo:TStream);
var BufferHandle   : HGLOBAL;
    NameHandle     : HGLOBAL;
    DescHandle     : HGLOBAL;
    SubHandle      : HGLOBAL;
    Info           : PConverterInfo;
    ErrorCode      : Integer;
begin
  if FImportConverter='' then ThrowError(MlgStringList['TextConverter',4]);
  CreateConverterList;
  Info:=InfoFromClass(ImportConverters,FImportConverter);
  if Info=NIL then ThrowError(MlgStringList['TextConverter',5]);
  LoadConverter(Info^.DLLName,TRUE);
  try
    BufferHandle:=GlobalAlloc(GHND,BufferSize);
    if BufferHandle<>0 then begin
      FBufferPtr:=GlobalLock(BufferHandle);
      FStream:=OutputTo;
      CharToOEM(PChar(FileName),PChar(FileName));
      NameHandle:=StringToHGLOBAL(FileName);
      DescHandle:=StringToHGLOBAL('');
      SubHandle:=StringToHGLOBAL('');
      if NameHandle*DescHandle*SubHandle<>0 then begin
        ActiveConverter:=Self;
        ThrowConverterError(FForeignToRTF(NameHandle,NIL,BufferHandle,DescHandle,
            SubHandle,@ReadCallback));
      end;
      if NameHandle<>0 then GlobalFree(NameHandle);
      if DescHandle<>0 then GlobalFree(DescHandle);
      if SubHandle<>0 then GlobalFree(SubHandle);
      GlobalUnlock(BufferHandle);
      GlobalFree(BufferHandle);
      ActiveConverter:=NIL;
    end;
  finally
    UnloadConverter;
  end;    
end;

Procedure TTextConverter.Write(OutputFrom:TStream);
begin
end;

Function TTextConverter.IsFormatCorrect(const FileName:String):Boolean;
var NameHandle     : HGLOBAL;
    DescHandle     : HGLOBAL;
    a:INteger;
begin
  Result:=FALSE;
  if @FIsFormatCorrect<>NIL then begin
    NameHandle:=StringToHGLOBAL(FileName);
	   DescHandle:=GlobalAlloc(GHND,256);
    if NameHandle*DescHandle<>0 then a:=FIsFormatCorrect(NameHandle,DescHandle);
    if NameHandle<>0 then GlobalFree(NameHandle);
    if DescHandle<>0 then GlobalFree(DescHandle);
  end;  
end;

Function TTextConverter.GetExportConverter:String;
var Info           : PConverterInfo;
begin
  if csWriting in ComponentState then Result:=FExportConverter
  else begin
    Info:=InfoFromClass(ExportConverters,FExportConverter);
    if Info<>NIL then Result:=Info^.Name
    else Result:='';
  end;
end;

Function TTextConverter.GetImportConverter:String;
var Info           : PConverterInfo;
begin
  if csWriting in ComponentState then Result:=FImportConverter
  else begin
    Info:=InfoFromClass(ImportConverters,FImportConverter);
    if Info<>NIL then Result:=Info^.Name 
    else Result:='';
  end;
end;

Procedure TTextConverter.SetExportConverter(const Name:String);
var Index          : Integer;
begin
  if csReading in ComponentState then FExportConverter:=Name
  else begin
    Index:=ExportConverters.IndexOf(Name);
    if Index>=0 then FExportConverter:=PConverterInfo(ExportConverters.
        Objects[Index])^.ClassName
    else FExportConverter:='';
  end;
end;

Procedure TTextConverter.SetImportConverter(const Name:String);
var Index          : Integer;
begin
  if csReading in ComponentState then FImportConverter:=Name
  else begin
    Index:=ImportConverters.IndexOf(Name);
    if Index>=0 then FImportConverter:=PConverterInfo(ImportConverters.
        Objects[Index])^.ClassName
    else FImportConverter:='';
  end;
end;

Class Function TTextConverter.GetImportConverters:TStringList;
begin
  CreateConverterList;
  Result:=MSConvert.ImportConverters;
end;

Class Function TTextConverter.GetExportConverters:TStringList;
begin
  CreateConverterList;
  Result:=MSConvert.ExportConverters;
end;

{==============================================================================+

+==============================================================================}

{ TImportConverterProperty }

function TImportConverterProperty.GetAttributes: TPropertyAttributes;
begin
  Result:=[paValueList,paSortList,paMultiSelect];
end;

procedure TImportConverterProperty.GetValues(Proc: TGetStrProc);
var Cnt:Integer;
begin
  CreateConverterList;
  if ImportConverters<>NIL then for Cnt:=0 to ImportConverters.Count-1 do
      Proc(ImportConverters[Cnt]);
end;

function TExportConverterProperty.GetAttributes: TPropertyAttributes;
begin
  Result:=[paValueList,paSortList,paMultiSelect];
end;

procedure TExportConverterProperty.GetValues(Proc: TGetStrProc);
var Cnt:Integer;
begin
  CreateConverterList;
  if ExportConverters<>NIL then for Cnt:=0 to ExportConverters.Count-1 do
      Proc(ExportConverters[Cnt]);
end;

Procedure Register;
begin
  RegisterComponents('Text', [TTextConverter]);
  RegisterPropertyEditor(TypeInfo(String),TTextConverter,'ExportConverter',TExportConverterProperty);
  RegisterPropertyEditor(TypeInfo(String),TTextConverter,'ImportConverter',TImportConverterProperty);
end;

Initialization
  ActiveConverter:=NIL;
  ImportConverters:=NIL;
  ExportConverters:=NIL;

Finalization
  if ImportConverters<>NIL then begin
    ClearConverterList(ImportConverters);
    ImportConverters.Free;
  end;
  if ExportConverters<>NIL then begin
    ClearConverterList(ExportConverters);
    ExportConverters.Free;
  end;

end.
