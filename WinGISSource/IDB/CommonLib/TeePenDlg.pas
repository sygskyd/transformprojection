{**********************************************}
{   TPenDialog                                 }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeePenDlg;

interface

uses
  {$IFDEF LINUX}
  LibC,
  {$ELSE}
  Windows, Messages,
  {$ENDIF}
  SysUtils, Classes,
  {$IFDEF CLX}
  QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls, QComCtrls,
  {$ELSE}
  Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, ComCtrls,
  {$ENDIF}
  TeCanvas, MultiLng;

type
  {$IFDEF CLX}
  TUpDown=class(TButton)
  private
    FWrap: Boolean;
    FPosition: Integer;
    FMax: Integer;
    FMin: Integer;
    FAssociate: TControl;
    FInc: Integer;
  published
    property Increment:Integer read FInc write FInc;
    property Position:Integer read FPosition write FPosition;
    property Min:Integer read FMin write FMin;
    property Max:Integer read FMax write FMax;
    property Associate:TControl read FAssociate write FAssociate;
    property Wrap:Boolean read FWrap write FWrap;
  end;
  {$ENDIF}

  TPenDialog = class(TForm)
    CBVisible: TCheckBox;
    SEWidth: TEdit;
    LWidth: TLabel;
    BOk: TButton;
    BColor: TButtonColor;
    BCancel: TButton;
    UDWidth: TUpDown;
    Label1: TLabel;
    CBStyle: TComboBox;
    MlgSection1: TMlgSection;
    procedure FormShow(Sender: TObject);
    procedure SEWidthChange(Sender: TObject);
    procedure CBVisibleClick(Sender: TObject);
    procedure BCancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CBStyleChange(Sender: TObject);
    {$IFNDEF CLX}
    procedure CBStyleDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    {$ENDIF}
    procedure BColorClick(Sender: TObject);
  private
    { Private declarations }
    BackupPen  : TChartPen;
    {$IFDEF TEEPENEND}
    CBEndStyle : TComboBox;
    procedure CBEndStyleChange(Sender: TObject);
    {$ENDIF}
    Procedure EnablePenStyle;
  public
    { Public declarations }
    ThePen : TPen;
  end;

Procedure EditChartPen(AOwner:TComponent; ChartPen:TChartPen);

{ Show / Hide controls in array }
Procedure ShowControls(Show:Boolean; Const AControls:Array of TControl);

{ Asks the user a question and returns Yes or No }
Function TeeYesNo(Const Message:String):Boolean;

type TButtonPen=class(TTeeButton)
     protected
       procedure DrawSymbol(Canvas:TCanvas); override;
     public
       procedure Click; override;
       procedure LinkPen(APen:TChartPen);
     end;

Procedure AddFormTo(AForm:TForm; AParent:TWinControl; ATag:Integer);
Procedure AddDefaultValueFormats(AItems:TStrings);

Procedure TeeLoadArrowBitmaps(AUp,ADown:TBitmap);

{ Helper listbox items routines }
procedure MoveList(Source,Dest:TCustomListBox);
procedure MoveListAll(Source,Dest:TStrings);

implementation

{$R *.dfm}

Uses TeeConst, TeeProcs, {$IFNDEF CLX}ExtDlgs,{$ENDIF} TypInfo;

Procedure EditChartPen(AOwner:TComponent; ChartPen:TChartPen);
Begin
  With TPenDialog.Create(AOwner) do
  try
    ThePen:=ChartPen;
    ShowModal;
  finally
    Free;
  end;
end;

procedure TPenDialog.FormShow(Sender: TObject);
begin
  BackupPen:=TChartPen.Create(nil);
  BackupPen.Assign(ThePen);
  if ThePen is TChartPen then
  begin
    CBVisible.Checked:=TChartPen(ThePen).Visible;
    BackupPen.Visible:=CBVisible.Checked;
    if IsWindowsNT then CBStyle.Items.Add(TeeMsg_SmallDotsPen);
    if TChartPen(ThePen).SmallDots then
       CBStyle.ItemIndex:=CBStyle.Items.Count-1
    else
       CBStyle.ItemIndex:=Ord(ThePen.Style);
    {$IFDEF TEEPENEND}
    if IsWindowsNT then
    begin
      CBEndStyle:=TComboBox.Create(Self);
      With CBEndStyle do
      begin
        Left:=CBVisible.Left;
        Top:=BOk.Top;
        Parent:=Self;
        Style:=csDropDownList;
        Width:=65;
        Items.Add('Round');
        Items.Add('Square');
        Items.Add('Flat');
        ItemIndex:=Ord(TChartPen(ThePen).EndStyle);
        OnChange:=CBEndStyleChange;
      end;
    end;
    {$ENDIF}
  end
  else
  begin
    CBVisible.Visible:=False;
    CBStyle.ItemIndex:=Ord(ThePen.Style);
  end;
  UDWidth.Position:=ThePen.Width;
  EnablePenStyle;
  BColor.LinkProperty(ThePen,'Color');
end;

Procedure TPenDialog.EnablePenStyle;
begin
  if not IsWindowsNT then CBStyle.Enabled:=ThePen.Width=1;
end;

procedure TPenDialog.SEWidthChange(Sender: TObject);
begin
  if Showing then
  begin
    ThePen.Width:=UDWidth.Position;
    EnablePenStyle;
  end;
end;

{$IFDEF TEEPENEND}
procedure TPenDialog.CBEndStyleChange(Sender: TObject);
begin
  TChartPen(ThePen).EndStyle:=TPenEndStyle(CBEndStyle.ItemIndex);
end;
{$ENDIF}

procedure TPenDialog.CBVisibleClick(Sender: TObject);
begin
  if Showing then TChartPen(ThePen).Visible:=CBVisible.Checked;
end;

procedure TPenDialog.BCancelClick(Sender: TObject);
begin
  ThePen.Assign(BackupPen);
  if ThePen is TChartPen then
  begin
    TChartPen(ThePen).Visible:=BackupPen.Visible;
    if Assigned(ThePen.OnChange) then ThePen.OnChange(Self);
  end;
  ModalResult:=mrCancel;
end;

procedure TPenDialog.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  BackupPen.Free;
  {$IFDEF TEEPENEND}
  CBEndStyle.Free;
  {$ENDIF}
end;

procedure TPenDialog.CBStyleChange(Sender: TObject);
begin
  if (ThePen is TChartPen) and IsWindowsNT and
     (CBStyle.ItemIndex=CBStyle.Items.Count-1) then
         TChartPen(ThePen).SmallDots:=True
  else
  begin
    ThePen.Style:=TPenStyle(CBStyle.ItemIndex);
    if ThePen is TChartPen then
       TChartPen(ThePen).SmallDots:=False;
  end;
end;

procedure TPenDialog.BColorClick(Sender: TObject);
begin
  CBStyle.Repaint;
end;

{$IFNDEF CLX}
procedure TPenDialog.CBStyleDrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
var tmp : TColor;
begin
  With TControlCanvas(CBStyle.Canvas) do
  begin
    FillRect(Rect);
    Pen.Style:=TPenStyle(Index);
    Pen.Color:=ThePen.Color;
    if odSelected in State then tmp:=clHighLight
                           else tmp:=CBStyle.Color;
    if Pen.Color=ColorToRGB(tmp) then
       if Pen.Color=clWhite then Pen.Color:=clBlack
                            else Pen.Color:=clWhite;
    MoveTo(Rect.Left+2,Rect.Top+8);
    LineTo(Rect.Left+30,Rect.Top+8);
    Brush.Style:=bsClear;
    {$IFNDEF CLX}
    {$IFDEF D4}
    UpdateTextFlags;
    {$ENDIF}
    {$ENDIF}
    TextOut(Rect.Left+34,Rect.Top+2,CBStyle.Items[Index]);
  end;
end;
{$ENDIF}

{ TButtonPen }
procedure TButtonPen.Click;
begin
  if Assigned(Instance) then
  begin
    EditChartPen(nil,TChartPen(Instance));
    Repaint;
  end;
  inherited;
end;

procedure TButtonPen.DrawSymbol(Canvas: TCanvas);
begin
  if Enabled then
  With Canvas do
  begin
    Brush.Style:=bsClear;
    Pen.Assign(TChartPen(Instance));
    MoveTo(Width-17,Height div 2);
    LineTo(Width-6,Height div 2);
  end;
end;

procedure TButtonPen.LinkPen(APen: TChartPen);
begin
  LinkProperty(APen,'');
end;

{ Utility functions }
Procedure ShowControls(Show:Boolean; Const AControls:Array of TControl);
var t : Integer;
begin
  for t:=Low(AControls) to High(AControls) do AControls[t].Visible:=Show;
end;

Function TeeYesNo(Const Message:String):Boolean;
Begin
  Screen.Cursor:=crDefault;
  result:=MessageDlg(Message,{$IFNDEF CLX}mtConfirmation{$ELSE}mtWarning{$ENDIF},[mbYes,mbNo],0)=mrYes;
End;

Procedure AddFormTo(AForm:TForm; AParent:TWinControl; ATag:Integer);
var OldVisibleFlag : Boolean;
begin
  With AForm do
  begin
    {$IFNDEF CLX}
    Position:=poDesigned;
    {$ENDIF}
    BorderStyle:={$IFDEF CLX}fbsNone{$ELSE}bsNone{$ENDIF};
    BorderIcons:=[];
    Tag:=ATag;
    Parent:=AParent;
    OldVisibleFlag:=Parent.Visible;
    Parent.Visible:=True;
    Left:=4; //((AParent.ClientWidth-ClientWidth) div 2);
    Top:=MinLong(4,Abs(AParent.ClientHeight-ClientHeight) div 2);
    Show;
    Parent.Visible:=OldVisibleFlag;
  end;
end;

{ utils }
Procedure AddDefaultValueFormats(AItems:TStrings);
begin
  AItems.Add(TeeMsg_DefValueFormat);
  AItems.Add('0');
  AItems.Add('0.0');
  AItems.Add('0.#');
  AItems.Add('#.#');
  AItems.Add('#,##0.00;(#,##0.00)');
  AItems.Add('00e-0');
  AItems.Add('#.0 "x10" E+0');
  AItems.Add('#.# x10E-#');
end;

{ Find and load a bitmap image to a SpeedButton }
type PTeeBitmap=^TTeeBitmap;
     TTeeBitmap=Packed Record
       AName   : String;
       ABitmap : TBitmap;
     end;

function TeeLoadBitmap(AInstance: Integer; Data: Pointer): Boolean;
begin
  result:=True;
  try
    With PTeeBitmap(Data)^ do
    if TryFindResource(AInstance,AName,ABitmap) then result:=False;
  except
    on Exception do ;
  end;
end;

Procedure TeeLoadArrowBitmaps(AUp,ADown: TBitmap);
Var tmpBitmap : TTeeBitmap;
begin
  tmpBitmap.ABitmap:=AUp;
  tmpBitmap.AName:='TEEARROWUP';
  EnumModules(TeeLoadBitmap,@tmpBitmap);
  tmpBitmap.ABitmap:=ADown;
  tmpBitmap.AName:='TEEARROWDOWN';
  EnumModules(TeeLoadBitmap,@tmpBitmap);
end;

{ Helper Listbox methods... }
procedure MoveList(Source,Dest:TCustomListBox);
var t:Integer;
begin
  with Source do
  begin
    for t:=0 to Items.Count-1 do
        if Selected[t] then Dest.Items.AddObject(Items[t],Items.Objects[t]);
    t:=0;
    While t<Items.Count do
    begin
      if Selected[t] then Items.Delete(t)
                     else Inc(t);
    end;
  end;
end;

procedure MoveListAll(Source,Dest:TStrings);
var t : Integer;
begin
  With Source do
  for t:=0 to Count-1 do Dest.AddObject(Strings[t],Objects[t]);
  Source.Clear;
end;

end.
