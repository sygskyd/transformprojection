{**********************************************}
{   TeeChart LinePoint Series Editor           }
{   Copyright (c) 1999-2000 by David Berneda   }
{**********************************************}
unit TeeLinePointEditor;

interface

uses TeePoEdi, ComCtrls, StdCtrls, TeCanvas, TeePenDlg, Controls, Classes;

type
  TLinePointEditor = class(TSeriesPointerEditor)
    BLines: TButtonPen;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

Uses MyPoint;

procedure TLinePointEditor.FormShow(Sender: TObject);
begin
  inherited;
  BLines.LinkPen(TMyPointSeries(Tag).LinesPen);
end;

initialization
  RegisterClass(TLinePointEditor);
end.
