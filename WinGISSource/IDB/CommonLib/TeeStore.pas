{*********************************************}
{  TeeChart Storage functions                 }
{  Copyright (c) 1997-2000 by David Berneda   }
{  All rights reserved                        }
{*********************************************}
{$I teedefs.inc}
unit TeeStore;

interface

Uses Classes, TeEngine, Chart;

{ Read a Chart from a file ( Chart1,'c:\demo.tee' ) }
Procedure LoadChartFromFile(Var AChart:TCustomChart; Const AName:String);

{ Write a Chart to a file ( Chart1,'c:\demo.tee' ) }
Procedure SaveChartToFile(AChart:TCustomChart; Const AName:String; IncludeData:Boolean{$IFDEF D4}=True{$ENDIF});

{ The same using TStream components (good for BLOB fields, etc)  }
Procedure LoadChartFromStream(Var AChart:TCustomChart; AStream:TStream);
Procedure SaveChartToStream(AChart:TCustomChart; AStream:TStream; IncludeData:Boolean{$IFDEF D4}=True{$ENDIF});

{ (Advanced) Read charts and check for errors  }
{ return True if ok, False to stop loading }
type TProcTeeCheckError=function(const Message: string): Boolean of object;

Procedure LoadChartFromStreamCheck( Var AChart:TCustomChart;
                                    AStream:TStream;
                                    ACheckError:TProcTeeCheckError
                               );

Procedure LoadChartFromFileCheck( Var AChart:TCustomChart;
                                  Const AName:String;
                                  ACheckError:TProcTeeCheckError
                                  );

{ Returns if a Series has "X" values }
Function HasNoMandatoryValues(ASeries:TChartSeries):Boolean;

{ Convert a *.tee file to text format, without point values }
Procedure ConvertTeeFileToText(Const InputFile,OutputFile:String);

{ Returns True if ASeries has text labels }
Function HasLabels(ASeries:TChartSeries):Boolean;

Const TeeTabDelimiter=#9;

type TeeFormatFlags = (tfNoMandatory,tfColor,tfLabel,tfMarkPosition);
     TeeFormatFlag  = set of TeeFormatFlags;

     TSeriesData=class
     private
       FIncludeIndex  : Boolean;
       FIncludeHeader : Boolean;
       FIncludeLabels : Boolean;
       FChart         : TCustomChart;
       FSeries        : TChartSeries;

       IFormat        : TeeFormatFlag;
       Procedure Prepare;
     protected
       Function MaxSeriesCount:Integer;
       Function PointToString(Index:Integer):String; virtual;
     public
       Constructor Create(AChart:TCustomChart; ASeries:TChartSeries); virtual;

       Function AsString:String; virtual;
       Procedure CopyToClipboard;
       Procedure SaveToFile(Const FileName:String);
       Procedure SaveToStream(AStream:TStream); virtual;

       property Chart:TCustomChart read FChart write FChart;
       property IncludeHeader:Boolean read FIncludeHeader write FIncludeHeader default False;
       property IncludeIndex:Boolean read FIncludeIndex write FIncludeIndex default False;
       property IncludeLabels:Boolean read FIncludeLabels write FIncludeLabels default True;
       property Series:TChartSeries read FSeries write FSeries;
     end;

     TSeriesDataText=class(TSeriesData)
     private
       FTextDelimiter : Char;
     protected
       Function PointToString(Index:Integer):String; override;
     public
       Constructor Create(AChart:TCustomChart; ASeries:TChartSeries); override;
       property TextDelimiter:Char read FTextDelimiter write FTextDelimiter default TeeTabDelimiter;
     end;

     TSeriesDataXML=class(TSeriesData)
     public
       Function AsString:String; override;
     end;

     TSeriesDataHTML=class(TSeriesData)
     protected
       Function PointToString(Index:Integer):String; override;
     public
       Function AsString:String; override;
     end;

     TSeriesDataXLS=class(TSeriesData)
     public
       Procedure SaveToStream(AStream:TStream); override;
     end;

Const TeeTextLineSeparator= #13#10;

implementation

Uses {$IFDEF CLX}
     QGraphics, QClipbrd,
     {$ELSE}
     Graphics, Clipbrd,
     {$ENDIF}
     SysUtils, TeeProcs, TeCanvas, TeeConst;

Const MagicTeeFile  =$3060;
      {$IFDEF D6}
      VersionTeeFile=$0150; { Delphi 6 }
      {$ELSE}
      {$IFDEF C5}
      VersionTeeFile=$0140; { C++ Builder 5 }
      {$ELSE}
      {$IFDEF D5}
      VersionTeeFile=$0130; { Delphi 5 }
      {$ELSE}
      {$IFDEF C4}
      VersionTeeFile=$0125; { C++ Builder 4 }
      {$ELSE}
      {$IFDEF D4}
      VersionTeeFile=$0120; { Delphi 4 }
      {$ELSE}
      {$IFDEF C3}
      VersionTeeFile=$0110; { C++ Builder 3 }
      {$ELSE}
      VersionTeeFile=$0100; { Delphi 3 }
      {$ENDIF}
      {$ENDIF}
      {$ENDIF}
      {$ENDIF}
      {$ENDIF}
      {$ENDIF}


Type TTeeFileHeader=Packed Record
        Magic   : Word; { secure checking }
        Version : Word; { Chart file version }
     end;

Procedure ReadSeriesData(AStream:TStream; ASeries:TChartSeries);

  Function ReadLabel:String;
  Var L : Byte;
  begin
    L:=0;
    { read the label length }
    AStream.Read(L, SizeOf(L));
    { read the label contents }
    SetString(Result, PChar(nil), L);
    AStream.Read(Pointer(Result)^, L);
  end;

Var tmpFormat       : TeeFormatFlag;
    ValuesListCount : Integer;
    NotYMandatory   : Boolean;

  Procedure ReadSeriesPoint(Index:Integer);
  Var tmpFloat : Double;
      Ax       : Double;
      Ay       : Double;
      AColor   : TColor;
      ALabel   : String;
      tt       : Integer;
      tmp      : TSeriesMarkPosition;
  begin
    { read the "X" value if exists }
    if tfNoMandatory in tmpFormat then AStream.Read(AX,Sizeof(AX));
    { read the "Y" value }
    AStream.Read(AY,Sizeof(AY));
    { Swap X <--> Y if necessary... }
    if NotYMandatory then SwapDouble(AX,AY);

    { read the Color value if exists }
    if tfColor in tmpFormat then AStream.Read(AColor,Sizeof(AColor))
                            else AColor:=clTeeColor;
    { read the Label value if exists }
    if tfLabel in tmpFormat then ALabel:=ReadLabel
                            else ALabel:='';

    With ASeries do
    begin
      { read the rest of lists values }
      for tt:=2 to ValuesListCount-1 do
      begin
        AStream.Read(tmpFloat,SizeOf(tmpFloat));
        ValuesList[tt].TempValue:=tmpFloat;
      end;
      { add the new point }
      if tfNoMandatory in tmpFormat then AddXY(AX,AY,ALabel,AColor)
                                    else Add(AY,ALabel,AColor);

      if tfMarkPosition in tmpFormat then
      begin
        tmp:=TSeriesMarkPosition.Create;
        With tmp,AStream do
        begin
          Read(ArrowFrom,SizeOf(ArrowFrom));
          Read(ArrowFix,SizeOf(ArrowFix));
          Read(ArrowTo,SizeOf(ArrowTo));
          Read(Custom,SizeOf(Custom));
          Read(Height,SizeOf(Height));
          Read(LeftTop,SizeOf(LeftTop));
          Read(Width,SizeOf(Width));
        end;
        Marks.Positions.Add(tmp);
      end;
    end;
  end;

Var tmpCount : Integer;
    t        : Integer;
begin
  { empty the Series }
  ASeries.Clear;
  { read the point flags }
  AStream.Read(tmpFormat,SizeOf(tmpFormat));
  { read the number of points }
  AStream.Read(tmpCount,Sizeof(tmpCount));
  { read each point }
  NotYMandatory:=ASeries.MandatoryValueList<>ASeries.YValues;
  ValuesListCount:=ASeries.ValuesList.Count;
  for t:=0 to tmpCount-1 do ReadSeriesPoint(t);
end;

Procedure ReadChartData(AStream:TStream; AChart:TCustomChart);
Var t : Integer;
begin { read each Series data }
  for t:=0 to AChart.SeriesCount-1 do ReadSeriesData(AStream,AChart.Series[t]);
end;

{ Special reader to skip Delphi 3 or 4 new properties when
  reading Charts in Delphi 1.0 or 2.0 }
type TChartReader=class(TReader)
     protected
       function Error(const Message: string): Boolean; override;
     public
       CheckError:TProcTeeCheckError;
     end;

function TChartReader.Error(const Message: string): Boolean;
begin
  if Assigned(CheckError) then result:=CheckError(Message)
                          else result:=True;
end;

{ Reads Series and Points from a Stream into a Chart }
Procedure LoadChartFromStreamCheck( Var AChart:TCustomChart;
                                    AStream:TStream;
                                    ACheckError:TProcTeeCheckError);
Var Header : TTeeFileHeader;
    t      : Integer;
    Reader : TChartReader;
    tmp    : TCustomChart;
begin
  AStream.Position:=0;
  { read file header }
  AStream.Read(Header,SizeOf(Header));
  { check is a valid Tee file }
  if Header.Magic=MagicTeeFile then
  begin
    { disable auto-repaint }
    AChart.AutoRepaint:=False;

    { remove all child Series and Tools }
    AChart.FreeAllSeries;
    AChart.Tools.Clear;

    { read the Chart and Series properties }
    Reader := TChartReader.Create(AStream, 4096);
    try
      Reader.CheckError:=ACheckError;
      tmp:=AChart;
      Reader.ReadRootComponent(AChart);
      AChart:=tmp;
    finally
      Reader.Free;
    end;
    if Assigned(AChart) then
    begin
      { read the Series points }
      if AStream.Size>AStream.Position then
         ReadChartData(AStream,AChart);

      { change each Series ownership }
      if AChart.Owner<>nil then
      for t:=0 to AChart.SeriesCount-1 do
      begin
        AChart.Series[t].Owner.RemoveComponent(AChart.Series[t]);
        AChart.Owner.InsertComponent(AChart.Series[t]);
      end;
      { restore AutoRepaint }
      AChart.AutoRepaint:=True;
      AChart.Invalidate;
    end
    else raise Exception.Create(TeeMsg_InvalidTeeFile);
  end
  else raise Exception.Create(TeeMsg_WrongTeeFileFormat);
end;

Procedure LoadChartFromStream(Var AChart:TCustomChart; AStream:TStream);
begin
  LoadChartFromStreamCheck(AChart,AStream,nil);
end;

Procedure LoadChartFromFileCheck( Var AChart:TCustomChart;
                                  Const AName:String;
                                  ACheckError:TProcTeeCheckError );
Var tmp:TFileStream;
begin
  tmp:=TFileStream.Create(AName,fmOpenRead);
  try
    LoadChartFromStreamCheck(AChart,tmp,ACheckError);
  finally
    tmp.Free;
  end;
end;

Procedure LoadChartFromFile(Var AChart:TCustomChart; Const AName:String);
begin
  LoadChartFromFileCheck(AChart,AName,nil);
end;

{ Create a text file from a binary *.tee file }
Procedure ConvertTeeFileToText(Const InputFile,OutputFile:String);
var SInput  : TFileStream;
    SOutput : TFileStream;
    Header  : TTeeFileHeader;
begin
  SInput:=TFileStream.Create(InputFile,fmOpenRead);
  try
    { read file header }
    SInput.Read(Header,SizeOf(Header));
    { check is a valid Tee file }
    if Header.Magic=MagicTeeFile then
    begin
      SOutput:=TFileStream.Create(OutputFile,fmCreate);
      try
        ObjectBinaryToText(SInput,SOutput);
      finally
        SOutput.Free;
      end;
    end;
  finally
    SInput.Free;
  end;
end;

{ Returns if a Series has "X" values (or Y values for HorizBar series) }
Function HasNoMandatoryValues(ASeries:TChartSeries):Boolean;
var t        : Integer;
    tmpCount : Integer;
    tmp      : TChartValueList;
begin
  result:=False;
  With ASeries do
  if Count>0 then
  begin
    tmp:=ASeries.NotMandatoryValueList;
    if (tmp.First=0) and (tmp.Last=Count-1) then
    begin
      tmpCount:=MinLong(10000,Count-1);
      for t:=0 to tmpCount do
      if tmp.Value[t]<>t then
      begin
        result:=True;
        Exit;
      end;
    end
    else result:=True;
  end;
end;

{ Returns if a Series has Colors }
Function HasColors(ASeries:TChartSeries):Boolean;
var t        : Integer;
    tmpCount : Integer;
    tmpColor : TColor;
    tmpSeriesColor:TColor;
begin
  result:=False;
  With ASeries do
  begin
    tmpSeriesColor:=SeriesColor;
    tmpCount:=MinLong(10000,Count-1);
    for t:=0 to tmpCount do
    begin
      tmpColor:=ValueColor[t];
      if (tmpColor<>clTeeColor) and
         (tmpColor<>tmpSeriesColor) then
      begin
        result:=True;
        exit;
      end;
    end;
  end;
end;

{ Returns if a Series has labels }
Function HasLabels(ASeries:TChartSeries):Boolean;
var t        : Integer;
    tmpCount : Integer;
begin
  result:=False;
  tmpCount:=MinLong(10000,ASeries.Count-1);
  for t:=0 to tmpCount do
  if ASeries.XLabel[t]<>'' then
  begin
    result:=True;
    Exit;
  end;
end;

{ Determine what a Series point is made of }
Function SeriesGuessContents(ASeries:TChartSeries):TeeFormatFlag;
begin
  if HasNoMandatoryValues(ASeries) then result:=[tfNoMandatory]
                                   else result:=[];
  if HasColors(ASeries) then result:=result+[tfColor];
  if HasLabels(ASeries) then result:=result+[tfLabel];
  if ASeries.Marks.Positions.ExistCustom then result:=result+[tfMarkPosition];
end;

Procedure WriteSeriesData(AStream:TStream; ASeries:TChartSeries);

  Procedure WriteLabel(Const AString:String);
  Var L : Byte;
  begin
    L:=Length(AString);
    AStream.Write(L,SizeOf(L));
    AStream.Write(PChar(AString)^,L);
  end;

Var tmpFormat      : TeeFormatFlag;
    tmpNoMandatory : TChartValueList;

  Procedure WriteSeriesPoint(Index:Integer);
  Var tmpFloat : Double;
      tmpColor : TColor;
      tt       : Integer;
  begin
    { write the "X" point value, if exists }
    if tfNoMandatory in tmpFormat then
    begin
      tmpFloat:=tmpNoMandatory.Value[Index];
      AStream.Write(tmpFloat,Sizeof(tmpFloat));
    end;
    { write the "Y" point value }
    tmpFloat:=ASeries.MandatoryValueList.Value[Index];
    AStream.Write(tmpFloat,Sizeof(tmpFloat));

    { write the Color point value, if exists }
    if tfColor in tmpFormat then
    begin
      tmpColor:=ASeries.ValueColor[Index];
      AStream.Write(tmpColor,Sizeof(tmpColor));
    end;
    { write the Label point value, if exists }
    if tfLabel in tmpFormat then WriteLabel(ASeries.XLabel[Index]);
    { write the rest of values (always) }
    for tt:=2 to ASeries.ValuesList.Count-1 do
    begin
      tmpFloat:=ASeries.ValuesList[tt].Value[Index];
      AStream.Write(tmpFloat,SizeOf(tmpFloat));
    end;
    if tfMarkPosition in tmpFormat then
    begin
      With ASeries.Marks.Positions[Index],AStream do
      begin
        Write(ArrowFrom,SizeOf(ArrowFrom));
        Write(ArrowFix,SizeOf(ArrowFix));
        Write(ArrowTo,SizeOf(ArrowTo));
        Write(Custom,SizeOf(Custom));
        Write(Height,SizeOf(Height));
        Write(LeftTop,SizeOf(LeftTop));
        Write(Width,SizeOf(Width));
      end;
    end;
  end;

var t   : Integer;
    tmp : Integer;
begin
  { write the "flag" containing the format of a point }
  tmpFormat:=SeriesGuessContents(ASeries);
  AStream.Write(tmpFormat,SizeOf(tmpFormat));
  { write all points. pre-calculate tmpNoMandatory }
  tmpNoMandatory:=ASeries.NotMandatoryValueList;
  { write the number of points }
  tmp:=ASeries.Count;
  AStream.Write(tmp,Sizeof(tmp));
  for t:=0 to tmp-1 do WriteSeriesPoint(t);
end;

Procedure WriteChartData(AStream:TStream; AChart:TCustomChart);
Var t : Integer;
begin
  With AChart do
  for t:=0 to SeriesCount-1 do WriteSeriesData(AStream,Series[t]);
end;

Procedure SaveChartToStream(AChart:TCustomChart; AStream:TStream; IncludeData:Boolean{$IFDEF D4}=True{$ENDIF});
var Header : TTeeFileHeader;
    tmp    : TCustomChart;
    OldName: TComponentName;
begin
  { write file header, with "magic" number and version }
  Header.Magic:=MagicTeeFile;
  Header.Version:=VersionTeeFile;
  AStream.Write(Header,SizeOf(Header));
  { write the Chart, Series and Tools properties }
  tmp:=AChart;
  if not (csDesigning in AChart.ComponentState) then
  begin
    OldName:=AChart.Name;
    AChart.Name:='';
  end;
  AStream.WriteComponent(AChart);
  AChart:=tmp;
  if not (csDesigning in AChart.ComponentState) then
     AChart.Name:=OldName;
  { write the Series data points }
  if IncludeData then WriteChartData(AStream,AChart);
end;

Procedure SaveChartToFile(AChart:TCustomChart; Const AName:String; IncludeData:Boolean);
Var tmp     : TFileStream;
    tmpName : String;
begin
  tmpName:=AName;
  if ExtractFileExt(AName)='' then tmpName:=AName+'.'+TeeMsg_TeeExtension;
  tmp:=TFileStream.Create(tmpName,fmCreate);
  try
    SaveChartToStream(AChart,tmp,IncludeData);
  finally
    tmp.Free;
  end;
end;

{ TSeriesData }
Constructor TSeriesData.Create(AChart:TCustomChart; ASeries:TChartSeries);
begin
  FChart:=AChart;
  FSeries:=ASeries;
  FIncludeLabels:=True;
end;

Procedure TSeriesData.Prepare;
var tmp : TChartSeries;
begin
  if Assigned(FSeries) then tmp:=FSeries
                       else if FChart.SeriesCount>0 then tmp:=Chart[0]
                                                    else tmp:=nil;
  if Assigned(tmp) then IFormat:=SeriesGuessContents(tmp);
  if not FIncludeLabels then IFormat:=IFormat-[tfLabel];
end;

Function TSeriesData.AsString:String;
Var tmp : Integer;
    t   : Integer;
begin
  Prepare;
  result:='';
  tmp:=MaxSeriesCount;
  for t:=0 to tmp-1 do result:=result+PointToString(t)+TeeTextLineSeparator;
end;

Procedure TSeriesData.CopyToClipboard;
begin
  Clipboard.AsText:=AsString;
end;

Function TSeriesData.MaxSeriesCount:Integer;
var t : Integer;
begin
  if Assigned(FSeries) then result:=FSeries.Count
  else
  begin
    result:=-1;
    for t:=0 to FChart.SeriesCount-1 do
    if (result=-1) or (FChart[t].Count>result) then
       result:=FChart[t].Count;
  end;
end;

Procedure TSeriesData.SaveToStream(AStream:TStream);
var tmpSt : String;
begin
  tmpSt:=AsString;
  AStream.Write(PChar(tmpSt)^,Length(tmpSt));
end;

Procedure TSeriesData.SaveToFile(Const FileName:String);
var tmp : TFileStream;
begin
  tmp:=TFileStream.Create(FileName,fmCreate);
  try
    SaveToStream(tmp);
  finally
    tmp.Free;
  end;
end;

function TSeriesData.PointToString(Index: Integer): String;
begin
end;

{ TSeriesDataText }
constructor TSeriesDataText.Create(AChart:TCustomChart; ASeries: TChartSeries);
begin
  inherited;
  FTextDelimiter:=TeeTabDelimiter;
end;

function TSeriesDataText.PointToString(Index: Integer): String;

  Procedure Add(Const St:String);
  begin
    if result='' then result:=St
                 else result:=result+FTextDelimiter+St;
  end;

  Procedure DoSeries(ASeries:TChartSeries);
  Var tt : Integer;
  begin
    { the point Label text, if exists }
    if tfLabel in IFormat then Add(ASeries.XLabel[Index]);

    { the "X" point value, if exists }
    if tfNoMandatory in IFormat then Add(FloatToStr(ASeries.NotMandatoryValueList.Value[Index]));

    { the "Y" point value }
    Add(FloatToStr(ASeries.MandatoryValueList.Value[Index]));

    { write the rest of values (always) }
    for tt:=2 to ASeries.ValuesList.Count-1 do
        result:=result+FTextDelimiter+FloatToStr(ASeries.ValuesList[tt].Value[Index]);
  end;

var t : Integer;
begin
  if IncludeIndex then Str(Index,result) else result:='';

  if Assigned(FSeries) then DoSeries(FSeries)
  else
  for t:=0 to FChart.SeriesCount-1 do DoSeries(FChart[t]);
end;

{ TSeriesDataXML }
Function TSeriesDataXML.AsString:String;

  Function SeriesPoints(ASeries:TChartSeries):String;

    Function GetPointString(Index:Integer):String;

      Procedure AddResult(Const St:String);
      begin
        if result='' then result:=St else result:=result+St;
      end;

      Function Get(AList:TChartValueList):String;
      begin
        with AList do
             result:=' '+Name+'="'+FloatToStr(Value[Index])+'"';
      end;

    var tt : Integer;
    begin
      if IncludeIndex then result:='index="'+TeeStr(Index)+'"'
                      else result:='';

      With ASeries do
      begin
        { the point Label text, if exists }
        if tfLabel in IFormat then result:=result+' text="'+XLabel[Index]+'"';

        { the "X" point value, if exists }
        if tfNoMandatory in IFormat then AddResult(Get(NotMandatoryValueList));

        { the "Y" point value }
        AddResult(Get(MandatoryValueList));

        { write the rest of values (always) }
        for tt:=2 to ValuesList.Count-1 do result:=result+Get(ValuesList[tt]);
      end;
    end;

  var t : Integer;
  begin
    result:='';
    if ASeries.Count>0 then
    begin
      for t:=0 to ASeries.Count-1 do
          result:=result+'<point '+GetPointString(t)+'/>'+TeeTextLineSeparator;
      result:=result+TeeTextLineSeparator;
    end;
  end;

  Function XMLSeries(ASeries:TChartSeries):String;
  begin
    result:=
          '<series title="'+SeriesTitleOrName(ASeries)+'" type="'+
          GetGallerySeriesName(ASeries)+'">'+TeeTextLineSeparator+
          '<points count="'+TeeStr(ASeries.Count)+'">'+TeeTextLineSeparator+
          SeriesPoints(ASeries)+
          '</points>'+TeeTextLineSeparator+
          '</series>'+TeeTextLineSeparator+TeeTextLineSeparator;
  end;

var t : Integer;
begin
  result:='<?xml version="1.0" ?>'+TeeTextLineSeparator;
  if Assigned(FSeries) then result:=result+XMLSeries(FSeries)
  else
  begin
    result:=result+'<chart>';
    for t:=0 to FChart.SeriesCount-1 do result:=result+XMLSeries(FChart[t]);
    result:=result+'</chart>';
  end;
end;

{ TSeriesDataHTML }
function TSeriesDataHTML.AsString: String;

  Function Header:String;

    Function HeaderSeries(ASeries:TChartSeries):String;
    var t : Integer;
    begin
      result:='';
      if tfLabel in IFormat then result:=result+'<td>'+TeeMsg_Text+'</td>';
      With ASeries do
      begin
        if tfNoMandatory in IFormat then
           result:=result+'<td>'+NotMandatoryValueList.Name+'</td>';

        if ValuesList.Count=2 then
           result:=result+'<td>'+SeriesTitleOrName(ASeries)+'</td>'
        else
        begin
          result:=result+'<td>'+MandatoryValueList.Name+'</td>';
          for t:=2 to ValuesList.Count-1 do
                result:=result+'<td>'+ValuesList[t].Name+'</td>';
        end;
      end;
    end;

  var t : Integer;
  begin
    result:='<tr>';
    if IncludeIndex then result:=result+'<td>'+TeeMsg_Index+'</td>';

    if Assigned(FSeries) then result:=result+HeaderSeries(FSeries)
    else
    for t:=0 to FChart.SeriesCount-1 do result:=result+HeaderSeries(FChart[t]);

    result:=result+'</tr>';
  end;

begin
  Prepare;
  result:='<table border="1">'+TeeTextLineSeparator;
  if IncludeHeader then result:=result+Header+TeeTextLineSeparator;
  result:=result+inherited AsString+TeeTextLineSeparator+'</table>';
end;

Function TSeriesDataHTML.PointToString(Index:Integer):String;

  Function GetPointString:String;

    Function GetPointStringSeries(ASeries:TChartSeries):String;

      Function CellDouble(Const Value:Double):String;
      begin
        result:='<td>'+FloatToStr(Value)+'</td>';
      end;

    Const EmptyCell='<td></td>';
    Var tt : Integer;
    begin
      result:='';
      With ASeries do
      if (Count-1)<Index then
      begin
        if tfLabel in IFormat then result:=result+EmptyCell;
        if tfNoMandatory in IFormat then result:=result+EmptyCell;
        result:=result+EmptyCell;
        for tt:=2 to ValuesList.Count-1 do result:=result+EmptyCell;
      end
      else
      begin
        { the point Label text, if exists }
        if tfLabel in IFormat then result:=result+'<td>'+XLabel[Index]+'</td>';

        { the "X" point value, if exists }
        if tfNoMandatory in IFormat then
           result:=result+CellDouble(NotMandatoryValueList.Value[Index]);

        { the "Y" point value }
        result:=result+CellDouble(MandatoryValueList.Value[Index]);

        { write the rest of values (always) }
        for tt:=2 to ValuesList.Count-1 do
            result:=result+CellDouble(ValuesList[tt].Value[Index]);
      end;
    end;

  var t : Integer;
  begin
    if IncludeIndex then result:='<td>'+TeeStr(Index)+'</td>'
                    else result:='';

    if Assigned(FSeries) then result:=result+GetPointStringSeries(FSeries)
    else
    for t:=0 to FChart.SeriesCount-1 do
        result:=result+GetPointStringSeries(FChart[t])
  end;

begin
  result:='<tr>'+GetPointString+'</tr>';
end;

{ TSeriesDataXLS }
Procedure TSeriesDataXLS.SaveToStream(AStream:TStream);
Const Attr:Array[0..2] of Byte=(0,0,0);
var Buf : Array[0..4] of Word;
    Row : Integer;
    Col : Integer;

  Procedure WriteBuf(Value,Size:Word);
  begin
    Buf[0]:=Value;
    Buf[1]:=Size;
    AStream.Write(Buf,2*SizeOf(Word));
  end;

  Procedure WriteParams(Value,Size:Word);
  begin
    WriteBuf(Value,Size+2*SizeOf(Word)+SizeOf(Attr));
    if IncludeHeader then WriteBuf(Row+1,Col)
                     else WriteBuf(Row,Col);
    AStream.Write(Attr,SizeOf(Attr));
  end;

  procedure WriteDouble(Const Value:Double);
  begin
    WriteParams(3,SizeOf(Double));
    AStream.WriteBuffer(Value,SizeOf(Double));
  end;

  procedure WriteText(Const Value:ShortString);
  begin
    WriteParams(4,Length(Value)+1);
    AStream.Write(Value,Length(Value)+1)
  end;

  procedure WriteNull;
  begin
    WriteParams(1,0);
  end;

  Procedure WriteHeaderSeries(ASeries:TChartSeries);
  var tt : Integer;
  begin
    if tfLabel in IFormat then
    begin
      WriteText(TeeMsg_Text);
      Inc(Col);
    end;
    if tfNoMandatory in IFormat then
    begin
      WriteText(ASeries.NotMandatoryValueList.Name);
      Inc(Col);
    end;
    for tt:=1 to ASeries.ValuesList.Count-1 do
    begin
      WriteText(ASeries.ValuesList[tt].Name);
      Inc(Col);
    end;
  end;

  Procedure WriteSeries(ASeries:TChartSeries);
  var tt : Integer;
  begin
    if (ASeries.Count-1)<Row then
    begin
      if tfLabel in IFormat then
      begin
        WriteText('');
        Inc(Col);
      end;
      if tfNoMandatory in IFormat then
      begin
        WriteNull;
        Inc(Col);
      end;
      for tt:=1 to ASeries.ValuesList.Count-1 do
      begin
        WriteNull;
        Inc(Col);
      end;
    end
    else
    begin
      if tfLabel in IFormat then
      begin
        WriteText(ASeries.XLabel[Row]);
        Inc(Col);
      end;
      if tfNoMandatory in IFormat then
      begin
        WriteDouble(ASeries.NotMandatoryValueList.Value[Row]);
        Inc(Col);
      end;
      if ASeries.IsNull(Row) then
      for tt:=1 to ASeries.ValuesList.Count-1 do
      begin
        WriteNull;
        Inc(Col);
      end
      else
      begin
        WriteDouble(ASeries.MandatoryValueList.Value[Row]);
        Inc(Col);
        for tt:=2 to ASeries.ValuesList.Count-1 do
        begin
          WriteDouble(ASeries.ValuesList[tt].Value[Row]);
          Inc(Col);
        end;
      end;
    end;
  end;

Const BeginExcel : Array[0..5] of Word=($809,8,0,$10,0,0);
      EndExcel   : Array[0..1] of Word=($A,0);
Var tt  : Integer;
    tmp : Integer;
begin
  Prepare;
  AStream.WriteBuffer(BeginExcel,SizeOf(BeginExcel)); { begin and BIF v5 }

  WriteBuf($0200,5*SizeOf(Word));  { row x col }

  Buf[0]:=0;
  Buf[2]:=0;
  Buf[3]:=0; { columns }
  Buf[4]:=0;

  Buf[1]:=MaxSeriesCount; { rows }
  if IncludeHeader then Inc(Buf[1]);

  if IncludeIndex then Inc(Buf[3]);
  if Assigned(FSeries) then tmp:=1
                       else tmp:=FChart.SeriesCount;
  if tfLabel in IFormat then Inc(Buf[3],tmp);
  if tfNoMandatory in IFormat then Inc(Buf[3],tmp);
  if Assigned(FSeries) then
     Inc(Buf[3],FSeries.ValuesList.Count-1)
  else
  for Row:=0 to FChart.SeriesCount-1 do
     Inc(Buf[3],FChart[Row].ValuesList.Count-1);

  AStream.Write(Buf,5*SizeOf(Word));

  if IncludeHeader then
  begin
    Row:=-1;
    Col:=0;
    if IncludeIndex then
    begin
      WriteText(TeeMsg_Index);
      Inc(Col);
    end;
    if Assigned(FSeries) then WriteHeaderSeries(FSeries)
    else
    for tt:=0 to FChart.SeriesCount-1 do WriteHeaderSeries(FChart[tt]);
  end;

  for Row:=0 to MaxSeriesCount-1 do
  begin
    Col:=0;
    if IncludeIndex then
    begin
      WriteDouble(Row);
      Inc(Col);
    end;

    if Assigned(FSeries) then WriteSeries(FSeries)
    else
    for tt:=0 to FChart.SeriesCount-1 do WriteSeries(FChart[tt]);
  end;

  AStream.WriteBuffer(EndExcel,SizeOf(EndExcel)); { end }
end;

{$IFNDEF TEEOCX}
type TODBCChart=class(TChart)
     end;

initialization
  RegisterClass(TODBCChart);
{$ENDIF}
end.

