Unit Cache;

Interface

Type {*************************************************************************}
     { Record TCacheItem                                                       }
     {-------------------------------------------------------------------------}
     { State            = Statusflags f�r den Second-Chance Algorithmus        }
     { Item             = Zeiger auf die Daten                                 }
     { Size             = G��e der Daten                                       }
     {*************************************************************************}
     TCacheItem         = Record
       State            : Byte;
       Item             : Pointer;
       Size             : LongInt;
       LockCnt          : Integer;
     end;

     PCacheItems        = ^TCacheItems;
     TCacheItems        = Array[0..1024] of TCacheItem;

     TCacheEvent        = Procedure(Item:Pointer) of object;

     {*************************************************************************}
     { Class TCache                                                            }
     {-------------------------------------------------------------------------}
     { Verwaltet Zeiger auf Datenstrukturen und verindert, da� die gesamte     }
     { Speichergr��e eine gewisse Grenze �berschreitet. Wird beim Einf�gen     }
     { eines Items die maximale Speichergr��e �berschritten, so werden mit     }
     { Hilfe eines etwas abgewandelten Second-Chance Algorithmus solange       }
     { Eintr�ge freigegeben, bis die maximale Gr��e wieder erreicht wird.      }
     { Der Unterschied zum klassischen Second-Chance Algorithmus besteht darin,}
     { da� die Gr��e der Eintr�ge unterschiedlich sein kann und da� die        }
     { Anzahl der Eintr�ge nicht konstant ist (solange die maximale Speicher-  }
     { gr��e nicht �berschritten ist werden Eintr�ge hinzugef�gt).             }
     { FreeItem und SaveItem m�ssen �berschrieben werden. SaveItem wird        }
     { aufgerufen, bevor ein als dirty markierter Eintrag freigegeben wird.    }
     { Die Methode mu� den entsprechenden Eintrag sichern. FreeItem wird       }
     { aufgerufen, wenn ein Eintrag freigegeben werden mu�.                    }
     {-------------------------------------------------------------------------}
     { FDirectory       = Verzeichnis der geladenen CacheItems und Daten       }
     {                    f�r den Second-Chance Algorithmus                    }
     { FCacheSize       = maximale Speichergr��e                               }
     { FCapacity        = Anzahl Eintr�ge f�r die Platz im Directory ist       }
     { FCount           = aktuelle Anzahl von Eintr�gen                        }
     { FCurrentFrame    = Aktueller Zeiger f�r den Second-Chance Algorithmus   }
     {*************************************************************************}
     TCache             = Class
      Private
       FCacheSize       : LongInt;
       FCapacity        : Integer;
       FCount           : Integer;
       FCurrentFrame    : Integer;
       FCurrentSize     : LongInt;
       FOnFreeItem      : TCacheEvent;
       FOnSaveItem      : TCacheEvent;
       Procedure   SetCapacity(ACapacity:Integer);
       Property    Capacity:Integer read FCapacity write SetCapacity;
       Procedure   DefaultOnFreeItem(Item:Pointer);
       Procedure   DefaultOnSaveItem(Item:Pointer);
       Procedure   Expand;
       Function    IsModified(Item:Integer):Boolean;
       Procedure   SetCacheSize(ASize:LongInt);
       Procedure   SetModified(Item:Integer;ASet:Boolean);
       Procedure   Shrink(ASize:LongInt);
      Public
       FDirectory       : PCacheItems;
       Constructor Create(ACacheSize:LongInt;ACapacity:Integer);
       Destructor  Destroy; override;
       Function    Add(AItem:Pointer;ASize:LongInt):Integer;
       Property    CacheSize:LongInt read FCacheSize write SetCacheSize;
       Procedure   ChangeSize(Item:Integer;NewSize:Longint);
       Property    Count:Integer read FCount;
       Property    CurrentSize:LongInt read FCurrentSize;
       Procedure   Delete(Item:Integer);
       Procedure   Lock(Item:Integer);
       Property    Modified[Item:Integer]:Boolean read IsModified write SetModified;
       Procedure   Referenced(Item:Integer);
       Property    OnFreeItem:TCacheEvent read FOnFreeItem write FOnFreeItem;
       Property    OnSaveItem:TCacheEvent read FOnSaveItem write FOnSaveItem;
       Procedure   UnLock(Item:Integer);
     end;

Implementation

Uses SysUtils;

Const csUsed            = $01;
      csReferenced      = $02;
      csModified        = $04;
      csDirty           = $08;

{==============================================================================}
{ TCache                                                                       }
{==============================================================================}

{******************************************************************************}
{ Constructor TCache.Create                                                    }
{------------------------------------------------------------------------------}
{ Erzeugt einen neuen Cache. ACacheSize gibt die maximale Speichergr��e f�r    }
{ den Cache an, ACapacity die (ungef�hre) Anzahl von Eintr�gen, bei der die    }
{ maximale Cachegr��e erreicht werden d�rfte. Capacity wird automatisch vom    }
{ Cache vergr��ert oder verkleinert.                                           }
{******************************************************************************}
Constructor TCache.Create
   (
   ACacheSize      : LongInt;
   ACapacity       : Integer
   );
  begin
    inherited Create;
    CacheSize:=ACacheSize;
    Capacity:=ACapacity;
    OnFreeItem:=DefaultOnFreeItem;
    OnSaveItem:=DefaultOnSaveItem;               
  end;

{******************************************************************************}
{ Destructor TCache.Destroy                                                    }
{------------------------------------------------------------------------------}
{ Gibt den vom der Cacheverwaltung belegten Speicher frei. Die Item-Zeiger im  }
{ Cachedirectory werden nicht freigegeben.                                     }
{******************************************************************************}
Destructor TCache.Destroy;
  begin
    if FCapacity>0 then FreeMem(FDirectory,FCapacity*SizeOf(TCacheItem));
    inherited Destroy;
  end;

{******************************************************************************}
{ Procedure TCache.Shrink                                                      }
{------------------------------------------------------------------------------}
{ Entfernt solange Eintr�ge aus dem Cache, bis die angegebene Speichergr��e    }
{ unterschritten wird. Die Auswahl der Eintr�ge erfolgt mit dem Second-Chance  }
{ Algorithmus.                                                                 }
{******************************************************************************}
Procedure TCache.Shrink
   (
   ASize           : LongInt
   );
  begin
    while CurrentSize>ASize do begin
      with FDirectory^[FCurrentFrame] do if State and csUsed<>0 then begin
        if State and csReferenced<>0 then State:=State and not csReferenced
        else if State and csModified<>0 then begin
          State:=State and not csModified;
          State:=State or csDirty;
        end
        else if LockCnt<=0 then begin
          if State and csDirty<>0 then OnSaveItem(Item);
          OnFreeItem(Item);
          State:=0;
          FCurrentSize:=FCurrentSize-Size;
          Dec(FCount);
          if CurrentSize<=ASize then Break;
        end;
      end;
      FCurrentFrame:=(FCurrentFrame+1) Mod FCapacity;
    end;
  end;

{******************************************************************************}
{ Procedure TCache.SetCacheSize                                                }
{------------------------------------------------------------------------------}
{ Ver�ndert die maximale Speichergr��e des Cache. Wird der maximale Speicher   }
{ verkleinert, so wird die aktuelle Gr��e mittels Shrink auf den neuen Wert    }
{ angepa�t (Eintr�ge freigegeben).                                             }
{******************************************************************************}
Procedure TCache.SetCacheSize
   (
   ASize           : LongInt
   );
  begin
    FCacheSize:=ASize;
    Shrink(FCacheSize);
  end;

{******************************************************************************}
{ Procedure TCache.SetCapacity                                                 }
{------------------------------------------------------------------------------}
{ Setzt die maximale Anzahl der Eintr�ge im Directory auf den �bergebenen      }
{ Wert. Das Directory kann nur vergr��ert werden.                              }
{******************************************************************************}
Procedure TCache.SetCapacity
   (
   ACapacity       : Integer
   );
  var HelpPtr      : PCacheItems;
  begin
    if ACapacity>FCapacity then begin
      if FCapacity=0 then begin
        GetMem(FDirectory,ACapacity*SizeOf(TCacheItem));
        FillChar(FDirectory^[0],ACapacity*SizeOf(TCacheItem),#0);
      end
      else begin
        GetMem(HelpPtr,ACapacity*SizeOf(TCacheItem));
        Move(FDirectory^[0],HelpPtr^[0],FCapacity*SizeOf(TCacheItem));
        FillChar(HelpPtr^[FCapacity],(ACapacity-FCapacity)*SizeOf(TCacheItem),#0);
        FreeMem(FDirectory,FCapacity*SizeOf(TCacheItem));
        FDirectory:=HelpPtr;
      end;
      FCapacity:=ACapacity;
    end;
  end;

{******************************************************************************}
{ Function TCache.Add                                                          }
{------------------------------------------------------------------------------}
{ F�gt ein Item in den Cache ein. Wird durch das neue Item die maximale        }
{ Speichergr��e �berschritten, so werden solange Eintr�ge freigegeben, bis     }
{ das Einf�gen ohne �berschreitung der maximalen Speichergr��e durchgef�hrt    }
{ werden kann. Add liefert die Position im Directory zur�ck, an der das        }
{ Item eingef�gt wurde. Dieser Index wird z.B. f�r Delete ben�tigt.            }
{******************************************************************************}
Function TCache.Add
   (
   AItem           : Pointer;
   ASize           : LongInt
   )
   : Integer;
  var Position     : Integer;
  begin
    Shrink(CacheSize-ASize);                { Cachegr��e anpassen              }
    if FCount=FCapacity then Expand;        { ggf. Directory vergr��ern        }
    { freien Eintrag zum Einf�gen suchen (Used und Modified gel�scht)          }
    Position:=FCurrentFrame;
    while FDirectory^[Position].State and csUsed<>0 do
        Position:=(Position+1) Mod FCapacity;
    { Directoryeintrag setzen, Used-Bit wird gesetzt                           }
    with FDirectory^[Position] do begin
      State:=csReferenced+csUsed;
      LockCnt:=0;
      Item:=AItem;
      Size:=ASize;
    end;
    FCurrentSize:=FCurrentSize+ASize;       { aktuelle Cachegr��e updaten      }
    Inc(FCount);                            { ein Eintrag mehr im Directory    }
    Result:=Position;
  end;

{******************************************************************************}
{ Procedure TCache.Delete                                                      }
{------------------------------------------------------------------------------}
{ L�scht einen Eintrag aus dem Cache. Item ist der Index des Eintrags im       }
{ Directory. Mu� der Eintrag noch gesichert werden, wird OnSaveItem aufgerufen.}
{ Zur Freigabe wird OnFreeItem aufgerufen.                                     }
{******************************************************************************}
Procedure TCache.Delete
   (
   Item            : Integer
   );
  begin
    {$IFDEF DEBUG}
    if (Item>Capacity)
        or (Item<0) then Raise Exception.Create('TCache.Delete: Index out of range');
    if FDirectory^[Item].State and csUsed=0 then
        Raise Exception.Create('TCache.Delete: Unused item specified');
    {$ENDIF}
    with FDirectory^[Item] do begin
      FCurrentSize:=FCurrentSize-Size;
      State:=0;
    end;
    Dec(FCount);
  end;

{******************************************************************************}
{ Procedure TCache.Modified                                                    }
{------------------------------------------------------------------------------}
{ Mu� aufgerufen werden, wenn ein Cache-Eintrag modifiziert wird. Modified     }
{ setzt das Modified-Bit des Cache-Eintrags und l�scht das Dirty-Bit. Item ist }
{ der Index des Eintrags in Directory.                                         }
{******************************************************************************}
Procedure TCache.SetModified
   (
   Item            : Integer;
   ASet            : Boolean
   );
  begin
    {$IFDEF DEBUG}
    if (Item>Capacity)
        or (Item<0) then Raise Exception.Create('TCache.Modified: Index out of range');
    if FDirectory^[Item].State and csUsed=0 then
        Raise Exception.Create('TCache.Modified: Unused item specified');
    {$ENDIF}
    with FDirectory^[Item] do begin
      if ASet then begin
        State:=State or (csReferenced+csModified);
        State:=State and not csDirty;
      end
      else State:=State and not (csModified+csDirty);
    end;
  end;

{******************************************************************************}
{ Procedure TCache.Referenced                                                  }
{------------------------------------------------------------------------------}
{ Referenced mu� aufgerufen, wenn ein Eintrag im Cache gelesen wird. Referenced}
{ setzt das Used-Bit des Cache-Eintrags. Item ist der Index des Eintrags im    }
{ Directory.                                                                   }
{******************************************************************************}
Procedure TCache.Referenced
   (
   Item            : Integer
   );
  begin
    {$IFDEF DEBUG}
    if (Item>Capacity)
        or (Item<0) then Raise Exception.Create('TCache.Referenced: Index out of range');
    if FDirectory^[Item].State and csUsed=0 then
        Raise Exception.Create('TCache.Referenced: Unused item specified');
    {$ENDIF}
    FDirectory^[Item].State:=FDirectory^[Item].State or csReferenced;
  end;

{******************************************************************************}
{ Procedure TCache.Expand                                                      }
{------------------------------------------------------------------------------}
{ Wird aufgerufen, wenn das Directory zu vergr��ern ist.                       }
{******************************************************************************}
Procedure TCache.Expand;
  begin
    Capacity:=Capacity+4;
  end;

Procedure TCache.DefaultOnFreeItem
   (
   Item            : Pointer
   );
  begin
  end;

Procedure TCache.DefaultOnSaveItem
   (
   Item            : Pointer
   );
  begin
  end;

Procedure TCache.Lock
   (
   Item            : Integer
   );
  begin
    {$IFDEF DEBUG}
    if (Item>Capacity)
        or (Item<0) then Raise Exception.Create('TCache.Lock: Index out of range');
    if FDirectory^[Item].State and csUsed=0 then
        Raise Exception.Create('TCache.Lock: Unused item specified');
    {$ENDIF}
    Inc(FDirectory^[Item].LockCnt);
  end;

Procedure TCache.UnLock
   (
   Item            : Integer
   );
  begin
    {$IFDEF DEBUG}
    if (Item>Capacity)
        or (Item<0) then Raise Exception.Create('TCache.UnLock: Index out of range');
    if FDirectory^[Item].State and csUsed=0 then
        Raise Exception.Create('TCache.UnLock: Unused item specified');
    {$ENDIF}
    Dec(FDirectory^[Item].LockCnt);
    {$IFDEF DEBUG}
    if FDirectory^[Item].LockCnt<0 then
        Raise Exception.Create('TCache.UnLock: Unlock of a not locked item');
    {$ENDIF};
  end;

Procedure TCache.ChangeSize
   (
   Item            : Integer;
   NewSize         : Longint
   );
  begin
    {$IFDEF DEBUG}
    if (Item>Capacity)
        or (Item<0) then Raise Exception.Create('TCache.UnLock: Index out of range');
    if FDirectory^[Item].State and csUsed=0 then
        Raise Exception.Create('TCache.UnLock: Unused item specified');
    {$ENDIF}
    if NewSize<>FDirectory^[Item].Size then begin
      FCurrentSize:=FCurrentSize-FDirectory^[Item].Size+NewSize;
      FDirectory^[Item].Size:=NewSize;
      Shrink(CacheSize);
    end;
  end;

Function TCache.IsModified
   (
   Item            : Integer
   )
   : Boolean;
  begin
    {$IFDEF DEBUG}
    if (Item>Capacity)
        or (Item<0) then Raise Exception.Create('TCache.IsModified: Index out of range');
    if FDirectory^[Item].State and csUsed=0 then
        Raise Exception.Create('TCache.IsModified: Unused item specified');
    {$ENDIF}
    Result:=FDirectory^[Item].State and (csModified+csDirty)<>0;
  end;

end.
