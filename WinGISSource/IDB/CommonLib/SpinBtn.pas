Unit Spinbtn;

Interface

Uses WinProcs,WinTypes,Classes,Controls,Messages,Forms,StdCtrls,Validate;

Type TUpDownEvent  = Procedure(Sender:TObject;Shift:TShiftState) of Object;
     TGetValueEvent= Procedure(Sender:TObject;var Value:Double) of object;
     TSetValueEvent= Procedure(Sender:TObject;Const Value:Double) of object;

     TSpinBtn      = Class(TCustomControl)
      Private
       FAltIncr    : Double;
       FArrowKeys  : Boolean;
       FClickState : Byte;
       FCombobox   : TCombobox;
       FCtrlIncr   : Double;
       FCurState   : Byte;
       FEdit       : TEdit;
       FIncrement  : Double;
       FMax        : Double;
       FMin        : Double;
       FMultiply   : Boolean;
       FNeedAlign  : Boolean;
       FPosition   : Double;
       FOnDownClick: TUpDownEvent;
       FOnGetValue : TGetValueEvent;
       FOnSetValue : TSetValueEvent;
       FOnUpClick  : TUpDownEvent;
       FShiftIncr  : Double;
       FValidator  : TCustomValidator;
       FWMTimerCnt : Integer;
       Procedure   AlignToEdit;
       Procedure   CMEnabledChanged(var Msg:TMessage); message cm_EnabledChanged;
       Procedure   DownClick(Shift:TShiftState);
       Procedure   DoGetValue(var AValue:Double);
       Procedure   DoSetValue(Const AValue:Double);
       Procedure   DoPaint;
       Function    FieldRect(Field:Integer):TRect;
       Function    GetControl:TControl;
       Procedure   SetAssociate(AEdit:TEdit);
       Procedure   SetCombobox(ACombobox:TCombobox);
       Procedure   SetValidator(AValidator:TCustomValidator);
       Procedure   UpClick(Shift:TShiftState);
       Procedure   UpdateValue(Const Delta:Double);
       Procedure   WMTimer(var Msg:TWMTimer); message wm_Timer;
      Protected 
       Procedure   ChangeScale(M,D:Integer); override;
       Procedure   KeyDown(var Key:Word;Shift:TShiftState); override;
       Procedure   MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
       Procedure   MouseMove(Shift:TShiftState;X,Y:Integer); override;
       Procedure   MouseUp(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
       Procedure   Paint; override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Property    Control:TControl read GetControl; 
       Function    IncrementValue(Shift:TShiftState):Double; virtual;
       Procedure   Notification(AComponent:TComponent;Operation:TOperation); override;
      Published
       Property    Anchors;
       Property    Arrowkeys:Boolean read FArrowKeys write FArrowKeys;
       Property    Associate:TEdit read FEdit write SetAssociate;
       Property    Combobox:TCombobox read FCombobox write SetCombobox;
       Property    Ctl3D;
       Property    CtrlIncrement:Double read FCtrlIncr write FCtrlIncr;
       Property    Enabled;
       Property    Height default 25;
       Property    Hint;
       Property    Increment:Double read FIncrement write FIncrement;
       Procedure   Loaded; override;
       Property    Max:Double read FMax write FMax;
       Property    Min:Double read FMin write FMin;
       Property    Multiply:Boolean read FMultiply write FMultiply default False;
       Property    OnClick;
       Property    OnDownClick:TUpDownEvent read FOnDownClick write FOnDownClick;
       Property    OnEnter;
       Property    OnExit;
       Property    OnGetValue:TGetValueEvent read FOnGetValue write FOnGetValue;
       Property    OnSetValue:TSetValueEvent read FOnSetValue write FOnSetValue;
       Property    OnUpClick:TUpDownEvent read FOnUpClick write FOnUpClick;
       Property    ParentCtl3D;
       Property    ParentShowHint;
       Property    Position:Double read FPosition write FPosition;
       Property    ShiftIncrement:Double read FShiftIncr write FShiftIncr;
       Property    ShowHint;
       Property    TabStop default False;
       Property    Validator:TCustomValidator read FValidator write SetValidator;
       Property    Visible;
       Property    Width default 15;
     end;

Implementation

{$IFDEF WIN32}
{$R *.R32}
{$ELSE}
{$R *.RES}
{$ENDIF}

Uses ExtCtrls,Graphics,NumTools,SysUtils,WCtrls;

var FUpDownBitmap  : TBitmap;
    FCursor        : TCursor;
    FLastPos       : TPoint;

Const InitRepeatTime    = 450;
      RepeatTime        = 60;
      FastRepeatTime    = 5;

Constructor TSpinBtn.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    ControlStyle:=ControlStyle+[csOpaque];
    TabStop:=False;
    Height:=25;
    Width:=15;
    if not Assigned(AOwner)
        or not (csLoading in Owner.ComponentState) then begin
      FMax:=1E301;
      FMin:=-1E301;
    end;  
    Increment:=1;
  end;

Procedure TSpinBtn.DoPaint;
  var ARect        : TRect;
  Procedure DrawBitmap
     (
     Dest          : TRect;
     Source        : TRect
     );
    var CRect      : TRect;
    begin
      with Canvas do begin
        CRect.Left:=Dest.Left+(Dest.Right-Dest.Left-5) Div 2;
        CRect.Top:=Dest.Top+(Dest.Bottom-Dest.Top-3) Div 2;
        CRect.Right:=CRect.Left+5;
        CRect.Bottom:=CRect.Top+3;
        if FCurState and 1<>0 then OffsetRect(CRect,1,1);
        Brush.Color:=clBtnFace;
        if Enabled then Font.Color:=clBlack
        else Font.Color:=clBtnShadow;
        CopyRect(CRect,FUpDownBitmap.Canvas,Source);
      end;
    end;
  begin
    with Canvas do begin
      ARect:=FieldRect(0);
      if FCurState and 1<>0 then begin
        Frame3D(Canvas,ARect,clBlack,clBtnHighlight,1);
        Frame3D(Canvas,ARect,clBtnShadow,clBtnFace,1);
      end
      else begin
        Frame3D(Canvas,ARect,clBtnHighlight,clBlack,1);
        Frame3D(Canvas,ARect,clBtnFace,clBtnShadow,1);
      end;
      Brush.Color:=clBtnFace;
      FillRect(ARect);
      DrawBitmap(ARect,Rect(0,0,5,3));
      ARect:=FieldRect(1);
      if FCurState and 2<>0 then begin
        Frame3D(Canvas,ARect,clBlack,clBtnHighlight,1);
        Frame3D(Canvas,ARect,clBtnShadow,clBtnFace,1);
      end
      else begin
        Frame3D(Canvas,ARect,clBtnHighlight,clBlack,1);
        Frame3D(Canvas,ARect,clBtnFace,clBtnShadow,1);
      end;
      FillRect(ARect);
      DrawBitmap(ARect,Rect(5,0,10,3));
      ARect:=Rect(-1,0,Width,Height);
      Frame3D(Canvas,ARect,clBtnShadow,clBtnHighlight,1);
      ARect:=FieldRect(2);
      Brush.Color:=clBtnShadow;
      FillRect(ARect);
      Pen.Color:=clBlack;
      MoveTo(0,1);
      LineTo(Width-2,1);
      MoveTo(Width-3,9);
      LineTo(Width-3,Height-9);
    end;
  end;

Procedure TSpinBtn.Paint;
  begin
    if FNeedAlign then begin
      AlignToEdit;
      FNeedAlign:=FALSE;
    end;  
    DoPaint;
  end;

Procedure TSpinBtn.KeyDown
   (
   var Key         : Word;
   Shift           : TShiftState
   );
  begin
  end;

Procedure TSpinBtn.MouseDown
   (
   Button          : TMouseButton;
   Shift           : TShiftState;
   X               : Integer;
   Y               : Integer
   );
  var NewState     : Byte;
      ARect        : TRect;
      Control      : TControl;
  begin
    Control:=GetControl;
    if Assigned(Control) and Control.Enabled then begin
      NewState:=0;
      ARect:=FieldRect(0);
      if ptInRect(ARect,Point(X,Y)) then begin
        NewState:=1;
        UpClick(Shift);
      end;
      ARect:=FieldRect(1);
      if ptInRect(ARect,Point(X,Y)) then begin
        NewState:=2;
        DownClick(Shift);
      end;
      ARect:=FieldRect(2);
      if ptInRect(ARect,Point(X,Y)) then NewState:=4;
      if NewState<>FClickState then begin
        FClickState:=NewState;
        if FClickState=4 then FCurState:=3
        else FCurState:=NewState;
        DoPaint;
        if (FClickState<>4)
            and (FCurState and 3<>0) then begin
          FWMTimerCnt:=0;  
          SetTimer(Handle,1,InitRepeatTime,NIL);
        end;  
      end;
    end;  
  end;

Procedure TSpinBtn.MouseUp
   (
   Button          : TMouseButton;
   Shift           : TShiftState;
   X               : Integer;
   Y               : Integer
   );
  var ARect        : TRect;
      Control      : TControl;
  begin
    Control:=GetControl;
    if Assigned(Control) and Control.Enabled then begin
      FClickState:=0;
      if FCurState<>0 then begin
        FCurState:=0;
        DoPaint;
      end;
      KillTimer(Handle,1);
      ARect:=FieldRect(2);
      if not ptInRect(ARect,Point(X,Y)) then Cursor:=crDefault;
      Click;
    end;
  end;

Procedure TSpinBtn.MouseMove
   (
   Shift           : TShiftState;
   X               : Integer;
   Y               : Integer
   );
  var ARect        : TRect;
      NewState     : Byte;
      Control      : TControl;
  begin
    Control:=GetControl;
    if Assigned(Control) and Control.Enabled then begin
      NewState:=FCurState;
      if FClickState=4 then begin
        if FLastPos.Y<>Y then begin
          if FLastPos.Y<Y then DownClick(Shift)
          else UpClick(Shift);
        end;
      end
      else if FClickState=1 then begin
        ARect:=FieldRect(0);
        if ptInRect(ARect,Point(X,Y)) then NewState:=1
        else NewState:=0;
      end
      else if FClickState=2 then begin
        ARect:=FieldRect(1);
        if ptInRect(ARect,Point(X,Y)) then NewState:=2
        else NewState:=0;
      end
      else if FClickState=0 then begin
        ARect:=FieldRect(2);
        if ptInRect(ARect,Point(X,Y)) then Cursor:=FCursor
        else Cursor:=crDefault;
      end;
      if NewState<>FCurState then begin
        FCurState:=NewState;
        DoPaint;
        if (FClickState<>4)
            and (FCurState and 3<>0) then begin
          FWMTimerCnt:=0;
          SetTimer(Handle,1,RepeatTime,NIL);
        end  
        else KillTimer(Handle,1);
      end;
      FLastPos:=Point(X,Y);
    end;  
  end;

Procedure TSpinBtn.SetAssociate
   (
   AEdit           : TEdit
   );
  begin
    FEdit:=AEdit;
    if FEdit<>NIL then begin
      FCombobox:=NIL;
      FValidator:=NIL;
    end;  
    AlignToEdit;
  end;

Procedure TSpinBtn.SetCombobox
   (
   ACombobox       : TCombobox
   );
  begin
    FCombobox:=ACombobox;
    if FCombobox<>NIL then begin
      FEdit:=NIL;
      FValidator:=NIL;
    end;  
    AlignToEdit;
  end;

Procedure TSpinBtn.SetValidator
   (
   AValidator      : TCustomValidator
   );
  begin
    FValidator:=AValidator;
    if FValidator<>NIL then begin
      FEdit:=NIL;
      FCombobox:=NIL;
    end;
    AlignToEdit;
  end;

Function TSpinBtn.GetControl
   : TControl;
  begin
    Result:=FEdit;
    if Result=NIL then Result:=FCombobox;
    if (Result=NIL) and (FValidator<>NIL) then Result:=FValidator.Edit;
    if (Result=NIL) and (FValidator<>NIL) then Result:=FValidator.Combobox;
  end;

Procedure TSpinBtn.AlignToEdit;
  var Control      : TControl;
  begin
    Control:=GetControl;
    if Control<>NIL then begin
      {$IFDEF WIN32}
      Top:=Control.Top;       
      Height:=Control.Height;
      Left:=Control.Left+Control.Width+1;
      {$ELSE}
      Top:=Control.Top-1;
      Height:=Control.Height+2;
      Left:=Control.Left+Control.Width+2;
      {$ENDIF}
    end;
  end;

Procedure TSpinBtn.Loaded;
  begin
    inherited Loaded;
    AlignToEdit;
  end;

Function TSpinBtn.FieldRect
   (
   Field           : Integer
   )
   : TRect;
  begin
    case Field of
      0: Result:=Rect(0,2,Width-2,9);
      1: Result:=Rect(0,Height-9,Width-2,Height-2);
      else Result:=Rect(0,9,Width-2,Height-9);
    end;
  end;

Procedure TSpinBtn.WMTimer
   (
   var Msg         : TWMTimer
   );
  var Shift        : TShiftState;
  begin
    if FWMTimerCnt>5 then SetTimer(Handle,1,FastRepeatTime,NIL)
    else SetTimer(Handle,1,RepeatTime,NIL);
    Inc(FWMTimerCnt);
    Shift:=[];
    if GetKeyState(VK_SHIFT)<0 then Include(Shift,ssShift);
    if GetKeyState(VK_CONTROL)<0 then Include(Shift,ssCtrl);
    if FClickState and 1<>0 then UpClick(Shift)
    else if FClickState and 2<>0 then DownClick(Shift);
  end;

Procedure TSpinBtn.UpClick
   (
   Shift           : TShiftState
   );
  begin
    if Assigned(FOnUpClick) then OnUpClick(Self,Shift)
    else UpdateValue(IncrementValue(Shift));
  end;

Procedure TSpinBtn.DownClick
   (
   Shift           : TShiftState
   );
  begin
    if Assigned(FOnDownClick) then OnDownClick(Self,Shift)
    else UpdateValue(-IncrementValue(Shift));
  end;

Procedure TSpinBtn.UpdateValue
   (
   Const Delta     : Double
   );
  var Value        : Double;
      NewValue     : Double;
  begin
    DoGetValue(Value);
    if not FMultiply then NewValue:=Value+Delta
    else if Abs(Delta)>1E-10 then begin
      if Delta>0 then NewValue:=Value*Delta
      else NewValue:=Value/Abs(Delta);
    end
    else NewValue:=Value;
    if NewValue<FMin then NewValue:=FMin;
    if NewValue>FMax then NewValue:=FMax;
    if Abs(NewValue-Value)>1E-10 then DoSetValue(NewValue);
  end;

Procedure TSpinBtn.DoGetValue
   (
   var AValue      : Double
   );
  begin
    try
      if Assigned(FOnGetValue) then OnGetValue(Self,AValue)
      else if FEdit<>NIL then AValue:=StrToFloat(FEdit.Text)
      else if FCombobox<>NIL then AValue:=StrToFloat(FCombobox.Text)
      else if FValidator<>NIL then AValue:=FValidator.AsFloat
      else AValue:=Position;
    except
      AValue:=Position;
    end;    
  end;

Procedure TSpinBtn.DoSetValue
   (
   Const AValue    : Double
   );
  begin
    if Assigned(FOnSetValue) then OnSetValue(Self,AValue)
    else if FEdit<>NIL then FEdit.Text:=FloatToStr(AValue)
    else if FCombobox<>NIL then FCombobox.Text:=FloatToStr(AValue)
    else if FValidator<>NIL then FValidator.AsFloat:=AValue;
  end;

Function TSpinBtn.IncrementValue
   (
   Shift           : TShiftState
   )
   : Double;
  begin
    if ssShift in Shift then Result:=FShiftIncr
    else if ssCtrl in Shift then Result:=FCtrlIncr
    else if ssAlt in Shift then Result:=FAltIncr
    else Result:=FIncrement;
    if Result=0 then Result:=FIncrement;
  end;

Procedure TSpinBtn.CMEnabledChanged
   (
   var Msg         : TMessage
   );
  begin
    inherited;
    Invalidate;
  end;

Procedure TSpinBtn.ChangeScale(M,D:Integer);
  begin
    inherited ChangeScale(M,D);
    FNeedAlign:=TRUE;
  end;

{$IFNDEF WIN32}
Procedure ExitUnit; Far; 
  begin
    FUpDownBitmap.Free;
  end;
{$ENDIF}  

procedure TSpinBtn.Notification(AComponent:TComponent;Operation:TOperation);
begin
  inherited Notification(AComponent,Operation);
  if Operation=opRemove then begin
    if AComponent=FEdit then FEdit:=NIL
    else if AComponent=FCombobox then FCombobox:=NIL
    else if AComponent=FValidator then FValidator:=NIL;
  end;
end;

Initialization
  begin
    FUpDownBitmap:=TBitmap.Create;
    FUpDownBitmap.Handle:=LoadBitmap(hInstance,'UPDOWN');
    FCursor:=AddCursor(LoadCursor(hInstance,'UPDOWNCURSOR'));
    {$IFNDEF WIN32}
    AddExitProc(ExitUnit);
    {$ENDIF}
  end;

{$IFDEF WIN32}
Finalization
  begin
    FUpDownBitmap.Free;
  end;
{$ENDIF}
  
end.
