{**********************************************}
{   TTriSurfaceSeries Editor Dialog            }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeTriSurfEdit;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, ExtCtrls, Chart, TeeSurfa, TeeTriSurface,
  TeCanvas, TeePenDlg;

type
  TTriSurfaceSeriesEditor = class(TForm)
    Button2: TButtonPen;
    Button3: TButton;
    Button1: TButtonPen;
    procedure FormShow(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
    Surface : TTriSurfaceSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeeBrushDlg, TeeProcs, TeeGriEd;

procedure TTriSurfaceSeriesEditor.FormShow(Sender: TObject);
begin
  Surface:=TTriSurfaceSeries(Tag);
  Button2.LinkPen(Surface.Pen);
  Button1.LinkPen(Surface.Border);
  TeeInsertGrid3DForm(Parent,Surface);
end;

procedure TTriSurfaceSeriesEditor.Button3Click(Sender: TObject);
begin
  EditChartBrush(Self,Surface.Brush);
end;

initialization
  RegisterClass(TTriSurfaceSeriesEditor);
end.
