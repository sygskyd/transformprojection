{********************************************}
{     TeeChart Pro Charting Library          }
{ Copyright (c) 1995-2000 by David Berneda   }
{         All Rights Reserved                }
{********************************************}
{$I TeeDefs.inc}
unit TeeSourceEdit;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls,
     {$ENDIF}
     TeEngine;

type
  TBaseSourceEditor = class(TForm)
    LLabel: TLabel;
    CBSources: TComboBox;
    BApply: TButton;
    Pan: TPanel;
    procedure CBSourcesChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  protected
    TheSeries : TChartSeries;
    Procedure AddComponentDataSource( Const AComponent:TComponent;
                                      AItems:TStrings;
                                      AddCurrent:Boolean);
    Function IsValid(AComponent:TComponent):Boolean; virtual;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeeConst;

Procedure TBaseSourceEditor.AddComponentDataSource( Const AComponent:TComponent;
                                                 AItems:TStrings;
                                                 AddCurrent:Boolean);
Var tmp         : String;
    tmpFormName : TComponentName;
begin
  if AddCurrent or (TheSeries.DataSources.IndexOf(AComponent)=-1) then
  if IsValid(AComponent) then
     if TheSeries.ParentChart.IsValidDataSource(TheSeries,AComponent) then
     begin
       if AComponent is TChartSeries then
          tmp:=SeriesTitleOrName(TChartSeries(AComponent))
       else
       begin
         tmp:=AComponent.Name;
         {$IFDEF TEEOCX}
         if Pos('ODBC',tmp)=1 then tmp:=PString(Pointer(AComponent.Tag))^;
         {$ENDIF}
       end;
       if (TheSeries.Owner<>AComponent.Owner) and
          Assigned(AComponent.Owner) then
       begin
         tmpFormName:=AComponent.Owner.Name;
         if tmpFormName<>'' then tmp:=tmpFormName+'.'+tmp;
       end;
       AItems.AddObject(tmp,AComponent);
     end;
end;

procedure TBaseSourceEditor.CBSourcesChange(Sender: TObject);
begin
  BApply.Enabled:=True;
end;

procedure TBaseSourceEditor.FormShow(Sender: TObject);
begin
  TheSeries:=TChartSeries(Tag);
  BApply.Enabled:=False;
end;

type TButtonAccess=class(TButton);

procedure TBaseSourceEditor.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  inherited;
  if BApply.Enabled then
  Case MessageDlg(TeeMsg_SureToApply,{$IFNDEF CLX}mtConfirmation{$ELSE}mtWarning{$ENDIF},[mbYes,mbNo,mbCancel],0) of
    mrYes: begin
             TButtonAccess(BApply).Click;
             CanClose:=not BApply.Enabled;
           end;
    mrNo: begin
            BApply.Enabled:=False;
            CanClose:=True;
          end;
    mrCancel: CanClose:=False;
  end;
end;

function TBaseSourceEditor.IsValid(AComponent: TComponent): Boolean;
begin
  result:=False;
end;

end.
