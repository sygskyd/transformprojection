{********************************************}
{   TeeChart Pro 4.0 TVolumeSeries Editor    }
{   Copyright (c) 1998-2000 by David Berneda }
{********************************************}
{$I teedefs.inc}
unit TeeVolEd;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, ExtCtrls, Chart, TeEngine, CandleCh, TeCanvas,
  TeePenDlg, ComCtrls;

type
  TVolumeSeriesEditor = class(TPenDialog)
    CBColorEach: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure CBColorEachClick(Sender: TObject);
    procedure BColorClick(Sender: TObject);
  private
    { Private declarations }
    TheSeries : TVolumeSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeeProcs;

procedure TVolumeSeriesEditor.FormShow(Sender: TObject);
begin
  TheSeries:=TVolumeSeries(Tag);
  ThePen:=TheSeries.Pen;
  inherited;
  BColor.Enabled:=not TheSeries.ColorEachPoint;
  CBColorEach.Checked:=TheSeries.ColorEachPoint;
end;

procedure TVolumeSeriesEditor.CBColorEachClick(Sender: TObject);
begin
  TheSeries.ColorEachPoint:=CBColorEach.Checked;
  BColor.Enabled:=not TheSeries.ColorEachPoint;
end;

procedure TVolumeSeriesEditor.BColorClick(Sender: TObject);
begin
  inherited;
  TheSeries.SeriesColor:=ThePen.Color;
end;

initialization
  RegisterClass(TVolumeSeriesEditor);
end.
