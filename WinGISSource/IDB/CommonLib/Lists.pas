Unit Lists;

Interface

Uses Classes,SysUtils;

Type PByteArray         = ^TByteArray;
     TByteArray         = Array[0..0] of Byte;

     PIntArray          = ^TIntArray;
     TIntArray          = Array[0..0] of Integer;

     TBitList     = Class(TPersistent)
      Private
       FSize       : Integer;
       FData       : PByteArray;
       Function    ByteSize(BitSize:Integer):Integer;
       Function    GetBit(ABit:Integer):Boolean;
       Procedure   SetBit(ABit:Integer;ASet:Boolean);
       Procedure   SetSize(ASize:Integer);
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Constructor Create(ASize:Integer);
       Destructor  Destroy; override;
       Procedure   AndOperator(Bits:TBitList);
       Property    Bits[AIndex:Integer]:Boolean read GetBit write SetBit; default;
       Property    Capacity:Integer read FSize write SetSize;
       Procedure   ClearAll;
       Function    FirstCleared:Integer;
       Function    FirstSet:Integer;
       Property    Data:PByteArray read FData;
       Function    DataSize:Integer;
       Procedure   OrOperator(Bits:TBitList);
       Procedure   SetAll;
     end;

     TIntList      = Class(TPersistent)
      Private
       FCapacity        : Integer;
       FCapacityDelta   : Integer;
       FCount           : Integer;
       FItems         : PIntArray;
       Procedure   SetCapacity(ACapacity:Integer);
       Function    GetItem(AIndex:Integer):Integer;
       Procedure   SetItem(AIndex:Integer;Const Item:Integer);
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Procedure   Add(Const Item:Integer);
       Property    Capacity:Integer read FCapacity write SetCapacity;
       Property    CapacityDelta:Integer read FCapacityDelta write FCapacityDelta default 8;
       Procedure   Clear;
       Property    Count:Integer read FCount;
       Procedure   Delete(AIndex:Integer);
       Procedure   Insert(AIndex:Integer;AItem:Integer);
       Property    Items[AIndex:Integer]:Integer read GetItem write SetItem; default;
       Property    List:PIntArray read FItems;
     end;

     {*************************************************************************+
       Class TRecList
     ---------------------------------------------------------------------------
       TRecList ist eine dynamische Liste f�r Records. Im Gegensatz zu TList
       werden nicht Pointer sondern die Records selbst in einem Array
       gespeichert. Die Gr��e der Records mu� im Constructor �bergeben
       werden. Ansonsten sind die gleichen Funktionen wie f�r TList definiert.
       Lediglich die Parametertypen unterscheiden sich etwas. Das Array f�r
       die Records kann maximal 64 kBytes gro� werden.
     ---------------------------------------------------------------------------
       FItems           = Zeiger auf das Array mit den Records
       FCount           = aktuelle Anzahl von Eintr�gen
       FCapacity        = aktuelle Gr��e des Arrays in Anzahl von Eintr�gen
       FItemSize        = Gr��e eines Records in Bytes
       FUsed            = Information about used items
       FUsedPtr         = Pointer cycling through list to mark free position
     +*************************************************************************}
     TRecList           = Class(TPersistent)
      Private
       FItems           : PByteArray;
       FCount           : Integer;
       FCapacity        : Integer;
       FItemSize        : Integer;
       FUsed            : TBitList;
       FUsedPtr         : Integer;
       Function    GetItem(AIndex:Integer):Pointer;
       Function    GetItems:Pointer;
       Procedure   SetItem(AIndex:Integer;Item:Pointer);
       Procedure   SetCapacity(ACapacity:Integer);
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Constructor Create(ACapacity:Integer;AItemSize:Integer);
       Destructor  Destroy; override;
       Function    Add(var Item):Integer;
       Function    At(Item:Pointer):Integer;
       Function    Available(var Index:Integer):Boolean;
       Property    Capacity:Integer read FCapacity write SetCapacity;
       Procedure   Clear;
       Property    Count:Integer read FCount;
       Procedure   Delete(Index:Integer);
       Procedure   Expand;
       Function    First:Pointer;
       Function    FirstItem:Pointer;
       Property    Items[AIndex:Integer]:Pointer read GetItem write SetItem; default;
       Function    Last:Pointer;
       Property    List:Pointer read GetItems;
       Property    ItemSize:Integer read FItemSize;
     end;
 
     TPackedRecList     = Class(TPersistent)
      Private
       FCapacity        : Integer;
       FCapacityDelta   : Integer;
       FCount           : Integer;
       FItems           : PByteArray;
       FItemSize        : Integer;
       Procedure   SetCapacity(ACapacity:Integer);
       Function    GetItem(AIndex:Integer):Pointer;
       Procedure   SetItem(AIndex:Integer;Const Item:Pointer);
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Constructor Create(AItemSize:Integer);
       Destructor  Destroy; override;
       Procedure   Add(Const Item);
       Property    Capacity:Integer read FCapacity write SetCapacity;
       Property    CapacityDelta:Integer read FCapacityDelta write FCapacityDelta default 8;
       Procedure   Clear;
       Property    Count:Integer read FCount;
       Procedure   Delete(AIndex:Integer);
       Procedure   Insert(AIndex:Integer;const AItem);
       Property    Items[AIndex:Integer]:Pointer read GetItem write SetItem; default;
       Property    ItemSize:Integer read FItemSize;
       Property    List:PByteArray read FItems;
     end;

Implementation

Uses NumTools;

{$IFNDEF WIN32}
Type  LongPtr       = Record
       case Word of
         0 : (Ptr   : Pointer);
         1 : (Long  : Integer);
         2 : (Lo,Hi : Word);
     end;
{$ENDIF}

{===============================================================================
  TRecList
+==============================================================================}

Constructor TRecList.Create(ACapacity:Integer;AItemSize:Integer);
begin
  inherited Create;
  FUsed:=TBitList.Create(0);
  FItemSize:=AItemSize;
  Capacity:=ACapacity;
end;

Destructor TRecList.Destroy;
begin
  if FCapacity>0 then FreeMem(FItems,Capacity*FItemSize);
  FUsed.Free;
  inherited Destroy;
end;

Function TRecList.Add(var Item):Integer;
begin
  if Count=Capacity then Expand;
  while FUsed[FUsedPtr] do FUsedPtr:=(FUsedPtr+1) Mod Capacity;
  Move(Item,FItems^[FUsedPtr*FItemSize],FItemSize);
  FUsed[FUsedPtr]:=TRUE;
  Inc(FCount);
  Result:=FUsedPtr;
  FUsedPtr:=(FUsedPtr+1) Mod Capacity;
end;

Function TRecList.At(Item:Pointer):Integer;
begin
  Result:=((Integer(Item)-Integer(FItems)) Div FItemSize);
end;

Procedure TRecList.Clear;
begin
  FCount:=0;
end;

Procedure TRecList.SetCapacity(ACapacity:Integer);
var HelpPtr      : PByteArray;
begin
  if ACapacity>FCapacity then begin
    if FCapacity>0 then begin
      GetMem(HelpPtr,ACapacity*FItemSize);
      Move(FItems^[0],HelpPtr^[0],FCapacity*FItemSize);
      FreeMem(FItems,FCapacity*FItemSize);
      FItems:=HelpPtr;
    end
    else GetMem(FItems,ACapacity*FItemSize);
    FUsed.Capacity:=ACapacity;
    FCapacity:=ACapacity;
  end;
end;

Procedure TRecList.Delete(Index:Integer);
begin
  {$IFDEF DEBUG}
  if Index>Capacity then Raise EListError.Create('TRecList.Delete: Listindex out of range');
  if not FUsed[Index] then Raise EListError.Create('TRecList.Delete: Access to a free listitem');
  {$ENDIF}
  FUsed[Index]:=FALSE;
  Dec(FCount);
end;

Procedure TRecList.Expand;
begin
  if Capacity<4 then Capacity:=Capacity+4
  else if Capacity<9 then Capacity:=Capacity+8
  else Capacity:=Capacity+16;
end;

Function TRecList.First:Pointer;
begin
  if Count>0 then Result:=FItems
  else Result:=NIL;
end;

Function TRecList.Last:Pointer;
begin
  if Count>0 then Result:=@FItems^[(Count-1)*FItemSize]
  else Result:=NIL;
end;

Function TRecList.GetItem(AIndex:Integer):Pointer;
begin
  {$IFDEF DEBUG}
  if AIndex>Capacity then Raise EListError.Create('Listindex out of range');
  {$ENDIF}
  if not FUsed[AIndex] then Result:=NIL
  else Result:=@FItems^[AIndex*FItemSize];
end;

Procedure TRecList.SetItem(AIndex:Integer;Item:Pointer);
begin
  {$IFDEF DEBUG}
  if AIndex>Capacity then Raise EListError.Create('Listindex out of range');
  if not FUsed[AIndex] then Raise EListError.Create('Access to a free listitem');
  {$ENDIF}
  System.Move(Item^,FItems^[AIndex*FItemSize],FItemSize);
end;

Function TRecList.GetItems:Pointer;
begin
  Result:=FItems;
end;

Function TRecList.Available(var Index:Integer):Boolean;
begin
  Result:=False;
  while Index < Capacity do begin
    if Items[Index]<>NIL then begin
      Result:=TRUE;
      break;
    end;
    Inc(Index);
  end;
end;

Function TRecList.FirstItem:Pointer;
var Index        : Integer;
begin
  Result:=NIL;
  for Index:=0 to FCapacity-1 do if Items[Index]<>NIL then begin
    Result:=@FItems^[Index*FItemSize];
    break;
  end;
end;

Procedure TRecList.AssignTo(Dest:TPersistent);
begin
  if not (Dest is TRecList) then inherited AssignTo(Dest)
  else with Dest as TRecList do begin
    if FCapacity>0 then begin
      FreeMem(FItems,FCapacity*FItemSize);
      FCapacity:=0;
    end;
    FCount:=0;
    FItemSize:=Self.FItemSize;
    FUsedPtr:=Self.FUsedPtr;
    SetCapacity(Self.FCapacity);
    FCount:=Self.FCount;
    FUsed.Assign(Self.FUsed);
    if FCount>0 then Move(Self.FItems^,FItems^,FCapacity*FItemSize);
  end;
end;

{==============================================================================+
  TPackedRecList
+==============================================================================}

Constructor TPackedRecList.Create(AItemSize:Integer);
begin
  inherited Create;
  FItemSize:=AItemSize;
  FCapacityDelta:=8;
end;

Destructor TPackedRecList.Destroy;
begin
  if FCapacity>0 then FreeMem(FItems,FCapacity*FItemSize);
  FCount:=0;
  FCapacity:=0;
end;

Procedure TPackedRecList.Clear;
begin
  FCount:=0;
end;

Function TPackedRecList.GetItem(AIndex:Integer):Pointer;
begin
  Result:=@FItems^[AIndex*FItemSize];
end;

Procedure TPackedRecList.SetItem(AIndex:Integer;Const Item:Pointer);
begin
  Move(Item^,FItems^[AIndex*FItemSize],FItemSize);
end;
                                                       
Procedure TPackedRecList.SetCapacity(ACapacity:Integer);
var FTempPtr     : PByteArray; 
begin
  if FCapacity<FCount then FCapacity:=FCount;
  if ACapacity>FCapacity then begin
    if FCapacity=0 then GetMem(FItems,ACapacity*FItemSize)
    else begin
      GetMem(FTempPtr,ACapacity*FItemSize);
      Move(FItems^,FTempPtr^,FCapacity*FItemSize);
      FreeMem(FItems,FCapacity*FItemSize);
      FItems:=FTempPtr;
    end;
    FCapacity:=ACapacity;
  end;
end;

Procedure TPackedRecList.Add(Const Item);
begin
  if FCount=FCapacity then Capacity:=Capacity+FCapacityDelta;
  Move(Item,FItems^[FCount*FItemSize],FItemSize);
  Inc(FCount);
end;

Procedure TPackedRecList.Delete(AIndex:Integer);
begin
  {$IFDEF DEBUG}
  if AIndex>=FCount then Raise EListError.Create('TPackedRecList.Delete: Listindex out of range');
  {$ENDIF}
  System.Move(FItems^[(AIndex+1)*FItemSize],FItems^[AIndex*FItemSize],
      (FCount-AIndex-1)*FItemSize);
  Dec(FCount);
  if FCapacity-FCount>FCapacityDelta then Capacity:=Capacity-FCapacityDelta;
end;

Procedure TPackedRecList.Insert(AIndex:Integer;const AItem);
begin
  {$IFDEF DEBUG}
  if AIndex>FCount then Raise EListError.Create('TPackedRecList.Insert: Listindex out of range');
  {$ENDIF}
  if FCount=FCapacity then Capacity:=Capacity+FCapacityDelta;
  System.Move(FItems^[AIndex*FItemSize],FItems^[(AIndex+1)*FItemSize],
      (FCount-AIndex)*FItemSize);
  System.Move(AItem,FItems[AIndex*FItemSize],FItemSize);
  Inc(FCount);
end;

Procedure TPackedRecList.AssignTo(Dest:TPersistent);
begin
  if not (Dest is TPackedRecList) then inherited AssignTo(Dest)
  else with Dest as TPackedRecList do begin
    if FCapacity>0 then FreeMem(FItems,FCapacity*FItemSize);
    FCapacity:=0;
    FCount:=0;
    FCapacityDelta:=Self.FCapacityDelta;
    Capacity:=Self.Capacity;
    FCount:=Self.FCount;
    Move(Self.FItems^,FItems^,FCount*SizeOf(Integer));
  end;
end;

{==============================================================================+
  TBitList
+==============================================================================}

Const BitMasks     : Array[0..7] of Byte = ($80,$40,$20,$10,$8,$4,$2,$1);

Constructor TBitList.Create(ASize:Integer);
begin
  inherited Create;
  Capacity:=ASize;
end;

Destructor TBitList.Destroy;
begin
  FreeMem(FData,ByteSize(FSize));
  inherited Destroy;
end;

Procedure TBitList.SetSize(ASize:Integer);
var Help         : PByteArray;
begin
  if ByteSize(FSize)<>ByteSize(ASize) then begin
    GetMem(Help,ByteSize(ASize));
    FillChar(Help^,ByteSize(ASize),#0);
    if FData<>NIL then begin
      Move(FData^,Help^,ByteSize(FSize));
      FreeMem(FData,ByteSize(FSize));
    end;
    FData:=Help;
  end;
  FSize:=ASize;
end;

Procedure TBitList.SetBit(ABit:Integer;ASet:Boolean);
var Offset       : Integer;
begin
  if ABit>=FSize then
     Raise Exception.Create('TBitList.SetBit: Bitnumber out of Range');
  Offset:=ABit Div 8;
  if ASet then FData^[Offset]:=FData^[Offset] or BitMasks[ABit Mod 8]
  else FData^[Offset]:=FData^[Offset] and not BitMasks[ABit Mod 8];
end;

Function TBitList.GetBit(ABit:Integer):Boolean;
var Offset       : Integer;
begin
  if ABit>=FSize then
     Raise Exception.Create('TBitList.GetBit: Bitnumber out of Range');
  Offset:=ABit Div 8;
  Result:=FData^[Offset] and BitMasks[ABit Mod 8]<>0;
end;

Procedure TBitList.ClearAll;
begin
  FillChar(FData^,ByteSize(FSize),#0);
end;

Procedure TBitList.SetAll;
begin
  FillChar(FData^,ByteSize(FSize),#$FF);
end;

Function TBitList.DataSize:Integer;
begin
  Result:=ByteSize(FSize);
end;

Function TBitList.ByteSize(BitSize:Integer):Integer;
begin
  Result:=BitSize Div 8;
  if BitSize Mod 8<>0 then Inc(Result);
end;

Procedure TBitList.AssignTo(Dest:TPersistent);
begin
  if not (Dest is TBitList) then inherited AssignTo(Dest)
  else begin
    TBitList(Dest).Capacity:=FSize;
    Move(Data^,TBitList(Dest).Data^,DataSize);
  end;
end;

Procedure TBitList.AndOperator(Bits:TBitList);
var DataCnt      : Integer;
    Cnt          : Integer;
begin
  DataCnt:=Min(Bits.DataSize,DataSize);
  for Cnt:=0 to DataCnt-1 do FData^[Cnt]:=FData^[Cnt] and Bits.FData^[Cnt];
  for Cnt:=DataCnt to DataSize-1 do FData^[Cnt]:=0;
end;

Procedure TBitList.OrOperator(Bits:TBitList);
var DataCnt      : Integer;
    Cnt          : Integer;
begin
  if Bits.DataSize<DataSize then DataCnt:=Bits.DataSize
  else DataCnt:=DataSize;
  for Cnt:=0 to DataCnt-1 do FData^[Cnt]:=FData^[Cnt] or Bits.FData^[Cnt];
end;

Function TBitList.FirstCleared:Integer;
var Cnt          : Integer;
    Mask         : Byte;
begin
  for Cnt:=0 to DataSize-1 do begin
    if FData^[Cnt]<>$FF then begin
      Mask:=$80;
      Result:=Cnt*8;
      while FData^[Cnt] and Mask<>0 do begin
        Inc(Result);
        Mask:=Mask Shr 1;
      end;
      if Result>=FSize then Result:=-1;
      Exit;
    end;
  end;
  Result:=-1;
end;

Function TBitList.FirstSet:Integer;
var Cnt          : Integer;
    Mask         : Byte;
begin
  for Cnt:=0 to DataSize-1 do begin
    if FData^[Cnt]<>0 then begin
      Mask:=$80;
      Result:=Cnt*8;
      while FData^[Cnt] and Mask=0 do begin
        Inc(Result);
        Mask:=Mask Shr 1;
      end;
      Exit;
    end;
  end;
  Result:=-1;
end;

{==============================================================================+
  TIntList
+==============================================================================}

Constructor TIntList.Create;
begin
  inherited Create;
  FCapacityDelta:=8;
end;  

Destructor TIntList.Destroy;
begin
  if FCapacity>0 then FreeMem(FItems,FCapacity*SizeOf(Integer));
  FCount:=0;
  FCapacity:=0;
end;

Procedure TIntList.Clear;
begin  
  FCount:=0;
end;

Function TIntList.GetItem(AIndex:Integer):Integer;
begin
  Result:=FItems^[AIndex];
end;

Procedure TIntList.SetItem(AIndex:Integer;Const Item:Integer);
begin
  Move(Item,FItems^[AIndex],SizeOf(Integer));
end;

Procedure TIntList.SetCapacity(ACapacity:Integer);
var FTempPtr     : PIntArray;
begin
  if FCapacity<FCount then FCapacity:=FCount;
  if ACapacity>FCapacity then begin
    if FCapacity=0 then GetMem(FItems,ACapacity*SizeOf(Integer))
    else begin
      GetMem(FTempPtr,ACapacity*SizeOf(Integer));
      Move(FItems^,FTempPtr^,FCapacity*SizeOf(Integer));
      FreeMem(FItems,FCapacity*SizeOf(Integer));
      FItems:=FTempPtr;
    end;
    FCapacity:=ACapacity;
  end;
end;

Procedure TIntList.Add(Const Item:Integer);
begin
  if FCount=FCapacity then Capacity:=Capacity+FCapacityDelta;
  FItems^[FCount]:=Item;
  Inc(FCount);
end;

Procedure TIntList.Delete(AIndex:Integer);
begin
  {$IFDEF DEBUG}
  if AIndex>=FCount then Raise EListError.Create('TIntList.Delete: Listindex out of range');
  {$ENDIF}
  System.Move(FItems^[AIndex+1],FItems^[AIndex],(FCount-AIndex-1)*SizeOf(Integer));
  Dec(FCount);
  if FCapacity-FCount>FCapacityDelta then Capacity:=Capacity-FCapacityDelta;
end;

Procedure TIntList.Insert(AIndex:Integer;AItem:Integer);
begin
  {$IFDEF DEBUG}
  if AIndex>FCount then Raise EListError.Create('TIntList.Insert: Listindex out of range');
  {$ENDIF}
  if FCount=FCapacity then Capacity:=Capacity+FCapacityDelta;
  System.Move(FItems^[AIndex],FItems^[AIndex+1],(FCount-AIndex)*SizeOf(Integer));
  FItems^[AIndex]:=AItem;
  Inc(FCount);
end;

Procedure TIntList.AssignTo(Dest:TPersistent);
begin
  if not (Dest is TIntList) then inherited AssignTo(Dest)
  else with Dest as TIntList do begin
    if FCapacity>0 then FreeMem(FItems,FCapacity*SizeOf(Integer));
    FCapacity:=0;
    FCount:=0;
    FCapacityDelta:=Self.FCapacityDelta;
    Capacity:=Self.Capacity;
    FCount:=Self.FCount;
    Move(Self.FItems^,FItems^,FCount*SizeOf(Integer));
  end;
end;

end.

