Unit TreeList;

Interface

Uses Classes;

Type TTreeList         = Class;

     TTreeListCompFunc  = Function(Item1,Item2:TTreeList):Integer;

     TTreeList          = Class
      Private
       FExpanded        : Boolean;
       FItems           : TList;
       FParent          : TTreeList;
       Function    GetAllCount:Integer;
       Function    GetAllItem(AIndex:Integer):TTreeList;
       Function    GetCount:Integer;
       Function    GetExpandedCount:Integer;
       Function    GetExpandedItem(AIndex:Integer):TTreeList;
       Function    GetItem(AIndex:Integer):TTreeList;
       Function    GetLevel:Integer;
       Function    GetVisible:Boolean;
       Function    GetVisibleParent:TTreeList;
       Procedure   QuickSort(SortFunction:TTreeListCompFunc;L:Integer;R:Integer);
       Procedure   SetItem(AIndex:Integer;Item:TTreeList);
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Property    AllCount:Integer read GetAllCount;
       Function    AllIndexOf(AItem:TTreeList):Integer;
       Property    AllItems[AIndex:Integer]:TTreeList read GetAllItem;
       Procedure   AllRemove(AItem:TTreeList);
       Procedure   Add(AItem:TTreeList);
       Procedure   Clear;
       Procedure   CollapseAllChildren;
       Property    Count:Integer read GetCount;
       Procedure   Delete(AIndex:Integer);
       Property    Expanded:Boolean read FExpanded write FExpanded;
       Procedure   ExpandAllChildren;
       Property    ExpandedCount:Integer read GetExpandedCount;
       Function    ExpandedIndexOf(AItem:TTreeList):Integer;
       Property    ExpandedItems[AIndex:Integer]:TTreeList read GetExpandedItem;
       Procedure   FreeAll;
       Property    Items[AIndex:Integer]:TTreeList read GetItem write SetItem; default;
       Function    IndexOf(AItem:TTreeList):Integer;       
       Procedure   Insert(AIndex:Integer;AItem:TTreeList);
       Property    Level:Integer read GetLevel;
       Function    Next:TTreeList;
       Property    Parent:TTreeList read FParent write FParent;
       Function    Previous:TTreeList;
       Procedure   Remove(AItem:TTreeList);
       Procedure   Sort(SortFunction:TTreeListCompFunc;SortSubEntries:Boolean);
       Property    Visible:Boolean read GetVisible;
       Property    VisibleParent:TTreeList read GetVisibleParent;
     end;

Implementation

{==============================================================================}
{ TTreeList                                                                    }
{==============================================================================}

Constructor TTreeList.Create;
begin
  inherited Create;
  FItems:=TList.Create;
  FExpanded:=TRUE;
end;

Destructor TTreeList.Destroy;
begin
  FreeAll;
  FItems.Free;
  inherited Destroy;
end;

Procedure TTreeList.FreeAll;
var Cnt          : Integer;
begin
  for Cnt:=0 to FItems.Count-1 do TTreeList(FItems[Cnt]).Free;
  FItems.Clear;
end;

Procedure TTreeList.Add
   (
   AItem           : TTreeList
   );
  begin
    FItems.Add(AItem);
    AItem.Parent:=Self;
  end;

Procedure TTreeList.Remove
   (
   AItem           : TTreeList
   );
  begin
    FItems.Remove(AItem);
    AItem.Parent:=NIL;
  end;

Function TTreeList.IndexOf
   (
   AItem           : TTreeList
   )
   : Integer;
  begin
    Result:=FItems.IndexOf(AItem);
  end;

Procedure TTreeList.Clear;
  begin
    FItems.Clear;
  end;

Procedure TTreeList.Insert
   (
   AIndex          : Integer;
   AItem           : TTreeList
   );
  begin
    FItems.Insert(AIndex,AItem);
    AItem.Parent:=Self;
  end;

Function TTreeList.GetExpandedItem
   (
   AIndex          : Integer
   )
   : TTreeList;
  var Cnt          : Integer;
  begin
    if FParent=NIL then Inc(AIndex);
    Result:=NIL;
    if AIndex=0 then Result:=Self
    else for Cnt:=0 to FItems.Count-1 do begin
      Result:=FItems[Cnt];
      if AIndex<=Result.ExpandedCount then begin
        Result:=Result.GetExpandedItem(AIndex-1);
        Break;
      end
      else Dec(AIndex,Result.ExpandedCount);
    end;
  end;

Function TTreeList.Next
   : TTreeList;
  var AIndex     : Integer;
  begin
    Result:=NIL;
    if Parent<>NIL then begin
      AIndex:=Parent.IndexOf(Self)+1;
      if (AIndex>0)
          and (AIndex<Parent.Count) then
        Result:=Parent[AIndex];
    end;
  end;

Function TTreeList.Previous
   : TTreeList;
  var AIndex     : Integer;
  begin
    Result:=NIL;
    if Parent<>NIL then begin
      AIndex:=Parent.IndexOf(Self)-1;
      if AIndex>=0 then Result:=Parent[AIndex];
    end;
  end;

Function TTreeList.GetItem
   (
   AIndex          : Integer
   )
   : TTreeList;
  begin
    Result:=FItems[AIndex];
  end;

Procedure TTreeList.Delete
   (
   AIndex          : Integer
   );
  begin
    FItems.Delete(AIndex);
  end;

Procedure TTreeList.SetItem
   (
   AIndex          : Integer;
   Item            : TTreeList
   );
  begin
    FItems[AIndex]:=Item;
  end;

Function TTreeList.GetCount
   : Integer;
  begin
    Result:=FItems.Count;
  end;

Function TTreeList.GetExpandedCount
   : Integer;
  var Cnt          : Integer;
  begin
    Result:=1;
    if Expanded then for Cnt:=0 to FItems.Count-1 do
        Inc(Result,TTreeList(FItems[Cnt]).ExpandedCount);
    if Parent=NIL then Dec(Result);
  end;

Function TTreeList.GetLevel
   : Integer;
  var AParent      : TTreeList;
  begin
    Result:=0;
    AParent:=Parent;
    while AParent<>NIL do begin
      Inc(Result);
      AParent:=AParent.Parent;
    end;
  end;

Function TTreeList.ExpandedIndexOf
   (
   AItem           : TTreeList
   )
   : Integer;
  var Cnt          : Integer;
      AIndex       : Integer;
  begin
    if AItem=Self then Result:=0
    else begin
      Result:=0;
      for Cnt:=0 to Count-1 do begin
        AIndex:=Items[Cnt].ExpandedIndexOf(AItem);
        if AIndex=-1 then Inc(Result,Items[Cnt].ExpandedCount)
        else begin
          Inc(Result,AIndex+1);
          if Parent=NIL then Dec(Result);
          Exit;
        end;
      end;
      Result:=-1;
    end;
  end;

Function TTreeList.AllIndexOf
   (
   AItem           : TTreeList
   )
   : Integer;
  var Cnt          : Integer;
      AIndex       : Integer;
  begin
    if AItem=Self then Result:=0
    else begin
      Result:=0;
      for Cnt:=0 to Count-1 do begin
        AIndex:=Items[Cnt].AllIndexOf(AItem);
        if AIndex=-1 then Inc(Result,Items[Cnt].AllCount)
        else begin
          Inc(Result,AIndex+1);
          if Parent=NIL then Dec(Result);
          Exit;
        end;
      end;
      Result:=-1;
    end;
  end;

Function TTreeList.GetAllCount
   : Integer;
  var Cnt          : Integer;
  begin
    if FParent=NIL then Result:=0
    else Result:=1;
    for Cnt:=0 to FItems.Count-1 do
        Inc(Result,Items[Cnt].GetAllCount);
  end;

Function TTreeList.GetAllItem
   (
   AIndex          : Integer
   )
   : TTreeList;                                                         
  var Cnt          : Integer;
  begin
    if FParent=NIL then Inc(AIndex);
    Result:=NIL;
    if AIndex=0 then Result:=Self
    else for Cnt:=0 to FItems.Count-1 do begin
      Result:=FItems[Cnt];
      if AIndex<=Result.AllCount then begin
        Result:=Result.GetAllItem(AIndex-1);
        Break;
      end
      else Dec(AIndex,Result.AllCount);
    end;
  end;

Procedure TTreeList.AllRemove
   (
   AItem           : TTreeList
   );
  var AIndex       : Integer;
  begin
    AIndex:=AItem.Parent.IndexOf(AItem); 
    if AIndex>-1 then AItem.Parent.Delete(AIndex);
  end;

Procedure TTreeList.QuickSort
   (
   SortFunction    : TTreeListCompFunc;
   L               : Integer;
   R               : Integer
   );
  var I            : Integer;
      J            : Integer;
      P            : TTreeList;
      Temp         : TTreeList;
  begin
    I:=L;
    J:=R;
    P:=FItems[(L+R) Shr 1];
    repeat
      while SortFunction(FItems[I],P)<0 do Inc(I);
      while SortFunction(FItems[J],P)>0 do Dec(J);
      if I<=J then begin
        Temp:=FItems[I];
        FItems[I]:=FItems[J];
        FItems[J]:=Temp;
        Inc(I);
        Dec(J);
      end;
    until I>J;
    if L<J then QuickSort(SortFunction,L,J);
    if I<R then QuickSort(SortFunction,I,R);
  end; 

Procedure TTreeList.Sort
   (
   SortFunction    : TTreeListCompFunc;
   SortSubEntries  : Boolean
   );
  var Cnt          : Integer; 
  begin
    if FItems.Count>0 then begin
      QuickSort(SortFunction,0,FItems.Count-1);
      if SortSubEntries then for Cnt:=0 to FItems.Count-1 do
        TTreeList(FItems[Cnt]).Sort(SortFunction,TRUE);
    end;    
  end;

Function TTreeList.GetVisible
   : Boolean;
  var Item         : TTreeList;
  begin
    Result:=FALSE;
    Item:=Parent;
    while Item<>NIL do begin
      if not Item.Expanded then Exit;
      Item:=Item.Parent;
    end;
    Result:=TRUE;
  end;

Function TTreeList.GetVisibleParent
   : TTreeList;
  begin
    Result:=Parent;
    while (Result<>NIL) and not(Result.Visible) do Result:=Result.Parent;
  end;

Procedure TTreeList.CollapseAllChildren;
  var Cnt          : Integer;
  begin
    for Cnt:=0 to Count-1 do Items[Cnt].Expanded:=FALSE;
  end;

Procedure TTreeList.ExpandAllChildren;
  var Cnt          : Integer;
  begin
    for Cnt:=0 to Count-1 do Items[Cnt].Expanded:=TRUE;
  end;

end.
