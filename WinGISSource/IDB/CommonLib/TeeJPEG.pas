{**********************************************}
{   TeeChart JPEG related functions            }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
{$IFDEF TEEOCX}
{$I teeaxdefs.inc}
{$ENDIF}
unit TeeJPEG;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, JPEG, TeeProcs, ComCtrls, TeExport;

type
  TTeeJPEGOptions = class(TForm)
    CBGray: TCheckBox;
    RGPerf: TRadioGroup;
    Label1: TLabel;
    EQuality: TEdit;
    UpDown1: TUpDown;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TJPEGExportFormat=class(TTeeExportFormat)
  private
    FProperties: TTeeJPEGOptions;
    Procedure CheckProperties;
  protected
    Procedure DoCopyToClipboard; override;
  public
    function Description:String; override;
    function FileExtension:String; override;
    function FileFilter:String; override;
    Function Jpeg:TJPEGImage;
    Function Options:TForm; override;
    Procedure SaveToStream(Stream:TStream); override;
  end;

procedure TeeSaveToJPEGFile( APanel:TCustomTeePanel;
                             const FileName: WideString;
                             Gray: WordBool;
                             Performance: TJPEGPerformance;
                             Quality, AWidth, AHeight: Integer);
implementation

{$R *.DFM}

Uses Clipbrd, TeeImageConstants;

Function TeeGetJPEGImageParams( APanel:TCustomTeePanel;
                                Params:TJPEGDefaults;
                                Width,Height:Integer):TJPEGImage;
var tmpBitmap : TBitmap;
begin
  result:=TJPEGImage.Create;
  tmpBitmap:=APanel.TeeCreateBitmap(APanel.Color,Rect(0,0,Width,Height));
  try
    { set the desired JPEG options... }
    With result do
    begin
      GrayScale            :=Params.GrayScale;
      ProgressiveEncoding  :=Params.ProgressiveEncoding;
      CompressionQuality   :=Params.CompressionQuality;
      PixelFormat          :=Params.PixelFormat;
      ProgressiveDisplay   :=Params.ProgressiveDisplay;
      Performance          :=Params.Performance;
      Scale                :=Params.Scale;
      Smoothing            :=Params.Smoothing;
      { Copy the temporary Bitmap onto the JPEG image... }
      Assign(tmpBitmap);
    end;
  finally
    tmpBitmap.Free;  { <-- free the temporary Bitmap }
  end;
end;

{ This function creates a JPEG Image, sets the desired JPEG
  parameters, draws a Chart (or TeeTree) on it, Saves the JPEG on disk and
  Loads the JPEG from disk to show it on this Form. }

procedure TeeSaveToJPEGFile( APanel:TCustomTeePanel;
                             Const FileName: WideString;
                             Gray: WordBool;
                             Performance: TJPEGPerformance;
                             Quality, AWidth, AHeight: Integer);
var tmp       : String;
    tmpWidth,
    tmpHeight : Integer;
    Params    : TJPEGDefaults;
begin
  tmp:=FileName;
  if Pos('.',tmp)=0 then tmp:=tmp+'.jpg';
  { Set the JPEG params }
  Params.GrayScale:=Gray;
  Params.CompressionQuality:=Quality;
  Params.Performance:=Performance;
  if AWidth=0 then tmpWidth:=APanel.Width
              else tmpWidth:=AWidth;
  if AHeight=0 then tmpHeight:=APanel.Height
               else tmpHeight:=AHeight;
  { Create the JPEG with the Chart image }
  With TeeGetJPEGImageParams(APanel,Params,tmpWidth,tmpHeight) do
  try
    SaveToFile(tmp);    { <-- save the JPEG to disk }
  finally
    Free;  { <-- free the temporary JPEG object }
  end;
end;

function TJPEGExportFormat.Description:String;
begin
  result:=TeeMsg_AsJPEG;
end;

function TJPEGExportFormat.FileExtension: String;
begin
  result:='jpg';
end;

function TJPEGExportFormat.FileFilter:String;
begin
  result:=TeeMsg_JPEGFilter;
end;

Function TJPEGExportFormat.Jpeg:TJPEGImage;
var Params : TJPEGDefaults;
begin
  CheckProperties;
  Params.GrayScale:=FProperties.CBGray.Checked;
  Params.CompressionQuality:=FProperties.UpDown1.Position;
  Params.Performance:=TJPEGPerformance(FProperties.RGPerf.ItemIndex);
  CheckSize;
  result:=TeeGetJPEGImageParams(Panel,Params,Width,Height);
end;

Procedure TJPEGExportFormat.CheckProperties;
begin
  if not Assigned(FProperties) then FProperties:=TTeeJPEGOptions.Create(nil);
end;

Function TJPEGExportFormat.Options:TForm;
begin
  CheckProperties;
  result:=FProperties;
end;

procedure TJPEGExportFormat.DoCopyToClipboard;
var tmp : TJPEGImage;
begin
  tmp:=Jpeg;
  try
    Clipboard.Assign(tmp);
  finally
    tmp.Free;
  end;
end;

procedure TJPEGExportFormat.SaveToStream(Stream:TStream);
begin
  With Jpeg do
  try
    SaveToStream(Stream);
  finally
    Free;
  end;
end;

initialization
  RegisterTeeExportFormat(TJPEGExportFormat);
finalization
  UnRegisterTeeExportFormat(TJPEGExportFormat);
end.
