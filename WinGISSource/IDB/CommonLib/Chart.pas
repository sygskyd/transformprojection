{******************************************}
{   TCustomChart & TChart Components       }
{ Copyright (c) 1995-2000 by David Berneda }
{    All Rights Reserved                   }
{******************************************}
{$I teedefs.inc}
unit Chart;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     Classes, SysUtils,
     {$IFDEF CLX}
     QGraphics, QControls, QExtCtrls, QForms, QButtons,
     {$ELSE}
     Graphics, Controls, ExtCtrls, Forms, Printers, Buttons,
     {$ENDIF}
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
     Objects,
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
     TeeProcs, TeEngine, TeCanvas;

Const TeeMsg_DefaultFunctionName = 'TTeeFunction'; { <-- dont translate }
      TeeMsg_DefaultSeriesName   = 'Series';       { <-- dont translate }
      TeeMsg_DefaultToolName     = 'ChartTool';    { <-- dont translate }
      ChartComponentPalette      = 'TeeChart';     { <-- dont translate }

      TeeMaxLegendColumns        = 2;
      TeeDefaultLegendSymbolWidth= 20;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Type TChartAlignment = ( alLeftTop, alMidTop, alRightTop,
                         alLeftMid, alCenter, alRightMid,
                         alLeftBot, alMidBot, alRightBot );

Const co_ChartStoreVersion : Byte = 1;  { value of the current Chart Kind load-store version}

      stCircle       =  0;
      stBar          =  1;
      stLine         =  2;
      stArea         =  3;
      stPoint        =  4;

      stHorizBar     =  5;
      stHorizLine    =  6;
      stFastLine     =  7;
      stShape        =  8;
      stGantt        =  9;
      stArrow        = 10;
      stBubble       = 11;
      stCandle       = 12;
      stVolume       = 13;
      stSurface      = 14;
      stContour      = 15;
      stWaterFall    = 16;
      stColorGrid    = 17;
      stErrorBar     = 18;
      stError        = 19;
      stHighLow      = 20;
      stHistogram    = 21;
      stPolar        = 22;
      stRadar        = 23;
      stBar3D        = 24;
      stImageBar     = 25;
      stImagePoint   = 26;
      stDeltaPoint   = 27;
      stLinePoint    = 28;
      stBarJoin      = 29;
      stBigCandle    = 30;
      stBezier       = 31;
      stPoint3D      = 32;
      stWindRose     = 33;
      stClock        = 34;
      stTriangleSurf = 35;
      stDonut        = 36;
      stBoxPlot      = 37;
      stHorizBoxPlot = 38;
      stPyramid      = 39;
      stSmith        = 40;
      stMap          = 41;
      stCalendar     = 42;
      stFunnelSeries = 43;

      ftAdd          = 128;
      ftSubtract     = 129;
      ftMultiply     = 130;
      ftDivide       = 131;
      ftHigh         = 132;
      ftLow          = 133;
      ftAverage      = 134;
      ftCount        = 135;
      ftADX          = 136;
      ftMovingAvrg   = 137;
      ftExpMovAvrg   = 138;
      ftExpAverage   = 139;
      ftRSI          = 140;
      ftMomentum     = 141;
      ftMomentumDiv  = 142;
      ftStdDeviation = 143;
      ftRootMeanSq   = 144;
      ftMACD         = 145;
      ftStochastic   = 146;
      ftBollinger    = 147;
      ftCurveFitting = 148;
      ftTrend        = 149;
      ftExpTrend     = 150;
      ftCumulative   = 151;

var   RegistredTeeSeriesNotLocalized : Boolean = TRUE;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

var   AnimatedZoomFactor   : Double=3.0;     { controls the animated zoom "speed" }
      TeeScrollKeyShift    : TShiftState=[]; { keys that should be pressed to start scroll }

      TeeTitleFootDistance : Integer=5;      { fixed pixels distance between title/foot and chart }
      TeeUseMouseWheel     : Boolean=True;   { use the mouse wheel to scroll the axes }

type
  TCustomChartWall=class(TTeeCustomShape)
  private
    FDark3D      : Boolean;
    FSize        : Integer;
    Function ApplyDark3D:Boolean;
    Function GetPen:TChartPen;
    Procedure InitColor(AColor:TColor);
    Function IsColorStored:Boolean;
    Procedure SetDark3D(Value:Boolean);
    Procedure SetPen(Value:TChartPen);
    Procedure SetSize(Value:Integer);
  protected
    DefaultColor : TColor;
  public
    Constructor Create(AOwner: TCustomTeePanel);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TCustomTeePanel);
    Procedure   Store(var S: TOldStream);
    Function    CreateDeterminant(Determinant: String): String;
    Function    OnCompareItems(Item: TTeeCustomShape): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    Procedure   Assign(Source: TPersistent); override;

    property Color stored IsColorStored;
    property Dark3D:Boolean read FDark3D write SetDark3D default True;
    property Pen:TChartPen read GetPen write SetPen;
    property Size:Integer read FSize write SetSize default 0;
  end;

  TChartWall=class(TCustomChartWall)
  published
    property Brush;
    property Color;
    property Dark3D;
    property Pen;
    property Size;
    property Transparent;
    property Visible default True;
  end;

  TChartLegendGradient=class(TChartGradient)
  public
    Constructor Create(ChangedEvent:TNotifyEvent); override;
  published
    property Direction default gdRightLeft;
    property EndColor default clWhite;
    property StartColor default clSilver;
  end;

  { TCustomChartLegend Component }

  TLegendStyle    =(lsAuto,lsSeries,lsValues,lsLastValues);
  TLegendAlignment=(laLeft,laRight,laTop,laBottom);
  LegendException =class(Exception);

  TOnGetLegendText=Procedure( Sender:TCustomAxisPanel;
			      LegendStyle:TLegendStyle;
			      Index:Integer;
			      Var LegendText:String) of Object;

  TCustomChartLegend=class;

  TLegendSymbolSize=(lcsPercent,lcsPixels);

  TLegendSymbolPosition=(spLeft,spRight);

  TLegendSymbol=class(TPersistent)
  private
    FContinuous : Boolean;
    FPosition   : TLegendSymbolPosition;
    FWidth      : Integer;
    FWidthUnits : TLegendSymbolSize;

    ILegend     : TCustomChartLegend;
    procedure SetContinuous(const Value: Boolean);
    Procedure SetPosition(Const Value:TLegendSymbolPosition);
    procedure SetWidth(Const Value: Integer);
    procedure SetWidthUnits(const Value: TLegendSymbolSize);
  protected
    Function CalcWidth(Value:Integer):Integer;
  public
    Constructor Create(ALegend: TCustomChartLegend);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; ALegend: TCustomChartLegend);
    Procedure   Store(var S: TOldStream);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    Procedure   Assign(Source: TPersistent); override;
  published
    property Continuous:Boolean read FContinuous write SetContinuous default False;
    property Position:TLegendSymbolPosition read FPosition write SetPosition default spLeft;
    property Width:Integer read FWidth write SetWidth default TeeDefaultLegendSymbolWidth;
    property WidthUnits:TLegendSymbolSize read FWidthUnits
                                 write SetWidthUnits default lcsPercent;
  end;

  TTeeCustomShapePosition=class(TTeeCustomShape)
  private
    FCustomPosition : Boolean;
    Function GetLeft:Integer;
    Function GetTop:Integer;
    procedure SetCustomPosition(Const Value:Boolean);
    procedure SetLeft(Const Value: Integer);
    procedure SetTop(Const Value: Integer);
  public
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TCustomTeePanel);
    Procedure   Store(var S: TOldStream);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    Procedure   Assign(Source: TPersistent); override;

    property CustomPosition:Boolean read FCustomPosition write SetCustomPosition default False;
    property Left:Integer read GetLeft write SetLeft stored FCustomPosition;
    property Top:Integer read GetTop write SetTop stored FCustomPosition;
  end;

  TCustomChart=class;

  TCustomChartLegend=class(TTeeCustomShapePosition)
  private
    FAlignment     : TLegendAlignment;    //sr
    FCheckBoxes    : Boolean;             //sr+
    FCurrentPage   : Boolean;             //sr
    FDividingLines : TChartHiddenPen;     //sr
    FFirstValue    : Integer;             //sr+
    FFontSeriesColor: Boolean;            //sr+
    FHorizMargin   : Integer;             //sr+
    FInverted      : Boolean;             //sr+
    FLegendStyle   : TLegendStyle;        //sr
    FMaxNumRows    : Integer;             //sr
    FResizeChart   : Boolean;             //sr
    FSeries        : TChartSeries;
    FSymbol        : TLegendSymbol;       //sr
    FTextStyle     : TLegendTextStyle;    //sr
    FTopLeftPos    : Integer;             //sr
    FVertMargin    : Integer;             //sr+
    FVertSpacing   : Integer;             //sr+

    { Internal }
    IColorWidth    : Integer;
    ILegendStyle   : TLegendStyle;        //sr
    ILastValue     : Integer;             //sr
    ITotalItems    : Integer;

    Function GetShapeBounds:TRect;
    Function GetSymbolWidth:Integer;
    Function GetVertical:Boolean;
    Procedure SetAlignment(Const Value:TLegendAlignment);
    Procedure SetCheckBoxes(Const Value:Boolean);
    Procedure SetDividingLines(Const Value:TChartHiddenPen);
    Procedure SetFirstValue(Const Value:Integer);
    Procedure SetHorizMargin(Const Value:Integer);
    Procedure SetInverted(Const Value:Boolean);
    Function  GetLegendSeries:TChartSeries;
    Procedure SetLegendStyle(Const Value:TLegendStyle);
    Procedure SetMaxNumRows(Const Value:Integer);
    Procedure SetResizeChart(Const Value:Boolean);
    Procedure SetSeries(Const Value:TChartSeries);
    Procedure SetSymbol(Const Value:TLegendSymbol);
    Procedure SetSymbolWidth(Const Value:Integer);
    Procedure SetTextStyle(Const Value:TLegendTextStyle);
    Procedure SetTopLeftPos(Const Value:Integer);
    Procedure SetVertMargin(Const Value:Integer);
    Procedure SetVertSpacing(Const Value:Integer);
    procedure SetCurrentPage(Const Value: Boolean);
    procedure SetFontSeriesColor(const Value: Boolean);
  protected
    Procedure CalcLegendStyle;
    Function GetGradientClass:TChartGradientClass; override;
    property InternalLegendStyle:TLegendStyle read ILegendStyle;
  public
    NumCols        : Integer;
    NumRows        : Integer;
    ColumnWidthAuto: Boolean;             //sr
    ColumnWidths   : Array[0..TeeMaxLegendColumns-1] of Integer;

    Constructor Create(AOwner: TCustomChart);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TCustomChart);
    Procedure   Store(var S: TOldStream);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    Destructor  Destroy; override;

    Procedure Assign(Source:TPersistent); override;
    Function FormattedValue(ASeries:TChartSeries; ValueIndex:Integer):String;
    Function FormattedLegend(SeriesOrValueIndex:Integer):String;
    Procedure DrawLegend;
    property TotalLegendItems:Integer read ITotalItems;
    Function Clicked(x,y:Integer):Integer;

    { public properties }
    property RectLegend:TRect read GetShapeBounds;
    property Vertical:Boolean read GetVertical;

    { to be published }
    property Alignment:TLegendAlignment read FAlignment write SetAlignment
					default laRight;
    property CheckBoxes:Boolean read FCheckBoxes
                                      write SetCheckBoxes default False;
    property ColorWidth:Integer read GetSymbolWidth write SetSymbolWidth default TeeDefaultLegendSymbolWidth;
    property CurrentPage:Boolean read FCurrentPage write SetCurrentPage default True;
    property DividingLines:TChartHiddenPen read FDividingLines write SetDividingLines;
    property FirstValue:Integer read FFirstValue write SetFirstValue default 0;
    property FontSeriesColor:Boolean read FFontSeriesColor write SetFontSeriesColor default False;
    property HorizMargin:Integer read FHorizMargin write SetHorizMargin default 0;
    property Inverted:Boolean read FInverted write SetInverted default False;
    property LegendStyle:TLegendStyle read FLegendStyle
				      write SetLegendStyle default lsAuto;
    property MaxNumRows:Integer read FMaxNumRows write SetMaxNumRows default 10;
    property ResizeChart:Boolean read FResizeChart write SetResizeChart default True;
    property Series:TChartSeries read FSeries write SetSeries;
    property Symbol:TLegendSymbol read FSymbol write SetSymbol;
    property TextStyle:TLegendTextStyle read FTextStyle
					write SetTextStyle default ltsLeftValue;
    property TopPos:Integer read FTopLeftPos write SetTopLeftPos default 10;
    property VertMargin:Integer read FVertMargin write SetVertMargin default 0;
    property VertSpacing:Integer read FVertSpacing write SetVertSpacing default 0;
    property Visible default True;
  end;

  TChartLegend=class(TCustomChartLegend)
  published
    property Alignment;
    property Bevel;
    property BevelWidth;
    property Brush;
    property CheckBoxes;
    property Color;
    property ColorWidth;
    property CustomPosition;
    property DividingLines;
    property FirstValue;
    property Font;
    property FontSeriesColor;
    property Frame;
    property Gradient;
    property HorizMargin;
    property Inverted;
    property Left;
    property LegendStyle;
    property MaxNumRows;
    property ResizeChart;
    property ShadowColor;
    property ShadowSize;
    property ShapeStyle;
    property Symbol;
    property TextStyle;
    property Top;
    property TopPos;
    property Transparency;
    property Transparent;
    property VertMargin;
    property VertSpacing;
    property Visible;
  end;

  TChartTitle=class(TTeeCustomShapePosition)
  private
    FAdjustFrame : Boolean;
    FAlignment   : TAlignment;
    FText        : TStrings;

    IOnTop       : Boolean;
    Function GetShapeBounds:TRect;
    Procedure SetAdjustFrame(Value:Boolean);
    Procedure SetAlignment(Value:TAlignment);
    Procedure SetText(Value:TStrings);
  public
    Constructor Create(AOwner: TCustomTeePanel); virtual;
    Destructor  Destroy; override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TCustomTeePanel);
    Procedure   Store(var S: TOldStream);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    Procedure Assign(Source:TPersistent); override;
    Function Clicked(x,y:Integer):Boolean;
    procedure DrawTitle;
    property TitleRect:TRect read GetShapeBounds;
  published
    property AdjustFrame:Boolean read FAdjustFrame write SetAdjustFrame
				 default True;
    property Alignment: TAlignment read FAlignment
				   write SetAlignment default taCenter;
    property Bevel;
    property BevelWidth;
    property Brush;
    property Color;
    property Font;
    property Frame;
    property Gradient;
    property ShadowColor;
    property ShadowSize;
    property ShapeStyle;
    property Text:TStrings read FText write SetText;
    property Transparency;
    property Transparent default True;
    property Visible default True;
  end;

  TChartFootTitle=class(TChartTitle)
  public
    Constructor Create(AOwner: TCustomTeePanel); override;
  end;

  TChartClick=procedure( Sender:TCustomChart;
			 Button:TMouseButton;
			 Shift: TShiftState;
			 X, Y: Integer) of object;

  TChartClickAxis=procedure( Sender:TCustomChart;
			     Axis:TChartAxis;
			     Button:TMouseButton;
			     Shift: TShiftState;
			     X, Y: Integer) of object;

  TChartClickSeries=procedure( Sender:TCustomChart;
			       Series:TChartSeries;
			       ValueIndex:Integer;
			       Button:TMouseButton;
			       Shift: TShiftState;
			       X, Y: Integer) of object;

  TChartClickTitle=procedure( Sender:TCustomChart;
			      ATitle:TChartTitle;
			      Button:TMouseButton;
			      Shift: TShiftState;
			      X, Y: Integer) of object;

  TOnGetLegendPos=Procedure( Sender:TCustomChart; Index:Integer;
			     Var X,Y,XColor:Integer) of object;

  TOnGetLegendRect=Procedure( Sender:TCustomChart; Var Rect:TRect) of object;

  TAxisSavedScales=Packed Record
    Auto    : Boolean;
    AutoMin : Boolean;
    AutoMax : Boolean;
    Min     : Double;
    Max     : Double;
  end;

  TAllAxisSavedScales=Packed Record
    Top    : TAxisSavedScales;
    Left   : TAxisSavedScales;
    Right  : TAxisSavedScales;
    Bottom : TAxisSavedScales;
  end;

  TChartBackWall=class(TChartWall)
  published
    property Color default clSilver;
    property Gradient;
    property Transparency;
    property Transparent default True;
  end;

  TChartRightWall=class(TChartWall)
  published
    property Color default clSilver;
    property Visible default False;
  end;

  TChartAllowScrollEvent=Procedure( Sender:TChartAxis; Var AMin,AMax:Double;
				    Var AllowScroll:Boolean ) of object;

  TCustomChart=class(TCustomAxisPanel)
  private
    FBackWall          : TChartBackWall;        //sr
    FBottomWall        : TChartWall;            //sr
    FFoot              : TChartTitle;           //sr
    FLeftWall          : TChartWall;            //sr
    FLegend            : TChartLegend;          //sr
    FRightWall         : TChartRightWall;       //sr
    FSavedScales       : TAllAxisSavedScales;
    FScrollMouse       : TMouseButton;          //set
    FSubFoot           : TChartTitle;           //sr
    FSubTitle          : TChartTitle;           //sr
    FTitle             : TChartTitle;           //sr

    { events }
    FOnAllowScroll     : TChartAllowScrollEvent;
    FOnClickAxis       : TChartClickAxis;
    FOnClickBackGround : TChartClick;
    FOnClickLegend     : TChartClick;
    FOnClickSeries     : TChartClickSeries;
    FOnClickTitle      : TChartClickTitle;
    FOnGetLegendPos    : TOnGetLegendPos;
    FOnGetLegendRect   : TOnGetLegendRect;
    FOnGetLegendText   : TOnGetLegendText;

    Function ActiveSeriesUseAxis:Boolean;
    procedure BroadcastMouseEvent(AEvent:TChartMouseEvent;
                         Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    Function CalcNeedClickedPart(Pos:TPoint; Needed:Boolean):TChartClickedPart;
    Procedure DrawRightWall;
    Function DrawRightWallAfter:Boolean;
    Function GetFrame:TChartPen;
    Procedure PrepareWallCanvas(AWall:TChartWall);
    procedure ReadBackColor(Reader: TReader);
    Procedure RestoreScales(Const Saved:TAllAxisSavedScales);
    Function SaveScales:TAllAxisSavedScales;
    {$IFDEF D4}
    Procedure ScrollVerticalAxes(Up:Boolean);
    {$ENDIF}
    Procedure SetBackColor(Value:TColor);
    Procedure SetBackWall(Value:TChartBackWall);
    Procedure SetBottomWall(Value:TChartWall);
    Procedure SetFoot(Value:TChartTitle);
    Procedure SetFrame(Value:TChartPen);
    Procedure SetLeftWall(Value:TChartWall);
    Procedure SetLegend(Value:TChartLegend);
    Procedure SetRightWall(Value:TChartRightWall);
    procedure SetSubFoot(const Value: TChartTitle);
    procedure SetSubTitle(const Value: TChartTitle);
    Procedure SetTitle(Value:TChartTitle);
  protected
    RestoredAxisScales: Boolean;
    Procedure CalcWallsRect; override;
    Function CalcWallSize(Axis:TChartAxis):Integer; override;
    Procedure CalcZoomPoints; dynamic;
    Procedure DefineProperties(Filer:TFiler); override;
    Procedure DoZoom(Const TopI,TopF,BotI,BotF,LefI,LefF,RigI,RigF:Double);
    procedure DrawTitlesAndLegend(BeforeSeries:Boolean); override;
    Procedure DrawWalls; override;
    Function GetBackColor:TColor; override;
    Procedure GetChildren(Proc:TGetChildProc; Root:TComponent); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    {$IFDEF D4}
    function DoMouseWheel(Shift: TShiftState; WheelDelta: Integer;
      MousePos: TPoint): Boolean; override;
    function DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean; override;
    function DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean; override;
    {$ENDIF}
  public
    Constructor Create(AOwner: TComponent); override;
    Destructor  Destroy; override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TComponent);
    Procedure   Store(var S: TOldStream);
    Function    CreateDeterminant(Determinant: String): String;
    Function    OnCompareItems(Item: TComponent): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    Procedure Assign(Source:TPersistent); override;
    Function AxisTitleOrName(Axis:TChartAxis):String;
    Procedure CalcClickedPart(Pos:TPoint; Var Part:TChartClickedPart);
    Procedure FillSeriesSourceItems(ASeries:TChartSeries; Proc:TGetStrProc); dynamic;
    Procedure FillValueSourceItems(ValueList:TChartValueList; Proc:TGetStrProc); dynamic;
    Function GetASeries:TChartSeries; { return first active Series }
    Function IsFreeSeriesColor(AColor:TColor; CheckBackground:Boolean):Boolean; override;
    Procedure NextPage;
    Procedure PreviousPage;
    Procedure RemoveAllSeries;
    Procedure SeriesDown(ASeries:TChartSeries);
    Procedure SeriesUp(ASeries:TChartSeries);
    procedure UndoZoom; override;
    Procedure ZoomPercent(Const PercentZoom:Double);
    Procedure ZoomRect(Const Rect:TRect);
    Function FormattedValueLegend(ASeries:TChartSeries; ValueIndex:Integer):String; override;
    Function FormattedLegend(SeriesOrValueIndex:Integer):String;

    property BackColor:TColor read GetBackColor write SetBackColor; { compatibility }

    { to be published }
    property BackWall:TChartBackWall read FBackWall write SetBackWall;
    property Frame:TChartPen read GetFrame write SetFrame;
    property BottomWall:TChartWall read FBottomWall write SetBottomWall;
    property Foot:TChartTitle read FFoot write SetFoot;
    property LeftWall:TChartWall read FLeftWall write SetLeftWall;
    property Legend:TChartLegend read FLegend write SetLegend;
    property RightWall:TChartRightWall read FRightWall write SetRightWall;
    property ScrollMouseButton:TMouseButton read FScrollMouse write FScrollMouse default mbRight;
    property SubFoot:TChartTitle read FSubFoot write SetSubFoot;
    property SubTitle:TChartTitle read FSubTitle write SetSubTitle;
    property Title:TChartTitle read FTitle write SetTitle;

    { events }
    property OnAllowScroll:TChartAllowScrollEvent read FOnAllowScroll write FOnAllowScroll;
    property OnClickAxis:TChartClickAxis read FOnClickAxis write FOnClickAxis;
    property OnClickBackground:TChartClick read FOnClickbackground write FOnClickBackGround;
    property OnClickLegend:TChartClick read FOnClickLegend write FOnClickLegend;
    property OnClickSeries:TChartClickSeries read FOnClickSeries write FOnClickSeries;
    property OnClickTitle:TChartClickTitle read FOnClickTitle write FOnClickTitle;
    property OnGetLegendPos:TOnGetLegendPos read FOnGetLegendPos write FOnGetLegendPos;
    property OnGetLegendRect:TOnGetLegendRect read FOnGetLegendRect write FOnGetLegendRect;
    property OnGetLegendText:TOnGetLegendText read FOnGetLegendText write FOnGetLegendText;

    { TPanel properties }
    property Align;
    property BevelInner;
    property BevelOuter;
    property BevelWidth;
    property BorderWidth;
    property BorderStyle;
    property Color;
    {$IFNDEF CLX}
    {$IFDEF D4}
    {$IFNDEF TEEOCX}
    property UseDockManager default True;
    property DockSite;
    {$ENDIF}
    {$ENDIF}
    property DragCursor;
    {$ENDIF}
    property DragMode;
    property Enabled;
    property ParentColor;
    property ParentShowHint;
    {$IFNDEF TEEOCX}
    property PopupMenu;
    {$ENDIF}
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    {$IFDEF D4}
    property Anchors;
    {$IFNDEF CLX}
    property AutoSize;
    {$ENDIF}
    {$IFNDEF TEEOCX}
    property Constraints;
    {$ENDIF}
    {$IFNDEF CLX}
    property DragKind;
    {$ENDIF}
    property Locked;
    {$ENDIF}

    { TPanel events }
    property OnClick;
    {$IFDEF D5}
    property OnContextPopup;
    {$ENDIF}
    property OnDblClick;
    {$IFNDEF TEEOCX}
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnStartDrag;
    property OnEnter;
    property OnExit;
    {$ENDIF}
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    {$IFDEF D4}
    property OnMouseWheel;
    property OnMouseWheelDown;
    property OnMouseWheelUp;
    {$ENDIF}
    {$IFDEF D4}
    {$IFNDEF CLX}
    property OnCanResize;
    {$ENDIF}
    {$IFNDEF TEEOCX}
    property OnConstrainedResize;
    {$IFNDEF CLX}
    property OnDockDrop;
    property OnDockOver;
    property OnEndDock;
    property OnGetSiteInfo;
    property OnStartDock;
    property OnUnDock;
    {$ENDIF}
    {$ENDIF}
    {$ENDIF}
  end;

  TChart=class(TCustomChart)
  {$IFNDEF CLX}
  {$IFDEF D4}
  public
    property DockManager;
  {$ENDIF}
  {$ENDIF}
  published
    { TCustomChart Properties }
    property AllowPanning;
    property BackImage;
    property BackImageInside;
    property BackImageMode;
    property BackWall;
    property BottomWall;
    property Foot;
    property Gradient;
    property LeftWall;
    property Legend;
    property MarginBottom;
    property MarginLeft;
    property MarginRight;
    property MarginTop;
    property PrintProportional;
    property RightWall;
    property ScrollMouseButton;
    property SubFoot;
    property SubTitle;
    property Title;

    { TCustomChart Events }
    property OnAllowScroll;
    property OnClickAxis;
    property OnClickBackground;
    property OnClickLegend;
    property OnClickSeries;
    property OnClickTitle;
    property OnGetLegendPos;
    property OnGetLegendRect;
    property OnScroll;
    property OnUndoZoom;
    property OnZoom;

    { TCustomAxisPanel properties }
    property AxisBehind;
    property AxisVisible;
    property BottomAxis;
    property Chart3DPercent;
    property ClipPoints;
    property CustomAxes;
    property DepthAxis;
    property Frame;
    property LeftAxis;
    property MaxPointsPerPage;
    property Monochrome;
    property Page;
    property RightAxis;
    property ScaleLastPage;
    property SeriesList;
    property TopAxis;
    property View3D;
    property View3DOptions;
    property View3DWalls;
    property Zoom;

    { TCustomAxisPanel events }
    property OnAfterDraw;
    property OnBeforeDrawAxes;
    property OnBeforeDrawChart;
    property OnBeforeDrawSeries;
    property OnGetAxisLabel;
    property OnGetLegendText;
    property OnGetNextAxisLabel;
    property OnPageChange;

    { TPanel properties }
    property Align;
    property BevelInner;
    property BevelOuter;
    property BevelWidth;
    property BorderWidth;
    property BorderStyle;
    property Color;
    {$IFNDEF CLX}
    {$IFDEF D4}
    {$IFNDEF TEEOCX}
    property UseDockManager default True;
    property DockSite;
    {$ENDIF}
    property DragCursor;
    property DragMode;
    {$ENDIF}
    {$ELSE}
    property DragMode;
    {$ENDIF}
    property Enabled;
    property ParentColor;
    property ParentShowHint;
    {$IFNDEF TEEOCX}
    property PopupMenu;
    {$ENDIF}
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    {$IFNDEF CLX}
    {$IFDEF D4}
    property Anchors;
    property AutoSize;
    {$IFNDEF TEEOCX}
    property Constraints;
    {$ENDIF}
    property DragKind;
    property Locked;
    {$ENDIF}
    {$ELSE}
    property Anchors;
    property Locked;
    {$ENDIF}

    { TPanel events }
    property OnClick;
    {$IFDEF D5}
    property OnContextPopup;
    {$ENDIF}
    property OnDblClick;
    {$IFNDEF TEEOCX}
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnStartDrag;
    property OnEnter;
    property OnExit;
    {$ENDIF}
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    {$IFDEF D4}
    {$IFNDEF CLX}
    property OnCanResize;
    {$ENDIF}
    property OnMouseWheel;
    property OnMouseWheelDown;
    property OnMouseWheelUp;
    {$IFNDEF TEEOCX}
    property OnConstrainedResize;
    {$IFNDEF CLX}
    property OnDockDrop;
    property OnDockOver;
    property OnEndDock;
    property OnGetSiteInfo;
    property OnStartDock;
    property OnUnDock;
    {$ENDIF}
    {$ENDIF}
    {$ENDIF}
  end;

  TTeeSeriesTypes=class(TList)
  private
    Function Get(Index: Integer):TTeeSeriesType;
  public
    {$IFNDEF D4}
    Destructor Destroy; override;
    {$ELSE}
    Procedure Clear; override;
    {$ENDIF}
    Function Find(ASeriesClass: TChartSeriesClass): TTeeSeriesType;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Function FindAndGetSeriesType(ASeriesClass: TChartSeriesClass): Byte;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    property Items[Index: Integer]: TTeeSeriesType read Get; default;
  end;

Var TeeSeriesTypes : TTeeSeriesTypes;  { List of Series types }

type
  TTeeToolTypes=class(TList)
  private
    Function Get(Index:Integer):TTeeCustomToolClass;
  public
    property Items[Index:Integer]:TTeeCustomToolClass read Get; default;
  end;

Var TeeToolTypes : TTeeToolTypes;

{$IFNDEF CLX}
type
  TTeeDragObject = class(TDragObject)
  private
    FPart: TChartClickedPart;
  protected
    function GetDragCursor(Accepted: Boolean; X, Y: Integer): TCursor; override;
    procedure Finished(Target: TObject; X, Y: Integer; Accepted: Boolean); override;
  public
    constructor Create(Const APart: TChartClickedPart);
    property Part: TChartClickedPart read FPart;
  end;
{$ENDIF}

{ Adds a new Series component definition for the Gallery }
{ Setting ANumGallerySeries to zero makes the Series to not appear on the
  Gallery. }
Procedure RegisterTeeSeries( ASeriesClass: TChartSeriesClass;
			     Const ADescription, AGalleryPage: String;
			     ANumGallerySeries: Integer;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
                             ASeriesType: Byte);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{ Adds a new Function component definition for the Gallery }
Procedure RegisterTeeFunction( AFunctionClass: TTeeFunctionClass;
			       Const ADescription, AGalleryPage: String;
			       ANumGallerySeries: Integer;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
                               ASeriesType: Byte);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{ Adds a new Function component to the basic functions gallery }
Procedure RegisterTeeBasicFunction( AFunctionClass: TTeeFunctionClass;
				    Const ADescription: String;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
                                     ASeriesType: Byte);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{ Adds a new Series & Function components definition for the Gallery }
Procedure RegisterTeeSeriesFunction( ASeriesClass: TChartSeriesClass;
				     AFunctionClass: TTeeFunctionClass;
				     Const ADescription, AGalleryPage: String;
				     ANumGallerySeries: Integer;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
                                     ASeriesType: Byte);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{ Removes Series from Gallery }
Procedure UnRegisterTeeSeries(ASeriesList:Array of TChartSeriesClass);

{ Removes Functions from Gallery }
Procedure UnRegisterTeeFunctions(AFunctionList:Array of TTeeFunctionClass);

{ Assigns all Series properties from Old to New.
  (Data values are not assigned). }
Procedure AssignSeries(Var OldSeries,NewSeries:TChartSeries);

{ Creates a new "class" TeeFunction and sets its ParentSeries to ASeries }
Function CreateNewTeeFunction( ASeries:TChartSeries;
			       AClass:TTeeFunctionClass):TTeeFunction;

{ Creates a new "class" TChartSeries and sets its ParentChart to AChart }
Function CreateNewSeries( AOwner:TComponent;
			  AChart:TCustomAxisPanel;
			  AClass:TChartSeriesClass;
			  AFunctionClass:TTeeFunctionClass):TChartSeries;

Function CloneChartSeries(ASeries:TChartSeries):TChartSeries;

procedure ChangeSeriesType( Var ASeries:TChartSeries;
				NewType:TChartSeriesClass );

procedure ChangeAllSeriesType( AChart:TCustomChart; AClass:TChartSeriesClass );

Function GetNewSeriesName(AOwner:TComponent):TComponentName;

Procedure TeeDrawCheckBox( x,y:Integer; Canvas:TTeeCanvas; Checked:Boolean;
                           ABackColor:TColor);

Procedure RegisterTeeTools(Tools:Array of TTeeCustomToolClass);
Procedure UnRegisterTeeTools(Tools:Array of TTeeCustomToolClass);

{ Returns the name of a Series in it�s "gallery" style:
   TLineSeries returns "Line"
   TPieSeries returns "Pie"
   etc.
}
Function GetGallerySeriesName(ASeries:TChartSeries):String;

{ Draws the Series "Legend" on the specified rectangle and Canvas }
Procedure PaintSeriesLegend(ASeries:TChartSeries; ACanvas:TCanvas; Const R:TRect);

implementation

Uses TeeConst
     {$IFNDEF D5}
     {$IFNDEF TEEOCX}
     ,DsgnIntf
     {$ENDIF}
     {$ENDIF}
     {$IFDEF D6}
     ,Types
     {$ENDIF}
     ;

Const TeeCheckBoxSize=11;

Procedure TeeDrawCheckBox( x,y:Integer; Canvas:TTeeCanvas; Checked:Boolean;
                           ABackColor:TColor);
var t:Integer;
begin
  With Canvas do
  begin
    Pen.Style:=psSolid;
    Pen.Width:=1;
    Pen.Color:=clGray;
    DoHorizLine(x+TeeCheckBoxSize,x,y);
    LineTo(x,y+TeeCheckBoxSize+1);
    if ColorToRGB(ABackColor)=clWhite then Pen.Color:=clSilver
                                      else Pen.Color:=clWhite;
    DoHorizLine(x,x+TeeCheckBoxSize+1,y+TeeCheckBoxSize+1);
    LineTo(x+TeeCheckBoxSize+1,y-1);
    Pen.Color:=clBlack;
    DoHorizLine(x+TeeCheckBoxSize-1,x+1,y+1);
    LineTo(x+1,y+TeeCheckBoxSize);
    Brush.Style:=bsSolid;
    Brush.Color:=clWindow;
    Pen.Style:=psClear;
    Rectangle(x+2,y+2,x+TeeCheckBoxSize+1,y+TeeCheckBoxSize+1);
    if Checked then
    begin
      Pen.Style:=psSolid;
      Pen.Color:=clWindowText;
      for t:=1 to 3 do DoVertLine(x+2+t,y+4+t,y+7+t);
      for t:=1 to 4 do DoVertLine(x+5+t,y+7-t,y+10-t);
    end;
  end;
end;

{ TCustomChartWall }
Constructor TCustomChartWall.Create(AOwner: TCustomTeePanel);
begin
   inherited;
   FDark3D:=True;
   Color:=clTeeColor;
   DefaultColor:=clTeeColor;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TCustomChartWall.Load(var S: TOldStream; AOwner: TCustomTeePanel);
var intValue    : Integer;
begin
   inherited;
   S.Read(intValue,SizeOf(Integer));
   // Unpack values
   FDark3D:=(intValue and 1) <> 0;
   FSize:=intValue shr 1;
   S.Read(DefaultColor,SizeOf(TColor));
end;

Procedure TCustomChartWall.Store(var S: TOldStream);
var intValue    : Integer;
begin
   inherited Store(S);
   // Pack values
   intValue:=FSize shl 1;
   if FDark3D then
      intValue:=intValue or 1;
   S.Write(intValue,SizeOf(Integer));
   S.Write(DefaultColor,SizeOf(TColor));
end;

Function TCustomChartWall.CreateDeterminant(Determinant: String): String;
var Size        : Integer;
    intValue    : Integer;
begin
   Size:=Length(Determinant);
   // Pack values
   intValue:=FSize shl 1;
   if FDark3D then
      intValue:=intValue or 1;
   SetLength(Result,Size+SizeOf(intValue));
   CopyMemory(@Result[1],@Determinant[1],Size);
   Inc(Size);
   CopyMemory(@Result[Size],@intValue,SizeOf(intValue));
   Result := inherited CreateDeterminant(Result);
end;

Function TCustomChartWall.OnCompareItems(Item: TTeeCustomShape): Boolean;
begin
   Result:=FALSE;
   if Item is TCustomChartWall then
      with TCustomChartWall(Item) do
         if Self.FDark3D =  FDark3D then
            if Self.FSize =  FSize then
               Result := inherited OnCompareItems(Item);
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Function TCustomChartWall.IsColorStored:Boolean;
begin
  result:=Color<>DefaultColor;
end;

Procedure TCustomChartWall.Assign(Source:TPersistent);
Begin
  if Source is TCustomChartWall then
  With TCustomChartWall(Source) do
  Begin
    Self.FDark3D := FDark3D;
    Self.FSize   := FSize;
  end;
  inherited;
End;

Function TCustomChartWall.ApplyDark3D:Boolean;
begin
  result:=(Brush.Style<>bsClear) and (Brush.Bitmap=nil) and FDark3D;
end;

Function TCustomChartWall.GetPen:TChartPen;
begin
  result:=Frame;
end;

Procedure TCustomChartWall.SetPen(Value:TChartPen);
Begin
  Frame:=Value;
end;

type TTeePanelAccess=class(TCustomTeePanel);

Procedure TCustomChartWall.SetSize(Value:Integer);
Begin
  TTeePanelAccess(ParentChart).SetIntegerProperty(FSize,Value);
End;

Procedure TCustomChartWall.SetDark3D(Value:Boolean);
begin
  TTeePanelAccess(ParentChart).SetBooleanProperty(FDark3D,Value);
end;

type TTeeFontAccess=class(TTeeFont);

procedure TCustomChartWall.InitColor(AColor: TColor);
begin
  Color:=AColor;
  DefaultColor:=AColor;
end;

{ TChartTitle }
Constructor TChartTitle.Create(AOwner: TCustomTeePanel);
begin
   inherited;
   IOnTop:=True;

   FAlignment:=taCenter;
   FAdjustFrame:=True;
   FText:=TStringList.Create;
   TStringList(FText).OnChange:=AOwner.CanvasChanged;
   { inherited }
   Transparent:=True;
   Font.Color:=clBlue;
   TTeeFontAccess(Font).IDefColor:=clBlue;
end;

Destructor TChartTitle.Destroy;
Begin
  FText.Free;
  inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TChartTitle.Load(var S: TOldStream; AOwner: TCustomTeePanel);
begin
   inherited;
   S.Read(IOnTop,SizeOf(Boolean));
   S.Read(FAlignment,SizeOf(TAlignment));
   S.Read(FAdjustFrame,SizeOf(Boolean));

   FText:=TStringList.Create;
   TStringList(FText).OnChange:=AOwner.CanvasChanged;
end;

Procedure TChartTitle.Store(var S: TOldStream);
begin
   inherited;
   S.Write(IOnTop,SizeOf(Boolean));
   S.Write(FAlignment,SizeOf(TAlignment));
   S.Write(FAdjustFrame,SizeOf(Boolean));
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TChartTitle.Assign(Source:TPersistent);
Begin
  if Source is TChartTitle then
  With TChartTitle(Source) do
  Begin
    Self.FAdjustFrame  := FAdjustFrame;
    Self.FAlignment    := FAlignment;
    Self.Text          := FText;
  end;
  inherited;
end;

Function TChartTitle.Clicked(x,y:Integer):Boolean;
begin
  result:=Visible and {$IFDEF D6}Types.{$ENDIF}PtInRect(ShapeBounds,{$IFDEF D6}Types.{$ELSE}Classes.{$ENDIF}Point(x,y));
end;

procedure TChartTitle.DrawTitle;
var tmpXPosTitle  : Integer;
    tmpMargin     : Integer;
    FontH         : Integer;
    tmpFrameWidth : Integer;

  Procedure DrawTitleLine(AIndex:Integer);
  Var St   : String;
      APos : Integer;
  Begin
    St:=Text[AIndex];
    APos:=AIndex*FontH+tmpFrameWidth;
    if IOnTop then Inc(APos,ShapeBounds.Top)
              else APos:=ShapeBounds.Bottom-FontH-APos;
    if Alignment=taRightJustify then
       tmpXPosTitle:=ShapeBounds.Right-ParentChart.Canvas.TextWidth(St)-(tmpMargin div 2)
    else
    if Alignment=taCenter then
       tmpXPosTitle:=((ShapeBounds.Left+ShapeBounds.Right) div 2)-(ParentChart.Canvas.TextWidth(St) div 2);
    With ParentChart.Canvas do
    if SupportsFullRotation then TextOut3D(tmpXPosTitle,APos,0,St)
                            else TextOut(tmpXPosTitle,APos,St);
  end;

Var t               : Integer;
    tmpMaxWidth     : Integer;
    tmp             : Integer;
    tmpFrameVisible : Boolean;
Begin
  if Visible and (Text.Count>0) then
  begin
    tmpFrameVisible:=Frame.Visible and (Frame.Color<>clTeeColor);
    if tmpFrameVisible then tmpFrameWidth:=Frame.Width
                       else tmpFrameWidth:=0;

    if Bevel<>bvNone then tmpFrameWidth:=BevelWidth;

    if not FCustomPosition then
    begin
      ShapeBounds:=ParentChart.ChartRect;
      if IOnTop then ShapeBounds.Top:=ShapeBounds.Top+tmpFrameWidth;
    end;

    With ParentChart.Canvas do
    begin
      AssignFont(Self.Font);
      TextAlign:=ta_Left;
      FontH:=FontHeight;
    end;

    if IOnTop or FCustomPosition then
       ShapeBounds.Bottom:=ShapeBounds.Top+Text.Count*FontH
    else
       ShapeBounds.Top   :=ShapeBounds.Bottom-Text.Count*FontH;

    if not FCustomPosition then
       InflateRect(ShapeBounds,tmpFrameWidth,tmpFrameWidth);

    tmpMargin:=ParentChart.Canvas.TextWidth('W');

    if AdjustFrame then
    Begin
      tmpMaxWidth:=0;
      for t:=0 to Text.Count-1 do
      Begin
        tmp:=ParentChart.Canvas.TextWidth(Text[t]);
        if tmp>tmpMaxWidth then tmpMaxWidth:=tmp;
      end;
      Inc(tmpMaxWidth,tmpMargin+tmpFrameWidth);
      With ShapeBounds do
      Case Alignment of
          taLeftJustify  : Right:=Left +tmpMaxWidth;
          taRightJustify : Left :=Right-tmpMaxWidth;
          taCenter       : begin
                             if FCustomPosition then Right:=Left+tmpMaxWidth;
                             tmp:=(Left+Right) div 2;
                             Left :=tmp-(tmpMaxWidth div 2);
                             Right:=tmp+(tmpMaxWidth div 2);
                           end;
      end;
    end;
    Draw;

    if Alignment=taLeftJustify then tmpXPosTitle:=ShapeBounds.Left+(tmpMargin div 2);

    ParentChart.Canvas.BackMode:=cbmTransparent;
    for t:=0 to Text.Count-1 do DrawTitleLine(t);

    if not FCustomPosition then
    begin
      tmp:=TeeTitleFootDistance+tmpFrameWidth;
      if not Transparent then Inc(tmp,ShadowSize);
      With ParentChart.ChartRect do
      if IOnTop then Top:=ShapeBounds.Bottom+tmp
                else Bottom:=ShapeBounds.Bottom-tmp-Text.Count*FontH;
      ParentChart.RecalcWidthHeight;
    end;
  end;
end;

Function TChartTitle.GetShapeBounds:TRect;
begin
  result:=ShapeBounds;
end;

Procedure TChartTitle.SetAdjustFrame(Value:Boolean);
begin
  TTeePanelAccess(ParentChart).SetBooleanProperty(FAdjustFrame,Value);
end;

Procedure TChartTitle.SetAlignment(Value:TAlignment);
Begin
  if FAlignment<>Value then
  begin
    FAlignment:=Value;
    Repaint;
  end;
end;

Procedure TChartTitle.SetText(Value:TStrings);
begin
  FText.Assign(Value);
  Repaint;
end;

{ TChartFootTitle }
Constructor TChartFootTitle.Create(AOwner: TCustomTeePanel);
Begin
  inherited;
  IOnTop:=False;
  With Font do
  Begin
    Color:=clRed;
    Style:=[fsItalic];
  end;
  With TTeeFontAccess(Font) do
  begin
    IDefColor:=clRed;
    IDefStyle:=[fsItalic];
  end;
end;

{ TCustomChart }
Constructor TCustomChart.Create(AOwner: TComponent);
Begin
  inherited;
  AutoRepaint:=False;
  FTitle   :=TChartTitle.Create(Self);
  if csDesigning in ComponentState then FTitle.FText.Add(ClassName);

  FScrollMouse:=mbRight;

  FSubTitle:=TChartTitle.Create(Self);
  FFoot    :=TChartFootTitle.Create(Self);
  FSubFoot :=TChartFootTitle.Create(Self);

  FBackWall:=TChartBackWall.Create(Self);
  With FBackWall do
  begin
    InitColor(clSilver);
    Transparent:=True;
  end;

  FLeftWall:=TChartWall.Create(Self);
  FLeftWall.InitColor($80FFFF); { ChartMarkColor }

  FLegend:=TChartLegend.Create(Self);

  FBottomWall:=TChartWall.Create(Self);
  FBottomWall.InitColor(clWhite);

  FRightWall:=TChartRightWall.Create(Self);
  With FRightWall do
  begin
    Visible:=False;
    InitColor(clSilver);
  end;

  RestoredAxisScales:=True;
  AutoRepaint :=True;
end;

Destructor TCustomChart.Destroy;
Begin
  FSubTitle.Free;
  FTitle.Free;
  FSubFoot.Free;
  FFoot.Free;
  FBackWall.Free;
  FBottomWall.Free;
  FLeftWall.Free;
  FRightWall.Free;
  FLegend.Free;
  inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TCustomChart.Load(var S: TOldStream; AOwner: TComponent);
begin
   S.Read(ChartStoreVersion,SizeOf(ChartStoreVersion));           {  Chart Store version }
   inherited;

   FTitle:=TChartTitle.Load(S,Self);
   if csDesigning in ComponentState then
      FTitle.FText.Add(ClassName);

   FScrollMouse:=mbRight;

   FSubTitle:=TChartTitle.Load(S,Self);
   FFoot:=TChartFootTitle.Load(S,Self);
   FSubFoot:=TChartFootTitle.Load(S,Self);

   FBackWall:=TChartBackWall.Load(S,Self);
   FLeftWall:=TChartWall.Load(S,Self);

   FLegend:=TChartLegend.Load(S,Self);

   FBottomWall:=TChartWall.Load(S,Self);
   FRightWall:=TChartRightWall.Load(S,Self);

   RestoredAxisScales:=TRUE;
   AutoRepaint:=TRUE;

   // Additional operation
   Parent:=TWinControl(AOwner);
end;

Procedure TCustomChart.Store(var S: TOldStream);
begin
   S.Write(co_ChartStoreVersion,SizeOf(co_ChartStoreVersion));
   inherited Store(S);
   
   FTitle.Store(S);

   FSubTitle.Store(S);
   FFoot.Store(S);
   FSubFoot.Store(S);

   FBackWall.Store(S);
   FLeftWall.Store(S);

   FLegend.Store(S);

   FBottomWall.Store(S);
   FRightWall.Store(S);
end;

Function TCustomChart.CreateDeterminant(Determinant: String): String;
begin
   Result:=FBackWall.CreateDeterminant(Determinant);
   Result:=FLeftWall.CreateDeterminant(Result);
   Result:=FBottomWall.CreateDeterminant(Result);
   Result:=FRightWall.CreateDeterminant(Result);
end;                                                          

Function TCustomChart.OnCompareItems(Item: TComponent): Boolean;
begin
   Result:=FALSE;
   try
      if Item is TCustomChart then
         with TCustomChart(Item) do
              if Self.FBackWall.OnCompareItems(FBackWall) then
                 if Self.FLeftWall.OnCompareItems(FLeftWall) then
                    if Self.FBottomWall.OnCompareItems(FBottomWall) then
                       if Self.FRightWall.OnCompareItems(FRightWall) then
                          Result := inherited OnCompareItems(Item);
   except
   end;
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Function TCustomChart.FormattedValueLegend(ASeries:TChartSeries; ValueIndex:Integer):String;
Begin
  if Assigned(ASeries) then result:=FLegend.FormattedValue(ASeries,ValueIndex)
		       else result:='';
end;

Function TCustomChart.FormattedLegend(SeriesOrValueIndex:Integer):String;
Begin
  result:=FLegend.FormattedLegend(SeriesOrValueIndex);
  if Assigned(FOnGetLegendText) then
     FOnGetLegendText(Self,FLegend.ILegendStyle,SeriesOrValueIndex,result);
end;

procedure TCustomChart.SetLegend(Value:TChartLegend);
begin
  FLegend.Assign(Value);
end;

Function TCustomChart.SaveScales:TAllAxisSavedScales;

 { "remember" the axis scales when zooming, to restore if unzooming }
  Procedure SaveAxisScales(Axis:TChartAxis; Var tmp:TAxisSavedScales);
  begin
    tmp.Auto:=Axis.Automatic;
    tmp.AutoMin:=Axis.AutomaticMinimum;
    tmp.AutoMax:=Axis.AutomaticMaximum;
    tmp.Min:=Axis.Minimum;
    tmp.Max:=Axis.Maximum;
  end;

begin
  With Result do
  begin
    SaveAxisScales(TopAxis,Top);
    SaveAxisScales(BottomAxis,Bottom);
    SaveAxisScales(LeftAxis,Left);
    SaveAxisScales(RightAxis,Right);
  end;
end;

Procedure TCustomChart.RestoreScales(Const Saved:TAllAxisSavedScales);

  { restore the "remembered" axis scales when unzooming }
  Procedure RestoreAxisScales(Axis:TChartAxis; Const tmp:TAxisSavedScales);
  begin
    With Axis do
    begin
      Automatic:=tmp.Auto;
      AutomaticMinimum:=tmp.AutoMin;
      AutomaticMaximum:=tmp.AutoMax;
      if not Automatic then SetMinMax(tmp.Min,tmp.Max);
    end;
  end;

begin
  With Saved do
  begin
    RestoreAxisScales(TopAxis,Top);
    RestoreAxisScales(BottomAxis,Bottom);
    RestoreAxisScales(LeftAxis,Left);
    RestoreAxisScales(RightAxis,Right);
  end;
end;

procedure TCustomChart.SetBackWall(Value:TChartBackWall);
begin
  FBackWall.Assign(Value);
end;

Function TCustomChart.GetFrame:TChartPen;
begin
  if Assigned(FBackWall) then result:=FBackWall.Pen
			 else result:=nil;
end;

Procedure TCustomChart.SetFrame(Value:TChartPen);
begin
  FBackWall.Pen.Assign(Value);
end;

Function TCustomChart.GetBackColor:TColor;
begin
  if FBackWall.Transparent then result:=Color
                           else result:=FBackWall.Color;
end;

Procedure TCustomChart.SetBackColor(Value:TColor);
begin
  FBackWall.Color:=Value;
  { fix 4.01: do not set backwall solid when loading dfms... }
  if Assigned(Parent) and (not (csLoading in ComponentState)) then
     FBackWall.Brush.Style:=bsSolid;
end;

Function TCustomChart.IsFreeSeriesColor(AColor:TColor; CheckBackground:Boolean):Boolean;
var t        : Integer;
    tmpColor : TColor;
Begin
  for t:=0 to SeriesCount-1 do
  Begin
    tmpColor:=Series[t].SeriesColor;
    if (tmpColor=AColor) or
       (CheckBackground and
       ( (AColor=Color) or (AColor=BackWall.Color) )) then
    Begin
      result:=False;
      exit;
    end;
  end;
  result:=(not CheckBackground) or ( (AColor<>Color) and (AColor<>BackWall.Color) );
end;

procedure TCustomChart.SetLeftWall(Value:TChartWall);
begin
  FLeftWall.Assign(Value);
end;

procedure TCustomChart.SetBottomWall(Value:TChartWall);
begin
  FBottomWall.Assign(Value);
end;

procedure TCustomChart.SetRightWall(Value:TChartRightWall);
begin
  FRightWall.Assign(Value);
end;

Procedure TCustomChart.DrawRightWall;
begin
  if ActiveSeriesUseAxis and View3D and View3DWalls then
  begin
    if FRightWall.Visible then
    begin
      PrepareWallCanvas(FRightWall);
      With FRightWall,ChartRect do
      if FSize>0 then
         Canvas.Cube( Right,Right+FSize,
                      Top,Bottom+CalcWallSize(BottomAxis),0,Width3D+FBackWall.Size,
                      ApplyDark3D)
      else
         Canvas.RectangleZ(Right,Top,Bottom+CalcWallSize(BottomAxis),0,Width3D+1);
    end;
  end;
end;

Function TCustomChart.DrawRightWallAfter:Boolean;
var P1 : TPoint;
    P2 : TPoint;
begin
  With ChartRect do
  begin
    P1:=Canvas.Calculate3DPosition(Right,Top,0);
    P2:=Canvas.Calculate3DPosition(Right,Bottom+CalcWallSize(BottomAxis),Width3D+FBackWall.Size);
  end;
  result:=P1.X<=P2.X;
end;

procedure TCustomChart.DrawTitlesAndLegend;

  Procedure DrawTitleFoot;
  Begin
    FTitle.DrawTitle;
    FSubTitle.DrawTitle;
    FFoot.DrawTitle;
    FSubFoot.DrawTitle;
  end;

  Function ShouldDrawLegend:Boolean;
  begin
    result:=Legend.Visible And (Legend.CheckBoxes or (CountActiveSeries>0))
  end;

  { draw title and foot, or draw foot and title, depending
    if legend is at left/right or at top/bottom. }
  { top/bottom legends need to leave space for the title and foot
    before they get displayed.  }
  { If the Legend.CustomPosition is True, then draw the Legend AFTER
    all Series and Axes (on top of chart) }
begin
  Canvas.FrontPlaneBegin;
  if BeforeSeries then
  begin
    if (not Legend.CustomPosition) and ShouldDrawLegend then
    begin
      if Legend.Vertical then
      begin
        Legend.DrawLegend;
        DrawTitleFoot;
      end
      else
      begin
        DrawTitleFoot;
        Legend.DrawLegend;
      end;
    end
    else DrawTitleFoot;
  end
  else
  if Legend.CustomPosition and ShouldDrawLegend then Legend.DrawLegend;
  Canvas.FrontPlaneEnd;
  if (not BeforeSeries) and DrawRightWallAfter then DrawRightWall;
end;

Function TCustomChart.CalcWallSize(Axis:TChartAxis):Integer;
begin
  result:=0;
  if View3D and View3DWalls then
  begin
    if Axis=LeftAxis then   result:=FLeftWall.FSize   else
    if Axis=BottomAxis then result:=FBottomWall.FSize
  end;
end;

Function TCustomChart.ActiveSeriesUseAxis:Boolean;
var t : Integer;
begin
  result:=True;
  for t:=0 to SeriesCount-1 do
  With Series[t] do
  if Active then
     if UseAxis then
     begin
       result:=True;
       Exit;
     end
     else result:=False;
end;

Procedure TCustomChart.PrepareWallCanvas(AWall:TChartWall);
begin
  With AWall do
  begin
    Canvas.AssignVisiblePen(Frame);
    if Transparent then Canvas.Brush.Style:=bsClear
                   else SetBrushCanvas(Color,Brush,Brush.Color)
  end;
end;

Procedure TCustomChart.DrawWalls;  { Left and Bottom wall only }
var tmpRect : TRect;

  Procedure CheckGradient;
  Var R2 : TRect;

     Procedure Check(APoint:TPoint);
     begin
       With APoint do
       begin
         if X<R2.Left then R2.Left:=X else if X>R2.Right then R2.Right:=X;
         if Y<R2.Top then R2.Top:=Y else if Y>R2.Bottom then R2.Bottom:=Y;
       end;
     end;

  Var P : TFourPoints;
  begin
    if (not BackWall.Transparent) and
       BackWall.Gradient.Visible then
    begin
      if View3DOptions.Orthogonal then
         BackWall.Gradient.Draw(Canvas,Canvas.CalcRect3D(tmpRect,Width3D),0)
      else
      begin
        P[0]:=Canvas.Calculate3DPosition(tmpRect.Left,tmpRect.Top,Width3D);
        P[1]:=Canvas.Calculate3DPosition(tmpRect.Right,tmpRect.Top,Width3D);
        P[2]:=Canvas.Calculate3DPosition(tmpRect.Right,tmpRect.Bottom,Width3D);
        P[3]:=Canvas.Calculate3DPosition(tmpRect.Left,tmpRect.Bottom,Width3D);

        R2:={$IFDEF D6}Types.{$ENDIF}Rect(P[0].X,P[0].Y,P[0].X,P[0].Y);
        Check(P[1]);
        Check(P[2]);
        Check(P[3]);

        ClipPolygon(Canvas,P,4);
        BackWall.Gradient.Draw(Canvas,R2,0);
        Canvas.UnClipRectangle;
      end;
      Canvas.Brush.Style:=bsClear;
    end;
  end;

Var tmp        : Integer;
    tmpBlend   : TTeeBlend;
Begin
  if ActiveSeriesUseAxis then
  begin
    if not DrawRightWallAfter then DrawRightWall;
    With FBackWall do
    if Visible then
    begin
      PrepareWallCanvas(FBackWall);
      if View3D then
      begin
        if View3DWalls then
        begin
          if Width3D<0 then tmp:=-FSize else tmp:=FSize;
          if FSize>0 then { <-- trick, was BackWall.Color }
          With ChartRect do
          begin
            tmpRect:={$IFDEF D6}Types.{$ENDIF}Rect(Left-CalcWallSize(LeftAxis),Top,
                         Right,Bottom+CalcWallSize(BottomAxis));
            CheckGradient;
            With tmpRect do
                 Canvas.Cube(Left,Right,Top,Bottom,Width3D,Width3D+tmp,ApplyDark3D)
          end
          else
          begin
            tmpRect:=ChartRect;
            CheckGradient;
            Canvas.RectangleWithZ(tmpRect,Width3D);
          end;
        end;
      end
      else
      begin
        With ChartRect do tmpRect:={$IFDEF D6}Types.{$ENDIF}Rect(Left,Top,Succ(Right),Succ(Bottom));
        if (Transparency>0) and (Canvas is TTeeCanvas3D) then
           tmpBlend:=TTeeBlend.Create(TTeeCanvas3D(Canvas),tmpRect)
        else
           tmpBlend:=nil;
        if Gradient.Visible and (not Transparent) then
        begin
          Gradient.Draw(Canvas,tmpRect,0);
          Canvas.Brush.Style:=bsClear;
        end;
        Canvas.DoRectangle(tmpRect);
        if Assigned(tmpBlend) then
        begin
          tmpBlend.DoBlend(Transparency);
          tmpBlend.Free;
        end;
      end;

      if Assigned(BackImage.Graphic) and BackImageInside then
      begin
        tmpRect:=ChartRect;
        if Pen.Visible then InflateRect(tmpRect,0,-1);
        DrawBitmap(tmpRect,Width3D);
      end;
    end;

    if View3D and View3DWalls then
    begin
      if FLeftWall.Visible then
      begin
       PrepareWallCanvas(FLeftWall);
       With FLeftWall,ChartRect do
       if FSize>0 then
          Canvas.Cube( Left-FSize,Left,
                       Top,Bottom+CalcWallSize(BottomAxis),0,Width3D,
                       ApplyDark3D)
       else
          Canvas.RectangleZ(Left,Top,Bottom+CalcWallSize(BottomAxis),0,Width3D);
      end;
      if FBottomWall.Visible then
      begin
       PrepareWallCanvas(FBottomWall);
       With FBottomWall,ChartRect do
       begin
         tmp:=Bottom;
         if Canvas.SupportsFullRotation then Inc(tmp);
         if FSize>0 then
            Canvas.Cube(Left,Right,tmp,tmp+FSize,0,Width3D,ApplyDark3D)
         else
            Canvas.RectangleY(Left,tmp,Right,0,Width3D);
       end;
      end;
    end;
  end;
end;

Procedure TCustomChart.SeriesUp(ASeries:TChartSeries);
var t : Integer;
Begin  { scroll up ASeries in SeriesList. This changes the Z order }
  t:=SeriesList.IndexOf(ASeries);
  if t>0 then ExchangeSeries(t,t-1);
end;

Procedure TCustomChart.SeriesDown(ASeries:TChartSeries);
var t : Integer;
Begin  { scroll down ASeries in SeriesList. This changes the Z order }
  t:=SeriesList.IndexOf(ASeries);
  if t<SeriesCount-1 then ExchangeSeries(t,t+1);
end;

Procedure TCustomChart.NextPage;
Begin
  if (MaxPointsPerPage>0) and (Page<NumPages) then Page:=Page+1;
End;

Procedure TCustomChart.PreviousPage;
Begin
  if (MaxPointsPerPage>0) and (Page>1) then Page:=Page-1;
End;

Procedure TCustomChart.RemoveAllSeries;
Begin
  While SeriesCount>0 do RemoveSeries(Series[0]);
End;

Procedure TCustomChart.DoZoom(Const topi,topf,boti,botf,lefi,leff,rigi,rigf:Double);

  Procedure DoZoomAnimated;

    Procedure ZoomAxis(AAxis:TChartAxis; Const tmpA,tmpB:Double);
    Begin { apply a portion of the desired zoom to achieve animation }
      with AAxis do
        SetMinMax(Minimum+((tmpA-Minimum)/AnimatedZoomFactor),
                  Maximum-((Maximum-tmpB)/AnimatedZoomFactor));
    end;

  Var t : Integer;
  begin
    for t:=1 to Zoom.AnimatedSteps-1 do
    begin
      ZoomAxis(LeftAxis,Lefi,Leff);
      ZoomAxis(RightAxis,Rigi,Rigf);
      ZoomAxis(TopAxis,Topi,Topf);
      ZoomAxis(BottomAxis,Boti,Botf);
      Refresh;
    end;
  end;

Begin
  if RestoredAxisScales then
  begin
    FSavedScales:=SaveScales;
    RestoredAxisScales:=False;
  end;
  if Zoom.Animated then DoZoomAnimated; { animation... }
  { final zoom }
  LeftAxis.SetMinMax(Lefi,Leff);
  RightAxis.SetMinMax(Rigi,Rigf);
  TopAxis.SetMinMax(Topi,Topf);
  BottomAxis.SetMinMax(Boti,Botf);
  Zoomed:=True;
  if Assigned(FOnZoom) then FOnZoom(Self);
end;

Procedure TCustomChart.ZoomRect(Const Rect:TRect);
Begin
  With Zoom do
  Begin
    X0:=Rect.Left;
    Y0:=Rect.Top;
    X1:=Rect.Right;
    Y1:=Rect.Bottom;
    CalcZoomPoints;
  end;
End;

Procedure TCustomChart.ZoomPercent(Const PercentZoom:Double);

  Procedure CalcAxisScale(Axis:TChartAxis; Var tmpA,tmpB:Double);
  Var tmpDelta : Double;
      AMin     : Double;
      AMax     : Double;
  Begin
    AMin:=Axis.Minimum;
    AMax:=Axis.Maximum;
    Axis.CalcMinMax(AMin,AMax);
    tmpDelta:=(AMax-AMin)*(PercentZoom-100.0)*0.01;
    tmpA:=AMin+tmpDelta;
    tmpB:=AMax-tmpDelta;
  end;

var Lefi : Double;
    Leff : Double;
    Rigi : Double;
    Rigf : Double;
    Topi : Double;
    Topf : Double;
    Boti : Double;
    Botf : Double;
Begin  { zoom a given "percent" }
  CalcAxisScale(LeftAxis,Lefi,Leff);
  CalcAxisScale(RightAxis,Rigi,Rigf);
  CalcAxisScale(TopAxis,Topi,Topf);
  CalcAxisScale(BottomAxis,Boti,Botf);
  DoZoom(Topi,Topf,Boti,Botf,Lefi,Leff,Rigi,Rigf);
  Repaint;
end;

Procedure TCustomChart.CalcClickedPart(Pos:TPoint; Var Part:TChartClickedPart);
begin
  Part:=CalcNeedClickedPart(Pos,False);
end;

Function TCustomChart.CalcNeedClickedPart(Pos:TPoint; Needed:Boolean):TChartClickedPart;

  Procedure ClickedAxis(Axis:TChartAxis);
  begin
    if Axis.Clicked(Pos.x,Pos.y) then
    begin
      result.Part:=cpAxis;
      result.AAxis:=Axis;
    end;
  end;

var t : Integer;
begin
  With result do
  begin
    Part:=cpNone;
    PointIndex:=-1;
    ASeries:=nil;
    AAxis:=nil;

    if Legend.Visible then
    begin
      PointIndex:=Legend.Clicked(Pos.X,Pos.Y);
      if PointIndex<>-1 then
      begin
        Part:=cpLegend;
        Exit;
      end;
    end;

    { IMPORTANT: Z order inverted }
    for t:=SeriesCount-1 downto 0 do
    With Self.Series[t] do
    if Active and
       ((not Needed) or
           ( Assigned(OnClick) or Assigned(OnDblClick) or
             Assigned(FOnClickSeries))) then
    Begin
      PointIndex:=Clicked(Pos.X,Pos.Y);
      if PointIndex<>-1 then
      begin
	ASeries:=Series[t];
	Part:=cpSeries;
	Exit;
      end;
      if Marks.Visible then
      begin
        PointIndex:=Marks.Clicked(Pos.X,Pos.Y);
        if PointIndex<>-1 then
        begin
          ASeries:=Series[t];
	  Part:=cpSeriesMarks;
          Exit;
        end;
      end;
    end;

    for t:=0 to Axes.Count-1 do
    begin
      ClickedAxis(Axes[t]);
      if Part=cpAxis then Exit;
    end;

    if Title.Clicked(Pos.X,Pos.Y) then Part:=cpTitle
    else
    if SubTitle.Clicked(Pos.X,Pos.Y) then Part:=cpSubTitle
    else
    if Foot.Clicked(Pos.X,Pos.Y) then Part:=cpFoot
    else
    if SubFoot.Clicked(Pos.X,Pos.Y) then Part:=cpSubFoot
    else
    if {$IFDEF D6}Types.{$ENDIF}PtInRect(ChartRect,Pos) and  { <-- should be PtInCube(ChartRect,0,MaxZ) }
       (CountActiveSeries>0) then
	  Part:=cpChartRect;
  end;
end;

Function TCustomChart.AxisTitleOrName(Axis:TChartAxis):String;
Begin
  result:=Axis.Title.Caption;
  if result='' then
  begin
    if Axis=DepthAxis then result:=TeeMsg_DepthAxis
    else
    With Axis do
    if Horizontal then
       if OtherSide then result:=TeeMsg_TopAxis
		    else result:=TeeMsg_BottomAxis
    else
       if OtherSide then result:=TeeMsg_RightAxis
		    else result:=TeeMsg_LeftAxis;
  end;
End;

type TToolAccess=class(TTeeCustomTool);

procedure TCustomChart.BroadcastMouseEvent(AEvent:TChartMouseEvent;
                    Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var t : Integer;
begin
  for t:=0 to Tools.Count-1 do
  With TToolAccess(Tools[t]) do
    if Active then
    begin
      ChartMouseEvent(AEvent,Button,Shift,X,Y);
      if CancelMouse then break;
    end;
end;

procedure TCustomChart.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var IClicked : Boolean;

    Procedure CheckZoomPanning;
    Begin
      if ActiveSeriesUseAxis then
      begin
        if Zoom.Allow and (Button=Zoom.MouseButton) and
           (Zoom.KeyShift<=Shift) then
        Begin
          if Zoom.Direction=tzdVertical then x:=ChartRect.Left;
          if Zoom.Direction=tzdHorizontal then y:=ChartRect.Top;
          Zoom.Activate(x,y);
          if Zoom.Direction=tzdVertical then Zoom.X1:=ChartRect.Right;
          if Zoom.Direction=tzdHorizontal then Zoom.Y1:=ChartRect.Bottom;

          DrawZoomRectangle;
          IClicked:=True;
        end;
        if (AllowPanning<>pmNone) and (Button=FScrollMouse) and
           (TeeScrollKeyShift<=Shift ) then
        Begin
          IPanning.Activate(x,y);
          IClicked:=True;
        end;
        if IClicked then Cursor:=crDefault;
      end;
    end;

    Procedure CheckBackground;
    begin
      if Assigned(FOnClickBackground) then
      begin
	CancelMouse:=True;
	FOnClickBackGround(Self,Button,Shift,x,y);
	IClicked:=CancelMouse;
      end;
    end;

    Procedure CheckTitle(ATitle:TChartTitle);
    begin
      if Assigned(FOnClickTitle) then
      begin
        CancelMouse:=True;
        FOnClickTitle(Self,ATitle,Button,Shift,x,y);
        IClicked:=CancelMouse;
      end;
      if not IClicked then CheckZoomPanning;
    end;

var tmpPart : TChartClickedPart;
    tmp     : Integer;
Begin
  inherited;
  if CancelMouse or Zoom.Active or IPanning.Active then Exit;
  CancelMouse:=False;
  BroadcastMouseEvent(cmeDown,Button,Shift,X,Y);
  if not CancelMouse then
  begin
    tmpPart:=CalcNeedClickedPart({$IFDEF D6}Types.{$ELSE}Classes.{$ENDIF}Point(x,y),True);
    IClicked:=False;
    Case tmpPart.Part of
      cpLegend: begin
                  if FLegend.CheckBoxes then
                  begin
                    tmp:=FLegend.Clicked(x,y);
                    if tmp<>-1 then
                    begin
                      With SeriesLegend(tmp,False) do Active:=not Active;
                      { implement "radio" style here... }
                      IClicked:=True;
                    end;
                  end;
		  if Assigned(FOnClickLegend) then
		  begin
		    CancelMouse:=True;
		    FOnClickLegend(Self,Button,Shift,x,y);
		    IClicked:=CancelMouse;
		  end;
                end;
      cpSeries: Begin
		  With tmpPart do
                  begin
                    CancelMouse:=False;
                    if ssDouble in Shift then
                    begin
                      if Assigned(ASeries.OnDblClick) then
                         ASeries.OnDblClick(ASeries,PointIndex,Button,Shift,x,y)
                    end
                    else
                    if Assigned(ASeries.OnClick) then
                       ASeries.OnClick(ASeries,PointIndex,Button,Shift,x,y);
                    IClicked:=CancelMouse;
                  end;
		  if Assigned(FOnClickSeries) then
		  begin
		    CancelMouse:=True;
		    FOnClickSeries(Self,tmpPart.ASeries,tmpPart.PointIndex,Button,Shift,x,y);
		    IClicked:=CancelMouse;
		  end;
		  if not IClicked then CheckZoomPanning;
		end;
	cpAxis: begin
		  if Assigned(FOnClickAxis) then
		  begin
		    CancelMouse:=True;
		    FOnClickAxis(Self,TChartAxis(tmpPart.AAxis),Button,Shift,x,y);
		    IClicked:=CancelMouse;
		  end;
		  if not IClicked then CheckZoomPanning;
		end;
       cpTitle: CheckTitle(Title);
       cpFoot: CheckTitle(Foot);
       cpSubTitle: CheckTitle(SubTitle);
       cpSubFoot: CheckTitle(SubFoot);
   cpChartRect: begin
		  CheckBackground;
		  if not IClicked then CheckZoomPanning;
		end;
    end;
    if not IClicked then CheckBackground;
  end;
  CancelMouse:=False;
End;

procedure TCustomChart.MouseMove(Shift: TShiftState; X, Y: Integer);

  Function CheckMouseSeries:Boolean;
  var t : Integer;
  begin
    result:=False;
    for t:=0 to SeriesCount-1 do
    With Series[t] do
    if Active then
    begin
      if (Cursor<>crDefault) and (Clicked(X,Y)<>-1) then
      Begin
	Self.Cursor:=Cursor; { <-- mouse is over a Series point ! }
	result:=True;
	break;
      end;
    end;
  end;

  Procedure ProcessPanning(Axis:TChartAxis; IniPos,EndPos:Integer);
  Var Delta          : Double;
      tmpMin         : Double;
      tmpMax         : Double;
      tmpAllowScroll : Boolean;
  Begin
    With Axis do
    begin
      Delta:=CalcPosPoint(IniPos)-CalcPosPoint(EndPos);
      tmpMin:=Minimum+Delta;
      tmpMax:=Maximum+Delta;
    end;
    tmpAllowScroll:=True;
    if Assigned(FOnAllowScroll) then
       FOnAllowScroll(TChartAxis(Axis),tmpMin,tmpMax,tmpAllowScroll);
    if tmpAllowScroll then Axis.SetMinMax(tmpMin,tmpMax);
  end;

Var Panned : Boolean;

   Procedure PanAxis(AxisHorizontal:Boolean; Const Pos1,Pos2:Integer);
   Var tmpAxis : TChartAxis;
       t       : Integer;
       tmpMode : TPanningMode;
   begin
     if AxisHorizontal then tmpMode:=pmHorizontal
		       else tmpMode:=pmVertical;
     if (Pos1<>Pos2) and
	( (AllowPanning=tmpMode) or (AllowPanning=pmBoth) ) then
     Begin
       for t:=0 to Axes.Count-1 do
       begin
	 tmpAxis:=Axes[t];
	 if not tmpAxis.IsDepthAxis then
	    if (AxisHorizontal and tmpAxis.Horizontal) or
	       ((not AxisHorizontal) and (not tmpAxis.Horizontal)) then
	       ProcessPanning(tmpAxis,Pos2,Pos1);
       end;
       Panned:=True;
     end;
   end;

Begin
  inherited;
  if not (csDesigning in ComponentState) then
     BroadcastMouseEvent(cmeMove,mbLeft,Shift,X,Y);
  if not CancelMouse then
  With Zoom do  { zoom }
  if Active then
  Begin
    if Direction=tzdVertical then x:=ChartRect.Right;
    if Direction=tzdHorizontal then y:=ChartRect.Bottom;
    if (x<>X1) or (y<>Y1) then
    Begin
      DrawZoomRectangle;
      X1:=x;
      Y1:=y;
      DrawZoomRectangle;
    end;
  end
  else
  With IPanning do { scroll }
  if Active then
  Begin
    if {$IFDEF D6}Types.{$ENDIF}PtInRect(ChartRect,{$IFDEF D6}Types.{$ELSE}Classes.{$ENDIF}Point(x,y)) then
    Begin
      if (x<>X1) or (y<>Y1) then
      Begin
	Panned:=False;
	Check;
	if RestoredAxisScales then
	begin
	  FSavedScales:=SaveScales;
	  RestoredAxisScales:=False;
	end;

	PanAxis(True,X,X1);
	PanAxis(False,Y,Y1);
	X1:=x;
	Y1:=y;
	if Panned then
	Begin
	  if Assigned(FOnScroll) then FOnScroll(Self);
	  Invalidate;
	end;
      End;
    end
    else IPanning.Active:=False;
  end
  else
  if not CheckMouseSeries then Cursor:=OriginalCursor;{ <-- guess cursor shape }
end;

{$IFDEF D4}
Procedure TCustomChart.ScrollVerticalAxes(Up:Boolean);
var t   : Integer;
    tmp : Double;
begin
  for t:=0 to Axes.Count-1 do
  with Axes[t] do
  if not Horizontal then
  begin
    tmp:=(Maximum-Minimum)*3.0*0.01;
    if not Up then tmp:=-tmp;
    Scroll(tmp,False);
  end;
end;

function TCustomChart.DoMouseWheel(Shift: TShiftState; WheelDelta: Integer;
      MousePos: TPoint): Boolean;
begin
  result:=inherited DoMouseWheel(Shift,WheelDelta,MousePos);
end;

function TCustomChart.DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean;
begin
  result:=inherited DoMouseWheelDown(Shift,MousePos);
  if TeeUseMouseWheel then ScrollVerticalAxes(True);
end;

function TCustomChart.DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean;
begin
  result:=inherited DoMouseWheelUp(Shift,MousePos);
  if TeeUseMouseWheel then ScrollVerticalAxes(False);
end;
{$ENDIF}

procedure TCustomChart.UndoZoom;
begin
  if not RestoredAxisScales then RestoreScales(FSavedScales);
  RestoredAxisScales:=True;
  inherited;
end;

Procedure TCustomChart.CalcZoomPoints;
Begin
  With Zoom do
  Begin
    Check;
    DoZoom( TopAxis.CalcPosPoint(X0),TopAxis.CalcPosPoint(X1),
	    BottomAxis.CalcPosPoint(X0),BottomAxis.CalcPosPoint(X1),
	    LeftAxis.CalcPosPoint(Y1),LeftAxis.CalcPosPoint(Y0),
	    RightAxis.CalcPosPoint(Y1),RightAxis.CalcPosPoint(Y0));
  end;
End;

procedure TCustomChart.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
Begin
  inherited;
  if Zoom.Active and (Button=Zoom.MouseButton) then
  With Zoom do
  Begin
    Active:=False;
    DrawZoomRectangle;
    Canvas.Pen.Mode:=pmCopy;
    if Direction=tzdVertical then x:=ChartRect.Right;
    if Direction=tzdHorizontal then y:=ChartRect.Bottom;
    X1:=x;
    Y1:=y;
    if (Abs(X1-X0)>MinimumPixels) and (Abs(Y1-Y0)>MinimumPixels) then
    Begin
      if (X1>X0) and (Y1>Y0) then CalcZoomPoints { <-- do zoom in ! }
			     else UndoZoom;      { <-- do zoom out ! }
      Invalidate;
    end;
  end;
  IPanning.Active:=False;
  BroadcastMouseEvent(cmeUp,Button,Shift,X,Y);
End;

Procedure TCustomChart.CalcWallsRect;
var tmp : Integer;
begin
  CalcSize3DWalls;
  { for orthogonal only }
  if View3D and View3DOptions.Orthogonal then
  begin
    if ActiveSeriesUseAxis then tmp:=BackWall.Size
		           else tmp:=0;
    With ChartRect do
    begin
      Dec(Right,Abs(Width3D)+tmp);
      Inc(Top,Abs(Height3D)+tmp);
      if RightWall.Visible then Dec(Right,RightWall.Size+1);
    end;
  end;
  RecalcWidthHeight;
end;

Procedure TCustomChart.Assign(Source: TPersistent);
begin
   if Source is TCustomChart then
      with TCustomChart(Source) do
         begin
            Self.BackWall   :=FBackWall;
            Self.BottomWall :=FBottomWall;
            Self.Foot       :=FFoot;
            Self.LeftWall   :=FLeftWall;
            Self.Legend     :=FLegend;
            Self.RightWall  :=FRightWall;
            Self.FScrollMouse:=FScrollMouse;
            Self.SubFoot    :=FSubFoot;
            Self.SubTitle   :=FSubTitle;
            Self.Title      :=FTitle;
         end;
   inherited Assign(Source);
end;

Procedure TCustomChart.GetChildren(Proc:TGetChildProc; Root:TComponent);
var t : Integer;
begin
  inherited;
  for t:=0 to Tools.Count-1 do Proc(Tools[t]);
end;

Procedure TCustomChart.FillSeriesSourceItems( ASeries:TChartSeries;
					Proc:TGetStrProc);
begin { abstract }
end;

Procedure TCustomChart.FillValueSourceItems( ValueList:TChartValueList;
				       Proc:TGetStrProc);
Var t:Integer;
Begin
  With ValueList.Owner do
  if Assigned(DataSource) and (DataSource is TChartSeries) then
     With TChartSeries(DataSource) do
     for t:=0 to ValuesList.Count-1 do Proc(ValuesList[t].Name);
end;

Function TCustomChart.GetASeries:TChartSeries;
var t : Integer;
Begin
  for t:=0 to SeriesCount-1 do
  if Series[t].Active then
  begin
    result:=SeriesList[t];
    exit;
  end;
  result:=nil;
End;

procedure TCustomChart.SetTitle(Value:TChartTitle);
begin
  FTitle.Assign(Value);
end;

procedure TCustomChart.SetFoot(Value:TChartTitle);
begin
  FFoot.Assign(Value);
end;

procedure TCustomChart.SetSubTitle(const Value: TChartTitle);
begin
  FSubTitle.Assign(Value);
end;

procedure TCustomChart.SetSubFoot(const Value: TChartTitle);
begin
  FSubFoot.Assign(Value);
end;

procedure TCustomChart.ReadBackColor(Reader: TReader);
var tmp    : String;
    L      : Byte;
    tmpVal : TValueType;
    tmpNum : Integer;
begin
  With Reader do
  begin
    tmpVal:=ReadValue;
    Case tmpVal of
    vaIdent: begin
               Read(L, SizeOf(Byte));
               SetString(tmp, PChar(nil), L);
               Read(tmp[1], L);
               BackColor:=StringToColor(tmp);
             end;
    vaInt8,
    vaInt16,
    vaInt32: begin
               Read(tmpNum, SizeOf(tmpNum));
               BackColor:=tmpNum;
             end;
    end;
  end;
end;

procedure TCustomChart.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('BackColor',ReadBackColor,nil,False);
end;

{ TChartLegendGradient }
Constructor TChartLegendGradient.Create(ChangedEvent: TNotifyEvent);
begin
  inherited;
  Direction:=gdRightLeft;
  EndColor:=clWhite;
  StartColor:=clSilver;
end;

{ TTeeCustomShapePosition }
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TTeeCustomShapePosition.Load(var S: TOldStream; AOwner: TCustomTeePanel);
begin
   inherited;
   S.Read(FCustomPosition,SizeOf(Boolean));
end;

Procedure TTeeCustomShapePosition.Store(var S: TOldStream);
begin
   inherited;
   S.Write(FCustomPosition,SizeOf(Boolean));
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TTeeCustomShapePosition.SetCustomPosition(Const Value:Boolean);
Begin
  TTeePanelAccess(ParentChart).SetBooleanProperty(FCustomPosition,Value);
end;

Procedure TTeeCustomShapePosition.SetLeft(Const Value:Integer);
Begin
  FCustomPosition:=True;
  TTeePanelAccess(ParentChart).SetIntegerProperty(ShapeBounds.Left,Value);
end;

Procedure TTeeCustomShapePosition.SetTop(Const Value:Integer);
Begin
  FCustomPosition:=True;
  TTeePanelAccess(ParentChart).SetIntegerProperty(ShapeBounds.Top,Value);
end;

Procedure TTeeCustomShapePosition.Assign(Source:TPersistent);
Begin
  if Source is TTeeCustomShapePosition then
  With TTeeCustomShapePosition(Source) do
  Begin
    Self.FCustomPosition:=FCustomPosition;
    if Self.FCustomPosition then
    begin
      Self.Left :=Left;
      Self.Top  :=Top;
    end;
  end;
  inherited;
end;

function TTeeCustomShapePosition.GetLeft: Integer;
begin
  result:=ShapeBounds.Left;
end;

function TTeeCustomShapePosition.GetTop: Integer;
begin
  result:=ShapeBounds.Top;
end;

{ TLegendSymbol }
Constructor TLegendSymbol.Create(ALegend:TCustomChartLegend);
begin
   inherited Create;
   ILegend:=ALegend;
   FWidth:=TeeDefaultLegendSymbolWidth;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TLegendSymbol.Load(var S: TOldStream; ALegend: TCustomChartLegend);
begin
   inherited Create;
   ILegend:=ALegend;
   S.Read(FContinuous,SizeOf(Boolean));
   S.Read(FPosition,SizeOf(TLegendSymbolPosition));
   S.Read(FWidthUnits,SizeOf(TLegendSymbolSize));
   S.Read(FWidth,SizeOf(Integer));
end;

Procedure TLegendSymbol.Store(var S: TOldStream);
begin
   S.Write(FContinuous,SizeOf(Boolean));
   S.Write(FPosition,SizeOf(TLegendSymbolPosition));
   S.Write(FWidthUnits,SizeOf(TLegendSymbolSize));
   S.Write(FWidth,SizeOf(Integer));
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TLegendSymbol.Assign(Source: TPersistent);
Begin
  if Source is TLegendSymbol then
  With TLegendSymbol(Source) do
  Begin
    Self.Continuous:=Continuous;
    Self.Position  :=Position;
    Self.Width     :=Width;
    Self.WidthUnits:=WidthUnits;
  end;
end;

Function TLegendSymbol.CalcWidth(Value:Integer):Integer;
begin
  if FWidthUnits=lcsPercent then result:=Round(Width*Value*0.01)
                            else result:=Width;
end;

procedure TLegendSymbol.SetWidth(Const Value: Integer);
begin
  TTeePanelAccess(ILegend.ParentChart).SetIntegerProperty(FWidth,Value);
end;

procedure TLegendSymbol.SetWidthUnits(const Value: TLegendSymbolSize);
begin
  if FWidthUnits<>Value then
  begin
    FWidthUnits:=Value;
    ILegend.Repaint;
  end;
end;

procedure TLegendSymbol.SetPosition(Const Value: TLegendSymbolPosition);
begin
  if FPosition<>Value then
  begin
    FPosition:=Value;
    ILegend.Repaint;
  end;
end;

procedure TLegendSymbol.SetContinuous(const Value: Boolean);
begin
  TTeePanelAccess(ILegend.ParentChart).SetBooleanProperty(FContinuous,Value);
end;

{ TCustomChartLegend }
Const TeeLegendOff2  = 2;
      TeeLegendOff4  = 4;

Constructor TCustomChartLegend.Create(AOwner: TCustomChart);
begin
   inherited Create(AOwner);
   ILegendStyle:=lsAuto;
   ILastValue:=TeeAllValues;

   FAlignment:=laRight;
   FCurrentPage:=True;
   FSymbol:=TLegendSymbol.Create(Self);

   FDividingLines:=TChartHiddenPen.Create(ParentChart.CanvasChanged);
   FLegendStyle:=lsAuto;
   FMaxNumRows:=10;
   FTextStyle:=ltsLeftValue;
   FTopLeftPos:=10; { % }
   FResizeChart:=True;
   ColumnWidthAuto:=True;
end;

Destructor TCustomChartLegend.Destroy;
begin
   FSymbol.Free;
   FDividingLines.Free;
   inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TCustomChartLegend.Load(var S: TOldStream; AOwner: TCustomChart);
begin
   inherited Load(S,AOwner);

   S.Read(FCheckBoxes,SizeOf(Boolean));
   S.Read(FFirstValue,SizeOf(Integer));
   S.Read(FFontSeriesColor,SizeOf(Boolean));
   S.Read(FHorizMargin,SizeOf(Integer));
   S.Read(FInverted,SizeOf(Boolean));
   S.Read(FVertMargin,SizeOf(Integer));
   S.Read(FVertSpacing,SizeOf(Integer));

   S.Read(ILegendStyle,SizeOf(TLegendStyle));
   S.Read(ILastValue,SizeOf(Integer));

   S.Read(FAlignment,SizeOf(TLegendAlignment));
   S.Read(FCurrentPage,SizeOf(Boolean));
   FSymbol:=TLegendSymbol.Load(S,Self);

   FDividingLines:=TChartHiddenPen.Load(S,ParentChart.CanvasChanged);
   S.Read(FLegendStyle,SizeOf(TLegendStyle));
   S.Read(FMaxNumRows,SizeOf(Integer));
   S.Read(FTextStyle,SizeOf(TLegendTextStyle));
   S.Read(FTopLeftPos,SizeOf(Integer));                  { % }
   S.Read(FResizeChart,SizeOf(Boolean));
   S.Read(ColumnWidthAuto,SizeOf(Boolean));
end;

Procedure TCustomChartLegend.Store(var S: TOldStream);
begin
   inherited;

   S.Write(FCheckBoxes,SizeOf(Boolean));
   S.Write(FFirstValue,SizeOf(Integer));
   S.Write(FFontSeriesColor,SizeOf(Boolean));
   S.Write(FHorizMargin,SizeOf(Integer));
   S.Write(FInverted,SizeOf(Boolean));
   S.Write(FVertMargin,SizeOf(Integer));
   S.Write(FVertSpacing,SizeOf(Integer));

   S.Write(ILegendStyle,SizeOf(TLegendStyle));
   S.Write(ILastValue,SizeOf(Integer));

   S.Write(FAlignment,SizeOf(TLegendAlignment));
   S.Write(FCurrentPage,SizeOf(Boolean));
   FSymbol.Store(S);

   FDividingLines.Store(S);
   S.Write(FLegendStyle,SizeOf(TLegendStyle));
   S.Write(FMaxNumRows,SizeOf(Integer));
   S.Write(FTextStyle,SizeOf(TLegendTextStyle));
   S.Write(FTopLeftPos,SizeOf(Integer));                  { % }
   S.Write(FResizeChart,SizeOf(Boolean));
   S.Write(ColumnWidthAuto,SizeOf(Boolean));
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TCustomChartLegend.Assign(Source:TPersistent);
Begin
  if Source is TCustomChartLegend then
  With TCustomChartLegend(Source) do
  Begin
    Self.FAlignment   :=FAlignment;
    Self.FCheckBoxes  :=FCheckBoxes;
    Self.FCurrentPage :=FCurrentPage;
    Self.DividingLines:=DividingLines;
    Self.FFirstValue  :=FFirstValue;
    Self.FFontSeriesColor:=FFontSeriesColor;
    Self.FHorizMargin :=FHorizMargin;
    Self.FInverted    :=FInverted;
    Self.FLegendStyle :=FLegendStyle;
    Self.FMaxNumRows  :=FMaxNumRows;
    Self.FResizeChart :=FResizeChart;
    Self.Symbol       :=Symbol;
    Self.FTextStyle   :=FTextStyle;
    Self.FTopLeftPos  :=FTopLeftPos;
    Self.FVertMargin  :=FVertMargin;
    Self.FVertSpacing :=FVertSpacing;
  end;
  inherited;
end;

Procedure TCustomChartLegend.SetAlignment(Const Value:TLegendAlignment);
Begin
  if FAlignment<>Value then
  begin
    FAlignment:=Value;
    Repaint;
  end;
end;

Procedure TCustomChartLegend.SetFirstValue(Const Value:Integer);
Begin
  if Value<0 then
     Raise LegendException.Create(TeeMsg_LegendFirstValue)
  else
     TTeePanelAccess(ParentChart).SetIntegerProperty(FFirstValue,Value);
End;

Procedure TCustomChartLegend.SetHorizMargin(Const Value:Integer);
Begin
  TTeePanelAccess(ParentChart).SetIntegerProperty(FHorizMargin,Value);
end;

Procedure TCustomChartLegend.SetInverted(Const Value:Boolean);
Begin
  TTeePanelAccess(ParentChart).SetBooleanProperty(FInverted,Value);
end;

Procedure TCustomChartLegend.SetCurrentPage(Const Value:Boolean);
Begin
  TTeePanelAccess(ParentChart).SetBooleanProperty(FCurrentPage,Value);
end;

procedure TCustomChartLegend.SetFontSeriesColor(const Value: Boolean);
begin
  TTeePanelAccess(ParentChart).SetBooleanProperty(FFontSeriesColor,Value);
end;

Procedure TCustomChartLegend.SetLegendStyle(Const Value:TLegendStyle);
Begin
  if FLegendStyle<>Value then
  begin
    FLegendStyle:=Value;
    CalcLegendStyle; { <-- force recalc of ILegendStyle }
    Repaint;
  end;
end;

Procedure TCustomChartLegend.SetMaxNumRows(Const Value:Integer);
Begin
  TTeePanelAccess(ParentChart).SetIntegerProperty(FMaxNumRows,Value);
end;

Procedure TCustomChartLegend.SetResizeChart(Const Value:Boolean);
Begin
  TTeePanelAccess(ParentChart).SetBooleanProperty(FResizeChart,Value);
end;

Procedure TCustomChartLegend.SetCheckBoxes(Const Value:Boolean);
Begin
  TTeePanelAccess(ParentChart).SetBooleanProperty(FCheckBoxes,Value);
end;

Procedure TCustomChartLegend.SetTextStyle(Const Value: TLegendTextStyle);
Begin
  if FTextStyle<>Value then
  begin
    FTextStyle:=Value;
    Repaint;
  end;
end;

Procedure TCustomChartLegend.SetTopLeftPos(Const Value:Integer);
Begin
  TTeePanelAccess(ParentChart).SetIntegerProperty(FTopLeftPos,Value);
end;

Procedure TCustomChartLegend.SetVertMargin(Const Value:Integer);
Begin
  TTeePanelAccess(ParentChart).SetIntegerProperty(FVertMargin,Value);
end;

Procedure TCustomChartLegend.SetVertSpacing(Const Value:Integer);
Begin
  TTeePanelAccess(ParentChart).SetIntegerProperty(FVertSpacing,Value);
end;

Procedure TCustomChartLegend.SetSymbolWidth(Const Value:Integer);
Begin
  FSymbol.Width:=Value;
end;

Procedure TCustomChartLegend.CalcLegendStyle;
begin
  if FLegendStyle=lsAuto then
  begin
    if FCheckBoxes or (TCustomChart(ParentChart).CountActiveSeries>1) then
       ILegendStyle:=lsSeries
    else
       ILegendStyle:=lsValues;
  end
  else ILegendStyle:=FLegendStyle;
end;

Function TCustomChartLegend.FormattedLegend(SeriesOrValueIndex:Integer):String;
Begin
  With TCustomChart(ParentChart) do
  Case ILegendStyle of
    lsSeries:     result:=SeriesTitleLegend(SeriesOrValueIndex,not FCheckBoxes);
    lsValues:     result:=FormattedValueLegend( GetLegendSeries,SeriesOrValueIndex );
    lsLastValues: result:=FormattedValueLegend( Series[SeriesOrValueIndex],
						Series[SeriesOrValueIndex].Count-1);
  end;
end;

Procedure TCustomChartLegend.SetDividingLines(Const Value:TChartHiddenPen);
begin
  FDividingLines.Assign(Value);
end;

Procedure TCustomChartLegend.SetSymbol(Const Value:TLegendSymbol);
begin
  FSymbol.Assign(Value);
end;

Function TCustomChartLegend.GetVertical:Boolean;
begin
  result:=(Alignment=laLeft) or (Alignment=laRight)
end;

Function TCustomChartLegend.GetShapeBounds:TRect;
begin
  result:=ShapeBounds;
end;

Function TCustomChartLegend.GetSymbolWidth:Integer;
begin
  result:=FSymbol.Width
end;

Function TCustomChartLegend.GetLegendSeries:TChartSeries;
begin
  result:=FSeries;
  if not Assigned(result) then result:=TCustomChart(ParentChart).GetASeries;
end;

Procedure TCustomChartLegend.DrawLegend;

  Procedure DrawSymbol(ASeries:TChartSeries; AIndex:Integer; Const R:TRect);
  begin
    if IColorWidth>0 then
    if Assigned(ASeries) then ASeries.DrawLegend(AIndex,R)
    else
    with ParentChart.Canvas do
    begin
      Brush.Color:=clWhite;
      Brush.Style:=bsSolid;
      DoRectangle(R);
    end;
  end;

type TLegendItemText=Array[0..TeeMaxLegendColumns-1] of String;

Var XLegendText  : Integer;
    XLegendColor : Integer;
    XLegendBox   : Integer;
    tmpMaxWidth  : Integer;
    tmpSeries    : TChartSeries;
    Items        : Array{$IFNDEF D4}[0..100]{$ENDIF} of TLegendItemText;
    ISpaceWidth  : Integer;
    ItemHeight   : Integer;

  Procedure DrawLegendItem(ItemIndex,ItemOrder:Integer);
  type TSetLegendTextStyle=Set of TLegendTextStyle;
  Var PosXLegend : Integer;
      t          : Integer;
      IncPos     : Boolean;

    Procedure SetRightAlign(Const ASet:TSetLegendTextStyle);
    begin
      if FTextStyle in ASet then
      begin
        ParentChart.Canvas.TextAlign:=ta_Right;
        Inc(posXLegend,ColumnWidths[t]);
        IncPos:=False;
      end
      else ParentChart.Canvas.TextAlign:=ta_Left;
    end;

  Var PosYLegend : Integer;
      tmp        : Integer;
      tmpBox     : Integer;
      R          : TRect;
      tmpSt      : String;
      tmpOrder   : Integer;
      PosXColor  : Integer;
      tmpX       : Integer;
  Begin
    if ItemOrder>=ITotalItems then exit;
    with TCustomChart(ParentChart),Canvas do
    begin
      Brush.Color:=Self.Color;
      Brush.Style:=bsClear;

      tmp:=XLegendText;
      tmpX:=0;
      posYLegend:=ShapeBounds.Top+1;
      PosXColor:=XLegendColor;
      if Self.Vertical then tmpOrder:=ItemOrder
      else
      begin
        tmpOrder:=ItemOrder div NumCols;
        tmpX:=(tmpMaxWidth+IColorWidth+TeeLegendOff4+TeeLegendOff2);
        if CheckBoxes then Inc(tmpX,TeeCheckBoxSize+2*TeeLegendOff2);
        tmpX:=tmpX*(ItemOrder mod NumCols);
        Inc(tmp,tmpX);
        Inc(PosXColor,tmpX);
      end;
      Inc(posYLegend,tmpOrder*ItemHeight+(FVertSpacing div 2));
      if Assigned(FOnGetLegendPos) then
         FOnGetLegendPos(TCustomChart(ParentChart),ItemIndex,tmp,posYLegend,PosXColor);

      posXLegend:=tmp;

      if FFontSeriesColor then
      begin
        if (InternalLegendStyle=lsSeries) or
           (InternalLegendStyle=lsLastValues) then
            Font.Color:=SeriesLegend(ItemIndex,not FCheckBoxes).SeriesColor
        else
            Font.Color:=tmpSeries.LegendItemColor(ItemIndex);
      end;

      for t:=0 to TeeMaxLegendColumns-1 do
      begin
        tmpSt:=Items[ItemOrder,t];
        IncPos:=True;
        if ILegendStyle=lsSeries then
           TextAlign:=ta_Left
        else
           if t=0 then
              SetRightAlign([ ltsXValue,
                              ltsValue,
                              ltsPercent,
                              ltsXandValue,
                              ltsXandPercent,
                              ltsLeftPercent,
                              ltsLeftValue ])
           else
           if t=1 then
              SetRightAlign([ ltsRightValue,
                              ltsXandValue,
                              ltsXandPercent,
                              ltsRightPercent ]);
        if tmpSt<>'' then
        begin
          if FCheckBoxes then tmpBox:=PosYLegend+1
                         else tmpBox:=PosYLegend;

          TextOut(posXLegend,tmpBox,tmpSt);  { <-- draw text }
        end;
        if IncPos then Inc(posXLegend,ColumnWidths[t]);
        Inc(posXLegend,ISpaceWidth);
      end;

      R.Left:=PosXColor;
      R.Right:=PosXColor+IColorWidth;
      R.Top:=PosYLegend;
      R.Bottom:=PosYLegend+ItemHeight+1;
      if (not FSymbol.Continuous) or (ItemOrder=0) then Inc(R.Top,2);
      if (not FSymbol.Continuous) or (ItemOrder=(ILastValue-FirstValue)) then
          Dec(R.Bottom,1+2+FVertSpacing);

      if (InternalLegendStyle=lsSeries) or (InternalLegendStyle=lsLastValues) then
      begin
        if FCheckBoxes then
        begin
          tmpBox:=XLegendBox;
          tmpSeries:=SeriesLegend(ItemIndex,False);
          if not Vertical then Inc(tmpBox,tmpX);
          TeeDrawCheckBox( tmpBox,posYLegend+((ItemHeight-FVertSpacing-TeeCheckBoxSize) div 2)-1,
                           ParentChart.Canvas,tmpSeries.Active,Self.Color);
          DrawSymbol(tmpSeries,-1,R);
        end
        else DrawSymbol(ActiveSeriesLegend(ItemIndex),-1,R);
      end
      else
      begin
        if Assigned(tmpSeries) then
           DrawSymbol(tmpSeries,tmpSeries.LegendToValueIndex(ItemIndex),R)
        else
           DrawSymbol(nil,-1,R);
      end;

      if (ItemOrder>0) and (FDividingLines.Visible) then
      begin
	AssignVisiblePen(FDividingLines);
	BackMode:=cbmTransparent;
	if Self.Vertical then
	   DoHorizLine(ShapeBounds.Left,ShapeBounds.Right,PosYLegend-(FVertSpacing div 2) )
	else
           DoVertLine(ShapeBounds.Left+tmpX+TeeLegendOff2,ShapeBounds.Top,ShapeBounds.Bottom);
      end;
    end;
  end;

Var FrameWidth : Integer;

  Function MaxLegendValues(YLegend,ItemHeight:Integer):Integer;

    Function CalcMaxLegendValues(A,B,C:Integer):Integer;
    begin
      With ParentChart do
      Begin
        if (YLegend<A) and (ItemHeight>0) then
        Begin
          result:=((B-2*Self.Frame.Width)-YLegend+C) div ItemHeight;
          result:=MinLong(result,TotalLegendItems);
        end
        else result:=0;
      end;
    end;

  Begin
    With ParentChart do
    if Self.Vertical then
       result:=CalcMaxLegendValues(ChartRect.Bottom,ChartHeight,ChartRect.Top)
    else
       result:=CalcMaxLegendValues(ChartRect.Right,ChartWidth,0);
  end;

  Function CalcTotalItems:Integer;
  var tmpSeries : TChartSeries;
      t         : Integer;
  begin
    result:=0;
    if (InternalLegendStyle=lsSeries) or (InternalLegendStyle=lsLastValues) then
    begin
      for t:=0 to TCustomChart(ParentChart).SeriesCount-1 do
      begin
        With TCustomChart(ParentChart).Series[t] do
        if (FCheckBoxes or Active) and ShowInLegend then Inc(result);
      end;
      Dec(result,FirstValue);
    end
    else
    begin
      tmpSeries:=GetLegendSeries;
      if Assigned(tmpSeries) and (tmpSeries.ShowInLegend) then
         result:=tmpSeries.CountLegendItems-FirstValue;
    end;
    result:=MaxLong(0,result);
  end;

  Procedure DrawItems;
  Var t : Integer;
  begin
    tmpSeries:=GetLegendSeries;
    if Inverted then
       for t:=ILastValue downto FirstValue do DrawLegendItem(t,(ILastValue-t))
    else
       for t:=FirstValue to ILastValue do DrawLegendItem(t,(t-FirstValue));
  end;

  Procedure GetItems;

    Procedure SetItem(AIndex,APos:Integer);
    var tmpSt : String;
        tmp   : Integer;
        i     : Integer;
    begin
      tmpSt:=FormattedLegend(APos);
      tmp:=0;
      Repeat
        i:=AnsiPos(TeeColumnSeparator,tmpSt);
        if i>0 then
        begin
         Items[AIndex,tmp]:=Copy(tmpSt,1,i-1);
         Delete(tmpSt,1,i);
         Inc(tmp);
        end;
      Until (i=0) or (tmpSt='') or (tmp>1);
      if (tmpSt<>'') and (tmp<=1) then Items[AIndex,tmp]:=tmpSt;
    end;

  var t     : Integer;
  begin
    {$IFDEF D4}
    SetLength(Items,ILastValue-FirstValue+1);
    {$ENDIF}
    if Inverted then
       for t:=ILastValue downto FirstValue do SetItem(ILastValue-t,t)
    else
       for t:=FirstValue to ILastValue do SetItem(t-FirstValue,t);
  end;

  Procedure CalcSymbolTextPos(ALeftPos:Integer);
  Var tmp : Integer;
  begin
    tmp:=ALeftPos+TeeLegendOff2+TeeLegendOff4;
    With ShapeBounds do
      if Symbol.Position=spLeft then
      begin
        XLegendColor:=tmp;
        XLegendText :=XLegendColor+IColorWidth+TeeLegendOff4;
      end
      else
      begin
        XLegendText :=tmp;
        XLegendColor:=XLegendText+tmpMaxWidth;
      end;
  end;

  Procedure CalcHorizontalPositions;
  var HalfMaxWidth : Integer;
      tmpW         : Integer;
  begin
    With ParentChart do
    begin
      HalfMaxWidth:=2*TeeLegendOff2+2*TeeLegendOff4+((tmpMaxWidth+IColorWidth)*NumCols)+
                    ((TeeLegendOff2+TeeLegendOff4)*(NumCols-1));
      if FCheckBoxes then Inc(HalfMaxWidth,TeeLegendOff4+(TeeCheckBoxSize+TeeLegendOff2)*NumCols);
      HalfMaxWidth:=MinLong(ChartWidth,HalfMaxWidth);
    end;
    HalfMaxWidth:=HalfMaxWidth div 2;
    With ShapeBounds do
    begin
      if not CustomPosition then
      begin
        tmpW:=Round(1.0*FTopLeftPos*
            (ParentChart.ChartBounds.Right-ParentChart.ChartBounds.Left-HalfMaxWidth)*0.01);
        Left :=ParentChart.ChartXCenter-HalfMaxWidth+tmpW;
      end;
      Right:=Left+(2*HalfMaxWidth);
      tmpW:=Left;
      if CheckBoxes then
      begin
        XLegendBox:=Left+TeeLegendOff4;
        Inc(tmpW,TeeCheckBoxSize+TeeLegendOff4);
      end;
      CalcSymbolTextPos(tmpW);
    end;
  end;

  Procedure GetRectangle;
  Var tmpRect : TRect;
  begin
    tmpRect:=ShapeBounds;
    TCustomChart(ParentChart).FOnGetLegendRect(TCustomChart(ParentChart),tmpRect);
    ShapeBounds:=tmpRect;
  end;

  Function CalcColumnsWidth(NumLegendValues:Integer):Integer;
  var t  : Integer;
      tt : Integer;
  Begin
    if ColumnWidthAuto then
    for t:=0 to TeeMaxLegendColumns-1 do
    begin
      ColumnWidths[t]:=0;
      With ParentChart do
      for tt:=FirstValue to ILastValue do
          ColumnWidths[t]:=MaxLong(ColumnWidths[t],Canvas.TextWidth(Items[tt-FirstValue,t]));
    end;
    result:=ISpaceWidth*TeeMaxLegendColumns-1;
    for t:=0 to TeeMaxLegendColumns-1 do Inc(result,ColumnWidths[t]);
  end;

  Procedure CalcVerticalPositions;
  var tmpTotalWidth : Integer;

    Procedure CalcWidths;
    begin
      tmpMaxWidth:=CalcColumnsWidth(NumRows);
      tmpTotalWidth:=2*TeeLegendOff4+tmpMaxWidth+TeeLegendOff2;
      IColorWidth:=Symbol.CalcWidth(tmpTotalWidth);
      Inc(tmpTotalWidth,IColorWidth);
    end;

  Var tmpW : Integer;
  begin
    With ShapeBounds do
    begin
      if CustomPosition or (Alignment=laLeft) then
      begin
        if not CustomPosition then
        begin
          Left:=ParentChart.ChartRect.Left;
          if Self.Frame.Visible then Inc(Left,FrameWidth+1);
        end;
        CalcWidths;
      end
      else
      begin
        Right:=ParentChart.ChartRect.Right;
        Dec(Right,ShadowSize);
        if Self.Frame.Visible then Dec(Right,FrameWidth+1);
        CalcWidths;
        Left:=Right-tmpTotalWidth;
        if CheckBoxes then Dec(Left,TeeCheckBoxSize+TeeLegendOff4);
      end;
      if FCheckBoxes then
      begin
        XLegendBox:=Left+TeeLegendOff4;
        tmpW:=XLegendBox+TeeCheckBoxSize;
      end
      else tmpW:=Left;
      Right:=tmpW+tmpTotalWidth;
      CalcSymbolTextPos(tmpW);
    end;  
  end;

  Procedure ResizeVertical;
  var tmp : Integer;
  begin
    tmp:=TeeLegendOff2+ItemHeight*NumRows;
    With ShapeBounds do
    if (Self.Alignment=laBottom) and (not CustomPosition) then
       Top:=Bottom-tmp
    else
       Bottom:=Top+tmp;
  end;

  Procedure ResizeChartRect;

    Function CalcMargin(AMargin,ADefault,ASize:Integer):Integer;
    begin
      if AMargin=0 then result:=MulDiv(ADefault,ASize,100)
       	           else result:=AMargin;
    end;

  var tmp : Integer;
  begin
    With ParentChart do
    begin
      if ResizeChart and (not CustomPosition) then
      Case Alignment of
        laLeft   : ChartRect.Left  :=ShapeBounds.Right;
        laRight  : ChartRect.Right :=ShapeBounds.Left;
        laTop    : ChartRect.Top   :=ShapeBounds.Bottom+ShadowSize;
        laBottom : ChartRect.Bottom:=ShapeBounds.Top;
      end;
      if Vertical then
      begin
        tmp:=CalcMargin(HorizMargin,TeeDefHorizMargin,ChartWidth);
        if Alignment=laLeft then Inc(ChartRect.Left,tmp)
                            else Dec(ChartRect.Right,tmp);
      end
      else
      begin
        tmp:=CalcMargin(VertMargin,TeeDefVerticalMargin,ChartHeight);
        if Alignment=laTop then Inc(ChartRect.Top,tmp)
                           else Dec(ChartRect.Bottom,tmp);
      end;
      ReCalcWidthHeight;
    end;
  end;

Var IsPage : Boolean;
Begin
  CalcLegendStyle;
  With TCustomChart(ParentChart) do
  Begin
    IsPage:=(ILegendStyle=lsValues) and FCurrentPage and (MaxPointsPerPage>0);
    if IsPage then FirstValue:=(Page-1)*MaxPointsPerPage;

    ITotalItems:=CalcTotalItems;

    if IsPage then ITotalItems:=MinLong(ITotalItems,MaxPointsPerPage);

    LegendColor:=Self.Color;

    Canvas.AssignFont(Self.Font);
    ItemHeight:=Canvas.FontHeight;
    ISpaceWidth:=Canvas.TextWidth(' ');

    if FCheckBoxes then ItemHeight:=MaxLong(6+TeeCheckBoxSize,ItemHeight);
    Inc(ItemHeight,FVertSpacing);

    if Self.Frame.Visible then FrameWidth:=Self.Frame.Width
                          else FrameWidth:=0;
    if Self.Bevel<>bvNone then FrameWidth:=Self.BevelWidth;

    if Self.Vertical then
    begin
      if not FCustomPosition then
      With ChartBounds do ShapeBounds.Top:=Top+Round(1.0*FTopLeftPos*(Bottom-Top)*0.01);

      NumCols:=1;
      NumRows:=MaxLegendValues(ShapeBounds.Top,ItemHeight);
      ILastValue:=FirstValue+MinLong(ITotalItems,NumRows)-1;
      GetItems;  // Only Visible !!
    end
    else
    begin
      ILastValue:=FirstValue+ITotalItems-1;
      GetItems;  // All !!
      tmpMaxWidth:=CalcColumnsWidth(TeeAllValues);
      IColorWidth:=Symbol.CalcWidth(tmpMaxWidth);

      if not FCustomPosition then
         With ShapeBounds do
         if Self.Alignment=laBottom then
            Bottom:=ChartRect.Bottom-ShadowSize-FrameWidth-1
         else
            Top:=ChartRect.Top+FrameWidth+1;

      NumCols:=MaxLegendValues(2*TeeLegendOff4,tmpMaxWidth+IColorWidth+2*TeeLegendOff4);
      if NumCols>0 then
      begin
        NumRows:=ITotalItems div NumCols;
        if (ITotalItems mod NumCols)>0 then Inc(NumRows);
        NumRows:=MinLong(NumRows,MaxNumRows);
      end
      else NumRows:=0;
      { adjust the last index now we know the max number of rows... }
      ILastValue:=FirstValue+MinLong(ITotalItems,NumCols*NumRows)-1;
    end;
  end;

  if ILastValue>=FirstValue then
  begin
    ResizeVertical;
    if Vertical then CalcVerticalPositions
                else CalcHorizontalPositions;
    if Assigned(TCustomChart(ParentChart).FOnGetLegendRect) then
       GetRectangle;

    Draw;

    DrawItems;
    ResizeChartRect;
  end;
  {$IFDEF D4}
  SetLength(Items,0);
  {$ENDIF}
end;

Function TCustomChartLegend.FormattedValue(ASeries:TChartSeries; ValueIndex:Integer):String;

  Procedure RemoveChar(AChar:Char; Var AString:String);
  Var i : Integer;
  begin
    Repeat
      i:=AnsiPos(AChar,AString);
      if i>0 then AString[i]:=' ';
    Until i=0;
  end;

begin
  if ValueIndex<>TeeAllValues then
  begin
    result:=ASeries.LegendString(ValueIndex,TextStyle);
    { eliminate breaks in string... }
    RemoveChar(TeeLineSeparator,result);
  end
  else result:='';
end;

procedure TCustomChartLegend.SetSeries(const Value: TChartSeries);
begin
  FSeries:=Value;
  Repaint;
end;

Function TCustomChartLegend.Clicked(x,y:Integer):Integer;
Var tmpH : Integer;

  Function ClickedRow:Integer;
  var t    : Integer;
      tmp  : Integer;
  begin
    result:=-1;
    for t:=0 to NumRows-1 do
    begin
      tmp:=ShapeBounds.Top+1+t*tmpH;
      if (y>=tmp) and (y<=(tmp+tmpH)) then
      begin
	result:=t;
        if FInverted then result:=NumRows-t-1;
	Break;
      end;
    end;
  end;

var t    : Integer;
    tmp2 : Integer;
    tmpW : Integer;
begin
  result:=-1;
  if {$IFDEF D6}Types.{$ENDIF}PtInRect(ShapeBounds,{$IFDEF D6}Types.{$ELSE}Classes.{$ENDIF}Point(x,y)) then { inside legend }
  begin
    if Vertical then
    begin
      if NumRows>0 then
      begin
	tmpH:=(ShapeBounds.Bottom-ShapeBounds.Top) div NumRows;
	result:=ClickedRow;
      end;
    end
    else
    begin
      if NumCols>0 then
      begin
	tmpW:=(ShapeBounds.Right-ShapeBounds.Left) div NumCols;
        ParentChart.Canvas.AssignFont(Self.Font);
	tmpH:=ParentChart.Canvas.FontHeight;
	for t:=0 to NumCols-1 do
	begin
	  tmp2:=ShapeBounds.Left+1+t*tmpW;
	  if (x>=tmp2) and (x<=(tmp2+tmpW)) then
	  begin
	    result:=ClickedRow;
	    if result<>-1 then
            begin
              result:=t+NumCols*result;
              if result>ITotalItems-1 then result:=-1;
            end;
	  end;
	end;
      end;
    end;
  end;
end;

function TCustomChartLegend.GetGradientClass: TChartGradientClass;
begin
  result:=TChartLegendGradient;
end;

{ Utility routines }

{ Draws the Series "Legend" on the specified rectangle and Canvas }
Procedure PaintSeriesLegend(ASeries:TChartSeries; ACanvas:TCanvas; Const R:TRect);
Var OldCanvas : TCanvas;
    tmpChart  : TChart;
begin
  if not Assigned(ASeries.ParentChart) then
  begin
    tmpChart:=TChart.Create(nil);
    tmpChart.AutoRepaint:=False;
    ASeries.ParentChart:=tmpChart;
  end
  else tmpChart:=nil;

  ACanvas.Brush.Style:=bsSolid;
  ACanvas.Brush.Color:=TCustomChart(ASeries.ParentChart).Legend.Color;
  With ASeries.ParentChart do
  if not Canvas.SupportsFullRotation then
  begin
    OldCanvas:=Canvas.ReferenceCanvas;
    Canvas.ReferenceCanvas:=ACanvas;
    try
      ASeries.DrawLegend(-1,R);
    finally
      Canvas.ReferenceCanvas:=OldCanvas;
    end;
  end;
  if ASeries.ParentChart=tmpChart then
  begin
    ASeries.ParentChart:=nil;
    tmpChart.Free;
  end;
end;

{ Returns a valid Name for a new Series (eg: Series1, Series2, etc) }
Function GetNewSeriesName(AOwner:TComponent):TComponentName;
begin
  result:=TeeGetUniqueName(AOwner,TeeMsg_DefaultSeriesName);
end;

{ Copies all properties from OldSeries to NewSeries }
type TSeriesAccess=class(TChartSeries);
     TChartAccess=class(TCustomAxisPanel);

Procedure AssignSeries(Var OldSeries,NewSeries:TChartSeries);
Var OldName   : TComponentName;
    tmpSeries : TChartSeries;
begin
  NewSeries.Assign(OldSeries);
  With NewSeries do
  begin    { events }
    AfterDrawValues  :=OldSeries.AfterDrawValues;
    BeforeDrawValues :=OldSeries.BeforeDrawValues;
    OnAfterAdd       :=OldSeries.OnAfterAdd;
    OnBeforeAdd      :=OldSeries.OnBeforeAdd;
    OnClearValues    :=OldSeries.OnClearValues;
    OnClick          :=OldSeries.OnClick;
    OnDblClick       :=OldSeries.OnDblClick;
    OnGetMarkText    :=OldSeries.OnGetMarkText;
  end;
  OldName:=OldSeries.Name;

  While OldSeries.LinkedSeries.Count>0 do
  begin
    tmpSeries:=TChartSeries(OldSeries.LinkedSeries[0]);
    { after removing... }
    With tmpSeries.DataSources do
    begin
      if IndexOf(NewSeries)=-1 then Add(NewSeries);
      if IndexOf(OldSeries)<>-1 then Remove(OldSeries);
    end;
    TSeriesAccess(NewSeries).AddLinkedSeries(tmpSeries);
    TSeriesAccess(OldSeries).RemoveLinkedSeries(tmpSeries);
  end;

  { Swap Series on Chart list }
  With OldSeries.ParentChart do
       ExchangeSeries(SeriesList.IndexOf(OldSeries),SeriesList.IndexOf(NewSeries));

  FreeAndNil(OldSeries);
  With NewSeries do
  begin
    Name:=OldName;
    TChartAccess(ParentChart).BroadcastSeriesEvent(NewSeries,seChangeTitle);
    if (not Assigned(DataSource)) and (csDesigning in ComponentState) then
	FillSampleValues(NumSampleValues);
    RefreshSeries;
  end;
end;

{ Creates a new TeeFunction object, associates it with a Series, and returns it }
Function CreateNewTeeFunction(ASeries:TChartSeries; AClass:TTeeFunctionClass):TTeeFunction;
begin
  result:=AClass.Create(ASeries.Owner);
  result.ParentSeries:=ASeries;
  result.Name:=TeeGetUniqueName(ASeries.Owner,TeeMsg_DefaultFunctionName);
end;

{ Creates a new Series object and sets the Name, ParentChart and
  FunctionType properties }
Function CreateNewSeries( AOwner:TComponent;
			  AChart:TCustomAxisPanel;
			  AClass:TChartSeriesClass;
			  AFunctionClass:TTeeFunctionClass):TChartSeries;
begin
//  if not Assigned(AOwner) then AOwner:=AChart;  { 4.0 }
  result:=AClass.Create(AOwner);
  result.Name:=GetNewSeriesName(AOwner);
  result.ParentChart:=AChart;
  if Assigned(AFunctionClass) then CreateNewTeeFunction(result,AFunctionClass);
end;

{ Duplicates a Series }
Function CloneChartSeries(ASeries:TChartSeries):TChartSeries;
Var tmp : TTeeFunctionClass;
begin
  With ASeries do
  begin
    if FunctionType=nil then tmp:=nil
			else tmp:=TTeeFunctionClass(FunctionType.ClassType);
    result:=CreateNewSeries(Owner,ParentChart,TChartSeriesClass(ClassType),tmp);
  end;
  result.Assign(ASeries);
  { add values from ASeries --> result }
  { if DataSource is not nil, values are already added in Assign }
  if result.DataSource=nil then result.AssignValues(ASeries);
end;

{ Replaces ASeries object with a new Series object of another class }
procedure ChangeSeriesType(Var ASeries:TChartSeries; NewType:TChartSeriesClass);
var NewSeries : TChartSeries;
begin
  if ASeries.ClassType<>NewType then { only if different classes }
  begin
    NewSeries:=CreateNewSeries(ASeries.Owner,ASeries.ParentChart,NewType,nil);
    if Assigned(NewSeries) then
    begin
      AssignSeries(ASeries,NewSeries);
      ASeries:=NewSeries;  { <-- change parameter }
    end;
  end;
end;

{ Replaces all Series objects in a Chart with a new Series class type }
procedure ChangeAllSeriesType( AChart:TCustomChart; AClass:TChartSeriesClass );
Var t         : Integer;
    tmpSeries : TChartSeries;
begin
  for t:=0 to AChart.SeriesCount-1 do
  begin
    tmpSeries:=AChart.Series[t];
    ChangeSeriesType(tmpSeries,AClass);
  end;
end;

{ TTeeSeriesTypes }

{$IFNDEF D4}
Destructor TTeeSeriesTypes.Destroy;
{$ELSE}
Procedure TTeeSeriesTypes.Clear;
{$ENDIF}
var t : Integer;
begin
  for t:=0 to Count-1 do Items[t].Free;
  inherited;
end;

Function TTeeSeriesTypes.Get(Index:Integer):TTeeSeriesType;
begin
  result:=TTeeSeriesType(inherited Items[Index]);
end;

Function TTeeSeriesTypes.Find(ASeriesClass: TChartSeriesClass): TTeeSeriesType;
var t : Integer;
begin
   for t:=0 to Count-1 do
       begin
          result:=Items[t];
          if (result.SeriesClass=ASeriesClass) and (result.FunctionClass=nil) then
             exit;
       end;
   result:=nil;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Function TTeeSeriesTypes.FindAndGetSeriesType(ASeriesClass: TChartSeriesClass): Byte;
var t                : Integer;
begin
   Result:=255;
   for t:=0 to Count-1 do
       begin
          if (Items[t].SeriesClass = ASeriesClass) and (Items[t].FunctionClass = Nil) then
             Result:=Items[t].SeriesType;
       end;
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure RegisterTeeSeriesFunction( ASeriesClass: TChartSeriesClass;
				     AFunctionClass: TTeeFunctionClass;
				     Const ADescription,AGalleryPage: String;
				     ANumGallerySeries: Integer;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
                                     ASeriesType: Byte);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
var t             : Integer;
    NewSeriesType : TTeeSeriesType;
begin
   if Assigned(ASeriesClass) then
      Classes.RegisterClass(ASeriesClass);
   if Assigned(AFunctionClass) then
      Classes.RegisterClass(AFunctionClass);
   with TeeSeriesTypes do
      for t:=0 to Count-1 do
          with Items[t] do
             if (SeriesClass=ASeriesClass) and (FunctionClass=AFunctionClass) then
                Exit;
   NewSeriesType:=TTeeSeriesType.Create;
   with NewSeriesType do
      begin
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
         SeriesType    := ASeriesType;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
         SeriesClass   := ASeriesClass;
         FunctionClass := AFunctionClass;
         Description   := ADescription;
         GalleryPage   := AGalleryPage;
         NumGallerySeries := ANumGallerySeries;
      end;
   TeeSeriesTypes.Add(NewSeriesType);
end;

{ Adds a new Series component definition for the Gallery }
Procedure RegisterTeeSeries( ASeriesClass: TChartSeriesClass;
			     Const ADescription, AGalleryPage: String;
			     ANumGallerySeries: Integer;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
                             ASeriesType: Byte);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
begin
  RegisterTeeSeriesFunction( ASeriesClass,
			     nil,
			     ADescription,
			     AGalleryPage,
			     ANumGallerySeries,
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
                             ASeriesType);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
end;

Procedure RegisterTeeFunction( AFunctionClass: TTeeFunctionClass;
			       Const ADescription, AGalleryPage: String;
			       ANumGallerySeries: Integer;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
                               ASeriesType: Byte);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
begin
  RegisterTeeSeriesFunction( nil,
			     AFunctionClass,
			     ADescription,
			     AGalleryPage,
			     ANumGallerySeries,
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
                             ASeriesType);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
end;

Procedure RegisterTeeBasicFunction( AFunctionClass: TTeeFunctionClass;
				    Const ADescription: String;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
                                    ASeriesType: Byte);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
begin
  RegisterTeeFunction(AFunctionClass,ADescription,TeeMsg_GalleryStandard,2,
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
                                                              ASeriesType);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
end;

Procedure InternalUnRegister(IsSeries:Boolean; AClass:TComponentClass);
var tmp : TTeeSeriesType;
    t   : Integer;
begin
  t:=0;
  While t<TeeSeriesTypes.Count do
  begin
    tmp:=TeeSeriesTypes[t];
    if (IsSeries and (tmp.SeriesClass=AClass)) or
     ((not IsSeries) and (tmp.FunctionClass=AClass)) then
    begin
      tmp.Free;
      TeeSeriesTypes.Delete(t);
    end
    else Inc(t);
  end;
end;

Procedure UnRegisterTeeSeries(ASeriesList:Array of TChartSeriesClass);
var t : Integer;
begin
  for t:=Low(ASeriesList) to High(ASeriesList) do
      InternalUnRegister(True,ASeriesList[t]);
end;

Procedure UnRegisterTeeFunctions(AFunctionList:Array of TTeeFunctionClass);
var t : Integer;
begin
  for t:=Low(AFunctionList) to High(AFunctionList) do
      InternalUnRegister(False,AFunctionList[t]);
end;

{$IFNDEF CLX}
{ TTeeDragObject }
constructor TTeeDragObject.Create(Const APart: TChartClickedPart);
begin
  FPart:=APart;
end;

{function TTeeDragObject.GetDragImages: TCustomImageList;
begin
  Result := FPart.GetDragImages;
end;}

{procedure TTeeDragObject.HideDragImage;
begin
  if FPart.GetDragImages <> nil then
    FPart.GetDragImages.HideDragImage;
end;

procedure TTeeDragObject.ShowDragImage;
begin
  if FPart.GetDragImages <> nil then
    FPart.GetDragImages.ShowDragImage;
end;
}
const crTeeDrag      = 2021;
      TeeMsg_TeeDrag = 'crTeeDrag'; { string cursor name (dont translate) }

function TTeeDragObject.GetDragCursor(Accepted: Boolean; X, Y: Integer): TCursor;
begin
  if Accepted then Result := crTeeHand
              else Result := crNoDrop;
end;

procedure TTeeDragObject.Finished(Target: TObject; X, Y: Integer; Accepted: Boolean);
begin
  if not Accepted then
  begin
{    FPart.DragCanceled;}
{    Target := nil;}
  end;
{  if Assigned(FPart. FOnEndDrag) then FOnEndDrag(Self, Target, X, Y);}
{  FPart.DoEndDrag(Target, X, Y);}
end;
{$ENDIF}

{ TTeeToolTypes }

function TTeeToolTypes.Get(Index: Integer): TTeeCustomToolClass;
begin
  result:=TTeeCustomToolClass(inherited Items[Index]);
end;

Procedure RegisterTeeTools(Tools:Array of TTeeCustomToolClass);
var t : Integer;
begin
  for t:=Low(Tools) to High(Tools) do
  With TeeToolTypes do
  if IndexOf(Tools[t])=-1 then
  begin
    Classes.RegisterClass(Tools[t]);
    Add(Tools[t]);
  end;
end;

Procedure UnRegisterTeeTools(Tools:Array of TTeeCustomToolClass);
var t   : Integer;
    tmp : Integer;
begin
  for t:=Low(Tools) to High(Tools) do
  With TeeToolTypes do
  begin
    tmp:=IndexOf(Tools[t]);
    if tmp<>-1 then Delete(tmp);
  end;
end;

Function GetGallerySeriesName(ASeries:TChartSeries):String;
var AType : TTeeSeriesType;
begin
  AType:=TeeSeriesTypes.Find(TChartSeriesClass(ASeries.ClassType));
  if Assigned(AType) then result:=AType.Description
                     else result:=ASeries.ClassName;
end;

initialization
  TeeSeriesTypes:=TTeeSeriesTypes.Create; { List of available Series/Function types }
  TeeToolTypes:=TTeeToolTypes.Create;     { List of available Tool types }
finalization
  FreeAndNil(TeeSeriesTypes);
  FreeAndNil(TeeToolTypes);
end.

