{******************************************}
{   TeeChart Pro  - URL files retrieval    }
{ Copyright (c) 1995-2000 by David Berneda }
{    All Rights Reserved                   }
{******************************************}
{$I teedefs.inc}
unit TeeURL;

interface

Uses Classes, Windows, Chart, TeEngine;

type
  TChartWebSource = class(TComponent)
  private
    { Private declarations }
    FChart : TCustomChart;
    FURL   : String;
    procedure SetChart(const Value: TCustomChart);
  protected
    { Protected declarations }
    procedure Notification( AComponent: TComponent;
                            Operation: TOperation); override;
  public
    { Public declarations }
    Constructor Create(AOwner:TComponent); override;
    Procedure Execute;
  published
    { Published declarations }
    property Chart:TCustomChart read FChart write SetChart;
    property URL:String read FURL write FURL;
  end;

Const
  TeeDefaultFieldSeparator=',';

type
  TSeriesTextField=class;

  TSeriesTextGetValue=procedure(Field:TSeriesTextField;
                                Const Text:String; Var Value:Double) of object;

  TSeriesTextField=class(TCollectionItem)
  private
    FFieldIndex : Integer;
    FFieldName  : String;
    FOnGetValue : TSeriesTextGetValue;
    procedure SetFieldIndex(const Value: Integer);
  protected
    { Protected declarations }
    Data : Pointer;
  published
    property FieldIndex:Integer read FFieldIndex write SetFieldIndex;
    property FieldName:String read FFieldName write FFieldName;
    property OnGetValue:TSeriesTextGetValue read FOnGetValue write FOnGetValue;
  end;

  TSeriesTextFields=class(TOwnedCollection)
  private
    Function Get(Index:Integer):TSeriesTextField;
    Procedure Put(Index:Integer; Const Value:TSeriesTextField);
  public
    property Items[Index:Integer]:TSeriesTextField read Get write Put; default;
  end;

  TSeriesTextSource=class(TComponent)
  private
    FActive: Boolean;
    FFields     : TSeriesTextFields;
    FFileName   : String;
    FHeader     : Integer;
    FSeparator  : String;
    FSeries     : TChartSeries;
    procedure SetFields(const Value: TSeriesTextFields);
    procedure SetSeries(const Value: TChartSeries);
    procedure SetActive(const Value: Boolean);
    procedure SetFileName(const Value: String);
  protected
    procedure Notification( AComponent: TComponent;
                            Operation: TOperation); override;
  public
    Constructor Create(AOwner:TComponent); override;
    Destructor Destroy; override;

    Function AddField(Const AName:String; AIndex:Integer):TSeriesTextField;
    Procedure Close;
    Procedure Load;
    Procedure Loaded; override;
    Procedure LoadFromStream(AStream:TStream);
    Procedure LoadFromStrings(AStrings:TStrings);
    Procedure LoadFromFile(Const AFileName:String);
    Procedure LoadFromURL(Const AURL:String);
    Procedure Open;
  published
    property Active:Boolean read FActive write SetActive default False;
    property HeaderLines:Integer read FHeader write FHeader default 0;
    property Fields:TSeriesTextFields read FFields write SetFields;
    property FieldSeparator:String read FSeparator write FSeparator;
    property FileName:String read FFileName write SetFileName;
    property Series:TChartSeries read FSeries write SetSeries;
  end;

{ Read a Chart from a file (ie: Chart1,'http://www.steema.com/demo.tee' ) }
Procedure LoadChartFromURL(Var AChart:TCustomChart; Const URL:String);

Function DownloadURL(URL:PChar; ToStream:TMemoryStream): HResult;

implementation

Uses TeeProcs, SysUtils, TeeProCo, TeeConst, TeeStore;

Procedure LoadChartFromURL(Var AChart:TCustomChart; Const URL:String);
var tmp    : Integer;
    R      : TRect;
    Stream : TMemoryStream;
    tmpURL : String;
begin
  Stream:=TMemoryStream.Create;
  try
    tmpURL:=URL;
    tmp:=DownloadURL(PChar(tmpURL),Stream);
    if tmp=0 then
    begin
      R:=AChart.BoundsRect;
      LoadChartFromStream(TCustomChart(AChart),Stream);
      if csDesigning in AChart.ComponentState then AChart.BoundsRect:=R;
    end
    else
      Raise ChartException.CreateFmt(TeeMsg_CannotLoadChartFromURL,[tmp,URL]);
  finally
    Stream.Free;
  end;
end;

Const INTERNET_OPEN_TYPE_PRECONFIG=0;
      INTERNET_FLAG_RAW_DATA = $40000000;               { receive the item as raw data }
      INTERNET_FLAG_NO_CACHE_WRITE        = $04000000;  { do not write this item to the cache }
      INTERNET_FLAG_DONT_CACHE            = INTERNET_FLAG_NO_CACHE_WRITE;

type
  HINTERNET = Pointer;

var WinInetDLL:THandle=0;

{$IFNDEF CLX}
   _InternetOpenA:function(lpszAgent: PAnsiChar; dwAccessType: DWORD;
                          lpszProxy, lpszProxyBypass: PAnsiChar;
                          dwFlags: DWORD): HINTERNET; stdcall;

   _InternetOpenURLA:function(hInet: HINTERNET; lpszUrl: PAnsiChar;
                         lpszHeaders: PAnsiChar; dwHeadersLength: DWORD; dwFlags: DWORD;
                         dwContext: DWORD): HINTERNET; stdcall;

  _InternetReadFile:function(hFile: HINTERNET; lpBuffer: Pointer;
                         dwNumberOfBytesToRead: DWORD;
                         var lpdwNumberOfBytesRead: DWORD): BOOL; stdcall;

  _InternetCloseHandle:function(hInet: HINTERNET): BOOL; stdcall;

procedure InitWinInet;
{$IFNDEF D5}
var OldError : Integer;
{$ENDIF}
begin
  {$IFNDEF D5}
  OldError:=SetErrorMode(SEM_NOOPENFILEERRORBOX);
  try
  {$ENDIF}
    if WinInetDLL = 0 then
    begin
      WinInetDLL:={$IFDEF D5}SafeLoadLibrary{$ELSE}LoadLibrary{$ENDIF}('wininet.dll');
      if WinInetDLL <> 0 then
      begin
        @_InternetOpenA      :=GetProcAddress(WinInetDLL,'InternetOpenA');
        @_InternetOpenURLA   :=GetProcAddress(WinInetDLL,'InternetOpenUrlA');
        @_InternetReadFile   :=GetProcAddress(WinInetDLL,'InternetReadFile');
        @_InternetCloseHandle:=GetProcAddress(WinInetDLL,'InternetCloseHandle');
      end;
    end;
  {$IFNDEF D5}
  finally
    SetErrorMode(OldError);
  end;
  {$ENDIF}
end;

Procedure ShowMessageUser(Const S:String);
var St : Array[0..1023] of Char;
begin
  MessageBox(0, StrPCopy(St,S),'',MB_OK or MB_ICONSTOP or MB_TASKMODAL);
end;

Function DownloadURL(URL:PChar; ToStream:TMemoryStream): HResult;
Const MaxSize= 128*1024;

var H1  : HINTERNET;
    H2  : HINTERNET;
    Buf : Pointer;
    tmp : Boolean;
    r   : {$IFDEF D4}DWord{$ELSE}Integer{$ENDIF};
begin
  {$IFDEF D5}
  result:=-1;
  {$ELSE}
  {$IFDEF D4}
  result:=$80000000;
  {$ELSE}
  result:=-1;
  {$ENDIF}
  {$ENDIF}
  if WinInetDLL=0 then InitWinInet;
  if WinInetDLL<>0 then
  begin
    h1:=_InternetOpenA('Tee',INTERNET_OPEN_TYPE_PRECONFIG,nil,nil,INTERNET_FLAG_DONT_CACHE);
    if h1<>nil then
    try
      h2:=_InternetOpenUrlA(h1,URL,nil,{$IFDEF D4}$80000000{$ELSE}-1{$ENDIF},
                              INTERNET_FLAG_DONT_CACHE {or
                              INTERNET_FLAG_RAW_DATA},
                             {INTERNET_FLAG_EXISTING_CONNECT} 0);
      if h2<>nil then
      try
        ToStream.Position:=0;
        Buf:=AllocMem(MaxSize);
        try
          Repeat
            r:=0;
            tmp:=_InternetReadFile(h2,Buf,MaxSize,r);
            if tmp then
            begin
              if r>0 then ToStream.WriteBuffer(Buf^,r)
              else
              begin
                ToStream.Position:=0;
                result:=0;
                break;
              end;
            end
            else result:=GetLastError;
          Until r=0;
        finally
          FreeMem(Buf,MaxSize);
        end;
      finally
        if not _InternetCloseHandle(h2) then result:=GetLastError;
      end
      else result:=GetLastError;
    finally
      if not _InternetCloseHandle(h1) then result:=GetLastError;
    end
    else result:=GetLastError;
  end
  else ShowMessageUser('Cannot load WinInet.dll to access TeeChart file: '+URL);
end;
{$ELSE}
Function DownloadURL(URL:PChar; ToStream:TMemoryStream): HResult;
begin
  result:=-1;
end;
{$ENDIF}

{ TSeriesTextSource }
Constructor TSeriesTextSource.Create(AOwner: TComponent);
begin
  inherited;
  FFields:=TSeriesTextFields.Create(Self,TSeriesTextField);
  FSeparator:=TeeDefaultFieldSeparator;
  FActive:=False;
end;

Destructor TSeriesTextSource.Destroy;
begin
  FFields.Free;
  inherited;
end;

function TSeriesTextSource.AddField(const AName: String;
  AIndex: Integer):TSeriesTextField;
begin
  result:=TSeriesTextField(FFields.Add);
  with result do
  begin
    FieldName:=AName;
    FieldIndex:=AIndex;
  end;
end;

procedure TSeriesTextSource.LoadFromFile(const AFileName: String);
var tmp:TFileStream;
begin
  tmp:=TFileStream.Create(AFileName,fmOpenRead);
  try
    LoadFromStream(tmp);
  finally
    tmp.Free;
  end;
end;

type TValueListAccess=class(TChartValueList);

procedure TSeriesTextSource.LoadFromStream(AStream: TStream);
var tmp : TStringList;
begin
  if not Assigned(FSeries) then Raise Exception.Create(TeeMsg_NoSeriesSelected);
  AStream.Position:=0;
  tmp:=TStringList.Create;
  try
    tmp.LoadFromStream(AStream);
    LoadFromStrings(tmp);
  finally
    tmp.Free;
  end;
end;

procedure TSeriesTextSource.SetFields(const Value: TSeriesTextFields);
begin
  FFields.Assign(Value);
end;

procedure TSeriesTextSource.LoadFromURL(const AURL: String);
var Stream : TMemoryStream;
    tmpURL : String;
    tmp    : Integer;
begin
  Stream:=TMemoryStream.Create;
  try
    tmpURL:=AURL;
    tmp:=DownloadURL(PChar(tmpURL),Stream);
    if tmp=0 then LoadFromStream(Stream)
             else Raise ChartException.CreateFmt(TeeMsg_CannotLoadSeriesDataFromURL,[tmp,AURL]);
  finally
    Stream.Free;
  end;
end;

procedure TSeriesTextSource.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if Operation=opRemove then
     if Assigned(FSeries) and (AComponent=FSeries) then
        Series:=nil;
end;

procedure TSeriesTextSource.SetSeries(const Value: TChartSeries);
begin
  if FSeries<>Value then
  begin
    FSeries:=Value;
    if Assigned(FSeries) then FSeries.FreeNotification(Self);
    Close;
  end;
end;

procedure TSeriesTextSource.LoadFromStrings(AStrings: TStrings);
Var St  : String;
    t,
    tt  : Integer;
    tmpLabel,tmpText:String;
    tmpHasNot:Boolean;
    tmpX:Double;
    tmpValue:Double;
begin
  if Assigned(FSeries) then
  begin
    tmpHasNot:=False;
    for tt:=0 to FFields.Count-1 do
    With FFields[tt] do
    begin
      if Uppercase(FieldName)=Uppercase(TeeMsg_Text) then
         Data:=FSeries.Labels
      else
      begin
        if Uppercase(FieldName)=Uppercase(FSeries.NotMandatoryValueList.Name) then
        begin
          tmpHasNot:=True;
          Data:=FSeries.NotMandatoryValueList;
        end
        else Data:=FSeries.GetYValueList(FieldName);
      end;
    end;
    TeeFieldsSeparator:=FSeparator;
    FSeries.Clear;
    for t:=HeaderLines to AStrings.Count-1 do
    begin
      St:=AStrings[t];
      if St<>'' then
      begin
        tmpLabel:='';
        for tt:=0 to FFields.Count-1 do
        With FFields[tt] do
        begin
          tmpText:=TeeExtractField(St,FieldIndex);
          if Data=FSeries.Labels then tmpLabel:=tmpText
          else
          begin
            if Assigned(FOnGetValue) then
               FOnGetValue(FFields[tt],tmpText,tmpValue)
            else
            if TValueListAccess(Data).DateTime then tmpValue:=StrToDateTime(tmpText)
                                               else tmpValue:=StrToFloat(tmpText);
            TValueListAccess(Data).TempValue:=tmpValue;
          end;
        end;
        With FSeries do
        begin
          if not tmpHasNot then tmpX:=Count
                           else tmpX:=NotMandatoryValueList.TempValue;
          AddXY(tmpX,MandatoryValueList.TempValue,tmpLabel,clTeeColor);
        end;
      end;
    end;
    FSeries.RefreshSeries;
  end;
end;

procedure TSeriesTextSource.Close;
begin
  if not (csLoading in ComponentState) then Active:=False;
end;

procedure TSeriesTextSource.Open;
begin
  Active:=True;
end;

Procedure TSeriesTextSource.Load;

  Function IsURL(Const St:String):Boolean;
  begin
    result:=(Pos('HTTP://',St)>0) or (Pos('FTP://',St)>0);
  end;

begin
  if Assigned(Series) and (FileName<>'') then
     if IsURL(UpperCase(FileName)) then LoadFromURL(FileName)
                                   else LoadFromFile(FileName);
end;

Procedure TSeriesTextSource.Loaded;
begin
  inherited;
  if Active then Load;
end;

procedure TSeriesTextSource.SetActive(const Value: Boolean);
begin
  if FActive<>Value then
  begin
    if Assigned(FSeries) then
    begin
      if Value then
      begin
        if not (csLoading in ComponentState) then Load;
      end
      else FSeries.Clear;
    end;
    FActive:=Value;
  end;
end;

procedure TSeriesTextSource.SetFileName(const Value: String);
begin
  if FFileName<>Value then
  begin
    Close;
    FFileName:=Value;
  end;
end;

{ TSeriesTextFields }
function TSeriesTextFields.Get(Index: Integer): TSeriesTextField;
begin
  result:=TSeriesTextField(inherited Items[Index]);
end;

procedure TSeriesTextFields.Put(Index: Integer; const Value: TSeriesTextField);
begin
  Items[Index].Assign(Value);
end;

{ TChartWebSource }
constructor TChartWebSource.Create(AOwner: TComponent);
begin
  inherited;
  FURL:=TeeMsg_DefaultDemoTee;
end;

procedure TChartWebSource.Execute;
begin
  if Assigned(FChart) and (FURL<>'') then LoadChartFromURL(FChart,FURL);
end;

procedure TChartWebSource.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if Operation=opRemove then
     if Assigned(FChart) and (AComponent=FChart) then
        Chart:=nil;
end;

procedure TChartWebSource.SetChart(const Value: TCustomChart);
begin
  if FChart<>Value then
  begin
    FChart:=Value;
    if Assigned(FChart) then FChart.FreeNotification(Self);
  end;
end;

{$IFNDEF CLX}

{ TSeriesTextField }
procedure TSeriesTextField.SetFieldIndex(const Value: Integer);
begin
  if Value>0 then FFieldIndex:=Value
             else Raise Exception.Create('Field index should be greater than zero.');
end;

initialization
  {$IFDEF C3}
  WinInetDLL:=0; { <-- BCB3 crashes in Win95 if we do not do this... }
  {$ENDIF}
finalization
  if WinInetDLL<>0 then FreeLibrary(WinInetDLL);
{$ENDIF}
end.

