{********************************************}
{     TeeChart Pro Charting Library          }
{    For Borland Delphi & C++ Builder        }
{  Copyright (c) 1995-2000 by David Berneda  }
{         All Rights Reserved                }
{********************************************}
{$I teedefs.inc}
Unit TeEngine;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls,
     {$ELSE}
     Graphics, Controls,
     {$ENDIF}
     TeeProcs, TeCanvas
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
     ,Objects
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
     {$IFNDEF TEEMULTIPLIER}
     ,Dialogs
     {$ENDIF};

Const ChartMarkColor   = clInfoBk (* $80FFFF *);  { default Series Mark back color }
      MinAxisIncrement :Double = 0.000000000001;  { <-- "double" for BCB }
      MinAxisRange     :Double = 0.0000000001;    { <-- "double" for BCB }
      TeeAllValues     = -1;
      clTeeColor       = TColor($20000000);
      ChartSamplesMax  = 1000;
      TeeAutoZOrder     = -1;
      TeeAutoDepth      = -1;
      TeeNoPointClicked = -1;
      TeeCheckMarkArrowColor:Boolean=False;  { when True, the Marks arrow pen
                                              color is changed if the point has
                                              the same color. }

      TeeMaxAxisTicks  = 2000;  { Maximum number of major ticks per axis }

      TeeColumnSeparator: AnsiChar = #6;  { To separate columns in Legend }
      TeeLineSeparator  : AnsiChar = #13; { To separate lines of text }

      TeeRandomAtRunTime:Boolean=False;

      clTeeGallery1 = clRed;
      clTeeGallery2 = clBlue;

Type
  TCustomAxisPanel=class;

  TCustomChartElement=class(TComponent)
  private
    FActive : Boolean;
    FBrush  : TChartBrush;
    FParent : TCustomAxisPanel;
    FPen    : TChartPen;
  protected
    Procedure CanvasChanged(Sender:TObject); virtual;
    Function CreateChartPen:TChartPen;
    Procedure GetBitmapEditor(ABitmap:TBitmap); dynamic;
    class Function GetEditorClass:String; virtual;
    Procedure SetActive(Value:Boolean); virtual;
    Procedure SetBooleanProperty(Var Variable:Boolean; Value:Boolean);
    procedure SetBrush(const Value: TChartBrush);
    Procedure SetColorProperty(Var Variable:TColor; Value:TColor);
    Procedure SetDoubleProperty(Var Variable:Double; Const Value:Double);
    Procedure SetIntegerProperty(Var Variable:Integer; Value:Integer);
    Procedure SetParentChart(Const Value:TCustomAxisPanel); virtual;
    procedure SetParentComponent(AParent: TComponent); override;
    procedure SetPen(const Value: TChartPen); virtual;
    Procedure SetStringProperty(Var Variable:String; Const Value:String);
  public
    Constructor Create(AOwner: TComponent); override;
    Destructor  Destroy; override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TComponent);
    Procedure   Store(var S: TOldStream);
    Function    OnCompareItems(Item: TComponent): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    procedure Assign(Source:TPersistent); override;
    Function GetParentComponent: TComponent; override;
    Function HasParent:Boolean; override;
    Procedure Repaint;

    property Active:Boolean read FActive write SetActive default True;
    property Brush:TChartBrush read FBrush write SetBrush;
    property ParentChart:TCustomAxisPanel read FParent write SetParentChart stored False;
    property Pen:TChartPen read FPen write SetPen;
  end;

  TCustomChartSeries=class;

  TChartSeries=class;

  {$IFDEF TEEVALUESINGLE}
  TChartValue=Single;
  {$ELSE}
  {$IFDEF TEEVALUEDOUBLE}
  TChartValue=Double;
  {$ELSE}
  {$IFDEF TEEVALUEEXTENDED}
  TChartValue=Extended;
  {$ELSE}
  TChartValue=Double;  { <-- default }
  {$ENDIF}
  {$ENDIF}
  {$ENDIF}

  {$IFDEF TEEARRAY}
  TChartValues=Array of TChartValue;
  {$ELSE}
  PChartValue=^TChartValue;
  {$ENDIF}

  TChartListOrder=(loNone,loAscending,loDescending);

  TChartValueList=class(TPersistent)
  private
    FDateTime    : Boolean;
    {$IFNDEF TEEARRAY}
    FList        : TList;
    {$ENDIF}
    FMaxValue    : TChartValue;
    FMinValue    : TChartValue;
    {$IFDEF TEEMULTIPLIER}
    FMultiplier  : Double;  { obsolete }
    {$ENDIF}
    FName        : String;
    FOrder       : TChartListOrder;
    FOwner       : TChartSeries;
    FTempValue   : TChartValue;
    FTotal       : Double;
    FTotalABS    : Double;
    FValueSource : String;
    { internal }
    Function CompareValueIndex(a,b:Integer):Integer;
    Function GetMaxValue:TChartValue;
    Function GetMinValue:TChartValue;
    Function GetTotal:Double;
    Function GetTotalABS:Double;
    {$IFDEF TEEARRAY}
    Procedure IncrementArray;
    {$ENDIF}
    procedure SetDateTime(Const Value:Boolean);
    {$IFDEF TEEMULTIPLIER}
    Function IsMultiStored:Boolean;
    Procedure SetMultiplier(Const Value:Double); { obsolete }
    {$ELSE}
    procedure ReadMultiplier(Reader: TReader);
    {$ENDIF}
    Procedure SetValueSource(Const Value:String);
  protected
    IData       : TObject;
    Function AddChartValue(Const AValue:TChartValue):Integer; virtual;
    Procedure ClearValues; virtual;
    {$IFNDEF TEEMULTIPLIER}
    procedure DefineProperties(Filer: TFiler); override;
    {$ENDIF}
    Function GetValue(ValueIndex:Integer):TChartValue;
    Procedure InsertChartValue(ValueIndex:Integer; Const AValue:TChartValue); virtual;
    Procedure RecalcStats;
    Procedure SetValue(ValueIndex:Integer; Const AValue:TChartValue);
  public
    {$IFDEF TEEARRAY}
    Value    : TChartValues;
    Count    : Integer;
    {$ENDIF}
    Modified : Boolean;
    Constructor Create(AOwner: TChartSeries; Const AName: String); virtual;
    Destructor  Destroy; override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TChartSeries);
    Procedure   Store(var S: TOldStream);
    Function    OnCompareItems(Item: TPersistent): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    Procedure Assign(Source:TPersistent); override;
    {$IFNDEF TEEARRAY}
    Function Count:Integer; virtual;
    {$ENDIF}
    Procedure Delete(ValueIndex:Integer); virtual;
    {$IFDEF TEEARRAY}
    Procedure Exchange(Index1,Index2:Integer);
    {$ENDIF}
    Procedure FillSequence;
    Function First:TChartValue;
    Function Last:TChartValue;
    Function Locate(Const AValue:TChartValue):Integer;
    Function Range:TChartValue;
    Procedure Scroll; dynamic;
    Procedure Sort;

    property MaxValue:TChartValue read GetMaxValue;
    property MinValue:TChartValue read GetMinValue;
    property Owner:TChartSeries read FOwner;
    property TempValue:TChartValue read FTempValue write FTempValue;
    property Total:Double read GetTotal;
    property TotalABS:Double read GetTotalABS write FTotalABS;
    {$IFDEF TEEARRAY}
    property Items[Index:Integer]:TChartValue read GetValue write SetValue; default;
    {$ELSE}
    property Value[Index:Integer]:TChartValue read GetValue write SetValue; default;
    {$ENDIF}
  published
    property DateTime:Boolean read FDateTime write SetDateTime default False;
    property Name:String read FName write FName;
    {$IFDEF TEEMULTIPLIER}
    property Multiplier:Double read FMultiplier write SetMultiplier stored IsMultiStored; { obsolete }
    {$ENDIF}
    property Order:TChartListOrder read FOrder write FOrder;
    property ValueSource:String read FValueSource write SetValueSource;
  end;

  TChartAxisTitle=class(TTeeCustomShape)
  private
    FAngle        : Integer;
    FCaption      : String;

    IDefaultAngle : Integer;
    Function IsAngleStored:Boolean;
    Procedure SetAngle(Value:Integer);
    Procedure SetCaption(Const Value:String);
  public
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TCustomTeePanel);
    Procedure   Store(var S: TOldStream);
    Function    OnCompareItems(Item: TPersistent): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    Procedure   Assign(Source: TPersistent); override;
  published
    property Angle:Integer read FAngle write SetAngle stored IsAngleStored;
    property Caption:String read FCaption write SetCaption;
    property Font;
    property Visible default True;
  end;

  AxisException=class(Exception);

  TAxisLabelStyle=(talAuto,talNone,talValue,talMark,talText);
  TAxisLabelAlign=(alDefault,alOpposite);

  TChartAxis=class(TCollectionItem)
  private
    { scales }
    FAutomatic        : Boolean;          //sr
    FAutomaticMaximum : Boolean;          //sr
    FAutomaticMinimum : Boolean;          //sr
    FDesiredIncrement : Double;           //sr
    FMaximumValue     : Double;           //sr
    FMinimumValue     : Double;           //sr
    FLogarithmic      : Boolean;          //sr
    FLogarithmicBase  : Integer;          //sr

    { axis }
    FAxis             : TChartAxisPen;    //sr
    FPosAxis          : Integer;          //calc

    { title }
    FAxisTitle        : TChartAxisTitle;  //sr
    FTitleSize        : Integer;          //sr
    FPosTitle         : Integer;          //calc

    { grid }
    FGrid             : TDottedGrayPen;   //sr
    FGridCentered     : Boolean;          //sr

    { labels }
    FLabels           : Boolean;          //sr
    FLabelsAlign      : TAxisLabelAlign;  //sr
    FLabelsAngle      : Integer;          //sr
    FLabelsFont       : TTeeFont;         //sr
    FLabelsOnAxis     : Boolean;          //sr
    FLabelsSeparation : Integer;          //sr
    FLabelsSize       : Integer;          //sr
    FLabelStyle       : TAxisLabelStyle;  //sr
    FLabelsExponent   : Boolean;          //sr
    FLabelsMultiLine  : Boolean;          //sr

    FPosLabels        : Integer;          //calc
    FAxisValuesFormat : String;           //sr
    FDateTimeFormat   : String;           //sr
    FExactDateTime    : Boolean;          //sr
    FRoundFirstLabel  : Boolean;          //sr

    { ticks }
    FMinorGrid        : TChartHiddenPen;  //sr

    FMinorTickCount   : Integer;          //sr
    FMinorTickLength  : Integer;          //sr
    FMinorTicks       : TDarkGrayPen;     //sr

    FTicks            : TDarkGrayPen;     //sr
    FTicksInner       : TDarkGrayPen;     //sr
    FTickInnerLength  : Integer;          //sr
    FTickLength       : Integer;          //sr
    FTickOnLabelsOnly : Boolean;          //sr

    { other }
    FInverted         : Boolean;          //sr
    FHorizontal       : Boolean;          //sr
    FOtherSide        : Boolean;          //sr
    FParentChart      : TCustomAxisPanel; //cr
    FVisible          : Boolean;          //sr
    FStartPosition    : Double;           //sr
    FEndPosition      : Double;           //sr
    FPositionPercent  : Double;           //sr

    { internal }
    IMaximum          : Double;
    IMinimum          : Double;
    IRange            : Double;
    IAxisDateTime     : Boolean;
    ICenterPos        : Integer;

    Procedure SetAutomatic(Value:Boolean);
    Procedure SetAutomaticMinimum(Value:Boolean);
    Procedure SetAutomaticMaximum(Value:Boolean);
    Procedure SetAutoMinMax(Var Variable:Boolean; Var2,Value:Boolean);
    Procedure SetAxis(Value:TChartAxisPen);
    procedure SetAxisTitle(Value:TChartAxisTitle);
    Procedure SetDateTimeFormat(Const Value:String);
    Procedure SetDesiredIncrement(Const Value:Double);
    Procedure SetExactDateTime(Value:Boolean);
    Procedure SetGrid(Value:TDottedGrayPen);
    procedure SetGridCentered(Value:Boolean);
    Procedure SetInverted(Value:Boolean);
    Procedure SetLabels(Value:Boolean);
    Procedure SetLabelsAlign(Value:TAxisLabelAlign);
    Procedure SetLabelsAngle(Value:Integer);
    Procedure SetLabelsFont(Value:TTeeFont);
    Procedure SetLabelsMultiLine(Value:Boolean);
    Procedure SetLabelsOnAxis(Value:Boolean);
    Procedure SetLabelsSeparation(Value:Integer);
    Procedure SetLabelsSize(Value:Integer);
    Procedure SetLabelStyle(Value:TAxisLabelStyle);
    Procedure SetLogarithmic(Value:Boolean);
    Procedure SetLogarithmicBase(Value:Integer);
    Procedure SetMaximum(Const Value:Double);
    Procedure SetMinimum(Const Value:Double);
    Procedure SetMinorGrid(Value:TChartHiddenPen);
    Procedure SetMinorTickCount(Value:Integer);
    Procedure SetMinorTickLength(Value:Integer);
    Procedure SetMinorTicks(Value:TDarkGrayPen);
    procedure SetStartPosition(Const Value:Double);
    procedure SetEndPosition(Const Value:Double);
    procedure SetPositionPercent(Const Value:Double);
    procedure SetRoundFirstLabel(Value:Boolean);
    Procedure SetTickLength(Value:Integer);
    Procedure SetTickInnerLength(Value:Integer);
    Procedure SetTicks(Value:TDarkGrayPen);
    Procedure SetTicksInner(Value:TDarkGrayPen);
    procedure SetTickOnLabelsOnly(Value:Boolean);
    Procedure SetTitleSize(Value:Integer);
    Procedure SetValuesFormat(Const Value:String);
    Procedure SetVisible(Value:Boolean);

    Function ApplyPosition(APos:Integer; Const R:TRect):Integer;
    Function CalcDateTimeIncrement(MaxNumLabels:Integer):Double;
    Function CalcLabelsIncrement(MaxNumLabels:Integer):Double;
    Procedure CalcRect(Var R:TRect; InflateChartRectangle:Boolean);
    Procedure DrawTitle(x,y:Integer);
    Function GetRectangleEdge(Const R:TRect):Integer;
    Procedure IncDecDateTime( Increment:Boolean;
                              Var Value:Double;
                              Const AnIncrement:Double;
                              tmpWhichDateTime:TDateTimeStep );
    Function InternalCalcLogPosValue(IsX:Boolean; Const Value:Double):Integer;
    Function InternalCalcDepthPosValue(Const Value:Double):Integer;
    Function InternalCalcLog(Var LogMax,LogMin:Double):Double;
    Procedure InternalCalcPositions;
    Function InternalCalcPosValue(Const Value:Double; FromEnd:Boolean):Integer;
    Function InternalCalcSize( tmpFont:TTeeFont;
                               tmpAngle:Integer;
                               Const tmpText:String;
                               tmpSize:Integer):Integer;
    Procedure InternalSetMaximum(Const Value:Double);
    Procedure InternalSetMinimum(Const Value:Double);

    function IsAxisValuesFormatStored:Boolean;
    Function IsMaxStored:Boolean;
    Function IsMinStored:Boolean;
    Function IsPosStored:Boolean;
    Function IsStartStored:Boolean;
    Function IsEndStored:Boolean;
    Function IsCustom:Boolean;
    Procedure RecalcSizeCenter;
    procedure SetHorizontal(const Value: Boolean);
    procedure SetOtherSide(const Value: Boolean);
    procedure SetLabelsExponent(Value: Boolean);
  protected
    Function InternalCalcLabelStyle:TAxisLabelStyle; virtual;
    Procedure InternalSetInverted(Value:Boolean);
    Function SizeLabels:Integer;
    Function SizeTickAxis:Integer;
  public
    IStartPos   : Integer;
    IEndPos     : Integer;
    IAxisSize   : Integer;
    IsDepthAxis : Boolean;

    {$IFDEF D5}
    Constructor Create(Chart:TCustomAxisPanel); reintroduce; overload;
    {$ENDIF}
    Constructor Create(Collection: Classes.TCollection); {$IFDEF D5}overload;{$ENDIF} override;
    Destructor  Destroy; override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; Collection: Classes.TCollection);
    Procedure   Store(var S: TOldStream);
    Function    OnCompareItems(Item: TCollectionItem): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    Procedure AdjustMaxMin;
    Procedure AdjustMaxMinRect(Const Rect:TRect);
    procedure Assign(Source: TPersistent); override;
    Function CalcIncrement:Double;
    Function CalcLabelStyle:TAxisLabelStyle;
    Procedure CalcMinMax(Var AMin,AMax:Double);
    Function CalcPosPoint(Value:Integer):Double;
    Function CalcPosValue(Const Value:Double):Integer;
    Function CalcSizeValue(Const Value:Double):Integer;
    Function CalcXPosValue(Const Value:Double):Integer;
    Function CalcYPosValue(Const Value:Double):Integer;
    Function CalcXYIncrement(MaxLabelSize:Integer):Double;

    Procedure CustomDraw( APosLabels,APosTitle,APosAxis:Integer;
                          GridVisible:Boolean);
    Procedure CustomDrawMinMax( APosLabels,
                                APosTitle,
                                APosAxis:Integer;
                                GridVisible:Boolean;
                                Const AMinimum,AMaximum,AIncrement:Double);
    Procedure CustomDrawMinMaxStartEnd( APosLabels,
                                        APosTitle,
                                        APosAxis:Integer;
                                        GridVisible:Boolean;
                                        Const AMinimum,AMaximum,AIncrement:Double;
                                        AStartPos,AEndPos:Integer);
    Procedure CustomDrawStartEnd( APosLabels,APosTitle,APosAxis:Integer;
                                  GridVisible:Boolean; AStartPos,AEndPos:Integer);

    Function Clicked(x,y:Integer):Boolean;
    Procedure Draw(CalcPosAxis:Boolean);
    procedure DrawAxisLabel(x,y,Angle:Integer; Const St:String);
    Function IsDateTime:Boolean;
    Function LabelWidth(Const Value:Double):Integer;
    Function LabelHeight(Const Value:Double):Integer;
    Function LabelValue(Const Value:Double):String;
    Function MaxLabelsWidth:Integer;

    Procedure Scroll(Const Offset:Double; CheckLimits:Boolean);
    Procedure SetMinMax(AMin,AMax:Double);

   { public }
    property PosAxis    : Integer read FPosAxis;
    property PosLabels  : Integer read FPosLabels;
    property PosTitle   : Integer read FPosTitle;
    property ParentChart: TCustomAxisPanel read FParentChart;
  published
    property Automatic:Boolean read FAutomatic write SetAutomatic default True;
    property AutomaticMaximum:Boolean read FAutomaticMaximum write SetAutomaticMaximum default True;
    property AutomaticMinimum:Boolean read FAutomaticMinimum write SetAutomaticMinimum default True;
    property Axis:TChartAxisPen read FAxis write SetAxis;
    property AxisValuesFormat:String read FAxisValuesFormat
                                     write SetValuesFormat stored IsAxisValuesFormatStored;
    property DateTimeFormat:String read FDateTimeFormat write SetDateTimeFormat;
    property ExactDateTime:Boolean read FExactDateTime write SetExactDateTime default True;
    property Grid:TDottedGrayPen read FGrid write SetGrid;
    property GridCentered:Boolean read FGridCentered write SetGridCentered default False;
    property Increment:Double read FDesiredIncrement write SetDesiredIncrement;
    property Inverted:Boolean read FInverted write SetInverted default False;

    property Horizontal : Boolean read FHorizontal write SetHorizontal stored IsCustom;
    property OtherSide  : Boolean read FOtherSide write SetOtherSide stored IsCustom;

    property Labels:Boolean read FLabels write SetLabels default True;
    property LabelsAlign:TAxisLabelAlign read FLabelsAlign write SetLabelsAlign default alDefault;
    property LabelsAngle:Integer read FLabelsAngle write SetLabelsAngle default 0;
    property LabelsExponent:Boolean read FLabelsExponent write SetLabelsExponent default False;
    property LabelsFont:TTeeFont read FLabelsFont write SetLabelsFont {stored IsFontStored};
    property LabelsMultiLine:Boolean read FLabelsMultiLine write SetLabelsMultiLine default False;
    property LabelsOnAxis:Boolean read FLabelsOnAxis write SetLabelsOnAxis default True;
    property LabelsSeparation:Integer read FLabelsSeparation
                                      write SetLabelsSeparation default 10;
    property LabelsSize:Integer read FLabelsSize write SetLabelsSize default 0;
    property LabelStyle:TAxisLabelStyle read FLabelStyle write SetLabelStyle default talAuto;

    property Logarithmic:Boolean read FLogarithmic write SetLogarithmic default False;
    property LogarithmicBase:Integer read FLogarithmicBase write SetLogarithmicBase default 10;
    property Maximum:Double read FMaximumValue write SetMaximum stored IsMaxStored;
    property Minimum:Double read FMinimumValue write SetMinimum stored IsMinStored;
    property MinorGrid:TChartHiddenPen read FMinorGrid write SetMinorGrid;
    property MinorTickCount:Integer read FMinorTickCount write SetMinorTickCount default 3;
    property MinorTickLength:Integer read FMinorTickLength write SetMinorTickLength default 2;
    property MinorTicks:TDarkGrayPen read FMinorTicks write SetMinorTicks;
    property StartPosition:Double read FStartPosition write SetStartPosition
                                    stored IsStartStored;
    property EndPosition:Double read FEndPosition write SetEndPosition
                                    stored IsEndStored;
    property PositionPercent:Double read FPositionPercent write SetPositionPercent
                                    stored IsPosStored;
    property RoundFirstLabel:Boolean read FRoundFirstLabel write SetRoundFirstLabel default True;
    property TickInnerLength:Integer read FTickInnerLength write SetTickInnerLength default 0;
    property TickLength:Integer read FTickLength write SetTickLength default 4;
    property Ticks:TDarkGrayPen read FTicks write SetTicks;
    property TicksInner:TDarkGrayPen read FTicksInner write SetTicksInner;
    property TickOnLabelsOnly:Boolean read FTickOnLabelsOnly write SetTickOnLabelsOnly default True;
    property Title:TChartAxisTitle read FAxisTitle write SetAxisTitle;
    property TitleSize:Integer read FTitleSize write SetTitleSize default 0;
    property Visible:Boolean read FVisible write SetVisible default True;
  end;

  TChartDepthAxis=class(TChartAxis)
  protected
    Function InternalCalcLabelStyle:TAxisLabelStyle; override;
  published
    property Visible default False;
  end;

  TAxisOnGetLabel=Procedure( Sender:TChartAxis; Series:TChartSeries;
                             ValueIndex:Integer; Var LabelText:String) of object;

  TAxisOnGetNextLabel=Procedure( Sender:TChartAxis; LabelIndex:Integer;
                                 Var LabelValue:Double; Var Stop:Boolean) of object;

  TSeriesMouseMove=procedure( Sender:TChartSeries;
                              Shift: TShiftState;
                              X, Y: Integer) of object;

  TSeriesClick=procedure( Sender:TChartSeries;
                          ValueIndex:Integer;
                          Button:TMouseButton;
                          Shift: TShiftState;
                          X, Y: Integer) of object;

  TSeriesDblClick=procedure( Sender:TChartSeries;
                             ValueIndex:Integer) of object;

  TValueEvent=(veClear,veAdd,veDelete,veRefresh,veModify);

  THorizAxis = (aTopAxis,aBottomAxis,aBothHorizAxis,aCustomHorizAxis);
  TVertAxis  = (aLeftAxis,aRightAxis,aBothVertAxis,aCustomVertAxis);

  TChartClickedPartStyle=( cpNone,
                           cpLegend,
                           cpAxis,
                           cpSeries,
                           cpTitle,
                           cpFoot,
                           cpChartRect,
                           cpSeriesMarks,
                           cpSubTitle,
                           cpSubFoot );

  TChartClickedPart=Packed Record
    Part       : TChartClickedPartStyle;
    PointIndex : Integer;
    ASeries    : TChartSeries;
    AAxis      : TChartAxis;
  end;

  TChartSeriesList=class(TList)
  private
    FOwner : TCustomAxisPanel;
    procedure Put(Index:Integer; Value:TChartSeries);
    function Get(Index:Integer):TChartSeries;
  public
    property Items[Index:Integer]:TChartSeries read Get write Put; default;
    property Owner:TCustomAxisPanel read FOwner;
  end;

  TChartAxes=class(TList)
  private
    function Get(Index:Integer):TChartAxis;
  public
    {$IFNDEF D4}
    Destructor Destroy; override;
    {$ELSE}
    procedure Clear; override;
    {$ENDIF}
    property Items[Index:Integer]:TChartAxis read Get; default;
  end;

  {$IFNDEF D4}
  TOwnedCollection=class(Classes.TCollection)
  private
    FOwner: TPersistent;
  protected
    function GetOwner: TPersistent; override;
  public
    constructor Create(AOwner: TPersistent; ItemClass: TCollectionItemClass);
  end;
  {$ENDIF}

  TChartCustomAxes=class(TOwnedCollection)
  private
    Function Get(Index:Integer):TChartAxis;
    Procedure Put(Index:Integer; Const Value:TChartAxis);
  public
    property Items[Index:Integer]:TChartAxis read Get write Put; default;
  end;

  TTeeCustomDesigner=class(TObject)
  public
    Procedure Refresh; dynamic;
    Procedure Repaint; dynamic;
  end;

  TChartSeriesEvent=( seAdd, seRemove, seChangeTitle, seChangeColor, seSwap,
                      seChangeActive, seDataChanged);

  TChartChangePage=class(TTeeEvent);

  TChartToolEvent=(cteAfterDraw,cteBeforeDrawSeries);
  TChartMouseEvent=(cmeDown,cmeMove,cmeUp);

  TTeeCustomTool=class(TCustomChartElement)
  protected
    Procedure ChartEvent(AEvent:TChartToolEvent); virtual;
    Procedure ChartMouseEvent( AEvent: TChartMouseEvent;
                               Button:TMouseButton;
                               Shift: TShiftState; X, Y: Integer); virtual;
    procedure SetParentChart(const Value: TCustomAxisPanel); override;
  public
    class Function Description:String; virtual;
  end;

  TTeeCustomToolClass=class of TTeeCustomTool;

  TChartTools=class(TList)
  private
    Owner : TCustomAxisPanel;
    Function Get(Index:Integer):TTeeCustomTool;
  public
    Procedure Add(Tool:TTeeCustomTool);
    {$IFNDEF D4}
    Destructor Destroy; override;
    {$ELSE}
    procedure Clear; override;
    {$ENDIF}
    property Items[Index:Integer]:TTeeCustomTool read Get; default;
  end;

  TTeeEventClass=class of TTeeEvent;

  TTeeSeriesEvent=class(TTeeEvent)
    Event  : TChartSeriesEvent;
    Series : TCustomChartSeries;
  end;

  TChartSeriesClass=class of TChartSeries;

  TCustomAxisPanel=class(TCustomTeePanelExtended)
  private
    { Private declarations }
    FAxes             : TChartAxes;        //cr
    FAxisBehind       : Boolean;           //sr
    FAxisVisible      : Boolean;           //sr
    FClipPoints       : Boolean;           //sr
    FCustomAxes       : TChartCustomAxes;  //cr
    FSeriesList       : TChartSeriesList;  //cr
    FDepthAxis        : TChartDepthAxis;   //sr
    FTopAxis          : TChartAxis;        //sr
    FBottomAxis       : TChartAxis;        //sr
    FLeftAxis         : TChartAxis;        //sr
    FRightAxis        : TChartAxis;        //sr
    FView3DWalls      : Boolean;           //sr

    FOnGetAxisLabel   : TAxisOnGetLabel;
    FOnGetNextAxisLabel:TAxisOnGetNextLabel;

    FOnPageChange     : TNotifyEvent;
    FOnBeforeDrawChart: TNotifyEvent;
    FOnBeforeDrawAxes : TNotifyEvent;
    FOnBeforeDrawSeries:TNotifyEvent;

    FPage             : Integer;           //sr
    FMaxPointsPerPage : Integer;           //sr
    FScaleLastPage    : Boolean;           //sr

    FMaxZOrder        : Integer;           //sr
    FSeriesWidth3D    : Integer;           //calc
    FSeriesHeight3D   : Integer;           //calc

    FTools            : TChartTools;       //cr

    Procedure BroadcastTeeEventClass(Event:TTeeEventClass);
    Procedure BroadcastToolEvent(AEvent:TChartToolEvent);
    Procedure CheckOtherSeries(Dest,Source:TChartSeries);
    Function GetSeries(Index:Integer):TChartSeries;
    Procedure InternalAddSeries(ASeries:TCustomChartSeries);
    Function InternalMinMax(AAxis:TChartAxis; IsMin,IsX:Boolean):Double;
    Function MultiLineTextWidth(S:String; Var NumLines:Integer):Integer;
    Function NoActiveSeries(AAxis:TChartAxis):Boolean;
    Procedure SetAxisBehind(Value:Boolean);
    Procedure SetAxisVisible(Value:Boolean);
    Procedure SetBottomAxis(Value:TChartAxis);
    procedure SetClipPoints(Value:Boolean);
    Procedure SetCustomAxes(Value:TChartCustomAxes);
    Procedure SetDepthAxis(Value:TChartDepthAxis);
    Procedure SetLeftAxis(Value:TChartAxis);
    Procedure SetMaxPointsPerPage(Value:Integer);
    Procedure SetRightAxis(Value:TChartAxis);
    Procedure SetScaleLastPage(Value:Boolean);
    Procedure SetTopAxis(Value:TChartAxis);
    procedure SetView3DWalls(Value:Boolean);
    function IsCustomAxesStored: Boolean;
  protected
    { Protected declarations }
    LegendColor : TColor;
    Procedure BroadcastSeriesEvent(ASeries:TCustomChartSeries; Event:TChartSeriesEvent);
    Procedure CalcWallsRect; virtual; abstract;
    Function CalcWallSize(Axis:TChartAxis):Integer; virtual; abstract;
    procedure DrawTitlesAndLegend(BeforeSeries:Boolean); virtual; abstract;
    Procedure DrawWalls; virtual; abstract;
    Function IsAxisVisible(Axis:TChartAxis):Boolean;
    procedure RemovedDataSource( ASeries: TChartSeries;
                                 AComponent: TComponent ); dynamic;
    Procedure SetPage(Value:Integer);
    Procedure GetChildren(Proc:TGetChildProc; Root:TComponent); override;
  public
    { Public declarations }
    Designer : TTeeCustomDesigner; { used only at Design-Time }
    Constructor Create(AOwner: TComponent); override;
    Destructor  Destroy; override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TComponent);
    Procedure   Store(var S: TOldStream);
    Function    OnCompareItems(Item: TComponent): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    { public methods }
    procedure Assign(Source:TPersistent); override;
    Function ActiveSeriesLegend(ItemIndex:Integer):TChartSeries;
    Function AddSeries(ASeries:TChartSeries):TChartSeries;
    Procedure CalcSize3DWalls;
    Procedure CheckDatasource(ASeries:TChartSeries); virtual;
    Function CountActiveSeries:Integer;
    Procedure ExchangeSeries(a,b:Integer);
    Function FormattedValueLegend(ASeries:TChartSeries; ValueIndex:Integer):String; virtual;
    Procedure FreeAllSeries {$IFDEF D4}( SeriesClass:TChartSeriesClass=nil ){$ENDIF};
    Function GetAxisSeries(Axis:TChartAxis):TChartSeries;
    Function GetFreeSeriesColor(CheckBackground:Boolean):TColor;
    Function GetMaxValuesCount:Integer;
    Procedure InternalDraw(Const UserRectangle:TRect); override;
    Function IsFreeSeriesColor(AColor:TColor; CheckBackground:Boolean):Boolean; virtual; abstract;
    Function IsValidDataSource(ASeries:TChartSeries; AComponent:TComponent):Boolean; dynamic;
    Function MaxXValue(AAxis:TChartAxis):Double;
    Function MaxYValue(AAxis:TChartAxis):Double;
    Function MinXValue(AAxis:TChartAxis):Double;
    Function MinYValue(AAxis:TChartAxis):Double;
    Function MaxMarkWidth:Integer;
    Function MaxTextWidth:Integer;
    Function NumPages:Integer; dynamic;
    procedure PrintPages(FromPage:Integer{$IFDEF D4}=1{$ENDIF}; ToPage: Integer{$IFDEF D4}=0{$ENDIF});
    Procedure RemoveSeries(ASeries:TCustomChartSeries);

    property Series[Index:Integer]:TChartSeries read GetSeries; default;
    Function SeriesCount:Integer;

    Function SeriesLegend(ItemIndex:Integer; OnlyActive:Boolean):TChartSeries;
    Function SeriesTitleLegend(SeriesIndex:Integer; OnlyActive:Boolean{$IFDEF D4}=False{$ENDIF}):String;

    { public properties }
    property Axes:TChartAxes read FAxes;
    property AxesList:TChartAxes read FAxes; // compatibility v4
    property CustomAxes:TChartCustomAxes read FCustomAxes write SetCustomAxes stored IsCustomAxesStored;
    property MaxZOrder:Integer read FMaxZOrder write FMaxZOrder;
    property SeriesWidth3D:Integer read FSeriesWidth3D;
    property SeriesHeight3D:Integer read FSeriesHeight3D;

    { to be published properties }
    property AxisBehind:Boolean read FAxisBehind write SetAxisBehind default True;
    property AxisVisible:Boolean read FAxisVisible write SetAxisVisible default True;
    property BottomAxis:TChartAxis read FBottomAxis write SetBottomAxis;
    property ClipPoints:Boolean read FClipPoints write SetClipPoints default True;
    property Color;
    property DepthAxis:TChartDepthAxis read FDepthAxis write SetDepthAxis;
    property LeftAxis:TChartAxis read FLeftAxis write SetLeftAxis;
    property MaxPointsPerPage:Integer read FMaxPointsPerPage write SetMaxPointsPerPage default 0;
    property Page:Integer read FPage write SetPage default 1;
    property RightAxis:TChartAxis read FRightAxis write SetRightAxis;
    property ScaleLastPage:Boolean read FScaleLastPage write SetScaleLastPage default True;
    property SeriesList:TChartSeriesList read FSeriesList;
    property Tools:TChartTools read FTools;
    property TopAxis:TChartAxis read FTopAxis write SetTopAxis;
    property View3DWalls:Boolean read FView3DWalls write SetView3DWalls default True;

    { to be published events }
    property OnBeforeDrawChart: TNotifyEvent read FOnBeforeDrawChart write FOnBeforeDrawChart;
    property OnBeforeDrawAxes:TNotifyEvent read FOnBeforeDrawAxes write FOnBeforeDrawAxes;
    property OnBeforeDrawSeries:TNotifyEvent read FOnBeforeDrawSeries write FOnBeforeDrawSeries;
    property OnGetAxisLabel:TAxisOnGetLabel read FOnGetAxisLabel write FOnGetAxisLabel;
    property OnGetNextAxisLabel:TAxisOnGetNextLabel read FOnGetNextAxisLabel
                                                    write FOnGetNextAxisLabel;
    property OnPageChange:TNotifyEvent read FOnPageChange write FOnPageChange;
  end;

  TSeriesMarkPosition=class
  public
    ArrowFrom : TPoint;
    ArrowFix  : Boolean;
    ArrowTo   : TPoint;
    Custom    : Boolean;
    Height    : Integer;
    LeftTop   : TPoint;
    Width     : Integer;
    Procedure Assign(Source:TSeriesMarkPosition);
    Function Bounds:TRect;
  end;

  TSeriesMarksPositions=class(TList)
  private
    Procedure SetPosition(Index:Integer; APosition:TSeriesMarkPosition);
    Function GetPosition(Index:Integer):TSeriesMarkPosition;
  public
    Procedure Automatic(Index:Integer);
    {$IFNDEF D4}
    Destructor Destroy; override;
    {$ELSE}
    procedure Clear; override;
    {$ENDIF}
    Function ExistCustom:Boolean;
    property Position[Index:Integer]:TSeriesMarkPosition read GetPosition
                                    write SetPosition; default;
  end;

  TSeriesMarksStyle=( smsValue,             { 1234 }
                      smsPercent,           { 12 % }
                      smsLabel,             { Cars }
                      smsLabelPercent,      { Cars 12 % }
                      smsLabelValue,        { Cars 1234 }
                      smsLegend,            { (Legend.Style) }
                      smsPercentTotal,      { 12 % of 1234 }
                      smsLabelPercentTotal, { Cars 12 % of 1234 }
                      smsXValue,            { 1..2..3.. or 21/6/1996 }
                      smsXY                 { 123 456 }
                      );

  TSeriesMarksGradient=class(TChartGradient)
  public
    Constructor Create(ChangedEvent:TNotifyEvent); override;
  published
    property Direction default gdRightLeft;
    property EndColor default clWhite;
    property StartColor default clSilver;
  end;

  TSeriesMarks=class(TTeeCustomShape)
  private
    FAngle           : Integer;               //sr+
    FArrow           : TChartArrowPen;        //sr
    FArrowLength     : Integer;               //sr
    FClip            : Boolean;               //sr+
    FDrawEvery       : Integer;               //sr
    FMarkerStyle     : TSeriesMarksStyle;     //sr
    FMultiLine       : Boolean;               //sr+
    FParent          : TChartSeries;          //set
    FPositions       : TSeriesMarksPositions; //cr
    FZPosition       : Integer;               //sr+

    Function GetBackColor:TColor;
    Procedure InternalDraw(Index:Integer; AColor:TColor; Const St:String; APosition:TSeriesMarkPosition);
    Procedure SetAngle(Value:Integer);
    Procedure SetArrow(Value:TChartArrowPen);
    Procedure SetArrowLength(Value:Integer);
    Procedure SetBackColor(Value:TColor);
    Procedure SetClip(Value:Boolean);
    Procedure SetDrawEvery(Value:Integer);
    Procedure SetMultiline(Value:Boolean);
    Procedure SetStyle(Value:TSeriesMarksStyle);
  protected
    Function GetGradientClass:TChartGradientClass; override;
    Procedure UsePosition(Index:Integer; Var MarkPosition:TSeriesMarkPosition);
  public
    Constructor Create(AOwner: TChartSeries);
    Destructor  Destroy; override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TChartSeries);
    Procedure   Store(var S: TOldStream);
    Function    OnCompareItems(Item: TPersistent): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    Procedure ApplyArrowLength(APosition:TSeriesMarkPosition);
    Procedure Assign(Source:TPersistent); override;
    Function Clicked(X,Y:Integer):Integer;
    Procedure DrawText(Const R:TRect; Const St:String);
    property ParentSeries:TChartSeries read FParent;
    property Positions:TSeriesMarksPositions read FPositions;
    procedure ResetPositions;
    property ZPosition : Integer read FZPosition write FZPosition;
  published
    property Angle:Integer read FAngle write SetAngle default 0;
    property Arrow:TChartArrowPen read FArrow write SetArrow;
    property ArrowLength:Integer read FArrowLength write SetArrowLength;
    property BackColor:TColor read GetBackColor write SetBackColor default ChartMarkColor;
    property Brush;
    property Clip:Boolean read FClip write SetClip default False;
    property Color default ChartMarkColor;
    property DrawEvery:Integer read FDrawEvery write SetDrawEvery default 1;
    property Font;
    property Frame;
    property Gradient;
    property MultiLine:Boolean read FMultiLine write SetMultiLine default False;
    property ShadowColor default clGray;
    property ShadowSize default 1;
    property ShapeStyle;
    property Style:TSeriesMarksStyle read FMarkerStyle
                                     write SetStyle default smsLabel;
    property Transparent;
    property Visible;
  end;

  TSeriesOnBeforeAdd=Function(Sender:TChartSeries):Boolean of object;
  TSeriesOnAfterAdd=Procedure(Sender:TChartSeries; ValueIndex:Integer) of object;
  TSeriesOnClear=Procedure(Sender:TChartSeries) of object;
  TSeriesOnGetMarkText=Procedure(Sender:TChartSeries; ValueIndex:Integer; Var MarkText:String) of object;

  TSeriesRecalcOptions=set of (rOnDelete, rOnModify, rOnInsert, rOnClear);

  TFunctionPeriodStyle=( psNumPoints, psRange );
  TFunctionPeriodAlign=( paFirst,paCenter,paLast );

  TTeeFunction=class(TComponent)
  private
    FPeriod      : Double;
    FPeriodStyle : TFunctionPeriodStyle;
    FPeriodAlign : TFunctionPeriodAlign;
    FParent      : TChartSeries;

    IUpdating    : Boolean;
    Procedure SetPeriod(Const Value:Double);
    Procedure SetParentSeries(AParent:TChartSeries);
    Procedure SetPeriodAlign(Value:TFunctionPeriodalign);
    Procedure SetPeriodStyle(Value:TFunctionPeriodStyle);
  protected
    CanUsePeriod : Boolean;
    Procedure AddFunctionXY(YMandatorySource:Boolean; tmpX,tmpY:Double);
    Procedure CalculatePeriod( Source:TChartSeries;
                               Const tmpX:Double;
                               FirstIndex,LastIndex:Integer); virtual;
    Procedure CalculateAllPoints(Source:TChartSeries; NotMandatorySource:TChartValueList); virtual;
    Procedure CalculateByPeriod(Source:TChartSeries; NotMandatorySource:TChartValueList); virtual;
    Procedure DoCalculation( Source:TChartSeries;
                             NotMandatorySource:TChartValueList); virtual;
    Procedure InternalSetPeriod(Const APeriod:Double);
    procedure SetParentComponent(Value: TComponent); override;
    Function ValueList(ASeries:TChartSeries):TChartValueList;
  public
    Constructor Create(AOwner: TComponent); override;
    Procedure Assign(Source:TPersistent); override;

    procedure AddPoints(Source:TChartSeries); dynamic;
    procedure BeginUpdate;
    Function Calculate(SourceSeries:TChartSeries; First,Last:Integer):Double; virtual;
    Function CalculateMany(SourceSeriesList:TList; ValueIndex:Integer):Double; virtual;
    procedure EndUpdate;
    function GetParentComponent: TComponent; override;
    function HasParent: Boolean; override;
    property ParentSeries:TChartSeries read FParent write SetParentSeries;
    Procedure ReCalculate;
  published
    property Period:Double read FPeriod write SetPeriod;
    property PeriodAlign:TFunctionPeriodAlign read FPeriodAlign
                                              write SetPeriodAlign default paCenter;
    property PeriodStyle:TFunctionPeriodStyle read FPeriodStyle
                                              write SetPeriodStyle default psNumPoints;
  end;

  TTeeMovingFunction=class(TTeeFunction)
  protected
    Procedure DoCalculation( Source:TChartSeries;
                             NotMandatorySource:TChartValueList); override;
  public
    Constructor Create(AOwner:TComponent); override;
  published
    property Period;
  end;

  TChartValueLists=class(TList)
  private
    Function Get(Index:Integer):TChartValueList;
  public
    {$IFNDEF D4}
    Destructor Destroy; override;
    {$ELSE}
    Procedure Clear; override;
    {$ENDIF}
    property ValueList[Index:Integer]:TChartValueList read Get; default;
  end;

  TChartSeriesStyle=set of ( tssIsTemplate,
                             tssDenyChangeType,
                             tssDenyDelete,
                             tssDenyClone,
                             tssIsPersistent,
                             tssHideDataSource );

  TCustomChartSeries=class(TCustomChartElement)
  private
    FShowInLegend : Boolean;
    FTitle        : String;
    FIdentifier   : String;  { DecisionCube }
    FStyle        : TChartSeriesStyle; { DecisionCube }
    procedure ReadIdentifier(Reader: TReader);
    procedure WriteIdentifier(Writer: TWriter);
    procedure ReadStyle(Reader: TReader);
    procedure WriteStyle(Writer: TWriter);

    Procedure RepaintDesigner;
    Procedure SetShowInLegend(Value:Boolean);
    Procedure SetTitle(Const Value:String);
  protected
    InternalUse : Boolean;
    Procedure Added; dynamic;
    Procedure CalcHorizMargins(Var LeftMargin,RightMargin:Integer); virtual;
    Procedure CalcVerticalMargins(Var TopMargin,BottomMargin:Integer); virtual;
    procedure DefineProperties(Filer: TFiler); override;
    procedure DoBeforeDrawChart; virtual;
    Procedure GalleryChanged3D(Is3D:Boolean); dynamic;
    Procedure Removed; dynamic;
    Procedure SetActive(Value:Boolean); override;
  public
    Constructor Create(AOwner: TComponent); override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TComponent);
    Procedure   Store(var S: TOldStream);
    Function    OnCompareItems(Item: TComponent): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    procedure Assign(Source:TPersistent); override;
    Function SameClass(tmpSeries:TChartSeries):Boolean;

    property ShowInLegend:Boolean read FShowInLegend write SetShowInLegend default True;
    property Title:String read FTitle write SetTitle;
    { DSS, hidden }
    property Identifier:String read FIdentifier write FIdentifier;
    property Style:TChartSeriesStyle read FStyle write FStyle default [];
  end;

  TSeriesRandomBounds=packed record
    tmpX,StepX,tmpY,MinY,DifY:Double;
  end;

  TTeeFunctionClass=class of TTeeFunction;

  TTeeSeriesType=class(TObject)
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    SeriesType       : Byte;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    SeriesClass      : TChartSeriesClass;
    FunctionClass    : TTeeFunctionClass;
    Description      : String;
    GalleryPage      : String;
    NumGallerySeries : Integer;
  end;

  TChartSubGalleryProc=Function(Const AName:String):TCustomAxisPanel of object;

  TLegendTextStyle=( ltsPlain,ltsLeftValue,ltsRightValue,
                     ltsLeftPercent,ltsRightPercent,ltsXValue,ltsValue,
                     ltsPercent,ltsXAndValue,ltsXAndPercent);

  TChartSeries=class(TCustomChartSeries)
  private
    FColor              : TColor;                 //sr
    FColorEachPoint     : Boolean;                //sr+
    FColors             : TList;
    FColorSource        : String;
    FCursor             : TCursor;                //set
    FDataSources        : TList;                  //cr
    FDepth              : Integer;                //sr
    FFirstVisibleIndex  : Integer;                //sr
    FGetHorizAxis       : TChartAxis;
    FGetVertAxis        : TChartAxis;
    FLabelsSource       : String;
    FLastVisibleIndex   : Integer;                //sr
    FLinkedSeries       : TList;                  //cr
    FMarks              : TSeriesMarks;           //sr
    FPercentFormat      : String;                 //sr
    FTempDataSources    : TStringList;
    FValueFormat        : String;                 //sr
    FValuesList         : TChartValueLists;       //cr
    FX                  : TChartValueList;        //sr
    FXLabels            : TList;                  //cr
    FY                  : TChartValueList;        //sr

    FHorizAxis          : THorizAxis;             //sr
    FCustomHorizAxis    : TChartAxis;
    FCustomVertAxis     : TChartAxis;
    FZOrder             : Integer;                //sr

    FVertAxis           : TVertAxis;              //sr
    FRecalcOptions      : TSeriesRecalcOptions;   //sr

    FTeeFunction        : TTeeFunction;

    { Events }
    FAfterDrawValues    : TNotifyEvent;
    FBeforeDrawValues   : TNotifyEvent;
    FOnAfterAdd         : TSeriesOnAfterAdd;
    FOnBeforeAdd        : TSeriesOnBeforeAdd;
    FOnClearValues      : TSeriesOnClear;
    FOnClick            : TSeriesClick;
    FOnDblClick         : TSeriesClick;
    FOnGetMarkText      : TSeriesOnGetMarkText;

    Function CanAddRandomPoints:Boolean;
    Procedure FreeXLabel(ValueIndex:Integer);
    Function GetDataSource:TComponent;
    Function GetZOrder:Integer;
    Procedure GrowColors;
    Procedure InsertLabel(ValueIndex:Integer; Const ALabel:String);
    Procedure InternalAddDataSource(Value:TComponent);
    Function IsPercentFormatStored:Boolean;
    Function IsValueFormatStored:Boolean;
    procedure ReadCustomHorizAxis(Reader: TReader);
    procedure ReadCustomVertAxis(Reader: TReader);
    procedure ReadData(Reader: TReader);
    Procedure RecalcGetAxis;
    Procedure RemoveAllLinkedSeries;
    Procedure SetColorSource(Const Value:String);
    Procedure SetCustomHorizAxis(Value:TChartAxis);
    Procedure SetCustomVertAxis(Value:TChartAxis);
    Procedure SetDataSource(Value:TComponent);
    procedure SetDepth(const Value: Integer);
    Procedure SetHorizAxis(Value:THorizAxis);
    Procedure SetLabelsSource(Const Value:String);
    procedure SetMarks(Value:TSeriesMarks);
    Procedure SetPercentFormat(Const Value:String);
    Procedure SetValueColor(ValueIndex:Integer; AColor:TColor);
    Procedure SetValueFormat(Const Value:String);
    Procedure SetVertAxis(Value:TVertAxis);
    Procedure SetXLabel(ValueIndex:Integer; Const AXLabel:String);
    Procedure SetXValue(Index:Integer; Const Value:Double);
    Procedure SetYValue(Index:Integer; Const Value:Double);
    Procedure SetZOrder(Value:Integer);
    procedure WriteCustomHorizAxis(Writer: TWriter);
    procedure WriteCustomVertAxis(Writer: TWriter);
    procedure WriteData(Writer: TWriter);
  protected
    IUpdating       : Integer;
    IZOrder         : Integer;
    IUseSeriesColor : Boolean;                    //sr+

    Function AddChartValue(Source:TChartSeries; ValueIndex:Integer):Integer; virtual;
    Procedure Added; override;
    Procedure AddedValue(Source:TChartSeries; ValueIndex:Integer); virtual;
    Procedure AddLinkedSeries(ASeries:TChartSeries);
    Procedure AddSampleValues(NumValues:Integer); dynamic;
    Procedure AddValues(Source:TChartSeries); virtual;
    Procedure CalcFirstLastVisibleIndex;
    Procedure CalcZOrder; virtual;
    Procedure ClearLists; virtual;
    class Procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); virtual;
    procedure DefineProperties(Filer: TFiler); override;
    Procedure DeletedValue(Source:TChartSeries; ValueIndex:Integer); virtual;
    procedure DoAfterDrawValues; virtual;
    procedure DoBeforeDrawValues; virtual;
    procedure DrawAllValues; virtual;
    Procedure DrawLegendShape(ValueIndex:Integer; Const Rect:TRect); virtual;
    Procedure DrawMark(ValueIndex:Integer; Const St:String; APosition:TSeriesMarkPosition); virtual;
    procedure DrawMarks;
    procedure DrawValue(ValueIndex:Integer); virtual;
    Function FirstInZOrder:Boolean;
    Procedure GetChildren(Proc:TGetChildProc; Root:TComponent); override;
    Function GetMarkText(ValueIndex:Integer):String;
    Function GetValueColor(ValueIndex:Integer):TColor; virtual;
    Function GetXLabel(ValueIndex:Integer):String; virtual;
    Function GetxValue(ValueIndex:Integer):Double; virtual;  { conflicts with c++ wingdi.h }
    Function GetyValue(ValueIndex:Integer):Double; virtual;  { conflicts with c++ wingdi.h }
    Procedure Loaded; override;
    procedure Notification( AComponent: TComponent;
                            Operation: TOperation); override;
    Procedure NotifyNewValue(Sender:TChartSeries; ValueIndex:Integer); virtual;
    Procedure NotifyValue(ValueEvent:TValueEvent; ValueIndex:Integer); virtual;
    Function MoreSameZOrder:Boolean; virtual;
    Procedure PrepareForGallery(IsEnabled:Boolean); dynamic;
    Procedure PrepareLegendCanvas( ValueIndex:Integer; Var BackColor:TColor;
                                   Var BrushStyle:TBrushStyle); virtual;
    Procedure Removed; override;
    Procedure RemoveLinkedSeries(ASeries:TChartSeries);
    Procedure SetChartValueList( AValueList:TChartValueList;
                                 Value:TChartValueList);
    Procedure SetColorEachPoint(Value:Boolean); virtual;
    Procedure SetHorizontal;
    Procedure SetParentChart(Const Value:TCustomAxisPanel); override;
    Procedure SetSeriesColor(AColor:TColor); virtual;
    class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); virtual;
    Procedure SetXValues(Value:TChartValueList);
    Procedure SetYValues(Value:TChartValueList);
  public
    CalcVisiblePoints     : Boolean;              //sr
    DrawBetweenPoints     : Boolean;              //sr+
    AllowSinglePoint      : Boolean;              //sr
    HasZValues            : Boolean;              //sr+
    StartZ                : Integer;              //sr+
    MiddleZ               : Integer;              //sr+
    EndZ                  : Integer;              //sr+
    MandatoryValueList    : TChartValueList;      //set
    NotMandatoryValueList : TChartValueList;      //set
    YMandatory            : Boolean;              //sr

    Constructor Create(AOwner: TComponent); override;
    Destructor  Destroy; override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TComponent);
    Procedure   Store(var S: TOldStream);
    Function    OnCompareItems(Item: TCustomChartSeries): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    Function Add(Const AValue:Double; Const ALabel:String {$IFDEF D4}=''{$ENDIF};
                       AColor:TColor{$IFDEF D4}=clTeeColor{$ENDIF}):Integer; virtual;
    Function AddArray(Const Values:Array of TChartValue):Integer;
    Function AddNull(Const ALabel:String{$IFDEF D4}=''{$ENDIF}):Integer; virtual;
    Function AddNullXY(Const X,Y:Double; Const ALabel:String{$IFDEF D4}=''{$ENDIF}):Integer; virtual;
    Function AddX(Const AXValue:Double; Const ALabel:String {$IFDEF D4}=''{$ENDIF};
                         AColor:TColor{$IFDEF D4}=clTeeColor{$ENDIF}):Integer;
    Function AddXY(Const AXValue,AYValue:Double; Const ALabel:String {$IFDEF D4}=''{$ENDIF};
                         AColor:TColor {$IFDEF D4}=clTeeColor{$ENDIF}):Integer; virtual;
    Function AddY(Const AYValue:Double; Const ALabel:String {$IFDEF D4}=''{$ENDIF};
                         AColor:TColor {$IFDEF D4}=clTeeColor{$ENDIF}):Integer;

    procedure Assign(Source:TPersistent); override;
    Function AssociatedToAxis(Axis:TChartAxis):Boolean; virtual;
    Procedure BeginUpdate;
    Procedure CheckOrder;
    Procedure Clear; virtual;
    Function Count:Integer;
    Function CountLegendItems:Integer; virtual;
    Procedure Delete(ValueIndex:Integer); virtual;
    Procedure EndUpdate;
    Procedure FillSampleValues(NumValues:Integer);
    Function IsNull(ValueIndex:Integer):Boolean;
    Function IsValidSourceOf(Value:TChartSeries):Boolean; dynamic;
    Function IsValidSeriesSource(Value:TChartSeries):Boolean; dynamic;
    Function LegendToValueIndex(LegendIndex:Integer):Integer; virtual;
    Function LegendItemColor(LegendIndex:Integer):TColor; virtual;
    Function LegendString(LegendIndex:Integer; LegendTextStyle:TLegendTextStyle):String; virtual;
    property LinkedSeries:TList read FLinkedSeries;
    Function MaxXValue:Double; virtual;
    Function MaxYValue:Double; virtual;
    Function MaxZValue:Double; virtual;
    Function MinXValue:Double; virtual;
    Function MinYValue:Double; virtual;
    Function MinZValue:Double; virtual;
    Function NumSampleValues:Integer; dynamic;
    Function RandomBounds(NumValues:Integer):TSeriesRandomBounds;
    Procedure RemoveDataSource(Value:TComponent);
    Function VisibleCount:Integer; { <-- Number of VISIBLE points (Last-First+1) }

    property ValuesList:TChartValueLists read FValuesList;
    property XValue[Index:Integer]:Double read GetxValue write SetXValue;
    property YValue[Index:Integer]:Double read GetyValue write SetYValue;

    property ZOrder:Integer read GetZOrder write SetZOrder default TeeAutoZOrder;
    Function MaxMarkWidth:Integer;

    Function CalcXPos(ValueIndex:Integer):Integer; virtual;
    Function CalcXPosValue(Const Value:Double):Integer;
    Function CalcXSizeValue(Const Value:Double):Integer;

    Function CalcYPos(ValueIndex:Integer):Integer; virtual;
    Function CalcYPosValue(Const Value:Double):Integer;
    Function CalcYSizeValue(Const Value:Double):Integer;

    Function CalcPosValue(Const Value:Double):Integer;

    Function XScreenToValue(ScreenPos:Integer):Double;
    Function YScreenToValue(ScreenPos:Integer):Double;

    Function XValueToText(Const AValue:Double):String;
    Function YValueToText(Const AValue:Double):String;

    Procedure ColorRange( AValueList:TChartValueList;
                          Const FromValue,ToValue:Double;
                          AColor:TColor);
    Procedure CheckDataSource;

    { Public Properties }
    property Labels:TList read FXLabels;
    property XLabel[Index:Integer]:String read GetXLabel write SetXLabel;
    property ValueMarkText[Index:Integer]:String read GetMarkText;

    property ValueColor[Index:Integer]:TColor read GetValueColor write SetValueColor;
    property XValues:TChartValueList read FX write SetXValues;
    property YValues:TChartValueList read FY write SetYValues;

    Function GetYValueList(AListName:String):TChartValueList; virtual;
    property GetVertAxis:TChartAxis read FGetVertAxis;
    property GetHorizAxis:TChartAxis read FGetHorizAxis;
    Function MarkPercent(ValueIndex:Integer; AddTotal:Boolean):String;
    Function Clicked(x,y:Integer):Integer; virtual;
    Procedure RefreshSeries;
    property FirstValueIndex:Integer read FFirstVisibleIndex;
    property LastValueIndex:Integer read FLastVisibleIndex;
    Function GetOriginValue(ValueIndex:Integer):Double; virtual;
    Function GetMarkValue(ValueIndex:Integer):Double; virtual;
    Procedure AssignValues(Source:TChartSeries);
    Function DrawValuesForward:Boolean; virtual;
    Function DrawSeriesForward(ValueIndex:Integer):Boolean; virtual;
    procedure SwapValueIndex(a,b:Integer); dynamic;
    property RecalcOptions: TSeriesRecalcOptions read FRecalcOptions
                                                 write FRecalcOptions
                                       default [ rOnDelete,
                                                 rOnModify,
                                                 rOnInsert,
                                                 rOnClear];
    Function GetCursorValueIndex:Integer;
    Procedure GetCursorValues(Var XValue,YValue:Double);
    Procedure DrawLegend(ValueIndex:Integer; Const Rect:TRect); virtual;
    Function UseAxis:Boolean; virtual;
    procedure SetFunction(AFunction:TTeeFunction); virtual;

    { other }
    property DataSources:TList read FDataSources;
    property FunctionType:TTeeFunction read FTeeFunction;
    Procedure CheckOtherSeries(Source:TChartSeries);
    Procedure ReplaceList(OldList,NewList:TChartValueList);
    property CustomHorizAxis:TChartAxis read FCustomHorizAxis write SetCustomHorizAxis;
    property CustomVertAxis:TChartAxis read FCustomVertAxis write SetCustomVertAxis;

    { to be published }
    property Active;
    property ColorEachPoint:Boolean read FColorEachPoint write SetColorEachPoint default False;
    property ColorSource:String read FColorSource write SetColorSource;
    property Cursor:TCursor read FCursor write FCursor default crDefault;
    property Depth:Integer read FDepth write SetDepth default TeeAutoDepth;
    property HorizAxis:THorizAxis read FHorizAxis write SetHorizAxis default aBottomAxis;
    property Marks:TSeriesMarks read FMarks write SetMarks;
    property ParentChart;
    { datasource below parentchart }
    property DataSource:TComponent read GetDataSource write SetDataSource;
    property PercentFormat:String read FPercentFormat write SetPercentFormat stored IsPercentFormatStored;
    property SeriesColor:TColor read FColor write SetSeriesColor default clTeeColor;
    property ShowInLegend;
    property Title;
    property ValueFormat:String read FValueFormat write SetValueFormat stored IsValueFormatStored;
    property VertAxis:TVertAxis read FVertAxis write SetVertAxis default aLeftAxis;
    property XLabelsSource:String read FLabelsSource write SetLabelsSource;

    { events }
    property AfterDrawValues:TNotifyEvent read FAfterDrawValues
                                           write FAfterDrawValues;
    property BeforeDrawValues:TNotifyEvent read FBeforeDrawValues
                                           write FBeforeDrawValues;
    property OnAfterAdd:TSeriesOnAfterAdd read FOnAfterAdd write FOnAfterAdd;
    property OnBeforeAdd:TSeriesOnBeforeAdd read FOnBeforeAdd write FOnBeforeAdd;
    property OnClearValues:TSeriesOnClear read FOnClearValues
                                          write FOnClearValues;
    property OnClick:TSeriesClick read FOnClick write FOnClick;
    property OnDblClick:TSeriesClick read FOnDblClick write FOnDblClick;
    property OnGetMarkText:TSeriesOnGetMarkText read FOnGetMarkText
                                                write FOnGetMarkText;
  end;

  ChartException=class(Exception);

Const TeeAxisClickGap    : Integer=3; { minimum pixels distance to trigger axis click }
      TeeDefaultCapacity : Integer=1000; { default TList.Capacity to speed-up Lists }

Function SeriesTitleOrName(ASeries:TCustomChartSeries):String;
Procedure FillSeriesItems(AItems:TStrings; AChart:TCustomAxisPanel);
Procedure TeeSplitInLines(Var St:String; Const Separator:String);

implementation

Uses TeeConst
     {$IFNDEF CLX}
     ,Printers
     {$ENDIF}
     {$IFDEF D6}
     ,Types
     {$ENDIF}
     {$IFDEF TEEARRAY}
     {$IFOPT R+}
     ,Consts
     {$ENDIF}
     {$ENDIF}
     {$IFDEF TEETRIAL}
     ,TeeAbout
     {$ENDIF}
     ;

{ Returns a "good" value, bigger than "OldStep", as 2..5..10..etc }
Function TeeNextStep(Const OldStep:Double):Double;
Begin
  if OldStep >= 10 then result := 10*TeeNextStep(0.1*OldStep)
  else
  if OldStep < 1 then result := 0.1*TeeNextStep(OldStep*10)
  else
  if OldStep < 2 then result:=2 else
  if OldStep < 5 then result:=5 else result:=10
end;

{ raises an exception if Value <0 or >360 }
Procedure TeeCheckAngle(Value:Integer; Const Description:String);
Begin
  if (Value<0) or (Value>360) then
     Raise ChartException.CreateFmt(TeeMsg_Angle,[Description]);
end;

{ TChartAxisTitle }
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TChartAxisTitle.Load(var S: TOldStream; AOwner: TCustomTeePanel);
begin
   S.Read(IDefaultAngle,SizeOf(Integer));
   S.Read(FAngle,SizeOf(Integer));
   FCaption:=S.ReadString;
   inherited Load(S,AOwner);
end;

Procedure TChartAxisTitle.Store(var S: TOldStream);
begin
   S.Write(IDefaultAngle,SizeOf(Integer));
   S.Write(FAngle,SizeOf(Integer));
   S.WriteString(FCaption);
   inherited;
end;

Function TChartAxisTitle.OnCompareItems(Item: TPersistent): Boolean;
begin
   Result:=FALSE;
   if Item is TChartAxisTitle then
      with TChartAxisTitle(Item) do
         if Self.FCaption = FCaption then
            if Self.FAngle = FAngle then
               if Self.IDefaultAngle = IDefaultAngle then
                  Result := inherited OnCompareItems(Item);
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TChartAxisTitle.Assign(Source:TPersistent);
begin
   if Source is TChartAxisTitle then
      with TChartAxisTitle(Source) do
         begin
            Self.FAngle  :=FAngle;
            Self.FCaption:=FCaption;
         end;
   inherited;
end;

Function TChartAxisTitle.IsAngleStored:Boolean;
begin
  result:=FAngle<>IDefaultAngle;
end;

Procedure TChartAxisTitle.SetAngle(Value:Integer);
Begin
  if FAngle<>Value then
  begin
    TeeCheckAngle(Value,TeeMsg_AxisTitle);
    FAngle:=Value;
    Repaint;
  end;
end;

type TTeePanelAccess=class(TCustomTeePanel);

Procedure TChartAxisTitle.SetCaption(Const Value:String);
Begin
  TTeePanelAccess(ParentChart).SetStringProperty(FCaption,Value);
end;

type TOwnedCollectionAccess=class(TOwnedCollection);

{ TChartAxis }
Constructor TChartAxis.Create(Collection: Classes.TCollection);
begin
   FParentChart:=TCustomAxisPanel(TOwnedCollectionAccess(Collection).GetOwner);
   with FParentChart do
      if FAxes.Count>=5 then
         inherited Create(Collection)
      else
         inherited Create(nil);
   FLabels:=True;
   FLogarithmicBase:=10;
   FAutomatic:=True;
   FAutomaticMaximum:=True;
   FAutomaticMinimum:=True;
   FLabelsSeparation:=10; { % }
   FAxisValuesFormat:=TeeMsg_DefValueFormat;
   FLabelsAlign:=alDefault;
   FLabelStyle:=talAuto;
   FLabelsOnAxis:=True;
   FLabelsFont:=TTeeFont.Create(FParentChart.CanvasChanged);

   FTickOnLabelsOnly:=True;

   FAxisTitle:=TChartAxisTitle.Create(ParentChart);
   FAxisTitle.IDefaultAngle:=0;

   FTicks:=TDarkGrayPen.Create(ParentChart.CanvasChanged);
   FTickLength:=4;

   FMinorTicks:=TDarkGrayPen.Create(ParentChart.CanvasChanged);
   FMinorTickLength :=2;
   FMinorTickCount  :=3;

   FTicksInner:=TDarkGrayPen.Create(ParentChart.CanvasChanged);
   FGrid:=TDottedGrayPen.Create(ParentChart.CanvasChanged);
   FMinorGrid:=TChartHiddenPen.Create(ParentChart.CanvasChanged);

   FAxis:=TChartAxisPen.Create(ParentChart.CanvasChanged);

   FVisible         :=True;
   RoundFirstLabel  :=True;
   FExactDateTime   :=True;
   FEndPosition     :=100;
   FParentChart.FAxes.Add(Self);
end;

Destructor TChartAxis.Destroy;

   Procedure ResetSeriesAxes;
   var t : Integer;
   begin
      with FParentChart do
         for t:=0 to SeriesCount-1 do
             with TChartSeries(Series[t]) do
                begin
                   if CustomHorizAxis=Self then
                      begin
                         CustomHorizAxis:=nil;
                         HorizAxis:=aBottomAxis;
                      end;
                   if CustomVertAxis=Self then
                      begin
                         CustomVertAxis:=nil;
                         VertAxis:=aLeftAxis;
                      end;
                end;
   end;

begin
   FMinorTicks.Free;
   FTicks.Free;
   FTicksInner.Free;
   FGrid.Free;
   FMinorGrid.Free;
   FAxis.Free;
   FAxisTitle.Free;
   FLabelsFont.Free;
   ResetSeriesAxes;
   FParentChart.FAxes.Remove(Self);
   inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TChartAxis.Load(var S: TOldStream; Collection: Classes.TCollection);
var byteValue   : Byte;
    smlValue    : SmallInt;
begin
   FParentChart:=TCustomAxisPanel(TOwnedCollectionAccess(Collection).GetOwner);
   with FParentChart do
      if FAxes.Count>=5 then
         inherited Create(Collection)
      else
         inherited Create(nil);

   // Unpack values (this method slightly limits the maximum value of FLogarithmicBase)
   S.Read(byteValue,SizeOf(byteValue));
   FLogarithmicBase:=byteValue;
   S.Read(byteValue,SizeOf(byteValue));
   FLabels:=(byteValue and co_Bit0) <> 0;
   FLogarithmic:=(byteValue and co_Bit1) <> 0;
   FAutomatic:=(byteValue and co_Bit2) <> 0;
   FAutomaticMaximum:=(byteValue and co_Bit3) <> 0;
   FAutomaticMinimum:=(byteValue and co_Bit4) <> 0;
   FLabelsExponent:=(byteValue and co_Bit5) <> 0;
   FLabelsMultiLine:=(byteValue and co_Bit6) <> 0;
   FLabelsOnAxis:=(byteValue and co_Bit7) <> 0;

   S.Read(FDesiredIncrement,SizeOf(Double));
   S.Read(FMaximumValue,SizeOf(Double));
   S.Read(FMinimumValue,SizeOf(Double));

   // Unpack value (this method slightly limits the maximum value of FLabelsSeparation)
   S.Read(byteValue,SizeOf(byteValue));
   FLabelsSeparation:=byteValue;

   // Unpack value (this method slightly limits the maximum value of FLabelsSize)
//   S.Read(smlValue,SizeOf(SmallInt));
//   FLabelsSize:=smlValue;

   FAxisValuesFormat:=S.ReadString;
   FDateTimeFormat:=S.ReadString;

   // Unpack values
   S.Read(smlValue,SizeOf(SmallInt));
   FLabelStyle:=TAxisLabelStyle(smlValue and $0007);
   FLabelsAlign:=TAxisLabelAlign((smlValue shr 3) and co_Bit0);
   FTickOnLabelsOnly:=(smlValue and co_Bit4) <> 0;
   FRoundFirstLabel:=(smlValue and co_Bit5) <> 0;
   FExactDateTime:=(smlValue and co_Bit6) <> 0;
   FLabelsAngle:=smlValue shr 7;

   FLabelsFont:=TTeeFont.Load(S,FParentChart.CanvasChanged);

   FAxisTitle:=TChartAxisTitle.Load(S,ParentChart);

   // Unpack value (this method slightly limits the maximum value of FTitleSize)
   S.Read(smlValue,SizeOf(SmallInt));
   FGridCentered:=(smlValue and co_Bit0) <> 0;
   FInverted:=(smlValue and co_Bit1) <> 0;
   FHorizontal:=(smlValue and co_Bit2) <> 0;
   FOtherSide:=(smlValue and co_Bit3) <> 0;
   FVisible:=(smlValue and co_Bit4) <> 0;
   FTitleSize:=smlValue shr 5;

   // Unpack value (this method slightly limits the maximum value of FTickLength)
   S.Read(byteValue,SizeOf(byteValue));
   FTickLength:=byteValue;
   // Unpack value (this method slightly limits the maximum value of FTickInnerLength)
   S.Read(byteValue,SizeOf(byteValue));
   FTickInnerLength:=byteValue;
   // Unpack value (this method slightly limits the maximum value of FMinorTickLength)
   S.Read(byteValue,SizeOf(byteValue));
   FMinorTickLength:=byteValue;
   // Unpack value (this method slightly limits the maximum value of FMinorTickCount)
   S.Read(byteValue,SizeOf(byteValue));
   FMinorTickCount:=byteValue;

   FTicks:=TDarkGrayPen.Load(S,ParentChart.CanvasChanged);
   FMinorTicks:=TDarkGrayPen.Load(S,ParentChart.CanvasChanged);
   FTicksInner:=TDarkGrayPen.Load(S,ParentChart.CanvasChanged);

   FGrid:=TDottedGrayPen.Load(S,ParentChart.CanvasChanged);
   FMinorGrid:=TChartHiddenPen.Load(S,ParentChart.CanvasChanged);

   FAxis:=TChartAxisPen.Load(S,ParentChart.CanvasChanged);

   S.Read(FStartPosition,SizeOf(Double));
   S.Read(FEndPosition,SizeOf(Double));
   S.Read(FPositionPercent,SizeOf(Double));

   FParentChart.FAxes.Add(Self);
end;

Procedure TChartAxis.Store(var S: TOldStream);
var byteValue   : Byte;
    smlValue    : SmallInt;
begin
   // Pack values (this method slightly limits the maximum value of FLogarithmicBase)
   byteValue:=FLogarithmicBase and $000000FF;
   S.Write(byteValue,SizeOf(byteValue));

   if FLabels then
      byteValue:=co_Bit0
   else
      byteValue:=0;
   if FLogarithmic then
      byteValue:=byteValue or co_Bit1;
   if FAutomatic then
      byteValue:=byteValue or co_Bit2;
   if FAutomaticMaximum then
      byteValue:=byteValue or co_Bit3;
   if FAutomaticMinimum then
      byteValue:=byteValue or co_Bit4;
   if FLabelsExponent then
      byteValue:=byteValue or co_Bit5;
   if FLabelsMultiLine then
      byteValue:=byteValue or co_Bit6;
   if FLabelsOnAxis then
      byteValue:=byteValue or co_Bit7;
   S.Write(byteValue,SizeOf(byteValue));

   S.Write(FDesiredIncrement,SizeOf(Double));
   S.Write(FMaximumValue,SizeOf(Double));
   S.Write(FMinimumValue,SizeOf(Double));

   // Pack value (this method slightly limits the maximum value of FLabelsSeparation)
   byteValue:=FLabelsSeparation and $000000FF;
   S.Write(byteValue,SizeOf(byteValue));
   // Pack value (this method slightly limits the maximum value of FLabelsSize)
//   smlValue:=FLabelsSize;
//   S.Write(smlValue,SizeOf(SmallInt));
   
   S.WriteString(FAxisValuesFormat);
   S.WriteString(FDateTimeFormat);

   // Pack values
   byteValue:=Ord(FLabelStyle);
   if FLabelsAlign = alOpposite then
      byteValue:=byteValue or co_Bit3;
   if FTickOnLabelsOnly then
      byteValue:=byteValue or co_Bit4;
   if FRoundFirstLabel then
      byteValue:=byteValue or co_Bit5;
   if FExactDateTime then
      byteValue:=byteValue or co_Bit6;
   smlValue:=FLabelsAngle mod 360;
   if smlValue < 0 then
      smlValue:=smlValue+360;
   smlValue:=smlValue shl 7;
   smlValue:=smlValue or byteValue;
   S.Write(smlValue,SizeOf(SmallInt));

   FLabelsFont.Store(S);

   FAxisTitle.Store(S);

   // Pack value (this method slightly limits the maximum value of FTitleSize)
   smlValue:=FTitleSize and $000007FF;
   smlValue:=smlValue shl 5;
   if FGridCentered then
      smlValue:=smlValue or co_Bit0;
   if FInverted then
      smlValue:=smlValue or co_Bit1;
   if FHorizontal then
      smlValue:=smlValue or co_Bit2;
   if FOtherSide then
      smlValue:=smlValue or co_Bit3;
   if FVisible then
      smlValue:=smlValue or co_Bit4;
   S.Write(smlValue,SizeOf(SmallInt));

   // Pack value (this method slightly limits the maximum value of FTickLength)
   byteValue:=FTickLength and $000000FF;
   S.Write(byteValue,SizeOf(byteValue));
   // Pack value (this method slightly limits the maximum value of FTickInnerLength)
   byteValue:=FTickInnerLength and $000000FF;
   S.Write(byteValue,SizeOf(byteValue));
   // Pack value (this method slightly limits the maximum value of FMinorTickLength)
   byteValue:=FMinorTickLength and $000000FF;
   S.Write(byteValue,SizeOf(byteValue));
   // Pack value (this method slightly limits the maximum value of FMinorTickCount)
   byteValue:=FMinorTickCount and $000000FF;
   S.Write(byteValue,SizeOf(byteValue));

   FTicks.Store(S);
   FMinorTicks.Store(S);
   FTicksInner.Store(S);

   FGrid.Store(S);
   FMinorGrid.Store(S);

   FAxis.Store(S);

   S.Write(FStartPosition,SizeOf(Double));
   S.Write(FEndPosition,SizeOf(Double));
   S.Write(FPositionPercent,SizeOf(Double));
end;

Function TChartAxis.OnCompareItems(Item: TCollectionItem): Boolean;
var Difference : Integer;
    PermisDiff : Integer;
begin
   Result:=FALSE;
   if Item is TChartAxis then
     with TChartAxis(Item) do
       begin
         if Self.FInverted = FInverted then
          if Self.FHorizontal = FHorizontal then
           if Self.FOtherSide = FOtherSide then
            if Self.FPositionPercent = FPositionPercent then
             if Self.FStartPosition = FStartPosition then
              if Self.FEndPosition = FEndPosition then
               if Self.FLogarithmic = FLogarithmic then
                if Self.FAutomatic = FAutomatic then
                 if Self.FDesiredIncrement = FDesiredIncrement then
                  if Self.FVisible = FVisible then
                   if Self.FVisible then
                     begin
                       if Self.FLabels = FLabels then
                        if Self.FLabels then
                          begin
                            if Self.FRoundFirstLabel = FRoundFirstLabel then
                             if Self.FLabelsSeparation = FLabelsSeparation then
                              if Self.FLabelsAlign = FLabelsAlign then
                               if Self.FLabelStyle = FLabelStyle then
                                if Self.FLabelsAngle = FLabelsAngle then
                                 if Self.FLabelsExponent = FLabelsExponent then
                                  if Self.FLabelsMultiLine = FLabelsMultiLine then
                                   if Self.FLabelsOnAxis = FLabelsOnAxis then
                                    if Self.FAxisValuesFormat = FAxisValuesFormat then
//                                   if Self.FDateTimeFormat = FDateTimeFormat then
                                       begin
                                         Difference:=Abs(Self.FLabelsSize-FLabelsSize);
                                         if Difference > 1  then
                                            begin
                                               PermisDiff:=Round(Self.FLabelsSize*co_PermisPercent);
                                               if Difference > PermisDiff  then
                                                  Exit;
                                            end;
                                         Result := Self.FLabelsFont.OnCompareItems(FLabelsFont)
                                       end;
                          end
                        else
                          Result:=TRUE;

                        if Result then
                          begin
                            Result:=FALSE;
                            if Self.FAxisTitle.Visible = FAxisTitle.Visible then
                              if Self.FAxisTitle.Visible then
                                begin
                                  if Self.FTitleSize = FTitleSize then
                                    Result := Self.FAxisTitle.OnCompareItems(FAxisTitle);
                                end
                              else
                                Result:=TRUE;
                          end;

                        if Result then
                          begin
                            Result:=FALSE;
                            if Self.FTickOnLabelsOnly = FTickOnLabelsOnly then
                              begin
                                if Self.FTicks.Visible = FTicks.Visible then
                                   if Self.FTicks.Visible then
                                     begin
                                       if Self.FTickLength = FTickLength then
                                         Result:=Self.FTicks.OnCompareItems(FTicks);
                                     end
                                   else
                                     Result:=TRUE;

                                if Result then
                                  begin
                                    Result:=FALSE;
                                    if Self.FTicksInner.Visible = FTicksInner.Visible then
                                      if Self.FTicksInner.Visible then
                                        begin
                                          if Self.FTickInnerLength = FTickInnerLength then
                                            Result:=Self.FTicksInner.OnCompareItems(FTicksInner);
                                        end
                                      else
                                        Result:=TRUE;
                                  end;

                                if Result then
                                   begin
                                     Result:=FALSE;
                                     if Self.FGrid.Visible = FGrid.Visible then
                                       if Self.FGrid.Visible then
                                         begin
                                           if Self.FRoundFirstLabel = FRoundFirstLabel then
                                             if Self.FLabelsSeparation = FLabelsSeparation then
                                               if Self.FGridCentered = FGridCentered then
                                                 Result:=Self.FGrid.OnCompareItems(FGrid);
                                         end
                                       else
                                         Result:=TRUE;
                                   end;

                                if Result then
                                  begin
                                    Result:=FALSE;
                                    if Self.FMinorTicks.Visible = FMinorTicks.Visible then
                                      if Self.FMinorTicks.Visible then
                                        begin
                                          if Self.FMinorTickLength = FMinorTickLength then
                                            if Self.FMinorTickCount = FMinorTickCount then
                                               Result:=Self.FMinorTicks.OnCompareItems(FMinorTicks);
                                        end
                                      else
                                        Result:=TRUE;
                                  end;

                                if Result then
                                  begin
                                    Result:=FALSE;
                                    if Self.FMinorGrid.Visible = FMinorGrid.Visible then
                                      if Self.FMinorGrid.Visible then
                                        begin
                                          if Self.FMinorTickCount = FMinorTickCount then
                                            Result:=Self.FMinorGrid.OnCompareItems(FMinorGrid);
                                        end
                                      else
                                        Result:=TRUE;
                                  end;

                                if Result then
                                  Result:=Self.FAxis.OnCompareItems(FAxis);
                              end;
                          end;
                     end
                   else
                     Result:=TRUE;

         if Result then
            if Self.FLogarithmic then
               Result:=Self.FLogarithmicBase = FLogarithmicBase;

         if Result then
            if not Self.FAutomatic then
               begin
                  Result:=FALSE;
                  if Self.FAutomaticMaximum = FAutomaticMaximum then
                     if Self.FAutomaticMinimum = FAutomaticMinimum then
                        begin
                           Result:=Self.FAutomaticMaximum;
                           if not Self.FAutomaticMaximum then
                              Result:=Self.FMaximumValue = FMaximumValue;
                           if Result then
                              if not Self.FAutomaticMinimum then
                                 Result:=Self.FMinimumValue = FMinimumValue;
                        end;
               end;
       end;   // End of the WITH-statement
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TChartAxis.IncDecDateTime( Increment:Boolean;
                                           Var Value:Double;
                                           Const AnIncrement:Double;
                                           tmpWhichDateTime:TDateTimeStep);
begin
  TeeDateTimeIncrement( FExactDateTime and IAxisDateTime and
                        (tmpWhichDateTime>=dtHalfMonth),
                        Increment,
                        Value,
                        AnIncrement,
                        tmpWhichDateTime );
end;

{ An axis is "DateTime" if at least one Active Series with
  datetime values is associated to it }
Function TChartAxis.IsDateTime:Boolean;
Var tmpSeries : Integer;
Begin
  With ParentChart do
  for tmpSeries:=0 to SeriesCount-1 do
  With Series[tmpSeries] do
  if FActive {or NoneActive(Self)} then
      if AssociatedToAxis(Self) then
      begin
        if Self.Horizontal then result:=FX.DateTime
                           else result:=FY.DateTime;
        Exit;
      end;
  result:=False;
end;

Procedure TChartAxis.SetTicks(Value:TDarkGrayPen);
Begin
  FTicks.Assign(Value);
end;

Procedure TChartAxis.SetMinorTicks(Value:TDarkGrayPen);
Begin
  FMinorTicks.Assign(Value);
end;

Procedure TChartAxis.SetTicksInner(Value:TDarkGrayPen);
Begin
  FTicksInner.Assign(Value);
end;

Procedure TChartAxis.SetGrid(Value:TDottedGrayPen);
Begin
  FGrid.Assign(Value);
end;

Procedure TChartAxis.SetMinorGrid(Value:TChartHiddenPen);
Begin
  FMinorGrid.Assign(Value);
end;

Procedure TChartAxis.SetGridCentered(Value:Boolean);
Begin
  ParentChart.SetBooleanProperty(FGridCentered,Value);
end;

Procedure TChartAxis.SetAxis(Value:TChartAxisPen);
Begin
  FAxis.Assign(Value);
end;

Function TChartAxis.IsPosStored:Boolean;
begin
  result:=FPositionPercent<>0;
end;

Function TChartAxis.IsStartStored:Boolean;
begin
  result:=FStartPosition<>0;
end;

Function TChartAxis.IsEndStored:Boolean;
begin
  result:=FEndPosition<>100;
end;

Function TChartAxis.IsCustom:Boolean;
begin
  result:=ParentChart.Axes.IndexOf(Self)>4;
end;

procedure TChartAxis.SetHorizontal(const Value: Boolean);
begin
  ParentChart.SetBooleanProperty(FHorizontal,Value);
end;

procedure TChartAxis.SetOtherSide(const Value: Boolean);
begin
  ParentChart.SetBooleanProperty(FOtherSide,Value);
end;

Function TChartAxis.CalcPosPoint(Value:Integer):Double;

  Function InternalCalcPos(Const A,B:Double):Double;
  begin
    if (Horizontal and FInverted) or
       ((not Horizontal) and (not FInverted)) then result:=A
                                              else result:=B
  end;

var tmp    : Double;
    LogMin : Double;
    LogMax : Double;
Begin
  if FLogarithmic then
  Begin
    if Value=IStartPos then result:=InternalCalcPos(IMaximum,IMinimum)
    else
    if Value=IEndPos then result:=InternalCalcPos(IMinimum,IMaximum)
    else
    begin
      tmp:=InternalCalcLog(LogMax,LogMin);
      if tmp=0 then result:=IMinimum
      else
      begin
        if FInverted then tmp:=((IEndPos-Value)*tmp/IAxisSize)
                     else tmp:=((Value-IStartPos)*tmp/IAxisSize);
        if Horizontal then result:=Exp(LogMin+tmp)
                      else result:=Exp(LogMax-tmp);
      end;
    end;
  end
  else
  if IAxisSize>0 then
  begin
    if FInverted then tmp:=IEndPos-Value
                 else tmp:=Value-IStartPos;
    tmp:=tmp*IRange/IAxisSize;
    if Horizontal then result:=IMinimum+tmp
                  else result:=IMaximum-tmp;
  end
  else result:=0;
end;

Procedure TChartAxis.SetDateTimeFormat(Const Value:String);
Begin
  ParentChart.SetStringProperty(FDateTimeFormat,Value);
end;

procedure TChartAxis.SetAxisTitle(Value:TChartAxisTitle);
begin
  FAxisTitle.Assign(Value);
end;

procedure TChartAxis.SetStartPosition(Const Value:Double);
begin
  ParentChart.SetDoubleProperty(FStartPosition,Value);
end;

procedure TChartAxis.SetEndPosition(Const Value:Double);
begin
  ParentChart.SetDoubleProperty(FEndPosition,Value);
end;

procedure TChartAxis.SetPositionPercent(Const Value:Double);
begin
  ParentChart.SetDoubleProperty(FPositionPercent,Value);
end;

procedure TChartAxis.SetRoundFirstLabel(Value:Boolean);
begin
  ParentChart.SetBooleanProperty(FRoundFirstLabel,Value);
end;

Procedure TChartAxis.SetLabelsMultiLine(Value:Boolean);
begin
  ParentChart.SetBooleanProperty(FLabelsMultiLine,Value);
end;

Procedure TChartAxis.SetLabelsExponent(Value:Boolean);
begin
  ParentChart.SetBooleanProperty(FLabelsExponent,Value);
end;

procedure TChartAxis.SetTickOnLabelsOnly(Value:Boolean);
begin
  ParentChart.SetBooleanProperty(FTickOnLabelsOnly,Value);
end;

Function TChartAxis.CalcDateTimeIncrement(MaxNumLabels:Integer):Double;
Var TempNumLabels : Integer;
Begin
  result:=MaxDouble(FDesiredIncrement,DateTimeStep[dtOneMilliSecond]);
  if (result>0) and (MaxNumLabels>0) then
  begin
    if (IRange/Result)>1000000 then Result:=IRange/1000000;
    Repeat
      TempNumLabels:=Round(IRange/result);
      if TempNumLabels>MaxNumLabels then
         if result<DateTimeStep[dtOneYear] then
            result:=NextDateTimeStep(result)
         else
            result:=2.0*result;
    Until (TempNumLabels<=MaxNumLabels){ or (result=DateTimeStep[dtOneYear])};
  end;
  result:=MaxDouble(result,DateTimeStep[dtOneMilliSecond]);
end;

Function TChartAxis.CalcLabelsIncrement(MaxNumLabels:Integer):Double;

  Procedure InternalCalcLabelsIncrement;
  Var TempNumLabels : Integer;
      tmp           : Double;
  Begin
    if FDesiredIncrement<=0 then
    Begin
      Result:=Abs(IRange);
      if Result<1 then Result:=MinAxisIncrement
                  else Result:=1;
    end
    else
      Result:=FDesiredIncrement;

    TempNumLabels:=MaxNumLabels+1;
    if LabelsSeparation>=0 then
    Repeat
      tmp:=IRange/Result;
      if Abs(tmp)<MaxLongint then
      begin
        TempNumLabels:=Round(tmp);
        if TempNumLabels>MaxNumLabels then Result:=TeeNextStep(Result);
      end
      else Result:=TeeNextStep(Result);
    Until (TempNumLabels<=MaxNumLabels);

    result:=MaxDouble(Result,MinAxisIncrement);
  end;

Begin
  if MaxNumLabels>0 then
  Begin
    if IAxisDateTime then result:=CalcDateTimeIncrement(MaxNumLabels)
                     else InternalCalcLabelsIncrement;
  end
  else
  if IAxisDateTime then result:=DateTimeStep[dtOneMilliSecond]
                   else result:=MinAxisIncrement;
end;

Function TChartAxis.LabelWidth(Const Value:Double):Integer;
var tmp : Integer;
Begin
  result:=ParentChart.MultiLineTextWidth(LabelValue(Value),tmp);
  if (FLabelsAngle=90) or (FLabelsAngle=270) then
     result:=ParentChart.Canvas.FontHeight*tmp;
End;

Function TChartAxis.LabelHeight(Const Value:Double):Integer;
var tmp : Integer;
Begin
  result:=ParentChart.MultiLineTextWidth(LabelValue(Value),tmp);
  if (FLabelsAngle=0) or (FLabelsAngle=180) then
     result:=ParentChart.Canvas.FontHeight*tmp;
End;

Function TChartAxis.IsMaxStored:Boolean;
Begin { dont store max property if automatic }
  result:=(not FAutomatic) and (not FAutomaticMaximum);
end;

Function TChartAxis.IsMinStored:Boolean;
Begin{ dont store min property if automatic }
  result:=(not FAutomatic) and (not FAutomaticMinimum);
end;

Function TChartAxis.CalcXYIncrement(MaxLabelSize:Integer):Double;
var tmp : Integer;
begin
  if MaxLabelSize>0 then
  begin
    if FLabelsSeparation>0 then
       Inc(MaxLabelSize,Round(0.01*FLabelsSeparation*MaxLabelSize));
    tmp:=Round((1.0*IAxisSize)/MaxLabelSize)
  end
  else tmp:=1;
  result:=CalcLabelsIncrement(tmp)
end;

Function TChartAxis.CalcIncrement:Double;
var tmp : Integer;
Begin
  if Horizontal then tmp:=MaxLong(LabelWidth(IMinimum),LabelWidth(IMaximum))
                else tmp:=MaxLong(LabelHeight(IMinimum),LabelHeight(IMaximum));
  result:=CalcXYIncrement(tmp);
End;

Procedure TChartAxis.AdjustMaxMinRect(Const Rect:TRect);
Var tmpMin : Double;
    tmpMax : Double;

    Procedure RecalcAdjustedMinMax(Pos1,Pos2:Integer);
    Var OldStart : Integer;
        OldEnd   : Integer;
    Begin
      OldStart :=IStartPos;
      OldEnd   :=IEndPos;
      Inc(IStartPos,Pos1);
      Dec(IEndPos,Pos2);
      IAxisSize:=IEndPos-IStartPos;
      tmpMin:=CalcPosPoint(OldStart);
      tmpMax:=CalcPosPoint(OldEnd);
    end;

Begin
  With ParentChart do
  begin
    with Rect do
    if Horizontal then ReCalcAdjustedMinMax(Left,Right)
                  else ReCalcAdjustedMinMax(Top,Bottom);
    InternalCalcPositions;
    IMinimum:=tmpMin;
    IMaximum:=tmpMax;
  end;
  if IMinimum>IMaximum then SwapDouble(IMinimum,IMaximum);
  IRange:=IMaximum-IMinimum;
end;

Procedure TChartAxis.CalcMinMax(Var AMin,AMax:Double);
Begin
  if FAutomatic or FAutomaticMaximum then
     AMax:=ParentChart.InternalMinMax(Self,False,Horizontal);
  if FAutomatic or FAutomaticMinimum then
     AMin:=ParentChart.InternalMinMax(Self,True,Horizontal);
end;

Procedure TChartAxis.AdjustMaxMin;
Begin
  CalcMinMax(FMinimumValue,FMaximumValue);
  IMaximum:=FMaximumValue;
  IMinimum:=FMinimumValue;
  IRange:=IMaximum-IMinimum;
end;

procedure TChartAxis.Assign(Source: TPersistent);
Begin
  if Source is TChartAxis then
  With TChartAxis(Source) do
  Begin
    Self.FAxis.Assign(FAxis);
    Self.FLabels              :=FLabels;
    Self.FLabelsAlign         :=FLabelsAlign;
    Self.FLabelsAngle         :=FLabelsAngle;
    Self.LabelsFont           :=FLabelsFont;
    Self.FLabelsExponent      :=FLabelsExponent;
    Self.FLabelsMultiLine     :=FLabelsMultiLine;
    Self.FLabelsSeparation    :=FLabelsSeparation;
    Self.FLabelsSize          :=FLabelsSize;
    Self.FLabelStyle          :=FLabelStyle;
    Self.FLabelsOnAxis        :=FLabelsOnAxis;
    Self.Ticks                :=FTicks;
    Self.TicksInner           :=FTicksInner;
    Self.FTitleSize           :=FTitleSize;
    Self.Grid                 :=FGrid;
    Self.MinorTicks           :=FMinorTicks;
    Self.MinorGrid            :=FMinorGrid;
    Self.FTickLength          :=FTickLength;
    Self.FMinorTickLength     :=FMinorTickLength;
    Self.FMinorTickCount      :=FMinorTickCount;
    Self.FTickInnerLength     :=FTickInnerLength;
    Self.FAxisValuesFormat    :=FAxisValuesFormat;
    Self.FDesiredIncrement    :=FDesiredIncrement;
    Self.FMaximumValue        :=FMaximumValue;
    Self.FMinimumValue        :=FMinimumValue;
    Self.FAutomatic           :=FAutomatic;
    Self.FAutomaticMaximum    :=FAutomaticMaximum;
    Self.FAutomaticMinimum    :=FAutomaticMinimum;
    Self.Title                :=FAxisTitle;
    Self.FDateTimeFormat      :=FDateTimeFormat;
    Self.FGridCentered        :=FGridCentered;
    Self.FLogarithmic         :=FLogarithmic;
    Self.FLogarithmicBase     :=FLogarithmicBase;
    Self.FInverted            :=FInverted;
    Self.FExactDateTime       :=FExactDateTime;
    Self.FRoundFirstLabel     :=FRoundFirstLabel;
    Self.FTickOnLabelsOnly    :=FTickOnLabelsOnly;
    Self.IsDepthAxis          :=IsDepthAxis;
    Self.FStartPosition       :=FStartPosition;
    Self.FEndPosition         :=FEndPosition;
    Self.FPositionPercent     :=FPositionPercent;
    Self.FVisible             :=FVisible;

    Self.FHorizontal          :=FHorizontal;
    Self.FOtherSide           :=FOtherSide;
  end
  else inherited;
end;

Function TChartAxis.LabelValue(Const Value:Double):String;
var tmp : String;
Begin
  if IAxisDateTime then
  begin
    if Value>=0 then
    Begin
      if FDateTimeFormat='' then tmp:=DateTimeDefaultFormat(IRange)
                            else tmp:=FDateTimeFormat;
      DateTimeToString(result,tmp,Value);
    end
    else result:='';
  end
  else result:=FormatFloat(FAxisValuesFormat,Value);
  if Assigned(ParentChart.FOnGetAxisLabel) then
     ParentChart.FOnGetAxisLabel(TChartAxis(Self),nil,-1,Result);
  if FLabelsMultiLine then TeeSplitInLines(Result,' ');
end;

Function TChartAxis.InternalCalcLabelStyle:TAxisLabelStyle;
var t : Integer;
begin
  result:=talNone;
  for t:=0 to ParentChart.SeriesCount-1 do
  With ParentChart.Series[t] do
  if Active and AssociatedToAxis(Self) then
  begin
    result:=talValue;
    if (Horizontal and YMandatory) or
       ((not Horizontal) and (not YMandatory)) then
       if (FXLabels.Count>0) and (FXLabels.First<>nil) then
       begin
         result:=talText;
         break;
       end;
  end;
end;

Function TChartAxis.CalcLabelStyle:TAxisLabelStyle;
Begin
 if FLabelStyle=talAuto then result:=InternalCalcLabelStyle
                        else result:=FLabelStyle;
End;

Function TChartAxis.MaxLabelsWidth:Integer;

  Function MaxLabelsValueWidth:Integer;
  var tmp    : Double;
      tmpA   : Double;
      tmpB   : Double;
      OldGetAxisLabel : TAxisOnGetLabel;
      tmpNum : Integer;
  begin
    if (IsDateTime and FExactDateTime) or RoundFirstLabel then
    begin
      tmp:=CalcIncrement;
      tmpA:=tmp*Int(IMinimum/tmp);
      tmpB:=tmp*Int(IMaximum/tmp);
    end
    else
    begin
      tmpA:=IMinimum;
      tmpB:=IMaximum;
    end;
    With ParentChart do
    begin
      OldGetAxisLabel:=FOnGetAxisLabel;
      FOnGetAxisLabel:=nil;
      With Canvas do
           result:=TextWidth(' ')+ MaxLong( MultiLineTextWidth(LabelValue(tmpA),tmpNum),
                                            MultiLineTextWidth(LabelValue(tmpB),tmpNum));
      FOnGetAxisLabel:=OldGetAxisLabel;
    end;
  end;

Begin
  Case CalcLabelStyle of
    talValue : result:=MaxLabelsValueWidth;
    talMark  : result:=ParentChart.MaxMarkWidth;
    talText  : result:=ParentChart.MaxTextWidth;
  else
  {talNone : } result:=0;
  end;
end;

Procedure TChartAxis.SetLabels(Value:Boolean);
Begin
  ParentChart.SetBooleanProperty(FLabels,Value);
end;

Procedure TChartAxis.SetLabelsFont(Value:TTeeFont);
begin
  FLabelsFont.Assign(Value);
end;

Procedure TChartAxis.SetAutomatic(Value:Boolean);
Begin
  ParentChart.SetBooleanProperty(FAutomatic,Value);
  if not (csLoading in ParentChart.ComponentState) then
  begin
    FAutomaticMinimum:=Value;
    FAutomaticMaximum:=Value;
  end;
end;

Procedure TChartAxis.SetAutoMinMax(Var Variable:Boolean; Var2,Value:Boolean);
Begin
  ParentChart.SetBooleanProperty(Variable,Value);
  if Value then
  begin { if both are automatic, then Automatic should be True too }
    if Var2 then FAutomatic:=True;
  end
  else FAutomatic:=False;
end;

Procedure TChartAxis.SetAutomaticMinimum(Value:Boolean);
Begin
  SetAutoMinMax(FAutomaticMinimum,FAutomaticMaximum,Value);
end;

Procedure TChartAxis.SetAutomaticMaximum(Value:Boolean);
Begin
  SetAutoMinMax(FAutomaticMaximum,FAutomaticMinimum,Value);
end;

Function TChartAxis.IsAxisValuesFormatStored:Boolean;
begin
  result:=FAxisValuesFormat<>TeeMsg_DefValueFormat;
end;

Procedure TChartAxis.SetValuesFormat(Const Value:String);
Begin
  ParentChart.SetStringProperty(FAxisValuesFormat,Value);
end;

Procedure TChartAxis.SetInverted(Value:Boolean);
Begin
  ParentChart.SetBooleanProperty(FInverted,Value);
end;

Procedure TChartAxis.InternalSetInverted(Value:Boolean);
Begin
  FInverted:=Value;
end;

Procedure TChartAxis.SetLogarithmicBase(Value:Integer);
begin
  if FLogarithmicBase<2 then raise AxisException.Create(TeeMsg_AxisLogBase);
  ParentChart.SetIntegerProperty(FLogarithmicBase,Value);
end;

Procedure TChartAxis.SetLogarithmic(Value:Boolean);
Begin
  if Value and IsDateTime then
     Raise AxisException.Create(TeeMsg_AxisLogDateTime);
  if Value then
  begin
    AdjustMaxMin;
    if ((IMinimum<0) or (IMaximum<0)) then
       Raise AxisException.Create(TeeMsg_AxisLogNotPositive);
  end;
  ParentChart.SetBooleanProperty(FLogarithmic,Value);
End;

Procedure TChartAxis.SetLabelsAlign(Value:TAxisLabelAlign);
Begin
  if FLabelsAlign<>Value then
  begin
    FLabelsAlign:=Value;
    ParentChart.Invalidate;
  end;
end;

Procedure TChartAxis.SetLabelsAngle(Value:Integer);
Begin
  TeeCheckAngle(Value,TeeMsg_AxisLabels);
  ParentChart.SetIntegerProperty(FLabelsAngle,Value);
end;

Procedure TChartAxis.SetLabelsSeparation(Value:Integer);
Begin
  if Value<0 then Raise AxisException.Create(TeeMsg_AxisLabelSep);
  ParentChart.SetIntegerProperty(FLabelsSeparation,Value);
end;

Procedure TChartAxis.SetLabelsSize(Value:Integer);
Begin
  ParentChart.SetIntegerProperty(FLabelsSize,Value);
end;

Procedure TChartAxis.SetTitleSize(Value:Integer);
Begin
  ParentChart.SetIntegerProperty(FTitleSize,Value);
end;

Procedure TChartAxis.SetLabelsOnAxis(Value:Boolean);
Begin
  ParentChart.SetBooleanProperty(FLabelsOnAxis,Value);
end;

Procedure TChartAxis.SetExactDateTime(Value:Boolean);
begin
  ParentChart.SetBooleanProperty(FExactDateTime,Value);
end;

Procedure TChartAxis.SetLabelStyle(Value:TAxisLabelStyle);
begin
  if FLabelStyle<>Value then
  begin
    FLabelStyle:=Value;
    ParentChart.Invalidate;
  end;
end;

Procedure TChartAxis.SetVisible(Value:Boolean);
Begin
  ParentChart.SetBooleanProperty(FVisible,Value);
end;

Procedure TChartAxis.SetDesiredIncrement(Const Value:Double);
Begin
  if Value<0 then Raise AxisException.Create(TeeMsg_AxisIncrementNeg);
  if IsDateTime then DateTimeToStr(Value);
  ParentChart.SetDoubleProperty(FDesiredIncrement,Value);
end;

Procedure TChartAxis.SetMinimum(Const Value:Double);
Begin
  if (not (csReading in ParentChart.ComponentState)) and
     (Value>FMaximumValue) then
       Raise AxisException.Create(TeeMsg_AxisMinMax);
  InternalSetMinimum(Value);
end;

Procedure TChartAxis.InternalSetMinimum(Const Value:Double);
Begin
  ParentChart.SetDoubleProperty(FMinimumValue,Value);
end;

Procedure TChartAxis.SetMaximum(Const Value:Double);
Begin
  if (not (csReading in ParentChart.ComponentState)) and
     (Value<FMinimumValue) then
       Raise AxisException.Create(TeeMsg_AxisMaxMin);
  InternalSetMaximum(Value);
end;

Procedure TChartAxis.SetMinMax(AMin,AMax:Double);
Begin
  FAutomatic:=False;
  FAutomaticMinimum:=False;
  FAutomaticMaximum:=False;
  if AMin>AMax then SwapDouble(AMin,AMax);
  InternalSetMinimum(AMin);
  InternalSetMaximum(AMax);
  if (FMaximumValue-FMinimumValue)<MinAxisRange then
     InternalSetMaximum(FMinimumValue+MinAxisRange);
end;

Procedure TChartAxis.InternalSetMaximum(Const Value:Double);
Begin
  ParentChart.SetDoubleProperty(FMaximumValue,Value);
end;

Procedure TChartAxis.SetTickLength(Value:Integer);
Begin
  ParentChart.SetIntegerProperty(FTickLength,Value);
end;

Procedure TChartAxis.SetMinorTickLength(Value:Integer);
Begin
  ParentChart.SetIntegerProperty(FMinorTickLength,Value);
end;

Procedure TChartAxis.SetMinorTickCount(Value:Integer);
Begin
  ParentChart.SetIntegerProperty(FMinorTickCount,Value);
End;

Procedure TChartAxis.SetTickInnerLength(Value:Integer);
Begin
  ParentChart.SetIntegerProperty(FTickInnerLength,Value);
end;

Procedure TChartAxis.DrawTitle(x,y:Integer);
begin
  With ParentChart,Canvas do
  begin
    AssignFont(FAxisTitle.Font);
    BackMode:=cbmTransparent;
    if IsDepthAxis then
    begin
      TextAlign:=TA_LEFT;
      TextOut3D(x,y,Width3D div 2,FAxisTitle.FCaption);
    end
    else DrawAxisLabel(x,y,FAxisTitle.FAngle,FAxisTitle.FCaption);
  end;
end;

procedure TChartAxis.DrawAxisLabel(x,y,Angle:Integer; Const St:String);
Const Aligns:Array[Boolean,Boolean] of Integer=(
              (TA_RIGHT +TA_TOP, TA_LEFT  +TA_TOP    ),    { vertical }
              (TA_CENTER+TA_TOP, TA_CENTER+TA_BOTTOM ) );  { horizontal }


Var tmpSt2 : String;
    tmpZ   : Integer;

  Procedure DrawExponentLabel;
  var tmpW : Integer;
      tmpH : Integer;
      i    : Integer;
      tmp  : String;
      tmpSub : String;
      Old  : Integer;
  begin
    i:=Pos('E',Uppercase(tmpSt2));
    With ParentChart.Canvas do
    if i=0 then TextOut3D(x,y,tmpZ,tmpSt2)
    else
    begin
      tmp:=Copy(tmpSt2,1,i-1);
      tmpSub:=Copy(tmpSt2,i+1,Length(tmpSt2)-1);
      tmpH:=FontHeight-1;
      Old:=Font.Size;
      if TextAlign=TA_LEFT then
      begin
        TextOut3D(x,y,tmpZ,tmp);
        tmpW:=TextWidth(tmp)+1;
        Font.Size:=Font.Size-(Font.Size div 4);
        TextOut3D(x+tmpW,y-(tmpH div 2)+2,tmpZ,tmpSub);
      end
      else
      begin
        Font.Size:=Font.Size-(Font.Size div 4);
        TextOut3D(x,y-(tmpH div 2)+2,tmpZ,tmpSub);
        tmpW:=TextWidth(tmpSub)+1;
        Font.Size:=Old;
        TextOut3D(x-tmpW,y,tmpZ,tmp);
      end;
      Font.Size:=Old;
    end;
  end;

  { Returns 1 + how many times "TeeLineSeparator #13" is found
    inside string St parameter }
  Function TeeNumTextLines(St:String):Integer;
  var i : Integer;
  begin
    result:=0;
    i:=AnsiPos(TeeLineSeparator,St);
    while i>0 do
    begin
      Inc(result);
      Delete(St,1,i);
      i:=AnsiPos(TeeLineSeparator,St);
    end;
    if St<>'' then Inc(result);
  end;

var Delta    : Integer;
    t        : Integer;
    n        : Integer;
    i        : Integer;
    tmpSt    : String;
    tmpH     : Integer;
    tmpD     : Integer;
    tmpAlign : TCanvasTextAlign;
begin
  tmpH:=ParentChart.Canvas.FontHeight div 2;
  Case Angle of
 0,360: Begin
         if Horizontal or (FLabelsAlign=alDefault) then
            tmpAlign:=Aligns[Horizontal,OtherSide]
         else
           if OtherSide then
           begin
             tmpAlign:=TA_RIGHT;
             Inc(X,SizeLabels);
           end
           else
           begin
             tmpAlign:=TA_LEFT;
             Dec(X,SizeLabels);
           end;
         if not Horizontal then Dec(Y,tmpH);
       end;
   90: Begin
         if Horizontal then
         begin
           tmpAlign:=Aligns[False,OtherSide];
           Dec(X,tmpH);
         end
         else tmpAlign:=Aligns[True,not OtherSide];
       end;
  180: Begin
         tmpAlign:=Aligns[Horizontal,not OtherSide];
         if not Horizontal then Inc(Y,tmpH);
       end;
  270: Begin
         if Horizontal then
         begin
           tmpAlign:=Aligns[False,not OtherSide];
           Inc(X,tmpH);
         end
         else tmpAlign:=Aligns[True,OtherSide];
       end;
    else tmpAlign:=TA_LEFT; { non-supported angles }
  end;

  if OtherSide then tmpZ:=ParentChart.Width3D else tmpZ:=0;

  With ParentChart.Canvas do
  begin
    TextAlign:=tmpAlign;
    n:=TeeNumTextLines(St);
    Delta:=FontHeight;
    if (Angle=180) or (Angle=270) then Delta:=-Delta;

    tmpD:=Round(Delta*n);
    if Horizontal then
    begin
      if Angle=0 then
         if OtherSide then y:=y-tmpD else y:=y-Delta
      else
      if Angle=180 then
         if OtherSide then y:=y-Delta else y:=y-tmpD
      else
      if (Angle=90) or (Angle=270) then
         x:=x-Round(0.5*Delta*(n+1));
    end
    else
      if (Angle=0) or (Angle=180) then
         y:=y-Round(0.5*Delta*(n+1))
      else
      if OtherSide then
      begin
         if Angle=90 then x:=x-Delta
                     else if Angle=270 then x:=x-tmpD
      end
      else
      if Angle=90 then x:=x-tmpD
                  else if Angle=270 then x:=x-Delta;

    tmpSt:=St;
    for t:=1 to n do
    begin
      i:=AnsiPos(TeeLineSeparator,tmpSt);
      if i>0 then tmpSt2:=Copy(tmpSt,1,i-1) else tmpSt2:=tmpSt;
      if Angle=0 then
      begin
        y:=y+Delta;
        if FLabelsExponent then DrawExponentLabel
                           else TextOut3D(X,Y,tmpZ,tmpSt2);
      end
      else
      begin
        if Angle=180 then y:=y+Delta
        else
        if (Angle=90) or (Angle=270) then x:=x+Delta;
        RotateLabel3D(X,Y,tmpZ,tmpSt2,Angle);
      end;
      Delete(tmpSt,1,i);
    end;
    TextAlign:=TA_LEFT;
  end;
end;

Procedure TChartAxis.Scroll(Const Offset:Double; CheckLimits:Boolean);
Begin
  if (not CheckLimits) or
     ( ((Offset>0) and (FMaximumValue<ParentChart.InternalMinMax(Self,False,Horizontal))) or
       ((Offset<0) and (FMinimumValue>ParentChart.InternalMinMax(Self,True,Horizontal)))
     ) then
  begin
    FAutomatic:=False;
    FAutomaticMaximum:=False;
    FMaximumValue:=FMaximumValue+Offset;
    FAutomaticMinimum:=False;
    FMinimumValue:=FMinimumValue+Offset;
    ParentChart.Invalidate;
  end;
end;

Function TChartAxis.InternalCalcLog(Var LogMax,LogMin:Double):Double;
Begin
  if IMinimum<=0 then LogMin:=0 else LogMin:=ln(IMinimum);
  if IMaximum<=0 then LogMax:=0 else LogMax:=ln(IMaximum);
  result:=LogMax-LogMin;
end;

Function TChartAxis.InternalCalcDepthPosValue(Const Value:Double):Integer;
var tmp : Double;
begin
  With ParentChart do
  if IRange=0 then result:=Width3D div 2
  else
  begin
    if FInverted then tmp:=IMaximum-Value
                 else tmp:=Value-IMinimum;
    result:=Round(1.0*Width3D*tmp/IRange);
  end;
end;

Function TChartAxis.InternalCalcLogPosValue(IsX:Boolean; Const Value:Double):Integer;
var tmp      : Double;
    tmpValue : Double;
    LogMax   : Double;
    LogMin   : Double;
    tmpPos   : Integer;
begin
  tmp:=InternalCalcLog(LogMax,LogMin);
  if tmp=0 then result:=ICenterPos
  else
  begin
    if Value<=0 then
       if (IsX and FInverted) or
          ((not IsX) and (not FInverted)) then result:=IEndPos
                                          else result:=IStartPos
    else
    begin
      if FInverted then tmpValue:=LogMax-ln(Value)
                   else tmpValue:=ln(Value)-LogMin;
      tmpPos:=Round(tmpValue*IAxisSize/tmp);
      if IsX then result:=IStartPos+tmpPos
             else result:=IEndPos-tmpPos;
    end;
  end;
end;

Function TChartAxis.CalcPosValue(Const Value:Double):Integer;
begin
  if Horizontal then result:=CalcXPosValue(Value)
                else result:=CalcYPosValue(Value);
end;

Function TChartAxis.CalcXPosValue(Const Value:Double):Integer;
begin
  if IsDepthAxis then result:=InternalCalcDepthPosValue(Value) else
  if FLogarithmic then result:=InternalCalcLogPosValue(True,Value)
                  else result:=InternalCalcPosValue(Value,FInverted);
end;

Function TChartAxis.CalcYPosValue(Const Value:Double):Integer;
begin
  if IsDepthAxis then result:=InternalCalcDepthPosValue(Value) else
  if FLogarithmic then result:=InternalCalcLogPosValue(False,Value)
                  else result:=InternalCalcPosValue(Value,not FInverted);
end;

Function TChartAxis.InternalCalcPosValue( Const Value:Double;
                                                FromEnd:Boolean):Integer;
var tmp : Double;
begin
  if IRange=0 then result:=ICenterPos
  else
  begin
    tmp:=(Value-IMinimum)*IAxisSize/IRange;
    if Abs(tmp)>=MaxLongint then
       result:=ICenterPos
    else
       if FromEnd then result:=IEndPos-Round(tmp)
                  else result:=IStartPos+Round(tmp)
  end;
end;

Function TChartAxis.CalcSizeValue(Const Value:Double):Integer;
var tmp    : Double;
    LogMax : Double;
    LogMin : Double;
begin
  result:=0;
  if Value>0 then
    if FLogarithmic then
    Begin
      tmp:=InternalCalcLog(LogMax,LogMin);
      if tmp<>0 then result:=Round(ln(Value)*IAxisSize/tmp);
    end
    else
    if IRange<>0 then result:=Round(Value*IAxisSize/IRange);
end;

Function TChartAxis.Clicked(x,y:Integer):Boolean;
Var tmpPos1 : Integer;
    Pos1    : Integer;
    tmpPos2 : Integer;
    Pos2    : Integer;
    tmpR    : TRect;
Begin
  if ParentChart.IsAxisVisible(Self) then
  begin
    if IStartPos>IEndPos then
    begin
      tmpPos1:=IEndPos;
      tmpPos2:=IStartPos;
    end
    else
    begin
      tmpPos1:=IStartPos;
      tmpPos2:=IEndPos;
    end;

    if PosAxis>FPosLabels then
    begin
      Pos1:=FPosLabels;
      Pos2:=PosAxis+TeeAxisClickGap;
    end
    else
    begin
      Pos1:=PosAxis-TeeAxisClickGap;
      Pos2:=FPosLabels;
    end;

    if Horizontal then tmpR:={$IFDEF D6}Types.{$ENDIF}Rect(tmpPos1,Pos1,tmpPos2,Pos2)
                  else tmpR:={$IFDEF D6}Types.{$ENDIF}Rect(Pos1,tmpPos1,Pos2,tmpPos2);
    result:={$IFDEF D6}Types.{$ENDIF}PtInRect(tmpR,{$IFDEF D6}Types.{$ENDIF}Point(x,y));
  end
  else result:=False;
end;

Procedure TChartAxis.CustomDrawMinMaxStartEnd( APosLabels,
                                               APosTitle,
                                               APosAxis:Integer;
                                               GridVisible:Boolean;
                                               Const AMinimum,AMaximum,
                                                     AIncrement:Double;
                                               AStartPos,AEndPos:Integer);

  Procedure SetInternals;
  begin
    IMaximum :=FMaximumValue;
    IMinimum :=FMinimumValue;
    IRange   :=IMaximum-IMinimum;
  end;

var OldMin       : Double;
    OldMax       : Double;
    OldIncrement : Double;
    OldAutomatic : Boolean;
begin
  OldMin      :=FMinimumValue;
  OldMax      :=FMaximumValue;
  OldIncrement:=FDesiredIncrement;
  OldAutomatic:=FAutomatic;
  try
    FAutomatic       :=False;
    FMinimumValue    :=AMinimum;
    FMaximumValue    :=AMaximum;
    FDesiredIncrement:=AIncrement;
    SetInternals;
    CustomDrawStartEnd(APosLabels,APosTitle,APosAxis,GridVisible,AStartPos,AEndPos);
  finally
    FMinimumValue    :=OldMin;
    FMaximumValue    :=OldMax;
    FDesiredIncrement:=OldIncrement;
    FAutomatic       :=OldAutomatic;
    SetInternals;
  end;
end;

Procedure TChartAxis.CustomDrawMinMax( APosLabels,
                                       APosTitle,
                                       APosAxis:Integer;
                                       GridVisible:Boolean;
                                       Const AMinimum,AMaximum,
                                       AIncrement:Double);
begin
  CustomDrawMinMaxStartEnd(APosLabels,APosTitle,APosAxis,GridVisible,
        AMinimum,AMaximum,AIncrement,IStartPos,IEndPos);
end;

Procedure TChartAxis.CustomDraw( APosLabels,APosTitle,APosAxis:Integer;
                                       GridVisible:Boolean);
begin
  InternalCalcPositions;
  CustomDrawStartEnd(APosLabels,APosTitle,APosAxis,GridVisible,IStartPos,IEndPos);
End;

Procedure TChartAxis.CustomDrawStartEnd( APosLabels,APosTitle,APosAxis:Integer;
                                               GridVisible:Boolean; AStartPos,AEndPos:Integer);
var OldGridVisible : Boolean;
    OldChange      : TNotifyEvent;
Begin
  FPosLabels:=APosLabels;
  FPosTitle :=APosTitle;
  FPosAxis  :=APosAxis;
  IStartPos :=AStartPos;
  IEndPos   :=AEndPos;
  RecalcSizeCenter;
  OldGridVisible:=FGrid.Visible;
  OldChange:=FGrid.OnChange;
  FGrid.OnChange:=nil;
  FGrid.Visible:=GridVisible;
  Draw(False);
  FGrid.Visible:=OldGridVisible;
  FGrid.OnChange:=OldChange;
end;

Procedure TChartAxis.RecalcSizeCenter;
begin
  IAxisSize:=IEndPos-IStartPos;
  ICenterPos:=(IStartPos+IEndPos) div 2;
end;

Procedure TChartAxis.InternalCalcPositions;

  Procedure DoCalculation(AStartPos:Integer; ASize:Integer);
  begin
    IStartPos:=AStartPos+Round(0.01*ASize*FStartPosition);
    IEndPos  :=AStartPos+Round(0.01*ASize*FEndPosition);
  end;

begin
  With ParentChart do
  if IsDepthAxis then DoCalculation(0,Width3D) else
  if Horizontal then DoCalculation(ChartRect.Left,ChartWidth)
                else DoCalculation(ChartRect.Top,ChartHeight);
  RecalcSizeCenter;
end;

Function TChartAxis.ApplyPosition(APos:Integer; Const R:TRect):Integer;
Var tmpSize : Integer;
begin
  result:=APos;
  if FPositionPercent<>0 then
  With R do
  begin
    if Horizontal then tmpSize:=Bottom-Top else tmpSize:=Right-Left;
    tmpSize:=Round(0.01*FPositionPercent*tmpSize);
    if OtherSide then tmpSize:=-tmpSize;
    if Horizontal then tmpSize:=-tmpSize;
    result:=APos+tmpSize;
  end;
end;

Function TChartAxis.GetRectangleEdge(Const R:TRect):Integer;
begin
  With R do
  if OtherSide then
     if Horizontal then result:=Top else result:=Right
  else
     if Horizontal then result:=Bottom else result:=Left;
end;

type TTeeMinorTickProc=Procedure(AMinorTickPos:Integer);

Procedure TChartAxis.Draw(CalcPosAxis:Boolean);
type TTicks=Array{$IFNDEF D4}[0..2000]{$ENDIF} of Integer;

Var tmpValue    : Double;
    tmpNumTicks : Integer;
    tmpTicks    : TTicks;

  Procedure DrawTicksGrid;

    Procedure DrawGridLine(tmp:Integer);
    Begin
      With ParentChart,Canvas,ChartRect do
      begin
        if IsDepthAxis then
        begin
          VertLine3D(Left,Top,Bottom,tmp);
          HorizLine3D(Left,Right,Bottom,tmp);
        end
        else
        if Horizontal then
        Begin
          if View3D then
          Begin
            if OtherSide then VertLine3D(tmp,Top,Bottom,Width3D)
            else
            begin
              if FAxisBehind then
              begin
                ZLine3D(tmp,PosAxis,0,Width3D);
                VertLine3D(tmp,Top,Bottom,Width3D);
              end
              else VertLine3D(tmp,Top,Bottom,0);
            end;
          end
          else DoVertLine(tmp,Top,Bottom);
        end
        else
        Begin
          if View3D then
          Begin
            if OtherSide then HorizLine3D(Left,Right,tmp,Width3D)
            else
            begin
              if FAxisBehind then
              begin
                ZLine3D(PosAxis,tmp,0,Width3D);
                HorizLine3D(Left,Right,tmp,Width3D);
              end
              else HorizLine3D(Left,Right,tmp,0);
            end;
          end
          else DoHorizLine(Left,Right,tmp);
        end;
      end;
    end;

    Procedure DrawGrids;
    var t : Integer;
    begin
      With ParentChart,Canvas do
      begin
        if FGrid.Color=clTeeColor then
           AssignVisiblePenColor(FGrid,clGray)
        else
           AssignVisiblePen(FGrid);
        CheckPenWidth(Pen);
      end;
      for t:=0 to tmpNumTicks-1 do
        if FGridCentered then
        begin
          if t>0 then DrawGridLine(Round(0.5*(tmpTicks[t]+tmpTicks[t-1])))
        end
        else DrawGridLine(tmpTicks[t]);
    end;

    Var tmpWallSize : Integer;

    Procedure InternalDrawTick(tmp,Delta,tmpTickLength:Integer);
    Begin
      with ParentChart,Canvas do
      Begin
        if IsDepthAxis then
           HorizLine3D(PosAxis+Delta,PosAxis+Delta+tmpTickLength,ChartRect.Bottom,tmp)
        else
        if OtherSide then
        Begin
          if Horizontal then
             VertLine3D(tmp,PosAxis-Delta,PosAxis-Delta-tmpTickLength,Width3D)
          else
             HorizLine3D(PosAxis+Delta,PosAxis+Delta+tmpTickLength,tmp,Width3D)
        end
        else
        begin
          Inc(Delta,tmpWallSize);
          if Horizontal then VertLine3D(tmp,PosAxis+Delta,PosAxis+Delta+tmpTickLength,0)
                        else HorizLine3D(PosAxis-Delta,PosAxis-Delta-tmpTickLength,tmp,0);
        end;
      end;
    end;

    Procedure DrawAxisLine;
    var tmp : Integer;
    begin
      With ParentChart,Canvas do
      if IsDepthAxis then
      With ChartRect do
      begin
        tmp:=Bottom+CalcWallSize(BottomAxis);
        MoveTo3D(Right,tmp,0);
        LineTo3D(Right,tmp,Width3D);
      end
      else
      if Horizontal then
         if OtherSide then
            HorizLine3D(IStartPos,IEndPos,PosAxis,Width3D)
         else
            HorizLine3D(IStartPos-CalcWallSize(LeftAxis),
                        IEndPos,PosAxis+tmpWallSize,0)
      else
         if OtherSide then
            VertLine3D(PosAxis,IStartPos,IEndPos,Width3D)
         else
            VertLine3D(PosAxis-tmpWallSize,
                       IStartPos,IEndPos+CalcWallSize(BottomAxis),0);
    end;

    Procedure ProcessMinorTicks(IsGrid:Boolean);

      Procedure AProc(APos:Integer);
      begin
        if (APos>IStartPos) and (APos<IEndPos) then
           if IsGrid then DrawGridLine(APos)
                     else InternalDrawTick(APos,1,FMinorTickLength);
      end;

    var t        : Integer;
        tt       : Integer;
        tmpDelta : Double;
        tmpValue : Double;
    begin
      if tmpNumTicks>1 then
      if not FLogarithmic then
      begin
        tmpDelta:=1.0*(tmpTicks[1]-tmpTicks[0])/(FMinorTickCount+1);
        for t:=1 to FMinorTickCount do
        begin
          AProc(tmpTicks[0]-Round(t*tmpDelta));
          AProc(tmpTicks[tmpNumTicks-1]+Round(t*tmpDelta));
        end;
      end;
      for t:=1 to tmpNumTicks-1 do
        if FLogarithmic then
        begin
          tmpValue:=CalcPosPoint(tmpTicks[t-1]);
          tmpDelta:=((tmpValue*FLogarithmicBase)-tmpValue)/(FMinorTickCount+1);
          for tt:=1 to FMinorTickCount do
          begin
            tmpValue:=tmpValue+tmpDelta;
            if tmpValue<=IMaximum then AProc(CalcPosValue(tmpValue))
                                  else Break;
          end;
        end
        else
        begin
          tmpDelta:=1.0*(tmpTicks[t]-tmpTicks[t-1])/(FMinorTickCount+1);
          for tt:=1 to FMinorTickCount do AProc(tmpTicks[t]-Round(tt*tmpDelta));
        end;
    end;

    Procedure ProcessTicks(APen:TChartPen; AOffset,ALength:Integer);
    var t : Integer;
    begin
      if APen.Visible then
      begin
        ParentChart.Canvas.AssignVisiblePen(APen);
        for t:=0 to tmpNumTicks-1 do
            InternalDrawTick(tmpTicks[t],AOffset,ALength);
      end;
    end;

    Procedure ProcessMinor(APen:TChartPen; IsGrid:Boolean);
    begin
      if (tmpNumTicks>0) and APen.Visible then
      begin
        ParentChart.Canvas.AssignVisiblePen(APen);
        ProcessMinorTicks(IsGrid);
      end;
    end;

  Begin
    With ParentChart.Canvas do
    begin
      Brush.Style:=bsClear;
      BackMode:=cbmTransparent;
    end;
    tmpWallSize:=ParentChart.CalcWallSize(Self);
    if FAxis.Visible then
    begin
      ParentChart.Canvas.AssignVisiblePen(FAxis);
      DrawAxisLine;
    end;
    ProcessTicks(FTicks,1,FTickLength);
    if FGrid.Visible then DrawGrids;
    ProcessTicks(FTicksInner,-1,-FTickInnerLength);
    ProcessMinor(FMinorTicks,False);
    ProcessMinor(FMinorGrid,True);
    ParentChart.Canvas.BackMode:=cbmOpaque;
  end;

  Procedure AddTick(APos:Integer);
  begin
    tmpTicks[tmpNumTicks]:=APos;
    Inc(tmpNumTicks);
  end;

  Procedure DrawThisLabel(LabelPos:Integer; Const tmpSt:String);
  var tmpZ : Integer;
  begin
    if TickOnLabelsOnly then AddTick(LabelPos);
    With ParentChart,Canvas do
    Begin
      AssignFont(LabelsFont);
      Brush.Style:=bsSolid;
      Brush.Style:=bsClear;
      if IsDepthAxis then
      begin
        TextAlign:=ta_Left;
        if (View3DOptions.Rotation=360) or View3DOptions.Orthogonal then
           tmpZ:=LabelPos+(FontHeight div 2)
        else
           tmpZ:=LabelPos;
        TextOut3D(PosLabels,ChartRect.Bottom,tmpZ,tmpSt);
      end
      else
      if Horizontal then DrawAxisLabel(LabelPos,PosLabels,FLabelsAngle,tmpSt)
                    else DrawAxisLabel(PosLabels,LabelPos,FLabelsAngle,tmpSt);
    end;
  end;

  Var tmpLabelStyle : TAxisLabelStyle;
      tmpSeriesList : TList;

  Function GetAxisSeriesLabel(AIndex:Integer; Var AValue:Double;
                              Var ALabel:String):Boolean;
  var t : Integer;
  begin
    result:=False;
    for t:=0 to tmpSeriesList.Count-1 do
    With TChartSeries(tmpSeriesList[t]) do
    if FXLabels.Count>AIndex then
    begin
      Case tmpLabelStyle of
        talMark : ALabel:=ValueMarkText[AIndex];
        talText : ALabel:=XLabel[AIndex];
      end;
      if Assigned(ParentChart.FOnGetAxisLabel) then
         ParentChart.FOnGetAxisLabel( TChartAxis(Self),
                         TChartSeries(tmpSeriesList[t]),AIndex,ALabel);
      if Horizontal then AValue:=XValues.Value[AIndex]
                    else AValue:=YValues.Value[AIndex];
      result:=True;
      break;
    end;
  end;

  { Select all active Series that have "Labels" }
  Procedure CalcAllSeries;
  var t : Integer;
  begin
    tmpSeriesList.Clear;
    With ParentChart do
    for t:=0 to SeriesCount-1 do
    With Series[t] do
      if Active and AssociatedToAxis(Self) then
      begin
        if FXLabels.Count>0 then tmpSeriesList.Add(Series[t]);
      end;
  end;

  Procedure CalcFirstLastAllSeries(Var tmpFirst,tmpLast:Integer);
  var t : Integer;
  begin
    tmpFirst:=-1;
    tmpLast:=-1;
    for t:=0 to tmpSeriesList.Count-1 do
    With TChartSeries(tmpSeriesList[t]) do
    begin
      CalcFirstLastVisibleIndex;
      if tmpFirst=-1 then  { fix 4.02 }
      begin
        if FFirstVisibleIndex<>-1 then tmpFirst:=FFirstVisibleIndex;
        if tmpFirst>FFirstVisibleIndex then
           tmpFirst:=FFirstVisibleIndex;
      end;
      if (tmpLast=-1) or (tmpLast<FLastVisibleIndex) then
         tmpLast:=FLastVisibleIndex;
    end;
  end;

  Procedure AxisLabelsSeries;
  Var t            : Integer;
      tmp          : Integer;
      tmpNum       : Integer;
      tmpFirst     : Integer;
      tmpLast      : Integer;
      tmpSt        : String;
      tmpValue     : Double;
      OldPosLabel  : Integer;
      OldSizeLabel : Integer;
      tmpLabelSize : Integer;
      tmpDraw      : Boolean;
      tmpLabelW    : Boolean;
  Begin
    tmpSeriesList:=TList.Create;
    CalcAllSeries;
    CalcFirstLastAllSeries(tmpFirst,tmpLast);

    if tmpFirst<>-1 then
    begin
      OldPosLabel :=-1;
      OldSizeLabel:= 0;
      tmpLabelW:=Horizontal;
      Case FLabelsAngle of
         90,270: tmpLabelW:=not tmpLabelW;
      end;
      for t:=tmpFirst to tmpLast do
      if GetAxisSeriesLabel(t,tmpValue,tmpSt) then
         if (tmpValue>=IMinimum) and (tmpValue<=IMaximum) then
         begin
          tmp:=Self.CalcPosValue(tmpValue);
          if not TickOnLabelsOnly then AddTick(tmp);
          if FLabels and (tmpSt<>'') then
          begin
            With ParentChart.Canvas do
            begin
              tmpLabelSize:=ParentChart.MultiLineTextWidth(tmpSt,tmpNum);
              if not tmpLabelW then tmpLabelSize:=FontHeight*tmpNum;
            end;

            if (FLabelsSeparation<>0) and (OldPosLabel<>-1) then
            begin
              Inc(tmpLabelSize,Trunc(0.02*tmpLabelSize*FLabelsSeparation));
              tmpLabelSize:=tmpLabelSize div 2;

              if tmp>=OldPosLabel then
                 tmpDraw:=(tmp-tmpLabelSize)>=(OldPosLabel+OldSizeLabel)
              else
                 tmpDraw:=(tmp+tmpLabelSize)<=(OldPosLabel-OldSizeLabel);
              if tmpDraw then
              begin
                DrawThisLabel(tmp,tmpSt);
                OldPosLabel:=tmp;
                OldSizeLabel:=tmpLabelSize;
              end;
            end
            else
            begin
              DrawThisLabel(tmp,tmpSt);
              OldPosLabel:=tmp;
              OldSizeLabel:=tmpLabelSize div 2;
            end;
          end;
         end;
    end;
    tmpSeriesList.Free;
  end;

  var IIncrement       : Double;
      tmpWhichDateTime : TDateTimeStep;

  Procedure InternalDrawLabel(DecValue:Boolean);
  var tmp : Integer;
  Begin
    tmp:=CalcPosValue(tmpValue);
    if FLabelsOnAxis or ((tmp>IStartPos) and (tmp<IEndPos)) then
    begin
      if not TickOnLabelsOnly then AddTick(tmp);
      if FLabels then DrawThisLabel(tmp,LabelValue(tmpValue));
    end;
    if DecValue then IncDecDateTime(False,tmpValue,IIncrement,tmpWhichDateTime);
  end;

  Procedure DoDefaultLogLabels;

    { From Borland's Math.pas unit. Some Delphi and BCB
      versions do not include math.pas unit }
    function IntPower(Const Base: Extended; Exponent: Integer): Extended;
    asm
        mov     ecx, eax
        cdq
        fld1                      { Result := 1 }
        xor     eax, edx
        sub     eax, edx          { eax := Abs(Exponent) }
        jz      @@3
        fld     Base
        jmp     @@2
    @@1:    fmul    ST, ST            { X := Base * Base }
    @@2:    shr     eax,1
        jnc     @@1
        fmul    ST(1),ST          { Result := Result * X }
        jnz     @@1
        fstp    st                { pop X from FPU stack }
        cmp     ecx, 0
        jge     @@3
        fld1
        fdivrp                    { Result := 1 / Result }
    @@3:
        fwait
    end;

    function LogBaseN(Const Base, X: Extended): Extended;
    asm
      FLD1
      FLD     X
      FYL2X
      FLD1
      FLD     Base
      FYL2X
      FDIV
      FWAIT
    end;

  begin
    if IMinimum<>IMaximum then
    begin
      if IMinimum<=0 then
      begin
        if IMinimum=0 then IMinimum:=0.1
                      else IMinimum:=MinAxisRange;
        tmpValue:=IMinimum;
      end
      else tmpValue:=IntPower(FLogarithmicBase,Round(LogBaseN(FLogarithmicBase,IMinimum)));
      While tmpValue<=IMaximum do
      begin
        if tmpValue>=IMinimum then InternalDrawLabel(False);
        tmpValue:=tmpValue*FLogarithmicBase;
      end;
    end;
  end;

  Procedure DoDefaultLabels;
  var tmp : Double;
  Begin
    tmpValue:=IMaximum/IIncrement;
    if Abs(IRange/IIncrement)<10000 then  { if less than 10000 labels... }
    Begin
      { calculate the maximum value... }
      if IAxisDateTime and FExactDateTime and
         (tmpWhichDateTime<>dtNone) and (tmpWhichDateTime>=dtOneDay) then
             tmpValue:=TeeRoundDate(IMaximum,tmpWhichDateTime)
      else
      if (FloatToStr(IMinimum)=FloatToStr(IMaximum)) or (not RoundFirstLabel) then
         tmpValue:=IMaximum
      else
         tmpValue:=IIncrement*Trunc(tmpValue);

      { adjust the maximum value to be inside "IMinimum" and "IMaximum" }
      While tmpValue>IMaximum do
            IncDecDateTime(False,tmpValue,IIncrement,tmpWhichDateTime);

      { Draw the labels... }
      if IRange=0 then
         InternalDrawLabel(False)  { Maximum is equal to Minimum. Draw one label }
      else
      begin
        { do the loop and draw labels... }
        if (Abs(IMaximum-IMinimum)<1e-10) or
           (FloatToStr(tmpValue)=FloatToStr(tmpValue-IIncrement)) then { fix zooming when axis Max=Min }
           InternalDrawLabel(False)
        else
        begin { draw labels until "tmpVale" is less than minimum }
          tmp:=(IMinimum-MinAxisIncrement)/(1.0+MinAxisIncrement);
          While tmpValue>=tmp do InternalDrawLabel(True);
        end;
      end;
    end;
  end;

  Procedure DoNotCustomLabels;
  begin
    if FLogarithmic and (FDesiredIncrement=0) then DoDefaultLogLabels
                                              else DoDefaultLabels;
  end;

  Procedure DoCustomLabels;
  Const DifFloat = 0.0000001;
  var LabelIndex  : Integer;
      Stop        : Boolean;
      LabelInside : Boolean;
  Begin
    tmpValue:=IMinimum;
    Stop:=True;
    LabelIndex:=0;
    LabelInside:=False;
    Repeat
      ParentChart.FOnGetNextAxisLabel(TChartAxis(Self),LabelIndex,tmpValue,Stop);
      if Stop then
      Begin
        if LabelIndex=0 then DoNotCustomLabels;
        Exit;
      end
      else
      begin { Trick with doubles... }
        LabelInside:=(tmpValue>=(IMinimum-DifFloat)) and
                     (tmpValue<=(IMaximum+DifFloat));
        if LabelInside then InternalDrawLabel(False);
        Inc(LabelIndex);
      end;
    Until (not LabelInside) or (LabelIndex>2000) or Stop; { maximum 2000 labels... }
  end;

  Procedure DrawAxisTitle;
  begin
    if (FAxisTitle.Visible) and (FAxisTitle.FCaption<>'') then
    begin
      if IsDepthAxis then DrawTitle(PosTitle,ParentChart.ChartRect.Bottom)
      else
      if Horizontal then DrawTitle(ICenterPos,PosTitle)
                    else DrawTitle(PosTitle,ICenterPos);
    end;
  end;

  Procedure DepthAxisLabels;
  Var t     : Integer;
      tmp   : Integer;
      tmpSt : String;
  Begin
    if ParentChart.CountActiveSeries>0 then
      for t:=Trunc(IMinimum) to Trunc(IMaximum) do
      Begin
        tmp:=Self.CalcYPosValue(IMaximum-t-0.5);
        if not TickOnLabelsOnly then AddTick(tmp);
        if FLabels then
        begin
          With ParentChart do
          begin
            tmpSt:=SeriesTitleLegend(t,True);
            if Assigned(FOnGetAxisLabel) then
               FOnGetAxisLabel(TChartAxis(Self),nil,t,tmpSt);
          end;
          DrawThisLabel(tmp,tmpSt);
        end;
      end;
  end;

Begin
  With ParentChart,ChartRect do
  Begin
    IAxisDateTime:=IsDateTime;
    Canvas.AssignFont(Self.FLabelsFont);
    IIncrement:=CalcIncrement;

    if CalcPosAxis then
    begin
      FPosAxis:=ApplyPosition(GetRectangleEdge(ChartRect),ChartRect);
    end;

    DrawAxisTitle;

    if IAxisDateTime and FExactDateTime and (FDesiredIncrement<>0) then
    begin
      tmpWhichDateTime:=FindDateTimeStep(FDesiredIncrement);
      if tmpWhichDateTime<>dtNone then
      While (IIncrement>DateTimeStep[tmpWhichDateTime]) and
            (tmpWhichDateTime<>dtOneYear) do
                tmpWhichDateTime:=Succ(tmpWhichDateTime);
    end
    else tmpWhichDateTime:=dtNone;

    tmpNumTicks:=0;
    {$IFDEF D4}
    SetLength(tmpTicks,TeeMaxAxisTicks);
    {$ENDIF}
    try
      if ((IIncrement>0) or
         ( (tmpWhichDateTime>=dtHalfMonth) and (tmpWhichDateTime<=dtOneYear)) )
         and (IMaximum>=IMinimum) then
      Begin
        tmpLabelStyle:=CalcLabelStyle;
        Case tmpLabelStyle of
         talValue: if Assigned(FOnGetNextAxisLabel) then DoCustomLabels
                                                    else DoNotCustomLabels;
          talMark: AxisLabelsSeries;
          talText: if IsDepthAxis then DepthAxisLabels else AxisLabelsSeries;
        end;
      end;
      DrawTicksGrid;
    finally
      {$IFDEF D4}
      SetLength(tmpTicks,0);
      {$ENDIF}
    end;
  end;
end;

Function TChartAxis.SizeTickAxis:Integer;
begin
  if FAxis.Visible then result:=FAxis.Width+1
                   else result:=0;
  if FTicks.Visible then Inc(result,FTickLength);
  if FMinorTicks.Visible then result:=MaxLong(result,FMinorTickLength);
end;

Function TChartAxis.SizeLabels:Integer;
begin
  result:=InternalCalcSize(FLabelsFont,FLabelsAngle,'',FLabelsSize);
end;

Function TChartAxis.InternalCalcSize( tmpFont:TTeeFont;
                                      tmpAngle:Integer;
                                      Const tmpText:String;
                                      tmpSize:Integer):Integer;
Begin
  if tmpSize<>0 then result:=tmpSize
  else
  With ParentChart,Canvas do
  Begin
    AssignFont(tmpFont);
    if Horizontal then
    Case tmpAngle of
      0, 180: result:=FontHeight;
    else { optimized for speed }
      if tmpText='' then result:=MaxLabelsWidth
                    else result:=Canvas.TextWidth(tmpText);
    end
    else
    Case tmpAngle of
     90, 270: result:=FontHeight;
    else { optimized for speed }
      if tmpText='' then result:=MaxLabelsWidth
                    else result:=Canvas.TextWidth(tmpText);
    end;
  end;
end;

Procedure TChartAxis.CalcRect(Var R:TRect; InflateChartRectangle:Boolean);

  Procedure InflateAxisRect(Value:Integer);
  Begin
    With R do
    if Horizontal then
       if OtherSide then Inc(Top,Value) else Dec(Bottom,Value)
    else
       if OtherSide then Dec(Right,Value) else Inc(Left,Value);
  end;

  Function InflateAxisPos(Value:Integer; Amount:Integer):Integer;
  Begin
    result:=Value;
    if Horizontal then
       if OtherSide then Dec(result,Amount) else Inc(result,Amount)
    else
       if OtherSide then Inc(result,Amount) else Dec(result,Amount);
  end;

  Function CalcLabelsRect(tmpSize:Integer):Integer;
  begin
    InflateAxisRect(tmpSize);
    result:=GetRectangleEdge(R);
  end;

var tmp : Integer;
Begin
  IAxisDateTime:=IsDateTime;

  if InflateChartRectangle then
  begin
    if IsDepthAxis then
       FPosTitle:=R.Right
    else
       With FAxisTitle do
       if Visible and (Caption<>'') then
          FPosTitle:=CalcLabelsRect(InternalCalcSize(Font,FAngle,FCaption,FTitleSize));

    if FLabels then FPosLabels:=CalcLabelsRect(SizeLabels);

    tmp:=SizeTickAxis;
    Inc(tmp,ParentChart.CalcWallSize(Self));
    if tmp>0 then InflateAxisRect(tmp);
    FPosTitle:=ApplyPosition(FPosTitle,R);
    FPosLabels:=ApplyPosition(FPosLabels,R);
  end
  else
  begin
    FPosAxis:=ApplyPosition(GetRectangleEdge(R),R);
    FPosLabels:=InflateAxisPos(FPosAxis,SizeTickAxis);
    FPosTitle:=InflateAxisPos(FPosLabels,SizeLabels);
  end;
end;

{$IFDEF D5}
constructor TChartAxis.Create(Chart: TCustomAxisPanel);
begin
  Create(Chart.CustomAxes);
end;
{$ENDIF}

{ TChartDepthAxis }
function TChartDepthAxis.InternalCalcLabelStyle: TAxisLabelStyle;
var t : Integer;
begin
  result:=talText;
  With ParentChart do
  for t:=0 to SeriesCount-1 do
  With Series[t] do
  if Active then
     if HasZValues or (MinZValue<>MaxZValue) then
     begin
       result:=talValue;
       break;
     end;
end;

{ TSeriesMarksPosition }
Procedure TSeriesMarkPosition.Assign(Source:TSeriesMarkPosition);
begin
  ArrowFrom :=Source.ArrowFrom;
  ArrowTo   :=Source.ArrowTo;
  LeftTop   :=Source.LeftTop;
  Height    :=Source.Height;
  Width     :=Source.Width;
end;

Function TSeriesMarkPosition.Bounds:TRect;
begin
  result:={$IFDEF D6}Types.{$ELSE}Classes.{$ENDIF}Bounds(LeftTop.X,LeftTop.Y,Width,Height);
end;

{ TChartValueList }
Constructor TChartValueList.Create(AOwner:TChartSeries; Const AName:String);
begin
   inherited Create;
   Modified:=True;
   {$IFDEF TEEMULTIPLIER}
   FMultiplier:=1;
   {$ENDIF}
   FOwner:=AOwner;
   FOwner.FValuesList.Add(Self);
   FName:=AName;
   {$IFNDEF TEEARRAY}
   FList:=TList.Create;
   {$ENDIF}
   ClearValues;
end;

Destructor TChartValueList.Destroy;
begin
   {$IFDEF TEEARRAY}
   ClearValues;
   {$ELSE}
   FList.Free;
   {$ENDIF}
   inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TChartValueList.Load(var S: TOldStream; AOwner: TChartSeries);
begin
   inherited Create;
   FOwner:=AOwner;
   FOwner.FValuesList.Add(Self);
   FName:=S.ReadString;
   S.Read(FDateTime,SizeOf(Boolean));
   S.Read(FOrder,SizeOf(TChartListOrder));
   ClearValues;
end;

Procedure TChartValueList.Store(var S: TOldStream);
begin
   S.WriteString(FName);
   S.Write(FDateTime,SizeOf(Boolean));
   S.Write(FOrder,SizeOf(TChartListOrder));
end;

Function TChartValueList.OnCompareItems(Item: TPersistent): Boolean;
begin
   Result:=FALSE;
   if Item is TChartValueList then
     with TChartValueList(Item) do
       if Self.FName = FName then
         if Self.FDateTime = FDateTime then
           Result:=Self.FOrder = FOrder;
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Function TChartValueList.First:TChartValue;
Begin
  result:=Value[0]
end;

{$IFNDEF TEEARRAY}
Function TChartValueList.Count:Integer;
Begin
  result:=FList.Count; { <-- virtual }
end;
{$ENDIF}

Function TChartValueList.Last:TChartValue;
Begin
  result:=Value[Count-1]
end;

Procedure TChartValueList.Delete(ValueIndex:Integer);
Begin
  {$IFDEF TEEARRAY}
  System.Move(Value[ValueIndex+1],Value[ValueIndex],SizeOf(TChartValue)*(Count-ValueIndex));
  Dec(Count);
  {$ELSE}
  Dispose(PChartValue(FList[ValueIndex]));
  FList.Delete(ValueIndex);
  {$ENDIF}
  Modified:=True;
end;

{$IFDEF TEEARRAY}
Procedure TChartValueList.IncrementArray;
var tmp : Integer;
begin
  Inc(Count);
  tmp:=Length(Value);
  if Count>tmp then SetLength(Value,tmp+TeeDefaultCapacity);
end;
{$ENDIF}

Procedure TChartValueList.InsertChartValue(ValueIndex:Integer; Const AValue:TChartValue);
{$IFDEF TEEARRAY}
Var t : Integer;
{$ELSE}
Var p : PChartValue;
{$ENDIF}
Begin
{$IFDEF TEEARRAY}
  IncrementArray;
  for t:=Pred(Count) downto Succ(ValueIndex) do Value[t]:=Value[t-1];
  Value[ValueIndex]:=AValue;
{$ELSE}
  New(p);
  p^:=AValue;
  FList.Insert(ValueIndex,p);  { <- virtual }
{$ENDIF}
  Modified:=True;
end;

Function TChartValueList.AddChartValue(Const AValue:TChartValue):Integer;
Var t : Integer;
{$IFNDEF TEEARRAY}
    p : PChartValue;
{$ENDIF}
Begin
{$IFNDEF TEEARRAY}
  New(p);    { virtual }
  p^:=AValue;
{$ENDIF}
  if FOrder=loNone then
  begin
    {$IFDEF TEEARRAY}
    result:=Count;
    IncrementArray;
    Value[result]:=AValue;
    {$ELSE}
    result:=FList.Add(p)
    {$ENDIF}
  end
  else
  begin
    t:={$IFDEF TEEARRAY}Count{$ELSE}FList.Count{$ENDIF}-1;
    if (t=-1) or
       ( (FOrder=loAscending) and (AValue>={$IFDEF TEEARRAY}Value[t]{$ELSE}PChartValue(FList[t])^{$ENDIF}) ) or
       ( (FOrder=loDescending) and (AValue<={$IFDEF TEEARRAY}Value[t]{$ELSE}PChartValue(FList[t])^{$ENDIF}) ) then
    begin
      {$IFDEF TEEARRAY}
      result:=Count;
      IncrementArray;
      Value[result]:=AValue;
      {$ELSE}
      result:=FList.Add(p)
      {$ENDIF}
    end
    else
    Begin
      if FOrder=loAscending then
         While (t>=0) and ({$IFDEF TEEARRAY}Value[t]{$ELSE}PChartValue(FList[t])^{$ENDIF}>AValue) do Dec(t)
      else
         While (t>=0) and ({$IFDEF TEEARRAY}Value[t]{$ELSE}PChartValue(FList[t])^{$ENDIF}<AValue) do Dec(t);
      result:=t+1;
      {$IFDEF TEEARRAY}
      IncrementArray;
      for t:=Pred(Count) downto Succ(result) do Value[t]:=Value[t-1];
      Value[result]:=AValue;
      {$ELSE}
      FList.Insert(result,p);
      {$ENDIF}
    end;
  end;
  Modified:=True;
end;

{$IFDEF TEEMULTIPLIER}
Procedure TChartValueList.SetMultiplier(Const Value:Double);
Begin
  if Value<>FMultiplier then
  begin
    FMultiplier:=Value;
    Modified:=True;
    FOwner.NotifyValue(veRefresh,0);
    FOwner.Repaint;
  end;
end;
{$ENDIF}

Function TChartValueList.GetValue(ValueIndex:Integer):TChartValue;
begin
  {$IFDEF TEEARRAY}
  {$IFOPT R+}
  if (ValueIndex<0) or (ValueIndex>(Count-1)) then
     Raise ChartException.CreateFmt(SListIndexError,[ValueIndex]);
  {$ENDIF}
  result:=Value[ValueIndex]{$IFDEF TEEMULTIPLIER}*FMultiplier{$ENDIF};
  {$ELSE}
  result:=PChartValue(FList[ValueIndex])^{$IFDEF TEEMULTIPLIER}*FMultiplier{$ENDIF};
  {$ENDIF}
end;

Function TChartValueList.Locate(Const AValue:TChartValue):Integer;
Var t : Integer;
Begin
  for t:=0 to Count-1 do
  if Value[t]=AValue then
  begin
    result:=t;
    exit;
  end;
  result:=-1;
end;

Procedure TChartValueList.SetValue(ValueIndex:Integer; Const AValue:TChartValue);
begin
  {$IFDEF TEEARRAY}
  {$IFOPT R+}
  if (ValueIndex<0) or (ValueIndex>(Count-1)) then
     Raise ChartException.CreateFmt(SListIndexError,[ValueIndex]);
  {$ENDIF}
  Value[ValueIndex]:=AValue;
  {$ELSE}
  PChartValue(FList[ValueIndex])^:=AValue;
  {$ENDIF}
  Modified:=True;
  FOwner.NotifyValue(veModify,ValueIndex);
end;

Function TChartValueList.GetMaxValue:TChartValue;
begin
  if Modified then RecalcStats;
  result:=FMaxValue;
end;

Function TChartValueList.GetMinValue:TChartValue;
begin
  if Modified then RecalcStats;
  result:=FMinValue;
end;

Function TChartValueList.GetTotal:Double;
begin
  if Modified then RecalcStats;
  result:=FTotal;
end;

Function TChartValueList.GetTotalABS:Double;
begin
  if Modified then RecalcStats;
  result:=FTotalABS;
end;

Procedure TChartValueList.RecalcStats;
var t        : Integer;
    tmpValue : TChartValue;
Begin
  if Count>0 then
  begin
    tmpValue:=Value[0];
    FMinValue:=tmpValue;
    FMaxValue:=tmpValue;
    FTotal:=tmpValue;
    FTotalABS:=Abs(tmpValue);
    for t:=1 to Count-1 do
    Begin
      tmpValue:=Value[t];
      if tmpValue<FMinValue then FMinValue:=tmpValue else
      if tmpValue>FMaxValue then FMaxValue:=tmpValue;
      FTotal:=FTotal+tmpValue;
      FTotalABS:=FTotalABS+Abs(tmpValue);
    end;
  end
  else
  begin
    FMinValue:=0;
    FMaxValue:=0;
    FTotal:=0;
    FTotalABS:=0;
  end;
  Modified:=False;
end;

procedure TChartValueList.SetDateTime(Const Value:Boolean);
Begin
  FOwner.SetBooleanProperty(FDateTime,Value);
end;

Procedure TChartValueList.ClearValues;
{$IFNDEF TEEARRAY}
Var t : Integer;
{$ENDIF}
Begin
  {$IFDEF TEEARRAY}
  SetLength(Value,0);
  Count:=0;
  {$ELSE}
  for t:=0 to FList.Count-1 do Dispose(PChartValue(FList[t])); { virtual }
  FList.Clear;
  FList.Capacity:=TeeDefaultCapacity;
  {$ENDIF}
  Modified:=True;
end;

Function TChartValueList.Range:TChartValue;
begin
  result:=MaxValue-MinValue;
end;

Procedure TChartValueList.Scroll;
var t      : Integer;
    tmpVal : {$IFDEF TEEARRAY}TChartValue{$ELSE}PChartValue{$ENDIF};
Begin
{$IFNDEF TEEARRAY}
  With FList do  { virtual }
{$ENDIF}
  if Count>0 then
  Begin
    {$IFDEF TEEARRAY}
    tmpVal:=Value[0];
    for t:=1 to Count-1 do Value[t-1]:=Value[t];
    Value[Count-1]:=tmpVal;
    {$ELSE}
    tmpVal:=Items[0];
    for t:=1 to Count-1 do Items[t-1]:=Items[t];
    Items[Count-1]:=tmpVal;
    {$ENDIF}
  end;
end;

Procedure TChartValueList.SetValueSource(Const Value:String);
Begin
  if FValueSource<>Value then
  begin
    FValueSource:=Value;
    FOwner.CheckDataSource;
  end;
end;

{$IFDEF TEEARRAY}
Procedure TChartValueList.Exchange(Index1,Index2:Integer);
var tmp : TChartValue;
begin
  {$IFOPT R+}
  if (Index1<0) or (Index1>(Count-1)) then
     Raise ChartException.CreateFmt(SListIndexError,[Index1]);
  if (Index2<0) or (Index2>(Count-1)) then
     Raise ChartException.CreateFmt(SListIndexError,[Index2]);
  {$ENDIF}
  tmp:=Value[Index1];
  Value[Index1]:=Value[Index2];
  Value[Index2]:=tmp;
  Modified:=True;
  FOwner.NotifyValue(veModify,Index1);
end;
{$ENDIF}

Procedure TChartValueList.FillSequence;
var t : Integer;
begin
  for t:=0 to Count-1 do Value[t]:=t;
{$IFDEF TEEARRAY}
  Modified:=True;
  FOwner.NotifyValue(veModify,0);
{$ENDIF}
end;

Function TChartValueList.CompareValueIndex(a,b:Integer):Integer;
var tmpA : TChartValue;
    tmpB : TChartValue;
begin
  tmpA:=Value[a];
  tmpB:=Value[b];
  if tmpA<tmpB then result:=-1 else
  if tmpA>tmpB then result:= 1 else result:= 0;
  if FOrder=loDescending then result:=-result;
end;

procedure TChartValueList.Sort;
begin
  if FOrder<>loNone then
     TeeSort(0,Count-1,CompareValueIndex,Owner.SwapValueIndex);
end;

Procedure TChartValueList.Assign(Source:TPersistent);
begin
  if Source is TChartValueList then
  With TChartValueList(Source) do
  begin
    Self.FOrder      :=FOrder;
    Self.FDateTime   :=FDateTime;
    {$IFDEF TEEMULTIPLIER}
    Self.FMultiplier :=FMultiplier;
    {$ENDIF}
    Self.FValueSource:=FValueSource;
  end;
end;

{$IFDEF TEEMULTIPLIER}
function TChartValueList.IsMultiStored: Boolean;
begin
  result:=FMultiplier<>1;
end;
{$ELSE}
procedure TChartValueList.ReadMultiplier(Reader: TReader);
begin
  if Reader.ReadFloat<>1 then
     ShowMessage('Error: Multiplier property is obsolete.'+#13+
                 'Modify TeeDefs.inc (enable Multiplier) and recompile.');
end;

procedure TChartValueList.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('Multiplier',ReadMultiplier,nil,False);
end;
{$ENDIF}

{ TSeriesMarksPositions }
Procedure TSeriesMarksPositions.SetPosition(Index:Integer; APosition:TSeriesMarkPosition);
begin
  While Index>=Count do Add(nil);
  if Items[Index]=nil then Items[Index]:=TSeriesMarkPosition.Create;
  With Position[Index] do
  begin
    Custom:=True;
    Assign(APosition);
  end;
end;

Function TSeriesMarksPositions.GetPosition(Index:Integer):TSeriesMarkPosition;
begin
  if (Index<Count) and Assigned(Items[Index]) then
     result:=TSeriesMarkPosition(Items[Index])
  else
     result:=nil
end;

Procedure TSeriesMarksPositions.Automatic(Index:Integer);
begin
  if (Index<Count) and Assigned(Items[Index]) then Position[Index].Custom:=False;
end;

{$IFNDEF D4}
Destructor TSeriesMarksPositions.Destroy;
{$ELSE}
procedure TSeriesMarksPositions.Clear;
{$ENDIF}
var t : Integer;
begin
  for t:=0 to Count-1 do Position[t].Free;
  inherited;
end;

Function TSeriesMarksPositions.ExistCustom:Boolean;
var t : Integer;
begin
  result:=False;
  for t:=0 to Count-1 do
  if Assigned(Position[t]) and Position[t].Custom then
  begin
    result:=True;
    Exit;
  end;
end;

{ TSeriesMarksGradient }
constructor TSeriesMarksGradient.Create(ChangedEvent: TNotifyEvent);
begin
  inherited;
  Direction:=gdRightLeft;
  EndColor:=clWhite;
  StartColor:=clSilver;
end;

{ TSeriesMarks }
Constructor TSeriesMarks.Create(AOwner: TChartSeries);
begin
   inherited Create(nil);
   FParent         := AOwner;
   FMarkerStyle    := smsLabel;
   FArrow          := TChartArrowPen.Create(FParent.CanvasChanged);
   FDrawEvery      := 1;
   FArrowLength    := 8;
   FPositions      := TSeriesMarksPositions.Create;
   { inherited }
   Color           := ChartMarkColor;
   ShadowSize      := 1;
   ShadowColor     := clGray;
   Visible         := False;
end;

Destructor TSeriesMarks.Destroy;
begin
   FArrow.Free;
   FPositions.Free;
   inherited;
end;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TSeriesMarks.Load(var S: TOldStream; AOwner: TChartSeries);
begin
   inherited Load(S,NIL);
   FParent:= AOwner;
   S.Read(FMarkerStyle,SizeOf(TSeriesMarksStyle));
   FArrow:=TChartArrowPen.Load(S,FParent.CanvasChanged);
   S.Read(FDrawEvery,SizeOf(Integer));
   S.Read(FArrowLength,SizeOf(Integer));
   FPositions:=TSeriesMarksPositions.Create;

   S.Read(FAngle,SizeOf(Integer));
   S.Read(FClip,SizeOf(Boolean));
   S.Read(FMultiLine,SizeOf(Boolean));
   S.Read(FZPosition,SizeOf(Integer));
end;

Procedure TSeriesMarks.Store(var S: TOldStream);
begin
   inherited Store(S);
   S.Write(FMarkerStyle,SizeOf(TSeriesMarksStyle));
   FArrow.Store(S);
   S.Write(FDrawEvery,SizeOf(Integer));
   S.Write(FArrowLength,SizeOf(Integer));

   S.Write(FAngle,SizeOf(Integer));
   S.Write(FClip,SizeOf(Boolean));
   S.Write(FMultiLine,SizeOf(Boolean));
   S.Write(FZPosition,SizeOf(Integer));
end;

Function TSeriesMarks.OnCompareItems(Item: TPersistent): Boolean;
begin
   Result:=FALSE;
   if Item is TSeriesMarks then
     with TSeriesMarks(Item) do
       if Self.FMarkerStyle = FMarkerStyle then
         if Self.FDrawEvery = FDrawEvery then
           if Self.FArrowLength = FArrowLength then
             if Self.FAngle = FAngle then
               if Self.FClip = FClip then
                 if Self.FMultiLine = FMultiLine then
                   if Self.FZPosition = FZPosition then
                     if Self.FArrow.OnCompareItems(FArrow) then
                       Result := inherited OnCompareItems(Item);
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TSeriesMarks.Assign(Source:TPersistent);
Begin
  if Source is TSeriesMarks then
  With TSeriesMarks(Source) do
  Begin
    Self.FAngle      :=FAngle;
    Self.FArrow.Assign(FArrow);
    Self.FArrowLength:=FArrowLength;
    Self.FClip       :=FClip;
    Self.FDrawEvery  :=FDrawEvery;
    Self.FMarkerStyle:=FMarkerStyle;
    Self.FMultiLine  :=FMultiLine;
  end;
  inherited;
end;

Function TSeriesMarks.GetBackColor:TColor;
begin
  result:=Color;
end;

Procedure TSeriesMarks.SetBackColor(Value:TColor);
begin
  Color:=Value;
end;

Procedure TSeriesMarks.ResetPositions;
var t : Integer;
begin
  for t:=0 to Positions.Count-1 do Positions.Position[t].Custom:=False;
  FParent.Repaint;
end;

Procedure TSeriesMarks.SetArrow(Value:TChartArrowPen);
begin
  FArrow.Assign(Value);
end;

Procedure TSeriesMarks.SetArrowLength(Value:Integer);
Begin
  FParent.SetIntegerProperty(FArrowLength,Value);
end;

Procedure TSeriesMarks.SetDrawEvery(Value:Integer);
begin
  FParent.SetIntegerProperty(FDrawEvery,Value);
end;

Procedure TSeriesMarks.SetAngle(Value:Integer);
begin
  FParent.SetIntegerProperty(FAngle,Value);
end;

Procedure TSeriesMarks.SetMultiLine(Value:Boolean);
Begin
  FParent.SetBooleanProperty(FMultiLine,Value);
end;

Function TSeriesMarks.Clicked(X,Y:Integer):Integer;
var t : Integer;
    P : TPoint;
begin
  if Assigned(FParent.ParentChart) then
     FParent.ParentChart.Canvas.Calculate2DPosition(x,y,ZPosition);
  P:={$IFDEF D6}Types.{$ENDIF}Point(x,y);
  with FPositions do
  for t:=0 to Count-1 do
  if (t mod FDrawEvery=0) and
     Assigned(Position[t]) and
     {$IFDEF D6}Types.{$ENDIF}PtInRect(Position[t].Bounds,P) then
  begin
    result:=t;
    Exit;
  end;
  result:=-1;
end;

Procedure TSeriesMarks.UsePosition( Index:Integer; Var MarkPosition:TSeriesMarkPosition);
var tmp : TSeriesMarkPosition;
    Old : TPoint;
begin
  with FPositions do
  begin
    while Index>=Count do Add(nil);
    if Items[Index]=nil then
    begin
      tmp:=TSeriesMarkPosition.Create;
      tmp.Custom:=False;
      Items[Index]:=tmp;
    end;
    tmp:=Position[Index];
    if tmp.Custom then
    begin
      if MarkPosition.ArrowFix then Old:=MarkPosition.ArrowFrom;
      MarkPosition.Assign(tmp);
      if MarkPosition.ArrowFix then MarkPosition.ArrowFrom:=Old;
    end
    else tmp.Assign(MarkPosition);
  end;
end;

Procedure TSeriesMarks.ApplyArrowLength(APosition:TSeriesMarkPosition);
var tmp : Integer;
begin
  tmp:=FArrowLength;
  With APosition do
  begin
    Dec(LeftTop.Y,tmp);
    Dec(ArrowTo.Y,tmp);
  end;
end;

Procedure TSeriesMarks.SetStyle(Value:TSeriesMarksStyle);
Begin
  if FMarkerStyle<>Value then
  begin
    FMarkerStyle:=Value;
    FParent.Repaint;
  end;
end;

Procedure TSeriesMarks.SetClip(Value:Boolean);
Begin
  FParent.SetBooleanProperty(FClip,Value);
end;

Procedure TSeriesMarks.DrawText(Const R:TRect; Const St:String);
Var tmpNumRow    : Integer;
    tmpRowHeight : Integer;

  Procedure DrawTextLine(Const LineSt:String);
  var tmpP : TPoint;
      tmpY : Integer;
      tmp  : Extended;
      S,C  : Extended;
      tmpCenter: TPoint;
  begin
    RectCenter(R,tmpCenter.X,tmpCenter.Y);
    if Angle<>0 then
    begin
      tmp:=Angle*TeePiStep;
      SinCos(tmp,S,C);
      tmpY:=tmpNumRow*tmpRowHeight-(R.Bottom-tmpCenter.y);
      tmpP.X:=tmpCenter.x+Round(tmpY*S);
      if Angle=90 then Inc(tmpP.X,2);
      tmpP.Y:=tmpCenter.y+Round(tmpY*C);
    end
    else
    begin
      tmpP.x:=tmpCenter.x;
      tmpP.y:=R.Top+tmpNumRow*tmpRowHeight;
      if Pen.Visible then Inc(tmpP.y,Pen.Width);
    end;
    With FParent.ParentChart.Canvas do
    if Supports3DText then
       if Angle=0 then
          TextOut3D(tmpP.X,tmpP.Y,ZPosition,LineSt)
       else
          RotateLabel3D(tmpP.X,tmpP.Y,ZPosition,LineSt,FAngle)
    else
       if Angle=0 then
          TextOut(tmpP.X,tmpP.Y,LineSt)
       else
          RotateLabel(tmpP.X,tmpP.Y,LineSt,FAngle);
  end;

Var i     : Integer;
    tmpSt : String;
begin
  tmpNumRow:=0;
  tmpRowHeight:=FParent.ParentChart.Canvas.FontHeight;

  i:=AnsiPos(TeeLineSeparator,St);
  if i>0 then
  begin
    tmpSt:=St;
    Repeat
      DrawTextLine(Copy(tmpSt,1,i-1));
      tmpSt:=Copy(tmpSt,i+1,255);
      Inc(tmpNumRow);
      i:=AnsiPos(TeeLineSeparator,tmpSt);
    Until i=0;
    if tmpSt<>'' then DrawTextLine(tmpSt);
  end
  else DrawTextLine(St);
end;

Procedure TSeriesMarks.InternalDraw( Index:Integer; AColor:TColor;
                                Const St:String; APosition:TSeriesMarkPosition);
Const ArrowColors:Array[Boolean] of TColor=(clBlack,clWhite);
var tmpPos2D  : TPoint;
    tmpDifX   : Integer;
    tmpDifY   : Integer;
    tmp3D     : Boolean;
    tmpBounds : TRect;
begin
  UsePosition(Index,APosition);
  with FParent,ParentChart.Canvas do
  Begin
    tmp3D:=ParentChart.View3D;
    if FArrow.Visible then
    Begin
      Brush.Style:=bsClear;
      if TeeCheckMarkArrowColor and
         ( (FArrow.Color=AColor) or (FArrow.Color=ParentChart.Color) ) then
         AssignVisiblePenColor(FArrow,ArrowColors[ParentChart.Color=clBlack])
      else
         AssignVisiblePen(FArrow);
      With APosition do
      if tmp3D then LineWithZ(ArrowFrom.X,ArrowFrom.Y,ArrowTo.X,ArrowTo.Y,ZPosition)
               else Line(ArrowFrom.X,ArrowFrom.Y,ArrowTo.X,ArrowTo.Y);
    end;

    if tmp3D and (not Supports3DText) then
    With APosition do
    begin
      tmpDifX:=ArrowTo.X-LeftTop.X;
      tmpDifY:=ArrowTo.Y-LeftTop.Y;
      tmpPos2D:=Calculate3DPosition(ArrowTo.X,ArrowTo.Y,ZPosition);
      LeftTop:={$IFDEF D6}Types.{$ENDIF}Point(tmpPos2D.X-tmpDifX,tmpPos2D.Y-tmpDifY);
    end;

    if Transparent then
    begin
      Brush.Style:=bsClear;
      BackMode:=cbmTransparent;
    end
    else AssignBrush(Self.Brush,Self.Color);
    AssignVisiblePen(Self.Pen);

    tmpBounds:=APosition.Bounds;
    DrawRectRotated(APosition.Bounds,FAngle mod 360,Succ(ZPosition));

    Brush.Style:=bsClear;
    BackMode:=cbmTransparent;
    TextAlign:=TA_Center;

    tmpBounds:=APosition.Bounds;
    Inc(tmpBounds.Left,2);
    DrawText(tmpBounds,St);
  end;
end;

function TSeriesMarks.GetGradientClass: TChartGradientClass;
begin
  result:=TSeriesMarksGradient;
end;

{ TTeeFunction }
Constructor TTeeFunction.Create(AOwner: TComponent);
begin
  inherited;
  {$IFDEF TEETRIAL}
  if Assigned(TeeAboutBoxProc) and not (csDesigning in ComponentState) then TeeAboutBoxProc;
  {$ENDIF}
  CanUsePeriod:=True;
  FPeriodAlign:=paCenter;
  FPeriodStyle:=psNumPoints;
end;

Procedure TTeeFunction.BeginUpdate;
begin
  IUpdating:=True;
end;

Procedure TTeeFunction.EndUpdate;
begin
  if IUpdating then
  begin
    IUpdating:=False;
    Recalculate;
  end;
end;

Procedure TTeeFunction.Recalculate;
begin
  if (not IUpdating) and Assigned(FParent) then FParent.CheckDataSource;
end;

Function TTeeFunction.Calculate(SourceSeries:TChartSeries; First,Last:Integer):Double;
begin
  result:=0; { abstract }
end;

Function TTeeFunction.ValueList(ASeries:TChartSeries):TChartValueList;
var tmp : String;
begin
  if Assigned(ParentSeries) then tmp:=ParentSeries.MandatoryValueList.ValueSource
                            else tmp:='';
  With ASeries do
  if tmp='' then result:=MandatoryValueList
            else result:=GetYValueList(tmp);
end;

Procedure TTeeFunction.CalculateAllPoints( Source:TChartSeries;
                                           NotMandatorySource:TChartValueList);
var tmpX : Double;
    tmpY : Double;
begin
  with ParentSeries do
  begin
    tmpY:=Calculate(Source,TeeAllValues,TeeAllValues);
    if not AllowSinglePoint then
    begin
      tmpX:=NotMandatorySource.MinValue;
      AddFunctionXY(Source.YMandatory,tmpX,tmpY);
      tmpX:=NotMandatorySource.MaxValue;
      AddFunctionXY(Source.YMandatory,tmpX,tmpY);
    end
    else { centered point }
    if (not Source.YMandatory) and (ParentSeries.YMandatory) then
    begin
      With NotMandatorySource do tmpX:=MinValue+0.5*Range;
      AddXY(tmpX,tmpY{$IFNDEF D4},'', clTeeColor{$ENDIF});
    end
    else
    begin
      With NotMandatorySource do tmpX:=MinValue+0.5*Range;
      if ParentSeries.YMandatory then
         AddFunctionXY(Source.YMandatory,tmpX,tmpY)
      else
         AddXY(tmpY,tmpX{$IFNDEF D4},'', clTeeColor{$ENDIF})
    end;
  end;
end;

Procedure TTeeFunction.CalculatePeriod( Source:TChartSeries;
                                        Const tmpX:Double;
                                        FirstIndex,LastIndex:Integer);
begin
  AddFunctionXY( Source.YMandatory, tmpX, Calculate(Source,FirstIndex,LastIndex) );
end;

Procedure TTeeFunction.CalculateByPeriod( Source:TChartSeries;
                                          NotMandatorySource:TChartValueList);
var tmpX     : Double;
    tmpCount : Integer;
    tmpFirst : Integer;
    tmpLast  : Integer;
    tmpBreakPeriod:Double;
    PosFirst : Double;
    PosLast  : Double;
    tmpStep  : TDateTimeStep;
    tmpCalc  : Boolean;
begin
  With ParentSeries do
  begin
    tmpFirst:=0;
    tmpCount:=Source.Count;
    tmpBreakPeriod:=NotMandatorySource.Value[tmpFirst];
    tmpStep:=FindDateTimeStep(FPeriod);
    Repeat
      PosLast:=0;
      if FPeriodStyle=psNumPoints then
      begin
        tmpLast:=tmpFirst+Round(FPeriod)-1;
        With NotMandatorySource do
        begin
          PosFirst:=Value[tmpFirst];
          if tmpLast<tmpCount then PosLast:=Value[tmpLast];
        end;
      end
      else
      begin
        tmpLast:=tmpFirst;
        PosFirst:=tmpBreakPeriod;
        GetHorizAxis.IncDecDateTime(True,tmpBreakPeriod,FPeriod,tmpStep);
        PosLast:=tmpBreakPeriod-(FPeriod*0.001);
        While tmpLast<(tmpCount-1) do
          if NotMandatorySource.Value[tmpLast+1]<tmpBreakPeriod then Inc(tmpLast)
                                                                else break;
      end;
      tmpCalc:=False;
      if tmpLast<tmpCount then
      begin
        { align periods }
        if FPeriodAlign=paFirst then tmpX:=PosFirst else
        if FPeriodAlign=paLast then tmpX:=PosLast else
                                    tmpX:=(PosFirst+PosLast)*0.5;
        if (FPeriodStyle=psRange) and (NotMandatorySource.Value[tmpFirst]<tmpBreakPeriod) then
           tmpCalc:=True;
        if (FPeriodStyle=psNumPoints) or tmpCalc then
           CalculatePeriod(Source,tmpX,tmpFirst,tmpLast)
        else
           AddFunctionXY( Source.YMandatory, tmpX, 0 );
      end;
      if (FPeriodStyle=psNumPoints) or tmpCalc then tmpFirst:=tmpLast+1;
    until tmpFirst>tmpCount-1;
  end;
end;

Procedure TTeeFunction.AddFunctionXY(YMandatorySource:Boolean; tmpX,tmpY:Double);
begin
  if not YMandatorySource then SwapDouble(tmpX,tmpY);
  ParentSeries.AddXY( tmpX, tmpY{$IFNDEF D4},'', clTeeColor{$ENDIF});
end;

procedure TTeeFunction.AddPoints(Source:TChartSeries);
Var NotMandatorySource : TChartValueList;
    YMandatorySource   : Boolean;

    Procedure CalculateFunctionMany;
    var t    : Integer;
        tmpX : Double;
        tmpY : Double;
    begin
      for t:=0 to Source.Count-1 do
      begin
        tmpX:=NotMandatorySource.Value[t];
        tmpY:=CalculateMany(ParentSeries.FDataSources,t);
        if not YMandatorySource then SwapDouble(tmpX,tmpY);
        ParentSeries.AddXY( tmpX,tmpY,Source.XLabel[t]{$IFNDEF D4},clTeeColor{$ENDIF});
      end;
    end;

begin
  if Assigned(Source) then
  With ParentSeries do
  begin
    YMandatorySource:=Source.YMandatory;
    NotMandatorySource:=Source.NotMandatoryValueList;
    if FDataSources.Count>1 then
       CalculateFunctionMany
    else
    if Source.Count>0 then DoCalculation(Source,NotMandatorySource);
  end;
end;

Procedure TTeeFunction.DoCalculation( Source:TChartSeries;
                                      NotMandatorySource:TChartValueList);
begin
  if FPeriod=0 then CalculateAllPoints(Source,NotMandatorySource)
               else CalculateByPeriod(Source,NotMandatorySource);
end;

Procedure TTeeFunction.SetParentSeries(AParent:TChartSeries);
begin
  if AParent<>FParent then
  begin
    if Assigned(FParent) then FParent.FTeeFunction:=nil;
    AParent.SetFunction(Self);
  end;
end;

Function TTeeFunction.CalculateMany(SourceSeriesList:TList; ValueIndex:Integer):Double;
begin
  result:=0;
end;

Procedure TTeeFunction.Assign(Source:TPersistent);
begin
  if Source is TTeeFunction then
  With TTeeFunction(Source) do
  begin
    Self.FPeriod     :=FPeriod;
    Self.FPeriodStyle:=FPeriodStyle;
    Self.FPeriodAlign:=FPeriodAlign;
  end;
end;

Procedure TTeeFunction.InternalSetPeriod(Const APeriod:Double);
begin
  FPeriod:=APeriod;
end;

Procedure TTeeFunction.SetPeriod(Const Value:Double);
begin
  if Value<0 then Raise ChartException.Create(TeeMsg_FunctionPeriod);
  if FPeriod<>Value then
  begin
    FPeriod:=Value;
    Recalculate;
  end;
end;

Procedure TTeeFunction.SetPeriodAlign(Value:TFunctionPeriodalign);
begin
  if Value<>FPeriodAlign then
  begin
    FPeriodAlign:=Value;
    Recalculate;
  end;
end;

Procedure TTeeFunction.SetPeriodStyle(Value:TFunctionPeriodStyle);
begin
  if Value<>FPeriodStyle then
  begin
    FPeriodStyle:=Value;
    Recalculate;
  end;
end;

function TTeeFunction.GetParentComponent: TComponent;
begin
  result:=FParent;
end;

procedure TTeeFunction.SetParentComponent(Value: TComponent);
begin
  if Assigned(Value) then ParentSeries:=TChartSeries(Value);
end;

function TTeeFunction.HasParent: Boolean;
begin
  result:=True;
end;

{ TeeMovingFunction }
Constructor TTeeMovingFunction.Create(AOwner:TComponent);
begin
  inherited;
  InternalSetPeriod(1);
end;

Procedure TTeeMovingFunction.DoCalculation( Source:TChartSeries;
                                      NotMandatorySource:TChartValueList);
Var t : Integer;
    P : Integer;
begin
  P:=Round(FPeriod);
  for t:=P-1 to Source.Count-1 do
      AddFunctionXY( Source.YMandatory, NotMandatorySource.Value[t], Calculate(Source,t-P+1,t));
end;

{ TCustomChartElement }
Constructor TCustomChartElement.Create(AOwner: TComponent);
begin
   inherited;
   {$IFDEF TEETRIAL}
   if Assigned(TeeAboutBoxProc) and not (csDesigning in ComponentState) then TeeAboutBoxProc;
   {$ENDIF}
   FActive:=True;
   FBrush:=TChartBrush.Create(CanvasChanged);
   FPen:=CreateChartPen;
end;

Destructor TCustomChartElement.Destroy;
begin
   FPen.Free;
   FBrush.Free;
   ParentChart:=nil;
   inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TCustomChartElement.Load(var S: TOldStream; AOwner: TComponent);
begin
   inherited Create(AOwner);
   S.Read(FActive,SizeOf(Boolean));
   FBrush:=TChartBrush.Load(S,CanvasChanged);
   FPen:=TChartPen.Load(S,CanvasChanged);

   // Additional operation
    FParent:=TCustomAxisPanel(AOwner);
end;

Procedure TCustomChartElement.Store(var S: TOldStream);
begin
   S.Write(FActive,SizeOf(Boolean));
   FBrush.Store(S);
   FPen.Store(S);
end;

Function TCustomChartElement.OnCompareItems(Item: TComponent): Boolean;
begin
   Result:=FALSE;
   if Item is TCustomChartElement then
      with TCustomChartElement(Item) do
         if Self.FActive = FActive then
            if Self.FBrush.OnCompareItems(FBrush) then
               Result := Self.FPen.OnCompareItems(FPen);
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

procedure TCustomChartElement.Assign(Source:TPersistent);
Begin
  if Source is TCustomChartElement then
  With TCustomChartElement(Source) do
  begin
    Self.FActive:= FActive;
    Self.Brush  := FBrush;
    Self.Pen    := FPen;
    Self.Tag    := Tag;
  end
  else inherited;
end;

Procedure TCustomChartElement.CanvasChanged(Sender:TObject);
Begin
  Repaint;
end;

Function TCustomChartElement.CreateChartPen:TChartPen;
begin
  result:=TChartPen.Create(CanvasChanged);
end;

{ Returns the Form class name that allows editing our properties }
class Function TCustomChartElement.GetEditorClass:String;
Begin
  result:='';
end;

procedure TCustomChartElement.SetBrush(const Value: TChartBrush);
begin
  FBrush.Assign(Value);
end;

procedure TCustomChartElement.SetPen(const Value: TChartPen);
begin
  FPen.Assign(Value);
end;

Procedure TCustomChartElement.Repaint;
Begin
  if Assigned(FParent) then FParent.Invalidate;
end;

Procedure TCustomChartElement.SetActive(Value:Boolean);
Begin
  SetBooleanProperty(FActive,Value);
end;

{ Changes the Parent property }
Procedure TCustomChartElement.SetParentChart(Const Value:TCustomAxisPanel);
begin
  FParent:=Value;
end;

{ Changes the Variable parameter with the Value and repaints if different }
Procedure TCustomChartElement.SetBooleanProperty(Var Variable:Boolean; Value:Boolean);
Begin
  if Variable<>Value then
  begin
    Variable:=Value;   Repaint;
  end;
end;

{ Changes the Variable parameter with the Value and repaints if different }
Procedure TCustomChartElement.SetColorProperty(Var Variable:TColor; Value:TColor);
Begin
  if Variable<>Value then
  begin
    Variable:=Value; Repaint;
  end;
end;

{ Changes the Variable parameter with the Value and repaints if different }
Procedure TCustomChartElement.SetDoubleProperty(Var Variable:Double; Const Value:Double);
begin
  if Variable<>Value then
  begin
    Variable:=Value; Repaint;
  end;
end;

{ Changes the Variable parameter with the Value and repaints if different }
Procedure TCustomChartElement.SetIntegerProperty(Var Variable:Integer; Value:Integer);
Begin
  if Variable<>Value then
  begin
    Variable:=Value;   Repaint;
  end;
end;

{ Changes the Variable parameter with the Value and repaints if different }
Procedure TCustomChartElement.SetStringProperty(Var Variable:String; Const Value:String);
Begin
  if Variable<>Value then
  begin
    Variable:=Value;   Repaint;
  end;
end;

{ Internal. VCL mechanism to define parentship between chart and series. }
function TCustomChartElement.GetParentComponent: TComponent;
begin
  result:=FParent;
end;

{ Internal. VCL mechanism to define parentship between chart and series. }
procedure TCustomChartElement.SetParentComponent(AParent: TComponent);
begin
  if AParent is TCustomAxisPanel then ParentChart:=TCustomAxisPanel(AParent);
end;

{ Internal. VCL mechanism to define parentship between chart and series. }
function TCustomChartElement.HasParent: Boolean;
begin
  result:=True;
end;

type PClassBitmap=^TClassBitmap;
     TClassBitmap=Record
       AClass  : TObject;
       ABitmap : TBitmap;
     end;

{ Load the ABitmap resource with AClass.ClassName name }
function LoadClassBitmap(AInstance: Integer; Data: Pointer): Boolean;
begin
  result:=True;
  try
    With PClassBitmap(Data)^ do
    if TryFindResource(AInstance,AClass.ClassName,ABitmap) or
       TryFindResource(AInstance,AClass.ClassParent.ClassName,ABitmap) then
         result:=False;
  except
    on Exception do ;
  end;
end;

{ Returns a bitmap from the linked resources that has the same name
  than our class name }
Procedure TCustomChartElement.GetBitmapEditor(ABitmap:TBitmap);
Var tmpBitmap : TClassBitmap;
begin
  tmpBitmap.ABitmap:=ABitmap;
  tmpBitmap.AClass:=Self;
  EnumModules(LoadClassBitmap,@tmpBitmap);
end;

{ TCustomChartSeries }
Constructor TCustomChartSeries.Create(AOwner: TComponent);
begin
   inherited;
   FShowInLegend:=True;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TCustomChartSeries.Load(var S: TOldStream; AOwner: TComponent);
begin
   inherited Load(S,AOwner);
   S.Read(FShowInLegend,SizeOf(Boolean));
   FTitle:=S.ReadString;
   FIdentifier:=S.ReadString;
   S.Read(FStyle,SizeOf(TChartSeriesStyle));
   S.Read(InternalUse,SizeOf(Boolean));
end;

Procedure TCustomChartSeries.Store(var S: TOldStream);
begin
   inherited Store(S);
   S.Write(FShowInLegend,SizeOf(Boolean));
   S.WriteString(FTitle);
   S.WriteString(FIdentifier);
   S.Write(FStyle,SizeOf(TChartSeriesStyle));
   S.Write(InternalUse,SizeOf(Boolean));
end;

Function TCustomChartSeries.OnCompareItems(Item: TComponent): Boolean;
begin
   Result:=FALSE;
   if Item is TCustomChartSeries then
      with TCustomChartSeries(Item) do
         if Self.FShowInLegend = FShowInLegend then
            if Self.FTitle = FTitle then
               if Self.FIdentifier = FIdentifier then
                  if Self.FStyle = FStyle then
                     if Self.InternalUse = InternalUse then
                        Result := inherited OnCompareItems(Item);
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TCustomChartSeries.Added;
begin
end;

procedure TCustomChartSeries.Assign(Source:TPersistent);
Begin
  if Source is TCustomChartSeries then
  With TCustomChartSeries(Source) do
  begin
    Self.FShowInLegend := FShowInLegend;
    Self.FTitle        := FTitle;
    Self.FStyle        := FStyle;
    Self.FIdentifier   := FIdentifier;
  end;
  inherited;
end;

{ Internal. Used in Decision Graph component. }
procedure TCustomChartSeries.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('Identifier',ReadIdentifier,WriteIdentifier,Identifier<>'');  { <-- don't translate }
  Filer.DefineProperty('Style',ReadStyle,WriteStyle,Style<>[]);  { <-- don't translate }
end;

{ Internal. Used in Decision Graph component. }
procedure TCustomChartSeries.ReadIdentifier(Reader: TReader);
begin
  Identifier:=Reader.ReadString;
end;

{ Internal. Used in Decision Graph component. }
procedure TCustomChartSeries.WriteIdentifier(Writer: TWriter);
begin
  Writer.WriteString(Identifier);
end;

{ Internal. Used in Decision Graph component. }
procedure TCustomChartSeries.ReadStyle(Reader: TReader);
var tmp : Integer;
begin
  tmp:=Reader.ReadInteger;
  FStyle:=[];
  if (tmp and 1)=1 then FStyle:=FStyle+[tssIsTemplate];
  if (tmp and 2)=2 then FStyle:=FStyle+[tssDenyChangeType];
  if (tmp and 4)=4 then FStyle:=FStyle+[tssDenyDelete];
  if (tmp and 8)=8 then FStyle:=FStyle+[tssDenyClone];
  if (tmp and 16)=16 then FStyle:=FStyle+[tssIsPersistent];
  if (tmp and 32)=32 then FStyle:=FStyle+[tssHideDataSource];
end;

{ Internal. Used in Decision Graph component. }
procedure TCustomChartSeries.WriteStyle(Writer: TWriter);
var tmp : Integer;
begin
  tmp:=0;
  if tssIsTemplate     in FStyle then Inc(tmp);
  if tssDenyChangeType in FStyle then Inc(tmp,2);
  if tssDenyDelete     in FStyle then Inc(tmp,4);
  if tssDenyClone      in FStyle then Inc(tmp,8);
  if tssIsPersistent   in FStyle then Inc(tmp,16);
  if tssHideDataSource in FStyle then Inc(tmp,32);
  Writer.WriteInteger(tmp);
end;

{ Returns the number of pixels for horizontal margins }
Procedure TCustomChartSeries.CalcHorizMargins(Var LeftMargin,RightMargin:Integer);
begin
  LeftMargin:=0; RightMargin:=0;  { abstract }
end;

{ Returns the number of pixels for vertical margins }
Procedure TCustomChartSeries.CalcVerticalMargins(Var TopMargin,BottomMargin:Integer);
begin
  TopMargin:=0; BottomMargin:=0;  { abstract }
end;

procedure TCustomChartSeries.DoBeforeDrawChart;
begin
end;

{ Called when the Gallery "3D" option is changed. }
Procedure TCustomChartSeries.GalleryChanged3D(Is3D:Boolean);
begin
  ParentChart.View3D:=Is3D;
end;

Procedure TCustomChartSeries.Removed;
begin
  FParent:=nil;
end;

{ Returns True when the tmpSeries parameter is of the same class }
Function TCustomChartSeries.SameClass(tmpSeries:TChartSeries):Boolean;
begin
  result:=( (Self is tmpSeries.ClassType) or (tmpSeries is Self.ClassType));
end;

{ Tells the design-time Series Designer window to repaint }
Procedure TCustomChartSeries.RepaintDesigner;
begin
  if Assigned(ParentChart) and Assigned(ParentChart.Designer) then
     ParentChart.Designer.Repaint;
end;

Procedure TCustomChartSeries.SetActive(Value:Boolean);
Begin
  inherited;
  if Assigned(ParentChart) then ParentChart.BroadcastSeriesEvent(Self,seChangeActive);
  RepaintDesigner;
end;

Procedure TCustomChartSeries.SetShowInLegend(Value:Boolean);
Begin
  SetBooleanProperty(FShowInLegend,Value);
end;

Procedure TCustomChartSeries.SetTitle(Const Value:String);
Begin
  SetStringProperty(FTitle,Value);
  if Assigned(ParentChart) then ParentChart.BroadcastSeriesEvent(Self,seChangeTitle);
  RepaintDesigner;
end;

{ TChartValueLists }
{$IFNDEF D4}
Destructor TChartValueLists.Destroy;
{$ELSE}
Procedure TChartValueLists.Clear;
{$ENDIF}
var t : Integer;
begin
  for t:=0 to Count-1 do ValueList[t].Free;
  inherited;
end;

Function TChartValueLists.Get(Index:Integer):TChartValueList;
begin
   result:=List^[Index];
end;

{ TChartSeries }
Constructor TChartSeries.Create(AOwner: TComponent);
begin
   inherited;
   FZOrder:=TeeAutoZOrder;
   FDepth:=TeeAutoDepth;
   FHorizAxis:=aBottomAxis;
   FVertAxis:=aLeftAxis;
   FValuesList:=TChartValueLists.Create;

   FX:=TChartValueList.Create(Self,TeeMsg_ValuesX);
   FX.FDateTime:=False;
   FX.FOrder:=loAscending;
   NotMandatoryValueList:=FX;

   FY:=TChartValueList.Create(Self,TeeMsg_ValuesY);
   MandatoryValueList:=FY;
   YMandatory:=True;

   FXLabels:=TList.Create;
   FColor :=clTeeColor;
   FCursor:=crDefault;
   FMarks :=TSeriesMarks.Create(Self);
   FValueFormat      :=TeeMsg_DefValueFormat;
   FPercentFormat    :=TeeMsg_DefPercentFormat;
   FDataSources      :=TList.Create;
   FLinkedSeries     :=TList.Create;
   FRecalcOptions    := [rOnDelete,rOnModify,rOnInsert,rOnClear];

   FFirstVisibleIndex:=-1;
   FLastVisibleIndex :=-1;
   AllowSinglePoint  :=True;
   CalcVisiblePoints :=True;
   IUseSeriesColor   :=True;
end;

{ Frees all properties }
Destructor TChartSeries.Destroy;
var t : Integer;
begin
   FreeAndNil(FTeeFunction);
   RemoveAllLinkedSeries;
   FreeAndNil(FDataSources);

   for t:=0 to FLinkedSeries.Count-1 do
       TChartSeries(FLinkedSeries[t]).RemoveDataSource(Self);

   Clear;
   FreeAndNil(FLinkedSeries);
   FValuesList.Free;
   FColors.Free;
   FXLabels.Free;
   FMarks.Free;
   inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TChartSeries.Load(var S: TOldStream; AOwner: TComponent);
begin
   inherited Load(S,AOwner);

   S.Read(FZOrder,SizeOf(Integer));
   S.Read(FDepth,SizeOf(Integer));
   S.Read(FHorizAxis,SizeOf(THorizAxis));
   S.Read(FVertAxis,SizeOf(TVertAxis));

   FValuesList:=TChartValueLists.Create;

   FX:=TChartValueList.Load(S,Self);
   NotMandatoryValueList:=FX;
   FY:=TChartValueList.Load(S,Self);
   MandatoryValueList:=FY;
   S.Read(YMandatory,SizeOf(Boolean));

   FXLabels:=TList.Create;
   S.Read(FColor,SizeOf(TColor));
   S.Read(FColorEachPoint,SizeOf(Boolean));
   FCursor:=crDefault;

   FMarks:=TSeriesMarks.Load(S,Self);
   FValueFormat:=S.ReadString;
   FPercentFormat:=S.ReadString;

   FDataSources:=TList.Create;
   FLinkedSeries:=TList.Create;
   S.Read(FRecalcOptions,SizeOf(TSeriesRecalcOptions));

   S.Read(FFirstVisibleIndex,SizeOf(Integer));
   S.Read(FLastVisibleIndex,SizeOf(Integer));
   S.Read(AllowSinglePoint,SizeOf(Boolean));
   S.Read(CalcVisiblePoints,SizeOf(Boolean));

   S.Read(IUseSeriesColor,SizeOf(Boolean));
   S.Read(DrawBetweenPoints,SizeOf(Boolean));
   S.Read(HasZValues,SizeOf(Boolean));
   S.Read(StartZ,SizeOf(Integer));
   S.Read(MiddleZ,SizeOf(Integer));
   S.Read(EndZ,SizeOf(Integer));
end;

Procedure TChartSeries.Store(var S: TOldStream);
begin
   inherited Store(S);

   S.Write(FZOrder,SizeOf(Integer));
   S.Write(FDepth,SizeOf(Integer));
   S.Write(FHorizAxis,SizeOf(THorizAxis));
   S.Write(FVertAxis,SizeOf(TVertAxis));

   FX.Store(S);
   FY.Store(S);
   S.Write(YMandatory,SizeOf(Boolean));

   S.Write(FColor,SizeOf(TColor));
   S.Write(FColorEachPoint,SizeOf(Boolean));

   FMarks.Store(S);
   S.WriteString(FValueFormat);
   S.WriteString(FPercentFormat);

   S.Write(FRecalcOptions,SizeOf(TSeriesRecalcOptions));

   S.Write(FFirstVisibleIndex,SizeOf(Integer));
   S.Write(FLastVisibleIndex,SizeOf(Integer));
   S.Write(AllowSinglePoint,SizeOf(Boolean));
   S.Write(CalcVisiblePoints,SizeOf(Boolean));

   S.Write(IUseSeriesColor,SizeOf(Boolean));
   S.Write(DrawBetweenPoints,SizeOf(Boolean));
   S.Write(HasZValues,SizeOf(Boolean));
   S.Write(StartZ,SizeOf(Integer));
   S.Write(MiddleZ,SizeOf(Integer));
   S.Write(EndZ,SizeOf(Integer));
end;

Function TChartSeries.OnCompareItems(Item: TCustomChartSeries): Boolean;
begin
   Result:=FALSE;
   if Item is TChartSeries then
     with TChartSeries(Item) do
       if Self.FZOrder = FZOrder then
         if Self.FDepth = FDepth then
           if Self.FHorizAxis = FHorizAxis then
             if Self.FVertAxis = FVertAxis then
               if Self.YMandatory = YMandatory then
                 if Self.FColor = FColor then
                   if Self.FColorEachPoint = FColorEachPoint then
                     if Self.FValueFormat = FValueFormat then
                       if Self.FPercentFormat = FPercentFormat then
                         if Self.FRecalcOptions = FRecalcOptions then
                           if Self.FFirstVisibleIndex = FFirstVisibleIndex then
                             if Self.FLastVisibleIndex = FLastVisibleIndex then
                               if Self.AllowSinglePoint = AllowSinglePoint then
                                 if Self.CalcVisiblePoints = CalcVisiblePoints then
                                   if Self.IUseSeriesColor = IUseSeriesColor then
                                     if Self.DrawBetweenPoints = DrawBetweenPoints then
                                       if Self.HasZValues = HasZValues then
                                         if Self.StartZ = StartZ then
                                           if Self.MiddleZ = MiddleZ then
                                             if Self.EndZ = EndZ then
                                               if Self.FX.OnCompareItems(FX) then
                                                 if Self.FY.OnCompareItems(FY) then
                                                   if Self.FMarks.OnCompareItems(FMarks) then
                                                     Result := inherited OnCompareItems(Item);
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{ Returns True when in design-mode or when we want random sample
  points for the series at run-time. }
Function TChartSeries.CanAddRandomPoints:Boolean;
begin
  result:=TeeRandomAtRunTime or (csDesigning in ComponentState);
end;

{ Called when the series is added into a Chart series list. }
Procedure TChartSeries.Added;
begin
  inherited;
  if FColor=clTeeColor then SeriesColor:=FParent.GetFreeSeriesColor(True);
  RecalcGetAxis;
  if CanAddRandomPoints and (FunctionType=nil) then
     FillSampleValues(NumSampleValues)
  else
     CheckDatasource;
end;

{ Called when the series is removed from a Chart series list. }
Procedure TChartSeries.Removed;
begin
  if Cursor=FParent.Cursor then FParent.Cursor:=FParent.OriginalCursor;
  RemoveAllLinkedSeries;
  inherited;
end;

{ internal. VCL streaming mechanism. Stores the function. }
Procedure TChartSeries.GetChildren(Proc:TGetChildProc; Root:TComponent);
begin
  inherited;
  if Assigned(FunctionType) then Proc(FunctionType);
end;

{ Sets the chart that will contain and display the series }
Procedure TChartSeries.SetParentChart(Const Value:TCustomAxisPanel);
Begin
  if FParent<>Value then
  Begin
    if Assigned(FParent) then FParent.RemoveSeries(Self);
    inherited;
    FMarks.ParentChart:=FParent;
    if Assigned(FParent) then FParent.InternalAddSeries(Self);
  end;
end;

{ Associates the series to a function }
procedure TChartSeries.SetFunction(AFunction:TTeeFunction);
begin
  FTeeFunction.Free;
  FTeeFunction:=AFunction;
  if Assigned(FTeeFunction) then FTeeFunction.FParent:=Self;
  CheckDataSource;
end;

{ When the data source is destroyed, sets it to nil. }
procedure TChartSeries.Notification( AComponent: TComponent;
                                     Operation: TOperation);
var tmp : TComponent;
begin
  inherited;
  if Operation=opRemove then
  begin
    tmp:=DataSource;
    if Assigned(tmp) and (AComponent=tmp) then SetDataSource(nil);
  end;
end;

{ Hidden properties. Defines them to store / load from the DFM stream }
procedure TChartSeries.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('DataSources',ReadData,WriteData,FDataSources.Count > 1);  { <-- don't translate }
  Filer.DefineProperty('CustomHorizAxis', ReadCustomHorizAxis,
                                          WriteCustomHorizAxis,Assigned(FCustomHorizAxis));  { <-- don't translate }
  Filer.DefineProperty('CustomVertAxis', ReadCustomVertAxis,
                                          WriteCustomVertAxis,Assigned(FCustomVertAxis));  { <-- don't translate }
end;

{ internal. Loads data sources from the DFM stream }
procedure TChartSeries.ReadData(Reader: TReader);
begin
  Reader.ReadListBegin;
  FTempDataSources:=TStringList.Create;
  With FTempDataSources do
  begin
    Clear;
    while not Reader.EndOfList do Add(Reader.ReadString);
  end;
  Reader.ReadListEnd;
end;

{ internal. stores data sources into the DFM stream }
procedure TChartSeries.WriteData(Writer: TWriter);
var t : Integer;
begin
  With Writer do
  begin
    WriteListBegin;
    With FDataSources do
         for t:=0 to Count-1 do WriteString(TComponent(Items[t]).Name);
    WriteListEnd;
  end;
end;

{ Returns True (the default), when visible points in the series
  should be drawn in ascending order. }
Function TChartSeries.DrawValuesForward:Boolean;
begin
  result:=True; { abstract }
end;

{ Returns True (the default), when several series sharing the
  same Z order should be drawn in ascending order (the same order
  the series have inside the SeriesList. }
Function TChartSeries.DrawSeriesForward(ValueIndex:Integer):Boolean;
begin
  result:=True; { abstract }
end;

{ Calls DrawValue for all points that are visible. }
procedure TChartSeries.DrawAllValues;
Var t : Integer;
Begin
  if DrawValuesForward then
     for t:=FFirstVisibleIndex to FLastVisibleIndex do DrawValue(t)
  else
     for t:=FLastVisibleIndex downto FFirstVisibleIndex do DrawValue(t);
End;

Procedure TChartSeries.CheckDataSource;
begin
  if Assigned(FParent) then FParent.CheckDataSource(Self);
end;

Procedure TChartSeries.SetColorSource(Const Value:String);
Begin
  if FColorSource<>Value then
  Begin
    FColorSource:=Value;
    if Assigned(FParent) then FParent.CheckDataSource(Self);
  end;
end;

Procedure TChartSeries.SetLabelsSource(Const Value:String);
Begin
  if FLabelsSource<>Value then
  Begin
    FLabelsSource:=Value;
    if Assigned(FParent) then FParent.CheckDataSource(Self);
  end;
end;

{ Returns the point index that is under the XY screen mouse position. }
Function TChartSeries.GetCursorValueIndex:Integer;
Begin
  With ParentChart.GetCursorPos do result:=Clicked(X,Y);
end;

{ Returns the X and Y values that correspond to the mouse
  screen position. }
Procedure TChartSeries.GetCursorValues(Var XValue,YValue:Double);
Begin
  With ParentChart.GetCursorPos do
  begin
    XValue:=XScreenToValue(X);
    YValue:=YScreenToValue(Y);
  end;
end;

{ Returns the data source component.
  It is the first component in the DataSources list. }
Function TChartSeries.GetDataSource:TComponent;
begin
  if Assigned(FDataSources) and (FDataSources.Count>0) then
     result:=FDataSources[0]
  else
     result:=nil;
end;

Procedure TChartSeries.InternalAddDataSource(Value:TComponent);
begin
  if Assigned(Value) then
  begin
    FDataSources.Add(Value);
    if Value is TChartSeries then
       TChartSeries(Value).AddLinkedSeries(Self)
    else
       Value.FreeNotification(Self);
  end;
end;

{ When this series is a datasource for other series, tells the
  other series to remove the reference to itself. }
Procedure TChartSeries.RemoveAllLinkedSeries;
var t : Integer;
begin
  if Assigned(FDataSources) then
     for t:=0 to FDataSources.Count-1 do
     if Assigned(FDataSources[t]) and (TComponent(FDataSources[t]) is TChartSeries) then
        TChartSeries(FDataSources[t]).RemoveLinkedSeries(Self);
end;

{ Replaces the DataSource for the series. }
Procedure TChartSeries.SetDataSource(Value:TComponent);

  Procedure ClearDataSources;
  begin
    RemoveAllLinkedSeries;
    FDataSources.Clear;
  end;

  Procedure InternalRemoveDataSource;
  Begin
    if Assigned(FParent) and
       Assigned(DataSource) then
         FParent.RemovedDataSource(Self,DataSource);
    ClearDataSources;
    if Assigned(FParent) and CanAddRandomPoints then
       FillSampleValues(NumSampleValues)
    else
       Clear;
    Repaint;
  end;

  Procedure InternalSetDataSource;
  Begin
    if not Assigned(FParent) then
       raise ChartException.Create(TeeMsg_SeriesSetDataSource)
    else
    if FParent.IsValidDataSource(Self,Value) then
    Begin
      if FDataSources.IndexOf(Value)=-1 then
      begin
        if Value is TChartSeries then CheckOtherSeries(TChartSeries(Value));
        if not (csLoading in ComponentState) then ClearDataSources;

        InternalAddDataSource(Value);
        if not (csLoading in ComponentState) then FParent.CheckDataSource(Self);
      end;
    end
    else
      Raise ChartException.CreateFmt(TeeMsg_SeriesInvDataSource,[Value.Name]);
  end;

Begin
  if not Assigned(Value) then InternalRemoveDataSource
                         else InternalSetDataSource;
end;

Function TChartSeries.IsValidSourceOf(Value:TChartSeries):Boolean;
Begin
  result:=Value<>Self; { abstract }
end;

Function TChartSeries.IsValidSeriesSource(Value:TChartSeries):Boolean;
Begin
  result:=True; { abstract }
end;

{ Returns the value list that has the AListName parameter. }
Function TChartSeries.GetYValueList(AListName:String):TChartValueList;
Var t : Integer;
Begin
  AListName:=AnsiUppercase(AListName);
  for t:=2 to FValuesList.Count-1 do
  if AListName=AnsiUpperCase(FValuesList[t].Name) then
  Begin
    result:=FValuesList[t];
    exit;
  end;
  result:=FY;
end;

Procedure TChartSeries.CheckOtherSeries(Source:TChartSeries);
Begin
  if Assigned(FParent) then FParent.CheckOtherSeries(Self,Source);
end;

{ Replaces the colors for points in the value list that
  have values between FromValue and ToValue. }
Procedure TChartSeries.ColorRange( AValueList:TChartValueList;
                                   Const FromValue,ToValue:Double; AColor:TColor);
var t        : Integer;
    tmpValue : Double;
begin
  for t:=0 to AValueList.Count-1 do
  Begin
    tmpValue:=AValueList.Value[t];
    if (tmpValue>=FromValue) and (tmpValue<=ToValue) and (not IsNull(t)) then
        ValueColor[t]:=AColor;
  end;
  Repaint;
end;

{ Returns the point index that is under the X Y pixel screen position. }
Function TChartSeries.Clicked(x,y:Integer):Integer;
begin
  Result:=-1; { abstract, no point clicked }
end;

{ Obtains pointers to the associated axes.
  This is done to optimized speed when having to locate the
  associated axes. }
Procedure TChartSeries.RecalcGetAxis;
Begin
  if Assigned(FParent) then
  begin
    FGetHorizAxis:=FParent.FBottomAxis;
    Case FHorizAxis of
      aTopAxis        : FGetHorizAxis:=FParent.FTopAxis;
      aCustomHorizAxis: if Assigned(FCustomHorizAxis) then FGetHorizAxis:=FCustomHorizAxis;
    end;
    FGetVertAxis:=FParent.LeftAxis;
    Case FVertAxis of
      aRightAxis     : FGetVertAxis:=FParent.FRightAxis;
      aCustomVertAxis: if Assigned(FCustomVertAxis) then FGetVertAxis:=FCustomVertAxis;
    end;
  end
  else
  begin
    FGetHorizAxis:=nil;
    FGetVertAxis:=nil;
  end;
end;

procedure TChartSeries.ReadCustomHorizAxis(Reader: TReader);
begin
  CustomHorizAxis:=FParent.CustomAxes[Reader.ReadInteger];
end;

procedure TChartSeries.ReadCustomVertAxis(Reader: TReader);
begin
  CustomVertAxis:=FParent.CustomAxes[Reader.ReadInteger];
end;

procedure TChartSeries.WriteCustomHorizAxis(Writer: TWriter);
begin
  Writer.WriteInteger(FCustomHorizAxis.Index);
end;

procedure TChartSeries.WriteCustomVertAxis(Writer: TWriter);
begin
  Writer.WriteInteger(FCustomVertAxis.Index);
end;

Procedure TChartSeries.SetHorizAxis(Value:THorizAxis);
Begin
  if FHorizAxis<>Value then
  begin
    FHorizAxis:=Value;
    RecalcGetAxis;
    Repaint;
  end;
end;

Procedure TChartSeries.SetVertAxis(Value:TVertAxis);
Begin
  if FVertAxis<>Value then
  begin
    FVertAxis:=Value;
    RecalcGetAxis;
    Repaint;
  end;
end;

Procedure TChartSeries.SetChartValueList( AValueList:TChartValueList;
                                          Value:TChartValueList);
Begin
  AValueList.Assign(Value);
  Repaint;
end;

Procedure TChartSeries.SetXValues(Value:TChartValueList);
Begin
  SetChartValueList(FX,Value);
end;

Procedure TChartSeries.SetYValues(Value:TChartValueList);
Begin
  SetChartValueList(FY,Value);
end;

{ Calculates the range of points that are visible.
  This depends on the series associated vertical and horizontal axis
  scales (minimum and maximum).  }
Procedure TChartSeries.CalcFirstLastVisibleIndex;

  Function CalcMinMaxValue(A,B,C,D:Integer):Double;
  begin
    if YMandatory then
    With GetHorizAxis do
         if Inverted then result:=CalcPosPoint(C)
                     else result:=CalcPosPoint(A)
    else
    With GetVertAxis do
         if Inverted then result:=CalcPosPoint(B)
                     else result:=CalcPosPoint(D);
  end;

Var tmpMin        : Double;
    tmpMax        : Double;
    NotMandatory  : TChartValueList;
    tmpLastIndex  : Integer;
    OrderedPoints : Boolean;
Begin
  FFirstVisibleIndex:=-1;
  FLastVisibleIndex:=-1;
  if Count>0 then
  Begin
    tmpLastIndex:=Count-1;
    if YMandatory then OrderedPoints:=XValues.Order<>loNone
                  else OrderedPoints:=YValues.Order<>loNone;

    if CalcVisiblePoints and OrderedPoints then
    begin
      FFirstVisibleIndex:=0;

      With ParentChart.ChartRect do
           tmpMin:=CalcMinMaxValue(Left,Top,Right,Bottom);

      NotMandatory:=NotMandatoryValueList;

      While NotMandatory.Value[FFirstVisibleIndex]<tmpMin do
      Begin
        inc(FFirstVisibleIndex);
        if FFirstVisibleIndex>tmpLastIndex then
        begin
          FFirstVisibleIndex:=-1;
          break;
        end;
      end;
      if FFirstVisibleIndex>=0 then
      Begin
        With ParentChart.ChartRect do
             tmpMax:=CalcMinMaxValue(Right,Bottom,Left,Top);

        if NotMandatory.Last<=tmpMax then FLastVisibleIndex:=tmpLastIndex
        else
        Begin
          FLastVisibleIndex:=FFirstVisibleIndex;
          While NotMandatory.Value[FLastVisibleIndex]<tmpMax do
          begin
            inc(FLastVisibleIndex);
            if FLastVisibleIndex>tmpLastIndex then
            begin
              FLastVisibleIndex:=tmpLastIndex;
              break;
            end;
          end;
          if (not DrawBetweenPoints) and
             (NotMandatory.Value[FLastVisibleIndex]>tmpMax) then
                Dec(FLastVisibleIndex);
        end;
      end;
    end
    else
    begin
      FFirstVisibleIndex:=0;
      FLastVisibleIndex:=tmpLastIndex;
    end;
  end;
end;

procedure TChartSeries.DrawValue(ValueIndex:Integer);
begin { nothing }
end;

Function TChartSeries.IsValueFormatStored:Boolean;
Begin
  result:=FValueFormat<>TeeMsg_DefValueFormat;
end;

Function TChartSeries.IsPercentFormatStored:Boolean;
Begin
  result:=FPercentFormat<>TeeMsg_DefPercentFormat;
end;

Procedure TChartSeries.DeletedValue(Source:TChartSeries; ValueIndex:Integer);
Begin
  Delete(ValueIndex);
end;

Function TChartSeries.AddChartValue(Source:TChartSeries; ValueIndex:Integer):Integer;
var t    : Integer;
    tmp  : Integer;
    tmpX : Double;
    tmpY : Double;
begin
  With Source do
  begin
    tmpX:=FX.Value[ValueIndex];
    tmpY:=FY.Value[ValueIndex];
  end;
  { if we are adding values from an Horizontal Bar series... }
  if Self.YMandatory<>Source.YMandatory then SwapDouble(tmpX,tmpY);
  { pending: if ...FY.Order<>loNone then (inverted) }
  result:=FX.AddChartValue(tmpX);
  FY.InsertChartValue(result,tmpY);
  { rest of lists... }
  tmp:=Source.ValuesList.Count-1;
  for t:=2 to FValuesList.Count-1 do
  begin
    if t<=tmp then tmpY:=Source.FValuesList[t].Value[ValueIndex]
              else tmpY:=0;
    FValuesList[t].InsertChartValue(result,tmpY);
  end;
end;

{ Called when a new value is added to the series. }
Procedure TChartSeries.AddedValue(Source:TChartSeries; ValueIndex:Integer);
Var tmpIndex : Integer;
Begin
  tmpIndex:=AddChartValue(Source,ValueIndex);
  if Assigned(Source.FColors) then
     if Source.FColors.Count>ValueIndex then
     begin
       if not Assigned(FColors) then GrowColors;
       FColors.Insert(tmpIndex,Source.FColors[ValueIndex]);
     end;

  if Source.FXLabels.Count>ValueIndex then
     InsertLabel(tmpIndex,Source.GetXLabel(ValueIndex));
  NotifyNewValue(Self,tmpIndex);
end;

Function TChartSeries.LegendToValueIndex(LegendIndex:Integer):Integer;
begin
  result:=LegendIndex;
end;

{ Returns the formatted string corresponding to the LegendIndex
  point. The string is used to show at the Legend.
  Uses the LegendTextStyle property to do the appropiate formatting.
}
Function TChartSeries.LegendString( LegendIndex:Integer;
                                    LegendTextStyle:TLegendTextStyle ):String;
var ValueIndex : Integer;

  Function CalcXValue:String;
  Var tmpAxis : TChartAxis;
  begin
    tmpAxis:=GetHorizAxis;
    if Assigned(tmpAxis) then
       result:=tmpAxis.LabelValue(XValues.Value[ValueIndex])
    else
       result:=FormatFloat(ValueFormat,XValues.Value[ValueIndex])
  end;

Var tmpValue : Double;

  Function ValueToStr:String;
  begin
    if MandatoryValueList.DateTime then
       DateTimeToString(result,DateTimeDefaultFormat(tmpValue),tmpValue)
    else
       result:=FormatFloat(ValueFormat,tmpValue);
  end;

  Function CalcPercentSt:String;
  begin
    With MandatoryValueList do
    if TotalAbs=0 then result:=FormatFloat(PercentFormat,100)
                  else result:=FormatFloat(PercentFormat,100.0*tmpValue/TotalAbs);
  end;

begin
  ValueIndex:=LegendToValueIndex(LegendIndex);
  result:=XLabel[ValueIndex];
  if LegendTextStyle<>ltsPlain then
  begin
    tmpValue:=GetMarkValue(ValueIndex);
    Case LegendTextStyle of
      ltsLeftValue   : begin
                         if result<>'' then result:=TeeColumnSeparator+result;
                         result:=ValueToStr+result;
                       end;
      ltsRightValue  : begin
                         if result<>'' then result:=result+TeeColumnSeparator;
                         result:=result+ValueToStr;
                       end;
      ltsLeftPercent : begin
                         if result<>'' then result:=TeeColumnSeparator+result;
                         result:=CalcPercentSt+result;
                       end;
      ltsRightPercent: begin
                         if result<>'' then result:=result+TeeColumnSeparator;
                         result:=result+CalcPercentSt;
                       end;
      ltsXValue      : result:=CalcXValue;
      ltsValue       : result:=ValueToStr;
      ltsPercent     : result:=CalcPercentSt;
      ltsXandValue   : result:=CalcXValue+TeeColumnSeparator+ValueToStr;
      ltsXandPercent : result:=CalcXValue+TeeColumnSeparator+CalcPercentSt;
    end;
  end;
end;

{ Clears and adds all points values from Source series. }
Procedure TChartSeries.AddValues(Source:TChartSeries);
var t : Integer;
Begin
  if IsValidSourceOf(Source) then
  begin
    BeginUpdate;
    Clear;
    if FunctionType=nil then { "Copy Function", copy values... }
       for t:=0 to Source.Count-1 do AddedValue(Source,t)
    else
    begin
      XValues.DateTime:=Source.XValues.DateTime;
      YValues.DateTime:=Source.YValues.DateTime;
      FunctionType.AddPoints(Source); { calculate function }
    end;
    EndUpdate; { propagate changes... }
  end;
end;

{ Copies (adds) all values from Source series. }
Procedure TChartSeries.AssignValues(Source:TChartSeries);
Begin
  AddValues(Source);
End;

{ Notifies all series and tools that data (points) has changed. }
Procedure TChartSeries.RefreshSeries;
Begin
  NotifyValue(veRefresh,0);
  if Assigned(ParentChart) then
     ParentChart.BroadcastSeriesEvent(Self,seDataChanged);
End;

{ Returns True if there are more series that share the same Z order.
  For example: Stacked Bars. }
Function TChartSeries.MoreSameZOrder:Boolean;
Var tmpSeries : TChartSeries;
    t         : Integer;
Begin
  if FParent.ApplyZOrder then  { fix 4.02 }
  for t:=0 to FParent.SeriesCount-1 do
  Begin
    tmpSeries:=FParent.Series[t];
    if tmpSeries<>Self then
    With tmpSeries do
    if FActive and (not HasZValues) and (ZOrder=Self.ZOrder) then
    Begin
      result:=True;
      exit;
    end;
  end;
  result:=False;
End;

{ Returns True if the series is the first one with the same Z order.
  Some series allow sharing the same Z order (example: Stacked Bars). }
Function TChartSeries.FirstInZOrder:Boolean;
Var tmpSeries : TChartSeries;
    t         : Integer;
Begin
  if FActive then
  begin
    result:=True;
    for t:=0 to FParent.SeriesCount-1 do
    Begin
     tmpSeries:=FParent.Series[t];
     if tmpSeries=Self then break
     else
       With tmpSeries do
       if FActive and (ZOrder=Self.ZOrder) then
       Begin
         result:=False;
         break;
       end;
    end;
  end
  else result:=False;
end;

{ Calls the BeforeDrawValues event. }
procedure TChartSeries.DoBeforeDrawValues;
Begin
  if Assigned(FBeforeDrawValues) then FBeforeDrawValues(Self);
end;

{ Calls the AfterDrawValues event. }
procedure TChartSeries.DoAfterDrawValues;
Begin
  if Assigned(FAfterDrawValues) then FAfterDrawValues(Self);
end;

{ Draws all Marks for all points in the series that are visible. }
procedure TChartSeries.DrawMarks;
Var t         : Integer;
    St        : String;
    tmpWidth  : Integer;
    tmpW      : Integer;
    tmpH      : Integer;
    APosition : TSeriesMarkPosition;
Begin
  APosition:=TSeriesMarkPosition.Create;
  With FParent,FMarks do
  for t:=FFirstVisibleIndex to FLastVisibleIndex do
  if (t mod FDrawEvery)=0 then
  if not IsNull(t) then
  Begin
    St:=GetMarkText(t);
    if St<>'' then
    Begin
      Canvas.AssignFont(FMarks.Font);
      if View3D and (not Canvas.Supports3DText) and View3DOptions.ZoomText then
         With Canvas.Font do
              Size:=MaxLong(1,Round(0.01*View3DOptions.Zoom*Size));

      tmpW:=MultiLineTextWidth(St,tmpH)+Canvas.TextWidth(TeeCharForHeight);
      tmpH:=tmpH*Canvas.FontHeight;

      Canvas.AssignVisiblePen(FMarks.Pen);
      if FMarks.Pen.Visible then
      Begin
        tmpWidth:=2*FMarks.Pen.Width;
        Inc(tmpW,tmpWidth);
        Inc(tmpH,tmpWidth);
      end
      else Inc(tmpH);

      With APosition do
      begin
        Width:=tmpW;
        Height:=tmpH;
        ArrowTo.X:=CalcXPos(t);
        ArrowTo.Y:=CalcYPos(t);
        ArrowFrom:=ArrowTo;
        LeftTop.X:=ArrowTo.X-(tmpW div 2);
        LeftTop.Y:=ArrowTo.Y-tmpH+1;
      end;

      DrawMark(t,St,APosition);
    end;
  end;
  APosition.Free;
end;

{ Draws a point Mark using the APosition coordinates. }
Procedure TChartSeries.DrawMark( ValueIndex:Integer; Const St:String;
                                 APosition:TSeriesMarkPosition);
Begin
  FMarks.InternalDraw(ValueIndex,ValueColor[ValueIndex],St,APosition);
end;

{ Allocates memory for the ALabel string parameter and inserts it
  into the labels list. }
Procedure TChartSeries.InsertLabel(ValueIndex:Integer; Const ALabel:String);
Var P : PString;
Begin
  if ALabel='' then FXLabels.Insert(ValueIndex,nil)
  else
  begin
    New(P);
    P^:=ALabel;
    FXLabels.Insert(ValueIndex,P);
  end;
end;

{ Triggers the OnAfterAdd event when a new point is added. }
Procedure TChartSeries.NotifyNewValue(Sender:TChartSeries; ValueIndex:Integer);
Begin
  if Assigned(FOnAfterAdd) then FOnAfterAdd(Sender,ValueIndex);
  NotifyValue(veAdd,ValueIndex);
  if FActive then Repaint;
end;

{ Returns True is the ValueIndex point in the Series is Null. }
Function TChartSeries.IsNull(ValueIndex:Integer):Boolean;
begin
  result:=ValueColor[ValueIndex]=clNone;
end;

{ Adds a new Null (missing) point to the series.
  The ALabel parameter is optional. }
Function TChartSeries.AddNull(Const ALabel:String):Integer;
begin
  result:=Add(0,ALabel,clNone);
end;

{ Adds a new Null (missing) point to the Series with the specified X and
  Y values.
  The ALabel parameter is optional. }
Function TChartSeries.AddNullXY(Const X,Y:Double; Const ALabel:String):Integer;
begin
  result:=AddXY(X,Y,ALabel,clNone);
end;

{ Adds the Values array parameter into the series. }
Function TChartSeries.AddArray(Const Values:Array of TChartValue):Integer;
var t : Integer;
begin
  BeginUpdate;
  result:=High(Values)-Low(Values)+1;
  for t:=Low(Values) to High(Values) do Add(Values[t],{$IFNDEF D4}'',clTeeColor{$ENDIF});
  EndUpdate;
end;

{ Adds a new point to the Series.
  The ALabel and AColor parameters are optional. }
Function TChartSeries.Add(Const AValue:Double; Const ALabel:String; AColor:TColor):Integer;
Begin
  if MandatoryValueList=YValues then
     result:=AddXY(FX.Count, AValue, ALabel, AColor )
  else
     result:=AddXY(AValue, FY.Count, ALabel, AColor );
end;

Function TChartSeries.AddX(Const AXValue:Double; Const ALabel:String; AColor:TColor):Integer;
Begin
  result:=AddXY(AXValue,FX.Count,ALabel,AColor);
end;

Function TChartSeries.AddY(Const AYValue:Double; Const ALabel:String; AColor:TColor):Integer;
Begin
  result:=AddXY(FX.Count,AYValue,ALabel,AColor);
end;

{ Adds a new point into the Series. }
Function TChartSeries.AddXY( Const AXValue,AYValue:Double;
                             Const ALabel:String; AColor:TColor):Integer;
var t : Integer;
Begin
  result:=-1;
  FX.TempValue:=AXValue;
  FY.TempValue:=AYValue;
  if (not Assigned(FOnBeforeAdd)) or FOnBeforeAdd(Self) then
  Begin
    result:=FX.AddChartValue(FX.TempValue);
    FY.InsertChartValue(result,FY.TempValue);

    for t:=2 to ValuesList.Count-1 do
        With ValuesList[t] do InsertChartValue(result,TempValue);

    if Assigned(FColors) then
       FColors.Insert(result,Pointer(AColor))
    else
    if AColor<>clTeeColor then
    begin
      GrowColors;
      FColors.Insert(result,Pointer(AColor));
    end;

    InsertLabel(result,ALabel);
    NotifyNewValue(Self,result);
  end;
end;

Function TChartSeries.Count:Integer; { <-- Total number of points in the series }
Begin
  result:=FX.Count;
end;

{ Virtual. Returns the number of points to show at the Legend.
  Some series override this function to return other values. }
Function TChartSeries.CountLegendItems:Integer;
begin
  result:=Count
end;

{ Returns the number of points that are inside the axis limits. (visible) }
Function TChartSeries.VisibleCount:Integer;
Begin
  result:=FLastVisibleIndex-FFirstVisibleIndex+1;
end;

{ Returns the string showing a "percent" value for a given point.
  For example: "25%"
  The optional "AddTotal" parameter, when True, returns for example:
    "25% of 1234".
}
Function TChartSeries.MarkPercent(ValueIndex:Integer; AddTotal:Boolean):String;
var tmp  : Double;
    tmpF : String;
Begin
  With MandatoryValueList do
  Begin
    if TotalAbs<>0 then tmp:=100.0*Abs(GetMarkValue(ValueIndex))/TotalAbs
                   else tmp:=100;
    result:=FormatFloat(FPercentFormat,tmp);
    if AddTotal then
    begin
      if Marks.MultiLine then tmpF:=TeeMsg_DefaultPercentOf2
                         else tmpF:=TeeMsg_DefaultPercentOf;
      FmtStr(result,tmpF,[result,FormatFloat(FValueFormat,TotalAbs)]);
    end;
  end;
end;

Function TChartSeries.GetOriginValue(ValueIndex:Integer):Double;
begin
  result:=GetMarkValue(ValueIndex);
end;

Function TChartSeries.GetMarkValue(ValueIndex:Integer):Double;
begin
  result:=MandatoryValueList.Value[ValueIndex];
end;

{ Returns the string corresponding to the Series Mark text
  for a given ValueIndex point.
  The Mark text depends on the Marks.Style property. }
Function TChartSeries.GetMarkText(ValueIndex:Integer):String;

  Function XLabelOrValue(ValueIndex:Integer):String;
  Begin
    result:=XLabel[ValueIndex];
    if result='' then result:=FormatFloat(FValueFormat,GetMarkValue(ValueIndex));
  End;

  Function GetAXValue:String;
  begin
    if not Assigned(GetHorizAxis) then
       result:=FormatFloat(FValueFormat,XValues.Value[ValueIndex])
    else
       result:=GetHorizAxis.LabelValue(XValues.Value[ValueIndex]);
  end;

  Function GetAYValue:String;
  begin
    result:=FormatFloat(FValueFormat,GetMarkValue(ValueIndex));
  end;

var tmp : Char;
Begin
  With FMarks do
  begin
    if MultiLine then tmp:=TeeLineSeparator else tmp:=' ';
    Case FMarkerStyle of
      smsValue:        result:=GetAYValue;
      smsPercent:      result:=MarkPercent(ValueIndex,False);
      smsLabel:        result:=XLabelOrValue(ValueIndex);
      smsLabelPercent: result:=XLabelOrValue(ValueIndex)+tmp+MarkPercent(ValueIndex,False);
      smsLabelValue:   result:=XLabelOrValue(ValueIndex)+tmp+GetAYValue;
      smsLegend:       result:=Self.ParentChart.FormattedValueLegend(Self,ValueIndex);
      smsPercentTotal: result:=MarkPercent(ValueIndex,True);
 smsLabelPercentTotal: result:=XLabelOrValue(ValueIndex)+tmp+MarkPercent(ValueIndex,True);
      smsXValue:       result:=GetAXValue;
      smsXY:           result:=GetAXValue+tmp+GetAYValue;
    else
      result:='';
    end;
  end;
  if Assigned(FOnGetMarkText) then FOnGetMarkText(Self,ValueIndex,result);
end;

Procedure TChartSeries.SetValueFormat(Const Value:String);
Begin
  SetStringProperty(FValueFormat,Value);
end;

Procedure TChartSeries.SetPercentFormat(Const Value:String);
Begin
  SetStringProperty(FPercentFormat,Value);
end;

{ Changes visual properties appropiate to show the series at
  the gallery. }
Procedure TChartSeries.PrepareForGallery(IsEnabled:Boolean);
begin
  inherited;
  FillSampleValues(4);
  With Marks do
  begin
    Visible:=False;
    Font.Size:=7;
    ArrowLength:=4;
    DrawEvery:=2;
  end;
  FColorEachPoint:=False;
  if IsEnabled then
     if ParentChart.SeriesList.IndexOf(Self)=0 then SeriesColor:=clTeeGallery1
                                               else SeriesColor:=clTeeGallery2
  else
     SeriesColor:=clSilver;
end;

Function TChartSeries.NumSampleValues:Integer;
Begin
  result:=25;  { default number or random values at design time }
end;

{ Calculates appropiate random limits used to add sample
  values (FillSampleValues method).
}
Function TChartSeries.RandomBounds(NumValues:Integer):TSeriesRandomBounds;
Var MinX : Double;
    MaxX : Double;
    MaxY : Double;
Begin
  result.MinY:=0;
  MaxY:=ChartSamplesMax;
  if Assigned(FParent) and (FParent.GetMaxValuesCount>0) then
  begin
    result.MinY:=FParent.MinYValue(GetVertAxis);
    MaxY:=FParent.MaxYValue(GetVertAxis);
    if MaxY=result.MinY then
       if MaxY=0 then MaxY:=ChartSamplesMax
                 else MaxY:=2.0*result.MinY;
    MinX:=FParent.MinXValue(GetHorizAxis);
    MaxX:=FParent.MaxXValue(GetHorizAxis);
    if MaxX=MinX then
       if MaxX=0 then MaxX:=NumValues
                 else MaxX:=2.0*MinX;
    if not YMandatory then
    begin
      SwapDouble(MinX,result.MinY);
      SwapDouble(MaxX,MaxY);
    end;
  end
  else
  begin
    if XValues.DateTime then
    begin
      MinX:=Date;
      MaxX:=MinX+NumValues-1;
    end
    else
    begin
      MinX:=0;
      MaxX:=NumValues-1;
    end;
  end;
  result.StepX:=MaxX-MinX;
  if NumValues>1 then result.StepX:=result.StepX/(NumValues-1);
  result.DifY:=MaxY-result.MinY;
  if result.DifY>MaxLongint then result.DifY:=MaxLongint else
  if result.DifY<-MaxLongint then result.DifY:=-MaxLongint;
  result.tmpY:=result.MinY+System.Random(Round(result.DifY));
  result.tmpX:=MinX;
end;

Procedure TChartSeries.AddSampleValues(NumValues:Integer);
var t    : Integer;
    tmpH : Integer;
begin
  With RandomBounds(NumValues) do
  begin
    tmpH:=Round(DifY) div 4;
    for t:=1 to NumValues do
    begin
      tmpY:=Abs(tmpY+System.Random(tmpH)-(tmpH div 2));
      if YMandatory then
         AddXY(tmpX,tmpY{$IFNDEF D4},'', clTeeColor{$ENDIF})
      else
         AddXY(tmpY,tmpX{$IFNDEF D4},'', clTeeColor{$ENDIF});
      tmpX:=tmpX+StepX;
    end;
  end;
end;

{ Sorts all points in the series if the Order property is used.
  By default, Series.XValues.Order is set to "ascending". }
Procedure TChartSeries.CheckOrder;
begin
  if MandatoryValueList.Order<>loNone then
  begin
    MandatoryValueList.Sort;
    NotMandatoryValueList.FillSequence;
    Repaint;
  end;
end;

{ Adds random point to the series. Used at design-time to
  show sample values. }
Procedure TChartSeries.FillSampleValues(NumValues:Integer);
Begin
  if NumValues<=0 then Raise ChartException.Create(TeeMsg_FillSample);
  Clear;
  BeginUpdate;
  Randomize;
  AddSampleValues(NumValues);
  CheckOrder;
  EndUpdate;
End;

procedure TChartSeries.Assign(Source:TPersistent);

  Procedure SelfSetDataSources(Value:TList);
  var t : Integer;
  begin
    FDataSources.Clear;
    for t:=0 to Value.Count-1 do InternalAddDataSource(TComponent(Value[t]));
  end;

  Procedure AssignColors;
  var t : Integer;
  begin
    With TChartSeries(Source) do
    if Assigned(FColors) then
    begin
      if Assigned(Self.FColors) then Self.FColors.Clear
                                else Self.FColors:=TList.Create;
      for t:=0 to FColors.Count-1 do Self.FColors.Add(FColors[t]);
    end
    else Self.FColors.Free;
  end;

var t : Integer;
Begin
  if Source is TChartSeries then
  begin
    inherited;
    With TChartSeries(Source) do
    begin
      Self.DataSource  := nil;
      if Self.YMandatory and YMandatory then
     begin
        Self.FX.Assign(FX);
        Self.FY.Assign(FY);
      end
      else
      begin
        Self.FY.Assign(FX);
        Self.FX.Assign(FY);
      end;
      Self.FLabelsSource:=FLabelsSource;
      Self.FColorSource :=FColorSource;

      { other lists }
      for t:=2 to MinLong(Self.ValuesList.Count-1,ValuesList.Count-1) do
          Self.ValuesList.ValueList[t].Assign(ValuesList[t]);

      if Assigned(FunctionType) and (Self.FunctionType=nil) then
         FunctionType.ParentSeries:=Self;

      if DataSource=nil then
      begin
        if not (csDesigning in ComponentState) then
        begin
          AssignColors;
          Self.FXLabels.Clear;
          for t:=0 to FXLabels.Count-1 do Self.InsertLabel(t,XLabel[t]);
          Self.AssignValues(TChartSeries(Source));
        end;
      end
      else SelfSetDataSources(DataSources); { <-- important !!! }

      Self.Marks           := FMarks;
      Self.FColor          := FColor;
      Self.FColorEachPoint := FColorEachPoint;
      Self.FValueFormat    := FValueFormat;
      Self.FPercentFormat  := FPercentFormat;
      Self.FHorizAxis      := FHorizAxis;
      Self.FVertAxis       := FVertAxis;
      Self.FRecalcOptions  := FRecalcOptions;
      Self.FCursor         := FCursor;
      Self.FDepth          := FDepth;

      Self.CheckDataSource;
    end;
  end
  else Inherited;
end;

Procedure TChartSeries.SetColorEachPoint(Value:Boolean);
var t : Integer;
Begin
  SetBooleanProperty(FColorEachPoint,Value);
  if not FColorEachPoint then
  begin
    for t:=0 to Count-1 do if IsNull(t) then exit;
    FreeAndNil(FColors);
  end;
end;

{ This method replaces a value list with a new one. }
Procedure TChartSeries.ReplaceList(OldList,NewList:TChartValueList);
var t : Integer;
begin
  t:=FValuesList.IndexOf(NewList);
  if t<>-1 then FValuesList.Delete(t);
  t:=FValuesList.IndexOf(OldList);
  if t<>-1 then
  begin
    NewList.Name:=OldList.Name;
    if OldList=FX then FX:=NewList else
    if OldList=FY then FY:=NewList;
    FValuesList[t].Free;
    FValuesList.Items[t]:=NewList;
  end;
end;

{ Returns an X value. }
Function TChartSeries.GetxValue(ValueIndex:Integer):Double; { "x" in lowercase for BCB }
Begin
  result:=FX.Value[ValueIndex]
end;

{ Returns an Y value. }
Function TChartSeries.GetyValue(ValueIndex:Integer):Double; { "y" in lowercase for BCB }
Begin
  result:=FY.Value[ValueIndex]
end;

{ Changes an Y value. Tells the chart to repaint. }
Procedure TChartSeries.SetYValue(Index:Integer; Const Value:Double);
Begin
  FY.Value[Index]:=Value;
  {$IFDEF TEEARRAY}
  FY.Modified:=True;
  {$ENDIF}
  Repaint;
end;

{ Changes an X value. Tells the chart to repaint. }
Procedure TChartSeries.SetXValue(Index:Integer; Const Value:Double);
Begin
  FX.Value[Index]:=Value;
  {$IFDEF TEEARRAY}
  FX.Modified:=True;
  {$ENDIF}
  Repaint;
end;

{ Returns the label string for the corresponding point (ValueIndex).
  Returns an empty string is no labels exist. }
Function TChartSeries.GetXLabel(ValueIndex:Integer):String;
Begin
  With FXLabels do
  if (Count<=ValueIndex) or (List^[ValueIndex]=nil) then
     result:=''
  else
     result:=PString(List^[ValueIndex])^;
end;

{ Replaces a Label into the labels list.
  Allocates the memory for the AXLabel string parameter. }
Procedure TChartSeries.SetXLabel(ValueIndex:Integer; Const AXLabel:String);
var P : PString;
Begin
  if Assigned(FXLabels.List^[ValueIndex]) then FreeXLabel(ValueIndex);

  if AXLabel='' then FXLabels.List^[ValueIndex]:=nil
  else
  begin
    New(P);
    P^:=AXLabel;
    FXLabels.List^[ValueIndex]:=P;
  end;

  Repaint;
end;

{ Returns the color for a given "ValueIndex" point.
  If the "ColorEachPoint" property is used (True) then
  returns a color from the color palette. }
Function TChartSeries.GetValueColor(ValueIndex:Integer):TColor;
Begin
  if Assigned(FColors) and (FColors.Count>ValueIndex) then
  begin
    result:=TColor(FColors[ValueIndex]);
    if result=clTeeColor then
       if FColorEachPoint then result:=GetDefaultColor(ValueIndex)
                          else result:=FColor;
  end
  else
  if FColorEachPoint then result:=GetDefaultColor(ValueIndex)
                     else result:=FColor;
end;

{ Creates the color list and adds the default color (clTeeColor)
  for all the points in the series. }
Procedure TChartSeries.GrowColors;
var t : Integer;
begin
  FColors:=TList.Create;
  for t:=0 to Count-1 do FColors.Add(Pointer(clTeeColor));
end;

{ Sets the AColor parameter into the color list.
  It is optimized to prevent allocating memory when all the
  colors are the default (clTeeColor). }
Procedure TChartSeries.SetValueColor(ValueIndex:Integer; AColor:TColor);
Begin
  if not Assigned(FColors) then
     if AColor=clTeeColor then Exit
                          else GrowColors;
  FColors[ValueIndex]:=Pointer(AColor);
  Repaint;
end;

{ Frees the memory ocupied by a Label. }
Procedure TChartSeries.FreeXLabel(ValueIndex:Integer);
Var tmpText : PChar;
Begin
  tmpText:=FXLabels[ValueIndex];
  if Assigned(tmpText) then FreeMem(tmpText,StrLen(tmpText)+1);
end;

{ Clears (empties) all lists of values, colors and labels. }
Procedure TChartSeries.ClearLists;
Var t : Integer;
Begin
  for t:=0 to FValuesList.Count-1 do FValuesList[t].ClearValues;
  FreeAndNil(FColors);
  for t:=0 to FXLabels.Count-1 do FreeXLabel(t);
  FXLabels.Clear;
  FXLabels.Capacity:=TeeDefaultCapacity;
  FMarks.Positions.Clear;
end;

{ Removes all points.
  Triggers all events and notifications and repaints. }
Procedure TChartSeries.Clear;
begin
  ClearLists;
  if Assigned(FOnClearValues) then FOnClearValues(Self);
  NotifyValue(veClear,0);
  if FActive and Assigned(FParent) and
    (not (csDestroying in FParent.ComponentState)) then Repaint;
end;

{ Returns True is the series is associated to Axis parameter.
  Associated means either the horizontal or vertical axis of the Legend
  includes the Axis parameter. }
Function TChartSeries.AssociatedToAxis(Axis:TChartAxis):Boolean;
Begin
  result:=UseAxis and (
         (Axis.Horizontal and ((FHorizAxis=aBothHorizAxis) or (GetHorizAxis=Axis))) or
         ((not Axis.Horizontal) and ((FVertAxis=aBothVertAxis) or (GetVertAxis=Axis)))
        );
end;

{ Draws a rectangle at the Legend that corresponds to the ValueIndex
  point position. If the point color is the same as the Legend
  background color (color conflict),
  then an alternative color is used. }
Procedure TChartSeries.DrawLegendShape(ValueIndex:Integer; Const Rect:TRect);
var OldColor : TColor;
begin
  With FParent,Canvas do
  begin
    DoRectangle(Rect);  { <-- rectangle }
    if Brush.Color=LegendColor then  { <-- color conflict ! }
    Begin
      OldColor:=Pen.Color;
      if Brush.Color=clBlack then Pen.Color:=clWhite;
      Brush.Style:=bsClear;
      DoRectangle(Rect);  { <-- frame rectangle }
      Pen.Color:=OldColor;
    end;
  end;
end;

{ Virtual. Returns the color of the corresponding position in
  the Legend. By default the color the the same as the point color.
  Other series (Surface, etc) return a color from the "palette". }
Function TChartSeries.LegendItemColor(LegendIndex:Integer):TColor;
begin
  result:=ValueColor[LegendIndex];
end;

{ For a given ValueIndex point in the series, paints it at the
  Legend. }
Procedure TChartSeries.DrawLegend(ValueIndex:Integer; Const Rect:TRect);
Var tmpBack  : TColor;
    tmpStyle : TBrushStyle;
Begin
  if (ValueIndex<>-1) or (not ColorEachPoint) then
  begin
    FParent.Canvas.AssignVisiblePen(Pen);
    with FParent.Canvas.Brush do
    if ValueIndex=-1 then Color:=SeriesColor
                     else Color:=LegendItemColor(ValueIndex);
    if FParent.Canvas.Brush.Color<>clNone then
    begin
      tmpBack:=FParent.LegendColor;
      tmpStyle:=bsSolid;
      PrepareLegendCanvas(ValueIndex,tmpBack,tmpStyle);
//      FParent.Canvas.Brush.Style:=tmpStyle;
      if tmpBack=clTeeColor then
      begin
        tmpBack:=FParent.LegendColor;
        if tmpBack=clTeeColor then tmpBack:=FParent.Color;
      end;
      With FParent do SetBrushCanvas(Canvas.Brush.Color,Self.Brush,tmpBack);
      DrawLegendShape(ValueIndex,Rect);
    end;
  end;
End;

Procedure TChartSeries.PrepareLegendCanvas( ValueIndex:Integer; Var BackColor:TColor;
                                            Var BrushStyle:TBrushStyle);
begin { abstract }
end;

{ Calculates the correct "Z" order for the series }
Procedure TChartSeries.CalcZOrder;
Begin
  if FZOrder=TeeAutoZOrder then
  begin
    if FParent.View3D then
    begin
      Inc(FParent.FMaxZOrder);
      IZOrder:=FParent.FMaxZOrder;
    end
    else IZOrder:=0;
  end
  else FParent.FMaxZOrder:=MaxLong(FParent.FMaxZOrder,ZOrder);
End;

Function TChartSeries.CalcXPosValue(Const Value:Double):Integer;
Begin
  result:=GetHorizAxis.CalcXPosValue(Value);
end;

{ Returns the axis value for a given horizontal ScreenPos pixel position }
Function TChartSeries.XScreenToValue(ScreenPos:Integer):Double;
Begin
  result:=GetHorizAxis.CalcPosPoint(ScreenPos);
end;

Function TChartSeries.CalcXSizeValue(Const Value:Double):Integer;
Begin
  result:=GetHorizAxis.CalcSizeValue(Value);
end;

Function TChartSeries.CalcYPosValue(Const Value:Double):Integer;
Begin
  result:=GetVertAxis.CalcYPosValue(Value);
end;

Function TChartSeries.CalcPosValue(Const Value:Double):Integer;
Begin
  if YMandatory then result:=CalcYPosValue(Value)
                else result:=CalcXPosValue(Value);
end;

Function TChartSeries.XValueToText(Const AValue:Double):String;
Begin
  result:=GetHorizAxis.LabelValue(AValue);
end;

Function TChartSeries.YValueToText(Const AValue:Double):String;
begin
  result:=GetVertAxis.LabelValue(AValue);
end;

{ Returns the axis value for a given vertical ScreenPos pixel position }
Function TChartSeries.YScreenToValue(ScreenPos:Integer):Double;
Begin
  result:=GetVertAxis.CalcPosPoint(ScreenPos);
end;

Function TChartSeries.CalcYSizeValue(Const Value:Double):Integer;
Begin
  result:=GetVertAxis.CalcSizeValue(Value);
end;

Function TChartSeries.CalcXPos(ValueIndex:Integer):Integer;
Begin
  result:=GetHorizAxis.CalcXPosValue(XValues.Value[ValueIndex]);
end;

Function TChartSeries.CalcYPos(ValueIndex:Integer):Integer;
begin
  result:=GetVertAxis.CalcYPosValue(YValues.Value[ValueIndex]);
end;

{ Removes a point in the series }
Procedure TChartSeries.Delete(ValueIndex:Integer);
Var t : Integer;
Begin
  if ValueIndex<Count then
  begin
    for t:=0 to FValuesList.Count-1 do FValuesList[t].Delete(ValueIndex);
    if Assigned(FColors) then With FColors do if Count>ValueIndex then Delete(ValueIndex);
    if FXLabels.Count>ValueIndex then
    Begin
      FreeXLabel(ValueIndex);
      FXLabels.Delete(ValueIndex);
    end;
    With FMarks.FPositions do if Count>ValueIndex then Delete(ValueIndex);
    NotifyValue(veDelete,ValueIndex);
    if FActive then Repaint;
  end
  else Raise ChartException.CreateFmt(TeeMsg_SeriesDelete,[ValueIndex,Count-1]);
end;

{ Exchanges one point with another.
  Also the point color and point label. }
procedure TChartSeries.SwapValueIndex(a,b:Integer);
var t : Integer;
begin
  for t:=0 to FValuesList.Count-1 do
      FValuesList[t].{$IFNDEF TEEARRAY}FList.{$ENDIF}Exchange(a,b);
  if Assigned(FColors) then FColors.Exchange(a,b);
  FXLabels.Exchange(a,b);
end;

procedure TChartSeries.SetMarks(Value:TSeriesMarks);
begin
  FMarks.Assign(Value);
end;

Procedure TChartSeries.SetSeriesColor(AColor:TColor);
Begin
  SetColorProperty(FColor,AColor);
  if Assigned(ParentChart) then ParentChart.BroadcastSeriesEvent(Self,seChangeColor);
  RepaintDesigner;
end;

Function TChartSeries.MaxXValue:Double;
Begin
  result:=FX.MaxValue;
end;

Function TChartSeries.MaxYValue:Double;
Begin
  result:=FY.MaxValue;
end;

Function TChartSeries.MinXValue:Double;
Begin
  result:=FX.MinValue;
end;

Function TChartSeries.MinYValue:Double;
Begin
  result:=FY.MinValue;
end;

Function TChartSeries.MaxZValue:Double;
begin
  result:=ZOrder;
end;

Function TChartSeries.MinZValue:Double;
begin
  result:=ZOrder;
end;

Function TChartSeries.MaxMarkWidth:Integer;
var t : Integer;
Begin
  result:=0;
  for t:=0 to Count-1 do
      result:=MaxLong(result,ParentChart.Canvas.TextWidth(GetMarkText(t)));
end;

Procedure TChartSeries.AddLinkedSeries(ASeries:TChartSeries);
Begin
  if FLinkedSeries.IndexOf(ASeries)=-1 then FLinkedSeries.Add(ASeries);
end;

Procedure TChartSeries.RemoveDataSource(Value:TComponent);
begin
  FDataSources.Remove(Value);
  CheckDataSource;
end;

Procedure TChartSeries.RemoveLinkedSeries(ASeries:TChartSeries);
Begin
  if Assigned(FLinkedSeries) then FLinkedSeries.Remove(ASeries);
end;

Procedure TChartSeries.BeginUpdate;
begin
  Inc(IUpdating);
end;

Procedure TChartSeries.EndUpdate;
begin
  if IUpdating>0 then
  begin
    Dec(IUpdating);
    if IUpdating=0 then RefreshSeries;
  end;
end;

{ This method is called whenever the series points are added,
  deleted, modified, etc. }
Procedure TChartSeries.NotifyValue(ValueEvent:TValueEvent; ValueIndex:Integer);
var t : Integer;
Begin
  if IUpdating=0 then
    for t:=0 to FLinkedSeries.Count-1 do
    With TChartSeries(FLinkedSeries[t]) do
    Case ValueEvent of
      veClear  : if rOnClear in FRecalcOptions then Clear;
      veDelete : if rOnDelete in FRecalcOptions then
                    if FunctionType=nil then DeletedValue(Self,ValueIndex);
      veAdd    : if rOnInsert in FRecalcOptions then
                    if FunctionType=nil then AddedValue(Self,ValueIndex);
      veModify : if rOnModify in FRecalcOptions then AddValues(Self);
      veRefresh: AddValues(Self);
    end;
end;

Function TChartSeries.UseAxis:Boolean;
begin
  result:=True;
end;

Procedure TChartSeries.Loaded;
var t : Integer;
begin
  inherited;
  if Assigned(FTempDataSources) then
  begin
    if FTempDataSources.Count>0 then
    begin
      FDataSources.Clear;
      for t:=0 to FTempDataSources.Count-1 do
          InternalAddDataSource( Owner.FindComponent(FTempDataSources[t]) );
    end;
    FTempDataSources.Free;
  end;
  CheckDatasource;
end;

Procedure TChartSeries.SetCustomHorizAxis(Value:TChartAxis);
begin
  FCustomHorizAxis:=Value;
  if Assigned(Value) then FHorizAxis:=aCustomHorizAxis
                     else FHorizAxis:=aBottomAxis;
  RecalcGetAxis;
  Repaint;
end;

Function TChartSeries.GetZOrder:Integer;
begin
  if FZOrder=TeeAutoZOrder then result:=IZOrder
                           else result:=FZOrder;
end;

Procedure TChartSeries.SetZOrder(Value:Integer);
begin
  SetIntegerProperty(FZOrder,Value);
  if FZOrder=TeeAutoZOrder then IZOrder:=0 else IZOrder:=FZOrder;
end;

Procedure TChartSeries.SetCustomVertAxis(Value:TChartAxis);
begin
  FCustomVertAxis:=Value;
  if Assigned(Value) then FVertAxis:=aCustomVertAxis
                     else FVertAxis:=aLeftAxis;
  RecalcGetAxis;
  Repaint;
end;

class procedure TChartSeries.CreateSubGallery(AddSubChart: TChartSubGalleryProc);
begin
  AddSubChart(TeeMsg_Normal);
end;

class procedure TChartSeries.SetSubGallery(ASeries: TChartSeries;
  Index: Integer);
begin
end;

procedure TChartSeries.SetDepth(const Value: Integer);
begin
  SetIntegerProperty(FDepth,Value);
end;

{ Tells the series to swap X and Y values }
procedure TChartSeries.SetHorizontal;
begin
  MandatoryValueList:=XValues;
  NotMandatoryValueList:=YValues;
  YMandatory:=False;
end;

{ TChartSeriesList }
procedure TChartSeriesList.Put(Index:Integer; Value:TChartSeries);
begin
  inherited Items[Index]:=Value;
end;

function TChartSeriesList.Get(Index:Integer):TChartSeries;
begin
  result:=TChartSeries(List^[Index]);
end;

{ TChartAxes }
{$IFNDEF D4}
Destructor TChartAxes.Destroy;
{$ELSE}
procedure TChartAxes.Clear;
{$ENDIF}
begin
  While Count>0 do Items[0].Free;
  inherited;
end;

function TChartAxes.Get(Index:Integer):TChartAxis;
begin
  result:=TChartAxis(List^[Index]);
end;

{$IFNDEF D4}
{ TOwnedCollection }
constructor TOwnedCollection.Create(AOwner: TPersistent;
  ItemClass: TCollectionItemClass);
begin
  FOwner := AOwner;
  inherited Create(ItemClass);
end;

function TOwnedCollection.GetOwner: TPersistent;
begin
  Result := FOwner;
end;
{$ENDIF}

{ TChartCustomAxes }
function TChartCustomAxes.Get(Index: Integer):TChartAxis;
begin
  result:=TChartAxis(inherited Items[Index]);
end;

procedure TChartCustomAxes.Put(Index:Integer; Const Value:TChartAxis);
begin
  Items[Index].Assign(Value);
end;

{ TTeeCustomDesigner }
Procedure TTeeCustomDesigner.Refresh;
begin
end;

Procedure TTeeCustomDesigner.Repaint;
begin
end;

{ TTeeCustomTool }
procedure TTeeCustomTool.ChartEvent(AEvent: TChartToolEvent);
begin
end;

Procedure TTeeCustomTool.ChartMouseEvent(AEvent: TChartMouseEvent;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
end;

class function TTeeCustomTool.Description: String;
begin
  result:='';
end;

procedure TTeeCustomTool.SetParentChart(const Value: TCustomAxisPanel);
begin
  if Assigned(ParentChart) then
  begin
    ParentChart.Tools.Remove(Self);
    if not Assigned(Value) then ParentChart.Repaint;
  end;
  inherited;
  if Assigned(ParentChart) then TList(ParentChart.Tools).Add(Self);
end;

{ TChartTools }
Procedure TChartTools.Add(Tool:TTeeCustomTool);
begin
  Tool.ParentChart:=Owner;
end;

{$IFNDEF D4}
Destructor TChartTools.Destroy;
{$ELSE}
procedure TChartTools.Clear;
{$ENDIF}
begin
  While Count>0 do Items[0].Free;
  inherited;
end;

function TChartTools.Get(Index: Integer): TTeeCustomTool;
begin
  result:=TTeeCustomTool(List^[Index]);
end;

{ TCustomAxisPanel }
Constructor TCustomAxisPanel.Create(AOwner: TComponent);
begin
  inherited;
  AutoRepaint :=False;

  FTools:=TChartTools.Create;
  FTools.Owner:=Self;

  FAxisVisible:=True;
  FAxisBehind:=True;
  FSeriesList:=TChartSeriesList.Create;
  FSeriesList.FOwner:=Self;
  FView3DWalls:=True;

  FClipPoints:=True;

  { create FIRST the collections... }
  FAxes:=TChartAxes.Create;
  FCustomAxes:=TChartCustomAxes.Create(Self,TChartAxis);

  { Create axes... }
  FBottomAxis:=TChartAxis.Create(FCustomAxes);
  FBottomAxis.FHorizontal:=True;

  FTopAxis:=TChartAxis.Create(FCustomAxes);
  With FTopAxis do
  begin
    FHorizontal:=True;
    FOtherSide:=True;
  end;

  FLeftAxis:=TChartAxis.Create(FCustomAxes);
  With FLeftAxis.FAxisTitle do
  begin
    FAngle:=90;
    IDefaultAngle:=90;
  end;

  FRightAxis:=TChartAxis.Create(FCustomAxes);
  With FRightAxis do
  begin
    FAxisTitle.FAngle:=270;
    FAxisTitle.IDefaultAngle:=270;
    FOtherSide:=True;
  end;

  FDepthAxis := TChartDepthAxis.Create(FCustomAxes);
  With FDepthAxis do
  begin
    FVisible:=False;
    IsDepthAxis:=True;
    FOtherSide:=True;
  end;

  { Paging default values }
  FPage:=1;
  FScaleLastPage:=True;
  AutoRepaint :=True;
end;

Destructor TCustomAxisPanel.Destroy;
Begin
  FreeAllSeries;
  FAxes.Free;
  FCustomAxes.Free;
  FSeriesList.Free;
  Designer.Free;
  FTools.Free;
  inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TCustomAxisPanel.Load(var S: TOldStream; AOwner: TComponent);
begin
   inherited;

   FTools:=TChartTools.Create;
   FTools.Owner:=Self;

   S.Read(FAxisVisible,SizeOf(Boolean));
   S.Read(FAxisBehind,SizeOf(Boolean));

   FSeriesList:=TChartSeriesList.Create;
   FSeriesList.FOwner:=Self;

   S.Read(FView3DWalls,SizeOf(Boolean));
   S.Read(FClipPoints,SizeOf(Boolean));

   // create FIRST the collections...
   FAxes:=TChartAxes.Create;
   FCustomAxes:=TChartCustomAxes.Create(Self,TChartAxis);

   // Load axes...
   FBottomAxis:=TChartAxis.Load(S,FCustomAxes);
   FTopAxis:=TChartAxis.Load(S,FCustomAxes);
   FLeftAxis:=TChartAxis.Load(S,FCustomAxes);
   FRightAxis:=TChartAxis.Load(S,FCustomAxes);
   FDepthAxis := TChartDepthAxis.Load(S,FCustomAxes);

   // Load Paging values
   S.Read(FPage,SizeOf(Integer));
   S.Read(FMaxPointsPerPage,SizeOf(Integer));
   S.Read(FScaleLastPage,SizeOf(Boolean));

   S.Read(FMaxZOrder,SizeOf(Integer));
end;

Procedure TCustomAxisPanel.Store(var S: TOldStream);
begin
   inherited Store(S);
   S.Write(FAxisVisible,SizeOf(Boolean));
   S.Write(FAxisBehind,SizeOf(Boolean));
   S.Write(FView3DWalls,SizeOf(Boolean));
   S.Write(FClipPoints,SizeOf(Boolean));

   // Store axes...
   FBottomAxis.Store(S);
   FTopAxis.Store(S);
   FLeftAxis.Store(S);
   FRightAxis.Store(S);
   FDepthAxis.Store(S);

   // Store Paging values
   S.Write(FPage,SizeOf(Integer));
   S.Write(FMaxPointsPerPage,SizeOf(Integer));
   S.Write(FScaleLastPage,SizeOf(Boolean));

   S.Write(FMaxZOrder,SizeOf(Integer));
end;

Function TCustomAxisPanel.OnCompareItems(Item: TComponent): Boolean;
begin
   Result:=FALSE;
   if Item is TCustomAxisPanel then
      with TCustomAxisPanel(Item) do
         if Self.FAxisVisible = FAxisVisible then
            if Self.FAxisBehind = FAxisBehind then
               if Self.FView3DWalls = FView3DWalls then
                  if Self.FClipPoints = FClipPoints then

                     if Self.FBottomAxis.OnCompareItems(FBottomAxis) then
                        if Self.FTopAxis.OnCompareItems(FTopAxis) then
                           if Self.FLeftAxis.OnCompareItems(FLeftAxis) then
                              if Self.FRightAxis.OnCompareItems(FRightAxis) then
                                 if Self.FDepthAxis.OnCompareItems(FDepthAxis) then
                                    Result := inherited OnCompareItems(Item);
   // Without comparison Paging values
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

procedure TCustomAxisPanel.BroadcastTeeEventClass(Event: TTeeEventClass);
begin
  BroadcastTeeEvent(Event.Create).Free;
end;

{ Changes the current page }
Procedure TCustomAxisPanel.SetPage(Value:Integer);
var tmp : Integer;
Begin
  { Allow "Page" to be into: >= 1 , <= NumPages }
  tmp:=NumPages;
  if Value>tmp then Value:=tmp;
  if Value<1 then Value:=1;
  if FPage<>Value then
  begin
    SetIntegerProperty(FPage,Value);
    BroadcastTeeEventClass(TChartChangePage);
    { Trigged "changed page" event }
    if Assigned(FOnPageChange) then FOnPageChange(Self);
  end;
end;

Procedure TCustomAxisPanel.SetScaleLastPage(Value:Boolean);
Begin
  SetBooleanProperty(FScaleLastPage,Value);
End;

{ Returns the number of series that are Active (visible). }
Function TCustomAxisPanel.CountActiveSeries:Integer;
var t : Integer;
Begin
  result:=0;
  for t:=0 to SeriesCount-1 do if SeriesList[t].Active then Inc(result);
end;

{ Calculates the number of pages. This only applies
  when the MaxPointsPerPage property is used.
  If it is not used (is zero), then returns 1 (one page only).
}
Function TCustomAxisPanel.NumPages:Integer;

  Function CalcNumPages(AAxis:TChartAxis):Integer;
  var tmp       : Integer;
      t         : Integer;
      FirstTime : Boolean;
  Begin
    { By default, one single page }
    result:=1;
    { Calc max number of points for all active series
      associated to "AAxis" axis }
    tmp:=0;
    FirstTime:=True;
    for t:=0 to SeriesCount-1 do
    With Series[t] do
    if FActive and AssociatedToAxis(AAxis) then
       if FirstTime or (Count>tmp) then
       begin
         tmp:=Count;
         FirstTime:=False;
       end;
    { If there are points... divide into pages... }
    if tmp>0 then
    Begin
      result:=tmp div FMaxPointsPerPage;
      { extra page for remaining points... }
      if (tmp mod FMaxPointsPerPage)>0 then Inc(result);
    end;
  end;

Begin
  if (FMaxPointsPerPage>0) and (SeriesCount>0) then
  begin
    if Series[0].YMandatory then
       result:=MaxLong(CalcNumPages(FTopAxis),CalcNumPages(FBottomAxis))
    else
       result:=MaxLong(CalcNumPages(FLeftAxis),CalcNumPages(FRightAxis));
  end
  else result:=1;
End;

{ Sends pages to the printer. This only applies when the
  Chart has more than one page (MaxPointsPerPage property).
  If no parameters are passed, all pages are printed.
  If only one page exists, then it's printed. }
procedure TCustomAxisPanel.PrintPages(FromPage, ToPage: Integer);
var tmpOld : Integer;
    t      : Integer;
    R      : TRect;
begin
  Printer.Title:=Name;
  Printer.BeginDoc;
  try
    if ToPage=0 then ToPage:=NumPages;
    if FromPage=0 then FromPage:=1;
    tmpOld:=Page;
    try
      R:=ChartPrintRect;
      for t:=FromPage to ToPage do
      begin
        Page:=t;
        PrintPartial(R);
        if t<ToPage then Printer.NewPage;
      end;
    finally
      Page:=tmpOld;
    end;
    Printer.EndDoc;
  except
    on Exception do
    begin
      Printer.Abort;
      if Printer.Printing then Printer.EndDoc;
      Raise;
    end;
  end;
end;

Procedure TCustomAxisPanel.SetMaxPointsPerPage(Value:Integer);
var tmp : Integer;
Begin
  if Value<0 then Raise ChartException.Create(TeeMsg_MaxPointsPerPage)
             else
             begin
               SetIntegerProperty(FMaxPointsPerPage,Value);
               tmp:=NumPages;
               if FPage>tmp then Page:=tmp;
               BroadcastTeeEventClass(TChartChangePage);
             end;
end;

{ Destroys and removes all Series in Chart. The optional parameter
  defines a series class (for example: TLineSeries) to remove only
  the series of that class. }
Procedure TCustomAxisPanel.FreeAllSeries{$IFDEF D4}( SeriesClass:TChartSeriesClass=nil ){$ENDIF};
var t   : Integer;
    tmp : TCustomChartSeries;
begin
  t:=0;
  While t<SeriesCount do
  begin
    tmp:=Series[t];
    {$IFDEF D4}
    if (not Assigned(SeriesClass)) or (tmp is SeriesClass) then
    begin
      tmp.ParentChart:=nil;
      tmp.Free;
    end
    else Inc(t);
    {$ELSE}
    tmp.ParentChart:=nil;
    tmp.Free;
    {$ENDIF}
  end;
end;

{ Steps to determine if an axis is Visible:

  1) The global Chart.AxisVisible property is True... and...
  2) The Axis Visible property is True... and...
  3) At least there is a Series Active and associated to the axis and
     the Series has the "UseAxis" property True.
}
Function TCustomAxisPanel.IsAxisVisible(Axis:TChartAxis):Boolean;
var t : Integer;
Begin
  result:=FAxisVisible and Axis.FVisible;
  if result then { if still visible... }
     if Axis.IsDepthAxis then result:=View3D
     else
     for t:=0 to SeriesCount-1 do
       With Series[t] do
       if FActive then
         if UseAxis then
         begin
           result:=AssociatedToAxis(Axis);
           if result then exit;
         end
         else
         begin
           result:=False;
           exit;
         end;
end;

{ This function returns the longest width in pixels for a given "S" string.
  It also considers multi-line strings, returning in the NumLines
  parameter the count of lines. }
Function TCustomAxisPanel.MultiLineTextWidth(S:String; Var NumLines:Integer):Integer;

  Procedure CalcString(Const St:String);
  begin
    result:=MaxLong(result,Canvas.TextWidth(St));
    Inc(NumLines);
  end;

var i : Integer;
begin
  result:=0;
  NumLines:=0;
  i:=AnsiPos(TeeLineSeparator,s);
  While i>0 do
  begin
    CalcString(Copy(s,1,i-1));
    Delete(s,1,i);
    i:=AnsiPos(TeeLineSeparator,s);
  end;
  if s<>'' then CalcString(s);
end;

{ Returns if no series are active (visible) and associated to
  the Axis parameter }
Function TCustomAxisPanel.NoActiveSeries(AAxis:TChartAxis):Boolean;
var t : Integer;
begin
  for t:=0 to SeriesCount-1 do
  With Series[t] do
  if Active and AssociatedToAxis(AAxis) then
  begin
    result:=False;
    Exit;
  end;
  result:=True;
end;

{ Calculates the minimum or maximum for the Axis parameter.
  This function is internally used. }
Function TCustomAxisPanel.InternalMinMax(AAxis:TChartAxis; IsMin,IsX:Boolean):Double;
var t             : Integer;
    tmpCount      : Integer;
    FirstTime     : Boolean;
    tmpPagingAxis : Boolean;
    tmp           : Double;
    tmpSeries     : TChartSeries;
    tmpFirstPoint : Integer;
    tmpLastPoint  : Integer;
    tmpNumPoints  : Integer;
    tt            : Integer;
Begin
  if AAxis.IsDepthAxis then
  begin
    if AAxis.CalcLabelStyle=talValue then
    begin
      result:=0;
      FirstTime:=True;
      for tt:=0 to SeriesCount-1 do
      With Series[tt] do
      if Active then
      begin
        if IsMin then tmp:=MinZValue else tmp:=MaxZValue;
        if FirstTime or
          ( IsMin and (tmp<result) ) or
          ( (Not IsMin) and (tmp>result) ) then
        begin
          FirstTime:=False;
          result:=tmp;
        end;
      end;
    end
    else
    if IsMin then result:=-0.5 else result:=MaxZOrder+0.5;
  end
  else
  begin
    result:=0;
    tmpSeries:=GetAxisSeries(AAxis);
    if Assigned(tmpSeries) then
    begin
      if tmpSeries.YMandatory then tmpPagingAxis:=IsX
                              else tmpPagingAxis:=not IsX;
    end
    else tmpPagingAxis:=IsX;
    if (FMaxPointsPerPage>0) and tmpPagingAxis then
    Begin
      if Assigned(tmpSeries) and (tmpSeries.Count>0) then
      Begin
        tmpFirstPoint:=(FPage-1)*FMaxPointsPerPage;
        tmpCount:=tmpSeries.Count;
        if tmpCount<=tmpFirstPoint then
           tmpFirstPoint:=MaxLong(0,(tmpCount div FMaxPointsPerPage)-1)*FMaxPointsPerPage;
        tmpLastPoint:=tmpFirstPoint+FMaxPointsPerPage-1;
        if tmpCount<=tmpLastPoint then
           tmpLastPoint:=tmpFirstPoint+(tmpCount mod FMaxPointsPerPage)-1;
        if IsMin then
           if IsX then result:=tmpSeries.XValues.Value[tmpFirstPoint]
                  else result:=tmpSeries.YValues.Value[tmpFirstPoint]
        else
        Begin
          if IsX then result:=tmpSeries.XValues.Value[tmpLastPoint]
                 else result:=tmpSeries.YValues.Value[tmpLastPoint];
          if not FScaleLastPage then
          begin
            tmpNumPoints:=tmpLastPoint-tmpFirstPoint+1;
            if tmpNumPoints<FMaxPointsPerPage then
            begin
              if IsX then tmp:=tmpSeries.XValues.Value[tmpFirstPoint]
                     else tmp:=tmpSeries.YValues.Value[tmpFirstPoint];
              result:=tmp+FMaxPointsPerPage*(result-tmp)/tmpNumPoints;
            end;
          end;
        end;
      end;
    end
    else
    begin
      FirstTime:=True;
      for t:=0 to SeriesCount-1 do
      With Series[t] do
      if (FActive or NoActiveSeries(AAxis)) and (Count>0) then
      Begin
        if (      IsX  and ((HorizAxis=aBothHorizAxis) or (GetHorizAxis=AAxis)) ) or
           ( (Not IsX) and ((VertAxis=aBothVertAxis) or (GetVertAxis =AAxis)) )  then
        Begin
          if IsMin then
             if IsX then tmp:=MinXValue else tmp:=MinYValue
          else
             if IsX then tmp:=MaxXValue else tmp:=MaxYValue;
          if FirstTime or
           ( IsMin and (tmp<result) ) or
           ( (Not IsMin) and (tmp>result) ) then
          Begin
            result:=tmp;
            FirstTime:=False;
          end;
        end;
      end;
    end;
  end;
end;

Function TCustomAxisPanel.MaxXValue(AAxis:TChartAxis):Double;
Begin
  result:=InternalMinMax(AAxis,False,True);
end;

Function TCustomAxisPanel.MaxYValue(AAxis:TChartAxis):Double;
Begin
  result:=InternalMinMax(AAxis,False,False);
end;

Function TCustomAxisPanel.MinXValue(AAxis:TChartAxis):Double;
Begin
  result:=InternalMinMax(AAxis,True,True);
end;

Function TCustomAxisPanel.MinYValue(AAxis:TChartAxis):Double;
Begin
  result:=InternalMinMax(AAxis,True,False);
end;

{ When the Legend style is "series", returns the series that
  corresponds to the Legend "ItemIndex" position.
  The "OnlyActive" parameter, when False takes into account all series,
  regardless if they are visible (active) or not. }
Function TCustomAxisPanel.SeriesLegend(ItemIndex:Integer; OnlyActive:Boolean):TChartSeries;
var t   : Integer;
    tmp : Integer;
begin
  tmp:=0;
  for t:=0 to SeriesCount-1 do
  With Series[t] do
  if ShowInLegend and ((not OnlyActive) or Active) then
     if tmp=ItemIndex then
     begin
       result:=Series[t];
       exit;
     end
     else Inc(tmp);
  result:=nil;
end;

{ Returns the Active series (visible) that corresponds to the
  ItemIndex position in the Legend }
Function TCustomAxisPanel.ActiveSeriesLegend(ItemIndex:Integer):TChartSeries;
begin
  result:=SeriesLegend(ItemIndex,True);
end;

Function TCustomAxisPanel.SeriesTitleLegend(SeriesIndex:Integer; OnlyActive:Boolean):String;
var tmpSeries : TChartSeries;
Begin
  if OnlyActive then tmpSeries:=ActiveSeriesLegend(SeriesIndex)
                else tmpSeries:=SeriesLegend(SeriesIndex,False);
  if Assigned(tmpSeries) then result:=SeriesTitleOrName(tmpSeries)
                         else result:='';
end;

{ Returns the width in pixels for the longest "Label" of all series,
  regardless if the series is active (visible) or not. }
Function TCustomAxisPanel.MaxTextWidth:Integer;
var t  : Integer;
    tt : Integer;
    tmp: Integer;
Begin
  result:=0;
  for t:=0 to SeriesCount-1 do
  With Series[t] do
  if FXLabels.Count>0 then
     for tt:=0 to Count-1 do
         result:=MaxLong(result,MultiLineTextWidth(XLabel[tt],tmp));
end;

{ Returns the longest width in pixels for all the active (visible)
  series marks. }
Function TCustomAxisPanel.MaxMarkWidth:Integer;
var t : Integer;
Begin
  result:=0;
  for t:=0 to SeriesCount-1 do
  With Series[t] do if FActive then result:=MaxLong(result,MaxMarkWidth);
end;

Function TCustomAxisPanel.GetSeries(Index:Integer):TChartSeries;
Begin
  result:=FSeriesList.List^[Index];
end;

{ Calculates internal parameters for the series Width and Height
  in 3D mode. }
Procedure TCustomAxisPanel.CalcSize3DWalls;
var tmpNumSeries : Integer;
    tmp          : Double;
    tmpSin       : Extended;
    tmpCos       : Extended;
Begin
  if View3D then
  Begin
    tmp:=0.001*Chart3DPercent;
    if not View3DOptions.Orthogonal then tmp:=tmp*2;
    FSeriesWidth3D :=Round(tmp*ChartWidth);
    if View3DOptions.Orthogonal then
    begin
      SinCos(View3DOptions.OrthoAngle*TeePiStep,tmpSin,tmpCos);
      tmp:=tmpSin/tmpCos;
    end
    else tmp:=1;
    if tmp>1 then FSeriesWidth3D:=Round(FSeriesWidth3D/tmp);
    FSeriesHeight3D:=Round(FSeriesWidth3D*tmp);
    if ApplyZOrder then tmpNumSeries:=MaxLong(1,MaxZOrder+1)
                   else tmpNumSeries:=1;
    Height3D:=FSeriesHeight3D * tmpNumSeries;
    Width3D :=FSeriesWidth3D  * tmpNumSeries;
  end
  else
  begin
    FSeriesWidth3D :=0;
    FSeriesHeight3D:=0;
    Width3D        :=0;
    Height3D       :=0;
  end;
end;

{ Returns the number of series in the chart }
Function TCustomAxisPanel.SeriesCount:Integer;
begin
  result:=FSeriesList.Count;
end;

{ Triggers an event for all "tools" }
Procedure TCustomAxisPanel.BroadcastToolEvent(AEvent:TChartToolEvent);
var t : Integer;
begin
  for t:=0 to Tools.Count-1 do
  With Tools[t] do
  if Active then ChartEvent(AEvent);
end;

{ Main drawing method. Draws everything. }
Procedure TCustomAxisPanel.InternalDraw(Const UserRectangle:TRect);

  Procedure DrawSeries(TheSeries:TChartSeries);
  Var ActiveRegion : Boolean;

    Procedure ClipRegionCreate;
    var tmpR : TRect;
    Begin
      if CanClip then
      begin
        tmpR:=ChartRect;
        Inc(tmpR.Bottom);
        Canvas.ClipCube(tmpR,0,Width3D);
        ActiveRegion:=True;
      end;
    end;

    Procedure ClipRegionDone;
    Begin
      if ActiveRegion then
      begin
        Canvas.UnClipRectangle;
        ActiveRegion:=False;
      end;
    end;

    Procedure DrawAllSeriesValue(ValueIndex:Integer);

      Procedure TryDrawSeries(ASeries:TChartSeries);
      begin
        With ASeries do
         if FActive and (ZOrder=TheSeries.ZOrder) and (ValueIndex<Count) then
            DrawValue(ValueIndex)
      end;

    var t    : Integer;
        tmp1 : Integer;
        tmp2 : Integer;
    Begin
      tmp1:=SeriesList.IndexOf(TheSeries);
      tmp2:=SeriesCount-1;
      if ValueIndex<TheSeries.Count then
      begin
        if TheSeries.DrawSeriesForward(ValueIndex) then
           for t:=tmp1 to tmp2 do TryDrawSeries(Series[t])
        else
           for t:=tmp2 downto tmp1 do TryDrawSeries(Series[t])
      end
      else for t:=tmp1 to tmp2 do TryDrawSeries(Series[t])
    end;

    Procedure DrawMarksSeries(ASeries:TChartSeries);
    begin
      if ASeries.Count>0 then
      With ASeries,FMarks do
      if Visible then
      Begin
        if FClip then ClipRegionCreate;
        DrawMarks;
        if FClip then ClipRegionDone;
      end;
    end;

  Var t        : Integer;
      tmpFirst : Integer;
      tmpLast  : Integer;
  Begin
    ActiveRegion:=False;  { <-- VERY IMPORTANT !!!! }
    With TheSeries do
    if View3D and MoreSameZOrder then
    Begin
      if FirstInZOrder then
      Begin
        ActiveRegion:=False;
        tmpFirst:=-1;
        tmpLast :=-1;
        for t:=SeriesList.IndexOf(TheSeries) to SeriesCount-1 do
        With Series[t] do
        if Active and (ZOrder=TheSeries.ZOrder) then
        Begin
          CalcFirstLastVisibleIndex;
          if FFirstVisibleIndex<>-1 then
          Begin
            if tmpFirst=-1 then tmpFirst:=FFirstVisibleIndex
                           else tmpFirst:=MaxLong(tmpFirst,FFirstVisibleIndex);
            if tmpLast=-1 then tmpLast:=FLastVisibleIndex
                          else tmpLast:=MaxLong(tmpLast,FLastVisibleIndex);
            DoBeforeDrawValues;
            if FClipPoints and (not ActiveRegion) then ClipRegionCreate;
          end;
        end;
        { values }
        if tmpFirst<>-1 then
          if DrawValuesForward then
             for t:=tmpFirst to tmpLast do DrawAllSeriesValue(t)
          else
             for t:=tmpLast downto tmpFirst do DrawAllSeriesValue(t);

        { Region }
        ClipRegionDone;

        { Marks and DoAfterDrawValues }
        for t:=SeriesList.IndexOf(TheSeries) to SeriesCount-1 do
        With Series[t] do
        if Active and (ZOrder=TheSeries.ZOrder) and (FFirstVisibleIndex<>-1) then
        begin
          DrawMarksSeries(Series[t]);
          DoAfterDrawValues;
        end;
      end;
    end
    else
    begin
      CalcFirstLastVisibleIndex;
      if FFirstVisibleIndex<>-1 then
      begin
        DoBeforeDrawValues;
        if UseAxis and FClipPoints then ClipRegionCreate;
        DrawAllValues;
        ClipRegionDone;
        DrawMarksSeries(TheSeries);
        DoAfterDrawValues;
      end;
    end;
  end;

  Procedure SetSeriesZOrder;
  Var tmpSeries : Integer;
  begin
    FMaxZOrder:=0;
    if ApplyZOrder then
       if View3D then
       Begin
         FMaxZOrder:=-1;
         for tmpSeries:=0 to SeriesCount-1 do
             With Series[tmpSeries] do if FActive then CalcZOrder;
       end;
    { invert Z Orders }
    for tmpSeries:=0 to SeriesCount-1 do
    With Series[tmpSeries] do
    if FActive then
       if View3D and ApplyZOrder then IZOrder:=MaxZOrder-ZOrder
                                 else IZOrder:=0;
  end;

  Procedure SetSeriesZPositions;
  Var tmpSeries : Integer;
  begin
    for tmpSeries:=0 to SeriesCount-1 do
    With Series[tmpSeries] do
    if Active then
    begin
      StartZ :=IZOrder*SeriesWidth3D;
      if Depth=-1 then EndZ:=StartZ+SeriesWidth3D
                  else EndZ:=StartZ+Depth;
      MiddleZ:=(StartZ+EndZ) div 2;
      FMarks.ZPosition:=MiddleZ;
    end;
  end;

  Procedure DrawAllAxis;
  var t       : Integer;
      tmpAxis : TChartAxis;
  Begin
    if Assigned(FOnBeforeDrawAxes) then FOnBeforeDrawAxes(Self);
    With FAxes do
    for t:=0 to Count-1 do
    begin
      tmpAxis:=FAxes[t];
      if IsAxisVisible(tmpAxis) then tmpAxis.Draw(True);
    end;
  end;

  procedure CalcAxisRect;

    Procedure RecalcPositions;
    var tmp : Integer;
    begin
      with FAxes do
      for tmp:=0 to Count-1 do Get(tmp).InternalCalcPositions;
    end;

  var tmpR : TRect;
      OldR : TRect;
      tmp  : Integer;
  Begin
    with FAxes do
    for tmp:=0 to Count-1 do Get(tmp).AdjustMaxMin;

    RecalcPositions;

    tmpR:=ChartRect;
    with FAxes do
    for tmp:=0 to Count-1 do
      if IsAxisVisible(Get(tmp)) then
      Begin
        OldR:=tmpR;
        Get(tmp).CalcRect(OldR,tmp<5);  { <-- inflate only for first 4 axes }
        if Windows.IntersectRect(OldR,tmpR,OldR) then tmpR:=OldR;
      end;
    ChartRect:=tmpR;

    RecalcWidthHeight;

    RecalcPositions;
  end;

  Procedure CalcSeriesRect;

    Procedure CalcSeriesAxisRect(Axis:TChartAxis);
    Var tmpR      : TRect;
        a         : Integer;
        b         : Integer;
        tmpSeries : Integer;
    Begin
      tmpR:={$IFDEF D6}Types.{$ENDIF}Rect(0,0,0,0);
      for tmpSeries:=0 to SeriesCount-1 do
      With Series[tmpSeries] do
      if Active then
         if AssociatedToAxis(Axis) then
            if Axis.Horizontal then
            begin
              CalcHorizMargins(a,b);
              With tmpR do
              begin
                if Axis.AutomaticMinimum then Left :=MaxLong(Left,a);
                if Axis.AutomaticMaximum then Right:=MaxLong(Right,b);
              end;
            end
            else
            begin
              CalcVerticalMargins(a,b);
              With tmpR do
              begin
                if Axis.AutomaticMaximum then Top   :=MaxLong(Top,a);
                if Axis.AutomaticMinimum then Bottom:=MaxLong(Bottom,b);
              end;
            end;
      Axis.AdjustMaxMinRect(tmpR);
    end;

  Begin
    CalcSeriesAxisRect(BottomAxis);
    CalcSeriesAxisRect(TopAxis);
    CalcSeriesAxisRect(LeftAxis);
    CalcSeriesAxisRect(RightAxis);
  end;

Var t   : Integer;
    tmp : TRect;
    Old : Boolean;
Begin
  Old:=AutoRepaint;
  AutoRepaint:=False;
  PanelPaint(UserRectangle);
  if Assigned(FOnBeforeDrawChart) then FOnBeforeDrawChart(Self);
  for t:=0 to SeriesCount-1 do
  With Series[t] do if Active then DoBeforeDrawChart;

  if not InternalCanvas.SupportsFullRotation then DrawTitlesAndLegend(True);

  SetSeriesZOrder;
  CalcWallsRect;
  CalcAxisRect;
  SetSeriesZPositions;
  CalcSeriesRect;

  InternalCanvas.Projection(Width3D,ChartBounds,ChartRect);

  if InternalCanvas.SupportsFullRotation then
  begin
    tmp:=ChartRect;
    ChartRect:=ChartBounds;
    DrawTitlesAndLegend(True);
    ChartRect:=tmp;
  end;

  DrawWalls;
  if FAxisVisible and FAxisBehind then DrawAllAxis;

  BroadcastToolEvent(cteBeforeDrawSeries);

  if Assigned(FOnBeforeDrawSeries) then FOnBeforeDrawSeries(Self);

  for t:=0 to SeriesCount-1 do
      if Series[t].Active then DrawSeries(Series[t]);

  if FAxisVisible and (not FAxisBehind) then DrawAllAxis;

  DrawTitlesAndLegend(False);

  if Zoom.Active then DrawZoomRectangle;

  BroadcastToolEvent(cteAfterDraw);

  Canvas.ResetState;
  if Assigned(FOnAfterDraw) then FOnAfterDraw(Self);
  AutoRepaint:=Old;
end;

procedure TCustomAxisPanel.SetView3DWalls(Value:Boolean);
Begin
  SetBooleanProperty(FView3DWalls,Value);
end;

{ Internal for VCL streaming mechanism.
  Stores all Series in the DFM. }
Procedure TCustomAxisPanel.GetChildren(Proc:TGetChildProc; Root:TComponent);
var t : Integer;
begin
  inherited;
  for t:=0 to SeriesCount-1 do
  if not Series[t].InternalUse then Proc(Series[t]);
end;

{ Abstract virtual }
procedure TCustomAxisPanel.RemovedDataSource( ASeries: TChartSeries;
                                              AComponent: TComponent );
begin
end;

procedure TCustomAxisPanel.SetClipPoints(Value:Boolean);
Begin
  SetBooleanProperty(FClipPoints,Value);
end;

Procedure TCustomAxisPanel.SetCustomAxes(Value:TChartCustomAxes);
begin
  FCustomAxes.Assign(Value);
end;

procedure TCustomAxisPanel.SetLeftAxis(Value:TChartAxis);
begin
  FLeftAxis.Assign(Value);
end;

procedure TCustomAxisPanel.SetDepthAxis(Value:TChartDepthAxis);
begin
  FDepthAxis.Assign(Value);
end;

procedure TCustomAxisPanel.SetRightAxis(Value:TChartAxis);
begin
  FRightAxis.Assign(Value);
end;

procedure TCustomAxisPanel.SetTopAxis(Value:TChartAxis);
begin
  FTopAxis.Assign(Value);
end;

procedure TCustomAxisPanel.SetBottomAxis(Value:TChartAxis);
begin
  FBottomAxis.Assign(Value);
end;

{ Deletes (not destroys) a given Series.
  Triggers a "removed series" event and repaints }
Procedure TCustomAxisPanel.RemoveSeries(ASeries:TCustomChartSeries);
var t : Integer;
Begin
  t:=SeriesList.IndexOf(ASeries);
  if t<>-1 then
  begin
    ASeries.Removed;
    FSeriesList.Delete(t);
    BroadcastSeriesEvent(ASeries,seRemove);
    Invalidate;
  end;
end;

{ Returns the first series that is Active (visible) and
  associated to a given Axis. }
Function TCustomAxisPanel.GetAxisSeries(Axis:TChartAxis):TChartSeries;
Var t : Integer;
Begin
  for t:=0 to SeriesCount-1 do
  begin
    result:=Series[t];
    With result do
    if (FActive or NoActiveSeries(Axis) ) and
       AssociatedToAxis(Axis) then Exit;
  end;
  result:=nil;
end;

Function TCustomAxisPanel.FormattedValueLegend(ASeries:TChartSeries; ValueIndex:Integer):String;
begin { virtual, overrided at TChart }
end;

{ Returns a color from the "color palette" global array that is
  not used by any series. ( SeriesColor ) }
Function TCustomAxisPanel.GetFreeSeriesColor(CheckBackground:Boolean):TColor;
var t : Integer;
Begin
  t:=0;
  Repeat
    result:=GetDefaultColor(t);
    Inc(t);
  Until (t>MaxDefaultColors) or IsFreeSeriesColor(result,CheckBackground);
end;

Function TCustomAxisPanel.AddSeries(ASeries:TChartSeries):TChartSeries;
begin
  ASeries.ParentChart:=Self;
  result:=ASeries;
end;

{ Triggers an event specific to Series }
Procedure TCustomAxisPanel.BroadcastSeriesEvent(ASeries:TCustomChartSeries;
                                                Event:TChartSeriesEvent);
var tmp : TTeeSeriesEvent;
begin
  tmp:=TTeeSeriesEvent.Create;
  try
    tmp.Event:=Event;
    tmp.Series:=ASeries;
    BroadcastTeeEvent(tmp);
  finally
    tmp.Free;
  end;
end;

Procedure TCustomAxisPanel.InternalAddSeries(ASeries:TCustomChartSeries);
Begin
  if SeriesList.IndexOf(ASeries)=-1 then
  begin
    with ASeries do
    begin
      FParent:=Self;
      Added;
    end;
    FSeriesList.Add(ASeries);
    BroadcastSeriesEvent(ASeries,seAdd);
    Invalidate;
  end;
end;

{ Returns the number of points of the Active (visible) Series
  that has more points. }
Function TCustomAxisPanel.GetMaxValuesCount:Integer;
var t         : Integer;
    FirstTime : Boolean;
Begin
  result:=0;
  FirstTime:=True;
  for t:=0 to SeriesCount-1 do
  with Series[t] do
  if Active and ( FirstTime or (Count>result) ) then
  begin
    result:=Count;
    FirstTime:=False;
  end;
end;

Procedure TCustomAxisPanel.SetAxisBehind(Value:Boolean);
Begin
  SetBooleanProperty(FAxisBehind,Value);
end;

Procedure TCustomAxisPanel.SetAxisVisible(Value:Boolean);
Begin
  SetBooleanProperty(FAxisVisible,Value);
end;

{ Prevents a circular relationship between series and
  series DataSource. Raises an exception. }
Procedure TCustomAxisPanel.CheckOtherSeries(Dest,Source:TChartSeries);
var t : Integer;
Begin
  if Assigned(Source) then
  if Source.DataSource=Dest then
     Raise ChartException.Create(TeeMsg_CircularSeries)
  else
     if Source.DataSource is TChartSeries then
     for t:=0 to Source.FDataSources.Count-1 do
        CheckOtherSeries(Dest,TChartSeries(Source.DataSources[t]));
end;

{ Returns True when the AComponent is a valid DataSource for the ASeries
  parameter. }
Function TCustomAxisPanel.IsValidDataSource(ASeries:TChartSeries; AComponent:TComponent):Boolean;
Begin
  result:=(ASeries<>AComponent) and
          (AComponent is TChartSeries) and
          ASeries.IsValidSeriesSource(TChartSeries(AComponent));
end;

{ When ASeries has a DataSource that is another Series,
  calls the AddValues method to add (copy) all the points from
  the DataSource to the ASeries. }
Procedure TCustomAxisPanel.CheckDatasource(ASeries:TChartSeries);
Begin
  With ASeries do
  if not (csLoading in ComponentState) then
     if Assigned(DataSource) and (DataSource is TChartSeries) then
        AddValues(TChartSeries(DataSource));
end;

{ Swaps one series with another in the series list.
  Triggers a series event (swap event) and repaints. }
Procedure TCustomAxisPanel.ExchangeSeries(a,b:Integer);
var tmpIndex : Integer;
begin
  SeriesList.Exchange(a,b);
  tmpIndex:=Series[a].ComponentIndex;
  Series[a].ComponentIndex:=Series[b].ComponentIndex;
  Series[b].ComponentIndex:=tmpIndex;
  BroadcastSeriesEvent(nil,seSwap);
  Invalidate;
end;

{ Copies all settings from one chart to self. }
Procedure TCustomAxisPanel.Assign(Source: TPersistent);
begin
   if Source is TCustomAxisPanel then
      with TCustomAxisPanel(Source) do
         begin
            Self.FAxisBehind       := FAxisBehind;
            Self.FAxisVisible      := FAxisVisible;
            Self.BottomAxis        := FBottomAxis;
            Self.FClipPoints       := FClipPoints;
            Self.CustomAxes        := FCustomAxes;
            Self.LeftAxis          := FLeftAxis;
            Self.DepthAxis         := FDepthAxis;
            Self.FMaxPointsPerPage := FMaxPointsPerPage;
            Self.FPage             := FPage;
            Self.RightAxis         := FRightAxis;
            Self.FScaleLastPage    := FScaleLastPage;
            Self.TopAxis           := FTopAxis;
            Self.FView3DWalls      := FView3DWalls;
         end;
   inherited Assign(Source);
end;

function TCustomAxisPanel.IsCustomAxesStored: Boolean;
begin
  result:=FCustomAxes.Count>0;
end;

{ Utility functions }

{ Returns the series Title (or series Name if Title is empty). }
Function SeriesTitleOrName(ASeries:TCustomChartSeries):String;
begin
  result:=ASeries.Title;
  if result='' then
  begin
    result:=ASeries.Name;
    if (result='') and Assigned(ASeries.ParentChart) then
       result:=TeeMsg_Series+' '+TeeStr(ASeries.ParentChart.SeriesList.IndexOf(ASeries));
  end
end;

{ Adds all Series to the AItems parameter. }
Procedure FillSeriesItems(AItems:TStrings; AChart:TCustomAxisPanel);
var t : Integer;
begin
  With AChart do
  for t:=0 to SeriesCount-1 do
  if not Series[t].InternalUse then
      AItems.AddObject(SeriesTitleOrName(Series[t]),Series[t]);
end;

{ Replaces all ocurrences of "Separator" in string "St" with
  #13 (TeeLineSeparator constant)  }
Procedure TeeSplitInLines(Var St:String; Const Separator:String);
var i : Integer;
begin
  Repeat
    i:=AnsiPos(Separator,St);
    if i>0 then St[i]:=TeeLineSeparator;
  Until i=0;
end;

{ Register basic classes }
initialization
  RegisterClasses([ TChartAxisTitle,TChartAxis,TChartDepthAxis,TSeriesMarks ]);
end.
