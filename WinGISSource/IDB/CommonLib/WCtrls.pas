unit WCtrls;

interface

{$H+}

uses WinProcs, WinTypes, Classes, Controls, ExtCtrls, Forms, Graphics, Menus, Messages,
  StdCtrls, TabNotBk, CommCtrl, ComCtrls, Dialogs;

type
  TOwnerDrawAction = set of (oaDrawEntire, oaFocus, oaSelect);

{$IFNDEF WIn32}
  TListSortCompare = function(Item1, Item2: Pointer): Integer;
{$ENDIF}

  TWButton = class(TButton)
  private
    FFlat: Boolean;
    procedure SetFlat(AFlat: Boolean);
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  published
    property Flat: Boolean read FFlat write SetFlat default False;
  end;

  TWStatusBar = class(TStatusBar)
  private
    FCancelText: Boolean;
    FDisabled: Pointer;
    FInProgress: Boolean;
    FProgress: Integer;
    FProgressPanels: TStatusPanels;
    FProgressText: string;
    FAbortText: string;
    FStoredPanels: TStatusPanels;
    procedure DoDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
    procedure SetProgress(const AProgress: Real);
    procedure SetProgressPanel(ASet: Boolean);
    procedure SetProgressText(const AText: string);
    procedure SetPanelText(AIndex: Integer; const AText: string);
  protected
    procedure Resize; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property AbortText: string read FAbortText write FAbortText;
    property CancelText: Boolean read FCancelText write FCancelText;
    property Progress: Real write SetProgress;
    property ProgressPanel: Boolean read FInProgress write SetProgressPanel;
    property ProgressText: string read FProgressText write SetProgressText;
    function QueryAbort: Boolean;
    property PanelText[AIndex: Integer]: string write SetPanelText; default;
  end;

     {**************************************************************************
       Class TWList
     ---------------------------------------------------------------------------
       Implements the sort-function which is not included in the 16-bit-version.
     +*************************************************************************}
  TWList = class(TList)
{$IFNDEF WIN32}
  private
    procedure QuickSort(SortFunction: TListSortCompare; L: Integer; R: Integer);
  public
    procedure Sort(Compare: TListSortCompare);
{$ENDIF}
  end;

  TWCustomListbox = class(TCustomListBox)
  private
    FAutoScrollDir: Integer;
    FOddLineColor: TColor;
    FDoInherited: Boolean;
    FDrawAction: TOwnerDrawAction;
    FHasStrings: Boolean;
    FLockUpdates: Integer;
    FOnPaint: TNotifyEvent;
    FPendingUpdates: TRect;
    FScrollbars: TScrollStyle;
    procedure CMRecreateWnd(var Message: TMessage); message cm_RecreateWnd;
    procedure CNCharToItem(var Msg: TWMCharToItem); message cn_CharToItem;
    procedure CNDrawItem(var Message: TWMDrawItem); message cn_DrawItem;
    procedure CNVKeyToItem(var Msg: TWMVKeyToItem); message cn_VKeyToItem;
    function GetCount: Integer;
    function GetFocusedIndex: Integer;
    function GetItemIndex: Integer;
    procedure SetAutoScrollTimer;
    procedure SetCount(ACount: Integer);
    procedure SetFocusedIndex(AIndex: Integer);
    procedure SetHasStrings(ASet: Boolean);
    procedure SetItemIndex(AIndex: Integer);
    procedure SetRangeSelected(AFrom, ATo: Integer; ASet: Boolean);
    procedure SetScrollBars(AScrollbars: TScrollStyle);
    procedure WMEraseBkgnd(var Msg: TMessage); message wm_EraseBkgnd;
    procedure WMLButtonDown(var Message: TWMMouse); message wm_LButtonDown;
    procedure WMMouseMove(var Message: TWMMouse); message wm_MouseMove;
    procedure WMPaint(var Msg: TMessage); message wm_Paint;
    procedure WMTimer(var Msg: TWMTimer); message wm_Timer;
  protected
    procedure ChangeScale(M, D: Integer); override;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure DragOver(Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean); override;
    procedure LockUpdates;
    property OnPaint: TNotifyEvent read FOnPaint write FOnPaint;
    procedure UnlockUpdates;
  public
    constructor Create(AParent: TComponent); override;
    property Count: Integer read GetCount write SetCount;
    property DoInherited: Boolean read FDoInherited write FDoInherited;
    property DrawAction: TOwnerDrawAction read FDrawAction;
    property FocusedIndex: Integer read GetFocusedIndex write SetFocusedIndex;
    property HasStrings: Boolean read FHasStrings write SetHasStrings default TRUE;
    property ItemIndex: Integer read GetItemIndex write SetItemIndex;
    property ItemHeight;
    procedure InvalidateItems(FromItem, ToItem: Integer);
    property Scrollbars: TScrollStyle read FScrollbars write SetScrollbars default ssBoth;
    property RangeSelected[AFrom, ATo: Integer]: Boolean write SetRangeSelected;
  end;

  TWCustomCombobox = class(TCustomComboBox)
  private
    FDrawAction: TOwnerDrawAction;
    FOddLineColor: TColor;
    procedure CNDrawItem(var Message: TWMDrawItem); message cn_DrawItem;
  public
    property DrawAction: TOwnerDrawAction read FDrawAction;
    property ItemHeight;
  end;

  TWListBox = class(TWCustomListBox)
  published
    property Align;
    property Anchors;
    property BorderStyle;
    property Color;
    property Columns;
    property Ctl3D;
    property DragCursor;
    property DragMode;
    property Enabled;
    property ExtendedSelect;
    property Font;
    property HasStrings;
{$IFDEF WIN32}
    property ImeMode;
    property ImeName;
{$ENDIF}
    property IntegralHeight;
    property ItemHeight;
    property Items;
    property MultiSelect;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property Scrollbars;
    property ShowHint;
    property Sorted;
    property Style;
    property TabOrder;
    property TabStop;
{$IFDEF WIN32}
    property TabWidth;
{$ENDIF}
    property Visible;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnDrawItem;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMeasureItem;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnPaint;
{$IFDEF WIN32}
    property OnStartDrag;
{$ENDIF}
  end;

  TBoxStyle = (bsBox, bsTopLine, bsNone);

  TWGroupBox = class(TGroupBox)
  private
    FBoxStyle: TBoxStyle;
    procedure SetBoxStyle(AStyle: TBoxStyle);
  protected
    procedure CMEnabledChanged(var Message: TMessage); message CM_EnabledChanged;
    procedure Paint; override;
  published
    property BoxStyle: TBoxStyle read FBoxStyle write SetBoxStyle default bsBox;
  end;

  TAlignToControl = (alcNone, alcTop, alcCenter, alcBottom);

  TWLabel = class(TLabel)
  private
    FAlignToControl: TAlignToControl;
    function GetFocusControl: TWinControl;
    procedure SetAlignToControl(AAlign: TAlignToControl);
    procedure SetFocusControl(AControl: TWinControl);
    procedure UpdateAlignment;
    procedure WMWindowPosChanged(var Msg: TMessage); message wm_WindowPosChanged;
  protected
    procedure Paint; override;
  published
    property AlignToControl: TAlignToControl read FAlignToControl write SetAlignToControl default alcNone;
    property FocusControl: TWinControl read GetFocusControl write SetFocusControl;
  end;

  TMapMode = (mmText, mmLoMetric, mmHiMetric, mmLoEnglish, mmHiEnglish, mmTwips, mmIsotropic, mmAnIsotropic);

  TWindow = class(TCustomControl)
  private
    FEraseBkgnd: Boolean;
    FMapMode: TMapMode;
    FOnPaint: TNotifyEvent;
    FOwnDC: Boolean;
    FScrollbars: TScrollStyle;
    procedure CMCtl3DChanged(var Msg: TMessage); message cm_Ctl3DChanged;
    procedure CMRecreateWnd(var Msg: TMessage); message cm_RecreateWnd;
    procedure SetDCMapMode(AMapMode: TMapMode);
    procedure SetScrollBars(AScrollbars: TScrollStyle);
    procedure WMEraseBkgnd(var Msg: TMessage); message wm_EraseBkgnd;
    procedure WMNCCalcSize(var Msg: TWMNCCalcSize); message wm_NCCalcSize;
    procedure WMNCPaint(var Msg: TWMNCPaint); message wm_NCPaint;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    procedure Paint; override;
  public
    property Canvas;
    constructor Create(AParent: TComponent); override;
    procedure DoChange(Sender: TObject);
  published
    property Align;
    property Anchors;
    property Ctl3D;
    property Color default clWindow;
    property EraseBackground: Boolean read FEraseBkgnd write FEraseBkgnd default TRUE;
    property Font;
    property Height default 65;
    property MapMode: TMapMode read FMapMode write SetDCMapMode default mmText;
    property OnClick;
    property OnDblClick;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnPaint: TNotifyEvent read FOnPaint write FOnPaint;
    property OnResize;
    property OwnDC: Boolean read FOwnDC write FOwnDC default False;
    property ParentColor default FALSE;
    property ParentCtl3D;
    property ParentFont;
    property PopupMenu;
    property Scrollbars: TScrollStyle read FScrollbars write SetScrollbars default ssNone;
    property Width default 65;
  end;

  TWForm = class(TForm)
  private
    FOnMDIActivate: TNotifyEvent;
    FOnMDIDeactivate: TNotifyEvent;
    FOnHidden: TNotifyEvent;
    FOnShown: TNotifyEvent;
    FScaled: Boolean;
    procedure CMShowingChanged(var Msg: TMessage); message cm_ShowingChanged;
    procedure WMMDIActivate(var Msg: TWMMDIActivate); message wm_MDIActivate;
  protected
    procedure ChangeScale(M, D: Integer); override;
    procedure ReadState(Reader: TReader); override;
  public
    constructor Create(AOwner: TComponent); override;
    property OnHidden: TNotifyEvent read FOnHidden write FOnHidden;
    property OnMDIActivate: TNotifyEvent read FOnMDIActivate write FOnMDIActivate;
    property OnMDIDeactivate: TNotifyEvent read FOnMDIDeactivate write FOnMDIDeactivate;
    property OnShown: TNotifyEvent read FOnShown write FOnShown;
  end;

  TWTabbedNoteBook = class(TTabbedNotebook)
  private
    procedure CMFontChanged(var Msg: TMessage); message cm_FontChanged;
  end;

  TWImageList = class(TImageList)
{$IFNDEF WIN32}
    constructor CreateSize(AWidth, AHeight: Integer);
{$ENDIF}
    procedure AddBitmaps(ABitmaps: TBitmap);
  end;

  TSpeedBtn = class(TGraphicControl)
  private
    FAllowAllUp: Boolean;
    FBitmap: TBitmap;
    FDown: Boolean;
    FDragging: Boolean;
    FGlyphList: Pointer;
    FNormalIndex: Integer;
    FDisabledIndex: Integer;
    FGroupIndex: Integer;
    procedure ClearGlyphs;
    procedure CMButtonPressed(var Message: TMessage); message CM_ButtonPressed;
    procedure CMEnabledChanged(var Message: TMessage); message CM_EnabledChanged;
    procedure CMMouseEnter(var Message: TMessage); message CM_MouseEnter;
    procedure CMMouseLeave(var Message: TMessage); message CM_MouseLeave;
    procedure CMSysColorChange(var Message: TMessage); message CM_SysColorChange;
    procedure PaintFrame(var ARect: TRect);
    procedure SetBitmap(ABitmap: TBitmap);
    procedure SetDown(ADown: Boolean);
  protected
    FOver: Boolean;
    procedure ActionChange(Sender: TObject; CheckDefaults: Boolean); override;
    function GetActionLinkClass: TControlActionLinkClass; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property MouseInControl: Boolean read FOver;
  published
    property Action;
    property Anchors;
    property AllowAllUp: Boolean read FAllowAllUp write FAllowAllUp default FALSE;
    property Caption;
    property Down: Boolean read FDown write SetDown;
    property Enabled;
    property Glyph: TBitmap read FBitmap write SetBitmap;
    property GroupIndex: Integer read FGroupIndex write FGroupIndex;
    property OnClick;
    property OnDblClick;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property ParentShowHint;
    property ShowHint;
  end;

  TSpeedBtnActionLink = class(TControlActionLink)
  private
    FClient: TSpeedBtn;
  protected
    procedure AssignClient(AClient: TObject); override;
    function IsCheckedLinked: Boolean; override;
    procedure SetChecked(Value: Boolean); override;
  end;

{$IFDEF WIN32}
  TWListView = class(TListView)
  private
    FCanvas: TCanvas;
    FDrawAction: TOwnerDrawAction;
    FOnDrawItem: TDrawItemEvent;
    FOwnerDraw: Boolean;
    function GetItemRect(Item, SubItem: Integer): TRect;
  protected
    procedure CNDrawItem(var Message: TWMDrawItem); message cn_DrawItem;
    procedure CreateParams(var Params: TCreateParams); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Canvas: TCanvas read FCanvas;
    property DrawAction: TOwnerDrawAction read FDrawAction;
    property ItemRect[Item, SubItem: Integer]: TRect read GetItemRect;
  published
    property OnDrawItem: TDrawItemEvent read FOnDrawItem write FOnDrawItem;
    property OwnerDraw: Boolean read FOwnerDraw write FOwnerDraw default false;
  end;
{$ENDIF}

  TUserInterfaceStyle = (uisWin31, uisWin95, uisOffice97);

  TWRadioGroup = class(TRadioGroup)
  private
    FBoxStyle: TBoxStyle;
    procedure SetBoxStyle(AStyle: TBoxStyle);
  protected
    procedure Paint; override;
  published
    property BoxStyle: TBoxStyle read FBoxStyle write SetBoxStyle default bsBox;
  end;

  TWOpenDialog = class(TOpenDialog)
  private
    FReadOnlyCaption: string;
  protected
    procedure DoShow; override;
  published
    property ReadOnlyCaption: string read FReadOnlyCaption write FReadOnlyCaption;
  end;

  TWSaveDialog = class(TSaveDialog)
  private
    FReadOnlyCaption: string;
  protected
    procedure DoShow; override;
  published
    property ReadOnlyCaption: string read FReadOnlyCaption write FReadOnlyCaption;
  end;

procedure TextRectEx(Canvas: TCanvas; const Rect: TRect; const Text: string;
  BackgroundColor: TColor; State: TOwnerDrawState);

{******************************************************************************+
  Function AddCursor
--------------------------------------------------------------------------------
  Adds a new cursor from a resource-handle to the cursor-list of the
  application. The cursor is given a unique cursor-id which is returned by
  the function.
+******************************************************************************}
function AddCursor(AHandle: THandle): TCursor;

{******************************************************************************+
  Procedure CreateDisabledBitmap
--------------------------------------------------------------------------------
  Creates a disabled version of ABitmap.
+******************************************************************************}
procedure CreateDisabledBitmap(ABitmap: TBitmap; DestImage: TBitmap);

{******************************************************************************+
  Procedure SetControlEnabled
--------------------------------------------------------------------------------
  Enables or disables a control and all linked labels and spin-buttons.
+******************************************************************************}
procedure SetControlEnabled(AControl: TControl; AEnable: Boolean);

{******************************************************************************+
  Procedure SetControlGroupEnabled
--------------------------------------------------------------------------------
  Enabled or disables a control and all it's sub-controls.
+******************************************************************************}
procedure SetControlGroupEnabled(AControl: TControl; AEnable: Boolean);

{******************************************************************************+
  Procedure SetControlVisible
--------------------------------------------------------------------------------
  Sets the visible-state of a control and all linked labels and spin-buttons.
+******************************************************************************}
procedure SetControlVisible(AControl: TControl; AVisible: Boolean);

var // name and size of the font to use for dialog-boxes (TWForm automatically
    // selects thsi font)
  DialogFontName: string;
  DialogFontSize: Integer;

implementation

uses ActnList, CommDlg, ImgList, Lists, NumTools, SpinBtn, SysUtils;

const // number of the last cursor added by AddCursor
  LastUserDefCursor: TCursor = 10000;

type {*************************************************************************+
       Class TGlyphList
     ---------------------------------------------------------------------------
       Liste von Button-Images mit einer bestimmten Gr��e.
     {*************************************************************************}
  TGlyphList = class(TWImageList)
  private
    FCount: Integer;
    FUsed: TBitList;
  public
    constructor CreateSize(AWidth, AHeight: Integer);
    destructor Destroy; override;
    function Add(Image: TBitmap): Integer;
    function AddMasked(Image: TBitmap; MaskColor: TColor): Integer;
    function AllocateIndex: Integer;
    procedure Delete(Index: Integer);
    property Count: Integer read FCount;
  end;

     {*************************************************************************+
       Class TGlyphList
     --------------------------------------------------------------------------
       Liste von TGlyphList um verschiedene Bitmap-Gr��en speichern zu k�nnen.
     {*************************************************************************}
  TGlyphCache = class
  private
    FLists: TList;
  public
    constructor Create;
    destructor Destroy; override;
    function GetList(AWidth, AHeight: Integer): TGlyphList;
    procedure ReturnList(AList: TGlyphList);
  end;

var
  Pattern: TBitmap; // bitmap used to draw checked menu-buttons
  GlyphCache: TGlyphCache; // cache for the menu-button-bitmaps

{==============================================================================+
  TWGroupbox
+==============================================================================}

procedure TWGroupBox.CMEnabledChanged;
begin
  Invalidate;
  inherited;
end;

procedure TWGroupBox.Paint;
var
  X: Integer;
  Y: Integer;
  R: TRect;
  S: array[0..255] of Char;
begin
  with Canvas do
    if FBoxStyle = bsBox then
      inherited Paint
    else
      if FBoxStyle <> bsNone then
      begin
        Brush.Color := Color;
        Font := Self.Font;
        R := Rect(0, 0, 0, 0);
        StrPCopy(S, Caption);
        DrawText(Handle, S, Length(Caption), R, DT_LEFT or DT_SINGLELINE or DT_CALCRECT);
        if not Enabled then
        begin
          Brush.Style := bsClear;
          Font.Color := clBtnShadow;
        end;
        DrawText(Handle, S, Length(Caption), R, DT_LEFT or DT_SINGLELINE);
        X := R.Right + 3;
        Y := Abs(Font.Height) div 2 + 1;
        Pen.Color := clBtnShadow;
        MoveTo(X, Y);
        LineTo(ClientWidth, Y);
        Pen.Color := clBtnHighLight;
        MoveTo(X, Y + 1);
        LineTo(ClientWidth, Y + 1);
      end;
end;

procedure TWGroupBox.SetBoxStyle
  (
  AStyle: TBoxStyle
  );
begin
  if AStyle <> FBoxStyle then
  begin
    FBoxStyle := AStyle;
    Invalidate;
  end;
end;

{==============================================================================+
  TWindow
+==============================================================================}

constructor TWindow.Create(AParent: TComponent);
begin
  inherited Create(AParent);
  FOwnDC := FALSE;
  FMapMode := mmText;
  FEraseBkgnd := TRUE;
  ParentColor := FALSE;
  Color := clWindow;
  Width := 65;
  Height := 65;
  Canvas.OnChange := DoChange;
end;

procedure TWindow.WMNCCalcSize(var Msg: TWMNCCalcSize);
const
  FrameWidth = 2;
begin
  inherited;
  if Ctl3D then
    with Msg do
    begin
      Inc(CalcSize_Params^.rgrc[0].Top, FrameWidth);
      Inc(CalcSize_Params^.rgrc[0].Left, FrameWidth);
      Dec(CalcSize_Params^.rgrc[0].Bottom, FrameWidth);
      Dec(CalcSize_Params^.rgrc[0].Right, FrameWidth);
      Result := 0;
    end;
end;

procedure TWindow.WMNCPaint(var Msg: TWMNCPaint);
var
  WindowDC: THandle;
  Rect: TRect;
begin
  inherited;
  if Ctl3D then
  begin
    WindowDC := GetWindowDC(Handle);
    try
      Canvas.Handle := WindowDC;
      with Canvas do
      begin
        Rect := ClientRect;
        Inc(Rect.Right, 4);
        Inc(Rect.Bottom, 4);
        Frame3D(Canvas, Rect, clBtnShadow, clBtnHighlight, 1);
        Frame3D(Canvas, Rect, clBtnHighlight, cl3DLight, 1);
      end;
    finally
      Canvas.Handle := 0;
      ReleaseDC(Handle, WindowDC);
    end;
  end;
end;

procedure TWindow.CMRecreateWnd(var Msg: TMessage);
begin
  inherited;
  SetDCMapMode(FMapMode);
end;

procedure TWindow.DoChange(Sender: TObject);
begin
end;

procedure TWindow.SetDCMapMode(AMapMode: TMapMode);
begin
  FMapMode := AMapMode;
  if HandleAllocated then
    case FMapMode of
      mmAnIsotropic: SetMapMode(Canvas.Handle, mm_AnIsotropic);
      mmHiEnglish: SetMapMode(Canvas.Handle, mm_HiEnglish);
      mmHiMetric: SetMapMode(Canvas.Handle, mm_HiMetric);
      mmIsotropic: SetMapMode(Canvas.Handle, mm_Isotropic);
      mmLoEnglish: SetMapMode(Canvas.Handle, mm_LoEnglish);
      mmLoMetric: SetMapMode(Canvas.Handle, mm_LoMetric);
      mmText: SetMapMode(Canvas.Handle, mm_Text);
      mmTwips: SetMapMode(Canvas.Handle, mm_Twips);
    end;
end;

procedure TWindow.CreateWnd;
begin
  inherited CreateWnd;
  SetDCMapMode(FMapMode);
end;

procedure TWindow.SetScrollBars(AScrollbars: TScrollStyle);
begin
  if AScrollbars <> FScrollbars then
  begin
    FScrollbars := AScrollbars;
    RecreateWnd;
  end;
end;

procedure TWindow.Paint;
begin
  inherited Paint;
  if Assigned(FOnPaint) then
  begin
    if not FOwnDC then
      SetDCMapMode(FMapMode);
    Canvas.Font.Assign(Font);
    OnPaint(Self);
  end;
end;

procedure TWindow.WMEraseBkgnd(var Msg: TMessage);
var
  ARect: TRect;
begin
  if FEraseBkgnd or (csDesigning in ComponentState) then
  begin
    if MapMode = mmText then
      inherited
    else
    begin
      ARect := ClientRect;
      DPToLP(Canvas.Handle, ARect, 2);
      Canvas.Brush := Brush;
      Canvas.FillRect(ARect);
    end;
  end
  else
    Msg.Result := 1;
end;

procedure TWindow.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  if FOwnDC then
    with Params.WindowClass do
    begin
      Style := Style or cs_OwnDC;
      Params.WinClassName := 'TWindowOwnDC';
    end;
  if FScrollbars in [ssVertical, ssBoth] then
    Params.Style := Params.Style or ws_VScroll;
  if FScrollbars in [ssHorizontal, ssBoth] then
    Params.Style := Params.Style or ws_HScroll;
end;

procedure TWindow.CMCtl3DChanged(var Msg: TMessage);
begin
  inherited;
  if HandleAllocated then
    RecreateWnd;
end;

{==============================================================================+
  TWForm
+==============================================================================}

constructor TWForm.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

procedure TWForm.ReadState(Reader: TReader);
begin
  inherited ReadState(Reader);
  if BorderStyle = bsSizeable then
  begin
    ClientWidth := MulDiv(ClientWidth, Screen.PixelsPerInch, 96);
    ClientHeight := MulDiv(ClientHeight, Screen.PixelsPerInch, 96);
  end;
  with Constraints do
  begin
    MinHeight := MulDiv(MinHeight, Screen.PixelsPerInch, 96);
    MaxHeight := MulDiv(MaxHeight, Screen.PixelsPerInch, 96);
    MinWidth := MulDiv(MinWidth, Screen.PixelsPerInch, 96);
    MaxWidth := MulDiv(MaxWidth, Screen.PixelsPerInch, 96);
  end;
end;

procedure TWForm.ChangeScale(M, D: Integer);
begin
  inherited ChangeScale(M, D);
  with Constraints do
  begin
    MinHeight := MulDiv(MinHeight, M, D);
    MaxHeight := MulDiv(MaxHeight, M, D);
    MinWidth := MulDiv(MinWidth, M, D);
    MaxWidth := MulDiv(MaxWidth, M, D);
  end;
end;

procedure TWForm.CMShowingChanged
  (
  var Msg: TMessage
  );
begin
  if not FScaled then
  begin
    if Font.Name <> DialogFontname then
      Font.Name := DialogFontName;
    if DialogFontSize <> Font.Size then
      ChangeScale(DialogFontSize, Font.Size);
    FScaled := TRUE;
  end;
  inherited;
  if Showing then
  begin
    if Assigned(FOnShown) then
      OnShown(Self);
  end
  else
    if Assigned(FOnHidden) then
      OnHidden(Self);
end;

procedure TWForm.WMMDIActivate
  (
  var Msg: TWMMDiActivate
  );
begin
  inherited;
  if not (csDestroying in ComponentState) then
  begin
    if (Msg.DeactiveWnd = Handle)
      and Assigned(FOnMDIDeactivate) then
      OnMDIDeactivate(Self);
    if (Msg.ActiveWnd = Handle)
      and Assigned(FOnMDIActivate) then
      OnMDIActivate(Self);
  end;
end;

{==============================================================================+
  TWLabel
+==============================================================================}

procedure TWLabel.WMWindowPosChanged
  (
  var Msg: TMessage
  );
begin
  inherited;
  UpdateAlignment;
end;

procedure TWLabel.SetFocusControl
  (
  AControl: TWinControl
  );
begin
  inherited FocusControl := AControl;
  UpdateAlignment;
end;

function TWLabel.GetFocusControl
  : TWinControl;
begin
  Result := inherited FocusControl;
end;

procedure TWLabel.SetAlignToControl
  (
  AAlign: TAlignToControl
  );
begin
  if AAlign <> FAlignToControl then
  begin
    FAlignToControl := AAlign;
    UpdateAlignment;
    FAlignToControl := AAlign;
  end;
end;

procedure TWLabel.UpdateAlignment;
begin
  if FocusControl <> nil then
    case FAlignToControl of
      alcTop: Top := FocusControl.Top;
      alcCenter: Top := FocusControl.Top + (FocusControl.Height - Height) div 2;
      alcBottom: Top := FocusControl.Top + FocusControl.Height - Height;
    end;
end;

procedure TWLabel.Paint;
var
  Buffer: array[0..128] of Char;
  Format: Word;
  ARect: TRect;
begin
  with Canvas do
  begin
    UpdateAlignment;
    ARect := ClientRect;
    if not Transparent then
    begin
      Brush.Color := Self.Color;
      Brush.Style := bsSolid;
      FillRect(ARect);
    end;
    Brush.Style := bsClear;
    if Alignment = taCenter then
      Format := DT_CENTER
    else
      if Alignment = taRightJustify then
        Format := DT_RIGHT
      else
        Format := DT_LEFT;
    Canvas.Font := Self.Font;
    if not Enabled then
      Font.Color := clBtnShadow;
    DrawText(Handle, StrPCopy(Buffer, Caption), Length(Caption), ARect,
      Format + DT_VCENTER);
  end;
end;

procedure SetControlGroupEnabled(AControl: TControl; AEnable: Boolean);
var
  Cnt: Integer;
begin
  if AEnable <> AControl.Enabled then
  begin
    AControl.Enabled := AEnable;
    if AControl is TWinControl then
      with AControl as TWinControl do
        for Cnt := 0 to ControlCount - 1 do
          SetControlGroupEnabled(Controls[Cnt], AEnable);
  end;
end;

procedure SetControlEnabled
  (
  AControl: TControl;
  AEnable: Boolean
  );
var
  Cnt: Integer;
  Item: TControl;
begin
  if AEnable <> AControl.Enabled then
  begin
    AControl.Enabled := AEnable;
    for Cnt := 0 to AControl.Parent.ControlCount - 1 do
    begin
      Item := AControl.Parent.Controls[Cnt];
      if (Item is TLabel) and (TLabel(Item).FocusControl = AControl) then
        Item.Enabled := AEnable
      else
        if (Item is TSpinBtn) and (TSpinBtn(Item).Control = AControl) then
          TSpinBtn(Item).Enabled := AEnable;
    end;
  end;
end;

procedure SetControlVisible
  (
  AControl: TControl;
  AVisible: Boolean
  );
var
  Cnt: Integer;
  Item: TControl;
begin
  if AVisible <> AControl.Visible then
  begin
    AControl.Visible := AVisible;
    for Cnt := 0 to AControl.Parent.ControlCount - 1 do
    begin
      Item := AControl.Parent.Controls[Cnt];
      if (Item is TLabel) and (TLabel(Item).FocusControl = AControl) then
        Item.Visible := AVisible
      else
        if (Item is TSpinBtn) and (TSpinBtn(Item).Control = AControl) then
          TSpinBtn(Item).Visible := AVisible;
    end;
  end;
end;

{===============================================================================
| TWCustomListbox
+==============================================================================}

constructor TWCustomListbox.Create
  (
  AParent: TComponent
  );
begin
  inherited Create(AParent);
  FScrollbars := ssBoth;
  FHasStrings := TRUE;
  FOddLineColor := Color;
end;

procedure TWCustomListbox.WMEraseBkgnd
  (
  var Msg: TMessage
  );
var
  ARect: TRect;
  Cnt: Integer;
  Count: Integer;
begin
  if FOddLineColor = Color then
    inherited
  else
    with Canvas do
    begin
      Count := ClientHeight div (2 * ItemHeight) + 1;
      ARect := Rect(0, 0, ClientWidth, ItemHeight);
      if TopIndex mod 1 = 1 then
        Brush.Color := Color
      else
        Brush.Color := FOddLineColor;
      for Cnt := 0 to Count do
      begin
        FillRect(ARect);
        OffsetRect(ARect, 0, 2 * ItemHeight);
      end;
      ARect := Rect(0, ItemHeight, ClientWidth, 2 * ItemHeight);
      if TopIndex mod 1 = 0 then
        Brush.Color := Color
      else
        Brush.Color := FOddLineColor;
      for Cnt := 0 to Count do
      begin
        FillRect(ARect);
        OffsetRect(ARect, 0, 2 * ItemHeight);
      end;
      Msg.Result := 1;
    end;
end;

procedure TWCustomListbox.CNDrawItem
  (
  var Message: TWMDrawItem
  );
var
  State: TOwnerDrawState;
begin
  if FLockUpdates <= 0 then
    with Message.DrawItemStruct^ do
    begin
{$IFDEF WIN32}
      State := TOwnerDrawState(LongRec(itemState).Lo);
      FDrawAction := TOwnerDrawAction(WordRec(LongRec(itemAction).Lo).Lo);
{$ELSE}
      State := TOwnerDrawState(WordRec(itemState).Lo);
      FDrawAction := TOwnerDrawAction(WordRec(itemAction).Lo);
{$ENDIF}
      Canvas.Handle := hDC;
      Canvas.Font := Font;
      Canvas.Brush := Brush;
      if itemID mod 1 = 0 then
        Canvas.Brush.Color := Self.Color
      else
        Canvas.Brush.Color := FOddLineColor;
      if odSelected in State then
      begin
        Canvas.Brush.Color := clHighlight;
        Canvas.Font.Color := clHighlightText;
      end;
      if Integer(itemID) >= 0 then
        DrawItem(itemID, rcItem, State)
      else
        Canvas.FillRect(rcItem);
      Canvas.Handle := 0;
    end;
end;

procedure TWCustomListbox.WMLButtonDown
  (
  var Message: TWMMouse
  );
begin
  with Message do
  begin
    DoInherited := TRUE;
{$IFDEF WIN32}
    SendCancelMode(Self);
{$ENDIF}
    if csCaptureMouse in ControlStyle then
      MouseCapture := TRUE;
    MouseDown(mbLeft, KeysToShiftState(Keys), XPos, YPos);
    if DoInherited then
      DefaultHandler(Message);
  end;
end;

procedure TWCustomListbox.WMMouseMove
  (
  var Message: TWMMouse
  );
begin
  with Message do
  begin
    DoInherited := TRUE;
    MouseMove(KeysToShiftState(Keys), XPos, YPos);
    if DoInherited then
      DefaultHandler(Message);
  end;
  SetAutoScrollTimer;
end;

procedure TWCustomListbox.CreateParams
  (
  var Params: TCreateParams
  );
begin
  inherited CreateParams(Params);
  Params.Style := Params.Style and not (ws_HScroll or ws_VScroll);
  if FScrollbars in [ssVertical, ssBoth] then
    Params.Style := Params.Style or lbs_DisableNoScroll or ws_VScroll;
  if FScrollbars in [ssHorizontal, ssBoth] then
    Params.Style := Params.Style or lbs_DisableNoScroll or ws_HScroll;
  if FHasStrings then
    Params.Style := Params.Style or lbs_HasStrings
{$IFDEF WIN32}
  else
    Params.Style := (Params.Style or lbs_NoData or lbs_WantKeyboardInput) and not lbs_HasStrings;
{$ELSE}
  else
    Params.Style := (Params.Style or lbs_WantKeyboardInput) and not lbs_HasStrings;
{$ENDIF}
end;

function TWCustomListbox.GetItemIndex
  : Integer;
var
  Cnt: Integer;
begin
  Result := -1;
  if MultiSelect then
  begin
    for Cnt := Items.Count - 1 downto 0 do
      if Selected[Cnt] then
      begin
        Result := Cnt;
        Break;
      end;
  end
  else
    Result := inherited ItemIndex;
end;

procedure TWCustomListbox.SetItemIndex
  (
  AIndex: Integer
  );
begin
  if MultiSelect and (AIndex >= 0) then
  begin
    if (SelCount > 1) or not Selected[AIndex] then
    begin
      RangeSelected[0, Count - 1] := FALSE;
      RangeSelected[AIndex, AIndex] := TRUE;
      FocusedIndex := AIndex;
    end;
  end
  else
    inherited ItemIndex := AIndex;
end;

procedure TWCustomListbox.SetScrollBars
  (
  AScrollbars: TScrollStyle
  );
begin
  if AScrollbars <> FScrollbars then
  begin
    FScrollbars := AScrollbars;
    RecreateWnd;
  end;
end;

procedure TWCustomListbox.SetHasStrings
  (
  ASet: Boolean
  );
begin
  if ASet <> FHasStrings then
  begin
    FHasStrings := ASet;
    RecreateWnd;
  end;
end;

function TWCustomListbox.GetCount
  : Integer;
begin
  Result := Items.Count;
end;

procedure TWCustomListbox.SetCount
  (
  ACount: Integer
  );
var
  OldItemIndex: Integer;
  OldTopIndex: Integer;
begin
  if ACount <> Count then
  begin
    if not FHasStrings then
    begin
      LockUpdates;
      OldItemIndex := ItemIndex;
      OldTopIndex := TopIndex;
      if ACount > Count then
        InvalidateItems(Count, ACount)
      else
        InvalidateItems(ACount, Count);
      SendMessage(Handle, lb_SetCount, ACount, 0);
      ItemIndex := Min(OldItemIndex, ACount - 1);
      TopIndex := OldTopIndex;
      if TopIndex <> OldTopIndex then
        UnionRect(FPendingUpdates, FPendingUpdates, ClientRect);
      UnlockUpdates;
    end
    else
    begin
      Items.BeginUpdate;
      while Count > ACount do
        Items.Delete(Count - 1);
      while Count < ACount do
        Items.Add(' ');
      Items.EndUpdate;
    end;
  end;
end;

procedure TWCustomListbox.ChangeScale(M, D: Integer);
begin
  inherited ChangeScale(M, D);
  ItemHeight := (MulDiv(ItemHeight, M, D) div 2) * 2;
end;

procedure TWCustomListbox.WMTimer(var Msg: TWMTimer);
begin
  TopIndex := Max(0, Min(TopIndex + FAutoScrollDir, Count - ClientHeight div ItemHeight));
  SetAutoScrollTimer;
end;

procedure TWCustomListbox.DragOver
  (
  Source: TObject;
  X: Integer;
  Y: Integer;
  State: TDragState;
  var Accept: Boolean
  );
begin
  inherited DragOver(Source, X, Y, State, Accept);
  SetAutoScrollTimer;
end;

procedure TWCustomListbox.SetAutoScrollTimer;
var
  Mouse: TPoint;
begin
  GetCursorPos(Mouse);
  Mouse := ScreenToClient(Mouse);
  if Mouse.Y < 0 then
    FAutoScrollDir := -(Abs(Mouse.Y - 10) div 10)
  else
    if Mouse.Y >= ClientHeight then
      FAutoScrollDir := (Mouse.Y - ClientHeight + 10) div 10
    else
      FAutoScrollDir := 0;
  if FAutoScrollDir <> 0 then
    SetTimer(Handle, 1, 100, nil)
  else
    KillTimer(Handle, 1);
end;

procedure TWCustomListbox.LockUpdates;
begin
  Inc(FLockUpdates);
    { disable the refresh of the window }
  if FLockUpdates = 1 then
  begin
    SendMessage(Handle, wm_SetRedraw, 0, 0);
    FPendingUpdates := Rect(0, 0, 0, 0);
  end;
end;

procedure TWCustomListbox.UnlockUpdates;
begin
  if FLockUpdates > 0 then
  begin
    Dec(FLockUpdates);
      { enable the redraw of the window }
    SendMessage(Handle, wm_SetRedraw, 1, 0);
      { wm_SetRedraw invalidates the whole client-area, let the listbox do this
      + by itself to avoid flicker }
    ValidateRect(Handle, nil);
    if not IsRectEmpty(FPendingUpdates) then
      InvalidateRect(Handle, @FPendingUpdates, TRUE);
  end;
end;

procedure TWCustomListbox.WMPaint
  (
  var Msg: TMessage
  );
begin
  inherited;
  if Assigned(FOnPaint) then
    OnPaint(Self);
end;

procedure TWCustomListbox.CNCharToItem
  (
  var Msg: TWMCharToItem
  );
begin
  Msg.Result := -1;
end;

procedure TWCustomListbox.CNVKeyToItem
  (
  var Msg: TWMVKeyToItem
  );
begin
  Msg.Result := -1
end;

procedure TWCustomListbox.InvalidateItems
  (
  FromItem: Integer;
  ToItem: Integer
  );
var
  ARect: TRect;
begin
  ARect := Rect(0, (FromItem - TopIndex) * ItemHeight, ClientWidth,
    (ToItem - TopIndex + 1) * ItemHeight);
  if FLockUpdates = 0 then
    InvalidateRect(Handle, @ARect, TRUE)
  else
    UnionRect(FPendingUpdates, FPendingUpdates, ARect);
end;

procedure TWCustomListbox.SetRangeSelected
  (
  AFrom: Integer;
  ATo: Integer;
  ASet: Boolean
  );
begin
{$IFDEF WIN32}
  SendMessage(Handle, lb_SelItemRange, Byte(ASet), MakeLParam(AFrom, ATo));
{$ELSE}
  SendMessage(Handle, lb_SelItemRange, Byte(ASet), MakeLong(AFrom, ATo));
{$ENDIF}
end;

function TWCustomListbox.GetFocusedIndex
  : Integer;
begin
  Result := SendMessage(Handle, lb_GetCaretIndex, 0, 0);
end;

procedure TWCustomListbox.SetFocusedIndex(AIndex: Integer);
begin
  SendMessage(Handle, lb_SetCaretIndex, AIndex, 0);
end;

procedure TWCustomListbox.CMRecreateWnd(var Message: TMessage);
var
  OldCount: Integer;
  OldItemIndex: Integer;
begin
  if not FHasStrings then
  begin
    OldCount := Count;
    OldItemIndex := ItemIndex;
    inherited;
    Count := OldCount;
    ItemIndex := OldItemIndex;
  end
  else
    inherited;
end;

{===============================================================================
| TWList
+==============================================================================}

{$IFNDEF WIN32}

procedure TWList.QuickSort
  (
  SortFunction: TListSortCompare;
  L: Integer;
  R: Integer
  );
var
  I: Integer;
  J: Integer;
  P: Pointer;
  T: Pointer;
begin
  repeat
    I := L;
    J := R;
    P := Items[(L + R) shr 1];
    repeat
      while SortFunction(Items[I], P) < 0 do
        Inc(I);
      while SortFunction(Items[J], P) > 0 do
        Dec(J);
      if I <= J then
      begin
        T := Items[I];
        Items[I] := Items[J];
        Items[J] := T;
        Inc(I);
        Dec(J);
      end;
    until I > J;
    if L < J then
      QuickSort(SortFunction, L, J);
    L := I;
  until I >= R;
end;

procedure TWList.Sort
  (
  Compare: TListSortCompare
  );
begin
  QuickSort(Compare, 0, Count - 1);
end;
{$ENDIF}

{===============================================================================
| TWTabbedNotebook
+==============================================================================}

procedure TWTabbedNotebook.CMFontChanged
  (
  var Msg: TMessage
  );
begin
  inherited;
  TabFont := Font;
end;

{===============================================================================
| TWImageList
+==============================================================================}

{$IFNDEF WIN32}

constructor TWImageList.CreateSize
  (
  AWidth: Integer;
  AHeight: Integer
  );
begin
  Create(AWidth, AHeight);
end;
{$ENDIF}

procedure TWImageList.AddBitmaps
  (
  ABitmaps: TBitmap
  );
var
  ARect: TRect;
  BRect: TRect;
  BBitmap: TBitmap;
begin
  ARect := Rect(0, 0, Width, Height);
  BRect := ARect;
  BBitmap := TBitmap.Create;
  try
    BBitmap.Width := Width;
    BBitmap.Height := Height;
    while BRect.Right <= ABitmaps.Width do
    begin
      BBitmap.Canvas.CopyRect(ARect, ABitmaps.Canvas, BRect);
      AddMasked(BBitmap, ABitmaps.TransparentColor);
      OffsetRect(BRect, Width, 0);
    end;
  finally
    BBitmap.Free;
  end;
end;

{==============================================================================}
{ TGlyphList                                                                   }
{==============================================================================}

constructor TGlyphList.CreateSize
  (
  AWidth: Integer;
  AHeight: Integer
  );
begin
  inherited CreateSize(AWidth, AHeight);
  FUsed := TBitList.Create(0);
end;

destructor TGlyphList.Destroy;
begin
  FUsed.Free;
  inherited Destroy;
end;

function TGlyphList.Add
  (
  Image: TBitmap
  )
  : Integer;
begin
  Result := AllocateIndex;
  Replace(Result, Image, nil);
  Inc(FCount);
end;

function TGlyphList.AddMasked
  (
  Image: TBitmap;
  MaskColor: TColor
  )
  : Integer;
begin
  Result := AllocateIndex;
  ReplaceMasked(Result, Image, MaskColor);
  Inc(FCount);
end;

procedure TGlyphList.Delete
  (
  Index: Integer
  );
begin
  if FUsed[Index] then
  begin
    Dec(FCount);
    FUsed[Index] := False;
  end;
end;

function TGlyphList.AllocateIndex
  : Integer;
begin
  Result := FUsed.FirstCleared;
  if Result = -1 then
  begin
    Result := inherited Add(nil, nil);
    FUsed.Capacity := Result + 1;
  end;
  FUsed[Result] := True;
end;

{==============================================================================}
{ TGlyphCache                                                                  }
{==============================================================================}

constructor TGlyphCache.Create;
begin
  inherited Create;
  FLists := TList.Create;
end;

destructor TGlyphCache.Destroy;
var
  Cnt: Integer;
begin
  for Cnt := 0 to FLists.Count - 1 do
    TGlyphList(FLists[Cnt]).Free;
  FLists.Free;
  inherited Destroy;
end;

function TGlyphCache.GetList
  (
  AWidth: Integer;
  AHeight: Integer
  )
  : TGlyphList;
var
  Cnt: Integer;
begin
  for Cnt := FLists.Count - 1 downto 0 do
  begin
    Result := FLists[Cnt];
    with Result do
      if (AWidth = Width)
        and (AHeight = Height) then
        Exit;
  end;
  Result := TGlyphList.CreateSize(AWidth, AHeight);
  FLists.Add(Result);
end;

procedure TGlyphCache.ReturnList
  (
  AList: TGlyphList
  );
begin
  if AList.Count = 0 then
  begin
    FLists.Remove(AList);
    AList.Free;
  end;
end;

procedure CreateBrushPattern;
var
  X: Integer;
  Y: Integer;
begin
  Pattern := TBitmap.Create;
  Pattern.Width := 8;
  Pattern.Height := 8;
  with Pattern.Canvas do
  begin
    Brush.Style := bsSolid;
    Brush.Color := clBtnFace;
    FillRect(Rect(0, 0, Pattern.Width, Pattern.Height));
    for Y := 0 to 7 do
      for X := 0 to 7 do
        if (Y mod 2) = (X mod 2) then
          Pixels[X, Y] := clBtnHighlight;
  end;
end;

{===============================================================================
| TSpeedBtn
+==============================================================================}

constructor TSpeedBtn.Create
  (
  AOwner: TComponent
  );
var
  ASize: Integer;
begin
  inherited Create(AOwner);
  ControlStyle := [csCaptureMouse, csOpaque];
  if NewStyleControls then
    ASize := 22
  else
    ASize := 23;
  SetBounds(0, 0, ASize, ASize);
  FBitmap := TBitmap.Create;
  FNormalIndex := -1;
  FDisabledIndex := -1;
  FGroupIndex := 0;
end;

destructor TSpeedBtn.Destroy;
begin
  FBitmap.Free;
  ClearGlyphs;
  inherited Destroy;
end;

procedure TSpeedBtn.PaintFrame(var ARect: TRect);
var
  ADown: Boolean;
begin
  ADown := FDown and Enabled;
  if ADown or (FDragging and FOver) then
    Frame3D(Canvas, ARect, clBtnShadow, clBtnHighlight, 1)
  else
    if FDragging or FOver then
      Frame3D(Canvas, ARect, clBtnHighlight, clBtnShadow, 1)
    else
      if csDesigning in ComponentState then
        Frame3D(Canvas, ARect, clBtnHighlight, clBtnShadow, 1)
      else
        Frame3D(Canvas, ARect, clBtnFace, clBtnFace, 1)
end;

procedure TSpeedBtn.Paint;
var
  ARect: TRect;
  ADown: Boolean;
  IHeight: Integer;
  IWidth: Integer;
  HasDisabled: Boolean;
  AIndex: Integer;
  TmpImage: TBitmap;
  IRect: TRect;
begin
  ARect := ClientRect;
  ADown := FDown and Enabled;
  Canvas.Brush.Color := clBtnFace;
  Canvas.Brush.Style := bsSolid;
  PaintFrame(ARect);
  if ADown then
  begin
    if Pattern = nil then
      CreateBrushPattern;
    Canvas.Brush.Bitmap := Pattern;
  end;
  Canvas.FillRect(ARect);
  InflateRect(ARect, -1, -1);
  ARect := ClientRect;
  if ADown or (FDragging and FOver) then
    OffsetRect(ARect, 1, 1);
  if not Assigned(FBitmap)
    or (FBitmap.Height = 0)
    or (FBitmap.Width = 0) then
    Exit;
  IWidth := FBitmap.Width;
  IHeight := FBitmap.Height;
  HasDisabled := IWidth > IHeight;
  if HasDisabled then
    IWidth := IWidth div 2;
  if FGlyphList = nil then
    FGlyphList := GlyphCache.GetList(IWidth, IHeight);
  IRect := Rect(0, 0, IWidth, IHeight);
  if not Enabled then
    AIndex := FDisabledIndex
  else
    AIndex := FNormalIndex;
  if AIndex = -1 then
  begin
    TmpImage := TBitmap.Create;
    try
      TmpImage.Width := IWidth;
      TmpImage.Height := IHeight;
      if not Enabled then
      begin
        CreateDisabledBitmap(FBitmap, TmpImage);
        FDisabledIndex := TGlyphList(FGlyphList).Add(TmpImage);
        AIndex := FDisabledIndex;
      end
      else
      begin
        FNormalIndex := TGlyphList(FGlyphList).AddMasked(FBitmap, FBitmap.TransparentColor);
        AIndex := FNormalIndex;
      end;
    finally
      TmpImage.Free;
    end;
    FBitmap.Dormant;
  end;
  TGlyphList(FGlyphList).Draw(Canvas, ARect.Left + (ARect.Right - ARect.Left - IRect.Right) div 2,
    ARect.Top + (ARect.Bottom - ARect.Top - IRect.Bottom) div 2, AIndex);
end;

procedure TSpeedBtn.SetBitmap
  (
  ABitmap: TBitmap
  );
begin
  FBitmap.Assign(ABitmap);
  ClearGlyphs;
  Invalidate;
end;

procedure TSpeedBtn.MouseMove
  (
  Shift: TShiftState;
  X: Integer;
  Y: Integer
  );
var
  ARect: TRect;
  NewOver: Boolean;
begin
  inherited MouseMove(Shift, X, Y);
  if FDragging and not FDown then
  begin
    ARect := ClientRect;
    NewOver := ptInRect(ARect, Point(X, Y));
    if NewOver <> FOver then
    begin
      FOver := NewOver;
      Repaint;
    end;
  end;
end;

procedure TSpeedBtn.MouseDown
  (
  Button: TMouseButton;
  Shift: TShiftState;
  X: Integer;
  Y: Integer
  );
begin
  inherited MouseDown(Button, Shift, X, Y);
  if (Button = mbLeft) and Enabled then
  begin
    FDragging := TRUE;
    FOver := TRUE;
    Repaint;
  end;
end;

procedure TSpeedBtn.MouseUp
  (
  Button: TMouseButton;
  Shift: TShiftState;
  X: Integer;
  Y: Integer
  );
var
  ARect: TRect;
begin
  inherited MouseUp(Button, Shift, X, Y);
  if FDragging then
  begin
    FDragging := FALSE;
    ARect := ClientRect;
    if ptInRect(ARect, Point(X, Y)) then
    begin
      if FGroupIndex = 0 then
        Invalidate
      else
        if not Down then
          SetDown(TRUE)
        else
          if FAllowAllUp then
            SetDown(FALSE);
      Click;
    end
    else
      Invalidate;
  end;
end;

procedure TSpeedBtn.SetDown(ADown: Boolean);
var
  Msg: TMessage;
begin
  if FDown <> ADown then
  begin
    FDown := ADown;
    if Action is TCustomAction then
      TCustomAction(Action).Checked := FDown;
    if FDown and (FGroupIndex > 0) then
    begin
      Msg.Msg := CM_BUTTONPRESSED;
      Msg.WParam := FGroupIndex;
      Msg.LParam := LongInt(Self);
      Msg.Result := 0;
      Parent.Broadcast(Msg);
    end;
    Invalidate;
  end;
end;

procedure TSpeedBtn.CMEnabledChanged;
begin
  Invalidate;
  inherited;
end;

procedure TSpeedBtn.CMSysColorChange;
begin
  ClearGlyphs;
  Pattern.Free;
  Pattern := nil;
  Invalidate;
end;

procedure TSpeedBtn.ClearGlyphs;
begin
  if FGlyphList <> nil then
  begin
    if FNormalIndex <> -1 then
      TGlyphList(FGlyphList).Delete(FNormalIndex);
    if FDisabledIndex <> -1 then
      TGlyphList(FGlyphList).Delete(FDisabledIndex);
    if TGlyphList(FGlyphList).Count = 0 then
      GlyphCache.ReturnList(FGlyphList);
    FNormalIndex := -1;
    FDisabledIndex := -1;
    FGlyphList := nil;
  end;
end;

procedure TSpeedBtn.CMMouseEnter(var Message: TMessage);
var
  ARect: TRect;
begin
  FOver := TRUE;
  ARect := ClientRect;
  PaintFrame(ARect);
end;

procedure TSpeedBtn.CMMouseLeave(var Message: TMessage);
var
  ARect: TRect;
begin
  FOver := FALSE;
  ARect := ClientRect;
  PaintFrame(ARect);
end;

procedure TSpeedBtn.CMButtonPressed(var Message: TMessage);
var
  Sender: TSpeedBtn;
begin
  if Message.WParam = FGroupIndex then
  begin
    Sender := TSpeedBtn(Message.LParam);
    if (Sender <> Self) and (Sender is TSpeedBtn)
      and Sender.Down then
      SetDown(FALSE);
  end;
end;

procedure TSpeedBtn.ActionChange(Sender: TObject; CheckDefaults: Boolean);

procedure CopyImage(ImageList: TCustomImageList; Index: Integer);
  begin
    with FBitmap do
    begin
      Width := ImageList.Width;
      Height := ImageList.Height;
      Canvas.Brush.Color := clFuchsia; //! for lack of a better color
      Canvas.FillRect(Rect(0, 0, Width, Height));
      ImageList.Draw(Canvas, 0, 0, Index);
    end;
  end;
begin
  inherited ActionChange(Sender, CheckDefaults);
  if Sender is TCustomAction then
    with Sender as TCustomAction do
    begin
      ClearGlyphs;
      if (ActionList <> nil) and (ActionList.Images <> nil)
        and (ImageIndex >= 0) and (ImageIndex < ActionList.Images.Count) then
        CopyImage(ActionList.Images, ImageIndex);
      SetDown(Checked);
    end;
end;

{===============================================================================
| TWStatusBar
+==============================================================================}

constructor TWStatusBar.Create
  (
  AOwner: TComponent
  );
var
  StatusPanel: TStatusPanel;
begin
  inherited Create(AOwner);
  FProgressPanels := TStatusPanels.Create(Self);
  StatusPanel := TStatusPanel.Create(FProgressPanels);
  StatusPanel.Bevel := pbNone;
  StatusPanel := TStatusPanel.Create(FProgressPanels);
  StatusPanel.Width := 30;
  StatusPanel.Bevel := pbNone;
  StatusPanel.Alignment := taRightJustify;
  StatusPanel := TStatusPanel.Create(FProgressPanels);
  StatusPanel.Width := 208;
  StatusPanel.Bevel := pbNone;
  StatusPanel.Style := psOwnerDraw;
  StatusPanel := TStatusPanel.Create(FProgressPanels);
  StatusPanel.Bevel := pbNone;
  OnDrawPanel := DoDrawPanel;
  FCancelText := TRUE;
  FStoredPanels := TStatusPanels.Create(Self);
end;

destructor TWStatusBar.Destroy;
begin
  FProgressPanels.Free;
  FStoredPanels.Free;
  inherited Destroy;
end;

procedure TWStatusBar.SetPanelText
  (
  AIndex: Integer;
  const AText: string
  );
var
  Rect: TRect;
  ARect: TRect;
  BRect: TRect;
begin
    { query the current invalidated area }
  GetUpdateRect(Handle, ARect, FALSE);
    { update the panle-text }
  Panels[AIndex].Text := AText;
    { delphi invalidates the whole statusbar, validate it }
  ValidateRect(Handle, nil);
    { get the rectangle of the statusbar-field }
  SendMessage(Handle, sb_GetRect, AIndex, LongInt(@Rect));
    { union the filed-area with the previous update-area }
  UnionRect(BRect, Rect, ARect);
    { invalidate it }
  InvalidateRect(Handle, @BRect, TRUE);
end;

procedure TWStatusBar.SetProgressPanel
  (
  ASet: Boolean
  );
begin
  if FInProgress <> ASet then
  begin
    SendMessage(Handle, wm_SetRedraw, 0, 0);
    FInProgress := ASet;
    if not ASet then
    begin
      if FDisabled <> nil then
      begin
        EnableTaskWindows(FDisabled);
        FDisabled := nil;
      end;
      Panels := FStoredPanels;
    end
    else
    begin
      FStoredPanels.Assign(Panels);
      Panels := FProgressPanels;
      Panels[0].Text := FProgressText;
      Panels[0].Width := Canvas.TextWidth(Panels[0].Text) + 5;
      Panels[1].Width := Canvas.TextWidth('100%') + 5;
{        if FCancelText then Panels[3].Text:=MlgStringList['StatusBar',1];}
      Progress := 0.0;
      Application.ProcessMessages;
      FDisabled := DisableTaskWindows(Handle);
{        WinGISMainForm.EscPressed:=FALSE;}
    end;
    Resize;
    SendMessage(Handle, wm_SetRedraw, 1, 0);
    InvalidateRect(Handle, nil, TRUE);
    Update;
  end;
end;

procedure TWStatusBar.Resize;
begin
  if FInProgress then
  begin
    Panels[0].Width := Canvas.TextWidth(Panels[0].Text) + 5;
      {Panels[0].Height:=0;}
    Panels[1].Width := Canvas.TextWidth('100%') + 5;
      {Panels[1].Height:=0;}
  end;
  inherited Resize;
end;

procedure TWStatusBar.DoDrawPanel
  (
  StatusBar: TStatusBar;
  Panel: TStatusPanel;
  const Rect: TRect
  );
var
  ARect: TRect;
  AWidth: Integer;
  Cnt: Integer;
begin
  with Canvas do
  begin
    ARect := Rect;
    InflateRect(ARect, 0, -2);
    Frame3D(Canvas, ARect, clBtnShadow, clBtnHighlight, 1);
    InflateRect(ARect, -1, -1);
    AWidth := (ARect.Right - ARect.Left) div 20;
    ARect.Right := ARect.Left + AWidth - 2;
    Brush.Color := clActiveCaption;
    for Cnt := 1 to (FProgress + 4) div 5 do
    begin
      FillRect(ARect);
      OffsetRect(ARect, AWidth, 0);
    end;
    ARect.Right := Rect.Right - 3;
    Brush.Color := Color;
    FillRect(ARect);
  end;
end;

procedure TWStatusBar.SetProgress
  (
  const AProgress: Real
  );
var
  BProgress: Integer;
  Rect: TRect;
begin
  BProgress := Round(AProgress);
  if BProgress <> FProgress then
  begin
    Panels[1].Text := IntToStr(BProgress) + '%';
    ValidateRect(Handle, nil);
    if (FProgress + 4) div 5 <> (BProgress + 4) div 5 then
    begin
      SendMessage(Handle, sb_GetRect, 2, LongInt(@Rect));
      InvalidateRect(Handle, @Rect, TRUE);
{        InflateRect(Rect,-1,-1);
        DoDrawPanel(Self,Panels[2],Rect);}
    end;
    FProgress := BProgress;
    SendMessage(Handle, sb_GetRect, 1, LongInt(@Rect));
    InvalidateRect(Handle, @Rect, FALSE);
    Update;
  end;
end;

procedure TWStatusBar.SetProgressText
  (
  const AText: string
  );
begin
  if FInProgress then
  begin
    FProgressText := AText;
    Panels[0].Text := FProgressText;
    Panels[0].Width := Canvas.TextWidth(Panels[0].Text) + 5;
    Update;
  end;
end;

function TWStatusBar.QueryAbort
  : Boolean;
begin
  Result := FALSE;
  if FInProgress then
  begin
    Application.ProcessMessages;
{      Result:=WinGISMainForm.EscPressed;
      if Result
          and (AbortText<>'') then begin
        Result:=MessageDialog(AbortText,mtConfirmation,[mbYes,mbNo],0)=mrYes;
        if not Result then WinGISMainForm.EscPressed:=FALSE;
      end;                               }
  end;
end;

function AddCursor
  (
  AHandle: THandle
  )
  : TCursor;
begin
  Screen.Cursors[LastUserDefCursor] := AHandle;
  Result := LastUserDefCursor;
  Inc(LastUserDefCursor);
end;

procedure CreateDisabledBitmap
  (
  ABitmap: TBitmap;
  DestImage: TBitmap
  );
const
  ROP_DSPDxax = $00E20746;
var
  IWidth: Integer;
  IHeight: Integer;
  MonoBmp: TBitmap;
  ORect: TRect;
  IRect: TRect;
  HasDisabled: Boolean;
begin
  IWidth := ABitmap.Width;
  IHeight := ABitmap.Height;
  HasDisabled := IWidth > IHeight;
  if HasDisabled then
    IWidth := IWidth div 2;
  IRect := Rect(0, 0, IWidth, IHeight);
  ORect := IRect;
  if HasDisabled then
    OffsetRect(ORect, IWidth, 0);
  DestImage.Width := IWidth;
  DestImage.Height := IHeight;
  MonoBmp := TBitmap.Create;
  try
    if HasDisabled then
      with DestImage.Canvas do
      begin
        DestImage.Canvas.Brush.Color := clBtnFace;
        BrushCopy(IRect, ABitmap, ORect, ABitmap.TransparentColor);
        MonoBmp.Width := IWidth;
        MonoBmp.Height := IHeight;
        MonoBmp.Canvas.CopyRect(IRect, ABitmap.Canvas, ORect);
        MonoBmp.Canvas.Brush.Color := clWhite;
        MonoBmp.Canvas.Font.Color := clBlack;
        MonoBmp.Monochrome := TRUE;
        { replace white-color with clBtnHightlight }
        Brush.Color := clBtnHighLight;
        SetBkColor(Handle, clWhite);
        BitBlt(Handle, 0, 0, IWidth, IHeight, MonoBmp.Canvas.Handle, 0, 0, ROP_DSPDxax);
        MonoBmp.Assign(DestImage);
        MonoBmp.Canvas.Brush.Color := clGray;
        MonoBmp.Monochrome := TRUE;
        { replace gray-color with clBtnShadow }
        Brush.Color := clBtnShadow;
        SetBkColor(Handle, clWhite);
        BitBlt(Handle, 0, 0, IWidth, IHeight, MonoBmp.Canvas.Handle, 0, 0, ROP_DSPDxax);
      end
    else
    begin
      with MonoBmp do
      begin
        Assign(ABitmap);
{$IFDEF WIN32}
        HandleType := bmDDB;
{$ENDIF}
        Canvas.Brush.Color := clBlack;
        Width := IWidth;
        if Monochrome then
        begin
          Canvas.Font.Color := clWhite;
          Monochrome := False;
          Canvas.Brush.Color := clWhite;
        end;
        Monochrome := True;
      end;
      with DestImage.Canvas do
      begin
        Brush.Color := clBtnFace;
        FillRect(IRect);
        Brush.Color := clBtnHighlight;
        SetTextColor(Handle, clBlack);
        SetBkColor(Handle, clWhite);
        BitBlt(Handle, 1, 1, IWidth, IHeight, MonoBmp.Canvas.Handle, 0, 0, ROP_DSPDxax);
        Brush.Color := clBtnShadow;
        SetTextColor(Handle, clBlack);
        SetBkColor(Handle, clWhite);
        BitBlt(Handle, 0, 0, IWidth, IHeight, MonoBmp.Canvas.Handle, 0, 0, ROP_DSPDxax);
      end;
    end;
  finally
    MonoBmp.Free;
  end;
end;

{===============================================================================
| TWListView
+==============================================================================}

{$IFDEF WIN32}

constructor TWListView.Create
  (
  AOwner: TComponent
  );
begin
  inherited Create(AOwner);
  FCanvas := TControlCanvas.Create;
end;

destructor TWListView.Destroy;
begin
  FCanvas.Free;
  inherited Destroy;
end;

procedure TWListView.CreateParams
  (
  var Params: TCreateParams
  );
begin
  inherited CreateParams(Params);
  if FOwnerDraw then
    Params.Style := Params.Style or LVS_OwnerDrawFixed;
end;

procedure TWListView.CNDrawItem
  (
  var Message: TWMDrawItem
  );
var
  State: TOwnerDrawState;
begin
  with Message.DrawItemStruct^ do
  begin
{$IFDEF WIN32}
    State := TOwnerDrawState(LongRec(itemState).Lo);
    FDrawAction := TOwnerDrawAction(WordRec(LongRec(itemAction).Lo).Lo);
{$ELSE}
    State := TOwnerDrawState(WordRec(itemState).Lo);
    FDrawAction := TOwnerDrawAction(WordRec(itemAction).Lo);
{$ENDIF}
    Canvas.Handle := hDC;
    Canvas.Font := Font;
    Canvas.Brush := Brush;
    if odSelected in State then
    begin
      Canvas.Brush.Color := clHighlight;
      Canvas.Font.Color := clHighlightText;
    end;
    if (Integer(itemID) >= 0) and Assigned(FOnDrawItem) then
      OnDrawItem(Self, itemID, rcItem, State)
    else
      Canvas.FillRect(rcItem);
    Canvas.Handle := 0;
  end;
end;

function TWListView.GetItemRect
  (
  Item: Integer;
  SubItem: Integer
  )
  : TRect;
begin
  ListView_GetSubItemRect(Handle, Item, SubItem, LVIR_BOUNDS, @Result);
end;

{$ENDIF}

{===============================================================================
  TWButton
+==============================================================================}

procedure TWButton.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
{$IFDEF WIN32}
  if FFlat then
    Params.Style := Params.Style or bs_Flat;
{$ENDIF}
end;

procedure TWButton.SetFlat(AFlat: Boolean);
begin
  if FFlat <> AFlat then
  begin
    FFlat := AFlat;
    RecreateWnd;
  end;
end;

{==============================================================================+
  TWRadioGroup
+==============================================================================}

procedure TWRadioGroup.Paint;
var
  X: Integer;
  Y: Integer;
  R: TRect;
  S: array[0..255] of Char;
begin
  with Canvas do
    if FBoxStyle = bsBox then
      inherited Paint
    else
      if FBoxStyle <> bsNone then
      begin
        Brush.Color := Color;
        Font := Self.Font;
        R := Rect(0, 0, 0, 0);
        StrPCopy(S, Caption);
        DrawText(Handle, S, Length(Caption), R, DT_LEFT or DT_SINGLELINE or DT_CALCRECT);
        if Enabled then
          DrawText(Handle, S, Length(Caption), R, DT_LEFT or DT_SINGLELINE)
        else
        begin
          Brush.Style := bsClear;
          Font.Color := clBtnHighlight;
          OffsetRect(R, 1, 1);
          DrawText(Handle, S, Length(Caption), R, DT_LEFT or DT_SINGLELINE);
          Font.Color := clBtnShadow;
          OffsetRect(R, -1, -1);
          DrawText(Handle, S, Length(Caption), R, DT_LEFT or DT_SINGLELINE);
        end;
        if R.Right = 0 then
          X := 0
        else
          X := R.Right + 3;
        Y := Abs(Font.Height) div 2 + 1;
        Pen.Color := clBtnShadow;
        MoveTo(X, Y);
        LineTo(ClientWidth, Y);
        Pen.Color := clBtnHighLight;
        MoveTo(X, Y + 1);
        LineTo(ClientWidth, Y + 1);
      end;
end;

procedure TWRadioGroup.SetBoxStyle
  (
  AStyle: TBoxStyle
  );
begin
  if AStyle <> FBoxStyle then
  begin
    FBoxStyle := AStyle;
    Invalidate;
  end;
end;

procedure TextRectEx(Canvas: TCanvas; const Rect: TRect; const Text: string;
  BackgroundColor: TColor; State: TOwnerDrawState);
var
  ARect: TRect;
  Size: TSize;
begin
  Size := Canvas.TextExtent(Text);
  ARect := Classes.Rect(Rect.Left + 2, Rect.Top, Rect.Left + 6 + Size.cx, Rect.Bottom);
  IntersectRect(ARect, ARect, Rect);
  Canvas.TextRect(ARect, ARect.Left + 2, CenterHeightInRect(Size.cy, ARect), Text);
  if odFocused in State then
    Canvas.DrawFocusRect(ARect);
  ARect.Left := ARect.Right;
  ARect.Right := Rect.Right;
  Canvas.Brush.Color := BackgroundColor;
  Canvas.FillRect(ARect);
end;

{==============================================================================+
  TWCustomComboBox
+==============================================================================}

procedure TWCustomCombobox.CNDrawItem(var Message: TWMDrawItem);
var
  State: TOwnerDrawState;
begin
  with Message.DrawItemStruct^ do
  begin
{$IFDEF WIN32}
    State := TOwnerDrawState(LongRec(itemState).Lo);
    FDrawAction := TOwnerDrawAction(WordRec(LongRec(itemAction).Lo).Lo);
{$ELSE}
    State := TOwnerDrawState(WordRec(itemState).Lo);
    FDrawAction := TOwnerDrawAction(WordRec(itemAction).Lo);
{$ENDIF}
    Canvas.Handle := hDC;
    Canvas.Font := Font;
    Canvas.Brush := Brush;
    if itemID mod 1 = 0 then
      Canvas.Brush.Color := Self.Color
    else
      Canvas.Brush.Color := FOddLineColor;
    if odSelected in State then
    begin
      Canvas.Brush.Color := clHighlight;
      Canvas.Font.Color := clHighlightText;
    end;
    if Integer(itemID) >= 0 then
      DrawItem(itemID, rcItem, State)
    else
      Canvas.FillRect(rcItem);
    Canvas.Handle := 0;
  end;
end;

{ TWOpenDialog }

procedure TWOpenDialog.DoShow;
begin
  inherited DoShow;
  if ofReadOnly in Options then {++Sygsky :-}
    SendMessage(GetParent(Handle), CDM_SETCONTROLTEXT, 1040, Integer(FReadOnlyCaption));
end;

{ TWSaveDialog }

procedure TWSaveDialog.DoShow;
begin
  inherited DoShow;
  if ofReadOnly in Options then {++Sygsky :-}
    SendMessage(GetParent(Handle), CDM_SETCONTROLTEXT, 1040, Integer(FReadOnlyCaption));
end;

{==============================================================================+
  Initialisierungs- und Terminierungscode
+==============================================================================}

function TSpeedBtn.GetActionLinkClass: TControlActionLinkClass;
begin
  Result := TSpeedBtnActionLink;
end;

{ TSpeedBtnActionLink }

procedure TSpeedBtnActionLink.AssignClient(AClient: TObject);
begin
  inherited AssignClient(AClient);
  FClient := TSpeedBtn(AClient);
end;

function TSpeedBtnActionLink.IsCheckedLinked: Boolean;
begin
  Result := inherited IsCheckedLinked and
    (FClient.Down = (Action as TCustomAction).Checked);
end;

procedure TSpeedBtnActionLink.SetChecked(Value: Boolean);
begin
  if IsCheckedLinked then
    FClient.Down := Value;
end;

initialization
  Pattern := nil;
  GlyphCache := TGlyphCache.Create;
  { set default dialog-font-name and size }
  DialogFontName := 'MS Sans Serif';
  DialogFontSize := 8;

finalization
  Pattern.Free;
  GlyphCache.Free;

end.

