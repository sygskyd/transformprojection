{******************************************}
{    TPieSeries Editor Dialog              }
{ Copyright (c) 1996-2000 by David Berneda }
{******************************************}
{$I teedefs.inc}
unit TeePieEdit;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls, QComCtrls,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, ComCtrls,
     {$ENDIF}
     Chart, Series, TeCanvas, TeePenDlg, MultiLng;

type
  TPieSeriesEditor = class(TForm)
    CBDark3d: TCheckBox;
    Label4: TLabel;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    CBOther: TComboBox;
    Label6: TLabel;
    EOtherValue: TEdit;
    Label7: TLabel;
    EOtherLabel: TEdit;
    SEExpBig: TEdit;
    UDExpBig: TUpDown;
    CBPatterns: TCheckBox;
    BPen: TButtonPen;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label1: TLabel;
    BShadowColor: TButtonColor;
    Edit3: TEdit;
    UDShadowHoriz: TUpDown;
    Edit4: TEdit;
    UDShadowVert: TUpDown;
    Label2: TLabel;
    Edit1: TEdit;
    UDAngleSize: TUpDown;
    MlgSection1: TMlgSection;
    Label8: TLabel;
    UDRot: TUpDown;
    Edit2: TEdit;
    procedure FormShow(Sender: TObject);
    procedure CBDark3dClick(Sender: TObject);
    procedure CBPatternsClick(Sender: TObject);
    procedure CBOtherChange(Sender: TObject);
    procedure EOtherValueChange(Sender: TObject);
    procedure EOtherLabelChange(Sender: TObject);
    procedure SEExpBigChange(Sender: TObject);
    procedure Edit3Change(Sender: TObject);
    procedure Edit4Change(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
  private
    { Private declarations }
    Pie : TPieSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeeConst, TeEngine, TeeProcs, TeeCircledEdit, TeeEdiSeri;

procedure TPieSeriesEditor.FormShow(Sender: TObject);
begin
  Pie:=TPieSeries(Tag);
  With Pie do
  begin
    UDExpBig.Position  :=ExplodeBiggest;
    CBDark3D.Checked   :=Dark3D;
//    CBDark3D.Enabled   :=ParentChart.View3D;
    CBPatterns.Checked :=UsePatterns;
    With OtherSlice do
    begin
      EOtherLabel.Text:=Text;
      EOtherValue.Text:=FloatToStr(Value);
      CBOther.ItemIndex:=Ord(Style);
      EnableControls(Style<>poNone,[EOtherLabel,EOtherValue]);
    end;
    UDShadowHoriz.Position:=Shadow.HorizSize;
    UDShadowVert.Position:=Shadow.VertSize;
    UDAngleSize.Position:=AngleSize;
    UDRot.Position:=RotationAngle;
    BPen.LinkPen(PiePen);
    BShadowColor.LinkProperty(Shadow,'Color');
  end;
{  TFormTeeSeries(Parent.Owner).InsertSeriesForm( TCircledSeriesEditor,
                                                 1,TeeMsg_GalleryCircled,
                                                 Pie);}
end;

procedure TPieSeriesEditor.CBDark3DClick(Sender: TObject);
begin
  if Showing then Pie.Dark3D:=CBDark3D.Checked;
end;

procedure TPieSeriesEditor.CBPatternsClick(Sender: TObject);
begin
  Pie.UsePatterns:=CBPatterns.Checked;
end;

procedure TPieSeriesEditor.CBOtherChange(Sender: TObject);
begin
  With Pie.OtherSlice do
  begin
    Style:=TPieOtherStyle(CBOther.ItemIndex);
    EnableControls(Style<>poNone,[EOtherLabel,EOtherValue]);
  end;
end;

procedure TPieSeriesEditor.EOtherValueChange(Sender: TObject);
begin
  if EOtherValue.Text<>'' then
     Pie.OtherSlice.Value:=StrToFloat(EOtherValue.Text);
end;

procedure TPieSeriesEditor.EOtherLabelChange(Sender: TObject);
begin
  Pie.OtherSlice.Text:=EOtherLabel.Text;
end;

procedure TPieSeriesEditor.SEExpBigChange(Sender: TObject);
begin
  if Showing then Pie.ExplodeBiggest:=UDExpBig.Position;
end;

procedure TPieSeriesEditor.Edit3Change(Sender: TObject);
begin
  if Showing then Pie.Shadow.HorizSize:=UDShadowHoriz.Position;
end;

procedure TPieSeriesEditor.Edit4Change(Sender: TObject);
begin
  if Showing then Pie.Shadow.VertSize:=UDShadowVert.Position;
end;

procedure TPieSeriesEditor.Edit1Change(Sender: TObject);
begin
  if Showing then Pie.AngleSize:=UDAngleSize.Position;
end;

procedure TPieSeriesEditor.Edit2Change(Sender: TObject);
begin
   if Showing then Pie.RotationAngle:=UDRot.Position;
end;

initialization
  RegisterClass(TPieSeriesEditor);
end.
