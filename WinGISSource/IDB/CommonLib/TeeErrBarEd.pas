{**********************************************}
{   TErrorBarSeries Component Editor Dialog    }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeErrBarEd;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, ExtCtrls, Chart, Series, ErrorBar, ComCtrls,
  TeCanvas, TeePenDlg;

type
  TErrorSeriesEditor = class(TForm)
    SEBarwidth: TEdit;
    Label1: TLabel;
    BPen: TButtonPen;
    RGWidthUnit: TRadioGroup;
    UDBarWidth: TUpDown;
    RGStyle: TRadioGroup;
    CBColorEach: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure SEBarwidthChange(Sender: TObject);
    procedure BPenClick(Sender: TObject);
    procedure RGWidthUnitClick(Sender: TObject);
    procedure RGStyleClick(Sender: TObject);
    procedure CBColorEachClick(Sender: TObject);
  private
    { Private declarations }
    ErrorSeries  : TCustomErrorSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeeBrushDlg, TeeBarEdit, TeeConst, TeeEdiSeri;

procedure TErrorSeriesEditor.FormShow(Sender: TObject);
begin
  ErrorSeries:=TCustomErrorSeries(Tag);
  With ErrorSeries do
  begin
    UDBarWidth.Position:=ErrorWidth;
    if ErrorWidthUnits=ewuPercent then RGWidthUnit.ItemIndex:=0
                                  else RGWidthUnit.ItemIndex:=1;
    RGStyle.Visible:=ErrorSeries is TErrorSeries;
    if RGStyle.Visible then RGStyle.ItemIndex:=Ord(ErrorStyle);
    CBColorEach.Checked:=ColorEachPoint;
    BPen.LinkPen(ErrorPen);
  end;
  if ErrorSeries is TErrorBarSeries then
     TFormTeeSeries(Parent.Owner).InsertSeriesForm( TBarSeriesEditor,
                                                    1,TeeMsg_GalleryBar,
                                                    ErrorSeries);
end;

procedure TErrorSeriesEditor.SEBarwidthChange(Sender: TObject);
begin
  if Showing then ErrorSeries.ErrorWidth:=UDBarWidth.Position;
end;

procedure TErrorSeriesEditor.BPenClick(Sender: TObject);
begin
  With ErrorSeries do
  if not (ErrorSeries is TErrorBarSeries) then SeriesColor:=ErrorPen.Color;
end;

procedure TErrorSeriesEditor.RGWidthUnitClick(Sender: TObject);
begin
  ErrorSeries.ErrorWidthUnits:=TErrorWidthUnits(RGWidthUnit.ItemIndex);
end;

procedure TErrorSeriesEditor.RGStyleClick(Sender: TObject);
begin
  if Showing then ErrorSeries.ErrorStyle:=TErrorSeriesStyle(RGStyle.ItemIndex);
end;

procedure TErrorSeriesEditor.CBColorEachClick(Sender: TObject);
begin
  ErrorSeries.ColorEachPoint:=CBColorEach.Checked;
end;

initialization
  RegisterClass(TErrorSeriesEditor);
end.
