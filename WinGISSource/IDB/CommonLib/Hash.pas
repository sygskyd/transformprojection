{*******************************************************************************
| Unit Hash
|-------------------------------------------------------------------------------
| Implementation einer Hashtabelle.
|-------------------------------------------------------------------------------
| Autor: Martin Forst, 04.09.1995
|-------------------------------------------------------------------------------
| Modifikationen:
+******************************************************************************}
Unit Hash;

Interface

Uses Classes,WinTypes
     {++ Ivanoff BUG#NEW14 BUILD#100}
     , Dialogs
     {-- Ivanoff}
     ;

Const { Schwellwerte f�r TAutoHashTable                                        }
      ExpandLevel       = 0.8;         { Belegungsschwelle f�r Vergr��erung    }
      ExpandTo          = 0.5;

Type {*************************************************************************+
       Class THashTable
     ---------------------------------------------------------------------------
       Die Hashtabelle verwendet doppeltes Hashing zur Zuweisung eines
       Eintrages zu einer Arrayposition. Die maximale Anzahl der Eintr�ge
       wird im Konstruktor angegeben, kann aber durch die Zuweisung eines
       neuen Wertes an die Eigenschaft Capacity ge�ndert werden (die Eintr�ge
       bleiben erhalten). Es sollte nicht mehr als etwa 80% der maximalen
       Gr��e belegt werden. Um die Hashtabelle verwenden zu k�nnen mu� ein
       neuer Typ abgeleitet und die Funktionen OnGetHashValue und
       OnCompareItems definiert werden. OnGetHashValue mu� f�r den angegebenen
       Eintrag einen Long-Wert berechnen. Mit diesem wird dann mit der
       Hashfunktion die Arrayposition des Eintrags berechnet. OnCompareItem
       wird aufgerufen, wenn ein Eintrag gesucht wird. Die Funktion mu�
       TRUE zur�ckgeben, wenn die beiden angegebenen Eintr�ge gleich sind.
       Wenn die Hashtabelle freigegeben wird, so werden alle noch vorhandenen
       Eintr�ge freigegeben. Dazu wird TObject.Free aufgerufen. Sind die
       Eintr�ge nicht von TObject abgeleitet, so mu� die Methode OnFreeItem
       �berschreiben werden.
     ---------------------------------------------------------------------------
       FCapacity        = maximale Anzahl von Eintr�gen
       FCount           = Anzahl eingef�gter Eintr�ge
       FItems           = Liste mit Pointern auf die Eintr�ge (NIL=unbenutzer
                          Eintrag, MarkedAsFree=gel�schter Eintrag)
       FSecodary        = Modulowert f�r die zweite Hashfunktion (FCapacity/4)
     +*************************************************************************}
     THashTable         = Class(TPersistent) 
      Private                              
       Function    GetItem(AIndex: Integer): Pointer;
       Function    GetPrimaryHashIndex(Item: Pointer): LongInt;
       Function    GetSecondaryHashIndex(Item: Pointer): LongInt;
       Procedure   SetCapacity(ACapacity: Integer);
       Procedure   SetCount(ACount: Integer); virtual;
       Function    SearchItem(Key: Pointer; var Found: Boolean): LongInt;
      Protected
       FCapacity        : Integer;
       FCount           : Integer;
       FItems           : PPointerList;
       FSecondary       : LongInt;
       Procedure   AssignTo(Dest: TPersistent); override;
       Function    KeyOf(Item: Pointer): Pointer; virtual;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
       Function    KeyOfValue(AItem: Pointer): LongInt; virtual; abstract;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
       Function    OnCompareItems(Item1,Item2: Pointer): Boolean; virtual; abstract;
       Function    OnCopyItem(Item: Pointer): Pointer; virtual;
       Procedure   OnClearItem(Item: Pointer); virtual;
       Procedure   OnFreeItem(Item: Pointer); virtual;
       Function    OnGetHashValue(Item: Pointer): LongInt; virtual; abstract;
      Public
       Constructor Create(ACapacity: Integer);
       Destructor  Destroy; override;
       Function    Add(Item: Pointer): Boolean; virtual;
       Property    Capacity: Integer read FCapacity write SetCapacity;
       Procedure   Clear(FreeItems: Boolean=FALSE);
       Property    Count: Integer read FCount write SetCount;
       Function    Delete(Item: Pointer): Boolean;
       Function    Find(Key: Pointer): Pointer;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
       Function    FindByIndex(AIndex: LongInt; AValue: LongInt): Pointer;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
       Property    Items[AIndex: Integer]: Pointer read GetItem;
       Function    Used(Item: Pointer): Boolean;
     end;

     TAutoHashTable     = Class(THashTable)
      Private
       Procedure   SetCount(ACount: Integer); override;
      Public
     end;

     {*************************************************************************+
       Class TRecHashTable
     ---------------------------------------------------------------------------
       Die Hashtabelle verwendet doppeltes Hashing zur Zuweisung eines
       Eintrages zu einer Arrayposition. Die maximale Anzahl der Eintr�ge
       wird im Konstruktor angegeben, kann aber durch die Zuweisung eines
       neuen Wertes an die Eigenschaft Capacity ge�ndert werden (die Eintr�ge
       bleiben erhalten). Es sollte nicht mehr als etwa 80% der maximalen
       Gr��e belegt werden. In die Hashtabelle k�nnen Records beliebiger Gr��e
       eingef�gt werden. Die Gr��e des Records wird im Constructor angegeben.
       Als erste Variable im Record mu� ein LongInt Wert stehen, der als
       Hashwert verwendet wird.
     ---------------------------------------------------------------------------
       FCapacity        = maximale Anzahl von Eintr�gen
       FCount           = Anzahl eingef�gter Eintr�ge
       FItems           = Liste mit den Records in der Hashtabelle. Ist der
                          Hashwert 0 ist der Eintrag frei, -1 kennzeichnet
                          einen gel�schten Eintrag.
       FItemSize        = Gr��e der Records in Bytes
       FSecodary        = Modulowert f�r die zweite Hashfunktion (FCapacity/4)
     +*************************************************************************}
     TRecHashTable      = Class(TPersistent)  
      Private
       FCapacity        : Integer;
       FCount           : Integer;
       FItems           : PChar;
       FItemSize        : Integer;
       FSecondary       : LongInt;
       Function    GetItem(AIndex:Integer):PLongInt;
       Function    GetAItem(AIndex:Integer):PLongInt;
       Function    GetPrimaryHashIndex(Item:LongInt):LongInt;
       Function    GetSecondaryHashIndex(Item:LongInt):LongInt;
       Function    SearchItem(Key:LongInt;var Found:Boolean):LongInt;
       Procedure   SetCapacity(ACapacity:Integer);
       Procedure   SetCount(ACount:Integer); virtual;
       Property    AItems[AIndex:Integer]:PLongInt read GetAItem;
      Public
       Constructor Create(ACapacity:Integer;AItemSize:Integer);
       Destructor  Destroy; override;
       Function    Add(var Item):Integer;
       Property    Capacity:Integer read FCapacity write SetCapacity;
       Procedure   Clear;
       Property    Count:Integer read FCount write SetCount;
       Function    Delete(Item:LongInt):Boolean;
       Function    Find(Key:LongInt):Pointer;
       Property    Items[AIndex:Integer]:PLongInt read GetItem;
       Procedure   OnFreeItem(Item:Pointer); virtual;
     end;

     TAutoRecHashTable  = Class(TRecHashTable)
      Private
       Procedure   SetCount(ACount:Integer); override;
      Public
     end;

Function String2HashIndex(WorkStr:String):LongInt;

Const MarkedAsFree      = Pointer(-1);

Implementation

Uses SysUtils;

{$I Primes.INC}

Function GetNextPrime(AValue: Integer): Integer;
var Left         : Integer;
    Right        : Integer;
    Position     : Integer;
    AIndex       : Integer;
begin
   Left:=Low(Primes);
   Right:=High(Primes);

   repeat
      Position:=(Right+Left) Div 2;
      AIndex:=AValue-Primes[Position];
      if AIndex<0 then
         Right:=Position-1
      else
         if AIndex=0 then
            Break
         else
            Left:=Position+1;
    until Right<Left;
    
    if AIndex>0 then
       Inc(Position);
    Result:=Primes[Position];
end;

{==============================================================================+
  THashTable
+==============================================================================}

{******************************************************************************+
  Constructor THashTable.Create
--------------------------------------------------------------------------------
  Erzeugt eine neue Hashtabelle mit der angegebenen Maximalgr��e.
+******************************************************************************}
Constructor THashTable.Create(ACapacity:Integer);
begin
   inherited Create;
   Capacity:=ACapacity;
end;

{******************************************************************************+
  Destructor THashTable.Destroy
--------------------------------------------------------------------------------
  Gibt den Speicher der Hashtabelle frei. F�r alle vorhandenen Eintr�ge wird
  die Funktion FreeItem aufgerufen.
+******************************************************************************}
Destructor THashTable.Destroy;
var Cnt          : Integer;
    Index        : Integer;
    Item         : Pointer;
begin
   { sofern Eintr�ge vorhanden wird OnClearItem f�r jeden aufrufen            }
   Cnt:=0;
   Index:=0;
   while Cnt<Count do
      begin
         Item:=FItems^[Index];
         if Used(Item) then
            begin
               OnFreeItem(Item);
               FItems^[Index]:=MarkedAsFree;
               Inc(Cnt);
            end;
         Inc(Index);
      end;
   if FCapacity>0 then
      FreeMem(FItems,FCapacity*SizeOf(Pointer)); { Speicher freigeben         }
   inherited Destroy;
end;

{******************************************************************************+
  Procedure THashTable.SetCapacity
--------------------------------------------------------------------------------
  �ndert die maximale Anzahl der Eintr�ge. Enth�lt die Hashtabelle bereits
  Eintr�ge, so werden diese wieder in die Hashtabelle eingef�gt. Die maximale
  Gr��e wird auf die ACapacity n�chstliegende Primzahl gesetzt.
+******************************************************************************}
Procedure THashTable.SetCapacity(ACapacity: Integer);
var HelpPtr      : PPointerList;
    CapacitySave : Integer;
    Cnt          : Integer;
begin
   ACapacity:=GetNextPrime(ACapacity);
   if FCapacity<ACapacity then
      begin                                      { �ndert sich die Gr��e?      }
         CapacitySave:=FCapacity;
         if FCapacity=0 then
            begin
               { noch keine Eintr�ge->Speicher reservieren und Null setzen     }
               GetMem(FItems,ACapacity*SizeOf(Pointer));
               FillChar(FItems^,ACapacity*SizeOf(Pointer),#0);
               FCapacity:=ACapacity;
               FSecondary:=Round(FCapacity/3.8); { Modulowert f�r zweite HashFn}
            end
         else
            begin
               { Eintr�ge sichern und neuen Speicher anfordern                 }
               HelpPtr:=FItems;
               GetMem(FItems,ACapacity*SizeOf(Pointer));
               FillChar(FItems^,ACapacity*SizeOf(Pointer),#0);
               FCapacity:=ACapacity;
               FSecondary:=Round(FCapacity/3.8); { Modulowert f�r zweite HashFn}
               { alte Eintr�ge wieder einf�gen                                 }
               for Cnt:=0 to CapacitySave-1 do
                   if (HelpPtr^[Cnt]<>NIL) and (HelpPtr^[Cnt]<>MarkedAsFree) then
                      begin
                         Dec(FCount);
                         Add(HelpPtr^[Cnt]);
                      end;
               { alten Speicher freigeben                                      }
               FreeMem(HelpPtr,CapacitySave*SizeOf(Pointer));
            end;
      end;
end;

{******************************************************************************+
  Procedure THashTable.Clear
-------------------------------------------------------------------------------
  Removes all items from the hash-table. If FreeItems is set to true
  OnFreeItem is called for each item, else OnClearItem is called.
+******************************************************************************}
Procedure THashTable.Clear(FreeItems:Boolean);
var Cnt          : Integer;
    Index        : Integer;
    Item         : Pointer;
begin
   { sofern Eintr�ge vorhanden wird OnClearItem f�r jeden aufrufen            }
   Cnt:=0;
   Index:=0;
   while Cnt<Count do
      begin
         Item:=Items[Index];
         if Item<>NIL then
            begin
               if FreeItems then
                  OnFreeItem(Item)
               else
                  OnClearItem(Item);
               Inc(Cnt);
            end;
         Inc(Index);
      end;
   { alle Eintr�ge unbelegt                                                   }
   FillChar(FItems^,FCapacity*SizeOf(Pointer),#0);
   Count:=0;                          { Hashtabelle hat keine Eintr�ge mehr   }
end;

Procedure THashTable.OnClearItem(Item: Pointer);
begin
   OnFreeItem(Item);
end;

{******************************************************************************+
 Function THashTable.Delete
--------------------------------------------------------------------------------
  L�scht einen Eintrag aus der Hashtabelle. Die Arrayposition an der sich
  der Eintrag befand wird als verf�gbar gekennzeichnet (MarkedAsFree). Der
  Speicher des Eintrag wird nicht freigegeben. Delete liefert FALSE, wenn der
  zu l�schende Eintrag nicht gefunden wurde.
{******************************************************************************}
Function THashTable.Delete(Item: Pointer): Boolean;
var Position     : Integer;
    Found        : Boolean;
begin
   Position:=SearchItem(KeyOf(Item),Found); { Eintrag suchen                   }
   if not Found then
      Result:=FALSE                         { nicht gefunden->FALSE            }
   else
      begin
         FItems^[Position]:=MarkedAsFree;   { als verf�gbar kennzeichnen       }
         Count:=Count-1;                    { ein Eintrag weniger              }
         Result:=TRUE;
      end;
   {$IFDEF DEBUG}
   if not Result then
      Raise Exception.Create('THashTable.Delete: Item not found');
   {$ENDIF}
end;                              

{******************************************************************************+
  Function THashTable.Insert                                                   
--------------------------------------------------------------------------------
  F�gt einen neuen Eintrag in die Hashtabelle ein. Der Eintrag wird nicht
  eingef�gt, wenn er bereits vorhanden ist. In diesem Fall liefert Insert
  FALSE.
+******************************************************************************}
Function THashTable.Add(Item: Pointer): Boolean;
var Position     : Integer;
    InsertPos    : Integer;
    Increment    : Integer;
    StartPos     : Integer;
    Key          : Pointer;
begin
   InsertPos:=-1;
   Key:=KeyOf(Item);
   Position:=GetPrimaryHashIndex(Key);          { Prim�re Position berechnen  }
   {$IFDEF DEBUG}
   if Position<0 then
      Raise Exception.Create('THashTable.Add: Negative Hash-Keys not allowed');
   {$ENDIF}
   StartPos:=Position;
   if FItems^[Position]<>NIL then
      begin
         Increment:=GetSecondaryHashIndex(Key);     { Inkrement berechnen     }
         {$IFDEF DEBUG}
         if Increment<0 then
            Raise Exception.Create('THashTable.Add: Negative Hash-Keys not allowed');
         {$ENDIF}
         while FItems^[Position]<>NIL do
            begin
               if FItems^[Position]=MarkedAsFree then
                  begin
                     { als freie markierten Eintrag gefunden->merken und weitersuchen, ob }
                     { Eintrag nacher vorhanden                                           }
                     if InsertPos=-1 then
                        InsertPos:=Position;
                  end
               else
                  if OnCompareItems(KeyOf(Item),KeyOf(FItems^[Position])) then
                     begin
                        {$IFDEF DEBUG}
{++ Ivanoff BUG#NEW14 BUILD#100}
//                      Raise Exception.Create('THashtable.Insert: Item already exists');
                        ShowMessage('THashtable.Insert: Item already exists');
{-- Ivanoff}
                        {$ENDIF}
                        Result:=FALSE;
                        Exit;
                     end;
               { Position um Inkrement erh�hen                                }
               Position:=(Position+Increment) Mod Capacity;
               if StartPos=Position then         { wenn einmal durch->abbrechen}
                  Break;
            end;
      end;
   { frei markierte Position bevorzugen, sonst freien Eintrag �berschreiben   }
   {$IFDEF DEBUG}
   if (FItems^[Position]<>NIL) and (InsertPos=-1) then
      Raise Exception.Create('THashTable.Add: No insertposition found');
   {$ENDIF}
   if InsertPos=-1 then
      InsertPos:=Position;
   FItems^[InsertPos]:=Item;
   Count:=Count+1;                              { ein Eintrag mehr            }
   Result:=TRUE;
end;

{******************************************************************************+
  Function THashTable.SearchItem
--------------------------------------------------------------------------------
  Durchsucht die Hashtabelle nach einem Eintrag. Wurde der Eintrag gefunde,
  so ist Found TRUE. In diesen Fall gibt der Funktionswert die Position des
  Eintrags an. Wurde der Eintrag nicht gefunden, so ist Found FALSE und
  der Funktionswert gibt die Position an, an der der Eintrag eingef�gt w�rde.
+******************************************************************************}
Function THashTable.SearchItem(Key: Pointer; var Found: Boolean): LongInt;
var Position     : Integer;
    Increment    : Integer;
    StartPos     : Integer;
begin
   Position:=GetPrimaryHashIndex(Key);          { Prim�rposition berechnen    }
   Increment:=GetSecondaryHashIndex(Key);       { Inkrement berechnen         }
   {$IFDEF DEBUG}
   if Position<0 then
      Raise Exception.Create('THashTable.SearchItem: Negative Hash-Keys not allowed');
   if Increment<0 then
      Raise Exception.Create('THashTable.SearchItem: Negative Hash-Keys not allowed');
   {$ENDIF}
   StartPos:=Position;
   while FItems^[Position]<>NIL do
      begin
         if (FItems^[Position]<>MarkedAsFree)   { frei markiert->weitersuchen }
            and OnCompareItems(Key,KeyOf(FItems^[Position])) then
            begin
               Result:=Position;                { gefunden->TRUE und Ende     }
               Found:=TRUE;
               Exit;
            end;
         { n�chste Position berechnen                                         }
         Position:=(Position+Increment) Mod Capacity;
         if Position=StartPos then
            Break;
      end;
   Result:=Position;                            { nicht gefungen->FALSE       }
   Found:=FALSE;
end;

{******************************************************************************+
  Function THashTable.Find
--------------------------------------------------------------------------------
  Sucht einen Eintrag in der Hashtabelle. Wird der Eintrag gefunden, so
  liefert Find den in der Hashtabelle gespeicherten Zeiger, sonst
  liefert Find NIL.
+******************************************************************************}
Function THashTable.Find(Key: Pointer): Pointer;
var Found        : Boolean;
    Position     : Integer;
begin
   Position:=SearchItem(Key,Found);
   if not Found then
      Result:=NIL
   else
      Result:=FItems^[Position];
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{******************************************************************************}
{------------------------------------------------------------------------------}
{******************************************************************************}
Function THashTable.FindByIndex(AIndex: LongInt; AValue: LongInt): Pointer;
var Position     : Integer;
    Increment    : Integer;
    StartPos     : Integer;
begin
   Result:=NIL;
   Position:=AIndex Mod Capacity;                 { Calculate Primary Hash Index   }
   Increment:=FSecondary-(AIndex Mod FSecondary); { Calculate Secondary Hash Index }
   StartPos:=Position;
   while FItems^[Position] <> NIL do
      if (FItems[Position] <> nil ) and (FItems^[Position] <> MarkedAsFree) and (KeyOfValue(FItems^[Position]) = AValue) then
         begin
            Result:=FItems^[Position];
            Break;
         end
      else
         begin
            Position:=(Position+Increment) Mod Capacity;
            if Position=StartPos then
               Break;
         end;
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{******************************************************************************+
  Function THashTable.GetPrimaryHashIndex
--------------------------------------------------------------------------------
  Ermittelt zu einem Eintrag die Hashvalue und berechnet daraus die Position
  des Eintrag in der Hashtabelle.
+******************************************************************************}
Function THashTable.GetPrimaryHashIndex(Item: Pointer): LongInt;
begin
   Result:=OnGetHashValue(Item) Mod Capacity;
end;

Function THashTable.Used(Item: Pointer): Boolean;
begin
   Result:=(Item<>NIL) and (Item<>MarkedAsFree);
end;

{******************************************************************************+
  Function THashTable.GetSecondaryHashIndex
--------------------------------------------------------------------------------
  Berechnet den Wert um den die Eintragsposition im Falle einer Hashkollision
  erh�ht wird.
+******************************************************************************}
Function THashTable.GetSecondaryHashIndex(Item: Pointer): LongInt;
begin
   Result:=FSecondary-(OnGetHashValue(Item) Mod FSecondary);
end;

{*******************************************************************************
| Function THashTable.OnFreeItem
|-------------------------------------------------------------------------------
| Gibt den Speicher eines Eintrags frei. OnFreeItem geht davon aus, das der
| Eintrag von TObject abgeleitet ist und ruft dessen Destruktor auf.
+******************************************************************************}
Procedure THashTable.OnFreeItem(Item: Pointer);
begin
   TObject(Item).Free;
end;

Function THashTable.GetItem(AIndex: Integer): Pointer;
begin
   {$IFDEF DEBUG}
   if AIndex>=FCapacity then
      Raise Exception.Create('THashTable.GetItem: Index out of range');
   {$ENDIF}
   if FItems^[AIndex]=MarkedAsFree then
      Result:=NIL
   else
      Result:=FItems^[AIndex];
end;

Function THashTable.KeyOf(Item: Pointer): Pointer;
begin
   Result:=Item;
end;

Procedure THashTable.SetCount(ACount: Integer);
begin
   FCount:=ACount;
end;

Procedure THashTable.AssignTo(Dest: TPersistent);
var Item         : Pointer;
    NewItem      : Pointer;
    Cnt          : Integer;
begin
   with Dest as THashTable do
      begin
         Clear;
         SetCapacity(Self.FCapacity);
         for Cnt:=0 to Self.Capacity-1 do
             begin
                Item:=Self.FItems^[Cnt];
                if Self.Used(Item) then
                   begin
                      NewItem:=OnCopyItem(Item);
                      if NewItem<>NIL then
                         Add(NewItem);
                   end;
             end;
      end;
end;

{===============================================================================
| TRecHashTable
+==============================================================================}

{*******************************************************************************
| Constructor TRecHashTable.Create
|-------------------------------------------------------------------------------
| Erzeugt eine neue Hashtabelle mit der angegebenen Maximalgr��e.              
+******************************************************************************}
Constructor TRecHashTable.Create
   (
   ACapacity       : Integer;
   AItemSize       : Integer
   );
  begin
    inherited Create;
    FItemSize:=AItemSize;
    SetCapacity(ACapacity);
  end;

{******************************************************************************+
  Destructor TRecHashTable.Destroy
--------------------------------------------------------------------------------
  Gibt den Speicher der Hashtabelle frei. F�r alle vorhandenen Eintr�ge wird
  die Funktion FreeItem aufgerufen.
+******************************************************************************}
Destructor TRecHashTable.Destroy;
  var Cnt          : Integer;
      Index        : Integer;
      Item         : Pointer;
  begin
    Cnt:=0;
    Index:=0;
    while Cnt<Count do begin
      Item:=Items[Index];
      if Item<>NIL then begin
        OnFreeItem(Item);
        Inc(Cnt);
      end;
      Inc(Index);
    end;
    if FCapacity<>0 then
      FreeMem(FItems,FCapacity*FItemSize);       { Speicher freigeben          }
    inherited Destroy;
  end;

{******************************************************************************+
  Procedure TRecHashTable.SetCapacity
--------------------------------------------------------------------------------
  �ndert die maximale Anzahl der Eintr�ge. Enth�lt die Hashtabelle bereits
  Eintr�ge, so werden diese wieder in die Hashtabelle eingef�gt. Die maximale
  Gr��e wird auf die ACapacity n�chstliegende Primzahl gesetzt.
+******************************************************************************}
Procedure TRecHashTable.SetCapacity(ACapacity: Integer);
var HelpPtr      : PChar;
    CapacitySave : Integer;
    Cnt          : Integer;
begin
   ACapacity:=GetNextPrime(ACapacity);
   if FCapacity<ACapacity then              { �ndert sich die Gr��e?        }
      if FCapacity=0 then
         begin
            { noch keine Eintr�ge->Speicher reservieren und Null setzen     }
            GetMem(FItems,ACapacity*FItemSize);
            for Cnt:=0 to ACapacity-1 do
                PLongInt(FItems+Cnt*FItemSize)^:=High(LongInt);
            FCapacity:=ACapacity;
            FSecondary:=Round(FCapacity/3.8);{ Modulowert f�r zweite HashFn }
         end
      else
         begin
            { Eintr�ge sichern und neuen Speicher anfordern                 }
            HelpPtr:=FItems;
            try
               GetMem(FItems,ACapacity*FItemSize);
            except
               FItems:=HelpPtr;                  { Fehler->r�ckg�ngigmachen }
            Raise;
            end;
            for Cnt:=0 to ACapacity-1 do
                PLongInt(FItems+Cnt*FItemSize)^:=High(LongInt);
            CapacitySave:=FCapacity;
            FCapacity:=ACapacity;
            FSecondary:=Round(FCapacity/3.8);{ Modulowert f�r zweite HashFn }
            { alte Eintr�ge wieder einf�gen                                 }
            for Cnt:=0 to CapacitySave-1 do
                if (PLongInt(HelpPtr+Cnt*FItemSize)^<>High(LongInt))
                   and (PLongInt(HelpPtr+Cnt*FItemSize)^<>Low(LongInt)) then
                   begin
                      Dec(FCount);
                      Add(HelpPtr[Cnt*FItemSize]);
                   end;
            { alten Speicher freigeben                                      }
            FreeMem(HelpPtr,CapacitySave*FItemSize);
         end;
end;

{******************************************************************************+
  Procedure TRecHashTable.Clear
--------------------------------------------------------------------------------
  Entfernt alle Eintr�ge in der HashTabelle. F�r jeden Eintrag wird
  OnFreeItem aufgerufen.
+******************************************************************************}
Procedure TRecHashTable.Clear;
  var Cnt          : Integer;
  begin
    { alle Eintr�ge unbelegt                                                   }
    for Cnt:=0 to Capacity-1 do AItems[Cnt]^:=High(LongInt);
    Count:=0;                          { Hashtabelle hat keine Eintr�ge mehr   }
  end;

{******************************************************************************+
  Function TRecHashTable.Delete
--------------------------------------------------------------------------------
  L�scht einen Eintrag aus der Hashtabelle. Die Arrayposition an der sich
  der Eintrag befand wird als verf�gbar gekennzeichnet (MarkedAsFree). Der
  Speicher des Eintrag wird nicht freigegeben. Delete liefert FALSE, wenn der
  zu l�schende Eintrag nicht gefunden wurde.
+******************************************************************************}
Function TRecHashTable.Delete
   (
   Item            : LongInt
   )
   : Boolean;
  var Position     : Integer;
      Found        : Boolean;
  begin
    Position:=SearchItem(Item,Found);       { Eintrag suchen                   }
    if not Found then Result:=FALSE         { nicht gefunden->FALSE            }
    else begin
      AItems[Position]^:=Low(LongInt);      { als verf�gbar kennzeichnen       }
      Count:=Count-1;                       { ein Eintrag weniger              }
      Result:=TRUE;
    end;
  end;

{******************************************************************************+
  Function TRecHashTable.Add
--------------------------------------------------------------------------------
  F�gt einen neuen Eintrag in die Hashtabelle ein. Der Eintrag wird nicht
  eingef�gt, wenn er bereits vorhanden ist. In diesem Fall liefert Insert
  FALSE.
+******************************************************************************}
Function TRecHashTable.Add
   (
   var Item
   )
   : Integer;
  var Position     : Integer;
      InsertPos    : Integer;
      Increment    : Integer;
      StartPos     : Integer;
  begin
    InsertPos:=-1;
    Position:=GetPrimaryHashIndex(LongInt(Item));      { Position berechnen    }
    Increment:=GetSecondaryHashIndex(LongInt(Item));   { Inkrement berechnen   }
    {$IFDEF DEBUG}
    if Position<0 then Raise Exception.Create('THashTable.Add: Negative Hash-Keys not allowed');
    if Increment<0 then Raise Exception.Create('THashTable.Add: Negative Hash-Keys not allowed');
    {$ENDIF}
    StartPos:=Position;
    while AItems[Position]^<>High(LongInt) do begin
      if AItems[Position]^=Low(LongInt) then begin
        { als freie markierten Eintrag gefunden->merken und weitersuchen, ob   }
        { Eintrag nacher vorhanden                                             }
        if InsertPos=-1 then InsertPos:=Position;
      end
      else if LongInt(Item)=AItems[Position]^ then begin
        { der Eintrag existiert bereits->FALSE returnieren                     }
        Result:=-1;
        Exit;
      end;
      { Position um Inkrement erh�hen                                          }
      Position:=(Position+Increment) Mod Capacity;
      if Position=StartPos then Break;
    end;
    { frei markierte Position bevorzugen, sonst freien Eintrag �berschreiben   }
    {$IFDEF DEBUG}
    if (AItems[Position]^<>High(LongInt))
        and (InsertPos=-1) then Raise Exception.Create('TRecHashTable.Add: No insertposition found');
    if InsertPos>=FCapacity then Raise Exception.Create('TRecHashTable.Add: Index out of range');
    if Position>=FCapacity then Raise Exception.Create('TRecHashTable.Add: Index out of range');
    {$ENDIF}
    if InsertPos=-1 then InsertPos:=Position;
    Move(Item,AItems[InsertPos]^,FItemSize);
    Count:=Count+1;                              { ein eintrag mehr            }
    Result:=InsertPos;
  end;

{******************************************************************************+
  Function TRecHashTable.SearchItem
--------------------------------------------------------------------------------
  Durchsucht die Hashtabelle nach einem Eintrag. Wurde der Eintrag gefunde,
  so ist Found TRUE. In diesen Fall gibt der Funktionswert die Position des
  Eintrags an. Wurde der Eintrag nicht gefunden, so ist Found FALSE und
  der Funktionswert gibt die Position an, an der der Eintrag eingef�gt w�rde.
+******************************************************************************}
Function TRecHashTable.SearchItem
   (
   Key             : LongInt;
   var Found       : Boolean
   )
   : LongInt;
  var Position     : Integer;
      Increment    : Integer;
      StartPos     : Integer;
  begin
    Position:=GetPrimaryHashIndex(Key);          { Prim�rposition berechnen    }
    Increment:=GetSecondaryHashIndex(Key);       { Inkrement berechnen         }
    {$IFDEF DEBUG}
    if Position<0 then Raise Exception.Create('TRecHashTable.SearchItem: Negative Hash-Keys not allowed');
    if Increment<0 then Raise Exception.Create('TRecHashTable.SearchItem: Negative Hash-Keys not allowed');
    {$ENDIF}
    StartPos:=Position;
    while AItems[Position]^<>High(LongInt) do begin
      if (AItems[Position]^<>Low(LongInt))       { frei->weitersuchen          }
          and (Key=AItems[Position]^) then begin
        Result:=Position;                        { gefunden->TRUE und Ende     }
        Found:=TRUE;
        Exit;
      end;
      { n�chste Position berechnen                                             }
      Position:=(Position+Increment) Mod Capacity;
      if Position=StartPos then Break;
    end;
    Result:=Position;                            { nicht gefungen->FALSE       }
    Found:=FALSE;
  end;

{******************************************************************************+
  Function TRecHashTable.Find
--------------------------------------------------------------------------------
  Sucht einen Eintrag in der Hashtabelle. Wird der Eintrag gefunden, so
  liefert Find den in der Hashtabelle gespeicherten Zeiger, sonst
  liefert Find NIL.
+******************************************************************************}
Function TRecHashTable.Find
   (
   Key             : LongInt
   )
   : Pointer;
  var Found        : Boolean;
      Position     : Integer;
  begin
    Position:=SearchItem(Key,Found);
    if not Found then Result:=NIL
    else Result:=AItems[Position];
  end;


{******************************************************************************+
  Function TRecHashTable.GetPrimaryHashIndex
--------------------------------------------------------------------------------
  Ermittelt zu einem Eintrag die Hashvalue und berechnet daraus die Position
  des Eintrag in der Hashtabelle.
+******************************************************************************}
Function TRecHashTable.GetPrimaryHashIndex
   (
   Item            : LongInt
   )
   : LongInt;
  begin
    Result:=Item Mod Capacity;
  end;

{******************************************************************************+
  Function TRecHashTable.GetSecondaryHashIndex
--------------------------------------------------------------------------------
  Berechnet den Wert um den die Eintragsposition im Falle einer Hashkollision
  erh�ht wird.
+******************************************************************************}
Function TRecHashTable.GetSecondaryHashIndex
   (
   Item            : LongInt
   )
   : LongInt;
  begin
    Result:=FSecondary-(Item Mod FSecondary);
  end;

{******************************************************************************+
  Function TRecHashTable.GetItem                                               
--------------------------------------------------------------------------------
  Berechnet die Adresse des Records mit der �bergebenen Nummer und liefert
  einen Zeiger auf diesen Record zur�ck.
+******************************************************************************}
Function TRecHashTable.GetItem
   (
   AIndex          : Integer
   )
   : PLongInt;
  begin
    if PLongInt(FItems+AIndex*FItemSize)^=Low(LongInt) then Result:=NIL
    else if PLongInt(FItems+AIndex*FItemSize)^=High(LongInt) then Result:=NIL
    else Result:=@FItems[AIndex*FItemSize];
  end;

{******************************************************************************+
  Function TRecHashTable.GetAItem
--------------------------------------------------------------------------------
  Berechnet die Adresse des Records mit der �bergebenen Nummer und liefert
  einen Zeiger auf diesen Record zur�ck.
+******************************************************************************}
Function TRecHashTable.GetAItem
   (
   AIndex          : Integer
   )
   : PLongInt;
  begin
    Result:=@FItems[AIndex*FItemSize];
  end;

{******************************************************************************+
  Procedure TRecHashTable.OnFreeItem
--------------------------------------------------------------------------------
  Wird f�r jeden Record in der Tabelle aufgerufen. Hier k�nnen dynamische
  Variablen in den Record freigegeben werden.
+******************************************************************************}
Procedure TRecHashTable.OnFreeItem(Item: Pointer);
begin
end;

Procedure TRecHashTable.SetCount(ACount: Integer);
begin
   FCount:=ACount;
end;

{==============================================================================+
  TAutoHashTable
+==============================================================================}

Procedure TAutoHashTable.SetCount(ACount: Integer);
begin
   inherited SetCount(ACount);
   if Count/FCapacity>ExpandLevel then
      Capacity:=Trunc(ACount/ExpandTo);
end;

{==============================================================================+
  TAutoHashTable
+==============================================================================}

Procedure TAutoRecHashTable.SetCount(ACount: Integer);
begin
   inherited SetCount(ACount);
   if Count/FCapacity>ExpandLevel then
      Capacity:=Trunc(ACount/ExpandTo);
end;

{===============================================================================
  Diverses
+==============================================================================}

{******************************************************************************+
  Function String2HashIndex
--------------------------------------------------------------------------------
  Autor: Leitner Raimund
--------------------------------------------------------------------------------
  Erzeugt mittels Horner-Schema einen HashIndex f�r einen String.
+******************************************************************************}
Function String2HashIndex(WorkStr: String): LongInt;
var Base,
    HashIndexLo,
    HashIndexHi,
    j,
    L,
    Rest      : Integer;
begin
   { lustige Primzahl f�r Modulo-Operation }
   Base:=32749;
   { L�nge des Symbolnamens }
   L:=Length(WorkStr);
   HashIndexHi:=0;
   HashIndexLo:=0;
   { L/4 Durchg�nge (in einem Durchgang werden 4 Buchstaben verwendet }
   for j:=0 to ((L div 4)-1) do
       begin
          { h�heren Teil des HashIndex berechnen }
          HashIndexHi:=(HashIndexHi+Word(WorkStr[4*j+1])) mod Base;
          { niederen Teil des HashIndex berechnen }
          HashIndexLo:=(HashIndexLo+Word(WorkStr[4*j+3])) mod Base;
       end;
   { Rest berechnen }
   Rest:=L mod 4;
   { eventuelle Restbuchstaben miteinbeziehen }
   if Rest>0 then
      begin
         { Startposition vom Rest berechnen }
         j:=L-Rest+1;
         HashIndexHi:=HashIndexHi+(Byte(WorkStr[j]) shl 8);
         if Rest>1 then
            begin
               HashIndexHi:=HashIndexHi+Byte(WorkStr[j+1]);
               if Rest>2 then
                  HashIndexLo:=HashIndexLo+(Byte(WorkStr[j+2]) shl 8);
            end;
         HashIndexHi:=HashIndexHi mod Base;
         HashIndexLo:=HashIndexLo mod Base;
      end;
   Result:=(HashIndexHi shl 16)+HashIndexLo;
end;

function THashTable.OnCopyItem(Item:Pointer):Pointer;
begin
   Result:=NIL;
end;

end.
