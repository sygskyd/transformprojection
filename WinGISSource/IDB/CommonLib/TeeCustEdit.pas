{**********************************************}
{   TCustomSeries Component Editor Dialog      }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeCustEdit;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls, QComCtrls,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, ComCtrls,
     {$ENDIF}
     Chart, Series, TeCanvas, TeePenDlg, MultiLng;

type
  TCustomSeriesEditor = class(TForm)
    BLineBorder: TButtonPen;
    BLineColor: TButtonColor;
    GBStair: TGroupBox;
    CBStairs: TCheckBox;
    CBInvStairs: TCheckBox;
    CBColorEach: TCheckBox;
    CBDark3D: TCheckBox;
    Label1: TLabel;
    CBStack: TComboBox;
    LHeight: TLabel;
    EHeight: TEdit;
    UDHeight: TUpDown;
    CBClick: TCheckBox;
    BBrush: TButton;
    MlgSection1: TMlgSection;
    procedure FormShow(Sender: TObject);
    procedure CBStairsClick(Sender: TObject);
    procedure CBInvStairsClick(Sender: TObject);
    procedure CBColorEachClick(Sender: TObject);
    procedure CBDark3DClick(Sender: TObject);
    procedure CBStackChange(Sender: TObject);
    procedure EHeightChange(Sender: TObject);
    procedure CBClickClick(Sender: TObject);
    procedure BBrushClick(Sender: TObject);
  private
    { Private declarations }
    TheSeries : TCustomSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
uses TeEngine, TeePoEdi, TeeProcs, TeeConst, TeeBrushDlg;

type TCustomSeriesAccess=class(TCustomSeries);

procedure TCustomSeriesEditor.FormShow(Sender: TObject);
begin
  TheSeries:=TCustomSeries(Tag);
  With TheSeries do
  Begin
    CBColorEach.Checked:=ColorEachPoint;
    BLineBorder.LinkPen(LinePen);
    CBStack.ItemIndex:=Ord(TCustomSeriesAccess(TheSeries).Stacked);
    CBClick.Checked:=ClickableLine;

    if TheSeries is TLineSeries then
    begin
      CBStairs.Checked:=Stairs;
      CBInvStairs.Checked:=InvertedStairs;
      CBInvStairs.Enabled:=CBStairs.Checked;
      CBDark3D.Checked:=Dark3D;
      UDHeight.Position:=LineHeight;
    end
    else
    begin
      ShowControls(False,[ BLineBorder,BBrush,
                           GBStair,CBDark3D,LHeight,EHeight,UDHeight]);
    end;
  end;
  BLineColor.LinkProperty(TheSeries,'SeriesColor');
  if Assigned(Parent) then TeeInsertPointerForm(Parent,TheSeries.Pointer,TeeMsg_GalleryPoint);
end;

procedure TCustomSeriesEditor.CBStairsClick(Sender: TObject);
begin
  TheSeries.Stairs:=CBStairs.Checked;
  CBInvStairs.Enabled:=CBStairs.Checked;
end;

procedure TCustomSeriesEditor.CBInvStairsClick(Sender: TObject);
begin
  TheSeries.InvertedStairs:=CBInvStairs.Checked;
end;

procedure TCustomSeriesEditor.CBColorEachClick(Sender: TObject);
begin
  TheSeries.ColorEachPoint:=CBColorEach.Checked;
end;

procedure TCustomSeriesEditor.CBDark3DClick(Sender: TObject);
begin
  TheSeries.Dark3D:=CBDark3D.Checked;
end;

procedure TCustomSeriesEditor.CBStackChange(Sender: TObject);
begin
  TCustomSeriesAccess(TheSeries).Stacked:=TCustomSeriesStack(CBStack.ItemIndex);
end;

procedure TCustomSeriesEditor.EHeightChange(Sender: TObject);
begin
  if Showing then TheSeries.LineHeight:=UDHeight.Position
end;

procedure TCustomSeriesEditor.CBClickClick(Sender: TObject);
begin
  TheSeries.ClickableLine:=CBClick.Checked
end;

procedure TCustomSeriesEditor.BBrushClick(Sender: TObject);
begin
  EditChartBrush(Self,TheSeries.Brush);
end;

initialization
  RegisterClass(TCustomSeriesEditor);
end.
