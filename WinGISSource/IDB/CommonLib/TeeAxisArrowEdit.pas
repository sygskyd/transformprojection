unit TeeAxisArrowEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeeAxisToolEdit, StdCtrls, TeCanvas, TeePenDlg, ComCtrls, TeeTools;

type
  TAxisArrowToolEditor = class(TAxisToolEditor)
    Button1: TButton;
    UDLength: TUpDown;
    ELength: TEdit;
    LL: TLabel;
    Label2: TLabel;
    CBPos: TComboBox;
    Label3: TLabel;
    Edit1: TEdit;
    UDScroll: TUpDown;
    Label4: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ELC(Sender: TObject);
    procedure CBPosChange(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
  private
    { Private declarations }
    AxisArrowTool : TAxisArrowTool;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeeBrushDlg;

procedure TAxisArrowToolEditor.Button1Click(Sender: TObject);
begin
  EditChartBrush(Self,AxisArrowTool.Brush);
end;

procedure TAxisArrowToolEditor.FormShow(Sender: TObject);
begin
  inherited;
  AxisArrowTool:=TAxisArrowTool(Tag);
  With AxisArrowTool do
  begin
    UDLength.Position:=Length;
    CBPos.ItemIndex:=Ord(Position);
    UDScroll.Position:=ScrollPercent;
  end;
end;

procedure TAxisArrowToolEditor.ELC(Sender: TObject);
begin
  if Showing then AxisArrowTool.Length:=UDLength.Position;
end;

procedure TAxisArrowToolEditor.CBPosChange(Sender: TObject);
begin
  inherited;
  AxisArrowTool.Position:=TAxisArrowToolPosition(CBPos.ItemIndex);
end;

procedure TAxisArrowToolEditor.Edit1Change(Sender: TObject);
begin
  if Showing then AxisArrowTool.ScrollPercent:=UDScroll.Position
end;

initialization
  RegisterClass(TAxisArrowToolEditor);
end.
