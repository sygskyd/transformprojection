{**********************************************}
{   TCustomBoxSeries Editor dialog             }
{     TBoxSeries                               }
{     THorizBoxSeries                          }
{                                              }
{   Copyright (c) 2000 by David Berneda        }
{**********************************************}
unit TeeBoxPlotEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, TeCanvas, TeePenDlg, TeeBoxPlot;

type
  TBoxSeriesEditor = class(TForm)
    BMedian: TButtonPen;
    Label1: TLabel;
    EPos: TEdit;
    Label2: TLabel;
    ELength: TEdit;
    BWhisker: TButtonPen;
    procedure FormShow(Sender: TObject);
    procedure ELengthChange(Sender: TObject);
    procedure EPosChange(Sender: TObject);
  private
    { Private declarations }
    Box:TCustomBoxSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

Uses TeePoEdi;

procedure TBoxSeriesEditor.FormShow(Sender: TObject);
begin
  Box:=TCustomBoxSeries(Tag);
  with Box do
  begin
    BMedian.LinkPen(MedianPen);
    BWhisker.LinkPen(WhiskerPen);
    ELength.Text:=FloatToStr(WhiskerLength);
    EPos.Text:=FloatToStr(Position);
  end;
  if Assigned(Parent) then
  begin
    TeeInsertPointerForm(Parent,Box.Pointer,'Box');
    TeeInsertPointerForm(Parent,Box.ExtrOut,'ExtrOut');
    TeeInsertPointerForm(Parent,Box.MildOut,'MildOut');
  end;
end;

procedure TBoxSeriesEditor.ELengthChange(Sender: TObject);
begin
  if Showing and (ELength.Text<>'') then Box.WhiskerLength:=StrToFloat(ELength.Text);
end;

procedure TBoxSeriesEditor.EPosChange(Sender: TObject);
begin
  if Showing and (EPos.Text<>'') then Box.Position:=StrToFloat(EPos.Text);
end;

initialization
  RegisterClass(TBoxSeriesEditor);
end.

