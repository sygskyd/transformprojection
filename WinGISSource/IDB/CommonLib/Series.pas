{*********************************************}
{  TeeChart Standard Series Types             }
{  Copyright (c) 1995-2000 by David Berneda   }
{  All Rights Reserved                        }
{                                             }
{   TCustomSeries                             }
{     TLineSeries                             }
{     THorizLineSeries                        }
{     TAreaSeries                             }
{     TPointSeries                            }
{   TCustomBarSeries                          }
{     TBarSeries                              }
{     THorizBarSeries                         }
{   TCircledSeries                            }
{     TPieSeries                              }
{   TFastLineSeries                           }
{                                             }
{*********************************************}
{$I teedefs.inc}
unit Series;

interface

Uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows,
     {$ENDIF}
     {$IFDEF CLX}
     QGraphics, QForms,
     {$ELSE}
     Graphics, Forms,
     {$ENDIF}
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
     Objects,
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
     TeEngine, Chart, SysUtils, Classes, TeCanvas, TeeProcs;

Const PiDegree : Double = Pi/180.0;
      Tee_PieShadowColor = TColor($A0A0A0);

Type
  TSeriesPointerStyle=( psRectangle,psCircle,psTriangle,psDownTriangle,
                        psCross,psDiagCross,psStar,psDiamond,psSmallDot,
                        psNothing );

  TOnGetPointerStyle=Function( Sender:TChartSeries;
                               ValueIndex:Integer):TSeriesPointerStyle of object;

  TSeriesPointer=class(TTeeCustomShapeBrushPen)
  private
    FDark3D    : Boolean;                  //sr
    FDraw3D    : Boolean;                  //sr
    FHorizSize : Integer;                  //sr
    FInflate   : Boolean;                  //sr
    FOwner     : TChartSeries;             //set
    FStyle     : TSeriesPointerStyle;      //sr
    FVertSize  : Integer;                  //sr
    Procedure CheckPointerSize(Value:Integer);
    Procedure SetDark3D(Value:Boolean);
    Procedure SetDraw3D(Value:Boolean);
    Procedure SetHorizSize(Value:Integer);
    Procedure SetInflate(Value:Boolean);
    Procedure SetStyle(Value:TSeriesPointerStyle);
    Procedure SetVertSize(Value:Integer);
  protected
    AllowChangeSize : Boolean;             //set
    Procedure CalcHorizMargins(Var LeftMargin,RightMargin:Integer);
    Procedure CalcVerticalMargins(Var TopMargin,BottomMargin:Integer);
    Procedure Change3D(Value:Boolean);
    Procedure ChangeHorizSize(NewSize:Integer);
    Procedure ChangeStyle(NewStyle:TSeriesPointerStyle);
    Procedure ChangeVertSize(NewSize:Integer);
    Procedure DrawLegendShape(AColor:TColor; Const Rect:TRect; DrawPen:Boolean);
    Procedure PrepareCanvas(ACanvas:TCanvas3D; ColorValue:TColor);
  public
    Constructor Create(AOwner: TChartSeries);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TChartSeries);
    Procedure   Store(var S: TOldStream);
    Function    OnCompareItems(Item: TPersistent): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    Procedure   Assign(Source: TPersistent); override;

    Procedure Draw(px,py:Integer; ColorValue:TColor; AStyle:TSeriesPointerStyle);
    Procedure DrawPointer( ACanvas:TCanvas3D;
                           Is3D:Boolean; px,py,tmpHoriz,tmpVert:Integer;
                           ColorValue:TColor; AStyle:TSeriesPointerStyle);
    property ParentSeries:TChartSeries read FOwner;
  published
    property Brush;
    property Dark3D:Boolean read FDark3D write SetDark3D default True;
    Property Draw3D:Boolean read FDraw3D write SetDraw3D default True;
    Property HorizSize:Integer read FHorizSize write SetHorizSize default 4;
    property InflateMargins:Boolean read FInflate write SetInflate;
    property Pen;
    Property Style:TSeriesPointerStyle read FStyle write SetStyle;
    Property VertSize:Integer read FVertSize write SetVertSize default 4;
    Property Visible;
  end;

  TCustomLineSeries=class(TChartSeries)
  protected
    Function GetLinePen:TChartPen;
  public
    property LinePen:TChartPen read GetLinePen write SetPen;
  end;

  TCustomSeries=class;

  TSeriesClickPointerEvent=Procedure( Sender:TCustomSeries;
                                      ValueIndex:Integer;
                                      X, Y: Integer) of object;

  TCustomSeriesStack=(cssNone,cssOverlap,cssStack,cssStack100);

  TCustomSeries=Class(TCustomLineSeries)
  private
    FAreaBrush          : TChartBrush;         //sr
    FAreaColor          : TColor;              //sr
    FAreaLinesPen       : TChartPen;           //sr
    FClickableLine      : Boolean;             //sr+
    FDark3D             : Boolean;             //sr
    FDrawArea           : Boolean;             //sr+
    FDrawLine           : Boolean;             //sr+
    FInvertedStairs     : Boolean;             //sr+
    FLineHeight         : Integer;             //sr+
    FPointer            : TSeriesPointer;      //sr
    FStacked            : TCustomSeriesStack;  //sr+
    FStairs             : Boolean;             //sr+

    { events }
    FOnClickPointer     : TSeriesClickPointerEvent;
    FOnGetPointerStyle  : TOnGetPointerStyle;

    { internal }
    BottomPos           : Integer;
    OldBottomPos        : Integer;
    OldX                : Integer;
    OldY                : Integer;
    OldColor            : TColor;
    tmpDark3DRatio      : Double;
    FColorEachLine      : Boolean;             //sr+
    Function CalcStackedYPos(ValueIndex:Integer; Value:Double):Integer;
    Function GetAreaBrush:TBrushStyle;
    Function GetLineBrush:TBrushStyle;
    Function PointOrigin(ValueIndex:Integer; SumAll:Boolean):Double;
    Procedure SetAreaBrush(Value:TChartBrush);
    Procedure SetAreaBrushStyle(Value:TBrushStyle);
    Procedure SetAreaColor(Value:TColor);
    Procedure SetAreaLinesPen(Value:TChartPen);
    Procedure SetDark3D(Value:Boolean);
    Procedure SetDrawArea(Value:Boolean);
    Procedure SetInvertedStairs(Value:Boolean);
    Procedure SetLineBrush(Value:TBrushStyle);
    Procedure SetLineHeight(Value:Integer);
    Procedure SetPointer(Value:TSeriesPointer);
    Procedure SetStacked(Value:TCustomSeriesStack);
    Procedure SetStairs(Value:Boolean);
    procedure SetColorEachLine(const Value: Boolean);
  protected
    Procedure CalcHorizMargins(Var LeftMargin,RightMargin:Integer); override;
    Procedure CalcVerticalMargins(Var TopMargin,BottomMargin:Integer); override;
    Procedure CalcZOrder; override;
    Function ClickedPointer( ValueIndex,tmpX,tmpY:Integer;
                             x,y:Integer):Boolean; virtual;
    Procedure DrawLegendShape(ValueIndex:Integer; Const Rect:TRect); override;
    Procedure DrawMark( ValueIndex:Integer; Const St:String;
                        APosition:TSeriesMarkPosition); override;
    Procedure DrawPointer(AX,AY:Integer; AColor:TColor; ValueIndex:Integer); virtual;
    procedure DrawValue(ValueIndex:Integer); override;
    Function GetAreaBrushColor(AColor:TColor):TColor;
    class Function GetEditorClass:String; override;
    procedure LinePrepareCanvas(tmpCanvas:TCanvas3D; tmpColor:TColor);
    Procedure SetParentChart(Const Value:TCustomAxisPanel); override;

    property Stacked:TCustomSeriesStack read FStacked write SetStacked default cssNone;
  public
    Constructor Create(AOwner: TComponent); override;
    Destructor  Destroy; override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TComponent);
    Procedure   Store(var S: TOldStream);
    Function    CreateDeterminant(Determinant: String): String;
    Function    OnCompareItems(Item: TChartSeries): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    Procedure Assign(Source:TPersistent); override;
    Function CalcYPos(ValueIndex:Integer):Integer; override;
    Function Clicked(x,y:Integer):Integer; override;
    Function GetOriginPos(ValueIndex:Integer):Integer; virtual;
    Function MaxYValue:Double; override;
    Function MinYValue:Double; override;

    property AreaBrush:TBrushStyle read GetAreaBrush write SetAreaBrushStyle
                                        default bsSolid;
    property AreaChartBrush:TChartBrush read FAreaBrush write SetAreaBrush;
    property AreaColor:TColor read FAreaColor write SetAreaColor default clTeeColor;
    property AreaLinesPen:TChartPen read FAreaLinesPen write SetAreaLinesPen;
    property ClickableLine:Boolean read FClickableLine write FClickableLine default True;
    property ColorEachLine:Boolean read FColorEachLine write SetColorEachLine default True; 
    property Dark3D:Boolean read FDark3D write SetDark3D default True;
    property DrawArea:Boolean read FDrawArea write SetDrawArea default False;
    property InvertedStairs:Boolean read FInvertedStairs write SetInvertedStairs default False;
    property LineBrush:TBrushStyle read GetLineBrush write SetLineBrush default bsSolid;
    property LineHeight:Integer read FLineHeight write SetLineHeight default 0;
    property Pointer:TSeriesPointer read FPointer write SetPointer;
    property Stairs:Boolean read FStairs write SetStairs default False;
    { events }
    property OnClickPointer:TSeriesClickPointerEvent read FOnClickPointer
                                                     write FOnClickPointer;
  published
    { events }
    property OnGetPointerStyle:TOnGetPointerStyle read FOnGetPointerStyle
                                                  write FOnGetPointerStyle;

  end;

  TLineSeries=Class(TCustomSeries)
  protected
    class Procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); override;
    Procedure PrepareLegendCanvas( ValueIndex:Integer; Var BackColor:TColor;
                                   Var BrushStyle:TBrushStyle); override;
    class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); override;
  public
    Constructor Create(AOwner: TComponent); override;
    Procedure Assign(Source:TPersistent); override;
  published
    property Active;
    property ColorEachPoint;
    property ColorSource;
    property Cursor;
    property Depth;
    property HorizAxis;
    property Marks;
    property ParentChart;
    property DataSource;  { after parentchart }
    property PercentFormat;
    property SeriesColor;
    property ShowInLegend;
    property Stacked;
    property Title;
    property ValueFormat;
    property VertAxis;
    property XLabelsSource;

    { events }
    property AfterDrawValues;
    property BeforeDrawValues;
    property OnAfterAdd;
    property OnBeforeAdd;
    property OnClearValues;
    property OnClick;
    property OnDblClick;
    property OnGetMarkText;

    property Brush;
    property ClickableLine;
    property Dark3D;
    property InvertedStairs;
    property LineBrush;
    property LineHeight;
    property LinePen;
    property Pointer;
    property Stairs;
    property XValues;
    property YValues;
  end;

  THorizLineSeries=class(TLineSeries)
  public
    Constructor Create(AOwner: TComponent); override;
  end;

  TPointSeries=Class(TCustomSeries)
  private
    Procedure SetFixed;
  protected
    class Function CanDoExtra : Boolean; virtual;
    class Procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); override;
    class Function GetEditorClass:String; override;
    Procedure PrepareForGallery(IsEnabled:Boolean); override;
    Procedure SetColorEachPoint(Value:Boolean); override;
    class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); override;
  public
    Constructor Create(AOwner: TComponent); override;
    Procedure   Assign(Source: TPersistent); override;
  published
    property Active;
    property ColorEachPoint;
    property ColorSource;
    property Cursor;
    property Depth;
    property HorizAxis;
    property Marks;
    property ParentChart;
    property DataSource;  { after parentchart }
    property PercentFormat;
    property SeriesColor;
    property ShowInLegend;
    property Stacked;
    property Title;
    property ValueFormat;
    property VertAxis;
    property XLabelsSource;

    { events }
    property AfterDrawValues;
    property BeforeDrawValues;
    property OnAfterAdd;
    property OnBeforeAdd;
    property OnClearValues;
    property OnClick;
    property OnDblClick;
    property OnGetMarkText;

    property ClickableLine;
    property Pointer;
    property XValues;
    property YValues;
   { events }
    property OnClickPointer;
  end;

  TMultiArea=(maNone,maStacked,maStacked100);

  TAreaSeries=Class(TCustomSeries)
  private
    FUseOrigin : Boolean;              //sr
    FOrigin    : Double;               //sr
    Function GetMultiArea:TMultiArea;
    Procedure SetMultiArea(Value:TMultiArea);
    Procedure SetOrigin(Const Value:Double);
    Procedure SetUseOrigin(Value:Boolean);
  protected
    class Procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); override;
    class Function GetEditorClass:String; override;
    Procedure PrepareLegendCanvas( ValueIndex:Integer; Var BackColor:TColor;
                                   Var BrushStyle:TBrushStyle); override;
    class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); override;
  public
    Constructor Create(AOwner: TComponent); override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TComponent);
    Procedure   Store(var S: TOldStream);
    Function    CreateDeterminant(Determinant: String): String;
    Function    OnCompareItems(Item: TCustomSeries): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    Procedure   Assign(Source: TPersistent); override;
    Function    GetOriginPos(ValueIndex: Integer): Integer; override;
    Function    MaxYValue: Double; override;
    Function    MinYValue: Double; override;
  published
    property Active;
    property ColorEachPoint;
    property ColorSource;
    property Cursor;
    property Depth;
    property HorizAxis;
    property Marks;
    property ParentChart;
    property DataSource;  { after parentchart }
    property PercentFormat;
    property SeriesColor;
    property ShowInLegend;
    property Title;
    property ValueFormat;
    property VertAxis;
    property XLabelsSource;

    { events }
    property AfterDrawValues;
    property BeforeDrawValues;
    property OnAfterAdd;
    property OnBeforeAdd;
    property OnClearValues;
    property OnClick;
    property OnDblClick;
    property OnGetMarkText;

    property AreaBrush;
    property AreaChartBrush;
    property AreaColor;
    property AreaLinesPen;
    property ClickableLine;
    property Dark3D;
    property DrawArea;
    property InvertedStairs;
    property LinePen;
    property MultiArea:TMultiArea read GetMultiArea write SetMultiArea default maNone;
    property Pointer;
    property Stairs;
    property UseYOrigin:Boolean read FUseOrigin write SetUseOrigin default False;
    property XValues;
    property YOrigin:Double read FOrigin write SetOrigin;
    property YValues;
   { events }
    property OnClickPointer;
  end;

  BarException=class(Exception);

  TMultiBar=(mbNone,mbSide,mbStacked,mbStacked100,mbSideAll);

  TCustomBarSeries=class;

  TBarStyle=( bsRectangle,bsPyramid,bsInvPyramid,
              bsCilinder,bsEllipse,bsArrow,bsRectGradient,bsCone);

  TGetBarStyleEvent=Procedure( Sender:TCustomBarSeries; ValueIndex:Integer;
                               Var TheBarStyle:TBarStyle) of object;

  TBarSeriesGradient=class(TCustomTeeGradient)
  published
    property Direction nodefault;
    property MidColor;
    property StartColor;
  end;

  TCustomBarSeries=Class(TChartSeries)
  private
    FAutoBarSize     : Boolean;            //sr
    FAutoMarkPosition: Boolean;            //sr
    FBarStyle        : TBarStyle;          //sr
    FBarWidthPercent : Integer;            //sr
    FConePercent     : Integer;            //sr
    FCustomBarSize   : Integer;            //sr
    FDark3D          : Boolean;            //sr
    FGradient        : TBarSeriesGradient; //sr
    FMultiBar        : TMultiBar;          //sr
    FOffsetPercent   : Integer;            //sr
    FSideMargins     : Boolean;            //sr
    FStackGroup      : Integer;            //sr
    FUseOrigin       : Boolean;            //sr
    FOrigin          : Double;             //sr
    { events }
    FOnGetBarStyle   : TGetBarStyleEvent;  //set

    { internal }
    FBarBounds     : TRect;
    IBarSize       : Integer;
    INumBars       : Integer;
    IMaxBarPoints  : Integer;
    IOrderPos      : Integer;
    IPreviousCount : Integer;

    Function GetBarBrush:TChartBrush;
    Function GetBarPen:TChartPen;
    Function GetBarStyle(ValueIndex:Integer):TBarStyle;
    Function InternalCalcMarkLength(ValueIndex:Integer):Integer; virtual; abstract;
    Function InternalPointOrigin(ValueIndex:Integer; SumAll:Boolean):Double;
    Procedure SetAutoBarSize(Value:Boolean);
    Procedure SetAutoMarkPosition(Value:Boolean);
    Procedure SetBarWidthPercent(Value:Integer);
    Procedure SetOffsetPercent(Value:Integer);
    Procedure SetBarStyle(Value:TBarStyle);
    procedure SetConePercent(const Value: Integer);
    Procedure SetDark3d(Value:Boolean);
    Procedure SetUseOrigin(Value:Boolean);
    Procedure SetSideMargins(Value:Boolean);
    Procedure SetGradient(Value:TBarSeriesGradient);
    Procedure SetOrigin(Const Value:Double);
    Procedure SetMultiBar(Value:TMultiBar);
    Procedure SetOtherBars(SetOthers:Boolean);
    Procedure SetStackGroup(Value:Integer);

    Procedure DoGradient3D(P0,P1:TPoint);
    Procedure BarGradient(Const R:TRect);
    Procedure InternalApplyBarMargin(Var MarginA,MarginB:Integer);
    Function InternalGetOriginPos(ValueIndex:Integer; DefaultOrigin:Integer):Integer;
    Function MaxMandatoryValue(Const Value:Double):Double;
    Function MinMandatoryValue(Const Value:Double):Double;
    Procedure SetCustomBarSize(Value:Integer);
  protected
    Procedure CalcZOrder; override;
    class procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); override;
    Procedure DoBeforeDrawChart; override;
    Procedure DrawLegendShape(ValueIndex:Integer; Const Rect:TRect); override;
    class Function GetEditorClass:String; override;
    Function InternalClicked(ValueIndex:Integer; Const APoint:TPoint):Boolean; virtual;
    Procedure PrepareForGallery(IsEnabled:Boolean); override;
    Procedure PrepareLegendCanvas( ValueIndex:Integer; Var BackColor:TColor;
                                   Var BrushStyle:TBrushStyle); override;
    Procedure SetParentChart(Const Value:TCustomAxisPanel); override;
    class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); override;
  public
    NormalBarColor : TColor;

    Constructor Create(AOwner: TComponent); override;
    Destructor  Destroy; override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TComponent);
    Procedure   Store(var S: TOldStream);
    Function    CreateDeterminant(Determinant: String): String;
    Function    OnCompareItems(Item: TChartSeries): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    Function AddBar(Const AValue:Double; Const ALabel:String; AColor:TColor):Integer;
    Function ApplyBarOffset(Position:Integer):Integer;
    Procedure Assign(Source:TPersistent); override;
    Function BarMargin:Integer; {virtual; 4.02 }
    Procedure BarRectangle(BarColor:TColor; ALeft,ATop,ARight,ABottom:Integer);
    Function CalcMarkLength(ValueIndex:Integer):Integer;
    Function Clicked(x,y:Integer):Integer; override;
    Function NumSampleValues:Integer; override;
    Function PointOrigin(ValueIndex:Integer; SumAll:Boolean):Double; virtual;
    Procedure SetPenBrushBar(BarColor:TColor);

    property BarBounds:TRect read FBarBounds;
    property ConePercent:Integer read FConePercent write SetConePercent
                                default 0;
  published
    property Active;
    property BarBrush:TChartBrush read GetBarBrush write SetBrush;
    property BarPen:TChartPen read GetBarPen write SetPen;
    property ColorEachPoint;
    property ColorSource;
    property Cursor;
    property Depth;
    property HorizAxis;
    property Marks;
    property ParentChart;
    property DataSource;
    property PercentFormat;
    property SeriesColor;
    property ShowInLegend;
    property Title;
    property ValueFormat;
    property VertAxis;
    property XLabelsSource;

    { events }
    property AfterDrawValues;
    property BeforeDrawValues;
    property OnAfterAdd;
    property OnBeforeAdd;
    property OnClearValues;
    property OnClick;
    property OnDblClick;
    property OnGetMarkText;

    property AutoBarSize:Boolean read FAutoBarSize write SetAutoBarSize default False;
    property AutoMarkPosition:Boolean read FAutoMarkPosition write SetAutoMarkPosition default True;
    property BarStyle:TBarStyle read FBarStyle write SetBarStyle
                                default bsRectangle;
    property BarWidthPercent:Integer read FBarWidthPercent
                                     write SetBarWidthPercent default 70;
    property Dark3D:Boolean read FDark3D write SetDark3D default True;
    property Gradient:TBarSeriesGradient read FGradient write SetGradient;
    property MultiBar:TMultiBar read FMultiBar write SetMultiBar default mbSide;
    property OffsetPercent:Integer read FOffsetPercent
                                   write SetOffsetPercent default 0;
    property SideMargins:Boolean read FSideMargins write SetSideMargins default True;
    property StackGroup:Integer read FStackGroup write SetStackGroup default 0;
    property UseYOrigin:Boolean read FUseOrigin write SetUseOrigin default True;
    property YOrigin:Double read FOrigin write SetOrigin;

    { inherited published }
    property XValues;
    property YValues;
    { events }
    property OnGetBarStyle:TGetBarStyleEvent read FOnGetBarStyle write
                                             FOnGetBarStyle;
  end;

  TBarSeries=class(TCustomBarSeries)
  private
    Function InternalCalcMarkLength(ValueIndex:Integer):Integer; override;
  protected
    Procedure CalcHorizMargins(Var LeftMargin,RightMargin:Integer); override;
    Procedure CalcVerticalMargins(Var TopMargin,BottomMargin:Integer); override;
    procedure DrawValue(ValueIndex:Integer); override;
    Procedure DrawMark( ValueIndex:Integer; Const St:String;
                        APosition:TSeriesMarkPosition); override;
    Function InternalClicked(ValueIndex:Integer; Const APoint:TPoint):Boolean; override;
    Function MoreSameZOrder:Boolean; override;
  public
    Constructor Create(AOwner: TComponent); override;

    Function CalcXPos(ValueIndex:Integer):Integer; override;
    Function CalcYPos(ValueIndex:Integer):Integer; override;
    Procedure DrawBar(BarIndex,StartPos,EndPos:Integer); virtual;
    Function DrawSeriesForward(ValueIndex:Integer):Boolean; override;
    Function DrawValuesForward:Boolean; override;
    Function GetOriginPos(ValueIndex:Integer):Integer;
    Function MaxXValue:Double; override;
    Function MaxYValue:Double; override;
    Function MinYValue:Double; override;

    property BarWidth:Integer read IBarSize;
  published
    property CustomBarWidth:Integer read FCustomBarSize
                                    write SetCustomBarSize default 0;
  end;

  THorizBarSeries=class(TCustomBarSeries)
  private
    Function InternalCalcMarkLength(ValueIndex:Integer):Integer; override;
  protected
    Procedure CalcHorizMargins(Var LeftMargin,RightMargin:Integer); override;
    Procedure CalcVerticalMargins(Var TopMargin,BottomMargin:Integer); override;
    procedure DrawValue(ValueIndex:Integer); override;
    Procedure DrawMark( ValueIndex:Integer; Const St:String;
                        APosition:TSeriesMarkPosition); override;
    Function InternalClicked(ValueIndex:Integer; Const APoint:TPoint):Boolean; override;
  public
    Constructor Create(AOwner: TComponent); override;

    Function CalcXPos(ValueIndex:Integer):Integer; override;
    Function CalcYPos(ValueIndex:Integer):Integer; override;
    Procedure DrawBar(BarIndex,StartPos,EndPos:Integer); virtual;
    Function DrawSeriesForward(ValueIndex:Integer):Boolean; override;
    Function DrawValuesForward:Boolean; override;
    Function GetOriginPos(ValueIndex:Integer):Integer;
    Function MaxXValue:Double; override;
    Function MinXValue:Double; override;
    Function MaxYValue:Double; override;

    property BarHeight:Integer read IBarSize;
  published
    property CustomBarHeight:Integer read FCustomBarSize
                                     write SetCustomBarSize default 0;
  end;

  TCircledSeries=class(TChartSeries)
  private
    FCircled         : Boolean;    //sr+
    FRotationAngle   : Integer;    //sr+
    FCustomXRadius   : Integer;    //sr+
    FCustomYRadius   : Integer;    //sr+
    FXRadius         : Integer;    //sr+
    FYRadius         : Integer;    //sr+
    FCircleBackColor : TColor;     //sr

    { internal }
    IBack3D         : TView3DOptions;
    FCircleWidth    : Integer;
    FCircleHeight   : Integer;
    FCircleXCenter  : Integer;
    FCircleYCenter  : Integer;
    FCircleRect     : TRect;
    IRotDegree      : Double;

    procedure SetCircleBackColor(Value:TColor);
    Procedure SetCircled(Value:Boolean);
    procedure SetCustomXRadius(Value:Integer);
    procedure SetCustomYRadius(Value:Integer);
    procedure SetOtherCustomRadius(IsXRadius:Boolean; Value:Integer);
  protected
    Procedure AdjustCircleRect;
    Function CalcCircleBackColor:TColor;
    Procedure CalcRadius;
    procedure DoBeforeDrawValues; override;
    Procedure PrepareLegendCanvas( ValueIndex:Integer; Var BackColor:TColor;
                                   Var BrushStyle:TBrushStyle); override;
    Procedure SetActive(Value:Boolean); override;
    Procedure SetParentChart(Const Value:TCustomAxisPanel); override;
    Procedure SetParentProperties(EnableParentProps:Boolean); dynamic;
    Procedure SetRotationAngle(Value:Integer);
  public
    Constructor Create(AOwner: TComponent); override;
    Destructor  Destroy; override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TComponent);
    Procedure   Store(var S: TOldStream);
    Function    OnCompareItems(Item: TChartSeries): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    Procedure AngleToPos( Const Angle,AXRadius,AYRadius:Double;
                          Var X,Y:Integer);
    Procedure Assign(Source:TPersistent); override;
    Function AssociatedToAxis(Axis:TChartAxis):Boolean; override;
    Function PointToAngle(x,y:Integer):Double;
    Procedure Rotate(Angle:Integer);
    Function UseAxis:Boolean; override;
    { read only properties }
    property XRadius:Integer read FXRadius;
    property YRadius:Integer read FYRadius;
    property CircleXCenter:Integer read FCircleXCenter;
    property CircleYCenter:Integer read FCircleYCenter;
    property CircleWidth:Integer read FCircleWidth;
    property CircleHeight:Integer read FCircleHeight;
    property CircleRect:TRect read FCircleRect;
    property CircleBackColor:TColor read FCircleBackColor
                                    write SetCircleBackColor default clTeeColor;
    property RotationAngle:Integer read FRotationAngle write SetRotationAngle
                                   default 0;
  published
    property ColorSource;
    property Cursor;
    property Marks;
    property ParentChart;
    property DataSource;
    property PercentFormat;
    property SeriesColor;
    property ShowInLegend;
    property Title;
    property ValueFormat;
    property XLabelsSource;

    { events }
    property AfterDrawValues;
    property BeforeDrawValues;
    property OnAfterAdd;
    property OnBeforeAdd;
    property OnClearValues;
    property OnClick;
    property OnDblClick;
    property OnGetMarkText;

    property Circled:Boolean read FCircled write SetCircled default True;
    property CustomXRadius:Integer read FCustomXRadius write SetCustomXRadius default 0;
    property CustomYRadius:Integer read FCustomYRadius write SetCustomYRadius default 0;
  end;

  PieException=class(ChartException);

  TPieAngle={$IFDEF D4}Packed Record{$ELSE}class{$ENDIF}
  {$IFNDEF D4}
  public
  {$ENDIF}
    StartAngle : Double;
    MidAngle   : Double;
    EndAngle   : Double;
  end;

  {$IFDEF D4}
  TPieAngles=Array of TPieAngle;
  {$ELSE}
  TPieAngles=class(TList)
  private
    Function GetAngle(Index:Integer):TPieAngle;
  public
    property Angle[Index:Integer]:TPieAngle read GetAngle; default;
  end;
  {$ENDIF}

  TExplodedSlices=class(TList)
  private
    Procedure Put(Index,Value:Integer);
    Function Get(Index:Integer):Integer;
  public
    OwnerSeries:TChartSeries;
    property Value[Index:Integer]:Integer read Get write Put; default;
  end;

  TPieOtherStyle=(poNone,poBelowPercent,poBelowValue);

  TPieOtherSlice=class(TPersistent)
  private
    FColor    : TColor;          //sr
    FStyle    : TPieOtherStyle;  //sr
    FText     : String;          //sr
    FValue    : Double;          //sr
    FOwner    : TChartSeries;
    Function IsOtherStored:Boolean;
    procedure SetColor(Value:TColor);
    procedure SetStyle(Value:TPieOtherStyle);
    procedure SetText(Const Value:String);
    procedure SetValue(Const Value:Double);
  public
    Constructor Create(AOwner: TChartSeries);
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TChartSeries);
    Procedure   Store(var S: TOldStream);
    Function    OnCompareItems(Item: TPersistent): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
    Procedure   Assign(Source: TPersistent); override;
  published
    property Color:TColor read FColor write SetColor default clTeeColor;
    property Style:TPieOtherStyle read FStyle write SetStyle default poNone;
    property Text:String read FText write SetText stored IsOtherStored;
    property Value:Double read FValue write SetValue;
  end;

  TPieSeries=Class(TCircledSeries)
  private
    FAngleSize     : Integer;         //sr
    FDark3D        : Boolean;         //sr
    FDonutPercent  : Integer;         //sr+
    FExplodedSlice : TExplodedSlices; { <-- Exploded slice % storage }
    FExplodeBiggest: Integer;         //sr+
    FOtherSlice    : TPieOtherSlice;  //sr
    FShadow        : TTeeShadow;      //sr
    FUsePatterns   : Boolean;         //sr+

    ISortedSlice   : Array{$IFNDEF D4}[0..1023]{$ENDIF} of Integer;
    Procedure CalcExplodeBiggest;
    Procedure CalcExplodedOffset( ValueIndex:Integer;
                                  Var OffsetX,OffsetY:Integer);
    Function CompareSlice(A,B:Integer):Integer;
    Procedure DisableRotation;
    {$IFNDEF D4}
    Procedure FreePieAngles;
    {$ENDIF}
    Function GetPiePen:TChartPen;
    Function GetPieValues:TChartValueList;
    Procedure SetAngleSize(Value:Integer);
    Procedure SetDark3D(Value:Boolean);
    procedure SetExplodeBiggest(Value:Integer);
    procedure SetOtherSlice(Value:TPieOtherSlice);
    Procedure SetPieValues(Value:TChartValueList);
    Procedure SetShadow(Value:TTeeShadow);
    procedure SetUsePatterns(Value:Boolean);
    Procedure SwapSlice(a,b:Integer);
  protected
    FAngles    : TPieAngles;
    IniX       : Integer;
    IniY       : Integer;
    EndX       : Integer;
    EndY       : Integer;
    IsExploded : Boolean;
    Procedure AddSampleValues(NumValues:Integer); override;
    Procedure CalcAngles;
    Procedure CalcExplodedRadius(ValueIndex:Integer; Var AXRadius,AYRadius:Integer);
    Procedure ClearLists; override;
    class Procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); override;
    procedure DoBeforeDrawChart; override;
    procedure DrawAllValues; override;
    Procedure DrawMark( ValueIndex:Integer; Const St:String;
                        APosition:TSeriesMarkPosition); override;
    Procedure DrawPie(ValueIndex:Integer); virtual;
    procedure DrawValue(ValueIndex:Integer); override;
    Procedure GalleryChanged3D(Is3D:Boolean); override;
    class Function GetEditorClass:String; override;
    Procedure PrepareForGallery(IsEnabled:Boolean); override;
    Procedure PrepareLegendCanvas( ValueIndex:Integer; Var BackColor:TColor;
                                   Var BrushStyle:TBrushStyle); override;
    procedure SetDonutPercent(Value:Integer);
    class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); override;
  public
    Constructor Create(AOwner: TComponent); override;
    Destructor  Destroy; override;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
    Constructor Load(var S: TOldStream; AOwner: TComponent);
    Procedure   Store(var S: TOldStream);
    Function    CreateDeterminant(Determinant: String): String;
    Function    OnCompareItems(Item: TChartSeries): Boolean;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

    Function AddPie(Const AValue:Double; Const ALabel:String{$IFDEF D4}=''{$ENDIF}; AColor:TColor{$IFDEF D4}=clTeeColor{$ENDIF}):Integer;
    Procedure Assign(Source:TPersistent); override;
    Function BelongsToOtherSlice(ValueIndex:Integer):Boolean;
    Function CalcClickedPie(x,y:Integer):Integer;
    Function CalcXPos(ValueIndex:Integer):Integer; override;
    Function Clicked(x,y:Integer):Integer; override;
    Function CountLegendItems:Integer; override;
    Function LegendToValueIndex(LegendIndex:Integer):Integer; override;
    Function MaxXValue:Double; override;
    Function MinXValue:Double; override;
    Function MaxYValue:Double; override;
    Function MinYValue:Double; override;
    Function NumSampleValues:Integer; override;
    procedure SwapValueIndex(a,b:Integer); override;

    property Angles:TPieAngles read FAngles;
    property DonutPercent:Integer read FDonutPercent write SetDonutPercent;
    property ExplodedSlice:TExplodedSlices read FExplodedSlice;
  published
    property Active;
    property AngleSize:Integer read FAngleSize write SetAngleSize default 360;
    property CircleBackColor;
    property ColorEachPoint default True;
    property Dark3D:Boolean read FDark3D write SetDark3D default True;
    property ExplodeBiggest:Integer read FExplodeBiggest write SetExplodeBiggest default 0;
    property OtherSlice:TPieOtherSlice read FOtherSlice write SetOtherSlice;
    property PiePen:TChartPen read GetPiePen write SetPen;
    property PieValues:TChartValueList read GetPieValues write SetPieValues;
    property RotationAngle;
    property Shadow:TTeeShadow read FShadow write SetShadow;
    property UsePatterns:Boolean read FUsePatterns write SetUsePatterns default False;
  end;

  TFastLineSeries=class(TCustomLineSeries)
  private
    FAutoRepaint : Boolean;
    { internal }
    OldX         : Integer;
    OldY         : Integer;
  protected
    class Procedure CreateSubGallery(AddSubChart:TChartSubGalleryProc); override;
    procedure DrawAllValues; override;
    Procedure DrawLegendShape(ValueIndex:Integer; Const Rect:TRect); override;
    Procedure DrawMark( ValueIndex:Integer; Const St:String;
                        APosition:TSeriesMarkPosition); override;
    procedure DrawValue(ValueIndex:Integer); override;
    class Function GetEditorClass:String; override;
    procedure PrepareCanvas;
    Procedure SetPen(Const Value:TChartPen); override;
    Procedure SetSeriesColor(AColor:TColor); override;
    class Procedure SetSubGallery(ASeries:TChartSeries; Index:Integer); override;
  public
    Constructor Create(AOwner: TComponent); override;
    Procedure Assign(Source:TPersistent); override;
    Function Clicked(x,y:Integer):Integer; override;
    Procedure NotifyNewValue(Sender:TChartSeries; ValueIndex:Integer); override;
  published
    property Active;
    property Cursor;
    property Depth;
    property HorizAxis;
    property Marks;
    property ParentChart;
    property DataSource;
    property PercentFormat;
    property SeriesColor;
    property ShowInLegend;
    property Title;
    property ValueFormat;
    property VertAxis;
    property XLabelsSource;

    { events }
    property AfterDrawValues;
    property BeforeDrawValues;
    property OnAfterAdd;
    property OnBeforeAdd;
    property OnClearValues;
    property OnClick;
    property OnDblClick;
    property OnGetMarkText;

    property AutoRepaint:Boolean read FAutoRepaint write FAutoRepaint default True;
    property LinePen;
    property XValues;
    property YValues;
  end;

Const
  bsCylinder=bsCilinder;  { <-- better spelling... }

{ calls RegisterTeeSeries for each "standard" series type
  (line,bar,pie,fastline,horizbar,area,point and horizline) }
Procedure RegisterTeeStandardSeries;

implementation

Uses TeeConst {$IFDEF D6},Types{$ENDIF};

Function GetDefaultPattern(PatternIndex:Integer):TBrushStyle;
Const MaxDefaultPatterns = 6;
      PatternPalette     : Array[1..MaxDefaultPatterns] of TBrushStyle=
	( bsHorizontal,
	  bsVertical,
	  bsFDiagonal,
	  bsBDiagonal,
	  bsCross,
	  bsDiagCross
	);
Begin
  result:=PatternPalette[1+(PatternIndex mod MaxDefaultPatterns)];
End;

{ TSeriesPointer }
Constructor TSeriesPointer.Create(AOwner:TChartSeries);
begin
   ParentChart:=AOwner.ParentChart;
//  if Assigned(ParentChart) then Canvas:=ParentChart.Canvas;
   inherited Create;
   AllowChangeSize:=True;

   FOwner:=AOwner;
   FInflate:=True;
   FHorizSize:=4;
   FVertSize:=4;
   FDark3D:=True;
   FDraw3D:=True;
   FStyle:=psRectangle;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TSeriesPointer.Load(var S: TOldStream; AOwner: TChartSeries);
begin
   ParentChart:=AOwner.ParentChart;
//  if Assigned(ParentChart) then Canvas:=ParentChart.Canvas;
   inherited Load(S);
   AllowChangeSize:=TRUE;

   FOwner:=AOwner;
   S.Read(FInflate,SizeOf(Boolean));
   S.Read(FHorizSize,SizeOf(Integer));
   S.Read(FVertSize,SizeOf(Integer));
   S.Read(FDark3D,SizeOf(Boolean));
   S.Read(FDraw3D,SizeOf(Boolean));
   S.Read(FStyle,SizeOf(TSeriesPointerStyle));
end;

Procedure TSeriesPointer.Store(var S: TOldStream);
begin
   inherited Store(S);

   S.Write(FInflate,SizeOf(Boolean));
   S.Write(FHorizSize,SizeOf(Integer));
   S.Write(FVertSize,SizeOf(Integer));
   S.Write(FDark3D,SizeOf(Boolean));
   S.Write(FDraw3D,SizeOf(Boolean));
   S.Write(FStyle,SizeOf(TSeriesPointerStyle));
end;

Function TSeriesPointer.OnCompareItems(Item: TPersistent): Boolean;
begin
   Result:=FALSE;
   if Item is TSeriesPointer then
      with TSeriesPointer(Item) do
         if Self.FInflate = FInflate then
            if Self.FHorizSize = FHorizSize then
               if Self.FVertSize = FVertSize then
                  if Self.FDark3D = FDark3D then
                     if Self.FDraw3D = FDraw3D then
                        if Self.FStyle = FStyle then
                           Result := inherited OnCompareItems(Item);
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TSeriesPointer.ChangeStyle(NewStyle:TSeriesPointerStyle);
Begin
  FStyle:=NewStyle;
end;

Procedure TSeriesPointer.ChangeHorizSize(NewSize:Integer);
Begin
  FHorizSize:=NewSize;
End;

Procedure TSeriesPointer.ChangeVertSize(NewSize:Integer);
Begin
  FVertSize:=NewSize;
End;

Procedure TSeriesPointer.CheckPointerSize(Value:Integer);
begin
//  if Value<1 then Raise ChartException.Create(TeeMsg_CheckPointerSize)
end;

type TSeriesAccess=class(TChartSeries);

Procedure TSeriesPointer.SetHorizSize(Value:Integer);
Begin
  CheckPointerSize(Value);
  TSeriesAccess(FOwner).SetIntegerProperty(FHorizSize,Value);
end;

Procedure TSeriesPointer.SetInflate(Value:Boolean);
begin
  TSeriesAccess(FOwner).SetBooleanProperty(FInflate,Value);
end;

Procedure TSeriesPointer.SetVertSize(Value:Integer);
Begin
  CheckPointerSize(Value);
  TSeriesAccess(FOwner).SetIntegerProperty(FVertSize,Value);
end;

Procedure TSeriesPointer.SetDark3D(Value:Boolean);
Begin
  TSeriesAccess(FOwner).SetBooleanProperty(FDark3D,Value);
end;

Procedure TSeriesPointer.SetDraw3D(Value:Boolean);
Begin
  TSeriesAccess(FOwner).SetBooleanProperty(FDraw3D,Value);
end;

Procedure TSeriesPointer.Change3D(Value:Boolean);
Begin
  FDraw3D:=Value;
end;

Procedure TSeriesPointer.CalcHorizMargins(Var LeftMargin,RightMargin:Integer);
begin
  if Visible and FInflate then
  begin
    LeftMargin :=MaxLong(LeftMargin, HorizSize+1);
    RightMargin:=MaxLong(RightMargin,HorizSize+1);
  end;
end;

Procedure TSeriesPointer.CalcVerticalMargins(Var TopMargin,BottomMargin:Integer);
begin
  if Visible and FInflate then
  begin
    TopMargin   :=MaxLong(TopMargin,   VertSize+1);
    BottomMargin:=MaxLong(BottomMargin,VertSize+1);
  end;
end;

Procedure TSeriesPointer.SetStyle(Value:TSeriesPointerStyle);
Begin
  if FStyle<>Value then
  begin
    FStyle:=Value;
    FOwner.Repaint;
  end;
end;

Procedure TSeriesPointer.PrepareCanvas(ACanvas:TCanvas3D; ColorValue:TColor);
var tmp : TColor;
Begin
  if (Pen.Visible) and (Pen.Color=clTeeColor) then
     ACanvas.AssignVisiblePenColor(Pen,ColorValue)
  else
     ACanvas.AssignVisiblePen(Pen);
  tmp:=Brush.Color;
  if tmp=clTeeColor then
     if Brush.Style=bsSolid then tmp:=ColorValue
                            else tmp:=clBlack;
  ACanvas.AssignBrushColor(Brush,tmp,ColorValue);
end;

Procedure TSeriesPointer.DrawPointer( ACanvas:TCanvas3D;
                                      Is3D:Boolean;
                                      px,py,tmpHoriz,tmpVert:Integer;
                                      ColorValue:TColor;
                                      AStyle:TSeriesPointerStyle);

Var PXMinus : Integer;
    PXPlus  : Integer;
    PYMinus : Integer;
    PYPlus  : Integer;

  Procedure DrawDiagonalCross;
  Begin
    With FOwner,ACanvas do
    Begin
      AssignVisiblePenColor(Pen,ColorValue);
      if Is3D then
      begin
        LineWithZ(PXMinus, PYMinus, PXPlus+1,PYPlus+1,StartZ);
        LineWithZ(PXPlus,  PYMinus, PXMinus-1,PYPlus+1,StartZ);
      end
      else
      begin
        Line(PXMinus, PYMinus, PXPlus+1,PYPlus+1);
        Line(PXPlus , PYMinus, PXMinus-1,PYPlus+1);
      end;
    end;
  end;

  Procedure DrawCross;
  Begin
    With FOwner,ACanvas do
    Begin
      AssignVisiblePenColor(Pen,ColorValue);
      if Is3D then
      begin
        VertLine3D(PX,PYMinus,PYPlus+1,StartZ);
        HorizLine3D(PXMinus,PXPlus+1,PY,StartZ);
      end
      else
      begin
        DoVertLine(PX,PYMinus,PYPlus+1);
        DoHorizLine(PXMinus,PXPlus+1,PY);
      end;
    end;
  end;

  Procedure DoTriangle3D(DeltaY:Integer);
  begin
    With FOwner,ACanvas do
    if Self.FDraw3D then
       Pyramid( True, PXMinus,PY-DeltaY,PXPlus,PY+DeltaY,StartZ,EndZ,FDark3D)
    else
       TriangleWithZ( {$IFDEF D6}Types.{$ENDIF}Point(PXMinus,PY+DeltaY),
                      {$IFDEF D6}Types.{$ENDIF}Point(PXPlus,PY+DeltaY),
                      {$IFDEF D6}Types.{$ENDIF}Point(PX,PY-DeltaY),StartZ )
  end;

Begin
  PXMinus:=PX-tmpHoriz;
  PXPlus :=PX+tmpHoriz;
  PYMinus:=PY-tmpVert;
  PYPlus :=PY+tmpVert;
  With FOwner,ACanvas do
  Begin
    if Is3D then
    Case AStyle of
    psRectangle: if Self.FDraw3D then
                    Cube(PXMinus,PXPlus,PYMinus,PYPlus,StartZ,EndZ,FDark3D)
                 else
                    RectangleWithZ({$IFDEF D6}Types.{$ENDIF}Rect(PXMinus,PYMinus,PXPlus+1,PYPlus+1),StartZ);
       psCircle: if Self.FDraw3D and SupportsFullRotation then
                    Sphere(PX,PY,MiddleZ,tmpHoriz)
                 else
                    EllipseWithZ(PXMinus,PYMinus,PXPlus,PYPlus,StartZ);
     psTriangle: DoTriangle3D( tmpVert);
 psDownTriangle: DoTriangle3D(-tmpVert);
        psCross: DrawCross;
    psDiagCross: DrawDiagonalCross;
         psStar: Begin DrawCross; DrawDiagonalCross; end;
      psDiamond: PlaneWithZ( {$IFDEF D6}Types.{$ENDIF}Point(PXMinus,PY),
                             {$IFDEF D6}Types.{$ENDIF}Point(PX,PYMinus),
                             {$IFDEF D6}Types.{$ENDIF}Point(PXPlus,PY),
                             {$IFDEF D6}Types.{$ENDIF}Point(PX,PYPlus),StartZ);
     psSmallDot: Pixels3D[PX,PY,MiddleZ]:=ColorValue
    end
    else
    Case AStyle of
    psRectangle: Rectangle(PXMinus,PYMinus,PXPlus+1,PYPlus+1);
       psCircle: Ellipse(PXMinus,PYMinus,PXPlus,PYPlus);
     psTriangle: Polygon([ {$IFDEF D6}Types.{$ENDIF}Point(PXMinus,PYPlus),
                           {$IFDEF D6}Types.{$ENDIF}Point(PXPlus,PYPlus),
                           {$IFDEF D6}Types.{$ENDIF}Point(PX,PYMinus)]);
 psDownTriangle: Polygon([ {$IFDEF D6}Types.{$ENDIF}Point(PXMinus,PYMinus),
                           {$IFDEF D6}Types.{$ENDIF}Point(PXPlus,PYMinus),
                           {$IFDEF D6}Types.{$ENDIF}Point(PX,PYPlus)]);
        psCross: DrawCross;
    psDiagCross: DrawDiagonalCross;
         psStar: Begin DrawCross; DrawDiagonalCross; end;
      psDiamond: Polygon([{$IFDEF D6}Types.{$ENDIF}Point(PXMinus,PY),
                          {$IFDEF D6}Types.{$ENDIF}Point(PX,PYMinus),
                          {$IFDEF D6}Types.{$ENDIF}Point(PXPlus,PY),
                          {$IFDEF D6}Types.{$ENDIF}Point(PX,PYPlus)] );
     psSmallDot: Pixels[PX,PY]:=ColorValue;
    end;
  end;
end;

Procedure TSeriesPointer.Draw(px,py:Integer; ColorValue:TColor; AStyle:TSeriesPointerStyle);
Begin
  DrawPointer(FOwner.ParentChart.Canvas,FOwner.ParentChart.View3D,px,py,FHorizSize,FVertSize,ColorValue,AStyle);
end;

Procedure TSeriesPointer.Assign(Source:TPersistent);
begin
  if Source is TSeriesPointer then
  With TSeriesPointer(Source) do
  begin
    Self.FDark3D    :=FDark3D;
    Self.FDraw3D    :=FDraw3D;
    Self.FHorizSize :=FHorizSize;
    Self.FInflate   :=FInflate;
    Self.FStyle     :=FStyle;
    Self.FVertSize  :=FVertSize;
  end;
  inherited;
end;

Procedure TSeriesPointer.DrawLegendShape(AColor:TColor; Const Rect:TRect; DrawPen:Boolean);
var tmpHoriz : Integer;
    tmpVert  : Integer;
begin
  PrepareCanvas(FOwner.ParentChart.Canvas,AColor);
  With Rect do
  begin
    if DrawPen then
    begin
      tmpHoriz:=(Right-Left) div 3;
      tmpVert :=(Bottom-Top) div 3;
    end
    else
    begin
      tmpHoriz:=1+((Right-Left) div 2);
      tmpVert :=1+((Bottom-Top) div 2);
    end;
    DrawPointer(FOwner.ParentChart.Canvas,
                       False, (Left+Right) div 2,(Top+Bottom) div 2,
                       MinLong(HorizSize,tmpHoriz),
                       MinLong(VertSize,tmpVert),AColor,FStyle);
  end;
end;

{ TCustomLineSeries }
Function TCustomLineSeries.GetLinePen:TChartPen;
Begin
  result:=Pen;
end;

{ TCustomSeries }
Constructor TCustomSeries.Create(AOwner: TComponent);
begin
   inherited;
   ClickableLine:=True;
   FColorEachLine:=True;
   DrawBetweenPoints:=True;
   FPointer:=TSeriesPointer.Create(Self);
   FAreaLinesPen:=CreateChartPen;
   FAreaBrush:=TChartBrush.Create(CanvasChanged);
   FAreaColor:=clTeeColor;
   FDark3D:=True;
end;

Destructor TCustomSeries.Destroy;
begin
   FAreaBrush.Free;
   FAreaLinesPen.Free;
   FPointer.Free;
   inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TCustomSeries.Load(var S: TOldStream; AOwner: TComponent);
var byteUnPack : Byte;
    intValue   : Integer;
begin
   inherited Load(S,AOwner);
   FPointer:=TSeriesPointer.Load(S,Self);
   FAreaLinesPen:=TChartPen.Load(S,CanvasChanged);
   FAreaBrush:=TChartBrush.Load(S,CanvasChanged);

   S.Read(FAreaColor,SizeOf(TColor));

   S.Read(byteUnPack,SizeOf(Byte));
   FStacked:=TCustomSeriesStack(byteUnPack and (co_Bit0 or co_Bit1));
   FDark3D:=(byteUnPack and co_Bit2) <> 0;
   FClickableLine:=(byteUnPack and co_Bit3) <> 0;
   FDrawArea:=(byteUnPack and co_Bit4) <> 0;
   FDrawLine:=(byteUnPack and co_Bit5) <> 0;
   FInvertedStairs:=(byteUnPack and co_Bit6) <> 0;
   FStairs:=(byteUnPack and co_Bit7) <> 0;

   // Unpack values (this method slightly limits the maximum value of FLineHeight)
   S.Read(intValue,SizeOf(Integer));
   FColorEachLine:=(intValue and $00000001) <> 0;
   FLineHeight:=intValue shr 1;
end;

Procedure TCustomSeries.Store(var S: TOldStream);
var bytePack   : Byte;
    intValue   : Integer;
begin
   inherited Store(S);
   FPointer.Store(S);
   FAreaLinesPen.Store(S);
   FAreaBrush.Store(S);

   S.Write(FAreaColor,SizeOf(TColor));

   bytePack:=Ord(FStacked);
   if FDark3D then
      bytePack:=bytePack or co_Bit2;
   if FClickableLine then
      bytePack:=bytePack or co_Bit3;
   if FDrawArea then
      bytePack:=bytePack or co_Bit4;
   if FDrawLine then
      bytePack:=bytePack or co_Bit5;
   if FInvertedStairs then
      bytePack:=bytePack or co_Bit6;
   if FStairs then
      bytePack:=bytePack or co_Bit7;
   S.Write(bytePack,SizeOf(Byte));

   // Pack values (this method slightly limits the maximum value of FLineHeight)
   intValue:=FLineHeight shl 1;
   if FColorEachLine then
      intValue:=intValue or co_Bit0;
   S.Write(intValue,SizeOf(Integer));
end;

Function TCustomSeries.CreateDeterminant(Determinant: String): String;
begin
   Result:=Determinant;
end;

Function TCustomSeries.OnCompareItems(Item: TChartSeries): Boolean;
begin
   Result:=FALSE;
   if Item is TCustomSeries then
     with TCustomSeries(Item) do
       if Self.FAreaColor = FAreaColor then
         if Self.FColorEachLine = FColorEachLine then
           if Self.FDark3D = FDark3D then
             if Self.FClickableLine = FClickableLine then
               if Self.FDrawArea = FDrawArea then
                 if Self.FDrawLine = FDrawLine then
                   if Self.FInvertedStairs = FInvertedStairs then
                     if Self.FStairs = FStairs then
                       if Self.FLineHeight = FLineHeight then
                         if Self.FStacked = FStacked then
                           if Self.FPointer.OnCompareItems(FPointer) then
                             if Self.FAreaLinesPen.OnCompareItems(FAreaLinesPen) then
                               if Self.FAreaBrush.OnCompareItems(FAreaBrush) then
                                 Result := inherited OnCompareItems(Item);
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TCustomSeries.DrawMark( ValueIndex:Integer; Const St:String;
                                  APosition:TSeriesMarkPosition);
begin
  Marks.ZPosition:=StartZ;
  Marks.ApplyArrowLength(APosition);
  inherited;
end;

Function TCustomSeries.ClickedPointer( ValueIndex,tmpX,tmpY:Integer;
                                       x,y:Integer):Boolean;
begin
  result:=(Abs(tmpX-X)<FPointer.FHorizSize) and
          (Abs(tmpY-Y)<FPointer.FVertSize);
end;

Procedure TCustomSeries.Assign(Source:TPersistent);
begin
  if Source is TCustomSeries then
  With TCustomSeries(Source) do
  begin
    Self.ClickableLine   :=ClickableLine;
    Self.AreaChartBrush  :=FAreaBrush;
    Self.FAreaColor      :=FAreaColor;
    Self.AreaLinesPen    :=FAreaLinesPen;
    Self.FColorEachLine  :=ColorEachLine;
    Self.FDark3D         :=FDark3D;
    Self.FDrawArea       :=FDrawArea;
    Self.FDrawLine       :=FDrawLine;
    Self.FInvertedStairs :=FInvertedStairs;
    Self.FLineHeight     :=FLineHeight;
    Self.Pointer         :=FPointer;
    Self.FStacked        :=FStacked;
    Self.FStairs         :=FStairs;
  end;
  inherited;
end;

Function TCustomSeries.Clicked(x,y:Integer):Integer;
var t        : Integer;
    OldXPos  : Integer;
    OldYPos  : Integer;
    tmpX     : Integer;
    tmpY     : Integer;
    P        : TPoint;

    Function CheckPointInLine:Boolean;

      Function PointInVertLine(x0,y0,y1:Integer):Boolean;
      begin
        result:=PointInLine(P,x0,y0,x0,y1);
      end;

      Function PointInHorizLine(x0,y0,x1:Integer):Boolean;
      begin
        result:=PointInLine(P,x0,y0,x1,y0);
      end;

    begin
      With ParentChart do
      if View3D then

         result:=PointInPolygon( P,[ {$IFDEF D6}Types.{$ENDIF}Point(tmpX,tmpY),
                                     {$IFDEF D6}Types.{$ENDIF}Point(tmpX+SeriesWidth3D,tmpY-SeriesHeight3D),
                                     {$IFDEF D6}Types.{$ENDIF}Point(OldXPos+SeriesWidth3D,OldYPos-SeriesHeight3D),
                                     {$IFDEF D6}Types.{$ENDIF}Point(OldXPos,OldYPos) ])
      else
         if Stairs then
         begin
            if FInvertedStairs then result:= PointInVertLine(OldXPos,OldYPos,tmpY) or
                                             PointInHorizLine(OldXPos,tmpY,tmpX)
                               else result:= PointInHorizLine(OldXPos,OldYPos,tmpX) or
                                             PointInVertLine(tmpX,OldYPos,tmpY);
         end
         else
            result:=PointInLine(P,tmpX,tmpY,OldXPos,OldYPos)
    end;

begin
  if Assigned(ParentChart) then ParentChart.Canvas.Calculate2DPosition(X,Y,StartZ);
  result:=inherited Clicked(x,y);
  if (result=TeeNoPointClicked) and
     (FirstValueIndex>-1) and (LastValueIndex>-1) then
  begin
    OldXPos:=0;
    OldYPos:=0;
    OldBottomPos:=0;
    P.X:=X;
    P.Y:=Y;
    for t:=FirstValueIndex to LastValueIndex do
    begin
      tmpX:=CalcXPos(t);
      tmpY:=CalcYPos(t);
      if FPointer.Visible then
      begin
        if ClickedPointer(t,tmpX,tmpY,x,y) then
        begin
          if Assigned(FOnClickPointer) then FOnClickPointer(Self,t,x,y);
          result:=t;
          break;
        end;
      end;
      if (tmpX=X) and (tmpY=Y) then
      begin
        result:=t;
        break;
      end;
      if (t>FirstValueIndex) and ClickableLine then
         if CheckPointInLine or
            ( FDrawArea and
               PointInPolygon( P,[ {$IFDEF D6}Types.{$ENDIF}Point(OldXPos,OldYPos), Point(tmpX,tmpY),
                                   {$IFDEF D6}Types.{$ENDIF}Point(tmpX,GetOriginPos(t)),
                                   {$IFDEF D6}Types.{$ENDIF}Point(OldXPos,GetOriginPos(t-1)) ] )
            ) then
         begin
           result:=t-1;
           break;
         end;
      OldXPos:=tmpX;
      OldYPos:=tmpY;
      OldBottomPos:=BottomPos;
    end;
  end;
end;

Procedure TCustomSeries.SetDrawArea(Value:Boolean);
Begin
  SetBooleanProperty(FDrawArea,Value);
end;

Procedure TCustomSeries.SetPointer(Value:TSeriesPointer);
Begin
  FPointer.Assign(Value);
end;

Procedure TCustomSeries.SetAreaLinesPen(Value:TChartPen);
Begin
  FAreaLinesPen.Assign(Value);
end;

Procedure TCustomSeries.SetLineHeight(Value:Integer);
Begin
  SetIntegerProperty(FLineHeight,Value);
end;

Procedure TCustomSeries.SetStairs(Value:Boolean);
Begin
  SetBooleanProperty(FStairs,Value);
end;

Procedure TCustomSeries.SetInvertedStairs(Value:Boolean);
Begin
  SetBooleanProperty(FInvertedStairs,Value);
end;

Procedure TCustomSeries.SetAreaColor(Value:TColor);
Begin
  SetColorProperty(FAreaColor,Value);
end;

Procedure TCustomSeries.SetAreaBrushStyle(Value:TBrushStyle);
Begin
  FAreaBrush.Style:=Value;
end;

Function TCustomSeries.GetLineBrush:TBrushStyle;
Begin
  result:=Brush.Style;
end;

Procedure TCustomSeries.SetLineBrush(Value:TBrushStyle);
Begin
  Brush.Style:=Value;
end;

Procedure TCustomSeries.DrawLegendShape(ValueIndex:Integer; Const Rect:TRect);
Var tmpColor : TColor;

    Procedure DrawLine(DrawRectangle:Boolean);
    begin
      LinePrepareCanvas(ParentChart.Canvas,tmpColor);
      With ParentChart.Canvas do
      if DrawRectangle then DoRectangle(Rect)
      else
      With Rect do DoHorizLine(Left,Right,(Top+Bottom) div 2);
    end;

begin
  if ValueIndex=TeeAllValues then tmpColor:=SeriesColor
                             else tmpColor:=ValueColor[ValueIndex];
  if FPointer.Visible then
  begin
    if FDrawLine then DrawLine(False);
    FPointer.DrawLegendShape(tmpColor,Rect,LinePen.Visible);
  end
  else
  if FDrawLine and (not FDrawArea) then
     DrawLine(ParentChart.View3D)
  else
     inherited
end;

procedure TCustomSeries.LinePrepareCanvas(tmpCanvas:TCanvas3D; tmpColor:TColor);
begin
  with tmpCanvas do
  begin
    if MonoChrome then tmpColor:=clWhite;
    if ParentChart.View3D then
    begin
      if Assigned(Self.Brush.Image.Graphic) then
         Brush.Bitmap:=Self.Brush.Image.Bitmap
      else
      begin
        Brush.Style:=LineBrush;
        Brush.Color:=tmpColor;
      end;
      AssignVisiblePen(LinePen);
    end
    else
    begin
      Brush.Style:=bsClear;
      AssignVisiblePenColor(LinePen,tmpColor);
    end;
    ParentChart.CheckPenWidth(tmpCanvas.Pen);
  end;
end;

Procedure TCustomSeries.SetParentChart(Const Value:TCustomAxisPanel);
begin
  inherited;
  if Assigned(Value) then Pointer.ParentChart:=Value;
end;

Function TCustomSeries.GetAreaBrushColor(AColor:TColor):TColor;
begin
  if ColorEachPoint or (FAreaColor=clTeeColor) then
     result:=AColor
  else
     result:=FAreaColor;
end;

procedure TCustomSeries.DrawValue(ValueIndex:Integer);
Var x : Integer;
    y : Integer;

  Function CalcYPosLeftRight(Const YLimit:Double; AnotherIndex:Integer):Integer;
  var tmpPredValueX : Double;
      tmpPredValueY : Double;
      tmpDif        : Double;
  begin
    tmpPredValueX:=XValues.Value[AnotherIndex];
    tmpDif:=XValues.Value[ValueIndex]-tmpPredValueX;
    With ParentChart do
    if tmpDif=0 then result:=CalcYPos(AnotherIndex)
    else
    begin
      tmpPredValueY:=YValues.Value[AnotherIndex];
      result:=CalcYPosValue( 1.0*tmpPredValueY+(YLimit-tmpPredValueX)*
                             (YValues.Value[ValueIndex]-tmpPredValueY)/tmpDif );
    end;
  end;

var tmpColor    : TColor;
    IsLastValue : Boolean;

   Procedure InternalDrawArea(BrushColor:TColor);
   var tmpY      : Integer;
       tmpBottom : Integer;
       tmpR      : TRect;
   Begin
     With ParentChart do
     Begin
       SetBrushCanvas(BrushColor,FAreaBrush,SeriesColor);
       if View3D and IsLastValue then
          Canvas.RectangleZ(X,Y,BottomPos,StartZ,EndZ);
       if Stairs then
       begin
         if FInvertedStairs then
         begin
           tmpY:=Y;
           tmpBottom:=BottomPos;
         end
         else
         begin
           tmpY:=OldY;
           tmpBottom:=OldBottomPos;
         end;
         tmpR:={$IFDEF D6}Types.{$ENDIF}Rect(OldX,tmpBottom,X+1,tmpY);
         With Canvas do
         begin
           if View3D then
           begin
             RectangleWithZ(tmpR,StartZ);
             if SupportsFullRotation then RectangleWithZ(tmpR,EndZ);
           end
           else DoRectangle(tmpR);
         end;
       end
       else
       With Canvas do
       begin
         PlaneWithZ( {$IFDEF D6}Types.{$ENDIF}Point(OldX,OldBottomPos),
                     {$IFDEF D6}Types.{$ENDIF}Point(OldX,OldY),
                     {$IFDEF D6}Types.{$ENDIF}Point(X,Y),
                     {$IFDEF D6}Types.{$ENDIF}Point(X,BottomPos),StartZ);
         if SupportsFullRotation then
            PlaneWithZ( {$IFDEF D6}Types.{$ENDIF}Point(OldX,OldBottomPos),
                        {$IFDEF D6}Types.{$ENDIF}Point(X,BottomPos),
                        {$IFDEF D6}Types.{$ENDIF}Point(X,Y),
                        {$IFDEF D6}Types.{$ENDIF}Point(OldX,OldY),EndZ);
         if LinePen.Visible then
         begin
           AssignVisiblePen(LinePen);
           LineWithZ(OldX,OldY,X,Y,StartZ);
         end;
       end;
     end;
   end;

   Procedure DrawPoint(DrawOldPointer:Boolean);
   var tmpPoint     : TPoint;
       tmpOldP      : TPoint;
       tmpDifX      : Integer;
       P4           : TFourPoints;
       OldDarkColor : TColor;
       tmpDark3D    : Boolean;
   Begin
     if ((x<>OldX) or (y<>OldY)) and (tmpColor<>clNone) then { <-- if not null }
     With ParentChart,Canvas do
     Begin
       if View3D then
       Begin
         if FDrawArea or FDrawLine then
         Begin
           Brush.Color:=GetAreaBrushColor(tmpColor);
           OldDarkColor:=Brush.Color;

           AssignVisiblePen(LinePen);
           if LinePen.Visible then CheckPenWidth(Pen);
           Brush.Style:=LineBrush;
           if Assigned(Self.Brush.Image.Graphic) then
              Brush.Bitmap:=Self.Brush.Image.Bitmap;

           tmpPoint.X  :=X;
           tmpPoint.Y  :=Y;
           tmpOldP.X   :=OldX;
           tmpOldP.Y   :=OldY;
           if Stairs then
           Begin
             if FInvertedStairs then
             begin
               if FDark3D then Brush.Color:=ApplyDark(Brush.Color,DarkColorQuantity);
               RectangleZ( tmpOldP.X,tmpOldP.Y, Y,StartZ,EndZ);
               if FDark3D then Brush.Color:=OldDarkColor;
               RectangleY( tmpPoint.X, tmpPoint.Y,OldX,StartZ,EndZ);
             end
             else
             begin
               RectangleY( tmpOldP.X, tmpOldP.Y, X, StartZ, EndZ);
               if FDark3D then Brush.Color:=ApplyDark(Brush.Color,DarkColorQuantity);
               RectangleZ( tmpPoint.X,tmpPoint.Y, OldY,StartZ,EndZ);
               if FDark3D then Brush.Color:=OldDarkColor;
             end;
           end
           else
           begin
             if (FLineHeight>0) and (not FDrawArea) then
             begin
               P4[0]:=tmpPoint;
               P4[1]:=tmpOldP;
               P4[2].X:=tmpOldP.X;
               P4[2].Y:=tmpOldP.Y+FLineHeight;
               P4[3].X:=tmpPoint.X;
               P4[3].Y:=tmpPoint.Y+FLineHeight;
               PlaneFour3D(P4,StartZ,StartZ);
               if IsLastValue then
                  RectangleZ(tmpPoint.X,tmpPoint.Y,tmpPoint.Y+FLineHeight,StartZ,EndZ);
             end;
             tmpDark3D:=FDark3D and (not SupportsFullRotation);
             if tmpDark3D then
             begin
               tmpDifX:=tmpPoint.X-tmpOldP.X;
               if (tmpDifX<>0) and
                  (tmpDark3DRatio<>0) and
                  ((tmpOldP.Y-tmpPoint.Y)/tmpDifX > tmpDark3DRatio) then
               begin
                 Brush.Color:=ApplyDark(Brush.Color,DarkColorQuantity);
                 if (FLineHeight>0) and (not FDrawArea) then {special case}
                 begin
                   Inc(tmpPoint.Y,FLineHeight);
                   Inc(tmpOldP.Y,FLineHeight);
                 end;
               end;
             end;
             if Monochrome then Brush.Color:=clWhite;
             Plane3D(tmpPoint,tmpOldP,StartZ,EndZ);
             if tmpDark3D then Brush.Color:=OldDarkColor;
           end;
         end;
       end;
       if FDrawArea then
       Begin
         Brush.Color:=GetAreaBrushColor(tmpColor);
         if (FAreaLinesPen.Color=clTeeColor) or (not FAreaLinesPen.Visible) then
            AssignVisiblePenColor(FAreaLinesPen,tmpColor)
         else
            AssignVisiblePen(FAreaLinesPen);
         InternalDrawArea(Brush.Color);
       end
       else
       if (not View3D) and FDrawLine then
       Begin
         if ColorEachLine then LinePrepareCanvas(Canvas,tmpColor)
                          else LinePrepareCanvas(Canvas,SeriesColor);
         if Stairs then
         begin
           if FInvertedStairs then DoVertLine(OldX,OldY,Y)
                              else DoHorizLine(OldX,X,OldY);
           LineTo(X,Y);
         end
         else Line(OldX,OldY,X,Y);
       end;
     end;

     { pointers }
     if FPointer.Visible and DrawOldPointer then
     Begin
       if OldColor<>clNone then { <-- if not null }
          DrawPointer(OldX,OldY,OldColor,Pred(ValueIndex));
       if IsLastValue and (tmpColor<>clNone) then {<-- if not null }
          DrawPointer(X,Y,tmpColor,ValueIndex);
     end;
   end;

Const MaxShortInt=32767;
Begin
  With ParentChart.Canvas do
  Begin
    tmpColor:=ValueColor[ValueIndex];
    X:=CalcXPos(ValueIndex);
    Y:=CalcYPos(ValueIndex);
    { Win95 limitation. only 16bit coordinates }
    if (X>-MaxShortInt) and (X<MaxShortInt) and
       (Y>-MaxShortInt) and (Y<MaxShortInt) then
    Begin
      Pen.Color:=clBlack;
      Brush.Color:=tmpColor;
      if OldColor=clNone then { if null }
      begin
        OldX:=X;
        OldY:=Y;
      end;
      IsLastValue:=ValueIndex=LastValueIndex;
      BottomPos:=GetOriginPos(ValueIndex);
      if ValueIndex=FirstValueIndex then { first point }
      Begin
        if FDark3D then
        With ParentChart do
           if SeriesWidth3D<>0 then
              tmpDark3DRatio:=Abs(SeriesHeight3D/SeriesWidth3D)
           else
              tmpDark3DRatio:=1;
        if ValueIndex>0 then { previous point outside left }
        Begin
          if FDrawArea then
          begin
            OldX:=CalcXPos(Pred(ValueIndex));
            OldY:=CalcYPos(Pred(ValueIndex));
            OldBottomPos:=GetOriginPos(Pred(ValueIndex));
          end
          else
          begin
            if GetHorizAxis.Inverted then OldX:=ParentChart.ChartRect.Right
                                     else OldX:=ParentChart.ChartRect.Left;
            if Stairs Then
               OldY:=CalcYPos(Pred(ValueIndex))
            else
               OldY:=CalcYPosLeftRight(XScreenToValue(OldX),Pred(ValueIndex));
          end;
          if not IsNull(Pred(ValueIndex)) then DrawPoint(False);
        end;
        if IsLastValue and FPointer.Visible then
           DrawPointer(X,Y,tmpColor,ValueIndex);
        if SupportsFullRotation and FDrawArea and ParentChart.View3D then
           RectangleZ(X,Y,BottomPos,StartZ,EndZ);
      end
      else DrawPoint(True);
      OldX:=X;
      OldY:=Y;
      OldBottomPos:=BottomPos;
    end;
    OldColor:=tmpColor;
  end;
end;

Procedure TCustomSeries.DrawPointer(AX,AY:Integer; AColor:TColor; ValueIndex:Integer);
var tmpStyle : TSeriesPointerStyle;
begin
  FPointer.PrepareCanvas(ParentChart.Canvas,AColor);
  if Assigned(FOnGetPointerStyle) then
     tmpStyle:=FOnGetPointerStyle(Self,ValueIndex)
  else
     tmpStyle:=FPointer.FStyle;
  FPointer.Draw(AX,AY,AColor,tmpStyle);
end;

class Function TCustomSeries.GetEditorClass:String;
Begin
  result:='TCustomSeriesEditor'; { <-- dont translate ! }
end;

Procedure TCustomSeries.CalcHorizMargins(Var LeftMargin,RightMargin:Integer);
begin
  inherited;
  FPointer.CalcHorizMargins(LeftMargin,RightMargin);
  if Stairs and FDrawLine then
  begin
    LeftMargin :=MaxLong(LeftMargin,LinePen.Width);
    RightMargin:=MaxLong(RightMargin,LinePen.Width);
  end;
end;

Procedure TCustomSeries.CalcVerticalMargins(Var TopMargin,BottomMargin:Integer);
begin
  inherited;
  FPointer.CalcVerticalMargins(TopMargin,BottomMargin);
  if Stairs and FDrawLine then
  begin
    TopMargin   :=MaxLong(TopMargin,LinePen.Width);
    BottomMargin:=MaxLong(BottomMargin,LinePen.Width+1);
  end;
  if (FLineHeight>0) and (not FDrawArea) then
     if FLineHeight>BottomMargin then BottomMargin:=FLineHeight;
  if Marks.Visible then TopMargin:=MaxLong(TopMargin,Marks.ArrowLength);
end;

Procedure TCustomSeries.SetDark3D(Value:Boolean);
begin
  SetBooleanProperty(FDark3D,Value);
end;

Procedure TCustomSeries.CalcZOrder;
Begin
  if FStacked=cssNone then inherited
                      else IZOrder:=ParentChart.MaxZOrder;
End;

Function TCustomSeries.PointOrigin(ValueIndex:Integer; SumAll:Boolean):Double;
var t         : Integer;
    tmpSeries : TChartSeries;
    tmp       : Double;
Begin
  result:=0;
  if Assigned(ParentChart) then
  with ParentChart do
  for t:=0 to SeriesCount-1 do
  Begin
    tmpSeries:=Series[t];
    if (not SumAll) and (tmpSeries=Self) then Break
    else
    With tmpSeries do
    if Active and SameClass(Self) and (Count>ValueIndex) then
    begin
      tmp:=GetOriginValue(ValueIndex);
      if tmp>0 then result:=result+tmp;
    end;
  end;
end;

Function TCustomSeries.CalcStackedYPos(ValueIndex:Integer; Value:Double):Integer;
var tmp : Double;
begin
  Value:=Value+PointOrigin(ValueIndex,False);
  if FStacked=cssStack then
     result:=MinLong(GetVertAxis.IEndPos,CalcYPosValue(Value))
  else
  begin
    tmp:=PointOrigin(ValueIndex,True);
    if tmp<>0 then result:=CalcYPosValue(Value*100.0/tmp)
              else result:=0;
  end;
end;

Function TCustomSeries.GetOriginPos(ValueIndex:Integer):Integer;
Begin
  if (FStacked=cssNone) or (FStacked=cssOverlap) then
     result:=GetVertAxis.IEndPos
  else
     result:=CalcStackedYPos(ValueIndex,0);
end;

Function TCustomSeries.MaxYValue:Double;
var t : Integer;
Begin
  if FStacked=cssStack100 then result:=100
  else
  begin
    result:=inherited MaxYValue;
    if FStacked=cssStack then
    for t:=0 to Count-1 do
        result:=MaxDouble(result,PointOrigin(t,False)+YValues.Value[t]);
  end;
end;

Function TCustomSeries.MinYValue:Double;
Begin
  if FStacked=cssStack100 then result:=0
                          else result:=inherited MinYValue;
end;

Function TCustomSeries.CalcYPos(ValueIndex:Integer):Integer;
Begin
  if (FStacked=cssNone) or (FStacked=cssOverlap) then
     result:=inherited CalcYPos(ValueIndex)
  else
     result:=CalcStackedYPos(ValueIndex,YValues.Value[ValueIndex]);
End;

procedure TCustomSeries.SetStacked(Value: TCustomSeriesStack);

  Procedure SetOther;
  var t : Integer;
  Begin
    if Assigned(ParentChart) then
    with ParentChart do
    for t:=0 to SeriesCount-1 do
      if Self is Series[t].ClassType then
         TCustomSeries(Series[t]).FStacked:=FStacked;
  end;

Begin
  if Value<>FStacked then
  Begin
    FStacked:=Value;
    SetOther;
    Repaint;
  end;
end;

function TCustomSeries.GetAreaBrush: TBrushStyle;
begin
  result:=FAreaBrush.Style;
end;

procedure TCustomSeries.SetAreaBrush(Value: TChartBrush);
begin
  FAreaBrush.Assign(Value);
end;

procedure TCustomSeries.SetColorEachLine(const Value: Boolean);
begin
  SetBooleanProperty(FColorEachLine,Value);
end;

{ TLineSeries }
Constructor TLineSeries.Create(AOwner: TComponent);
Begin
  inherited;
  FDrawLine:=True;
  AllowSinglePoint:=False;
  FPointer.Visible:=False;
end;

Procedure TLineSeries.PrepareLegendCanvas( ValueIndex:Integer; Var BackColor:TColor;
                                           Var BrushStyle:TBrushStyle);
Begin
  ParentChart.Canvas.AssignVisiblePen(LinePen);
  BrushStyle:=LineBrush;
end;

Procedure TLineSeries.Assign(Source:TPersistent);
begin
  inherited;
  if not (Source is Self.ClassType) then FPointer.Visible:=False;
  FDrawArea:= False;
  FDrawLine:= True;
end;

class Procedure TLineSeries.SetSubGallery(ASeries:TChartSeries; Index:Integer);
begin
  With TLineSeries(ASeries) do
  Case Index of
    1: Stairs:=True;
    2: Pointer.Visible:=True;
    3: LineHeight:=5;
    4: LineBrush:=bsClear;
    5: ColorEachPoint:=True;
    6: Marks.Visible:=True;
    7: Pen.Visible:=False;
  end;
end;

class Procedure TLineSeries.CreateSubGallery(AddSubChart:TChartSubGalleryProc);
begin
  inherited;
  AddSubChart(TeeMsg_Stairs);
  AddSubChart(TeeMsg_Points);
  AddSubChart(TeeMsg_Height);
  AddSubChart(TeeMsg_Hollow);
  AddSubChart(TeeMsg_Colors);
  AddSubChart(TeeMsg_Marks);
  AddSubChart(TeeMsg_NoBorder);
end;

{ THorizLineSeries }
Constructor THorizLineSeries.Create(AOwner: TComponent);
begin
  inherited;
  SetHorizontal;
  CalcVisiblePoints:=False; { avoid bug first point and scroll }
  XValues.Order:=loNone;
  YValues.Order:=loAscending;
end;

{ TPointSeries }
Constructor TPointSeries.Create(AOwner: TComponent);
begin
   inherited;
   SetFixed;
   Marks.ArrowLength:=0;
end;

Procedure TPointSeries.Assign(Source:TPersistent);
begin
  inherited;
  SetFixed;
end;

Procedure TPointSeries.SetFixed;
begin
  FPointer.Visible :=True;
  FDrawArea        :=False;
  FDrawLine        :=False;
  ClickableLine    :=False;
end;

class Function TPointSeries.GetEditorClass:String;
begin
  result:='TSeriesPointerEditor'; { <-- do not translate }
end;

Procedure TPointSeries.PrepareForGallery(IsEnabled:Boolean);
begin
  inherited;
  Pointer.HorizSize:=6;
  Pointer.VertSize:=6;
  With ParentChart do
  if (SeriesCount>1) and (Self=Series[1]) then Pointer.Style:=psTriangle;
end;

Procedure TPointSeries.SetColorEachPoint(Value:Boolean);
begin
  inherited;
  if Value then Pointer.Brush.Color:=clTeeColor;
end;

class Procedure TPointSeries.CreateSubGallery(AddSubChart:TChartSubGalleryProc);
begin
  inherited;
  AddSubChart(TeeMsg_Colors);
  AddSubChart(TeeMsg_Marks);
  AddSubChart(TeeMsg_Hollow);
  AddSubChart(TeeMsg_NoBorder);
  if CanDoExtra then
  begin
    AddSubChart(TeeMsg_Point2D);
    AddSubChart(TeeMsg_Triangle);
    AddSubChart(TeeMsg_Star);
    AddSubChart(TeeMsg_Circle);
    AddSubChart(TeeMsg_DownTri);
    AddSubChart(TeeMsg_Cross);
    AddSubChart(TeeMsg_Diamond);
  end;
end;

class Procedure TPointSeries.SetSubGallery(ASeries:TChartSeries; Index:Integer);
begin
  With TPointSeries(ASeries) do
  Case Index of
    1: ColorEachPoint:=True;
    2: Marks.Visible:=True;
    3: Pointer.Brush.Style:=bsClear;
    4: Pointer.Pen.Visible:=False;
  else
  if CanDoExtra then
    Case Index of
      5: Pointer.Draw3D:=False;
      6: Pointer.Style:=psTriangle;
      7: Pointer.Style:=psStar;
      8: With Pointer do
         begin
           Style:=psCircle;
           HorizSize:=8;
           VertSize:=8;
         end;
      9: Pointer.Style:=psDownTriangle;
     10: Pointer.Style:=psCross;
     11: Pointer.Style:=psDiamond;
    end;
  end;
end;

class function TPointSeries.CanDoExtra: Boolean;
begin
  result:=True;
end;

{ TAreaSeries }
Constructor TAreaSeries.Create(AOwner: TComponent);
begin
   inherited;
   FDrawArea:=True;
   AllowSinglePoint:=False;
   FPointer.Visible:=False;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TAreaSeries.Load(var S: TOldStream; AOwner: TComponent);
begin
   inherited Load(S,AOwner);
   S.Read(FUseOrigin,SizeOf(Boolean));
   S.Read(FOrigin,SizeOf(Double));
end;

Procedure TAreaSeries.Store(var S: TOldStream);
begin
   inherited Store(S);
   S.Write(FUseOrigin,SizeOf(Boolean));
   S.Write(FOrigin,SizeOf(Double));
end;

Function TAreaSeries.CreateDeterminant(Determinant: String): String;
begin
   Result:=Determinant;
end;

Function TAreaSeries.OnCompareItems(Item: TCustomSeries): Boolean;
begin
   Result:=FALSE;
   if Item is TAreaSeries then
      with TAreaSeries(Item) do
         if Self.FUseOrigin = UseYOrigin then
            if Self.FUseOrigin then
               begin
                  if Self.FOrigin = YOrigin then
                     Result := inherited OnCompareItems(Item);
               end
            else
               Result := inherited OnCompareItems(Item);
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TAreaSeries.PrepareLegendCanvas( ValueIndex:Integer; Var BackColor:TColor;
                                           Var BrushStyle:TBrushStyle);
begin
   BackColor:=GetAreaBrushColor(ParentChart.Canvas.Brush.Color);
   ParentChart.Canvas.AssignVisiblePen(FAreaLinesPen);
   BrushStyle:=FAreaBrush.Style;
end;

Procedure TAreaSeries.Assign(Source:TPersistent);
begin
   inherited;
   FDrawArea:=True;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
// FDrawLine:=True;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
   if Source is Self.ClassType then
      with TAreaSeries(Source) do
         begin
            Self.FUseOrigin :=UseYOrigin;
            Self.FOrigin    :=YOrigin;
         end
   else
      FPointer.Visible:=False;
end;

class Function TAreaSeries.GetEditorClass:String;
begin
   result:='TAreaSeriesEditor'; { <-- dont translate ! }
end;

function TAreaSeries.GetMultiArea: TMultiArea;
begin
  Case Stacked of
    cssStack: result:=maStacked;
    cssStack100: result:=maStacked100;
  else
    result:=maNone;
  end;
end;

Procedure TAreaSeries.SetMultiArea(Value:TMultiArea);
Begin
  if Value<>MultiArea then
  Case Value of
    maNone      : Stacked:=cssNone;
    maStacked   : Stacked:=cssStack;
    maStacked100: Stacked:=cssStack100;
  end;
End;

class Procedure TAreaSeries.CreateSubGallery(AddSubChart:TChartSubGalleryProc);
begin
  inherited;
  AddSubChart(TeeMsg_Stairs);
  AddSubChart(TeeMsg_Marks);
  AddSubChart(TeeMsg_Colors);
  AddSubChart(TeeMsg_Hollow);
  AddSubChart(TeeMsg_NoLines);
//  AType.NumGallerySeries:=2;
  AddSubChart(TeeMsg_Stack);
  AddSubChart(TeeMsg_Stack100);
//  AType.NumGallerySeries:=1;
  AddSubChart(TeeMsg_Points);
end;

class Procedure TAreaSeries.SetSubGallery(ASeries:TChartSeries; Index:Integer);
begin
  With TAreaSeries(ASeries) do
  Case Index of
    1: Stairs:=True;
    2: Marks.Visible:=True;
    3: ColorEachPoint:=True;
    4: AreaBrush:=bsClear;
    5: AreaLinesPen.Visible:=False;
    6: MultiArea:=maStacked;
    7: MultiArea:=maStacked100;
    8: Pointer.Visible:=True;
  end;
end;

procedure TAreaSeries.SetOrigin(const Value: Double);
begin
  SetDoubleProperty(FOrigin,Value)
end;

procedure TAreaSeries.SetUseOrigin(Value: Boolean);
begin
  SetBooleanProperty(FUseOrigin,Value)
end;

function TAreaSeries.MaxYValue: Double;
begin
  result:=inherited MaxYValue;
  if UseYOrigin and (result<YOrigin) then result:=YOrigin;
end;

function TAreaSeries.MinYValue: Double;
begin
  result:=inherited MinYValue;
  if UseYOrigin and (result>YOrigin) then result:=YOrigin;
end;

function TAreaSeries.GetOriginPos(ValueIndex: Integer): Integer;
begin
  if UseYOrigin then result:=CalcYPosValue(YOrigin)
                else result:=inherited GetOriginPos(ValueIndex);
end;

{ TCustomBarSeries }
Constructor TCustomBarSeries.Create(AOwner:TComponent);
begin
   inherited;
   FBarWidthPercent:=70;
   FBarStyle:=bsRectangle;
   FGradient:=TBarSeriesGradient.Create(CanvasChanged);
   FUseOrigin:=True;
   FMultiBar:=mbSide;
   FAutoMarkPosition:=True;
   Marks.Visible:=True;
   Marks.ArrowLength:=20;
   FDark3D:=True;
   FSideMargins:=True;
end;

Destructor TCustomBarSeries.Destroy;
begin
   FGradient.Free;
   inherited;
end;
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TCustomBarSeries.Load(var S: TOldStream; AOwner: TComponent);
begin
   inherited Load(S,AOwner);
   S.Read(FBarWidthPercent,SizeOf(Integer));
   S.Read(FBarStyle,SizeOf(TBarStyle));
   
   FGradient:=TBarSeriesGradient.Load(S,CanvasChanged);
   S.Read(FOrigin,SizeOf(Double));
   S.Read(FUseOrigin,SizeOf(Boolean));
   S.Read(FMultiBar,SizeOf(TMultiBar));
   S.Read(FAutoBarSize,SizeOf(Boolean));
   S.Read(FAutoMarkPosition,SizeOf(Boolean));
   S.Read(FDark3D,SizeOf(Boolean));
   S.Read(FSideMargins,SizeOf(Boolean));

   S.Read(FConePercent,SizeOf(Integer));
   S.Read(FCustomBarSize,SizeOf(Integer));
   S.Read(FOffsetPercent,SizeOf(Integer));
   S.Read(FStackGroup,SizeOf(Integer));
end;

Procedure TCustomBarSeries.Store(var S: TOldStream);
begin
   inherited Store(S);
   S.Write(FBarWidthPercent,SizeOf(Integer));
   S.Write(FBarStyle,SizeOf(TBarStyle));

   FGradient.Store(S);
   S.Write(FOrigin,SizeOf(Double));
   S.Write(FUseOrigin,SizeOf(Boolean));
   S.Write(FMultiBar,SizeOf(TMultiBar));
   S.Write(FAutoBarSize,SizeOf(Boolean));
   S.Write(FAutoMarkPosition,SizeOf(Boolean));
   S.Write(FDark3D,SizeOf(Boolean));
   S.Write(FSideMargins,SizeOf(Boolean));

   S.Write(FConePercent,SizeOf(Integer));
   S.Write(FCustomBarSize,SizeOf(Integer));
   S.Write(FOffsetPercent,SizeOf(Integer));
   S.Write(FStackGroup,SizeOf(Integer));
end;

Function TCustomBarSeries.CreateDeterminant(Determinant: String): String;
begin
   Result:=Determinant;
end;

Function TCustomBarSeries.OnCompareItems(Item: TChartSeries): Boolean;
begin
   Result:=FALSE;
   if Item is TCustomBarSeries then
     with TCustomBarSeries(Item) do
       if Self.FBarWidthPercent = FBarWidthPercent then
         if Self.FBarStyle = FBarStyle then
           if Self.FOrigin = FOrigin then
             if Self.FUseOrigin = FUseOrigin then
               if Self.FMultiBar = FMultiBar then
                 if Self.FAutoBarSize = FAutoBarSize then
                   if Self.FAutoMarkPosition = FAutoMarkPosition then
                     if Self.FDark3D = FDark3D then
                       if Self.FSideMargins = FSideMargins then
                         if Self.FConePercent = FConePercent then
                           if Self.FCustomBarSize = FCustomBarSize then
                             if Self.FOffsetPercent = FOffsetPercent then
                               if Self.FStackGroup = FStackGroup then
                                 if Self.FGradient.OnCompareItems(FGradient) then
                                   Result := inherited OnCompareItems(Item);
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TCustomBarSeries.CalcZOrder;
Var t         : Integer;
    tmpZOrder : Integer;
Begin
  if FMultiBar=mbNone then inherited
  else
  begin
    tmpZOrder:=-1;
    With ParentChart do
    for t:=0 to SeriesCount-1 do
    if Series[t].Active then
    begin
      if Series[t]=Self then break
      else
      if SameClass(Series[t]) then
      Begin
        tmpZOrder:=Series[t].ZOrder;
        break;
      end;
    end;
    if tmpZOrder=-1 then inherited
                    else IZOrder:=tmpZOrder;
  end;
End;

Function TCustomBarSeries.NumSampleValues:Integer;
Begin
  result:=6; { default number of BarSeries random sample values }
End;

Procedure TCustomBarSeries.SetBarWidthPercent(Value:Integer);
Begin
  SetIntegerProperty(FBarWidthPercent,Value);
end;

Procedure TCustomBarSeries.SetOffsetPercent(Value:Integer);
Begin
  SetIntegerProperty(FOffsetPercent,Value);
End;

Procedure TCustomBarSeries.SetCustomBarSize(Value:Integer);
Begin
  SetIntegerProperty(FCustomBarSize,Value);
end;

Procedure TCustomBarSeries.SetParentChart(Const Value:TCustomAxisPanel);
begin
  inherited;
  SetOtherBars(False);
end;

Procedure TCustomBarSeries.SetBarStyle(Value:TBarStyle);
Begin
  if FBarStyle<>Value then
  begin
    FBarStyle:=Value;
    Repaint;
  end;
end;

{ If more than one bar series exists in chart,
  which position are we? the first, the second, the third? }
Procedure TCustomBarSeries.DoBeforeDrawChart;
var Groups : Array{$IFNDEF D4}[0..100]{$ENDIF} of Integer;
{$IFNDEF D4}NumGroups:Integer;{$ENDIF}

  Function NewGroup(AGroup:Integer):Boolean;
  var t : Integer;
  begin
    for t:=0 to {$IFDEF D4}Length(Groups){$ELSE}NumGroups{$ENDIF}-1 do
    if Groups[t]=AGroup then
    begin
      result:=False;
      exit;
    end;
    {$IFDEF D4}
    SetLength(Groups,Length(Groups)+1);
    {$ELSE}
    Inc(NumGroups);
    {$ENDIF}
    Groups[{$IFDEF D4}Length(Groups){$ELSE}NumGroups{$ENDIF}-1]:=AGroup;
    result:=True;
  end;

var t    : Integer;
    Stop : Boolean;
    tmp  : Integer;
begin
  IOrderPos:=1;
  IPreviousCount:=0;
  INumBars:=0;
  IMaxBarPoints:=TeeAllValues;
  {$IFDEF D4}
  Groups:=nil;
  {$ELSE}
  NumGroups:=0;
  {$ENDIF}
  Stop:=False;
  With ParentChart do
  for t:=0 to SeriesCount-1 do
  if Series[t].Active and SameClass(Series[t]) then
  begin
    Stop:=Stop or (Series[t]=Self);
    tmp:=Series[t].Count;
    if (IMaxBarPoints=TeeAllValues) or (tmp>IMaxBarPoints) then
       IMaxBarPoints:=tmp;
    Case FMultiBar of
      mbNone: INumBars:=1;
      mbSide,
      mbSideAll: begin
                   Inc(INumBars);
                   if not Stop then Inc(IOrderPos);
                 end;
      mbStacked,
      mbStacked100: if NewGroup(TCustomBarSeries(Series[t]).FStackGroup) then
                    begin
                      Inc(INumBars);
                      if not Stop then Inc(IOrderPos);
                    end;
    end;
    if not Stop then Inc(IPreviousCount,tmp);
  end;
  for t:=0 to {$IFDEF D4}Length(Groups){$ELSE}NumGroups{$ENDIF}-1 do
  if Groups[t]=FStackGroup then
  begin
    IOrderPos:=t+1;
    break;
  end;
  {$IFDEF D4}
  Groups:=nil;
  {$ENDIF}
  { this should be after calculating INumBars }
  if ParentChart.MaxPointsPerPage>0 then IMaxBarPoints:=ParentChart.MaxPointsPerPage;
end;

Function TCustomBarSeries.InternalClicked(ValueIndex:Integer; Const APoint:TPoint):Boolean;
Begin
  result:=False; { <-- abstract }
End;

Function TCustomBarSeries.CalcMarkLength(ValueIndex:Integer):Integer;
Begin
  if (Count>0) and Marks.Visible then
  Begin
    ParentChart.Canvas.AssignFont(Marks.Font);
    result:=Marks.ArrowLength+InternalCalcMarkLength(ValueIndex);
    With Marks.Frame do if Visible then Inc(result,2*Width);
  end
  else result:=0;
end;

Procedure TCustomBarSeries.SetUseOrigin(Value:Boolean);
Begin
  SetBooleanProperty(FUseOrigin,Value);
End;

Procedure TCustomBarSeries.SetSideMargins(Value:Boolean);
Begin
  SetBooleanProperty(FSideMargins,Value);
  SetOtherBars(True);
end;

Procedure TCustomBarSeries.SetDark3d(Value:Boolean);
Begin
  SetBooleanProperty(FDark3d,Value);
End;

Procedure TCustomBarSeries.PrepareForGallery(IsEnabled:Boolean);
Begin
  inherited;
  BarWidthPercent:=85;
  MultiBar:=mbNone;
end;

Procedure TCustomBarSeries.SetOrigin(Const Value:Double);
Begin
  SetDoubleProperty(FOrigin,Value);
End;

Function TCustomBarSeries.Clicked(x,y:Integer):Integer;
var t : Integer;
    P : TPoint;
Begin
  if Assigned(ParentChart) then ParentChart.Canvas.Calculate2DPosition(X,Y,StartZ);
  result:=inherited Clicked(x,y);
  if (result=-1) and (FirstValueIndex>-1) and (LastValueIndex>-1) then
  begin
    P.X:=X;
    P.Y:=Y;
    for t:=FirstValueIndex to LastValueIndex do
    if t<Count then
       if InternalClicked(t,P) then
       begin
         result:=t;
         break;
       end;
  end;
end;

Procedure TCustomBarSeries.SetOtherBars(SetOthers:Boolean);
var t : Integer;
Begin
  if Assigned(ParentChart) then
  with ParentChart do
  for t:=0 to SeriesCount-1 do
    if SameClass(Series[t]) then
       With TCustomBarSeries(Series[t]) do
       begin
         if SetOthers then
         begin
           FMultiBar:=Self.FMultiBar;
           FSideMargins:=Self.FSideMargins;
         end
         else
         begin
           Self.FMultiBar:=FMultiBar;
           Self.FSideMargins:=FSideMargins;
           break;
         end;
       end;
end;

Procedure TCustomBarSeries.SetMultiBar(Value:TMultiBar);
Begin
  if Value<>FMultiBar then
  Begin
    FMultiBar:=Value;
    SetOtherBars(True);
    Repaint;
  end;
End;

Function TCustomBarSeries.InternalGetOriginPos(ValueIndex:Integer; DefaultOrigin:Integer):Integer;
Var tmp      : Double;
    tmpValue : Double;
Begin
  tmpValue:=PointOrigin(ValueIndex,False);
  Case FMultiBar of
    mbStacked: result:=CalcPosValue(tmpValue);
    mbStacked100:
      begin
        tmp:=PointOrigin(ValueIndex,True);
        if tmp<>0 then result:=CalcPosValue(tmpValue*100.0/tmp)
                  else result:=0;
      end;
  else
    if FUseOrigin then result:=CalcPosValue(tmpValue)
                  else result:=DefaultOrigin;
  end;
end;

Function TCustomBarSeries.InternalPointOrigin(ValueIndex:Integer; SumAll:Boolean):Double;
var t         : Integer;
    tmpSeries : TChartSeries;
    tmp       : Double;
    tmpValue  : Double;
Begin
  result:=0;
  tmpValue:=MandatoryValueList.Value[ValueIndex];
  if Assigned(ParentChart) then
  with ParentChart do
  for t:=0 to SeriesCount-1 do
  Begin
    tmpSeries:=Series[t];
    if (not SumAll) and (tmpSeries=Self) then Break
    else
    With tmpSeries do
    if Active and SameClass(Self) and (Count>ValueIndex)
       and (TCustomBarSeries(tmpSeries).StackGroup=Self.StackGroup) then
    begin
      tmp:=GetOriginValue(ValueIndex);
      if tmpValue<0 then
      begin
        if tmp<0 then result:=result+tmp;
      end
      else result:=result+tmp;
    end;
  end;
end;

Function TCustomBarSeries.PointOrigin(ValueIndex:Integer; SumAll:Boolean):Double;
Begin
  if (FMultiBar=mbStacked) or
     (FMultiBar=mbStacked100) then
    result:=InternalPointOrigin(ValueIndex,SumAll)
  else
    result:=FOrigin;
End;

Procedure TCustomBarSeries.DrawLegendShape(ValueIndex:Integer; Const Rect:TRect);
begin
  if Assigned(BarBrush.Image.Graphic) then
     ParentChart.Canvas.Brush.Bitmap:=BarBrush.Image.Bitmap;
  inherited;
end;

Procedure TCustomBarSeries.PrepareLegendCanvas( ValueIndex:Integer; Var BackColor:TColor;
                                                Var BrushStyle:TBrushStyle);
Begin
  ParentChart.Canvas.AssignVisiblePen(BarPen);
  BrushStyle:=BarBrush.Style;
  if BarBrush.Color=clTeeColor then BackColor:=ParentChart.Color
                               else BackColor:=BarBrush.Color;
end;

Procedure TCustomBarSeries.SetPenBrushBar(BarColor:TColor);
var tmpBack : TColor;
Begin
  With ParentChart do
  begin
    Canvas.AssignVisiblePen(BarPen);
    if BarBrush.Color=clTeeColor then tmpBack:=Color
                                 else tmpBack:=BarBrush.Color;
    SetBrushCanvas(BarColor,BarBrush,tmpBack);
  end;
end;

Procedure TCustomBarSeries.BarRectangle( BarColor:TColor;
                                         ALeft,ATop,ARight,ABottom:Integer);
Begin
  With ParentChart,Canvas do
  begin
    if BarBrush.Style=bsSolid then
    Begin
      if (ARight=ALeft) or (ATop=ABottom) then
      Begin
        Pen.Color:=Brush.Color;
        if Pen.Style=psClear then Pen.Style:=psSolid;
        Line(ALeft,ATop,ARight,ABottom);
      end
      else
      if (Abs(ARight-ALeft)<Pen.Width) or
         (Abs(ABottom-ATop)<Pen.Width) then
      Begin
        Pen.Color:=Brush.Color;
        if Pen.Style=psClear then Pen.Style:=psSolid;
        Brush.Style:=bsClear;
      end;
    end;
    Rectangle(ALeft,ATop,ARight,ABottom);
  end;
End;

Function TCustomBarSeries.GetBarStyle(ValueIndex:Integer):TBarStyle;
Begin
  result:=FBarStyle;
  if Assigned(FOnGetBarStyle) then FOnGetBarStyle(Self,ValueIndex,result);
end;

Function TCustomBarSeries.GetBarBrush:TChartBrush;
begin
  result:=Brush;
end;

Function TCustomBarSeries.GetBarPen:TChartPen;
begin
  result:=Pen;
end;

Procedure TCustomBarSeries.SetGradient(Value:TBarSeriesGradient);
begin
  FGradient.Assign(Value);
end;

class Function TCustomBarSeries.GetEditorClass:String;
Begin
  result:='TBarSeriesEditor'; { <-- dont translate ! }
end;

Function TCustomBarSeries.BarMargin:Integer;
Begin
  result:=IBarSize;
  if FMultiBar<>mbSideAll then result:=result*INumBars;
  if not SideMargins then result:=result shr 1;
end;

Function TCustomBarSeries.ApplyBarOffset(Position:Integer):Integer;
Begin
  result:=Position;
  if FOffsetPercent<>0 then Inc(result,Round(FOffsetPercent*IBarSize*0.01));
end;

Procedure TCustomBarSeries.SetAutoMarkPosition(Value:Boolean);
Begin
  SetBooleanProperty(FAutoMarkPosition,Value);
end;

Procedure TCustomBarSeries.SetAutoBarSize(Value:Boolean);
Begin
  SetBooleanProperty(FAutoBarSize,Value);
end;

Procedure TCustomBarSeries.Assign(Source:TPersistent);
begin
  if Source is TCustomBarSeries then
  With TCustomBarSeries(Source) do
  begin
    Self.FAutoMarkPosition:=FAutoMarkPosition;
    Self.FBarWidthPercent:=FBarWidthPercent;
    Self.FBarStyle       :=FBarStyle;
    Self.FCustomBarSize  :=FCustomBarSize;
    Self.FDark3d         :=FDark3D;
    Self.Gradient        :=FGradient;
    Self.FMultiBar       :=FMultiBar;
    Self.FOffsetPercent  :=FOffsetPercent;
    Self.FOrigin         :=FOrigin;
    Self.FSideMargins    :=FSideMargins;
    Self.FStackGroup     :=FStackGroup;
    Self.FUseOrigin      :=FUseOrigin;
  end;
  inherited;
end;

Function TCustomBarSeries.AddBar( Const AValue:Double;
                                  Const ALabel:String; AColor:TColor):Integer;
begin
  result:=Add(AValue,ALabel,AColor);
end;

Procedure TCustomBarSeries.DoGradient3D(P0,P1:TPoint);
begin
  if BarPen.Visible then
  begin
    Inc(P0.X);
    Inc(P0.Y);
    Dec(P1.X,BarPen.Width-1);
    Dec(P1.Y,BarPen.Width-1);
  end;
  FGradient.EndColor:=NormalBarColor;
  FGradient.Draw(ParentChart.Canvas,{$IFDEF D6}Types.{$ENDIF}Rect(P0.X,P0.Y,P1.X,P1.Y),0);
end;

Function TCustomBarSeries.MaxMandatoryValue(Const Value:Double):Double;
var t   : Integer;
    tmp : Double;
begin
  if FMultiBar=mbStacked100 then result:=100
  else
  begin
    result:=Value;
    if FMultiBar=mbStacked then
    for t:=0 to Count-1 do
    Begin
      tmp:=PointOrigin(t,False)+MandatoryValueList.Value[t];
      if tmp>result then result:=tmp;
    end;
    if FUseOrigin and (result<FOrigin) then result:=FOrigin;
  end;
end;

Function TCustomBarSeries.MinMandatoryValue(Const Value:Double):Double;
var t   : Integer;
    tmp : Double;
begin
  if FMultiBar=mbStacked100 then result:=0
  else
  begin
    result:=Value;
    if FMultiBar=mbStacked then
    for t:=0 to Count-1 do
    Begin
      tmp:=PointOrigin(t,False)+MandatoryValueList.Value[t];
      if tmp<result then result:=tmp;
    end;
    if FUseOrigin and (result>FOrigin) then result:=FOrigin;
  end;
end;

Procedure TCustomBarSeries.InternalApplyBarMargin(Var MarginA,MarginB:Integer);

  Procedure CalcBarWidth;
  Var tmpAxis : TChartAxis;
      tmp     : Integer;
  begin
    if FCustomBarSize<>0 then IBarSize:=FCustomBarSize
    else
    if IMaxBarPoints>0 then
    Begin
      if YMandatory then tmpAxis:=GetHorizAxis
                    else tmpAxis:=GetVertAxis;
      With ParentChart,tmpAxis do
      if FAutoBarSize then
         tmp:=Round(IAxisSize/(2.0+Maximum-Minimum))
      else
      begin
        if FSideMargins then Inc(IMaxBarPoints);
        tmp:=IAxisSize div IMaxBarPoints;
      end;
      IBarSize:=Round((FBarWidthPercent*0.01)*tmp) div INumBars;
      if (IBarSize mod 2)=1 then Inc(IBarSize);
    end
    else IBarSize:=0;
  end;

var tmp : Integer;
begin
  CalcBarWidth;
  tmp:=BarMargin;
  Inc(MarginA,tmp);
  Inc(MarginB,tmp);
end;

Procedure TCustomBarSeries.BarGradient(Const R:TRect);
begin
  With ParentChart do
  begin
    FGradient.EndColor:=NormalBarColor;
    FGradient.Draw(Canvas,R,0);
    if BarPen.Visible then
    begin
      Canvas.Brush.Style:=bsClear;
      With BarBounds do BarRectangle(NormalBarColor,Left,Top,Right,Bottom);
    end;
  end;
end;

class Procedure TCustomBarSeries.SetSubGallery(ASeries:TChartSeries; Index:Integer);
var tmp : TChartSeries;
begin
  With TCustomBarSeries(ASeries) do
  Case Index of
     0: ;
     1: ColorEachPoint:=True;
     2: BarStyle:=bsPyramid;
     3: BarStyle:=bsEllipse;
     4: BarStyle:=bsInvPyramid;
  else
  begin
    if ASeries.ParentChart.SeriesCount=1 then
    begin
      ASeries.FillsampleValues(2);
      tmp:=TChartSeriesClass(ASeries.ClassType).Create(nil);
      tmp.FillsampleValues(2);
      ASeries.ParentChart.AddSeries(tmp);
      tmp.Marks.Visible:=False;
      ASeries.Marks.Visible:=False;
      SetSubGallery(tmp,Index);
    end;
    Case Index of
       5: MultiBar:=mbStacked;
       6: MultiBar:=mbStacked100;
       7: MultiBar:=mbSide;
       8: MultiBar:=mbSideAll;
    else inherited;
    end;
  end;
  end;
end;

class Procedure TCustomBarSeries.CreateSubGallery(AddSubChart:TChartSubGalleryProc);
begin
  inherited;
  AddSubChart(TeeMsg_Colors);
  AddSubChart(TeeMsg_Pyramid);
  AddSubChart(TeeMsg_Ellipse);
  AddSubChart(TeeMsg_InvPyramid);
//  AType.NumGallerySeries:=3;
  AddSubChart(TeeMsg_Stack);
  AddSubChart(TeeMsg_Stack100);
//  AType.NumGallerySeries:=2;
  AddSubChart(TeeMsg_Sides);
  AddSubChart(TeeMsg_SideAll);
end;

procedure TCustomBarSeries.SetStackGroup(Value: Integer);
begin
  SetIntegerProperty(FStackGroup,Value);
end;

procedure TCustomBarSeries.SetConePercent(const Value: Integer);
begin
  SetIntegerProperty(FConePercent,Value);
end;

{ TBarSeries }
Constructor TBarSeries.Create(AOwner:TComponent);
begin
  inherited;
  FGradient.Direction:=gdTopBottom;
  MandatoryValueList.Name:=TeeMsg_ValuesBar;
end;

{ The horizontal Bar position is the "real" X pos +
  the BarWidth by our BarSeries order }
Function TBarSeries.CalcXPos(ValueIndex:Integer):Integer;
Begin
  if FMultiBar=mbSideAll then
     result:=GetHorizAxis.CalcXPosValue(IPreviousCount+ValueIndex)-(IBarSize shr 1)
  else
  begin
    result:=inherited CalcXPos(ValueIndex);
    if FMultiBar<>mbNone then
       result:=result+Round(IBarSize*((IOrderPos-(INumBars*0.5))-1.0))
    else
       result:=result-(IBarSize shr 1);
  end;
  result:=ApplyBarOffset(result);
End;

Function TBarSeries.MaxXValue:Double;
Begin
  if FMultiBar=mbSideAll then result:=IPreviousCount+Count-1
                         else result:=inherited MaxXValue;
end;

Function TBarSeries.MaxYValue:Double;
Begin
  result:=MaxMandatoryValue(inherited MaxYValue);
end;

Function TBarSeries.MinYValue:Double;
Begin
  result:=MinMandatoryValue(inherited MinYValue);
end;

Procedure TBarSeries.CalcHorizMargins(Var LeftMargin,RightMargin:Integer);
begin
  inherited;
  InternalApplyBarMargin(LeftMargin,RightMargin);
end;

Procedure TBarSeries.CalcVerticalMargins(Var TopMargin,BottomMargin:Integer);
var tmp : Integer;
begin
  inherited;
  tmp:=CalcMarkLength(0);
  if tmp>0 then
  begin
    Inc(tmp);
    if FUseOrigin and (inherited MinYValue<FOrigin) then
       if GetVertAxis.Inverted then Inc(TopMargin,tmp) { 4.01 }
                               else Inc(BottomMargin,tmp);
    if (not FUseOrigin) or (inherited MaxYValue>FOrigin) then
       if GetVertAxis.Inverted then Inc(BottomMargin,tmp)
                               else Inc(TopMargin,tmp);
  end;
end;

Function TBarSeries.InternalCalcMarkLength(ValueIndex:Integer):Integer;
Begin
  result:=ParentChart.Canvas.FontHeight;
end;

Procedure TBarSeries.DrawMark( ValueIndex:Integer; Const St:String;
                               APosition:TSeriesMarkPosition);
Var DifW : Integer;
    DifH : Integer;
    tmpH : Integer;
    t    : Integer;
    tmpR : TRect;
    tmpBounds : TRect;
    tmpDest : TRect;
Begin
  With APosition do
  begin
    DifW:=IBarSize div 2;
    DifH:=Marks.ArrowLength;
    if ArrowFrom.Y>GetOriginPos(ValueIndex) then DifH:=-DifH-Height;
    LeftTop.X:=LeftTop.X+DifW;
    LeftTop.Y:=LeftTop.Y-DifH;
    ArrowTo.X:=ArrowTo.X+DifW;
    ArrowTo.Y:=ArrowTo.Y-DifH;
    ArrowFrom.X:=ArrowFrom.X+DifW;

    if AutoMarkPosition then
    begin
      tmpBounds:=Bounds;
      InflateRect(tmpBounds,-1,-1);
      for t:=FirstValueIndex to ValueIndex-1 do
      if Assigned(Marks.Positions[t]) then 
      begin
        tmpR:=Marks.Positions[t].Bounds;
        InflateRect(tmpR,-1,-1);
        if Windows.IntersectRect(tmpDest,tmpR,tmpBounds) then
        begin
          tmpH:=tmpBounds.Bottom-tmpR.Top;
          LeftTop.Y:=LeftTop.Y-tmpH;
          ArrowTo.Y:=ArrowTo.Y-tmpH;
        end;
      end;
    end;
  end;
  inherited;
end;

Procedure TBarSeries.DrawBar(BarIndex,StartPos,EndPos:Integer);
Var tmpMidX : Integer;
    tmp     : TBarStyle;
Begin
  SetPenBrushBar(NormalBarColor);
  With ParentChart,Canvas,FBarBounds do
  begin
    tmpMidX:=(Left+Right) shr 1;
    tmp:=GetBarStyle(BarIndex);
    if View3D then
    Case tmp of
      bsRectangle: Cube(Left,Right,StartPos,EndPos,StartZ,EndZ,FDark3D);
      bsPyramid  : Pyramid(True,Left,StartPos,Right,EndPos,StartZ,EndZ,FDark3D);
     bsInvPyramid: Pyramid(True,Left,EndPos,Right,StartPos,StartZ,EndZ,FDark3D);
      bsCilinder : Cylinder(True,Left,Top,Right,Bottom,StartZ,EndZ,FDark3D);
      bsEllipse  : EllipseWithZ(Left,Top,Right,Bottom,MiddleZ);
      bsArrow    : Arrow(True, {$IFDEF D6}Types.{$ENDIF}Point(tmpMidX,EndPos),
                               {$IFDEF D6}Types.{$ENDIF}Point(tmpMidX,StartPos),
                             (Right-Left),(Right-Left) div 2,MiddleZ);
   bsRectGradient: begin
                     Cube(Left,Right,StartPos,EndPos,StartZ,EndZ,FDark3D);
                     if SupportsFullRotation or View3DOptions.Orthogonal then
                     With FBarBounds do
                       DoGradient3D( Calculate3DPosition(Left,StartPos,StartZ),
                                     Calculate3DPosition(Right,EndPos,StartZ));
                   end;
      bsCone     : Cone(True,Left,Top,Right,Bottom,StartZ,EndZ,FDark3D,ConePercent);
    end
    else
    begin
      Case tmp of
        bsRectangle,
        bsCilinder : BarRectangle(NormalBarColor,Left,Top,Right,Bottom);
        bsPyramid,
        bsCone     : Polygon([ {$IFDEF D6}Types.{$ENDIF}Point(Left,EndPos),
                               {$IFDEF D6}Types.{$ENDIF}Point(tmpMidX,StartPos),
                               {$IFDEF D6}Types.{$ENDIF}Point(Right,EndPos) ]);
       bsInvPyramid: Polygon([ {$IFDEF D6}Types.{$ENDIF}Point(Left,StartPos),
                               {$IFDEF D6}Types.{$ENDIF}Point(tmpMidX,EndPos),
                               {$IFDEF D6}Types.{$ENDIF}Point(Right,StartPos) ]);
        bsEllipse  : Ellipse(Left,Top,Right,Bottom);
        bsArrow    : Arrow(True, {$IFDEF D6}Types.{$ENDIF}Point(tmpMidX,EndPos),
                                 {$IFDEF D6}Types.{$ENDIF}Point(tmpMidX,StartPos),
                                 (Right-Left),(Right-Left) div 2,MiddleZ);
     bsRectGradient: BarGradient({$IFDEF D6}Types.{$ENDIF}Rect(Left,StartPos,Right,EndPos));
      end;
    end;
  end;
end;

procedure TBarSeries.DrawValue(ValueIndex:Integer);
Begin
  inherited;
  NormalBarColor:=ValueColor[ValueIndex];
  if NormalBarColor<>clNone then { if not null }
  With FBarBounds do
  Begin
    Left  :=CalcXPos(ValueIndex);
    Right :=Left+IBarSize;
    Top   :=CalcYPos(ValueIndex);
    Bottom:=GetOriginPos(ValueIndex);
    if not BarPen.Visible then
    begin
      if Bottom>Top then Inc(Bottom) else Inc(Top);
      Inc(Right);
    end;
    if Bottom>Top then DrawBar(ValueIndex,Top,Bottom)
                  else DrawBar(ValueIndex,Bottom,Top);
  end;
end;

Function TBarSeries.CalcYPos(ValueIndex:Integer):Integer;
var tmp      : Double;
    tmpValue : Double;
Begin
  Case FMultiBar of
    mbNone,mbSide,mbSideAll: result:=inherited CalcYPos(ValueIndex)
  else
  begin
    tmpValue:=YValues.Value[ValueIndex]+PointOrigin(ValueIndex,False);
    if FMultiBar=mbStacked then result:=CalcYPosValue(tmpValue)
    else
    begin
      tmp:=PointOrigin(ValueIndex,True);
      if tmp<>0 then result:=CalcYPosValue(tmpValue*100.0/tmp)
                else result:=0;
    end;
  end;
  end;
End;

Function TBarSeries.GetOriginPos(ValueIndex:Integer):Integer;
Begin
  result:=InternalGetOriginPos(ValueIndex,GetVertAxis.IEndPos);
end;

Function TBarSeries.InternalClicked(ValueIndex:Integer; Const APoint:TPoint):Boolean;
Var tmpSwap : Integer;
    tmpX    : Integer;
    tmpY    : Integer;
    endY    : Integer;
Begin
  result:=False;
  tmpX:=CalcXPos(ValueIndex);
  if (APoint.x>=tmpX) and (APoint.x<=(tmpX+IBarSize)) then
  Begin
    tmpY:=CalcYPos(ValueIndex);
    endY:=GetOriginPos(ValueIndex);
    if endY<tmpY then
    begin
      tmpSwap:=endY;
      endY:=tmpY;
      tmpY:=tmpSwap;
    end;
    Case FBarStyle of
   bsInvPyramid: result:=PointInTriangle(APoint,tmpX,tmpX+IBarSize,tmpY,endY);
      bsPyramid,
      bsCone   : result:=PointInTriangle(APoint,tmpX,tmpX+IBarSize,endY,tmpY);
      bsEllipse: result:=PointInEllipse(APoint,{$IFDEF D6}Types.{$ENDIF}Rect(tmpX,tmpY,tmpX+IBarSize,endY));
    else
      result:=(APoint.y>=tmpY) and (APoint.y<=endY);
    end;
  end;
end;

Function TBarSeries.MoreSameZOrder:Boolean;
begin
  if FMultiBar=mbSideAll then result:=False
                         else result:=inherited MoreSameZOrder;
end;

Function TBarSeries.DrawValuesForward:Boolean;
begin
  result:=not GetHorizAxis.Inverted;
end;

Function TBarSeries.DrawSeriesForward(ValueIndex:Integer):Boolean;
begin
  Case FMultiBar of
    mbNone,mbSide,mbSideAll: result:=True;
    mbStacked: begin
                 result:=YValues.Value[ValueIndex]>YOrigin;
                 if GetVertAxis.Inverted then result:=not result;
               end;
  else
    result:=(not GetVertAxis.Inverted);
  end;
end;

{ THorizBarSeries }
Constructor THorizBarSeries.Create(AOwner:TComponent);
begin
  inherited;
  SetHorizontal;
  XValues.Order:=loNone;
  YValues.Order:=loAscending;
  FGradient.Direction:=gdLeftRight;
  MandatoryValueList.Name:=TeeMsg_ValuesBar;
end;

Procedure THorizBarSeries.CalcHorizMargins(Var LeftMargin,RightMargin:Integer);
var tmp : Integer;
begin
  inherited;
  tmp:=CalcMarkLength(TeeAllValues);
  if tmp>0 then Inc(tmp);
  if (FUseOrigin and (inherited MinXValue<FOrigin)) then
     Inc(LeftMargin,tmp);
  if (not FUseOrigin) or (inherited MaxXValue>FOrigin) then
     if GetHorizAxis.Inverted then Inc(LeftMargin,tmp)
                              else Inc(RightMargin,tmp);
end;

Procedure THorizBarSeries.CalcVerticalMargins(Var TopMargin,BottomMargin:Integer);
begin
  inherited;
  InternalApplyBarMargin(TopMargin,BottomMargin);
end;

Function THorizBarSeries.CalcXPos(ValueIndex:Integer):Integer;
var tmp      : Double;
    tmpValue : Double;
Begin
  if (FMultiBar=mbNone) or (FMultiBar=mbSide) or (FMultiBar=mbSideAll) then
     result:=inherited CalcXPos(ValueIndex)
  else
  begin
    tmpValue:=XValues.Value[ValueIndex]+PointOrigin(ValueIndex,False);
    if FMultiBar=mbStacked then result:=CalcXPosValue(tmpValue)
    else
    begin
      tmp:=PointOrigin(ValueIndex,True);
      if tmp<>0 then result:=CalcXPosValue(tmpValue*100.0/tmp)
                else result:=0;
    end;
  end;
End;

Function THorizBarSeries.GetOriginPos(ValueIndex:Integer):Integer;
Begin
  result:=InternalGetOriginPos(ValueIndex,GetHorizAxis.IStartPos);
end;

{ The vertical Bar position is the "real" Y pos +
  the Barwidth by our BarSeries order }
Function THorizBarSeries.CalcYPos(ValueIndex:Integer):Integer;
Begin
  if FMultiBar=mbSideAll then
     result:=GetVertAxis.CalcYPosValue(IPreviousCount+ValueIndex)-(IBarSize shr 1)
  else
  begin
    result:=inherited CalcYPos(ValueIndex);
    if FMultiBar<>mbNone then
       result:=result+Round(IBarSize*( ((INumBars*0.5)-(1+INumBars-IOrderPos)) ))
    else
       result:=result-(IBarSize shr 1);
  end;
  result:=ApplyBarOffset(result);
end;

Function THorizBarSeries.InternalClicked(ValueIndex:Integer; Const APoint:TPoint):Boolean;
Var tmpX  : Integer;
    tmpY  : Integer;
    endX  : Integer;
Begin
  result:=False;
  tmpY:=CalcYPos(ValueIndex);
  if (APoint.y>=tmpY) and (APoint.y<=(tmpY+IBarSize)) then
  Begin
    tmpX:=CalcXPos(ValueIndex);
    endX:=GetOriginPos(ValueIndex);
    if endX<tmpX then SwapInteger(tmpX,endX);
    Case FBarStyle of
   bsInvPyramid: result:=PointInHorizTriangle(APoint,tmpY,tmpY+IBarSize,endX,tmpX);
      bsPyramid,
      bsCone   : result:=PointInHorizTriangle(APoint,tmpY,tmpY+IBarSize,tmpX,endX);
      bsEllipse: result:=PointInEllipse(APoint,{$IFDEF D6}Types.{$ENDIF}Rect(tmpX,tmpY,endX,tmpY+IBarSize));
    else
      result:=(APoint.x>=tmpX) and (APoint.x<=endX);
    end;
  end;
end;

Function THorizBarSeries.MaxXValue:Double;
Begin
  result:=MaxMandatoryValue(inherited MaxXValue);
end;

Function THorizBarSeries.MinXValue:Double;
Begin
  result:=MinMandatoryValue(inherited MinXValue);
end;

Function THorizBarSeries.MaxYValue:Double;
Begin
  if FMultiBar=mbSideAll then result:=IPreviousCount+Count-1
                         else result:=inherited MaxYValue;
end;

Function THorizBarSeries.InternalCalcMarkLength(ValueIndex:Integer):Integer;
Begin
  if ValueIndex=TeeAllValues then
     result:=MaxMarkWidth
  else
     result:=ParentChart.Canvas.TextWidth(GetMarkText(ValueIndex));
end;

Procedure THorizBarSeries.DrawMark( ValueIndex:Integer; Const St:String;
                                    APosition:TSeriesMarkPosition);
Var DifH : Integer;
    DifW : Integer;
Begin
  With APosition do
  begin
    DifH:=IBarSize div 2;
    DifW:=Marks.ArrowLength;
    if ArrowFrom.X<GetOriginPos(ValueIndex) then DifW:=-DifW-Width;
    LeftTop.X:=LeftTop.X+DifW+(Width div 2);
    LeftTop.Y:=LeftTop.Y+DifH+(Height div 2);
    ArrowTo.X:=ArrowTo.X+DifW;
    ArrowTo.Y:=ArrowTo.Y+DifH;
    ArrowFrom.Y:=ArrowFrom.Y+DifH;
  end;
  inherited;
end;

Procedure THorizBarSeries.DrawBar(BarIndex,StartPos,EndPos:Integer);
Var tmpMidY : Integer;
    tmp     : TBarStyle;
Begin
  SetPenBrushBar(NormalBarColor);
  With ParentChart,Canvas,FBarBounds do
  begin
    tmpMidY:=(Top+Bottom) shr 1;
    tmp:=GetBarStyle(BarIndex);
    if View3D then
    Case tmp of
      bsRectangle: Cube(StartPos,EndPos,Top,Bottom,StartZ,EndZ,FDark3D);
      bsPyramid  : Pyramid(False,StartPos,Top,EndPos,Bottom,StartZ,EndZ,FDark3D);
     bsInvPyramid: Pyramid(False,EndPos,Top,StartPos,Bottom,StartZ,EndZ,FDark3D);
      bsCilinder : Cylinder(False,Left,Top,Right,Bottom,StartZ,EndZ,FDark3D);
      bsEllipse  : EllipseWithZ(Left,Top,Right,Bottom,MiddleZ);
      bsArrow    : Arrow(True, {$IFDEF D6}Types.{$ENDIF}Point(StartPos,tmpMidY),
                               {$IFDEF D6}Types.{$ENDIF}Point(EndPos,tmpMidY),
                               (Bottom-Top),(Bottom-Top) div 2,MiddleZ);
   bsRectGradient: begin
                     Cube(StartPos,EndPos,Top,Bottom,StartZ,EndZ,FDark3D);
                     if Canvas.SupportsFullRotation or View3DOptions.Orthogonal then
                     With FBarBounds do
                       DoGradient3D( Calculate3DPosition(StartPos,Top,StartZ),
                                     Calculate3DPosition(EndPos,Bottom,StartZ));
                   end;
       bsCone    : Cone(False,Left,Top,Right,Bottom,StartZ,EndZ,FDark3D,ConePercent);
    end
    else
    begin
      Case tmp of
        bsRectangle,
        bsCilinder : BarRectangle(NormalBarColor,Left,Top,Right,Bottom);
        bsPyramid,
        bsCone     : Polygon([ {$IFDEF D6}Types.{$ENDIF}Point(StartPos,Top),
                               {$IFDEF D6}Types.{$ENDIF}Point(EndPos,tmpMidY),
                               {$IFDEF D6}Types.{$ENDIF}Point(StartPos,Bottom) ]);
       bsInvPyramid: Polygon([ {$IFDEF D6}Types.{$ENDIF}Point(EndPos,Top),
                               {$IFDEF D6}Types.{$ENDIF}Point(StartPos,tmpMidY),
                               {$IFDEF D6}Types.{$ENDIF}Point(EndPos,Bottom) ]);
        bsEllipse  : Ellipse(Left,Top,Right,Bottom);
        bsArrow    : Arrow(True, {$IFDEF D6}Types.{$ENDIF}Point(StartPos,tmpMidY),
                                 {$IFDEF D6}Types.{$ENDIF}Point(EndPos,tmpMidY),
                                 (Bottom-Top),(Bottom-Top) div 2,MiddleZ);
     bsRectGradient: BarGradient({$IFDEF D6}Types.{$ENDIF}Rect(StartPos,Top,EndPos,Bottom));
      end;
    end;
  end;
end;

procedure THorizBarSeries.DrawValue(ValueIndex:Integer);
Begin
  inherited;
  NormalBarColor:=ValueColor[ValueIndex];
  if NormalBarColor<>clNone then  { <-- if not null }
  With FBarBounds do
  Begin
    Top   :=CalcYPos(ValueIndex);
    Bottom:=Top+IBarSize;
    Right :=CalcXPos(ValueIndex);
    Left  :=GetOriginPos(ValueIndex);
    if not BarPen.Visible then
    begin
      if Right>Left then Inc(Right)
      else
      begin
        Inc(Left);
        Dec(Right);
      end;
    end;
    if Right>Left then DrawBar(ValueIndex,Left,Right)
                  else DrawBar(ValueIndex,Right,Left);
  end;
end;

Function THorizBarSeries.DrawValuesForward:Boolean;
begin
  result:=not GetVertAxis.Inverted;
end;

Function THorizBarSeries.DrawSeriesForward(ValueIndex:Integer):Boolean;
begin
  Case FMultiBar of
    mbNone   : result:=True;
    mbSide,
    mbSideAll: result:=False;
    mbStacked: begin
                 result:=MandatoryValueList.Value[ValueIndex]>0;
                 if GetHorizAxis.Inverted then result:=not result;
               end;
  else
    result:=not GetHorizAxis.Inverted;
  end;
end;

{ TCircledSeries }
Constructor TCircledSeries.Create(AOwner: TComponent);
begin
   inherited;
   CalcVisiblePoints:=False; { always draw all points }
   XValues.Name:=TeeMsg_ValuesAngle;
   FCircleBackColor:=clTeeColor;
end;

Destructor TCircledSeries.Destroy;
begin
   SetParentProperties(True);
   FreeAndNil(IBack3D);
   inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TCircledSeries.Load(var S: TOldStream; AOwner: TComponent);
begin
   inherited Load(S,AOwner);
   S.Read(FCircled,SizeOf(Boolean));
   S.Read(FRotationAngle,SizeOf(Integer));
   S.Read(FCustomXRadius,SizeOf(Integer));
   S.Read(FCustomYRadius,SizeOf(Integer));
   S.Read(FXRadius,SizeOf(Integer));
   S.Read(FYRadius,SizeOf(Integer));
   S.Read(FCircleBackColor,SizeOf(TColor));
end;

Procedure TCircledSeries.Store(var S: TOldStream);
begin
   inherited Store(S);
   S.Write(FCircled,SizeOf(Boolean));
   S.Write(FRotationAngle,SizeOf(Integer));
   S.Write(FCustomXRadius,SizeOf(Integer));
   S.Write(FCustomYRadius,SizeOf(Integer));
   S.Write(FXRadius,SizeOf(Integer));
   S.Write(FYRadius,SizeOf(Integer));
   S.Write(FCircleBackColor,SizeOf(TColor));
end;

Function TCircledSeries.OnCompareItems(Item: TChartSeries): Boolean;
var Difference : Integer;
    PermisDiff : Integer;
begin
   Result:=FALSE;
   if Item is TCircledSeries then
      with TCircledSeries(Item) do
         if Self.FCircled = FCircled then
            if Self.FRotationAngle = FRotationAngle then
               begin
                 Difference:=Abs(Self.FCustomXRadius-FCustomXRadius);
                 if Difference > 1  then
                    begin
                       PermisDiff:=Round(Self.FCustomXRadius*co_PermisPercent);
                       if Difference > PermisDiff  then
                          Exit;
                    end;
                 Difference:=Abs(Self.FCustomYRadius-FCustomYRadius);
                 if Difference > 1  then
                    begin
                       PermisDiff:=Round(Self.FCustomYRadius*co_PermisPercent);
                       if Difference > PermisDiff  then
                          Exit;
                    end;
                  if Self.FXRadius = FXRadius then
                     if Self.FYRadius = FYRadius then
                        if Self.FCircleBackColor = FCircleBackColor then
                           Result := inherited OnCompareItems(Item);
               end;
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

procedure TCircledSeries.SetCircled(Value:Boolean);
var t : Integer;
Begin
  SetBooleanProperty(FCircled,Value);
  if Assigned(ParentChart) then
  with ParentChart do
  for t:=0 to SeriesCount-1 do
    if Self is Series[t].ClassType then
      With TCircledSeries(Series[t]) do
      begin
        FCircled:=Value;
        if not (csLoading in ComponentState) then
        begin
          FCustomXRadius:=0;
          FCustomYRadius:=0;
        end;
      end;
end;

Procedure TCircledSeries.SetRotationAngle(Value:Integer);
Begin
  if Value<0 then
     Raise PieException.CreateFmt(TeeMsg_Angle,[TeeMsg_Rotation]);
  SetIntegerProperty(FRotationAngle,Value mod 360);
  IRotDegree:=FRotationAngle*PiDegree;
end;

procedure TCircledSeries.SetOtherCustomRadius(IsXRadius:Boolean; Value:Integer);
var t : Integer;
Begin
  if Assigned(ParentChart) then
  with ParentChart do
  for t:=0 to SeriesCount-1 do
  With TCircledSeries(Series[t]) do
    if Self is ClassType then
       if IsXRadius then FCustomXRadius:=Value
                    else FCustomYRadius:=Value;
end;

Function TCircledSeries.UseAxis:Boolean;
begin
  result:=False;
end;

procedure TCircledSeries.SetCustomXRadius(Value:Integer);
Begin
  SetIntegerProperty(FCustomXRadius,Value);
  SetOtherCustomRadius(True,Value);
End;

procedure TCircledSeries.SetCustomYRadius(Value:Integer);
Begin
  SetIntegerProperty(FCustomYRadius,Value);
  SetOtherCustomRadius(False,Value);
End;

Procedure TCircledSeries.SetParentProperties(EnableParentProps:Boolean);
begin
  if Assigned(ParentChart) then
  With ParentChart do
  if (not (csDestroying in ComponentState)) and
     (not Canvas.SupportsFullRotation) and
      Assigned(View3DOptions) then
  begin
    if EnableParentProps and (not Active) then
    begin
      View3DOptions.Assign(IBack3D);
      FreeAndNil(IBack3D);
    end
    else
    if not Assigned(IBack3D) then
    begin
      IBack3D:=TView3DOptions.Create(ParentChart);
      IBack3D.Assign(View3DOptions);
      With View3DOptions do
      begin
        Orthogonal:=False;
        Rotation:=360;
        Elevation:=315;
        Perspective:=0;
        Tilt:=0;
      end;
    end;
  end;
end;

Procedure TCircledSeries.SetParentChart(Const Value:TCustomAxisPanel);
Begin
  if Value=nil then
     SetParentProperties(True);
  if Value<>ParentChart then
  begin
    inherited;
    if Assigned(ParentChart) then SetParentProperties(False);
  end;
end;

Procedure TCircledSeries.Rotate(Angle:Integer);
Begin
  if (Angle<0) or (Angle>360) then
     Raise PieException.CreateFmt(TeeMsg_Angle,[TeeMsg_Rotation]);
  RotationAngle:=(RotationAngle+Angle) mod 360;
End;

Function TCircledSeries.AssociatedToAxis(Axis:TChartAxis):Boolean;
Begin
  result:=True;
end;

Procedure TCircledSeries.AdjustCircleRect;
Begin
  with FCircleRect do
  Begin
    if Odd(Right-Left) then Dec(Right);
    if Odd(Bottom-Top) then Dec(Bottom);
    if (Right-Left)<4 then Right:=Left+4;
    if (Bottom-Top)<4 then Bottom:=Top+4;
    FCircleWidth  :=Right-Left;
    FCircleHeight :=Bottom-Top;
  end;
  RectCenter(FCircleRect,FCircleXCenter,FCircleYCenter);
End;

Procedure TCircledSeries.DoBeforeDrawValues;

  Procedure CalcCircledRatio;

    Function TeeAdjustRatio(Const ARatio:Double; ACanvas:TCanvas3D):Double;
    Var tmpRatio : Double;
        tmpH     : Integer;
        tmpW     : Integer;
    begin
      result:=ARatio;
      {$IFDEF CLX}
      tmpW:=Screen.Width;
      tmpH:=Screen.Height;
      {$ELSE}
      tmpW:=GetDeviceCaps(ACanvas.Handle,HORZSIZE);
      tmpH:=GetDeviceCaps(ACanvas.Handle,VERTSIZE);
      {$ENDIF}
      if tmpH<>0 then
      begin
        tmpRatio:=(1.0*tmpW/tmpH);
        if tmpRatio<>0 then result:=1.0*ARatio/tmpRatio;
      end;
    end;

  Var Ratio : Double;
      Dif   : Integer;
  Begin
    Ratio:=TeeAdjustRatio(Screen.Width/Screen.Height,ParentChart.Canvas);
    CalcRadius;
    if Round(Ratio*FYRadius)<FXRadius then
    Begin
      dif:=(FXRadius-Round(Ratio*FYRadius));
      Inc(FCircleRect.Left,Dif);
      Dec(FCircleRect.Right,Dif);
    end
    else
    Begin
      dif:=(FYRadius-Round(1.0*FXRadius/Ratio));
      Inc(FCircleRect.Top,Dif);
      Dec(FCircleRect.Bottom,Dif);
    end;
    AdjustCircleRect;
  end;

  Procedure AdjustCircleMarks;
  Var tmpH     : Integer;
      tmpW     : Integer;
      tmpFrame : Integer;
  begin
    tmpFrame:=Marks.ArrowLength;
    With Marks.Frame do if Visible then Inc(tmpFrame,2*Width);
    With ParentChart do
    Begin
      Canvas.AssignFont(Marks.Font);
      tmpH:=Canvas.FontHeight+tmpFrame;
      Inc(FCircleRect.Top,tmpH);
      Dec(FCircleRect.Bottom,tmpH);
      tmpW:=MaxMarkWidth+Canvas.TextWidth(TeeCharForHeight)+tmpFrame;
      Inc(FCircleRect.Left,tmpW);
      Dec(FCircleRect.Right,tmpW);
      AdjustCircleRect;
    end;
  end;

begin
  inherited;
  FCircleRect:=ParentChart.ChartRect;
  AdjustCircleRect;
  if Marks.Visible then AdjustCircleMarks;
  if FCircled then CalcCircledRatio;
  CalcRadius;
end;

Procedure TCircledSeries.CalcRadius;
Begin
  if CustomXRadius<>0 then
  Begin
    FXRadius:=CustomXRadius;
    FCircleWidth:=2*FXRadius;
  end
  else FXRadius:=FCircleWidth shr 1;

  if CustomYRadius<>0 then
  begin
    FYRadius:=CustomYRadius;
    FCircleHeight:=2*FYRadius;
  end
  else FYRadius:=FCircleHeight shr 1;

  With FCircleRect do
  begin
    Left  :=FCircleXCenter-FXRadius;
    Right :=FCircleXCenter+FXRadius;
    Top   :=FCircleYCenter-FYRadius;
    Bottom:=FCircleYCenter+FYRadius;
  end;
end;

procedure TCircledSeries.SetCircleBackColor(Value:TColor);
begin
  SetColorProperty(FCircleBackColor,Value);
end;

Procedure TCircledSeries.AngleToPos( Const Angle,AXRadius,AYRadius:Double;
                                     Var X,Y:Integer);
var tmpSin : Extended;
    tmpCos : Extended;
Begin
  SinCos(IRotDegree+Angle,tmpSin,tmpCos);
  X:=FCircleXCenter+Round(AXRadius*tmpCos);
  Y:=FCircleYCenter-Round(AYRadius*tmpSin);
end;

Function TCircledSeries.CalcCircleBackColor:TColor;
begin
  result:=FCircleBackColor;
  if result=clTeeColor then
     if ParentChart.Printing then result:=clWhite
     else
     With (ParentChart as TCustomChart).BackWall do
        if not Transparent then result:=Color;
  if result=clTeeColor then result:=ParentChart.Color;
end;

Procedure TCircledSeries.PrepareLegendCanvas( ValueIndex:Integer; Var BackColor:TColor;
                                              Var BrushStyle:TBrushStyle);
Begin
  BackColor:=CalcCircleBackColor;
end;

Function TCircledSeries.PointToAngle(x,y:Integer):Double;
Begin
  if (x-FCircleXCenter)=0 then
  begin
    if y>FCircleYCenter then result:=-0.5*Pi{1.5*pi}
                        else result:=0.5*Pi;
  end
  else
  if (FYRadius=0) or (FYRadius=0) then result:=0
  else
    result:=ArcTan2( ((FCircleYCenter-y)/FYRadius),
                     ((x-FCircleXCenter)/FXRadius));
  if result<0 then result:=2.0*Pi+result;
  result:=result-IRotDegree;
  if result<0 then result:=2.0*Pi+result;
End;

Procedure TCircledSeries.Assign(Source:TPersistent);
begin
  if Source is TCircledSeries then
  With TCircledSeries(Source) do
  begin
    Self.FCircled         := FCircled; 
    Self.FRotationAngle   := FRotationAngle;
    Self.FCustomXRadius   := CustomXRadius;
    Self.FCustomYRadius   := CustomYRadius;
    Self.FCircleBackColor := FCircleBackColor;
  end;
  inherited;
end;

Procedure TCircledSeries.SetActive(Value:Boolean);
begin
  inherited;
  SetParentProperties(not Active);
end;

{$IFNDEF D4}

{ TPieAngles }
Function TPieAngles.GetAngle(Index:Integer):TPieAngle;
begin
  result:=TPieAngle(Items[Index]);
end;
{$ENDIF}

{ TExplodedSlices }
Function TExplodedSlices.Get(Index:Integer):Integer;
begin
  if (Index<Count) and Assigned(Items[Index]) then
     result:=Integer(Items[Index])
  else
     result:=0
end;

Procedure TExplodedSlices.Put(Index,Value:Integer);
begin
  While Index>=Count do Add(nil);
  if Get(Index)<>Value then
  begin
    Items[Index]:=Pointer(Value);
    OwnerSeries.Repaint;
  end;
end;

{ TPieOtherSlice }
Constructor TPieOtherSlice.Create(AOwner: TChartSeries);
begin
   inherited Create;
   FOwner:=AOwner;
   FColor:=clTeeColor;
   FText:=TeeMsg_PieOther;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TPieOtherSlice.Load(var S: TOldStream; AOwner: TChartSeries);
begin
   inherited Create;
   FOwner:=AOwner;
   S.Read(FColor,SizeOf(TColor));
   S.Read(FStyle,SizeOf(TPieOtherStyle));
   FText:=S.ReadString;
   S.Read(FValue,SizeOf(Double));
end;

Procedure TPieOtherSlice.Store(var S: TOldStream);
begin
   S.Write(FColor,SizeOf(TColor));
   S.Write(FStyle,SizeOf(TPieOtherStyle));
   S.WriteString(FText);
   S.Write(FValue,SizeOf(Double));
end;

Function TPieOtherSlice.OnCompareItems(Item: TPersistent): Boolean;
begin
   Result:=FALSE;
   if Item is TPieOtherSlice then
      with TPieOtherSlice(Item) do
         if Self.FColor = FColor then
            if Self.FStyle = FStyle then
               if Self.FText = FText then
                  Result := Self.FValue = FValue;
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

Procedure TPieOtherSlice.Assign(Source:TPersistent);
begin
  if Source is TPieOtherSlice then
  With TPieOtherSlice(Source) do
  begin
    Self.FColor:=FColor;
    Self.FStyle:=FStyle;
    Self.FText :=FText;
    Self.FValue:=FValue;
  end;
end;

Function TPieOtherSlice.IsOtherStored:Boolean;
begin
  result:=FText<>TeeMsg_PieOther;
end;

procedure TPieOtherSlice.SetColor(Value:TColor);
begin
  TSeriesAccess(FOwner).SetColorProperty(FColor,Value);
end;

procedure TPieOtherSlice.SetStyle(Value:TPieOtherStyle);
begin
  if FStyle<>Value then
  begin
    FStyle:=Value;
    FOwner.Repaint;
  end;
end;

procedure TPieOtherSlice.SetText(Const Value:String);
begin
  TSeriesAccess(FOwner).SetStringProperty(FText,Value);
end;

procedure TPieOtherSlice.SetValue(Const Value:Double);
begin
  TSeriesAccess(FOwner).SetDoubleProperty(FValue,Value);
end;

{ TPieSeries }
Const TeePieBelongsToOther = -1;
      TeePieOtherFlag      = MaxLongint;

Constructor TPieSeries.Create(AOwner: TComponent);
begin
   inherited;
   {$IFNDEF D4}
   FAngles:=TPieAngles.Create;
   {$ENDIF}
   FAngleSize:=360;
   FExplodedSlice:=TExplodedSlices.Create;
   FExplodedSlice.OwnerSeries:=Self;
   FOtherSlice:=TPieOtherSlice.Create(Self);
   FShadow:=TTeeShadow.Create(CanvasChanged);
   FShadow.Color:=Tee_PieShadowColor;
   XValues.Name:='';
   YValues.Name:=TeeMsg_ValuesPie;
   Marks.Visible:=True;
   Marks.ArrowLength:=8;
   FDark3D:=True;
   ColorEachPoint:=True;
   IUseSeriesColor:=True;
end;

Destructor TPieSeries.Destroy;
begin
  {$IFDEF D4}
  SetLength(FAngles,0);
  {$ELSE}
  FreePieAngles;
  FAngles.Free;
  {$ENDIF}
  FreeAndNil(FExplodedSlice);
  FShadow.Free;
  FOtherSlice.Free;
  inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
Constructor TPieSeries.Load(var S: TOldStream; AOwner: TComponent);
begin
   inherited Load(S,AOwner);
   S.Read(FAngleSize,SizeOf(Integer));

   FExplodedSlice:=TExplodedSlices.Create;
   FExplodedSlice.OwnerSeries:=Self;

   FOtherSlice:=TPieOtherSlice.Load(S,Self);
   FShadow:=TTeeShadow.Load(S,CanvasChanged);

   S.Read(FDark3D,SizeOf(Boolean));

   S.Read(FDonutPercent,SizeOf(Integer));
   S.Read(FExplodeBiggest,SizeOf(Integer));
   S.Read(FUsePatterns,SizeOf(Boolean));
end;

Procedure TPieSeries.Store(var S: TOldStream);
begin
   inherited Store(S);
   S.Write(FAngleSize,SizeOf(Integer));

   FOtherSlice.Store(S);
   FShadow.Store(S);

   S.Write(FDark3D,SizeOf(Boolean));
   
   S.Write(FDonutPercent,SizeOf(Integer));
   S.Write(FExplodeBiggest,SizeOf(Integer));
   S.Write(FUsePatterns,SizeOf(Boolean));
end;

Function TPieSeries.CreateDeterminant(Determinant: String): String;
begin
   Result:=Determinant;
end;

Function TPieSeries.OnCompareItems(Item: TChartSeries): Boolean;
begin
   Result:=FALSE;
   if Item is TPieSeries then
      with TPieSeries(Item) do
         if Self.FAngleSize = FAngleSize then
            if Self.FDark3D = FDark3D then
               if Self.FDonutPercent = FDonutPercent then
                  if Self.FExplodeBiggest = FExplodeBiggest then
                     if Self.FUsePatterns = FUsePatterns then
                        if Self.FOtherSlice.OnCompareItems(FOtherSlice) then
                           if Self.FShadow.OnCompareItems(FShadow) then
                              Result := inherited OnCompareItems(Item);
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{$IFNDEF D4}
Procedure TPieSeries.FreePieAngles;
var t : Integer;
begin
  With FAngles do
  begin
    for t:=0 to Count-1 do Angle[t].Free;
    Clear;
  end;
end;
{$ENDIF}

Function TPieSeries.CalcXPos(ValueIndex:Integer):Integer;
begin
  if XValues.Value[ValueIndex]=TeePieOtherFlag then  { do not try to calc }
     result:=0
  else
     result:=inherited CalcXPos(ValueIndex);
end;

Function TPieSeries.NumSampleValues:Integer;
Begin
  result:=8;
End;

Procedure TPieSeries.ClearLists;
begin
  inherited;
  if Assigned(FExplodedSlice) then FExplodedSlice.Clear;
end;

Procedure TPieSeries.PrepareLegendCanvas( ValueIndex:Integer; Var BackColor:TColor;
                                          Var BrushStyle:TBrushStyle);
Begin
  inherited;
  ParentChart.Canvas.AssignVisiblePen(PiePen);
  if FUsePatterns or ParentChart.Monochrome then
     BrushStyle:=GetDefaultPattern(ValueIndex);
end;

Procedure TPieSeries.GalleryChanged3D(Is3D:Boolean);
begin
  inherited;
  DisableRotation;
  Circled:=not ParentChart.View3D;
end;

Procedure TPieSeries.AddSampleValues(NumValues:Integer);
Const PieSampleStr:Array[0..7] of String=
         ( TeeMsg_PieSample1,TeeMsg_PieSample2,TeeMsg_PieSample3,
           TeeMsg_PieSample4,TeeMsg_PieSample5,TeeMsg_PieSample6,
           TeeMsg_PieSample7,TeeMsg_PieSample8);

var t : Integer;
Begin
  for t:=0 to NumValues-1 do
      Add( 1+System.Random(ChartSamplesMax), { <-- Value }
           PieSampleStr[t mod 8]      { <-- Label }
{$IFNDEF D4},clTeeColor{$ENDIF} );    { <-- Color }
end;

Function TPieSeries.CalcClickedPie(x,y:Integer):Integer;
Var tmpAngle : Double;
    t        : Integer;
    tmpOffX  : Integer;
    tmpOffY  : Integer;
Begin
  result:=TeeNoPointClicked;
  if Assigned(ParentChart) then ParentChart.Canvas.Calculate2DPosition(x,y,0);
  tmpAngle:=PointToAngle(x,y);
  for t:=0 to Count-1 do
  begin
    CalcExplodedOffset(t,tmpOffX,tmpOffY);
    if (Abs(x-FCircleXCenter)<(FXRadius+tmpOffX)) and
       (Abs(y-FCircleYCenter)<(FYRadius+tmpOffY)) then
    With FAngles[t] do
    if (tmpAngle>=StartAngle) and (tmpAngle<=EndAngle) then
    Begin
      result:=t;
      Exit;
    end;
  end;
end;

Function TPieSeries.Clicked(x,y:Integer):Integer;
begin
  result:=inherited Clicked(x,y);
  if result=TeeNoPointClicked then result:=CalcClickedPie(x,y);
end;

Function TPieSeries.CountLegendItems:Integer;
var t : Integer;
begin
  result:=Count;
  for t:=0 to Count-1 do if BelongsToOtherSlice(t) then Dec(result);
end;

Function TPieSeries.LegendToValueIndex(LegendIndex:Integer):Integer;
var Num : Integer;
    t   : Integer;
begin
  result:=LegendIndex;
  Num:=-1;
  for t:=0 to Count-1 do
  if not BelongsToOtherSlice(t) then
  begin
    Inc(Num);
    if Num=LegendIndex then
    begin
      result:=t;
      break;
    end;
  end;
end;

Function TPieSeries.BelongsToOtherSlice(ValueIndex:Integer):Boolean;
begin
  result:=XValues.Value[ValueIndex]=TeePieBelongsToOther;
end;

Procedure TPieSeries.CalcAngles;
Var tmpSumAbs : Double;
    AcumValue : Double;
    PiPortion : Double;
    t         : Integer;
    TotalAngle: Double;
Begin
  TotalAngle:=2.0*Pi*FAngleSize/360.0;
  AcumValue:=0;
  tmpSumAbs:=YValues.TotalAbs;
  if tmpSumAbs<>0 then PiPortion:=TotalAngle/tmpSumAbs
                  else PiPortion:=0;
  {$IFDEF D4}
  SetLength(FAngles,Count);
  {$ELSE}
  FreePieAngles;
  {$ENDIF}
  for t:=0 to Count-1 do
  begin
    {$IFNDEF D4}
    FAngles.Add(TPieAngle.Create);
    {$ENDIF}
    With FAngles[t] do
    Begin
      if t=0 then StartAngle:=0 else StartAngle:=FAngles[t-1].EndAngle;
      if tmpSumAbs<>0 then
      Begin
        if not BelongsToOtherSlice(t) then
           AcumValue:=AcumValue+Abs(YValues.Value[t]);
        if AcumValue=tmpSumAbs then EndAngle:=TotalAngle
                               else EndAngle:=AcumValue*PiPortion;
        { prevent small pie sectors }
        if (EndAngle-StartAngle)>TotalAngle then EndAngle:=StartAngle+TotalAngle;
      end
      else EndAngle:=TotalAngle;
      MidAngle:=(StartAngle+EndAngle)*0.5;
    end;
  end;
end;

Procedure TPieSeries.CalcExplodedRadius(ValueIndex:Integer; Var AXRadius,AYRadius:Integer);
var tmpExp : Double;
begin
  tmpExp:=1.0+FExplodedSlice[ValueIndex]*0.01;
  AXRadius:=Round(FXRadius*tmpExp);
  AYRadius:=Round(FYRadius*tmpExp);
end;

Procedure TPieSeries.DrawMark( ValueIndex:Integer; Const St:String;
                               APosition:TSeriesMarkPosition);
var tmpXRadius : Integer;
    tmpYRadius : Integer;
    tmp        : Double;
Begin
  if not BelongsToOtherSlice(ValueIndex) then
  begin
    With ParentChart,FAngles[ValueIndex] do
    begin
      CalcExplodedRadius(ValueIndex,tmpXRadius,tmpYRadius);
      if Canvas.SupportsFullRotation then
      begin
        tmp:=MidAngle+Pi+0.5*Pi;
        Marks.ZPosition:=StartZ;
      end
      else
      begin
        tmp:=MidAngle;
        Marks.ZPosition:=EndZ;
      end;

      With APosition do
      begin
        ArrowFix:=True;
        AngleToPos( tmp,
                    tmpXRadius{+Canvas.TextWidth(TeeCharForHeight)}+Marks.ArrowLength,
                    tmpYRadius{+Canvas.FontHeight}+Marks.ArrowLength,
                    ArrowTo.X,ArrowTo.Y );

        AngleToPos( tmp,tmpXRadius,tmpYRadius,ArrowFrom.X,ArrowFrom.Y );

        With ArrowTo do
        begin
          if X>FCircleXCenter then LeftTop.X:=X
                              else LeftTop.X:=X-Width;
          if Y>FCircleYCenter then LeftTop.Y:=Y
                              else LeftTop.Y:=Y-Height;
        end;
      end;
    end;
    inherited;
  end;
end;

Function TPieSeries.CompareSlice(A,B:Integer):Integer;
Var TotalAngle : Double;

  Function GetAngleSlice(Index:Integer):Double;
  begin
    result:=FAngles[Index].MidAngle+IRotDegree;
    if result>TotalAngle then result:=result-TotalAngle;
    if result>(0.25*TotalAngle) then
    begin
      result:=result-(0.25*TotalAngle);
      if result>Pi then result:=TotalAngle-result;
    end
    else result:=(0.25*TotalAngle)-result;
  end;

Var tmpA : Double;
    tmpB : Double;
begin
  TotalAngle:=2.0*Pi*FAngleSize/360.0;
  tmpA:=GetAngleSlice(ISortedSlice[A]);
  tmpB:=GetAngleSlice(ISortedSlice[B]);
  if tmpA<tmpB then result:=-1 else
  if tmpA>tmpB then result:= 1 else result:= 0;
end;

Procedure TPieSeries.SwapSlice(a,b:Integer);
begin
  SwapInteger(ISortedSlice[a],ISortedSlice[b]);
end;

procedure TPieSeries.DrawAllValues;
Var t                : Integer;
    tmpCount         : Integer;
    MaxExploded      : Integer;
    MaxExplodedIndex : Integer;
    tmpOffX          : Integer;
    tmpOffY          : Integer;
begin
  if FExplodeBiggest>0 then CalcExplodeBiggest;
  MaxExplodedIndex:=-1;
  MaxExploded:=0;
  tmpCount:=Count-1;
  { calc biggest exploded index }
  for t:=0 to tmpCount do
  if FExplodedSlice[t]>MaxExploded then
  begin
    MaxExploded:=Round(FExplodedSlice[t]);
    MaxExplodedIndex:=t;
  end;
  { calc each slice angles }
  CalcAngles;
  { adjust circle rectangle }
  IsExploded:=MaxExplodedIndex<>-1;
  if IsExploded then
  begin
    CalcExplodedOffset(MaxExplodedIndex,tmpOffX,tmpOffY);
    InflateRect(FCircleRect,-Abs(tmpOffX) div 2,-Abs(tmpOffY) div 2);
    AdjustCircleRect;
    CalcRadius;
  end;
  { start xy pos }
  AngleToPos(0,FXRadius,FYRadius,iniX,iniY);

  With FShadow do
  if (Color<>clNone) and ((HorizSize<>0) or (VertSize<>0)) then
  With ParentChart.Canvas do
  begin
    Brush.Color:=FShadow.Color;
    Brush.Style:=bsSolid;
    Pen.Style:=psClear;
    With CircleRect do
         EllipseWithZ(Left+HorizSize,Top+VertSize,Right+HorizSize,Bottom+VertSize,EndZ-10);
  end;

  { exploded slices drawing order... }
  if ParentChart.View3D and IsExploded
     and (not ParentChart.Canvas.SupportsFullRotation) then
  begin
    {$IFDEF D4}
    SetLength(ISortedSlice,tmpCount+1);
    {$ENDIF}
    for t:=0 to tmpCount do ISortedSlice[t]:=t;
    TeeSort(0,tmpCount,CompareSlice,SwapSlice);
    for t:=0 to tmpCount do DrawValue(ISortedSlice[t]);
    {$IFDEF D4}
    SetLength(ISortedSlice,0);
    {$ENDIF}
  end
  else inherited;
end;

Procedure TPieSeries.DrawPie(ValueIndex:Integer);
var tmpOffX : Integer;
    tmpOffY : Integer;
Begin
  ParentChart.Canvas.AssignVisiblePen(PiePen);
  CalcExplodedOffset(ValueIndex,tmpOffX,tmpOffY);

  With FAngles[ValueIndex] do
  with ParentChart,Canvas do { Draw pie slice }
  if View3D then
     Pie3D( FCircleXCenter+tmpOffX,
            FCircleYCenter-tmpOffY,
            FXRadius,FYRadius,
            StartZ,EndZ,
            StartAngle+IRotDegree,EndAngle+IRotDegree,
            FDark3D,
            IsExploded)
  else
  begin
    if FDonutPercent>0 then
       Donut( FCircleXCenter+tmpOffX,
              FCircleYCenter-tmpOffY,
              FXRadius,FYRadius,
              StartAngle+IRotDegree,EndAngle+IRotDegree,
              FDonutPercent)
    else
    begin
      AngleToPos(EndAngle,FXRadius,FYRadius,endX,endY);
      if ((IniX<>EndX) or (IniY<>EndY)) or (Count=1) or
         ( (Count>1) and (EndAngle-StartAngle>1))  then { bug win32 api }
      begin
        With FCircleRect do
             Pie( Left + tmpOffX,Top   -tmpOffY,
                  Right+ tmpOffX,Bottom-tmpOffY,
                  IniX + tmpOffX,IniY  -tmpOffY,
                  EndX + tmpOffX,EndY  -tmpOffY);
        IniX:=EndX;
        IniY:=EndY;
      end;
    end;
  end;
end;

Procedure TPieSeries.CalcExplodedOffset( ValueIndex:Integer;
                                         Var OffsetX,OffsetY:Integer);
var tmpExp : Double;
    tmpSin : Extended;
    tmpCos : Extended;
    tmp    : Double;
begin
  OffsetX :=0;
  OffsetY :=0;
  if IsExploded then
  begin
    tmpExp:=FExplodedSlice[ValueIndex];
    if tmpExp>0 then
    begin { Apply exploded % to radius }
      With FAngles[ValueIndex] do
      if ParentChart.Canvas.SupportsFullRotation then tmp:=MidAngle+(0.25*2.0*Pi*FAngleSize/360.0)+Pi
                                                 else tmp:=MidAngle;
      SinCos(tmp+IRotDegree,tmpSin,tmpCos);
      tmpExp:=tmpExp*0.01;
      OffsetX:=Round(FXRadius*tmpExp*tmpCos);
      OffsetY:=Round(FYRadius*tmpExp*tmpSin);
    end;
  end;
end;

Procedure TPieSeries.CalcExplodeBiggest;
var tmp : Integer;
begin
  With YValues do tmp:=Locate(MaxValue);
  if tmp<>-1 then FExplodedSlice[tmp]:=FExplodeBiggest;
end;

Procedure TPieSeries.SetAngleSize(Value:Integer);
begin
  SetIntegerProperty(FAngleSize,Value);
end;

procedure TPieSeries.SetDonutPercent(Value:Integer);
begin
  SetIntegerProperty(FDonutPercent,Value);
end;

procedure TPieSeries.SetExplodeBiggest(Value:Integer);
begin
  SetIntegerProperty(FExplodeBiggest,Value);
  CalcExplodeBiggest;
end;

procedure TPieSeries.SetOtherSlice(Value:TPieOtherSlice);
begin
  FOtherSlice.Assign(Value);
end;

Procedure TPieSeries.DrawValue(ValueIndex:Integer);
var tmpColor : TColor;
Begin
  if (FCircleWidth>4) and (FCircleHeight>4) then
  if not BelongsToOtherSlice(ValueIndex) then
  begin
    { Slice pattern }
    if FUsePatterns or ParentChart.Monochrome then
       Brush.Style:=GetDefaultPattern(ValueIndex)
    else
       Brush.Style:=bsSolid;

    if ParentChart.Monochrome then tmpColor:=clBlack
                              else tmpColor:=ValueColor[ValueIndex];

    { Set slice back color }
    ParentChart.SetBrushCanvas(tmpColor,Brush,CalcCircleBackColor);
    DrawPie(ValueIndex);
  end;
end;

procedure TPieSeries.SetUsePatterns(Value:Boolean);
Begin
  SetBooleanProperty(FUsePatterns,Value);
end;

class Function TPieSeries.GetEditorClass:String;
Begin
  result:='TPieSeriesEditor'; { <-- dont translate ! }
end;

Function TPieSeries.GetPieValues:TChartValueList;
Begin
  result:=YValues;
end;

Function TPieSeries.GetPiePen:TChartPen;
begin
  result:=Pen;
end;

Procedure TPieSeries.SetDark3d(Value:Boolean);
Begin
  SetBooleanProperty(FDark3d,Value);
End;

Procedure TPieSeries.SetPieValues(Value:TChartValueList);
Begin
  SetYValues(Value);
end;

Procedure TPieSeries.SetShadow(Value:TTeeShadow);
begin
  FShadow.Assign(Value)
end;

Function TPieSeries.MaxXValue:Double;
Begin
  result:=GetHorizAxis.Maximum;
End;

Function TPieSeries.MinXValue:Double;
Begin
  result:=GetHorizAxis.Minimum;
End;

Function TPieSeries.MaxYValue:Double;
Begin
  result:=GetVertAxis.Maximum;
End;

Function TPieSeries.MinYValue:Double;
Begin
  result:=GetVertAxis.Minimum;
End;

Procedure TPieSeries.DisableRotation;
begin
  With ParentChart.View3DOptions do
  begin
    Orthogonal:=False;
    Rotation:=0;
    Elevation:=305;
  end;
end;

Procedure TPieSeries.PrepareForGallery(IsEnabled:Boolean);
Begin
  inherited;
  FillSampleValues(8);
  ParentChart.Chart3DPercent:=75;
  Marks.ArrowLength:=0;
  Marks.DrawEvery:=1;
  DisableRotation;
  ColorEachPoint:=IsEnabled;
end;

Procedure TPieSeries.Assign(Source:TPersistent);
begin
  if Source is TPieSeries then
  With TPieSeries(Source) do
  begin
    Self.FAngleSize     :=FAngleSize;
    Self.FDark3d        :=FDark3D;
    Self.FDonutPercent  :=FDonutPercent;
    Self.FUsePatterns   :=FUsePatterns;
    Self.FExplodeBiggest:=FExplodeBiggest;
    Self.OtherSlice     :=FOtherSlice;
    Self.Shadow         :=FShadow;
  end;
  inherited;
  ColorEachPoint:=True;
end;

Function TPieSeries.AddPie( Const AValue:Double;
                            Const ALabel:String; AColor:TColor):Integer;
begin
  result:=Add(AValue,ALabel,AColor);
end;

procedure TPieSeries.DoBeforeDrawChart;
var t        : Integer;
    tmp      : Double;
    tmpValue : Double;
begin
  { re-order values }
  With PieValues do if Order<>loNone then Sort;
  { remove "other" slice, if exists... }
  for t:=0 to Count-1 do
  if XValues.Value[t]=TeePieOtherFlag then
  begin
    Delete(t);
    Break;
  end;
  { reset X order... }
  XValues.FillSequence;
  { calc "Other" slice... }
  if (FOtherSlice.Style<>poNone) and (YValues.TotalABS>0) then
  Begin
    tmpValue:=0;
    for t:=0 to Count-1 do
    begin
      tmp:=YValues.Value[t];
      if FOtherSlice.Style=poBelowPercent then tmp:=tmp*100.0/YValues.TotalAbs;
      if tmp<FOtherSlice.Value then
      begin
        tmpValue:=tmpValue+YValues.Value[t];
        XValues.Value[t]:=TeePieBelongsToOther; { <-- belongs to "other" }
      end;
    end;
    { Add "Other" slice }
    if tmpValue<>0 then
    begin
      AddXY(TeePieOtherFlag,tmpValue,FOtherSlice.Text,FOtherSlice.Color);
      With YValues do TotalABS:=TotalABS-tmpValue; { reset Y total }
    end;
  end;
end;

procedure TPieSeries.SwapValueIndex(a,b:Integer);
begin
  inherited;
  With FExplodedSlice do
  begin
    While Self.Count>Count do Put(Count,0);
    Exchange(a,b);
  end;
end;

class Procedure TPieSeries.SetSubGallery(ASeries:TChartSeries; Index:Integer);
begin
  With TPieSeries(ASeries) do
  Case Index of
    1: UsePatterns:=True;
    2: ExplodeBiggest:=30;
    3: With Shadow do
       begin
         HorizSize:=10;
         VertSize:=10;
       end;
    4: begin
         Marks.Visible:=True;
         Clear;
         Add(30,'A'{$IFNDEF D4},clTeeColor{$ENDIF});
         Add(70,'B'{$IFNDEF D4},clTeeColor{$ENDIF});
       end;
    5: AngleSize:=180;
    6: Pen.Visible:=False;
  end;
end;

class Procedure TPieSeries.CreateSubGallery(AddSubChart:TChartSubGalleryProc);
begin
  inherited;
  AddSubChart(TeeMsg_Patterns);
  AddSubChart(TeeMsg_Exploded);
  AddSubChart(TeeMsg_Shadow);
  AddSubChart(TeeMsg_Marks);
  AddSubChart(TeeMsg_SemiPie);
  AddSubChart(TeeMsg_NoBorder);
end;

{ TFastLineSeries }
Constructor TFastLineSeries.Create(AOwner: TComponent);
Begin
  inherited;
  AllowSinglePoint:=False;
  DrawBetweenPoints:=True;
  FAutoRepaint:=True;
End;

Procedure TFastLineSeries.SetPen(Const Value:TChartPen);
Begin
  inherited;
  SeriesColor:=LinePen.Color;
End;

Procedure TFastLineSeries.NotifyNewValue(Sender:TChartSeries; ValueIndex:Integer);
{$IFNDEF TEEOCX}
Var tmpIndex : Integer;
{$ENDIF}
begin
  if AutoRepaint then inherited
  {$IFNDEF TEEOCX}
  else
  begin
    With ParentChart,Canvas do
    begin
      AssignVisiblePen(LinePen);
      if ValueIndex=0 then tmpIndex:=0
                      else tmpIndex:=Pred(ValueIndex);
      if View3D then
         MoveTo3D(CalcXPos(tmpIndex),CalcYPos(tmpIndex),MiddleZ)
      else
         MoveTo(CalcXPos(tmpIndex),CalcYPos(tmpIndex));
    end;
    DrawValue(ValueIndex);
  end;
  {$ENDIF}
end;

class Function TFastLineSeries.GetEditorClass:String;
Begin
  result:='TFastLineSeriesEditor'; { <-- dont translate ! }
End;

procedure TFastLineSeries.PrepareCanvas;
Begin
  With ParentChart,Canvas do
  begin
    AssignVisiblePen(LinePen);
    CheckPenWidth(Pen);
    Brush.Style:=bsClear;
    BackMode:=cbmTransparent;
  end;
end;

procedure TFastLineSeries.DrawAllValues;
Begin
  PrepareCanvas;
  inherited;
End;

procedure TFastLineSeries.DrawValue(ValueIndex:Integer);
var X : Integer;
    Y : Integer;

   Procedure FirstPoint;
   var tmpX  : Integer;
       tmpY  : Integer;
   begin
     tmpX:=CalcXPos(ValueIndex-1);
     tmpY:=CalcYPos(ValueIndex-1);
     With ParentChart.Canvas do
     if ParentChart.View3D then LineWithZ(tmpX,tmpY,X,Y,MiddleZ)
                           else Line(tmpX,tmpY,X,Y)
   end;

Begin
  X:=CalcXPos(ValueIndex);
  Y:=CalcYPos(ValueIndex);
  if ValueIndex=FirstValueIndex then
  begin
    if ValueIndex>0 then FirstPoint
    else
    With ParentChart.Canvas do
    if ParentChart.View3D then MoveTo3D(X,Y,MiddleZ) else MoveTo(X,Y)
  end
  else
  if (X<>OldX) or (Y<>OldY) then
     With ParentChart.Canvas do
     if ParentChart.View3D then LineTo3D(X,Y,MiddleZ) else LineTo(X,Y)
  else
     Exit;
  OldX:=X;
  OldY:=Y;
End;

Procedure TFastLineSeries.SetSeriesColor(AColor:TColor);
begin
  inherited;
  LinePen.Color:=AColor;
end;

Procedure TFastLineSeries.DrawLegendShape(ValueIndex:Integer; Const Rect:TRect);
begin
  PrepareCanvas;
  With Rect do ParentChart.Canvas.DoHorizLine(Left,Right,(Top+Bottom) shr 1);
end;

Procedure TFastLineSeries.Assign(Source:TPersistent);
begin
  if Source is TFastLineSeries then
     FAutoRepaint:=TFastLineSeries(Source).FAutoRepaint;
  inherited;
end;

Function TFastLineSeries.Clicked(x,y:Integer):Integer;
var t    : Integer;
    OldX : Integer;
    OldY : Integer;
    tmpX : Integer;
    tmpY : Integer;
    P    : TPoint;
begin
  result:=TeeNoPointClicked;
  if (FirstValueIndex>-1) and (LastValueIndex>-1) then
  begin
    if Assigned(ParentChart) then ParentChart.Canvas.Calculate2DPosition(X,Y,MiddleZ);
    OldX:=0;
    OldY:=0;
    P.X:=X;
    P.Y:=Y;
    for t:=FirstValueIndex to LastValueIndex do
    begin
      tmpX:=CalcXPos(t);
      tmpY:=CalcYPos(t);
      if (tmpX=X) and (tmpY=Y) then { clicked right on point }
      begin
        result:=t;
        break;
      end
      else
      if (t>FirstValueIndex) and PointInLine(P,tmpX,tmpY,OldX,OldY) then
      begin
        result:=t-1;
        break;
      end;
      OldX:=tmpX;
      OldY:=tmpY;
    end;
  end;
end;

Procedure TFastLineSeries.DrawMark( ValueIndex:Integer; Const St:String;
                                    APosition:TSeriesMarkPosition);
begin
  Marks.ApplyArrowLength(APosition);
  inherited;
end;

class Procedure TFastLineSeries.CreateSubGallery(AddSubChart:TChartSubGalleryProc);
begin
  inherited;
  AddSubChart(TeeMsg_Marks);
  AddSubChart(TeeMsg_Dotted);
end;

class Procedure TFastLineSeries.SetSubGallery(ASeries:TChartSeries; Index:Integer);
begin
  With TFastLineSeries(ASeries) do
  Case Index of
     1: Marks.Visible:=True;
     2: LinePen.SmallDots:=True;
  end;
end;

Procedure RegisterTeeStandardSeries;
begin
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{
   RegisterTeeSeries(TLineSeries,     TeeMsg_GalleryLine,     TeeMsg_GalleryStandard,2);
   RegisterTeeSeries(TBarSeries,      TeeMsg_GalleryBar,      TeeMsg_GalleryStandard,2);
   RegisterTeeSeries(THorizBarSeries, TeeMsg_GalleryHorizBar, TeeMsg_GalleryStandard,2);
   RegisterTeeSeries(TAreaSeries,     TeeMsg_GalleryArea,     TeeMsg_GalleryStandard,2);
   RegisterTeeSeries(TPointSeries,    TeeMsg_GalleryPoint,    TeeMsg_GalleryStandard,2);
   RegisterTeeSeries(TPieSeries,      TeeMsg_GalleryPie,      TeeMsg_GalleryStandard,1);
   RegisterTeeSeries(TFastLineSeries, TeeMsg_GalleryFastLine, TeeMsg_GalleryStandard,2);
   RegisterTeeSeries(THorizLineSeries,TeeMsg_GalleryHorizLine,TeeMsg_GalleryStandard,1);
}
   RegisterTeeSeries(TPieSeries,      TeeMsg_GalleryPie,      TeeMsg_GalleryStandard,1,stCircle);
   RegisterTeeSeries(TBarSeries,      TeeMsg_GalleryBar,      TeeMsg_GalleryStandard,2,stBar);
   RegisterTeeSeries(THorizBarSeries, TeeMsg_GalleryHorizBar, TeeMsg_GalleryStandard,2,stHorizBar);
   RegisterTeeSeries(TAreaSeries,     TeeMsg_GalleryArea,     TeeMsg_GalleryStandard,2,stArea);
   RegisterTeeSeries(TPointSeries,    TeeMsg_GalleryPoint,    TeeMsg_GalleryStandard,2,stPoint);
   RegisterTeeSeries(TLineSeries,     TeeMsg_GalleryLine,     TeeMsg_GalleryStandard,2,stLine);
   RegisterTeeSeries(TFastLineSeries, TeeMsg_GalleryFastLine, TeeMsg_GalleryStandard,2,stFastLine);
   RegisterTeeSeries(THorizLineSeries,TeeMsg_GalleryHorizLine,TeeMsg_GalleryStandard,1,stHorizLine);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
end;

end.


