{*****************************************}
{  TeeChart Pro                           }
{  Copyright (c) 1996-2000 David Berneda  }
{                                         }
{  Component Registration Unit            }
{                                         }
{ Funcs:   TCountTeeFunction              }
{          TCurveFittingTeeFunction       }
{          TAverageTeeFunction            }
{          TMovingAverageTeeFunction      }
{          TExpMovAveFunction             }
{          TExpAverageTeeFunction         }
{          TMomentumTeeFunction           }
{          TRSITeeFunction                }
{          TStdDeviationTeeFunction       }
{          TMACDFunction                  }
{          TRootMeanSquareFunction        }
{                                         }
{ Series:  TCandleSeries                  }
{          TVolumeSeries                  }
{          TSurfaceSeries                 }
{          TContourSeries                 }
{          TWaterFallSeries               }
{          TErrorBarSeries                }
{          TPolarSeries                   }
{          TBezierSeries                  }
{          TPoint3DSeries                 }
{          TDonutSeries                   }
{          TBoxPlotSeries                 }
{          THistogramSeries               }
{          TSmithSeries                   }
{          TPyramidSeries                 }
{          TMapSeries                     }
{                                         }
{ Tools:   TCursorTool                    }
{          TDragMarksTool                 }
{          TDrawLineTool                  }
{          THintsTool                     }
{          TRotateTool                    }
{          TAxisArrowTool                 }
{          TColorLineTool                 }
{          TColorBandTool                 }
{          TImageTool                     }
{                                         }
{ Other:   TDraw3D                        }
{          TTeeCommander                  }
{          TChartEditor                   }
{          TChartPreviewer                }
{          TChartScrollBar                }
{          TChartListBox                  }
{          TSeriesDataSet                 }
{          TChartGalleryPanel             }
{          TTeePreviewPanel               }
{          TChartGrid                     }
{          TChartGridNavigator            }
{          TChartPageNavigator            }
{          TChartWebSource                }
{          TSeriesTextSource              }
{                                         }
{ Sample Series:                          }
{          TMyPointSeries                 }
{          TBar3DSeries                   }
{          TBigCandleSeries               }
{          TImagePointSeries              }
{          TDeltaPoint                    }
{          TImageBarSeries                }
{          TWindRoseSeries                }
{          TClockSeries                   }
{          TBarJoinSeries                 }
{          TCalendarSeries                }
{                                         }
{ TeeChart Actions (not in Delphi 3)      }
{          *Many*                         }
{                                         }
{*****************************************}
{$I TeeDefs.inc}
unit TeeChartPro;

interface

procedure Register;

implementation

Uses Classes, Controls, Graphics, CandleCh, CurvFitt, ErrorBar, TeeErrBarEd,
     TeeSurfa, TeeNavigator,
     TeeSurfEdit, TeePolar, TeePolarEditor, TeeCandlEdi, StatChar,
     DsgnIntf, TeEngine,
     {$IFDEF D4}{$IFNDEF BCB}TeeSeriesDesign,{$ENDIF}{$ENDIF}
     Chart,
     TeeProcs, TeeChartReg, TeeEditPro, Dialogs, TeeConst, TeeProCo,
     TeeBezie, TeePoin3, TeCanvas, TeeScroB, TeeEdit, TeeComma, TeeVolEd, TeeLisB,
     TeeCount, TeeCumu, TeeDonut, TeeTools, TeeTriSurface, TeeData,
     TeeGalleryPanel, TeePrevi, TeePreviewPanel, TeePreviewPanelEditor,
     MyPoint, Bar3D, BigCandl, ImaPoint, ImageBar, TeeImaEd, TeeRose,
     TeeChartGrid, TeeBoxPlot, TeeDraw3D, TeeURL, TeeSeriesTextEd,
     {$IFDEF D4}TeeMapSeries, TeeChartActions, ActnList,{$ENDIF}
     TeeSmith, TeeCalendar;

type TChartEditorCompEditor=class(TComponentEditor)
     public
       procedure ExecuteVerb( Index : Integer ); override;
       function GetVerbCount : Integer; override;
       function GetVerb( Index : Integer ) : string; override;
     end;

     TPreviewPanelCompEditor=class(TComponentEditor)
     public
       procedure ExecuteVerb( Index : Integer ); override;
       function GetVerbCount : Integer; override;
       function GetVerb( Index : Integer ) : string; override;
     end;

{ TChartEditorCompEditor }
procedure TChartEditorCompEditor.ExecuteVerb( Index : Integer );
begin
  if Index=0 then TCustomChartEditor(Component).Execute
             else inherited;
end;

function TChartEditorCompEditor.GetVerbCount : Integer;
begin
  Result := inherited GetVerbCount+1;
end;

function TChartEditorCompEditor.GetVerb( Index : Integer ) : string;
begin
  if Index=0 then result:=TeeMsg_Test
             else result:=inherited GetVerb(Index);
end;

{ TPreviewPanelCompEditor }
procedure TPreviewPanelCompEditor.ExecuteVerb( Index : Integer );
begin
  if Index=0 then
  With TFormPreviewPanelEditor.CreatePanel(nil,TTeePreviewPanel(Component)) do
  try
    ShowModal;
  finally
    Free;
  end
  else inherited;
end;

function TPreviewPanelCompEditor.GetVerbCount : Integer;
begin
  Result:=inherited GetVerbCount+1;
end;

function TPreviewPanelCompEditor.GetVerb( Index : Integer ) : string;
begin
  if Index=0 then result:=TeeMsg_Edit
             else result:=inherited GetVerb(Index);
end;

type
  TTeeCustomToolAxisProperty = class(TPropertyEditor)
  public
    function GetAttributes : TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  end;

{ TTeeCustomToolAxisProperty }
function TTeeCustomToolAxisProperty.GetAttributes: TPropertyAttributes;
begin
  result:=[paValueList,paMultiSelect,paRevertable]
end;

function TTeeCustomToolAxisProperty.GetValue: string;
begin
  With TTeeCustomToolAxis(GetComponent(0)) do
  if Axis=ParentChart.LeftAxis then result:=TeeMsg_LeftAxis
  else
  if Axis=ParentChart.RightAxis then result:=TeeMsg_RightAxis
  else
  if Axis=ParentChart.TopAxis then result:=TeeMsg_TopAxis
  else
  if Axis=ParentChart.BottomAxis then result:=TeeMsg_BottomAxis;
end;

procedure TTeeCustomToolAxisProperty.GetValues(Proc: TGetStrProc);
begin
  inherited;
  Proc(TeeMsg_LeftAxis);
  Proc(TeeMsg_RightAxis);
  Proc(TeeMsg_TopAxis);
  Proc(TeeMsg_BottomAxis);
end;

procedure TTeeCustomToolAxisProperty.SetValue(const Value: string);
var tmp : TCustomAxisPanel;
begin
  tmp:=TTeeCustomToolAxis(GetComponent(0)).ParentChart;
  if Value=TeeMsg_LeftAxis then SetOrdValue(Integer(tmp.LeftAxis))
  else
  if Value=TeeMsg_RightAxis then SetOrdValue(Integer(tmp.RightAxis))
  else
  if Value=TeeMsg_TopAxis then SetOrdValue(Integer(tmp.TopAxis))
  else
  if Value=TeeMsg_BottomAxis then SetOrdValue(Integer(tmp.BottomAxis))
end;

type TChartWebCompEditor=class(TComponentEditor)
     public
       procedure ExecuteVerb(Index:Integer); override;
       function GetVerbCount:Integer; override;
       function GetVerb(Index:Integer):string; override;
     end;

{ TChartWebCompEditor }
procedure TChartWebCompEditor.ExecuteVerb(Index:Integer);
begin
  if Index=0 then TChartWebSource(Component).Execute
             else inherited;
end;

function TChartWebCompEditor.GetVerbCount:Integer;
begin
  Result:=inherited GetVerbCount+1;
end;

function TChartWebCompEditor.GetVerb(Index:Integer):string;
begin
  if Index=0 then result:=TeeMsg_Load
             else result:=inherited GetVerb(Index);
end;

type TSeriesTextCompEditor=class(TComponentEditor)
     public
       procedure ExecuteVerb(Index:Integer); override;
       function GetVerbCount:Integer; override;
       function GetVerb(Index:Integer):string; override;
     end;

{ TSeriesTextCompEditor }
procedure TSeriesTextCompEditor.ExecuteVerb(Index:Integer);
begin
  if Index=0 then
  begin
    TeeEditSeriesTextSource(TSeriesTextSource(Component));
    Designer.Modified;
  end
  else inherited;
end;

function TSeriesTextCompEditor.GetVerbCount:Integer;
begin
  Result:=inherited GetVerbCount+1;
end;

function TSeriesTextCompEditor.GetVerb(Index:Integer):string;
begin
  if Index=0 then result:=TeeMsg_Edit
             else result:=inherited GetVerb(Index);
end;

procedure Register;
begin
  RegisterComponents(TeeMsg_TeeChartPalette, [ TChartEditor,
                                               TChartPreviewer,
                                               TDraw3D,
                                               TChartScrollBar,
                                               TTeeCommander,
                                               TChartListBox,
                                               TSeriesDataSet,
                                               TChartEditorPanel,
                                               TChartGalleryPanel,
                                               TTeePreviewPanel,
                                               TChartGrid,
                                               TChartGridNavigator,
                                               TChartPageNavigator,
                                               TChartWebSource,
                                               TSeriesTextSource ]);

  RegisterComponentEditor(  TChartEditor,TChartEditorCompEditor);
  RegisterComponentEditor(  TChartPreviewer,TChartEditorCompEditor);
  RegisterComponentEditor(  TTeePreviewPanel,TPreviewPanelCompEditor);
  RegisterComponentEditor(  TChartWebSource,TChartWebCompEditor);
  RegisterComponentEditor(  TSeriesTextSource,TSeriesTextCompEditor);
  RegisterPropertyEditor(TypeInfo(TChartAxis), TTeeCustomToolAxis, '',TTeeCustomToolAxisProperty);

  {$IFDEF D4}
  RegisterActions(TeeMsg_CategoryChartActions,
                  [ TChartAction3D,TChartActionEdit,
                    TChartActionCopy,TChartActionPrint,
                    TChartActionAxes,TChartActionGrid,
                    TChartActionLegend,
                    TChartActionSave ],nil);

  RegisterActions(TeeMsg_CategorySeriesActions,
                  [ TSeriesActionActive,TSeriesActionEdit,
                    TSeriesActionMarks ], nil);

  {$ENDIF}
end;

{$IFDEF D4}
{$IFNDEF BCB}
initialization
  TeeChartEditorHook:=TeeShowSeriesEditor;
finalization
  TeeChartEditorHook:=nil;
{$ENDIF}
{$ENDIF}
end.
