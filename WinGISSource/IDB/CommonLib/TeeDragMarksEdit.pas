unit TeeDragMarksEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeeToolSeriesEdit, StdCtrls;

type
  TDragMarksToolEditor = class(TSeriesToolEditor)
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

Uses Chart, TeeConst;

procedure TDragMarksToolEditor.Button1Click(Sender: TObject);
var t : Integer;
begin
  if Assigned(Tool.Series) then
     Tool.Series.Marks.ResetPositions
  else
  With Tool.ParentChart do
     for t:=0 to SeriesCount-1 do
         Series[t].Marks.ResetPositions;
end;

procedure TDragMarksToolEditor.FormShow(Sender: TObject);
begin
  inherited;
  CBSeries.Items[0]:=TeeMsg_All;
  if CBSeries.ItemIndex=-1 then CBSeries.ItemIndex:=0;
end;

initialization
  RegisterClass(TDragMarksToolEditor);
end.
