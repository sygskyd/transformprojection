�
 TCANDLEEDITOR 0�  TPF0TCandleEditorCandleEditorLeftQTop� ActiveControlRGStyleBorderIconsbiSystemMenu BorderStylebsDialogClientHeight� ClientWidthColor	clBtnFace
ParentFont	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTopcWidthKHeight	AlignmenttaRightJustifyAutoSizeCaptionCandle &Width:  TRadioGroupRGStyleLeftTopWidthaHeightMHelpContext� CaptionStyle:	ItemIndex Items.Strings&Stick&Bar
Open Close TabOrder OnClickRGStyleClick  TButtonColorBUpColorLeftxTopWidth� HelpContext�Caption&Up Close...TabOrder  TButtonColor
BDownColorLeftxTop(Width� HelpContext� Caption&Down Close...TabOrder  TEditSEWidthLeftaTop_Width(HeightHelpContext� TabOrderText0OnChangeSEWidthChange  	TCheckBox
CBShowOpenLeft� TopLWidthUHeightHelpContext�Caption
Show &OpenTabOrderOnClickCBShowOpenClick  	TCheckBoxCBShowCloseLeft� TopdWidthUHeightHelpContext�CaptionShow &CloseTabOrderOnClickCBShowCloseClick  
TButtonPenButton1Left	Top� HelpContextwCaption
Bo&rder...TabOrder  TUpDownUDWidthLeft� Top_WidthHeight	AssociateSEWidthMin Position TabOrderWrap  	TCheckBoxCBDraw3DLeftbTop� WidthGHeightHelpContext� CaptionDraw &3DTabOrderOnClickCBDraw3DClick  	TCheckBoxCBDark3DLeft� Top� WidthYHeightHelpContext�CaptionDar&k 3DTabOrder	OnClickCBDark3DClick   