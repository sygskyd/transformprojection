{**********************************************}
{   TImageBarSeries Component Editor Dialog    }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeImaEd;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Chart, Series, ImageBar, ComCtrls;

type
  TImageBarSeriesEditor = class(TForm)
    GroupBox1: TGroupBox;
    Image1: TImage;
    BBrowse: TButton;
    CBTiled: TCheckBox;
    Bevel1: TBevel;
    procedure FormShow(Sender: TObject);
    procedure BBrowseClick(Sender: TObject);
    procedure CBTiledClick(Sender: TObject);
  private
    { Private declarations }
    ImageBarSeries : TImageBarSeries;
    procedure EnableImageControls;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeePenDlg, TeeBrushDlg, TeeConst, TeeBarEdit, TeeEdiSeri, TeeEdiPane, ExtDlgs;

procedure TImageBarSeriesEditor.FormShow(Sender: TObject);
begin
  ImageBarSeries:=TImageBarSeries(Tag);
  With ImageBarSeries do
  begin
    CBTiled.Checked:=ImageTiled;
    Image1.Picture.Assign(Image);
  end;
  With TFormTeeSeries(Parent.Owner).InsertSeriesForm( TBarSeriesEditor,
                                                      1,TeeMsg_GalleryBar,
                                                      ImageBarSeries) as
                                                      TBarSeriesEditor do
  begin
    LStyle.Visible:=False;
    CBBarStyle.Visible:=False;
  end;
  EnableImageControls;
end;

procedure TImageBarSeriesEditor.EnableImageControls;
begin
  CBTiled.Enabled:=Assigned(ImageBarSeries.Image.Graphic);
  if CBTiled.Enabled then BBrowse.Caption:=TeeMsg_ClearImage
                     else BBrowse.Caption:=TeeMsg_BrowseImage;
  Image1.Picture.Assign(ImageBarSeries.Image);
end;

procedure TImageBarSeriesEditor.BBrowseClick(Sender: TObject);
begin
  TeeLoadClearImage(Self,ImageBarSeries.Image);
  EnableImageControls;
end;

procedure TImageBarSeriesEditor.CBTiledClick(Sender: TObject);
begin
  ImageBarSeries.ImageTiled:=CBTiled.Checked;
end;

initialization
  RegisterClass(TImageBarSeriesEditor);
end.
