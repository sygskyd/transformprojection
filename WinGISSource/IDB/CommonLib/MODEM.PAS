Unit Modem;

Interface

Uses Classes,SerPort,SysUtils,WinProcs;

Type TTableEntry   = Array[0..20] of Char;

     TTextNotification = Procedure(Text:String) of object;

     TDialMethod   = (dmTone,dmPulse);

     EModem        = CLass(Exception);

     TModem        = Class(TComponent)
      Private
       FBaudrate        : LongInt;
       FCancel          : Boolean;
       FConnectBaudrate : LongInt;
       FConnected       : Boolean;
       FConnectTimeout  : Integer;
       FDialMethod     : TDialMethod;
       FHandshake       : Boolean;
       FInitString      : String;
       FInitialized     : Boolean;
       FOnCommandText   : TTextNotification;
       FPhoneNumber     : String;
       FPortNumber      : Integer;
       FSerialPort      : TSerialPort;
       FWaitForDialTone : Boolean;
       Function    GetPortOpened:Boolean;
       Procedure   RaiseError(MainError,SubError:Integer);
       Function    WriteCommand(var Text:String;const Table:Array of TTableEntry;
                       TimeOut:Double):Integer;
       Function    WriteWaitResult(var Text:String;TimeOut:Double):Integer;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Procedure   Cancel;
       Procedure   Close;
       Procedure   Connect;
       Property    Connectedudrate:LongInt read FConnectBaudRate;
       Property    Connected:Boolean read FConnected;
       Procedure   Disconnect;
       Procedure   Initialize;
       Property    Initialized:Boolean read FInitialized;
       Procedure   OpenPort;
       Property    PortOpened:Boolean read GetPortOpened;
       Function    OutBufferCount:Integer;
       Function    Read(var Buffer;BufferSize:Integer):Integer;
       Function    Write(var Buffer;BufferSize:Integer):Integer;
      Published
       Property    Baudrate:LongInt read FBaudRate write FBaudRate default 115200;
       Property    ConnectTimeout:Integer read FConnectTimeout write FConnectTimeout;
       Property    DialMethod:TDialMethod read FDialMethod write FDialMethod;
       Property    HardwareHandshake:Boolean read FHandshake write FHandshake default TRUE;
       Property    InitString:String read FInitString write FInitString;
       Property    OnCommandText:TTextNotification read FOnCommandText write FOnCommandText;
       Property    PhoneNumber:String read FPhoneNumber write FPhoneNumber;
       Property    Port:Integer read FPortNumber write FPortNumber default 1;
       Property    WaitForDialTone:Boolean read FWaitForDialTone write FWaitForDialTone default TRUE;
     end;

Function IdentToDialMethod(const Ident:String):TDialMethod;
Function DialMethodToIdent(DialMethod:TDialMethod):String;

Implementation

Uses Forms,MultiLng,StrTools;

Const CRLF         = #$0D#$0A;

Const CommandModeTable : Array[0..1] of TTableEntry =
                         ('+++', CRLF+'OK'+CRLF);

      CommandReplyTable : Array[0..7] of TTableEntry =
                         (CRLF+'OK'+CRLF, CRLF+'ERROR'+CRLF, CRLF+'BUSY'+CRLF,
                          CRLF+'NO CARRIER'+CRLF, CRLF+'NO DIALTONE'+CRLF,
                          CRLF+'NO ANSWER'+CRLF, CRLF+'CONNECT', CRLF+'DIAL LOCKED'+CRLF);  

Procedure Wait(Seconds:Double);
var StartTime      : TDateTime;
begin
  Seconds:=Seconds/86400;
  StartTime:=Now;
  while Now-StartTime<Seconds do;
end;

Constructor TModem.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  FSerialPort:=TSerialPort.Create(Self);
  FBaudRate:=115200;
  FPortNumber:=1;
  FHandshake:=TRUE;
  FWaitForDialTone:=TRUE;
  FDialMethod:=dmTone;
  FConnectTimeout:=30;
end;

Destructor TModem.Destroy;
begin
  Close;
  inherited Destroy;
end;

Procedure TModem.OpenPort;
begin
  FSerialPort.Active:=FALSE;
  { setup the port-parameters }
  FSerialPort.Port:=FPortNumber;
  FSerialPort.BaudRate:=FBaudRate;
  if FHandshake then begin
    FSerialPort.RTSControl:=rtsHandshake;
    FSerialPort.CTSHandshake:=TRUE;
  end;
  { open the port }
  FSerialPort.Open;
end;                          

Procedure TModem.RaiseError(MainError,SubError:Integer);
begin
  Raise EModem.Create(Format(MlgStringList['Modem',MainError],
      [MlgStringList['Modem',SubError]]));
end;

Procedure TModem.Initialize;
var SendText       : String;
begin
  { cancel a probably existing connection and look if modem is connected}
  SendText:='ATI0'+CRLF;
  case WriteWaitResult(SendText,2) of
    -2: RaiseError(1,3);
    -1: RaiseError(1,4);
  end;
  { write initialization-string }
  if FInitString<>'' then begin
    SendText:=FInitString+CRLF;
    case WriteCommand(SendText,CommandReplyTable,5) of
      -2: RaiseError(1,3);
      -1: RaiseError(1,5);
      0: ;
      else RaiseError(1,6);
    end;
  end;
  { set wait for dial-tone }
  SendText:='AT';
  if FWaitForDialTone then SendText:=SendText+'X4'
  else SendText:=SendText+'X3';
  SendText:=SendText+CRLF;
  case WriteCommand(SendText,CommandReplyTable,5) of
    -2: RaiseError(1,3);
    -1: RaiseError(1,5);
    0: FInitialized:=TRUE;
    else RaiseError(1,6);
  end;
end;

Procedure TModem.Connect;
var SendText       : String;
    TableEntry     : Integer;
begin
  { send dial-string }
  SendText:='ATD';
  if DialMethod=dmTone then SendText:=SendText+'T'
  else SendText:=SendText+'P';
  SendText:=SendText+PhoneNumber+CRLF;
  TableEntry:=WriteCommand(SendText,CommandReplyTable,FConnectTimeout);
  case TableEntry of
    -2: begin
          Disconnect;
          RaiseError(2,3);
        end;
    -1: begin
          Disconnect;
          RaiseError(2,7);
        end;
    0, 1: RaiseError(2,6);
    2: RaiseError(2,8);
    3: RaiseError(2,7);
    4: RaiseError(2,9);
    5: RaiseError(2,5);
    6: begin
         if ScanString('CONNECT %L',SendText,[@FConnectBaudRate])<>0 then
             FConnectBaudrate:=0;
         FConnected:=TRUE;
       end; 
    7: RaiseError(2,10);
  end;
end;

Function TModem.Write(var Buffer;BufferSize:Integer):Integer;
begin
  Result:=FSerialPort.Write(Buffer,BufferSize);
end;

Function TModem.Read(var Buffer;BufferSize:Integer):Integer;
begin
  Result:=FSerialPort.Read(Buffer,BufferSize);
end;

Procedure TModem.Disconnect;
var SendText       : String;
begin
  { wait some time because otherwise modem may not recognize +++ }
  if Connected then Wait(2);
  FConnected:=FALSE; 
  { send return-to-command-mode, if modem return OK it was online before,
    send ATH in that case }
  SendText:='+++';
  if WriteCommand(SendText,CommandModeTable,4)=1 then begin
    { send break-connection-command }
    SendText:='ATH'+CRLF;
    WriteCommand(SendText,CommandReplyTable,2);
  end;
end;

Function TModem.WriteWaitResult(var Text:String;TimeOut:Double):Integer;
var StartTime      : TDateTime;
    FirstTime      : TDateTime;
    CharTimeout    : Double;
    Buffer         : Array[0..255] of Char;
    AText          : Array[0..255] of Char;
    BytesRead      : Integer;
    BufferLength   : Integer;
begin
  StrPCopy(AText,Text);
  FSerialPort.Write(Text[1],Length(Text));
  FCancel:=FALSE;
  StartTime:=Now;
  TimeOut:=TimeOut/86400.0;
  CharTimeout:=0.2/86400.0;
  BufferLength:=0;
  repeat
    BytesRead:=FSerialPort.Read(Buffer[BufferLength],SizeOf(Buffer)-BufferLength-1);
    if BytesRead>0 then begin
      FirstTime:=Now;
      BufferLength:=BufferLength+BytesRead;
      repeat
        BytesRead:=FSerialPort.Read(Buffer[BufferLength],SizeOf(Buffer)-BufferLength-1);
        if BytesRead>0 then FirstTime:=Now; 
        BufferLength:=BufferLength+BytesRead;
        Application.HandleMessage;
      until FCancel or (Now-FirstTime>CharTimeOut);
      Buffer[BufferLength]:=#0;
      if Assigned(FOnCommandText) then OnCommandText(StrPas(Buffer));
      if BufferLength>0 then begin
        Text:=StrPas(Buffer);
        Result:=0;
        Exit;
      end;
    end;
    Application.HandleMessage;
  until FCancel or (Now-StartTime>TimeOut);
  if FCancel then Result:=-2
  else Result:=-1;
end;

Function TModem.WriteCommand(var Text:String;const Table:Array of TTableEntry;
    TimeOut:Double):Integer;
var StartTime      : TDateTime;
    FirstTime      : TDateTime;
    CharTimeout    : Double;
    Buffer         : Array[0..255] of Char;
    BytesRead      : Integer;
    BufferLength   : Integer;
    APos           : PChar;
    BPos           : PChar;
begin
  FSerialPort.Write(Text[1],Length(Text));
  FCancel:=FALSE;
  StartTime:=Now;
  TimeOut:=TimeOut/86400.0;
  CharTimeout:=0.5/86400.0;
  BufferLength:=0;
  repeat
    BytesRead:=FSerialPort.Read(Buffer[BufferLength],SizeOf(Buffer)-BufferLength-1);
    if BytesRead>0 then begin
      FirstTime:=Now;
      BufferLength:=BufferLength+BytesRead;
      repeat
        BytesRead:=FSerialPort.Read(Buffer[BufferLength],SizeOf(Buffer)-BufferLength-1);
        if BytesRead>0 then FirstTime:=Now; 
        BufferLength:=BufferLength+BytesRead;
        Application.HandleMessage;
      until FCancel or (Now-FirstTime>CharTimeOut);
      Buffer[BufferLength]:=#0;
      if Assigned(FOnCommandText) then OnCommandText(StrPas(Buffer));
      for Result:=0 to High(Table) do begin
        APos:=StrPos(Buffer,Table[Result]);
        if APos<>NIL then begin
          BPos:=StrPos(APos+1,CRLF);
          if BPos=NIL then begin
            BPos:=StrEnd(Buffer);
            BytesRead:=BPos-APos;
          end
          else BytesRead:=BPos-APos+2;
          SetLength(Text,BytesRead);
          Move(APos^,Text[1],BytesRead);
          Exit;
        end;
      end;
    end;
    Application.HandleMessage;
  until FCancel or (Now-StartTime>TimeOut);
  if FCancel then Result:=-2
  else Result:=-1;
end;

Procedure TModem.Cancel;
begin
  FCancel:=TRUE;
end;

Function TModem.OutBufferCount:Integer;
begin
  Result:=FSerialPort.OutBufferCount;
end;

Function TModem.GetPortOpened:Boolean;
begin
  Result:=FSerialPort.Active;
end;

Procedure TModem.Close;
begin
  if FSerialPort.Active then begin
    if Connected then Disconnect;
    FSerialPort.Active:=FALSE;
    FInitialized:=FALSE;
  end;
end;

Function IdentToDialMethod(const Ident:String):TDialMethod;
begin
  if CompareText(Ident,'PULSE')=0 then Result:=dmPulse
  else Result:=dmTone;
end;

Function DialMethodToIdent(DialMethod:TDialMethod):String;
begin
  if DialMethod=dmPulse then Result:='Pulse'
  else Result:='Tone';
end;

end.
