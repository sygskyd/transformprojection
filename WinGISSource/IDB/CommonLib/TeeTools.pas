{**********************************************}
{   TeeChart Tools                             }
{   Copyright (c) 1999-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeTools;

interface

Uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls,
     {$ELSE}
     Graphics, Controls,
     {$ENDIF}
     TeEngine, Chart, TeCanvas, TeeProcs
     {$IFDEF D6}
     ,Types
     {$ENDIF}
     ;

Const TeeClickTolerance = 3;

Type
  TTeeCustomToolSeries=class(TTeeCustomTool)
  private
    FSeries : TChartSeries;
  protected
    Function GetHorizAxis:TChartAxis;
    Function GetVertAxis:TChartAxis;
    class Function GetEditorClass:String; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetSeries(const Value: TChartSeries); virtual;
  public
    property Series:TChartSeries read FSeries write SetSeries;
  end;

  TCursorToolStyle=(cssHorizontal,cssVertical,cssBoth);

  TCursorTool=class;

  TCursorClicked=(ccNone,ccHorizontal,ccVertical,ccBoth);

  TCursorToolChangeEvent=procedure( Sender:TCursorTool;
                                    x,y:Integer;
                                    Const XValue,YValue:Double;
                                    Series:TChartSeries;
                                    ValueIndex:Integer) of object;

  TCursorToolGetAxisRect=procedure( Sender:TCursorTool; Var Rect:TRect) of object;

  TCursorTool=class(TTeeCustomToolSeries)
  private
    FFollowMouse   : Boolean;
    FOnChange      : TCursorToolChangeEvent;
    FOnGetAxisRect : TCursorToolGetAxisRect;
    FSnap          : Boolean;
    FStyle         : TCursorToolStyle;

    IDragging      : TCursorClicked;
    Procedure CalcScreenPositions;
    Procedure CalcValuePositions(X,Y:Integer);
    Function GetAxisRect:TRect;
    Procedure SetStyle(Value:TCursorToolStyle);
  protected
    IXValue     : Double;
    IYValue     : Double;
    IPoint      : TPoint;
    Procedure Changed(SnapPoint:Integer); virtual;
    Procedure ChartEvent(AEvent:TChartToolEvent); override;
    Procedure ChartMouseEvent( AEvent: TChartMouseEvent;
                               Button:TMouseButton;
                               Shift: TShiftState; X, Y: Integer); override;
    class Function GetEditorClass:String; override;
    procedure SetSeries(const Value: TChartSeries); override;
  public
    Constructor Create(AOwner:TComponent); override;

    Function Clicked(x,y:Integer):TCursorClicked;
    class Function Description:String; override;
    Function NearestPoint(AStyle:TCursorToolStyle; Var Difference:Double):Integer;
    Function SnapToPoint:Integer;
    procedure RedrawCursor;
    property OnGetAxisRect:TCursorToolGetAxisRect read FOnGetAxisRect write FOnGetAxisRect;
  published
    property Active;
    property FollowMouse:Boolean read FFollowMouse write FFollowMouse default False;
    property Pen;
    property Series;
    property Snap:Boolean read FSnap write FSnap default False;
    property Style:TCursorToolStyle read FStyle write SetStyle default cssBoth;
    { events }
    property OnChange:TCursorToolChangeEvent read FOnChange write FOnChange;
  end;

  TDragMarksTool=class(TTeeCustomToolSeries)
  private
    { internal }
    IOldX    : Integer;
    IOldY    : Integer;
    IPosition: TSeriesMarkPosition;
    ISeries  : TChartSeries;
  protected
    class Function GetEditorClass:String; override;
  public
    class Function Description:String; override;

    Procedure ChartMouseEvent( AEvent: TChartMouseEvent;
                               Button:TMouseButton;
                               Shift: TShiftState; X, Y: Integer); override;
  published
    property Active;
    property Series;
  end;

  TTeeCustomToolAxis=class(TTeeCustomTool)
  private
    FAxis: TChartAxis;
    procedure ReadAxis(Reader: TReader);
    procedure SetAxis(const Value: TChartAxis);
    procedure WriteAxis(Writer: TWriter);
  protected
    procedure DefineProperties(Filer: TFiler); override;
    class Function GetEditorClass:String; override;
  public
    property Axis:TChartAxis read FAxis write SetAxis stored False;
  end;

  TAxisArrowToolPosition=(aaStart,aaEnd,aaBoth);

  TAxisArrowTool=class(TTeeCustomToolAxis)
  private
    FLength   : Integer;
    FPosition : TAxisArrowToolPosition;
    FScrollPercent: Integer;
    Function ClickedArrow(x,y:Integer):Integer;
    procedure SetLength(const Value: Integer);
    procedure SetPosition(const Value: TAxisArrowToolPosition);
  protected
    Procedure ChartEvent(AEvent:TChartToolEvent); override;
    Procedure ChartMouseEvent( AEvent: TChartMouseEvent;
                               Button:TMouseButton;
                               Shift: TShiftState; X, Y: Integer); override;
    class function GetEditorClass: String; override;
  public
    Constructor Create(AOwner:TComponent); override;

    class Function Description:String; override;
  published
    property Active;
    property Position:TAxisArrowToolPosition read FPosition write SetPosition default aaBoth;
    property Axis;
    property Brush;
    property Length:Integer read FLength write SetLength default 16;
    property Pen;
    property ScrollPercent:Integer read FScrollPercent write FScrollPercent default 10;
  end;

  TFloatPoint=record
    X : Double;
    Y : Double;
  end;

  TDrawLineTool=class;

  TDrawLine=class(TObject)
  public
    EndPos   : TFloatPoint;
    Parent   : TDrawLineTool;
    Pen      : TChartPen;
    StartPos : TFloatPoint;
    Procedure DrawHandles;
    Function EndHandle:TRect;
    Function StartHandle:TRect;
  end;

  TDrawLines=class(TList)
  private
    IParent : TDrawLineTool;
    Function Get(Index:Integer):TDrawLine;
  public
    {$IFDEF D4}
    procedure Clear; override;
    {$ELSE}
    Destructor Destroy; override;
    {$ENDIF}
    property Line[Index:Integer]:TDrawLine read Get; default;
  end;

  TDrawLineHandle=(chNone,chStart,chEnd,chSeries);

  TDrawLineTool=class(TTeeCustomToolSeries)
  private
    FButton       : TMouseButton;
    FEnableDraw   : Boolean;
    FEnableSelect : Boolean;
    FLines        : TDrawLines;
    FOnDragLine   : TNotifyEvent;
    FOnNewLine    : TNotifyEvent;
    FOnSelect     : TNotifyEvent;

    IDrawing  : Boolean;
    IHandle   : TDrawLineHandle;
    IPoint    : TPoint;
    ISelected : TDrawLine;
    Procedure DrawLine(Const StartPos,EndPos:TPoint);
    Function InternalClicked(X,Y:Integer; AHandle:TDrawLineHandle):TDrawLine;
    Procedure SetEnableSelect(Value:Boolean);
    Procedure SetSelected(Value:TDrawLine);
    Procedure RedrawLine;
  protected
    Procedure ChartEvent(AEvent:TChartToolEvent); override;
    Procedure ChartMouseEvent( AEvent: TChartMouseEvent;
                               AButton:TMouseButton;
                               AShift: TShiftState; X, Y: Integer); override;
    Procedure ClipDrawingRegion; virtual;
    class function GetEditorClass: String; override;
  public
    FromPoint : TPoint;
    ToPoint   : TPoint;
    Constructor Create(AOwner:TComponent); override;
    Destructor Destroy; override;

    Function AxisPoint(Const P:TFloatPoint):TPoint;
    Function Clicked(X,Y:Integer):TDrawLine;
    Procedure DeleteSelected;
    Function ScreenPoint(P:TPoint):TFloatPoint;

    class Function Description:String; override;
    property Lines:TDrawLines read FLines;
    property Selected:TDrawLine read ISelected write SetSelected;
  published
    property Active;
    property Button:TMouseButton read FButton write FButton default mbLeft;
    property EnableDraw:Boolean read FEnableDraw write FEnableDraw default True;
    property EnableSelect:Boolean read FEnableSelect write SetEnableSelect default True;
    property Pen;
    property Series;
    { events }
    property OnDragLine:TNotifyEvent read FOnDragLine write FOnDragLine;
    property OnNewLine:TNotifyEvent read FOnNewLine write FOnNewLine;
    property OnSelect:TNotifyEvent read FOnSelect write FOnSelect;
  end;

  TMarkToolMouseAction = (mtmMove, mtmClick);

  TMarksTipTool=class;

  TMarksTipGetText=procedure(Sender:TMarksTipTool; Var Text:String) of object;

  TMarksTipTool=class(TTeeCustomToolSeries)
  private
    FMouseAction : TMarkToolMouseAction;
    FOnGetText   : TMarksTipGetText;
    FStyle       : TSeriesMarksStyle;
    Procedure SetMouseAction(Value:TMarkToolMouseAction);
  protected
    Function Chart:TCustomChart;
    Procedure ChartMouseEvent( AEvent: TChartMouseEvent;
                               Button:TMouseButton;
                               Shift: TShiftState; X, Y: Integer); override;
    class function GetEditorClass: String; override;
    Procedure SetActive(Value:Boolean); override;
    procedure SetSeries(const Value: TChartSeries); override;
  public
    Constructor Create(AOwner:TComponent); override;
    class Function Description:String; override;
  published
    property Active;
    property MouseAction:TMarkToolMouseAction read FMouseAction
                                               write SetMouseAction default mtmMove;
    property Series;
    property Style:TSeriesMarksStyle read FStyle write FStyle default smsLabel;

    property OnGetText:TMarksTipGetText read FOnGetText write FOnGetText;
  end;

  TNearestToolStyle=(hsNone,hsCircle,hsRectangle,hsDiamond);

  TNearestTool=class(TTeeCustomToolSeries)
  private
    FDrawLine   : Boolean;
    FFullRepaint: Boolean;
    FSize       : Integer;
    FStyle      : TNearestToolStyle;
    FOnChange   : TNotifyEvent;

    IMouse      : TPoint;
    procedure PaintHint;
    procedure SetSize(const Value: Integer);
    procedure SetStyle(const Value: TNearestToolStyle);
  protected
    procedure ChartEvent(AEvent: TChartToolEvent); override;
    Procedure ChartMouseEvent( AEvent: TChartMouseEvent;
                               Button:TMouseButton;
                               Shift: TShiftState; X, Y: Integer); override;
    class function GetEditorClass: String; override;
  public
    Point : Integer;
    Constructor Create(AOwner:TComponent); override;

    class Function Description:String; override;
  published
    property Active;
    property Brush;
    property DrawLine:Boolean read FDrawLine write FDrawLine default True;
    property FullRepaint:Boolean read FFullRepaint write FFullRepaint default True;
    property Pen;
    property Series;
    property Size:Integer read FSize write SetSize default 20;
    property Style:TNearestToolStyle read FStyle write SetStyle default hsCircle;
    property OnChange:TNotifyEvent read FOnChange write FOnChange;
  end;

  TColorBandTool=class(TTeeCustomToolAxis)
  private
    FColor        : TColor;
    FDrawBehind   : Boolean;
    FEnd          : Double;
    FGradient     : TChartGradient;
    FStart        : Double;
    FTransparency : TTeeTransparency;
    procedure PaintBand;
    Procedure SetColor(Value:TColor);
    procedure SetDrawBehind(const Value: Boolean);
    procedure SetEnd(const Value: Double);
    procedure SetGradient(const Value: TChartGradient);
    procedure SetStart(const Value: Double);
    procedure SetTransparency(const Value: TTeeTransparency);
  protected
    procedure ChartEvent(AEvent: TChartToolEvent); override;
    class function GetEditorClass: String; override;
  public
    Constructor Create(AOwner: TComponent); override;
    Destructor Destroy; override;

    class Function Description:String; override;
  published
    property Active;
    property Axis;
    property Brush;
    property Color:TColor read FColor write SetColor default clWhite;
    property DrawBehind:Boolean read FDrawBehind write SetDrawBehind default True;
    property EndValue:Double read FEnd write SetEnd;
    property Gradient:TChartGradient read FGradient write SetGradient;
    property Pen;
    property StartValue:Double read FStart write SetStart;
    property Transparency:TTeeTransparency read FTransparency
                                           write SetTransparency default 0;
  end;

  TColorLineTool=class;

  TColorLineToolOnDrag=procedure(Sender:TColorLineTool) of object;

  TColorLineTool=class(TTeeCustomToolAxis)
  private
    FAllowDrag     : Boolean;
    FOnDragLine    : TColorLineToolOnDrag;
    FOnEndDragLine : TColorLineToolOnDrag;
    FValue         : Double;

    IDragging : Boolean;
    procedure SetValue(const AValue: Double);
  protected
    procedure ChartEvent(AEvent: TChartToolEvent); override;
    Procedure ChartMouseEvent( AEvent: TChartMouseEvent;
                               Button:TMouseButton;
                               Shift: TShiftState; X, Y: Integer); override;
    class function GetEditorClass: String; override;
  public
    Constructor Create(AOwner:TComponent); override;

    Function Clicked(x,y:Integer):Boolean;
    class Function Description:String; override;
  published
    property Active;
    property Axis;
    property AllowDrag:Boolean read FAllowDrag write FAllowDrag default True;
    property Pen;
    property Value:Double read FValue write SetValue;

    property OnDragLine:TColorLineToolOnDrag read FOnDragLine write FOnDragLine;
    property OnEndDragLine:TColorLineToolOnDrag read FOnEndDragLine write FOnEndDragLine;
  end;

  TRotateTool=class(TTeeCustomTool)
  private
    FButton   : TMouseButton;

    IOldX     : Integer;
    IOldY     : Integer;
    IDragging : Boolean;
  protected
    Procedure ChartMouseEvent( AEvent: TChartMouseEvent;
                               AButton:TMouseButton;
                               AShift: TShiftState; X, Y: Integer); override;
  public
    Constructor Create(AOwner:TComponent); override;
    class Function Description:String; override;
  published
    property Active;
    property Button:TMouseButton read FButton write FButton default mbLeft;
  end;

  TChartImageTool=class(TTeeCustomToolSeries)
  private
    FPicture : TPicture;
    procedure SetPicture(const Value: TPicture);
  protected
    procedure ChartEvent(AEvent: TChartToolEvent); override;
    class function GetEditorClass: String; override;
    procedure SetSeries(const Value: TChartSeries); override;
  public
    Constructor Create(AOwner:TComponent); override;
    Destructor Destroy; override;
    class Function Description:String; override;
  published
    property Active;
    property Picture:TPicture read FPicture write SetPicture;
    property Series;
  end;

implementation

Uses Forms, TeeConst, TeeProco;

{ TTeeCustomToolSeries }
class function TTeeCustomToolSeries.GetEditorClass: String;
begin
  result:='TSeriesToolEditor';
end;

Function TTeeCustomToolSeries.GetHorizAxis:TChartAxis;
begin
  if Assigned(FSeries) then result:=FSeries.GetHorizAxis
                       else result:=ParentChart.BottomAxis;
end;

Function TTeeCustomToolSeries.GetVertAxis:TChartAxis;
begin
  if Assigned(FSeries) then result:=FSeries.GetVertAxis
                       else result:=ParentChart.LeftAxis;
end;

procedure TTeeCustomToolSeries.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if Operation=opRemove then
     if Assigned(FSeries) and (AComponent=FSeries) then
        Series:=nil;
end;

procedure TTeeCustomToolSeries.SetSeries(const Value: TChartSeries);
begin
  FSeries:=Value;
  if Assigned(FSeries) then FSeries.FreeNotification(Self);
end;

{ TCursorTool }
Constructor TCursorTool.Create(AOwner:TComponent);
begin
  inherited;
  FFollowMouse:=False;
  FSnap:=False;
  FStyle:=cssBoth;

  IDragging:=ccNone;
  IXValue:=0;
  IYValue:=0;
  IPoint.X:=-1;
  IPoint.Y:=-1;
end;

Procedure TCursorTool.CalcValuePositions(X,Y:Integer);
begin
  Case IDragging of
    ccVertical  : IXValue:=GetHorizAxis.CalcPosPoint(X);
    ccHorizontal: IYValue:=GetVertAxis.CalcPosPoint(Y);
  else
  begin
    IXValue:=GetHorizAxis.CalcPosPoint(X);
    IYValue:=GetVertAxis.CalcPosPoint(Y);
  end;
  end;
end;

Procedure TCursorTool.CalcScreenPositions;
var tmpSnap : Integer;
begin
  if (IPoint.X=-1) or (IPoint.Y=-1) then
  begin
    With GetHorizAxis do IPoint.X:=(IStartPos+IEndPos) div 2;
    With GetVertAxis do IPoint.Y:=(IStartPos+IEndPos) div 2;
    CalcValuePositions(IPoint.X,IPoint.Y);
    tmpSnap:=SnapToPoint;
    CalcScreenPositions;
    Changed(tmpSnap);
  end
  else
  begin
    IPoint.X:=GetHorizAxis.CalcPosValue(IXValue);
    IPoint.Y:=GetVertAxis.CalcPosValue(IYValue);
  end;
end;

Procedure TCursorTool.Changed(SnapPoint:Integer);
begin
  if Assigned(FOnChange) then
     FOnChange(Self,IPoint.X,IPoint.Y,IXValue,IYValue,FSeries,SnapPoint);
end;

class Function TCursorTool.GetEditorClass:String;
begin
  result:='TCursorToolEditor';
end;

class Function TCursorTool.Description:String;
begin
  result:=TeeMsg_CursorTool;
end;

Function TCursorTool.NearestPoint(AStyle:TCursorToolStyle; Var Difference:Double):Integer;
var t      : Integer;
    tmpDif : Double;
    tmpMin : Integer;
    tmpMax : Integer;
begin
  result:=-1;
  Difference:=-1;
  with FSeries do
  if Count>0 then
  begin
    tmpMin:=FirstValueIndex;
    tmpMax:=LastValueIndex;
    if tmpMin<0 then tmpMin:=0;
    if tmpMax<0 then tmpMax:=Count-1;
    if (tmpMin<=Count) and (tmpMax<=Count) then
    for t:=tmpMin to tmpMax do
    begin
      Case AStyle of
        cssHorizontal: tmpDif:=Abs(IYValue-YValues.Value[t]);
        cssVertical  : tmpDif:=Abs(IXValue-XValues.Value[t]);
      else
        tmpDif:=Sqrt(Sqr(IXValue-XValues.Value[t])+Sqr(IYValue-YValues.Value[t]));
      end;
      if (Difference=-1) or (tmpDif<Difference) then
      begin
        Difference:=tmpDif;
        result:=t;
      end;
    end;
  end;
end;

Function TCursorTool.SnapToPoint:Integer;
var Difference : Double;
begin
  if Assigned(FSeries) and FSnap then result:=NearestPoint(FStyle,Difference)
                                 else result:=-1;
  if result<>-1 then
  Case FStyle of
    cssHorizontal: IYValue:=FSeries.YValues.Value[result];
    cssVertical  : IXValue:=FSeries.XValues.Value[result];
  else
    begin
      IXValue:=FSeries.XValues.Value[result];
      IYValue:=FSeries.YValues.Value[result]
    end;
  end;
end;

Function TCursorTool.GetAxisRect:TRect;
begin
  With GetHorizAxis do
  begin
    result.Left:=IStartPos;
    result.Right:=IEndPos;
  end;
  With GetVertAxis do
  begin
    result.Top:=IStartPos;
    result.Bottom:=IEndPos;
  end;
  if Assigned(FOnGetAxisRect) then FOnGetAxisRect(Self,result);
end;

procedure TCursorTool.RedrawCursor;

  Procedure DrawCursorLines(Draw3D:Boolean; Const R:TRect; X,Y:Integer);

    Procedure DrawHorizontal;
    begin
      With ParentChart.Canvas do
      if Draw3D then HorizLine3D(R.Left,R.Right,Y,0)
                else DoHorizLine(R.Left,R.Right,Y);
    end;

    Procedure DrawVertical;
    begin
      With ParentChart.Canvas do
      if Draw3D then VertLine3D(X,R.Top,R.Bottom,0)
                else DoVertLine(X,R.Top,R.Bottom);
    end;

  begin
    Case FStyle of
      cssHorizontal: DrawHorizontal;
      cssVertical  : DrawVertical;
    else
    begin
      DrawHorizontal;
      DrawVertical;
    end;
    end;
  end;

var tmpColor : TColor;
begin
  tmpColor:=ParentChart.Color;
  if tmpColor=clTeeColor then tmpColor:=clBtnFace;
  With ParentChart.Canvas do
  begin
    AssignVisiblePenColor(Self.Pen,Self.Pen.Color xor ColorToRGB(tmpColor));
    Brush.Style:=bsClear;
    Pen.Mode:=pmXor;
    DrawCursorLines(ParentChart.View3D,GetAxisRect,IPoint.X,IPoint.Y);
    Pen.Mode:=pmCopy;
    ParentChart.Canvas.Invalidate;
  end;
end;

Procedure TCursorTool.ChartMouseEvent( AEvent: TChartMouseEvent;
                                       Button:TMouseButton;
                                       Shift: TShiftState; X, Y: Integer);

  Procedure MouseMove;
  var tmpSnap : Integer;
      tmp     : TCursorClicked;
  begin
    { if mouse button is pressed and dragging then... }
    if ((IDragging<>ccNone) or FollowMouse) and
       {$IFDEF D6}Types.{$ENDIF}PtInRect(GetAxisRect,{$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(X,Y)) then
    begin
      RedrawCursor;      { hide the cursor }
      CalcValuePositions(X,Y);
      tmpSnap:=SnapToPoint;  { snap to the nearest point of SnapSeries }
      { draw again the cursor at the new position }
      CalcScreenPositions;
      RedrawCursor;
      Changed(tmpSnap);
    end
    else
    begin  { mouse button is not pressed, user is not dragging the cursor }
           { change the mouse cursor when passing over the Cursor }
      tmp:=Clicked(x,y);
      case tmp of
         ccHorizontal : ParentChart.Cursor:=crVSplit;
         ccVertical   : ParentChart.Cursor:=crHSplit;
         ccBoth       : ParentChart.Cursor:={$IFDEF D4}crSizeAll{$ELSE}crHandPoint{$ENDIF};
      else
        ParentChart.Cursor:=crDefault;
      end;
      ParentChart.OriginalCursor:=ParentChart.Cursor;
      ParentChart.CancelMouse:=tmp<>ccNone;
    end;
  end;

begin
  Case AEvent of
      cmeUp: IDragging:=ccNone;
    cmeMove: MouseMove;
    cmeDown: if not FFollowMouse then
             begin
               IDragging:=Clicked(x,y);
               if IDragging<>ccNone then ParentChart.CancelMouse:=True;
             end;
  end;
end;

Function TCursorTool.Clicked(x,y:Integer):TCursorClicked;
begin
  result:=ccNone;
  if {$IFDEF D6}Types.{$ENDIF}PtInRect(GetAxisRect,{$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(x,y)) then
  begin
    if (FStyle=cssBoth) and (Abs(Y-IPoint.Y)<TeeClickTolerance)
                        and (Abs(X-IPoint.X)<TeeClickTolerance) then
       result:=ccBoth
    else
    if ((FStyle=cssHorizontal) or (FStyle=cssBoth))
            and (Abs(Y-IPoint.Y)<TeeClickTolerance) then
       result:=ccHorizontal
    else
    if ((FStyle=cssVertical) or (FStyle=cssBoth))
            and (Abs(X-IPoint.X)<TeeClickTolerance) then
       result:=ccVertical
  end;
end;

Procedure TCursorTool.SetStyle(Value:TCursorToolStyle);
begin
  if FStyle<>Value then
  begin
    FStyle:=Value;
    SnapToPoint;
    Repaint;
  end;
end;

procedure TCursorTool.SetSeries(const Value: TChartSeries);
begin
  inherited;
  if Assigned(Series) and (not (csLoading in ComponentState)) then
  begin
    if Active then RedrawCursor;
    SnapToPoint;
    CalcScreenPositions;
    if Active then RedrawCursor;
  end;
end;

procedure TCursorTool.ChartEvent(AEvent: TChartToolEvent);
begin
  inherited;
  if AEvent=cteAfterDraw then
  begin
    CalcScreenPositions;
    RedrawCursor;
  end;
end;

{ TDragMarksTool }
class function TDragMarksTool.Description: String;
begin
  result:=TeeMsg_DragMarksTool;
end;

Procedure TDragMarksTool.ChartMouseEvent( AEvent: TChartMouseEvent;
                                       Button:TMouseButton;
                                       Shift: TShiftState; X, Y: Integer);

  Procedure MouseMove;

    Procedure CheckCursor;

      Function CheckCursorSeries(ASeries:TChartSeries):Boolean;
      begin
        result:=ASeries.Active and ASeries.Marks.Visible and
                (ASeries.Marks.Clicked(x,y)<>-1);
      end;

    var tmp : Boolean;
        t   : Integer;
    begin
      tmp:=False;
      if Assigned(FSeries) then tmp:=CheckCursorSeries(FSeries)
      else
      With ParentChart do
      for t:=SeriesCount-1 downto 0 do
      begin
        tmp:=CheckCursorSeries(Series[t]);
        if tmp then break;
      end;
      if tmp then
      begin
        ParentChart.Cursor:=crHandPoint;
        ParentChart.CancelMouse:=True;
      end
      else ParentChart.Cursor:=crDefault;
    end;

  var DifX : Integer;
      DifY : Integer;
  begin
    if not Assigned(IPosition) then CheckCursor
    else
    With IPosition do
    begin
      DifX:=X-IOldX;
      DifY:=Y-IOldY;
      Custom:=True;
      Inc(LeftTop.X,DifX);
      Inc(LeftTop.Y,DifY);
      Inc(ArrowTo.X,DifX);
      Inc(ArrowTo.Y,DifY);
      IOldX:=X;
      IOldY:=Y;
      ParentChart.CancelMouse:=True;
      Repaint;
    end;
  end;

  Procedure MouseDown;

    Function CheckSeries(ASeries:TChartSeries):Integer;
    begin
      result:=-1;
      if ASeries.Active then
      begin
        result:=ASeries.Marks.Clicked(x,y);
        if result<>-1 then
        begin
          ISeries:=ASeries;
          IPosition:=ISeries.Marks.Positions.Position[result];
          Exit;
        end;
      end;
    end;

  var t : Integer;
  begin
    if Assigned(FSeries) then CheckSeries(FSeries)
    else
    With ParentChart do
    for t:=SeriesCount-1 downto 0 do
        if CheckSeries(Series[t])<>-1 then break;
    if Assigned(IPosition) then
    begin
      IOldX:=X;
      IOldY:=Y;
    end;
  end;

begin
  Case AEvent of
    cmeUp  : IPosition:=nil;
    cmeDown: begin
               MouseDown;
               if Assigned(IPosition) then ParentChart.CancelMouse:=True;
             end;
    cmeMove: MouseMove;
  end;
end;

class function TDragMarksTool.GetEditorClass: String;
begin
  result:='TDragMarksToolEditor';
end;

{ TTeeCustomToolAxis }
procedure TTeeCustomToolAxis.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('AxisID',ReadAxis,WriteAxis,Assigned(Axis));
end;

class function TTeeCustomToolAxis.GetEditorClass: String;
begin
  result:='TAxisToolEditor';
end;

procedure TTeeCustomToolAxis.ReadAxis(Reader: TReader);
begin
  Axis:=ParentChart.Axes[Reader.ReadInteger];
end;

procedure TTeeCustomToolAxis.SetAxis(const Value: TChartAxis);
begin
  FAxis:=Value;
  Repaint;
end;

procedure TTeeCustomToolAxis.WriteAxis(Writer: TWriter);
begin
  Writer.WriteInteger(ParentChart.Axes.IndexOf(Axis));
end;

{ TAxisArrowTool }
Constructor TAxisArrowTool.Create(AOwner: TComponent);
begin
  inherited;
  FLength:=16;
  FPosition:=aaBoth;
  FScrollPercent:=10;
end;

procedure TAxisArrowTool.ChartEvent(AEvent: TChartToolEvent);
Var tmpZ : Integer;

  Procedure DrawArrow(APos,ALength:Integer);
  var P0 : TPoint;
      P1 : TPoint;
  begin
    With Axis do
    if Horizontal then
    begin
      P0:={$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(APos+ALength,PosAxis);
      P1:={$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(APos,PosAxis)
    end
    else
    begin
      P0:={$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(PosAxis,APos+ALength);
      P1:={$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(PosAxis,APos);
    end;
    ParentChart.Canvas.Arrow(True,P0,P1,8,8,tmpZ);
  end;

begin
  inherited;
  if (AEvent=cteAfterDraw) and Assigned(Axis) then
  begin
    ParentChart.Canvas.AssignBrush(Self.Brush,Self.Brush.Color);
    ParentChart.Canvas.AssignVisiblePen(Self.Pen);
    if ParentChart.View3D and Axis.OtherSide then
       tmpZ:=ParentChart.Width3D
    else
       tmpZ:=0;
    if (FPosition=aaStart) or (FPosition=aaBoth) then
       DrawArrow(Axis.IStartPos,Length);
    if (FPosition=aaEnd) or (FPosition=aaBoth) then
       DrawArrow(Axis.IEndPos,-Length);
  end;
end;

class function TAxisArrowTool.Description: String;
begin
  result:=TeeMsg_AxisArrowTool;
end;

procedure TAxisArrowTool.SetLength(const Value: Integer);
begin
  SetIntegerProperty(FLength,Value);
end;

Function TAxisArrowTool.ClickedArrow(x,y:Integer):Integer;

  Procedure Check(Pos1,Pos2:Integer);
  begin
    With Axis do
    if (Abs(Pos1-PosAxis)<TeeClickTolerance) then
    begin
      if (FPosition=aaStart) or (FPosition=aaBoth) then
        if (Pos2>IStartPos) and (Pos2<IStartPos+Length) then
        begin
          result:=0;
          exit;
        end;
      if (FPosition=aaEnd) or (FPosition=aaBoth) then
        if (Pos2<IEndPos) and (Pos2>IEndPos-Length) then
        begin
          result:=1;
          exit;
        end;
    end;
  end;

begin
  result:=-1;
  if Axis.Horizontal then Check(y,x) else Check(x,y);
end;

Procedure TAxisArrowTool.ChartMouseEvent( AEvent: TChartMouseEvent;
                                       Button:TMouseButton;
                                       Shift: TShiftState; X, Y: Integer);
var tmp : Integer;
    Delta : Double;
begin
  if Assigned(Axis) and Axis.Visible then
  Case AEvent of
    cmeDown: if ScrollPercent<>0 then
             With Axis do
             begin
               tmp:=ClickedArrow(x,y);
               Delta:=(Maximum-Minimum)/ScrollPercent;
               if tmp=0 then Scroll(Delta,False)
               else
               if tmp=1 then Scroll(-Delta,False);
               if (tmp=0) or (tmp=1) then ParentChart.CancelMouse:=True;
             end;
    cmeMove: begin
               if ClickedArrow(x,y)<>-1 then ParentChart.Cursor:=crHandPoint;
               ParentChart.OriginalCursor:=ParentChart.Cursor;
             end;
  end;
end;

class function TAxisArrowTool.GetEditorClass: String;
begin
  result:='TAxisArrowToolEditor';
end;

procedure TAxisArrowTool.SetPosition(const Value: TAxisArrowToolPosition);
begin
  if FPosition<>Value then
  begin
    FPosition:=Value;
    Repaint;
  end;
end;

{ TDrawLine }
Function TDrawLine.StartHandle:TRect;
begin
  With Parent.AxisPoint(StartPos) do result:=Rect(X-3,Y-3,X+3,Y+3);
end;

Function TDrawLine.EndHandle:TRect;
begin
  With Parent.AxisPoint(EndPos) do result:=Rect(X-3,Y-3,X+3,Y+3);
end;

Procedure TDrawLine.DrawHandles;
begin
  With Parent.ParentChart.Canvas do
  begin
    Brush.Style:=bsSolid;
    if Parent.ParentChart.Color=clBlack then Brush.Color:=clSilver
                                        else Brush.Color:=clBlack;
    Pen.Style:=psClear;
    RectangleWithZ(StartHandle,0);
    RectangleWithZ(EndHandle,0);
  end;
end;

{ TDrawLines }
{$IFNDEF D4}
Destructor TDrawLines.Destroy;
{$ELSE}
procedure TDrawLines.Clear;
{$ENDIF}
var t : Integer;
begin
  IParent.ISelected:=nil;
  for t:=0 to Count-1 do Line[t].Free;
  inherited;
end;

function TDrawLines.Get(Index: Integer): TDrawLine;
begin
  result:=TDrawLine(inherited Items[Index]);
end;

{ TDrawLineTool }
Constructor TDrawLineTool.Create(AOwner: TComponent);
begin
  inherited;
  FButton:=mbLeft;
  FLines:=TDrawLines.Create;
  FLines.IParent:=Self;
  FEnableDraw:=True;
  FEnableSelect:=True;
  ISelected:=nil;
  IHandle:=chNone;
end;

Destructor TDrawLineTool.Destroy;
begin
  ISelected:=nil;
  FLines.Free;
  inherited;
end;

Procedure TDrawLineTool.DrawLine(Const StartPos,EndPos:TPoint);
begin
  With ParentChart.Canvas do
  if ParentChart.View3D then
  begin
    MoveTo3D(StartPos.X,StartPos.Y,0);
    LineTo3D(EndPos.X,EndPos.Y,0);
  end
  else Line(StartPos.X,StartPos.Y,EndPos.X,EndPos.Y);
end;

procedure TDrawLineTool.ChartEvent(AEvent: TChartToolEvent);
var t : Integer;
begin
  inherited;
  if (AEvent=cteAfterDraw) and (Lines.Count>0) then
  begin
    with ParentChart do
    begin
      Canvas.BackMode:=cbmTransparent;
      ClipDrawingRegion;
      Canvas.AssignVisiblePen(Self.Pen);
    end;
    for t:=0 to Lines.Count-1 do
    With Lines[t] do
         DrawLine(AxisPoint(StartPos),AxisPoint(EndPos));
    if Assigned(ISelected) then ISelected.DrawHandles;
    ParentChart.Canvas.UnClipRectangle;
  end;
end;

Procedure TDrawLineTool.ClipDrawingRegion;
var R : TRect;
begin
  if Assigned(Series) then
  With Series do
     R:=Rect(GetHorizAxis.IStartPos,GetVertAxis.IStartPos,
             GetHorizAxis.IEndPos,GetVertAxis.IEndPos)
  else
     R:=ParentChart.ChartRect;
  With ParentChart do
  if CanClip then Canvas.ClipCube(R,0,Width3D);
end;

Function TDrawLineTool.InternalClicked(X,Y:Integer; AHandle:TDrawLineHandle):TDrawLine;
var P : TPoint;

  Function ClickedLine(ALine:TDrawLine):Boolean;
  var tmpStart : TPoint;
      tmpEnd   : TPoint;
  begin
    With ALine do
    begin
      tmpStart:=AxisPoint(StartPos);
      tmpEnd:=AxisPoint(EndPos);
      Case AHandle of
        chStart: result:={$IFDEF D6}Types.{$ENDIF}PtInRect(StartHandle,P);
        chEnd  : result:={$IFDEF D6}Types.{$ENDIF}PtInRect(EndHandle,P);
      else
        result:=PointInLineTolerance(P,tmpStart.X,tmpStart.Y,tmpEnd.X,tmpEnd.Y,4);
      end;
    end;
  end;

var t : Integer;
begin
  result:=nil;
  ParentChart.Canvas.Calculate2DPosition(X,Y,0);
  P:={$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(X,Y);
  if Assigned(ISelected) and ClickedLine(ISelected) then
  begin
    result:=ISelected;
    exit;
  end;

  for t:=0 to Lines.Count-1 do
      if ClickedLine(Lines[t]) then
      begin
        result:=Lines[t];
        break;
      end;
end;

Function TDrawLineTool.Clicked(X,Y:Integer):TDrawLine;
begin
  result:=InternalClicked(X,Y,chNone);
  if not Assigned(result) then result:=InternalClicked(X,Y,chStart);
  if not Assigned(result) then result:=InternalClicked(X,Y,chEnd);
end;

Procedure TDrawLineTool.ChartMouseEvent( AEvent: TChartMouseEvent;
                                       AButton:TMouseButton;
                                       AShift: TShiftState; X, Y: Integer);

  Procedure CheckCursor;
  begin
    if Assigned(ISelected) and
       ((InternalClicked(X,Y,chStart)=ISelected) or
        (InternalClicked(X,Y,chEnd)=ISelected)) then
          ParentChart.Cursor:=crCross
    else
    if Assigned(Clicked(X,Y)) then ParentChart.Cursor:=crHandPoint
                              else ParentChart.Cursor:=crDefault;
    ParentChart.OriginalCursor:=ParentChart.Cursor;
  end;

var tmpLine : TDrawLine;
begin
  Case AEvent of
    cmeDown: if AButton=FButton then
             begin
               if FEnableSelect then tmpLine:=Clicked(X,Y)
                                else tmpLine:=nil;
               if Assigned(tmpLine) then
               begin
                 FromPoint:=AxisPoint(tmpLine.StartPos);
                 ToPoint:=AxisPoint(tmpLine.EndPos);
                 IHandle:=chSeries;
                 IPoint:={$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(X,Y);
                 if tmpLine<>ISelected then
                 begin
                   if Assigned(ISelected) then
                   begin
                     ISelected:=tmpLine;
                     Repaint;
                   end
                   else
                   begin
                     ISelected:=tmpLine;
                     ISelected.DrawHandles;
                   end;
                   if Assigned(FOnSelect) then FOnSelect(Self);
                 end
                 else
                 begin
                   if Assigned(InternalClicked(X,Y,chStart)) then
                      IHandle:=chStart
                   else
                   if Assigned(InternalClicked(X,Y,chEnd)) then
                      IHandle:=chEnd;
                 end;
                 ParentChart.CancelMouse:=True;
               end
               else
               begin
                 ISelected:=nil;
                 if EnableDraw then
                 begin
                   IDrawing:=True;
                   FromPoint:={$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(X,Y);
                   ToPoint:=FromPoint;
                   RedrawLine;
                   ParentChart.CancelMouse:=True;
                 end;
               end;
             end;
    cmeMove: if IDrawing or (IHandle<>chNone) then
             begin
               RedrawLine;
               if IDrawing then ToPoint:={$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(X,Y)
               else
               begin
                 if IHandle=chStart then FromPoint:={$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(X,Y)
                 else
                 if IHandle=chEnd then ToPoint:={$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(X,Y)
                 else
                 if IHandle=chSeries then
                 begin
                   Inc(FromPoint.X,X-IPoint.X);
                   Inc(FromPoint.Y,Y-IPoint.Y);
                   Inc(ToPoint.X,X-IPoint.X);
                   Inc(ToPoint.Y,Y-IPoint.Y);
                   IPoint:={$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(X,Y);
                 end;
               end;
               RedrawLine;
               ParentChart.CancelMouse:=True;
               if Assigned(FOnDragLine) then FOnDragLine(Self);
             end
             else
             if FEnableSelect then CheckCursor;
      cmeUp: if AButton=FButton then
             begin
               if IHandle<>chNone then
               begin
                 if (IHandle=chStart) or (IHandle=chSeries) then
                    ISelected.StartPos:=ScreenPoint(FromPoint);
                 if (IHandle=chEnd) or (IHandle=chSeries) then
                    ISelected.EndPos:=ScreenPoint(ToPoint);
                 IHandle:=chNone;
                 Repaint;
               end
               else
               if IDrawing then
               begin
                 if (FromPoint.X<>ToPoint.X) or (FromPoint.Y<>ToPoint.Y) then
                 begin
                   tmpLine:=TDrawLine.Create;
                   with tmpLine do
                   begin
                     Parent:=Self;
                     StartPos:=ScreenPoint(FromPoint);
                     EndPos  :=ScreenPoint(ToPoint);
                   end;
                   Lines.Add(tmpLine);
                   Repaint;
                   if Assigned(FOnNewLine) then FOnNewLine(Self);
                 end;
                 IDrawing:=False;
               end;
             end;
  end;
end;

class function TDrawLineTool.Description: String;
begin
  result:=TeeMsg_DrawLineTool
end;

type TCustomTeePanelAccess=class(TCustomTeePanel);

procedure TDrawLineTool.RedrawLine;
var tmp : TColor;
begin
  With ParentChart.Canvas do
  begin
    tmp:=ColorToRGB(TCustomTeePanelAccess(ParentChart).GetBackColor);
    AssignVisiblePenColor(Self.Pen,(clWhite-tmp) xor Self.Pen.Color);
    Pen.Mode:=pmNotXor;
    DrawLine(FromPoint,ToPoint);
    Pen.Mode:=pmCopy;
  end;
end;

class function TDrawLineTool.GetEditorClass: String;
begin
  result:='TDrawLineEdit';
end;

function TDrawLineTool.AxisPoint(const P: TFloatPoint): TPoint;
begin
  result.X:=GetHorizAxis.CalcPosValue(P.X);
  result.Y:=GetVertAxis.CalcPosValue(P.Y);
end;

function TDrawLineTool.ScreenPoint(P: TPoint): TFloatPoint;
begin
  result.X:=GetHorizAxis.CalcPosPoint(P.X);
  result.Y:=GetVertAxis.CalcPosPoint(P.Y);
end;

procedure TDrawLineTool.SetEnableSelect(Value: Boolean);
begin
  if FEnableSelect<>Value then
  begin
    FEnableSelect:=Value;
    if not FEnableSelect then
    begin
      if Assigned(ISelected) then
      begin
        ISelected:=nil;
        Repaint;
      end;  
    end;
  end;
end;

procedure TDrawLineTool.DeleteSelected;
begin
  if Assigned(ISelected) then
  begin
    FLines.Remove(ISelected);
    FreeAndNil(ISelected);
    Repaint;
  end;
end;

procedure TDrawLineTool.SetSelected(Value: TDrawLine);
begin
  ISelected:=Value;
  Repaint;
end;

{ TNearestTool }
Constructor TNearestTool.Create(AOwner: TComponent);
begin
  inherited;
  Point:=-1;
  FullRepaint:=True;
  Brush.Style:=bsClear;
  Pen.Style:=psDot;
  Pen.Color:=clWhite;
  FSize:=20;
  FDrawLine:=True;
  FStyle:=hsCircle;
end;

procedure TNearestTool.PaintHint;
var x : Integer;
    y : Integer;
    R : TRect;
    P : TFourPoints;
begin
  if Assigned(Series) and (Point<>-1) then
  With ParentChart.Canvas do
  begin
    AssignVisiblePen(Self.Pen);
    if not FullRepaint then Pen.Mode:=pmNotXor;

    x:=Series.CalcXPos(Point);
    y:=Series.CalcYPos(Point);

    if Self.Style<>hsNone then
    begin
      AssignBrushColor(Self.Brush,clBlack,Self.Brush.Color);
      Case Self.Style of
        hsCircle: if ParentChart.View3D then
                     EllipseWithZ(x-FSize,y-FSize,x+FSize,y+FSize,Series.StartZ)
                  else
                     Ellipse(x-FSize,y-FSize,x+FSize,y+FSize);
     hsRectangle: begin
                    R:=Rect(x-FSize,y-FSize,x+FSize,y+FSize);
                    if ParentChart.View3D then RectangleWithZ(R,Series.StartZ)
                                          else DoRectangle(R);
                  end;
       hsDiamond: begin
                    P[0]:={$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(x,y-FSize);
                    P[1]:={$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(x+FSize,y);
                    P[2]:={$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(x,y+FSize);
                    P[3]:={$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(x-FSize,y);
                    PolygonWithZ(P,Series.StartZ);
                  end;
      end;
    end;

    if FDrawLine then
    begin
      Pen.Style:=psSolid;
      MoveTo(IMouse.X,IMouse.Y);
      LineTo(x,y);
    end;
    if not FullRepaint then Pen.Mode:=pmCopy;
  end;
end;

procedure TNearestTool.ChartMouseEvent(AEvent: TChartMouseEvent;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

  Function GetNearestPoint:Integer;
  var t    : Integer;
      Dif  : Integer;
      Dist : Integer;
  begin
    result:=-1;
    Dif:=10000;
    for t:=0 to Series.Count-1 do   { <-- traverse all points in a Series... }
    begin
      { calculate distance in pixels... }
      Dist:=Round(Sqrt(Sqr(X-Series.CalcXPos(t))+Sqr(Y-Series.CalcYPos(t))));

      if Dist<Dif then { store if distance is lower... }
      begin
        Dif:=Dist;
        result:=t;  { <-- set this point to be the nearest... }
      end;
    end;
  end;

begin
  inherited;
  if (AEvent=cmeMove) and Assigned(Series) then
  begin
    if not FullRepaint then PaintHint;
    Point:=GetNearestPoint;
    IMouse:={$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(x,y);
    if not FullRepaint then PaintHint;
    if Assigned(FOnChange) then FOnChange(Self);
    if FullRepaint then Repaint;
  end;
end;

procedure TNearestTool.ChartEvent(AEvent: TChartToolEvent);
begin
  inherited;
  if AEvent=cteAfterDraw then PaintHint;
end;

class Function TNearestTool.Description:String;
begin
  result:=TeeMsg_NearestTool
end;

procedure TNearestTool.SetSize(const Value: Integer);
begin
  SetIntegerProperty(FSize,Value);
end;

class function TNearestTool.GetEditorClass: String;
begin
  result:='TNearestToolEdit';
end;

procedure TNearestTool.SetStyle(const Value: TNearestToolStyle);
begin
  if FStyle<>Value then
  begin
    FStyle:=Value;
    Repaint;
  end;
end;

{ TColorBandTool }
Constructor TColorBandTool.Create(AOwner: TComponent);
begin
  inherited;
  FGradient:=TChartGradient.Create(CanvasChanged);
  FColor:=clWhite;
  FDrawBehind:=True;
end;

destructor TColorBandTool.Destroy;
begin
  FGradient.Free;
  inherited;
end;

procedure TColorBandTool.PaintBand;
var R    : TRect;
    tmpR : TRect;
    tmp0 : Double;
    tmp1 : Double;
    tmpDraw : Boolean;
    tmpBlend : TTeeBlend;
begin
  if Assigned(Axis) then
  begin
    R:=ParentChart.ChartRect;
    tmp0:=FStart;
    tmp1:=FEnd;

    With Axis do
    begin
      if Inverted then
      begin
        if tmp0<tmp1 then SwapDouble(tmp0,tmp1);
        tmpDraw:=(tmp1<=Maximum) and (tmp0>=Minimum);
      end
      else
      begin
        if tmp0>tmp1 then SwapDouble(tmp0,tmp1);
        tmpDraw:=(tmp0<=Maximum) and (tmp1>=Minimum);
      end;
      if tmpDraw then
      begin
        if Horizontal then
        begin
          R.Left:=MaxLong(IStartPos,CalcPosValue(tmp0));
          R.Right:=MinLong(IEndPos,CalcPosValue(tmp1));
          if not Self.Pen.Visible then Inc(R.Right);
        end
        else
        begin
          R.Top:=MaxLong(IStartPos,CalcPosValue(tmp1));
          R.Bottom:=MinLong(IEndPos,CalcPosValue(tmp0));
          Inc(R.Left);
          if not Self.Pen.Visible then
          begin
            Inc(R.Bottom);
            Inc(R.Right);
          end;
        end;
        With ParentChart,Canvas do
        begin
          AssignBrushColor(Self.Brush,Self.Color,Self.Brush.Color);
          AssignVisiblePen(Self.Pen);

          if Self.Gradient.Visible and View3DOptions.Orthogonal then
          begin
            tmpR:=R;
            Dec(tmpR.Right);
            Dec(tmpR.Bottom);
            Self.Gradient.Draw(Canvas,CalcRect3D(tmpR,Width3D),0);
            Brush.Style:=bsClear;
          end;

          if Transparency=0 then
          begin
             if View3D then RectangleWithZ(R,Width3D)
                       else DoRectangle(R);
          end
          else
          begin
            tmpBlend:=TTeeBlend.Create(TTeeCanvas3D(ParentChart.Canvas),R);
            try
              if View3D then RectangleWithZ(R,Width3D)
                        else DoRectangle(R);
              tmpBlend.DoBlend(Transparency);
            finally
              tmpBlend.Free;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TColorBandTool.ChartEvent(AEvent: TChartToolEvent);
begin
  inherited;
  if ((AEvent=cteBeforeDrawSeries) and DrawBehind) or
     ((AEvent=cteAfterDraw) and (not DrawBehind)) then PaintBand;
end;

class function TColorBandTool.Description: String;
begin
  result:=TeeMsg_ColorBandTool
end;

class function TColorBandTool.GetEditorClass: String;
begin
  result:='TColorBandToolEditor';
end;

procedure TColorBandTool.SetEnd(const Value: Double);
begin
  SetDoubleProperty(FEnd,Value);
end;

procedure TColorBandTool.SetStart(const Value: Double);
begin
  SetDoubleProperty(FStart,Value);
end;

procedure TColorBandTool.SetGradient(const Value: TChartGradient);
begin
  FGradient.Assign(Value);
end;

procedure TColorBandTool.SetColor(Value: TColor);
begin
  SetColorProperty(FColor,Value);
end;

procedure TColorBandTool.SetTransparency(const Value: TTeeTransparency);
begin
  if FTransparency<>Value then
  begin
    FTransparency:=Value;
    Repaint;
  end;
end;

procedure TColorBandTool.SetDrawBehind(const Value: Boolean);
begin
  SetBooleanProperty(FDrawBehind,Value);
end;

{ TColorLineTool }
constructor TColorLineTool.Create(AOwner: TComponent);
begin
  inherited;
  FAllowDrag:=True;
end;

procedure TColorLineTool.ChartEvent(AEvent: TChartToolEvent);
var tmp : Integer;
begin
  inherited;
  if Assigned(Axis) and
     ((AEvent=cteBeforeDrawSeries) or (AEvent=cteAfterDraw)) then
  With ParentChart,Canvas do
  begin
    AssignVisiblePen(Self.Pen);
    tmp:=Axis.CalcPosValue(FValue);
    if AEvent=cteBeforeDrawSeries then
    begin
      if Axis.Horizontal then
      begin
        ZLine3D(tmp,ChartRect.Bottom,0,Width3D);
        VertLine3D(tmp,ChartRect.Top,ChartRect.Bottom,Width3D);
      end
      else
      begin
        ZLine3D(ChartRect.Left,tmp,0,Width3D);
        HorizLine3D(ChartRect.Left,ChartRect.Right,tmp,Width3D);
      end;
    end
    else
    if View3D then
      if Axis.Horizontal then
      begin
        ZLine3D(tmp,ChartRect.Top,0,Width3D);
        VertLine3D(tmp,ChartRect.Top,ChartRect.Bottom,0);
      end
      else
      begin
        ZLine3D(ChartRect.Right,tmp,0,Width3D);
        HorizLine3D(ChartRect.Left,ChartRect.Right,tmp,0);
      end;
  end;
end;

procedure TColorLineTool.ChartMouseEvent(AEvent: TChartMouseEvent;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var tmp : Integer;
begin
  if AllowDrag and Assigned(Axis) then
  Case AEvent of
      cmeUp: begin
               IDragging:=False;
               if Assigned(FOnEndDragLine) then FOnEndDragLine(Self);
             end;
    cmeMove: if IDragging then
             begin
               if Axis.Horizontal then tmp:=x else tmp:=y;
               Value:=Axis.CalcPosPoint(tmp);
               ParentChart.CancelMouse:=True;
               if Assigned(FOnDragLine) then FOnDragLine(Self);
             end
             else
             begin
               if Clicked(x,y) then
               begin
                 if Axis.Horizontal then
                    ParentChart.Cursor:=crHSplit
                 else
                    ParentChart.Cursor:=crVSplit;
                 ParentChart.CancelMouse:=True;
               end
               else ParentChart.Cursor:=crDefault;
               ParentChart.OriginalCursor:=ParentChart.Cursor;
             end;
    cmeDown: begin
               IDragging:=Clicked(x,y);
               ParentChart.CancelMouse:=IDragging;
             end;
  end;
end;

function TColorLineTool.Clicked(x, y: Integer): Boolean;
var tmp : Integer;
begin
  if Axis.Horizontal then tmp:=x else tmp:=y;
  result:=Abs(tmp-Axis.CalcPosValue(FValue))<TeeClickTolerance;
end;

class function TColorLineTool.Description: String;
begin
  result:=TeeMsg_ColorLineTool;
end;

class function TColorLineTool.GetEditorClass: String;
begin
  result:='TColorLineToolEditor';
end;

procedure TColorLineTool.SetValue(const AValue: Double);
begin
  SetDoubleProperty(FValue,AValue);
end;


{ TRotateTool }
constructor TRotateTool.Create(AOwner: TComponent);
begin
  inherited;
  FButton:=mbLeft;
  IDragging:=False;
end;

procedure TRotateTool.ChartMouseEvent(AEvent: TChartMouseEvent;
  AButton: TMouseButton; AShift: TShiftState; X, Y: Integer);

  Procedure MouseMove;

      Function CorrectAngle(Const AAngle:Integer):Integer;
      begin
        result:=AAngle;
        if result>360 then result:=result-360 else
        if result<0 then result:=360+result;
      end;

      Function CalcAngleChange(AAngle,AChange:Integer):Integer;
      var tmpMinAngle : Integer;
      begin
        if AChange>0 then result:=MinLong(360,AAngle+AChange)
        else
        begin
          if ParentChart.Canvas.SupportsFullRotation then tmpMinAngle:=0
                                                     else tmpMinAngle:=TeeMinAngle;
          result:=MaxLong(tmpMinAngle,AAngle+AChange);
        end;
      end;

    Var tmpX : Integer;
        tmpY : Integer;
    begin
      With ParentChart,View3DOptions do
      begin
        View3D:=True;
        Orthogonal:=False;
        tmpX:=Round(90.0*(X-IOldX)/Width);
        tmpY:=Round(90.0*(IOldY-Y)/Height);
        if Canvas.SupportsFullRotation then
        begin
          Rotation:=CorrectAngle(Rotation+tmpX);
          Elevation:=CorrectAngle(Elevation+tmpY);
        end
        else
        begin
          Rotation:=CalcAngleChange(Rotation,tmpX);
          Elevation:=CalcAngleChange(Elevation,tmpY);
        end;
        IOldX:=X;
        IOldY:=Y;
        CancelMouse:=True;
      end;
    end;

begin
  inherited;
  Case AEvent of
      cmeUp: IDragging:=False;
    cmeMove: if IDragging then MouseMove;
    cmeDown: if AButton=Self.Button then
             begin
               IDragging:=True;
               IOldX:=X;
               IOldY:=Y;
               ParentChart.CancelMouse:=True;
             end;
  end;
end;

class function TRotateTool.Description: String;
begin
  result:=TeeMsg_RotateTool;
end;

{ TChartImageTool }
Constructor TChartImageTool.Create(AOwner: TComponent);
begin
  inherited;
  FPicture:=TPicture.Create;
  FPicture.OnChange:=CanvasChanged;
end;

Destructor TChartImageTool.Destroy;
begin
  FPicture.Free;
  inherited;
end;

procedure TChartImageTool.ChartEvent(AEvent: TChartToolEvent);
var R : TRect;
begin
  inherited;
  if (AEvent=cteBeforeDrawSeries) and Assigned(FPicture) then
  begin
    if Assigned(Series) then
    With Series do
    begin
      R.Left:=CalcXPosValue(MinXValue);
      R.Right:=CalcXPosValue(MaxXValue);
      R.Top:=CalcYPosValue(MaxYValue);
      R.Bottom:=CalcYPosValue(MinYValue);
    end
    else
    begin
      With GetHorizAxis do
      begin
        R.Left:=CalcPosValue(Minimum);
        R.Right:=CalcPosValue(Maximum);
      end;
      With GetVertAxis do
      begin
        R.Top:=CalcYPosValue(Maximum);
        R.Bottom:=CalcYPosValue(Minimum);
      end;
    end;
    With ParentChart,Canvas do
    begin
      if CanClip then ClipCube(ChartRect,0,Width3D);
      StretchDraw(CalcRect3D(R,0),FPicture.Graphic);
      UnClipRectangle;
    end;
  end;
end;

class function TChartImageTool.Description: String;
begin
  result:=TeeMsg_ImageTool;
end;

class function TChartImageTool.GetEditorClass: String;
begin
  result:='TChartImageToolEditor';
end;

procedure TChartImageTool.SetPicture(const Value: TPicture);
begin
  FPicture.Assign(Value);
end;

procedure TChartImageTool.SetSeries(const Value: TChartSeries);
begin
  inherited;
  Repaint;
end;

{ TMarksToolTip }
constructor TMarksTipTool.Create(AOwner: TComponent);
begin
  inherited;
  FStyle:=smsLabel;
end;

procedure TMarksTipTool.ChartMouseEvent(AEvent: TChartMouseEvent;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var tmp      : Integer;
    tmpStyle : TSeriesMarksStyle;
    tmpOld   : Boolean;
    tmpText  : String;
begin
  inherited;
  if Assigned(Series) and
    ( ((MouseAction=mtmMove) and (AEvent=cmeMove)) or
      ((MouseAction=mtmClick) and (AEvent=cmeDown))
    ) then
  begin
    tmp:=Series.Clicked(x,y);
    if tmp=-1 then Application.CancelHint
    else
    begin
      tmpStyle:=Series.Marks.Style;
      tmpOld:=Chart.AutoRepaint;
      Chart.AutoRepaint:=False;
      Series.Marks.Style:=FStyle;
      try
        tmpText:=Series.ValueMarkText[tmp];
        if Assigned(FOnGetText) then FOnGetText(Self,tmpText);
        if Chart.Hint<>tmpText then
        begin
          Chart.Hint:=tmpText;
          {$IFDEF D5}
          Application.ActivateHint(
             Series.ParentChart.ClientToScreen(
                {$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(X,Y)));
          {$ENDIF}
        end;
      finally
        Series.Marks.Style:=tmpStyle;
        Chart.AutoRepaint:=tmpOld;
      end;
    end;
  end;
end;

class function TMarksTipTool.Description: String;
begin
  result:=TeeMsg_MarksTipTool;
end;

class function TMarksTipTool.GetEditorClass: String;
begin
  result:='TMarksTipToolEdit';
end;

Function TMarksTipTool.Chart:TCustomChart;
begin
  if Assigned(Series) then result:=TCustomChart(Series.ParentChart) else result:=nil;
end;

procedure TMarksTipTool.SetActive(Value: Boolean);
begin
  inherited;
  if (not Active) and (Chart<>nil) then Chart.Hint:='';
end;

procedure TMarksTipTool.SetMouseAction(Value: TMarkToolMouseAction);
begin
  FMouseAction:=Value;
  if Chart<>nil then Chart.Hint:='';
end;

procedure TMarksTipTool.SetSeries(const Value: TChartSeries);
begin
  inherited;
  if Chart<>nil then Chart.ShowHint:=True;
end;

Const T:Array[0..9] of TTeeCustomToolClass=( TCursorTool,TDragMarksTool,
                     TAxisArrowTool,TDrawLineTool,TNearestTool,TColorBandTool,
                     TColorLineTool,TRotateTool,TChartImageTool,
                     TMarksTipTool);
initialization
  RegisterTeeTools(T);
finalization
  UnRegisterTeeTools(T);
end.
