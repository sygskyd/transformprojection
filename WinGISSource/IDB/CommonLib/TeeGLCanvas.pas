{******************************************}
{    TeeChart Pro OpenGL Canvas            }
{ Copyright (c) 1998-2000 by David Berneda }
{       All Rights Reserved                }
{******************************************}
{$I teedefs.inc}
unit TeeGLCanvas;

{$C-}  { Turn off assertions }

{$DEFINE TEENOCULL}
{.$DEFINE TEECUBE}

interface

uses Windows, Messages, SysUtils, Classes, Graphics, OpenGL2, Controls,
     TeCanvas;

type  GLMat=Array[0..3] of GLFloat;

Const TeeOpenGLFontExtrusion : Integer=0;
      TeeOpenGLFontName      : PChar='Arial';

      TeeMaterialDiffuse     : Double=1;
      TeeMaterialSpecular    : Double=1;

      TeeSphereSlices = 16;
      TeeSphereStacks = 16;

      TeeFontListRange  = 128-32+1;

      TeeFullLightModel :GLENum= GL_FALSE;
      TeeLightLocal     :GLENum= GL_FALSE;
      TeeColorPlanes    :GLENum= GL_FRONT_AND_BACK;

      TeeTextAngleY  : Integer=0;
      TeeTextAngleZ  : Integer=0;

      TeeDefaultLightSpot:Integer=180;

type
    TGLCanvas = class(TCanvas3D)
    private
      { Private declarations }
      FBackColor     : TColor;
      FBackMode      : TCanvasBackMode;

      FDepth         : Integer;
      FTextAlign     : Integer;

      FWidth         : Integer;
      FHeight        : Integer;
      FXCenter       : Integer;
      FYCenter       : Integer;

      FOnInit        : TNotifyEvent;

      { internal }
      FDC            : HDC;
      HRC            : HGLRC;
      FX             : Integer;
      FY             : Integer;
      FZ             : Integer;
      FIs3D          : Boolean;

      { fonts }
      TheFontHandle  : Integer;
      FontOffset     : Integer;
      IFontCreated   : Boolean;

      FUseBuffer     : Boolean;
      IDestCanvas    : TCanvas;

      FSavedError    : GLEnum;

      FQuadric       : PGLUQuadricObj;

//      FDirty         : Boolean;

      {$IFDEF TEECUBE}
      Procedure PlayCube(Solid:Boolean; X,Y,Z:Integer; AWidth,AHeight,ADepth:Integer);
      {$ENDIF}

      Function CalcZoom:Double;
      Procedure DeleteTextures;
      Procedure DestroyGLContext;
      Procedure DoProjection;
      Function FindTexture(ABitmap:TBitmap):GLUInt;
      Procedure InitMatrix;
      procedure SetBrushBitmap;
      Procedure SetColor(AColor:TColor);
      Procedure SetPen;
      Procedure TeeVertex2D(x,y:Integer);
      Procedure TeeVertex3D(x,y,z:Integer);
      Procedure TeeNormal(x,y,z:Integer);
      procedure InternalCylinder(Vertical:Boolean; Left,Top,Right,Bottom,
                          Z0,Z1:Integer; Dark3D:Boolean; ConePercent:Integer);
    protected
      { Protected declarations }
      GLComponent : TComponent;
      Procedure InitOpenGLFont;
      Procedure InitAmbientLight(AmbientLight:Integer);
      Procedure InitLight0(Const AColor:GLMat; Const X,Y,Z:Double);
      Procedure SetShininess(Const Value:Double);
    public
      FontOutlines   : Boolean;
      ShadeQuality   : Boolean;

      { Public declarations }
      Constructor Create;
      Destructor Destroy; override;

      Function CheckGLError:Boolean;
      Procedure DeleteFont;
      Procedure Repaint;

      { 2d }
      procedure SetPixel(X, Y: Integer; Value: TColor); override;
      Function GetSupports3DText:Boolean; override;
      Function GetSupportsFullRotation:Boolean; override;
      Function GetTextAlign:TCanvasTextAlign; override;
      Function GetUseBuffer:Boolean; override;
      Procedure SetUseBuffer(Value:Boolean); override;
      Function GetHandle:HDC; override;

      { 3d }
      Procedure EnableRotation;
      Procedure DisableRotation;
      Procedure SetMaterialColor;

      procedure SetPixel3D(X,Y,Z:Integer; Value: TColor); override;

      Procedure SetBackMode(Mode:TCanvasBackMode); override;
      Function GetMonochrome:Boolean; override;
      Procedure SetMonochrome(Value:Boolean); override;
      Procedure SetBackColor(Color:TColor); override;
      Function GetBackMode:TCanvasBackMode; override;
      Function GetBackColor:TColor; override;
      Procedure SetTextAlign(Align:TCanvasTextAlign); override;

      procedure Arc(X1, Y1, X2, Y2, X3, Y3, X4, Y4: Integer); override;
      procedure Donut( XCenter,YCenter,XRadius,YRadius:Integer;
                       Const StartAngle,EndAngle,HolePercent:Double); override;
      procedure Draw(X, Y: Integer; Graphic: TGraphic); override;
      procedure EraseBackground(const Rect: TRect); override;
      procedure FillRect(const Rect: TRect); override;
      procedure Frame3D( Rect: TRect; TopColor,BottomColor: TColor;
                         Width: Integer); override;
      procedure Ellipse(X1, Y1, X2, Y2: Integer); override;
      procedure LineTo(X,Y:Integer); override;
      procedure MoveTo(X,Y:Integer); override;
      procedure Pie(X1, Y1, X2, Y2, X3, Y3, X4, Y4: Integer); override;
      procedure Rectangle(X0,Y0,X1,Y1:Integer); override;
      procedure RoundRect(X1, Y1, X2, Y2, X3, Y3: Integer); override;
      procedure StretchDraw(const Rect: TRect; Graphic: TGraphic); override;
      Procedure TextOut(X,Y:Integer; const Text:String); override;
      Procedure DoRectangle(Const Rect:TRect); override;
      Procedure DoHorizLine(X0,X1,Y:Integer); override;
      Procedure DoVertLine(X,Y0,Y1:Integer); override;
      procedure ClipRectangle(Const Rect:TRect); override;
      procedure ClipCube(Const Rect:TRect; MinZ,MaxZ:Integer); override;
      procedure UnClipRectangle; override;
      Procedure GradientFill( Const Rect:TRect;
                              StartColor,EndColor:TColor;
                              Direction:TGradientDirection); override;
      procedure RotateLabel(x,y:Integer; Const St:String; RotDegree:Integer); override;
      procedure RotateLabel3D(x,y,z:Integer;
                              Const St:String; RotDegree:Integer); override;
      Procedure Invalidate; override;
      Procedure Line(X0,Y0,X1,Y1:Integer); override;
      Procedure Polygon(const Points: array of TPoint); override;
      { 3d }
      Procedure Calculate2DPosition(Var x,y:Integer; z:Integer); override;
      Function Calculate3DPosition(x,y,z:Integer):TPoint; override;
      Procedure Projection(MaxDepth:Integer; const Bounds,Rect:TRect); override;
      Function InitWindow( DestCanvas:TCanvas;
                           A3DOptions:TView3DOptions;
                           ABackColor:TColor;
                           Is3D:Boolean;
                           Const UserRect:TRect):TRect; override;
      Procedure ShowImage(DestCanvas,DefaultCanvas:TCanvas; Const UserRect:TRect); override;
      Function ReDrawBitmap:Boolean; override;
      Procedure Arrow( Filled:Boolean;
                       Const FromPoint,ToPoint:TPoint;
                       ArrowWidth,ArrowHeight,Z:Integer); override;
      Procedure Cube(Left,Right,Top,Bottom,Z0,Z1:Integer; DarkSides:Boolean); override;
      procedure Cylinder(Vertical:Boolean; Left,Top,Right,Bottom,Z0,Z1:Integer; DarkCover:Boolean); override;
      procedure EllipseWithZ(X1, Y1, X2, Y2, Z:Integer); override;
      Procedure RectangleZ(Left,Top,Bottom,Z0,Z1:Integer); override;
      Procedure RectangleY(Left,Top,Right,Z0,Z1:Integer); override;
      procedure FrontPlaneBegin; override;
      procedure FrontPlaneEnd; override;
      Procedure HorizLine3D(Left,Right,Y,Z:Integer); override;
      procedure LineTo3D(X,Y,Z:Integer); override;
      Procedure LineWithZ(X0,Y0,X1,Y1,Z:Integer); override;
      procedure MoveTo3D(X,Y,Z:Integer); override;
      procedure Pie3D( XCenter,YCenter,XRadius,YRadius,Z0,Z1:Integer;
                       Const StartAngle,EndAngle:Double;
                       DarkSides,DrawSides:Boolean); override;
      procedure Plane3D(Const A,B:TPoint; Z0,Z1:Integer); override;
      procedure PlaneWithZ(P1,P2,P3,P4:TPoint; Z:Integer); override;
      procedure PlaneFour3D(Var Points:TFourPoints; Z0,Z1:Integer); override;
      procedure PolygonWithZ(Points: array of TPoint; Z:Integer); override;
      procedure Pyramid(Vertical:Boolean; Left,Top,Right,Bottom,z0,z1:Integer; DarkSides:Boolean); override;
      Procedure PyramidTrunc(Const R:TRect; StartZ,EndZ:Integer;
                             TruncX,TruncZ:Integer); override;
      Procedure RectangleWithZ(Const Rect:TRect; Z:Integer); override;
      Procedure Surface3D( Style:TTeeCanvasSurfaceStyle;
                           SameBrush:Boolean;
                           NumXValues,NumZValues:Integer;
                           CalcPoints:TTeeCanvasCalcPoints ); override;
      Procedure TextOut3D(X,Y,Z:Integer; const Text:String); override;
      procedure Triangle3D(Const Points:TTrianglePoints3D; Const Colors:TTriangleColors3D); override;
      procedure TriangleWithZ(Const P1,P2,P3:TPoint; Z:Integer); override;
      Procedure VertLine3D(X,Top,Bottom,Z:Integer); override;
      Procedure ZLine3D(X,Y,Z0,Z1:Integer); override;

      procedure Sphere(x,y,z:Integer; Const Radius:Double); override;
      procedure Cone(Vertical:Boolean; Left,Top,Right,Bottom,Z0,Z1:Integer; Dark3D:Boolean; ConePercent:Integer); override;

      property OnInit:TNotifyEvent read FOnInit write FOnInit;
    published
    end;

Procedure ColorToGL(AColor:TColor; Var C:GLMat);

implementation

Const TeeZoomScale     = -80000;
      TeeSolidCubeList =   8888;
      TeeWireCubeList  = TeeSolidCubeList+1;

Function MinInteger(a,b:Integer):Integer;
begin
  if a>b then result:=b else result:=a;
end;

Procedure ColorToGL(AColor:TColor; Var C:GLMat);
begin
  AColor:=ColorToRGB(AColor);
  C[0]:=(1+GetRValue(AColor))/128.0-1.0;
  C[1]:=(1+GetGValue(AColor))/128.0-1.0;
  C[2]:=(1+GetBValue(AColor))/128.0-1.0;
  C[3]:=1;
end;

{ TGLCanvas }
Constructor TGLCanvas.Create;
begin
  inherited Create;
//  FDirty:=True;
  FSavedError:=GL_NO_ERROR;
  FUseBuffer:=False;
  IDestCanvas:=nil;
  IFontCreated:=False;
  FontOutlines:=False;
  FX:=0;
  FY:=0;
  FZ:=0;
  FIs3D:=False;
  HRC:=0;
  TheFontHandle:=-1;
  FTextAlign:=TA_LEFT;
  FQuadric:=nil;
end;

Procedure TGLCanvas.DestroyGLContext;
begin
  DeleteFont;
  if HRC<>0 then
  begin
    DeactivateRenderingContext;
    DestroyRenderingContext(HRC);
    HRC:=0;
//    Assert(CheckGLError,'DestroyGLContext');
  end;
end;

Destructor TGLCanvas.Destroy;
begin
  {$IFDEF TEECUBE}
  glDeleteLists(TeeSolidCubeList,1);
  glDeleteLists(TeeWireCubeList,1);
  {$ENDIF}

  if Assigned(FQuadric) then gluDeleteQuadric(FQuadric);
  DestroyGLContext;
  DeleteTextures;
  inherited;
end;

Function TGLCanvas.CheckGLError:Boolean;
begin
  FSavedError:=glGetError;
  result:=FSavedError=GL_NO_ERROR;
end;

Procedure TGLCanvas.Calculate2DPosition(Var x,y:Integer; z:Integer);
begin { nothing yet }
end;

Function TGLCanvas.Calculate3DPosition(x,y,z:Integer):TPoint;
begin
  result:=Point(x,y);
end;

Procedure TGLCanvas.DoProjection;
Var tmp:Double;
    tmp2:Integer;
    FarZ:Integer;
begin
  FarZ:=400*(FDepth+1);

  glViewport(0, 0, FWidth, FHeight);
  Assert(CheckGLError,'ViewPort'+IntToStr(FSavedError));

  glMatrixMode(GL_PROJECTION);
  Assert(CheckGLError,'Projection');
  glLoadIdentity;
  Assert(CheckGLError,'ProjectionInit');

  if (not FIs3D) or View3DOptions.Orthogonal then
  begin
    tmp:=100.0/CalcZoom;
    glOrtho(-FXCenter*tmp,FXCenter*tmp,-FYCenter*tmp,FYCenter*tmp,0.001,FarZ);
    Assert(CheckGLError,'Orthogonal');
  end
  else
  begin
    tmp2:=Round(100.0/CalcZoom);
    if tmp2<1 then tmp2:=1;
    gluPerspective(MaxDouble(10,View3DOptions.Perspective),    // Field-of-view angle
                   FWidth/FHeight,  // Aspect ratio of viewing volume
                   0.1,               // Distance to near clipping plane
                   tmp2*FarZ);  // Distance to far clipping plane
    Assert(CheckGLError,'Perspective');
  end;
end;

Procedure TGLCanvas.Projection(MaxDepth:Integer; const Bounds,Rect:TRect);
begin
  RectSize(Bounds,FWidth,FHeight);
  RectCenter(Rect,FXCenter,FYCenter);
  FDepth:=MaxDepth;
  DoProjection;
  InitMatrix;
end;

Function TGLCanvas.CalcZoom:Double;
var tmp:Integer;
begin
  result:=View3DOptions.Zoom;
  if result=0 then result:=1;
  if FIs3D and (not View3DOptions.Orthogonal) then
  begin
    tmp:=MaxLong(10,View3DOptions.Perspective);
    if tmp=0 then tmp:=1;
    result:=result*4*tmp/100.0;
  end;
end;

Procedure TGLCanvas.InitMatrix;
var B:GLMat;
    AColor:TColor;
begin
  AColor:=ColorToRGB(FBackColor);
  B[0]:=GetRValue(AColor)/255;
  B[1]:=GetGValue(AColor)/255;
  B[2]:=GetBValue(AColor)/255;
  B[3]:=1;
  glClearColor(B[0],B[1],B[2],1);
  Assert(CheckGLError,'ClearColor');
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  Assert(CheckGLError,'Clear');

{  With View3DOptions do
  begin
    glTranslatef(HorizOffset,-VertOffset,TeeZoomScale/CalcZoom);
    Assert(CheckGLError,'TranslateInit');
  end;
}

  glMatrixMode(GL_MODELVIEW);
  Assert(CheckGLError,'ModelView');
  glLoadIdentity;
  Assert(CheckGLError,'ModelInit');

  With View3DOptions do glTranslatef(HorizOffset,-VertOffset,TeeZoomScale/CalcZoom);

  if ShadeQuality then glShadeModel(GL_SMOOTH)
                  else glShadeModel(GL_FLAT);

  Assert(CheckGLError,'ShadeModel');

  if Assigned(FOnInit) then FOnInit(Self);

  Assert(CheckGLError,'Init');

// glGetIntegerv(GL_MAX_VIEWPORT_DIMS,@p);
// showmessagefmt( 'view: %d %d',[p[0],p[1]]);

  With View3DOptions do
  if FIs3D then
  begin
    glRotatef(Tilt, 0, 0, 1);
    if not Orthogonal then glRotatef(-Elevation, 1, 0, 0);
    glRotatef(Rotation, 0, 1, 0);
  end;
  glTranslatef( -FXCenter+RotationCenter.X,
                FYCenter+RotationCenter.Y,
                RotationCenter.Z+(FDepth/2.0));

  Assert(CheckGLError,'Rotations');

  if not Assigned(FQuadric) then FQuadric:=gluNewQuadric;
  if ShadeQuality then gluQuadricNormals(FQuadric,GL_SMOOTH)
                  else gluQuadricNormals(FQuadric,GL_FLAT);
  Assert(CheckGLError,'QuadricNormals');
end;

Procedure TGLCanvas.TeeVertex2D(x,y:Integer);
begin
  glVertex2i(x,-y);
end;

Procedure TGLCanvas.TeeVertex3D(x,y,z:Integer);
begin
  glVertex3i(x,-y,-z);
end;

Procedure TGLCanvas.TeeNormal(x,y,z:Integer);
begin
  glNormal3i(x,y,-z);
end;

{$IFDEF TEECUBE}
Procedure TGLCanvas.PlayCube(Solid:Boolean; X,Y,Z:Integer; AWidth,AHeight,ADepth:Integer);
begin
  glPushMatrix;
  glTranslatef(x,-y,-z);
  glScalef(AWidth,AHeight,ADepth);
  if Solid then glCallList(TeeSolidCubeList)
           else glCallList(TeeWireCubeList);
  glPopMatrix;
end;
{$ENDIF}

Const MaxTextures=10;
Type TTeeTextureBits=Array[0..800*600,0..3] of GLUByte;
     PTeeTextureBits=^TTeeTextureBits;
     TTeeTexture=record
       Bits      : PTeeTextureBits;
       Bitmap    : Pointer;
       GLTexture : GLUInt;
     end;

Var ITextures:Array[0..MaxTextures-1] of TTeeTexture;
    NumTextures:Integer=0;

Procedure TGLCanvas.DeleteTextures;
var t:Integer;
begin
  for t:=0 to NumTextures-1 do Dispose(ITextures[t].Bits);
  NumTextures:=0;
end;

Function TGLCanvas.FindTexture(ABitmap:TBitmap):GLUInt;
var t,
    tt  : Integer;
    tmp : TColor;
begin
  for t:=0 to NumTextures-1 do
  begin
    if ITextures[t].Bitmap=ABitmap then
    begin
      result:=ITextures[t].GLTexture;
      Exit;
    end;
  end;
  if NumTextures<MaxTextures then
  begin
    Inc(NumTextures);
    ITextures[NumTextures-1].Bitmap:=ABitmap;
    New(ITextures[NumTextures-1].Bits);
    With ABitmap,Canvas do
    begin
      for t:=0 to Height-1 do
      begin
        for tt:=0 to Width-1 do
        begin
          tmp:=Pixels[t,tt];
          With ITextures[NumTextures-1] do
          begin
            bits^[t*Height+tt,0]:=GetRValue(tmp);
            bits^[t*Height+tt,1]:=GetGValue(tmp);
            bits^[t*Height+tt,2]:=GetBValue(tmp);
            bits^[t*Height+tt,3]:=255;
          end;
        end;
      end;
    end;
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
    glGenTextures(1, @ITextures[NumTextures-1].GLTexture);
    glBindTexture(GL_TEXTURE_2D, ITextures[NumTextures-1].GLTexture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    tmp:=GL_NEAREST; {GL_LINEAR}
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, tmp);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, tmp);

    With ABitmap do
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Width, Height, 0,
                   GL_RGBA, GL_UNSIGNED_BYTE, ITextures[NumTextures-1].Bits);
    result:=ITextures[NumTextures-1].GLTexture;
  end
  else result:=0;
end;

procedure TGLCanvas.SetBrushBitmap;
begin
  if Brush.Bitmap<>nil then
  begin
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, FindTexture(Brush.Bitmap));
  end;
end;

Procedure TGLCanvas.Cube(Left,Right,Top,Bottom,Z0,Z1:Integer; DarkSides:Boolean);
begin
  {$IFDEF TEENOCULL}
  glEnable(GL_CULL_FACE);
  {$ENDIF}

  if Brush.Style<>bsClear then
  begin
    SetColor(Brush.Color);

    SetBrushBitmap;

    {$IFDEF TEECUBE}
    PlayCube(True,Left,Top,Z0,Right-Left,Bottom-Top,Z1-Z0);
    {$ELSE}

    glBegin(GL_QUADS);
    TeeNormal( 0, 0, -1);
     glTexCoord2f(1,0);
     TeeVertex3D( Left,  Bottom, z0);
     glTexCoord2f(1,1);
     TeeVertex3D( Right, Bottom, z0);
     glTexCoord2f(0,1);
     TeeVertex3D( Right, Top,    z0);
     glTexCoord2f(0,0);
     TeeVertex3D( Left,  Top,    z0);

    TeeNormal(-1,  0,  0);
     glTexCoord2f(0,0);
     TeeVertex3D( Left, Top,    z1);
     glTexCoord2f(0,1);
     TeeVertex3D( Left, Bottom, z1);
     glTexCoord2f(1,1);
     TeeVertex3D( Left, Bottom, z0);
     glTexCoord2f(1,0);
     TeeVertex3D( Left, Top,    z0);

    TeeNormal( 0, 0, 1);
     glTexCoord2f(0,0);
     TeeVertex3D( Right, Top,    z1);
     glTexCoord2f(0,1);
     TeeVertex3D( Right, Bottom, z1);
     glTexCoord2f(1,1);
     TeeVertex3D( Left,  Bottom, z1);
     glTexCoord2f(1,0);
     TeeVertex3D( Left,  Top,    z1);

    TeeNormal( 1,  0,  0);
     glTexCoord2f(0,0);
     TeeVertex3D( Right, Bottom, z0);
     glTexCoord2f(0,1);
     TeeVertex3D( Right, Bottom, z1);
     glTexCoord2f(1,1);
     TeeVertex3D( Right, Top,    z1);
     glTexCoord2f(1,0);
     TeeVertex3D( Right, Top,    z0);

    TeeNormal( 0, 1,  0);
     glTexCoord2f(0,0);
     TeeVertex3D( Left,  Top, z1);
     glTexCoord2f(0,1);
     TeeVertex3D( Left,  Top, z0);
     glTexCoord2f(1,1);
     TeeVertex3D( Right, Top, z0);
     glTexCoord2f(1,0);
     TeeVertex3D( Right, Top, z1);

    TeeNormal( 0, -1,  0);
     glTexCoord2f(0,0);
     TeeVertex3D( Right, Bottom, z0);
     glTexCoord2f(0,1);
     TeeVertex3D( Left,  Bottom, z0);
     glTexCoord2f(1,1);
     TeeVertex3D( Left,  Bottom, z1);
     glTexCoord2f(1,0);
     TeeVertex3D( Right, Bottom, z1);
    glEnd;
    {$ENDIF}
    if Brush.Bitmap<>nil then glDisable(GL_TEXTURE_2D);
  end;

  if Pen.Style<>psClear then
  begin
    SetPen;
    {$IFDEF TEECUBE}
    PlayCube(False,Left,Top,Z0,Right-Left,Bottom-Top,Z1-Z0);
    {$ELSE}

    glBegin(GL_LINE_LOOP);
      TeeVertex3D( Left,  Bottom, z0);
      TeeVertex3D( Right, Bottom, z0);
      TeeVertex3D( Right, Top,    z0);
      TeeVertex3D( Left,  Top,    z0);
    glEnd;

    glBegin(GL_LINE_LOOP);
      TeeVertex3D( Left,  Top,    z1);
      TeeVertex3D( Left,  Bottom, z1);
      TeeVertex3D( Right, Bottom, z1);
      TeeVertex3D( Right, Top,    z1);
    glEnd;

    glBegin(GL_LINE_LOOP);
      TeeVertex3D( Right, Top,    z0);
      TeeVertex3D( Right, Top,    z1);
      TeeVertex3D( Right, Bottom, z1);
      TeeVertex3D( Right, Bottom, z0);
    glEnd;

    glBegin(GL_LINE_LOOP);
      TeeVertex3D( Left, Top,    z0);
      TeeVertex3D( Left, Bottom, z0);
      TeeVertex3D( Left, Bottom, z1);
      TeeVertex3D( Left, Top,    z1);
    glEnd;

    glBegin(GL_LINE_LOOP);
      TeeVertex3D( Right, Top, z1);
      TeeVertex3D( Right, Top, z0);
      TeeVertex3D( Left,  Top, z0);
      TeeVertex3D( Left,  Top, z1);
    glEnd;
    {$ENDIF}
  end;

  {$IFDEF TEENOCULL}
  glDisable(GL_CULL_FACE);
  {$ENDIF}
  Assert(CheckGLError,'Cube');
end;

Procedure TGLCanvas.SetMaterialColor;

  Function GLColor(Const Value:Double):GLMat;
  begin
    result[0]:=Value;
    result[1]:=Value;
    result[2]:=Value;
    result[3]:=1;
  end;

var tmp:GLMat;
begin
  tmp:=GLColor(TeeMaterialDiffuse);
  glMaterialfv(TeeColorPlanes,GL_DIFFUSE,@tmp);
  tmp:=GLColor(TeeMaterialSpecular);
  glMaterialfv(TeeColorPlanes,GL_SPECULAR,@tmp);
  Assert(CheckGLError,'Material '+IntToStr(FSavedError));
end;

Function TGLCanvas.InitWindow( DestCanvas:TCanvas;
                               A3DOptions:TView3DOptions;
                               ABackColor:TColor;
                               Is3D:Boolean;
                               Const UserRect:TRect):TRect;
begin
  if (IDestCanvas<>DestCanvas) or (View3DOptions<>A3DOptions) then
  begin
    IDestCanvas:=DestCanvas;
    View3DOptions:=A3DOptions;
    InitOpenGL;
    DestroyGLContext;

    FDC:=DestCanvas.Handle;
    HRC:=CreateRenderingContext(FDC,[opDoubleBuffered],24,0);

    ActivateRenderingContext(FDC,HRC);
    Assert(CheckGLError,'ActivateContext');

 {   glEnable(GL_AUTO_NORMAL);
    Assert(CheckGLError,'EnableNormal');}

    glEnable(GL_NORMALIZE);
    Assert(CheckGLError,'EnableNormalize');

    glEnable(GL_DEPTH_TEST);
    Assert(CheckGLError,'EnableDepth');
    glDepthFunc(GL_LESS);
    Assert(CheckGLError,'DepthFunc');
//    glDepthRange(0.1,0.2);
    glEnable(GL_LINE_STIPPLE);
    Assert(CheckGLError,'EnableLineStipple');

//    glEnableClientState(GL_VERTEX_ARRAY);

    glEnable(GL_COLOR_MATERIAL);
    Assert(CheckGLError,'EnableColorMaterial');

    glColorMaterial(TeeColorPlanes,GL_AMBIENT);
    Assert(CheckGLError,'ColorMaterial');

    SetMaterialColor;

    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonOffset(0.5,1);

    Assert(CheckGLError,'PolygonOffset');

    glDisable(GL_POLYGON_SMOOTH);
    glDisable(GL_POINT_SMOOTH);
    glDisable(GL_LINE_SMOOTH);

    {$IFDEF TEENOCULL}
    glDisable(GL_CULL_FACE);
    {$ELSE}
    glEnable(GL_CULL_FACE);
    {$ENDIF}

    glEnable(GL_DITHER);

    Assert(CheckGLError,'Dither');

//    glEnable(GL_DITHER);
//    glCullFace(GL_FRONT);
//    glDepthRange(0.1,0.7);
//    glEnable(GL_LINE_SMOOTH);
//    glLineWidth(1);

    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glLightModelf(GL_LIGHT_MODEL_TWO_SIDE,TeeFullLightModel);
    glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER,TeeLightLocal);

    Assert(CheckGLError,'LightModel');

    {$IFDEF TEECUBE}
    glNewList(TeeSolidCubeList,GL_COMPILE);
    glBegin(GL_QUADS);
    TeeNormal( 0, 0, -1);
     TeeVertex3D( 0, 1, 0);
     TeeVertex3D( 1, 1, 0);
     TeeVertex3D( 1, 0, 0);
     TeeVertex3D( 0, 0, 0);

    TeeNormal(-1,  0,  0);
     TeeVertex3D( 0, 0, 1);
     TeeVertex3D( 0, 1, 1);
     TeeVertex3D( 0, 1, 0);
     TeeVertex3D( 0, 0, 0);

    TeeNormal( 0, 0, 1);
     TeeVertex3D( 1, 0, 1);
     TeeVertex3D( 1, 1, 1);
     TeeVertex3D( 0, 1, 1);
     TeeVertex3D( 0, 0, 1);

    TeeNormal( 1,  0,  0);
     TeeVertex3D( 1, 1, 0);
     TeeVertex3D( 1, 1, 1);
     TeeVertex3D( 1, 0, 1);
     TeeVertex3D( 1, 0, 0);

    TeeNormal( 0, 1,  0);
     TeeVertex3D( 0, 0, 1);
     TeeVertex3D( 0, 0, 0);
     TeeVertex3D( 1, 0, 0);
     TeeVertex3D( 1, 0, 1);

    TeeNormal( 0, -1,  0);
     TeeVertex3D( 1, 1, 0);
     TeeVertex3D( 0, 1, 0);
     TeeVertex3D( 0, 1, 1);
     TeeVertex3D( 1, 1, 1);
    glEnd;
    glEndList;

    glNewList(TeeWireCubeList,GL_COMPILE);
    glBegin(GL_LINE_LOOP);
      TeeVertex3D( 0, 1, 0);
      TeeVertex3D( 1, 1, 0);
      TeeVertex3D( 1, 0, 0);
      TeeVertex3D( 0, 0, 0);
    glEnd;

    glBegin(GL_LINE_LOOP);
      TeeVertex3D( 0, 0, 1);
      TeeVertex3D( 0, 1, 1);
      TeeVertex3D( 1, 1, 1);
      TeeVertex3D( 1, 0, 1);
    glEnd;

    glBegin(GL_LINE_LOOP);
      TeeVertex3D( 1, 0, 0);
      TeeVertex3D( 1, 0, 1);
      TeeVertex3D( 1, 1, 1);
      TeeVertex3D( 1, 1, 0);
    glEnd;

    glBegin(GL_LINE_LOOP);
      TeeVertex3D( 0, 0, 0);
      TeeVertex3D( 0, 1, 0);
      TeeVertex3D( 0, 1, 1);
      TeeVertex3D( 0, 0, 1);
    glEnd;

    glBegin(GL_LINE_LOOP);
      TeeVertex3D( 1, 0, 1);
      TeeVertex3D( 1, 0, 0);
      TeeVertex3D( 0, 0, 0);
      TeeVertex3D( 0, 0, 1);
    glEnd;
    glEndList;

    {$ENDIF}
  end;
  FX:=0;
  FY:=0;
  FIs3D:=Is3D;

  if GetObjectType(FDC) <> OBJ_MEMDC then
  begin
    FDC:=DestCanvas.Handle;
    ActivateRenderingContext(FDC,HRC);
  end;
  SetCanvas(DestCanvas);
  FBackColor:=ABackColor;
  result:=UserRect;
end;

Procedure TGLCanvas.InitAmbientLight(AmbientLight:Integer);
Const tmpSpec=0.9;
      tmpDif =0.9;
      tmpAmb =0.2;
var tmp:GLMat;
    tmpNum:Double;
begin
  glDisable(GL_LIGHTING);
  glDisable(GL_LIGHT0);
  Assert(CheckGLError,'DisableLight');

  if AmbientLight>0 then
  begin
    glEnable(GL_LIGHTING);
    Assert(CheckGLError,'EnableLight');
    tmpNum:=AmbientLight/100.0;
    tmp[0]:=tmpNum;
    tmp[1]:=tmpNum;
    tmp[2]:=tmpNum;
    tmp[3]:=1;
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT,  @tmp);
    Assert(CheckGLError,'LightModel');
  end
  else
  begin
    tmp[0]:=0;
    tmp[1]:=0;
    tmp[2]:=0;
    tmp[3]:=1;
    glEnable(GL_LIGHTING);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT,  @tmp);
    Assert(CheckGLError,'LightModel');
    glDisable(GL_LIGHTING);
    Assert(CheckGLError,'DisableLightModel');
  end;
end;

Procedure TGLCanvas.SetShininess(Const Value:Double);
begin
  glMateriali(TeeColorPlanes, GL_SHININESS, Round(128.0*Value));
  Assert(CheckGLError,'Shininess');
end;

Procedure TGLCanvas.InitLight0(Const AColor:GLMat; Const X,Y,Z:Double);
Const tmpSpec=0.9;
      tmpDif =0.9;
      tmpAmb =0.2;
var tmp:GLMat;
begin
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  Assert(CheckGLError,'EnableLight0');

  tmp[0]:=tmpAmb;
  tmp[1]:=tmpAmb;
  tmp[2]:=tmpAmb;
  tmp[3]:=1;
  tmp:=AColor;
  glLightfv(GL_LIGHT0,  GL_AMBIENT, @tmp);
  Assert(CheckGLError,'Light0Ambient');

  tmp[0]:=tmpDif;
  tmp[1]:=tmpDif;
  tmp[2]:=tmpDif;
  tmp[3]:=1;
  glLightfv(GL_LIGHT0,  GL_DIFFUSE, @tmp);
  Assert(CheckGLError,'Light0Diffuse');

  tmp[0]:=tmpSpec;
  tmp[1]:=tmpSpec;
  tmp[2]:=tmpSpec;
  tmp[3]:=1;
  glLightfv(GL_LIGHT0,  GL_SPECULAR, @tmp);
  Assert(CheckGLError,'Light0Specular');

  tmp[0]:=  X;
  tmp[1]:= -Y;
  tmp[2]:= -Z;
  tmp[3]:=1;
  glLightfv(GL_LIGHT0, GL_POSITION, @tmp);
  Assert(CheckGLError,'Light0Position');

  glLighti(GL_LIGHT0,GL_SPOT_CUTOFF,TeeDefaultLightSpot);
  Assert(CheckGLError,'Light0Spot');

//  glLighti(GL_LIGHT0,GL_SPOT_EXPONENT,2);
//  glLightf(GL_LIGHT0,GL_CONSTANT_ATTENUATION,1.2);
//  glLightf(GL_LIGHT0,GL_QUADRATIC_ATTENUATION,0.00001);
end;

Procedure TGLCanvas.ShowImage(DestCanvas,DefaultCanvas:TCanvas; Const UserRect:TRect);
begin
  glFlush;
  Assert(CheckGLError,'Flush');
  SwapBuffers(FDC);
//  FDirty:=False;
  SetCanvas(DefaultCanvas);
  Assert(CheckGLError,'ShowImage');
end;

Function TGLCanvas.ReDrawBitmap:Boolean;
begin
{  if UseBuffer then
  begin
    result:=not FDirty;
    if result then SwapBuffers(FDC);
  end
  else} result:=False;
end;

procedure TGLCanvas.Rectangle(X0,Y0,X1,Y1:Integer);
begin
  if Brush.Style<>bsClear then FillRect(Rect(X0,Y0,X1,Y1));
  if Pen.Style<>psClear then
  begin
    SetPen;
    glBegin(GL_LINE_LOOP);
    TeeVertex2D(X0,Y0);
    TeeVertex2D(X1,Y0);
    TeeVertex2D(X1,Y1);
    TeeVertex2D(X0,Y1);
    glEnd;
  end;
  Assert(CheckGLError,'Rectangle');
end;

procedure TGLCanvas.SetTextAlign(Align:Integer);
begin
  FTextAlign:=Align;
end;

procedure TGLCanvas.MoveTo(X, Y: Integer);
begin
  FX:=X;
  FY:=Y;
end;

procedure TGLCanvas.Pyramid(Vertical:Boolean; Left,Top,Right,Bottom,z0,z1:Integer; DarkSides:Boolean);
var AWidth,
    AHeight,
    ADepth:Integer;
begin
  glPushMatrix;
  glTranslatef(Left,-Bottom,-z0);
  if Vertical then
  begin
    AWidth:=Right-Left;
    AHeight:=Top-Bottom;
  end
  else
  begin
    AWidth:=Bottom-Top;
    AHeight:=Right-Left;
    glRotatef(90,0,0,1);
  end;
  ADepth:=z1-z0;
  if Brush.Style<>bsClear then
  begin
    glBegin(GL_TRIANGLE_FAN);
    SetColor(Brush.Color);
    TeeNormal(0,0,-1);
    TeeVertex3D(AWidth div 2,AHeight,ADepth div 2);
    TeeVertex2D(0,0);
    TeeVertex2D(AWidth,0);
    TeeVertex3D(AWidth,0,ADepth);
    TeeVertex3D(0,0,ADepth);
    TeeVertex2D(0,0);
    glEnd;
    RectangleY(0,0,AWidth,0,ADepth);
  end;
  if Pen.Style<>psClear then
  begin
    SetPen;
    glBegin(GL_LINE_LOOP);
    TeeVertex2D(0,0);
    TeeVertex2D(AWidth,0);
    TeeVertex3D(AWidth,0,ADepth);
    TeeVertex3D(0,0,ADepth);
    glEnd;
    glBegin(GL_LINE_STRIP);
    TeeVertex2D(0,0);
    TeeVertex3D(AWidth div 2,AHeight,ADepth div 2);
    TeeVertex3D(0,0,ADepth);
    glEnd;
    glBegin(GL_LINE_STRIP);
    TeeVertex2D(AWidth,0);
    TeeVertex3D(AWidth div 2,AHeight,ADepth div 2);
    TeeVertex3D(AWidth,0,ADepth);
    glEnd;
  end;
  glPopMatrix;
  Assert(CheckGLError,'Pyramid');
end;

procedure TGLCanvas.InternalCylinder(Vertical:Boolean;
                     Left,Top,Right,Bottom,Z0,Z1:Integer; Dark3D:Boolean;
                     ConePercent:Integer);
Var tmpSize,
    tmp,
    tmp2,
    Radius:Integer;
begin
  glPushMatrix;
  Radius:=Abs(Z1-Z0) div 2;
  if Vertical then
  begin
    Radius:=MinInteger((Right-Left) div 2,Radius);
    glTranslatef((Left+Right) div 2,-Top,-(z0+z1) div 2);
    glRotatef(90,1,0,0);
    tmpSize:=Bottom-Top;
  end
  else
  begin
    Radius:=MinInteger((Bottom-Top) div 2,Radius);
    glTranslatef(Left,-(Top+Bottom) div 2,-(z0+z1) div 2);
    glRotatef(90,0,1,0);
    tmpSize:=Right-Left;
  end;

  if ConePercent=100 then tmp:=Radius
                     else tmp:=Round(0.01*ConePercent*Radius);

  tmp2:=MinLong(18,6*Radius);
  if Brush.Style<>bsClear then
  begin
    SetColor(Brush.Color);
    gluCylinder(FQuadric,tmp,Radius,tmpSize,tmp2,6);
    if ConePercent=100 then gluDisk(FQuadric,0,tmp,tmp2,6);
    // bottom disk !
  end;
  if Pen.Style<>psClear then
  begin
    SetPen;
    gluQuadricDrawStyle(FQuadric, GLU_LINE);
    gluCylinder(FQuadric,tmp+0.5,Radius+0.5,tmpSize+0.5,tmp2,6);
    gluQuadricDrawStyle(FQuadric, GLU_FILL);
  end;
  glPopMatrix;
end;

procedure TGLCanvas.Cylinder(Vertical:Boolean; Left,Top,Right,Bottom,Z0,Z1:Integer; DarkCover:Boolean);
begin
  InternalCylinder(Vertical,Left,Top,Right,Bottom,Z0,Z1,DarkCover,100);
  Assert(CheckGLError,'Cylinder');
end;

procedure TGLCanvas.Cone(Vertical:Boolean; Left,Top,Right,Bottom,Z0,Z1:Integer; Dark3D:Boolean; ConePercent:Integer);
begin
  InternalCylinder(Vertical,Left,Top,Right,Bottom,Z0,Z1,Dark3D,ConePercent);
  Assert(CheckGLError,'Cone');
end;

procedure TGLCanvas.Sphere(x,y,z:Integer; Const Radius:Double);
begin
  glPushMatrix;
  glTranslatef(x,-y,-z);
  SetColor(Brush.Color);
  if Brush.Bitmap<>nil then
  begin
    glEnable(GL_TEXTURE_2D);
    gluQuadricTexture(FQuadric,FindTexture(Brush.Bitmap));
  end;
  gluSphere(FQuadric,Radius,TeeSphereSlices,TeeSphereStacks);
  if Brush.Bitmap<>nil then glDisable(GL_TEXTURE_2D);
  glPopMatrix;
  Assert(CheckGLError,'Sphere');
end;

Procedure TGLCanvas.SetColor(AColor:TColor);
var tmp:GLMat;
begin
  ColorToGL(AColor,tmp);
  glColor3fv(@tmp);
end;

procedure TGLCanvas.SetPen;
begin
  With Pen do
  begin
    if Style=psSolid then glDisable(GL_LINE_STIPPLE)
    else
    begin
      glEnable(GL_LINE_STIPPLE);
      Case Style of
       psSolid   : glLineStipple(1,$FFFF);
       psDot     : glLineStipple(1,$5555);
       psDash    : glLineStipple(1,$00FF);
       psDashDot : glLineStipple(1,$55FF);
      else
       glLineStipple(1,$1C47);
      end;
    end;
    glLineWidth(Width);
    SetColor(Color);
  end;
  Assert(CheckGLError,'SetPen');
end;

procedure TGLCanvas.LineTo(X, Y: Integer);
begin
  SetPen;
  glBegin(GL_LINES);
    TeeVertex2D(FX,FY);
    TeeVertex2D(X,Y);
  glEnd;
  FX:=X;
  FY:=Y;
  Assert(CheckGLError,'LineTo');
end;

procedure TGLCanvas.ClipRectangle(Const Rect:TRect);
begin
end;

procedure TGLCanvas.ClipCube(Const Rect:TRect; MinZ,MaxZ:Integer);
begin
end;

procedure TGLCanvas.UnClipRectangle;
begin
end;

function TGLCanvas.GetBackColor:TColor;
begin
  result:=clWhite;
end;

procedure TGLCanvas.SetBackColor(Color:TColor);
begin
  FBackColor:=Color;
end;

procedure TGLCanvas.SetBackMode(Mode:TCanvasBackMode);
begin
  FBackMode:=Mode;
end;

Function TGLCanvas.GetMonochrome:Boolean;
begin
  result:=False;
end;

Procedure TGLCanvas.SetMonochrome(Value:Boolean);
begin
end;

procedure TGLCanvas.StretchDraw(const Rect: TRect; Graphic: TGraphic);
begin
end;

procedure TGLCanvas.Draw(X, Y: Integer; Graphic: TGraphic);
begin
end;

Procedure TGLCanvas.GradientFill( Const Rect:TRect;
                                  StartColor,EndColor:TColor;
                                  Direction:TGradientDirection);

begin
  Assert(CheckGLError,'Before GradientFill');
  glBegin(GL_QUADS);
  TeeNormal(0,0,-1);
  Case Direction of
     gdTopBottom  : With Rect do
                    begin
                      SetColor(EndColor);
                      TeeVertex2D(Right,Bottom);
                      SetColor(StartColor);
                      TeeVertex2D(Right,Top);
                      TeeVertex2D(Left, Top);
                      SetColor(EndColor);
                      TeeVertex2D(Left, Bottom);
                    end;
     gdBottomTop  : ;
     gdLeftRight  : ;
     gdRightLeft  : ;
     gdFromCenter : ;
     gdFromTopLeft: ;
  else
{     gdFromBottomLeft }
  end;
  glEnd;
  Assert(CheckGLError,'GradientFill');
end;

Procedure TGLCanvas.RectangleY(Left,Top,Right,Z0,Z1:Integer);
begin
  if Brush.Style<>bsClear then
  begin
    glBegin(GL_QUADS);
    TeeNormal(0,1,0);
    SetColor(Brush.Color);
    TeeVertex3D(Left, Top,Z0);
    TeeVertex3D(Right,Top,Z0);
    TeeVertex3D(Right,Top,Z1);
    TeeVertex3D(Left, Top,Z1);
    glEnd;
  end;
  if Pen.Style<>psClear then
  begin
    SetPen;
    glBegin(GL_LINE_LOOP);
    TeeVertex3D(Left, Top,Z0);
    TeeVertex3D(Right,Top,Z0);
    TeeVertex3D(Right,Top,Z1);
    TeeVertex3D(Left, Top,Z1);
    glEnd;
  end;
  Assert(CheckGLError,'RectangleY');
end;

Procedure TGLCanvas.RectangleWithZ(Const Rect:TRect; Z:Integer);
begin
  With Rect do
  begin
    if Pen.Style<>psClear then
    begin
      SetPen;
      glBegin(GL_LINE_LOOP);
      TeeVertex3D(Left, Top,   Z);
      TeeVertex3D(Right,Top,   Z);
      TeeVertex3D(Right,Bottom,Z);
      TeeVertex3D(Left, Bottom,Z);
      glEnd;
    end;
    if Brush.Style<>bsClear then
    begin
      SetColor(Brush.Color);
      SetBrushBitmap;
      glBegin(GL_QUADS);
      TeeNormal(0,0,-1);
      glTexCoord2f(0,1);
      TeeVertex3D(Left, Top,   Z);
      glTexCoord2f(1,1);
      TeeVertex3D(Left, Bottom,Z);
      glTexCoord2f(1,0);
      TeeVertex3D(Right,Bottom,Z);
      glTexCoord2f(0,0);
      TeeVertex3D(Right,Top,   Z);
      glEnd;
      if Brush.Bitmap<>nil then glDisable(GL_TEXTURE_2D);
    end;
  end;
  Assert(CheckGLError,'RectangleWithZ');
end;

Procedure TGLCanvas.RectangleZ(Left,Top,Bottom,Z0,Z1:Integer);
begin
  if Pen.Style<>psClear then
  begin
    SetPen;
    glBegin(GL_LINE_LOOP);
    TeeVertex3D(Left,Top,   Z0);
    TeeVertex3D(Left,Bottom,Z0);
    TeeVertex3D(Left,Bottom,Z1);
    TeeVertex3D(Left,Top,   Z1);
    glEnd;
  end;
  if Brush.Style<>bsClear then
  begin
    SetColor(Brush.Color);
    SetBrushBitmap;
    glBegin(GL_QUADS);
    TeeNormal(1,0,0);
    glTexCoord2f(0,1);
    TeeVertex3D(Left, Top,   Z0);
    glTexCoord2f(1,1);
    TeeVertex3D(Left, Bottom,Z0);
    glTexCoord2f(1,0);
    TeeVertex3D(Left,Bottom,Z1);
    glTexCoord2f(0,0);
    TeeVertex3D(Left,Top,   Z1);
    glEnd;
    if Brush.Bitmap<>nil then glDisable(GL_TEXTURE_2D);
  end;
  Assert(CheckGLError,'RectangleZ');
end;

procedure TGLCanvas.FillRect(const Rect: TRect);
begin
  if Brush.Style<>bsClear then
  begin
    glBegin(GL_QUADS);
    TeeNormal(0,0,-1);
    SetColor(Brush.Color);
    With Rect do
    begin
      TeeVertex2D(Left, Top);
      TeeVertex2D(Left, Bottom);
      TeeVertex2D(Right,Bottom);
      TeeVertex2D(Right,Top);
    end;
    glEnd;
  end;
  Assert(CheckGLError,'FillRect '+IntToStr(FSavedError));
end;

procedure TGLCanvas.Frame3D( Rect: TRect; TopColor, BottomColor: TColor;
                             Width: Integer);
begin
//  Brush.Style:=bsClear;
//  DoRectangle(Rect);
//  FillRect(Rect);
end;

procedure TGLCanvas.Ellipse(X1, Y1, X2, Y2: Integer);
begin
  EllipseWithZ(X1,Y1,X2,Y2,0);
  Assert(CheckGLError,'Ellipse');
end;

procedure TGLCanvas.EllipseWithZ(X1, Y1, X2, Y2, Z: Integer);
Const PiStep=pi/10.0;
var t,XC,YC,XR,YR:Integer;
    tmpSin,tmpCos:Extended;
begin
  XR:=(X2-X1) div 2;
  YR:=(Y2-Y1) div 2;
  XC:=(X1+X2) div 2;
  YC:=(Y1+Y2) div 2;
  if Pen.Style<>psClear then
  begin
    SetPen;
    glBegin(GL_LINE_LOOP);
    for t:=0 to 18 do
    begin
      SinCos(t*piStep,tmpSin,tmpCos);
      TeeVertex3D(XC+Trunc(XR*tmpSin),YC-Trunc(YR*tmpCos),Z);
    end;
    glEnd;
  end;
  if Brush.Style<>bsClear then
  begin
    glBegin(GL_TRIANGLE_FAN);
    SetColor(Brush.Color);
    TeeNormal(0,0,-1);
    TeeVertex3D(XC,YC,Z);
    for t:=0 to 20 do
    begin
      SinCos(t*piStep,tmpSin,tmpCos);
      TeeVertex3D(XC+Trunc(XR*tmpSin),YC-Trunc(YR*tmpCos),Z);
    end;
    glEnd;
  end;
  Assert(CheckGLError,'EllipseWithZ');
end;

procedure TGLCanvas.FrontPlaneBegin;  { for titles and legend only... }
begin
//  DisableRotation;
//  With View3DOptions do
//  glTranslatef(-FXCenter-HorizOffset,FYCenter+VertOffset,TeeZoomScale/CalcZoom);
end;

procedure TGLCanvas.FrontPlaneEnd;
begin
//  EnableRotation;
end;

Procedure TGLCanvas.EnableRotation;
begin
  glPopMatrix;
  Assert(CheckGLError,'FrontPlaneEnd');
end;

Procedure TGLCanvas.DisableRotation;
begin
  glPushMatrix;
  glLoadIdentity;
  Assert(CheckGLError,'FrontPlaneBegin');
end;

procedure TGLCanvas.SetPixel3D(X,Y,Z:Integer; Value: TColor);
begin
  if Pen.Style<>psClear then
  begin
    glBegin(GL_POINT);
    SetColor(Value);
    TeeVertex3D(X,Y,Z);
    glEnd;
    Assert(CheckGLError,'Pixel3D');
  end;
end;

procedure TGLCanvas.SetPixel(X, Y: Integer; Value: TColor);
begin
  if Pen.Style<>psClear then
  begin
    glBegin(GL_POINT);
    SetColor(Value);
    TeeVertex2D(X,Y);
    glEnd;
    Assert(CheckGLError,'Pixel');
  end;
end;

procedure TGLCanvas.Arc(X1, Y1, X2, Y2, X3, Y3, X4, Y4: Integer);
begin
  Assert(CheckGLError,'Arc');
//  gluPartialDisk
end;

procedure TGLCanvas.Donut( XCenter,YCenter,XRadius,YRadius:Integer;
                           Const StartAngle,EndAngle,HolePercent:Double);
begin
  Assert(CheckGLError,'Donut');
end;

procedure TGLCanvas.Pie(X1, Y1, X2, Y2, X3, Y3, X4, Y4: Integer);
begin
  Assert(CheckGLError,'Pie');
//  gluPartialDisk
end;

procedure TGLCanvas.Pie3D( XCenter,YCenter,XRadius,YRadius,Z0,Z1:Integer;
                           Const StartAngle,EndAngle:Double;
                           DarkSides,DrawSides:Boolean);

Const NumSliceParts=16;

Var piStep:Double;
    tmpSin,
    tmpCos:Extended;

  Function ToDegree(Const Value:Double):Double;
  begin
    result:=Value*180.0/pi;
  end;

  Procedure DrawPieSlice(z,ANormal:Integer);

    Procedure DrawSlice;
    var t:Integer;

      Procedure DrawSliceStep;
      begin
        SinCos(StartAngle+(t*piStep),tmpSin,tmpCos);
        TeeVertex3D(Trunc(XRadius*tmpSin),Trunc(YRadius*tmpCos),z);
      end;

    begin
      TeeVertex3D(0,0,z);
      if z=z0 then for t:=0 to NumSliceParts do DrawSliceStep
              else for t:=NumSliceParts downto 0 do DrawSliceStep;
    end;

  begin
    if Pen.Style<>psClear then
    begin
      SetPen;
      glBegin(GL_LINE_LOOP);
      DrawSlice;
      glEnd;
    end;
    if Brush.Style<>bsClear then
    begin
      glBegin(GL_TRIANGLE_FAN);
      SetColor(Brush.Color);
      TeeNormal(0,0,ANormal);
      DrawSlice;
      glEnd;
    end;
  end;

  Procedure DrawCover;
  var t,x,y:Integer;
  begin
    glBegin(GL_QUAD_STRIP);
    SetColor(Brush.Color);
    TeeNormal(0,1,0);
    for t:=0 to NumSliceParts do
    begin
      SinCos(StartAngle+(t*piStep),tmpSin,tmpCos);
      X:=Trunc(XRadius*tmpSin);
      Y:=Trunc(YRadius*tmpCos);
      TeeVertex2D(X,Y);
      TeeVertex3D(X,Y,z1-z0);
    end;
    glEnd;
  end;

  Procedure DrawSide(Const AAngle:Double);
  begin
    SinCos(AAngle,tmpSin,tmpCos);
    Plane3D(Point(0,0),Point(Round(XRadius*tmpSin),Round(YRadius*tmpCos)),Z0,Z1);
  end;

begin
  glPushMatrix;
  glTranslatef(XCenter,-YCenter,0);
  piStep:=(EndAngle-StartAngle)/NumSliceParts;
  if DrawSides then
  begin
    DrawSide(StartAngle);
    DrawSide(EndAngle);
  end;
  {$IFDEF TEENOCULL}
  glEnable(GL_CULL_FACE);
  {$ENDIF}
  DrawCover;
  DrawPieSlice(z0,-1);
  DrawPieSlice(z1,1);
  {$IFDEF TEENOCULL}
  glDisable(GL_CULL_FACE);
  {$ENDIF}
  glPopMatrix;
  Assert(CheckGLError,'Pie3D');
end;

procedure TGLCanvas.Polygon(const Points: array of TPoint);
begin
  PolygonWithZ(Points,0);
  Assert(CheckGLError,'Polygon');
end;

procedure TGLCanvas.PlaneFour3D(Var Points:TFourPoints; Z0,Z1:Integer);
var tmpNormal:GLMat;

  Procedure CalcNormalPlaneFour;
  var Qx,Qy,Qz,Px,Py,Pz:Double;
  begin
    Qx:= Points[3].x-Points[2].x;
    Qy:= Points[3].y-Points[2].y;
    Qz:= z1-z1;
    Px:= Points[0].x-Points[2].x;
    Py:= Points[0].y-Points[2].y;
    Pz:= z0-z1;
    tmpNormal[0]:= (Py*Qz - Pz*Qy);
    tmpNormal[1]:= (Pz*Qx - Px*Qz);
    tmpNormal[2]:= -(Px*Qy - Py*Qx);
  end;

  Procedure AddPoints;
  begin
    With Points[0] do TeeVertex3D(x,y,z0);
    With Points[1] do TeeVertex3D(x,y,z0);
    With Points[2] do TeeVertex3D(x,y,z1);
    With Points[3] do TeeVertex3D(x,y,z1);
  end;

begin
  if Pen.Style<>psClear then
  begin
    SetPen;
    glBegin(GL_LINE_LOOP);
    AddPoints;
    glEnd;
  end;
  if Brush.Style<>bsClear then
  begin
    glBegin(GL_QUADS);
    CalcNormalPlaneFour;
    glNormal3fv(@tmpNormal);
    SetColor(Brush.Color);
    AddPoints;
    glEnd;
  end;
  Assert(CheckGLError,'PlaneFour3D');
end;

procedure TGLCanvas.RoundRect(X1, Y1, X2, Y2, X3, Y3: Integer);
begin
  Rectangle(X1,Y1,X2,Y2);
  Assert(CheckGLError,'RoundRect');
end;

Procedure TGLCanvas.Repaint;
begin
  if Assigned(View3DOptions) then View3DOptions.Repaint;
end;

Procedure TGLCanvas.Invalidate;
begin
//  FDirty:=True;
end;

type PointFloat=record x,y:Double; end;
{     GLYPHMETRICSFLOAT=record
       gmfBlackBoxX,
       gmfBlackBoxY:Double;
       gmfptGlyphOrigin:PointFloat;
       gmfCellIncX,
       gmfCellIncY:Double;
     end;}

Procedure TGLCanvas.InitOpenGLFont;
var Old,
    HFont    : THandle;
    FontMode : Integer;
//    agmf:Array[0..255] of GLYPHMETRICSFLOAT ;
begin
  HFont := CreateFont(-12, 0, 0, 0, FW_BOLD,
		      0, 0, 0, ANSI_CHARSET,
		      OUT_DEFAULT_PRECIS,
                      CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY{DRAFT_QUALITY},
		      DEFAULT_PITCH or FF_DONTCARE{FIXED_PITCH or FF_MODERN},
                      TeeOpenGLFontName);

  Old:=SelectObject (FDC, HFont);
  FontOffset:=1000;

  DeleteFont;

  if FontOutlines then FontMode:=WGL_FONT_LINES
                  else FontMode:=WGL_FONT_POLYGONS;
  {$R-,Q-,S-,I-,D-,L-,C-}
  wglUseFontOutlines( FDC,32,TeeFontListRange,FontOffset,0,
                      TeeOpenGLFontExtrusion,FontMode,nil{@agmf});

  DeleteObject(SelectObject(FDC,Old));
  IFontCreated:=True;
  Assert(CheckGLError,'InitFont');
end;

Procedure TGLCanvas.TextOut3D(X,Y,Z:Integer; const Text:String);
var tmp       : TSize;
    tmpLength : Integer;
    tmpSize   : Double;
begin
  if not IFontCreated then InitOpenGLFont;
  tmpLength:=Length(Text);

  ReferenceCanvas.Font.Assign(Font);
  GetTextExtentPoint(ReferenceCanvas.Handle,PChar(Text),tmpLength, tmp);


  if (FTextAlign and TA_CENTER)=TA_CENTER then
    x:=x-Round(0.55*tmp.Cx)
  else
  if (FTextAlign and TA_RIGHT)=TA_RIGHT then
    x:=x-tmp.Cx-(tmpLength div 2)               {-Round(Sqr(tmp.Cx)/19.0)};

  if (FTextAlign and TA_BOTTOM)<>TA_BOTTOM then
    y:=y+Round(0.7*tmp.Cy);

  {$IFDEF TEENOCULL}
  if TeeOpenGLFontExtrusion>0 then glEnable(GL_CULL_FACE);
  {$ENDIF}

  glPushMatrix;

  glTranslatef(x,-y,-z+2);

{  if FTextToViewer
  With View3DOptions do
  begin
    glRotatef(360-Tilt, 0, 0, 1);
    glRotatef(360-Rotation, 0, 1, 0);
    if not Orthogonal then glRotatef(360+Elevation, 1, 0, 0);
  end; }

  if TeeTextAngleY<>0 then glRotatef(TeeTextAngleY,1,0,0);
  if TeeTextAngleZ<>0 then glRotatef(TeeTextAngleZ,0,1,0);

//  other font rotations: glRotatef(270,1,0,0);
  tmpSize:=Font.Size*1.5;
  glScalef(tmpSize,tmpSize,1);
  TeeNormal(0,0,1);
  SetColor(Font.Color);

  glListBase(FontOffset-32);
  glCallLists(tmpLength, GL_UNSIGNED_BYTE, PChar(Text));
  glPopMatrix;
  Assert(CheckGLError,'TextOut3D');
  if TeeOpenGLFontExtrusion>0 then
  begin
    {$IFDEF TEENOCULL}
    glDisable(GL_CULL_FACE);
    {$ENDIF}
    glFrontFace(GL_CCW);
    Assert(CheckGLError,'FrontFace');
  end;
end;

Procedure TGLCanvas.TextOut(X,Y:Integer; const Text:String);
begin
  TextOut3D(x,y,0,Text);
end;

procedure TGLCanvas.MoveTo3D(X,Y,Z:Integer);
begin
  FX:=X;
  FY:=Y;
  FZ:=Z;
end;

procedure TGLCanvas.LineTo3D(X,Y,Z:Integer);
begin
  SetPen;
  glBegin(GL_LINES);
  TeeVertex3D(FX,FY,FZ);
  TeeVertex3D(x,y,z);
  glEnd;
  FX:=X;
  FY:=Y;
  FZ:=Z;
  Assert(CheckGLError,'LineTo3D');
end;

procedure TGLCanvas.PlaneWithZ(P1,P2,P3,P4:TPoint; Z:Integer);
begin
  if Pen.Style<>psClear then
  begin
    SetPen;
    glBegin(GL_LINE_LOOP);
    TeeVertex3D(P1.X,P1.Y,Z);
    TeeVertex3D(P2.X,P2.Y,Z);
    TeeVertex3D(P3.X,P3.Y,Z);
    TeeVertex3D(P4.X,P4.Y,Z);
    glEnd;
  end;
  if Brush.Style<>bsClear then
  begin
    glBegin(GL_QUADS);
    SetColor(Brush.Color);
    TeeNormal(0,0,-1);
    TeeVertex3D(P1.X,P1.Y,Z);
    TeeVertex3D(P2.X,P2.Y,Z);
    TeeVertex3D(P3.X,P3.Y,Z);
    TeeVertex3D(P4.X,P4.Y,Z);
    glEnd;
  end;
  Assert(CheckGLError,'PlaneWithZ');
end;

procedure TGLCanvas.Plane3D(Const A,B:TPoint; Z0,Z1:Integer);
begin
  if Brush.Style<>bsClear then
  begin
    glBegin(GL_QUADS);
    TeeNormal(0,1,0);  { <-- CalcNormal }
    SetColor(Brush.Color);
    TeeVertex3D(A.X,A.Y,Z0);
    TeeVertex3D(B.X,B.Y,Z0);
    TeeVertex3D(B.X,B.Y,Z1);
    TeeVertex3D(A.X,A.Y,Z1);
    glEnd;
  end;
  if Pen.Style<>psClear then
  begin
    SetPen;
    glBegin(GL_LINE_LOOP);
    TeeVertex3D(A.X,A.Y,Z0);
    TeeVertex3D(B.X,B.Y,Z0);
    TeeVertex3D(B.X,B.Y,Z1);
    TeeVertex3D(A.X,A.Y,Z1);
    glEnd;
  end;
  Assert(CheckGLError,'Plane3D');
end;

Function TGLCanvas.GetSupports3DText:Boolean;
begin
  result:=True;
end;

Function TGLCanvas.GetSupportsFullRotation:Boolean;
begin
  result:=True;
end;

Function TGLCanvas.GetTextAlign:TCanvasTextAlign;
begin
  result:=FTextAlign;
end;

Function TGLCanvas.GetUseBuffer:Boolean;
begin
  result:=FUseBuffer;
end;

Procedure TGLCanvas.SetUseBuffer(Value:Boolean);
begin
  FUseBuffer:=Value;
  IDestCanvas:=nil;
  IFontCreated:=False;
end;

Procedure TGLCanvas.DeleteFont;
begin
  if IFontCreated then
  begin
    glDeleteLists(FontOffset,TeeFontListRange);
    IFontCreated:=False;
  end;
//  Assert(CheckGLError,'DeleteFont '+IntToStr(FSavedError));
end;

Function TGLCanvas.GetHandle:HDC;
begin
  result:=FDC;
end;

Procedure TGLCanvas.DoRectangle(Const Rect:TRect);
begin
  With Rect do Rectangle(Left,Top,Right,Bottom)
end;

Procedure TGLCanvas.DoHorizLine(X0,X1,Y:Integer);
begin
  MoveTo(X0,Y);
  LineTo(X1,Y);
end;

Procedure TGLCanvas.DoVertLine(X,Y0,Y1:Integer);
begin
  MoveTo(X,Y0);
  LineTo(X,Y1);
end;

procedure TGLCanvas.RotateLabel3D(x,y,z:Integer; Const St:String; RotDegree:Integer);
begin
  glPushMatrix;
  glTranslatef(x,-y,0);
  glRotatef(RotDegree,0,0,1);
  glTranslatef(-x,y,z);
  TextOut(X,Y,St);
  glPopMatrix;
  Assert(CheckGLError,'RotateLabel3D');
end;

procedure TGLCanvas.RotateLabel(x,y:Integer; Const St:String; RotDegree:Integer);
begin
  RotateLabel3D(x,y,0,St,RotDegree);
end;

Procedure TGLCanvas.Line(X0,Y0,X1,Y1:Integer);
begin
  MoveTo(X0,Y0);
  LineTo(X1,Y1);
end;

procedure TGLCanvas.EraseBackground(const Rect: TRect);
begin { nothing ! OpenGL already clears... }
end;

Procedure TGLCanvas.HorizLine3D(Left,Right,Y,Z:Integer);
begin
  MoveTo3D(Left,Y,Z);
  LineTo3D(Right,Y,Z);
end;

Procedure TGLCanvas.VertLine3D(X,Top,Bottom,Z:Integer);
begin
  MoveTo3D(X,Top,Z);
  LineTo3D(X,Bottom,Z);
end;

Procedure TGLCanvas.ZLine3D(X,Y,Z0,Z1:Integer);
begin
  MoveTo3D(X,Y,Z0);
  LineTo3D(X,Y,Z1);
end;

Procedure TGLCanvas.Arrow( Filled:Boolean;
                           Const FromPoint,ToPoint:TPoint;
                           ArrowWidth,ArrowHeight,Z:Integer);
Var x    : Double;
    y    : Double;
    SinA : Double;
    CosA : Double;

    Function CalcArrowPoint:TPoint;
    Begin
      result.X:=Round( x*CosA + y*SinA);
      result.Y:=Round(-x*SinA + y*CosA);
    end;

Var tmpHoriz  : Integer;
    tmpVert   : Integer;
    dx        : Integer;
    dy        : Integer;
    tmpHoriz4 : Double;
    xb        : Double;
    yb        : Double;
    l         : Double;

   { These are the Arrows points coordinates }
    To3D,pc,pd,pe,pf,pg,ph:TPoint;

    (*           pc
                   |\
    ph           pf| \
      |------------   \ ToPoint
 From |------------   /
    pg           pe| /
                   |/
                 pd
    *)
begin
  Assert(CheckGLError,'BeforeArrow');
  dx := ToPoint.x-FromPoint.x;
  dy := FromPoint.y-ToPoint.y;
  l  := Sqrt(1.0*dx*dx+1.0*dy*dy);
  if l>0 then  { if at least one pixel... }
  Begin
    tmpHoriz:=ArrowWidth;
    tmpVert :=MinLong(Round(l),ArrowHeight);
    SinA:= dy / l;
    CosA:= dx / l;
    xb:= ToPoint.x*CosA - ToPoint.y*SinA;
    yb:= ToPoint.x*SinA + ToPoint.y*CosA;
    x := xb - tmpVert;
    y := yb - tmpHoriz/2;
    pc:=CalcArrowPoint;
    y := yb + tmpHoriz/2;
    pd:=CalcArrowPoint;
    if Filled then
    Begin
      tmpHoriz4:=tmpHoriz/4;
      y := yb - tmpHoriz4;
      pe:=CalcArrowPoint;
      y := yb + tmpHoriz4;
      pf:=CalcArrowPoint;
      x := FromPoint.x*cosa - FromPoint.y*sina;
      y := yb - tmpHoriz4;
      pg:=CalcArrowPoint;
      y := yb + tmpHoriz4;
      ph:=CalcArrowPoint;
      To3D:=ToPoint;
      PolygonWithZ([ph,pg,pe,pf],Z);
      PolygonWithZ([pc,To3D,pd],Z);
    end
    else
    begin
      MoveTo3D(FromPoint.x,FromPoint.y,z);
      LineTo3D(ToPoint.x,ToPoint.y,z);
      LineTo3D(pd.x,pd.y,z);
      MoveTo3D(ToPoint.x,ToPoint.y,z);
      LineTo3D(pc.x,pc.y,z);
    end;
  end;
  Assert(CheckGLError,'Arrow');
end;

Procedure TGLCanvas.LineWithZ(X0,Y0,X1,Y1,Z:Integer);
begin
  MoveTo3D(X0,Y0,Z);
  LineTo3D(X1,Y1,Z);
end;

procedure TGLCanvas.PolygonWithZ(Points: array of TPoint; Z:Integer);

  Procedure AddPoints;
  var t : Integer;
  begin
    for t:=Low(Points) to High(Points) do
        With Points[t] do TeeVertex3D(x,y,z);
  end;

begin
  if Brush.Style<>bsClear then
  begin
    glBegin(GL_POLYGON);
    TeeNormal(0,0,-1);
    SetColor(Brush.Color);
    AddPoints;
    glEnd;
  end;
  if Pen.Style<>psClear then
  begin
    SetPen;
    glBegin(GL_LINE_LOOP);
    AddPoints;
    glEnd;
  end;
  Assert(CheckGLError,'PolygonWithZ');
end;

procedure TGLCanvas.Triangle3D( Const Points:TTrianglePoints3D;
                                Const Colors:TTriangleColors3D);
var t:Integer;
begin
  if Brush.Style<>bsClear then
  begin
    glBegin(GL_POLYGON);
    TeeNormal(0,0,-1);  { <-- calc Normal }
    SetColor(Colors[0]);
    With Points[0] do TeeVertex3D(x,y,z);
    SetColor(Colors[1]);
    With Points[1] do TeeVertex3D(x,y,z);
    SetColor(Colors[2]);
    With Points[2] do TeeVertex3D(x,y,z);
    glEnd;
  end;
  if Pen.Style<>psClear then
  begin
    SetPen;
    glBegin(GL_LINE_LOOP);
    for t:=0 to 2 do With Points[t] do TeeVertex3D(x,y,z);
    glEnd;
  end;
  Assert(CheckGLError,'Triangle3D');
end;

procedure TGLCanvas.TriangleWithZ(Const P1,P2,P3:TPoint; Z:Integer);
begin
  PolygonWithZ([P1,P2,P3],Z);
end;

Function TGLCanvas.GetBackMode:TCanvasBackMode;
begin
  result:=FBackMode;
end;

Procedure TGLCanvas.Surface3D( Style:TTeeCanvasSurfaceStyle;
                               SameBrush:Boolean;
                               NumXValues,NumZValues:Integer;
                               CalcPoints:TTeeCanvasCalcPoints);

  Procedure DrawCells;
  var P0,
      P1   : TPoint3D;
      tmpX,
      tmpZ : Integer;

    Procedure AddVertexs;
    Var tmpColor0,
        tmpColor1 : TColor;
        tmp       : GLMat;
    begin
      if CalcPoints(tmpX,tmpZ+1,P0,P1,tmpColor0,tmpColor1) then
      begin
        if SameBrush then
        begin
          With P0 do TeeVertex3D(x,y,z);
          With P1 do TeeVertex3D(x,y,z);
        end
        else
        begin
          if tmpColor0<>clNone then
          begin
            ColorToGL(tmpColor0,tmp);
            glColor3fv(@tmp);
          end;
          With P0 do TeeVertex3D(x,y,z);
          if tmpColor1<>clNone then
          begin
            ColorToGL(tmpColor1,tmp);
            glColor3fv(@tmp);
          end;
          With P1 do TeeVertex3D(x,y,z);
        end;
      end
      else
      begin
        glEnd;
        glBegin(GL_QUAD_STRIP);
        TeeNormal(0,-1,0);
      end;
    end;

  begin
    for tmpX:=2 to NumXValues do
    begin
      glBegin(GL_QUAD_STRIP);
      TeeNormal(0,1,0);
      for tmpZ:=NumZValues-1 downto 0 do AddVertexs;
      glEnd;
    end;
  end;

begin
  SetPen;
  if (Style=tcsSolid) or (not SameBrush) then SetColor(Brush.Color);

  if Style=tcsDot then glPolygonMode(GL_FRONT_AND_BACK,GL_POINT)
  else
  if Style=tcsWire then glPolygonMode(GL_FRONT_AND_BACK,GL_LINE)
                   else glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

  DrawCells;
  if (Pen.Style<>psClear) and (Style=tcsSolid) then
  begin
    glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
    SetColor(Pen.Color);
    SameBrush:=True;
//    glDisable(GL_LIGHTING);
    DrawCells;
//    glEnable(GL_LIGHTING);
  end;
  glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
  Assert(CheckGLError,'Surface3D');
end;

procedure TGLCanvas.PyramidTrunc(Const R: TRect; StartZ, EndZ: Integer;
                                 TruncX,TruncZ:Integer);
begin
  Pyramid(True,R.Left,R.Top,R.Right,R.Bottom,StartZ,EndZ,True);
end;

end.

