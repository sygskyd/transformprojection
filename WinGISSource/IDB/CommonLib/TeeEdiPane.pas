{**********************************************}
{  TCustomChart (or derived) Editor Dialog     }
{  Copyright (c) 1996-2000 by David Berneda    }
{**********************************************}
{$I teedefs.inc}
unit TeeEdiPane;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls, QComCtrls,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, ComCtrls,
     {$ENDIF}
     TeeProcs, TeeEdiGrad, TeCanvas, TeePenDlg;

type
  TFormTeePanel = class(TForm)
    PageControl1: TPageControl;
    TabBack: TTabSheet;
    TabBorders: TTabSheet;
    TabGradient: TTabSheet;
    L19: TLabel;
    L2: TLabel;
    RGBevelIn: TRadioGroup;
    RGBevelOut: TRadioGroup;
    SEPanelBor: TEdit;
    SEPanelWi: TEdit;
    UDPanelWi: TUpDown;
    UDPanelBor: TUpDown;
    CBPanelBorder: TCheckBox;
    BPanelColor: TButtonColor;
    GB6: TGroupBox;
    RGBitmap: TRadioGroup;
    BBrowseImage: TButton;
    CBImageInside: TCheckBox;
    procedure RGBevelInClick(Sender: TObject);
    procedure RGBevelOutClick(Sender: TObject);
    procedure CBPanelBorderClick(Sender: TObject);
    procedure SEPanelWiChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RGBitmapClick(Sender: TObject);
    procedure BBrowseImageClick(Sender: TObject);
    procedure CBImageInsideClick(Sender: TObject);
    procedure SEPanelBorChange(Sender: TObject);
    procedure BPanelColorClick(Sender: TObject);
  private
    { Private declarations }
    FGradientEditor : TTeeGradientEditor;
    procedure EnableImageControls;
  public
    { Public declarations }
    ThePanel:TCustomTeePanelExtended;
    Constructor CreatePanel(Owner:TComponent; APanel:TCustomTeePanelExtended);
  end;

implementation

{$R *.dfm}
Uses TeeBrushDlg, TeeConst;

{ TFormTeePanel }
Constructor TFormTeePanel.CreatePanel(Owner:TComponent; APanel:TCustomTeePanelExtended);
begin
  inherited Create(Owner);
  ThePanel:=APanel;
  FGradientEditor:=TTeeGradientEditor.Create(Self);
  With FGradientEditor do
  begin
    BOk.Visible:=False;
    BCancel.Visible:=False;
    Height:=Height-BOk.Height;
  end;
  AddFormTo(FGradientEditor,TabGradient,Integer(ThePanel.Gradient));
  PageControl1.ActivePage:=TabBack;
end;

procedure TFormTeePanel.RGBevelInClick(Sender: TObject);
begin
  if ThePanel.BevelInner<>TPanelBevel(RGBevelIn.ItemIndex) then
     ThePanel.BevelInner:=TPanelBevel(RGBevelIn.ItemIndex);
end;

procedure TFormTeePanel.RGBevelOutClick(Sender: TObject);
begin
  if ThePanel.BevelOuter<>TPanelBevel(RGBevelOut.ItemIndex) then
     ThePanel.BevelOuter:=TPanelBevel(RGBevelOut.ItemIndex);
end;

procedure TFormTeePanel.CBPanelBorderClick(Sender: TObject);
Const Styles:Array[Boolean] of TBorderStyle=(bsNone,bsSingle);
begin
  ThePanel.BorderStyle:=Styles[CBPanelBorder.Checked]
end;

procedure TFormTeePanel.SEPanelWiChange(Sender: TObject);
begin
  if Showing then
  With ThePanel do
  if BevelWidth<>UDPanelWi.Position then BevelWidth:=UDPanelWi.Position;
end;

procedure TFormTeePanel.FormShow(Sender: TObject);
begin
  With ThePanel do
  begin
    RGBevelIn.ItemIndex    :=Ord(BevelInner);
    RGBevelOut.ItemIndex   :=Ord(BevelOuter);
    CBPanelBorder.Checked  :=BorderStyle=bsSingle;
    UDPanelWi.Position     :=BevelWidth;
    UDPanelBor.Position    :=BorderWidth;
    RGBitmap.ItemIndex     :=Ord(BackImageMode);
    CBImageInside.Checked  :=BackImageInside;
    EnableImageControls;
  end;
  BPanelColor.LinkProperty(ThePanel,'Color');
  FGradientEditor.Visible:=True;
  PageControl1.ActivePage:=TabBack;
end;

procedure TFormTeePanel.RGBitmapClick(Sender: TObject);
begin
  ThePanel.BackImageMode:=TTeeBackImageMode(RGBitmap.ItemIndex);
end;

procedure TFormTeePanel.EnableImageControls;
begin
  RGBitmap.Enabled:=Assigned(ThePanel.BackImage.Graphic);
  CBImageInside.Enabled:=RGBitmap.Enabled;
  if RGBitmap.Enabled then
     BBrowseImage.Caption:=TeeMsg_ClearImage
  else
     BBrowseImage.Caption:=TeeMsg_BrowseImage;
end;

procedure TFormTeePanel.BBrowseImageClick(Sender: TObject);
begin
  TeeLoadClearImage(Self,ThePanel.BackImage);
  EnableImageControls;
end;

procedure TFormTeePanel.CBImageInsideClick(Sender: TObject);
begin
  ThePanel.BackImageInside:=CBImageInside.Checked;
end;

procedure TFormTeePanel.SEPanelBorChange(Sender: TObject);
begin
  if Showing then
  With ThePanel do
  if BorderWidth<>UDPanelBor.Position then BorderWidth:=UDPanelBor.Position;
end;

procedure TFormTeePanel.BPanelColorClick(Sender: TObject);
begin
  ThePanel.Repaint;
end;

end.
