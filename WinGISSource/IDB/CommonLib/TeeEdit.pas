{*****************************************}
{   TeeChart Pro                          }
{   Copyright (c) 1995-2000 David Berneda }
{     TChartEditor                        }
{     TChartPreviewer                     }
{     TChartEditorPanel                   }
{*****************************************}
{$I teedefs.inc}
unit TeeEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, Chart, TeeEditCha, TeeProcs {$IFNDEF D4},ExtCtrls{$ENDIF};

type
  TCustomChartEditor=class(TComponent)
  private
    { Private declarations }
    FChart   : TCustomChart;
    FTitle   : String;
    FOnClose : TNotifyEvent;
    FOnShow  : TNotifyEvent;
  protected
    { Protected declarations }
    procedure Notification( AComponent: TComponent;
                            Operation: TOperation); override;
    procedure SetChart(const Value: TCustomChart); virtual;
  public
    { Public declarations }
    Procedure Execute; virtual; abstract;
    property Title:String read FTitle write FTitle;
  published
    { Published declarations }
    property Chart:TCustomChart read FChart write SetChart;
    property OnClose: TNotifyEvent read FOnClose write FOnClose;
    property OnShow: TNotifyEvent read FOnShow write FOnShow;
  end;

  TChartEditor = class(TCustomChartEditor)
  private
    { Private declarations }
    FAutoRepaint : Boolean;
    FDefaultTab  : TChartEditorTab;
    FHideTabs    : TChartEditorHiddenTabs;
    FOptions     : TChartEditorOptions;
  public
    { Public declarations }
    Constructor Create(AOwner:TComponent); override;
    Procedure Execute; override;
    Function SetType: Byte;
  published
    { Published declarations }
    property AutoRepaint:Boolean read FAutoRepaint write FAutoRepaint default True;
    property DefaultTab:TChartEditorTab read FDefaultTab write FDefaultTab default cetMain;
    property HideTabs:TChartEditorHiddenTabs read FHideTabs write FHideTabs;
    property Options:TChartEditorOptions read FOptions write FOptions
                                         default eoAll;
    property Title;
  end;

  TChartPreviewOption=( cpoChangePrinter,
                        cpoSetupPrinter,
                        cpoResizeChart,
                        cpoMoveChart,
                        cpoChangeDetail,
                        cpoChangePaperOrientation,
                        cpoChangeMargins,
                        cpoProportional,
                        cpoDragChart,
                        cpoPrintPanel,
                        cpoAsBitmap );

  TChartPreviewOptions=set of TChartPreviewOption;

Const DefaultChartPreviewOptions=[ cpoChangePrinter,
                                   cpoSetupPrinter,
                                   cpoResizeChart,
                                   cpoMoveChart,
                                   cpoChangeDetail,
                                   cpoChangePaperOrientation,
                                   cpoChangeMargins,
                                   cpoProportional ];


type
  TChartPreviewer = class(TCustomChartEditor)
  private
    FOptions    : TChartPreviewOptions;
    FPaperColor : TColor;
    FWindowState: TWindowState;
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    Constructor Create(AOwner:TComponent); override;
    Procedure Execute; override;
  published
    { Published declarations }
    property Options:TChartPreviewOptions read FOptions write FOptions
             default DefaultChartPreviewOptions;
    property PaperColor:TColor read FPaperColor write FPaperColor default clWhite;
    property Title;
    property WindowState:TWindowState read FWindowState write FWindowState default wsNormal;
  end;

  TChartEditorPanel=class(TCustomPanelNoCaption)
  private
    FChart  : TCustomChart;
    FEditor : TChartEditForm;
    Procedure SetChart(Value:TCustomChart);
  protected
    procedure Notification( AComponent: TComponent;
                            Operation: TOperation); override;
  public
    { Public declarations }
    Constructor Create(AOwner:TComponent); override;
    Procedure SelectUnderMouse;
    property Editor:TChartEditForm read FEditor;
  published
    property Align;
    property BevelOuter default bvLowered;
    property Chart:TCustomChart read FChart write SetChart;

    {$IFNDEF CLX}
    {$IFDEF D4}
    {$IFNDEF TEEOCX}
    property UseDockManager default True;
    property DockSite;
    {$ENDIF}
    {$ENDIF}
    property DragCursor;
    property DragMode;
    {$ENDIF}
    property Enabled;
    property ParentColor;
    property ParentShowHint;
    {$IFNDEF TEEOCX}
    property PopupMenu;
    {$ENDIF}
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    {$IFDEF D4}
    property Anchors;
    {$IFNDEF TEEOCX}
    property Constraints;
    {$ENDIF}
    property Locked;
    {$ENDIF}
    {$IFNDEF CLX}
    {$IFDEF D4}
    property AutoSize;
    property DragKind;
    {$ENDIF}
    {$ENDIF}

    { TPanel events }
    property OnClick;
    {$IFNDEF CLX}
    {$IFDEF D5}
    property OnContextPopup;
    {$ENDIF}
    {$ENDIF}
    property OnDblClick;
    {$IFNDEF CLX}
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    {$ENDIF}
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    {$IFNDEF CLX}
    {$IFDEF D4}
    property OnMouseWheel;
    property OnMouseWheelDown;
    property OnMouseWheelUp;
    {$ENDIF}
    property OnStartDrag;
    {$IFDEF D4}
    {$IFNDEF TEEOCX}
    property OnConstrainedResize;
    {$ENDIF}
    {$ENDIF}
    {$IFNDEF CLX}
    {$IFDEF D4}
    property OnCanResize;
    {$IFNDEF TEEOCX}
    property OnDockDrop;
    property OnDockOver;
    property OnEndDock;
    property OnGetSiteInfo;
    property OnStartDock;
    property OnUnDock;
    {$ENDIF}
    {$ENDIF}
    {$ENDIF}
    {$ENDIF}
  end;

implementation

Uses TeeProCo, EditChar, TeePrevi, TeEngine, TeeEdiAxis, ComCtrls,
     TeeEdiTitl;

{ TCustomChartEditor }
procedure TCustomChartEditor.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if Operation=opRemove then
     if Assigned(FChart) and (AComponent=FChart) then
        Chart:=nil;
end;

procedure TCustomChartEditor.SetChart(const Value: TCustomChart);
begin
  FChart:=Value;
  if Assigned(FChart) then FChart.FreeNotification(Self);
end;

{ TChartEditor }
Constructor TChartEditor.Create(AOwner:TComponent);
begin
  inherited;
  FHideTabs:=[];
  FOptions:=eoAll;
  FDefaultTab:=cetMain;
  FAutoRepaint:=True;
end;

function TChartEditor.SetType: Byte;
var
   TheForm : TChartEditForm;
   OldAuto : Boolean;
begin
  Result:=0;
  if Assigned(FChart) then
  begin
    TheForm:=TChartEditForm.Create(nil);
    With TheForm do
    try
      Caption:=Self.FTitle;
      Chart:=FChart;
      EditorOptions:=FOptions;
      CheckHelpFile;
      TheActivePageIndex:=Ord(FDefaultTab);
      TheHiddenTabs:=FHideTabs;
      if Assigned(Self.FOnShow) then Self.FOnShow(TheForm);
      OldAuto:=True;
      if not Self.FAutoRepaint then
      begin
        OldAuto:=FChart.AutoRepaint;
        FChart.AutoRepaint:=False;
      end;
      TheForm.BChangeTypeSeriesClick(nil);
      if not Self.FAutoRepaint then
      begin
        FChart.AutoRepaint:=OldAuto;
        FChart.Repaint;
      end;
    finally
      if Assigned(Self.FOnClose) then Self.FOnClose(TheForm);
      Free;
    end;
    FChart.CancelMouse:=True;
  end;
end;

procedure TChartEditor.Execute;
Var TheForm : TChartEditForm;
    OldAuto : Boolean;
begin
  if Assigned(FChart) then
  begin
    TheForm:=TChartEditForm.Create(nil);
    With TheForm do
    try
      Caption:=Self.FTitle;
      Chart:=FChart;
      EditorOptions:=FOptions;
      CheckHelpFile;
      TheActivePageIndex:=Ord(FDefaultTab);
      TheHiddenTabs:=FHideTabs;
      if Assigned(Self.FOnShow) then Self.FOnShow(TheForm);
      OldAuto:=True;
      if not Self.FAutoRepaint then
      begin
        OldAuto:=FChart.AutoRepaint;
        FChart.AutoRepaint:=False;
      end;
      ShowModal;
      if not Self.FAutoRepaint then
      begin
        FChart.AutoRepaint:=OldAuto;
        FChart.Repaint;
      end;
    finally
      if Assigned(Self.FOnClose) then Self.FOnClose(TheForm);
      Free;
    end;
    FChart.CancelMouse:=True;
  end;
end;

{ TChartPreviewer }
Constructor TChartPreviewer.Create(AOwner:TComponent);
begin
  inherited;
  FOptions:=DefaultChartPreviewOptions;
  FPaperColor:=clWhite;
  FWindowState:=wsNormal;
end;

procedure TChartPreviewer.Execute;
var OldPrintTeePanel : Boolean;
    TheForm          : TChartPreview;
begin
  if Assigned(FChart) then
  begin
    TheForm:=TChartPreview.Create(nil);
    with TheForm do
    try
      if Self.FTitle<>'' then Caption:=Self.FTitle;
      TeePreviewPanel1.Panel:=Self.FChart;
      WindowState:=Self.FWindowState;
      if not (cpoChangePrinter in Self.FOptions) then CBPrinters.Enabled:=False;
      if not (cpoSetupPrinter in Self.FOptions) then BSetupPrinter.Enabled:=False;
      TeePreviewPanel1.AllowResize:=(cpoResizeChart in Self.FOptions);
      TeePreviewPanel1.AllowMove:=(cpoMoveChart in Self.FOptions);
      if not (cpoChangeDetail in Self.FOptions) then ChangeDetailGroup.Enabled:=False;
      if not (cpoChangePaperOrientation in Self.FOptions) then Orientation.Enabled:=False;
      if not (cpoChangeMargins in Self.FOptions) then
      begin
        GBMargins.Enabled:=False;
        BReset.Enabled:=False;
      end;
      With TeePreviewPanel1.Panel do
      if PrintProportional then
         PrintProportional:=(cpoProportional in Self.FOptions);
      TeePreviewPanel1.PaperColor:=Self.FPaperColor;
      TeePreviewPanel1.DragImage:=(cpoDragChart in Self.FOptions);
      OldPrintTeePanel:=PrintTeePanel;
      PrintTeePanel:=(cpoPrintPanel in Self.FOptions);
      TeePreviewPanel1.AsBitmap:=(cpoAsBitmap in Self.FOptions);
      if Assigned(Self.FOnShow) then Self.FOnShow(TheForm);
      ShowModal;
      PrintTeePanel:=OldPrintTeePanel;
    finally
      if Assigned(Self.FOnClose) then Self.FOnClose(TheForm);
      Free;
      Self.FChart.Repaint;
    end;
    Self.FChart.CancelMouse:=True;
  end;
end;

{ TChartEditorPanel }
Constructor TChartEditorPanel.Create(AOwner:TComponent);
begin
  inherited;
  BevelOuter:=bvLowered;
  Width :=409;
  Height:=291;
  if not (csDesigning in ComponentState) then
  begin
    FEditor:=TChartEditForm.Create(Self);
    With FEditor do
    begin
      BorderStyle:={$IFDEF CLX}fbsNone{$ELSE}bsNone{$ENDIF};
      PanBottom.Visible:=False;
      Show;
      Align:=alClient;
      Parent:=Self;
    end;
  end;
end;

procedure TChartEditorPanel.Notification( AComponent: TComponent;
                                          Operation: TOperation);
begin
  inherited;
  if Operation=opRemove then
     if Assigned(FChart) and (AComponent=FChart) then
        Chart:=nil;
end;

procedure TChartEditorPanel.SelectUnderMouse;
Var P : TChartClickedPart;

  Procedure Select(ATab:TTabSheet);
  begin
    With Editor do
    begin
      MainPage.ActivePage:=TabChart;
      SubPage.ActivePage:=ATab;
      SubPageChange(Self);
    end;
  end;

  Procedure SelectTitle(ATitle:TChartTitle);
  begin
    Select(Editor.TabTitle);
    With Editor.TabTitle.Controls[0] as TFormTeeTitle do
    begin
      TheTitle:=ATitle;
      FormShow(Self);
    end;
  end;

begin
  if Assigned(Chart) then
  begin
    Chart.CalcClickedPart(Chart.GetCursorPos,P);
    Case P.Part of
      cpLegend: With Editor do
                begin
                  MainPage.ActivePage:=TabChart;
                  SubPage.ActivePage:=TabLegend;
                  SubPageChange(Self);
                end;
      cpSeriesMarks,
      cpSeries: With Editor do
                begin
                  MainPage.ActivePage:=TabSeries;
                  MainPageChange(Self);
                  TheEditSeries:=P.ASeries;
                  TheFormSeries.TheSeries:=TheEditSeries;
                  LBSeries.FillSeries(TheEditSeries);
                  SetTabSeries;
                  if P.Part=cpSeriesMarks then
                  With TheFormSeries do
                     PageSeries.ActivePage:=TabMarks;
                end;
        cpAxis: With Editor do
                begin
                  MainPage.ActivePage:=TabChart;
                  TheAxis:=P.AAxis;
                  SubPage.ActivePage:=TabAxis;
                  SubPageChange(Self);
                  With (TabAxis.Controls[0] as TFormTeeAxis) do
                  begin
                    TheAxis:=P.AAxis;
                    FormShow(Self);
                  end;
                end;
       cpTitle : SelectTitle(Chart.Title);
    cpSubTitle : SelectTitle(Chart.SubTitle);
        cpFoot : SelectTitle(Chart.Foot);
     cpSubFoot : SelectTitle(Chart.SubFoot);
    else
      Select(Editor.TabPanel);
    end;
  end;
end;

Procedure TChartEditorPanel.SetChart(Value:TCustomChart);
begin
  FChart:=Value;
  if Assigned(FEditor) then FEditor.Chart:=FChart;
end;

end.
