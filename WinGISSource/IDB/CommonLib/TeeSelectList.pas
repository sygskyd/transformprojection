{********************************************}
{     TeeChart Pro Charting Library          }
{ Copyright (c) 1995-2000 by David Berneda   }
{         All Rights Reserved                }
{********************************************}
{$I TeeDefs.inc}
unit TeeSelectList;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QButtons,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, Buttons,
     {$ENDIF}
     TeeProcs;

type
  TSelectListForm = class(TForm)
    L22: TLabel;
    L24: TLabel;
    BMoveUP: TSpeedButton;
    BMoveDown: TSpeedButton;
    FromList: TListBox;
    ToList: TListBox;
    BRightOne: TButton;
    BRightAll: TButton;
    BLeftOne: TButton;
    BLeftAll: TButton;
    procedure FormCreate(Sender: TObject);
    procedure BMoveUPClick(Sender: TObject);
    procedure ToListDblClick(Sender: TObject);
    procedure FromListDblClick(Sender: TObject);
    procedure BLeftAllClick(Sender: TObject);
    procedure BRightAllClick(Sender: TObject);
    procedure BLeftOneClick(Sender: TObject);
    procedure BRightOneClick(Sender: TObject);
    procedure ToListClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure Changed;
  public
    { Public declarations }
    OnChange : TNotifyEvent;
    procedure EnableButtons;
  end;

implementation

{$R *.dfm}
Uses TeePenDlg;

procedure TSelectListForm.FormCreate(Sender: TObject);
begin
  TeeLoadArrowBitmaps(BMoveUp.Glyph,BMoveDown.Glyph);
end;

procedure TSelectListForm.Changed;
begin
  EnableButtons;
  if Assigned(OnChange) then OnChange(Self);
end;

procedure TSelectListForm.BMoveUPClick(Sender: TObject);
var tmp : Integer;
begin
  if Sender=BMoveUp then tmp:=-1 else tmp:=1;
  with ToList do Items.Exchange(ItemIndex,ItemIndex+tmp);
  ToList.SetFocus;
  Changed;
end;

procedure TSelectListForm.ToListDblClick(Sender: TObject);
begin
  BLeftOneClick(Self);
end;

procedure TSelectListForm.FromListDblClick(Sender: TObject);
begin
  BRightOneClick(Self);
end;

procedure TSelectListForm.BLeftAllClick(Sender: TObject);
begin
  MoveListAll(ToList.Items,FromList.Items);
  Changed;
end;

procedure TSelectListForm.BRightAllClick(Sender: TObject);
begin
  MoveListAll(FromList.Items,ToList.Items);
  Changed;
end;

procedure TSelectListForm.BLeftOneClick(Sender: TObject);
begin
  MoveList(ToList,FromList);
  Changed;
end;

procedure TSelectListForm.BRightOneClick(Sender: TObject);
begin
  MoveList(FromList,ToList);
  Changed;
  if FromList.Items.Count=0 then ToList.SetFocus;
end;

procedure TSelectListForm.ToListClick(Sender: TObject);
begin
  EnableButtons;
end;

procedure TSelectListForm.EnableButtons;
begin
  BRightOne.Enabled:=FromList.Items.Count>0;
  BRightAll.Enabled:=BRightOne.Enabled;
  BLeftOne.Enabled :=ToList.Items.Count>0;
  BLeftAll.Enabled :=BLeftOne.Enabled;
  BMoveUp.Enabled  :=ToList.ItemIndex>0;
  BMoveDown.Enabled:=(ToList.ItemIndex>-1) and (ToList.ItemIndex<ToList.Items.Count-1);
end;

procedure TSelectListForm.FormShow(Sender: TObject);
begin
  EnableButtons;
  FromList.SetFocus;
end;

end.
