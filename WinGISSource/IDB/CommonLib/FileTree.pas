Unit FileTree;

Interface

Uses WinProcs,WinTypes,Classes,Controls,Forms,Graphics,Menus,Messages,StdCtrls,
     TreeList,TreeView,WCtrls;

Type TFileInfo     = Class
      Private
       FLongName        : AnsiString;
       FShortName       : AnsiString;
       FRelLongName     : AnsiString;
       FRelShortName    : AnsiString;
       FReference       : LongInt;
       FValid           : Boolean;
       FPtr             : Pointer;
       //brovak
       FComment         : Ansistring;
      Public
       //changed by brovak
       Constructor Create(Const ALongName,AShortName,ARelLongName,ARelShortName:AnsiString;
                 ARef:LongInt;Ptr:Pointer=nil;Const AComment:Ansistring = '');
       Property    LongName:AnsiString read FLongName write FLongName;
       Function    Name(AShort,ARelative:Boolean):AnsiString;
       Property    Reference:LongInt read FReference write FReference;
       Property    RelativeLongName:AnsiString read FRelLongName write FRelLongName;
       Property    RelativeShortName:AnsiString read FRelShortName write FRelShortName;
       Property    ShortName:AnsiString read FShortName write FShortName;
       Property    Valid:Boolean read FValid write FValid;
       Property    SomePtr:Pointer read FPtr write FPtr;
       //brovak
       Property    Comment:Ansistring read FComment write FComment;
     end;

     TFileTree     = Class(TTreeEntry)
      Private
       FFileInfo   : TFileInfo;
       FIsDir      : Boolean;
       FValid      : Boolean;
      Public
       Constructor Create(AName:AnsiString;AInfo:TFileInfo;ABitmap:Integer;AValid,AIsDir:Boolean);
       Property    FileInfo:TFileInfo read FFileInfo write FFileInfo;
       Property    IsDirectory:Boolean read FIsDir write FIsDir;
       Property    Valid:Boolean read FValid write FValid;
     end;

     TOnFilterItems     = Function(AInfo:TFileInfo):Boolean of object;

     TFileTreeView = Class(TCustomTreeListView)
      Private
       FFiles           : TList;
       FMergeEmptyDirs  : Boolean;
       FOleBitmaps      : Boolean;
       FOnFilterItems   : TOnFilterItems;
       FRelativeName    : AnsiString;
       FRelativePaths   : Boolean;
       FRelativeTo      : AnsiString;
       FRelativeTree    : TTreeEntry;
       FShortPaths      : Boolean;
       Procedure   SetRelativePaths(ARelative:Boolean);
       Procedure   SetRelativeTo(const Value:AnsiString);
       Procedure   SetShortPaths(AShort:Boolean);
       Procedure   UpdateTree;
      Protected
       Procedure   DoDrawBitmap(Index:Integer;ARect:TRect;State:TOwnerDrawState); override;
       Procedure   DoDrawCaption(Index:Integer;ARect:TRect;State:TOwnerDrawState); override;
       Procedure   DoUpdate; override;
       Function    GetImageIndex(ABitmap:Integer):Integer; override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       //changed by brovak
       Procedure   Add(Const ALongName,AShortName,ARelLongName,ARelShortName:AnsiString;ARef:LongInt; Ptr: Pointer = nil;Comment:AnsiString='');
       Procedure   Clear;
       Property    Files:TList read FFiles;
       Function    FileInfoToTree(AInfo:TFileInfo):TFileTree;
       Function    PathOfTree(ATree:TFileTree):AnsiString;
       Property    RelativeTree:TTreeEntry read FRelativeTree;
       Property    Tree;
      Published
       Property    Align;
       Property    BorderStyle;
       Property    Color;
       Property    Columns;
       Property    Ctl3D;
       Property    DragCursor;
       Property    DragMode;
       Property    Enabled;
       Property    ExtendedSelect;
       Property    Font;
       {$IFDEF WIN32}
       Property    ImeMode;
       Property    ImeName;
       {$ENDIF}
       Property    IntegralHeight;
       Property    ItemHeight default 17;
       Property    MultiSelect;
       Property    ParentColor;
       Property    ParentCtl3D;
       Property    ParentFont;
       Property    ParentShowHint;
       Property    PopupMenu;
       Property    ShowHint;
       Property    Sorted;
       Property    TabOrder;
       Property    TabStop;
       Property    Visible;
       Property    OnClick;
       Property    OnDblClick;
       Property    OnDragDrop;
       Property    OnDragOver;
       Property    OnDrawBitmap;
       Property    OnDrawCaption;
       Property    OnEndDrag;
       Property    OnEnter;
       Property    OnExit;
       Property    OnKeyDown;
       Property    OnKeyPress;
       Property    OnKeyUp;
       Property    OnMeasureItem;
       Property    OnMouseDown;
       Property    OnMouseMove;
       Property    OnMouseUp;
       {$IFDEF Win32}
       Property    OnStartDrag;
       {$ENDIF}
       Property    ItemWidth default 20;
       Property    MergeEmptyDirectories:Boolean read FMergeEmptyDirs write FMergeEmptyDirs;
       Property    OLEBitmaps:Boolean read FOLEBitmaps write FOLEBitmaps;
       Property    OnFilterItems:TOnFilterItems read FOnFilterItems write FOnFilterItems;
       Property    RelativePaths:Boolean read FRelativePaths write SetRelativePaths default True;
       Property    RelativeCaption:AnsiString read FRelativeName write FRelativeName;
       Property    RelativeToPath:AnsiString read FRelativeTo write SetRelativeTo;
       Property    Scrollbars;
       Property    ShortPaths:Boolean read FShortPaths write SetShortPaths default False;
     end;

Implementation

{$IFDEF WIN32}
{$R *.R32}
{$ELSE}
{$R *.RES}
{$ENDIF}

Uses FileCtrl,ShellApi,SysUtils,StrTools
{$IFDEF WIN32}
;
{$ELSE}
,AnsiStr;
{$ENDIF}

Const OLEBitmapOffset   = 5;

var FImages        : TImageList;
    FExtensions    : TStringList;

{===============================================================================
| Diverses
+==============================================================================}

{$IFNDEF WIN32}
Type TIconInfo     =  Record
       fIcon       : BOOL;
       xHotspot    : Integer;
       yHotspot    : Integer;
       hbmMask     : HBITMAP;
       hbmColor    : HBITMAP;
     end;
{$ENDIF}

Function GetOLEBitmap(Const FileName:AnsiString):Integer;
var AIndex       : Integer;
    Buffer       : Array[0..255] of Char;
    RegistryName : Array[0..255] of Char;
    BufferSize   : LongInt;
    APos         : PChar;
    Icon         : HIcon;
    LargeIcon    : HIcon;
    IconNumber   : Integer;
    IconInfo     : TIconInfo;
    ABitmap      : TBitmap;
    BBitmap      : TBitmap;
    CBitmap      : TBitmap;
    WIconNumber  : Word;
    AIcon        : TIcon;
    StoreFileName: Boolean;
begin
  AIndex:=FExtensions.IndexOf(FileName);
  if AIndex<0 then AIndex:=FExtensions.IndexOf(ExtractFileExt(FileName));
  if AIndex>=0 then Result:=LongInt(FExtensions.Objects[AIndex])
  else begin
    StoreFileName:=FALSE;
    Result:=3;
    Icon:=0;
    BufferSize:=255;
{      FindExecutable(StrPCopy(Buffer,FileName),'',Registryname);}
{      Icon:=ExtractAssociatedIcon(HInstance,StrPCopy(Buffer,FileName),WIconNumber);}
    if RegQueryValue(HKEY_CLASSES_ROOT,StrPCopy(RegistryName,ExtractFileExt(FileName)),
        RegistryName,BufferSize)=ERROR_SUCCESS then begin
      BufferSize:=255;
      StrCopy(Buffer,RegistryName);
      if RegQueryValue(HKEY_CLASSES_ROOT,StrCat(Buffer,'\DefaultIcon'),
          Buffer,BufferSize)=ERROR_SUCCESS then begin
        IconNumber:=0;  
        if StrLIComp(Buffer,'%1',2)=0 then begin
          StrPCopy(Buffer,ExpandFileName(FileName));
          StoreFileName:=TRUE;
        end  
        else begin
          APos:=AnsiStrPos(Buffer,',');
          if APos<>NIL then begin
            APos^:=#0;
            IconNumber:=StrToInt(StrPas(APos+1));
          end
        end;
        {$IFDEF WIN32}
        ExtractIconEx(Buffer,IconNumber,LargeIcon,Icon,1);
        {$ELSE}
        Icon:=ExtractIcon(HInstance,Buffer,IconNumber);
        {$ENDIF}
      end;
      if Icon=0 then begin
        BufferSize:=255;
        StrCopy(Buffer,RegistryName);
        if RegQueryValue(HKEY_CLASSES_ROOT,StrCat(Buffer,'\Shell\Open\Command'),
            Buffer,BufferSize)=ERROR_SUCCESS then begin
          {$IFDEF WIN32}
          APos:=AnsiStrPos(Buffer,' ');
          if APos<>NIL then APos^:=#0;
          ExtractIconEx(Buffer,0,LargeIcon,Icon,1);
          {$ELSE}
          Icon:=ExtractIcon(HInstance,Buffer,Word(-1));
          {$ENDIF}
        end;
      end;
      if Icon<>0 then begin
        Result:=FImages.Count+OLEBitmapOffset;
        {$IFDEF WIN32}
        GetIconInfo(Icon,IconInfo);
        ABitmap:=TBitmap.Create;
        ABitmap.Width:=16;
        ABitmap.Height:=16;
        BBitmap:=TBitmap.Create;
        BBitmap.Handle:=IconInfo.hbmMask;
        ABitmap.Canvas.CopyRect(Rect(0,0,16,16),BBitmap.Canvas,Rect(0,0,BBitmap.Width,BBitmap.Height));
        CBitmap:=TBitmap.Create;
        CBitmap.Width:=16;
        CBitmap.Height:=16;
        BBitmap.Handle:=IconInfo.hbmColor;
        CBitmap.Canvas.CopyRect(Rect(0,0,16,16),BBitmap.Canvas,Rect(0,0,BBitmap.Width,BBitmap.Height));
        FImages.Add(CBitmap,ABitmap);
        ABitmap.Free;
        BBitmap.Free;
        CBitmap.Free;
        {$ELSE}
        ABitmap:=TBitmap.Create;
        ABitmap.Width:=16;
        ABitmap.Height:=16;
        AIcon:=TIcon.Create;
        AIcon.Handle:=Icon;
        ABitmap.Canvas.StretchDraw(Rect(0,0,16,16),AIcon);
        FImages.AddMasked(ABitmap,clWhite);
        {$ENDIF}
      end;
    end;
    if StoreFileName then FExtensions.AddObject(FileName,Pointer(Result))
    else FExtensions.AddObject(ExtractFileExt(FileName),Pointer(Result));
  end;
end;

{===============================================================================
| TFileTreeView
+==============================================================================}

Constructor TFileTreeView.Create
   (
   AOwner          : TComponent
   );
  var ABitmap      : TBitmap;
  begin  
    inherited Create(AOwner);
    FFiles:=TList.Create;
    Style:=lbOwnerDrawFixed;
    FRelativePaths:=TRUE;
    ItemHeight:=17;
    ItemWidth:=20;
    ABitmap:=TBitmap.Create;
    ABitmap.Handle:=LoadBitmap(hInstance,'FILETREE');
    AddBitmaps(ABitmap,2);
    ABitmap.Free;
  end;

Destructor TFileTreeView.Destroy;
  var Cnt          : Integer;
  begin
    for Cnt:=0 to FFiles.Count-1 do TFileInfo(Files[Cnt]).Free;
    FFiles.Free;
    inherited Destroy;
  end;

Procedure TFileTreeView.Add
   (
   Const ALongName      : AnsiString;
   Const AShortName     : AnsiString;
   Const ARelLongName   : AnsiString;
   Const ARelShortName  : AnsiString;
   ARef                 : LongInt;
   Ptr                  : Pointer = nil;
   //brovak
   Comment              : AnsiString = ''
   );
  var AInfo        : TFileInfo;
  begin
    BeginUpdate;
    AInfo:=TFileInfo.Create(ALongName,AShortName,ARelLongName,ARelShortName,ARef,Ptr,Comment);
    FFiles.Add(AInfo);
    EndUpdate;
  end;

Procedure TFileTreeView.UpdateTree;
  var APos         : Integer;
      APath        : AnsiString;
      TreeInfo     : TFileTree;
      Cnt          : Integer;
      AInfo        : TFileInfo;
      IsAbsolute   : Boolean;
      StartTree    : TTreeEntry;
  Function FindCreate
     (
     Const AName   : AnsiString;
     Info          : TFileTree;
     FileInfo      : TFileInfo;
     AIndex        : Integer;
     AValid        : Boolean;
     ADirectory    : Boolean
     )
     : TFileTree;
    begin
      Result:=TFileTree(Info.Find(AName));
      if Result=NIL then begin
        Result:=TFileTree.Create(AName,FileInfo,AIndex,AValid,ADirectory);
        Info.Add(Result);
        Info.SortByCaption(False);
      end;
    end;
  Procedure UpdateExistance
     (
     Tree          : TFileTree;
     CurPath       : AnsiString
     );
    var Cnt        : Integer;
        ATree      : TFileTree;
    Procedure SetInvalid
       (
       ATree       : TFileTree
       );
      var Cnt      : Integer;
      begin
        ATree.Valid:=FALSE;
        for Cnt:=0 to ATree.Count-1 do SetInvalid(TFileTree(ATree.Items[Cnt]));
      end;
    begin
      if CurPath<>'' then begin
        if Tree.IsDirectory then Tree.Valid:=Tree.Valid and DirectoryExists(CurPath+'\')
        else Tree.Valid:=Tree.Valid and FileExists(CurPath);
        if not Tree.Valid then begin
          SetInvalid(Tree);
          Exit;
        end;  
      end;
      for Cnt:=0 to Tree.Count-1 do begin
        ATree:=TFileTree(Tree.Items[Cnt]);
        if ATree=FRelativeTree then UpdateExistance(ATree,FRelativeTo)
        else if CurPath='' then UpdateExistance(ATree,ATree.Caption)
        else if CurPath[Length(CurPath)]='\' then UpdateExistance(ATree,CurPath+ATree.Caption)
        else UpdateExistance(ATree,CurPath+'\'+ATree.Caption);
      end;
    end;
  begin
    Tree.Clear;
    FRelativeTree:=NIL;
    for Cnt:=0 to FFiles.Count-1 do begin
      AInfo:=FFiles[Cnt];
      if Assigned(FOnFilterItems) and not OnFilterItems(AInfo) then Continue;
      APath:=AInfo.Name(FShortPaths,FRelativePaths);
      IsAbsolute:=not FRelativePaths or (Pos(':',APath)<>0);
      if IsPrefix('\\',APath,FALSE) then begin
        IsAbsolute:=not FRelativePaths;
        APos:=Pos('\',Copy(APath,3,Length(APath)-3))+2
      end
      else if IsPrefix('\',APath,FALSE) then APos:=1
      else APos:=Pos('\',APath);
      if IsAbsolute then StartTree:=Tree
      else StartTree:=FRelativeTree;
      if StartTree=NIL then begin
        FRelativeTree:=FindCreate('~.~~~',TFileTree(Tree),NIL,0,TRUE,TRUE);
        StartTree:=FRelativeTree;
      end;
      if APos=0 then TreeInfo:=TFileTree(StartTree)
      else begin
        TreeInfo:=FindCreate(Copy(APath,1,APos-1),TFileTree(StartTree),NIL,0,TRUE,TRUE);
        Delete(APath,1,APos);
      end;
      APos:=Pos('\',APath);
      while APos<>0 do begin
        if APos=1 then Delete(APath,1,1)
        else begin
          TreeInfo:=FindCreate(Copy(APath,1,APos-1),TreeInfo,NIL,0,TRUE,TRUE);
          Delete(APath,1,APos);
        end;
        APos:=Pos('\',APath);
      end;
      if not FOLEBitmaps then APos:=0
      else if IsAbsolute then APos:=GetOLEBitmap(AInfo.Name(FShortPaths,FRelativePaths))
      else APos:=GetOLEBitmap(FRelativeTo+'\'+AInfo.Name(FShortPaths,FRelativePaths));
      if APath<>'' then TreeInfo.Add(TFileTree.Create(APath,AInfo,APos,AInfo.Valid,FALSE));
//    Tree.SortByCaption(TRUE);
    end;
//    Tree.SortByCaption(TRUE);
    if (FRelativeTree<>NIL) and (FRelativeTree.Caption='~.~~~') then
        FRelativeTree.Caption:=Format(FRelativeName,[FRelativeTo]);
    UpdateExistance(TFileTree(Tree),'');
  end;

Procedure TFileTreeView.SetRelativePaths
   (
   ARelative       : Boolean
   );
  begin
    if ARelative<>FRelativePaths then begin
      BeginUpdate;
      FRelativePaths:=ARelative;
      UpdateTree;
      EndUpdate;
    end;
  end;

Procedure TFileTreeView.SetShortPaths
   (
   AShort          : Boolean
   );
  begin
    if AShort<>FShortPaths then begin
      BeginUpdate;
      FShortPaths:=AShort;
      UpdateTree;
      EndUpdate;
    end;  
  end;
   
Procedure TFileTreeView.DoDrawBitmap
   (
   Index           : Integer;
   ARect           : TRect;
   State           : TOwnerDrawState
   );
  var AItem        : TFileTree;
      AIndex       : Integer;
      Source       : TImageList;
  begin
    AItem:=TFileTree(Tree.ExpandedItems[Index]);
    with AItem,ARect do begin
      if Bitmap>=OLEBitmapOffset then begin
        Source:=FImages;
        AIndex:=Bitmap-OLEBitmapOffset;
      end
      else Begin
        Source:=Images;
        AIndex:=GetImageIndex(Bitmap);
      end;
      if AIndex>0 then Source.Draw(Canvas,Left+(ItemWidth-15) Div 2,
          Top+(ItemHeight-15) Div 2,AIndex)
      else begin
        if (Count>0) and Expanded then Source.Draw(Canvas,Left+(ItemWidth-15) Div 2,
          Top+(ItemHeight-15) Div 2,AIndex+1)
        else Source.Draw(Canvas,Left+(ItemWidth-15) Div 2,
          Top+(ItemHeight-15) Div 2,AIndex);
      end;
      if not Valid then Images.Draw(Canvas,Left+(ItemWidth-15) Div 2,
          Top+(ItemHeight-15) Div 2,GetImageIndex(2));
    end;
  end;

Procedure TFileTreeView.DoDrawCaption
   (
   Index           : Integer;
   ARect           : TRect;
   State           : TOwnerDrawState
   );
  var AItem        : TFileTree;
  begin
    AItem:=TFileTree(Tree.ExpandedItems[Index]);
    with ARect,Canvas do begin
      ARect:=Rect(Left,Top,Left+4+TextWidth(AItem.Caption),Bottom);
      TextRect(ARect,Left+2,(Top+Bottom-TextHeight(AItem.Caption)) Div 2,
          AItem.Caption);
      if odFocused in State then DrawFocusRect(ARect);
    end;
  end;

Procedure TFileTreeView.DoUpdate;
  Procedure DoMerge
     (
     Tree          : TTreeList
     );
    var Cnt        : Integer;
        AIndex     : Integer;
        AItem      : TTreeList;
    begin
      if (Tree.Parent<>NIL)
          and (Tree<>FRelativeTree)
          and (Tree.Count=1)
          and (TFileTree(Tree[0]).IsDirectory) then begin
        AIndex:=Tree.Parent.IndexOf(Tree);
        Tree.Parent.Delete(AIndex);
        AItem:=Tree[0];
        Tree.Delete(0);
        Tree.Parent.Insert(AIndex,AItem);
        TFileTree(Tree.Parent).Valid:=TFileTree(Tree.Parent).Valid and TFileTree(AItem).Valid;
        TFileTree(AItem).Caption:=TFileTree(Tree).Caption+'\'+TFileTree(AItem).Caption;
        Tree.Free;
        DoMerge(Tree.Parent[AIndex]);
      end
      else for Cnt:=0 to Tree.Count-1 do DoMerge(Tree.Items[Cnt]);
    end; 
  begin
    UpdateTree;
    if FMergeEmptyDirs then DoMerge(Tree);
  end;

Procedure TFileTreeView.Clear;
  var Cnt          : Integer;
  begin
    BeginUpdate;
    Tree.Clear;
    for Cnt:=0 to FFiles.Count-1 do TFileInfo(FFiles[Cnt]).Free;
    FFiles.Clear;
    FRelativeTree:=NIL;
    EndUpdate;
  end;

{******************************************************************************+
  Function TFileTreeView.GetImageIndex
--------------------------------------------------------------------------------
  Determines the index of the image in the image-list. Is overwritten here
  because FileTreeView has its own Bitmaps and TreeListView would write the
  caption in the bitmap-area because it cannot find an image for the bitmap.
+******************************************************************************}
Function TFileTreeView.GetImageIndex
   (
   ABitmap         : Integer
   )
   : Integer;
  begin
    Result:=inherited GetImageIndex(ABitmap);
    if Result<0 then Result:=0;
  end;

{******************************************************************************+
  Function TFileTreeView.FileInfoToTree
--------------------------------------------------------------------------------
  Determines the tree-entry that contains the specified file-info. Returns
  NIL if not tree-entry exists.
+******************************************************************************}
Function TFileTreeView.FileInfoToTree(AInfo:TFileInfo):TFileTree;
  Function SearchItem(Tree:TFileTree):TFileTree;
  var Cnt        : Integer;
  begin
    for Cnt:=0 to Tree.Count-1 do begin
      Result:=SearchItem(TFileTree(Tree[Cnt]));
      if Result<>NIL then Exit
      else if TFileTree(Tree[Cnt]).FileInfo=AInfo then begin
        Result:=TFileTree(Tree[Cnt]);
        Exit;
      end;  
    end;
    Result:=NIL;
  end;
begin
  Result:=SearchItem(TFileTree(Tree));
end;

Function TFileTreeView.PathOfTree
   (
   ATree         : TFileTree
   )
   : AnsiString;
var Prev         : TFileTree;
begin
  Prev:=ATree;
  if Prev.FIsDir then begin
    // recursively collect paht-names
    Result:='';
    while Prev.Parent<>NIL do begin
      if Prev=FRelativeTree then Result:=FRelativeTo+'\'+Result
      else Result:=Prev.Caption+'\'+Result;
      Prev:=TFileTree(Prev.Parent);
    end;
    Result:=ExpandFileName(Result);
  end
  else Result:=ExtractFilePath(Prev.FileInfo.LongName);
end;

{===============================================================================
| TFileInfo
+==============================================================================}

Constructor TFileInfo.Create
   (
   Const ALongName      : AnsiString;
   Const AShortName     : AnsiString;
   Const ARelLongName   : AnsiString;
   Const ARelShortName  : AnsiString;
   ARef                 : LongInt;
   Ptr                  : Pointer = nil;
   //brovak
   Const AComment       : Ansistring = ''
   );
  begin
    inherited Create;
    FLongName:=ALongName;
    FShortName:=AShortName;
    FRelLongName:=ARelLongName;
    FRelShortName:=ARelShortName;
    FReference:=ARef;
    FValid:=TRUE;
    FPtr := Ptr;
    //brovak
    FComment:=AComment;
  end;

Function TFileInfo.Name
   (
   AShort          : Boolean;
   ARelative       : Boolean
   )
   : AnsiString;
  begin
    if AShort then begin
      if ARelative then Result:=RelativeShortName
      else Result:=ShortName;
    end
    else begin
      if ARelative then Result:=RelativeLongName
      else Result:=LongName;
    end;
  end;
    
{===============================================================================
| TFileTree
+==============================================================================}

Constructor TFileTree.Create
   (
   AName           : AnsiString;
   AInfo           : TFileInfo;
   ABitmap         : Integer;
   AValid          : Boolean;
   AIsDir          : Boolean
   );
  begin
    inherited Create;
    FFileInfo:=AInfo;
    Caption:=AName;
    Bitmap:=ABitmap;
    FValid:=AValid;
    FIsDir:=AIsDir;
  end;

{===============================================================================
| Initialisierungs- und Terminierungscode
+==============================================================================}

{$IFNDEF WIN32}
Procedure ExitUnit; Far;
  begin
    FImages.Free;
    FExtensions.Free;
  end;
{$ENDIF}  

Procedure TFileTreeView.SetRelativeTo(const Value: AnsiString);
begin
  FRelativeTo:=Value;
  if IsSuffix('\',FRelativeTo,False) then
    FRelativeTo:=Copy(FRelativeTo,1,Length(FRelativeTo)-1);
end;

Initialization
  begin
    FImages:=NIL;
    {$IFDEF WIN32}
    FImages:=TImageList.CreateSize(16,16);
    {$ELSE}
    AddExitProc(ExitUnit);
    FImages:=TImageList.Create(16,16);
    {$ENDIF}
    FExtensions:=TStringList.Create;
    FExtensions.Sorted:=TRUE;
  end;

{$IFDEF WIN32}
Finalization
  begin
    FImages.Free;
    FExtensions.Free;
  end;
{$ENDIF}

end.
