{**********************************************}
{  TCustomChart (or derived) Editor Dialog     }
{  Copyright (c) 1996-2000 by David Berneda    }
{**********************************************}
{$I teedefs.inc}
unit TeeEdiLege;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QComCtrls, QExtCtrls,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls,
     {$ENDIF}
     Chart, TeeCustomShapeEditor, TeCanvas, TeePenDlg;

type
  TFormTeeLegend = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    L12: TLabel;
    L7: TLabel;
    CBShow: TCheckBox;
    CBInverted: TCheckBox;
    CBLegendStyle: TComboBox;
    CBLegStyle: TComboBox;
    CBBoxes: TCheckBox;
    BDivLines: TButtonPen;
    TabSheet2: TTabSheet;
    L11: TLabel;
    SEColWi: TEdit;
    UDColWi: TUpDown;
    Label2: TLabel;
    CBColWUnits: TComboBox;
    Label3: TLabel;
    CBSymbolPos: TComboBox;
    TabSheet3: TTabSheet;
    CBResizeChart: TCheckBox;
    L10: TLabel;
    SETopPos: TEdit;
    UDTopPos: TUpDown;
    L1: TLabel;
    SEMargin: TEdit;
    UDMargin: TUpDown;
    RGPosition: TRadioGroup;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    ECustLeft: TEdit;
    UDLeft: TUpDown;
    Label5: TLabel;
    ECustTop: TEdit;
    UDTop: TUpDown;
    Label1: TLabel;
    EVertSpacing: TEdit;
    UDVertSpacing: TUpDown;
    CBCustPos: TCheckBox;
    CBFontColor: TCheckBox;
    CBContinuous: TCheckBox;
    procedure SEMarginChange(Sender: TObject);
    procedure CBLegendStyleChange(Sender: TObject);
    procedure SEColWiChange(Sender: TObject);
    procedure SETopPosChange(Sender: TObject);
    procedure CBLegStyleChange(Sender: TObject);
    procedure CBShowClick(Sender: TObject);
    procedure CBResizeChartClick(Sender: TObject);
    procedure CBInvertedClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBBoxesClick(Sender: TObject);
    procedure EVertSpacingChange(Sender: TObject);
    procedure CBColWUnitsChange(Sender: TObject);
    procedure CBSymbolPosChange(Sender: TObject);
    procedure RGPositionClick(Sender: TObject);
    procedure ECustLeftChange(Sender: TObject);
    procedure ECustTopChange(Sender: TObject);
    procedure CBCustPosClick(Sender: TObject);
    procedure CBFontColorClick(Sender: TObject);
    procedure CBContinuousClick(Sender: TObject);
  private
    { Private declarations }
    CreatingForm : Boolean;
    TheLegend    : TChartLegend;
    ITeeObject   : TFormTeeShape;
    Function CanChangePos:Boolean;
    Procedure EnableCustomPosition;
    Procedure EnableMarginControls;
  public
    { Public declarations }
    Constructor CreateLegend(Owner:TComponent; ALegend:TChartLegend);
  end;

implementation

{$R *.dfm}
Uses TeEngine;

Constructor TFormTeeLegend.CreateLegend(Owner:TComponent; ALegend:TChartLegend);
begin
  inherited Create(Owner);
  CreatingForm:=True;
  TheLegend:=ALegend;
  ITeeObject:=InsertTeeObjectForm(PageControl1,TheLegend);
end;

procedure TFormTeeLegend.SEMarginChange(Sender: TObject);
begin
  if not CreatingForm then
  With TheLegend do
  if Vertical then HorizMargin:=UDMargin.Position
              else VertMargin:=UDMargin.Position;
end;

procedure TFormTeeLegend.CBLegendStyleChange(Sender: TObject);
begin
  TheLegend.LegendStyle:=TLegendStyle(CBLegendStyle.ItemIndex);
end;

procedure TFormTeeLegend.SEColWiChange(Sender: TObject);
begin
  if not CreatingForm then TheLegend.Symbol.Width:=UDColWi.Position;
end;

procedure TFormTeeLegend.SETopPosChange(Sender: TObject);
begin
  if not CreatingForm then TheLegend.TopPos:=UDTopPos.Position;
end;

procedure TFormTeeLegend.CBLegStyleChange(Sender: TObject);
begin
  TheLegend.TextStyle:=TLegendTextStyle(CBLegStyle.ItemIndex);
end;

procedure TFormTeeLegend.CBShowClick(Sender: TObject);
begin
  TheLegend.Visible:=CBShow.Checked;
end;

procedure TFormTeeLegend.CBResizeChartClick(Sender: TObject);
begin
  TheLegend.ResizeChart:=CBResizeChart.Checked;
end;

procedure TFormTeeLegend.CBInvertedClick(Sender: TObject);
begin
  TheLegend.Inverted:=CBInverted.Checked;
end;

procedure TFormTeeLegend.FormShow(Sender: TObject);
begin
  With TheLegend do
  begin
    With Symbol do
    begin
      UDColWi.Position       :=Width;
      if WidthUnits=lcsPercent then CBColWUnits.ItemIndex:=0
                               else CBColWUnits.ItemIndex:=1;
      if Position=spLeft then CBSymbolPos.ItemIndex:=0
                         else CBSymbolPos.ItemIndex:=1;
      CBContinuous.Checked:=Continuous;
    end;
    UDTopPos.Position      :=TopPos;

    CBCustPos.Checked      :=CustomPosition;
    UDLeft.Position        :=Left;
    ECustLeft.Text         :=IntToStr(Left);
    UDTop.Position         :=Top;
    ECustTop.Text          :=IntToStr(Top);

    CBFontColor.Checked    :=FontSeriesColor;
    CBResizeChart.Checked  :=ResizeChart;
    CBLegendStyle.ItemIndex:=Ord(LegendStyle);
    CBLegStyle.ItemIndex   :=Ord(TextStyle);
    CBShow.Checked         :=Visible;
    CBInverted.Checked     :=Inverted;
    CBBoxes.Checked        :=CheckBoxes;
    RGPosition.ItemIndex   :=Ord(Alignment);
    UDVertSpacing.Position :=VertSpacing;
    BDivLines.LinkPen(DividingLines);
    EnableMarginControls;
    EnableCustomPosition;
  end;
  ITeeObject.RefreshControls(TheLegend);
  CreatingForm:=False;
end;

Procedure TFormTeeLegend.EnableCustomPosition;
var tmp : Boolean;
    Old : Boolean;
begin
  Old:=CreatingForm;
  CreatingForm:=True;
  tmp:=TheLegend.CustomPosition;
  ECustLeft.Enabled :=tmp;
  ECustTop.Enabled  :=tmp;
  UDLeft.Enabled    :=tmp;
  UDTop.Enabled     :=tmp;
  if tmp then
  begin
    UDLeft.Position :=TheLegend.Left;
    UDTop.Position  :=TheLegend.Top;
  end;
  CBResizeChart.Enabled:=not tmp;
  UDMargin.Enabled:=not tmp;
  SEMargin.Enabled:=not tmp;
  SETopPos.Enabled:=not tmp;
  UDTopPos.Enabled:=not tmp;
  CreatingForm:=Old;
end;

Procedure TFormTeeLegend.EnableMarginControls;
begin
  With TheLegend do
  if Vertical then UDMargin.Position:=HorizMargin
              else UDMargin.Position:=VertMargin;
end;

procedure TFormTeeLegend.FormCreate(Sender: TObject);
begin
  CreatingForm:=True;
  PageControl1.ActivePage:=TabSheet1;
end;

procedure TFormTeeLegend.CBBoxesClick(Sender: TObject);
begin
  TheLegend.CheckBoxes:=CBBoxes.Checked;
end;

procedure TFormTeeLegend.EVertSpacingChange(Sender: TObject);
begin
  if not CreatingForm then
  TheLegend.VertSpacing:=UDVertSpacing.Position;
end;

procedure TFormTeeLegend.CBColWUnitsChange(Sender: TObject);
begin
  if CBColWUnits.ItemIndex=0 then
     TheLegend.Symbol.WidthUnits:=lcsPercent
  else
     TheLegend.Symbol.WidthUnits:=lcsPixels;
end;

procedure TFormTeeLegend.CBSymbolPosChange(Sender: TObject);
begin
  if CBSymbolPos.ItemIndex=0 then TheLegend.Symbol.Position:=spLeft
                             else TheLegend.Symbol.Position:=spRight;
end;

procedure TFormTeeLegend.RGPositionClick(Sender: TObject);
begin
  TheLegend.Alignment:=TLegendAlignment(RGPosition.ItemIndex);
end;

Function TFormTeeLegend.CanChangePos:Boolean;
begin
  result:=(not CreatingForm) and (TheLegend.CustomPosition);
end;

procedure TFormTeeLegend.ECustLeftChange(Sender: TObject);
begin
  if CanChangePos then TheLegend.Left:=UDLeft.Position;
end;

procedure TFormTeeLegend.ECustTopChange(Sender: TObject);
begin
  if CanChangePos then TheLegend.Top:=UDTop.Position;
end;

procedure TFormTeeLegend.CBCustPosClick(Sender: TObject);
begin
  TheLegend.CustomPosition:=CBCustPos.Checked;
  EnableCustomPosition;
end;

procedure TFormTeeLegend.CBFontColorClick(Sender: TObject);
begin
  TheLegend.FontSeriesColor:=CBFontColor.Checked
end;

procedure TFormTeeLegend.CBContinuousClick(Sender: TObject);
begin
  TheLegend.Symbol.Continuous:=CBContinuous.Checked
end;

end.
