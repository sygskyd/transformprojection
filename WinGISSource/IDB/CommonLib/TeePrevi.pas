{********************************************}
{   PrintPreview Form                        }
{   Copyright (c) 1996-2000 by David Berneda }
{********************************************}
{$I teedefs.inc}
unit TeePrevi;

interface

uses
  {$IFDEF LINUX}
  LibC,
  {$ELSE}
  Windows, Messages,
  {$ENDIF}
  SysUtils, Classes,
  {$IFDEF CLX}
  QGraphics, QControls, QForms, QDialogs, QExtCtrls, QStdCtrls, QComCtrls,
  TeePenDlg,  
  {$ELSE}
  Graphics, Controls, Forms, Dialogs, ExtCtrls, StdCtrls, ComCtrls,
  {$ENDIF}
  TeeProcs, TeePreviewPanel, Chart, TeeNavigator
  {$IFDEF CLX}
  ,TeCanvas
  {$ENDIF}
  {$IFDEF D6}
  ,Types
  {$ENDIF};


{$IFDEF CLX}
type
  TPrinterSetupDialog=class(TDialog);
  TPrintDialog=class(TDialog)
  public
      Copies,
      FromPage,
      MinPage,
      ToPage,
      MaxPage  : Integer;
  end;
{$ENDIF}

type
  TChartPreview = class(TForm)
    Panel1: TPanel;
    CBPrinters: TComboBox;
    Label1: TLabel;
    BSetupPrinter: TButton;
    Panel2: TPanel;
    Orientation: TRadioGroup;
    GBMargins: TGroupBox;
    SETopMa: TEdit;
    SELeftMa: TEdit;
    SEBotMa: TEdit;
    SERightMa: TEdit;
    PrinterSetupDialog1: TPrinterSetupDialog;
    ChangeDetailGroup: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    BClose: TButton;
    BPrint: TButton;
    UDLeftMa: TUpDown;
    UDTopMa: TUpDown;
    UDRightMa: TUpDown;
    UDBotMa: TUpDown;
    TeePreviewPanel1: TTeePreviewPanel;
    ChartPageNavigator1: TChartPageNavigator;
    Resolution: TTrackBar;
    PanelMargins: TPanel;
    BReset: TButton;
    ShowMargins: TCheckBox;
    Panel4: TPanel;
    CBProp: TCheckBox;
    PrintDialog1: TPrintDialog;
    procedure FormShow(Sender: TObject);
    procedure BSetupPrinterClick(Sender: TObject);
    procedure CBPrintersChange(Sender: TObject);
    procedure OrientationClick(Sender: TObject);
    procedure SETopMaChange(Sender: TObject);
    procedure SERightMaChange(Sender: TObject);
    procedure SEBotMaChange(Sender: TObject);
    procedure SELeftMaChange(Sender: TObject);
    procedure ShowMarginsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BResetClick(Sender: TObject);
    procedure BPrintClick(Sender: TObject);
    procedure BCloseClick(Sender: TObject);
    procedure CBPropClick(Sender: TObject);
    procedure TeePreviewPanel1ChangeMargins(Sender: TObject;
      DisableProportional: Boolean; const NewMargins: TRect);
    procedure ChartPageNavigator1ButtonClicked(Index: TChartNavigateBtn);
    procedure TrackBar1Change(Sender: TObject);
  private
    { Private declarations }
    ChangingMargins : Boolean;
    ChangingProp    : Boolean;
    OldMargins      : TRect;
    Procedure ResetMargins;
    Procedure CheckOrientation;
    procedure ChangeMargin(UpDown:TUpDown; Var APos:Integer; OtherSide:Integer);
  public
    { Public declarations }
    Function PreviewPanel : TTeePreviewPanel;
  end;

Procedure ChartPreview(AOwner:TComponent; TeePanel:TCustomTeePanel);

implementation

{$R *.dfm}

{$IFNDEF CLX}
Uses Printers;
{$ENDIF}

Procedure ChartPreview(AOwner:TComponent; TeePanel:TCustomTeePanel);
Begin
  with TChartPreview.Create(AOwner) do
  try
    TeePreviewPanel1.Panel:=TeePanel;
    ShowModal;
  finally
    Free;
    TeePanel.Repaint;
  end;
End;

{ TChartPreview }
Procedure TChartPreview.ResetMargins;
begin
  With TeePreviewPanel1 do
  if Assigned(Panel) then
  begin
    if Panel.PrintProportional and (Printer.Printers.Count>0) then
    begin
      OldMargins:=Panel.CalcProportionalMargins;
      Panel.PrintMargins:=OldMargins;
    end;
    TeePreviewPanel1ChangeMargins(Self,False,Panel.PrintMargins);
    Invalidate;
  end;
end;

procedure TChartPreview.FormShow(Sender: TObject);
begin
  Screen.Cursor:=crDefault;

  if Tag<>0 then TeePreviewPanel1.Panel:=TCustomTeePanel(Tag);

  CBPrinters.Items:=Printer.Printers;
  if Printer.Printers.Count>0 then
  begin
    CBPrinters.ItemIndex:=Printer.PrinterIndex;

    {$IFNDEF TEEOCX}
    Printer.Orientation:=poLandscape;
    {$ENDIF}
    CheckOrientation;
  end
  else
  begin
    EnableControls(False,[BPrint,BSetupPrinter,Orientation]);
  end;

  With TeePreviewPanel1 do
  if Assigned(Panel) then
  begin
    CBProp.Checked:=Panel.PrintProportional;
    Resolution.Position:=Panel.PrintResolution+100;
    OldMargins:=Panel.PrintMargins;
    ResetMargins;
    if Panel is TCustomChart then
       ChartPageNavigator1.Chart:=TCustomChart(Panel)
    else
       ChartPageNavigator1.Visible:=False;
  end
  else
  begin
    CBProp.Enabled:=False;
    Resolution.Enabled:=False;
    GBMargins.Enabled:=False;
    BPrint.Enabled:=False;
  end;
end;

procedure TChartPreview.BSetupPrinterClick(Sender: TObject);
begin
  PrinterSetupDialog1.Execute;
  CBPrinters.Items:=Printer.Printers;
  CBPrinters.ItemIndex:=Printer.PrinterIndex;
  CheckOrientation;
  TeePreviewPanel1.Invalidate;
end;

procedure TChartPreview.CBPrintersChange(Sender: TObject);
begin
  Printer.PrinterIndex:=CBPrinters.ItemIndex;
  CheckOrientation;
  OrientationClick(Self);
end;

procedure TChartPreview.OrientationClick(Sender: TObject);
begin
  Printer.Orientation:=TPrinterOrientation(Orientation.ItemIndex);
  ResetMargins;
  TeePreviewPanel1.Invalidate;
end;

procedure TChartPreview.ChangeMargin(UpDown:TUpDown; Var APos:Integer; OtherSide:Integer);
begin
  if Showing then
  begin
    if UpDown.Position+OtherSide<100 then
    begin
      APos:=UpDown.Position;
      if not ChangingMargins then
      With TeePreviewPanel1 do
      Begin
        Invalidate;
        BReset.Enabled:=not EqualRect(Panel.PrintMargins,OldMargins);
        CBProp.Checked:=False;
      end;
    end
    else UpDown.Position:=APos;
  end;
end;

procedure TChartPreview.SETopMaChange(Sender: TObject);
begin
  if Showing then with TeePreviewPanel1.Panel.PrintMargins do ChangeMargin(UDTopMa,Top,Bottom);
end;

procedure TChartPreview.SERightMaChange(Sender: TObject);
begin
  if Showing then with TeePreviewPanel1.Panel.PrintMargins do ChangeMargin(UDRightMa,Right,Left);
end;

procedure TChartPreview.SEBotMaChange(Sender: TObject);
begin
  if Showing then with TeePreviewPanel1.Panel.PrintMargins do ChangeMargin(UDBotMa,Bottom,Top);
end;

procedure TChartPreview.SELeftMaChange(Sender: TObject);
begin
  if Showing then with TeePreviewPanel1.Panel.PrintMargins do ChangeMargin(UDLeftMa,Left,Right);
end;

procedure TChartPreview.ShowMarginsClick(Sender: TObject);
begin
  TeePreviewPanel1.Margins.Visible:=ShowMargins.Checked;
end;

procedure TChartPreview.FormCreate(Sender: TObject);
begin
  ChangingMargins:=True;
  ChangingProp:=False;
end;

procedure TChartPreview.BResetClick(Sender: TObject);
begin
  With TeePreviewPanel1 do
  Begin
    Panel.PrintMargins:=OldMargins;
    TeePreviewPanel1ChangeMargins(Self,False,Panel.PrintMargins);
    Invalidate;
  end;
  BReset.Enabled:=False;
end;

Procedure TChartPreview.CheckOrientation;
Begin
  Orientation.ItemIndex:=Ord(Printer.Orientation);
End;

procedure TChartPreview.BPrintClick(Sender: TObject);

  procedure PrintPages;
  begin

  end;

begin
  if (TeePreviewPanel1.Panel is TCustomChart) and
     (TCustomChart(TeePreviewPanel1.Panel).NumPages>1) then
    With PrintDialog1 do
    begin
      FromPage:=1;
      MinPage:=FromPage;
      ToPage:=TCustomChart(TeePreviewPanel1.Panel).NumPages;
      MaxPage:=ToPage;
      if Execute then
         TCustomChart(TeePreviewPanel1.Panel).PrintPages(FromPage,ToPage);
    end
  else
  begin
    Screen.Cursor:=crHourGlass;
    try
      TeePreviewPanel1.Print;
    finally
      Screen.Cursor:=crDefault;
    end;
  end;
end;

procedure TChartPreview.BCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TChartPreview.CBPropClick(Sender: TObject);
begin
  if not ChangingProp then
  begin
    TeePreviewPanel1.Panel.PrintProportional:=CBProp.Checked;
    ResetMargins;
  end;
end;

procedure TChartPreview.TeePreviewPanel1ChangeMargins(Sender: TObject;
  DisableProportional: Boolean; const NewMargins: TRect);
begin
  ChangingMargins:=True;
  try
    UDLeftMa.Position :=NewMargins.Left;
    UDRightMa.Position:=NewMargins.Right;
    UDTopMa.Position  :=NewMargins.Top;
    UDBotMa.Position  :=NewMargins.Bottom;
    if DisableProportional then
    begin
      TeePreviewPanel1.Panel.PrintProportional:=False;
      ChangingProp:=True;
      CBProp.Checked:=False;
      ChangingProp:=False;
    end;
  finally
    ChangingMargins:=False;
  end;
end;

procedure TChartPreview.ChartPageNavigator1ButtonClicked(
  Index: TChartNavigateBtn);
begin
  TeePreviewPanel1.Invalidate;
end;

procedure TChartPreview.TrackBar1Change(Sender: TObject);
begin
  With TeePreviewPanel1 do
  Begin
    Panel.PrintResolution:=Resolution.Position-100;
    Invalidate;
  end;
end;

function TChartPreview.PreviewPanel: TTeePreviewPanel;
begin
  result:=TeePreviewPanel1;
end;

end.
