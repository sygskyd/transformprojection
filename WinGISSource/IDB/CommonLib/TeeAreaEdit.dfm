�
 TAREASERIESEDITOR 0H  TPF0TAreaSeriesEditorAreaSeriesEditorLeftTopActiveControl
BAreaColorBorderIcons BorderStylebsDialogClientHeight� ClientWidth1Color	clBtnFace
ParentFont	OldCreateOrder	OnShowFormShowPixelsPerInch`
TextHeight TRadioGroupRGMultiAreaLeftTopPWidth� HeightIHelpContext+Caption!124Items.Strings!125!126!127 TabOrder OnClickRGMultiAreaClick  	TGroupBox	GroupBox2LeftTop Width)HeightICaption!118TabOrder TLabelLabel4LeftTopWidth1Height	AlignmenttaRightJustifyAutoSizeCaption!119FocusControlCBAreaBrush  	TCheckBoxCBStairsLeftTop,WidthMHeightHelpContext�Caption!120TabOrder OnClickCBStairsClick  	TComboBoxCBAreaBrushLeft<TopWidthvHeightHelpContext4StylecsDropDownListDropDownCount
ItemHeightItems.StringsSolidClear
HorizontalVerticalDiagonal
B.DiagonalCross
Diag.Cross TabOrderOnChangeCBAreaBrushChange  
TButtonPenBAreaLinesPenLeft� Top)WidthYHelpContext5Caption!123TabOrder  
TButtonPenBAreaLinePenLeft� TopWidthYHelpContextCaption!122TabOrder  	TCheckBoxCBInvStairsLeftdTop,WidthRHeightHelpContext�Caption!121TabOrderOnClickCBInvStairsClick   	TGroupBox	GroupBox1Left� TopPWidthiHeightICaption!128TabOrder 	TCheckBoxCBColorEachLeft	TopWidthXHeightHelpContext� Caption!129TabOrder OnClickCBColorEachClick  TButtonColor
BAreaColorLeft	Top(HelpContext�Caption!130TabOrder   	TGroupBox	GroupBox3LeftTop� Width)Height'TabOrder TLabelLabel1LefthTopWidth.Height	AlignmenttaRightJustifyAutoSizeCaption!132FocusControlEOrigin  	TCheckBoxCBUseOriginLeftTopWidthaHeightHelpContextNCaption!131TabOrder OnClickCBUseOriginClick  TEditEOriginLeft� TopWidth9HeightHelpContextOTabOrderText0OnChangeEOriginChange  TUpDownUDOriginLeft� TopWidthHeight	AssociateEOriginMinЊMax0uPosition TabOrderWrap   TMlgSectionMlgSection1SectionBusinessGraphicsLeft� TopX   