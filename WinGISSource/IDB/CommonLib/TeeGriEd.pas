{**********************************************}
{   TGrid3DSeries Editor Dialog                }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeGriEd;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, ExtCtrls, TeeProcs, Chart, TeeSurfa, ComCtrls, TeCanvas;

type
  TGrid3DSeriesEditor = class(TForm)
    GroupBox1: TGroupBox;
    RGColor: TRadioButton;
    RGRange: TRadioButton;
    RGPalette: TRadioButton;
    Label4: TLabel;
    SEPalette: TEdit;
    UDPalette: TUpDown;
    GroupBox2: TGroupBox;
    SEXGrid: TEdit;
    UDXGrid: TUpDown;
    SEZGrid: TEdit;
    UDZGrid: TUpDown;
    Label1: TLabel;
    Label5: TLabel;
    LDepth: TLabel;
    EDepth: TEdit;
    UDDepth: TUpDown;
    BColor: TButtonColor;
    BFromColor: TButtonColor;
    BToColor: TButtonColor;
    CBIrreg: TCheckBox;
    Label2: TLabel;
    CBPalStyle: TComboBox;
    BMidColor: TButtonColor;
    procedure FormShow(Sender: TObject);
    procedure SEZGridChange(Sender: TObject);
    procedure SEPaletteChange(Sender: TObject);
    procedure RGColorClick(Sender: TObject);
    procedure RGRangeClick(Sender: TObject);
    procedure RGPaletteClick(Sender: TObject);
    procedure EDepthChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBIrregClick(Sender: TObject);
    procedure CBPalStyleChange(Sender: TObject);
  private
    { Private declarations }
    Grid3D : TCustom3DPaletteSeries;
    Creating : Boolean;
  public
    { Public declarations }
  end;

Procedure TeeInsertGrid3DForm(AParent:TControl; AGrid3D:TCustom3DPaletteSeries);

implementation

{$R *.dfm}
Uses TeeBrushDlg, TeeProco, TeeEdiSeri, TeePenDlg;

Procedure TeeInsertGrid3DForm(AParent:TControl; AGrid3D:TCustom3DPaletteSeries);
begin
  (AParent.Owner as TFormTeeSeries).InsertSeriesForm( TGrid3DSeriesEditor,
                                                     1,TeeMsg_Grid3D,
                                                     AGrid3D);
end;

type TCustom3DGridSeriesAccess=class(TCustom3DGridSeries);

procedure TGrid3DSeriesEditor.FormShow(Sender: TObject);
begin
  Grid3D:=TCustom3DPaletteSeries(Tag);
  if Grid3D is TCustom3DGridSeries then
  With TCustom3DGridSeriesAccess(Grid3D) do
  begin
    UDXGrid.Position:=NumXValues;
    UDZGrid.Position:=NumZValues;
    CBIrreg.Checked :=IrregularGrid;
    EnableControls(CanCreateValues,[UDXGrid,UDZGrid,SEXGrid,SEZGrid]);
  end
  else
  begin
    EnableControls(False,[SEXGrid,SEZGrid,UDXGrid,UDZGrid]);
    CBIrreg.Visible:=False;
  end;
  With Grid3D do
  begin
    UDDepth.Position:=TimesZOrder;
    UDPalette.Position:=PaletteSteps;
    CBPalStyle.ItemIndex:=Ord(PaletteStyle);

    RGRange.Checked:=UseColorRange;
    RGPalette.Checked:=UsePalette;
    RGColor.Checked:=(not UseColorRange) and (not UsePalette);
  end;
  BColor.LinkProperty(Grid3D,'SeriesColor');
  BFromColor.LinkProperty(Grid3D,'StartColor');
  BMidColor.LinkProperty(Grid3D,'MidColor');
  BToColor.LinkProperty(Grid3D,'EndColor');
  Creating:=False;
end;

procedure TGrid3DSeriesEditor.SEZGridChange(Sender: TObject);
begin
  if not Creating then
  if Grid3D is TCustom3DGridSeries then
  With TCustom3DGridSeries(Grid3D) do
     if (UDXGrid.Position<>NumXValues) or (UDZGrid.Position<>NumZValues) then
        CreateValues(UDXGrid.Position,UDZGrid.Position);
end;

procedure TGrid3DSeriesEditor.SEPaletteChange(Sender: TObject);
begin
  if not Creating then
  if SEPalette.Text<>'' then
  begin
    Grid3D.PaletteSteps:=UDPalette.Position;
    Grid3D.Repaint;
  end;
end;

procedure TGrid3DSeriesEditor.RGColorClick(Sender: TObject);
begin
  if not Creating then
  With Grid3D do
  begin
    ColorEachPoint:=True;
    ColorEachPoint:=False;
    UseColorRange:=False;
    UsePalette:=False;
  end;
end;

procedure TGrid3DSeriesEditor.RGRangeClick(Sender: TObject);
begin
  Grid3D.UseColorRange:=True;
  Grid3D.UsePalette:=False;
end;

procedure TGrid3DSeriesEditor.RGPaletteClick(Sender: TObject);
begin
  Grid3D.UseColorRange:=False;
  Grid3D.UsePalette:=True;
end;

procedure TGrid3DSeriesEditor.EDepthChange(Sender: TObject);
begin
  if not Creating then Grid3D.TimesZOrder:=UDDepth.Position;
end;

procedure TGrid3DSeriesEditor.FormCreate(Sender: TObject);
begin
  Creating:=True;
end;

procedure TGrid3DSeriesEditor.CBIrregClick(Sender: TObject);
begin
 TCustom3DGridSeries(Grid3D).IrregularGrid:=CBIrreg.Checked
end;

procedure TGrid3DSeriesEditor.CBPalStyleChange(Sender: TObject);
begin
  Grid3D.PaletteStyle:=TTeePaletteStyle(CBPalStyle.ItemIndex);
end;

initialization
  RegisterClass(TGrid3DSeriesEditor);
end.
