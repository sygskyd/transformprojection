{********************************************}
{     TeeChart Pro Charting Library          }
{ Copyright (c) 1995-2000 by David Berneda   }
{         All Rights Reserved                }
{********************************************}
{$I TeeDefs.inc}
unit TeeEdiPeri;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QExtCtrls, QStdCtrls,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, ExtCtrls, StdCtrls,
     {$ENDIF}
     TeEngine;

type
  TFormPeriod = class(TForm)
    ENum: TEdit;
    CBAll: TCheckBox;
    RBNum: TRadioButton;
    RBRange: TRadioButton;
    BChange: TButton;
    LCalc: TLabel;
    RGAlign: TRadioGroup;
    BLabel: TLabel;
    procedure FormShow(Sender: TObject);
    procedure RBRangeClick(Sender: TObject);
    procedure RBNumClick(Sender: TObject);
    procedure BChangeClick(Sender: TObject);
    procedure CBAllClick(Sender: TObject);
    procedure ENumChange(Sender: TObject);
    procedure RGAlignClick(Sender: TObject);
  private
    { Private declarations }
    IFunction     : TTeeFunction;
    TheIsDateTime : Boolean;
    Procedure EnableControls(Value:Boolean);
    Procedure SetLabels;
  public
    { Public declarations }
    BApply : TButton;
    procedure Apply;
    Procedure SetFunction(AFunction:TTeeFunction);
  end;

implementation

{$R *.dfm}
Uses TeeAxisIncr, TeeConst, TeeProcs, TeCanvas;

Procedure TFormPeriod.SetFunction(AFunction:TTeeFunction);
var CanRange : Boolean;
begin
  IFunction:=AFunction;
  if Assigned(IFunction) then
  begin
    CanRange:=not (IFunction is TTeeMovingFunction);
    With IFunction do
    begin
      With ParentSeries do
      if Assigned(DataSource) and (DataSource is TChartSeries) then
        TheIsDateTime:=TChartSeries(DataSource).XValues.DateTime
      else
        TheIsDateTime:=False;

      if (PeriodStyle=psRange) and CanRange then
         RBRange.Checked:=True
      else
         RBNum.Checked:=True;

      RGAlign.ItemIndex:=Ord(PeriodAlign);
      if RBNum.Checked then
         ENum.Text:=FloatToStr(Round(MaxDouble(0,Period)))
      else
         ENum.Text:=FloatToStr(Period);
      LCalc.Caption:=Format(TeeMsg_CalcPeriod,[Caption]);
      SetLabels;
    end;
    RBRange.Enabled:=CanRange;
    BChange.Enabled:=CanRange;
    EnableControls(RBNum.Checked)
  end
  else EnableControls(False);
end;

procedure TFormPeriod.FormShow(Sender: TObject);
begin
  SetFunction(TTeeFunction(Tag));
end;

Procedure TFormPeriod.EnableControls(Value:Boolean);
begin
  CBAll.Enabled:=Value;
  ENum.Enabled:=Value;
  BChange.Enabled:=not Value;
end;

procedure TFormPeriod.RBRangeClick(Sender: TObject);
begin
  if RBRange.Checked then
  begin
    BApply.Enabled:=True;
    EnableControls(False);
    BChange.SetFocus;
  end;
end;

procedure TFormPeriod.RBNumClick(Sender: TObject);
begin
  if RBNum.Checked then
  begin
    BApply.Enabled:=True;
    EnableControls(True);
    ENum.SetFocus;
  end;
end;

procedure TFormPeriod.BChangeClick(Sender: TObject);
begin
  RBRange.Checked:=True;
  With TAxisIncrement.Create(Self) do
  try
    IsDateTime :=TheIsDateTime;
    IsExact    :=True;
    Increment  :=IFunction.Period;
    IStep      :=FindDateTimeStep(IFunction.Period);
    Caption    :=TeeMsg_PeriodRange;
    if ShowModal=mrOk then
    begin
      ENum.Text:=FloatToStr(Increment);
      TheIsDateTime:=IsDateTime;
      Self.SetLabels;
    end;
  finally
    Free;
  end;
end;

Procedure TFormPeriod.SetLabels;
begin
  if ENum.Text<>'' then
     BLabel.Caption:=GetIncrementText(Self,StrToFloat(ENum.Text),TheIsDateTime,True,'')
  else
     BLabel.Caption:='';
  CBAll.Checked:=StrToFloat(ENum.Text)=0;
  CBAll.Enabled:=not CBAll.Checked;
end;

procedure TFormPeriod.CBAllClick(Sender: TObject);
begin
  if CBAll.Checked then
  begin
    ENum.Text:='0';
    SetLabels;
    BApply.Enabled:=True;
  end;
end;

procedure TFormPeriod.ENumChange(Sender: TObject);
begin
  SetLabels;
  BApply.Enabled:=True;
end;

procedure TFormPeriod.Apply;
begin
  if Assigned(IFunction) then
  With IFunction do
  begin
    BeginUpdate;
    PeriodAlign:=TFunctionPeriodAlign(RGAlign.ItemIndex);
    if RBNum.Checked then PeriodStyle:=psNumPoints
                     else PeriodStyle:=psRange;
    Period     :=StrToFloat(ENum.Text);
    EndUpdate;
  end;
end;

procedure TFormPeriod.RGAlignClick(Sender: TObject);
begin
  BApply.Enabled:=True;
end;

end.
