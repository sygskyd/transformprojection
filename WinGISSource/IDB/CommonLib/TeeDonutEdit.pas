{******************************************}
{    TDonutSeries Editor Dialog            }
{ Copyright (c) 1999-2000 by David Berneda }
{******************************************}
{$I teedefs.inc}
unit TeeDonutEdit;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, ExtCtrls, Chart, Series, TeeDonut,
  ComCtrls, TeeProcs;

type
  TDonutSeriesEditor = class(TForm)
    EDonut: TEdit;
    UDDonut: TUpDown;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure EDonutChange(Sender: TObject);
  private
    { Private declarations }
    Donut : TDonutSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeeConst, TeePieEdit, TeeEdiSeri;

procedure TDonutSeriesEditor.FormShow(Sender: TObject);
begin
  Donut:=TDonutSeries(Tag);
  (Parent.Owner as TFormTeeSeries).InsertSeriesForm( TPieSeriesEditor,
                                                     1,TeeMsg_GalleryPie,
                                                     Donut);
  UDDonut.Position:=Donut.DonutPercent;
end;

procedure TDonutSeriesEditor.EDonutChange(Sender: TObject);
begin
  if Showing then Donut.DonutPercent:=UDDonut.Position;
end;

initialization
  RegisterClass(TDonutSeriesEditor);
end.
