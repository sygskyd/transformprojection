�
 TTEEDLGWIZARD 0:  TPF0TTeeDlgWizardTeeDlgWizardLeft� TopxBorderStylebsDialogCaptionTeeChart WizardClientHeight>ClientWidth�
ParentFont	HelpFileteechart.hlp
KeyPreview	PositionpoScreenCenterOnClose	FormCloseOnCloseQueryFormCloseQueryOnCreate
FormCreate	OnKeyDownFormKeyDownPixelsPerInch`
TextHeight TPageControlPageControlLeft Top Width�Height
ActivePageTabStyleAlignalClientHotTrack		MultiLine		TabHeightTabOrder TabStop 	TTabSheetTabStyle
TabVisible TPanelPanel4Left Top Width�HeightQAlignalTopTabOrder  TImageImage2LeftTopWidth�HeightJCenter	Stretch	OnClickImage1Click  TLabelLabelURLLeft� TopWidthTHeightCursorcrHandPoint	AlignmenttaCenterCaptionwww.steema.comColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameDefault
Font.StylefsUnderline ParentColor
ParentFontTransparent	OnClickLabelURLClick  TImageImage1LeftTopWidthyHeight1AutoSize	   TRadioGroup
RGDatabaseLeft� TophWidth� HeightaCaptionSelect a Chart style:	ItemIndex Items.Strings&Database ChartN&on Database Chart TabOrder   	TTabSheet	TableName
TabVisible TLabelLabel3LeftTopWidthHeightAutoSizeCaptionSelect a Database Table:  TPanelPanel8LeftTop$Width{Height� TabOrder  TLabelLabel1LeftGTopHWidthHeight	AlignmenttaRightJustifyCaption&Alias:FocusControlCBAlias  TLabelLabel5LeftBTopcWidthHeight	AlignmenttaRightJustifyCaption&Table:FocusControlCBTables  	TComboBoxCBAliasLefthTopEWidth� Height
ItemHeightSorted	TabOrder OnChangeCBAliasChange  TButtonButton6LeftTopDWidthHeightCaption...TabOrderOnClickButton6Click  	TComboBoxCBTablesLefthTop`Width� HeightStylecsDropDownList
ItemHeightTabOrderOnChangeCBTablesChange    	TTabSheetButtons
TabVisible TLabelLabel2Left"Top
WidthCHeightAutoSizeCaption#Select the desired Fields to Chart:  TPanelPanel1Left!TopWidth`Height� 
BevelOuter	bvLoweredTabOrder   TPanelPanel2Left!Top� Width`Height)
BevelOuter	bvLoweredTabOrder TLabelLabel6LeftTopWidth� Height	AlignmenttaRightJustifyAutoSizeCaptionSelect a text &labels Field:FocusControlcbLabelsFields  	TComboBoxcbLabelsFieldsLeft� TopWidth� Height
ItemHeightTabOrder     	TTabSheet	TabSheet1
TabVisible TLabelLabel4LeftTopWidthQHeightAutoSizeCaptionChoose the desired Chart type:  TRadioGroupRG3DLeft	Top� Width� Height$Columns	ItemIndexItems.Strings&2D&3D TabOrder OnClick	RG3DClick  TChartGalleryPanel
tmpGalleryLeftTopWidth�Height� NumRowsOnSelectedCharttmpGallerySelectedChartTabOrder   	TTabSheet	TabSheet2
TabVisible TPageControlPageControl1Left Top Width�Height
ActivePageTabPreviewChartAlignalClientHotTrack	TabOrder  	TTabSheetTabPreviewChartCaptionChart Preview TDBChartPreviewChartLeft Top Width�Height� AllowPanningpmNoneBackWall.Brush.ColorclWhiteFoot.Frame.VisibleTitle.Frame.VisibleTitle.Text.StringsTDBChart Chart3DPercent#
Zoom.AllowZoom.Animated	AlignalClient
BevelOuterbvNoneTabOrder   TPanelPanel7Left Top� Width�HeightAlignalBottom
BevelInner	bvLowered
BevelOuterbvNoneTabOrder 	TCheckBox
CB3DOptionLeftTopWidth-HeightCaption3&DState	cbCheckedTabOrder OnClickCB3DOptionClick  	TCheckBoxCBShowLegendOptionLeft@TopWidthaHeightCaptionShow &LegendState	cbCheckedTabOrderOnClickCBShowLegendOptionClick  	TCheckBoxCBShowMarksOptionLeft� TopWidthaHeightCaptionShow &MarksTabOrderOnClickCBShowMarksOptionClick       TPanelPanel5Left TopWidth�Height)AlignalBottomTabOrder TButtonButton1LeftTopWidthMHeightCaption&HelpTabOrder OnClickButton1Click  TButton
PrevButtonLeft|TopWidthMHeightCaption< &BackEnabledTabOrderOnClick	PrevClick  TButton
NextButtonLeft� TopWidthMHeightCaption&Next >Default	TabOrderOnClick	NextClick  TButton	CancelBtnLeftXTopWidthMHeightCancel	CaptionCancelModalResultTabOrderOnClickCancelClick   TTableTable1ReadOnly	Left� Top�   TDataSourceDataSource1DataSetTable1LeftTop�    