{***************************************************************
 *
 * Unit Name: TeeFunnel
 * Purpose  : The Funnel or Pipeline Series
 * Author   : Marjan Slatinek, marjan@steema.com
 * History  : v1.0 (needs TeeChart v5)
 *
 ****************************************************************}

{$I TeeDefs.inc}
unit TeeFunnel;

interface

Uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     {$IFNDEF CLX}
     Windows, Messages,
     {$ENDIF}
     {$ENDIF}
     {$IFDEF CLX}
     QGraphics, Types,
     {$ELSE}
     Graphics,
     {$ENDIF}
     TeEngine, Chart, Classes, SysUtils, TeCanvas;

type
  TFunnelSeries = class(TChartSeries)
  private
    ISorted : boolean;
    IMin,
    IMax    : double;
    ISlope  : double;
    IDiff   : double;
    FOpportunityValues: TChartValueList;
    FAutoUpdate: boolean;
    FQuotesSorted: boolean;
    FDifferenceLimit: double;
    FBelowColor: TColor;
    FWithinColor: TColor;
    FAboveColor: TColor;
    FLinesPen: TChartPen;

    { internal }
    IPolyPoints   : Array{$IFNDEF D4}[0..4]{$ENDIF} of TPoint;
    {$IFNDEF D4}
    IPolyCount   : Integer;
    {$ENDIF}
    BoundingPoints: Array[0..3] of TPoint;

    procedure SetOpportunityValues(const Value: TChartValueList);
    procedure SetAutoUpdate(const Value: boolean);
    function GetQuoteValues: TChartValueList; virtual;
    procedure SetQuoteValues(const Value: TChartValueList); virtual;
    procedure SetQuotesSorted(const Value: boolean);
    procedure SetDifferenceLimit(const Value: double);
    procedure SetAboveColor(const Value: TColor);
    procedure SetBelowColor(const Value: TColor);
    procedure SetWithinColor(const Value: TColor);
    function DefineFunnelRegion(ValueIndex: Integer): TColor;
    procedure SetLinesPen(const Value: TChartPen);
  protected
    procedure AddSampleValues(NumValues:Integer);override;
    procedure DrawValue(ValueIndex: Integer);override;
    procedure DrawAllValues; override;
    Procedure DrawMark( ValueIndex:Integer; Const St:String;
                                       APosition:TSeriesMarkPosition); override;
    procedure DoBeforeDrawChart; override;
    class Function GetEditorClass:String; override;
    procedure GetMarkText(Sender: TChartSeries; ValueIndex: Integer;
                          var MarkText: String);
  public
    Constructor Create(AOwner: TComponent); override;
    Destructor Destroy; override;

    function AddSegment(Const AQuote, AOpportunity: Double;
                        Const ALabel: String; AColor: TColor): Integer;
    Function MinYValue:Double; override;
    Function MinXValue:Double; override;
    Function MaxXValue:Double; override;
    function Clicked(X,Y:Integer):Integer; override;
    Function CountLegendItems:Integer; override;
    Function LegendItemColor(LegendIndex:Integer):TColor; override;
    Function LegendString( LegendIndex:Integer;
                           LegendTextStyle:TLegendTextStyle ):String; override;
    procedure Recalc;
  published
    property Brush;
    property Pen;   
    property LinesPen:TChartPen read FLinesPen write SetLinesPen;
    property AutoUpdate: boolean read FAutoUpdate write SetAutoUpdate default true;
    property QuotesSorted : boolean read FQuotesSorted write SetQuotesSorted default false;
    property DifferenceLimit : double read FDifferenceLimit write SetDifferenceLimit;
    property AboveColor: TColor read FAboveColor write SetAboveColor default clGreen;
    property WithinColor : TColor read FWithinColor write SetWithinColor default clYellow;
    property BelowColor : TColor read FBelowColor write SetBelowColor default clRed;
    property OpportunityValues: TChartValueList read FOpportunityValues write SetOpportunityValues;
    property QuoteValues: TChartValueList read GetQuoteValues write SetQuoteValues;
  end;

implementation

//{$R TeeFunne.res}

Uses TeeProcs, TeeProCo, TeeConst;

{ TFunnelSeries }
function TFunnelSeries.AddSegment(const AQuote, AOpportunity: Double;
  const ALabel: String; AColor: TColor): Integer;
begin
     FOpportunityValues.TempValue := AOpportunity;
     Result := Add(AQuote,ALabel,AColor);
     if Not FQuotesSorted then
     begin
          YValues.Sort;
          XVAlues.FillSequence;
          ISorted := true;
     end;
end;

constructor TFunnelSeries.Create(AOwner: TComponent);
begin
  inherited;
  FOpportunityValues := TChartValueList.Create(Self,'OpportunityValues');
  Self.XValues.Name := '';
  Self.XValues.Order := loNone;

  PercentFormat:='0.00 %';

  FOpportunityValues.Order := loNone;
  Self.QuoteValues.Order := loDescending;
  Self.YValues.Name := 'QuoteValues';
  FAutoUpdate := true;
  FQuotesSorted := false;
  ISorted := false;
  FDifferenceLimit := 30.0;
  FAboveColor := clGreen;
  FWithinColor := clYellow;
  FBelowColor := clRed;
  Self.OnGetMarkText := GetMarkText;
  FLinesPen:=CreateChartPen;
  IUseSeriesColor:=False;
end;

destructor TFunnelSeries.Destroy;
begin
  FLinesPen.Free;
  {$IFDEF D4}
  IPolyPoints:=nil;
  {$ENDIF}
  inherited;
end;

function TFunnelSeries.GetQuoteValues: TChartValueList;
begin
     Result := MandatoryValueList;
end;

procedure TFunnelSeries.Recalc;
begin
   if Not (ISorted) then
   begin
      QuoteValues.Sort;
      XValues.FillSequence;
      ISorted := true;
   end;

   if Count>0 then
   begin
     IMax := QuoteValues.First;
     IMin := QuoteValues.Last;
     ISlope := 0.5*(IMax-IMin) / Count;
   end;
end;

procedure TFunnelSeries.SetAutoUpdate(const Value: boolean);
begin
  FAutoUpdate := Value;
  if FAutoUpdate then Recalc;
end;

procedure TFunnelSeries.SetQuoteValues(
  const Value: TChartValueList);
begin
  SetYValues(Value); { overrides the default YValues }
end;

procedure TFunnelSeries.SetQuotesSorted(const Value: boolean);
begin
  FQuotesSorted := Value;
  ISorted := FQuotesSorted;
end;

procedure TFunnelSeries.SetOpportunityValues(const Value: TChartValueList);
begin
  SetChartValueList(FOpportunityValues,Value); { standard method }
end;

procedure TFunnelSeries.DrawValue(ValueIndex: Integer);
var tmpColor: TColor;
begin
   tmpColor := DefineFunnelRegion(ValueIndex);

   With ParentChart.Canvas do
   begin
      AssignBrushColor(Self.Brush,tmpColor,Self.Brush.Color);

      {$IFDEF D4}
      if ParentChart.View3D then PolygonWithZ(IPolyPoints,StartZ)
                            else Polygon(IPolyPoints);
      {$ELSE}
      if ParentChart.View3D then PolygonWithZ(Slice(IPolyPoints,IPolyCount),StartZ)
                            else Polygon(Slice(IPolyPoints,IPolyCount));
      {$ENDIF}
   end
end;

procedure TFunnelSeries.DrawAllValues;

    procedure DrawVertLine(ValueIndex: Integer);
    var tmpX : Integer;
    begin
         tmpX := CalcXPosValue(ValueIndex + 0.5);
         With ParentChart.Canvas do
         begin
           if ParentChart.View3D then
           begin
              MoveTo3D(tmpX,CalcYPosValue(IMax - ISlope*(ValueIndex+1)),StartZ);
              LineTo3D(tmpX,CalcYPosValue(ISlope*(ValueIndex+1)),StartZ);
           end
           else
           begin
              MoveTo(tmpX,CalcYPosValue(IMax - ISlope*(ValueIndex+1)));
              LineTo(tmpX,CalcYPosValue(ISlope*(ValueIndex+1)));
           end;
         end;
    end;

var t: Integer;

begin
  inherited;
  With ParentChart.Canvas do
  begin
     AssignVisiblePen(Self.Pen);

     Brush.Style := bsClear;
     BoundingPoints[0] := Point(CalcXPosValue(-0.5),CalcYPosValue(IMax));
     BoundingPoints[1] := Point(CalcXPosValue(Count-0.5), CalcYPosValue((IMax+IMin)*0.5));
     BoundingPoints[2] := Point(BoundingPoints[1].x, CalcYPosValue((IMax-IMin)*0.5));
     BoundingPoints[3] := Point(BoundingPoints[0].x, CalcYPosValue(0.0));

     if ParentChart.View3D then PolygonWithZ(BoundingPoints,StartZ)
                           else Polygon(BoundingPoints);

     if Self.LinesPen.Visible then
     begin
       AssignVisiblePen(Self.LinesPen);
       for t := FirstValueIndex to LastValueIndex -1 do DrawVertLine(t);
     end;
  end;
end;

function TFunnelSeries.MinYValue: Double;
begin
  Result := 0.0;
end;

function TFunnelSeries.MaxXValue: Double;
begin
  Result := Count -0.5;
end;

function TFunnelSeries.MinXValue: Double;
begin
  Result := -0.5;
end;

procedure TFunnelSeries.SetDifferenceLimit(const Value: double);
begin
  FDifferenceLimit := Value;
  if FAutoUpdate then Recalc;
  Repaint;
end;

procedure TFunnelSeries.SetAboveColor(const Value: TColor);
begin
  SetColorProperty(FAboveColor,Value);
end;

procedure TFunnelSeries.SetBelowColor(const Value: TColor);
begin
  SetColorProperty(FBelowColor,Value);
end;

procedure TFunnelSeries.SetWithinColor(const Value: TColor);
begin
  SetColorProperty(FWithinColor,Value);
end;

procedure TFunnelSeries.GetMarkText(Sender: TChartSeries;
  ValueIndex: Integer; var MarkText: String);
begin
  MarkText := FormatFloat(PercentFormat,
       100*FOpportunityValues.Value[ValueIndex]/QuoteValues.Value[ValueIndex]);
end;

procedure TFunnelSeries.DrawMark(ValueIndex: Integer; const St: String;
  APosition: TSeriesMarkPosition);
begin
  APosition.LeftTop := Point(CalcXPosValue(ValueIndex)-APosition.Width div 2,
                             CalcYPosValue(IMax*0.5)-APosition.Height div 2);
  inherited;
end;

function TFunnelSeries.DefineFunnelRegion(ValueIndex: Integer): TColor;
var tmpX, tmpY : Integer;
begin
     { Calculate multiplying factor }

     if QuoteValues.Value[ValueIndex]=0 then
        IDiff:=0
     else
        IDiff := FOpportunityValues.Value[ValueIndex]/QuoteValues.Value[ValueIndex];


     { calculate bouding rectangle }
     BoundingPoints[0] := Point(CalcXPosValue(ValueIndex-0.5),CalcYPosValue(IMax - ISlope*ValueIndex));
     BoundingPoints[1] := Point(CalcXPosValue(ValueIndex+0.5),CalcYPosValue(IMax - ISlope*(ValueIndex+1)));
     BoundingPoints[2] := Point(BoundingPoints[1].x,CalcYPosValue(ISlope*(ValueIndex+1)));
     BoundingPoints[3] := Point(BoundingPoints[0].x,CalcYPosValue(ISlope*ValueIndex));

     tmpY := CalcYPosValue((IMax - 2*ISlope*ValueIndex)*IDiff+ISlope*ValueIndex); { Actual value, expressed in axis scale }

     if tmpY <= BoundingPoints[0].Y then { IDiff >= 1 }
     begin
          {$IFDEF D4}
          SetLength(IPolyPoints,4);
          {$ELSE}
          IPolyCount:=4;
          {$ENDIF}
          IPolyPoints[0] := BoundingPoints[0];
          IPolyPoints[1] := BoundingPoints[1];
          IPolyPoints[2] := BoundingPoints[2];
          IPolyPoints[3] := BoundingPoints[3];
     end
     else if (tmpY > BoundingPoints[0].Y) and (tmpY <= BoundingPoints[1].Y) then
     begin
          {$IFDEF D4}
          SetLength(IPolyPoints,5);
          {$ELSE}
          IPolyCount:=5;
          {$ENDIF}
          IPolyPoints[0] := Point(BoundingPoints[0].x,tmpY);
          tmpX := CalcXPosValue((IMax - 2*ISlope*ValueIndex)*(1-IDiff)/ISlope + ValueIndex - 0.5);
          IPolyPoints[1] := Point(tmpX,tmpY);
          IPolyPoints[2] := BoundingPoints[1];
          IPolyPoints[3] := BoundingPoints[2];
          IPolyPoints[4] := BoundingPoints[3];
     end
     else if tmpY > BoundingPoints[2].y then
     begin
          {$IFDEF D4}
          SetLength(IPolyPoints,3);
          {$ELSE}
          IPolyCount:=3;
          {$ENDIF}
          IPolyPoints[0] := Point(BoundingPoints[0].x,tmpY);
          tmpX := CalcXPosValue((IMax - 2*ISlope*ValueIndex)*IDiff /ISlope + ValueIndex-0.5);
          IPolyPoints[1] := Point(tmpX,tmpY);
          IPolyPoints[2] := BoundingPoints[3];
     end
     else
     begin
          {$IFDEF D4}
          SetLength(IPolyPoints,4);
          {$ELSE}
          IPolyCount:=4;
          {$ENDIF}
          IPolyPoints[0] := Point(BoundingPoints[0].x, tmpY);
          IPolyPoints[1] := Point(BoundingPoints[1].x, tmpY);
          IPolyPoints[2] := BoundingPoints[2];
          IPolyPoints[3] := BoundingPoints[3];
     end;

     if IDiff >= 1 then Result := FAboveColor
     else
     if (1-IDiff)*100 > FDifferenceLimit then
         Result := FBelowColor
     else
         Result := FWithinColor;
end;

function TFunnelSeries.Clicked(X, Y: Integer): Integer;
var t : Integer;
begin
  Result := inherited Clicked(X,Y);
  if (result=-1) and (FirstValueIndex>-1) and (LastValueIndex>-1) then
    for t := FirstValueIndex to LastValueIndex do
    begin
         DefineFunnelRegion(t);
         if PointInPolygon(Point(X,Y),
                           {$IFDEF D4}
                           IPolyPoints
                           {$ELSE}
                           Slice(IPolyPoints,IPolyCount)
                           {$ENDIF}) then
         begin
              Result := t;
              break;
         end
    end;
end;

procedure TFunnelSeries.AddSampleValues(NumValues: Integer);
var t: Integer;
begin
     Clear;
     for t := 0 to NumValues - 1 do
          AddSegment(2.3*NumValues*(t+1),(2.3-Random(2))*NumValues*(t+2),
                     'Segment '+TeeStr(t+1),clTeeColor);
     Recalc;
end;

procedure TFunnelSeries.DoBeforeDrawChart;
begin
  inherited;
  if Assigned(GetVertAxis) then GetVertAxis.Visible := false;
end;

function TFunnelSeries.CountLegendItems: Integer;
begin
  result:=3;
end;

function TFunnelSeries.LegendItemColor(LegendIndex: Integer): TColor;
begin
  Case LegendIndex of
    0: result:=AboveColor;
    1: result:=WithinColor;
  else
    result:=BelowColor;
  end;
end;

function TFunnelSeries.LegendString(LegendIndex: Integer;
  LegendTextStyle: TLegendTextStyle): String;
begin
  Case LegendIndex of
    0: result:='Exceeds quota';
    1: result:=FormatFloat(PercentFormat,DifferenceLimit)+ ' within quota';
  else
    result:=FormatFloat(PercentFormat,DifferenceLimit)+ ' or more below quota';
  end;
end;

class function TFunnelSeries.GetEditorClass: String;
begin
  result:='TFunnelSeriesEditor';
end;

procedure TFunnelSeries.SetLinesPen(const Value: TChartPen);
begin
  FLinesPen.Assign(Value);
end;

initialization
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{
   RegisterTeeSeries(TFunnelSeries,'Funnel Series',TeeMsg_GalleryStats,1);
}
   RegisterTeeSeries(TFunnelSeries,'Funnel Series',TeeMsg_GalleryStats,1,stFunnelSeries);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
finalization
   UnRegisterTeeSeries([TFunnelSeries]);
end.
