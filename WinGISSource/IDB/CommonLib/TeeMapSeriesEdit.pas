{********************************************}
{ TeeChart Pro - Map Series Editor           }
{ Copyright (c) 1995-2000 by David Berneda   }
{ All Rights Reserved                        }
{********************************************}
{$I teedefs.inc}
unit TeeMapSeriesEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, TeCanvas, TeeMapSeries, TeePenDlg, ComCtrls, Grids,
  TeeChartGrid, TeeLisB, ExtCtrls, TeeNavigator, Buttons;

type
  TMapSeriesEditor = class(TForm)
    PageControl2: TPageControl;
    TabGlobal: TTabSheet;
    BMapBrush: TButton;
    TabShapes: TTabSheet;
    PageControl1: TPageControl;
    TabFormat: TTabSheet;
    Label2: TLabel;
    CBGlobalPen: TCheckBox;
    BBrush: TButton;
    CBGlobalBrush: TCheckBox;
    BGradient: TButton;
    EText: TEdit;
    TabData: TTabSheet;
    Splitter1: TSplitter;
    Label1: TLabel;
    EditZ: TEdit;
    ButtonPen1: TButtonPen;
    ButtonPen2: TButtonPen;
    Panel1: TPanel;
    ButtonColor1: TButtonColor;
    ChartGrid1: TChartGrid;
    ChartGridNavigator1: TChartGridNavigator;
    ChartListBox1: TChartListBox;
    Panel2: TPanel;
    SBDelete: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure BMapBrushClick(Sender: TObject);
    procedure ETextChange(Sender: TObject);
    procedure ChartListBox1Click(Sender: TObject);
    procedure CBGlobalPenClick(Sender: TObject);
    procedure CBGlobalBrushClick(Sender: TObject);
    procedure BGradientClick(Sender: TObject);
    procedure BBrushClick(Sender: TObject);
    procedure EditZChange(Sender: TObject);
    procedure ButtonPen2Click(Sender: TObject);
    procedure ButtonColor1Click(Sender: TObject);
    procedure PageControl2Change(Sender: TObject);
    procedure SBDeleteClick(Sender: TObject);
  private
    { Private declarations }
    Map : TMapSeries;
    Procedure FillShapes;
    Function SelectedShape:TTeePolygon;
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

Uses TeeBrushDlg, TeeEdiGrad, TeeGriEd;

procedure TMapSeriesEditor.FormShow(Sender: TObject);
begin
  Map:=TMapSeries(Tag);
  ButtonPen1.LinkPen(Map.Pen);
  TeeInsertGrid3DForm(Parent,Map);
  PageControl2.ActivePage:=TabGlobal;
end;

procedure TMapSeriesEditor.BMapBrushClick(Sender: TObject);
begin
  EditChartBrush(Self,Map.Brush);
end;

procedure TMapSeriesEditor.ETextChange(Sender: TObject);
begin
  With SelectedShape do
  if Text<>EText.Text then
  begin
    Text:=EText.Text;
    With ChartListBox1 do Items[ItemIndex]:=Text;
  end;
end;

procedure TMapSeriesEditor.ChartListBox1Click(Sender: TObject);
begin
  With SelectedShape do
  begin
    ButtonPen2.LinkPen(Pen);
    ButtonColor1.LinkProperty(SelectedShape,'Color');
    ButtonColor1.Repaint;
    CBGlobalPen.Checked:=ParentPen;
    CBGlobalBrush.Checked:=ParentBrush;
    EText.Text:=Text;
    EditZ.Text:=FloatToStr(Z);
    ChartGrid1.Series:=Points;
  end;
  SBDelete.Enabled:=True;
end;

procedure TMapSeriesEditor.CBGlobalPenClick(Sender: TObject);
begin
  SelectedShape.ParentPen:=CBGlobalPen.Checked;
end;

procedure TMapSeriesEditor.CBGlobalBrushClick(Sender: TObject);
begin
  SelectedShape.ParentBrush:=CBGlobalBrush.Checked;
end;

procedure TMapSeriesEditor.BGradientClick(Sender: TObject);
begin
  EditTeeGradient(Self,SelectedShape.Gradient);
end;

procedure TMapSeriesEditor.BBrushClick(Sender: TObject);
begin
  EditChartBrush(Self,SelectedShape.Brush);
  CBGlobalBrush.Checked:=False;
  Map.Repaint;
end;

function TMapSeriesEditor.SelectedShape: TTeePolygon;
begin
  result:=Map.Shapes[ChartListBox1.ItemIndex];
end;

procedure TMapSeriesEditor.EditZChange(Sender: TObject);
var tmp : Double;
begin
  tmp:=StrToFloat(EditZ.Text);
  if SelectedShape.Z<>tmp then
  begin
    SelectedShape.Z:=tmp;
    Map.Repaint;
  end;
end;

procedure TMapSeriesEditor.ButtonPen2Click(Sender: TObject);
begin
  CBGlobalPen.Checked:=False;
  Map.Repaint;
end;

procedure TMapSeriesEditor.ButtonColor1Click(Sender: TObject);
begin
  ChartListBox1.Repaint;
end;

procedure TMapSeriesEditor.PageControl2Change(Sender: TObject);
begin
  if ChartListBox1.Items.Count<>Map.Shapes.Count then FillShapes;
end;

Procedure TMapSeriesEditor.FillShapes;
var t : Integer;
begin
  With ChartListBox1 do
  begin
    ClearItems;
    for t:=0 to Map.Shapes.Count-1 do
        Items.AddObject(Map.XLabel[t],Map.Shapes[t].Points);
    if Items.Count>0 then
    begin
      ItemIndex:=0;
      ChartListBox1Click(Self);
    end;
  end;
  TabShapes.Enabled:=Map.Shapes.Count>0;
end;

procedure TMapSeriesEditor.SBDeleteClick(Sender: TObject);
begin
  Map.Delete(ChartListBox1.ItemIndex);
  FillShapes;
end;

initialization
  RegisterClass(TMapSeriesEditor);
end.
