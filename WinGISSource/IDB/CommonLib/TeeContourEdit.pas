{**********************************************}
{   TContourSeries Editor Dialog               }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeContourEdit;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, ExtCtrls, Chart, TeeSurfa, ComCtrls, TeCanvas,
  TeePenDlg, TeeProcs;

type
  TContourSeriesEditor = class(TForm)
    Button2: TButtonPen;
    Label2: TLabel;
    SEYPos: TEdit;
    CBYPosLevel: TCheckBox;
    UDYPos: TUpDown;
    GroupBox1: TGroupBox;
    CBAutoLevels: TCheckBox;
    Label4: TLabel;
    SENum: TEdit;
    UDNum: TUpDown;
    CBColorEach: TCheckBox;
    Label1: TLabel;
    SHColor: TShape;
    ELevel: TEdit;
    UDLevel: TUpDown;
    EValue: TEdit;
    procedure FormShow(Sender: TObject);
    procedure SENumChange(Sender: TObject);
    procedure CBYPosLevelClick(Sender: TObject);
    procedure SEYPosChange(Sender: TObject);
    procedure CBColorEachClick(Sender: TObject);
    procedure CBAutoLevelsClick(Sender: TObject);
    procedure SHColorMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ELevelChange(Sender: TObject);
    procedure EValueChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Contour : TContourSeries;
    CreatingForm : Boolean;
    procedure SetLevel;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeeGriEd;

procedure TContourSeriesEditor.FormShow(Sender: TObject);
begin
  Contour:=TContourSeries(Tag);
  With Contour do
  begin
    CBColorEach.Checked :=ColorEachPoint;
    CBYPosLevel.Checked :=YPositionLevel;
    SEYPos.Enabled      :=not YPositionLevel;
    UDYPos.Position     :=Round(YPosition);
    CBAutoLevels.Checked:=AutomaticLevels;
    UDNum.Position      :=NumLevels;
    UDLevel.Max         :=NumLevels-1;
    Button2.LinkPen(Pen);
    EnableControls(AutomaticLevels,[SENum,UDNum,CBColorEach]);
  end;
  TeeInsertGrid3DForm(Parent,Contour);
  SetLevel;
  CreatingForm:=False;
end;

procedure TContourSeriesEditor.SetLevel;
begin
  CreatingForm:=True;
  if Contour.Levels.Count>UDLevel.Position then
  With Contour.Levels[UDLevel.Position] do
  begin
    SHColor.Brush.Color:=Color;
    EValue.Text:=FormatFloat('0.###',UpToValue);
  end;
  CreatingForm:=False;
end;

procedure TContourSeriesEditor.SENumChange(Sender: TObject);
begin
  if not CreatingForm then
  begin
    Contour.NumLevels:=UDNum.Position;
    Contour.CreateAutoLevels;
    With UDLevel do
    begin
      Max:=Contour.NumLevels-1;
      ELevel.Text:=TeeStr(Position);
      SetLevel;
    end;
  end;
end;

procedure TContourSeriesEditor.CBYPosLevelClick(Sender: TObject);
begin
  Contour.YPositionLevel:=CBYPosLevel.Checked;
  SEYPos.Enabled:=not CBYPosLevel.Checked;
end;

procedure TContourSeriesEditor.SEYPosChange(Sender: TObject);
begin
  if not CreatingForm then Contour.YPosition:=UDYPos.Position;
end;

procedure TContourSeriesEditor.CBColorEachClick(Sender: TObject);
begin
  Contour.ColorEachPoint:=CBColorEach.Checked;
  Contour.CreateAutoLevels;
  SetLevel;
end;

procedure TContourSeriesEditor.CBAutoLevelsClick(Sender: TObject);
begin
  Contour.AutomaticLevels:=CBAutoLevels.Checked;
  EnableControls(Contour.AutomaticLevels,[SENum,UDNum,CBColorEach]);
end;

procedure TContourSeriesEditor.SHColorMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  With Contour.Levels[UDLevel.Position] do
  begin
    Color:=EditColor(Self,Color);
    SHColor.Brush.Color:=Color;
  end;
  CBAutoLevels.Checked:=Contour.AutomaticLevels;
end;

procedure TContourSeriesEditor.ELevelChange(Sender: TObject);
begin
  if not CreatingForm and (Contour.Levels.Count>0) then
     SetLevel;
end;

procedure TContourSeriesEditor.EValueChange(Sender: TObject);
begin
  if not CreatingForm then
  begin
    Contour.Levels[UDLevel.Position].UpToValue:=StrToFloat(EValue.Text);
    CBAutoLevels.Checked:=Contour.AutomaticLevels;
  end;
end;

procedure TContourSeriesEditor.FormCreate(Sender: TObject);
begin
  CreatingForm:=True;
end;

initialization
  RegisterClass(TContourSeriesEditor);
end.
