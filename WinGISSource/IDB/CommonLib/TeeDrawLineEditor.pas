{**********************************************}
{   TeeChart Tools                             }
{   Copyright (c) 1999-2000 by David Berneda   }
{**********************************************}
unit TeeDrawLineEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, TeCanvas, TeePenDlg, TeeTools, TeeToolSeriesEdit;

type
  TDrawLineEdit = class(TSeriesToolEditor)
    Label11: TLabel;
    CBButton: TComboBox;
    BPen: TButtonPen;
    CBEnable: TCheckBox;
    CBSelect: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure CBButtonChange(Sender: TObject);
    procedure CBEnableClick(Sender: TObject);
    procedure CBSelectClick(Sender: TObject);
  private
    { Private declarations }
    Draw : TDrawLineTool;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TDrawLineEdit.FormShow(Sender: TObject);
begin
  inherited;
  Draw:=TDrawLineTool(Tag);
  CBButton.ItemIndex:=Ord(Draw.Button);
  CBEnable.Checked:=Draw.EnableDraw;
  CBSelect.Checked:=Draw.EnableSelect;
  BPen.LinkPen(Draw.Pen);
end;

procedure TDrawLineEdit.CBButtonChange(Sender: TObject);
begin
  Draw.Button:=TMouseButton(CBButton.ItemIndex);
end;

procedure TDrawLineEdit.CBEnableClick(Sender: TObject);
begin
  Draw.EnableDraw:=CBEnable.Checked
end;

procedure TDrawLineEdit.CBSelectClick(Sender: TObject);
begin
  Draw.EnableSelect:=CBSelect.Checked
end;

initialization
  RegisterClass(TDrawLineEdit);
end.
