{******************************************}
{    TFastLineSeries Editor Dialog         }
{ Copyright (c) 1996-2000 by David Berneda }
{******************************************}
{$I teedefs.inc}
unit TeeFLineEdi;

interface

uses
  Windows, Graphics, Forms, Series, TeePenDlg, Classes, Controls, TeCanvas,
  StdCtrls, ComCtrls, MultiLng;

type
  TFastLineSeriesEditor = class(TPenDialog)
    procedure FormShow(Sender: TObject);
    procedure BColorClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TFastLineSeriesEditor.FormShow(Sender: TObject);
begin
  ThePen:=TFastLineSeries(Tag).Pen;
  inherited;
end;

procedure TFastLineSeriesEditor.BColorClick(Sender: TObject);
begin
  inherited;
  TFastLineSeries(Tag).SeriesColor:=ThePen.Color;
  CBStyle.Repaint;
end;

initialization
  RegisterClass(TFastLineSeriesEditor);
end.
