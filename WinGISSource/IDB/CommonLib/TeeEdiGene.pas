{**********************************************}
{  TCustomChart (or derived) Editor Dialog     }
{  Copyright (c) 1996-2000 by David Berneda    }
{**********************************************}
{$I teedefs.inc}
unit TeeEdiGene;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls, QComCtrls,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, ComCtrls,
     {$ENDIF}
     Chart, TeCanvas, TeePenDlg, TeeProcs;

type
  TFormTeeGeneral = class(TForm)
    CBClipPoints: TCheckBox;
    BPrint: TButton;
    GBMargins: TGroupBox;
    SETopMa: TEdit;
    SELeftMa: TEdit;
    SEBotMa: TEdit;
    SERightMa: TEdit;
    UDTopMa: TUpDown;
    UDRightMa: TUpDown;
    UDLeftMa: TUpDown;
    UDBotMa: TUpDown;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    LSteps: TLabel;
    Label1: TLabel;
    CBAllowZoom: TCheckBox;
    CBAnimatedZoom: TCheckBox;
    SEAniZoomSteps: TEdit;
    UDAniZoomSteps: TUpDown;
    BZoomPen: TButtonPen;
    BZoomColor: TButton;
    EMinPix: TEdit;
    UDMinPix: TUpDown;
    TabSheet2: TTabSheet;
    RGPanning: TRadioGroup;
    Label2: TLabel;
    CBDir: TComboBox;
    Label3: TLabel;
    CBZoomMouse: TComboBox;
    Label4: TLabel;
    CBScrollMouse: TComboBox;
    procedure BPrintClick(Sender: TObject);
    procedure CBAllowZoomClick(Sender: TObject);
    procedure CBClipPointsClick(Sender: TObject);
    procedure CBAnimatedZoomClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RGPanningClick(Sender: TObject);
    procedure SEAniZoomStepsChange(Sender: TObject);
    procedure SERightMaChange(Sender: TObject);
    procedure SETopMaChange(Sender: TObject);
    procedure SEBotMaChange(Sender: TObject);
    procedure SELeftMaChange(Sender: TObject);
    procedure BZoomColorClick(Sender: TObject);
    procedure EMinPixChange(Sender: TObject);
    procedure CBDirChange(Sender: TObject);
    procedure CBZoomMouseChange(Sender: TObject);
    procedure CBScrollMouseChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Function ChangeMargin(UpDown:TUpDown; APos,OtherSide:Integer):Integer;
    Procedure EnableZoomControls;
  public
    { Public declarations }
    TheChart : TCustomChart;
    Constructor CreateChart(Owner:TComponent; AChart:TCustomChart);
  end;

implementation

{$R *.dfm}
Uses TeePrevi, TeExport, TeeStore, TeeBrushDlg;

{ Chart General }
Constructor TFormTeeGeneral.CreateChart(Owner:TComponent; AChart:TCustomChart);
begin
  inherited Create(Owner);
  TheChart:=AChart;
end;

procedure TFormTeeGeneral.BPrintClick(Sender: TObject);
begin
  ChartPreview(Self,TheChart);
end;

Procedure TFormTeeGeneral.EnableZoomControls;
begin
  EnableControls(TheChart.Zoom.Allow,[ CBAnimatedZoom, UDAniZoomSteps,
                                       SEAniZoomSteps,EMinPix,UDMinPix,
                                       BZoomPen,BZoomColor,CBDir,CBZoomMouse]);
end;

procedure TFormTeeGeneral.CBAllowZoomClick(Sender: TObject);
begin
  TheChart.Zoom.Allow:=CBAllowZoom.Checked;
  EnableZoomControls;
end;

procedure TFormTeeGeneral.CBClipPointsClick(Sender: TObject);
begin
  TheChart.ClipPoints:=CBClipPoints.Checked;
end;

procedure TFormTeeGeneral.CBAnimatedZoomClick(Sender: TObject);
begin
  TheChart.Zoom.Animated:=CBAnimatedZoom.Checked;
end;

procedure TFormTeeGeneral.FormShow(Sender: TObject);
begin
  With TheChart do
  begin
    RGPanning.ItemIndex    :=Ord(AllowPanning);
    CBClipPoints.Checked   :=ClipPoints;

    UDTopMa.Position       :=MarginTop;
    UDLeftMa.Position      :=MarginLeft;
    UDBotMa.Position       :=MarginBottom;
    UDRightMa.Position     :=MarginRight;

    With Zoom do
    begin
      CBAllowZoom.Checked    :=Allow;
      CBAnimatedZoom.Checked :=Animated;
      UDAniZoomSteps.Position:=AnimatedSteps;
      EnableZoomControls;
      UDMinPix.Position      :=MinimumPixels;
      CBDir.ItemIndex        :=Ord(Direction);
      CBZoomMouse.ItemIndex  :=Ord(MouseButton);
      BZoomPen.LinkPen(Pen);
    end;

    CBScrollMouse.ItemIndex  :=Ord(ScrollMouseButton);
    CBScrollMouse.Enabled    :=AllowPanning<>pmNone;
  end;
end;

procedure TFormTeeGeneral.RGPanningClick(Sender: TObject);
begin
  TheChart.AllowPanning:=TPanningMode(RGPanning.ItemIndex);
  CBScrollMouse.Enabled:=TheChart.AllowPanning<>pmNone;
end;

procedure TFormTeeGeneral.SEAniZoomStepsChange(Sender: TObject);
begin
  if Showing then TheChart.Zoom.AnimatedSteps:=UDAniZoomSteps.Position;
end;

Function TFormTeeGeneral.ChangeMargin(UpDown:TUpDown; APos,OtherSide:Integer):Integer;
begin
  result:=APos;
  if Showing then
  With UpDown do
  if Position+OtherSide<100 then result:=Position
                            else Position:=APos;
end;

procedure TFormTeeGeneral.SERightMaChange(Sender: TObject);
begin
  if Showing then
  With TheChart do MarginRight:=ChangeMargin(UDRightMa,MarginRight,MarginLeft);
end;

procedure TFormTeeGeneral.SETopMaChange(Sender: TObject);
begin
  if Showing then
  With TheChart do MarginTop:=ChangeMargin(UDTopMa,MarginTop,MarginBottom);
end;

procedure TFormTeeGeneral.SEBotMaChange(Sender: TObject);
begin
  if Showing then
  With TheChart do MarginBottom:=ChangeMargin(UDBotMa,MarginBottom,MarginTop);
end;

procedure TFormTeeGeneral.SELeftMaChange(Sender: TObject);
begin
  if Showing then
  With TheChart do MarginLeft:=ChangeMargin(UDLeftMa,MarginLeft,MarginRight);
end;

procedure TFormTeeGeneral.BZoomColorClick(Sender: TObject);
begin
  EditChartBrush(Self,TheChart.Zoom.Brush);
end;

procedure TFormTeeGeneral.EMinPixChange(Sender: TObject);
begin
  if Showing then TheChart.Zoom.MinimumPixels:=UDMinPix.Position
end;

procedure TFormTeeGeneral.CBDirChange(Sender: TObject);
begin
  TheChart.Zoom.Direction:=TTeeZoomDirection(CBDir.ItemIndex);
end;

procedure TFormTeeGeneral.CBZoomMouseChange(Sender: TObject);
begin
  TheChart.Zoom.MouseButton:=TMouseButton(CBZoomMouse.ItemIndex)
end;

procedure TFormTeeGeneral.CBScrollMouseChange(Sender: TObject);
begin
  TheChart.ScrollMouseButton:=TMouseButton(CBScrollMouse.ItemIndex)
end;

procedure TFormTeeGeneral.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePage:=TabSheet1;
end;

end.
