Unit NumTools;

Interface

Uses WinProcs,WinTypes;

Const Tolerance    = 1E-10;      { tolerance for float-compares }

{******************************************************************************+
  Function DblCompare
--------------------------------------------------------------------------------
  Compares two double-values. The result is -1 if Value1 is smaller, 1
  if Value1 is bigger and 0 if the values are equal.
+******************************************************************************}
Function DblCompare(Const Value1,Value2:Double):Integer; Far;

{******************************************************************************+
  Function LongCompare
--------------------------------------------------------------------------------
  Compares two integer-values. The result is -1 if Value1 is smaller, 1
  if Value1 is bigger and 0 if the values are equal.
+******************************************************************************}
Function LongCompare(Value1,Value2:LongInt):Integer; Far;

{******************************************************************************+
  Function IntCheckRange
--------------------------------------------------------------------------------
  Determines if value lies within the specified intervall.
+******************************************************************************}
Function IntCheckRange(Value,V1,V2:LongInt):Boolean;

{******************************************************************************+
  Function DblCheckRange
--------------------------------------------------------------------------------
  Determines if value lies within the specified intervall.
+******************************************************************************}
Function DblCheckRange(Const Value,V1,V2:Double):Boolean;

{*******************************************************************************
  Function Min
--------------------------------------------------------------------------------
  Calculates the smaller of two integer-values.
+******************************************************************************}
Function Min(Const V1,V2:Integer):Integer; overload;
Function Min(Const V1,V2:Int64):Int64; overload;
Function Min(Const V1,V2:Double):Double; overload;

{*******************************************************************************
  Function Max
--------------------------------------------------------------------------------
  Calculates the bigger of two integer-values.
+******************************************************************************}
Function Max(Const V1,V2:Integer):Integer; overload;
Function Max(Const V1,V2:Int64):Int64; overload;
Function Max(Const V1,V2:Double):Double; overload;

{*******************************************************************************
  Function RectWidth
--------------------------------------------------------------------------------
  Calculates the width of a rectangle.
+******************************************************************************}
Function RectWidth(Const ARect:TRect):Integer; overload;

{*******************************************************************************
  Function RectHeight
--------------------------------------------------------------------------------
  Calculates the height of a rectangle.
+******************************************************************************}
Function RectHeight(Const ARect:TRect):Integer; overload;

{*******************************************************************************
  Function CenterRectInRect
--------------------------------------------------------------------------------
  Centeres ToCenter in InRect.
+******************************************************************************}
Function CenterRectInRect(Const ToCenter:TRect;Const InRect:TRect):TRect;

{*******************************************************************************
  Function CenterWidthInRect
--------------------------------------------------------------------------------
+******************************************************************************}
Function CenterWidthInRect(Width:Integer;Const InRect:TRect):Integer;

{*******************************************************************************
  Function CenterHeightInRect
--------------------------------------------------------------------------------
+******************************************************************************}
Function CenterHeightInRect(Height:Integer;Const InRect:TRect):Integer;

{*******************************************************************************
  Procedure InflateWidth
--------------------------------------------------------------------------------
  Increases or decreases the width of the specified rectangle. A positive
  value increases the width, a negative value decreases it.
+******************************************************************************}
Procedure InflateWidth(var ARect:TRect;ALeft:Integer;ARight:Integer);

{*******************************************************************************
  Procedure InflateHeight
--------------------------------------------------------------------------------
  Increases or decreases the height of the specified rectangle. A positive
  value increases the height, a negative value decreases it.
+******************************************************************************}
Procedure InflateHeight(var ARect:TRect;ATop:Integer;ABottom:Integer);

{*******************************************************************************
  Function InflatedWidth
--------------------------------------------------------------------------------
  Increases or decreases the width of the specified rectangle. A positive
  value increases the width, a negative value decreases it.
+******************************************************************************}
Function InflatedWidth(Const ARect:TRect;ALeft:Integer;ARight:Integer):TRect;

{*******************************************************************************
  Function InflatedHeight
--------------------------------------------------------------------------------
  Increases or decreases width and height of the specified rectangle. A positive
  value increases the limits, a negative value decreases it.
+******************************************************************************}
Function InflatedWidthHeight(Const ARect:TRect;ALeft:Integer;ARight:Integer;ATop:Integer;ABottom:Integer):TRect;

{*******************************************************************************
  Function InflatedHeight
--------------------------------------------------------------------------------
  Increases or decreases the height of the specified rectangle. A positive
  value increases the height, a negative value decreases it.
+******************************************************************************}
Function InflatedHeight(Const ARect:TRect;ATop:Integer;ABottom:Integer):TRect;

Procedure InflateLeft(var Rect:TRect;ALeft:Integer);
Procedure InflateRight(var Rect:TRect;ARight:Integer);
Function InflatedLeft(Const Rect:TRect;ALeft:Integer):TRect;
Function InflatedRight(Const Rect:TRect;ARight:Integer):TRect;

{*******************************************************************************
  Procedure InsertPointInRect
--------------------------------------------------------------------------------
  Increases the size of the rectangle so that the point lies in it.
+******************************************************************************}
Procedure InsertPointInRect(APoint:TPoint;var ARect:TRect); overload;

{*******************************************************************************
  Procedure ScaleRect
--------------------------------------------------------------------------------
  Scales the rectangle-coordinates by the given values.
+******************************************************************************}
Function ScaleRect(Const ARect:TRect;Const XScale,YScale:Double):TRect; overload;

Procedure MovePoint(var APoint:TPoint;DX,DY:Integer); overload;
Function MovedPoint(const Point:TPoint;const DeltaX,DeltaY:Integer):TPoint; overload;

{******************************************************************************+
  Swap
--------------------------------------------------------------------------------
  Swaps two values
+******************************************************************************}
Procedure Swap(var Value1,Value2:Integer); overload;
Procedure Swap(var Value1,Value2:Double); overload;

{******************************************************************************+
  Update
--------------------------------------------------------------------------------
  Updates the value with the new value. Changed is set to true if value and
  new-value are different. Returns the value of Changed.
+******************************************************************************}
Function Update(var Value:Boolean;const NewValue:Boolean;var Changed:Boolean):Boolean; overload;
Function Update(var Value:Byte;const NewValue:Byte;var Changed:Boolean):Boolean; overload;
Function Update(var Value:Word;const NewValue:Word;var Changed:Boolean):Boolean; overload;
Function Update(var Value:ShortInt;const NewValue:ShortInt;var Changed:Boolean):Boolean; overload;
Function Update(var Value:SmallInt;const NewValue:SmallInt;var Changed:Boolean):Boolean; overload;
Function Update(var Value:Integer;const NewValue:Integer;var Changed:Boolean):Boolean; overload;
Function Update(var Value:Double;const NewValue:Double;var Changed:Boolean):Boolean; overload;

Function InflatedRect(const Rect:TRect;DX,DY:Integer):TRect; overload;

Function PointsEqual(const Point1,Point2:TPoint):Boolean;

Procedure SinCos(Theta:Double;var ASin,ACos:Double); register;

Function RectEmpty(const Rect:TRect):Boolean; overload;

Procedure MoveRect(var Rect:TRect;const DX,DY:Integer); overload;
Function MovedRect(const Rect:TRect;const DX,DY:Integer):TRect; overload;

Function RectInside(Rect:TRect;InRect:TRect):Boolean; overload;

Type TRectCorners = Array[0..3] of TPoint;

Procedure RectCorners(const Rect:TRect;out Corners:TRectCorners); overload;

Implementation
                      
Uses Classes;

Function Min(Const V1,V2:Integer):Integer;
begin
  if V1>V2 then Result:=V2
  else Result:=V1;
end;

Function Min(Const V1,V2:Int64):Int64;
begin
  if V1>V2 then Result:=V2
  else Result:=V1;
end;

Function Max(Const V1,V2:Integer):Integer;
begin
  if V1>V2 then Result:=V1
  else Result:=V2;
end;

Function Max(Const V1,V2:Int64):Int64;
begin
  if V1>V2 then Result:=V1
  else Result:=V2;
end;

Function Min(Const V1,V2:Double):Double;
begin
  if V1>V2 then Result:=V2
  else Result:=V1;
end;

Function Max(Const V1,V2:Double):Double;
begin
  if V1>V2 then Result:=V1
  else Result:=V2;
end;

Function DblCompare
   (
   Const Value1    : Double;
   Const Value2    : Double
   )
   : Integer;
  begin
    if Abs(Value1-Value2)<Tolerance then Result:=0
    else if Value1<Value2 then Result:=-1
    else Result:=1;
  end;

Function LongCompare
   (
   Value1          : LongInt;
   Value2          : LongInt
   )
   : Integer;
  begin
    if Value1<Value2 then Result:=-1
    else if Value1>Value2 then Result:=1
    else Result:=0;
  end;

Function RectWidth
   (
   Const ARect     : TRect
   )
   : Integer;
  begin
    Result:=Abs(ARect.Right-ARect.Left);
  end;

Function RectHeight
   (
   Const ARect     : TRect
   )
   : Integer;
  begin
    Result:=Abs(ARect.Bottom-ARect.Top);
  end;

Function CenterRectInRect
   (
   Const ToCenter   : TRect;
   Const InRect     : TRect
   )
   : TRect;
  begin
    Result.Left:=(InRect.Right+InRect.Left-ToCenter.Right+ToCenter.Left) Div 2;
    Result.Top:=(InRect.Bottom+InRect.Top-ToCenter.Bottom+ToCenter.Top) Div 2;
    Result.Right:=Result.Left+RectWidth(ToCenter);
    Result.Bottom:=Result.Top+RectHeight(ToCenter);
  end;

Function CenterWidthInRect
   (
   Width           : Integer;
   Const InRect    : TRect
   )
   : Integer;
  begin
    Result:=(InRect.Right+InRect.Left-Width) Div 2;
  end;

Function CenterHeightInRect
   (
   Height          : Integer;
   Const InRect    : TRect
   )
   : Integer;
  begin
    Result:=(InRect.Bottom+InRect.Top-Height) Div 2;
  end;

Function InflatedWidth(Const ARect:TRect;ALeft,ARight:Integer):TRect;
begin
  with ARect do Result:=Rect(Left-ALeft,Top,Right+ARight,Bottom);
end;

Procedure InflateWidth(var ARect:TRect;ALeft,ARight:Integer);
begin
  Dec(ARect.Left,ALeft);
  Inc(ARect.Right,ARight);
end;

Function InflatedHeight(Const ARect:TRect;ATop,ABottom:Integer):TRect;
begin
  with ARect do Result:=Rect(Left,Top-ATop,Bottom,Bottom+ABottom);
end;

Procedure InflateHeight(var ARect:TRect;ATop,ABottom:Integer);
begin
  Dec(ARect.Top,ATop);
  Inc(ARect.Bottom,ABottom);
end;

Procedure InsertPointInRect
   (
   APoint          : TPoint;
   var ARect       : TRect
   );
  begin
    with ARect,APoint do begin
      if X<Left then Left:=X;
      if X>Right then Right:=X;
      if Y<Top then Top:=Y;
      if Y>Bottom then Bottom:=Y;
    end;
  end;

Function ScaleRect
   (
   Const ARect     : TRect;
   Const XScale    : Double;
   Const YScale    : Double
   )
   : TRect;
  begin
    Result.Left:=Round(ARect.Left*XScale);
    Result.Right:=Round(ARect.Right*XScale);
    Result.Top:=Round(ARect.Top*YScale);
    Result.Bottom:=Round(ARect.Bottom*YScale);
  end;

Procedure MovePoint(var APoint:TPoint;DX,DY:Integer);
begin
  Inc(APoint.X,DX);
  Inc(APoint.Y,DY);
end;

Function MovedPoint(const Point:TPoint;const DeltaX,DeltaY:Integer):TPoint;
begin
  Result.X:=Point.X+DeltaX;
  Result.Y:=Point.Y+DeltaY;
end;

{*******************************************************************************
| Function IntCheckRange
|-------------------------------------------------------------------------------
| Determines if value lies within the specified intervall.
+******************************************************************************}
Function IntCheckRange
   (
   Value           : LongInt;
   V1              : LongInt;
   V2              : LongInt
   )
   : Boolean;
  begin
    Result:=(Value>=V1) and (Value<=V2);
  end; 

{*******************************************************************************
| Function DblCheckRange
|-------------------------------------------------------------------------------
| Determines if value lies within the specified intervall.
+******************************************************************************}
Function DblCheckRange
   (
   Const Value     : Double;
   Const V1        : Double;
   Const V2        : Double
   )
   : Boolean;
  begin
    Result:=(Value-V1>=-Tolerance) and (Value-V2<=Tolerance);
  end; 

Procedure Swap(var Value1,Value2:Integer);
var Temp         : Integer;
begin
  Temp:=Value1;
  Value1:=Value2;
  Value2:=Temp;
end;

Procedure Swap(var Value1,Value2:Double);
var Temp         : Double;
begin
  Temp:=Value1;
  Value1:=Value2;
  Value2:=Temp;
end;

Function PointsEqual(const Point1,Point2:TPoint):Boolean;
begin
  Result:=(Point1.X=Point2.X) and (Point1.Y=Point2.Y);
end;

Procedure SinCos(Theta:Double;var ASin,ACos:Double);
{++ Ivanoff BUG#309 BUILD#100
The calculation of sine or cosine of angle, which equals -2E301, causes exception
when they are calculated by assembler procedure. The standard procedures of Delphi
do not cause this exception.}
{asm
        FLD     Theta
        FSINCOS
        FSTP    qword ptr [edx]
        FSTP    qword ptr [eax]
        FWAIT}
Begin
      ASin := Sin(Theta);
      ACos := Cos(Theta);
{-- Ivanoff}
end;

Function RectEmpty(const Rect:TRect):Boolean;
begin
  Result:=(Rect.Right<=Rect.Left) or (Rect.Top<=Rect.Bottom);
end;

Function Update(var Value:Boolean;const NewValue:Boolean;var Changed:Boolean):Boolean;
begin
  if Value<>NewValue then begin
    Value:=NewValue;
    Changed:=TRUE;
  end;
  Result:=Changed;
end;

Function Update(var Value:Byte;const NewValue:Byte;var Changed:Boolean):Boolean;
begin
  if Value<>NewValue then begin
    Value:=NewValue;
    Changed:=TRUE;
  end;
  Result:=Changed;
end;

Function Update(var Value:Word;const NewValue:Word;var Changed:Boolean):Boolean;
begin
  if Value<>NewValue then begin
    Value:=NewValue;
    Changed:=TRUE;
  end;
  Result:=Changed;
end;

Function Update(var Value:ShortInt;const NewValue:ShortInt;var Changed:Boolean):Boolean;
begin
  if Value<>NewValue then begin
    Value:=NewValue;
    Changed:=TRUE;
  end;
  Result:=Changed;
end;

Function Update(var Value:SmallInt;const NewValue:SmallInt;var Changed:Boolean):Boolean;
begin
  if Value<>NewValue then begin
    Value:=NewValue;
    Changed:=TRUE;
  end;
  Result:=Changed;
end;

Function Update(var Value:Integer;const NewValue:Integer;var Changed:Boolean):Boolean;
begin
  if Value<>NewValue then begin
    Value:=NewValue;
    Changed:=TRUE;
  end;
  Result:=Changed;
end;

Function Update(var Value:Double;const NewValue:Double;var Changed:Boolean):Boolean;
begin
  if Abs(Value-NewValue)>=Tolerance then begin
    Value:=NewValue;
    Changed:=TRUE;
  end;
  Result:=Changed;
end;

Function InflatedRect(const Rect:TRect;DX,DY:Integer):TRect;
begin
  Result:=Rect;
  InflateRect(Result,DX,DY);
end;

Function InflatedLeft(Const Rect:TRect;ALeft:Integer):TRect;
begin
  Result:=Rect;
  Inc(Result.Left,ALeft);
end;

Function InflatedRight(Const Rect:TRect;ARight:Integer):TRect;
begin
  Result:=Rect;
  Dec(Result.Right,ARight);
end;

Procedure InflateLeft(var Rect:TRect;ALeft:Integer);
begin
  Inc(Rect.Left,ALeft);
end;

Procedure InflateRight(var Rect:TRect;ARight:Integer);
begin
  Dec(Rect.Right,ARight);
end;

Function InflatedWidthHeight(Const ARect:TRect;ALeft:Integer;ARight:Integer;ATop:Integer;ABottom:Integer):TRect;
begin
  with ARect do Result:=Rect(Left-ALeft,Top-ATop,Right+ARight,Bottom+ABottom);
end;

Procedure MoveRect(var Rect:TRect;const DX,DY:Integer);
begin
  with Rect do begin
    Left:=Left+DX;
    Right:=Right+DX;
    Top:=Top+DY;
    Bottom:=Bottom+DY;
  end;
end;

Function MovedRect(const Rect:TRect;const DX,DY:Integer):TRect;
begin
  with Result do begin
    Left:=Rect.Left+DX;
    Right:=Rect.Right+DX;
    Top:=Rect.Top+DY;
    Bottom:=Rect.Bottom+DY;
  end;
end;

Function RectInside(Rect:TRect;InRect:TRect):Boolean;
begin
  Result:=FALSE;
  with Rect do  if Left>=InRect.Left then if Right<=InRect.Right then
      if Top>=InRect.Top then if Bottom<=InRect.Bottom then Result:=TRUE;
end;

Procedure RectCorners(const Rect:TRect;out Corners:TRectCorners);
begin
  with Rect do begin
    Corners[0]:=Point(Left,Top);
    Corners[1]:=Point(Left,Bottom);
    Corners[2]:=Point(Right,Bottom);
    Corners[3]:=Point(Right,Top);
  end;
end;

end.
