{**********************************************}
{   TAreaSeries Component Editor Dialog        }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeAreaEdit;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls, QComCtrls,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, ComCtrls,
     {$ENDIF}
     Chart, Series, TeCanvas, TeePenDlg, MultiLng;

type
  TAreaSeriesEditor = class(TForm)
    RGMultiArea: TRadioGroup;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    CBStairs: TCheckBox;
    CBAreaBrush: TComboBox;
    BAreaLinesPen: TButtonPen;
    BAreaLinePen: TButtonPen;
    GroupBox1: TGroupBox;
    CBColorEach: TCheckBox;
    BAreaColor: TButtonColor;
    CBInvStairs: TCheckBox;
    GroupBox3: TGroupBox;
    CBUseOrigin: TCheckBox;
    Label1: TLabel;
    EOrigin: TEdit;
    UDOrigin: TUpDown;
    MlgSection1: TMlgSection;
    procedure FormShow(Sender: TObject);
    procedure RGMultiAreaClick(Sender: TObject);
    procedure CBColorEachClick(Sender: TObject);
    procedure CBStairsClick(Sender: TObject);
    procedure CBAreaBrushChange(Sender: TObject);
    procedure CBInvStairsClick(Sender: TObject);
    procedure CBUseOriginClick(Sender: TObject);
    procedure EOriginChange(Sender: TObject);
  private
    { Private declarations }
    Area : TAreaSeries;
    Procedure EnableOrigin;
    Function GetAreaColor:TColor;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeEngine, TeeBrushDlg, TeePoEdi, TeeProcs, TeeConst;

procedure TAreaSeriesEditor.FormShow(Sender: TObject);
begin
  Area:=TAreaSeries(Tag);
  With Area do
  begin
    RGMultiArea.ItemIndex :=Ord(MultiArea);
    CBColorEach.Checked   :=ColorEachPoint;
    CBAreaBrush.ItemIndex :=Ord(AreaBrush);
    CBStairs.Checked      :=Stairs;
    CBInvStairs.Checked   :=InvertedStairs;
    CBInvStairs.Enabled   :=Stairs;
    CBUseOrigin.Checked   :=UseYOrigin;
    UDOrigin.Position     :=Round(YOrigin);
    BAreaLinesPen.LinkPen(AreaLinesPen);
    BAreaLinePen.LinkPen(LinePen);
  end;
  With BAreaColor do
  begin
    GetColorProc:=GetAreaColor;
    LinkProperty(Area,'SeriesColor');
    Enabled:=not Area.ColorEachPoint;
  end;
  EnableOrigin;
  TeeInsertPointerForm(Parent,Area.Pointer,TeeMsg_GalleryPoint);
end;

procedure TAreaSeriesEditor.RGMultiAreaClick(Sender: TObject);
begin
  Area.MultiArea:=TMultiArea(RGMultiArea.ItemIndex);
end;

procedure TAreaSeriesEditor.CBColorEachClick(Sender: TObject);
begin
  Area.ColorEachPoint:=CBColorEach.Checked;
  BAreaColor.Enabled:=not Area.ColorEachPoint;
end;

Function TAreaSeriesEditor.GetAreaColor:TColor;
begin
  With Area do
  if AreaColor=clTeeColor then result:=SeriesColor
                          else result:=AreaColor;
end;

procedure TAreaSeriesEditor.CBStairsClick(Sender: TObject);
begin
  Area.Stairs:=CBStairs.Checked;
  CBInvStairs.Enabled:=Area.Stairs;
end;

procedure TAreaSeriesEditor.CBAreaBrushChange(Sender: TObject);
begin
  Area.AreaBrush:=TBrushStyle(CBAreaBrush.ItemIndex);
end;

procedure TAreaSeriesEditor.CBInvStairsClick(Sender: TObject);
begin
  Area.InvertedStairs:=CBInvStairs.Checked;
end;

procedure TAreaSeriesEditor.CBUseOriginClick(Sender: TObject);
begin
  Area.UseYOrigin:=CBUseOrigin.Checked;
  EnableOrigin;
end;

procedure TAreaSeriesEditor.EOriginChange(Sender: TObject);
begin
  if Showing then Area.YOrigin:=UDOrigin.Position
end;

Procedure TAreaSeriesEditor.EnableOrigin;
begin
  EOrigin.Enabled:=CBUseOrigin.Checked;
  UDOrigin.Enabled:=EOrigin.Enabled;
end;

initialization
  RegisterClass(TAreaSeriesEditor);
end.
