{**********************************************}
{   TCandleSeries Component Editor Dialog      }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeCandlEdi;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Chart, Series, CandleCh, TeCanvas, ExtCtrls, ComCtrls, TeePenDlg;

type
  TCandleEditor = class(TForm)
    RGStyle: TRadioGroup;
    Label1: TLabel;
    SEWidth: TEdit;
    CBShowOpen: TCheckBox;
    CBShowClose: TCheckBox;
    Button1: TButtonPen;
    UDWidth: TUpDown;
    CBDraw3D: TCheckBox;
    CBDark3D: TCheckBox;
    BUpColor: TButtonColor;
    BDownColor: TButtonColor;
    procedure FormShow(Sender: TObject);
    procedure RGStyleClick(Sender: TObject);
    procedure SEWidthChange(Sender: TObject);
    procedure CBShowOpenClick(Sender: TObject);
    procedure CBShowCloseClick(Sender: TObject);
    procedure CBDraw3DClick(Sender: TObject);
    procedure CBDark3DClick(Sender: TObject);
  private
    { Private declarations }
    Candle : TCandleSeries;
    Procedure RefreshControls;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeeProcs;

Procedure TCandleEditor.RefreshControls;
begin
  CBShowOpen.Enabled :=Candle.CandleStyle=csCandleBar;
  CBShowClose.Enabled:=CBShowOpen.Enabled;
  CBDraw3D.Enabled:=not CBShowOpen.Enabled;
  CBDark3D.Enabled:=CBDraw3D.Enabled;
end;

procedure TCandleEditor.FormShow(Sender: TObject);
begin
  Candle:=TCandleSeries(Tag);
  With Candle do
  begin
    Case CandleStyle of
      csCandleStick: RGStyle.ItemIndex:=0;
      csCandleBar:   RGStyle.ItemIndex:=1;
    else RGStyle.ItemIndex:=2;
    end;
    UDWidth.Position      :=CandleWidth;
    CBShowOpen.Checked    :=ShowOpenTick;
    CBShowClose.Checked   :=ShowCloseTick;
    CBDraw3D.Checked      :=Draw3D;
    CBDark3D.Checked      :=Dark3D;
    Button1.LinkPen(Pen);
    RefreshControls;
  end;
  BUpColor.LinkProperty(Candle,'UpCloseColor');
  BDownColor.LinkProperty(Candle,'DownCloseColor');
end;

procedure TCandleEditor.RGStyleClick(Sender: TObject);
begin
  with Candle do
  Case RGStyle.ItemIndex of
    0: CandleStyle:=csCandleStick;
    1: CandleStyle:=csCandleBar;
    2: CandleStyle:=csOpenClose;
  end;
  RefreshControls;
end;

procedure TCandleEditor.SEWidthChange(Sender: TObject);
begin
  if Showing then Candle.CandleWidth:=UDWidth.Position;
end;

procedure TCandleEditor.CBShowOpenClick(Sender: TObject);
begin
  Candle.ShowOpenTick:=CBShowOpen.Checked;
end;

procedure TCandleEditor.CBShowCloseClick(Sender: TObject);
begin
  Candle.ShowCloseTick:=CBShowClose.Checked;
end;

procedure TCandleEditor.CBDraw3DClick(Sender: TObject);
begin
  Candle.Draw3D:=CBDraw3D.Checked;
end;

procedure TCandleEditor.CBDark3DClick(Sender: TObject);
begin
  Candle.Dark3D:=CBDark3D.Checked;
end;

initialization
  RegisterClass(TCandleEditor);
end.
