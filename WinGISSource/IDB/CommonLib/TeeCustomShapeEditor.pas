{**********************************************}
{  TChartObject (or derived) Editor Dialog     }
{  Copyright (c) 1999-2000 by David Berneda    }
{**********************************************}
{$I TeeDefs.inc}
unit TeeCustomShapeEditor;

interface

uses
  {$IFDEF LINUX}
  LibC,
  {$ELSE}
  Windows, Messages,
  {$ENDIF}
  SysUtils, Classes,
  {$IFDEF CLX}
  QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls, QComCtrls,
  {$ELSE}
  Graphics, Controls, Forms, Dialogs, ComCtrls, StdCtrls, ExtCtrls,
  {$ENDIF}
  TeeProcs, TeeEdiGrad, TeeEdiFont, TeCanvas, TeePenDlg, MultiLng;

type
  TFormTeeShape = class(TForm)
    PC1: TPageControl;
    TabFormat: TTabSheet;
    BBackColor: TButtonColor;
    Button4: TButtonPen;
    Button6: TButton;
    CBRound: TCheckBox;
    CBTransparent: TCheckBox;
    TabGradient: TTabSheet;
    TabText: TTabSheet;
    CBBevel: TComboBox;
    TabShadow: TTabSheet;
    BShadowColor: TButtonColor;
    Label1: TLabel;
    Edit1: TEdit;
    UDShadowSize: TUpDown;
    Label2: TLabel;
    Label3: TLabel;
    EBevWidth: TEdit;
    UDBevW: TUpDown;
    MlgSection1: TMlgSection;
    procedure BColorClick(Sender: TObject);
    procedure SEShadowSizeChange(Sender: TObject);
    procedure CBRoundClick(Sender: TObject);
    procedure BBrushClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBTransparentClick(Sender: TObject);
    procedure CBBevelChange(Sender: TObject);
    procedure EBevWidthChange(Sender: TObject);
  private
    { Private declarations }
    CreatingForm : Boolean;
    FGradientEditor:TTeeGradientEditor;
    FFontEditor  : TTeeFontEditor;
  public
    { Public declarations }
    TheShape    : TTeeCustomShape;
    procedure RefreshControls(AShape:TTeeCustomShape);
  end;

Function InsertTeeObjectForm(APageControl:TPageControl; AShape:TTeeCustomShape):TFormTeeShape;

implementation

{$R *.dfm}
Uses TeeConst, TeeBrushDlg;

Function InsertTeeObjectForm(APageControl:TPageControl; AShape:TTeeCustomShape):TFormTeeShape;
begin
  result:=TFormTeeShape.Create(APageControl.Owner);
  with result do
  begin
    TheShape:=AShape;
    BorderStyle:={$IFDEF CLX}fbsNone{$ELSE}bsNone{$ENDIF};
    Align:=alClient;
    While PC1.PageCount>0 do PC1.Pages[0].PageControl:=APageControl;
  end;
end;

procedure TFormTeeShape.BColorClick(Sender: TObject);
begin
  CBTransparent.Checked:=False;
end;

procedure TFormTeeShape.SEShadowSizeChange(Sender: TObject);
begin
  if not CreatingForm then TheShape.ShadowSize:=UDShadowSize.Position;
end;

procedure TFormTeeShape.CBRoundClick(Sender: TObject);
begin
  if CBRound.Checked then TheShape.ShapeStyle:=fosRoundRectangle
                     else TheShape.ShapeStyle:=fosRectangle
end;

procedure TFormTeeShape.BBrushClick(Sender: TObject);
begin
  EditChartBrush(Self,TheShape.Brush);
end;

procedure TFormTeeShape.RefreshControls(AShape:TTeeCustomShape);
begin
  CreatingForm:=True;
  TheShape:=AShape;
  With TheShape do
  begin
    UDShadowSize.Position  :=ShadowSize;
    CBRound.Checked        :=ShapeStyle=fosRoundRectangle;
    CBTransparent.Checked  :=Transparent;
    CBBevel.ItemIndex      :=Ord(Bevel);
    UDBevW.Position        :=BevelWidth;
    FGradientEditor.RefreshGradient(Gradient);
    FFontEditor.RefreshControls(Font);
    Button4.LinkPen(Frame);
  end;
  BBackColor.LinkProperty(TheShape,'Color');
  BShadowColor.LinkProperty(TheShape,'ShadowColor');
  CreatingForm:=False;
end;

procedure TFormTeeShape.FormShow(Sender: TObject);
begin
  PC1.ActivePage:=TabFormat;
  if Assigned(TheShape) then RefreshControls(TheShape);
end;

procedure TFormTeeShape.FormCreate(Sender: TObject);
begin
  CreatingForm:=True;
  FGradientEditor:=TTeeGradientEditor.Create(Self);
  With FGradientEditor do
  begin
    BOk.Visible:=False;
    BCancel.Visible:=False;
    Height:=Height-BOk.Height;
  end;
  AddFormTo(FGradientEditor,TabGradient,0);
  FFontEditor:=InsertTeeFontEditor(TabText);
end;

procedure TFormTeeShape.CBTransparentClick(Sender: TObject);
begin
  TheShape.Transparent:=CBTransparent.Checked;
end;

procedure TFormTeeShape.CBBevelChange(Sender: TObject);
begin
  TheShape.Bevel:=TPanelBevel(CBBevel.ItemIndex);
end;

procedure TFormTeeShape.EBevWidthChange(Sender: TObject);
begin
  if not CreatingForm then TheShape.BevelWidth:=UDBevW.Position
end;

end.
