�
 TFORMTEEWALL 0^  TPF0TFormTeeWallFormTeeWallLeft� TopfWidthrHeight� ActiveControlCBView3dWallsBorderIcons Color	clBtnFace
ParentFont	Position	poDefaultOnShowFormShowPixelsPerInch`
TextHeight 	TCheckBoxCBView3dWallsLeftTop	WidthYHeightHelpContext�Caption&Visible WallsTabOrder OnClickCBView3dWallsClick  TTabControlTabSubWallsLeftHTop Width� Height� HotTrack	TabOrderTabs.StringsLeftRightBottomBack TabIndex OnChangeTabSubWallsChange TLabelL33LeftuTopfWidthHeight	AlignmenttaRightJustifyAutoSizeCaptionSiz&e:FocusControl
SEWallSize  TButtonColor
BWallColorLeftTop HelpContext� Caption	&Color...TabOrder   
TButtonPenBWallPenLeftTop@HelpContextyCaption
B&order...TabOrder  TButton
BWallBrushLeftTop`WidthKHeightHelpContexttCaption&Pattern...TabOrderOnClickBWallBrushClick  TEdit
SEWallSizeLeft� TopbWidthHeightHelpContextsColorclWindowTabOrderText0OnChangeSEWallSizeChange  	TCheckBoxCBTranspLeftxTop� Width`HeightHelpContextCaption&TransparentTabOrderOnClickCBTranspClick  TUpDown
UDWallSizeLeft� TopbWidthHeight	Associate
SEWallSizeMin Position TabOrderWrap  	TCheckBoxCBDark3DLeftxTopDWidthaHeightHelpContext� Caption&Dark 3DTabOrderOnClickCBDark3DClick  TButton	BGradientLeftTop� WidthKHeightHelpContext�Caption&Gradient...TabOrderVisibleOnClickBGradientClick  	TCheckBox	CBVisibleLeftxTop$Width`HeightHelpContext�CaptionV&isibleTabOrderOnClickCBVisibleClick    