unit TeeWindRoseEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeePolarEditor, StdCtrls, ComCtrls, TeCanvas, TeePenDlg;

type
  TWindRoseEditor = class(TPolarSeriesEditor)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TWindRoseEditor.FormCreate(Sender: TObject);
begin
  inherited;
  CBClockWise.Visible:=False;
end;

initialization
  RegisterClass(TWindRoseEditor);
end.
