{********************************************}
{     TeeChart Pro Charting Library          }
{ Copyright (c) 1995-2000 by David Berneda   }
{         All Rights Reserved                }
{********************************************}
unit TeeDBEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DB, StdCtrls, TeEngine, ExtCtrls, TeeSourceEdit;

type
  TBaseDBChartEditor = class(TBaseSourceEditor)
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure FillSourceDatasets;
    procedure FillSources;
  protected
    Function DataSet:TDataSet;
    Function IsValid(AComponent:TComponent):Boolean; override;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

Uses TeeConst;

procedure TBaseDBChartEditor.FillSourceDatasets;
begin
  CBSources.Items.Clear;
  FillSources;
  With CBSources do ItemIndex:=Items.IndexOfObject(TheSeries.DataSource);
end;

Function TBaseDBChartEditor.IsValid(AComponent:TComponent):Boolean;
begin
  result:=AComponent is TDataSet;
end;

procedure TBaseDBChartEditor.FillSources;

  Procedure FillSourcesForm(AOwner:TComponent);
  var t : Integer;
  begin
    if Assigned(AOwner) then
    With AOwner do
    for t:=0 to ComponentCount-1 do
        AddComponentDataSource(Components[t],CBSources.Items,True);
  end;

var t : Integer;
begin
  With Screen do
  for t:=0 to DataModuleCount-1 do FillSourcesForm(DataModules[t]);
  FillSourcesForm(TheSeries.ParentChart.Owner);
end;

procedure TBaseDBChartEditor.FormShow(Sender: TObject);
begin
  inherited;
  LLabel.Caption:=TeeMsg_AskDataSet;
  FillSourceDatasets;
  With CBSources do ItemIndex:=Items.IndexOfObject(TheSeries.DataSource);
end;

function TBaseDBChartEditor.DataSet: TDataSet;
begin
  With CBSources do
  if ItemIndex=-1 then result:=nil
                  else result:=TDataSet(Items.Objects[ItemIndex]);
end;

end.
