Unit MultiLng;
  
Interface

Uses Classes,Dialogs,Forms,Hash,SysUtils;

{$H+}

Type EMlgError          = Class(Exception);

     {**************************************************************************
       Class TMlgStringList
     ---------------------------------------------------------------------------
       Verwaltet die String-Eintr�ge des String-Files (Sections und Strings).
     ---------------------------------------------------------------------------
       Sections = Liefert die Abschnitts-Nummer zu einem Abschnitts-Namen
       SectionCount = Anzahl der Abschnitte
       SectionNames = Zugriff auf die Abschnitts-Namen �ber den Listenindex
       SectionStrings = Zugriff auf die Strings �ber Abschnitts-Nummer und
           String-Nummer
       Strings =  Zugriff auf die Strings �ber Abschnitts-Namen und String-Nummer
     ---------------------------------------------------------------------------
       AddSection = F�gt einen neuen Abschnitt ein
       AddString = F�gt einen neuen String in den angegebenen Abschnitt ein
       FindSection = Liefert die Abschnitts-Nummer zu einem Abschnitts-Namen
     +*************************************************************************}
     TMlgStringList     = Class(TComponent)
      Private
       FSections        : TStringList;
       FStrings         : TAutoRecHashTable;
       Function    GetString(ASection,AString:Integer):String;
       Function    GetSection(Const ASection:String):Integer;
       Function    GetSectionCount:Integer;
       Function    GetSectionName(AIndex:Integer):String;
       Function    GetSectionString(Const ASection:String;AString:Integer):String;
      Protected
       Procedure   AssignTo(Dest:TPersistent); override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Function    AddSection(const AName:String):Integer;
       Procedure   AddString(ASection,ANumber:Integer;const AString:String);
       Function    FindSection(Const ASection:String):Integer;
       Property    Sections[Const AName:String]:Integer read GetSection;
       Property    SectionCount:Integer read GetSectionCount;
       Property    SectionNames[AIndex:Integer]:String read GetSectionName;
       Property    SectionStrings[ASection,AString:Integer]:String read GetString;
       Property    Strings[Const ASection:String;AString:Integer]:String read GetSectionString; default;
     end;

     {**************************************************************************
       Class TMlgSection
     ---------------------------------------------------------------------------
       Komponente die die Mehrsprachigkeit f�r Delphi-Formulare realisiert. Die
       Komponente mu� auf dem Formular plaziert werden. Beim Laden des Formulars
       automatisch in alllen String-Eigenschaften bzw. String-Listen-Eigenschaften
       Strings im Format !Nummer durch den zugeh�rigen Eintrag im String-File
       ersetzt.
     ---------------------------------------------------------------------------
       Strings = Zugriff auf die String-Eintr�ge im zugeh�rigen Abschnitt
       Section = Name des Abschnitts im String-File aus dem die Texte geladen
           werden.
     +*************************************************************************}
     TMlgSection        = Class(TComponent)
      Private
       FSection         : String;
       FSectionNumber   : Integer;
       FSwitchSupport   : Boolean;
       FReplacementList : TStringList;
       Function    GetString(ANumber:Integer):String;
      Protected
       Procedure   Loaded; override;
      Public
       Property    Strings[ANumber:Integer]:String read GetString; default;
      Published
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Procedure   OnLanguageChanged; 
       Property    Section:String read FSection write FSection;
       // set this property to true if the parent exists when a laguage-switch
       // occurs, replacement-information will be stored in that case
       Property    SwitchLanguageSupport:Boolean read FSwitchSupport write FSwitchSupport default FALSE;
     end;

Function MessageDialog(Const Msg:String;DlgType:TMsgDlgType;
    Buttons:TMsgDlgButtons;HelpCtx:LongInt=0;DefaultButton:TMsgDlgBtn=mbOK):Word;
Function MessageDialogPos(Const Msg:String;DlgType:TMsgDlgType;
    Buttons:TMsgDlgButtons;HelpCtx:Longint;X,Y:Integer;DefaultButton:TMsgDlgBtn=mbOK):Word;
Function MessageDialogPosHelp(Const Msg:String;DlgType:TMsgDlgType;
    Buttons:TMsgDlgButtons;HelpCtx:LongInt;X,Y:Integer;
    Const HelpFileName:String;DefaultButton:TMsgDlgBtn=mbOK):Word;
Function CreateMessageDialog(Const Msg:String;DlgType:TMsgDlgType;
    Buttons:TMsgDlgButtons;DefaultButton:TMsgDlgBtn=mbOK):TForm;

Procedure ReplaceMlgStrings(Component:TObject;const SectionName:String;
    ReplacementList:TStringList=NIL);    

var MlgStringList       : TMlgStringList;
    // list of components that must be informed about a language-change
    // this components must have a published method named OnLanguageChanged
    LanguageDependend   : TList;

Implementation

Uses Controls,ExtCtrls,Graphics,NumTools,StdCtrls,TypInfo,Windows,
     StrTools;

{===============================================================================
| TMlgStringList
+==============================================================================}

Type PMlgString         = ^TMlgString;
     TMlgString         = Record
      Number            : LongInt;
      Text              : PString;
     end;

     TMlgHashTable      = Class(TAutoRecHashTable)
       Procedure   OnFreeItem(Item:Pointer); override;
     end;

Constructor TMlgStringList.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    FSections:=TStringList.Create;
    FStrings:=TMlgHashTable.Create(1111,SizeOf(TMlgString));
  end;

Destructor TMlgStringList.Destroy;
  begin
    FSections.Free;
    FStrings.Free;
    inherited Destroy;
  end;

Function TMlgStringList.GetString
   (
   ASection        : Integer;
   AString         : Integer
   )
   : String;
  var Number       : LongInt;
      MlgString    : PMlgString;
  begin
    LongRec(Number).Hi:=ASection+1;
    LongRec(Number).Lo:=AString;
    MlgString:=FStrings.Find(Number);
    if MlgString=NIL then Result:='Unknown'+IntToStr(AString)
    else Result:=MlgString^.Text^;
  end;

Function TMlgStringList.GetSectionName
   (
   AIndex        : Integer
   )
   : String;
  begin
    Result:=FSections[AIndex];
  end;

Function TMlgStringList.AddSection
   (
   const AName     : String
   )
   : Integer;
  var ASection     : Integer;
  begin
    ASection:=FindSection(AName);
    if ASection>0 then Raise EMlgError.Create('The Section '+AName+' already exists');
    ASection:=FSections.Count;
    FSections.Add(AName);
    Result:=ASection;
  end;

Procedure TMlgStringList.AddString
   (
   ASection        : Integer;
   ANumber         : Integer;
   const AString   : String
   );
  var Number       : LongInt;
      MlgString    : TMlgString;
  begin
    LongRec(Number).Hi:=ASection+1;
    LongRec(Number).Lo:=ANumber;
    if FStrings.Find(Number)<>NIL then Raise EMlgError.Create('String '+IntToStr(ANumber)+' already inserted');
    MlgString.Number:=Number;
    MlgString.Text:=NewStr(AString);
    FStrings.Add(MlgString);
  end;

Function TMlgStringList.FindSection
   (
   Const ASection  : String
   )
   : Integer;
  begin
    for Result:=0 to FSections.Count-1 do
        if CompareText(FSections[Result],ASection)=0 then Exit;
    Result:=-1;
  end;

Function TMlgStringList.GetSection
   (
   Const ASection  : String
   )                             
   : Integer;
  begin                              
    Result:=FindSection(ASection);
    //Assert(Result>=0,'The Section '+ASection+' does not exist');
  end;

Function TMlgStringList.GetSectionCount
   : Integer;
  begin
    Result:=FSections.Count;
  end;

Function TMlgStringList.GetSectionString
   (
   Const ASection  : String;
   AString         : Integer
   )
   : String;
  var BSection     : Integer;
  begin
    BSection:=Sections[ASection];
    Result:=SectionStrings[BSection,AString];
  end;

Procedure TMlgStringList.AssignTo(Dest:TPersistent);
begin
  Assert(Dest is TMlgStringList);
  with Dest as TMlgStringList do begin
    Swap(Integer(FSections),Integer(Self.FSections));
    Swap(Integer(FStrings),Integer(Self.FStrings));
  end;
end;

{==============================================================================+
  TMlgSection
+==============================================================================}

Constructor TMlgSection.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  LanguageDependend.Add(Self);
end;

{******************************************************************************+
  Procedure TMlgSection.Loaded
--------------------------------------------------------------------------------
  Ersetzt alle String-Eigenschaften und alle Strings in String-Listen die mit
  einem Rufzeichen beginnen durch den zugeh�rigen Text im String-File.
+******************************************************************************}
Procedure TMlgSection.Loaded;
begin
  if (Owner<>NIL) and not (csDesigning in ComponentState) then begin
    FSectionNumber:=MlgStringList.Sections[Section];
    ReplaceMlgStrings(Owner,Section);
  end;
end;

Destructor TMlgSection.Destroy;
begin
  FReplacementList.Free;
  LanguageDependend.Remove(Self);
  inherited Destroy;
end;

Procedure TMlgSection.OnLanguageChanged;
begin
  if FSectionNumber>=0 then FSectionNumber:=MlgStringList.Sections[Section];
end;

{==============================================================================+
  Other
+==============================================================================}

Procedure ReplaceMlgStrings(Component:TObject;const SectionName:String;
                            ReplacementList:TStringList);
var Cnt          : Integer;
    SectionNumber: Integer;

   Procedure SetStringProps(AComponent:TObject);
   var Replaced   : Boolean;

      Function Replace(Text:String):String;
      var CurPos     : PChar;
          LastPos    : PChar;
          Number     : PChar;
      begin
         Replaced:=FALSE;
         if Text='' then
            Result:=''
         else
            begin
               Result:='';
               CurPos:=@Text[1];
               LastPos:=CurPos;
               repeat
                  CurPos:=AnsiStrScan(CurPos,'!');
                  if CurPos<>NIL then
                     Result:=Result+StrLPas(LastPos,CurPos-LastPos)
                  else
                     Result:=Result+StrLPas(LastPos,StrEnd(LastPos)-LastPos);
                  if CurPos<>NIL then
                     begin
                        CurPos:=CurPos+1;
                        Number:=CurPos;
                        while (Number^>='0') and (Number^<='9') do
                           Number:=Number+1;
                        if Number<>CurPos then
                           begin
                              Result:=Result+MlgStringList.SectionStrings[SectionNumber,
                                               StrToInt(StrLPas(CurPos,Number-CurPos))];
                              LastPos:=Number;
                              Replaced:=TRUE;
                           end
                        else
                           LastPos:=Number-1;
                     end;
               until CurPos=NIL;
            end;
      end;

   var PropInfo   : PPropInfo;
       PropList   : PPropList;
       PropCnt    : Integer;
       Cnt        : Integer;
       Cnt1       : Integer;
       AObject    : TObject;
  {$IFDEF WIN32}
       ACaption   : AnsiString;
  {$ELSE}
       ACaption   : String;
   Const tkLString     = tkString;
  {$ENDIF}
   begin
      if AComponent<>NIL then
         begin
            New(PropList);
            try
               PropCnt:=GetPropList(AComponent.ClassInfo,[tkClass,tkString,tkLString],PropList);
               for Cnt:=0 to PropCnt-1 do
                   begin
                      PropInfo:=PropList^[Cnt];
                      if PropInfo^.PropType^.Kind in [tkString,tkLString] then
                         begin
                            ACaption:=Replace(GetStrProp(AComponent,PropInfo));
                            if Replaced then
                               SetStrProp(AComponent,PropInfo,ACaption);
                         end
                      else
                         if PropInfo^.PropType^.Kind=tkClass then
                            begin
                               AObject:=Pointer(GetOrdProp(AComponent,PropInfo));
                               if AObject is TStrings then
                                  with AObject as TStrings do
                                     begin
                                        BeginUpdate;
                                        for Cnt1:=0 to Count-1 do
                                            if Copy(Strings[Cnt1],1,1)='!' then
                                               Strings[Cnt1]:=MlgStringList.SectionStrings[SectionNumber,
                                                  StrToInt(Copy(Strings[Cnt1],2,Length(Strings[Cnt1])-1))];
                                        EndUpdate;
                                     end
                               else
                                  if AObject is TCollection then
                                     with AObject as TCollection do
                                        for Cnt1:=0 to Count-1 do
                                            SetStringProps(Items[Cnt1])
                                  else
                                     if AObject is TComponent then
                                        with AObject as TComponent do
                                           for Cnt1:=0 to ComponentCount-1 do
                                               SetStringProps(Components[Cnt1])
                                     else
                                        SetStringProps(AObject);
                            end;
                   end;
            finally
               Dispose(PropList);
            end;
         end;
   end;

begin
   SectionNumber:=MlgStringList.Sections[SectionName];
   SetStringProps(Component);
   if Component is TComponent then
      with Component as TComponent do
         for Cnt:=0 to ComponentCount-1 do
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
             if Components[Cnt].ClassName <> 'TChart' then
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
                SetStringProps(Components[Cnt]);
end;

{******************************************************************************+
  Function TMlgSection.GetString
--------------------------------------------------------------------------------
+******************************************************************************}
Function TMlgSection.GetString(ANumber:Integer):String;
begin
  Result:=MlgStringList.SectionStrings[FSectionNumber,ANumber];
end;

{==============================================================================+
  TMlgHashTable
+==============================================================================}

Procedure TMlgHashTable.OnFreeItem(Item:Pointer);
begin
  with PMlgString(Item)^ do if Text<>NullStr then DisposeStr(Text);
end;

{==============================================================================+
  MessageDialoge
+==============================================================================}

Const IconIDs        : Array[TMsgDlgType] of PChar = (IDI_EXCLAMATION,IDI_HAND,
                         IDI_ASTERISK,IDI_QUESTION,NIL);
      {$IFDEF WIN32}
      ModalResults   : Array[TMsgDlgBtn] of Integer = (mrYes,mrNo,mrOk,mrCancel,
                         mrAbort,mrRetry,mrIgnore,mrAll,mrNoToAll,mrYesToAll,0);
      {$ELSE}
      ModalResults   : Array[TMsgDlgBtn] of Integer = (mrYes,mrNo,mrOk,mrCancel,
                         mrAbort,mrRetry,mrIgnore,mrAll,0);
      {$ENDIF}

Type TMessageForm    = Class(TForm)
      Private
       Procedure     HelpButtonClick(Sender:TObject);
      Public
       Constructor   CreateNew(AOwner:TComponent;Dummy:Integer = 0); override;
     end;

Constructor TMessageForm.CreateNew(AOwner:TComponent;Dummy:Integer=0);
{$IFDEF WIN32}
var NonClientMetrics  : TNonClientMetrics;
{$ENDIF}
begin
  inherited CreateNew(AOwner);
  {$IFDEF WIN32}
  NonClientMetrics.cbSize:=sizeof(NonClientMetrics);
  if SystemParametersInfo(SPI_GETNONCLIENTMETRICS,0,@NonClientMetrics,0) then
      Font.Handle:=CreateFontIndirect(NonClientMetrics.lfMessageFont);
  {$ELSE}
  Font.Name:='MS Sans Serif';
  Font.Height:=-11;
  {$ENDIF}
end;

Procedure TMessageForm.HelpButtonClick(Sender: TObject);
  begin
    Application.HelpContext(HelpContext);
  end;

Function GetAveCharSize
   (
   Canvas          : TCanvas
   )
   : TPoint;
  var I            : Integer;
      Buffer       : Array[0..51] of Char;
  begin
    for I:=0 to 25 do Buffer[I]:=Chr(I+Ord('A'));
    for I:=0 to 25 do Buffer[I+26]:=Chr(I+Ord('a'));
    GetTextExtentPoint(Canvas.Handle,Buffer,52,TSize(Result));
    Result.X:=Result.X div 52;
  end;

Function Max
   (
   Value1          : Integer;            
   Value2          : Integer
   )
   : Integer;
  begin
    if Value1>Value2 then Result:=Value1
    else Result:=Value2;
  end;   

Function CreateMessageDialog
   (
   Const Msg       : String;
   DlgType         : TMsgDlgType;
   Buttons         : TMsgDlgButtons;
   DefaultButton   : TMsgDlgBtn
   )
   : TForm;
  const mcHorzMargin    = 8;
        mcVertMargin    = 6;
        mcHorzSpacing   = 10;
        mcVertSpacing   = 10;
        mcButtonWidth   = 60;
        mcButtonHeight  = 14;
        mcButtonSpacing = 4;
  var DialogUnits       : TPoint;
      HorzMargin        : Integer;
      VertMargin        : Integer;
      HorzSpacing       : Integer;
      VertSpacing       : Integer;
      ButtonWidth       : Integer;
      ButtonHeight      : Integer;
      ButtonSpacing     : Integer;
      ButtonCount       : Integer;
      ButtonGroupWidth  : Integer;
      IconTextWidth     : Integer;
      IconTextHeight    : Integer;
      X                 : Integer;
      B                 : TMsgDlgBtn;
      CancelButton      : TMsgDlgBtn;
      IconID            : PChar;
      TextRect          : TRect;
      Button            : TButton;
  begin
    Result:=TMessageForm.CreateNew(Application);
    with Result do begin
      BorderStyle:=bsDialog;
      Canvas.Font:=Font;
      DialogUnits:=GetAveCharSize(Canvas);
      HorzMargin:=MulDiv(mcHorzMargin,DialogUnits.X,4);
      VertMargin:=MulDiv(mcVertMargin,DialogUnits.Y,8);
      HorzSpacing:=MulDiv(mcHorzSpacing,DialogUnits.X,4);
      VertSpacing:=MulDiv(mcVertSpacing,DialogUnits.Y,8);
      ButtonWidth:=MulDiv(mcButtonWidth,DialogUnits.X,4);
      ButtonHeight:=MulDiv(mcButtonHeight,DialogUnits.Y,8);
      ButtonSpacing:=MulDiv(mcButtonSpacing,DialogUnits.X,4);
      SetRect(TextRect,0,0,Screen.Width Div 2,0);
      DrawText(Canvas.Handle,@Msg[1],Length(Msg),TextRect,
          DT_EXPANDTABS or DT_CALCRECT or DT_WORDBREAK);
      IconID:=IconIDs[DlgType];
      IconTextWidth:=TextRect.Right;
      IconTextHeight:=TextRect.Bottom;
      if IconID<>nil then begin
        Inc(IconTextWidth,32+HorzSpacing);
        if IconTextHeight<32 then IconTextHeight:=32;
      end;
      ButtonCount:=0;
      for B:=Low(TMsgDlgBtn) to High(TMsgDlgBtn) do
        if B in Buttons then Inc(ButtonCount);
      ButtonGroupWidth:=0;
      if ButtonCount<>0 then ButtonGroupWidth:=ButtonWidth*ButtonCount+
          ButtonSpacing*(ButtonCount-1);
      ClientWidth:=Max(IconTextWidth,ButtonGroupWidth)+HorzMargin*2;
      ClientHeight:=IconTextHeight+ButtonHeight+VertSpacing+
          VertMargin*2;
      Left:=(Screen.Width Div 2)-(Width Div 2);
      Top:=(Screen.Height Div 2)-(Height Div 2);
      if DlgType<>mtCustom then Caption:=MlgStringList['MessageDialog',Integer(DlgType)+1]
      else Caption:=Application.Title;
      if IconID<>NIL then  with TImage.Create(Result) do begin
        Parent:=Result;
        Picture.Icon.Handle:=LoadIcon(0,IconID);
        SetBounds(HorzMargin,VertMargin,32,32);
      end;
      with TLabel.Create(Result) do begin
        Name:='Message';
        Parent:=Result;
        WordWrap:=True;
        Caption:=Msg;
        BoundsRect:=TextRect;
        SetBounds(IconTextWidth-TextRect.Right+HorzMargin,VertMargin,
          TextRect.Right,TextRect.Bottom);
      end;
      if not (DefaultButton in Buttons) then begin
        if mbOk in Buttons then DefaultButton:=mbOk
        else if mbYes in Buttons then DefaultButton:=mbYes
        else DefaultButton:=mbRetry;
      end;  
      if mbCancel in Buttons then CancelButton:=mbCancel
      else if mbNo in Buttons then CancelButton:=mbNo
      else CancelButton:=mbOk;
      X:=(ClientWidth-ButtonGroupWidth) div 2;
      for B:=Low(TMsgDlgBtn) to High(TMsgDlgBtn) do
          if B in Buttons then begin
        Button:=TButton.Create(Result);
        with Button do begin
          Parent:=Result;
          Caption:=MlgStringList['MessageDialog',Integer(B)+50];
          ModalResult:=ModalResults[B];
          if B=DefaultButton then begin
            Default:=True;
            ActiveControl:=Button;
          end;
          if B=CancelButton then Cancel:=True;
          SetBounds(X,IconTextHeight+VertMargin+VertSpacing,ButtonWidth,ButtonHeight);
          Inc(X,ButtonWidth+ButtonSpacing);
          if B=mbHelp then OnClick:=TMessageForm(Result).HelpButtonClick;
        end;  
      end;
    end;
  end;

Function MessageDialog
   (
   Const Msg       : String;
   DlgType         : TMsgDlgType;
   Buttons         : TMsgDlgButtons;
   HelpCtx         : LongInt;
   DefaultButton   : TMsgDlgBtn
   )
   : Word;
  begin
    Result:=MessageDialogPosHelp(Msg,DlgType,Buttons,HelpCtx,-1,-1,'',DefaultButton);
  end;

Function MessageDialogPos
   (
   const Msg       : String;
   DlgType         : TMsgDlgType;
   Buttons         : TMsgDlgButtons;
   HelpCtx         : Longint;
   X               : Integer;
   Y               : Integer;
   DefaultButton   : TMsgDlgBtn
   )
   : Word;
  begin
    Result:=MessageDialogPosHelp(Msg,DlgType,Buttons,HelpCtx,X,Y,'',DefaultButton);
  end;

Function MessageDialogPosHelp
   (
   Const Msg       : String;
   DlgType         : TMsgDlgType;
   Buttons         : TMsgDlgButtons;
   HelpCtx         : LongInt;
   X               : Integer;
   Y               : Integer;
   Const HelpFileName   : String;
   DefaultButton   : TMsgDlgBtn
   )
   : Word;
  begin
    with CreateMessageDialog(Msg,DlgType,Buttons,DefaultButton) do try
      HelpContext:=HelpCtx;
{      HelpFile:=HelpFileName;}
      if X>=0 then Left:=X;
      if Y>=0 then Top:=Y;
      Result:=ShowModal;
    finally
      Free;
    end;
  end;

{==============================================================================+
  Initialization- and termination-code
+==============================================================================}

Initialization
  MlgStringList:=TMlgStringList.Create(NIL);
  LanguageDependend:=TList.Create;

Finalization
  MlgStringList.Free;
  LanguageDependend.Free;
end. 
