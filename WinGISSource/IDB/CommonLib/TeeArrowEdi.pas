{**********************************************}
{   TArrowSeries Editor Dialog                 }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeArrowEdi;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, ArrowCha, ComCtrls, TeCanvas, TeePenDlg;

type
  TArrowSeriesEditor = class(TForm)
    BPen: TButtonPen;
    BBrush: TButton;
    Label1: TLabel;
    SEArrowWidth: TEdit;
    Label2: TLabel;
    SEArrowHeight: TEdit;
    GroupBox1: TGroupBox;
    CBColorEach: TCheckBox;
    BArrowColor: TButtonColor;
    UDArrowWidth: TUpDown;
    UDArrowHeight: TUpDown;
    procedure CBColorEachClick(Sender: TObject);
    procedure BBrushClick(Sender: TObject);
    procedure SEArrowWidthChange(Sender: TObject);
    procedure SEArrowHeightChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    Arrow : TArrowSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeeBrushDlg, TeEngine, TeeProcs;

procedure TArrowSeriesEditor.CBColorEachClick(Sender: TObject);
begin
  Arrow.ColorEachPoint:=CBColorEach.Checked;
  BArrowColor.Enabled:=not Arrow.ColorEachPoint;
  if Arrow.ColorEachPoint then Arrow.Pointer.Brush.Color:=clTeeColor;
end;

procedure TArrowSeriesEditor.BBrushClick(Sender: TObject);
begin
  EditChartBrush(Self,Arrow.Pointer.Brush);
end;

procedure TArrowSeriesEditor.SEArrowWidthChange(Sender: TObject);
begin
  if Showing then Arrow.ArrowWidth:=UDArrowWidth.Position;
end;

procedure TArrowSeriesEditor.SEArrowHeightChange(Sender: TObject);
begin
  if Showing then Arrow.ArrowHeight:=UDArrowHeight.Position;
end;

procedure TArrowSeriesEditor.FormShow(Sender: TObject);
begin
  Arrow:=TArrowSeries(Tag);
  With Arrow do
  begin
    CBColorEach.Checked    :=ColorEachPoint;
    UDArrowWidth.Position  :=ArrowWidth;
    UDArrowHeight.Position :=ArrowHeight;
    BPen.LinkPen(Pointer.Pen);
  end;
  BArrowColor.LinkProperty(Arrow,'SeriesColor');
end;

initialization
  RegisterClass(TArrowSeriesEditor);
end.
