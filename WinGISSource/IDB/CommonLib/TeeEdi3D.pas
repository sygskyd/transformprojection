{**********************************************}
{   TeeChart Pro 3D editor options             }
{   Copyright (c) 1999-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeEdi3D;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QStdCtrls, QComCtrls, QExtCtrls,
     TeePenDlg,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls,
     {$ENDIF}
     TeeProcs, Chart, MultiLng;

type
  TFormTee3D = class(TForm)
    MlgSection1: TMlgSection;
    Panel1: TPanel;
    CBView3d: TCheckBox;
    SE3d: TEdit;
    UD3D: TUpDown;
    L13: TLabel;
    CBOrthogonal: TCheckBox;
    Label4: TLabel;
    EOrthoAngle: TEdit;
    UDOrthoAngle: TUpDown;
    L35: TLabel;
    SBRotation: TTrackBar;
    LRotation: TLabel;
    LElevation: TLabel;
    SBElevation: TTrackBar;
    L36: TLabel;
    SBPerspec: TTrackBar;
    LPerspec: TLabel;
    Label2: TLabel;
    procedure CBOrthogonalClick(Sender: TObject);
    procedure SBZoomChange(Sender: TObject);
    procedure SBRotationChange(Sender: TObject);
    procedure SBElevationChange(Sender: TObject);
    procedure CBView3dClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SBHOffsetChange(Sender: TObject);
    procedure SBVOffsetChange(Sender: TObject);
    procedure SE3dChange(Sender: TObject);
    procedure CBZoomTextClick(Sender: TObject);
    procedure SBPerspecChange(Sender: TObject);
    procedure EOrthoAngleChange(Sender: TObject);
  private
    { Private declarations }
    TheChart : TCustomChart;
  public
    { Public declarations }
    AllowRotation : Boolean;
    Constructor CreateChart(AOwner:TComponent; AChart:TCustomChart);
    Procedure CheckRotation;
  end;

implementation

{$R *.dfm}
uses TeCanvas;

Constructor TFormTee3D.CreateChart(AOwner:TComponent; AChart:TCustomChart);
begin
  inherited Create(AOwner);
  TheChart:=AChart;
end;

Procedure TFormTee3D.CheckRotation;
begin
  With TheChart do
  begin
    CBOrthogonal.Enabled:=View3D and AllowRotation;
    SBRotation.Enabled  :=CBOrthogonal.Enabled and (not View3DOptions.Orthogonal);
    SBElevation.Enabled :=SBRotation.Enabled;
    SBPerspec.Enabled :=SBRotation.Enabled;
    EOrthoAngle.Enabled:=CBOrthogonal.Enabled;
    UDOrthoAngle.Enabled:=EOrthoAngle.Enabled;
  end;
end;

procedure TFormTee3D.CBOrthogonalClick(Sender: TObject);
begin
  With TheChart.View3DOptions do
  begin
    Orthogonal:=CBOrthogonal.Checked;
    SBRotation.Enabled:=(not Orthogonal) and AllowRotation;
    SBElevation.Enabled:=not Orthogonal;
    SBPerspec.Enabled:=not Orthogonal;
    EOrthoAngle.Enabled:=Orthogonal;
    UDOrthoAngle.Enabled:=Orthogonal;
  end;
end;

procedure TFormTee3D.SBZoomChange(Sender: TObject);
begin
//  TheChart.View3DOptions.Zoom:=SBZoom.Position;
//  LZoom.Caption:=IntToStr(SBZoom.Position)+'%';
end;

procedure TFormTee3D.SBRotationChange(Sender: TObject);
begin
  TheChart.View3DOptions.Rotation:=SBRotation.Position;
  LRotation.Caption:=IntToStr(SBRotation.Position);
end;

procedure TFormTee3D.SBElevationChange(Sender: TObject);
begin
  TheChart.View3DOptions.Elevation:=SBElevation.Position;
  LElevation.Caption:=IntToStr(SBElevation.Position);
end;

procedure TFormTee3D.CBView3dClick(Sender: TObject);
var tmp:Boolean;
begin
  With TheChart do
  Begin
    View3D              :=CBView3D.Checked;
    SE3D.Enabled        :=View3D;
    UD3D.Enabled        :=SE3D.Enabled;
    CBOrthogonal.Enabled:=View3D and AllowRotation;
    tmp:=View3D and (not View3DOptions.Orthogonal);
    SBRotation.Enabled:=tmp and AllowRotation;
    SBElevation.Enabled:=tmp;
    SBPerspec.Enabled:=tmp;
//    SBHOffset.Enabled:=View3D;
//    SBVOffset.Enabled:=View3D;
//    SBZoom.Enabled:=View3D;
//    CBZoomText.Enabled:=View3D;
    EOrthoAngle.Enabled:=CBOrthogonal.Enabled;
    UDOrthoAngle.Enabled:=EOrthoAngle.Enabled;
  end;
  CheckRotation;
end;

procedure TFormTee3D.FormShow(Sender: TObject);
var tmp : Integer;
begin
  With TheChart do
  begin
    CBView3D.Checked :=View3D;
    SE3D.Enabled     :=View3D;
    UD3D.Position    :=Chart3DPercent;

    if Canvas.SupportsFullRotation then tmp:=1
                                   else tmp:=270;
    SBRotation.Min  :=tmp;
    SBElevation.Min :=tmp;

    CBOrthogonal.Enabled :=View3D and AllowRotation;

    With View3DOptions do
    begin
//      SBZoom.Position      :=Zoom;
      CBOrthogonal.Checked :=Orthogonal;
      SBRotation.Position  :=Rotation;
      SBElevation.Position :=Elevation;
//      SBHOffset.Position   :=HorizOffset;
//      SBVOffset.Position   :=VertOffset;
      SBRotation.Enabled   :=CBOrthogonal.Enabled and (not Orthogonal);
//      CBZoomText.Checked   :=ZoomText;
      SBPerspec.Position   :=Perspective;
      UDOrthoAngle.Position:=OrthoAngle;
    end;
    EOrthoAngle.Enabled:=CBOrthogonal.Enabled;
    UDOrthoAngle.Enabled:=EOrthoAngle.Enabled;
  end;
  {$IFNDEF D4}
  LPerspec.Caption:=IntToStr(SBPerspec.Position);
  LVOffset.Caption:=IntToStr(SBVOffset.Position);
  LHOffset.Caption:=IntToStr(SBHOffset.Position);
  LElevation.Caption:=IntToStr(SBElevation.Position);
  LRotation.Caption:=IntToStr(SBRotation.Position);
  {$ENDIF}
end;

procedure TFormTee3D.SBHOffsetChange(Sender: TObject);
begin
//  TheChart.View3DOptions.HorizOffset:=SBHOffset.Position;
//  LHOffset.Caption:=IntToStr(SBHOffset.Position);
end;

procedure TFormTee3D.SBVOffsetChange(Sender: TObject);
begin
//  TheChart.View3DOptions.VertOffset:=SBVOffset.Position;
//  LVOffset.Caption:=IntToStr(SBVOffset.Position);
end;

procedure TFormTee3D.SE3dChange(Sender: TObject);
begin
  if Showing then TheChart.Chart3DPercent:=UD3D.Position;
end;

procedure TFormTee3D.CBZoomTextClick(Sender: TObject);
begin
//  TheChart.View3DOptions.ZoomText:=CBZoomText.Checked;
end;

procedure TFormTee3D.SBPerspecChange(Sender: TObject);
begin
  TheChart.View3DOptions.Perspective:=SBPerspec.Position;
  LPerspec.Caption:=IntToStr(SBPerspec.Position);
end;

procedure TFormTee3D.EOrthoAngleChange(Sender: TObject);
begin
  if Showing then TheChart.View3DOptions.OrthoAngle:=UDOrthoAngle.Position;
end;

end.
