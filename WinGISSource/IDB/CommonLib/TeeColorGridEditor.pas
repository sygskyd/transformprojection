unit TeeColorGridEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeeGriEd, StdCtrls, TeCanvas, ComCtrls, TeePenDlg;

type
  TColorGridEditor = class(TGrid3DSeriesEditor)
    BGrid: TButtonPen;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeeSurfa;

procedure TColorGridEditor.FormShow(Sender: TObject);
begin
  inherited;
  ShowControls(False,[LDepth,EDepth,UDDepth]);
  BGrid.LinkPen(TCustom3DPaletteSeries(Tag).Pen);
end;

initialization
  RegisterClass(TColorGridEditor);
end.
