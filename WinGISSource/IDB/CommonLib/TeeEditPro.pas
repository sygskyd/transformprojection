{********************************************}
{    TeeChart Extended Series Editors        }
{   Copyright (c) 1996-2000 by David Berneda }
{********************************************}
{$I teedefs.inc}
unit TeeEditPro;

interface

Uses Windows, Controls, Chart, TeEngine;

{ Show the user the dialog associated to the Series }
Procedure EditOneSeries(AOwner:TControl; ASeries:TChartSeries);

{ Show the user a "Save to..." dialog to save it to disk. }
Function SaveChartDialog(AChart:TCustomChart):String;

implementation

{ This unit should be "used" by your applications if showing the
  Chart Editor with "Extended" Series and Functions. }

uses Classes, Forms, SysUtils, Dialogs,
     EditChar, StatChar, TeeCandlEdi, TeeVolEd, TeeSurfEdit, TeePolarEditor,
     TeeErrBarEd, CurvFitt, TeeContourEdit, TeeConst, TeeProco, TeeStore,
     TeeBezie, TeePo3DEdit,
     TeeCount, TeeCumu, TeeDonutEdit, TeeCursorEdit, TeeTriSurfEdit,
     MyPoint, Bar3D, BigCandl, ImaPoint, ImageBar, TeeImaEd, TeeRose,
     TeeBoxPlot, TeeHistoEdit, TeeTools, TeeColorLineEditor, TeeColorBandEdit,
     TeeAxisArrowEdit, TeeDrawLineEditor, TeeImageToolEdit,
     TeeBarJoinEditor, TeeLinePointEditor,
     TeeHighLowEdit, TeeClockEditor, TeeBoxPlotEditor, TeeWindRoseEditor,
     TeeColorGridEditor, TeeWaterFallEdit, TeeSmith, TeePyramid, TeePyramidEdit,
     TeeNearestToolEditor, TeeSmithEdit, TeeCalendarEditor,
     {$IFDEF D4}TeeMapSeries, TeeMapSeriesEdit, {$ENDIF}
     TeeMarksTipToolEdit, TeeDragMarksEdit,
     TeeFunnel, TeeFunnelEditor;

{$R TeeProBm.res}

{ This method shows the "Save As..." dialog to save a Chart }
Function SaveChartDialog(AChart:TCustomChart):String;
begin
  result:='';
  With TSaveDialog.Create(nil) do
  try
    {$IFDEF CLX}
    Filters.Add(TeeMsg_TeeFiles+
          ' (*.' +
          TeeMsg_TeeExtension +
          ')');
    {$ELSE}
    Filter:=TeeMsg_TeeFiles+
          ' (*.' +
          TeeMsg_TeeExtension +
          ')|*.' +
          TeeMsg_TeeExtension;
    Options:=[ofHideReadOnly,ofOverwritePrompt];
    {$ENDIF}
    DefaultExt:=TeeMsg_TeeExtension;
    if Execute then
    begin
      SaveChartToFile(AChart,FileName{$IFNDEF D4},True{$ENDIF});
      result:=FileName;
    end;
  finally
    Free;
  end;
end;

type TSeriesAccess=class(TCustomChartSeries);

{ This method shows the Series editor }
Procedure EditOneSeries(AOwner:TControl; ASeries:TChartSeries);
var tmpClass : TFormClass;
    tmp      : TForm;
    tmpEdit  : String;
begin
  tmpEdit:=TSeriesAccess(ASeries).GetEditorClass;
  tmpClass:=TFormClass(GetClass(tmpEdit));
  if Assigned(tmpClass) then
  begin
    tmp:=tmpClass.Create(AOwner);
    With tmp do
    try
      {$IFNDEF CLX}
      Position:=poScreenCenter;
      {$ENDIF}
      BorderStyle:={$IFDEF CLX}fbsDialog{$ELSE}bsDialog{$ENDIF};
      BorderIcons:=[biSystemMenu];
      Scaled:=False;
      Caption:=Format(TeeMsg_Editing,[ASeries.Name]);
      Tag:=Integer(ASeries);
      {$IFNDEF CLX}
      Height:=Height+GetSystemMetrics(SM_CYDLGFRAME)+GetSystemMetrics(SM_CYCAPTION);
      {$ENDIF}
      ShowModal;
    finally
      Free;
    end;
  end
  else raise Exception.CreateFmt(TeeMsg_CannotFindEditor,[tmpEdit]);
end;

end.
