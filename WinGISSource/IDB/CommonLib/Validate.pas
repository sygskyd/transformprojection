Unit Validate;

Interface

{$H+}

Uses Windows,Classes,Controls,Forms,Graphics,Measures,Menus,Messages,
     StdCtrls,SysUtils;

Const veBlankError      = 1;
      veFullLengthError = 2;
      veRangeError      = 3;
      veInvalidInput    = 4;
      veTooSmallError   = 5;
      veTooBigError     = 6;
      veNoFileError     = 7;
      veNoPathError     = 8;
      veRdOnlyPathError = 9;

Type TFilterSet         = Set of Char;
     TValidatorClass    = Class of TCustomValidator;

     TValErrorProc      = Procedure(Sender:TObject;ErrorCode:Word) of Object;

     TCaseConversion    = (ccNone,ccAllUpperCase,ccAllLowerCase,ccFirstUpperCase);

     TCustomValidator = Class(TComponent)
      Private
       FAutoSkip        : Boolean;
       FCombobox        : TComboBox;
       FEdit            : TCustomEdit;
       FFullLength      : Boolean;
       FHistory         : TStringList;
       FHistorySize     : Integer;
       FHookProc        : Pointer;
       FHookParentProc  : Pointer;
       FHookWnd         : THandle;
       FHookParent      : TCustomForm;
       FInvalidColor    : TColor;
       FLeaveBlank      : Boolean;
       FMaxLength       : Integer;
       FOnValError      : TValErrorProc;
       FPopupMenu       : TPopupMenu;
       FValidChars      : TFilterSet;
       FValOnExit       : Boolean;
{++ Ivanoff}
       FFormatWhenExit: Boolean;  { This variable determines is there a necessity to format data in assigned
                                    control or not. For example, we can have in floating validator '123456,789'
                                    and after foramtting this value will be transformed in '123 456,789'.
                                    I think that sometimes this functionality is excessively. }
       FShowMyselfPopup: Boolean; { This variable determines will the validator create itself popup menu or not.
                                    If not, the assigned control will show itself popup menu. I added this
                                    variable because the validator always guess that there is a language file
                                    like WinGIS.lng and this file contains 'Validators' string section. I want
                                    to use validators in cases when such file does not exist. }
{-- Ivanoff}
       FOrgWndProc      : Pointer;
       FOrgParentWndProc: Pointer;
       Procedure   DoHookWnd;
       Procedure   DoHookParentWnd;
       Procedure   DoSaveHistory;
       Procedure   DoStandardMenus(Sender:TObject);
       Procedure   DoUnHookWnd;
       Procedure   DoUnhookParentWnd;
       Function    GetAsBoolean:Boolean;
       Function    GetAsFloat:Double;
       Function    GetAsInteger:LongInt;
       Function    GetAsText:String;
       Function    GetControl:TWinControl;
       Function    GetEditText:String;
       Function    GetHistoryCount:Integer;
       Function    GetHistoryItem(AIndex:Integer):String;
       Function    GetValidChars:String;
       Procedure   HookWndProc(var Message:TMessage);
       Procedure   MoveCursorToEnd;
       Procedure   HookParentWndProc(var Message:TMessage);
       Procedure   SetAsBoolean(AValue:Boolean);
       Procedure   SetAsFloat(AValue:Double);
       Procedure   SetAsInteger(AValue:LongInt);
       Procedure   SetAsText(AValue:String);
       Procedure   SetComboBox(ACombo:TComboBox);
       Procedure   SetEdit(AEdit:TCustomEdit);
       Procedure   SetEditText(Const AText:String);
       Procedure   SetValidChars(AChars:String);
       Property    Text:String read GetEditText write SetEditText;
       Procedure   UpdateColor;
       Procedure   UpdateHistory;
      Protected
       Procedure   AddToHistory(const EditText:String);
       Function    CheckValid(Const S:String):Word; virtual;
       Procedure   DoAutoSkip;
       Procedure   DoError(ErrorCode:Word); virtual;
       Procedure   DoExit;
       Function    DoKeyPress(var Msg:TWMChar):Boolean;
       Function    DoPopup(X,Y:Integer):Boolean; virtual;
       Function    FormatOnExit(var S:String):Boolean; virtual;
       Property    FullLength:Boolean read FFullLength write FFullLength default False;
       Procedure   GetData(DataType:Integer;var Data); virtual;
       Function    GetPopupMenu:TPopupMenu; virtual;
       Function    IsValid(Const S:String):Boolean; virtual;
       Function    IsValidInput(var S:String;var CaretPos:Integer):Boolean; virtual;
       Procedure   Notification(AComponent:TComponent;Operation:TOperation); override;
       Procedure   SetData(DataType:Integer;var Data); virtual;
{++ BUG#447. Ivanoff}
       Function DoPaste: Boolean;
{-- BUG#447. Ivanoff}
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Procedure   AddHistory(const Text:String); overload;
       Property    AsBoolean:Boolean read GetAsBoolean write SetAsBoolean;
       Property    AsFloat:Double read GetAsFloat write SetAsFloat;
       Property    AsInteger:LongInt read GetAsInteger write SetAsInteger;
       Property    AsText:String read GetAsText write SetAsText;
       Procedure   Clear;
       Procedure   ClearHistory;
       Property    Control:TWinControl read GetControl;
       Property    HistoryCount:Integer read GetHistoryCount;
       Property    HistorySize:Integer read FHistorySize write FHistorySize default 6;
       Property    HistoryStrings[AIndex:Integer]:String read GetHistoryItem;
       Property    MaxLength:Integer read FMaxLength write FMaxLength;
       Procedure   SetFocus;
       Function    ValidateInput:Boolean;
       Property    ValidChars:String read GetValidChars write SetValidChars;
       Function    ValidInput:Boolean;
      Published
       Property    AutoSkip:Boolean read FAutoSkip write FAutoSkip default False;
       Property    ComboBox:TComboBox read FComboBox write SetComboBox;
       Property    Edit:TCustomEdit read FEdit write SetEdit;
       Property    InvalidColor:TColor read FInvalidColor write FInvalidColor default clRed;
       Property    LeaveBlank:Boolean read FLeaveBlank write FLeaveBlank default False;
       Property    OnError:TValErrorProc read FOnValError write FOnValError;
       Property    ValidateOnExit:Boolean read FValOnExit write FValOnExit default True;
       {++ Ivanoff}
       Property    FormatWhenExit: Boolean Read FFormatWhenExit Write FFormatWhenExit Default TRUE;
       Property    ShowMyselfPopup: Boolean Read FShowMyselfPopup Write FShowMyselfPopup Default TRUE;
       {-- Ivanoff}
     end;

     TValidator     = Class(TCustomValidator)
      Private
       FCaseConversion  : TCaseConversion;
       FTrailingBlanks  : Boolean;
       FLeadingBlanks   : Boolean;
      Protected
       Function    FormatOnExit(var S:String):Boolean; override;
       Function    CheckValid(Const S:String):Word; override;
       Function    IsValidInput(var S:String;var CaretPos:Integer):Boolean; override;
       Procedure   GetData(DataType:Integer;var Data); override;
      Published
       Property    CaseConversion:TCaseConversion read FCaseConversion write FCaseConversion default ccNone;
       Property    FullLength;
       Property    LeadingBlanks:Boolean read FLeadingBlanks write FLeadingBlanks default False;
       Property    MaxLength;
       Property    TrailingBlanks:Boolean read FTrailingBlanks write FTrailingBlanks default False;
       Property    ValidChars;
     end;

     TFileValidator = Class(TValidator)
      Protected
       Function    CheckValid(Const S:String):Word; override;
       Procedure   GetData(DataType:Integer;var Data); override;
       Procedure   DoError(ErrorCode:Word); override;
     end;

     TPathValidator = Class(TValidator)
      Protected
       Function    CheckValid(Const S:String):Word; override;
       Procedure   GetData(DataType:Integer;var Data); override;
       Procedure   DoError(ErrorCode:Word); override;
     end;

     TIntValidator  = Class(TCustomValidator)
      Private
       FAllowSign   : Boolean;
       FDefaultValue: LongInt;
       FDelLeading  : Boolean;
       FLeadingZeros: Boolean;
       FMax         : LongInt;
       FMin         : LongInt;
       FShowZero    : Boolean;
       Function    GetHistoryItem(AIndex:Integer):Integer;
       Procedure   SetAllowSign(AAllow:Boolean);
      Protected
       Procedure   DoError(ErrorCode:Word); override;
       Function    FormatOnExit(var S:String):Boolean; override;
       Procedure   GetData(DataType:Integer;var Data); override;
       Function    CheckValid(Const S:String):Word; override;
       Function    IsValidInput(var S:String;var CaretPos:Integer):Boolean; override;
       Procedure   SetData(DataType:Integer;var Data); override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Procedure   AddHistory(Value:Integer); overload;
       Property    HistoryItems[AIndex:Integer]:Integer read GetHistoryItem;
      Published
       Property    AllowSign:Boolean read FAllowSign write SetAllowSign default True;
       Property    DefaultValue:LongInt read FDefaultValue write FDefaultValue default 0;
       Property    MinValue:LongInt read FMin write FMin default 0;
       Property    MaxLength;
       Property    MaxValue:LongInt read FMax write FMax default 0;
       Property    RemoveLeadingZeros:Boolean read FDelLeading write FDelLeading default False;
       Property    ShowLeadingZeros:Boolean read FLeadingZeros write FLeadingZeros default False;
       Property    ShowZeroValues:Boolean read FShowZero write FShowZero default True;
     end;

     TFloatValidator= Class(TCustomValidator)
      Private
       FAllowSign       : Boolean;
       FCommas          : Integer;
       FDefaultValue    : Double;
       FTrailingZeros   : Boolean;
       FMax             : Double;
       FMin             : Double;
       FShowZero        : Boolean;
       Function    GetHistoryItem(AIndex:Integer):Double;
       Procedure   SetAllowSign(AAllow:Boolean);
       Function    StoreDefault:Boolean;
       Function    StoreMin:Boolean;
       Function    StoreMax:Boolean;
       Function    TextForValue(Const AValue:Double):String;
      Protected
       Procedure   DoError(ErrorCode:Word); override;
       Function    FormatOnExit(var S:String):Boolean; override;
       Procedure   GetData(DataType:Integer;var Data); override;
       Function    CheckValid(Const S:String):Word; override;
       Function    IsValidInput(var S:String;var CaretPos:Integer):Boolean; override;
       Procedure   SetData(DataType:Integer;var Data); override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Procedure   AddHistory(Value:Double); overload;
       Property    HistoryItems[AIndex:Integer]:Double read GetHistoryItem;
      Published
       Property    AllowSign:Boolean read FAllowSign write SetAllowSign default True;
       Property    DefaultValue:Double read FDefaultValue write FDefaultValue stored StoreDefault;
       Property    Commas:Integer read FCommas write FCommas;
       Property    MinValue:Double read FMin write FMin stored StoreMin;
       Property    MaxLength;
       Property    MaxValue:Double read FMax write FMax stored StoreMax;
       Property    ShowZeroValues:Boolean read FShowZero write FShowZero default True;
       Property    ShowTrailingZeros:Boolean read FTrailingZeros write FTrailingZeros default False;
     end;

     TPicResult    = (prComplete,prIncomplete,prEmpty,prError,prSyntax,prAmbiguous,prIncompNoFill);

     TCustomPXValidator = Class(TCustomValidator)
      Private
       FPicture    : PString;
       Function    CheckPicture(var Input:string;AutoFill:Boolean):TPicResult;
       Function    GetPicture:String;
       Procedure   SetPicture(Const APicture:String);
      Protected
       Function    CheckValid(Const S:String):Word; override;
       Function    IsValidInput(var S:String;var CaretPos:Integer):Boolean; override;
       Property    Picture:String read GetPicture write SetPicture;
      Public
       Constructor Create(AOwner:TComponent); override;
     end;

     TPXValidator       = Class(TCustomPXValidator)
       Property    Picture;
     end;

     TTimeValidator     = Class(TCustomPXValidator)
      Private
       FDefaultValue    : TDateTime;
       FShowHundreds    : Boolean;
       Procedure   SetShowHundreds(AHundreds:Boolean);
       Function    GetTime:TDateTime;
       Procedure   SetTime(Const Time:TDateTime);
       Function    TextToTime(Const S:String;var Time:TDateTime):Boolean;
      Protected
       Function    CheckValid(Const S:String):Word; override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Property    AsTime:TDateTime read GetTime write SetTime;
       Property    DefaultValue:TDateTime read FDefaultValue write FDefaultValue stored false;
      Published
       Property    ShowHundreds:Boolean read FShowHundreds write SetShowHundreds;
     end;

     TScaleValidator    = Class(TCustomValidator)
      Private
       FCommas          : Integer;
       FDefaultValue    : Double;
       FMaxScale        : Double;
       FMinScale        : Double;
       FTrailingZeros   : Boolean;
       Function    GetHistoryItem(AIndex:Integer):Double;
       Procedure   SetCommas(ACommas:Integer);
       Function    StoreDefault:Boolean;
       Function    StoreMin:Boolean;
       Function    StoreMax:Boolean;
       Function    TextForValue(Const AValue:Double):String;
       Function    ValueFromText(Const S:String):Double;
      Protected
       Procedure   DoError(ErrorCode:Word); override;
       Function    FormatOnExit(var S:String):Boolean; override;
       Procedure   GetData(DataType:Integer;var Data); override;
       Function    CheckValid(Const S:String):Word; override;
       Function    IsValidInput(var S:String;var CaretPos:Integer):Boolean; override;
       Procedure   SetData(DataType:Integer;var Data); override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Property    HistoryItems[AIndex:Integer]:Double read GetHistoryItem;
      Published
       Property    Commas:Integer read FCommas write SetCommas default 0;
       Property    DefaultValue:Double read FDefaultValue write FDefaultValue stored StoreDefault;
       Property    MaxLength default 10;
       Property    MaxScale:Double read FMaxScale write FMaxScale stored StoreMax;
       Property    MinScale:Double read FMinScale write FMinScale stored StoreMin;
       Property    ShowTrailingZeros:Boolean read FTrailingZeros write FTrailingZeros default False;
     end;

     TMeasureValidator  = Class(TCustomValidator)
      Private
       FAllowedUnits    : TMeasureUnitsSet;
       FCommas          : Integer;
       FDefault         : TMeasure;
       FMeasure         : TMeasure;
       FMinValue        : TMeasure;
       FMaxValue        : TMeasure;
       FShowZero        : Boolean;
       FTrailingZeros   : Boolean;
       {brovak}
       FValUnitsChange  : TNotifyEvent;
       {brovak}

       Procedure   DoSetUnits(Sender:TObject);
       Procedure   DoConvertUnits(Sender:TObject);
       Function    GetUnits:TMeasureUnits;
       Function    GetUseableUnits:LongInt;
       Function    GetValue:Double;
       Function    GetValues(AUnits:TMeasureUnits):Double;
       Procedure   SetMeasure(AMeasure:TMeasure);
       Procedure   SetMax(AMeasure:TMeasure);
       Procedure   SetMin(AMeasure:TMeasure);
       Procedure   SetUnits(AUnits:TMeasureUnits);
       Procedure   SetUseableUnits(AUnits:LongInt);
       Procedure   SetValue(Const AValue:Double);
       Procedure   SetValues(AUnits:TMeasureUnits;Const AValue:Double);
       Function    StoreDefault:Boolean;
       Function    StoreMax:Boolean;
       Function    StoreMin:Boolean;
       Function    TextForValue(AMeasure:TMeasure):String;
       Function    ValueForText(var AValue:Double;var AUnits:TMeasureUnits;var ValueOK,NoUnits:Boolean):Boolean;
      Protected
       Function    CheckValid(Const S:String):Word; override;
       Procedure   DoError(ErrorCode:Word); override;
       Function    FormatOnExit(var S:String):Boolean; override;
       Procedure   GetData(DataType:Integer;var Data); override;
       Function    GetPopupMenu:TPopupMenu; override;
       Procedure   ReadState(Reader:TReader); override;
       Procedure   SetData(DataType:Integer;var Data); override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Property    Measure:TMeasure write SetMeasure;
       Property    Values[Units:TMeasureUnits]:Double read GetValues write SetValues; default;
       Property    AllowedUnits:TMeasureUnitsSet read FAllowedUnits write FAllowedUnits stored False;
      Published
       Property    UseableUnits:LongInt read GetUseableUnits write SetUseableUnits;
       Property    Commas:Integer read FCommas write FCommas default 0;
       Property    DefaultValue:TMeasure read FDefault write FDefault stored StoreDefault;
       Property    MaxLength default 10;
       Property    MinValue:TMeasure read FMinValue write SetMin stored StoreMin;
       Property    MaxValue:TMeasure read FMaxValue write SetMax stored StoreMax;
       Property    ShowZeroValues:Boolean read FShowZero write FShowZero default True;
       Property    ShowTrailingZeros:Boolean read FTrailingZeros write FTrailingZeros default False;
       Property    Units:TMeasureUnits read GetUnits write SetUnits default muMillimeters;
       Property    Value:Double read GetValue write SetValue;
       {brovak}
       Property    OnChangeUnits:TNotifyEvent read FValUnitsChange write FValUnitsChange;
       {brovak}
     end;

     TMeasureLookupValidator = Class(TMeasureValidator)
      Private
       FLookUpList      : TStringList;
       Procedure   DoSetSpecialValue(Sender:TObject);
       Function    GetLookupIndex:Integer;
       Procedure   SetLookupIndex(AIndex:Integer);
       Procedure   SetLookupList(AList:TStringList);
      Protected
       Function    FormatOnExit(var S:String):Boolean; override;
       Function    GetPopupMenu:TPopupMenu; override;
       Function    CheckValid(Const S:String):Word; override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Property    LookupIndex:Integer read GetLookupIndex write SetLookupIndex;
      Published
       Property    LookupList:TStringList read FLookupList write SetLookupList;
     end;

Function CheckValidators(Form:TForm):Boolean;

Procedure RehookValidators(Form:TForm);

Implementation

Uses ComCtrls,Dialogs,MultiLng,NumTools,StrTools,TabNotbk,TypInfo,FileCtrl
,TextUtils //+Sygsky
{++ BUG#447. Ivanoff}
, ClipBrd
{-- BUG#447. Ivanoff}
;

var WindowHooks    : TList;

{==============================================================================}
{ Diverses                                                                     }
{==============================================================================}

Function CheckValidators(Form:TForm):Boolean;
var TabList      : TList;
    Cnt          : Integer;
    Cnt1         : Integer;
    AItem        : TWinControl;
    AText        : String;
begin
  Result:=TRUE;
  if Form.ModalResult<>mrCancel then begin
    TabList:=TList.Create;
    try
      Form.GetTabOrderList(TabList);
      for Cnt:=0 to TabList.Count-1 do begin
        AItem:=TabList[Cnt];
        if AItem.Enabled then with TForm(AItem.Owner) do begin
          for Cnt1:=0 to ComponentCount-1 do if Components[Cnt1] is TCustomValidator then
              with Components[Cnt1] as TCustomValidator do
              if (Edit=AItem) or (ComboBox=AItem) then begin
            AText:=Text;
            if FormatOnExit(AText) then Text:=AText;
            if not ValidateInput then begin
              Result:=FALSE;
              Exit;
            end;
          end;
        end;
      end;
    finally
      TabList.Free;
    end;
  end;
end;

Procedure SaveValidatorHistories(Form:TCustomForm);
var Cnt          : Integer;
begin
  if Form.ModalResult<>mrCancel then with Form do begin
    for Cnt:=0 to ComponentCount-1 do if Components[Cnt] is TCustomValidator then
        TCustomValidator(Components[Cnt]).DoSaveHistory;
  end;
end;

Function GetErrorMessage
   (
   ErrorNumber     : Integer
   )
   : String;
  begin
    Result:=MlgStringList['Validators',ErrorNumber];
  end;

Procedure ReHookValidators(Form:TForm);
var Cnt          : Integer;
    Validator    : TCustomValidator;
begin
  for Cnt:=0 to Form.ComponentCount-1 do
      if Form.Components[Cnt] is TCustomValidator then begin
    Validator:=TCustomValidator(Form.Components[Cnt]);
    Validator.DoUnhookWnd;
    Validator.DoHookWnd;
  end;
end;

Procedure RemoveTrailingZeros
   (
   var Str         : String
   );
  var Cnt          : Integer;
  begin
    if Pos(DecimalSeparator,Str)<>0 then begin
      Cnt:=Length(Str);
      while Str[Cnt]='0' do begin
        Dec(Cnt);
        if Str[Cnt]=DecimalSeparator then begin
          Dec(Cnt);
          Break;
        end;
      end;
      Str:=Copy(Str,1,Cnt);
    end;
  end;

{******************************************************************************+
  Function StringToFloat
--------------------------------------------------------------------------------
  Converts a string to the corresponding float-value. Removes thousand-
  separators from the string which are not accepted by StrToFloat.
+******************************************************************************}
Function StringToFloat(const Text:String):Double;
begin
  if (Trim(Text)='-') or (Trim(Text)='+') then Result:=0
  else
      try
         Result:=StrToFloat(StringReplace(Text,ThousandSeparator,'',[rfReplaceAll]));
      except
         Result:=0;
      end;
end;

{==============================================================================}
{ TCustomPXValidator                                                                 }
{==============================================================================}

Constructor TCustomPXValidator.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    FPicture:=NullStr;
  end;

Function TCustomPXValidator.IsValidInput
   (
   var S           : String;
   var CaretPos    : Integer
   )
   : Boolean;
  begin
    if CaretPos=Length(S) then begin
      Result:=CheckPicture(S,TRUE)<>prError;
      CaretPos:=Length(S);
    end
    else Result:=CheckPicture(S,TRUE)<>prError;
  end;

Function TCustomPXValidator.CheckValid(Const S:String):Word;
var Text         : String;
begin
  Result:=inherited CheckValid(S);
  if (Result=0) and (S<>'') then begin
    Text:=S;
    if CheckPicture(Text,TRUE)<>prComplete then Result:=veInvalidInput;
  end;
end;

Function TCustomPXValidator.GetPicture
   : String;
  begin
    Result:=FPicture^;
  end;

Procedure TCustomPXValidator.SetPicture
  (
  Const APicture   : String
  );
 begin
   DisposeStr(FPicture);
   FPicture:=NewStr(APicture);
 end;

Function IsNumber
   (
   Chr             : Char
   )
   : Boolean;
  begin
    Result:=(Chr>='0') and (Chr<='9');
  end;

Function IsLetter
   (
   Chr             : Char
   )
   : Boolean;
  begin
    Result:=IsCharAlpha(Chr);
  end;

Function IsSpecial
   (
   Chr             : Char;
   const Special   : String
   )
   : Boolean;
  begin
    Result:=Pos(Chr,Special)<>0;
  end;

Function IsComplete
   (
   Rslt            : TPicResult
   )
   : Boolean;
  begin
    IsComplete:=Rslt in [prComplete,prAmbiguous];
  end;

Function IsIncomplete(Rslt: TPicResult): Boolean;
  begin
    IsIncomplete:=Rslt in [prIncomplete,prIncompNoFill];
  end;

Function TCustomPXValidator.CheckPicture
   (
   var Input       : string;
   AutoFill        : Boolean
   )
   : TPicResult;
var
  I, J: Byte;
  Rslt: TPicResult;
  Reprocess: Boolean;

  Function Process(TermCh: Byte): TPicResult;
  var
    Rslt: TPicResult;
    Incomp: Boolean;
    OldI, OldJ, IncompJ, IncompI: Byte;

    { Consume input }

    procedure Consume(Ch: Char);
    begin
      Input[J] := Ch;
      Inc(J);
      Inc(I);
    end;

    { Skip a character or a picture group }

    procedure ToGroupEnd(var I: Byte);
    var
      BrkLevel, BrcLevel: Integer;
    begin
      BrkLevel := 0;
      BrcLevel := 0;
      repeat
        if I = TermCh then Exit;
        case FPicture^[I] of
          '[': Inc(BrkLevel);
          ']': Dec(BrkLevel);
          '{': Inc(BrcLevel);
          '}': Dec(BrcLevel);
          ';': Inc(I);
          '*':
            begin
              Inc(I);
              while IsNumber(FPicture^[I]) do Inc(I);
              ToGroupEnd(I);
              Continue;
            end;
        end;
        Inc(I);
      until (BrkLevel = 0) and (BrcLevel = 0);
    end;

    { Find the a comma separator }

    Function SkipToComma: Boolean;
    begin
      repeat ToGroupEnd(I) until (I = TermCh) or (FPicture^[I] = ',');
      if FPicture^[I] = ',' then Inc(I);
      SkipToComma := I < TermCh;
    end;

    { Calclate the end of a group }

    Function CalcTerm: Byte;
    var
      K: Byte;
    begin
      K := I;
      ToGroupEnd(K);
      CalcTerm := K;
    end;

    { The next group is repeated X times }

    Function Iteration: TPicResult;
    var
      Itr, K, L: Byte;
      Rslt: TPicResult;
      NewTermCh: Byte;
    begin
      Itr := 0;
      Result := prError;

      Inc(I);  { Skip '*' }

      { Retrieve number }

      while IsNumber(FPicture^[I]) do
      begin
        Itr := Itr * 10 + Byte(FPicture^[I]) - Byte('0');
        Inc(I);
      end;

      if I > TermCh then
      begin
        Result := prSyntax;
        Exit;
      end;

      K := I;
      NewTermCh := CalcTerm;

      { If Itr is 0 allow any number, otherwise enforce the number }
      if Itr <> 0 then
      begin
        for L := 1 to Itr do
        begin
          I := K;
          Rslt := Process(NewTermCh);
          if not IsComplete(Rslt) then
          begin
            { Empty means incomplete since all are required }
            if Rslt = prEmpty then Rslt := prIncomplete;
            Result := Rslt;
            Exit;
          end;
        end;
      end
      else
      begin
        repeat
          I := K;
          Rslt := Process(NewTermCh);
        until not IsComplete(Rslt);
        if (Rslt = prEmpty) or (Rslt = prError) then
        begin
          Inc(I);
          Rslt := prAmbiguous;
        end;
      end;
      I := NewTermCh;
      Result := Rslt;
    end;

    { Process a picture group }

    Function Group: TPicResult;
    var
      Rslt: TPicResult;
      TermCh: Byte;
    begin
      TermCh := CalcTerm;
      Inc(I);
      Rslt := Process(TermCh - 1);
      if not IsIncomplete(Rslt) then I := TermCh;
      Group := Rslt;
    end;

    Function CheckComplete(Rslt: TPicResult): TPicResult;
    var
      J: Byte;
    begin
      J := I;
      if IsIncomplete(Rslt) then
      begin
        { Skip optional pieces }
        while True do
          case FPicture^[J] of
            '[': ToGroupEnd(J);
            '*':
              if not IsNumber(FPicture^[J + 1]) then
              begin
                Inc(J);
                ToGroupEnd(J);
              end
              else
                Break;
          else
            Break;
          end;

        if J = TermCh then Rslt := prAmbiguous;
      end;
      CheckComplete := Rslt;
    end;

    Function Scan: TPicResult;
    var
      Ch: Char;
      Rslt: TPicResult;
    begin
      Scan := prError;
      Rslt := prEmpty;
      while (I <> TermCh) and (FPicture^[I] <> ',') do
      begin
        if J > Length(Input) then
        begin
          Scan := CheckComplete(Rslt);
          Exit;
        end;

        Ch := Input[J];
        case FPicture^[I] of
          '#': if not IsNumber(Ch) then Exit
               else Consume(Ch);
          '?': if not IsLetter(Ch) then Exit
               else Consume(Ch);
          '&': if not IsLetter(Ch) then Exit
               else Consume(UpCase(Ch));
          '!': Consume(UpCase(Ch));
          '@': Consume(Ch);
          '*':
            begin
              Rslt := Iteration;
              if not IsComplete(Rslt) then
              begin
                Scan := Rslt;
                Exit;
              end;
              if Rslt = prError then Rslt := prAmbiguous;
            end;
          '{':
            begin
              Rslt := Group;
              if not IsComplete(Rslt) then
              begin
                Scan := Rslt;
                Exit;
              end;
            end;
          '[':
            begin
              Rslt := Group;
              if IsIncomplete(Rslt) then
              begin
                Scan := Rslt;
                Exit;
              end;
              if Rslt = prError then Rslt := prAmbiguous;
            end;
        else
          if FPicture^[I] = ';' then Inc(I);
          if UpCase(FPicture^[I]) <> UpCase(Ch) then
            if Ch = ' ' then Ch := FPicture^[I]
            else Exit;
          Consume(FPicture^[I]);
        end;

        if Rslt = prAmbiguous then
          Rslt := prIncompNoFill
        else
          Rslt := prIncomplete;
      end;

      if Rslt = prIncompNoFill then
        Scan := prAmbiguous
      else
        Scan := prComplete;
    end;

  begin
    Incomp := False;
    OldI := I;
    OldJ := J;
    repeat
      Rslt := Scan;

      { Only accept completes if they make it farther in the input
        stream from the last incomplete }
      if (Rslt in [prComplete, prAmbiguous]) and Incomp and (J < IncompJ) then
      begin
        Rslt := prIncomplete;
        J := IncompJ;
      end;

      if (Rslt = prError) or (Rslt = prIncomplete) then
      begin
        Process := Rslt;
        if not Incomp and (Rslt = prIncomplete) then
        begin
          Incomp := True;
          IncompI := I;
          IncompJ := J;
        end;
        I := OldI;
        J := OldJ;
        if not SkipToComma then
        begin
          if Incomp then
          begin
            Process := prIncomplete;
            I := IncompI;
            J := IncompJ;
          end;
          Exit;
        end;
        OldI := I;
      end;
    until (Rslt <> prError) and (Rslt <> prIncomplete);

    if (Rslt = prComplete) and Incomp then
      Process := prAmbiguous
    else
      Process := Rslt;
  end;

  Function SyntaxCheck: Boolean;
  var
    I: Integer;
    BrkLevel, BrcLevel: Integer;
  begin
    SyntaxCheck := False;

    if FPicture^ = '' then Exit;

    if FPicture^[Length(FPicture^)] = ';' then Exit;
    if (FPicture^[Length(FPicture^)] = '*') and (FPicture^[Length(FPicture^) - 1] <> ';') then
      Exit;

    I := 1;
    BrkLevel := 0;
    BrcLevel := 0;
    while I <= Length(FPicture^) do
    begin
      case FPicture^[I] of
        '[': Inc(BrkLevel);
        ']': Dec(BrkLevel);
        '{': Inc(BrcLevel);
        '}': Dec(BrcLevel);
        ';': Inc(I);
      end;
      Inc(I);
    end;
    if (BrkLevel <> 0) or (BrcLevel <> 0) then Exit;

    SyntaxCheck := True;
  end;


begin
  Result := prSyntax;
  if not SyntaxCheck then Exit;

  Result := prEmpty;
  if Input = '' then Exit;

  J := 1;
  I := 1;

  Rslt := Process(Length(FPicture^) + 1);
  if (Rslt <> prError) and (Rslt <> prSyntax) and (J <= Length(Input)) then
    Rslt := prError;

  if (Rslt = prIncomplete) and AutoFill then
  begin
    Reprocess := False;
    while (I <= Length(FPicture^)) and
      not IsSpecial(FPicture^[I], '#?&!@*{}[],'#0) do
    begin
      if FPicture^[I] = ';' then Inc(I);
      Input := Input + FPicture^[I];
      Inc(I);
      Reprocess := True;
    end;
    J := 1;
    I := 1;
    if Reprocess then
      Rslt := Process(Length(FPicture^) + 1)
  end;

  if Rslt = prAmbiguous then
    Result := prComplete
  else if Rslt = prIncompNoFill then
    Result := prIncomplete
  else
    Result := Rslt;
end;

{===============================================================================
| TCustomValidator
+==============================================================================}

Constructor TCustomValidator.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    FHistory:=TStringList.Create;
    FHistorySize:=6;
    FHookProc:=MakeObjectInstance(HookWndProc);
    FHookParentProc:=MakeObjectInstance(HookParentWndProc);
    FValidChars:=[#32..#255];
    FValOnExit:=TRUE;
{++ Ivanoff}
    FFormatWhenExit := TRUE;
    FShowMyselfPopup := TRUE;
{-- Ivanoff}
    FInvalidColor:=clRed;
  end;

Destructor TCustomValidator.Destroy;
begin
  DoUnhookWnd;
  FreeObjectInstance(FHookProc);
  FreeObjectInstance(FHookParentProc);
  FHistory.Free;
  inherited Destroy;
end;

Function TCustomValidator.GetHistoryCount:Integer;
begin
  Result:=FHistory.Count;
end;

Procedure TCustomValidator.AddToHistory(const EditText:String);
var Index          : Integer;
begin
  Index:=FHistory.IndexOf(EditText);
  if Index>-1 then FHistory.Delete(Index);
  FHistory.Add(EditText);
  while FHistory.Count>FHistorySize do FHistory.Delete(0);
  UpdateHistory;
end;


Procedure TCustomValidator.UpdateHistory;
var Cnt            : Integer;
begin
  if FCombobox<>NIL then begin
    FComboBox.Items.BeginUpdate;
    FComboBox.Items.Clear;
    for Cnt:=FHistory.Count-1 downto 0 do FComboBox.Items.Add(FHistory[Cnt]);
    FComboBox.Items.EndUpdate;
  end;
end;

Procedure TCustomValidator.ClearHistory;
begin
  FHistory.Clear;
  UpdateHistory;
end;

Function TCustomValidator.GetHistoryItem(AIndex:Integer):String;
begin
  Result:=FHistory[AIndex];
end;

Procedure TCustomValidator.AddHistory(const Text:String);
begin
  AddToHistory(Text);
end;

Function TCustomValidator.DoKeyPress(var Msg:TWMChar):Boolean;
var AText        : String;
    SelStart     : Integer;
    SelLength    : Integer;
    Method       : TMethod;
    PropInfo     : PPropInfo;
    Control      : TWinControl;
    Ch           : Char;
begin
  Result:=TRUE;
  if Assigned(FEdit) then begin
    Control:=FEdit;
    SelStart:=FEdit.SelStart;
    SelLength:=FEdit.SelLength;
  end
  else if Assigned(FComboBox) then begin
    Control:=FComboBox;
    SelStart:=FComboBox.SelStart;
    SelLength:=FComboBox.SelLength;
  end
  else begin
    Result:=FALSE;
    Exit;
  end;
  PropInfo:=GetPropInfo(Control.ClassInfo,'ONKEYPRESS');
  if (PropInfo<>NIL) and (PropInfo^.PropType^.Kind=tkMethod) then begin
    Method:=GetMethodProp(Control,PropInfo);
    if Assigned(Method.Code) then begin
      Ch:=Char(Msg.CharCode);
      TKeyPressEvent(Method)(Control,Ch);
      Msg.CharCode:=Word(Ch);
    end;
  end;
  with Msg do if (CharCode<>0) and (CharCode<>vk_Return)
      and (CharCode<>vk_Escape) then begin
//    if CharCode <>5 then begin
    AText:=Text;
    Delete(AText,SelStart+1,SelLength);
    if CharCode<>vk_Back then begin
      Insert(Char(CharCode),AText,SelStart+1);
      Inc(SelStart);
    end
    else if SelLength=0 then begin
      Delete(AText,SelStart,1);
      Dec(SelStart);
    end;
//    end;
    if IsValidInput(AText,SelStart) then begin
      Text:=AText;
      if Assigned(FEdit) then FEdit.SelStart:=SelStart
      else FComboBox.SelStart:=SelStart;
    end;
    UpdateColor;
    CharCode:=0;
  end;
end;

{++ BUG#447. Ivanoff}
{ TValidator did not have a method to handle 'paste' event before. Now it has. :) }
Function TCustomValidator.DoPaste: Boolean;
Var
    AText,
    sData: String;
    iI,
    SelStart,
    SelLength: Integer;
    Control: TWinControl;
Begin
     RESULT := FALSE;
     If Assigned(FEdit) Then Begin
        Control := FEdit;
        SelStart := FEdit.SelStart;
        SelLength := FEdit.SelLength;
        End
     Else
        If Assigned(FComboBox) Then Begin
           Control := FComboBox;
           SelStart := FComboBox.SelStart;
           SelLength := FComboBox.SelLength;
           End
        Else Begin
           RESULT := FALSE;
           EXIT;
           End;

     If NOT Clipboard.HasFormat(CF_TEXT) Then Begin
        RESULT := FALSE;
        EXIT;
        End;

     AText := Text;
     Delete(AText, SelStart+1, SelLength);
     sData := Clipboard.AsText;
     For iI:=1 To Length(sData) Do Begin
         Insert(Copy(sData, iI, 1), AText, SelStart+1);
         Inc(SelStart);
         End;
     If IsValidInput(AText, SelStart) Then Begin
        Text:=AText;
        If Assigned(FEdit) Then FEdit.SelStart:=SelStart
        Else FComboBox.SelStart:=SelStart;
        End;
     UpdateColor;
End;
{-- BUG#447. Ivanoff}

Procedure TCustomValidator.DoExit;
  var AText        : String;
  begin
{++ Ivanoff}
    If NOT FFormatWhenExit Then EXIT;
{-- Ivanoff}
    AText:=Text;
    if FormatOnExit(AText) then Text:=AText;
  end;

Procedure TCustomValidator.MoveCursorToEnd;
  begin
    if FEdit<>NIL then FEdit.SelStart:=Length(FEdit.Text)
    else if FComboBox<>NIL then FComboBox.SelStart:=Length(FComboBox.Text);
  end;

Procedure TCustomValidator.SetEdit
   (
   AEdit           : TCustomEdit
   );
  begin
    FCombobox:=NIL;
    if not (csDesigning in ComponentState) then begin
      DoUnhookWnd;
      FEdit:=AEdit;
      DoHookWnd;
    end
    else FEdit:=AEdit;
  end;

Procedure TCustomValidator.SetComboBox
   (
   ACombo          : TComboBox
   );
  begin
    FEdit:=NIL;
    if not (csDesigning in ComponentState) then begin
      DoUnhookWnd;
      FCombobox:=ACombo;
      DoHookWnd;
    end
    else FCombobox:=ACombo;
  end;

Function TCustomValidator.IsValidInput
   (
   var S           : String;
   var CaretPos    : Integer
   )
   : Boolean;
  var I            : Integer;
  begin
    Result:=FALSE;
    if Length(S)<=MaxLength then begin
      I:=1;
      while (I<=Length(S))
          and (S[I] in FValidChars) do Inc(I);
      if I>Length(S) then begin
        Result:=TRUE;
        if FAutoSkip
            and (Length(S)=MaxLength)
            and (CaretPos=MaxLength) then
          DoAutoSkip;
      end;
    end;
  end;

Function TCustomValidator.FormatOnExit
   (
   var S           : String
   )
   : Boolean;
  begin
    Result:=FALSE;
  end;

Procedure TCustomValidator.SetAsInteger
   (
   AValue          : LongInt
   );
  begin
    SetData(1,AValue);
  end;

Procedure TCustomValidator.SetAsBoolean
   (
   AValue          : Boolean
   );
  begin
    SetData(3,AValue);
  end;

Procedure TCustomValidator.SetAsFloat
   (
   AValue          : Double
   );
  begin
    SetData(2,AValue);
  end;

Procedure TCustomValidator.SetAsText
   (
   AValue          : String
   );
  begin
    SetData(0,AValue);
  end;

Function TCustomValidator.GetAsInteger
   : LongInt;
  begin
    GetData(1,Result);
  end;

Function TCustomValidator.GetAsBoolean
   : Boolean;
  begin
    GetData(3,Result);
  end;

Function TCustomValidator.GetAsFloat
   : Double;
  begin
    GetData(2,Result);
  end;

Function TCustomValidator.GetAsText
   : String;
  begin
    GetData(0,Result);
  end;

Procedure TCustomValidator.DoAutoSkip;
  begin
    GetParentForm(FEdit).Perform(cm_DialogKey,vk_Tab,0);
  end;

procedure TCustomValidator.UpdateColor;
begin
  if FInvalidColor<>clNone then begin
    if Control is TCustomEdit then begin
      if CheckValid(TEdit(Control).Text)=0 then TEdit(Control).Font.Color:=clWindowText
      else TEdit(Control).Font.Color:=FInvalidColor;
    end
    else if Control is TComboBox then begin
      if CheckValid(TComboBox(Control).Text)=0 then TComboBox(Control).Font.Color:=clWindowText
      else TComboBox(Control).Font.Color:=FInvalidColor;
    end;
  end;
end;

function TCustomValidator.CheckValid(const S: String): Word;
begin
  if not FLeaveBlank and (Length(S)=0) then Result:=veBlankError
  else if FFullLength and (Length(S)<FMaxLength) then Result:=veFullLengthError
  else Result:=0;
end;

Function TCustomValidator.IsValid(Const S:String):Boolean;
var Error          : Word;
begin
  Error:=CheckValid(S);
  if Error<>0 then DoError(Error);
  Result:=Error=0;
end;

Procedure TCustomValidator.DoError(ErrorCode:Word);
var AStr         : String;
begin
  if Assigned(FOnValError) then FOnValError(Self,ErrorCode)
  else begin
    SetFocus;
    case ErrorCode of
      veBlankError      : AStr:=GetErrorMessage(veBlankError);
      veFullLengthError : AStr:=Format(GetErrorMessage(veFullLengthError),[MaxLength]);
      veInvalidInput    : AStr:=GetErrorMessage(veInvalidInput);
      else AStr:='';
    end;
    MessageDialog(AStr,mtInformation,[mbOk],0);
  end;
end;

Procedure TCustomValidator.SetFocus;
var Cnt            : Integer;
    Item           : TControl;
begin
  if Control<>NIL then begin
    { if control is visible then focus can be set directly }
    Item:=Control;
    while (Item<>NIL) and Item.Visible do Item:=Item.Parent;
    if Item=NIL then Control.SetFocus
    { if parent is a TTabPage then activate the right page of the notebook }
    else begin
      Item:=Control.Parent;
      while (Item<>NIL) and not (Item is TTabPage)
          and not (Item is TTabSheet) do Item:=Item.Parent;
      if Item is TTabSheet then Item.Show
      else if Item is TTabPage then begin
        Cnt:=0;
        while TTabbedNotebook(Item.Parent).Pages.Objects[Cnt]<>Item do Inc(Cnt);
        TTabbedNotebook(Item.Parent).PageIndex:=Cnt;
      end;
      Control.SetFocus;
    end;
  end;
end;

Procedure TCustomValidator.Clear;
  begin
    if Assigned(FEdit) then FEdit.Clear
    else if Assigned(FComboBox) then FComboBox.Clear;
  end;

Function TCustomValidator.GetControl
   : TWinControl;
begin
  if Assigned(FEdit) then Result:=FEdit
  else Result:=FCombobox;
end;

Procedure TCustomValidator.DoHookWnd;
begin
  if (Control<>NIL) and (FHookWnd=0) then begin
    if Control is TCustomComboBox then FHookWnd:=GetWindow(Control.Handle,GW_CHILD)
    else FHookWnd:=Control.Handle;
    FOrgWndProc:=Pointer(GetWindowLong(FHookWnd,gwl_WndProc));
    SetWindowLong(FHookWnd,gwl_WndProc,LongInt(FHookProc));
    DoHookParentWnd;
  end;
end;

Procedure TCustomValidator.DoUnHookWnd;
var AHandle        : THandle;
begin
  if (Control<>NIL) and Control.HandleAllocated then begin
    if Control is TCustomComboBox then AHandle:=GetWindow(Control.Handle,GW_CHILD)
    else AHandle:=Control.Handle;
    if FHookWnd=AHandle then SetWindowLong(FHookWnd,gwl_WndProc,LongInt(FOrgWndProc));
  end;
  FHookWnd:=0;
end;

Procedure TCustomValidator.DoHookParentWnd;
begin
  if (Control<>NIL) and (FHookParent=NIL)
      and (WindowHooks.IndexOf(GetParentForm(Control))=-1) then begin
    FHookParent:=GetParentForm(Control);
    if FHookParent<>NIL then begin
      FOrgParentWndProc:=Pointer(GetWindowLong(FHookParent.Handle,gwl_WndProc));
      SetWindowLong(FHookParent.Handle,gwl_WndProc,LongInt(FHookParentProc));
      WindowHooks.Add(FHookParent);
    end;
  end;
end;

Procedure TCustomValidator.DoUnhookParentWnd;
begin
  if FHookParent<>NIL then begin
    if FHookParent.HandleAllocated then SetWindowLong(
        FHookParent.Handle,gwl_WndProc,LongInt(FOrgParentWndProc));
    WindowHooks.Remove(FHookParent);
  end;
  FHookParent:=NIL;
end;

Procedure TCustomValidator.HookWndProc
   (
   var Message     : TMessage
   );
  var DoInherited  : Boolean;
      APoint       : TPoint;
{      ARect        : TRect;
      PaintStruct  : TPaintStruct;}
  begin
    with Message do begin
      DoInherited:=TRUE;
      if Control<>NIL then case Msg of
{++ BUG#447. Ivanoff}
        WM_PASTE: DoInherited := DoPaste;
{-- BUG#447. Ivanoff}
        wm_Char        : DoInherited:=DoKeyPress(TWMChar(Message));
        wm_RButtonUp   : begin
            APoint:=Control.ClientToScreen(Point(lParamLo,lParamHi));
            DoInherited:=not DoPopup(APoint.X,APoint.Y);
          end;
        wm_KillFocus      : DoExit;
        cn_KeyDown   : if (wParam=vk_Tab)
            and (GetKeyState(vk_Shift)>=0)
            and FValOnExit
            and not ValidateInput then begin
              wParam:=0;
              DoInherited:=FALSE;
            end;
        wm_Destroy,
        cm_RecreateWnd : DoUnhookWnd;
      end;
      if DoInherited then Result:=CallWindowProc(FOrgWndProc,Control.Handle,
          Msg,WParam,LParam);
      if Control<>NIL then case Msg of
        cm_RecreateWnd : DoHookWnd;
      end;
    end;
  end;

Procedure TCustomValidator.HookParentWndProc(var Message:TMessage);
var DoInherited    : Boolean;
begin
  with Message do begin
    DoInherited:=TRUE;
    case Msg of
      cm_RecreateWnd : begin
          Result:=CallWindowProc(FOrgParentWndProc,FHookParent.Handle,Msg,WParam,LParam);
          DoUnhookParentWnd;
          DoInherited:=FALSE;
        end;
      cm_Deactivate  : SaveValidatorHistories(FHookParent);
    end;
    if DoInherited then Result:=CallWindowProc(FOrgParentWndProc,FHookParent.Handle,Msg,WParam,LParam);
    case Msg of
      wm_Destroy     : DoUnhookParentWnd;
      cm_RecreateWnd : DoHookParentWnd;
    end;
  end;
end;

Procedure TCustomValidator.DoStandardMenus
   (
   Sender          : TObject
   );
  begin
    case FPopupMenu.Items.IndexOf(TMenuItem(Sender)) of
      0 : if FEdit<>NIL then FEdit.SelectAll
          else if FCombobox<>NIL then FCombobox.SelectAll;
      2 : SendMessage(Control.Handle,wm_Cut,0,0);
      3 : SendMessage(Control.Handle,wm_Copy,0,0);
      4 : SendMessage(Control.Handle,wm_Paste,0,0);
      5 : SendMessage(Control.Handle,wm_Clear,0,0);
    end;
  end;

Function TCustomValidator.GetPopupMenu
   : TPopupMenu;
  Procedure AddMenu
     (
     Const AName   : String;
     Enabled       : Boolean
     );
    var MenuItem   : TMenuItem;
    begin
      MenuItem:=TMenuItem.Create(Result);
      MenuItem.Caption:=AName;
      MenuItem.OnClick:=DoStandardMenus;
      MenuItem.Enabled:=Enabled;
      Result.Items.Add(MenuItem);
    end;
  var AEnable      : Boolean;
      BEnable      : Boolean;
  begin
    if FPopupMenu<>NIL then begin
      FPopupMenu.Free;
      FPopupMenu:=NIL;
    end;
    Result:=TPopupMenu.Create(NIL);
    if FEdit<>NIL then begin
      AEnable:=FEdit.SelLength>0;
      BEnable:=FEdit.SelLength<>Length(FEdit.Text);
    end
    else if FCombobox<>NIL then begin
      AEnable:=FCombobox.SelLength>0;
      BEnable:=FCombobox.SelLength<>Length(FComboBox.Text);
    end
    else begin
      AEnable:=FALSE;
      BEnable:=FALSE;
    end;
    AddMenu(MlgStringList['Validators',50],BEnable);
    AddMenu('-',TRUE);
    AddMenu(MlgStringList['Validators',51],AEnable);
    AddMenu(MlgStringList['Validators',52],AEnable);
    AddMenu(MlgStringList['Validators',53],IsClipboardFormatAvailable(cf_Text));
    AddMenu(MlgStringList['Validators',54],AEnable);
  end;

Function TCustomValidator.DoPopup
   (
   X               : Integer;
   Y               : Integer
   )
   : Boolean;
  begin
{++ Ivanoff}
    If NOT FShowMyselfPopup Then Begin
       RESULT := FALSE;
       EXIT;
       End;
{-- Ivanoff}
    FPopupMenu:=GetPopupMenu;
    if FPopupMenu<>NIL then begin
      FPopupMenu.Popup(X,Y);
      Result:=TRUE;
    end
    else Result:=FALSE;
  end;

Function TCustomValidator.ValidateInput:Boolean;
begin
  Result:=IsValid(AsText);
end;

Function TCustomValidator.ValidInput:Boolean;
begin
  Result:=CheckValid(AsText)=0;
end;

Procedure TCustomValidator.SetData
   (
   DataType        : Integer;
   var Data
   );
  begin
    case DataType of
      0: Text:=String(Data);
      1: Text:=IntToStr(LongInt(Data));
      2: Text:=FloatToStr(Double(Data));
      3: Text:=IntToStr(Byte(Data));
    end;
  end;

Procedure TCustomValidator.GetData
   (
   DataType        : Integer;
   var Data                       
   );
  begin
    case DataType of
      0: String(Data):=Text;
      1: LongInt(Data):=StrToInt(Text);
      2: Double(Data):=StringToFloat(Text);
      3:
    end;
  end;

Function TCustomValidator.GetEditText
   : String;
  begin
    if Assigned(FEdit) then Result:=FEdit.Text
    else if Assigned(FCombobox) then Result:=FComboBox.Text
    else Result:='';
  end;

Procedure TCustomValidator.SetEditText
   (
   Const AText     : String
   );
  begin
    if Assigned(FEdit) then
    begin
      FEdit.Text:=AText;
      UpdateColor;  {++ Sygsky 12-SEP-1999 BUG#260 BUILD#90 }
    end
    else if Assigned(FCombobox) then
     begin
      FComboBox.Text:=AText;
      UpdateColor;  {++ Sygsky 12-SEP-1999 BUG#260 BUILD#90}
    end;
  end;

Function TCustomValidator.GetValidChars
   : String;
  var Cnt          : Char;
      Start        : Char;
  Function CharToStr
     (
     AChar         : Char
     )
     : String;
    begin
      if (AChar<#32)
          or (AChar=#255) then
        Result:='#'+IntToStr(Ord(AChar))
      else Result:=''''+AChar+'''';
    end;
  begin
    Result:='';
    Cnt:=#0;
    while Cnt<#255 do begin
      while (Cnt<#255)
          and not (Cnt in FValidChars) do Inc(Cnt);
      if Cnt in FValidChars then begin
        Start:=Cnt;
        while (Cnt<#255)
            and (Cnt in FValidChars) do Inc(Cnt);
        if Cnt<>#255 then Cnt:=Pred(Cnt);
        if Result<>'' then Result:=Concat(Result,',');
        Result:=Concat(Result,CharToStr(Start));
        if Start<>Cnt then begin
          if Cnt=Succ(Start) then Result:=Concat(Result,',',CharToStr(Cnt))
          else Result:=Concat(Result,'..',CharToStr(Cnt));
        end;
        if Cnt<>#255 then Cnt:=Succ(Cnt);
      end;
    end;
  end;

Procedure TCustomValidator.SetValidChars
   (
   AChars          : String
   );
  var Cnt          : Integer;
      FNewSet      : TFilterSet;
      Range        : Boolean;
      CharDefs     : Integer;
      Chars        : Array[0..1] of Char;
  Procedure Error;
    begin
      Raise Exception.Create('Invalid character-set-format at position '+IntToStr(Cnt));
    end;
  Procedure ConsumeChar;
    begin
      if (CharDefs>2)
          or (Cnt>Length(AChars)-1)
          or (AChars[Cnt+2]<>'''') then Error
      else begin
        Chars[CharDefs]:=AChars[Cnt+1];
        Inc(CharDefs);
        Inc(Cnt,3);
      end;
    end;
  Procedure ConsumeNumberChar;
    var Number     : Integer;
        Value      : Integer;
    begin
      Number:=Cnt+1;
      while (Number<=Length(AChars))
          and (AChars[Number] in ['0'..'9']) do
        Inc(Number);
      Dec(Number);
      Value:=StrToInt(Copy(AChars,Cnt+1,Number-Cnt));
      if (CharDefs>2)
          or (Number=Cnt)
          or (Value>255) then Error
      else begin
        Chars[CharDefs]:=Char(Value);
        Inc(CharDefs);
        Cnt:=Number+1;
      end;
    end;
  begin
    Cnt:=1;
    FNewSet:=[];
    Range:=FALSE;
    CharDefs:=0;
    AChars:=AChars+',';
    while Cnt<=Length(AChars) do begin
      case AChars[Cnt] of
        '''' : ConsumeChar;
        ','  : if Range then begin
            if (CharDefs<2)
                or (Chars[0]>Chars[1]) then Error
            else begin
              while Chars[0]<Chars[1] do begin
                Include(FNewSet,Chars[0]);
                Chars[0]:=Succ(Chars[0]);
              end;
              Include(FNewSet,Chars[1]);
              CharDefs:=0;
              Range:=FALSE;
              Inc(Cnt);
            end;
          end
          else if CharDefs=0 then Error
          else begin
            Include(FNewSet,Chars[0]);
            CharDefs:=0;
            Inc(Cnt);
          end;
        '.' : if (AChars[Cnt+1]<>'.')
                or (CharDefs=0) then Error
          else begin
            Range:=TRUE;
            Inc(Cnt,2);
          end;
        '#': ConsumeNumberChar;
        else Error;
      end;
    end;
    FValidChars:=FNewSet;
  end;

Procedure TCustomValidator.Notification
   (
   AComponent      : TComponent;
   Operation       : TOperation
   );
  begin
    inherited Notification(AComponent,Operation);
    if Operation=opRemove then begin
      if AComponent=FEdit then begin
        DoUnhookWnd;
        FEdit:=NIL;
      end
      else if AComponent=FCombobox then begin
        DoUnhookWnd;
        FCombobox:=NIL;
      end;
    end;
  end;

Procedure TCustomValidator.DoSaveHistory;
begin
  if (FComboBox<>NIL) and (FComboBox.Parent<>NIL)
      and (FComboBox.Text<>'') then begin
    AddToHistory(FComboBox.Text);
    UpdateHistory;
  end;
end;

{==============================================================================}
{ TIntValidator                                                                }
{==============================================================================}

Constructor TIntValidator.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    FValidChars:=['+','-','0'..'9'];
    FShowZero:=True;
    FAllowSign:=True;
  end;

Function TIntValidator.IsValidInput
   (
   var S           : String;
   var CaretPos    : Integer
   )
   : Boolean;
  var I            : Integer;
  begin
    Result:=FALSE;
    I:=1;
    while (I<=Length(S))
        and (S[I] in FValidChars) do Inc(I);
    if I>Length(S) then begin
      if FDelLeading
          and (Length(S)>MaxLength)
          and (S[1]='0') then Delete(S,1,1);
      if Length(S)<=MaxLength then begin
        Result:=TRUE;
        if FAutoSkip
            and (Length(S)=MaxLength)
            and (CaretPos=MaxLength) then
          DoAutoSkip;
      end;
    end;
  end;

Procedure TIntValidator.SetAllowSign
   (
   AAllow          : Boolean
   );
  begin
    if AAllow<>FAllowSign then begin
      FAllowSign:=AAllow;
      if FAllowSign then FValidChars:=FValidChars+['-','+']
      else FValidChars:=FValidChars-['-','+'];
    end;
  end;

Function TIntValidator.CheckValid(Const S:String):Word;
begin
  Result:=inherited CheckValid(S);
  if (Result=0) and (S<>'') and (FMin<>FMax) and
      ((AsInteger<FMin) or (AsInteger>FMax)) then
    Result:=veRangeError;
end;

Procedure TIntValidator.DoError(ErrorCode:Word);
begin
  if Assigned(FOnValError) then FOnValError(Self,ErrorCode)
  else if ErrorCode=veRangeError then begin
    SetFocus;
    MessageDialog(Format(GetErrorMessage(veRangeError),
        [IntToStr(FMin),IntToStr(FMax)]),mtInformation,[mbOk],0);
  end
  else inherited DoError(ErrorCode);
end;

Procedure TIntValidator.SetData
   (
   DataType        : Integer;
   var Data
   );
  var AValue       : LongInt;
      AStr         : String;
  begin
    case DataType of
      0: AValue:=StrToInt(String(Data));
      1: AValue:=LongInt(Data);
      2: AValue:=Round(Double(Data));
      else AValue:=Byte(Data);
    end;
    if (AValue<>0)
        or FShowZero then begin
      AStr:=IntToStr(AValue);
      if FLeadingZeros then
          while Length(AStr)<MaxLength do AStr:='0'+AStr;
    end
    else AStr:='';
    Text:=AStr;
  end;

Procedure TIntValidator.GetData
   (
   DataType        : Integer;
   var Data
   );
  begin
    case DataType of
      0: String(Data):=Text;
      1: if Text='' then LongInt(Data):=DefaultValue
         else LongInt(Data):=StrToInt(Text);
      2: if Text='' then Double(Data):=DefaultValue
         else Double(Data):=StrToInt(Text);
      3:
    end;
  end;

Function TIntValidator.FormatOnExit
   (
   var S           : String
   )
   : Boolean;
  begin
    if S<>'' then begin
      if FLeadingZeros then
          while Length(S)<MaxLength do S:='0'+S;
    end;
    Result:=TRUE;
  end;

Procedure TIntValidator.AddHistory(Value:Integer);
begin
  AddToHistory(IntToStr(Value));
end;

Function TIntValidator.GetHistoryItem(AIndex:Integer):Integer;
begin
  Result:=StrToInt(inherited HistoryStrings[AIndex]);
end;

{==============================================================================}
{ TFloatValidator                                                              }
{==============================================================================}

Constructor TFloatValidator.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
//++Sygsky: ThousandSeparator is added here, enter it if you like it :)
    FValidChars:=['+','-','0'..'9',DecimalSeparator,ThousandSeparator];
    FShowZero:=True;
    FAllowSign:=True;
  end;

Function CountChar
   (
   AChar         : Char;
   Const S       : String
   )
   : Integer;
  var Cnt        : Integer;
      Chars      : Integer;
  begin
    Chars:=0;
    for Cnt:=1 to Length(S) do if S[Cnt]=AChar then Inc(Chars);
    CountChar:=Chars;
  end;

Function TFloatValidator.CheckValid(Const S:String):Word;
var Value        : Double;
begin
  Result:=inherited CheckValid(S);
  if (Result=0) and (S<>'') and (Abs(MinValue-MaxValue)>1E-200) then begin
    Value:=StringToFloat(S);
    if (Value<MinValue) or (Value>MaxValue) then Result:=veRangeError;
  end;
end;

Function TFloatValidator.IsValidInput
   (
   var S             : String;
   var CaretPos      : Integer
   )
   : Boolean;
  var APos         : Integer;
      Sign         : Integer;
  begin
    Result:=FALSE;
    if inherited IsValidInput(S,CaretPos) then begin
      if (CountChar(DecimalSeparator,S)<=1)
          and (CountChar('-',S)<=1)
          and (Pos('-',S)<=1) then begin
        if Pos('-',S)<>0 then Sign:=1
        else if Pos('+',S)<>0 then Sign:=1
        else Sign:=0;
        APos:=Pos(DecimalSeparator,S);
        if APos=0 then Result:=Length(S)<=MaxLength-FCommas-1
        else Result:=(APos-1<=MaxLength-FCommas)
              and (Length(S)-APos<=FCommas);
        if Result
            and FAutoSkip
            and (Length(S)-Sign=MaxLength)
            and (CaretPos=MaxLength+Sign) then DoAutoSkip;
      end;
    end;
  end;

Function TFloatValidator.FormatOnExit
   (
   var S           : String
   )
   : Boolean;
  var Value        : Real;
  begin
    if S<>'' then begin
      Value:=StringToFloat(S);
      S:=TextForValue(Value);
    end;
    Result:=TRUE;
  end;

Procedure TFloatValidator.DoError
   (
   ErrorCode       : Word
   );
  begin
    if Assigned(FOnValError) then FOnValError(Self,ErrorCode)
    else if ErrorCode=veRangeError then begin
      SetFocus;
      MessageDialog(Format(GetErrorMessage(veRangeError),[FloatToStrF(FMin,ffGeneral,MaxLength,FCommas),
          FloatToStrF(FMax,ffGeneral,MaxLength,FCommas)]),mtInformation,[mbOk],0);
    end
    else inherited DoError(ErrorCode);
  end;

Procedure TFloatValidator.GetData
   (
   DataType        : Integer;
   var Data
   );
var
  Str: AnsiString;
  begin
//++Sygsky: First remove any thousand separators, after check the value!
    Str := Text;
    Q_DelCharInPlace(Str, ',');
//--Sygsky
    case DataType of
      0: String(Data):=Text;
      1: if Text='' then LongInt(Data):=Round(DefaultValue)
         else LongInt(Data):=Round(StringToFloat(Str));
      2: if Text='' then Double(Data):=DefaultValue
         else Double(Data) :=StringToFloat(Str);
      3:
    end;
  end;

Function TFloatValidator.TextForValue
   (
   Const AValue    : Double
   )
   : String;
  begin
    if (Abs(AValue)<=1E-200)
        and not FShowZero then Result:=''
    else begin
      Result:=FloatToStrF(AValue,ffNumber,MaxLength,FCommas);
      if not FTrailingZeros then RemoveTrailingZeros(Result);
    end;
  end;

Procedure TFloatValidator.SetData
   (
   DataType        : Integer;
   var Data
   );
  begin
    case DataType of
      0: Text:=String(Data);
      1: Text:=TextForValue(LongInt(Data));
      2: Text:=TextForValue(Double(Data));
      3: Text:='';
    end;
  end;

Procedure TFloatValidator.SetAllowSign
   (
   AAllow          : Boolean
   );
  begin
    if AAllow<>FAllowSign then begin
      FAllowSign:=AAllow;
      if FAllowSign then FValidChars:=FValidChars+['-','+']
      else FValidChars:=FValidChars-['-','+'];
    end;
  end;

Function TFloatValidator.StoreMin:Boolean;
begin
  Result:=Abs(FMin)>1E-200;
end;

Function TFloatValidator.StoreMax:Boolean;
begin
  Result:=Abs(FMax)>1E-200;
end;

Function TFloatValidator.StoreDefault:Boolean;
begin
  Result:=Abs(FDefaultValue)>1E-200;
end;

Procedure TFloatValidator.AddHistory(Value:Double);
begin
  AddToHistory(TextForValue(Value));
end;

Function TFloatValidator.GetHistoryItem(AIndex:Integer):Double;
begin
  Result:=StringToFloat(inherited HistoryStrings[AIndex]);
end;

{==============================================================================}
{ TValidator                                                                   }
{==============================================================================}

Function TValidator.IsValidInput
   (
   var S           : String;
   var CaretPos    : Integer
   )
   : Boolean;
  begin
    Result:=FALSE;
    if inherited IsValidInput(S,CaretPos) then begin
      if Length(S) = 0 then Result:=TRUE
      else if FLeadingBlanks
          or (S[1]<>' ')
          or (CaretPos<>1) then begin
        if FCaseConversion=ccAllUpperCase then S:=AnsiUpperCase(S)
        else if FCaseConversion=ccAllLowerCase then S:=AnsiLowerCase(S)
        else if (FCaseConversion=ccFirstUpperCase)
            and (CaretPos=1) then AnsiUpperBuff(@S[1],1);
        Result:=TRUE;
      end;
    end;
  end;

Function TValidator.CheckValid(Const S:String):Word;
Function IsBlankString
   : Boolean;
  var Cnt        : Integer;
  begin
    Cnt:=1;
    while (Cnt<=Length(S)) and (S[Cnt]=' ') do Inc(Cnt);
    Result:=Cnt>Length(S);
  end;
begin
  Result:=inherited CheckValid(S);
  if (Result=0) and not FTrailingBlanks and not FLeaveBlank
      and IsBlankString then Result:=veBlankError;
end;

Procedure TValidator.GetData
   (
   DataType        : Integer;
   var Data
   );
  var AStr         : String;
      Cnt          : Integer;
  begin
    case DataType of
      0: begin
          AStr:=Text;
          if not FLeadingBlanks then begin
            Cnt:=1;
            while (Cnt<=Length(AStr)) and (AStr[Cnt]=' ') do Inc(Cnt);
            Delete(AStr,1,Cnt-1);
          end;
          if not FTrailingBlanks then begin
            Cnt:=Length(AStr);
            while (Cnt>=1) and (AStr[Cnt]=' ') do Dec(Cnt);
            Delete(AStr,Cnt+1,Length(AStr)-Cnt);
          end;
          String(Data):=AStr;
        end;
      1: LongInt(Data):=StrToInt(Text);
      2: Double(Data):=StringToFloat(Text);
      3:
    end;
  end;

Function TValidator.FormatOnExit
   (
   var S           : String
   )
   : Boolean;
  var Cnt          : Integer;
  begin
    Result:=FALSE;
    if not FLeadingBlanks then begin
      Cnt:=1;
      while (Cnt<=Length(S)) and (S[Cnt]=' ') do Inc(Cnt);
      Delete(S,1,Cnt-1);
      Result:=Cnt>1;
    end;
    if not FTrailingBlanks then begin
      Cnt:=Length(S);
      while (Cnt>=1) and (S[Cnt]=' ') do Dec(Cnt);
      Result:=Result or (Cnt<Length(S));
      Delete(S,Cnt+1,Length(S)-Cnt);
    end;
  end;

{==============================================================================}
{ TFileValidator                                                               }
{==============================================================================}

Function TFileValidator.CheckValid
   (
   Const S         : String
   )
   :Word;
  var TmpString    : String;
  begin
    Result:=inherited CheckValid(S);
    GetData(0,TmpString);
    if (Result = 0) and (TmpString <> '') then begin
      if not FileExists(TmpString) then Result:=veNoFileError;
    end;
  end;

Procedure TFileValidator.GetData
   (
   DataType        : Integer;
   var Data
   );
  var AStr         : String;
      Cnt          : Integer;
  begin
    case DataType of
      0: begin
          AStr:=Text;
          if not FLeadingBlanks then begin
            Cnt:=1;
            while (Cnt<=Length(AStr)) and (AStr[Cnt]=' ') do Inc(Cnt);
            Delete(AStr,1,Cnt-1);
          end;
          if not FTrailingBlanks then begin
            Cnt:=Length(AStr);
            while (Cnt>=1) and (AStr[Cnt]=' ') do Dec(Cnt);
            Delete(AStr,Cnt+1,Length(AStr)-Cnt);
          end;
          String(Data):=AStr;
        end;
      1: ;
      2: ;
      3: ;
    end;
  end;

Procedure TFileValidator.DoError
   (
   ErrorCode       : Word
   );
  var TmpString    : String;
  begin
    if Assigned(FOnValError) then FOnValError(Self,ErrorCode)
    else if ErrorCode=veNoFileError then begin
      SetFocus;
      GetData(0,TmpString);
      MessageDialog(Format(GetErrorMessage(veNoFileError),[TmpString]),mtInformation,[mbOk],0);
    end
    else inherited DoError(ErrorCode);
  end;

{==============================================================================}
{ TPathValidator                                                               }
{==============================================================================}

Function TPathValidator.CheckValid
   (
   Const S         : String
   )
   :Word;
  var TmpString    : String;
      Code         : Integer;
      DriveName    : String;
      DriveType    : Integer;
  begin
    Result:=inherited CheckValid(S);
    GetData(0,TmpString);
    if (Result = 0) and (TmpString <> '') then begin
      if not DirectoryExists(TmpString) then Result:=veNoPathError
      else begin
        Code:=GetFileAttributes(PChar(TmpString));
        DriveName:=ExtractFileDrive(TmpString);
        DriveType:=GetDriveType(PChar(DriveName));
        if (FILE_ATTRIBUTE_READONLY and Code <> 0) or (DriveType = DRIVE_CDROM) then Result:=veRdOnlyPathError;
      end;
    end;
  end;

Procedure TPathValidator.GetData
   (
   DataType        : Integer;
   var Data
   );
  var AStr         : String;
      Cnt          : Integer;
  begin
    case DataType of
      0: begin
          AStr:=Text;
          if not FLeadingBlanks then begin
            Cnt:=1;
            while (Cnt<=Length(AStr)) and (AStr[Cnt]=' ') do Inc(Cnt);
            Delete(AStr,1,Cnt-1);
          end;
          if not FTrailingBlanks then begin
            Cnt:=Length(AStr);
            while (Cnt>=1) and (AStr[Cnt]=' ') do Dec(Cnt);
            Delete(AStr,Cnt+1,Length(AStr)-Cnt);
          end;
          String(Data):=AStr;
        end;
      1: ;
      2: ;
      3: ;
    end;
  end;

Procedure TPathValidator.DoError
   (
   ErrorCode       : Word
   );
  var TmpString    : String;
  begin
    if Assigned(FOnValError) then FOnValError(Self,ErrorCode)
    else if ErrorCode=veNoPathError then begin
      SetFocus;
      GetData(0,TmpString);
      MessageDialog(Format(GetErrorMessage(veNoPathError),[TmpString]),mtInformation,[mbOk],0);
    end
    else if ErrorCode=veRdOnlyPathError then begin
      SetFocus;
      GetData(0,TmpString);
      MessageDialog(Format(GetErrorMessage(veRdOnlyPathError),[TmpString]),mtInformation,[mbOk],0);
    end
    else inherited DoError(ErrorCode);
  end;

{==============================================================================}
{ TScaleValidator                                                              }
{==============================================================================}

Constructor TScaleValidator.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    FValidChars:=[':','0'..'9','.',','];
    FMaxLength:=10;
  end;

Procedure TScaleValidator.SetCommas
   (
   ACommas         : Integer
   );
  begin
    FCommas:=ACommas;
    if FCommas<>0 then FValidChars:=FValidChars+[DecimalSeparator]
    else FValidChars:=FValidChars-[DecimalSeparator];
  end;

Function TScaleValidator.CheckValid(const S:String):Word;
var Value        : Double;
begin
  Result:=inherited CheckValid(S);
  if (Result=0) and (Abs(MinScale-MaxScale)>1E-200) then begin
    Value:=ValueFromText(S);
    if (Value<MinScale) or (Value>MaxScale) then Result:=veRangeError;
  end;
end;

Function TScaleValidator.IsValidInput
   (
   var S           : String;
   var CaretPos    : Integer
   )
   : Boolean;
  var APos         : Integer;
      BPos         : Integer;
      Str1         : String;
      Str2         : String;
  begin
    Result:=FALSE;
    if inherited IsValidInput(S,CaretPos)
        and (CountChar(':',S)<=1) then begin
      BPos:=Pos(':',S);
      if BPos=0 then Str1:=S
      else begin
        Str1:=Copy(S,1,BPos-1);
        Str2:=Copy(S,BPos+1,Length(S)-BPos);
      end;     
      APos:=Pos(DecimalSeparator,Str1);
      BPos:=Pos(DecimalSeparator,Str2);
      Result:=(CountChar(DecimalSeparator,Str1)<=1)
          and (CountChar(DecimalSeparator,Str2)<=1)
          and ((APos=0) or (APos>=Length(Str1)-FCommas))
          and ((BPos=0) or (BPos>=Length(Str2)-FCommas));
    end;
  end;

Function TScaleValidator.FormatOnExit
   (
   var S           : String
   )
   : Boolean;
  var Value        : Real;
  begin
    if S<>'' then begin
      Value:=ValueFromText(S);
      S:=TextForValue(Value);
    end;
    Result:=TRUE;
  end;

Procedure TScaleValidator.DoError(ErrorCode:Word);
begin
  if Assigned(FOnValError) then FOnValError(Self,ErrorCode)
  else if ErrorCode=veRangeError then begin
    SetFocus;
    MessageDialog(Format(GetErrorMessage(veRangeError),[TextForValue(FMinScale),
        TextForValue(FMaxScale)]),mtInformation,[mbOk],0);
  end
  else inherited DoError(ErrorCode);
end;

Procedure TScaleValidator.GetData
   (
   DataType        : Integer;
   var Data
   );
  begin
    case DataType of
      0: String(Data):=Text;
      1: if Text='' then LongInt(Data):=Round(DefaultValue)
         else LongInt(Data):=Round(ValueFromText(Text));
      2: if Text='' then Double(Data):=DefaultValue
         else Double(Data):=ValueFromText(Text);
      3:
    end;
  end;

Function TScaleValidator.TextForValue(Const AValue:Double):String;
begin
  if (Abs(AValue)<=1E-200) then Result:=''
  else begin
    if AValue<1 then Result:=FloatToStrF(1.0/AValue,ffNumber,MaxLength,FCommas)
    else Result:=FloatToStrF(AValue,ffNumber,MaxLength,FCommas);
    if not FTrailingZeros then RemoveTrailingZeros(Result);
    if AValue<1 then Result:='1:'+Result
    else Result:=Result+':1';
  end;
end;

Procedure TScaleValidator.SetData(DataType:Integer;var Data);
begin
  case DataType of
    0: Text:=String(Data);
    1: Text:=TextForValue(LongInt(Data));
    2: Text:=TextForValue(Double(Data));
    3: Text:='';
  end;
end;

Function TScaleValidator.StoreDefault
   : Boolean;
  begin
    Result:=Abs(FDefaultValue)>1E-200;
  end;

Function TScaleValidator.StoreMin
   : Boolean;
  begin
    Result:=Abs(FMinScale)>1E-200;
  end;

Function TScaleValidator.StoreMax
   : Boolean;
  begin
    Result:=Abs(FMaxScale)>1E-200;
  end;

Function TScaleValidator.ValueFromText
   (
   Const S         : String
   )
   : Double;
  var APos         : Integer;
  begin
    try
      APos:=Pos(':',S);
      if APos=0 then Result:=1.0/StringToFloat(S)
      else Result:=StringToFloat(Copy(S,1,APos-1))/StringToFloat(Copy(S,APos+1,Length(S)-APos));
    except                      
      Result:=DefaultValue;
    end;    
  end;

Function TScaleValidator.GetHistoryItem(AIndex:Integer):Double;
begin
  Result:=ValueFromText(HistoryStrings[AIndex]);
end;

{==============================================================================}
{ TMeasureValidator                                                            }
{==============================================================================}

Constructor TMeasureValidator.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    FMeasure:=TMeasure.Create(0.0,muMillimeters);
    FMaxLength:=10;
    FShowZero:=TRUE;
    FMinValue:=TMeasure.Create(-1E301,muMillimeters);
    FMaxValue:=TMeasure.Create(1E301,muMillimeters);
    FDefault:=TMeasure.Create(0,muMillimeters);
  end;

Destructor TMeasureValidator.Destroy;
  begin
    Fmeasure.Free;
    FMinValue.Free;
    FMaxValue.Free;
    FDefault.Free;
    inherited Destroy;
  end;    

Function TMeasureValidator.GetUnits
   : TMeasureUnits;
  var AValue       : Double;
      NoUnits      : Boolean;
      ValueOK      : Boolean;
  begin
    if csDesigning in ComponentState then Result:=FMeasure.Units
    else ValueForText(AValue,Result,ValueOK,NoUnits);
  end;

Function TMeasureValidator.GetValues
   (
   AUnits          : TMeasureUnits
   )
   : Double;
  var AValues      : Double;
      AUnit        : TMeasureUnits;
      NoUnits      : Boolean;
      ValueOK      : Boolean;
  begin
    ValueForText(AValues,AUnit,ValueOK,NoUnits);
    if not ValueOK then Result:=FDefault[AUnits]
    else Result:=ConvertMeasure(AValues,AUnit,AUnits);
  end;

Procedure TMeasureValidator.SetUnits
   (
   AUnits          : TMeasureUnits
   );
  begin
    FMeasure.Units:=AUnits;
    Text:=TextForValue(FMeasure);
  end;

Procedure TMeasureValidator.ReadState(Reader:TReader);
begin
  FMinValue[muMillimeters]:=-1E302;
  FMaxValue[muMillimeters]:=1E302;
  inherited ReadState(Reader);
  if DblCompare(FMinValue[muMillimeters],-1E302)=0 then FMinValue[muMillimeters]:=0;
  if DblCompare(FMaxValue[muMillimeters],1E302)=0 then FMaxValue[muMillimeters]:=0;
end;

Procedure TMeasureValidator.SetValues
   (
   AUnits          : TMeasureUnits;
   Const AValue    : Double
   );
  begin
    FMeasure[AUnits]:=AValue;
    Text:=TextForValue(FMeasure);
  end;

Procedure TMeasureValidator.GetData
   (
   DataType        : Integer;
   var Data
   );
  var AUnits       : TMeasureUnits;
      AValue       : Double;
      NoUnits      : Boolean;
      ValueOK      : Boolean;
  begin
    ValueForText(AValue,AUnits,ValueOK,NoUnits);
    case DataType of
      0: String(Data):=TextForValue(FMeasure);
      1: LongInt(Data):=Round(AValue);
      2: Double(Data):=AValue;
      3:
    end;
  end;

Procedure TMeasureValidator.SetData
   (
   DataType        : Integer;
   var Data
   );
  begin
    case DataType of
      0: Text:=String(Data);
      1: begin
           FMeasure.Value:=LongInt(Data);
           Text:=TextForValue(FMeasure);
         end;
      2: begin
           FMeasure.Value:=Double(Data);
           Text:=TextForValue(FMeasure);
         end;  
      3: Text:='';
    end;
  end;

Function TMeasureValidator.TextForValue
   (
   AMeasure        : TMeasure
   )
   : String;
  begin
    if (csDesigning in ComponentState) then Result:=''
    else begin
      if (Abs(AMeasure.Value)<=1E-200)
          and not FShowZero then Result:=''
      else Result:=FloatToStrF(AMeasure.Value,ffNumber,MaxLength,FCommas);
      if not FTrailingZeros then RemoveTrailingZeros(Result);
      if Result<>'' then Result:=Result+' '+AMeasure.UnitsShortName
    end;  
  end;

Function RemoveBlanks
   (
   Str           : String
   )
   : String;
  var Cnt        : Integer;
      Cnt1       : Integer;
  begin
    Cnt:=1;
    while (Cnt<Length(Str))
        and (Str[Cnt]=' ') do Inc(Cnt);
    Cnt1:=Length(Str);
    while (Cnt1>0)
        and (Str[Cnt1]=' ') do Dec(Cnt1);
    RemoveBlanks:=Copy(Str,Cnt,Cnt1-Cnt+1);
  end;

Function TMeasureValidator.ValueForText
   (
   var AValue      : Double;
   var AUnits      : TMeasureUnits;
   var ValueOK     : Boolean;
   var NoUnits     : Boolean
   )
   : Boolean;
  var AText        : String;
      BText        : String;
      CText        : String;
      Cnt          : Integer;
      AUnit        : TMeasureUnits;
  begin
    Result:=FALSE;
    AText:=Text;                                  
    BText:='';
    CText:='';
    for Cnt:=1 to Length(AText) do begin
      if AText[Cnt] in ['+','-','0'..'9',DecimalSeparator,ThousandSeparator] then
          BText:=Concat(BText,AText[Cnt])
      else begin
        CText:=RemoveBlanks(Copy(AText,Cnt,Length(AText)-Cnt+1));
        Break;
      end;
    end;
    ValueOK:=FALSE;
    NoUnits:=FALSE;
    if BText='' then AValue:=FDefault.Value
    else begin
      AValue:=StringToFloat(BText);
      ValueOK:=TRUE;
    end;
    if CText='' then NoUnits:=TRUE
    else if (CountChar(DecimalSeparator,AText)<=1) then begin
      for AUnit:=Low(TMeasureUnits) to High(TMeasureUnits) do
        if (AUnit in FAllowedUnits)
            and ((AnsiCompareText(CText,UnitsToShortName(AUnit))=0)
                or (AnsiCompareText(CText,UnitsToName(AUnit))=0)) then begin
        AUnits:=AUnit;        
        FMeasure.Units:=AUnit;
        FMeasure.Value:=AValue;
        Result:=TRUE;
        Exit;
      end;
    end;
    AUnits:=FDefault.Units;
  end;

Function TMeasureValidator.StoreDefault
   : Boolean;
  begin
    Result:=FDefault.Units<>muMillimeters;
  end;

Function TMeasureValidator.StoreMin:Boolean;
begin
  Result:=(Abs(FMinValue.Value)>1E-200) or (FMinValue.Units<>muMillimeters);
end;

Function TMeasureValidator.StoreMax:Boolean;
begin
  Result:=(Abs(FMaxValue.Value)>1E-200) or (FMaxValue.Units<>muMillimeters);
end;

Procedure TMeasureValidator.SetValue
   (
   Const AValue    : Double
   );
  begin
    FMeasure.Value:=AValue;
    Text:=TextForValue(FMeasure);
  end;

Function TMeasureValidator.GetValue
   : Double;
  var AUnits       : TMeasureUnits;
      NoUnits      : Boolean;
      ValueOK      : Boolean;
  begin
    if csDesigning in ComponentState then Result:=FMeasure.Value
    else ValueForText(Result,AUnits,ValueOK,NoUnits);
  end;

Procedure TMeasureValidator.SetMeasure
   (
   AMeasure        : TMeasure
   );
  begin
    FMeasure.Assign(AMeasure);
    Text:=TextForValue(FMeasure);
  end;

Procedure TMeasureValidator.SetMin
   (
   AMeasure        : TMeasure
   );
  begin
    if Assigned(AMeasure) then FMinValue.Assign(AMeasure)
    else begin
      FMinValue.Units:=muMillimeters;
      FMinValue.Value:=-1E300;
    end;
  end;

Procedure TMeasureValidator.SetMax
   (
   AMeasure        : TMeasure
   );
  begin
    if Assigned(AMeasure) then FMaxValue.Assign(AMeasure)
    else begin
      FMaxValue.Units:=muMillimeters;
      FMaxValue.Value:=1E300;
    end;
  end;

Function TMeasureValidator.FormatOnExit                
   (
   var S           : String
   )
   : Boolean;
  var AValue       : Double;
      AUnits       : TMeasureUnits;
      NoUnits      : Boolean;
      ValueOK      : Boolean;
  begin
    Result:=FALSE;
    if ValueForText(AValue,AUnits,ValueOK,NoUnits)
        or (ValueOK and NoUnits) then begin
      if ValueOK then FMeasure.Value:=AValue;
      S:=TextForValue(FMeasure);
      Result:=TRUE;
    end;
  end;

Function TMeasureValidator.CheckValid(const S:String):Word;
var AValue       : Double;
    AUnit        : TMeasureUnits;
    NoUnits      : Boolean;
    ValueOK      : Boolean;
    ValidInput   : Boolean;
begin
  Result:=inherited CheckValid(S);
  if Result=0 then begin
    ValidInput:=ValueForText(AValue,AUnit,ValueOK,NoUnits);
    if not ValidInput and (not ValueOK or not NoUnits) then Result:=veInvalidInput
    else begin
      ValueOK:=TRUE;
      if (MaxValue.Value<1E300) and (AValue>MaxValue[AUnit]+Tolerance) then ValueOK:=FALSE;
      if (MinValue.Value>-1E300) and (AValue<MinValue[AUnit]-Tolerance) then ValueOK:=FALSE;
      if not ValueOK then Result:=veRangeError;
    end;
  end;
end;

Procedure TMeasureValidator.DoError(ErrorCode:Word);
begin
  if Assigned(FOnValError) then FOnValError(Self,ErrorCode)
  else if ErrorCode=veRangeError then begin
    SetFocus;
    if (MaxValue.Value<1E300) and (MinValue.Value>-1E300) then
        MessageDialog(Format(GetErrorMessage(veRangeError),[TextForValue(FMinValue),
          TextForValue(FMaxValue)]),mtInformation,[mbOk],0)
    else if (MaxValue.Value<1E300) then MessageDialog(Format(
        GetErrorMessage(veTooBigError),[TextForValue(FMinValue)]),mtInformation,
            [mbOk],0)
    else MessageDialog(Format(GetErrorMessage(veTooSmallError),
        [TextForValue(FMinValue)]),mtInformation,[mbOk],0);
  end
  else inherited DoError(ErrorCode);
end;

Function TMeasureValidator.GetUseableUnits
   : LongInt;
  begin
    Move(FAllowedUnits,Result,SizeOf(Result));
  end;

Procedure TMeasureValidator.SetUseableUnits
   (
   AUnits          : LongInt
   );
  begin
    FillChar(FAllowedUnits,SizeOf(FallowedUnits),#0);
    Move(AUnits,FAllowedUnits,SizeOf(AUnits));
  end;

Function TMeasureValidator.GetPopupMenu
   : TPopupMenu;
  var MenuItem     : TMenuItem;
      Convertible  : TMeasureUnitsSet;
      AAllowedUnits: TMeasureUnitsSet;
  Procedure AddMeasuresToMenu
     (
     Menu          : TMenuItem;
     AUnits        : TMeasureUnitsSet;
     MenuFn        : TNotifyEvent
     );
    var Cnt        : TMeasureUnits;
        Cnt1       : Integer;
        MenuItem   : TMenuItem;
    begin
      for Cnt:=Low(Cnt) to High(Cnt) do if Cnt in AUnits then begin
        MenuItem:=TMenuItem.Create(Result);
        MenuItem.Caption:=UnitsToName(Cnt);
        MenuItem.OnClick:=MenuFn;
        Cnt1:=0;
        while (Cnt1<Menu.Count)
            and (AnsiCompareText(Menu.Items[Cnt1].Caption,MenuItem.Caption)<=0) do
          Inc(Cnt1);   
        Menu.Insert(Cnt1,MenuItem);
      end;
    end;
  begin
    Result:=inherited GetPopupMenu;
    Convertible:=ConvertibleUnits(Units)*AllowedUnits;
    AAllowedUnits:=AllowedUnits-[Units];
    if Convertible+AAllowedUnits<>[] then begin
      { Separator }
      MenuItem:=TMenuItem.Create(Result);
      MenuItem.Caption:='-';
      Result.Items.Add(MenuItem);
      { Ma�einheiten Men� }
      if AAllowedUnits<>[] then begin
        MenuItem:=TMenuItem.Create(Result);
        Result.Items.Add(MenuItem);
        MenuItem.Caption:=MlgStringList['Validators',56];
        AddMeasuresToMenu(MenuItem,AAllowedUnits,DoSetUnits);
      end;
      { Umwandeln Men� }
      if Convertible<>[] then begin
        MenuItem:=TMenuItem.Create(Result);
        Result.Items.Add(MenuItem);
        MenuItem.Caption:=MlgStringList['Validators',55];
        AddMeasuresToMenu(MenuItem,Convertible,DoConvertUnits);
      end;
    end;  
  end;

Procedure TMeasureValidator.DoSetUnits
   (
   Sender          : TObject
   );
  var AValue       : Double; 
  begin
    AValue:=Values[Units];
    Units:=NameToUnits(TMenuItem(Sender).Caption);
    Value:=AValue;
     {brovak}
      OnChangeUnits(Self);
    {brovak}
    Control.SetFocus;
    MoveCursorToEnd;
  end;

Procedure TMeasureValidator.DoConvertUnits
   (
   Sender          : TObject
   );
  begin
    Units:=NameToUnits(TMenuItem(Sender).Caption);
     {brovak}
      OnChangeUnits(Self);
    {brovak}
    Control.SetFocus;
    MoveCursorToEnd;    
  end;

{==============================================================================+
  TMeasureLookupValidator                                                      
+==============================================================================}

Constructor TMeasureLookupValidator.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    FLookupList:=TStringList.Create;
  end;

Destructor TMeasureLookupValidator.Destroy;
  begin
    FLookupList.Free;
    inherited Destroy;
  end;

Procedure TMeasureLookupValidator.SetLookupIndex
   (
   AIndex          : Integer
   );
  begin
    if AIndex<0 then AsText:=''
    else if (AIndex>=0) and (AIndex<FLookupList.Count) then
       AsText:=FLookupList[AIndex];
  end;

Function TMeasureLookupValidator.GetLookupIndex
   : Integer;
  var AText        : String;
  begin
    AText:=Text;
    for Result:=0 to FLookupList.Count-1 do
        if AnsiCompareText(AText,FLookupList[Result])=0 then Exit;
    Result:=-1;
  end;

Procedure TMeasureLookupValidator.SetLookupList
   (
   AList           : TStringList
   );
  begin
    FLookuplist.Assign(AList);
  end;
     
Function TMeasureLookupValidator.FormatOnExit
   (
   var S           : String
   )
   : Boolean;
  var AIndex       : Integer;
  begin
    AIndex:=GetLookupIndex;
    if AIndex>=0 then S:=FLookupList[AIndex]
    else inherited FormatOnExit(S);
    Result:=TRUE;
  end;

Function TMeasureLookupValidator.CheckValid(const S:String):Word;
begin
  if GetLookupIndex>=0 then Result:=0
  else Result:=inherited CheckValid(S);
end;

Function TMeasureLookupValidator.GetPopupMenu
   : TPopupMenu;
  var MenuItem     : TMenuItem;
      MenuItem1    : TMenuItem;
      Cnt          : Integer;
      Cnt1         : Integer;
  begin
    Result:=inherited GetPopupMenu;
    if LookupList.Count>0 then begin
      { add a separator if this was not already done by TMeasureValidator }
      if ConvertibleUnits(Units)*AllowedUnits+AllowedUnits-[Units]=[] then begin
        MenuItem:=TMenuItem.Create(Result);
        MenuItem.Caption:='-';
        Result.Items.Add(MenuItem);
      end;
      { add 'special values' sub-menu }
      MenuItem:=TMenuItem.Create(Result);
      Result.Items.Add(MenuItem);
      MenuItem.Caption:=MlgStringList['Validators',57];
      { add all special values to the sub-menu }
      for Cnt:=0 to LookupList.Count-1 do begin
        MenuItem1:=TMenuItem.Create(MenuItem);
        MenuItem1.Caption:=LookupList[Cnt];
        MenuItem1.OnClick:=DoSetSpecialValue;
        Cnt1:=0;
        while (Cnt1<MenuItem.Count)
            and (AnsiCompareText(MenuItem.Items[Cnt1].Caption,MenuItem1.Caption)<=0) do
          Inc(Cnt1);
        MenuItem.Insert(Cnt1,MenuItem1);
      end;
    end;
  end;

Procedure TMeasureLookupValidator.DoSetSpecialValue
   (
   Sender          : TObject
   );
  begin
    AsText:=TMenuItem(Sender).Caption;
    MoveCursorToEnd;
  end;
   
{===============================================================================
| TTimeValidator
+==============================================================================}

Constructor TTimeValidator.Create
   (
   AOwner          : TComponent
   );
  begin                  
    inherited Create(AOwner);
    Picture:=Format('#[#]%s#[#]%s#[#]',[TimeSeparator,TimeSeparator]);
  end;

Procedure TTimeValidator.SetShowHundreds
   (
   AHundreds       : Boolean
   );
  begin
    if AHundreds<>FShowHundreds then begin
      FShowHundreds:=AHundreds;
      if FShowHundreds then Picture:=Format('#[#]%s#[#].#[#]',[TimeSeparator])
      else Picture:=Format('#[#]%s#[#]%s#[#]',[TimeSeparator,TimeSeparator]);
    end;
  end;
   
Function TTimeValidator.GetTime
   : TDateTime;
  begin
    if not TextToTime(Text,Result) then Result:=FDefaultValue;
  end;

Function TTimeValidator.TextToTime(Const S:String;var Time:TDateTime):Boolean;
var Min          : Integer;
    Sec          : Integer;
    HSec         : Integer;
begin
  Result:=True;
  try
    if FShowHundreds then begin
      ScanString('%i'+TimeSeparator+'%i.%i',S,[@Min,@Sec,@HSec]);
      if (Min>60) or (Sec>60) or (HSec>99) then Result:=FALSE
      else Time:=(Min*60+Sec+HSec/100)/86400;
    end
    else Time:=StrToTime(S);
  except
  end;
end;

Procedure TTimeValidator.SetTime(Const Time:TDateTime);
var Hour         : Word;
    Min          : Word;
    Sec          : Word;
    MSec         : Word;
begin
  if FShowHundreds then begin
    DecodeTime(Time,Hour,Min,Sec,MSec);
    Text:=Format('%.02d%s%.02d.%.02d',[Min,TimeSeparator,Sec,MSec Div 10])
  end
  else Text:=FormatDateTime('hh:nn:ss',Time);
end;

Function TTimeValidator.CheckValid(const S:String):Word;
var Time         : TDateTime;
begin
  Result:=inherited CheckValid(S);
  if (Result=0) and (S<>'') and not TextToTime(S,Time) then Result:=veInvalidInput;
end;

{==============================================================================}
{ Initialisierungs- und Terminierungscode                                      }
{==============================================================================}

Initialization
  RegisterClass(TValidator);
  RegisterClass(TIntValidator);
  RegisterClass(TFloatValidator);
  RegisterClass(TPXValidator);
  RegisterClass(TTimeValidator);
  RegisterClass(TScaleValidator);
  RegisterClass(TMeasureValidator);
  RegisterClass(TFileValidator);
  RegisterClass(TPathValidator);
  WindowHooks:=TList.Create;

Finalization
  WindowHooks.Free;

end.


