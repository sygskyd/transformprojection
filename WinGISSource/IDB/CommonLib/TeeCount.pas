{**********************************************}
{   TCountTeeFunction Component                }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeCount;

interface

Uses Classes,Graphics,Teengine,Chart,Series;

Type TCountTeeFunction=class(TTeeFunction)
     public
       Function Calculate(Series:TChartSeries; First,Last:Integer):Double; override;
       Function CalculateMany(SeriesList:TList; ValueIndex:Integer):Double;  override;
     end;

implementation

Uses SysUtils,TeeProco;

Function TCountTeeFunction.Calculate(Series:TChartSeries; First,Last:Integer):Double;
begin
  if First=TeeAllValues then result:=ValueList(Series).Count
                        else result:=Last-First+1;
end;

Function TCountTeeFunction.CalculateMany(SeriesList:TList; ValueIndex:Integer):Double;
var t : Integer;
begin
  result:=0;
  for t:=0 to SeriesList.Count-1 do
    if ValueList(TChartSeries(SeriesList[t])).Count>ValueIndex then result:=result+1;
end;

initialization
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{
   RegisterTeeBasicFunction(TCountTeeFunction, TeeMsg_FunctionCount);
}
   RegisterTeeBasicFunction(TCountTeeFunction, TeeMsg_FunctionCount, ftCount);
{-- Moskaliov Business Graphics BUILD#150 17.01.01}
finalization
   UnRegisterTeeFunctions([TCountTeeFunction]);
end.
