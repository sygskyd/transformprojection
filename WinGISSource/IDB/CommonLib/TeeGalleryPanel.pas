{*********************************************}
{   TeeChart Gallery Panel                     }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeGalleryPanel;

interface

Uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     {$IFDEF CLX}
     Qt, QControls, QExtCtrls, QForms,
     {$ELSE}
     Controls, ExtCtrls, Forms,
     {$ENDIF}
     Classes, Chart, TeeProcs, TeEngine
     {$IFDEF D6}, Types{$ENDIF};

Const TeeGalleryNumRows=3;
      TeeGalleryNumCols=4;
      TeeCursorDisabled=crNoDrop;

type
  TGalleryChart=class(TCustomChart)
  private
    Procedure AfterDraw(Sender:TObject);
    {$IFNDEF CLX}
    procedure CMMouseEnter(var Message: TMessage); message CM_MOUSEENTER;
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    {$ENDIF}
  protected
    {$IFDEF CLX}
    procedure MouseEnter(AControl:TControl); override;
    procedure MouseLeave(AControl:TControl); override;
    {$ENDIF}
  public
    Constructor Create(AOwner:TComponent); override;

    Procedure CheckShowLabels;
    Procedure DrawFrame3D(Erase:Boolean);
    Procedure Focus(Is3D:Boolean);
    Procedure SetMargins;
    Procedure UnFocus(Is3D:Boolean);
  end;

  TChartList=class(TList)
  private
    Function GetChart(Index:Integer):TGalleryChart;
  public
    {$IFNDEF D4}
    Destructor Destroy; override;
    {$ELSE}
    Procedure Clear; override;
    {$ENDIF}
    property Items[Index:Integer]:TGalleryChart read GetChart; default;
  end;

  TChartGalleryPanel=class(TCustomPanelNoCaption)
  private
    FColWidth        : Integer;
    FDisplaySub      : Boolean;
    FNumRows         : Integer;
    FNumCols         : Integer;
    FRowHeight       : Integer;
    FView3D          : Boolean;
    FOnChangeChart   : TNotifyEvent;
    FOnSelectedChart : TNotifyEvent;
    FOnSubSelected   : TNotifyEvent;

    tmpType   : TTeeSeriesType;
    tmpSeries : TChartSeries;
    tmpG      : TChartGalleryPanel;
    tmpSub    : TForm;
    Procedure ChartEvent(Sender: TObject);
    Procedure SetNumCols(Value:Integer);
    Procedure SetNumRows(Value:Integer);
    Procedure SetView3D(Value:Boolean);
    procedure ShowSubGallery;
    procedure SubKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SubPaintBox(Sender:TObject);
    procedure SubPanelClick(Sender:TObject);
    procedure SubSelected(Sender: TObject);
    Function ValidSeries(Const ASeriesType:TTeeSeriesType; Const APage:String):Boolean;
    {$IFNDEF CLX}
    procedure WMGetDlgCode(var Message: TWMGetDlgCode); message WM_GETDLGCODE;
    {$ENDIF}
  protected
    Procedure CalcChartWidthHeight;
    procedure ChartOnDblClick(Sender: TObject);
    Function CreateSubChart(Const ATitle:String):TCustomAxisPanel;
    Procedure CreateSubGallery(AGallery:TChartGalleryPanel; AClass:TChartSeriesClass);
    procedure FindSelectedChart;
    Procedure GetChartXY(AChart:TGalleryChart; Var x,y:Integer);
    procedure Resize; override;
    Procedure ResizeChart(AChart:TGalleryChart);
    Procedure ShowSelectedChart;
  public
    Charts           : TChartList;
    CheckSeries      : Boolean;
    FunctionsVisible : Boolean;
    SelectedChart    : TGalleryChart;
    SelectedSeries   : TChartSeries;

    Constructor Create(AOwner:TComponent); override;
    Destructor Destroy; override;

    procedure SelectChart(Chart: TGalleryChart); virtual;
    Function CreateChart(Const AType:TTeeSeriesType):TCustomAxisPanel;
    Procedure CreateChartList(ASeriesList:Array of TChartSeriesClass);
    Procedure CreateGalleryPage(Const PageName:String);
    property ColWidth:Integer read FColWidth;
    Function GetSeriesClass( Var tmpClass:TChartSeriesClass;
                             Var tmpFunctionClass:TTeeFunctionClass;
                             Var SubIndex:Integer):Boolean;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    Procedure ResizeCharts;
    property RowHeight:Integer read FRowHeight;
  published
    property DisplaySub:Boolean read FDisplaySub write FDisplaySub default True;
    property NumRows:Integer read FNumRows write SetNumRows default TeeGalleryNumRows;
    property NumCols:Integer read FNumCols write SetNumCols default TeeGalleryNumCols;
    property View3D:Boolean read FView3D write SetView3D default True;
    { events }
    property OnSelectedChart:TNotifyEvent read FOnSelectedChart write FOnSelectedChart;
    property OnChangeChart:TNotifyEvent read FOnChangeChart write FOnChangeChart;
    property OnSubSelected:TNotifyEvent read FOnSubSelected write FOnSubSelected;

    { TPanel properties }
    property Align;
    property BevelInner;
    property BevelOuter;
    property BevelWidth;
    property BorderWidth;
    property BorderStyle;
    property Color;
    {$IFNDEF CLX}
    property DragCursor;
    {$ENDIF}
    property DragMode;
    property Enabled;
    property ParentColor;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    {$IFDEF D4}
    property Anchors;
    property Constraints;
    {$ENDIF}
    {$IFNDEF CLX}
    {$IFDEF D4}
    property AutoSize;
    property DragKind;
    {$ENDIF}
    {$ENDIF}
    property Locked;

    { TPanel events }
    property OnClick;
    {$IFDEF D5}
    property OnContextPopup;
    {$ENDIF}
    property OnDblClick;
    {$IFNDEF CLX}
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnStartDrag;
    {$ENDIF}
    property OnEnter;
    property OnExit;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    {$IFDEF D4}
    property OnConstrainedResize;
    {$ENDIF}
    {$IFNDEF CLX}
    {$IFDEF D4}
    property OnCanResize;
    {$IFNDEF TEEOCX}
    property OnDockDrop;
    property OnDockOver;
    property OnEndDock;
    property OnStartDock;
    property OnUnDock;
    {$ENDIF}
    {$ENDIF}
    {$ENDIF}
  end;

implementation

Uses SysUtils, TeeConst, TeCanvas, Series,
     Objects,        {++ Moskaliov Business Graphics BUILD#150 17.01.01}
     {$IFDEF CLX}
     QGraphics
     {$ELSE}
     Graphics
     {$ENDIF}
     ;

Const clTeeSelected = clSilver; //$0080FFFF;   { <-- color use to select charts }

{ TChartList }
{$IFNDEF D4}
Destructor TChartList.Destroy;
{$ELSE}
Procedure TChartList.Clear;
{$ENDIF}
var t : Integer;
begin
  for t:=0 to Count-1 do Items[t].Free;
  inherited;
end;

Function TChartList.GetChart(Index:Integer):TGalleryChart;
begin
  result:=TGalleryChart(List^[Index]);
end;

{ TGalleryChart }
Constructor TGalleryChart.Create(AOwner: TComponent);
begin
   inherited;
   Legend.Visible:=False;
   LeftAxis.Labels:=False;
   BottomAxis.Labels:=False;
   Title.Font.Color:=clNavy;

   Zoom.Animated:=True;
   with View3DOptions do
      begin
         Orthogonal:=False;
         Zoom       :=90;
         Perspective:=55;
         Rotation   :=335;
         Elevation  :=350;
      end;
   Chart3DPercent:=100;
   ClipPoints:=False;

   Frame.Visible:=False;
   BevelWidth:=2;
   BevelOuter:=bvNone;
   with LeftWall do
      begin
         Color:=clWhite;
         Size:=4;
         Pen.Color:=clDkGray;
      end;
   with BottomWall do
      begin
         Color:=clWhite;
         Size:=4;
         Pen.Color:=clDkGray;
      end;
   OnAfterDraw:=AfterDraw;
end;

Procedure TGalleryChart.SetMargins;
Var tmp : Integer;
begin
  if View3D then tmp:=4 else tmp:=5;
  MarginTop    :=tmp;
  MarginBottom :=tmp;
  MarginLeft   :=tmp;
  MarginRight  :=tmp;
end;

Procedure TGalleryChart.DrawFrame3D(Erase:Boolean);
var R : TRect;
begin
  if Cursor<>TeeCursorDisabled then
  begin
    R:=ClientRect;
    if Erase then Frame3D(DelphiCanvas,R,clBtnFace,clBtnFace,1)
             else Frame3D(DelphiCanvas,R,clWhite,clDkGray,1);
  end;
end;

type TSeriesAccess=class(TChartSeries);

Procedure TGalleryChart.Focus(Is3D:Boolean);
begin
  With Gradient do
  begin
    Visible:=True;
    Direction:=gdFromTopLeft;
    StartColor:=clTeeSelected;
    EndColor:=clWhite;
  end;
  View3DOptions.Rotation:=345;
  if SeriesCount>0 then TSeriesAccess(Series[0]).GalleryChanged3D(Is3D);
  With Title.Font do
  begin
    Style:=[fsBold];
    Color:=clBlack;
    Size:=9;
  end;
  BevelOuter:=bvRaised;
  if Showing then SetFocus;
end;

Procedure TGalleryChart.UnFocus(Is3D:Boolean);
begin
  if Gradient.Visible then
  begin
    Gradient.Visible:=False;
    BevelOuter:=bvNone;
    With Title.Font do
    begin
      Style:=[];
      Color:=clNavy;
      Size:=GetDefaultFontSize;
    end;
    View3DOptions.Rotation:=335;
    if SeriesCount>0 then TSeriesAccess(Series[0]).GalleryChanged3D(Is3D);
  end;
end;

Procedure TGalleryChart.CheckShowLabels;

  Function IsMaximized:Boolean;
  {$IFNDEF CLX}
  var Placement : TWindowPlacement;
  {$ENDIF}
  begin
    {$IFDEF CLX}
    result:=GetParentForm(Self).WindowState=wsMaximized;
    {$ELSE}
    Placement.length := SizeOf(TWindowPlacement);
    GetWindowPlacement(GetParentForm(Self).Handle, @Placement);
    result:=Placement.ShowCmd=SW_SHOWMAXIMIZED;
    {$ENDIF}
  end;

begin
  if AxisVisible then
  begin
    LeftAxis.Labels:=IsMaximized;
    BottomAxis.Labels:=LeftAxis.Labels;
  end;
end;

{$IFDEF CLX}
procedure TGalleryChart.MouseEnter(AControl:TControl);
{$ELSE}
procedure TGalleryChart.CMMouseEnter(var Message: TMessage);
{$ENDIF}
begin
  DrawFrame3D(False);
  inherited;
end;

{$IFDEF CLX}
procedure TGalleryChart.MouseLeave(AControl:TControl);
{$ELSE}
procedure TGalleryChart.CMMouseLeave(var Message: TMessage);
{$ENDIF}
begin
  DrawFrame3D(True);
  inherited;
end;

procedure TGalleryChart.AfterDraw(Sender: TObject);
Const tmpH=6;
      tmpStart=4;
      tmpW=7;
begin
  if Gradient.Visible then
    if TChartGalleryPanel(Parent).DisplaySub then
    With Canvas do
    begin
      Pen.Mode:=pmNotXor;
      Brush.Style:=bsSolid;
      Brush.Color:=clBlack;
      Pen.Style:=psClear;
      Polygon([Point(tmpStart,Height-tmpH-2),Point(tmpStart+tmpW,Height-tmpH-2),
               Point(tmpStart+(tmpW div 2),Height-4)]);
      Pen.Mode:=pmCopy;
      Pen.Style:=psSolid;
    end;
end;

{ TChartGalleryPanel }
Constructor TChartGalleryPanel.Create(AOwner:TComponent);
begin
  inherited;
  FunctionsVisible:=False;
  SelectedChart:=nil;
  SelectedSeries:=nil;
  FOnSelectedChart:=nil;
  FOnChangeChart:=nil;
  Charts:=TChartList.Create;
  FView3D:=True;
  DisplaySub:=True;
  FNumRows:=3;
  FNumCols:=4;
end;

Destructor TChartGalleryPanel.Destroy;
begin
  Charts.Free;
  inherited;
end;

Procedure TChartGalleryPanel.SetNumCols(Value:Integer);
begin
  FNumCols:=Value;
  ResizeCharts;
end;

Procedure TChartGalleryPanel.SetNumRows(Value:Integer);
begin
  FNumRows:=Value;
  ResizeCharts;
end;

Procedure TChartGalleryPanel.GetChartXY(AChart:TGalleryChart; Var x,y:Integer);
var tmp : Integer;
begin
  tmp:=Charts.IndexOf(AChart);
  y:=tmp div NumCols;
  x:=tmp mod NumCols;
end;

procedure TChartGalleryPanel.ChartOnDblClick(Sender: TObject);
begin
  SelectedChart:=TGalleryChart(Sender);
  if Assigned(FOnSelectedChart) then OnSelectedChart(Self);
end;

Procedure TChartGalleryPanel.CalcChartWidthHeight;
var tmp : Integer;
begin
  if (NumRows>0) and (NumCols>0) then
  begin
    tmp:=BevelWidth+BorderWidth;
    FRowHeight:=(Height-tmp) div NumRows;
    FColWidth:=(Width-tmp) div NumCols;
  end
  else
  begin
    FRowHeight:=0;
    FColWidth:=0;
  end;
end;

Function TChartGalleryPanel.CreateChart(Const AType: TTeeSeriesType): TCustomAxisPanel;
var tmpClass : TChartSeriesClass;

   Procedure CreateSeries;
   var tmp : Integer;
   begin
      for tmp:=1 to MaxLong(1,AType.NumGallerySeries) do
          CreateNewSeries(nil,result,tmpClass,AType.FunctionClass);
   end;

var t              : Integer;
    DisabledSeries : Boolean;
begin
   tmpClass:=AType.SeriesClass;
   if not Assigned(tmpClass) then
      tmpClass:=TLineSeries;

   result:=TGalleryChart.Create(Self);
   Charts.Add(result);
   with TGalleryChart(result) do
      begin
         Title.Text.Add(AType.Description);
         Parent:=Self;
         Name:=TeeGetUniqueName(Owner,TeeMsg_GalleryChartName);
         ResizeChart(TGalleryChart(result));

         CreateSeries;
         DisabledSeries:=CheckSeries and
                         Assigned(SelectedSeries) and
                         ( not Series[0].IsValidSourceOf(SelectedSeries) );
{++ Moskaliov Business Graphics BUILD#150 17.01.01}
         if not DisabledSeries then
            if WinGIS_runing then
               DisabledSeries:=AType.SeriesType > 4;   // Current realized
{++ Moskaliov Business Graphics BUILD#150 17.01.01}

         if DisabledSeries then
            begin
               Cursor:=TeeCursorDisabled;
               OriginalCursor:=Cursor;
               OnClick:=nil;
               OnDblClick:=nil;
               Title.Font.Color:=clGray;
               LeftWall.Pen.Color:=clGray;
               BottomWall.Pen.Color:=clGray;
               LeftAxis.Axis.Width:=1;
               LeftAxis.Axis.Color:=clWhite;
               BottomAxis.Axis.Width:=1;
               BottomAxis.Axis.Color:=clWhite;
            end
         else
            begin
               Cursor:=crHandPoint;
               OriginalCursor:=Cursor;
               OnClick:=ChartEvent;
               OnDblClick:=ChartOnDblClick;
               OnEnter:=ChartEvent;
            end;
         TSeriesAccess(Series[0]).GalleryChanged3D(Self.FView3D);
         for t:=0 to SeriesCount-1 do
             TSeriesAccess(Series[t]).PrepareForGallery(not DisabledSeries);
         SetMargins;
         CheckShowLabels;
      end;
end;

procedure TChartGalleryPanel.KeyDown(var Key: Word; Shift: TShiftState);

    Function FindChartXY(x,y:Integer):TGalleryChart;
    var tmpX : Integer;
        tmpY : Integer;
        t    : Integer;
    begin
      for t:=0 to Charts.Count-1 do
      begin
        result:=Charts[t];
        GetChartXY(result,tmpX,tmpY);
        if (x=tmpx) and (y=tmpy) then Exit;
      end;
      result:=nil;
    end;

var x,y : Integer;
    tmp : TGalleryChart;
begin
  inherited;
  GetChartXY(SelectedChart,x,y);
  Case Key of
    {$IFDEF CLX}Key_Left{$ELSE}VK_LEFT{$ENDIF}:   if x>0 then Dec(x);
    {$IFDEF CLX}Key_Right{$ELSE}VK_RIGHT{$ENDIF}:  if x<NumCols then Inc(x);
    {$IFDEF CLX}Key_Up{$ELSE}VK_UP{$ENDIF}:     if y>0 then Dec(y);
    {$IFDEF CLX}Key_Down{$ELSE}VK_DOWN{$ENDIF}:
               if ssAlt in Shift then
               begin
                 if DisplaySub then ShowSubGallery
               end
               else if y<NumRows then Inc(y);
    {$IFDEF CLX}Key_Return{$ELSE}VK_RETURN{$ENDIF}: ChartOnDblClick(SelectedChart);
  end;
  tmp:=FindChartXY(x,y);
  if Assigned(tmp) and (tmp.Cursor=crHandPoint) then SelectChart(tmp);
end;

Procedure TChartGalleryPanel.ResizeChart(AChart:TGalleryChart);
var tmp    : Integer;
    tmpCol : Integer;
    tmpRow : Integer;
    R      : TRect;
begin
  if (NumCols>0) and (NumRows>0) then
  begin
    GetChartXY(AChart,tmpCol,tmpRow);
    tmp:=BevelWidth+BorderWidth;
    With R do
    begin
      Left:=tmp+(tmpCol*ColWidth);
      Top:=tmp+(tmpRow*RowHeight);
      Right:=Left+MinLong(ColWidth,Width-tmp)-1;
      Bottom:=Top+MinLong(RowHeight,Height-tmp)-1;
    end;
    AChart.BoundsRect:=R;
    AChart.CheckShowLabels;
  end;
end;

Procedure TChartGalleryPanel.ShowSelectedChart;
begin
  if not Assigned(SelectedChart) then
     if Charts.Count>0 then SelectedChart:=Charts[0];
  if Assigned(SelectedChart) then
  begin
    SelectedChart.Focus(FView3D);
    if Assigned(FOnChangeChart) then OnChangeChart(Self);
  end;
end;

procedure TChartGalleryPanel.FindSelectedChart;
var t : Integer;
begin
  SelectedChart:=nil;
  if Assigned(SelectedSeries) then
  begin
    for t:=0 to Charts.Count-1 do
    With Charts[t].Series[0] do
    if (ClassType=SelectedSeries.ClassType) and (FunctionType=nil) then
    begin
      SelectedChart:=Charts[t];
      break;
    end;
  end;
  ShowSelectedChart;
end;

procedure TChartGalleryPanel.SubKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key={$IFDEF CLX}Key_Escape{$ELSE}VK_ESCAPE{$ENDIF} then
     tmpSub.ModalResult:=mrCancel
  else
     tmpG.KeyDown(Key,Shift);
end;

procedure TChartGalleryPanel.SubSelected(Sender: TObject);
var t   : Integer;
    tmpClass : TChartSeriesClass;
    tmpNum   : Integer;
begin
  With SelectedChart do
  begin
    Tag:=tmpG.Charts.IndexOf(tmpG.SelectedChart);
    if Tag=0 then
    begin
      tmpClass:=TChartSeriesClass(Series[0].ClassType);
      tmpNum:=SeriesCount;
      FreeAllSeries;
      for t:=1 to tmpNum do CreateNewSeries(nil,SelectedChart,tmpClass,nil);
      TSeriesAccess(Series[0]).GalleryChanged3D(tmpG.View3D);
      for t:=0 to SeriesCount-1 do
          TSeriesAccess(Series[t]).PrepareForGallery(True);
    end
    else
    for t:=0 to SeriesCount-1 do
        TSeriesAccess(Series[t]).SetSubGallery(Series[t],Tag);
  end;
  tmpSub.ModalResult:=mrOk;
end;

procedure TChartGalleryPanel.SubPaintBox(Sender:TObject);
begin
  With TPaintBox(Sender).Canvas do
  begin
    Brush.Color:=clBlack;
    Pen.Style:=psClear;
    Polygon([Point(0,8),Point(4,0),Point(9,8)]);
  end;
end;

procedure TChartGalleryPanel.SubPanelClick(Sender:TObject);
begin
  tmpSub.ModalResult:=mrOk;
end;

Procedure TChartGalleryPanel.CreateSubGallery(AGallery:TChartGalleryPanel; AClass:TChartSeriesClass);
var tmpN : Integer;
begin
  tmpG:=AGallery;
  tmpType:=TTeeSeriesType.Create;
  try
    tmpType.SeriesClass:=AClass;
    tmpType.NumGallerySeries:=1;
    tmpType.FunctionClass:=nil;
    tmpSeries:=tmpType.SeriesClass.Create(nil);
    With AGallery do
    begin
      Charts.Clear;
      With TSeriesAccess(Self.tmpSeries) do
           CreateSubGallery(Self.CreateSubChart);
    end;
    tmpSeries.Free;
    AGallery.CalcChartWidthHeight;
    tmpN:=AGallery.Charts.Count div AGallery.NumCols;
    if AGallery.Charts.Count mod AGallery.NumCols > 0 then Inc(tmpN);
    AGallery.Parent.Height:=8+AGallery.FRowHeight*tmpN;
    AGallery.NumRows:=tmpN;
  finally
    tmpType.Free;
  end;
end;

procedure TChartGalleryPanel.ShowSubGallery;
var tmp      : TPoint;
    tmpPanel : TPanel;
    tmpPaint : TPaintBox;
begin
  if Assigned(tmpSub) then FreeAndNil(tmpSub);
  SelectedChart.UnFocus(FView3D);
  tmp:=ClientToScreen(Point(SelectedChart.Left,SelectedChart.Top+SelectedChart.Height));
  tmpSub:=TForm.Create(Self);
  try
    With tmpSub do
    begin
      BorderStyle:={$IFDEF CLX}fbsNone{$ELSE}bsNone{$ENDIF};
      Left:=tmp.X;
      Top:=tmp.Y;
      KeyPreview:=True;
      OnKeyDown:=SubKeyDown;
    end;
    tmpPanel:=TPanel.Create(tmpSub);
    tmpPanel.Align:=alTop;
    tmpPanel.Caption:='';
    tmpPanel.Height:=12;
    tmpPanel.Parent:=tmpSub;
    tmpPanel.OnClick:=SubPanelClick;
    tmpPaint:=TPaintBox.Create(tmpSub);
    tmpPaint.Parent:=tmpPanel;
    tmpPaint.BoundsRect:=Rect(4,2,12,10);
    tmpPaint.OnClick:=SubPanelClick;
    tmpPaint.OnPaint:=SubPaintBox;

    tmpG:=TChartGalleryPanel.Create(tmpSub);
    tmpG.View3D:=View3D;
    tmpG.OnSelectedChart:=SubSelected;
    tmpG.DisplaySub:=False;
    tmpG.Align:=alClient;
    tmpG.Parent:=tmpSub;
    CreateSubGallery(tmpG,TChartSeriesClass(SelectedChart[0].ClassType));
    tmpG.ShowSelectedChart;
    if tmpSub.ShowModal=mrOk then
       if Assigned(FOnSubSelected) then FOnSubSelected(Self);
  finally
    FreeAndNil(tmpSub);
  end;
  ShowSelectedChart;
end;

procedure TChartGalleryPanel.ChartEvent(Sender: TObject);
begin
  if Sender is TGalleryChart then
     SelectChart(TGalleryChart(Sender));
end;

procedure TChartGalleryPanel.SelectChart(Chart: TGalleryChart);
var t : Integer;
begin
  SelectedChart:=Chart;
  ShowSelectedChart;
  for t:=0 to Charts.Count-1 do
      if Charts[t]<>SelectedChart then Charts[t].UnFocus(FView3D);

  if DisplaySub then
  begin
    if {$IFDEF D6}Types.{$ENDIF}PtInRect(Rect(0,SelectedChart.Height-12,12,SelectedChart.Height),
        SelectedChart.GetCursorPos) then ShowSubGallery;
  end;
end;

procedure TChartGalleryPanel.SetView3D(Value:Boolean);
var t : Integer;
begin
  if Value<>FView3D then
  begin
    FView3D:=Value;
    for t:=0 to Charts.Count-1 do
    begin
      TSeriesAccess(Charts[t].Series[0]).GalleryChanged3D(FView3D);
      Charts[t].SetMargins;
    end;
  end;
end;

Procedure TChartGalleryPanel.CreateChartList(ASeriesList:Array of TChartSeriesClass);
var t     : Integer;
    AType : TTeeSeriesType;
begin
  Charts.Clear;
  for t:=Low(ASeriesList) to High(ASeriesList) do
  begin
    AType:=TeeSeriesTypes.Find(ASeriesList[t]);
    if Assigned(AType) then CreateChart(AType);
  end;
  FindSelectedChart;
end;

Procedure TChartGalleryPanel.ResizeCharts;
var t : Integer;
begin
  CalcChartWidthHeight;
  for t:=0 to Charts.Count-1 do ResizeChart(Charts[t]);
end;

procedure TChartGalleryPanel.Resize;
begin
  inherited;
  ResizeCharts;
end;

Function TChartGalleryPanel.ValidSeries(Const ASeriesType:TTeeSeriesType; Const APage:String):Boolean;
begin
  result:= (ASeriesType.GalleryPage=APage) and
           (
             (FunctionsVisible and Assigned(ASeriesType.FunctionClass)) or
             ((not FunctionsVisible) and (not Assigned(ASeriesType.FunctionClass)))

           );
end;

Function TChartGalleryPanel.CreateSubChart(Const ATitle:String):TCustomAxisPanel;
var tmp : Integer;
begin
   tmpType.Description:=ATitle;
   tmp:=tmpG.Charts.Count;
   result:=tmpG.CreateChart(tmpType);
   TSeriesAccess(tmpSeries).SetSubGallery(result[0],tmp);
end;

Procedure TChartGalleryPanel.CreateGalleryPage(Const PageName: String);
var tmp    : TTeeSeriesType;
    t      : Integer;
begin
  {$IFNDEF D4}
   Charts.Free;
   Charts:=TChartList.Create;
  {$ENDIF}
   Charts.Clear;
   with TeeSeriesTypes do
     for t:=0 to Count-1 do
         begin
            tmp:=Items[t];
            if (tmp.SeriesType < 5) and (ValidSeries(tmp,PageName)) then
               CreateChart(tmp);
         end;
     FindSelectedChart;
end;

{$IFNDEF CLX}
procedure TChartGalleryPanel.WMGetDlgCode(var Message: TWMGetDlgCode);
begin
  Message.Result := DLGC_WANTARROWS;
end;
{$ENDIF}

Function TChartGalleryPanel.GetSeriesClass( Var tmpClass:TChartSeriesClass;
                                            Var tmpFunctionClass:TTeeFunctionClass;
                                            Var SubIndex:Integer):Boolean;
begin
  result:=Assigned(SelectedChart);
  if result then
  begin
    tmpSeries:=SelectedChart.Series[0];
    tmpClass:=TChartSeriesClass(tmpSeries.ClassType);
    if Assigned(tmpSeries) and Assigned(tmpSeries.FunctionType) then
       tmpFunctionClass:=TTeeFunctionClass(tmpSeries.FunctionType.ClassType)
    else
       tmpFunctionClass:=nil;
    SubIndex:=SelectedChart.Tag;
  end;
end;

initialization
  RegisterTeeStandardSeries;
end.

