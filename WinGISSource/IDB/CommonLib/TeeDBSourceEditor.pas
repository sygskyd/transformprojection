{********************************************}
{     TeeChart Pro Charting Library          }
{ Copyright (c) 1995-2000 by David Berneda   }
{         All Rights Reserved                }
{********************************************}
{$I TeeDefs.inc}
unit TeeDBSourceEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeeDBEdit, StdCtrls, ExtCtrls, TeeSelectList, DB;

type
  TDBChartSourceEditor = class(TBaseDBChartEditor)
    procedure BApplyClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CBSourcesChange(Sender: TObject);
  private
    { Private declarations }
    ISources : TSelectListForm;
    Procedure FillFields;
    Function DataSource:TDataSource;
    procedure OnChangeSources(Sender: TObject);
  protected
    Function IsValid(AComponent:TComponent):Boolean; override;
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}
Uses DBChart, Chart, TeeProcs, TeeConst, TeePenDlg;

Function TDBChartSourceEditor.IsValid(AComponent:TComponent):Boolean;
begin
  result:=AComponent is TDataSource;
end;

procedure TDBChartSourceEditor.BApplyClick(Sender: TObject);
var tmpSt : String;
    t     : Integer;
begin
  inherited;
  TheSeries.DataSource:=nil;
  With ISources.ToList do
  if Items.Count>0 then
  begin
    tmpSt:=Items[0];
    for t:=1 to Items.Count-1 do
        tmpSt:=tmpSt+';'+Items[t];
  end
  else tmpSt:='';
  TheSeries.MandatoryValueList.ValueSource:=tmpSt;
  TheSeries.DataSource:=DataSource;
  BApply.Enabled:=False;
end;

Function TDBChartSourceEditor.DataSource:TDataSource;
begin
  With CBSources do
  if ItemIndex=-1 then result:=nil
                  else result:=TDataSource(Items.Objects[CBSources.ItemIndex]);
end;

procedure TDBChartSourceEditor.OnChangeSources(Sender: TObject);
begin
  BApply.Enabled:=True;
end;

procedure TDBChartSourceEditor.FormShow(Sender: TObject);
begin
  inherited;
  LLabel.Caption:=TeeMsg_AskDataSource;
  ISources:=TSelectListForm.Create(Self);
  ISources.Align:=alClient;
  ISources.OnChange:=OnChangeSources;
  AddFormTo(ISources,Self,Tag);
  FillFields;
end;

procedure TDBChartSourceEditor.CBSourcesChange(Sender: TObject);
begin
  inherited;
  FillFields;
  TheSeries.XLabelsSource:='';
end;

Procedure TDBChartSourceEditor.FillFields;

    Procedure AddField(Const tmpSt:String; tmpType:TFieldType);
    begin
      Case TeeFieldType(tmpType) of
      tftNumber,
     tftDateTime: ISources.FromList.Items.Add(tmpSt);
      end;
    end;

    {$IFDEF D4}
    Procedure AddAggregateFields;
    var t : Integer;
    begin
       With DataSource.DataSet do
       for t:=0 to AggFields.Count-1 do
           AddField(AggFields[t].FieldName,ftFloat);
    end;
    {$ENDIF}

var tmpSt : String;
    tmpField : String;
    t     : Integer;
begin
  ISources.FromList.Clear;
  ISources.ToList.Clear;
  if (DataSource<>nil) and (DataSource.DataSet<>nil) then
  begin
    With DataSource.DataSet do
    if FieldCount>0 then
    begin
       for t:=0 to FieldCount-1 do
           AddField(Fields[t].FieldName,Fields[t].DataType);
       {$IFDEF D4}
       AddAggregateFields;
       {$ENDIF}
    end
    else
    begin
      FieldDefs.Update;
      for t:=0 to FieldDefs.Count-1 do
          AddField(FieldDefs[t].Name,FieldDefs[t].DataType);
      {$IFDEF D4}
      AddAggregateFields;
      {$ENDIF}
    end;

    tmpSt:=TheSeries.MandatoryValueList.ValueSource;
    for t:=1 to TeeNumFields(tmpSt) do
    begin
      tmpField:=TeeExtractField(tmpSt,t);
      With ISources.FromList.Items do
      if IndexOf(tmpField)<>-1 then
      begin
        ISources.ToList.Items.Add(tmpField);
        Delete(IndexOf(tmpField));
      end;
    end;
  end;
  ISources.EnableButtons;
end;

end.
