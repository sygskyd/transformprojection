{********************************************}
{    TeeChart Pro Charting Library           }
{    Calendar Series Editor                  }
{  Copyright (c) 1995-2000 by David Berneda  }
{         All Rights Reserved                }
{********************************************}
unit TeeCalendarEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, TeCanvas, TeePenDlg, ComCtrls, TeeCustomShapeEditor,
  TeeCalendar;

type
  TCalendarSeriesEditor = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ButtonPen1: TButtonPen;
    CBWeekDays: TCheckBox;
    CBWeekUpper: TCheckBox;
    CBTrailing: TCheckBox;
    CBToday: TCheckBox;
    TabSheet2: TTabSheet;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    PageControl3: TPageControl;
    TabSheet4: TTabSheet;
    PageControl4: TPageControl;
    TabSheet5: TTabSheet;
    PageControl5: TPageControl;
    TabSheet6: TTabSheet;
    PageControl6: TPageControl;
    CBMonths: TCheckBox;
    TabSheet7: TTabSheet;
    PageControl7: TPageControl;
    CBMonthUpper: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure CBWeekDaysClick(Sender: TObject);
    procedure CBWeekUpperClick(Sender: TObject);
    procedure CBTrailingClick(Sender: TObject);
    procedure CBTodayClick(Sender: TObject);
    procedure CBMonthsClick(Sender: TObject);
    procedure CBMonthUpperClick(Sender: TObject);
  private
    { Private declarations }
    IDays,ISunday,IToday,ITrailing,
    IMonths,IWeekDays:TFormTeeShape;
    Calendar:TCalendarSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TCalendarSeriesEditor.FormShow(Sender: TObject);

  Procedure Check(AForm:TFormTeeShape);
  begin
    AForm.RefreshControls(AForm.TheShape);
  end;

begin
  Calendar:=TCalendarSeries(Tag);
  With Calendar do
  begin
    ButtonPen1.LinkPen(Pen);
    IDays:=InsertTeeObjectForm(PageControl2,Days);
    IWeekDays:=InsertTeeObjectForm(PageControl3,WeekDays);
    ISunday:=InsertTeeObjectForm(PageControl5,Sunday);
    ITrailing:=InsertTeeObjectForm(PageControl6,Trailing);
    IToday:=InsertTeeObjectForm(PageControl4,Today);
    IMonths:=InsertTeeObjectForm(PageControl7,Months);
  end;
  Check(IDays);
  Check(IWeekDays);
  Check(IToday);
  Check(ISunday);
  Check(ITrailing);
  Check(IMonths);
  CBWeekDays.Checked:=Calendar.WeekDays.Visible;
  CBTrailing.Checked:=Calendar.Trailing.Visible;
  CBMonths.Checked:=Calendar.Months.Visible;
  CBToday.Checked:=Calendar.Today.Visible;
  CBWeekUpper.Checked:=Calendar.WeekDays.UpperCase;
  CBMonthUpper.Checked:=Calendar.Months.UpperCase;
end;

procedure TCalendarSeriesEditor.CBWeekDaysClick(Sender: TObject);
begin
  Calendar.WeekDays.Visible:=CBWeekDays.Checked;
end;

procedure TCalendarSeriesEditor.CBWeekUpperClick(Sender: TObject);
begin
  Calendar.WeekDays.UpperCase:=CBWeekUpper.Checked;
end;

procedure TCalendarSeriesEditor.CBTrailingClick(Sender: TObject);
begin
  Calendar.Trailing.Visible:=CBTrailing.Checked;
end;

procedure TCalendarSeriesEditor.CBTodayClick(Sender: TObject);
begin
  Calendar.Today.Visible:=CBToday.Checked;
end;

procedure TCalendarSeriesEditor.CBMonthsClick(Sender: TObject);
begin
  Calendar.Months.Visible:=CBMonths.Checked;
end;

procedure TCalendarSeriesEditor.CBMonthUpperClick(Sender: TObject);
begin
  Calendar.Months.UpperCase:=CBMonthUpper.Checked;
end;

initialization
  RegisterClass(TCalendarSeriesEditor);
end.
