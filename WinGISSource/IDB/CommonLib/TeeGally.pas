{**********************************************}
{   TeeChart Gallery Dialog                    }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
unit TeeGally;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     {$IFDEF CLX}
     QGraphics, QControls, QExtCtrls, QStdCtrls, QComCtrls, QForms,
     {$ELSE}
     Graphics, Controls, StdCtrls, ExtCtrls, ComCtrls, Forms,
     {$ENDIF}
     SysUtils, Classes, TeEngine, Chart, TeeGalleryPanel, TeeProcs,
  MultiLng;

type
  TTeeGallery = class(TForm)
    P1                 : TPanel;
    BOk                : TButton;
    CB3D               : TCheckBox;
    TabPages           : TTabControl;
    TabTypes           : TTabControl;
    ChartGalleryPanel1 : TChartGalleryPanel;
    MlgSection1: TMlgSection;
    procedure CB3DClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TabPagesChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TabTypesChange(Sender: TObject);
    procedure ChartGalleryPanel1SelectedChart(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure HideButtons;
    Procedure HideFunctions;
  end;

{ Shows the gallery and asks the user a Series type.
  If user double clicks a chart or presses Ok, a new Series
  is created and returned. The new Series is owned by AOwner
  parameter (usually the Form).
}
Function CreateNewSeriesGallery( AOwner:TComponent;
                                 OldSeries:TChartSeries;
                                 tmpChart:TCustomAxisPanel;
                                 AllowSameType,
                                 CheckSeries:Boolean;
                                 Var SubIndex:Integer ):TChartSeries;

{ Shows the gallery and asks the user a Series type. Then
  changes tmpSeries to the new type. }
procedure ChangeSeriesTypeGallery(AOwner:TComponent; Var tmpSeries:TChartSeries);

{ Shows the Gallery Dialog and lets the user choose a Series type.
  Returns True if user pressed OK.
  The "tmpClass" parameter returns the choosen type.
}
Function GetChartGalleryClass( AOwner:TComponent;
                               OldSeries:TChartSeries;
                               ShowFunctions:Boolean;
                               Var tmpClass:TChartSeriesClass;
                               Var Show3D:Boolean;
                               Var tmpFunctionClass:TTeeFunctionClass;
                               CheckValidSeries:Boolean;
                               Var SubIndex:Integer):Boolean;

{ Shows the gallery and asks the user a Series type. Then
  changes all Series in AChart to the new type. }
procedure ChangeAllSeriesGallery( AOwner:TComponent; AChart:TCustomChart );

implementation

{$R *.dfm}

Uses TeeConst, Series, TeCanvas;

{ TeeGallery }
Procedure TTeeGallery.HideFunctions;
begin
  TabTypes.Tabs.Delete(1);
  ChartGalleryPanel1.FunctionsVisible:=False;
end;

Procedure TTeeGallery.HideButtons;
begin
  P1.Visible:=False;
end;

procedure TTeeGallery.CB3DClick(Sender: TObject);
begin
  ChartGalleryPanel1.View3D:=CB3D.Checked;
end;

procedure TTeeGallery.FormShow(Sender: TObject);
begin
  TabTypes.TabIndex:=0;
  TabTypesChange(Self);
end;

procedure TTeeGallery.TabPagesChange(Sender: TObject);
begin
  With ChartGalleryPanel1 do
  begin
    FunctionsVisible:=TabTypes.TabIndex=1;
    View3D:=CB3D.Checked;
    With TabPages do
    if TabIndex<>-1 then CreateGalleryPage(Tabs[TabIndex]{$IFDEF CLX}.Caption{$ENDIF})
                    else Charts.Clear;
  end;
end;

procedure TTeeGallery.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  ChartGalleryPanel1.KeyDown(Key,Shift);
end;

procedure TTeeGallery.TabTypesChange(Sender: TObject);

   Procedure AddTabPages;
   var t : Integer;
   begin
      TabPages.Tabs.Clear;

      with TeeSeriesTypes do
         for t:=0 to Count-1 do
             with Items[t] do
                if NumGallerySeries>0 then
                   {$IFNDEF CLX}
                   if TabPages.Tabs.IndexOf(GalleryPage)=-1 then
                   {$ENDIF}
                      case TabTypes.TabIndex of
                         0: if not Assigned(FunctionClass) then
                               TabPages.Tabs.Add(GalleryPage);
                         1: if Assigned(FunctionClass) then
                               TabPages.Tabs.Add(GalleryPage);
                      end;
   end;

begin
   AddTabPages;
   TabPages.TabIndex:=0;
   TabPagesChange(Self);
end;

procedure TTeeGallery.ChartGalleryPanel1SelectedChart(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

{ Helper functions }
Function GetChartGalleryClass( AOwner:TComponent;
                               OldSeries:TChartSeries;
                               ShowFunctions:Boolean;
                               Var tmpClass:TChartSeriesClass;
                               Var Show3D:Boolean;
                               Var tmpFunctionClass:TTeeFunctionClass;
                               CheckValidSeries:Boolean;
                               Var SubIndex:Integer):Boolean;
begin
   result:=False;
   with TTeeGallery.Create(nil) do
      try
         CB3D.Checked:=Show3D;
         ChartGalleryPanel1.SelectedSeries:=OldSeries;
         if not ShowFunctions then
            HideFunctions;
         ChartGalleryPanel1.CheckSeries:=CheckValidSeries;
         if ShowModal=mrOk then
            begin
               result:=ChartGalleryPanel1.GetSeriesClass(tmpClass,tmpFunctionClass,SubIndex);
               if result then
                  Show3D:=ChartGalleryPanel1.SelectedChart.View3D;
            end;
      finally
        Free;
      end;
end;

type TSeriesAccess=class(TChartSeries);

Function CreateNewSeriesGallery( AOwner:TComponent;
                                 OldSeries:TChartSeries;
                                 tmpChart:TCustomAxisPanel;
                                 AllowSameType,
                                 CheckSeries:Boolean;
                                 Var SubIndex:Integer ):TChartSeries;
var Show3D   : Boolean;
    tmpClass : TChartSeriesClass;
    tmpFunctionClass:TTeeFunctionClass;
begin
   result:=nil;
   Show3D:=tmpChart.View3D;
   if GetChartGalleryClass(AOwner,OldSeries,AllowSameType,tmpClass,Show3D,
                           tmpFunctionClass,CheckSeries,SubIndex) then
      begin
         tmpChart.View3D:=Show3D;
         if (not Assigned(OldSeries)) or
            (AllowSameType or ((tmpClass<>OldSeries.ClassType) or (SubIndex<>-1))) then
            begin
              result:=CreateNewSeries(AOwner,tmpChart,tmpClass,tmpFunctionClass);
              if SubIndex<>-1 then TSeriesAccess(result).SetSubGallery(result,SubIndex);
            end;
      end;
end;

procedure ChangeSeriesTypeGallery(AOwner:TComponent; Var tmpSeries:TChartSeries);
var NewSeries : TChartSeries;
    SubIndex  : Integer;
begin
  NewSeries:=CreateNewSeriesGallery(AOwner,tmpSeries,tmpSeries.ParentChart,False,True,SubIndex);
  if Assigned(NewSeries) then
  begin
    AssignSeries(tmpSeries,NewSeries);
    tmpSeries:=NewSeries;
  end;
end;

procedure ChangeAllSeriesGallery( AOwner:TComponent; AChart:TCustomChart );
var NewSeries : TChartSeries;
    tmpSeries : TChartSeries;
    SubIndex  : Integer;
begin
  if AChart.SeriesCount>0 then
  begin
    tmpSeries:=AChart.Series[0];
    NewSeries:=CreateNewSeriesGallery(AOwner,tmpSeries,AChart,False,True,SubIndex);
    if Assigned(NewSeries) then
    begin
      AssignSeries(tmpSeries,NewSeries);
      ChangeAllSeriesType(AChart,TChartSeriesClass(NewSeries.ClassType));
    end;
  end;
end;

procedure TTeeGallery.FormCreate(Sender: TObject);
begin
     Left:=Screen.Width-Width-20;
     Top:=20;
end;

end.
