�
 TPOLARSERIESEDITOR 0�  TPF0TPolarSeriesEditorPolarSeriesEditorLeft� Top� BorderIconsbiSystemMenu BorderStylebsDialogClientHeight� ClientWidthEColor	clBtnFace
ParentFont	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabel	LAngleIncLeftTopLWidth_Height	AlignmenttaRightJustifyAutoSizeCaptionAngle &Increment:FocusControl
SEAngleInc  TLabelLabel8Left TophWidthgHeight	AlignmenttaRightJustifyAutoSizeCaptionRadius Increment:  	TCheckBoxCBCloseLeft]TopWidthTHeightHelpContext� CaptionC&lose CircleChecked	State	cbCheckedTabOrder OnClickCBCloseClick  TEdit
SEAngleIncLeftjTopHWidth0HeightHelpContext.TabOrderText0OnChangeSEAngleIncChange  TEditSERadiusIncLeftjTopdWidth0HeightHelpContext�TabOrderText0OnChangeSERadiusIncChange  TUpDownUDRadiusIncLeft� TopdWidthHeight	AssociateSERadiusIncMin Max�Position TabOrderWrap  TUpDown
UDAngleIncLeft� TopHWidthHeight	Associate
SEAngleIncMin MaxhPosition TabOrderWrap  
TButtonPenBPenLeftTopHelpContext�Caption&Pen...TabOrder  
TButtonPenBPiePenLeft`Top(HelpContext�Caption
&Circle...TabOrder  TButtonBBrushLeftTop(WidthKHeightHelpContextuCaption
Pattern...TabOrderOnClickBBrushClick  	TGroupBox	GroupBox1Left� TopWidth� HeightuCaptionLabels:TabOrder TButtonBFontLeft	TopVWidthKHeightHelpContext]Caption&Font...TabOrder OnClick
BFontClick  	TCheckBoxCBAngleLabelsLeftTopWidth<HeightHelpContext\Caption&VisibleTabOrderOnClickCBAngleLabelsClick  	TCheckBoxCBLabelsRotLeftTop(WidthLHeightHelpContext^CaptionR&otatedTabOrderOnClickCBLabelsRotClick  	TCheckBoxCBClockWiseLeft@TopWidthIHeightHelpContext)Caption	ClockWiseTabOrderOnClickCBClockWiseClick  	TCheckBoxCBInsideLeftTop?WidthLHeightHelpContext*CaptionI&nsideTabOrderOnClickCBInsideClick    