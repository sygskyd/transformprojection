{***************************************************************
 *
 * Purpose  : The Editor dialog for Funnel or Pipeline Series
 * Author   : Marjan Slatinek, marjan@steema.com
 *
 ****************************************************************}
unit TeeFunnelEditor;
{$I TeeDefs.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, TeeFunnel, StdCtrls, TeCanvas, TeePenDlg, ComCtrls;

type
  TFunnelSeriesEditor = class(TForm)
    ButtonPen1: TButtonPen;
    ButtonPen2: TButtonPen;
    Button1: TButton;
    AboveColor: TButtonColor;
    WithinColor: TButtonColor;
    BelowColor: TButtonColor;
    Label1: TLabel;
    Edit1: TEdit;
    DifLimit: TUpDown;
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
  private
    { Private declarations }
    Funnel : TFunnelSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

Uses TeeBrushDlg;

procedure TFunnelSeriesEditor.FormShow(Sender: TObject);
begin
  Funnel:=TFunnelSeries(Tag);
  ButtonPen1.LinkPen(Funnel.LinesPen);
  ButtonPen2.LinkPen(Funnel.Pen);
  AboveColor.LinkProperty(Funnel,'AboveColor');
  WithinColor.LinkProperty(Funnel,'WithinColor');
  BelowColor.LinkProperty(Funnel,'BelowColor');
  DifLimit.Position:=Round(Funnel.DifferenceLimit);
end;

procedure TFunnelSeriesEditor.Button1Click(Sender: TObject);
begin
  EditChartBrush(Self,Funnel.Brush);
end;

procedure TFunnelSeriesEditor.Edit1Change(Sender: TObject);
begin
  if Showing then Funnel.DifferenceLimit:=DifLimit.Position;
end;

initialization
  RegisterClass(TFunnelSeriesEditor);
end.
