{**********************************************}
{  TTeeFont (or derived) Editor Dialog         }
{  Copyright (c) 1999-2000 by David Berneda    }
{**********************************************}
{$I TeeDefs.inc}
unit TeeEdiFont;

interface

uses
  {$IFDEF LINUX}
  LibC,
  {$ELSE}
  Windows, Messages,
  {$ENDIF}
  SysUtils, Classes,
  {$IFDEF CLX}
  QGraphics, QControls, QForms, QDialogs, QStdCtrls, QComCtrls, QExtCtrls,
  {$ELSE}
  Graphics, Controls, Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls,
  {$ENDIF}
  TeeProcs, TeCanvas, TeePenDlg, MultiLng;

type
  TTeeFontEditor = class(TForm)
    Button3: TButton;
    SHText: TShape;
    Label2: TLabel;
    Edit2: TEdit;
    UDInter: TUpDown;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    BFontShadowColor: TButtonColor;
    Edit3: TEdit;
    UDFontShadowHoriz: TUpDown;
    Label4: TLabel;
    Edit4: TEdit;
    UDFontShadowVert: TUpDown;
    BOutline: TButtonPen;
    MlgSection1: TMlgSection;
    procedure BFontClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SHTextMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Edit2Change(Sender: TObject);
    procedure Edit3Change(Sender: TObject);
    procedure Edit4Change(Sender: TObject);
  private
    { Private declarations }
    TheFont      : TTeeFont;
  public
    { Public declarations }
    procedure RefreshControls(AFont:TTeeFont);
  end;

Function InsertTeeFontEditor(ATab:TTabSheet):TTeeFontEditor;

implementation

{$R *.dfm}
Uses TeeConst, TeeBrushDlg;

Function InsertTeeFontEditor(ATab:TTabSheet):TTeeFontEditor;
begin
  result:=TTeeFontEditor.Create(ATab.Owner);
  AddFormTo(result,ATab,0);
end;

procedure TTeeFontEditor.BFontClick(Sender: TObject);
begin
  EditTeeFont(Self,TheFont);
end;

procedure TTeeFontEditor.RefreshControls(AFont:TTeeFont);
begin
  TheFont:=AFont;
  With TheFont do
  begin
    SHText.Brush.Color     :=Color;
    UDInter.Position       :=InterCharSize;
    UDFontShadowHoriz.Position :=Shadow.HorizSize;
    UDFontShadowVert.Position  :=Shadow.VertSize;
    BOutline.LinkPen(OutLine);
    BFontShadowColor.LinkProperty(Shadow,'Color');
  end;
end;

procedure TTeeFontEditor.FormShow(Sender: TObject);
begin
  if Assigned(TheFont) then RefreshControls(TheFont);
end;

procedure TTeeFontEditor.SHTextMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  With TheFont do
  begin
    Color:=EditColor(Self,Color);
    SHText.Brush.Color:=Color;
  end;
end;

procedure TTeeFontEditor.Edit2Change(Sender: TObject);
begin
  if Showing then TheFont.InterCharSize:=UDInter.Position;
end;

procedure TTeeFontEditor.Edit3Change(Sender: TObject);
begin
  if Showing then TheFont.Shadow.HorizSize:=UDFontShadowHoriz.Position;
end;

procedure TTeeFontEditor.Edit4Change(Sender: TObject);
begin
  if Showing then TheFont.Shadow.VertSize:=UDFontShadowVert.Position;
end;

end.
 