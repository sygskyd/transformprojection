{**********************************************}
{   TTeeCommander Component                    }
{   Copyright (c) 1998-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
{$IFDEF TEEOCX}
{$I teeAxdefs.inc}
{$ENDIF}
unit TeeComma;

interface

uses Windows, Classes, Controls, StdCtrls, Buttons, TeeProcs, ExtCtrls,
     Forms, Graphics, Series,
     {$IFNDEF TEEOCXSTD}
     TeeEdit,
     {$ENDIF}
     Chart;

type
  TCustomTeeCommander=class(TCustomPanelNoCaption,ITeeEventListener)
  private
    FMouseButton : TMouseButton;
    FPanel       : TCustomTeePanel;
    FVertical    : Boolean;

    procedure TeeEvent(Event:TTeeEvent);
    Procedure ReAlignTeeControls;
    Procedure SetVertical(Value:Boolean);
  protected
    Procedure DoMouseDown(X,Y:Integer); virtual;
    Procedure DoMouseMove(X,Y:Integer); virtual;
    Procedure DoMouseUp; virtual;
    Function DoPanelMouse:Boolean; virtual;
    Procedure FreeOwnedControls;
    procedure Loaded; override;
    procedure Notification( AComponent: TComponent;
                            Operation: TOperation); override;
    Procedure RemovingControl(AControl:TControl); dynamic;
    procedure SetPanel(Const Value: TCustomTeePanel); virtual;
    Function TeePanelClass:String;
    Procedure ShowHideControls(Value:Boolean); virtual;
    {$IFNDEF D4}
    { IUnknown }
    function QueryInterface(const IID: TGUID; out Obj): Integer; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    {$ENDIF}
  public
    { Public declarations }
    Constructor Create(AOwner:TComponent); override;
    Destructor Destroy; override;

    Procedure CreateBevel(APos:Integer);
    Function CreateButton( APos:Integer; AProc:TNotifyEvent;
                           Const AHint:String;
                           Const AResName:String;
                           AGroupIndex:Integer):TSpeedButton;
    Function CreateLabel(APos:Integer; AColor:TColor):TLabel;
  published
    { Published declarations }
    property Panel:TCustomTeePanel read FPanel write SetPanel;
    property Vertical:Boolean read FVertical write SetVertical default False;

    { TPanel properties }
    property Align;
    property BevelInner;
    property BevelOuter;
    property BevelWidth;
    property BorderWidth;
    property BorderStyle;
    property Color;
    property DragMode;
    {$IFNDEF CLX}
    property DragCursor;
    {$ENDIF}
    property Enabled;
    property ParentColor;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint default True;
    property TabOrder;
    property TabStop;
    property Visible;
    {$IFDEF D4}
    property Anchors;
    property Constraints;
    {$ENDIF}
    {$IFNDEF CLX}
    {$IFDEF D4}
    property AutoSize;
    property DragKind;
    {$ENDIF}
    {$ENDIF}

    { TPanel events }
    property OnClick;
    {$IFNDEF CLX}
    {$IFDEF D5}
    property OnContextPopup;
    {$ENDIF}
    {$ENDIF}
    property OnDblClick;
    {$IFNDEF CLX}
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnStartDrag; 
    {$ENDIF}
    property OnEnter;
    property OnExit;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    {$IFDEF D4}
    property OnConstrainedResize;
    {$ENDIF}
    {$IFNDEF CLX}
    {$IFDEF D4}
    property OnCanResize;
    {$IFNDEF TEEOCX}
    property OnDockDrop;
    property OnDockOver;
    property OnEndDock;
    property OnStartDock;
    property OnUnDock;
    {$ENDIF}
    {$ENDIF}
    {$ENDIF}
  end;

  TeeCommanderControls=( tcbNormal,
                         tcbSeparator,
                         tcbRotate,
                         tcbMove,
                         tcbZoom,
                         tcbDepth,
                         tcbEdit,
                         tcbPrintPreview,
                         tcbCopy,
                         tcbSave,
                         tcbLabel,
                         tcb3D );

  TTeeCommander=class;
  TTeeEditedChartEvent=procedure(Sender:TTeeCommander; AChart:TCustomChart) of object;

  TTeeCommander=class(TCustomTeeCommander)
  private
    { Private declarations }
    FButtonCopy    : TSpeedButton;
    FButtonDepth   : TSpeedButton;
    FButtonEdit    : TSpeedButton;
    FButtonMove    : TSpeedButton;
    FButtonNormal  : TSpeedButton;
    FButtonPrint   : TSpeedButton;
    FButtonRotate  : TSpeedButton;
    FButtonSave    : TSpeedButton;
    FButtonZoom    : TSpeedButton;
    FButton3D      : TSpeedButton;
    {$IFNDEF TEEOCXSTD}
    FEditor        : TChartEditor;
    FPreviewer     : TChartPreviewer;
    {$ENDIF}
    FLabel         : TLabel;
    FLabelValues   : Boolean;

    FDragging      : Boolean;
    FDraggingIndex : Integer;
    FOldX          : Integer;
    FOldY          : Integer;

    FOnEditedChart : TTeeEditedChartEvent;

    procedure Button3DClick(Sender: TObject);
    procedure ButtonCopyClick(Sender: TObject);
    procedure ButtonDepthClick(Sender: TObject);
    procedure ButtonEditClick(Sender: TObject);
    procedure ButtonMoveClick(Sender: TObject);
    procedure ButtonNormalClick(Sender: TObject);
    procedure ButtonPrintClick(Sender: TObject);
    procedure ButtonRotateClick(Sender: TObject);
    procedure ButtonSaveClick(Sender: TObject);
    procedure ButtonZoomClick(Sender: TObject);
    Function IsButtonDown(AButton:TSpeedButton):Boolean;
    procedure SetLabelCaption(Const ACaption:String);
    procedure SetLabelValues(Value:Boolean);

    Function FirstSeriesPie:TPieSeries;
  protected
    { Protected declarations }
    Procedure DoMouseDown(X,Y:Integer); override;
    Procedure DoMouseMove(X,Y:Integer); override;
    Procedure DoMouseUp; override;
    Function DoPanelMouse:Boolean; override;
    Procedure RemovingControl(AControl:TControl); override;
    Procedure ShowHideControls(Value:Boolean); override;
  public
    { Public declarations }
    Constructor Create(AOwner:TComponent); override;

    Procedure CreateControls(AControls:Array of TeeCommanderControls);

    property Button3D      : TSpeedButton read FButton3D;
    property ButtonCopy    : TSpeedButton read FButtonCopy;
    property ButtonDepth   : TSpeedButton read FButtonDepth;
    property ButtonEdit    : TSpeedButton read FButtonEdit;
    property ButtonMove    : TSpeedButton read FButtonMove;
    property ButtonNormal  : TSpeedButton read FButtonNormal;
    property ButtonPrint   : TSpeedButton read FButtonPrint;
    property ButtonRotate  : TSpeedButton read FButtonRotate;
    property ButtonSave    : TSpeedButton read FButtonSave;
    property ButtonZoom    : TSpeedButton read FButtonZoom;

    {$IFNDEF TEEOCXSTD}
    property ChartEditor   : TChartEditor read FEditor write FEditor;
    property Previewer     : TChartPreviewer read FPreviewer write FPreviewer;
    {$ENDIF}
    property LabelText     : TLabel read FLabel;
    Procedure ShowValues;
  published
    property LabelValues:Boolean read FLabelValues write SetLabelValues default True;
    property OnEditedChart:TTeeEditedChartEvent read FOnEditedChart
                                                write FOnEditedChart;
  end;

implementation

{$R TeeComma.res}

Uses TeEngine, TeCanvas, SysUtils, TeeProCo, TeeEditPro
     {$IFNDEF TEEOCXSTD}
     ,EditChar, TeePrevi
     {$ENDIF}
     ;

{ TCustomTeeCommander }
Constructor TCustomTeeCommander.Create(AOwner:TComponent);
begin
  inherited;
  Panel:=nil;
  Height:=33;
  Width:=400;
  FMouseButton:=mbLeft;
  ShowHint:=True;
  if (csDesigning in ComponentState) and
     (not (csLoading in Owner.ComponentState)) then Align:=alTop;
end;

Destructor TCustomTeeCommander.Destroy;
begin
  FreeOwnedControls;
  Panel:=nil;
  inherited;
end;

procedure TCustomTeeCommander.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if Operation=opRemove then
     if Assigned(FPanel) and (AComponent=FPanel) then
        Panel:=nil;
end;

{$IFNDEF D4}
function TCustomTeeCommander.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if VCLComObject = nil then
  begin
    if GetInterface(IID, Obj) then Result := S_OK
    else Result := E_NOINTERFACE
  end
  else
    Result := IVCLComObject(VCLComObject).QueryInterface(IID, Obj);
end;

function TCustomTeeCommander._AddRef: Integer;
begin
  if VCLComObject = nil then
    Result := -1   // -1 indicates no reference counting is taking place
  else
    Result := IVCLComObject(VCLComObject)._AddRef;
end;

function TCustomTeeCommander._Release: Integer;
begin
  if VCLComObject = nil then
    Result := -1   // -1 indicates no reference counting is taking place
  else
    Result := IVCLComObject(VCLComObject)._Release;
end;
{$ENDIF}

procedure TCustomTeeCommander.TeeEvent(Event:TTeeEvent);
begin
  if Event is TTeeMouseEvent then
  With TTeeMouseEvent(Event) do
  begin
    Case Event of
      meDown: begin
                FMouseButton:=Button;
                DoMouseDown(X,Y);
              end;
      meUp:  DoMouseUp;
      meMove: DoMouseMove(X,Y);
    end;
    Sender.CancelMouse:=not DoPanelMouse;
  end
  else
  if Event is TTeeView3DEvent then
     ShowHideControls(Assigned(Panel));
end;

Procedure TCustomTeeCommander.DoMouseDown(X,Y:Integer);
begin
end;

Procedure TCustomTeeCommander.DoMouseMove(X,Y:Integer);
begin
end;

Procedure TCustomTeeCommander.DoMouseUp;
begin
end;

Procedure TCustomTeeCommander.ShowHideControls(Value:Boolean);
Var t : Integer;
begin
  if not( csDestroying in ComponentState) then
     for t:=0 to ControlCount-1 do
         if Controls[t].Owner=Self then Controls[t].Enabled:=Value;
end;

Procedure TCustomTeeCommander.ReAlignTeeControls;
var t      : Integer;
    tmpPos : Integer;
begin
  tmpPos:=4;
  for t:=0 to ControlCount-1 do
  if Controls[t].Owner=Self then
  begin
    With Controls[t] do
    begin
      if FVertical then
      begin
        Left:=4;
        Top:=tmpPos;
      end
      else
      begin
        Left:=tmpPos;
        Top:=4;
      end;
    end;
    if Controls[t] is TBevel then
    begin
      Inc(tmpPos,4+2);
      with Controls[t] as TBevel do
      if FVertical then
      begin
        Shape:=bsTopLine;
        Width:=Self.Width-2*4;
        Height:=2;
      end
      else
      begin
        Shape:=bsLeftLine;
        Width:=2;
        Height:=Self.Height-2*4;
      end;
    end
    else Inc(tmpPos,25+4);
  end;
end;

procedure TCustomTeeCommander.SetVertical(Value:Boolean);
begin
  if FVertical<>Value then
  begin
    FVertical:=Value;
    if Align<>alNone then
       if FVertical then Align:=alLeft
                    else Align:=alTop;
    ReAlignTeeControls;
  end;
end;

type TTeePanelAccess=class(TCustomTeePanel);

procedure TCustomTeeCommander.SetPanel(const Value: TCustomTeePanel);
begin
  if Assigned(FPanel) then TTeePanelAccess(FPanel).Listeners.Remove(Self);
  FPanel:=Value;
  if Assigned(FPanel) then
  begin
    FPanel.FreeNotification(Self);
    TTeePanelAccess(FPanel).Listeners.Add(Self);
  end;
  if not (csDestroying in ComponentState) then
    ShowHideControls(Assigned(FPanel));
end;

procedure TCustomTeeCommander.Loaded;
var t : Integer;
begin
  inherited;
  if not Assigned(FPanel) then
  if Assigned(Parent) then
  With Parent do
  for t:=0 to ComponentCount-1 do
    if Components[t] is TCustomTeePanel then
    begin
      Self.Panel:=TCustomTeePanel(Components[t]);
      break;
    end;
  ShowHideControls(Assigned(FPanel));
end;

Function TCustomTeeCommander.CreateButton( APos:Integer;
                                           AProc:TNotifyEvent;
                                           Const AHint:String;
                                           Const AResName:String;
                                           AGroupIndex:Integer):TSpeedButton;
begin
  result:=TSpeedButton.Create(Self);
  With result do
  begin
    OnClick:=AProc;
    if FVertical then SetBounds(4,APos,25,25)
                 else SetBounds(APos,4,25,25);
    Down:= True;
    Flat:= True;
    ParentShowHint:=True;
    Hint:=AHint;
    Parent:=Self;
    {$IFNDEF D4}
    Enabled:=False;
    {$ENDIF}
    GroupIndex:=AGroupIndex;
    {$IFNDEF CLX}
    Glyph.LoadFromResourceName(HInstance,AResName);
    {$ENDIF}
  end;
end;

Function TCustomTeeCommander.DoPanelMouse:Boolean;
begin
  result:=True;
end;

Procedure TCustomTeeCommander.CreateBevel(APos:Integer);
begin
  With TBevel.Create(Self) do
  begin
    Shape:=bsLeftLine;
    Width:=2;
    Height:=Self.Height-4;
    Top:=2;
    Left:=APos;
    Parent:=Self;
  end;
end;

Function TCustomTeeCommander.CreateLabel(APos:Integer; AColor:TColor):TLabel;
begin
  result:=TLabel.Create(Self);
  With result do
  begin
    Left:=APos;
    Top:=12;
    Font.Name:=GetDefaultFontName;
    Font.Color:=AColor;
    Font.Size:=GetDefaultFontSize;
    Caption:='';
    Parent:=Self;
  end;
end;

Function TCustomTeeCommander.TeePanelClass:String;
begin
  if FPanel is TCustomAxisPanel then result:=TeeCommanMsg_Chart
                                else result:=TeeCommanMsg_Panel;
end;

Procedure TCustomTeeCommander.FreeOwnedControls;
var Num : Integer;
    tmp : Integer;
    t   : Integer;
begin
  Repeat
    Num:=0;
    tmp:=-1;
    for t:=0 to ControlCount-1 do
    if Controls[t].Owner=Self then
    begin
      Inc(Num);
      tmp:=t;
    end;
    if tmp<>-1 then
    begin
      RemovingControl(Controls[tmp]);
      Controls[tmp].Free;
    end;
  Until Num=0;
end;

procedure TCustomTeeCommander.RemovingControl(AControl: TControl);
begin
end;

{ TTeeCommander }
Constructor TTeeCommander.Create(AOwner:TComponent);
begin
  inherited;
  {$IFNDEF TEEOCXSTD}
  FEditor:=nil;
  FPreviewer:=nil;
  {$ENDIF}
  FDragging:=False;
  FLabelValues:=True;
  FVertical:=False;

  CreateControls([ tcbNormal,
                   tcbSeparator,
                   tcbRotate,
                   tcbMove,
                   tcbZoom,
                   tcbDepth,
                   tcb3D,
                   tcbSeparator,
                   tcbEdit,
                   tcbPrintPreview,
                   tcbCopy,
                   tcbSave,
                   tcbLabel ]);

  if Assigned(FButtonNormal) then FButtonNormal.Down:=True;
end;

Procedure TTeeCommander.RemovingControl(AControl:TControl);
begin
  inherited;
  if AControl=FButtonNormal then FButtonNormal:=nil else
  if AControl=FButtonRotate then FButtonRotate:=nil else
  if AControl=FButtonMove then FButtonMove:=nil else
  if AControl=FButtonZoom then FButtonZoom:=nil else
  if AControl=FButtonDepth then FButtonDepth:=nil else
  if AControl=FButtonEdit then FButtonEdit:=nil else
  if AControl=FButtonPrint then FButtonPrint:=nil else
  if AControl=FButtonCopy then FButtonCopy:=nil else
  if AControl=FButtonSave then FButtonSave:=nil else
  if AControl=FButton3D then FButton3D:=nil else
  if AControl=FLabel then FLabel:=nil;
end;

Procedure TTeeCommander.CreateControls(AControls:Array of TeeCommanderControls);
var t,
    tmpPos:Integer;
begin
  FreeOwnedControls;
  tmpPos:=4;
  for t:=Low(AControls) to High(AControls) do
  begin
    Case AControls[t] of
      tcbNormal : FButtonNormal := CreateButton(tmpPos,ButtonNormalClick,TeeCommanMsg_Normal,'TeeNormal',1);
      tcbRotate:  FButtonRotate := CreateButton(tmpPos,ButtonRotateClick,TeeCommanMsg_Rotate,'TeeRotate',1);
      tcbMove:    FButtonMove   := CreateButton(tmpPos,ButtonMoveClick,TeeCommanMsg_Move,'TeeMove',1);
      tcbZoom:    FButtonZoom   := CreateButton(tmpPos,ButtonZoomClick,TeeCommanMsg_Zoom,'TeeZoom',1);
      tcbDepth:   FButtonDepth  := CreateButton(tmpPos,ButtonDepthClick,TeeCommanMsg_Depth,'TeeDepth',1);
      tcbEdit:    FButtonEdit   := CreateButton(tmpPos,ButtonEditClick,TeeCommanMsg_Edit,'TeeEdit',0);
 tcbPrintPreview: FButtonPrint  := CreateButton(tmpPos,ButtonPrintClick,TeeCommanMsg_Print,'TeePrint',0);
      tcbCopy:    FButtonCopy   := CreateButton(tmpPos,ButtonCopyClick,TeeCommanMsg_Copy,'TeeCopy',0);
      tcbSave:    FButtonSave   := CreateButton(tmpPos,ButtonSaveClick,TeeCommanMsg_Save,'TeeSave',0);
      tcb3D:      begin
                    FButton3D:= CreateButton(tmpPos,Button3DClick,TeeCommanMsg_3D,'TeeView3D',2);
                    FButton3D.AllowAllUp:=True;
                  end;
    tcbSeparator: CreateBevel(tmpPos);
      tcbLabel:   FLabel        := CreateLabel(tmpPos,clNavy);
    end;
    if AControls[t]=tcbSeparator then Inc(tmpPos,4+2)
                                 else Inc(tmpPos,25+4);
  end;
end;

Procedure TTeeCommander.DoMouseDown(X,Y:Integer);

  Function ButtonNormalUp:Boolean;
  begin
    result:=(not Assigned(FButtonNormal)) or (not FButtonNormal.Down);
  end;

var tmpSeries : TPieSeries;
begin
  tmpSeries:=FirstSeriesPie;
  if Assigned(tmpSeries) or ButtonNormalUp then
  begin
    FDragging:=True;
    FOldX:=X;
    FOldY:=Y;
    if Assigned(tmpSeries) then FDraggingIndex:=tmpSeries.Clicked(X,Y)
                           else FDraggingIndex:=-1;
    if FPanel is TCustomTeePanel then
       TCustomTeePanel(FPanel).CancelMouse:=(FDraggingIndex<>-1) or ButtonNormalUp;
  end;
end;

Function TTeeCommander.DoPanelMouse:Boolean;

  Function IsButtonUp(AButton:TSpeedButton):Boolean;
  begin
    result:=((not Assigned(AButton)) or (not AButton.Down));
  end;

begin
  result:=IsButtonUp(FButtonRotate) and
          IsButtonUp(FButtonDepth) and
          IsButtonUp(FButtonMove) and
          IsButtonUp(FButtonZoom);
end;

Procedure TTeeCommander.DoMouseUp;
begin
  FDragging:=False;
  FDraggingIndex:=-1;
end;

Function TTeeCommander.FirstSeriesPie:TPieSeries;
var t:Integer;
begin
  result:=nil;
  if FPanel is TCustomChart then
  With TCustomChart(FPanel) do
  begin
    for t:=0 to SeriesCount-1 do
    if (Series[t] is TPieSeries) and (Series[t].Active) then
    begin
      result:=Series[t] as TPieSeries;
      exit;
    end;
  end;
end;

Function TTeeCommander.IsButtonDown(AButton:TSpeedButton):Boolean;
begin
  result:=Assigned(AButton) and AButton.Down;
end;

Procedure TTeeCommander.DoMouseMove(X,Y:Integer);

   Procedure Set3D;
   begin
     FPanel.View3D:=True;
     if Assigned(FButton3D) then FButton3D.Down:=True;
   end;

    Procedure DoRotate;

      Function CorrectAngle(Const AAngle:Integer):Integer;
      begin
        result:=AAngle;
        if result>360 then result:=result-360 else
        if result<0 then result:=360+result;
      end;

      Function CalcAngleChange(AAngle,AChange:Integer):Integer;
      var tmpMinAngle : Integer;
      begin
        if AChange>0 then result:=MinLong(360,AAngle+AChange)
        else
        begin
          if FPanel.Canvas.SupportsFullRotation then tmpMinAngle:=0
                                                else tmpMinAngle:=TeeMinAngle;
          result:=MaxLong(tmpMinAngle,AAngle+AChange);
        end;
      end;

    Var tmpX      : Integer;
        tmpY      : Integer;
        tmpSeries : TPieSeries;
    begin
      With FPanel,View3DOptions do
      begin
        Set3D;
        Orthogonal:=False;
        tmpX:=Round(90.0*(X-FOldX)/Width);
        tmpY:=Round(90.0*(FOldY-Y)/Height);
        tmpSeries:=FirstSeriesPie;
        if Canvas.SupportsFullRotation then
        begin
          Rotation:=CorrectAngle(Rotation+tmpX);
          Elevation:=CorrectAngle(Elevation+tmpY);
        end
        else
        begin
          if Assigned(tmpSeries) then
          begin
            Rotation:=360;
            if not Canvas.SupportsFullRotation then Perspective:=0;
            if tmpX<>0 then
            With tmpSeries do RotationAngle:=CorrectAngle(RotationAngle+tmpX);
          end
          else Rotation:=CalcAngleChange(Rotation,tmpX);
          Elevation:=CalcAngleChange(Elevation,tmpY);
        end;

        FOldX:=X;
        FOldY:=Y;
      end;
    end;

    Procedure DoMove;
    begin
      Set3D;
      With FPanel.View3DOptions do
      begin
        HorizOffset:=HorizOffset+(X-FOldX);
        VertOffset:=VertOffset+(Y-FOldY);
      end;
      FOldX:=X;
      FOldY:=Y;
    end;

    Procedure DoZoom;
    var tmp : Integer;
        tmp2: Integer;
    begin
      Set3D;
      With FPanel,View3DOptions do
      begin
        tmp:=Round(10.0*(FOldY-Y)/Sqrt(Sqr(ChartWidth)+Sqr(ChartHeight)));
        tmp2:=Round(tmp*Zoom/100.0);
        if tmp>0 then Zoom:=Zoom+MaxLong(1,tmp2)
                 else Zoom:=Zoom+MinLong(-1,tmp2);
        if Zoom<5 then Zoom:=5;
      end;
    end;

    Function CalcDistPercent(APercent,AWidth,AHeight:Integer):Integer;
    Var Dist      : Integer;
        ChartDiag : Integer;
    begin
      Dist:=Round(Sqrt(Sqr(1.0*FOldX-X)+Sqr(1.0*FOldY-Y)));
      ChartDiag:=Round(Sqrt(Sqr(1.0*AWidth)+Sqr(1.0*AHeight)));
      result:=Round(1.0*APercent*Dist/ChartDiag);
    end;

    Procedure DoDepth;
    var tmp : Integer;
    begin
      if FPanel is TCustomAxisPanel then
      With TCustomAxisPanel(FPanel) do
      begin
        Set3D;
        tmp:=CalcDistPercent(200,ChartWidth,ChartHeight);
        if (tmp>=1) then Chart3DPercent:=MinLong(100,tmp);
      end;
    end;

    Procedure DoNormal;
    Var tmpSeries : TPieSeries;
        tmp       : Integer;
    begin
      if FDraggingIndex<>-1 then
      begin
        tmpSeries:=FirstSeriesPie;
        if Assigned(tmpSeries) then
        With tmpSeries do
        begin
          tmp:=MinLong(100,CalcDistPercent(100,CircleWidth,CircleHeight));
          ExplodedSlice[FDraggingIndex]:= tmp;
        end;
      end;
    end;

begin
  if FDragging then
  begin
    FDragging:=False;
    if IsButtonDown(FButtonNormal) then DoNormal  { first do this !!! }
    else
    if FMouseButton=mbRight then DoMove
    else
    if IsButtonDown(FButtonRotate) then DoRotate else
    if IsButtonDown(FButtonMove)   then DoMove else
    if IsButtonDown(FButtonZoom)   then DoZoom else
    if IsButtonDown(FButtonDepth)  then DoDepth;
    if FLabelValues and (not FVertical) then ShowValues;
    FDragging:=True;
  end;
end;

procedure TTeeCommander.SetLabelCaption(Const ACaption:String);
begin
  if Assigned(FLabel) then
     if FLabelValues and (not FVertical) then
        FLabel.Caption:=Format(ACaption,[TeePanelClass])
     else
        FLabel.Visible:=False;
end;

procedure TTeeCommander.ButtonRotateClick(Sender: TObject);
begin
  SetLabelCaption(TeeCommanMsg_RotateLabel);
end;

procedure TTeeCommander.ButtonMoveClick(Sender: TObject);
begin
  SetLabelCaption(TeeCommanMsg_MoveLabel);
end;

procedure TTeeCommander.ButtonZoomClick(Sender: TObject);
begin
  SetLabelCaption(TeeCommanMsg_ZoomLabel);
end;

procedure TTeeCommander.ButtonDepthClick(Sender: TObject);
begin
  SetLabelCaption(TeeCommanMsg_DepthLabel);
end;

procedure TTeeCommander.ButtonEditClick(Sender: TObject);
begin
  {$IFNDEF TEEOCXSTD}
  if Assigned(FEditor) then FEditor.Execute
  else
  if Assigned(FPanel) and (FPanel is TCustomChart) then
     EditChart(nil,TCustomChart(FPanel));
  if Assigned(FOnEditedChart) then FOnEditedChart(Self,FPanel as TCustomChart);
  if Assigned(FButton3D) then FButton3D.Down:=FPanel.View3D;
  {$ENDIF}
end;

procedure TTeeCommander.ButtonPrintClick(Sender: TObject);
begin
  {$IFNDEF TEEOCXSTD}
  if Assigned(FPreviewer) then FPreviewer.Execute
  else
  if Assigned(FPanel) then ChartPreview(nil,FPanel);
  {$ENDIF}
end;

procedure TTeeCommander.Button3DClick(Sender: TObject);
begin
  if Assigned(FPanel) then FPanel.View3D:=FButton3D.Down;
end;

procedure TTeeCommander.ButtonCopyClick(Sender: TObject);
begin
  if Assigned(FPanel) then FPanel.CopyToClipboardBitmap;
end;

procedure TTeeCommander.ButtonNormalClick(Sender: TObject);
begin
  if FirstSeriesPie=nil then SetLabelCaption(TeeCommanMsg_NormalLabel)
                        else SetLabelCaption(TeeCommanMsg_NormalPieLabel)
end;

Procedure TTeeCommander.ShowHideControls(Value:Boolean);
begin
  EnableControls(Value,[FButtonRotate,FButtonMove,FButtonZoom,FButtonNormal,
                        FButtonCopy,FButtonSave,FButtonPrint,FButton3D]);

  if Value then Value:=FPanel is TCustomAxisPanel;

  if Assigned(FButtonDepth) then FButtonDepth.Visible:=Value;
  if Assigned(FButtonEdit) then FButtonEdit.Visible:=Value;
  if Assigned(FButtonSave) then FButtonSave.Visible:=Value;

  {$IFNDEF D4}
  if Assigned(FButtonDepth) then FButtonDepth.Enabled:=Value;
  if Assigned(FButtonEdit) then FButtonEdit.Enabled:=Value;
  if Assigned(FButtonSave) then FButtonSave.Enabled:=Value;
  {$ENDIF}

  if Assigned(FButton3D) and Assigned(FPanel) then FButton3D.Down:=FPanel.View3D;
end;

procedure TTeeCommander.SetLabelValues(Value:Boolean);
begin
  if FLabelValues<>Value then
  begin
    FLabelValues:=Value;
    if Assigned(FLabel) then FLabel.Visible:=FLabelValues;
  end;
end;

Procedure TTeeCommander.ShowValues;
var tmpSeries : TPieSeries;
begin
  With FPanel.View3DOptions do
  if IsButtonDown(FButtonRotate) then
     FLabel.Caption:=Format(TeeCommanMsg_Rotating,[Rotation,Elevation])
  else
  if IsButtonDown(FButtonMove) then
     FLabel.Caption:=Format(TeeCommanMsg_Moving,[HorizOffset,VertOffset])
  else
  if IsButtonDown(FButtonZoom) then
     FLabel.Caption:=Format(TeeCommanMsg_Zooming,[Zoom])
  else
  if IsButtonDown(FButtonDepth) then
  begin
    if FPanel is TCustomAxisPanel then
    With TCustomAxisPanel(FPanel) do
      FLabel.Caption:=Format(TeeCommanMsg_Depthing,[Chart3DPercent])
  end
  else
  if IsButtonDown(FButtonNormal) and (FDraggingIndex<>-1) then
  begin
    tmpSeries:=FirstSeriesPie;
    if tmpSeries=nil then
       FLabel.Caption:=''
    else
       FLabel.Caption:=Format( TeeCommanMsg_PieExploding,
                               [FDraggingIndex,tmpSeries.ExplodedSlice[FDraggingIndex]]);
  end
  else
  if Assigned(FLabel) then FLabel.Caption:='';

  if Assigned(FLabel) then FLabel.Update;
end;

procedure TTeeCommander.ButtonSaveClick(Sender: TObject);
begin
  if Assigned(FPanel) then
  if FPanel is TCustomChart then SaveChartDialog(FPanel as TCustomChart);
end;

end.
