{**********************************************}
{  TCustomChart (or derived) Editor Dialog     }
{  Copyright (c) 1996-2000 by David Berneda    }
{**********************************************}
{$I teedefs.inc}
unit TeeEdiSeri;

interface

uses
  {$IFDEF LINUX}
  LibC,
  {$ELSE}
  Windows, Messages,
  {$ENDIF}
  SysUtils, Classes,
  {$IFDEF CLX}
  QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls, QComCtrls, QButtons,
  {$ELSE}
  Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, ComCtrls, Buttons,
  {$ENDIF}
  TeEngine, TeeCustomShapeEditor, TeCanvas, TeePenDlg, Chart, TeeSourceEdit,
  MultiLng;

type
  { special case for TFormDesigner in Charteg unit }
  TAddComponentDataSource=Procedure( Const AComponent:TComponent;
                                     AItems:TStrings;
                                     AddCurrent:Boolean) of object;

  { special case for TFormDesigner in Charteg unit }
  TOnGetDesignerNamesEvent=Procedure( AProc:TAddComponentDataSource;
                                      ASeries:TChartSeries;
                                      AItems:TStrings;
                                      AddCurrent:Boolean );

  TOnChartGetSourceStyle=Function(ASeries:TChartSeries):TFormClass;

  TFormTeeSeries = class(TForm)
    CBPersistent: TCheckBox;
    PageSeries: TPageControl;
    TheTabSheet: TTabSheet;
    TabMarks: TTabSheet;
    TabDataSource: TTabSheet;
    PageControlMarks: TPageControl;
    TabSheet1: TTabSheet;
    CBMarksVisible: TCheckBox;
    CBMarkClip: TCheckBox;
    L23: TLabel;
    SEMarksAngle: TEdit;
    UDMarksAngle: TUpDown;
    GB3: TGroupBox;
    L32: TLabel;
    SEArrowLength: TEdit;
    BMarkLinCol: TButtonPen;
    UDArrowsLength: TUpDown;
    RGMarkStyle: TRadioGroup;
    Label3: TLabel;
    EMarksEvery: TEdit;
    UDEvery: TUpDown;
    CBMultiLine: TCheckBox;
    CBSeries: TComboBox;
    ImageSeries: TImage;
    LabelSeriesClass: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    PanTop: TPanel;
    CBDataSourcestyle: TComboBox;
    CBAllVisible: TCheckBox;
    PaintBox1: TPaintBox;
    Panel3: TPanel;
    MlgSection1: TMlgSection;
    GroupBox1: TGroupBox;
    GB5: TGroupBox;
    L27: TLabel;
    LDepth: TLabel;
    CBShowInLegend: TCheckBox;
    CBCursor: TComboBox;
    EDepth: TEdit;
    UDDepth: TUpDown;
    CBDepth: TCheckBox;
    GB2: TGroupBox;
    L15: TLabel;
    L21: TLabel;
    EPercentFormat: TEdit;
    CBFormat: TComboBox;
    GBHorizAxis: TGroupBox;
    CBHorizAxis: TComboBox;
    CBXDateTime: TCheckBox;
    GBVertAxis: TGroupBox;
    CBVertAxis: TComboBox;
    CBYDateTime: TCheckBox;
    LSort: TLabel;
    CBSort: TComboBox;
    procedure EPercentFormatChange(Sender: TObject);
    procedure PageSeriesChange(Sender: TObject);
    procedure CBSeriesChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGMarkStyleClick(Sender: TObject);
    procedure CBPersistentClick(Sender: TObject);
    procedure CBYDateTimeClick(Sender: TObject);
    procedure CBXDateTimeClick(Sender: TObject);
    procedure CBDataSourcestyleChange(Sender: TObject);
    procedure CBShowInLegendClick(Sender: TObject);
    procedure CBCursorChange(Sender: TObject);
    procedure SEArrowLengthChange(Sender: TObject);
    procedure CBMarkClipClick(Sender: TObject);
    procedure CBMarksVisibleClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SEMarksAngleChange(Sender: TObject);
    procedure CBHorizAxisChange(Sender: TObject);
    procedure CBVertAxisChange(Sender: TObject);
    procedure EMarksEveryChange(Sender: TObject);
    procedure CBMultiLineClick(Sender: TObject);
    procedure CBSortChange(Sender: TObject);
    procedure CBFormatChange(Sender: TObject);
    procedure PageSeriesChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure CBDepthClick(Sender: TObject);
    procedure EDepthChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CBAllVisibleClick(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject);
    procedure PaintBox1Click(Sender: TObject);
  private
    { Private declarations }
    TheSeriesForms  : Array[1..4] of TForm;
    NumSeriesForms  : Integer;
    CreatingForm    : Boolean;
    ChangingPage    : Boolean;
    IMarksObject    : TFormTeeShape;
    LastSeries      : TChartSeries;
    Function AddToTab(AClass:TFormClass; ATab:TTabSheet; AObject:TObject):TForm;
    procedure CreateDataSourceForm;
    procedure EnableMultiLine;
    procedure FillAxes(Items:TStrings; Horizontal:Boolean);
    procedure ProcGetCursors(const S: string);
    procedure SetTabDataSource;
  public
    { Public declarations }
    TheSeries             : TChartSeries;
    IsDssGraph            : Boolean;
    ShowTabDataSource     : Boolean;
    ShowTabGeneral        : Boolean;
    ShowTabMarks          : Boolean;
    DataSourceStyle       : TBaseSourceEditor;
    OnGetDesignerNames    : TOnGetDesignerNamesEvent;
    OnChartGetSourceStyle : TOnChartGetSourceStyle;

    procedure DestroySeriesForms;
    Function InsertSeriesForm( EditorClass:TFormClass;
                               Position:Integer;
                               Const EditorTabName:String;
                               AnObject:TPersistent):TForm;
    Procedure SetCBSeries;
  end;

Type TOnCreateEditSeries=Procedure(Sender:TFormTeeSeries; AChart:TCustomChart) {$IFDEF TEEOCX}of object{$ENDIF};
Var InternalOnCreateEditSeries:TOnCreateEditSeries=nil;

implementation

{$R *.dfm}
Uses TeeConst, TeeProcs, TeeGally, TeeBrushDlg, TeeFuncEdit,
     Series {<-because TPointSeries editor trick} ;

{ TFormTeeSeries}
procedure TFormTeeSeries.EPercentFormatChange(Sender: TObject);
begin
  TheSeries.PercentFormat:=LocalToDelphiFormat(EPercentFormat.Text);
end;

Procedure TFormTeeSeries.SetCBSeries;
begin
  if Assigned(TheSeries) then
  begin
    CBSeries.ItemIndex:=CBSeries.Items.IndexOfObject(TheSeries);
    CBSeriesChange(Self);
  end
  else PageSeries.Visible:=False;
end;

Const TeeCursorPrefix='cr';

Function DeleteCursorPrefix(Const S:String):String;
begin
  result:=S;
  if Copy(result,1,2)=TeeCursorPrefix then Delete(result,1,2);
end;

procedure TFormTeeSeries.ProcGetCursors(const S: string);
begin
  CBCursor.Items.Add(DeleteCursorPrefix(S));
end;

procedure TFormTeeSeries.EnableMultiLine;
begin
  CBMultiLine.Enabled:=TheSeries.Marks.Style in
    [smsLabelPercent,smsLabelValue,smsPercentTotal,smsLabelPercentTotal,smsXY];
end;

procedure TFormTeeSeries.FillAxes(Items:TStrings; Horizontal:Boolean);
var t : Integer;
begin
  With Items do While Count>3 do Delete(Count-1);
  With TheSeries.ParentChart do
  begin
    for t:=5 to Axes.Count-1 do
    if (Axes[t].Horizontal and Horizontal) or
       ((not Axes[t].Horizontal) and (not Horizontal)) then
         Items.AddObject(TeeMsg_CustomAxesEditor+' '+IntToStr(t-5),Axes[t]);
  end;
end;

procedure TFormTeeSeries.PageSeriesChange(Sender: TObject);

  procedure SetTabSeriesMarks;

    Function AllSeriesVisible:Boolean;
    var t : Integer;
    begin
      result:=True;
      With TheSeries.ParentChart do
      for t:=0 to SeriesCount-1 do
      if not Series[t].Marks.Visible then
      begin
        result:=False;
        exit;
      end;
    end;

  begin
    if not Assigned(IMarksObject) then
       IMarksObject:=InsertTeeObjectForm(PageControlMarks,nil);
    With TheSeries.Marks do
    begin
      RGMarkStyle.ItemIndex  :=Ord(Style);
      UDArrowsLength.Position:=ArrowLength;
      UDMarksAngle.Position  :=Angle;
      CBMarkClip.Checked     :=Clip;
      UDEvery.Position       :=DrawEvery;
      CBMultiLine.Checked    :=MultiLine;
      CBMarksVisible.Checked :=Visible;
      BMarkLinCol.LinkPen(Arrow);
    end;
    CBAllVisible.Checked:=AllSeriesVisible;
    EnableMultiLine;
    IMarksObject.RefreshControls(TheSeries.Marks);
  end;

  procedure SetTabSeriesGeneral;
  var tmpSt : String;
  begin
    With TheSeries do
    begin
      CBShowInLegend.Checked   :=ShowInLegend;
      CBDepth.Checked          :=Depth=-1;
      if Depth=-1 then UDDepth.Position:=0
                  else UDDepth.Position:=Depth;

      AddDefaultValueFormats(CBFormat.Items);

      tmpSt:=DelphiToLocalFormat(ValueFormat);
      With CBFormat.Items do if IndexOf(tmpSt)=-1 then Add(tmpSt);
      CBFormat.Text:=tmpSt;

      EPercentFormat.Text      :=DelphiToLocalFormat(PercentFormat);
      FillAxes(CBHorizAxis.Items,True);
      FillAxes(CBVertAxis.Items,False);

      With CBHorizAxis do
      Case TheSeries.HorizAxis of
        aTopAxis:       ItemIndex:=0;
        aBottomAxis:    ItemIndex:=1;
        aBothHorizAxis: ItemIndex:=2;
      else
        ItemIndex:=Items.IndexOfObject(TheSeries.CustomHorizAxis);
      end;

      With CBVertAxis do
      Case TheSeries.VertAxis of
        aLeftAxis:     ItemIndex:=0;
        aRightAxis:    ItemIndex:=1;
        aBothVertAxis: ItemIndex:=2;
      else
        ItemIndex:=Items.IndexOfObject(TheSeries.CustomVertAxis);
      end;

      CBXDateTime.Checked:=XValues.DateTime;
      CBYDateTime.Checked:=YValues.DateTime;

      Case MandatoryValueList.Order of
        loNone       : CBSort.ItemIndex:= 2;
        loAscending  : CBSort.ItemIndex:= 0;
        loDescending : CBSort.ItemIndex:= 1;
      end;
      ShowControls(ValuesList.Count<=2,[CBSort,LSort,LDepth,EDepth,UDDepth,CBDepth]);
    end;
    With CBCursor do
    begin
      Items.BeginUpdate;
      Clear;
      GetCursorValues(ProcGetCursors);
      ProcGetCursors(TeeMsg_TeeHand);
      Items.EndUpdate;
    end;
    With CBCursor do
    if TeeCursorToIdent(TheSeries.Cursor,tmpSt) then
       ItemIndex:=Items.IndexOf(DeleteCursorPrefix(tmpSt))
    else
       ItemIndex:=-1;
  end;

begin
  With PageSeries do
//  if ActivePage=TabGeneral then SetTabSeriesGeneral else
  if ActivePage=TabMarks then SetTabSeriesMarks     else
  if ActivePage=TabDataSource then SetTabDataSource;
  ChangingPage:=False;
end;

procedure TFormTeeSeries.SetTabDataSource;
var tmp : Integer;
begin
  tmp:=CBDataSourceStyle.ItemIndex;
  if Assigned(TheSeries.FunctionType) then
     CBDataSourceStyle.ItemIndex:=2
  else
  if not Assigned(TheSeries.DataSource) then
  begin
    if TheSeries.Count>0 then CBDataSourceStyle.ItemIndex:=1
                         else CBDataSourceStyle.ItemIndex:=0;
  end
  else
  if TheSeries.DataSource is TChartSeries then
     CBDataSourceStyle.ItemIndex:=2
  else
  if Assigned(OnChartGetSourceStyle) then
     With CBDataSourceStyle do
          ItemIndex:=Items.IndexOfObject(TObject(OnChartGetSourceStyle(TheSeries)))
  else
     CBDataSourceStyle.ItemIndex:=0;
  if CBDataSourceStyle.ItemIndex<>tmp then CreateDataSourceForm;
end;

Function TFormTeeSeries.AddToTab(AClass:TFormClass; ATab:TTabSheet; AObject:TObject):TForm;
begin
  result:=AClass.Create(Self);
  Inc(NumSeriesForms);
  TheSeriesForms[NumSeriesForms]:=result;
  AddFormTo(result,ATab,Integer(AObject));
end;

Function TFormTeeSeries.InsertSeriesForm( EditorClass:TFormClass;
                                          Position:Integer;
                                          Const EditorTabName:String;
                                          AnObject:TPersistent):TForm;
var tmpPage  : TTabSheet;
begin
  tmpPage:=TTabSheet.Create(Self);
  With tmpPage do
  begin
    PageControl:=PageSeries;
    PageIndex:=Position;
    Caption:=EditorTabName;
  end;
  result:=AddToTab(EditorClass,tmpPage,AnObject);
end;

procedure TFormTeeSeries.DestroySeriesForms;
var t       : Integer;
    tmpPage : TTabSheet;
begin
  for t:=1 to NumSeriesForms do
  if Assigned(TheSeriesForms[t]) then
  begin
    tmpPage:=(TheSeriesForms[t].Parent as TTabSheet);
    FreeAndNil(TheSeriesForms[t]);
    if tmpPage.PageIndex>0 then
       if not (csDestroying in tmpPage.PageControl.ComponentState) then
          tmpPage.Free;
  end;
  NumSeriesForms:=0;
end;

type TSeriesAccess=class(TCustomChartSeries);
     TChartSeriesAccess=class(TChartSeries);
     
procedure TFormTeeSeries.CBSeriesChange(Sender: TObject);

  Procedure CreateTheSeriesForm;
  var tmpClass : TFormClass;
  begin
    tmpClass:=TFormClass(GetClass(TSeriesAccess(TheSeries).GetEditorClass));
    if Assigned(tmpClass) then AddToTab(tmpClass,TheTabSheet,TheSeries)
    else
    begin
      DestroySeriesForms;
      TheTabSheet.TabVisible:=False;
    end;
  end;

  Procedure HideSeriesPage;
  begin
    ImageSeries.Visible:=False;
    LabelSeriesClass.Caption:='';
    PageSeries.Visible:=False;
  end;

  Function FindTab(Const ATab:String):TTabSheet;
  var t : Integer;
  begin
    with PageSeries do
    begin
      result:=nil;
      for t:=0 to PageCount-1 do
      if Pages[t].Caption=ATab then
      begin
        result:=Pages[t];
        break;
      end;
    end;
  end;

var OldTab    : String;
    tmpBitmap : TBitmap;
begin
  CreatingForm:=True;

  if Assigned(PageSeries.ActivePage) then OldTab:=PageSeries.ActivePage.Caption
                                     else OldTab:='';
  With CBSeries do
  if ItemIndex<>-1 then
  begin
    TheSeries:=TChartSeries(Items.Objects[ItemIndex]);
    if Assigned(TheSeries) then
    begin
      if (TheSeries=LastSeries) then Exit;

      tmpBitmap:=TBitmap.Create;
      try
        TSeriesAccess(TheSeries).GetBitmapEditor(tmpBitmap);
        ImageSeries.Picture.Assign(tmpBitmap);
      finally
        tmpBitmap.Free;
      end;

      PaintBox1.Visible:=TChartSeriesAccess(TheSeries).IUseSeriesColor;

      LabelSeriesClass.Caption:=GetGallerySeriesName(TheSeries)+': '+
                                SeriesTitleOrName(TheSeries);
      ImageSeries.Visible:=True;
      PageSeries.Visible:=True;
//      TabGeneral.TabVisible:=ShowTabGeneral;
      TabMarks.TabVisible:=ShowTabMarks;
      TabDataSource.TabVisible:=ShowTabDataSource and
                                ( (not (tssIsTemplate in TheSeries.Style)) and
                                  (not (tssHideDataSource in TheSeries.Style)) );
      if (OldTab='') or
         ((OldTab=TabDataSource.Caption) and (not TabDataSource.TabVisible)) then
           OldTab:=TheTabSheet.Caption;

      ShowControls(TheSeries.UseAxis,[ GBHorizAxis,GBVertAxis ]);
      CBPersistent.Visible:=IsDssGraph and (not (tssIsTemplate in TheSeries.Style));
      if CBPersistent.Visible then
         CBPersistent.Checked:=(tssIsPersistent in TheSeries.Style);

      {$IFNDEF CLX}
      LockWindowUpdate(PageSeries.Handle);
      {$ENDIF}
      DestroySeriesForms;
      CreateTheSeriesForm;
      {$IFNDEF CLX}
      LockWindowUpdate(0);
      {$ENDIF}
      
      PageSeries.ActivePage:=FindTab(OldTab);
      if (OldTab='') or (PageSeries.ActivePage=nil) then
         PageSeries.ActivePage:=TheTabSheet;

{      if not PageSeries.ActivePage.TabVisible then
         PageSeries.ActivePage:=TabGeneral;}

      CBDataSourceStyle.ItemIndex:=-1;
      PageSeriesChange(Self);

      LastSeries:=TheSeries;
    end
    else HideSeriesPage;
  end
  else HideSeriesPage;

  CreatingForm:=False;
end;

procedure TFormTeeSeries.FormCreate(Sender: TObject);
begin
  ChangingPage:=False;
  CreatingForm:=True;
  NumSeriesForms:=0;
  ShowTabDataSource:=True;
  ShowTabGeneral:=True;
  ShowTabMarks:=True;
  IsDssGraph:=False;
  OnGetDesignerNames:=nil;
  OnChartGetSourceStyle:=nil;
  PageSeries.ActivePage:=TheTabSheet;
end;

procedure TFormTeeSeries.RGMarkStyleClick(Sender: TObject);
begin
  TheSeries.Marks.Style:=TSeriesMarksStyle(RGMarkStyle.ItemIndex);
  EnableMultiLine;
end;

procedure TFormTeeSeries.CBPersistentClick(Sender: TObject);
begin
  With TheSeries do
  if CBPersistent.Checked then Style:=Style+[tssIsPersistent]
                          else Style:=Style-[tssIsPersistent];
end;

procedure TFormTeeSeries.CBYDateTimeClick(Sender: TObject);
begin
  TheSeries.YValues.DateTime:=CBYDateTime.Checked;
end;

procedure TFormTeeSeries.CBXDateTimeClick(Sender: TObject);
begin
  TheSeries.XValues.DateTime:=CBXDateTime.Checked;
end;

procedure TFormTeeSeries.CBDataSourcestyleChange(Sender: TObject);
begin
  if Assigned(DataSourceStyle) then DataSourceStyle.BApply.Enabled:=False;
  With CBDataSourceStyle do
  Case ItemIndex of
     0: begin
          TheSeries.DataSource:=nil;
          TheSeries.Clear;
        end;
     1: With TheSeries do
        begin
          DataSource:=nil;
          SetFunction(nil);
          FillSampleValues(NumSampleValues);
        end;
  end;
  CreateDataSourceForm;
end;

type TBaseSourceEditorClass=class of TBaseSourceEditor;

procedure TFormTeeSeries.CreateDataSourceForm;
begin
  if Assigned(DataSourceStyle) and DataSourceStyle.CloseQuery then
     FreeAndNil(DataSourceStyle);
  if not Assigned(DataSourceStyle) then
  begin
    if CBDataSourceStyle.ItemIndex>1 then
    begin
      With CBDataSourceStyle do
         DataSourceStyle:=TBaseSourceEditorClass(Items.Objects[ItemIndex]).Create(Self);
      DataSourceStyle.Align:=alClient;
      AddFormTo(DataSourceStyle,Panel2,Integer(TheSeries));
    end;
  end;
end;

procedure TFormTeeSeries.CBShowInLegendClick(Sender: TObject);
begin
  TheSeries.ShowInLegend:=CBShowInLegend.Checked;
end;

procedure TFormTeeSeries.CBCursorChange(Sender: TObject);
var tmpCursor:Integer;
begin
  if TeeIdentToCursor(TeeCursorPrefix+
                      CBCursor.Items[CBCursor.ItemIndex],
                      tmpCursor) then
     TheSeries.Cursor:=tmpCursor;
end;

procedure TFormTeeSeries.SEArrowLengthChange(Sender: TObject);
begin
  if (not CreatingForm) and (not ChangingPage) then
     TheSeries.Marks.ArrowLength:=UDArrowsLength.Position;
end;

procedure TFormTeeSeries.CBMarkClipClick(Sender: TObject);
begin
  TheSeries.Marks.Clip:=CBMarkClip.Checked;
end;

procedure TFormTeeSeries.CBMarksVisibleClick(Sender: TObject);
var Old : Boolean;
begin
  TheSeries.Marks.Visible:=CBMarksVisible.Checked;
  Old:=ChangingPage;
  ChangingPage:=True;
  CBAllVisible.Checked:=CBMarksVisible.Checked;
  ChangingPage:=Old;
end;

procedure TFormTeeSeries.FormDestroy(Sender: TObject);
begin
  DestroySeriesForms;
end;

procedure TFormTeeSeries.FormShow(Sender: TObject);
begin
  CreatingForm:=False;
  CBDataSourceStyle.Items.Objects[2]:=TObject(TTeeFuncEditor);
  if Assigned(InternalOnCreateEditSeries) and Assigned(TheSeries) then
     InternalOnCreateEditSeries(Self,TCustomChart(TheSeries.ParentChart));
end;

procedure TFormTeeSeries.SEMarksAngleChange(Sender: TObject);
begin
  if (not CreatingForm) and (not ChangingPage) then
     TheSeries.Marks.Angle:=UDMarksAngle.Position;
end;

procedure TFormTeeSeries.CBHorizAxisChange(Sender: TObject);
begin
  With CBHorizAxis do
  if ItemIndex<3 then
     TheSeries.HorizAxis:=THorizAxis(ItemIndex)
  else
  begin
    TheSeries.HorizAxis:=aCustomHorizAxis;
    TheSeries.CustomHorizAxis:=TChartAxis(Items.Objects[ItemIndex]);
  end;
end;

procedure TFormTeeSeries.CBVertAxisChange(Sender: TObject);
begin
  With CBVertAxis do
  if ItemIndex<3 then
     TheSeries.VertAxis:=TVertAxis(ItemIndex)
  else
  begin
    TheSeries.VertAxis:=aCustomVertAxis;
    TheSeries.CustomVertAxis:=TChartAxis(Items.Objects[ItemIndex]);
  end;
end;

procedure TFormTeeSeries.EMarksEveryChange(Sender: TObject);
begin
  TheSeries.Marks.DrawEvery:=UDEvery.Position;
end;

procedure TFormTeeSeries.CBMultiLineClick(Sender: TObject);
begin
  TheSeries.Marks.MultiLine:=CBMultiLine.Checked;
end;

procedure TFormTeeSeries.CBSortChange(Sender: TObject);
begin
  With TheSeries do
  begin
    NotMandatoryValueList.Order:=loNone;
    if CBSort.ItemIndex=2 then MandatoryValueList.Order:=loNone
    else
    begin
      if CBSort.ItemIndex=0 then MandatoryValueList.Order:=loAscending
                            else MandatoryValueList.Order:=loDescending;
      CheckOrder;
    end;
  end;
end;

procedure TFormTeeSeries.CBFormatChange(Sender: TObject);
begin
  TheSeries.ValueFormat:=LocalToDelphiFormat(CBFormat.Text);
end;

procedure TFormTeeSeries.PageSeriesChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  if Assigned(DataSourceStyle) then
     AllowChange:=DataSourceStyle.CloseQuery
  else
     AllowChange:=True;
  ChangingPage:=AllowChange;
end;

procedure TFormTeeSeries.CBDepthClick(Sender: TObject);
begin
  if CBDepth.Checked then TheSeries.Depth:=-1
                     else TheSeries.Depth:=UDDepth.Position;
end;

procedure TFormTeeSeries.EDepthChange(Sender: TObject);
begin
  if (not CreatingForm) and (not ChangingPage) then
  begin
    TheSeries.Depth:=UDDepth.Position;
    CBDepth.Checked:=False;
  end;
end;

procedure TFormTeeSeries.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if Assigned(DataSourceStyle) then
     CanClose:=DataSourceStyle.CloseQuery
  else
     CanClose:=True;
end;

procedure TFormTeeSeries.CBAllVisibleClick(Sender: TObject);
var t : Integer;
begin
  if (not CreatingForm) and (not ChangingPage) then
  begin
    With TheSeries.ParentChart do
    for t:=0 to SeriesCount-1 do
      Series[t].Marks.Visible:=CBAllVisible.Checked;
    CBMarksVisible.Checked:=CBAllVisible.Checked;
  end;
end;

procedure TFormTeeSeries.PaintBox1Paint(Sender: TObject);
begin
  if Assigned(TheSeries) then
  begin
    PaintBox1.Canvas.FillRect(PaintBox1.ClientRect);
    PaintSeriesLegend(TheSeries,PaintBox1.Canvas,PaintBox1.ClientRect);
  end;
end;

procedure TFormTeeSeries.PaintBox1Click(Sender: TObject);
begin
  if Assigned(TheSeries) then
  begin
    TheSeries.SeriesColor:=EditColor(Self,TheSeries.SeriesColor);
    PaintBox1.Repaint;
  end;
end;

end.

