{******************************************}
{ TCursorTool Editor Dialog                }
{ Copyright (c) 1999-2000 by David Berneda }
{******************************************}
{$I teedefs.inc}
unit TeeColorBandEdit;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Chart, TeeTools, ComCtrls,
  TeCanvas, TeePenDlg, TeeAxisToolEdit;

type
  TColorBandToolEditor = class(TAxisToolEditor)
    BPattern: TButton;
    Label2: TLabel;
    Label3: TLabel;
    EStart: TEdit;
    EEnd: TEdit;
    BGradient: TButton;
    ButtonColor1: TButtonColor;
    Label4: TLabel;
    ETrans: TEdit;
    UDTrans: TUpDown;
    CBDrawBehind: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure BPatternClick(Sender: TObject);
    procedure EStartChange(Sender: TObject);
    procedure EEndChange(Sender: TObject);
    procedure BGradientClick(Sender: TObject);
    procedure ETransChange(Sender: TObject);
    procedure CBDrawBehindClick(Sender: TObject);
  private
    { Private declarations }
    ColorBandTool : TColorBandTool;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeEngine, TeeConst, TeeProco, TeeBrushDlg, TeeEdiAxis, TeeEdiGrad;

procedure TColorBandToolEditor.FormShow(Sender: TObject);
begin
  inherited;
  ColorBandTool:=TColorBandTool(Tag);
  With ColorBandTool do
  begin
    EStart.Text:=FloatToStr(StartValue);
    EEnd.Text:=FloatToStr(EndValue);
    UDTrans.Position:=Transparency;
    CBDrawBehind.Checked:=DrawBehind;
  end;
  ButtonColor1.LinkProperty(ColorBandTool,'Color');
end;

procedure TColorBandToolEditor.BPatternClick(Sender: TObject);
begin
  EditChartBrush(Self,ColorBandTool.Brush);
end;

procedure TColorBandToolEditor.EStartChange(Sender: TObject);
var tmp : Double;
begin
  try
    tmp:=StrToFloat(EStart.Text);
  except
    exit;
  end;
  ColorBandTool.StartValue:=tmp;
end;

procedure TColorBandToolEditor.EEndChange(Sender: TObject);
var tmp : Double;
begin
  try
    tmp:=StrToFloat(EEnd.Text);
  except
    exit;
  end;
  ColorBandTool.EndValue:=tmp;
end;

procedure TColorBandToolEditor.BGradientClick(Sender: TObject);
begin
  EditTeeGradient(Self,ColorBandTool.Gradient);
end;

procedure TColorBandToolEditor.ETransChange(Sender: TObject);
begin
  if Showing then ColorBandTool.Transparency:=UDTrans.Position;
end;

procedure TColorBandToolEditor.CBDrawBehindClick(Sender: TObject);
begin
  ColorBandTool.DrawBehind:=CBDrawBehind.Checked;
end;

initialization
  RegisterClass(TColorBandToolEditor);
end.
