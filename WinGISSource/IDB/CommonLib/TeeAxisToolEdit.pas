{******************************************}
{ TCursorTool Editor Dialog                }
{ Copyright (c) 1999-2000 by David Berneda }
{******************************************}
{$I teedefs.inc}
unit TeeAxisToolEdit;

interface

Uses Windows, Forms, TeePenDlg, TeCanvas, StdCtrls, Controls, Classes, TeeTools;

type
  TAxisToolEditor = class(TForm)
    BPen: TButtonPen;
    CBAxis: TComboBox;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure CBAxisChange(Sender: TObject);
  private
    { Private declarations }
    AxisTool : TTeeCustomToolAxis;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeEngine, TeeEdiAxis;

procedure TAxisToolEditor.FormShow(Sender: TObject);
begin
  AxisTool:=TTeeCustomToolAxis(Tag);
  TeeAddAxes(AxisTool.ParentChart,CBAxis.Items,False);
  With AxisTool do
  begin
    BPen.LinkPen(Pen);
    With ParentChart,CBAxis do
    if Axis=LeftAxis then ItemIndex:=0 else
    if Axis=RightAxis then ItemIndex:=1 else
    if Axis=TopAxis then ItemIndex:=2 else
    if Axis=BottomAxis then ItemIndex:=3 else
       ItemIndex:=Axes.IndexOf(Axis)-1;
  end;
end;

procedure TAxisToolEditor.CBAxisChange(Sender: TObject);
begin
  With AxisTool do
  Case CBAxis.ItemIndex of
    -1: Axis:=nil;
     0: Axis:=ParentChart.LeftAxis;
     1: Axis:=ParentChart.RightAxis;
     2: Axis:=ParentChart.TopAxis;
     3: Axis:=ParentChart.BottomAxis;
  else
    Axis:=ParentChart.Axes[CBAxis.ItemIndex+1];
  end;
end;

initialization
  RegisterClass(TAxisToolEditor);
end.           
