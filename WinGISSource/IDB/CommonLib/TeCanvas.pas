{**********************************************}
{   TeeChart and TeeTree TCanvas3D component   }
{   Copyright (c) 1999-2000 by David Berneda   }
{        All Rights Reserved                   }
{**********************************************}
{$I teedefs.inc}
unit TeCanvas;

interface

uses{$IFDEF LINUX}
  LibC,
  {$ELSE}
  {$IFNDEF CLX}
  Windows, Messages,
  {$ENDIF}
  {$ENDIF}
  Classes, SysUtils,
  {$IFDEF CLX}
  Qt, QGraphics, QStdCtrls, QControls, QDialogs,
  {$ELSE}
  Controls, Graphics, StdCtrls, Dialogs,
  {$ENDIF}
  TypInfo
  {$IFDEF D6}
  , Types
  {$ENDIF}
  {++ Moskaliov Business Graphics BUILD#150 17.01.01}
  , Objects
  {-- Moskaliov Business Graphics BUILD#150 17.01.01}
  ;

const
  TeePiStep         : Double = Pi / 180.0;
  TeeDefaultPerspective = 15;
  TeeDefaultConePercent: Integer = 10;
  TeeMinAngle       = 270;

  {$IFNDEF D6}
  clMoneyGreen      = TColor($C0DCC0);
  clSkyBlue         = TColor($F0CAA6);
  clCream           = TColor($F0FBFF);
  clMedGray         = TColor($A4A0A0);
  {$ENDIF}

  {$IFDEF CLX}
  clMoneyGreen      = TColor($C0DCC0);
  clSkyBlue         = TColor($F0CAA6);
  clCream           = TColor($F0FBFF);
  clMedGray         = TColor($A4A0A0);

  TA_LEFT           = 1;
  TA_CENTER         = 2;
  TA_RIGHT          = 4;
  {$ENDIF}

type
  {$IFDEF TEEPENEND}
  TPenEndStyle = (esRound, esSquare, esFlat);
  {$ENDIF}

  TChartPen = class(TPen)
  private
    {$IFDEF TEEPENEND}
    FEndStyle: TPenEndStyle;
    {$ENDIF}
    FSmallDots: Boolean;
    FVisible: Boolean;
    {$IFDEF TEEPENEND}
    procedure SetEndStyle(const Value: TPenEndStyle);
    {$ENDIF}
    procedure SetSmallDots(Value: Boolean);
    procedure SetVisible(Value: Boolean);
  public
    constructor Create(OnChangeEvent: TNotifyEvent);
    {++ Moskaliov Business Graphics BUILD#150 17.01.01}
    constructor Load(var S: TOldStream; OnChangeEvent: TNotifyEvent);
    procedure Store(var S: TOldStream);
    function OnCompareItems(Item: TPersistent): Boolean;
    function CreateDeterminant(Determinant: string): string;
    {-- Moskaliov Business Graphics BUILD#150 17.01.01}
    procedure Assign(Source: TPersistent); override;
  published
    {$IFDEF TEEPENEND}
    property EndStyle: TPenEndStyle read FEndStyle write SetEndStyle default esRound;
    {$ENDIF}
    property SmallDots: Boolean read FSmallDots write SetSmallDots default False;
    property Visible: Boolean read FVisible write SetVisible default True;
  end;

  TChartHiddenPen = class(TChartPen)
  public
    constructor Create(OnChangeEvent: TNotifyEvent);
    {++ Moskaliov Business Graphics BUILD#150 17.01.01}
    //     Constructor Load(var S: TOldStream; OnChangeEvent: TNotifyEvent);
    //     Procedure   Store(var S: TOldStream);
    {-- Moskaliov Business Graphics BUILD#150 17.01.01}
  published
    property Visible default False;
  end;

  TDottedGrayPen = class(TChartPen)
  public
    constructor Create(OnChangeEvent: TNotifyEvent);
  published
    property Color default clGray;
    property Style default psDot;
  end;

  TDarkGrayPen = class(TChartPen)
  public
    constructor Create(OnChangeEvent: TNotifyEvent);
    {++ Moskaliov Business Graphics BUILD#150 17.01.01}
    //     Constructor Load(var S: TOldStream; OnChangeEvent: TNotifyEvent);
    //     Procedure   Store(var S: TOldStream);
    {-- Moskaliov Business Graphics BUILD#150 17.01.01}
  published
    property Color default clDkGray;
  end;

  TChartArrowPen = class(TChartPen)
  public
    constructor Create(OnChangeEvent: TNotifyEvent);
  published
    property Color default clWhite;
  end;

  TChartAxisPen = class(TChartPen)
  public
    constructor Create(OnChangeEvent: TNotifyEvent);
  published
    property Width default 2;
  end;

  TChartBrush = class(TBrush)
  private
    FImage: TPicture;
    procedure SetImage(Value: TPicture);
  public
    constructor Create(OnChangeEvent: TNotifyEvent);
    destructor Destroy; override;
    {++ Moskaliov Business Graphics BUILD#150 17.01.01}
    constructor Load(var S: TOldStream; OnChangeEvent: TNotifyEvent);
    procedure Store(var S: TOldStream);
    function OnCompareItems(Item: TPersistent): Boolean;
    {-- Moskaliov Business Graphics BUILD#150 17.01.01}
    procedure Assign(Source: TPersistent); override;
  published
    property Color default clDefault;
    property Image: TPicture read FImage write SetImage;
  end;

  TTeeView3DScrolled = procedure(IsHoriz: Boolean) of object;
  TTeeView3DChangedZoom = procedure(NewZoom: Integer) of object;

  TView3DOptions = class(TPersistent)
  private
    FElevation: Integer;
    FHorizOffset: Integer;
    FOrthogonal: Boolean;
    FOrthoAngle: Integer;
    FPerspective: Integer;
    FRotation: Integer;
    FTilt: Integer;
    FVertOffset: Integer;
    FZoom: Integer;
    FZoomText: Boolean;
    FOnScrolled: TTeeView3DScrolled;
    FOnChangedZoom: TTeeView3DChangedZoom;

    FParent: TWinControl;
    procedure SetElevation(Value: Integer);
    procedure SetPerspective(Value: Integer);
    procedure SetRotation(Value: Integer);
    procedure SetTilt(Value: Integer);
    procedure SetHorizOffset(Value: Integer);
    procedure SetVertOffset(Value: Integer);
    procedure SetOrthoAngle(Value: Integer);
    procedure SetOrthogonal(Value: Boolean);
    procedure SetZoom(Value: Integer);
    procedure SetZoomText(Value: Boolean);
    procedure SetBooleanProperty(var Variable: Boolean; Value: Boolean);
    procedure SetIntegerProperty(var Variable: Integer; Value: Integer);
  public
    constructor Create(AParent: TWinControl);
    {++ Moskaliov Business Graphics BUILD#150 17.01.01}
    constructor Load(var S: TOldStream; AParent: TWinControl);
    procedure Store(var S: TOldStream);
    function OnCompareItems(Item: TPersistent): Boolean;
    {-- Moskaliov Business Graphics BUILD#150 17.01.01}
    procedure Repaint;
    procedure Assign(Source: TPersistent); override;
    property Parent: TWinControl read FParent write FParent;
    property OnChangedZoom: TTeeView3DChangedZoom read FOnChangedZoom
      write FOnChangedZoom;
    property OnScrolled: TTeeView3DScrolled read FOnScrolled write FOnScrolled;
  published
    property Elevation: Integer read FElevation write SetElevation default 345;
    property HorizOffset: Integer read FHorizOffset write SetHorizOffset default 0;
    property OrthoAngle: Integer read FOrthoAngle write SetOrthoAngle default 45;
    property Orthogonal: Boolean read FOrthogonal write SetOrthogonal default True;
    property Perspective: Integer read FPerspective
      write SetPerspective default TeeDefaultPerspective;
    property Rotation: Integer read FRotation write SetRotation default 345;
    property Tilt: Integer read FTilt write SetTilt default 0;
    property VertOffset: Integer read FVertOffset write SetVertOffset default 0;
    property Zoom: Integer read FZoom write SetZoom default 100;
    property ZoomText: Boolean read FZoomText write SetZoomText default True;
  end;

  TTeeShadow = class(TPersistent)
  private
    FColor: TColor;
    FHorizSize: Integer;
    FVertSize: Integer;

    IOnChange: TNotifyEvent;
    procedure SetColor(Value: TColor);
    procedure SetHorizSize(Value: Integer);
    procedure SetIntegerProperty(var Variable: Integer; const Value: Integer);
    procedure SetVertSize(Value: Integer);
  public
    constructor Create(AOnChange: TNotifyEvent);
    {++ Moskaliov Business Graphics BUILD#150 17.01.01}
    constructor Load(var S: TOldStream; AOnChange: TNotifyEvent);
    procedure Store(var S: TOldStream);
    function OnCompareItems(Item: TPersistent): Boolean;
    function CreateDeterminant(Determinant: string): string;
    {-- Moskaliov Business Graphics BUILD#150 17.01.01}
    procedure Assign(Source: TPersistent); override;
  published
    property Color: TColor read FColor write SetColor default clSilver;
    property HorizSize: Integer read FHorizSize write SetHorizSize default 0;
    property VertSize: Integer read FVertSize write SetVertSize default 0;
  end;

  TTeeCanvas = class;

  TTeeFont = class(TFont)
  private
    FInterCharSize: Integer;
    FOutLine: TChartHiddenPen;
    FShadow: TTeeShadow;

    ICanvas: TTeeCanvas;
    function IsColorStored: Boolean;
    function IsHeightStored: Boolean;
    function IsNameStored: Boolean;
    function IsStyleStored: Boolean;
    procedure SetInterCharSize(Value: Integer);
    procedure SetOutLine(Value: TChartHiddenPen);
    procedure SetShadow(Value: TTeeShadow);
  protected
    IDefColor: TColor;
    IDefStyle: TFontStyles;
  public
    constructor Create(ChangedEvent: TNotifyEvent);
    destructor Destroy; override;
    {++ Moskaliov Business Graphics BUILD#150 17.01.01}
    constructor Load(var S: TOldStream; ChangedEvent: TNotifyEvent);
    procedure Store(var S: TOldStream);
    function OnCompareItems(Item: TPersistent): Boolean;
    function CreateDeterminant(Determinant: string): string;
    {-- Moskaliov Business Graphics BUILD#150 17.01.01}
    procedure Assign(Source: TPersistent); override;
  published
    property Charset default DEFAULT_CHARSET;
    property Color stored IsColorStored;
    property Height stored IsHeightStored;
    property InterCharSize: Integer read FInterCharSize
      write SetInterCharSize default 0;
    property Name stored IsNameStored;
    property OutLine: TChartHiddenPen read FOutLine write SetOutLine;
    property Shadow: TTeeShadow read FShadow write SetShadow;
    property Style stored IsStyleStored;
  end;

  TCanvasBackMode = (cbmNone, cbmTransparent, cbmOpaque);
  TCanvasTextAlign = Integer;
  TGradientDirection = (gdTopBottom, gdBottomTop,
    gdLeftRight, gdRightLeft,
    gdFromCenter, gdFromTopLeft, gdFromBottomLeft);

  TTeeCanvasHandle = {$IFDEF CLX}QPainterH{$ELSE}HDC{$ENDIF};

  TTeeCanvas = class
  private
    FCanvas: TCanvas;
    FFont: TFont;
    FPen: TPen;
    FBrush: TBrush;
    FMetafiling: Boolean;
    FTextAlign: TCanvasTextAlign;

    IFont: TTeeFont;
  protected
    procedure SetCanvas(ACanvas: TCanvas);

    function GetBackColor: TColor; virtual;
    function GetBackMode: TCanvasBackMode; virtual;
    function GetHandle: TTeeCanvasHandle; virtual; abstract;
    function GetMonochrome: Boolean; virtual; abstract;
    function GetTextAlign: TCanvasTextAlign; virtual; abstract;
    function GetUseBuffer: Boolean; virtual; abstract;
    procedure SetBackColor(Color: TColor); virtual;
    procedure SetBackMode(Mode: TCanvasBackMode); virtual;
    procedure SetMonochrome(Value: Boolean); virtual; abstract;
    procedure SetPixel(X, Y: Integer; Value: TColor); virtual; abstract;
    procedure SetTextAlign(Align: TCanvasTextAlign); virtual; abstract;
    procedure SetUseBuffer(Value: Boolean); virtual; abstract;
  public
    procedure AssignBrush(ABrush: TChartBrush; ABackColor: TColor);
    procedure AssignBrushColor(ABrush: TChartBrush; AColor, ABackColor: TColor);
    procedure AssignVisiblePen(APen: TPen);
    procedure AssignVisiblePenColor(APen: TPen; AColor: TColor);
    procedure AssignFont(AFont: TTeeFont);
    procedure ResetState;

    { 2d }
    procedure Arc(X1, Y1, X2, Y2, X3, Y3, X4, Y4: Integer); virtual; abstract;
    procedure Donut(XCenter, YCenter, XRadius, YRadius: Integer;
      const StartAngle, EndAngle, HolePercent: Double); virtual; abstract;
    procedure Draw(X, Y: Integer; Graphic: TGraphic); virtual; abstract;
    procedure Ellipse(X1, Y1, X2, Y2: Integer); virtual; abstract;
    procedure FillRect(const Rect: TRect); virtual; abstract;
    procedure Frame3D(Rect: TRect; TopColor, BottomColor: TColor;
      Width: Integer); virtual; abstract;
    procedure LineTo(X, Y: Integer); virtual; abstract;
    procedure MoveTo(X, Y: Integer); virtual; abstract;
    procedure Pie(X1, Y1, X2, Y2, X3, Y3, X4, Y4: Integer); virtual; abstract;
    procedure Rectangle(X0, Y0, X1, Y1: Integer); virtual; abstract;
    procedure RoundRect(X1, Y1, X2, Y2, X3, Y3: Integer); virtual; abstract;
    procedure StretchDraw(const Rect: TRect; Graphic: TGraphic); virtual; abstract;
    procedure TextOut(X, Y: Integer; const Text: string); virtual; abstract;
    function TextWidth(const St: string): Integer; virtual;
    function TextHeight(const St: string): Integer; virtual;
    function FontHeight: Integer;

    { 2d extra }
    procedure ClipRectangle(const Rect: TRect); virtual; abstract;
    procedure DoHorizLine(X0, X1, Y: Integer); virtual; abstract;
    procedure DoRectangle(const Rect: TRect); virtual; abstract;
    procedure DoVertLine(X, Y0, Y1: Integer); virtual; abstract;
    procedure EraseBackground(const Rect: TRect); virtual; abstract;
    procedure GradientFill(const Rect: TRect;
      StartColor, EndColor: TColor;
      Direction: TGradientDirection); virtual; abstract;
    procedure Invalidate; virtual; abstract;
    procedure Line(X0, Y0, X1, Y1: Integer); virtual; abstract;
    procedure Polygon(const Points: array of TPoint); virtual; abstract;
    procedure RotateLabel(x, y: Integer; const St: string;
      RotDegree: Integer); virtual; abstract;
    procedure UnClipRectangle; virtual; abstract;

    { properties }
    property BackColor: TColor read GetBackColor write SetBackColor;
    property BackMode: TCanvasBackMode read GetBackMode write SetBackMode;
    property Brush: TBrush read FBrush;
    property Font: TFont read FFont;
    {++ Moskaliov Business Graphics BUILD#150 17.01.01}
    property Handle: TTeeCanvasHandle read GetHandle;
    {-- Moskaliov Business Graphics BUILD#150 17.01.01}
    property Metafiling: Boolean read FMetafiling write FMetafiling;
    property Monochrome: Boolean read GetMonochrome write SetMonochrome;
    property Pen: TPen read FPen;
    property Pixels[X, Y: Integer]: TColor write SetPixel;
    property ReferenceCanvas: TCanvas read FCanvas write SetCanvas;
    property TextAlign: TCanvasTextAlign read GetTextAlign write SetTextAlign;
    property UseBuffer: Boolean read GetUseBuffer write SetUseBuffer;
  end;

  { 3d }
  TPoint3DFloat = packed record
    X: Double;
    Y: Double;
    Z: Double;
  end;

  TPoint3D = packed record x, y, z: Integer;
  end;
  TFourPoints = array[0..3] of TPoint;
  TTrianglePoints3D = array[0..2] of TPoint3D;
  TTriangleColors3D = array[0..2] of TColor;

  TTeeCanvasCalcPoints = function(x, z: Integer; var P0, P1: TPoint3D;
    var Color0, Color1: TColor): Boolean of object;

  TTeeCanvasSurfaceStyle = (tcsSolid, tcsWire, tcsDot);

  TCanvas3D = class(TTeeCanvas)
  private
    F3DOptions: TView3DOptions;
    FIsOrthogonal: Boolean;
  protected
    function GetSupportsFullRotation: Boolean; virtual; abstract;
    function GetSupports3DText: Boolean; virtual; abstract;
    procedure SetPixel3D(X, Y, Z: Integer; Value: TColor); virtual; abstract;
  public
    { 3d }
    RotationCenter: TPoint3DFloat;
    function CalcRect3D(const R: TRect; Z: Integer): TRect;
    procedure Calculate2DPosition(var x, y: Integer; z: Integer); virtual; abstract;
    function Calculate3DPosition(x, y, z: Integer): TPoint; virtual; abstract;

    function InitWindow(DestCanvas: TCanvas; A3DOptions: TView3DOptions;
      ABackColor: TColor;
      Is3D: Boolean;
      const UserRect: TRect): TRect; virtual; abstract;

    procedure Assign(Source: TCanvas3D); virtual;

    procedure Projection(MaxDepth: Integer; const Bounds, Rect: TRect); virtual; abstract;
    procedure ShowImage(DestCanvas, DefaultCanvas: TCanvas;
      const UserRect: TRect); virtual; abstract;
    {++ Moskaliov Business Graphics BUILD#150 17.01.01}
    procedure DrawImage(ABitmapDC: HDC; const UserRect: TRect); virtual; abstract;
    {-- Moskaliov Business Graphics BUILD#150 17.01.01}
    function ReDrawBitmap: Boolean; virtual; abstract;

    procedure Arrow(Filled: Boolean; const FromPoint, ToPoint: TPoint;
      ArrowWidth, ArrowHeight, Z: Integer); virtual; abstract;
    procedure ClipCube(const Rect: TRect; MinZ, MaxZ: Integer); virtual; abstract;
    procedure Cone(Vertical: Boolean; Left, Top, Right, Bottom, Z0, Z1: Integer;
      Dark3D: Boolean; ConePercent: Integer); virtual; abstract;
    procedure Cube(Left, Right, Top, Bottom, Z0, Z1: Integer;
      DarkSides: Boolean); virtual; abstract;
    procedure Cylinder(Vertical: Boolean; Left, Top, Right, Bottom, Z0, Z1: Integer;
      Dark3D: Boolean); virtual; abstract;
    procedure HorizLine3D(Left, Right, Y, Z: Integer); virtual; abstract;
    procedure VertLine3D(X, Top, Bottom, Z: Integer); virtual; abstract;
    procedure ZLine3D(X, Y, Z0, Z1: Integer); virtual; abstract;
    procedure EllipseWithZ(X1, Y1, X2, Y2, Z: Integer); virtual; abstract;
    procedure FrontPlaneBegin; virtual; abstract;
    procedure FrontPlaneEnd; virtual; abstract;
    procedure LineWithZ(X0, Y0, X1, Y1, Z: Integer); virtual; abstract;
    procedure MoveTo3D(X, Y, Z: Integer); virtual; abstract;
    procedure LineTo3D(X, Y, Z: Integer); virtual; abstract;
    procedure Pie3D(XCenter, YCenter, XRadius, YRadius, Z0, Z1: Integer;
      const StartAngle, EndAngle: Double;
      DarkSides, DrawSides: Boolean); virtual; abstract;
    procedure Plane3D(const A, B: TPoint; Z0, Z1: Integer); virtual; abstract;
    procedure PlaneWithZ(P1, P2, P3, P4: TPoint; Z: Integer); virtual; abstract;
    procedure PlaneFour3D(var Points: TFourPoints; Z0, Z1: Integer); virtual; abstract;
    procedure PolygonWithZ(Points: array of TPoint; Z: Integer); virtual; abstract;
    procedure Pyramid(Vertical: Boolean; Left, Top, Right, Bottom, z0, z1: Integer;
      DarkSides: Boolean); virtual; abstract;
    procedure PyramidTrunc(const R: TRect; StartZ, EndZ: Integer;
      TruncX, TruncZ: Integer); virtual; abstract;
    procedure RectangleWithZ(const Rect: TRect; Z: Integer); virtual; abstract;
    procedure RectangleY(Left, Top, Right, Z0, Z1: Integer); virtual; abstract;
    procedure RectangleZ(Left, Top, Bottom, Z0, Z1: Integer); virtual; abstract;
    procedure RotateLabel3D(x, y, z: Integer; const St: string;
      RotDegree: Integer); virtual; abstract;
    procedure Sphere(x, y, z: Integer; const Radius: Double); virtual; abstract;
    procedure Surface3D(Style: TTeeCanvasSurfaceStyle;
      SameBrush: Boolean; NumXValues, NumZValues: Integer;
      CalcPoints: TTeeCanvasCalcPoints); virtual; abstract;
    procedure TextOut3D(x, y, z: Integer; const Text: string); virtual; abstract;
    procedure Triangle3D(const Points: TTrianglePoints3D;
      const Colors: TTriangleColors3D); virtual; abstract;
    procedure TriangleWithZ(const P1, P2, P3: TPoint; Z: Integer); virtual; abstract;

    property Pixels3D[X, Y, Z: Integer]: TColor write SetPixel3D;
    property Supports3DText: Boolean read GetSupports3DText;
    property SupportsFullRotation: Boolean read GetSupportsFullRotation;
    property View3DOptions: TView3DOptions read F3DOptions write F3DOptions;
  end;

  TTeeCanvas3D = class(TCanvas3D)
  private
    FXCenter: Integer;
    FYCenter: Integer;
    FZCenter: Integer;
    FXCenterOffset: Integer;
    FYCenterOffset: Integer;

    s2: Extended;
    c2s1: Double;
    c2s3: Double;
    c2c3: Double;
    c2c1: Double;
    tempXX: Double;
    tempYX: Double;
    tempXZ: Double;
    tempYZ: Double;

    FWas3D: Boolean;
    FIs3D: Boolean;

    FBitmap: TBitmap;

    FBufferedDisplay: Boolean;
    FMonochrome: Boolean;
    FDirty: Boolean;

    FBounds: TRect;

    IOrthoY: Double;
    IPerspec: Double;
    IZoomFactor: Double;
    IZoomText: Boolean;

    IPoints: TFourPoints;

    procedure DeleteBitmap;
    procedure InternalCylinder(Vertical: Boolean; Left, Top, Right, Bottom,
      Z0, Z1: Integer; Dark3D: Boolean; ConePercent: Integer);
    procedure PolygonFour;
    procedure TransferBitmap(ALeft, ATop: Integer; ACanvas: TCanvas);
  protected
    { 2d }
    function GetHandle: TTeeCanvasHandle; override;
    function GetMonochrome: Boolean; override;
    function GetSupports3DText: Boolean; override;
    function GetSupportsFullRotation: Boolean; override;
    function GetTextAlign: TCanvasTextAlign; override;
    function GetUseBuffer: Boolean; override;

    procedure SetMonochrome(Value: Boolean); override;
    procedure SetPixel(X, Y: Integer; Value: TColor); override;
    procedure SetTextAlign(Align: TCanvasTextAlign); override;
    procedure SetUseBuffer(Value: Boolean); override;

    { 3d private }
    procedure Calc3DTPoint(var P: TPoint; z: Integer);
    function Calc3DTPoint3D(const P: TPoint3D): TPoint;
    procedure Calc3DPoint(var P: TPoint; x, y, z: Integer);

    { 3d }
    procedure SetPixel3D(X, Y, Z: Integer; Value: TColor); override;
    procedure Calc3DPos(var x, y: Integer; z: Integer);
  public
    { almost public... }
    procedure Calculate2DPosition(var x, y: Integer; z: Integer); override;
    function Calculate3DPosition(x, y, z: Integer): TPoint; override;

    { public }
    constructor Create;
    destructor Destroy; override;

    function InitWindow(DestCanvas: TCanvas;
      A3DOptions: TView3DOptions;
      ABackColor: TColor;
      Is3D: Boolean;
      const UserRect: TRect): TRect; override;
    function ReDrawBitmap: Boolean; override;
    procedure ShowImage(DestCanvas, DefaultCanvas: TCanvas; const UserRect: TRect); override;
    {++ Moskaliov Business Graphics BUILD#150 17.01.01}
    procedure DrawImage(ABitmapDC: HDC; const UserRect: TRect); override;
    {-- Moskaliov Business Graphics BUILD#150 17.01.01}

    procedure Arc(X1, Y1, X2, Y2, X3, Y3, X4, Y4: Integer); override;
    procedure Donut(XCenter, YCenter, XRadius, YRadius: Integer;
      const StartAngle, EndAngle, HolePercent: Double); override;
    procedure Draw(X, Y: Integer; Graphic: TGraphic); override;
    procedure Ellipse(X1, Y1, X2, Y2: Integer); override;
    procedure EraseBackground(const Rect: TRect); override;
    procedure FillRect(const Rect: TRect); override;
    procedure Frame3D(Rect: TRect; TopColor, BottomColor: TColor;
      Width: Integer); override;
    procedure LineTo(X, Y: Integer); override;
    procedure MoveTo(X, Y: Integer); override;
    procedure Pie(X1, Y1, X2, Y2, X3, Y3, X4, Y4: Integer); override;
    procedure Rectangle(X0, Y0, X1, Y1: Integer); override;
    procedure RoundRect(X1, Y1, X2, Y2, X3, Y3: Integer); override;
    procedure StretchDraw(const Rect: TRect; Graphic: TGraphic); override;
    procedure TextOut(X, Y: Integer; const Text: string); override;

    { 2d extra }
    procedure ClipRectangle(const Rect: TRect); override;
    procedure ClipCube(const Rect: TRect; MinZ, MaxZ: Integer); override;
    procedure DoRectangle(const Rect: TRect); override;
    procedure DoHorizLine(X0, X1, Y: Integer); override;
    procedure DoVertLine(X, Y0, Y1: Integer); override;
    procedure GradientFill(const Rect: TRect;
      StartColor, EndColor: TColor;
      Direction: TGradientDirection); override;
    procedure Invalidate; override;
    procedure Line(X0, Y0, X1, Y1: Integer); override;
    procedure Polygon(const Points: array of TPoint); override;
    procedure RotateLabel(x, y: Integer; const St: string; RotDegree: Integer); override;
    procedure RotateLabel3D(x, y, z: Integer;
      const St: string; RotDegree: Integer); override;
    procedure UnClipRectangle; override;

    property XCenter: Integer read FXCenter write FXCenter;
    property YCenter: Integer read FYCenter write FYCenter;
    property ZCenter: Integer read FZCenter write FZCenter;

    { 3d }
    procedure Projection(MaxDepth: Integer; const Bounds, Rect: TRect); override;

    procedure Arrow(Filled: Boolean; const FromPoint, ToPoint: TPoint;
      ArrowWidth, ArrowHeight, Z: Integer); override;
    procedure Cone(Vertical: Boolean; Left, Top, Right, Bottom, Z0, Z1: Integer;
      Dark3D: Boolean; ConePercent: Integer); override;
    procedure Cube(Left, Right, Top, Bottom, Z0, Z1: Integer; DarkSides: Boolean); override;
    procedure Cylinder(Vertical: Boolean; Left, Top, Right, Bottom, Z0, Z1: Integer;
      Dark3D: Boolean); override;
    procedure EllipseWithZ(X1, Y1, X2, Y2, Z: Integer); override;
    procedure RectangleZ(Left, Top, Bottom, Z0, Z1: Integer); override;
    procedure RectangleY(Left, Top, Right, Z0, Z1: Integer); override;
    procedure FrontPlaneBegin; override;
    procedure FrontPlaneEnd; override;
    procedure HorizLine3D(Left, Right, Y, Z: Integer); override;
    procedure LineTo3D(X, Y, Z: Integer); override;
    procedure LineWithZ(X0, Y0, X1, Y1, Z: Integer); override;
    procedure MoveTo3D(X, Y, Z: Integer); override;
    procedure Pie3D(XCenter, YCenter, XRadius, YRadius, Z0, Z1: Integer;
      const StartAngle, EndAngle: Double;
      DarkSides, DrawSides: Boolean); override;
    procedure Plane3D(const A, B: TPoint; Z0, Z1: Integer); override;
    procedure PlaneWithZ(P1, P2, P3, P4: TPoint; Z: Integer); override;
    procedure PlaneFour3D(var Points: TFourPoints; Z0, Z1: Integer); override;
    procedure PolygonWithZ(Points: array of TPoint; Z: Integer); override;
    procedure Pyramid(Vertical: Boolean; Left, Top, Right, Bottom, z0, z1: Integer;
      DarkSides: Boolean); override;
    procedure PyramidTrunc(const R: TRect; StartZ, EndZ: Integer;
      TruncX, TruncZ: Integer); override;
    procedure RectangleWithZ(const Rect: TRect; Z: Integer); override;
    procedure Sphere(x, y, z: Integer; const Radius: Double); override;
    procedure Surface3D(Style: TTeeCanvasSurfaceStyle;
      SameBrush: Boolean;
      NumXValues, NumZValues: Integer;
      CalcPoints: TTeeCanvasCalcPoints); override;
    procedure TextOut3D(X, Y, Z: Integer; const Text: string); override;
    procedure Triangle3D(const Points: TTrianglePoints3D;
      const Colors: TTriangleColors3D); override;
    procedure TriangleWithZ(const P1, P2, P3: TPoint; Z: Integer); override;
    procedure VertLine3D(X, Top, Bottom, Z: Integer); override;
    procedure ZLine3D(X, Y, Z0, Z1: Integer); override;

    property Bitmap: TBitmap read FBitmap;
    property Bounds: TRect read FBounds;
  end;

  TTeeTransparency = 0..100;

  TTeeBlend = class
  private
    FBitmap: TBitmap;
    FCanvas: TTeeCanvas3D;
    FRect: TRect;
  public
    constructor Create(ACanvas: TTeeCanvas3D; const R: TRect);
    destructor Destroy; override;

    procedure DoBlend(Transparency: TTeeTransparency);
  end;

function ApplyDark(Color: TColor; HowMuch: Byte): TColor;
function ApplyBright(Color: TColor; HowMuch: Byte): TColor;

function Point3D(x, y, z: Integer): TPoint3D;

procedure SinCos(const Angle: Extended; var ResultSin, ResultCos: Extended);
function ArcTan2(Y, X: Extended): Extended;

procedure SwapDouble(var a, b: Double); { exchanges a and b }
procedure SwapInteger(var a, b: Integer); { exchanges a and b }

function MaxDouble(const a, b: Double): Double; { returns max (a or b) }
function MinDouble(const a, b: Double): Double; { returns min (a or b) }

function MaxLong(a, b: Integer): Integer; { returns max (a or b) }
function MinLong(a, b: Integer): Integer; { returns min (a or b) }

procedure RectSize(const R: TRect; var RectWidth, RectHeight: Integer);
procedure RectCenter(const R: TRect; var X, Y: Integer);
procedure ClipCanvas(ACanvas: TCanvas; const Rect: TRect);
procedure UnClipCanvas(ACanvas: TCanvas);
procedure ClipRoundRectangle(ACanvas: TTeeCanvas; const Rect: TRect; RoundSize: Integer);
procedure ClipPolygon(ACanvas: TTeeCanvas; var Points: array of TPoint; NumPoints: Integer);

const
  TeeCharForHeight  = 'W';              { <-- this is used to calculate Text Height }
  DarkerColorQuantity: Byte = 128;      { <-- for dark 3D sides }
  DarkColorQuantity : Byte = 64;

type
  TButtonGetColorProc = function: TColor of object;

  TTeeButton = class(TButton)
  private
    {$IFNDEF CLX}
    procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
    {$ENDIF}
  protected
    Instance: TObject;
    Info: PPropInfo;
    procedure DrawSymbol(Canvas: TCanvas); virtual; abstract;
    {$IFNDEF CLX}
    procedure PaintWindow(DC: HDC); override;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    {$ELSE}
    procedure Painting(Sender: QObjectH; EventRegion: QRegionH); override;
    {$ENDIF}
  public
    procedure LinkProperty(AInstance: TObject; const PropName: string);
  published
    { Published declarations }
    property Height default 25;
    property Width default 75;
  end;

  TButtonColor = class(TTeeButton)
  private
    { Private declarations }
    function GetTeeColor: TColor;
  protected
    { Protected declarations }
    procedure DrawSymbol(Canvas: TCanvas); override;
  public
    { Public declarations }
    GetColorProc: TButtonGetColorProc;
    procedure Click; override;
  end;

function EditColor(AOwner: TComponent; AColor: TColor): TColor;

{$IFNDEF D5}
procedure FreeAndNil(var Obj);
{$ENDIF}

function GetDefaultFontSize: Integer;
function GetDefaultFontName: string;

function IsWindowsNT: Boolean;

var
  TeeCustomEditColors: TStrings = nil;  { for the color editor dialog }

  {$IFDEF CLX}
function GetRValue(Color: Integer): Byte;
function GetGValue(Color: Integer): Byte;
function GetBValue(Color: Integer): Byte;
function RGB(r, g, b: Integer): TColor;
{$ENDIF}

implementation

uses TeeConst, {$IFDEF CLX}QForms{$ELSE}Forms{$ENDIF};

type
  PPoints = ^TPoints;
  TPoints = array[0..0] of TPoint;

var
  OldRegion         : {$IFNDEF CLX}HRgn = 0{$ELSE}QRegionH = nil{$ENDIF};
  WasOldRegion      : Boolean = False;

procedure SinCos(const Angle: Extended; var ResultSin, ResultCos: Extended);
asm
  FLD     Angle
  FSINCOS
  FSTP    tbyte ptr [edx]    { Cos }
  FSTP    tbyte ptr [eax]    { Sin }
  FWAIT
end;

function ArcTan2(Y, X: Extended): Extended;
asm
  FLD     Y
  FLD     X
  FPATAN
  FWAIT
end;

function Point3D(x, y, z: Integer): TPoint3D;
begin
  result.x := x;
  result.y := y;
  result.z := z;
end;

procedure RectSize(const R: TRect; var RectWidth, RectHeight: Integer);
begin
  with R do
  begin
    RectWidth := Right - Left;
    RectHeight := Bottom - Top;
  end;
end;

procedure RectCenter(const R: TRect; var X, Y: Integer);
begin
  with R do
  begin
    X := (Left + Right) div 2;
    Y := (Top + Bottom) div 2;
  end;
end;

{ TChartPen }

constructor TChartPen.Create(OnChangeEvent: TNotifyEvent);
begin
  inherited Create;
  FVisible := True;
  OnChange := OnChangeEvent;
  {$IFDEF TEEPENEND}
  FEndStyle := esRound;
  {$ENDIF}
  {$IFDEF CLX}
  Width := 1;
  {$ENDIF}
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}

constructor TChartPen.Load(var S: TOldStream; OnChangeEvent: TNotifyEvent);
var
  smlValue          : SmallInt;
  tmpColor          : TColor;
begin
  inherited Create;

  // Unpack values (this method slightly limits the maximum value of Width)
  S.Read(smlValue, SizeOf(SmallInt));
  FVisible := (smlValue and $0001) <> 0;
  FSmallDots := (smlValue and $0002) <> 0;
  smlValue := smlValue shr 2;
  Style := TPenStyle(smlValue and $0007);
  Width := smlValue shr 3;

  OnChange := OnChangeEvent;
  S.Read(tmpColor, SizeOf(TColor));
  Color := tmpColor;
end;

procedure TChartPen.Store(var S: TOldStream);
var
  tmpColor          : TColor;
  smlValue          : SmallInt;
begin
  // Pack values (this method slightly limits the maximum value of Width)
  smlValue := Width;
  smlValue := smlValue shl 3;
  smlValue := smlValue or Ord(Style);
  smlValue := smlValue shl 1;
  if FSmallDots then
    smlValue := smlValue or 1;
  smlValue := smlValue shl 1;
  if FVisible then
    smlValue := smlValue or 1;
  S.Write(smlValue, SizeOf(SmallInt));

  tmpColor := Color;
  S.Write(tmpColor, SizeOf(TColor));
end;

function TChartPen.OnCompareItems(Item: TPersistent): Boolean;
begin
  Result := FALSE;
  if Item is TChartPen then
    with TChartPen(Item) do
      if Self.FVisible = FVisible then
        if Self.FSmallDots = FSmallDots then
          if Self.Style = Style then
            if Self.Width = Width then
              Result := Self.Color = Color;
end;

function TChartPen.CreateDeterminant(Determinant: string): string;
var
  Size              : Integer;
  smlValue          : SmallInt;
  tmpColor          : TColor;
begin
  Size := Length(Determinant);
  SetLength(Result, Size + SizeOf(SmallInt) + SizeOf(TColor));
  CopyMemory(@Result[1], @Determinant[1], Size);
  Inc(Size);

  // Pack values (this method slightly limits the maximum value of Width)
  smlValue := Width;
  smlValue := smlValue shl 3;
  smlValue := smlValue or Ord(Style);
  smlValue := smlValue shl 1;
  if FSmallDots then
    smlValue := smlValue or 1;
  smlValue := smlValue shl 1;
  if FVisible then
    smlValue := smlValue or 1;
  CopyMemory(@Result[Size], @smlValue, SizeOf(SmallInt));
  Inc(Size, SizeOf(SmallInt));

  tmpColor := Color;
  CopyMemory(@Result[Size], @tmpColor, SizeOf(TColor));
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

procedure TChartPen.Assign(Source: TPersistent);
begin
  if Source is TChartPen then
  begin
    FVisible := TChartPen(Source).Visible;
    FSmallDots := TChartPen(Source).SmallDots;
  end;
  inherited;
end;

{$IFDEF TEEPENEND}

procedure TChartPen.SetEndStyle(const Value: TPenEndStyle);
begin
  if FEndStyle <> Value then
  begin
    FEndStyle := Value;
    Changed;
  end;
end;
{$ENDIF}

procedure TChartPen.SetSmallDots(Value: Boolean);
begin
  if FSmallDots <> Value then
  begin
    FSmallDots := Value;
    Changed;
  end;
end;

procedure TChartPen.SetVisible(Value: Boolean);
begin
  if FVisible <> Value then
  begin
    FVisible := Value;
    Changed;
  end;
end;

{ TChartHiddenPen }

constructor TChartHiddenPen.Create(OnChangeEvent: TNotifyEvent);
begin
  inherited;
  Visible := False;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{
Constructor TChartHiddenPen.Load(var S: TOldStream; OnChangeEvent: TNotifyEvent);
begin
   inherited;
end;

Procedure TChartHiddenPen.Store(var S: TOldStream);
begin
   inherited;
end;
}
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{ TDottedGrayPen }

constructor TDottedGrayPen.Create(OnChangeEvent: TNotifyEvent);
begin
  inherited;
  Color := clGray;
  Style := psDot;
end;

{ TDarkGrayPen }

constructor TDarkGrayPen.Create(OnChangeEvent: TNotifyEvent);
begin
  inherited;
  Color := clDkGray;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}
{
Constructor TDarkGrayPen.Load(var S: TOldStream; OnChangeEvent: TNotifyEvent);
begin
   inherited;
   Color:=clDkGray;
end;

Procedure TDarkGrayPen.Store(var S: TOldStream);
begin
   inherited;
end;
}
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{ TChartAxisPen }

constructor TChartAxisPen.Create(OnChangeEvent: TNotifyEvent);
begin
  inherited;
  Width := 2;
end;

{ TChartArrowPen }

constructor TChartArrowPen.Create(OnChangeEvent: TNotifyEvent);
begin
  inherited;
  Color := clWhite;
end;

{ TChartBrush }

constructor TChartBrush.Create(OnChangeEvent: TNotifyEvent);
begin
  inherited Create;
  Color := clDefault;
  FImage := TPicture.Create;
  FImage.OnChange := OnChangeEvent;
  OnChange := OnChangeEvent;
end;

destructor TChartBrush.Destroy;
begin
  FImage.Free;
  inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}

constructor TChartBrush.Load(var S: TOldStream; OnChangeEvent: TNotifyEvent);
var
  tmpColor          : TColor;
  tmpStyle          : TBrushStyle;
begin
  inherited Create;
  FImage := TPicture.Create;
  FImage.OnChange := OnChangeEvent;
  OnChange := OnChangeEvent;
  S.Read(tmpColor, SizeOf(TColor));
  Color := tmpColor;
  S.Read(tmpStyle, SizeOf(TBrushStyle));
  Style := tmpStyle;
end;

procedure TChartBrush.Store(var S: TOldStream);
var
  tmpColor          : TColor;
  tmpStyle          : TBrushStyle;
begin
  tmpColor := Color;
  S.Write(tmpColor, SizeOf(TColor));
  tmpStyle := Style;
  S.Write(tmpStyle, SizeOf(TBrushStyle));
end;

function TChartBrush.OnCompareItems(Item: TPersistent): Boolean;
begin
  Result := FALSE;
  if Item is TChartBrush then
    with TChartBrush(Item) do
      if Self.Style = Style then
        Result := Self.Color = Color;
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

procedure TChartBrush.Assign(Source: TPersistent);
begin
  if Source is TChartBrush then
    FImage.Assign(TChartBrush(Source).Image);
  inherited;
end;

procedure TChartBrush.SetImage(Value: TPicture);
begin
  FImage.Assign(Value);
end;

{ TView3DOptions }

constructor TView3DOptions.Create(AParent: TWinControl);
begin
  inherited Create;
  FParent := AParent;
  FOrthogonal := True;
  FOrthoAngle := 45;
  FZoom := 100;                         { % }
  FZoomText := True;
  FRotation := 345;
  FElevation := 345;
  FPerspective := TeeDefaultPerspective; { % }
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}

constructor TView3DOptions.Load(var S: TOldStream; AParent: TWinControl);
begin
  inherited Create;
  FParent := AParent;
  S.Read(FOrthogonal, SizeOf(Boolean));
  S.Read(FOrthoAngle, SizeOf(FOrthoAngle));
  S.Read(FZoom, SizeOf(FZoom));
  S.Read(FZoomText, SizeOf(Boolean));
  S.Read(FRotation, SizeOf(FRotation));
  S.Read(FElevation, SizeOf(FElevation));
  S.Read(FPerspective, SizeOf(FPerspective));
  S.Read(FTilt, SizeOf(FTilt));
  S.Read(FHorizOffset, SizeOf(FHorizOffset));
  S.Read(FVertOffset, SizeOf(FVertOffset));
end;

procedure TView3DOptions.Store(var S: TOldStream);
begin
  S.Write(FOrthogonal, SizeOf(Boolean));
  S.Write(FOrthoAngle, SizeOf(FOrthoAngle));
  S.Write(FZoom, SizeOf(FZoom));
  S.Write(FZoomText, SizeOf(Boolean));
  S.Write(FRotation, SizeOf(FRotation));
  S.Write(FElevation, SizeOf(FElevation));
  S.Write(FPerspective, SizeOf(FPerspective));
  S.Write(FTilt, SizeOf(FTilt));
  S.Write(FHorizOffset, SizeOf(FHorizOffset));
  S.Write(FVertOffset, SizeOf(FVertOffset));
end;

function TView3DOptions.OnCompareItems(Item: TPersistent): Boolean;
begin
  Result := FALSE;
  if Item is TView3DOptions then
    with TView3DOptions(Item) do
      if Self.FOrthogonal = FOrthogonal then
        if Self.FOrthoAngle = FOrthoAngle then
          if Self.FZoom = FZoom then
            if Self.FZoomText = FZoomText then
              if Self.FRotation = FRotation then
                if Self.FElevation = FElevation then
                  if Self.FPerspective = FPerspective then
                    if Self.FTilt = FTilt then
                      if Self.FHorizOffset = FHorizOffset then
                        Result := Self.FVertOffset = FVertOffset;
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

procedure TView3DOptions.Repaint;
begin
  FParent.Invalidate;
end;

procedure TView3DOptions.SetIntegerProperty(var Variable: Integer; Value: Integer);
begin
  if Variable <> Value then
  begin
    Variable := Value;
    Repaint;
  end;
end;

procedure TView3DOptions.SetBooleanProperty(var Variable: Boolean; Value: Boolean);
begin
  if Variable <> Value then
  begin
    Variable := Value;
    Repaint;
  end;
end;

procedure TView3DOptions.SetElevation(Value: Integer);
begin
  SetIntegerProperty(FElevation, Value);
end;

procedure TView3DOptions.SetPerspective(Value: Integer);
begin
  SetIntegerProperty(FPerspective, Value);
end;

procedure TView3DOptions.SetRotation(Value: Integer);
begin
  SetIntegerProperty(FRotation, Value);
end;

procedure TView3DOptions.SetTilt(Value: Integer);
begin
  SetIntegerProperty(FTilt, Value);
end;

procedure TView3DOptions.SetHorizOffset(Value: Integer);
begin
  if FHorizOffset <> Value then
  begin
    FHorizOffset := Value;
    Repaint;
    if Assigned(FOnScrolled) then FOnScrolled(True);
  end;
end;

procedure TView3DOptions.SetVertOffset(Value: Integer);
begin
  if FVertOffset <> Value then
  begin
    FVertOffset := Value;
    Repaint;
    if Assigned(FOnScrolled) then FOnScrolled(False);
  end;
end;

procedure TView3DOptions.SetOrthoAngle(Value: Integer);
begin
  SetIntegerProperty(FOrthoAngle, Value);
end;

procedure TView3DOptions.SetOrthogonal(Value: Boolean);
begin
  SetBooleanProperty(FOrthogonal, Value);
end;

procedure TView3DOptions.SetZoom(Value: Integer);
begin
  if FZoom <> Value then
  begin
    if Assigned(FOnChangedZoom) then FOnChangedZoom(Value);
    FZoom := Value;
    Repaint;
  end;
end;

procedure TView3DOptions.SetZoomText(Value: Boolean);
begin
  SetBooleanProperty(FZoomText, Value);
end;

procedure TView3DOptions.Assign(Source: TPersistent);
begin
  if Source is TView3DOptions then
    with TView3DOptions(Source) do
    begin
      Self.FElevation := FElevation;
      Self.FHorizOffset := FHorizOffset;
      Self.FOrthoAngle := FOrthoAngle;
      Self.FOrthogonal := FOrthogonal;
      Self.FPerspective := FPerspective;
      Self.FRotation := FRotation;
      Self.FTilt := FTilt;
      Self.FVertOffset := FVertOffset;
      Self.FZoom := FZoom;
      Self.FZoomText := FZoomText;
    end;
end;

{ TTeeCanvas }

procedure TTeeCanvas.SetCanvas(ACanvas: TCanvas);
begin
  FCanvas := ACanvas;
  FPen := FCanvas.Pen;
  FFont := FCanvas.Font;
  FBrush := FCanvas.Brush;
end;

function TTeeCanvas.GetBackMode: TCanvasBackMode;
begin
  {$IFDEF CLX}
  if QPainter_BackgroundMode(Handle) = BGMode_TransparentMode then
    result := cbmTransparent
  else
    result := cbmOpaque;
  {$ELSE}
  result := TCanvasBackMode(GetBkMode(FCanvas.Handle));
  {$ENDIF}
end;

procedure TTeeCanvas.SetBackMode(Mode: TCanvasBackMode); { Opaque, Transparent }
begin
  {$IFDEF CLX}
  if Mode = cbmTransparent then
    QPainter_setBackgroundMode(Handle, BGMode_TransparentMode)
  else
    if Mode = cbmOpaque then QPainter_setBackGroundMode(Handle, BGMode_OpaqueMode);
  {$ELSE}
  SetBkMode(FCanvas.Handle, Ord(Mode));
  {$ENDIF}
end;

procedure TTeeCanvas.SetBackColor(Color: TColor);
{$IFDEF CLX}
var
  QC                : QColorH;
  {$ENDIF}
begin
  {$IFDEF CLX}
  QC := QColor(Color);
  QPainter_setBackgroundColor(Handle, QC);
  QColor_destroy(QC);
  {$ELSE}
  SetBkColor(FCanvas.Handle, TColorRef(ColorToRGB(Color)));
  {$ENDIF}
end;

function TTeeCanvas.GetBackColor: TColor;
begin
  {$IFDEF CLX}
  result := QColorColor(QPainter_backgroundColor(Handle));
  {$ELSE}
  result := TColor(GetBkColor(FCanvas.Handle));
  {$ENDIF}
end;

procedure TTeeCanvas.ResetState;
begin
  with FPen do
  begin
    Color := clBlack;
    Width := 1;
    Style := psSolid;
  end;
  with FBrush do
  begin
    Color := clWhite;
    Style := bsSolid;
  end;
  with FFont do
  begin
    Color := clBlack;
    Size := 10;
  end;
  BackColor := clWhite;
  BackMode := cbmTransparent;
end;

procedure TTeeCanvas.AssignBrush(ABrush: TChartBrush; ABackColor: TColor);
begin
  AssignBrushColor(ABrush, ABackColor, ABrush.Color);
end;

{$IFDEF CLX}

procedure SetTextColor(Handle: QPainterH; Color: Integer);
var
  QC                : QColorH;
begin
  QC := QColor(Color);
  try
    QPen_setColor(QPainter_pen(Handle), QC);
  finally
    QColor_destroy(QC);
  end;
end;
{$ENDIF}

procedure TTeeCanvas.AssignBrushColor(ABrush: TChartBrush; AColor, ABackColor: TColor);
begin
  if Monochrome then AColor := clWhite;
  if Assigned(ABrush.Image.Graphic) then
  begin
    Brush.Style := bsClear;
    Brush.Bitmap := ABrush.Image.Bitmap;
    SetTextColor(Handle, ColorToRGB(AColor));
    BackMode := cbmOpaque;
  end
  else
  begin
    Brush.Color := AColor;
    Brush.Style := ABrush.Style;
    BackMode := cbmOpaque;
  end;
  BackColor := ABackColor;
end;

procedure TTeeCanvas.AssignVisiblePen(APen: TPen);
begin
  AssignVisiblePenColor(APen, APen.Color);
end;

function IsWindowsNT: Boolean;
begin
  {$IFNDEF CLX}
  result := Win32Platform = VER_PLATFORM_WIN32_NT;
  {$ELSE}
  result := False;
  {$ENDIF}
end;

procedure TTeeCanvas.AssignVisiblePenColor(APen: TPen; AColor: TColor);
{$IFNDEF CLX}
const
  PenStyles         : array[TPenStyle] of Word =
    (PS_SOLID, PS_DASH, PS_DOT, PS_DASHDOT, PS_DASHDOTDOT, PS_NULL,
    PS_INSIDEFRAME);

var
  LBrush            : TLogBrush;
  {$ENDIF}
var
  tmpIsPen          : Boolean;
  {$IFDEF TEEPENEND}
var
  tmpEnd            : Integer;
  {$ENDIF}
begin
  if MonoChrome then AColor := clBlack;
  tmpIsPen := APen is TChartPen;
  if (not tmpIsPen) or TChartPen(APen).Visible then
  begin
    {$IFNDEF CLX}
    { only valid in Windows-NT ... }
    if IsWindowsNT then
    begin
      if tmpIsPen and TChartPen(APen).SmallDots then
      begin
        LBrush.lbStyle := bs_Solid;
        LBrush.lbColor := ColorToRGB(AColor);
        FPen.Handle := ExtCreatePen(PS_COSMETIC or PS_ALTERNATE,
          1, LBrush, 0, nil);
      end
      else
        if (APen.Width > 1){$IFNDEF TEEPENEND} and (APen.Style <> psSolid){$ENDIF} then
        begin
          LBrush.lbStyle := bs_Solid;
          LBrush.lbColor := ColorToRGB(AColor);
          {$IFDEF TEEPENEND}
          case APen.EndStyle of
            esRound: tmpEnd := PS_ENDCAP_ROUND;
            esSquare: tmpEnd := PS_ENDCAP_SQUARE;
          else
            tmpEnd := PS_ENDCAP_FLAT;
          end;
          {$ENDIF}
          FPen.Handle := ExtCreatePen(PS_GEOMETRIC or
            PenStyles[APen.Style] or
            {$IFDEF TEEPENEND}tmpEnd{$ELSE}PS_ENDCAP_SQUARE{$ENDIF},
            APen.Width, LBrush, 0, nil);
        end
        else
        begin
          FPen.Assign(APen);
          FPen.Color := AColor;
        end;
    end
    else
      {$ENDIF}
    begin
      FPen.Assign(APen);
      FPen.Color := AColor;
    end;
  end
  else
    FPen.Style := psClear;
end;

procedure TTeeCanvas.AssignFont(AFont: TTeeFont);
begin
  with FFont do
  begin
    AFont.PixelsPerInch := PixelsPerInch;
    Assign(AFont);
  end;
  if MonoChrome then FFont.Color := clBlack;
  {$IFNDEF CLX}
  SetTextCharacterExtra(Handle, AFont.InterCharSize);
  {$ENDIF}
  IFont := AFont;
  AFont.ICanvas := Self;
end;

function TTeeCanvas.TextWidth(const St: string): Integer;
{$IFNDEF CLX}
var
  Extent            : TSize;
  {$ENDIF}
begin
  {$IFNDEF CLX}
  ReferenceCanvas.Font.Assign(FFont);
  if GetTextExtentPoint32(ReferenceCanvas.Handle,
    PChar(St), Length(St), Extent) then
  begin
    result := Extent.cX;
    if Assigned(IFont) then Inc(result, Abs(IFont.Shadow.HorizSize));
  end
  else
    result := 0;
  {$ELSE}
  result := FCanvas.TextWidth(St);
  if Assigned(IFont) then Inc(result, Abs(IFont.Shadow.HorizSize));
  {$ENDIF}
end;

function TTeeCanvas.TextHeight(const St: string): Integer;
{$IFNDEF CLX}
var
  Extent            : TSize;
  {$ENDIF}
begin
  {$IFNDEF CLX}
  ReferenceCanvas.Font.Assign(FFont);
  if GetTextExtentPoint32(ReferenceCanvas.Handle,
    PChar(St), Length(St), Extent) then
  begin
    result := Extent.cY;
    if Assigned(IFont) then Inc(result, Abs(IFont.Shadow.VertSize));
  end
  else
    result := 0;
  {$ELSE}
  result := FCanvas.TextHeight(St);
  if Assigned(IFont) then Inc(result, Abs(IFont.Shadow.VertSize));
  {$ENDIF}
end;

function TTeeCanvas.FontHeight: Integer;
begin
  result := TextHeight(TeeCharForHeight);
end;

{ TCanvas3D }

procedure TCanvas3D.Assign(Source: TCanvas3D);
begin
  Monochrome := Source.Monochrome;
end;

function TCanvas3D.CalcRect3D(const R: TRect; Z: Integer): TRect;
begin
  result.TopLeft := Calculate3DPosition(R.Left, R.Top, Z);
  result.BottomRight := Calculate3DPosition(R.Right, R.Bottom, Z);
end;

{ TTeeCanvas3D }

constructor TTeeCanvas3D.Create;
begin
  inherited;
  IZoomText := True;
  FBufferedDisplay := True;
  FDirty := True;
  FTextAlign := TA_LEFT;
end;

procedure TTeeCanvas3D.DeleteBitmap;
begin
  FreeAndNil(FBitmap);
end;

destructor TTeeCanvas3D.Destroy;
begin
  DeleteBitmap;
  inherited;
end;

procedure TTeeCanvas3D.TextOut(X, Y: Integer; const Text: string);
var
  tmpX              : Integer;
  tmpY              : Integer;
  tmpDC             : {$IFDEF CLX}QPainterH{$ELSE}HDC{$ENDIF};
begin
  tmpDC := FCanvas.Handle;
  if Assigned(IFont) then
    with IFont.Shadow do
      if (HorizSize <> 0) or (VertSize <> 0) then
      begin
        if HorizSize < 0 then
        begin
          tmpX := X;
          X := X - HorizSize;
        end
        else
          tmpX := X + HorizSize;
        if VertSize < 0 then
        begin
          tmpY := Y;
          Y := Y - VertSize;
        end
        else
          tmpY := Y + VertSize;
        SetTextColor(tmpDC, ColorToRGB(IFont.Shadow.Color));
        {$IFNDEF CLX}
        Windows.TextOut(tmpDC, tmpX, tmpY, PChar(Text), Length(Text));
        {$ELSE}
        FCanvas.TextOut(tmpX, tmpY, Text);
        {$ENDIF}
      end;
  SetTextColor(tmpDC, ColorToRGB(FFont.Color));

  {$IFNDEF CLX}
  if Assigned(IFont) and (IFont.OutLine.Visible) then
  begin
    AssignVisiblePen(IFont.OutLine);
    Brush.Color := FFont.Color;
    Brush.Style := bsSolid;
    tmpDC := FCanvas.Handle;
    BackMode := cbmTransparent;
    BeginPath(tmpDC);
    Windows.TextOut(tmpDC, X, Y, PChar(Text), Length(Text));
    EndPath(tmpDC);
    StrokeAndFillPath(tmpDC);
  end
  else
    Windows.TextOut(tmpDC, X, Y, PChar(Text), Length(Text));
  {$ELSE}
  if (TextAlign and TA_RIGHT) = TA_RIGHT then
    Dec(x, TextWidth(Text))
  else
    if (TextAlign and TA_CENTER) = TA_CENTER then Dec(x, TextWidth(Text) div 2);
  FCanvas.TextOut(x, y, Text);
  {$ENDIF}
end;

procedure TTeeCanvas3D.Rectangle(X0, Y0, X1, Y1: Integer);
begin
  {$IFNDEF CLX}
  Windows.Rectangle(FCanvas.Handle, X0, Y0, X1, Y1);
  {$ELSE}
  FCanvas.Rectangle(X0, Y0, X1, Y1);
  {$ENDIF}
end;

procedure TTeeCanvas3D.RoundRect(X1, Y1, X2, Y2, X3, Y3: Integer);
begin
  {$IFNDEF CLX}
  Windows.RoundRect(FCanvas.Handle, X1, Y1, X2, Y2, X3, Y3);
  {$ELSE}
  FCanvas.RoundRect(X1, Y1, X2, Y2, X3, Y3);
  {$ENDIF}
end;

procedure TTeeCanvas3D.SetTextAlign(Align: TCanvasTextAlign);
begin
  {$IFDEF CLX}
  FTextAlign := Align;
  {$ELSE}
  Windows.SetTextAlign(FCanvas.Handle, Ord(Align));
  {$ENDIF}
end;

procedure TTeeCanvas3D.MoveTo(X, Y: Integer);
begin
  {$IFNDEF CLX}
  Windows.MoveToEx(FCanvas.Handle, X, Y, nil);
  {$ELSE}
  FCanvas.MoveTo(X, Y);
  {$ENDIF}
end;

procedure TTeeCanvas3D.LineTo(X, Y: Integer);
begin
  {$IFNDEF CLX}
  Windows.LineTo(FCanvas.Handle, X, Y);
  {$ELSE}
  FCanvas.LineTo(X, Y);
  {$ENDIF}
end;

procedure TTeeCanvas3D.DoRectangle(const Rect: TRect);
begin
  with Rect do
    {$IFNDEF CLX}
    Windows.Rectangle(FCanvas.Handle, Left, Top, Right, Bottom);
  {$ELSE}
    FCanvas.Rectangle(Left, Top, Right, Bottom);
  {$ENDIF}
end;

{ 3D Canvas }

procedure TTeeCanvas3D.PolygonFour;
begin
  {$IFNDEF CLX}
  Windows.Polygon(FCanvas.Handle, PPoints(@IPoints)^, 4);
  {$ELSE}
  FCanvas.Polygon(IPoints);
  {$ENDIF}
end;

procedure TTeeCanvas3D.PlaneWithZ(P1, P2, P3, P4: TPoint; Z: Integer);
begin
  Calc3DTPoint(P1, Z);
  Calc3DTPoint(P2, Z);
  Calc3DTPoint(P3, Z);
  Calc3DTPoint(P4, Z);
  IPoints[0] := P1;
  IPoints[1] := P2;
  IPoints[2] := P3;
  IPoints[3] := P4;
  PolygonFour;
end;

procedure TTeeCanvas3D.Calc3DTPoint(var P: TPoint; z: Integer);
begin
  Calc3DPos(P.X, P.Y, Z);
end;

function TTeeCanvas3D.Calc3DTPoint3D(const P: TPoint3D): TPoint;
begin
  Calc3DPoint(result, P.X, P.Y, P.Z);
end;

function TTeeCanvas3D.Calculate3DPosition(x, y, z: Integer): TPoint;
begin
  Calc3DPos(x, y, z);
  result.x := x;
  result.y := y;
end;

procedure TTeeCanvas3D.Calc3DPoint(var P: TPoint; x, y, z: Integer);
begin
  Calc3DPos(x, y, z);
  P.x := x;
  P.y := y;
end;

procedure TTeeCanvas3D.Calculate2DPosition(var x, y: Integer; z: Integer);
var
  x1                : Integer;
  tmp               : Double;
begin
  if IZoomFactor <> 0 then
  begin
    tmp := 1.0 / IZoomFactor;
    if FIsOrthogonal then
    begin
      x := Round((x - FXCenterOffset) * tmp) - z + FXCenter;
      y := Round(((y - FYCenterOffset) * tmp) + (IOrthoY * z)) + FYCenter;
    end
    else
      if FIs3D and (tempXX <> 0) and (c2c3 <> 0) then
      begin
        x1 := x;
        z := z - FZCenter;
        x := Round((((x1 - FXCenterOffset) * tmp) - (z * tempXZ) -
          (y - FYCenter) * c2s3) / tempXX) + FXCenter;
        y := Round((((y - FYCenterOffset) * tmp) - (z * tempYZ) -
          (x1 - FXCenter) * tempYX) / c2c3) + FYCenter;
      end;
  end;
end;

procedure TTeeCanvas3D.Calc3DPos(var x, y: Integer; z: Integer);
var
  tmp               : Double;
  x1                : Integer;
  y1                : Integer;
begin
  if FIsOrthogonal then
  begin
    x := Round(IZoomFactor * (x - FXCenter + z)) + FXCenterOffset;
    y := Round(IZoomFactor * (y - FYCenter - (IOrthoY * z))) + FYCenterOffset;
  end
  else
    if FIs3D then
    begin
      Dec(z, FZCenter);
      x1 := x - FXCenter;
      y1 := y - FYCenter;
      if IPerspec > 0 then
        tmp := IZoomFactor * (1 - ((x1 * c2s1 - y1 * s2 + z * c2c1) * IPerspec))
      else
        tmp := IZoomFactor;
      x := Round((x1 * tempXX + y1 * c2s3 + z * tempXZ) * tmp) + FXCenterOffset;
      y := Round((x1 * tempYX + y1 * c2c3 + z * tempYZ) * tmp) + FYCenterOffset;
    end;
end;

function TTeeCanvas3D.GetHandle: TTeeCanvasHandle;
begin
  result := FCanvas.Handle;
end;

procedure TTeeCanvas3D.Cone(Vertical: Boolean; Left, Top, Right, Bottom,
  Z0, Z1: Integer; Dark3D: Boolean; ConePercent: Integer);
begin
  InternalCylinder(Vertical, Left, Top, Right, Bottom, Z0, Z1, Dark3D, ConePercent);
end;

procedure TTeeCanvas3D.Sphere(x, y, z: Integer; const Radius: Double);
var
  tmp               : Integer;
begin
  tmp := Round(Radius);
  EllipseWithZ(x - tmp, y - tmp, x + tmp, y + tmp, z);
end;

procedure TTeeCanvas3D.Surface3D(Style: TTeeCanvasSurfaceStyle;
  SameBrush: Boolean;
  NumXValues, NumZValues: Integer;
  CalcPoints: TTeeCanvasCalcPoints);
begin
  { not implemented in GDI mode. (Use TeeOpenGL) }
end;

procedure TTeeCanvas3D.TextOut3D(x, y, z: Integer; const Text: string);
{$IFNDEF CLX}
var
  tmp               : Integer;
  OldSize           : Integer;
  tmpSizeChanged    : Boolean;
  FDC               : HDC;
  LogRec            : TLogFont;
  NewFont           : HFont;
  OldFont           : HFont;
  {$ENDIF}
begin
  Calc3DPos(x, y, z);
  {$IFNDEF CLX}
  if IZoomText then
  begin
    tmpSizeChanged := False;
    FDC := 0;
    OldFont := 0;
    if IZoomFactor <> 1 then
      with FFont do
      begin
        OldSize := Size;
        tmp := MaxLong(1, Round(IZoomFactor * OldSize));
        if OldSize <> tmp then
        begin
          FDC := FCanvas.Handle;
          GetObject(FFont.Handle, SizeOf(LogRec), @LogRec);
          LogRec.lfHeight := -MulDiv(tmp, FFont.PixelsPerInch, 72);
          NewFont := CreateFontIndirect(LogRec);
          OldFont := SelectObject(FDC, NewFont);
          tmpSizeChanged := True;
        end;
      end;
    TextOut(X, Y, Text);
    if tmpSizeChanged then DeleteObject(SelectObject(FDC, OldFont));
  end
  else
    {$ENDIF}
    TextOut(X, Y, Text);
end;

procedure TTeeCanvas3D.MoveTo3D(X, Y, Z: Integer);
begin
  Calc3DPos(x, y, z);
  {$IFNDEF CLX}
  Windows.MoveToEx(FCanvas.Handle, X, Y, nil);
  {$ELSE}
  FCanvas.MoveTo(X, Y);
  {$ENDIF}
end;

procedure TTeeCanvas3D.LineTo3D(X, Y, Z: Integer);
begin
  Calc3DPos(x, y, z);
  {$IFNDEF CLX}
  Windows.LineTo(FCanvas.Handle, X, Y);
  {$ELSE}
  FCanvas.LineTo(X, Y);
  {$ENDIF}
end;

procedure TTeeCanvas3D.RectangleWithZ(const Rect: TRect; Z: Integer);
begin
  with Rect do
  begin
    Calc3DPoint(IPoints[0], Left, Top, Z);
    Calc3DPoint(IPoints[1], Right, Top, Z);
    Calc3DPoint(IPoints[2], Right, Bottom, Z);
    Calc3DPoint(IPoints[3], Left, Bottom, Z);
  end;
  PolygonFour;
end;

procedure TTeeCanvas3D.DoHorizLine(X0, X1, Y: Integer);
{$IFNDEF CLX}
var
  FDC               : HDC;
  {$ENDIF}
begin
  {$IFNDEF CLX}
  FDC := FCanvas.Handle;
  Windows.MoveToEx(FDC, X0, Y, nil);
  Windows.LineTo(FDC, X1, Y);
  {$ELSE}
  QPainter_drawLine(Handle, x0, y, x1, y);
  {$ENDIF}
end;

procedure TTeeCanvas3D.DoVertLine(X, Y0, Y1: Integer);
{$IFNDEF CLX}
var
  FDC               : HDC;
  {$ENDIF}
begin
  {$IFNDEF CLX}
  FDC := FCanvas.Handle;
  Windows.MoveToEx(FDC, X, Y0, nil);
  Windows.LineTo(FDC, X, Y1);
  {$ELSE}
  QPainter_drawLine(Handle, x, y0, x, y1);
  {$ENDIF}
end;

procedure ClipCanvas(ACanvas: TCanvas; const Rect: TRect);
{$IFNDEF CLX}
var
  P                 : array[0..1] of TPoint;
  Region            : HRgn;
  tmpDC             : HDC;
  {$ENDIF}
begin
  {$IFNDEF CLX}
  with Rect do
  begin
    p[0] := TopLeft;
    p[1] := BottomRight;
  end;
  tmpDC := ACanvas.Handle;
  LPToDP(tmpDC, P, 2);
  Region := CreateRectRgn(P[0].X, P[0].Y, P[1].X, P[1].Y);
  WasOldRegion := GetClipRgn(tmpDC, OldRegion) = 1;
  ExtSelectClipRgn(tmpDC, Region, RGN_AND);
  DeleteObject(Region);
  {$ELSE}
  ACanvas.SetClipRect(Rect);
  {$ENDIF}
end;

procedure TTeeCanvas3D.ClipRectangle(const Rect: TRect);
begin
  ClipCanvas(FCanvas, Rect);
end;

procedure ClipRoundRectangle(ACanvas: TTeeCanvas; const Rect: TRect; RoundSize: Integer);
{$IFNDEF CLX}
var
  R                 : TRect;
  Region            : HRgn;
  DC                : HDC;
  {$ENDIF}
begin
  {$IFNDEF CLX}
  DC := ACanvas.Handle;
  R := Rect;
  LPToDP(DC, R, 2);
  with R do
    Region := CreateRoundRectRgn(Left, Top, Right, Bottom, RoundSize, RoundSize);
  WasOldRegion := GetClipRgn(DC, OldRegion) = 1;
  ExtSelectClipRgn(DC, Region, RGN_AND);
  DeleteObject(Region);
  {$ELSE}
  ACanvas.ClipRectangle(Rect);
  {$ENDIF}
end;

procedure ClipPolygon(ACanvas: TTeeCanvas; var Points: array of TPoint; NumPoints: Integer);
{$IFNDEF CLX}
var
  tmpDC             : HDC;
  Region            : HRgn;
  {$ENDIF}
begin
  {$IFNDEF CLX}
  tmpDC := ACanvas.Handle;
  LPToDP(tmpDC, Points, NumPoints);
  Region := CreatePolygonRgn(Points, NumPoints, ALTERNATE);
  if Region <> 0 then
  begin
    WasOldRegion := GetClipRgn(tmpDC, OldRegion) = 1;
    ExtSelectClipRgn(tmpDC, Region, RGN_AND);
    DeleteObject(Region);
  end;
  {$ENDIF}
end;

procedure TTeeCanvas3D.ClipCube(const Rect: TRect; MinZ, MaxZ: Integer);
{$IFNDEF CLX}
var
  P                 : array[0..5] of TPoint;
  Region            : HRgn;
  FDC               : HDC;
  tmpR              : TRect;
  pa                : TPoint;
  pb                : TPoint;
  {$ENDIF}
begin
  {$IFNDEF CLX}
  if FIs3D then
    with Rect do
    begin
      with View3DOptions do
        if Elevation = 270 then
        begin
          if (Rotation = 270) or (Rotation = 360) then
          begin
            Calc3DPoint(tmpR.TopLeft, Left, Top, MinZ);
            Calc3DPoint(tmpR.BottomRight, Right, Top, MaxZ);
            ClipRectangle(tmpR);
            Exit;
          end;
        end;

      Calc3DPoint(p[0], Left, Bottom, MinZ);
      Calc3DPoint(p[1], Left, Top, MinZ);

      Calc3DPoint(pa, Left, Top, MaxZ);
      Calc3DPoint(pb, Right, Top, MinZ);

      if pb.Y < pa.Y then
        p[2] := pb
      else
        p[2] := pa;

      Calc3DPoint(p[3], Right, Top, MaxZ);

      Calc3DPoint(pa, Right, Bottom, MaxZ);
      Calc3DPoint(pb, Right, Top, MinZ);

      if pb.x > pa.x then
        p[4] := pb
      else
        p[4] := pa;

      Calc3DPoint(p[5], Right, Bottom, MinZ);
      if p[5].x < p[0].x then
      begin
        p[0].x := p[5].x;
        if pb.y < p[0].y then p[0].y := pb.y;
      end;

      FDC := FCanvas.Handle;
      LPToDP(FDC, P, 6);
      Region := CreatePolygonRgn(P, 6, ALTERNATE);
      SelectClipRgn(FDC, Region);
      DeleteObject(Region);
    end
  else
  begin
    tmpR := Rect;
    Inc(tmpR.Left);
    Inc(tmpR.Top);
    Dec(tmpR.Bottom);
    ClipRectangle(tmpR);
  end;
  {$ELSE}
  ClipRectangle(Rect);
  {$ENDIF}
end;

procedure UnClipCanvas(ACanvas: TCanvas);
begin
  {$IFNDEF CLX}
  if WasOldRegion then
    SelectClipRgn(ACanvas.Handle, OldRegion)
  else
    SelectClipRgn(ACanvas.Handle, 0);
  {$ELSE}
  QPainter_setClipping(ACanvas.Handle, False);
  {$ENDIF}
end;

procedure TTeeCanvas3D.UnClipRectangle;
begin
  UnClipCanvas(FCanvas);
end;

procedure TTeeCanvas3D.Projection(MaxDepth: Integer; const Bounds, Rect: TRect);
begin
  RectCenter(Rect, FXCenter, FYCenter);
  Inc(FXCenter, Round(RotationCenter.X));
  Inc(FYCenter, Round(RotationCenter.Y));
  FZCenter := (MaxDepth div 2) + Round(RotationCenter.Z);
  FXCenterOffset := FXCenter;
  FYCenterOffset := FYCenter;
  if Assigned(F3DOptions) then
    with F3DOptions do
    begin
      Inc(FXCenterOffset, HorizOffset);
      Inc(FYCenterOffset, VertOffset);
      if (Perspective > 0) and ((Rect.Right - Rect.Left) <> 0) then
        IPerspec := Perspective / 150 / (Rect.Right - Rect.Left);
    end;
end;

function TTeeCanvas3D.InitWindow(DestCanvas: TCanvas;
  A3DOptions: TView3DOptions;
  ABackColor: TColor;
  Is3D: Boolean;
  const UserRect: TRect): TRect;

  procedure CalcTrigValues;
  var
    c1              : Extended;
    c2              : Extended;
    c3              : Extended;
    s1              : Extended;
    s3              : Extended;
    rx              : Double;
    ry              : Double;
    rz              : Double;
  begin
    if not FIsOrthogonal then
    begin
      if Assigned(F3DOptions) then
        with F3DOptions do
        begin
          rx := Rotation;
          ry := Elevation;
          rz := Tilt;
        end
      else
      begin
        rx := 0;
        ry := 0;
        rz := 0;
      end;

      IPerspec := 0;

      SinCos(rx * TeePiStep, s1, c1);
      SinCos(ry * TeePiStep, s2, c2);
      SinCos(rz * TeePiStep, s3, c3);

      c2s3 := c2 * s3;
      c2c3 := MaxDouble(1E-5, c2 * c3);

      tempXX := MaxDouble(1E-5, s1 * s2 * s3 + c1 * c3);
      tempYX := (c3 * s1 * s2 - c1 * s3);

      tempXZ := (c1 * s2 * s3 - c3 * s1);
      tempYZ := (c1 * c3 * s2 + s1 * s3);

      c2s1 := c2 * s1;
      c2c1 := c1 * c2;
    end;
  end;

var
  tmpH              : Integer;
  tmpW              : Integer;
  tmpCanvas         : TCanvas;
  tmpSin            : Extended;
  tmpCos            : Extended;
begin
  FBounds := UserRect;
  F3DOptions := A3DOptions;

  FIs3D := Is3D;
  FIsOrthogonal := False;
  IZoomFactor := 1;
  if FIs3D then
  begin
    if Assigned(F3DOptions) then
    begin
      FIsOrthogonal := F3DOptions.Orthogonal;
      if FIsOrthogonal then
      begin
        SinCos(F3DOptions.OrthoAngle * TeePiStep, tmpSin, tmpCos);
        if tmpCos < 0.01 then
          IOrthoY := 1
        else
          IOrthoY := tmpSin / tmpCos;
      end;
      IZoomFactor := 0.01 * F3DOptions.Zoom;

      IZoomText := F3DOptions.ZoomText;
    end;
    CalcTrigValues;
  end;

  if FBufferedDisplay then
  begin
    RectSize(UserRect, tmpW, tmpH);
    if Assigned(FBitmap) and ((FBitmap.Width <> tmpW) or (FBitmap.Height <> tmpH)) then
      DeleteBitmap;
    if not Assigned(FBitmap) then
    begin
      FBitmap := TBitMap.Create;
      FBitmap.Width := tmpW;
      FBitmap.Height := tmpH;
    end;
    tmpCanvas := FBitmap.Canvas;
    tmpCanvas.OnChange := nil;
    tmpCanvas.OnChanging := nil;
    SetCanvas(tmpCanvas);
    result := {$IFDEF D6}Types.{$ENDIF}Rect(0, 0, tmpW, tmpH);
  end
  else
  begin
    SetCanvas(DestCanvas);
    result := UserRect;
  end;
end;

procedure TTeeCanvas3D.TransferBitmap(ALeft, ATop: Integer; ACanvas: TCanvas);
begin
  {$IFNDEF CLX}
  BitBlt(ACanvas.Handle, ALeft, ATop,
    FBitmap.Width,
    FBitmap.Height,
    FBitmap.Canvas.Handle, 0, 0, SRCCOPY);
  {$ELSE}
  QPainter_drawPixmap(ACanvas.Handle, ALeft, ATop, FBitmap.Handle, 0, 0,
    FBitmap.Width, FBitmap.Height);
  //  FCanvas.Draw(ALeft,ATop,FBitmap);
  {$ENDIF}
end;

function TTeeCanvas3D.ReDrawBitmap: Boolean;
begin
  result := not FDirty;
  if result then TransferBitmap(0, 0, FCanvas);
end;

procedure TTeeCanvas3D.ShowImage(DestCanvas, DefaultCanvas: TCanvas; const UserRect: TRect);
begin
  if FBufferedDisplay then
  begin
    with UserRect do
      TransferBitmap(Left, Top, DestCanvas);
    FDirty := False;
  end;
  SetCanvas(DefaultCanvas);
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}

procedure TTeeCanvas3D.DrawImage(ABitmapDC: HDC; const UserRect: TRect);
const
  Name              : AnsiString = 'E:\1.bmp';
var
  Res               : DWORD;
  Info              : PBitMapInfo;
  Bits              : PByte;
  DC                : HDC;
begin
  if FBufferedDisplay then
  begin
    Info := AllocMem(SizeOf(TBitMapInfo) + 256 * SizeOf(TRGBQuad));
    if Assigned(Info) then
    try
      Info.bmiHeader.biSize := SizeOf(TBitmapInfoHeader); // Only that way :o)
      DC := CreateCompatibleDC(GetDC(0));
      try
        if GetDIBits({ABitmap} DC, FBitMap.Handle, 0, Info.bmiHeader.biHeight, nil, Info^, DIB_RGB_COLORS) <> 0 then
        begin
          GetMem(Bits, Info.bmiHeader.biSizeImage);
          try
            // Now get the bits themself
            { Old variant used by Alex (our) not works from now. Don't want know why that :o)
                StretchBlt(ABitmapDC, UserRect.Left, UserRect.Bottom,
                  Abs(UserRect.Right - UserRect.Left), -Abs(UserRect.Bottom - UserRect.Top),
                  FBitmap.Canvas.Handle, 0, 0,
                  FBitmap.Width, FBitmap.Height,
                  SRCCOPY)
            }
            if GetDIBits(DC, FBitMap.Handle, 0, Info.bmiHeader.biHeight, Bits,
                         Info^, DIB_RGB_COLORS) <> 0 then
              Res := StretchDIBits(ABitmapDC, UserRect.Left, UserRect.Bottom,
                                   Succ(Abs(UserRect.Right - UserRect.Left)),
                                   Pred(-Abs(UserRect.Bottom - UserRect.Top)),
                                   0, 0, Info.bmiHeader.biWidth,
                                   Info.bmiHeader.biHeight,
                                   Bits, Info^, DIB_RGB_COLORS, SRCCOPY);
          finally
            FreeMem(Bits);
          end;
        end;
      finally
        DeleteDC(DC);
      end;
    finally
      FreeMem(Info);                    // Free bitmap info
    end;
    //      Res := GetDeviceCaps(ABitmapDC, TECHNOLOGY);
    FDirty := False;
  end;
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

type
  TTeeGraphic = class(TGraphic)
  end;

procedure TTeeCanvas3D.StretchDraw(const Rect: TRect; Graphic: TGraphic);
begin
  if Assigned(Graphic) then
    {$IFDEF CLX}
    FCanvas.StretchDraw(Rect, Graphic)
      {$ELSE}
    TTeeGraphic(Graphic).Draw(FCanvas, Rect);
  {$ENDIF}
end;

procedure TTeeCanvas3D.Draw(X, Y: Integer; Graphic: TGraphic);
begin
  if Assigned(Graphic) and (not Graphic.Empty) then
  begin
    BackColor := ColorToRGB(FBrush.Color);
    SetTextColor(FCanvas.Handle, ColorToRGB(FFont.Color));
    TTeeGraphic(Graphic).Draw(FCanvas,
      {$IFDEF D6}Types.{$ELSE}Classes.{$ENDIF}Bounds(X, Y, Graphic.Width, Graphic.Height));
  end;
end;

{$IFDEF CLX}

function GetRValue(Color: Integer): Byte;
var
  QC                : QColorH;
begin
  QC := QColor(Color);
  try
    result := QColor_red(QC);
  finally QColor_destroy(QC);
  end;
end;

function GetGValue(Color: Integer): Byte;
var
  QC                : QColorH;
begin
  QC := QColor(Color);
  try
    result := QColor_green(QC);
  finally QColor_destroy(QC);
  end;
end;

function GetBValue(Color: Integer): Byte;
var
  QC                : QColorH;
begin
  QC := QColor(Color);
  try
    result := QColor_blue(QC);
  finally QColor_destroy(QC);
  end;
end;

function QRGB(r, g, b: Integer): QColorH;
begin
  result := QColor_create(r, g, b);
end;

function RGB(r, g, b: Integer): TColor;
begin
  result := QColorColor(QRGB(r, g, b))
end;
{$ENDIF}

procedure TTeeCanvas3D.GradientFill(const Rect: TRect;
  StartColor: TColor;
  EndColor: TColor;
  Direction: TGradientDirection);
var
  T0, T1, T2        : Integer;
  D0, D1, D2        : Integer;
  tmpBrush          : {$IFDEF CLX}QBrushH{$ELSE}HBRUSH{$ENDIF};
  FDC               : TTeeCanvasHandle;
  OldColor          : {$IFDEF CLX}QColorH{$ELSE}TColor{$ENDIF};

  procedure CalcBrushColor(Index, Range: Integer);
  var
    tmp             : {$IFDEF CLX}QColorH{$ELSE}TColor{$ENDIF};
  begin
    tmp := {$IFDEF CLX}QRGB{$ELSE}RGB{$ENDIF}(T0 + MulDiv(Index, D0, Range),
      T1 + MulDiv(Index, D1, Range),
      T2 + MulDiv(Index, D2, Range));
    if tmp <> OldColor then
    begin
      {$IFDEF CLX}
      QBrush_setColor(tmpBrush, tmp);
      {$ELSE}
      if tmpBrush <> 0 then DeleteObject(SelectObject(FDC, tmpBrush));
      tmpBrush := SelectObject(FDC, CreateSolidBrush(tmp));
      {$ENDIF}
      OldColor := tmp;
    end;
  end;

var
  tmpRect           : TRect;

  procedure RectGradient(Horizontal: Boolean);
  var
    t,
      P1,
      P2,
      P3            : Integer;
    Size            : Integer;
    Steps           : Integer;
  begin
    FDC := FCanvas.Handle;
    with tmpRect do
    begin
      if Horizontal then
      begin
        P3 := Bottom - Top;
        Size := Right - Left;
      end
      else
      begin
        P3 := Right - Left;
        Size := Bottom - Top;
      end;
      Steps := Size;
      if Steps > 256 then Steps := 256;
      P1 := 0;
      {$IFNDEF CLX}
      OldColor := -1;
      {$ENDIF}
      for t := 0 to Steps - 1 do
      begin
        CalcBrushColor(t, Pred(Steps));
        P2 := MulDiv(t + 1, Size, Steps);
        {$IFDEF CLX}
        {$ELSE}
        if Horizontal then
          PatBlt(FDC, Right - P1, Top, P1 - P2, P3, PATCOPY)
        else
          PatBlt(FDC, Left, Bottom - P1, P3, P1 - P2, PATCOPY);
        {$ENDIF}
        P1 := P2;
      end;
    end;
  end;

  procedure FromCorner;
  var
    FromTop         : Boolean;
    SizeX,
      SizeY,
      tmpDiagonal,
      tmp1,
      tmp2,
      P0,
      P1            : Integer;
  begin
    FromTop := Direction = gdFromTopLeft;
    with tmpRect do
      if FromTop then
        P1 := Top
      else
        P1 := Bottom;
    P0 := P1;
    RectSize(tmpRect, SizeX, SizeY);
    tmpDiagonal := SizeX + SizeY;
    FDC := FCanvas.Handle;
    tmp1 := 0;
    tmp2 := 0;
    repeat
      CalcBrushColor(tmp1 + tmp2, tmpDiagonal);
      {$IFDEF CLX}
      {$ELSE}
      PatBlt(FDC, tmpRect.Left + tmp2, P0, 1, P1 - P0, PATCOPY);
      {$ENDIF}
      if tmp1 < SizeY then
      begin
        Inc(tmp1);
        if FromTop then
        begin
          {$IFDEF CLX}
          {$ELSE}
          PatBlt(FDC, tmpRect.Left, P0, tmp2 + 1, 1, PATCOPY);
          {$ENDIF}
          if P0 < tmpRect.Bottom then
            Inc(P0)
        end
        else
        begin
          {$IFDEF CLX}
          {$ELSE}
          PatBlt(FDC, tmpRect.Left, P0 - 1, tmp2 + 1, 1, PATCOPY);
          {$ENDIF}
          if P0 > tmpRect.Top then Dec(P0);
        end;
      end;
      if tmp2 < SizeX then Inc(tmp2);
    until (tmp1 >= SizeY) and (tmp2 >= SizeX);
  end;

  procedure FromCenter;
  const
    TeeGradientPrecision: Integer = 1;  { how many pixels precision, ( 1=best) }
  var
    tmpXCenter,
      tmpYCenter,
      SizeX,
      SizeY,
      P0,
      P1,
      tmpLeft,
      tmpTop,
      tmp1,
      tmp2,
      tmp3          : Integer;
  begin
    RectSize(tmpRect, SizeX, SizeY);
    tmpXCenter := SizeX shr 1;
    tmpYCenter := SizeY shr 1;
    tmp1 := 0;
    tmp2 := 0;
    tmp3 := tmpXCenter + tmpYCenter;
    FDC := FCanvas.Handle;
    repeat
      CalcBrushColor(tmp1 + tmp2, tmp3);
      P0 := SizeY - (2 * tmp1);
      P1 := SizeX - (2 * tmp2);
      tmpLeft := tmpRect.Left + tmp2;
      tmpTop := tmpRect.Top + tmp1;
      {$IFDEF CLX}
      {$ELSE}
      PatBlt(FDC, tmpLeft, tmpTop, TeeGradientPrecision, P0, PATCOPY);
      PatBlt(FDC, tmpRect.Right - tmp2 - 1, tmpTop, TeeGradientPrecision, P0, PATCOPY);
      PatBlt(FDC, tmpLeft, tmpTop, P1, TeeGradientPrecision, PATCOPY);
      PatBlt(FDC, tmpLeft, tmpRect.Bottom - tmp1 - TeeGradientPrecision,
        P1, TeeGradientPrecision, PATCOPY);
      {$ENDIF}
      if tmp1 < tmpYCenter then Inc(tmp1, TeeGradientPrecision);
      if tmp2 < tmpXCenter then Inc(tmp2, TeeGradientPrecision);
    until (tmp1 >= tmpYCenter) and (tmp2 >= tmpXCenter);
  end;

begin
  if (Direction = gdTopBottom) or (Direction = gdLeftRight) then
  else
    SwapInteger(Integer(StartColor), Integer(EndColor));

  StartColor := ColorToRGB(StartColor);
  EndColor := ColorToRGB(EndColor);

  T0 := GetRValue(StartColor);
  T1 := GetGValue(StartColor);
  T2 := GetBValue(StartColor);
  D0 := GetRValue(EndColor) - T0;
  D1 := GetGValue(EndColor) - T1;
  D2 := GetBValue(EndColor) - T2;

  tmpRect := Rect;
  with tmpRect do
  begin
    if Right < Left then SwapInteger(Left, Right);
    if Bottom < Top then SwapInteger(Top, Bottom);
  end;

  {$IFDEF CLX}
  tmpBrush := QBrush_create;
  {$ELSE}
  tmpBrush := 0;
  OldColor := -1;
  {$ENDIF}

  case Direction of
    gdLeftRight,
      gdRightLeft: RectGradient(True);
    gdTopBottom,
      gdBottomTop: RectGradient(False);
    gdFromTopLeft,
      gdFromBottomLeft: FromCorner;
    gdFromCenter: FromCenter;
  end;
  {$IFDEF CLX}
  QBrush_destroy(tmpBrush);
  {$ELSE}
  if tmpBrush <> 0 then DeleteObject(SelectObject(FDC, tmpBrush));
  {$ENDIF}
end;

procedure TTeeCanvas3D.EraseBackground(const Rect: TRect);
begin
  FillRect(Rect);
end;

procedure TTeeCanvas3D.FillRect(const Rect: TRect);
begin
  {$IFNDEF CLX}
  Windows.FillRect(FCanvas.Handle, Rect, FBrush.Handle);
  {$ELSE}
  FCanvas.FillRect(Rect);
  {$ENDIF}
end;

procedure TTeeCanvas3D.Frame3D(Rect: TRect; TopColor, BottomColor: TColor;
  Width: Integer);
var
  TopRight          : TPoint;
  BottomLeft        : TPoint;
  P                 : array[0..2] of TPoint;
begin
  FPen.Width := 1;
  Dec(Rect.Bottom);
  Dec(Rect.Right);
  while Width > 0 do
  begin
    Dec(Width);
    with Rect do
    begin
      TopRight.X := Right;
      TopRight.Y := Top;
      BottomLeft.X := Left;
      BottomLeft.Y := Bottom;
      FPen.Color := TopColor;
      P[0] := BottomLeft;
      P[1] := TopLeft;
      P[2] := TopRight;

      {$IFNDEF CLX}
      Windows.Polyline(FCanvas.Handle, PPoints(@P)^, High(P) + 1);
      {$ELSE}
      FCanvas.PolyLine(P);
      {$ENDIF}

      FPen.Color := BottomColor;
      Dec(BottomLeft.X);
      P[0] := TopRight;
      P[1] := BottomRight;
      P[2] := BottomLeft;

      {$IFNDEF CLX}
      Windows.Polyline(FCanvas.Handle, PPoints(@P)^, High(P) + 1);
      {$ELSE}
      FCanvas.PolyLine(P);
      {$ENDIF}

    end;
    InflateRect(Rect, -1, -1);
  end;
end;

function ApplyDark(Color: TColor; HowMuch: Byte): TColor;
var
  r                 : Byte;
  g                 : Byte;
  b                 : Byte;
  {$IFDEF CLX}
  tmp               : QColorH;
  {$ENDIF}
begin
  Color := ColorToRGB(Color);
  r := GetRValue(Color);
  g := GetGValue(Color);
  b := GetBValue(Color);
  if r > HowMuch then
    r := r - HowMuch
  else
    r := 0;
  if g > HowMuch then
    g := g - HowMuch
  else
    g := 0;
  if b > HowMuch then
    b := b - HowMuch
  else
    b := 0;
  {$IFDEF CLX}
  tmp := QColor(Color);
  QColor_setrgb(tmp, r, g, b);
  result := QColorColor(tmp);
  {$ELSE}
  result := RGB(r, g, b);
  {$ENDIF};
end;

function ApplyBright(Color: TColor; HowMuch: Byte): TColor;
var
  r                 : Byte;
  g                 : Byte;
  b                 : Byte;
  {$IFDEF CLX}
  tmp               : QColorH;
  {$ENDIF}
begin
  Color := ColorToRGB(Color);
  r := GetRValue(Color);
  g := GetGValue(Color);
  b := GetBValue(Color);
  if (r + HowMuch) < 256 then
    r := r + HowMuch
  else
    r := 255;
  if (g + HowMuch) < 256 then
    g := g + HowMuch
  else
    g := 255;
  if (b + HowMuch) < 256 then
    b := b + HowMuch
  else
    b := 255;
  {$IFDEF CLX}
  tmp := QColor(Color);
  QColor_setrgb(tmp, r, g, b);
  result := QColorColor(tmp);
  {$ELSE}
  result := RGB(r, g, b);
  {$ENDIF};
end;

procedure TTeeCanvas3D.Cube(Left, Right, Top, Bottom, Z0, Z1: Integer; DarkSides: Boolean);
var
  OldColor          : TColor;
  tmpSolid          : Boolean;

  procedure Dark(Quantity: Byte);
  begin
    if tmpSolid then
      FBrush.Color := ApplyDark(OldColor, Quantity)
    else
      BackColor := ApplyDark(OldColor, Quantity);
  end;

var
  P0, P1, P2, P3    : TPoint;
begin
  Calc3DPoint(P0, Left, Top, z0);
  Calc3DPoint(P1, Right, Top, z0);
  Calc3DPoint(P2, Right, Bottom, z0);
  Calc3DPoint(P3, Right, Top, z1);

  tmpSolid := FBrush.Style = bsSolid;
  if tmpSolid then
    OldColor := FBrush.Color
  else
    OldColor := BackColor;

  IPoints[0] := P0;
  IPoints[1] := P1;
  IPoints[2] := P2;
  Calc3DPoint(IPoints[3], Left, Bottom, z0);

  PolygonFour;

  Calc3DPoint(IPoints[2], Right, Bottom, z1);
  if IPoints[2].x > p2.x then
  begin
    IPoints[0] := P1;
    IPoints[1] := P3;
    IPoints[3] := P2;
    if DarkSides then Dark(DarkerColorQuantity);
    PolygonFour;
  end
  else
  begin
    IPoints[0] := P0;
    Calc3DPoint(IPoints[1], Left, Top, z1);
    Calc3DPoint(IPoints[2], Left, Bottom, z1);
    Calc3DPoint(IPoints[3], Left, Bottom, z0);

    if IPoints[2].x < IPoints[3].x then
    begin
      if DarkSides then Dark(DarkerColorQuantity);
      PolygonFour;
    end;
  end;

  Calc3DPoint(IPoints[3], Left, Top, z1);
  if P1.y > IPoints[3].Y then
  begin
    IPoints[0] := P0;
    IPoints[1] := P1;
    IPoints[2] := P3;
    if DarkSides then Dark(DarkColorQuantity);
    PolygonFour;
  end;

  (* bottom
  Calc3DPoint(IPoints[0],Left,Bottom,z0);
  Calc3DPoint(IPoints[2],Right,Bottom,z1);
  if IPoints[0].Y<=IPoints[2].Y then
  begin
    Calc3DPoint(IPoints[1],Left,Bottom,z1);
    IPoints[3]:=P2;
    PolygonFour;
  end;
  *)
end;

procedure TTeeCanvas3D.RectangleZ(Left, Top, Bottom, Z0, Z1: Integer);
begin
  Calc3DPoint(IPoints[0], Left, Top, Z0);
  Calc3DPoint(IPoints[1], Left, Top, Z1);
  Calc3DPoint(IPoints[2], Left, Bottom, Z1);
  Calc3DPoint(IPoints[3], Left, Bottom, Z0);
  PolygonFour;
end;

procedure TTeeCanvas3D.RectangleY(Left, Top, Right, Z0, Z1: Integer);
begin
  Calc3DPoint(IPoints[0], Left, Top, Z0);
  Calc3DPoint(IPoints[1], Right, Top, Z0);
  Calc3DPoint(IPoints[2], Right, Top, Z1);
  Calc3DPoint(IPoints[3], Left, Top, Z1);
  PolygonFour;
end;

procedure TTeeCanvas3D.FrontPlaneBegin;
begin
  FWas3D := FIs3D;
  FIs3D := False;
end;

procedure TTeeCanvas3D.FrontPlaneEnd;
begin
  FIs3D := FWas3D;
end;

procedure TTeeCanvas3D.Invalidate;
begin
  FDirty := True;
end;

procedure TTeeCanvas3D.RotateLabel3D(x, y, z: Integer; const St: string; RotDegree: Integer);
begin
  Calc3DPos(x, y, z);
  RotateLabel(x, y, St, RotDegree);
end;

procedure TTeeCanvas3D.RotateLabel(x, y: Integer; const St: string; RotDegree: Integer);
{$IFNDEF CLX}
var
  OldFont           : HFONT;
  NewFont           : HFONT;
  LogRec            : TLOGFONT;
  FDC               : HDC;
  {$ENDIF}
begin
  {$IFNDEF CLX}
  if RotDegree > 360 then RotDegree := RotDegree - 360;
  FBrush.Style := bsClear;
  FDC := FCanvas.Handle;
  GetObject(FFont.Handle, SizeOf(LogRec), @LogRec);
  LogRec.lfEscapement := RotDegree * 10;
  LogRec.lfOrientation := RotDegree * 10; { <-- fix, was zero }
  LogRec.lfOutPrecision := OUT_TT_ONLY_PRECIS;
  if IZoomText then
    if IZoomFactor <> 1 then
      LogRec.lfHeight := -MulDiv(MaxLong(1, Round(IZoomFactor * FFont.Size)),
        FFont.PixelsPerInch, 72);
  NewFont := CreateFontIndirect(LogRec);
  OldFont := SelectObject(FDC, NewFont);

  TextOut(x, y, St);
  DeleteObject(SelectObject(FDC, OldFont));
  {$ELSE}
  TextOut(x, y, St);
  {$ENDIF}
end;

procedure TTeeCanvas3D.Line(X0, Y0, X1, Y1: Integer);
{$IFNDEF CLX}
var
  FDC               : HDC;
  {$ENDIF}
begin
  {$IFNDEF CLX}
  FDC := FCanvas.Handle;
  Windows.MoveToEx(FDC, X0, Y0, nil);
  Windows.LineTo(FDC, X1, Y1);
  {$ELSE}
  FCanvas.MoveTo(X0, Y0);
  FCanvas.LineTo(X1, Y1);
  {$ENDIF}
end;

procedure TTeeCanvas3D.LineWithZ(X0, Y0, X1, Y1, Z: Integer);
{$IFNDEF CLX}
var
  FDC               : HDC;
  {$ENDIF}
begin
  Calc3DPos(x0, y0, z);
  Calc3DPos(x1, y1, z);
  {$IFNDEF CLX}
  FDC := FCanvas.Handle;
  Windows.MoveToEx(FDC, X0, Y0, nil);
  Windows.LineTo(FDC, X1, Y1);
  {$ELSE}
  Line(X0, Y0, X1, Y1);
  {$ENDIF}
end;

procedure TTeeCanvas3D.Polygon(const Points: array of TPoint);
begin
  {$IFNDEF CLX}
  Windows.Polygon(FCanvas.Handle, PPoints(@Points)^, High(Points) + 1);
  {$ELSE}
  FCanvas.Polygon(Points);
  {$ENDIF}
end;

procedure TTeeCanvas3D.PolygonWithZ(Points: array of TPoint; Z: Integer);
var
  t                 : Integer;
begin
  for t := 0 to {$IFDEF D4}Length(Points) - 1{$ELSE}High(Points){$ENDIF} do
    Calc3DPoint(Points[t], Points[t].X, Points[t].Y, z);
  Polygon(Points);
end;

procedure TTeeCanvas3D.Ellipse(X1, Y1, X2, Y2: Integer);
begin
  {$IFNDEF CLX}
  Windows.Ellipse(FCanvas.Handle, X1, Y1, X2, Y2);
  {$ELSE}
  FCanvas.Ellipse(X1, Y1, X2, Y2);
  {$ENDIF}
end;

procedure TTeeCanvas3D.EllipseWithZ(X1, Y1, X2, Y2, Z: Integer);
const
  PiStep            = Pi * 0.1;
  NumCirclePoints   = 32;
var
  P                 : array[0..NumCirclePoints - 1] of TPoint;
  Points            : array[0..2] of TPoint;
  PCenter           : TPoint;
  t                 : Integer;
  XRadius           : Integer;
  YRadius           : Integer;
  tmpSin            : Extended;
  tmpCos            : Extended;
  Old               : TPenStyle;
  {$IFNDEF CLX}
  FDC               : HDC;
  {$ENDIF}
begin
  {$IFNDEF CLX}
  FDC := FCanvas.Handle;
  {$ENDIF}
  if FIsOrthogonal then
  begin
    Calc3DPos(X1, Y1, Z);
    Calc3DPos(X2, Y2, Z);
    {$IFNDEF CLX}
    Windows.Ellipse(FDC, X1, Y1, X2, Y2);
    {$ELSE}
    FCanvas.Ellipse(X1, Y1, X2, Y2);
    {$ENDIF}
  end
  else
    if FIs3D then
    begin
      PCenter.X := (X2 + X1) div 2;
      PCenter.Y := (Y2 + Y1) div 2;
      XRadius := (X2 - X1) div 2;
      YRadius := (Y2 - Y1) div 2;
      Calc3DPoint(P[0], PCenter.X, Y2, Z);
      for t := 1 to NumCirclePoints - 1 do
      begin
        SinCos(t * piStep, tmpSin, tmpCos);
        Calc3DPoint(P[t], PCenter.X + Trunc(XRadius * tmpSin), PCenter.Y + Trunc(YRadius * tmpCos), Z);
      end;
      if FBrush.Style <> bsClear then
      begin
        Old := FPen.Style;
        FPen.Style := psClear;
        {$IFNDEF CLX}
        FDC := FCanvas.Handle;
        {$ENDIF}
        Calc3DTPoint(PCenter, Z);
        Points[0] := PCenter;
        Points[1] := P[0];
        Points[2] := P[1];
        {$IFNDEF CLX}
        Windows.Polygon(FDC, PPoints(@Points)^, 3);
        {$ELSE}
        FCanvas.Polygon(Points);
        {$ENDIF}
        Points[1] := P[1];
        for t := 2 to NumCirclePoints - 1 do
        begin
          Points[2] := P[t];
          {$IFNDEF CLX}
          Windows.Polygon(FDC, PPoints(@Points)^, 3);
          {$ELSE}
          FCanvas.Polygon(Points);
          {$ENDIF}
          Points[1] := P[t];
        end;
        FPen.Style := Old;
      end;
      if FPen.Style <> psClear then
        {$IFNDEF CLX}
        Windows.Polyline(FCanvas.Handle, PPoints(@P)^, High(P) + 1);
      {$ELSE}
        FCanvas.PolyLine(P);
      {$ENDIF}
    end
    else
      {$IFNDEF CLX}
      Windows.Ellipse(FCanvas.Handle, X1, Y1, X2 + 1, Y2 + 1);
  {$ELSE}
      FCanvas.Ellipse(X1, Y1, X2 + 1, Y2 + 1);
  {$ENDIF}
end;

procedure TTeeCanvas3D.SetPixel(X, Y: Integer; Value: TColor);
begin
  {$IFNDEF CLX}
  Windows.SetPixel(FCanvas.Handle, X, Y, ColorToRGB(Value));
  {$ELSE}
  QPainter_drawPoint(Handle, x, y);
  {$ENDIF}
end;

procedure TTeeCanvas3D.Arc(X1, Y1, X2, Y2, X3, Y3, X4, Y4: Integer);
begin
  {$IFNDEF CLX}
  Windows.Arc(FCanvas.Handle, X1, Y1, X2, Y2, X3, Y3, X4, Y4);
  {$ELSE}
  FCanvas.Arc(X1, Y1, X2, Y2, X3, Y3, X4, Y4);
  {$ENDIF}
end;

procedure TTeeCanvas3D.Donut(XCenter, YCenter, XRadius, YRadius: Integer;
  const StartAngle, EndAngle, HolePercent: Double);
const
  MaxCircleSteps    = 128;
var
  Points            : array[0..2 * MaxCircleSteps - 1] of TPoint;
  t,
    CircleSteps,
    tmp             : Integer;
  tmpXRadius,
    tmpYRadius,
    Step            : Double;
  tmpAngle,
    tmpSin,
    tmpCos          : Extended;
begin
  tmpXRadius := HolePercent * XRadius / 100.0;
  tmpYRadius := HolePercent * YRadius / 100.0;

  CircleSteps := Round(MaxCircleSteps * (EndAngle - StartAngle) / Pi);
  if CircleSteps < 2 then
    CircleSteps := 2
  else
    if CircleSteps > MaxCircleSteps then CircleSteps := MaxCircleSteps;

  Step := (EndAngle - StartAngle) / Pred(CircleSteps);
  tmpAngle := StartAngle;
  for t := 1 to CircleSteps do
  begin
    SinCos(tmpAngle, tmpSin, tmpCos);
    Points[t].X := XCenter + Round(XRadius * tmpCos);
    Points[t].Y := YCenter - Round(YRadius * tmpSin);
    if t = 1 then
      tmp := 0
    else
      tmp := 2 * CircleSteps - t + 1;
    Points[tmp].X := XCenter + Round(tmpXRadius * tmpCos);
    Points[tmp].Y := YCenter - Round(tmpYRadius * tmpSin);
    tmpAngle := tmpAngle + Step;
  end;

  {$IFNDEF CLX}
  Windows.Polygon(FCanvas.Handle, PPoints(@Points)^, 2 * CircleSteps - 1);
  {$ELSE}
  FCanvas.Polygon(Points, False, 0, 2 * CircleSteps - 1);
  {$ENDIF}
end;

procedure TTeeCanvas3D.Pie(X1, Y1, X2, Y2, X3, Y3, X4, Y4: Integer);
begin
  {$IFNDEF CLX}
  Windows.Pie(FCanvas.Handle, X1, Y1, X2, Y2, X3, Y3, X4, Y4);
  {$ELSE}
  FCanvas.Pie(X1, Y1, X2, Y2, X3, Y3, X4, Y4);
  {$ENDIF}
end;

procedure TTeeCanvas3D.Pie3D(XCenter, YCenter, XRadius, YRadius, Z0, Z1: Integer;
  const StartAngle, EndAngle: Double;
  DarkSides, DrawSides: Boolean);
var
  OldColor          : TColor;

  procedure Dark(Quantity: Byte);
  begin
    if FBrush.Style = bsSolid then
      FBrush.Color := ApplyDark(OldColor, Quantity)
    else
      BackColor := ApplyDark(OldColor, Quantity);
  end;

const
  MaxCircleSteps    = 32;
var
  Points            : array[0..MaxCircleSteps] of TPoint;
  Points3D          : array[1..2 * MaxCircleSteps] of TPoint;
  Start3D,
    End3D,
    CircleSteps     : Integer;

  procedure Draw3DPie;
  var
    t, tt           : Integer;
  begin
    if DarkSides then Dark(32);
    if (Start3D = 1) and (End3D = CircleSteps) then
    begin
      for t := 1 to CircleSteps do
        Points3D[t] := Points[t];
      tt := CircleSteps;
    end
    else
    begin
      tt := 0;
      for t := Start3D to End3D do
      begin
        inc(tt);
        Points3D[tt] := Points[t];
        Points3D[End3D - Start3D + 1 + tt] := Points3D[2 * CircleSteps - End3D + tt];
      end;
    end;
    {$IFNDEF CLX}
    Windows.Polygon(FCanvas.Handle, PPoints(@Points3D)^, 2 * tt);
    {$ELSE}
    FCanvas.Polygon(Points3D, False, 0, 2 * tt);
    {$ENDIF}
  end;

var
  tmpAngle,
    tmpSin,
    tmpCos          : Extended;
  Step              : Double;
  Started,
    Ended           : Boolean;
  t,
    tt,
    tmpX,
    tmpY            : Integer;
begin
  CircleSteps := 2 + MinLong(MaxCircleSteps - 2, Round(180.0 * Abs(EndAngle - StartAngle) / Pi / 10.0));
  //  CircleSteps:=MaxCircleSteps;
  Calc3DPoint(Points[0], XCenter, YCenter, Z1);
  Step := (EndAngle - StartAngle) / (CircleSteps - 1);
  tmpAngle := StartAngle;
  for t := 1 to CircleSteps do
  begin
    SinCos(tmpAngle, tmpSin, tmpCos);
    tmpX := XCenter + Round(XRadius * tmpCos);
    tmpY := YCenter - Round(YRadius * tmpSin);
    Calc3DPoint(Points[t], tmpX, tmpY, Z1);
    Calc3DPoint(Points3D[2 * CircleSteps + 1 - t], tmpX, tmpY, Z0);
    tmpAngle := tmpAngle + Step;
  end;

  if FBrush.Style = bsSolid then
    OldColor := FBrush.Color
  else
    OldColor := BackColor;

  { side }
  if DrawSides then
  begin
    if Points[CircleSteps].X < XCenter then
    begin
      IPoints[0] := Points[0];
      IPoints[1] := Points[CircleSteps];
      IPoints[2] := Points3D[CircleSteps + 1];
      Calc3DPoint(IPoints[3], XCenter, YCenter, Z0);
      if DarkSides then Dark(32);
      PolygonFour;
    end;

    { side }
    if Points[1].X > XCenter then
    begin
      IPoints[0] := Points[0];
      IPoints[1] := Points[1];
      IPoints[2] := Points3D[2 * CircleSteps];
      Calc3DPoint(IPoints[3], XCenter, YCenter, Z0);
      if DarkSides then Dark(32);
      PolygonFour;
    end;
  end;

  { 2d pie }
  if FBrush.Style = bsSolid then
    FBrush.Color := OldColor
  else
    BackColor := OldColor;
  {$IFNDEF CLX}
  Windows.Polygon(FCanvas.Handle, PPoints(@Points)^, CircleSteps + 1);
  {$ELSE}
  FCanvas.Polygon(Points, False, 0, CircleSteps + 1);
  {$ENDIF}

  { 3d pie }
  Ended := False;
  Start3D := 0;
  End3D := 0;
  for t := 2 to CircleSteps do
  begin
    if Points[t].X > Points[t - 1].X then
    begin
      Start3D := t - 1;
      Started := True;
      for tt := t + 1 to CircleSteps - 1 do
        if Points[tt + 1].X < Points[tt].X then
        begin
          End3D := tt;
          Ended := True;
          Break;
        end;
      if (not Ended) and (Points[CircleSteps].X >= Points[CircleSteps - 1].X) then
      begin
        End3D := CircleSteps;
        Ended := True;
      end;
      if Started and Ended then Draw3DPie;
      if End3D <> CircleSteps then
        if Points[CircleSteps].X > Points[CircleSteps - 1].X then
        begin
          End3D := CircleSteps;
          tt := CircleSteps - 1;
          while (Points[tt].X > Points[tt - 1].X) do
          begin
            Dec(tt);
            if tt = 1 then break;
          end;
          if tt > 1 then
          begin
            Start3D := tt;
            Draw3DPie;
          end;
        end;
      Break;
    end;
  end;
end;

procedure TTeeCanvas3D.Plane3D(const A, B: TPoint; Z0, Z1: Integer);
begin
  Calc3DPoint(IPoints[0], A.X, A.Y, Z0);
  Calc3DPoint(IPoints[1], B.X, B.Y, Z0);
  Calc3DPoint(IPoints[2], B.X, B.Y, Z1);
  Calc3DPoint(IPoints[3], A.X, A.Y, Z1);
  PolygonFour;
end;

procedure TTeeCanvas3D.InternalCylinder(Vertical: Boolean; Left, Top, Right, Bottom,
  Z0, Z1: Integer; Dark3D: Boolean; ConePercent: Integer);
const
  NumCylinderSides  = 16;
  Step              = 2.0 * pi / NumCylinderSides;
  StepColor         = 256 div NumCylinderSides;

var
  OldColor          : TColor;

  procedure Dark(Quantity: Byte);
  begin
    if FBrush.Style = bsSolid then
      FBrush.Color := ApplyDark(OldColor, Quantity)
    else
      BackColor := ApplyDark(OldColor, Quantity)
  end;

var
  tmpSize,
    tmpMid,
    tmpMidZ,
    Radius,
    ZRadius,
    t,
    NumSide         : Integer;
  Poly              : array[1..NumCylinderSides] of TPoint3D;
  tmpPoly           : array[1..NumCylinderSides] of TPoint;
  tmpSin            : Extended;
  tmpCos            : Extended;
  tmp               : Boolean;
begin
  if ConePercent = 0 then ConePercent := TeeDefaultConePercent;
  if FBrush.Style = bsSolid then
    OldColor := FBrush.Color
  else
    OldColor := BackColor;
  ZRadius := (Z1 - Z0) div 2;
  tmpMidZ := (Z1 + Z0) div 2;
  if Vertical then
  begin
    Radius := (Right - Left) shr 1;
    tmpMid := (Right + Left) shr 1;
    tmpSize := Abs(Bottom - Top);

    for t := 1 to NumCylinderSides do
    begin
      SinCos((t - 4) * Step, tmpSin, tmpCos);
      Poly[t].X := tmpMid + Round(tmpSin * Radius);
      if Top < Bottom then
        Poly[t].Y := Top
      else
        Poly[t].Y := Bottom;
      Poly[t].Z := tmpMidZ - Round(tmpCos * ZRadius);
    end;
    Radius := Round(Radius * ConePercent / 100.0);
    ZRadius := Round(ZRadius * ConePercent / 100.0);
    with Poly[1] do
    begin
      Calc3DPoint(tmpPoly[2], x, y + tmpSize, z);
      SinCos((1 - 4) * Step, tmpSin, tmpCos);
      X := tmpMid + Round(tmpSin * Radius);
      Z := tmpMidZ - Round(tmpCos * ZRadius);
    end;
    tmpPoly[1] := Calc3DTPoint3D(Poly[1]);
    NumSide := 0;
    for t := 2 to NumCylinderSides do
    begin
      with Poly[t] do
      begin
        Calc3DPoint(tmpPoly[3], x, y + tmpSize, z);
        SinCos((t - 4) * Step, tmpSin, tmpCos);
        X := tmpMid + Round(tmpSin * Radius);
        Z := tmpMidZ - Round(tmpCos * ZRadius);
      end;
      tmpPoly[4] := Calc3DTPoint3D(Poly[t]);
      tmp := (tmpPoly[1].x - tmpPoly[3].x + tmpPoly[2].x - tmpPoly[4].x) < 0;
      if tmp then
      begin
        if Dark3D then Dark(StepColor * NumSide);
        {$IFNDEF CLX}
        Windows.Polygon(FCanvas.Handle, PPoints(@tmpPoly)^, 4);
        {$ELSE}
        FCanvas.Polygon(tmpPoly);
        {$ENDIF}
      end;
      tmpPoly[1] := tmpPoly[4];
      tmpPoly[2] := tmpPoly[3];
      Inc(NumSide);
    end;
  end
  else
  begin
    Radius := (Bottom - Top) shr 1;
    tmpMid := (Bottom + Top) shr 1;

    for t := 1 to NumCylinderSides do
    begin
      SinCos((t - 5) * Step, tmpSin, tmpCos);
      if Left < Right then
        Poly[t].X := Right
      else
        Poly[t].X := Left;
      Poly[t].Y := tmpMid + Round(tmpSin * Radius);
      Poly[t].Z := tmpMidZ - Round(tmpCos * ZRadius);
    end;

    Radius := Round(Radius * ConePercent / 100.0);
    ZRadius := Round(ZRadius * ConePercent / 100.0);
    tmpSize := Abs(Right - Left);

    with Poly[1] do
    begin
      Calc3DPoint(tmpPoly[2], x - tmpSize, y, z);
      SinCos(-4 * Step, tmpSin, tmpCos);
      Y := tmpMid + Round(tmpSin * Radius);
      Z := tmpMidZ - Round(tmpCos * ZRadius);
    end;
    tmpPoly[1] := Calc3DTPoint3D(Poly[1]);
    NumSide := 0;

    for t := 2 to NumCylinderSides do
    begin
      with Poly[t] do
      begin
        Calc3DPoint(tmpPoly[3], x - tmpSize, y, z);
        SinCos((t - 5) * Step, tmpSin, tmpCos);
        Y := tmpMid + Round(tmpSin * Radius);
        Z := tmpMidZ - Round(tmpCos * ZRadius);
      end;
      tmpPoly[4] := Calc3DTPoint3D(Poly[t]);
      tmp := (tmpPoly[1].y - tmpPoly[3].y + tmpPoly[2].y - tmpPoly[4].y) < 0;
      if tmp then
      begin
        if Dark3D then Dark(StepColor * NumSide);
        {$IFNDEF CLX}
        Windows.Polygon(FCanvas.Handle, PPoints(@tmpPoly)^, 4);
        {$ELSE}
        FCanvas.Polygon(tmpPoly);
        {$ENDIF}
      end;
      tmpPoly[1] := tmpPoly[4];
      tmpPoly[2] := tmpPoly[3];
      Inc(NumSide);
    end;
  end;
  for t := 1 to NumCylinderSides do
    tmpPoly[t] := Calc3DTPoint3D(Poly[t]);
  if Dark3D then Dark(DarkColorQuantity);
  {$IFNDEF CLX}
  Windows.Polygon(FCanvas.Handle, PPoints(@tmpPoly)^, NumCylinderSides);
  {$ELSE}
  FCanvas.Polygon(tmpPoly);
  {$ENDIF}
end;

procedure TTeeCanvas3D.Cylinder(Vertical: Boolean; Left, Top, Right, Bottom, Z0, Z1: Integer; Dark3D: Boolean);
begin
  InternalCylinder(Vertical, Left, Top, Right, Bottom, Z0, Z1, Dark3D, 100);
end;

procedure TTeeCanvas3D.Pyramid(Vertical: Boolean; Left, Top, Right, Bottom, z0, z1: Integer; DarkSides: Boolean);
var
  OldColor          : TColor;

  procedure Dark(Quantity: Byte);
  begin
    if FBrush.Style = bsSolid then
      FBrush.Color := ApplyDark(OldColor, Quantity)
    else
      BackColor := ApplyDark(OldColor, Quantity)
  end;

var
  P0, P1, P2, P3, PTop: TPoint;
begin
  if FBrush.Style = bsSolid then
    OldColor := FBrush.Color
  else
    OldColor := BackColor;
  if Vertical then
  begin
    if Top <> Bottom then
    begin
      Calc3DPoint(P0, Left, Bottom, Z0);
      Calc3DPoint(P1, Right, Bottom, Z0);
      Calc3DPoint(PTop, (Left + Right) div 2, Top, (Z0 + Z1) div Integer(2));
      Polygon([P0, PTop, P1]);
      if Top < Bottom then
      begin
        Calc3DPoint(P2, Left, Bottom, Z1);
        if P2.Y < PTop.Y then Polygon([P0, PTop, P2]);
      end;
      if DarkSides then Dark(DarkerColorQuantity);
      Calc3DPoint(P3, Right, Bottom, Z1);
      Polygon([P1, PTop, P3]);
      if (Top < Bottom) and (P2.Y < PTop.Y) then
      begin
        Calc3DPoint(P2, Left, Bottom, Z1);
        Polygon([PTop, P2, P3]);
      end;
    end;
    if Top >= Bottom then
    begin
      if DarkSides then Dark(DarkColorQuantity);
      RectangleY(Left, Bottom, Right, Z0, Z1);
    end;
  end
  else
  begin
    if Left <> Right then
    begin
      Calc3DPoint(P0, Left, Top, Z0);
      Calc3DPoint(P1, Left, Bottom, Z0);
      Calc3DPoint(PTop, Right, (Top + Bottom) shr 1, (Z0 + Z1) shr 1);
      Polygon([P0, PTop, P1]);
      if DarkSides then Dark(DarkColorQuantity);
      Calc3DPoint(P2, Left, Top, Z1);
      Polygon([P0, PTop, P2]);
    end;
    if Left >= Right then
    begin
      if DarkSides then Dark(DarkerColorQuantity);
      RectangleZ(Left, Top, Bottom, Z0, Z1);
    end;
  end;
end;

procedure TTeeCanvas3D.PyramidTrunc(const R: TRect; StartZ, EndZ: Integer;
  TruncX, TruncZ: Integer);
var
  MidX              : Integer;
  MidZ              : Integer;

  procedure PyramidFrontWall(StartZ, EndZ: Integer);
  var
    P               : TFourPoints;
  begin
    P[0].X := MidX - TruncX;
    P[0].Y := R.Top;
    P[1].X := MidX + TruncX;
    P[1].Y := R.Top;
    P[2].X := R.Right;
    P[2].Y := R.Bottom;
    P[3].X := R.Left;
    P[3].Y := R.Bottom;
    PlaneFour3D(P, StartZ, EndZ);
  end;

  procedure PyramidSideWall(HorizPos1, HorizPos2, StartZ, EndZ: Integer);
  begin
    IPoints[0] := Calculate3DPosition(HorizPos2, R.Top, MidZ - TruncZ);
    IPoints[1] := Calculate3DPosition(HorizPos2, R.Top, MidZ + TruncZ);
    IPoints[2] := Calculate3DPosition(HorizPos1, R.Bottom, EndZ);
    IPoints[3] := Calculate3DPosition(HorizPos1, R.Bottom, StartZ);
    PolygonFour;
  end;

  procedure BottomCover;
  begin
    RectangleY(R.Left, R.Bottom, R.Right, StartZ, EndZ)
  end;

  procedure TopCover;
  begin
    if TruncX <> 0 then
      RectangleY(MidX - TruncX, R.Top, MidX + TruncX, MidZ - TruncZ, MidZ + TruncZ);
  end;

begin
  MidX := (R.Left + R.Right) div 2;
  MidZ := (StartZ + EndZ) div 2;
  if R.Bottom > R.Top then
    BottomCover
  else
    TopCover;
  PyramidFrontWall(MidZ + TruncZ, EndZ);
  PyramidSideWall(R.Left, MidX - TruncX, StartZ, EndZ);
  PyramidFrontWall(MidZ - TruncZ, StartZ);
  PyramidSideWall(R.Right, MidX + TruncX, StartZ, EndZ);
  if R.Bottom > R.Top then
    TopCover
  else
    BottomCover;
end;

procedure TTeeCanvas3D.PlaneFour3D(var Points: TFourPoints; Z0, Z1: Integer);
begin
  IPoints := Points;
  Calc3DTPoint(IPoints[0], z0);
  Calc3DTPoint(IPoints[1], z0);
  Calc3DTPoint(IPoints[2], z1);
  Calc3DTPoint(IPoints[3], z1);
  PolygonFour;
end;

procedure TTeeCanvas3D.SetPixel3D(X, Y, Z: Integer; Value: TColor);
{$IFNDEF CLX}
var
  FDC               : HDC;
  {$ENDIF}
begin
  Calc3DPos(X, Y, Z);
  if FPen.Width = 1 then
    SetPixel(X, Y, Value)
  else
  begin                                 { simulate a big dot pixel }
    {$IFNDEF CLX}
    FDC := FCanvas.Handle;
    Windows.MoveToEx(FDC, X, Y, nil);
    Windows.LineTo(FDC, X, Y);
    {$ELSE}
    Pixels[X, Y] := Value;
    {$ENDIF}
  end;
end;

function TTeeCanvas3D.GetSupports3DText: Boolean;
begin
  result := False;
end;

function TTeeCanvas3D.GetSupportsFullRotation: Boolean;
begin
  result := False;
end;

function TTeeCanvas3D.GetTextAlign: TCanvasTextAlign;
begin
  {$IFNDEF CLX}
  result := Windows.GetTextAlign(FCanvas.Handle);
  {$ELSE}
  result := FTextAlign;
  {$ENDIF}
end;

function TTeeCanvas3D.GetUseBuffer: Boolean;
begin
  result := FBufferedDisplay;
end;

procedure TTeeCanvas3D.SetUseBuffer(Value: Boolean);
begin
  FBufferedDisplay := Value;
  if (not FBufferedDisplay) and Assigned(FBitmap) then
  begin
    DeleteBitmap;
    FDirty := True;
  end;
end;

function TTeeCanvas3D.GetMonochrome: Boolean;
begin
  result := FMonochrome;
end;

procedure TTeeCanvas3D.SetMonochrome(Value: Boolean);
begin
  if FMonochrome <> Value then
  begin
    FMonochrome := Value;
    FDirty := True;
    if Assigned(F3DOptions) then F3DOptions.Repaint;
  end;
end;

procedure TTeeCanvas3D.HorizLine3D(Left, Right, Y, Z: Integer);
var
  {$IFNDEF CLX}
  FDC               : HDC;
  {$ENDIF}
  tmpY              : Integer;
begin
  tmpY := Y;
  Calc3DPos(Left, tmpY, Z);
  {$IFNDEF CLX}
  FDC := FCanvas.Handle;
  Windows.MoveToEx(FDC, Left, tmpY, nil);
  {$ELSE}
  FCanvas.MoveTo(Left, tmpY);
  {$ENDIF}
  tmpY := Y;
  Calc3DPos(Right, tmpY, Z);
  {$IFNDEF CLX}
  Windows.LineTo(FDC, Right, tmpY);
  {$ELSE}
  FCanvas.LineTo(Right, tmpY);
  {$ENDIF}
end;

procedure TTeeCanvas3D.VertLine3D(X, Top, Bottom, Z: Integer);
var
  {$IFNDEF CLX}
  FDC               : HDC;
  {$ENDIF}
  tmpX              : Integer;
begin
  tmpX := X;
  Calc3DPos(tmpX, Top, Z);
  {$IFNDEF CLX}
  FDC := FCanvas.Handle;
  Windows.MoveToEx(FDC, tmpX, Top, nil);
  {$ELSE}
  FCanvas.MoveTo(tmpX, Top);
  {$ENDIF}
  tmpX := X;
  Calc3DPos(tmpX, Bottom, Z);
  {$IFNDEF CLX}
  Windows.LineTo(FDC, tmpX, Bottom);
  {$ELSE}
  FCanvas.LineTo(tmpX, Bottom);
  {$ENDIF}
end;

procedure TTeeCanvas3D.ZLine3D(X, Y, Z0, Z1: Integer);
var
  {$IFNDEF CLX}
  FDC               : HDC;
  {$ENDIF}
  tmpX              : Integer;
  tmpY              : Integer;
begin
  tmpX := X;
  tmpY := Y;
  Calc3DPos(tmpX, tmpY, Z0);
  {$IFNDEF CLX}
  FDC := FCanvas.Handle;
  Windows.MoveToEx(FDC, tmpX, tmpY, nil);
  {$ELSE}
  FCanvas.MoveTo(tmpX, tmpY);
  {$ENDIF}
  tmpX := X;
  tmpY := Y;
  Calc3DPos(tmpX, tmpY, Z1);
  {$IFNDEF CLX}
  Windows.LineTo(FDC, tmpX, tmpY);
  {$ELSE}
  FCanvas.LineTo(tmpX, tmpY);
  {$ENDIF}
end;

procedure TTeeCanvas3D.Triangle3D(const Points: TTrianglePoints3D;
  const Colors: TTriangleColors3D);
var
  P                 : array[0..2] of TPoint;
begin
  P[0] := Calc3DTPoint3D(Points[0]);
  P[1] := Calc3DTPoint3D(Points[1]);
  P[2] := Calc3DTPoint3D(Points[2]);
  if Brush.Style <> bsClear then Brush.Color := Colors[0];
  {$IFNDEF CLX}
  Windows.Polygon(FCanvas.Handle, PPoints(@P)^, 3);
  {$ELSE}
  FCanvas.Polygon(P);
  {$ENDIF}
end;

procedure TTeeCanvas3D.TriangleWithZ(const P1, P2, P3: TPoint; Z: Integer);
var
  Points            : array[0..2] of TPoint;
begin
  Calc3DPoint(Points[0], P1.X, P1.Y, Z);
  Calc3DPoint(Points[1], P2.X, P2.Y, Z);
  Calc3DPoint(Points[2], P3.X, P3.Y, Z);
  {$IFNDEF CLX}
  Windows.Polygon(FCanvas.Handle, PPoints(@Points)^, 3);
  {$ELSE}
  FCanvas.Polygon(Points);
  {$ENDIF}
end;

procedure TTeeCanvas3D.Arrow(Filled: Boolean;
  const FromPoint, ToPoint: TPoint;
  ArrowWidth, ArrowHeight, Z: Integer);
var
  x                 : Double;
  y                 : Double;
  SinA              : Double;
  CosA              : Double;

  function CalcArrowPoint: TPoint;
  begin
    result.X := Round(x * CosA + y * SinA);
    result.Y := Round(-x * SinA + y * CosA);
    Calc3DTPoint(result, Z);
  end;

var
  tmpHoriz          : Integer;
  tmpVert           : Integer;
  dx                : Integer;
  dy                : Integer;
  tmpHoriz4         : Double;
  xb                : Double;
  yb                : Double;
  l                 : Double;

  { These are the Arrows points coordinates }
  To3D, pc, pd, pe, pf, pg, ph: TPoint;

  (*           pc
                 |\
  ph           pf| \
    |------------   \ ToPoint
From |------------   /
  pg           pe| /
                 |/
               pd
  *)
begin
  dx := ToPoint.x - FromPoint.x;
  dy := FromPoint.y - ToPoint.y;
  l := Sqrt(1.0 * dx * dx + 1.0 * dy * dy);
  if l > 0 then                         { if at least one pixel... }
  begin
    tmpHoriz := ArrowWidth;
    tmpVert := MinLong(Round(l), ArrowHeight);
    SinA := dy / l;
    CosA := dx / l;
    xb := ToPoint.x * CosA - ToPoint.y * SinA;
    yb := ToPoint.x * SinA + ToPoint.y * CosA;
    x := xb - tmpVert;
    y := yb - tmpHoriz / 2;
    pc := CalcArrowPoint;
    y := yb + tmpHoriz / 2;
    pd := CalcArrowPoint;
    if Filled then
    begin
      tmpHoriz4 := tmpHoriz / 4;
      y := yb - tmpHoriz4;
      pe := CalcArrowPoint;
      y := yb + tmpHoriz4;
      pf := CalcArrowPoint;
      x := FromPoint.x * cosa - FromPoint.y * sina;
      y := yb - tmpHoriz4;
      pg := CalcArrowPoint;
      y := yb + tmpHoriz4;
      ph := CalcArrowPoint;
      To3D := ToPoint;
      Calc3DTPoint(To3D, Z);
      Polygon([ph, pg, pe, pc, To3D, pd, pf]);
    end
    else
    begin
      MoveTo3D(FromPoint.x, FromPoint.y, z);
      LineTo3D(ToPoint.x, ToPoint.y, z);
      LineTo3D(pd.x, pd.y, z);
      MoveTo3D(ToPoint.x, ToPoint.y, z);
      LineTo3D(pc.x, pc.y, z);
    end;
  end;
end;

{ TTeeShadow }

constructor TTeeShadow.Create(AOnChange: TNotifyEvent);
begin
  inherited Create;
  IOnChange := AOnChange;
  FColor := clSilver;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}

constructor TTeeShadow.Load(var S: TOldStream; AOnChange: TNotifyEvent);
var
  smlValue          : SmallInt;
begin
  inherited Create;
  IOnChange := AOnChange;
  S.Read(FColor, SizeOf(TColor));
  S.Read(smlValue, SizeOf(SmallInt));
  FHorizSize := smlValue;
  S.Read(smlValue, SizeOf(SmallInt));
  FVertSize := smlValue;
end;

procedure TTeeShadow.Store(var S: TOldStream);
var
  smlValue          : SmallInt;
begin
  S.Write(FColor, SizeOf(TColor));
  // Pack values (this method slightly limits the maximum value of FHorizSize and FVertSize)
  smlValue := FHorizSize;
  S.Write(smlValue, SizeOf(SmallInt));
  smlValue := FVertSize;
  S.Write(smlValue, SizeOf(SmallInt));
end;

function TTeeShadow.OnCompareItems(Item: TPersistent): Boolean;
begin
  Result := FALSE;
  if Item is TTeeShadow then
    with TTeeShadow(Item) do
      if Self.FColor = FColor then
        if Self.FHorizSize = FHorizSize then
          Result := Self.FVertSize = FVertSize;
end;

function TTeeShadow.CreateDeterminant(Determinant: string): string;
var
  Size              : Integer;
  smlValue          : SmallInt;
begin
  Size := Length(Determinant);
  SetLength(Result, Size + SizeOf(TColor) + 2 * SizeOf(SmallInt));
  CopyMemory(@Result[1], @Determinant[1], Size);
  Inc(Size);

  CopyMemory(@Result[Size], @FColor, SizeOf(TColor));
  Inc(Size, SizeOf(TColor));

  // Pack values (this method slightly limits the maximum value of FHorizSize and FVertSize)
  smlValue := FHorizSize;
  CopyMemory(@Result[Size], @smlValue, SizeOf(SmallInt));
  Inc(Size, SizeOf(SmallInt));
  smlValue := FVertSize;
  CopyMemory(@Result[Size], @smlValue, SizeOf(SmallInt));
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

procedure TTeeShadow.Assign(Source: TPersistent);
begin
  if Source is TTeeShadow then
    with TTeeShadow(Source) do
    begin
      Self.FColor := Color;
      Self.FHorizSize := HorizSize;
      Self.FVertSize := VertSize;
    end
  else
    inherited;
end;

procedure TTeeShadow.SetIntegerProperty(var Variable: Integer; const Value: Integer);
begin
  if Variable <> Value then
  begin
    Variable := Value;
    if Assigned(IOnChange) then IOnChange(Self);
  end;
end;

procedure TTeeShadow.SetColor(Value: TColor);
begin
  SetIntegerProperty(Integer(FColor), Value);
end;

procedure TTeeShadow.SetHorizSize(Value: Integer);
begin
  SetIntegerProperty(FHorizSize, Value);
end;

procedure TTeeShadow.SetVertSize(Value: Integer);
begin
  SetIntegerProperty(FVertSize, Value);
end;

{ TTeeFont }

constructor TTeeFont.Create(ChangedEvent: TNotifyEvent);
begin
  inherited Create;
  Color := clBlack;
  IDefColor := clBlack;
  FShadow := TTeeShadow.Create(ChangedEvent);
  FOutLine := TChartHiddenPen.Create(ChangedEvent);
  OnChange := ChangedEvent;
  CharSet := DEFAULT_CHARSET;
  {$IFNDEF CLX}
  Name := GetDefaultFontName;
  Size := GetDefaultFontSize;
  {$ENDIF}
end;

destructor TTeeFont.Destroy;
begin
  FOutLine.Free;
  FShadow.Free;
  if Assigned(ICanvas) and (ICanvas.IFont = Self) then
    ICanvas.IFont := nil;
  inherited;
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}

constructor TTeeFont.Load(var S: TOldStream; ChangedEvent: TNotifyEvent);
var
  tmpColor          : TColor;
  tmpCharset        : TFontCharset;
  tmpStyle          : TFontStyles;
  smlValue          : SmallInt;
begin
  inherited Create;
  Name := S.ReadString;
  S.Read(tmpCharset, SizeOf(TFontCharset));
  CharSet := tmpCharset;
  S.Read(tmpStyle, SizeOf(TFontStyles));
  Style := tmpStyle;
  S.Read(IDefStyle, SizeOf(TFontStyles));
  S.Read(smlValue, SizeOf(SmallInt));
  Height := smlValue;
  S.Read(FInterCharSize, SizeOf(FInterCharSize));
  S.Read(tmpColor, SizeOf(TColor));
  Color := tmpColor;
  S.Read(IDefColor, SizeOf(TColor));

  FShadow := TTeeShadow.Load(S, ChangedEvent);
  FOutLine := TChartHiddenPen.Load(S, ChangedEvent);
  OnChange := ChangedEvent;
end;

procedure TTeeFont.Store(var S: TOldStream);
var
  tmpCharset        : TFontCharset;
  tmpStyle          : TFontStyles;
  smlValue          : SmallInt;
begin
  S.WriteString(Name);
  tmpCharset := CharSet;
  S.Write(tmpCharset, SizeOf(TFontCharset));
  tmpStyle := Style;
  S.Write(tmpStyle, SizeOf(TFontStyles));
  S.Write(IDefStyle, SizeOf(TFontStyles));
  // Pack values (this method slightly limits the maximum value of Height)
  smlValue := Height;
  S.Write(smlValue, SizeOf(SmallInt));
  S.Write(FInterCharSize, SizeOf(FInterCharSize));
  S.Write(Color, SizeOf(TColor));
  S.Write(IDefColor, SizeOf(TColor));

  FShadow.Store(S);
  FOutLine.Store(S);
end;

function TTeeFont.OnCompareItems(Item: TPersistent): Boolean;
var
  Difference        : Integer;
  PermisDiff        : Integer;
begin
  Result := FALSE;
  if Item is TTeeFont then
    with TTeeFont(Item) do
      if Self.Name = Name then
        if Self.CharSet = CharSet then
          if Self.Style = Style then
          begin
            Difference := Abs(Self.Height - Height);
            if Difference > 1 then
            begin
              PermisDiff := Round(Self.Height * co_PermisPercent);
              if Difference > PermisDiff then
                Exit;
            end;
            if Self.Color = Color then
              if Self.IDefStyle = IDefStyle then
                if Self.IDefStyle = IDefStyle then
                  if Self.IDefColor = IDefColor then
                    if Self.FInterCharSize = FInterCharSize then
                      if Self.FShadow.OnCompareItems(FShadow) then
                        Result := Self.FOutLine.OnCompareItems(FOutLine);
          end;
end;

function TTeeFont.CreateDeterminant(Determinant: string): string;
var
  Size              : Integer;
  tmpCharset        : TFontCharset;
  tmpStyle          : TFontStyles;
  smlValue          : SmallInt;
  tmpColor          : TColor;
begin
  Result := FShadow.CreateDeterminant(Determinant);
  Result := FOutLine.CreateDeterminant(Result);

  Size := Length(Result);
  SetLength(Result, Size + Length(Name) + SizeOf(TFontCharset) + 2 * SizeOf(TFontStyles) +
    SizeOf(SmallInt) + SizeOf(FInterCharSize) + 2 * SizeOf(TColor));
  //   CopyMemory(@Result[1],@Determinant[1],Size);
  Inc(Size);

  CopyMemory(@Result[Size], @Name[1], Length(Name));
  Inc(Size, Length(Name));

  tmpCharset := CharSet;
  CopyMemory(@Result[Size], @tmpCharset, SizeOf(TFontCharset));
  Inc(Size, SizeOf(TFontCharset));

  tmpStyle := Style;
  CopyMemory(@Result[Size], @tmpStyle, SizeOf(TFontStyles));
  Inc(Size, SizeOf(TFontStyles));

  CopyMemory(@Result[Size], @IDefStyle, SizeOf(TFontStyles));
  Inc(Size, SizeOf(TFontStyles));

  // Pack values (this method slightly limits the maximum value of Height)
  smlValue := Height;
  CopyMemory(@Result[Size], @smlValue, SizeOf(SmallInt));
  Inc(Size, SizeOf(SmallInt));

  CopyMemory(@Result[Size], @FInterCharSize, SizeOf(FInterCharSize));
  Inc(Size, SizeOf(FInterCharSize));

  tmpColor := Color;
  CopyMemory(@Result[Size], @tmpColor, SizeOf(TColor));
  Inc(Size, SizeOf(TColor));

  CopyMemory(@Result[Size], @IDefColor, SizeOf(TColor));
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

procedure TTeeFont.Assign(Source: TPersistent);
begin
  if Source is TTeeFont then
    with TTeeFont(Source) do
    begin
      Self.FInterCharSize := FInterCharSize;
      Self.OutLine := FOutLine;
      Self.Shadow := FShadow;
    end;
  inherited;
end;

procedure TTeeFont.SetInterCharSize(Value: Integer);
begin
  if Value <> FInterCharSize then
  begin
    FInterCharSize := Value;
    if Assigned(OnChange) then OnChange(Self);
  end;
end;

procedure TTeeFont.SetOutLine(Value: TChartHiddenPen);
begin
  FOutLine.Assign(Value);
end;

procedure TTeeFont.SetShadow(Value: TTeeShadow);
begin
  FShadow.Assign(Value)
end;

function TTeeFont.IsHeightStored: Boolean;
begin
  result := Size <> GetDefaultFontSize;
end;

function TTeeFont.IsNameStored: Boolean;
begin
  result := {$IFNDEF CLX}Name <> GetDefaultFontName{$ELSE}True{$ENDIF};
end;

function TTeeFont.IsStyleStored: Boolean;
begin
  result := Style <> IDefStyle;
end;

function TTeeFont.IsColorStored: Boolean;
begin
  result := Color <> IDefColor;
end;

{ Util functions }

function MaxDouble(const a, b: Double): Double;
begin
  if a > b then
    result := a
  else
    result := b;
end;

function MinDouble(const a, b: Double): Double;
begin
  if a < b then
    result := a
  else
    result := b;
end;

function MaxLong(a, b: Integer): Integer;
begin
  if a > b then
    result := a
  else
    result := b;
end;

function MinLong(a, b: Integer): Integer;
begin
  if a > b then
    result := b
  else
    result := a;
end;

procedure SwapDouble(var a, b: Double);
var
  tmp               : Double;
begin
  tmp := a;
  a := b;
  b := tmp;
end;

procedure SwapInteger(var a, b: Integer);
var
  tmp               : Integer;
begin
  tmp := a;
  a := b;
  b := tmp;
end;

{ TTeeButton }
{$IFNDEF CLX}

procedure TTeeButton.WMPaint(var Message: TWMPaint);
begin
  //  {$IFDEF D4}
  //  ControlState:=ControlState+[csCustomPaint];
  inherited;
  //  ControlState:=ControlState-[csCustomPaint];
  //  {$ELSE}
  //  PaintHandler(Message);
  //  {$ENDIF}
end;

procedure TTeeButton.PaintWindow(DC: HDC);
var
  tmp               : TCanvas;
begin
  inherited;
  if Assigned(Instance) then
  begin
    tmp := TCanvas.Create;
    with tmp do
    try
      Lock;
      Handle := DC;
      try
        DrawSymbol(tmp);
      finally
        Handle := 0;
        Unlock;
      end;
    finally
      Free;
    end;
  end;
end;

procedure TTeeButton.CMEnabledChanged(var Message: TMessage);
begin
  inherited;
  Repaint;
end;
{$ELSE}

procedure TTeeButton.Painting(Sender: QObjectH; EventRegion: QRegionH);
begin
  inherited;
end;
{$ENDIF}

procedure TTeeButton.LinkProperty(AInstance: TObject;
  const PropName: string);
begin
  Instance := AInstance;
  Info := GetPropInfo(Instance{$IFNDEF D5}.ClassInfo{$ENDIF}, PropName);
end;

{ EditColor }

function EditColor(AOwner: TComponent; AColor: TColor): TColor;
begin
  with TColorDialog.Create(AOwner) do
  try
    Color := AColor;
    if not Assigned(TeeCustomEditColors) then
      TeeCustomEditColors := TStringList.Create
    else
      CustomColors := TeeCustomEditColors;
    if Execute then
    begin
      TeeCustomEditColors.Assign(CustomColors);
      result := Color;
    end
    else
      result := AColor;
  finally
    Free;
  end;
end;

{ TButtonColor }

procedure TButtonColor.Click;
begin
  if Assigned(Instance) then
  begin
    SetOrdProp(Instance, Info, EditColor(Self, GetTeeColor));
    Repaint;
  end;
  inherited;
end;

function TButtonColor.GetTeeColor: TColor;
begin
  if Assigned(GetColorProc) then
    result := GetColorProc
  else
    if Assigned(Info) then
      result := GetOrdProp(Instance, Info)
    else
      result := clNone;
end;

procedure TButtonColor.DrawSymbol(Canvas: TCanvas);
begin
  with Canvas do
  begin
    Brush.Color := GetTeeColor;
    if Brush.Color <> clNone then
    begin
      if not Enabled then Pen.Style := psClear;
      Rectangle(Width - 19, Height - 19, Width - 6, Height - 7);
    end;
  end;
end;

{$IFNDEF D5}

procedure FreeAndNil(var Obj);
var
  P                 : TObject;
begin
  P := TObject(Obj);
  TObject(Obj) := nil;
  P.Free;
end;
{$ENDIF}

procedure CheckJapaneseFontSize(var ASize: Integer);
begin
  {$IFNDEF CLX}
  if (ASize = 8) and (SysLocale.PriLangID = LANG_JAPANESE) then
    ASize := -MulDiv(DefFontData.Height, 72, Screen.PixelsPerInch);
  {$ENDIF}
end;

function GetDefaultFontSize: Integer;
begin
  result := StrToInt(TeeMsg_DefaultFontSize);
  CheckJapaneseFontSize(result);
end;

function GetDefaultFontName: string;
begin
  result := TeeMsg_DefaultFontName;
  {$IFNDEF CLX}
  if (result = 'Arial') and (SysLocale.PriLangID = LANG_JAPANESE) then { <-- do not translate }
    result := DefFontData.Name;
  {$ENDIF}
end;

procedure TeeBlendBitmaps(const Percent: Double; ABitmap, BBitmap: TBitmap; BOrigin: TPoint);
{$IFNDEF CLX}
var
  tmpX              : Integer;
  tmpX3             : Integer;
  tmpXB3            : Integer;
  tmpY              : Integer;
  tmpW              : Integer;
  tmpH              : Integer;
  tmpScanA          : PByteArray;
  tmpScanB          : PByteArray;
  tmpScanC          : PByteArray;
  Percent1          : Double;
  Percent2          : Double;
  tmpDest           : TBitmap;
  {$ENDIF}
begin
  {$IFNDEF CLX}
  ABitmap.PixelFormat := pf24Bit;
  BBitmap.PixelFormat := pf24Bit;
  tmpDest := BBitmap;
  if BOrigin.Y < 0 then BOrigin.Y := 0;
  if BOrigin.X < 0 then BOrigin.X := 0;
  tmpW := MinLong(ABitmap.Width, BBitmap.Width - BOrigin.X);
  tmpH := MinLong(ABitmap.Height, BBitmap.Height - BOrigin.Y);
  if (tmpW > 0) and (tmpH > 0) then
  begin
    Percent1 := Percent * 0.01;
    Percent2 := 1 - Percent1;
    for tmpY := 0 to tmpH - 1 do
    begin
      tmpScanA := ABitmap.ScanLine[tmpY];
      tmpScanB := BBitmap.ScanLine[tmpY + BOrigin.Y];
      tmpScanC := tmpDest.ScanLine[tmpY + BOrigin.Y];
      for tmpX := 0 to tmpW - 1 do
      begin
        tmpX3 := tmpX * 3;
        tmpXB3 := (tmpX + BOrigin.X) * 3;
        if TColor(tmpScanB[tmpX3]) = $FFFFFF then tmpScanC[tmpX3 + 2] := $FF - 1;
        tmpScanC[tmpXB3] := Round(Percent2 * tmpScanA[tmpX3] + Percent1 * tmpScanB[tmpXB3]);
        tmpScanC[tmpXB3 + 1] := Round(Percent2 * tmpScanA[tmpX3 + 1] + Percent1 * tmpScanB[tmpXB3 + 1]);
        tmpScanC[tmpXB3 + 2] := Round(Percent2 * tmpScanA[tmpX3 + 2] + Percent1 * tmpScanB[tmpXB3 + 2]);
      end;
    end;
  end;
  {$ENDIF}
end;

type
  TCanvasAccess = class(TTeeCanvas3D);

  { TTeeBlend }

constructor TTeeBlend.Create(ACanvas: TTeeCanvas3D; const R: TRect);
var
  tmp               : TCanvas;
begin
  inherited Create;
  FBitmap := TBitmap.Create;
  FRect := R;
  FCanvas := ACanvas;
  with FBitmap do
  begin
    {$IFNDEF CLX}
    PixelFormat := pf24Bit;
    {$ENDIF}
    with FRect do
    begin
      Width := Right - Left;
      Height := Bottom - Top;
    end;
    tmp := FCanvas.ReferenceCanvas;
    if Assigned(TCanvasAccess(FCanvas).Bitmap) then
      tmp := TCanvasAccess(FCanvas).Bitmap.Canvas;
    Canvas.CopyRect({$IFDEF D6}Types.{$ENDIF}Rect(0, 0, Width, Height),
      tmp, FRect);
  end;
end;

destructor TTeeBlend.Destroy;
begin
  FBitmap.Free;
  inherited;
end;

procedure TTeeBlend.DoBlend(Transparency: TTeeTransparency);
var
  tmp               : TBitmap;
  tmpNew            : Boolean;
  tmpPoint          : TPoint;
begin
  tmp := TCanvasAccess(FCanvas).Bitmap;
  tmpNew := not Assigned(tmp);
  if tmpNew then
  begin
    tmp := TBitmap.Create;
    tmp.Width := FRect.Right - FRect.Left;
    tmp.Height := FRect.Bottom - FRect.Top;
    tmp.Canvas.CopyRect(FRect, FCanvas.ReferenceCanvas, Rect(0, 0, tmp.Width, tmp.Height));
    tmpPoint := {$IFDEF D6}Types{$ELSE}Classes{$ENDIF}.Point(0, 0);
  end
  else
    tmpPoint := FRect.TopLeft;
  TeeBlendBitmaps(100 - Transparency, FBitmap, tmp, tmpPoint);
  if tmpNew then
  begin
    FCanvas.Draw(FRect.Left, FRect.Top, tmp);
    tmp.Free;
  end;
end;

initialization
  {$IFNDEF CLX}
  OldRegion := CreateRectRgn(0, 0, 0, 0);
  {$ELSE}
  OldRegion := QRegion_create(0, 0, 0, 0, QRegionRegionType_Rectangle);
  {$ENDIF}
finalization
  {$IFDEF CLX}
  if Assigned(OldRegion) then QRegion_destroy(OldRegion);
  {$ELSE}
  if OldRegion > 0 then DeleteObject(OldRegion);
  {$ENDIF}
  TeeCustomEditColors.Free;
end.

