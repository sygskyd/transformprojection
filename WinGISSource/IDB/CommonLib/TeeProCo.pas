{*****************************************}
{   TeeChart Pro                          }
{   Copyright (c) 1995-2000 David Berneda }
{   Constants for Pro components          }
{*****************************************}
{$I teedefs.inc}
unit TeeProCo;

interface

resourcestring
  TeeMsg_GalleryPolar       ='Polar';
  TeeMsg_GalleryCandle      ='Candle';
  TeeMsg_GalleryVolume      ='Volume';
  TeeMsg_GallerySurface     ='Surface';
  TeeMsg_GalleryContour     ='Contour';
  TeeMsg_GalleryBezier      ='Bezier';
  TeeMsg_GalleryPoint3D     ='Point 3D';
  TeeMsg_GalleryRadar       ='Radar';
  TeeMsg_GalleryDonut       ='Donut';
  TeeMsg_GalleryCursor      ='Cursor';
  TeeMsg_GalleryBar3D       ='Bar 3D';
  TeeMsg_GalleryBigCandle   ='Big Candle';
  TeeMsg_GalleryLinePoint   ='Line Point';
  TeeMsg_GalleryHistogram   ='Histogram';
  TeeMsg_GalleryWaterFall   ='Water Fall';
  TeeMsg_GalleryWindRose    ='Wind Rose';
  TeeMsg_GalleryClock       ='Clock';
  TeeMsg_GalleryColorGrid   ='ColorGrid';
  TeeMsg_GalleryBoxPlot     ='BoxPlot';
  TeeMsg_GalleryHorizBoxPlot='Horiz.BoxPlot';
  TeeMsg_GalleryBarJoin     ='Bar Join';
  TeeMsg_GallerySmith       ='Smith';
  TeeMsg_GalleryPyramid     ='Pyramid';
  TeeMsg_GalleryMap         ='Map';

  TeeMsg_PolyDegreeRange    ='Polynomial degree must be between 1 and 20';
  TeeMsg_AnswerVectorIndex   ='Answer Vector index must be between 1 and %d';
  TeeMsg_FittingError        ='Cannot process fitting';
  TeeMsg_PeriodRange         ='Period must be >= 0';
  TeeMsg_ExpAverageWeight    ='ExpAverage Weight must be between 0 and 1';
  TeeMsg_GalleryErrorBar     ='Error Bar';
  TeeMsg_GalleryError        ='Error';
  TeeMsg_GalleryHighLow      ='High-Low';
  TeeMsg_FunctionMomentum    ='Momentum';
  TeeMsg_FunctionMomentumDiv ='Momentum Div';
  TeeMsg_FunctionExpAverage  ='Exp. Average';
  TeeMsg_FunctionMovingAverage='Moving Avrg.';
  TeeMsg_FunctionExpMovAve   ='Exp.Mov.Avrg.';
  TeeMsg_FunctionRSI         ='R.S.I.';
  TeeMsg_FunctionCurveFitting='Curve Fitting';
  TeeMsg_FunctionTrend       ='Trend';
  TeeMsg_FunctionExpTrend    ='Exp.Trend';
  TeeMsg_FunctionLogTrend    ='Log.Trend';
  TeeMsg_FunctionCumulative  ='Cumulative';
  TeeMsg_FunctionStdDeviation='Std.Deviation';
  TeeMsg_FunctionBollinger   ='Bollinger';
  TeeMsg_FunctionRMS         ='Root Mean Sq.';
  TeeMsg_FunctionMACD        ='MACD';
  TeeMsg_FunctionStochastic  ='Stochastic';
  TeeMsg_FunctionADX         ='ADX';

  TeeMsg_FunctionCount       ='Count';
  TeeMsg_LoadChart           ='Open TeeChart...';
  TeeMsg_SaveChart           ='Save TeeChart...';
  TeeMsg_TeeFiles            ='TeeChart Pro files';

  TeeMsg_GallerySamples      ='Other';
  TeeMsg_GalleryStats        ='Stats';

  TeeMsg_CannotFindEditor    ='Cannot find Series editor Form: %s';


  TeeMsg_CannotLoadChartFromURL='Error code: %d downloading Chart from URL: %s';
  TeeMsg_CannotLoadSeriesDataFromURL='Error code: %d downloading Series data from URL: %s';

  TeeMsg_Test                ='Test...';

  TeeMsg_ValuesDate          ='Date';
  TeeMsg_ValuesOpen          ='Open';
  TeeMsg_ValuesHigh          ='High';
  TeeMsg_ValuesLow           ='Low';
  TeeMsg_ValuesClose         ='Close';
  TeeMsg_ValuesOffset        ='Offset';
  TeeMsg_ValuesStdError      ='StdError';

  TeeMsg_Grid3D              ='Grid 3D';

  TeeMsg_LowBezierPoints     ='Number of Bezier points should be > 1';

  { TeeCommander component... }

  TeeCommanMsg_Normal   = 'Normal';
  TeeCommanMsg_Edit     = 'Edit';
  TeeCommanMsg_Print    = 'Print';
  TeeCommanMsg_Copy     = 'Copy';
  TeeCommanMsg_Save     = 'Save';
  TeeCommanMsg_3D       = '3D';

  TeeCommanMsg_Rotating = 'Rotation: %d� Elevation: %d�';
  TeeCommanMsg_Rotate   = 'Rotate';

  TeeCommanMsg_Moving   = 'Horiz.Offset: %d Vert.Offset: %d';
  TeeCommanMsg_Move     = 'Move';

  TeeCommanMsg_Zooming  = 'Zoom: %d %%';
  TeeCommanMsg_Zoom     = 'Zoom';

  TeeCommanMsg_Depthing = '3D: %d %%';
  TeeCommanMsg_Depth    = 'Depth';

  TeeCommanMsg_Chart    ='Chart';
  TeeCommanMsg_Panel    ='Panel';

  TeeCommanMsg_RotateLabel='Drag %s to Rotate';
  TeeCommanMsg_MoveLabel  ='Drag %s to Move';
  TeeCommanMsg_ZoomLabel  ='Drag %s to Zoom';
  TeeCommanMsg_DepthLabel ='Drag %s to resize 3D';

  TeeCommanMsg_NormalLabel='Drag Left button to Zoom, Right button to Scroll';
  TeeCommanMsg_NormalPieLabel='Drag a Pie Slice to Explode it';

  TeeCommanMsg_PieExploding = 'Slice: %d Exploded: %d %%';

  TeeMsg_TriSurfaceLess='Number of points must be >= 4';
  TeeMsg_TriSurfaceAllColinear='All colinear data points';
  TeeMsg_TriSurfaceSimilar='Similar points - cannot execute';
  TeeMsg_GalleryTriSurface='Triangle Surf.';

  TeeMsg_AllSeries = 'All Series';
  TeeMsg_Edit      = 'Edit';

  TeeMsg_GalleryFinancial    ='Financial';

  TeeMsg_CursorTool    = 'Cursor';
  TeeMsg_DragMarksTool = 'Drag Marks';
  TeeMsg_AxisArrowTool = 'Axis Arrows';
  TeeMsg_DrawLineTool  = 'Draw Line';
  TeeMsg_NearestTool   = 'Nearest Point';
  TeeMsg_ColorBandTool = 'Color Band';
  TeeMsg_ColorLineTool = 'Color Line';
  TeeMsg_RotateTool    = 'Rotate';
  TeeMsg_ImageTool     = 'Image';
  TeeMsg_MarksTipTool  = 'Mark Tips';

  TeeMsg_CantDeleteAncestor  = 'Can not delete ancestor';

  TeeMsg_Load	         = 'Load...';
  TeeMsg_WinInet         = '(WinInet.dll to access TeeChart)';
  TeeMsg_DefaultDemoTee  = 'http://www.steema.com/demo.tee';
  TeeMsg_NoSeriesSelected= 'No Series selected';

  { TeeChart Actions }
  TeeMsg_CategoryChartActions  = 'Chart';
  TeeMsg_CategorySeriesActions = 'Chart Series';

  TeeMsg_Action3D               = '&3D';
  TeeMsg_Action3DHint           = 'Switch 2D / 3D';
  TeeMsg_ActionSeriesActive     = '&Active';
  TeeMsg_ActionSeriesActiveHint = 'Show / Hide Series';
  TeeMsg_ActionEditHint         = 'Edit Chart';
  TeeMsg_ActionEdit             = '&Edit...';
  TeeMsg_ActionCopyHint         = 'Copy to Clipboard';
  TeeMsg_ActionCopy             = '&Copy';
  TeeMsg_ActionPrintHint        = 'Print preview Chart';
  TeeMsg_ActionPrint            = '&Print...';
  TeeMsg_ActionAxesHint         = 'Show / Hide Axes';
  TeeMsg_ActionAxes             = '&Axes';
  TeeMsg_ActionGridsHint        = 'Show / Hide Grids';
  TeeMsg_ActionGrids            = '&Grids';
  TeeMsg_ActionLegendHint       = 'Show / Hide Legend';
  TeeMsg_ActionLegend           = '&Legend';
  TeeMsg_ActionSeriesEditHint   = 'Edit Series';
  TeeMsg_ActionSeriesMarksHint  = 'Show / Hide Series Marks';
  TeeMsg_ActionSeriesMarks      = '&Marks';
  TeeMsg_ActionSaveHint         = 'Save Chart';
  TeeMsg_ActionSave             = '&Save...';

  TeeMsg_CandleBar              = 'Bar';
  TeeMsg_CandleNoOpen           = 'No Open';
  TeeMsg_CandleNoClose          = 'No Close';
  TeeMsg_NoLines                = 'No Lines';
  TeeMsg_NoHigh                 = 'No High';
  TeeMsg_NoLow                  = 'No Low';
  TeeMsg_ColorRange             = 'Color Range';
  TeeMsg_WireFrame              = 'Wireframe';
  TeeMsg_DotFrame               = 'Dot Frame';
  TeeMsg_Positions              = 'Positions';
  TeeMsg_NoGrid                 = 'No Grid';
  TeeMsg_NoPoint                = 'No Point';
  TeeMsg_NoLine                 = 'No Line';
  TeeMsg_Labels                 = 'Labels';
  TeeMsg_NoCircle               = 'No Circle';
  TeeMsg_Lines                  = 'Lines';
  TeeMsg_Border                 = 'Border';

  TeeMsg_SmithResistance      = 'Resistance';
  TeeMsg_SmithReactance       = 'Reactance';
  
  TeeMsg_Column  = 'Column';

implementation

end.
