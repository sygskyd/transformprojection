{ TeeChart Pro Defines      }
{ See User Options below... }

{ Internal use. Do NOT modify.            }
{=========================================}

{$DEFINE TEE5}

{$H+}
{$IFDEF VER140}
  {$DEFINE D3}
  {$DEFINE D4}
  {$DEFINE D5}
  {$DEFINE C5}
  {$DEFINE D6}
{$ENDIF}
{$IFDEF VER130}
  {$DEFINE D3}
  {$DEFINE D4}
  {$DEFINE D5}
  {$DEFINE C4}
  {$DEFINE C5}
  {$ObjExportAll On}
{$ENDIF}
{$IFDEF VER125}
  {$DEFINE D3}
  {$DEFINE D4}
  {$DEFINE C3D4}
  {$DEFINE C4}
  {$ObjExportAll On}
{$ENDIF}
{$IFDEF VER120}
  {$DEFINE D3}
  {$DEFINE D4}
  {$DEFINE C3D4}
{$ENDIF}
{$IFDEF VER110}
  {$DEFINE D3}
  {$IFDEF BCB}
    {$DEFINE C3}
    {$DEFINE C3D4}
    {$ObjExportAll On}
  {$ELSE}
    {$DEFINE D4}
    {$DEFINE C3D4}
  {$ENDIF}
{$ENDIF}
{$IFDEF VER100}
  {$DEFINE D3}
{$ENDIF}

{$IFDEF VER80}
  'Error: TeeChart 5 does not support Delphi 1 and 2. Use TeeChart v4.'
{$ENDIF}

{$IFDEF D6}
{.$DEFINE CLX}
{$ENDIF}

{$IFDEF LINUX}
{$DEFINE CLX}
{$ENDIF}

{.$DEFINE TEETRIAL}

{===================================}
{ User Options                      }
{===================================}

{.$DEFINE NOUSE_BDE}      { <-- remove the "." to avoid using the BDE (in QRChart, etc) }
{.$DEFINE TEEMULTIPLIER}  { <-- remove the "." to enable "Multiplier" property. (slower) }

{$IFDEF D4}

{ TList     = less speed modifying values, more speed deleting values, much MORE memory }
{ Dyn Array = Direct access to values, more speed modifying values, much LESS memory,
              less speed deleting values. Not for Delphi 3. }

{$DEFINE TEEARRAY}  { <-- put a "." in front to use TList instead of Dynamic Arrays }

{$ENDIF}

{ "Value" type. Remove the "." on ONLY one of the items below: }

{$DEFINE TEEVALUEDOUBLE}   { <-- default, balance of speed and memory. }
{.$DEFINE TEEVALUESINGLE}    { <-- less memory used, less precision, more speed }
{.$DEFINE TEEVALUEEXTENDED} { <-- more memory used, more precision, less speed }

{ Other }
{.$DEFINE TEEPENEND} { <-- adds support for TPen "ending" style in TeePenDlg }

