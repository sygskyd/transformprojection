{**********************************************}
{   TeeChart Pro and TeeTree VCL About Form    }
{   Copyright (c) 1995-2000 by David Berneda   }
{**********************************************}
{$I TeeDefs.inc}
unit TeeAbout;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     {$IFDEF CLX}
     Qt, QGraphics, QControls, QForms, QExtCtrls, QStdCtrls,
     {$ELSE}
     Graphics, Controls, Forms, ExtCtrls, StdCtrls,
     {$ENDIF}
     Classes, SysUtils;

type
  TTeeAboutForm = class(TForm)
    BClose: TButton;
    Image2: TImage;
    Label3: TLabel;
    LabelVersion: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Labelwww: TLabel;
    Image1: TImage;
    Bevel1: TBevel;
    LabelEval: TLabel;
    procedure LabelwwwClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Procedure GotoURL(Handle:{$IFDEF CLX}QOpenScrollViewH{$ELSE}Integer{$ENDIF}; Const URL:String);

{ Displays the TeeChart about-box dialog }
Procedure TeeShowAboutBox;

Var TeeAboutBoxProc:Procedure=nil;

implementation

{$R *.dfm}

Uses TeCanvas,
     {$IFNDEF CLX}
     ShellApi,
     {$ENDIF}
     TeeConst;

Procedure TeeShowAboutBox;
begin
  With TTeeAboutForm.Create(nil) do
  try
    {$IFDEF TEETRIAL}
    LabelEval.Caption:='TRIAL VERSION';
    {$ENDIF}
    ShowModal;
  finally
    Free;
  end;
end;

Procedure GotoURL(Handle:{$IFDEF CLX}QOpenScrollViewH{$ELSE}Integer{$ENDIF}; Const URL:String);
{$IFNDEF CLX}
Var St : Array[0..255] of char;
{$ENDIF}
begin
  {$IFNDEF CLX}
  ShellExecute(Handle,'open',StrPCopy(St,URL),nil,nil,SW_SHOW);  { <-- do not translate }
  {$ENDIF}
end;

procedure TTeeAboutForm.LabelwwwClick(Sender: TObject);
begin
  GotoURL(Handle,LabelWWW.Caption);
end;

procedure TTeeAboutForm.FormPaint(Sender: TObject);
begin
  With TTeeCanvas3D.Create do
  try
    ReferenceCanvas:=Self.Canvas;
    GradientFill(ClientRect,$E0E0E0,clDkGray,gdFromTopLeft);
  finally
    Free;
  end;
end;

procedure TTeeAboutForm.FormCreate(Sender: TObject);
begin
  Caption:=Caption+' '+TeeMsg_Version;
  LabelVersion.Caption:=TeeMsg_Version;
end;

{$IFDEF TEETRIAL}
Procedure TeeShowAboutBox2;
begin
  if Assigned(TeeAboutBoxProc) then
  begin
    TeeShowAboutBox;
    TeeAboutBoxProc:=nil;
  end;
end;
{$ENDIF}

{$IFDEF TEETRIAL}
initialization
  TeeAboutBoxProc:=TeeShowAboutBox2;
finalization
  TeeAboutBoxProc:=nil;
{$ENDIF}
end.
