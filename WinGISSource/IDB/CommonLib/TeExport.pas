{**********************************************}
{   TeeChart Export Dialog                     }
{   Copyright (c) 1996-2000 by David Berneda   }
{**********************************************}
{$I teedefs.inc}
{$IFDEF TEEOCX}
{$I teeaxdefs.inc}
{$ENDIF}
unit TeExport;

interface

uses
  {$IFDEF LINUX}
  LibC,
  {$ELSE}
  Windows, Messages,
  {$ENDIF}
  SysUtils, Classes,
  {$IFDEF CLX}
  QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls, QComCtrls,
  TeePenDlg,
  {$ELSE}
  Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, ComCtrls,
  {$ENDIF}
  TeeProcs, TeeStore, TeEngine, Chart;

type
  TTeeExportFormat=class
  protected
    FFilterIndex : Integer;
    Procedure CheckSize;
    Procedure DoCopyToClipboard; virtual; abstract;
    function FileFilterIndex:Integer; virtual;
    Procedure IncFileFilterIndex(Var FilterIndex:Integer); virtual;
  public
    Height : Integer;
    Panel  : TCustomTeePanel;
    Width  : Integer;
    Destructor Destroy; override;

    Procedure CopyToClipboard;
    function Description:String; virtual; abstract;
    function FileExtension:String; virtual; abstract;
    function FileFilter:String; virtual; abstract;
    Procedure SaveToFile(Const FileName:String);
    Procedure SaveToStream(Stream:TStream); virtual; abstract;
    Function Options:TForm; virtual;
  end;

  TTeeExportForm = class(TForm)
    SaveDialogPicture: TSaveDialog;
    PageControl1: TPageControl;
    TabPicture: TTabSheet;
    TabData: TTabSheet;
    RGFormat: TRadioGroup;
    Label3: TLabel;
    CBSeries: TComboBox;
    SaveDialogData: TSaveDialog;
    RGText: TRadioGroup;
    TabNative: TTabSheet;
    CBNativeData: TCheckBox;
    SaveDialogNative: TSaveDialog;
    LabelSize: TLabel;
    CBFileSize: TCheckBox;
    CBDelim: TComboBox;
    ECustom: TEdit;
    GroupBox1: TGroupBox;
    CBLabels: TCheckBox;
    CBIndex: TCheckBox;
    Label4: TLabel;
    CBHeader: TCheckBox;
    PageOptions: TPageControl;
    TabOptions: TTabSheet;
    TabSize: TTabSheet;
    Label1: TLabel;
    EWidth: TEdit;
    UDWidth: TUpDown;
    Label2: TLabel;
    EHeight: TEdit;
    UDHeight: TUpDown;
    CBAspect: TCheckBox;
    Panel1: TPanel;
    BCopy: TButton;
    BSave: TButton;
    BSend: TButton;
    BClose: TButton;
    procedure BCopyClick(Sender: TObject);
    procedure BSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGFormatClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EWidthChange(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure RGTextClick(Sender: TObject);
    procedure CBFileSizeClick(Sender: TObject);
    procedure CBNativeDataClick(Sender: TObject);
    procedure CBDelimChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BSendClick(Sender: TObject);
  private
    { Private declarations }
    IAspect      : Double;
    ChangingSize : Boolean;
    Function CanChangeSize:Boolean;
    Function Chart:TCustomChart;
    Function CreateSeriesData:TSeriesData;
    Procedure FreeExportFormats;
    Function GetDataFilterIndex:Integer;
    Function GetSeparator:Char;
    Function PictureFormat:TTeeExportFormat;
    Procedure SaveDataToFile(Const FileName:String);
    Procedure SaveNativeToFile(Const FileName:String);
    Procedure SavePictureToFile(Const FileName:String);
    Function SelectedSeries:TChartSeries;
  public
    { Public declarations }
    ExportPanel  : TCustomTeePanel;
    procedure EnableButtons;
  end;

{ Retrieves a native "*.tee" file }
Procedure LoadTeeFromFile(Var APanel:TCustomTeePanel; Const AName:String);
Procedure LoadTeeFromStream(Var APanel:TCustomTeePanel; AStream:TStream);

{ Saves a Chart or TeePanel to a native "*.tee" file format }
Procedure SaveTeeToFile(APanel:TCustomTeePanel; Const AName:String);
Procedure SaveTeeToStream(APanel:TCustomTeePanel; AStream:TStream);

{ "Tee export formats" }
type TTeeExportFormatClass=class of TTeeExportFormat;

Procedure RegisterTeeExportFormat(AFormat:TTeeExportFormatClass);
Procedure UnRegisterTeeExportFormat(AFormat:TTeeExportFormatClass);

{ Show the Export dialog }
{ example: TeeExport(Self,Chart1); }
Procedure TeeExport(AOwner:TComponent; APanel:TCustomTeePanel);

{ Show the Save dialog and save to AFormat export format }
{ example: TeeSavePanel(TGIFExportFormat,Chart1); }
Procedure TeeSavePanel(AFormat:TTeeExportFormatClass; APanel:TCustomTeePanel);

procedure TeeFillPictureDialog(ADialog:TSaveDialog; APanel:TCustomTeePanel; AItems:TStrings);

type TTeeExportFormats=class(TList)
     private
       Function Get(Index:Integer):TTeeExportFormatClass;
     public
       property Format[Index:Integer]:TTeeExportFormatClass read Get; default;
     end;

Const TeeExportFormats : TTeeExportFormats=nil;

{ starts the MAPI (eg: Outlook) application with an empty new email
  message with the attachment file "FileName" }
Procedure TeeEmailFile(Const FileName:String);

implementation

{$R *.dfm}
Uses {$IFDEF CLX}
     QClipbrd,
     {$ELSE}
     Clipbrd, ExtDlgs, TeePenDlg,
     {$ENDIF}
     TeeConst,
     {$IFNDEF D5}
     TeCanvas,
     {$ENDIF}
     TeeBMPOptions
     {$IFNDEF CLX}
     ,TeeEMFOptions
     {$ENDIF}
     {$IFNDEF LINUX}
     ,MAPI
     {$ENDIF};

Procedure SaveTeeToStream(APanel:TCustomTeePanel; AStream:TStream);
begin
  AStream.WriteComponent(APanel);
end;

Procedure SaveTeeToFile(APanel:TCustomTeePanel; Const AName:String);
Var tmp        : TFileStream;
    OldVisible : Boolean;
begin
  tmp:=TFileStream.Create(AName,fmCreate);
  try
    OldVisible:=APanel.Visible;
    APanel.Visible:=True;
    try
      SaveTeeToStream(APanel,tmp);
    finally
      APanel.Visible:=OldVisible;
    end;
  finally
    tmp.Free;
  end;
end;

Procedure LoadTeeFromStream(Var APanel:TCustomTeePanel; AStream:TStream);
begin
  AStream.ReadComponent(APanel);
end;

Procedure LoadTeeFromFile(Var APanel:TCustomTeePanel; Const AName:String);
Var tmp : TFileStream;
begin
  tmp:=TFileStream.Create(AName,fmOpenRead);
  try
    LoadTeeFromStream(APanel,tmp);
  finally
    tmp.Free;
  end;
end;

{ Export Dialog }
procedure TTeeExportForm.BCopyClick(Sender: TObject);
begin
  if PageControl1.ActivePage=TabPicture then
  with PictureFormat do
  begin
    Width:=UDWidth.Position;
    Height:=UDHeight.Position;
    CopyToClipboard;
  end
  else
  if PageControl1.ActivePage=TabData then
  begin
    Screen.Cursor:=crHourGlass;
    try
      With CreateSeriesData do
      try
        CopyToClipboard;
      finally
        Free;
      end;
    finally
      Screen.Cursor:=crDefault;
    end;
  end;
end;

Function TTeeExportForm.CreateSeriesData:TSeriesData;
begin
  Case RGText.ItemIndex of
    0: begin
         result:=TSeriesDataText.Create(Chart,SelectedSeries);
         TSeriesDataText(result).TextDelimiter:=GetSeparator;
       end;
    1: result:=TSeriesDataXML.Create(Chart,SelectedSeries);
    2: result:=TSeriesDataHTML.Create(Chart,SelectedSeries);
  else
    result:=TSeriesDataXLS.Create(Chart,SelectedSeries);
  end;
  With result do
  begin
    IncludeLabels:=CBLabels.Checked;
    IncludeIndex :=CBIndex.Checked;
    IncludeHeader:=CBHeader.Checked;
  end;
end;

Function TTeeExportForm.SelectedSeries:TChartSeries;
begin
  result:=TChartSeries(CBSeries.Items.Objects[CBSeries.ItemIndex]);
end;

Function TTeeExportForm.GetSeparator:Char;
begin
  Case CBDelim.ItemIndex of
    0: result:=' ';
    1: result:=TeeTabDelimiter;
    2: result:=',';
    3: result:=';';
  else if ECustom.Text='' then result:=' ' else result:=ECustom.Text[1];
  end;
end;

Procedure TTeeExportForm.SaveNativeToFile(Const FileName:String);
begin
  Screen.Cursor:=crHourGlass;
  try
    if ExportPanel is TCustomChart then
       SaveChartToFile(TCustomChart(ExportPanel),FileName,CBNativeData.Checked)
    else
       SaveTeeToFile(ExportPanel,FileName);
  finally
    Screen.Cursor:=crDefault;
  end;
end;

Procedure TTeeExportForm.SaveDataToFile(Const FileName:String);
begin
  Screen.Cursor:=crHourGlass;
  try
    With CreateSeriesData do
    try
      SaveToFile(FileName);
    finally
      Free;
    end;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

Procedure TTeeExportForm.SavePictureToFile(Const FileName:String);
begin
  Screen.Cursor:=crHourGlass;
  try
    With PictureFormat do
    begin
      Width:=UDWidth.Position;
      Height:=UDHeight.Position;
      SaveToFile(FileName);
    end;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

Function TTeeExportForm.GetDataFilterIndex:Integer;
begin
  if RGText.ItemIndex=0 then result:=CBDelim.ItemIndex+1
                        else result:=4+RGText.ItemIndex;
end;

procedure TTeeExportForm.BSaveClick(Sender: TObject);
begin
  if PageControl1.ActivePage=TabPicture then  { as picture... }
  With SaveDialogPicture do
  begin
    With PictureFormat do
    begin
      DefaultExt:=FileExtension;
      {$IFNDEF CLX}
      FilterIndex:=FileFilterIndex;
      {$ENDIF}
    end;
    FileName:='';
    if Execute then SavePictureToFile(FileName);
  end
  else
  if PageControl1.ActivePage=TabNative then   { as native *.tee file... }
  With SaveDialogNative do
  begin
    FileName:='';
    if Execute then SaveNativeToFile(FileName);
  end
  else
  if PageControl1.ActivePage=TabData then    { series data... }
  With SaveDialogData do
  begin
    FileName:='';
    {$IFNDEF CLX}
    FilterIndex:=GetDataFilterIndex;
    {$ENDIF}
    if Execute then
    begin
      if FilterIndex<=4 then RGText.ItemIndex:=0
                        else RGText.ItemIndex:=FilterIndex-4;
      SaveDataToFile(FileName);
    end;
  end;
end;

procedure TTeeExportForm.FormCreate(Sender: TObject);
begin
  ChangingSize:=False;
  PageControl1.ActivePage:=TabPicture;
end;

procedure TTeeExportForm.RGFormatClick(Sender: TObject);
var tmp : TForm;
    t   : Integer;
begin
  With TabOptions do
  for t:=0 to ControlCount-1 do Controls[t].Hide;
  With PictureFormat do
  begin
    tmp:=Options;
    TabOptions.TabVisible:=Assigned(tmp) and (tmp.ControlCount>0);
    if TabOptions.TabVisible then
    begin
      PageOptions.ActivePage:=TabOptions;
      AddFormTo(tmp,TabOptions,0);
    end
    else PageOptions.ActivePage:=TabSize;
  end;
end;

procedure TeeFillPictureDialog(ADialog:TSaveDialog; APanel:TCustomTeePanel; AItems:TStrings);
var t   : Integer;
    tmp : TTeeExportFormat;
    tmpFilter : Integer;
begin
  tmpFilter:=0;
  if Assigned(TeeExportFormats) then
  begin
    for t:=0 to TeeExportFormats.Count-1 do
    begin
      tmp:=TeeExportFormats[t].Create;
      tmp.Panel:=APanel;
      tmp.IncFileFilterIndex(tmpFilter);
      With ADialog do
      begin
        if Filter<>'' then Filter:=Filter+'|';
        Filter:=Filter+tmp.FileFilter;
      end;
      if Assigned(AItems) then AItems.AddObject(tmp.Description,tmp)
                          else tmp.Free;
    end;
  end;
end;

procedure TTeeExportForm.FormShow(Sender: TObject);
begin
  {$IFDEF D4}
  with SaveDialogData do Options:=Options+[ofEnableSizing];
  with SaveDialogPicture do Options:=Options+[ofEnableSizing];
  with SaveDialogNative do Options:=Options+[ofEnableSizing];
  {$ENDIF}
  if Tag<>0 then ExportPanel:=TCustomTeePanel(Tag);
  ChangingSize:=True;
  With ExportPanel do
  begin
    UDWidth.Position :=Width;
    UDHeight.Position:=Height;
    IAspect:=Width/Height;
  end;
  if ExportPanel is TCustomAxisPanel then
  begin
    FillSeriesItems(CBSeries.Items,TCustomAxisPanel(ExportPanel));
    CBSeries.Items.Insert(0,TeeMsg_All);
    CBSeries.ItemIndex:=0;
  end
  else
  begin
    TabData.TabVisible:=False;
    CBNativeData.Visible:=False;
  end;

  FreeExportFormats;
  TeeFillPictureDialog(SaveDialogPicture,ExportPanel,RGFormat.Items);

  CBDelim.ItemIndex:=1;
  RGFormat.ItemIndex:=0;
  EnableButtons;
  ChangingSize:=False;
end;

Function TTeeExportForm.CanChangeSize:Boolean;
begin
  result:=Showing and (not ChangingSize) and
          CBAspect.Checked and (IAspect<>0);
end;

procedure TTeeExportForm.EWidthChange(Sender: TObject);
begin
  if CanChangeSize then
  begin
    ChangingSize:=True;
    if Sender=EWidth then
       UDHeight.Position:=Round(UDWidth.Position/IAspect)
    else
       UDWidth.Position:=Round(UDHeight.Position*IAspect);
    ChangingSize:=False;
  end;
end;

procedure TTeeExportForm.PageControl1Change(Sender: TObject);
begin
  EnableButtons;
end;

Function MapiInstalled:Boolean;
{$IFNDEF CLX}
var tmp : Integer;
{$ENDIF}
begin
  {$IFNDEF CLX}
  tmp:={$IFDEF D5}SafeLoadLibrary{$ELSE}LoadLibrary{$ENDIF}('Mapi32.dll');
  result:=tmp>0;
  if result then FreeLibrary(tmp);
  {$ELSE}
  result:=False;
  {$ENDIF}
end;

procedure TTeeExportForm.EnableButtons;

  Function ExistSeries:Boolean;
  begin
    result:=(ExportPanel is TCustomAxisPanel) and
            (TCustomAxisPanel(ExportPanel).SeriesCount>0);
  end;

begin
  BCopy.Enabled:=(PageControl1.ActivePage=TabPicture) or
                 ((PageControl1.ActivePage=TabData) and ExistSeries);
  BSave.Enabled:=BCopy.Enabled or (PageControl1.ActivePage=TabNative);
  if (PageControl1.ActivePage=TabData) and (RGText.ItemIndex=3) then
     BCopy.Enabled:=False;
  BSend.Enabled:=BSave.Enabled and MapiInstalled;
end;

procedure TTeeExportForm.RGTextClick(Sender: TObject);
begin
  CBDelim.Enabled:=RGText.ItemIndex=0;
  ECustom.Enabled:=CBDelim.Enabled;
  CBHeader.Enabled:=(RGText.ItemIndex=2) or (RGText.ItemIndex=3);
  EnableButtons;
end;

procedure TTeeExportForm.CBFileSizeClick(Sender: TObject);
var tmp : TMemoryStream;
begin
  if CBFileSize.Checked then
  begin
    tmp:=TMemoryStream.Create;
    try
      SaveChartToStream(TCustomChart(ExportPanel),tmp,CBNativeData.Checked);
      LabelSize.Caption:=Format(TeeMsg_FileSize,[tmp.Size]);
    finally
      tmp.Free;
    end;
  end
  else LabelSize.Caption:='?';
end;

procedure TTeeExportForm.CBNativeDataClick(Sender: TObject);
begin
  CBFileSizeClick(Self);
end;

procedure TTeeExportForm.CBDelimChange(Sender: TObject);
begin
  ECustom.Enabled:=CBDelim.ItemIndex=4;
  if ECustom.Enabled then ECustom.SetFocus;
end;

function TTeeExportForm.Chart: TCustomChart;
begin
  result:=TCustomChart(ExportPanel);
end;

Procedure TTeeExportForm.FreeExportFormats;
var t: Integer;
begin
  if Assigned(TeeExportFormats) then
     for t:=0 to RGFormat.Items.Count-1 do
         TTeeExportFormat(RGFormat.Items.Objects[t]).Free;
  RGFormat.Items.Clear;
end;

procedure TTeeExportForm.FormDestroy(Sender: TObject);
begin
  FreeExportFormats;
end;

function TTeeExportForm.PictureFormat: TTeeExportFormat;
begin
  result:=TTeeExportFormat(RGFormat.Items.Objects[RGFormat.ItemIndex]);
end;

type TPathName=Array[0..MAX_PATH] of Char;

procedure TTeeExportForm.BSendClick(Sender: TObject);
{$IFNDEF CLX}
var tmpPath      : TPathName;
    tmpName      : TPathName;

  Procedure AdjustExtension(Const Extension:String);
  begin
    StrPCopy(tmpName,ChangeFileExt(tmpName,'.'+Extension));
  end;

  Function GetDataExtension(Index:Integer):String;
  var t  : Integer;
      i  : Integer;
      St : String;
  begin
    St:=SaveDialogData.Filter;
    for t:=1 to (2*Index)-1 do
    begin
      i:=Pos('|',St);
      if i>0 then Delete(St,1,i);
    end;
    i:=Pos('|',St);
    if i>0 then Delete(St,i,Length(St));
    i:=Pos('.',St);
    if i>0 then Delete(St,1,i);
    result:=St;
  end;

{$ENDIF}
begin
{$IFNDEF CLX}
  if GetTempPath(MAX_PATH,tmpPath)=0 then
     Raise Exception.Create(TeeMsg_CanNotFindTempPath);

  StrPCopy(tmpName,StrPas(tmpPath)+'\'+TeeMsg_TeeChartPalette);

  if PageControl1.ActivePage=TabPicture then
  begin
    AdjustExtension(PictureFormat.FileExtension);
    SavePictureToFile(tmpName);
  end
  else
  if PageControl1.ActivePage=TabNative then
  begin
    AdjustExtension(TeeMsg_TeeExtension);
    SaveNativeToFile(tmpName);
  end
  else
  if PageControl1.ActivePage=TabData then
  begin
    AdjustExtension(GetDataExtension(GetDataFilterIndex));
    SaveDataToFile(tmpName);
  end;

  TeeEmailFile(tmpName);
  DeleteFile(tmpName);
{$ENDIF}
end;

{ TTeeExportFormat }
procedure TTeeExportFormat.CopyToClipboard;
begin
  if Assigned(Panel) then
  begin
    Options;
    CheckSize;
    DoCopyToClipboard;
  end
  else raise Exception.Create(TeeMsg_ExportPanelNotSet);
end;

Destructor TTeeExportFormat.Destroy;
begin
  Options.Free;
  inherited;
end;

function TTeeExportFormat.FileFilterIndex: Integer;
begin
  result:=FFilterIndex;
end;

procedure TTeeExportFormat.IncFileFilterIndex(var FilterIndex: Integer);
begin
  Inc(FilterIndex);
  FFilterIndex:=FilterIndex;
end;

function TTeeExportFormat.Options: TForm;
begin
  result:=nil;
end;

procedure TTeeExportFormat.SaveToFile(const FileName: String);
var tmpStream : TStream;
begin
  if Assigned(Panel) then
  begin
    Options;
    CheckSize;
    tmpStream:=TFileStream.Create(Filename, fmCreate);
    try
      SaveToStream(tmpStream);
    finally
      tmpStream.Free;
    end;
  end
  else raise Exception.Create(TeeMsg_ExportPanelNotSet);
end;

Procedure TTeeExportFormat.CheckSize;
begin
  if Width=0 then Width:=Panel.Width;
  if Height=0 then Height:=Panel.Height;
end;

{ TTeeExportFormats }
function TTeeExportFormats.Get(Index: Integer): TTeeExportFormatClass;
begin
  result:=TTeeExportFormatClass(Items[Index]);
end;

{ Tools }
Procedure RegisterTeeExportFormat(AFormat:TTeeExportFormatClass);
begin
  if not Assigned(TeeExportFormats) then
     TeeExportFormats:=TTeeExportFormats.Create;
  TeeExportFormats.Add(AFormat);
end;

Procedure UnRegisterTeeExportFormat(AFormat:TTeeExportFormatClass);
begin
  if Assigned(TeeExportFormats) then TeeExportFormats.Remove(AFormat);
end;

Procedure TeeExport(AOwner:TComponent; APanel:TCustomTeePanel);
begin
  With TTeeExportForm.Create(AOwner) do
  try
    ExportPanel:=APanel;
    ShowModal;
  finally
    Free;
  end;
end;

Procedure TeeSavePanel(AFormat:TTeeExportFormatClass; APanel:TCustomTeePanel);
begin
  With AFormat.Create do
  try
    Panel:=APanel;
    With TSaveDialog.Create(nil) do
    try
      {$IFNDEF CLX}
      Filter:=FileFilter;
      {$ENDIF}
      DefaultExt:=FileExtension;
      if Execute then SaveToFile(FileName);
    finally
      Free;
    end;
  finally
    Free;
  end;
end;

Procedure TeeEmailFile(Const FileName:String);
{$IFNDEF LINUX}
var tmpName2     : TPathName;
    MapiMessage  : TMapiMessage;
    MapiFileDesc : TMapiFileDesc;
    MError       : Cardinal;
{$ENDIF}
begin
 {$IFNDEF LINUX}
  With MapiFileDesc do
  begin
    ulReserved:=0;
    flFlags:=0;
    nPosition:=Cardinal(-1);
    lpszPathName:=PChar(FileName);
    StrPCopy(tmpName2,ExtractFileName(FileName));
    lpszFileName:=tmpName2;
    lpFileType:=nil;
  end;

  with MapiMessage do
  begin
    ulReserved := 0;
    lpszSubject := PChar(TeeMsg_TeeChartPalette);
    lpszNoteText := nil;
    lpszMessageType := nil;
    lpszDateReceived := nil;
    lpszConversationID := nil;
    flFlags := 0;
    lpOriginator := nil;
    nRecipCount := 0;
    lpRecips := nil;
    nFileCount := 1;
    lpFiles := @MapiFileDesc;
  end;

  MError:=MapiSendMail(0, {$IFDEF CLX}0{$ELSE}Application.Handle{$ENDIF},
    MapiMessage,
    MAPI_DIALOG or MAPI_LOGON_UI or MAPI_NEW_SESSION, 0);

  if MError<>0 then
     Raise Exception.CreateFmt(TeeMsg_CanNotEmailChart,[MError]);
  {$ENDIF}
end;

initialization
finalization
  FreeAndNil(TeeExportFormats);
end.
