{********************************************}
{      TeeOpenGL Editor Component            }
{   Copyright (c) 1999-2000 by David Berneda }
{         All Rights Reserved                }
{********************************************}
{$I teedefs.inc}
unit TeeGLEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, StdCtrls, TeeOpenGL, TeCanvas;

type
  TTeeOpenGLBackup=packed record
    Active                 : Boolean;
    AmbientLight           : Integer;
    FontOutlines           : Boolean;
    LightColor             : TColor;
    LightVisible           : Boolean;
    LightX                 : Double;
    LightY                 : Double;
    LightZ                 : Double;
    ShadeQuality           : Boolean;
    Shininess              : Double;
    TeeOpenGLFontExtrusion : Integer;
  end;

  TFormTeeGLEditor = class(TForm)
    CheckBox1: TCheckBox;
    Label1: TLabel;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    CheckBox4: TCheckBox;
    Label4: TLabel;
    TrackBar1: TTrackBar;
    TrackBar2: TTrackBar;
    TrackBar3: TTrackBar;
    Label5: TLabel;
    Label6: TLabel;
    TrackBar4: TTrackBar;
    Label7: TLabel;
    TrackBar5: TTrackBar;
    TrackBar6: TTrackBar;
    BOK: TButton;
    BCancel: TButton;
    Label8: TLabel;
    UpDown1: TUpDown;
    Edit1: TEdit;
    BLightColor: TButtonColor;
    procedure BOKClick(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure TrackBar6Change(Sender: TObject);
    procedure TrackBar4Change(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox4Click(Sender: TObject);
    procedure TrackBar5Change(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure TrackBar2Change(Sender: TObject);
    procedure TrackBar3Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    CreatingForm : Boolean;
    GL           : TTeeOpenGL;
    GLBackup     : TTeeOpenGLBackup;
  public
    { Public declarations }
  end;

Function EditTeeOpenGL(AOwner:TComponent; ATeeOpenGL:TTeeOpenGL):Boolean;

implementation

{$R *.DFM}
Uses TeeGLCanvas, TeeProcs, TeeEditCha, TeePenDlg;

Function EditTeeOpenGL(AOwner:TComponent; ATeeOpenGL:TTeeOpenGL):Boolean;
begin
  With TFormTeeGLEditor.Create(AOwner) do
  try
    Tag:=Integer(ATeeOpenGL);
    result:=ShowModal=mrOk;
  finally
    Free;
  end
end;

procedure TFormTeeGLEditor.BOKClick(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TFormTeeGLEditor.CheckBox3Click(Sender: TObject);
begin
  if not CreatingForm then GL.ShadeQuality:=CheckBox3.Checked;
end;

procedure TFormTeeGLEditor.CheckBox2Click(Sender: TObject);
begin
  if not CreatingForm then GL.FontOutlines:=CheckBox2.Checked;
end;

procedure TFormTeeGLEditor.Edit1Change(Sender: TObject);
begin
  if not CreatingForm then
  begin
    TeeOpenGLFontExtrusion:=UpDown1.Position;
    GL.Canvas.DeleteFont;
    GL.TeePanel.Repaint;
  end;
end;

procedure TFormTeeGLEditor.TrackBar6Change(Sender: TObject);
begin
  if not CreatingForm then GL.Shininess:=TrackBar6.Position/100.0;
end;

procedure TFormTeeGLEditor.TrackBar4Change(Sender: TObject);
begin
  if not CreatingForm then GL.AmbientLight:=TrackBar4.Position;
end;

procedure TFormTeeGLEditor.CheckBox1Click(Sender: TObject);
begin
  if not CreatingForm then GL.Active:=CheckBox1.Checked;
end;

procedure TFormTeeGLEditor.CheckBox4Click(Sender: TObject);
begin
  if not CreatingForm then GL.Light.Visible:=CheckBox4.Checked;
end;

procedure TFormTeeGLEditor.TrackBar5Change(Sender: TObject);
begin
  if not CreatingForm then
  With GL.Light,TrackBar5 do
  begin
    Color:=RGB(Position,Position,Position);
    BLightColor.Repaint;
  end;
end;

procedure TFormTeeGLEditor.TrackBar1Change(Sender: TObject);
begin
  if not CreatingForm then GL.Light.Position.X:=TrackBar1.Position;
end;

procedure TFormTeeGLEditor.TrackBar2Change(Sender: TObject);
begin
  if not CreatingForm then GL.Light.Position.Y:=TrackBar2.Position;
end;

procedure TFormTeeGLEditor.TrackBar3Change(Sender: TObject);
begin
  if not CreatingForm then GL.Light.Position.Z:=TrackBar3.Position;
end;

procedure TFormTeeGLEditor.FormShow(Sender: TObject);
begin
  GL:=TTeeOpenGL(Tag);
  With GL do
  begin
    CheckBox3.Checked:=ShadeQuality;
    CheckBox2.Checked:=FontOutlines;
    UpDown1.Position:=TeeOpenGLFontExtrusion;
    TrackBar6.Position:=Round(Shininess*100.0);
    TrackBar4.Position:=AmbientLight;
    CheckBox1.Checked:=Active;
    CheckBox4.Checked:=Light.Visible;
    TrackBar5.Position:=GetRValue(Light.Color);
    with Light.Position do
    begin
      TrackBar1.Position:=Round(X);
      TrackBar2.Position:=Round(Y);
      TrackBar3.Position:=Round(Z);
    end;

    GLBackup.ShadeQuality:=ShadeQuality;
    GLBackup.FontOutlines:=FontOutlines;
    GLBackup.TeeOpenGLFontExtrusion:=TeeOpenGLFontExtrusion;
    GLBackup.Shininess:=Shininess;
    GLBackup.AmbientLight:=AmbientLight;
    GLBackup.Active:=Active;
    GLBackup.LightVisible:=Light.Visible;
    GLBackup.LightColor:=Light.Color;
    with Light.Position do
    begin
      GLBackup.LightX:=X;
      GLBackup.LightY:=Y;
      GLBackup.LightZ:=Z;
    end;
  end;
  BLightColor.LinkProperty(GL.Light,'Color');
  CreatingForm:=False;
end;

procedure TFormTeeGLEditor.BCancelClick(Sender: TObject);
begin
  With GL do
  begin
    ShadeQuality:=GLBackup.ShadeQuality;
    FontOutlines:=GLBackup.FontOutlines;
    if TeeOpenGLFontExtrusion<>GLBackup.TeeOpenGLFontExtrusion then
    begin
      TeeOpenGLFontExtrusion:=GLBackup.TeeOpenGLFontExtrusion;
      Canvas.DeleteFont;
    end;
    Shininess     :=GLBackup.Shininess;
    AmbientLight  :=GLBackup.AmbientLight;
    Active        :=GLBackup.Active;
    With Light do
    begin
      Visible:=GLBackup.LightVisible;
      Color  :=GLBackup.LightColor;
      with Position do
      begin
        X:=GLBackup.LightX;
        Y:=GLBackup.LightY;
        Z:=GLBackup.LightZ;
      end;
    end;
  end;
  ModalResult:=mrCancel;
end;

procedure TFormTeeGLEditor.FormCreate(Sender: TObject);
begin
  CreatingForm:=True;
end;

type TPrivateGLCanvas=class(TGLCanvas)
     end;

Procedure TeeOpenGLShowEditor(Editor:TChartEditForm);
var tmp     : TTabSheet;
    tmpForm : TFormTeeGLEditor;
begin
  if Assigned(Editor) and Assigned(Editor.Chart) then
  if Editor.Chart.Canvas.SupportsFullRotation then
  begin
    tmp:=TTabSheet.Create(Editor);
    tmp.Caption:='OpenGL';
    tmp.PageControl:=Editor.MainPage;
    tmpForm:=TFormTeeGLEditor.Create(Editor);
    ShowControls(False,[tmpForm.BOk,tmpForm.BCancel]);
    AddFormTo(tmpForm,tmp,Integer(TPrivateGLCanvas(Editor.Chart.Canvas).GLComponent));
  end;
end;

initialization
  TeeOnShowEditor:=TeeOpenGLShowEditor;
finalization
  TeeOnShowEditor:=nil;
end.
