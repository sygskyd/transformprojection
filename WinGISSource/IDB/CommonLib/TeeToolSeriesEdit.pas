{******************************************}
{ TSeriesTool Editor Dialog                }
{ Copyright (c) 1999-2000 by David Berneda }
{******************************************}
{$I teedefs.inc}
unit TeeToolSeriesEdit;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, TeeTools, StdCtrls;

type
  TSeriesToolEditor = class(TForm)
    Label1: TLabel;
    CBSeries: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure CBSeriesChange(Sender: TObject);
  private
    { Private declarations }
  protected
    Tool : TTeeCustomToolSeries;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
Uses TeEngine, TeeConst, TeeProco;

procedure TSeriesToolEditor.FormShow(Sender: TObject);
begin
  Tool:=TTeeCustomToolSeries(Tag);
  With Tool,CBSeries do
  begin
    FillSeriesItems(Items,ParentChart);
    Enabled:=ParentChart.SeriesCount>0;
    Items.InsertObject(0,TeeMsg_None,nil);
    ItemIndex:=Items.IndexOfObject(Series);
  end;
end;

procedure TSeriesToolEditor.CBSeriesChange(Sender: TObject);
begin
  With CBSeries do Tool.Series:=TChartSeries(Items.Objects[ItemIndex]);
end;

initialization
  RegisterClass(TSeriesToolEditor);
end.
