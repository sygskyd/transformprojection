{*********************************************}
{      TeeChart Pro OpenGL Component          }
{   Copyright (c) 1998-2000 by David Berneda  }
{         All Rights Reserved                 }
{*********************************************}
{$I teedefs.inc}
unit TeeOpenGL;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, TeeProcs, StdCtrls,
  ExtCtrls {$IFNDEF CLX},TeeGLCanvas{$ENDIF};

type
    TTeeOpenGL=class;

    TGLPosition=class(TPersistent)
    private
      FX       : Double;
      FY       : Double;
      FZ       : Double;
      FOwner   : TTeeOpenGL;
    protected
      Procedure SetX(Const Value:Double);
      Procedure SetY(Const Value:Double);
      Procedure SetZ(Const Value:Double);
    public
      Procedure Assign(Source:TPersistent); override;
    published
      property X:Double read FX write SetX;
      property Y:Double read FY write SetY;
      property Z:Double read FZ write SetZ;
    end;

    TGLLight=class(TPersistent)
    private
      FColor   : TColor;
      FVisible : Boolean;

      IOwner   : TTeeOpenGL;
    protected
      Procedure SetColor(Value:TColor);
      procedure SetVisible(Value:Boolean);
    public
      Constructor Create(AOwner:TTeeOpenGL);
      Procedure Assign(Source:TPersistent); override;
      Function GLColor:GLMat;
    published
      property Color:TColor read FColor write SetColor default clSilver;
      property Visible:Boolean read FVisible write SetVisible default True;
    end;

    TGLLightSource=class(TGLLight)
    private
      FPosition  : TGLPosition;
    protected
      Procedure SetPosition(Value:TGLPosition);
    public
      Constructor Create(AOwner:TTeeOpenGL);
      Destructor Destroy; override;
      Procedure Assign(Source:TPersistent); override;
    published
      property Position:TGLPosition read FPosition write SetPosition;
    end;

    TTeeOpenGL = class(TComponent)
    private
      { Private declarations }
      FActive       : Boolean;
      FAmbientLight : Integer;
      FFontOutlines : Boolean;
      FLight        : TGLLightSource;
      FShadeQuality : Boolean;
      FShininess    : Double;
      FTeePanel     : TCustomTeePanel;

      FOnInit       : TNotifyEvent;

      Function GetCanvas:TGLCanvas;
      Procedure SetActive(Value:Boolean);
      Procedure SetAmbientLight(Value:Integer);
      Procedure SetFontOutlines(Value:Boolean);
      Procedure SetLightSource(Value:TGLLightSource);
      Procedure SetShadeQuality(Value:Boolean);
      Procedure SetShininess(Const Value:Double);
      Procedure SetTeePanel(Value:TCustomTeePanel);

      Procedure Activate;
      Procedure OnCanvasInit(Sender:TObject);
      Procedure SetDoubleProperty(Var Variable:Double; Const Value:Double);
    protected
      { Protected declarations }
      procedure Notification( AComponent: TComponent;
                              Operation: TOperation); override;
      Procedure Repaint;
    public
      { Public declarations }
      Constructor Create(AOwner:TComponent); override;
      Destructor Destroy; override;
      property Canvas:TGLCanvas read GetCanvas;
    published
      { Published declarations }
      property Active:Boolean read FActive write SetActive default False;
      property AmbientLight:Integer read FAmbientLight write SetAmbientLight;
      property FontOutlines:Boolean read FFontOutlines write SetFontOutlines;
      property Light:TGLLightSource read FLight write SetLightSource;
      property ShadeQuality:Boolean read FShadeQuality write SetShadeQuality default True;
      property Shininess:Double read FShininess write SetShininess;
      property TeePanel:TCustomTeePanel read FTeePanel write SetTeePanel;

      property OnInit:TNotifyEvent read FOnInit write FOnInit;
    end;

implementation

{$R TeeGL.res}

Uses Controls, TeeConst, TeCanvas;

{ TGLPosition }
Procedure TGLPosition.SetX(Const Value:Double);
begin
  FOwner.SetDoubleProperty(FX,Value);
end;

Procedure TGLPosition.SetY(Const Value:Double);
begin
  FOwner.SetDoubleProperty(FY,Value);
end;

Procedure TGLPosition.SetZ(Const Value:Double);
begin
  FOwner.SetDoubleProperty(FZ,Value);
end;

Procedure TGLPosition.Assign(Source:TPersistent);
begin
  if Source is TGLPosition then
  With TGLPosition(Source) do
  begin
    Self.FX:=X;
    Self.FY:=Y;
    Self.FZ:=Z;
  end
  else inherited;
end;

{ TGLLightSource }
Constructor TGLLightSource.Create(AOwner:TTeeOpenGL);
begin
  inherited;
  FPosition:=TGLPosition.Create;
  With FPosition do
  begin
    FOwner:=Self.IOwner;
    FX:=-100;
    FY:=-100;
    FZ:=-100;
  end;
end;

Destructor TGLLightSource.Destroy;
begin
  FPosition.Free;
  inherited;
end;

Procedure TGLLightSource.SetPosition(Value:TGLPosition);
begin
  FPosition.Assign(Value);
end;

Procedure TGLLightSource.Assign(Source:TPersistent);
begin
  if Source is TGLLightSource then
  With TGLLightSource(Source) do
  begin
    Self.Position:=Position;
  end;
  inherited;
end;

{ TGLLight }
Constructor TGLLight.Create(AOwner:TTeeOpenGL);
begin
  inherited Create;
  IOwner:=AOwner;
  FColor:=clGray;
  FVisible:=True;
end;

Procedure TGLLight.SetColor(Value:TColor);
begin
  if FColor<>Value then
  begin
    FColor:=Value;
    IOwner.Repaint;
  end;
end;

procedure TGLLight.SetVisible(Value:Boolean);
begin
  if FVisible<>Value then
  begin
    FVisible:=Value;
    IOwner.Repaint;
  end;
end;

Function TGLLight.GLColor:GLMat;
var AColor : TColor;
begin
  AColor:=ColorToRGB(FColor);
  result[0]:=GetRValue(AColor)/255.0;
  result[1]:=GetGValue(AColor)/255.0;
  result[2]:=GetBValue(AColor)/255.0;
  result[3]:=1;
end;

Procedure TGLLight.Assign(Source:TPersistent);
begin
  if Source is TGLLight then
  With TGLLight(Source) do
  begin
    Self.FColor:=Color;
    Self.FVisible:=Visible;
  end
  else inherited;
end;

{ TTeeOpenGL }
Constructor TTeeOpenGL.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  FLight:=TGLLightSource.Create(Self);
  FAmbientLight:=8; {%}
  FShininess:=0.5;
  FShadeQuality:=True;
  FFontOutlines:=False;
end;

Destructor TTeeOpenGL.Destroy;
begin
  FLight.Free;
  if FActive and Assigned(FTeePanel) then
     FTeePanel.Canvas:=TTeeCanvas3D.Create;
  inherited;
end;

Function TTeeOpenGL.GetCanvas:TGLCanvas;
begin
  if Assigned(FTeePanel) and (FTeePanel.Canvas is TGLCanvas) then
     result:=TGLCanvas(FTeePanel.Canvas)
  else
     result:=nil;
end;

type TPrivateGLCanvas=class(TGLCanvas)
     end;

Procedure TTeeOpenGL.OnCanvasInit(Sender:TObject);
begin
  TPrivateGLCanvas(Canvas).GLComponent:=Self;
  TPrivateGLCanvas(Canvas).InitAmbientLight(FAmbientLight);
  With FLight do
  if Visible then
     With Position do TPrivateGLCanvas(Canvas).InitLight0(GLColor,X,Y,Z);

  TPrivateGLCanvas(Canvas).SetShininess(FShininess);
  if Assigned(FOnInit) then FOnInit(Self);
end;

Procedure TTeeOpenGL.SetShininess(Const Value:Double);
begin
  SetDoubleProperty(FShininess,Value);
  if Assigned(FTeePanel) then FTeePanel.Repaint;
end;

Procedure TTeeOpenGL.SetShadeQuality(Value:Boolean);
begin
  if FShadeQuality<>Value then
  begin
    FShadeQuality:=Value;
    if FActive and Assigned(FTeePanel) then
    With Canvas do
    begin
      ShadeQuality:=Value;
      Repaint;
    end;
  end;
end;

procedure TTeeOpenGL.Notification( AComponent: TComponent;
                                   Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
     if Assigned(FTeePanel) and (AComponent=FTeePanel) then
        TeePanel:=nil;
end;

Procedure TTeeOpenGL.Activate;
var tmpCanvas : TGLCanvas;
begin
  if Assigned(FTeePanel) then
    if FActive then
    begin
      tmpCanvas:=TGLCanvas.Create;
      With tmpCanvas do
      begin
        FontOutlines:=Self.FFontOutlines;
        ShadeQuality:=Self.FShadeQuality;
        OnInit:=OnCanvasInit;
      end;
      FTeePanel.Canvas:=tmpCanvas;
    end
    else FTeePanel.Canvas:=TTeeCanvas3D.Create;
end;

Procedure TTeeOpenGL.SetAmbientLight(Value:Integer);
begin
  if FAmbientLight<>Value then
  begin
    FAmbientLight:=Value;
    Repaint;
  end;
end;

Procedure TTeeOpenGL.SetFontOutlines(Value:Boolean);
begin
  FFontOutlines:=Value;
  if FActive and Assigned(FTeePanel) then
  With Canvas do
  begin
    DeleteFont;
    FontOutlines:=Value;
    Repaint;
  end;
end;

Procedure TTeeOpenGL.SetLightSource(Value:TGLLightSource);
begin
  FLight.Assign(Value);
end;

Procedure TTeeOpenGL.SetActive(Value:Boolean);
begin
  if FActive<>Value then
  begin
    FActive:=Value;
    Activate;
  end;
end;

Procedure TTeeOpenGL.Repaint;
begin
  if Assigned(FTeePanel) then FTeePanel.Repaint;
end;

Procedure TTeeOpenGL.SetTeePanel(Value:TCustomTeePanel);
begin
  FTeePanel:=Value;
  if Assigned(FTeePanel) then
  begin
    FTeePanel.FreeNotification(Self);
    Activate;
  end
  else FActive:=False;
end;

Procedure TTeeOpenGL.SetDoubleProperty(Var Variable:Double; Const Value:Double);
begin
  if Variable<>Value then
  begin
    Variable:=Value;
    Repaint;
  end;
end;

end.

