{******************************************}
{ TeeChart Tools Editor                    }
{ Copyright (c) 2000 by David Berneda      }
{******************************************}
{$I teedefs.inc}
unit TeeEditTools;

interface

uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     Windows, Messages,
     {$ENDIF}
     SysUtils, Classes,
     {$IFDEF CLX}
     QGraphics, QControls, QForms, QDialogs, QExtCtrls, QStdCtrls, QButtons,
     {$ELSE}
     Graphics, Controls, Forms, Dialogs, ExtCtrls, StdCtrls, Buttons,
     {$ENDIF}
     TeCanvas, TeEngine, Chart, TeePenDlg, TeeProcs;

type
  TFormTeeTools = class(TForm)
    LBTools: TListBox;
    Panel1: TPanel;
    PTop: TPanel;
    BAdd: TButtonColor;
    BDelete: TButtonColor;
    CBActive: TCheckBox;
    BMoveUp: TSpeedButton;
    BMoveDown: TSpeedButton;
    PBottom: TPanel;
    Splitter1: TSplitter;
    procedure LBToolsClick(Sender: TObject);
    procedure BDeleteClick(Sender: TObject);
    procedure CBActiveClick(Sender: TObject);
    procedure BAddClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BMoveUpClick(Sender: TObject);
    procedure BMoveDownClick(Sender: TObject);
    {$IFNDEF CLX}
    procedure LBToolsDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    {$ENDIF}
  private
    { Private declarations }
    Function CurrentTool:TTeeCustomTool;
    procedure DeleteForm;
    procedure EnableButtons;
    procedure FillTools;
    procedure SwapTool(A,B:Integer);
  public
    { Public declarations }
    Chart : TCustomChart;
  end;

implementation

{$R *.dfm}
Uses TeeConst, TeeToolsGallery;

procedure TFormTeeTools.FillTools;
var t : Integer;
begin
  PBottom.Caption:='';
  With LBTools do
  begin
    Clear;
    for t:=0 to Chart.Tools.Count-1 do
        Items.AddObject(Chart.Tools[t].Description,Chart.Tools[t]);
  end;
  CBActive.Enabled:=Chart.Tools.Count>0;
  BDelete.Enabled:=CBActive.Enabled;
end;

type TToolAccess=class(TTeeCustomTool);

procedure TFormTeeTools.DeleteForm;
begin
  if Panel1.ControlCount>0 then Panel1.Controls[0].Free;
end;

procedure TFormTeeTools.LBToolsClick(Sender: TObject);
var tmpClass : TClass;
begin
  CBActive.Checked:=CurrentTool.Active;
  DeleteForm;
  tmpClass:=GetClass(TToolAccess(CurrentTool).GetEditorClass);
  if Assigned(tmpClass) then
     AddFormTo(TFormClass(tmpClass).Create(Self),Panel1,Integer(CurrentTool));
  EnableButtons;
  if PBottom.Visible and (CurrentTool<>nil) then
     PBottom.Caption:=CurrentTool.Name;
end;

procedure TFormTeeTools.BDeleteClick(Sender: TObject);
begin
  if TeeYesNo(Format(TeeMsg_SureToDelete,[CurrentTool.Description])) then
  begin
    CurrentTool.Free;
    DeleteForm;
    FillTools;
  end;
end;

procedure TFormTeeTools.CBActiveClick(Sender: TObject);
begin
  CurrentTool.Active:=CBActive.Checked;
end;

procedure TFormTeeTools.BAddClick(Sender: TObject);
begin
  With TTeeToolsGallery.Create(Self) do
  try
    if ShowModal=mrOk then
    begin
      With SelectedTool.Create(Chart.Owner) do
      begin
        ParentChart:=Chart;
        Name:=TeeGetUniqueName(Owner,TeeMsg_DefaultToolName);
      end;
      FillTools;
      LBTools.ItemIndex:=LBTools.Items.Count-1;
      LBTools.SetFocus;
      LBToolsClick(Self);
    end;
  finally
    Free;
  end;
end;

function TFormTeeTools.CurrentTool: TTeeCustomTool;
begin
  result:=Chart.Tools[LBTools.ItemIndex]
end;

procedure TFormTeeTools.FormShow(Sender: TObject);
begin
  Chart:=TCustomChart(Tag);
  PBottom.Visible:=csDesigning in Chart.ComponentState;
  FillTools;
  With LBTools do
  if Items.Count>0 then
  begin
    ItemIndex:=0;
    LBToolsClick(Self);
  end
  else EnableButtons;
end;

procedure TFormTeeTools.FormCreate(Sender: TObject);
begin
  TeeLoadArrowBitmaps(BMoveUp.Glyph,BMoveDown.Glyph);
end;

procedure TFormTeeTools.SwapTool(A,B:Integer);
var tmpIndex : Integer;
begin
  LBTools.Items.Exchange(A,B);
  {$IFNDEF D4}
  LBTools.ItemIndex:=B;
  {$ENDIF}
  With Chart do
  begin
    Tools.Exchange(A,B);
    tmpIndex:=Tools[A].ComponentIndex;
    Tools[A].ComponentIndex:=Tools[B].ComponentIndex;
    Tools[B].ComponentIndex:=tmpIndex;
    Invalidate;
  end;
  EnableButtons;
end;

procedure TFormTeeTools.EnableButtons;
begin
  BMoveUp.Enabled:=LBTools.ItemIndex>0;
  BMoveDown.Enabled:=LBTools.ItemIndex<LBTools.Items.Count-1;
end;

procedure TFormTeeTools.BMoveUpClick(Sender: TObject);
begin
  With LBTools do SwapTool(ItemIndex,ItemIndex-1);
end;

procedure TFormTeeTools.BMoveDownClick(Sender: TObject);
begin
  With LBTools do SwapTool(ItemIndex,ItemIndex+1);
end;

{$IFNDEF CLX}
procedure TFormTeeTools.LBToolsDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  TeeDrawTool(LBTools,Index,Rect,State,Chart.Tools[Index]);
end;
{$ENDIF}

end.
