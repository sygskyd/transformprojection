{******************************************}
{    TeeChart. TBrushDialog                }
{ Copyright (c) 1996-2000 by David Berneda }
{******************************************}
{$I teedefs.inc}
unit TeeBrushDlg;

interface

uses
  {$IFDEF LINUX}
  LibC,
  {$ELSE}
  Windows, Messages,
  {$ENDIF}
  SysUtils, Classes,
  {$IFDEF CLX}
  QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls,
  {$ELSE}
  Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls,
  {$ENDIF}
  TeCanvas, TeePenDlg, MultiLng;

type
  TBrushDialog = class(TForm)
    Button2: TButton;
    BColor: TButtonColor;
    Button3: TButton;
    GroupBox2: TGroupBox;
    Button1: TButton;
    Image1: TImage;
    LStyle: TListBox;
    MlgSection1: TMlgSection;
    procedure FormShow(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
    procedure LStyleDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure LStyleClick(Sender: TObject);
  private
    { Private declarations }
    BackupBrush : TChartBrush;
    Procedure CheckImageButton;
    procedure RedrawShape;
  public
    { Public declarations }
    TheBrush : TChartBrush;
  end;

Procedure EditChartBrush( AOwner:TComponent;
                          ChartBrush:TChartBrush);

Procedure EditTeeFont(AOwner:TComponent; AFont:TTeeFont);

Function TeeGetPictureFileName(AOwner:TComponent):String;

procedure TeeLoadClearImage(AOwner:TComponent; AImage:TPicture);

Procedure GetTeeBrush(Index:Integer; ABitmap:TBitmap);

implementation

{$R *.dfm}

{$R TeeBrushes.res}

Uses TeeConst, TeeProcs
     {$IFNDEF CLX}
     ,ExtDlgs
     {$ENDIF}
     ;

Procedure EditTeeFont(AOwner:TComponent; AFont:TTeeFont);
Begin
  With TFontDialog.Create(AOwner) do
  try
    Font.Assign(AFont);
    if Execute then AFont.Assign(Font);
  finally
    Free;
  end;
end;

Procedure EditChartBrush( AOwner:TComponent;
                          ChartBrush:TChartBrush);
Begin
  With TBrushDialog.Create(AOwner) do
  try
    TheBrush:=ChartBrush;
    ShowModal;
  finally
    Free;
  end;
end;

Function TeeGetPictureFileName(AOwner:TComponent):String;
begin
  result:='';
  With {$IFDEF CLX}TOpenDialog{$ELSE}TOpenPictureDialog{$ENDIF}.Create(AOwner) do
  try
    if Execute then result:=FileName;
  finally
    Free;
  end;
end;

procedure TeeLoadClearImage(AOwner:TComponent; AImage:TPicture);
var tmp : String;
begin
  if Assigned(AImage.Graphic) then AImage.Assign(nil)
  else
  begin
    tmp:=TeeGetPictureFileName(AOwner);
    if tmp<>'' then AImage.LoadFromFile(tmp);
  end;
end;

{ TBrushDialog }
procedure TBrushDialog.RedrawShape;
begin
  BColor.Enabled:=TheBrush.Style<>bsClear;
end;

procedure TBrushDialog.FormShow(Sender: TObject);
begin
  BackupBrush:=TChartBrush.Create(nil);
  BackupBrush.Assign(TheBrush);
  LStyle.ItemIndex:=Ord(TheBrush.Style);
  BColor.LinkProperty(TheBrush,'Color');
  CheckImageButton;
  RedrawShape;
end;

procedure TBrushDialog.Button3Click(Sender: TObject);
begin
  TheBrush.Assign(BackupBrush);
  ModalResult:=mrCancel;
end;

procedure TBrushDialog.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  BackupBrush.Free;
end;

procedure TBrushDialog.Button1Click(Sender: TObject);
var tmp : String;
begin
  if LStyle.ItemIndex>7 then LStyle.ItemIndex:=Ord(TheBrush.Style);
  if Button1.Tag=1 then TheBrush.Image.Assign(nil)
  else
  begin
    tmp:=TeeGetPictureFileName(Self);
    if tmp<>'' then TheBrush.Image.LoadFromFile(tmp);
  end;
  CheckImageButton;
end;

Procedure TBrushDialog.CheckImageButton;
begin
  if Assigned(TheBrush.Image.Graphic) then
  begin
    Button1.Tag:=1;
    Button1.Caption:=TeeMsg_ClearImage;
    Image1.Picture.Assign(TheBrush.Image);
  end
  else
  begin
    Button1.Tag:=0;
    Button1.Caption:=TeeMsg_BrowseImage;
    Image1.Picture:=nil;
  end;
end;

procedure TBrushDialog.LStyleDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
{$IFNDEF CLX}
Var Old : TColor;
{$ENDIF}
begin
  With TControlCanvas(LStyle.Canvas) do
  begin
    FillRect(Rect);
    if Index<>1 then
    begin
      if Index>7 then
      begin
        Brush.Bitmap.Free;
        Brush.Bitmap:=TBitmap.Create;
        GetTeeBrush(Index-8,Brush.Bitmap);
      end
      else
      begin
        Brush.Style:=TBrushStyle(Index);
        Brush.Color:=clBlack;
      end;
      {$IFNDEF CLX}
      SetBkColor(Handle,clWhite);
      SetBkMode(Handle,Transparent);
      Old:=GetTextColor(Handle);
      SetTextColor(Handle,clBlack);
      {$ENDIF}
      With Rect do FillRect(Classes.Rect(Left+2,Top+2,Left+16,Bottom-2));
      Brush.Bitmap.Free;
      {$IFNDEF CLX}
      SetTextColor(Handle,Old);
      {$ENDIF}
    end;
    Brush.Style:=bsClear;
    {$IFNDEF CLX}
    {$IFDEF D4}
    UpdateTextFlags;
    {$ENDIF}
    {$ENDIF}
    TextOut(Rect.Left+20,Rect.Top+1,LStyle.Items[Index]);
  end;
end;

procedure TBrushDialog.LStyleClick(Sender: TObject);
begin
  TheBrush.Image.Assign(nil);
  if LStyle.ItemIndex>7 then
     GetTeeBrush(LStyle.ItemIndex-8,TheBrush.Image.Bitmap)
  else
     TheBrush.Style:=TBrushStyle(LStyle.ItemIndex);
  CheckImageButton;
  RedrawShape;
end;

Procedure GetTeeBrush(Index:Integer; ABitmap:TBitmap);
begin
  ABitmap.LoadFromResourceName(HInstance,'TeeBrush'+TeeStr(1+Index));
  ABitmap.TransparentMode:=tmFixed;
  ABitmap.TransparentColor:=clWhite;
end;

end.
