�
 TFORMPERIOD 0�  TPF0TFormPeriod
FormPeriodLeft>Top� 
AutoScrollClientHeighttClientWidth1Color	clBtnFace
ParentFont	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLCalcLeftTopWidth#HeightAutoSizeCaptionCalculate every:  TLabelBLabelLeftTop1WidthHeightCaption0  TEditENumLeft|TopWidthAHeightColorclWindowTabOrderText0OnChange
ENumChange  	TCheckBoxCBAllLeft� TopWidthAHeightCaptionAll &pointsTabOrderOnClick
CBAllClick  TRadioButtonRBNumLeftTopWidthiHeightCaption&Number of points:TabOrder OnClick
RBNumClick  TRadioButtonRBRangeLeftTop/WidthiHeightCaption&Range of values:TabOrderOnClickRBRangeClick  TButtonBChangeLeft� Top+WidthKHeightCaption
&Change...TabOrderOnClickBChangeClick  TRadioGroupRGAlignLeftTopGWidthHeight)Caption
Alignment:ColumnsItems.Strings&FirstC&enter&Last TabOrderOnClickRGAlignClick   