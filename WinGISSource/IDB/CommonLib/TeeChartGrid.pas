{******************************************}
{   TeeChart VCL Library                   }
{ Copyright (c) 1995-2000 by David Berneda }
{        All Rights Reserved               }
{******************************************}
{$I TeeDefs.inc}
unit TeeChartGrid;

interface

Uses {$IFDEF LINUX}
     LibC,
     {$ELSE}
     {$IFNDEF CLX}
     Windows, Messages,
     {$ENDIF}
     {$ENDIF}
     Classes, Chart, TeEngine, TeeProcs, TeCanvas, TeeNavigator,
     {$IFDEF CLX}
     QControls, QGrids, QGraphics, QExtCtrls, Qt, Types
     {$ELSE}
     Controls, Grids, Graphics, ExtCtrls
     {$ENDIF}
     ;

type
  TChartGridNavigator=class;

  TChartGridShow=(cgsAuto,cgsNo,cgsYes);

  TCustomChartGrid=class(TCustomGrid,ITeeEventListener)
  private
    FChart     : TCustomChart;
    FLabels    : Boolean;
    FXValues   : TChartGridShow;
    FOldValue  : String;
    FWasNull   : Boolean;

    IHasNo     : Array[0..1000] of Boolean;
    INavigator : TChartGridNavigator;
    FSeries: TChartSeries;
    procedure TeeEvent(Event: TTeeEvent);
    Function GetSeries(ACol:Integer; Var AList:TChartValueList):TChartSeries;
    procedure NotifyChange;
    procedure Regenerate;
    Procedure SetChart(AChart:TCustomChart);
    Procedure SetNavigator(ANavigator:TChartGridNavigator);
    Procedure SetShowLabels(Value:Boolean);
    Procedure SetShowXValues(Value:TChartGridShow);
    procedure SetSeries(const Value: TChartSeries);
  protected
    FActiveChanged   : TNotifyEvent;
    FSelectedChanged : TSelectCellEvent;

    function CanEditModify: Boolean; override;
    function CanEditShow: Boolean; override;
    procedure DrawCell(ACol, ARow: Integer; ARect: TRect;
                       AState: TGridDrawState); override;
    function GetEditText(ACol, ARow: Integer): string; override;
    Function HasPoints:Boolean;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure Notification( AComponent: TComponent;
                            Operation: TOperation); override;
    Procedure Loaded; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    {$IFNDEF CLX}
    {$IFNDEF D4}
    { IUnknown }
    function QueryInterface(const IID: TGUID; out Obj): Integer; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    {$ENDIF}
    {$ENDIF}
    function SelectCell(ACol, ARow: Integer): Boolean; override;
    procedure SetEditText(ACol, ARow: Integer; const Value: string); override;
  public
    Constructor Create(AOwner:TComponent); override;
    Destructor Destroy; override;

    Procedure Delete;
    Procedure Insert;
    Procedure RecalcDimensions;
    Procedure StartEditing;
    Procedure StopEditing(Cancel:Boolean);

    property Chart:TCustomChart read FChart write SetChart;
    property Series:TChartSeries read FSeries write SetSeries;
    property ShowLabels:Boolean read FLabels write SetShowLabels default True;
    property ShowXValues:TChartGridShow read FXValues write SetShowXValues default cgsAuto;

    property FixedCols default 1;
    property DefaultRowHeight default 16;
    property GridLineWidth default 1;
  end;

  TChartGrid=class(TCustomChartGrid)
  public
    property Col;
    property ColCount;
    property ColWidths;
    property EditorMode;
    property GridHeight;
    property GridWidth;
    property LeftCol;
    property Selection;
    property Row;
    property RowCount;
    property RowHeights;
    property TabStops;
    property TopRow;
  published
    property Align;
    {$IFDEF D4}
    property Anchors;
    {$IFNDEF CLX}
    property BiDiMode;
    {$ENDIF}
    {$ENDIF}
    property BorderStyle;
    property Color;
    {$IFDEF D4}
    property Constraints;
    {$ENDIF}
    {$IFNDEF CLX}
    property Ctl3D;
    {$ENDIF}
    property DefaultColWidth;
    property DefaultRowHeight;
    property DefaultDrawing;
    property DragMode;
    {$IFNDEF CLX}
    property DragCursor;
    {$IFDEF D4}
    property DragKind;
    {$ENDIF}
    {$ENDIF}
    property Enabled;
    property FixedColor;
    property Font;
    property GridLineWidth;
    property Options;
    {$IFDEF D4}
    {$IFNDEF CLX}
    property ParentBiDiMode;
    {$ENDIF}
    {$ENDIF}
    property ParentColor;
    {$IFNDEF CLX}
    property ParentCtl3D;
    {$ENDIF}
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ScrollBars;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    { events }
    property OnClick;
    {$IFDEF D5}
    property OnContextPopup;
    {$ENDIF}
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnStartDrag;
    {$IFDEF D4}
    {$IFNDEF CLX}
    property OnEndDock;
    {$ENDIF}
    {$ENDIF}
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    {$IFDEF D4}
    property OnMouseWheelDown;
    property OnMouseWheelUp;
    {$IFNDEF CLX}
    property OnStartDock;
    {$ENDIF}
    {$ENDIF}

    property Chart;
    property Series;
    property ShowLabels;
  end;

  TChartGridNavigator=class(TCustomChartNavigator)
  private
    FGrid : TChartGrid;
    procedure ActiveChanged(Sender:TObject);
    procedure EnableButtonsColRow(ACol,ARow:Integer);
    procedure SelectedChanged(Sender: TObject; ACol, ARow: Integer;
                              var CanSelect: Boolean);
    procedure SetGrid(AGrid:TChartGrid);
  protected
    procedure BtnClick(Index: TChartNavigateBtn); override;
    procedure Notification( AComponent: TComponent;
                            Operation: TOperation); override;
  public
    procedure EnableButtons; override;
  published
    property Grid:TChartGrid read FGrid write SetGrid;
  end;

implementation

Uses SysUtils, TeeStore, TeeConst;

{ TChartGridNavigator }
procedure TChartGridNavigator.Notification( AComponent: TComponent;
                                        Operation: TOperation);
begin
  inherited;
  if Operation=opRemove then
     if Assigned(FGrid) and (AComponent=FGrid) then
        Grid:=nil;
end;

procedure TChartGridNavigator.SetGrid(AGrid:TChartGrid);
begin
  FGrid:=AGrid;
  if Assigned(FGrid) then
  begin
    FGrid.FreeNotification(Self);
    FGrid.FActiveChanged:=ActiveChanged;
    FGrid.FSelectedChanged:=SelectedChanged;
    FGrid.SetNavigator(Self);
  end;
  EnableButtons;
end;

procedure TChartGridNavigator.ActiveChanged(Sender:TObject);
begin
  EnableButtons;
end;

type TCustomGridAccess=class(TStringGrid);

procedure TChartGridNavigator.BtnClick(Index: TChartNavigateBtn);
begin
  if Assigned(FGrid) then
  with FGrid do
  begin
    case Index of
      nbPrior : if Row>1 then Row:=Row-1;
      nbNext  : if Row<RowCount-1 then Row:=Row+1;
      nbFirst : if Row>1 then Row:=1;
      nbLast  : if Row<RowCount-1 then Row:=RowCount-1;
      nbInsert: Insert;
      nbEdit  : begin
                  SetFocus;
                  StartEditing;
                end;
      nbCancel: StopEditing(True);
      nbPost  : StopEditing(False);
      nbDelete: Delete;
    end;
  end;
  inherited;
end;

procedure TChartGridNavigator.EnableButtons;
begin
  inherited;
  if Assigned(FGrid) then EnableButtonsColRow(FGrid.Col,FGrid.Row);
end;

procedure TChartGridNavigator.EnableButtonsColRow(ACol,ARow:Integer);
var UpEnable : Boolean;
    DnEnable : Boolean;
begin
  UpEnable := ARow>1;
  DnEnable := ARow<FGrid.RowCount-1;
  Buttons[nbFirst].Enabled  := UpEnable;
  Buttons[nbPrior].Enabled  := UpEnable;
  Buttons[nbNext].Enabled   := DnEnable;
  Buttons[nbLast].Enabled   := DnEnable;
  Buttons[nbInsert].Enabled := (goEditing in FGrid.Options) and
                               (not FGrid.EditorMode) and
                               Assigned(FGrid.Chart)  and
                               (FGrid.Chart.SeriesCount>0);
  Buttons[nbDelete].Enabled := (FGrid.RowCount>1) and
                               Buttons[nbInsert].Enabled and
                               FGrid.HasPoints;
  Buttons[nbPost].Enabled   := FGrid.EditorMode;
  Buttons[nbEdit].Enabled   := Buttons[nbInsert].Enabled;
  Buttons[nbCancel].Enabled := FGrid.EditorMode;
end;

procedure TChartGridNavigator.SelectedChanged(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  EnableButtonsColRow(ACol,ARow);
end;

{ TCustomChartGrid }
Constructor TCustomChartGrid.Create(AOwner:TComponent);
begin
  inherited;
  FSaveCellExtents:=False;
  FOldValue:='';
  FLabels:=True;
  FXValues:=cgsAuto;
  FWasNull:=False;
  GridLineWidth:=1;
  ColWidths[0]:=30;
  DefaultRowHeight:=16;
  FixedCols:=1;
  RowHeights[0]:=20;

  Options:=[goHorzLine,
        goVertLine,
        goRangeSelect,
        goDrawFocusSelected,
        goRowSizing,
        goColSizing,
        goEditing,
        goTabs,
        goThumbTracking
        ];

  RecalcDimensions;
end;

Destructor TCustomChartGrid.Destroy;
begin
  Chart:=nil;
  inherited;
end;

Procedure TCustomChartGrid.Loaded;
begin
  inherited;
  {$IFNDEF D4}
  ColWidths[0]:=30;
  {$ENDIF}
  RowHeights[0]:=20;
end;

{$IFNDEF D4}
function TCustomChartGrid.QueryInterface(const IID: TGUID; out Obj): Integer; stdcall;
begin
  if VCLComObject = nil then
  begin
    if GetInterface(IID, Obj) then Result := S_OK
    else Result := E_NOINTERFACE
  end
  else
    Result := IVCLComObject(VCLComObject).QueryInterface(IID, Obj);
end;

function TCustomChartGrid._AddRef: Integer; stdcall;
begin
  if VCLComObject = nil then
    Result := -1   // -1 indicates no reference counting is taking place
  else
    Result := IVCLComObject(VCLComObject)._AddRef;
end;

function TCustomChartGrid._Release: Integer; stdcall;
begin
  if VCLComObject = nil then
    Result := -1   // -1 indicates no reference counting is taking place
  else
    Result := IVCLComObject(VCLComObject)._Release;
end;
{$ENDIF}

Function TCustomChartGrid.HasPoints:Boolean;
var t: Integer;
begin
  result:=False;
  if Assigned(FSeries) then result:=FSeries.Count>0
  else
  if Assigned(Chart) then
  With Chart do
  for t:=0 to SeriesCount-1 do
  if Series[t].Count>0 then
  begin
    result:=True;
    Exit;
  end;
end;

function TCustomChartGrid.SelectCell(ACol, ARow: Integer): Boolean;
begin
  result:=inherited SelectCell(ACol,ARow) and (ACol>0);
  if result then
     if Assigned(FSelectedChanged) then FSelectedChanged(Self,ACol,ARow,result);
end;

function TCustomChartGrid.CanEditModify: Boolean;
begin
  result:=inherited CanEditModify;
  if result then
     if Assigned(INavigator) then INavigator.ActiveChanged(Self);
end;

procedure TCustomChartGrid.MouseDown(Button: TMouseButton; Shift: TShiftState;
                                     X, Y: Integer);
begin
  inherited;
  if Assigned(INavigator) then INavigator.ActiveChanged(Self);
end;

Procedure TCustomChartGrid.StopEditing(Cancel:Boolean);
begin
  if EditorMode then
  begin
    HideEditor;
    if Cancel and (not FWasNull) then SetEditText(Col,Row,FOldValue);
    NotifyChange;
  end;
end;

Procedure TCustomChartGrid.StartEditing;
var tmpList   : TChartValueList;
    tmpSeries : TChartSeries;
begin
  if (goEditing in Options) and (not EditorMode) then
  begin
    if Col>0 then
    begin
      tmpSeries:=GetSeries(Col,tmpList);
      if Assigned(tmpSeries) then
      With tmpSeries do
      if DataSource=nil then
      begin
        FWasNull:=(Row>Count) or (IsNull(Row-1));
        if FWasNull then FOldValue:=''
                    else FOldValue:=GetEditText(Col,Row);
        EditorMode:=True;
        NotifyChange;
      end;
    end;
  end;
end;

procedure TCustomChartGrid.KeyDown(var Key: Word; Shift: TShiftState);
var tmpList   : TChartValueList;
    tmpSeries : TChartSeries;
begin
  if (not (ssCtrl in Shift)) then
  begin
    Case Key of
{$IFDEF CLX}Key_Escape{$ELSE}VK_ESCAPE{$ENDIF} : StopEditing(True);
{$IFDEF CLX}Key_F2{$ELSE}VK_F2{$ENDIF}     : StartEditing;
{$IFDEF CLX}Key_Up{$ELSE}VK_UP{$ENDIF}     : if (Row=RowCount-1) then
            begin
              tmpSeries:=GetSeries(Col,tmpList);
              if Assigned(tmpSeries) and
                 tmpSeries.IsNull(Row-1) and
                 (tmpSeries.XLabel[Row-1]='') then
              begin
                RowCount:=RowCount-1;
                tmpSeries.Delete(Row);
              end;
            end
            else inherited;
{$IFDEF CLX}Key_Down{$ELSE}VK_DOWN{$ENDIF}   : begin
              if Row=RowCount-1 then
              begin
                tmpSeries:=GetSeries(Col,tmpList);
                if Assigned(tmpSeries) then
                begin
                  tmpSeries.AddNull{$IFNDEF D4}(''){$ENDIF};
                  RowCount:=RowCount+1;
                  Row:=RowCount-1;
                end;
              end
              else inherited;
            end;
{$IFDEF CLX}Key_Insert{$ELSE}VK_INSERT{$ENDIF} : Insert;
{$IFDEF CLX}Key_Delete{$ELSE}VK_DELETE{$ENDIF} : if ssCtrl in Shift then Delete
            else
            begin
              if not EditorMode then EditorMode:=True;
              inherited;
            end;
    else inherited;
    end;
  end
  else inherited;
end;

function TCustomChartGrid.GetEditText(ACol, ARow: Integer): string;

  Function GetALabel(Index:Integer):String;
  var t : Integer;
  begin
    result:='';
    if Assigned(FSeries) then result:=FSeries.XLabel[Index]
    else
    for t:=0 to FChart.SeriesCount-1 do
    With FChart.Series[t] do
    if Labels.Count>Index then
    begin
      result:=XLabel[Index];
      break;
    end;
  end;

var tmpSeries : TChartSeries;
    tmp       : Integer;
    tmpList   : TChartValueList;
begin
  if ACol=0 then
  begin
    if ARow>0 then Str(ARow-1,Result)
              else result:='#';
  end
  else
  begin
    if FLabels then tmp:=1 else tmp:=0;
    if ARow=0 then
    begin
      if (ACol=1) and (tmp>0) then result:=TeeMsg_Text
      else
         result:='';
    end
    else
    begin
      if (ACol=1) and (tmp>0) then
         result:=GetALabel(ARow-1)
      else
      begin
        tmpSeries:=GetSeries(ACol,tmpList);
        if tmpSeries.Count>=ARow then
        begin
          if tmpSeries.IsNull(ARow-1) then
             result:=''
          else
          if tmpList.DateTime then
             result:=DateTimeToStr(tmpList.Value[ARow-1])
          else
             result:=FormatFloat(tmpSeries.ValueFormat,tmpList.Value[ARow-1])
        end
        else
           result:='';
      end;
    end;
  end;
end;

procedure TCustomChartGrid.SetEditText(ACol, ARow: Integer; const Value: string);

  Procedure SetALabel(Index:Integer; Const AValue:String);
  var t : Integer;
  begin
    if Assigned(FSeries) then
    begin
      FSeries.XLabel[Index]:=AValue;
      exit;
    end
    else
    for t:=0 to FChart.SeriesCount-1 do
    With FChart.Series[t] do
    if Labels.Count>Index then
    begin
      XLabel[Index]:=AValue;
      Exit;
    end;
    While Index>FChart.Series[0].Count-1 do FChart.Series[0].AddNull('');
    FChart.Series[0].XLabel[Index]:=AValue;
  end;

var tmpSeries : TChartSeries;
    tmp       : Integer;
    tmpList   : TChartValueList;
begin
  inherited;
  if Assigned(FSeries) or (Assigned(FChart) and (FChart.SeriesCount>0)) then
  try
    if FLabels then tmp:=1 else tmp:=0;
    if (ACol=1) and (tmp=1) then
       SetALabel(ARow-1,Value)
    else
    begin
      tmpSeries:=GetSeries(ACol,tmpList);
      While (ARow-1)>=tmpSeries.Count do tmpSeries.AddNull({$IFNDEF D4}''{$ENDIF});
      if Value='' then tmpSeries.ValueColor[ARow-1]:=clNone
      else
      begin
        if tmpList.DateTime then
           tmpList.Value[ARow-1]:=StrToDateTime(Value)
        else
           tmpList.Value[ARow-1]:=StrToFloat(Value);
        tmpList.Modified:=True;
        if tmpSeries.IsNull(ARow-1) then tmpSeries.ValueColor[ARow-1]:=clTeeColor;
      end;
      if Assigned(FSeries) then FSeries.Repaint
                           else FChart.Repaint;
    end;
  except
  end;
end;

Function TCustomChartGrid.GetSeries(ACol:Integer; Var AList:TChartValueList):TChartSeries;
var tmp  : Integer;
    tmp2 : Integer;
    t    : Integer;
begin
  if FLabels then tmp:=1 else tmp:=0;
  Dec(ACol,tmp);
  if Assigned(FSeries) then
  begin
    tmp2:=FSeries.ValuesList.Count;
    if not IHasNo[0] then Dec(tmp2);

    if (tmp2>=ACol) then
    begin
      result:=FSeries;
      if IHasNo[0] then
         AList:=result.ValuesList[ACol-1]
      else
      if result.YMandatory then
         AList:=result.ValuesList[ACol]
      else
         AList:=result.ValuesList[ACol-1];
      exit;
    end;
  end
  else
  for t:=0 to FChart.SeriesCount-1 do
  begin
    tmp2:=FChart.Series[t].ValuesList.Count;
    if not IHasNo[t] then Dec(tmp2);

    if (tmp2>=ACol) then
    begin
      result:=FChart.Series[t];
      if IHasNo[t] then
         AList:=result.ValuesList[ACol-1]
      else
      if result.YMandatory then
         AList:=result.ValuesList[ACol]
      else
         AList:=result.ValuesList[ACol-1];
      exit;
    end;
    Dec(ACol,tmp2);
  end;
  result:=nil;
end;

procedure TCustomChartGrid.DrawCell(ACol, ARow: Integer; ARect: TRect;
  AState: TGridDrawState);

  Function GetSeriesCol(ACol:Integer):TChartSeries;
  var tmp : Integer;
      t   : Integer;
  begin
    if FLabels then tmp:=1 else tmp:=0;
    if Assigned(FSeries) then
    begin
      result:=FSeries;
      exit;
    end
    else
    for t:=0 to FChart.SeriesCount-1 do
    begin
      Inc(tmp);
      if tmp=ACol then
      begin
        result:=FChart.Series[t];
        exit;
      end;
      Inc(tmp,FChart.Series[t].ValuesList.Count-2);
      if IHasNo[t] then Inc(tmp);
    end;
    result:=nil;
  end;

var tmp       : Integer;
    tmpSeries : TChartSeries;
    AList     : TChartValueList;
    tmpSt     : String;
begin
  if Assigned(FChart) or Assigned(FSeries) then
  begin
    if FLabels then tmp:=1 else tmp:=0;
    if (ARow=0) and (ACol>tmp) then
    begin
      tmpSeries:=GetSeriesCol(ACol);
      if Assigned(FSeries) then
      begin
        GetSeries(ACol,AList);
        Canvas.TextRect(ARect, ARect.Left+18, ARect.Top+4, AList.Name);
      end
      else
      if Assigned(tmpSeries) then
      begin
        With ARect do
           PaintSeriesLegend(tmpSeries,Canvas,Rect(Left+4,Top+4,Left+16,Top+16));
        {$IFDEF CLX}
        QPainter_setBackgroundMode(Canvas.Handle,BGMode_TransparentMode);
        {$ENDIF}
        Canvas.Brush.Style:=bsClear;
        Canvas.Pen.Style:=psSolid;
        Canvas.TextRect(ARect, ARect.Left+18, ARect.Top+4,
                        SeriesTitleOrName(tmpSeries));
      end;
    end
    else
    begin
      if ACol=0 then
      begin
        Canvas.Brush.Color:=FixedColor;
        Canvas.Brush.Style:=bsSolid;
        Canvas.FillRect(ARect);
      end;
      if (tmp=1) and (ACol=1) then
         Canvas.TextRect(ARect, ARect.Left+2, ARect.Top+2, GetEditText(ACol, ARow))
      else
      begin
        {$IFNDEF CLX}
        Windows.SetTextAlign(Canvas.Handle,TA_RIGHT);
        {$ENDIF}
        {$IFDEF CLX}
        QPainter_setBackgroundMode(Canvas.Handle,BGMode_TransparentMode);
        {$ENDIF}
        tmpSt:=GetEditText(ACol, ARow);
        Canvas.TextRect(ARect,
            ARect.Right-2{$IFDEF CLX}-Canvas.TextWidth(tmpSt)-4{$ENDIF},
            ARect.Top+2, tmpSt
           {$IFDEF CLX},Integer(AlignmentFlags_AlignRight){$ENDIF});
        {$IFNDEF CLX}
        Windows.SetTextAlign(Canvas.Handle,TA_LEFT);
        {$ENDIF}
      end;
    end;
  end;
end;

procedure TCustomChartGrid.Notification( AComponent: TComponent;
                                   Operation: TOperation);
begin
  inherited;
  if Operation=opRemove then
  begin
    if Assigned(FChart) and (AComponent=FChart) then
        Chart:=nil
    else
    if Assigned(INavigator) and (AComponent=INavigator) then
        INavigator:=nil
    else
    if Assigned(FSeries) and (AComponent=FSeries) then
        Series:=nil
  end;
end;

Procedure TCustomChartGrid.Delete;

  Procedure TryDelete(ASeries:TChartSeries; AIndex:Integer);
  begin
    With ASeries do
    if Count>0 then
    begin
      Delete(Row-1);
      if not IHasNo[AIndex] then NotMandatoryValueList.FillSequence;
    end;
  end;

var t : Integer;
begin
  if Assigned(FSeries) then
     TryDelete(FSeries,0)
  else
  With FChart do for t:=0 to SeriesCount-1 do TryDelete(Series[t],t);

  if RowCount>2 then RowCount:=RowCount-1 else
  begin
    Repaint;
    NotifyChange;
  end;
end;

Procedure TCustomChartGrid.Insert;
var tmpInc : Boolean;

  Procedure TryInsert(ASeries:TChartSeries);
  begin
    With ASeries do
    begin
      tmpInc:=Count>0;
      AddNullXY(Count,0{$IFNDEF D4},''{$ENDIF});
    end;
  end;

Var t : Integer;
begin
  tmpInc:=False;
  if Assigned(FSeries) then
     TryInsert(FSeries)
  else
  With FChart do for t:=0 to SeriesCount-1 do TryInsert(Series[t]);
  if tmpInc then RowCount:=RowCount+1;
  Row:=RowCount-1;
end;

type TTeePanelAccess=class(TCustomTeePanel);

Procedure TCustomChartGrid.SetChart(AChart:TCustomChart);
begin
  if Assigned(FChart) and (not (csDestroying in FChart.ComponentState)) then
     TTeePanelAccess(FChart).Listeners.Remove(Self);
  FChart:=AChart;
  if Assigned(FChart) then
  begin
    FChart.FreeNotification(Self);
    TTeePanelAccess(FChart).Listeners.Add(Self);
  end;
  Regenerate;
end;

Procedure TCustomChartGrid.SetNavigator(ANavigator:TChartGridNavigator);
begin
  INavigator:=ANavigator;
  if Assigned(INavigator) then INavigator.FreeNotification(Self);
end;

Procedure TCustomChartGrid.RecalcDimensions;

  Function MaxNumPoints:Integer;
  var t:Integer;
  begin
    result:=0;
    if Assigned(FSeries) then result:=FSeries.Count
    else
    for t:=0 to FChart.SeriesCount-1 do
    With FChart.Series[t] do if (t=0) or (Count>result) then result:=Count;
  end;

var tmpCol : Integer;

  Procedure CalcParams(ASeries:TChartSeries; AIndex:Integer);
  begin
    Inc(tmpCol);
    if FXValues=cgsAuto then
       IHasNo[AIndex]:=HasNoMandatoryValues(ASeries)
    else
       IHasNo[AIndex]:=FXValues=cgsYes;

    if IHasNo[AIndex] then Inc(tmpCol);
    Inc(tmpCol,ASeries.ValuesList.Count-2);
  end;

var t : Integer;
begin
  tmpCol:=1;
  if Assigned(FChart) or Assigned(FSeries) then
  begin
    if Assigned(FSeries) then CalcParams(FSeries,0)
    else
    for t:=0 to FChart.SeriesCount-1 do CalcParams(FChart[t],t);

    if FLabels then Inc(tmpCol);
    RowCount:=MaxLong(2,MaxNumPoints+1);
  end
  else RowCount:=2;
  ColCount:=tmpCol;
  NotifyChange;
end;

procedure TCustomChartGrid.NotifyChange;
begin
  if Assigned(FActiveChanged) then FActiveChanged(Self);
end;

procedure TCustomChartGrid.SetShowLabels(Value: Boolean);
begin
  FLabels:=Value;
  RecalcDimensions;
end;

Procedure TCustomChartGrid.SetShowXValues(Value:TChartGridShow);
begin
  FXValues:=Value;
  RecalcDimensions;
end;

procedure TCustomChartGrid.TeeEvent(Event: TTeeEvent);
begin
  if not (csDestroying in ComponentState) then
  if Event is TTeeSeriesEvent then
  Case TTeeSeriesEvent(Event).Event of
    seChangeTitle,
    seChangeColor,
    seChangeActive: Repaint
  else RecalcDimensions;
  end;
end;

function TCustomChartGrid.CanEditShow: Boolean;
begin
  result:=inherited CanEditShow;
  if result then FOldValue:=GetEditText(Col,Row);
end;

procedure TCustomChartGrid.Regenerate;
begin
  if not (csDestroying in ComponentState) then
  begin
    RecalcDimensions;
    Col:=MinLong(ColCount-1,1);
  end;
end;

procedure TCustomChartGrid.SetSeries(const Value: TChartSeries);
begin
  FSeries:=Value;
  if Assigned(FSeries) then
  begin
    FSeries.FreeNotification(Self);
    Chart:=nil;
  end;
  Regenerate;
  Repaint;
end;

end.
