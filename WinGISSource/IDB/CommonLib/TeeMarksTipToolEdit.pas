{******************************************}
{ TMarksTipTool Editor Dialog              }
{ Copyright (c) 1999-2000 by David Berneda }
{******************************************}
{$I teedefs.inc}
unit TeeMarksTipToolEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeeToolSeriesEdit, StdCtrls, ExtCtrls, ComCtrls
  {$IFDEF CLX}, TeeProcs{$ENDIF};

type
  TMarksTipToolEdit = class(TSeriesToolEditor)
    Label2: TLabel;
    CBMarkStyle: TComboBox;
    RGMouseAction: TRadioGroup;
    Label3: TLabel;
    EDelay: TEdit;
    UDDelay: TUpDown;
    Label4: TLabel;
    procedure FormShow(Sender: TObject);
    procedure CBMarkStyleChange(Sender: TObject);
    procedure RGMouseActionClick(Sender: TObject);
    procedure EDelayChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}
Uses TeEngine, TeeTools, TeeEdiSeri;

procedure TMarksTipToolEdit.FormShow(Sender: TObject);
begin
  inherited;
  RGMouseAction.ItemIndex:=Ord(TMarksTipTool(Tool).MouseAction);
  With TFormTeeSeries.Create(nil) do
  try
    Self.CBMarkStyle.Items.Assign(RGMarkStyle.Items);
  finally
    Free;
  end;
  CBMarkStyle.ItemIndex:=Ord(TMarksTipTool(Tool).Style);
  UDDelay.Position:=Application.HintPause;
end;

procedure TMarksTipToolEdit.CBMarkStyleChange(Sender: TObject);
begin
  TMarksTipTool(Tool).Style:=TSeriesMarksStyle(CBMarkStyle.ItemIndex);
end;

procedure TMarksTipToolEdit.RGMouseActionClick(Sender: TObject);
begin
  TMarksTipTool(Tool).MouseAction:=TMarkToolMouseAction(RGMouseAction.ItemIndex);
end;

procedure TMarksTipToolEdit.EDelayChange(Sender: TObject);
begin
  if Showing then Application.HintPause:=UDDelay.Position;
end;

initialization
  RegisterClass(TMarksTipToolEdit);
end.
