{**********************************************}
{  TChartGradient Editor Dialog                }
{  Copyright (c) 1999-2000 by David Berneda    }
{**********************************************}
{$I teedefs.inc}
unit TeeEdiGrad;

interface

uses
  {$IFDEF LINUX}
  LibC,
  {$ELSE}
  Windows, Messages,
  {$ENDIF}
  SysUtils, Classes,
  {$IFDEF CLX}
  QGraphics, QControls, QForms, QDialogs, QStdCtrls, QExtCtrls, QComCtrls,
  {$ELSE}
  Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, ComCtrls,
  {$ENDIF}
  TeeProcs, TeePenDlg, TeCanvas, MultiLng;

type
  TTeeGradientEditor = class(TForm)
    GB4: TGroupBox;
    Label1: TLabel;
    CBVisible: TCheckBox;
    CBDirection: TComboBox;
    BOk: TButton;
    BCancel: TButton;
    GroupBox1: TGroupBox;
    BStart: TButtonColor;
    BMid: TButtonColor;
    BEnd: TButtonColor;
    BSwap: TButton;
    CBMid: TCheckBox;
    MlgSection1: TMlgSection;
    procedure CBVisibleClick(Sender: TObject);
    procedure CBDirectionChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BCancelClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BSwapClick(Sender: TObject);
    procedure CBMidClick(Sender: TObject);
    procedure BMidClick(Sender: TObject);
  private
    { Private declarations }
    Backup : TCustomTeeGradient;
    IOnlyStart : Boolean;
    procedure CheckDirection;
    procedure CheckVisible;
  public
    { Public declarations }
    TheGradient : TCustomTeeGradient;
    Procedure RefreshGradient(AGradient:TCustomTeeGradient);
  end;

{$IFNDEF D4}
Procedure EditTeeGradientCustom( AOwner:TComponent;
                                 AGradient:TCustomTeeGradient;
                                 OnlyStart:Boolean);
Procedure EditTeeGradient(AOwner:TComponent;
                          AGradient:TCustomTeeGradient);
{$ELSE}
Procedure EditTeeGradient(AOwner:TComponent;
                          AGradient:TCustomTeeGradient;
                          OnlyStart:Boolean=False);
{$ENDIF}

implementation

{$R *.dfm}

{$IFNDEF D4}
Procedure EditTeeGradient(AOwner:TComponent; AGradient:TCustomTeeGradient);
begin
  EditTeeGradientCustom(AOwner,AGradient,False);
end;

Procedure EditTeeGradientCustom( AOwner:TComponent;
                                 AGradient:TCustomTeeGradient;
                                 OnlyStart:Boolean);
{$ELSE}
Procedure EditTeeGradient( AOwner:TComponent;
                           AGradient:TCustomTeeGradient;
                           OnlyStart:Boolean=False);
{$ENDIF}
begin
  With TTeeGradientEditor.Create(AOwner) do
  try
    Tag:=Integer(AGradient);
    IOnlyStart:=OnlyStart;
    if IOnlyStart then
    begin
      CBVisible.Visible:=False;
      BEnd.Visible:=False;
      BSwap.Visible:=False;
    end;
    ShowModal;
  finally
    Free;
  end;
end;

procedure TTeeGradientEditor.CheckVisible;
Begin
  EnableControls(IOnlyStart or TheGradient.Visible,
                 [CBDirection,BStart,CBMid,BMid,BEnd,BSwap]);
end;

procedure TTeeGradientEditor.CheckDirection;
var tmp : Boolean;
begin
  With TheGradient do
  begin
    tmp:=(Direction=gdFromCenter) or
         (Direction=gdFromTopLeft) or
         (Direction=gdFromBottomLeft);
    EnableControls(IOnlyStart or Visible and (not tmp),[CBMid,BMid]);
  end;
end;

procedure TTeeGradientEditor.CBVisibleClick(Sender: TObject);
begin
  TheGradient.Visible:=CBVisible.Checked;
  CheckVisible;
end;

procedure TTeeGradientEditor.CBDirectionChange(Sender: TObject);
begin
  TheGradient.Direction:=TGradientDirection(CBDirection.ItemIndex);
  CheckDirection;
end;

Procedure TTeeGradientEditor.RefreshGradient(AGradient:TCustomTeeGradient);
begin
  Tag:=Integer(AGradient);
  TheGradient:=AGradient;
  if Assigned(TheGradient) then
  begin
    Backup.Free;
    Backup:=TChartGradient.Create(nil);
    Backup.Assign(TheGradient);
    With TheGradient do
    begin
      CBVisible.Checked:=Visible;
      CBDirection.ItemIndex:=Ord(Direction);
      CBMid.Checked:=MidColor=clNone;
    end;
    BStart.LinkProperty(TheGradient,'StartColor');
    BMid.LinkProperty(TheGradient,'MidColor');
    BEnd.LinkProperty(TheGradient,'EndColor');
    CheckVisible;
    CheckDirection;
  end;
end;

procedure TTeeGradientEditor.FormShow(Sender: TObject);
begin
  RefreshGradient(TChartGradient(Tag));
end;

procedure TTeeGradientEditor.BCancelClick(Sender: TObject);
begin
  TheGradient.Assign(Backup);
  TheGradient.Changed(Self);
end;

procedure TTeeGradientEditor.FormDestroy(Sender: TObject);
begin
  Backup.Free;
end;

procedure TTeeGradientEditor.BSwapClick(Sender: TObject);
var tmp : TColor;
begin
  With TheGradient do
  begin
    tmp:=StartColor;
    StartColor:=EndColor;
    EndColor:=tmp;
  end;
  BStart.Repaint;
  BEnd.Repaint;
end;

procedure TTeeGradientEditor.CBMidClick(Sender: TObject);
begin
  if CBMid.Checked then
  begin
    TheGradient.MidColor:=clNone;
    BMid.Repaint;
  end;
end;

procedure TTeeGradientEditor.BMidClick(Sender: TObject);
begin
  CBMid.Checked:=TheGradient.MidColor=clNone;
end;

end.
