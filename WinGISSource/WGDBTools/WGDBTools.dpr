library WGDBTools;

uses
  SysUtils, Classes, DB, ADODB, Dialogs;

{$R *.res}

var
DBValues: AnsiString;

function ProgisIDToDBValues(AProjName: PChar; ALayerName: PChar; AProgisID: Integer): PChar; stdcall;
var
DBName: AnsiString;
CS: AnsiString;
DBTable: TADOTable;
i,n: Integer;
AFName,AFValue: AnsiString;
begin
        DBValues:='';
        DBName:=StringReplace(AProjName,'.amp','.wgi',[]);
        CS:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=%1;Persist Security Info=False;';
        CS:=StringReplace(CS,'%1',DBName,[]);
        if FileExists(DBName) then begin
                DBTable:=TADOTable.Create(nil);
                DBTable.TableDirect:=True;
                DBTable.ConnectionString:=CS;
                DBTable.TableName:='ATT_' + AnsiString(ALayerName);
                try
                        DBTable.Open;
                        if DBTable.Active then begin
                                if DBTable.Locate('ProgisID',IntToStr(AProgisID),[loCaseInsensitive]) then begin
                                        n:=DBTable.FieldCount;
                                        for i:=0 to n-1 do begin
                                                AFName:=DBTable.Fields[i].FullName;
                                                AFValue:=DBTable.Fields[i].AsString;
                                                DBValues:=DBValues + AFName + '=' + AFValue;
                                                if i < (n-1) then begin
                                                        DBValues:=DBValues + '|';
                                                end;
                                        end;
                                end;
                        end;
                finally
                        if DBTable.Active then begin
                                DBTable.Close;
                        end;
                        DBTable.Free;
                end;
        end;
        Result:=PChar(DBValues);
end;

function GSTToProgisID(AProjName,ALayerName,KG,GN: PChar): Integer; stdcall;
var
DBName: AnsiString;
CS: AnsiString;
DBTable: TADOTable;
begin
        Result:=0;
        DBName:=StringReplace(AProjName,'.amp','.wgi',[]);
        CS:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=%1;Persist Security Info=False;';
        CS:=StringReplace(CS,'%1',DBName,[]);
        if FileExists(DBName) then begin
                DBTable:=TADOTable.Create(nil);
                DBTable.TableDirect:=True;
                DBTable.ConnectionString:=CS;
                DBTable.TableName:='ATT_' + AnsiString(ALayerName);
                try
                        DBTable.Open;
                        if DBTable.Active then begin
                                DBTable.Filter:='[KGNR]=' + '''' + AnsiString(KG) + '''' + ' AND [GSTNR]=' + '''' + AnsiString(GN) + '''';
                                DBTable.Filtered:=True;
                                if DBTable.RecordCount > 0 then begin
                                        DBTable.First;
                                        Result:=DBTable.FieldByName('ProgisID').AsInteger;
                                end;
                        end;
                finally
                        if DBTable.Active then begin
                                DBTable.Close;
                        end;
                        DBTable.Free;
                end;
        end;
end;

exports ProgisIDToDBValues,GSTToProgisID;

begin
end.
