unit DTI_LanguageManager;
{
Internal database. Ver. II.
This module provides the functionality of the langauge localization.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 01-02-2001
}

interface

Uses Classes;

Type
    TLanguageManager = Class
       Private
          listEntries: TStringList;
       Public
          Constructor Create(sLanguageFileName: AnsiString);
          Destructor Free;

          Function GetValue(iValue: Integer): AnsiString; Overload;
          Function GetValue(sValue: AnsiString): AnsiString; Overload;

          Procedure MakeReplacement(AComponent: TObject); 
       End;
Var
   ALangManager: TLanguageManager;

implementation

Uses Forms, SysUtils, TypInfo, Windows,
     DTI_Consts;

Constructor TLanguageManager.Create(sLanguageFileName: AnsiString);
Var
   iI: Integer;
   sEntry: AnsiString;
   fsFileStream: TFileStream;
   HRsrc, HMem: THandle;
   Size: DWORD;
   Point: Pointer;
   AFile: TextFile;
   AString,
   AnUpperedString: AnsiString;
Begin
     listEntries := TStringList.Create;
      // Try to find such file.
     If NOT FileExists(sLanguageFileName) Then Begin
        // If the english file also does not exist, create it.
        HRsrc := FindResource(HInstance, PChar(cs_ResNameOfEnglishLngFile), RT_RCDATA);
        Size := SizeofResource(HInstance, HRsrc);
        HMem := LoadResource(HInstance, HRsrc);
        Point := LockResource(HMem);
        TRY
           fsFileStream := TFileStream.Create(sLanguageFileName, fmCreate);
           fsFileStream.Position := 0;
           fsFileStream.Write(Point^, Size);
           fsFileStream.Free;
        EXCEPT
           EXIT;
        END;
        End;
     TRY
        // Load language file.
        listEntries.LoadFromFile(sLanguageFileName);
        // Delete empty lines from the list.
        iI := 0;
        While iI < listEntries.Count Do Begin
              sEntry := listEntries[iI];
              If Trim(sEntry) = '' Then
                 listEntries.Delete(iI)
              Else
                 Inc(iI);
              End; // While end
     EXCEPT
        listEntries.Clear;
     END;
End;

Destructor TLanguageManager.Free;
Begin
     listEntries.Clear;
     listEntries.Free;
End;

{ This function returns a string by a ID value. It looks like Multilanguage of WinGIS. }
Function TLanguageManager.GetValue(iValue: Integer): AnsiString;
Begin
     RESULT := SELF.GetValue(IntToStr(iValue));
End;

Function TLanguageManager.GetValue(sValue: AnsiString): AnsiString;
Var
   iP: Integer;
Begin
     RESULT := 'Unknown ' + sValue;
     iP := listEntries.IndexOfName(sValue);
     If (iP < 0) OR (iP >= listEntries.Count) Then EXIT;
     RESULT := listEntries[iP];
     iP := Pos('=', RESULT);
     If iP > 0 Then
        RESULT := Copy(RESULT, iP+1, Length(RESULT));
End;

Procedure TLanguageManager.MakeReplacement(AComponent: TObject);

  Procedure SetStringProps(AComponent: TObject);
  Var
     bReplaced: Boolean;

      Function Replace(Text:String):String;
      Var
         iP: Integer;
      Begin
           RESULT := '';
           bReplaced:=FALSE;
           If Text = '' Then EXIT
           Else Begin
              iP := Pos('!', Text);
              If iP < 1 Then EXIT;
              RESULT := SELF.GetValue(Copy(Text, iP+1, Length(text)));
              bReplaced := TRUE;
              End;
      End;

  Var
     APropInfo: PPropInfo;
     APropList: PPropList;
     iPropCount, iI, iJ: Integer;
     AObject: TObject;
     sCaption: AnsiString;
  Begin
       If AComponent = NIL Then EXIT;
       New(APropList);
       TRY
          iPropCount := GetPropList(AComponent.ClassInfo, [tkClass,tkString,tkLString], APropList);
          For iI:=0 To iPropCount-1 Do Begin
              APropInfo := APropList[iI];
              If APropInfo.PropType^.Kind IN [tkString,tkLString] Then Begin
                 sCaption := Replace(GetStrProp(AComponent, APropInfo));
                 If bReplaced Then
                    SetStrProp(AComponent, APropInfo, sCaption);
                 End
              Else
                 If APropInfo.PropType^.Kind = tkClass Then Begin
                    AObject := Pointer(GetOrdProp(AComponent, APropInfo));
                    If AObject IS TStrings Then Begin
                       TStrings(AObject).BeginUpdate;
                       For iJ:=0 to TStrings(AObject).Count-1 Do
                           If Copy(TStrings(AObject).Strings[iJ], 1, 1)='!' Then
                              TStrings(AObject).Strings[iJ] := Replace(TStrings(AObject).Strings[iJ]);
                       TStrings(AObject).EndUpdate;
                       End
                    Else
                      If AObject IS TCollection Then
                         For iJ:=0 to TCollection(AObject).Count-1 Do
                             SetStringProps(TCollection(AObject).Items[iJ])
                      Else
                         If AObject IS TComponent Then
                            For iJ:=0 To TComponent(AObject).ComponentCount-1 Do
                                SetStringProps(TComponent(AObject).Components[iJ])
                         Else SetStringProps(AObject);
                    End;
              End; // For iI end
       FINALLY
          Dispose(APropList);
       END;
  End;

Var
   iI: Integer;
Begin
     SetStringProps(AComponent);
     If AComponent IS TComponent Then
        For iI:=0 To TComponent(AComponent).ComponentCount-1 Do
            SetStringProps(TComponent(AComponent).Components[iI]);
End;

end.
