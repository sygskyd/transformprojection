unit DTI_AccessViaADO;
{
Geotext/Tooltip library.
This module provides access to Delphi starndard database format files
(xBase and Paradox).

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 18-05-2001
}
interface

Uses Classes,
     DMaster, DTables, ADODB;

Type
    TDTI_ADO_Access = Class
        Private
           AQuery: TDQuery;
//           AQuery: TADOQuery;
           Function SplitParameter(sParameter: AnsiSTring; Var sDatabaseType: AnsiString; Var sDatabaseFileName: AnsiString; Var sTableName: AnsiString): Boolean;

           Procedure Check_MSAccess_Connection(sDatabaseFileName: AnsiString);
           Function UnloadMSAccessFile(sDatabaseFileName: AnsiString): Boolean;
           Function MSAccess_ChooseTableName(sDatabaseFileName, sTableName: AnsiString): AnsiString;           
           Function MSAccess_GetGeoText(sDataBaseFileName, sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): AnsiString;
           Procedure MSAccess_SetGeoText(sDataBaseFileName, sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName, sGeoTextValue: AnsiString);
           Function MSAccess_CreateTable(sDataBaseFileName, sTableName, sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): Boolean;
           Function MSAccess_GetFieldDescs(sDataBaseFileName, sTableName: AnsiString): TList;
           Function MSAccess_TableExists(sDataBaseFileName, sTableName: AnsiString): Boolean;
        Public
           Constructor Create;
           Destructor Free;

           Function GetGeoText(sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): AnsiString;
           Procedure SetGeoText(sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName, sGeoTextValue: AnsiString);
           Function CreateTable(sTableName, sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): Boolean;
           Function GetFieldDescs(sTableName: AnsiString): TList;

           Function ChooseTableName(sAlreadySelectedTableName: AnsiString): AnsiString;
           Function TableExists(sTableName: AnsiString): Boolean;
        End;

implementation

Uses SysUtils, Windows, Forms, Controls,
     DUtils,
     DTI_Consts, DTI_SelectInListBox;

Constructor TDTI_ADO_Access.Create;
Begin
     SELF.AQuery := TDQuery.Create(NIL);
End;

Destructor TDTI_ADO_Access.Free;
Begin
     AQuery.Close;
     AQuery.Free;
End;

Function TDTI_ADO_Access.SplitParameter(sParameter: AnsiSTring; Var sDatabaseType: AnsiString; Var sDatabaseFileName: AnsiString; Var sTableName: AnsiString): Boolean;
Var
   iP: Integer;
   sData1, sData2, sData3: AnsiString;
Begin
     RESULT := FALSE;
     sDatabaseType := '';
     sDatabaseFileName := '';
     sTableName := '';

     iP := Pos(cs_ParameterSeparator, sParameter);
     If iP < 1 Then EXIT;
     // Detect Database type.
     sData1 := AnsiUpperCase(Trim(Copy(sParameter, 1, iP-1)));
     sParameter := Copy(sParameter, iP+1, Length(sParameter));
     // Detect Database file name
     iP := Pos(cs_ParameterSeparator, sParameter);
     If iP < 1 Then EXIT;
     sData2 := Copy(sParameter, 1, iP-1);
     sData3 := Copy(sParameter, iP+1, Length(sParameter));

     sDatabaseType := sData1;
     sDatabaseFileName := sData2;
     sTableName := sData3;
     RESULT := TRUE;
End;

Function TDTI_ADO_Access.GetGeoText(sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): AnsiString;
Var
   sDatabaseType, sDatabaseFileName, sDatabaseTableName: AnsiString;
Begin
     RESULT := '';
     If SELF.SplitParameter(sTableName, sDatabaseType, sDatabaseFileName, sDatabaseTableName) Then Begin
        If sDatabaseType = cs_MSACCESS_TypeSign Then
           RESULT := MSAccess_GetGeoText(sDatabaseFileName, sDatabaseTableName, iItemID, sGeoTextIDFieldName, sGeoTextValueFieldName);
        End;
End;

Procedure TDTI_ADO_Access.SetGeoText(sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName, sGeoTextValue: AnsiString);
Var
   sDatabaseType, sDatabaseFileName, sDatabaseTableName: AnsiString;
Begin
     If SELF.SplitParameter(sTableName, sDatabaseType, sDatabaseFileName, sDatabaseTableName) Then Begin
        If sDatabaseType = cs_MSACCESS_TypeSign Then
           SELF.MSAccess_SetGeoText(sDatabaseFileName, sDatabaseTableName, iItemID, sGeoTextIDFieldName, sGeoTextValueFieldName, sGeoTextValue);
        End;
End;

Function TDTI_ADO_Access.CreateTable(sTableName, sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): Boolean;
Var
   sDatabaseType, sDatabaseFileName, sDatabaseTableName: AnsiString;
Begin
     RESULT := FALSE;
     If SELF.SplitParameter(sTableName, sDatabaseType, sDatabaseFileName, sDatabaseTableName) Then Begin
        If sDatabaseType = cs_MSACCESS_TypeSign Then
           RESULT := SELF.MSAccess_CreateTable(sDatabaseFileName, sDatabaseTableName, sGeoTextIDFieldName, sGeoTextValueFieldName);
        End;
End;

Function TDTI_ADO_Access.GetFieldDescs(sTableName: AnsiString): TList;
Var
   sDatabaseType, sDatabaseFileName, sDatabaseTableName: AnsiString;
Begin
     If SELF.SplitParameter(sTableName, sDatabaseType, sDatabaseFileName, sDatabaseTableName) Then Begin
        If sDatabaseType = cs_MSACCESS_TypeSign Then
           RESULT := MSAccess_GetFieldDescs(sDatabaseFileName, sDatabaseTableName);
        End
     Else
        RESULT := TList.Create;
End;

Function TDTI_ADO_Access.ChooseTableName(sAlreadySelectedTableName: AnsiString): AnsiString;
Var
   sDatabaseType, sDatabaseFileName, sDatabaseTableName: AnsiString;
Begin
     RESULT := '';
     If SELF.SplitParameter(sAlreadySelectedTableName, sDatabaseType, sDatabaseFileName, sDatabaseTableName) Then
        RESULT := SELF.MSAccess_ChooseTableName(sDatabaseFileName, sDatabaseTableName);
End;

Function TDTI_ADO_Access.TableExists(sTableName: AnsiString): Boolean;
Var
   sDatabaseType, sDatabaseFileName, sDatabaseTableName: AnsiString;
Begin
     RESULT := FALSE;
     If SELF.SplitParameter(sTableName, sDatabaseType, sDatabaseFileName, sDatabaseTableName) Then
        RESULT := SELF.MSAccess_TableExists(sDatabaseFileName, sDatabaseTableName);
End;

////////////////////////////////////////////////////////////////////////////////
Function TDTI_ADO_Access.MSAccess_ChooseTableName(sDatabaseFileName, sTableName: AnsiString): AnsiString;
Var
   AMaster: TDMaster;
   SLBF: TDTI_SelectInListBoxForm;
   iI, iP: Integer;
   sTemp: AnsiString;
Begin
     RESULT := '';

     Check_MSAccess_Connection(sDatabaseFileName);

     AMaster := TDMaster.Create(NIL);
     AMaster.Connection := AQuery.Connection;
     TRY
        AMaster.Connected := TRUE;
        SLBF := TDTI_SelectInListBoxForm.Create(Application);
        TRY
           AMaster.CentralData.GetADOTableNames(SLBF.ListBox1.Items);
           SLBF.ListBox1.MultiSelect := FALSE;
           sTemp := AnsiUpperCase(sTableName);
           iP := 0;
           For iI:=0 To SLBF.ListBox1.Items.Count-1 Do
               If AnsiUpperCase(SLBF.ListBox1.Items[iI]) = sTemp Then Begin
                  iP := iI;
                  BREAK;
                  End;
           SLBF.ListBox1.ItemIndex := iP;
           If SLBF.ShowModal = mrOK Then Begin
              RESULT := SLBF.TableName;
              If SLBF.ProgisIDFieldName <> '' Then
                 RESULT := RESULT + ']' + SLBF.ProgisIDFieldName + ']' + SLBF.GeoTextFieldName; // Use of ']' is forbidden.
              End;
        FINALLY
           AMaster.Connected := FALSE;
           AMaster.Free;
           SLBF.Free;
        END;
     EXCEPT
     END;
End;

Procedure TDTI_ADO_Access.Check_MSAccess_Connection(sDatabaseFileName: AnsiString);
Var
   sCurrentDatabaseFileName: AnsiString;
Begin
     sDataBaseFileName := AnsiUpperCase(sDataBaseFileName);
     sCurrentDatabaseFileName := AnsiUpperCase(GetADOPart(AQuery.Connection, 'Data Source'));
     If sCurrentDatabaseFileName <> sDataBaseFileName Then Begin
        // If not - make a new connection to the new file.
        AQuery.Close; // Just in case
        AQuery.Connection := 'Provider=Microsoft.Jet.OLEDB.4.0;' +
                              'Password=;' +
                              'User ID=;' +
                              'Data Source=' + sDataBaseFileName + ';' +
                              'Mode=ReadWrite|Share Deny None;' +
                              'Persist Security Info=True';
        End;
End;

Function TDTI_ADO_Access.UnloadMSAccessFile(sDatabaseFileName: AnsiString): Boolean;
Var
   fsFileStream: TFileStream;
   HRsrc, HMem: THandle;
   Size: DWORD;
   Point: Pointer;
   sResName: AnsiString;
Begin
     RESULT := FALSE;

     sResName := 'EMPTYDB97';

     HRsrc := FindResource(HInstance, PChar(sResName), RT_RCDATA);
     If HRsrc = 0 Then Begin
        Application.MessageBox(PChar('Resource ''' + sResName + ''' not found.'), 'WinGIS', MB_OK + MB_DEFBUTTON1);
        EXIT;
        End;
     Size := SizeofResource(HInstance, HRsrc);
     If Size = 0 Then Begin
        Application.MessageBox(PChar('Size of resource ''' + sResName + ''' is zero.'), 'WinGIS', MB_OK + MB_DEFBUTTON1);
        EXIT;
        End;
     HMem := LoadResource(HInstance, HRsrc);
     If HMem = 0 Then Begin
        Application.MessageBox(PChar('Resource ''' + sResName + ''' cannot be loaded.'), 'WinGIS', MB_OK + MB_DEFBUTTON1);
        EXIT;
        End;
     Point := LockResource(HMem);
     If Point = nil Then Begin
        Application.MessageBox(PChar('Resource ''' + sResName + ''' cannot be locked.'), 'WinGIS', MB_OK + MB_DEFBUTTON1);
        EXIT;
        End;
     TRY
        fsFileStream := TFileStream.Create(sDatabaseFileName, fmCreate);
        fsFileStream.Position := 0;
        fsFileStream.Write(Point^, Size);
        fsFileStream.Free;
     EXCEPT
        EXIT;
     END;
     RESULT := TRUE;
End;

Function TDTI_ADO_Access.MSAccess_GetFieldDescs(sDataBaseFileName, sTableName: AnsiString): TList;
Type
    TrecFieldDesc = Record
        sFieldName: AnsiString;
        iFieldType: Integer;
        End;
Var
   iI: Integer;
   pFD: ^TrecFieldDesc;
   ATable: TDTable;
Begin
     RESULT := TList.Create;

     SELF.Check_MSAccess_Connection(sDataBaseFileName);

     ATable := TDTable.Create(NIL);
     ATable.Connection := AQuery.Connection;
     ATable.TableName := sTableName;
     TRY
        ATable.Open;
     EXCEPT
        ATable.Free;
        EXIT;
     END;
     TRY
        For iI:=0 To ATable.Fields.Count-1 Do Begin
            New(pFD);
            pFD.sFieldName := ATable.Fields[iI].FieldName;
            pFD.iFieldType := Ord(ATable.Fields[iI].DataType);
            If pFD.iFieldType = 4 Then pFD.iFieldType := 12; // Word type is Byte type indeed.
            RESULT.Add(pFD);
            End;
     FINALLY
        ATable.Close;
        ATable.Free;
     END;
End;

Function TDTI_ADO_Access.MSAccess_GetGeoText(sDataBaseFileName, sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): AnsiString;
Var
   sItemID: AnsiString;
Begin
     RESULT := '';
     TRY
        sItemID := IntToStr(iItemID);
     EXCEPT
        EXIT;
     END;
     // Check if AMaster is connected to the same database file.
     SELF.Check_MSAccess_Connection(sDataBaseFileName);
     AQuery.Close; // Just in case.
     AQuery.SQL.Text:='SELECT [' + sGeoTextValueFieldName + '] FROM [' + sTableName + '] WHERE [' + sGeoTextIDFieldName + '] = ' + sItemID;
     TRY
        AQuery.Open;
        If AQuery.RecordCount > 0 Then
           RESULT := AQuery.Fields[0].AsString;
     EXCEPT
     END;
     AQuery.Close;
End;

Procedure TDTI_ADO_Access.MSAccess_SetGeoText(sDataBaseFileName, sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName, sGeoTextValue: AnsiString);
Var
   sItemID: AnsiString;
   iRecordCount: Integer;
Begin
     TRY
        sItemID := IntToStr(iItemID);
     EXCEPT
        EXIT;
     END;
     // Check if AMaster is connected to the same database file.
     SELF.Check_MSAccess_Connection(sDataBaseFileName);
     AQuery.Close; // Just in case.
     AQuery.SQL.Text := 'SELECT [' + sGeoTextIDFieldName + '] FROM [' + sTableName + '] WHERE [' + sGeoTextIDFieldName + '] = ' + sItemID;
     TRY
        AQuery.Open;
        iRecordCount := AQuery.RecordCount;
     EXCEPT
        iRecordCount := 0;
     END;
     AQuery.Close;
     If iRecordCount > 0 Then
        AQuery.SQL.Text := 'UPDATE [' + sTableName + '] ' +
                           'SET [' + sGeoTextValueFieldName + '] = ''' + sGeoTextValue + ''' ' +
                           'WHERE [' + sGeoTextIDFieldName + '] = ' + sItemID
     Else
        AQuery.SQL.Text := 'INSERT INTO [' + sTableName + '] ' +
                           '([' + sGeoTextIDFieldName + '], ['+ sGeoTextValueFieldName + ']) ' +
                           'VALUES (' + sItemID + ', ''' + sGeoTextValue + ''')';
     TRY
        AQuery.ExecSQL;
     EXCEPT
     END;
End;

Function TDTI_ADO_Access.MSAccess_CreateTable(sDataBaseFileName, sTableName, sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): Boolean;
Begin
     RESULT := FALSE;

     SELF.Check_MSAccess_Connection(sDataBaseFileName);
     If (NOT FileExists(sDataBaseFileName)) AND (NOT UnloadMSAccessFile(sDataBaseFileName)) Then
        EXIT;

     AQuery.Close;
     TRY
        AQuery.ExecSQL('DROP TABLE [' + sTableName + ']');
     EXCEPT
     END;
     TRY
        AQuery.ExecSQL('CREATE TABLE [' + sTableName + '] (' +
                        sGeoTextIDFieldName + ' INTEGER, ' +
                        sGeoTextValueFieldName + ' TEXT(100))');
        RESULT := TRUE;
     EXCEPT
     END;
End;

Function TDTI_ADO_Access.MSAccess_TableExists(sDataBaseFileName, sTableName: AnsiString): Boolean;
Var
   AMaster: TDMaster;
   iI: Integer;
   slTableNames: TStringList;
Begin
     RESULT := FALSE;
     Check_MSAccess_Connection(sDatabaseFileName);

     sTableName := AnsiUpperCase(sTableName);
     AMaster := TDMaster.Create(NIL);
     AMaster.Connection := AQuery.Connection;
     slTableNames := TStringList.Create;
     TRY
        AMaster.Connected := TRUE;
        AMaster.CentralData.GetADOTableNames(slTableNames);
        For iI:=0 To slTableNames.Count-1 Do
            If AnsiUpperCase(slTableNames[iI]) = sTableName Then Begin
               RESULT := TRUE;
               BREAK;
               End;
     FINALLY
        AMaster.Connected := FALSE;
        AMaster.Free;
        slTableNames.Clear;
        slTableNames.Free;
     END;
End;

end.
