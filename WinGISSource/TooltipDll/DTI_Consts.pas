unit DTI_Consts;
{
Geotext/Tooltip library.
Constants definitions.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 19-09-2001
}
interface

Const
     cs_MSACCESS_TypeSign = 'MS_ACCESS';
     cs_ResNameOfEnglishLngFile = 'DTI_ENGLISH_LANGUAGE_FILE';

     cs_ParameterSeparator = '|'; // This sign cannot be used in filenames.
                                          // 1. It follows just after database type and before database file name.
                                          // 2. It follows just after database file name and before database table name.
                                          // For example: 'MSACCESS|c:\required database file name.mdb|required table name'

implementation

end.
