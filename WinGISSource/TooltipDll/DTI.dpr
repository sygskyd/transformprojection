library DTI;
{
Geotext/Tooltip library.
It works only with desktop formats (xBase and Paradox).

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 16-05-2001

Now an abilitity to connect to MS Access format database files also is added.
}
uses
  SHAREMEM,
  Classes,
  Forms,
  SysUtils,
  DTI_AccessViaBDE in 'DTI_AccessViaBDE.pas',
  DTI_AccessViaADO in 'DTI_AccessViaADO.pas',
  DTI_Consts in 'DTI_Consts.pas',
  DTI_SelectInListBox in 'DTI_SelectInListBox.pas' {DTI_SelectInListBoxForm},
  DTI_LanguageManager in 'DTI_LanguageManager.pas',
  DTI_TableParameters in 'DTI_TableParameters.pas' {DTI_TableParametersForm};

{$R DTI_EDBF_97.res} // Empty database file of MS Access 97 format
{$R DTI_EnglishLangFile.res}
{$R DTIVersionResource.res} // Version resource of dll.

Var
    BDE_Access: TDTI_BDE_Access;
    ADO_Access: TDTI_ADO_Access;

    AnOldApplication: TApplication;

Procedure InitLibrary; stdcall;
Begin
     AnOldApplication := NIL;
     TRY
        BDE_Access := TDTI_BDE_Access.Create;
     EXCEPT
        BDE_Access := NIL;
     END;

     TRY
        ADO_Access := TDTI_ADO_Access.Create;
     EXCEPT
        ADO_Access := NIL;
     END;
End;

Procedure FinLibrary; stdcall;
Begin
     If BDE_Access <> NIL Then
        BDE_Access.Free;
     If ADO_Access <> NIL Then
        ADO_Access.Free;
     // Restore TApplication object. If it was substituted.
     If AnOldApplication <> NIL Then
        Application := AnOldApplication;
End;

Procedure SynchronizeLibrary(AnApplication: TApplication; pcLanguageFileExtension: PChar); stdcall;
Var
   sLanguageFileName: AnsiString;
Begin
     AnOldApplication := Application;
     Application := AnApplication;

     sLanguageFileName := ExtractFilePath(Application.ExeName) + 'DTI.' + pcLanguageFileExtension;
     If NOT FileExists(sLanguageFileName) Then
        sLanguageFileName := ExtractFilePath(Application.ExeName) + 'DTI.044';
     ALangManager := TLanguageManager.Create(sLanguageFileName);
End;

//////////////////////  Routines that work via BDE  ////////////////////////////
Function GetGeoText(pcTableName: PChar; iItemID: Integer; pcGeoTextIDFieldName, pcGeoTextValueFieldName: PChar): PChar; stdcall;
Var
   sData: AnsiString;
Begin
     sData := '';
     If BDE_Access <> NIL Then
        sData := BDE_Access.GetGeoText(pcTableName, iItemID, pcGeoTextIDFieldName, pcGeoTextValueFieldName);
     GetMem(RESULT, Length(sData) + 1);
     StrPCopy(RESULT, sData);
End;

Procedure SetGeoText(pcTableName: PChar; iItemID: Integer; pcGeoTextIDFieldName, pcGeoTextValueFieldName, pcGeoTextValue: PChar); stdcall;
Begin
     If BDE_Access <> NIL Then
        BDE_Access.SetGeoText(pcTableName, iItemID, pcGeoTextIDFieldName, pcGeoTextValueFieldName, pcGeoTextValue);
End;

Function CreateTable(pcTableName, pcGeoTextIDFieldName, pcGeoTextValueFieldName: PChar): Boolean; stdcall;
Begin
     If BDE_Access <> NIL Then
        RESULT := BDE_Access.CreateTable(pcTableName, pcGeoTextIDFieldName, pcGeoTextValueFieldName)
     Else
        RESULT := FALSE;
End;

Function GetFieldDescs(pcTableName: PChar): TList; stdcall;
Begin
     If BDE_Access <> NIL Then
        RESULT := BDE_Access.GetFieldDescs(pcTableName)
     Else
        RESULT := TList.Create; // Empty TList will be returned
End;

//////////////////////  Routines that work via ADO  ////////////////////////////
Function ADO_TableExists(pcTableName: PChar): Boolean; stdcall;
Begin
     If ADO_Access <> NIL Then
        RESULT := ADO_Access.TableExists(pcTableName)
     Else
        RESULT := FALSE;
End;

Function ADO_GetFieldDescs(pcTableName: PChar): TList; stdcall;
Begin
     If BDE_Access <> NIL Then
        RESULT := ADO_Access.GetFieldDescs(pcTableName)
     Else
        RESULT := TList.Create; // Empty TList will be returned
End;

Function ADO_GetGeoText(pcTableName: PChar; iItemID: Integer; pcGeoTextIDFieldName, pcGeoTextValueFieldName: PChar): PChar; stdcall;
Var
   sData: AnsiString;
Begin
     sData := '';
     If ADO_Access <> NIL Then
        sData := ADO_Access.GetGeoText(pcTableName, iItemID, pcGeoTextIDFieldName, pcGeoTextValueFieldName);
     GetMem(RESULT, Length(sData) + 1);
     StrPCopy(RESULT, sData);
End;

Procedure ADO_SetGeoText(pcTableName: PChar; iItemID: Integer; pcGeoTextIDFieldName, pcGeoTextValueFieldName, pcGeoTextValue: PChar); stdcall;
Begin
     If BDE_Access <> NIL Then
        ADO_Access.SetGeoText(pcTableName, iItemID, pcGeoTextIDFieldName, pcGeoTextValueFieldName, pcGeoTextValue);
End;

Function ADO_CreateTable(pcTableName, pcGeoTextIDFieldName, pcGeoTextValueFieldName: PChar): Boolean; stdcall;
Begin
     If BDE_Access <> NIL Then
        RESULT := ADO_Access.CreateTable(pcTableName, pcGeoTextIDFieldName, pcGeoTextValueFieldName)
     Else
        RESULT := FALSE;
End;

Function ADO_ChooseTableName(pcAlreadySelectedTableName: PChar): PChar; stdcall;
Var
   sData: AnsiString;
Begin
     sData := '';
     If ADO_Access <> NIL Then
        sData := ADO_Access.ChooseTableName(pcAlreadySelectedTableName);
     GetMem(RESULT, Length(sData) + 1);
     StrPCopy(RESULT, sData);
End;

////////////////////////////////////////////////////////////////////////////////
Exports
        InitLibrary,
        SynchronizeLibrary,
        FinLibrary,

        GetGeoText,
        SetGeoText,
        CreateTable,
        GetFieldDescs,

        ADO_GetGeoText,
        ADO_SetGeoText,
        ADO_CreateTable,
        ADO_GetFieldDescs,
        ADO_ChooseTableName,
        ADO_TableExists
        ;
begin
end.
