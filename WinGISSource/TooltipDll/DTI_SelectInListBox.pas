unit DTI_SelectInListBox;
{
Geotext/Tooltip library.
This form is purposed for selecting by user some data in TListBox.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 19-09-2001
}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Menus, ExtCtrls;

type
  TDTI_SelectInListBoxForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    CancelButton: TButton;
    OKButton: TButton;
    Label1: TLabel;
    ListBox1: TListBox;
    Button1: TButton;
    procedure ListBox1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    sTableName,
    sProgisIDFieldName,
    sGeoTextFieldName: AnsiString;
  public
    { Public declarations }
    Property TableName: AnsiString Read sTableName;
    Property ProgisIDFieldName: AnsiString Read sProgisIDFieldName;
    Property GeoTextFieldName: AnsiString Read sGeoTextFieldName;
  end;

implementation

Uses
     DTI_LanguageManager, DTI_Consts, DTI_TableParameters;

{$R *.DFM}

procedure TDTI_SelectInListBoxForm.ListBox1DblClick(Sender: TObject);
begin
     SELF.OKButton.Click;
end;

procedure TDTI_SelectInListBoxForm.FormCreate(Sender: TObject);
begin
     If ALangManager <> NIL Then
        ALangManager.MakeReplacement(SELF);
end;

procedure TDTI_SelectInListBoxForm.Button1Click(Sender: TObject);
Var
   TPF: TDTI_TableParametersForm;
   iP: Integer;
   sTableName: AnsiString;
begin
     TPF := TDTI_TableParametersForm.Create(SELF);
     TRY
        TPF.ProgisIDComboBox.Visible := FALSE;
        TPF.GeoTextComboBox.Visible := FALSE;
        // 5=New table
        TPF.TableNameEdit.Text := ALangManager.GetValue(5);
        If TPF.ShowModal = mrOK Then Begin
           // Create new table.
           sTableName := Trim(TPF.TableNameEdit.Text);

           SELF.sProgisIDFieldName := Trim(TPF.ProgisIDEdit.Text);
           SELF.sGeoTextFieldName := Trim(TPF.GeoTextEdit.Text);
           
           iP := ListBox1.Items.Add(sTableName);
           ListBox1.ItemIndex := iP;
           End;
     FINALLY
        TPF.Free;
        SELF.OKButton.Enabled := SELF.ListBox1.Items.Count > 0;        
     END;
end;

procedure TDTI_SelectInListBoxForm.OKButtonClick(Sender: TObject);
Var
   TPF: TDTI_TableParametersForm;
begin
     SELF.sTableName := ListBox1.Items[ListBox1.ItemIndex];
{
     TPF := TDTI_TableParametersForm.Create(SELF);
     TRY
        TPF.ProgisIDEdit.Visible := FALSE;
        TPF.GeoTextEdit.Visible := FALSE;
        TPF.TableNameEdit.Text := SELF.ListBox1.Items[ListBox1.ItemIndex];
        TPF.TableNameEdit.ReadOnly := TRUE;
        If TPF.ShowModal = mrOK Then Begin
           // Create new table.
           SELF.sTableName := Trim(TPF.TableNameEdit.Text);
           SELF.sProgisIDFieldName := Trim(TPF.ProgisIDEdit.Text);
           SELF.sGeoTextFieldName := Trim(TPF.GeoTextEdit.Text);
           End;
     FINALLY
        TPF.Free;
     END;
}     
end;

procedure TDTI_SelectInListBoxForm.FormShow(Sender: TObject);
begin
     SELF.OKButton.Enabled := SELF.ListBox1.Items.Count > 0;
end;

end.
