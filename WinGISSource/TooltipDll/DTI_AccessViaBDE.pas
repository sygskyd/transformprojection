unit DTI_AccessViaBDE;
{
Geotext/Tooltip library.
This module provides access to MS Access databse files.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 18-09-2001

08-01-2002 - The class was modified to use ADO for xBase and Paradox files. 
}
interface

Uses Classes,
     DTables;

Type
    TDTI_BDE_Access = Class
        Private
           AQuery: TDQuery;
           Function MakeConnectionString(sTableName: AnsiString): AnsiString;
        Public
           Constructor Create;
           Destructor Free;

           Function GetGeoText(sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): AnsiString;
           Procedure SetGeoText(sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName, sGeoTextValue: AnsiString);
           Function CreateTable(sTableName, sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): Boolean;
           Function GetFieldDescs(sTableName: AnsiString): TList;
        End;

implementation

Uses SysUtils, Windows, Forms,
     DTI_LanguageManager;

Constructor TDTI_BDE_Access.Create;
Begin
     SELF.AQuery := TDQuery.Create(NIL);
End;

Destructor TDTI_BDE_Access.Free;
Begin
     SELF.AQuery.Free;
End;

Function TDTI_BDE_Access.MakeConnectionString(sTableName: AnsiString): AnsiString;
Var
   sExtension, sLevel: AnsiString;
Begin
     sExtension := AnsiUpperCase(ExtractFileExt(sTableName));
     If sExtension = '.DBF' Then
        sLevel := 'dBase 5.0'
     Else
        sLevel := 'Paradox 7.x';

     RESULT := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + ExtractFilePath(sTableName) + ';' +
               'Extended Properties=' + sLevel + ';' +
               'Mode=ReadWrite|Share Deny None;Persist Security Info=False';
End;

Function TDTI_BDE_Access.GetGeoText(sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): AnsiString;
Var
   sItemID: AnsiString;
   bAllOK: Boolean;
Begin
     RESULT := '';
     TRY
        sItemID := IntToStr(iItemID);
        bAllOK := TRUE;
     EXCEPT
        bAllOK := FALSE;
     END;
     If bAllOK Then Begin
        AQuery.Close;
        AQuery.Connection := SELF.MakeConnectionString(sTableName);
        AQuery.Sql.Text := 'SELECT ' + sGeoTextValueFieldName + ' FROM [' + ExtractFileName(sTableName) + '] ' +
                           'WHERE ' + sGeoTextIDFieldName + ' = ' + sItemID;
        TRY
           AQuery.Open;
           If AQuery.RecordCount > 0 Then
              RESULT := AQuery.Fields[0].AsString;
        EXCEPT
        END;
        AQuery.Close;
        End;
End;

Procedure TDTI_BDE_Access.SetGeoText(sTableName: AnsiString; iItemID: Integer; sGeoTextIDFieldName, sGeoTextValueFieldName, sGeoTextValue: AnsiString);
Var
   iRecordCount: Integer;
   sItemID: AnsiString;
Begin
     TRY
        sItemID := IntToStr(iItemID);
     EXCEPT
        EXIT;
     END;

     AQuery.Close;
     AQuery.Connection := SELF.MakeConnectionString(sTableName);
     
     sTableName := ExtractFileName(sTableName);
     AQuery.SQL.Text := 'SELECT ' + sGeoTextIDFieldName + ' FROM [' + sTableName + '] ' +
                        'WHERE ' + sGeoTextIDFieldName + ' = ' + sItemID;
     TRY
        AQuery.Open;
        iRecordCount := AQuery.RecordCount;
     EXCEPT
        iRecordCount := 0;
     END;
     AQuery.Close;
     If iRecordCount > 0 Then
        AQuery.SQL.Text := 'UPDATE [' + sTableName + '] ' +
                           'SET ' + sGeoTextValueFieldName + ' = ''' + sGeoTextValue + ''' ' +
                           'WHERE ' + sGeoTextIDFieldName + ' = ' + sItemID
     Else
        AQuery.SQL.Text := 'INSERT INTO [' + sTableName + '] ' +
                           '(' + sGeoTextIDFieldName + ', '+ sGeoTextValueFieldName + ') ' +
                           'VALUES (' + sItemID + ', ''' + sGeoTextValue + ''')';
     TRY
        AQuery.ExecSQL;
     EXCEPT
        // 11=Data cannot be updated.
        MessageBox(Application.Handle, PChar(ALangManager.GetValue(11)), 'WinGIS', MB_OK + MB_DEFBUTTON1 + MB_ICONSTOP);        
     END;
End;

Function TDTI_BDE_Access.CreateTable(sTableName, sGeoTextIDFieldName, sGeoTextValueFieldName: AnsiString): Boolean;
Var
   sSQL, sExtension: AnsiString;
Begin
     RESULT := FALSE;
     sExtension := AnsiUpperCase(ExtractFileExt(sTableName));
     sTableName := ExtractFileName(sTableName);
     If sExtension = '.DBF' Then
        sSQL := 'CREATE TABLE [' + sTableName + '] (' +
                 sGeoTextIDFieldName + ' NUMERIC(16, 0), ' +
                 sGeoTextValueFieldName + ' CHAR(100))'
     Else
        If sExtension = '.DB' Then
           sSQL := 'CREATE TABLE [' + sTableName + '] (' +
                   sGeoTextIDFieldName + ' INTEGER, ' +
                   sGeoTextValueFieldName + ' CHAR(100))'
        Else
           EXIT;

     AQuery.Close;
     AQuery.Connection := SELF.MakeConnectionString(sTableName);
     
     If FileExists(sTableName) Then Begin
        TRY
           AQuery.ExecSQL('DROP TABLE [' + sTableName + ']');
        EXCEPT
           EXIT;
        END;
        End;

     TRY
        AQuery.ExecSQL(sSQL);
        RESULT := TRUE;
     EXCEPT
     END;
End;

Function TDTI_BDE_Access.GetFieldDescs(sTableName: AnsiString): TList;
Type
    TrecFieldDesc = Record
        sFieldName: AnsiString;
        iFieldType: Integer;
        End;
Var
   iI: Integer;
   pFD: ^TrecFieldDesc;
   ATable: TDTable;
Begin
     RESULT := TList.Create;
     ATable := TDTable.Create(NIL);
     ATable.Connection := SELF.MakeConnectionString(sTableName);
     ATable.TableName := ExtractFileName(sTableName);
     TRY
        ATable.Open;
     EXCEPT
        ATable.Free;
        EXIT;
     END;
     For iI:=0 To ATable.Fields.Count-1 Do Begin
         New(pFD);
         pFD.sFieldName := ATable.Fields[iI].FieldName;
         // I return the order in TFieldType of tyep of a field. Then
         // this order can be transformed in the type again.
         pFD.iFieldType := Ord(ATable.Fields[iI].DataType);
         RESULT.Add(pFD);
         End;
     ATable.Close;
     ATable.Free;
End;

end.
