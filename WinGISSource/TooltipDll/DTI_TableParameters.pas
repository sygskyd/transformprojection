unit DTI_TableParameters;
{
Geotext/Tooltip library.
This form is purposed for setting of new table that wil be created.

Author: Dennis Ivanoff, Progis Russia, Moscow.
Date: 20-09-2001
}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TDTI_TableParametersForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    OKButton: TButton;
    CancelButton: TButton;
    TableNameEdit: TEdit;
    ProgisIDEdit: TEdit;
    GeoTextEdit: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ProgisIDComboBox: TComboBox;
    GeoTextComboBox: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
   DTI_TableParametersForm: TDTI_TableParametersForm;

implementation
{$R *.DFM}

Uses
     DTI_LanguageManager;

procedure TDTI_TableParametersForm.FormCreate(Sender: TObject);
begin
     ALangManager.MakeReplacement(SELF);

     ProgisIDComboBox.Left := ProgisIDEdit.Left;
     ProgisIDComboBox.Width := ProgisIDEdit.Width;
     ProgisIDComboBox.Top := ProgisIDEdit.Top;
     ProgisIDComboBox.Height := ProgisIDEdit.Height;

     GeoTextComboBox.Left := GeoTextEdit.Left;
     GeoTextComboBox.Width := GeoTextEdit.Width;
     GeoTextComboBox.Top := GeoTextEdit.Top;
     GeoTextComboBox.Height := GeoTextEdit.Height;
end;

procedure TDTI_TableParametersForm.OKButtonClick(Sender: TObject);
begin
     If (Trim(TableNameEdit.Text) = '')
        OR
        (Trim(ProgisIDEdit.Text) = '')
        OR
        (Trim(GeoTextEdit.Text) = '') Then
           SELF.ModalResult := mrNone;                                
end;

end.
