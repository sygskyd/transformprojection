Unit WCtrls;

Interface

{$H+}

Uses WinProcs,WinTypes,Classes,Controls,ExtCtrls,Forms,Graphics,Menus,Messages,
     StdCtrls,TabNotBk,CommCtrl,ComCtrls,Dialogs;

Type TOwnerDrawAction   = Set of (oaDrawEntire,oaFocus,oaSelect);

     {$IFNDEF WIn32}
     TListSortCompare   = Function (Item1,Item2:Pointer):Integer;
     {$ENDIF}

     TWButton           = Class(TButton)
      Private
       FFlat            : Boolean;
       Procedure   SetFlat(AFlat:Boolean);
      Protected
       Procedure   CreateParams(var Params:TCreateParams); override;
      Published
       Property    Flat:Boolean read FFlat write SetFlat default False;
     end;

     TWStatusBar        = Class(TStatusBar)
      Private
       FCancelText      : Boolean;
       FDisabled        : Pointer;
       FInProgress      : Boolean;
       FProgress        : Integer;
       FProgressPanels  : TStatusPanels;
       FProgressText    : String;
       FAbortText       : String;
       FStoredPanels    : TStatusPanels;
       Procedure   DoDrawPanel(StatusBar:TStatusBar;Panel:TStatusPanel;const Rect:TRect);
       Procedure   SetProgress(Const AProgress:Real);
       Procedure   SetProgressPanel(ASet:Boolean);
       Procedure   SetProgressText(Const AText:String);
       Procedure   SetPanelText(AIndex:Integer;Const AText:String);
      Protected 
       Procedure   Resize; override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Property    AbortText:String read FAbortText write FAbortText;
       Property    CancelText:Boolean read FCancelText write FCancelText;
       Property    Progress:Real write SetProgress;
       Property    ProgressPanel:Boolean read FInProgress write SetProgressPanel;
       Property    ProgressText:String read FProgressText write SetProgressText;
       Function    QueryAbort:Boolean;
       Property    PanelText[AIndex:Integer]:String write SetPanelText; default;
     end;

     {**************************************************************************
       Class TWList
     ---------------------------------------------------------------------------
       Implements the sort-function which is not included in the 16-bit-version.
     +*************************************************************************}
     TWList             = Class(TList)
       {$IFNDEF WIN32}
      Private
       Procedure   QuickSort(SortFunction:TListSortCompare;L:Integer;R:Integer);
      Public
       Procedure   Sort(Compare:TListSortCompare);
       {$ENDIF}
     end;

     TWCustomListbox    = Class(TCustomListBox)
      Private
       FAutoScrollDir   : Integer;
       FOddLineColor    : TColor;
       FDoInherited     : Boolean;
       FDrawAction      : TOwnerDrawAction;
       FHasStrings      : Boolean;
       FLockUpdates     : Integer;
       FOnPaint         : TNotifyEvent;
       FPendingUpdates  : TRect;
       FScrollbars      : TScrollStyle;
       Procedure   CMRecreateWnd(var Message:TMessage); message cm_RecreateWnd;
       Procedure   CNCharToItem(var Msg:TWMCharToItem); message cn_CharToItem;
       Procedure   CNDrawItem(var Message: TWMDrawItem); message cn_DrawItem;
       Procedure   CNVKeyToItem(var Msg:TWMVKeyToItem); message cn_VKeyToItem;
       Function    GetCount:Integer;
       Function    GetFocusedIndex:Integer;
       Function    GetItemIndex:Integer;
       Procedure   SetAutoScrollTimer;
       Procedure   SetCount(ACount:Integer);
       Procedure   SetFocusedIndex(AIndex:Integer);
       Procedure   SetHasStrings(ASet:Boolean);
       Procedure   SetItemIndex(AIndex:Integer);
       Procedure   SetRangeSelected(AFrom,ATo:Integer;ASet:Boolean);
       Procedure   SetScrollBars(AScrollbars:TScrollStyle);
       Procedure   WMEraseBkgnd(var Msg:TMessage); message wm_EraseBkgnd;
       Procedure   WMLButtonDown(var Message:TWMMouse); message wm_LButtonDown;
       Procedure   WMMouseMove(var Message:TWMMouse); message wm_MouseMove;
       Procedure   WMPaint(var Msg:TMessage); message wm_Paint;
       Procedure   WMTimer(var Msg:TWMTimer); message wm_Timer;
      Protected
       Procedure   ChangeScale(M,D:Integer); override;
       Procedure   CreateParams(var Params:TCreateParams); override;
       Procedure   DragOver(Source:TObject;X,Y:Integer;State:TDragState;var Accept:Boolean); override;
       Procedure   LockUpdates;
       Property    OnPaint:TNotifyEvent read FOnPaint write FOnPaint;
       Procedure   UnlockUpdates;
      Public
       Constructor Create(AParent:TComponent); override;
       Property    Count:Integer read GetCount write SetCount;
       Property    DoInherited:Boolean read FDoInherited write FDoInherited;
       Property    DrawAction:TOwnerDrawAction read FDrawAction;
       Property    FocusedIndex:Integer read GetFocusedIndex write SetFocusedIndex;
       Property    HasStrings:Boolean read FHasStrings write SetHasStrings default TRUE;
       Property    ItemIndex:Integer read GetItemIndex write SetItemIndex;
       Property    ItemHeight;
       Procedure   InvalidateItems(FromItem,ToItem:Integer);
       Property    Scrollbars:TScrollStyle read FScrollbars write SetScrollbars default ssBoth;
       Property    RangeSelected[AFrom,ATo:Integer]:Boolean write SetRangeSelected;
     end;

     TWCustomCombobox   = Class(TCustomComboBox)
      Private
       FDrawAction      : TOwnerDrawAction;
       FOddLineColor    : TColor;
       Procedure   CNDrawItem(var Message: TWMDrawItem); message cn_DrawItem;
      Public
       Property    DrawAction:TOwnerDrawAction read FDrawAction;
       Property    ItemHeight;
     end;

     TWListBox = class(TWCustomListBox)
      Published
       Property    Align;
       Property    Anchors;
       Property    BorderStyle;
       Property    Color;
       Property    Columns;
       Property    Ctl3D;
       Property    DragCursor;
       Property    DragMode;
       Property    Enabled;
       Property    ExtendedSelect;
       Property    Font;
       Property    HasStrings;
       {$IFDEF WIN32}
       Property    ImeMode;
       Property    ImeName;
       {$ENDIF}
       Property    IntegralHeight;
       Property    ItemHeight;
       Property    Items;
       Property    MultiSelect;
       Property    ParentColor;
       Property    ParentCtl3D;
       Property    ParentFont;
       Property    ParentShowHint;
       Property    PopupMenu;
       Property    Scrollbars;
       Property    ShowHint;
       Property    Sorted;
       Property    Style;
       Property    TabOrder;
       Property    TabStop;
       {$IFDEF WIN32}
       Property    TabWidth;
       {$ENDIF}
       Property    Visible;
       Property    OnClick;
       Property    OnDblClick;
       Property    OnDragDrop;
       Property    OnDragOver;
       Property    OnDrawItem;
       Property    OnEndDrag;
       Property    OnEnter;
       Property    OnExit;
       Property    OnKeyDown;
       Property    OnKeyPress;
       Property    OnKeyUp;
       Property    OnMeasureItem;
       Property    OnMouseDown;
       Property    OnMouseMove;
       Property    OnMouseUp;
       Property    OnPaint;
       {$IFDEF WIN32}
       Property    OnStartDrag;
       {$ENDIF}
     end;

     TBoxStyle    = (bsBox, bsTopLine, bsNone);

     TWGroupBox    = Class(TGroupBox)
      Private  
       FBoxStyle : TBoxStyle;
       Procedure   SetBoxStyle(AStyle:TBoxStyle);
      Protected
       Procedure   CMEnabledChanged(var Message:TMessage); message CM_EnabledChanged;
       Procedure   Paint; override;
      Published
       Property    BoxStyle:TBoxStyle read FBoxStyle write SetBoxStyle default bsBox;
     end;

     TAlignToControl    = (alcNone, alcTop, alcCenter, alcBottom);

     TWLabel       = Class(TLabel)
      Private
       FAlignToControl  : TAlignToControl;
       Function    GetFocusControl:TWinControl;
       Procedure   SetAlignToControl(AAlign:TAlignToControl);
       Procedure   SetFocusControl(AControl:TWinControl);
       Procedure   UpdateAlignment;
       Procedure   WMWindowPosChanged(var Msg:TMessage); message wm_WindowPosChanged;
      Protected
       Procedure   Paint; override;
      Published
       Property    AlignToControl:TAlignToControl read FAlignToControl write SetAlignToControl default alcNone;
       Property    FocusControl:TWinControl read GetFocusControl write SetFocusControl;
     end;

     TMapMode      = (mmText,mmLoMetric,mmHiMetric,mmLoEnglish,mmHiEnglish,mmTwips,mmIsotropic,mmAnIsotropic);

     TWindow       = Class(TCustomControl)
      Private
       FEraseBkgnd : Boolean;
       FMapMode    : TMapMode;
       FOnPaint    : TNotifyEvent;
       FOwnDC      : Boolean;
       FScrollbars : TScrollStyle;
       Procedure   CMCtl3DChanged(var Msg:TMessage); message cm_Ctl3DChanged;
       Procedure   CMRecreateWnd(var Msg:TMessage); message cm_RecreateWnd;
       Procedure   SetDCMapMode(AMapMode:TMapMode);
       Procedure   SetScrollBars(AScrollbars:TScrollStyle);
       Procedure   WMEraseBkgnd(var Msg:TMessage); message wm_EraseBkgnd;
       Procedure   WMNCCalcSize(var Msg:TWMNCCalcSize); message wm_NCCalcSize;
       Procedure   WMNCPaint(var Msg:TWMNCPaint); message wm_NCPaint;
      Protected
       Procedure   CreateParams(var Params:TCreateParams); override;
       Procedure   CreateWnd; override;
       Procedure   Paint; override;
      Public
       Property    Canvas;
       Constructor Create(AParent:TComponent); override;
       Procedure   DoChange(Sender:TObject); 
      Published
       Property    Align;
       Property    Anchors;
       Property    Ctl3D;
       Property    Color default clWindow;
       Property    EraseBackground:Boolean read FEraseBkgnd write FEraseBkgnd default TRUE;
       Property    Font;
       Property    Height default 65;
       Property    MapMode:TMapMode read FMapMode write SetDCMapMode default mmText;
       Property    OnClick;
       Property    OnDblClick;
       Property    OnKeyDown;
       Property    OnKeyPress;
       Property    OnKeyUp;
       Property    OnMouseDown;
       Property    OnMouseMove;
       Property    OnMouseUp;
       Property    OnPaint:TNotifyEvent read FOnPaint write FOnPaint;
       Property    OnResize;
       Property    OwnDC:Boolean read FOwnDC write FOwnDC default False;
       Property    ParentColor default FALSE;
       Property    ParentCtl3D;
       Property    ParentFont;
       Property    PopupMenu;
       Property    Scrollbars:TScrollStyle read FScrollbars write SetScrollbars default ssNone;
       Property    Width default 65;
     end;

     TWForm      = Class(TForm)
      Private
       FOnMDIActivate   : TNotifyEvent;
       FOnMDIDeactivate : TNotifyEvent;
       FOnHidden        : TNotifyEvent;
       FOnShown         : TNotifyEvent;
       FScaled          : Boolean;
       Procedure   CMShowingChanged(var Msg:TMessage); message cm_ShowingChanged;
       Procedure   WMMDIActivate(var Msg:TWMMDIActivate); message wm_MDIActivate;
      Protected
       Procedure   ChangeScale(M,D:Integer); override;
       Procedure   ReadState(Reader:TReader); override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Property    OnHidden:TNotifyEvent read FOnHidden write FOnHidden;
       Property    OnMDIActivate:TNotifyEvent read FOnMDIActivate write FOnMDIActivate;
       Property    OnMDIDeactivate:TNotifyEvent read FOnMDIDeactivate write FOnMDIDeactivate;
       Property    OnShown:TNotifyEvent read FOnShown write FOnShown;
     end;

     TWTabbedNoteBook = Class(TTabbedNotebook)
      Private
       Procedure   CMFontChanged(var Msg:TMessage); message cm_FontChanged;
     end;

     TWImageList      = Class(TImageList)
       {$IFNDEF WIN32}
       Constructor CreateSize(AWidth,AHeight:Integer);
       {$ENDIF}
       Procedure   AddBitmaps(ABitmaps:TBitmap);
     end;

     TSpeedBtn          = Class(TGraphicControl)
      Private
       FAllowAllUp      : Boolean;
       FBitmap          : TBitmap;
       FDown            : Boolean;
       FDragging        : Boolean;
       FGlyphList       : Pointer;
       FNormalIndex     : Integer;
       FDisabledIndex   : Integer;
       FGroupIndex      : Integer;
       Procedure   ClearGlyphs;
       Procedure   CMButtonPressed(var Message: TMessage); message CM_ButtonPressed;
       Procedure   CMEnabledChanged(var Message:TMessage); message CM_EnabledChanged;
       Procedure   CMMouseEnter(var Message:TMessage); message CM_MouseEnter;
       Procedure   CMMouseLeave(var Message:TMessage); message CM_MouseLeave;
       Procedure   CMSysColorChange(var Message:TMessage); message CM_SysColorChange;
       Procedure   PaintFrame(var ARect:TRect);
       Procedure   SetBitmap(ABitmap:TBitmap);
       Procedure   SetDown(ADown:Boolean);
      Protected
       FOver            : Boolean;
       Procedure   ActionChange(Sender:TObject;CheckDefaults:Boolean); override;
       Function    GetActionLinkClass:TControlActionLinkClass; override;
       Procedure   MouseDown(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
       Procedure   MouseMove(Shift:TShiftState;X,Y:Integer); override;
       Procedure   MouseUp(Button:TMouseButton;Shift:TShiftState;X,Y:Integer); override;
       Procedure   Paint; override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Property    MouseInControl:Boolean read FOver;
      Published
       Property    Action;
       Property    Anchors;
       Property    AllowAllUp:Boolean read FAllowAllUp write FAllowAllUp default FALSE;
       Property    Caption;
       Property    Down:Boolean read FDown write SetDown;
       Property    Enabled;
       Property    Glyph:TBitmap read FBitmap write SetBitmap;
       Property    GroupIndex:Integer read FGroupIndex write FGroupIndex;
       Property    OnClick;
       Property    OnDblClick;
       Property    OnMouseDown;
       Property    OnMouseMove;
       Property    OnMouseUp;
       Property    ParentShowHint;
       Property    ShowHint;
     end;

     TSpeedBtnActionLink = Class(TControlActionLink)
      Private
       FClient     : TSpeedBtn;
      Protected
       Procedure   AssignClient(AClient:TObject); override;
       Function    IsCheckedLinked:Boolean; override;
       Procedure   SetChecked(Value:Boolean); override;
     end;
     
     {$IFDEF WIN32}
     TWListView    = Class(TListView)
      Private
       FCanvas     : TCanvas;
       FDrawAction : TOwnerDrawAction;
       FOnDrawItem : TDrawItemEvent;
       FOwnerDraw  : Boolean;
       Function    GetItemRect(Item,SubItem:Integer):TRect;
      Protected
       Procedure   CNDrawItem(var Message: TWMDrawItem); message cn_DrawItem;
       Procedure   CreateParams(var Params:TCreateParams); override;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Property    Canvas:TCanvas read FCanvas;
       Property    DrawAction:TOwnerDrawAction read FDrawAction;
       Property    ItemRect[Item,SubItem:Integer]:TRect read GetItemRect;
      Published
       Property    OnDrawItem:TDrawItemEvent read FOnDrawItem write FOnDrawItem;
       Property    OwnerDraw:Boolean read FOwnerDraw write FOwnerDraw default false;
     end;
     {$ENDIF}

     TUserInterfaceStyle = (uisWin31,uisWin95,uisOffice97);

     TWRadioGroup  = Class(TRadioGroup)
      Private
       FBoxStyle   : TBoxStyle;
       Procedure   SetBoxStyle(AStyle:TBoxStyle);
      Protected
       Procedure   Paint; override;
      Published
       Property    BoxStyle:TBoxStyle read FBoxStyle write SetBoxStyle default bsBox;
     end;

     TWOpenDialog  = Class(TOpenDialog)
      Private
       FReadOnlyCaption: String;
      Protected
       Procedure   DoShow; override;
      Published
       Property    ReadOnlyCaption:String read FReadOnlyCaption write FReadOnlyCaption;
     end;

     TWSaveDialog  = Class(TSaveDialog)
      Private
       FReadOnlyCaption: String;
      Protected
       Procedure   DoShow; override;
      Published
       Property    ReadOnlyCaption:String read FReadOnlyCaption write FReadOnlyCaption;
     end;

procedure Register;

Procedure TextRectEx(Canvas:TCanvas;Const Rect:TRect;const Text:String;
    BackgroundColor:TColor;State:TOwnerDrawState);

{******************************************************************************+
  Function AddCursor
--------------------------------------------------------------------------------
  Adds a new cursor from a resource-handle to the cursor-list of the
  application. The cursor is given a unique cursor-id which is returned by
  the function.
+******************************************************************************}
Function AddCursor(AHandle:THandle):TCursor;

{******************************************************************************+
  Procedure CreateDisabledBitmap
--------------------------------------------------------------------------------
  Creates a disabled version of ABitmap.
+******************************************************************************}
Procedure CreateDisabledBitmap(ABitmap:TBitmap;DestImage:TBitmap);

{******************************************************************************+
  Procedure SetControlEnabled
--------------------------------------------------------------------------------
  Enables or disables a control and all linked labels and spin-buttons. 
+******************************************************************************}
Procedure SetControlEnabled(AControl:TControl;AEnable:Boolean);

{******************************************************************************+
  Procedure SetControlGroupEnabled
--------------------------------------------------------------------------------
  Enabled or disables a control and all it's sub-controls.
+******************************************************************************}
Procedure SetControlGroupEnabled(AControl:TControl;AEnable:Boolean);

{******************************************************************************+
  Procedure SetControlVisible
--------------------------------------------------------------------------------
  Sets the visible-state of a control and all linked labels and spin-buttons.
+******************************************************************************}
Procedure SetControlVisible(AControl:TControl;AVisible:Boolean);

var // name and size of the font to use for dialog-boxes (TWForm automatically
    // selects thsi font)
    DialogFontName : String;
    DialogFontSize : Integer;

Implementation

Uses ActnList,CommDlg,ImgList,Lists,NumTools,SpinBtn,SysUtils;

Const // number of the last cursor added by AddCursor
      LastUserDefCursor : TCursor = 10000;

Type {*************************************************************************+
       Class TGlyphList
     ---------------------------------------------------------------------------
       Liste von Button-Images mit einer bestimmten Gr��e.
     {*************************************************************************}
     TGlyphList    = Class(TWImageList)
      Private
       FCount      : Integer;
       FUsed       : TBitList;
      Public
       Constructor CreateSize(AWidth,AHeight:Integer); 
       Destructor  Destroy; override;
       Function    Add(Image:TBitmap):Integer;
       Function    AddMasked(Image:TBitmap;MaskColor:TColor):Integer;
       Function    AllocateIndex:Integer;
       Procedure   Delete(Index:Integer);
       Property    Count:Integer read FCount;
     end;

     {*************************************************************************+
       Class TGlyphList                                                        
     --------------------------------------------------------------------------
       Liste von TGlyphList um verschiedene Bitmap-Gr��en speichern zu k�nnen. 
     {*************************************************************************}
     TGlyphCache   = Class
      Private
       FLists       : TList;
      Public
       Constructor Create;
       Destructor  Destroy; override;
       Function    GetList(AWidth,AHeight:Integer):TGlyphList;
       Procedure   ReturnList(AList:TGlyphList);
     end;

var Pattern             : TBitmap;          // bitmap used to draw checked menu-buttons
    GlyphCache          : TGlyphCache;      // cache for the menu-button-bitmaps

{==============================================================================+
  TWGroupbox
+==============================================================================}

Procedure TWGroupBox.CMEnabledChanged;
  begin
    Invalidate;
    inherited;
  end;

Procedure TWGroupBox.Paint;
  var X            : Integer;
      Y            : Integer;
      R            : TRect;
      S            : Array[0..255] of Char;
  begin
    with Canvas do if FBoxStyle=bsBox then inherited Paint
    else if FBoxStyle<>bsNone then begin
      Brush.Color:=Color;
      Font:=Self.Font;
      R:=Rect(0,0,0,0);
      StrPCopy(S,Caption);
      DrawText(Handle,S,Length(Caption),R,DT_LEFT or DT_SINGLELINE or DT_CALCRECT);
      if not Enabled then begin
        Brush.Style:=bsClear;
        Font.Color:=clBtnShadow;
      end;
      DrawText(Handle,S,Length(Caption),R,DT_LEFT or DT_SINGLELINE);
      X:=R.Right+3;
      Y:=Abs(Font.Height) Div 2+1;
      Pen.Color:=clBtnShadow;
      MoveTo(X,Y);
      LineTo(ClientWidth,Y);
      Pen.Color:=clBtnHighLight;
      MoveTo(X,Y+1);
      LineTo(ClientWidth,Y+1);
    end;
  end;

Procedure TWGroupBox.SetBoxStyle
   (
   AStyle          : TBoxStyle
   );
  begin
    if AStyle<>FBoxStyle then begin
      FBoxStyle:=AStyle;
      Invalidate;
    end;
  end;

{==============================================================================+
  TWindow
+==============================================================================}

Constructor TWindow.Create(AParent:TComponent);
begin
  inherited Create(AParent);
  FOwnDC:=FALSE;
  FMapMode:=mmText;
  FEraseBkgnd:=TRUE;
  ParentColor:=FALSE;
  Color:=clWindow;
  Width:=65;
  Height:=65;
  Canvas.OnChange:=DoChange;
end;

Procedure TWindow.WMNCCalcSize(var Msg:TWMNCCalcSize);
Const FrameWidth   = 2;
begin
  inherited;
  if Ctl3D then with Msg do begin
    Inc(CalcSize_Params^.rgrc[0].Top,FrameWidth);
    Inc(CalcSize_Params^.rgrc[0].Left,FrameWidth);
    Dec(CalcSize_Params^.rgrc[0].Bottom,FrameWidth);
    Dec(CalcSize_Params^.rgrc[0].Right,FrameWidth);
    Result:=0;
  end;
end;

Procedure TWindow.WMNCPaint(var Msg: TWMNCPaint);
var WindowDC       : THandle;
    Rect           : TRect;
begin
  inherited;
  if Ctl3D then begin
    WindowDC:=GetWindowDC(Handle);
    try
      Canvas.Handle:=WindowDC;
      with Canvas do begin
        Rect:=ClientRect;
        Inc(Rect.Right,4); Inc(Rect.Bottom,4);
        Frame3D(Canvas,Rect,clBtnShadow,clBtnHighlight,1);
        Frame3D(Canvas,Rect,clBtnHighlight,cl3DLight,1);
      end;
    finally
      Canvas.Handle:=0;                            
      ReleaseDC(Handle,WindowDC);
    end;
  end;
end;

Procedure TWindow.CMRecreateWnd(var Msg: TMessage);
begin
  inherited;
  SetDCMapMode(FMapMode);
end;

Procedure TWindow.DoChange(Sender:TObject);
begin
end;

Procedure TWindow.SetDCMapMode(AMapMode:TMapMode);
begin
  FMapMode:=AMapMode;
  if HandleAllocated then case FMapMode of
    mmAnIsotropic: SetMapMode(Canvas.Handle,mm_AnIsotropic);
    mmHiEnglish  : SetMapMode(Canvas.Handle,mm_HiEnglish);
    mmHiMetric   : SetMapMode(Canvas.Handle,mm_HiMetric);
    mmIsotropic  : SetMapMode(Canvas.Handle,mm_Isotropic);
    mmLoEnglish  : SetMapMode(Canvas.Handle,mm_LoEnglish);
    mmLoMetric   : SetMapMode(Canvas.Handle,mm_LoMetric);
    mmText       : SetMapMode(Canvas.Handle,mm_Text);
    mmTwips      : SetMapMode(Canvas.Handle,mm_Twips);
  end;
end;

Procedure TWindow.CreateWnd;
begin
  inherited CreateWnd;
  SetDCMapMode(FMapMode);
end;

Procedure TWindow.SetScrollBars(AScrollbars:TScrollStyle);
begin
  if AScrollbars<>FScrollbars then begin
    FScrollbars:=AScrollbars;
    RecreateWnd;
  end;
end;

Procedure TWindow.Paint;
begin
  inherited Paint;
  if Assigned(FOnPaint) then begin
    if not FOwnDC then SetDCMapMode(FMapMode);
    Canvas.Font.Assign(Font);
    OnPaint(Self);
  end;
end;

Procedure TWindow.WMEraseBkgnd(var Msg:TMessage);
var ARect        : TRect;
begin
  if FEraseBkgnd or (csDesigning in ComponentState) then begin
    if MapMode=mmText then inherited
    else begin
      ARect:=ClientRect;
      DPToLP(Canvas.Handle,ARect,2);
      Canvas.Brush:=Brush;
      Canvas.FillRect(ARect);
    end;
  end
  else Msg.Result:=1;
end;

Procedure TWindow.CreateParams(var Params:TCreateParams);
begin                                         
  inherited CreateParams(Params);
  if FOwnDC then with Params.WindowClass do begin
    Style:=Style or cs_OwnDC;
    Params.WinClassName:='TWindowOwnDC';
  end;
  if FScrollbars in [ssVertical,ssBoth] then Params.Style:=Params.Style or ws_VScroll;
  if FScrollbars in [ssHorizontal,ssBoth] then Params.Style:=Params.Style or ws_HScroll;
end;

Procedure TWindow.CMCtl3DChanged(var Msg:TMessage);
begin
  inherited;
  if HandleAllocated then RecreateWnd;
end;

{==============================================================================+
  TWForm
+==============================================================================}

Constructor TWForm.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
end;

Procedure TWForm.ReadState(Reader:TReader);
begin
  inherited ReadState(Reader);
  if BorderStyle=bsSizeable then begin
    ClientWidth:=MulDiv(ClientWidth,Screen.PixelsPerInch,96);
    ClientHeight:=MulDiv(ClientHeight,Screen.PixelsPerInch,96);
  end;
  with Constraints do begin
    MinHeight:=MulDiv(MinHeight,Screen.PixelsPerInch,96);
    MaxHeight:=MulDiv(MaxHeight,Screen.PixelsPerInch,96);
    MinWidth:=MulDiv(MinWidth,Screen.PixelsPerInch,96);
    MaxWidth:=MulDiv(MaxWidth,Screen.PixelsPerInch,96);
  end;
end;

Procedure TWForm.ChangeScale(M,D:Integer);
begin
  inherited ChangeScale(M,D);
  with Constraints do begin
    MinHeight:=MulDiv(MinHeight,M,D);
    MaxHeight:=MulDiv(MaxHeight,M,D);
    MinWidth:=MulDiv(MinWidth,M,D);
    MaxWidth:=MulDiv(MaxWidth,M,D);
  end;
end;

Procedure TWForm.CMShowingChanged
   (
   var Msg         : TMessage
   );
  begin
    if not FScaled then begin
      if Font.Name<>DialogFontname then
          Font.Name:=DialogFontName;
      if DialogFontSize<>Font.Size then
          ChangeScale(DialogFontSize,Font.Size);
      FScaled:=TRUE;
    end;
    inherited;
    if Showing then begin
      if Assigned(FOnShown) then OnShown(Self);
    end
    else if Assigned(FOnHidden) then OnHidden(Self);
  end;

Procedure TWForm.WMMDIActivate
   (
   var Msg         : TWMMDiActivate
   );
  begin
    inherited;
    if not (csDestroying in ComponentState) then begin
      if (Msg.DeactiveWnd=Handle)
          and Assigned(FOnMDIDeactivate) then
        OnMDIDeactivate(Self);
      if (Msg.ActiveWnd=Handle)
          and Assigned(FOnMDIActivate) then
        OnMDIActivate(Self);
    end;
  end;

{==============================================================================+
  TWLabel
+==============================================================================}

Procedure TWLabel.WMWindowPosChanged
   (
   var Msg         : TMessage
   );
  begin
    inherited;
    UpdateAlignment;
  end;
   
Procedure TWLabel.SetFocusControl
   (
   AControl        : TWinControl
   );
  begin
    inherited FocusControl:=AControl;
    UpdateAlignment;
  end;

Function TWLabel.GetFocusControl
   : TWinControl;
  begin
    Result:=inherited FocusControl;
  end;

Procedure TWLabel.SetAlignToControl
   (
   AAlign          : TAlignToControl
   );
  begin
    if AAlign<>FAlignToControl then begin
      FAlignToControl:=AAlign;
      UpdateAlignment;
      FAlignToControl:=AAlign;
    end;
  end;

Procedure TWLabel.UpdateAlignment;
  begin
    if FocusControl<>NIL then case FAlignToControl of
      alcTop    : Top:=FocusControl.Top;
      alcCenter : Top:=FocusControl.Top+(FocusControl.Height-Height) Div 2;
      alcBottom : Top:=FocusControl.Top+FocusControl.Height-Height;
    end;
 end;

Procedure TWLabel.Paint;
  var Buffer       : Array[0..128] of Char;
      Format       : Word;
      ARect        : TRect;
  begin
    with Canvas do begin
      UpdateAlignment;
      ARect:=ClientRect;
      if not Transparent then begin
        Brush.Color:=Self.Color;
        Brush.Style:=bsSolid;
        FillRect(ARect);
      end;
      Brush.Style:=bsClear;
      if Alignment=taCenter then Format:=DT_CENTER
      else if Alignment=taRightJustify then Format:=DT_RIGHT
      else Format:=DT_LEFT;
      Canvas.Font:=Self.Font;
      if not Enabled then Font.Color:=clBtnShadow;
      DrawText(Handle,StrPCopy(Buffer,Caption),Length(Caption),ARect,
          Format+DT_VCENTER);
    end;
  end;

Procedure SetControlGroupEnabled(AControl:TControl;AEnable:Boolean);
var Cnt          : Integer;
begin
  if AEnable<>AControl.Enabled then begin
    AControl.Enabled:=AEnable;
    if AControl is TWinControl then with AControl as TWinControl do
        for Cnt:=0 to ControlCount-1 do SetControlGroupEnabled(Controls[Cnt],AEnable);
  end;
end;

Procedure SetControlEnabled
   (
   AControl        : TControl;
   AEnable         : Boolean
   );
  var Cnt          : Integer;
      Item         : TControl;
  begin
    if AEnable<>AControl.Enabled then begin
      AControl.Enabled:=AEnable;
      for Cnt:=0 to AControl.Parent.ControlCount-1 do begin
        Item:=AControl.Parent.Controls[Cnt];
        if (Item is TLabel) and (TLabel(Item).FocusControl=AControl) then
            Item.Enabled:=AEnable
        else if (Item is TSpinBtn) and (TSpinBtn(Item).Control=AControl) then
            TSpinBtn(Item).Enabled:=AEnable;
      end;
    end;
  end;

Procedure SetControlVisible
   (
   AControl        : TControl;
   AVisible        : Boolean
   );
  var Cnt          : Integer;
      Item         : TControl;
  begin
    if AVisible<>AControl.Visible then begin
      AControl.Visible:=AVisible;
      for Cnt:=0 to AControl.Parent.ControlCount-1 do begin
        Item:=AControl.Parent.Controls[Cnt];
        if (Item is TLabel) and (TLabel(Item).FocusControl=AControl) then
            Item.Visible:=AVisible
        else if (Item is TSpinBtn) and (TSpinBtn(Item).Control=AControl) then
            TSpinBtn(Item).Visible:=AVisible;
      end;
    end;
  end;

{===============================================================================
| TWCustomListbox
+==============================================================================}

Constructor TWCustomListbox.Create
   (
   AParent         : TComponent
   );
  begin
    inherited Create(AParent);
    FScrollbars:=ssBoth;
    FHasStrings:=TRUE;
    FOddLineColor:=Color;
  end;

Procedure TWCustomListbox.WMEraseBkgnd
   (
   var Msg         : TMessage
   );
  var ARect        : TRect;
      Cnt          : Integer;
      Count        : Integer;
  begin
    if FOddLineColor=Color then inherited
    else with Canvas do begin
      Count:=ClientHeight Div (2*ItemHeight)+1;
      ARect:=Rect(0,0,ClientWidth,ItemHeight);
      if TopIndex Mod 1=1 then Brush.Color:=Color
      else Brush.Color:=FOddLineColor;
      for Cnt:=0 to Count do begin
        FillRect(ARect);
        OffsetRect(ARect,0,2*ItemHeight);
      end;
      ARect:=Rect(0,ItemHeight,ClientWidth,2*ItemHeight);
      if TopIndex Mod 1=0 then Brush.Color:=Color
      else Brush.Color:=FOddLineColor;
      for Cnt:=0 to Count do begin
        FillRect(ARect);
        OffsetRect(ARect,0,2*ItemHeight);
      end;
      Msg.Result:=1;
    end;
  end; 

Procedure TWCustomListbox.CNDrawItem
   (
   var Message     : TWMDrawItem
   );
  var State        : TOwnerDrawState;
  begin
    if FLockUpdates<=0 then with Message.DrawItemStruct^ do begin
      {$IFDEF WIN32}
//      State:=TOwnerDrawState(WordRec(LongRec(itemState).Lo).Lo);
      FDrawAction:=TOwnerDrawAction(WordRec(LongRec(itemAction).Lo).Lo);
      {$ELSE}
      State:=TOwnerDrawState(WordRec(itemState).Lo);
      FDrawAction:=TOwnerDrawAction(WordRec(itemAction).Lo);
      {$ENDIF}
      Canvas.Handle:=hDC;
      Canvas.Font:=Font;
      Canvas.Brush:=Brush;
      if itemID Mod 1=0 then Canvas.Brush.Color:=Self.Color
      else Canvas.Brush.Color:=FOddLineColor;
      if odSelected in State then begin
        Canvas.Brush.Color:=clHighlight;
        Canvas.Font.Color:=clHighlightText;
      end;
      if Integer(itemID)>=0 then DrawItem(itemID,rcItem,State)
      else Canvas.FillRect(rcItem);
      Canvas.Handle:=0;
    end;
  end;

Procedure TWCustomListbox.WMLButtonDown
   (                                     
   var Message     : TWMMouse
   );
  begin
    with Message do begin
      DoInherited:=TRUE;
      {$IFDEF WIN32}
      SendCancelMode(Self);
      {$ENDIF}
      if csCaptureMouse in ControlStyle then MouseCapture:=TRUE;
      MouseDown(mbLeft,KeysToShiftState(Keys),XPos,YPos);
      if DoInherited then DefaultHandler(Message);
    end;
  end;

Procedure TWCustomListbox.WMMouseMove
   (
   var Message     : TWMMouse
   );
  begin
    with Message do begin
      DoInherited:=TRUE;
      MouseMove(KeysToShiftState(Keys),XPos,YPos);
      if DoInherited then DefaultHandler(Message);
    end;
    SetAutoScrollTimer;
  end;

Procedure TWCustomListbox.CreateParams
   (
   var Params      : TCreateParams
   );
  begin
    inherited CreateParams(Params);                                     
    Params.Style:=Params.Style and not(ws_HScroll or ws_VScroll);
    if FScrollbars in [ssVertical,ssBoth] then Params.Style:=Params.Style or lbs_DisableNoScroll or ws_VScroll;
    if FScrollbars in [ssHorizontal,ssBoth] then Params.Style:=Params.Style or lbs_DisableNoScroll or ws_HScroll;
    if FHasStrings then Params.Style:=Params.Style or lbs_HasStrings
    {$IFDEF WIN32}
    else Params.Style:=(Params.Style or lbs_NoData or lbs_WantKeyboardInput) and not lbs_HasStrings;
    {$ELSE}
    else Params.Style:=(Params.Style or lbs_WantKeyboardInput) and not lbs_HasStrings;
    {$ENDIF}
  end;

Function TWCustomListbox.GetItemIndex
   : Integer;
  var Cnt          : Integer;
  begin
    Result:=-1;
    if MultiSelect then begin
      for Cnt:=Items.Count-1 downto 0 do if Selected[Cnt] then begin
        Result:=Cnt;
        Break;
      end;  
    end
    else Result:=inherited ItemIndex;
  end;

Procedure TWCustomListbox.SetItemIndex
   (
   AIndex          : Integer
   );
  begin
    if MultiSelect and (AIndex>=0) then begin
      if (SelCount>1) or not Selected[AIndex] then begin
        RangeSelected[0,Count-1]:=FALSE;
        RangeSelected[AIndex,AIndex]:=TRUE;
        FocusedIndex:=AIndex;
      end;
    end
    else inherited ItemIndex:=AIndex;
  end;

Procedure TWCustomListbox.SetScrollBars
   (
   AScrollbars     : TScrollStyle
   );
  begin
    if AScrollbars<>FScrollbars then begin
      FScrollbars:=AScrollbars;
      RecreateWnd;
    end;
  end;

Procedure TWCustomListbox.SetHasStrings
   (
   ASet            : Boolean
   );
  begin
    if ASet<>FHasStrings then begin
      FHasStrings:=ASet;
      RecreateWnd;
    end;
  end;

Function TWCustomListbox.GetCount
   : Integer;
  begin
    Result:=Items.Count;
  end;         
   
Procedure TWCustomListbox.SetCount
   (
   ACount          : Integer
   );
  var OldItemIndex : Integer;
      OldTopIndex  : Integer;
  begin
    if ACount<>Count then begin
      if not FHasStrings then begin
        LockUpdates;
        OldItemIndex:=ItemIndex;
        OldTopIndex:=TopIndex;
        if ACount>Count then InvalidateItems(Count,ACount)
        else InvalidateItems(ACount,Count);
        SendMessage(Handle,lb_SetCount,ACount,0);
        ItemIndex:=Min(OldItemIndex,ACount-1);
        TopIndex:=OldTopIndex;
        if TopIndex<>OldTopIndex then UnionRect(FPendingUpdates,FPendingUpdates,ClientRect);
        UnlockUpdates;
      end
      else begin
        Items.BeginUpdate;
        while Count>ACount do Items.Delete(Count-1);
        while Count<ACount do Items.Add(' ');
        Items.EndUpdate;
      end;
    end;
  end;

Procedure TWCustomListbox.ChangeScale(M,D:Integer);
begin
  inherited ChangeScale(M,D);
  ItemHeight:=(MulDiv(ItemHeight,M,D) Div 2)*2;
end;

Procedure TWCustomListbox.WMTimer(var Msg:TWMTimer);
begin
  TopIndex:=Max(0,Min(TopIndex+FAutoScrollDir,Count-ClientHeight Div ItemHeight));
  SetAutoScrollTimer;
end;

Procedure TWCustomListbox.DragOver
   (
   Source          : TObject;
   X               : Integer;
   Y               : Integer;
   State           : TDragState;
   var Accept      : Boolean
   );
  begin
    inherited DragOver(Source,X,Y,State,Accept);
    SetAutoScrollTimer;
  end;

Procedure TWCustomListbox.SetAutoScrollTimer;
  var Mouse        : TPoint;
  begin
    GetCursorPos(Mouse);
    Mouse:=ScreenToClient(Mouse);
    if Mouse.Y<0 then FAutoScrollDir:=-(Abs(Mouse.Y-10) Div 10)
    else if Mouse.Y>=ClientHeight then FAutoScrollDir:=(Mouse.Y-ClientHeight+10) Div 10
    else FAutoScrollDir:=0;
    if FAutoScrollDir<>0 then SetTimer(Handle,1,100,NIL)
    else KillTimer(Handle,1);
  end;

Procedure TWCustomListbox.LockUpdates;
  begin
    Inc(FLockUpdates);
    { disable the refresh of the window }
    if FLockUpdates=1 then begin
      SendMessage(Handle,wm_SetRedraw,0,0);
      FPendingUpdates:=Rect(0,0,0,0);
    end;  
  end;

Procedure TWCustomListbox.UnlockUpdates;
  begin
    if FLockUpdates>0 then begin
      Dec(FLockUpdates);
      { enable the redraw of the window }
      SendMessage(Handle,wm_SetRedraw,1,0);
      { wm_SetRedraw invalidates the whole client-area, let the listbox do this
      + by itself to avoid flicker }
      ValidateRect(Handle,NIL);
      if not IsRectEmpty(FPendingUpdates) then
          InvalidateRect(Handle,@FPendingUpdates,TRUE);
    end;
  end;

Procedure TWCustomListbox.WMPaint
   (
   var Msg         : TMessage
   );
  begin
    inherited;
    if Assigned(FOnPaint) then OnPaint(Self);
  end;    

Procedure TWCustomListbox.CNCharToItem
   (
   var Msg         : TWMCharToItem
   );
  begin
    Msg.Result:=-1;
  end;

Procedure TWCustomListbox.CNVKeyToItem
   (
   var Msg         : TWMVKeyToItem
   );
  begin
    Msg.Result:=-1
  end;                                    

Procedure TWCustomListbox.InvalidateItems
   (
   FromItem        : Integer;
   ToItem          : Integer
   );
  var ARect        : TRect;
  begin
    ARect:=Rect(0,(FromItem-TopIndex)*ItemHeight,ClientWidth,
        (ToItem-TopIndex+1)*ItemHeight);
    if FLockUpdates=0 then InvalidateRect(Handle,@ARect,TRUE)
    else UnionRect(FPendingUpdates,FPendingUpdates,ARect);
  end;    

Procedure TWCustomListbox.SetRangeSelected
   (
   AFrom           : Integer;
   ATo             : Integer;
   ASet            : Boolean
   );
  begin
    {$IFDEF WIN32}
    SendMessage(Handle,lb_SelItemRange,Byte(ASet),MakeLParam(AFrom,ATo));
    {$ELSE}
    SendMessage(Handle,lb_SelItemRange,Byte(ASet),MakeLong(AFrom,ATo));
    {$ENDIF}
  end;

Function TWCustomListbox.GetFocusedIndex
   : Integer;
  begin
    Result:=SendMessage(Handle,lb_GetCaretIndex,0,0);
  end;

Procedure TWCustomListbox.SetFocusedIndex(AIndex:Integer);
begin
  SendMessage(Handle,lb_SetCaretIndex,AIndex,0);
end;

Procedure TWCustomListbox.CMRecreateWnd(var Message:TMessage);
var OldCount       : Integer;
    OldItemIndex   : Integer;
begin     
  if not FHasStrings then begin       
    OldCount:=Count;
    OldItemIndex:=ItemIndex;
    inherited;
    Count:=OldCount;
    ItemIndex:=OldItemIndex;
  end
  else inherited;
end;
   
{===============================================================================
| TWList
+==============================================================================}

{$IFNDEF WIN32}
Procedure TWList.QuickSort
   (
   SortFunction    : TListSortCompare;
   L               : Integer;
   R               : Integer
   );
  var I            : Integer;
      J            : Integer;
      P            : Pointer;
      T            : Pointer;
  begin
    repeat
      I:=L;
      J:=R;
      P:=Items[(L+R) Shr 1];
      repeat
        while SortFunction(Items[I],P)<0 do Inc(I);
        while SortFunction(Items[J],P)>0 do Dec(J);
        if I<=J then begin
          T:=Items[I];
          Items[I]:=Items[J];
          Items[J]:=T;
          Inc(I);
          Dec(J);
        end;
      until I>J;
      if L<J then QuickSort(SortFunction,L,J);
      L:=I;
    until I>=R;
  end;
  
Procedure TWList.Sort
   (
   Compare         : TListSortCompare
   );
  begin
    QuickSort(Compare,0,Count-1);
  end;
{$ENDIF}

{===============================================================================
| TWTabbedNotebook
+==============================================================================}

Procedure TWTabbedNotebook.CMFontChanged
   (
   var Msg         : TMessage
   );
  begin
    inherited;
    TabFont:=Font;
  end;

{===============================================================================
| TWImageList
+==============================================================================}

{$IFNDEF WIN32}
Constructor TWImageList.CreateSize
   (
   AWidth          : Integer;
   AHeight         : Integer
   );
  begin
    Create(AWidth,AHeight);
  end;
{$ENDIF}

Procedure TWImageList.AddBitmaps
   (
   ABitmaps        : TBitmap
   );
  var ARect        : TRect;
      BRect        : TRect;
      BBitmap      : TBitmap; 
  begin
    ARect:=Rect(0,0,Width,Height);
    BRect:=ARect;
    BBitmap:=TBitmap.Create;
    try
      BBitmap.Width:=Width;
      BBitmap.Height:=Height;
      while BRect.Right<=ABitmaps.Width do begin
        BBitmap.Canvas.CopyRect(ARect,ABitmaps.Canvas,BRect);
        AddMasked(BBitmap,ABitmaps.TransparentColor);
        OffsetRect(BRect,Width,0);
      end;
    finally
      BBitmap.Free;
    end;
  end;

{==============================================================================}
{ TGlyphList                                                                   }
{==============================================================================}

Constructor TGlyphList.CreateSize
   (
   AWidth          : Integer;
   AHeight         : Integer
   );
  begin
    inherited CreateSize(AWidth,AHeight);
    FUsed:=TBitList.Create(0);
  end;

Destructor TGlyphList.Destroy;
  begin
    FUsed.Free;
    inherited Destroy;
  end;

Function TGlyphList.Add
   (
   Image           : TBitmap
   )
   : Integer;
  begin
    Result:=AllocateIndex;
    Replace(Result,Image,NIL);
    Inc(FCount);
  end;

Function TGlyphList.AddMasked
   (
   Image           : TBitmap;
   MaskColor       : TColor
   )
   : Integer;
  begin
    Result:=AllocateIndex;
    ReplaceMasked(Result,Image,MaskColor);
    Inc(FCount);
  end;

Procedure TGlyphList.Delete
   (
   Index           : Integer
   );
  begin
    if FUsed[Index] then begin
      Dec(FCount);
      FUsed[Index]:=False;
    end;
  end;

Function TGlyphList.AllocateIndex
   : Integer;
  begin
    Result:=FUsed.FirstCleared;
    if Result=-1 then begin
      Result:=inherited Add(NIL,NIL);
      FUsed.Capacity:=Result+1;
    end;
    FUsed[Result]:=True;
  end;

{==============================================================================}
{ TGlyphCache                                                                  }
{==============================================================================}

Constructor TGlyphCache.Create;
  begin
    inherited Create;
    FLists:=TList.Create;
  end;

Destructor TGlyphCache.Destroy;
  var Cnt          : Integer;
  begin
    for Cnt:=0 to FLists.Count-1 do TGlyphList(FLists[Cnt]).Free;
    FLists.Free;
    inherited Destroy;
  end;

Function TGlyphCache.GetList
   (
   AWidth          : Integer;
   AHeight         : Integer
   )
   : TGlyphList;
  var Cnt          : Integer;
  begin
    for Cnt:=FLists.Count-1 downto 0 do begin
      Result:=FLists[Cnt];
      with Result do if (AWidth=Width)
          and (AHeight=Height) then
        Exit;
    end;
    Result:=TGlyphList.CreateSize(AWidth,AHeight);
    FLists.Add(Result);
  end;

Procedure TGlyphCache.ReturnList
   (
   AList           : TGlyphList
   );
  begin
    if AList.Count=0 then begin
      FLists.Remove(AList);
      AList.Free;
    end;
  end;

Procedure CreateBrushPattern;
  var X            : Integer;
      Y            : Integer;
  begin
    Pattern:=TBitmap.Create;
    Pattern.Width:=8;
    Pattern.Height:=8;
    with Pattern.Canvas do begin
      Brush.Style:=bsSolid;
      Brush.Color:=clBtnFace;
      FillRect(Rect(0,0,Pattern.Width,Pattern.Height));
      for Y:=0 to 7 do for X:=0 to 7 do
        if (Y mod 2)=(X mod 2) then
            Pixels[X,Y]:=clBtnHighlight;
    end;
  end;

{===============================================================================
| TSpeedBtn
+==============================================================================}

Constructor TSpeedBtn.Create
   (
   AOwner          : TComponent
   );
  var ASize        : Integer;
  begin
    inherited Create(AOwner);
    ControlStyle:=[csCaptureMouse,csOpaque];
    if NewStyleControls then ASize:=22
    else ASize:=23;
    SetBounds(0,0,ASize,ASize);
    FBitmap:=TBitmap.Create;
    FNormalIndex:=-1;
    FDisabledIndex:=-1;
    FGroupIndex:=0;
  end;

Destructor TSpeedBtn.Destroy;
  begin
    FBitmap.Free;
    ClearGlyphs;
    inherited Destroy;
  end;

Procedure TSpeedBtn.PaintFrame(var ARect:TRect);
var ADown        : Boolean;
begin
  ADown:=FDown and Enabled;
  if ADown or (FDragging and FOver) then Frame3D(Canvas,ARect,clBtnShadow,clBtnHighlight,1)
  else if FDragging or FOver then Frame3D(Canvas,ARect,clBtnHighlight,clBtnShadow,1)
  else if csDesigning in ComponentState then Frame3D(Canvas,ARect,clBtnHighlight,clBtnShadow,1)
  else Frame3D(Canvas,ARect,clBtnFace,clBtnFace,1)
end;

Procedure TSpeedBtn.Paint;
  var ARect        : TRect;
      ADown        : Boolean;
      IHeight      : Integer;
      IWidth       : Integer;
      HasDisabled  : Boolean;
      AIndex       : Integer;
      TmpImage     : TBitmap;
      IRect        : TRect;
  begin
    ARect:=ClientRect;
    ADown:=FDown and Enabled;
    Canvas.Brush.Color:=clBtnFace;
    Canvas.Brush.Style:=bsSolid;
    PaintFrame(ARect);
    if ADown then begin
      if Pattern=NIL then CreateBrushPattern;
      Canvas.Brush.Bitmap:=Pattern;
    end;
    Canvas.FillRect(ARect);
    InflateRect(ARect,-1,-1);
    ARect:=ClientRect;
    if ADown or (FDragging and FOver) then OffsetRect(ARect,1,1);
    if not Assigned(FBitmap)
        or (FBitmap.Height=0)
        or (FBitmap.Width=0) then
      Exit;
    IWidth:=FBitmap.Width;
    IHeight:=FBitmap.Height;
    HasDisabled:=IWidth>IHeight;
    if HasDisabled then IWidth:=IWidth Div 2;
    if FGlyphList=NIL then FGlyphList:=GlyphCache.GetList(IWidth,IHeight);
    IRect:=Rect(0,0,IWidth,IHeight);
    if not Enabled then AIndex:=FDisabledIndex
    else AIndex:=FNormalIndex;                  
    if AIndex=-1 then begin
      TmpImage:=TBitmap.Create;
      try
        TmpImage.Width:=IWidth;
        TmpImage.Height:=IHeight;
        if not Enabled then begin
          CreateDisabledBitmap(FBitmap,TmpImage); 
          FDisabledIndex:=TGlyphList(FGlyphList).Add(TmpImage);
          AIndex:=FDisabledIndex;
        end
        else begin
          FNormalIndex:=TGlyphList(FGlyphList).AddMasked(FBitmap,FBitmap.TransparentColor);
          AIndex:=FNormalIndex;
        end;
      finally
        TmpImage.Free;
      end;                                                     
      FBitmap.Dormant;
    end;
    TGlyphList(FGlyphList).Draw(Canvas,ARect.Left+(ARect.Right-ARect.Left-IRect.Right) Div 2,
        ARect.Top+(ARect.Bottom-ARect.Top-IRect.Bottom) Div 2,AIndex);
  end;

Procedure TSpeedBtn.SetBitmap
   (
   ABitmap         : TBitmap
   );
  begin
    FBitmap.Assign(ABitmap);
    ClearGlyphs;
    Invalidate;
  end;

Procedure TSpeedBtn.MouseMove
   (
   Shift           : TShiftState;
   X               : Integer;
   Y               : Integer
   );
  var ARect        : TRect;
      NewOver      : Boolean;
  begin
    inherited MouseMove(Shift,X,Y);
    if FDragging and not FDown then begin
      ARect:=ClientRect;
      NewOver:=ptInRect(ARect,Point(X,Y));
      if NewOver<>FOver then begin
        FOver:=NewOver;
        Repaint;
      end;
    end;
  end;

Procedure TSpeedBtn.MouseDown
   (
   Button          : TMouseButton;
   Shift           : TShiftState;
   X               : Integer;
   Y               : Integer
   );
  begin
    inherited MouseDown(Button,Shift,X,Y);
    if (Button=mbLeft) and Enabled then begin
      FDragging:=TRUE;
      FOver:=TRUE;
      Repaint;
    end;
  end;

Procedure TSpeedBtn.MouseUp
   (
   Button          : TMouseButton;
   Shift           : TShiftState;
   X               : Integer;
   Y               : Integer
   );
  var ARect        : TRect;
  begin
    inherited MouseUp(Button,Shift,X,Y);
    if FDragging then begin
      FDragging:=FALSE;
      ARect:=ClientRect;
      if ptInRect(ARect,Point(X,Y)) then begin
        if FGroupIndex=0 then Invalidate
        else if not Down then SetDown(TRUE)
        else if FAllowAllUp then SetDown(FALSE);
        Click;
      end
      else Invalidate;
    end;
  end;

Procedure TSpeedBtn.SetDown(ADown:Boolean);
var Msg          : TMessage;
begin
  if FDown<>ADown then begin
    FDown:=ADown;
    if Action is TCustomAction then TCustomAction(Action).Checked:=FDown;
    if FDown and (FGroupIndex>0) then begin
      Msg.Msg:=CM_BUTTONPRESSED;
      Msg.WParam:=FGroupIndex;
      Msg.LParam:=LongInt(Self);
      Msg.Result:=0;
      Parent.Broadcast(Msg);
    end;
    Invalidate;
  end;
end;

Procedure TSpeedBtn.CMEnabledChanged;
  begin
    Invalidate;
    inherited;
  end;

Procedure TSpeedBtn.CMSysColorChange;
  begin
    ClearGlyphs;
    Pattern.Free;
    Pattern:=NIL;
    Invalidate;
  end;

Procedure TSpeedBtn.ClearGlyphs;
  begin
    if FGlyphList<>NIL then begin
      if FNormalIndex<>-1 then TGlyphList(FGlyphList).Delete(FNormalIndex);
      if FDisabledIndex<>-1 then TGlyphList(FGlyphList).Delete(FDisabledIndex);
      if TGlyphList(FGlyphList).Count=0 then GlyphCache.ReturnList(FGlyphList);
      FNormalIndex:=-1;
      FDisabledIndex:=-1;
      FGlyphList:=NIL;
    end;
  end;

Procedure TSpeedBtn.CMMouseEnter(var Message:TMessage);
  var ARect        : TRect;
  begin
    FOver:=TRUE;
    ARect:=ClientRect;
    PaintFrame(ARect);
  end;

Procedure TSpeedBtn.CMMouseLeave(var Message:TMessage);
  var ARect        : TRect;
  begin
    FOver:=FALSE;
    ARect:=ClientRect;
    PaintFrame(ARect);
  end;

Procedure TSpeedBtn.CMButtonPressed(var Message:TMessage);
var Sender       : TSpeedBtn;
begin
  if Message.WParam=FGroupIndex then begin
    Sender:=TSpeedBtn(Message.LParam);
    if (Sender<>Self) and (Sender is TSpeedBtn)
        and Sender.Down then SetDown(FALSE);
  end;
end;

Procedure TSpeedBtn.ActionChange(Sender: TObject; CheckDefaults: Boolean);
  Procedure CopyImage(ImageList:TCustomImageList;Index:Integer);
  begin
    with FBitmap do begin
      Width:=ImageList.Width;
      Height:=ImageList.Height;
      Canvas.Brush.Color:=clFuchsia;//! for lack of a better color
      Canvas.FillRect(Rect(0,0,Width,Height));
      ImageList.Draw(Canvas,0,0,Index);
    end;
  end;
begin                                                
  inherited ActionChange(Sender,CheckDefaults);
  if Sender is TCustomAction then with Sender as TCustomAction do begin
    ClearGlyphs;
    if (ActionList<>NIL) and (ActionList.Images<>NIL)
        and (ImageIndex>=0) and (ImageIndex<ActionList.Images.Count) then
      CopyImage(ActionList.Images,ImageIndex);
    SetDown(Checked);  
  end;
end;

{===============================================================================
| TWStatusBar
+==============================================================================}

Constructor TWStatusBar.Create
   (
   AOwner          : TComponent
   );
  var StatusPanel  : TStatusPanel;
  begin
    inherited Create(AOwner);
    FProgressPanels:=TStatusPanels.Create(Self);
    StatusPanel:=TStatusPanel.Create(FProgressPanels);
    StatusPanel.Bevel:=pbNone;
    StatusPanel:=TStatusPanel.Create(FProgressPanels);
    StatusPanel.Width:=30;
    StatusPanel.Bevel:=pbNone;
    StatusPanel.Alignment:=taRightJustify;
    StatusPanel:=TStatusPanel.Create(FProgressPanels);
    StatusPanel.Width:=208;
    StatusPanel.Bevel:=pbNone;
    StatusPanel.Style:=psOwnerDraw;
    StatusPanel:=TStatusPanel.Create(FProgressPanels);
    StatusPanel.Bevel:=pbNone;
    OnDrawPanel:=DoDrawPanel;
    FCancelText:=TRUE;
    FStoredPanels:=TStatusPanels.Create(Self);
  end;

Destructor TWStatusBar.Destroy;
begin
  FProgressPanels.Free;
  FStoredPanels.Free;
  inherited Destroy;
end;

Procedure TWStatusBar.SetPanelText
   (
   AIndex          : Integer;
   Const AText     : String
   );
  var Rect         : TRect;
      ARect        : TRect;
      BRect        : TRect;  
  begin
    { query the current invalidated area }
    GetUpdateRect(Handle,ARect,FALSE);
    { update the panle-text }
    Panels[AIndex].Text:=AText;
    { delphi invalidates the whole statusbar, validate it }
    ValidateRect(Handle,NIL);
    { get the rectangle of the statusbar-field }
    SendMessage(Handle,sb_GetRect,AIndex,LongInt(@Rect));
    { union the filed-area with the previous update-area }
    UnionRect(BRect,Rect,ARect);
    { invalidate it }
    InvalidateRect(Handle,@BRect,TRUE);
  end;

Procedure TWStatusBar.SetProgressPanel
   (
   ASet            : Boolean
   );
  begin
    if FInProgress<>ASet then begin
      SendMessage(Handle,wm_SetRedraw,0,0);
      FInProgress:=ASet;
      if not ASet then begin
        if FDisabled<>NIL then begin
          EnableTaskWindows(FDisabled);
          FDisabled:=NIL;
        end;
        Panels:=FStoredPanels;
      end
      else begin
        FStoredPanels.Assign(Panels);
        Panels:=FProgressPanels;
        Panels[0].Text:=FProgressText;
        Panels[0].Width:=Canvas.TextWidth(Panels[0].Text)+5;
        Panels[1].Width:=Canvas.TextWidth('100%')+5;
{        if FCancelText then Panels[3].Text:=MlgStringList['StatusBar',1];}
        Progress:=0.0;
        Application.ProcessMessages;
        FDisabled:=DisableTaskWindows(Handle);
{        WinGISMainForm.EscPressed:=FALSE;}
      end;
      Resize;
      SendMessage(Handle,wm_SetRedraw,1,0);
      InvalidateRect(Handle,NIL,TRUE);
      Update;
    end;
  end;

Procedure TWStatusBar.Resize;
  begin
    if FInProgress then begin
      Panels[0].Width:=Canvas.TextWidth(Panels[0].Text)+5;
      {Panels[0].Height:=0;}
      Panels[1].Width:=Canvas.TextWidth('100%')+5;
      {Panels[1].Height:=0;}
    end;
    inherited Resize;
  end;

Procedure TWStatusBar.DoDrawPanel
   (
   StatusBar       : TStatusBar;
   Panel           : TStatusPanel;
   const Rect      : TRect
   );
  var ARect        : TRect;
      AWidth       : Integer;
      Cnt          : Integer;
  begin
    with Canvas do begin
      ARect:=Rect; 
      InflateRect(ARect,0,-2);
      Frame3D(Canvas,ARect,clBtnShadow,clBtnHighlight,1);
      InflateRect(ARect,-1,-1);
      AWidth:=(ARect.Right-ARect.Left) Div 20;
      ARect.Right:=ARect.Left+AWidth-2;
      Brush.Color:=clActiveCaption;
      for Cnt:=1 to (FProgress+4) Div 5 do begin
        FillRect(ARect);
        OffsetRect(ARect,AWidth,0);
      end;
      ARect.Right:=Rect.Right-3;
      Brush.Color:=Color;
      FillRect(ARect);
    end;
  end;

Procedure TWStatusBar.SetProgress
   (
   Const AProgress : Real
   );
  var BProgress    : Integer;
      Rect         : TRect;
  begin
    BProgress:=Round(AProgress);
    if BProgress<>FProgress then begin
      Panels[1].Text:=IntToStr(BProgress)+'%';
      ValidateRect(Handle,NIL);
      if (FProgress+4) Div 5<>(BProgress+4) Div 5 then begin
        SendMessage(Handle,sb_GetRect,2,LongInt(@Rect));
        InvalidateRect(Handle,@Rect,TRUE);
{        InflateRect(Rect,-1,-1);
        DoDrawPanel(Self,Panels[2],Rect);}
      end;
      FProgress:=BProgress;
      SendMessage(Handle,sb_GetRect,1,LongInt(@Rect));
      InvalidateRect(Handle,@Rect,FALSE);
      Update;
    end;
  end;

Procedure TWStatusBar.SetProgressText
   (
   Const AText     : String
   );
  begin
    if FInProgress then begin
      FProgressText:=AText;
      Panels[0].Text:=FProgressText;
      Panels[0].Width:=Canvas.TextWidth(Panels[0].Text)+5;
      Update;
    end;
  end;

Function TWStatusBar.QueryAbort
   : Boolean;
  begin
    Result:=FALSE;
    if FInProgress then begin
      Application.ProcessMessages;
{      Result:=WinGISMainForm.EscPressed;
      if Result
          and (AbortText<>'') then begin
        Result:=MessageDialog(AbortText,mtConfirmation,[mbYes,mbNo],0)=mrYes;
        if not Result then WinGISMainForm.EscPressed:=FALSE;
      end;                               }
    end;
  end;

Function AddCursor
   (
   AHandle         : THandle
   )
   : TCursor;
  begin
    Screen.Cursors[LastUserDefCursor]:=AHandle;
    Result:=LastUserDefCursor;
//    Inc(LastUserDefCursor);
  end;

Procedure CreateDisabledBitmap
   (
   ABitmap         : TBitmap;
   DestImage       : TBitmap
   );
  Const ROP_DSPDxax     = $00E20746;
  var IWidth       : Integer;
      IHeight      : Integer;
      MonoBmp      : TBitmap;
      ORect        : TRect;
      IRect        : TRect;
      HasDisabled  : Boolean;
  begin
    IWidth:=ABitmap.Width;
    IHeight:=ABitmap.Height;
    HasDisabled:=IWidth>IHeight;
    if HasDisabled then IWidth:=IWidth Div 2;
    IRect:=Rect(0,0,IWidth,IHeight);
    ORect:=IRect;
    if HasDisabled then OffsetRect(ORect,IWidth,0);
    DestImage.Width:=IWidth;
    DestImage.Height:=IHeight;
    MonoBmp:=TBitmap.Create;
    try
      if HasDisabled then with DestImage.Canvas do begin
        DestImage.Canvas.Brush.Color:=clBtnFace;
        BrushCopy(IRect,ABitmap,ORect,ABitmap.TransparentColor);
        MonoBmp.Width:=IWidth;
        MonoBmp.Height:=IHeight;
        MonoBmp.Canvas.CopyRect(IRect,ABitmap.Canvas,ORect);
        MonoBmp.Canvas.Brush.Color:=clWhite;
        MonoBmp.Canvas.Font.Color:=clBlack;
        MonoBmp.Monochrome:=TRUE;
        { replace white-color with clBtnHightlight }
        Brush.Color:=clBtnHighLight;
        SetBkColor(Handle,clWhite);
        BitBlt(Handle,0,0,IWidth,IHeight,MonoBmp.Canvas.Handle,0,0,ROP_DSPDxax);
        MonoBmp.Assign(DestImage);
        MonoBmp.Canvas.Brush.Color:=clGray;
        MonoBmp.Monochrome:=TRUE;
        { replace gray-color with clBtnShadow }
        Brush.Color:=clBtnShadow;
        SetBkColor(Handle,clWhite);
        BitBlt(Handle,0,0,IWidth,IHeight,MonoBmp.Canvas.Handle,0,0,ROP_DSPDxax);
      end
      else begin
        with MonoBmp do begin
          Assign(ABitmap);
          {$IFDEF WIN32}
          HandleType:=bmDDB;
          {$ENDIF}
          Canvas.Brush.Color:=clBlack;
          Width:=IWidth;
          if Monochrome then begin
            Canvas.Font.Color:=clWhite;
            Monochrome:=False;
            Canvas.Brush.Color:=clWhite;
          end;
          Monochrome:=True;
        end;
        with DestImage.Canvas do begin
          Brush.Color:=clBtnFace;
          FillRect(IRect);
          Brush.Color:=clBtnHighlight;
          SetTextColor(Handle,clBlack);
          SetBkColor(Handle,clWhite);
          BitBlt(Handle,1,1,IWidth,IHeight,MonoBmp.Canvas.Handle,0,0,ROP_DSPDxax);
          Brush.Color:=clBtnShadow;
          SetTextColor(Handle,clBlack);
          SetBkColor(Handle,clWhite);
          BitBlt(Handle,0,0,IWidth,IHeight,MonoBmp.Canvas.Handle,0,0,ROP_DSPDxax);
        end;
      end;
    finally
      MonoBmp.Free;
    end;
  end;

{===============================================================================
| TWListView
+==============================================================================}

{$IFDEF WIN32}
Constructor TWListView.Create
   (
   AOwner          : TComponent
   );
  begin
    inherited Create(AOwner);
    FCanvas:=TControlCanvas.Create;
  end;
   
Destructor TWListView.Destroy;
  begin
    FCanvas.Free;
    inherited Destroy;
  end;
Procedure TWListView.CreateParams
   (
   var Params      : TCreateParams
   );
  begin
    inherited CreateParams(Params);
    if FOwnerDraw then Params.Style:=Params.Style or LVS_OwnerDrawFixed;
  end;

Procedure TWListView.CNDrawItem
   (
   var Message     : TWMDrawItem
   );
  var State        : TOwnerDrawState;
  begin
    with Message.DrawItemStruct^ do begin
      {$IFDEF WIN32}
//      State:=TOwnerDrawState(WordRec(LongRec(itemState).Lo).Lo);
      FDrawAction:=TOwnerDrawAction(WordRec(LongRec(itemAction).Lo).Lo);
      {$ELSE}
      State:=TOwnerDrawState(WordRec(itemState).Lo);
      FDrawAction:=TOwnerDrawAction(WordRec(itemAction).Lo);
      {$ENDIF}
      Canvas.Handle:=hDC;
      Canvas.Font:=Font;
      Canvas.Brush:=Brush;
      if odSelected in State then begin
        Canvas.Brush.Color:=clHighlight;
        Canvas.Font.Color:=clHighlightText;
      end;
      if (Integer(itemID)>=0) and Assigned(FOnDrawItem) then
          OnDrawItem(Self,itemID,rcItem,State)
      else Canvas.FillRect(rcItem);
      Canvas.Handle:=0;
    end;
  end;

Function TWListView.GetItemRect
   (
   Item            : Integer;
   SubItem         : Integer
   )
   : TRect;
  begin
    ListView_GetSubItemRect(Handle,Item,SubItem,LVIR_BOUNDS,@Result);
  end;

{$ENDIF}

{===============================================================================
  TWButton
+==============================================================================}

Procedure TWButton.CreateParams(var Params:TCreateParams);
begin
  inherited CreateParams(Params);
  {$IFDEF WIN32}
  if FFlat then Params.Style:=Params.Style or bs_Flat;
  {$ENDIF}
end;

Procedure TWButton.SetFlat(AFlat:Boolean);
begin
  if FFlat<>AFlat then begin
    FFlat:=AFlat;
    RecreateWnd;
  end;
end;

{==============================================================================+
  TWRadioGroup
+==============================================================================}

Procedure TWRadioGroup.Paint;
  var X            : Integer;
      Y            : Integer;
      R            : TRect;
      S            : Array[0..255] of Char;
  begin
    with Canvas do if FBoxStyle=bsBox then inherited Paint
    else if FBoxStyle<>bsNone then begin
      Brush.Color:=Color;
      Font:=Self.Font;
      R:=Rect(0,0,0,0);
      StrPCopy(S,Caption);
      DrawText(Handle,S,Length(Caption),R,DT_LEFT or DT_SINGLELINE or DT_CALCRECT);
      if Enabled then DrawText(Handle,S,Length(Caption),R,DT_LEFT or DT_SINGLELINE)
      else begin
        Brush.Style:=bsClear;
        Font.Color:=clBtnHighlight;
        OffsetRect(R,1,1);
        DrawText(Handle,S,Length(Caption),R,DT_LEFT or DT_SINGLELINE);
        Font.Color:=clBtnShadow;
        OffsetRect(R,-1,-1);
        DrawText(Handle,S,Length(Caption),R,DT_LEFT or DT_SINGLELINE);
      end;
      if R.Right=0 then X:=0
      else X:=R.Right+3;
      Y:=Abs(Font.Height) Div 2+1;
      Pen.Color:=clBtnShadow;
      MoveTo(X,Y);
      LineTo(ClientWidth,Y);
      Pen.Color:=clBtnHighLight;
      MoveTo(X,Y+1);
      LineTo(ClientWidth,Y+1);
    end;
  end;

Procedure TWRadioGroup.SetBoxStyle
   (
   AStyle          : TBoxStyle
   );
  begin
    if AStyle<>FBoxStyle then begin
      FBoxStyle:=AStyle;
      Invalidate;
    end;
  end;

Procedure TextRectEx(Canvas:TCanvas;Const Rect:TRect;const Text:String;
    BackgroundColor:TColor;State:TOwnerDrawState);
var ARect          : TRect;
    Size           : TSize;
begin
  Size:=Canvas.TextExtent(Text);
  ARect:=Classes.Rect(Rect.Left+2,Rect.Top,Rect.Left+6+Size.cx,Rect.Bottom);
  IntersectRect(ARect,ARect,Rect);
  Canvas.TextRect(ARect,ARect.Left+2,CenterHeightInRect(Size.cy,ARect),Text);
  if odFocused in State then Canvas.DrawFocusRect(ARect);
  ARect.Left:=ARect.Right;
  ARect.Right:=Rect.Right;
  Canvas.Brush.Color:=BackgroundColor;
  Canvas.FillRect(ARect);
end;

{==============================================================================+
  TWCustomComboBox
+==============================================================================}

Procedure TWCustomCombobox.CNDrawItem(var Message:TWMDrawItem);
var State        : TOwnerDrawState;
begin
  with Message.DrawItemStruct^ do begin
    {$IFDEF WIN32}
//    State:=TOwnerDrawState(WordRec(LongRec(itemState).Lo).Lo);
    FDrawAction:=TOwnerDrawAction(WordRec(LongRec(itemAction).Lo).Lo);
    {$ELSE}
    State:=TOwnerDrawState(WordRec(itemState).Lo);
    FDrawAction:=TOwnerDrawAction(WordRec(itemAction).Lo);
    {$ENDIF}
    Canvas.Handle:=hDC;
    Canvas.Font:=Font;
    Canvas.Brush:=Brush;
    if itemID Mod 1=0 then Canvas.Brush.Color:=Self.Color
    else Canvas.Brush.Color:=FOddLineColor;
    if odSelected in State then begin
      Canvas.Brush.Color:=clHighlight;
      Canvas.Font.Color:=clHighlightText;
    end;
    if Integer(itemID)>=0 then DrawItem(itemID,rcItem,State)
    else Canvas.FillRect(rcItem);
    Canvas.Handle:=0;
  end;
end;

{ TWOpenDialog }

Procedure TWOpenDialog.DoShow;
begin
  inherited DoShow;
  if ofReadOnly in Options then {++Sygsky :-}
    SendMessage(GetParent(Handle),CDM_SETCONTROLTEXT,1040,Integer(FReadOnlyCaption));
end;

{ TWSaveDialog }

procedure TWSaveDialog.DoShow;
begin
  inherited DoShow;
  if ofReadOnly in Options then {++Sygsky :-}
    SendMessage(GetParent(Handle),CDM_SETCONTROLTEXT,1040,Integer(FReadOnlyCaption));
end;

{==============================================================================+
  Initialisierungs- und Terminierungscode
+==============================================================================}

function TSpeedBtn.GetActionLinkClass: TControlActionLinkClass;
begin
  Result:=TSpeedBtnActionLink;
end;

{ TSpeedBtnActionLink }

Procedure TSpeedBtnActionLink.AssignClient(AClient: TObject);
begin
  inherited AssignClient(AClient);
  FClient:=TSpeedBtn(AClient);
end;

Function TSpeedBtnActionLink.IsCheckedLinked: Boolean;
begin
  Result := inherited IsCheckedLinked and
    (FClient.Down = (Action as TCustomAction).Checked);
end;

Procedure TSpeedBtnActionLink.SetChecked(Value: Boolean);
begin
  if IsCheckedLinked then FClient.Down := Value;
end;

procedure Register;
begin
  RegisterComponents('Gis-Controls', [TWButton]);
  RegisterComponents('Gis-Controls', [TWStatusBar]);
  RegisterComponents('Gis-Controls', [TWCustomListbox]);
  RegisterComponents('Gis-Controls', [TWCustomCombobox]);
  RegisterComponents('Gis-Controls', [TWListBox]);
  RegisterComponents('Gis-Controls', [TWGroupBox]);
  RegisterComponents('Gis-Controls', [TWLabel]);
  RegisterComponents('Gis-Controls', [TWindow]);
  RegisterComponents('Gis-Controls', [TWForm]);
  RegisterComponents('Gis-Controls', [TWTabbedNoteBook]);
  RegisterComponents('Gis-Controls', [TWImageList]);
  RegisterComponents('Gis-Controls', [TSpeedBtn]);
  RegisterComponents('Gis-Controls', [TWListView]);
  RegisterComponents('Gis-Controls', [TWRadioGroup]);
  RegisterComponents('Gis-Controls', [TWOpenDialog]);
  RegisterComponents('Gis-Controls', [TWSaveDialog]);
end;


Initialization
  Pattern:=NIL;
  GlyphCache:=TGlyphCache.Create;
  { set default dialog-font-name and size }
  DialogFontName:='MS Sans Serif';
  DialogFontSize:=8;

Finalization
  Pattern.Free;
  GlyphCache.Free;

end.
