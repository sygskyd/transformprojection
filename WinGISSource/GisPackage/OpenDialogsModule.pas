unit OpenDialogsModule;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type TOpenDialogType=(file_open, file_save, font_selection); // set of possible Dialog types

type
  TOpenDialogsModuleWnd = class(TForm)
    SetupWindowTimer: TTimer;
    SaveDialog: TSaveDialog;
    OpenDialog: TOpenDialog;
    FontDialog: TFontDialog;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    ParentHandle:integer;
    OpenMode    :TOpenDialogType;
    Filename:string;  // filename to store result of open/save dialog
    Fontname:string;  // name of font that had been selected
    Fontsize:integer; // size of font that had been selected
  end;

var
  OpenDialogsModuleWnd: TOpenDialogsModuleWnd;

implementation

{$R *.dfm}

procedure TOpenDialogsModuleWnd.FormCreate(Sender: TObject);
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   Self.Height:=1;
   Self.Width:=1;
   Self.BorderStyle:=bsNone;
   OpenMode:=file_open; // File open is default dialog
end;

procedure TOpenDialogsModuleWnd.FormShow(Sender: TObject);
begin
   SetupWindowTimer.Enabled:=TRUE;
end;

procedure TOpenDialogsModuleWnd.SetupWindowTimerTimer(Sender: TObject);
begin
   SetupWindowTimer.Enabled:=FALSE;
   windows.setparent (Self.Handle, ParentHandle);
   SetWindowPos(Handle,HWND_TOP,1,1,1, 1,SWP_SHOWWINDOW);
   Self.Repaint;

   // reset results
   Filename:='';
   Fontname:='';
   Fontsize:=0;
   if OpenMode = file_open then
   begin
      if OpenDialog.Execute then
         Filename:=OpenDialog.Filename;
   end
   else if OpenMode = file_save then
   begin
      if SaveDialog.Execute then
         Filename:=SaveDialog.Filename;
   end
   else if OpenMode = font_selection then
   begin
      if FontDialog.Execute then
      begin
         Fontname:=FontDialog.Font.Name;
         Fontsize:=FontDialog.Font.Size;
      end;
   end;
   Self.Close;
end;

end.
