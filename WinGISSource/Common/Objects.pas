unit Objects;

interface

uses
  Windows, SysUtils, Classes;

const

   { TOldStream_Old access modes }

  stCreate = $3C00; { Create new file }
  stOpenRead = $3D00; { Read access only }
  stOpenWrite = $3D01; { Write access only }
  stOpen = $3D02; { Read and write access }

   { TOldStream_Old error codes }

  stOk = 0; { No error }
  stError = -1; { Access error }
  stInitError = -2; { Cannot initialize stream }
  stReadError = -3; { Read beyond end of stream }
  stWriteError = -4; { Cannot expand stream }
  stGetError = -5; { Get of unregistered object type }
  stPutError = -6; { Put of unregistered object type }

   { Maximum TCollection size }

  MaxCollectionSize = MaxLongInt div SizeOf(Pointer);

   { TCollection error codes }

  coIndexError = -1; { Index out of range }
  coOverflow = -2; { Overflow }

   { VMT header size }

  vmtHeaderSize = 8;

   {++ Moskaliov Business Graphics BUILD#150 17.01.01}
const
  co_Bit0: Byte = $01; { Mask for 0-bit    |7|6|5|4|3|2|1|0| }
  co_Bit1: Byte = $02; { Mask for 1-bit    |7|6|5|4|3|2|1|0| }
  co_Bit2: Byte = $04; { Mask for 2-bit    |7|6|5|4|3|2|1|0| }
  co_Bit3: Byte = $08; { Mask for 3-bit    |7|6|5|4|3|2|1|0| }
  co_Bit4: Byte = $10; { Mask for 4-bit    |7|6|5|4|3|2|1|0| }
  co_Bit5: Byte = $20; { Mask for 5-bit    |7|6|5|4|3|2|1|0| }
  co_Bit6: Byte = $40; { Mask for 6-bit    |7|6|5|4|3|2|1|0| }
  co_Bit7: Byte = $80; { Mask for 7-bit    |7|6|5|4|3|2|1|0| }

  co_BaseChartSize: Integer = 1000; { Base Chart Size                   }
  co_HalfChartSize: Integer = 500; { Half of the Base Chart Size       }
  co_RelChartPercent: Integer = 50; { Template Percent                  }
  co_FontWidthPrec: Integer = 10; { Chart Font Width Precision        }
  co_TextVisible: Integer = 3; { Chart Font Height Text Visibility }
  co_PermisPercent: Double = 0.01; { Permissible Percent (coefficient) }
   {            of the Size Difference }

  co_DelphiExeName: string = 'delphi32.exe';

var
  ChartStoreVersion: Byte; { Chart Kind load-store version     }
   {+++ Brovak Bug 541 build 168 - show progress bar while saving project}
  ShowProgressPut: boolean = false;
  PercentPut, PercentNowPut, PercentOldPut, TotalCountPut: real;
   {-- Brovak}

{ WinGIS runing state type}
type
  TWinGIS_RunState = (runNotDef, runWinGIS, runComponent);

function WinGIS_runing: Boolean; { WinGIS is runing ?                }
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

type

   { Type conversion records }

  WordRec = record
    Lo, Hi: Byte;
  end;

  LongRec = record
    Lo, Hi: Word;
  end;

  PtrRec = record
    Ofs, Seg: Word;
  end;

   { String pointers }

   { Character set type }

  PCharSet = ^TCharSet;
  TCharSet = set of Char;

   { General arrays }

  PByteArray = ^TByteArray;
  TByteArray = array[0..32767] of Byte;

  PWordArray = ^TWordArray;
  TWordArray = array[0..16383] of Word;

   { TObject base object }

  POldObject = ^TOldObject;
  TOldObject = object
    constructor Init;
    procedure Free;
    destructor Done; virtual;
  end;

   { TOldStreamRec }

  PStreamRec = ^TStreamRec;
  TStreamRec = record
    ObjType: Word;
    VmtLink: Pointer;
    Load: Pointer;
    Store: Pointer;
    Next: PStreamRec;
  end;

  POldStream = ^TOldStream;

  TOldStream = class(TStream)
  public
    Status: Integer;
    ErrorInfo: Integer;

    FileName: AnsiString;

    constructor Create;
//    destructor Done;

    function Get: POldObject;
    procedure Put(P: POldObject);

    function GetPC(AObjNumber: Word = 0): POldObject;
    procedure PutPC(P: POldObject; AObjType: Word = 0);

    function GetPos: Int64; virtual;
//    procedure Seek(Pos: Int64); overload;

    function ReadString: string;
    function ReadStr: PString;
    function StrRead: PChar;
    procedure StrWrite(P: PChar);
    procedure WriteStr(P: PString);
    procedure WriteString(const S: string);

    procedure Error(Code, Info: Integer); virtual;
  end;

{ TOldHandleStream class }

  TOldHandleStream = class(TOldStream)
  protected
    FHandle: Integer;
    procedure SetSize(NewSize: Longint); override;
    procedure SetSize(const NewSize: Int64); override;
  public
    constructor Create(AHandle: Integer);
    function Read(var Buffer; Count: Longint): Longint; override;
    function Write(const Buffer; Count: Longint): Longint; override;
    function Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;
    property Handle: Integer read FHandle;
  end;

{ TOldFileStream class }
  TOldFileStream = class(TOldHandleStream)
  public
    constructor Create(const FileName: string; Mode: Word); overload;
    constructor Create(const FileName: string; Mode: Word; Rights: Cardinal); overload;
    destructor Destroy; override;
  end;

{ TCustomMemoryStream abstract class }

  TOldCustomMemoryStream = class(TOldStream)
  private
    FMemory: Pointer;
    FSize, FPosition: Longint;
  protected
    procedure SetPointer(Ptr: Pointer; Size: Longint);
  public
    function Read(var Buffer; Count: Longint): Longint; override;
    function Seek(Offset: Longint; Origin: Word): Longint; override;
    procedure SaveToStream(Stream: TStream);
    procedure SaveToFile(const FileName: string);
    property Memory: Pointer read FMemory;
  end;

{ TMemoryStream }

  TOldMemoryStream = class(TOldCustomMemoryStream)
  private
    FCapacity: Longint;
    procedure SetCapacity(NewCapacity: Longint);
  protected
    function Realloc(var NewCapacity: Longint): Pointer; virtual;
    property Capacity: Longint read FCapacity write SetCapacity;
  public
    destructor Destroy; override;
    procedure Clear;
    procedure LoadFromStream(Stream: TStream);
    procedure LoadFromFile(const FileName: string);
    procedure SetSize(NewSize: Longint); override;
    function Write(const Buffer; Count: Longint): Longint; override;
  end;

   { TCollection types }

  PItemList = ^TItemList;
  TItemList = array[0..MaxCollectionSize - 1] of Pointer;

   { TCollection object }

  PCollection = ^TCollection;
  TCollection = object(TOldObject)
    Items: PItemList;
    Count: Integer;
    Limit: Integer;
    Delta: SmallInt;

    constructor Init(ALimit, ADelta: Integer);
    constructor Load(S: TOldStream);
    destructor Done; virtual;

    function At(Index: Integer): Pointer;
    procedure AtDelete(Index: Integer);
    procedure AtFree(Index: Integer);
    procedure AtInsert(Index: Integer; Item: Pointer);
    procedure AtPut(Index: Integer; Item: Pointer);
    procedure Delete(Item: Pointer);
    procedure DeleteAll;
    procedure Error(Code, Info: Integer); virtual;
    function FirstThat(Test: Pointer): Pointer;
    procedure ForEach(Action: Pointer);
    procedure Free(Item: Pointer);
    procedure FreeAll;
    procedure FreeItem(Item: Pointer); virtual;
    function GetItem(S: TOldStream): Pointer; virtual;
    function IndexOf(Item: Pointer): Integer; virtual;
    procedure Insert(Item: Pointer); virtual;
    function LastThat(Test: Pointer): Pointer;
    procedure Pack;
    procedure PutItem(S: TOldStream; Item: Pointer); virtual;
    procedure SetLimit(ALimit: Integer); virtual;
    procedure Store(S: TOldStream);
  end;

   { TSortedCollection object }

  PSortedCollection = ^TSortedCollection;
  TSortedCollection = object(TCollection)
    Duplicates: Boolean;
    constructor Init(ALimit, ADelta: Integer);
    constructor Load(S: TOldStream);
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    function IndexOf(Item: Pointer): Integer; virtual;
    procedure Insert(Item: Pointer); virtual;
    function KeyOf(Item: Pointer): Pointer; virtual;
    function Search(Key: Pointer; var Index: Integer): Boolean; virtual;
    procedure Store(S: TOldStream);
  end;

   { TStringCollection object }

  PStringCollection = ^TStringCollection;
  TStringCollection = object(TSortedCollection)
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    procedure FreeItem(Item: Pointer); virtual;
    function GetItem(S: TOldStream): Pointer; virtual;
    procedure PutItem(S: TOldStream; Item: Pointer); virtual;
  end;

   { TStrCollection object }

  PStrCollection = ^TStrCollection;
  TStrCollection = object(TSortedCollection)
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    procedure FreeItem(Item: Pointer); virtual;
    function GetItem(S: TOldStream): Pointer; virtual;
    procedure PutItem(S: TOldStream; Item: Pointer); virtual;
  end;

   { Stream routines }

function NewOfs(const Ptr: Pointer): Pointer;

procedure RegisterType(var S: TStreamRec);

{ Abstract notification procedure }

procedure Abstract;

{ Objects registration procedure }

procedure RegisterObjects;

const

   { Stream error procedure }

  StreamError: Pointer = nil;

   { Stream registration records }

const
  RCollection: TStreamRec = (
    ObjType: 50;
    VmtLink: TypeOf(TCollection);
    Load: @TCollection.Load;
    Store: @TCollection.Store);

const
  RStringCollection: TStreamRec = (
    ObjType: 51;
    VmtLink: TypeOf(TStringCollection);
    Load: @TStringCollection.Load;
    Store: @TStringCollection.Store);

const
  RStrCollection: TStreamRec = (
    ObjType: 69;
    VmtLink: TypeOf(TStrCollection);
    Load: @TStrCollection.Load;
    Store: @TStrCollection.Store);

implementation

uses
  RTLConsts, Forms;

var
  WinGIS_RunState: TWinGIS_RunState = runNotDef; { WinGIS runing state }
   {-- Moskaliov Business Graphics BUILD#150 17.01.01}

{$DEFINE NewExeFormat}

procedure Abstract;
begin
   //  RunError(211);
end;

function NewOfs(const Ptr: Pointer): Pointer;
begin
  Result := Ptr;
end;

{ TObject }

constructor TOldObject.Init;
begin
end;

{ Shorthand procedure for a done/dispose }

procedure TOldObject.Free;
begin
  Dispose(POldObject(@Self), Done);
end;

destructor TOldObject.Done;
begin
end;

const
  StreamTypes: PStreamRec = nil;

var
  Last: Word;

function TypeToStreamRec(ObjType: Pointer): PStreamRec;
begin
  Result := StreamTypes;
  while (Result <> nil)
    and (Result^.VMTLink <> ObjType) do
    Result := Result^.Next;
end;

function NumberToStreamRec(ObjType: Word): PStreamRec;
begin
  Result := StreamTypes;
  while (Result <> nil)
    and (Result^.ObjType <> ObjType) do
    Result := Result^.Next;
end;

procedure RegisterError;
begin
   //  RunError(212);
end;

procedure RegisterType(var S: TStreamRec);
begin
  if NumberToStreamRec(S.ObjType) <> nil then
    RegisterError
  else
  begin
    S.Next := StreamTypes;
    StreamTypes := @S;
  end;
end;

constructor TOldStream.Create;
begin
  inherited Create;
//  SELF.FileName := FileName;
  SELF.Status := stOk;
//  SELF.Project := nil;
//  SELF.ParentWindow := nil;
end;

{destructor TOldStream.Done;
begin
  SELF.Free;
end;}

function TOldStream.GetPos: Int64;
begin
  RESULT := SELF.Position;
end;

{procedure TOldStream.Seek(Pos: Int64);
begin
  inherited Seek(Pos, soFromBeginning);
end;}

function TOldStream.Get: POldObject;
var
  Info: PStreamRec;
  ObjNumber: Word;
  Cons: Pointer;
  VMT: Pointer;
//  AStream: TOldStream;
begin
  Read(ObjNumber, SizeOf(ObjNumber));
  if Status <> stOK then
    Result := nil
  else
    if ObjNumber = 0 then
      Result := nil
    else
    begin
      Info := NumberToStreamRec(ObjNumber);
      if Info = nil then
      begin
{$IFDEF AXDLL} // <----------------- AXDLL
        Result := nil;
        exit;
{$ENDIF} // <----------------- AXDLL
        Error(stGetError, ObjNumber);
        Result := nil;
      end
      else
      begin
        Cons := Info^.Load;
        VMT := Info^.VMTLink;
//        AStream := SELF;

        asm
        MOV ECX,SELF
        MOV EDX,VMT
        XOR EAX,EAX
        CALL DWORD PTR Cons
        MOV @Result,EAX
        end;
      end;
    end;
  Last := ObjNumber;
end;

procedure TOldStream.Put(P: POldObject);
var
  Info: PStreamRec;
  Cons: Pointer;
begin
  if P = nil then
    Write(P, SizeOf(Word))
  else
  begin
    Info := TypeToStreamRec(TypeOf(P^));
    if Info = nil then
      Error(stPutError, 0)
    else
    begin
      Write(Info^.ObjType, SizeOf(Info^.ObjType));
      Cons := Info^.Store;
      asm
        MOV EDX,Self
        MOV EAX,P
        CALL DWORD PTR Cons
      end;
    end;
  end;
end;

function TOldStream.StrRead: PChar;
var
  L: Word;
  P: PChar;
begin
  Read(L, SizeOf(Word));
  if L = 0 then
    StrRead := nil
  else
  begin
    P := StrAlloc(L + 1);
    Read(P[0], L);
    P[L] := #0;
    StrRead := P;
  end;
end;

procedure TOldStream.StrWrite(P: PChar);
var
  L: Word;
begin
  if P = nil then
    L := 0
  else
    L := StrLen(P);
  Write(L, SizeOf(Word));
  if P <> nil then
    Write(P[0], L);
end;

function TOldStream.ReadStr: PString;
var
  L: Byte;
  P: array[0..255] of Char;
begin
  Read(L, 1);
  if L > 0 then
  begin
    Read(P, L);
    P[L] := #0;
    Result := NewStr(StrPas(P));
  end
  else
    ReadStr := nil;
end;

function TOldStream.ReadString: string;
begin
  Read(Result[0], 1);
  Read(Result[1], Byte(Result[0]));
end;

procedure TOldStream.WriteStr(P: PString);
var
  Str: string;
begin
  if P <> nil then
  begin
    Str := P^;
    Write(Str, Length(Str) + 1);
  end
  else
  begin
    Str := '';
    Write(Str, 1);
  end;
end;

procedure TOldStream.WriteString(const S: string);
begin
  Write(S, Length(S) + 1);
end;

{function TOldStream.ReadString: string;
begin
  Read(Result[0], 1);
  Read(Result[1], Byte(Result[0]));
end;}

function TOldStream.GetPC(AObjNumber: Word = 0): POldObject;
var
  Info: PStreamRec;
  ObjNumber: Word;
  Cons: Pointer;
  VMT: Pointer;
begin
  Read(ObjNumber, SizeOf(ObjNumber));
  if AObjNumber <> 0 then
    ObjNumber := AObjNumber;
  if Status <> stOK then
    Result := nil
  else
    if ObjNumber = 0 then
      Result := nil
    else
    begin
      Info := NumberToStreamRec(ObjNumber);
      if Info = nil then
      begin
        Error(stGetError, ObjNumber);
        Result := nil;
      end
      else
      begin
        Cons := Info^.Load;
        VMT := Info^.VMTLink;
        asm
        MOV ECX,Self
        MOV EDX,VMT
        XOR EAX,EAX
        CALL DWORD PTR Cons
        MOV @Result,EAX
        end;
      end;
    end;
  Last := ObjNumber;
end;

procedure TOldStream.PutPC(P: POldObject; AObjType: Word = 0);
var
  Info: PStreamRec;
  Cons: Pointer;
begin
  if P = nil then
    Write(P, SizeOf(Word))
  else
  begin
    Info := TypeToStreamRec(TypeOf(P^));
    if Info = nil then
      Error(stPutError, 0)
    else
    begin
      if AObjType = 0 then
        Write(Info^.ObjType, SizeOf(Info^.ObjType))
      else
        Write(AObjType, SizeOf(AObjType));
      Cons := Info^.Store;
      asm
        MOV EDX,Self
        MOV EAX,P
        CALL DWORD PTR Cons
      end;
    end;
  end;
end;

procedure TOldStream.Error(Code, Info: Integer);
type
  TErrorProc = procedure(S: TOldStream);
begin
  Status := Code;
  ErrorInfo := Info;
  if StreamError <> nil then
    TErrorProc(StreamError)(Self);
end;

{ THandleStream }

constructor TOldHandleStream.Create(AHandle: Integer);
begin
  inherited Create;
  FHandle := AHandle;
end;

function TOldHandleStream.Read(var Buffer; Count: Longint): Longint;
begin
  Result := FileRead(FHandle, Buffer, Count);
  if Result = -1 then
    Result := 0;
end;

function TOldHandleStream.Write(const Buffer; Count: Longint): Longint;
begin
  Result := FileWrite(FHandle, Buffer, Count);
  if Result = -1 then
    Result := 0;
end;

function TOldHandleStream.Seek(const Offset: Int64; Origin: TSeekOrigin): Int64;
begin
  Result := FileSeek(FHandle, Offset, Ord(Origin));
end;

procedure TOldHandleStream.SetSize(NewSize: Longint);
begin
  SetSize(Int64(NewSize));
end;

procedure TOldHandleStream.SetSize(const NewSize: Int64);
begin
  Seek(NewSize, soBeginning);
{$IFDEF MSWINDOWS}
  Win32Check(SetEndOfFile(FHandle));
{$ELSE}
  if ftruncate(FHandle, Position) = -1 then
    raise EStreamError(sStreamSetSize);
{$ENDIF}
end;

{ TFileStream }

constructor TOldFileStream.Create(const FileName: string; Mode: Word);
begin
{$IFDEF MSWINDOWS}
  Create(Filename, Mode, 0);
{$ELSE}
  Create(Filename, Mode, FileAccessRights);
{$ENDIF}
end;

constructor TOldFileStream.Create(const FileName: string; Mode: Word; Rights: Cardinal);
begin
  if Mode = fmCreate then
  begin
    inherited Create(FileCreate(FileName, Rights));
    if FHandle < 0 then
      raise EFCreateError.CreateResFmt(@SFCreateErrorEx, [ExpandFileName(FileName), SysErrorMessage(GetLastError)]);
  end
  else
  begin
    inherited Create(FileOpen(FileName, Mode));
    if FHandle < 0 then
      raise EFOpenError.CreateResFmt(@SFOpenErrorEx, [ExpandFileName(FileName), SysErrorMessage(GetLastError)]);
  end;
end;

destructor TOldFileStream.Destroy;
begin
  if FHandle >= 0 then
    FileClose(FHandle);
  inherited Destroy;
end;

{ TCustomMemoryStream }

procedure TOldCustomMemoryStream.SetPointer(Ptr: Pointer; Size: Longint);
begin
  FMemory := Ptr;
  FSize := Size;
end;

function TOldCustomMemoryStream.Read(var Buffer; Count: Longint): Longint;
begin
  if (FPosition >= 0) and (Count >= 0) then
  begin
    Result := FSize - FPosition;
    if Result > 0 then
    begin
      if Result > Count then
        Result := Count;
      Move(Pointer(Longint(FMemory) + FPosition)^, Buffer, Result);
      Inc(FPosition, Result);
      Exit;
    end;
  end;
  Result := 0;
end;

function TOldCustomMemoryStream.Seek(Offset: Longint; Origin: Word): Longint;
begin
  case Origin of
    soFromBeginning: FPosition := Offset;
    soFromCurrent: Inc(FPosition, Offset);
    soFromEnd: FPosition := FSize + Offset;
  end;
  Result := FPosition;
end;

procedure TOldCustomMemoryStream.SaveToStream(Stream: TStream);
begin
  if FSize <> 0 then
    Stream.WriteBuffer(FMemory^, FSize);
end;

procedure TOldCustomMemoryStream.SaveToFile(const FileName: string);
var
  Stream: TStream;
begin
  Stream := TFileStream.Create(FileName, fmCreate);
  try
    SaveToStream(Stream);
  finally
    Stream.Free;
  end;
end;

{ TMemoryStream }

const
  MemoryDelta = $2000; { Must be a power of 2 }

destructor TOldMemoryStream.Destroy;
begin
  Clear;
  inherited Destroy;
end;

procedure TOldMemoryStream.Clear;
begin
  SetCapacity(0);
  FSize := 0;
  FPosition := 0;
end;

procedure TOldMemoryStream.LoadFromStream(Stream: TStream);
var
  Count: Longint;
begin
  Stream.Position := 0;
  Count := Stream.Size;
  SetSize(Count);
  if Count <> 0 then
    Stream.ReadBuffer(FMemory^, Count);
end;

procedure TOldMemoryStream.LoadFromFile(const FileName: string);
var
  Stream: TStream;
begin
  Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
  try
    LoadFromStream(Stream);
  finally
    Stream.Free;
  end;
end;

procedure TOldMemoryStream.SetCapacity(NewCapacity: Longint);
begin
  SetPointer(Realloc(NewCapacity), FSize);
  FCapacity := NewCapacity;
end;

procedure TOldMemoryStream.SetSize(NewSize: Longint);
var
  OldPosition: Longint;
begin
  OldPosition := FPosition;
  SetCapacity(NewSize);
  FSize := NewSize;
  if OldPosition > NewSize then
    Seek(0, soFromEnd);
end;

function TOldMemoryStream.Realloc(var NewCapacity: Longint): Pointer;
begin
  if (NewCapacity > 0) and (NewCapacity <> FSize) then
    NewCapacity := (NewCapacity + (MemoryDelta - 1)) and not (MemoryDelta - 1);
  Result := Memory;
  if NewCapacity <> FCapacity then
  begin
    if NewCapacity = 0 then
    begin
{$IFDEF MSWINDOWS}
      GlobalFreePtr(Memory);
{$ELSE}
      FreeMem(Memory);
{$ENDIF}
      Result := nil;
    end
    else
    begin
{$IFDEF MSWINDOWS}
      if Capacity = 0 then
        Result := GlobalAllocPtr(HeapAllocFlags, NewCapacity)
      else
        Result := GlobalReallocPtr(Memory, NewCapacity, HeapAllocFlags);
{$ELSE}
      if Capacity = 0 then
        GetMem(Result, NewCapacity)
      else
        ReallocMem(Result, NewCapacity);
{$ENDIF}
      if Result = nil then
        raise EStreamError.CreateRes(@SMemoryStreamError);
    end;
  end;
end;

function TOldMemoryStream.Write(const Buffer; Count: Longint): Longint;
var
  Pos: Longint;
begin
  if (FPosition >= 0) and (Count >= 0) then
  begin
    Pos := FPosition + Count;
    if Pos > 0 then
    begin
      if Pos > FSize then
      begin
        if Pos > FCapacity then
          SetCapacity(Pos);
        FSize := Pos;
      end;
      System.Move(Buffer, Pointer(Longint(FMemory) + FPosition)^, Count);
      FPosition := Pos;
      Result := Count;
      Exit;
    end;
  end;
  Result := 0;
end;

{ TCollection }

constructor TCollection.Init(ALimit, ADelta: Integer);
begin
  TOldObject.Init;
  Items := nil;
  Count := 0;
  Limit := 0;
  Delta := ADelta;
  SetLimit(ALimit);
end;

constructor TCollection.Load(S: TOldStream);
var
  C, I: Integer;
  Cnt: SmallInt;
  OldData: array[0..1] of SmallInt;
  p: Pointer;
begin
  S.Read(Cnt, SizeOf(Cnt));
  if Cnt <> -1 then
  begin
    Count := Cnt;
    S.Read(OldData, SizeOf(SmallInt) * 2);
    Limit := OldData[0];
    Delta := OldData[1];
  end
  else
    S.Read(Count, SizeOf(Integer) * 2 + SizeOf(SmallInt));
  Items := nil;
  C := Count;
  I := Limit;
  Count := 0;
  Limit := 0;
  SetLimit(I);
  Count := C;
  for I := 0 to C - 1 do
  begin
    p := GetItem(S);
    if p <> nil then
    begin
      AtPut(I, p);
    end
    else
    begin
      S.Status := stError;
    end;
  end;
end;

destructor TCollection.Done;
begin
  FreeAll;
  SetLimit(0);
end;

function TCollection.At(Index: Integer): Pointer;
begin
  if Index >= Count then
    Error(coIndexError, 0)
  else
    Result := Items^[Index];
end;

procedure TCollection.AtDelete(Index: Integer);
begin
  if Index >= Count then
    Error(coIndexError, 0)
  else
  begin
    Move(Items^[Index + 1], Items^[Index], (Count - Index - 1) * SizeOf(Pointer));
    Dec(Count);
  end;
end;

procedure TCollection.AtFree(Index: Integer);
var
  Item: Pointer;
begin
  Item := At(Index);
  AtDelete(Index);
  FreeItem(Item);
end;

procedure TCollection.AtInsert(Index: Integer; Item: Pointer);
begin
  if Index > Count then
    Error(coIndexError, 0)
  else
  begin
    if Count >= Limit then
      SetLimit(Limit + Delta);
    Move(Items^[Index], Items^[Index + 1], (Count - Index) * SizeOf(Pointer));
    Items^[Index] := Item;
    Inc(Count);
  end;
end;

procedure TCollection.AtPut(Index: Integer; Item: Pointer);
begin
  if Index >= Count then
    Error(coIndexError, 0)
  else
    Items^[Index] := Item;
end;

procedure TCollection.Delete(Item: Pointer);
begin
  AtDelete(IndexOf(Item));
end;

procedure TCollection.DeleteAll;
begin
  Count := 0;
end;

procedure TCollection.Error(Code, Info: Integer);
begin
end;

function TCollection.FirstThat(Test: Pointer): Pointer;
var
  Cnt: Integer;
  Item: Pointer;
begin
  Result := nil;
  Cnt := 0;
  while (Cnt < Count) do
  begin
    Item := At(Cnt);
    asm
      MOV     EAX,Item
      PUSH    DWORD PTR [EBP]
      CALL    DWORD PTR Test
      ADD     ESP,4
      CMP AL,0
      JE @@1
      MOV EAX,Item
      MOV @Result,EAX
      MOV Cnt,$7FFE
    @@1: INC Cnt
    end;
  end;
end;

procedure TCollection.ForEach(Action: Pointer);
var
  Cnt: Integer;
  Item: Pointer;
begin
  for Cnt := 0 to Count - 1 do
  begin
    Item := At(Cnt);
    asm
      MOV     EAX,Item
      PUSH    DWORD PTR [EBP]
      CALL    DWORD PTR Action
      ADD     ESP,4
    end;
  end;
end;

procedure TCollection.Free(Item: Pointer);
begin
  Delete(Item);
  FreeItem(Item);
end;

procedure TCollection.FreeAll;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    FreeItem(At(I));
  Count := 0;
end;

procedure TCollection.FreeItem(Item: Pointer);
begin
  if Item <> nil then
    Dispose(POldObject(Item), Done);
end;

function TCollection.GetItem(S: TOldStream): Pointer;
begin
  Result := S.Get;
end;

function TCollection.IndexOf(Item: Pointer): Integer;
var
  Cnt: Integer;
begin
  for Cnt := 0 to Count - 1 do
    if Items^[Cnt] = Item then
    begin
      Result := Cnt;
      Exit;
    end;
  Result := -1;
end;

procedure TCollection.Insert(Item: Pointer);
begin
  AtInsert(Count, Item);
end;

function TCollection.LastThat(Test: Pointer): Pointer;
var
  Cnt: Integer;
  Item: Pointer;
begin
  Cnt := Count - 1;
  while (Cnt >= 0) do
  begin
    Item := At(Cnt);
    asm
      MOV EAX,Item
      PUSH DWORD PTR [EBP]
      CALL DWORD PTR Test
      ADD  ESP,4
      CMP AL,0
      JE @@1
      MOV Cnt,0
      MOV EAX,Item
      MOV @Result,EAX
    @@1: DEC Cnt
    end;
  end;
end;

procedure TCollection.Pack;
begin
end;

procedure TCollection.PutItem(S: TOldStream; Item: Pointer);
begin
  S.Put(Item);
end;

procedure TCollection.SetLimit(ALimit: Integer);
var
  AItems: PItemList;
begin
  if ALimit < Count then
    ALimit := Count;
  if ALimit > MaxCollectionSize then
    ALimit := MaxCollectionSize;
  if ALimit <> Limit then
  begin
    if ALimit = 0 then
      AItems := nil
    else
    begin
      GetMem(AItems, ALimit * SizeOf(Pointer));
      if (Count <> 0) and (Items <> nil) then
        Move(Items^, AItems^, Count * SizeOf(Pointer));
    end;
    if Limit <> 0 then
      FreeMem(Items, Limit * SizeOf(Pointer));
    Items := AItems;
    Limit := ALimit;
  end;
end;

procedure TCollection.Store(S: TOldStream);

  procedure DoPutItem(P: Pointer); far;
  begin
      {+++ Brovak Bug 541 build 168 - show progress bar while saving project}
    if ShowProgressPut = true then
      if (PercentNowPut - PercentOldPut) > PercentPut then
      begin;
// TODO
//        StatusBar.Progress := (95 * PercentNowPut) / TotalCountPut;
        PercentOldPut := PercentNowPut;
      end;
      {-- Brovak}
    PutItem(S, P);
  end;
var
  Cnt: SmallInt;
begin
  Cnt := -1;
  S.Write(Cnt, SizeOf(SmallInt));
  S.Write(Count, SizeOf(Integer) * 2 + SizeOf(SmallInt));
  ForEach(@DoPutItem);
end;

{ TSortedCollection }

constructor TSortedCollection.Init(ALimit, ADelta: Integer);
begin
  TCollection.Init(ALimit, ADelta);
  Duplicates := False;
end;

constructor TSortedCollection.Load(S: TOldStream);
begin
  TCollection.Load(S);
  S.Read(Duplicates, SizeOf(Boolean));
end;

function TSortedCollection.Compare(Key1, Key2: Pointer): Integer;
begin
  Abstract;
end;

function TSortedCollection.IndexOf(Item: Pointer): Integer;
var
  I: Integer;
begin
  IndexOf := -1;
  if Search(KeyOf(Item), I) then
  begin
    if Duplicates then
      while (I < Count) and (Item <> Items^[I]) do
        Inc(I);
    if I < Count then
      IndexOf := I;
  end;
end;

procedure TSortedCollection.Insert(Item: Pointer);
var
  I: Integer;
begin
  if not Search(KeyOf(Item), I) or Duplicates then
    AtInsert(I, Item);
end;

function TSortedCollection.KeyOf(Item: Pointer): Pointer;
begin
  KeyOf := Item;
end;

function TSortedCollection.Search(Key: Pointer; var Index: Integer): Boolean;
var
  L, H, I, C: Integer;
begin
  Search := False;
  L := 0;
  H := Count - 1;
  while L <= H do
  begin
    I := (L + H) shr 1;
    C := Compare(KeyOf(Items^[I]), Key);
    if C < 0 then
      L := I + 1
    else
    begin
      H := I - 1;
      if C = 0 then
      begin
        Search := True;
        if not Duplicates then
          L := I;
      end;
    end;
  end;
  Index := L;
end;

procedure TSortedCollection.Store(S: TOldStream);
begin
  TCollection.Store(S);
  S.Write(Duplicates, SizeOf(Boolean));
end;

{ TStringCollection }

function TStringCollection.Compare(Key1, Key2: Pointer): Integer;
begin
  Result := CompareText(PString(Key1)^, PString(Key2)^);
end;

procedure TStringCollection.FreeItem(Item: Pointer);
begin
  DisposeStr(Item);
end;

function TStringCollection.GetItem(S: TOldStream): Pointer;
begin
  GetItem := S.ReadStr;
end;

procedure TStringCollection.PutItem(S: TOldStream; Item: Pointer);
begin
  S.WriteStr(Item);
end;

{ TStrCollection }

function TStrCollection.Compare(Key1, Key2: Pointer): Integer;
begin
  Compare := StrComp(Key1, Key2);
end;

procedure TStrCollection.FreeItem(Item: Pointer);
begin
  StrDispose(Item);
end;

function TStrCollection.GetItem(S: TOldStream): Pointer;
begin
  GetItem := S.StrRead;
end;

procedure TStrCollection.PutItem(S: TOldStream; Item: Pointer);
begin
  S.StrWrite(Item);
end;

{++ Moskaliov Business Graphics BUILD#150 17.01.01}

function WinGIS_runing: Boolean; { WinGIS is runing ?                }
var
  strName: string;
  intLength: Integer;
begin
  Result := FALSE;
  if WinGIS_RunState = runNotDef then
    if Assigned(Application) then
    begin
      intLength := Length(Application.ExeName);
      if intLength >= 12 then
      begin
        strName := Copy(Application.ExeName, intLength - 11, 12);
        if strName = co_DelphiExeName then
          WinGIS_RunState := runComponent
        else
          WinGIS_RunState := runWinGIS;
      end;
    end;
  Result := WinGIS_RunState = runWinGis;
end;
{-- Moskaliov Business Graphics BUILD#150 17.01.01}

{ Objects registration procedure }

procedure RegisterObjects;
begin
  RegisterType(RCollection);
  RegisterType(RStringCollection);
  RegisterType(RStrCollection);
end;

end.

