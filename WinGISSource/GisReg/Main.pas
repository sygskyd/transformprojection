unit Main;

interface

uses AddDlg,InfoDlg,Windows,WinOSInfo;

const PROGISIDFILE = 'progis.id';
      MAXLICENSES = 1024;
      MAXWINGISVERSION = 20;
      MAXUINT = 4294967295;

function GetId(var Id: DWord): Boolean;
function GetHaspId: Integer;
function GetWingisLicense: Integer;
function GetAllLicenses(var Licenses: Array of Boolean): Integer;
function GetAllLicensesFromFile(var Licenses: Array of Boolean; IDFilename: PChar): Integer;
procedure AddLicense;
procedure ShowInfoDialog(s: PChar; Info: PChar);
function GetUserId: DWord;

implementation

uses u_hasp_hl,SysUtils,Classes,IniFiles;

function GetUserId: DWord;
var
Id: DWord;
begin
     GetId(Id);
     Result:=Id;
end;

function GetHaspId: Integer;
const
  VendorCode : AnsiString =
'cm9tNlk2RJRtSfZGQNcdJSDr7yv6LZrNAm+CYLTEgYHlXyViCbg8wpe7JFasx5AzRkQCWGtW8I1ZQCXJ' +
'mOepzemLaCargcu2ZcE6TdFeBE9CLOjIimKgEB/BPYNPiG6VSalZjrZORbWUC484oNVmmVHcT04uUSBh' +
'tLgsiS41xDWa+96P9IUSqAx6lfDk1+shDg1ctNybIthglRAAHrXhx9PkdU5kTWP3fdDw65PhRsepmN30' +
'lcb2j361Q4cYEVEfsA5TfSn8nU22WhBonEcnoAySFfvtc08iHwyk50dA90yPKHfo3kNQlgRFkVZFi1gI' +
'oTOtS+/t50yuifxtl7Nkcyqq9IRYcdqMwbaW+lIYg+Zg6IWk8CfaaOg7yqZHayHqm1WO+glJfDHH7vve' +
'dTippkiMheaX9Lf2jakWiPztqK24Kq4ps0V8yePmDzGO2EyI0jQN5V+nQcLDTRvVHEWwVzfmi+6p0nvP' +
'uKejm/kAiPXaq/9CIcaE+Br5H29UtHdkGkfHA/gPxvFHIeyFvxn++Jjq3YVfJD8f3znlxahp/em+BaA7' +
'GWwR7hY52WtChneDfPxfw85xWAXOKeU2xA7uhC0WddZ7e9yix8l/nxWu8pybKuntNKVSI1qmHDrJ0e+n' +
'eZZnDs7wUJttmqo1FP+QLJWL/laT94fN5YHjTBVl4YA42UchhzBD28aVlO7irRLVpUqCkLsgA6HdRAuB' +
'OZlJhqqfCY9W4Q==';

var
LoginHandle: hasp_handle_t;
Feature: hasp_feature_t;
SerialData: Array[0..8] of Char;
SerialStr: AnsiString;
Res: hasp_status_t;
begin
        Result:=0;

        LoginHandle:=0;
        Feature:= $FFFF4000;

        Res:=hasp_login(Feature,@VendorCode[1],LoginHandle);
        if Res = HASP_STATUS_OK then begin

                FillChar(SerialData,9,0);
                Res:=hasp_read(LoginHandle,HASP_FILEID_MAIN,0,8,SerialData);

                if Res = HASP_STATUS_OK then begin
                        SerialStr:=StrPas(SerialData);
                        if StrLen(SerialData) > 0 then begin
                                Result:=StrToInt('$' + SerialStr);
                        end;
                end;

                hasp_logout(LoginHandle);
        end;
end;

function GetId(var Id: DWord): Boolean;
const
RefKey     : Array[0..7] of Byte = ($50,$52,$4F,$47,$49,$53,$00,$00);
VerifyKey1 : Array[0..7] of Byte = ($76,$45,$78,$E9,$E1,$FD,$09,$84);
VerifyKey2 : Array[0..7] of Byte = ($2C,$F6,$d8,$b0,$58,$af,$3a,$e6);
ModuleAddress = 29810;
var
DiskId: DWord;
Dummy1: DWord;
Dummy2: DWord;
begin
    Id:=0;
    Result:=False;
    Id:=GetHaspId;
    if Id = 0 then begin
        GetVolumeInformation(PChar('C:\'),nil,0,@DiskId,Dummy1,Dummy2,nil,0);
        if DiskId > 0 then begin
                Id:=DiskId;
        end;
        Result:=False;
    end
    else begin
       Result:=True;
    end;
end;

function ExtractCode(s,v,d: String): DWord;
var
Code,Version,Userid,LastDate: DWord;
begin
     Result:=0;
     s:=StringReplace(s,'-','',[rfReplaceAll]);
     Code:=StrToInt64Def(s,0);
     if Code > 0 then begin
        Version:=StrToIntDef(v,0);
        if Version > 0 then begin
           Version:=Version-9546;
           Version:=Version div 21;
           Code:=not Code;
           LastDate:=StrToIntDef(d,35065);
           if LastDate = 0 then LastDate:=35065;
           UserId:=Code - LastDate;
           UserId:=UserId - (Version * 1000000);
           Result:=UserId;
        end;
     end;
end;

function WriteToIni(AIni: TIniFile; const Section,Ident,Value: String): Boolean;
begin
        if AIni <> nil then begin
                try
                        Result:=True;
                        AIni.WriteString(Section,Ident,Value);
                except
                        Result:=False;
                end;
        end;
end;

function GetWingisLicense: Integer;
var
Version: DWord;
CodeDate: DWord;
Ini: TIniFile;
CurrentDate: DWord;
FirstDate: DWord;
LastDate: DWord;
Cid: DWord;
Cdc: DWord;
HardwareId: DWord;
SectionList: TStringList;
i: Integer;
Code: DWord;
CodeOk: Boolean;
IdStr: String;
HighestVersion: Integer;
begin
     {$IFDEF WGUL}
     Result:=14;
     exit;
     {$ENDIF}
     HighestVersion:=0;
     Ini:=TIniFile.Create(OSInfo.CommonAppDataDir + PROGISIDFILE);
     if Ini <> nil then begin
          GetId(HardwareId);
          SectionList:=TStringList.Create;
          Ini.ReadSections(SectionList);
          for i:=0 to SectionList.Count-1 do begin
               CodeOK:=True;
               Code:=StrToInt64(SectionList.Strings[i]);
               Code:=not Code;
               if (Code < HardwareId) then begin
                  Code:=Code+MAXUINT;
               end;
               Code:=Code-HardwareId;
               CodeDate:=Code mod 1000000;
               Version:=Trunc(Code/1000000);

               Cid:=Ini.ReadInteger(SectionList.Strings[i],'Cid',0);
               CurrentDate:=Ini.ReadInteger(SectionList.Strings[i],'Current',0);
               FirstDate:=Ini.ReadInteger(SectionList.Strings[i],'First',0);
               LastDate:=Ini.ReadInteger(SectionList.Strings[i],'Last',0);
               IdStr:=Ini.ReadString(SectionList.Strings[i],'Id','');

               if (CodeDate < 35065) or (CodeDate > 73415) then CodeOK:=False;
               if (Version > DWord(MAXWINGISVERSION)) then CodeOK:=False;

               if IdStr <> '' then begin
                 if ExtractCode(SectionList[i],IdStr,IntToStr(LastDate)) = 1 then CodeOk:=True;
               end;

               if CodeOk then begin
                    if CodeDate <> 35065 then begin
                         Cdc:=Ini.ReadInteger(SectionList.Strings[i],'Cdc',0);
                         if (Trunc(Date) >= CurrentDate) and (Cdc = CurrentDate or 9546) then begin
                                WriteToIni(Ini,SectionList.Strings[i], 'Current',IntToStr(Trunc(Date)));
                                WriteToIni(Ini,SectionList.Strings[i],'Cdc',IntToStr(Trunc(Date) or 9546));
                                if Cid = (HardwareId and (FirstDate*LastDate)) then begin
                                   if FirstDate = LastDate then begin
                                        if Version > HighestVersion then begin
                                                HighestVersion:=Version;
                                                WingisDate:=LastDate;
                                        end;
                                   end
                                   else if Trunc(Date) <= LastDate then begin
                                        if Version > HighestVersion then begin
                                                HighestVersion:=Version;
                                                WingisDate:=LastDate;
                                        end;
                                   end;
                              end;
                         end;
                    end
                    else begin
                        if Version > HighestVersion then begin
                                HighestVersion:=Version;
                                WingisDate:=0;
                        end;
                    end;
               end;
          end;
          SectionList.Free;
          Ini.Free;
     end;
     Result:=HighestVersion;
end;

function GetAllLicenses(var Licenses: Array of Boolean): Integer;
var
Version: DWord;
CodeDate: DWord;
Ini: TIniFile;
CurrentDate: DWord;
FirstDate: DWord;
LastDate: DWord;
Cid: DWord;
Cdc: DWord;
HardwareId: DWord;
SectionList: TStringList;
i: Integer;
Code: DWord;
CodeOk: Boolean;
IdStr: String;
begin
     {$IFDEF DPUL}
     for i:=0 to MAXLICENSES-1 do begin
        Licenses[i]:=True;
     end;
     Result:=19;
     exit;
     {$ELSE}
     for i:=0 to MAXLICENSES-1 do begin
        Licenses[i]:=False;
     end;
     {$ENDIF}

     {$IFDEF WGUL}
     for i:=21 to MAXLICENSES-1 do begin
        Licenses[i]:=True;
     end;
     Result:=14;
     exit;
     {$ENDIF}
     Ini:=TIniFile.Create(OSInfo.CommonAppDataDir + PROGISIDFILE);
     if Ini <> nil then begin
          GetId(HardwareId);
          SectionList:=TStringList.Create;
          Ini.ReadSections(SectionList);
          for i:=0 to SectionList.Count-1 do begin
               CodeOK:=True;
               Code:=StrToInt64(SectionList.Strings[i]);
               Code:=not Code;
               if (Code < HardwareId) then begin
                  Code:=Code+MAXUINT;
               end;
               Code:=Code-HardwareId;
               CodeDate:=Code mod 1000000;
               Version:=Trunc(Code/1000000);

               Cid:=Ini.ReadInteger(SectionList.Strings[i],'Cid',0);
               CurrentDate:=Ini.ReadInteger(SectionList.Strings[i],'Current',0);
               FirstDate:=Ini.ReadInteger(SectionList.Strings[i],'First',0);
               LastDate:=Ini.ReadInteger(SectionList.Strings[i],'Last',0);
               IdStr:=Ini.ReadString(SectionList.Strings[i],'Id','');

               if (CodeDate < 35065) or (CodeDate > 73415) then CodeOK:=False;
               if (Version > DWord(MAXLICENSES-1)) then CodeOK:=False;

               if IdStr <> '' then begin
                 if ExtractCode(SectionList[i],IdStr,IntToStr(LastDate)) = 1 then CodeOk:=True;
               end;

               if CodeOk then begin
                    if CodeDate <> 35065 then begin
                         Cdc:=Ini.ReadInteger(SectionList.Strings[i],'Cdc',0);
                         if (Trunc(Date) >= CurrentDate) and (Cdc = CurrentDate or 9546) then begin
                                WriteToIni(Ini,SectionList.Strings[i], 'Current',IntToStr(Trunc(Date)));
                                WriteToIni(Ini,SectionList.Strings[i],'Cdc',IntToStr(Trunc(Date) or 9546));
                                if Cid = (HardwareId and (FirstDate*LastDate)) then begin
                                   if FirstDate = LastDate then Licenses[Version]:=True
                                   else if Trunc(Date) <= LastDate then Licenses[Version]:=True;
                                end;
                         end;
                    end
                    else Licenses[Version]:=True;
               end;
          end;
          SectionList.Free;
          Ini.Free;
     end;
     Result:=0;
     for i:=0 to MAXWINGISVERSION-1 do begin
          if Licenses[i] = True then Result:=i;
     end;
end;

function GetAllLicensesFromFile(var Licenses: Array of Boolean; IDFilename: PChar): Integer;
var
Version: DWord;
CodeDate: DWord;
Ini: TIniFile;
CurrentDate: DWord;
FirstDate: DWord;
LastDate: DWord;
Cid: DWord;
Cdc: DWord;
HardwareId: DWord;
SectionList: TStringList;
i: Integer;
Code: DWord;
CodeOk: Boolean;
IdStr: String;
begin
     {$IFDEF DPUL}
     for i:=0 to MAXLICENSES-1 do begin
        Licenses[i]:=True;
     end;
     Result:=19;
     exit;
     {$ELSE}
     for i:=0 to MAXLICENSES-1 do begin
        Licenses[i]:=False;
     end;
     {$ENDIF}

     {$IFDEF WGUL}
     for i:=21 to MAXLICENSES-1 do begin
        Licenses[i]:=True;
     end;
     Result:=14;
     exit;
     {$ENDIF}
     Ini:=TIniFile.Create(StrPas(IDFilename));
     if Ini <> nil then begin
          GetId(HardwareId);
          SectionList:=TStringList.Create;
          Ini.ReadSections(SectionList);
          for i:=0 to SectionList.Count-1 do begin
               CodeOK:=True;
               Code:=StrToInt64(SectionList.Strings[i]);
               Code:=not Code;
               if (Code < HardwareId) then begin
                  Code:=Code+MAXUINT;
               end;
               Code:=Code-HardwareId;
               CodeDate:=Code mod 1000000;
               Version:=Trunc(Code/1000000);

               Cid:=Ini.ReadInteger(SectionList.Strings[i],'Cid',0);
               CurrentDate:=Ini.ReadInteger(SectionList.Strings[i],'Current',0);
               FirstDate:=Ini.ReadInteger(SectionList.Strings[i],'First',0);
               LastDate:=Ini.ReadInteger(SectionList.Strings[i],'Last',0);
               IdStr:=Ini.ReadString(SectionList.Strings[i],'Id','');

               if (CodeDate < 35065) or (CodeDate > 73415) then CodeOK:=False;
               if (Version > DWord(MAXLICENSES-1)) then CodeOK:=False;

               if IdStr <> '' then begin
                 if ExtractCode(SectionList[i],IdStr,IntToStr(LastDate)) = 1 then CodeOk:=True;
               end;

               if CodeOk then begin
                    if CodeDate <> 35065 then begin
                         Cdc:=Ini.ReadInteger(SectionList.Strings[i],'Cdc',0);
                         if (Trunc(Date) >= CurrentDate) and (Cdc = CurrentDate or 9546) then begin
                                WriteToIni(Ini,SectionList.Strings[i], 'Current',IntToStr(Trunc(Date)));
                                WriteToIni(Ini,SectionList.Strings[i],'Cdc',IntToStr(Trunc(Date) or 9546));
                                if Cid = (HardwareId and (FirstDate*LastDate)) then begin
                                        if FirstDate = LastDate then Licenses[Version]:=True
                                        else if Trunc(Date) <= LastDate then Licenses[Version]:=True;
                                end;
                         end;
                    end
                    else begin
                        Licenses[Version]:=True;
                    end;
               end;
          end;
          SectionList.Free;
          Ini.Free;
     end;
     Result:=0;
     for i:=0 to MAXWINGISVERSION-1 do begin
          if Licenses[i] = True then Result:=i;
     end;
end;

procedure AddLicense;
begin
     AddDialog:=TAddDialog.Create(nil);
     try
        AddDialog.ShowModal;
     finally
        AddDialog.Free;
     end;
end;

procedure ShowInfoDialog(s: PChar; Info: PChar);
var
t: AnsiString;
i: Integer;
begin
     InfoDialog:=TInfoDialog.Create(nil);
     t:=AnsiString(s);
     InfoDialog.VersionText:='';
     for i:=1 to Length(t) do begin
          if (t[i] >='0') and (t[i] <= '9') or (t[i] = '.') then
               InfoDialog.VersionText:=InfoDialog.VersionText+t[i];
     end;
     InfoDialog.InfoText:=Info;
     InfoDialog.ShowModal;
     InfoDialog.Free;
end;

end.
