unit InfoDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ShellApi, jpeg;

type
  TInfoDialog = class(TForm)
    ButtonOk: TButton;
    LabelHomepage: TLabel;
    LabelProduct: TLabel;
    LabelCopyright: TLabel;
    MemoAddress: TMemo;
    LabelVersion: TLabel;
    LabelID: TLabel;
    LabelValidUntil: TLabel;
    LabelEmailWingis: TLabel;
    LabelInfoText: TLabel;
    ImageProgis: TImage;
    Shape: TShape;
    ShapeTitle: TShape;
    procedure FormCreate(Sender: TObject);
    procedure ButtonOkClick(Sender: TObject);
    procedure LabelHomepageClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ImageLogoClick(Sender: TObject);
    procedure LabelEmailWingisClick(Sender: TObject);
    procedure LabelEmailSupportClick(Sender: TObject);
    procedure LabelProductMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
  public
    VersionText: String;
    InfoText: String;
  end;

var
  InfoDialog: TInfoDialog;
  WingisDate: Integer = 0;

implementation

uses Main,IniFiles;

{$R *.DFM}

procedure TInfoDialog.FormCreate(Sender: TObject);

begin
     Self.Scaleby(Trunc(9600000/(Self.PixelsPerInch)),100000);
end;

procedure TInfoDialog.FormShow(Sender: TObject);
var
IsHardlock: Boolean;
HardwareId: DWord;
IdLabel: String;
begin
     IsHardlock:=GetId(HardwareId);
     if IsHardlock then IdLabel:='Hardlock-ID: '
     else  IdLabel:='Hardware-ID: ';
     LabelVersion.Caption:='Version: ' + VersionText;
     LabelId.Caption:=IdLabel + IntToStr(HardwareId);
     if WingisDate > 0 then begin
        LabelValidUntil.Caption:='License expires: ' + DateToStr(WingisDate);
     end
     else begin
        LabelValidUntil.Caption:='';
     end;
     LabelInfoText.Caption:=InfoText;
end;

procedure TInfoDialog.ButtonOkClick(Sender: TObject);
begin
     Close;
end;

procedure TInfoDialog.LabelHomepageClick(Sender: TObject);
begin
     ShellExecute(0,'open','http://www.progis.com','','',SW_SHOW);
end;

procedure TInfoDialog.ImageLogoClick(Sender: TObject);
begin
        LabelHomepageClick(nil);
end;

procedure TInfoDialog.LabelEmailWingisClick(Sender: TObject);
begin
     ShellExecute(0,'open','mailto:wingis@progis.com','','',SW_SHOW);
end;

procedure TInfoDialog.LabelEmailSupportClick(Sender: TObject);
begin
     ShellExecute(0,'open','mailto:support@progis.com','','',SW_SHOW);
end;

procedure TInfoDialog.LabelProductMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
        Close;
end;

end.
