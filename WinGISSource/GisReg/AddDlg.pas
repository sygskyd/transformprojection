unit AddDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, ComCtrls, ShellApi;

type
  TAddDialog = class(TForm)
    GroupBoxID: TGroupBox;
    LabelId: TLabel;
    LabelCode: TLabel;
    EditCode: TEdit;
    MemoId: TMemo;
    GroupBoxButtons: TGroupBox;
    ButtonActivate: TButton;
    ButtonClose: TButton;
    LabelMailTo: TLabel;
    LabelMail: TLabel;
    procedure ButtonCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ButtonActivateClick(Sender: TObject);
    procedure EditCodeKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EditCodeKeyPress(Sender: TObject; var Key: Char);
    procedure EditCodeChange(Sender: TObject);
    procedure LabelMailClick(Sender: TObject);
  private
    HardwareId: DWord;
    DummyStrings: TStringList;
  end;

var
  AddDialog: TAddDialog;

implementation

uses Main, IniFiles, WinOSInfo;

{$R *.DFM}

procedure TAddDialog.ButtonCloseClick(Sender: TObject);
begin
     Close;
end;

procedure TAddDialog.FormCreate(Sender: TObject);
var
Ts: String;
i: Integer;
begin
     Self.Scaleby(Trunc(9600000/(Self.PixelsPerInch)),100000);

     GetId(HardwareId);
     Ts:=IntToStr(HardwareId);
     while Length(Ts) < 12 do Ts:='0' + Ts;
     MemoId.Lines[0]:='';
     for i:=1 to 12 do begin
          MemoId.Lines[0]:=MemoId.Lines[0]+Ts[i];
          if (i = 4) or (i = 8) then MemoId.Lines[0]:=MemoId.Lines[0] + '-';
     end;

     if not FileExists(OSInfo.CommonAppDataDir + PROGISIDFILE) then begin
          DummyStrings:=TStringList.Create;
          DummyStrings.SaveToFile(OSInfo.CommonAppDataDir + PROGISIDFILE);
          DummyStrings.Free;
     end;
end;

procedure TAddDialog.ButtonActivateClick(Sender: TObject);
var
VersionId: DWord;
Code: DWord;
CodeOK: Boolean;
Ini: TIniFile;
CurrentDate: DWord;
FirstDate: DWord;
LastDate: DWord;
HardwareId: DWord;
Ts: String;
SectionList: TStringList;
ExistingCode: DWord;
ExistingVersion: DWord;
i: Integer;
begin
     CodeOK:=True;
     Code:=0;
     GetId(HardwareId);
     Ts:=StringReplace(EditCode.Text,'-','',[rfReplaceAll]);
     try
          Code:=StrToInt64(Ts);
     except
          on Exception do CodeOK:=False;
     end;
     Code:=not Code;
     Code:=Code-HardwareId;
     LastDate:=Code mod 1000000;
     VersionId:=Trunc(Code/1000000);

     if (LastDate < 35065) or (LastDate > 73415) then CodeOK:=False;
     if (VersionId > 1024) then CodeOK:=False;

     if not CodeOK then begin
          Ts:='';
          MessageBox(Handle,PChar('The activation code "' + EditCode.Text + '" is invalid.'),PChar(Caption),MB_ICONERROR);
     end
     else begin
          Ini:=TIniFile.Create(OSInfo.CommonAppDataDir + PROGISIDFILE);
          SectionList:=TStringList.Create;
          Ini.ReadSections(SectionList);
          for i:=0 to SectionList.Count-1 do begin
               ExistingCode:=StrToInt64(SectionList.Strings[i]);
               ExistingCode:=not ExistingCode;
               ExistingCode:=ExistingCode-HardwareId;
               ExistingVersion:=Trunc(ExistingCode/1000000);
               if (ExistingVersion = VersionId) then begin
                    Ini.EraseSection(SectionList.Strings[i]);
               end;
          end;
          SectionList.Free;
          Ini.Free;

          Ini:=TIniFile.Create(OSInfo.CommonAppDataDir + PROGISIDFILE);
          if Ini <> nil then begin
               if not Ini.SectionExists(Ts) then begin
                    Ini.WriteInteger(Ts,'Id',(VersionId*21)+9546);
                    if LastDate <> 35065 then begin
                         CurrentDate:=Trunc(Date);
                         FirstDate:=CurrentDate;
                         with Ini do begin
                              WriteInteger(Ts,'First',FirstDate);
                              WriteInteger(Ts,'Current',CurrentDate);
                              WriteInteger(Ts,'Last',LastDate);
                              WriteInteger(Ts,'Cdc',CurrentDate or 9546);
                              WriteInteger(Ts,'Cid',HardwareId and (FirstDate*LastDate));
                         end;
                    end
                    else begin
                          with Ini do begin
                              WriteInteger(Ts,'Dummy',1);
                              DeleteKey(Ts,'Dummy');
                          end;
                   end;
               end;
               Ini.Free;
               Close;
          end;
     end;
end;

procedure TAddDialog.EditCodeKeyUp(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
     If EditCode.Text <> '' then ButtonActivate.Enabled:=True
     else ButtonActivate.Enabled:=False;
end;

procedure TAddDialog.EditCodeKeyPress(Sender: TObject; var Key: Char);
begin
     if (Key = Char(13)) and (ButtonActivate.Enabled) then ButtonActivateClick(nil);
end;

procedure TAddDialog.EditCodeChange(Sender: TObject);
begin
     ButtonActivate.Enabled := (Trim(EditCode.Text) <> '');
end;

procedure TAddDialog.LabelMailClick(Sender: TObject);
var
b,s: AnsiString;
begin
     b:=b + 'Hardware-ID:%20' + MemoId.Text + '%0A%0D%0A%0D';
     b:=b + 'Select%20Version:%0A%0D%0A%0D[%20]%20WinGIS%202010%20Standard%0A%0D[%20]%20WinGIS%202010%20Professional%0A%0D%0A%0D';
     s:='mailto:wingis@progis.com?subject=WinGIS%20Activation&body=' + b;
     ShellExecute(0,'open',PChar(s),'','',SW_SHOW);
end;

end.
