library GisReg;

uses
  Main in 'Main.pas',
  InfoDlg in 'InfoDlg.pas' {InfoDialog};

{$R *.RES}

exports   GetWingisLicense,
          GetAllLicenses,
          GetAllLicensesFromFile,
          AddLicense,
          ShowInfoDialog,
          GetUserId;
begin
end.

