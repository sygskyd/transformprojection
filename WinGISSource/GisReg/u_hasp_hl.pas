
(******************************************************************************
*
* HASP HL API for Borland Delphi
*
* Copyright (c) Aladdin Knowledge Systems Ltd.
*
*******************************************************************************)

unit u_hasp_hl;

interface

{------------------------------------------------------------------------------
  basic types (for platform compatibility)
 ------------------------------------------------------------------------------}

type
  hasp_u64_t = int64;
  hasp_s64_t = int64;

  hasp_u32_t = longword;
  hasp_s32_t = longint;
  hasp_u16_t = word;
  hasp_s16_t = smallint;
  hasp_u8_t  = byte;
  hasp_s8_t  = shortint;

{-----------------------------------------------------------------------------
  hasp_general General declarations
 -----------------------------------------------------------------------------}

type
  hasp_status_t  = hasp_u32_t;  // raw error code
  hasp_size_t    = hasp_u32_t;  // length
  hasp_handle_t  = hasp_u32_t;  // connection handle
  hasp_feature_t = hasp_u32_t;  // feature id
  hasp_fileid_t  = hasp_u32_t;  // memory file id
  hasp_time_t    = hasp_u64_t;  // time, seconds since Jan-01-1970 0:00 GMT

  hasp_vendor_code_t = pointer;  // contains the vendor code

{-----------------------------------------------------------------------------}

const
  { hasp_get_info() / hasp_get_sessioninfo() format to get update info (CTV) }
  HASP_UPDATEINFO = '<haspformat format="updateinfo"/>';

  { hasp_get_info() / hasp_get_sessioninfo() format to get session info }
  HASP_SESSIONINFO = '<haspformat format="sessioninfo"/>';

  { hasp_get_info() / hasp_get_sessioninfo() format to get key/hardware info }
  HASP_KEYINFO = '<haspformat format="keyinfo"/>';


{------------------------------------------------------------------------------
  Feature ID defines
 ------------------------------------------------------------------------------}

  // AND-mask used to identify feature type
  HASP_FEATURETYPE_MASK = $ffff0000;

  // After AND-ing with HASP_FEATURETYPE_MASK feature type contain this value.
  HASP_PROGNUM_FEATURETYPE = $ffff0000;

  // AND-mask used to extract program number from feature id if
  // program number feature.
  HASP_PROGNUM_MASK = $000000ff;

{------------------------------------------------------------------------------
  prognum options mask

  AND-mask used to identify prognum options:
      - HASP_PROGNUM_OPT_NO_LOCAL
      - HASP_PROGNUM_OPT_NO_REMOTE
      - HASP_PROGNUM_OPT_PROCESS
      - HASP_PROGNUM_OPT_CLASSIC
      - HASP_PROGNUM_OPT_TS

   3 bits of the mask are reserved for future extensions and currently unused.
   Initialize them to zero.
}

const
  HASP_PROGNUM_OPT_MASK = $0000ff00;

  { Disable local license search }
  HASP_PROGNUM_OPT_NO_LOCAL = $00008000;

  { Disable network license search }
  HASP_PROGNUM_OPT_NO_REMOTE = $00004000;

  { Sets session count of network licenses to per-process }
  HASP_PROGNUM_OPT_PROCESS = $00002000;

  { Enables the API to access "classic" (HASP4 or earlier) keys }
  HASP_PROGNUM_OPT_CLASSIC = $00001000;

  { Presence of Terminal Services gets ignored }
  HASP_PROGNUM_OPT_TS = $00000800;

  { Present in every hardware key. }
  HASP_DEFAULT_FID = 0;

  { Present in every hardware HASP key. }
  HASP_PROGNUM_DEFAULT_FID = (HASP_DEFAULT_FID or HASP_PROGNUM_FEATURETYPE);

{------------------------------------------------------------------------------
  hasp_file_ids Memory file id defines
 ------------------------------------------------------------------------------}

  { File id for HASP4 compatible memory contents w/o license data area }
  HASP_FILEID_MAIN = $fff0;

  {(Dummy) file id for HASP4 RTC chip memory contents }
  HASP_FILEID_TIME = $fff1;

  { (Dummy) file id for license data area of memory contents }
  HASP_FILEID_LICENSE = $fff2;


{------------------------------------------------------------------------------
  hasp_error_codes Error code defines
 ------------------------------------------------------------------------------}

const
  HASP_STATUS_OK             =  0; // no error occured
  HASP_MEM_RANGE             =  1; // invalid memory address
  HASP_INV_PROGNUM_OPT       =  2; // unknown/invalid feature id option
  HASP_INSUF_MEM             =  3; // memory allocation failed
  HASP_TMOF                  =  4; // too many open features
  HASP_ACCESS_DENIED         =  5; // feature access denied
  HASP_INCOMPAT_FEATURE      =  6; // incompatible feature
  HASP_CONTAINER_NOT_FOUND   =  7; // license container not found
  HASP_TOO_SHORT             =  8; // en-/decryption length too short
  HASP_INV_HND               =  9; // invalid handle
  HASP_INV_FILEID            = 10; // invalid file id / memory descriptor
  HASP_OLD_DRIVER            = 11; // driver or support daemon version too old
  HASP_NO_TIME               = 12; // real time support not available
  HASP_SYS_ERR               = 13; // generic error from host system call
  HASP_NO_DRIVER             = 14; // hardware key driver not found
  HASP_INV_FORMAT            = 15; // unrecognized info format
  HASP_REQ_NOT_SUPP          = 16; // request not supported
  HASP_INV_UPDATE_OBJ        = 17; // invalid update object
  HASP_KEYID_NOT_FOUND       = 18; // key with reuested id was not found
  HASP_INV_UPDATE_DATA       = 19; // update data consistency check failed
  HASP_INV_UPDATE_NOTSUPP    = 20; // update not supported by this key
  HASP_INV_UPDATE_CNTR       = 21; // update counter mismatch
  HASP_INV_VCODE             = 22; // invalid vendor code
  HASP_ENC_NOT_SUPP          = 23; // requested encryption algorithm not supported
  HASP_INV_TIME              = 24; // invalid date / time
  HASP_NO_BATTERY_POWER      = 25; // clock has no power
  HASP_NO_ACK_SPACE          = 26; // update requested ack., but no area to return it
  HASP_TS_DETECTED           = 27; // terminal services (remote terminal) detected
  HASP_FEATURE_TYPE_NOT_IMPL = 28; // feature type not implemented
  HASP_UNKNOWN_ALG           = 29; // unknown algorithm
  HASP_INV_SIG               = 30; // signature check failed
  HASP_FEATURE_NOT_FOUND     = 31; // feature not found

  // inside-api use
  HASP_NO_EXTBLOCK          = 600; // no classic memory extension block available

  //* catch-all */
  HASP_NOT_IMPL             = 698; // capability isn't available
  HASP_INT_ERR              = 699; // internal API error


{------------------------------------------------------------------------------
  The Basic API
 ------------------------------------------------------------------------------}

{-----------------------------------------------------------------------------
  Login into a feature.

  This function establishes a context (logs into a feature).
  hasp_login() also does the connection handling. There is one connection
  (secure channel) per API object. The last logout closes the channel.
  (see hasp_logout())
  When logging into a "classic" feature, the secure channel is not used.

  feature_id       - unique identifier of the feature,
                     With "classic" features (see \ref HASP_FEATURETYPE_MASK),
                     8 bits are reserved for legacy options (see \ref HASP_PROGNUM_OPT_MASK,
                     currently 5 bits are used):
                      - only local
                      - only remote
                      - login is counted per process ID
                      - disable terminal server check
                      - enable access to old (HASP3/HASP4) keys
  vendor_code      - pointer to the vendor blob
  handle           - pointer to the resulting session handle

  return           status code
                   - HASP_STATUS_OK         - the request completed successfully
                   - HASP_FEATURE_NOT_FOUND - the requested feature isn't available
                   - HASP_NOT_IMPL          - the type of feature isn't implemented
                   - HASP_INV_CLASSIC_OPT   - unknown classic option requested (HASP_PROGNUM_OPT_MASK)
                   - HASP_TMOF              - too many open handles
                   - HASP_INSUF_MEM         - out of memory

  For local prognum features, concurrency is not handled and each
  login performs a decrement if it is a counting license.

  Network prognum features just use the old HASPLM login logic with all
  drawbacks. There is only support for concurrent usage of one server
  (global server address).

}

function hasp_login( feature_id  : hasp_feature_t;
                     vendor_code : hasp_vendor_code_t;
                     var handle  : hasp_handle_t ) : hasp_status_t; stdcall;


{-----------------------------------------------------------------------------
  Logout.

  Logs out from a session and frees all allocated memory for the session. If it
  was the last session from this API, the connection to the LLM is shut down.

  handle       - session handle of session to log out from

  return     status code
               - HASP_STATUS_OK         - the request completed successfully
               - HASP_INV_HND           - invalid input handle

}

function hasp_logout(handle : hasp_handle_t) : hasp_status_t; stdcall;

{------------------------------------------------------------------------------
  Encrypt a buffer.

  This function encrypts a buffer.

  handle      - session handle
  buffer      - pointer to the buffer to be encrypted
  length      - size in bytes of the buffer to be encrypted (16 bytes minimum)

  return     status code
               - HASP_STATUS_OK         - the request completed successfully
               - HASP_INV_HND           - invalid input handle
               - HASP_NOT_IMPL          - the functionality for this feature type isn't implemented
               - HASP_TOO_SHORT         - the length of the data to be encrypted is too short
               - HASP_ENC_NOT_SUPP      - encryption type not supported by the hardware
               - HASP_FEATURE_NOT_FOUND - the requested feature isn't available anymore

  If the encryption fails (e.g. key removed in-between) the data pointed to
  by buffer is unmodified.
}

function hasp_encrypt( handle : hasp_handle_t;
                       var buffer;
                       length : hasp_size_t) : hasp_status_t; stdcall;


{------------------------------------------------------------------------------
  Decrypt a buffer.

  This function decrypts a buffer. This is the reverse operation of the
  hasp_encrypt() function. See hasp_encrypt() for more information.

  handle      - session handle
  buffer      - pointer to the buffer to be decrypted
  length      - size in bytes of the buffer to be decrypted (16 bytes minimum)

  return     status code
               - HASP_STATUS_OK         - the request completed successfully
               - HASP_INV_HND           - invalid input handle
               - HASP_NOT_IMPL          - the functionality for this feature type isn't implemented
               - HASP_TOO_SHORT         - the length of the data to be decrypted is too short
               - HASP_ENC_NOT_SUPP      - encryption type not supported by the hardware
               - HASP_FEATURE_NOT_FOUND - the requested feature isn't available anymore

 If the decryption fails (e.g. key removed in-between) the data
 pointed to by buffer is unmodified.
}

function hasp_decrypt ( handle : hasp_handle_t;
                        var buffer;
                        length : hasp_size_t)  : hasp_status_t; stdcall;


{------------------------------------------------------------------------------
  Read from key memory.

  This function is used to read from the key memory.

  handle       - session handle
  fileid       - id of the file to read (memory descriptor)
  offset       - file offset in the file
  length       - length
  buffer       - result of the read operation

  return     status code.
               - HASP_STATUS_OK         - the request completed successfully
               - HASP_INV_HND           - invalid input handle
               - HASP_NOT_IMPL          - the functionality for this feature type isn't implemented
               - HASP_INV_FILEID        - unknown fileid
               - HASP_MEM_RANGE         - attempt to read beyond eom
               - HASP_FEATURE_NOT_FOUND - the requested feature isn't available anymore

}

function hasp_read( handle : hasp_handle_t;
                    fileid : hasp_fileid_t;
                    offset : hasp_size_t;
                    length : hasp_size_t;
                    var buffer) : hasp_status_t;  stdcall;


{------------------------------------------------------------------------------
  Write to key memory.

  This function is used to write to the key memory. Depending on the provided
  session handle (either logged into the default feature or any other feature),
  write access to the license data memory (HASP_FILEID_LICENSE) is not permitted.

  handle       - session handle
  fileid       - id of the file to write
  offset       - file offset in the file
  length       - length
  buffer       - what to write

  return     status code.
               - HASP_STATUS_OK         - the request completed successfully
               - HASP_INV_HND           - invalid input handle
               - HASP_NOT_IMPL          - the functionality for this feature type isn't implemented
               - HASP_INV_FILEID        - unknown fileid
               - HASP_MEM_RANGE         - attempt to write beyond eom
               - HASP_FEATURE_NOT_FOUND - the requested feature isn't available anymore
}

function hasp_write ( handle : hasp_handle_t;
                      fileid : hasp_fileid_t;
                      offset : hasp_size_t;
                      length : hasp_size_t;
                      var buffer) : hasp_status_t;  stdcall;

{------------------------------------------------------------------------------
  Get memory size.

  This function is used to determine the memory size.

  handle       - session handle
  fileid       - id of the file to query
  size         - pointer to the resulting file size

  result     status code
               - HASP_STATUS_OK         - the request completed successfully
               - HASP_INV_HND           - invalid input handle
               - HASP_NOT_IMPL          - the functionality for this feature type isn't implemented
               - HASP_INV_FILEID        - unknown fileid
               - HASP_FEATURE_NOT_FOUND - the requested feature isn't available anymore
}

function hasp_get_size( handle : hasp_handle_t;
                        fileid : hasp_fileid_t;
                        var size : hasp_size_t) : hasp_status_t;  stdcall;

{------------------------------------------------------------------------------
  Read current time from a time key.

  This function reads the current time from a time key.
  The time will be returned in seconds since Jan-01-1970 0:00 GMT.

  remark:
    The general purpose of this function is not related to
    licensing, but to get reliable timestamps which are independent
    from the system clock.

  handle       - session handle
  time         - pointer to the actual time

  return     status code
               - HASP_STATUS_OK         - the request completed successfully
               - HASP_INV_HND           - invalid input handle
               - HASP_NOT_IMPL          - the functionality for this feature type isn't implemented
               - HASP_FEATURE_NOT_FOUND - the requested feature isn't available anymore
               - HASP_NO_TIME           - RTC not available
}

function hasp_get_rtc( handle : hasp_handle_t;
                       var time : hasp_time_t) : hasp_status_t;  stdcall;


{------------------------------------------------------------------------------
  Legacy HASP functionality for backward compatibility
 ------------------------------------------------------------------------------}

{------------------------------------------------------------------------------
  Legacy HASP4 compatible encryption function.

  handle       - session handle
  buffer       - pointer to the buffer to be encrypted
  length       - size in bytes of the buffer  (8 bytes minimum)

  return     status code
               - HASP_STATUS_OK         - the request completed successfully
               - HASP_INV_HND           - invalid input handle
               - HASP_TOO_SHORT         - the length of the data to be encrypted is too short
               - HASP_ENC_NOT_SUPP      - encryption type not supported by the hardware
               - HASP_INCOMPAT_FEATURE  - classic encryption is not available for this feature
               - HASP_FEATURE_NOT_FOUND - the requested feature isn't available anymore

  The handle must have been obtained by calling hasp_login() with
  a prognum feature id.

  If the encryption fails (e.g. key removed in-between) the data
  pointed to by buffer is undefined.
}

function hasp_legacy_encrypt(handle : hasp_handle_t;
                             var buffer;
                             length : hasp_size_t ) : hasp_status_t; stdcall;


{------------------------------------------------------------------------------
  Legacy HASP4 compatible decryption function.

  handle       - session handle
  buffer       - pointer to the buffer to be decrypted
  length       - size in bytes of the buffer (8 bytes minimum)

  return     status code
               - HASP_STATUS_OK         - the request completed successfully
               - HASP_INV_HND           - invalid input handle
               - HASP_TOO_SHORT         - the length of the data to be decrypted is too short
               - HASP_ENC_NOT_SUPP      - encryption type not supported by the hardware
               - HASP_INCOMPAT_FEATURE  - classic decryption is not available for this feature
               - HASP_FEATURE_NOT_FOUND - the requested feature isn't available anymore

  The handle must have been obtained by calling \ref hasp_login() with
  a prognum feature id.

  If the decryption fails (e.g. key removed in-between) the data
  pointed to by buffer is undefined.
}

function hasp_legacy_decrypt ( handle : hasp_handle_t;
                               var buffer;
                               length : hasp_size_t ) : hasp_status_t; stdcall;


{------------------------------------------------------------------------------
  Write to HASP4 compatible real time clock

  handle       - session handle
  new_time     - time value to be set

  return     status code
               - HASP_STATUS_OK         - the request completed successfully
               - HASP_INV_HND           - invalid input handle
               - HASP_INCOMPAT_FEATURE  - functionality is not available for this feature
               - HASP_FEATURE_NOT_FOUND - the requested feature isn't available anymore
               - HASP_NO_TIME           - RTC not available

  Note: The handle must have been obtained by calling hasp_login() with
        a prognum feature id.
}

function hasp_legacy_set_rtc( handle : hasp_handle_t;
                             new_time : hasp_time_t ) : hasp_status_t; stdcall;


{------------------------------------------------------------------------------
  Set the LM idle time.

  handle       - session handle
  idle_time    - the idle time in minutes

  return      status code
               - HASP_STATUS_OK         - the request completed successfully
               - HASP_INV_HND           - invalid input handle
               - HASP_INCOMPAT_FEATURE  - changing the idletime is not available for this feature
               - HASP_FEATURE_NOT_FOUND - the requested feature isn't available anymore
               - HASP_REQ_NOT_SUPP      - attempt to set the idle time for a local license

  Note: The handle must have been obtained by calling hasp_login() with
        a prognum feature id.
}

function hasp_legacy_set_idletime ( handle : hasp_handle_t;
                                    idle_time : hasp_u16_t ) : hasp_status_t; stdcall;


{------------------------------------------------------------------------------
  Extended HASP HL API
 ------------------------------------------------------------------------------

  The extended API consists of functions which provide extended functionality.
  This advanced functionality is sometimes necessary, and addresses the
  "advanced" user.

 ------------------------------------------------------------------------------
  Get information in a session context.

  Memory for the information is allocated by this function and has to be
  freed by the hasp_free() function.

  handle       - session handle
  format       - XML definition of the output data structure
  info         - pointer to the returned information (XML list)

  return     status code
               - HASP_STATUS_OK  - the request completed successfully
               - HASP_INV_HND    - handle not active
               - HASP_INV_FORMAT - unrecognized format
}

function hasp_get_sessioninfo( handle : hasp_handle_t;
                               format : pchar;
                               var info : pchar) : hasp_status_t; stdcall;


{------------------------------------------------------------------------------
  Free resources allocated by hasp_get_sessioninfo

  This function must be called to free the resources
  allocated by hasp_get_sessioninfo.

  info   -  pointer to the resources to be freed
}

procedure hasp_free (info : pointer); stdcall;


{------------------------------------------------------------------------------
  Write an update.

  This function writes update information. The update blob contains all
  necessary data to perform the update: Where to write (in which "container",
  e.g. dongle), the necessary access data (passwords) and of course the
  update itself. The function returns in an acknowledge blob, which is
  signed/encrypted by the updated instance and contains a proof that this
  update has been succeeded. Memory for the acknowledge blob is allocated by
  the API and has to be freed by the programmer (see hasp_free()).

  update_data      - pointer to the complete update data.
  ack_data         - pointer to a buffer to get the acknowledge data.

  return     status code
}

function hasp_update ( update_data  : pchar;
                       var ack_data : pchar ) : hasp_status_t; stdcall;


{------------------------------------------------------------------------------
  Utility functions
 ------------------------------------------------------------------------------}

{------------------------------------------------------------------------------
  Convert broken up time into a time type

  day          - input day
  month        - input month
  year         - input year
  hour         - input hour
  minute       - input minute
  second       - input second
  time         - pointer to put result

  return     status code
               - HASP_STATUS_OK  - the request completed successfully
               - HASP_INV_TIME   - time outside of the supported range
}

function hasp_datetime_to_hasptime ( day, month, year,
                                    hour, minute, second : cardinal;
                                    var time : hasp_time_t ) : hasp_status_t; stdcall;


{------------------------------------------------------------------------------
  Convert time type into broken up time

  time         - pointer to put result
  day          - pointer to day
  month        - pointer to month
  year         - pointer to year
  hour         - pointer to hour
  minute       - pointer to minute
  second       - pointer to second

  return     status code
               - HASP_STATUS_OK  - the request completed successfully
               - HASP_INV_TIME   - time outside of the supported range
}

function hasp_hasptime_to_datetime (
             time : hasp_time_t;
             var day, month, year,
                 hour, minute, second : cardinal) : hasp_status_t; stdcall;

{------------------------------------------------------------------------------
  Feature ID convention
 ------------------------------------------------------------------------------

  Feature ids are 32bits wide. If the upper 16 bit contain the value indicated
  by HASP_PROGNUM_FEATURETYPE, the feature defines a prognum feature.

  For prognum features there are some options encoded in the feature id.
  These include

  - HASP_PROGNUM_OPT_NO_LOCAL
    Don't search for a license locally. "Remote-only"

  - HASP_PROGNUM_OPT_NO_REMOTE
    Don't search for a license on the network. "Local-only"

  - HASP_PROGNUM_OPT_PROCESS
    In case, a license is found in the network, count license usage per
    process instead of per workstation.

  - HASP_PROGNUM_OPT_TS
    Don't detect whether the program is running on a remote screen
    on a terminal server.

  - HASP_PROGNUM_OPT_CLASSIC
    The API by default only searches for HASPHL keys. When this option
    is set, it also searches for HASP3/HASP4 keys.
}

{-----------------------------------------------------------------------------}
  implementation
{-----------------------------------------------------------------------------}

const
  DLLName = 'hasp_windows.dll';

function hasp_login;                stdcall; external DLLName;
function hasp_logout;               stdcall; external DLLName;
function hasp_encrypt;              stdcall; external DLLName;
function hasp_decrypt;              stdcall; external DLLName;
function hasp_read;                 stdcall; external DLLName;
function hasp_write;                stdcall; external DLLName;
function hasp_get_size;             stdcall; external DLLName;
function hasp_get_rtc;              stdcall; external DLLName;
function hasp_legacy_encrypt;       stdcall; external DLLName;
function hasp_legacy_decrypt;       stdcall; external DLLName;
function hasp_legacy_set_rtc;       stdcall; external DLLName;
function hasp_legacy_set_idletime;  stdcall; external DLLName;

function hasp_get_sessioninfo;      stdcall; external DLLName;
procedure hasp_free;                stdcall; external DLLName;
function hasp_update;               stdcall; external DLLName;

function hasp_datetime_to_hasptime; stdcall; external DLLName;
function hasp_hasptime_to_datetime; stdcall; external DLLName;

{-----------------------------------------------------------------------------}

initialization

{-----------------------------------------------------------------------------}

finalization

{-----------------------------------------------------------------------------}

end.


