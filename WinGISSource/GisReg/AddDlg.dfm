object AddDialog: TAddDialog
  Left = 493
  Top = 261
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'WinGIS Activation'
  ClientHeight = 194
  ClientWidth = 249
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBoxID: TGroupBox
    Left = 8
    Top = 5
    Width = 233
    Height = 124
    TabOrder = 0
    object LabelId: TLabel
      Left = 8
      Top = 16
      Width = 63
      Height = 13
      Caption = 'Hardware ID:'
    end
    object LabelCode: TLabel
      Left = 8
      Top = 80
      Width = 78
      Height = 13
      Caption = 'Activation Code:'
    end
    object LabelMailTo: TLabel
      Left = 8
      Top = 56
      Width = 11
      Height = 13
      Caption = #174
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Symbol'
      Font.Style = []
      ParentFont = False
    end
    object LabelMail: TLabel
      Left = 24
      Top = 56
      Width = 91
      Height = 13
      Cursor = crHandPoint
      Caption = 'wingis@progis.com'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentColor = False
      ParentFont = False
      OnClick = LabelMailClick
    end
    object EditCode: TEdit
      Left = 8
      Top = 96
      Width = 217
      Height = 21
      MaxLength = 15
      TabOrder = 0
      OnChange = EditCodeChange
      OnKeyPress = EditCodeKeyPress
      OnKeyUp = EditCodeKeyUp
    end
    object MemoId: TMemo
      Left = 8
      Top = 32
      Width = 217
      Height = 21
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
    end
  end
  object GroupBoxButtons: TGroupBox
    Left = 8
    Top = 136
    Width = 233
    Height = 49
    TabOrder = 1
    object ButtonActivate: TButton
      Left = 72
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Activate'
      Default = True
      Enabled = False
      TabOrder = 0
      TabStop = False
      OnClick = ButtonActivateClick
    end
    object ButtonClose: TButton
      Left = 152
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Close'
      TabOrder = 1
      TabStop = False
      OnClick = ButtonCloseClick
    end
  end
end
