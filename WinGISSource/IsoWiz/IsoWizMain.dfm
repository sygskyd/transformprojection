object IsoWizForm: TIsoWizForm
  Left = 634
  Top = 154
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'ISO Wizard'
  ClientHeight = 459
  ClientWidth = 389
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000000020000000000000000000000000000000000000000
    0000000080000080000000808000800000008000800080800000C0C0C0008080
    80000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000444444444000000000000000000000C4C444444444400000000000000
    0084C4C4C42444444444000000000000084CCCCC4642C4C4C424400000000000
    8CCCCCCC622224CC42224400000000086CCCCCCC6222224CC462444000000086
    CCCCCCCCC2222224CC4C244400000082CCCCCCCC2222222CCCC4C44400000822
    6CCCCCCC22222226CCCC4C444000082C6CCCCCCC222222226CCCC4C440008767
    CCCCC7662222222226CCCC44440086767CC772222222222222CCC4C444008767
    CC7222222222222226CCCC4C44008676FCF7F22222222222CC4CCCC42400876F
    FF7F22222222222CC2CCCC44240086FFFFC2F222222222CC224CCCC4C200867F
    FFFF222222222CC22244CCCC240086FFFF7CCF222CCC222224CCC4C242008F6F
    FF22CC7CCCCCCCC24C6222242400087FFF72264CC42C222222222242400008F7
    FFFFF22CCCCC2CCC4C22222420000087FF7F222222CC6CC2C62222420000008F
    67CFF222222222222222242400000008F6272C22222222222222224000000000
    8FFF2F62C2222222222224000000000008F7FFFC4C2222222222200000000000
    008FF66FCC222222222200000000000000088F72FC2226C22288000000000000
    0000088F66F6C66688000000000000000000000888888888000000000000FFFF
    FFFFFFE00FFFFF8003FFFE0000FFFC00007FF800003FF000001FE000000FC000
    0007C00000078000000380000003000000010000000100000001000000010000
    0001000000010000000100000001000000018000000380000003C0000007C000
    0007E000000FF000001FF800003FFC00007FFE0000FFFF8003FFFFE00FFF}
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBoxMisc: TGroupBox
    Left = 8
    Top = 408
    Width = 369
    Height = 41
    TabOrder = 1
    object ProgressBar: TProgressBar
      Left = 8
      Top = 14
      Width = 353
      Height = 16
      Min = 0
      Max = 100
      Smooth = True
      Step = 1
      TabOrder = 0
    end
  end
  object GroupBoxDatasource: TGroupBox
    Left = 8
    Top = 8
    Width = 369
    Height = 121
    Caption = 'Datasource'
    TabOrder = 0
    object LabelTables: TLabel
      Left = 8
      Top = 72
      Width = 30
      Height = 13
      Caption = 'Table:'
    end
    object LabelProgisIdField: TLabel
      Left = 128
      Top = 72
      Width = 68
      Height = 13
      Caption = 'ProgisID Field:'
    end
    object LabelISOField: TLabel
      Left = 248
      Top = 72
      Width = 46
      Height = 13
      Caption = 'ISO Field:'
    end
    object LabelDatabase: TLabel
      Left = 8
      Top = 24
      Width = 49
      Height = 13
      Caption = 'Database:'
    end
    object ButtonDatabase: TButton
      Left = 216
      Top = 40
      Width = 21
      Height = 21
      Caption = '...'
      TabOrder = 0
      OnClick = ButtonDatabaseClick
    end
    object ComboBoxTables: TComboBox
      Left = 8
      Top = 88
      Width = 113
      Height = 21
      Style = csDropDownList
      Enabled = False
      ItemHeight = 13
      TabOrder = 1
      TabStop = False
      OnChange = ComboBoxTablesClick
    end
    object ComboBoxID: TComboBox
      Left = 128
      Top = 88
      Width = 113
      Height = 21
      Style = csDropDownList
      Enabled = False
      ItemHeight = 13
      TabOrder = 2
      TabStop = False
      OnChange = ComboBoxIDChange
    end
    object ComboBoxISO: TComboBox
      Left = 248
      Top = 88
      Width = 113
      Height = 21
      Style = csDropDownList
      Enabled = False
      ItemHeight = 13
      TabOrder = 3
      TabStop = False
      OnChange = ComboBoxISOChange
    end
    object EditDatabase: TEdit
      Left = 8
      Top = 40
      Width = 209
      Height = 21
      ReadOnly = True
      TabOrder = 4
    end
  end
  object GroupBoxObjects: TGroupBox
    Left = 8
    Top = 248
    Width = 369
    Height = 145
    Caption = 'ISO Objects'
    TabOrder = 2
    object LabelStep: TLabel
      Left = 184
      Top = 24
      Width = 25
      Height = 13
      Caption = 'Step:'
    end
    object LabelStart: TLabel
      Left = 8
      Top = 24
      Width = 25
      Height = 13
      Caption = 'Start:'
    end
    object LabelStop: TLabel
      Left = 96
      Top = 24
      Width = 25
      Height = 13
      Caption = 'Stop:'
    end
    object LabelColors: TLabel
      Left = 8
      Top = 72
      Width = 32
      Height = 13
      Caption = 'Colors:'
    end
    object PanelColor: TPanel
      Left = 40
      Top = 88
      Width = 225
      Height = 22
      BevelOuter = bvLowered
      Color = clGray
      TabOrder = 0
      object PaintBox: TPaintBox
        Left = 1
        Top = 1
        Width = 223
        Height = 20
        Align = alClient
        OnMouseDown = PaintBoxMouseDown
        OnPaint = PaintBoxPaint
      end
    end
    object ButtonCreate: TButton
      Left = 280
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Create'
      Enabled = False
      TabOrder = 1
      TabStop = False
      OnClick = ButtonCreateClick
    end
    object ButtonReset: TButton
      Left = 280
      Top = 48
      Width = 75
      Height = 25
      Caption = 'Reset'
      Enabled = False
      TabOrder = 2
      TabStop = False
      OnClick = ButtonResetClick
    end
    object RadioButtonLines: TRadioButton
      Left = 72
      Top = 120
      Width = 57
      Height = 17
      Caption = 'Lines'
      Enabled = False
      TabOrder = 3
    end
    object RadioButtonAreas: TRadioButton
      Left = 8
      Top = 120
      Width = 65
      Height = 17
      Caption = 'Areas'
      Checked = True
      Enabled = False
      TabOrder = 4
      TabStop = True
    end
    object ButtonClose: TButton
      Left = 280
      Top = 112
      Width = 75
      Height = 25
      Caption = 'Close'
      TabOrder = 5
      TabStop = False
      OnClick = ButtonCloseClick
    end
    object ButtonProfile: TButton
      Left = 280
      Top = 80
      Width = 75
      Height = 25
      Caption = 'Profile...'
      Enabled = False
      TabOrder = 6
      TabStop = False
      OnClick = ButtonProfileClick
    end
    object EditStart: TEdit
      Left = 8
      Top = 40
      Width = 81
      Height = 21
      TabOrder = 7
      OnChange = EditStartStopStepChange
    end
    object EditStop: TEdit
      Left = 96
      Top = 40
      Width = 81
      Height = 21
      TabOrder = 8
      OnChange = EditStartStopStepChange
    end
    object EditStep: TEdit
      Left = 184
      Top = 40
      Width = 81
      Height = 21
      TabOrder = 9
      OnChange = EditStartStopStepChange
    end
    object SpinEditColors: TSpinEdit
      Left = 8
      Top = 88
      Width = 33
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 10
      Value = 0
      OnChange = SpinEditColorsChange
    end
    object CheckBoxOverlay: TCheckBox
      Left = 184
      Top = 120
      Width = 81
      Height = 17
      Caption = 'Overlay'
      TabOrder = 11
    end
  end
  object GroupBoxGrid: TGroupBox
    Left = 8
    Top = 136
    Width = 369
    Height = 105
    Caption = 'ISO Grid'
    TabOrder = 3
    object LabelModule: TLabel
      Left = 248
      Top = 24
      Width = 38
      Height = 13
      Caption = 'Module:'
    end
    object LabelRadius: TLabel
      Left = 8
      Top = 24
      Width = 53
      Height = 13
      Caption = 'Radius [m]:'
    end
    object LabelSpacing: TLabel
      Left = 128
      Top = 24
      Width = 59
      Height = 13
      Caption = 'Spacing [m]:'
    end
    object ComboBoxModule: TComboBox
      Left = 248
      Top = 40
      Width = 113
      Height = 21
      Style = csDropDownList
      Enabled = False
      ItemHeight = 13
      TabOrder = 0
      TabStop = False
      OnChange = ComboBoxModuleChange
    end
    object ButtonGrid: TButton
      Left = 8
      Top = 72
      Width = 121
      Height = 25
      Caption = 'Refresh Grid'
      Enabled = False
      TabOrder = 1
      TabStop = False
      OnClick = ButtonGridClick
    end
    object EditRadius: TEdit
      Left = 8
      Top = 40
      Width = 113
      Height = 21
      TabOrder = 2
      OnChange = EditRadiusSpacingChange
    end
    object EditSpacing: TEdit
      Left = 128
      Top = 40
      Width = 113
      Height = 21
      TabOrder = 3
      OnChange = EditRadiusSpacingChange
    end
  end
  object ADOConnection: TADOConnection
    CursorLocation = clUseServer
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 112
  end
  object ADOTable: TADOTable
    Connection = ADOConnection
    CursorLocation = clUseServer
    CursorType = ctStatic
    LockType = ltReadOnly
    TableDirect = True
    Left = 144
  end
  object OpenDialog: TOpenDialog
    Filter = 'Database Files (*.mdb;*.wgi)|*.mdb;*.wgi|All Files (*.*)|*.*'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 304
  end
  object IGrid_servEvents: TIGrid_servEvents
    OnLineReady = IGrid_servEventsLineReady
    Left = 176
  end
  object IGridCreate_servEvents: TIGridCreate_servEvents
    OnProgress = IGridCreate_servEventsProgress
    Left = 208
  end
  object ColorDialog: TColorDialog
    Ctl3D = True
    Color = clFuchsia
    Left = 240
  end
  object TimerSelection: TTimer
    Enabled = False
    Interval = 100
    OnTimer = TimerSelectionTimer
    Left = 336
  end
  object OpenDialogGrid: TOpenDialog
    Filter = 'Grid Files (*.grid)|*.grid|All Files (*.*)|*.*'
    Left = 272
  end
end
