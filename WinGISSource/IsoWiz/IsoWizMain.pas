unit IsoWizMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Wingis_TLB, StdCtrls, Mask, DB, ADODB, iso32_dll_TLB,
  iso32_dllEvents, ActiveX, Spin, ComCtrls, ExtCtrls, Types, TeEngine;

const
        MAXCOLORS = 8;
        WM_WGMousePos = 15010;

type
  TIsoWizForm = class(TForm)
    ADOConnection: TADOConnection;
    ADOTable: TADOTable;
    OpenDialog: TOpenDialog;
    IGrid_servEvents: TIGrid_servEvents;
    IGridCreate_servEvents: TIGridCreate_servEvents;
    ColorDialog: TColorDialog;
    GroupBoxMisc: TGroupBox;
    ProgressBar: TProgressBar;
    TimerSelection: TTimer;
    OpenDialogGrid: TOpenDialog;
    GroupBoxDatasource: TGroupBox;
    LabelTables: TLabel;
    LabelProgisIdField: TLabel;
    LabelISOField: TLabel;
    ButtonDatabase: TButton;
    ComboBoxTables: TComboBox;
    ComboBoxID: TComboBox;
    ComboBoxISO: TComboBox;
    GroupBoxObjects: TGroupBox;
    LabelStep: TLabel;
    LabelStart: TLabel;
    LabelStop: TLabel;
    LabelColors: TLabel;
    PanelColor: TPanel;
    PaintBox: TPaintBox;
    ButtonCreate: TButton;
    ButtonReset: TButton;
    RadioButtonLines: TRadioButton;
    RadioButtonAreas: TRadioButton;
    GroupBoxGrid: TGroupBox;
    LabelModule: TLabel;
    LabelRadius: TLabel;
    LabelSpacing: TLabel;
    ComboBoxModule: TComboBox;
    ButtonGrid: TButton;
    ButtonClose: TButton;
    ButtonProfile: TButton;
    EditRadius: TEdit;
    EditSpacing: TEdit;
    EditStart: TEdit;
    EditStop: TEdit;
    EditStep: TEdit;
    SpinEditColors: TSpinEdit;
    LabelDatabase: TLabel;
    EditDatabase: TEdit;
    CheckBoxOverlay: TCheckBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButtonCloseClick(Sender: TObject);
    procedure ButtonCreateClick(Sender: TObject);
    procedure ButtonDatabaseClick(Sender: TObject);
    procedure ComboBoxTablesClick(Sender: TObject);
    procedure IGrid_servEventsLineReady(Sender: TObject;
      out Pcount: Integer; out Lvalue: Single; Line: OleVariant);
    procedure ButtonGridClick(Sender: TObject);
    procedure IGridCreate_servEventsProgress(Sender: TObject;
      Progress: Integer);
    procedure PaintBoxMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PaintBoxPaint(Sender: TObject);
    procedure ComboBoxModuleChange(Sender: TObject);
    procedure ButtonResetClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ComboBoxIDChange(Sender: TObject);
    procedure ComboBoxISOChange(Sender: TObject);
    procedure ButtonProfileClick(Sender: TObject);
    procedure TimerSelectionTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EditRadiusSpacingChange(Sender: TObject);
    procedure EditStartStopStepChange(Sender: TObject);
    procedure SpinEditColorsChange(Sender: TObject);
  public
    Doc: IDocument;
    Core: ICore;
    Layer: ILayer;
    GridCreate_serv: IGridCreate_serv;
    Grid_serv: IGrid_serv;
    ColorCount: Integer;
    Color: Array[0..MAXCOLORS-1] of Integer;
    LastProfId: Integer;
    CreateCount: Integer;
    FieldName: Array[0..255] of Char;
    Settings: TStringList;
    AStarting: Boolean;
    procedure FillData;
    function GetColorStep(AStep,AMax: Double): Integer;
    procedure CalculateProfile;
    procedure CheckSelection;
    procedure CheckDocument;
    procedure UpdateGridControls;
    procedure SaveSettings;
    procedure WMWGMousePos(var Msg:TMessage); message WM_WGMousePos;
  end;

  function RunWiz(AHandle: Integer; ADoc: Variant; ACore: Variant): HRESULT;
  function SToF(s: String): Double;

var
  IsoWizForm: TIsoWizForm = nil;

const
  ConnectionStringTemplate = 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=%;Persist Security Info=False';

const
  Licensee = 'Progis Software AG';
  AccessKey = '5FA7-635F-66FD-B77F';
  GridFileVersion: Integer = 1;
  GridFileExt = '.grd';
  SettingsFileExt = '.ims';
  ModuleId: TGUID = '{354B6C7C-98F4-4759-AA1B-4808DADD3865}';
  NoDataVal = -9999;

implementation

uses Math, IsoWizProf;

{$R *.dfm}

function RunWiz(AHandle: Integer; ADoc: Variant; ACore: Variant): HRESULT;
begin
        Result:=S_OK;

        try
                Application.Handle:=AHandle;

                if IsoWizForm <> nil then begin
                        if IsoWizForm.Visible then begin
                                IsoWizForm.Close;
                        end;
                        IsoWizForm.Free;
                        IsoWizForm:=nil;
                end;

                IsoWizForm:=TIsoWizForm.Create(Application);
                IsoWizProfForm:=TIsoWizProfForm.Create(IsoWizForm);

                IsoWizForm.Doc:=IUnknown(ADoc) as IDocument;
                IsoWizForm.Core:=IUnknown(ACore) as ICore;

                IsoWizForm.Show;
        except
                Result:=S_FALSE;
        end;

end;

function SToF(s: String): Double;  
begin
     s:=StringReplace(s,'.',DecimalSeparator,[rfReplaceAll]);
     s:=StringReplace(s,',',DecimalSeparator,[rfReplaceAll]);
     try
        Result:=StrToFloat(s);
     except
        Result:=0;
     end;
end;

{ TIsoMainForm }

procedure TIsoWizForm.FormCreate(Sender: TObject);
var
i,n: Integer;
s: String;
begin
        Left:=Screen.Width-Width-10;
        Top:=10;

        GridCreate_serv:=CoGridCreate_Serv.Create;
        GridCreate_serv.InitAccess(Licensee,AccessKey);

        n:=GridCreate_serv.getGridModuleCount;

        for i:=0 to n-1 do begin
                s:=GridCreate_serv.getGridModule(i);
                ComboBoxModule.Items.Add(s);
        end;
        GridCreate_serv.activateModule(0);
        ComboBoxModule.ItemIndex:=0;

        IGridCreate_servEvents.Connect(GridCreate_serv);

        LastProfId:=0;

        ColorCount:=4;
        SpinEditColors.Value:=ColorCount;

        Color[0]:=clGreen;
        Color[1]:=clYellow;
        Color[2]:=clRed;
        Color[3]:=clFuchsia;
        Color[4]:=clBlue;
        Color[5]:=clAqua;
        Color[6]:=clLime;
        Color[7]:=clWhite;

        FieldName:='';

        Settings:=TStringList.Create;
end;

procedure TIsoWizForm.FormShow(Sender: TObject);
var
AFileName: AnsiString;
i,n: Integer;
begin
        AStarting:=True;

        AFileName:=ChangeFileExt(Doc.Name,SettingsFileExt);

        if (FileExists(AFileName)) then begin

                Settings.LoadFromFile(AFileName);

                ComboBoxModule.ItemIndex:=StrToIntDef(Settings.Values['Module'],0);

                n:=StrToIntDef(Settings.Values['ColorCount'],2);
                n:=Max(n,2);

                for i:=0 to n-1 do begin
                        Color[i]:=StrToIntDef('$' + Settings.Values['Color' + IntToStr(i)],Color[i]);
                end;

                RadioButtonLines.Checked:=(StrToIntDef(Settings.Values['ObjectType'],0) = 1);

                CheckBoxOverlay.Checked:=(StrToIntDef(Settings.Values['Overlay'],0) = 1);
        end;

        ButtonDatabaseClick(nil);

        Settings.Clear;

        AStarting:=False;

        TimerSelection.Enabled:=True;
end;

procedure TIsoWizForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
        TimerSelection.Enabled:=False;

        Settings.Free;

        if ADOConnection.Connected then begin
                ADOConnection.Close;
        end;

        Doc:=nil;

        IGridCreate_servEvents.Disconnect;
        GridCreate_serv:=nil;
        Grid_serv:=nil;

        IsoWizProfForm.Free;
end;

procedure TIsoWizForm.ButtonCloseClick(Sender: TObject);
begin
        Close;
end;

procedure TIsoWizForm.FillData;
var
i: Integer;
x,y: Integer;
AId: Integer;
ABase: IBase;
s: AnsiString;
d,z: Double;
RadiusFound,SpacingFound: Boolean;
Radius,Spacing: Double;
begin
        if (ADOTable.Active) and (Doc <> nil) then begin
                GridCreate_serv.clearXYZ;
                ADOTable.First;
                for i:=1 to ADOTable.RecordCount do begin
                        ProgressBar.Position:=(i*100) div ADOTable.RecordCount;
                        s:=ADOTable.Fields[ComboBoxID.ItemIndex].AsString;
                        AId:=StrToIntDef(s,-1);
                        if AId > 0 then begin
                                ABase:=Doc.GetObjectByIndex(AId);
                                if ABase <> nil then begin
                                        x:=(ABase.ClipRect.x1 + ABase.ClipRect.x2) div 2;
                                        y:=(ABase.ClipRect.y1 + ABase.ClipRect.y2) div 2;
                                        s:=ADOTable.Fields[ComboBoxISO.ItemIndex].AsString;
                                        if Trim(s) <> '' then begin
                                                z:=SToF(s);
                                        end
                                        else begin
                                                z:=NoDataVal;
                                        end;
                                        GridCreate_serv.addXYZ(x,y,z);
                                end;
                        end;
                        ADOTable.Next;
                end;
                ProgressBar.Position:=0;

                if (GridCreate_serv.SampleCount > 0) then begin

                        RadiusFound:=False;
                        SpacingFound:=False;

                        s:=Settings.Values['Radius'];
                        if s <> '' then begin
                                d:=SToF(s);
                                if d > 0 then begin
                                        RadiusFound:=True;
                                        EditRadius.Text:=FloatToStr(d);
                                end;
                        end;

                        s:=Settings.Values['Spacing'];
                        if s <> '' then begin
                                d:=SToF(s);
                                if d > 0 then begin
                                        SpacingFound:=True;
                                        EditSpacing.Text:=FloatToStr(d);
                                end;
                        end;

                        if (not RadiusFound) or (not SpacingFound) then begin
                                d:=Sqrt(Sqr(GridCreate_serv.Xmax_sample - GridCreate_serv.Xmin_sample) + Sqr(GridCreate_serv.Ymax_sample - GridCreate_serv.Ymin_sample));
                                d:=d/1000;

                                Radius:=Max(d,0.01);
                                Spacing:=Max(d/10,0.01);

                                Radius:=(Round(Radius * 100)/100);
                                Spacing:=(Round(Spacing * 100)/100);

                                if not RadiusFound then begin
                                        EditRadius.Text:=FloatToStr(Radius);
                                end;

                                if not SpacingFound then begin
                                        EditSpacing.Text:=FloatToStr(Spacing);
                                end;
                        end;
                end;

                UpdateGridControls;
                ButtonGridClick(nil);
        end;
end;

procedure TIsoWizForm.ButtonDatabaseClick(Sender: TObject);
var
s: String;
Idx: Integer;
ALayername: AnsiString;
AExt: AnsiString;
DBPath,DBFileName: AnsiString;
DBTableName: AnsiString;
ApplyDB: Boolean;
begin
        ApplyDB:=False;
        DBFileName:=Settings.Values['Database'];
        DBPath:=ExtractFilePath(DBFileName);
        if DBPath = '' then begin
                DBPath:=ExtractFilePath(Doc.Name);
                DBFileName:=DBPath + DBFileName;
        end;
        if (DBFileName <> '') and (FileExists(DBFileName)) then begin
                ApplyDB:=True;
                OpenDialog.FileName:=DBFileName;
        end;

        if not ApplyDB then begin
                OpenDialog.InitialDir:=ExtractFilePath(Doc.Name);

                if FileExists(ChangeFileExt(Doc.Name,'.wgi')) then begin
                        OpenDialog.FileName:=ChangeFileExt(Doc.Name,'.wgi');
                end
                else if FileExists(ChangeFileExt(Doc.Name,'.mdb')) then begin
                        OpenDialog.FileName:=ChangeFileExt(Doc.Name,'.mdb');
                end;
        end;

        if (ApplyDB) or (OpenDialog.Execute) then begin

                AExt:=ExtractFileExt(OpenDialog.FileName);

                s:=ConnectionStringTemplate;
                s:=StringReplace(s,'%',OpenDialog.FileName,[]);

                if ADOConnection.Connected then begin
                        ADOConnection.Close;
                end;

                ADOConnection.ConnectionString:=s;
                ADOConnection.Open;

                if ADOConnection.Connected then begin

                        EditDatabase.Text:=OpenDialog.FileName;

                        ADOConnection.GetTableNames(ComboBoxTables.Items);

                        ComboBoxTables.Enabled:=True;
                        ComboBoxTables.SetFocus;

                        Idx:=-1;

                        if ApplyDB then begin
                                DBTableName:=Settings.Values['Table'];
                                if DBTableName <> '' then begin
                                        Idx:=ComboBoxTables.Items.IndexOf(DBTableName);
                                        if Idx > -1 then begin
                                                ComboBoxTables.ItemIndex:=Idx;
                                                ComboBoxTablesClick(nil);
                                        end;
                                end;
                        end;

                        if Idx = -1 then begin
                                ALayername:=Doc.Layers.ActiveLayer.LayerName;

                                if Lowercase(AExt) = '.wgi' then begin
                                        Idx:=ComboBoxTables.Items.IndexOf('ATT_' + ALayername);
                                end
                                else if Lowercase(AExt) = '.mdb' then begin
                                        Idx:=ComboBoxTables.Items.IndexOf(ALayername);
                                end;

                                if Idx > -1 then begin
                                        ComboBoxTables.ItemIndex:=Idx;
                                        ComboBoxTablesClick(nil);
                                end;
                        end;
                end;
        end;
end;

procedure TIsoWizForm.ComboBoxTablesClick(Sender: TObject);
var
i: Integer;
s: String;
IDFieldIndex,ISOFieldIndex: Integer;
IDFieldName,ISOFieldName: AnsiString;
begin
        if (ADOConnection.Connected) and (ComboBoxTables.ItemIndex <> -1) then begin

                IDFieldIndex:=-1;
                ISOFieldIndex:=-1;

                if ADOTable.Active then begin
                        ADOTable.Close;
                end;

                ADOTable.TableName:=ComboBoxTables.Items[ComboBoxTables.ItemIndex];

                ADOTable.Open;

                if (ADOTable.Active) then begin

                        ComboBoxID.Items.Clear;
                        ComboBoxISO.Items.Clear;

                        for i:=0 to ADOTable.FieldCount-1 do begin
                                s:=ADOTable.Fields[i].DisplayName;

                                if Pos('progis',Lowercase(s)) > 0 then begin
                                        IDFieldIndex:=i;
                                end;

                                ComboBoxID.Items.Add(s);
                                ComboBoxISO.Items.Add(s);
                        end;

                        ComboBoxID.Enabled:=True;
                        ComboBoxISO.Enabled:=True;

                        IDFieldName:=Settings.Values['ProgisIDField'];
                        if (IDFieldName <> '') then begin
                                IDFieldIndex:=ComboBoxID.Items.IndexOf(IDFieldName);
                                if IDFieldIndex > -1 then begin
                                        ComboBoxID.ItemIndex:=IDFieldIndex;
                                        ComboBoxIDChange(nil);
                                end;
                        end
                        else if IDFieldIndex > -1 then begin
                                ComboBoxID.ItemIndex:=IDFieldIndex;
                                ComboBoxIDChange(nil);
                        end;

                        ISOFieldName:=Settings.Values['ISOField'];
                        if (ISOFieldName <> '') then begin
                                ISOFieldIndex:=ComboBoxISO.Items.IndexOf(ISOFieldName);
                                if ISOFieldIndex > -1 then begin
                                        ComboBoxISO.ItemIndex:=ISOFieldIndex;
                                        ComboBoxISOChange(nil);
                                end;
                        end;

                        if ISOFieldIndex = -1 then begin
                                ComboBoxISO.SetFocus;
                        end;
                        if IDFieldIndex = -1 then begin
                                ComboBoxID.SetFocus;
                        end;
                        if (IDFieldIndex > -1) and (ISOFieldIndex > -1) then begin
                                FillData;
                        end;
                end;
        end;
end;

procedure TIsoWizForm.ButtonGridClick(Sender: TObject);
var
Spacing,Radius: Double;
begin
        Spacing:=SToF(EditSpacing.Text)*100;
        Radius:=SToF(EditRadius.Text)*100;

        if (Spacing > 0) and (Radius > 0) then begin
                GridCreate_serv.setSpacing(Spacing,Spacing);
                GridCreate_serv.search_radius:=Radius;
                GridCreate_serv.NodataValue := NoDataVal;

                Grid_serv:=GridCreate_serv.createGrid;
                Grid_serv.InitAccess(Licensee,AccessKey);

                UpdateGridControls;
                ButtonGrid.Enabled:=False;
        end;
end;

procedure TIsoWizForm.UpdateGridControls;
var
DimX,DimY: Integer;
AStart,AStop,AStep: Double;
StartFound,StopFound,StepFound: Boolean;
d,a: Double;
s: AnsiString;
begin
        AStart:=0;
        AStop:=0;
        AStep:=0;

        if (GridCreate_serv.SampleCount > 0) then begin
                ComboBoxModule.Enabled:=True;
                EditRadius.Enabled:=True;
                EditSpacing.Enabled:=True;
                ButtonGrid.Enabled:=True;
        end;

        if (Grid_serv <> nil) then begin

                if AStarting then begin
                        EditRadius.Text:=Settings.Values['Radius'];
                        EditSpacing.Text:=Settings.Values['Spacing'];
                end;

                StartFound:=False;
                StopFound:=False;
                StepFound:=False;

                s:=Settings.Values['Start'];
                if s <> '' then begin
                        d:=SToF(s);
                        StartFound:=True;
                        AStart:=d;
                end;

                s:=Settings.Values['Stop'];
                if s <> '' then begin
                        d:=SToF(s);
                        StopFound:=True;
                        AStop:=d;
                end;

                s:=Settings.Values['Step'];
                if s <> '' then begin
                        d:=SToF(s);
                        StepFound:=True;
                        AStep:=d;
                end;

                if (not StartFound) or (not StopFound) or (not StepFound) then begin
                        Grid_serv.get_dim(DimX,DimY);

                        AStart:=Grid_serv.minZ;
                        AStop:=Grid_serv.maxZ;

                        AStep:=(Round(AStop-AStart)*10)/1000;
                        AStep:=Max(AStep,0.001);
                        
                        a:=AStart;
                        while a <= (AStop+AStep) do begin
                                a:=a + AStep;
                        end;
                        AStop:=a;

                        AStart:=(Round(AStart*1000))/1000;
                        AStop:=(Round(AStop*1000))/1000;
                end;

                EditStart.Text:=FloatToStr(AStart);
                EditStop.Text:=FloatToStr(AStop);
                EditStep.Text:=FloatToStr(AStep);

                PaintBox.Visible:=True;

                EditStart.Enabled:=True;
                EditStop.Enabled:=True;
                EditStep.Enabled:=True;

                RadioButtonAreas.Enabled:=True;
                RadioButtonLines.Enabled:=True;
        end;
end;

procedure TIsoWizForm.ButtonCreateClick(Sender: TObject);
var
AColor: Integer;
z: Double;
d: Double;
ALayerName: String;
Start,Stop,Step: Double;
LastLayerName: String;
OverlayLayerName: String;
begin
        ButtonResetClick(nil);

        CreateCount:=0;

        Start:=SToF(EditStart.Text);
        Stop:=SToF(EditStop.Text);
        Step:=SToF(EditStep.Text);

        if (Doc <> nil) and (Grid_serv <> nil) and (Start < Stop) and (Step > 0.001) then begin

                IGrid_servEvents.Connect(Grid_serv);

                z:=Stop;
                d:=Step;

                OverlayLayerName:=StrPas(FieldName) + '_Overlay';

                Layer:=Doc.Layers.GetLayerByName(OverlayLayerName);
                if Layer <> nil then begin
                        Doc.DeleteLayerByName(OverlayLayerName);
                end;

                Layer:=Doc.Layers.InsertLayerByName(OverlayLayerName,False);

                LastLayerName:='';

                while z >= Start do begin

                        ProgressBar.Position:=Trunc(((Stop-z)*100)/(Stop-Start));

                        z:=(Round(z*1000)/1000);

                        ALayerName:=FloatToStr(z);
                        ALayerName:=StringReplace(ALayerName,',','.',[rfReplaceAll]);
                        ALayerName:=StrPas(FieldName) + '_' + ALayerName;

                        Layer:=Doc.Layers.GetLayerByName(ALayerName);
                        if Layer <> nil then begin
                                Doc.DeleteLayerByName(ALayerName);
                        end;

                        Layer:=Doc.Layers.InsertLayerByName(ALayerName,False);

                        AColor:=GetColorStep(z-Start,Stop-Start);

                        Layer.LineStyle.Color:=AColor;

                        Layer.FillStyle.Pattern:=7;
                        Layer.FillStyle.ForeColor:=AColor;

                        Grid_serv.getLines(z);

                        z:=z-d;

                        if (CheckBoxOverlay.Checked) and (LastLayerName <> '') then begin
                                Core.ExecDDE('[DPO][' + ALayerName + '][' + LastLayername + '][' + OverlayLayerName + '][1,0,0,1]');
                        end;

                        LastLayerName:=ALayerName;
                end;

                Doc.ReArrangeBounds;
                Doc.Redraw;

                ProgressBar.Position:=0;

                IGrid_servEvents.Disconnect;

                SaveSettings;
        end;
end;

procedure TIsoWizForm.IGrid_servEventsLineReady(Sender: TObject;
  out Pcount: Integer; out Lvalue: Single; Line: OleVariant);
var
i: Integer;
x,y: Integer;
APolygon: IPolygon;
APolyline: IPolyline;

begin
        if Doc <> nil then begin
                if RadioButtonAreas.Checked then begin
                        APolygon:=Doc.CreatePolygon;
                end
                else begin
                        APolyline:=Doc.CreatePolyline;
                end;

                for i := 0 to PCount-1 do begin
                        x:=Trunc(Line[0,i]);
                        y:=Trunc(Line[1,i]);
                        if RadioButtonAreas.Checked then begin
                                APolygon.InsertPointxy(x,y);
                        end
                        else begin
                                APolyline.InsertPointxy(x,y);
                        end;
                end;

                if RadioButtonAreas.Checked then begin
                        Layer.InsertObject(APolygon);
                end
                else begin
                        Layer.InsertObject(APolyline);
                end;

                Inc(CreateCount);
        end;
end;

procedure TIsoWizForm.IGridCreate_servEventsProgress(Sender: TObject;
  Progress: Integer);
begin
        ProgressBar.Position:=Progress;
end;

procedure TIsoWizForm.PaintBoxMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
i: Integer;
begin
        if Button = mbLeft then begin

                i:=((X*ColorCount) div PaintBox.Width);

                ColorDialog.Color:=Color[i];
                if ColorDialog.Execute then begin
                        Color[i]:=ColorDialog.Color;
                end;

                PaintBox.Repaint;
        end;
end;

procedure TIsoWizForm.PaintBoxPaint(Sender: TObject);
var
i,x: Integer;
begin
        for i:=0 to PaintBox.Width-1 do begin
                PaintBox.Canvas.Pen.Color:=GetColorStep(i,PaintBox.Width);
                PaintBox.Canvas.MoveTo(i,0);
                PaintBox.Canvas.LineTo(i,PaintBox.Height);
        end;

        for i:=1 to ColorCount-1 do begin

                x:=(PaintBox.Width div ColorCount) * i;

                PaintBox.Canvas.Pen.Color:=clWhite;
                PaintBox.Canvas.MoveTo(x+i,0);
                PaintBox.Canvas.LineTo(x+i,PaintBox.Height);

                PaintBox.Canvas.Pen.Color:=clSilver;
                PaintBox.Canvas.MoveTo(x+i+1,0);
                PaintBox.Canvas.LineTo(x+i+1,PaintBox.Height);
        end;
end;

function TIsoWizForm.GetColorStep(AStep, AMax: Double): Integer;
var
R,G,B: Double;
REnd,GEnd,BEnd: Double;
RStep,GStep,BStep: Double;
n: Double;
i: Integer;
begin
        n:=(AMax/(ColorCount-1));
        i:=Trunc((AStep*(ColorCount-1)) / AMax);

        R:=GetRValue(Color[i]);
        G:=GetGValue(Color[i]);
        B:=GetBValue(Color[i]);

        REnd:=GetRValue(Color[i+1]);
        GEnd:=GetGValue(Color[i+1]);
        BEnd:=GetBValue(Color[i+1]);

        RStep:=(REnd - R) / n;
        GStep:=(GEnd - G) / n;
        BStep:=(BEnd - B) / n;

        R:=R + RStep * (AStep-(n*i));
        G:=G + GStep * (AStep-(n*i));
        B:=B + BStep * (AStep-(n*i));
        
        Result:=RGB(Trunc(R),Trunc(G),Trunc(B));
end;

procedure TIsoWizForm.ComboBoxModuleChange(Sender: TObject);
begin
        GridCreate_serv.activateModule(ComboBoxModule.ItemIndex);
        ButtonGrid.Enabled:=True;
end;

procedure TIsoWizForm.ButtonResetClick(Sender: TObject);
var
i,n: Integer;
s,fn: AnsiString;
ll: TStringList;
begin
        if Doc <> nil then begin
                ll:=TStringList.Create;
                try
                        Doc.DeselectAll;
                        fn:=StrPas(FieldName);
                        n:=Doc.Layers.Count;

                        for i:=0 to n-1 do begin
                                s:=Doc.Layers.Items[i].LayerName;
                                if (fn <> '') and (Pos(fn + '_',s) > 0) then begin
                                        ll.Add(s);
                                end;
                        end;

                        for i:=0 to ll.Count-1 do begin
                                ProgressBar.Position:=((i+1)*100) div (n);
                                Doc.DeleteLayerByName(ll[i]);
                        end;
                finally
                        ll.Free;
                        Doc.ReArrangeBounds;
                        Doc.Redraw;
                        ProgressBar.Position:=0;
                end;
        end;
end;

procedure TIsoWizForm.ComboBoxIDChange(Sender: TObject);
begin
        if ComboBoxISO.ItemIndex > -1 then begin
                FillData;
        end
        else begin
                ComboBoxISO.SetFocus;
        end;
end;

procedure TIsoWizForm.ComboBoxISOChange(Sender: TObject);
begin
        if ComboBoxID.ItemIndex > -1 then begin
                StrCopy(FieldName,PChar(ComboBoxISO.Items[ComboBoxISO.ItemIndex]));
                ButtonReset.Enabled:=True;
                ButtonProfile.Enabled:=True;
                if not AStarting then begin
                        Settings.Clear;
                end;
                FillData;
        end;
end;

procedure TIsoWizForm.ButtonProfileClick(Sender: TObject);
begin
        CalculateProfile;
        Core.ExecDDE('[SMR][' + IntToStr(Handle) + ']');
        IsoWizProfForm.Show;
end;

procedure TIsoWizForm.CalculateProfile;
var
ABase: IBase;
APoly: IPolyline;
x,y,xs,ys,xe,ye: Double;
dx,dy: Double;
z: Single;
NoDataFlag,OutOfGrid: WordBool;
i,k: Integer;
Ser: TChartSeries;
Km: Boolean;
Pos,Step: Double;
StepCount: Double;
n: Double;
begin
        if (Doc <> nil) and (Grid_serv <> nil) then begin
                Ser:=IsoWizProfForm.Chart.SeriesList.Items[0];
                if  (Doc.Layers.SelectLayer.Count = 1) then begin
                        ABase:=Doc.Layers.SelectLayer.Items[0];
                        if (ABase <> nil) and (ABase.ObjectType = 2 {ot_Poly}) then begin
                                APoly:=ABase as IPolyline;
                                if (APoly.Count >= 2) then begin
                                        if ABase.Index <> LastProfId then begin

                                              LastProfId:=ABase.Index;

                                              Ser.Clear;

                                              Pos:=0;
                                              StepCount:=1000;
                                              Step:=APoly.Length / StepCount;

                                              for k:=0 to APoly.Count-2 do begin

                                                xs:=APoly.Points[k].x;
                                                ys:=APoly.Points[k].y;
                                                xe:=APoly.Points[k+1].x;
                                                ye:=APoly.Points[k+1].y;

                                                n:=StepCount / (APoly.Count-1);
                                                Km:=(APoly.Length > 10000);

                                                dx:=(xe-xs)/n;
                                                dy:=(ye-ys)/n;

                                                for i:=0 to Trunc(n) do begin
                                                        x:=xs + i*dx;
                                                        y:=ys + i*dy;

                                                        Grid_serv.getInterpolatedVertex(x,y,z,NoDataFlag,OutOfGrid);

                                                        if (NoDataFlag) or (OutOfGrid) then begin
                                                                z:=0;
                                                        end;

                                                        Pos:=Pos + Step;

                                                        if Km then begin
                                                                Ser.AddXY(Pos/1000,z)
                                                        end
                                                        else begin
                                                                Ser.AddXY(Pos,z);
                                                        end;
                                                end;
                                           end;
                                        end;
                                end;
                        end;
                end
                else begin
                        Ser.Clear;
                        LastProfId:=0;
                end;
        end;
end;

procedure TIsoWizForm.TimerSelectionTimer(Sender: TObject);
begin
        TimerSelection.Enabled:=False;
        try
                CheckDocument;
                CalculateProfile;
                CheckSelection;
        finally
                TimerSelection.Enabled:=True;
        end;
end;

procedure TIsoWizForm.CheckSelection;
var
i: Integer;
ABase: IBase;
begin
        if (Doc <> nil) and (Grid_serv <> nil) then begin
                for i:=0 to Doc.Layers.SelectLayer.Count-1 do begin
                        ABase:=Doc.Layers.SelectLayer.Items[i];
                        if (ABase <> nil) and (ABase.ObjectType = 1 {ot_Pixel}) then begin
                                break;
                        end;
                end;
        end;
end;

procedure TIsoWizForm.CheckDocument;
begin
        try
                if Doc <> nil then begin
                        Doc.Layers.Count;
                end;
        except
                Doc:=nil;
                Close;
        end;
end;

procedure TIsoWizForm.SaveSettings;
var
AFilename: AnsiString;
i: Integer;
begin
        Settings.Clear;

        Settings.Add('Database=' + ExtractFileName(OpenDialog.FileName));
        Settings.Add('Table=' + ADOTable.TableName);
        Settings.Add('ProgisIDField=' + ComboBoxID.Text);
        Settings.Add('ISOField=' + ComboBoxISO.Text);
        Settings.Add('Radius=' + EditRadius.Text);
        Settings.Add('Spacing=' + EditSpacing.Text);
        Settings.Add('Module=' + IntToStr(ComboBoxModule.ItemIndex));
        Settings.Add('Start=' + EditStart.Text);
        Settings.Add('Stop=' + EditStop.Text);
        Settings.Add('Step=' + EditStep.Text);

        Settings.Add('ColorCount=' + IntToStr(ColorCount));
        for i:=0 to ColorCount-1 do begin
                Settings.Add('Color' + IntToStr(i) + '=' + IntToHex(Color[i],6));
        end;

        Settings.Add('ObjectType=' + IntToStr(Ord(RadioButtonLines.Checked)));
        Settings.Add('Overlay=' + IntToStr(Ord(CheckboxOverlay.Checked)));

        AFilename:=ChangeFileExt(Doc.Name,SettingsFileExt);
        Settings.SaveToFile(AFilename);
end;

procedure TIsoWizForm.EditRadiusSpacingChange(Sender: TObject);
var
Radius,Spacing: Double;
begin
        Radius:=SToF(EditRadius.Text);
        Spacing:=SToF(EditSpacing.Text);
        ButtonGrid.Enabled:=(Radius > 0.01) and (Spacing > 0.01);
end;

procedure TIsoWizForm.EditStartStopStepChange(Sender: TObject);
var
Start,Stop,Step: Double;
begin
        Start:=SToF(EditStart.Text);
        Stop:=SToF(EditStop.Text);
        Step:=SToF(EditStep.Text);
        ButtonCreate.Enabled:=(Start < Stop) and (Step > 0.001);
end;

procedure TIsoWizForm.SpinEditColorsChange(Sender: TObject);
begin
        SpinEditColors.Value:=Min(SpinEditColors.Value,MAXCOLORS);
        SpinEditColors.Value:=Max(SpinEditColors.Value,2);
        ColorCount:=SpinEditColors.Value;
        PaintBox.Repaint;
end;

procedure TIsoWizForm.WMWGMousePos(var Msg: TMessage);
var
x,y: Double;
z: Single;
NoDataFlag,OutOfGrid: WordBool;
begin
        if (Doc <> nil) and (Grid_serv <> nil) then begin

                x:=Msg.WParam;
                y:=Msg.LParam;

                Grid_serv.getInterpolatedVertex(x,y,z,NoDataFlag,OutOfGrid);

                if (NoDataFlag) or (OutOfGrid) then begin
                        IsoWizProfForm.EditValue.Text:='';
                end
                else begin
                        IsoWizProfForm.EditValue.Text:=Format('%.2f',[z]);
                end;
        end;
end;

end.
