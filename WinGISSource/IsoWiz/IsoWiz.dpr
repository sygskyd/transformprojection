library IsoWiz;

uses
  IsoWizMain in 'IsoWizMain.pas' {IsoWizForm},
  IsoWizProf in 'IsoWizProf.pas' {IsoWizProfForm};

function CreateIsoObjects(Handle: Integer; Doc: Variant; Core: Variant): HRESULT; stdcall;
begin
        Result:=RunWiz(Handle,Doc,Core);
end;

{$R *.res}

exports CreateIsoObjects;

begin
end.
