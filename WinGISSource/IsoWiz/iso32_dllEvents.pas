{ *****************************************************************************
  WARNING: This component file was generated using the EventSinkImp utility.
           The contents of this file will be overwritten everytime EventSinkImp
           is asked to regenerate this sink component.

  NOTE:    When using this component at the same time with the XXX_TLB.pas in
           your Delphi projects, make sure you always put the XXX_TLB unit name
           AFTER this component unit name in the USES clause of the interface
           section of your unit; otherwise you will get interface conflict
           errors from the Delphi compiler.

           EventSinkImp is written by Binh Ly (bly@castle.net)
  *****************************************************************************
  //Sink Classes//
  TIGrid_servEvents
  TIGridCreate_servEvents
}

{$IFDEF VER100}
{$DEFINE D3}
{$ENDIF}

//SinkUnitName//
unit iso32_dllEvents;

interface

uses
  Windows,
  ActiveX,
  Classes,
  ComObj,
  OleCtrls
  //SinkUses//
  , StdVCL
  , iso32_dll_TLB
  ;

type
  { backward compatibility types }
  {$IFDEF D3}
  OLE_COLOR = TOleColor;
  {$ENDIF}

  Tiso32_dllEventsBaseSink = class (TComponent, IUnknown, IDispatch)
  protected
    { IUnknown }
    function QueryInterface(const IID: TGUID; out Obj): HResult; {$IFNDEF D3} override; {$ENDIF} stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;

    { IDispatch }
    function GetIDsOfNames(const IID: TGUID; Names: Pointer;
      NameCount, LocaleID: Integer; DispIDs: Pointer): HResult; virtual; stdcall;
    function GetTypeInfo(Index, LocaleID: Integer; out TypeInfo): HResult; virtual; stdcall;
    function GetTypeInfoCount(out Count: Integer): HResult; virtual; stdcall;
    function Invoke(DispID: Integer; const IID: TGUID; LocaleID: Integer;
      Flags: Word; var Params; VarResult, ExcepInfo, ArgErr: Pointer): HResult; virtual; stdcall;
  protected
    FCookie : integer;
    FCP : IConnectionPoint;
    FSinkIID : TGuid;
    FSource : IUnknown;
    function DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
      Flags: Word; var dps : TDispParams; pDispIds : PDispIdList;
      VarResult, ExcepInfo, ArgErr: Pointer): HResult; virtual; abstract;
  public
    destructor Destroy; override;
    procedure Connect (pSource : IUnknown);
    procedure Disconnect;
    property SinkIID : TGuid read FSinkIID;
    property Source : IUnknown read FSource;
  end;

  //SinkImportsForwards//

  //SinkImports//

  //SinkIntfStart//

  //SinkEventsForwards//
  TIGrid_servEventsOnLineReadyEvent = procedure (Sender : TObject; out Pcount: Integer; out Lvalue: Single; Line: OleVariant) of object;
  TIGrid_servEventsOnReadyEvent = procedure (Sender : TObject) of object;
  TIGrid_servEventsOnVertexEvent = procedure (Sender : TObject; x: Double; y: Double; Z: Double) of object;
  TIGrid_servEventsOnProgressEvent = procedure (Sender : TObject; Progress: Integer) of object;
  TIGrid_servEventsOnProfileReadyEvent = procedure (Sender : TObject; Pcount: Integer; Profile: OleVariant) of object;

  //SinkComponent//
  TIGrid_servEvents = class (Tiso32_dllEventsBaseSink
    //ISinkInterface//
  )
  protected
    function DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
      Flags: Word; var dps : TDispParams; pDispIds : PDispIdList;
      VarResult, ExcepInfo, ArgErr: Pointer): HResult; override;

    //ISinkInterfaceMethods//
  public
    { system methods }
    constructor Create (pOwner : TComponent); override;
  protected
    //SinkInterface//
    procedure DoOnLineReady(out Pcount: Integer; out Lvalue: Single; Line: OleVariant); safecall;
    procedure DoOnReady; safecall;
    procedure DoOnVertex(x: Double; y: Double; Z: Double); safecall;
    procedure DoOnProgress(Progress: Integer); safecall;
    procedure DoOnProfileReady(Pcount: Integer; Profile: OleVariant); safecall;
  protected
    //SinkEventsProtected//
    FOnLineReady : TIGrid_servEventsOnLineReadyEvent;
    FOnReady : TIGrid_servEventsOnReadyEvent;
    FOnVertex : TIGrid_servEventsOnVertexEvent;
    FOnProgress : TIGrid_servEventsOnProgressEvent;
    FOnProfileReady : TIGrid_servEventsOnProfileReadyEvent;
  published
    //SinkEventsPublished//
    property OnLineReady : TIGrid_servEventsOnLineReadyEvent read FOnLineReady write FOnLineReady;
    property OnReady : TIGrid_servEventsOnReadyEvent read FOnReady write FOnReady;
    property OnVertex : TIGrid_servEventsOnVertexEvent read FOnVertex write FOnVertex;
    property OnProgress : TIGrid_servEventsOnProgressEvent read FOnProgress write FOnProgress;
    property OnProfileReady : TIGrid_servEventsOnProfileReadyEvent read FOnProfileReady write FOnProfileReady;
  end;


  //SinkEventsForwards//
  TIGridCreate_servEventsOnProgressEvent = procedure (Sender : TObject; Progress: Integer) of object;

  //SinkComponent//
  TIGridCreate_servEvents = class (Tiso32_dllEventsBaseSink
    //ISinkInterface//
  )
  protected
    function DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
      Flags: Word; var dps : TDispParams; pDispIds : PDispIdList;
      VarResult, ExcepInfo, ArgErr: Pointer): HResult; override;

    //ISinkInterfaceMethods//
  public
    { system methods }
    constructor Create (pOwner : TComponent); override;
  protected
    //SinkInterface//
    procedure DoOnProgress(Progress: Integer); safecall;
  protected
    //SinkEventsProtected//
    FOnProgress : TIGridCreate_servEventsOnProgressEvent;
  published
    //SinkEventsPublished//
    property OnProgress : TIGridCreate_servEventsOnProgressEvent read FOnProgress write FOnProgress;
  end;

  //SinkIntfEnd//

procedure Register;

implementation

uses
  SysUtils
  ;

{ globals }

procedure BuildPositionalDispIds (pDispIds : PDispIdList; const dps : TDispParams);
var
  i : integer;
begin
  Assert (pDispIds <> NIL);
  
  { by default, directly arrange in reverse order }
  for i := 0 to dps.cArgs - 1 do
    pDispIds^ [i] := dps.cArgs - 1 - i;

  { check for named args }
  if (dps.cNamedArgs <= 0) then Exit;

  { parse named args }
  for i := 0 to dps.cNamedArgs - 1 do
    pDispIds^ [dps.rgdispidNamedArgs^ [i]] := i;
end;


{ Tiso32_dllEventsBaseSink }

function Tiso32_dllEventsBaseSink.GetIDsOfNames(const IID: TGUID; Names: Pointer;
  NameCount, LocaleID: Integer; DispIDs: Pointer): HResult;
begin
  Result := E_NOTIMPL;
end;

function Tiso32_dllEventsBaseSink.GetTypeInfo(Index, LocaleID: Integer; out TypeInfo): HResult;
begin
  Result := E_NOTIMPL;
  pointer (TypeInfo) := NIL;
end;

function Tiso32_dllEventsBaseSink.GetTypeInfoCount(out Count: Integer): HResult;
begin
  Result := E_NOTIMPL;
  Count := 0;
end;

function Tiso32_dllEventsBaseSink.Invoke(DispID: Integer; const IID: TGUID; LocaleID: Integer;
  Flags: Word; var Params; VarResult, ExcepInfo, ArgErr: Pointer): HResult;
var
  dps : TDispParams absolute Params;
  bHasParams : boolean;
  pDispIds : PDispIdList;
  iDispIdsSize : integer;
begin
  { validity checks }
  if (Flags AND DISPATCH_METHOD = 0) then
    raise Exception.Create (
      Format ('%s only supports sinking of method calls!', [ClassName]
    ));

  { build pDispIds array. this maybe a bit of overhead but it allows us to
    sink named-argument calls such as Excel's AppEvents, etc!
  }
  pDispIds := NIL;
  iDispIdsSize := 0;
  bHasParams := (dps.cArgs > 0);
  if (bHasParams) then
  begin
    iDispIdsSize := dps.cArgs * SizeOf (TDispId);
    GetMem (pDispIds, iDispIdsSize);
  end;  { if }

  try
    { rearrange dispids properly }
    if (bHasParams) then BuildPositionalDispIds (pDispIds, dps);
    Result := DoInvoke (DispId, IID, LocaleID, Flags, dps, pDispIds, VarResult, ExcepInfo, ArgErr);
  finally
    { free pDispIds array }
    if (bHasParams) then FreeMem (pDispIds, iDispIdsSize);
  end;  { finally }
end;

function Tiso32_dllEventsBaseSink.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  Result := E_NOINTERFACE;
  pointer (Obj) := NIL;
  if (GetInterface (IID, Obj)) then Result := S_OK;
  if not Succeeded (Result) then
    if (IsEqualIID (IID, FSinkIID)) then
      if (GetInterface (IDispatch, Obj)) then
        Result := S_OK;
end;

function Tiso32_dllEventsBaseSink._AddRef: Integer;
begin
  Result := 2;
end;

function Tiso32_dllEventsBaseSink._Release: Integer;
begin
  Result := 1;
end;

destructor Tiso32_dllEventsBaseSink.Destroy;
begin
  Disconnect;
  inherited;
end;

procedure Tiso32_dllEventsBaseSink.Connect (pSource : IUnknown);
var
  pcpc : IConnectionPointContainer;
begin
  Assert (pSource <> NIL);
  Disconnect;
  try
    OleCheck (pSource.QueryInterface (IConnectionPointContainer, pcpc));
    OleCheck (pcpc.FindConnectionPoint (FSinkIID, FCP));
    OleCheck (FCP.Advise (Self, FCookie));
    FSource := pSource;
  except
    raise Exception.Create (Format ('Unable to connect %s.'#13'%s',
      [Name, Exception (ExceptObject).Message]
    ));
  end;  { finally }
end;

procedure Tiso32_dllEventsBaseSink.Disconnect;
begin
  if (FSource = NIL) then Exit;
  try
    OleCheck (FCP.Unadvise (FCookie));
    FCP := NIL;
    FSource := NIL;
  except
    pointer (FCP) := NIL;
    pointer (FSource) := NIL;
  end;  { except }
end;


//SinkImplStart//

function TIGrid_servEvents.DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
  Flags: Word; var dps : TDispParams; pDispIds : PDispIdList;
  VarResult, ExcepInfo, ArgErr: Pointer): HResult;
type
  POleVariant = ^OleVariant;
begin
  Result := DISP_E_MEMBERNOTFOUND;

  //SinkInvoke//
    case DispId of
      1 :
      begin
        DoOnLineReady (dps.rgvarg^ [pDispIds^ [0]].plval^, dps.rgvarg^ [pDispIds^ [1]].pfltval^, OleVariant (dps.rgvarg^ [pDispIds^ [2]]));
        Result := S_OK;
      end;
      2 :
      begin
        DoOnReady ();
        Result := S_OK;
      end;
      3 :
      begin
        DoOnVertex (dps.rgvarg^ [pDispIds^ [0]].dblval, dps.rgvarg^ [pDispIds^ [1]].dblval, dps.rgvarg^ [pDispIds^ [2]].dblval);
        Result := S_OK;
      end;
      4 :
      begin
        DoOnProgress (dps.rgvarg^ [pDispIds^ [0]].lval);
        Result := S_OK;
      end;
      5 :
      begin
        DoOnProfileReady (dps.rgvarg^ [pDispIds^ [0]].lval, OleVariant (dps.rgvarg^ [pDispIds^ [1]]));
        Result := S_OK;
      end;
    end;  { case }
  //SinkInvokeEnd//
end;

constructor TIGrid_servEvents.Create (pOwner : TComponent);
begin
  inherited Create (pOwner);
  //SinkInit//
  FSinkIID := IGrid_servEvents;
end;

//SinkImplementation//
procedure TIGrid_servEvents.DoOnLineReady(out Pcount: Integer; out Lvalue: Single; Line: OleVariant);
begin
  if not Assigned (OnLineReady) then Exit;
  OnLineReady (Self, Pcount, Lvalue, Line);
end;

procedure TIGrid_servEvents.DoOnReady;
begin
  if not Assigned (OnReady) then Exit;
  OnReady (Self);
end;

procedure TIGrid_servEvents.DoOnVertex(x: Double; y: Double; Z: Double);
begin
  if not Assigned (OnVertex) then Exit;
  OnVertex (Self, x, y, Z);
end;

procedure TIGrid_servEvents.DoOnProgress(Progress: Integer);
begin
  if not Assigned (OnProgress) then Exit;
  OnProgress (Self, Progress);
end;

procedure TIGrid_servEvents.DoOnProfileReady(Pcount: Integer; Profile: OleVariant);
begin
  if not Assigned (OnProfileReady) then Exit;
  OnProfileReady (Self, Pcount, Profile);
end;



function TIGridCreate_servEvents.DoInvoke (DispID: Integer; const IID: TGUID; LocaleID: Integer;
  Flags: Word; var dps : TDispParams; pDispIds : PDispIdList;
  VarResult, ExcepInfo, ArgErr: Pointer): HResult;
type
  POleVariant = ^OleVariant;
begin
  Result := DISP_E_MEMBERNOTFOUND;

  //SinkInvoke//
    case DispId of
      1 :
      begin
        DoOnProgress (dps.rgvarg^ [pDispIds^ [0]].lval);
        Result := S_OK;
      end;
    end;  { case }
  //SinkInvokeEnd//
end;

constructor TIGridCreate_servEvents.Create (pOwner : TComponent);
begin
  inherited Create (pOwner);
  //SinkInit//
  FSinkIID := IGridCreate_servEvents;
end;

//SinkImplementation//
procedure TIGridCreate_servEvents.DoOnProgress(Progress: Integer);
begin
  if not Assigned (OnProgress) then Exit;
  OnProgress (Self, Progress);
end;


//SinkImplEnd//

procedure Register;
begin
  //SinkRegisterStart//
  RegisterComponents ('ActiveX', [TIGrid_servEvents]);
  RegisterComponents ('ActiveX', [TIGridCreate_servEvents]);
  //SinkRegisterEnd//
end;

end.
