object IsoWizProfForm: TIsoWizProfForm
  Left = 349
  Top = 169
  Width = 330
  Height = 212
  BorderIcons = [biSystemMenu]
  Caption = 'ISO Profile'
  Color = clBtnFace
  Constraints.MinHeight = 212
  Constraints.MinWidth = 330
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    314
    174)
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBoxProfile: TGroupBox
    Left = 8
    Top = 8
    Width = 297
    Height = 108
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    DesignSize = (
      297
      108)
    object Chart: TChart
      Left = 8
      Top = 16
      Width = 281
      Height = 81
      AllowPanning = pmNone
      AllowZoom = False
      BackWall.Brush.Color = clWhite
      BackWall.Brush.Style = bsClear
      BottomWall.Pen.Style = psClear
      MarginBottom = 0
      MarginLeft = 0
      MarginRight = 0
      MarginTop = 0
      Title.Text.Strings = (
        'TChart')
      Title.Visible = False
      BottomAxis.Axis.Width = 1
      BottomAxis.Axis.Visible = False
      BottomAxis.Grid.SmallDots = True
      BottomAxis.LabelsAngle = 90
      BottomAxis.LabelsFont.Charset = ANSI_CHARSET
      BottomAxis.LabelsFont.Color = clBlack
      BottomAxis.LabelsFont.Height = -9
      BottomAxis.LabelsFont.Name = 'Small Fonts'
      BottomAxis.LabelsFont.Style = []
      BottomAxis.LabelStyle = talValue
      BottomAxis.TickLength = 5
      BottomAxis.TicksInner.Visible = False
      Chart3DPercent = 25
      LeftAxis.Axis.Width = 1
      LeftAxis.Axis.Visible = False
      LeftAxis.Grid.Style = psSolid
      LeftAxis.Grid.SmallDots = True
      LeftAxis.LabelsFont.Charset = ANSI_CHARSET
      LeftAxis.LabelsFont.Color = clBlack
      LeftAxis.LabelsFont.Height = -9
      LeftAxis.LabelsFont.Name = 'Small Fonts'
      LeftAxis.LabelsFont.Style = []
      LeftAxis.TickLength = 5
      Legend.Visible = False
      RightAxis.Axis.Visible = False
      TopAxis.Axis.Visible = False
      TopAxis.Labels = False
      TopAxis.Visible = False
      View3D = False
      View3DOptions.Elevation = 360
      View3DOptions.Rotation = 360
      View3DWalls = False
      BevelOuter = bvLowered
      Color = clWhite
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight, akBottom]
      object Series1: TLineSeries
        Marks.ArrowLength = 0
        Marks.BackColor = clWhite
        Marks.Font.Charset = ANSI_CHARSET
        Marks.Font.Color = clBlack
        Marks.Font.Height = -8
        Marks.Font.Name = 'Small Fonts'
        Marks.Font.Style = []
        Marks.Frame.Visible = False
        Marks.Style = smsValue
        Marks.Transparent = True
        Marks.Visible = False
        SeriesColor = clRed
        Pointer.Brush.Color = clRed
        Pointer.HorizSize = 1
        Pointer.InflateMargins = False
        Pointer.Style = psRectangle
        Pointer.VertSize = 1
        Pointer.Visible = False
        XValues.DateTime = False
        XValues.Name = 'X'
        XValues.Multiplier = 1
        XValues.Order = loAscending
        YValues.DateTime = False
        YValues.Name = 'Y'
        YValues.Multiplier = 1
        YValues.Order = loNone
      end
    end
  end
  object GroupBoxValue: TGroupBox
    Left = 8
    Top = 118
    Width = 297
    Height = 50
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 1
    DesignSize = (
      297
      50)
    object LabelValue: TLabel
      Left = 8
      Top = 16
      Width = 30
      Height = 13
      Caption = 'Value:'
    end
    object EditValue: TEdit
      Left = 48
      Top = 16
      Width = 105
      Height = 21
      TabStop = False
      ReadOnly = True
      TabOrder = 1
    end
    object ButtonClose: TButton
      Left = 216
      Top = 16
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Close'
      TabOrder = 0
      OnClick = ButtonCloseClick
    end
  end
end
