unit IsoWizProf;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, TeeProcs, TeEngine, Chart, Series;

type
  TIsoWizProfForm = class(TForm)
    GroupBoxProfile: TGroupBox;
    Chart: TChart;
    Series1: TLineSeries;
    GroupBoxValue: TGroupBox;
    LabelValue: TLabel;
    EditValue: TEdit;
    ButtonClose: TButton;
    procedure FormCreate(Sender: TObject);
    procedure ButtonCloseClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  IsoWizProfForm: TIsoWizProfForm;

implementation

uses IsoWizMain;

{$R *.dfm}

procedure TIsoWizProfForm.FormCreate(Sender: TObject);
begin
        Icon:=IsoWizForm.Icon;

        Left:=IsoWizForm.Left-Width-10;
        Top:=10;
end;

procedure TIsoWizProfForm.ButtonCloseClick(Sender: TObject);
begin
        Close;
end;

end.
