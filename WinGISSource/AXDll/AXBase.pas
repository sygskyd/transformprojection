unit AXBase;

interface

uses
  ComObj, ActiveX, AXDLL_TLB;

type
  TBase = class(TAutoObject, IBase)
  protected
    BaseItem : pointer;
    hIndex   : Integer;
    function Get_Index: Integer; safecall;
    procedure Set_Index(Value: Integer); safecall;
    function Get_ObjectType: Integer; safecall;
    function Get_ObjectStyle: IObjectStyle; safecall;
    procedure Set_ObjectStyle(const Value: IObjectStyle); safecall;
    function Get_OtherStyle: WordBool; safecall;
    function Get_ClipRect: IRect; safecall;
    function Get_GUID: TGUID; safecall;
    procedure Set_GUID(Value: TGUID); safecall;
//    function Get_DBIndex: Integer; safecall;
    { Protected declarations }
  end;

implementation

uses ComServ,am_view,axrect,am_def,axdef,axobjectstyle,sysutils;

function TBase.Get_Index: Integer;
begin
   Result:=-1;
   if BaseItem <> nil then
      Result:=PView(BaseItem)^.Index;
end;

procedure TBase.Set_Index(Value: Integer);
begin
   hIndex:=Value;
end;

function TBase.Get_ObjectType: Integer;
begin
  REsult:=0;
  if BaseITem<>NIL then
     Result:=PView(BaseItem)^.GetObjtype;
end;

function TBase.Get_ObjectStyle: IObjectStyle;
begin
 Result:=NIL;
  if BaseITem<>NIL then begin
    if  PView(BaseITem)^.ObjectStyle = NIL then CreateStyle(PView(BaseItem));
    REsult:=TObjectStyle.Create(PView(BaseiTem)^.ObjectStyle,false);
  end;
end;

procedure TBase.Set_ObjectStyle(const Value: IObjectStyle);
begin

end;

function TBase.Get_OtherStyle: WordBool;
begin
   result:=false;
   If BaseITem<>NIL then
      Result:=PView(BaseItem)^.GetState(sf_Otherstyle);
end;


function TBase.Get_ClipRect: IRect;
begin
  If BaseItem<>NIL then
  REsult:=AXRect.TRect.Create(@PView(BaseItem).ClipRect,false) else REsult:=NIL;
end;

{
function TBase.Get_DBIndex: Integer;
var s : String;
begin
  Result:=-1;
  if BaseItem<>NIL then
  if PView(BaseItem)^.ToolTip<>NIL then begin
    s:=PView(BaseItem)^.ToolTip^;
    Result:=STrToInt(s);
  end;
end; }

function TBase.Get_GUID: TGUID;
begin
     if BaseItem <> nil then begin
        Result:=PView(BaseItem)^.GUID;
     end;
end;

procedure TBase.Set_GUID(Value: TGUID);
begin
     if BaseItem <> nil then begin
        PView(BaseItem)^.GUID:=Value;
     end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TBase, Class_Base,
    ciInternal, tmApartment);
end.
