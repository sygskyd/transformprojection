unit AXLinestyle;

interface

uses
  ComObj, ActiveX, AXDLL_TLB, am_def,WinTypes, graphics, StdVcl;

type
  TLineStyle = class(TAutoObject, ILineStyle)
  private
    //FLineStyle   : am_def.TLIneStyle;
    FLineStyle       : Pointer;
    Release          : boolean;
  protected
    function Get_Color: UINT; safecall;
    function Get_FillColor: UINT; safecall;
    function Get_Scale: Double; safecall;
    function Get_Style: Integer; safecall;
    function Get_Width: Double; safecall;
    function Get_WidthType: Integer; safecall;
    procedure Set_Color(Value: UINT); safecall;
    procedure Set_FillColor(Value: UINT); safecall;
    procedure Set_Scale(Value: Double); safecall;
    procedure Set_Style(Value: Integer); safecall;
    procedure Set_Width(Value: Double); safecall;
    procedure Set_WidthType(Value: Integer); safecall;

    { Protected declarations }
  public
    constructor Create(AItem:Pointer;ARelease:boolean);
    Destructor Destroy; override;
  end;

implementation

uses ComServ;

constructor TLineStyle.Create;
  begin
    inherited CReate;
    FLineStyle:=AItem;
    Release:=ARelease;
    if Release then begin
      getmem(FLIneSTyle,sizeof(am_def.TLineStyle));
    end;
  end;

Destructor TLineSTyle.Destroy;
  begin
    if Release then freemem(FlIneStyle,sizeof(am_def.TLIneStyle));
    inherited Destroy;
  end;

function TLineStyle.Get_Color: UINT;
begin
  Result:=PLineStyle(FLineStyle)^.Color;
end;

function TLineStyle.Get_FillColor: UINT;
begin
  Result:=PLineStyle(FLineStyle)^.FillColor;
end;

function TLineStyle.Get_Scale: Double;
begin
  Result:=PLineStyle(FLineStyle)^.Scale;
end;

function TLineStyle.Get_Style: Integer;
begin
  Result:=PLineStyle(FLineStyle)^.Style;
end;

function TLineStyle.Get_Width: Double;
begin
  Result:=PLineStyle(FLineStyle)^.Width;
end;

function TLineStyle.Get_WidthType: Integer;
begin
  Result:=PLineStyle(FLineStyle)^.WidthType;
end;

procedure TLineStyle.Set_Color(Value: UINT);
begin
  PLineStyle(FLineStyle)^.Color:=Value;
end;

procedure TLineStyle.Set_FillColor(Value: UINT);
begin
  PLineStyle(FLineStyle)^.FillColor:=Value;
end;

procedure TLineStyle.Set_Scale(Value: Double);
begin
  PLineStyle(FLineStyle)^.Scale:=Value;
end;

procedure TLineStyle.Set_Style(Value: Integer);
begin
  PLineStyle(FLineStyle)^.Style:=Value;
end;

procedure TLineStyle.Set_Width(Value: Double);
begin
  PLineStyle(FLineStyle)^.Width:=Value;
end;

procedure TLineStyle.Set_WidthType(Value: Integer);
begin
  PLineStyle(FLineStyle)^.WidthType:=Value;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TLineStyle, Class_LineStyle,
    ciInternal, tmApartment);
end.
