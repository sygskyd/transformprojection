unit AXStringItem;

interface

uses
  ComObj, ActiveX, AXDLL_TLB;

type
  TStringItem = class(TAutoObject, IStringItem)
  protected
    { Protected declarations }
  end;

implementation

uses ComServ;

initialization
  TAutoObjectFactory.Create(ComServer, TStringItem, Class_StringItem,
    ciInternal, tmApartment);
end.
