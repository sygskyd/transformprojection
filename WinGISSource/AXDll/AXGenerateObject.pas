unit AXGenerateObject;

interface

uses
  ComObj, ActiveX, AXDLL_TLB;

const
      // Object types
      ot_Pixel     = 1;
      ot_Poly      = 2;
      ot_CPoly     = 4;
      ot_Text      = 7;
      ot_Symbol    = 8;
      ot_Spline    = 9;
      ot_Image     = 10;
      ot_MesLine   = 11;
      ot_Circle    = 12;
      ot_Arc       = 13;


      tr_BackTrace      = 1;
      ar_Envelope       = 0;
      ar_Lines          = 1;
      ar_Area           = 2;
      ar_Merge          = 3;

type
  TGenerateObject = class(TAutoObject, IGenerateObject)
  private
    FDocument   : IDocument;
  protected
    procedure GenerateObjects(const InputLayers: IList;
      const DestLayer: WideString; Convertion: Integer); safecall;
    function Get_test: Integer; safecall;
    { Protected declarations }
  public
    constructor Create(AItem:IDispatch);
  end;

implementation



uses ComServ,Area,SLBase,sort;


Constructor TGenerateObject.Create(AItem:IDispatch);
begin
  inherited Create;
  FDocument:=AItem as IDocument;
end;

procedure TGenerateObject.GenerateObjects(const InputLayers: IList;
  const DestLayer: WideString; Convertion: Integer);
var AName   : widestring;
    AItem   : Variant;
    i       : integer;
    k       : integer;
    j       : integer;
    cnt     : integer;
    Layers  : ILayers;
    ADisp   : IDispatch;
    Layer   : ILayer;
    AObject : variant;
    ASymbol : ISymbol;
    APoint  : IPoint;

    BDisp   : IDispatch;
    BLayer   : ILayer;
    BObject : Idispatch;
    BPoint  : IPoint;
    BPoly   : IPolygon;
    BItem   : Variant;
    hint    : integer;
    MArea   : TArea;
    APoly   : IPolyline;
    ACPoly  : IPolygon;

    sItem        : TSListItem;
    MSort        : TMergeSort;
    AllAbort     : Boolean;
    aLayer       : ILayer;
begin
  MArea:=TArea.Create(Pointer(FDocument.Project));
  for i:=0 to InputLayers.count-1 do
  begin
     ALayer:=InputLayers.Items[i] as ILayer;
     AName:=ALayer.Layername;
     Layers:=FDocument.Layers;
     ADisp:=Layers.GetLayerByName(AName);
     if ADisp<>NIL then
     begin
        Layer:=ADisp as ILayer;
        for k:=0 to Layer.Count-1 do
        begin
           ADisp:=Layer.Items[k];
           if ADisp<>NIL then
           begin
              AObject:=ADisp;
              if (AObject.ObjectType = ot_poly)or(AObject.ObjectType = ot_cpoly) then
              begin
                 with MArea do
                 begin
                    Alist:=TSLBase.Create;
                    for j:=0 to AObject.Count-1 do
                    begin
                       ADisp:=AObject.POints[j];
                       APOint:=ADisp as IPOint;
                       sItem:=TSListItem.Create;
                       sItem.x:=APoint.x;
                       sItem.y:=APoint.y;
                      AList.Add(sItem);
                    end;
                    ConvertPolyToEdge;
                    AList.Free
                 end;
              end;
           end; //ADisp<>NIL
        end;
     end;  //if ADisp<>NIL
  end;

  MArea.Radius:=Convertion;
  Marea.Data.ERLayer:=false;
  Marea.Data.ISLayer:=false;
  Marea.Data.Frame:=false;
  MArea.Data.INtersections:=true;

  Marea.Data.Trace:=tr_BackTrace;
  MArea.Data.Area:=ar_Area;

  if MArea.Data.ISLayer then begin
    Layer:=Layers.InsertLayerByName('IntersectLayer',true);
    Marea.Data.InterSectLayer:=Layer.Index;
  end;
  if Marea.Data.ERLayer then begin
    Layer:=Layers.InsertLayerByName('ErrorLayer',true);
    Marea.Data.ErrorLayer:=Layer.Index;
  end;

  Layer:=Layers.InsertLayerByName(DestLayer,true);
  Marea.Data.DestLayer:=Layer.Index;

  with MArea do begin
    if Data.Radius=0 then Data.Radius:=Convertion;
    STree.Radius:=Data.Radius;
    Radius := Data.Radius;
    DeltaX := Data.Radius;
    DeltaY := Data.Radius;
    AllAbort:=false;

  if EdgeList.Count>1 then begin
     if Data.Frame then begin
       ProjectSize(ProjSize);
       AddFrame(ProjSize);
     end;
     if not AllAbort then begin
       Msort:=TMergeSort.Create(EdgeList);
       MSort.MergeSort;
       MSort.Free;
     end;
     if not AllAbort then EdgeList.completeList;
     if not AllAbort then EdgeList.Count:=SetListCount(EdgeList);
     if not AllAbort then Snap2;
     if not AllAbort then NodeList.Count:=SetListCount(NodeList);
     if not AllAbort then EleminateEqual;
     if not AllAbort then EleminateDouble;
     if not AllAbort then SortLinkList;
     if not AllAbort then SortList(NodeList);
     if not AllAbort then if Data.Intersections then Intersection;
     if not AllAbort then NodeList.Count:=SetListCount(NodeList);
     if not AllAbort then Snap3;
     FinalList.Count:=SetListCount(FinalList);
     if not AllAbort then EleminateEqual2;
     if not AllAbort then EleminateDouble2;
     if not AllAbort then SortList(FinalList);
     if ((Data.Area = ar_Area)or(Data.Area = ar_Envelope))and(not AllAbort) then begin
       MergeLink;
       EleminateLines;
       EleminateLinesBack;
     end;
     if (Data.Area = ar_Merge)and(not AllAbort) then begin
       MergeLink;
       MergeToPolyLines;
       MergeToPolylInesBack;
     end;
     if not AllAbort then begin
       if (Data.Area = ar_Area)or(Data.Area = ar_Envelope) then BackTrace else
       if Data.Area = ar_Lines then DrawLines;;
     end;
   end;{Edgelist.count > 0 }
  end;
  MArea.Free;
end;


function TGenerateObject.Get_test: Integer;
begin
result:=5;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TGenerateObject, Class_GenerateObject,
    ciInternal, tmApartment);
end.
