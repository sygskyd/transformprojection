unit AXFontDef;

interface

uses
  ComObj, ActiveX, AXDLL_TLB, StdVcl;

type
  TFontDef = class(TAutoObject, IFontDef)
    FFontDef        : Pointer;
    Data            : POinter;
    Release         : Boolean;
  protected
    function Get_CharSet: Word; safecall;
    function Get_Family: Word; safecall;
    function Get_FontType: Integer; safecall;
    function Get_Installed: WordBool; safecall;
    function Get_Name: WideString; safecall;
    function Get_Number: Integer; safecall;
    function Get_TrueType: WordBool; safecall;
    procedure Set_CharSet(Value: Word); safecall;
    procedure Set_Family(Value: Word); safecall;
    procedure Set_FontType(Value: Integer); safecall;
    procedure Set_Name(const Value: WideString); safecall;
    procedure Set_Number(Value: Integer); safecall;
    procedure Set_Installed(Value: WordBool); safecall;
    procedure Set_TrueType(Value: WordBool); safecall;
    { Protected declarations }
  public
    Constructor Create(AData,AItem:POInter;aRelease:Boolean);   //PProj, FontDef
    Destructor Destroy; override;
  end;

implementation

uses ComServ,am_font,sysutils;

Constructor TFontDef.Create;
  begin
    inherited Create;
    FFontDef:=AItem;
    Data:=AData;
    RElease:=ARElease;
  end;

Destructor TFontDef.Destroy;
  begin
    if Release then begin
      Dispose(PFontDes(FFontDef),done);
    end;
    inherited Destroy;
  end;


function TFontDef.Get_CharSet: Word;
begin
  Result:=PFontDes(FFontDef)^.CharSet;
end;

function TFontDef.Get_Family: Word;
begin
  Result:=PFontDes(FFontDef)^.Family;
end;

function TFontDef.Get_FontType: Integer;
begin
  Result:=PFontDes(FFontDef)^.FontType;
end;

function TFontDef.Get_Installed: WordBool;
begin
    Result:=PFontDes(FFontDef)^.Installed;
end;

function TFontDef.Get_Name: WideString;
begin
  Result:=PFontDes(FFontDef)^.Name^;
end;

function TFontDef.Get_Number: Integer;
begin
  Result:=PFontDes(FFontDef)^.FNum;
end;

function TFontDef.Get_TrueType: WordBool;
begin
  Result:=PFontDes(FFontDef)^.isTrueType;
end;

procedure TFontDef.Set_CharSet(Value: Word);
begin
  PFontDes(FFontDef)^.Charset:=Value;
end;

procedure TFontDef.Set_Family(Value: Word);
begin
  PFontDes(FFontDef)^.Family:=Value;
end;

procedure TFontDef.Set_FontType(Value: Integer);
begin
  PFontDes(FFontDef)^.FontType:=Value;
end;

procedure TFontDef.Set_Name(const Value: WideString);
begin
  PFontDes(FFontDef)^.Name:=newSTr(Value);
end;

procedure TFontDef.Set_Number(Value: Integer);
begin
  PFontDes(FFontDef)^.FNum:=Value;
end;

procedure TFontDef.Set_Installed(Value: WordBool);
begin
  PFontDes(FFontDef)^.Installed:=Value;
end;

procedure TFontDef.Set_TrueType(Value: WordBool);
begin
  PFontDes(FFontDef)^.isTrueType:=Value;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TFontDef, Class_FontDef,
    ciInternal, tmApartment);
end.
