unit AXFillStyle;

interface

uses
  ComObj, ActiveX, AXDLL_TLB,WinTypes,am_def, StdVcl;

type
  TFillStyle = class(TAutoObject, IFillStyle)
  private
    //FillStyle     : am_def.TFillStyle;
    FillStyle       : Pointer;
    Release         : boolean;
  protected
    function Get_BackColor: UINT; safecall;
    function Get_ForeColor: UINT; safecall;
    function Get_Pattern: Integer; safecall;
    function Get_Scale: Double; safecall;
    function Get_ScaleType: Integer; safecall;
    procedure Set_BackColor(Value: UINT); safecall;
    procedure Set_ForeColor(Value: UINT); safecall;
    procedure Set_Pattern(Value: Integer); safecall;
    procedure Set_Scale(Value: Double); safecall;
    procedure Set_ScaleType(Value: Integer); safecall;
  
    { Protected declarations }
  public
    constructor Create(AItem:Pointer;ARelease:boolean);
    Destructor Destroy; override;
  end;

implementation

uses ComServ;

constructor TFillStyle.Create;
  begin
    inherited CReate;
    FillStyle:=AItem;
    Release:=ARelease;
    if Release then begin
      getmem(FillSTyle,sizeof(am_def.TFillStyle));
    end;
  end;

Destructor TFillSTyle.Destroy;
  begin
    if Release then freemem(FillStyle,sizeof(am_def.TFillStyle));
    inherited Destroy;
  end;


function TFillStyle.Get_BackColor: UINT;
begin
  Result:=PFillStyle(Fillstyle)^.BackColor;
end;

function TFillStyle.Get_ForeColor: UINT;
begin
  Result:=PFillStyle(FillStyle)^.ForeColor;
end;

function TFillStyle.Get_Pattern: Integer;
begin
  Result:=PFillStyle(FillStyle)^.Pattern;
end;

function TFillStyle.Get_Scale: Double;
begin
  Result:=PFillStyle(FillStyle)^.Scale;
end;

function TFillStyle.Get_ScaleType: Integer;
begin
  Result:=PFillStyle(FillStyle)^.ScaleType;
end;

procedure TFillStyle.Set_BackColor(Value: UINT);
begin
  PFillStyle(FillStyle)^.BAckColor:=Value;
end;

procedure TFillStyle.Set_ForeColor(Value: UINT);
begin
  PFillStyle(FillStyle)^.ForeColor:=Value;
end;

procedure TFillStyle.Set_Pattern(Value: Integer);
begin
  PFillStyle(FillStyle)^.Pattern:=Value;
end;

procedure TFillStyle.Set_Scale(Value: Double);
begin
  PFillStyle(FillStyle)^.Scale:=Value;
end;

procedure TFillStyle.Set_ScaleType(Value: Integer);
begin
  PFillStyle(FillStyle)^.ScaleType:=Value;
end;


initialization
  TAutoObjectFactory.Create(ComServer, TFillStyle, Class_FillStyle,
    ciInternal, tmApartment);
end.
