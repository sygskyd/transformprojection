unit AXPoint;

interface

uses
  ComObj, ActiveX, AXDLL_TLB, StdVcl;

type
  TPoint = class(TAutoObject, IPoint)
  private
    FPoint   : POinter;
    Release : Boolean;
  protected
    function Get_x: Integer; safecall;
    function Get_y: Integer; safecall;
    procedure Set_x(Value: Integer); safecall;
    procedure Set_y(Value: Integer); safecall;
    { Protected declarations }
  public
    constructor Create(APoint:POInter;ARelease:Boolean);
    constructor Create2(x,y :integer);
    Destructor  Destroy; override;
  end;

implementation

uses ComServ,am_def;

Constructor TPOint.Create(APOint:Pointer;ARelease:Boolean);
  begin
    inherited Create;
    FPOint:=APOint;
    Release:=ARelease;
  end;

Destructor TPoint.Destroy;
  begin
    if Release then
    begin
       Dispose(PDPoint(FPoint),Done);
    end;
    inherited Destroy;
  end;

constructor TPoint.Create2(x,y:integer);
begin
  inherited create;
  Release:=true;
  FPoint:=new(PDPoint,Init(x,y));
end;

function TPoint.Get_x: Integer;
begin
  REsult:=-1;
  if FPoint<>NIL then begin
    Result:=PDPOint(FPoint)^.x;
  end;
end;

function TPoint.Get_y: Integer;
begin
  REsult:=-1;
  if Fpoint<>NIL then begin
    Result:=PDPOint(FPoint)^.y;
  end;
end;

procedure TPoint.Set_x(Value: Integer);
begin
  if FPoint<>NIL then begin
    PDPOint(FPoint)^.x:=Value;
  end;
end;

procedure TPoint.Set_y(Value: Integer);
begin
  if FPoint<>NIL then begin
    PDPOint(FPoint)^.y:=Value;
  end;
end;


initialization
  TAutoObjectFactory.Create(ComServer, TPoint, Class_Point,
    ciInternal, tmApartment);
end.
