unit AXCircle;

interface

uses
  ComObj, ActiveX, AXDLL_TLB, StdVcl,AXBase;

type
  TCircle = class(TBase, ICircle)
    FCircle   : POinter;
    Data      : Pointer;
    Release   : Boolean;
    hIndex    : Integer;
  protected
    function Get_ObjectType: Integer; safecall;
    function Get_Angle: Double; safecall;
    function Get_Position: IPoint; safecall;
    function Get_PrimaryAxis: Integer; safecall;
    function Get_SecondaryAxis: Integer; safecall;
    procedure Set_Angle(Value: Double); safecall;
    procedure Set_Position(const Value: IPoint); safecall;
    procedure Set_PrimaryAxis(Value: Integer); safecall;
    procedure Set_SecondaryAxis(Value: Integer); safecall;
    procedure Assign(const Item: ICircle); safecall;
    function Get_Area: Double; safecall;
    function Get_Length: Double; safecall;
    function Get_Radius: Integer; safecall;
    procedure Set_Radius(Value: Integer); safecall;
    function Get_Index: Integer; safecall;
    function Get_ObjectStyle: IObjectStyle; safecall;
    procedure Set_ObjectStyle(const Value: IObjectStyle); safecall;
    function Get_OtherStyle: WordBool; safecall;
    procedure Set_Index(Value: Integer); safecall;
    procedure CreateObjectStyle; safecall;
    function Get_ClipRect: IRect; safecall;
    { Protected declarations }
  Public
    Constructor Create(AData,AItem:POInter;ARelease:Boolean);
    Destructor  Destroy; override;
  end;

implementation

uses ComServ,am_Circl,am_def,AxDef,AXPoint,wintypes,axobjectstyle, AxRect;

Constructor TCircle.Create;
  begin
    inherited Create;
    FCircle:=AItem;
    BaseItem:=AItem;
    Data:=AData;
    Release:=ARelease;
    hIndex:=-1;
  end;

Destructor TCircle.Destroy;
  begin
    if Release Then Dispose(PEllipse(FCircle),Done);
    inherited Destroy;
  end;

function TCircle.Get_ObjectType: Integer;
begin
  Result:=-1;
  if FCircle<>NIL then begin
    REsult:=PEllipse(FCircle)^.GetObjtype;
  end;
end;

function TCircle.Get_Angle: Double;
begin
  Result:=-1;
  if FCircle<>NIL then begin
    Result:=PEllipse(FCircle)^.Angle;
  end;
end;

function TCircle.Get_Position: IPoint;
//var APoint : PDPoint;
begin
  Result:=NIL;
  if FCircle<>NIL then begin
    //APOint:=new(PDPOint,Init(PEllipse(FCircle)^.Position.x,PEllipse(FCircle)^.Position.y));
    //Result:=AXPoint.TPoint.Create(APoint,true);
    Result:=AXPoint.TPoint.Create(@PEllipse(FCircle)^.Position,false);
  end;
end;

function TCircle.Get_PrimaryAxis: Integer;
begin
  Result:=-1;
  if FCircle<>NIL then begin
    Result:=PEllipse(FCircle)^.PrimaryAxis;
  end;
end;

function TCircle.Get_SecondaryAxis: Integer;
begin
  Result:=-1;
  if FCircle<>NIL then begin
    Result:=PEllipse(FCircle)^.SecondaryAxis;
  end;
end;

procedure TCircle.Set_Angle(Value: Double);
begin
  if FCircle<>NIL then begin
    PEllipse(Fcircle)^.Angle:=Value;
  end;
end;

procedure TCircle.Set_Position(const Value: IPoint);
begin
  if FCircle<>NIL then begin
    PEllipse(Fcircle)^.Position.x:=Value.x;
    PEllipse(Fcircle)^.Position.y:=Value.y;
  end;
end;

procedure TCircle.Set_PrimaryAxis(Value: Integer);
begin
  if FCircle<>NIL then begin
    PEllipse(Fcircle)^.PrimaryAxis:=Value;
  end;
end;

procedure TCircle.Set_SecondaryAxis(Value: Integer);
begin
  if FCircle<>NIL then begin
    PEllipse(Fcircle)^.SecondaryAxis:=Value;
  end;
end;



procedure TCircle.Assign(const Item: ICircle);
begin

end;


function TCircle.Get_Area: Double;
begin
  Result:=-1;
  if FCircle<>NIL then begin
    Result:=PEllipse(FCircle)^.Flaeche;
  end;
end;

function TCircle.Get_Length: Double;
begin
  Result:=-1;
  if FCircle<>NIL then begin
    Result:=PEllipse(FCircle)^.Laenge;
  end;
end;

function TCircle.Get_Radius: Integer;
begin
  Result:=-1;
  if FCircle<>NIL then begin
    if PEllipse(FCircle)^.PrimaryAxis=PEllipse(FCircle)^.SecondaryAxis then
    Result:=PEllipse(FCircle)^.PrimaryAxis;
  end;
end;

procedure TCircle.Set_Radius(Value: Integer);
begin
  if FCircle<>NIL then begin
    PEllipse(Fcircle)^.SecondaryAxis:=Value;
    PEllipse(Fcircle)^.PrimaryAxis:=Value;
  end;
end;

function TCircle.Get_Index: Integer;
begin
  Result:=-1;
  if FCircle<>NIL then begin
    if Hindex<>-1 then Result:=Hindex else
    Result:=PEllipse(Fcircle)^.Index;
  end;
end;

function TCircle.Get_ObjectStyle: IObjectStyle;
begin
  Result:=NIL;
  if FCircle<>NIL then begin
    if  PEllipse(FCircle)^.ObjectStyle <> NIL then
       REsult:=TObjectStyle.Create(PEllipse(FCircle)^.ObjectStyle,false);
  end;
end;

procedure TCircle.Set_ObjectStyle(const Value: IObjectStyle);
begin
  if FCircle<>NIL then begin
    SetStyle(PEllipse(FCircle),Value);
  end;
end;



function TCircle.Get_OtherStyle: WordBool;
begin
 if FCircle<>NIL then begin
   REsult:=PEllipse(FCircle)^.Getstate(sf_otherstyle);
 end;
end;

procedure TCircle.Set_Index(Value: Integer);
begin
 if FCircle<>NIL then begin
   hIndex:=Value;
 end;
end;

procedure TCircle.CreateObjectStyle;
begin
   CreateStyle(PEllipse(FCircle));
end;

function TCircle.Get_ClipRect: IRect;
begin
  REsult:=AXRect.TRect.Create(@PEllipse(FCircle).ClipRect,false);
end;



initialization
  TAutoObjectFactory.Create(ComServer, TCircle, Class_Circle,
    ciInternal, tmApartment);
end.
