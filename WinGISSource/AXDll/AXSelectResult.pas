unit AXSelectResult;

interface

uses
  ComObj, ActiveX, AXDLL_TLB;

type
  TSelectResult = class(TAutoObject, ISelectResult)
  private
    FPolyLIne : Pointer;
    x,y : integer;
  protected
    function Get_Poly: IPolyline; safecall;
    function Get_Position: IPoint; safecall;
    { Protected declarations }
  public
    Constructor Create(AX,AY:integer;AItem:Pointer);
  end;

implementation

uses ComServ,axpoint,axPolyLine,am_Def;

constructor TSelectREsult.Create;

begin
  inherited Create;
  FPolyLIne:=AItem;
  x:=ax;
  y:=ay;
end;

function TSelectResult.Get_Poly: IPolyline;
begin
Result:=AXPolyLine.TPolyLine.Create(NIL,FPolyLIne,false);
end;

function TSelectResult.Get_Position: IPoint;
var APoint : PdPOint;
begin
  APOint:=new(PDPOint,Init(x,y));
  Result:=AXPoint.TPoint.Create(APoint,true);
end;

initialization
  TAutoObjectFactory.Create(ComServer, TSelectResult, Class_SelectResult,
    ciInternal, tmApartment);
end.
