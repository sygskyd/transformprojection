unit AXCore;

interface

uses
  ComObj, ActiveX, AxCtrls, AXDLL_TLB, AX_proj, {am_ini,userintf,} StdVcl;

type
  TCore = class(TAutoObject, IConnectionPointContainer, ICore)
  private
    { Private-Deklarationen}
    FConnectionPoints: TConnectionPoints;
    FEvents: ICoreEvents;
    Proj          : TXProj;

  public
    procedure Initialize; override;
  protected
    { Protected-Deklarationen}
    property ConnectionPoints: TConnectionPoints read FConnectionPoints
      implements IConnectionPointContainer;
    procedure EventSinkChanged(const EventSink: IUnknown); override;
    function OpenDocument(const FileName: WideString): IDocument; safecall;
    function CreateDocument(const FileName: WideString): IDocument; safecall;
    procedure CloseDocument(const FileName: WideString); safecall;
    function CreateDocumentInt(ADoc: Integer): OleVariant; safecall;
    function GetActiveDocument: IDocument; safecall;
    procedure ExecDDE(const DDEString: WideString); safecall;
    procedure Terminate; safecall;
    procedure ExecMenu(const ItemName: WideString); safecall;
  end;

implementation

uses ComServ,am_Main, sysutils, AXDocument,am_Proj, classes;

procedure TCore.EventSinkChanged(const EventSink: IUnknown);
begin
  FEvents := EventSink as ICoreEvents;
end;

procedure TCore.Initialize;
begin
  inherited Initialize;
  FConnectionPoints := TConnectionPoints.Create(Self);
  if AutoFactory.EventTypeInfo <> nil then
    FConnectionPoints.CreateConnectionPoint(AutoFactory.EventIID,
      ckSingle, EventConnect);
  if WinGISMainForm = nil then
     WinGISMainForm:=TWinGISMainForm.Create(nil);
end;

function TCore.OpenDocument(const FileName: WideString): IDocument;
var hName  : array[0..255] of char;
begin
  REsult:=NIL;
  Proj:=TXProj.Create(NIL,FileName);
  if Proj.Load then Result:=TDocument.create(Proj) else Proj.free;    //TXProj
end;

{function TClore.GetSSPPassword(const FileName: WideString): WideString;
var APassWd:widestring;
begin
   APassWd:='';
   Proj:=TXProj.Create(NIL,Filename);
   if Proj.Load
   Proj.free;
   result:=APassWd;
end; }


function TCore.CreateDocument(const FileName: WideString): IDocument;
var hName  : array[0..255] of char;
begin
  Proj:=TXProj.Create(NIL,FileName);
  Proj.OnNew;
  Result:=TDocument.create(Proj);    //TXProj
end;

procedure TCore.CloseDocument(const FileName: WideString);
var hName  : array[0..255] of char;
begin
  strpCopy(hName,FileName);
  WingisMainForm.FileNameClose(hName);
end;

function TCore.CreateDocumentInt(ADoc: Integer): OleVariant;
var ADisp : IDispatch;
begin
  Proj:=TXProj.Create(NIL,'');
  Dispose(Proj.PINfo,done);
  Proj.Pinfo:=PProj(ADoc)^.PInfo;
  Proj.Data:=PProj(ADoc);
  ADisp:=TDocument.create(Proj);
  Result:=ADisp;
end;

function TCore.GetActiveDocument: IDocument;
begin
   // dummy implementation
end;

procedure TCore.ExecDDE(const DDEString: WideString);
begin
   // dummy implementation
end;

procedure TCore.Terminate;
begin
   // dummy implementation
end;

procedure TCore.ExecMenu(const ItemName: WideString);
begin
   // dummy implementation
end;

initialization
  TAutoObjectFactory.Create(ComServer, TCore, Class_Core,
    ciMultiInstance, tmApartment);
    //IniFile:=New(PIniFile,Init(NIL));
    //UserInterface:=NIL;

finalization
end.



//ToDoList

//axdef ... copyObject ... STate kopieren.
