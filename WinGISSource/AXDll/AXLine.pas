unit AXLine;

interface

uses
  ComObj, ActiveX, AXDLL_TLB;


type
  PDouble = ^double;
  TLine = class(TAutoObject, ILine)
  private
//    x1,y1,x2,y2 : double;
    px1,py1,px2,py2 : Pdouble;
    Release     : boolean;
  protected
    function Get_A: IDPoint; safecall;
    procedure Set_A(const Value: IDPoint); safecall;
    function Get_B: IDPoint; safecall;
    function Get_length: Double; safecall;
    function Get_x1: Double; safecall;
    function Get_x2: Double; safecall;
    function Get_y1: Double; safecall;
    function Get_y2: Double; safecall;
    procedure Set_B(const Value: IDPoint); safecall;
    procedure Set_x1(Value: Double); safecall;
    procedure Set_x2(Value: Double); safecall;
    procedure Set_y1(Value: Double); safecall;
    procedure Set_y2(Value: Double); safecall;
    { Protected declarations }
  public
    Constructor Create(apx1,apy1,apx2,apy2:Pdouble;ARelease:Boolean);
    Destructor Destroy; override;
  end;

implementation

uses ComServ,AXDPoint;

constructor TLine.create;
begin
  inherited create;
  px1:=apx1;
  py1:=apy1;
  px2:=apx2;
  py2:=apy2;
  Release:=ARelease;
  if Release then begin
    getmem(px1,sizeof(double));
    getmem(py1,sizeof(double));
    getmem(px2,sizeof(double));
    getmem(py2,sizeof(double));
  end;
end;

Destructor TLine.Destroy;
begin
  if Release then begin
    freemem(px1,sizeof(double));
    freemem(py1,sizeof(double));
    freemem(px2,sizeof(double));
    freemem(py2,sizeof(double));
  end;
  inherited Destroy;
end;

function TLine.Get_A: IDPoint;
begin
  Result:=AXDPoint.TDpoint.Create(px1^,py1^);
end;

procedure TLine.Set_A(const Value: IDPoint);
begin
  px1^:=Value.x;
  py1^:=Value.y
end;

function TLine.Get_B: IDPoint;
begin
  Result:=AXDpoint.TDPoint.Create(px2^,py2^);
end;

function TLine.Get_length: Double;
begin
  result:=sqrt(abs((py2^-py1^)*(py2^-py1^)-(px2^-px1^)*(px2^-px1^)));
end;

function TLine.Get_x1: Double;
begin
  Result:=px1^;
end;

function TLine.Get_x2: Double;
begin
  Result:=px2^;
end;

function TLine.Get_y1: Double;
begin
  Result:=py1^;
end;

function TLine.Get_y2: Double;
begin
  Result:=py2^;
end;

procedure TLine.Set_B(const Value: IDPoint);
begin
  px2^:=Value.x;
  py2^:=Value.y
end;

procedure TLine.Set_x1(Value: Double);
begin
  px1^:=value;
end;

procedure TLine.Set_x2(Value: Double);
begin
  px2^:=value;
end;

procedure TLine.Set_y1(Value: Double);
begin
  py1^:=value;
end;

procedure TLine.Set_y2(Value: Double);
begin
  py2^:=value;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TLine, Class_Line,
    ciInternal, tmApartment);
end.
