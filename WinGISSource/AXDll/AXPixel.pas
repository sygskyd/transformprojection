unit AXPixel;

interface

uses
  ComObj, ActiveX, AXDLL_TLB, StdVcl,AXBase;

type
  TPixel = class(TBase, IPixel)
  private
    FPixel  : POinter;
    Data    : Pointer;
    Release : Boolean;
    hIndex   : Integer;
  protected
    function Get_ObjectType: Integer; safecall;
    function Get_Position: IPoint; safecall;
    procedure Set_Position(const Value: IPoint); safecall;
    function Get_OtherStyle: WordBool; safecall;
    function Get_ObjectStyle: IObjectStyle; safecall;
    procedure Set_ObjectStyle(const Value: IObjectStyle); safecall;
    function Get_Index: Integer; safecall;
    procedure Set_Index(Value: Integer); safecall;
    procedure CreateObjectStyle; safecall;
    function Get_ClipRect: IRect; safecall;
    procedure MoveAndRedraw(NewX, NewY: Integer); safecall;
    procedure Redraw; safecall;
    procedure FollowObject; safecall;
    procedure ZoomToObject; safecall;
    { Protected declarations }
  Public
    Constructor Create(AData,AItem:POInter;ARelease:Boolean);
    Destructor  Destroy; override;
  end;

implementation

uses ComServ,am_Point,AXPoint,am_def,axdef,axobjectstyle, AxRect, am_Proj, am_Projp;

Constructor TPixel.Create(AData,AItem:POinter;ARelease:Boolean);
  begin
    inherited Create;
    FPixel:=AItem;
    BaseItem:=AItem;
    Data:=AData;
    Release:=ARelease;
    hIndex:=-1;
  end;

Destructor TPixel.Destroy;
  begin
    if Release Then
    begin
       Dispose(PPixel(FPixel),Done);
    end;
    inherited Destroy;
  end;


function TPixel.Get_ObjectType: Integer;
begin
  Result:=-1;
  if FPixel<>NIL then begin
    REsult:=PPixel(FPixel)^.GetObjtype;
  end;
end;

function TPixel.Get_Position: IPoint;
begin
  Result:=NIL;
  If FPixel<>NIL then begin
    Result:=AXPoint.TPoint.Create(@PPixel(FPixel).Position,false);
  end;
end;

procedure TPixel.Set_Position(const Value: IPoint);
begin
  if FPixel<>NIL then begin
    PPixel(FPixel)^.Position.x:=Value.x;
    PPixel(FPixel)^.Position.y:=Value.y;
  end;
end;


function TPixel.Get_OtherStyle: WordBool;
begin
  REsult:=false;
  if FPixel<>NIL then begin
    Result:=PPixel(Fpixel)^.Getstate(sf_otherstyle);
  end;
end;

function TPixel.Get_ObjectStyle: IObjectStyle;
begin
  Result:=NIL;
  if FPixel<>NIL then begin
    if  PPixel(FPixel)^.ObjectStyle <> NIL then
       REsult:=TObjectStyle.Create(PPixel(FPixel)^.ObjectStyle,false);
  end;
end;

procedure TPixel.Set_ObjectStyle(const Value: IObjectStyle);
begin
  if FPixel<>NIL then begin
    SetStyle(PPixel(FPixel),Value);
  end;
end;

function TPixel.Get_Index: Integer;
begin
 Result:=-1;
  if FPixel<>NIL then begin
    if Hindex<>-1 then result:=HIndex else
    Result:=PPixel(FPixel)^.Index;
  end;
end;

procedure TPixel.Set_Index(Value: Integer);
begin
 if FPixel<>NIL then begin
   //PPixel(FPixel)^.Index:=Value;
   HIndex:=Value;
 end;
end;

procedure TPixel.CreateObjectStyle;
begin
   CreateStyle(PPixel(FPixel));
end;

function TPixel.Get_ClipRect: IRect;
begin
  Result:=AXRect.TRect.Create(@PPixel(FPixel).ClipRect,false);
end;

// method is used to move the object to the new position
// and redraw it there
procedure TPixel.MoveAndRedraw(NewX, NewY: Integer);
begin
   PPixel(FPixel)^.Invalidate(PProj(Data)^.PInfo);
   PPixel(FPixel)^.Position.X:=NewX;
   PPixel(FPixel)^.Position.Y:=NewY;
   PPixel(FPixel)^.CalculateClipRect;
   PPixel(FPixel)^.Invalidate(PProj(Data)^.PInfo);
   PProj(Data)^.UpdateClipRect(PPixel(FPixel));
   PProj(Data)^.CorrectSize(PPixel(FPixel)^.ClipRect,TRUE);
   PProj(Data)^.PInfo^.RedrawInvalidate;
end;

// method to redraw pixel object
procedure TPixel.Redraw;
begin
   PPixel(FPixel)^.Invalidate(PProj(Data)^.PInfo);
   PPixel(FPixel)^.CalculateClipRect;
   PPixel(FPixel)^.Invalidate(PProj(Data)^.PInfo);
   PProj(Data)^.UpdateClipRect(PPixel(FPixel));
   PProj(Data)^.CorrectSize(PPixel(FPixel)^.ClipRect,TRUE);
   PProj(Data)^.PInfo^.RedrawInvalidate;
end;

// method is used to make shure that object is on current screen
procedure TPixel.FollowObject;
var CurrentView : TDRect;
begin
   PProj(Data)^.PInfo^.GetCurrentScreen(CurrentView);
   if ((PPixel(FPixel)^.Position.X <= CurrentView.A.X) or (PPixel(FPixel)^.Position.X >= CurrentView.B.X) or
       (PPixel(FPixel)^.Position.Y <= CurrentView.A.Y) or (PPixel(FPixel)^.Position.Y >= CurrentView.B.Y)) then
   begin
      // the object is outside of the current screen
      // so it has to be rezoomed

      // in AXDLL not possible, but in internal DX
      // FuncZoomToObject(PProj(Data),PPixel(FPixel)^.Index);
   end;
end;

// method is used to zoom to pixel
procedure TPixel.ZoomToObject;
begin
   // in AXDLL not possible, but in internal DX
   // FuncZoomToObject(PProj(Data),PPixel(FPixel)^.Index);
end;

initialization
  TAutoObjectFactory.Create(ComServer, TPixel, Class_Pixel,
    ciInternal, tmApartment);
end.
