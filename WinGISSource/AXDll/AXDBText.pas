unit AXDBText;

interface

uses
  ComObj, ActiveX, AXDLL_TLB, StdVcl,AXBase;

type
  TDBText = class(TBase, IDBText)
    FDBText   : POinter;
    Data      : Pointer;
    Release   : Boolean;
    hIndex    : Integer;
  protected
    function Get_ObjectType: Integer; safecall;
    function Get_ObjectStyle: IObjectStyle; safecall;
    procedure Set_ObjectStyle(const Value: IObjectStyle); safecall;
    function Get_Index: Integer; safecall;
    procedure Set_Index(Value: Integer); safecall;
    function Get_TextRect: IRect; safecall;
    procedure Set_TextRect(const Value: IRect); safecall;
    function Get_FontStyle: Integer; safecall;
    procedure Set_FontStyle(Value: Integer); safecall;
    function Get_FontHeight: Integer; safecall;
    procedure Set_FontHeight(Value: Integer); safecall;
    function Get_FontName: WideString; safecall;
    procedure Set_FontName(const Value: WideString); safecall;
    function Get_RText: WideString; safecall;
    procedure Set_RText(const Value: WideString); safecall;
    function Get_ID: Integer; safecall;
    procedure Set_ID(Value: Integer); safecall;
    procedure CreateObjectStyle; safecall;
    { Protected declarations }
  Public
    Constructor Create(AData,AItem:POInter;ARelease:Boolean);
    Destructor  Destroy; override;
  end;

implementation

uses ComServ,am_RText,axdef,axobjectstyle, AxRect, am_def, am_Proj,am_font, SysUtils;

Constructor TDBText.Create;
  begin
    inherited Create;
    FDBTExt:=AItem;
    Data:=AData;
    Release:=ARelease;
    BaseItem:=AItem;
    hIndex:=-1;
  end;

Destructor TDBText.Destroy;
  begin
    if Release Then Dispose(PRText(FDBText),Done);
    inherited Destroy;
  end;

function TDBText.Get_ObjectType: Integer;
begin
  Result:=-1;
  if FDBText<>NIL then begin
    REsult:=PRText(FDBText)^.GetObjtype;
  end;
end;

function TDBText.Get_ObjectStyle: IObjectStyle;
begin
  Result:=NIL;
  if FDBText<>NIL then begin
    if  PRText(FDBText)^.ObjectStyle = NIL then CreateStyle(PRText(FDBText));
    REsult:=TObjectStyle.Create(PRText(FDBText)^.ObjectStyle,false);
  end;
end;

procedure TDBText.Set_ObjectStyle(const Value: IObjectStyle);
begin
  if FDBText<>NIL then begin
    SetStyle(PRText(FDBText),Value);
  end;
end;

function TDBText.Get_Index: Integer;
begin
 Result:=-1;
  if FDBText<>NIL then begin
    if HIndex<>-1 then Result:=HIndex else
    Result:=PRText(FDBText)^.Index;
  end;
end;

procedure TDBText.Set_Index(Value: Integer);
begin
   if FDBText<>NIL then
   begin
      HIndex:=Value;
   end;
end;

function TDBText.Get_TextRect: IRect;
begin
   result:=NIL;
   if FDBText <> NIL then
   begin
      result:=AXRect.TRect.Create(@PRText(FDBText).TextRect,false);
   end;
end;

procedure TDBText.Set_TextRect(const Value: IRect);
begin
   if FDBText <> NIL then
   begin
      PRText(FDBText)^.TextRect.A.X:=Value.A.X;
      PRText(FDBText)^.TextRect.A.Y:=Value.A.Y;
      PRText(FDBText)^.TextRect.B.X:=Value.B.X;
      PRText(FDBText)^.TextRect.B.Y:=Value.B.Y;
   end;
end;

function TDBText.Get_FontStyle: Integer;
begin
   Result:=-1;
   if FDBText <>NIL then
   begin
      Result:=PRText(FDBText)^.Font.Style;
   end;
end;

procedure TDBText.Set_FontStyle(Value: Integer);
begin
   if FDBText<>NIL then
   begin
      PRText(FDBText)^.Font.Style:=Integer16(Value);
   end;
end;

function TDBText.Get_FontHeight: Integer;
begin
   result:=-1;
   if FDBText <>NIL then
   begin
      Result:=PRText(FDBText)^.Font.Height;
   end;
end;

procedure TDBText.Set_FontHeight(Value: Integer);
begin
   if FDBText<>NIL then
   begin
      PRText(FDBText)^.Font.Height:=Value;
   end;
end;

function TDBText.Get_FontName: WideString;
var FontDes  : PFontDes;
begin
   result:='not defined';
   if FDBText<>NIl then
   begin
      FontDes:=PProj(Data)^.Pinfo^.Fonts^.GetFont(PRText(FDBText)^.Font.Font);
      result:=FontDes^.Name^;
   end;
end;

procedure TDBText.Set_FontName(const Value: WideString);
begin
   if FDBText<>NIL then
   begin
      PRText(FDBText)^.Font.Font:=PProj(Data)^.Pinfo^.Fonts^.IDFromName(Value);
   end;
end;

function TDBText.Get_RText: WideString;
begin
   result:='';
   if FDBText<>NIL then
   begin
      result:=strpas(PRText(FDBText)^.RText);
   end;
end;

procedure TDBText.Set_RText(const Value: WideString);
var dummy:Pchar;
begin
   if FDBText<>NIL then
   begin
      dummy:=stralloc(length(Value)+1);
      strpcopy(dummy, Value);
      PRText(FDBText)^.RText:=StrNew(dummy);
      strdispose(dummy);
   end;
end;

function TDBText.Get_ID: Integer;
begin
   result:=-1;
   if FDBText<>NIL then
   begin
      result:=PRText(FDBText)^.ID;
   end;
end;

procedure TDBText.Set_ID(Value: Integer);
begin
   if FDBText<>NIL then
   begin
      PRText(FDBText)^.ID:=Value;
   end;
end;

procedure TDBText.CreateObjectStyle;
begin
   CreateStyle(PRText(FDBText));
end;

initialization
  TAutoObjectFactory.Create(ComServer, TDBText, Class_DBText,
    ciInternal, tmApartment);
end.
