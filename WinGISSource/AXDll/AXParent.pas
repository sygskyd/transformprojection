unit AXParent;

interface

uses
  ComObj, ActiveX, AXDLL_TLB, StdVcl;

type
  TParent = class(TAutoObject, IParent)
  protected
    function Get_ObjectType: Integer; safecall;
    { Protected declarations }
  end;

implementation

uses ComServ;

function TParent.Get_ObjectType: Integer;
begin

end;

initialization
  TAutoObjectFactory.Create(ComServer, TParent, Class_Parent,
    ciInternal, tmApartment);
end.
