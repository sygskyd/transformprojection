unit AXSymbolDef;

interface

uses
  ComObj, ActiveX, AXDLL_TLB, StdVcl,AXBase;

type
  TSymbolDef = class(TBase, ISymbolDef)
   private
    FGroup       : Pointer;
    Data         : Pointer;
    Release      : Boolean;
    hIndex       : Integer;
  protected
    function Get_Count: Integer; safecall;
    function Get_Items(Index: Integer): IBase; safecall;
    function Get_DefaultAngle: Double; safecall;
    function Get_Defaultsize: Double; safecall;
    function Get_DefaultSizeType: Integer; safecall;
    function Get_Index: Integer; safecall;
    function Get_LibName: WideString; safecall;
    function Get_Name: WideString; safecall;
    //function Get_RefLineLength: Double; safecall;
    //function Get_RefLinePoint1: IDPoint; safecall;
    function Get_RefPoint: IPoint; safecall;
    procedure InsertSymbolItem(const AItem: IBase); safecall;
    procedure Set_DefaultAngle(Value: Double); safecall;
    procedure Set_Defaultsize(Value: Double); safecall;
    procedure Set_DefaultSizeType(Value: Integer); safecall;
    procedure Set_Index(Value: Integer); safecall;
    procedure Set_LibName(const Value: WideString); safecall;
    procedure Set_Name(const Value: WideString); safecall;
    //procedure Set_RefLineLength(Value: Double); safecall;
    //procedure Set_RefLinePoint1(const Value: IDPoint); safecall;
    procedure Set_RefPoint(const Value: IPoint); safecall;
    function Get_RefLine: ILine; safecall;
    procedure Set_RefLine(const Value: ILine); safecall;
    //function Get_RefLinePoint2: IDPoint; safecall;
    //procedure Set_RefLinePoint2(const Value: IDPoint); safecall;
    { Protected declarations }
  public
    Constructor Create(AData,AItem:POInter;ARelease:Boolean);
    Destructor  Destroy; override;
  end;

implementation

uses ComServ, am_group, am_view, am_def,axPolygon,AXPolyLine,AXPixel,AXText,
     AXCircle,AXArc,AXSpline,AXPoint,AXDPoint,am_proj,axdef,am_Poly,am_Cpoly,am_Obj,
     AXLine;

Constructor TSymbolDef.Create;
  begin
    inherited Create;
    FGroup:=AItem;
    Data:=AData;
    Release:=ARelease;
    hIndex:=-1;
    BAseItem:=AItem;
  end;

Destructor TSymbolDef.Destroy;
  begin
    if Release Then
    begin
      Dispose(PSGroup(FGroup),Done);
    end;
    inherited Destroy;
  end;


function TSymbolDef.Get_Count: Integer;
begin
  Result:=-1;
  if FGroup<>NIL then begin
    Result:=PSGroup(FGroup)^.Data^.Count;
  end;
end;

function TSymbolDef.Get_Items(Index: Integer): IBase;
var AView : PView;
    BView : IBase;
    Atype : Word;
begin
  Result:=nil;
  if FGroup<>NIL then begin
    AView:=PSGroup(FGroup)^.Data^.At(Index);
    AType:=AView^.GetObjType;
    if AType = ot_poly then begin
      Bview:=TPolyLine.Create(Data,AView,false);
      Result:=BView;
    end else
    if AType = ot_CPoly then begin
      Bview:=TPolygon.Create(Data,AView,false);
      Result:=BView;
    end else
    if Atype = ot_Pixel then begin
      Bview:=TPixel.Create(Data,AView,false);
      Result:=BView;
    end else
    if AType = ot_Circle then begin
      Bview:=TCircle.Create(Data,AView,false);
      Result:=BView;
    end else
    if AType = ot_Arc then begin
      Bview:=TArc.Create(Data,AView,false);
      Result:=BView;
    end else
    if AType = ot_Text then begin
      Bview:=TText.Create(Data,AView,false);
      Result:=BView;
    end else
    if AType = ot_Spline then begin
      Bview:=TSpline.Create(Data,AView,false);
      Result:=BView;
    end;
  end;
end;


function TSymbolDef.Get_DefaultAngle: Double;
begin
  Result:=-1;
  if FGroup<>NIL then begin
    Result:=PSGroup(FGroup)^.DefaultAngle;
  end;
end;

function TSymbolDef.Get_Defaultsize: Double;
begin
  Result:=-1;
  if FGroup<>NIL then begin
    Result:=PSGroup(FGroup)^.DefaultSize;
  end;
end;

function TSymbolDef.Get_DefaultSizeType: Integer;
begin
  Result:=-1;
  if FGroup<>NIL then begin
    Result:=PSGroup(FGroup)^.DefaultSizeType;
  end;
end;

function TSymbolDef.Get_LibName: WideString;
begin
  Result:='';
  if FGroup<>NIL then begin
    if PSGroup(FGroup)^.LibName <> NIL then
      Result:=PSGroup(FGroup)^.LibName^;
  end;
end;

function TSymbolDef.Get_Name: WideString;
begin
  Result:='';
  if FGroup<>NIL then begin
    if PSGroup(FGroup)^.Name <> NIL then
      Result:=PSGroup(FGroup)^.Name^;
  end;
end;

{
function TSymbolDef.Get_RefLineLength: Double;
begin
  Result:=-1;
  if FGroup<>NIL then begin
    Result:=PSGroup(FGroup)^.RefLineLength;
  end;
end;

function TSymbolDef.Get_RefLinePoint1: IDPoint;
begin
  Result:=NIL;
  if FGroup<>NIL then begin
    Result:=AXDPoint.TDPoint.Create;
    Result.X:=PSGroup(FGroup)^.RefLinePoint1.x;
    Result.y:=PSGroup(FGroup)^.RefLinePoint1.y;
  end;
end;

function TSymbolDef.Get_RefLinePoint2: IDPoint;
begin
  Result:=NIL;
  if FGroup<>NIL then begin
    Result:=AXDPoint.TDPoint.Create;
    Result.X:=PSGroup(FGroup)^.RefLinePoint2.x;
    Result.y:=PSGroup(FGroup)^.RefLinePoint2.y;
  end;
end;
 }
function TSymbolDef.Get_RefPoint: IPoint;
begin
  Result:=NIL;
  if FGroup<>NIL then begin
    Result:=AXPoint.TPoint.Create(@PSGroup(FGroup)^.RefPoint,false);
  end;
end;

procedure TSymbolDef.Set_DefaultAngle(Value: Double);
begin
  if FGroup<>NIL then begin
    PSGroup(FGroup)^.DefaultAngle:=Value;
  end;
end;

procedure TSymbolDef.Set_Defaultsize(Value: Double);
begin
  if FGroup<>NIL then begin
    PSGroup(FGroup)^.DefaultSize:=Value;
  end;
end;

procedure TSymbolDef.Set_DefaultSizeType(Value: Integer);
begin
  if FGroup<>NIL then begin
    PSGroup(FGroup)^.DefaultSizeType:=Value;
    //PSGroup(FGroup)^.InEditor:=true;
  end;
end;

procedure TSymbolDef.Set_LibName(const Value: WideString);
begin
  if FGroup<>NIL then begin
    PSGroup(FGroup)^.SetLIbName(Value);
  end;
end;

procedure TSymbolDef.Set_Name(const Value: WideString);
begin
  if FGroup<>NIL then begin
    PSGroup(FGroup)^.SetName(Value);
  end;
end;
                                      {
procedure TSymbolDef.Set_RefLineLength(Value: Double);
begin
  if FGroup<>NIL then begin
    PSGroup(FGroup)^.RefLineLength:=Value;
  end;
end;

procedure TSymbolDef.Set_RefLinePoint1(const Value: IDPoint);
begin
  if FGroup<>NIL then begin
    PSGroup(FGroup)^.RefLinePoint1.x:=Value.x;
    PSGroup(FGroup)^.RefLinePoint1.y:=Value.y;
  end;
end;

procedure TSymbolDef.Set_RefLinePoint2(const Value: IDPoint);
begin
 if FGroup<>NIL then begin
    PSGroup(FGroup)^.RefLinePoint2.x:=Value.x;
    PSGroup(FGroup)^.RefLinePoint2.y:=Value.y;
  end;
end;
                                       }
procedure TSymbolDef.Set_RefPoint(const Value: IPoint);
begin
 if FGroup<>NIL then begin
    PSGroup(FGroup)^.RefPoint.x:=Value.x;
    PSGroup(FGroup)^.RefPoint.y:=Value.y;
  end;
end;

procedure TSymbolDef.InsertSymbolItem(const AItem: IBase);
var
    AView         : PView;
    a             : Integer;

begin
  if FGroup<>NIl then begin
    Aview:=CopySymbolObject(AItem);
    if AView<>NIL then begin
       try
          PSGroup(FGroup)^.InsertItem(AView);
       except
       end;
    end
  end;
end;

{
procedure TSymbolDef.InsertSymbol;
begin
  if FGroup <> NIL then begin
    PSymbols(PProj(Data)^.Pinfo^.Symbols)^.InsertLibSymbol(PProj(Data)^.Pinfo,'',PSGroup(FGroup),false);
  end;
end;
}
function TSymbolDef.Get_Index: Integer;
begin
  Result:=-1;
  if FGroup<>NIL then begin
    if HIndex<>-1 then result:=HIndex else
    REsult:=PSGroup(FGroup)^.Index;
  end;
end;

procedure TSymbolDef.Set_Index(Value: Integer);
begin
  if FGroup<>NIL then begin
    HIndex:=Value;
    //PSGroup(FGroup)^.Index:=Value;
  end;
end;

function TSymbolDef.Get_RefLine: ILine;
begin
  if FGroup<>NIL then begin
    //PSGroup(FGroup)^.
    Result:=TLine.create(
      @PSGroup(FGroup)^.RefLInePoint1.x,
      @PSGroup(FGroup)^.RefLInePoint1.y,
      @PSGroup(FGroup)^.RefLInePoint2.x,
      @PSGroup(FGroup)^.RefLInePoint2.y,false);
  end;
end;

procedure TSymbolDef.Set_RefLine(const Value: ILine);
begin
  if FGroup<>NIL then begin
    PSGroup(FGroup)^.RefLInePoint1.x:=Value.x1;
    PSGroup(FGroup)^.RefLInePoint1.y:=Value.y1;
    PSGroup(FGroup)^.RefLInePoint2.x:=Value.x2;
    PSGroup(FGroup)^.RefLInePoint2.y:=Value.y2;
  end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TSymbolDef, Class_SymbolDef,
    ciInternal, tmApartment);
end.
