unit AXObjectstyle;

interface

uses
  ComObj, ActiveX, AXDLL_TLB,am_Index;

type
  TObjectStyle = class(TAutoObject, IObjectStyle)
    ObjectStyle   : PObjStyleV2;
    Release       : boolean;
  protected
    function Get_FillStyle: IFillStyle; safecall;
    function Get_LineStyle: ILineStyle; safecall;
    function Get_SymbolFill: ISymbolFill; safecall;
    procedure Set_FillStyle(const Value: IFillStyle); safecall;
    procedure Set_LineStyle(const Value: ILineStyle); safecall;
    procedure Set_SymbolFill(const Value: ISymbolFill); safecall;
    { Protected declarations }
  public
    constructor Create(AItem:Pointer;ARelease:boolean);
    Destructor Destroy; override;
  end;

implementation

uses ComServ,axdef,axlinestyle,axfillstyle,axsymbolfill;

constructor TObjectStyle.Create;
  begin
    inherited CReate;
    ObjectStyle:=AItem;
    Release:=ARelease;
    if Release then begin
      getmem(ObjectSTyle,sizeof(am_index.TObjStyleV2));
    end;
  end;

Destructor TObjectSTyle.Destroy;
  begin
    if Release then freemem(ObjectStyle,sizeof(am_index.TObjStyleV2));
    inherited Destroy;
  end;

function TObjectStyle.Get_FillStyle: IFillStyle;
begin
  Result:=AXFillStyle.TFillStyle.Create(@ObjectStyle^.FillStyle,false);
  //SetIFillStyle(ObjectStyle.FillStyle,Result);
end;

function TObjectStyle.Get_LineStyle: ILineStyle;
begin
  Result:=AXLineStyle.TLineStyle.Create(@ObjectSTyle^.LineSTyle,false);
  //SetILineStyle(ObjectSTyle.LineSTyle,Result);
end;

function TObjectStyle.Get_SymbolFill: ISymbolFill;
begin
  REsult:=AXSymbolFill.TSymbolFill.Create;
  SetISymbolFill(ObjectStyle^.SymbolFill,REsult);
end;

procedure TObjectStyle.Set_FillStyle(const Value: IFillStyle);
begin
  SetFillStyle(ObjectSTyle.FillStyle,Value);
end;

procedure TObjectStyle.Set_LineStyle(const Value: ILineStyle);
begin
  SetLineStyle(ObjectStyle.LineStyle,Value);
end;

procedure TObjectStyle.Set_SymbolFill(const Value: ISymbolFill);
begin
  SetSymbolFill(ObjectStyle.SymbolFill,Value);
end;

initialization
  TAutoObjectFactory.Create(ComServer, TObjectStyle, Class_ObjectStyle,
    ciInternal, tmApartment);
end.
