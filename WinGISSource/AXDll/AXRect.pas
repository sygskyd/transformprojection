unit AXRect;

interface

uses
  ComObj, ActiveX, AXDLL_TLB, StdVcl;

type
  TRect = class(TAutoObject, IRect)
  private
    FRect     : POinter;
    Release   : Boolean;
    hIndex    : Integer;
  protected
    function Get_A: IPoint; safecall;
    function Get_B: IPoint; safecall;
    procedure Set_A(const Value: IPoint); safecall;
    procedure Set_B(const Value: IPoint); safecall;
    function Get_height: Integer; safecall;
    function Get_width: Integer; safecall;
    function Get_x1: Integer; safecall;
    function Get_x2: Integer; safecall;
    function Get_y1: Integer; safecall;
    function Get_y2: Integer; safecall;
    procedure Set_x1(Value: Integer); safecall;
    procedure Set_x2(Value: Integer); safecall;
    procedure Set_y1(Value: Integer); safecall;
    procedure Set_y2(Value: Integer); safecall;
    procedure Init(x1, y1, x2, y2: Integer); safecall;
    { Protected declarations }
  public
    constructor Create(AItem:Pointer;ARelease:boolean);
    Destructor Destroy; override;
  end;

implementation

uses ComServ,am_def,AxPoint;

constructor TRect.Create;
  begin
    inherited CReate;
    FRect:=AItem;
    Release:=ARelease;
    if Release then begin
      FRect:=new(PDRect,init);
    end;
  end;

Destructor TRect.Destroy;
  begin
    if Release then Dispose(PDRect(FRect),Done);
    inherited Destroy;
  end;



function TRect.Get_A: IPoint;
var APoint : PDPOint;
begin
  APOint:=new(PDPOint,Init(PDRect(FRect)^.A.x,PDRect(FRect)^.A.y));
  Result:=AXPoint.TPoint.Create(APoint,true);
end;

function TRect.Get_B: IPoint;
var APoint : PDPOint;
begin
  APOint:=new(PDPOint,Init(PDRect(FRect)^.B.x,PDRect(FRect)^.B.y));
  Result:=AXPoint.TPoint.Create(APoint,true);
end;

procedure TRect.Set_A(const Value: IPoint);
begin
 PDRect(FRect)^.A.Init(Value.x,Value.y);
end;

procedure TRect.Set_B(const Value: IPoint);
begin
 PDRect(FRect)^.B.Init(Value.x,Value.y);
end;

function TRect.Get_height: Integer;
begin
 Result:=abs(PDRect(FRect)^.B.y-PDRect(FRect)^.A.y);
end;

function TRect.Get_width: Integer;
begin
Result:=abs(PDRect(FRect)^.B.x-PDRect(FRect)^.A.x);
end;

function TRect.Get_x1: Integer;
begin
Result:=PDRect(FRect)^.A.x;
end;

function TRect.Get_x2: Integer;
begin
REsult:=PDRect(FRect)^.b.x;
end;

function TRect.Get_y1: Integer;
begin
Result:=PDRect(FRect)^.A.y;
end;

function TRect.Get_y2: Integer;
begin
 REsult:=PDRect(FRect)^.B.y;
end;

procedure TRect.Set_x1(Value: Integer);
begin
PDRect(FRect)^.A.x:=Value;
end;

procedure TRect.Set_x2(Value: Integer);
begin
PDRect(FRect)^.B.x:=Value;
end;

procedure TRect.Set_y1(Value: Integer);
begin
PDRect(FRect)^.A.y:=value;
end;

procedure TRect.Set_y2(Value: Integer);
begin
PDRect(FRect)^.B.y:=value;
end;

procedure TRect.Init(x1, y1, x2, y2: Integer);
begin
PDRect(FRect)^.A.Init(x1,y1);
PDRect(FRect)^.B.Init(x2,y2);
end;

initialization
  TAutoObjectFactory.Create(ComServer, TRect, Class_Rect,
    ciInternal, tmApartment);
end.
