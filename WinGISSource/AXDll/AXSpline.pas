unit AXSpline;

interface

uses
  ComObj, ActiveX, AXDLL_TLB, StdVcl,AXBase;

type
  TSpline = class(TBase, ISpline)
    FSpline  : POinter;
    Data     : Pointer;
    Release  : Boolean;
    hIndex   : Integer;
  protected
    function Get_ObjectType: Integer; safecall;
    function Get_Count: Integer; safecall;
    function Get_Points(AIndex: Integer): IPoint; safecall;
    procedure InsertPoint(const Item: IPoint); safecall;
    function Get_ObjectStyle: IObjectStyle; safecall;
    procedure Set_ObjectStyle(const Value: IObjectStyle); safecall;
    function Get_Index: Integer; safecall;
    procedure Set_Index(Value: Integer); safecall;
    function Get_OtherStyle: WordBool; safecall;
    procedure CreateObjectStyle; safecall;
    function Get_ClipRect: IRect; safecall;
    { Protected declarations }
  Public
    Constructor Create(AData,AItem:POInter;ARelease:Boolean);
    Destructor  Destroy; override;
  end;

implementation

uses ComServ,am_Splin,axPoint,am_def,axdef,axobjectstyle, AxRect;

Constructor TSpline.Create;
  begin
    inherited Create;
    FSpline:=AItem;
    BaseItem:=AItem;
    Data:=AData;
    Release:=ARelease;
    hIndex:=-1;
  end;

Destructor TSpline.Destroy;
  begin
    if Release Then Dispose(PSPline(FSPline),Done);
    inherited Destroy;
  end;

function TSpline.Get_ObjectType: Integer;
begin
  Result:=-1;
  if FSpLine<>NIL then begin
    REsult:=PSpline(FSpLine)^.GetObjtype;
  end;
end;

function TSpline.Get_Count: Integer;
begin
  Result:=-1;
  if FSpLine<>NIL then begin
    REsult:=PSpline(FSpLine)^.Data^.Count;
  end;
end;

function TSpline.Get_Points(AIndex: Integer): IPoint;
begin
  Result:=NIL;
  if FSpLine<>NIL then begin
    REsult:=TPoint.Create(PSpline(FSpLine)^.Data^.at(AIndex),false);
  end;
end;


procedure TSpline.InsertPoint(const Item: IPoint);
var APoint : TDPOint;
begin
  if FSpline<>NIL then begin
    APoint.Init(Item.x,Item.y);
    PSPline(FSpline)^.InsertPoint(APOint);
  end;
end;

function TSpline.Get_ObjectStyle: IObjectStyle;
begin
  Result:=NIL;
  if FSpline<>NIL then begin
    if  PSpline(FSpline)^.ObjectStyle <> NIL then
       REsult:=TObjectStyle.Create(PSpline(FSpline)^.ObjectStyle,false);
  end;
end;

procedure TSpline.Set_ObjectStyle(const Value: IObjectStyle);
begin
 if FSpline<>NIL then begin
    SetStyle(PSpline(FSpline),Value);
  end;
end;

function TSpline.Get_Index: Integer;
begin
 Result:=-1;
  if FSpline<>NIL then begin
    if HIndex<>-1 then Result:=HIndex else
    Result:=PSpline(FSpline)^.Index;
  end;
end;

procedure TSpline.Set_Index(Value: Integer);
begin
 if FSpline<>NIL then begin
   //PSpline(FSpline)^.Index:=Value;
   HIndex:=Value;
 end;
end;

function TSpline.Get_OtherStyle: WordBool;
begin
  REsult:=false;
  if FSpline<>NIL then begin
    Result:=PSpline(FSpline)^.Getstate(sf_otherstyle);
  end;
end;

procedure TSpline.CreateObjectStyle;
begin
   CreateStyle(PSpline(FSpline));
end;

function TSpline.Get_ClipRect: IRect;
begin
  Result:=AXRect.TRect.Create(@PSpline(FSpline).ClipRect,false);
end;

initialization
  TAutoObjectFactory.Create(ComServer, TSpline, Class_Spline,
    ciInternal, tmApartment);
end.
