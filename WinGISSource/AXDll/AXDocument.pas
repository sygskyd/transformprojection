unit AXDocument;

interface

uses
  ComObj, ActiveX, AXDLL_TLB, StdVcl, am_Coll;

type
  TDocument = class(TAutoObject, IDocument)
  private
    FDocument    : Pointer;        //TXProj
    ToExport     : TViewColl;
    ToExportCreated:boolean;
  protected
    function CreatePixel: IPixel; safecall;
    function CreatePoint: IPoint; safecall;
    function Get_Layers: ILayers; safecall;
    function Get_Name: WideString; safecall;
    function CreatePolygon: IPolygon; safecall;
    function CreatePolyLine: IPolyline; safecall;
    function CreateText: IText; safecall;
    procedure Redraw; safecall;
    function CreateList: IList; safecall;
    procedure Save; safecall;
    procedure SaveAs(const FileName: WideString); safecall;
    function CreateArc: IArc; safecall;
    function CreateCircle: ICircle; safecall;
    function CreateDBText: IDBText; safecall;
    function CreateDPoint: IDPoint; safecall;
    function CreateImage(const FileName: WideString): IImage; safecall;
    function CreateMesLine: IMesLine; safecall;
    function CreateRect: IRect; safecall;
    function CreateSpline: ISpline; safecall;
    function CreateSymbol: ISymbol; safecall;
    function CreateSymbolDef: ISymbolDef; safecall;
    function CreateIslandArea(const List: IList): IPolygon; safecall;
    function Get_Bounds: IRect; safecall;
    function Get_Symbols: ISymbols; safecall;
    procedure Set_Bounds(const Value: IRect); safecall;
    function CreateFillStyle: IFillStyle; safecall;
    function CreateLineStyle: ILineStyle; safecall;
    function IDocument_CreateFontDef: IDispatch; safecall;
    function Get_Fonts: IFonts; safecall;
    procedure ReArrangeBounds; safecall;
    function CreateCustomers: ICustomers; safecall;
    function CreateCustomerItem: ICustomerItem; safecall;
    function CreateGenerateObject: IGenerateObject; safecall;
    function Get_Project: Integer; safecall;
    function CreateLine: ILine; safecall;
    function Get_Projection: Integer; safecall;
    function Get_Projectdate: WideString; safecall;
    function Get_ProjectProjection: WideString; safecall;
    procedure Set_Projectdate(const Value: WideString); safecall;
    procedure Set_ProjectProjection(const Value: WideString); safecall;
    function Get_ProjectProjSettings: WideString; safecall;
    procedure Set_ProjectProjSettings(const Value: WideString); safecall;
    function Get_NrOfSelectedObjects: Integer; safecall;
    function Get_SelItems(Index: Integer): IBase; safecall;
    procedure CreateSelectedList; safecall;
    procedure DestroySelectedList; safecall;
    procedure Set_Projection(Value: Integer); safecall;
    function Get_ProjectionXOffset: Double; safecall;
    procedure Set_ProjectionXOffset(Value: Double); safecall;
    function Get_ProjectionYOffset: Double; safecall;
    procedure Set_ProjectionYOffset(Value: Double); safecall;
    function Get_ProjectionScale: Double; safecall;
    procedure Set_ProjectionScale(Value: Double); safecall;
    procedure DeleteLayerByName(const Name: WideString); safecall;
    procedure DeleteObjectByIndex(Index: Integer); safecall;
    procedure SelectByRect(const Rect: IRect); safecall;
    function GetObjectByIndex(Index: Integer): IBase; safecall;
    procedure DeselectAll; safecall;
    procedure FinishImpExpRoutine(ExitMode: WordBool; const ModuleType:WideString); safecall;
    procedure DoAmpDkmImport(const StartDir, SourceProj, SourceDb: WideString;
      DestDbMode: Integer; const DestDb, DestTable, LngCode: WideString);
      safecall;
    procedure ExportUserDefStylesToWlf(const Filename: WideString); safecall;
    procedure ImportUserDefStylesFromWlf(const Filename: WideString); safecall;
    function GetUserFillStyleNumberByName(
      const StyleName: WideString): Integer; safecall;
    function GetUserFillStyleNameByNumber(StyleNumber: Integer): WideString;
      safecall;
    function GetUserLineStyleNumberByName(
      const StyleName: WideString): Integer; safecall;
    function GetUserLineStyleNameByNumber(StyleNumber: Integer): WideString;
      safecall;
    function CreateSight(const Name: WideString; X, Y, Height, Width,
      Rotation: Double): ISight; safecall;
    function Get_Sights: ISights; safecall;
    function Get_StartProjectGUID: WideString; safecall;
    function Get_CurrentProjectGUID: WideString; safecall;
    function ConvToPoly(Item: OleVariant): IPolyline; safecall;
    procedure SelectSearchText(const aText: WideString); safecall;
    procedure SelectByRectTouch(const Rect: IRect); safecall;
    procedure DeleteSelectedObjects; safecall;
    procedure FinishModule(ModuleIdx: Integer); safecall;
    procedure SelectObjectByIndex(Index: Integer); safecall;
    function IsModuleRegistered(ModuleIdx: Integer): WordBool; safecall;
    function Get_IdbProcessMask: Integer; safecall;
    procedure Set_IdbProcessMask(Value: Integer); safecall;
    procedure Set_Name(const Value: WideString); safecall;
    procedure RedrawRect(const ARect: IRect); safecall;
    procedure ZoomByRect(const Rect: IRect; Frame: Integer); safecall;
    procedure Close; safecall;
    procedure SetCanvas(ParentHandle, Mode: Integer); safecall;
    { Protected declarations }
  public
    Constructor Create(ADocument:Pointer);
    DEstructor  Destroy; override;
  end;

implementation

uses ComServ,AX_Proj,am_proj,
     am_def,am_child,AXLayers,AXPixel,am_Point,AXPoint,am_cpoly,AXPolygon,
     AXPolyLIne,am_Poly,Forms,am_text,AXText,am_font,am_Circl,AXCircle,AXArc,am_Meas,
     AXMesLine,am_rText,AXDBText,am_bugra,am_splin,AXSpline,Am_Sym,AXSymbol,
     bmpimage, AXImage, AXRect, am_group, AXSymbolDef,AXSymbols,AXList,axdef,am_view,
     am_Layer,classes,sysutils,AXFillStyle,AXLineStyle,AXFonts,AXCustomers,AXCustomerItem,
     AXGenerateObject,axline,coordinatesystem, am_index,axlayer, am_Sight, GrTools,
     AXSight, AXSights, Idb_Consts, Objects;

Constructor TDocument.Create;
  begin
    inherited Create;
    FDocument:=ADocument;
    ToExportCreated:=FALSE;
  end;

Destructor TDocument.Destroy;
  begin
    if FDocument<>NIL then begin
      TXProj(FDocument).Free;
    end;
    inherited Destroy;
  end;

function TDocument.CreatePixel: IPixel;
var APixel : PPixel;
    APOint : TDPoint;
begin
  Result:=NIL;
  If FDocument<>NIL then begin
    APoint.Init(0,0);
    APixel:=new(PPixel,Init(APOint));
    Result:=AXPixel.TPixel.create(TXProj(FDocument).DAta,APixel,true);
  end;
end;

function TDocument.CreatePoint: IPoint;
Var APoint : PDPOint;
begin
  APOint:=new(PDPOint,Init(0,0));
  Result:=AXPoint.TPoint.Create(APoint,true);
end;

function TDocument.Get_Layers: ILayers;
begin
  Result:=NIL;
  if FDocument<>NIL then begin
    Result:=TLayers.Create(PProj(TXProj(FDocument).Data)^.Layers,TXProj(FDocument).Data);
  end;
end;

function TDocument.Get_Name: WideString;
begin
  // Result:=StrPas(PProj(TXProj(FDocument).Data)^.FName);
  Result:=TXProj(FDocument).Filename;
end;

function TDocument.CreatePolygon: IPolygon;
var APolygon : PCPoly;
begin
  APolygon:=new(PCPoly,Init);
  Result:=AXPolygon.TPolygon.Create(TXProj(FDocument).Data,APolygon,true);
end;

function TDocument.CreatePolyLine: IPolyLine;
var APolyLine : PPoly;
begin
  APolyLine:=new(PPoly,Init);
  Result:=AXPolyLine.TPolyLine.Create(TXProj(FDocument).Data,APolyLIne,true);
end;

procedure TDocument.Redraw;
begin
  if FDocument<>NIL then begin
    PProj(TXProj(FDocument).Data)^.Pinfo^.RedrawScreen(false);
    Application.Processmessages;
  end;
end;

function TDocument.CreateText: IText;
Var AText : PText;
    AFont : TFontData;
begin
  AText:=new(PText,Init(Afont,'',0));
  Result:=AXText.TText.Create(TXProj(FDocument).Data,AText,true);
end;


function TDocument.CreateList: IList;
begin
  Result:=AXList.TList.Create;
end;

procedure TDocument.Save;
begin
  TXProj(FDocument).Save;
end;

function TDocument.CreateArc: IArc;
var AArc  : PEllipseArc;
    APoint : TDPoint;
begin
  AArc:=new(PEllipseArc);
  AArc^.INit(APoint,0,0,0,0,0);
  REsult:=AXArc.TArc.Create(TXProj(FDocument).DAta,AArc,true);
end;

function TDocument.CreateCircle: ICircle;
var ACirc  : PEllipse;
    aPoint : TDPoint;
begin
  ACirc:=new(PEllipse);
  ACirc^.INit(APoint,0,0,0);
  REsult:=AXCircle.Tcircle.Create(TXProj(FDocument).DAta,ACirc,true);
end;

function TDocument.CreateDBText: IDBText;
var AText : PRText;
    AFont : TFontData;
begin
  Result:=NIL;
  AText:=new(PRText,init(AFont,''));
  Result:=AXDBText.TDBTExt.Create(TXProj(FDocument).DAta,AText,true);
end;



function TDocument.CreateImage(const FileName: WideString): IImage;
var AIMage    : PImage;
    RelPath   : String;
    BitSet    : TBitmapSettings;
begin
  Result:=NIL;
  RelPath:=ExtractRelativePath(PProj(TXProj(FDocument).Data)^.FName,FileName);
  BitSet:=PProj(TXProj(FDocument).Data)^.PInfo^.BitmapSettings;
  AImage:=new(PImage,init(FileName,ExtractRelativePath(PProj(TXProj(FDocument).Data)^.FName,FileName),so_ShowBMP,PProj(TXProj(FDocument).Data)^.PInfo^.BitmapSettings));
  // AImage:=new(PImage,init(FileName,RelPath,so_ShowBMP,BitSet));
  Result:=AXImage.TImage.Create(TMDIChild(FDocument).Data,AImage,true);
end;

function TDocument.CreateSpline: ISpline;
var ASpline : PSpline;
begin
  Result:=NIL;
  ASPline:=new(PSpline,Init);
  Result:=AXSpline.TSpline.Create(TXProj(FDocument).Data,ASpline,true);
end;

function TDocument.CreateSymbol: ISymbol;
var ASymbol : PSymbol;
    APoint  : TDPoint;
begin
  Result:=NIL;
  ASymbol:=new(PSymbol,Init(PProj(TXProj(FDocument).Data)^.PInfo,APoint,0,0,0));
  Result:=AXSymbol.TSymbol.Create(TXProj(FDocument).Data,ASymbol,true);
end;

function TDocument.CreateRect: IRect;
begin
  REsult:=AXREct.TRect.Create(NIL,true);
end;

function TDocument.CreateSymbolDef: ISymbolDef;
  var AGroup : PSGroup;
begin
  AGroup:=new(PSGroup,init);
  Result:=TSymbolDef.Create(TXProj(FDocument).Data,AGroup,true);
end;

function TDocument.Get_Symbols: ISymbols;
begin
  REsult:=AXSymbols.TSymbols.Create(TXProj(FDocument).Data,
  PProj(TXProj(FDocument).Data)^.Pinfo^.Symbols);
end;

function TDocument.CreateIslandArea(const List: IList): IPolygon;
var APolygon      : PCPoly;
    VarItem       : IBase;
    cnt           : integer;
    AType         : Integer;
    Abort         : Boolean;
    AView         : PView;
    InsPoly       : PCPoly;
    SrcLayer      : TList;
    DestLayer     : TList;
begin
  Result:=NIL;
  Abort:=false;
  if List<>NIL then begin
    SrcLayer:=Classes.TList.Create;
    DestLayer:=Classes.TList.Create;
    for cnt:=0 to List.count-1 do begin
      VarItem:=List.Items[cnt];
      try
        AType:=VarItem.ObjectType;
      except
        Abort:=true;
      end;
      if Not Abort then begin  //Object mit ObjectType
        if VarItem.ObjectType=ot_CPoly then begin
          //Polygon in Src-Layer List aufnehmen
          InsPoly:=PCPoly(AXDef.CopyObject(TXProj(FDocument).Data,VarItem));
          SrcLayer.Add(InsPoly);
        end;
      end;
    end;
    if SrcLayer.Count>0 then begin
      //Polys auf Src-Layer nach DestLayer (Island konvertieren)
      AXDef.CreateIslandArea(TXProj(FDocument).Data,SrcLayer,DestLayer);
      FreeLayer(SrcLayer);
      //FreeLayer(DestLayer);
      //APolygon:=new(PCPoly,Init);
      APolygon:=DestLayer[0];
      DestLayer.Free;
      Result:=AXPolygon.TPolygon.Create(TXProj(FDocument).Data,APolygon,true);
    end;
  end;
end;


function TDocument.CreateMesLine: IMesLine;
var AMesLine : PMeasureLine;
    APoint   : TDPoint;
begin
  AMesLine:=new(PMeasureLine,Init(APoint,APoint));
  Result:=AXMesLine.TMesLine.Create(TXProj(FDocument).Data,AMesLine,true);
end;



procedure TDocument.SaveAs(const FileName: WideString);
begin

end;

function TDocument.CreateDPoint: IDPoint;
begin

end;

function TDocument.Get_Bounds: IRect;
var ALayer   : ILayer;
    ADis     : IDispatch;
    ABase    : IParent;
begin
  REsult:=AXrect.TRect.Create(@PProj(TXProj(FDocument).Data)^.Size,false);
  {
  ALayer:=Get_Layers.Item[1];
  ADis:=ALayer.Item[1];
  ABase:=IBase(ADis);
  if ABase.ObjectType=1 then begin
  end;
  }
end;

procedure TDocument.Set_Bounds(const Value: IRect);
begin

end;

function TDocument.CreateFillStyle: IFillStyle;
begin
  Result:=AXFillStyle.TFillStyle.Create(Nil,true);
end;
{
function TDocument.CreateFontDef: IFontDef;
begin
  Result:=AXFontDef.TFontDef.Create;
end;
}
function TDocument.CreateLineStyle: ILineStyle;
begin
  REsult:=AXLineStyle.TLineStyle.Create(nil,true);
end;

function TDocument.IDocument_CreateFontDef: IDispatch;
begin

end;

function TDocument.Get_Fonts: IFonts;
begin
  REsult:=AXFonts.TFonts.Create(TXProj(FDocument).Data,
  PProj(TXProj(FDocument).Data)^.Pinfo^.Fonts);
end;


procedure TDocument.ReArrangeBounds;
begin
  if FDocument<>NIL then begin
    PProj(TXProj(FDocument).Data)^.CalculateSize;
  end;
  {
    PProj(TXProj(FDocument).Data)^.Size.Init;
    PLayer(PProj(TXProj(FDocument).Data)^.PInfo^.Objects)^.GetSize(PProj(TXProj(FDocument).Data)^.Size);
    if PProj(TXProj(FDocument).Data)^.Size.IsEmpty then begin
      PProj(TXProj(FDocument).Data)^.Size.A.Init(0,0);
      PProj(TXProj(FDocument).Data)^.Size.B.Init(siStartX,siStartY);
    end;
    PProj(TXProj(FDocument).Data)^.SetModified;
    }
end;

function TDocument.CreateCustomers: ICustomers;
begin
  Result:=AXCustomers.TCustomers.Create(Self);
end;

function TDocument.CreateCustomerItem: ICustomerItem;
begin
  Result:=TCustomerItem.Create;
end;

function TDocument.CreateGenerateObject: IGenerateObject;
begin
  Result:=AXGenerateObject.TGenerateObject.Create(Self);
end;

function TDocument.Get_Project: Integer;
begin
Result:=Integer(TXProj(FDocument).DAta);
end;

function TDocument.CreateLine: ILine;
begin
  Result:=TLine.Create(0,0,0,0,true);
end;

function TDocument.Get_Projection: Integer;
begin
  if PProj(TXProj(FDocument).Data)^.Pinfo^.CoordinateSystem.CoordinateType=ctGeodatic then result:=1 else
  if PProj(TXProj(FDocument).Data)^.Pinfo^.CoordinateSystem.CoordinateType=ctCarthesian then result:=0;
end;

function TDocument.Get_Projectdate: WideString;
begin
   result:=PProj(TXProj(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectDate;
end;

function TDocument.Get_ProjectProjection: WideString;
begin
   result:=PProj(TXProj(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectProjection;
end;

procedure TDocument.Set_Projectdate(const Value: WideString);
begin
   PProj(TXProj(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectDate:=Value;
end;

procedure TDocument.Set_ProjectProjection(const Value: WideString);
begin
   PProj(TXProj(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectProjection:=Value;
end;

function TDocument.Get_ProjectProjSettings: WideString;
begin
   result:=PProj(TXProj(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectProjSettings;
end;

procedure TDocument.Set_ProjectProjSettings(const Value: WideString);
begin
   PProj(TXProj(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectProjSettings:=Value;
end;

// function that as used to create a collection of the items that are
// currently selected
procedure TDocument.CreateSelectedList;
var Cnt:integer;
    AItem:PIndex;
    ALayer:PLayer;
    layercnt:integer;
    FirstLayerItem:boolean;
    Layername:string;
    numlayers:integer;
   function DoCollectObjects(Item:PIndex):Boolean;
   begin
      ToExport.AtInsert(ToExport.GetCount,Item);
      DoCollectObjects:=FALSE;
   end;

begin
   ToExport.Init;
   // now all layers will be iterated to insert the objects sorted
   // by layer into the list of the selected objects
   numlayers:=PProj(TXProj(FDocument).Data)^.Layers^.LData^.Count-1;
   for layercnt:=1 to numlayers do
   begin
      FirstLayerItem:=TRUE;
      ALayer:=PLayer(PProj(TXProj(FDocument).Data)^.Layers^.LData^.at(layercnt));
      if ALayer = nil then continue;
      // search object on layer
      Layername:=ALayer^.Text^;
      for Cnt:=0 to PProj(TXProj(FDocument).Data)^.Layers^.SelLayer^.Data^.GetCount-1 do
      begin
         AItem:=PProj(TXProj(FDocument).Data)^.Layers^.SelLayer^.Data^.At(Cnt);
         if (ALayer^.HasObject(AItem)) <> nil then
         begin
             // Layer has this item
             if (FirstLayerItem) then // if it�s the first item on this layer insert the layer itself
             begin
                ToExport.AtInsert(ToExport.GetCount,ALayer);
                FirstLayerItem:=false;
             end;
             if DoCollectObjects(PProj(TXProj(FDocument).Data)^.Layers^.SelLayer^.Data^.At(Cnt)) then Break;
         end;
      end;
   end;
   ToExportCreated:=TRUE;
end;

// This function is used to remove the selected list after
// the export is finished
procedure TDocument.DestroySelectedList;
begin
   ToExport.DeleteAll;
   ToExport.Done;
   ToExportCreated:=FALSE;
end;

function  TDocument.Get_NrOfSelectedObjects: Integer;
begin
   if not ToExportCreated then
      result:=PProj(TXProj(FDocument).Data)^.Layers^.SelLayer^.Data^.GetCount
   else
      result:=ToExport.GetCount;
end;

// function is used to get item of selected objects
function TDocument.Get_SelItems(Index: Integer): IBase;
safecall;
var AIndex  : PIndex;
    AView   : PView;
    Item    : PIndex;
begin
   Result:=NIL;
   AIndex:=NIL;
   Item:=ToExport.At(Index);
   if Item.GetObjType = ot_Layer then begin
      Result:=AXLayer.TLayer.create(Item,PProj(TXProj(FDocument).Data));
   end
   else
   begin
      AIndex:=PProj(TXProj(FDocument).Data)^.Layers^.IndexObject(PProj(TXProj(FDocument).Data)^.PInfo,Item);
   end;

   if Aindex<>NIl then
   begin
      AView:=PView(AIndex.Mptr);
      AView:=PView(PLayer(PProj(TXProj(FDocument).Data)^.PInfo^.Objects)^.IndexObject(PProj(TXProj(FDocument).Data)^.PInfo,AIndex));

      // copy the objectstyle
      if AIndex^.ObjectStyle = nil then
      begin
         AView^.ObjectStyle:=nil;
      end
      else
      begin
         AIndex^.CopyObjectStyleTo(AView);
      end;

      if AIndex.GetObjType = ot_CPoly then begin
        Result:=AXPolyGon.TPolygon.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_Poly then begin
        Result:=AXPolyLine.TPolyLine.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_Text then begin
        Result:=AXText.TText.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_Symbol then begin
        Result:=AXSymbol.TSymbol.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_Pixel then begin
        Result:=AXPixel.TPixel.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_Circle then begin
        Result:=AXCircle.TCircle.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_Arc then begin
        Result:=TArc.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_MesLine then begin
        Result:=TMesLine.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_Image then begin
        Result:=AXImage.TImage.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_RText then begin
        Result:=TDBText.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_Spline then begin
        Result:=AXSpline.TSPLine.Create(PProj(TXProj(FDocument).Data),AView,false);
      end;
   end;
end;

procedure TDocument.Set_Projection(Value: Integer);
begin
  if Value=0 then PProj(TXProj(FDocument).Data)^.Pinfo^.CoordinateSystem.CoordinateType:=ctCarthesian;
  if Value=1 then PProj(TXProj(FDocument).Data)^.Pinfo^.CoordinateSystem.CoordinateType:=ctGeodatic;
end;

function TDocument.Get_ProjectionXOffset: Double;
begin
   result:=PProj(TXProj(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectionXOffset;
end;

procedure TDocument.Set_ProjectionXOffset(Value: Double);
begin
   PProj(TXProj(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectionXOffset:=Value;
end;

function TDocument.Get_ProjectionYOffset: Double;
begin
   result:=PProj(TXProj(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectionYOffset;
end;

procedure TDocument.Set_ProjectionYOffset(Value: Double);
begin
   PProj(TXProj(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectionYOffset:=Value;
end;

function TDocument.Get_ProjectionScale: Double;
begin
   result:=PProj(TXProj(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectionScale;
end;

procedure TDocument.Set_ProjectionScale(Value: Double);
begin
   PProj(TXProj(FDocument).Data)^.Pinfo^.ProjectionSettings.ProjectionScale:=Value;
end;

procedure TDocument.DeleteLayerByName(const Name: WideString);
var numlayers,layercnt:integer;
    ALayer    :PLayer;
    FoundLayer:PLayer;
    i:integer;
    AIndex:PIndex;
begin
   // first get the layer
   numlayers:=PProj(TXProj(FDocument).Data)^.Layers^.LData^.Count-1;
   ALayer:=nil; FoundLayer:=nil;
   for layercnt:=1 to numlayers do
   begin
      ALayer:=PLayer(PProj(TXProj(FDocument).Data)^.Layers^.LData^.at(layercnt));
      if ALayer = nil then continue;
      // check if the item exists on the layer and return item
      if ALayer^.Text^ = Name then
      begin
         FoundLayer:=ALayer;
         break;
      end;
   end;
   if FoundLayer <> nil then
   begin
      // first all objects on the layer have to be deleted
      for i:=0 to ALayer^.Data^.GetCount - 1 do
      begin
         AIndex:=ALayer^.Data^.at(i);
         PLayer (PProj(TXProj(FDocument).Data)^.Pinfo^.Objects)^.DeleteIndex (PProj(TXProj(FDocument).Data)^.Pinfo, AIndex);
      end;
      // now remove all objects from layer itself
      ALayer^.Data^.FreeAll;
      // due to all objects have been deleted the layer can be removed
      PProj(TXProj(FDocument).Data)^.Layers^.DeleteLayer(ALayer);
      PProj(TXProj(FDocument).Data)^.Layers^.DetermineTopLayer;
   end;
end;

procedure TDocument.DeleteObjectByIndex(Index: Integer);
var numlayers,layercnt:integer;
    AIndex:PIndex;
    ALayer  : PLayer;
begin
   // first get the item
   AIndex:=nil;
   numlayers:=PProj(TXProj(FDocument).Data)^.Layers^.LData^.Count-1;
   for layercnt:=1 to numlayers do
   begin
      ALayer:=PLayer(PProj(TXProj(FDocument).Data)^.Layers^.LData^.at(layercnt));
      if ALayer = nil then continue;
      // check if the item exists on the layer and return item
      AIndex:=ALayer^.HasIndexObject(Index);
      if (AIndex) <> nil then // object found
      begin
         break;
      end;
   end;
   if Aindex<>NIl then
   begin
      // delete object from project
      PLayer (PProj(TXProj(FDocument).Data)^.Pinfo^.Objects)^.DeleteIndex (PProj(TXProj(FDocument).Data)^.Pinfo, AIndex);
      // delete object from layer
      ALayer^.DeleteIndex(PProj(TXProj(FDocument).Data)^.Pinfo,AIndex);
   end;
end;

procedure TDocument.SelectByRect(const Rect: IRect);
var ARect:TDRect;
begin
   ARect.Init;
   ARect.A.X:=Rect.A.X; ARect.A.Y:=Rect.A.Y;
   ARect.B.X:=Rect.B.X; ARect.B.Y:=Rect.B.Y;
   PProj(TXProj(FDocument).Data)^.Layers^.SelectByRectFastWithoutDlg(PProj(TXProj(FDocument).Data)^.Pinfo, ARect, TRUE); // TRUE -> only objects inside rect
end;

function TDocument.GetObjectByIndex(Index: Integer): IBase;
var numlayers,layercnt:integer;
    AIndex:PIndex;
    AView   : PView;
    ALayer  : PLayer;
begin
   Result:=NIL;
   AIndex:=NIL;
   // first get the item
   numlayers:=PProj(TXProj(FDocument).Data)^.Layers^.LData^.Count-1;
   for layercnt:=1 to numlayers do
   begin
      ALayer:=PLayer(PProj(TXProj(FDocument).Data)^.Layers^.LData^.at(layercnt));
      if ALayer = nil then continue;
      // check if the item exists on the layer and return item
      AIndex:=ALayer^.HasIndexObject(Index);
      if (AIndex) <> nil then // object found
         break;
   end;
   if Aindex<>NIl then
   begin
      AView:=PView(PLayer(PProj(TXProj(FDocument).Data)^.PInfo^.Objects)^.IndexObject(PProj(TXProj(FDocument).Data)^.PInfo,AIndex));

      // copy objectstyle
      if AIndex^.ObjectStyle = nil then
      begin
         AView^.ObjectStyle:=nil;
      end
      else
      begin
         AIndex^.CopyObjectStyleTo(AView);
      end;

      if AIndex.GetObjType = ot_CPoly then begin
         Result:=AXPolyGon.TPolygon.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_Poly then begin
        Result:=AXPolyLine.TPolyLine.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_Text then begin
        Result:=AXText.TText.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_Symbol then begin
        Result:=AXSymbol.TSymbol.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_Pixel then begin
        Result:=AXPixel.TPixel.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_Circle then begin
        Result:=AXCircle.TCircle.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_Arc then begin
        Result:=TArc.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_MesLine then begin
        Result:=TMesLine.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_Image then begin
        Result:=AXImage.TImage.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_RText then begin
        Result:=TDBText.Create(PProj(TXProj(FDocument).Data),AView,false);
      end
      else if AIndex.GetObjType = ot_Spline then begin
        Result:=AXSpline.TSPLine.Create(PProj(TXProj(FDocument).Data),AView,false);
      end;
   end;
end;

procedure TDocument.DeselectAll;
begin
   if FDocument<>NIL then
   begin
      PProj(TXProj(FDocument).Data)^.DeselectAll(false);
   end;
end;

procedure TDocument.FinishImpExpRoutine(ExitMode: WordBool; const ModuleType:WideString); safecall;
begin
   // dummy declaration
end;

// method is used to finish the dxf-import routine and
// execute an Amp-Import with the specified parameters
procedure TDocument.DoAmpDkmImport(const StartDir, SourceProj, SourceDb: WideString;
                                   DestDbMode: Integer; const DestDb, DestTable,
                                   LngCode: WideString
                                  );
begin
   PProj(TXProj(FDocument).Data)^.Layers^.DoAmpDkmImport(StartDir, SourceProj, SourceDb,
                                                         DestDbMode, DestDb, DestTable,
                                                         LngCode);
end;

procedure TDocument.ExportUserDefStylesToWlf(const Filename: WideString);
begin
   PProj(TXProj(FDocument).Data)^.Pinfo^.ExportUserDefStylesToWlf(Filename);
end;

procedure TDocument.ImportUserDefStylesFromWlf(const Filename: WideString);
begin
   PProj(TXProj(FDocument).Data)^.Pinfo^.ImportUserDefStylesFromWlf(Filename);
end;

function TDocument.GetUserFillStyleNumberByName(
  const StyleName: WideString): Integer;
begin
   result:=PProj(TXProj(FDocument).Data)^.Pinfo^.GetUserFillStyleNumberByName(StyleName);
end;

function TDocument.GetUserFillStyleNameByNumber(
  StyleNumber: Integer): WideString;
begin
   result:=PProj(TXProj(FDocument).Data)^.Pinfo^.GetUserFillStyleNameByNumber(StyleNumber);
end;

function TDocument.GetUserLineStyleNumberByName(
  const StyleName: WideString): Integer;
begin
   result:=PProj(TXProj(FDocument).Data)^.Pinfo^.GetUserLineStyleNumberByName(StyleName);
end;

function TDocument.GetUserLineStyleNameByNumber(
  StyleNumber: Integer): WideString;
begin
   result:=PProj(TXProj(FDocument).Data)^.Pinfo^.GetUserLineStyleNameByNumber(StyleNumber);
end;

function TDocument.CreateSight(const Name: WideString; X, Y, Height, Width,
  Rotation: Double): ISight;
var ASight    : PSight;
    ARotRect  : TRotRect;
begin
  Result:=NIL;
  ARotRect.X:=X;
  ARotRect.Y:=Y;
  ARotRect.Width:=Width;
  ARotRect.Height:=Height;
  ARotRect.Rotation:=Rotation;
  ASight:=new(PSight,init(Name, ARotRect));
  Result:=AXSight.TAXSight.Create(TMDIChild(FDocument).Data,ASight,true);
end;

function TDocument.Get_Sights: ISights;
begin
   result:=AXSights.TSights.Create(TXProj(FDocument).Data, PProj(TXProj(FDocument).Data)^.Sights);
end;

function TDocument.Get_StartProjectGUID: WideString;
var sGUID: AnsiString;
    Value: TGUID;
begin
   PProj(TXProj(FDocument).Data).Registry.OpenKey(cs_ProjectRegistry_IDB_KeyName, TRUE);
   sGUID := PProj(TXProj(FDocument).Data).Registry.ReadString(cs_ProjectRegistry_IDB_StartGUID_ParameterName);
   // if there still exists no GUID a new one has to be created
   if sGUID = '' then
   begin
      CoCreateGUID(Value);
      sGUID:=GuidToString(Value);
      PProj(TXProj(FDocument).Data).Registry.WriteString(cs_ProjectRegistry_IDB_StartGUID_ParameterName, sGUID);
      PProj(TXProj(FDocument).Data).Registry.WriteString(cs_ProjectRegistry_IDB_GUID_ParameterName, sGUID);
   end;
   result:=sGUID;
end;

function TDocument.Get_CurrentProjectGUID: WideString;
var sGUID: AnsiString;
    Value: TGUID;
begin
   PProj(TXProj(FDocument).Data).Registry.OpenKey(cs_ProjectRegistry_IDB_KeyName, TRUE);
   sGUID:=PProj(TXProj(FDocument).Data).Registry.ReadString(cs_ProjectRegistry_IDB_GUID_ParameterName);
   // if there still exists no GUID a new one has to be created
   if sGUID = '' then
   begin
      CoCreateGUID(Value);
      sGUID:=GuidToString(Value);
      PProj(TXProj(FDocument).Data).Registry.WriteString(cs_ProjectRegistry_IDB_StartGUID_ParameterName, sGUID);
      PProj(TXProj(FDocument).Data).Registry.WriteString(cs_ProjectRegistry_IDB_GUID_ParameterName, sGUID);
   end;
   result:=sGUID;
end;

function TDocument.ConvToPoly(Item: OleVariant): IPolyline;
var numlayers,layercnt:integer;
    AIndex  : PIndex;
    AView   : PView;
    ALayer  : PLayer;
    OriArc  : PEllipseArc;
    APoly   : IPolyline;
    APoint  : IPoint;
    Pnt     : TDPoint;
    iVtx, nVtx  : Integer;
    iSeg, nSeg  : Integer;
    dVtx        : Double;
begin
   // function is used to convert a specified Arc to a Polyline
   // and return a reference to this polyline
   result:=nil;
   // first get the item
   numlayers:=PProj(TXProj(FDocument).Data)^.Layers^.LData^.Count-1;
   for layercnt:=1 to numlayers do
   begin
      ALayer:=PLayer(PProj(TXProj(FDocument).Data)^.Layers^.LData^.at(layercnt));
      if ALayer = nil then continue;
      // check if the item exists on the layer and return item
      AIndex:=ALayer^.HasIndexObject(Item.Index);
      if (AIndex) <> nil then // object found
         break;
   end;
   if Aindex<>NIl then
   begin
      AView:=PView(AIndex.Mptr);
      AView:=PView(PLayer(PProj(TXProj(FDocument).Data)^.PInfo^.Objects)^.IndexObject(PProj(TXProj(FDocument).Data)^.PInfo,AIndex));
      Pnt.Init( 0, 0 );
      if (AView.GetObjType in [ot_Arc, ot_Circle, ot_Spline])  then
      begin
         // create polyline object
         APoly:=CreatePolyLine;
         if (AView.GetObjType = ot_Arc) or (AView.GetObjType = ot_Circle) then
         begin
            with PProj(TXProj(FDocument).Data)^.PInfo.VariousSettings do
            begin
               if bConvRelMistake then  nVtx:= PEllipse(AView).ApproximatePointsNumber( dbConvRelMistake, True )
               else nVtx:= PEllipse(AView).ApproximatePointsNumber( dbConvAbsMistake, False );
            end;
            if nVtx >= MaxCollectionSize  then nVtx:= MaxCollectionSize-1;
            for iVtx:=0 to nVtx-1 do
            begin
               Pnt:= PEllipse(AView).ApproximatePoint( iVtx, nVtx );
               APoint:=CreatePoint;
               APoint.X:=Pnt.X;
               APoint.Y:=Pnt.Y;
               APoly.InsertPoint(APoint);
            end;
         end;

        if (AView.GetObjType = ot_Spline) then
        begin
           // Insert the 1st spline point
           Pnt:= PDPoint( PSpline(AView).Data.At( 0 ) )^;
           APoint:=CreatePoint;
           APoint.X:=Pnt.X;
           APoint.Y:=Pnt.Y;
           APoly.InsertPoint(APoint);
           nSeg:= PSpline(AView).SegmentsNum;

           // Evaluate the number of vertices
           nVtx:= 1;
           for iSeg:=0 to nSeg-1 do
           begin
              with PProj(TXProj(FDocument).Data)^.PInfo.VariousSettings do
              begin
                 if bConvRelMistake then  iVtx:= PSpline(AView).ApprSegmPointsNumber( iSeg, dbConvRelMistake, True )
                 else  iVtx:= PSpline(AView).ApprSegmPointsNumber( iSeg, dbConvAbsMistake, False );
              end;
              nVtx:= nVtx + iVtx - 1;
           end;
           if nVtx < MaxCollectionSize then  dVtx:= 1.0
           else  dVtx:= (MaxCollectionSize-64) / nVtx;

           // Insert points
           for iSeg:=0 to nSeg-1 do
           begin
              with PProj(TXProj(FDocument).Data)^.PInfo.VariousSettings do
              begin
                 if bConvRelMistake then  iVtx:= PSpline(AView).ApprSegmPointsNumber( iSeg, dbConvRelMistake, True )
                 else  iVtx:= PSpline(AView).ApprSegmPointsNumber( iSeg, dbConvAbsMistake, False );
              end;
              nVtx:= Round( dVtx * (iVtx-1) ) + 1;
              if nVtx < 2  then nVtx:= 2;
              for iVtx:=1 to nVtx-1 do
              begin
                 Pnt:= PSpline(AView).ApprSegmPoint( iSeg, iVtx, nVtx );
                 APoint:=CreatePoint;
                 APoint.X:=Pnt.X;
                 APoint.Y:=Pnt.Y;
                 APoly.InsertPoint(APoint);
              end;
           end;
        end;
        result:=APoly;
     end;
   end;
end;

// method is used to search for a text on the active layer and select result
procedure TDocument.SelectSearchText(const aText: WideString);
var TxtItem     : PText;
    Selected    : PCollection;
    i           : integer;
    Item        : PIndex;

   function CollectObjects( AItem: PIndex ): Boolean; Far;
   var bSel      : Boolean;
       Idx       : PIndex;
       Layer     : PLayer;
       i         : Integer;
   begin
      CollectObjects:= False;
      bSel:= False;
      TxtItem:= Pointer( PProj(TXProj(FDocument).Data)^.Layers.IndexObject( PProj(TXProj(FDocument).Data)^.PInfo, AItem ) );
      if TxtItem.TemplateMatched( aText, True, True, True ) then
         bSel:= True;
      if bSel  then
         Selected.Insert( TxtItem );
  end;

begin
   Selected:= New( PCollection, Init( 16, 16 ) );
   PProj(TXProj(FDocument).Data)^.Layers.TopLayer.FirstObjectType( ot_Text, @CollectObjects );
   if PProj(TXProj(FDocument).Data)^.Layers.SelLayer.Data.GetCount > 0 then
      DeSelectAll;
   // mark the found items as selected
   for i:=0 to Selected.Count-1 do
   begin
      Item:= Selected.At( i );
      Item^.SetState(sf_Selected,TRUE);
      PProj(TXProj(FDocument).Data)^.Layers.SelLayer^.InsertObjectFast(PProj(TXProj(FDocument).Data)^.PInfo,Item);
      PProj(TXProj(FDocument).Data)^.PInfo^.SelectionRect.CorrectByRect(Item^.ClipRect);
   end;
   Selected.DeleteAll;
   Dispose( Selected );
end;

procedure TDocument.SelectByRectTouch(const Rect: IRect);
var ARect:TDRect;
begin
   ARect.Init;
   ARect.A.X:=Rect.A.X; ARect.A.Y:=Rect.A.Y;
   ARect.B.X:=Rect.B.X; ARect.B.Y:=Rect.B.Y;
   PProj(TXProj(FDocument).Data)^.Layers^.SelectByRectFastWithoutDlg(PProj(TXProj(FDocument).Data)^.Pinfo, ARect, FALSE); // FALSE -> also objects where only a part is inside
end;

procedure TDocument.DeleteSelectedObjects;
begin
   // dummy implementation - method does not work in internal DX
end;

procedure TDocument.FinishModule(ModuleIdx: Integer);
begin
   // dummy implementation - method does not work in internal DX
end;

procedure TDocument.SelectObjectByIndex(Index: Integer);
begin
   // dummy implementation - method does not work in internal DX
end;

function TDocument.IsModuleRegistered(ModuleIdx: Integer): WordBool;
begin
   // in external DX each module is registed (for testing)
   result:=TRUE;
end;

function TDocument.Get_IdbProcessMask: Integer;
begin
   result:=0; // dummy implementation due to external DX does not work with idb
end;

procedure TDocument.Set_IdbProcessMask(Value: Integer);
begin
   // dummy implementation due to external DX does not work with idb
end;

procedure TDocument.Set_Name(const Value: WideString);
begin
   // dummy implementation
end;

procedure TDocument.RedrawRect(const ARect: IRect);
begin
   // dummy implementation
end;

procedure TDocument.ZoomByRect(const Rect: IRect; Frame: Integer);
begin
   // dummy implementation
end;


procedure TDocument.Close;
begin

end;

procedure TDocument.SetCanvas(ParentHandle, Mode: Integer);
begin

end;

initialization
  TAutoObjectFactory.Create(ComServer, TDocument, Class_Document,
    ciInternal, tmApartment);
end.
