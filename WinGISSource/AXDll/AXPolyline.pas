unit AXPolyline;

interface

uses
  ComObj, ActiveX, AXDLL_TLB, StdVcl,AXBase;

type
  TPolyLine = class(TBase, IPolyLine)
    FPolyLIne : Pointer;
    Data      : Pointer;
    Release   : Boolean;
    hIndex    : integer;
  protected
    function Get_Count: Integer; safecall;
    function Get_ObjectType: Integer; safecall;
    function Get_Points(Index: Integer): IPoint; safecall;
    procedure InsertPoint(const Item: IPoint); safecall;
    procedure Assign(const Item: IPolyLine); safecall;
    function Get_Length: Double; safecall;
    procedure InsertPointxy(x, y: Integer); safecall;
    function Get_OtherStyle: WordBool; safecall;
    function Get_ObjectStyle: IObjectStyle; safecall;
    procedure Set_ObjectStyle(const Value: IObjectStyle); safecall;
    function Get_Index: Integer; safecall;
    procedure Set_Index(Value: Integer); safecall;
    procedure CreateObjectStyle; safecall;
    function Get_ClipRect: IRect; safecall;
    procedure Redraw; safecall;
    { Protected declarations }
  public
    Constructor Create(AData,AItem:POInter;ARelease:Boolean);
    Destructor  Destroy; override;
  end;

implementation

uses ComServ,am_Poly,AXPoint,am_def,am_proj,am_view,am_Layer,am_index,axdef,
     axobjectstyle,axrect;

Constructor TPolyLine.Create;
  begin
    inherited create;
    FPolyLine:=AItem;
    BaseItem:=AItem;
    Data:=AData;
    Release:=ARelease;
    hIndex:=-1;
  end;

Destructor TPolyLine.Destroy;
  begin
    if Release Then Dispose(PPoly(FPolyLine),Done);
    inherited Destroy;
  end;

function TPolyLine.Get_Count: Integer;
begin
  Result:=-1;
  if FPolyLine<>NIL then begin
    REsult:=PPoly(FPolyLine)^.Data^.Count;
  end;
end;

function TPolyLine.Get_ObjectType: Integer;
begin
  Result:=-1;
  if FPolyLine<>NIL then begin
    REsult:=PPoly(FPolyLine)^.GetObjtype;
  end;
end;

function TPolyLine.Get_Points(Index: Integer): IPoint;
begin
  Result:=NIL;
  if FPolyLine <> NIL then begin
    REsult:=TPoint.Create(PPoly(FPolyLine)^.Data^.at(Index),false);
  end;
end;

procedure TPolyLine.InsertPoint(const Item: IPoint);
var APoint : TDPOint;
begin
  if FPolyLine<>NIL then begin
    APoint.Init(Item.x,Item.y);
    PPoly(FPolyLIne)^.InsertPoint(APOint);
  end;
end;

procedure TPolyLine.Assign(const Item: IPolyLine);
var APolyLine : PPoly;
begin
  FPolyLine:=PPoly(copyObject(DAta,Item));
end;

function TPolyLine.Get_Length: Double;
begin
  Result:=-1;
  if FPolyLine<>NIL then begin
    Result:=PPoly(FPolyLine)^.Laenge;
  end;
end;

procedure TPolyLine.InsertPointxy(x, y: Integer);
var APoint : TDPOint;
begin
  if FPolyLine<>NIL then begin
    APoint.Init(x,y);
    PPoly(FPolyLIne)^.InsertPoint(APOint);
  end;
end;

function TPolyLine.Get_OtherStyle: WordBool;
begin
Result:=false;
if FPolyLIne<>NIl then begin
  Result:=PPoly(FPolyLIne)^.GetState(sf_otherStyle);
end;
end;

function TPolyLine.Get_ObjectStyle: IObjectStyle;
begin
  Result:=NIL;
  if FPolyLine<>NIL then begin
    if  PPoly(FPolyLine)^.ObjectStyle <> NIL then
       REsult:=TObjectStyle.Create(PPoly(FPolyLine)^.ObjectStyle,false);
  end;
end;

procedure TPolyLine.Set_ObjectStyle(const Value: IObjectStyle);
begin
  if FPolyLine<>NIL then begin
    SetStyle(PPoly(FPolyLine),Value);
  end;
end;

function TPolyLine.Get_Index: Integer;
begin
 Result:=-1;
  if FPolyLine<>NIL then begin
    if HIndex<>-1 then REsult:=Hindex else
    Result:=PPoly(FPolyLine)^.Index;
  end;
end;

procedure TPolyLine.Set_Index(Value: Integer);
begin
 if FPolyLine<>NIL then begin
   //PPoly(FPolyLine)^.Index:=Value;
   HIndex:=Value;
 end;
end;

function TPolyLine.Get_ClipRect: IRect;
begin
  REsult:=AXRect.TRect.Create(@PPoly(FPolyLine).ClipRect,false);
end;

procedure TPolyLine.CreateObjectStyle;
begin
   CreateStyle(PPoly(FPolyLine));
end;

procedure TPolyLine.Redraw;
begin
   PPoly(FPolyLine)^.Invalidate(PProj(Data)^.PInfo);
   PPoly(FPolyLine)^.CalculateClipRect; // (PProj(Data)^.PInfo);
   PPoly(FPolyLine)^.Invalidate(PProj(Data)^.PInfo);
   PProj(Data)^.UpdateClipRect(PPoly(FPolyLine));
   PProj(Data)^.CorrectSize(PPoly(FPolyLine)^.ClipRect,TRUE);
   PProj(Data)^.PInfo^.RedrawInvalidate;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TPolyLine, Class_PolyLine,
    ciInternal, tmApartment);
end.
