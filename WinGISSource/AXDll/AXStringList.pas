unit AXStringList;

interface

uses
  ComObj, ActiveX, AXDLL_TLB;

type
  TStringList = class(TAutoObject, IStringList)
  protected
    { Protected declarations }
  end;

implementation

uses ComServ;

initialization
  TAutoObjectFactory.Create(ComServer, TStringList, Class_StringList,
    ciInternal, tmApartment);
end.
