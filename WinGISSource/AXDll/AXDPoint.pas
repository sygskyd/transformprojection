unit AXDPoint;

interface

uses
  ComObj, ActiveX, AXDLL_TLB, StdVcl;

type
  TDPoint = class(TAutoObject, IDPoint)
  private
    x   : Double;
    y   : Double;
  protected
    function Get_x: Double; safecall;
    function Get_y: Double; safecall;
    procedure Set_x(Value: Double); safecall;
    procedure Set_y(Value: Double); safecall;
    { Protected declarations }
  public
    constructor create(ax,ay:double);
  end;

implementation

uses ComServ;

constructor TDPoint.create;
begin
 inherited create;
 x:=ax;
 y:=ay;
end;

function TDPoint.Get_x: Double;
begin
  Result:=x;
end;

function TDPoint.Get_y: Double;
begin
  Result:=y;
end;

procedure TDPoint.Set_x(Value: Double);
begin
  x:=value;
end;

procedure TDPoint.Set_y(Value: Double);
begin
  y:=value;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TDPoint, Class_DPoint,
    ciInternal, tmApartment);
end.
