unit AXLayer;

interface

uses
  ComObj, ActiveX, AXDLL_TLB, StdVcl, AXBase;

type
  TLayer = class(TBase, ILayer)
  private
    FLayer     : POinter;
    Data       : Pointer;      //PProj
  protected
    function Get_Count: Integer; safecall;
    function Get_Items(Index: Integer): IBase; safecall;
    function Get_LayerName: WideString; safecall;
    procedure AssignStyle(const Layer: ILayer); safecall;
    procedure InsertObject(const Item: IBase); safecall;
    function Get_FillStyle: IFillStyle; safecall;
    function Get_GeneralLower: Double; safecall;
    function Get_GeneralUpper: Double; safecall;
    function Get_LineStyle: ILineStyle; safecall;
    function Get_Locked: WordBool; safecall;
    function Get_State: Integer; safecall;
    function Get_SymbolFill: ISymbolFill; safecall;
    function Get_Visible: WordBool; safecall;
    procedure Set_FillStyle(const Value: IFillStyle); safecall;
    procedure Set_GeneralLower(Value: Double); safecall;
    procedure Set_GeneralUpper(Value: Double); safecall;
    procedure Set_LineStyle(const Value: ILineStyle); safecall;
    procedure Set_Locked(Value: WordBool); safecall;
    procedure Set_SymbolFill(const Value: ISymbolFill); safecall;
    procedure Set_Visible(Value: WordBool); safecall;
    function Get_Assigned: WordBool; safecall;
    function SelectByPoint(const APoint: IPoint): IDispatch; safecall;
    function ClipByPoint(const Apoint: IPoint;
      ARadius: Integer): ISelectResult; safecall;
    function Get_Index: Integer; safecall;
//  function Get_HSDVisible: WordBool; safecall;
//  procedure Set_HSDVisible(Value: WordBool); safecall;
    function SnapPointToLine(const APoint: IPoint; Radius: Integer): IPoint;
      safecall;
    function Get_ObjectType: Integer; safecall;
    procedure SelectByIndex(Index: Integer); safecall;
    procedure DeselectByIndex(Index: Integer); safecall;
    procedure DuplicateObject(Item: OleVariant); safecall;
    function InsertObjectEx(const Item: IBase): WideString; safecall;
    procedure SetPosition(Index: Integer); safecall;
    procedure Set_LayerName(const Value: WideString); safecall;
    //function InsertObjectAt(const Item: IDispatch; Index: Integer): WordBool;safecall;
    { Protected declarations }
  public
    constructor create(ALayer,AOWner:Pointer);
  end;

implementation


uses ComServ,am_layer,am_def,am_Index,am_view,am_Child,axPolygon,am_Main,
     AXPolyLine,AXPixel,AXText,AXSymbol,AXMesLine,AXDBText,ListHndl,
     AXCircle,AXArc,AXSpline,AXImage,AXFillStyle,AXLineStyle,am_proj,
     am_Poly,am_CPoly,am_Point,am_Text,am_splin,am_circl,bmpimage,
     am_rtext,am_Meas,am_sym,am_font,axdef,AXSymbolFill,AXSelectResult,am_Obj, SysUtils;


constructor TLayer.create;
  begin
    inherited create;
    FLayer:=ALayer;
    Data:=AOwner;
  end;

function TLayer.Get_Count: Integer;
begin
  Result:=-1;
  if FLayer<>NIL then begin
    Result:=PLayer(FLayer)^.Data^.GetCount;
  end;
end;

function TLayer.Get_Items(Index: Integer): IBase;
var   AIndex  : PIndex;
      AView   : PView;
begin
  Result:=NIL;
  if FLayer<>NIL then begin
    AIndex:=PLayer(FLayer)^.Data^.at(Index);
    if Aindex<>NIl then begin
      AView:=PView(AIndex.Mptr);
      AView:=PView(PLayer(PProj(Data)^.PInfo^.Objects)^.IndexObject(
        PProj(Data)^.PInfo,AIndex));

      if AView = nil then // check if object exists
      begin
         result:=nil;
         exit;
      end;

      // copy the objectstyle
      if AIndex^.ObjectStyle = nil then
      begin
         AView^.ObjectStyle:=nil;
      end
      else
      begin
         AIndex^.CopyObjectStyleTo(AView);
      end;

      if AIndex.GetObjType = ot_CPoly then begin
        Result:=AXPolyGon.TPolygon.Create(Data,AView,false);
      end
      else if AIndex.GetObjType = ot_Poly then begin
        Result:=AXPolyLine.TPolyLine.Create(Data,AView,false);
      end
      else if AIndex.GetObjType = ot_Text then begin
        Result:=AXText.TText.Create(Data,AView,false);
      end
      else if AIndex.GetObjType = ot_Symbol then begin
        Result:=AXSymbol.TSymbol.Create(Data,AView,false);
      end
      else if AIndex.GetObjType = ot_Pixel then begin
        Result:=AXPixel.TPixel.Create(Data,AView,false);
      end
      else if AIndex.GetObjType = ot_Circle then begin
        Result:=AXCircle.TCircle.Create(Data,AView,false);
      end
      else if AIndex.GetObjType = ot_Arc then begin
        Result:=TArc.Create(Data,AView,false);
      end
      else if AIndex.GetObjType = ot_MesLine then begin
        Result:=TMesLine.Create(Data,AView,false);
      end
      else if AIndex.GetObjType = ot_Image then begin
        Result:=AXImage.TImage.Create(Data,AView,false);
      end
      else if AIndex.GetObjType = ot_RText then begin
        Result:=TDBText.Create(Data,AView,false);
      end
      else if AIndex.GetObjType = ot_Spline then begin
        Result:=AXSpline.TSPLine.Create(Data,AView,false);
      end;
    end
  end;
end;


procedure TLayer.InsertObject(const Item: IBase);
var
    AProj       : PProj;
    AView       : PView;
    AType       : integer;
begin

  if FLayer<>NIL then begin
    Aview:=NIL;
    AProj:=PProj(Data);
    AView:=CopyObject(AProj,Item);
    AType:=Item.ObjectType;
    //if not(AType in [ot_Poly,ot_CPoly,ot_Circle,ot_text]) then
    InsertObjectOnLayer(AProj,AView,FLayer);
    if Atype = ot_symbol then begin
      Psymbol(AView)^.Calculatecliprect(AProj^.PInfo);
    end;
    if AType in [ot_Poly,ot_CPoly,ot_pixel,ot_Circle,ot_text,
    ot_symbol,ot_spline,ot_image,ot_RText,ot_mesline,ot_Arc] then
    begin
      // set the Index
      Item.Index:=AView^.Index;
    end;

  end;
end;
function TLayer.Get_LayerName: WideString;
begin
  Result:='';
  if FLayer<>NIl then begin
    Result:=PLayer(FLayer)^.Text^;
  end;
end;

function TLayer.Get_LineStyle: ILineStyle;
begin
  Result:=NIL;
  if FLayer<>NIL then begin
    Result:=AXLineStyle.TLineStyle.Create(@PLayer(FLayer)^.LineStyle,false);
    {
    Result.Width:=PLayer(FLayer)^.LineStyle.Width;
    Result.WidthType:=PLayer(FLayer)^.LineStyle.WidthType;
    Result.Scale:=PLayer(FLayer)^.LineStyle.Scale;
    Result.Color:=PLayer(FLayer)^.LineStyle.Color;
    Result.FillColor:=PLayer(FLayer)^.LineStyle.FillColor;
    }
  end;
end;


function TLayer.Get_Visible: WordBool;
begin
  Result:=not(PLayer(FLayer)^.GetSTate(sf_LayerOff));
end;

procedure TLayer.Set_FillStyle(const Value: IFillStyle);
begin
 if FLayer<>NIL then begin
   PLayer(FLayer)^.Fillstyle.Pattern:=Value.Pattern;
   PLayer(FLayer)^.Fillstyle.ForeColor:=Value.ForeColor;
   PLayer(FLayer)^.Fillstyle.BAckColor:=Value.BackColor;
   PLayer(FLayer)^.Fillstyle.Scale:=Value.Scale;
   PLayer(FLayer)^.Fillstyle.ScaleType:=Value.ScaleType;
  end;
end;


function TLayer.Get_FillStyle: IFillStyle;
var AStyle : IFillStyle;
begin
  Result:=NIL;
  if FLayer<>NIL then begin
    AStyle:=AXFillStyle.TFillStyle.Create(@PLayer(FLayer)^.Fillstyle,false);
    {
    AStyle.Pattern:=PLayer(FLayer)^.Fillstyle.Pattern;
    AStyle.BackColor:=PLayer(FLayer)^.Fillstyle.BAckColor;
    AStyle.Scale:=PLayer(FLayer)^.Fillstyle.Scale;
    AStyle.ScaleType:=PLayer(FLayer)^.Fillstyle.ScaleType;
    }
    Result:=ASTyle;
  end;
end;


procedure TLayer.Set_LineStyle(const Value: ILineStyle);
begin
  if FLayer<>NIL then begin
    PLayer(FLayer)^.LineStyle.Width:=Value.Width;
    PLayer(FLayer)^.LineStyle.WidthType:=Value.WidthType;
    PLayer(FLayer)^.LineStyle.Scale:=Value.Scale;
    PLayer(FLayer)^.LineStyle.Color:=VAlue.Color;
    PLayer(FLayer)^.LineStyle.FillColor:=VAlue.FillColor;
  end;
end;

procedure TLayer.Set_Visible(Value: WordBool);
begin
  PLayer(FLayer)^.SetState(Sf_LayerOff,not(Value));
end;

function TLayer.Get_SymbolFill: ISymbolFill;
begin
  Result:=AXSymbolFill.TSymbolFill.Create;
  SetISymbolFill(Player(FLayer)^.SymbolFill,REsult);
end;

procedure TLayer.Set_SymbolFill(const Value: ISymbolFill);
begin
  if FLayer<>NIL then begin
    SetSymbolFill(Player(FLayer)^.SymbolFill,Value);
  end;
end;


function TLayer.Get_State: Integer;
begin
  Result:=-1;
  If FLayer<>NIL then begin
    Result:=PLayer(FLayer)^.State;
  end;
end;

procedure TLayer.AssignStyle(const Layer: ILayer);
var
    RLayer        : PLayer;
    AFillStyle    : IFillStyle;
    ALineStyle    : ILineStyle;
    ASymbolfill   : ISymbolFill;
begin
  RLayer:=FLayer;
  AFillStyle:=Layer.FillStyle;
  ALineStyle:=Layer.LineStyle;
  AsymbolFill:=Layer.SymbolFill;
  if RLayer<>NIL then begin
    RLayer^.GeneralMin:=Layer.GeneralLower;
    RLayer^.GeneralMax:=Layer.GeneralUpper;
    RLayer^.State:=Layer.State;

    SetFillStyle(RLayer^.FillStyle,AFillStyle);
    SetLineStyle(RLayer^.LineStyle,ALineStyle);
    SetSymbolFill(RLayer^.SymbolFill,ASymbolFill);
  end;
end;

function TLayer.Get_GeneralLower: Double;
begin
  Result:=PLayer(FLayer)^.GeneralMin;
end;

function TLayer.Get_GeneralUpper: Double;
begin
   Result:=PLayer(FLayer)^.GeneralMax;
end;

procedure TLayer.Set_GeneralLower(Value: Double);
begin
  PLayer(FLayer)^.GeneralMin:=Value;
end;

procedure TLayer.Set_GeneralUpper(Value: Double);
begin
  PLayer(FLayer)^.GeneralMax:=Value;
end;

function TLayer.Get_Locked: WordBool;
begin
  Result:=PLayer(FLayer)^.GetState(sf_fixed);
end;

procedure TLayer.Set_Locked(Value: WordBool);
begin
  PLayer(FLayer)^.SetState(sf_fixed,Value);
end;

function TLayer.Get_Assigned: WordBool;
begin
  Result:=false;
  If FLayer<>nIL then Result:=true;
end;

function TLayer.SelectByPoint(const APoint: IPoint): IDispatch;
begin

end;

function TLayer.ClipByPoint(const Apoint: IPoint;
  ARadius: Integer): ISelectResult;
  var BPOInt    : TDPoint;
      RPoint    : TDpoint;
      APoly     : PPoly;
      cnt       : integer;
begin
REsult:=NIL;
if FLayer<>NIL then begin
  Bpoint.x:=APOint.x;
  BPoint.y:=APOint.y;
  cnt:=-1;
  APoly:=PPoly(PLayer(FLayer)^.CutByPOint(PProj(Data)^.PInfo,BPoint,[ot_Poly],cnt,RPOint,ARadius));
  if APOly<>NIl then
  REsult:=TSelectREsult.create(RPoint.x,RPOint.y,APoly) else
  Result:=NIL;
end;
end;

function TLayer.Get_Index: Integer;
begin
  Result:=-1;
  if FLayer<>NIL then begin
    Result:=PLayer(FLayer)^.Index;
  end;
end;
    {
function TLayer.InsertObjectAt(const Item: IDispatch;
  Index: Integer): WordBool;
var AIndex : PIndex;
begin
  Result:=false;
  if FLayer<>NIL then begin
    AIndex:=PLayer(FLayer)^.Data^.at(Index);
    if Aindex<>NIl then begin
      if AIndex^.Index<>Index then begin
        result:=true;
        PObjects(PProj(Data)^.PInfo^.Objects)^.LastPoly:=1;
      end;
    end;
  end;
end;
     }

{
function TLayer.Get_HSDVisible: WordBool;
begin
  Result:=false;
  if FLayer<>NIL then begin
    Result:=PLayer(FLayer)^.getSTate(sf_HSDVisible);
  end;
end;

procedure TLayer.Set_HSDVisible(Value: WordBool);
begin
  if FLayer<>NIL then begin
    PLayer(FLayer)^.SetSTate(sf_HSDVisible,Value);
  end;
end; }

function TLayer.SnapPointToLine(const APoint: IPoint; Radius: Integer): IPoint;
var BPoint : TDPoint;
    RPoint : PDPoint;
begin
  Result:=NIL;
  if FLayer<>NIL then begin
    BPoint.Init(APoint.x,APoint.Y);
    RPoint:=New(PDPoint,INit(0,0));
    //if PLayer(FLayer)^.SnapPointToLIne(PProj(Data)^.PInfo,BPoint,RPoint,Radius) then
    //  REsult:=AXPoint.TPoint.Create(RPoint,true);
  end;
end;

function TLayer.Get_ObjectType: Integer;
begin
  Result:=-1;
  if FLayer<>NIL then begin
    REsult:=PLayer(FLayer)^.GetObjtype;
  end;
end;

procedure TLayer.SelectByIndex(Index: Integer);
begin

end;

procedure TLayer.DeselectByIndex(Index: Integer);
begin

end;

procedure TLayer.DuplicateObject(Item: OleVariant);
begin

end;

function TLayer.InsertObjectEx(const Item: IBase): WideString;
var
    AProj       : PProj;
    AView       : PView;
    AType       : integer;
begin

  if FLayer<>NIL then begin
    Aview:=NIL;
    AProj:=PProj(Data);
    AView:=CopyObject(AProj,Item);
    AType:=Item.ObjectType;
    //if not(AType in [ot_Poly,ot_CPoly,ot_Circle,ot_text]) then
    InsertObjectOnLayer(AProj,AView,FLayer);
{++ IDB_AXInterface}

{-- IDB_AXInterface}
    if Atype = ot_symbol then begin
      Psymbol(AView)^.Calculatecliprect(AProj^.PInfo);
    end;
    if AType in [ot_Poly,ot_CPoly,ot_pixel,ot_Circle,ot_text,
    ot_symbol,ot_spline,ot_image,ot_RText,ot_mesline,ot_Arc] then begin
      Item.Index:=AView^.Index;
    end;
    Result:=IntToStr(AView^.Index);
  end;
end;

procedure TLayer.SetPosition(Index: Integer);
var
OldPos: Integer;
OldTopLayer: PLayer;
begin
     if (FLayer <> nil) and (Index > -1) then begin
        OldPos:=PProj(Data)^.Layers^.LData^.IndexOf(FLayer);
        if Index < PProj(Data)^.Layers^.LData^.Count-1 then begin
           if OldPos-1 <> Index then begin
              OldTopLayer:=PProj(Data)^.Layers^.TopLayer;
              if (Index = 0) or (OldPos = 1) then begin
                 PProj(Data)^.DeselectAll(False);
              end;
              PProj(Data)^.Layers^.LData^.AtDelete(OldPos);
              PProj(Data)^.Layers^.NumberLayers;
              PProj(Data)^.Layers^.LData^.AtInsert(Index+1,FLayer);
              PProj(Data)^.Layers^.NumberLayers;
              PProj(Data)^.Layers^.SetTopLayer(OldTopLayer);

              UpdateLayersViewsLegendsLists(Data);
              PProj(Data)^.SetModified;
          end;
        end;
      end;
end;

procedure TLayer.Set_LayerName(const Value: WideString);
begin
   if FLayer <> nil then begin
      PLayer(FLayer)^.SetText(String(Value));
   end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TLayer, Class_Layer,
    ciInternal, tmApartment);
end.
