unit AX_proj;

interface
uses am_main, graphics,Classes,Grtools,Forms,am_def,wintypes,udraw,uheap,uglob,am_Paint,regdb,
     measures,numtools,flatsb,lists,Objects,uvalidator, am_pass;

const vnPRoject = 28;

type TProjectState      = Set of (psUpdateScrollers);
     TDrawThread = class;
     TXProj = class
      private
        FREady            : Boolean;
        FData             : Pointer;
      Public
        FileName         : string;
        Parent           : TForm;      //nicht benutzt
        Invalidator        : TInvalidator;
        ClientID         : integer;
        Size             : TDRect;
        PInfo            : PPaint;
        CurrentViewRect  : TRotRect;
        BMPLoadAction    : Boolean;
        ScrollXPos       : Double;
        ScrollYPos       : Double;
        ScrollXStep      : Double;
        ScrollYStep      : Double;
        ScrollXOffset    : Double;
        ScrollYOffset    : Double;
        ScrollAssigned   : Boolean;
        BmpPixelsExact   : Boolean;
        Overview         : Boolean;
        drawn            : boolean;
        ZoomRect         : TDRect;
        AHeapmanager     : THeap;
        Document         : TDocument;
        Registry         : TRegistryDataBase;
        FileVersion      : Integer;
        ProjectState     : TProjectState;
        PreviousZooms    : TPackedRecList;
        DrawThread       : TDrawThread;
        DRawThreadRunning : Boolean;
        ProjectSizeAssigned : Boolean;
        ImageStream         : TMemorySTream;

        OldSymbols       : Pointer;
        OldFonts         : Pointer;
        OldObjects       : Pointer;


        Constructor Create(AInvalidator:TINvalidator;AfileName:String);
        Destructor  Destroy; override;
        Procedure   InitObject;


        Function    Load1(AFileName:string):Boolean;
        PRocedure   OnError(ACode:Integer);

        PRoperty    REady:Boolean read FREady;
        PRoperty    Data: Pointer read FData write FDATA;
        function    Load:Boolean;
        Procedure   OnNew;
        Procedure   New1;
        Function    Save:Boolean;
     end;

     TDrawThread = class(TThread)
        private
          { Private-Deklarationen}
          FProj  : TXProj;
        protected
          procedure Execute; override;
          property  Proj : TXProj read FProj write FProj;
        public
          constructor Create(AProj : TXProj);
        end;

implementation

uses Xlines,ExtCanvas,SysUtils,am_Ole,am_Proj,am_ini;

Constructor TXProj.Create;
var i,j      : integer;
    aLine  : TXlineStyle;
  begin
    inherited Create;
    FileName:=AFileName;
    Invalidator:=AInvalidator;
    ImageStream:=TMemoryStream.create;
    PInfo:=new(PPaint,Init);
    ProjectVersion:=VnProject;
    Fileversion:=vnProject;
    //DrawThread:=TDrawThread.Create(Self);

     {
    for i:=0 to PInfo^.ProjStyles.XLineStyles.Capacity-1 do begin
      ALIne:=(PInfo^.PRojStyles.XLIneStyles.Items[i]);
      if Aline<>NIL then begin
        j:=0;
      end;
    end;
    }
  end;

Procedure TXProj.OnNew;
  begin
    New1;
    Registry:=PProj(FData)^.Registry;
    InitObject;
  end;

Procedure TXProj.New1;
var S            : TOLEClientStream;
    KnowPwd      : Boolean;
    Check        : Word;
    Version      : Byte;
    AFlags       : Byte;
    AData        : PProj;
    aName        : Array [0..255] of Char;
  begin
begin
      StrPCopy(AName,FileName);
      if CompareText(ExtractFileName(AName),'Default.amp')<>0 then begin
        S.Init('Default.amp',stOpenRead,am_def.StrBufSize);
        S.ParentWindow:=nil;
        if S.Status<>stOK then begin
          //MsgBox(Handle,2014,mb_OK or mb_IConExclamation,'');
          OnError(2014);
          FData:=New(PProj,Init);
        end
        else FData:=Pointer(S.Get);
        S.Done;
      end
      else FData:=New(PProj,Init);
      PProj(FData)^.Pinfo^.Fonts^.CheckInsertFonts(0);
    end;
  end;

function TXProj.Load:Boolean;
  begin
  //Load
    Result:=Load1(FileName);
    if Result then begin
      Registry:=PProj(FData)^.Registry;
      InitObject;
      Pinfo^.ProjectSize.InitByRect(PProj(FData)^.size);
    end;
    //end load
  end;

Function TXProj.load1(AFileName:string):Boolean;
var S            : TOLEClientStream;
    KnowPwd      : Boolean;
    Check        : Word;
    Version      : Byte;
    AFlags       : Byte;
    AData        : PProj;
    AFName        : Array [0..255] of Char;
    LTPwd        : String[100];
  begin
    Result:=false;
    StrPCopy(AFName,FileName);
    S.Init(AFName,Objects.stOpenRead,am_def.StrBufSize);
    S.Read(Check,SizeOf(Check));
    S.Read(Version,SizeOf(Version));
    S.EachBMP:=False;
    KnowPwd:=TRUE;

    if Version > 200 then begin
      KnowPwd:=TRUE;
      Version:=Version-200;
      // S.Read(LTPwd,SizeOf(LTPwd));
      // LTPwd:=Uncode(LTPwd);
      // if CheckLTPassword(WinGISMainForm,LTPwd) then KnowPwd:=TRUE;
      // if CheckLTPassword(NIL,LTPwd) then KnowPwd:=TRUE;
    end;

    if KnowPwd then begin
      S.Read(AFlags,SizeOf(AFlags));
      S.ParentWindow := NIL;    //MainWindow
      S.Seek(0);
      if Check=rn_Proj then begin
        if Version>vnProject then OnError(3613)
        else begin
          AData:=Pointer(S.Get);
          FData:=AData;
          if {(S.Status=stOK) and} (AData<>NIL) and (AData^.LoadResult) then begin
            Result:=TRUE;
          end
          else begin
            if (AData=NIL) or (AData^.LoadResult) then OnError(1005);
            if AData<>NIL then Dispose(AData,Done);
            AData:=NIL;
          end;
        end;
      end
      else OnError(1005);
    end;
    S.Done;
    if Result then PProj(FData)^.Pinfo^.Fonts^.CheckInsertFonts(0);
  end;

function TXProj.Save:Boolean;
  var S            : TBufStream;
    ABuffer      : Array[0..255] of Char;
    TempFileName : String;
    BakFileName  : String;
    wgpFileName  : String;
    FAttr        : Integer;
    IsReadOnly   : Boolean;
    BAckUpFile   : Boolean;
begin
    REsult:=false;
    TempFileName:=ChangeFileExt(FileName,'.$$$');
    BakFileName:=ChangeFileExt(FileName,'.BAK');
    //wgp settings
    WGPFileName:=ChangeFileExt(FileName,'.HSD');
    PProj(FData)^.Extmap:=true;
    PProj(FData)^.BaseFile:=WGPFileName;
    //
    S.Init(StrPCopy(ABuffer,TempFileName),stCreate,am_def.StrBufSize);
    PProj(FData)^.Registry.WriteDateTime('\Project\Info\ModificationDate',Now);
    S.Put(FData);
    S.Done;
    if S.Status=stOK then begin
      REsult:=true;
      BackupFile:=False;
      if BackupFile then begin
        if FileExists(BakFileName) then DeleteFile(StrPCopy(ABuffer,BakFileName));
        if FileExists(FileName) then Result:=RenameFile(FileName,BakFileName);
      end
      else if FileExists(FileName) then Result:=DeleteFile(FileName);
      if Result then Result:=RenameFile(TempFileName,FileName);
    end;

    if not Result then begin
      DeleteFile(StrPCopy(ABuffer,TempFileName));
      if S.Status <> -99 then OnError(4101);
    end
    else begin
      PProj(FData)^.Modified:=FALSE;
      PProj(FData)^.FileVersion:=vnProject;
    end;
  end;


Destructor TXProj.destroy;
  begin
    //DrawThread.Suspend;
    //DrawThread.Free;

    //unload
    {if OldFonts<>NIL then Pinfo^.Fonts:=OldFonts;
    if OldSymbols<>NIL then PInfo^.Symbols:=OldSymbols;
    if OldObjects<>NIL then PInfo^.Objects:=OldObjects;
    }
    Dispose(PINfo,done);
    //end unload

    PreviousZooms.Free;
    Imagestream.free;
    if FData<>NIL then begin
      Dispose(PProj(FData),Done);
    end;
    inherited Destroy;
  end;

Procedure TXPRoj.InitObject;
  begin
    ScrollXOffset:=0;
    ScrollYOffset:=0;
    ScrollXStep:=0;
    ScrollYStep:=0;
    BmpPixelsExact:=true;
    PreviousZooms:=TPackedRecList.Create(SizeOf(TRotRect));

 with Registry do begin
      OpenKey('\Project\Settings',FALSE);
      // scale factor
      PInfo^.ProjectScale:=ReadFloat('Scale');
      if LastError<>rdbeNoError then PInfo^.ProjectScale:=1/10000;
      // project units
      PInfo^.SetProjectUnits(TMeasureUnits(ReadInteger('Units')));
      if LastError<>rdbeNoError then PInfo^.SetProjectUnits(muMeters);
      // coordinate system
      ReadInteger('CoordinateSystem');
      //if LastError<>rdbeNoError then PInfo^.CoordinateSystem:=csGeodatic;
      // color palette
      PInfo^.ProjStyles.Palette.ReadFromRegistry(Registry,'\Project\Palette');
      // extended line-styles
      PInfo^.ProjStyles.XLineStyles.ReadFromRegistry(Registry,'\Project\XLineStyles');
      // extended fill-styles
      if FileVersion>=20 then PInfo^.ProjStyles.XFillStyles.
          ReadFromRegistry(Registry,'\Project\XFillStyles');
      // selection options
      with PInfo^.SelectionSettings do begin
        OpenKey('\Project\Settings\Selection',FALSE);
        UseGlobals:=ReadBool('UseGlobals');
        if LastError<>rdbeNoError then UseGlobals:=TRUE;
        if not UseGlobals then begin
          DBSelectionMode:=ReadBool('DBSelectionMode');
          TransparentMode:=ReadInteger('TransparentMode');
          ReadLineStyleFromRegistry(Registry,'\Project\Settings\Selection\LineStyle',LineStyle);
          ReadFillStyleFromRegistry(Registry,'\Project\Settings\Selection\FillStyle',FillStyle);
        end;
      end;
      // various options
      with PInfo^.VariousSettings do begin
        OpenKey('\Project\Settings\Various',FALSE);
        UseGlobals:=ReadBool('UseGlobals');
        if LastError<>rdbeNoError then UseGlobals:=TRUE;
        if not UseGlobals then begin
          ShowEdgePoints:=ReadBool('ShowEdgePoints');
          ZoomSize:=ReadFloat('ZoomSize');
          ZoomSizeType:=ReadInteger('ZoomSizeType');
          CombineRadius:=ReadFloat('CombineRadius');
          CombineRadiusType:=ReadInteger('CombineRadiusType');
          SnapToNearest:=ReadBool('SnapToNearest');
          SnapRadius:=ReadFloat('SnapRadius');
          SnapRadiusType:=ReadInteger('SnapRadiusType');
        end;  
      end;
      // bitmap options
      with PInfo^.BitmapSettings do begin
        OpenKey('\Project\Settings\Bitmaps',FALSE);
        UseGlobals:=ReadBool('UseGlobals');
        if LastError<>rdbeNoError then UseGlobals:=TRUE;
        if not UseGlobals then begin
          ShowBitmap:=ReadBool('ShowBitmap');
          ShowFrame:=ReadBool('ShowFrame');
          ShowMinSize:=ReadInteger('ShowMinSize');
          Transparency:=ReadInteger('Transparency');
          TransparencyType:=ReadInteger('TransparencyType');
        end;
      end;
      // read floating text settings
      OpenKey('\Project\Settings\FloatingText',False);
    end;

  end;


constructor TDrawThread.Create(APRoj:TXProj);
  begin
    inherited Create(true);  //Thread wird  angehalten nach STart
    Proj:=aProj;
    ReturnValue:=0;
    FreeOnTerminate:=true;  // Thread wird nach Beendigung automatisch freigegebn
    //OnTerminate:=Proj.DrawThreadTerminating;
  end;

procedure TDrawThread.Execute;      //TThread
begin
  { Plazieren Sie den Thread-Quelltext hier}
  while not terminated do begin
    if Proj<>NIL then with Proj do begin
      if not DrawThreadrunning then begin
         DrawThreadrunning:=true;

         {
         if Document.Loaded then begin
            BHeapManager:=AHeapManager;
            TGlobal(AHeapManager.GlobalObj).DRawLst.ResetAll;
            Document.Draw;
         end;
         }
         PProj(FData)^.DrawGraphic;

         //Proj.EndDrawing;
         DrawThreadrunning:=false;
         FREady:=true;
         Suspend;
      end;
     end;
   end;
end;
               {
Procedure TXProj.HScroll;
  begin
    SCrollXPos:=ScrollXPos+Item;
    OnScroll;
  end;

Procedure TXProj.VScroll;
  begin
    SCrollYPos:=ScrollYPos+Item;
    OnScroll;
  end;

Procedure TXProj.EndDrawing;
  begin
    Pinfo^.ExtCanvas.Screenshot;
    Invalidator.Invalidate;
  end;
                }
PRocedure TXProj.ONError(ACode:Integer);
  begin
  end;

end.
