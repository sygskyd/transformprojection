library AXDLL;

uses
  ComServ,
  Windows,
  AXDLL_TLB in 'AXDLL_TLB.pas',
  AXCore in 'AXCore.pas' {Core: CoClass},
  AXDocument in 'AXDocument.pas' {Document: CoClass},
  AXLayers in 'AXLayers.pas' {Layers: CoClass},
  AXLayer in 'AXLayer.pas' {Layer: CoClass},
  AXList in 'AXList.pas' {List: CoClass},
  AXPoint in 'AXPoint.pas' {Point: CoClass},
  AXPixel in 'AXPixel.pas' {Pixel: CoClass},
  AXPolygon in 'AXPolygon.pas' {Polygon: CoClass},
  AXPolyline in 'AXPolyline.pas' {Polyline: CoClass},
  AXText in 'AXText.pas' {Text: CoClass},
  AXParent in 'AXParent.pas' {Parent: CoClass},
  AXSymbol in 'AXSymbol.pas' {Symbol: CoClass},
  AXSpline in 'AXSpline.pas' {Spline: CoClass},
  AXCircle in 'AXCircle.pas' {Circle: CoClass},
  AXArc in 'AXArc.pas' {Arc: CoClass},
  AXImage in 'AXImage.pas' {Image: CoClass},
  AXDBText in 'AXDBText.pas' {DBText: CoClass},
  AXMesLine in 'AXMesLine.pas' {MesLine: CoClass},
  AXSymbols in 'AXSymbols.pas' {Symbols: CoClass},
  AXSymbolDef in 'AXSymbolDef.pas' {SymbolDef: CoClass},
  AXObjectstyle in 'AXObjectstyle.pas' {ObjectStyle: CoClass},
  AXRect in 'AXRect.pas' {Rect: CoClass},
  AXLinestyle in 'AXLinestyle.pas' {LineStyle: CoClass},
  AXFillStyle in 'AXFillStyle.pas' {FillStyle: CoClass},
  AXSymbolFill in 'AXSymbolFill.pas' {SymbolFill: CoClass},
  AXDPoint in 'AXDPoint.pas' {DPoint: CoClass},
  AXFonts in 'AXFonts.pas' {Fonts: CoClass},
  AXFontDef in 'AXFontDef.pas' {FontDef: CoClass},
  AXCustomerItem in 'AXCustomerItem.pas' {CustomerItem: CoClass},
  AXCustomers in 'AXCustomers.pas' {Customers: CoClass},
  AXSelectResult in 'AXSelectResult.pas' {SelectResult: CoClass},
  AXGenerateObject in 'AXGenerateObject.pas' {GenerateObject: CoClass},
  AXLine in 'AXLine.pas' {Line: CoClass},
  AXBase in 'AXBase.pas' {Base: CoClass},
  AXSight in 'AXSight.pas',
  AXSights in 'AXSights.pas',
  AXSightLData in 'AXSightLData.pas';

function DllCanUnloadNow: HResult; stdcall;
begin
     Result:=1;
end;

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}
{$R *.RES}

begin

end.
