unit Preferences;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, ExtCtrls, Grids, Outline, DirOutln, StdCtrls, Localize, TabFiles;

type
  TPreferencesForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    StoreButton: TSpeedButton;
    CancelButton: TSpeedButton;
    GroupBox1: TGroupBox;
    OverwriteTAB: TCheckBox;
    UnitsComboBox: TComboBox;
    procedure CancelButtonClick(Sender: TObject);
    procedure StoreButtonClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PreferencesForm: TPreferencesForm;
  HelpLanguage: LangType = English; { English }
//  OverWriteTABs : Boolean;
  ShowToolBar : Boolean;
  MapUnits : TabUnits;

implementation

{$R *.DFM}
uses
  Main, FileCtrl;

procedure TPreferencesForm.CancelButtonClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TPreferencesForm.StoreButtonClick(Sender: TObject);
begin
  ModalResult := mrOk;
  MapUnits := TabUnits(UnitsComboBox.ItemIndex);
  MultFactor := UnitsMultFactor(MapUnits);
end;

procedure TPreferencesForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
  begin
    Key := 0;
    CancelButtonClick(Sender);
  end;
end;

procedure TPreferencesForm.FormShow(Sender: TObject);
begin
   { Set user values }
  with Self do
  begin
    Width := MainForm.Width;
    Height := MainForm.Height;
    Left := MainForm.Left;
    Top := MainForm.Top;
  end;
  OverWriteTAB.Checked := OverWriteTABs;
  UnitsComboBox.ItemIndex := Integer(MapUnits);
end;

end.

