{
 This unit contains routine to convert TAB file for corresponding TIFF to TFW file
 for ARC/INFO/ArcView
}
unit ProcessTABFile;

interface
uses Windows, TrLib32, Main, SysUtils, Dialogs, Controls,
  Localize, Classes, TabFiles;

function ProcessTABFile(Name: AnsiString): Boolean;

implementation

uses
  Forms, Math;

function ProcessTABFile(Name: AnsiString): Boolean;
var
  PageCnt: Integer;
  ImgFile: AnsiString;
  IsVMSI: Boolean;
  PageCount {, FileVolume}: Integer;
  PInfo: PBITMAPINFO;
  TFWText: TStringList;
  ExtList: AnsiString;
  W, H: Integer;
  RefX, RefY, SizeX, SizeY: double;
  LineIdx: Integer;
  Rect: TRect;
begin
  Result := False;
  TFWText := TStringList.Create;
  TFWText.LoadFromFile(Name);
  with TFWText do
  try // to free TFWText object
    LineIdx := 0;
    while LineIdx < Count do
    begin
      Strings[LineIdx] := Trim(Strings[LineIdx]);
      if Strings[LineIdx] = EmptyStr then
        Delete(LineIdx)
      else
        Inc(LineIdx);
    end;
    if Count < 6 then
    begin
      AddText('...but it seems not to be a TFW file: only ' + IntToStr(Count) + ' text lines!');
      Exit;
    end;
    ImgFile := ChangeFileExt(Name, TIFExt);
    if not FileIsImageOne(ImgFile) then
    begin
      AddText('...but the corresponding image isn''t from the follow list:');
      ExtList := '"' + TrLib.GetImpStrings + '".';
      AddText(ExtList);
      Exit;
    end
    else
      AddText('corresponding "' + ExtractFileName(ImgFile) + '" found...');
    PageCnt := TrLib.GetPageCount(@ImgFile[1]);
    if PageCnt > 1 then
    begin
      AddText('...but it has ' + IntToStr(PageCnt) + ' pages that isn''t supported, sorry.');
      Exit;
    end;
    with TrLib do
    begin
      PInfo := GetHeader(PChar(ImgFile), PageCount, IsVMSI);
      if PInfo = nil then
      begin
        AddText('...but it hasn''t a valuable header.');
        Exit;
      end;
    end;
    try // to free PInfo at end
      try // to detect conversion error
        LineIdx := 0;
        SizeX := StrToFloat(TFWTExt[0]);
        LineIdx := 3;
        SizeY := StrToFloat(TFWTExt[3]);
        LineIdx := 4;
        RefX := StrToFloat(TFWTExt[4]);
        LineIdx := 5;
        RefY := StrToFloat(TFWTExt[5]);
      except
        AddText('...but it has an illegal line #' + IntToStr(LineIdx + 1) + ' :');
        AddText('"' + TFWText[LineIdx] + '"');
        Exit;
      end;
      W := PInfo.bmiHeader.biWidth;
      H := PInfo.bmiHeader.biHeight;
    finally
      VirtualFree(PInfo, 0, MEM_RELEASE);
    end;
    // I multiply to 100 to scale meters from TFW to centimeters of Progis TAB
    Rect.Left := Round((RefX - SizeX / 2.0) * 100.0);
    Rect.Top := Round((RefY - SizeY / 2.0) * 100.0);
    Rect.Right := Rect.Left + Round(W * SizeX * 100.0);
    Rect.Bottom := Rect.Top + Round(H * SizeY * 100.0);
    if CreateTABFile(ImgFile, W, H, @Rect, []) then
    begin
      AddText('Successfully converted to TAB...');
      Result := True;
    end
    else
      AddText('Failure:' + TrLib.GetErrorText);
  finally
    TFWText.Free;
  end;
end;

end.

