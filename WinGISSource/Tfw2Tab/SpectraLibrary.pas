// SpectraLibrary
// Copyright (C) 1998, 2000, Earl F. Glynn, Overland Park, KS.
// May be copied freely for non-commercial use.
// Not to be sold for profit without permission.
//
// This unit originally appeared in the Spectra Lab Report
// www.efg2.com/Lab/ScienceAndEngineering/Spectra.htm
//
// All TColor values used in this unit are assumed to be of the form
// $00bbggrr.  For details about TColor, see "TColor" at
// www.efg2.com/Lab/Library/Delpi/Graphics/VCLRTL.htm.

unit SpectraLibrary;

interface

uses
  Windows, // TRGBTriple
  Graphics; // TColor

type
  Nanometers = DOUBLE;

const
  WavelengthMinimum = 380; // Nanometers
  WavelengthMaximum = 780;

  // Overload for later convenience
function WavelengthToRGB(const Wavelength: Nanometers): TColor; OVERLOAD;
procedure WavelengthToRGB(const Wavelength: Nanometers; var R, G, B: BYTE); OVERLOAD;

function Rainbow(const fraction: DOUBLE): TColor;
function ColorInterpolate(const fraction: DOUBLE;
  const Color1, Color2: TColor): TColor;

function ColorToRGBTriple(const Color: TColor): TRGBTriple;

implementation

uses
  Math; // Power

function WavelengthToRGB(const Wavelength: Nanometers): TColor;
var
  R: BYTE;
  G: BYTE;
  B: BYTE;
begin
  WavelengthToRGB(Wavelength, R, G, B);
  Result := RGB(R, G, B);
end {WavelengthToRGB};

  // Adapted from www.isc.tamu.edu/~astro/color.html

procedure WavelengthToRGB(const Wavelength: Nanometers; var R, G, B: BYTE);

const
  Gamma = 0.80;
  IntensityMax = 255;

var
  Blue: DOUBLE;
  factor: DOUBLE;
  Green: DOUBLE;
  Red: DOUBLE;

  function Adjust(const Color, Factor: DOUBLE): INTEGER;
  begin
    if Color = 0.0 then
      RESULT := 0 // Don't want 0^x = 1 for x <> 0
    else
      RESULT := ROUND(IntensityMax * Power(Color * Factor, Gamma))
  end {Adjust};

begin

  case TRUNC(Wavelength) of
    380..439:
      begin
        Red := -(Wavelength - 440) / (440 - 380);
        Green := 0.0;
        Blue := 1.0
      end;

    440..489:
      begin
        Red := 0.0;
        Green := (Wavelength - 440) / (490 - 440);
        Blue := 1.0
      end;

    490..509:
      begin
        Red := 0.0;
        Green := 1.0;
        Blue := -(Wavelength - 510) / (510 - 490)
      end;

    510..579:
      begin
        Red := (Wavelength - 510) / (580 - 510);
        Green := 1.0;
        Blue := 0.0
      end;

    580..644:
      begin
        Red := 1.0;
        Green := -(Wavelength - 645) / (645 - 580);
        Blue := 0.0
      end;

    645..780:
      begin
        Red := 1.0;
        Green := 0.0;
        Blue := 0.0
      end;

  else
    Red := 0.0;
    Green := 0.0;
    Blue := 0.0
  end;

    // Let the intensity fall off near the vision limits
  case TRUNC(Wavelength) of
    380..419: factor := 0.3 + 0.7 * (Wavelength - 380) / (420 - 380);
    420..700: factor := 1.0;
    701..780:
      factor := 0.3 + 0.7 * (780 - Wavelength) / (780 - 700)
  else
    factor := 0.0
  end;

  R := Adjust(Red, Factor);
  G := Adjust(Green, Factor);
  B := Adjust(Blue, Factor)
end {WavelengthToRGB};

  // Fraction ranges from 0.0 (WavelengthMinimum) to 1.0 (WavelengthMaximum)

function Rainbow(const fraction: DOUBLE): TColor;
begin
  if (fraction < 0.0) or (fraction > 1.0) then
    RESULT := clBlack
  else
  begin
    RESULT := WavelengthToRGB(WavelengthMinimum +
      fraction * (WavelengthMaximum - WavelengthMinimum))
  end
end {Raindbow};

  // While a mathematical "linear interpolation" is used here, this is a
  // non-linear interpolation in color perception space.  Fraction is assumed
  // to be from 0.0 to 1.0, but this is not enforced.  Returns Color1 for
  // fraction = 0.0 and Color2 for fraction = 1.0.

function ColorInterpolate(const fraction: DOUBLE;
  const Color1, Color2: TColor): TColor;
var
  complement: Double;
  R1, R2: BYTE;
  G1, G2: BYTE;
  B1, B2: BYTE;
begin
  if fraction <= 0 then
    RESULT := Color1
  else
    if fraction >= 1.0 then
      RESULT := Color2
    else
    begin
      R1 := GetRValue(Color1);
      G1 := GetGValue(Color1);
      B1 := GetBValue(Color1);

      R2 := GetRValue(Color2);
      G2 := GetGValue(Color2);
      B2 := GetBValue(Color2);

      complement := 1.0 - fraction;
      RESULT := RGB(ROUND(complement * R1 + fraction * R2),
        ROUND(complement * G1 + fraction * G2),
        ROUND(complement * B1 + fraction * B2))
    end
end {ColorInterpolate};

  // Conversion utility routines

function ColorToRGBTriple(const Color: TColor): TRGBTriple;
begin
  with RESULT do
  begin
    rgbtRed := GetRValue(Color);
    rgbtGreen := GetGValue(Color);
    rgbtBlue := GetBValue(Color)
  end
end {ColorToRGBTriple};

function RGBTripleToColor(const Triple: TRGBTriple): TColor;
begin
  RESULT := RGB(Triple.rgbtRed, Triple.rgbtGreen, Triple.rgbtBlue)
end {RGBTripleToColor};

end.

