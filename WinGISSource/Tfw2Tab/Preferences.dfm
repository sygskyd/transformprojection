�
 TPREFERENCESFORM 0�
  TPF0TPreferencesFormPreferencesFormLeft
Top;HorzScrollBar.VisibleBorderIconsbiSystemMenu BorderStylebsSingleBorderWidthCaptionPreferencesClientHeight� ClientWidth� Color	clBtnFaceConstraints.MinHeight Constraints.MinWidth Font.CharsetDEFAULT_CHARSET
Font.Color @Font.Height�	Font.NameMS Sans Serif
Font.Style 	FormStylefsStayOnTop
KeyPreview	OldCreateOrderPosition	poDefaultShowHint	
OnActivateFormShow	OnKeyDownFormKeyDownOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top Width� Height� AlignalClient
BevelInner	bvLowered
BevelOuterbvNoneParentColor	TabOrder 	TGroupBox	GroupBox1LeftTopWidth� Height� AlignalClientCaptionOutput parametersTabOrder  	TCheckBoxOverwriteTABLeft	TopWidth� HeightHintPSet if you want to overwrite existing *.PRI files, clear if want to skip them...CaptionOverwrite existing filesTabOrder   	TComboBoxUnitsComboBoxLeft
Top<Width� HeightHintSelect map units in TFW file
ItemHeightItems.StringsMM - millimetersCM - centimetersM  - metersKM - kilometersPX - pixels (not in use)IN - inches TabOrder    TPanelPanel2Left Top� Width� Height)AlignalBottom
BevelOuter	bvLoweredParentColor	TabOrder  TSpeedButtonStoreButtonLeft4TopWidthQHeightCursorcrHandPointHintUse settings and return to VMSIAnchorsakTopakRight Caption&StoreFlat	Font.CharsetDEFAULT_CHARSET
Font.Color @Font.Height�	Font.NameMS Sans Serif
Font.Style 
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUUUUUUUUUUUUUUUUUUUUUU�UUUUUUYUUUUUUW�UUUUU��UUUUUUww�UUUUU��UUUUUUww�UUUUY��UUUUWwwUUUU���UUUUwww�UUW���UUUWwuww�UUyUY�UUUwuUWw�UUUUUY�UUUUUWwUUUUUU�UUUUUUw�UUUUUY�UUUUUUWw�UUUUUUyUUUUUUw�UUUUUW�UUUUUUWw�UUUUUUY�UUUUUUWwUUUUUUUUUUUUUUUU	NumGlyphs
ParentFontOnClickStoreButtonClick  TSpeedButtonCancelButtonLeft� TopWidthQHeightCursorcrHandPointHintESC to cancel current changesAnchorsakTopakRight Caption&CancelFlat	Font.CharsetDEFAULT_CHARSET
Font.Color @Font.Height�	Font.NameMS Sans Serif
Font.Style 
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333333333333333?�3�3?�3��73��3w��ws39�0 9�337w�w�w333����333wsws3339�9�33337w7w33333�	�33333ss3333313333337w�33333��333337w?�3339� �333377w7�333�� ��333w7w7�39�0 9�337w7w�w�3��73��3ws3s3ws333333333333333333333333333333333333333333333333	NumGlyphs
ParentFontOnClickCancelButtonClick    