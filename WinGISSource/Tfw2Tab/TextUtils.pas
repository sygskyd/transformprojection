unit TextUtils;

interface

uses SysUtils, Windows;

type
  TCharSet = set of Char;

const
  Q_StdDelimsSet = [#0..#32, '!', '"', '(', ')', '*', '+', ',', '-', '.', '/', ':',
    ';', '<', '=', '>', '?', '[', '\', ']', '^', '{', '}', '|', ''''];

  Q_MonthsEng: array[1..12] of string =
    ('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
    'September', 'October', 'November', 'December');

  Q_SMonthsEng: array[1..12] of string =
    ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');

  Q_WeekDaysEng: array[1..7] of string =
    ('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

procedure Q_SpaceCompressInPlace(var S: AnsiString); overload;
function Q_SpaceCompressInPlace1(var S: AnsiString): AnsiString; overload;
function Q_GetWordN(OrdN: Integer; const S: AnsiString; const Delimiters: TCharSet = Q_StdDelimsSet): AnsiString;
function HexToUInt(const S: AnsiString): DWORD;
// Free style conversion routime
function TimeToStrF(const T: TDateTime; IncludeMSecs: Boolean = False): AnsiString;

{ Q_DelCharInPlace ������� ��� ������� Ch �� ������ S. �������� ������ ���
  ���� ����������. }

procedure Q_DelCharInPlace(var S: AnsiString; Ch: Char = ' ');

{ Q_DelChar ������� ��� ������� Ch �� ������, ���������� ���������� S. }

function Q_DelChar(const S: AnsiString; Ch: Char = ' '): AnsiString;

{ Q_Delete ������� ��������� �� ������ S. ��� ���� �������� ������ ����������.
  Index - ������ ������� ���������� �������, Count - ���������� ��������,
  ���������� ��������. ��� ������� �������� �������, ��� ����������� Delete. }

implementation

// From QString library
/////////////////////////////////////////////////////
//                                                 //
//   QStrings 6.03.420      ( general release )    //
//                                                 //
//   Quick string manipulation library             //
//                                                 //
//   Russian version                               //
//                                                 //
//   Copyright (C) 2000,2001 Andrew N. Driazgov    //
//   e-mail: andrey@asp.tstu.ru                    //
//                                                 //
//   Portions (C) 2000, Sergey G. Shcherbakov      //
//   e-mail: mover@mail.ru, mover@rada.gov.ua      //
//                                                 //
//   Last updated: January 7, 2001                 //
//                                                 //
/////////////////////////////////////////////////////

procedure Q_SpaceCompressInPlace(var S: AnsiString);
asm
        PUSH    EBX
        PUSH    EAX
        CALL    UniqueString
//        PUSH    EAX         // Preserve new address of S
        TEST    EAX,EAX
        JE      @@qt
        MOV     ECX,[EAX-4]
        MOV     EBX,EAX
        DEC     ECX
        JS      @@qt
        MOV     EDX,EAX
@@lp0:  CMP     BYTE PTR [EAX+ECX],$20
        JA      @@lp1
        DEC     ECX
        JNS     @@lp0
        JMP     @@nx4
@@lp1:  CMP     BYTE PTR [EBX],$20
        JA      @@lp3
        INC     EBX
        DEC     ECX
        JMP     @@lp1
@@lp3:  MOV     AL,BYTE PTR [EBX]
        INC     EBX
        CMP     AL,$20
        JBE     @@me
@@nx3:  MOV     BYTE PTR [EDX],AL
        INC     EDX
        DEC     ECX
        JNS     @@lp3
@@nx4:  POP     EAX
        MOV     EBX,[EAX]
        MOV     BYTE PTR [EDX],0
        SUB     EDX,EBX
        MOV     [EBX-4],EDX
        JMP     @@qt1
//        POP     EBX
//        RET
@@me:   MOV     BYTE PTR [EDX],$20
        INC     EDX
        DEC     ECX
        JS      @@nx4
@@ml:   MOV     AL,BYTE PTR [EBX]
        INC     EBX
        CMP     AL,$20
        JA      @@nx3
        DEC     ECX
        JNS     @@ml
        JMP     @@nx4
@@qt:   POP     ECX
@@qt1:  POP     EBX
//        POP     EAX  // Return this new address
end;

function Q_SpaceCompressInPlace1(var S: AnsiString): AnsiString; overload;
begin
  Q_SpaceCompressInPlace(S);
  Result := S;
end;

// From QString library
{ Q_GetWordN ���������� ����� � ���������� ������� OrdN �� ������ S. �����
  ���������� � �������. ������ Delimiters ������ �������, ����������� �������-
  ������ ���� � ������ S. ���� ����� � ������� OrdN � ������ S �����������,
  ������������ ������ ������.  ���� ����������� ���������� � ���� ���������
  ���� TCharSet, ��� �� ������������ ��� ����������� ������� �������. }

function Q_GetWordN(OrdN: Integer; const S: AnsiString; const Delimiters: TCharSet): AnsiString;
var
  I, J, N: Integer;
  L: LongWord;
  P: PChar;
  A: Boolean;
begin
  L := Length(S);
  P := PChar(S);
  A := False;
  N := 1;
  for I := 1 to L do
  begin
    if not (P^ in Delimiters) then
    begin
      if not A then
      begin
        if N = OrdN then
        begin
          N := L + 1;
          Inc(P);
          for J := I + 1 to L do
          begin
            if P^ in Delimiters then
            begin
              N := J;
              Break;
            end;
            Inc(P);
          end;
          Result := Copy(S, I, N - I);
          Exit;
        end;
        A := True;
        Inc(N);
      end;
    end
    else
      if A then
        A := False;
    Inc(P);
  end;
  Result := '';
end;

procedure Q_DelCharInPlace(var S: AnsiString; Ch: Char);
asm
        PUSH    EBX
        PUSH    ESI
        PUSH    EAX
        MOV     EBX,EDX
        CALL    UniqueString
        TEST    EAX,EAX
        JE      @@qt
        MOV     ECX,[EAX-4]
        MOV     EDX,EAX
        TEST    ECX,ECX
        JE      @@zq0
@@lp1:  MOV     AL,BYTE PTR [EDX]
        CMP     BL,AL
        JE      @@cf
        INC     EDX
        DEC     ECX
        JNE     @@lp1
        JMP     @@qt
@@cf:   MOV     ESI,EDX
        INC     EDX
        DEC     ECX
        JE      @@rt
@@lp2:  MOV     AL,BYTE PTR [EDX]
        CMP     BL,AL
        JE      @@nx
        MOV     BYTE PTR [ESI],AL
        INC     ESI
@@nx:   INC     EDX
        DEC     ECX
        JNE     @@lp2
@@rt:   POP     EAX
        MOV     EBX,[EAX]
        MOV     BYTE PTR [ESI],0
        SUB     ESI,EBX
        JE      @@zq1
        MOV     [EBX-4],ESI
        POP     ESI
        POP     EBX
        RET
@@qt:   POP     ECX
        POP     ESI
        POP     EBX
        RET
@@zq0:  POP     EAX
@@zq1:  CALL    System.@LStrClr
        POP     ESI
        POP     EBX
end;

function Q_DelChar(const S: AnsiString; Ch: Char): AnsiString;
asm
        PUSH    ESI
        PUSH    EBX
        PUSH    EDI
        MOV     ESI,ECX
        TEST    EAX,EAX
        JE      @@qt
        MOV     ECX,[EAX-4]
        TEST    ECX,ECX
        JE      @@qt
        MOV     EBX,EAX
        MOV     EDI,EDX
        XOR     EDX,EDX
        MOV     EAX,ESI
        CALL    System.@LStrFromPCharLen
        MOV     EDX,EDI
        MOV     ECX,[EBX-4]
        MOV     EDI,[ESI]
@@lp:   MOV     AL,BYTE PTR [EBX]
        CMP     DL,AL
        JE      @@nx
        MOV     BYTE PTR [EDI],AL
        INC     EDI
@@nx:   INC     EBX
        DEC     ECX
        JNE     @@lp
        MOV     EAX,[ESI]
        MOV     BYTE PTR [EDI],0
        SUB     EDI,EAX
        JE      @@qt
        MOV     [EAX-4],EDI
        POP     EDI
        POP     EBX
        POP     ESI
        RET
@@qt:   MOV     EAX,ESI
        CALL    System.@LStrClr
        POP     EDI
        POP     EBX
        POP     ESI
end;

procedure ConvertErrorFmt(const Msg, S: string);
begin
  raise EConvertError.CreateFmt(Msg, [S]);
end;

//--------------------------------------------

function HexToUInt(const S: AnsiString): DWORD;
const
  Msg: AnsiString = 'Error during hex (%s) to int conversion.';
asm
        PUSH    ESI
        PUSH    EBX
        MOV     ESI,EAX
        TEST    EAX,EAX
        JE      @@err
        MOV     ECX,[EAX-4]
        TEST    ECX,ECX
        JE      @@err
        MOV     EBX,EAX
        XOR     EAX,EAX
@@lp:   MOV     DL,BYTE PTR [EBX]
        SHL     EAX,4
        SUB     DL,$30
        JB      @@err
        CMP     DL,$09
        JBE     @@ct
        SUB     DL,$11
        JB      @@err
        CMP     DL,$05
        JBE     @@pt
        SUB     DL,$20
        JB      @@err
        CMP     DL,$05
        JA      @@err
@@pt:   ADD     DL,$0A
@@ct:   OR      AL,DL
        INC     EBX
        DEC     ECX
        JNE     @@lp
        POP     EBX
        POP     ESI
        RET
@@err:  MOV     EAX,Msg
        MOV     EDX,ESI
        POP     EBX
        POP     ESI
        CALL    ConvertErrorFmt
end;

// Converts TDateTime time part to string as follow:
// 3 secs, 5 min 45 secs, 1 hours 14 mins 19 secs etc. Only existed elements
// are included.

function TimeToStrF(const T: TDateTime; IncludeMSecs: Boolean = False): AnsiString;
var
  Hours, Mins, Secs, MSecs: Word;
begin
  DecodeTime(T, Hours, Mins, Secs, MSecs);
  case Hours of
    0: Result := '';
    1: Result := IntToStr(Hours) + ' hour ';
  else
    Result := IntToStr(Hours) + ' hour ';
  end;
  case Mins of
    0: ;
    1: Result := Result + IntToStr(Mins) + ' min ';
  else
    Result := Result + IntToStr(Mins) + ' mins '
  end;
  case Secs of
    0: ;
    1: Result := Result + IntToStr(Secs) + 'sec ';
  else
    Result := Result + IntToStr(Secs) + ' secs '
  end;
  if IncludeMSecs then
    case MSecs of
      0: ;
      1: Result := Result + IntToStr(MSecs) + 'msec ';
    else
      Result := Result + IntToStr(MSecs) + ' msecs '
    end;
  Result := TrimRight(Result);
end;

end.

