{
 This unit contains routine to convert TAB file for corresponding TIFF to TFW file
 for ARC/INFO/ArcView
}
unit ProcessTAB;

interface
uses Windows, TrLib32, Main, SysUtils, Dialogs, Controls,
  Localize, Classes, TabFiles, FileUtils;

const
  TFWExtension = '.tfw';
  TFWExt = TFWExtension;

  JGWExt = '.jgw';
  SDWExt = '.sdw';
  EWWExt = '.eww';

function ProcessTABFile(Name: AnsiString): Boolean;

implementation

uses
  Forms, Math;

function ProcessTABFile(Name: AnsiString): Boolean;
var
  TABFile: TTabFile;
  I: Integer;
  ImgFile, TFWFile: AnsiString;
  PageCount: Integer;
  PInfo: PBITMAPINFO;
  TFWText: TStringList;
  W, H: Integer;
  RefX, RefY, SizeX, SizeY: double;
  Rect: TRect;
  APoint, BPoint : TFPoint;
  IsVMSI : Boolean;
begin
  Result := False;
  TABFile := TTabFile.Create(Name);
  try
    if not TABFile.IsValid then
    begin
      AddText('TAB file open error:' + TabFile.LastErrorText);
      Exit;
    end;
    if TABFile.ImageCount > 1 then // TAB file name bcontains wild cards :(
    begin // Try to find TIFF in the list
      ImgFile := EmptyStr;
      for I := 0 to TABFile.ImageCount - 1 do
        if CompareText(ExtractFileExt(TABFile.MultiNames[I]), TIFExt) = 0 then
          begin
            ImgFile := TABFile.DirName + TABFile.MultiNames[I];
            break;
          end;
          if ImgFile = EmptyStr then
          begin
            AddText('...but TAB file contains wildscards and don''t point to any TIFF image file');
            Exit;
          end;
    end;
    ImgFile := ChangeFileExt(Name, TIFExt);
    PInfo := TrLib.GetHeader(PChar(ImgFile), PageCount, IsVMSI);
    if PInfo = nil then
    begin
      AddText('...but it hasn''t a valuable header.');
      Exit;
    end;
    try
      // Read image characteristics
      W := PInfo.bmiHeader.biWidth;
      H := PInfo.bmiHeader.biHeight;

      TFWText := TStringList.Create;
      try
        TFWFile := ChangeFileExt(Name, TFWExt);
        if FileExists(TFWFile) then // create BAK file
          if not MoveFileToBAK(TFWFile) then
          begin
            AddText('...but original file can''t be deleted!');
            Exit;
          end;
        Rect  := TABFile.ProjClipRect;
        APoint := TABFile.APoint;
        BPoint := TabFile.BPoint;

        SizeX := Abs(APoint.X - BPoint.X)/W;  // 1 - width of pixel in meters
        SizeY := -Abs(APoint.Y - BPoint.Y) /H; // 4 - height of the pixel
//        PRectHeight(@Rect)/H; // 4 - width of pixel in meters
        RefX  := Rect.Left + SizeX/2.0;// 5 - start pixel X
        RefY  := Rect.Top  + SizeY/2.0;// 6 - start pixel Y

        TFWText.Add(Format( '%35.14f', [SizeX/100.0]));
        TFWText.Add(Format( '%35.14f', [0.0]));
        TFWText.Add(Format( '%35.14f', [0.0]));
        TFWText.Add(Format( '%35.14f', [SizeY/100.0]));
        TFWText.Add(Format( '%35.14f', [RefX/100.0]));
        TFWText.Add(Format( '%35.14f', [RefY/100.0]));

        Result := True;
        try
          TFWText.SaveToFile(TFWFile);
        except
          Result := False;
          AddText('Some error occured during TFW creation :o(');
        end;
        if Result  then
          AddText('Successfully converted to TFW...');
      finally
        TFWText.Free;
      end;
    finally
      VirtualFree(PInfo, 0, MEM_RELEASE);
    end;
  finally
    TabFile.Free;
  end;
//--------------------------------------------------
    // I multiply to 100 to scale meters from TFW to centimeters of Progis TAB
{    Rect.Left := Round((RefX - SizeX / 2.0) * 100.0);
    Rect.Top := Round((RefY - SizeY / 2.0) * 100.0);
    Rect.Right := Rect.Left + Round(W * SizeX * 100.0);
    Rect.Bottom := Rect.Top + Round(H * SizeY * 100.0);
    if CreateTABFile(ImgFile, W, H, @Rect, []) then    }
end;

end.

