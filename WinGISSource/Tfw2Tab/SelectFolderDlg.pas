Unit SelectFolderDlg;

Interface

Uses WinTypes, WinProcs, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, {Ctrls,} CommCtrl, CommDlg;

Type TSelectFolderDialog = Class(TOpenDialog)
      Private
       FOkClicked  : Boolean;
       FHooked     : Boolean;
       FHookProc   : Pointer;
       FOrgWndProc : Pointer;
       Function    GetFilter:String;
       Function    GetDirectoryName:String;
       Procedure   SetFilter(Const AFilter:String);
      Protected
       Function    DoCanClose:Boolean; override;
       Procedure   DoClose; override;
       Procedure   DoShow; override;
       Procedure   DoSelectionChange; override;
       Procedure   Hook;
       Procedure   HookProc(var Message:TMessage);
       Procedure   WndProc(var Msg:TMessage); override;
       Procedure   UnHook;
      Public
       Constructor Create(AOwner:TComponent); override;
       Destructor  Destroy; override;
       Function    Execute:Boolean; override;
      Published
       Property    Filter:String read GetFilter write SetFilter;
     end;

Implementation

Uses FileCtrl,Localize;

Constructor TSelectFolderDialog.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  Filter:='~|~.~~~';
  FHookProc:=MakeObjectInstance(HookProc);
end;

Destructor TSelectFolderDialog.Destroy;
begin
  UnHook;
  FreeObjectInstance(FHookProc);
  inherited Destroy;
end;

Procedure TSelectFolderDialog.UnHook;
begin
  if FHooked then begin
    SetWindowLong(GetParent(Handle),gwl_WndProc,LongInt(FOrgWndProc));
    FHooked:=FALSE;
  end;
end;

Procedure TSelectFolderDialog.Hook;
begin
  if not FHooked and not(csDestroying in ComponentState) then begin
    LongInt(FOrgWndProc):=GetWindowLong(GetParent(Handle),gwl_WndProc);
    SetWindowLong(GetParent(Handle),gwl_WndProc,LongInt(FHookProc));
    FHooked:=TRUE;
  end;
end;

Procedure TSelectFolderDialog.HookProc(var Message:TMessage);
begin
  with Message do begin
    if (Msg=wm_Command) and (TWMCommand(Message).ItemID=1) then begin
      if DirectoryExists(ExtractFilePath(GetDirectoryName+'\')) then begin
        FileName:=GetDirectoryName;
        EndDialog(GetParent(Handle),id_Cancel);
        FOkClicked:=TRUE;
      end;
      Result:=1;
    end
    else Result:=CallWindowProc(FOrgWndProc,GetParent(Handle),Msg,wParam,lParam);
  end;
end;

Procedure TSelectFolderDialog.DoShow;
var AText        : Array[0..255] of Char;
    AHandle      : THandle;
begin
  Hook;
  AHandle:=GetParent(Handle);
  SendMessage(AHandle,CDM_HIDECONTROL,1089,0);
  SendMessage(AHandle,CDM_HIDECONTROL,1136,0);
  StrPCopy(AText,FolderButtonTitle);
  SendMessage(AHandle,CDM_SETCONTROLTEXT,1090,Integer(@AText));
  StrPCopy(AText,OKButtonTitle);
  SendMessage(AHandle,CDM_SETCONTROLTEXT,1,Integer(@AText));
  StrPCopy(AText,CancelButtonTitle);
  SendMessage(AHandle,CDM_SETCONTROLTEXT,2,Integer(@AText));
  inherited DoShow;
end;

Procedure TSelectFolderDialog.DoClose;
begin
  UnHook;
  inherited DoClose;
end;

Procedure TSelectFolderDialog.DoSelectionChange;
var AText        : Array[0..255] of Char;
begin
  StrPCopy(AText,GetDirectoryName);
  SendMessage(GetParent(Handle),CDM_SETCONTROLTEXT,1152,Integer(@AText));
  inherited DoSelectionChange;
end;

Procedure TSelectFolderDialog.WndProc(var Msg:TMessage);
begin
  inherited WndProc(Msg);
  Msg.Result:=0;
  if (Msg.Msg=WM_INITDIALOG) and not (ofOldStyleDialog in Options) then Exit
  else if (Msg.Msg=WM_NOTIFY) then case POFNotify(Msg.LParam)^.hdr.code of
    CDN_INCLUDEITEM : ;
  end;
end;

Function TSelectFolderDialog.Execute:Boolean;
begin
  if Title='' then Title:= SelectOutDirTitle;
  inherited Execute;
  Result:=FOkClicked;
end;

Function TSelectFolderDialog.GetFilter:String;
begin
  if csDesigning in ComponentState then Result:=''
  else Result:='~|~.~~~';
end;

Procedure TSelectFolderDialog.SetFilter(Const AFilter:String);
begin
  inherited Filter:='~|~.~~~'
end;

const
    Suffix       : PChar = '\';
Function TSelectFolderDialog.GetDirectoryName:String;
var Index        : Integer;
    AText        : Array[0..255] of Char;
    BText        : Array[0..255] of Char;
    Item         : TLVItem;
    FListHandle  : THandle;
begin
  FListHandle:=GetDlgItem(GetDlgItem(GetParent(Handle),1121),1);
  SendMessage(GetParent(Handle),CDM_GETFOLDERPATH,SizeOf(AText),Integer(@AText));
  Index:=ListView_GetNextItem(FListHandle,-1,LVNI_ALL or LVNI_SELECTED);
  if Index>=0 then begin
    with Item do begin
      mask:=LVIF_TEXT;
      iItem:=Index;
      iSubItem:=0;
      pszText:=@BText;
      cchTextMax:=SizeOf(BText);
    end;
    if ListView_GetItem(FListHandle,Item) then begin
      if (AText[StrLen(AText)-1] <> '\') then
        StrCat(PChar(@AText[0]), Suffix);
      StrCat(AText,BText);
    end;
  end;
  Result:=StrPas(AText);
end;

function TSelectFolderDialog.DoCanClose: Boolean;
begin
  Result:=TRUE;
end;

end.
