{** unit WelcomeScr
***
*** enthaelt die klasse TWelcomeScreen, die ein
*** Fenster repraesentiert, in dem einerseits Tips,
*** andererseits Mediendateien angezeigt werden koennen
***
*** die unterstuetzten dateitypen sind:
***   AVI
***   JPEG
***   BMP
***   WMF
***   EMF
***   ICO
***
*** die tips werden aus textdateien gelesen, pro tip eine datei
*** die mediadateien und die tipdateien werden am besten in der
*** methode create angegeben, ebenso die labels fuer die buttons
*** das form ist als dialog ausgefuehrt und muss nur mit showmodal
*** gestartet werden
*** nach dem schliessen kann mit ShouldShowNextTime der status der
*** ShowNextTime-checkbox abgefragt werden
***
*** die umschaltung zwischen tip- und medien-modus erfoglt ueber einen schalter
*** (ModeButton) sie kann aber auch durch aufrufen der mehoden DoMedia bw. DoTip
*** erfolgen
**}

unit WelcomeScr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, IniFiles, MPlayer, JPEG, ComCtrls, VerMgr;

type
  TWelcomeMode = (mdMedia, mdTip);

type
  TTipMode = (tmRandom, tmOrder);

type
  TWelcomeScreen = class(TForm)
    ExitButton: TButton;
    ModeButton: TButton;
    ShowNextTime: TCheckBox;
    NextTipButton: TButton;
    BackImage: TImage;
    Timer: TTimer;
    ImgBkObj: TImage;
    MediaImage: TImage;
    TipMemo: TMemo;
    AnimMedia: TAnimate;
    ButtonPanel: TPanel;
    procedure ExitButtonClick(Sender: TObject);
    procedure ModeButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure NextTipButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure TimerTimer(Sender: TObject);
  private
    { Private-Deklarationen}
    ExitButtonLabel: string;
    NextTipButtonLabel: string;
    TipLabel: string;
    MediaLabel: string;
    ShowNextLabel: string;

    Mode: TWelcomeMode;
    TipTextFiles: TStringList;
    MediaFiles: TStringList;
    TipMode: TTipMode;
    ActualTip: integer;
    ActualMedia: integer;
    bShowNextTime: boolean;

    procedure DoNextTip;
    procedure DoNextMedia;
  public
    { Public-Deklarationen}
    procedure AddTipTextFile(fname: string);
    procedure AddMediaFile(fname: string);

    procedure DoMedia;
    procedure DoTip;

    function ShouldShowNextTime: boolean;
  end;

var
  WelcomeScreen: TWelcomeScreen;

implementation

{$R *.DFM}
{$IFDEF RUSSIA}
{$R WELCOMERUS.RES}
{$ENDIF}
uses
  AM_Greet
{$IFDEF RUSSIA}
  , LicenseHndl
{$ENDIF}
  ;

{** ExitButtonClick
***
*** wird vom application framework aufgerufen, wenn der
*** benutzer auf den ExitButton klickt
*** schliesst das fenster
***
*** parameter:
***   Sender: gibt das aufrufende objekt an
***
*** rueckgabewert: keiner
**}

procedure TWelcomeScreen.ExitButtonClick(Sender: TObject);
begin
  Close;
end;

{** TimerTimer
***
*** wird vom Timer-Komponente aufgerufen, wenn der
*** eingestellte Intervall abgelaufen ist.
*** schliesst das fenster
***
*** parameter:
***   Sender: gibt das aufrufende objekt an
***
*** rueckgabewert: keiner
**}

procedure TWelcomeScreen.TimerTimer(Sender: TObject);
begin
  Close;
end;

{** DoMedia
***
*** wird von ModeButtonKlick bzw. extern aufgerufen
*** schaltet in den medien-modus
***
*** parameter: keine
***
*** rueckgabewert: keiner
**}

procedure TWelcomeScreen.DoMedia;
begin
  Mode := mdMedia;
  ModeButton.Caption := TipLabel;
  NextTipButton.Visible := false;
  if AnimMedia.Active = True then
    AnimMedia.Active := False;
  DoNextMedia;
end;

{** DoTip
***
*** wird von ModeButtonKlick bzw. extern aufgerufen
*** schaltet in den tip-modus
***
*** parameter: keine
***
*** rueckgabewert: keiner
**}

procedure TWelcomeScreen.DoTip;
begin
  Mode := mdTip;
  if AnimMedia.Active = True then
    AnimMedia.Active := False;
  ModeButton.Caption := MediaLabel;
  DoNextTip;
end;

{** ModeButtonClick
***
*** wird vom application framework aufgerufen, wenn der
*** benutzer auf den ModeButton klickt
*** toggelt zwischen dem tip- und dem medien modus
***
*** parameter:
***   Sender: gibt das aufrufende objekt an
***
*** rueckgabewert: keiner
**}

procedure TWelcomeScreen.ModeButtonClick(Sender: TObject);
begin
  Timer.Interval := 0;
  if Mode = mdMedia then
    DoTip
  else
    DoMedia;
end;

{** FormCreate
***
*** wird vom application framework aufgerufen, wenn das
*** fenster erzeugt wird
***
*** initialisiert die beschriftung der buttons
*** hinzufuegen von medien- und tip-files erfolgt hier
***
*** schaltet in den tip-modus
***
*** parameter:
***   Sender: gibt das aufrufende objekt an
***
*** rueckgabewert: keiner
**}

procedure TWelcomeScreen.FormCreate(Sender: TObject);
var
  TipDir: string;
  MediaFile: string;
  Search: TSearchRec;
  SearchRes: Integer;
  Ext: string;
  Ini: TIniFile;
  CurDir: string;
{$IFDEF RUSSIA}
  Jpg: TJPEGImage;
  TempStream: TResourceStream;
  BGName: AnsiString;
{$ENDIF}
begin

{$IFDEF RUSSIA}
  Jpg := TJPEGImage.Create;
  try
{$IFDEF UNIVERSITY}
    BGName := 'WELCOME_UNIVERSITY';
{$ELSE}
{$IFDEF CADASTRE}
    BGName := 'WELCOME_CADASTRE';
{$ELSE}
    if WinGISLicense < 11 then { It is  some WinMAP version }
      BGName := 'WELCOME_WINMAP'
    else
      BGName := 'WELCOME_WINGIS';
{$ENDIF}
{$ENDIF}
    TempStream := TResourceStream.Create(HInstance, BGName, PChar('JPEG'));
    Jpg.LoadFromStream(TempStream);
    if not Jpg.Empty then
      BackImage.Picture.Bitmap.Assign(Jpg);
  finally
    if Assigned(TempStream) then TempStream.Free;
    Jpg.Free;
  end;
  MediaImage.Picture.Bitmap.LoadFromResourceName(HInstance,'WELCOME_MEDIAIMAGE');
{$ENDIF}
  { hier koennen die labels gemaess einem stringfile angepasst werden }
  { Allow real size of background picture be shown w/out stretching }
  Self.ClientWidth := BackImage.Width;
  Self.ClientHeight := BackImage.Height+ButtonPanel.Height;

  ExitButtonLabel := GetWelcomeString(1);
  NextTipButtonLabel := GetWelcomeString(2);
  TipLabel := GetWelcomeString(3);
  MediaLabel := GetWelcomeString(4);
  ShowNextLabel := GetWelcomeString(5);

  Timer.Interval := GetWelcomeSettings(TipDir, MediaFile);

  Mode := mdTip;
  TipMode := tmRandom;
  TipTextFiles := TStringList.Create;
  ActualTip := 0;
  ActualMedia := 0;
  MediaFiles := TStringList.Create;

  ExitButton.Caption := ExitButtonLabel;
  NextTipButton.Caption := NextTipButtonLabel;
  ShowNextTime.Caption := ShowNextLabel;

  { hier muessen die medien- und tip-dateien angegeben werden }
  AddMediaFile(MediaFile);

  GetDir(0, CurDir);
  Ini := TIniFile.Create(CurDir + '\' + getinifilename);
  Ext := Ini.ReadString('Settings', 'Language', 'txt');
  Ini.Free;

  SearchRes := FindFirst(TipDir + '\*.' + Ext, faAnyFile, Search);
  while SearchRes = 0 do
  begin
    AddTipTextFile(TipDir + '\' + Search.Name);
    SearchRes := FindNext(Search);
  end;
  FindClose(Search);

  if TipTextFiles.Count > 0 then
    DoTip
  else
  begin
    DoMedia;
    ModeButton.Visible := False;
  end;
end;

{** AddTipFile
***
*** fuegt einen eintrag zu der liste der tip-file-namen
*** hinzu, falls die datei existiert
***
*** parameter:
***   s: name der hinzuzufuegenden datei
***
*** rueckgabewert: keiner
**}

procedure TWelcomeScreen.AddTipTextFile(fname: string);
begin
  if FileExists(fname) then
    TipTextFiles.Add(fname);
end;

{** LoadTextFile
***
*** laed die zeilen einer textdatei und fuegt sie hintereinander
*** in einen string ein, die zeilen werden mit blanks separiert
*** der so generierte string kann in einem control mit wordwrap
*** angezeigt werden
***
*** parameter:
***   fname: name der textdatei (muss existieren)
***   t: generierter string, existierende daten werden
***      ueberschrieben
***
*** rueckgabewert: keiner
**}

procedure LoadTextFile(fname: string; var t: string);
var
  f: textfile;
  s: string;
begin
  t := '';
  AssignFile(f, fname);
  Reset(f);
  while eof(f) = false do
  begin
    ReadLn(f, s);
    t := t + s + ' ';
  end;
  CloseFile(f);
end;

{** DoNextTip
***
*** zeigt den naechsten tip gemaess einer zufaelligen reihenfolge an
*** deaktiviert die mediendarstellung
***
*** parameter: keine
***
*** rueckgabewert: keiner
**}

procedure TWelcomeScreen.DoNextTip;
var
  i: integer;
  s: string;
begin
  if TipTextFiles.Count <> 0 then
  begin
    NextTipButton.Visible := true;
    TipMemo.Visible := true;
    AnimMedia.Visible := false;
    MediaImage.Visible := false;

    i := random(TipTextFiles.Count);
    if i = ActualTip then
    begin
      inc(i);
      i := i mod TipTextFiles.Count;
    end;
    ActualTip := i;
    LoadTextFile(TipTextFiles.Strings[ActualTip], s);
    TipMemo.Text := s;
  end
  else
    DoMedia;
end;

{** NextTipButtonClick
***
*** wird vom application framework aufgerufen, wenn der
*** benutzer auf den NextTipButton klickt
*** zeigt den naechsten tip an
***
*** parameter:
***   Sender: gibt das aufrufende objekt an
***
*** rueckgabewert: keiner
**}

procedure TWelcomeScreen.NextTipButtonClick(Sender: TObject);
begin
  Timer.Interval := 0;
  DoNextTip;
end;

{** AddMediaFile
***
*** fuegt einen eintrag zu der liste der medien-file-namen
*** hinzu, falls die datei existiert
***
*** parameter:
***   fname: name der hinzuzufuegenden datei
***
*** rueckgabewert: keiner
**}

procedure TWelcomeScreen.AddMediaFile(fname: string);
begin
  if FileExists(fname) then
    MediaFiles.Add(fname);
end;

{** DoNextMedia
***
*** zeigt die naechste mediendatei gemaess einer zufaelligen reihenfolge an
*** deaktiviert die tipdarstellung und den NextTip-Button
***
*** parameter: keine
***
*** rueckgabewert: keiner
**}

procedure TWelcomeScreen.DoNextMedia;
var
  ext: string;
  i: integer;
  jpg: TJPEGImage;
  wmf: TMetaFile;
begin
  if MediaFiles.Count <> 0 then
  begin
    TipMemo.Visible := false;
    i := random(MediaFiles.Count);
    if i = ActualMedia then
    begin
      inc(i);
      i := i mod MediaFiles.Count;
    end;
    ActualMedia := i;

    ext := ExtractFileExt(MediaFiles.Strings[ActualMedia]);
    if (ext = '.avi') then
    begin
      AnimMedia.FileName := MediaFiles.Strings[ActualMedia];
      AnimMedia.Visible := true;
      MediaImage.Visible := False;
      AnimMedia.Active := True;
    end;
    if (ext = '.jpg')
      or (ext = '.jpeg')
      or (ext = '.bmp')
      or (ext = '.ico')
      or (ext = '.emf')
      or (ext = '.wmf') then
    begin
      AnimMedia.Visible := false;
      MediaImage.Visible := true;
      if (ext = '.jpg') or (ext = '.jpeg') then
      begin
        jpg := TJPEGImage.Create;
        jpg.LoadFromFile(MediaFiles.Strings[ActualMedia]);
        MediaImage.Picture.Assign(jpg);
        jpg.Free;
      end
      else
        if (ext = '.bmp') then
          MediaImage.Picture.LoadFromFile(MediaFiles.Strings[ActualMedia])
        else
          if (ext = '.wmf') or (ext = '.emf') then
          begin
            wmf := TMetaFile.Create;
            wmf.LoadFromFile(MediaFiles.Strings[ActualMedia]);
            MediaImage.Picture.Assign(wmf);
            wmf.Free;
          end;
    end
    else
      if (ext <> '.avi') then
      begin
        MediaImage.Visible := True;
        TipMemo.Visible := False;
      end;
  end
  else
    MediaImage.Visible := True;
  TipMemo.Visible := False;
end;

{** FormCloseQuery
***
*** wird vom application framework aufgerufen, bevor das fenster
*** geschlossen wird
*** speichert den status der ShowNextTime-checkbox und gibt das
*** fenster zum schliessen frei
***
*** parameter:
***   Sender: gibt das aufrufende objekt an
***   CanClose: inidikator, ob das fenster geschlossen werden darf
***
*** rueckgabewert: keiner
**}

procedure TWelcomeScreen.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  bShowNextTime := ShowNextTime.Checked;
  CanClose := true;
end;

{** ShouldShowNextTime
***
*** gibt den wert der variablen bShowNextTime zurueck, die
*** den status der ShowNextTime-checkbox enthaelt
***
*** parameter: keine
***
*** rueckgabewert:
***   wert der variablen bShowNextTime
**}

function TWelcomeScreen.ShouldShowNextTime: boolean;
begin
  ShouldShowNextTime := bShowNextTime;
end;

end.

