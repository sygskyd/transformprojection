unit About;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls,
  ShellAPI, Dialogs,
  VersionInfo
{$IFDEF FUTURE}
  , FractalForm
{$ENDIF FUTURE}
  , Main
  ;

type
  TAboutBox = class(TForm)
    Panel1: TPanel;
    HelpText: TMemo;
    OkButton: TSpeedButton;
    LanguageButton: TSpeedButton;
    Logo: TImage;
    GroupBox1: TGroupBox;
    Copyright: TLabel;
    Version: TLabel;
    Description: TLabel;
    GroupBox2: TGroupBox;
    RussianLogo: TLabel;
    RussianFlag: TImage;
    Bevel1: TBevel;
    Bevel2: TBevel;
    procedure RussianLogoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure LanguageButtonClick(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
{$IFDEF FUNTURE}
    procedure FractalButtonClick(Sender: TObject);
{$ENDIF}
    procedure LogoClick(Sender: TObject);
    procedure LogoClick1(Sender: TObject);
    procedure RussianFlagClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure ShowAbout;

var
  AboutBox: TAboutBox;
  WasClicked: Boolean = FALSE;

implementation

uses
  Localize;

{$R *.DFM}
//{$R ABOUT.RES}
{$R Def_help.res}

{$B-,R-}

procedure LoadHelpText(ResourceName: AnsiString);
var
  TempStream: TResourceStream;
begin
  TempStream := TResourceStream.Create(HInstance, ResourceName, PChar('HELP_TEXT'));
  if Assigned(TempStream) then
  try
    AboutBox.HelpText.Lines.LoadfromStream(TempStream);
  finally
    TempStream.Free;
  end;
end;

procedure ReloadHelp;
var
  FName: AnsiString;
begin
  FName := TFWHelp;
  with AboutBox do
  begin
{    if FName = EmptyStr then // try to load default value, not from INI
      FName := TFW_HELP_NAME;}
    if not FileExists(FName) then // Not fully referenced file pointed ?
      if ExtractFilePath(FName) = EmptyStr then // May be in run directory?
      begin
        FName := ExtractFilePath(Application.ExeName) + FName;
      end;
    if FileExists(FName) then // Try to load help text from a file
    begin
      AboutBox.HelpText.Lines.LoadFromFile(FName);
      if AboutBox.HelpText.Lines.Count > 0 then // Ok, something was loaded. Enjoy :))
      begin
        LanguageButton.Visible := False;
        Exit;
      end;
    end;
// At last load help from resources
    LanguageButton.Visible := True;
    LoadHelpText(ResLangName[HelpLanguage]);
    LanguageButton.Caption := LangButtonText[HelpLanguage];
  end;
end;

procedure ShowAbout;
begin
  AboutBox := TAboutBox.Create(Application.MainForm);
  with AboutBox do
  begin
    with TVersionInfo.Create(Panel1) do
    begin
      Description.Caption := FileDescription;
      CopyRight.Caption := LegalCopyright;
      Version.Caption := FileVersion;
      Destroy;
    end;
    ReloadHelp;
    ShowModal;
    Destroy;
  end;
end;

procedure TAboutBox.FormCreate(Sender: TObject);
var
  Present: TDateTime;
  Year, Month, Day : Word;
  WeekDay : Integer;
begin
  with Self do
  begin
    Width := MainForm.Width;
    Height := MainForm.Height;
    Left := MainForm.Left;
    Top := MainForm.Top;
{    with Panel1 do
    begin
      OKButton.Left := (Width - OKButton.Width) div 2;
    end;}
  end;
  if WasClicked then
  begin
    RussianLogo.Font.Color := clBlue;
    RussianLogo.Font.Style := RussianLogo.Font.Style + [fsUnderLine];
  end;
  Present := Now;
  DecodeDate(Present, Year, Month, Day);
  WeekDay := DayOfWeek(Present);

  // Present from Sigolaeff :o)))))))))))
  // Filter not wanted days. If not wanted, simply exit to the dialog.
//  Month := 11; // Here only for debugging purposes, delete first of all :o)
//  Day := 7;
//  WeekDay := 6;
  if (Month <> 11) then
    Exit;
  case Day of
    5: // 5th of November
      if WeekDay <> 6 then Exit; // Not Friday - so 7th will be at work day
    6: //6th of November - check to be Friday of Saturday
      if (WeekDay < 6) and (WeekDay > 7) then Exit; // Not Friday..Saturday
    7: ; // 7th of November - always hit :o)
  else
    Exit;
  end;                      
  // So now it is time to show Red flag (Easter egg)
  try
  Logo.Picture.Bitmap.LoadFromResourceName(HInstance, 'USSR_FLAG');
  except
    Exit;
  end;
  with Logo.Picture.Bitmap do
  begin
    Logo.Height  := Height;
    Logo.Width   := Width;
    Logo.Stretch := True;
    Logo.Left := Self.Width - Width - 1;
  end;
  Logo.OnClick := LogoClick1;
  Logo.Hint := 'Long live to The Great October Revolution :o!';
end;

procedure TAboutBox.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    ModalResult := mrOk;
  Key := 0; { To prevent follow processing  }
end;

procedure TAboutBox.FormShow(Sender: TObject);
begin
  with Self do
  begin
    Width := MainForm.Width;
    Height := MainForm.Height;
    Left := MainForm.Left;
    Top := MainForm.Top;
  end;

  HelpText.Refresh;
  HelpText.WordWrap := True;
end;

procedure TAboutBox.LanguageButtonClick(Sender: TObject);
begin
  with LanguageButton do
    case HelpLanguage of
      English: { English , change to Russian }
        HelpLanguage := Russian;
      Russian:
        HelpLanguage := English;
    end;
  ReloadHelp;
end;

procedure TAboutBox.OkButtonClick(Sender: TObject);
begin
  Modalresult := mrOk;
end;

procedure TAboutBox.LogoClick(Sender: TObject);
begin
  ShellExecute(Handle, PChar('Open'), PChar('HTTP://WWW.PROGIS.COM'), nil, nil, SW_SHOWNORMAL);
end;

procedure TAboutBox.LogoClick1(Sender: TObject);
begin
  ShellExecute(Handle, PChar('Open'), PChar('HTTP://WWW.PRAVDA.RU'), nil, nil, SW_SHOWNORMAL);
end;

procedure TAboutBox.RussianLogoClick(Sender: TObject);
begin
  ShellExecute(Handle, PChar('Open'), PChar('HTTP://WWW.PROGIS.RU'), nil, nil, SW_SHOWNORMAL);
end;

procedure TAboutBox.RussianFlagClick(Sender: TObject);
begin
  ShellExecute(Handle, PChar('Open'), PChar('HTTP://WWW.VOR.RU'), nil, nil, SW_SHOWNORMAL);
  WasClicked := True;
  RussianLogo.Font.Color := clBlue;
  RussianLogo.Font.Style := RussianLogo.Font.Style + [fsUnderLine];
end;

end.

