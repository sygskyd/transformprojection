unit FractalForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, FractalImage,
  ExtCtrls, Menus,
  SpectraLibrary;

type
  TFractalBox = class(TForm)
    FractalDock: TPanel;
    MainMenu1: TMainMenu;
    Mandelbrott1: TMenuItem;
    Julian1: TMenuItem;
    Moire1: TMenuItem;
    Sierpinski1: TMenuItem;
    Fern1: TMenuItem;
    Leaf1: TMenuItem;
    Curl1: TMenuItem;
    Koch1: TMenuItem;
    Fractaltype1: TMenuItem;
    Stop1: TMenuItem;
    Start1: TMenuItem;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Mandelbrott1Click(Sender: TObject);
    procedure Julian1Click(Sender: TObject);
    procedure Moire1Click(Sender: TObject);
    procedure Sierpinski1Click(Sender: TObject);
    procedure Fern1Click(Sender: TObject);
    procedure Leaf1Click(Sender: TObject);
    procedure Curl1Click(Sender: TObject);
    procedure Koch1Click(Sender: TObject);
    procedure Stop1Click(Sender: TObject);
    procedure Start1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FractalDockResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure ShowFractal;
function CreateRainbowPalette: PMaxLogPalette;

implementation

uses main;

{$R *.DFM}

var
  FractalBox: TFractalBox;
  FI: TFractalImage;
  RPal: PMaxLogPalette;
  HPal: HPALETTE;

procedure TFractalBox.FormCreate(Sender: TObject);
begin
  with Self do
  begin
    Width := MainForm.Width;
    Height := MainForm.Height;
    Left := MainForm.Left;
    Top := MainForm.Top;
  end;
  RPal := CreateRainbowPalette;
  HPal := Windows.CreatePalette(PLOGPALETTE(RPal)^);
  FI := TFractalImage.Create(FractalDock);
  with FI do
  begin
    AutoSize := True;
    if HPal <> 0 then
      SetPalette(HPal);
    Parent := FractalDock;
    Width := FractalDock.ClientWidth - 6;
    Height := FractalDock.ClientHeight - 6;
    Left := 3;
    Top := 3;
    OnMouseDown := Self.OnMouseDown;
    IncrementalDisplay := True;
  end;
end;

procedure TFractalBox.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
  begin
    ModalResult := mrOk;
    Exit;
  end;
  FI.Active := True;
  Key := 0; { To prevent follow processing  }
end;

procedure TFractalBox.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbLeft then
  begin
    ModalResult := mrOk;
    Exit;
  end;
  FI.Active := True;
end;

procedure ShowFractal;
begin
  FractalBox := TFractalBox.Create(Application.MainForm);
  with FractalBox do
  begin
    ShowModal;
    Destroy;
  end;
end;

procedure TFractalBox.FormDestroy(Sender: TObject);
begin
  FI.Free;
  FreeMem(RPal);
  DeleteObject(HPal);
end;

procedure TFractalBox.FormShow(Sender: TObject);
begin
  Mandelbrott1Click(Sender);
end;

procedure TFractalBox.Mandelbrott1Click(Sender: TObject);
var
  i, j: Integer;
  P: PByteArray;
begin
  with FI do
  begin
    FractalProperties.FractalType := ftMandelBrot;
    with FI.Picture do
      if not BitMap.Empty then
        for j := Height - 29 to Height - 1 do
      begin
        P := BitMap.ScanLine[j];
        for i := 0 to Width - 1 do
          P[i] := i mod 255;
      end;
      Refresh;
  end;
//  FI.Picture.SaveToFile('D:\temp\myPal.bmp');
end;

procedure TFractalBox.Julian1Click(Sender: TObject);
begin
  FI.FractalProperties.FractalType := ftJulian;
end;

procedure TFractalBox.Moire1Click(Sender: TObject);
begin
  FI.FractalProperties.FractalType := ftMoire;
end;

procedure TFractalBox.Sierpinski1Click(Sender: TObject);
begin
  FI.FractalProperties.FractalType := ftSierpinski;
end;

procedure TFractalBox.Fern1Click(Sender: TObject);
begin
  FI.FractalProperties.FractalType := ftFern;
end;

procedure TFractalBox.Leaf1Click(Sender: TObject);
begin
  FI.FractalProperties.FractalType := ftLeaf;
end;

procedure TFractalBox.Curl1Click(Sender: TObject);
begin
  FI.FractalProperties.FractalType := ftCurl;
end;

procedure TFractalBox.Koch1Click(Sender: TObject);
begin
  FI.FractalProperties.FractalType := ftKoch;
end;

function CreateRainbowPalette: PMaxLogPalette;
type
  TPALArray = array[0..255] of TPALETTEENTRY;
var
  SpectreStep, Spectre: Double;
  i: Integer;
begin
  SpectreStep := 1.0 / 256.0; // 1/256th of step
  GetMem(Result, SizeOf(TMaxLogPalette));
  Spectre := SpectreStep;
  for i := 1 to 254 do
    with ColorToRGBTriple(Rainbow(Spectre)), Result.palPalEntry[i] do
    begin
      peRed := rgbtRed;
      peGreen := rgbtGreen;
      peBlue := rgbtBlue;
      peFlags := 0;
      Spectre := Spectre + SpectreStep;
    end;
  with Result^ do
  begin
    with palPalEntry[0] do
    begin
      peRed := 0;
      peGreen := 0;
      peBlue := 0;
      peFlags := 0;
    end;
    with palPalEntry[255] do
    begin
      peRed := 255;
      peGreen := 255;
      peBlue := 255;
      peFlags := 0;
    end;
    palversion := $300; // Magical nnumber of MS
    palNumEntries := 256;
  end;
end;

procedure TFractalBox.Stop1Click(Sender: TObject);
begin
  if FI.Active then
    FI.Stop
  else
end;

procedure TFractalBox.Start1Click(Sender: TObject);
begin
  Start1.Enabled := False;
  Stop1.Enabled := True;
  FI.Active := True;
end;

procedure TFractalBox.Timer1Timer(Sender: TObject);
begin
  if FI.Active then
  begin
    Start1.Enabled := False;
    Stop1.Enabled := True;
  end
  else
  begin
    Start1.Enabled := True;
    Stop1.Enabled := False;
  end;
end;

procedure TFractalBox.FractalDockResize(Sender: TObject);
begin
  FI.Recreate;
end;

end.

