unit VersionInfo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TVersionInfo = class(TComponent)
  private
    FVersionInfo : Pointer;
    FFileName : String;
    FLangCharSet : String;
    function GetCompanyName : String;
    function GetFileDescription : String;
    function GetFileVersion : String;
    function GetInternalName : String;
    function GetLegalCopyright : String;
    function GetOriginalFilename : String;
    function GetProductName : String;
    function GetProductVersion : String;
    procedure Init;
    procedure SetFileName(const Value : String);
    procedure Clear;
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    function GetValue(const ValueName : String; var Buffer : Pointer) : Boolean;
    function GetLocalValue(const ValueName : String) : String;
    property CompanyName : String read GetCompanyName;
    property FileDescription : String read GetFileDescription;
    property FileVersion : String read GetFileVersion;
    property InternalName : String read GetInternalName;
    property LegalCopyright : String read GetLegalCopyright;
    property OriginalFilename : String read GetOriginalFilename;
    property ProductName : String read GetProductName;
    property ProductVersion : String read GetProductVersion;
    property LangCharSet : String read FLangCharSet;
  published
    property FileName : String read FFileName write SetFileName;
  end;

procedure Register;

implementation

constructor TVersionInfo.Create(AOwner : TComponent);
begin
     inherited Create(AOwner);
     FVersionInfo := nil;
     FFileName := Application.ExeName;
end;

destructor TVersionInfo.Destroy;
begin
     Clear;
     inherited Destroy;
end;

procedure TVersionInfo.Clear;
begin
     if FVersionInfo <> nil then FreeMem(FVersionInfo);
     FVersionInfo := nil;
end;

procedure TVersionInfo.SetFileName(const Value : String);
begin
     Clear;
     FFileName := Value;
end;

procedure TVersionInfo.Init;
type T = array [0..1] of WORD;
var Size, Fake : DWORD;
    P : ^T;
begin
     if FVersionInfo <> nil then exit;
     Size := GetFileVersionInfoSize(PChar(FFileName), Fake);
     if Size = 0 then raise Exception.Create('Error in detecting VersionInfo size!');
     GetMem(FVersionInfo, Size);
     try
        if not GetFileVersionInfo(PChar(FFileName), 0, Size, FVersionInfo) then
           raise Exception.Create('Error in detecting VersionInfo!');
     except
        FreeMem(FVersionInfo);
        FVersionInfo := nil;
        raise;
     end;
     GetValue('\VarFileInfo\Translation', Pointer(P));
     FLangCharSet := Format('%.4x%.4x', [P^[0], P^[1]]);
end;

function TVersionInfo.GetValue(const ValueName : String; var Buffer : Pointer) : Boolean;
var Size : UINT;
begin
     Init;
     Result := VerQueryValue(FVersionInfo, PChar(ValueName), Buffer, Size);
end;

function TVersionInfo.GetLocalValue(const ValueName : String) : String;
var P : Pointer;
begin
     Init;
     if GetValue('\StringFileInfo\' + FLangCharSet + '\' + ValueName, P) then Result := StrPas(P)
     else Result := '';
end;

function TVersionInfo.GetCompanyName : String;
begin
     Result := GetLocalValue('CompanyName');
end;

function TVersionInfo.GetFileDescription : String;
begin
     Result := GetLocalValue('FileDescription');
end;

function TVersionInfo.GetFileVersion : String;
begin
     Result := GetLocalValue('FileVersion');
end;

function TVersionInfo.GetInternalName : String;
begin
     Result := GetLocalValue('InternalName');
end;

function TVersionInfo.GetLegalCopyright : String;
begin
     Result := GetLocalValue('LegalCopyright');
end;

function TVersionInfo.GetOriginalFilename : String;
begin
     Result := GetLocalValue('OriginalFilename');
end;

function TVersionInfo.GetProductName : String;
begin
     Result := GetLocalValue('ProductName');
end;

function TVersionInfo.GetProductVersion : String;
begin
     Result := GetLocalValue('ProductVersion');
end;

procedure Register;
begin
  RegisterComponents('Ei Controls', [TVersionInfo]);
end;

end.
