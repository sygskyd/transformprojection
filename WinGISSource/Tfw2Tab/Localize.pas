{
  To help localization of reports
}
unit Localize;

interface

type
  LangType = (English, Russian);
const
  ImgManFileNum = 2;
  ResLangName: array[LangType] of AnsiString = ('ENGLISH_HELP_TEXT', 'RUSSIAN_HELP_TEXT');
  LangButtonText: array[LangType] of AnsiString = ('Read in Russian', 'Read in English');
  ImgManFiles: array[1..ImgManFileNum] of AnsiString = (
    'imgman32.dll',
    'im31tif.dil'
    );
resourcestring
    MyEMail = 'mailto:support@progis.com';
    TFW_HELP_NAME = 'TFW2TAB.TXT';  // Default file to store help text for about box
    TFWSection = 'Main';       // Section in INI file to read TFW settings

    // Lower line defines what files to extract from resources and how store them
    // Syntax is as follow:
    // "outputFIleName:ResourceName:ResourceType"
    // To separate multiple file, use semicolon between them
    ImageManJob = 'ImgMan32.dll:IMGMAN32:IMGMAN_FILE;Im31tif.dil:IM31TIF:IMGMAN_FILE';

//    ToFreddie = '(-: Hello from PROGIS-Moscow... and Sigolaeff personally :-)';
    //ToFreddie = 'Please drag&drop any file of TFW/TAB format into this frame or use the "File/Open" function...';
    SelectOutDirTitle = 'Select Output Directory';
    FolderButtonTitle = '&Folder:';
    OKButtonTitle = '&OK';
    CancelButtonTitle = '&Cancel';


implementation

end.

