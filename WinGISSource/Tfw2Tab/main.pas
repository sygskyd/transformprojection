unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, FileCtrl, StdCtrls, ClipBrd, ComCtrls, ExtCtrls, Gauges,
  ToolWin, ImgList, Buttons, ActnList, IniFiles, DropForm,
  Localize, SelectFolderDlg, ResourceExport, TabFiles, WinOSInfo;

type
  TMainForm = class(TDropForm)
    TextLines: TMemo;
    MemoPopupMenu: TPopupMenu;
    H1: TMenuItem;
    Copy1: TMenuItem;
    Savetoafile1: TMenuItem;
    LogSave: TSaveDialog;
    ImageList: TImageList;
    ToolBar: TPanel;
    OpenButton: TSpeedButton;
    ExitButton: TSpeedButton;
    ActionList1: TActionList;
    OpenAction: TAction;
    ExitAction: TAction;
    AboutAction: TAction;
    ToolBarPopupMenu: TPopupMenu;
    Hidemetoolbar1: TMenuItem;
    OpenTABDialog: TOpenDialog;
    ResourceExport: TResourceExport;
    procedure Help1Click(Sender: TObject);
//    procedure Open1Click(Sender: TObject);
//    procedure Exit1Click(Sender: TObject);
    procedure Copy1Click(Sender: TObject);
    procedure H1Click(Sender: TObject);
    procedure Savetoafile1Click(Sender: TObject);
    procedure OpenActionExecute(Sender: TObject);
    procedure ExitActionExecute(Sender: TObject);
    procedure AboutActionExecute(Sender: TObject);
//    procedure Panel1Resize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
//    procedure PreferencesActionExecute(Sender: TObject);
    procedure FormDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure FormDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure Hidemetoolbar1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure MyPopupHandler(Sender: TObject);
  public
    { Public declarations }
    procedure PrepareMemo;
  end;

const
  LOG_EXT = '.LOG';
var
  MainForm: TMainForm;

  Job: TStringList = nil; // Job array
  Ini: TIniFile = nil; // Ini file handler
  LastDir {, OutputDir}: AnsiString; // some temps
  TFWHelp: AnsiString; // Help file name
  ShowToolBar: Boolean;
  HelpLanguage: LangType = English; { English }

procedure AddText(Txt: AnsiString);
procedure MakeTrueDir(var DirName: AnsiString);

implementation

{$R *.DFM}
{$R ImageMan.RES}
//{$R VMSIRes.RES}

uses About, TrLib32, ProcessFile, DropTarget, ActiveX, ShellApi,
  Math, QStrings;

var
  MyMenuIndex: Integer;

function ProcessWildCards(Name: AnsiString): Integer;
var
  SearchRec: TWin32FindData;
  SearchHnd: THandle;
  fpATH, FName: AnsiString;
  NLen: Integer;
begin
  Result := 0; { Success counter }
  if (Pos('*', Name) <> 0) or (Pos('?', Name) <> 0) then { Wildcards detected }
    AddText('Wildcards detected:"' + Name + '"');
  SearchHnd := FindFirstFile(PChar(Name), SearchRec);
  if SearchHnd <> INVALID_HANDLE_VALUE then
  try
    fPath := ExtractFilePath(Name);
    repeat
{    NLen := GetFullPathName(PChar(SearchRec.Name), 259, FName, PName);}
      FName := AnsiString(SearchRec.CFileName);
{        if (SearchRec.Attr and (faHidden + faVolumeID + faSysFile + faDirectory)) = 0 then}
      if (SearchRec.dwFileAttributes and (FILE_ATTRIBUTE_HIDDEN + FILE_ATTRIBUTE_OFFLINE +
        +FILE_ATTRIBUTE_SYSTEM + FILE_ATTRIBUTE_DIRECTORY)) = 0 then
      begin
        if ProcessImageFile(FPath + FName) then
          Inc(Result);
      end
      else
        AddText('Skip for strange attribs:"' + FName + '"');
    until not FindNextFile(SearchHnd, SearchRec);
  finally
    Windows.FindClose(SearchHnd);
  end
  else
  begin
    FlashWindow(MainForm.Handle, True);
    SetLength(FName, 256);
    NLen := Windows.FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, nil,
      GetLastError, 0, PChar(FName), 255, nil);
    if NLen > 0 then
    begin
      SetLength(FName, NLen);
      AddText('.. error:' + FName);
    end
    else
      AddText('..but some error occured!');
    FlashWindow(MainForm.Handle, False);
  end;
end;

procedure AddText(Txt: AnsiString);
begin
  MainForm.TextLines.Lines.Add(Txt);
end;

procedure TMainForm.Help1Click(Sender: TObject);
begin
  ShowAbout;
end;
{ Open and process files! }

var
  InitDir: AnsiString = '';
//  OpenDlg: TWOpenPictureDialog = nil;
//  Flags: DlgCtrlFlags = []; { Nothing is enabled }
//  Status: OpenStatus;
  Filters: AnsiString;
  Opts: TOpenOptions = [ofHideReadOnly, ofAllowMultiSelect, ofPathMustExist,
    ofFileMustExist, ofEnableSizing];
  FileNames: AnsiString = '';

function ProcessFiles: Integer;
var
  I, J, K: Integer;
  FName: AnsiString;
begin
  Result := 0; { Success counter}
  for I := 0 to Job.Count - 1 do
  begin
    FName := Job[I];
    if (FName[1] = '/') or (FName[1] = '-') then { switch detected }
    begin { Parse switch now }
      FName := UpperCase(FName);
      case FName[2] of
        'X': // Exit
          Application.Terminate;
      else
        AddText('Invalid switch "' + Copy(FName, 1, 2) + '" detected');
      end;
      continue; // Continue job
    end;

    for K := 0 to MaxInt do { Delete double quotes '"' from file name }
    begin
      J := Pos('"', FName);
      if J = 0 then
        Break;
      Delete(FName, J, 1)
    end;
    FName := ExpandFileName(FName);
    if (Pos('*', FName) <> 0) or (Pos('?', FName) <> 0) then { Wildcards detected }
    begin
      Result := Result + ProcessWildCards(FName);
      Continue;
    end
    else
      if ProcessImageFile(FName) then
        Inc(Result);
  end;
  Job.Clear; { If done - wipe it out! }
end;

procedure TMainForm.Copy1Click(Sender: TObject);
begin
  ClipBoard.Open;
  ClipBoard.SetTextBuf(PChar(TextLines.Lines.Text));
  ClipBoard.Close;
end;

procedure TMainForm.H1Click(Sender: TObject);
begin
  PrepareMemo;
end;

procedure TMainForm.Savetoafile1Click(Sender: TObject);
var
  Name: string;
begin
  if LogSave.Execute then
  begin
    Name := LogSave.FileName;
    if Pos('.', Name) = 0 then { No extension was found, add default one }
      Name := Name + LOG_EXT;
    if FileExists(Name) then
      DeleteFile(Name);
    TextLines.Lines.SaveToFile(Name);
  end;
end;

procedure TMainForm.OpenActionExecute(Sender: TObject);
begin
  if (Filters = '') then
  begin
    Filters := TrLib.ImageGetImpFormats;
    if Filters = '' then
    begin
      ShowMessage('No file extensions are supported'#13#10 +
        'Please check that ImageMan32.dll and *.dil *.del'#13#10 +
        'Files are in the PATH of your computer');
      Exit;
    end;
  end;
//  if OpenDlg = nil then
//  begin
//    OpenDlg := TWOpenPictureDialog.Create(MainForm, @Flags, @Status);
//    OpenDlg.Title := 'Input file[s] open dialog.';
//  end;
  with OpenTABDialog do
  begin
    Options := Opts - [ofExtensionDifferent {, ofAllowMultiSelect}]; { Clear flags }
//    Filter := Filters;
    if LastDir <> '' then { Set last dir visited }
    begin
      OpenTABDialog.InitialDir := LastDir; // Load last used directory
    end;
    if Execute then { Check input file names }
    begin
{      if Status = osNotImage then
      begin
        ShowMessage('Next time select some real image, pleassss');
        Exit;
      end;}
      Job.Assign(Files);
      AddText('---Processing files from dialog...');
      AddText(IntToStr(ProcessFiles) + ' processed successfully :)');
      LastDir := OpenTABDialog.Files[0]; // Save last used directory
      if not DirectoryExists(LastDir) then
        LastDir := ExtractFilePath(LastDir);
    end;
  end;
end;

procedure TMainForm.ExitActionExecute(Sender: TObject);
begin
  Close;
end;

procedure TMainForm.AboutActionExecute(Sender: TObject);
begin
  ShowAbout;
end;

{procedure TMainForm.Panel1Resize(Sender: TObject);
begin
  AboutButton.Left := TPanel(Sender).Width - AboutButton.Width - 1;
end;}

procedure TMainForm.FormShow(Sender: TObject);
var
  I, Res: Integer;
  Ext, Path: AnsiString;
  Cmd, Mess: AnsiString;
  FileName, ResType, ResName: AnsiString;
const
  Hdr = 'Fatal error';
begin
  Caption:=Application.Title;

  Job := TStringList.Create;

//+++++++++++++++++++++++++++++++ READ INI FILE ++++++++++++++++++++++++++++++++

  Ini := TIniFile.Create(OSInfo.LocalAppDataDir + ChangeFileExt(ExtractFileName(Application.ExeName),'.ini'));

  // Last directopry visited in open dialog
  LastDir := ExtractFilePath(Ini.ReadString(TFWSection, 'LastDir', EmptyStr));
  MakeTrueDir(LastDir);
  WasClicked := Ini.ReadBool(TFWSection, 'WasClicked', FALSE);
  if not Ini.ReadBool(TFWSection, 'ShowToolBar', TRUE) then
  begin
    HideMeToolBar1Click(Sender);
  end;

  //  last default help resource used
  HelpLanguage := LangType(Ini.ReadInteger(TFWSection, 'HelpLng', Integer(English)));
  // User definaed file to load
  TFWHelp := Ini.ReadString(TFWSection, 'HelpFile', TFW_HELP_NAME);
  // Last extension used in open dialog
  OpenTabDialog.FilterIndex := Ini.ReadInteger(TFWSection, 'LastExt', 1);
  Ini.Free;
  //----------------------------------------------------------------------------
  PrepareMemo;
  if Tag <> 0 then { Not first show must go }
    Exit;
  Mess := '';
  if not TrLib32.ImageManAvailable then
  begin
    Res := Application.MessageBox(PChar('Do you want to install it NOW to the TFW2TAB.exe' + #10#13 +
      'run directory and use this time and later?'),
      PChar('Image library wasn''t found !!!'),
      MB_YESNO + MB_ICONQUESTION);
    if Res = IDYES then { Try to extract library's files to the run directory }
    begin
      Path := ExtractFilePath(Application.ExeName);
      Res := 0;
{$IFDEF OLD}
      for I := 1 to ImgManFileNum do
        with ResourceExport1 do
        begin
          ResourceName := 'FILE' + IntToStr(I);
          ExportFileName := Path + ImgManFiles[I];
          AddText('Installing file "' + ImgManFiles[I] + '"');
          case Execute of
            reOK:
              begin
                Mess := 'OK.';
                Inc(Res);
              end;
            reNotFound: Mess := 'Resource for file not found!';
            reLoadError: Mess := 'Resource #' + IntToStr(I) + ' load error!';
            reFileExists: Mess := 'File already exists!';
          end;
          AddText(Mess);
          Mess := EmptyStr;
        end;
{$ELSE}
      for I := 1 to MaxInt do
      begin
        Cmd := Q_GetWordN(I, ImageManJob, [';']); // Get next command
        if Cmd = EmptyStr then
          break;
        Q_TrimInPlace(Cmd); // Trim command line
        FileName := Q_GetWordN(1, Cmd, ':');
        if FileName = EmptyStr then
        begin
          AddText('In command "' + Cmd + '" 1st item is absent (file name to install) - skipped.');
          continue;
        end;
        if ExtractFilePath(FileName) <> EmptyStr then
        begin
          AddText('In command "' + Cmd + '" 1st item (file name to install) contains some path  - skipped.');
          continue;
        end;
        ResName := Q_GetWordN(2, Cmd, ':');
        if ResName = EmptyStr then
        begin
          AddText('In command "' + Cmd + '" 2nd item is absent (resource name to extract) - skipped.');
          continue;
        end;
        ResType := Q_GetWordN(3, Cmd, ':');
        if ResName = EmptyStr then
        begin
          AddText('In command "' + Cmd + '" 3rd item is absent (resource type to extract) - skipped.');
          continue;
        end;

        // Extract from resource and write to disk

        with ResourceExport do
        begin
          ResourceName := ResName;
          ExportFileName := Path + FileName;
          AddText('Installing file "' + FileName + '"');
          case Execute of
            reOK:
              begin
                Mess := 'OK.';
                Inc(Res);
              end;
            reNotFound: Mess := 'Resource "' + ResName + '" for the file not found!';
            reLoadError: Mess := 'Resource "' + ResName + '" load error!';
            reFileExists: Mess := 'File already exists!';
          end;
          AddText(Mess);
          Mess := EmptyStr;
        end;
      end;
{$ENDIF}
      case Res of
        ImgManFileNum:
          AddText('So all was OK and are you ready to continue to use this pretty program :))');
      else
        begin
          AddText(' Not all files were successfully installed...');
          AddText(' Hmm... This run can be a tricky thing :((');
          AddText(' Why not to ask author about this problem :-?');
          AddText(' If so try "' + MyEMail + '"');
        end;
      end;
    end
    else
      Mess := '"' + TrLib.DllName + '" wasn''t found in EXE/PATH directory.'#10#13 +
        'Next time try to put TFW2TAB.exe to a WinGIS directory.'
  end;
  if Mess = '' then
  begin
    Ext := Trlib.ImageGetImpFormats;
    if not IsExtInFilter(Ext, TIFExt) then
    begin
      Mess := 'Main import library "*tif.dil"  isn''t available in this session.'#10#13 +
        'Check its existance in EXE/Imageman directory.'#10#13 +
        'Or put it into WINGIS directory.'#10#13 +
        'Avaliable are ONLY:' + Ext;
    end;
  end;
  if Mess <> '' then { Some error occured }
  begin
    Application.MessageBox(PChar(Mess),
      PChar(Hdr), MB_OK + MB_ICONEXCLAMATION);
    Application.Terminate;
  end;

  if ParamCount > 0 then { Load files to proccess from command line }
  begin
    AddText('---Processing command line...');
    for I := 1 to ParamCount do
    begin
      Job.Add(ParamStr(I)); { Add name by name - they can be with wildcards }
    end;
    AddText(IntToStr(ProcessFiles) + ' w[ere|as] processed successfully :)');
  end;

  OpenActionExecute(nil);
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//++++++++++++++++++++++= WRITE INI FILES ++++++++++++++++++++++++
  Ini := TIniFile.Create(OSInfo.LocalAppDataDir + ChangeFileExt(ExtractFileName(Application.ExeName),'.ini'));
  if OpenTABDialog.Files.Count > 0 then
  begin
    Ini.WriteString(TFWSection, 'LastDir', ExtractFilePath(OpenTABDialog.Files[0]));
  end;
  Ini.WriteBool(TFWSection, 'WasClicked', WasClicked);
  Ini.WriteInteger(TFWSection, 'HelpLng', Integer(HelpLanguage));
  Ini.WriteBool(TFWSection, 'ShowToolBar', ToolBar.Visible);
  Ini.WriteInteger(TFWSection, 'LastExt', OpenTABDialog.FilterIndex);
  Ini.Free;
end;

{procedure TMainForm.PreferencesActionExecute(Sender: TObject);
begin
  PreferencesForm := TPreferencesForm.Create(MainForm);
  try
    PreferencesForm.ShowModal;
  finally
    PreferencesForm.Free;
  end;
end;}

procedure TMainForm.PrepareMemo;
begin
  TextLines.Clear;
//  AddText(ToFreddie);
//  AddText('Run directory  : ' + ExtractFilePath(Application.ExeName));
//  AddText('Current directory : ' + GetCurrentDir + '\');
{  if OutputDir = EmptyStr then
    AddText('Output directory is not set. Original one will be used')
  else
    AddText('Output directory : ' + OutputDir);}
end;

{------------------------ Drag'n'Drop functions ------------------------------}

procedure TMainForm.FormDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  case TTeComDragObject(Source).DragOperation of
    doCopy, doMove: Accept := True;
  else
    Accept := False;
  end;
end;

procedure TMainForm.FormDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  Obj: TTeComDragObject;
  IObj: IDataObject;
  IEnum: TEnumFormats;
  Drop: THandle;
  NumFiles: Integer;
  i: Integer;
  buffer: array[0..255] of char;
begin
  Obj := TTeComDragObject(Source);
{  AddText('Dropped:');}
  case Obj.DragOperation of
    doNothing, doLink: ;
    doCopy, doMove:
      begin
        IObj := Obj.DataObject;
        IEnum := TEnumFormats.Create(IObj);
        try
          if IEnum.HasFormat(CF_HDROP) then { File[s] dropped}
          begin
            Drop := IEnum.GlobalHandle;
            {How many files are being dropped}
            NumFiles := DragQueryFile(Drop, $FFFFFFFF, nil, 0);
            if NumFiles > 0 then
            begin
              Job.Clear;
              {Accept the dropped files}
              for i := 0 to (NumFiles - 1) do
              begin
                DragQueryFile(Drop, i, @buffer, sizeof(buffer));
                Job.Add(buffer);
              end;
              AddText(IntToStr(NumFiles) + ' Drag&Drop files processing:');
              AddText(IntToStr(ProcessFiles) + ' processed successfully :)');
            end;
          end;
        finally
          IEnum.Destroy;
        end;
      end;
  else
 {   AddText('UnKnown?!')};
  end;
end;
//----------------------------------------------------

procedure MakeTrueDir(var DirName: AnsiString);
begin
  DirName := Trim(DirName);
  if DirName = EmptyStr then
    Exit;
  if DirName[Length(DirName)] <> '\' then
    DirName := DirName + '\';
end;

procedure TMainForm.MyPopupHandler(Sender: TObject);
begin
  with Sender as TMenuItem do
  begin
    ToolBar.Visible := True;
    MemoPopUpMenu.Items.Delete(MyMenuIndex);
  end;
end;

procedure TMainForm.Hidemetoolbar1Click(Sender: TObject);
var
  MySubItem: TMenuItem;
begin
  ToolBar.Visible := False;
  MySubItem := TMenuItem.Create(Self);
  with MySubItem do
  begin
    Caption := 'Show tool bar';
    OnClick := MyPopupHandler;
  end;
  MemoPopUpMenu.Items.Add(MySubItem);
  MyMenuIndex := MemoPopUpMenu.Items.IndexOf(MySubItem);
end;

procedure TMainForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Wnd: HWND;
begin
  if Key = VK_ESCAPE then
  begin
//    EscapePressed := True;
    Wnd := GetNextWindow(Application.Handle, GW_HWNDNEXT);
    while (not IsWindowVisible(Wnd)) and (Wnd <> 0) do
      if not IsWindowVisible(Wnd) then
        Wnd := GetNextWindow(Wnd, GW_HWNDNEXT)
      else
        Break;
    if Wnd <> 0 then
    begin
      SetForegroundWindow(Wnd);
      SetActiveWindow(Wnd);
    end;
  end;
end;

end.

