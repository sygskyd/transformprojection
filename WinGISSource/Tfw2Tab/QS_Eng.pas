
{ The file made with PROMPT 99 translator }

unit QStrings;

{
   The list of changes:

  07.01.2001 (v6.03) - the function Q_CompMemS for matching blocks is supplemented
                       Memories on more-less (by contents), starting
                       From bytes disposed to low addresses; is amplified(strengthened)
                       Algorithm of generation of a key of symmetric encoding,
                       Used in the procedure Q_DHGetCipherKey;
                       The functions for conversion of string in number are supplemented;
                       This version QStrings is compatible with Delphi 4;
  19.11.2000 (v6.01) - new functions for simulation of random variables
                       With the uniform and normal laws of allocation
                       On the basis of the generator Mersenne Twister; are supplemented
                       Functions for quick sort of the arrays 4-�������
                       Or 2-������� of units and binary search in these
                       The arrays, for junction of such arrays on a basis
                       Various logic operations, functions fulfilling
                       Scanning of double words and bit strings in searches
                       Single or offbits installing and
                       Resetting groups of bits; the function is supplemented
                       Q_StrTok1, which does not bunch going the labour contract
                       Characters - separators in the initial string; are supplemented
                       Functions for deleting units with defined
                       By value from the array (with shift of units) and
                       Deleting of repeating units, function for
                       Scannings of the array in searches of value, greater
                       Or smaller preset; now for check on
                       It is possible to write empty string instead of Length(S)<>0
                       S<>'' (the functions Q_DelXXX and Q_TrimXXX) are corrected;
  07.10.2000 (v5.12) - the functions Q_TestWildStr and Q_TestWildText are supplemented
                       For matching strings on a mask with the registration WildCard-
                       Characters, function Q_IsDecimal; is once again modified
                       Algorithm of operation of functions Q_MixHashXXX (!!!); are removed(cleaned)
                       Procedures Q_IncludeChar, Q_ExcludeChar (instead of them
                       It is necessary to call(cause) standard Include, Exclude);
  02.10.2000 (v5.11) - the functions for check of character record are supplemented
                       Numbers in string and check that number in string
                       Belongs to a preset range; the functions are supplemented
                       Q_SwapMem, Q_SwapLongs, Q_NotByteArr, Q_IsEmptySet,
                       Functions for operation with bit patterns, for exchange
                       Values of a variable type Word and Byte;
  23.09.2000 (v5.10) - is modified (!!!) algorithm of operation 320-bit hash-
                       Functions (Q_MixHashXXX); the implementation of a method is corrected
                       RC4; the procedures Q_RC4, Q_RC4(En/De)cryptCBC are remote;
  20.09.2000 (v5.09) - the code of some functions (Q_GetCharSet is optimized,
                       Q_SameStr, Q_Delete, Q_CompareMem and others);
  17.09.2000 (v5.08) - the procedures Q_MixHashXXX, for obtaining are supplemented
                       320-bit value of one-sided hash-function;
                       The support of encoding with an open key is supplemented
                       (Method of the Diffie-Hellman); is corrected a little
                       Procedures and functions (including, method CAST-256,
                       Q_RotateBitsLeft/Right, Q_RandRC6Update);
  05.09.2000 (v5.07) - the function Q_PadInside (alignment of the text is supplemented
                       On both edges(territories)) and function Q_ChangeBase (translation
                       Numbers from one number system in another);
  23.08.2000 (v5.06) - the functions realizing compression RLE are renamed;
  19.08.2000 (v5.05) - the functions Q_TabsToSpaces and Q_SpacesToTabs are supplemented;
  18.08.2000 (v5.04) - the algorithm of operation of procedures Q_FillLong is optimized,
                       Q_CopyLongs; the new procedures are supplemented: Q_OnesMem,
                       Q_CompareMem, Q_CompLongs, Q_ReverseBits; is supplemented
                       Compression of strings and byte arrays by a method RLE;
  27.07.2000 (v5.03) - the changes to operation of the safe generator are brought in
                       Pseudo-random numbers;
  24.07.2000 (v5.02) - the error in functions Q_PadLeft, Q_PadRight is corrected,
                       Q_CenterStr; about an error has signalled Sergey G. Menylenko
                       e-mail: serega@pricenews.ru;
  18.07.2000 (v4.12) - the error in the following functions is corrected: Q_StrMoveL,
                       Q_StrUpperMoveL, Q_StrLowerMoveL, originating at
                       To transmission in them of empty string in the parameter Dest;
                       The algorithm of operation of the random-number generator is corrected
                       Mersenne Twister; the functions Q_SecureRandNext are supplemented,
                       Q_SecureRandFill; function Q_RandXOR is substituted on
                       Q_SecureRandXOR; the types of descriptors TRC4ID are supplemented,
                       TRC6ID, TCASTID, TSHAID, TMTID;
  28.06.2000 (v4.11) - the functions for comparison, substitution are supplemented
                       And clipping of fragments of strings on a mask: Q_TestByMask,
                       Q_ApplyMask (InPlace), Q_ExtractByMask (InPlace);
  25.06.2000 (v4.10) - function Q_SetBitCount and Q_FreeBitCount now
                       Are named, accordingly, Q_CountOfSetBits and
                       Q_CountOfFreeBits; Q_ReplaceAllByOne is renamed
                       In Q_ReplaceCharsByOneChar; the functions are supplemented
                       Q_DeleteStr And Q_DeleteText for deleting preset
                       Substrings; the error in Q_PStrLen is corrected; are supplemented
                       The following functions: Q_PosLastStr, Q_PosLastText,
                       Q_ReplaceFirst/LastXXXX, Q_DeleteFirst/LastXXXX;
  19.06.2000 (v4.09) - the function for fast calculation of length is supplemented
                       Strings such as PChar (writer of implementation is
                       Robert Lee), function Q_SetBitCount and Q_FreeBitCount
                       For count in the byte array of number single and
                       Offbits accordingly;
  31.05.2000 (v4.08) - the function Q_CharSet is renamed in Q_GetCharStr;
                       Q_RemoveChars - > Q_DelChars; Q_ReplaceChars now
                       Is realized as the procedure; the support is supplemented
                       Character sets (TCharSet); the functions are supplemented
                       For operation with byte strings;
  19.05.2000 (v4.07) - the error in the function Q_CenterStr is corrected; is supplemented
                       Support of the dynamic tables of code conversion;
  16.05.2000 (v4.06) - the third parameter in the function Q_StrRScan is supplemented;
  03.05.2000 (v4.05) - the new functions for operation with usual are supplemented
                       And binary strings; are supplemented cryptography
                       Functions; the support of the coding Base64 and is supplemented
                       Commands of the processor RDTSC;
  09.03.2000 (v3.05) - is supplemented some functions for encoding strings;
  26.02.2000 (v3.03) - is corrected some errors; the procedure Q_XORByKey
                       Is renamed in Q_XORByStr; the procedure is supplemented
                       Q_SetDelimiters; the notes are supplemented;
  21.02.2000 (v3.01) - is supplemented many new functions, some functions
                       Are renamed; the functions for conversion are supplemented
                       Decimal numbers in hexadecimal, octal,
                       Binary, Roman and on the contrary; is corrected a little
                       Errors (including Q_FillChar);
  09.02.2000 (v2.10) - at record of the pecuniary sums the first character is done(made)
                       Large, the functions Q_CRC32 and Q_NextCRC32 are modified,
                       (!!!) to correspond(meet) to the generally accepted algorithm
                       Calculation CRC32, are supplemented function for operation with
                       The date, brings in corrections to the comments;
  07.02.2000 (v2.09) - is supplemented some functions for operation with byte
                       By strings, function for conversion of number in string
                       And for obtaining record of the pecuniary sums in words;
  25.01.2000 (v2.07) - is corrected a little(a little bit;some) errors, are supplemented functions for
                       Operations with the byte arrays;
  The end of 1999    - prime versions QStrings.


    NOTICE!!!

 1. If it is necessary to apply Q_FillChar, Q_NOTByteArr or other similar
    The function (in which the pointer is transmitted and the data on the pointer vary)
    To string S, at first it is necessary to call(cause) the standard procedure UniqueString (S)
    For actual selection of memory and exception of the multiple references and
    The references to constant string. Only after that it is possible to change string
    Under the pointer, for example: Q_FillChar(Pointer(S),Length (S),'*').

 2. When you arrange memory under string S with the help SetString or
    SetLength, and then call(cause) any functions or procedures from this
    Libraries receiving as var-parameter (not const-) string S,
    That at an output(exit) from these functions or procedures a size of memories selected(allocated)
    Under string S, can differ from initial (i.e. during execution
    Functions the memory under string can be reallocated). This remark
    Does not refer to procedures Q_StrMoveL, Q_StrUpperMoveL, Q_StrLowerMoveL,
    Q_IntToStrBuf, Q_UIntToStrBuf, Q_UIntToStrLBuf, Q_UIntToHexBuf,
    Q_UIntToOctBuf, Q_UIntToBinBuf and procedures, in which the string is transmitted
    As the pointer. During execution of the listed procedures memory
    Is not reallocated.
}

interface

{ Set of characters in Pascal.}

type
  TCharSet = set of Char;

{ Characters by default used as separators of words.}

const
  Q_StdDelimsSet = [#0..#32,'!','"','(',')','*','+',',','-','.','/',':', 
     ';','<','=','>','?','[','\',']','^','{','}','|']; 

{ Title of months and days of week.}

  Q_MonthsUp: array [1.. 12] of string =
    ('������','�������','����','������','���','����','����','������',
     '��������','�������','������','�������');

  Q_MonthsLo: array [1.. 12] of string =
    ('������','�������','�����','������','���','����','����','�������',
     '��������','�������','������','�������');

  Q_MonthsEng: array [1.. 12] of string =
    ('January','February','March','April','May','June','July','August',
     'September','October','November','December');

  Q_SMonthsUp: array [1.. 12] of string =
    ('���','���','���','���','���','���','���','���','���','���','���','���');

  Q_SMonthsLo: array [1.. 12] of string =
    ('���','���','���','���','���','���','���','���','���','���','���','���');

  Q_SMonthsEng: array [1.. 12] of string =
    ('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');

  Q_WeekDays: array [1.. 7] of string =
    ('�����������','�����������','�������','�����','�������',
     '�������','�������');

  Q_WeekDaysEng: array [1.. 7] of string =
    ('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

  Q_SWeekDays2: array [1.. 7] of string =
    ('��','��','��','��','��','��','��');

  Q_SWeekDays3: array [1.. 7] of string =
    ('���','���','���','���','���','���','���');

  Q_SWeekDaysEng: array [1.. 7] of string =
    ('Sun','Mon','Tue','Wed','Thu','Fri','Sat');


{ Function for matching strings.}

{ Q_CompStr compares two strings to case sensitivity. Reset result
  It is less than zero, if S1 < S2; it is more than zero, if S1 > S2, and is equal to zero, if
  S1 = S2. This function ceases matching, when the discrepancy of strings is revealed
  Or when the character with the code 0 is detected. If this character can meet in
  To middle of string, use instead of Q_CompStr the function Q_CompStrL. Second
  The function Q_PCompStr is similar Q_CompStr for PChar and Pointer (String). If
  It is necessary to clarify only, two strings are equal or are not equal, take advantage
  Instead of Q_CompStr by the function Q_SameStr.}

function Q_CompStr (const S1, S2: string): Integer;
function Q_PCompStr (P1, P2: PChar): Integer;

{ Q_CompStrL compares two strings on MaxL to the first characters to case sensitivity.
  The reset result is less than zero, if Copy (S1,1, MaxL) < Copy (S2,1, MaxL);
  The result is more than zero, if Copy (S1,1, MaxL) > Copy (S2,1, MaxL), differently
  The result is equal to zero (if Copy (S1,1, MaxL) = Copy (S2,1, MaxL)). If to you
  It is necessary to clarify only, two strings are equal or are not equal, take advantage
  Instead of Q_CompStrL by the function Q_SameStrL.}

function Q_CompStrL (const S1, S2: string; MaxL: Cardinal = MaxInt): Integer;

{ Q_CompText compares two strings without case sensitivity. Reset result
  It is less than zero, if S1 < S2; it is more than zero, if S1 > S2, and is equal to zero, if
  S1 = S2. This function ceases matching, when the discrepancy of strings is revealed
  Or when the character with the code 0 is detected. If this character can meet in
  To middle of string, use instead of Q_CompText the function Q_CompTextL. Second
  The function Q_PCompText is similar Q_CompText for PChar and Pointer (String). If
  It is necessary to clarify only, whether two strings whether or not are equal, take advantage
  Instead of Q_CompText by the function Q_SameText.}

function Q_CompText (const S1, S2: string): Integer;
function Q_PCompText (P1, P2: PChar): Integer;

{ Q_CompTextL compares two strings on MaxL to the first characters without case sensitivity.
  If the fragment of the first string is more (in alphabetic order), than fragment
  The second string the reset value is more than zero. If a fragment of the first string
  It is less, than the fragment of the second string reset value is less than zero, differently
  The result is equal to zero. If it is necessary to clarify only, whether two strings are equal
  Or are not equal, use instead of Q_CompTextL the function Q_SameTextL.}

function Q_CompTextL (const S1, S2: string; MaxL: Cardinal = MaxInt): Integer;

{ Q_SameStr compares two strings to case sensitivity and resets True, if
  The strings are equal, differently resets False. The function Q_PSameStr is similar Q_SameStr
  For Pointer (String).}

function Q_SameStr (const S1, S2: string): Boolean;
function Q_PSameStr (P1, P2: Pointer): Boolean;

{ Q_SameStrL compares two strings on MaxL to the first characters to case sensitivity.
  Resets True, if the strings are equal, differently resets False.}

function Q_SameStrL (const S1, S2: string; MaxL: Cardinal): Boolean;

{ Q_SameText compares two strings without case sensitivity and resets True, if
  The strings are equal, differently - False. Function Q_PSameText is similar Q_SameStr for
  Pointer (String).}

function Q_SameText (const S1, S2: string): Boolean;
function Q_PSameText (P1, P2: Pointer): Boolean;

{ Q_SameTextL compares two strings on MaxL to the first characters without case sensitivity.
  Resets True, if the strings are equal, differently resets False.}

function Q_SameTextL (const S1, S2: string; MaxL: Cardinal): Boolean;

{ Q_MatchStr checks, whether has a place log-on of a substring SubStr in string S,
  Since the character S [Pos]. At matching the register of characters is taken into account. If
  The log-on takes place, the function resets True, differently - False. This function
  Realizes by itself fast variant of check Q_SameStr (X, Copy (S, Pos, Length (X))).
  As against other similar functions, substring of zero length enters in
  Any string. The idea of this function is borrowed from the unit cStrings.pas, writer
  Which is David Butler (david@e.co.za). }

function Q_MatchStr (const SubStr, S: string; Pos: Integer = 1): Boolean;

{ Q_MatchText checks, whether has a place log-on of a substring SubStr in string S,
  Since the character S [Pos]. At matching the register of characters is not taken into account. If
  The log-on takes place, the function resets True, differently - False. This function
  Realizes by itself fast variant of check Q_SameText (X, Copy (S, Pos, Length (X))).
  As against other similar functions, substring of zero length enters in
  Any string. The idea of this function is borrowed from the unit cStrings.pas, writer
  Which is David Butler (david@e.co.za). }

function Q_MatchText (const SubStr, S: string; Pos: Integer = 1): Boolean;

{ Q_TestByMask checks, whether the string S to a mask Mask satisfies, reputing,
  That the characters MaskChar from string Mask can be substituted in string S by anyone
  By other characters. At matching the register of characters is considered.
  If the string S satisfies to a mask, the function resets True, differently False.
  For example, Q_TestMask('ISBN 5-09-007017-2','ISBN ?-??-??????-?','?') will return
  Value True. }

function Q_TestByMask (const S, Mask: string; MaskChar: Char = 'X'): Boolean;

{ Q_TestWildStr checks, whether the string S to a mask Mask satisfies, reputing,
  That the characters MaskChar from string Mask can be substituted in string S by anyone
  By other characters, and the characters WildCard can be substituted by any quantity
  Other characters. At matching the large and small characters differ. The character
  WildCard should be distinct from #0. If the string S satisfies to a mask,
  The function resets True, differently False. For example, following function call
  Will return True: Q_TestWildStr('abc12345_infQ_XL.dat','abc*_???Q_*.d*at'). }

function Q_TestWildStr (const S, Mask: string; MaskChar: Char = '?';
  WildCard: Char = '*'): Boolean;

{ Q_TestWildText is similar to the function Q_TestWildStr, but register of characters
  Is not considered (large and small characters do not differ).}

function Q_TestWildText (const S, Mask: string; MaskChar: Char = '?';
  WildCard: Char = '*'): Boolean;


{ Function for change of the register of characters.}

{ Q_CharUpper translates the character Ch in the upper case (in the large character).}

function Q_CharUpper (Ch: Char): Char;

{ Q_CharLower translates the character Ch in lowercase (in the small character).}

function Q_CharLower (Ch: Char): Char;

{ Q_StrUpper translates string S in the upper case (in the large characters). At
  It the initial string varies. The function Q_PStrUpper is similar to the procedure
  Q_StrUpper For PChar and Pointer (String), except that she(it)
  In addition resets the pointer by the beginning of string.}

procedure Q_StrUpper (var S: string);
function Q_PStrUpper (P: PChar): PChar;

{ Q_StrLower translates string S in lowercase (in small characters). At
  It the initial string varies. The function Q_PStrLower is similar to the procedure
  Q_StrLower For PChar and Pointer (String), except that she(it)
  In addition resets the pointer by the beginning of string.}

procedure Q_StrLower (var S: string);
function Q_PStrLower (P: PChar): PChar;

{ Q_StrUpperMoveL copies contents of string Source in string Dest. Thus
  The characters are translated in the upper case. Maximum number copied
  Characters is equal MaxL. Length of string Dest is installed equal to number
  The copied characters. The memory for string Dest should be distributed
  Beforehand by function call SetString (or SetLength) (size
  Not less MaxL of characters).}

procedure Q_StrUpperMoveL (const Source: string; var Dest: string; MaxL: Cardinal);

{ Q_StrLowerMoveL copies contents of string Source in string Dest. Thus
  The characters are translated in lowercase. Maximum number copied
  Characters is equal MaxL. Length of string Dest is installed equal to number
  The copied characters. The memory for string Dest should be distributed
  Previously by function call SetString (or SetLength) (size
  Not less MaxL of characters).}

procedure Q_StrLowerMoveL (const Source: string; var Dest: string; MaxL: Cardinal);

{ Q_UpperCase translates string S in the upper case (in the large characters). Initial
  The string thus does not vary. This function works more slowly, than Q_StrUpper
  Or Q_StrUpperMoveL.}

function Q_UpperCase (const S: string): string;

{ Q_LowerCase translates string S in lowercase (in small characters). Initial
  The string thus does not vary. This function works more slowly, than Q_StrLower
  Or Q_StrLowerMoveL.}

function Q_LowerCase (const S: string): string;

{ Q_UpLowerInPlace will convert the first character of string to the upper case, and all
  Other characters - to lowercase. The initial string varies.}

procedure Q_UpLowerInPlace (var S: string);

{ Q_UpLowerStr will convert the first character of string to the upper case, and all
  Other characters - to lowercase. The initial string does not vary.
  For example: 'r��a���� bsDSFc' - > 'R���a��� bsdsfc'.}

function Q_UpLowerStr (const S: string): string;

{ Q_ProperCaseInPlace changes string S in such a manner that each word will be
  To start with large, and to keep by small characters (first character everyone
  Words will be converted to the upper case, and other characters - to lower
  To the register). The initial string S varies. In string Delimiters are transmitted
  The characters, which are considered as separators of words in string S. If
  Delimiters is equal to empty string, the list of separators, which is used
  Were preset during the previous call of one from the following functions Q_StrTok,
  Q_StrSpn, Q_StrCSpn, Q_ProperCase, Q_ProperCaseInPlace, Q_WordAtPos,
  Q_GetWordN, Q_SetDelimiters, Q_CountOfWords. If the separators are transmitted
  As set, they are not stored for the subsequent calls of functions.}

procedure Q_ProperCaseInPlace (var S: string; const Delimiters: string); overload;
procedure Q_ProperCaseInPlace (var S: string;
  const Delimiters: TCharSet = Q_StdDelimsSet); overload;

{ Q_ProperCase resets string S, transformed in such a manner that everyone
  Word starts with large character (first character of each word is translated in
  The upper case, and other characters - in lowercase). In string Delimiters
  The characters are transmitted which are considered as separators of words in string S.
  For example, the function call Q_ProperCase (' resources of the INTERNET ', ') will return string
  ' Resources of the Internet '. If Delimiters is equal to empty string, is used
  List of separators, which were preset during previous call of one of
  The following functions: Q_ProperCaseInPlace, Q_ProperCase, Q_StrTok, Q_StrSpn,
  Q_StrCSpn, Q_WordAtPos, Q_GetWordN, Q_SetDelimiters, Q_CountOfWords. If
  The separators are transmitted as set, they are not stored for subsequent
  Calls of functions.}

function Q_ProperCase (const S, Delimiters: string): string; overload;
function Q_ProperCase (const S: string;
  const Delimiters: TCharSet = Q_StdDelimsSet): string; overload;


{ Function of code conversion of strings: from DOS in Windows and on the contrary.}

{ Q_StrToAnsi translates string S from the coding DOS in the coding Windows. At
  It the initial string varies. Q_PStrToAnsi is similar to the procedure
  Q_StrToAnsi For PChar and Pointer (String), except that she(it)
  In addition resets the pointer by the beginning of string. }

procedure Q_StrToAnsi (var S: string);
function Q_PStrToAnsi (P: PChar): PChar;

{ Q_StrToOem translates string S from the coding Windows in the coding DOS. At
  It the initial string varies. The function Q_PStrToOem is similar to the procedure
  Q_StrToOem For PChar and Pointer (String), except that she(it)
  In addition resets the pointer by the beginning of string. }

procedure Q_StrToOem (var S: string);
function Q_PStrToOem (P: PChar): PChar;

{ Q_PStrToAnsiL translates L of the first characters of string pointed by the parameter P,
  From the coding DOS in the coding Windows. Thus the initial string varies.
  The function resets the pointer by the beginning of string. }

function Q_PStrToAnsiL (P: PChar; L: Cardinal): PChar;

{ Q_PStrToOemL translates L of the first characters of string pointed by the parameter P,
  From the coding Windows in the coding DOS. Thus the initial string varies.
  The function resets the pointer by the beginning of string. }

function Q_PStrToOemL (P: PChar; L: Cardinal): PChar;

{ Q_Str2ToAnsi translates string Source from the coding DOS in the coding Windows.
  The result is saved in string Dest. Q_PStr2ToAnsi is similar to the procedure
  Q_Str2ToAnsi For PChar and Pointer (String), for exception that she(it)
  In addition resets the pointer by the beginning of the string - receiver Dest.}

procedure Q_Str2ToAnsi (const Source: string; var Dest: string);
function Q_PStr2ToAnsi (Source, Dest: PChar): PChar;

{ Q_Str2ToOem translates string Source from the coding Windows in the coding DOS.
  The result is saved in string Dest. Q_PStr2ToOem is similar to the procedure
  Q_Str2ToOem For PChar and Pointer (String), for exception that she(it)
  In addition resets the pointer by the beginning of the string - receiver Dest.}

procedure Q_Str2ToOem (const Source: string; var Dest: string);
function Q_PStr2ToOem (Source, Dest: PChar): PChar;

{ Q_PStr2ToAnsiL translates L of the first characters of string Source from the coding
  DOS in the coding Windows. The result is saved in string Dest. The function
  Resets the pointer by the beginning of the string - receiver Dest.}

function Q_PStr2ToAnsiL (Source, Dest: PChar; L: Cardinal): PChar;

{ Q_PStr2ToOemL translates L of the first characters of string Source from the coding Windows
  In the coding DOS. The result is saved in string Dest. The function resets
  The pointer by the beginning of the string - receiver Dest.}

function Q_PStr2ToOemL (Source, Dest: PChar; L: Cardinal): PChar;

{ Q_ToAnsi translates string OemStr from the coding DOS in the coding Windows. At
  It the initial string does not vary. This function works more slowly, than
  Other similar functions and procedures from this unit.}

function Q_ToAnsi (const OemStr: string): string;

{ Q_ToOem translates string AnsiStr from the coding Windows in the coding DOS. At
  It the initial string does not vary. This function works more slowly, than
  Other similar functions and procedures from this unit.}

function Q_ToOem (const AnsiStr: string): string;


{ Search, replacement and deleting of substrings and separate characters.}

{ Q_PosStr finds first �������� of a substring FindString in string SourceString,
  Since a position StartPos. Number of the character is reset, with which starts
  Log-on or 0, if the substring FindString is not retrieved in string SourceString.
  The search of a substring is made with case sensitivity (large and small characters
  Differ). The writer of algorithm - Peter Morris (UK) (unit FastStrings).}

function Q_PosStr (const FindString, SourceString: string;
  StartPos: Integer = 1): Integer;

{ Q_PosText finds first �������� of a substring FindString in string SourceString,
  Since a position StartPos. Number of the character is reset, with which starts
  Log-on or 0, if the substring FindString is not retrieved in string SourceString.
  The search of a substring is made without case sensitivity (large and small characters
  Do not differ). The writer of algorithm - Peter Morris (UK) (unit FastStrings).}

function Q_PosText (const FindString, SourceString: string;
  StartPos: Integer = 1): Integer;

{ Q_PosLastStr finds the latter �������� of a substring FindString in string
  SourceString, believing, that the following log-on was retrieved in a position
  LastPos. Number of the character is reset, with which the required substring starts
  Or 0, if the given substring is not retrieved earlier (more to the left) of indicated position.
  It is supposed, that the separate log-ons of a substring do not overlap each other.
  The search of a substring is made with case sensitivity (large and small characters
  Differ). If LastPos exceeds length of string SourceString, is searched
  The latest log-on of a substring FindString.}

function Q_PosLastStr (const FindString, SourceString: string;
  LastPos: Integer = MaxInt): Integer;

{ Q_PosLastText finds the latter �������� of a substring FindString in string
  SourceString, believing, that the following log-on was retrieved in a position
  LastPos. Number of the character is reset, with which the required substring starts
  Or 0, if the given substring is not retrieved earlier (more to the left) of indicated position.
  It is supposed, that the separate log-ons of a substring do not overlap each other.
  The search of a substring is made without case sensitivity (large and small characters
  Do not differ). If LastPos exceeds length of string SourceString,
  The latest log-on of a substring FindString is searched. }

function Q_PosLastText (const FindString, SourceString: string;
  LastPos: Integer = MaxInt): Integer;

{ The functions Q_TablePosXXX can be used for multiple search of a substring
  In string (one or many), when the substring consists of several characters.
  These functions represent one of implementations of a search algorithm of substrings,
  Called " Boyer-Moore pattern searching algorithm ". At first is called(caused)
  The procedure such as Q_InitTablePosXXX for the job(definition) of a required substring, and then
  Appropriate function Q_TablePosXXX for actual search of a substring in
  To string. This second function can be called(caused) multiply for search some
  Fragment in many strings or for search of several log-ons of a substring in
  One string. All functions and procedures are threadsafe, i.e.
  Q_TablePosXXX Works only with a substring, which was indicated by call
  Q_InitTablePosXXX In the same stream. Simultaneously search can be conducted
  In the other stream, that will not affect in any way current stream. It is necessary to mark,
  That Q_InitTablePosStr and Q_InitTablePosText use to the same area
  Memories for storage of a required substring. Thus, it is impossible to mix search
  Substrings with the registration and without case sensitivity. The speed of operation of these functions can
  To be various depending on length of a substring and set included in it(her)
  Characters. In many cases these functions work faster, than Q_PosStr and
  Q_PosText, but more often it happens on the contrary.}

{ Q_InitTablePosStr initiates search of a substring in string with case sensitivity
  Characters. This procedure should be called(caused) for the job(definition) of a required substring
 , how the function Q_TablePosStr will be called(caused). }

procedure Q_InitTablePosStr (const FindString: string);

{ Q_TablePosStr finds in string SourceString a substring defined at
  Call Q_InitTablePosStr. In the parameter LastPos number of the character is transmitted,
  With which started previous retrieved log-on of this substring in
  To string SourceString. In the same parameter the position new is reset
  The next log-on of a required substring. Originally, value of the parameter
  LastPos should be equal to zero. If the substring is retrieved, function
  Q_TablePosStr Resets True, differently - False. If the substring is not retrieved,
  The value of the parameter LastPos does not vary.}

function Q_TablePosStr (const SourceString: string; var LastPos: Integer): Boolean;

{ Q_InitTablePosText initiates search of a substring in string without the registration
  The register of characters. This procedure should be called(caused) for the job(definition) required
  Substrings, how the function Q_TablePosText will be called(caused). }

procedure Q_InitTablePosText (const FindString: string);

{ Q_TablePosText finds in string SourceString a substring defined at
  Call Q_InitTablePosText. In the parameter LastPos number of the character is transmitted,
  With which started previous retrieved log-on of this substring in
  To string SourceString. In the same parameter the position new is reset
  The next log-on of a required substring. Originally, value of the parameter
  LastPos should be equal to zero. If the substring is retrieved, function
  Q_TablePosText Resets True, differently - False. If the substring is not retrieved,
  The value of the parameter LastPos does not vary.}

function Q_TablePosText (const SourceString: string; var LastPos: Integer): Boolean;

{ Q_ReplaceStr substitutes all �������� of a substring FindString in string SourceString
  By substring ReplaceString. The search of a substring is made with case sensitivity
  (The large and small characters differ). The function resets string - result.
  If the substring FindString is absent in string SourceString, is reset
  The initial string SourceString.}

function Q_ReplaceStr (const SourceString, FindString, ReplaceString: string): string;

{ Q_ReplaceText substitutes all �������� of a substring FindString in string
  SourceString by a substring ReplaceString. The search of a substring is made without the registration
  The register (the large and small characters do not differ). The function resets
  String - result. If the substring FindString is absent in string SourceString,
  That is reset the initial string SourceString.}

function Q_ReplaceText (const SourceString, FindString, ReplaceString: string): string;

{ Q_ReplaceFirstStr substitutes the first log-on of a substring FindString in string
  SourceString by a substring ReplaceString. The substring FindString is searched, starting
  From the character S [StartPos]. Number of the character, with which starts retrieved ����-
  ����� of a substring, is reset as result of the function (or zero, if a substring
  Is not retrieved). The search of a substring FindString is fulfilled with case sensitivity
  Characters (the large and small characters differ).}

function Q_ReplaceFirstStr (var S: string; const FindString, ReplaceString: string;
  StartPos: Integer = 1): Integer;

{ Q_ReplaceFirstText substitutes the first log-on of a substring FindString in string
  SourceString by a substring ReplaceString. The substring FindString is searched, starting
  From the character S [StartPos]. Number of the character, with which starts retrieved ����-
  ����� of a substring, is reset as result of the function (or zero, if a substring
  Is not retrieved). The search of a substring FindString is fulfilled without case sensitivity
  Characters (the large and small characters do not differ).}

function Q_ReplaceFirstText (var S: string; const FindString, ReplaceString: string;
  StartPos: Integer = 1): Integer;

{ Q_ReplaceLastStr substitutes the last log-on of a substring FindString in string
  SourceString by a substring ReplaceString. Substring FindString is searched to the left of
  The character S [LastPos] (i.e. in a fragment Copy (S, 1, LastPos-1)). Number of the character,
  With which the retrieved log-on starts, is reset as result of the function
  (Or zero, if the substring is not retrieved). The search of a substring FindString is fulfilled
  With case sensitivity of characters (the large and small characters differ). If
  LastPos exceeds length of string S, the latest log-on is substituted
  Required substring.}

function Q_ReplaceLastStr (var S: string; const FindString, ReplaceString: string;
  LastPos: Integer = MaxInt): Integer;

{ Q_ReplaceLastText substitutes the last log-on of a substring FindString in string
  SourceString by a substring ReplaceString. Substring FindString is searched to the left of
  The character S [LastPos] (i.e. in a fragment Copy (S, 1, LastPos-1)). Number of the character,
  With which the retrieved log-on starts, is reset as result of the function
  (Or zero, if the substring is not retrieved). The search of a substring FindString is fulfilled
  Without case sensitivity of characters (the large and small characters do not differ). If
  LastPos exceeds length of string S, the latest log-on is substituted
  Required substring.}

function Q_ReplaceLastText (var S: string; const FindString, ReplaceString: string;
  LastPos: Integer = MaxInt): Integer;

{ Q_DeleteStr deletes from string S all log-ons of a substring SubStrToDel. Search
  Substrings is carried on with case sensitivity of characters. The function resets quantity
  Retrieved (and remote) fragments.}

function Q_DeleteStr (var S: string; const SubStrToDel: string): Integer;

{ Q_DeleteText deletes from string S all log-ons of a substring SubStrToDel. Search
  Substrings is carried on without case sensitivity of characters. The function resets quantity
  Retrieved (and remote) fragments.}

function Q_DeleteText (var S: string; const SubStrToDel: string): Integer;

{ Q_DeleteFirstStr deletes the first log-on of a substring SubStrToDel from string
  SourceString. The substring SubStrToDel is searched, since the character S [StartPos].
  Number of the character, with which starts the retrieved log-on of a substring, ������-
  ������ as result of the function (or zero, if the substring is not retrieved). Search
  Substrings SubStrToDel is fulfilled with case sensitivity of characters (large and
  The small characters differ).}

function Q_DeleteFirstStr (var S: string; const SubStrToDel: string; StartPos:
  Integer = 1): Integer;

{ Q_DeleteFirstText deletes the first log-on of a substring SubStrToDel from string
  SourceString. The substring SubStrToDel is searched, since the character S [StartPos].
  Number of the character, with which starts the retrieved log-on of a substring, ������-
  ������ as result of the function (or zero, if the substring is not retrieved). Search
  Substrings SubStrToDel is fulfilled without case sensitivity of characters (large and
  The small characters do not differ).}

function Q_DeleteFirstText (var S: string; const SubStrToDel: string; StartPos:
  Integer = 1): Integer;

{ Q_DeleteLastStr deletes the last log-on of a substring SubStrToDel in string
  SourceString. The substring SubStrToDel is searched to the left of the character S [LastPos]
  (In a fragment Copy (S, 1, LastPos-1)). Number of the character, with which starts
  The retrieved log-on of a substring, is reset as result of the function (or zero,
  If the substring is not retrieved). The search of a substring SubStrToDel is fulfilled with the registration
  The register of characters (the large and small characters differ).}

function Q_DeleteLastStr (var S: string; const SubStrToDel: string; LastPos:
  Integer = MaxInt): Integer;

{ Q_DeleteLastText deletes the last log-on of a substring SubStrToDel in string
  SourceString. The substring SubStrToDel is searched to the left of the character S [LastPos]
  (In a fragment Copy (S, 1, LastPos-1)). Number of the character, with which starts
  The retrieved log-on of a substring, is reset as result of the function (or zero,
  If the substring is not retrieved). Search of a substring SubStrToDel is fulfilled without
  Case sensitivity of characters (the large and small characters do not differ).}

function Q_DeleteLastText (var S: string; const SubStrToDel: string; LastPos:
  Integer = MaxInt): Integer;

{ Q_ReplaceChar substitutes in string S each log-on of the character ChOld on the character
  ChNew. The result of the function is equal to quantity of the effected replacements. Initial
  The string S varies.}

function Q_ReplaceChar (var S: string; ChOld, ChNew: Char): Integer;

{ Q_ReplaceChars substitutes in string S all characters from string StrChOld ���������-
  ������� by characters from string StrChNew. The initial string S varies. If
  The number of characters in string StrChOld is not equal to a number of characters in string StrChNew,
  There is an exclusive situation such as Exception.}

procedure Q_ReplaceChars (var S: string; const StrChOld, StrChNew: string);

{ Q_ReplaceCharsByOneChar substitutes all labour contract going log-ons of characters from
  Sets ChOldSet in string S by one character ChNew. Initial string S at
  It varies. If it is necessary to substitute some characters by one without deleting
  Repetitions, take advantage of the procedure Q_ReplaceChars.}

procedure Q_ReplaceCharsByOneChar (var S: string; const ChOldSet: TCharSet;
  ChNew: Char);

{ Q_StrScan finds the first log-on of the character Ch in string S, since the character
  Number StartPos. Resets number of the retrieved character or zero, if the character Ch
  In string S is not retrieved. Function Q_PStrScan is similar Q_StrScan for
  Pointer (String).}

function Q_StrScan (const S: string; Ch: Char; StartPos: Integer = 1): Integer;
function Q_PStrScan (P: Pointer; Ch: Char; StartPos: Integer = 1): Integer;

{ Q_StrRScan finds the previous log-on of the character Ch in string S. In the parameter
  LastPos number of the character appropriate to current log-on Ch in is transmitted
  String S. The search starts with the character prior to the character S [LastPos] or
  From the last character of string, if LastPos exceeds length of string S. The function
  Resets number of the retrieved character or zero, if character Ch is not retrieved in
  To string S more to the left of the character with number LastPos. The function Q_PStrRScan is similar
  Q_StrRScan For Pointer (String).}

function Q_StrRScan (const S: string; Ch: Char; LastPos: Integer = MaxInt): Integer;
function Q_PStrRScan (P: Pointer; Ch: Char; LastPos: Integer = MaxInt): Integer;

{ Q_StrSpn resets number of the first character of string S, not included in string
  Delimiters, since the character number StartPos. The string Delimiters sets
  Set of characters - separators, and the function finds the first character,
  Not being a separator. If the usual characters in string S are absent,
  The zero is reset. If Delimiters is equal to empty string, the list is used
  Separators, which were preset during previous call of one of
  The following functions: Q_StrTok, Q_StrSpn, Q_StrCSpn, Q_ProperCase, Q_WordAtPos,
  Q_GetWordN, Q_ProperCaseInPlace, Q_SetDelimiters, Q_CountOfWords. If
  The separators are transmitted as set such as TCharSet, they are not stored
  For the subsequent calls of functions.}

function Q_StrSpn (const S, Delimiters: string; StartPos: Cardinal = 1): Integer; overload;
function Q_StrSpn (const S: string; StartPos: Cardinal = 1;
  const Delimiters: TCharSet = Q_StdDelimsSet): Integer; overload;

{ Q_StrCSpn resets number of the first character of string S, included in string
  Delimiters, since the character number StartPos. The string Delimiters sets
  Set of characters - separators, and the function finds the first character - separator.
  If the characters - separators in string S are absent, the zero is reset. If
  Delimiters is equal to empty string, the list of separators, which is used
  Were preset during the previous call of one from the following functions: Q_StrTok,
  Q_StrSpn, Q_StrCSpn, Q_ProperCase, Q_ProperCaseInPlace, Q_WordAtPos,
  Q_GetWordN, Q_SetDelimiters, Q_CountOfWords. If the separators are transmitted
  As set, they are not stored for the subsequent calls of functions.}

function Q_StrCSpn (const S, Delimiters: string; StartPos: Cardinal = 1): Integer; overload;
function Q_StrCSpn (const S: string; StartPos: Cardinal = 1;
  const Delimiters: TCharSet = Q_StdDelimsSet): Integer; overload;

{ Q_DelCharInPlace deletes all characters Ch from string S. Initial string at
  It varies.}

procedure Q_DelCharInPlace (var S: string; Ch: Char = ' ');

{ Q_DelChar deletes all characters Ch from string transferred(handed) by the parameter S.}

function Q_DelChar (const S: string; Ch: Char = ' '): string;

{ Q_Delete deletes a substring from string S. Thus the initial string varies.
  Index - index of the first deleted character, Count - quantity of characters,
  Subjects to deleting. This function works faster, than standard Delete.}

procedure Q_Delete (var S: string; Index, Count: Integer);

{ Q_DelChars deletes from string S characters, which are present at string
  (Or set) CharsToRemove. The initial string S varies.}

procedure Q_DelChars (var S: string; const CharsToRemove: string); overload;
procedure Q_DelChars (var S: string; const CharsToRemove: TCharSet); overload;

{ Q_KeepChars leaves in string S only those characters, which are present in
  To string (or set) CharsToKeep, other characters deletes. Initial
  The string S varies.}

procedure Q_KeepChars (var S: string; const CharsToKeep: string); overload;
procedure Q_KeepChars (var S: string; const CharsToKeep: TCharSet); overload;

{ Q_ApplyMask applies a mask Mask to string SourceStr and resets obtained
  String as result of the function. Character MaskChar is used in string Mask for
  The instructions(indication) of positions, in which are substituted characters from string SourceStr. Length
  SourceStr should be equal to quantity of characters MaskChar a mask. An example:
   Q_ApplyMask('(###) ##-##-##','075723293','# ') will return string ' (075) 72-32-93 '.
  The idea is borrowed by this and following three functions from the unit jbStr. Pas, writer
  Which is Jaro Benes (micrel@micrel.cz). }

function Q_ApplyMask (const Mask, SourceStr: string; MaskChar: Char = 'X'): string;

{ Q_ApplyMaskInPlace applies a mask Mask to string SourceStr and saves
  The obtained string in a variable Mask. The character MaskChar is used in string
  Mask for the instruction(indication) of positions, in which the characters from string are substituted
  SourceStr. Length of string SourceStr should correspond(meet) to quantity of characters
  Substitutions MaskChar in a mask Mask.}

procedure Q_ApplyMaskInPlace (var Mask: string; const SourceStr: string;
  MaskChar: Char = 'X');

{ Q_ExtractByMask deletes from string S all characters being fixed
  For the given mask Mask, leaving only substituted characters, i.e. characters,
  To which in a mask there corresponds(meets) the character MaskChar. Obtained thus
  The string is reset as result of the function. Length of strings S and Mask should be
  Identical. An example: Q_ExtractByMask ('7-35-01','X-XX-XX') will return '73501'.}

function Q_ExtractByMask (const S, Mask: string; MaskChar: Char = 'X'): string;

{ Q_ExtractByMaskInPlace deletes from string S all characters being
  Fixed for the given mask Mask, leaving only substituted characters,
  I.e. characters, with which in a mask there corresponds(meets) the character MaskChar. Obtained
  Thus string is saved in a variable transferred(handed) by the parameter S.
  Initial length of strings S and Mask should be identical.}

procedure Q_ExtractByMaskInPlace (var S: string; const Mask: string;
  MaskChar: Char = 'X');


{ Formatting strings, clipping of fragments of string.}

{ Q_TrimInPlace deletes carrying on and end blanks and command characters from
  Strings S. Thus the initial string varies. This procedure works
  Faster, than standard function Trim.}

procedure Q_TrimInPlace (var S: string);

{ Q_TrimLeftInPlace deletes carrying on blanks and command characters from string S.
  Thus the initial string varies. This procedure works faster, than
  The standard function TrimLeft.}

procedure Q_TrimLeftInPlace (var S: string);

{ Q_TrimRightInPlace deletes end blanks and command characters from
  Strings S. Thus the initial string varies. This procedure works
  Faster, than standard function TrimRight.}

procedure Q_TrimRightInPlace (var S: string);

{ Q_TrimChar deletes carrying on and end characters Ch from string S. Initial
  The string S does not vary.}

function Q_TrimChar (const S: string; Ch: Char = ' '): string;

{ Q_TrimCharLeft deletes carrying on characters Ch from string S. The initial string S
  Does not vary.}

function Q_TrimCharLeft (const S: string; Ch: Char = ' '): string;

{ Q_TrimCharRight deletes end characters Ch from string S. The initial string S
  Does not vary.}

function Q_TrimCharRight (const S: string; Ch: Char = ' '): string;

{ Q_KeepOneChar deletes all the labour contract going characters Ch, except for one, from string,
  Transferred(handed) by the parameter S. The initial string thus does not vary. For example,
  Q_KeepOneChar (' How do you do ', ') will return string ' How do you do '.}

function Q_KeepOneChar (const S: string; Ch: Char = ' '): string;

{ Q_SpaceCompressInPlace deletes from string initial and trailing blanks and
  The command characters (are less than a blank). Besides all the labour contract going blanks
  And the command characters in middle of string are substituted by one blank. Initial
  The string varies.}

procedure Q_SpaceCompressInPlace (var S: string);

{ Q_SpaceCompress deletes from string initial both trailing blanks and controlling
  The characters (are less than a blank). Besides all the labour contract going blanks and controlling
  The characters in middle of string are substituted by one blank. The initial string thus
  Does not vary. This function works more slowly, than Q_SpaceCompressInPlace.}

function Q_SpaceCompress (const S: string): string;

{ Q_PadLeft supplements string S by characters PadCh at the left up to length Length. If
  Length of string S is more Length,, if the parameter Cut = True, string ����������
  On the right up to length Length, differently (Cut = False) the initial string S is reset. }

function Q_PadLeft (const S: string; Length: Integer; PadCh: Char = ' ';
  Cut: Boolean = False): string;

{ Q_PadRight supplements string S by characters PadCh on the right up to length Length. If
  Length of string S is more Length,, if the parameter Cut = True, string ����������
  On the right up to length Length, differently (Cut = False) the initial string S is reset.}

function Q_PadRight (const S: string; Length: Integer; PadCh: Char = ' ';
  Cut: Boolean = False): string;

{ Q_CenterStr centers string S by characters PadCh concerning length Length.
  If length of string S is more Length,, if the parameter Cut = True, string
  ���������� on the right up to length Length, differently (Cut = False) is reset initial
  String S.}

function Q_CenterStr (const S: string; Length: Integer; PadCh: Char = ' ';
  Cut: Boolean = False): string;

{ Q_PadInside adds (evenly) characters PadCh in string S in those positions,
  Where they already are present, so long as length of string does not become equal
  Length. In other words, this function makes alignment of the text on both
  To edges(territories). If length of string exceeds Length,, if the parameter Cut = True,
  The string ���������� on the right up to length Length, differently (Cut = False) is reset
  The initial string S. Any special characters are not recognized.}

function Q_PadInside (const S: string; Length: Integer; PadCh: Char = ' ';
  Cut: Boolean = False): string;

{ Q_TabsToSpaces substitutes all tab characters (#9) in string S by blanks.
  The interval of a tab stops is set by the parameter TabStop.}

function Q_TabsToSpaces (const S: string; TabStop: Integer = 8): string;

{ Q_SpacesToTabs substitutes sequences of blanks in string S by characters
  Tab stops #9. The interval of a tab stops is set by the parameter TabStop. This function
  Works, even, if the initial string already contains the tab characters.}

function Q_SpacesToTabs (const S: string; TabStop: Integer = 8): string;

{ Q_StrTok resets next fragment of string S, simultaneously deleting it(him) from
  The initial string. Q_StrTok considers string S as sequence from
  Zero or more text fragments separated from each other one or more
  By character - separator from string Delimiters. In the parameter Delimiters is transmitted
  String, which consists of characters, which are considered as separators
  For string S. Separators do not switch on in a fragment reset
  By the function Q_StrTok. If the string starts with characters - separators, all
  They are deleted. If Delimiters - empty string, the separators are used,
  Which were preset during the previous call of one from the following functions:
  Q_StrTok, Q_StrSpn, Q_StrCSpn, Q_ProperCase, Q_WordAtPos, Q_SetDelimiters,
  Q_GetWordN, Q_ProperCaseInPlace, Q_CountOfWords. If the separators are transmitted
  As set, they are not stored for the subsequent calls of functions.}

function Q_StrTok (var S: string; const Delimiters: string): string; overload;
function Q_StrTok (var S: string;
  const Delimiters: TCharSet = Q_StdDelimsSet): string; overload;

{ Q_StrTok1 resets the next fragment of string S, simultaneously deleting it(him)
  From the initial string and deleting the character - separator, following it(him). Q_StrTok1
  Considers string S as a sequence from zero or more text
  Fragments separated from each other by a single character - separator from
  Strings Delimiters. If in string S the labour contract goes some separators,
  The function will reset empty string for each such character, if
  Before it(him) there is no text fragment. In the parameter Delimiters is transmitted
  String, which consists of characters, which are considered as separators
  For string S. Separators do not switch on in a fragment reset
  By the function Q_StrTok1. If Delimiters - empty string, the characters are used,
  Which were preset during the previous call of one from the following functions:
  Q_StrTok, Q_StrSpn, Q_StrCSpn, Q_ProperCase, Q_WordAtPos, Q_SetDelimiters,
  Q_GetWordN, Q_ProperCaseInPlace, Q_CountOfWords. If the separators are transmitted
  As set, they are not stored for the subsequent calls of functions.}

function Q_StrTok1 (var S: string; const Delimiters: string): string; overload;
function Q_StrTok1 (var S: string;
  const Delimiters: TCharSet = Q_StdDelimsSet): string; overload;

{ Q_WordAtPos resets a word from string S, in which composition is found
  The character S [Pos]. The string Delimiters sets characters considered as separators
  Words in string S. If the character in a position Pos is a separator, function
  Resets empty string. If Delimiters is equal to empty string, is used
  List of separators, which were preset during previous call of one of
  The following functions: Q_StrTok, Q_StrSpn, Q_StrCSpn, Q_ProperCase, Q_WordAtPos,
  Q_GetWordN, Q_ProperCaseInPlace, Q_SetDelimiters, Q_CountOfWords. If
  The separators are transmitted as set such as TCharSet, they are not stored
  For the subsequent calls of functions.}

function Q_WordAtPos (const S: string; Pos: Integer; const Delimiters: string): string; overload;
function Q_WordAtPos (const S: string; Pos: Integer;
  const Delimiters: TCharSet = Q_StdDelimsSet): string; overload;

{ Q_GetWordN resets a word with a serial number OrdN from string S. Words
  Are numbered from unit. The string Delimiters sets characters considered divide
  ������ of words in string S. If the word with number OrdN in string S is absent,
  The empty string is reset. If Delimiters is equal to empty string, is used
  List of separators, which were preset during previous call of one of
  The following functions: Q_StrTok, Q_StrSpn, Q_StrCSpn, Q_ProperCase, Q_WordAtPos,
  Q_GetWordN, Q_ProperCaseInPlace, Q_SetDelimiters, Q_CountOfWords. If
  The separators are transmitted as set such as TCharSet, they are not stored
  For the subsequent calls of functions.}

function Q_GetWordN (OrdN: Integer; const S, Delimiters: string): string; overload;
function Q_GetWordN (OrdN: Integer; const S: string;
  const Delimiters: TCharSet = Q_StdDelimsSet): string; overload;

{ The idea of the following of five functions is borrowed from the unit cStrings.pas, writer
  Which is David Butler (david@e.co.za). }

{ Q_CopyRange resets a substring from string S, since the character, index
  Which is equal Start, and finishing the character with an index Stop.}

function Q_CopyRange (const S: string; Start, Stop: Integer): string;

{ Q_CopyFrom resets a substring from string S, since the character, index
  Which is equal Start, and up to the end of string.}

function Q_CopyFrom (const S: string; Start: Integer): string;

{ Q_CopyLeft resets Count of the first characters of string transferred(handed) by the parameter S.}

function Q_CopyLeft (const S: string; Count: Integer): string;

{ Q_CopyRight resets Count of the last characters of string transferred(handed)
  By the parameter S.}

function Q_CopyRight (const S: string; Count: Integer): string;

{ Q_PasteStr substitutes Count of characters of string Dest, since a position Pos, �����-
  ����� of string Source. The string Dest thus varies. For example, if string
  Dest is equal 'How do you do', after execution of the procedure: Q_PasteStr (Dest,
  5,6,'does he'), she(it) will be equal 'How does he do'. This procedure is fulfilled
  Faster, than combination standard Delete-Insert.}

procedure Q_PasteStr (var Dest: string; Pos, Count: Integer; const Source: string);

{ Q_CopyDel resets a substring, simultaneously deleting her(it) from the initial string.
  S - initial string, Start - number of the first cut character, Len - length
  Cut substring. This function was borrowed from AGSLib.pas written by Alexey Lukin.}

function Q_CopyDel (var S: string; Start, Length: Integer): string;


{ Other functions for operation with strings.}

{ Q_SetDelimiters sets string (or set) characters - separators of words
  For the subsequent call of such functions, as Q_StrTok, Q_StrSpn, Q_StrCSpn,
  Q_ProperCase, Q_WordAtPos, Q_GetWordN, Q_ProperCaseInPlace, Q_CountOfWords.
  In this case argument Delimiters by call of these functions should be equal
  To empty string. If procedure Q_SetDelimiters is called(caused) without parameters, in
  Quality of separators the characters from constant set are received
  Q_StdDelimsSet, unit, defined in the beginning, QStrings.}

procedure Q_SetDelimiters (const Delimiters: string); overload;
procedure Q_SetDelimiters (const Delimiters: TCharSet = Q_StdDelimsSet); overload;

{ Q_GetDelimiters resets string consisting of characters - separators of words,
  Listed in the order, their appropriate codes. If were not called(caused) earlier
  Any functions specifying string of separators (for example Q_SetDelimiters),
  The result of the function Q_GetDelimiters will be uncertain.}

function Q_GetDelimiters: string;

{ Q_StrMoveL copies contents of string Source in string Dest. Maximum
  The number of copied characters is equal MaxL. Length of string Dest is installed
  Equal to number of the copied characters. The memory for string Dest should be
  Is distributed beforehand by function call SetString (or SetLength) (size
  Not less MaxL of characters). Q_StrMoveL works much faster, than usual
  The assignment of string, at which occurs its(her) copying.}

procedure Q_StrMoveL (const Source: string; var Dest: string; MaxL: Cardinal);

{ Q_StrReverse overturns string S so, that the first character becomes
  By the latter, second - penultimate etc. thus varies initial
  String. The function Q_PStrReverse is similar Q_StrReverse for Pointer (String).
  Besides she(it) resets the pointer by the beginning of string.}

procedure Q_StrReverse (var S: string);
function Q_PStrReverse (P: Pointer): Pointer;

{ Q_CutLeft �������� string S at the left on CharCount of characters, reducing thus
  Its(her) length. If the parameter CharCount negative, string ���������� on the right.}

procedure Q_CutLeft (var S: string; CharCount: Integer);

{ Q_CutRight �������� string S on the right on CharCount of characters, reducing thus
  Its(her) length. If the parameter CharCount negative, string ���������� at the left.}

procedure Q_CutRight (var S: string; CharCount: Integer);

{ Q_RotateLeft cyclic shifts string S on Shift of characters to the left. The parameter
  Shift can be both positive, and negative.}

procedure Q_RotateLeft (var S: string; Shift: Integer);

{ Q_RotateRight cyclic shifts string S on Shift of characters to the right.
  The parameter Shift can be both positive, and negative.}

procedure Q_RotateRight (var S: string; Shift: Integer);

{ Q_Duplicate resets string consisting from Count of copies of string S.}

function Q_Duplicate (const S: string; Count: Integer): string;

{ Q_Base64Encode resets string S or byte array addressed by the parameter
  P, length L byte encoded in the format Base64 (MIME), frequently used
  At transfer of files by the e-mail. The resulting string is supplemented
  In the end by characters '=' up to length, multiple 4. To avoid addition, length
  The initial string or byte array should be multiple 3.}

function Q_Base64Encode (const S: string): string; overload;
function Q_Base64Encode (P: Pointer; L: Cardinal): string; overload;

{ Q_Base64Decode resets result �������������� of string S from the coding
  Base64, i.e. restores initial contents of string. Length
  The encoded string should be multiple 4.}

function Q_Base64Decode (const S: string): string;

{ The following functions realize compression of text and binary strings by a method RLE.
  The given method is based on deleting from the text of sequences identical
  Characters (replacement them by one character and counter). If the text does not contain
  Similar sequences, his(its) length usually does not vary. Length of the text
  Can even be magnified (in the worse case twice), if it(he) contains
  Special characters (see constants RLECC in section implementation). This
  The algorithm is very fast and provides effective compression of the information,
  Containing group of repeating bytes.

  If the character repeats less than three times, any replacements is not made.
  If number of repetitions of the character from three up to eight, this sequence
  Is substituted by two characters, from 9 up to 127 - three characters, from 128 up to 16383 -
  By four, from 16384 up to 2097151 - five characters, from 2097152 up to 268435455 -
  By six characters, otherwise - seven characters. Everyone single special
  The character is substituted by a couple of characters, sequence from two special
  Characters (identical) the sequence from 3 is substituted by a couple of characters
  Up to 127 special characters is substituted by three characters, further - as it is usual.
  If it is supposed to use these procedures for compression gray-scale
  Graphics images, the algorithm of their operation should be reconsidered on a subject
  Decreases of number of special characters with seven up to two or three.}

{ Q_GetPackRLESize resets a size of the output array (in bytes), which
  Is necessary for saving in it(him) of results of compression by a method RLE of the data,
  Addressed by the parameter Source by a size SrcL byte. Greatest possible
  The size is equal to double value of the parameter SrcL (worst variant).}

function Q_GetPackRLESize (Source: Pointer; SrcL: Cardinal): Cardinal;

{ Q_GetUnpackRLESize resets a size of the output array (in bytes), which
  Is necessary for saving in it(him) of results of unpacking of the data compressed by a method
  RLE, addressed by the parameter Source by a size SrcL byte. This function is called(caused)
  Before call Q_UnpackRLE for the byte array.}

function Q_GetUnpackRLESize (Source: Pointer; SrcL: Cardinal): Cardinal;

{ Q_PackRLE fulfils data compression addressed by the parameter Source, length
  SrcL byte by a method RLE. The result is saved in a storage area pointed
  Dest. The result of the function is equal to a size (bytes) output data array.
  The size of a storage area Dest should be sufficient (necessary size
  Is defined(determined) with the help Q_GetPackRLESize). Inside the  function any
  Similar checks is not fulfilled.}

function Q_PackRLE (Source, Dest: Pointer; SrcL: Cardinal): Cardinal; overload;

{ Q_UnpackRLE fulfils unpacking the data addressed by the parameter Source, length
  SrcL byte compressed by a method RLE. The output data is saved in a storage area
  Pointed Dest. The result of the function is equal to a size (bytes) output
  Data array. The size of a storage area Dest should be sufficient for this purpose,
  To contain all data (it(him) it is possible to define with the help Q_GetUnpackRLESize).
  Inside the function Q_UnpackRLE the size of a storage area is not checked.}

function Q_UnpackRLE (Source, Dest: Pointer; SrcL: Cardinal): Cardinal; overload;

{ Q_PackRLE packs string S by a method RLE. In the parameter MaxL is transmitted
  Greatest possible length of output string or -1, for automatic
  Definitions of length of output string.}

function Q_PackRLE (const S: string; MaxL: Integer = -1): string; overload;

{ Q_UnpackRLE fulfils unpacking string S, compressed by a method RLE. In the parameter
  MaxL is transmitted greatest possible length of output string or -1, for
  Autodetections of length of output string.}

function Q_UnpackRLE (const S: string; MaxL: Integer = -1): string; overload;


{ Information functions.}

{ Q_PStrLen resets length of string such as PChar, addressed by the parameter P. This
  The function works much faster standard StrLen. By the writer of implementation
  Is Robert Lee (rhlee@optimalcode.com). }

function Q_PStrLen (P: PChar): Integer;

{ Q_IsEmptyStr resets True, if the string S contains only characters from string
  (Or set) EmptyChars, differently function resets False. This function can
  To be called(caused) without the parameter EmptyChars. In this case all characters smaller or
  Equal to a blank are considered empty.}

function Q_IsEmptyStr (const S, EmptyChars: string): Boolean; overload;
function Q_IsEmptyStr (const S: string; const EmptyChars: TCharSet): Boolean; overload;
function Q_IsEmptyStr (const S: string): Boolean; overload;

{ Q_CharCount foots quantity of log-ons of the character Ch in string S.}

function Q_CharCount (const S: string; Ch: Char): Integer;

{ Q_CharsCount foots in string S quantity of log-ons of characters,
  Present in set CharSet.}

function Q_CharsCount (const S: string; const CharSet: TCharSet): Integer;

{ Q_GetCharStr resets string containing in alphabetic order all characters
  (On one), included in a composition of the initial string S.}

function Q_GetCharStr (const S: string): string;

{ Q_CountOfWords foots quantity of words in string transferred(handed) by the parameter
  S. In string Delimiters the characters are transmitted which are considered just -
  Dividers of words in string S. If Delimiters is equal to empty string, is used
  List of separators, which were preset during previous call of one of
  The following functions: Q_StrTok, Q_StrSpn, Q_StrCSpn, Q_ProperCase, Q_WordAtPos,
  Q_GetWordN, Q_ProperCaseInPlace, Q_SetDelimiters, Q_CountOfWords. If
  The separators are transmitted as set such as TCharSet, they are not stored
  For the subsequent calls of functions.}

function Q_CountOfWords (const S, Delimiters: string): Integer; overload;
function Q_CountOfWords (const S: string;
  const Delimiters: TCharSet = Q_StdDelimsSet): Integer; overload;

{ Q_StrCheckSum resets the sum of the characters codes making string S. It
  The value can be used as the check total. The function Q_PStrCheckSum
  Is similar Q_StrCheckSum for Pointer (String). For a control of integrity
  The data are more reliable for using the function Q_CRC32.}

function Q_StrCheckSum (const S: string): LongWord;
function Q_PStrCheckSum (P: Pointer): LongWord;

{ Q_StrCheckXOR resets number which is growing out of association everyone
  Four characters of string S in one double word with the help " eliminating or ".
  This operation is fulfilled very fast. The obtained thus number can
  To be used as the check total or value of hash-function. However, for
  Reliable control of data integrity, it is better to use Q_CRC32. The function
  Q_PStrCheckXOR Is similar Q_StrCheckXOR for Pointer (String).}

function Q_StrCheckXOR (const S: string): LongWord;
function Q_PStrCheckXOR (P: Pointer): LongWord;

{ Q_StrHashKey generates hash-code for string transferred(handed) by the parameter S.
  In string the large and small characters differ. The function Q_PStrHashKey
  Is similar Q_StrHashKey for Pointer (String).}

function Q_StrHashKey (const S: string): LongWord;
function Q_PStrHashKey (P: Pointer): LongWord;

{ Q_TextHashKey generates hash-code for string transferred(handed) by the parameter S.
  The large and small characters do not differ. The function Q_PTextHashKey is similar
  Q_TextHashKey For Pointer (String).}

function Q_TextHashKey (const S: string): LongWord;
function Q_PTextHashKey (P: Pointer): LongWord;

{ Q_CRC32 calculates CRC-32 for a storage area addressed by the parameter P. A size
  To storage area (in bytes) is set by the parameter L.}

function Q_CRC32 (P: Pointer; L: Cardinal): LongWord;

{ Q_NextCRC32 calculates on base value CRC32 new value for area
  Memories addressed by the parameter P, size L byte. New CRC32 is reset
  As value of the same parameter, and also as result of the function. If
  Earlier CRC32 was not calculated, the appropriate parameter should be equal 0.}

function Q_NextCRC32 (var CRC32: LongWord; P: Pointer; L: Cardinal): LongWord;

{ Q_TimeStamp reads out contents of a 64-bit time counter, which
  Is magnified by unit at each clock tick of the processor (value of the counter
  Is read by the command RDTSC). This function does not work on processors below P5.}

function Q_TimeStamp: Int64;


{ Function for operation with sets such as TCharSet.}

{ The standard procedures Include, Exclude and 'in' operator are assembled
  In the effective enough code. Instead of all other operations with sets,
  For a type TCharSet it is necessary to use functions from this units.}

{ Q_GetCharSet resets set such as TCharSet, switching on only characters,
  Which are present at string S.}

function Q_GetCharSet (const S: string): TCharSet;

{ Q_CharSetToStr resets string of characters, which represents
  Listing in alphabetic order of characters present in set
  CharSet such as TCharSet.}

function Q_CharSetToStr (const CharSet: TCharSet): string;

{ Q_ComplementChar adds in set CharSet the character Ch, if it(him) there
  Was not, or deletes the character Ch from this set, if it(he) there was.}

procedure Q_ComplementChar (var CharSet: TCharSet; Ch: Char);

{ Q_ClearCharSet deletes all characters from set CharSet. After an output(exit)
  From this procedure the set CharSet becomes empty.}

procedure Q_ClearCharSet (var CharSet: TCharSet);

{ Q_FillCharSet adds in set CharSet all missing characters. The ambassador
  Output(exit) from this procedure the set CharSet contains all possible(probable) characters.}

procedure Q_FillCharSet (var CharSet: TCharSet);

{ Q_ComplementSet inverts set CharSet. After execution by this
  Procedures the set CharSet will contain only those characters, which
  Were absent in this set before call Q_ComplementSet.}

procedure Q_ComplementSet (var CharSet: TCharSet);

{ Q_CloneCharSet copies set SourceSet in set DestSet. Both these
  The sets should have the type TCharSet. After execution of this procedure call
  The functions Q_IsEqualSet (SourceSet, DestSet) will be returned by(with) value True.}

procedure Q_CloneCharSet (const SourceSet: TCharSet; var DestSet: TCharSet);

{ Q_CharSetUnion consolidates sets SourceSet and DestSet and saves result
  In set DestSet. The resulting set includes characters, which
  Contain even in one of the initial sets.}

procedure Q_CharSetUnion (var DestSet: TCharSet; const SourceSet: TCharSet);

{ Q_CharSetSubtract subtracts set SourceSet from set DestSet and
  Saves result in set DestSet. The resulting set includes
  Only characters, which were present at set DestSet, but not ������-
  �������� (i.e. were absent) in set SourceSet.}

procedure Q_CharSetSubtract (var DestSet: TCharSet; const SourceSet: TCharSet);

{ Q_CharSetIntersect finds an intersection (common units) sets SourceSet
  And DestSet also saves result in set DestSet. Obtained thus
  The set contains only characters present in both sets.}

procedure Q_CharSetIntersect (var DestSet: TCharSet; const SourceSet: TCharSet);

{ Q_CharSetXOR adds in set DestSet characters present in
  Set SourceSet, but absent in set DestSet, also deletes from
  Sets DestSet characters present in set SourceSet and
  Simultaneously present in set DestSet.}

procedure Q_CharSetXOR (var DestSet: TCharSet; const SourceSet: TCharSet);

{ Q_IsSubset checks, whether is the set LeftSet a subset in RightSet.
  This function resets True, if all characters present in set
  LeftSet, contain as well in set RightSet. If it is not fulfilled,
  The function resets False.}

function Q_IsSubset (const LeftSet, RightSet: TCharSet): Boolean;

{ Q_IsSuperset checks, whether is set LeftSet superset for
  RightSet. This function resets True, if set LeftSet contains on
  To extreme measure all characters from set RightSet. If it is not fulfilled,
  The function resets False.}

function Q_IsSuperset (const LeftSet, RightSet: TCharSet): Boolean;

{ Q_IsEqualSet checks equality of sets LeftSet and RightSet. The function
  Resets True, if each character from set LeftSet is present in
  Set RightSet and on the contrary. Differently, the function resets False.}

function Q_IsEqualSet (const LeftSet, RightSet: TCharSet): Boolean;

{ Q_IsEmptySet resets True, if CharSet is equal to empty set (i.e. it
  The set does not contain any character), differently function resets False.}

function Q_IsEmptySet (const CharSet: TCharSet): Boolean;

{ Q_CharSetCharCount resets quantity of characters in set CharSet.}

function Q_CharSetCharCount (const CharSet: TCharSet): Integer;


{ Function for operation with character record of numbers.}

{ Plenty of functions for conversion of number in string and on the contrary,
  For operation with date and time is found in the unit SysUtils. Here represented
  Some functions, which are critical on execution time or
  Are absent in standard libraries.}

{ Q_IsInteger resets True, if the string S contains character record whole
  Decimal number, which can be saved in a variable such as Integer,
  Differently function resets False.}

function Q_IsInteger (const S: string): Boolean;

{ Q_IsCardinal resets True, if the string S contains character record
  ������������ of the whole decimal or hexadecimal number, which can
  To be saved in a variable such as Cardinal, differently function resets False.}

function Q_IsCardinal (const S: string): Boolean;

{ Q_IsDecimal resets True, if the string S contains only characters,
  Appropriate to decimal digits, differently resets False.}

function Q_IsDecimal (const S: string): Boolean;

{ Q_IsHexadecimal resets True, if the string S contains only characters,
  Appropriate to hexadecimal digits, differently resets False.}

function Q_IsHexadecimal (const S: string): Boolean;

{ Q_IsOctal resets True, if the string S contains only characters,
  Appropriate to octal digits, differently resets False.}

function Q_IsOctal (const S: string): Boolean;

{ Q_IsBinary resets True, if the string S contains only characters,
  Appropriate to binary figures, differently resets False.}

function Q_IsBinary (const S: string): Boolean;

{ Q_IsFloat checks, whether the string S contains character record material
  Numbers, which includes a mantissa and, probably, the order (they can be as
  Positive, and negative). For sharing the whole and fractional part
  The character from a standard variable DecimalSeparator is used. If string
  S can be transformed to the material type, the function resets True,
  Differently ����������� False.}

function Q_IsFloat (const S: string): Boolean;

{ Q_AdjustSeparator substitutes the first point in string S on a comma, if the character
  DecimalSeparator is equal comma, or substitutes first comma in string S on
  Point, if DecimalSeparator is equal to a point. After application of this function to
  String she(it) can be transformed in number with fractional part irrespective of
  What character the fractional part is separated (point or comma). However,
  It is necessary to remember, that the procedure Val does not pay notice on DecimalSeparator and
  Always expects, that the fractional part is separated by a point.}

function Q_AdjustSeparator (const S: string): string;

{ The functions Q_BetweenXXX reset True, if the string S contains number (whole,
  Whole without the sign, 64-bit whole, material or number designating
  The pecuniary sum), finding in a range [LowBound, HighBound]. Differently,
  The functions reset False. By operation with Q_BetweenFloat and Q_BetweenCurr
  It is necessary to take into account, that the whole part of number should be separated from fractional
  Parts by the character assigned by a variable DecimalSeparator (from SysUtils).}

function Q_BetweenInt (const S: string; LowBound, HighBound: Integer): Boolean;
function Q_BetweenUInt (const S: string; LowBound, HighBound: LongWord): Boolean;
function Q_BetweenInt64 (const S: string; LowBound, HighBound: Int64): Boolean;
function Q_BetweenFloat (const S: string; LowBound, HighBound: Extended): Boolean;
function Q_BetweenCurr (const S: string; LowBound, HighBound: Currency): Boolean;

{ The functions Q_StrToXXX will convert number, which character record contains
  In string S, in the normal numerical form. The variable, in which is saved
  The result, is transmitted by the parameter V. If conversion of string S in number V
  Has passed successfully, the functions reset True. If during conversion has arisen
  The error, is reset False. The exclusive situation thus does not arise.}

function Q_StrToInt (const S: string; var V: Integer): Boolean;
function Q_StrToUInt (const S: string; var V: LongWord): Boolean;
function Q_StrToInt64 (const S: string; var V: Int64): Boolean;
function Q_StrToFloat (const S: string; var V: Extended): Boolean;
function Q_StrToCurr (const S: string; var V: Currency): Boolean;

{ Q_IntToStr resets decimal record of number N as string.}

function Q_IntToStr (N: Integer): string;

{ Q_IntToStrBuf saves decimal record of number N in string S. Memory under
  String S should be selected(allocated) beforehand by function call SetString (or
  SetLength) size, sufficient for storage of the greatest possible number N
  In view of the sign. Q_IntToStrBuf works faster, than Q_IntToStr.}

procedure Q_IntToStrBuf (N: Integer; var S: string);

{ Q_UIntToStr resets decimal record ������������ of number N as string.}

function Q_UIntToStr (N: LongWord): string;

{ Q_UIntToStrBuf saves decimal record ������������ of number N in string S.
  The memory under string S should be selected(allocated) beforehand by function call SetString
  (Or SetLength) size, sufficient for storage greatest possible
  Numbers N. Q_UIntToStrBuf works faster, than Q_UIntToStr.}

procedure Q_UIntToStrBuf (N: LongWord; var S: string);

{ Q_UIntToStrL resets decimal record ������������ of number N as string.
  The parameter Digits sets quantity of characters in reset number. If it is necessary,
  It ���������� at the left or is supplemented in zero& at the left up to length Digits.}

function Q_UIntToStrL (N: LongWord; Digits: Cardinal): string;

{ Q_UIntToStrLBuf saves decimal record ������������ of number N in string S.
  The parameter Digits sets quantity of characters in reset number. If it is necessary,
  The string S ���������� at the left or is supplemented in zero& at the left up to length Digits.
  The memory under string S should be selected(allocated) beforehand by function call SetString
  By size not less Digits of characters. Q_UIntToStrLBuf works faster, than
  Q_UIntToStrL.}

procedure Q_UIntToStrLBuf (N: LongWord; Digits: Cardinal; var S: string);

{ Q_IntToRoman resets string containing number N, written Roman
  In digits. A possible(probable) range of numbers: from 1 up to 5000. If number N does not lay in
  The indicated range, '?????' string is returned.}

function Q_IntToRoman (N: Integer): string;

{ Q_RomanToInt will convert Roman number (�����������), transmitted string S,
  In an ordinary integer. If during conversion there is an error,
  The exclusive situation EConvertError.} is excited

function Q_RomanToInt (const S: string): Integer;

{ Q_UIntToHex resets hexadecimal record ������������ number N as
  Strings. The parameter Digits sets quantity of digits in reset number. If
  It is necessary, it ���������� at the left or is supplemented in zero& at the left up to length Digits.}

function Q_UIntToHex (N: LongWord; Digits: Cardinal): string;

{ Q_UIntToHexBuf saves hexadecimal record ������������ number N in
  To string S. The parameter Digits sets quantity of digits in reset number. If
  It is necessary, the string S ���������� at the left or is supplemented in zero& at the left up to length Digits.
  The memory under string S should be selected(allocated) beforehand by function call SetString
  By size not less Digits of characters. Q_UIntToHexBuf works faster, than
  Q_UIntToHex.}

procedure Q_UIntToHexBuf (N: LongWord; Digits: Cardinal; var S: string);

{ Q_HexToUInt will convert hexadecimal number transferred(handed) by string S,
  As a whole ����������� number. If during conversion there is an error,
  The exclusive situation EConvertError is excited.}

function Q_HexToUInt (const S: string): LongWord;

{ Q_UIntToOct resets octal record ������������ number N as
  Strings. The parameter Digits sets quantity of digits in reset number. If
  It is necessary, it ���������� at the left or is supplemented in zero& at the left up to length Digits.}

function Q_UIntToOct (N: LongWord; Digits: Cardinal): string;

{ Q_UIntToOctBuf saves octal record ������������ of number N in string S.
  The parameter Digits sets quantity of digits in reset number. If it is necessary,
  The string S ���������� at the left or is supplemented in zero& at the left up to length Digits.
  The memory under string S should be selected(allocated) beforehand by function call SetString
  By size not less Digits of characters. Q_UIntToOctBuf works faster, than
  Q_UIntToOct.}

procedure Q_UIntToOctBuf (N: LongWord; Digits: Cardinal; var S: string);

{ Q_OctToUInt will convert an octal number transferred(handed) by string S,
  As a whole ����������� number. If during conversion there is an error,
  The exclusive situation EConvertError is excited. }

function Q_OctToUInt (const S: string): LongWord;

{ Q_UIntToBin resets binary notation ������������ of number N as string.
  The parameter Digits sets quantity of digits in reset number. If it is necessary
  ���������� at the left or is supplemented in zero& at the left up to length Digits.}

function Q_UIntToBin (N: LongWord; Digits: Cardinal): string;

{ Q_UIntToBinBuf saves binary notation ������������ of number N in string S.
  The parameter Digits sets quantity of digits in reset number. If it is necessary,
  The string S ���������� at the left or is supplemented in zero& at the left up to length Digits.
  The memory under string S should be selected(allocated) beforehand by function call SetString
  By size not less Digits of characters. Q_UIntToBinBuf works faster, than
  Q_UIntToBin.}

procedure Q_UIntToBinBuf (N: LongWord; Digits: Cardinal; var S: string);

{ Q_BinToUInt will convert binary number transferred(handed) by string S, as a whole
  ����������� number. If during conversion there is an error,
  The exclusive situation EConvertError is excited. }

function Q_BinToUInt (const S: string): LongWord;

{ Q_ChangeBase translates ����������� number from a number system on the basis
  BaseFrom, in a number system on the basis BaseTo. Character record of number
  Is transmitted by string Number. The function resets number (string)((line)) written
  On the new basis. Parameters BaseFrom and BaseTo should be in range from
  2 up to 36. The digits more to than nine are designated by latin characters from A up to Z (or
  From a up to z). Quantity of signs among is not limited. If at it(him) are present
  Impermissible characters, the exclusive situation EConvertError is excited.
  If the parameter DigitsInGroup is distinct from zero, the digits in output string will be
  To be consolidated in groups on DigitsInGroup of digits. Among themselves groups are divided
  By the character GroupSeparator. The same character can be used in initial
  Number Number for sharing groups of digits.}

function Q_ChangeBase (const Number: string; BaseFrom, BaseTo: Cardinal;
  DigitsInGroup: Cardinal = 0; GroupSeparator: Char = ' '): string;

{ Q_StrToCodes resets string consisting of hexadecimal codes,
  Characters of string S. For example, Q_StrToCodes('XYZ#$*~') - > '58595A23242A7E'.
  The initial string thus does not vary.}

function Q_StrToCodes (const S: string): string;

{ Q_CodesToStr will convert string S, consisting from hexadecimal codes
  Characters, in string of characters. For example, Q_StrToCodes ('413F3C2A') - > 'A?<*'.
  The initial string thus does not vary. If during conversion
  There is an error, the exclusive situation EConvertError is excited. }

function Q_CodesToStr (const S: string): string;

{ Q_NumToStr saves in string S number transferred(handed) by the parameter N, written
  By words (in Russian). The parameter FmtFlags sets a way of conversion of number
  In string. The function Q_NumToStr resets number of the form, in which should stand
  The following behind the given number a word (see comment to constants rfmXXXX).
  The string S is always ended by a blank. If want to remove it(him), call
  Then Q_ShiftRight (S, 1).}

const
{ The constants for the parameter FmtFlags (can be consolidated them with the help "or"):}

  nsMale = 1;    {a Man's sort}
  nsFemale = 2;  {a Female sort}
  nsMiddle = 3;  {an Average sort}

  nsFull = 0;  {the Complete title of triads: one thousand, one million...}
  nsShort = 4; {the Brief title of triads: thousand, ���....}

{ Reset values of the function Q_NumToStr:}

  rfmFirst = 1;  {the First form: " one elephant " or " twenty one cats "}
  rfmSecond = 2; {the Second form: " three elephants " or " four cats "}
  rfmThird = 3;  {the Third form: " six elephants " or " eight cats "}

function Q_NumToStr(N: Int64; var S: string; FmtFlags: LongWord = nsMale): Integer;

{ Q_NumToRub resets the pecuniary sum in words. The parameter V should contain
  Numerical value of the pecuniary sum in roubles. The 100-th shares express copecks.
  The parameters RubFormat and CopFormat define(determine) record format accordingly
  Roubles and copecks. If CopFormat = nrNone, the sum is rounded off up to roubles and
  The copecks are not output. If RubFormat = nrNone, roubles are not output, and to
  To copecks the number of roubles multiplied on increases 100. If both parameters
  Are equal nrNone, the string containing number V is simply reset. A constant
  nrShTriad is combined with other constants with the help of the bit operation
  "or". The reset string starts with the large character. If the pecuniary sum
  Negative, the string consists in parentheseses. This function is written
  After acquaintance with the procedure num_to_rub (N2R.pas) ������� ��������.}

const
  nrNumShort = 1; {the Brief numerical format: " 475084 ���. " Or " 15 ���. "}
  nrShort = 3;    {the Brief lower case format: " ���. " Or " ten ���. "}
  nrNumFull = 0;  {the Complete numerical format: " 342 roubles " or " 25 copecks "}
  nrFull = 2;     {the Complete lower case format: " One rouble " or " two copecks "}
  nrShTriad = 4;  {Brief record of the titles of triads: thousand, ���....}
  nrNone = 8;     {There are no roubles, there are no copecks or simple numerical record}

function Q_NumToRub (V: Currency; RubFormat: LongWord = nrFull;
  CopFormat: LongWord = nrNumShort): string;


{ Function for operation with dates.}

{ Q_GetDateStr resets string representing date Date in Russian. For example,
  Q_GetDateStr(Now) can return: 'on February 9, 2000 '.}

function Q_GetDateStr (Date: TDateTime): string;

{ Q_GetMonthStr resets string representing month and year, appropriate
  To date Date, written in Russian. For example, Q_GetMonthStr (Now) can return:
  'February, 2000'.}

function Q_GetMonthStr (Date: TDateTime): string;


{ Function for operation with binary strings.}

{ Q_ZeroMem unsets a storage area pointed by the parameter P. Number byte
  Is set by the parameter L.}

procedure Q_ZeroMem (P: Pointer; L: Cardinal);

{ Q_OnesMem fills storage area addressed by the parameter P, units in
  All bits (byte $FF). The number byte is set by the parameter L.}

procedure Q_OnesMem (P: Pointer; L: Cardinal);

{ Q_FillChar fills L in adjoining byte addressed by the parameter P, value C.
  This procedure works faster standard FillChar.}

procedure Q_FillChar (P: Pointer; L: Cardinal; Ch: Char); overload;
procedure Q_FillChar (P: Pointer; L: Cardinal; Ch: Byte); overload;

{ Q_FillLong fills the array of units such as LongWord (or anyone another
  4-�-�������� such as), pointed by the parameter P, value Value. Quantity
  Array cells (not of bytes!!!) is transmitted by the parameter Count.}

procedure Q_FillLong (Value: LongWord; P: Pointer; Count: Cardinal);

{ Q_TinyFill fast fills up to 32 bytes of memory (inclusively), addressed
  By the parameter P, value, which is transmitted by the parameter Value (are used
  All 4 bytes Value). In the parameter L the number byte is set which is necessary
  To fill. Impermissible& to call(cause) Q_TinyFill with L it is more 32.}

procedure Q_TinyFill (P: Pointer; L: Cardinal; Value: LongWord);

{ Q_FillRandom fills the byte array addressed P, length L byte output
  By sequence of the standard generator of pseudo-random numbers. Initial
  The value of the generator is transmitted by the parameter Seed. It is necessary to mean, that
  This sequence is not ���������������� a rack and impermissible&
  To use her(it) for creation of keys of encoding.}

procedure Q_FillRandom (P: Pointer; L: Cardinal; Seed: LongWord);

{ Q_CopyMem copies L byte from a storage area pointed by the parameter
  Source, in a storage area pointed by the parameter Dest. Storage areas
  Should not be overlapped. Q_CopyMem works much faster, than
  The standard procedure Move and, even, faster, than Q_MoveMem.}

procedure Q_CopyMem (Source, Dest: Pointer; L: Cardinal);

{ Q_CopyLongs copies Count of array cells such as LongWord (or any
  Another 4-�-�������� of a type, for example, Integer or Pointer) from a storage area,
  Pointed by the parameter Source, in a storage area addressed by the parameter Dest.
  The storage areas should not be overlapped.}

procedure Q_CopyLongs (Source, Dest: Pointer; Count: Cardinal);

{ Q_TinyCopy fast copies up to 32 bytes (inclusively) from a storage area,
  Pointed by the parameter Source in a storage area pointed by the parameter Dest.
  The number byte for copying is set by the parameter L. An overlap of storage areas
  Is not admitted. The call with L > 32 results in interesting errors.}

procedure Q_TinyCopy (Source, Dest: Pointer; L: Cardinal);

{ Q_MoveMem copies L byte from Source in Dest. She(it) works even in a case,
  When the memories addressed by parameters Source and Dest, are overlapped.
  If the memories are displaced from each other less, than on 4 bytes,
  It is better to take advantage of procedures Q_MoveBytes or Q_MoveWords. If beforehand
  It is known, that the memories are not overlapped, it is better to use Q_CopyMem.
  Q_MoveMem Works faster, than standard procedure Move.}

procedure Q_MoveMem (Source, Dest: Pointer; L: Cardinal);

{ Q_MoveLongs copies Count of array cells such as LongWord (or another
  4-�-�������� such as) from storage area pointed by the parameter Source, in
  Storage area pointed by the parameter Dest. This procedure works even
  In a case, when the storage areas are overlapped.}

procedure Q_MoveLongs (Source, Dest: Pointer; Count: Cardinal);

{ Q_MoveWords copies Count of array cells of a type Word or Smallint (2-bytes)
  from a storage area pointed by the parameter Source, in a storage area,
  Pointed by the parameter Dest. The storage areas can be overlapped.}

procedure Q_MoveWords (Source, Dest: Pointer; Count: Cardinal);

{ Q_MoveBytes copies L byte from a storage area pointed by the parameter
  Source in a storage area pointed by the parameter Dest. This procedure
  It is recommended to use, if offset of one storage area rather
  Another is less 4 (especially, when it is equal 1).}

procedure Q_MoveBytes (Source, Dest: Pointer; L: Cardinal);

{ Q_SwapMem interchanges contents of two byte arrays addressed P1 and P2,
  In length L byte. Before usage of this procedure consider possibility
  Exchange of values of pointers by the procedure Q_Exchange.}

procedure Q_SwapMem (P1, P2: Pointer; L: Cardinal);

{ Q_SwapLongs interchanges contents of the arrays 4-������� of units addressed
  P1 And P2, length Count of double words. Before usage of this procedure
  Think of possibility of exchange of values of the pointers with the help Q_Exchange.}

procedure Q_SwapLongs (P1, P2: Pointer; Count: Cardinal);

{ Q_CompareMem fulfils ��������� matching of the memories addressed P1 and P2.
  The size of the memories (in bytes) is set by the parameter L. The function resets
  True, if contents of the memories are completely identical. This function works
  Faster by standard CompareMem.}

function Q_CompareMem (P1, P2: Pointer; L: Cardinal): Boolean;

{ Q_CompLongs fulfils ��������� matching of the arrays �������������� of units
  (For example, such as Integer), addressed P1 and P2. Quantity of units in the arrays
  Is set by the parameter Count. The function resets True, if all appropriate
  The units of both arrays are equal, is otherwise reset False.}

function Q_CompLongs (P1, P2: Pointer; Count: Cardinal): Boolean;

{ Q_CompMemS fulfils ��������� matching of the memories addressed P1 and P2,
  By a principle it is more - less. The size of both memories (in bytes) is set
  By the parameter L. The function resets number less than zero, if contents first
  It is less than the block second (greater weight have bytes with the smaller address), resets
  The number is more than zero, if contents of the first block are more than second and 0, if
  Contents of both memories are completely identical.}

function Q_CompMemS (P1, P2: Pointer; L: Cardinal): Integer;

{ Q_ScanInteger finds number transferred(handed) by the parameter N in the array of numbers of a type
  Integer, pointed by the parameter ArrPtr. An index of the retrieved array cell
  Is reset as result of the function (units are numbered from zero). If number
  N in the array is not retrieved, is reset -1. In the parameter Count is transmitted
  Quantity of units in the array. Functions Q_ScanLongWord and Q_ScanPointer
  Are completely similar Q_ScanInteger.}

function Q_ScanInteger (N: Integer; ArrPtr: Pointer; Count: Cardinal): Integer;
function Q_ScanLongWord (N: LongWord; ArrPtr: Pointer; Count: Cardinal): Integer;
function Q_ScanPointer (P: Pointer; ArrPtr: Pointer; Count: Cardinal): Integer;

{ Q_ScanWord finds number transferred(handed) by the parameter N in the array of numbers of a type
  Word, pointed by the parameter ArrPtr. An index of the retrieved array cell
  Is reset as result of the function (units are numbered from zero). If number
  N in the array is not retrieved, is reset -1. In the parameter Count is transmitted
  Quantity of units in the array.}

function Q_ScanWord (N: Integer; ArrPtr: Pointer; Count: Cardinal): Integer;

{ Q_ScanByte finds number transferred(handed) by the parameter N in the byte array,
  Pointed by the parameter ArrPtr. An index of the retrieved array cell
  Is reset as result of the function (index of first byte is equal to zero). If
  The number N in the array is not retrieved, is reset -1. In the parameter L is transmitted
  Size of the array in bytes.}

function Q_ScanByte (N: Integer; ArrPtr: Pointer; L: Cardinal): Integer;

{ Function Q_ScanGE_XXX scan array addressed ArrPtr, consisting from
  Count of units of an appropriate type, in searches of value, greater or
  Equal to number N. If such value is retrieved, the function resets an index
  Appropriate unit (indexing from zero). If a unit, greater or
  Equal N, in the array is absent, the function resets -1.}

function Q_ScanGE_Integer (N: Integer; ArrPtr: Pointer; Count: Cardinal): Integer;
function Q_ScanGE_LongWord (N: LongWord; ArrPtr: Pointer; Count: Cardinal): Integer;
function Q_ScanGE_Word (N: Integer; ArrPtr: Pointer; Count: Cardinal): Integer;

{ Function Q_ScanLesser_XXX scan array addressed ArrPtr, consisting from
  Count of units of an appropriate type, in searches of value, smaller N. If
  Such value is retrieved, the function resets an index of an appropriate unit
  (Indexing from zero). If the unit, smaller N, in the array is absent,
  The function resets -1.}

function Q_ScanLesser_Integer (N: Integer; ArrPtr: Pointer; Count: Cardinal): Integer;
function Q_ScanLesser_LongWord (N: LongWord; ArrPtr: Pointer; Count: Cardinal): Integer;
function Q_ScanLesser_Word (N: Integer; ArrPtr: Pointer; Count: Cardinal): Integer;

{ Q_CountInteger foots quantity of log-ons of number N in the array of numbers
  Such as Integer, pointed by the parameter ArrPtr. In the parameter Count is transmitted
  Quantity of units in the array. Functions Q_CountLongWord and Q_CountPointer
  Are completely similar Q_CountInteger and are entered only for convenience.}

function Q_CountInteger (N: Integer; ArrPtr: Pointer; Count: Cardinal): Cardinal;
function Q_CountLongWord (N: LongWord; ArrPtr: Pointer; Count: Cardinal): Cardinal;
function Q_CountPointer (P: Pointer; ArrPtr: Pointer; Count: Cardinal): Cardinal;

{ Q_CountWord foots quantity of log-ons of number N in the array of numbers
  Type Word, pointed by the parameter ArrPtr. In the parameter Count is transmitted
  Quantity of units in the array.}

function Q_CountWord (N: Integer; ArrPtr: Pointer; Count: Cardinal): Cardinal;

{ Q_CountByte foots quantity of log-ons of number N in the byte array,
  Pointed by the parameter ArrPtr. The parameter L sets a size of the array in bytes.}

function Q_CountByte (N: Integer; ArrPtr: Pointer; L: Cardinal): Cardinal;

{ Q_ReverseLongArr overturns the array of units such as LongWord so, that
  The first unit becomes the latter, second - penultimate etc. P - address
  The array, Count - quantity of units in the array.}

procedure Q_ReverseLongArr (P: Pointer; Count: Cardinal);

{ Q_ReverseWordArr overturns the array of units of a type Word so, that first
  The unit becomes the latter, second - penultimate etc. P - address of the array,
  Count - quantity of units in the array.}

procedure Q_ReverseWordArr (P: Pointer; Count: Cardinal);

{ Q_ReverseByteArr overturns the byte array so, that the first unit
  There is the latter, second - penultimate etc. P - address of the array,
  L - size of the array in bytes.}

procedure Q_ReverseByteArr (P: Pointer; L: Cardinal);

{ Q_BSwap changes the order of following of bytes in a double word D (first byte
  There is the latter, second - third and on the contrary).}

function Q_BSwap (D: LongWord): LongWord;

{ Q_BSwapLongs changes the order of following of bytes in each double word
  The array 4-�-������� of units, which address is transmitted by the parameter P. Count
  Sets quantity of units in the array.}

procedure Q_BSwapLongs (P: Pointer; Count: Cardinal);

{ Q_Exchange interchanges values of two variables. The type of variables can be
  By one of the following: String, Pointer, PChar, Integer, Cardinal, Longint,
  LongWord, Single.}

procedure Q_Exchange (var A, B);

{ Q_ExchangeWords interchanges values variable anyone 2-x-�������� of a type.
  This type can be one of the following: Word, SmallInt, WideChar.}

procedure Q_ExchangeWords (var A, B);

{ Q_ExchangeBytes interchanges values of two variables by a size in one byte.
  This type can be one of the following: Byte, ShortInt, Char.}

procedure Q_ExchangeBytes (var A, B);

{ The functions Q_RemValue_XXX delete from the array of units of an appropriate type
  All units, which value are equal N (or P for a pointer array).
  ArrPtr - address of the array, Count (or L for the array of bytes) - initial number
  Units in the array. The functions reset new number of units, which
  Remained in the array after deleting an indicated value.}

function Q_RemValue_Integer (N: Integer; ArrPtr: Pointer; Count: Cardinal): Cardinal;
function Q_RemValue_LongWord (N: LongWord; ArrPtr: Pointer; Count: Cardinal): Cardinal;
function Q_RemValue_Pointer (P: Pointer; ArrPtr: Pointer; Count: Cardinal): Cardinal;

function Q_RemValue_Word (N: Integer; ArrPtr: Pointer; Count: Cardinal): Cardinal;

function Q_RemValue_Byte (N: Integer; ArrPtr: Pointer; L: Cardinal): Cardinal;

{ The functions Q_RemDuplicates_XXX delete from the array 4-�������, 2-������� or
  1-������� of units, accordingly, all following one behind another (labour contract)
  Repeating units, i.e., if in the array is present a little number standing
  Units having the same value, from them there will be only one
  Unit with such value, and all others will be deleted. If the initial array
  Sorted, after application of the function Q_RemDuplicates_XXX, all
  The values in it(him) will be unique. If two arrays containing unique
  Value to unite in one array, then to sort it(him) and to apply to
  It(him) given function, then in obtained array will be all units (on
  To one), which are even in one of the initial arrays (operation "OR").
  ArrPtr - address of the array, Count (or L for the array of bytes) - initial number
  Units. The functions reset new number of units in the array, which
  Remained after deleting repetitions.}

function Q_RemDuplicates_Int32 (ArrPtr: Pointer; Count: Cardinal): Cardinal;
function Q_RemDuplicates_Int16 (ArrPtr: Pointer; Count: Cardinal): Cardinal;
function Q_RemDuplicates_Byte (ArrPtr: Pointer; L: Cardinal): Cardinal;

{ The functions Q_ANDSetInPlace_XXX delete from the array 4-������� or 2-�������
  Units, accordingly, all units, which are present at the array
  In the single copy (i.e. the unit will be remote, if at once ambassador
  Him(it) does not cost a unit with the same value). All repeating the labour contract
  The units are substituted of unique units with such value (i.e.,
  If in the array is present a little by number of standing units having one and
  The same value, from them remain only one unit with such value,
  And all others will be deleted). If two arrays containing unique values
  To unite in one array, then to sort it(him) and to apply to it(him) given
  Function, in result will stay only value, which are present in
  Both initial arrays (operation "And"). ArrPtr - address of the array, Count -
  The initial number of units. The functions reset new number of units, which
  Remained in the array after deleting repetitions and single values.}

function Q_ANDSetInPlace_Int32 (ArrPtr: Pointer; Count: Cardinal): Cardinal;
function Q_ANDSetInPlace_Int16 (ArrPtr: Pointer; Count: Cardinal): Cardinal;

{ The functions Q_XORSetInPlace_XXX delete from the array 4-������� or 2-�������
  Units, accordingly, all units, which are present at the array
  In several copies (one behind another, labour contract). In the array remain only
  Units which are not having "doubles". If two arrays containing unique
  Value to unite in one array, then to sort it(him) and to apply to
  To it(him) the given function, in this array the values, which will stay only
  Are present at one of the initial arrays, but not in both at once (operation
  " Eliminating OR "). ArrPtr - address of the array, Count - initial number
  Units. The functions reset new number of units, which remained
  In the array after deleting all not single values.}

function Q_XORSetInPlace_Int32 (ArrPtr: Pointer; Count: Cardinal): Cardinal;
function Q_XORSetInPlace_Int16 (ArrPtr: Pointer; Count: Cardinal): Cardinal;

{ The procedures Q_Sort_XXX sort the array 4-������� or 2-������� of units
  Appropriate type in ascending order (the algorithm fast is used
  Sortings). ArrPtr - address of the array, Count - number of units in the array.
  The value of the address of the array ArrPtr should be to a multiple size of an array cell.
  For descending sort sort the array with the help of one of the following
  Procedures, and then invert it(him) with the help Q_ReverseLong (Word) Arr.}

procedure Q_Sort_Integer (ArrPtr: Pointer; Count: Cardinal);
procedure Q_Sort_LongWord (ArrPtr: Pointer; Count: Cardinal);
procedure Q_Sort_Word (ArrPtr: Pointer; Count: Cardinal);

{ The functions Q_SearchUnique_XXX fulfil binary search in the array 4-������� or
  2-������� of units of value of an appropriate type. The array previously
  Should be sorted by increase. The function resets an index (starting
  From zero) retrieved value N in the array, which address is transmitted ArrPtr,
  Consisting from Count of units. If the value N in the array is not retrieved, function
  Resets -1. If the array contains repeating units, retrieved
  The unit will be not necessary for first among having such value.}

function Q_SearchUnique_Integer (N: Integer; ArrPtr: Pointer; Count: Cardinal): Integer;
function Q_SearchUnique_LongWord (N: LongWord; ArrPtr: Pointer; Count: Cardinal): Integer;
function Q_SearchUnique_Word (N: Integer; ArrPtr: Pointer; Count: Cardinal): Integer;

{ The functions Q_SearchFirst_XXX fulfil binary search in the array 4-������� or
  2-������� of units of value of an appropriate type. The array previously
  Should be sorted by increase. The function resets very first
  Index (since zero) retrieved value N in the array, which address
  Is transmitted ArrPtr, consisting from Count of units. If value N in the array
  Is not retrieved, the function resets -1. If in the array there are repeating units,
  That the retrieved index always will be minimum for a given data.}

function Q_SearchFirst_Integer (N: Integer; ArrPtr: Pointer; Count: Cardinal): Integer;
function Q_SearchFirst_LongWord (N: LongWord; ArrPtr: Pointer; Count: Cardinal): Integer;
function Q_SearchFirst_Word (N: Integer; ArrPtr: Pointer; Count: Cardinal): Integer;

{ The functions Q_SearchFirstGE_XXX fulfil binary search in the array 4-������� or
  2-������� of units of value of an appropriate type, greater or equal N.
  The array previously should be sorted by increase. The function
  Resets an index (since zero) first unit, which value is more
  Or is equal N, in the array, which address is transmitted ArrPtr, consisting from Count
  Units. If the value, greater or equal N, in the array is not retrieved,
  The function resets -1.}

function Q_SearchFirstGE_Integer (N: Integer; ArrPtr: Pointer; Count: Cardinal): Integer;
function Q_SearchFirstGE_LongWord (N: LongWord; ArrPtr: Pointer; Count: Cardinal): Integer;
function Q_SearchFirstGE_Word (N: Integer; ArrPtr: Pointer; Count: Cardinal): Integer;

{ The functions Q_ANDSet_XXX form of two arrays 4-������� or 2-�������
  Units of an appropriate type the new array consisting only of those
  Units, which are present at both initial arrays. P1 - address first
  The array, Count1 - number of units in the first array, P2 - address second
  The array, Count2 - number of units in the second array, OutPlace - address of area
  Memories, in which the array - result will be saved. The functions reset
  Quantity of units in the output array. If OutPlace is equal nil, function
  Do not fill the output array, but only calculate quantity of units in it(him);
  Differently, OutPlace should point a storage area, sufficient for storage
  Array - result (the maximum size is equal to a size smaller of initial
  The arrays). The units of the arrays P1 and P2 should be sorted in the order
  Increases.}

function Q_ANDSet_Integer (P1: Pointer; Count1: Cardinal;
  P2: Pointer; Count2: Cardinal; OutPlace: Pointer = nil): Cardinal;
function Q_ANDSet_LongWord (P1: Pointer; Count1: Cardinal;
  P2: Pointer; Count2: Cardinal; OutPlace: Pointer = nil): Cardinal;
function Q_ANDSet_Word (P1: Pointer; Count1: Cardinal;
  P2: Pointer; Count2: Cardinal; OutPlace: Pointer = nil): Cardinal;

{ The functions Q_ORSet_XXX form of two arrays 4-������� or 2-�������
  Units of an appropriate type the new array consisting of units,
  Which are present even at one of the initial arrays. P1 - address
  The first array, Count1 - number of units in the first array, P2 - address
  The second array, Count2 - number of units in the second array, OutPlace -
  The address of a storage area, in which will be saved array - result. Functions
  Reset quantity of units in the output array. If OutPlace is equal
  nil, the functions do not fill the output array, but only calculate quantity
  Units in it(him); differently, OutPlace should point a storage area,
  Sufficient for storage of array - result (the maximum size is equal
  To total size of the first and second arrays). Units of the arrays
  P1 And P2 should be sorted in ascending order.}

function Q_ORSet_Integer (P1: Pointer; Count1: Cardinal;
  P2: Pointer; Count2: Cardinal; OutPlace: Pointer = nil): Cardinal;
function Q_ORSet_LongWord (P1: Pointer; Count1: Cardinal;
  P2: Pointer; Count2: Cardinal; OutPlace: Pointer = nil): Cardinal;
function Q_ORSet_Word (P1: Pointer; Count1: Cardinal;
  P2: Pointer; Count2: Cardinal; OutPlace: Pointer = nil): Cardinal;

{ The functions Q_XORSet_XXX form of two arrays 4-������� or 2-�������
  Units of an appropriate type the new array consisting of units,
  Which are present at one of the initial arrays, but are absent in the friend.
  P1 - address first array, Count1 - number of units in the first array,
  P2 - address second array, Count2 - number of units in the second array,
  OutPlace - the address of a storage area, in which will be saved array - result.
  The functions reset quantity of units in the output array. If OutPlace
  Equally nil, the functions do not fill the output array, but only calculate
  Quantity of units in it(him); differently, OutPlace should point area
  Memories, sufficient for storage of array - result (maximum size
  Is equal to a total size of the first and second arrays). Units of the arrays
  P1 And P2 should be sorted by increase.}

function Q_XORSet_Integer (P1: Pointer; Count1: Cardinal;
  P2: Pointer; Count2: Cardinal; OutPlace: Pointer = nil): Cardinal;
function Q_XORSet_LongWord (P1: Pointer; Count1: Cardinal;
  P2: Pointer; Count2: Cardinal; OutPlace: Pointer = nil): Cardinal;
function Q_XORSet_Word (P1: Pointer; Count1: Cardinal;
  P2: Pointer; Count2: Cardinal; OutPlace: Pointer = nil): Cardinal;

{ The functions Q_ANDNOTSet_XXX form of two arrays 4-������� or 2-�������
  Units of an appropriate type the new array consisting only of those
  Units, which are present at the first initial array, but are absent
  In second. P1 - address first array, Count1 - number of units in first
  The array, P2 - address second array, Count2 - number of units in second
  The array, OutPlace - the address of a storage area, in which will be saved the array
  Result. The functions reset quantity of units in the output array. If
  OutPlace is equal nil, the functions do not fill the output array, but only calculate
  Quantity of units in it(him); differently, OutPlace should point area
  Memories, sufficient for storage of array - result (maximum size
  Is equal to a size of the first initial array). The units of the arrays P1 and P2 owe
  To be sorted by increase.}

function Q_ANDNOTSet_Integer (P1: Pointer; Count1: Cardinal;
  P2: Pointer; Count2: Cardinal; OutPlace: Pointer = nil): Cardinal;
function Q_ANDNOTSet_LongWord (P1: Pointer; Count1: Cardinal;
  P2: Pointer; Count2: Cardinal; OutPlace: Pointer = nil): Cardinal;
function Q_ANDNOTSet_Word (P1: Pointer; Count1: Cardinal;
  P2: Pointer; Count2: Cardinal; OutPlace: Pointer = nil): Cardinal;

{ Q_BitTest32 resets True, if in a double word D bit with number Index
  Is placed(installed) (indexing of bits from zero). Differently, the function resets False.}

function Q_BitTest32 (D: LongWord; Index: Cardinal): Boolean;

{ Q_BitSet32 installs in a double word D bit with an index Index in unit.}

function Q_BitSet32 (D: LongWord; Index: Cardinal): LongWord;

{ Q_BitReset32 dumps(resets) in a double word D bit with an index Index in a zero.}

function Q_BitReset32 (D: LongWord; Index: Cardinal): LongWord;

{ Q_BitToggle32 inverts in a double word D bit with an index Index.}

function Q_BitToggle32 (D: LongWord; Index: Cardinal): LongWord;

{ Q_CountOfSetBits32 resets quantity placed(installed) (single) bits
  In a double word D.}

function Q_CountOfSetBits32 (D: LongWord): Cardinal;

{ Q_CountOfFreeBits32 resets quantity reset (zero) bits
  In a double word D.}

function Q_CountOfFreeBits32 (D: LongWord): Cardinal;

{ Q_SetBitScanForward32 scans a double word D in searches of bit, excellent(different)
  From zero. Function resets index of first placed(installed) bit, since
  Bit FirstBit (from 0 up to 31). If bit is not retrieved, the function resets -1.}

function Q_SetBitScanForward32 (D: LongWord; FirstBit: Integer = 0): Integer;

{ Q_FreeBitScanForward32 scans a double word D in searches of offbit.
  The function resets an index of first reset bit, since bit FirstBit
  (From 0 up to 31). If offbit is not retrieved, the function resets -1.}

function Q_FreeBitScanForward32 (D: LongWord; FirstBit: Integer = 0): Integer;

{ Q_SetBitScanReverse32 scans back double word D in searches of bit,
  Nonzero. The function resets an index of last placed(installed) bit,
  Since bit LastBit (from 0 up to 31). If onbit is not retrieved, function
  Resets -1.}

function Q_SetBitScanReverse32 (D: LongWord; LastBit: Integer = 31): Integer;

{ Q_FreeBitScanReverse32 scans back double word D in searches zero
  Bit. The function resets an index of last reset bit, since bit
  LastBit (from 0 up to 31). If offbit is not retrieved, the function resets -1.}

function Q_FreeBitScanReverse32 (D: LongWord; LastBit: Integer = 31): Integer;

{ Q_BitTest checks bit and resets True, if bit is placed(installed) and False, if
  It(he) is reset. The address of bit string is transmitted by the parameter P. Offset of bit
  Rather beginnings of string is set by the parameter Index. Very first bit of string
  Has offset a zero. The job(definition) of a negative index (offset) is possible.}

function Q_BitTest (P: Pointer; Index: Integer): Boolean;

{ Q_BitSet installs bit and resets True, if before bit already was
  Is placed(installed) and False, if before bit was reset. The address of bit string
  Is transmitted by the parameter P. The offset of bit rather beginnings of string is set
  By the parameter Index. Very first bit of string has offset a zero. Probably
  The job(definition) of a negative index (offset).}

function Q_BitSet (P: Pointer; Index: Integer): Boolean;

{ Q_BitReset dumps(resets) bit and resets True, if before bit was placed(installed)
  And False, if before bit was reset. The address of bit string is transmitted
  By the parameter P. The offset of bit rather beginnings of string is set by the parameter
  Index. The indexing of bits starts with zero. The job(definition) negative is possible
  Index (offset).}

function Q_BitReset (P: Pointer; Index: Integer): Boolean;

{ Q_BitToggle inverts bit and resets True, if earlier bit was placed(installed)
  And False, if bit was reset. The address of bit string is transmitted by the parameter P.
  The offset of bit rather beginnings of string is set by the parameter Index. Indexing
  Bits starts with zero. The job(definition) of a negative index (offset) is possible.}

function Q_BitToggle (P: Pointer; Index: Integer): Boolean;

{ Q_SetBits installs each bit in the array addressed by the parameter P,
  Since bit with index FirstBit (indexing from zero) and finishing bit with
  By index LastBit. A size of the array and his(its) address should be multiple to 4 bytes.}

procedure Q_SetBits (P: Pointer; FirstBit, LastBit: Integer);

{ Q_ResetBits unsets (dumps(resets)) each bit in the array, which address
  Is transmitted parameter P, since bit with index FirstBit (indexing from
  Zero) and finishing bit with an index LastBit. A size of the array and his(its) address owe
  To be multiple to 4 bytes.}

procedure Q_ResetBits (P: Pointer; FirstBit, LastBit: Integer);

{ Q_ToggleBits inverts each bit in the array addressed by the parameter P,
  Since bit with index FirstBit (indexing from zero) and finishing bit with
  By index LastBit. A size of the array and his(its) address should be multiple to 4 bytes.}

procedure Q_ToggleBits (P: Pointer; FirstBit, LastBit: Integer);

{ Q_CountOfSetBits resets number placed(installed) (single) bits in byte
  The array addressed by the parameter P, length L byte.}

function Q_CountOfSetBits (P: Pointer; L: Cardinal): Cardinal;

{ Q_CountOfFreeBits resets number reset (zero) bits in byte
  The array addressed by the parameter P, length L byte.}

function Q_CountOfFreeBits (P: Pointer; L: Cardinal): Cardinal;

{ Q_SetBitScanForward scans a bit map addressed P, in searches of bit,
  Nonzero. The function resets an index of first placed(installed) bit,
  Since bit with an index FirstBit (from zero) and finishing an index LastBit. If
  Onbit in this range is not retrieved, the function resets -1. A size
  The array and his(its) address should be multiple to 4 bytes.}

function Q_SetBitScanForward (P: Pointer; FirstBit, LastBit: Integer): Integer;

{ Q_FreeBitScanForward scans a bit map addressed P, in searches
  Offbit. The function resets an index of first reset bit, starting
  From bit with an index FirstBit (indexing from zero) and finishing an index LastBit.
  If offbit in this range is not retrieved, the function resets -1. A size
  The array and his(its) address should be multiple to 4 bytes.}

function Q_FreeBitScanForward (P: Pointer; FirstBit, LastBit: Integer): Integer;

{ Q_SetBitScanReverse scans back bit map addressed P, in searches
  Bit, nonzero. The function resets an index last placed(installed)
  Bit, since bit with an index LastBit and finishing an index FirstBit. If
  Onbit in this range is not retrieved, the function resets -1. A size
  The array and his(its) address should be multiple to 4 bytes.}

function Q_SetBitScanReverse (P: Pointer; FirstBit, LastBit: Integer): Integer;

{ Q_FreeBitScanReverse scans back bit map addressed P, in searches
  Offbit. The function resets an index of last reset bit, starting
  From bit with an index LastBit and finishing an index FirstBit. If offbit in
  This range is not retrieved, the function resets -1. A size of the array and his(its) address
  Should be multiple to 4 bytes.}

function Q_FreeBitScanReverse (P: Pointer; FirstBit, LastBit: Integer): Integer;

{ Q_NOTByteArr inverts each bit of the array addressed by the parameter P,
  In length L byte.}

procedure Q_NOTByteArr (P: Pointer; L: Cardinal);

{ Q_XORByData fulfils the bit operation " eliminating or " above each byte
  Storage areas addressed by the parameter Source, and appropriate byte
  Storage areas addressed by the parameter Dest. The result is saved in Dest.
  The parameter L sets a size of storage areas in bytes.}

procedure Q_XORByData (Dest, Source: Pointer; L: Cardinal);

{ Q_ANDLongs fulfils logical multiplying above each appropriate bit
  Arrays addressed by parameters Dest and Source, also writes result in
  The array Dest (Dest < - Dest AND Source). In Count the number double is transmitted
  Words in each of the arrays.}

procedure Q_ANDLongs (Dest, Source: Pointer; Count: Cardinal);

{ Q_ORLongs fulfils logical addition above each appropriate bit
  Arrays addressed by parameters Dest and Source, also writes result in
  The array Dest (Dest < - Dest OR Source). In Count the number double is transmitted
  Words in each of the arrays.}

procedure Q_ORLongs (Dest, Source: Pointer; Count: Cardinal);

{ Q_XORLongs fulfils the operation " eliminating or " between appropriate
  In bits of the arrays addressed by parameters Dest and Source, also writes result
  In the array Dest (Dest < - Dest XOR Source). In Count the number double is transmitted
  Words in each of the arrays.}

procedure Q_XORLongs (Dest, Source: Pointer; Count: Cardinal);

{ Q_NOTLongArr inverts each bit of the array addressed by the parameter P,
  In length Count of double words.}

procedure Q_NOTLongArr (P: Pointer; Count: Cardinal);

{ Q_ANDNOTLongs fulfils logical multiplying of each bit of the array,
  Addressed by the parameter Dest on appropriate inverted bit of the array,
  Addressed Source, also writes result in the array Dest
  (Dest < - Dest AND NOT Source). In Count the number of double words is transmitted
  In each of the arrays. As a result of execution of this operation in the array
  Dest all bits, for which appropriate bits in the array Source are unset
  Are placed(installed) in unit.}

procedure Q_ANDNOTLongs (Dest, Source: Pointer; Count: Cardinal);

{ Q_LShift1Longs shifts the array addressed by the parameter P, length Count double
  Words on 1 bit to the left (multiplies it(him) on 2). The low bit of the array is unset.}

procedure Q_LShift1Longs (P: Pointer; Count: Cardinal);

{ Q_RShift1Longs shifts the array addressed by the parameter P, length Count double
  Words into 1 bit to the right (divides it(him) on 2). Most significant bit of the array is unset.}

procedure Q_RShift1Longs (P: Pointer; Count: Cardinal);

{ Q_ReverseBits pays a bit map addressed by the parameter P, so, that
  The first bits become last, and last - first. Length of the array
  (In bits) is transmitted by the parameter BitCount. If this length is not multiple to eight
  To bits, the bits, remaining up to a multiplicity, are filled in zero&.}

procedure Q_ReverseBits (P: Pointer; BitCount: Cardinal);

{ Q_Lrot32 cyclic shifts a double word D on Shift bit to the left.}

function Q_Lrot32 (D: LongWord; Shift: Byte): LongWord;

{ Q_Rrot32 cyclic shifts a double word D on Shift bit to the right.}

function Q_Rrot32 (D: LongWord; Shift: Byte): LongWord;

{ Q_Lrot16 cyclic shifts a machine word W on Shift bit to the left.}

function Q_Lrot16 (W: Word; Shift: Byte): Word;

{ Q_Rrot16 cyclic shifts a machine word W on Shift bit to the right.}

function Q_Rrot16 (W: Word; Shift: Byte): Word;

{ Q_Lrot8 cyclic shifts byte B on Shift bit to the left.}

function Q_Lrot8 (B, Shift: Byte): Byte;

{ Q_Rrot8 cyclic shifts byte B on Shift bit to the right.}

function Q_Rrot8 (B, Shift: Byte): Byte;

{ Q_RotateLongsLeft cyclic shifts each array cell consisting from
  Count of units such as LongWord, addressed by the parameter P, on Shift bit to the left.}

procedure Q_RotateLongsLeft (P: Pointer; Count: Cardinal; Shift: Byte);

{ Q_RotateLongsRight cyclic shifts each array cell consisting from
  Count of units such as LongWord, addressed by the parameter P, on Shift bit to the right.}

procedure Q_RotateLongsRight (P: Pointer; Count: Cardinal; Shift: Byte);

{ Q_RotateWordsLeft cyclic shifts each array cell consisting from
  Count of units of a type Word, addressed by the parameter P, on Shift bit to the left.}

procedure Q_RotateWordsLeft (P: Pointer; Count: Cardinal; Shift: Byte);

{ Q_RotateWordsRight cyclic shifts each array cell consisting from
  Count of units of a type Word, addressed by the parameter P, on Shift bit to the right.}

procedure Q_RotateWordsRight (P: Pointer; Count: Cardinal; Shift: Byte);

{ Q_RotateBytesLeft cyclic shifts each byte in the memory by a size L
  Byte addressed by the parameter P, on Shift bit to the left.}

procedure Q_RotateBytesLeft (P: Pointer; L: Cardinal; Shift: Byte);

{ Q_RotateBytesRight cyclic shifts each byte in the memory by a size L
  Byte addressed by the parameter P, on Shift bit to the right.}

procedure Q_RotateBytesRight (P: Pointer; L: Cardinal; Shift: Byte);

{ Q_RotateBitsLeft cyclic shifts bit string of length L byte, address
  To which is transmitted by the parameter P, on Shift bit to the left. The parameter Shift can
  To receive both positive, and negative values. Bit string,
  As it is accepted, is written, since most significant bits.}

procedure Q_RotateBitsLeft (P: Pointer; L: Cardinal; Shift: Integer);

{ Q_RotateBitsRight cyclic shifts bit string of length L byte, address
  To which is transmitted by the parameter P, on Shift bit to the right. The parameter Shift can
  To receive both positive, and negative values. Bit string
  Is written, since most significant bits.}

procedure Q_RotateBitsRight (P: Pointer; L: Cardinal; Shift: Integer);


{ Cryptography functions.}

{ Before to use any functions from this units, familiarize
  With the legislation of your country in that part, which concerns usage
  Algorithms of proof encoding!!! If it is not authorized by your laws,
  Delete all functions from this units from your copy of the unit QStrings.}

{ Q_XORByChar fulfils the bit operation "exclusive or" above each byte
  Storage areas of length L byte addressed by the parameter P, and character (byte)
  Ch. The repeated application of this operation with the same character Ch restores
  Reset state of the byte array.}

procedure Q_XORByChar (P: Pointer; L: Cardinal; Ch: Char); overload;
procedure Q_XORByChar (P: Pointer; L: Cardinal; Ch: Byte); overload;

{ Q_XORByLong is similar Q_XORByChar, but works not with bytes, and with double
  By words (LongWord). Count sets quantity �������������� of double words.}

procedure Q_XORByLong (P: Pointer; Count: Cardinal; D: LongWord);

{ Q_XORByWord is similar Q_XORByChar, but works not with bytes, and with machine
  By words (Word). Count sets quantity ����������� of words.}

procedure Q_XORByWord (P: Pointer; Count: Cardinal; W: Word);

{ Q_XORByStr fulfils the bit operation " eliminating or " above each byte
  Storage areas of length L byte addressed by the parameter P, and each character
  Strings Key. If the size of a storage area exceeds length of string Key, the ambassador
  Sampling of the last character from this string she(it) starts to be viewed with
  Beginnings. Repeated application of this operation to a storage area with the same key
  Restores a reset state of a storage area.}

procedure Q_XORByStr (P: Pointer; L: Cardinal; const Key: string);

{ Q_XORByRandom fulfils ������������ (with the help XOR) byte array of length
  L byte addressed by the parameter P, sensor of pseudo-random numbers similar
  To standard (Random). Initial value of a pseudo-random sequence
  Is defined(determined) by a constant Seed. Repeated application of this operation to same
  The storage areas with same Seed are restored by(with) a reset state of the data.}

procedure Q_XORByRandom (P: Pointer; L: Cardinal; Seed: LongWord);

{ The functions Q_RC4XXX realize line algorithm of encoding RC4, developed
  ����� �������� (all rights belong RSA Data Security, Inc.), which was
  Is anonymously published in computer conference sci.crypt in 1995. Algorithm
  Very fast and reliable enough. Repeated application it(him) with same
  By key restores a reset state of the data. It is not necessary to use
  The same key some times. If it is necessary to you fast to cipher
  Any data, generate a pseudo-random session key K1 (for example,
  With the help Q_SecureRandFill) size from 16 up to 256 bytes, cipher it(him)
  Method CAST6 or RC6 in mode CBC with some key K2 also place in
  Output stream of the data, then with the help of a session key K1 cipher
  The data by a method RC4. The key K2 for encoding a session key can vary
  Not too frequently. At data reading you first of all read out ciphered
  Session key also decrypt it(him) with the help of a key, known (you,) K2
  (By procedures Q_CAST6XXX or Q_RC6XXX), receive a session key K1, read
  Data, decrypt them with a key K1 (RC4 method).}

type
  TRC4ID = type Pointer;

{ Q_RC4Init sets a key sequence for data encoding by a method
  RC4 Also initializes the process of encoding. As a key is received
  Contents of the byte array addressed by the parameter Key, length L byte.
  The key length can be up to 256 characters. In the parameter ID is reset
  Identifier of the given process of encoding, which then is transmitted in
  Procedures Q_RC4Apply and Q_RC4Done. Each identifier owes in the end
  To be released by call Q_RC4Done. Since the version QStrings 5.10 methods
  RC4 Is realized as it is necessary (key is viewed from the first bytes).}

procedure Q_RC4Init (var ID: TRC4ID; Key: Pointer; KeyLen: Cardinal);

{ Q_RC4Apply fulfils actual encoding or decryption of the data addressed
  By the parameter P, length L byte by a key coupled to the identifier ID. This
  The procedure can be called(caused) multiply for sequential encoding
  Several data arrays. Thus Q_RC4Init and Q_RC4Done are called(caused) only
  Once, in the beginning and at the end of all process.}

procedure Q_RC4Apply (ID: TRC4ID; P: Pointer; L: Cardinal);

{ Q_RC4Done releases internal operational lifes coupled to the identifier ID.
  After call Q_RC4Done this identifier can not be used any more
  By the procedure Q_RC4Apply.}

procedure Q_RC4Done (ID: TRC4ID);

{ Q_RC4SelfTest checks service capability of algorithm RC4 on test vectors,
  Given in the function RC4SelfTest from the unit RC4.pas (writer of this unit
  Is David Barton (davebarton@bigfoot.com)). If the test ������� is successful,
  The function resets True, differently False.}

function Q_RC4SelfTest: Boolean;

{ The procedures Q_RC6XXX realize block algorithm of encoding RC6 (TM). The writers
  Algorithm: Ronald L. Rivest, M.J.B. Robshaw, R. Sidney and Y.L. Yin (algorithm
  Is patented). Unique feature of this cipher is his(its) high
  Stability(resistance) at excellent(different) productivity (one of five algorithms, past
  In the second round AES). A key length - up to 255 bytes, however to use keys
  More longly than 48 bytes it is not recommended (normal key length - 32 bytes). Here
  The modes CBC, CFB and OFB are realized. }

type
  TRC6InitVector = array [0.. 15] of Byte;
  TRC6ID = type Pointer;

{ Q_RC6Init initializes encoding by a method RC6. The key is transmitted by the parameter
  Key (address of the byte array), and key length (in bytes) - parameter KeyLen.
  The identifier of the current process of encoding is reset in the parameter ID.
  The given identifier is transmitted as the first parameter in all other procedures
  Q_RC6XXX, and in the end it(he) should be released by call Q_RC6Done.}

procedure Q_RC6Init (var ID: TRC6ID; Key: Pointer; KeyLen: Cardinal);

{ The procedures of encoding / decryption in the mode ECB should be used only
  In the debug purposes. A size of a data package - 128 bits.}

procedure IntRC6EncryptECB (ID: TRC6ID; P: Pointer);
procedure IntRC6DecryptECB (ID: TRC6ID; P: Pointer);

{ Q_RC6SetOrdinaryVector unsets an initial vector used in modes
  CBC, CFB and OFB, and then ciphers by his(its) current key. Thus passes
  Necessity in �������������. ID - identifier of the process of encoding,
  Obtained from Q_RC6Init. This procedure should be called(caused) before everyone
  By session of encoding (decryption). The idea of the procedure is borrowed from the unit
  RC6.pas In a composition DCPcrypt v2, which writer is David Barton
  (davebarton@bigfoot.com). }

procedure Q_RC6SetOrdinaryVector (ID: TRC6ID);

{ Q_RC6SetInitVector installs an initial vector used in modes
  CBC, CFB and OFB, equal to a vector transferred(handed) by the parameter IV. ID - the identifier
  The process of encoding obtained from Q_RC6Init. This procedure should be called(caused)
  Before each session of encoding (or decryption).}

procedure Q_RC6SetInitVector (ID: TRC6ID; const IV: TRC6InitVector);

{ Q_RC6EncryptCBC fulfils encoding the byte array addressed
  By the parameter P, length L byte by a method RC6 in the mode CBC. ID - identifier
  The process of encoding obtained from Q_RC6Init. If length of the byte array
  Is not multiple to 128 bits (16 characters), the tail is ciphered in the mode CFB.}

procedure Q_RC6EncryptCBC (ID: TRC6ID; P: Pointer; L: Cardinal);

{ Q_RC6DecryptCBC fulfils decryption of the byte array addressed
  By the parameter P, length L byte ciphered by the procedure Q_RC6EncryptCBC.
  ID - identifier of the process of encoding obtained from Q_RC6Init.}

procedure Q_RC6DecryptCBC (ID: TRC6ID; P: Pointer; L: Cardinal);

{ Q_RC6EncryptCFB128 fulfils encoding the byte array addressed
  By the parameter P, length L byte by a method RC6 in a block mode CFB (128-bit).
  ID - identifier of the process of encoding obtained from Q_RC6Init.}

procedure Q_RC6EncryptCFB128 (ID: TRC6ID; P: Pointer; L: Cardinal);

{ Q_RC6DecryptCFB128 fulfils decryption of the byte array addressed
  By the parameter P, length L byte ciphered by the procedure Q_RC6EncryptCFB128.
  ID - identifier of the process of encoding obtained from Q_RC6Init.}

procedure Q_RC6DecryptCFB128 (ID: TRC6ID; P: Pointer; L: Cardinal);

{ Q_RC6EncryptCFB fulfils encoding the byte array addressed
  By the parameter P, length L byte by a method RC6 in the mode CFB (8-bit). ID -
  The identifier of the process of encoding obtained from Q_RC6Init.}

procedure Q_RC6EncryptCFB (ID: TRC6ID; P: Pointer; L: Cardinal);

{ Q_RC6DecryptCFB fulfils decryption of the byte array addressed
  By the parameter P, length L byte ciphered by the procedure Q_RC6EncryptCFB.
  ID - identifier of the process of encoding obtained from Q_RC6Init.}

procedure Q_RC6DecryptCFB (ID: TRC6ID; P: Pointer; L: Cardinal);

{ Q_RC6ApplyOFB128 fulfils encoding or decryption of a storage area,
  Addressed by the parameter P, length L byte by a method RC6 in a block mode OFB
  (128 bits). The parameter ID sets the identifier of the process of encoding. Follows
  To mean, that the mode OFB does not provide distribution of errors and,
  Actually, is an equivalent of the line cipher. In this mode does not follow
  To use the same key some times. The block mode OFB follows
  To apply with extra care, as, if for someone becomes known
  Fragment of a plain text, it(he) easily can decrypt all subsequent
  The text. If the size of a storage area is not multiple to 16 bytes, a fragment of a gamma,
  Come on bytes, missing up to a multiplicity, is skipped.}

procedure Q_RC6ApplyOFB128 (ID: TRC6ID; P: Pointer; L: Cardinal);

{ Q_RC6ApplyOFB fulfils encoding or decryption of a storage area addressed
  By the parameter P, length L byte by a method RC6 in the mode OFB (8 bits). The parameter ID
  Sets the identifier of the process of encoding.}

procedure Q_RC6ApplyOFB (ID: TRC6ID; P: Pointer; L: Cardinal);

{ Q_RC6Done releases identifier of the process of encoding ID, obtained from
  Procedures Q_RC6Init and all operational lifes, coupled to it(him).}

procedure Q_RC6Done (ID: TRC6ID);

{ Q_RC6SelfTest checks service capability of algorithm RC6 on test vectors,
  Given in the document: The RC6 (TM) Block Cipher. Ronald L. Rivest, M.J.B.
  Robshaw, R. Sidney, and Y.L. Yin, http://theory.lcs.mit.edu/~rivest/rc6.pdf.
  If the test passed successfully, the function returns True, otherwise it
  returns False.}

function Q_RC6SelfTest: Boolean;

{ The procedures Q_CAST6XXX realize block algorithm of encoding CAST6-256. It(him)
  The writers are Carlisle Adams and Jeff Gilchrist (Canada). This algorithm
  Is extremely reliable, but works in 2.5 times more slowly, than RC6.
  Key length - up to 256 bits (32 bytes). The modes CBC, CFB and OFB are maintained.
  This implementation of algorithm completely corresponds(meets) RFC 2612.}

type
  TCAST6InitVector = array [0.. 15] of Byte;
  TCASTID = type Pointer;

{ Q_CAST6Init initializes encoding by a method CAST6. The key is transmitted a couple
  In meter Key (address of the byte array), and the key length (in bytes) is set
  By the parameter KeyLen. The identifier of the current process of encoding is reset
  In the parameter ID. The given identifier is transmitted as the first parameter in all
  Other procedures Q_CAST6XXX, and in the end it(he) should be released by call
  Q_CAST6Done.}

procedure Q_CAST6Init (var ID: TCASTID; Key: Pointer; KeyLen: Cardinal);

{ The procedures of encoding / decryption in the mode ECB should be used only
  In the debug purposes. A size of a data package - 128 bits.}

procedure IntCAST6EncryptECB (ID: TCASTID; P: Pointer);
procedure IntCAST6DecryptECB (ID: TCASTID; P: Pointer);

{ Q_CAST6SetOrdinaryVector unsets initial vector, which is required in
  Modes CBC, CFB and OFB, and then ciphers by his(its) current key. Thus passes
  Necessity in �������������. ID - identifier of the process of encoding,
  Obtained from Q_CAST6Init. This procedure should be called(caused) before each session
  Encodings (or decryption). The idea of the procedure is borrowed from the unit
  Cast256.pas In a composition DCPcrypt v2, which writer is David Barton
  (davebarton@bigfoot.com). }

procedure Q_CAST6SetOrdinaryVector (ID: TCASTID);

{ Q_CAST6SetInitVector installs an initial vector used in modes
  CBC, CFB and OFB, equal to a vector IV. ID - identifier of the process of encoding,
  Obtained from Q_CAST6Init. This procedure should be called(caused) before everyone
  By session of encoding (or decryption).}

procedure Q_CAST6SetInitVector (ID: TCASTID; const IV: TCAST6InitVector);

{ Q_CAST6EncryptCBC fulfils encoding the byte array addressed
  By the parameter P, length L byte by a method CAST6 in the mode CBC. ID - identifier
  The process of encoding obtained from Q_CAST6Init. If length of the byte array
  Is not multiple to 128 bits (16 characters), the tail is ciphered in the mode CFB.}

procedure Q_CAST6EncryptCBC (ID: TCASTID; P: Pointer; L: Cardinal);

{ Q_CAST6DecryptCBC fulfils decryption of the byte array addressed
  By the parameter P, length L byte ciphered by the procedure Q_CAST6EncryptCBC.
  ID - identifier of the process of encoding obtained from Q_CAST6Init.}

procedure Q_CAST6DecryptCBC (ID: TCASTID; P: Pointer; L: Cardinal);

{ Q_CAST6EncryptCFB128 fulfils encoding the byte array addressed
  By the parameter P, length L byte by a method CAST6 in a block mode CFB (128-bit).
  ID - identifier of the process of encoding obtained from Q_CAST6Init.}

procedure Q_CAST6EncryptCFB128 (ID: TCASTID; P: Pointer; L: Cardinal);

{ Q_CAST6DecryptCFB128 fulfils decryption of the byte array addressed
  By the parameter P, length L byte ciphered by the procedure Q_CAST6EncryptCFB128.
  ID - identifier of the process of encoding obtained from Q_CAST6Init.}

procedure Q_CAST6DecryptCFB128 (ID: TCASTID; P: Pointer; L: Cardinal);

{ Q_CAST6EncryptCFB fulfils encoding the byte array addressed
  By the parameter P, length L byte by a method CAST6 in the mode CFB (8-bit). ID -
  The identifier of the process of encoding obtained from Q_CAST6Init.}

procedure Q_CAST6EncryptCFB (ID: TCASTID; P: Pointer; L: Cardinal);

{ Q_CAST6DecryptCFB fulfils decryption of the byte array addressed
  By the parameter P, length L byte ciphered by the procedure Q_CAST6EncryptCFB.
  ID - identifier of the process of encoding obtained from Q_CAST6Init.}

procedure Q_CAST6DecryptCFB (ID: TCASTID; P: Pointer; L: Cardinal);

{ Q_CAST6ApplyOFB128 fulfils encoding or decryption of a storage area,
  Addressed by the parameter P, length L byte by a method CAST6 in a block mode OFB
  (128 bits). The parameter ID sets the identifier of the process of encoding. Follows
  To mean, that the mode OFB does not provide distribution of errors and,
  Actually, is an equivalent of the line cipher. In this mode does not follow
  To use the same key some times. The block mode OFB follows
  To apply with extra care, as, if for someone becomes known
  Fragment of a plain text, it(he) easily can decrypt all subsequent
  The text. If the size of a storage area is not multiple to 16 bytes, a fragment of a gamma,
  Come on bytes, missing up to a multiplicity, is skipped.}

procedure Q_CAST6ApplyOFB128 (ID: TCASTID; P: Pointer; L: Cardinal);

{ Q_CAST6ApplyOFB fulfils encoding or decryption of a storage area, which
  Is addressed by the parameter P, length L byte by a method CAST6 in the mode OFB (8 bits).
  ID - identifier of the process of encoding.}

procedure Q_CAST6ApplyOFB (ID: TCASTID; P: Pointer; L: Cardinal);

{ Q_CAST6Done releases identifier of the process of encoding ID, obtained from
  Procedures Q_CAST6Init and all operational lifes, coupled to it(him).}

procedure Q_CAST6Done (ID: TCASTID);

{ Q_CAST6SelfTest checks service capability of algorithm CAST6 on test
  To vectors given in RFC 2612. If the test ������� is successful, function
  Resets True, differently False.}

function Q_CAST6SelfTest: Boolean;

{ The functions Q_SHA1XXX are intended for obtaining value of hash-function SHA-1
  (���������) for the byte array or string of characters. This algorithm (Secure
  Hash Standard-1) is developed NIST (National Institute of Standards and
  Technology) also is published in FIPS180-1 (Federal Information Processing
  Standards Publication 180-1). The functions Q_SHA1XXX generate the 160-bit array,
  Consisting from five double words (or 20 bytes). On ��������� practically
  It is impossible to restore the initial message. It is impossible also to find another
  The message with same digest. The given implementation of algorithm SHA-1 is fulfilled
  On the basis of the unit SHA1.pas, which writer is David Barton.}

type
  TSHA1Digest = array [0.. 4] of LongWord;
  TSHAID = type Pointer;

  TMixDigest = array [0.. 9] of LongWord; // see Q_MixHashXXX (below)
  TMixID = type Pointer;

{ Q_SHA1Init initializes hash-function SHA-1. In the parameter ID is reset
  The new identifier ���������. In further this identifier is transmitted in
  Procedures Q_SHA1Update and Q_SHA1Final. Last of these procedures it is necessary
  Should be called(caused) in the end for release of the occupied operational lifes.}

procedure Q_SHA1Init (var ID: TSHAID);

{ Q_SHA1Update adds to ���������, which identifier is transferred(handed) by the parameter
  ID, information on the byte array of length L byte addressed by the parameter P. This
  The procedure can is called(caused) multiply for several data arrays, however
  Value ��������� will be obtained only at closing the identifier
  By the procedure Q_SHA1Final.}

procedure Q_SHA1Update (ID: TSHAID; P: Pointer; L: Cardinal);

{ Q_SHA1Final resets in parameter Digest value of hash-function SHA-1 for
  The indicated identifier ID. Thus the operational lifes coupled with are released
  By this identifier. After call Q_SHA1Final to use given ID
  Already it is impossible.}

procedure Q_SHA1Final (ID: TSHAID; var Digest: TSHA1Digest);

{ Q_SHA1 resets in the parameter Digest value of hash-function SHA-1 for string
  Characters S, storage area of length L byte addressed by the parameter P, or
  Some reference value SourceDigest 160-bit or 320-bit
  Hash-functions (SHA-1 or MixHash).}

procedure Q_SHA1 (const S: string; var Digest: TSHA1Digest); overload;
procedure Q_SHA1 (P: Pointer; L: Cardinal; var Digest: TSHA1Digest); overload;

procedure Q_SHA1 (const SourceDigest: TSHA1Digest; var Digest: TSHA1Digest); overload;
procedure Q_SHA1 (const SourceDigest: TMixDigest; var Digest: TSHA1Digest); overload;

{ Q_SHA1SelfTest checks implementation of algorithm SHA-1 on test vectors,
  Published NIST. If the test ������� is successful, the function resets True,
  Differently False.}

function Q_SHA1SelfTest: Boolean;

{ The functions Q_MixHashXXX realize algorithm of hash-function with twice greater, than
  For SHA-1 in length of a signature (320 bits): the initial string (or byte array)
  Is divided into three approximately equal parts, for generation of a signature ������-
  �������� three 160-bit values of the one-sided function SHA-1: the first value
  The second value - on second and is calculated by the first and second parts of string
  To the third parts, and third - by the third and first parts. First two values
  Hash-functions SHA-1 are consolidated with each other by concatenation, derivating 320-
  Bit string. Third value is used as key of encoding for
  Modifications of the obtained bit string. This modification is fulfilled with the help
  Four-feed-through encoding with cyclic distribution of errors on a basis
  Algorithm CAST6-160. The four-feed-through encoding is necessary to reach(achieve)
  Complete distribution of errors both in one, and in other side. Thus
  Each bit participates in eight cycles of encoding. At the end of the process bit
  String covered by a pseudo-random sequence obtained on a basis
  First two values of the function SHA-1, also increases (on double words) to
  To reference value of hash-function. A disadvantage of given hash-function is
  That she(it) works approximately at 2.25 of time more slowly, than SHA-1.}

{ Q_MixHashInit initializes hash-function. In the parameter ID is reset
  The new identifier ���������. In further this identifier is transmitted
  In procedures Q_MixHashUpdate and Q_MixHashFinal. Last of these procedures
  Necessarily should be called(caused) in the end for release of the occupied operational lifes.}

procedure Q_MixHashInit (var ID: TMixID);

{ Q_MixHashUpdate adds to ���������, which identifier is transferred(handed)
  By the parameter ID, information on the byte array of length L byte addressed
  By the parameter P. This procedure can is called(caused) multiply for several
  Data arrays, however digest will be obtained only at closing the identifier
  by the procedure Q_MixHashFinal.}

procedure Q_MixHashUpdate (ID: TMixID; P: Pointer; L: Cardinal);

{ Q_MixHashFinal resets in parameter Digest value of hash-function for
  The indicated identifier ID. Thus the operational lifes coupled with are released
  By this identifier. After call Q_MixHashFinal to use given ID
  Already it is impossible.}

procedure Q_MixHashFinal (ID: TMixID; var Digest: TMixDigest);

{ Q_MixHash resets in the parameter Digest value of hash-function for string
  Characters S, storage area of length L byte addressed by the parameter P, or
  Some reference value SourceDigest of given hash-function.}

procedure Q_MixHash (const S: string; var Digest: TMixDigest); overload;
procedure Q_MixHash (P: Pointer; L: Cardinal; var Digest: TMixDigest); overload;
procedure Q_MixHash (const SourceDigest: TMixDigest; var Digest: TMixDigest); overload;

{ Q_MixHashSelfTest checks implementation of algorithm on test vectors,
  If the test ������� is successful, the function resets True, differently False.}

function Q_MixHashSelfTest: Boolean;

{ The procedures Q_RandXXX realize the sensor of pseudo-random numbers Mersenne Twister
  (unsigned integers in an interval from 0 up to 2^32 - 1) with the uniform law
  Allocations. His(its) writers: Makoto Matsumoto (matumoto@math.keio.ac.jp) and
  Takuji Nishimura. This sensor is developed on the basis of several existing
  Generators ��� also has incorporated their best qualities. His(its) period constitutes
  2^19937 - 1. In itself sensor is not intended for the cryptography purposes,
  Since on sampling the large size suffices it is possible to restore initial
  Sequence. However, in the represented here implementation to obtained
  To values of the sensor can be applied one-sided (not inverted) hash-
  The function SHA-1. In this case, if the sensor was correctly initialized
  (Procedures Q_RandInit and Q_RandXXXUpdate), his(its) output sequence
  Will be ���������������� a rack. More complete information on this sensor ���
  It is possible to receive on a site: http://www.math.keio.ac.jp/~matumoto/emt.html }

type
  TRandVector = array [0.. 623] of LongWord;
  TMTID = type Pointer;

{ Q_RandInit initializes the sensor ���. The identifier of the sensor is reset
  In the parameter ID. It(he) should be transmitted as the first parameter by call others
  Functions Q_RandXXX. For initialization is used ����������� an integer
  Seed, unequal zero, or initial vector InitVector. The identifier owes
  To be released at the end of operation with the sensor with the help of call Q_RandDone.}

procedure Q_RandInit (var ID: TMTID; Seed: LongWord = 4357); overload;
procedure Q_RandInit (var ID: TMTID; const InitVector: TRandVector); overload;

{ The procedures MT_RandXXXUpdate change an internal state (vector) sensor
  Pseudo-random numbers. The current value of a vector is ciphered with the help one
  From algorithms: CAST-256 or RC6. The second parameter of procedures Q_RandXXXUpdate
  Is used as a key of encoding. It(he) can be string of characters S, byte
  By the array addressed P, length L byte, or digital signature (SHA-1 or
  MixHash), transmitted the parameter Digest. If length of string S, or array,
  Addressed parameter P, is more 32 bytes (in case CAST6) or 48 bytes (in
  Case RC6), the encoding is fulfilled some times, using fragments of a key
  In length up to 32 bytes or up to 48 bytes, accordingly. At creation of string S
  Use value of the counter of clock ticks of the processor (Q_TimeStamp), current date
  And time, identifiers of the user and system, arbitrary string of characters,
  Entered by the user or manager from the keyboard etc. In finite
  The score, the stability(resistance) of the sensor is defined(determined) by , as far as
  unpredictable and the initializing byte sequence transmitted will be long
  In Q_RandXXXUpdate. These procedures can be called(caused) in various combinations.}

procedure Q_RandCAST6Update (ID: TMTID; const S: string); overload;
procedure Q_RandCAST6Update (ID: TMTID; P: Pointer; L: Cardinal); overload;
procedure Q_RandCAST6Update (ID: TMTID; const Digest: TSHA1Digest); overload;

procedure Q_RandRC6Update (ID: TMTID; const S: string); overload;
procedure Q_RandRC6Update (ID: TMTID; P: Pointer; L: Cardinal); overload;
procedure Q_RandRC6Update (ID: TMTID; const Digest: TSHA1Digest); overload;
procedure Q_RandRC6Update (ID: TMTID; const Digest: TMixDigest); overload;

{ Q_RandGetVector copies current contents of a vector used for
  Generation of pseudo-random numbers, in the array transferred(handed) by the parameter Vector.
  Thus, in the further generator it is possible to initialize by this vector
  And to play back sequence of pseudo-random numbers (since
  The following number). In the parameter ID the identifier of the sensor is transmitted.}

procedure Q_RandGetVector (ID: TMTID; var Vector: TRandVector);

{ Q_RandSetVector sets new value of a vector used for generation
  Pseudo-random numbers. Allows to play back a sequence, starting
  From a vector transferred(handed) by the parameter Vector. ID - the identifier of the sensor.}

procedure Q_RandSetVector (ID: TMTID; const Vector: TRandVector);

{ MT_RandNext resets the following pseudo-random number, which is
  Evenly distributed in an interval from 0 up to $FFFFFFFF, for the sensor,
  Which identifier is transmitted by the parameter ID. Values of this function
  Are not cryptographically strong.}

function Q_RandNext (ID: TMTID): LongWord;

{ Q_RandUniform resets the pseudo-random value evenly distributed
  In an interval [0,1], for the sensor, which identifier is transferred(handed) by the parameter ID.
  The continuous pseudo-random value with uniform allocation in an interval
  [Min,Max] it turns out under the formula: (Max-Min)*Q_RandUniform(ID)+Min. }

function Q_RandUniform (ID: TMTID): Extended;

{ Q_RandUInt32 resets whole pseudo-random number (0 < = X < Range) for
  The sensor, which identifier is transferred(handed) by the parameter ID. The function Q_RandUInt64
  Is similar Q_RandUInt32, but the valid range is set by 64-bit number.
  Q_RandUInt64 Works much more slowly, than Q_RandUInt32.}

function Q_RandUInt32 (ID: TMTID; Range: Cardinal): Cardinal;
function Q_RandUInt64 (ID: TMTID; Range: Int64): Int64;

{ Q_RandGauss is used for simulation of a continuous random variable
  With the normal law of allocation. This function resets (0,1) normal&
  The distributed pseudo-random number for the sensor, which identifier
  Is transmitted by the parameter ID. In the parameter ExtraNumber the address can be transferred(handed)
  The variable such as Double, in which will write other number with similar
  By allocation. It is possible, as used algorithm (Marsaglia-Bray)
  Generates at once pair of numbers with normal allocation. That
  To receive the value with expectation Mean and ��������������������
  By deviation StdDev, use the formula: Q_RandGauss(ID)*StdDev+Mean.}

function Q_RandGauss (ID: TMTID; ExtraNumber: Pointer = nil): Extended;

{ MT_SecureRandNext resets the following pseudo-random number, which
  Is evenly distributed in interval from 0 up to $FFFFFFFF, for
  The sensor, which identifier is transmitted by the parameter ID. This value
  It turns out as a result of application of the one-sided function (SHA-1) to output
  Sequences of the sensor Mersenne Twister. Thus everyone 5 values
  The functions MT_SecureRandNext are formed on the basis of 14 values of the function
  Q_RandNext. At usage of this sensor in a subsystem of safety
  The special notice is necessary to give initialization of the sensor.}

function Q_SecureRandNext (ID: TMTID): LongWord;

{ Q_RandFill fills a storage area addressed by the parameter P, length L byte
  By pseudo-random numbers with the uniform law of allocation. ID -
  The identifier of the sensor obtained by call Q_RandInit. Received by such
  By image the sequence is not ���������������� a rack.}

procedure Q_RandFill (ID: TMTID; P: Pointer; L: Cardinal);

{ MT_SecureRandFill fills a storage area addressed by the parameter P in length
  L byte ���������������� by a rack (at correct initialization of the sensor)
  By sequence of pseudo-random numbers on the basis of the sensor, identifier
  Which is transmitted by the parameter ID.}

procedure Q_SecureRandFill (ID: TMTID; P: Pointer; L: Cardinal);

{ Q_SecureRandXOR fulfils overlay with the help " eliminating OR " by a rack
  Pseudo-random sequence on a storage area addressed by the parameter
  P, length L byte. The identifier of the sensor is transmitted by the parameter ID. Repeated
  The overlay of the same sequence restores a reset state
  Storage areas. If this operation is applied to several fragments of memory,
  That at data recovery sizes of these fragments should coincide with
  Initial, or length of all fragments should be multiple to four bytes.
  It is coupled that, if Q_SecureRandXOR is applied to a storage area,
  Which size is not multiple to four bytes, a fragment of a gammas come
  On bytes, missing up to a multiplicity, is lost. The procedure Q_SecureRandXOR,
  Actually, realizes self algorithm of stream encryption, which at
  Appropriate initialization of the generator can considerably exceed
  On stability(resistance) such methods as RC6 or CAST6. However, for this purpose is necessary
  Source of a long pseudo-random sequence (for example, another
  It is impossible to admit the similar generator) and, besides, that any
  The fragment of a gamma was repeatedly superimposed on other data.}

procedure Q_SecureRandXOR (ID: TMTID; P: Pointer; L: Cardinal);

{ Q_RandDone releases operational lifes coupled c by the identifier of the sensor
  Pseudo-random numbers ID. After call of this procedure given ID it is more
  Can not be used.}

procedure Q_RandDone (ID: TMTID);

{ The procedures Q_DHXXX realize algorithm of the Diffie-Hellman, which is used
  In encoding with an open key. These procedures do not fulfil directly
  Encodings of the information. They are intended for generation of a key, which can
  Then to be used for encoding by usual methods. This key is formed
  With the help of a couple of keys: closed and open. The closed key, as a rule
  Is generated by the pseudo-random generator, and the open key forms on the basis of closed.
  At calculation of an open key the integer G (erected is used still
  In a degree pseudo-random number), which size corresponds(meets) to a size
  Keys. It should be identical to both participants of information exchange
  And to be nonzero and unit. Main idea of algorithm of the Diffie-Hellman
  Consists in that the same private key, with which are ciphered
  The data, can be obtained as from the closed key and another's open,
  And from the open key and another's closed. Thus, everyone
  The participant of information exchange should own the personal closed key and open
  By keys of all other participants. Then for each couple will be confidential
  Key, and, not knowing their closed keys, nobody can decrypt or
  To forge the messages, which these two participants interchange. It is possible also
  To prepare the message for the concrete subject (holder of the closed key),
  Using his(its) open key, so that anybody, except for this subject, could not
  To read the given message (even the remailer). That it to make,
  It is necessary to generate the temporary closed key, to encipher a message with the help
  The given key and open key of the receiver to generate temporary open
  Key (on the basis of closed) and to enclose it(him) on the message, then temporary
  The closed key to delete. A method of the Diffie-Hellman on the basis here is realized
  2^4253-1. This prime number such as Mersenne. On stability(resistance) the given implementation
  Corresponds(meets) approximately to 226 bits for the symmetric ciphers. Have in
  To sort, that the generation open or private key can borrow(occupy) a little
  Seconds. The most simple way of modular exponentiation is used.}

type
  PDHKey4253 = ^TDHKey4253;
  TDHKey4253 = array [0.. 132] of LongWord;

{ Q_DHCreatePublicKey form an open key PublicKey on the basis of closed
  Key PrivateKey and constant G. Memory under open key is arranged up to
  Call of this procedure (the one who calls(causes) her(it)).}

procedure Q_DHCreatePublicKey (G, PrivateKey: PDHKey4253; PublicKey: PDHKey4253);

{ Q_DHGetCipherKey generate a main key by a size KeySize byte, which
  Then is used for encoding or decryption of the data (with the help anyone
  Symmetric method), on the basis of an open key PublicKey (another's) and
  private key PrivateKey (one's). The obtained key is saved in area
  Memories pointed by the parameter CipherKey.}

procedure Q_DHGetCipherKey (PublicKey, PrivateKey: PDHKey4253;
  CipherKey: Pointer; KeySize: Integer);

{ Q_DHSelfTest checks service capability of procedures realizing a method Diffie-Hellman
  also returns True, if all in the order, differently - False. On the computer
  With the processor PII-233 this function is fulfilled approximately for 8.5 seconds.}

function Q_DHSelfTest: Boolean;


{ Useful standard string functions.}

{ StringOfChar resets string consisting from Count of characters Ch. For example,
   StringOfChar ('A', 10) will return string 'AAAAAAAAAA'. (unit System)

function StringOfChar (Ch: Char; Count: Integer): string;}

{ SetString installs S so that she(it) pointed on anew
  The distributed string of length Len of characters. If the parameter Buffer is equal nil,
  Contents of new string remain uncertain, differently SetString copies
  Len of characters from Buffer in new string. If for allocation of string
  Not enough memory there is an exclusive situation EOutOfMemory. Call
  Functions SetString guarantees, that S will be unique string, i.e.
  The counter of the references of this string will be equal to unit.

  For allocation in memory of new string S in length Len of characters use call
  Procedures SetString: SetString (S,nil,Len). (unit System)

procedure SetString (var S: string; Buffer: PChar; Len: Integer);}

{ SetLength reallocates memory under string addressed by the parameter S, such
  By image, that its(her) length equalled NewLength. Existing characters in string
  Are saved, and contents of the additional selected(allocated) memory will be
  Uncertain. If the necessary quantity of memory can not be selected(allocated),
  The exception EOutOfMemory is called(caused). The call SetLength guarantees, that the ambassador
  Him(it) the string S will be unique, i.e. its(her) counter of the references will be equal
  To unit. (unit System)

procedure SetLength (var S; NewLength: Integer);}

{ Copy resets a substring from string. S is initial string (expression of a type
  string), Index and Count - integer expressions. Copy resets a substring,
  Containing Count of characters, since S [Index]. If Index it is more, than length
  Strings S, Copy resets empty string. If Count sets more characters,
  Than them there is up to an end of string, are reset a substring, since S [Index]
  And up to the end of string. (unit System)

function Copy (S; Index, Count: Integer): string;}

{ Insert inserts a substring into string, since the indicated character. A substring
  Source is inserted into string S, since the character S [Index]. Index - number
  The character, with which the insert starts. If Index lengths of string S there are more,
  The substring Source is simply added in the end of string S. (unit System)

procedure Insert (Source: string; var S: string; Index: Integer);}

{ UniqueString guarantees, that the given string Str will have the counter of the references
  Equal to unit. The call of this procedure is necessary, when in the program string
  Is resulted in the type PChar, and then contents of string vary.
  (Unit System)

procedure UniqueString (var Str: string);}

{ WrapText finds in string Line characters entering into set BreakChars,
  Also adds line feed transferred(handed) by the parameter BreakStr, ambassador of the latter
  The character from BreakChars before a position MaxCol. MaxCol is a maximum length
  Strings (up to characters of line feed). If parameters BreakStr and BreakChars
  Are lowered(omitted), the function WrapText searches in string Line for blanks, characters of a worm and characters
  The tab stops, in which position can be supplemented breakings of string as a couple
  Characters #13#10. This function does not insert breakings of string into fragments,
  Concluded in single or double �������. (unit SysUtils)

function WrapText (const Line, BreakStr: string; BreakChars: TSysCharSet;
  MaxCol: Integer): string; overload;
function WrapText (const Line: string; MaxCol: Integer = 45): string; overload;}

{ AdjustLineBreaks substitutes all line feeds in string S on correct
  Sequence of characters CR/LF. This function transforms any character CR,
  Which the character LF does not follow, and any character LF, before which does not cost
  The character CR, in a pair of characters CR/LF. She(it) also will convert couples of characters LF/CR
  In couples CR/LF. The sequence LF/CR is usually used in text
  Files Unix. (unit SysUtils)

function AdjustLineBreaks (const S: string): string;}

{ QuotedStr concludes the string,(line,) transferred(handed) to it,(her,) in unary quotes ('). They
  Are added in the beginning and at the end of string. Besides each character unary
  Quotes inside string is doubled. (unit SysUtils)

function QuotedStr (const S: string): string;}


{ DEFINE USE_DYNAMIC_TABLES} // Substitute or remove $ in the beginning of string

{ Code charts for change of the register of characters and code conversion of strings from
  OEM in ANSI and on the contrary can be formed dynamically at start of the program
  (According to current customizations on the computer of the user) or they
  Can rigidly be set at compilation of the program (as the constant arrays).
  If the character USE_DYNAMIC_TABLES is defined (with the help $DEFINE), are used
  The dynamic tables, differently - static.}

{ $IFNDEF USE_DYNAMIC_TABLES}

{ If you gather to use static tables of code conversion, but with
  By other character set (distinct from Russian coding Win1251), for this purpose
  It is necessary to change the given below constant arrays. They can be
  Are obtained as follows: install in Control Panel the language, necessary to you,
  Compile and launch the following program.

  program MakeTables;
  uses
    Windows, SysUtils;
  var
    Ch, N: Byte;
    I, J: Integer;
    F: TextFile;
  begin
    AssignFile (F, 'CsTables.txt');
    Rewrite (F);
    WriteLn (F);
    WriteLn (F, ' ToUpperChars: array [0.. 255] of Char = ');
    Write (F, '(');
    for I: = 0 to 15 do
    begin
      for J: = 0 to 15 do
      begin
        N: = (I shl 4) or J;
        Ch: = N;
        CharUpperBuff (@Ch, 1);
         Write(F,'#$'+IntToHex(Ch,2));
        if N < > 255 then
          Write (F ',')
        else
          Write (F, '); ');
      end;
      WriteLn (F);
      Write (F, ');
    end;
    WriteLn (F);
    WriteLn (F, ' ToLowerChars: array [0.. 255] of Char = ');
    Write (F, '(');
    for I: = 0 to 15 do
    begin
      for J: = 0 to 15 do
      begin
        N: = (I shl 4) or J;
        Ch: = N;
        CharLowerBuff (@Ch, 1);
         Write(F,'#$'+IntToHex(Ch,2));
        if N < > 255 then
          Write (F ',')
        else
          Write (F, '); ');
      end;
      WriteLn (F);
      Write (F, ');
    end;
    WriteLn (F);
    WriteLn (F, ' ToOemChars: array [0.. 255] of Char = ');
    Write (F, '(');
    for I: = 0 to 15 do
    begin
      for J: = 0 to 15 do
      begin
        N: = (I shl 4) or J;
        CharToOemBuff (@N, @Ch, 1);
         Write(F,'#$'+IntToHex(Ch,2));
        if N < > 255 then
          Write (F ',')
        else
          Write (F, '); ');
      end;
      WriteLn (F);
      Write (F, ');
    end;
    WriteLn (F);
    WriteLn (F, ' ToAnsiChars: array [0.. 255] of Char = ');
    Write (F, '(');
    for I: = 0 to 15 do
    begin
      for J: = 0 to 15 do
      begin
        N: = (I shl 4) or J;
        OemToCharBuff (@N, @Ch, 1);
         Write(F,'#$'+IntToHex(Ch,2));
        if N < > 255 then
          Write (F ',')
        else
          Write (F, '); ');
      end;
      WriteLn (F);
      Write (F, ');
    end;
    CloseFile (F);
  end.

  In the current directory find the file CsTables.txt and "as is" insert his(its) ambassador
  const. Compiling this unit with a key $J+, you receive possibility to change
  The static tables of code conversion in execution time of the program (dynamic
  The tables you can change irrespective of any keys).

  Multibyte characters and Unicode are not supported.
}

const
  ToUpperChars: array[0..255] of Char =
    (#$00,#$01,#$02,#$03,#$04,#$05,#$06,#$07,#$08,#$09,#$0A,#$0B,#$0C,#$0D,#$0E,#$0F,
     #$10,#$11,#$12,#$13,#$14,#$15,#$16,#$17,#$18,#$19,#$1A,#$1B,#$1C,#$1D,#$1E,#$1F,
     #$20,#$21,#$22,#$23,#$24,#$25,#$26,#$27,#$28,#$29,#$2A,#$2B,#$2C,#$2D,#$2E,#$2F,
     #$30,#$31,#$32,#$33,#$34,#$35,#$36,#$37,#$38,#$39,#$3A,#$3B,#$3C,#$3D,#$3E,#$3F,
     #$40,#$41,#$42,#$43,#$44,#$45,#$46,#$47,#$48,#$49,#$4A,#$4B,#$4C,#$4D,#$4E,#$4F,
     #$50,#$51,#$52,#$53,#$54,#$55,#$56,#$57,#$58,#$59,#$5A,#$5B,#$5C,#$5D,#$5E,#$5F,
     #$60,#$41,#$42,#$43,#$44,#$45,#$46,#$47,#$48,#$49,#$4A,#$4B,#$4C,#$4D,#$4E,#$4F,
     #$50,#$51,#$52,#$53,#$54,#$55,#$56,#$57,#$58,#$59,#$5A,#$7B,#$7C,#$7D,#$7E,#$7F,
     #$80,#$81,#$82,#$81,#$84,#$85,#$86,#$87,#$88,#$89,#$8A,#$8B,#$8C,#$8D,#$8E,#$8F,
     #$80,#$91,#$92,#$93,#$94,#$95,#$96,#$97,#$98,#$99,#$8A,#$9B,#$8C,#$8D,#$8E,#$8F,
     #$A0,#$A1,#$A1,#$A3,#$A4,#$A5,#$A6,#$A7,#$A8,#$A9,#$AA,#$AB,#$AC,#$AD,#$AE,#$AF,
     #$B0,#$B1,#$B2,#$B2,#$A5,#$B5,#$B6,#$B7,#$A8,#$B9,#$AA,#$BB,#$A3,#$BD,#$BD,#$AF,
     #$C0,#$C1,#$C2,#$C3,#$C4,#$C5,#$C6,#$C7,#$C8,#$C9,#$CA,#$CB,#$CC,#$CD,#$CE,#$CF,
     #$D0,#$D1,#$D2,#$D3,#$D4,#$D5,#$D6,#$D7,#$D8,#$D9,#$DA,#$DB,#$DC,#$DD,#$DE,#$DF,
     #$C0,#$C1,#$C2,#$C3,#$C4,#$C5,#$C6,#$C7,#$C8,#$C9,#$CA,#$CB,#$CC,#$CD,#$CE,#$CF,
     #$D0,#$D1,#$D2,#$D3,#$D4,#$D5,#$D6,#$D7,#$D8,#$D9,#$DA,#$DB,#$DC,#$DD,#$DE,#$DF);

  ToLowerChars: array[0..255] of Char =
    (#$00,#$01,#$02,#$03,#$04,#$05,#$06,#$07,#$08,#$09,#$0A,#$0B,#$0C,#$0D,#$0E,#$0F,
     #$10,#$11,#$12,#$13,#$14,#$15,#$16,#$17,#$18,#$19,#$1A,#$1B,#$1C,#$1D,#$1E,#$1F,
     #$20,#$21,#$22,#$23,#$24,#$25,#$26,#$27,#$28,#$29,#$2A,#$2B,#$2C,#$2D,#$2E,#$2F,
     #$30,#$31,#$32,#$33,#$34,#$35,#$36,#$37,#$38,#$39,#$3A,#$3B,#$3C,#$3D,#$3E,#$3F,
     #$40,#$61,#$62,#$63,#$64,#$65,#$66,#$67,#$68,#$69,#$6A,#$6B,#$6C,#$6D,#$6E,#$6F,
     #$70,#$71,#$72,#$73,#$74,#$75,#$76,#$77,#$78,#$79,#$7A,#$5B,#$5C,#$5D,#$5E,#$5F,
     #$60,#$61,#$62,#$63,#$64,#$65,#$66,#$67,#$68,#$69,#$6A,#$6B,#$6C,#$6D,#$6E,#$6F,
     #$70,#$71,#$72,#$73,#$74,#$75,#$76,#$77,#$78,#$79,#$7A,#$7B,#$7C,#$7D,#$7E,#$7F,
     #$90,#$83,#$82,#$83,#$84,#$85,#$86,#$87,#$88,#$89,#$9A,#$8B,#$9C,#$9D,#$9E,#$9F,
     #$90,#$91,#$92,#$93,#$94,#$95,#$96,#$97,#$98,#$99,#$9A,#$9B,#$9C,#$9D,#$9E,#$9F,
     #$A0,#$A2,#$A2,#$BC,#$A4,#$B4,#$A6,#$A7,#$B8,#$A9,#$BA,#$AB,#$AC,#$AD,#$AE,#$BF,
     #$B0,#$B1,#$B3,#$B3,#$B4,#$B5,#$B6,#$B7,#$B8,#$B9,#$BA,#$BB,#$BC,#$BE,#$BE,#$BF,
     #$E0,#$E1,#$E2,#$E3,#$E4,#$E5,#$E6,#$E7,#$E8,#$E9,#$EA,#$EB,#$EC,#$ED,#$EE,#$EF,
     #$F0,#$F1,#$F2,#$F3,#$F4,#$F5,#$F6,#$F7,#$F8,#$F9,#$FA,#$FB,#$FC,#$FD,#$FE,#$FF,
     #$E0,#$E1,#$E2,#$E3,#$E4,#$E5,#$E6,#$E7,#$E8,#$E9,#$EA,#$EB,#$EC,#$ED,#$EE,#$EF,
     #$F0,#$F1,#$F2,#$F3,#$F4,#$F5,#$F6,#$F7,#$F8,#$F9,#$FA,#$FB,#$FC,#$FD,#$FE,#$FF);

  ToOemChars: array[0..255] of Char =
    (#$00,#$01,#$02,#$03,#$04,#$05,#$06,#$07,#$08,#$09,#$0A,#$0B,#$0C,#$0D,#$0E,#$0F,
     #$10,#$11,#$12,#$13,#$14,#$15,#$16,#$17,#$18,#$19,#$1A,#$1B,#$1C,#$1D,#$1E,#$1F,
     #$20,#$21,#$22,#$23,#$24,#$25,#$26,#$27,#$28,#$29,#$2A,#$2B,#$2C,#$2D,#$2E,#$2F,
     #$30,#$31,#$32,#$33,#$34,#$35,#$36,#$37,#$38,#$39,#$3A,#$3B,#$3C,#$3D,#$3E,#$3F,
     #$40,#$41,#$42,#$43,#$44,#$45,#$46,#$47,#$48,#$49,#$4A,#$4B,#$4C,#$4D,#$4E,#$4F,
     #$50,#$51,#$52,#$53,#$54,#$55,#$56,#$57,#$58,#$59,#$5A,#$5B,#$5C,#$5D,#$5E,#$5F,
     #$60,#$61,#$62,#$63,#$64,#$65,#$66,#$67,#$68,#$69,#$6A,#$6B,#$6C,#$6D,#$6E,#$6F,
     #$70,#$71,#$72,#$73,#$74,#$75,#$76,#$77,#$78,#$79,#$7A,#$7B,#$7C,#$7D,#$7E,#$7F,
     #$3F,#$3F,#$27,#$3F,#$22,#$3A,#$C5,#$D8,#$3F,#$25,#$3F,#$3C,#$3F,#$3F,#$3F,#$3F,
     #$3F,#$27,#$27,#$22,#$22,#$07,#$2D,#$2D,#$3F,#$54,#$3F,#$3E,#$3F,#$3F,#$3F,#$3F,
     #$FF,#$F6,#$F7,#$3F,#$FD,#$3F,#$B3,#$15,#$F0,#$63,#$F2,#$3C,#$BF,#$2D,#$52,#$F4,
     #$F8,#$2B,#$3F,#$3F,#$3F,#$E7,#$14,#$FA,#$F1,#$FC,#$F3,#$3E,#$3F,#$3F,#$3F,#$F5,
     #$80,#$81,#$82,#$83,#$84,#$85,#$86,#$87,#$88,#$89,#$8A,#$8B,#$8C,#$8D,#$8E,#$8F,
     #$90,#$91,#$92,#$93,#$94,#$95,#$96,#$97,#$98,#$99,#$9A,#$9B,#$9C,#$9D,#$9E,#$9F,
     #$A0,#$A1,#$A2,#$A3,#$A4,#$A5,#$A6,#$A7,#$A8,#$A9,#$AA,#$AB,#$AC,#$AD,#$AE,#$AF,
     #$E0,#$E1,#$E2,#$E3,#$E4,#$E5,#$E6,#$E7,#$E8,#$E9,#$EA,#$EB,#$EC,#$ED,#$EE,#$EF);

  ToAnsiChars: array[0..255] of Char =
    (#$00,#$01,#$02,#$03,#$04,#$05,#$06,#$07,#$08,#$09,#$0A,#$0B,#$0C,#$0D,#$0E,#$A4,
     #$10,#$11,#$12,#$13,#$B6,#$A7,#$16,#$17,#$18,#$19,#$1A,#$1B,#$1C,#$1D,#$1E,#$1F,
     #$20,#$21,#$22,#$23,#$24,#$25,#$26,#$27,#$28,#$29,#$2A,#$2B,#$2C,#$2D,#$2E,#$2F,
     #$30,#$31,#$32,#$33,#$34,#$35,#$36,#$37,#$38,#$39,#$3A,#$3B,#$3C,#$3D,#$3E,#$3F,
     #$40,#$41,#$42,#$43,#$44,#$45,#$46,#$47,#$48,#$49,#$4A,#$4B,#$4C,#$4D,#$4E,#$4F,
     #$50,#$51,#$52,#$53,#$54,#$55,#$56,#$57,#$58,#$59,#$5A,#$5B,#$5C,#$5D,#$5E,#$5F,
     #$60,#$61,#$62,#$63,#$64,#$65,#$66,#$67,#$68,#$69,#$6A,#$6B,#$6C,#$6D,#$6E,#$6F,
     #$70,#$71,#$72,#$73,#$74,#$75,#$76,#$77,#$78,#$79,#$7A,#$7B,#$7C,#$7D,#$7E,#$7F,
     #$C0,#$C1,#$C2,#$C3,#$C4,#$C5,#$C6,#$C7,#$C8,#$C9,#$CA,#$CB,#$CC,#$CD,#$CE,#$CF,
     #$D0,#$D1,#$D2,#$D3,#$D4,#$D5,#$D6,#$D7,#$D8,#$D9,#$DA,#$DB,#$DC,#$DD,#$DE,#$DF,
     #$E0,#$E1,#$E2,#$E3,#$E4,#$E5,#$E6,#$E7,#$E8,#$E9,#$EA,#$EB,#$EC,#$ED,#$EE,#$EF,
     #$2D,#$2D,#$2D,#$A6,#$2B,#$A6,#$A6,#$AC,#$AC,#$A6,#$A6,#$AC,#$2D,#$2D,#$2D,#$AC,
     #$4C,#$2B,#$54,#$2B,#$2D,#$2B,#$A6,#$A6,#$4C,#$E3,#$A6,#$54,#$A6,#$3D,#$2B,#$A6,
     #$A6,#$54,#$54,#$4C,#$4C,#$2D,#$E3,#$2B,#$2B,#$2D,#$2D,#$2D,#$2D,#$A6,#$A6,#$2D,
     #$F0,#$F1,#$F2,#$F3,#$F4,#$F5,#$F6,#$F7,#$F8,#$F9,#$FA,#$FB,#$FC,#$FD,#$FE,#$FF,
     #$A8,#$B8,#$AA,#$BA,#$AF,#$BF,#$A1,#$A2,#$B0,#$95,#$B7,#$76,#$B9,#$A4,#$A6,#$A0);

{$ELSE}

var
  ToUpperChars,ToLowerChars: array[0..255] of Char;
  ToOemChars,ToAnsiChars: array[0..255] of Char;

{$ENDIF}

implementation

