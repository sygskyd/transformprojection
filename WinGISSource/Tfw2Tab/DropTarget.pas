unit DropTarget;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Classes,
  ActiveX,
  Controls;

Type
  TTeDropTarget = class;
  TTeDropInterface = class;

  TTeDropTargetLifeState = (lsStart, lsExists, lsLocked, lsRegd);

  TTeDragOperation = (doNothing, doCopy, doMove, doLink);

  TTeComDragObject = class (TDragObject)
  private
    FDropInterface : TTeDropInterface;
    function GetDataObject: IDataObject;
    function GetDragOperation: TTeDragOperation;
    procedure SetDragOperation(Value: TTeDragOperation);
    function GetShiftState: TShiftState;
  public
    constructor Create(ADropInterface:TTeDropInterface); virtual;
    property DataObject : IDataObject read GetDataObject;
    property DragOperation : TTeDragOperation read GetDragOperation write SetDragOperation;
    property ShiftState : TShiftState read GetShiftState;
  end;

  TComDragObjectClass = class of TTeComDragObject;

  TTeDropInterface = class
  private
    function DoDragOver(DragMsg: TDragMessage): Boolean;
    function DragTo(const Pos: TPoint): Boolean;
    function DragFindTarget(const Pos: TPoint; var Handle: HWND): Pointer;
  protected
    FDropTarget     : TTeDropTarget;
    FWinControl     : TWinControl;
    FDataObject     : IDataObject;
    FDragOperation  : TTeDragOperation;
    FShiftState     : TShiftState;
    FDragObject     : TTeComDragObject;
  public
    property  DropTarget: TTeDropTarget read FDropTarget;

    constructor Create(AWinControl: TWinControl); virtual;
    destructor  Destroy; override;
    procedure   BeforeDestruction; override;

    function   DropTarget_Create  : HResult;
    function   DropTarget_Destroy : HResult;
    function   DropTarget_Exists  : Boolean;
  protected
    procedure  DropTarget_Forget;
  public
    function   DropTarget_LifeState: TTeDropTargetLifeState;

    function   DragEnter(const dataObj: IDataObject; grfKeyState: Longint;
      pt: TPoint; var dwEffect: Longint): HResult; virtual;
    function   DragOver(grfKeyState: Longint; pt: TPoint;
      var dwEffect: Longint): HResult; virtual;
    function   DragLeave: HResult; virtual;
    function   Drop(const dataObj: IDataObject; grfKeyState: Longint; pt: TPoint;
      var dwEffect: Longint): HResult; virtual;

    property DataObject: IDataObject read FDataObject;
    property DragOperation : TTeDragOperation read FDragOperation write FDragOperation;
    property ShiftState : TShiftState read FShiftState;
  end;

  TTeDropTarget = class (TInterfacedObject, IDropTarget)
  private
    FDropHWND       : HWND;
    FDropWinControl : TWinControl;

    FDropInterface  : TTeDropInterface;
    FLifeState      : TTeDropTargetLifeState;

    procedure SetLifeState(Value: TTeDropTargetLifeState);
  public
    property DropHWND       : HWND         read FDropHWnd;
    property DropWinControl : TWinControl  read FDropWinControl;
    property LifeState: TTeDropTargetLifeState read FLifeState write SetLifeState;

    constructor Create(AWinControl: TWinControl; ADropInterface: TTeDropInterface); virtual;
    procedure   BeforeDestruction; override;

    function ToState_Exists : HResult;
    function ToState_Locked : HResult;
    function ToState_Regd   : HResult;
  public
    { IDropTarget }
    function DragEnter(const dataObj: IDataObject; grfKeyState: Longint;
      pt: TPoint; var dwEffect: Longint): HResult; stdcall;
    function DragOver(grfKeyState: Longint; pt: TPoint;
      var dwEffect: Longint): HResult; stdcall;
    function DragLeave: HResult; stdcall;
    function Drop(const dataObj: IDataObject; grfKeyState: Longint; pt: TPoint;
      var dwEffect: Longint): HResult; stdcall;
  end;

//=== ENUMERATE FORMATS ========================================================
// This is a simple format enumerator for an IDataObject interface.  It has
// been written to be expanded but this version can only respond to an
// IDataObject that contains a CF_TEXT format.

type
  TEnumFormats = class
  private
    FDataObject : IDataObject;
    FEnumerator : IEnumFormatEtc;
    FFormatEtc  : TFormatEtc;
    FMediumValid,
    FValid      : boolean;
    FCount      : integer;
    FMedium     : TStgMedium;
    procedure SetDataObject (Value : IDataObject);
    function SomeText (Format : TClipFormat) : string;
  public
    constructor Create (DataObject : IDataObject);
    destructor Destroy; override;
// frees memory associated with the storage medium
    procedure FreeMedium;
// reset to the start of the enum list
    function Reset : boolean;
// returns next formatetc or first if reset just called
    function Next : boolean;
// returns true if a given format is available
    function HasFormat (ClipFormat : TClipFormat) : boolean;
// returns the handle to a given type of medium required
    function Handle (Tymed : integer): hGlobal;
// Global handle from a tsGlobal type of medium
    function GlobalHandle : hGlobal;
// Text available?
    function HasText : boolean;
    function Text : string;
// number of formats available
    property Count : integer read FCount;
// the dataobject interface for which enum is required
    property DataObject : IDataObject read FDataObject write SetDataObject;
// true if formatetc stuff is valid
    property Valid : boolean read FValid;
// information held by current formatetc if valid
    property FormatEtc : TFormatEtc read FFormatEtc;
    property Aspect : integer read FFormatEtc.dwAspect write FFormatEtc.dwAspect;
    property Format : TClipFormat read FFormatEtc.cfFormat write FFormatEtc.cfFormat;
    property Index : integer read FFormatEtc.lIndex write FFormatEtc.lIndex;
    property Medium : integer read FFormatEtc.Tymed write FFormatEtc.Tymed;
  end;

var
  ComDragObjectClass : TComDragObjectClass;

const
  Effects :array[TTeDragOperation]of Integer =
    (DROPEFFECT_NONE,DROPEFFECT_COPY,DROPEFFECT_MOVE,DROPEFFECT_LINK);

// Data transfer direction during data type enumeration (original in ActiveX)
// as Delphi style constants
const
  ddGet = DATADIR_GET;
  ddSet = DATADIR_SET;

// Type of storage medium for data formats (original in ActiveX)
// as Delphi style constants
const
  tsGlobal       = TYMED_HGLOBAL;   // handle to global memory clock
  tsFile         = TYMED_FILE;      // file
  tsStream       = TYMED_ISTREAM;   // stream interface
  tsStorage      = TYMED_ISTORAGE;  // storage interface
  tsGDI          = TYMED_GDI;       // gdi object
  tsMetafilePict = TYMED_MFPICT;    // metafilepict structure
  tsEnhMetafile  = TYMED_ENHMF;     // enhanced metafile
  tsNull         = TYMED_NULL;      // no storage


function Succeeded(Res: HResult): Boolean;

implementation

function Succeeded(Res: HResult): Boolean;
begin
  Result := Res and $80000000 = 0;
end;

function DragMessage(Handle: HWND; Msg: TDragMessage; Source: TDragObject; Target: Pointer; const Pos: TPoint): Longint;
var
  DragRec: TDragRec;
begin
  Result := 0;
  if Handle <> 0 then
  begin
    DragRec.Pos := Pos;
    DragRec.Target := Target;
    DragRec.Source := Source;
    DragRec.Docking := false;
    Result := SendMessage(Handle, CM_DRAG, Longint(Msg), Longint(@DragRec));
  end;
end;

function DragFindWindow(const Pos: TPoint): HWND;
begin
  Result := WindowFromPoint(Pos);
  while Result <> 0 do
    if not Assigned(FindControl(Result)) then Result := GetParent(Result)
    else Exit;
end;

function TTeDropInterface.DragFindTarget(const Pos: TPoint; var Handle: HWND ): Pointer;
begin
  Handle := DragFindWindow(Pos);
  Result := Pointer(DragMessage(Handle, dmFindTarget, FDragObject, nil, Pos));
end;

function TTeDropInterface.DoDragOver(DragMsg: TDragMessage): Boolean;
begin
  Result := False;
  if FDragObject.DragTarget <> nil then
    Result := LongBool(DragMessage(FDragObject.DragHandle, DragMsg, FDragObject,
      FDragObject.DragTarget, FDragObject.DragPos));
end;

function TTeDropInterface.DragTo(const Pos: TPoint) : Boolean;
var
  Target: TControl;
  TargetHandle: HWND;
begin
  Target := DragFindTarget(Pos, TargetHandle);
  if Target <> FDragObject.DragTarget then
  begin
    DoDragOver(dmDragLeave);
    FDragObject.DragTarget := Target;
    FDragObject.DragHandle := TargetHandle;
    FDragObject.DragPos := Pos;
    DoDragOver(dmDragEnter);
  end;
  FDragObject.DragPos := Pos;
  if FDragObject.DragTarget <> nil then
    FDragObject.DragTargetPos := TControl(FDragObject.DragTarget).ScreenToClient(Pos);
  Result := DoDragOver(dmDragMove);
end;

constructor TTeDropInterface.Create(AWinControl: TWinControl);
Begin
  inherited Create;
  FWinControl        := AWinControl;
  FDropTarget        := nil;
  FDragObject        := ComDragObjectClass.Create(Self);
end;

procedure TTeDropInterface.BeforeDestruction;
begin
  inherited;
  if Assigned(FDragObject) then
    FDragObject.FDropInterface:=nil;
  If Assigned(FDropTarget) then FDropTarget.Free;
end;

function  TTeDropInterface.DropTarget_Create: HResult;
Begin
  Result := E_UNEXPECTED;
  try
  if not Assigned(FDropTarget) then
    FDropTarget := TTeDropTarget.Create(FWinControl, Self);
  If Assigned(FDropTarget) then
    Result := DropTarget.ToState_Regd;
  except
    Result := E_UNEXPECTED;
  end;
end;

function TTeDropInterface.DropTarget_Destroy : HResult;
begin
  Result := S_OK;
  try
    If Assigned(FDropTarget) then
      Result := DropTarget.ToState_Locked;
  except
    Result := E_UNEXPECTED;
  end;
end;

function TTeDropInterface.DropTarget_Exists: Boolean;
Begin
  Result := Assigned(FDropTarget);
end;

procedure TTeDropInterface.DropTarget_Forget;
Begin
  FDropTarget := nil;
end;

function  TTeDropInterface.DropTarget_LifeState: TTeDropTargetLifeState;
Begin
  If DropTarget_Exists then
    Result := DropTarget.LifeState
  else
    Result := lsStart;
end;

function CreateShiftState(grfKeyState : Longint):TShiftState;
begin
  Result:=[];
  if (grfKeyState and MK_CONTROL) = MK_CONTROL then Include (Result, ssCtrl);
  if (grfKeyState and MK_SHIFT)   = MK_SHIFT   then Include (Result, ssShift);
//  if (grfKeyState and MK_ALT)     = MK_ALT     then Include (Result, ssAlt);
  if (grfKeyState and MK_LBUTTON) = MK_LBUTTON then Include (Result, ssLeft);
  if (grfKeyState and MK_MBUTTON) = MK_MBUTTON then Include (Result, ssMiddle);
  if (grfKeyState and MK_RBUTTON) = MK_RBUTTON then Include (Result, ssRight);
end;

function CreateDragOperation(ShiftState:TShiftState): TTeDragOperation;
begin
  Result:=doMove; // muss noch ge�ndert werden;
  if ssCtrl in ShiftState then Result := doCopy;
  if ssShift in ShiftState then Result := doMove;
  if (ssCtrl in ShiftState) and (ssShift in ShiftState) then Result := doLink;
end;

function TTeDropInterface.DragEnter(const dataObj : IDataObject; grfKeyState : Longint; pt : TPoint; var dwEffect : Longint): HResult;
Begin
  Result := S_OK;
  dwEffect := DROPEFFECT_NONE;
  if not Assigned(FWinControl) then exit;
  if not Assigned(FDragObject) then exit;
  try
    FShiftState:=CreateShiftState(grfKeyState);
    FDragOperation:=CreateDragOperation(FShiftState);
    FDataObject:= dataObj;
    if not DragTo(pt) then
      FDragOperation:=doNothing;
    dwEffect := Effects[FDragOperation];
  except
    Result := E_UNEXPECTED;
  end;
end;

function TTeDropInterface.DragOver( grfKeyState : Longint; pt : TPoint; var dwEffect : Longint): HResult;
Begin
  Result := S_OK;
  dwEffect := DROPEFFECT_NONE;
  if not Assigned(FWinControl) then exit;
  if not Assigned(FDragObject) then exit;
  try
    FShiftState:=CreateShiftState(grfKeyState);
    FDragOperation:=CreateDragOperation(FShiftState);
    if not DragTo(pt) then
      FDragOperation:=doNothing;
    dwEffect := Effects[FDragOperation];
  except
    Result := E_UNEXPECTED;
  end;
end;

function TTeDropInterface.DragLeave: HResult;
Begin
  Result := S_OK;
  if not Assigned(FWinControl) then exit;
  if not Assigned(FDragObject) then exit;
  try
    DoDragOver(dmDragLeave);
    FDragObject.DragTarget :=  nil;
    FDragObject.DragHandle := 0;
    FDataObject:=nil;
  except
    Result := E_UNEXPECTED;
  end;
end;

function TTeDropInterface.Drop(const dataObj : IDataObject; grfKeyState : Longint; pt : TPoint; var dwEffect : Longint): HResult;
Begin
  Result := S_OK;
  dwEffect := DROPEFFECT_NONE;
  if not Assigned(FWinControl) then exit;
  if not Assigned(FDragObject) then exit;
  try
    FDataObject:=dataObj;
    try
      FShiftState:=CreateShiftState(grfKeyState);
      FDragOperation:=CreateDragOperation(FShiftState);
      if not DragTo(pt) then
        FDragOperation:=doNothing;
      dwEffect := Effects[FDragOperation];
      if FDragOperation <> doNothing then
        DoDragOver(dmDragDrop);
    finally
      FDataObject:=nil;
    end;
  except
    Result := E_UNEXPECTED;
  end;
end;

constructor TTeDropTarget.Create(AWinControl: TWinControl; ADropInterface: TTeDropInterface);
Begin
  inherited Create;
  FDropWinControl    := AWinControl;
  FDropInterface     := ADropInterface;
  FLifeState         := lsExists;
end;

procedure TTeDropTarget.BeforeDestruction;
begin
  If Assigned(FDropInterface) then FDropInterface.DropTarget_Forget;

  if FLifeState > lsLocked then
  Begin
    While RefCount < 2 do _AddRef;
    ActiveX.RevokeDragDrop(FDropHWND);
    FDropHWND:=0;
    FLifeState := lsLocked;
  end;

  if FLifeState > lsExists then
  Begin
    While RefCount < 2 do _AddRef;
    ActiveX.CoLockObjectExternal(Self as IDropTarget, false, false );
    FLifeState := lsExists;
  end;
end;

function TTeDropTarget.ToState_Exists : HResult;
Begin
  Result := S_OK;
  If LifeState = lsRegd then Result := ToState_Locked;

  If LifeState = lsLocked then
  Begin
    LifeState := lsExists;
    Result := ActiveX.CoLockObjectExternal(Self as IDropTarget, false, true );
  end;
end;

function TTeDropTarget.ToState_Locked : HResult;
Begin
  Result := S_OK;

  If LifeState = lsExists then
  Begin
    Result := ActiveX.CoLockObjectExternal(Self as IDropTarget, true, false);
    If Result = S_OK then LifeState := lsLocked;
  end;

  If LifeState = lsRegd then
  Begin
    While RefCount < 2 do _AddRef;
    Result := ActiveX.RevokeDragDrop(FDropHWND);
    FDropHWND:=0;
    If Result = S_OK then LifeState := lsLocked;
  end;
end;

function TTeDropTarget.ToState_Regd   : HResult;
Begin
  Result := S_OK;
  If LifeState = lsExists then Result := ToState_Locked;
  If LifeState = lsLocked then
  Begin
    FDropHWND:=FDropWinControl.Handle;
    Result := ActiveX.RegisterDragDrop(FDropHWND, Self as IDropTarget);
    If Result = S_OK then LifeState := lsRegd;
  end;
end;

procedure TTeDropTarget.SetLifeState(Value: TTeDropTargetLifeState);
Begin
  FLifeState := Value;
end;

function TTeDropTarget.DragEnter(const dataObj : IDataObject; grfKeyState : Longint; pt : TPoint; var dwEffect : Longint): HResult;
Begin
  if Assigned(FDropInterface) then
    Result := FDropInterface.DragEnter(dataObj, grfKeyState, pt, dwEffect)
  else
    Result:= E_UNEXPECTED;
end;

function TTeDropTarget.DragOver(grfKeyState : Longint; pt : TPoint; var dwEffect    : Longint): HResult;
Begin
  if Assigned(FDropInterface) then
    Result := FDropInterface.DragOver(grfKeyState, pt, dwEffect)
  else
    Result:= E_UNEXPECTED;
end;

function TTeDropTarget.DragLeave: HResult;
Begin
  if Assigned(FDropInterface) then
    Result := FDropInterface.DragLeave
  else
    Result:= E_UNEXPECTED;
end;

function TTeDropTarget.Drop(const dataObj : IDataObject; grfKeyState : Longint; pt : TPoint; var dwEffect : Longint): HResult;
Begin
  if Assigned(FDropInterface) then
    Result := FDropInterface.Drop(dataObj, grfKeyState, pt, dwEffect)
  else
    Result:= E_UNEXPECTED;
end;

{ TTeComDragObject }

constructor TTeComDragObject.Create(ADropInterface: TTeDropInterface);
begin
  inherited Create;
  FDropInterface:=ADropInterface;
end;

function TTeComDragObject.GetDataObject: IDataObject;
begin
 if Assigned(FDropInterface) then
   result := FDropInterface.DataObject
 else
   result:=nil;
end;

function TTeComDragObject.GetDragOperation: TTeDragOperation;
begin
 if Assigned(FDropInterface) then
   result := FDropInterface.DragOperation
 else
   result:=doNothing;
end;

function TTeComDragObject.GetShiftState: TShiftState;
begin
 if Assigned(FDropInterface) then
   result := FDropInterface.ShiftState
 else
   result:=[];
end;

procedure TTeComDragObject.SetDragOperation(Value: TTeDragOperation);
begin
 if Assigned(FDropInterface) then
   FDropInterface.DragOperation:=Value;
end;

destructor TTeDropInterface.Destroy;
begin
  if Assigned(FDragObject) then
    begin
      FDragObject.Free;
      FDragObject:=nil;
    end;
  inherited;
end;
// Create the enumerator and set the dataobject
constructor TEnumFormats.Create (DataObject : IDataObject);
begin
  inherited Create;
  SetDataObject (DataObject)
end;

// Destroy the dataobject copy and the enumerator
destructor TEnumFormats.Destroy;
begin
  FreeMedium;
  SetDataObject (nil);
  inherited Destroy
end;

//--- free the memory associated with the storage medium record FMedium

procedure TEnumFormats.FreeMedium;
begin
  if FMediumValid then
    ReleaseStgMedium (FMedium);
  FMediumValid := false
end;

//--- function to obtain the next Format supported by the DataObject, or the
//    first if Reset has just been called, returns true on success
function TEnumFormats.Next : boolean;
var
  Returned : integer;
begin
  inc (FCount);
  FValid := FEnumerator.Next (1, FFormatEtc, @Returned) = S_OK;
  Result := FValid
end;

//--- Reset the Enumerator interface back to the beginning of the list,
//    returns true on success
function TEnumFormats.Reset : boolean;
begin
  FValid := false;
  FCount := 0;
  Result := Succeeded (FEnumerator.Reset)
end;

//--- Enumerate the data object for a specific format, returns true if
//    found, then the FFormatEtc record will be valid
function TEnumFormats.HasFormat (ClipFormat : TClipFormat) : boolean;
begin
  Result := false;
  if Reset then
    while (not Result) and Next do
      Result := ClipFormat = Format
end;

procedure TEnumFormats.SetDataObject (Value : IDataObject);
var
  Result : integer;
begin
// clear current values and free
  FDataObject := nil;

// new interfaces
  FDataObject := Value;
  if Assigned (FDataObject) then
  begin
    Result := FDataObject.EnumFormatEtc (ddGet, FEnumerator);
    Assert (Succeeded (Result), 'Cannot get the format enumerator');
    Reset
  end
end;

// returns a handle to the current formatetc given the
// type of medium required
function TEnumFormats.Handle (Tymed : integer): hGlobal;
var
  FormatEtc : TFormatEtc;
begin
  FreeMedium;
  Result := 0;
  if FValid and (FFormatEtc.tymed and Tymed = Tymed) then
  begin
    FormatEtc := FFormatEtc;
    FormatEtc.tymed := FormatEtc.tymed and Tymed; // use only the requested type
    if Succeeded (FDataObject.GetData (FormatEtc, FMedium)) then
    begin
      FMediumValid := true;
      Result := FMedium.hGlobal
    end
  end
end;

// Get a global data handle (eg for CF_TEXT)
function TEnumFormats.GlobalHandle : hGlobal;
begin
  Result := Handle (tsGlobal)
end;

//--- function to return a string, used by CF_TEXT
function TEnumFormats.SomeText (Format : TClipFormat) : string;
var
  H : hGlobal;
  P : PChar;
begin
  Result := '';
  if HasFormat (Format) then  // check that text is available *AND* position
  begin                       // the enumerator on the text data
    H := GlobalHandle;        // get the global handle to the data
    if FMediumValid then
    try
      if H <> 0 then
      begin
        P := GlobalLock (H);    // it's a pointer to a null terminated string
        try
          Result := P           // get our copy
        finally
          GlobalUnLock (H)      // let it go
        end
      end
    finally
      FreeMedium                // free the storage medium
    end
  end
end;

//--- TEXT ---
// Returns a text item or empty if not present
function TEnumFormats.Text : string;
begin
  Result := SomeText (CF_TEXT)
end;

// Returns true if some text is available
function TEnumFormats.HasText : boolean;
begin
  Result := HasFormat (CF_TEXT)
end;

initialization
  ComDragObjectClass := TTeComDragObject;
  OleInitialize (Nil);
finalization
  OleUninitialize;
end.

