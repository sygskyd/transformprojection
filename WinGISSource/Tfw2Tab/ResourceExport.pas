unit ResourceExport;

{ Resource Exporter v.1.0 }

{ (c) 2000 - Tom Peiffer < peiffer@rhrk.uni-kl.de > }
{ Read readme.htm file for description }
{ Feel free to distibute this component }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

const
 reOK = 0 ;
 reNotFound = 1 ;
 reLoadError = 2 ;
 reFileExists = 4 ;

type
  TOnSuccessExportFile = procedure (Sender : TObject ;
                           BytesWritten : integer) of object ;

  TResourceExport = class(TComponent)
  private
    FOverwrite: boolean;
    FResourceName: string;
    FResourceCategory: string;
    FExportFileName: string;
    FSize: integer;
    FOnResourceNotFound: TNotifyEvent;
    FOnWriteFileError: TNotifyEvent;
    FOnLoadResourceError: TNotifyEvent;
    FOnSuccess: TOnSuccessExportFile;
    procedure SetExportFileName(const Value: string);
    procedure SetOverwrite(const Value: boolean);
    procedure SetResourceCategory(const Value: string);
    procedure SetResourceName(const Value: string);
    procedure SetOnResourceNotFound(const Value: TNotifyEvent);
    procedure SetOnLoadResourceError(const Value: TNotifyEvent);
    procedure SetOnWriteFileError(const Value: TNotifyEvent);
    procedure SeTOnSuccessExportFile(const Value: TOnSuccessExportFile);
    { Private-Deklarationen}
  protected
    { Protected-Deklarationen}
  public
    { Public-Deklarationen}
    property Size : integer  read FSize ;
    constructor Create (AOwner : TComponent) ; override ;
    function Execute : integer ;
  published
    { Published-Deklarationen }
    property ResourceName : string  read FResourceName write SetResourceName;
    property ResourceCategory : string  read FResourceCategory write SetResourceCategory;
    property ExportFileName : string  read FExportFileName write SetExportFileName;
    property Overwrite : boolean  read FOverwrite write SetOverwrite;
    property OnResourceNotFound : TNotifyEvent  read FOnResourceNotFound write SetOnResourceNotFound;
    property OnLoadResourceError : TNotifyEvent  read FOnLoadResourceError write SetOnLoadResourceError;
    property OnWriteFileError : TNotifyEvent  read FOnWriteFileError write SetOnWriteFileError;
    property OnSuccess : TOnSuccessExportFile  read FOnSuccess write SeTOnSuccessExportFile;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Tom', [TResourceExport]);
end;

{ TResourceExport }

constructor TResourceExport.Create(AOwner: TComponent);
begin
     inherited Create (AOwner) ;
     FExportFileName := '' ;
     FResourceName := '' ;
     FResourceCategory := '' ;
     FOverwrite := false ;
     FSize := -1 ;
end;

function TResourceExport.Execute: integer;
var
 Res, ResHandle : THandle ;
 P : ^Char ;
 N : integer ;
 FS : TFileStream ;
begin
     FSize := -1 ;
     Result := reOK ;

     Res := FindResource (HInstance,PChar (FResourceName),PChar(FResourceCategory)) ;
     if res <> 0 then begin
        ResHandle := LoadResource (HInstance,Res) ;
        if ResHandle <> 0 then begin
           N := SizeOfResource (HInstance,res) ;
           FSize := N ;
           P := lockresource (ResHandle) ;
           if not(FileExists (FExportFileName)) or FOverwrite then begin
               DeleteFile (Pchar(FExportFileName)) ;
               FS := TFileStream.Create (FExportFileName,fmCreate) ;
               FS.Write (P^,N) ;
               FS.Free ;
               UnLockResource (resHandle) ;
               FreeResource (resHandle) ;
               P := nil ;
               if Assigned (OnSuccess)
               then OnSuccess (Self,N) ;
           end else begin
               Result := reFileExists ;
               if Assigned (OnWriteFileError)
               then OnWriteFileError (Self) ;
           end ;
        end else begin
            Result := reLoadError ;
            if Assigned (OnLoadResourceError)
            then OnLoadResourceError (Self) ;
        end ;
     end else begin
         Result := reNotFound ;
         if Assigned (OnResourceNotFound)
         then OnResourceNotFound (Self) ;
     end ;
end;

procedure TResourceExport.SetExportFileName(const Value: string);
begin
  FExportFileName := Value;
end;

procedure TResourceExport.SetOnLoadResourceError(
  const Value: TNotifyEvent);
begin
  FOnLoadResourceError := Value;
end;

procedure TResourceExport.SetOnResourceNotFound(const Value: TNotifyEvent);
begin
  FOnResourceNotFound := Value;
end;

procedure TResourceExport.SeTOnSuccessExportFile(const Value: TOnSuccessExportFile);
begin
  FOnSuccess := Value;
end;

procedure TResourceExport.SetOnWriteFileError(const Value: TNotifyEvent);
begin
  FOnWriteFileError := Value;
end;

procedure TResourceExport.SetOverwrite(const Value: boolean);
begin
  FOverwrite := Value;
end;

procedure TResourceExport.SetResourceCategory(const Value: string);
begin
  FResourceCategory := Value;
end;

procedure TResourceExport.SetResourceName(const Value: string);
begin
  FResourceName := Value;
end;

end.
