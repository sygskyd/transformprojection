�
 TFRACTALBOX 0)  TPF0TFractalBox
FractalBoxLeft�TopHintFractal screenBorderIcons BorderStylebsSingleCaptionFractal projectorClientHeightClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
KeyPreview	Menu	MainMenu1OldCreateOrderPositionpoScreenCenterOnCreate
FormCreate	OnDestroyFormDestroy	OnKeyDownFormKeyDownOnMouseDownFormMouseDownOnShowFormShowPixelsPerInch`
TextHeight TPanelFractalDockLeft Top Width�HeightAlignalClientAutoSize	BorderStylebsSingleCaptionFractalDockTabOrder OnResizeFractalDockResize  	TMainMenu	MainMenu1LeftRTopR 	TMenuItemFractaltype1Caption&Fractal typeDefault	ShortCutF@ 	TMenuItemMandelbrott1Caption&1:MandelbrottDefault		RadioItem	OnClickMandelbrott1Click  	TMenuItemJulian1Caption	&2:Julian	RadioItem	OnClickJulian1Click  	TMenuItemMoire1Caption&3:Moire	RadioItem	OnClickMoire1Click  	TMenuItemSierpinski1Caption&4:Sierpinski	RadioItem	OnClickSierpinski1Click  	TMenuItemFern1Caption&5:Fern	RadioItem	OnClick
Fern1Click  	TMenuItemLeaf1Caption&6:Leaf	RadioItem	OnClick
Leaf1Click  	TMenuItemCurl1Caption&7:Curl	RadioItem	OnClick
Curl1Click  	TMenuItemKoch1Caption&8:Koch	RadioItem	OnClick
Koch1Click   	TMenuItemStop1Caption&Stop RenderingEnabled
GroupIndex	RadioItem	OnClick
Stop1Click  	TMenuItemStart1CaptionStartEnabled
GroupIndex	RadioItem	OnClickStart1Click   TTimerTimer1OnTimerTimer1TimerLeftRTop*   