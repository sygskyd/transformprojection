unit DropForm;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  DropTarget;

type
  TDropForm = class(TForm)
  private
    FDropInterface : TTeDropInterface;
  protected
  public
    constructor Create ( AOwner : TComponent ); override;
    destructor Destroy; override;
    procedure CreateWnd; override;
    procedure DestroyWnd; override;
  published
  end;

implementation

constructor TDropForm.Create(AOwner: TComponent);
begin
  FDropInterface := TTeDropInterface.Create(Self);
  inherited;
end;

procedure TDropForm.CreateWnd;
begin
  inherited;
  FDropInterface.DropTarget_Create;
end;

destructor TDropForm.Destroy;
begin
  inherited;
  FDropInterface.Free;
  FDropInterface:=nil;
end;

procedure TDropForm.DestroyWnd;
begin
  FDropInterface.DropTarget_Destroy;
  inherited;
end;

end.
