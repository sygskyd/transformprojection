unit ProcessFile;

interface
uses Windows, Main, SysUtils, TrLib32,
  Localize, Classes, TabFiles, ProcessTab, ProcessTFW;

function ProcessImageFile(Name: AnsiString): Boolean;

const
  PRIExt = '.PRI';
var
  EscapePressed: Boolean;

implementation

uses
  Forms, Math;

function ProcessImageFile(Name: AnsiString): Boolean;
var
  ImgFile: AnsiString;
  PageCnt: Integer;
begin
  Result := False;

  AddText('Process file:"' + Name + '"...');

  if not FileExists(Name) then
  begin
    AddText('...but it doesn''t exists :-(');
    Exit;
  end;

  if Lowercase(ExtractFileExt(Name)) = '.jgw' then begin
     ImgFile := ChangeFileExt(Name, JPGExt);
  end
  else begin
     ImgFile := ChangeFileExt(Name, TIFExt);

     if not FileExists(ImgFile) then begin
        ImgFile := ChangeFileExt(Name, ECWExt);
     end;

  end;

  if not FileExists(ImgFile) then
  begin
    AddText('...but the corresponding TIFF/JPEG/ECW doesn''t exists :-(');
    Exit;
  end;

  if not FileIsImageOne(ImgFile) then
  begin
    AddText('...but the corresponding TIFF/JPEG/ECW isn''t an image file');
    Exit;
  end
  else
    AddText('corresponding "' + ExtractFileName(ImgFile) + '" found...');

  PageCnt := TrLib.GetPageCount(PChar(ImgFile));
  if PageCnt > 1 then
  begin
    AddText('...but it has ' + IntToStr(PageCnt) + ' pages. This version supports only single paged TIFFs.');
    Exit;
  end;
  if CompareText(ExtractFileExt(Name), TABExt) = 0 then
    Result := ProcessTABFile(Name)
  else
    if (CompareText(ExtractFileExt(Name), TFWExt) = 0) then begin
      Result := ProcessTFWFile(Name);
    end
    else if (CompareText(ExtractFileExt(Name), JGWExt) = 0) then begin
      Result := ProcessJGWFile(Name);
    end
    else if (CompareText(ExtractFileExt(Name), SDWExt) = 0) then begin
      Result := ProcessTFWFile(Name);
    end
    else if (CompareText(ExtractFileExt(Name), EWWExt) = 0) then begin
      Result := ProcessTFWFile(Name);
    end
    else begin
      AddText('...but it hasn''t correct extension :o(');
      Exit;
    end;
  if Result then
  else
    AddText('Some error occured during TAB creation...');
end;

end.

