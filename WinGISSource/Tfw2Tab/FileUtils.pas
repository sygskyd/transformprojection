unit FileUtils;

interface
uses
  Windows, SysUtils;


const
  BAKExt = '.bak';


function DropReadOnly(Name: AnsiString): Boolean;
function MoveFileToBAK(Name: AnsiString): Boolean;

implementation

function DropReadOnly(Name: AnsiString): Boolean;
var
  Attr: DWORD;
begin
  Result := False;
  Attr :=  GetFileAttributes(PChar(Name));
  if Attr = $FFFFFFFF then                   // Some error occured
    Exit;
  if (Attr AND FILE_ATTRIBUTE_READONLY) <> 0 then
    Result := SetFileAttributes(PChar(Name), Attr AND (not FILE_ATTRIBUTE_READONLY))
  else
    Result := True; // Not READONLY attribute set
end;

{
  Function tries to move file to the BAk file
  if failed, it simply tries to delete input file
}
function MoveFileToBAK(Name: AnsiString): Boolean;
var
  Attr: DWORD;
  BAKName : AnsiString;
begin
  Result :=False;
  if not FileExists(Name) then
  begin
    Result := True;
    Exit;
  end;
  Attr := GetFileAttributes(PChar(Name));
  if Attr AND
      (FILE_ATTRIBUTE_DIRECTORY	+
       FILE_ATTRIBUTE_HIDDEN +
       FILE_ATTRIBUTE_OFFLINE +
       FILE_ATTRIBUTE_SYSTEM)
     <> 0 then // We can't delete directory/system/hidden/offline file
    Exit;
  // Make new BAK name
  BAKName := Name + BAKExt;
  // First check if future BAK file name is already occupied
  if not FileExists (BAKName) then // Try to delete it in any case
    Result := TRUE
  else
    if DropReadOnly(BAKName) then
      Result := DeleteFile(BAKName);
  Result := Result AND DropReadOnly(Name); // Hmm, too complicated statement :o?
  // Check rename ability
  if Result then
    Result := RenameFile(Name, BAKName); // So it was renamed with 'BAK' extension
  // Check result file exists
  if FileExists(Name) then // simply try to delete it
    Result := DeleteFile(Name);
  // Check ability to delete file
end;

end.
