Unit DDEDef;

Interface

Uses Messages,WinTypes,WinProcs,SysUtils,Objects,Graphics,DDEML;


Const MaxList           = 50;                   {maximale Anzahl der ID-Eintr�ge in einem DDE-String}
      ListGlobal        = 32768;                 {maximale L�nge eines DDE-Strings}
      WinGISStrFmt      = 0;                     {WinGIS-Kommando-Format}
      DesmaStrFmt       = 1;                     {Desma-Kommando-Format}
      NoConn            = 0;                     {keine Applikation verbunden}
      WgisConn          = 1;                     {Applikation mit Wingis-Kommando-Format ist verbunden}
      DesmaConn         = 2;                     {Applikation mit Desma-Kommando-Format ist verbunden}
      DBLInfoNone       = 0;                     {keine Layerinformationen senden}
      DBLInfoText       = 1;                     {Layerinformationen als Layernamen senden}
      DBLInfoIndex      = 2;                     {Layerinformationen als Layerindex senden}
      DBCBackBool       = 0;                     {CallBackProcedures arbeiten mit TRUE und FALSE}
      DBCBackDDE_       = 1;                     {CallBackProcedures arbeiten mit DDE_FACK und DDE_FNOTPROCESSED und DDE_FBUSY}
      AMFALSE           = 0;                     {keine Anfrage}
      AMDDEWM           = 1;                     {Anfrage von DDE-Verbindung}
      AMDDEML           = 2;                     {Anfrage von DDEML-Verbindung}
      WgBlockStart      = '[';                   {erstes Zeichen eines Stringblocks im Wingis-Stringformat}
      WgBlockEnd        = ']';                   {letztes Zeichen eines Stringblocks im Wingis-Stringformat}
      DmBlockStart      = '(';                   {erstes Zeichen eines Stringblocks im Desma-Stringformat}
      DmBlockEnd        = ')';                   {letztes Zeichen eines Stringblocks im Desma-Stringformat}
      IndepSepSign      = #255;                  {neutrales Trennzeichen bei verschiedenen DDE-Trennzeichen}
      DDECommAll        = 0;                     {n�chstes Kommando kann beliegiger DDE-Befehl sein}
      DDECommInit       = 1;                     {n�chste Kommandos sind DDE-Projektinitialisierungsbefehle}
      DDECommMWR        = 2;                     {n�chstes Kommando ist MainWindowReady-DDE-Befehl}
      DDECommGRD        = 3;                     {n�chstes Kommando ist GraphicReady-DDE-Befehl}
      DDECommGRDAV      = 4;                     {n�chstes Kommando ist GraphicReadyAfterView-DDE-Befehl}
      DDECommLMI        = 6;                     {n�chstes Kommando ist LayermanagerInfo nach Layerpriorit�ten-Dialog}
      DDECommSADSA      = 7;                     {[DSA] wurde gesendet, evtl. selbst�ndig [SHW] nachliefern}
      DDECommOSel       = 8;                     {n�chstes Kommando ist Monitoring im Modus OneSelect}
      DDECommAIns       = 9;                     {n�chstes Kommando ist EditInsert im Modus DBAutoInsert}
      DDECommMIns       = 10;                    {n�chstes Kommando ist Multible Insert}
      DDECommO          = 11;                    {n�chstes Kommando ist [PRW] oder [PRO]}
      DDECommS          = 12;                    {n�chstes Kommando ist [PRS]}
      DDECommSA         = 13;                    {n�chstes Kommando ist [PRW] oder [PSA]}
      DDECommN          = 14;                    {n�chstes Kommando ist [PRW] oder [PRN]}
      DDECommPInfo      = 15;                    {n�chstes Kommando ist [PRS], [PSA] oder [PCL]}
      DDECommMon        = 16;                    {n�chstes Kommando ist [MON]}
      CommChanged       = 0;                     {Kommando im Init-Mode ver�ndert}
      CommNChanged      = 1;                     {Kommando im Init-Mode nicht ver�ndert}
      CommNotSend       = 2;                     {Kommando im Init-Mode nicht senden}
      CommDDEWM         = 1;                     {Kommando kommt von der DDEWM-Verbindung}
      CommDDEML         = 2;                     {Kommando kommt von der DDEML-Verbindung}
      DDEStrUsed        = '#';                   {wird als erstes Zeichen eines DDE-Kommandos gesetzt, nachdem es verarbeitet wurde}

      cm_CGReadDDEWMIni = 3000;
      cm_CGReadDDEMLSer = 3001;
      cm_CGReadDDEMLIni = 3002;
      cm_EndDDESeq      = 3003;
      cm_EndDDEMLSeq    = 3004;
      cm_UpdateClients  = 3005;
      cm_GetNewDDEString= 3006;
      cm_SetOneSelect   = 3007;
      cm_SetAutoInsert  = 3008;
      cm_SendProjData   = 3009;
      cm_SetSelMode     = 3010;
      cm_AddDBEntry     = 3011;
      cm_WriteDDEDDEML  = 3012;
      cm_SendRDRCommADDE= 3013;
      cm_PostRDRCommADDE= 3014;
      cm_GetLayerName   = 3015;
      cm_GetTopLayerName= 3016;
      cm_SetStatusText  = 3017;
      cm_DoShowMessage  = 3018;
      cm_WriteLog       = 3019;
      cm_SetTRData      = 3020;

      rn_DBSetting      = 1176;
      rn_DBSettings     = 1177;

      di_ProjOpen       = $0001;
      di_ProjSave       = $0002;
      di_ProjSaveAs     = $0004;
      di_ProjNew        = $0008;
      di_ProjChange     = $0010;
      di_ProjClose      = $0020;
      str_Wingis        = 0;
      str_Desma         = 1;
      li_None           = 0;
      li_Text           = 1;
      li_Index          = 2;

Type PGArray            = ^TGArray;
     TGArray            = Array[0..32000] of Char;

     PLPoint            = ^LPoint;
     LPoint             = Record
       x                : Longint;
       y                : LongInt;
     end;

     PLPointArray       = ^TLPointArray;
     TLPointArray       = Array[0..127] of LPoint;

     PDBSetting         = ^TDBSetting;
     TDBSetting         = Object(TOldObject)
       DBName           : PChar;
       DBConnect        : Boolean;
       DBSend           : Boolean;
       Constructor Init(AName:PChar;AConnect,ASend:Boolean);
       Constructor InitByEntry(AEntry:PDBSetting);
       Destructor  Done; virtual;
       Constructor Load(var S:TOldStream);
       Procedure   Store(var S:TOldStream); virtual;
     end;

     PDBSettings        = ^TDBSettings;
     TDBSettings        = Object(TOldObject)
       UseGlobals       : Boolean;
       Databases        : PCollection;
       Constructor Init(AUseGlobals:Boolean);
       Constructor InitByList(AUseGlobals:Boolean;AList:PDBSettings);
       Destructor  Done; virtual;
       Constructor Load(var S:TOldStream);
       Procedure   Store(var S:TOldStream); virtual;
     end;

     PDDEApp            = ^TDDEApp;
     TDDEApp            = Object(TOldObject)
       Name             : PChar;                 {Name der Verbindung}
       ExeFile          : PChar;                 {Pfad und Name der Anwendung}
       Service          : PChar;                 {Servicename}
       Topic            : PChar;                 {Topicname}
       Menu             : Integer;               {Men�behandlung}
       Connect          : Boolean;               {Verbindung aufbauen (nur eine Verbindung m�glich!)}
       SendStrings      : Boolean;               {Kommandos senden}
       StrFormat        : Word;                  {Format der Kommandostrings}
       DBCLayerInf      : Word;                  {Layerinformationen mitsenden}
       Connected        : Boolean;               {Verbindung OK}
       Active           : Boolean;               {n�chstes Kommando senden (z.B. Antwort auf Kommando nicht an alle Anwendungen}
       Constructor Init(AName,AExeFile,AService,ATopic:PChar;AMenu:Integer;ASendStrings:Boolean;
                        AStrFormat,ADBCLayerInf:Word);
       Constructor InitByListEntry(AEntry:PDDEApp);
       Destructor  Done; virtual;
     end;

     PDDEMLApp          = ^TDDEMLApp;
     TDDEMLApp          = Object(TOldObject)
       CObjAddress      : Pointer;               {Adresse des Client-Objekts}
       Name             : PChar;                 {Name der Verbindung}
       ExeFile          : PChar;                 {Pfad und Name der Anwendung}
       StartParams      : PChar;                 {Startparameter f�r die Anwendung}
       Service          : PChar;                 {Servicename}
       Topic            : PChar;                 {Topicname}
       Item             : PChar;                 {Itemname}
       Connect          : Boolean;               {Verbindung aufbauen global}
       ConnectProj      : Boolean;               {Verbindung aufbauen f�r Projekt}
       SendStrings      : Boolean;               {Kommandos senden global}
       SendStrProj      : Boolean;               {Kommandos senden f�r Projekt}
       StrFormat        : Word;                  {Format der Kommandostrings}
       DBCLayerInf      : Word;                  {Layerinformationen mitsenden}
       CDDESepSign      : Char;                  {Trennzeichen in DDE-Strings f�r diese Verbindung}
       CCallBack        : Word;                  {R�ckgabewerte der CallBack-Procedure}
       CMWReady         : Boolean;               {MainWindowReadyMessage senden}
       CGReady          : Boolean;               {GraphicReadyMessage senden}
       CGReadyAV        : Boolean;               {GraphicReadyMessageAfterView senden}
       CDBSelMode       : Boolean;               {auf oberstem oder auf allen Layern selektieren}
       CSendLMInfo      : Boolean;               {Layerinfos nach OK im Layerdialog senden}
       CShowADSA        : Boolean;               {nach DeselectAll Grafik in den Vordergrund}
       COneSelect       : Boolean;               {max. 1 Objekt selektieren und [MON] an DB senden}
       CDBAutoIns       : Boolean;               {nach Erzeugen eines neuen Objektes automatisch [EIN] senden}
       COpenInfo        : Word;                  {neu ge�ffnetes Projekt mit [PRW] oder [PRO] bekanntgeben}
       CSaveInfo        : Word;                  {gespeichertes Projekt mit [PRS] bekanntgeben}
       CSaveAsInfo      : Word;                  {unter neuem Namen gespeichertes Projekt mit [PRW] oder [PSA] bekanntgeben}
       CNewInfo         : Word;                  {neu erzeugtes Projekt mit [PRW] oder [PRN] bekanntgeben}
       CChangeInfo      : Word;                  {Projektwechsel mit [PRW] wird nur mit Namen oder mit vollem Pfad bekanntgegeben}
       CCloseInfo       : Word;                  {[PCL] wird nur mit Namen oder mit vollem Pfad gesendet}
       HSCnv            : HConv;                 {Handle der Serverkonversation}
       Connected        : Boolean;               {Verbindung OK}
       Active           : Boolean;               {n�chstes Kommando senden (z.B. Antwort auf Kommando nicht an alle Anwendungen}
       ActChanged       : Boolean;               {Aktiv-Status wurde ge�ndert (zum selektiven Versenden an div. Anwendungen)}
       RealActive       : Boolean;               {Zwischenspeicher f�r Active wenn CommMode=DDECommInit}
       InitedProj       : HWnd;                  {letztes Projekt, f�r das Initialisierungsstrings gesendet wurden}
       SendGReady       : Boolean;               {TRUE, wenn ein evtl. folgendes [GRD]-Kommando auch gesendet werden soll}
       Constructor Init(AName,AExeFile,AStartParams,AService,ATopic,AItem:PChar;
                        AConnect,AConnectProj,ASendStrings,ASendStrsProj:Boolean;
                        AStrFormat,ADBCLayerInf:Word;ADDESepSign:Char;ACallBack:Word;
                        AMWReady,AGReady,AGReadyAV,ADBSelMode,ASendLMInfo,AShowADSA,AOneSelect,ADBAutoIns:Boolean;
                        AOpenInfo,ASaveInfo,ASaveAsInfo,ANewInfo,AChangeInfo,ACloseInfo:Word);
       Constructor InitByListEntry(AEntry:PDDEMLApp);
       Destructor  Done; virtual;
     end;

     Function  ReadDDECommandLongInt(AStr:PChar;AEntry:Integer;AIndex:Integer;AFirstChar:Char;ASep:Char;ALastChar:Char;
                                     var ResInt:LongInt;var StrFound:Boolean;var MoreStrings:Boolean):Boolean;
     Function  ReadDDECommandPChar(AStr:PChar;AEntry:Integer;AIndex:Integer;AFirstChar:Char;ASep:Char;
                                   ALastChar:Char;var ResString:PChar;var MoreStrings:Boolean):Boolean;
     Function  ReadDDECommandReal(AStr:PChar;AEntry:Integer;AIndex:Integer;AFirstChar:Char;ASep:Char;ALastChar:Char;
                                  var ResReal:Real;var StrFound:Boolean;var MoreStrings:Boolean):Boolean;
     Function  ReadDDECommandString(AStr:PChar;AEntry:Integer;AIndex:Integer;AFirstChar:Char;ASep:Char;ALastChar:Char;
                                    var ResString:String;var StrToLong:Boolean;var MoreStrings:Boolean):Boolean;


Implementation

Uses {AM_Def,}{AM_Main,}WinDOS,{AM_CPoly,}{UserIntf,}{MultiLng,}Dialogs;

{**************************************** ReadDDECommandReal ****************************************}

{***************************************************************************}
{ Function ReadDDECommandReal                                               }
{---------------------------------------------------------------------------}
{ Liest aus einem DDE-String einen Teilstring aus und wandelt diesen in     }
{ einen Real-Wert um.                                                       }
{---------------------------------------------------------------------------}
{ Parameter:                                                                }
{  AStr         Gesamtstring                                                }
{  AEntry       Block, in dem sich der Teilstring befinden soll (ab 0)      }
{  AIndex       Teilstring der ermittelt werden soll (ab 0)                 }
{  AFirstChar   erstes Zeichen eines Stringblockes (normalerweise '[')      }
{  ASep         Separationszeichen innerhalb eines Stringblockes            }
{  ALastChar    letztes Zeichen eines Stringblockes (normalerweise ']')     }
{  ResInt       ausgelesener Real-Wert                                      }
{  StrFound     TRUE, wenn ein Teilstring gefunden wurde, um eine           }
{               Umwandlung in einen Real-Wert zu versuchen                  }
{  MoreStrings  FALSE, wenn ResString letzter Teilstring eines Blockes ist  }
{               oder kein Teilstring mehr ermittelt werden konnte           }
{---------------------------------------------------------------------------}
{ Ergebnis:     TRUE, wenn ein Teilstring ermittelt werden konnte und die   }
{               Umwandlung in eine Realwert geklappt hat, sonst FALSE       }
{***************************************************************************}
Function ReadDDECommandReal
   (
   AStr            : PChar;
   AEntry          : Integer;
   AIndex          : Integer;
   AFirstChar      : Char;
   ASep            : Char;
   ALastChar       : Char;
   var ResReal     : Real;
   var StrFound    : Boolean;
   var MoreStrings : Boolean
   )
   : Boolean;
  var ValString    : String;
      TmpStrToLong : Boolean;
      TmpMoreStrs  : Boolean;
      StrOK        : Boolean;
      TempVal      : Real;
      Error        : Integer;
  begin
    ResReal:=0;
    StrFound:=FALSE;
    MoreStrings:=FALSE;
    Result:=FALSE;
    StrOK:=ReadDDECommandString(AStr,AEntry,AIndex,AFirstChar,ASep,ALastChar,ValString,TmpStrToLong,TmpMoreStrs);
    if StrOK then begin
      StrFound:=TRUE;
      MoreStrings:=TmpMoreStrs;
      if (not TmpStrToLong) and (ValString <> '') then begin
        Val(ValString,TempVal,Error);
        if Error = 0 then begin
          ResReal:=TempVal;
          Result:=TRUE;
        end;
      end;
    end;
  end;

{**************************************** ReadDDECommandLongInt ****************************************}

{***************************************************************************}
{ Function ReadDDECommandLongInt                                            }
{---------------------------------------------------------------------------}
{ Liest aus einem DDE-String einen Teilstring aus und wandelt diesen in     }
{ einen LongInt-Wert um.                                                    }
{---------------------------------------------------------------------------}
{ Parameter:                                                                }
{  AStr         Gesamtstring                                                }
{  AEntry       Block, in dem sich der Teilstring befinden soll (ab 0)      }
{  AIndex       Teilstring der ermittelt werden soll (ab 0)                 }
{  AFirstChar   erstes Zeichen eines Stringblockes (normalerweise '[')      }
{  ASep         Separationszeichen innerhalb eines Stringblockes            }
{  ALastChar    letztes Zeichen eines Stringblockes (normalerweise ']')     }
{  ResInt       ausgelesener LongInt-Wert                                   }
{  StrFound     TRUE, wenn ein Teilstring gefunden wurde, um eine           }
{               Umwandlung in einen Integer-Wert zu versuchen               }
{  MoreStrings  FALSE, wenn ResString letzter Teilstring eines Blockes ist  }
{               oder kein Teilstring mehr ermittelt werden konnte           }
{---------------------------------------------------------------------------}
{ Ergebnis:     TRUE, wenn ein Teilstring ermittelt werden konnte und die   }
{               Umwandlung in eine Integerwert geklappt hat, sonst FALSE    }
{***************************************************************************}
Function ReadDDECommandLongInt
   (
   AStr            : PChar;
   AEntry          : Integer;
   AIndex          : Integer;
   AFirstChar      : Char;
   ASep            : Char;
   ALastChar       : Char;
   var ResInt      : LongInt;
   var StrFound    : Boolean;
   var MoreStrings : Boolean
   )
   : Boolean;
  var ValString    : String;
      TmpStrToLong : Boolean;
      TmpMoreStrs  : Boolean;
      StrOK        : Boolean;
      TempVal      : LongInt;
      Error        : Integer;
  begin
    ResInt:=0;
    StrFound:=FALSE;
    MoreStrings:=FALSE;
    Result:=FALSE;
    StrOK:=ReadDDECommandString(AStr,AEntry,AIndex,AFirstChar,ASep,ALastChar,ValString,TmpStrToLong,TmpMoreStrs);
    if StrOK then begin
      StrFound:=TRUE;
      MoreStrings:=TmpMoreStrs;
      if (not TmpStrToLong) and (ValString <> '') then begin
        Val(ValString,TempVal,Error);
        if Error = 0 then begin
          ResInt:=TempVal;
          Result:=TRUE;
        end;
      end;
    end;
  end;

{**************************************** ReadDDECommandString ****************************************}

{***************************************************************************}
{ Function ReadDDECommandString                                             }
{---------------------------------------------------------------------------}
{ Liest aus einem DDE-String einen Teilstring aus.                          }
{---------------------------------------------------------------------------}
{ Parameter:                                                                }
{  AStr         Gesamtstring                                                }
{  AEntry       Block, in dem sich der Teilstring befinden soll (ab 0)      }
{  AIndex       Teilstring der ermittelt werden soll (ab 0)                 }
{  AFirstChar   erstes Zeichen eines Stringblockes (normalerweise '[')      }
{  ASep         Separationszeichen innerhalb eines Stringblockes            }
{  ALastChar    letztes Zeichen eines Stringblockes (normalerweise ']')     }
{  ResString    Teilstring als Pascal-String                                }
{  StrToLong    TRUE, wenn der String mehr als 255 Zeichen lang ist,        }
{               ResString beinhaltet nur die ersten 255 Zeichen             }
{  MoreStrings  FALSE, wenn ResString letzter Teilstring eines Blockes ist  }
{               oder kein Teilstring mehr ermittelt werden konnte           }
{---------------------------------------------------------------------------}
{ Ergebnis:     TRUE, wenn String ermittelt werden konnte, sonst FALSE      }
{***************************************************************************}
Function ReadDDECommandString
   (
   AStr            : PChar;
   AEntry          : Integer;
   AIndex          : Integer;
   AFirstChar      : Char;
   ASep            : Char;
   ALastChar       : Char;
   var ResString   : String;
   var StrToLong   : Boolean;
   var MoreStrings : Boolean
   )
   : Boolean;
  var TempStr      : PChar;
      TempMoreStrs : Boolean;
      StrOK        : Boolean;
      LongStr      : Array[0..255] of Char;
  begin
    ResString:='';
    StrToLong:=FALSE;
    MoreStrings:=FALSE;
    Result:=FALSE;
    StrOK:=ReadDDECommandPChar(AStr,AEntry,AIndex,AFirstChar,ASep,ALastChar,TempStr,TempMoreStrs);
    if StrOK then begin
      MoreStrings:=TempMoreStrs;
      if TempStr <> NIL then begin
        if StrLen(TempStr) < 256 then begin
          ResString:=StrPas(TempStr);
          Result:=TRUE;
        end
        else begin
          StrLCopy(LongStr,TempStr,255);
          ResString:=StrPas(LongStr);
          StrToLong:=TRUE;
          Result:=TRUE;
        end;
        StrDispose(TempStr);
      end
      else Result:=TRUE;
    end;
  end;

{**************************************** ReadDDECommandPChar ****************************************}

{***************************************************************************}
{ Function ReadDDECommandPChar                                              }
{---------------------------------------------------------------------------}
{ Liest aus einem DDE-String einen Teilstring aus.                          }
{---------------------------------------------------------------------------}
{ Parameter:                                                                }
{  AStr         Gesamtstring                                                }
{  AEntry       Block, in dem sich der Teilstring befinden soll (ab 0)      }
{  AIndex       Teilstring der ermittelt werden soll (ab 0)                 }
{  AFirstChar   erstes Zeichen eines Stringblockes (normalerweise '[')      }
{  ASep         Separationszeichen innerhalb eines Stringblockes            }
{  ALastChar    letztes Zeichen eines Stringblockes (normalerweise ']')     }
{  ResString    Teilstring als 0-terminierter String; dieser Parameter wird }
{               NIL, wenn kein Teilstring ermittelt werden konnte           }
{               (Result=FALSE) oder wenn der Teilstring ein Leerstring ist  }
{               (Result=TRUE)                                               }
{  MoreStrings  FALSE, wenn ResString letzter Teilstring eines Blockes ist  }
{               oder kein Teilstring mehr ermittelt werden konnte           }
{---------------------------------------------------------------------------}
{ Ergebnis:     TRUE, wenn String ermittelt werden konnte, sonst FALSE      }
{***************************************************************************}
Function ReadDDECommandPChar
   (
   AStr            : PChar;
   AEntry          : Integer;
   AIndex          : Integer;
   AFirstChar      : Char;
   ASep            : Char;
   ALastChar       : Char;
   var ResString   : PChar;
   var MoreStrings : Boolean
   )
   : Boolean;
  var TempStr      : Array[0..255] of Char;
      TempPointer  : PChar;
      i            : Integer;
      SepPos       : PChar;
      BracketPos   : PChar;
  begin
    SepPos:=NIL;
    BracketPos:=NIL;
    ResString:=NIL;
    MoreStrings:=FALSE;
    Result:=FALSE;
    if AStr = NIL then begin
      ResString:=NIL;
      Exit;
    end
    else begin
      TempPointer:=AStr;
      for i:=0 to AEntry do begin
        if TempPointer <> NIL then begin
          TempPointer:=AnsiStrScan(TempPointer,AFirstChar);
          if TempPointer <> NIL then TempPointer:=AnsiNext(TempPointer);
        end
        else begin
          ResString:=NIL;
          Exit;
        end;
      end;
      if TempPointer <> NIL then begin
        BracketPos:=AnsiStrScan(TempPointer,ALastChar);
        if BracketPos <> NIL then begin
          for i:=1 to AIndex do begin
            if TempPointer <> NIL then begin
              TempPointer:=AnsiStrScan(TempPointer,ASep);
              if TempPointer <> NIL then TempPointer:=AnsiNext(TempPointer);
            end
            else begin
              ResString:=NIL;
              Exit;
            end;
            if (TempPointer = NIL) or (TempPointer > BracketPos) then begin
              ResString:=NIL;
              Exit;
            end;
          end;
        end
        else begin
          ResString:=NIL;
          Exit;
        end;
      end
      else begin
        ResString:=NIL;
        Exit;
      end;
      if TempPointer <> NIL then begin
        SepPos:=AnsiStrScan(TempPointer,ASep);
        BracketPos:=AnsiStrScan(TempPointer,ALastChar);
        if (SepPos = NIL) or (SepPos > BracketPos) then SepPos:=BracketPos;
        if SepPos <> NIL then begin
          StrLCopy(TempStr,TempPointer,SepPos-TempPointer);
          ResString:=StrNew(TempStr);
          Result:=TRUE;
        end
        else begin
          ResString:=NIL;
          Exit;
        end;
      end;
    end;
    if SepPos <> NIL then begin
      MoreStrings:=TRUE;
      BracketPos:=AnsiStrScan(SepPos,']');
      if (BracketPos = NIL) or (BracketPos = SepPos) then MoreStrings:=FALSE                          {Eintrag ist zu Ende}
      else begin
        BracketPos:=AnsiStrScan(SepPos,ALastChar);
        if (BracketPos = NIL) or (BracketPos = SepPos) then MoreStrings:=FALSE;                       {Eintrag ist zu Ende}
      end;
    end;
  end;

{*****************************************************************************************************************************}
{**************************************** TDDEApp ****************************************************************************}
{*****************************************************************************************************************************}

Constructor TDDEApp.Init
   (
   AName           : PChar;
   AExeFile        : PChar;
   AService        : PChar;
   ATopic          : PChar;
   AMenu           : Integer;
   ASendStrings    : Boolean;
   AStrFormat      : Word;
   ADBCLayerInf    : Word
   );
  begin
    inherited Init;
    Name:=StrNew(AName);
    ExeFile:=StrNew(AExeFile);
    Service:=StrNew(AService);
    Topic:=StrNew(ATopic);
    Menu:=AMenu;
    Connect:=FALSE;
    SendStrings:=ASendStrings;
    StrFormat:=AStrFormat;
    DBCLayerInf:=ADBCLayerInf;
    Connected:=FALSE;
    Active:=FALSE;
  end;

Constructor TDDEApp.InitByListEntry
   (
   AEntry          : PDDEApp
   );
  begin
    inherited Init;
    Name:=StrNew(AEntry^.Name);
    ExeFile:=StrNew(AEntry^.ExeFile);
    Service:=StrNew(AEntry^.Service);
    Topic:=StrNew(AEntry^.Topic);
    Menu:=AEntry^.Menu;
    Connect:=AEntry^.Connect;
    SendStrings:=AEntry^.SendStrings;
    StrFormat:=AEntry^.StrFormat;
    DBCLayerInf:=AEntry^.DBCLayerInf;
    Connected:=AEntry^.Connected;
    Active:=AEntry^.Active;
  end;

Destructor TDDEApp.Done;
  begin
    StrDispose(Name);
    StrDispose(ExeFile);
    StrDispose(Service);
    StrDispose(Topic);
    inherited Done;
  end;

{*****************************************************************************************************************************}
{**************************************** TDDEMLApp **************************************************************************}
{*****************************************************************************************************************************}

Constructor TDDEMLApp.Init
   (
   AName           : PChar;
   AExeFile        : PChar;
   AStartParams    : PChar;
   AService        : PChar;
   ATopic          : PChar;
   AItem           : PChar;
   AConnect        : Boolean;
   AConnectProj    : Boolean;
   ASendStrings    : Boolean;
   ASendStrsProj   : Boolean;
   AStrFormat      : Word;
   ADBCLayerInf    : Word;
   ADDESepSign     : Char;
   ACallBack       : Word;
   AMWReady        : Boolean;
   AGReady         : Boolean;
   AGReadyAV       : Boolean;
   ADBSelMode      : Boolean;
   ASendLMInfo     : Boolean;
   AShowADSA       : Boolean;
   AOneSelect      : Boolean;
   ADBAutoIns      : Boolean;
   AOpenInfo       : Word;
   ASaveInfo       : Word;
   ASaveAsInfo     : Word;
   ANewInfo        : Word;
   AChangeInfo     : Word;
   ACloseInfo      : Word
   );
  begin
    inherited Init;
    CObjAddress:=NIL;
    Name:=StrNew(AName);
    ExeFile:=StrNew(AExeFile);
    if AStartParams <> NIL then StartParams:=StrNew(AStartParams)
    else StartParams:=NIL;
    Service:=StrNew(AService);
    Topic:=StrNew(ATopic);
    Item:=StrNew(AItem);
    Connect:=AConnect;
    ConnectProj:=AConnectProj;
    SendStrings:=ASendStrings;
    SendStrProj:=ASendStrsProj;
    StrFormat:=AStrFormat;
    DBCLayerInf:=ADBCLayerInf;
    CDDESepSign:=ADDESepSign;
    CCallBack:=ACallBack;
    CMWReady:=AMWReady;
    CGReady:=AGReady;
    CGReadyAV:=AGReadyAV;
    CDBSelMode:=ADBSelMode;
    CSendLMInfo:=ASendLMInfo;
    CShowADSA:=AShowADSA;
    COneSelect:=AOneSelect;
    CDBAutoIns:=ADBAutoIns;
    COpenInfo:=AOpenInfo;
    CSaveInfo:=ASaveInfo;
    CSaveAsInfo:=ASaveAsInfo;
    CNewInfo:=ANewInfo;
    CChangeInfo:=AChangeInfo;
    CCloseInfo:=ACloseInfo;
    HSCnv:=0;
    Connected:=FALSE;
    Active:=FALSE;
    RealActive:=FALSE;
    ActChanged:=FALSE;
    InitedProj:=0;
    SendGReady:=FALSE;
  end;

Constructor TDDEMLApp.InitByListEntry
   (
   AEntry          : PDDEMLApp
   );
  begin
    inherited Init;
    CObjAddress:=AEntry^.CObjAddress;
    Name:=StrNew(AEntry^.Name);
    ExeFile:=StrNew(AEntry^.ExeFile);
    StartParams:=StrNew(AEntry^.StartParams);
    Service:=StrNew(AEntry^.Service);
    Topic:=StrNew(AEntry^.Topic);
    Item:=StrNew(AEntry^.Item);
    Connect:=AEntry^.Connect;
    SendStrings:=AEntry^.SendStrings;
    StrFormat:=AEntry^.StrFormat;
    DBCLayerInf:=AEntry^.DBCLayerInf;
    CDDESepSign:=AEntry^.CDDESepSign;
    CCallBack:=AEntry^.CCallBack;
    CMWReady:=AEntry^.CMWReady;
    CGReady:=AEntry^.CGReady;
    CGReadyAV:=AEntry^.CGReadyAV;
    CDBSelMode:=AEntry^.CDBSelMode;
    CSendLMInfo:=AEntry^.CSendLMInfo;
    CShowADSA:=AEntry^.CShowADSA;
    COneSelect:=AEntry^.COneSelect;
    CDBAutoIns:=AEntry^.CDBAutoIns;
    COpenInfo:=AEntry^.COpenInfo;
    CSaveInfo:=AEntry^.CSaveInfo;
    CSaveAsInfo:=AEntry^.CSaveAsInfo;
    CChangeInfo:=AEntry^.CChangeInfo;
    CCloseInfo:=AEntry^.CCloseInfo;
    CNewInfo:=AEntry^.CNewInfo;
    CChangeInfo:=AEntry^.CChangeInfo;
    CCloseInfo:=AEntry^.CCloseInfo;
    HSCnv:=AEntry^.HSCnv;
    Connected:=AEntry^.Connected;
    Active:=AEntry^.Active;
    RealActive:=AEntry^.RealActive;
    ActChanged:=AEntry^.ActChanged;
    InitedProj:=AEntry^.InitedProj;
    SendGReady:=AEntry^.SendGReady;
  end;

Destructor TDDEMLApp.Done;
  begin
    StrDispose(Name);
    StrDispose(ExeFile);
    if StartParams <> NIL then StrDispose(StartParams);
    StrDispose(Service);
    StrDispose(Topic);
    StrDispose(Item);
    inherited Done;
  end;

{*****************************************************************************************************************************}
{**************************************** TDBSetting *************************************************************************}
{*****************************************************************************************************************************}

Constructor TDBSetting.Init
   (
   AName           : PChar;
   AConnect        : Boolean;
   ASend           : Boolean
   );
  begin
    Inherited Init;
    DBName:=StrNew(AName);
    DBConnect:=AConnect;
    DBSend:=ASend;
  end;

Constructor TDBSetting.InitByEntry
   (
   AEntry          : PDBSetting
   );
  begin
    Inherited Init;
    DBName:=StrNew(AEntry^.DBName);
    DBConnect:=AEntry^.DBConnect;
    DBSend:=AEntry^.DBSend;
  end;

Destructor TDBSetting.Done;
  begin
    StrDispose(DBName);
    Inherited Done;
  end;

Constructor TDBSetting.Load
   (
   var S           : TOldStream
   );
  begin
    DBName:=StrNew(S.StrRead);
    S.Read(DBConnect,SizeOf(Boolean));
    S.Read(DBSend,SizeOf(Boolean));
  end;

Procedure TDBSetting.Store
   (
   var S           : TOldStream
   );
  begin
    S.StrWrite(DBName);
    S.Write(DBConnect,SizeOf(Boolean));
    S.Write(DBSend,SizeOf(Boolean));
  end;

{*****************************************************************************************************************************}
{**************************************** TDBSettings ************************************************************************}
{*****************************************************************************************************************************}

Constructor TDBSettings.Init
   (
   AUseGlobals     : Boolean
   );
  begin
    Inherited Init;
    UseGlobals:=AUseGlobals;
    Databases:=New(PCollection,Init(3,3));
  end;

Constructor TDBSettings.InitByList
   (
   AUseGlobals     : Boolean;
   AList           : PDBSettings
   );
  Procedure DoAll
     (
     Item          : PDBSetting
     ); Far;
    begin
      Databases^.Insert(New(PDBSetting,InitByEntry(Item)));
    end;
  begin
    Inherited Init;
    UseGlobals:=AUseGlobals;
    Databases:=New(PCollection,Init(3,3));
    AList^.Databases^.ForEach(@DoAll);
  end;

Destructor TDBSettings.Done;
  begin
    Dispose(Databases,Done);
    Inherited Done;
  end;

Constructor TDBSettings.Load
   (
   var S           : TOldStream
   );
  begin
    S.Read(UseGlobals,SizeOf(Boolean));
    Databases:=New(PCollection,Load(S));
  end;

Procedure TDBSettings.Store
   (
   var S           : TOldStream
   );
  begin
    S.Write(UseGlobals,SizeOf(Boolean));
    Databases^.Store(S);
  end;

{*****************************************************************************************************************************}
{*****************************************************************************************************************************}
{*****************************************************************************************************************************}

{$IFDEF WIN32}
Const RDBSetting   : TStreamRec = (
			ObjType : rn_DBSetting;
			VmtLink : TypeOf(TDBSetting);
			Load    : @TDBSetting.Load;
			Store   : @TDBSetting.Store);

      RDBSettings  : TStreamRec = (
			ObjType : rn_DBSettings;
			VmtLink : TypeOf(TDBSettings);
			Load    : @TDBSettings.Load;
			Store   : @TDBSettings.Store);
{$ELSE}
Const RDBSetting   : TStreamRec = (
			ObjType : rn_DBSetting;
			VmtLink : Ofs(TypeOf(TDBSetting)^);
			Load    : @TDBSetting.Load;
			Store   : @TDBSetting.Store);

      RDBSettings  : TStreamRec = (
			ObjType : rn_DBSettings;
			VmtLink : Ofs(TypeOf(TDBSettings)^);
			Load    : @TDBSettings.Load;
			Store   : @TDBSettings.Store);
{$ENDIF}

begin
  RegisterType(RDBSetting);
  RegisterType(RDBSettings);
end.











