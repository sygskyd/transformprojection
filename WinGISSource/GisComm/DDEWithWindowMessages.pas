{****************************************************************************}
{ Unit AM_DDEWM                                                              }
{----------------------------------------------------------------------------}
{ - Verwaltung von DDE-Speicher                                              }
{ - generieren von DDE-Listen                                                }
{ - DDE-Initialisierung                                                      }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  16.02.1994 Martin Forst                                                   }
{  30.03.1994 Martin Forst   DDE-Initialisierung f�r User-Datenbanken        }
{  04.04.1994 Martin Forst   DDE-Initialisierung f�r User-Datenbanken        }
{****************************************************************************}
Unit DDEWithWindowMessages;

Interface

Uses Messages,Objects,WinProcs,WinTypes,DDEMain,DDEDef;

Const idt_DDEInit  =  5;

Type {**********************************************************************}
     { Object TDDEWMHandler                                                 }
     {----------------------------------------------------------------------}
     { Felder:                                                              }
     {    GlobalPtr          = Zeiger auf den DDE-Speicherblock             }
     {    GSize              = Gr��e des DDE-Speicherblocks                 }
     {    HGlobal            = Handle des DDE-Speichers                     }
     {    HServer            = Handle der Datenbank                         }
     {    HWindow            = Handle des Projektfensters                   }
     {    Position           = Aktuelle Schreibposition f�r Listen          }
     {    LString            = Command-String f�r Listen                    }
     {    LCnt               = Anzahl der Listeneintr�ge                    }
     {**********************************************************************}
     PDDEWMHandler      = ^TDDEWMHandler;
     TDDEWMHandler      = Object(TOldObject)
       AMainWindow      : HWnd;
       AppFile          : Array[0..255] of Char;
       AppName          : Array[0..255] of Char;
       TopicName        : Array[0..255] of Char;
       HServer          : Word;
       HWindow          : HWnd;
       AnswerType       : Integer;
       AckReceived      : Boolean;
       SequenceMode     : Boolean;
       Active           : Boolean;
       StrFormat        : Word;
       DBCLayerInf      : Word;
       LIFMode          : Boolean;
       RealActive       : Boolean;
       InitMode         : Boolean;
       NewInitiated     : Boolean;
       Constructor Init(AMainWin:HWnd);
       Destructor  Done; virtual;
       Procedure   DDEAcknowledge(var Msg:TMessage);
       Procedure   DDEInitiate(var Msg:TMessage);
       Procedure   DDETerminate(ServerHandle:THandle;MustBeSender:Boolean);
       Function    DDEConnectionOk:Boolean;
       Function    DDEOk:Boolean;
       Function    Execute(HGlobal:THandle;GlobalPtr:PGArray):Boolean;
       Function    GetDBLInfo:Word;
       Function    GetDBStrFormat:Word;
       Function    InitiateDDE(AWindow:THandle;LoadDatabase:Boolean):Boolean;
       Function    IsSequenceMode:Boolean;
       Function    QuestHServer:Word;
       Procedure   SendAck(AWindow:HWnd;AParam:LongInt);
       Function    SetDBSendInitStrings(SendStrings:Boolean):Boolean;
       Procedure   SetDBSendStrings(SendStrings:Boolean);
       Procedure   SetProjInited(SetInited:Boolean);
       Procedure   SetRealActive;
     end;

var DDEWMHandler   : PDDEWMHandler;

Implementation

Uses SysUtils,Forms;

Constructor TDDEWMHandler.Init
   (
   AMainWin        : HWnd
   );
  begin
    AMainWindow:=AMainWin;
    StrCopy(AppFile,'');
    StrCopy(AppName,'');
    StrCopy(TopicName,'');
    Active:=FALSE;
    RealActive:=FALSE;
    InitMode:=FALSE;
    NewInitiated:=FALSE;
    TOldObject.Init;
    HWindow:=0;
    HServer:=0;
    AnswerType:=0;
    SequenceMode:=TRUE;
    LIFMode:=FALSE;
  end;

Destructor TDDEWMHandler.Done;
  begin
    inherited Done;
  end;

Function TDDEWMHandler.Execute
   (
   HGlobal         : THandle;
   GlobalPtr       : PGArray
   )
   : Boolean;
  var WaitCount    : Integer;
      Msg          : TMsg;
      ACopyData    : TCopyDataStruct;
      FMHandle     : THandle;
      DDETempStr   : PChar;
      DDEExecStr   : PChar;
      HSendGlobal  : THandle;
      SendGlobalPtr: PChar;
  Procedure FreeGlobal;
    begin
      if HSendGlobal <> 0 then begin
        GlobalUnlock(HSendGlobal);
        GlobalFree(HSendGlobal);
      end;
      HSendGlobal:=0;
    end;
  Function GetGlobal
     (
     Size            : LongInt
     )
     : Boolean;
    begin
      HSendGlobal:=GlobalAlloc(gmem_Moveable or gmem_DDEShare,Size);
      if HSendGlobal <> 0 then begin
        SendGlobalPtr:=GlobalLock(HSendGlobal);
        if GlobalPtr = NIL then begin
          GlobalFree(HSendGlobal);
          Result:=FALSE;
        end
        else Result:=TRUE;
      end
      else Result:=FALSE;
    end;
  begin
    if (HServer <> 0) and (HGlobal <> 0) then begin
      HSendGlobal:=0;
      if DBCLayerInf <> DDEHandler^.FLayerInfo then begin
        DDETempStr:=DDEHandler^.TranslateOutStringLayerInfo(PChar(GlobalPtr),DBCLayerInf,LIFMode);
        if DDETempStr <> NIL then if DDEHandler^.IniSepSign <> DDEHandler^.FOutSepSign then
          DDEHandler^.TranslateOutStringSepSign(DDETempStr,DDEHandler^.IniSepSign);
        if DDETempStr <> NIL then if GetGlobal(StrLen(DDETempStr)+1) then StrCopy(SendGlobalPtr,DDETempStr);
      end
      else HSendGlobal:=HGlobal;
      if HSendGlobal <> 0 then begin
        if Active then begin
          AckReceived:=FALSE;
          if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLogFmt('DDE-Send : %s sent to %d.',[PChar(GlobalPtr),HServer]);
          SendMessage(HServer,wm_DDE_Execute,HWindow,HSendGlobal);
          if not AckReceived then begin
            if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLogFmt('DDE-Error: no Acknowledge received from %d.',[HServer]);
            DDEHandler^.ShowMsgBox('',11006,0);
            DDETerminate(0,FALSE);
          end;
          if DBCLayerInf <> DDEHandler^.FLayerInfo then FreeGlobal;
        end;
        Result:=TRUE;
      end
      else begin
        if LIFMode then Result:=TRUE
        else Result:=FALSE;
      end;
    end
    else Result:=FALSE;
  end;

Function TDDEWMHandler.DDEOk
   : Boolean;
  begin
    Result:=(HServer <> 0) and Active;
  end;

Function TDDEWMHandler.DDEConnectionOk
   : Boolean;
  begin
    Result:=(HServer <> 0);
  end;

Procedure TDDEWMHandler.DDEAcknowledge
   (
   var Msg         : TMessage
   );
  begin
    if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLogFmt('DDE-Ack  : received from %d',[Msg.wParam]);
    if AnswerType=wm_DDE_Initiate then HServer:=Msg.wParam;
    AnswerType:=0;
    AckReceived:=True;
  end;

Procedure TDDEWMHandler.SendAck
   (
   AWindow         : HWnd;
   AParam          : LongInt
   );                                    
  begin
    SendMessage(HServer,wm_DDE_Ack,AWindow,AParam);
  end;

Function TDDEWMHandler.QuestHServer
   : Word;
  begin
    Result:=HServer;
  end;

{**************************************** GetDBLInfo ****************************************}

Function TDDEWMHandler.GetDBLInfo
   : Word;
  begin
    Result:=DBCLayerInf;
  end;

{**************************************** GetDBStrFormat ****************************************}

Function TDDEWMHandler.GetDBStrFormat
   : Word;
  begin
    Result:=StrFormat;
  end;

{**************************************** SetProjInited ****************************************}

Procedure TDDEWMHandler.SetProjInited
   (
   SetInited       : Boolean
   );
  begin
    if SetInited then NewInitiated:=FALSE
    else NewInitiated:=TRUE;
  end;

{**************************************** SetDBSendInitStrings ****************************************}

Function TDDEWMHandler.SetDBSendInitStrings
   (
   SendStrings     : Boolean
   )
   : Boolean;
  begin
    if SendStrings then begin
      InitMode:=FALSE;
      Active:=RealActive;
      NewInitiated:=FALSE;
      Result:=FALSE;
    end
    else begin
      InitMode:=TRUE;
      RealActive:=Active;
      if not NewInitiated then begin
        Active:=FALSE;
        Result:=FALSE;
      end
      else Result:=TRUE;
    end;
  end;

{**************************************** SetDBSendStrings ****************************************}

Procedure TDDEWMHandler.SetDBSendStrings
   (
   SendStrings     : Boolean
   );
  begin
    if SendStrings then Active:=RealActive
    else begin
      RealActive:=Active;
      Active:=FALSE;
    end;
  end;

{****************************************  ****************************************}

Procedure TDDEWMHandler.SetRealActive;
  begin
    RealActive:=Active;
  end;

{**************************************** DDEInitiate ****************************************}

Procedure TDDEWMHandler.DDEInitiate
   (
   var Msg         : TMessage
   );
  var Application  : Array[0..255] of Char;
      Topic        : Array[0..255] of Char;
  begin
    GetAtomName(Msg.lParamLo,@Application,SizeOf(Application));
    GetAtomName(Msg.lParamLo,@Topic,SizeOf(Topic));
  end;

{**********************************************************************************************}
{ Function TDDEWMHandler.InitiateDDE                                                           }
{----------------------------------------------------------------------------------------------}
{ Initialisiert die DDE-Kommunikation mit der Datenbank. LoadDatabase gibt an, ob das          }
{ Datenbankmodul geladen werden soll, wenn sich die Datenbank nicht meldet.                    }
{----------------------------------------------------------------------------------------------}
{ Parameter:                                                                                   }
{  LoadDatabase    i = Datenbankmodul laden                                                    }
{----------------------------------------------------------------------------------------------}
{ Ergebnis: TRUE, wenn die Kommunikation aufgebaut werden konnte.                              }
{**********************************************************************************************}
Function TDDEWMHandler.InitiateDDE
   (
   AWindow         : THandle;
   LoadDatabase    : Boolean
   )
   : Boolean;
  var NAppFile     : Array[0..255] of Char;
      NAppName     : Array[0..255] of Char;
      NTopicName   : Array[0..255] of Char;
      NActive      : Boolean;
      AppAtom      : TAtom;
      TopicAtom    : TAtom;
      WaitCount    : Integer;
      Msg          : TMsg;
      Disabled     : Pointer;
  begin
    Result:=FALSE;
    NewInitiated:=FALSE;             
    if DDEHandler^.ReadDBWMSettings(NAppFile,NAppName,NTopicName,DDEHandler^.DBLoaded,NActive,StrFormat,DBCLayerInf) then begin
      if not InitMode then Active:=NActive;
      if (HServer = 0) or (AWindow <> HWindow) or (StrComp(NAppFile,AppFile) <> 0)
         or (StrComp(NAppName,AppName) <> 0) or (StrComp(NTopicName,TopicName) <> 0) then begin
        if HServer <> 0 then DDETerminate(HServer,FALSE);
        StrCopy(AppFile,NAppFile);
        StrCopy(AppName,NAppName);
        StrCopy(TopicName,NTopicName);            
        HWindow:=AWindow;
        AppAtom:=GlobalAddAtom(AppName);       
        TopicAtom:=GlobalAddAtom(TopicName);
        AnswerType:=wm_DDE_Initiate;
        if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLogFmt('DDE-Init : sent to %s, %s.',[AppName,TopicName]);
        SendMessage(HWnd(-1),wm_DDE_Initiate,HWindow,MakeLong(AppAtom,TopicAtom));
        if (HServer=0) and LoadDatabase and (StrComp(AppFile,'')<>0) then begin
          Disabled:=DisableTaskWindows(0);
          SendMessage(AMainWindow,wm_Command,cm_SetStatusText,4014);
          if WinExec(AppFile,sw_normal)<=32 then begin
            SendMessage(AMainWindow,wm_Command,cm_SetStatusText,0);
            EnableTaskWindows(Disabled);
            DDEHandler^.ShowMsgBox('',1004,0);
          end
          else begin
            EnableTaskWindows(Disabled);
            SetFocus(HWindow);
            SetTimer(HWindow,idt_DDEInit,500,NIL);
            WaitCount:=60;                                { 60 * 500 ms = 30 Sekunden }
            repeat
              AnswerType:=wm_DDE_Initiate;
              if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLogFmt('DDE-Init : sent to %s, %s.',[AppName,TopicName]);
              SendMessage(HWnd(-1),wm_DDE_Initiate,HWindow,MakeLong(AppAtom,TopicAtom));
              Dec(WaitCount);
              repeat
                if PeekMessage(Msg,0,0,0,pm_Remove) then begin
                  TranslateMessage(Msg);
                  DispatchMessage(Msg);
                end;
              until (HServer<>0) or ((Msg.Message=wm_Timer) and (Msg.wParam=idt_DDEInit))
                 or ((Msg.Message=wm_KeyDown) and (Msg.wParam=vk_Escape));
              if ((Msg.Message=wm_Timer) and (Msg.wParam=idt_DDEInit)) then Msg.Message:=0;
            until (HServer<>0) or (WaitCount<=0) or ((Msg.Message=wm_KeyDown) and (Msg.wParam=vk_Escape));
            KillTimer(HWindow,idt_DDEInit);
            if HServer=0 then DDEHandler^.ShowMsgBox('',3802,0);
            SendMessage(AMainWindow,wm_Command,cm_SetStatusText,0); 
          end;
        end;
        GlobalDeleteAtom(AppAtom);
        GlobalDeleteAtom(TopicAtom);
        if HServer <> 0 then NewInitiated:=TRUE;
      end;
    end
    else if HServer <> 0 then DDETerminate(HServer,FALSE);
    AnswerType:=0;
    if HServer <> 0 then begin
      DDEHandler^.SetDBWMConnected;
      Result:=TRUE;
    end
    else begin
      DDEHandler^.SetDBWMUnconnected;
      Result:=FALSE;
    end;
  end;

{**********************************************************************************************}
{ Function TDDEWMHandler.DDETerminate                                                          }
{----------------------------------------------------------------------------------------------}
{ Beendet die DDE-Kommunikation mit der Datenbank. Wenn MustBeSender TRUE ist wird der         }
{ wm_DDE_Terminate Befehl nur gesendet, wenn der Wert in Msg.wParam gleich HServer ist.        }
{ HServer wird auf seine G�ltigkeit mittels IsWindow �berpr�ft.                                }
{----------------------------------------------------------------------------------------------}
{ Parameter:                                                                                   }
{  Msg             i = Handle der Datenbank in wParam                                          }
{  MustBeSender    i = �berpr�fen, ob HServer =Msg.wParam                                      }
{**********************************************************************************************}
Procedure TDDEWMHandler.DDETerminate
   (
   ServerHandle    : THandle;
   MustBeSender    : Boolean
   );
  var Window : HWnd;
  begin
    if not(MustBeSender)
        or (ServerHandle=HServer) then begin
      Window:=HServer;
      HServer:=0;
      if IsWindow(Window) then begin
        SendMessage(Window,wm_DDE_Terminate,HWindow,0);
        if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLogFmt('DDE-Term : sent to %d.',[Window]);
      end;
    end;
  end;

Function TDDEWMHandler.IsSequenceMode
   : Boolean;
  begin
    Result:=SequenceMode;
  end;

end.

