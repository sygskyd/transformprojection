Unit DDEColl;

Interface

Uses Messages,WinTypes,WinProcs,SysUtils,Objects,Graphics;

Const MaxBlockSize      = 16380;                 {max. Eintr�ge in einer Liste}

Type PUnsortedColl      = ^TUnsortedColl;
     TUnsortedColl      = Object(TOldObject)
       Data             : PCollection;
       Constructor Init;
       Destructor  Done; virtual;
       Procedure   AtDelete(Index:LongInt);
       Procedure   AtInsert(Index:LongInt;Item:Pointer);
       Procedure   DeleteAll;
       Function    FirstThat(Action:Pointer):Pointer;
       Procedure   ForEach(Action:Pointer); virtual;
       Procedure   FreeAll;
       Function    GetCount:LongInt;
       Procedure   Insert(Item:Pointer); virtual;
       Function    LastThat(Action:Pointer):Pointer;
       Function    NewSubCollection:PCollection; virtual;
     end;

     PBigUStrColl  = ^TBigUStrColl;
     TBigUStrColl  = Object(TUnsortedColl)
       Constructor Init;
       Destructor  Done; virtual;
       Function    At(Index:LongInt):PChar;
       Procedure   Delete(Item:Pointer);
       Function    Search(Key:Pointer;var Index:LongInt):Boolean;
     end;

Implementation

Uses WinDOS;

{*****************************************************************************************************************************}
{**************************************** TUnsortedColl **********************************************************************}
{*****************************************************************************************************************************}

Constructor TUnsortedColl.Init;
  var NewColl      : PCollection;
  begin
    inherited Init;
    Data:=New(PCollection,Init(128,128));
    NewColl:=NewSubCollection;
    Data^.Insert(NewColl);
  end;

Destructor TUnsortedColl.Done;
  begin
    Dispose(Data,Done);
    inherited Done;
  end;

Procedure TUnsortedColl.Insert
   (
   Item            : Pointer
   );
  var Coll         : PCollection;
  begin
    if PCollection(Data^.At(Data^.Count-1))^.Count >= MaxBlockSize then
      Data^.Insert(NewSubCollection);
    Coll:=Data^.At(Data^.Count-1);
    Coll^.Insert(Item);
  end;

Procedure TUnsortedColl.AtInsert
   (
   Index           : LongInt;
   Item            : Pointer
   );
  var Coll         : PCollection;
      AktColl      : Integer;
  Function DoSome
     (
     Item          : PCollection
     ) : Boolean; Far;
    begin
      if AktColl > Index Div MaxBlockSize then begin
        Item^.AtInsert(0,PCollection(Data^.At(AktColl-1))^.At(MaxBlockSize-1));
        PCollection(Data^.At(AktColl-1))^.AtDelete(MaxBlockSize-1);
        Dec(AktColl);
        DoSome:=FALSE;
      end
      else DoSome:=TRUE;
    end;
  begin
    if PCollection(Data^.At(Data^.Count-1))^.Count>=MaxBlockSize then
      Data^.Insert(NewSubCollection);
    AktColl:=Data^.Count-1;
    Data^.LastThat(@DoSome);
    Coll:=Data^.At(Index Div MaxBlockSize);
    Coll^.AtInsert(Index mod MaxBlockSize,Item);
  end;

Procedure TUnsortedColl.AtDelete
   (
   Index : LongInt
   );

  var Coll : PCollection;
      AktColl : Integer;
  begin
    Coll:=Data^.At(Index Div MaxBlockSize);
    Coll^.AtDelete(Index Mod MaxBlockSize);
    AktColl:=Index div MaxBlockSize;
    while AktColl<Data^.Count-1 do begin
      PCollection(Data^.At(AktColl))^.AtInsert(MaxBlockSize-1,PCollection(Data^.At(AktColl+1))^.At(0));
      PCollection(Data^.At(AktColl+1))^.AtDelete(0);
      Inc(AktColl);
    end;
    if (Data^.Count > 1) and (PCollection(Data^.At(Data^.Count-1))^.Count = 0) then
      Data^.AtDelete(Data^.Count-1);
  end;

Procedure TUnsortedColl.DeleteAll;
  Procedure DoAll
     (
     Item          : PCollection
     ); Far;
    begin
      Item^.DeleteAll;
    end;
  begin
    Data^.ForEach(@DoAll);
    FreeAll;
  end;

Procedure TUnsortedColl.FreeAll;
  begin
    Data^.FreeAll;
    Data^.Insert(NewSubCollection);
  end;

Procedure TUnsortedColl.ForEach
   (
   Action          : Pointer
   );
  var Cnt          : Integer;
      Cnt1         : Integer;
      Coll         : PCollection;
      Item         : Pointer;
      {$IFNDEF WIN32}
      OldBP        : Integer;
      {$ENDIF}
  begin     
    {$IFNDEF WIN32}
    ASM MOV AX,[BP]
        {$IFOPT W+}
        DEC AX
        {$ENDIF}
        MOV OldBp,AX
    end;
    {$ENDIF}
    for Cnt:=0 to Data^.Count-1 do begin
      Coll:=Data^.At(Cnt);
      for Cnt1:=0 to Coll^.Count-1 do begin
        Item:=Coll^.At(Cnt1);
        {$IFDEF WIN32}
        ASM
          MOV EAX,Item
          PUSH DWORD PTR [EBP]
          CALL DWORD PTR Action
          ADD ESP,4
        end;
        {$ELSE}
        TItemProc(Action)(Item,OldBP);
        {$ENDIF}
      end;
    end;
  end;

Function TUnsortedColl.FirstThat
   (
   Action          : Pointer
   )
   : Pointer;
  var Cnt          : Integer;
      Cnt1         : Integer;
      Coll         : PCollection;
      Item         : Pointer;
      {$IFNDEF WIN32}
      OldBP        : Integer;
      {$ENDIF}
  begin     
    {$IFNDEF WIN32}
    ASM MOV AX,[BP]
        {$IFOPT W+}
        DEC AX
        {$ENDIF}
        MOV OldBp,AX
    end;
    {$ENDIF}
    Result:=NIL;
    for Cnt:=0 to Data^.Count-1 do begin
      Coll:=Data^.At(Cnt);
      for Cnt1:=0 to Coll^.Count-1 do begin
        Item:=Coll^.At(Cnt1);
        {$IFDEF WIN32}
        ASM
          MOV EAX,Item
          PUSH DWORD PTR [EBP]
          CALL DWORD PTR Action
          ADD ESP,4
          CMP AL,0
          JE @@1
          MOV EAX,Item
          MOV @Result,EAX
        @@1:
        end;
        {$ELSE}
        if TItemFunc(Action)(Item,OldBP) then Result:=Item;
        {$ENDIF}
        if Result<>NIL then Exit;
      end;
    end;
  end;

Function TUnsortedColl.LastThat
   (
   Action          : Pointer
   )
   : Pointer;
  var Cnt          : Integer;
      Cnt1         : Integer;
      Coll         : PCollection;
      Item         : Pointer;
      {$IFNDEF WIN32}
      OldBP        : Integer;
      {$ENDIF} 
  begin
    {$IFNDEF WIN32}
    ASM MOV AX,[BP]
        {$IFOPT W+}
        DEC AX
        {$ENDIF}
        MOV OldBp,AX
    end;
    {$ENDIF}
    Result:=NIL;
    for Cnt:=Data^.Count-1 downto 0 do begin
      Coll:=Data^.At(Cnt);
      for Cnt1:=Coll^.Count-1 downto 0 do begin
        Item:=Coll^.At(Cnt1);
        {$IFDEF WIN32}
        ASM
          MOV EAX,Item
          PUSH DWORD PTR [EBP]
          CALL DWORD PTR Action
          ADD ESP,4
          CMP AL,0
          JE @@1
          MOV EAX,Item
          MOV @Result,EAX
        @@1:
        end;
        {$ELSE}
        if TItemFunc(Action)(Item,OldBP) then Result:=Item;
        {$ENDIF}
        if Result<>NIL then Exit;  
      end;
    end;
  end;

Function TUnsortedColl.GetCount
   : LongInt;
  begin
    GetCount:=LongInt(Data^.Count-1)*MaxBlockSize+PCollection(Data^.At(Data^.Count-1))^.Count;
  end;

Function TUnsortedColl.NewSubCollection
   : PCollection;
  begin
    NewSubCollection:=New(PCollection,Init(4096,1024));
  end;

{*****************************************************************************************************************************}
{**************************************** TUnsortedColl **********************************************************************}
{*****************************************************************************************************************************}

Constructor TBigUStrColl.Init;
  begin
    inherited Init;
  end;

Function TBigUStrColl.At
   (
   Index : LongInt
   ) : PChar;
  var Coll : PCollection;
  begin
    Coll:=Data^.At(Index div MaxBlockSize);
    At:=Coll^.At((Index mod MaxBlockSize));
  end;

Destructor TBigUStrColl.Done;
  begin
    inherited Done;
  end;

Procedure TBigUStrColl.Delete
   (
   Item : Pointer
   );
  var Coll       : PCollection;
      IndexCount : Integer;
  Function DoSome
     (
     Item2 : PCollection
     ) : Boolean; Far;
    Function FindIndIndex
       (
       Item3 : PChar
       ) : Boolean; Far;
      begin
        Inc(IndexCount);
        FindIndIndex:=StrComp(Item3,Item) = 0;
      end;
    var FoundItem : PChar;
    begin
      IndexCount:=-1;
      FoundItem:=Item2^.FirstThat(@FindIndIndex);
      DoSome:=FoundItem <> NIL;
    end;
  begin
    Coll:=Data^.FirstThat(@DoSome);
    if Coll <> NIL then AtDelete(Data^.IndexOf(Coll)*LongInt(MaxBlockSize)+LongInt(IndexCount));
  end;

Function TBigUStrColl.Search
   (
   Key             : Pointer;
   var Index       : LongInt
   ) : Boolean;
  var Coll         : PCollection;
  Function DoSome
     (
     Item2 : PCollection
     ) : Boolean; Far;
    var FoundItem  : PChar;
        IndexCount : Integer;
    Function FindIndIndex
       (
       Item3 : PChar
       ) : Boolean; Far;
      begin
        Inc(IndexCount);
        FindIndIndex:=StrComp(Item3,PChar(Key)) = 0;
      end;
    begin
      IndexCount:=-1;
      FoundItem:=Item2^.FirstThat(@FindIndIndex);
      if FoundItem = NIL then Inc(Index,MaxBlockSize)
      else Inc(Index,IndexCount);
      DoSome:=FoundItem <> NIL;
    end;
  begin
    Index:=0;
    Coll:=Data^.FirstThat(@DoSome);
    if Coll = NIL then Search:=FALSE
    else Search:=TRUE;
  end;

end.











