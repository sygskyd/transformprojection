{****************************************************************************}
{ Unit AM_DDECS                                                              }
{----------------------------------------------------------------------------}
{ - DDEML-Client-Server-Kommunikation                                        }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{  15.06.1997 Heinz Ofner                                                    }
{****************************************************************************}
Unit DDEWithDDEML;


// changed by AS
{$DEFINE COM}   // uncomment this to compile with COM support


Interface

Uses Objects,Messages,WinTypes,WinProcs,DDEML,DDEMain,Controls,DDEDef,Classes
{$IFDEF COM}
     ,AXIpc,Forms
{$ENDIF}
     ,RegUtils
     ;

Type PServerCnv         = ^TServerCnv;
     TServerCnv         = Record
       HSCnv            : HConv;
     end;
     // changed by AS
     // stores a Command (string, Conv, ..) for Wingis to fetch
     PTSaveCmdItem      = ^TSaveCmdItem;
     TSaveCmdItem       = record
       SaveHSZCTopic    : HSZ;
       SaveAStr         : PChar;
       SaveACnv         : PChar;
     end;

     PDDEMLServer       = ^TDDEMLServer;
     TDDEMLServer       = Object(TOldObject)
       Constructor Init(AWindow:HWnd);
       Destructor  Done; virtual;
       Function    DDEOk:Boolean;
       Function    DoExecute(HSZClientTopic,HSZClientItem:HSZ;AStr:PChar;ACnv:HConv):Boolean;
       Function    EnableDDEConversation(AEnable:Boolean):Boolean;
       Procedure   EnableDDEServerConversation(AEnable:Boolean);
       Function    InitiateDDE:Boolean;
       Procedure   TerminateDDE;
       Procedure   SetActualChild(AChild:HWnd);
       Procedure   ServerDisconnect(DelCnv:HConv);
     private
       AMainWindow      : HWnd;
       ActualChild      : HWnd;
       StrSService      : Array[0..255] of Char;
       StrSTopic        : Array[0..255] of Char;
       StrSItem         : Array[0..255] of Char;
       HSCnv            : HConv;
       HSZSService      : HSZ;
       HSZSTopic        : HSZ;
       HSZSItem         : HSZ;
       PSPrcCallBack    : ^TFNCallBack;
       ServerChange     : Boolean;
       CnvList          : PCollection;
       StrFormat        : Word;
       EndMode          : Boolean;
       DBSLayerInfo     : Word;
       GeoRef           : PLPointArray;
       TRConv           : HConv;
       SaveHSZCTopic    : HSZ;
       SaveAStr         : PChar;
       SaveACnv         : PChar;
       tlSaveCmdList    : TList;       // contains cmds to be delivered to wingis
       tlDeleteCmdList  : TList;       // contains cmds already delivered to wingis (items to be deleted)

       Function    InitiateServerDDE:Boolean;
       Function    AcceptExecute(HSZClientTopic,HSZClientItem:HSZ;HData:HDDEData;Fmt:Word;ACnv:HConv):Boolean; virtual;
       Function    DDECreateString(DDEInst:LongInt;PSZString:PChar):HSZ;
       Procedure   DDEFreeString(DDEInst:LongInt;var HSZStr:HSZ);
       Procedure   GetStringTypeAndLayerInfo(AHSCnv:HConv);
       Function    MatchTopicAndService(HSZClientTopic:HSZ;HSZClientService:HSZ):Boolean;
       Function    RegisterClient(HSZClientTopic:HSZ;HSZClientService:HSZ;NewCnv:HConv):Boolean;
       Function    RegisterClientInfo(DDEString:PChar;AStrFmt:Word;var AStartDBLInf:Word;var AStartCBack:Word):Boolean;
       Function    RegisterClientInfoIPC(DDEString:PChar;AStrFmt:Word;var AStartDBLInf:Word;var AStartCBack:Word;lSession:longint):Boolean;
       Function    Request(HSZClientTopic,HSZClientItem:HSZ;HData:HDDEData;Fmt:Word;ACnv:HConv):HDDEData; virtual;
     public
       SDDEInst         : LongInt;
       SBusy            : Boolean;
       SCallBack        : Word;
       Req_pSerial      : PChar;
       Req_pModCod      : PChar;
       Req_pLizence     : PChar;
       Req_im_TR        : PChar;
       Procedure   SendRefPoints(GeoReference:PLPointArray);
       Procedure   GetDDECommand(var AHSZSTopic:HSZ;var AHSZClientTopic:HSZ;var AStr:PChar;var ACnv:PChar);

{$IFDEF COM}
    public
       // Ipc members
       //AXIpc: TAXIpcWrapper;
       procedure OnIpcReceive(const sCmd: WideString; iSessionID: Integer);   // callback
       procedure OnIpcConnect(lSessionID: Integer; const sRemoteName: WideString);
       procedure OnIpcDisconnect(lSessionID: Integer);


       Function RegisterClientIPC(sClientName: string; iNewCnv: integer): Boolean;
{$ENDIF}
     end;


{*****************************************************************************************************************************}

Type PDDEMLClient       = ^TDDEMLClient;
     TDDEMLClient       = Object(TOldObject)
       HCCnv            : HConv;
       Constructor Init(AWindow:HWnd;AStringFormat,ADBLayerInfo:Word;ASepSign:Char;ACallBack:Word;lSession:longint = 0);
       Destructor  Done; virtual;
       Function    DDEOk:Boolean;
       Function    Execute(HGlobal:THandle;GlobalPtr:PGArray;lSession: longint = 0):Boolean;
       Function    InitiateClientDDE(AName,ExeFile,StartPars,Service,Topic,Item:PChar; lSession: longint = 0):Boolean;
       Procedure   TerminateDDE;
       Procedure   UpdateConnectionSettings(AStringFormat:Word;ADBLayerInfo:Word;ASepSign:Char;ACallBack:Word);
       Procedure   ClientDisconnect;
     private
       AMainWindow      : HWnd;
       Name             : Array[0..255] of Char;
       ApplicFile       : Array[0..255] of Char;
       StartParams      : Array[0..255] of Char;
       StrCService      : Array[0..255] of Char;
       StrCTopic        : Array[0..255] of Char;
       StrCItem 	      : Array[0..255] of Char;
       HSZCService      : HSZ;
       HSZCTopic        : HSZ;
       HSZCItem         : HSZ;
       CDDEInst         : LongInt;
       PCPrcCallBack    : ^TFNCallBack;
       StrFormat        : Word;
       DBCLayerInfo     : Word;
       CSepSign         : Char;
       LIFMode          : Boolean;
       CCallBack        : Word;

       FSession         : longint;
       lAckTimeout      : longint;

       Function    ClientConnect:Boolean;
       Function    DDECreateString(DDEInst:LongInt;PSZString:PChar):HSZ;
       Procedure   DDEFreeString(DDEInst:LongInt;var hszStr:HSZ);
       Function    ExecuteString(AStr:PChar; lSession: longint = 0):Boolean;
     public
       { Public declarations }

     end;

var DDEMLServer    : PDDEMLServer;
{$IFDEF COM}
    FAXIpc         : TAXIpcWrapper;
{$ENDIF}

{*****************************************************************************************************************************}
{*****************************************************************************************************************************}
{*****************************************************************************************************************************}

Implementation

Uses SysUtils, GisCommMain;

{*****************************************************************************************************************************}
{**************************************** DDEML-Server ***********************************************************************}
{*****************************************************************************************************************************}

{**************************************** OnIpcConnect *****************************************************************}

{$IFDEF COM}
procedure TDDEMLServer.OnIpcConnect(lSessionID: Integer; const sRemoteName: WideString);
begin


{$IFDEF LOG}
    WriteLog('OnIpcConnect');
    if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('IPCServer - OnConnect');
{$ENDIF}

   DDEMLServer^.HSCnv:=lSessionID;
   DDEMLServer^.RegisterClientIPC(sRemoteName,lSessionID);
end;

procedure TDDEMLServer.OnIpcDisconnect(lSessionID: Integer);
  var DelCnvData    : PServerCnv;
  Function SearchCnv
     (
     Item           : PServerCnv
     )
     : Boolean; Far;
    begin
      Result:=Item^.HSCnv = lSessionID;
    end;
  begin
{$IFDEF LOG}
    if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('IPCServer - OnDisconnect');
{$ENDIF}
    WriteLog('OnIpcDisconnect');

    if Assigned(CnvList) then begin
       DelCnvData:=CnvList^.FirstThat(@SearchCnv);
       if DelCnvData <> NIL then begin
         CnvList^.Delete(DelCnvData);
         Dispose(DelCnvData);
       end;
    end;
    DDEMLServer^.HSCnv:=lSessionID;
    DDEMLServer^.ServerDisconnect(lSessionID);
    DDEHandler^.UnConnect(lSessionID);

end;
{$ENDIF}

{**************************************** CallbackProcServer *****************************************************************}

Function CallbackProcServer
   (
   CallType        : UINT;
   Fmt             : UINT;
   Conv            : HConv;
   HSZ1            : HSZ;
   HSZ2            : HSZ;
   Data            : HDDEData;
   Data1           : DWORD;
   Data2           : DWORD
   )
   : HDDEData; stdcall;
  var cItem        : Array[0..32] of Char;
      hItem        : HSZ;
      cbOff        : DWord;
  begin
    Result:=0;

{$IFDEF LOG}
    if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcServer - Start');
{$ENDIF}
    if DDEMLServer^.SBusy then begin
      if DDEMLServer^.SCallBack = DBCBackBool then Result:=0
      else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FBUSY;
    end
    else begin
      DDEMLServer^.SBusy:=TRUE;
      {DDEMLServer^.EnableDDEServerConversation(TRUE);}
      if DDEMLServer^.SCallBack = DBCBackBool then Result:=0
      else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FNOTPROCESSED;
      case CallType of
        xtyp_AdvReq            : begin
{$IFDEF LOG}
                                   if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcServer - xtyp_AdvReq');
{$ENDIF}
                                   StrCopy(cItem,'AdvRefPoints');
                                   cbOff:=0;
                                   hItem:=DDECreateStringHandle(DDEMLServer^.SDDEInst,cItem,CP_WINANSI);
                                   Result:=DDECreateDataHandle(DDEMLServer^.SDDEInst,DDEMLServer^.GeoRef,SizeOf(LPoint)*128,
                                                               cbOff,hItem,CF_TEXT,0);
                                   DDEFreeStringHandle(DDEMLServer^.SDDEInst,hItem);
                                 end;
        xtyp_AdvStart          : begin
{$IFDEF LOG}
                                   if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcServer - xtyp_AdvStart');
{$ENDIF}
                                   if Conv = DDEMLServer^.TRConv then begin
                                     if DDEMLServer^.SCallBack = DBCBackBool then Result:=1
                                     else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FACK;
                                   end
                                   else begin
                                     if DDEMLServer^.SCallBack = DBCBackBool then Result:=0
                                     else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FNOTPROCESSED;
                                   end;
                                 end;
        xtyp_AdvStop           : begin
{$IFDEF LOG}
                                   if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcServer - xtyp_AdvStop');
{$ENDIF}
                                   if Conv = DDEMLServer^.TRConv then begin
                                     if DDEMLServer^.SCallBack = DBCBackBool then Result:=1
                                     else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FACK;
                                   end
                                   else begin
                                     if DDEMLServer^.SCallBack = DBCBackBool then Result:=0
                                     else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FNOTPROCESSED;
                                   end;
                                 end;
        xtyp_Connect           : begin
{$IFDEF LOG}
                                   if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcServer - xtyp_Connect');
{$ENDIF}
                                   if DDEMLServer^.SCallBack = DBCBackBool then Result:=0
                                   else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FNOTPROCESSED;
                                   if Conv = 0 then begin
                                     if DDEMLServer^.MatchTopicAndService(HSZ1,HSZ2) then begin
                                       if DDEMLServer^.SCallBack = DBCBackBool then Result:=1
                                       else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FACK;
{$IFDEF LOG}
                                       WriteLog('Server - Connect');
{$ENDIF}
                                     end;
                                   end;
                                 end;
        xtyp_Connect_Confirm   : begin
{$IFDEF LOG}
                                   if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcServer - xtyp_Connect_Confirm');
{$ENDIF}
{$IFDEF LOG}
                                   WriteLog('Server - Connect_confirm');
{$ENDIF}

                                   DDEMLServer^.HSCnv:=Conv;
                                   DDEMLServer^.RegisterClient(HSZ1,HSZ2,Conv);
                                   if DDEMLServer^.SCallBack = DBCBackBool then Result:=1
                                   else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FACK;
                                 end;
        xtyp_Disconnect        : begin
{$IFDEF LOG}
                                   if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcServer - xtyp_Disconnect');
{$ENDIF}
                                   DDEMLServer^.HSCnv:=Conv;
                                   DDEMLServer^.ServerDisconnect(Conv);
                                   if DDEMLServer^.SCallBack = DBCBackBool then Result:=1
                                   else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FACK;
                                 end;
        xtyp_Error             : begin
{$IFDEF LOG}
                                   if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcServer - xtyp_Error');
{$ENDIF}
                                   if DDEMLServer^.SCallBack = DBCBackBool then Result:=0
                                   else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FNOTPROCESSED;
                                 end;
        xtyp_Execute           : begin
{$IFDEF LOG}
                                   if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcServer - xtyp_Execute');
{$ENDIF}
                                   if DDEMLServer^.HSCnv <> Conv then begin
                                     DDEMLServer^.HSCnv:=Conv;
                                     DDEMLServer^.ServerChange:=TRUE;
                                   end
                                   else DDEMLServer^.ServerChange:=FALSE;
                                   if DDEMLServer^.AcceptExecute(HSZ1,HSZ2,Data,Fmt,Conv) then begin
                                     if DDEMLServer^.SCallBack = DBCBackBool then Result:=1
                                     else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FACK;
                                   end
                                   else begin
                                     if DDEMLServer^.SCallBack = DBCBackBool then Result:=0
                                     else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FNOTPROCESSED;
                                   end;
                                 end;
        xtyp_Poke              : begin
{$IFDEF LOG}
                                   if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcServer - xtyp_Poke');
{$ENDIF}
                                   if DDEMLServer^.SCallBack = DBCBackBool then Result:=0
                                   else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FNOTPROCESSED;
                                 end;
        xtyp_Register          : begin
{$IFDEF LOG}
                                   if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcServer - xtyp_Register');
{$ENDIF}
                                   if DDEMLServer^.SCallBack = DBCBackBool then Result:=0
                                   else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FNOTPROCESSED;
                                 end;
        xtyp_Request           : begin
{$IFDEF LOG}
                                   if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcServer - xtyp_Request');
{$ENDIF}
                                   if DDEMLServer^.HSCnv <> Conv then begin
                                     DDEMLServer^.HSCnv:=Conv;
                                     DDEMLServer^.ServerChange:=TRUE;
                                   end
                                   else DDEMLServer^.ServerChange:=FALSE;
                                   Result:=DDEMLServer^.Request(HSZ1,HSZ2,Data,Fmt,Conv);
                                 end;
        xtyp_Unregister        : begin
{$IFDEF LOG}
                                   if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcServer - xtyp_Unregister');
{$ENDIF}
                                   if DDEMLServer^.SCallBack = DBCBackBool then Result:=0
                                   else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FNOTPROCESSED;
                                 end;
        xtyp_WildConnect       : begin
{$IFDEF LOG}
                                   if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcServer - xtyp_WildConnect');
{$ENDIF}
                                   if DDEMLServer^.SCallBack = DBCBackBool then Result:=0
                                   else if DDEMLServer^.SCallBack = DBCBackDDE_ then Result:=DDE_FNOTPROCESSED;
                                 end;

      end;
      DDEMLServer^.SBusy:=FALSE;
      {DDEMLServer^.EnableDDEServerConversation(FALSE);}
    end;
{$IFDEF LOG}
    if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcServer - End');
{$ENDIF}
  end;

{**************************************** Init *******************************************************************************}

Constructor TDDEMLServer.Init
   (
   AWindow         : HWnd
   );
  begin
{$IFDEF COM}
    //PAXIpc := nil;
{$ENDIF}
    Inherited Init;
    AMainWindow:=AWindow;
    ActualChild:=0;
    PSPrcCallBack:=NIL;
    SDDEInst:=0;
    HSCnv:=0;
    CnvList:=nil;
    CnvList:=New(PCollection,Init(3,3));
    ServerChange:=TRUE;
    DBSLayerInfo:=DBLInfoNone;
    GeoRef:=NIL;
    TRConv:=0;
    SBusy:=FALSE;
    SCallBack:=DBCBackBool;
    Req_pSerial:=NIL;
    Req_pModCod:=NIL;
    Req_pLizence:=NIL;
    Req_im_TR:=NIL;

    // changed by AS
    tlSaveCmdList := TList.Create;
    tlDeleteCmdList := TList.Create;

{$IFDEF COM}
    //New(pAXIpc);
    Application.CreateForm(TAXIpcWrapper, FAXIpc);

    FAXIpc.FOnIpcReceive := OnIpcReceive;
    FAXIpc.FOnIpcConnect := OnIpcConnect;
    FAXIpc.FOnIpcDisconnect := OnIpcDisconnect;
{$ENDIF}
  end;

{**************************************** Done *******************************************************************************}

Destructor TDDEMLServer.Done;
var
  pTItem: PTSaveCmdItem;
  iListIndex: integer;
  begin
    if Req_im_TR <> NIL then StrDispose(Req_im_TR);
    if Req_pLizence <> NIL then StrDispose(Req_pLizence);
    if Req_pModCod <> NIL then StrDispose(Req_pModCod);
    if Req_pSerial <> NIL then StrDispose(Req_pSerial);
    HSCnv:=0;
    SDDEInst:=0;
    PSPrcCallBack:=NIL;
    ActualChild:=0;
    AMainWindow:=0;

{$IFDEF COM}
    FAXIpc.Disconnect;
    FAXIpc.Close;
    FAXIpc.Free;
{$ENDIF}

    { Cleanup: Free Listitems and the list itself }
    for iListIndex := 0 to (tlSaveCmdList.Count - 1) do
    begin
       pTItem := tlSaveCmdList.Items[iListIndex];

       Dispose(pTItem);
    end;
    tlSaveCmdList.Free;

    { Cleanup: Free Listitems and the list itself }
    for iListIndex := 0 to (tlDeleteCmdList.Count - 1) do
    begin
       pTItem := tlDeleteCmdList.Items[iListIndex];

       Dispose(pTItem);
    end;
    tlDeleteCmdList.Free;
    Inherited Done;
  end;

{**************************************** InitiateDDE ************************************************************************}

Function TDDEMLServer.InitiateDDE
   : Boolean;
  begin
    Result:=TRUE;
    if not InitiateServerDDE then begin
      DDEHandler^.ShowMsgBox('',11600,0);
      Result:=FALSE;
    end;

{$IFDEF COM}
    // initiate IPC server
    FAXIpc.Listen;
{$IFDEF LOG}
    if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('IPCServer - Listen');
{$ENDIF}
{$ENDIF}
  end;

{**************************************** InitiateServerDDE ******************************************************************}

Function TDDEMLServer.InitiateServerDDE
   : Boolean;
  var ulRes        : DWord;
      hsz2         : DWord;
  begin
    Result:=FALSE;
    if HSCnv = 0 then begin
      SDDEInst:=0;
      PSPrcCallBack:=MakeProcInstance(@CallBackProcServer,HInstance);
      ulRes:=0;
      if DdeInitialize(SDDEInst,TFNCallback(PSPrcCallBack),APPCLASS_STANDARD,ulRes) <> dmlErr_No_Error then
        DDEHandler^.ShowMsgBox('',11601,0)
      else begin
        DDEHandler^.ReadDDEMLWingisServerSettings(StrSService,StrSTopic,StrSItem);
        HSZSService:=DdeCreateString(SDDEInst,StrSService);
        HSZSTopic:=DdeCreateString(SDDEInst,StrSTopic);
        HSZSItem:=DdeCreateString(SDDEInst,StrSItem);
        hsz2:=0;
        if DdeNameService(SDDEInst,HSZSService,hsz2,dns_Register) = 0 then DDEHandler^.ShowMsgBox('',11602,0)
        else Result:=TRUE;
      end;
    end
    else Result:=TRUE;
  end;

{**************************************** TerminateDDE ****************************************}

Procedure TDDEMLServer.TerminateDDE;
  var hsz2         : DWord;
  begin
{$IFDEF LOG}
    if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('Server - TerminatDDE');
{$ENDIF}
    while CnvList^.Count > 0 do ServerDisconnect(PServerCnv(CnvList^.At(0))^.HSCnv);
    Dispose(CnvList,Done);
    CnvList:=nil;
    if SDDEInst <> 0 then begin
      hsz2:=0;
      if DDENameService(SDDEInst,HSZSService,hsz2,dns_Unregister) = 0 then DDEHandler^.ShowMsgBox('',11603,0);
    end;
    DdeFreeString(SDDEInst,HSZSService);
    DdeFreeString(SDDEInst,HSZSTopic);
    DdeFreeString(SDDEInst,HSZSItem);
    SDDEInst:=0;
    if PSPrcCallBack <> NIL then begin
      FreeProcInstance(PSPrcCallBack);
      PSPrcCallBack:=NIL;
    end;

{$IFDEF COM}
    // Close all IPC sessions
    FAXIpc.Disconnect;
{$ENDIF}    
  end;

{**************************************** MatchTopicAndService ****************************************}

Function TDDEMLServer.MatchTopicAndService
   (
   HSZClientTopic   : HSZ;
   HSZClientService : HSZ
   )
   : Boolean;
  begin
    Result:=(DdeCmpStringHandles(HSZSTopic,HSZClientTopic) = 0) and (DdeCmpStringHandles(HSZSService,HSZClientService) = 0);
  end;

{**************************************** RegisterClientInfo ****************************************}

Function TDDEMLServer.RegisterClientInfo
   (
   DDEString        : PChar;
   AStrFmt          : Word;
   var AStartDBLInf : Word;
   var AStartCBack  : Word
   )
   : Boolean;
  begin
    Result:=DDEHandler^.RegisterDBClientInfo(DDEString,HSCnv,AStrFmt,AStartDBLInf,AStartCBack);
  end;

Function TDDEMLServer.RegisterClientInfoIPC
   (
   DDEString        : PChar;
   AStrFmt          : Word;
   var AStartDBLInf : Word;
   var AStartCBack  : Word;
   lSession         : longint
   )
   : Boolean;
  begin
    Result:=DDEHandler^.RegisterDBClientInfo(DDEString,HSCnv,AStrFmt,AStartDBLInf,AStartCBack,lSession);
  end;

{**************************************** RegisterClient ****************************************}

Function TDDEMLServer.RegisterClient
   (
   HSZClientTopic   : HSZ;
   HSZClientService : HSZ;
   NewCnv           : HConv
   )
   : Boolean;
  var NewCnvData    : PServerCnv;
  begin
    Result:=FALSE;
    if (HSZClientService = HSZSService) and (HSZClientTopic = HSZSTopic) then begin
      NewCnvData:=New(PServerCnv);
      NewCnvData^.HSCnv:=HSCnv;
      CnvList^.Insert(NewCnvData);
      GetStringTypeAndLayerInfo(NewCnv);
      Result:=TRUE;
    end;
  end;

{$IFDEF COM}
Function TDDEMLServer.RegisterClientIPC
   (
   sClientName      : string;
   iNewCnv          : integer
   )
   : Boolean;
  var NewCnvData    : PServerCnv;
  begin

      NewCnvData:=New(PServerCnv);
      NewCnvData^.HSCnv:=iNewCnv;
      CnvList^.Insert(NewCnvData);
      GetStringTypeAndLayerInfo(iNewCnv);
      Result := TRUE;

  end;
{$ENDIF}
{**************************************** EnableDDEServerConversation ****************************************}

Procedure TDDEMLServer.EnableDDEServerConversation
   (
   AEnable         : Boolean
   );
{$IFDEF LOG}
  var Tmp          : String;
{$ENDIF}
  begin
(*
{$IFDEF LOG}
    if DDEHandler^.WriteToLogFile then begin
      Str(SBusy:0,Tmp);
      if AEnable then Tmp:='EnableDDEServerConversation Begin - '+Tmp
      else Tmp:='DisableServerConversation - '+Tmp;
      DDEHandler^.WriteLog(Tmp);
    end;
{$ENDIF}
    if AEnable then begin
      if SBusy > 0 then Dec(SBusy);
      {if DDEDataRecv = 0 then SendString('[SOK][1]');}
    end
    else begin
      Inc(SBusy);
      {if DDEDataRecv = 1 then SendString('[SOK][0]');}
    end;
{$IFDEF LOG}
    if DDEHandler^.WriteToLogFile then begin
      Str(SBusy:0,Tmp);
      if AEnable then Tmp:='EnableDDEServerConversation - '+Tmp
      else Tmp:='DisableDDEServerConversation End - '+Tmp;
      DDEHandler^.WriteLog(Tmp);
    end;
{$ENDIF}
*)
  end;


{**************************************** EnableDDEConversation ****************************************}

Function TDDEMLServer.EnableDDEConversation
   (
   AEnable         : Boolean
   )
   : Boolean;
  var Action       : Word;
      AConv        : HConv;
  begin
    if AEnable then Action:=EC_ENABLEALL
    else Action:=EC_DISABLE;
    {EnableDDEServerConversation(AEnable);}
    AConv:=0;
    Result:=DdeEnableCallback(SDDEInst,AConv,Action);
  end;

{**************************************** ServerDisconnect ****************************************}

Procedure TDDEMLServer.ServerDisconnect
   (
   DelCnv           : HConv
   );
  var DelCnvData    : PServerCnv;
  Function SearchCnv
     (
     Item           : PServerCnv
     )
     : Boolean; Far;
    begin
      Result:=Item^.HSCnv = DelCnv;
    end;
  begin
{$IFDEF LOG}
    if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('Server - ServerDisconnect');
{$ENDIF}
    if Assigned(CnvList) then begin
       DelCnvData:=CnvList^.FirstThat(@SearchCnv);
       if DelCnvData <> NIL then begin
         CnvList^.Delete(DelCnvData);
         Dispose(DelCnvData);
       end;
    end;
    DDEDisconnect(DelCnv);
    DDEHandler^.UnConnect(DelCnv);

  end;

{**************************************** DDEOk ****************************************}

Function TDDEMLServer.DDEOk
   : Boolean;
  begin
    Result:=SDDEInst <> 0;
  end;

{**************************************** DDECreateString ****************************************}

Function TDDEMLServer.DDECreateString
   (
   DDEInst         : LongInt;
   PSZString       : PChar
   )
   : HSZ;
  var HSZResult    : HSZ;
  begin
    HSZResult:=0;
    if DDEInst <> 0 then HSZResult:=DdeCreateStringHandle(DDEInst,PSZString,cp_WinAnsi);
    Result:=HSZResult;
  end;

{**************************************** DDEFreeString ****************************************}

Procedure TDDEMLServer.DDEFreeString
   (
   DDEInst         : LongInt;
   var HSZStr      : HSZ
   );
  begin
    if HSZStr <> 0 then begin
      if DDEInst <> 0 then DDEFreeStringHandle(DDEInst,HSZStr);
      HSZStr:=0;
    end;
  end;

{**************************************** SetActualChild ****************************************}

Procedure TDDEMLServer.SetActualChild
   (
   AChild          : HWnd
   );
  begin
    ActualChild:=AChild;
  end;

{**************************************** GetStringTypeAndLayerInfo ****************************************}

Procedure TDDEMLServer.GetStringTypeAndLayerInfo
   (
   AHSCnv          : HConv
   );
  var DDEMLEntry   : PDDEMLApp;
  Function SearchEntry
     (
     Item          : PDDEMLApp
     )
     : Boolean; Far;
    begin
      Result:=Item^.HSCnv = AHSCnv;
    end;
  begin
    DDEMLEntry:=DDEHandler^.DDEMLApps^.FirstThat(@SearchEntry);
    if DDEMLEntry <> NIL then begin
      StrFormat:=DDEMLEntry^.StrFormat;
      DBSLayerInfo:=DDEMLEntry^.DBCLayerInf; 
      SCallBack:=DDEMLEntry^.CCallBack;
    end
    else begin
      StrFormat:=WinGISStrFmt;
      DBSLayerInfo:=DBLInfoNone;
      SCallBack:=DBCBackBool;
    end;
  end;

{**************************************** AcceptExecute ****************************************}

{$IFDEF COM}
procedure TDDEMLServer.OnIpcReceive(const sCmd: WideString; iSessionID: Integer);   // callback
  var DDEString    : PChar;
      DDESSize     : LongInt;
      DDEExecStr   : PChar;
      StartDBLInf  : Word;
      StartCBack   : Word;
      DDEMLClnt    : PDDEMLApp;
      CName        : Array[0..255] of Char;
  Function SearchEntry
     (
     Item          : PDDEMLApp
     )
     : Boolean; Far;
    begin
      Result:=Item^.HSCnv = iSessionID;
    end;
  begin


    EndMode:=FALSE;

      GetMem(DDEString, Length(sCmd) + 2);
      StrPCopy(DDEString, sCmd);

      WriteLog('OnIpcReceive ' + DDEString);

      if DDEString <> '' then begin
        {if LogFile.Logging then begin
          DDEMLClnt:=DDEHandler^.DDEMLApps^.FirstThat(@SearchEntry);
          if DDEMLClnt <> NIL then StrCopy(CName,DDEMLClnt^.Service)
          else StrCopy(CName,'???');
          LogFile.WriteLogFmt('DDE-Recv : %s received from %s.',[DDEString,CName]);
        end;}
        DDEExecStr:=NIL;
        if ServerChange then GetStringTypeAndLayerInfo(iSessionID);
        DDEHandler^.FGotLInfo:=DBSLayerInfo;
        if (StrLComp(DDEString,'[CIF]',5) = 0) then begin
        //if Copy(DDEString,1,5) = '[CIF]' then begin
          RegisterClientInfoIPC(DDEString,WinGISStrFmt,StartDBLInf,StartCBack, iSessionID);
          StrFormat:=WinGISStrFmt;
          DBSLayerInfo:=StartDBLInf;
          DDEMLServer^.HSCnv:=0;
          SCallBack:=StartCBack;

        end
        else if (StrLComp(DDEString,'[CONVERSATIONINFO(',18) = 0) then begin
          RegisterClientInfo(DDEString,DesmaStrFmt,StartDBLInf,StartCBack);
          StrFormat:=DesmaStrFmt;
          DBSLayerInfo:=StartDBLInf;
          DDEMLServer^.HSCnv:=0;
          SCallBack:=StartCBack;

        end
        else begin
          case StrFormat of
            WinGISStrFmt : DDEExecStr:=StrNew(DDEString);
            DesmaStrFmt  : DDEExecStr:=DDEHandler^.TranslateInStringDesma(DDEString,EndMode);
          end;
          if (DDEExecStr <> NIL) and (StrComp(DDEExecStr,'') <> 0) then begin
            DDEHandler^.MacroCnv:=iSessionID;
            if not DDEHandler^.MacroMode then begin
              if (StrLComp(DDEExecStr,'[MCB]',5) = 0) then begin
                DDEHandler^.MacroMode:=TRUE;
              end else begin
                DoExecute(iSessionID,iSessionID,DDEExecStr,iSessionID);
                if EndMode then DoExecute(iSessionID,iSessionID,'[END][]',iSessionID);
              end;
            end
            else if DDEHandler^.MacroMode then begin
              if (StrLComp(DDEExecStr,'[MCE]',5) = 0) then begin
                DDEHandler^.MacroCnv:=iSessionID;
                DDEHandler^.MacroHSZCT:=iSessionID;
                DDEHandler^.MacroHSZCI:=iSessionID;
                if ActualChild <> 0 then begin
                  PostMessage(ActualChild,wm_Command,cm_EndDDEMLSeq,0);
                end
                else if AMainWindow <> 0 then begin
                  PostMessage(AMainWindow,wm_Command,cm_EndDDEMLSeq,0);
                end;
                {
                DDEHandler^.EnableDDEReception(FALSE);
                DDEHandler^.NWUMode:=TRUE;
                DDEHandler^.DoAllDDEMLCommands(NIL);
                DDEHandler^.NWUMode:=FALSE;
                DDEHandler^.MacroMode:=FALSE;
                SendMessage(AMainWindow,wm_Command,cm_SendRDRCommADDE,0);
                DDEHandler^.EnableDDEReception(TRUE);
                }
              end
              else begin
                DDEHandler^.InsertAllDDEData(DDEExecStr);
                if EndMode then DDEHandler^.InsertAllDDEData('[END][]');
              end;
            end;
          end;

        end;
        if DDEExecStr <> NIL then StrDispose(DDEExecStr);
      end;

end;
{$ENDIF}

Function TDDEMLServer.AcceptExecute
   (
   HSZClientTopic  : HSZ;
   HSZClientItem   : HSZ;
   HData           : HDDEData;
   Fmt             : Word;
   ACnv            : HConv
   )
   : Boolean;
  var DDEString    : PChar;
      DDESSize     : LongInt;
      DDEExecStr   : PChar;
      StartDBLInf  : Word;
      StartCBack   : Word;
      DDEMLClnt    : PDDEMLApp;
      CName        : Array[0..255] of Char;
  Function SearchEntry
     (
     Item          : PDDEMLApp
     )
     : Boolean; Far;
    begin
      Result:=Item^.HSCnv = ACnv;
    end;
  begin
    Result:=FALSE;
    DDEString:=NIL;
    EndMode:=FALSE;
    if (HSZClientTopic = HSZSTopic) then begin
      DDEString:=DDEAccessData(HData,@DDESSize);
      //MessageBox(0,DDEString,'GisComm.dll - Debugger',MB_ICONINFORMATION or MB_SYSTEMMODAL);
      if DDEString <> NIL then begin
        if DDEHandler^.WriteToLogFile then begin
          DDEMLClnt:=DDEHandler^.DDEMLApps^.FirstThat(@SearchEntry);
          if DDEMLClnt <> NIL then StrCopy(CName,DDEMLClnt^.Service)
          else StrCopy(CName,'???');
          DDEHandler^.WriteLogFmt('DDE-Recv : %s received from %s.',[DDEString,CName]);
        end;
        WriteLog('DDE-Recv : ' + DDEString + ' received');
        DDEExecStr:=NIL;
        if ServerChange then GetStringTypeAndLayerInfo(ACnv);
        DDEHandler^.FGotLInfo:=DBSLayerInfo;
        if (StrLComp(DDEString,'[CIF]',5) = 0) then begin
          RegisterClientInfo(DDEString,WinGISStrFmt,StartDBLInf,StartCBack);
          StrFormat:=WinGISStrFmt;
          DBSLayerInfo:=StartDBLInf;
          DDEMLServer^.HSCnv:=0;
          SCallBack:=StartCBack;
          Result:=TRUE;
        end
        else if (StrLComp(DDEString,'[CONVERSATIONINFO(',18) = 0) then begin
          RegisterClientInfo(DDEString,DesmaStrFmt,StartDBLInf,StartCBack);
          StrFormat:=DesmaStrFmt;
          DBSLayerInfo:=StartDBLInf;
          DDEMLServer^.HSCnv:=0;
          SCallBack:=StartCBack;
          Result:=TRUE;
        end
        else begin
          case StrFormat of
            WinGISStrFmt : DDEExecStr:=StrNew(DDEString);
            DesmaStrFmt  : DDEExecStr:=DDEHandler^.TranslateInStringDesma(DDEString,EndMode);
          end;
          if (DDEExecStr <> NIL) and (StrComp(DDEExecStr,'') <> 0) then begin
            DDEHandler^.MacroCnv:=ACnv;
            if not DDEHandler^.MacroMode then begin
              if (StrLComp(DDEExecStr,'[MCB]',5) = 0) then begin
                DDEHandler^.MacroMode:=TRUE;
                Result:=TRUE;
{$IFDEF WGIS}
                {EnableAllMenuCommands(DrawMenu^.MenHandle,FALSE);
                DrawMenuBar(Application^.MainWindow^.HWindow);
                DrawMenu^.EnableMenuEntries(sm_SideMenu);}
{$ENDIF}
{$IFDEF WinMAP}
                {EnableAllMenuCommands(DrawMenu^.MenHandle,FALSE);
                DrawMenuBar(Application^.MainWindow^.HWindow);
                DrawMenu^.EnableMenuEntries(sm_SideMenu);}
{$ENDIF}
{$IFDEF WMLT}
 {$IFDEF SDK}
                {DrawMenuSDK^.EnableMenuEntries(sm_SideMenu);}
 {$ELSE}
                {DrawMenuV^.EnableMenuEntries(sm_SideMenu);
                DrawMenuH^.EnableMenuEntries(sm_SideMenu);}
 {$ENDIF}
{$ENDIF}
              end
              else begin
                Result:=DoExecute(HSZClientTopic,HSZClientItem,DDEExecStr,ACnv);
                if EndMode then DoExecute(HSZClientTopic,HSZClientItem,'[END][]',ACnv);
              end;
            end
            else if DDEHandler^.MacroMode then begin
              if (StrLComp(DDEExecStr,'[MCE]',5) = 0) then begin
                DDEHandler^.MacroCnv:=ACnv;
                DDEHandler^.MacroHSZCT:=HSZClientTopic;
                DDEHandler^.MacroHSZCI:=HSZClientItem;
                if ActualChild <> 0 then begin
                  PostMessage(ActualChild,wm_Command,cm_EndDDEMLSeq,0);
                  Result:=TRUE;
                end
                else if AMainWindow <> 0 then begin
                  PostMessage(AMainWindow,wm_Command,cm_EndDDEMLSeq,0);
                  Result:=TRUE;
                end
                else Result:=FALSE;
              end
              else begin
                DDEHandler^.InsertAllDDEData(DDEExecStr);
                if EndMode then DDEHandler^.InsertAllDDEData('[END][]');
                Result:=TRUE;
              end;
            end;
          end
          else Result:=TRUE;
        end;
        if DDEExecStr <> NIL then StrDispose(DDEExecStr);
        DDEUnaccessData(hData);
        Result:=TRUE;
      end;
    end
    else Result:=FALSE;
    DDEFreeDataHandle(HData);
  end;

{**************************************** DoExecute ****************************************}

Function TDDEMLServer.DoExecute
   (
   HSZClientTopic  : HSZ;
   HSZClientItem   : HSZ;
   AStr            : PChar;
   ACnv            : HConv
   )
   : Boolean;
  var DDEMLClnt    : PDDEMLApp;
      CName        : Array[0..255] of Char;
      // added by AS
      pTItem       : PTSaveCmdItem;
  Function SearchEntry
     (
     Item          : PDDEMLApp
     )
     : Boolean; Far;
    begin
      Result:=Item^.HSCnv = ACnv;
    end;
  begin
    DDEMLClnt:=DDEHandler^.DDEMLApps^.FirstThat(@SearchEntry);
    if DDEMLClnt <> NIL then StrCopy(CName,DDEMLClnt^.Service)
    else StrCopy(CName,'');

    // changed by AS
  {  SaveHSZCTopic:=HSZClientTopic;
    SaveAStr:=StrNew(AStr);
    SaveACnv:=StrNew(CName);
    SendMessage(AMainWindow,wm_Command,cm_GetNewDDEString,0);
    StrDispose(SaveAStr);
  }
    New(pTItem);
    pTItem^.SaveHSZCTopic := HSZClientTopic;
    pTItem^.SaveAStr := StrNew(AStr);
    pTItem^.SaveACnv := StrNew(CName);

    tlSaveCmdList.Add(pTItem);
    SendMessage(AMainWindow,wm_Command,cm_GetNewDDEString,0);

    if tlDeleteCmdList.Count > 0 then begin
       pTItem := tlDeleteCmdList.Items[0];
       if pTItem <> nil then begin
          StrDispose(pTItem^.SaveAStr);
          StrDispose(pTItem^.SaveACnv);
          Dispose(pTItem);
          tlDeleteCmdList.Delete(0);
       end;
    end;

    Result:=TRUE;
  end;

{**************************************** GetDDECommand ****************************************}

Procedure TDDEMLServer.GetDDECommand
   (
   var AHSZSTopic      : HSZ;
   var AHSZClientTopic : HSZ;
   var AStr            : PChar;
   var ACnv            : PChar
   );
var
       pTItem          : PTSaveCmdItem;
  begin

    // changed by AS
  {
    //AHSZSTopic:=HSZSTopic;
    AHSZSTopic := SaveHSZCTopic;
    AHSZClientTopic:=SaveHSZCTopic;

//    AHSZSTopic:=1234;
//    AHSZClientTopic:=1234;

    AStr:=SaveAStr;
    ACnv:=SaveACnv;
   }

    if tlSaveCmdList.Count > 0 then begin
       pTItem := tlSaveCmdList.Items[0];
       if pTItem <> nil then begin
          AStr := pTItem^.SaveAStr;
          ACnv := pTItem^.SaveACnv;
          AHSZSTopic := pTItem^.SaveHSZCTopic;
          AHSZClientTopic := pTItem^.SaveHSZCTopic;

          // add to the list of already delivered items (will get disposed afterwards)
          tlDeleteCmdList.Add(pTItem);
          // delete first item
          tlSaveCmdList.Delete(0);
       end;
    end;
  end;

Function TDDEMLServer.Request
   (
   HSZClientTopic  : HSZ;
   HSZClientItem   : HSZ;
   HData           : HDDEData;
   Fmt             : Word;
   ACnv            : HConv
   )
   : HDDEData;
  var DDEString    : PChar;
      DDESSize     : LongInt;
      DDEExecStr   : PChar;
      pData        : PChar;
      pSerial      : PChar;
      pModCod      : PChar;
      pLizence     : PChar;
      pModShort    : PChar;
      pTemp        : PChar;
      DestSize     : LongInt;
      Tmp          : Array[0..10] of Char;
      Item         : Array[0..32] of Char;
      cchMax       : DWord;
      cbOff        : DWord;
      afCmd        : UInt;
      im_TurboR    : PChar;
  begin
    DDEString:=NIL;
    if (HSZClientTopic = HSZSTopic) then begin
      cchMax:=32;
      DDEQueryString(SDDEInst,HSZClientItem,Item,cchMax,CP_WINANSI);
      DDEString:=@Item[0];
      if (DDEString <> NIL) then begin
        if (StrComp(DDEString,'GetRegInfo') = 0) then begin                  { TurboRaster wants Modulecode, Serial,  ... }
          SendMessage(AMainWindow,wm_Command,cm_SetTRData,0);
          pSerial:=StrNew(Req_pSerial);
          pModCod:=StrNew(Req_pModCod);
          pLizence:=StrNew(Req_pLizence);
          im_TurboR:=StrNew(Req_im_TR);
          pModShort:=StrNew(StrPCopy(Tmp,im_TurboR));
          DestSize:=StrLen(pSerial)+1+StrLen(pModCod)+1+StrLen(pModShort)+1+StrLen(pLizence)+1;
          GetMem(Pointer(pData),DestSize);
          if (pData = NIL) then begin
            DDEHandler^.ShowMsgBox('',11603,0);
            StrDispose(pSerial);
            StrDispose(pModCod);
            StrDispose(pModShort);
            StrDispose(pLizence);
            StrDispose(im_TurboR);
            FreeMem(Pointer(pData),DestSize);
            Exit;
          end;
          pTemp:=pData;
          StrCopy(pTemp,pSerial);
          pTemp:=pTemp+StrLen(pSerial)+1;
          StrCopy(pTemp,pModCod);
          pTemp:=pTemp+StrLen(pModCod)+1;
          StrCopy(pTemp,pLizence);
          pTemp:=pTemp+StrLen(pLizence)+1;
          StrCopy(pTemp,pModShort);
          cbOff:=0;
          afCmd:=0;
          Result:=DDECreateDataHandle(SDDEInst,Pointer(pData),DestSize,cbOff,HSZClientItem,Fmt,afCmd);
          if (Result = 0) then begin
            DDEHandler^.ShowMsgBox('',11606,0);
            Exit;
          end;
          FreeMem(Pointer(pData),DestSize);
          TRConv:=ACnv;
        end
        else Result:=0;
      end;
    end;
  end;

Procedure TDDEMLServer.SendRefPoints
   (
   GeoReference    : PLPointArray
   );
  var cItem        : Array[0..24] of Char;
      hItem        : HSZ;
  begin
    if (TRConv = 0) then Exit;
    StrCopy(cItem,'AdvRefPoints');
    hItem:=DDECreateStringHandle(SDDEInst,cItem,CP_WINANSI);
    GeoRef:=GeoReference;
    DDEPostAdvise(SDDEInst,HSZSTopic,hItem);
    if (hItem <> 0) then DDEFreeStringHandle(SDDEInst,hItem);
  end;

{*****************************************************************************************************************************}
{**************************************** DDEML-Client ***********************************************************************}
{*****************************************************************************************************************************}

{**************************************** CallbackProcClient *****************************************************************}

Function CallbackProcClient
   (
   CallType        : UINT;
   Fmt             : UINT;
   Conv            : HConv;
   HSZ1            : HSZ;
   HSZ2            : HSZ;
   Data            : HDDEData;
   Data1           : DWORD;
   Data2           : DWORD
   )
   : HDDEData; stdcall;
  var CallBackType : Word;
      DDEMLEntry   : PDDEMLApp;
  Function SearchEntry
     (
     Item          : PDDEMLApp
     )
     : Boolean; Far;
    begin
      Result:=FALSE;
      with Item^ do begin
        if CObjAddress <> NIL then begin
          if ((HSCnv = Conv) or (PDDEMLClient(CObjAddress)^.HCCnv = Conv)) then Result:=TRUE;
        end;
      end;
    end;
  begin
    Result:=0;
{$IFDEF LOG}
    if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcClient - Start');
{$ENDIF}
    DDEMLEntry:=DDEHandler^.DDEMLApps^.FirstThat(@SearchEntry);
    if DDEMLEntry <> NIL then CallBackType:=DDEMLEntry^.CCallBack
    else CallBackType:=DBCBackBool;
    if CallBackType = DBCBackBool then Result:=0
    else if CallBackType = DBCBackDDE_ then Result:=DDE_FNOTPROCESSED;
    case CallType of
      xtyp_AdvData           : begin
{$IFDEF LOG}
                                 if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcClient - xtyp_AdvData');
{$ENDIF}
                                 if CallBackType = DBCBackBool then Result:=0
                                 else if CallBackType = DBCBackDDE_ then Result:=DDE_FNOTPROCESSED;
                               end;
      xtyp_Disconnect        : begin
{$IFDEF LOG}
                                 if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcClient - xtyp_Disconnect');
{$ENDIF}
                                 DDEHandler^.UnConnect(Conv);
                                 if CallBackType = DBCBackBool then Result:=1
                                 else if CallBackType = DBCBackDDE_ then Result:=DDE_FACK;
                               end;
      xtyp_Error             : begin
{$IFDEF LOG}
                                 if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcClient - xtyp_Error');
{$ENDIF}
                                 if CallBackType = DBCBackBool then Result:=0
                                 else if CallBackType = DBCBackDDE_ then Result:=DDE_FNOTPROCESSED;
                               end;
      xtyp_Register          : begin
{$IFDEF LOG}
                                 if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcClient - xtyp_Register');
{$ENDIF}
                                 if CallBackType = DBCBackBool then Result:=0
                                 else if CallBackType = DBCBackDDE_ then Result:=DDE_FNOTPROCESSED;
                               end;
      xtyp_Unregister        : begin
{$IFDEF LOG}
                                 if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcClient - xtyp_Unregister');
{$ENDIF}
                                 if CallBackType = DBCBackBool then Result:=0
                                 else if CallBackType = DBCBackDDE_ then Result:=DDE_FNOTPROCESSED;
                               end;
      xtyp_Xact_Complete     : begin
{$IFDEF LOG}
                                   WriteLog('CallbackProcServer - xtyp_Xact_Complete');
                                   if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcClient - xtyp_XAct_Complete');
{$ENDIF}
                                   ReleaseSemaphore( hsemDDEAck, 1, nil );
                                   Result:=0
                                 end;

    end;
{$IFDEF LOG}
    if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('CallbackProcClient - End');
{$ENDIF}
  end;

{**************************************** Init ****************************************}

Constructor TDDEMLClient.Init
   (
   AWindow         : HWnd;
   AStringFormat   : Word;
   ADBLayerInfo    : Word;
   ASepSign        : Char;
   ACallBack       : Word;
   lSession        : longint = 0
   );
  var sTimeout     : string;
  begin
    Inherited Init;
    AMainWindow:=AWindow;
    PCPrcCallBack:=NIL;
    CDDEInst:=0;
    HCCnv:=0;
    StrFormat:=AStringFormat;
    DBCLayerInfo:=ADBLayerInfo;
    CSepSign:=ASepSign;
    LIFMode:=FALSE;
    CCallBack:=ACallBack;
    FSession := lSession;

    try
      sTimeout := GetRegString('SOFTWARE\Progis\AXWingis', 'AckTimeout', '');
    except
      on exception do ;
    end;   
    try
      lAckTimeout := 0;
      lAckTimeout := StrToInt(sTimeout);
    except
      on exception do begin
         lAckTimeout := 0;
      end;
    end;

    if lAckTimeout < 10 then begin
       lAckTimeout := 2000;
       sTimeout := IntToStr(lAckTimeout);
       try
         SetRegString('SOFTWARE\Progis\AXWingis', 'AckTimeout', sTimeout);
       except
         on exception do ;
       end;
    end;

  end;

{**************************************** Done ****************************************}

Destructor TDDEMLClient.Done;
  begin
    HCCnv:=0;
    CDDEInst:=0;
    PCPrcCallBack:=NIL;
    AMainWindow:=0;
    Inherited Done;
  end;

{**************************************** UpdateConnectionSettings ****************************************}

Procedure TDDEMLClient.UpdateConnectionSettings
   (
   AStringFormat   : Word;
   ADBLayerInfo    : Word;
   ASepSign        : Char;
   ACallBack       : Word
   );
  begin
    StrFormat:=AStringFormat;
    DBCLayerInfo:=ADBLayerInfo;
    CSepSign:=ASepSign;
    CCallBack:=ACallBack;
  end;

{**************************************** InitiateClientDDE ****************************************}

Function TDDEMLClient.InitiateClientDDE
   (
   AName           : PChar;
   ExeFile         : PChar;
   StartPars       : PChar;
   Service         : PChar;
   Topic           : PChar;
   Item            : PChar;
   lSession: longint = 0
   )
   : Boolean;
  var AppFile      : Array[0..512] of Char;
      Error        : Word;
      DDEStr       : Array[0..512] of Char;
      Tmp          : Array[0..255] of Char;
      ulRes        : DWord;
      siStartUpInfo: TStartupInfo;
      piProcessInformation: TProcessInformation;
      lbFalse: longbool;
      iRetryCount: integer;
      bConnectOk: boolean;
      sLastErrorMsg: string;
  begin
    Result:=FALSE;
    {ExecuteString1('[SOK][0]');}
    StrCopy(Name,AName);
    StrCopy(ApplicFile,ExeFile);
    if StartPars <> NIL then StrCopy(StartParams,StartPars)
    else StrCopy(StartParams,'');
    StrCopy(StrCService,Service);
    StrCopy(StrCTopic,Topic);
    StrCopy(StrCItem,Item);
    if CDDEInst = 0 then begin
      PCPrcCallBack:=MakeProcInstance(@CallBackProcClient,HInstance);
      ulRes:=0;
      if DdeInitialize(CDDEInst,TFNCallback(PCPrcCallBack),APPCLASS_STANDARD,ulRes) <> dmlErr_No_Error then
        DDEHandler^.ShowMsgBox('',11604,0);
    end;
    if CDDEInst <> 0 then begin
      DdeFreeString(CDDEInst,HSZCService);                                          {falls alte Konversation vorhanden war}
      DdeFreeString(CDDEInst,HSZCTopic);
      DdeFreeString(CDDEInst,HSZCItem);
      if (StrComp(StrCService,'') <> 0) and (StrComp(StrCTopic,'') <> 0) and (StrComp(StrCItem,'') <> 0) then begin
        HSZCService:=DdeCreateString(CDDEInst,StrCService);
        HSZCTopic:=DdeCreateString(CDDEInst,StrCTopic);
        HSZCItem:=DdeCreateString(CDDEInst,StrCItem);

        // changed by AS
        // Connect by DDE only if Item is not "COM"
        if Item <> 'COM' then
           ClientConnect
        else
           HCCnv := lSession;

        if (HCCnv = 0) and (StrComp(ApplicFile,'') <> 0) then begin
          StrCopy(AppFile,ApplicFile);
          if StrComp(StartParams,'') <> 0 then begin
            StrCat(AppFile,' ');
            StrCat(AppFile,StartParams);
          end;

          FillChar(siStartUpInfo, sizeof(siStartUpInfo), #0);
          siStartUpInfo.cb := sizeof(siStartUpInfo);
          siStartUpInfo.lpReserved := nil;
          siStartUpInfo.wShowWindow := SW_SHOWNORMAL;
          siStartUpInfo.dwFlags := STARTF_USESHOWWINDOW;
          lbFalse := false;
          if CreateProcess( ApplicFile
                               , AppFile {Cmdline}
                               , nil {ProcessAttr}
                               , nil {ThreadAttr}
                               , false {InheritHandle}
                               , CREATE_DEFAULT_ERROR_MODE + NORMAL_PRIORITY_CLASS
                               , nil {EnvironmentStrings}
                               , nil {CurrentDir}
                               , siStartUpInfo
                               , piProcessInformation) then begin
            Error := 33;
          end else begin
            Error := 0;
          end;

          //Error:=WinExec(AppFile,sw_normal);

          if Error > 32 then begin
             // changed by AS
             if Item <> 'COM' then begin
                iRetryCount := 0;
                bConnectOk := false;

                while (not bConnectOk) and (iRetryCount < 40) do begin
                   WaitForInputIdle(piProcessInformation.hProcess, 500);
                   if ClientConnect then bConnectOk := true;
                   iRetryCount := iRetryCount + 1;
                end;

                if not bConnectOk then DDEHandler^.ShowMsgBox('',11605,0);
             end;
          end;
        end;
      end;
    end;

    if lSession <> 0 then HCCnv := lSession;
    if HCCnv <> 0 then begin
      if StrFormat = WinGISStrFmt then StrCopy(DDEStr,'[CIF][')
      else if StrFormat = DesmaStrFmt then StrCopy(DDEStr,'[CONVERSATIONINFO(');
      GetModuleFileName(HInstance,Tmp,255);
      StrCat(DDEStr,Tmp);
      StrCat(DDEStr,',');
      StrCat(DDEStr,DDEMLServer^.StrSService);
      StrCat(DDEStr,',');
      StrCat(DDEStr,DDEMLServer^.StrSTopic);
      StrCat(DDEStr,',');
      StrCat(DDEStr,DDEMLServer^.StrSItem);
      if StrFormat = 0 then StrCat(DDEStr,']')
      else if StrFormat = 1 then StrCat(DDEStr,')]');

      StrCat(DDEStr,'[');
      StrCat(DDEStr,PChar(AnsiString(DDEHandler.IniSepSign)));
      StrCat(DDEStr,']');

{$IFDEF COM}
      // changed by AS
      if lSession <> 0 then begin
         FSession := lSession;
         if Assigned(FAXIpc) then begin
            if FAXIpc.SendTo(DDEStr, lSession) <> 0 then begin

               if FAXIpc.GetLastError <> 10035 then begin  // WSAEWOULDBLOCK
                  sLastErrorMsg := FAXIpc.GetLastErrorMsg;
                  if sLastErrorMsg <> '' then
                     Application.MessageBox(PChar(sLastErrorMsg), 'COM Error', 0);
               end;
            end;
         end else begin
            //Application.MessageBox('Error: COM not initialized.', 'COM', 0);
            ;
         end;
      end else
{$ENDIF}
         ExecuteString(DDEStr);

      Result:=TRUE;
    end;
  end;

{**************************************** ClientConnect ****************************************}

Function TDDEMLClient.ClientConnect
   : Boolean;
  var BInitiate    : Boolean;
  begin
    BInitiate:=FALSE;
    if (HSZCService <> 0) and (HSZCTopic <> 0) then begin
      HCCnv:=DdeConnect(CDDEInst,HSZCService,HSZCTopic,NIL);
      BInitiate:=HCCnv <> 0;
    end
    else DDEHandler^.ShowMsgBox('',11606,0);
    Result:=BInitiate;

{$IFDEF LOG}
    if Result then
       WriteLog('Client - Connect OK')
    else
       WriteLog('Client - Connect FAILED')    
{$ENDIF}
end;

{**************************************** TerminateDDE ****************************************}

Procedure TDDEMLClient.TerminateDDE;
  begin
{$IFDEF LOG}
    if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('Client - TerminatDDE');
{$ENDIF}
{$IFDEF LOG}
    WriteLog('Client - TerminatDDE');
{$ENDIF}

    ClientDisconnect;
    DdeFreeString(CDDEInst,HSZCService);
    DdeFreeString(CDDEInst,HSZCTopic);
    DdeFreeString(CDDEInst,HSZCItem);
    CDDEInst:=0;
    if PCPrcCallBack <> NIL then begin
      FreeProcInstance(PCPrcCallBack);
      PCPrcCallBack:=NIL;
    end;
  end;

{**************************************** ClientDisconnect ****************************************}

Procedure TDDEMLClient.ClientDisconnect;
  begin
{$IFDEF LOG}
    if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLog('Client - ClientDisconnect');
{$ENDIF}
    if HCCnv <> 0 then begin
      DDEDisconnect(HCCnv);
      HCCnv:=0;
    end;
  end;

{**************************************** DDEOk ****************************************}

Function TDDEMLClient.DDEOk
   : Boolean;
  begin
    Result:=HCCnv <> 0;
  end;

{**************************************** DDECreateString ****************************************}

Function TDDEMLClient.DDECreateString
   (
   DDEInst         : LongInt;
   PSZString       : PChar
   )
   : HSZ;
  var HSZResult    : HSZ;
  begin
    HSZResult:=0;
    if DDEInst <> 0 then HSZResult:=DdeCreateStringHandle(DDEInst,PSZString,cp_WinAnsi);
    Result:=HSZResult;
  end;

{**************************************** DDEFreeString ****************************************}

Procedure TDDEMLClient.DDEFreeString
   (
   DDEInst         : LongInt;
   var HSZStr      : HSZ
   );
  begin
    if HSZStr <> 0 then begin
      if DDEInst <> 0 then DDEFreeStringHandle(DDEInst,HSZStr);
      HSZStr:=0;
    end;
  end;

{**************************************** Execute ****************************************}

Function TDDEMLClient.Execute
   (
   HGlobal         : THandle;
   GlobalPtr       : PGArray;
   lSession: longint = 0
   )
   : Boolean;
  var DDETempStr   : PChar;
      DDEExecStr   : PChar;
      AskForData   : Boolean;
      sLastErrorMsg: string;
  begin
    AskForData:=FALSE;
    if DDEOK then begin
      if DBCLayerInfo <> DDEHandler^.FLayerInfo then
        DDETempStr:=DDEHandler^.TranslateOutStringLayerInfo(PChar(GlobalPtr),DBCLayerInfo,LIFMode)
      else DDETempStr:=StrNew(PChar(GlobalPtr));
      if (DDETempStr <> NIL) and (StrComp(DDETempStr,'') <> 0) then begin
        if CSepSign <> DDEHandler^.FOutSepSign then DDEHandler^.TranslateOutStringSepSign(DDETempStr,CSepSign);
        case StrFormat of
          WinGISStrFmt : begin
                           if GlobalPtr <> NIL then DDEExecStr:=StrNew(DDETempStr)
                           else DDEExecStr:=NIL;
                         end;
          DesmaStrFmt  : begin
                           DDEExecStr:=DDEHandler^.TranslateOutStringDesma(DDETempStr);
                           if (DDEExecStr <> NIL) and (StrComp(DDEExecStr,'[ENDMACRO()]') = 0)
                              and DDEHandler^.DataMode then begin
                             AskForData:=TRUE;
                             DDEHandler^.DataMode:=FALSE;
                           end;
                         end;
          else           begin
                           if GlobalPtr <> NIL then DDEExecStr:=StrNew(DDETempStr)
                           else DDEExecStr:=NIL;
                         end;
        end;
        StrDispose(DDETempStr);
        if (DDEExecStr <> NIL) and (StrComp(DDEExecStr,'') <> 0) then begin

{$IFDEF COM}
          if (lSession <> 0) or (FSession <> 0) then begin

             if Assigned(FAXIpc) then begin
                WriteLog('IpcSend Start: ' + DDEExecStr);
                if FAXIpc.SendTo(DDEExecStr, lSession) <> 0 then begin
                   WriteLog('IpcSend FAILED: ' + DDEExecStr);
                   if FAXIpc.GetLastError <> 10035 then begin  // WSAEWOULDBLOCK
                      sLastErrorMsg := FAXIpc.GetLastErrorMsg;
                      if sLastErrorMsg <> '' then
                         Application.MessageBox(PChar(sLastErrorMsg), 'COM Error', 0);
                   end;
                end else begin
                   WriteLog('IpcSend OK: ' + DDEExecStr);
                end;
                if AskForData then FAXIpc.SendTo('[DATASTART()]', lSession);
             end else begin
                //Application.MessageBox('Error: COM not initialized.', 'COM Error', 0);
                ;
             end;

          end else begin
{$ENDIF}
            if not ExecuteString(DDEExecStr) then begin
              //DDEHandler^.ShowMsgBox(Name,11696,0);
            end;
            if AskForData then ExecuteString('[DATASTART()]');
{$IFDEF COM}
          end;
{$ENDIF}
        end;
        if DDEExecStr <> NIL then StrDispose(DDEExecStr);
      end;
      Result:=TRUE;
    end
    else Result:=FALSE;
  end;

{**************************************** ExecuteString ****************************************}

Function TDDEMLClient.ExecuteString
   (
   AStr            : PChar;
   lSession: longint = 0
   )
   : Boolean;
  var PData        : PChar;
      LSize        : LongInt;
      WFmt         : Word;
      ErrLast      : Integer;
      ConvOK       : Boolean;
      Count        : Integer;
      LastError    : Word;
      ErrorCode    : Word;
      ItemHdl      : Longint;
      dwWaitResult : DWord;
      dwResPtr     : DWord;
      bAckTimeout  : boolean;
      i            : integer;
  begin

  try
    Result:=FALSE;
    PData := nil;

    ConvOK:=DDEOK;
    if ConvOK then begin

      //PData:=AStr;
      GetMem(PData, Length(AStr)+1);
      strcopy(PData, AStr);


      // changed by AS
      // to avoid the error DMLERR_REENTRANCY
      //EnterCriticalSection(csDDEClientCriticalSection);
      SetTimer(0, 1, 300, nil);
      i := 0;
      repeat
        dwWaitResult := WaitForSingleObject(hsemDDEClient, 300);
        Application.ProcessMessages();
        i := i + 1;
      until (dwWaitResult <> WAIT_TIMEOUT) or (i >= 10);

      if dwWaitResult = WAIT_FAILED then
         WriteLog('WaitForSingleObject hsemDDEClient FAILED');


      ErrLast:=0;
      ItemHdl:=0;
      WFmt:=cf_Text;
      LSize:=StrLen(PData)+1;
      DDEHandler^.FSending:=TRUE;
      if DDEHandler^.WriteToLogFile then begin
{$IFDEF LOG}
        DDEHandler^.WriteLogFmt('DDE-Send : %s sent to %s.',[PData,StrCService]);
{$ENDIF}
        {DDEHandler^.WriteLogFmt('DDE-Send : %d sent to %s.',[LSize,StrCService]);}
      end;
{$IFDEF LOG}
      WriteLog('START SEND ' + PData);
{$ENDIF}

      dwWaitResult := WaitForSingleObject(hsemDDEAck, 0);
      if dwWaitResult = WAIT_FAILED then
         WriteLog('WaitForSingleObject hsemDDEAck FAILED');

      bAckTimeout := false;
      //ErrLast:=DDEClientTransaction(PData,LSize,HCCnv,ItemHdl,WFmt,xtyp_Execute,lAckTimeout,NIL);
      ErrLast:=DDEClientTransaction(PData,LSize,HCCnv,0,WFmt,xtyp_Execute,TIMEOUT_ASYNC,@dwResPtr);

      if ErrLast <> 0 then begin   // successfull transaction ?
        i := 0;
        repeat
          dwWaitResult := WaitForSingleObject(hsemDDEAck, 200);
          if dwWaitResult = WAIT_TIMEOUT then
             Application.ProcessMessages();
          i := i + 1;
        until (dwWaitResult <> WAIT_TIMEOUT) or ((i*200) >= lAckTimeout);
        if dwWaitResult = WAIT_FAILED then
           WriteLog('WaitForSingleObject hsemDDEAck FAILED');
        if dwWaitResult = WAIT_TIMEOUT then begin
           WriteLog('Timeout ' + PData);
           bAckTimeout := true;
           ErrLast := 0;       // signal an error
        end;
      end;
      ReleaseSemaphore( hsemDDEAck, 1, nil );

{$IFDEF LOG}
      WriteLog('END SEND ' + PData);
{$ENDIF}
{$IFDEF LOG}
      if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLogFmt('DDE-Sedd : %s sent to %s.',[PData,StrCService]);
{$ENDIF}



      // changed by AS
      // to avoid the error DMLERR_REENTRANCY
      ReleaseSemaphore( hsemDDEClient, 1, nil );
      //LeaveCriticalSection(csDDEClientCriticalSection);

      KillTimer(0,1);
      // changed by AS
      // DDEClientTransaction returns 0 on error
      //if CCallBack = DBCBackBool then Result := ErrLast <> 0
      //else if CCallBack = DBCBackDDE_ then begin
      if ErrLast = 0 then begin
        ErrorCode:=1;    // unknown error
        LastError:=DdeGetLastError(CDDEInst);
        if LastError <> DMLERR_NO_ERROR then begin
          case LastError of
            DMLERR_ADVACKTIMEOUT       : ErrorCode:=11660;
            DMLERR_BUSY                : ErrorCode:=11661;
            DMLERR_DATAACKTIMEOUT      : ErrorCode:=11662;
            DMLERR_DLL_NOT_INITIALIZED : ErrorCode:=11663;
            DMLERR_EXECACKTIMEOUT      : ErrorCode:=11664;
            DMLERR_INVALIDPARAMETER    : ErrorCode:=11665;
            DMLERR_LOW_MEMORY          : ErrorCode:=11666;
            DMLERR_MEMORY_ERROR        : ErrorCode:=11667;
            DMLERR_NO_CONV_ESTABLISHED : ErrorCode:=11668;
            DMLERR_NOTPROCESSED        : ErrorCode:=DMLERR_NO_ERROR;
            //DMLERR_NOTPROCESSED        : ErrorCode:=11669;
            DMLERR_POKEACKTIMEOUT      : ErrorCode:=11670;
            DMLERR_POSTMSG_FAILED      : ErrorCode:=11671;
            DMLERR_REENTRANCY          : ErrorCode:=11672;
            DMLERR_SERVER_DIED         : ErrorCode:=11673;
            DMLERR_SYS_ERROR           : ErrorCode:=11674;
            DMLERR_UNADVACKTIMEOUT     : ErrorCode:=11675;
            DMLERR_UNFOUND_QUEUE_ID    : ErrorCode:=11676;
          end; // end case
        end;   // end if LastError <> DMLERR_NO_ERROR then begin
        if bAckTimeout then
           ErrorCode:=11664;    // DMLERR_EXECACKTIMEOUT
      end else begin     // end if not Result then begin
        ErrorCode := DMLERR_NO_ERROR;
      end;

      if ErrorCode <> DMLERR_NO_ERROR then begin
//        DDEHandler^.ShowMsgBox('',11698,ErrorCode);
        if DDEHandler^.WriteToLogFile then DDEHandler^.WriteLogInt(11698,ErrorCode);
{$IFDEF LOG}
        WriteLog('DDE-Sedd: ' + PData + ' sent to ' + StrCService + ' Errorcode: ' + IntToStr(ErrorCode));
{$ENDIF}
      end else begin
{$IFDEF LOG}
        WriteLog('DDE-Sedd: ' + PData + ' sent to ' + StrCService + ' OK');
{$ENDIF}
      end;
      Result := ErrorCode = DMLERR_NO_ERROR;

      DDEHandler^.FSending:=FALSE;
      FreeMem(PData);
      PData := nil;

    end;

  except
    on exception do begin
{$IFDEF LOG}
       WriteLog('Exception in ExecuteString');
{$ENDIF}
       LeaveCriticalSection(csDDEClientCriticalSection);
       if PData <> nil then FreeMem(PData);
       ReleaseSemaphore( hsemDDEClient, 1, nil );
    end;
  end;
  end;
end.

