unit AXIpc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OleCtrls, AXIPCLib_TLB, ExtCtrls;

const
  AXIPCERR_NO_ERROR          = 0;
  AXIPCERR_NO_CONNECTION     = 1;
  AXIPCERR_WRITE_FAILED      = 2;
  AXIPCERR_READ_FAILED       = 3;
  AXIPCERR_CONNECTION_FAILED = 4;
  AXIPCERR_UNKNOWN_ERRORCODE = 9998;
  AXIPCERR_STATUS_INVALID    = 9999;

type
  TOnIpcReceive = procedure(const sCmd: WideString; iSessionID: Integer) of object;
  TOnIpcConnect = procedure(lSessionID: Integer; const sRemoteName: WideString) of object;
  TOnIpcDisconnect = procedure(lSessionID: Integer) of object;

     PTSaveCmdItem      = ^TSaveCmdItem;
     TSaveCmdItem       = record
       sCmd             : string;
       iSession         : integer;
     end;

  TAXIpcWrapper = class(TForm)
    AXIpc1: TAXIpc;
    Timer1: TTimer;
    Timer2: TTimer;
    procedure AXIpc1Receive(Sender: TObject; const sCmd: WideString;
      sSessionID: Integer);
    procedure AXIpc1Connect(Sender: TObject; lSessionID: Integer;
      const sRemoteName: WideString);
    procedure AXIpc1Disconnect(Sender: TObject; lSessionID: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
  private
    { Private-Deklarationen}
  public
    hOwner: HWND;
    lAckTimeout: DWORD;
    lSOKTimeout: DWORD;
    lSOKTimeStamp: DWORD;           // contains the milliseconds when the last SOK 0
                                    // was sent or 0 when a SOK 1 was sent last
                                    // is used to block cmd delivery to the FOnIpcReceive event
    lSOKKeepAlive: DWORD;
    lLastSID: integer;

    tlSaveCmdList    : TList;       // contains cmds to be delivered to wingis

    FOnIpcConnect: TOnIpcConnect;       // callback is fired after a connect was received from the client
    FOnIpcReceive: TOnIpcReceive;       // callback is fired after a cmd was received
    FOnIpcDisconnect: TOnIpcDisconnect;
    //constructor Create(Owner: TComponent);
    { Public-Deklarationen}

    function Listen(): integer;
    function Disconnect(): integer;
    function Send(sCmd: WideString): integer;
    function SendTo(sCmd: WideString; lSession: longint): integer;

    function GetLastError: longint;
    function GetLastErrorMsg: string;
    Procedure NewAXIPCCommand(var Msg:TMessage); message WM_USER+123;
  end;
  PAXIpcWrapper = ^TAXIpcWrapper;

//var
  //AXIpcWrapper: TAXIpcWrapper;

implementation

uses regutils;

{$R *.DFM}

Procedure TAXIpcWrapper.NewAXIPCCommand(var Msg:TMessage);
var pTItem: PTSaveCmdItem;
    sMsg : string;
    iSessionID : integer;
begin

  try
    // cmds available ?
    if tlSaveCmdList.Count > 0 then begin

       if (GetTickCount() - lSOKTimeStamp) > lSOKTimeout then begin
          // waiting for [SOK][1] timed out, go on and allow incoming cmds again
          lSOKTimeStamp := 0;
       end;

       if lSOKTimeStamp = 0 then begin

         // get cmd
         pTItem := tlSaveCmdList.Items[0];
         // delete first item from incoming list
         tlSaveCmdList.Delete(0);

         if pTItem <> nil then begin
            sMsg := pTItem^.sCmd;
            iSessionID := pTItem^.iSession;
            lLastSID := iSessionID;
            try
               Dispose(pTItem);    // delete cmd structure
            except
               on EInvalidPointer do begin
                  //ShowMessage('Invalid Pointer');
                  ;
               end;
            end;
            // dispatch the cmd to giscomm
            if sMsg <> '' then begin
               if Assigned(FOnIpcReceive) then begin
                  FOnIpcReceive(sMsg, iSessionID);
               end;
            end;
            // call ourself again if more cmds available
            if tlSaveCmdList.Count > 0 then begin
               PostMessage(handle,WM_USER+123,0,0);
            end;
         end;
       end;  //endif lSOKTimeStamp = 0

    end;
  except
    on exception do ;
  end;

end;

procedure TAXIpcWrapper.AXIpc1Receive(Sender: TObject;
  const sCmd: WideString; sSessionID: Integer);
var pTItem: PTSaveCmdItem;
begin

  try
    New(pTItem);
    if pTItem <> nil then begin
       pTItem^.sCmd := sCmd;
       pTItem^.iSession := sSessionID;

       tlSaveCmdList.Add(pTItem);

       // set back the keep alive timer
       Timer1.Enabled := false;
       Timer1.Interval := lAckTimeout + 500;
       Timer1.Enabled := true;
    end;

    PostMessage(handle,WM_USER+123,0,0);
  except
    on exception do ;
  end;
end;

procedure TAXIpcWrapper.AXIpc1Connect(Sender: TObject; lSessionID: Integer;
  const sRemoteName: WideString);
begin
  try
     if Assigned(FOnIpcConnect) then
        FOnIpcConnect(lSessionID, sRemoteName);
  except
    on exception do ;
  end;

end;

procedure TAXIpcWrapper.AXIpc1Disconnect(Sender: TObject;
  lSessionID: Integer);
begin
   try
     if Assigned(FOnIpcDisconnect) then
        FOnIpcDisconnect(lSessionID);
  except
    on exception do ;
  end;
end;

function TAXIpcWrapper.Listen(): integer;
begin
  try
     Result := AXIpc1.Listen;
  except
    on exception do Result := AXIPCERR_UNKNOWN_ERRORCODE;
  end;
end;

function TAXIpcWrapper.Disconnect(): integer;
begin
  try
     Result := AXIpc1.Disconnect;
  except
    on exception do Result := AXIPCERR_UNKNOWN_ERRORCODE;
  end;
end;

function TAXIpcWrapper.Send(sCmd: WideString): integer;
begin
  try
     if sCmd = '[SOK][0]' then begin
        lSOKTimeStamp := GetTickCount();
     end else if sCmd = '[SOK][1]' then begin
        lSOKTimeStamp := 0;
        lSOKKeepAlive := 0;
     end;

     Result := AXIpc1.Send(sCmd);
  except
    on exception do Result := AXIPCERR_UNKNOWN_ERRORCODE;
  end;
end;

function TAXIpcWrapper.SendTo(sCmd: WideString; lSession: longint): integer;
begin
  try
     if sCmd = '[SOK][0]' then begin
        lSOKTimeStamp := GetTickCount();
     end else if sCmd = '[SOK][1]' then begin
        lSOKTimeStamp := 0;
        lSOKKeepAlive := 0;
     end;

     Result := AXIpc1.Send(sCmd); //, lSession);
  except
    on exception do Result := AXIPCERR_UNKNOWN_ERRORCODE;
  end;
end;

function TAXIpcWrapper.GetLastErrorMsg: string;
begin
  try
     Result := AXIpc1.GetLastErrorMsg;
  except
    on exception do ;
  end;
end;

function TAXIpcWrapper.GetLastError: longint;
begin
  try
     Result := AXIpc1.GetLastError;
  except
    on exception do Result := AXIPCERR_UNKNOWN_ERRORCODE;
  end;
end;

procedure TAXIpcWrapper.FormCreate(Sender: TObject);
var sTimeout: string;
begin

    lSOKTimeStamp := 0;
    tlSaveCmdList := TList.Create;

    // read the AckTimeout from the registry
    try
      sTimeout := GetRegString('SOFTWARE\Progis\AXWingis', 'AckTimeout', '0');
    except
      on exception do ;
    end;
    try
      lAckTimeout := 0;
      lAckTimeout := StrToInt(sTimeout);
    except
      on exception do begin
         lAckTimeout := 0;
      end;
    end;
    if lAckTimeout < 10 then begin
       lAckTimeout := 2000;
       sTimeout := IntToStr(lAckTimeout);
       try
         SetRegString('SOFTWARE\Progis\AXWingis', 'AckTimeout', sTimeout);
       except
         on exception do ;
       end;
    end;
    // read the SOK from the registry
    try
      sTimeout := GetRegString('SOFTWARE\Progis\AXWingis', 'SOKTimeout', '0');
    except
      on exception do ;
    end;
    try
      lSOKTimeout := 0;
      lSOKTimeout := StrToInt(sTimeout);
    except
      on exception do begin
         lSOKTimeout := 0;
      end;
    end;
    if lSOKTimeout < 10 then begin
       lSOKTimeout := 10000;
       sTimeout := IntToStr(lSOKTimeout);
       try
         SetRegString('SOFTWARE\Progis\AXWingis', 'SOKTimeout', sTimeout);
       except
         on exception do ;
       end;
    end;

    // set back the keep alive timer
    Timer1.Enabled := false;
    Timer1.Interval := lAckTimeout + 1000;
    Timer1.Enabled := true;

end;

procedure TAXIpcWrapper.FormDestroy(Sender: TObject);
var pTItem: PTSaveCmdItem;
begin
  try
    { Cleanup: Free Listitems and the list itself }
    while tlSaveCmdList.Count > 0 do begin
       pTItem := tlSaveCmdList.Items[0];
       // delete first item
       tlSaveCmdList.Delete(0);

       if pTItem <> nil then Dispose(pTItem);
    end;
    tlSaveCmdList.Free;
  except
    on exception do ;
  end;
end;

// ** the incoming (from axipc) keep alive timer
procedure TAXIpcWrapper.Timer1Timer(Sender: TObject);
var iDummy: integer;
begin
   try
     Timer1.Enabled := false;
     iDummy := AXIpc1.Status;
     Inc(iDummy);
   finally
     Timer1.Enabled := true;
   end;
end;

// ** the dispatch (to giscomm) keep alive timer
procedure TAXIpcWrapper.Timer2Timer(Sender: TObject);
begin
   try
     Timer2.Enabled := false;
     // call ourself again if more cmds available
     if tlSaveCmdList.Count > 0 then begin
        PostMessage(handle,WM_USER+123,0,0);
     end;
   finally
     Timer2.Enabled := true;
   end;

end;

end.

