
{*******************************************************}
{                                                       }
{       Turbo Pascal Version 7.0                        }
{       Standard Objects Unit                           }
{                                                       }
{       Copyright (c) 1992 Borland International        }
{                                                       }
{*******************************************************}

unit Objects;

{$IFDEF WIN32}

{$H-}

interface

const

{ TOldStream access modes }

  stCreate    = $3C00;           { Create new file }
  stOpenRead  = $3D00;           { Read access only }
  stOpenWrite = $3D01;           { Write access only }
  stOpen      = $3D02;           { Read and write access }

{ TOldStream error codes }

  stOk         =  0;              { No error }
  stError      = -1;              { Access error }
  stInitError  = -2;              { Cannot initialize stream }
  stReadError  = -3;              { Read beyond end of stream }
  stWriteError = -4;              { Cannot expand stream }
  stGetError   = -5;              { Get of unregistered object type }
  stPutError   = -6;              { Put of unregistered object type }

{ Maximum TCollection size }

  MaxCollectionSize = MaxLongInt Div SizeOf(Pointer);  

{ TCollection error codes }

  coIndexError = -1;              { Index out of range }
  coOverflow   = -2;              { Overflow }

{ VMT header size }

  vmtHeaderSize = 8;

type

{ Type conversion records }

  WordRec = record
    Lo, Hi: Byte;
  end;

  LongRec = record
    Lo, Hi: Word;
  end;

  PtrRec = record
    Ofs, Seg: Word;
  end;

{ String pointers }

{ Character set type }

  PCharSet = ^TCharSet;
  TCharSet = set of Char;

{ General arrays }

  PByteArray = ^TByteArray;
  TByteArray = array[0..32767] of Byte;

  PWordArray = ^TWordArray;
  TWordArray = array[0..16383] of Word;

{ TObject base object }

  POldObject = ^TOldObject;
  TOldObject = object
    constructor Init;
    procedure Free;
    destructor Done; virtual;
  end;

{ TOldStreamRec }

  PStreamRec = ^TStreamRec;
  TStreamRec = record
    ObjType: Word;
    VmtLink: Pointer;
    Load: Pointer;
    Store: Pointer;
    Next: PStreamRec;
  end;

{ TOldStream }

  POldStream = ^TOldStream;
  TOldStream = object(TOldObject)
    Status: Integer;
    ErrorInfo: Integer;
    constructor Init;
    procedure CopyFrom(var S: TOldStream; Count: Longint);
    procedure Error(Code, Info: Integer); virtual;
    procedure Flush; virtual;
    function Get: POldObject;
    function GetPos: Longint; virtual;
    function GetSize: Longint; virtual;
    procedure Put(P: POldObject);
    procedure Read(var Buf; Count: Integer); virtual;
    function ReadString:String;
    function ReadStr: PString;
    procedure Reset;
    procedure Seek(Pos: Longint); virtual;
    function StrRead: PChar;
    procedure StrWrite(P: PChar);
    procedure Truncate; virtual;
    procedure Write(const Buf; Count: Integer); virtual;
    procedure WriteStr(P: PString);
    procedure WriteString(Const S: String);
  end;

{ DOS file name string }

  FNameStr = PChar;

{ TBufStream }

  PBufStream = ^TBufStream;
  TBufStream = object(TOldStream)
    DataFile    : File;
    Mode        : Integer;
    Buffer      : Pointer;
    BufSize     : Integer;
    BufPtr      : Integer;
    BufEnd      : Integer;
    constructor Init(FileName: FNameStr; AMode, Size: Integer);
    destructor Done; virtual;
    procedure Flush; virtual;
    function GetPos: Longint; virtual;
    function GetSize: Longint; virtual;
    procedure Read(var Buf; Count: Integer); virtual;
    procedure Seek(Pos: Longint); virtual;
    procedure Write(const Buf; Count: Integer); virtual;
  end;

{ TOldMemoryStream }

  POldMemoryStream = ^TOldMemoryStream;
  TOldMemoryStream = object(TOldStream)
  private
    FCapacity: Longint;
    FMemory: Pointer;
    FSize, FPosition: Longint;
  protected
    procedure SetPointer(Ptr: Pointer; Size: Longint);
    procedure SetCapacity(NewCapacity: Longint);
    function Realloc(var NewCapacity: Longint): Pointer; virtual;
  public
    constructor Init(ALimit: Longint; ABlockSize: Integer);
    destructor Done; virtual;
    function GetPos: Longint; virtual;
    function GetSize: Longint; virtual;
    procedure Read(var Buf; Count: Integer); virtual;
    procedure Seek(Pos: Longint); virtual;
    procedure Truncate; virtual;
    procedure Write(const Buf; Count: Integer); virtual;
  end;
        
{ TCollection types }

  PItemList = ^TItemList;
  TItemList = array[0..MaxCollectionSize - 1] of Pointer;

{ TCollection object }

  PCollection = ^TCollection;
  TCollection = object(TOldObject)
    Items: PItemList;
    Count: Integer;
    Limit: Integer;
    Delta: SmallInt;
    constructor Init(ALimit, ADelta: Integer);
    constructor Load(var S: TOldStream);
    destructor Done; virtual;
    function At(Index: Integer): Pointer;
    procedure AtDelete(Index: Integer);
    procedure AtFree(Index: Integer);
    procedure AtInsert(Index: Integer; Item: Pointer);
    procedure AtPut(Index: Integer; Item: Pointer);
    procedure Delete(Item: Pointer);
    procedure DeleteAll;
    procedure Error(Code, Info: Integer); virtual;
    function FirstThat(Test: Pointer): Pointer;
    procedure ForEach(Action: Pointer);
    procedure Free(Item: Pointer);
    procedure FreeAll;
    procedure FreeItem(Item: Pointer); virtual;
    function GetItem(var S: TOldStream): Pointer; virtual;
    function IndexOf(Item: Pointer): Integer; virtual;
    procedure Insert(Item: Pointer); virtual;
    function LastThat(Test: Pointer): Pointer; 
    procedure Pack;
    procedure PutItem(var S: TOldStream; Item: Pointer); virtual;
    procedure SetLimit(ALimit: Integer); virtual;
    procedure Store(var S: TOldStream);
  end;

{ TSortedCollection object }

  PSortedCollection = ^TSortedCollection;
  TSortedCollection = object(TCollection)
    Duplicates: Boolean;
    constructor Init(ALimit, ADelta: Integer);
    constructor Load(var S: TOldStream);
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    function IndexOf(Item: Pointer): Integer; virtual;
    procedure Insert(Item: Pointer); virtual;
    function KeyOf(Item: Pointer): Pointer; virtual;
    function Search(Key: Pointer; var Index: Integer): Boolean; virtual;
    procedure Store(var S: TOldStream);
  end;

{ TStringCollection object }

  PStringCollection = ^TStringCollection;
  TStringCollection = object(TSortedCollection)
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    procedure FreeItem(Item: Pointer); virtual;
    function GetItem(var S: TOldStream): Pointer; virtual;
    procedure PutItem(var S: TOldStream; Item: Pointer); virtual;
  end;

{ TStrCollection object }

  PStrCollection = ^TStrCollection;
  TStrCollection = object(TSortedCollection)
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    procedure FreeItem(Item: Pointer); virtual;
    function GetItem(var S: TOldStream): Pointer; virtual;
    procedure PutItem(var S: TOldStream; Item: Pointer); virtual;
  end;

{ Stream routines }

function NewOfs(Const Ptr:Pointer):Pointer;

procedure RegisterType(var S: TStreamRec);

{ Abstract notification procedure }

procedure Abstract;

{ Objects registration procedure }

procedure RegisterObjects;

const

{ Stream error procedure }

  StreamError: Pointer = nil;

{ Stream registration records }

const
  RCollection: TStreamRec = (
    ObjType: 50;
    VmtLink: TypeOf(TCollection);
    Load: @TCollection.Load;
    Store: @TCollection.Store);

const
  RStringCollection: TStreamRec = (
    ObjType: 51;
    VmtLink: TypeOf(TStringCollection);
    Load: @TStringCollection.Load;
    Store: @TStringCollection.Store);

const
  RStrCollection: TStreamRec = (
    ObjType: 69;
    VmtLink: TypeOf(TStrCollection);
    Load:    @TStrCollection.Load;
    Store:   @TStrCollection.Store);

implementation

uses WinProcs, SysUtils, Dialogs;

  {$DEFINE NewExeFormat}

procedure Abstract;
begin
  RunError(211);
end;

function NewOfs(Const Ptr:Pointer):Pointer;
  begin
    Result:=Ptr;
  end;

{ TObject }

constructor TOldObject.Init;
type
  PImage=^Image;
  Image = record
    Link: Word;
    Data: Word;
  end;
begin
  {  FillChar(PImage(@Self)^.Data, SizeOf(Self) - 4, 0);}
end;

{ Shorthand procedure for a done/dispose }

procedure TOldObject.Free;
begin
  Dispose(POldObject(@Self), Done);
end;

destructor TOldObject.Done;
begin
end;

{ TOldStream type registration routines }

const
  StreamTypes : PStreamRec = NIL;

Function TypeToStreamRec
   (
   ObjType         : Pointer
   )
   : PStreamRec;
  begin
    Result:=StreamTypes;
    while (Result<>NIL)
        and (Result^.VMTLink<>ObjType) do
      Result:=Result^.Next;
  end;

Function NumberToStreamRec
   (
   ObjType         : Word
   )
   : PStreamRec;
  begin
    Result:=StreamTypes;
    while (Result<>NIL)
        and (Result^.ObjType<>ObjType) do
      Result:=Result^.Next;
  end;

procedure RegisterError;
begin
  RunError(212);
end;

procedure RegisterType(var S: TStreamRec);
begin
  if NumberToStreamRec(S.ObjType)<>NIL then RegisterError
  else begin
    S.Next:=StreamTypes;
    StreamTypes:=@S;
  end;  
end;

{ TOldStream support routines }

{ TOldStream }

constructor TOldStream.Init;
begin
  TOldObject.Init;
  Status := 0;
  ErrorInfo := 0;
end;

procedure TOldStream.CopyFrom(var S: TOldStream; Count: Longint);
var
  N: Word;
  Buffer: array[0..1023] of Byte;
begin
  while Count > 0 do
  begin
    if Count > SizeOf(Buffer) then N := SizeOf(Buffer) else N := Count;
    S.Read(Buffer, N);
    Write(Buffer, N);
    Dec(Count, N);
  end;
end;

procedure TOldStream.Error(Code, Info: Integer);
type
  TErrorProc = procedure(var S: TOldStream);
begin
  Status := Code;
  ErrorInfo := Info;
  if StreamError <> nil then TErrorProc(StreamError)(Self);
end;

procedure TOldStream.Flush;
begin
end;

var Last:Word;
function TOldStream.Get: POldObject;
var Info           : PStreamRec;
    ObjNumber      : Word;
    Cons           : Pointer;
    VMT            : Pointer;
begin
 try
  Read(ObjNumber,SizeOf(ObjNumber));
  if Status<>stOK then Result:=NIL
  else if ObjNumber=0 then Result:=NIL
  else begin
    Info:=NumberToStreamRec(ObjNumber);
    if Info=NIL then begin
      Error(stGetError,ObjNumber);
      Result:=NIL;
    end
    else begin
      Cons:=Info^.Load;
      VMT:=Info^.VMTLink;
      ASM
        MOV ECX,Self
        MOV EDX,VMT
        XOR EAX,EAX
        CALL DWORD PTR Cons
        MOV @Result,EAX
      end;
    end;
  end;
  Last:=ObjNumber;
 except
  on exception do begin
    Result:=NIL;
  end;
 end;
end;

function TOldStream.GetPos: Longint;
begin
  Abstract;
end;

function TOldStream.GetSize: Longint;
begin
  Abstract;
end;

procedure TOldStream.Put(P: POldObject);
var Info           : PStreamRec;
    Cons           : Pointer;
begin
 try
  if P=NIL then Write(P,SizeOf(Word))
  else begin
    Info:=TypeToStreamRec(TypeOf(P^));
    if Info=NIL then Error(stPutError,0)
    else begin
      Write(Info^.ObjType,SizeOf(Info^.ObjType));
      Cons:=Info^.Store;
      ASM
        MOV EDX,Self
        MOV EAX,P
        CALL DWORD PTR Cons
      end;
    end;
  end;
 except
  on exception do begin
    ;
  end;
 end;
end;

procedure TOldStream.Read(var Buf; Count: Integer);
begin
  Abstract;
end;

function TOldStream.ReadStr: PString;
var
  L: Byte;
  P: Array[0..255] of Char;
begin
  Read(L, 1);
  if L > 0 then begin
    Read(P,L);
    P[L]:=#0;
    Result:=NewStr(StrPas(P));
  end
  else ReadStr := nil;
end;

procedure TOldStream.Reset;
begin
  Status := 0;
  ErrorInfo := 0;
end;

procedure TOldStream.Seek(Pos: Longint);
begin
  Abstract;
end;

function TOldStream.StrRead: PChar;
var
  L: Word;
  P: PChar;
begin
  Read(L, SizeOf(Word));
  if L = 0 then StrRead := nil else
  begin
    P:=StrAlloc(L+1);
    Read(P[0], L);
    P[L] := #0;
    StrRead := P;
  end;
end;

procedure TOldStream.StrWrite(P: PChar);
var
  L: Word;
begin
  if P = nil then L := 0 else L := StrLen(P);
  Write(L, SizeOf(Word));
  if P <> nil then Write(P[0], L);
end;

procedure TOldStream.Truncate;
begin
  Abstract;
end;

procedure TOldStream.Write(const Buf; Count: Integer);
begin
  Abstract;
end;

procedure TOldStream.WriteStr(P: PString);
var Str            : String;
begin
  if P<>NIL then begin
    Str:=P^;
    Write(Str,Length(Str)+1);
  end
  else begin
    Str:='';
    Write(Str,1);
  end;  
end;

procedure TOldStream.WriteString(Const S: String);
begin
  Write(S,Length(S)+1);
end;

function TOldStream.ReadString:String;
begin
  Read(Result[0],1);
  Read(Result[1],Byte(Result[0]));
end;

{ TBufStream }

Constructor TBufStream.Init
   (
   FileName        : FNameStr;
   AMode           : Integer;
   Size            : Integer
   );
  begin
    Mode:=AMode;
    BufSize:=Size;
    Status:=stOK;
    GetMem(Buffer,Size);
    // read IOResult to reset it and prevent from wrong results
    ErrorInfo:=IOResult;
    Assign(DataFile,FileName);
    {$I-}
    case Mode of
      stCreate   : Rewrite(DataFile,1);
      stOpenRead : begin
          FileMode:=0;
          System.Reset(DataFile,1);
          FileMode:=2;     
        end;
    end;
    {$I+}
    ErrorInfo:=IOResult;
    if ErrorInfo<>0 then Status:=stInitError;
    BufPtr:=0;
    BufEnd:=0;
  end;

Destructor TBufStream.Done;
  begin
    Flush;
    if Buffer<>NIL then FreeMem(Buffer,BufSize);
    if Status<>stInitError then Close(DataFile);
  end;

Procedure TBufStream.Flush;
  var Written      : LongInt;
  begin
    if Mode<>stOpenRead then begin
      BlockWrite(DataFile,Buffer^,BufEnd,Written);
      ErrorInfo:=IOResult;
      if Written<BufEnd then Status:=stReadError;
    end;
    BufPtr:=0;
    BufEnd:=0;
  end;

Function TBufStream.GetPos
   : LongInt;
  begin
    Result:=FilePos(DataFile)-BufEnd+BufPtr;
  end;

Function TBufStream.GetSize
   : LongInt;
  begin
    GetSize:=FileSize(DataFile);
  end;

Type PBA           = ^TByteArray;  

Procedure TBufStream.Read
   (
   var Buf         ;
   Count           : Integer
   );
  var Cnt          : Integer;
      Addr         : Integer;
      Data         : TByteArray Absolute Buf;
      BytesRead    : LongInt;
  begin
    if Status<>stOK then Exit;
    Addr:=0;
    while BufPtr+Count>BufEnd do begin
      Cnt:=BufEnd-BufPtr;
      Move(PBA(Buffer)^[BufPtr],Data[Addr],Cnt);
      BlockRead(DataFile,Buffer^,BufSize,BytesRead);
      ErrorInfo:=IOResult;
      if IOResult<>0 then Status:=stReadError;
      BufEnd:=BytesRead;
      BufPtr:=0;
      if BytesRead=0 then Status:=stReadError;
      if (Status<>stOK) then Exit;
      Dec(Count,Cnt);
      Inc(Addr,Cnt);
    end;
    Move(PBA(Buffer)^[BufPtr],Data[Addr],Count);
    Inc(BufPtr,Count);
  end;

Procedure TBufStream.Seek
   (
   Pos             : LongInt
   );
  begin
    if Status<>stOK then Exit;
    if Mode<>stOpenRead then Flush;
    System.Seek(DataFile,Pos);
    BufPtr:=0;
    BufEnd:=0;
  end;

Procedure TBufStream.Write
   (
   Const Buf         ;
   Count           : Integer
   );
  var Cnt          : Integer;
      Addr         : Integer;
      Data         : TByteArray Absolute Buf;
  begin
    if Status<>stOK then Exit;
    Addr:=0;
    while BufPtr+Count>BufSize do begin
      Cnt:=BufSize-BufPtr;
      Move(Data[Addr],PBA(Buffer)^[BufPtr],Cnt);
      Inc(BufPtr,Cnt);
      BufEnd:=BufPtr;
      Flush;
      Dec(Count,Cnt);
      Inc(Addr,Cnt);
    end;
    Move(Data[Addr],PBA(Buffer)^[BufPtr],Count);
    Inc(BufPtr,Count);
    BufEnd:=BufPtr;
  end;

{ TOldMemoryStream }

const
  MemoryDelta = $2000; { Must be a power of 2 }

constructor TOldMemoryStream.Init(ALimit: Longint; ABlockSize: Integer);
begin
  inherited Init;
  FCapacity:=0;
  FMemory:=NIL;
  FSize:=0;
  FPosition:=0;
  SetCapacity(ALimit);
end;

destructor TOldMemoryStream.Done;
begin
  SetCapacity(0);
  inherited Done;
end;

procedure TOldMemoryStream.SetCapacity(NewCapacity: Longint);
begin
  SetPointer(Realloc(NewCapacity), FSize);
  FCapacity := NewCapacity;
end;

function TOldMemoryStream.Realloc(var NewCapacity: Longint): Pointer;
begin
  if NewCapacity > 0 then
    NewCapacity := (NewCapacity + (MemoryDelta - 1)) and not (MemoryDelta - 1);
  Result := FMemory;
  if NewCapacity <> FCapacity then
  begin
    if NewCapacity = 0 then
    begin
      GlobalFreePtr(FMemory);
      Result := nil;
    end else
    begin
      if FCapacity = 0 then
        Result := GlobalAllocPtr(HeapAllocFlags, NewCapacity)
      else
        Result := GlobalReallocPtr(FMemory, NewCapacity, HeapAllocFlags);
      if Result = nil then Error(stError,0);
    end;
  end;
end;

function TOldMemoryStream.GetPos: Longint;
begin
  if Status<>0 then Result:=-1
  else Result:=FPosition;
end;

function TOldMemoryStream.GetSize: Longint;
begin
  if Status<>stOK then Result:=-1
  else Result:=FSize;
end;

procedure TOldMemoryStream.Read(var Buf; Count: Integer);
var ACount : LongInt;
begin
  if (FPosition >= 0) and (Count >= 0) then
  begin
    ACount := FSize - FPosition;
    if ACount<0 then Error(stReadError,0)
    else begin
      if ACount > Count then ACount := Count;
      Move(Pointer(Longint(FMemory) + FPosition)^, Buf, ACount);
      Inc(FPosition, ACount);
    end;
  end;
end;

procedure TOldMemoryStream.Seek(Pos: Longint);
begin
  FPosition:=Pos;
end;

procedure TOldMemoryStream.SetPointer(Ptr: Pointer; Size: Longint);
begin
  FMemory := Ptr;
  FSize := Size;
end;

procedure TOldMemoryStream.Truncate;
begin
end;

procedure TOldMemoryStream.Write(const Buf; Count: Integer);
var
  Pos: Longint;
begin
  if (FPosition >= 0) and (Count >= 0) then
  begin
    Pos := FPosition + Count;
    if Pos > 0 then
    begin
      if Pos > FSize then
      begin
        if Pos > FCapacity then
          SetCapacity(Pos);
        FSize := Pos;
      end;
      System.Move(Buf, Pointer(Longint(FMemory) + FPosition)^, Count);
      FPosition := Pos;
      Exit;
    end;
  end;
end;

{ TCollection }

constructor TCollection.Init(ALimit, ADelta: Integer);
begin
  TOldObject.Init;
  Items := nil;
  Count := 0;
  Limit := 0;
  Delta := ADelta;
  SetLimit(ALimit);
end;

constructor TCollection.Load(var S: TOldStream);
var C, I: Integer;
    Cnt : SmallInt;
    OldData : Array[0..1] of SmallInt;
begin
  S.Read(Cnt,SizeOf(Cnt));
  if Cnt<>-1 then begin
    Count:=Cnt;
    S.Read(OldData,SizeOf(SmallInt)*2);
    Limit:=OldData[0];
    Delta:=OldData[1];
  end
  else S.Read(Count,SizeOf(Integer)*2+SizeOf(SmallInt));
  Items := nil;
  C := Count;
  I := Limit;
  Count := 0;
  Limit := 0;
  SetLimit(I);
  Count := C;
  for I := 0 to C - 1 do AtPut(I, GetItem(S));
end;

destructor TCollection.Done;
begin
  FreeAll;
  SetLimit(0);
end;

function TCollection.At(Index: Integer): Pointer;
begin
  try
    if Index>=Count then Error(coIndexError,0)
     else Result:=Items^[Index];
  except
     on E: Exception do begin
        ShowMessage(E.message);
     end;
  end;
end;

procedure TCollection.AtDelete(Index: Integer);
begin
  if Index>=Count then Error(coIndexError,0)
  else begin
    Move(Items^[Index+1],Items^[Index],(Count-Index-1)*SizeOf(Pointer));
    Dec(Count);
  end;
end;

procedure TCollection.AtFree(Index: Integer);
var
  Item: Pointer;
begin
  Item := At(Index);
  AtDelete(Index);
  FreeItem(Item);
end;

procedure TCollection.AtInsert(Index: Integer; Item: Pointer);
begin
  if Index>Count then Error(coIndexError,0)
  else begin
    if Count>=Limit then SetLimit(Limit+Delta);
    Move(Items^[Index],Items^[Index+1],(Count-Index)*SizeOf(Pointer));
    Items^[Index]:=Item;
    Inc(Count);
  end;
end;

procedure TCollection.AtPut(Index: Integer; Item: Pointer);
begin
  if Index>=Count then Error(coIndexError,0)
  else Items^[Index]:=Item;
end;

procedure TCollection.Delete(Item: Pointer);
begin
  AtDelete(IndexOf(Item));
end;

procedure TCollection.DeleteAll;
begin
  Count := 0;
end;

procedure TCollection.Error(Code, Info: Integer);
begin
  RunError(212 - Code);
end;

function TCollection.FirstThat(Test: Pointer): Pointer;
var Cnt       : Integer;
    Item      : Pointer;
begin
  Result:=NIL; 
  Cnt:=0;
  while (Cnt<Count) do begin
    Item:=At(Cnt);
    ASM
      MOV     EAX,Item
      PUSH    DWORD PTR [EBP]
      CALL    DWORD PTR Test
      ADD     ESP,4
      CMP AL,0
      JE @@1
      MOV EAX,Item
      MOV @Result,EAX
      MOV Cnt,$7FFE
    @@1: INC Cnt
    end;
  end;
end;

procedure TCollection.ForEach(Action: Pointer);
var Cnt       : Integer;             
    Item      : Pointer;
begin
 try
  for Cnt:=0 to Count-1 do begin
    Item:=At(Cnt);
    ASM
      MOV     EAX,Item
      PUSH    DWORD PTR [EBP]
      CALL    DWORD PTR Action
      ADD     ESP,4
    end;
  end;
 except
  on exception do begin
    ;
  end;
 end;
end;

procedure TCollection.Free(Item: Pointer);
begin
  Delete(Item);
  FreeItem(Item);
end;

procedure TCollection.FreeAll;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do FreeItem(At(I));
  Count := 0;
end;

procedure TCollection.FreeItem(Item: Pointer);
begin
  if Item <> nil then Dispose(POldObject(Item), Done);
end;
 
function TCollection.GetItem(var S: TOldStream): Pointer;
begin
  GetItem := S.Get;
end;

function TCollection.IndexOf(Item: Pointer): Integer;
var Cnt       : Integer;
begin
  for Cnt:=0 to Count-1 do if Items^[Cnt]=Item then begin
    Result:=Cnt;
    Exit;
  end;
  Result:=-1;
end;

procedure TCollection.Insert(Item: Pointer);
begin
  AtInsert(Count, Item);
end;

function TCollection.LastThat(Test: Pointer): Pointer; 
var Cnt       : Integer;
    Item      : Pointer;
begin
 try
  Cnt:=Count-1;
  while (Cnt>=0) do begin
    Item:=At(Cnt);
    ASM
      MOV EAX,Item
      PUSH DWORD PTR [EBP]
      CALL DWORD PTR Test
      ADD  ESP,4
      CMP AL,0
      JE @@1
      MOV Cnt,0
      MOV EAX,Item
      MOV @Result,EAX
    @@1: DEC Cnt
    end;
  end;
 except
  on exception do begin
    Result:=NIL;
  end;
 end;
end;

procedure TCollection.Pack;
begin
end;

procedure TCollection.PutItem(var S: TOldStream; Item: Pointer);
begin
  S.Put(Item);
end;

procedure TCollection.SetLimit(ALimit: Integer);
var
  AItems: PItemList;
begin
  if ALimit < Count then ALimit := Count;
  if ALimit > MaxCollectionSize then ALimit := MaxCollectionSize;
  if ALimit <> Limit then begin
    if ALimit = 0 then AItems := nil else
    begin
      GetMem(AItems, ALimit * SizeOf(Pointer));
      if (Count <> 0) and (Items <> nil) then
        Move(Items^, AItems^, Count * SizeOf(Pointer));
    end;
    if Limit <> 0 then FreeMem(Items, Limit * SizeOf(Pointer));
    Items := AItems;
    Limit := ALimit;
  end;
end;

procedure TCollection.Store(var S: TOldStream);

procedure DoPutItem(P: Pointer); far;
begin
  PutItem(S, P);
end;
var Cnt : SmallInt;
begin
  Cnt:=-1;
  S.Write(Cnt, SizeOf(SmallInt));
  S.Write(Count,SizeOf(Integer)*2+SizeOf(SmallInt));
  ForEach(@DoPutItem);
end;

{ TSortedCollection }

constructor TSortedCollection.Init(ALimit, ADelta: Integer);
begin
  TCollection.Init(ALimit, ADelta);
  Duplicates := False;
end;

constructor TSortedCollection.Load(var S: TOldStream);
begin
  TCollection.Load(S);
  S.Read(Duplicates, SizeOf(Boolean));
end;

function TSortedCollection.Compare(Key1, Key2: Pointer): Integer;
begin
  Abstract;
end;

function TSortedCollection.IndexOf(Item: Pointer): Integer;
var
  I: Integer;
begin
  IndexOf := -1;
  if Search(KeyOf(Item), I) then
  begin
    if Duplicates then
      while (I < Count) and (Item <> Items^[I]) do Inc(I);
    if I < Count then IndexOf := I;
  end;
end;

procedure TSortedCollection.Insert(Item: Pointer);
var
  I: Integer;
begin
  if not Search(KeyOf(Item), I) or Duplicates then AtInsert(I, Item);
end;

function TSortedCollection.KeyOf(Item: Pointer): Pointer;
begin
  KeyOf := Item;
end;

function TSortedCollection.Search(Key: Pointer; var Index: Integer): Boolean;
var
  L, H, I, C: Integer;
begin
  Search := False;
  L := 0;
  H := Count - 1;
  while L <= H do
  begin
    I := (L + H) shr 1;
    C := Compare(KeyOf(Items^[I]), Key);
    if C < 0 then L := I + 1 else
    begin
      H := I - 1;
      if C = 0 then
      begin
        Search := True;
        if not Duplicates then L := I;
      end;
    end;
  end;
  Index := L;
end;

procedure TSortedCollection.Store(var S: TOldStream);
begin
  TCollection.Store(S);
  S.Write(Duplicates, SizeOf(Boolean));
end;

{ TStringCollection }

function TStringCollection.Compare(Key1, Key2: Pointer): Integer;
begin
  Result:=CompareText(PString(Key1)^,PString(Key2)^);
end;

procedure TStringCollection.FreeItem(Item: Pointer);
begin
  DisposeStr(Item);
end;

function TStringCollection.GetItem(var S: TOldStream): Pointer;
begin
  GetItem := S.ReadStr;
end;

procedure TStringCollection.PutItem(var S: TOldStream; Item: Pointer);
begin
  S.WriteStr(Item);
end;

{ TStrCollection }

function TStrCollection.Compare(Key1, Key2: Pointer): Integer;
begin
  Compare := StrComp(Key1, Key2);
end;

procedure TStrCollection.FreeItem(Item: Pointer);
begin
  StrDispose(Item);
end;

function TStrCollection.GetItem(var S: TOldStream): Pointer;
begin
  GetItem := S.StrRead;
end;

procedure TStrCollection.PutItem(var S: TOldStream; Item: Pointer);
begin
  S.StrWrite(Item);
end;

{ Objects registration procedure }

procedure RegisterObjects;
begin
  RegisterType(RCollection);
  RegisterType(RStringCollection);
  RegisterType(RStrCollection);
end;

{$ELSE}

{$O+,F+,X+,I-,S-}

interface

const

{ TOldStream access modes }

  stCreate    = $3C00;           { Create new file }
  stOpenRead  = $3D00;           { Read access only }
  stOpenWrite = $3D01;           { Write access only }
  stOpen      = $3D02;           { Read and write access }

{ TOldStream error codes }

  stOk         =  0;              { No error }
  stError      = -1;              { Access error }
  stInitError  = -2;              { Cannot initialize stream }
  stReadError  = -3;              { Read beyond end of stream }
  stWriteError = -4;              { Cannot expand stream }
  stGetError   = -5;              { Get of unregistered object type }
  stPutError   = -6;              { Put of unregistered object type }

{ Maximum TCollection size }

  MaxCollectionSize = 65520 div SizeOf(Pointer);

{ TCollection error codes }

  coIndexError = -1;              { Index out of range }
  coOverflow   = -2;              { Overflow }

{ VMT header size }

  vmtHeaderSize = 8;

type

{ Type conversion records }

  WordRec = record
    Lo, Hi: Byte;
  end;

  LongRec = record
    Lo, Hi: Word;
  end;

  PtrRec = record
    Ofs, Seg: Word;
  end;

{ String pointers }

{ Character set type }

  PCharSet = ^TCharSet;
  TCharSet = set of Char;

{ General arrays }

  PByteArray = ^TByteArray;
  TByteArray = array[0..32767] of Byte;

  PWordArray = ^TWordArray;
  TWordArray = array[0..16383] of Word;

{ TOldObject base object }

  POldObject = ^TOldObject;
  TOldObject = object
    constructor Init;
    procedure Free;
    destructor Done; virtual;
  end;

{ TStreamRec }

  PStreamRec = ^TStreamRec;
  TStreamRec = record
    ObjType: Word;
    VmtLink: Word;
    Load: Pointer;
    Store: Pointer;
    Next: Word;
  end;

{ TOldStream }

  POldStream = ^TOldStream;
  TOldStream = object(TOldObject)
    Status: Integer;
    ErrorInfo: Integer;
    constructor Init;
    procedure CopyFrom(var S: TOldStream; Count: Longint);
    procedure Error(Code, Info: Integer); virtual;
    procedure Flush; virtual;
    function Get: POldObject;
    function GetPos: Longint; virtual;
    function GetSize: Longint; virtual;
    procedure Put(P: POldObject);
    procedure Read(var Buf; Count: Word); virtual;
    function ReadStr: PString;
    function ReadString: String;
    procedure Reset;
    procedure Seek(Pos: Longint); virtual;
    function StrRead: PChar;
    procedure StrWrite(P: PChar);
    procedure Truncate; virtual;
    procedure Write(const Buf; Count: Word); virtual;
    procedure WriteStr(P: PString);
    procedure WriteString(const S:String);
  end;

{ DOS file name string }

{$IFDEF Windows}
  FNameStr = PChar;
{$ELSE}
  FNameStr = string[79];
{$ENDIF}

{ TDosStream }

  PDosStream = ^TDosStream;
  TDosStream = object(TOldStream)
    Handle: Word;
    constructor Init(FileName: FNameStr; Mode: Word);
    destructor Done; virtual;
    function GetPos: Longint; virtual;
    function GetSize: Longint; virtual;
    procedure Read(var Buf; Count: Word); virtual;
    procedure Seek(Pos: Longint); virtual;
    procedure Truncate; virtual;
    procedure Write(const Buf; Count: Word); virtual;
  end;

{ TBufStream }

  PBufStream = ^TBufStream;
  TBufStream = object(TDosStream)
    Buffer: Pointer;
    BufSize: Word;
    BufPtr: Word;
    BufEnd: Word;
    constructor Init(FileName: FNameStr; Mode, Size: Word);
    destructor Done; virtual;
    procedure Flush; virtual;
    function GetPos: Longint; virtual;
    function GetSize: Longint; virtual;
    procedure Read(var Buf; Count: Word); virtual;
    procedure Seek(Pos: Longint); virtual;
    procedure Truncate; virtual;
    procedure Write(const Buf; Count: Word); virtual;
  end;

{ TOldMemoryStream }

  POldMemoryStream = ^TOldMemoryStream;
  TOldMemoryStream = object(TOldStream)
    SegCount: Integer;
    SegList: PWordArray;
    CurSeg: Integer;
    BlockSize: Integer;
    Size: Longint;
    Position: Longint;
    constructor Init(ALimit: Longint; ABlockSize: Word);
    destructor Done; virtual;
    function GetPos: Longint; virtual;
    function GetSize: Longint; virtual;
    procedure Read(var Buf; Count: Word); virtual;
    procedure Seek(Pos: Longint); virtual;
    procedure Truncate; virtual;
    procedure Write(const Buf; Count: Word); virtual;
  private
    function ChangeListSize(ALimit: Word): Boolean;
  end;

{ TCollection types }

  PItemList = ^TItemList;
  TItemList = array[0..MaxCollectionSize - 1] of Pointer;

{ TCollection object }

  PCollection = ^TCollection;
  TCollection = object(TOldObject)
    Items: PItemList;
    Count: Integer;
    Limit: Integer;
    Delta: Integer;
    constructor Init(ALimit, ADelta: Integer);
    constructor Load(var S: TOldStream);
    destructor Done; virtual;
    function At(Index: Integer): Pointer;
    procedure AtDelete(Index: Integer);
    procedure AtFree(Index: Integer);
    procedure AtInsert(Index: Integer; Item: Pointer);
    procedure AtPut(Index: Integer; Item: Pointer);
    procedure Delete(Item: Pointer);
    procedure DeleteAll;
    procedure Error(Code, Info: Integer); virtual;
    function FirstThat(Test: Pointer): Pointer;
    procedure ForEach(Action: Pointer);
    procedure Free(Item: Pointer);
    procedure FreeAll;
    procedure FreeItem(Item: Pointer); virtual;
    function GetItem(var S: TOldStream): Pointer; virtual;
    function IndexOf(Item: Pointer): Integer; virtual;
    procedure Insert(Item: Pointer); virtual;
    function LastThat(Test: Pointer): Pointer;
    procedure Pack;
    procedure PutItem(var S: TOldStream; Item: Pointer); virtual;
    procedure SetLimit(ALimit: Integer); virtual;
    procedure Store(var S: TOldStream);
  end;

{ TSortedCollection object }

  PSortedCollection = ^TSortedCollection;
  TSortedCollection = object(TCollection)
    Duplicates: Boolean;
    constructor Init(ALimit, ADelta: Integer);
    constructor Load(var S: TOldStream);
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    function IndexOf(Item: Pointer): Integer; virtual;
    procedure Insert(Item: Pointer); virtual;
    function KeyOf(Item: Pointer): Pointer; virtual;
    function Search(Key: Pointer; var Index: Integer): Boolean; virtual;
    procedure Store(var S: TOldStream);
  end;

{ TStringCollection object }

  PStringCollection = ^TStringCollection;
  TStringCollection = object(TSortedCollection)
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    procedure FreeItem(Item: Pointer); virtual;
    function GetItem(var S: TOldStream): Pointer; virtual;
    procedure PutItem(var S: TOldStream; Item: Pointer); virtual;
  end;

{ TStrCollection object }

  PStrCollection = ^TStrCollection;
  TStrCollection = object(TSortedCollection)
    function Compare(Key1, Key2: Pointer): Integer; virtual;
    procedure FreeItem(Item: Pointer); virtual;
    function GetItem(var S: TOldStream): Pointer; virtual;
    procedure PutItem(var S: TOldStream; Item: Pointer); virtual;
  end;

{ Dynamic string handling routines }

{ Longint routines }

function LongMul(X, Y: Integer): Longint;
inline($5A/$58/$F7/$EA);

function LongDiv(X: Longint; Y: Integer): Integer;
inline($59/$58/$5A/$F7/$F9);

{ Stream routines }

procedure RegisterType(var S: TStreamRec);

{ Abstract notification procedure }

procedure Abstract;

{ Objects registration procedure }

procedure RegisterObjects;

const

{ Stream error procedure }

  StreamError: Pointer = nil;

{ EMS stream state variables }

  EmsCurHandle: Word = $FFFF;
  EmsCurPage: Word = $FFFF;

{ Stream registration records }

const
  RCollection: TStreamRec = (
    ObjType: 50;
    VmtLink: Ofs(TypeOf(TCollection)^);
    Load: @TCollection.Load;
    Store: @TCollection.Store);

const
  RStringCollection: TStreamRec = (
    ObjType: 51;
    VmtLink: Ofs(TypeOf(TStringCollection)^);
    Load: @TStringCollection.Load;
    Store: @TStringCollection.Store);

const
  RStrCollection: TStreamRec = (
    ObjType: 69;
    VmtLink: Ofs(TypeOf(TStrCollection)^);
    Load:    @TStrCollection.Load;
    Store:   @TStrCollection.Store);

implementation

{$IFDEF Windows}
uses WinProcs, SysUtils, OMemory;
{$ELSE}
uses Memory, SysUtils;
{$ENDIF}

{$IFDEF Windows}
  {$DEFINE NewExeFormat}
{$ENDIF}

{$IFDEF DPMI}
  {$DEFINE NewExeFormat}
{$ENDIF}

procedure Abstract;
begin
  RunError(211);
end;

{ TOldObject }

constructor TOldObject.Init;
type
  Image = record
    Link: Word;
    Data: record end;
  end;
begin
end;

{ Shorthand procedure for a done/dispose }

procedure TOldObject.Free;
begin
  Dispose(POldObject(@Self), Done);
end;

destructor TOldObject.Done;
begin
end;

{ TOldStream type registration routines }

const
  StreamTypes: Word = 0;

procedure RegisterError;
begin
  RunError(212);
end;

procedure RegisterType(var S: TStreamRec); assembler;
asm
        MOV     AX,DS
        CMP     AX,S.Word[2]
        JNE     @@1
        MOV     SI,S.Word[0]
        MOV     AX,[SI].TStreamRec.ObjType
        OR      AX,AX
        JE      @@1
        MOV     DI,StreamTypes
        MOV     [SI].TStreamRec.Next,DI
        JMP     @@3
@@1:    JMP     RegisterError
@@2:    CMP     AX,[DI].TStreamRec.ObjType
        JE      @@1
        MOV     DI,[DI].TStreamRec.Next
@@3:    OR      DI,DI
        JNE     @@2
        MOV     StreamTypes,SI
end;

{ TOldStream support routines }

const
  TOldStream_Error = vmtHeaderSize + $04;
  TOldStream_Flush = vmtHeaderSize + $08;
  TOldStream_Read  = vmtHeaderSize + $14;
  TOldStream_Write = vmtHeaderSize + $20;

{ Stream error handler                                  }
{ In    AX    = Error info                              }
{       DX    = Error code                              }
{       ES:DI = Stream object pointer                   }
{ Uses  AX,BX,CX,DX,SI                                  }

procedure DoStreamError; near; assembler;
asm
        PUSH    ES
        PUSH    DI
        PUSH    DX
        PUSH    AX
        PUSH    ES
        PUSH    DI
        MOV     DI,ES:[DI]
        CALL    DWORD PTR [DI].TOldStream_Error
        POP     DI
        POP     ES
end;

{ TOldStream }

constructor TOldStream.Init;
begin
  TOldObject.Init;
  Status := 0;
  ErrorInfo := 0;
end;

procedure TOldStream.CopyFrom(var S: TOldStream; Count: Longint);
var
  N: Word;
  Buffer: array[0..1023] of Byte;
begin
  while Count > 0 do
  begin
    if Count > SizeOf(Buffer) then N := SizeOf(Buffer) else N := Count;
    S.Read(Buffer, N);
    Write(Buffer, N);
    Dec(Count, N);
  end;
end;

procedure TOldStream.Error(Code, Info: Integer);
type
  TErrorProc = procedure(var S: TOldStream);
begin
  Status := Code;
  ErrorInfo := Info;
  if StreamError <> nil then TErrorProc(StreamError)(Self);
end;

procedure TOldStream.Flush;
begin
end;

function TOldStream.Get: POldObject; assembler;
asm
        PUSH    AX
        MOV     AX,SP
        PUSH    SS
        PUSH    AX
        MOV     AX,2
        PUSH    AX
        LES     DI,Self
        PUSH    ES
        PUSH    DI
        MOV     DI,ES:[DI]
        CALL    DWORD PTR [DI].TOldStream_Read
        POP     AX
        OR      AX,AX
        JE      @@3
        MOV     BX,StreamTypes
        JMP     @@2
@@1:    CMP     AX,[BX].TStreamRec.ObjType
        JE      @@4
        MOV     BX,[BX].TStreamRec.Next
@@2:    OR      BX,BX
        JNE     @@1
        LES     DI,Self
        MOV     DX,stGetError
        CALL    DoStreamError
@@3:    XOR     AX,AX
        MOV     DX,AX
        JMP     @@5
@@4:    LES     DI,Self
        PUSH    ES
        PUSH    DI
        PUSH    [BX].TStreamRec.VmtLink
        XOR     AX,AX
        PUSH    AX
        PUSH    AX
        CALL    [BX].TStreamRec.Load
@@5:
end;

function TOldStream.GetPos: Longint;
begin
  Abstract;
end;

function TOldStream.GetSize: Longint;
begin
  Abstract;
end;

procedure TOldStream.Put(P: POldObject); assembler;
asm
        LES     DI,P
        MOV     CX,ES
        OR      CX,DI
        JE      @@4
        MOV     AX,ES:[DI]
        MOV     BX,StreamTypes
        JMP     @@2
@@1:    CMP     AX,[BX].TStreamRec.VmtLink
        JE      @@3
        MOV     BX,[BX].TStreamRec.Next
@@2:    OR      BX,BX
        JNE     @@1
        LES     DI,Self
        MOV     DX,stPutError
        CALL    DoStreamError
        JMP     @@5
@@3:    MOV     CX,[BX].TStreamRec.ObjType
@@4:    PUSH    BX
        PUSH    CX
        MOV     AX,SP
        PUSH    SS
        PUSH    AX
        MOV     AX,2
        PUSH    AX
        LES     DI,Self
        PUSH    ES
        PUSH    DI
        MOV     DI,ES:[DI]
        CALL    DWORD PTR [DI].TOldStream_Write
        POP     CX
        POP     BX
        JCXZ    @@5
        LES     DI,Self
        PUSH    ES
        PUSH    DI
        PUSH    P.Word[2]
        PUSH    P.Word[0]
        CALL    [BX].TStreamRec.Store
@@5:
end;

procedure TOldStream.Read(var Buf; Count: Word);
begin
  Abstract;
end;

function TOldStream.ReadStr: PString;
var
  L: Byte;
  P: PString;
begin
  Read(L, 1);
  if L > 0 then
  begin
    GetMem(P, L + 1);
    P^[0] := Char(L);
    Read(P^[1], L);
    ReadStr := P;
  end else ReadStr := nil;
end;

procedure TOldStream.Reset;
begin
  Status := 0;
  ErrorInfo := 0;
end;

procedure TOldStream.Seek(Pos: Longint);
begin
  Abstract;
end;

function TOldStream.StrRead: PChar;
var
  L: Word;
  P: PChar;
begin
  Read(L, SizeOf(Word));
  if L = 0 then StrRead := nil else
  begin
    P:=StrAlloc(L+1);
    Read(P[0], L);
    P[L] := #0;
    StrRead := P;                             
  end;
end;

procedure TOldStream.StrWrite(P: PChar);
var
  L: Word;
begin
  if P = nil then L := 0 else L := StrLen(P);
  Write(L, SizeOf(Word));
  if P <> nil then Write(P[0], L);
end;

procedure TOldStream.Truncate;
begin
  Abstract;
end;

procedure TOldStream.Write(const Buf; Count: Word);
begin
  Abstract;
end;

procedure TOldStream.WriteStr(P: PString);
const
  Empty: String[1] = '';
begin
  if P <> nil then Write(P^, Length(P^) + 1) else Write(Empty, 1);
end;

procedure TOldStream.WriteString(Const S: String);
begin
  Write(S,Length(S)+1);
end;

function TOldStream.ReadString:String;
begin
  Read(Result[0],1);
  Read(Result[1],Byte(Result[0]));
end;

{ TDosStream }

constructor TDosStream.Init(FileName: FNameStr; Mode: Word); assembler;
var
  NameBuf: array[0..79] of Char;
asm
        XOR     AX,AX
        PUSH    AX
        LES     DI,Self
        PUSH    ES
        PUSH    DI
        CALL    TOldStream.Init
{$IFDEF Windows}
	LEA	DI,NameBuf
	PUSH	SS
	PUSH	DI
	LES	DI,FileName
	PUSH	ES
	PUSH	DI
	MOV	AX,79
	PUSH	AX
	CALL	StrLCopy
	PUSH	DX
	PUSH	AX
	PUSH	DX
	PUSH	AX
	CALL	AnsiToOem
	PUSH	DS
	LEA	DX,NameBuf
{$ELSE}
        PUSH    DS
        LDS     SI,FileName
        LEA     DI,NameBuf
        MOV     DX,DI
        PUSH    SS
        POP     ES
        CLD
        LODSB
        CMP     AL,79
        JB      @@1
        MOV     AL,79
@@1:    CBW
        XCHG    AX,CX
        REP     MOVSB
        XCHG    AX,CX
        STOSB
{$ENDIF}
        PUSH    SS
        POP     DS
        XOR     CX,CX
        MOV     AX,Mode
        INT     21H
        POP     DS
        JNC     @@2
        LES     DI,Self
        MOV     DX,stInitError
        CALL    DoStreamError
        MOV     AX,-1
@@2:    LES     DI,Self
        MOV     ES:[DI].TDosStream.Handle,AX
end;

destructor TDosStream.Done; assembler;
asm
        LES     DI,Self
        MOV     BX,ES:[DI].TDosStream.Handle
        CMP     BX,-1
        JE      @@1
        MOV     AH,3EH
        INT     21H
@@1:    XOR     AX,AX
        PUSH    AX
        PUSH    ES
        PUSH    DI
        CALL    TOldStream.Done
end;

function TDosStream.GetPos: Longint; assembler;
asm
        LES     DI,Self
        XOR     DX,DX
        CMP     DX,ES:[DI].TDosStream.Status
        JNE     @@1
        MOV     CX,DX
        MOV     BX,ES:[DI].TDosStream.Handle
        MOV     AX,4201H
        INT     21H
        JNC     @@2
        MOV     DX,stError
        CALL    DoStreamError
@@1:    MOV     AX,-1
        CWD
@@2:
end;

function TDosStream.GetSize: Longint; assembler;
asm
        LES     DI,Self
        XOR     DX,DX
        CMP     DX,ES:[DI].TDosStream.Status
        JNE     @@1
        MOV     CX,DX
        MOV     BX,ES:[DI].TDosStream.Handle
        MOV     AX,4201H
        INT     21H
        PUSH    DX
        PUSH    AX
        XOR     DX,DX
        MOV     CX,DX
        MOV     AX,4202H
        INT     21H
        POP     SI
        POP     CX
        PUSH    DX
        PUSH    AX
        MOV     DX,SI
        MOV     AX,4200H
        INT     21H
        POP     AX
        POP     DX
        JNC     @@2
        MOV     DX,stError
        CALL    DoStreamError
@@1:    MOV     AX,-1
        CWD
@@2:
end;

procedure TDosStream.Read(var Buf; Count: Word); assembler;
asm
        LES     DI,Self
        CMP     ES:[DI].TDosStream.Status,0
        JNE     @@2
        PUSH    DS
        LDS     DX,Buf
        MOV     CX,Count
        MOV     BX,ES:[DI].TDosStream.Handle
        MOV     AH,3FH
        INT     21H
        POP     DS
        MOV     DX,stError
        JC      @@1
        CMP     AX,CX
        JE      @@3
        XOR     AX,AX
        MOV     DX,stReadError
@@1:    CALL    DoStreamError
@@2:    LES     DI,Buf
        MOV     CX,Count
        XOR     AL,AL
        CLD
        REP     STOSB
@@3:
end;

procedure TDosStream.Seek(Pos: Longint); assembler;
asm
        LES     DI,Self
        CMP     ES:[DI].TDosStream.Status,0
        JNE     @@2
        MOV     DX,Pos.Word[0]
        MOV     CX,Pos.Word[2]
        OR      CX,CX
        JNS     @@1
        XOR     DX,DX
        XOR     CX,CX
@@1:    MOV     BX,ES:[DI].TDosStream.Handle
        MOV     AX,4200H
        INT     21H
        JNC     @@2
        MOV     DX,stError
        CALL    DoStreamError
@@2:
end;

procedure TDosStream.Truncate; assembler;
asm
        LES     DI,Self
        XOR     CX,CX
        CMP     CX,ES:[DI].TDosStream.Status
        JNE     @@1
        MOV     BX,ES:[DI].TDosStream.Handle
        MOV     AH,40H
        INT     21H
        JNC     @@1
        MOV     DX,stError
        CALL    DoStreamError
@@1:
end;

procedure TDosStream.Write(const Buf; Count: Word); assembler;
asm
        LES     DI,Self
        CMP     ES:[DI].TDosStream.Status,0
        JNE     @@2
        PUSH    DS
        LDS     DX,Buf
        MOV     CX,Count
        MOV     BX,ES:[DI].TDosStream.Handle
        MOV     AH,40H
        INT     21H
        POP     DS
        MOV     DX,stError
        JC      @@1
        CMP     AX,CX
        JE      @@2
        XOR     AX,AX
        MOV     DX,stWriteError
@@1:    CALL    DoStreamError
@@2:
end;

{ TBufStream }

{ Flush TBufStream buffer                               }
{ In    AL    = Flush mode (0=Read, 1=Write, 2=Both)    }
{       ES:DI = TBufStream pointer                      }
{ Out   ZF    = Status test                             }

procedure FlushBuffer; near; assembler;
asm
        MOV     CX,ES:[DI].TBufStream.BufPtr
        SUB     CX,ES:[DI].TBufStream.BufEnd
        JE      @@3
        MOV     BX,ES:[DI].TDosStream.Handle
        JA      @@1
        CMP     AL,1
        JE      @@4
        MOV     DX,CX
        MOV     CX,-1
        MOV     AX,4201H
        INT     21H
        JMP     @@3
@@1:    CMP     AL,0
        JE      @@4
        PUSH    DS
        LDS     DX,ES:[DI].TBufStream.Buffer
        MOV     AH,40H
        INT     21H
        POP     DS
        MOV     DX,stError
        JC      @@2
        CMP     AX,CX
        JE      @@3
        XOR     AX,AX
        MOV     DX,stWriteError
@@2:    CALL    DoStreamError
@@3:    XOR     AX,AX
        MOV     ES:[DI].TBufStream.BufPtr,AX
        MOV     ES:[DI].TBufStream.BufEnd,AX
        CMP     AX,ES:[DI].TOldStream.Status
@@4:
end;

constructor TBufStream.Init(FileName: FNameStr; Mode, Size: Word);
begin
  TDosStream.Init(FileName, Mode);
  BufSize := Size;
  if Size = 0 then Error(stInitError, 0)
  else GetMem(Buffer, Size);
  BufPtr := 0;
  BufEnd := 0;
end;

destructor TBufStream.Done;
begin
  TBufStream.Flush;
  TDosStream.Done;
  FreeMem(Buffer, BufSize);
end;

procedure TBufStream.Flush; assembler;
asm
        LES     DI,Self
        CMP     ES:[DI].TBufStream.Status,0
        JNE     @@1
        MOV     AL,2
        CALL    FlushBuffer
@@1:
end;

function TBufStream.GetPos: Longint; assembler;
asm
        LES     DI,Self
        PUSH    ES
        PUSH    DI
        CALL    TDosStream.GetPos
        OR      DX,DX
        JS      @@1
        LES     DI,Self
        SUB     AX,ES:[DI].TBufStream.BufEnd
        SBB     DX,0
        ADD     AX,ES:[DI].TBufStream.BufPtr
        ADC     DX,0
@@1:
end;

function TBufStream.GetSize: Longint; assembler;
asm
        LES     DI,Self
        PUSH    ES
        PUSH    DI
        PUSH    ES
        PUSH    DI
        CALL    TBufStream.Flush
        CALL    TDosStream.GetSize
end;

procedure TBufStream.Read(var Buf; Count: Word); assembler;
asm
        LES     DI,Self
        CMP     ES:[DI].TBufStream.Status,0
        JNE     @@6
        MOV     AL,1
        CALL    FlushBuffer
        JNE     @@6
        XOR     BX,BX
@@1:    MOV     CX,Count
        SUB     CX,BX
        JE      @@7
        LES     DI,Self
        MOV     AX,ES:[DI].TBufStream.BufEnd
        SUB     AX,ES:[DI].TBufStream.BufPtr
        JA      @@2
        PUSH    DS
        PUSH    CX
        PUSH    BX
        LDS     DX,ES:[DI].TBufStream.Buffer
        MOV     CX,ES:[DI].TBufStream.BufSize
        MOV     BX,ES:[DI].TBufStream.Handle
        MOV     AH,3FH
        INT     21H
        POP     BX
        POP     CX
        POP     DS
        MOV     DX,stError
        JC      @@5
        MOV     ES:[DI].TBufStream.BufPtr,0
        MOV     ES:[DI].TBufStream.BufEnd,AX
        OR      AX,AX
        JE      @@4
@@2:    CMP     CX,AX
        JB      @@3
        MOV     CX,AX
@@3:    PUSH    DS
        LDS     SI,ES:[DI].TBufStream.Buffer
        ADD     SI,ES:[DI].TBufStream.BufPtr
        ADD     ES:[DI].TBufStream.BufPtr,CX
        LES     DI,Buf
        ADD     DI,BX
        ADD     BX,CX
        CLD
        REP     MOVSB
        POP     DS
        JMP     @@1
@@4:    MOV     DX,stReadError
@@5:    CALL    DoStreamError
@@6:    LES     DI,Buf
        MOV     CX,Count
        XOR     AL,AL
        CLD
        REP     STOSB
@@7:
end;

procedure TBufStream.Seek(Pos: Longint); assembler;
asm
        LES     DI,Self
        PUSH    ES
        PUSH    DI
        CALL    TDosStream.GetPos
        OR      DX,DX
        JS      @@2
        LES     DI,Self
        SUB     AX,Pos.Word[0]
        SBB     DX,Pos.Word[2]
        JNE     @@1
        OR      AX,AX
        JE      @@1
        MOV     DX,ES:[DI].TBufStream.BufEnd
        SUB     DX,AX
        JB      @@1
        MOV     ES:[DI].TBufStream.BufPtr,DX
        JMP     @@2
@@1:    PUSH    Pos.Word[2]
        PUSH    Pos.Word[0]
        PUSH    ES
        PUSH    DI
        PUSH    ES
        PUSH    DI
        CALL    TBufStream.Flush
        CALL    TDosStream.Seek
@@2:
end;

procedure TBufStream.Truncate;
begin
  TBufStream.Flush;
  TDosStream.Truncate;
end;

procedure TBufStream.Write(const Buf; Count: Word); assembler;
asm
        LES     DI,Self
        CMP     ES:[DI].TBufStream.Status,0
        JNE     @@4
        MOV     AL,0
        CALL    FlushBuffer
        JNE     @@4
        XOR     DX,DX
@@1:    MOV     CX,Count
        SUB     CX,DX
        JE      @@4
        LES     DI,Self
        MOV     AX,ES:[DI].TBufStream.BufSize
        SUB     AX,ES:[DI].TBufStream.BufPtr
        JA      @@2
        PUSH    CX
        PUSH    DX
        MOV     AL,1
        CALL    FlushBuffer
        POP     DX
        POP     CX
        JNE     @@4
        MOV     AX,ES:[DI].TBufStream.BufSize
@@2:    CMP     CX,AX
        JB      @@3
        MOV     CX,AX
@@3:    PUSH    DS
        MOV     AX,ES:[DI].TBufStream.BufPtr
        ADD     ES:[DI].TBufStream.BufPtr,CX
        LES     DI,ES:[DI].TBufStream.Buffer
        ADD     DI,AX
        LDS     SI,Buf
        ADD     SI,DX
        ADD     DX,CX
        CLD
        REP     MOVSB
        POP     DS
        JMP     @@1
@@4:
end;

{ TOldMemoryStream }

const
  MaxSegArraySize = 16384;

{$IFDEF NewExeFormat}

  DefaultBlockSize = $2000;

{$ELSE}

  DefaultBlockSize = $0800;

{$ENDIF}

procedure MemSelectSeg; near; assembler;
asm
        MOV     AX,ES:[DI].TOldMemoryStream.Position.Word[0]
        MOV     DX,ES:[DI].TOldMemoryStream.Position.Word[2]
        MOV     CX,ES:[DI].TOldMemoryStream.BlockSize
        DIV     CX
        SUB     CX,DX
        MOV     SI,DX
        SHL     AX,1
        MOV     ES:[DI].TOldMemoryStream.CurSeg,AX
end;

const
  MemStreamSize = (SizeOf(TOldMemoryStream) - SizeOf(TOldStream)) div 2;

constructor TOldMemoryStream.Init(ALimit: Longint; ABlockSize: Word); assembler;
asm
        XOR     AX,AX
        PUSH    AX
        LES     DI,Self
        PUSH    ES
        PUSH    DI
        CALL    TOldStream.Init
        LES     DI,Self
{$IFDEF Windows}
        XOR     AX,AX
        PUSH    DI
        ADD     DI,OFFSET TOldMemoryStream.SegCount
        MOV     CX,MemStreamSize
        REP     STOSW
        POP     DI
{$ENDIF}
        CMP     ABlockSize,0
        JNZ     @@1
        MOV     ABlockSize,DefaultBlockSize
@@1:    MOV     AX,ALimit.Word[0]
        MOV     DX,ALimit.Word[2]
        DIV     ABlockSize
        NEG     DX
        ADC     AX,0
        MOV     DX,ABlockSize
        MOV     ES:[DI].TOldMemoryStream.BlockSize,DX
        PUSH    AX
        PUSH    ES
        PUSH    DI
        CALL    ChangeListSize
        LES     DI,Self
        OR      AL,AL
        JNZ     @@2
        MOV     DX,stInitError
        CALL    DoStreamError
        MOV     ALimit.Word[0],0
        MOV     ALimit.Word[2],0
@@2:    MOV     AX,ALimit.Word[0]
        MOV     DX,ALimit.Word[2]
        MOV     ES:[DI].TOldMemoryStream.Size.Word[0],AX
        MOV     ES:[DI].TOldMemoryStream.Size.Word[2],DX
end;

destructor TOldMemoryStream.Done;
begin
  ChangeListSize(0);
  inherited Done;
end;

function TOldMemoryStream.ChangeListSize(ALimit: Word): Boolean;
var
  AItems: PWordArray;
  Dif, Term: Word;
  NewBlock: Pointer;
begin
  ChangeListSize := False;
  if ALimit > MaxSegArraySize then ALimit := MaxSegArraySize;
  if ALimit <> SegCount then
  begin
    if ALimit = 0 then AItems := nil else
    begin
      AItems := MemAlloc(ALimit * SizeOf(Word));
      if AItems = nil then Exit;
      FillChar(AItems^, ALimit * SizeOf(Word), 0);
      if (SegCount <> 0) and (SegList <> nil) then
        if SegCount > ALimit then
          Move(SegList^, AItems^, ALimit * SizeOf(Word))
        else
          Move(SegList^, AItems^, SegCount * SizeOf(Word));
    end;
    if ALimit < SegCount then
    begin
      Dif  := ALimit;
      Term := SegCount - 1;
      while Dif <= Term do
      begin
        if SegList^[Dif] <> 0 then
          FreeMem(Ptr(SegList^[Dif], 0), BlockSize);
        Inc(Dif);
      end;
    end
    else
    begin
      Dif := SegCount;
      Term := ALimit - 1;
      while Dif <= Term do
      begin
        NewBlock := MemAllocSeg(BlockSize);
        if NewBlock = nil then Break
        else AItems^[Dif] := PtrRec(NewBlock).Seg;
        Inc(Dif);
      end;
      if Dif = ALimit then
        ChangeListSize := True;
    end;
    if SegCount <> 0 then FreeMem(SegList, SegCount * SizeOf(Word));
    SegList := AItems;
    SegCount := ALimit;
  end else ChangeListSize := True;
end;

function TOldMemoryStream.GetPos: Longint; assembler;
asm
        LES     DI,Self
        CMP     ES:[DI].TOldMemoryStream.Status,0
        JNE     @@1
        MOV     AX,ES:[DI].TOldMemoryStream.Position.Word[0]
        MOV     DX,ES:[DI].TOldMemoryStream.Position.Word[2]
        JMP     @@2
@@1:    MOV     AX,-1
        CWD
@@2:
end;

function TOldMemoryStream.GetSize: Longint; assembler;
asm
        LES     DI,Self
        CMP     ES:[DI].TOldMemoryStream.Status,0
        JNE     @@1
        MOV     AX,ES:[DI].TOldMemoryStream.Size.Word[0]
        MOV     DX,ES:[DI].TOldMemoryStream.Size.Word[2]
        JMP     @@2
@@1:    MOV     AX,-1
        CWD
@@2:
end;

procedure TOldMemoryStream.Read(var Buf; Count: Word); assembler;
asm
        LES     DI,Self
        XOR     BX,BX
        CMP     BX,ES:[DI].TOldMemoryStream.Status
        JNE     @@3
        MOV     AX,ES:[DI].TOldMemoryStream.Position.Word[0]
        MOV     DX,ES:[DI].TOldMemoryStream.Position.Word[2]
        ADD     AX,Count
        ADC     DX,BX
        CMP     DX,ES:[DI].TOldMemoryStream.Size.Word[2]
        JA      @@1
        JB      @@7
        CMP     AX,ES:[DI].TOldMemoryStream.Size.Word[0]
        JBE     @@7
@@1:    XOR     AX,AX
        MOV     DX,stReadError
@@2:    CALL    DoStreamError
@@3:    LES     DI,Buf
        MOV     CX,Count
        XOR     AL,AL
        CLD
        REP     STOSB
        JMP     @@8
@@5:    CALL    MemSelectSeg
        MOV     AX,Count
        SUB     AX,BX
        CMP     CX,AX
        JB      @@6
        MOV     CX,AX
@@6:    ADD     ES:[DI].TOldMemoryStream.Position.Word[0],CX
        ADC     ES:[DI].TOldMemoryStream.Position.Word[2],0
        PUSH    ES
        PUSH    DS
        PUSH    DI
        MOV     DX,ES:[DI].TOldMemoryStream.CurSeg
        LES     DI,ES:[DI].TOldMemoryStream.SegList
        ADD     DI,DX
        MOV     DS,WORD PTR ES:[DI]
        LES     DI,Buf
        ADD     DI,BX
        ADD     BX,CX
        CLD
        REP     MOVSB
        POP     DI
        POP     DS
        POP     ES
@@7:    CMP     BX,Count
        JB      @@5
@@8:
end;

procedure TOldMemoryStream.Seek(Pos: Longint); assembler;
asm
        LES     DI,Self
        MOV     AX,Pos.Word[0]
        MOV     DX,Pos.Word[2]
        OR      DX,DX
        JNS     @@1
        XOR     AX,AX
        CWD
@@1:    MOV     ES:[DI].TOldMemoryStream.Position.Word[0],AX
        MOV     ES:[DI].TOldMemoryStream.Position.Word[2],DX
end;

procedure TOldMemoryStream.Truncate; assembler;
asm
        LES     DI,Self
        XOR     BX,BX
        CMP     ES:[DI].TOldMemoryStream.Status,BX
        JNE     @@2
        MOV     AX,ES:[DI].TOldMemoryStream.Position.Word[0]
        MOV     DX,ES:[DI].TOldMemoryStream.Position.Word[2]
        DIV     ES:[DI].TOldMemoryStream.BlockSize
        NEG     DX
        ADC     AX,BX
        PUSH    AX
        PUSH    ES
        PUSH    DI
        CALL    ChangeListSize
        OR      AL,AL
        JNZ     @@1
        MOV     DX,stError
        CALL    DoStreamError
        JMP     @@2
@@1:    LES     DI,Self
        MOV     AX,ES:[DI].TOldMemoryStream.Position.Word[0]
        MOV     DX,ES:[DI].TOldMemoryStream.Position.Word[2]
        MOV     ES:[DI].TOldMemoryStream.Size.Word[0],AX
        MOV     ES:[DI].TOldMemoryStream.Size.Word[2],DX
@@2:
end;

procedure TOldMemoryStream.Write(const Buf; Count: Word); assembler;
asm
        LES     DI,Self
        XOR     BX,BX
        CMP     BX,ES:[DI].TOldMemoryStream.Status
        JNE     @@7
        MOV     AX,ES:[DI].TOldMemoryStream.Position.Word[0]
        MOV     DX,ES:[DI].TOldMemoryStream.Position.Word[2]
        ADD     AX,Count
        ADC     DX,BX
        DIV     ES:[DI].TOldMemoryStream.BlockSize
        NEG     DX
        ADC     AX,BX
        CMP     AX,ES:[DI].TOldMemoryStream.SegCount
        JBE     @@4
        PUSH    BX
        PUSH    ES
        PUSH    DI
        PUSH    AX
        PUSH    ES
        PUSH    DI
        CALL    ChangeListSize
        POP     DI
        POP     ES
        POP     BX
        OR      AL,AL
        JNZ     @@4
@@1:    MOV     DX,stWriteError
        CALL    DoStreamError
        JMP     @@7
@@2:    CALL    MemSelectSeg
        MOV     AX,Count
        SUB     AX,BX
        CMP     CX,AX
        JB      @@3
        MOV     CX,AX
@@3:    ADD     ES:[DI].TOldMemoryStream.Position.Word[0],CX
        ADC     ES:[DI].TOldMemoryStream.Position.Word[2],0
        PUSH    ES
        PUSH    DS
        PUSH    DI
        MOV     DX,ES:[DI].TOldMemoryStream.CurSeg
        LES     DI,ES:[DI].TOldMemoryStream.SegList
        ADD     DI,DX
        MOV     ES,WORD PTR ES:[DI]
        MOV     DI,SI
        LDS     SI,Buf
        ADD     SI,BX
        ADD     BX,CX
        CLD
        REP     MOVSB
        POP     DI
        POP     DS
        POP     ES
@@4:    CMP     BX,Count
        JB      @@2
@@5:    MOV     AX,ES:[DI].TOldMemoryStream.Position.Word[0]
        MOV     DX,ES:[DI].TOldMemoryStream.Position.Word[2]
        CMP     DX,ES:[DI].TOldMemoryStream.Size.Word[2]
        JB      @@7
        JA      @@6
        CMP     AX,ES:[DI].TOldMemoryStream.Size.Word[0]
        JBE     @@7
@@6:    MOV     ES:[DI].TOldMemoryStream.Size.Word[0],AX
        MOV     ES:[DI].TOldMemoryStream.Size.Word[2],DX
@@7:
end;

{ TCollection }

const
  TCollection_Error    = vmtHeaderSize + $04;
  TCollection_SetLimit = vmtHeaderSize + $1C;

procedure CollectionError; near; assembler;
asm
        PUSH    AX
        PUSH    BX
        PUSH    ES
        PUSH    DI
        MOV     DI,ES:[DI]
        CALL    DWORD PTR [DI].TCollection_Error
end;

constructor TCollection.Init(ALimit, ADelta: Integer);
begin
  TOldObject.Init;
  Items := nil;
  Count := 0;
  Limit := 0;
  Delta := ADelta;
  SetLimit(ALimit);
end;

constructor TCollection.Load(var S: TOldStream);
var
  C, I: Integer;
begin
  S.Read(Count, SizeOf(Integer) * 3);
  Items := nil;
  C := Count;
  I := Limit;
  Count := 0;
  Limit := 0;
  SetLimit(I);
  Count := C;
  for I := 0 to C - 1 do AtPut(I, GetItem(S));
end;

destructor TCollection.Done;
begin
  FreeAll;
  SetLimit(0);
end;

function TCollection.At(Index: Integer): Pointer; assembler;
asm
        LES     DI,Self
        MOV     BX,Index
        OR      BX,BX
        JL      @@1
        CMP     BX,ES:[DI].TCollection.Count
        JGE     @@1
        LES     DI,ES:[DI].TCollection.Items
        SHL     BX,1
        SHL     BX,1
	MOV	AX,ES:[DI+BX]
	MOV	DX,ES:[DI+BX+2]
        JMP     @@2
@@1:    MOV     AX,coIndexError
        CALL    CollectionError
        XOR     AX,AX
        MOV     DX,AX
@@2:
end;

procedure TCollection.AtDelete(Index: Integer); assembler;
asm
        LES     DI,Self
        MOV     BX,Index
        OR      BX,BX
        JL      @@1
        CMP     BX,ES:[DI].TCollection.Count
        JGE     @@1
        DEC     ES:[DI].TCollection.Count
        MOV     CX,ES:[DI].TCollection.Count
        SUB     CX,BX
        JE      @@2
        CLD
        LES     DI,ES:[DI].TCollection.Items
        SHL     BX,1
        SHL     BX,1
        ADD     DI,BX
        LEA     SI,[DI+4]
        SHL     CX,1
        PUSH    DS
        PUSH    ES
        POP     DS
        REP     MOVSW
        POP     DS
        JMP     @@2
@@1:    MOV     AX,coIndexError
        CALL    CollectionError
@@2:
end;

procedure TCollection.AtFree(Index: Integer);
var
  Item: Pointer;
begin
  Item := At(Index);
  AtDelete(Index);
  FreeItem(Item);
end;

procedure TCollection.AtInsert(Index: Integer; Item: Pointer); assembler;
asm
        LES     DI,Self
        MOV     BX,Index
        OR      BX,BX
        JL      @@3
        MOV     CX,ES:[DI].TCollection.Count
        CMP     BX,CX
        JG      @@3
        CMP     CX,ES:[DI].TCollection.Limit
        JNE     @@1
        PUSH    CX
        PUSH    BX
        ADD     CX,ES:[DI].TCollection.Delta
        PUSH    CX
        PUSH    ES
        PUSH    DI
        MOV     DI,ES:[DI]
        CALL    DWORD PTR [DI].TCollection_SetLimit
        POP     BX
        POP     CX
        LES     DI,Self
        CMP     CX,ES:[DI].TCollection.Limit
        JE      @@4
@@1:    INC     ES:[DI].TCollection.Count
        STD
        LES     DI,ES:[DI].TCollection.Items
        SHL     CX,1
        ADD     DI,CX
        ADD     DI,CX
        INC     DI
        INC     DI
        SHL     BX,1
        SUB     CX,BX
        JE      @@2
        LEA     SI,[DI-4]
        PUSH    DS
        PUSH    ES
        POP     DS
        REP     MOVSW
        POP     DS
@@2:    MOV     AX,WORD PTR [Item+2]
        STOSW
        MOV     AX,WORD PTR [Item]
        STOSW
        CLD
        JMP     @@6
@@3:    MOV     AX,coIndexError
        JMP     @@5
@@4:    MOV     AX,coOverflow
        MOV     BX,CX
@@5:    CALL    CollectionError
@@6:
end;

procedure TCollection.AtPut(Index: Integer; Item: Pointer); assembler;
asm
	MOV	AX,Item.Word[0]
        MOV	DX,Item.Word[2]
        LES	DI,Self
        MOV     BX,Index
        OR      BX,BX
        JL      @@1
        CMP     BX,ES:[DI].TCollection.Count
        JGE     @@1
        LES     DI,ES:[DI].TCollection.Items
        SHL     BX,1
        SHL     BX,1
        MOV     ES:[DI+BX],AX
        MOV     ES:[DI+BX+2],DX
        JMP     @@2
@@1:    MOV     AX,coIndexError
        CALL    CollectionError
@@2:
end;

procedure TCollection.Delete(Item: Pointer);
begin
  AtDelete(IndexOf(Item));
end;

procedure TCollection.DeleteAll;
begin
  Count := 0;
end;

procedure TCollection.Error(Code, Info: Integer);
begin
  RunError(212 - Code);
end;

function TCollection.FirstThat(Test: Pointer): Pointer; assembler;
asm
        LES     DI,Self
        MOV     CX,ES:[DI].TCollection.Count
        JCXZ    @@2
        LES     DI,ES:[DI].TCollection.Items
@@1:    PUSH    ES
        PUSH    DI
        PUSH    CX
        PUSH    WORD PTR ES:[DI+2]
        PUSH    WORD PTR ES:[DI]
{$IFDEF Windows}
	MOV	AX,[BP]
	AND	AL,0FEH
	PUSH	AX
{$ELSE}
        PUSH    WORD PTR [BP]
{$ENDIF}
        CALL    Test
        POP     CX
        POP     DI
        POP     ES
        OR      AL,AL
        JNE     @@3
        ADD     DI,4
        LOOP    @@1
@@2:    XOR     AX,AX
        MOV     DX,AX
        JMP     @@4
@@3:	MOV	AX,ES:[DI]
	MOV	DX,ES:[DI+2]
@@4:
end;

procedure TCollection.ForEach(Action: Pointer); assembler;
asm
        LES     DI,Self
        MOV     CX,ES:[DI].TCollection.Count
        JCXZ    @@2
        LES     DI,ES:[DI].TCollection.Items
@@1:    PUSH    ES
        PUSH    DI
        PUSH    CX
        PUSH    WORD PTR ES:[DI+2]
        PUSH    WORD PTR ES:[DI]
{$IFDEF Windows}
	MOV	AX,[BP]
	AND	AL,0FEH
	PUSH	AX
{$ELSE}
        PUSH    WORD PTR [BP]
{$ENDIF}
        CALL    Action
        POP     CX
        POP     DI
        POP     ES
        ADD     DI,4
        LOOP    @@1
@@2:
end;

procedure TCollection.Free(Item: Pointer);
begin
  Delete(Item);
  FreeItem(Item);
end;

procedure TCollection.FreeAll;
var
  I: Integer;
begin
  for I := 0 to Count - 1 do FreeItem(At(I));
  Count := 0;
end;

procedure TCollection.FreeItem(Item: Pointer);
begin
  if Item <> nil then Dispose(POldObject(Item), Done);
end;

function TCollection.GetItem(var S: TOldStream): Pointer;
begin
  GetItem := S.Get;
end;

function TCollection.IndexOf(Item: Pointer): Integer; assembler;
asm
	MOV	AX,Item.Word[0]
	MOV	DX,Item.Word[2]
        LES     DI,Self
        MOV     CX,ES:[DI].TCollection.Count
        JCXZ    @@3
        LES     DI,ES:[DI].TCollection.Items
        MOV     BX,DI
        SHL     CX,1
        CLD
@@1:    REPNE   SCASW
        JCXZ    @@3
        TEST    CX,1
        JE      @@1
        XCHG    AX,DX
        SCASW
        XCHG    AX,DX
        LOOPNE  @@1
        JNE     @@3
        MOV     AX,DI
        SUB     AX,BX
        SHR     AX,1
        SHR     AX,1
        DEC     AX
        JMP     @@2
@@3:    MOV     AX,-1
@@2:
end;

procedure TCollection.Insert(Item: Pointer);
begin
  AtInsert(Count, Item);
end;

function TCollection.LastThat(Test: Pointer): Pointer; assembler;
asm
        LES     DI,Self
        MOV     CX,ES:[DI].TCollection.Count
        JCXZ    @@2
        LES     DI,ES:[DI].TCollection.Items
        MOV     AX,CX
        SHL     AX,1
        SHL     AX,1
        ADD     DI,AX
@@1:    SUB     DI,4
        PUSH    ES
        PUSH    DI
        PUSH    CX
        PUSH    WORD PTR ES:[DI+2]
        PUSH    WORD PTR ES:[DI]
{$IFDEF Windows}
	MOV	AX,[BP]
	AND	AL,0FEH
	PUSH	AX
{$ELSE}
        PUSH    WORD PTR [BP]
{$ENDIF}
        CALL    Test
        POP     CX
        POP     DI
        POP     ES
        OR      AL,AL
        JNE     @@3
        LOOP    @@1
@@2:    XOR     AX,AX
        MOV     DX,AX
        JMP     @@4
@@3:	MOV	AX,ES:[DI]
	MOV	DX,ES:[DI+2]
@@4:
end;

procedure TCollection.Pack; assembler;
asm
        LES     DI,Self
        MOV     CX,ES:[DI].TCollection.Count
        JCXZ    @@3
        LES     DI,ES:[DI].TCollection.Items
        MOV     SI,DI
        PUSH    DS
        PUSH    ES
        POP     DS
        CLD
@@1:    LODSW
        XCHG    AX,DX
        LODSW
        MOV     BX,AX
        OR      BX,DX
        JE      @@2
        XCHG    AX,DX
        STOSW
        XCHG    AX,DX
        STOSW
@@2:    LOOP    @@1
        POP     DS
        LES     BX,Self
        SUB     DI,WORD PTR ES:[BX].TCollection.Items
        SHR     DI,1
        SHR     DI,1
        MOV     ES:[BX].TCollection.Count,DI
@@3:
end;

procedure TCollection.PutItem(var S: TOldStream; Item: Pointer);
begin
  S.Put(Item);
end;

procedure TCollection.SetLimit(ALimit: Integer);
var
  AItems: PItemList;
begin
  if ALimit < Count then ALimit := Count;
  if ALimit > MaxCollectionSize then ALimit := MaxCollectionSize;
  if ALimit <> Limit then
  begin
    if ALimit = 0 then AItems := nil else
    begin
      GetMem(AItems, ALimit * SizeOf(Pointer));
      if (Count <> 0) and (Items <> nil) then
        Move(Items^, AItems^, Count * SizeOf(Pointer));
    end;
    if Limit <> 0 then FreeMem(Items, Limit * SizeOf(Pointer));
    Items := AItems;
    Limit := ALimit;
  end;
end;

procedure TCollection.Store(var S: TOldStream);

procedure DoPutItem(P: Pointer); far;
begin
  PutItem(S, P);
end;

begin
  S.Write(Count, SizeOf(Integer) * 3);
  ForEach(@DoPutItem);
end;

{ TSortedCollection }

constructor TSortedCollection.Init(ALimit, ADelta: Integer);
begin
  TCollection.Init(ALimit, ADelta);
  Duplicates := False;
end;

constructor TSortedCollection.Load(var S: TOldStream);
begin
  TCollection.Load(S);
  S.Read(Duplicates, SizeOf(Boolean));
end;

function TSortedCollection.Compare(Key1, Key2: Pointer): Integer;
begin
  Abstract;
end;

function TSortedCollection.IndexOf(Item: Pointer): Integer;
var
  I: Integer;
begin
  IndexOf := -1;
  if Search(KeyOf(Item), I) then
  begin
    if Duplicates then
      while (I < Count) and (Item <> Items^[I]) do Inc(I);
    if I < Count then IndexOf := I;
  end;
end;

procedure TSortedCollection.Insert(Item: Pointer);
var
  I: Integer;
begin
  if not Search(KeyOf(Item), I) or Duplicates then AtInsert(I, Item);
end;

function TSortedCollection.KeyOf(Item: Pointer): Pointer;
begin
  KeyOf := Item;
end;

function TSortedCollection.Search(Key: Pointer; var Index: Integer): Boolean;
var
  L, H, I, C: Integer;
begin
  Search := False;
  L := 0;
  H := Count - 1;
  while L <= H do
  begin
    I := (L + H) shr 1;
    C := Compare(KeyOf(Items^[I]), Key);
    if C < 0 then L := I + 1 else
    begin
      H := I - 1;
      if C = 0 then
      begin
        Search := True;
        if not Duplicates then L := I;
      end;
    end;
  end;
  Index := L;
end;

procedure TSortedCollection.Store(var S: TOldStream);
begin
  TCollection.Store(S);
  S.Write(Duplicates, SizeOf(Boolean));
end;

{ TStringCollection }

function TStringCollection.Compare(Key1, Key2: Pointer): Integer; assembler;
asm
        PUSH    DS
        CLD
        LDS     SI,Key1
        LES     DI,Key2
        LODSB
        MOV     AH,ES:[DI]
        INC     DI
        MOV     CL,AL
        CMP     CL,AH
        JBE     @@1
        MOV     CL,AH
@@1:    XOR     CH,CH
        REP     CMPSB
        JE      @@2
        MOV     AL,DS:[SI-1]
        MOV     AH,ES:[DI-1]
@@2:    SUB     AL,AH
        SBB     AH,AH
        POP     DS
end;

procedure TStringCollection.FreeItem(Item: Pointer);
begin
  DisposeStr(Item);
end;

function TStringCollection.GetItem(var S: TOldStream): Pointer;
begin
  GetItem := S.ReadStr;
end;

procedure TStringCollection.PutItem(var S: TOldStream; Item: Pointer);
begin
  S.WriteStr(Item);
end;

{ TStrCollection }

function TStrCollection.Compare(Key1, Key2: Pointer): Integer;
begin
  Compare := StrComp(Key1, Key2);
end;

procedure TStrCollection.FreeItem(Item: Pointer);
begin
  StrDispose(Item);
end;

function TStrCollection.GetItem(var S: TOldStream): Pointer;
begin
  GetItem := S.StrRead;
end;

procedure TStrCollection.PutItem(var S: TOldStream; Item: Pointer);
begin
  S.StrWrite(Item);
end;

{ Objects registration procedure }

procedure RegisterObjects;
begin
  RegisterType(RCollection);
  RegisterType(RStringCollection);
  RegisterType(RStrCollection);
end;

{$ENDIF}
end.
