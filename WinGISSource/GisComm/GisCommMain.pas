Unit GisCommMain;

Interface

Uses SysUtils,Classes,WinTypes,Messages,DDEDef,DDEML,Inifiles, WinOSInfo;

Function  CreateCommunicationModule(AMainWindowHandle:HWnd;ALogging:Boolean):Boolean;
Procedure DestroyCommunicationModule;
Procedure DDEHandler_SetIniSettings(AIniSepSign:Char;AMWReady,AGReady,ASendLMSettings,AGReadyAfterView,AShowAfterDSA,
                                    AOneSelect,ADBAutoInsert,ADDEMLConnect,ADDEMLSend,ADBSelMode,ADoOneSelect,
                                    AFrameSelect:Boolean;
                                    ADBLayerInfos,ADDEMLCallBack:Word;ASendInfosToDB:LongInt);
Procedure DDEHandler_DDEInitiate(var Msg:TMessage);
Function  DDEHandler_InitiateDDE(AWindow:THandle;LoadDatabase:Boolean;InitDDEML:Boolean):Boolean;
Procedure DDEHandler_InsertDDEApp(ADDEApp:PDDEApp);
Procedure DDEHandler_InsertDDEMLServerSettings(AService,ATopic,AItem:PChar);
Procedure DDEHandler_InsertDDEMLApp(ADDEMLApp:PDDEMLApp);
Procedure DDEHandler_EnableDDEReception(AEnable:Boolean);
Procedure DDEHandler_DDETerminate(ServerHandle:THandle;MustBeSender,TermDDEML:Boolean);
Procedure DDEHandler_CombineParamsOfAllActiveConnections;
Procedure DDEHandler_SetDBSendStrings(SendMode:Word;ProjToInit:HWnd);
Function  DDEHandler_GetFSendMWR:Boolean;
Function  DDEHandler_SendString(AText:ShortString):Boolean;
Function  DDEHandler_SendPChar(AText:PChar):Boolean;
Procedure DDEHandler_DDEAcknowledge(var Msg:TMessage);
Procedure DDEHandler_SendAck(AWindow:HWnd;AParam:LongInt);
Procedure DDEHandler_SetAnswerMode(SetMode:Boolean);
Procedure DDEHandler_SetActualChild(AChild:HWnd);
Procedure DDEHandler_UnActivateConnections;
Procedure DDEHandler_GetDDECommand(var AHSZSTopic:HSZ;var AHSZClientTopic:HSZ;var AStr:PChar;var ACnv:PChar);
Function  DDEHandler_GetDBLoaded:Integer;
Function  DDEHandler_GetDDEMLLoaded:Integer;
Function  DDEHandler_GetDDEOK:Boolean;
Function  DDEHandler_GetFLayerInfo:Integer;
Procedure DDEHandler_StartList(AText:ShortString);
Function  DDEHandler_AppendList(Index:LongInt):Boolean;
Function  DDEHandler_AppendList1(Index:LongInt;NameLength:Integer):Boolean;
Function  DDEHandler_AppendString(AStr:ShortString):Boolean;
Function  DDEHandler_EndList(SendEnd:Boolean):Boolean;
Function  DDEHandler_EndList1(SendEnd:Boolean;NameLength:Integer):Boolean;
Function  DDEHandler_GetFGotLInfo:Word;
Function  DDEHandler_QuestHServer:Word;
Function  DDEHandler_GetGlobal(Size:LongInt):Boolean;
Procedure DDEHandler_FreeGlobal;
Function  DDEHandler_Write(Pos:LongInt;AStr:ShortString):Boolean;
Function  DDEHandler_Append(AStr:PChar):Boolean;
Procedure DDEHandler_Reset;
Function  DDEHandler_QuestLCnt:Integer;
Function  DDEHandler_GetDBLInfo:Word;
Function  DDEHandler_GetDBStrFormat:Word;
Procedure DDEHandler_SetDBWMProjInited(SetInited:Boolean);
Function  DDEHandler_EnableDDEMLConversation(AEnable:Boolean):Boolean;
Function  DDEHandler_Execute:Boolean;
Function  DDEHandler_GetFDDESepSign:Char;
Function  DDEHandler_GetFOutSepSign:Char;
Procedure DDEHandler_SetFGotLInfo(AFGLInfo:Word);
Function  DDEHandler_GetFSendGR:Boolean;
Function  DDEHandler_GetFSendGRAV:Boolean;
Procedure DDEHandler_SetDBSelMode(ADBSMode:Boolean);
Function  DDEHandler_GetDBSelMode:Boolean;
Function  DDEHandler_GetFOneSelect:Boolean;
Procedure DDEHandler_SetFOneSelect(AOneSelect:Boolean);
Function  DDEHandler_GetIniDoOneSelect:Boolean;
Procedure DDEHandler_SetIniDoOneSelect(ADoOneSelect:Boolean);
Function  DDEHandler_GetIniFrameSelect:Boolean;
Procedure DDEHandler_SetIniFrameSelect(AFrameSelect:Boolean);
Function  DDEHandler_GetDataMode:Boolean;
Procedure DDEHandler_SetDataMode(ADataMode:Boolean);
Function  DDEHandler_GetDoAutoIns:Boolean;
Procedure DDEHandler_SetDoAutoIns(AAutoIns:Boolean);
Function  DDEHandler_GetFChange:Word;
Function  DDEHandler_GetFClose:Word;
Procedure DDEHandler_SetFLayerInfo(AFLInfo:Word);
Function  DDEHandler_GetFMoreSelect:Boolean;
Function  DDEHandler_GetFNew:Word;
Function  DDEHandler_GetFNoDBAutoIn:Boolean;
Function  DDEHandler_GetFOpen:Word;
Function  DDEHandler_GetFSave:Word;
Function  DDEHandler_GetFSaveAs:Word;
Function  DDEHandler_GetFSending:Boolean;
Procedure DDEHandler_SetFSending(AFSending:Boolean);
Function  DDEHandler_GetFLMSettings:Boolean;
Function  DDEHandler_GetFShowADSA:Boolean;
Function  DDEHandler_GetFSuspendDDE:Boolean;
Procedure DDEHandler_SetFSuspendDDE(AFSuspendDDE:Boolean);
Function  DDEHandler_GetInAction:Boolean;
Function  DDEHandler_GetMacroCnv:HConv;
Procedure DDEHandler_SetMacroCnv(ACnv:HConv);
Function  DDEHandler_GetMacroHSZCI:HSZ;
Procedure DDEHandler_SetMacroHSZCI(AHSZ:HSZ);
Function  DDEHandler_GetMacroHSZCT:HSZ;
Procedure DDEHandler_SetMacroHSZCT(AHSZ:HSZ);
Function  DDEHandler_GetMacroMode:Boolean;
Procedure DDEHandler_SetMacroMode(AMacroMode:Boolean);
Function  DDEHandler_GetNWUMode:Boolean;
Procedure DDEHandler_SetNWUMode(ANWUMode:Boolean);
Function  DDEHandler_GetRequestFrom:Word;
Procedure DDEHandler_SetRequestFrom(ARequestFrom:Word);
Procedure DDEHandler_SetDDECommMode(AConversation:Word;AConvName:PChar);
Procedure DDEHandler_GetNewDBEntry(ACommName:PChar;var ACon:Boolean;var AConProj:Boolean;
                                   var ASendStr:Boolean;var ASendProj:Boolean);
Function  DDEHandler_GetDDEApp(AIndex:Integer):PDDEApp;
Function  DDEHandler_GetDDEMLApp(AIndex:Integer):PDDEMLApp;
Procedure DDEHandler_SendRefPoints(GeoReference:PLPointArray);
Procedure DDEHandler_SetLayerName(ALName:PChar);
Function  DDEHandler_TranslateInStringDesma(ADDEString:PChar;var EndMode:Boolean):PChar;
Procedure DDEHandler_DoExecute(AMacroHSZCT,AMacroHSZCI:HSZ;AItem:PChar;AMacroCnv:HConv);
Procedure DDEHandler_GetMsgBoxParams(AText:PChar;var AMessage:LongInt;var ATextInt:LongInt);
Procedure DDEHandler_UpdateDDEApp(ADDEApp:PDDEApp);
Procedure DDEHandler_UpdateDDEMLApp(ADDEMLApp:PDDEMLApp);
Procedure DDEHandler_GetLogData(ALData:PChar;var ALInt0:LongInt;var ALInt1:LongInt);
Procedure DDEHandler_SetTRData(pSerial,pModCod,pLizence,p_im_TR:PChar);
Procedure DDEHandler_InsertDDEAppWithParams(AName,AExeFile,AService,ATopic:PChar;AMenu:Integer;
                                            AConnect,ASendStrings:Boolean;AStrFormat,ADBCLayerInf:Word;
                                            AConnected,AActive:Boolean);
Function  DDEHandler_GetDDEAppWithParams(AIndex:Integer;AName,AExeFile,AService,ATopic:PChar;var AMenu:Integer;
                                         var AConnect:Boolean;var ASendStrings:Boolean;var AStrFormat:Word;
                                         var ADBCLayerInf:Word;var AConnected:Boolean;var AActive:Boolean):Boolean;
Procedure DDEHandler_UpdateDDEAppWithParams(AName,AExeFile,AService,ATopic:PChar;AConnect,ASendStrings:Boolean);
Procedure DDEHandler_InsertDDEMLAppWithParams(ACObjAddress:Pointer;AName,AExeFile,AStartParams,AService,ATopic,
                                              AItem:PChar;AConnect,AConnectProj,ASendStrings,ASendStrProj:Boolean;
                                              AStrFormat,ADBCLayerInf:Word;ACDDESepSign:Char;ACCallBack:Word;
                                              ACMWReady,ACGReady,ACGReadyAV,ACDBSelMode,ACSendLMInfo,ACShowADSA,
                                              ACOneSelect,ACDBAutoIns:Boolean;ACOpenInfo,ACSaveInfo,ACSaveAsInfo,
                                              ACNewInfo,ACChangeInfo,ACCloseInfo:Word;AHSCnv:HConv;AConnected,
                                              AActive,AActChanged,ARealActive:Boolean;AInitedProj:HWnd;
                                              ASendGReady:Boolean);
Function  DDEHandler_GetDDEMLAppWithParams(AIndex:Integer;var ACObjAddress:Pointer;AName:PChar;AExeFile:PChar;
                                           AStartParams:PChar;AService:PChar;ATopic:PChar;AItem:PChar;
                                           var AConnect:Boolean;var AConnectProj:Boolean;var ASendStrings:Boolean;
                                           var ASendStrProj:Boolean;var AStrFormat:Word;var ADBCLayerInf:Word;
                                           var ACDDESepSign:Char;var ACCallBack:Word;var ACMWReady:Boolean;
                                           var ACGReady:Boolean;var ACGReadyAV:Boolean;var ACDBSelMode:Boolean;
                                           var ACSendLMInfo:Boolean;var ACShowADSA:Boolean;var ACOneSelect:Boolean;
                                           var ACDBAutoIns:Boolean;var ACOpenInfo:Word;var ACSaveInfo:Word;
                                           var ACSaveAsInfo:Word;var ACNewInfo:Word;var ACChangeInfo:Word;
                                           var ACCloseInfo:Word;var AHSCnv:HConv;var AConnected:Boolean;
                                           var AActive:Boolean;var AActChanged:Boolean;var ARealActive:Boolean;
                                           var AInitedProj:HWnd;var ASendGReady:Boolean):Boolean;
Procedure DDEHandler_UpdateDDEMLAppWithParams(AName,AExeFile,AService,ATopic:PChar;AConnect,ASendStrings:Boolean);
Procedure DDEHandler_DoAllDDECommands(AChild:Pointer);
Procedure DDEHandler_DoAllDDEMLCommands;
Procedure DDEHandler_DeleteAllDDEData;
Procedure WriteLog(LogText: String);

Implementation

Uses DDEMain;

var
IsLogging    : Boolean;
LogFile      : TextFile;

Procedure WriteLog(LogText: String);
var
      ActualTime   : TDateTime;
      AYear        : Word;
      AMonth       : Word;
      ADay         : Word;
      AHour        : Word;
      AMin         : Word;
      ASec         : Word;
      AMSec        : Word;
      AOutString   : String;
      TempStr      : String;
begin
     //IsLogging := TRUE;

     if IsLogging then begin
        ActualTime:=Now;
        DecodeDate(ActualTime,AYear,AMonth,ADay);
        DecodeTime(Now,AHour,AMin,ASec,AMSec);
{        Str(ADay:0,TempStr);
        if Length(TempStr) = 1 then AOutString:='0'+TempStr+'.'
        else AOutString:=TempStr+'.';
        Str(AMonth:0,TempStr);
        if Length(TempStr) = 1 then AOutString:=AOutString+'0'+TempStr+'.'
        else AOutString:=AOutString+TempStr+'.';
        Str(AYear:4,TempStr);
        AOutString:=AOutString+TempStr+'-';
}        Str(AHour:0,TempStr);
        if Length(TempStr) = 1 then AOutString:=AOutString+'0'+TempStr+':'
        else AOutString:=AOutString+TempStr+':';
        Str(AMin:0,TempStr);
        if Length(TempStr) = 1 then AOutString:=AOutString+'0'+TempStr+':'
        else AOutString:=AOutString+TempStr+':';
        Str(ASec:0,TempStr);
        if Length(TempStr) = 1 then AOutString:=AOutString+'0'+TempStr+'.'
        else AOutString:=AOutString+TempStr+'.';
        Str(AMSec:0,TempStr);
        if Length(TempStr) = 1 then AOutString:=AOutString+'00'+TempStr+': '+LogText
        else if Length(TempStr) = 2 then AOutString:=AOutString+'0'+TempStr+': '+LogText
        else AOutString:=AOutString+TempStr+': '+LogText;
        LogText:=AOutString;

          if FileExists(OSInfo.TempDataDir + 'GisComm.log') = False then
          begin
               try
                  AssignFile(LogFile,OSInfo.TempDataDir + 'GisComm.log');
                  Rewrite(LogFile);
               finally
                  CloseFile(LogFile);
               end;
          end;
          try
             AssignFile(LogFile,OSInfo.TempDataDir + 'GisComm.log');
             Reset(LogFile);
             Append(LogFile);
             Writeln(LogFile,LogText);
          finally
             CloseFile(LogFile);
          end;
     end;
end;

Function CreateCommunicationModule
   (

   AMainWindowHandle    : HWnd;
   ALogging             : Boolean
   )
   : Boolean;
  var
     Ini	: TInifile;
     s          : String;
  begin
    Ini:=TIniFile.Create(OSInfo.WingisIniFileName);
    s:=Ini.ReadString('Settings','LogFile','FALSE');
    if (Uppercase(s) = 'TRUE') or (s = '1') then IsLogging:=True else IsLogging:=False; 
    Ini.Free;
    if IsLogging then SysUtils.DeleteFile(OSInfo.TempDataDir + 'GisComm.log');
    Result:=FALSE;
    DDEHandler:=New(PDDEHandler,Init(AMainWindowHandle,ALogging));
    if DDEHandler <> NIL then Result:=TRUE;
  end;

Procedure DestroyCommunicationModule;
  begin
    Dispose(DDEHandler,Done);
  end;

Procedure DDEHandler_SetIniSettings
   (
   AIniSepSign          : Char;
   AMWReady             : Boolean;
   AGReady              : Boolean;
   ASendLMSettings      : Boolean;
   AGReadyAfterView     : Boolean;
   AShowAfterDSA        : Boolean;
   AOneSelect           : Boolean;
   ADBAutoInsert        : Boolean;
   ADDEMLConnect        : Boolean;
   ADDEMLSend           : Boolean;
   ADBSelMode           : Boolean;
   ADoOneSelect         : Boolean;
   AFrameSelect         : Boolean;
   ADBLayerInfos        : Word;
   ADDEMLCallBack       : Word;
   ASendInfosToDB       : LongInt
   );
  begin
    DDEHandler^.SetIniSettings(AIniSepSign,AMWReady,AGReady,ASendLMSettings,AGReadyAfterView,AShowAfterDSA,
                                    AOneSelect,ADBAutoInsert,ADDEMLConnect,ADDEMLSend,ADBSelMode,ADoOneSelect,
                                    AFrameSelect,ADBLayerInfos,ADDEMLCallBack,ASendInfosToDB);
  end;

Procedure DDEHandler_DDEInitiate
   (
   var Msg              : TMessage
   );
  begin
    DDEHandler^.DDEInitiate(Msg);
  end;


Function DDEHandler_InitiateDDE
   (
   AWindow              : THandle;
   LoadDatabase         : Boolean;
   InitDDEML            : Boolean
   )
   : Boolean;
  begin
    Result:=DDEHandler^.InitiateDDE(AWindow,LoadDatabase,InitDDEML);
  end;

Procedure DDEHandler_InsertDDEApp
   (
   ADDEApp              : PDDEApp
   );
  begin
    DDEHandler^.InsertDDEApp(ADDEApp);
  end;

Procedure DDEHandler_InsertDDEMLServerSettings
   (
   AService             : PChar;
   ATopic               : PChar;
   AItem                : PChar
   );
  begin
    DDEHandler^.InsertDDEMLServerSettings(AService,ATopic,AItem);
  end;

Procedure DDEHandler_InsertDDEMLApp
   (
   ADDEMLApp            : PDDEMLApp
   );
  begin
    DDEHandler^.InsertDDEMLApp(ADDEMLApp);
  end;

Procedure DDEHandler_EnableDDEReception
   (
   AEnable              : Boolean
   );
  begin
    DDEHandler^.EnableDDEReception(AEnable);
  end;

Procedure DDEHandler_DDETerminate
   (
   ServerHandle         : THandle;
   MustBeSender         : Boolean;
   TermDDEML            : Boolean
   );
  begin
    DDEHandler^.DDETerminate(ServerHandle,MustBeSender,TermDDEML);
  end;

Procedure DDEHandler_CombineParamsOfAllActiveConnections;
  begin
    DDEHandler^.CombineParamsOfAllActiveConnections;
  end;

Procedure DDEHandler_SetDBSendStrings
   (
   SendMode        : Word;
   ProjToInit      : HWnd
   );
  begin
    DDEHandler^.SetDBSendStrings(SendMode,ProjToInit);
  end;

Function DDEHandler_GetFSendMWR
   : Boolean;
  begin
    Result:=DDEHandler^.GetFSendMWR;
  end;

Function DDEHandler_SendString
   (
   AText           : ShortString
   )
   : Boolean;
  begin
    Result:=DDEHandler^.SendString(AText);
  end;

Function DDEHandler_SendPChar
   (
   AText                : PChar
   )
   : Boolean;
  begin
    Result:=DDEHandler^.SendPChar(AText);
  end;

Procedure DDEHandler_DDEAcknowledge
   (
   var Msg              : TMessage
   );
  begin
    DDEHandler^.DDEAcknowledge(Msg);
  end;

Procedure DDEHandler_SendAck
   (
   AWindow              : HWnd;
   AParam               : LongInt
   );
  begin
    DDEHandler^.SendAck(AWindow,AParam);
  end;

Procedure DDEHandler_SetAnswerMode
   (
   SetMode              : Boolean
   );
  begin
    DDEHandler^.SetAnswerMode(SetMode);
  end;

Procedure DDEHandler_SetActualChild
   (
   AChild               : HWnd
   );
  begin
    DDEHandler^.SetActualChild(AChild);
  end;

Procedure DDEHandler_UnActivateConnections;
  begin
    DDEHandler^.UnActivateConnections;
  end;

Procedure DDEHandler_GetDDECommand
   (
   var AHSZSTopic       : HSZ;
   var AHSZClientTopic  : HSZ;
   var AStr             : PChar;
   var ACnv             : PChar
   );
  begin
    DDEHandler^.GetDDECommand(AHSZSTopic,AHSZClientTopic,AStr,ACnv);
  end;

Function DDEHandler_GetDBLoaded
   : Integer;
  begin
    Result:=DDEHandler^.GetDBLoaded;
  end;

Function DDEHandler_GetDDEMLLoaded
   : Integer;
  begin
    Result:=DDEHandler^.GetDDEMLLoaded;
  end;

Function DDEHandler_GetDDEOK
   : Boolean;
  begin
    Result:=DDEHandler^.DDEOK;
  end;

Function DDEHandler_GetFLayerInfo
   : Integer;
  begin
    Result:=DDEHandler^.GetFLayerInfo;
  end;

Procedure DDEHandler_StartList
   (
   AText                : ShortString
   );
  begin
    DDEHandler^.StartList(AText);
  end;

Function DDEHandler_AppendList
   (
   Index                : LongInt
   )
   : Boolean;
  begin
    Result:=DDEHandler^.AppendList(Index);
  end;

Function DDEHandler_AppendList1
   (
   Index                : LongInt;
   NameLength           : Integer
   )
   : Boolean;
  begin
    Result:=DDEHandler^.AppendList1(Index,NameLength);
  end;

Function DDEHandler_AppendString
   (
   AStr                 : ShortString
   )
   : Boolean;
  begin
    Result:=DDEHandler^.AppendString(AStr);
  end;

Function DDEHandler_EndList
   (
   SendEnd              : Boolean
   )
   : Boolean;
  begin
    Result:=DDEHandler.EndList(SendEnd);
  end;

Function DDEHandler_EndList1
   (
   SendEnd              : Boolean;
   NameLength           : Integer
   )
   : Boolean;
  begin
    Result:=DDEHandler.EndList1(SendEnd,NameLength);
  end;

Function DDEHandler_GetFGotLInfo
   : Word;
  begin
    Result:=DDEHandler^.GetFGotLInfo;
  end;

Function DDEHandler_QuestHServer
   : Word;
  begin
    Result:=DDEHandler^.QuestHServer;
  end;

Function DDEHandler_GetGlobal(Size:LongInt)
   : Boolean;
  begin
    Result:=DDEHandler^.GetGlobal(Size);
  end;

Procedure DDEHandler_FreeGlobal;
  begin
    DDEHandler^.FreeGlobal;
  end;

Function DDEHandler_Write
   (
   Pos                  : LongInt;
   AStr                 : ShortString
   )
   : Boolean;
  begin
    Result:=DDEHandler^.Write(Pos,AStr);
  end;

Function DDEHandler_Append
   (
   AStr                 : PChar
   )
   : Boolean;
  begin
    Result:=DDEHandler^.Append(StrPas(AStr));
  end;

Procedure DDEHandler_Reset;
  begin
    DDEHandler^.Reset;
  end;

Function DDEHandler_QuestLCnt:Integer;
  begin
    Result:=DDEHandler^.QuestLCnt;
  end;

Function DDEHandler_GetDBLInfo
   : Word;
  begin
    Result:=DDEHandler^.GetDBLInfo;
  end;

Function DDEHandler_GetDBStrFormat
   : Word;
  begin
    Result:=DDEHandler^.GetDBStrFormat;
  end;

Procedure DDEHandler_SetDBWMProjInited
   (
   SetInited            : Boolean
   );
  begin
    DDEHandler^.SetDBWMProjInited(SetInited);
  end;

Function DDEHandler_EnableDDEMLConversation
   (
   AEnable              : Boolean
   )
   : Boolean;
  begin
    Result:=DDEHandler^.EnableDDEMLConversation(AEnable);
  end;

Function DDEHandler_Execute
   :
   Boolean;
  begin
    Result:=DDEHandler^.Execute;
  end;

Function DDEHandler_GetFDDESepSign
   : Char;
  begin
    Result:=DDEHandler^.GetFDDESepSign;
  end;

Function DDEHandler_GetFOutSepSign
   : Char;
  begin
    Result:=DDEHandler^.GetFOutSepSign;
  end;

Procedure DDEHandler_SetFGotLInfo
   (
   AFGLInfo             : Word
   );
  begin
    DDEHandler^.SetFGotLInfo(AFGLInfo);
  end;

Function DDEHandler_GetFSendGR
   : Boolean;
  begin
    Result:=DDEHandler^.GetFSendGR;
  end;

Function DDEHandler_GetFSendGRAV
   : Boolean;
  begin
    Result:=DDEHandler^.GetFSendGRAV;
  end;

Procedure DDEHandler_SetDBSelMode
   (
   ADBSMode             : Boolean
   );
  begin
    DDEHandler^.SetDBSelMode(ADBSMode);
  end;

Function DDEHandler_GetDBSelMode
   : Boolean;
  begin
    Result:=DDEHandler^.GetDBSelMode;
  end;

Function DDEHandler_GetFOneSelect
   : Boolean;
  begin
    Result:=DDEHandler^.GetFOneSelect;
  end;

Procedure DDEHandler_SetFOneSelect
   (
   AOneSelect           : Boolean
   );
  begin
    DDEHandler^.SetFOneSelect(AOneSelect);
  end;

Function DDEHandler_GetIniDoOneSelect
   : Boolean;
  begin
    Result:=DDEHandler^.GetIniDoOneSelect;
  end;

Procedure DDEHandler_SetIniDoOneSelect
   (
   ADoOneSelect         : Boolean
   );
  begin
    DDEHandler^.SetIniDoOneSelect(ADoOneSelect);
  end;

Function  DDEHandler_GetIniFrameSelect
   : Boolean;
  begin
    Result:=DDEHandler^.GetIniFrameSelect;
  end;

Procedure DDEHandler_SetIniFrameSelect
   (
   AFrameSelect         : Boolean
   );
  begin
    DDEHandler^.SetIniFrameSelect(AFrameSelect);
  end;

Function DDEHandler_GetDataMode
   : Boolean;
  begin
    Result:=DDEHandler^.GetDataMode;
  end;

Procedure DDEHandler_SetDataMode
   (
   ADataMode            : Boolean
   );
  begin
    DDEHandler^.SetDataMode(ADataMode);
  end;

Function DDEHandler_GetDoAutoIns
   : Boolean;
  begin
    Result:=DDEHandler^.GetDoAutoIns;
  end;

Procedure DDEHandler_SetDoAutoIns
   (
   AAutoIns             : Boolean
   );
  begin
    DDEHandler^.SetDoAutoIns(AAutoIns);
  end;

Function DDEHandler_GetFChange
   : Word;
  begin
    Result:=DDEHandler^.GetFChange;
  end;

Function DDEHandler_GetFClose
   : Word;
  begin
    Result:=DDEHandler^.GetFClose;
  end;

Procedure DDEHandler_SetFLayerInfo
   (
   AFLInfo              : Word
   );
  begin
    DDEHandler^.SetFLayerInfo(AFLInfo);
  end;

Function DDEHandler_GetFMoreSelect
   : Boolean;
  begin
    Result:=DDEHandler^.GetFMoreSelect;
  end;

Function DDEHandler_GetFNew
   : Word;
  begin
    Result:=DDEHandler^.GetFNew;
  end;

Function DDEHandler_GetFNoDBAutoIn
   : Boolean;
  begin
    Result:=DDEHandler^.GetFNoDBAutoIn;
  end;

Function DDEHandler_GetFOpen
   : Word;
  begin
    Result:=DDEHandler^.GetFOpen;
  end;

Function DDEHandler_GetFSave
   : Word;
  begin
    Result:=DDEHandler^.GetFSave;
  end;

Function DDEHandler_GetFSaveAs
   : Word;
  begin
    Result:=DDEHandler^.GetFSaveAs;
  end;

Function DDEHandler_GetFSending
   : Boolean;
  begin
    Result:=DDEHandler^.GetFSending;
  end;

Procedure DDEHandler_SetFSending
   (
   AFSending            : Boolean
   );
  begin
    DDEHandler^.SetFSending(AFSending);
  end;

Function DDEHandler_GetFLMSettings
   : Boolean;
  begin
    Result:=DDEHandler^.GetFLMSettings;
  end;

Function DDEHandler_GetFShowADSA
   : Boolean;
  begin
    Result:=DDEHandler^.GetFShowADSA;
  end;

Function DDEHandler_GetFSuspendDDE
   : Boolean;
  begin
    Result:=DDEHandler^.GetFSuspendDDE;
  end;

Procedure DDEHandler_SetFSuspendDDE
   (
   AFSuspendDDE         : Boolean
   );
  begin
    DDEHandler^.SetFSuspendDDE(AFSuspendDDE);
  end;

Function DDEHandler_GetInAction
   : Boolean;
  begin
    Result:=DDEHandler^.GetInAction;
  end;

Function DDEHandler_GetMacroCnv
   : HConv;
  begin
    Result:=DDEHandler^.GetMacroCnv;
  end;

Procedure DDEHandler_SetMacroCnv
   (
   ACnv                 : HConv
   );
  begin
    DDEHandler^.SetMacroCnv(ACnv);
  end;

Function DDEHandler_GetMacroHSZCI
   : HSZ;
  begin
    Result:=DDEHandler^.GetMacroHSZCI;
  end;

Procedure DDEHandler_SetMacroHSZCI
   (
   AHSZ                 : HSZ
   );
  begin
    DDEHandler^.SetMacroHSZCI(AHSZ);
  end;

Function DDEHandler_GetMacroHSZCT
   : HSZ;
  begin
    Result:=DDEHandler^.GetMacroHSZCT;
  end;

Procedure DDEHandler_SetMacroHSZCT
   (
   AHSZ                 : HSZ
   );
  begin
    DDEHandler^.SetMacroHSZCT(AHSZ);
  end;

Function DDEHandler_GetMacroMode
   : Boolean;
  begin
    Result:=DDEHandler^.GetMacroMode;
  end;

Procedure DDEHandler_SetMacroMode
   (
   AMacroMode           : Boolean
   );
  begin
    DDEHandler^.SetMacroMode(AMacroMode);
  end;

Function DDEHandler_GetNWUMode
   : Boolean;
  begin
    Result:=DDEHandler^.GetNWUMode;
  end;

Procedure DDEHandler_SetNWUMode
   (
   ANWUMode             : Boolean
   );
  begin
    DDEHandler^.SetNWUMode(ANWUMode);
  end;

Function DDEHandler_GetRequestFrom
   : Word;
  begin
    Result:=DDEHandler^.GetRequestFrom;
  end;

Procedure DDEHandler_SetRequestFrom
   (
   ARequestFrom         : Word
   );
  begin
    DDEHandler^.SetRequestFrom(ARequestFrom);
  end;

Procedure DDEHandler_SetDDECommMode
   (
   AConversation        : Word;
   AConvName            : PChar
   );
  begin
    DDEHandler^.SetDDECommMode(AConversation,AConvName);
  end;

Procedure DDEHandler_GetNewDBEntry
   (
   ACommName            : PChar;
   var ACon             : Boolean;
   var AConProj         : Boolean;
   var ASendStr         : Boolean;
   var ASendProj        : Boolean
   );
  begin
    DDEHandler^.GetNewDBEntry(ACommName,ACon,AConProj,ASendStr,ASendProj);
  end;

Function DDEHandler_GetDDEApp
   (
   AIndex               : Integer
   )
   : PDDEApp;
  begin
    Result:=DDEHandler^.GetDDEApp(AIndex);
  end;

Function DDEHandler_GetDDEMLApp
   (
   AIndex               : Integer
   )
   : PDDEMLApp;
  begin
    Result:=DDEHandler^.GetDDEMLApp(AIndex);
  end;

Procedure DDEHandler_SendRefPoints
   (
   GeoReference    : PLPointArray
   );
  begin
    DDEHandler^.SendRefPoints(GeoReference);
  end;

Procedure DDEHandler_SetLayerName
   (
   ALName               : PChar
   );
  begin
    DDEHandler^.SetLayerName(ALName);
  end;

Function DDEHandler_TranslateInStringDesma
   (
   ADDEString           : PChar;
   var EndMode          : Boolean
   )
   : PChar;
  begin
    Result:=DDEHandler^.TranslateInStringDesma(ADDEString,EndMode);
  end;

Procedure DDEHandler_DoExecute
   (
   AMacroHSZCT          : HSZ;
   AMacroHSZCI          : HSZ;
   AItem                : PChar;
   AMacroCnv            : HConv
   );
  begin
    DDEHandler^.DoExecute(AMacroHSZCT,AMacroHSZCI,AItem,AMacroCnv);
  end;

Procedure DDEHandler_GetMsgBoxParams
   (
   AText                : PChar;
   var AMessage         : LongInt;
   var ATextInt         : LongInt
   );
  begin
    DDEHandler^.GetMsgBoxParams(AText,AMessage,ATextInt);
  end;

Procedure DDEHandler_UpdateDDEApp
   (
   ADDEApp              : PDDEApp
   );
  begin
    DDEHandler^.UpdateDDEApp(ADDEApp);
  end;

Procedure DDEHandler_UpdateDDEMLApp
   (
   ADDEMLApp            : PDDEMLApp
   );
  begin
    DDEHandler^.UpdateDDEMLApp(ADDEMLApp);
  end;

Procedure DDEHandler_GetLogData
   (
   ALData               : PChar;
   var ALInt0           : LongInt;
   var ALInt1           : LongInt
   );
  begin
    DDEHandler^.GetLogData(ALData,ALInt0,ALInt1);
  end;

Procedure DDEHandler_SetTRData
   (
   pSerial              : PChar;
   pModCod              : PChar;
   pLizence             : PChar;
   p_im_TR              : PChar
   );
  begin
    DDEHandler^.SetTRData(pSerial,pModCod,pLizence,p_im_TR);
  end;

Procedure DDEHandler_InsertDDEAppWithParams
   (
   AName                : PChar;
   AExeFile             : PChar;
   AService             : PChar;
   ATopic               : PChar;
   AMenu                : Integer;
   AConnect             : Boolean;
   ASendStrings         : Boolean;
   AStrFormat           : Word;
   ADBCLayerInf         : Word;
   AConnected           : Boolean;
   AActive              : Boolean
   );
  begin
    DDEHandler^.InsertDDEAppWithParams(AName,AExeFile,AService,ATopic,AMenu,AConnect,ASendStrings,
                                       AStrFormat,ADBCLayerInf,AConnected,AActive);
  end;

Function DDEHandler_GetDDEAppWithParams
   (
   AIndex               : Integer;
   AName                : PChar;
   AExeFile             : PChar;
   AService             : PChar;
   ATopic               : PChar;
   var AMenu            : Integer;
   var AConnect         : Boolean;
   var ASendStrings     : Boolean;
   var AStrFormat       : Word;
   var ADBCLayerInf     : Word;
   var AConnected       : Boolean;
   var AActive          : Boolean
   )
   : Boolean;
  begin
    Result:=DDEHandler^.GetDDEAppWithParams(AIndex,AName,AExeFile,AService,ATopic,AMenu,AConnect,
                                            ASendStrings,AStrFormat,ADBCLayerInf,AConnected,AActive);
  end;

Procedure DDEHandler_UpdateDDEAppWithParams
   (
   AName                : PChar;
   AExeFile             : PChar;
   AService             : PChar;
   ATopic               : PChar;
   AConnect             : Boolean;
   ASendStrings         : Boolean
   );
  begin
    DDEHandler^.UpdateDDEAppWithParams(AName,AExeFile,AService,ATopic,AConnect,ASendStrings);
  end;

Procedure DDEHandler_InsertDDEMLAppWithParams
   (
   ACObjAddress         : Pointer;
   AName                : PChar;
   AExeFile             : PChar;
   AStartParams         : PChar;
   AService             : PChar;
   ATopic               : PChar;
   AItem                : PChar;
   AConnect             : Boolean;
   AConnectProj         : Boolean;
   ASendStrings         : Boolean;
   ASendStrProj         : Boolean;
   AStrFormat           : Word;
   ADBCLayerInf         : Word;
   ACDDESepSign         : Char;
   ACCallBack           : Word;
   ACMWReady            : Boolean;
   ACGReady             : Boolean;
   ACGReadyAV           : Boolean;
   ACDBSelMode          : Boolean;
   ACSendLMInfo         : Boolean;
   ACShowADSA           : Boolean;
   ACOneSelect          : Boolean;
   ACDBAutoIns          : Boolean;
   ACOpenInfo           : Word;
   ACSaveInfo           : Word;
   ACSaveAsInfo         : Word;
   ACNewInfo            : Word;
   ACChangeInfo         : Word;
   ACCloseInfo          : Word;
   AHSCnv               : HConv;
   AConnected           : Boolean;
   AActive              : Boolean;
   AActChanged          : Boolean;
   ARealActive          : Boolean;
   AInitedProj          : HWnd;
   ASendGReady          : Boolean
   );
  begin
    DDEHandler^.InsertDDEMLAppWithParams(ACObjAddress,AName,AExeFile,AStartParams,AService,ATopic,AItem,
                                         AConnect,AConnectProj,ASendStrings,ASendStrProj,AStrFormat,
                                         ADBCLayerInf,ACDDESepSign,ACCallBack,ACMWReady,ACGReady,ACGReadyAV,
                                         ACDBSelMode,ACSendLMInfo,ACShowADSA,ACOneSelect,ACDBAutoIns,ACOpenInfo,
                                         ACSaveInfo,ACSaveAsInfo,ACNewInfo,ACChangeInfo,ACCloseInfo,AHSCnv,
                                         AConnected,AActive,AActChanged,ARealActive,AInitedProj,ASendGReady);
  end;

Function DDEHandler_GetDDEMLAppWithParams
   (
   AIndex               : Integer;
   var ACObjAddress     : Pointer;
   AName                : PChar;
   AExeFile             : PChar;
   AStartParams         : PChar;
   AService             : PChar;
   ATopic               : PChar;
   AItem                : PChar;
   var AConnect         : Boolean;
   var AConnectProj     : Boolean;
   var ASendStrings     : Boolean;
   var ASendStrProj     : Boolean;
   var AStrFormat       : Word;
   var ADBCLayerInf     : Word;
   var ACDDESepSign     : Char;
   var ACCallBack       : Word;
   var ACMWReady        : Boolean;
   var ACGReady         : Boolean;
   var ACGReadyAV       : Boolean;
   var ACDBSelMode      : Boolean;
   var ACSendLMInfo     : Boolean;
   var ACShowADSA       : Boolean;
   var ACOneSelect      : Boolean;
   var ACDBAutoIns      : Boolean;
   var ACOpenInfo       : Word;
   var ACSaveInfo       : Word;
   var ACSaveAsInfo     : Word;
   var ACNewInfo        : Word;
   var ACChangeInfo     : Word;
   var ACCloseInfo      : Word;
   var AHSCnv           : HConv;
   var AConnected       : Boolean;
   var AActive          : Boolean;
   var AActChanged      : Boolean;
   var ARealActive      : Boolean;
   var AInitedProj      : HWnd;
   var ASendGReady      : Boolean
   )
   : Boolean;
  begin
    Result:=DDEHandler^.GetDDEMLAppWithParams(AIndex,ACObjAddress,AName,AExeFile,AStartParams,AService,ATopic,AItem,
                                              AConnect,AConnectProj,ASendStrings,ASendStrProj,AStrFormat,ADBCLayerInf,
                                              ACDDESepSign,ACCallBack,ACMWReady,ACGReady,ACGReadyAV,ACDBSelMode,
                                              ACSendLMInfo,ACShowADSA,ACOneSelect,ACDBAutoIns,ACOpenInfo,ACSaveInfo,
                                              ACSaveAsInfo,ACNewInfo,ACChangeInfo,ACCloseInfo,AHSCnv,AConnected,
                                              AActive,AActChanged,ARealActive,AInitedProj,ASendGReady);
  end;

Procedure DDEHandler_UpdateDDEMLAppWithParams
   (
   AName                : PChar;
   AExeFile             : PChar;
   AService             : PChar;
   ATopic               : PChar;
   AConnect             : Boolean;
   ASendStrings         : Boolean
   );
  begin
    DDEHandler^.UpdateDDEMLAppWithParams(AName,AExeFile,AService,ATopic,AConnect,ASendStrings);
  end;

Procedure DDEHandler_DoAllDDECommands
   (
   AChild               : Pointer
   );
  begin
    DDEHandler^.DoAllDDECommands(AChild);
  end;

Procedure DDEHandler_DoAllDDEMLCommands;
  begin
    DDEHandler^.DoAllDDEMLCommands;
  end;

Procedure DDEHandler_DeleteAllDDEData;
  begin
    DDEHandler^.DeleteAllDDEData;
  end;

end.

