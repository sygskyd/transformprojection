{****************************************************************************}
{ Unit DDEMain                                                               }
{----------------------------------------------------------------------------}
{ -                                                                          }
{----------------------------------------------------------------------------}
{ Modifikationen                                                             }
{****************************************************************************}
Unit DDEMainOld;

Interface 

Uses Messages,Objects,WinTypes,WinProcs,SysUtils,DDEML,Controls,SyncObjs,DDEDef,DDEColl;

Type PDDEHandler        = ^TDDEHandler;
     TDDEHandler        = Object(TOldObject)
       DDEData          : PBigUStrColl;          {Liste f�r DDE-Strings, die mit [END][] abgeschlossen werden}
       AllDDEData       : PBigUStrColl;          {Liste f�r DDE-Strings, die innerhalb eines Makro-Blocks gesendet werden}
       UnSendComms      : PBigUStrColl;          {Liste f�r DDE-Strings, die w�hrend laufender DDE-�bermittlung gesendet werden}
       HGlobal          : THandle;               {Handle auf den globalen Speicherbereich eines zu sendenden DDE-Strings}
       GlobalPtr        : PGArray;               {Zeiger auf den globalen Speicherbereich eines zu sendenden DDE-Strings}
       GSize            : LongInt;               {Gr��e des f�r einen zu sendenen DDE-Strings reservierten Speicherbereiches}
       Position         : LongInt;               {Schreibposition im zu sendenden DDE-String}
       LString          : String;                {Hilfsstring zum DDE-String-Aufbau}
       LCnt             : Integer;               {Anzahl der IDs im DDE-String}
       LCntPos          : Integer;               {Position f�r die Anzahl Elemente in der Liste}
       AnswerMode       : Word;                  {Antwort nur an fragende Anwendung}
       RequestFrom      : Word;                  {Frage von DDE- oder von DDEML-Verbindung}
       MacroMode        : Boolean;               {Makro-Modus ein/aus}
       MacroCnv         : HConv;                 {Konversationshandle zur sendenden Applikation w�hrend eines Makros}
       MacroHSZCT       : HSZ;                   {Topic der gerade ein Makro sendenden Applikation}
       MacroHSZCI       : HSZ;                   {Item der gerade ein Makro sendenden Applikation}
       NWUMode          : Boolean;               {TRUE, wenn kein BildschirmRedraw, weil mehrere DDE-Befehle schnell hintereinander}
       SendWithWM       : Boolean;               {soll an mit alter DDE-Methode verbundene Anwendung DDE-String gesendet werden}
       InAction         : Boolean;               {TRUE, wenn die DDE-Verbindung gerade Befehle versendet}
       DidReadDBLi      : Boolean;               {wurde DB-Abschnitt schon ausgelesen}
       DDEApps          : PCollection;           {Liste aller Anwendungen, die im DB-Abschnitt der ini aufgef�hrt sind}
       DDEMLApps        : PCollection;           {Liste aller Anwendungen, die im DDEML-Abschnitt der ini aufgef�hrt sind}
       SSAppFName       : Array[0..255] of Char; {Pfad und Dateiname der zu aktivierenden Anwendung}
       SSCService       : Array[0..255] of Char; {Service und Dateiname der zu aktivierenden Anwendung}
       SSCTopic         : Array[0..255] of Char; {Topic und Dateiname der zu aktivierenden Anwendung}
       SSCItem          : Array[0..255] of Char; {Item und Dateiname der zu aktivierenden Anwendung}
       SendToApps       : Word;                  {ausgew�hlter Men�punkt, um zu unterscheiden, welche Anwendung String erh�lt}
       AppsConn         : Word;                  {Bitfeld f�r zu bedienende Befehlss�tze (1=Wingis,2=Desma)}
       SStrDDEWM        : Boolean;               {noch nicht verwendet!!!!!!!!!!!!!!!!}
       AMainWindowHndl  : HWnd;                  {FensterHandle des WinGIS-Hauptfensters}
       ActualChild      : Pointer;               {Adresse des aktiven Projektfensters}
       CommMode         : Word;                  {enth�lt Info �ber das Kommando, das als n�chstes gesendet werden soll}
       SendGReady       : Boolean;               {TRUE, wenn noch kein GraphicReady-Kommando nach der Initialsierung gesendet}
       DataMode         : Boolean;               {TRUE, wenn die Datenbank dazu auffordert, Daten anzufordern}
       TempName         : Array[0..255] of Char; {Zwischenspeicher f�r komplette Pfadangabe im InitMode}
       FLayerInfo       : Word;                  {enth�lt h�chste Layerinfoanforderung aller verbundenen DBs}
       FGotLInfo        : Word;                  {enth�lt Layerinfoangabe der zuletzt sendenden DB}
       FDDESepSign      : Char;                  {Trennzeichen in DDE-Stings der zuletzt sendenden DB}
       FOutSepSign      : Char;                  {Trennzeichen zur Erzeugung zu versendender DDE-Strings}
       FSendMWR         : Boolean;               {TRUE, wenn min. eine DB eine MainWindowReadyMessage erh�lt}
       FSendGR          : Boolean;               {TRUE, wenn min. eine DB eine GraphicReadyMessage erh�lt}
       FSendGRAV        : Boolean;               {TRUE, wenn min. eine DB eine GraphicReadyMessageAfterView erh�lt}
       FSendLMSets      : Boolean;               {TRUE, wenn min. eine DB Layerinfos nach OK in Layermanager erh�lt}
       FShowADSA        : Boolean;               {TRUE, wenn min. eine DB ShowGraphic nach DeselectAll anfordert}
       FOneSelect       : Boolean;               {TRUE, wenn min. eine DB sofort die ID eines selekierten Objekts erhalten soll}
       FMoreSelect      : Boolean;               {TRUE, wenn min. eine DB normales Monitoring anfordert}
       FDBAutoIn        : Boolean;               {TRUE, wenn min. eine DB sofort Infos �ber ein neu erzeugtes Objekt erhalten soll}
       FNoDBAutoIn      : Boolean;               {TRUE, wenn min. eine DB kein Auto-Edit-Insert erwartet}
       FOpen            : Word;                  {1, wenn min. eine DB volle Info �ber ein ge�ffnetes Projekt erhalten soll}
       FSave            : Word;                  {1, wenn min. eine DB volle Info �ber ein gespeichertes Projekt erhalten soll}
       FSaveAs          : Word;                  {1, wenn min. eine DB v. Info �. ein unter anderem Namen gesp. Proj. erhalten soll}
       FNew             : Word;                  {1, wenn min. eine DB volle Info �ber ein neues Projekt erhalten soll}
       FChange          : Word;                  {1, wenn min. eine DB volle Info �ber einen Projektwechsel erhalten soll}
       FClose           : Word;                  {1, wenn min. eine DB volle Info �ber ein geschlossenes Projekt erhalten soll}
       FSuspendDDE      : Boolean;               {1, wenn die DDE-Kommunikation unterbrochen werden soll}
       FSending         : Boolean;               {TRUE, w�hrend DDEClientTransaction in Gang}
       DoAutoIns        : Boolean;               {TRUE, wenn AutoInsert ausgef�hrt werden soll}
       DDEDataSend      : Word;                  {Z�hler der Zust�nde, die das Senden von DDE-Befehlen untersagen}
       DDEDataRecv      : Word;                  {Z�hler der Zust�nde, die das Empfangen von DDE-Befehlen untersagen}
       ProjDrawing      : Boolean;               {TRUE, wenn das Grafikfenster gerade aufgebaut wird}
       IniSepSign       : Char;                  {in der ini festgelegtes allgemeines DDE-Trennzeichen}
       DBLoaded         : Integer;               {verbundene DB f�r Window-Message-basierendes DDE}
       WGisService      : Array[0..255] of Char; {Service-Einstellung des WinGIS-DDEML-Servers}
       WGisTopic        : Array[0..255] of Char; {Topic-Einstellung des WinGIS-DDEML-Servers}
       WGisItem         : Array[0..255] of Char; {Item-Einstellung des WinGIS-DDEML-Servers}
       DDEMLLoaded      : Integer;               {Anzahl der bestehenden DDEML-Verbindungen}
       MWReady          : Boolean;               {wenn TRUE, wird MainWindowReady an Anwendungen gesendet, die es erwarten}
       GReady           : Boolean;               {wenn TRUE, wird GraphicReady an Anwendungen gesendet, die es erwarten}
       GReadyAfterView  : Boolean;               {wenn TRUE, wird GraphicReadyAfterView an Anwendungen gesendet, die es erwarten}
       SendLMSettings   : Boolean;               {wenn TRUE, werden LayerManagerSettings an Anwendungen gesendet, die es erwarten}
       ShowAfterDSA     : Boolean;               {wenn TRUE, wird die Anwendung, die ein DeselectAll aufgerufen hat, wieder aktiviert}
       OneSelect        : Boolean;               {wenn TRUE, ist OneSelect f�r min. eine Verbindung aktiviert}
       DBAutoInsert     : Boolean;               {wenn TRUE, wird eine Datenbank, die es erwartet, �ber neue Objekte informiert}
       DDEMLConnect     : Boolean;               {allgemeine ini-Einstellung von DDEMLConnect}
       DDEMLSend        : Boolean;               {allgemeine ini-Einstellung von DDEMLSend}
       DBLayerInfos     : Word;                  {Format der Layer-Infos}
       DDEMLCallBack    : Word;                  {Art, wie die Callback-Routinen reagieren}
       DBSelMode        : Boolean;               {Selektionsmodus der Datenbanken}
       SendInfosToDB    : LongInt;               {Flags f�r die von den Datenbanken angeforderten Projektinfos}
       DoOneSelect      : Boolean;               {OneSelect ist aktiv - FrameSelect mu� inaktiv sein}
       FrameSelect      : Boolean;               {FrameSelect ist aktiv - OneSelect mu� inaktiv sein}
       Constructor Init(ActWinHandle:HWnd);
       Destructor  Done; virtual;
       Function    Append(AStr:String):Boolean;
       Function    AppendList(Index:LongInt):Boolean;
       Function    AppendList1(Index:LongInt;NameLength:Integer):Boolean;
       Function    AppendString(AStr:String):Boolean;
       Function    ChangeInitCommands(ACOpenInfo,ACSaveInfo,ACSaveAsInfo,ACNewInfo,ACChangeInfo,ACCloseInfo:Word):Word;
       Procedure   CloseProject(AChildWindow:Pointer);
       Procedure   CombineParamsOfAllActiveConnections;
       Procedure   ConnectAndActivateApp(InactivateOther:Boolean);
       Procedure   DDEAcknowledge(var Msg:TMessage);
       Procedure   DDEInitiate(var Msg:TMessage);
       Function    DDEOk:Boolean;
       Procedure   DDETerminate(ServerHandle:THandle;MustBeSender:Boolean;TermDDEML:Boolean);
       Function    DDEWMOk:Boolean;
       Procedure   DeleteAllDDEData;
       Procedure   DeleteDDEData;
       Procedure   DeleteDDEDataComplete;
       Procedure   DeleteUnSendComms;
       Procedure   DoAllDDECommands(ActChild:Pointer);
       Procedure   DoAllDDEMLCommands(ActChild:Pointer);
       Function    EnableDDEMLConversation(AEnable:Boolean):Boolean;
       Procedure   EnableDDEReception(AEnable:Boolean);
       Function    EndList(SendEnd:Boolean):Boolean;
       Function    EndList1(SendEnd:Boolean;NameLength:Integer):Boolean;
       Function    Execute:Boolean;
       Procedure   FreeGlobal;
       Function    GetDBLInfo:Word;
       Function    GetDBStrFormat:Word;
       Function    GetDDEMLLoaded:Integer;
       Function    GetGlobal(Size:LongInt):Boolean;
       Function    InitiateDDE(AWindow:THandle;LoadDatabase:Boolean;InitDDEML:Boolean):Boolean;
       Procedure   InsertAllDDEData(DDELine:PChar);
       Procedure   InsertDDEData(DDELine:PChar);
       Procedure   InsertUnSendComms(DDELine:PChar);
       Function    IsSequenceMode:Boolean;
       Function    QuestHServer:Word;
       Function    QuestLCnt:Integer;
       Function    ReadDBWMSettings(AppFile,AppName,TopicName:PChar;var AMenus:Integer;
                                    var AActive:Boolean;var AStrFormat:Word;var ADBCLayerInf:Word):Boolean;
       Function    ReadDDEMLWingisServerSettings(Service,Topic,Item:PChar):Boolean;
       Function    RegisterDBClientInfo(ADDEInfo:PChar;AHSCnv:HConv;AStrFmt:Word;var AStartDBLInf:Word;
                                    var AStartCBack:Word):Boolean;
       Procedure   Reset;
       Procedure   RestoreInitCommand;
       Procedure   SendAck(AWindow:HWnd;AParam:LongInt);
       Function    SendPChar(AText:PChar):Boolean;
       Procedure   SendSHWADSA;
       Function    SendString(AText:String):Boolean;
       Procedure   SetActualChild(AChild:Pointer);
       Procedure   SetAnswerMode(SetMode:Boolean);
       Procedure   SetDBSendStrings(SendMode:Word;ProjToInit:Pointer);
       Procedure   SetDBWMConnected;
       Procedure   SetDBWMProjInited(SetInited:Boolean);
       Procedure   SetDBWMUnconnected;
       Procedure   SetDDECommMode(AConversation:Word;AConvName:PChar);
       Procedure   SetIniSettings(AIniSepSign:Char;AMWReady,AGReady,ASendLMSettings,AGReadyAfterView,AShowAfterDSA,
                                  AOneSelect,ADBAutoInsert,ADDEMLConnect,ADDEMLSend,ADBSelMode,ADoOneSelect,
                                  AFrameSelect:Boolean;
                                  ADBLayerInfos,ADDEMLCallBack:Word;ASendInfosToDB:LongInt);
       Procedure   StartList(AText:String);
       Function    TranslateInStringDesma(ADDEString:PChar;var EndMode:Boolean):PChar;
       Function    TranslateOutStringDesma(ADDEString:PChar):PChar;
       Function    TranslateOutStringLayerInfo(ADDEString:PChar;DBCLayerInfo:Word;var LIFMode:Boolean):PChar;
       Function    TranslateOutStringSepSign(ADDEString:PChar;ASepSign:Char):PChar;
       Procedure   UnActivateConnections;
       Procedure   UnConnect(ADelCnv:HConv);
       Function    Write(Pos:LongInt;AStr:String):Boolean;
{$IFDEF DESMA}
       Function    AktActive:Boolean;
       Procedure   AktActivate(ActivateAkt:Boolean);
{$ENDIF}
     end;

var DDEHandler          : PDDEHandler;

Implementation

Uses WinDos,DDEWithWindowMessages,DDEWithDDEML,{AM_Main,}{AM_Child,}{AM_Ini,}{AM_Def,}{AM_Layer,}{AM_ProjO,}
     {AM_Log,}AnsiStr{,MenuFn}{,UserIntf};

{**************************************** Init ****************************************}

Constructor TDDEHandler.Init
   (
   ActWinHandle         : HWnd
   );
  begin
    Inherited Init;
    AMainWindowHndl:=ActWinHandle;
    ActualChild:=NIL;
    DDEApps:=New(PCollection,Init(3,3));
    DDEMLApps:=New(PCollection,Init(3,3));
    DDEData:=New(PBigUStrColl,Init);
    AllDDEData:=New(PBigUStrColl,Init);
    UnSendComms:=New(PBigUStrColl,Init);
    {DDEWMHandler:=New(PDDEWMHandler,Init);
    DDEMLServer:=New(PDDEMLServer,Init(ActWinHandle));}
    AnswerMode:=AMFALSE;
    RequestFrom:=AMFALSE;
    MacroMode:=FALSE;
    SendWithWM:=FALSE;
    SendToApps:=0;
    AppsConn:=NoConn;
    DidReadDBLi:=FALSE;
    CommMode:=DDECommAll;
    SendGReady:=FALSE;
    DataMode:=FALSE;
    HGlobal:=0;
    GlobalPtr:=NIL;
    GSize:=0;
    Position:=0;
    LString:='';
    LCnt:=0;
    LCntPos:=0;
    MacroCnv:=0;
    MacroHSZCT:=0;
    MacroHSZCI:=0;
    FLayerInfo:=0;
    FGotLInfo:=0;
    FDDESepSign:=',';
    FSendMWR:=FALSE;
    FSendGR:=FALSE;
    FSendGRAV:=FALSE;
    FSendLMSets:=FALSE;
    FOneSelect:=FALSE;
    FMoreSelect:=FALSE;
    FDBAutoIn:=FALSE;
    FNoDBAutoIn:=FALSE;
    FOpen:=0;
    FSave:=0;
    FSaveAs:=0;
    FNew:=0;
    FChange:=0;
    FClose:=0;
    InAction:=FALSE;
    FSuspendDDE:=FALSE;
    FSending:=FALSE;
    NWUMode:=FALSE;
    DoAutoIns:=TRUE;
    DDEDataSend:=0;
    DDEDataRecv:=0;
    ProjDrawing:=FALSE;
    IniSepSign:=',';
    DBLoaded:=0;
    StrCopy(WGisService,'');
    StrCopy(WGisTopic,'');
    StrCopy(WGisItem,'');
    DDEMLLoaded:=0;
    MWReady:=FALSE;
    GReady:=FALSE;
    GReadyAfterView:=FALSE;
    SendLMSettings:=FALSE;
    ShowAfterDSA:=FALSE;
    OneSelect:=FALSE;
    DBAutoInsert:=FALSE;
    DDEMLConnect:=FALSE;
    DDEMLSend:=FALSE;
    DBLayerInfos:=0;
    DDEMLCallBack:=DBCBackBool;
    DBSelMode:=FALSE;
    SendInfosToDB:=0;
    DoOneSelect:=FALSE;
    FrameSelect:=TRUE;
  end;

{**************************************** Done ****************************************}

Destructor TDDEHandler.Done;
  begin
    if DDEMLServer <> NIL then begin
      DDEMLServer^.TerminateDDE;
      Dispose(DDEMLServer,Done);
    end;
    DDETerminate(0,TRUE,TRUE);
    if DDEWMHandler <> NIL then Dispose(DDEWMHandler,Done);
    if UnSendComms^.GetCount > 0 then DeleteUnSendComms;
    Dispose(UnSendComms,Done);
    if AllDDEData^.GetCount > 0 then DeleteAllDDEData;
    Dispose(AllDDEData,Done);
    if DDEData^.GetCount > 0 then DeleteDDEDataComplete;
    Dispose(DDEData,Done);
    Dispose(DDEMLApps,Done);
    Dispose(DDEApps,Done);
    Inherited Done;
  end;



(*
{**************************************** DDEInitiate ****************************************}

Procedure TDDEHandler.DDEInitiate
   (
   var Msg         : TMessage
   );
  begin
    DDEWMHandler^.DDEInitiate(Msg);
  end;

{**************************************** InitiateDDE ****************************************}

Function TDDEHandler.InitiateDDE
   (
   AWindow         : THandle;
   LoadDatabase    : Boolean;
   InitDDEML       : Boolean
   )
   : Boolean;
  Procedure ConnectDisconnectClients
     (
     Item          : PDDEMLApp
     ); Far;
    var DDEMLClnt  : PDDEMLClient;
    begin
      with Item^ do begin
        if Connect then begin
          if CObjAddress = NIL then begin
            DDEMLClnt:=New(PDDEMLClient,Init(AMainWindowHndl,StrFormat,DBCLayerInf,CDDESepSign,CCallBack));
            CObjAddress:=DDEMLClnt;
          end
          else DDEMLClnt:=CObjAddress;
          if not Connected then begin
            Connected:=TRUE;
            if DDEMLClnt^.InitiateClientDDE(Name,ExeFile,StartParams,Service,Topic,Item) then begin
              Connected:=TRUE;
              if SendStrings then Active:=TRUE
              else Active:=FALSE;
              Inc(DDEMLLoaded);
            end
            else Connected:=FALSE;
          end;
        end
        else begin
          if CObjAddress <> NIL then begin
            if Connected then begin
              DDEMLServer^.ServerDisconnect(HSCnv);
              Connected:=FALSE;
              Active:=FALSE;
              Dec(DDEMLLoaded);
            end;
          end;
        end;
      end;
    end;
  begin
    if not DidReadDBLi then begin                                         {wenn DB-Liste noch nicht ausgelesen, dann jetzt}
      // IniFile^.ReadDDEApps(DDEApps);
      DidReadDBLi:=TRUE;
    end;                    
    DDEWMHandler^.InitiateDDE(AWindow,LoadDatabase);
    if InitDDEML then begin
      if (DDEMLServer <> NIL) and not DDEMLServer^.DDEOK then DDEMLServer^.InitiateDDE;
      if DDEMLApps^.Count = 0 then {IniFile^.ReadDDEMLApps(DDEMLApps)};     //
      DDEMLApps^.ForEach(@ConnectDisconnectClients);
    end;
    Result:=TRUE;
  end;

{**************************************** EnableDDEMLConversation **********************************}

Function TDDEHandler.EnableDDEMLConversation
   (
   AEnable         : Boolean
   )
   : Boolean;
  begin
    Result:=DDEMLServer^.EnableDDEConversation(AEnable);
  end;

{**************************************** EnableDDEReception **********************************}

Procedure TDDEHandler.EnableDDEReception
   (
   AEnable         : Boolean
   );
{$IFDEF LOG}
  var Tmp          : String;
{$ENDIF}
  begin
{$IFDEF LOG}
    if LogFile.Logging then begin
      Str(DDEDataRecv:0,Tmp);
      if AEnable then Tmp:='EnableDDEReception Begin - '+Tmp
      else Tmp:='DisableDDEReception - '+Tmp;
      LogFile.WriteLog(Tmp);
    end;
{$ENDIF}
    if AEnable then begin
      if DDEDataRecv > 0 then Dec(DDEDataRecv);
      if DDEDataRecv = 0 then SendString('[SOK][1]');
    end
    else begin
      Inc(DDEDataRecv);
      if DDEDataRecv = 1 then SendString('[SOK][0]');
    end;
{$IFDEF LOG}
    if LogFile.Logging then begin
      Str(DDEDataRecv:0,Tmp);
      if AEnable then Tmp:='EnableDDEReception - '+Tmp
      else Tmp:='DisableDDEReception End - '+Tmp;
      LogFile.WriteLog(Tmp);
    end;
{$ENDIF}
  end;

{**************************************** ReadDDEMLWingisServerSettings ****************************************}

Function TDDEHandler.ReadDDEMLWingisServerSettings
   (
   Service         : PChar;
   Topic           : PChar;
   Item            : PChar
   )
   : Boolean;
  begin
    StrCopy(Service,WGisService);
    StrCopy(Topic,WGisTopic);
    StrCopy(Item,WGisItem);
  end;

Procedure TDDEHandler.SetIniSettings
   (
   AIniSepSign     : Char;
   AMWReady        : Boolean;
   AGReady         : Boolean;
   ASendLMSettings : Boolean;
   AGReadyAfterView: Boolean;
   AShowAfterDSA   : Boolean;
   AOneSelect      : Boolean;
   ADBAutoInsert   : Boolean;
   ADDEMLConnect   : Boolean;
   ADDEMLSend      : Boolean;
   ADBSelMode      : Boolean;
   ADoOneSelect    : Boolean;
   AFrameSelect    : Boolean;
   ADBLayerInfos   : Word;
   ADDEMLCallBack  : Word;
   ASendInfosToDB  : LongInt
   );
  begin

  end;

{**************************************** GetDDEMLLoaded ****************************************}

Function TDDEHandler.GetDDEMLLoaded:Integer;
  begin
    Result:=DDEMLLoaded;
  end;

{**************************************** SetDBWMProjInited ****************************************}

Procedure TDDEHandler.SetDBWMProjInited
   (
   SetInited       : Boolean
   );
  begin
    DDEWMHandler^.SetProjInited(SetInited);
  end;

{**************************************** SetDBSendStrings ****************************************}

Procedure TDDEHandler.SetDBSendStrings
   (
   SendMode        : Word;
   ProjToInit      : Pointer
   );
  Procedure DoAll
     (
     Item          : PDDEMLApp
     ); Far;
    begin
      with Item^ do begin
        case SendMode of
          DDECommAll    : Active:=RealActive;
          DDECommInit   : begin
                            SendGReady:=FALSE;
                            RealActive:=Active;
                            if Active then begin
                              if InitedProj = ProjToInit then Active:=FALSE
                              else begin
                                InitedProj:=ProjToInit;
                                SendGReady:=TRUE;
                              end;
                            end;
                          end;
          DDECommMWR    : begin
                            RealActive:=Active;
                            if Active then if not CMWReady then Active:=FALSE;
                          end;
          DDECommGRD    : begin
                            RealActive:=Active;
                            if Active then if (not CGReady) or (not SendGReady) then Active:=FALSE;
                            SendGReady:=FALSE;
                          end;
          DDECommGRDAV  : begin
                            RealActive:=Active;
                            if Active then if not CGReadyAV then Active:=FALSE;
                          end;
          DDECommLMI    : begin
                            RealActive:=Active;
                            if Active then if not CSendLMInfo then Active:=FALSE;
                          end;
          DDECommSADSA  : begin
                            RealActive:=Active;
                            if Active then if not CShowADSA then Active:=FALSE;
                          end;
          DDECommOSel   : begin
                            RealActive:=Active;
                            if Active then if not COneSelect then Active:=FALSE;
                          end;
          DDECommMon    : begin
                            RealActive:=Active;
                            if Active then if COneSelect then Active:=FALSE;
                          end;
          DDECommAIns   : begin
                            RealActive:=Active;
                            if Active then if not CDBAutoIns then Active:=FALSE;
                          end;
          DDECommMIns   : begin
                            RealActive:=Active;
                            if Active then if CDBAutoIns then Active:=FALSE;
                          end;
          else            Active:=RealActive;
        end;
      end;
    end;
  begin
    case SendMode of
      DDECommAll    : begin
                        if CommMode = DDECommInit then begin
                          DDEWMHandler^.SetDBSendInitStrings(TRUE);
                          CommMode:=DDECommAll;
                        end
                        else DDEWMHandler^.SetDBSendStrings(TRUE);
                      end;
      DDECommInit   : begin
                        SendGReady:=DDEWMHandler^.SetDBSendInitStrings(FALSE);
                        CommMode:=DDECommInit;
                      end;
      DDECommMWR    : begin
                        if not MWReady then DDEWMHandler^.SetDBSendStrings(FALSE)
                        else DDEWMHandler^.SetRealActive;
                      end;
      DDECommGRD    : begin
                        if (not GReady) or (not SendGReady) then DDEWMHandler^.SetDBSendStrings(FALSE)
                        else DDEWMHandler^.SetRealActive;
                        SendGReady:=FALSE;
                      end;
      DDECommGRDAV  : begin
                        if not SendLMSettings then DDEWMHandler^.SetDBSendStrings(FALSE)
                        else DDEWMHandler^.SetRealActive;
                      end;
      DDECommPInfo  : begin
                        CommMode:=DDECommPInfo;
                      end;
      DDECommLMI    : begin
                        if not GReadyAfterView then DDEWMHandler^.SetDBSendStrings(FALSE)
                        else DDEWMHandler^.SetRealActive;
                      end;
      DDECommSADSA  : begin
                        if not ShowAfterDSA then DDEWMHandler^.SetDBSendStrings(FALSE)
                        else DDEWMHandler^.SetRealActive;
                      end;
      DDECommOSel   : begin
                        if not OneSelect then DDEWMHandler^.SetDBSendStrings(FALSE)
                        else DDEWMHandler^.SetRealActive;
                      end;
      DDECommMon    : begin
                        if OneSelect then DDEWMHandler^.SetDBSendStrings(FALSE)
                        else DDEWMHandler^.SetRealActive;
                      end;
      DDECommAIns   : begin
                        if not DBAutoInsert then DDEWMHandler^.SetDBSendStrings(FALSE)
                        else DDEWMHandler^.SetRealActive;
                      end;
      DDECommMIns   : begin
                        if DBAutoInsert then DDEWMHandler^.SetDBSendStrings(FALSE)
                        else DDEWMHandler^.SetRealActive;
                      end;
      else            begin
                        if CommMode = DDECommInit then begin
                          DDEWMHandler^.SetDBSendInitStrings(TRUE);
                          CommMode:=DDECommAll;
                        end
                        else DDEWMHandler^.SetDBSendStrings(TRUE);
                      end;
    end;
    DDEMLApps^.ForEach(@DoAll);
  end;

{**************************************** SetDDECommMode ****************************************}

Procedure TDDEHandler.SetDDECommMode
   (
   AConversation   : Word;
   AConvName       : PChar
   );
  var FoundConv    : PDDEMLApp;
  Function SearchConv
     (
     Item          : PDDEMLApp
     )
     : Boolean; Far;
    begin
      Result:=StrComp(Item^.Service,AConvName) = 0;
    end;
  begin
    if AConversation = CommDDEWM then begin
      // TMDIChild(ActualChild).Data^.PInfo^.SelectionSettings.DBSelectionMode:=IniFile^.DBSelMode;
      FDDESepSign:=IniSepSign;
    end
    else if AConversation = CommDDEML then begin
      FoundConv:=DDEMLApps^.FirstThat(@SearchConv);
      if FoundConv <> NIL then begin
        // TMDIChild(ActualChild).Data^.PInfo^.SelectionSettings.DBSelectionMode:=FoundConv^.CDBSelMode;
        FDDESepSign:=FoundConv^.CDDESepSign;
      end;
    end;
  end;

{**************************************** ConnectAndActivateApp ****************************************}

Procedure TDDEHandler.ConnectAndActivateApp
   (
   InactivateOther : Boolean
   );
  var DDEMLEntry   : PDDEMLApp;
      DDEMLClnt    : PDDEMLClient;
  Procedure SearchEntry
     (
     Item          : PDDEMLApp
     ); Far;
    begin
      if (StrComp(StrUpper(Item^.ExeFile),StrUpper(SSAppFName)) = 0)
         and (StrComp(StrUpper(Item^.Service),StrUpper(SSCService)) = 0)
         and (StrComp(StrUpper(Item^.Topic),StrUpper(SSCTopic)) = 0)
         and (StrComp(StrUpper(Item^.Item),StrUpper(SSCItem)) = 0) then DDEMLEntry:=Item
      else if InactivateOther then Item^.Active:=FALSE;
    end;
  begin
    DDEMLEntry:=NIL;
    DDEMLApps^.ForEach(@SearchEntry);
    if DDEMLEntry <> NIL then with DDEMLEntry^ do begin
      if CObjAddress = NIL then begin
        DDEMLClnt:=New(PDDEMLClient,Init(AMainWindowHndl,StrFormat,DBCLayerInf,CDDESepSign,CCallBack));
        CObjAddress:=DDEMLClnt;
      end
      else DDEMLClnt:=CObjAddress;
      if not Connected then begin
        Connected:=TRUE;
        if DDEMLClnt^.InitiateClientDDE(Name,ExeFile,StartParams,Service,Topic,Item) then begin
          Connected:=TRUE;
          Active:=TRUE;
          Inc(DDEMLLoaded);
        end
        else Connected:=FALSE;
      end
      else Active:=TRUE;
    end;
  end;

{**************************************** UnActivateConnections ****************************************}

Procedure TDDEHandler.UnActivateConnections;
  Procedure DDEUnActivate
     (
     Item          : PDDEApp
     ); Far;
    begin
      with Item^ do begin
        if SendStrings and Connected then Active:=TRUE
        else Active:=FALSE;
        if Connected then begin
          if SendStrings then DDEWMHandler^.Active:=TRUE
          else DDEWMHandler^.Active:=FALSE;
        end;
      end;
    end;
  Procedure DDEMLUnActivate
     (
     Item          : PDDEMLApp
     ); Far;
    begin
      with Item^ do begin
        if SendStrings and Connected then Active:=TRUE
        else Active:=FALSE;
      end;
    end;
  begin
    DDEApps^.ForEach(@DDEUnActivate);
    DDEMLApps^.ForEach(@DDEMLUnActivate);
  end;

{**************************************** DDETerminate ****************************************}

Procedure TDDEHandler.DDETerminate
   (
   ServerHandle    : THandle;
   MustBeSender    : Boolean;
   TermDDEML       : Boolean
   );
  Procedure TerminateClients
     (
     Item          : PDDEMLApp
     ); Far;
    begin
      with Item^ do begin
        if CObjAddress <> NIL then begin
          if Connected then begin
            PDDEMLClient(CObjAddress)^.TerminateDDE;
            Dec(DDEMLLoaded);
          end;
          Connected:=FALSE;
          Active:=FALSE;
          Dispose(PDDEMLClient(CObjAddress),Done);
          CObjAddress:=NIL;
        end;
      end;
    end;
  begin
    DDEWMHandler^.DDETerminate(ServerHandle,MustBeSender);
    if TermDDEML then DDEMLApps^.ForEach(@TerminateClients);      
  end;

{**************************************** ReInitiateDDE ****************************************}

{$IFDEF WMLT}
Function TDDEHandler.ReInitiateDDE
   (
   AWindow         : THandle;
   LoadDatabase    : Boolean
   )
   : Boolean;
  var IniOK        : Boolean;
  begin
    Result:=DDEWMHandler^.ReInitiateDDE(AWindow,LoadDatabase);
  end;
{$ENDIF}

{**************************************** RegisterDBClientInfo ****************************************}

Function TDDEHandler.RegisterDBClientInfo
   (
   ADDEInfo        : PChar;
   AHSCnv          : HConv;
   AStrFmt         : Word;
   var AStartDBLInf: Word;
   var AStartCBack : Word
   )
   : Boolean;
  const NoInfo     = -1;
  var DDEMLEntry   : PDDEMLApp;
      CommunicName : Array[0..255] of Char;
      FileName     : Array[0..255] of Char;
      StartPars    : Array[0..255] of Char;
      ServiceName  : Array[0..255] of Char;
      TopicName    : Array[0..255] of Char;
      ItemName     : Array[0..255] of Char;
      Con          : Boolean;
      ConProj      : Boolean;
      SendStr      : Boolean;
      SendProj     : Boolean;
      StrFmt       : Integer;
      LInf         : Integer;
      SSign        : Char;
      CBack        : Word;
      MWRdy        : Boolean;
      GRdy         : Boolean;
      GRdyAV       : Boolean;
      SelMod       : Boolean;
      SLMInf       : Boolean;
      SADSA        : Boolean;
      OneSel       : Boolean;
      AutoIns      : Boolean;
      OInfo        : Word;
      SInfo        : Word;
      SAInfo       : Word;
      NInfo        : Word;
      ChInfo       : Word;
      CInfo        : Word;
      ParamSize    : Integer;
      PartStr      : PChar;
      MoreEntries  : Boolean;
      Entry        : Integer;
      Index        : Integer;
      FirstChar    : Char;
      Sep          : Char;
      LastChar     : Char;
      AllParams    : Boolean;
      BufferValue  : String;
      Value        : Integer;
      Error        : Integer;
      DDEMLClnt    : PDDEMLClient;
      SaveNewSetts : Boolean;
  Function SearchEntryWithPath
     (
     Item          : PDDEMLApp
     )
     : Boolean; Far;
    var ItemEF     : PChar;
        ItemS      : PChar;
        ItemT      : PChar;
        ItemI      : PChar;
        CheckEF    : PChar;
        CheckS     : PChar;
        CheckT     : PChar;
        CheckI     : PChar;
    begin
      with Item^ do begin
        ItemEF:=StrNew(ExeFile);
        ItemS:=StrNew(Service);
        ItemT:=StrNew(Topic);
        ItemI:=StrNew(Item);
        CheckEF:=StrNew(FileName);
        CheckS:=StrNew(ServiceName);
        CheckT:=StrNew(TopicName);
        CheckI:=StrNew(ItemName);
        if (ItemEF = NIL) or (ItemS = NIL) or (ItemT = NIL) or (ItemI = NIL)
           or (CheckEF = NIL) or (CheckS = NIL) or (CheckT = NIL) or (CheckI = NIL) then Result:=FALSE
        else Result:=(StrComp(AnsiUpper(ItemEF),AnsiUpper(CheckEF)) = 0)
           and (StrComp(AnsiUpper(ItemS),AnsiUpper(CheckS)) = 0)
           and (StrComp(AnsiUpper(ItemT),AnsiUpper(CheckT)) = 0)
           and (StrComp(AnsiUpper(ItemI),AnsiUpper(CheckI)) = 0);
        if ItemEF <> NIL then StrDispose(ItemEF);
        if ItemS <> NIL then StrDispose(ItemS);
        if ItemT <> NIL then StrDispose(ItemT);
        if ItemI <> NIL then StrDispose(ItemI);
        if CheckEF <> NIL then StrDispose(CheckEF);
        if CheckS <> NIL then StrDispose(CheckS);
        if CheckT <> NIL then StrDispose(CheckT);
        if CheckI <> NIL then StrDispose(CheckI);
      end;
    end;
  Function SearchEntryWithName
     (
     Item          : PDDEMLApp
     )
     : Boolean; Far;
    var ItemN      : PChar;
        ItemS      : PChar;
        ItemT      : PChar;
        ItemI      : PChar;
        CheckN     : PChar;
        CheckS     : PChar;
        CheckT     : PChar;
        CheckI     : PChar;
    begin
      with Item^ do begin
        ItemN:=StrNew(Name);
        ItemS:=StrNew(Service);
        ItemT:=StrNew(Topic);
        ItemI:=StrNew(Item);
        CheckN:=StrNew(CommunicName);
        CheckS:=StrNew(ServiceName);
        CheckT:=StrNew(TopicName);
        CheckI:=StrNew(ItemName);
        if (ItemN = NIL) or (ItemS = NIL) or (ItemT = NIL) or (ItemI = NIL)
           or (CheckN = NIL) or (CheckS = NIL) or (CheckT = NIL) or (CheckI = NIL) then Result:=FALSE
        else Result:=(StrComp(AnsiUpper(ItemN),AnsiUpper(CheckN)) = 0)
           and (StrComp(AnsiUpper(ItemS),AnsiUpper(CheckS)) = 0)
           and (StrComp(AnsiUpper(ItemT),AnsiUpper(CheckT)) = 0)
           and (StrComp(AnsiUpper(ItemI),AnsiUpper(CheckI)) = 0);
        if ItemN <> NIL then StrDispose(ItemN);
        if ItemS <> NIL then StrDispose(ItemS);
        if ItemT <> NIL then StrDispose(ItemT);
        if ItemI <> NIL then StrDispose(ItemI);
        if CheckN <> NIL then StrDispose(CheckN);
        if CheckS <> NIL then StrDispose(CheckS);
        if CheckT <> NIL then StrDispose(CheckT);
        if CheckI <> NIL then StrDispose(CheckI);
      end;
    end;
   Function SearchForConnectionName
     (
     Item          : PDDEMLApp
     )
     : Boolean; Far;
    var ItemN      : PChar;
        CheckN     : PChar;
    begin
      ItemN:=StrNew(Item^.Name);
      CheckN:=StrNew(CommunicName);
      if (ItemN = NIL) or (CheckN = NIL) then Result:=FALSE
      else Result:=StrComp(AnsiUpper(ItemN),AnsiUpper(CheckN)) = 0;
      if ItemN <> NIL then StrDispose(ItemN);
      if CheckN <> NIL then StrDispose(CheckN);
    end;
  begin
    if AStrFmt = WinGISStrFmt then begin
      Entry:=1;
      FirstChar:=WgBlockStart;
      Sep:=FDDESepSign;
      LastChar:=WgBlockEnd;
    end
    else if AStrFmt = DesmaStrFmt then begin
      Entry:=0;
      FirstChar:=DmBlockStart;
      Sep:=FDDESepSign;
      LastChar:=DmBlockEnd;
    end;
    AllParams:=TRUE;
    SaveNewSetts:=FALSE;
    Index:=0;
    ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
    if PartStr <> NIL then begin
      StrCopy(FileName,PartStr);
      StrDispose(PartStr);
    end
    else AllParams:=FALSE;
    Index:=1;
    ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
    if PartStr <> NIL then begin
      StrCopy(ServiceName,PartStr);
      StrDispose(PartStr);
    end
    else AllParams:=FALSE;
    Index:=2;
    ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
    if PartStr <> NIL then begin
      StrCopy(TopicName,PartStr);
      StrDispose(PartStr);
    end
    else AllParams:=FALSE;
    Index:=3;
    ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
    if PartStr <> NIL then begin
      StrCopy(ItemName,PartStr);
      StrDispose(PartStr);
    end
    else AllParams:=FALSE;
    Index:=4;
    ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
    if PartStr <> NIL then begin
      StrCopy(CommunicName,PartStr);
      StrDispose(PartStr);
    end
    else StrCopy(CommunicName,'');
    Index:=5;
    ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
    if PartStr <> NIL then begin
      StrCopy(StartPars,PartStr);
      StrDispose(PartStr);
    end
    else StrCopy(StartPars,'');
    if AllParams then begin
      DDEMLEntry:=DDEMLApps^.FirstThat(@SearchEntryWithPath);
      if DDEMLEntry = NIL then begin
        DDEMLEntry:=DDEMLApps^.FirstThat(@SearchEntryWithName);
        if DDEMLEntry <> NIL then begin
          StrDispose(DDEMLEntry^.ExeFile);
          DDEMLEntry^.ExeFile:=StrNew(FileName);
        end;
      end;
      {***** Eintrag in ini gefunden *****}
      if DDEMLEntry <> NIL then begin
        DDEMLEntry^.HSCnv:=AHSCnv;
        {***** Wingis *****}
        if AStrFmt = WinGISStrFmt then begin
          Entry:=2;
          Index:=0;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.Connect:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.Connect:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=1;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.SendStrings:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.SendStrings:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=2;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'DESMA') = 0 then DDEMLEntry^.StrFormat:=DesmaStrFmt
            else if StrComp(AnsiUpper(PartStr),'WINGIS') = 0 then DDEMLEntry^.StrFormat:=WinGISStrFmt;
            StrDispose(PartStr);
          end;
          Index:=3;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'INDEX') = 0 then DDEMLEntry^.DBCLayerInf:=DBLInfoIndex
            else if StrComp(AnsiUpper(PartStr),'TEXT') = 0 then DDEMLEntry^.DBCLayerInf:=DBLInfoText
            else if StrComp(AnsiUpper(PartStr),'NONE') = 0 then DDEMLEntry^.DBCLayerInf:=DBLInfoNone;
            AStartDBLInf:=DDEMLEntry^.DBCLayerInf;
            StrDispose(PartStr);
          end
          else AStartDBLInf:=DDEMLEntry^.DBCLayerInf;
          Index:=4;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrLComp(PartStr,'#',1) = 0 then begin
              BufferValue:=StrPas(PartStr);
              Delete(BufferValue,1,1);
              Val(BufferValue,Value,Error);
              if (Error = 0) and (Value >= 0) and (Value <= 255) then DDEMLEntry^.CDDESepSign:=Chr(Value);
            end;
            StrDispose(PartStr);
          end;
          Index:=5;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'0') = 0 then DDEMLEntry^.CCallBack:=DBCBackBool
            else if StrComp(AnsiUpper(PartStr),'1') = 0 then DDEMLEntry^.CCallBack:=DBCBackDDE_;
            AStartCBack:=DDEMLEntry^.CCallBack;
            StrDispose(PartStr);
          end
          else AStartCBack:=DDEMLEntry^.CCallBack;
          Index:=6;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.ConnectProj:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.ConnectProj:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=7;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.SendStrProj:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.SendStrProj:=FALSE;
            StrDispose(PartStr);
          end;
          Entry:=3;
          Index:=0;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.CMWReady:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.CMWReady:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=1;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.CGReady:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.CGReady:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=2;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.CGReadyAV:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.CGReadyAV:=FALSE;
            StrDispose(PartStr);
          end;
          Entry:=4;
          Index:=0;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'ALL LAYERS') = 0 then DDEMLEntry^.CDBSelMode:=TRUE
            else if StrComp(AnsiUpper(PartStr),'TOPLAYER') = 0 then DDEMLEntry^.CDBSelMode:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=1;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.CSendLMInfo:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.CSendLMInfo:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=2;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.CShowADSA:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.CShowADSA:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=3;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.COneSelect:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.COneSelect:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=4;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.CDBAutoIns:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.CDBAutoIns:=FALSE;
            StrDispose(PartStr);
          end;
          Entry:=5;
          Index:=0;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'0') = 0 then DDEMLEntry^.COpenInfo:=0
            else if StrComp(AnsiUpper(PartStr),'1') = 0 then DDEMLEntry^.COpenInfo:=1;
            StrDispose(PartStr);
          end;
          Index:=1;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'0') = 0 then DDEMLEntry^.CSaveInfo:=0
            else if StrComp(AnsiUpper(PartStr),'1') = 0 then DDEMLEntry^.CSaveInfo:=1;
            StrDispose(PartStr);
          end;
          Index:=2;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'0') = 0 then DDEMLEntry^.CSaveAsInfo:=0
            else if StrComp(AnsiUpper(PartStr),'1') = 0 then DDEMLEntry^.CSaveAsInfo:=1;
            StrDispose(PartStr);
          end;
          Index:=3;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'0') = 0 then DDEMLEntry^.CNewInfo:=0
            else if StrComp(AnsiUpper(PartStr),'1') = 0 then DDEMLEntry^.CNewInfo:=1;
            StrDispose(PartStr);
          end;
          Index:=4;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'0') = 0 then DDEMLEntry^.CChangeInfo:=0
            else if StrComp(AnsiUpper(PartStr),'1') = 0 then DDEMLEntry^.CChangeInfo:=1;
            StrDispose(PartStr);
          end;
          Index:=5;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'0') = 0 then DDEMLEntry^.CCloseInfo:=0
            else if StrComp(AnsiUpper(PartStr),'1') = 0 then DDEMLEntry^.CCloseInfo:=1;
            StrDispose(PartStr);
          end;
        end
        {***** Desma *****}
        else if AStrFmt = DesmaStrFmt then begin
          Index:=5;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.Connect:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.Connect:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=6;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.SendStrings:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.SendStrings:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=7;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'DESMA') = 0 then DDEMLEntry^.StrFormat:=DesmaStrFmt
            else if StrComp(AnsiUpper(PartStr),'WINGIS') = 0 then DDEMLEntry^.StrFormat:=WinGISStrFmt;
            StrDispose(PartStr);
          end;
          Index:=8;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'INDEX') = 0 then DDEMLEntry^.DBCLayerInf:=DBLInfoIndex
            else if StrComp(AnsiUpper(PartStr),'TEXT') = 0 then DDEMLEntry^.DBCLayerInf:=DBLInfoText
            else if StrComp(AnsiUpper(PartStr),'NONE') = 0 then DDEMLEntry^.DBCLayerInf:=DBLInfoNone;
            AStartDBLInf:=DDEMLEntry^.DBCLayerInf;
            StrDispose(PartStr);
          end
          else AStartDBLInf:=DDEMLEntry^.DBCLayerInf;
          Index:=9;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrLComp(PartStr,'#',1) = 0 then begin
              BufferValue:=StrPas(PartStr);
              Delete(BufferValue,1,1);
              Val(BufferValue,Value,Error);
              if (Error = 0) and (Value >= 0) and (Value <= 255) then DDEMLEntry^.CDDESepSign:=Chr(Value);
            end;
            StrDispose(PartStr);
          end;
          Index:=10;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.CMWReady:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.CMWReady:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=11;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.CGReady:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.CGReady:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=12;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.CGReadyAV:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.CGReadyAV:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=13;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'ALL LAYERS') = 0 then DDEMLEntry^.CDBSelMode:=TRUE
            else if StrComp(AnsiUpper(PartStr),'TOPLAYER') = 0 then DDEMLEntry^.CDBSelMode:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=14;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.CSendLMInfo:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.CSendLMInfo:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=15;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.CShowADSA:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.CShowADSA:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=16;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.COneSelect:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.COneSelect:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=17;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.CDBAutoIns:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.CDBAutoIns:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=18;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'0') = 0 then DDEMLEntry^.COpenInfo:=0
            else if StrComp(AnsiUpper(PartStr),'1') = 0 then DDEMLEntry^.COpenInfo:=1;
            StrDispose(PartStr);
          end;
          Index:=19;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'0') = 0 then DDEMLEntry^.CSaveInfo:=0
            else if StrComp(AnsiUpper(PartStr),'1') = 0 then DDEMLEntry^.CSaveInfo:=1;
            StrDispose(PartStr);
          end;
          Index:=20;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'0') = 0 then DDEMLEntry^.CSaveAsInfo:=0
            else if StrComp(AnsiUpper(PartStr),'1') = 0 then DDEMLEntry^.CSaveAsInfo:=1;
            StrDispose(PartStr);
          end;
          Index:=21;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'0') = 0 then DDEMLEntry^.CNewInfo:=0
            else if StrComp(AnsiUpper(PartStr),'1') = 0 then DDEMLEntry^.CNewInfo:=1;
            StrDispose(PartStr);
          end;
          Index:=22;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'0') = 0 then DDEMLEntry^.CChangeInfo:=0
            else if StrComp(AnsiUpper(PartStr),'1') = 0 then DDEMLEntry^.CChangeInfo:=1;
            StrDispose(PartStr);
          end;
          Index:=23;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'0') = 0 then DDEMLEntry^.CCloseInfo:=0
            else if StrComp(AnsiUpper(PartStr),'1') = 0 then DDEMLEntry^.CCloseInfo:=1;
            StrDispose(PartStr);
          end;
          Index:=24;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'0') = 0 then DDEMLEntry^.CCallBack:=DBCBackBool
            else if StrComp(AnsiUpper(PartStr),'1') = 0 then DDEMLEntry^.CCallBack:=DBCBackDDE_;
            AStartCBack:=DDEMLEntry^.CCallBack;
            StrDispose(PartStr);
          end
          else AStartCBack:=DDEMLEntry^.CCallBack;
          Index:=25;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.ConnectProj:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.ConnectProj:=FALSE;
            StrDispose(PartStr);
          end;
          Index:=26;
          ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
          if PartStr <> NIL then begin
            if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then DDEMLEntry^.SendStrProj:=TRUE
            else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then DDEMLEntry^.SendStrProj:=FALSE;
            StrDispose(PartStr);
          end;
        end;
        {SaveNewSetts:=TRUE;}
        SaveNewSetts:=FALSE;
      end
      {***** Eintrag in ini nicht gefunden *****}
      else begin
        if (StrComp(CommunicName,'') <> 0) and (DDEMLApps^.FirstThat(@SearchForConnectionName) = NIL) then begin
          {***** Wingis *****}
          if AStrFmt = WinGISStrFmt then begin
            Entry:=2;
            Index:=0;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then Con:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then Con:=FALSE
              else Con:=DDEMLConnect;
              StrDispose(PartStr);
            end
            else Con:=DDEMLConnect;
            Index:=1;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then SendStr:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then SendStr:=FALSE
              else SendStr:=DDEMLSend;
              StrDispose(PartStr);
            end
            else SendStr:=DDEMLSend;
            Index:=2;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'DESMA') = 0 then StrFmt:=DesmaStrFmt
              else if StrComp(AnsiUpper(PartStr),'WINGIS') = 0 then StrFmt:=WinGISStrFmt
              else StrFmt:=WinGISStrFmt;
              StrDispose(PartStr);
            end
            else StrFmt:=WinGISStrFmt;
            Index:=3;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'INDEX') = 0 then LInf:=DBLInfoIndex
              else if StrComp(AnsiUpper(PartStr),'TEXT') = 0 then LInf:=DBLInfoText
              else if StrComp(AnsiUpper(PartStr),'NONE') = 0 then LInf:=DBLInfoNone
              else LInf:=DBLayerInfos;
              StrDispose(PartStr);
            end
            else LInf:=DBLayerInfos;
            Index:=4;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrLComp(PartStr,'#',1) = 0 then begin
                BufferValue:=StrPas(PartStr);
                Delete(BufferValue,1,1);
                Val(BufferValue,Value,Error);
                if (Error = 0) and (Value >= 0) and (Value <= 255) then SSign:=Chr(Value)
                else SSign:=IniSepSign;
              end
              else SSign:=IniSepSign;
              StrDispose(PartStr);
            end
            else SSign:=IniSepSign;
            Index:=5;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'0') = 0 then CBack:=DBCBackBool
              else if StrComp(AnsiUpper(PartStr),'1') = 0 then CBack:=DBCBackDDE_
              else CBack:=DDEMLCallBack;
              StrDispose(PartStr);
            end
            else CBack:=DDEMLCallBack;
            Index:=6;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then ConProj:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then ConProj:=FALSE
              else ConProj:=FALSE;
              StrDispose(PartStr);
            end
            else ConProj:=FALSE;
            Index:=7;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then SendProj:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then SendProj:=FALSE
              else SendProj:=FALSE;
              StrDispose(PartStr);
            end
            else SendProj:=FALSE;
            Entry:=3;
            Index:=0;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then MWRdy:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then MWRdy:=FALSE
              else MWRdy:=MWReady;
              StrDispose(PartStr);
            end
            else MWRdy:=MWReady;
            Index:=1;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then GRdy:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then GRdy:=FALSE
              else GRdy:=GReady;
              StrDispose(PartStr);
            end
            else GRdy:=GReady;
            Index:=2;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then GRdyAV:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then GRdyAV:=FALSE
              else GRdyAV:=GReadyAfterView;
              StrDispose(PartStr);
            end
            else GRdyAV:=GReadyAfterView;
            Entry:=4;
            Index:=0;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'ALL LAYERS') = 0 then SelMod:=TRUE
              else if StrComp(AnsiUpper(PartStr),'TOPLAYER') = 0 then SelMod:=FALSE
              else SelMod:=DBSelMode;
              StrDispose(PartStr);
            end
            else SelMod:=DBSelMode;
            Index:=1;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then SLMInf:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then SLMInf:=FALSE
              else SLMInf:=SendLMSettings;
              StrDispose(PartStr);
            end
            else SLMInf:=SendLMSettings;
            Index:=2;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then SADSA:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then SADSA:=FALSE
              else SADSA:=ShowAfterDSA;
              StrDispose(PartStr);
            end
            else SADSA:=ShowAfterDSA;
            Index:=3;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then OneSel:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then OneSel:=FALSE
              else OneSel:=OneSelect;
              StrDispose(PartStr);
            end
            else OneSel:=OneSelect;
            Index:=4;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then AutoIns:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then AutoIns:=FALSE
              else AutoIns:=DBAutoInsert;
              StrDispose(PartStr);
            end
            else AutoIns:=DBAutoInsert;
            Entry:=5;
            Index:=0;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'0') = 0 then OInfo:=0
              else if StrComp(AnsiUpper(PartStr),'1') = 0 then OInfo:=1
              else OInfo:=(SendInfosToDB and di_ProjOpen);
              StrDispose(PartStr);
            end
            else OInfo:=(SendInfosToDB and di_ProjOpen);
            Index:=1;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'0') = 0 then SInfo:=0
              else if StrComp(AnsiUpper(PartStr),'1') = 0 then SInfo:=1
              else SInfo:=(SendInfosToDB and di_ProjSave) Shr 1;
              StrDispose(PartStr);
            end
            else SInfo:=(SendInfosToDB and di_ProjSave) Shr 1;
            Index:=2;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'0') = 0 then SAInfo:=0
              else if StrComp(AnsiUpper(PartStr),'1') = 0 then SAInfo:=1
              else SAInfo:=(SendInfosToDB and di_ProjSaveAs) Shr 2;
              StrDispose(PartStr);
            end
            else SAInfo:=(SendInfosToDB and di_ProjSaveAs) Shr 2;
            Index:=3;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'0') = 0 then NInfo:=0
              else if StrComp(AnsiUpper(PartStr),'1') = 0 then NInfo:=1
              else NInfo:=(SendInfosToDB and di_ProjNew) Shr 3;
              StrDispose(PartStr);
            end
            else NInfo:=(SendInfosToDB and di_ProjNew) Shr 3;
            Index:=4;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'0') = 0 then ChInfo:=0
              else if StrComp(AnsiUpper(PartStr),'1') = 0 then ChInfo:=1
              else ChInfo:=(SendInfosToDB and di_ProjChange) Shr 4;
              StrDispose(PartStr);
            end
            else ChInfo:=(SendInfosToDB and di_ProjChange) Shr 4;
            Index:=5;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'0') = 0 then CInfo:=0
              else if StrComp(AnsiUpper(PartStr),'1') = 0 then CInfo:=1
              else CInfo:=(SendInfosToDB and di_ProjClose) Shr 5;
              StrDispose(PartStr);
            end
            else CInfo:=(SendInfosToDB and di_ProjClose) Shr 5;
          end
          {***** Desma *****}
          else if AStrFmt = DesmaStrFmt then begin
            Index:=5;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then Con:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then Con:=FALSE
              else Con:=DDEMLConnect;
              StrDispose(PartStr);
            end
            else Con:=DDEMLConnect;
            Index:=6;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then SendStr:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then SendStr:=FALSE
              else SendStr:=DDEMLSend;
              StrDispose(PartStr);
            end
            else SendStr:=DDEMLSend;
            Index:=7;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'DESMA') = 0 then StrFmt:=DesmaStrFmt
              else if StrComp(AnsiUpper(PartStr),'WINGIS') = 0 then StrFmt:=WinGISStrFmt
              else StrFmt:=DesmaStrFmt;
              StrDispose(PartStr);
            end
            else StrFmt:=DesmaStrFmt;
            Index:=8;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'INDEX') = 0 then LInf:=DBLInfoIndex
              else if StrComp(AnsiUpper(PartStr),'TEXT') = 0 then LInf:=DBLInfoText
              else if StrComp(AnsiUpper(PartStr),'NONE') = 0 then LInf:=DBLInfoNone
              else LInf:=DBLayerInfos;
              StrDispose(PartStr);
            end
            else LInf:=DBLayerInfos;
            Index:=9;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrLComp(PartStr,'#',1) = 0 then begin
                BufferValue:=StrPas(PartStr);
                Delete(BufferValue,1,1);
                Val(BufferValue,Value,Error);
                if (Error = 0) and (Value >= 0) and (Value <= 255) then SSign:=Chr(Value)
                else SSign:=IniSepSign;
              end
              else SSign:=IniSepSign;
              StrDispose(PartStr);
            end
            else SSign:=IniSepSign;
            Index:=10;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then MWRdy:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then MWRdy:=FALSE
              else MWRdy:=MWReady;
              StrDispose(PartStr);
            end
            else MWRdy:=MWReady;
            Index:=11;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then GRdy:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then GRdy:=FALSE
              else GRdy:=GReady;
              StrDispose(PartStr);
            end
            else GRdy:=GReady;
            Index:=12;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then GRdyAV:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then GRdyAV:=FALSE
              else GRdyAV:=GReadyAfterView;
              StrDispose(PartStr);
            end
            else GRdyAV:=GReadyAfterView;
            Index:=13;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'ALL LAYERS') = 0 then SelMod:=TRUE
              else if StrComp(AnsiUpper(PartStr),'TOPLAYER') = 0 then SelMod:=FALSE
              else SelMod:=DBSelMode;
              StrDispose(PartStr);
            end
            else SelMod:=DBSelMode;
            Index:=14;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then SLMInf:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then SLMInf:=FALSE
              else SLMInf:=SendLMSettings;
              StrDispose(PartStr);
            end
            else SLMInf:=SendLMSettings;
            Index:=15;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then SADSA:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then SADSA:=FALSE
              else SADSA:=ShowAfterDSA;
              StrDispose(PartStr);
            end
            else SADSA:=ShowAfterDSA;
            Index:=16;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then OneSel:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then OneSel:=FALSE
              else OneSel:=OneSelect;
              StrDispose(PartStr);
            end
            else OneSel:=OneSelect;
            Index:=17;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then AutoIns:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then AutoIns:=FALSE
              else AutoIns:=DBAutoInsert;
              StrDispose(PartStr);
            end
            else AutoIns:=DBAutoInsert;
            Index:=18;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'0') = 0 then OInfo:=0
              else if StrComp(AnsiUpper(PartStr),'1') = 0 then OInfo:=1
              else OInfo:=(SendInfosToDB and di_ProjOpen);
              StrDispose(PartStr);
            end
            else OInfo:=(SendInfosToDB and di_ProjOpen);
            Index:=19;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'0') = 0 then SInfo:=0
              else if StrComp(AnsiUpper(PartStr),'1') = 0 then SInfo:=1
              else SInfo:=(SendInfosToDB and di_ProjSave) Shr 1;
              StrDispose(PartStr);
            end
            else SInfo:=(SendInfosToDB and di_ProjSave) Shr 1;
            Index:=20;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'0') = 0 then SAInfo:=0
              else if StrComp(AnsiUpper(PartStr),'1') = 0 then SAInfo:=1
              else SAInfo:=(SendInfosToDB and di_ProjSaveAs) Shr 2;
              StrDispose(PartStr);
            end
            else SAInfo:=(SendInfosToDB and di_ProjSaveAs) Shr 2;
            Index:=21;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'0') = 0 then NInfo:=0
              else if StrComp(AnsiUpper(PartStr),'1') = 0 then NInfo:=1
              else NInfo:=(SendInfosToDB and di_ProjNew) Shr 3;
              StrDispose(PartStr);
            end
            else NInfo:=(SendInfosToDB and di_ProjNew) Shr 3;
            Index:=22;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'0') = 0 then ChInfo:=0
              else if StrComp(AnsiUpper(PartStr),'1') = 0 then ChInfo:=1
              else ChInfo:=(SendInfosToDB and di_ProjChange) Shr 4;
              StrDispose(PartStr);
            end
            else ChInfo:=(SendInfosToDB and di_ProjChange) Shr 4;
            Index:=23;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'0') = 0 then CInfo:=0
              else if StrComp(AnsiUpper(PartStr),'1') = 0 then CInfo:=1
              else CInfo:=(SendInfosToDB and di_ProjClose) Shr 5;
              StrDispose(PartStr);
            end
            else CInfo:=(SendInfosToDB and di_ProjClose) Shr 5;
            Index:=24;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'0') = 0 then CBack:=DBCBackBool
              else if StrComp(AnsiUpper(PartStr),'1') = 0 then CBack:=DBCBackDDE_
              else CBack:=DDEMLCallBack;
              StrDispose(PartStr);
            end
            else CInfo:=DDEMLCallBack;
            Index:=25;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then ConProj:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then ConProj:=FALSE
              else ConProj:=FALSE;
              StrDispose(PartStr);
            end
            else ConProj:=FALSE;
            Index:=26;
            ReadDDECommandPChar(ADDEInfo,Entry,Index,FirstChar,Sep,LastChar,PartStr,MoreEntries);
            if PartStr <> NIL then begin
              if StrComp(AnsiUpper(PartStr),'TRUE') = 0 then SendProj:=TRUE
              else if StrComp(AnsiUpper(PartStr),'FALSE') = 0 then SendProj:=FALSE
              else SendProj:=FALSE;
              StrDispose(PartStr);
            end
            else SendProj:=FALSE;
          end;
          DDEMLEntry:=New(PDDEMLApp,Init(CommunicName,FileName,StartPars,ServiceName,TopicName,ItemName,
                                         Con,ConProj,SendStr,SendProj,StrFmt,LInf,SSign,CBack,MWRdy,
                                         GRdy,GRdyAV,SelMod,SLMInf,SADSA,OneSel,AutoIns,OInfo,SInfo,
                                         SAInfo,NInfo,ChInfo,CInfo));
          AStartDBLInf:=DDEMLEntry^.DBCLayerInf;
          AStartCBack:=DDEMLEntry^.CCallBack;
          DDEMLEntry^.HSCnv:=AHSCnv;
          DDEMLApps^.Insert(DDEMLEntry);                                              {neue Verbindung in Liste einf�gen}
          // WinGISMainForm.AddDBEntry(CommunicName,Con,ConProj,SendStr,SendProj);
          AStartDBLInf:=DBLInfoNone;
          DDEMLEntry^.HSCnv:=AHSCnv;
        end;
        SaveNewSetts:=FALSE;
      end;
    end;
    {***** Verbindung aktualisieren *****}
    if SaveNewSetts then {IniFile^.WriteDDEDDEMLSettings(DDEApps,DDEMLApps)};      //
    CombineParamsOfAllActiveConnections;
    if DDEMLEntry <> NIL then with DDEMLEntry^ do begin
      if CObjAddress <> NIL then with DDEMLEntry^ do
        PDDEMLClient(CObjAddress)^.UpdateConnectionSettings(StrFormat,DBCLayerInf,CDDESepSign,CCallBack);
      if Connect then begin
        if CObjAddress = NIL then begin
          DDEMLClnt:=New(PDDEMLClient,Init(AMainWindowHndl,StrFormat,DBCLayerInf,CDDESepSign,CCallBack));
          CObjAddress:=DDEMLClnt;
        end
        else DDEMLClnt:=CObjAddress;
        if not Connected then begin
          Connected:=TRUE;
          if DDEMLClnt^.InitiateClientDDE(Name,ExeFile,StartParams,Service,Topic,Item) then begin
            Connected:=TRUE;
            if SendStrings then begin
              Active:=TRUE;
              if ActualChild <> NIL then begin
                SetDBWMProjInited(TRUE);
                NWUMode:=TRUE;
                SetDBSendStrings(DDECommInit,ActualChild);
                // TMDIChild(ActualChild).SendProjDataToDB;
                SetDBSendStrings(DDECommAll,NIL);
                SetDBSendStrings(DDECommGRD,NIL);
                SendString('[RED][]');
                NWUMode:=FALSE;
                // PostMessage(Application^.MainWindow^.HWindow,wm_Command,cm_UpdateClients,0);
                SetDBSendStrings(DDECommAll,NIL);
              end;
            end
            else Active:=FALSE;
            Inc(DDEMLLoaded);
          end
          else Connected:=FALSE;
        end;
      end
      else begin     { if not Connect then}
        if CObjAddress <> NIL then begin
          if Connected then begin
            DDEMLServer^.ServerDisconnect(HSCnv);
            Connected:=FALSE;
            Active:=FALSE;
            Dec(DDEMLLoaded);
          end;
        end;
      end;
    end;
  end;

{**************************************** UnConnect ****************************************}

Procedure TDDEHandler.UnConnect
   (
   ADelCnv         : HConv
   );
  Procedure TerminateClient
     (
     Item          : PDDEMLApp
     ); Far;
    begin
      with Item^ do begin
        if CObjAddress <> NIL then begin
          if ((HSCnv = ADelCnv) or (PDDEMLClient(CObjAddress)^.HCCnv = ADelCnv)) and Connected then begin
            PDDEMLClient(CObjAddress)^.ClientDisconnect;
            Connected:=FALSE;
            Active:=FALSE;
            RealActive:=FALSE;
            InitedProj:=NIL;
          end;
        end;
      end;
    end;
  begin
    DDEMLApps^.ForEach(@TerminateClient);
  end;

{**************************************** CombineParamsOfAllActiveConnections ****************************************}

Procedure TDDEHandler.CombineParamsOfAllActiveConnections;
  var CheckOneSel  : Boolean;
      CheckAutoEIn : Boolean;
      EntryChecked : Boolean;
      FirstSepSign : Char;
      DiffSepSigns : Boolean;
  Procedure CombineDDEParams
     (
     Item          : PDDEApp
     ); Far;
    begin
      with Item^ do begin
        if Connected and (ExeFile <> '') and (Service <> '') and (Topic <> '') then begin
          SendWithWM:=TRUE;
          if StrFormat = WinGISStrFmt then AppsConn:=AppsConn or WgisConn
          else if StrFormat = DesmaStrFmt then AppsConn:=AppsConn or DesmaConn;
          FLayerInfo:=DBCLayerInf;
          FirstSepSign:=IniSepSign;
          FSendMWR:=MWReady;
          FSendGR:=GReady;
          FSendGRAV:=GReadyAfterView;
          FSendLMSets:=SendLMSettings;
          FShowADSA:=ShowAfterDSA;
          FOneSelect:=OneSelect;
          if not FOneSelect then CheckOneSel:=FALSE;
          FDBAutoIn:=DBAutoInsert;
          if not FDBAutoIn then CheckAutoEIn:=FALSE;
          FOpen:=SendInfosToDB and di_ProjOpen;
          FSave:=(SendInfosToDB and di_ProjSave) Shr 1;
          FSaveAs:=(SendInfosToDB and di_ProjSaveAs) Shr 2;
          FNew:=(SendInfosToDB and di_ProjNew) Shr 3;
          FChange:=(SendInfosToDB and di_ProjChange) Shr 4;
          FClose:=(SendInfosToDB and di_ProjClose) Shr 5;
          EntryChecked:=TRUE;
        end;
      end;
    end;
  Procedure CombineDDEMLParams
     (
     Item          : PDDEMLApp
     ); Far;
    begin
      with Item^ do begin
        if Connect then begin
          if StrFormat = WinGISStrFmt then AppsConn:=AppsConn or WgisConn
          else if StrFormat = DesmaStrFmt then AppsConn:=AppsConn or DesmaConn;
          if DBCLayerInf > FLayerInfo then FLayerInfo:=DBCLayerInf;
          if not EntryChecked then FirstSepSign:=CDDESepSign
          else if CDDESepSign <> FirstSepSign then DiffSepSigns:=TRUE;
          if CMWReady then FSendMWR:=TRUE;
          if CGReady then FSendGR:=TRUE;
          if CGReadyAV then FSendGRAV:=TRUE;
          if CSendLMInfo then FSendLMSets:=TRUE;
          if CShowADSA then FShowADSA:=TRUE;
          if COneSelect then FOneSelect:=TRUE
          else CheckOneSel:=FALSE;
          if CDBAutoIns then FDBAutoIn:=TRUE
          else CheckAutoEIn:=FALSE;
          if COpenInfo > FOpen then FOpen:=COpenInfo;
          if CSaveInfo > FSave then FSave:=CSaveInfo;
          if CSaveAsInfo > FSaveAs then FSaveAs:=CSaveAsInfo;
          if CNewInfo > FNew then FNew:=CNewInfo;
          if CChangeInfo > FChange then FChange:=CChangeInfo;
          if CCloseInfo > FClose then FClose:=CCloseInfo;
          EntryChecked:=TRUE;
        end;
      end;
    end;
  begin
    SendWithWM:=FALSE;
    CheckOneSel:=TRUE;
    CheckAutoEIn:=TRUE;
    EntryChecked:=FALSE;
    DiffSepSigns:=FALSE;
    AppsConn:=NoConn;
    FLayerInfo:=0;
    FirstSepSign:=',';
    FSendMWR:=FALSE;
    FSendGR:=FALSE;
    FSendGRAV:=FALSE;
    FSendLMSets:=FALSE;
    FShowADSA:=FALSE;
    FOneSelect:=FALSE;
    FMoreSelect:=FALSE;
    FDBAutoIn:=FALSE;
    FNoDBAutoIn:=FALSE;
    FOpen:=0;
    FSave:=0;
    FSaveAs:=0;
    FNew:=0;
    FChange:=0;
    FClose:=0;
    DDEApps^.ForEach(@CombineDDEParams);
    DDEMLApps^.ForEach(@CombineDDEMLParams);
    if DiffSepSigns then FOutSepSign:=IndepSepSign
    else FOutSepSign:=FirstSepSign;
    if not EntryChecked then begin
      CheckOneSel:=FALSE;
      CheckAutoEIn:=FALSE;
    end;
    if CheckOneSel then begin
      DoOneSelect:=TRUE;
      FrameSelect:=FALSE;
      FMoreSelect:=FALSE;
      // MenuFunctions['DatabaseAutoMonitoring'].Checked:=TRUE;
    end
    else begin
      FMoreSelect:=TRUE;
      if FOneSelect then begin
        DoOneSelect:=TRUE;
        FrameSelect:=FALSE;
        // MenuFunctions['DatabaseAutoMonitoring'].Checked:=TRUE;
      end
      else begin
        DoOneSelect:=FALSE;
        FrameSelect:=TRUE;
        // MenuFunctions['DatabaseAutoMonitoring'].Checked:=FALSE;
      end;
    end;
    if CheckAutoEIn then begin
      DoAutoIns:=TRUE;
      FNoDBAutoIn:=FALSE;
      // MenuFunctions['DatabaseAutoEditInsert'].Checked:=TRUE;
    end
    else begin
      FNoDBAutoIn:=TRUE;
      if FDBAutoIn then begin
        DoAutoIns:=TRUE;
        // MenuFunctions['DatabaseAutoEditInsert'].Checked:=TRUE;
      end
      else begin
        DoAutoIns:=FALSE;
        // MenuFunctions['DatabaseAutoEditInsert'].Checked:=FALSE;
      end;
    end;
  end;

{**************************************** ReadDBWMSettings ****************************************}

Function TDDEHandler.ReadDBWMSettings
   (
   AppFile          : PChar;
   AppName          : PChar;
   TopicName        : PChar;
   var AMenus       : Integer;
   var AActive      : Boolean;
   var AStrFormat   : Word;
   var ADBCLayerInf : Word
   )
   : Boolean;
  var ActiveApp     : PDDEApp;
  Function SearchActiveApp
     (
     Item           : PDDEApp
     )
     : Boolean; Far;
    begin
      Result:=(Item^.Connect = TRUE) and (StrComp(Item^.ExeFile,'') <> 0) and
         (StrComp(Item^.Service,'') <> 0) and (StrComp(Item^.Topic,'') <> 0);
    end;
  begin
    Result:=FALSE;
    ActiveApp:=DDEApps^.FirstThat(@SearchActiveApp);
    if ActiveApp <> NIL then with ActiveApp^ do begin
      Result:=TRUE;
      if ExeFile <> NIL then StrCopy(AppFile,ExeFile)
      else Result:=FALSE;
      if Service <> NIL then StrCopy(AppName,Service)
      else Result:=FALSE;
      if Topic <> NIL then StrCopy(TopicName,Topic)
      else Result:=FALSE;
      AMenus:=Menu;
      AActive:=SendStrings;
      AStrFormat:=StrFormat;
      ADBCLayerInf:=DBCLayerInf;
    end;
  end;

{**************************************** SetDBWMConnected ****************************************}

Procedure TDDEHandler.SetDBWMConnected;
  Procedure DoAll
     (
     Item          : PDDEApp
     ); Far;
    begin
      with Item^ do begin
        if Connect then Connected:=TRUE
        else Connected:=FALSE;
      end;
    end;
  begin
    DDEApps^.ForEach(@DoAll);
  end;

{**************************************** SetDBWMUnconnected ****************************************}

Procedure TDDEHandler.SetDBWMUnconnected;
  Procedure DoAll
     (
     Item          : PDDEApp
     ); Far;
    begin
      Item^.Connected:=FALSE;
    end;
  begin
    DDEApps^.ForEach(@DoAll);
  end;

{**************************************** SetAnswerMode ****************************************}

Procedure TDDEHandler.SetAnswerMode
   (
   SetMode         : Boolean
   );
  begin
    if SetMode then AnswerMode:=RequestFrom
    else AnswerMode:=AMFALSE;
  end;

{**************************************** SendSHWADSA ****************************************}

Procedure TDDEHandler.SendSHWADSA;
  begin
    SetAnswerMode(TRUE);
    SetDBSendStrings(DDECommSADSA,NIL);
    SendString('[SHW][]');
    SetDBSendStrings(DDECommAll,NIL);
    SetAnswerMode(FALSE);
  end;

{**************************************** ChangeInitCommands ****************************************}

Function TDDEHandler.ChangeInitCommands
   (
   ACOpenInfo      : Word;
   ACSaveInfo      : Word;
   ACSaveAsInfo    : Word;
   ACNewInfo       : Word;
   ACChangeInfo    : Word;
   ACCloseInfo     : Word
   )
   : Word;
  Function MakeNewCommand
     (
     AComm     : String
     )
     : Boolean;
    var ProjInfo : String;
        TmpStr   : Array[0..255] of Char;
        StartPos : PChar;
        EndPos   : PChar;
        Dir      : Array[0..255] of Char;
        DName    : Array[0..255] of Char;
        Ext      : Array[0..255] of Char;
    begin
      StrCopy(TempName,GlobalPtr^);
      FreeGlobal;
      StartPos:=AnsiStrScan(TempName,WgBlockEnd);
      StartPos:=AnsiStrScan(StartPos,WgBlockStart);
      StartPos:=AnsiNext(StartPos);
      EndPos:=AnsiStrScan(StartPos,WgBlockEnd);
      StrLCopy(TmpStr,StartPos,EndPos-StartPos);
      FileSplit(TmpStr,Dir,DName,Ext);
      ProjInfo:='['+AComm+']['+StrPas(DName)+']';
      if GetGlobal(Length(ProjInfo)+1) then begin
        Append(ProjInfo+#0);
        Result:=TRUE;
      end
      else Result:=FALSE;
    end;
  begin
    if StrLComp(GlobalPtr^,'[PRN]',5) = 0 then begin
      if (FNew > 0) and (ACNewInfo = 0) then begin
        if MakeNewCommand('PRW') then Result:=CommChanged
        else Result:=CommNChanged;
      end
      else Result:=CommNChanged;
    end
    else if StrLComp(GlobalPtr^,'[PRO]',5) = 0 then begin
      if (FOpen > 0) and (ACOpenInfo = 0) then begin
        if MakeNewCommand('PRW') then Result:=CommChanged
        else Result:=CommNChanged;
      end
      else Result:=CommNChanged;

    end
    else if StrLComp(GlobalPtr^,'[PRW]',5) = 0 then begin
      if (FChange > 0) and (ACChangeInfo = 0) then begin
        if MakeNewCommand('PRW') then Result:=CommChanged
        else Result:=CommNChanged;
      end
      else Result:=CommNChanged;
    end
    else if StrLComp(GlobalPtr^,'[PRS]',5) = 0 then begin
      if (FSave > 0) and (ACSaveInfo = 0) then begin
        Result:=CommNotSend;
      end
      else Result:=CommNChanged;
    end
    else if StrLComp(GlobalPtr^,'[PSA]',5) = 0 then begin
      if (FSaveAs > 0) and (ACSaveAsInfo = 0) then begin
        if MakeNewCommand('PRW') then Result:=CommChanged
        else Result:=CommNChanged;
      end
      else Result:=CommNChanged;
    end
    else if StrLComp(GlobalPtr^,'[PCL]',5) = 0 then begin
      if (FClose > 0) and (ACCloseInfo = 0) then begin
        if MakeNewCommand('PCL') then Result:=CommChanged
        else Result:=CommNChanged;
      end
      else Result:=CommNChanged;
    end
    else Result:=CommNChanged;
  end;

{**************************************** RestoreInitCommand ****************************************}

Procedure TDDEHandler.RestoreInitCommand;
  var OldComm  : String;
  begin
    FreeGlobal;
    OldComm:=StrPas(TempName);
    if GetGlobal(Length(OldComm)+1) then Append(OldComm+#0);
  end;

{**************************************** Execute ****************************************}

Function TDDEHandler.Execute
   : Boolean;
  var ChangeRes    : Word;
      SendWM       : Boolean;
  Procedure DDEMLSendString
     (
     Item          : PDDEMLApp
     ); Far;
    var ExecError  : Word;
    begin
      with Item^ do begin
        ExecError:=0;
        if Connected then begin
          if AnswerMode = AMDDEML then begin
            if Active then begin
{$IFDEF DESMA}
              if (SendToApps = 0) or ((SendToApps = 1) and (StrFormat = WinGISStrFmt)) or
                 (((SendToApps = 2) or (SendToApps = 3)) and (StrFormat = DesmaStrFmt)) then begin
{$ENDIF}
                if HSCnv = MacroCnv then begin
                  if CObjAddress <> NIL then begin
                    if not PDDEMLClient(CObjAddress)^.Execute(HGlobal,GlobalPtr) then ExecError:=1;
                  end
                  else ExecError:=2;
                end;
{$IFDEF DESMA}
              end;
{$ENDIF}
            end;
          end
          else begin
            if Active then begin
{$IFDEF DESMA}
              if (SendToApps = 0) or ((SendToApps = 1) and (StrFormat = WinGISStrFmt)) or
                 (((SendToApps = 2) or (SendToApps = 3)) and (StrFormat = DesmaStrFmt)) then begin
{$ENDIF}
                if CObjAddress <> NIL then begin
                  if (CommMode = DDECommInit) or (CommMode = DDECommPInfo) then begin
                    ChangeRes:=ChangeInitCommands(COpenInfo,CSaveInfo,CSaveAsInfo,CNewInfo,CChangeInfo,CCloseInfo)
                  end
                  else ChangeRes:=CommNChanged;
                  {
                  if (CommMode = DDECommInit) or (CommMode = DDECommPInfo) then
                    ChangeRes:=ChangeInitCommands(COpenInfo,CSaveInfo,CSaveAsInfo,CNewInfo,CChangeInfo,CCloseInfo)
                  else ChangeRes:=CommNChanged;
                  }
                  if ChangeRes <> CommNotSend then begin
                    if not PDDEMLClient(CObjAddress)^.Execute(HGlobal,GlobalPtr) then ExecError:=1;
                  end;
                  if ChangeRes = CommChanged then RestoreInitCommand;
                end
                else ExecError:=2;
{$IFDEF DESMA}
              end;
{$ENDIF}
            end;
          end;
        end;
        case ExecError of
          0 : {alles OK};
          1 : {MsgBox(TWinGISMainForm(AMainWindow).Handle,11609,mb_OK or mb_IconExclamation,StrPas(Name))};   //
          2 : {MsgBox(TWinGISMainForm(AMainWindow).Handle,11610,mb_OK or mb_IconExclamation,StrPas(Name))};   //
        end;
      end;
    end;
  begin
    if not FSuspendDDE then begin
      InAction:=TRUE;
      FSending:=FALSE;
      try
{$IFDEF DESMA}
        if SendWithWM then if (AnswerMode = AMFALSE) or (AnswerMode = AMDDEWM) then begin
          SendWM:=FALSE;
          if (SendToApps <> 2) and (SendToApps <> 3) then SendWM:=TRUE
          else SendWM:=DDEWMHandler^.StrFormat <> DesmaStrFmt;
          if SendWM then begin
            if (CommMode = DDECommInit) or (CommMode = DDECommPInfo) then
              ChangeRes:=ChangeInitCommands(IniFile^.SendInfosToDB and di_ProjOpen,IniFile^.SendInfosToDB and di_ProjSave,
                                            IniFile^.SendInfosToDB and di_ProjSaveAs,IniFile^.SendInfosToDB and di_ProjNew,
                                            IniFile^.SendInfosToDB and di_ProjChange,IniFile^.SendInfosToDB and di_ProjClose)
            else ChangeRes:=CommNChanged;
            if ChangeRes <> CommNotSend then Result:=DDEWMHandler^.Execute(HGlobal,GlobalPtr)
            else Result:=TRUE;
            if ChangeRes = CommChanged then RestoreInitCommand;
          end;
        end;
        if (AnswerMode = AMFALSE) or (AnswerMode = AMDDEML) then begin
          if SendToApps = 3 then AktActivate(TRUE);
          DDEMLApps^.ForEach(@DDEMLSendString);
          if SendToApps = 3 then AktActivate(FALSE);
        end;
{$ELSE}
        if SendWithWM then if (AnswerMode = AMFALSE) or (AnswerMode = AMDDEWM) then begin
          if ((CommMode = DDECommInit) or (CommMode = DDECommPInfo)) and not (StrComp(GlobalPtr^,'[CLO][]') = 0) then
            ChangeRes:=ChangeInitCommands(SendInfosToDB and di_ProjOpen,SendInfosToDB and di_ProjSave,
                                          SendInfosToDB and di_ProjSaveAs,SendInfosToDB and di_ProjNew,
                                          SendInfosToDB and di_ProjChange,SendInfosToDB and di_ProjClose)
          else ChangeRes:=CommNChanged;
          if ChangeRes <> CommNotSend then Result:=DDEWMHandler^.Execute(HGlobal,GlobalPtr)
          else Result:=TRUE;
          if ChangeRes = CommChanged then RestoreInitCommand;
        end;
        if (AnswerMode = AMFALSE) or (AnswerMode = AMDDEML) then DDEMLApps^.ForEach(@DDEMLSendString);
{$ENDIF}
      finally
        FSending:=FALSE;
        InAction:=FALSE;
        if not NWUMode then {TWinGISMainForm(AMainWindow).SendRDRCommAfterDDE};   //
      end;
    end;
  end;

{**************************************** DDEAcknowledge ****************************************}

Procedure TDDEHandler.DDEAcknowledge
   (
   var Msg         : TMessage
   );
  begin
    DDEWMHandler^.DDEAcknowledge(Msg);
  end;

{**************************************** SendAck ****************************************}

Procedure TDDEHandler.SendAck
   (
   AWindow         : HWnd;
   AParam          : LongInt
   );
  begin
    DDEWMHandler^.SendAck(AWindow,AParam);
  end;

{**************************************** DDEOk ****************************************}

Function TDDEHandler.DDEOk
   : Boolean;
  var DDEMLOK      : Boolean;
      ClientCnv    : PDDEMLApp;
  Function SearchConnection
     (
     Item          : PDDEMLApp
     )
     : Boolean; Far;
    begin
      Result:=Item^.Connected and Item^.Active and (Item^.CObjAddress <> NIL);
    end;
  begin
    ClientCnv:=DDEMLApps^.FirstThat(@SearchConnection);
    Result:=DDEWMHandler^.DDEOk or (ClientCnv <> NIL);
  end;

{**************************************** DDEWMOk ****************************************}

Function TDDEHandler.DDEWMOk
   : Boolean;
  begin
    Result:=DDEWMHandler^.DDEConnectionOk;
  end;

{$IFDEF DESMA}
{**************************************** AktActive ****************************************}

Function TDDEHandler.AktActive
   : Boolean;
  var AVConnection : PDDEMLApp;
  Function SearchAV
     (
     Item          : PDDEMLApp
     )
     : Boolean; Far;
    begin
      with Item^ do Result:=StrComp(Service,'Aktverw') = 0;
    end;
  begin
    AVConnection:=DDEMLApps^.FirstThat(@SearchAV);
    if AVConnection <> NIL then Result:=AVConnection^.Connected and AVConnection^.Active
    else Result:=FALSE;
  end;

{**************************************** AktActivate ****************************************}

Procedure TDDEHandler.AktActivate
   (
   ActivateAkt     : Boolean
   );
  Procedure SearchAndActivateAV
     (
     Item          : PDDEMLApp
     ); Far;
    begin
      with Item^ do begin
        if StrComp(AnsiLower(Service),'aktverw') = 0 then begin
          StrCopy(SSAppFName,ExeFile);
          StrCopy(SSCService,Service);
          StrCopy(SSCTopic,Topic);
          StrCopy(SSCItem,Item);
          ConnectAndActivateApp(FALSE);
          ActChanged:=TRUE;
        end
        else begin
          if Active then begin
            Active:=FALSE;
            ActChanged:=TRUE;
          end;
        end;
      end;
    end;
  Procedure SearchAndDeactivateAV
     (
     Item          : PDDEMLApp
     ); Far;
    begin
      with Item^ do begin
        if ActChanged then begin
          Active:=not Active;
          ActChanged:=FALSE;
        end;
      end;
    end;
  begin
    if ActivateAkt then DDEMLApps^.ForEach(@SearchAndActivateAV)
    else DDEMLApps^.ForEach(@SearchAndDeactivateAV);
  end;

{**************************************** QuestHServer ****************************************}
{$ENDIF}

Function TDDEHandler.QuestHServer
   : Word;
  begin
    Result:=DDEWMHandler^.QuestHServer;
  end;

{**************************************** GetDBLInfo ****************************************}

Function TDDEHandler.GetDBLInfo
   : Word;
  begin
    Result:=DDEWMHandler^.GetDBLInfo;
  end;

{**************************************** GetDBStrFormat ****************************************}

Function TDDEHandler.GetDBStrFormat
   : Word;
  begin
    Result:=DDEWMHandler^.GetDBStrFormat;
  end;

{**************************************** SetActualChild ****************************************}

Procedure TDDEHandler.SetActualChild
   (
   AChild          : Pointer
   );
  Procedure DoAll
     (
     Item          : PDDEMLApp
     ); Far;
    begin
      if Item^.Active then Item^.InitedProj:=AChild;
    end;
  begin
    ActualChild:=AChild;
    DDEMLServer^.SetActualChild(AChild);
    DDEMLApps^.ForEach(@DoAll);
  end;

{**************************************** CloseProject ****************************************}

Procedure TDDEHandler.CloseProject
   (
   AChildWindow    : Pointer
   );
  var Dir          : Array[0..fsPathName] of Char;
      DName        : Array[0..fsFileName+fsExtension] of Char;
      Ext          : Array[0..fsExtension] of Char;
      SendStr      : String;
  begin
    (*
    if AChildWindow <> NIL then with TMDIChild(AChildWindow) do begin
      SetDBSendStrings(DDECommPInfo,NIL);
      if FClose > 0 then SendString('[PCL]['+StrPas(FName)+']')
      else begin
        FileSplit(FName,Dir,DName,Ext);
        SendStr:=StrPas(DName);
        SendString('[PCL]['+SendStr+']');
      end;
      SetDBSendStrings(DDECommAll,NIL);
    end;
    SetActualChild(NIL);
    *)
  end;

{**************************************** IsSequenceMode ****************************************}

Function TDDEHandler.IsSequenceMode
   : Boolean;
  begin
    Result:=DDEWMHandler^.IsSequenceMode;
  end;

{*****************************************************************************************************************************}

{**************************************** DoAllDDECommands ****************************************}

Procedure TDDEHandler.DoAllDDECommands
   (
   ActChild        : Pointer
   );
  Procedure DoAll
     (
     Item          : PChar
     ); Far;
    begin
      // TMDIChild(ActChild).DoDDECommands(Item);
    end;
  begin
    SendString('[MAB][]');
    AllDDEData^.ForEach(@DoAll);
    SendString('[MAE][]');
    DeleteAllDDEData;
  end;

{**************************************** DoAllDDEMLCommands ****************************************}

Procedure TDDEHandler.DoAllDDEMLCommands
   (
   ActChild        : Pointer
   );
  Procedure DoAll
     (
     Item          : PChar
     ); Far;
    begin
      DDEMLServer^.DoExecute(MacroHSZCT,MacroHSZCI,Item,MacroCnv);
    end;
  begin
    SendString('[MAB][]');
    AllDDEData^.ForEach(@DoAll);
    SendString('[MAE][]');
    DeleteAllDDEData;
  end;

{*****************************************************************************************************************************}
{**************************************** nicht DDE-spezifische Methoden *****************************************************}
{*****************************************************************************************************************************}

{***************************************************************************}
{ Procedure TDDEHandler.FreeGlobal                                          }
{---------------------------------------------------------------------------}
{ Gibt den f�r die DDE-Kommunikation belegten globalen Speicher frei.       }
{***************************************************************************}
Procedure TDDEHandler.FreeGlobal;
  begin
    if HGlobal<>0 then begin
      GlobalUnlock(HGlobal);
      GlobalFree(HGlobal);
    end;
    HGlobal:=0;
    Position:=0;
    GSize:=0;
  end;

{***************************************************************************}
{ Function TDDEHandler.GetGlobal                                            }
{---------------------------------------------------------------------------}
{ Reserviert Speicher f�r die DDE-Kommunikation.                            }
{---------------------------------------------------------------------------}
{ Parameter:                                                                }
{  Size            i = Gr��e des Speichers in Bytes                         }
{---------------------------------------------------------------------------}
{ Objektvariablen:                                                          }
{  HGlobal           = Handle des Speicherbereichs                          }
{  GlobalPrt         = Zeiger auf den Speicherbereich                       }
{  GSize             = 0, wenn Fehler, Size sonst                           }
{  Position          = 0                                                    }
{---------------------------------------------------------------------------}
{ Ergebnis: TRUE, wenn Reservierung OK, FALSE bei Fehler                    }
{***************************************************************************}
Function TDDEHandler.GetGlobal
   (
   Size            : LongInt
   )
   : Boolean;
  begin
    FreeGlobal;
    HGlobal:=GlobalAlloc(gmem_Moveable or gmem_DDEShare,Size);
    if HGlobal<>0 then begin
      GlobalPtr:=GlobalLock(HGlobal);
      if GlobalPtr=NIL then begin
        GlobalFree(HGlobal);
        GSize:=0;
        GetGlobal:=FALSE;
      end
      else begin
        GetGlobal:=TRUE;
        GSize:=Size;
      end;
    end
    else GetGlobal:=FALSE;
  end;

Function TDDEHandler.Write
   (
   Pos             : LongInt;
   AStr            : String
   )
   : Boolean;
  begin
    if Pos+Length(AStr)<=GSize then begin
      Move(AStr[1],GlobalPtr^[Pos],Length(AStr));
      Write:=TRUE;
    end
    else Write:=FALSE;
  end;

Function TDDEHandler.Append
   (
   AStr            : String
   )
   : Boolean;
  begin
    if Write(Position,AStr) then begin
      Inc(Position,Length(AStr));
      Append:=TRUE;
    end
    else Append:=FALSE;
  end;

{****************************************************************************}
{ Procedure TDDEWMHandler.Reset                                              }
{----------------------------------------------------------------------------}
{ Setzt die aktuelle Schreibposition f�r Append auf 0.                       }
{****************************************************************************}
Procedure TDDEHandler.Reset;
  begin
    Position:=0;
  end;

Function TDDEHandler.SendPChar
   (
   AText           : PChar
   )
   : Boolean;
  Procedure SendUnSendComms
     (
     Item          : PChar
     ); Far;
    begin
     if DDEOk then begin
       if GetGlobal(StrLen(Item)+1) then begin
         StrCopy(PChar(GlobalPtr),Item);
         Result:=Execute;
         FreeGlobal;
       end;
     end;
    end;
  begin
    if InAction then begin
      InsertUnSendComms(AText);
      Result:=TRUE;
    end
    else begin
      Result:=FALSE;
      if DDEOk then begin
        if GetGlobal(StrLen(AText)+1) then begin
          StrCopy(PChar(GlobalPtr),AText);
          Result:=Execute;
          FreeGlobal;
        end;
        if UnSendComms^.GetCount > 0 then begin
          UnSendComms^.ForEach(@SendUnSendComms);
          DeleteUnSendComms;
        end;
      end;
    end;
  end;

Function TDDEHandler.SendString
   (
   AText           : String
   )
   : Boolean;
  var USComm       : Array[0..255] of Char;
  Procedure SendUnSendComms
     (
     Item          : PChar
     ); Far;
    begin
     if DDEOk then begin
       if GetGlobal(StrLen(Item)+1) then begin
         StrCopy(PChar(GlobalPtr),Item);
         Result:=Execute;
         FreeGlobal;
       end;
     end;
    end;
  begin
    if InAction then begin
      StrPCopy(USComm,AText);
      InsertUnSendComms(USComm);
      Result:=TRUE;
    end
    else begin
      Result:=FALSE;
      if DDEOk then begin
        if GetGlobal(Length(AText)+1) then begin
          Append(AText+#0);
          Result:=Execute;
          FreeGlobal;
        end;
        if UnSendComms^.GetCount > 0 then begin
          UnSendComms^.ForEach(@SendUnSendComms);
          DeleteUnSendComms;
        end;
      end;
    end;
  end;

Procedure TDDEHandler.StartList
   (
   AText           : String
   );
  begin
    LString:=AText;
    LCnt:=0;
    LCntPos:=Length(AText)-4;
    GetGlobal(ListGlobal);
    Append(AText);
  end;

Function TDDEHandler.AppendList
   (
   Index           : LongInt
   )
   : Boolean;
  var AStr         : String;
  begin
    Str(Index:10,AStr);
    AppendList:=Append('['+AStr+']');
    Inc(LCnt);
    if LCnt Mod MaxList=0 then begin
      Str(LCnt:3,AStr);
      Write(LCntPos,AStr);
      Append(#0);
      Execute;
      Reset;
      Append(LString);
      LCnt:=0;
    end;
  end;

Function TDDEHandler.AppendList1
   (
   Index           : LongInt;
   NameLength      : Integer
   )
   : Boolean;
  var AStr         : String;
  begin
    Str(Index:10,AStr);
    AppendList1:=Append('['+AStr+']');
    Inc(LCnt);
    if LCnt Mod MaxList=0 then begin
      Str(LCnt:3,AStr);
      Write(8+NameLength,AStr);
      Append(#0);
      Execute;
      Reset;
      Append(LString);
      LCnt:=0;
    end;
  end;

Function TDDEHandler.AppendString
   (
   AStr            : String
   )
   : Boolean;
  var CStr         : String;
  begin
    AppendString:=Append('['+AStr+']');
    Inc(LCnt);
    if LCnt Mod MaxList=0 then begin
      Str(LCnt:3,CStr);
      Write(LCntPos,CStr);
      Append(#0);
      Execute;
      Reset;
      Append(LString);
      LCnt:=0;
    end;
  end;

Function TDDEHandler.EndList
   (
   SendEnd         : Boolean
   )
   : Boolean;
  var AStr         : String;
    Procedure SendUnSendComms
     (
     Item          : PChar
     ); Far;
    begin
     if DDEOk then begin
       if GetGlobal(StrLen(Item)+1) then begin
         StrCopy(PChar(GlobalPtr),Item);
         Result:=Execute;
         FreeGlobal;
       end;
     end;
    end;
  begin
    if LCnt Mod MaxList<>0 then begin
      Str(LCnt:3,AStr);
      Write(LCntPos,AStr);
      Append(#0);
      if InAction then InsertUnSendComms(PChar(GlobalPtr))
      else Execute;
    end
    else begin
      Str(LCnt:3,AStr);
      Write(LCntPos,AStr);
      Append(#0);
      if InAction then InsertUnSendComms(PChar(GlobalPtr))
      else Execute;
    end;
    FreeGlobal;
    if SendEnd then SendString('[END][]');
    EndList:=LCnt<>0;
    if not InAction then begin
      if UnSendComms^.GetCount > 0 then begin
        UnSendComms^.ForEach(@SendUnSendComms);
        DeleteUnSendComms;
      end;
    end;
  end;

Function TDDEHandler.EndList1
   (
   SendEnd         : Boolean;
   NameLength      : Integer
   )
   : Boolean;
  var AStr         : String;
  Procedure SendUnSendComms
     (
     Item          : PChar
     ); Far;
    begin
     if DDEOk then begin
       if GetGlobal(StrLen(Item)+1) then begin
         StrCopy(PChar(GlobalPtr),Item);
         Result:=Execute;
         FreeGlobal;
       end;
     end;
    end;
  begin
    if LCnt Mod MaxList<>0 then begin
      Str(LCnt:3,AStr);
      Write(8+NameLength,AStr);
      Append(#0);
      if InAction then InsertUnSendComms(PChar(GlobalPtr))
      else Execute;
    end
    else begin
      Str(LCnt:3,AStr);
      Write(8+NameLength,AStr);
      Append(#0);
      if InAction then InsertUnSendComms(PChar(GlobalPtr))
      else Execute;
    end;
    FreeGlobal;
    if SendEnd then SendString('[END][]');
    EndList1:=LCnt<>0;
    if not InAction then begin
      if UnSendComms^.GetCount > 0 then begin
        UnSendComms^.ForEach(@SendUnSendComms);
        DeleteUnSendComms;
      end;
    end;
  end;

Procedure TDDEHandler.InsertDDEData
   (
   DDELine         : PChar
   );
{$IFDEF LOG}
  var LogStr       : String;
{$ENDIF}
  begin
{$IFDEF LOG}
    if LogFile.Logging then begin
      Str(DDEData^.GetCount:0,LogStr);
      LogFile.WriteLog('Start InsertDDEData with '+LogStr);
    end;
{$ENDIF}
    DDEData^.Insert(StrNew(DDELine));
{$IFDEF LOG}
    if LogFile.Logging then begin
      LogFile.WriteLog('InsertDDEData: '+DDELine);
      Str(DDEData^.GetCount:0,LogStr);
      LogFile.WriteLog('End InsertDDEData with '+LogStr);
    end;
{$ENDIF}
  end;

Procedure TDDEHandler.DeleteDDEData;
  var i            : LongInt;
{$IFDEF LOG}
      LogStr       : String;
{$ENDIF}
  Function DelString
     (
     Item          : PChar
     )
     : Boolean ;
    begin
      Result:=FALSE;
      if Item[0] = DDEStrUsed then begin
{$IFDEF LOG}
        if LogFile.Logging then LogFile.WriteLog('DeleteDDEData: '+Item);
{$ENDIF}
        StrDispose(Item);
        Result:=TRUE;
      end;
    end;
  begin
{$IFDEF LOG}
    if LogFile.Logging then begin
      Str(DDEData^.GetCount:0,LogStr);
      LogFile.WriteLog('Start DeleteDDEData with '+LogStr);
    end;
{$ENDIF}
    for i:=DDEData^.GetCount-1 downto 0 do begin
      if DelString(DDEData^.At(i)) then DDEData^.AtDelete(i);
    end;
{$IFDEF LOG}
    if LogFile.Logging then begin
      Str(DDEData^.GetCount:0,LogStr);
      LogFile.WriteLog('End DeleteDDEData with '+LogStr);
    end;
{$ENDIF}
  end;

Procedure TDDEHandler.DeleteDDEDataComplete;
{$IFDEF LOG}
  var LogStr       : String;
{$ENDIF}
  Procedure DelStrings
     (
     Item          : PChar
     ); Far;
    begin
{$IFDEF LOG}
      if LogFile.Logging then LogFile.WriteLog('DeleteDDEDataComplete: '+Item);
{$ENDIF}
      StrDispose(Item);
    end;
  begin
{$IFDEF LOG}
    if LogFile.Logging then begin
      Str(DDEData^.GetCount:0,LogStr);
      LogFile.WriteLog('Start DeleteDDEDataComplete with '+LogStr);
    end;
{$ENDIF}
    DDEData^.ForEach(@DelStrings);
    DDEData^.DeleteAll;
{$IFDEF LOG}
    if LogFile.Logging then begin
      Str(DDEData^.GetCount:0,LogStr);
      LogFile.WriteLog('End DeleteDDEDataComplete with '+LogStr);
    end;
{$ENDIF}
  end;

Procedure TDDEHandler.InsertAllDDEData
   (
   DDELine         : PChar
   );
  begin
    AllDDEData^.Insert(StrNew(DDELine));
  end;

Procedure TDDEHandler.DeleteAllDDEData;
  Procedure DelStrings
     (
     Item          : PChar
     ); Far;
    begin
      StrDispose(Item);
    end;
  begin
    AllDDEData^.ForEach(@DelStrings);
    AllDDEData^.DeleteAll;
  end;

Procedure TDDEHandler.InsertUnSendComms
   (
   DDELine         : PChar
   );
  begin
    UnSendComms^.Insert(StrNew(DDELine));
  end;

Procedure TDDEHandler.DeleteUnSendComms;
  Procedure DelStrings
     (
     Item          : PChar
     ); Far;
    begin
      StrDispose(Item);
    end;
  begin
    UnSendComms^.ForEach(@DelStrings);
    UnSendComms^.DeleteAll;
  end;

Function TDDEHandler.QuestLCnt
   : Integer;
  begin
    Result:=LCnt;
  end;

{**************************************** TranslateOutStringSepSign ****************************************}

Function TDDEHandler.TranslateOutStringSepSign
   (
   ADDEString      : PChar;
   ASepSign        : Char
   )
   : PChar;
  var SepPos       : PChar;
  begin
    if (StrLComp(ADDEString,'[CPY]',5) = 0) or (StrLComp(ADDEString,'[GRD]',5) = 0)
       or (StrLComp(ADDEString,'[LIF]',5) = 0) or (StrLComp(ADDEString,'[LMA]',5) = 0)
       or (StrLComp(ADDEString,'[LMS]',5) = 0) or (StrLComp(ADDEString,'[LVD]',5) = 0)
       or (StrLComp(ADDEString,'[LVN]',5) = 0) or (StrLComp(ADDEString,'[MOD]',5) = 0)
       or (StrLComp(ADDEString,'[NON]',5) = 0) or (StrLComp(ADDEString,'[SEL]',5) = 0) then begin
      repeat
        SepPos:=AnsiStrScan(ADDEString,FOutSepSign);
        if SepPos <> NIL then SepPos[0]:=ASepSign;
      until SepPos = NIL;
    end;
  end;

{**************************************** TranslateOutStringLayerInfo ****************************************}

Function TDDEHandler.TranslateOutStringLayerInfo
   (
   ADDEString      : PChar;
   DBCLayerInfo    : Word;
   var LIFMode     : Boolean
   )
   : PChar;
  Const TrLINameToIndex  = 1;
        TrLINameToNone   = 2;
        TrLIIndexToName  = 3;
        TrLIIndexToNone  = 4;
  var TransFkt     : Word;
      ResStr       : Array[0..4096] of Char;
      TempStr      : Array[0..255] of Char;
      ReadStr      : PChar;
      MoreEntries  : Boolean;
      RemainStr    : PChar;
      {ALayer       : PLayer;}
      ALIndex      : LongInt;
      TmpVal       : Integer;
      Error        : Integer;
      i            : Integer;
  begin
    (*
    case FLayerInfo of
      DBLInfoText  : begin
                       case DBCLayerInfo of
                         DBLInfoIndex : TransFkt:=TrLINameToIndex;
                         DBLInfoNone  : TransFkt:=TrLINameToNone;
                       end;
                     end;
      DBLInfoIndex : begin
                       case DBCLayerInfo of
                         DBLInfoText  : TransFkt:=TrLIIndexToName;
                         DBLInfoNone  : TransFkt:=TrLIIndexToNone;
                       end;
                     end;
    end;

    case TransFkt of
      TrLINameToIndex : begin
                        end;
      TrLIIndexToName : begin
                          if StrLComp(ADDEString,'[AIN]',5) = 0 then begin
                            StrCopy(ResStr,'[AIN]');
                            for i:=1 to 5 do begin
                              ReadDDECommandPChar(ADDEString,i,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                              StrCat(ResStr,'[');
                              StrCat(ResStr,ReadStr);
                              StrCat(ResStr,']');
                              StrDispose(ReadStr);
                            end;
                            ReadDDECommandPChar(ADDEString,6,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCat(ResStr,'[');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                            ReadDDECommandPChar(ADDEString,7,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            if ReadStr <> NIL then begin
                              StrCat(ResStr,'[');
                              StrCat(ResStr,ReadStr);
                              StrCat(ResStr,']');
                              StrDispose(ReadStr);
                            end;
                          end
                          else if StrLComp(ADDEString,'[ALA]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            if ReadStr <> NIL then begin
                              Val(ReadStr,ALIndex,Error);
                              StrDispose(ReadStr);
                              if Error = 0 then begin
                                ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                                if ALayer <> NIL then begin
                                  StrPCopy(TempStr,ALayer^.Text^);
                                  StrCopy(ResStr,'[ALA][');
                                  StrCat(ResStr,TempStr);
                                  StrCat(ResStr,']');
                                end
                                else StrCopy(ResStr,ADDEString);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,'[ALA][]');
                          end
                          else if StrLComp(ADDEString,'[CPY]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCopy(ResStr,'[CPY][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                RemainStr:=StrScan(ADDEString+1,'[');
                                RemainStr:=StrScan(RemainStr+1,'[');
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[DEL]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCopy(ResStr,'[DEL][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                RemainStr:=StrScan(ADDEString+1,'[');
                                RemainStr:=StrScan(RemainStr+1,'[');
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[EIN]',5) = 0 then begin
                            StrCopy(ResStr,'[EIN]');
                            for i:=1 to 5 do begin
                              ReadDDECommandPChar(ADDEString,i,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                              StrCat(ResStr,'[');
                              StrCat(ResStr,ReadStr);
                              StrCat(ResStr,']');
                              StrDispose(ReadStr);
                            end;
                            ReadDDECommandPChar(ADDEString,6,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCat(ResStr,'[');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                            ReadDDECommandPChar(ADDEString,7,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            if ReadStr <> NIL then begin
                              StrCat(ResStr,'[');
                              StrCat(ResStr,ReadStr);
                              StrCat(ResStr,']');
                              StrDispose(ReadStr);
                            end;
                          end
                          else if StrLComp(ADDEString,'[EXP]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCopy(ResStr,'[EXP][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                RemainStr:=StrScan(ADDEString+1,'[');
                                RemainStr:=StrScan(RemainStr+1,'[');
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[GDI]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCopy(ResStr,'[GDI][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                RemainStr:=StrScan(ADDEString+1,'[');
                                RemainStr:=StrScan(RemainStr+1,'[');
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[GDT]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCopy(ResStr,'[GDT][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                RemainStr:=StrScan(ADDEString+1,'[');
                                RemainStr:=StrScan(RemainStr+1,'[');
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[GRD]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCopy(ResStr,'[GRD][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                RemainStr:=StrScan(ADDEString+1,'[');
                                RemainStr:=StrScan(RemainStr+1,'[');
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[IMP]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCopy(ResStr,'[IMP][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                RemainStr:=StrScan(ADDEString+1,'[');
                                RemainStr:=StrScan(RemainStr+1,'[');
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[LEX]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCopy(ResStr,'[LEX][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                ReadDDECommandPChar(ADDEString,2,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                                if StrComp(ReadStr,'1') = 0 then StrCat(ResStr,'[1]')
                                else StrCat(ResStr,'[0]');
                                StrDispose(ReadStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[LIF]',5) = 0 then begin
                            StrCopy(ResStr,'[LIF][');
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,TmpVal,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              Str(TmpVal:3,TempStr);
                              StrCat(ResStr,TempStr);
                              StrCat(ResStr,']');
                              for i:=1 to TmpVal do begin
                                ReadDDECommandPChar(ADDEString,i+1,0,WgBlockStart,FOutSepSign,WgBlockEnd,ReadStr,MoreEntries);
                                StrCat(ResStr,'[');
                                StrCat(ResStr,ReadStr);
                                StrCat(ResStr,']');
                              end;
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[MON]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCopy(ResStr,'[MON][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                RemainStr:=StrScan(ADDEString+1,'[');
                                RemainStr:=StrScan(RemainStr+1,'[');
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if (StrLComp(ADDEString,'[MO0]',5) = 0) or (StrLComp(ADDEString,'[MO1]',5) = 0)
                             or (StrLComp(ADDEString,'[MO2]',5) = 0) or (StrLComp(ADDEString,'[MO3]',5) = 0)
                             or (StrLComp(ADDEString,'[MO4]',5) = 0) or (StrLComp(ADDEString,'[MO5]',5) = 0)
                             or (StrLComp(ADDEString,'[MO6]',5) = 0) or (StrLComp(ADDEString,'[MO7]',5) = 0)
                             or (StrLComp(ADDEString,'[MO8]',5) = 0) or (StrLComp(ADDEString,'[MO9]',5) = 0)
                             or (StrLComp(ADDEString,'[M00]',5) = 0) or (StrLComp(ADDEString,'[M01]',5) = 0)
                             or (StrLComp(ADDEString,'[M02]',5) = 0) or (StrLComp(ADDEString,'[M03]',5) = 0)
                             or (StrLComp(ADDEString,'[M04]',5) = 0) or (StrLComp(ADDEString,'[M05]',5) = 0)
                             or (StrLComp(ADDEString,'[M06]',5) = 0) or (StrLComp(ADDEString,'[M07]',5) = 0)
                             or (StrLComp(ADDEString,'[M08]',5) = 0) or (StrLComp(ADDEString,'[M09]',5) = 0) then begin
                            StrCopy(ResStr,'[');
                            ReadDDECommandPChar(ADDEString,0,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            StrCat(ResStr,ReadStr);
                            StrDispose(ReadStr);
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCat(ResStr,'][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                RemainStr:=StrScan(ADDEString+1,'[');
                                RemainStr:=StrScan(RemainStr+1,'[');
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[MOV]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCopy(ResStr,'[MOV][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                RemainStr:=StrScan(ADDEString+1,'[');
                                RemainStr:=StrScan(RemainStr+1,'[');
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[NGS]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCopy(ResStr,'[NGS][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                RemainStr:=StrScan(ADDEString+1,'[');
                                RemainStr:=StrScan(RemainStr+1,'[');
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[NHA]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCopy(ResStr,'[NHA][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                RemainStr:=StrScan(ADDEString+1,'[');
                                RemainStr:=StrScan(RemainStr+1,'[');
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[NIA]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCopy(ResStr,'[NIA][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                RemainStr:=StrScan(ADDEString+1,'[');
                                RemainStr:=StrScan(RemainStr+1,'[');
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[NTX]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCopy(ResStr,'[NTX][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                ReadDDECommandPChar(ADDEString,2,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                                StrCat(ResStr,'[');
                                StrCat(ResStr,ReadStr);
                                StrCat(ResStr,']');
                                StrDispose(ReadStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[ONE]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                if ALayer^.Text <> NIL then StrPCopy(TempStr,ALayer^.Text^)
                                else StrCopy(TempStr,'');
                                StrCopy(ResStr,'[ONE][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                RemainStr:=StrScan(ADDEString+1,'[');
                                RemainStr:=StrScan(RemainStr+1,'[');
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[PPR]',5) = 0 then begin
                            StrCopy(ResStr,'[PPR][');
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCat(ResStr,TempStr);
                              end
                              else StrCat(ResStr,ReadStr);
                            end;
                            StrCat(ResStr,'][');
                            StrDispose(ReadStr);
                            ReadDDECommandPChar(ADDEString,2,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            StrCat(ResStr,ReadStr);
                            StrCat(ResStr,'][');
                            ReadDDECommandPChar(ADDEString,3,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCat(ResStr,TempStr);
                              end
                              else StrCat(ResStr,ReadStr);
                            end;
                            StrCat(ResStr,']');
                            StrDispose(ReadStr);
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[SEL]',5) = 0 then begin
                            StrCopy(ResStr,'[SEL][');
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,FOutSepSign,WgBlockEnd,ReadStr,MoreEntries);
                            StrCat(ResStr,ReadStr);
                            StrDispose(ReadStr);
                            ReadDDECommandPChar(ADDEString,1,1,WgBlockStart,FOutSepSign,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,FOutSepSign);
                                StrCat(ResStr,TempStr);
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCat(ResStr,TempStr);
                                RemainStr:=StrScan(ADDEString+1,FOutSepSign);
                                RemainStr:=StrScan(RemainStr+1,FOutSepSign);
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[SPD]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCopy(ResStr,'[SPD][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                RemainStr:=StrScan(ADDEString+1,'[');
                                RemainStr:=StrScan(RemainStr+1,'[');
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[UPD]',5) = 0 then begin
                            StrCopy(ResStr,'[UPD]');
                            for i:=1 to 5 do begin
                              ReadDDECommandPChar(ADDEString,i,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                              StrCat(ResStr,'[');
                              StrCat(ResStr,ReadStr);
                              StrCat(ResStr,']');
                              StrDispose(ReadStr);
                            end;
                            ReadDDECommandPChar(ADDEString,6,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCat(ResStr,'[');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                            ReadDDECommandPChar(ADDEString,7,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            if ReadStr <> NIL then begin
                              StrCat(ResStr,'[');
                              StrCat(ResStr,ReadStr);
                              StrCat(ResStr,']');
                              StrDispose(ReadStr);
                            end;
                          end
                          else StrCopy(ResStr,ADDEString);
                        end;

{*****************************************************************************************************************************}

      TrLINameToNone  : begin
                          if StrLComp(ADDEString,'[CPY]',5) = 0 then begin
                            StrCopy(ResStr,'[CPY]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[END]',5) = 0 then begin
                            if LIFMode then begin
                              LIFMode:=FALSE;
                              StrCopy(ResStr,'');
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[DEL]',5) = 0 then begin
                            StrCopy(ResStr,'[DEL]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[EXP]',5) = 0 then begin
                            StrCopy(ResStr,'[EXP]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[GDI]',5) = 0 then begin
                            StrCopy(ResStr,'[GDI]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[GDT]',5) = 0 then begin
                            StrCopy(ResStr,'[GDT]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[GRD]',5) = 0 then begin
                            StrCopy(ResStr,'[GRD]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[IMP]',5) = 0 then begin
                            StrCopy(ResStr,'[IMP]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[LIF]',5) = 0 then begin
                            LIFMode:=TRUE;
                            StrCopy(ResStr,'');
                          end
                          else if StrLComp(ADDEString,'[MON]',5) = 0 then begin
                            StrCopy(ResStr,'[MON]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if (StrLComp(ADDEString,'[MO0]',5) = 0) or (StrLComp(ADDEString,'[MO1]',5) = 0)
                             or (StrLComp(ADDEString,'[MO2]',5) = 0) or (StrLComp(ADDEString,'[MO3]',5) = 0)
                             or (StrLComp(ADDEString,'[MO4]',5) = 0) or (StrLComp(ADDEString,'[MO5]',5) = 0)
                             or (StrLComp(ADDEString,'[MO6]',5) = 0) or (StrLComp(ADDEString,'[MO7]',5) = 0)
                             or (StrLComp(ADDEString,'[MO8]',5) = 0) or (StrLComp(ADDEString,'[MO9]',5) = 0)
                             or (StrLComp(ADDEString,'[M00]',5) = 0) or (StrLComp(ADDEString,'[M01]',5) = 0)
                             or (StrLComp(ADDEString,'[M02]',5) = 0) or (StrLComp(ADDEString,'[M03]',5) = 0)
                             or (StrLComp(ADDEString,'[M04]',5) = 0) or (StrLComp(ADDEString,'[M05]',5) = 0)
                             or (StrLComp(ADDEString,'[M06]',5) = 0) or (StrLComp(ADDEString,'[M07]',5) = 0)
                             or (StrLComp(ADDEString,'[M08]',5) = 0) or (StrLComp(ADDEString,'[M09]',5) = 0) then begin
                            StrCopy(ResStr,'[');
                            ReadDDECommandPChar(ADDEString,0,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            StrCat(ResStr,ReadStr);
                            StrDispose(ReadStr);
                            StrCat(ResStr,']');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[MOV]',5) = 0 then begin
                            StrCopy(ResStr,'[MOV]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[NGS]',5) = 0 then begin
                            StrCopy(ResStr,'[NGS]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[NHA]',5) = 0 then begin
                            StrCopy(ResStr,'[NHA]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[NIA]',5) = 0 then begin
                            StrCopy(ResStr,'[NIA]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[NTX]',5) = 0 then begin
                            StrCopy(ResStr,'[NTX][');
                            ReadDDECommandPChar(ADDEString,2,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            StrCat(ResStr,ReadStr);
                            StrCat(ResStr,']');
                            StrDispose(ReadStr);
                          end
                          else if StrLComp(ADDEString,'[ONE]',5) = 0 then begin
                            StrCopy(ResStr,'[ONE]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[SPD]',5) = 0 then begin
                            StrCopy(ResStr,'[SPD]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else StrCopy(ResStr,ADDEString);
                        end;

{*****************************************************************************************************************************}

      TrLIIndexToNone : begin
                          if StrLComp(ADDEString,'[AIN]',5) = 0 then begin
                            StrCopy(ResStr,'[AIN]');
                            for i:=1 to 5 do begin
                              ReadDDECommandPChar(ADDEString,i,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                              StrCat(ResStr,'[');
                              StrCat(ResStr,ReadStr);
                              StrCat(ResStr,']');
                              StrDispose(ReadStr);
                            end;
                            ReadDDECommandPChar(ADDEString,6,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCat(ResStr,'[');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                            ReadDDECommandPChar(ADDEString,7,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            if ReadStr <> NIL then begin
                              StrCat(ResStr,'[');
                              StrCat(ResStr,ReadStr);
                              StrCat(ResStr,']');
                              StrDispose(ReadStr);
                            end;
                          end
                          else if StrLComp(ADDEString,'[ALA]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            if ReadStr <> NIL then begin
                              Val(ReadStr,ALIndex,Error);
                              StrDispose(ReadStr);
                              if Error = 0 then begin
                                ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                                if ALayer <> NIL then begin
                                  StrPCopy(TempStr,ALayer^.Text^);
                                  StrCopy(ResStr,'[ALA][');
                                  StrCat(ResStr,TempStr);
                                  StrCat(ResStr,']');
                                end
                                else StrCopy(ResStr,ADDEString);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,'[ALA][]');
                          end
                          else if StrLComp(ADDEString,'[CPY]',5) = 0 then begin
                            StrCopy(ResStr,'[CPY]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[DEL]',5) = 0 then begin
                            StrCopy(ResStr,'[DEL]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[EIN]',5) = 0 then begin
                            StrCopy(ResStr,'[EIN]');
                            for i:=1 to 5 do begin
                              ReadDDECommandPChar(ADDEString,i,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                              StrCat(ResStr,'[');
                              StrCat(ResStr,ReadStr);
                              StrCat(ResStr,']');
                              StrDispose(ReadStr);
                            end;
                            ReadDDECommandPChar(ADDEString,6,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCat(ResStr,'[');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                            ReadDDECommandPChar(ADDEString,7,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            if ReadStr <> NIL then begin
                              StrCat(ResStr,'[');
                              StrCat(ResStr,ReadStr);
                              StrCat(ResStr,']');
                              StrDispose(ReadStr);
                            end;
                          end
                          else if StrLComp(ADDEString,'[END]',5) = 0 then begin
                            if LIFMode then begin
                              LIFMode:=FALSE;
                              StrCopy(ResStr,'');
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[EXP]',5) = 0 then begin
                            StrCopy(ResStr,'[EXP]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[GDI]',5) = 0 then begin
                            StrCopy(ResStr,'[GDI]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[GDT]',5) = 0 then begin
                            StrCopy(ResStr,'[GDT]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[GRD]',5) = 0 then begin
                            StrCopy(ResStr,'[GRD]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[IMP]',5) = 0 then begin
                            StrCopy(ResStr,'[IMP]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[LEX]',5) = 0 then begin
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCopy(ResStr,'[LEX][');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                                ReadDDECommandPChar(ADDEString,2,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                                if StrComp(ReadStr,'1') = 0 then StrCat(ResStr,'[1]')
                                else StrCat(ResStr,'[0]');
                                StrDispose(ReadStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[LIF]',5) = 0 then begin
                            LIFMode:=TRUE;
                            StrCopy(ResStr,'');
                          end
                          else if StrLComp(ADDEString,'[MON]',5) = 0 then begin
                            StrCopy(ResStr,'[MON]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if (StrLComp(ADDEString,'[MO0]',5) = 0) or (StrLComp(ADDEString,'[MO1]',5) = 0)
                             or (StrLComp(ADDEString,'[MO2]',5) = 0) or (StrLComp(ADDEString,'[MO3]',5) = 0)
                             or (StrLComp(ADDEString,'[MO4]',5) = 0) or (StrLComp(ADDEString,'[MO5]',5) = 0)
                             or (StrLComp(ADDEString,'[MO6]',5) = 0) or (StrLComp(ADDEString,'[MO7]',5) = 0)
                             or (StrLComp(ADDEString,'[MO8]',5) = 0) or (StrLComp(ADDEString,'[MO9]',5) = 0)
                             or (StrLComp(ADDEString,'[M00]',5) = 0) or (StrLComp(ADDEString,'[M01]',5) = 0)
                             or (StrLComp(ADDEString,'[M02]',5) = 0) or (StrLComp(ADDEString,'[M03]',5) = 0)
                             or (StrLComp(ADDEString,'[M04]',5) = 0) or (StrLComp(ADDEString,'[M05]',5) = 0)
                             or (StrLComp(ADDEString,'[M06]',5) = 0) or (StrLComp(ADDEString,'[M07]',5) = 0)
                             or (StrLComp(ADDEString,'[M08]',5) = 0) or (StrLComp(ADDEString,'[M09]',5) = 0) then begin
                            StrCopy(ResStr,'[');
                            ReadDDECommandPChar(ADDEString,0,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            StrCat(ResStr,ReadStr);
                            StrDispose(ReadStr);
                            StrCat(ResStr,']');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[MOV]',5) = 0 then begin
                            StrCopy(ResStr,'[MOV]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[NGS]',5) = 0 then begin
                            StrCopy(ResStr,'[NGS]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[NHA]',5) = 0 then begin
                            StrCopy(ResStr,'[NHA]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[NIA]',5) = 0 then begin
                            StrCopy(ResStr,'[NIA]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[NTX]',5) = 0 then begin
                            StrCopy(ResStr,'[NTX][');
                            ReadDDECommandPChar(ADDEString,2,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            StrCat(ResStr,ReadStr);
                            StrCat(ResStr,']');
                            StrDispose(ReadStr);
                          end
                          else if StrLComp(ADDEString,'[ONE]',5) = 0 then begin
                            StrCopy(ResStr,'[ONE]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[SEL]',5) = 0 then begin
                            StrCopy(ResStr,'[SEL][');
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,FOutSepSign,WgBlockEnd,ReadStr,MoreEntries);
                            StrCat(ResStr,ReadStr);
                            StrDispose(ReadStr);
                            ReadDDECommandPChar(ADDEString,1,1,WgBlockStart,FOutSepSign,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,FOutSepSign);
                                StrCat(ResStr,TempStr);
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCat(ResStr,TempStr);
                                RemainStr:=StrScan(ADDEString+1,FOutSepSign);
                                RemainStr:=StrScan(RemainStr+1,FOutSepSign);
                                StrCat(ResStr,RemainStr);
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                          end
                          else if StrLComp(ADDEString,'[PPR]',5) = 0 then begin
                            StrCopy(ResStr,'[PPR][');
                            ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCat(ResStr,TempStr);
                              end
                              else StrCat(ResStr,ReadStr);
                            end;
                            StrCat(ResStr,'][');
                            StrDispose(ReadStr);
                            ReadDDECommandPChar(ADDEString,2,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            StrCat(ResStr,ReadStr);
                            StrCat(ResStr,'][');
                            ReadDDECommandPChar(ADDEString,3,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCat(ResStr,TempStr);
                              end
                              else StrCat(ResStr,ReadStr);
                            end;
                            StrCat(ResStr,']');
                            StrDispose(ReadStr);
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[SPD]',5) = 0 then begin
                            StrCopy(ResStr,'[SPD]');
                            RemainStr:=StrScan(ADDEString+1,'[');
                            RemainStr:=StrScan(RemainStr+1,'[');
                            StrCat(ResStr,RemainStr);
                          end
                          else if StrLComp(ADDEString,'[UPD]',5) = 0 then begin
                            StrCopy(ResStr,'[UPD]');
                            for i:=1 to 5 do begin
                              ReadDDECommandPChar(ADDEString,i,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                              StrCat(ResStr,'[');
                              StrCat(ResStr,ReadStr);
                              StrCat(ResStr,']');
                              StrDispose(ReadStr);
                            end;
                            ReadDDECommandPChar(ADDEString,6,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            Val(ReadStr,ALIndex,Error);
                            StrDispose(ReadStr);
                            if Error = 0 then begin
                              ALayer:=TMDIChild(ActualChild).Data^.Layers^.IndexToLayer(ALIndex);
                              if ALayer <> NIL then begin
                                StrPCopy(TempStr,ALayer^.Text^);
                                StrCat(ResStr,'[');
                                StrCat(ResStr,TempStr);
                                StrCat(ResStr,']');
                              end
                              else StrCopy(ResStr,ADDEString);
                            end
                            else StrCopy(ResStr,ADDEString);
                            ReadDDECommandPChar(ADDEString,7,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
                            if ReadStr <> NIL then begin
                              StrCat(ResStr,'[');
                              StrCat(ResStr,ReadStr);
                              StrCat(ResStr,']');
                              StrDispose(ReadStr);
                            end;
                          end
                          else StrCopy(ResStr,ADDEString);
                        end;
    end;
    Result:=StrNew(ResStr);
    *)
  end;

{**************************************** TranslateStringDesma ****************************************}

Function TDDEHandler.TranslateOutStringDesma
   (
   ADDEString      : PChar
   )
   : PChar;
  var ResStr       : Array[0..4096] of Char;
      TempStr      : Array[0..255] of Char;
      ReadStr      : PChar;
      MoreEntries  : Boolean;
      IDs          : Integer;
      Error        : Integer;
      i            : Integer;
      LastItem     : Boolean;
      FreeProgMon  : Boolean;
  begin
    (*
    FreeProgMon:=FALSE;
    if StrLComp(ADDEString,'[GRD]',5) = 0 then begin
      StrCopy(ResStr,'[SCHACHTDATA(');
      ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
      Val(ReadStr,IDs,Error);
      StrDispose(ReadStr);
      for i:=1 to IDs do begin
        if i > 1 then StrCopy(TempStr,',')
        else StrCopy(TempStr,'');
        ReadDDECommandPChar(ADDEString,i+1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
        StrCat(TempStr,ReadStr);
        StrCat(ResStr,TempStr);
        StrDispose(ReadStr);
      end;
      StrCat(ResStr,')]');
    end
    else if StrComp(ADDEString,'[MAB][]') = 0 then StrCopy(ResStr,'[BEGINMACRO()]')
    else if StrComp(ADDEString,'[MAE][]') = 0 then StrCopy(ResStr,'[ENDMACRO()]')
    else if StrLComp(ADDEString,'[MON]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT(');
      ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
      Val(ReadStr,IDs,Error);
      StrDispose(ReadStr);
      for i:=1 to IDs do begin
        if i > 1 then StrCopy(TempStr,',')
        else StrCopy(TempStr,'');
        ReadDDECommandPChar(ADDEString,i+1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
        StrCat(TempStr,ReadStr);
        StrCat(ResStr,TempStr);
        StrDispose(ReadStr);
      end;
      StrCat(ResStr,')]');
    end
    else if StrLComp(ADDEString,'[MO0]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT00(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[MO1]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT01(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[MO2]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT02(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[MO3]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT03(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[MO4]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT04(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[MO5]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT05(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[MO6]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT06(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[MO7]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT07(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[MO8]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT08(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[MO9]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT09(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[M00]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT10(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[M01]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT11(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[M02]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT12(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[M03]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT13(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[M04]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT14(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[M05]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT15(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[M06]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT16(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[M07]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT17(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[M08]',5) = 0 then begin
      StrCopy(ResStr,'[SELECT18(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[M09]',5) = 0 then begin
      StrCopy(ResStr,'[SHOWAKTID(');
      FreeProgMon:=TRUE;
    end
    else if StrLComp(ADDEString,'[NGS]',5) = 0 then begin
      StrCopy(ResStr,'[INSGRSDATA(');
      ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
      StrCat(ResStr,ReadStr);
      StrDispose(ReadStr);
      ReadDDECommandPChar(ADDEString,2,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
      StrCopy(TempStr,',');
      StrCat(TempStr,ReadStr);
      StrDispose(ReadStr);
      StrCat(ResStr,TempStr);
      ReadDDECommandPChar(ADDEString,3,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
      StrCopy(TempStr,',');
      StrCat(TempStr,ReadStr);
      StrDispose(ReadStr);
      StrCat(ResStr,TempStr);
      StrCat(ResStr,')]');
    end
    else if StrLComp(ADDEString,'[NHA]',5) = 0 then begin
      StrCopy(ResStr,'[INSHALTDATA(');
      ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
      StrCat(ResStr,ReadStr);
      StrDispose(ReadStr);
      ReadDDECommandPChar(ADDEString,2,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
      StrCopy(TempStr,',');
      StrCat(TempStr,ReadStr);
      StrDispose(ReadStr);
      StrCat(ResStr,TempStr);
      i:=3;
      while ReadDDECommandPChar(ADDEString,i,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries) do begin
        StrCopy(TempStr,',');
        StrCat(TempStr,ReadStr);
        StrDispose(ReadStr);
        StrCat(ResStr,TempStr);
        inc(i);
      end;
      StrCat(ResStr,')]');
    end
    else StrCopy(ResStr,'');
    if FreeProgMon then begin
      ReadDDECommandPChar(ADDEString,1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
      Val(ReadStr,IDs,Error);
      StrDispose(ReadStr);
      for i:=1 to IDs do begin
        if i > 1 then StrCopy(TempStr,',')
        else StrCopy(TempStr,'');
        ReadDDECommandPChar(ADDEString,i+1,0,WgBlockStart,WgBlockEnd,WgBlockEnd,ReadStr,MoreEntries);
        StrCat(TempStr,ReadStr);
        StrCat(ResStr,TempStr);
        StrDispose(ReadStr);
      end;
      StrCat(ResStr,')]');
    end;
    Result:=StrNew(ResStr);
    *)
  end;

{**************************************** TranslateInStringDesma ****************************************}

Function TDDEHandler.TranslateInStringDesma
   (
   ADDEString      : PChar;
   var EndMode     : Boolean
   )
   : PChar;
  var ResStr       : Array[0..4096] of Char;
      TempStr      : Array[0..255] of Char;
      ReadStr      : PChar;
      MoreEntries  : Boolean;
      i            : Integer;
      f            : Integer;
      StrOK        : Boolean;
      AReal        : Real;
      ALongInt     : LongInt;
      AInteger     : Integer;
      Error        : Integer;
  begin
    (*
    if StrComp(ADDEString,'[BEGINMACRO()]') = 0 then StrCopy(ResStr,'[MCB][]')
    else if StrComp(ADDEString,'[BEGINSELECT()]') = 0 then StrCopy(ResStr,'[SMB][]')
    else if StrComp(ADDEString,'[DATASTART()]') = 0 then DataMode:=TRUE
    else if StrComp(ADDEString,'[ENDMACRO()]') = 0 then StrCopy(ResStr,'[MCE][]')
    else if StrComp(ADDEString,'[ENDSELECT()]') = 0 then StrCopy(ResStr,'[SME][]')
    else if StrLComp(ADDEString,'[OPEN(',6) = 0 then begin
      StrCopy(ResStr,'[OPR][');
      ReadDDECommandPChar(ADDEString,0,0,DmBlockStart,DmBlockEnd,DmBlockEnd,ReadStr,MoreEntries);
      if ReadStr <> NIL then begin
        StrCat(ResStr,ReadStr);
        StrCat(ResStr,']');
        StrDispose(ReadStr);
      end
      else StrCat(ResStr,']');
    end
    else if StrComp(ADDEString,'[DESELALL()]') = 0 then StrCopy(ResStr,'[DSA][]')
    else if StrLComp(ADDEString,'[SELECT(',8) = 0 then begin
      StrCopy(ResStr,'[ZOM][   ]');
      i:=0;
      f:=0;
      MoreEntries:=TRUE;
      while MoreEntries do begin
        StrOK:=ReadDDECommandPChar(ADDEString,0,i,DmBlockStart,FDDESepSign,DmBlockEnd,ReadStr,MoreEntries);
        if ReadStr <> NIL then begin
          StrCopy(TempStr,'[');
          StrLCat(TempStr,'          ',11-StrLen(ReadStr));
          StrCat(TempStr,ReadStr);
          StrCat(TempStr,']');
          StrCat(ResStr,TempStr);
          StrDispose(ReadStr);
        end
        else Inc(f);
        Inc(i);
      end;
      Str(i-f:3,TempStr);
      ResStr[6]:=TempStr[0];
      ResStr[7]:=TempStr[1];
      ResStr[8]:=TempStr[2];
      EndMode:=TRUE;
    end
    else if StrLComp(ADDEString,'[DSASELECT(',11) = 0 then begin
      StrCopy(ResStr,'[ZOD][   ]');
      i:=0;
      f:=0;
      MoreEntries:=TRUE;
      while MoreEntries do begin
        StrOK:=ReadDDECommandPChar(ADDEString,0,i,DmBlockStart,FDDESepSign,DmBlockEnd,ReadStr,MoreEntries);
        if ReadStr <> NIL then begin
          StrCopy(TempStr,'[');
          StrLCat(TempStr,'          ',11-StrLen(ReadStr));
          StrCat(TempStr,ReadStr);
          StrCat(TempStr,']');
          StrCat(ResStr,TempStr);
          StrDispose(ReadStr);
        end
        else Inc(f);
        Inc(i);
      end;
      Str(i-f:3,TempStr);
      ResStr[6]:=TempStr[0];
      ResStr[7]:=TempStr[1];
      ResStr[8]:=TempStr[2];
      EndMode:=TRUE;
    end
    else if StrLComp(ADDEString,'[SELOFFSET(',11) = 0 then begin
      ReadDDECommandPChar(ADDEString,0,0,DmBlockStart,FDDESepSign,DmBlockEnd,ReadStr,MoreEntries);       {Selektionsoffset}
      if ReadStr <> NIL then begin
        if (StrComp(ReadStr,'') = 0) then AReal:=0
        else begin
          Val(ReadStr,AReal,Error);
          if (Error <> 0) or (AReal > MaxLongInt/100) or (AReal < -MaxLongInt/100) then AReal:=0;
        end;
        StrDispose(ReadStr);
      end
      else AReal:=0;
      ReadDDECommandPChar(ADDEString,0,1,DmBlockStart,FDDESepSign,DmBlockEnd,ReadStr,MoreEntries);                  {Ecken}
      if ReadStr <> NIL then begin
        if (StrComp(ReadStr,'') = 0) then AInteger:=0
        else begin
          Val(ReadStr,AInteger,Error);
          if (Error <> 0) or (AInteger > 1) or (AInteger < 0) then AInteger:=0;
        end;
        StrDispose(ReadStr);
      end
      else AInteger:=0;
      ReadDDECommandPChar(ADDEString,0,2,DmBlockStart,FDDESepSign,DmBlockEnd,ReadStr,MoreEntries);       {Selektionsobjekt}
      if ReadStr <> NIL then begin
        if (StrComp(ReadStr,'') = 0) then ALongInt:=0
        else begin
          Val(ReadStr,ALongInt,Error);
          if (Error <> 0) or (ALongInt > MaxLongInt) or (ALongInt < 0) then ALongInt:=0;
        end;
        StrDispose(ReadStr);
      end
      else ALongInt:=0;
      if AReal = 0 then StrCopy(ResStr,'[SEL][6,')                                             {Selektion mit einer Fl�che}
      else StrCopy(ResStr,'[SEL][5,');                                                               {Selektion mit Offset}
      if ALongInt = 0 then StrCat(ResStr,'1,,')                                                   {Selektionsobjekt w�hlen}
      else begin
        StrCat(ResStr,'4,');                                                                   {Selektionsobjekt �bergeben}
        Str(ALongInt:10,TempStr);
        StrCat(ResStr,TempStr);
        StrCat(ResStr,',');
      end;
      StrCat(ResStr,',0,4][][0,');
      Str(AReal:0:2,TempStr);
      StrCat(ResStr,TempStr);                                                                                      {Offset}
      StrCat(ResStr,',');
      if AInteger = 1 then StrCat(ResStr,'0,0][]')                                                                  {Ecken}
      else StrCat(ResStr,'1,0][]');
      ReadDDECommandPChar(ADDEString,0,3,DmBlockStart,FDDESepSign,DmBlockEnd,ReadStr,MoreEntries);        {Ergebnis zur�ck}
      if ReadStr <> NIL then begin
        if (StrComp(ReadStr,'') = 0) then AInteger:=0
        else begin
          Val(ReadStr,AInteger,Error);
          if (Error <> 0) or (AInteger > 1) or (AInteger < 0) then AInteger:=0;
        end;
        StrDispose(ReadStr);
      end
      else AInteger:=0;
      if AInteger = 1 then AReal:=1
      else AReal:=0;
      ReadDDECommandPChar(ADDEString,0,4,DmBlockStart,FDDESepSign,DmBlockEnd,ReadStr,MoreEntries);    {Selgrenzen zeichnen}
      if ReadStr <> NIL then begin
        if (StrComp(ReadStr,'') = 0) then AInteger:=0
        else begin
          Val(ReadStr,AInteger,Error);
          if (Error <> 0) or (AInteger > 1) or (AInteger < 0) then AInteger:=0;
        end;
        StrDispose(ReadStr);
      end
      else AInteger:=0;
      if AInteger = 1 then begin
        if ActualChild <> NIL then begin
          StrCat(ResStr,'[0,');
          StrPCopy(TempStr,TMDIChild(ActualChild).Data^.Layers^.TopLayer^.Text^);
          StrCat(ResStr,TempStr);
          StrCat(ResStr,'][1,');
        end
        else StrCat(ResStr,'[0,][1,');
      end
      else StrCat(ResStr,'[0,][1,');
      if AReal = 1 then StrCat(ResStr,'1][1]')
      else StrCat(ResStr,'0][1]');
    end
    else if StrLComp(ADDEString,'[SETACTIVELAYER(',16) = 0 then begin
      StrCopy(ResStr,'[SAL][2,');
      ReadDDECommandPChar(ADDEString,0,0,DmBlockStart,DmBlockEnd,DmBlockEnd,ReadStr,MoreEntries);
      if ReadStr <> NIL then begin
        StrCat(ResStr,ReadStr);
        StrCat(ResStr,']');
        StrDispose(ReadStr);
      end
      else StrCat(ResStr,']');
    end
    else if StrLComp(ADDEString,'[SCHACHTDATA(',13) = 0 then begin
      StrCopy(ResStr,'[DGR][01,');
      ReadDDECommandPChar(ADDEString,0,6,DmBlockStart,FDDESepSign,DmBlockEnd,ReadStr,MoreEntries);
      if ReadStr <> NIL then begin
        StrCat(ResStr,ReadStr);
        StrCat(ResStr,',');
        StrDispose(ReadStr);
      end
      else StrCat(ResStr,',');
      ReadDDECommandPChar(ADDEString,0,2,DmBlockStart,FDDESepSign,DmBlockEnd,ReadStr,MoreEntries);
      if ReadStr <> NIL then begin
        StrCat(ResStr,ReadStr);
        StrCat(ResStr,',');
        StrDispose(ReadStr);
      end
      else StrCat(ResStr,',');
      ReadDDECommandPChar(ADDEString,0,3,DmBlockStart,FDDESepSign,DmBlockEnd,ReadStr,MoreEntries);
      if ReadStr <> NIL then begin
        StrCat(ResStr,ReadStr);
        StrCat(ResStr,',');
        StrDispose(ReadStr);
      end
      else StrCat(ResStr,',');
      ReadDDECommandPChar(ADDEString,0,0,DmBlockStart,FDDESepSign,DmBlockEnd,ReadStr,MoreEntries);
      if ReadStr <> NIL then begin
        StrCat(ResStr,ReadStr);
        StrDispose(ReadStr);
      end;
      ReadDDECommandPChar(ADDEString,0,5,DmBlockStart,FDDESepSign,DmBlockEnd,ReadStr,MoreEntries);
      if ReadStr <> NIL then begin
        if StrComp(ReadStr,'') = 0 then begin
          StrCat(ResStr,',');
          StrCat(ResStr,ReadStr);
        end;
        StrDispose(ReadStr);
      end;
      StrCat(ResStr,']');
      EndMode:=TRUE;
    end
    else if StrLComp(ADDEString,'[NEWDDEAPP(',11) = 0 then begin        {?!? wie STARTAPP ?!?}
      ReadDDECommandPChar(ADDEString,0,0,DmBlockStart,FDDESepSign,DmBlockEnd,ReadStr,MoreEntries);
      if ReadStr <> NIL then begin
        StrCopy(SSAppFName,ReadStr);
        StrDispose(ReadStr);
      end
      else StrCopy(SSAppFName,'');
      ReadDDECommandPChar(ADDEString,0,1,DmBlockStart,FDDESepSign,DmBlockEnd,ReadStr,MoreEntries);
      if ReadStr <> NIL then begin
        StrCopy(SSCService,ReadStr);
        StrDispose(ReadStr);
      end
      else StrCopy(SSCService,'');
      ReadDDECommandPChar(ADDEString,0,2,DmBlockStart,FDDESepSign,DmBlockEnd,ReadStr,MoreEntries);
      if ReadStr <> NIL then begin
        StrCopy(SSCTopic,ReadStr);
        StrDispose(ReadStr);
      end
      else StrCopy(SSCTopic,'');
      ReadDDECommandPChar(ADDEString,0,3,DmBlockStart,FDDESepSign,DmBlockEnd,ReadStr,MoreEntries);
      if ReadStr <> NIL then begin
        StrCopy(SSCItem,ReadStr);
        StrDispose(ReadStr);
      end
      else StrCopy(SSCItem,'');
      ConnectAndActivateApp(TRUE);
      StrCopy(ResStr,'');
    end
    else StrCopy(ResStr,ADDEString);
    Result:=StrNew(ResStr);
    *)
  end;

{$IFDEF WIN32}
Const RDBSetting   : TStreamRec = (
			ObjType : rn_DBSetting;
			VmtLink : TypeOf(TDBSetting);
			Load    : @TDBSetting.Load;
			Store   : @TDBSetting.Store);

      RDBSettings  : TStreamRec = (
			ObjType : rn_DBSettings;
			VmtLink : TypeOf(TDBSettings);
			Load    : @TDBSettings.Load;
			Store   : @TDBSettings.Store);
{$ELSE}
Const RDBSetting   : TStreamRec = (
			ObjType : rn_DBSetting;
			VmtLink : Ofs(TypeOf(TDBSetting)^);
			Load    : @TDBSetting.Load;
			Store   : @TDBSetting.Store);

      RDBSettings  : TStreamRec = (
			ObjType : rn_DBSettings;
			VmtLink : Ofs(TypeOf(TDBSettings)^);
			Load    : @TDBSettings.Load;
			Store   : @TDBSettings.Store);
{$ENDIF}

begin
  RegisterType(RDBSetting);
  RegisterType(RDBSettings);
end.

