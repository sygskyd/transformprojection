
{*******************************************************}
{                                                       }
{       Turbo Pascal Runtime Library                    }
{       Windows DOS Interface Unit                      }
{                                                       }
{       Copyright (c) 1991,92 Borland International     }
{                                                       }
{*******************************************************}

unit WinDos;

Interface

{ Maximum file name component string lengths }

const
  fsPathName  = 254;
  fsDirectory = 254;
  fsFileName  = 254;
  fsExtension = 254;

function FileExpand(Dest, Name: PChar): PChar;

function FileSplit(Path, Dir, Name, Ext: PChar): Word;

function GetCurDir(Dir: PChar; Drive: Byte): PChar; 

Implementation

Uses SysUtils;

function FileExpand(Dest, Name: PChar): PChar;
  begin
    StrPCopy(Dest,ExpandFileName(StrPas(Name)));
    Result:=Dest;
  end;

function FileSplit(Path,Dir,Name,Ext:PChar):Word;
  begin
    StrPCopy(Dir,ExtractFilePath(StrPas(Path)));
    StrPCopy(Name,ChangeFileExt(ExtractFileName(StrPas(Path)),''));
    StrPCopy(Ext,ExtractFileExt(StrPas(Path)));
    Result:=0;
  end;

function GetCurDir(Dir: PChar; Drive: Byte): PChar;
  begin
    Result:=StrPCopy(Dir,GetCurrentDir);
  end;

end.
