{******************************************************************************
Funktion:   Sammlung von Funktionen f�r die Verarbeitung von Registry-
            Eintragungen

Autor:      ml
Datum       23.5.97

Geteste:    ml

Referenzen: Nur Standard-Units

Copyright:  Diese Biblithek wurde nicht im Rahmen einer Auftragsarbeit
            erstellt. Das Urherberrecht an diesem Modul wird, selbst
            bei Verwendung in einem Programm eines Auftraggebers nicht
            an diesen weitergegeben. Sofern nicht anders vereinbart, darf
            der Auftraggeber diese Bibliothek aber unentgeltlich in allen
            Modulen verwenden, die mit dem Auftrag an die eMEDES
            in Zusammenhang stehen. Der Auftraggeber darf auch �nderungen
            an diesem Modul vornehmen mu� diese allerdings Kennzeichnen.
            In KEINEM FALL darf dieser Copyright-Hinweis ver�ndert oder
            entfernt werden und in KEINEM FALL darf diese Bibliothek an
            Dritte weitergegeben werden.

            Copyright (c) 1997 by eMEDES Software
            Alexander Schupp, Laussegger Michael

History:    04.10.1998        function GetRegNodeNext added by AS
******************************************************************************}

unit RegUtils;

interface

Uses Windows, SysUtils {$IFDEF DebugLib} ,DebugWin {$ENDIF}, Dialogs;

//Function Prototypes
function GetRegString(RegKey, RegName, std: string) : string;
function SetRegString(RegKeyName, RegValName, RegVal: String): Boolean;
function DelRegString(RegKey, RegName: string) : boolean;
function GetUserRegString(RegKey, RegName, std: string) : string;
function SetUserRegString(RegKeyName, RegValName, RegVal: String): Boolean;
function GetSubRegString(ParentKey: Integer; RegKey, RegName, std: string) : string;
function SetSubRegString(ParentKey: HKEY; RegKeyName, RegValName,
                         RegVal: String): Boolean;
function DelSubRegString(ParentKey: Integer; RegKey, RegName: string) : boolean;                         
function GetRegNodeNext(hParentKey: HKEY; sKey: string; i: integer): string;
function SaveRegKey(ParentKey: Integer; RegKey, sFile: string) : boolean;

procedure DisplaySysError;

implementation


{__________________________________________________________________ GetRegString
}
function GetRegString(RegKey, RegName, std: string) : string;
begin
  Result:=GetSubRegString(HKEY_LOCAL_MACHINE, RegKey, RegName, std);
end;



{__________________________________________________________________ SetRegString
Funktion: Schreibt einen Wert vom Typ String in die Registry. Liefert
          True wenn alles gklappt hat, sonst False;
}
function SetRegString(RegKeyName, RegValName, RegVal: String): Boolean;
begin

     Result := SetSubRegString(HKEY_LOCAL_MACHINE,
                               RegKeyName,
                               RegValName,
                               RegVal);
end;

{__________________________________________________________________ DelRegString
}
function DelRegString(RegKey, RegName: string) : boolean;
begin
  Result := DelSubRegString(HKEY_LOCAL_MACHINE, RegKey, RegName);
end;



{______________________________________________________________ GetUserRegString
Funktion: Liest einen Wert vom Typ String aus der Regtistry. Allerding wird der
  Wert aus dem Ast CURRENT_USER geholt statt aus LOCAL_MACHINE.
}

function GetUserRegString(RegKey, RegName, std: string) : string;
begin
     Result:=GetSubRegString(HKEY_CURRENT_USER, RegKey, RegName, std);
end;


{______________________________________________________________ SetUserRegString
}
function SetUserRegString(RegKeyName, RegValName, RegVal: String): Boolean;
begin
     Result := SetSubRegString(HKEY_CURRENT_USER,
                               RegKeyName,
                               RegValName,
                               RegVal);

end;


{_______________________________________________________________ SetSubRegString
Funktion: Schreibt einen Wert vom Typ String in die Registry. Liefert
          True wenn alles gklappt hat, sonst False}

function SetSubRegString(ParentKey: HKEY; RegKeyName, RegValName,
                         RegVal: String): Boolean;

var hkKey: HKEY;
    lpdwDisposition: LPDWORD;
    pRegKeyName, pRegValName, pRegVal: PChar;
begin
     Result := False;

     {$IFDEF DebugLib}
     Debug.Print(IntToStr(ParentKey)+':'+RegKeyName+':'+RegValName+':'+RegVal+':FIN');
     {$ENDIF}

     GetMem(pRegKeyName,length(RegKeyName) +1);StrPCopy(pRegKeyName,RegKeyName);
     GetMem(pRegValName,length(RegValName) +1);StrPCopy(pRegValName,RegValName);
     GetMem(pRegVal,length(RegVal) +1);StrPCopy(pRegVal,RegVal);

     if RegCreateKeyEx(ParentKey, pRegKeyName , 0, 'EMML',
        REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, nil, hkKey, @lpdwDisposition)
         = ERROR_SUCCESS then begin

        if RegSetValueEx(hkKey, pRegValName, 0, REG_SZ, pRegVal,
           Length(RegVal)+1) = ERROR_SUCCESS then
             Result := True;


        {$IFDEF DebugLib}
        if lpdwDisposition = PDWORD(REG_CREATED_NEW_KEY) then
           Debug.Print('SetSubRegString\CREATE: ' + RegKeyName)
        else
           Debug.Print('SetSubRegString\OPEN: ' + RegKeyName);
        {$ENDIF}

        RegCloseKey(hkKey);

     end;

     {$IFDEF DebugLib}
     if Result then Debug.Print('OK') else Debug.Print('NICHT OK');
     {$ENDIF}

     FreeMem(pRegKeyName);
     FreeMem(pRegValName);
     FreeMem(pRegVal);

end;

{______________________________________________________________ GetUserRegString
Funktion: Liest einen Wert vom Typ String aus der Regtistry. ParentKey bestimmt
  aus welchem Ast der Reg die Werte gelesen werden.
}

function GetSubRegString(ParentKey: Integer; RegKey, RegName, std: string) : string;
var hParKey,hkKey: HKEY;
    sBackend: array[0..255] of Char;
    acValue: array[0..255] of Char;
    lBytes,lErr: longint;
    pcBuf: PChar;
    pcValue: PChar;
begin

     hParKey:=HKEY(ParentKey);

     Result := std;
     if RegOpenKeyEx(hParKey
                   , PChar(RegKey)
                   , 0, KEY_QUERY_VALUE, hkKey) = NO_ERROR then begin

        {hKeyUp := hKey;}
        lBytes := 254;
        pcBuf := sBackend;          {Pointer to Result Buf.}
        pcValue := acValue;         {Pointer to Schl�ssel Buf.}
        StrPCopy(pcValue, RegName); {Copy Schl�ssel to Buf.}
        lErr := RegQueryValueEx(hkKey, pcValue, nil, nil, PBYTE(pcBuf), @lBytes);
        if lErr = NO_ERROR then
           if StrLen(sBackend) > 0 then Result := sBackend;

        RegCloseKey(hkKey)
     end else begin
        Result:=std;
     end;
end;

{______________________________________________________________ DelUserRegString
Funktion: L�scht einen Wert aus der Regtistry. ParentKey bestimmt
  aus welchem Ast der Reg der Wert gel�scht wird.
}
function DelSubRegString(ParentKey: Integer; RegKey, RegName: string) : boolean;
var hkKey: HKEY;
    acValue: array[0..255] of Char;
    lErr: longint;
    pcValue: PChar;
begin
     Result := false;
     if RegOpenKeyEx(ParentKey
                   , PChar(RegKey)
                   , 0, KEY_SET_VALUE, hkKey) = NO_ERROR then begin


        pcValue := acValue;         {Pointer to Schl�ssel Buf.}
        StrPCopy(pcValue, RegName); {Copy Schl�ssel to Buf.}
        lErr := RegDeleteValue(hkKey, pcValue);
        if lErr = NO_ERROR then
           Result := true;

        RegCloseKey(hkKey)
     end else begin
        Result := false;
     end;
end;


{*****************************************************************************************
'    FUNCTION: GetRegNodeNext
'   PARAMETER: sKey - Registry Key to get SubNodes from, starts from HKEY_CURRENT_USER
'              i - number of subkey, starts from 0
'              sName - OUTPUT, the subnode name
'      RETURN: ERROR_SUCCESS, ERROR_NO_MORE_ITEMS or any other error code
' DESCRIPTION: this function reads all records from one datafile into the
'              global array aData.
'*****************************************************************************************}
function GetRegNodeNext(hParentKey: HKEY; sKey: string; i: integer): string;
var
    lName: DWORD; cJunk: longint; ft: TFILETIME;
    hkKey: HKEY;
    pClassName: PChar;
    lClassNameSize: DWORD;
    lNameMax: longint;
    pName: PChar;
begin
    hkKey := 0;
    Result := '';

    if RegOpenKeyEx(hParentKey, PChar(sKey), 0,
                    KEY_ENUMERATE_SUB_KEYS Or KEY_QUERY_VALUE,
                    hkKey) = NO_ERROR then begin

       if RegQueryInfoKey(hkKey, nil, nil, nil, nil,
                          @lNameMax, @lClassNameSize, nil, nil, nil, nil, @ft) = NO_ERROR then begin


          lName := lNameMax + 1;
          GetMem(pName, lName);

          lClassNameSize := lClassNameSize + 1;
          GetMem(pClassName, lClassNameSize);

          if RegEnumKeyEx(hkKey, i, pName, lName, nil, pClassName, @lClassNameSize, @ft) = NO_ERROR then
             Result := StrPas(pName);

       end;
    end;
    If hkKey <> 0 then RegCloseKey(hkKey);         // close key opened above

end;

{*****************************************************************************************
'    FUNCTION: GetRegNodeNext
'   PARAMETER: sKey - Registry Key to get SubNodes from, starts from HKEY_CURRENT_USER
'              i - number of subkey, starts from 0
'              sName - OUTPUT, the subnode name
'      RETURN: ERROR_SUCCESS, ERROR_NO_MORE_ITEMS or any other error code
' DESCRIPTION: this function reads all records from one datafile into the
'              global array aData.
'*****************************************************************************************}
function SaveRegKey(ParentKey: Integer; RegKey, sFile: string) : boolean;
var hkKey: HKEY;

begin
     Result := FALSE;
     if RegOpenKeyEx(ParentKey
                   , PChar(RegKey)
                   , 0, KEY_ALL_ACCESS, hkKey) = NO_ERROR then begin

        if RegSaveKey(hkKey, PChar(sFile), nil) = ERROR_SUCCESS then
           Result := TRUE
        else
          DisplaySysError;

        RegCloseKey(hkKey);         // close key opened above
     end;
end;

procedure DisplaySysError;
var lpMsgBuf: PChar;
begin


FormatMessage(
    FORMAT_MESSAGE_ALLOCATE_BUFFER or FORMAT_MESSAGE_FROM_SYSTEM,
    nil,
    GetLastError(),
    0, // Default language
     @lpMsgBuf,
    0,
    nil
);

// Display the string.
ShowMessage(StrPas(lpMsgBuf));
// Free the buffer.
LocalFree( Cardinal(lpMsgBuf) );
end;

end.
