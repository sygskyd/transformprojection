unit BMExpMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  WINGIS_TLB, Math, StdCtrls, OleCtrls, AxGisPro_TLB, ComCtrls, Dialogs, IniFiles,
  DB, ADODB, WinOSInfo;

type
  TGisPoint = record
        X,Y: Double;
  end;

  TGisRect = record
        Left,Top,Right,Bottom: Double;
  end;

type
  TBMExpForm = class(TForm)
    GisPro: TAxGisProjection;
    Pb: TProgressBar;
    Sb: TStaticText;
    ADOConnection: TADOConnection;
    ADOTable: TADOTable;
  public
    Doc: IDocument;
    XML: TStringList;
    MapRect: TGisRect;
    UsingDB: Boolean;
    UsingTable: Boolean;
    GisIdFieldName: AnsiString;
    GuidFieldName: AnsiString;
    XMLFilename: AnsiString;
    function EncodeText(s: AnsiString): AnsiString;
    function SetProjection: Boolean;
    function ConvertPoint(APoint: IPoint): TGisPoint; overload;
    function DoProject: Boolean;
    procedure DoLayer(ALayer: ILayer);
    procedure DoObjectStyle(ALineStyle: ILineStyle; AFillStyle: IFillStyle);
    procedure DoLineStyle(ALineStyle: ILineStyle);
    procedure DoFillStyle(AFillStyle: IFillStyle);
    procedure DoObject(ABase: IBase; ALayer: ILayer);
    procedure DoPos(APos: TGisPoint);
    procedure DoPolyline(APolyline: IPolyline);
    procedure DoPolygon(APolygon: IPolygon);
    procedure DoPixel(APixel: IPixel);
    procedure DoSymbol(ASymbol: ISymbol);
    procedure CorrectMapRect(APos: TGisPoint);
    procedure OpenDB;
    procedure CloseDB;
    procedure OpenTable(ALayer: ILayer);
    procedure CloseTable;
    procedure DoRecord(ABase: IBase);
    procedure DoField(AField: TField);
  end;

var
  BMExpForm: TBMExpForm;

const
  ot_Pixel              = 1;
  ot_Poly               = 2;
  ot_CPoly              = 4;
  ot_Text               = 7;
  ot_Symbol             = 8;
  ot_Image              = 10;
  ot_Circle             = 12;
  ot_Arc                = 13;

const
   lt_Solid     = 0;
   lt_Dash      = 1;
   lt_Dot       = 2;
   lt_DashDot   = 3;
   lt_DashDDot  = 4;

   pt_NoPattern = 0;
   pt_FDiagonal = 1;
   pt_Cross     = 2;
   pt_DiagCross = 3;
   pt_BDiagonal = 4;
   pt_Horizontal= 5;
   pt_Vertical  = 6;
   pt_Solid     = 7;

const
  SupportedObjectTypes = [ot_Poly,ot_CPoly,ot_Pixel,ot_Symbol];

const
  wtProjectUnits        = 0;
  wtScaleIndependent    = 1;
  wtUnknownUnits        = 2;

  stProjectUnits        = 0;
  stScaleIndependent    = 1;
  stDefaultSize         = 2;

  dblTolerance          = 1E-10;

const
  XMLFileExt            = '.xml';
  DBFileExt             = '.wgi';
  DBConnectionTemplate  = 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=%1;Mode=Read;Persist Security Info=False';

implementation

{$R *.dfm}

function TBMExpForm.EncodeText(s: AnsiString): AnsiString;
begin
        Result:=Utf8Encode(Trim(s));
end;

function TBMExpForm.SetProjection: Boolean;
begin
   GisPro.WorkingPath:=OSInfo.WingisDir;

   GisPro.SourceCoordinateSystem:=Doc.Projection;
   GisPro.SourceProjSettings:=Doc.ProjectProjSettings;
   GisPro.SourceProjection:=Doc.ProjectProjection;
   GisPro.SourceDate:=Doc.ProjectDate;
   GisPro.SourceXOffset:=Doc.ProjectionXOffset;
   GisPro.SourceYOffset:=Doc.ProjectionYOffset;
   GisPro.SourceScale:=Doc.ProjectionScale;

   GisPro.TargetCoordinateSystem:=0;
   GisPro.TargetProjSettings:='Geodetic Phi/Lamda';
   GisPro.TargetProjection:='Phi/Lamda values';
   GisPro.TargetDate:='WGS84';
   GisPro.TargetXOffset:=0;
   GisPro.TargetYOffset:=0;
   GisPro.TargetScale:=1;

   GisPro.ShowOnlySource:=False;
   GisPro.LngCode:='044';

   if Doc.ProjectProjection = 'NONE' then begin
        GisPro.SetupByDialog;
   end;
   Result:=GisPro.SourceProjection <> 'NONE';
end;

function TBMExpForm.ConvertPoint(APoint: IPoint): TGisPoint;
var
DX,DY: Double;
begin
        DX:=APoint.x;
        DY:=APoint.y;

        DX:=DX/100;
        DY:=DY/100;

        GisPro.Calculate(DX,DY);

        Result.X:=DX;
        Result.Y:=DY;
end;

function TBMExpForm.DoProject: Boolean;
var
i: Integer;
ALayer: ILayer;
begin
        Result:=False;
        DecimalSeparator:='.';

        if (Doc <> nil) and (XMLFilename <> '') then begin
                if SetProjection then begin

                        MapRect.Left:=180;
                        MapRect.Top:=-90;
                        MapRect.Right:=-180;
                        MapRect.Bottom:=90;

                        XML:=TStringList.Create;
                        OpenDB;
                        try
                                XML.Add('<?xml version="1.0" encoding="utf-8"?>');
                                XML.Add('<map version="1.2">');

                                for i:=Doc.Layers.Count-1 downto 0 do begin
                                        ALayer:=Doc.Layers.Items[i];
                                        if ALayer.Visible then begin
                                                DoLayer(ALayer);
                                        end;
                                end;

                                XML.Add('<mapsize left="' + FloatToStr(MapRect.Left) + '" '
                                        + 'top="' + FloatToStr(MapRect.Top) + '" '
                                        + 'right="' + FloatToStr(MapRect.Right) + '" '
                                        + 'bottom="' + FloatToStr(MapRect.Bottom) + '" />');

                                XML.Add('</map>');

                                Result:=True;
                        finally
                                XML.SaveToFile(XMLFilename);
                                XML.Free;
                                CloseDB;
                                Doc:=nil;
                        end;
                end;
        end;
end;

procedure TBMExpForm.DoLayer(ALayer: ILayer);
var
i,n,Step: Integer;
AnyObject: Boolean;
ABase: IBase;
begin
        AnyObject:=False;

        n:=ALayer.Count;
        for i:=0 to n-1 do begin
                ABase:=ALayer.Items[i];
                if ABase.ObjectType in SupportedObjectTypes then begin
                        AnyObject:=True;
                        CorrectMapRect(ConvertPoint(ABase.ClipRect.A));
                        CorrectMapRect(ConvertPoint(ABase.ClipRect.B));
                end;
        end;

        if AnyObject then begin

                Sb.Caption:=ALayer.LayerName;

                XML.Add('<layer name="' + EncodeText(ALayer.Layername + '">'));

                DoObjectStyle(ALayer.LineStyle,ALayer.FillStyle);

                OpenTable(ALayer);

                n:=ALayer.Count;

                for i:=0 to n-1 do begin

                        Step:=((i+1)*100) div n;
                        if Step <> Pb.Position then begin
                                Pb.Position:=Step;
                                Application.ProcessMessages;
                        end;

                        DoObject(ALayer.Items[i],ALayer);
                end;

                XML.Add('</layer>');

                CloseTable;
                Pb.Position:=0;
                Sb.Caption:='';
        end;
end;

procedure TBMExpForm.DoObjectStyle(ALineStyle: ILineStyle; AFillStyle: IFillStyle);
begin
        XML.Add('<objectstyle>');
        DoLineStyle(ALineStyle);
        DoFillStyle(AFillStyle);
        XML.Add('</objectstyle>');
end;

procedure TBMExpForm.DoLineStyle(ALineStyle: ILineStyle);
var
R,G,B: String;
LS,LW: String;
begin
        B:=IntToStr((ALineStyle.Color and $FF0000) shr 16);
        G:=IntToStr((ALineStyle.Color and $00FF00) shr 8);
        R:=IntToStr((ALineStyle.Color and $0000FF));

        LS:='solid';

        if ALineStyle.WidthType = wtScaleIndependent then begin
                LW:=IntToStr(Max(Round(ALineStyle.Width/10),1));
        end
        else begin
                LW:='1';
        end;

        XML.Add('<linestyle r="' + R + '" g="' + G + '" b="' + B + '" style="' + LS + '" width="' + LW + '" />');
end;

procedure TBMExpForm.DoFillStyle(AFillStyle: IFillStyle);
var
R,G,B: String;
FS: String;
begin
        B:=IntToStr((AFillStyle.ForeColor and $FF0000) shr 16);
        G:=IntToStr((AFillStyle.ForeColor and $00FF00) shr 8);
        R:=IntToStr((AFillStyle.ForeColor and $0000FF));

        if AFillStyle.Pattern = 0 then begin
                FS:='none';
        end
        else if AFillStyle.Pattern = 7 then begin
                FS:='solid';
        end
        else begin
                FS:='halftone';
        end;

        XML.Add('<fillstyle r="' + R + '" g="' + G + '" b="' + B + '" style="' + FS + '" />');
end;

procedure TBMExpForm.DoObject(ABase: IBase; ALayer: ILayer);
var
ValidObject: Boolean;
begin
        XML.Add('<object>');

        if ABase.OtherStyle then begin
                DoObjectStyle(ABase.ObjectStyle.LineStyle,ABase.ObjectStyle.FillStyle);
        end;

        ValidObject:=True;
        case ABase.ObjectType of
                ot_Pixel: DoPixel(ABase as IPixel);
                ot_Symbol: DoSymbol(ABase as ISymbol);
                ot_Poly: DoPolyline(ABase as IPolyline);
                ot_CPoly: DoPolygon(ABase as IPolygon);
        else
                ValidObject:=False;
        end;

        if (ValidObject) and (UsingDB) and (UsingTable) then begin
                DoRecord(ABase);
        end;

        XML.Add('</object>');
end;

procedure TBMExpForm.DoPos(APos: TGisPoint);
begin
        XML.Add('<pos x="' + FloatToStr(APos.X) + '" y="' + FloatToStr(APos.Y) + '" />');
end;

procedure TBMExpForm.DoPolyline(APolyline: IPolyline);
var
i,n: Integer;
begin
        XML.Add('<polyline>');
        n:=APolyline.Count;
        for i:=0 to n-1 do begin
                DoPos(ConvertPoint(APolyline.Points[i]));
        end;
        XML.Add('</polyline>');
end;

procedure TBMExpForm.DoPolygon(APolygon: IPolygon);
var
i,n: Integer;
begin
        XML.Add('<polygon>');
        n:=APolygon.Count;
        for i:=0 to n-1 do begin
                DoPos(ConvertPoint(APolygon.Points[i]));
        end;
        XML.Add('</polygon>');
end;

procedure TBMExpForm.DoPixel(APixel: IPixel);
begin
        XML.Add('<point>');
        DoPos(ConvertPoint(APixel.Position));
        XML.Add('</point>');
end;

procedure TBMExpForm.DoSymbol(ASymbol: ISymbol);
begin
        XML.Add('<point>');
        DoPos(ConvertPoint(ASymbol.Position));
        XML.Add('</point>');
end;

procedure TBMExpForm.CorrectMapRect(APos: TGisPoint);
begin
        MapRect.Left:=Min(MapRect.Left,APos.X);
        MapRect.Top:=Max(MapRect.Top,APos.Y);
        MapRect.Right:=Max(MapRect.Right,APos.X);
        MapRect.Bottom:=Min(MapRect.Bottom,APos.Y);
end;

procedure TBMExpForm.OpenDB;
var
DBFilename: AnsiString;
begin
        CloseDB;
        DBFilename:=ChangeFileExt(Doc.Name,DBFileExt);
        if FileExists(DBFilename) then begin
                ADOConnection.ConnectionString:=StringReplace(DBConnectionTemplate,'%1',DBFilename,[]);
                try
                        ADOConnection.Open;
                        UsingDB:=True;
                except
                        UsingDB:=False;
                end;
        end;
end;

procedure TBMExpForm.CloseDB;
begin
        UsingDB:=False;
        if ADOConnection.Connected then begin
                ADOConnection.Close;
        end;
end;

procedure TBMExpForm.OpenTable(ALayer: ILayer);
var
ATableList: TStringList;
ATablename: AnsiString;
i: Integer;
AFieldname: AnsiString;
begin
        CloseTable;
        if UsingDB then begin
                ATableList:=TStringList.Create;
                try
                        ATablename:='ATT_' + ALayer.LayerName;
                        ADOConnection.GetTableNames(ATableList);
                        if ATableList.IndexOf(ATablename) > -1 then begin
                                ADOTable.TableName:=ATableName;
                                try
                                        ADOTable.Open;
                                        for i:=0 to ADOTable.FieldCount-1 do begin
                                                AFieldname:=ADOTable.Fields[i].FieldName;
                                                if Pos('gisid',Lowercase(AFieldname)) > 0 then begin
                                                        GisIdFieldName:=AFieldname;
                                                        GuidFieldName:='GUID';
                                                        UsingTable:=True;
                                                        break;
                                                end;
                                        end;
                                except
                                        UsingTable:=False;
                                end;
                        end;

                finally
                        ATableList.Free;
                end;
        end;
end;

procedure TBMExpForm.CloseTable;
begin
        UsingTable:=False;
        if ADOTable.Active then begin
                ADOTable.Close;
        end;
end;

procedure TBMExpForm.DoRecord(ABase: IBase);
var
i: Integer;
AFieldname: AnsiString;
begin
        if ADOTable.Locate(GisIdFieldName,ABase.Index,[]) then begin
                XML.Add('<record>');
                for i:=0 to ADOTable.FieldCount-1 do begin
                        AFieldName:=ADOTable.Fields[i].Fieldname;
                        if (AFieldName <> GisIdFieldName) and (AFieldName <> GuidFieldName) and (AFieldName[1] <> '_') then begin
                                DoField(ADOTable.Fields[i]);
                        end;
                end;
                XML.Add('</record>');
        end;
end;

procedure TBMExpForm.DoField(AField: TField);
var
AText: AnsiString;
begin
        AText:=EncodeText(AField.AsString);
        if AText <> '' then begin
                XML.Add('<field name="' + EncodeText(AField.FieldName) + '" value="' + AText + '" />');
        end;
end;

end.
