library BMLib;

uses
  Forms,
  Windows,
  SysUtils,
  ShellApi,
  WINGIS_TLB,
  BMExpMain,
  BMDBGeoCoderMain;

{$R *.res}

function ExecDBG(AParent: Integer; AKey: PChar; ALanguageCode: PChar): Integer; stdcall;
begin
        DBGMainForm:=TDBGMainForm.CreateParented(AParent);
        try
                DBGMainForm.BingMapsKey:=AKey;
                DBGMainForm.LanguageCode:=ALanguageCode;
                DBGMainForm.ShowModal;
                Result:=DBGMainForm.TransactionCount;
        finally
                DBGMainForm.Free;
        end;
end;

function ProjectToXMLFile(AHandle: Integer; ADoc: Variant; XMLFilename: PChar): HRESULT; stdcall;
begin
        Result:=S_FALSE;

        Application.Handle:=AHandle;
        BMExpForm:=TBMExpForm.Create(Application);
        try
                BMExpForm.Doc:=IUnknown(ADoc) as IDocument;
                BMExpForm.XMLFilename:=AnsiString(XMLFilename);
                if BMExpForm.Doc <> nil then begin
                        BMExpForm.Show;
                        BMExpForm.GisPro.Visible:=False;
                        if BMExpForm.DoProject then begin
                                Result:=S_OK;
                        end;
                        BMExpForm.Close;
                end;
        finally
                Application.Handle:=0;
                BMExpForm.Free;
        end;
end;

exports ExecDBG,ProjectToXMLFile;

begin
end.
