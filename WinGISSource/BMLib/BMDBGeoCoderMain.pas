unit BMDBGeoCoderMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, Grids, DBGrids, ComCtrls, WinInet;

type
  TDBGMainForm = class(TForm)
    ADOConnection: TADOConnection;
    OpenDialogDB: TOpenDialog;
    ADOTable: TADOTable;
    ADOCommand: TADOCommand;
    GroupBoxDB: TGroupBox;
    LabelTables: TLabel;
    ListBoxTables: TListBox;
    LabelFields: TLabel;
    ListBoxFields: TListBox;
    ButtonAddField: TButton;
    ButtonRemoveField: TButton;
    LabelQueryFields: TLabel;
    ListBoxQueryFields: TListBox;
    GroupBoxButtons: TGroupBox;
    ButtonLoadDB: TButton;
    ButtonConvert: TButton;
    ButtonClose: TButton;
    ButtonStop: TButton;
    GroupBoxProgress: TGroupBox;
    ProgressBar: TProgressBar;
    CheckBoxOverwrite: TCheckBox;
    procedure ButtonLoadDBClick(Sender: TObject);
    procedure ListBoxTablesClick(Sender: TObject);
    procedure ListBoxFieldsDblClick(Sender: TObject);
    procedure ListBoxQueryFieldsDblClick(Sender: TObject);
    procedure ButtonAddFieldClick(Sender: TObject);
    procedure ButtonRemoveFieldClick(Sender: TObject);
    procedure ButtonConvertClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure ButtonCloseClick(Sender: TObject);
    procedure ButtonStopClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    InternetSession: hInternet;
    StopClicked: Boolean;
    ReportMessage: AnsiString;
    function Address2Coordinate(AQuery: AnsiString; var X,Y: Double): Integer;
    function GetResponseParam(s: AnsiString; i: Integer): AnsiString;
    function PrepareAddress(s: AnsiString): AnsiString;
    function ConvertGeoCodeResult(s: AnsiString): AnsiString;
  public
    BingMapsKey: AnsiString;
    LanguageCode: AnsiString;
    TransactionCount: Integer;
  end;

const
  DBConnectionString = 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=%1;Persist Security Info=False';
  VEGeoCodeUrl = 'http://dev.virtualearth.net/REST/v1/Locations/{query}?o=xml&key={bingmapskey}';

  MaxBufferSize = 1024*1024;

var
WebBuffer: Array[0..1000000] of Char;
WebBufferSize: DWORD;

var
  DBGMainForm: TDBGMainForm;

implementation

{$R *.dfm}

function SToF(s: AnsiString): Double;
begin
     try
        s:=StringReplace(s,'.',DecimalSeparator,[rfReplaceAll]);
        s:=StringReplace(s,',',DecimalSeparator,[rfReplaceAll]);
        Result:=StrToFloat(s);
     except
        Result:=0;
     end;
end;

function StringBetween(var s: AnsiString; s1,s2: AnsiString; ARemove: Boolean): AnsiString;
var
p1,p2: Integer;
begin
     Result:='';
     p1:=Pos(s1,s) + StrLen(PChar(s1));
     p2:=Pos(s2,s);
     if (p1 > 0) and (p2 > 0) and (p2 > p1) then begin
        Result:=Copy(s,p1,p2-p1);
        if ARemove then begin
           s:=StringReplace(s,s1,'',[]);
           s:=StringReplace(s,s2,'',[]);
        end;
     end;
end;

procedure TDBGMainForm.FormCreate(Sender: TObject);
begin
        StopClicked:=False;
        TransactionCount:=0;
        BingMapsKey:='';
        LanguageCode:='';
        InternetSession:=InternetOpen(nil,INTERNET_OPEN_TYPE_PRECONFIG,nil,nil,INTERNET_SERVICE_URL);
end;

procedure TDBGMainForm.FormShow(Sender: TObject);
var
Buffer: Array[0..1023] of Char;
LngFileName: AnsiString;
LngList: TStringList;
begin
        ZeroMemory(@Buffer,Sizeof(Buffer));
        GetModuleFileName(hInstance,Buffer,Sizeof(Buffer));
        LngFileName:=StrPas(Buffer);
        LngFileName:=ChangeFileExt(LngFileName,'.' + LanguageCode);

        if not FileExists(LngFileName) then begin
                LngFileName:=ChangeFileExt(LngFileName,'.044');
        end;

        LngList:=TStringList.Create;
        try
                LngList.LoadFromFile(LngFileName);

                Self.Caption:=LngList.Values['1'];
                LabelTables.Caption:=LngList.Values['2'];
                LabelFields.Caption:=LngList.Values['3'];
                LabelQueryFields.Caption:=LngList.Values['4'];
                CheckBoxOverwrite.Caption:=LngList.Values['5'];
                ButtonLoadDB.Caption:=LngList.Values['6'];
                ButtonConvert.Caption:=LngList.Values['7'];
                ButtonStop.Caption:=LngList.Values['8'];
                ButtonClose.Caption:=LngList.Values['9'];
                ReportMessage:=LngList.Values['10'];

        finally
                LngList.Free;
        end;
end;

procedure TDBGMainForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
        InternetCloseHandle(InternetSession);
        ADOTable.Close;
        ADOConnection.Close;
end;

procedure TDBGMainForm.ButtonLoadDBClick(Sender: TObject);
begin
        if OpenDialogDB.Execute then begin
                if ADOConnection.Connected then begin
                        ADOConnection.Close;
                end;
                ADOConnection.ConnectionString:=StringReplace(DBConnectionString,'%1',OpenDialogDB.FileName,[]);
                ADOConnection.Open;
                if ADOConnection.Connected then begin
                        ADOConnection.GetTableNames(ListBoxTables.Items);
                end
                else begin
                        ListBoxTables.Clear;
                end;
        end;
end;

procedure TDBGMainForm.ListBoxTablesClick(Sender: TObject);
var
ATableName: AnsiString;
begin
        Screen.Cursor:=crHourGlass;
        try
                ATableName:=ListBoxTables.Items[ListBoxTables.ItemIndex];
                if ADOTable.Active then begin
                        ADOTable.Close;
                end;
                ADOTable.TableName:=ATableName;
                ADOTable.Open;
                if ADOTable.Active then begin
                        ADOTable.GetFieldNames(ListBoxFields.Items);
                        ListBoxQueryFields.Clear;
                end
                else begin
                        ListBoxFields.Clear;
                        ListBoxQueryFields.Clear;
                end;
        finally
                Screen.Cursor:=crDefault;
        end;
end;

procedure TDBGMainForm.ListBoxFieldsDblClick(Sender: TObject);
var
AFieldName: AnsiString;
begin
        if ListBoxFields.ItemIndex >= 0 then begin
                AFieldName:=ListBoxFields.Items[ListBoxFields.ItemIndex];
                if ListBoxQueryFields.Items.IndexOf(AFieldName) = -1 then begin
                        ListBoxQueryFields.Items.Add(AFieldName);
                end;
        end;
        ButtonConvert.Enabled:=(ListBoxQueryFields.Items.Count > 0);
end;

procedure TDBGMainForm.ListBoxQueryFieldsDblClick(Sender: TObject);
begin
        if ListBoxQueryFields.ItemIndex >= 0 then begin
                ListBoxQueryFields.Items.Delete(ListBoxQueryFields.ItemIndex);
        end;
        ButtonConvert.Enabled:=(ListBoxQueryFields.Items.Count > 0);
end;

procedure TDBGMainForm.ButtonAddFieldClick(Sender: TObject);
begin
        ListBoxFieldsDblClick(nil);
end;

procedure TDBGMainForm.ButtonRemoveFieldClick(Sender: TObject);
begin
        ListBoxQueryFieldsDblClick(nil);
end;

procedure TDBGMainForm.ButtonConvertClick(Sender: TObject);
var
AQuery: AnsiString;
i,n: Integer;
AFieldName: AnsiString;
XFound,YFound: Boolean;
X,Y: Double;
ARes: Integer;
FoundCount,RequestCount: Integer;
AMsg: AnsiString;
begin
        ButtonStop.Enabled:=True;
        ButtonLoadDB.Enabled:=False;

        XFound:=(ADOTable.Fields.FindField('X') <> nil);
        YFound:=(ADOTable.Fields.FindField('Y') <> nil);

        if (not XFound) or (not YFound) then begin
                ADOTable.Close;
        end;

        if not XFound then begin
                ADOCommand.CommandText:='ALTER TABLE ' + ADOTable.TableName + ' ADD COLUMN X FLOAT';
                ADOCommand.Execute;
        end;

        if not YFound then begin
                ADOCommand.CommandText:='ALTER TABLE ' + ADOTable.TableName + ' ADD COLUMN Y FLOAT';
                ADOCommand.Execute;
        end;

        if not ADOTable.Active then begin
                ADOTable.Open;
        end;

        if not ADOTable.Active then exit;

        ADOTable.First;
        ProgressBar.Max:=ADOTable.RecordCount;
        ProgressBar.Position:=0;

        FoundCount:=0;
        RequestCount:=0;

        repeat
                AQuery:='';

                n:=ListBoxQueryFields.Items.Count;
                for i:=0 to n-1 do begin
                        AFieldName:=ListBoxQueryFields.Items[i];
                        AQuery:=AQuery + PrepareAddress(ADOTable.FieldByName(AFieldName).AsString);
                        if i < n-1 then begin
                                AQuery:=AQuery + ' ';
                        end;
                end;

                ARes:=0;
                AQuery:=Trim(AQuery);

                if (AQuery <> '') then begin
                        if (CheckBoxOverWrite.Checked) or (ADOTable.FieldByName('X').AsFloat = 0) or (ADOTable.FieldByName('Y').AsFloat = 0) then begin
                                ARes:=Address2Coordinate(AQuery,X,Y);
                                Inc(RequestCount);
                        end;
                end;

                if ARes > 0 then begin
                        ADOTable.Edit;
                        ADOTable.FieldByName('X').Value:=X;
                        ADOTable.FieldByName('Y').Value:=Y;
                        ADOTable.Post;
                        Inc(FoundCount);
                end;

                if ARes = -1 then begin
                        break;
                end;

                ProgressBar.StepIt;
                ADOTable.Next;

                Application.ProcessMessages;

                if StopClicked then begin
                        break;
                end;

        until ADOTable.Eof;

        ProgressBar.Position:=0;
        ButtonLoadDB.Enabled:=True;

        if (FoundCount > 0) or (RequestCount > 0)then begin
                AMsg:=Format(ReportMessage,[FoundCount,RequestCount,OpenDialogDB.FileName,ADOTable.TableName]);
                AMsg:=StringReplace(AMsg,'\n',#13#10,[rfReplaceAll]);

                MessageBox(Handle,PChar(AMsg),'Report',MB_ICONINFORMATION);
        end;
end;

function TDBGMainForm.Address2Coordinate(AQuery: AnsiString; var X,
  Y: Double): Integer;
var
hURL: HInternet;
s,AUrl: AnsiString;
ResCount: Integer;
begin
        Result:=0;

        AUrl:=VEGeoCodeUrl;

        AUrl:=StringReplace(AUrl,'{query}',AQuery,[]);
        AUrl:=StringReplace(AUrl,'{bingmapskey}',BingMapsKey,[]);

        if InternetSession <> nil then begin
                hURL:=InternetOpenURL(InternetSession,PChar(AUrl),nil,0,INTERNET_FLAG_RELOAD,0);
                if hURL <> nil then begin
                        Inc(TransactionCount);
                        s:='';
                        repeat
                                InternetReadFile(hURL,@WebBuffer,Sizeof(WebBuffer),WebBufferSize);
                                s:=s + Copy(WebBuffer,0,WebBufferSize);
                        until (WebBufferSize = 0);

                        s:=ConvertGeoCodeResult(s);
                        ResCount:=StrToIntDef(GetResponseParam(s,0),0);
                        if ResCount > 0 then begin
                                X:=SToF(GetResponseParam(s,1));
                                Y:=SToF(GetResponseParam(s,2));
                                if (X <> 0) and (Y <> 0) then begin
                                        Result:=1;
                                end;
                        end;
                        InternetCloseHandle(hURL);
                end
                else begin
                        Result:=-1;
                end;
        end;
end;

function TDBGMainForm.ConvertGeoCodeResult(s: AnsiString): AnsiString;
var
ResultCount: Integer;
AName,ALat,ALong: AnsiString;
l: TStringList;
begin
     ResultCount:=0;
     Result:='';
     repeat
           AName:=Utf8Decode(StringBetween(s,'<Name>','</Name>',True));
           ALat:=StringBetween(s,'<Latitude>','</Latitude>',True);
           ALong:=StringBetween(s,'<Longitude>','</Longitude>',True);
           if (AName <> '') and (ALat <> '') and (ALong <> '') then begin
              Result:=Result + '|' + ALong + '|' + ALat + '|' + AName;
              Inc(ResultCount);
           end;
     until (AName = '');
     Result:=IntToStr(ResultCount) + Result;
end;

function TDBGMainForm.GetResponseParam(s: AnsiString; i: Integer): AnsiString;
var
AStringList: TStringList;
begin
        Result:='';
        AStringList:=TStringList.Create;
        try
                s:=StringReplace(s,'|',#13#10,[rfReplaceAll]);
                AStringList.Text:=s;
                if i < AStringList.Count then begin
                        Result:=AStringList[i];
                end;
        finally
                AStringList.Free;
        end;
end;

procedure TDBGMainForm.ButtonCloseClick(Sender: TObject);
begin
        Close;
end;

procedure TDBGMainForm.ButtonStopClick(Sender: TObject);
begin
        StopClicked:=True;
        ButtonStop.Enabled:=False;
end;

function TDBGMainForm.PrepareAddress(s: AnsiString): AnsiString;
begin
        s:=StringReplace(s,'-',' ',[rfReplaceAll]);
        s:=StringReplace(s,'_',' ',[rfReplaceAll]);
        s:=StringReplace(s,',',' ',[rfReplaceAll]);
        s:=StringReplace(s,'/',' ',[rfReplaceAll]);
        s:=StringReplace(s,'(',' ',[rfReplaceAll]);
        s:=StringReplace(s,')',' ',[rfReplaceAll]);
        Result:=s;
end;

end.

