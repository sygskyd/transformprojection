object DBGMainForm: TDBGMainForm
  Left = 353
  Top = 222
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = '!1'
  ClientHeight = 337
  ClientWidth = 441
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBoxDB: TGroupBox
    Left = 8
    Top = 8
    Width = 425
    Height = 217
    TabOrder = 0
    object LabelTables: TLabel
      Left = 8
      Top = 16
      Width = 9
      Height = 13
      Caption = '!2'
    end
    object LabelFields: TLabel
      Left = 136
      Top = 16
      Width = 9
      Height = 13
      Caption = '!3'
    end
    object LabelQueryFields: TLabel
      Left = 296
      Top = 16
      Width = 9
      Height = 13
      Caption = '!4'
    end
    object ListBoxTables: TListBox
      Left = 8
      Top = 32
      Width = 121
      Height = 153
      ItemHeight = 13
      TabOrder = 0
      OnClick = ListBoxTablesClick
    end
    object ListBoxFields: TListBox
      Left = 136
      Top = 32
      Width = 121
      Height = 153
      ItemHeight = 13
      TabOrder = 1
      OnDblClick = ListBoxFieldsDblClick
    end
    object ButtonAddField: TButton
      Left = 264
      Top = 72
      Width = 25
      Height = 25
      Caption = '>'
      TabOrder = 2
      OnClick = ButtonAddFieldClick
    end
    object ButtonRemoveField: TButton
      Left = 264
      Top = 104
      Width = 25
      Height = 25
      Caption = '<'
      TabOrder = 3
      OnClick = ButtonRemoveFieldClick
    end
    object ListBoxQueryFields: TListBox
      Left = 296
      Top = 32
      Width = 121
      Height = 153
      ItemHeight = 13
      TabOrder = 4
      OnDblClick = ListBoxQueryFieldsDblClick
    end
    object CheckBoxOverwrite: TCheckBox
      Left = 8
      Top = 192
      Width = 249
      Height = 17
      Caption = '!5'
      TabOrder = 5
    end
  end
  object GroupBoxButtons: TGroupBox
    Left = 8
    Top = 232
    Width = 425
    Height = 49
    TabOrder = 1
    object ButtonLoadDB: TButton
      Left = 8
      Top = 16
      Width = 75
      Height = 25
      Caption = '!6'
      TabOrder = 0
      OnClick = ButtonLoadDBClick
    end
    object ButtonConvert: TButton
      Left = 88
      Top = 16
      Width = 75
      Height = 25
      Caption = '!7'
      Enabled = False
      TabOrder = 1
      OnClick = ButtonConvertClick
    end
    object ButtonClose: TButton
      Left = 344
      Top = 16
      Width = 75
      Height = 25
      Caption = '!9'
      TabOrder = 2
      OnClick = ButtonCloseClick
    end
    object ButtonStop: TButton
      Left = 168
      Top = 16
      Width = 75
      Height = 25
      Caption = '!8'
      Enabled = False
      TabOrder = 3
      OnClick = ButtonStopClick
    end
  end
  object GroupBoxProgress: TGroupBox
    Left = 8
    Top = 288
    Width = 425
    Height = 41
    TabOrder = 2
    object ProgressBar: TProgressBar
      Left = 8
      Top = 17
      Width = 409
      Height = 16
      Min = 0
      Max = 100
      Smooth = True
      Step = 1
      TabOrder = 0
    end
  end
  object ADOConnection: TADOConnection
    LoginPrompt = False
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 24
    Top = 80
  end
  object OpenDialogDB: TOpenDialog
    Filter = 'DB Files|*.mdb; *.wgi|All Files|*.*'
    Left = 24
    Top = 48
  end
  object ADOTable: TADOTable
    Connection = ADOConnection
    Left = 56
    Top = 80
  end
  object ADOCommand: TADOCommand
    Connection = ADOConnection
    Parameters = <>
    Left = 88
    Top = 80
  end
end
