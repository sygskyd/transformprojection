unit wgusermain;

interface

uses
  SysUtils, Classes, HTTPApp, DB, ADODB, Windows, ActiveX;

type
  TWebModule1 = class(TWebModule)
    procedure WebModule1WebActionItemAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
    procedure WebModuleException(Sender: TObject; E: Exception; var Handled: Boolean);
  end;

var
WebModule1: TWebModule1;

implementation

{$R *.DFM}

procedure TWebModule1.WebModule1WebActionItemAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
const
DBCONNECTION = 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=%1;Persist Security Info=False';
var
DBFilename: AnsiString;
HTMLFilename: AnsiString;
Kunde: AnsiString;
Produkt: AnsiString;
Datum: AnsiString;
HardwareId: AnsiString;
Buffer: Array[0..255] of Char;
Code,Version,Userid,LastDate: DWord;
IdString: String;
s: AnsiString;
ADOTable: TADOTable;
i: Integer;
HTML: TStringList;
Res: AnsiString;
begin
        Res:='0';

        ZeroMemory(@Buffer,Sizeof(Buffer));
        GetModuleFileName(hInstance,Buffer,Sizeof(Buffer));
        DBFileName:=StrPas(Buffer);
        DBFileName:=StringReplace(DBFileName,'\\?\','',[]);
        DBFileName:=ChangeFileExt(DBFileName,'.mdb');

        HTMLFileName:=ChangeFileExt(DBFileName,'.htm');

        HTML:=TStringList.Create;
        try
                HTML.LoadFromFile(HTMLFilename);
                Response.Content:=HTML.Text;
        finally
                HTML.Free;
        end;

        if Request.QueryFields.Count = 0 then begin
                Response.SendResponse;
                exit;
        end;

        Kunde:='';
        Kunde:=Trim(Request.QueryFields.Values['u']);

        if Kunde = '' then begin
                Response.Content:=StringReplace(Response.Content,'###','ERROR PARAM',[]);
                Response.SendResponse;
                exit;
        end;

        Produkt:='';
        Produkt:=Trim(Request.QueryFields.Values['p']);

        Datum:='';
        Datum:=Trim(Request.QueryFields.Values['d']);

        HardwareId:=Trim(Request.QueryFields.Values['h']);

        if HardwareId = '' then begin
                Response.Content:=StringReplace(Response.Content,'###','ERROR_PARAM',[]);
                Response.SendResponse;
                exit;
        end;

        CoInitialize(nil);

        ADOTable:=TADOTable.Create(nil);

        LastDate:=35065;

        if Uppercase(Produkt) = 'WGPRO' then begin
                Version:=12;
        end
        else begin
                Version:=11;
        end;

        if (HardwareId <> '') then begin

                if Datum <> '' then begin
                        LastDate:=Trunc(StrToDate(Datum));
                end;

                IdString:=HardwareId;
                IdString:=StringReplace(IdString,'-','',[rfReplaceAll]);
                UserId:=StrToInt64(IdString);

                Code:=UserId + Version*1000000 + LastDate;
                Code:=not Code;

                s:=IntToStr(Code);

                while Length(s) < 12 do begin
                        s:='0' + s;
                end;

                Res:='';

                for i:=1 to 12 do begin
                        Res:=Res + s[i];
                        if (i = 4) or (i = 8) then begin
                                Res:=Res + '-';
                        end;
                end;
        end;

        if ADOTable.Active then begin
                ADOTable.Close;
        end;

        if not ADOTable.Active then begin
                ADOTable.ConnectionString:=StringReplace(DBCONNECTION,'%1',DBFilename,[]);
                ADOTable.TableName:='Freischaltungen';
                ADOTable.Open;
        end;

        if (ADOTable.Active) and (Uppercase(Kunde) <> 'X') then begin
                ADOTable.Append;

                ADOTable.FieldByName('Datum').AsDateTime:=Trunc(Now);
                ADOTable.FieldByName('Kunde').AsString:=Kunde;
                ADOTable.FieldByName('HardwareID').AsString:=HardwareId;

                if Version = 12 then begin
                        s:='WinGIS Professional';
                end
                else begin
                        s:='WinGIS Standard';
                end;

                ADOTable.FieldByName('Lizenz').AsString:=s;
                ADOTable.FieldByName('AktivierungsCode').AsString:=Res;

                if Datum <> '' then begin
                        ADOTable.FieldByName('Limitierung').AsDateTime:=Trunc(StrToDate(Datum));
                end;

                ADOTable.Post;
                ADOTable.Close;
        end;

        ADOTable.Free;
        CoUninitialize;

        Response.Content:=StringReplace(Response.Content,'###',Res,[]);
        Response.SendResponse;
end;

procedure TWebModule1.WebModuleException(Sender: TObject; E: Exception; var Handled: Boolean);
begin
        Response.Content:=StringReplace(Response.Content,'###',E.Message,[]);
        Response.Content:='Exception: ' + E.Message;
        Response.SendResponse;
end;

end.
