unit NPKLogMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, CPort, Buttons, ExtCtrls;

type TEventHandlers = class
    procedure GPSEvent(Sender: TObject; Count: Integer);
    procedure NPKEvent(Sender: TObject; Count: Integer);
  end;

type TGPSPos = record
     Latitude: Double;
     Longitude: Double;
end;

type TNPKLogMainForm = class(TForm)
    GroupBoxHeader: TGroupBox;
    GroupBoxData: TGroupBox;
    ButtonClose: TButton;
    ButtonClear: TButton;
    EditGPS: TEdit;
    LabelGPS: TLabel;
    LabelNPK: TLabel;
    EditNPK: TEdit;
    ButtonGPS: TSpeedButton;
    ButtonNPK: TSpeedButton;
    ButtonSave: TButton;
    SaveDialog: TSaveDialog;
    StatusBar: TStatusBar;
    ButtonLog: TButton;
    Timer: TTimer;
    CheckBoxAutoLog: TCheckBox;
    GroupBoxLog: TGroupBox;
    Memo: TMemo;
    procedure ButtonCloseClick(Sender: TObject);
    procedure ButtonGPSClick(Sender: TObject);
    procedure ButtonNPKClick(Sender: TObject);
    procedure ButtonSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButtonClearClick(Sender: TObject);
    procedure ButtonLogClick(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
  private
    LogLine: AnsiString;
    function NMEAToGPSPos(NMEAString: AnsiString; var AGPSPos: TGPSPos): Boolean;
    function NMEAToDeg(s: AnsiString; var d: Double): Boolean;
    procedure ShowGPSStatus(s: AnsiString);
    procedure ShowNPKStatus(s: AnsiString);
    procedure EnableControls;
  public
    procedure CreateLogLine;
    procedure AddLogLine;
  end;

var
  NPKLogMainForm: TNPKLogMainForm;
  EvHandler: TEventHandlers = nil;
  ComPortGPS: TComPort = nil;
  ComPortNPK: TComPort = nil;
  RxGPS: AnsiString = '';
  RxNPK: AnsiString = '';

const
  SepSign = ',';
  GPS_REGPATH = 'HKEY_CURRENT_USER\Software\PROGIS\GPS\Live';
  NPK_REGPATH = 'HKEY_CURRENT_USER\Software\PROGIS\GPS\NPK';

function StartGPS: Integer;
function StopGPS: Integer;
function StartNPK: Integer;
function StopNPK: Integer;
function StringToList(s: AnsiString; c: Char): TStringList;
function SToF(s: AnsiString): Double;

implementation

{$R *.dfm}

function SToF(s: AnsiString): Double;
begin
     try
        s:=StringReplace(s,'.',DecimalSeparator,[rfReplaceAll]);
        s:=StringReplace(s,',',DecimalSeparator,[rfReplaceAll]);
        Result:=StrToFloat(s);
     except
        Result:=0;
     end;
end;

function StringToList(s: AnsiString; c: Char): TStringList;
var
i,n: Integer;
APart: AnsiString;
begin
     Result:=TStringList.Create;
     n:=StrLen(PChar(s))+1;
     APart:='';
     for i:=1 to n do begin
         if (s[i] = c) or (i = n) then begin
            Result.Add(APart);
            APart:='';
         end
         else begin
            APart:=APart + s[i];
         end;
     end;
end;

procedure TNPKLogMainForm.ButtonCloseClick(Sender: TObject);
begin
        Close;
end;

procedure TNPKLogMainForm.CreateLogLine;
var
TimeStamp: AnsiString;
GPSPart: AnsiString;
NPKPart: AnsiString;
AGPSPos: TGPSPos;
TipId: AnsiString;
NHex,PHex,KHex: AnsiString;
NDec,PDec,KDec: AnsiString;
NVolt,PVolt,KVolt: AnsiString;
s: AnsiString;
p: DWORD;
begin
        TimeStamp:=FormatDateTime('yyyy-mm-dd' + SepSign + 'hh:nn:ss',Now);
        if NMEAToGPSPos(EditGPS.Text,AGPSPos) then begin
                GPSPart:=Format('%.6f',[AGPSPos.Latitude]) + SepSign + Format('%.6f',[AGPSPos.Longitude]);
        end
        else begin
                GPSPart:=SepSign;
        end;
        if (EditNPK.Text <> '') then begin

                TipId:='';
                NDec:='';
                PDec:='';
                KDec:='';
                NVolt:='';
                PVolt:='';
                KVolt:='';

                s:=EditNPK.Text;
                p:=Pos('7E',s)+38;
                if (p+4) < StrLen(PChar(s)) then begin
                        TipId:=Copy(s,p,4);
                end;

                p:=Pos('7E',s)+56;
                if (p+4) < StrLen(PChar(s)) then begin
                        NHex:=Copy(s,p+2,2) + Copy(s,p,2);
                        NDec:=Format('%d',[(StrToInt('$' + NHex))]);
                        NVolt:=Format('%.4f',[(StrToInt('$' + NHex)*(2.048/1023))]);
                end;

                p:=Pos('7E',s)+60;
                if (p+4) < StrLen(PChar(s)) then begin
                        PHex:=Copy(s,p+2,2) + Copy(s,p,2);
                        PDec:=Format('%d',[(StrToInt('$' + PHex))]);
                        PVolt:=Format('%.4f',[(StrToInt('$' + PHex)*(2.048/1023))]);
                end;

                p:=Pos('7E',s)+64;
                if (p+4) < StrLen(PChar(s)) then begin
                        KHex:=Copy(s,p+2,2) + Copy(s,p,2);
                        KDec:=Format('%d',[(StrToInt('$' + KHex))]);
                        KVolt:=Format('%.4f',[(StrToInt('$' + KHex)*(2.048/1023))]);
                end;

                if (TipId <> '') and (NDec <> '') and (PDec <> '') and (KDec <> '') then begin
                        NPKPart:=TipId + SepSign +
                                 NDec + SepSign + NVolt + SepSign +
                                 PDec + SepSign + PVolt + SepSign +
                                 KDec + SepSign + KVolt;
                        LogLine:=TimeStamp + SepSign + GPSPart + SepSign + NPKPart;
                        ShowNPKStatus('N=' + NDec + ' | P=' + PDec + ' | K=' + KDec);
                end
                else begin
                        ShowNPKStatus('No NPK Data');
                end;
        end;
end;

procedure TNPKLogMainForm.AddLogLine;
begin
        if Memo.Lines.Count = 0 then begin
                Memo.Lines.Add('Date' + SepSign +
                               'Time' + SepSign +
                               'Latitude' + SepSign +
                               'Longitude' + SepSign +
                               'Tip-ID' + SepSign +
                               'N-Dec' + SepSign +
                               'N-Volt' + SepSign +
                               'P-Dec' + SepSign +
                               'P-Volt' + SepSign +
                               'K-Dec' + SepSign +
                               'K-Volt'
                               );
        end;
        if LogLine <> '' then begin
                Memo.Lines.Add(LogLine);
        end;
end;

function TNPKLogMainForm.NMEAToGPSPos(NMEAString: AnsiString; var AGPSPos: TGPSPos): Boolean;
var
ParamsList: TStringList;
GPSQuality,SatNum: Integer;
HDOP: Double;
AStatus,Q,T: AnsiString;
ConvOK: Boolean;
begin
     Result:=False;
     NMEAString:=Trim(NMEAString);
     if Pos('$GPGGA',NMEAString) = 1 then begin
        ParamsList:=StringToList(NMEAString,',');
        try
           if ParamsList.Count > 8 then begin
              T:=ParamsList[1];
              T:=Copy(T,1,6);
              GPSQuality:=StrToInt(ParamsList[6]);
              SatNum:=StrToInt(ParamsList[7]);
              HDOP:=SToF(ParamsList[8]);
              ConvOK:=NMEAToDeg(ParamsList[2],AGPSPos.Latitude);
              if ParamsList[3] = 'S' then begin
                 AGPSPos.Latitude:=-AGPSPos.Latitude;
              end;
              ConvOK:=ConvOK and NMEAToDeg(ParamsList[4],AGPSPos.Longitude);
              if ParamsList[5] = 'W' then begin
                 AGPSPos.Longitude:=-AGPSPos.Longitude;
              end;
              if GPSQuality = 0 then begin
                 Q:='NO GPS FIX';
              end
              else if GPSQuality = 1 then begin
                 Q:='GPS FIX';
              end
              else if GPSQuality >=2 then begin
                 Q:='DGPS FIX';
              end;
              if GPSQuality > 0 then begin
                 AStatus:=' ' + Q + ' | SAT=' + IntToStr(SatNum) + ' | HDOP=' + FloatToStr(HDOP) + ' ';
              end
              else begin
                 AStatus:=' ' + Q + ' ';
              end;
              ShowGPSStatus(AStatus);
              Result:=(ConvOK) and (GPSQuality > 0);
           end;
        finally
           ParamsList.Free;
        end;
     end;
end;

function TNPKLogMainForm.NMEAToDeg(s: AnsiString; var d: Double): Boolean;
var
dd,mm: Double;
l: Integer;
begin
     Result:=False;
     d:=0;
     dd:=0;
     mm:=0;
     s:=Trim(s);
     if s <> '' then begin
        l:=StrLen(PChar(s));
        if l = 9 then begin
           dd:=SToF(Copy(s,1,2));
           mm:=SToF(Copy(s,3,7));
        end
        else if l = 10 then begin
           dd:=SToF(Copy(s,1,3));
           mm:=SToF(Copy(s,4,7));
        end;
        d:=dd + (mm/60);
        Result:=(d >= -180) and (d <= 180);
     end;
end;

procedure TNPKLogMainForm.ShowGPSStatus(s: AnsiString);
begin
        StatusBar.Panels[0].Text:=s;
        Application.ProcessMessages;
end;

procedure TNPKLogMainForm.ShowNPKStatus(s: AnsiString);
begin
        StatusBar.Panels[1].Text:=s;
        Application.ProcessMessages;
end;

procedure TEventHandlers.GPSEvent(Sender: TObject; Count: Integer);
var
s,gp: AnsiString;
p,l: Integer;
begin
        ComPortGPS.ReadStr(s,Count);
        s:=StringReplace(s,#13,'',[rfReplaceAll]);
        s:=StringReplace(s,#10,'',[rfReplaceAll]);
        if (RxGPS = '') or (s[1] = '$') then begin
                RxGPS:=s;
        end
        else if (RxGPS <> '') and (s[1] <> '$') then begin
                RxGPS:=RxGPS + s;
                p:=Pos('$GPGGA',RxGPS);
                if p > 0 then begin
                        l:=StrLen(PChar(RxGPS));
                        gp:=Copy(RxGPS,p,l-(p-1));
                        p:=Pos('*',gp);
                        if p > 0 then begin
                                gp:=Copy(gp,1,p+2);
                        end;
                        NPKLogMainForm.EditGPS.Text:=gp;
                        RxGPS:='';
                        NPKLogMainForm.CreateLogLine;
                end;
        end;
end;

function StartGPS: Integer;
begin
        Result:=-1;
        NPKLogMainForm.EditGPS.Text:='';
        ComPortGPS:=TComPort.Create(nil);
        if ComPortGPS <> nil then begin
                ComPortGPS.Events:=[evRxChar];
                ComPortGPS.OnRxChar:=EvHandler.GPSEvent;
                ComPortGPS.SyncMethod:=smNone;
                ComPortGPS.LoadSettings(stRegistry,GPS_REGPATH);
                if (ComPortGPS.ShowSetupDialog) and (ComPortGPS.Port <> '') then begin
                        try
                                ComPortGPS.Open;
                                if ComPortGPS.Connected then begin
                                        ComPortGPS.StoreSettings(stRegistry,GPS_REGPATH);
                                        Result:=0;
                                end;
                        except
                                Result:=-2;
                        end;
                end
                else begin
                        NPKLogMainForm.ButtonGPS.Down:=False;
                end;
        end;
end;

function StopGPS: Integer;
begin
        if ComPortGPS <> nil then begin
                if ComPortGPS.Connected then begin
                        ComPortGPS.Close;
                end;
                ComPortGPS.Free;
                ComPortGPS:=nil;
        end;
        NPKLogMainForm.ShowGPSStatus('');
        Result:=0;
end;

procedure TEventHandlers.NPKEvent(Sender: TObject; Count: Integer);
var
s,gp: AnsiString;
p,l: Integer;
begin
        ComPortNPK.ReadStr(s,Count);
        s:=StringReplace(s,#13,'',[rfReplaceAll]);
        s:=StringReplace(s,#10,'',[rfReplaceAll]);
        if (RxNPK = '') or (s[1] = '{') then begin
                RxNPK:=s;
        end
        else if (RxNPK <> '') and (s[1] <> '{') then begin
                RxNPK:=RxNPK + s;
                p:=Pos('{',RxNPK);
                if p > 0 then begin
                        l:=StrLen(PChar(RxNPK));
                        gp:=Copy(RxNPK,p,l-(p-1));
                        p:=Pos('}',gp);
                        if p > 0 then begin
                                gp:=Copy(gp,1,p);
                        end;
                        NPKLogMainForm.EditNPK.Text:=gp;
                        RxNPK:='';
                        NPKLogMainForm.CreateLogLine;
                        if NPKLogMainForm.CheckBoxAutoLog.Checked then begin
                                NPKLogMainForm.AddLogLine;
                        end;
                end;
        end;
end;

function StartNPK: Integer;
begin
        Result:=-1;
        NPKLogMainForm.EditNPK.Text:='';
        ComPortNPK:=TComPort.Create(nil);
        if ComPortNPK <> nil then begin
                ComPortNPK.Events:=[evRxChar];
                ComPortNPK.OnRxChar:=EvHandler.NPKEvent;
                ComPortNPK.SyncMethod:=smNone;
                ComPortNPK.LoadSettings(stRegistry,NPK_REGPATH);
                if (ComPortNPK.ShowSetupDialog) and (ComPortNPK.Port <> '') then begin
                        try
                                ComPortNPK.Open;
                                if ComPortNPK.Connected then begin
                                        ComPortNPK.StoreSettings(stRegistry,NPK_REGPATH);
                                        Result:=0;
                                end;
                        except
                                Result:=-2;
                        end;
                end
                else begin
                        NPKLogMainForm.ButtonNPK.Down:=False;
                end;
        end;
end;

function StopNPK: Integer;
begin
        if ComPortNPK <> nil then begin
                if ComPortNPK.Connected then begin
                        ComPortNPK.Close;
                end;
                ComPortNPK.Free;
                ComPortNPK:=nil;
        end;
        Result:=0;
end;

procedure TNPKLogMainForm.ButtonGPSClick(Sender: TObject);
begin
        if ButtonGPS.Down then begin
                StartGPS;
        end
        else begin
                StopGPS;
        end;
end;

procedure TNPKLogMainForm.ButtonNPKClick(Sender: TObject);
begin
        if ButtonNPK.Down then begin
                StartNPK;
        end
        else begin
                StopNPK;
        end;
end;

procedure TNPKLogMainForm.ButtonSaveClick(Sender: TObject);
begin
        if SaveDialog.Execute then begin
                Memo.Lines.SaveToFile(SaveDialog.FileName);
        end;
end;

procedure TNPKLogMainForm.FormCreate(Sender: TObject);
begin
        DecimalSeparator:='.';
        Application.Title:=Caption;
        EvHandler:=TEventHandlers.Create;
end;

procedure TNPKLogMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
        StopGPS;
        StopNPK;
        if EvHandler <> nil then begin
                EvHandler.Free;
                EvHandler:=nil;
        end;
end;

procedure TNPKLogMainForm.ButtonClearClick(Sender: TObject);
begin
        Memo.Lines.Clear;
end;

procedure TNPKLogMainForm.ButtonLogClick(Sender: TObject);
begin
        AddLogLine;
end;

procedure TNPKLogMainForm.EnableControls;
begin
        ButtonLog.Enabled:=(EditNPK.Text <> '') and
                           (DWORD(Pos('7E',EditNPK.Text)+68) < StrLen(PChar(EditNPK.Text))) and
                           (not CheckBoxAutoLog.Checked);
        ButtonSave.Enabled:=(Memo.Lines.Count > 0);
        ButtonClear.Enabled:=(Memo.Lines.Count > 0);
end;

procedure TNPKLogMainForm.TimerTimer(Sender: TObject);
begin
        EnableControls;
end;

end.
