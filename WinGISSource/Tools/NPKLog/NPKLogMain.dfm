object NPKLogMainForm: TNPKLogMainForm
  Left = 395
  Top = 150
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'NPK Log'
  ClientHeight = 388
  ClientWidth = 617
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBoxHeader: TGroupBox
    Left = 8
    Top = 8
    Width = 105
    Height = 233
    TabOrder = 0
    object ButtonGPS: TSpeedButton
      Left = 8
      Top = 16
      Width = 89
      Height = 25
      AllowAllUp = True
      GroupIndex = 1
      Caption = 'GPS...'
      Spacing = 0
      OnClick = ButtonGPSClick
    end
    object ButtonNPK: TSpeedButton
      Left = 8
      Top = 48
      Width = 89
      Height = 25
      AllowAllUp = True
      GroupIndex = 2
      Caption = 'NPK...'
      Spacing = 0
      OnClick = ButtonNPKClick
    end
    object ButtonClose: TButton
      Left = 8
      Top = 200
      Width = 89
      Height = 25
      Caption = 'Close'
      TabOrder = 4
      OnClick = ButtonCloseClick
    end
    object ButtonClear: TButton
      Left = 8
      Top = 168
      Width = 89
      Height = 25
      Caption = 'Clear Log'
      Enabled = False
      TabOrder = 3
      OnClick = ButtonClearClick
    end
    object ButtonSave: TButton
      Left = 8
      Top = 136
      Width = 89
      Height = 25
      Caption = 'Save Log...'
      Enabled = False
      TabOrder = 2
      OnClick = ButtonSaveClick
    end
    object ButtonLog: TButton
      Left = 8
      Top = 80
      Width = 89
      Height = 25
      Caption = 'Log Values'
      Enabled = False
      TabOrder = 0
      OnClick = ButtonLogClick
    end
    object CheckBoxAutoLog: TCheckBox
      Left = 8
      Top = 112
      Width = 89
      Height = 17
      Caption = 'Auto-Log'
      TabOrder = 1
    end
  end
  object GroupBoxData: TGroupBox
    Left = 8
    Top = 248
    Width = 601
    Height = 113
    TabOrder = 1
    object LabelGPS: TLabel
      Left = 8
      Top = 16
      Width = 51
      Height = 13
      Caption = 'GPS Data:'
    end
    object LabelNPK: TLabel
      Left = 8
      Top = 64
      Width = 51
      Height = 13
      Caption = 'NPK Data:'
    end
    object EditGPS: TEdit
      Left = 8
      Top = 32
      Width = 585
      Height = 21
      ReadOnly = True
      TabOrder = 0
    end
    object EditNPK: TEdit
      Left = 8
      Top = 80
      Width = 585
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 369
    Width = 617
    Height = 19
    Panels = <
      item
        Width = 200
      end
      item
        Width = 200
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object GroupBoxLog: TGroupBox
    Left = 120
    Top = 8
    Width = 489
    Height = 233
    TabOrder = 3
    object Memo: TMemo
      Left = 8
      Top = 16
      Width = 473
      Height = 209
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  object SaveDialog: TSaveDialog
    DefaultExt = '*.csv'
    Filter = 'CSV Files|*.csv'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 136
    Top = 32
  end
  object Timer: TTimer
    Interval = 100
    OnTimer = TimerTimer
    Left = 176
    Top = 32
  end
end
