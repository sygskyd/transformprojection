unit DkmTabCombMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, DB, ADODB, ExtCtrls, ShellApi;

type
  TMainForm = class(TForm)
    ProgressBar: TProgressBar;
    GroupBoxFiles: TGroupBox;
    LabelCSVFile: TLabel;
    EditCSV: TEdit;
    ButtonSelectCSV: TButton;
    LabelWGIFile: TLabel;
    EditWGI: TEdit;
    OpenDialogCSV: TOpenDialog;
    OpenDialogWGI: TOpenDialog;
    ButtonSelectWGI: TButton;
    GroupBoxButtons: TGroupBox;
    ButtonStart: TButton;
    ButtonClose: TButton;
    TimerControlsState: TTimer;
    LabelKGNR: TLabel;
    LabelGSTNR: TLabel;
    ComboBoxKGNR: TComboBox;
    ComboBoxGSTNR: TComboBox;
    procedure ButtonSelectCSVClick(Sender: TObject);
    procedure ButtonSelectWGIClick(Sender: TObject);
    procedure ButtonCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TimerControlsStateTimer(Sender: TObject);
    procedure ButtonStartClick(Sender: TObject);
  private
    WGIOK: Boolean;
    SepSign: AnsiString;
    CSV: TStringList;
    FieldList: TStringList;
    ValueList: TStringList;
    SQL: AnsiString;
    LastError: AnsiString;
    LogFileName: AnsiString;
  end;

var
  MainForm: TMainForm;

var
  CS: AnsiString = 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=%;Persist Security Info=False';

implementation

{$R *.dfm}

procedure TMainForm.FormCreate(Sender: TObject);
begin
        Application.Title:=Caption;
        WGIOK:=False;
        SepSign:='';
        CSV:=TStringList.Create;
        FieldList:=TStringList.Create;
        ValueList:=TStringList.Create;
        SQL:='';
        LogFileName:=ChangeFileExt(Application.ExeName,'.txt');
end;

procedure TMainForm.TimerControlsStateTimer(Sender: TObject);
begin
        ButtonStart.Enabled:=(Trim(EditCSV.Text) <> '') and
                             (Trim(EditWGI.Text) <> '') and
                             (WGIOK) and
                             (ComboBoxKGNR.ItemIndex > -1) and
                             (ComboBoxGSTNR.ItemIndex > -1);
end;

procedure TMainForm.ButtonSelectCSVClick(Sender: TObject);
var
i: Integer;
s: AnsiString;
d: Integer;
begin
        d:=1;
        if OpenDialogCSV.Execute then begin
                EditCSV.Text:=OpenDialogCSV.FileName;
                CSV.LoadFromFile(OpenDialogCSV.FileName);
                if CSV.Count > 1 then begin
                        s:=CSV[0];
                        if Pos(';',s) > 0 then begin
                                SepSign:=';';
                        end
                        else if Pos(',',s) > 0 then begin
                                SepSign:=',';
                        end
                        else begin
                                exit;
                        end;
                        s:=StringReplace(s,SepSign,#13#10,[rfReplaceAll]);
                        FieldList.Text:=s;
                        ComboBoxKGNR.Clear;
                        ComboBoxGSTNR.Clear;
                        for i:=0 to FieldList.Count-1 do begin
                                s:=FieldList[i];
                                s:=Trim(s);
                                if s = '' then begin
                                        s:='Field' + IntToStr(d);
                                        Inc(d);
                                end;
                                s:=StringReplace(s,' ','_',[rfReplaceAll]);
                                s:=StringReplace(s,',','_',[rfReplaceAll]);
                                s:=StringReplace(s,';','_',[rfReplaceAll]);
                                s:=StringReplace(s,'.','_',[rfReplaceAll]);
                                s:=StringReplace(s,'-','_',[rfReplaceAll]);
                                s:=StringReplace(s,'�','2',[rfReplaceAll]);
                                s:=StringReplace(s,'/','_',[rfReplaceAll]);
                                FieldList[i]:=s;
                                ComboBoxKGNR.Items.Add(s);
                                ComboBoxGSTNR.Items.Add(s);
                                s:=Uppercase(s);
                                if (ComboBoxKGNR.ItemIndex = -1) and (Pos('KG',s) > 0) and (Pos('NR',s) > 0) then begin
                                        ComboBoxKGNR.ItemIndex:=i;
                                end;
                                if (ComboBoxGSTNR.ItemIndex = -1) and (Pos('GST',s) > 0) and (Pos('NR',s) > 0) then begin
                                        ComboBoxGSTNR.ItemIndex:=i;
                                end;
                        end;
                end;
        end;
end;

procedure TMainForm.ButtonSelectWGIClick(Sender: TObject);
begin
        WGIOK:=False;
        if OpenDialogWGI.Execute then begin
                EditWGI.Text:=OpenDialogWGI.FileName;
                CS:=StringReplace(CS,'%',OpenDialogWGI.FileName,[]);
                WGIOK:=True;
        end;
end;

procedure TMainForm.ButtonCloseClick(Sender: TObject);
begin
        CSV.Free;
        FieldList.Free;
        ValueList.Free;
        Close;
end;

procedure TMainForm.ButtonStartClick(Sender: TObject);
var
i,k: Integer;
ADOCommand: TADOCommand;
ADODataset: TADODataset;
Cmd,s: AnsiString;
LogList: TStringList;
begin
        ADOCommand:=TADOCommand.Create(nil);
        ADODataset:=TADODataset.Create(nil);
        LogList:=TStringList.Create;
        LogList.Add('[OK] ' + CSV[0]);
        try
                ADOCommand.ConnectionString:=CS;
                ADODataset.ConnectionString:=CS;

                for i:=0 to FieldList.Count-1 do begin
                        if (Uppercase(FieldList[i]) <> Uppercase(ComboBoxKGNR.Items[ComboBoxKGNR.ItemIndex])) and
                           (Uppercase(FieldList[i]) <> Uppercase(ComboBoxGSTNR.Items[ComboBoxGSTNR.ItemIndex])) then begin
                                try
                                        Cmd:='ALTER TABLE ATT_Parzellen ADD ' + '[' + FieldList[i] + ']' + ' nvarchar(50)';
                                        ADOCommand.CommandText:=Cmd;
                                        ADOCommand.Execute;
                                except
                                        on E: Exception do begin
                                                LastError:=E.Message;
                                        end;
                                end;
                        end;
                end;

                ProgressBar.Max:=CSV.Count-1;
                ProgressBar.Position:=0;

                for i:=1 to CSV.Count-1 do begin
                        s:=CSV[i];
                        s:=StringReplace(s,SepSign,#13#10,[rfReplaceAll]);
                        ValueList.Text:=s;

                        ValueList[ComboBoxKGNR.ItemIndex]:=Trim(ValueList[ComboBoxKGNR.ItemIndex]);
                        ValueList[ComboBoxGSTNR.ItemIndex]:=Trim(ValueList[ComboBoxGSTNR.ItemIndex]);

                        if (ValueList[ComboBoxKGNR.ItemIndex] <> '') and (ValueList[ComboBoxGSTNR.ItemIndex] <> '') then begin

                        try

                        Cmd:='SELECT * FROM [ATT_Parzellen]' +
                             ' WHERE [KGNR] = "' + ValueList[ComboBoxKGNR.ItemIndex] + '"' +
                             ' AND [GSTNR] = "' + ValueList[ComboBoxGSTNR.ItemIndex] + '"';

                        if ADODataset.Active then begin
                                ADODataset.Close;
                        end;

                        ADODataset.CommandText:=Cmd;
                        ADODataset.Open;

                        if (ADODataset.Active) and (ADODataset.RecordCount >= 1) then begin
                                for k:=0 to ValueList.Count-1 do begin
                                        if (k <> ComboBoxKGNR.ItemIndex) and (k <> ComboBoxGSTNR.ItemIndex) then begin
                                                ADODataset.Edit;
                                                ADODataset.FieldByName(FieldList[k]).AsString:=ValueList[k];
                                                ADODataset.Post;
                                        end;
                                end;
                                LogList.Add('[OK] ' + CSV[i]);
                        end
                        else begin
                                LogList.Add('[FEHLER] ' + CSV[i]);
                        end;

                        except
                                on E: Exception do begin
                                        LastError:=E.Message;
                                end;
                        end;

                        end;

                        ProgressBar.StepIt;
                        Application.ProcessMessages;
                end;
        finally
                LogList.SaveToFile(LogFileName);
                ShellExecute(0,'open',PChar(LogFileName),nil,nil,SW_SHOW);
                LogList.Free;
                ProgressBar.Position:=0;
                ADOCommand.Free;
                if ADODataset.Active then begin
                        ADODataset.Close;
                end;
                ADODataset.Free;
        end;
end;

end.
