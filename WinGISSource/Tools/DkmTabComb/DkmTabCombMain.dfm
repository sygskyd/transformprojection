object MainForm: TMainForm
  Left = 393
  Top = 185
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'WinGIS - DKM Excel Combine - V1.2'
  ClientHeight = 258
  ClientWidth = 474
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ProgressBar: TProgressBar
    Left = 0
    Top = 241
    Width = 474
    Height = 17
    Align = alBottom
    Min = 0
    Max = 100
    Smooth = True
    Step = 1
    TabOrder = 1
  end
  object GroupBoxFiles: TGroupBox
    Left = 8
    Top = 8
    Width = 457
    Height = 161
    TabOrder = 0
    object LabelCSVFile: TLabel
      Left = 8
      Top = 16
      Width = 87
      Height = 13
      Caption = 'Excel Datei (CSV):'
    end
    object LabelWGIFile: TLabel
      Left = 8
      Top = 112
      Width = 89
      Height = 13
      Caption = 'WinGIS DB (WGI):'
    end
    object LabelKGNR: TLabel
      Left = 8
      Top = 56
      Width = 57
      Height = 13
      Caption = 'KGNR-Feld:'
    end
    object LabelGSTNR: TLabel
      Left = 184
      Top = 56
      Width = 64
      Height = 13
      Caption = 'GSTNR-Feld:'
    end
    object EditCSV: TEdit
      Left = 8
      Top = 32
      Width = 345
      Height = 21
      TabStop = False
      ReadOnly = True
      TabOrder = 4
    end
    object ButtonSelectCSV: TButton
      Left = 360
      Top = 32
      Width = 89
      Height = 25
      Caption = 'Ausw'#228'hlen...'
      TabOrder = 0
      OnClick = ButtonSelectCSVClick
    end
    object EditWGI: TEdit
      Left = 8
      Top = 128
      Width = 345
      Height = 21
      TabStop = False
      ReadOnly = True
      TabOrder = 5
    end
    object ButtonSelectWGI: TButton
      Left = 360
      Top = 128
      Width = 89
      Height = 25
      Caption = 'Ausw'#228'hlen...'
      TabOrder = 3
      OnClick = ButtonSelectWGIClick
    end
    object ComboBoxKGNR: TComboBox
      Left = 8
      Top = 72
      Width = 169
      Height = 21
      Style = csDropDownList
      DropDownCount = 20
      ItemHeight = 13
      TabOrder = 1
    end
    object ComboBoxGSTNR: TComboBox
      Left = 184
      Top = 72
      Width = 169
      Height = 21
      Style = csDropDownList
      DropDownCount = 20
      ItemHeight = 13
      TabOrder = 2
    end
  end
  object GroupBoxButtons: TGroupBox
    Left = 8
    Top = 176
    Width = 457
    Height = 57
    TabOrder = 2
    object ButtonStart: TButton
      Left = 8
      Top = 16
      Width = 89
      Height = 25
      Caption = 'Start'
      TabOrder = 0
      OnClick = ButtonStartClick
    end
    object ButtonClose: TButton
      Left = 104
      Top = 16
      Width = 89
      Height = 25
      Caption = 'Beenden'
      TabOrder = 1
      OnClick = ButtonCloseClick
    end
  end
  object OpenDialogCSV: TOpenDialog
    Filter = 'CSV Dateien|*.csv'
    Left = 128
    Top = 24
  end
  object OpenDialogWGI: TOpenDialog
    Filter = 'WGI Dateien|*.wgi'
    Left = 128
    Top = 136
  end
  object TimerControlsState: TTimer
    Interval = 100
    OnTimer = TimerControlsStateTimer
    Left = 384
    Top = 72
  end
end
