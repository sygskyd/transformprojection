object HaspMainForm: THaspMainForm
  Left = 416
  Top = 269
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'HASP Interface'
  ClientHeight = 93
  ClientWidth = 251
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 69
    Height = 13
    Caption = 'Serial Number:'
  end
  object ButtonWrite: TButton
    Left = 168
    Top = 40
    Width = 75
    Height = 25
    Caption = 'Write'
    Enabled = False
    TabOrder = 0
    OnClick = ButtonWriteClick
  end
  object EditSn: TSpinEdit
    Left = 88
    Top = 8
    Width = 153
    Height = 22
    MaxLength = 10
    MaxValue = 2147483647
    MinValue = 0
    TabOrder = 1
    Value = 0
  end
  object ButtonRead: TButton
    Left = 88
    Top = 40
    Width = 75
    Height = 25
    Caption = 'Read'
    Enabled = False
    TabOrder = 2
    OnClick = ButtonReadClick
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 74
    Width = 251
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object ButtonCreate: TButton
    Left = 8
    Top = 40
    Width = 75
    Height = 25
    Caption = 'Create'
    TabOrder = 4
    OnClick = ButtonCreateClick
  end
end
