unit HaspMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, u_hasp_hl, StdCtrls, Spin, ComCtrls;

type
  THaspMainForm = class(TForm)
    Label1: TLabel;
    ButtonWrite: TButton;
    EditSn: TSpinEdit;
    ButtonRead: TButton;
    StatusBar: TStatusBar;
    ButtonCreate: TButton;
    procedure ButtonWriteClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButtonReadClick(Sender: TObject);
    procedure ButtonCreateClick(Sender: TObject);
  private
  public
    procedure UpdateStatus(AStatus: hasp_status_t);
  end;

var
  HaspMainForm: THaspMainForm;

const
  VendorCode : AnsiString =
'cm9tNlk2RJRtSfZGQNcdJSDr7yv6LZrNAm+CYLTEgYHlXyViCbg8wpe7JFasx5AzRkQCWGtW8I1ZQCXJ' +
'mOepzemLaCargcu2ZcE6TdFeBE9CLOjIimKgEB/BPYNPiG6VSalZjrZORbWUC484oNVmmVHcT04uUSBh' +
'tLgsiS41xDWa+96P9IUSqAx6lfDk1+shDg1ctNybIthglRAAHrXhx9PkdU5kTWP3fdDw65PhRsepmN30' +
'lcb2j361Q4cYEVEfsA5TfSn8nU22WhBonEcnoAySFfvtc08iHwyk50dA90yPKHfo3kNQlgRFkVZFi1gI' +
'oTOtS+/t50yuifxtl7Nkcyqq9IRYcdqMwbaW+lIYg+Zg6IWk8CfaaOg7yqZHayHqm1WO+glJfDHH7vve' +
'dTippkiMheaX9Lf2jakWiPztqK24Kq4ps0V8yePmDzGO2EyI0jQN5V+nQcLDTRvVHEWwVzfmi+6p0nvP' +
'uKejm/kAiPXaq/9CIcaE+Br5H29UtHdkGkfHA/gPxvFHIeyFvxn++Jjq3YVfJD8f3znlxahp/em+BaA7' +
'GWwR7hY52WtChneDfPxfw85xWAXOKeU2xA7uhC0WddZ7e9yix8l/nxWu8pybKuntNKVSI1qmHDrJ0e+n' +
'eZZnDs7wUJttmqo1FP+QLJWL/laT94fN5YHjTBVl4YA42UchhzBD28aVlO7irRLVpUqCkLsgA6HdRAuB' +
'OZlJhqqfCY9W4Q==';

var
LoginHandle: hasp_handle_t = 0;
Feature: hasp_feature_t = $FFFF4000;

implementation

{$R *.dfm}

procedure THaspMainForm.UpdateStatus(AStatus: hasp_status_t);
var
s: String;
begin
        case AStatus of
                HASP_STATUS_OK:                 s:='HASP_STATUS_OK';
                HASP_MEM_RANGE:                 s:='HASP_MEM_RANGE';
                HASP_INV_PROGNUM_OPT:           s:='HASP_INV_PROGNUM_OPT';
                HASP_INSUF_MEM:                 s:='HASP_INSUF_MEM';
                HASP_TMOF:                      s:='HASP_TMOF';
                HASP_ACCESS_DENIED:             s:='HASP_ACCESS_DENIED';
                HASP_INCOMPAT_FEATURE:          s:='HASP_INCOMPAT_FEATURE';
                HASP_CONTAINER_NOT_FOUND:       s:='HASP_CONTAINER_NOT_FOUND';
                HASP_TOO_SHORT:                 s:='HASP_TOO_SHORT';
                HASP_INV_HND:                   s:='HASP_INV_HND';
                HASP_INV_FILEID:                s:='HASP_INV_FILEID';
                HASP_OLD_DRIVER:                s:='HASP_OLD_DRIVER';
                HASP_NO_TIME:                   s:='HASP_NO_TIME';
                HASP_SYS_ERR:                   s:='HASP_SYS_ERR';
                HASP_NO_DRIVER:                 s:='HASP_NO_DRIVER';
                HASP_INV_FORMAT:                s:='HASP_INV_FORMAT';
                HASP_REQ_NOT_SUPP:              s:='HASP_REQ_NOT_SUPP';
                HASP_INV_UPDATE_OBJ:            s:='HASP_INV_UPDATE_OBJ';
                HASP_KEYID_NOT_FOUND:           s:='HASP_KEYID_NOT_FOUND';
                HASP_INV_UPDATE_DATA:           s:='HASP_INV_UPDATE_DATA';
                HASP_INV_UPDATE_NOTSUPP:        s:='HASP_INV_UPDATE_NOTSUPP';
                HASP_INV_UPDATE_CNTR:           s:='HASP_INV_UPDATE_CNTR';
                HASP_INV_VCODE:                 s:='HASP_INV_VCODE';
                HASP_ENC_NOT_SUPP:              s:='HASP_ENC_NOT_SUPP';
                HASP_INV_TIME:                  s:='HASP_INV_TIME';
                HASP_NO_BATTERY_POWER:          s:='HASP_NO_BATTERY_POWER';
                HASP_NO_ACK_SPACE:              s:='HASP_NO_ACK_SPACE';
                HASP_TS_DETECTED:               s:='HASP_TS_DETECTED';
                HASP_FEATURE_TYPE_NOT_IMPL:     s:='HASP_FEATURE_TYPE_NOT_IMPL';
                HASP_UNKNOWN_ALG:               s:='HASP_UNKNOWN_ALG';
                HASP_INV_SIG:                   s:='HASP_INV_SIG';
                HASP_FEATURE_NOT_FOUND:         s:='HASP_FEATURE_NOT_FOUND';
        else
                s:='Error: ' + IntToStr(AStatus);
        end;

        if AStatus = HASP_STATUS_OK then begin
                StatusBar.Font.Color:=clBlack;
        end
        else begin
                StatusBar.Font.Color:=clRed;
        end;

        StatusBar.SimpleText:=s;
end;

procedure THaspMainForm.FormShow(Sender: TObject);
var
Res: hasp_status_t;
begin
        Res:=hasp_login(Feature,@VendorCode[1],LoginHandle);
        if Res = HASP_STATUS_OK then begin
                ButtonWrite.Enabled:=True;
                ButtonRead.Enabled:=True;
        end;
        UpdateStatus(Res);
        Randomize;
end;

procedure THaspMainForm.ButtonReadClick(Sender: TObject);
var
Res: hasp_status_t;
SerialData: Array[0..8] of Char;
SerialStr: AnsiString;
begin
        EditSn.Value:=0;

        FillChar(SerialData,9,0);

        Res:=hasp_read(LoginHandle,HASP_FILEID_MAIN,0,8,SerialData);

        SerialStr:=StrPas(SerialData);

        if StrLen(PChar(SerialStr)) > 0 then begin
                EditSn.Value:=StrToInt('$' + SerialStr);
        end;

        UpdateStatus(Res);
end;

procedure THaspMainForm.ButtonWriteClick(Sender: TObject);
var
Res: hasp_status_t;
SerialData: Array[0..8] of Char;
SerialStr: AnsiString;
begin
        FillChar(SerialData,9,0);

        SerialStr:=IntToHex(EditSn.Value,8);
        StrCopy(SerialData,PChar(SerialStr));

        Res:=hasp_write(LoginHandle,HASP_FILEID_MAIN,0,8,SerialData);

        UpdateStatus(Res);
end;

procedure THaspMainForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
        hasp_logout(LoginHandle);
end;

procedure THaspMainForm.ButtonCreateClick(Sender: TObject);
begin
        EditSn.Value:=100000+Random(900000);
end;

end.
