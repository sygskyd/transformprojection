library gpsapi;

uses Windows, SysUtils, Classes, CPort, Math, WinOSInfo, Forms, Controls;

{$R *.RES}

type TEventHandlers = class
        procedure ComPortRxChar(Sender: TObject; Count: Integer);
    end;

type TLogEntry = record
     Flags: Word;
     TimeStamp: DWord;
     Y: Integer;
     X: Integer;
     Alt: Word;
end;

Type PLogEntry = ^TLogEntry;

const
WINTEC_REGPATH = 'HKEY_CURRENT_USER\Software\PROGIS\GPS\Wintec';
LIVE_REGPATH = 'HKEY_CURRENT_USER\Software\PROGIS\GPS\Live';

TIMEOUT = 3000;
BUFFERSIZE = 4096;
BLOCKSIZE = 4096;

var
ComPort: TComPort;
Events: TComEvents;
PList: TStringList;
TList: TStringList;
LogStart: Integer;
LogEnd: Integer;
EntryCount: Integer;
Pwd: AnsiString;
ErrorCode: Integer;
LastDate,LastTime: AnsiString;
NMEAString: AnsiString;
Rx: AnsiString;

EvHandler: TEventHandlers;

NMEACallbackProc: procedure(NMEA: PChar); stdcall;

function Login: Boolean;
var
Buffer: Array[0..BUFFERSIZE-1] of Char;
s: AnsiString;
Retry: Boolean;
RetryCount: Integer;
n: Integer;
begin
        Result:=False;
        s:='';

        LogStart:=0;
        LogEnd:=0;

        n:=ComPort.InputCount;
        if ((n > 0 ) and (n < BUFFERSIZE)) then begin
                ComPort.Read(Buffer,n);
                ComPort.ClearBuffer(True,True);
        end;

        ZeroMemory(@Buffer,BUFFERSIZE-1);
        Events:=[evRxChar];

        if Pwd <> '' then begin
                ComPort.WriteStr('@AL,1' + Pwd + #10);
        end
        else begin
                ComPort.WriteStr('@AL,' + #10);
        end;

        ComPort.ClearBuffer(True,True);
        ComPort.WaitForEvent(Events,0,TIMEOUT);
        if evRxChar in Events then begin

                RetryCount:=0;
                repeat
                        ComPort.Read(Buffer,ComPort.InputCount);
                        s:=Buffer;
                        s:=StringReplace(s,#10,'',[rfReplaceAll]);
                        Result:=(Pos('LoginOK',s) > 0);

                        if ((Result = False) and (RetryCount < 10)) then begin
                                Retry:=True;
                        end
                        else begin
                                Retry:=False;
                        end;

                        Inc(RetryCount);
                        Sleep(10);

                until Retry = False;
        end
        else begin
                ErrorCode:=-2;
                exit;
        end;

        if not Result then begin
                if (Pos('PassworError',s) > 0) then begin
                        ErrorCode:=-3;
                        exit;
                end
                else begin
                        ErrorCode:=-4;
                        exit;
                end;
        end;

        ZeroMemory(@Buffer,BUFFERSIZE-1);
        Events:=[evRxChar];
        ComPort.WriteStr('@AL,5,1' + #10);
        ComPort.ClearBuffer(True,True);
        ComPort.WaitForEvent(Events,0,TIMEOUT);
        if evRxChar in Events then begin
                ComPort.Read(Buffer,ComPort.InputCount);
                s:=Buffer;
                s:=StringReplace(s,#10,'',[rfReplaceAll]);
                s:=StringReplace(s,'@AL,5,1,','',[rfReplaceAll]);
                s:=StringReplace(s,#13,'',[rfReplaceAll]);
                s:=StringReplace(s,#10,'',[rfReplaceAll]);
                LogStart:=StrToIntDef(s,-1);
        end
        else begin
                ErrorCode:=-5;
                exit;
        end;

        if LogStart = -1 then begin
                ErrorCode:=-7;
                exit;
        end;

        ZeroMemory(@Buffer,BUFFERSIZE-1);
        Events:=[evRxChar];
        ComPort.WriteStr('@AL,5,2' + #10);
        ComPort.ClearBuffer(True,True);
        ComPort.WaitForEvent(Events,0,TIMEOUT);
        if evRxChar in Events then begin
                ComPort.Read(Buffer,ComPort.InputCount);
                s:=Buffer;
                s:=StringReplace(s,#10,'',[rfReplaceAll]);
                s:=StringReplace(s,'@AL,5,2,','',[rfReplaceAll]);
                s:=StringReplace(s,#13,'',[rfReplaceAll]);
                s:=StringReplace(s,#10,'',[rfReplaceAll]);
                LogEnd:=StrToIntDef(s,-1);
        end
        else begin
                ErrorCode:=-6;
                exit;
        end;

        if LogEnd = -1 then begin
                ErrorCode:=-8;
                exit;
        end;

        if LogEnd < LogStart then begin
                ErrorCode:=-9;
                exit;
        end;

        if ((LogEnd-LogStart) = 0) then begin
                ErrorCode:=-10;
                exit;
        end;

        Result:=(ErrorCode = 0);
end;

Procedure Logout;
begin
        Events:=[evRxChar];
        ComPort.WriteStr('@AL,2,1' + #10);
        ComPort.WaitForEvent(Events,0,TIMEOUT);
end;

function BinToInt(Value: string): DWord;
var
i,iValueSize: Integer;
begin
        Result:=0;
        iValueSize:=Length(Value);
        for i:=iValueSize downto 1 do begin
                if Value[i] = '1' then begin
                        Result:=Result + (1 shl (iValueSize - i));
                end;
        end;
end;

function GetDate(i: DWord): String;
var
Day,Month,Year: DWord;
begin
        Day:=(i and BinToInt('00000000001111100000000000000000')) shr 17;
        Month:=(i and BinToInt('00000011110000000000000000000000')) shr 22;
        Year:=(i and BinToInt('11111100000000000000000000000000')) shr 26;
        Year:=Year + 2000;

        Result:=Format('%.4d/%.2d/%.2d',[Year,Month,Day]);
        LastDate:=Format('%.4d%.2d%.2d',[Year,Month,Day]);
end;

function GetTime(i: Dword): String;
var
Second,Minute,Hour: DWord;
begin
        Second:=(i and BinToInt('00000000000000000000000000111111'));
        Minute:=(i and BinToInt('00000000000000000000111111000000')) shr 6;
        Hour:=(i and BinToInt('00000000000000011111000000000000')) shr 12;

        Result:=Format('%.2d:%.2d:%.2d',[Hour,Minute,Second]);
        LastTime:=Format('%.2d%.2d',[Hour,Minute]);
end;

procedure ReadLogData;
var
LogList: PLogEntry;
LogEntry: PLogEntry;
LogListSize: Integer;
i,n: Integer;
s: AnsiString;
d: Double;
TrackId: Integer;
Pos: Integer;
P: Pointer;
l: Integer;
DataReceived: Boolean;
BlockInfo: Array[0..BUFFERSIZE-1] of Char;
begin
        TrackId:=0;
        DataReceived:=False;

        PList.Add('No,Latitude,Longitude,Name,Altitude,Date,Time');
        TList.Add('No,Latitude,Longitude,Name,Altitude,Date,Time');

        n:=(LogEnd-LogStart) div 16;

        LogListSize:=(n*SizeOf(TLogEntry))+1024;
        GetMem(LogList,LogListSize);
        ZeroMemory(LogList,LogListSize-1);

        P:=LogList;

        repeat
                Events:=[evRxChar];
                ComPort.ClearBuffer(True,True);
                ComPort.WriteStr('@AL,5,3,' + IntToStr(LogStart) + #10);

                ComPort.WaitForEvent(Events,0,TIMEOUT);
                if evRxChar in Events then begin

                        DataReceived:=True;

                        Pos:=0;
                        while (Pos < Min(BLOCKSIZE,(LogEnd-LogStart))) do begin
                                try
                                        l:=ComPort.InputCount;
                                        ComPort.Read(P^,l);
                                        P:=Pointer(Integer(P)+l);
                                        Pos:=Pos+l;
                                except
                                        ErrorCode:=-12;
                                        break;
                                end;
                        end;
                end;

                LogStart:=LogStart+BLOCKSIZE;

                ZeroMemory(@BlockInfo,BUFFERSIZE-1);
                Events:=[evRxChar];

                ComPort.WaitForEvent(Events,0,TIMEOUT);
                if evRxChar in Events then begin
                        ComPort.Read(BlockInfo,ComPort.InputCount);
                end;

        until (LogStart > LogEnd);

        if DataReceived then begin

                LogEntry:=LogList;

                for i:=0 to n-1 do begin

                        try
                                if ((LogEntry^.Flags and 1) > 0) then begin
                                        Inc(TrackId);
                                end;

                                s:='';
                                d:=LogEntry^.Y;
                                d:=d/10000000;
                                s:=s + Format('%2.7f,',[d]);
                                d:=LogEntry^.X;
                                d:=d/10000000;
                                s:=s + Format('%2.7f,',[d]);
                                s:=s + 'Track' + IntToStr(TrackId) + ',';
                                s:=s + IntToStr(LogEntry^.Alt) + ',';
                                s:=s + GetDate(LogEntry^.TimeStamp) + ',';
                                s:=s + GetTime(LogEntry^.TimeStamp);

                                if not ((LogEntry^.Flags and 4) > 0) then begin

                                        TList.Add(IntToStr(TList.Count) + ',' + s);
                                        Inc(EntryCount);

                                        if ((LogEntry^.Flags and 2) > 0) then begin
                                                PList.Add(IntToStr(PList.Count) + ',' + s);
                                        end;
                                end;

                                LogEntry:=Pointer(Integer(LogEntry) + SizeOf(TLogEntry));
                        except
                                ErrorCode:=-13;
                                break;
                        end;
                end;
        end
        else begin
                ErrorCode:=-11;
        end;

        FreeMem(LogList);
end;

procedure SaveLogFiles(ADir: AnsiString);
var
FileID: AnsiString;
begin
        FileID:=LastDate + LastTime;
        if PList.Count > 1 then begin
                PList.SaveToFile(ADir + FileID + '_poi.csv');
        end;
        if TList.Count > 1 then begin
                TList.SaveToFile(ADir + FileID + '_track.csv');
        end;
end;

function WintecToCsv(Password: PChar; TargetDir: PChar): Integer; stdcall;
begin
        ErrorCode:=0;
        EntryCount:=0;

        Events:=[evRxChar];
        DecimalSeparator:='.';

        LastDate:='';
        LastTime:='';

        Pwd:='';
        if Password <> nil then begin
                Pwd:=Trim(AnsiString(Password));
        end;

        ComPort:=TComPort.Create(nil);
        try
                ComPort.Events:=[];
                ComPort.LoadSettings(stRegistry,WINTEC_REGPATH);
                ComPort.ShowSetupDialog;

                if ComPort.Port <> '' then begin
                        try
                                ComPort.Open;
                        except
                                ErrorCode:=-1;
                        end;
                end;

                if ComPort.Connected then begin
                        PList:=TStringList.Create;
                        TList:=TStringList.Create;
                        Screen.Cursor:=crHourGlass;
                        try
                                if Login then begin
                                        ReadLogData;
                                end;
                        finally
                                Logout;

                                Screen.Cursor:=crDefault;

                                ComPort.Close;
                                ComPort.StoreSettings(stRegistry,WINTEC_REGPATH);

                                if (StrPas(TargetDir) <> '') then begin
                                        SaveLogFiles(TargetDir);
                                end
                                else begin
                                        SaveLogFiles(OSInfo.DocDataDir);
                                end;
                                PList.Free;
                                TList.Free;
                        end;
                end
                else begin
                        ErrorCode:=-1;
                end;
        finally
                ComPort.Free;
        end;

        if ErrorCode <> 0 then begin
                Result:=ErrorCode;
        end
        else begin
                Result:=EntryCount;
        end;
end;

procedure TEventHandlers.ComPortRxChar(Sender: TObject; Count: Integer);
var
s,gp: AnsiString;
p,l: Integer;
begin
        ComPort.ReadStr(s,Count);
        s:=StringReplace(s,#13,'',[rfReplaceAll]);
        s:=StringReplace(s,#10,'',[rfReplaceAll]);
        if (Rx = '') or (s[1] = '$') then begin
          Rx:=s;
        end;
        if (Rx <> '') and (s[1] <> '$') then begin
          Rx:=Rx + s;
        end;
        p:=Pos('$GPGGA',Rx);
        if p > 0 then begin
          l:=StrLen(PChar(Rx));
          gp:=Copy(Rx,p,l-(p-1));
          p:=Pos('*',gp);
          if p > 0 then begin
            gp:=Copy(gp,1,p+2);
          end;
          NMEAString:=gp;
          if @NMEACallbackProc <> nil then begin
            NMEACallbackProc(PChar(NMEAString));
            Rx:='';
          end;
        end;
end;

function StartLiveGPS(ACallback: Pointer): Integer; stdcall;
begin
        Result:=-1;
        if ACallback <> nil then begin
                @NMEACallbackProc:=ACallback;
                NMEAString:='';
                ComPort:=TComPort.Create(nil);
                if ComPort <> nil then begin
                        ComPort.Events:=[evRxChar];
                        EvHandler:=TEventHandlers.Create;
                        ComPort.OnRxChar:=EvHandler.ComPortRxChar;
                        ComPort.SyncMethod:=smNone;
                        ComPort.LoadSettings(stRegistry,LIVE_REGPATH);
                        if (ComPort.ShowSetupDialog) and (ComPort.Port <> '') then begin
                                try
                                        ComPort.Open;
                                        if ComPort.Connected then begin
                                                ComPort.StoreSettings(stRegistry,LIVE_REGPATH);
                                                Result:=0;
                                        end;
                                except
                                        Result:=-2;
                                end;
                        end;
                end;
        end;
end;

function StopLiveGPS: Integer; stdcall;
begin
        @NMEACallbackProc:=nil;
        if ComPort <> nil then begin
                if ComPort.Connected then begin
                        ComPort.Close;
                end;
                ComPort.Free;
                ComPort:=nil;
        end;
        if EvHandler <> nil then begin
                EvHandler.Free;
                EvHandler:=nil;
        end;
        Result:=0;
end;

exports WintecToCsv, StartLiveGPS, StopLiveGPS;

begin
end.


