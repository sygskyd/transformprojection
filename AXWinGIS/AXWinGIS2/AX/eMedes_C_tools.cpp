#include "stdafx.h"
#include "eMedes_C_tools.h"
#include "DebugWin.h"

#define xor ^

#define uchar unsigned char

#define MAX_NUMBER_LEN 1024

CString IntToStr(long lValue, int iBase)
{
 CString csValue;

    _ltoa(lValue, csValue.GetBuffer(MAX_NUMBER_LEN), iBase);
    csValue.ReleaseBuffer(-1);

    return csValue;
}

CString UIntToStr(long lValue, int iBase)
{
 CString csValue;

    _ultoa(lValue, csValue.GetBuffer(MAX_NUMBER_LEN), iBase);
    csValue.ReleaseBuffer(-1);

    return csValue;
}

CString DToStr(double dValue)
{
  CString csRet;
  int i;   
  int iPointOffset;
  CDebug Debug;
  
  //sprintf(csRet.GetBuffer(MAX_NUMBER_LEN), "%.2f", dValue);
  //csRet.ReleaseBuffer(-1);
  // old unsafe version
  // the csRet buffer could be overwritten if dValue has more digits than MAX_NUMBER_LEN
  // now values with lots of digits are written in exponential notation (eg 1.0E+0298)
  // is fixed since 1.1.0.19

  sprintf(csRet.GetBuffer(MAX_NUMBER_LEN), "%.14G", dValue);
  csRet.ReleaseBuffer(-1);
  
  iPointOffset = -1;
  i = 0;
  while (i < csRet.GetLength())
     {
      if (csRet.GetAt(i) == ',')
        {
         csRet.SetAt(i, '.');
        }
      i++;
     }
  

/*
  Debug.Print("sprintf: " + csRet);

  int  decimal, sign;
  CString csBuffer;
  
  csRet = "";

  csBuffer = _fcvt( dValue, 2, &decimal, &sign );
  // decimal contains the position of the decimal point inside the string (the offset)
  // the bytevalue of . is NOT stored in the string
  
  //int iBufferLen = csBuffer.GetLength();

  if (decimal < 0) decimal = 0;

  // add the point to our string
  csRet = csBuffer.Left(decimal) + "." + csBuffer.Mid(decimal);

  // Value between -1 and 1 (eg .123) ?  --  add a leading zero
  if (decimal == 0) 
     csRet = "0" + csRet;

  // add the sign
  if (sign != 0)
     csRet = "-" + csRet;


  Debug.Print("_fcvt: " + csRet);

*/
  // ---- next version

/*
  _gcvt(dValue, 14, csRet.GetBuffer(MAX_NUMBER_LEN));
  csRet.ReleaseBuffer(-1);

  Debug.Print("_gcvt: " + csRet); 
*/

  return csRet;   
}

CString IntToStrPad(long lValue, char cPad, int iLen, int iBase)
{
 CString csValue;
 CString csResult;
 LPTSTR pBuf;

    csValue = IntToStr(lValue, iBase);

    pBuf = csResult.GetBuffer(iLen + 1);
    memset(pBuf, 0, iLen + 1);
    memset(pBuf, cPad,
      (((iLen <= csValue.GetLength()) ? 0 : (iLen - csValue.GetLength()) ) ) );
    csResult.ReleaseBuffer(-1);

    csResult = csResult + csValue;

    return csResult;
}

CString IntToStrCut(long lValue, char cPad, int iLen)
{
 CString csValue;
 CString csResult;
 CString csCutted;
 LPTSTR pBuf;
 LPTSTR pCutted;

    csValue = IntToStr(lValue, 10);

    pBuf = csResult.GetBuffer(iLen + 1);
    memset(pBuf, 0, iLen + 1);
    memset(pBuf, cPad,
      (((iLen <= csValue.GetLength()) ? 0 : (iLen - csValue.GetLength()) ) ) );
    csResult.ReleaseBuffer(-1);

    csResult = csResult + csValue;
    

    pCutted = csCutted.GetBuffer(iLen + 1);
    memset(pCutted, 0, iLen + 1);
    memcpy(pCutted, 
           (((const char*)csResult) + (csResult.GetLength() - iLen) ),
           iLen);

    csResult.ReleaseBuffer(-1);
    csCutted.ReleaseBuffer(iLen);

    return csCutted;
}

CString BoolToStr(BOOL bValue)
{
  CString csRet;

  if (bValue)
    csRet = "1";
  else
    csRet = "0";

  return csRet;
}

long StrToInt(const char * csValue, int iBase)
{
char *stopstring;

    __try
       {        
        return strtol(csValue, &stopstring, iBase);
       }
    __except(EXCEPTION_EXECUTE_HANDLER)
       {
        return 0;
       }
}

BOOL StrToBool(CString csValue)
{
    if ((csValue == "0") || (csValue == ""))
      return FALSE;
    else
      return TRUE;
}

double StrToD(CString csValue)
{
    return atof((const char *)csValue);
}


CString GetRegValue(CString cskey, CString csValue)
{
HKEY hKey;
char * pBuffer;
DWORD lSize;
CString csResult;

    csResult = "";
    if (RegOpenKeyEx(HKEY_AXROOT, (const char *)cskey, 0, KEY_ENUMERATE_SUB_KEYS || KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS)
      {  
    
       lSize = MAX_PATH;       
       pBuffer = csResult.GetBuffer((lSize + 1));
       
       if (!RegQueryValueEx(hKey, (const char *)csValue, NULL, NULL, (uchar *)pBuffer, &lSize))
         {          
          csResult.ReleaseBuffer(-1);
         }
     
       RegCloseKey(hKey);              // close key opened above
      }  
    
    return (csResult);
}

CString GetLicValue(CString cskey, CString csValue, CString csFP)
{
CString csHexStr;
CString csResult;
CString csByte;
CString csFPByte;
UINT uiByte;
UINT uiFPByte;
char *stopstring;

   csResult = "";
   csHexStr = GetRegValue(cskey, csValue);
   // read two chars and convert to 8-bit integer
   for (int i = 0; (i+1) < csHexStr.GetLength(); i+=2)
      {
       
       csByte = csHexStr.Mid(i, 2);
       // convert byte as hex string to unsigned int
       uiByte = strtoul(csByte, &stopstring, 16);


       csFPByte = csFP.Mid((i % csFP.GetLength()), 2);
       // convert byte as hex string to unsigned int
       uiFPByte = strtoul(csFPByte, &stopstring, 16);

       csResult = csResult + CString((TCHAR)(uiByte ^ uiFPByte));

      }

   return csResult;    
}

long SetLicValue(CString cskey, CString csValue, CString csData, CString csFP)
{
CString csHexStr;
CString csResult;
CString csByte;
CString csFPByte;
UINT uiByte;
UINT uiFPByte;
char *stopstring;

   csHexStr = "";
   
   // read two chars and convert to 8-bit integer
   for (int i = 0; i < csData.GetLength(); i++)
      {
       
       uiByte = csData[i];

       csFPByte = csFP.Mid(((i*2) % csFP.GetLength()), 2);
       // convert byte as hex string to unsigned int
       uiFPByte = strtoul(csFPByte, &stopstring, 16);

       // crypt and hex it
       csHexStr = csHexStr + CString( IntToStrPad((TCHAR)(uiByte ^ uiFPByte), '0', 2, 16) );

      }

   return SetRegValue(cskey, csValue, csHexStr);
}

// test if the checksums stored in the registry are valid for this license key
// Return: TRUE if valid, FALSE else
BOOL ChecksumValid(CString csLKey, int iLKNr)
{
 BOOL bRet = FALSE;
 BOOL bLoop = TRUE;
 CString csChkS = "";
 unsigned int iChkS;
 int i;

 // calculate the checksum from the plain license key string
 for (i = 0, iChkS = 0; i < csLKey.GetLength(); i++)
    {
     iChkS += ((unsigned int)((TCHAR)csLKey[i]));
    }
 iChkS = iChkS % (0xff + 1);  // modulo 256
 
 for (i = 0; bLoop; i++)
    {
     switch(i)
       {
        case 0:          
          csChkS = GetRegValue("SYSTEM\\CurrentControlSet\\Control\\Print", "CHK" + IntToStr(iLKNr));
          if ( (csChkS == "") || (iChkS != (unsigned int)StrToInt(csChkS)) )
             bLoop = FALSE;    // invalid checksum, aborting
          break;

        case 1:
          csChkS = GetRegValue("SYSTEM\\CurrentControlSet\\Control\\Windows", "CHK" + IntToStr(iLKNr));
          if ( (csChkS == "") || (iChkS != (unsigned int)StrToInt(csChkS)) )
             bLoop = FALSE;    // invalid checksum, aborting
          break;

        case 2:
          csChkS = GetRegValue("Software\\Microsoft\\Windows\\CurrentVersion", "CHK" + IntToStr(iLKNr));
          if ( (csChkS == "") || (iChkS != (unsigned int)StrToInt(csChkS)) )
             bLoop = FALSE;    // invalid checksum, aborting
          break;

        default:
          bRet = TRUE;
          bLoop = FALSE;
          break; 
       }
    }

 return bRet;
}

// write the checksums for this license key into the registry
// Return: TRUE if successfull
BOOL UpdateChecksum(CString csLKey, int iLKNr)
{
 BOOL bRet = FALSE;
 BOOL bLoop = TRUE;
 CString csChkS = "";
 unsigned int iChkS;
 int i;

 // calculate the checksum from the plain license key string
 for (i = 0, iChkS = 0; i < csLKey.GetLength(); i++)
    {
     iChkS += ((unsigned int)((TCHAR)csLKey[i]));
    }
 iChkS = iChkS % (0xff + 1);  // modulo 256

 csChkS = IntToStr(iChkS);

 return 
 (SetRegValue("SYSTEM\\CurrentControlSet\\Control\\Print", "CHK" + IntToStr(iLKNr), csChkS) 
              == ERROR_SUCCESS) &&
 (SetRegValue("SYSTEM\\CurrentControlSet\\Control\\Windows", "CHK" + IntToStr(iLKNr), csChkS) 
              == ERROR_SUCCESS) &&
 (SetRegValue("Software\\Microsoft\\Windows\\CurrentVersion", "CHK" + IntToStr(iLKNr), csChkS) 
              == ERROR_SUCCESS);

}

CString BinaryToHex(BYTE *pByte, int iLen)
{
int i;
CString csResult = "";

   for (i = 0; i < iLen; i++)
      {
       csResult += IntToStrPad( *(BYTE *)(pByte+i), '0', 2, 16);
      }

   return csResult;  
}

void HexToBinary(CString csHex, BYTE *pByte, int &iLen)
{
int i;
int x;
CString csResult = "";

   x = 0; 
   for (i = 0; i < csHex.GetLength(); i+=2)
      {
       *(BYTE *)(pByte+(x++)) = (BYTE)StrToInt(csHex.Mid(i, 2));       
      }

   iLen = x;

   return;  
}

long SetRegValue(CString cskey, CString csValue, CString csData)
{
HKEY hKey;
long lResult;

    
    lResult = RegOpenKeyEx(HKEY_AXROOT, (const char *)cskey, 0, KEY_SET_VALUE, &hKey);
    if (lResult != ERROR_SUCCESS)
      {
       lResult = RegCreateKey(HKEY_AXROOT, (const char *)cskey, &hKey);
      }
    if (lResult == ERROR_SUCCESS)
      {  
    
       lResult = RegSetValueEx(hKey, (const char *)csValue, NULL, REG_SZ, (const BYTE *)(const char *)csData, csData.GetLength() + 1);
       RegCloseKey(hKey);              // close key opened above
      }
     
    return (lResult);
}

CString Crypt(CString csKey, CString csData)
{
ULONG lIndex;
ULONG lLen;
CString csResult;
char * pResult;

      lLen = csData.GetLength();
      pResult = csResult.GetBuffer(lLen + 1);
      
      for (lIndex = 0; lIndex < lLen; lIndex++)
         { 
          *(pResult + lIndex) = (csData.GetAt(lIndex) ^ csKey.GetAt( (lIndex % csKey.GetLength()) ));
         }
      *(pResult + lIndex) = 0;
      csResult.ReleaseBuffer(-1);

      return csResult;
}

CString Decrypt(CString csFP, CString csHexStr )
{
CString csResult;
CString csByte;
CString csFPByte;
UINT uiByte;
UINT uiFPByte;
char *stopstring;

   csResult = "";
   // read two chars and convert to 8-bit integer
   for (int i = 0; (i+1) < csHexStr.GetLength(); i+=2)
      {
       
       csByte = csHexStr.Mid(i, 2);
       // convert byte as hex string to unsigned int
       uiByte = strtoul(csByte, &stopstring, 16);


       csFPByte = csFP.Mid((i % csFP.GetLength()), 2);
       // convert byte as hex string to unsigned int
       uiFPByte = strtoul(csFPByte, &stopstring, 16);

       csResult = csResult + CString((TCHAR)(uiByte ^ uiFPByte));

      }

   return csResult;    

}

// Return: TRUE if already valid, FALSE if new created
BOOL ValidateFingerPrint(CString &csFingerPrint)
{
BOOL bRet = TRUE;

   if (csFingerPrint == "")
     {
      csFingerPrint = GetRegValue(AXWINGIS_REGKEY, "FP");
      if (csFingerPrint == "")
        {
         csFingerPrint = "1f2e3d4c5b6a";
         SetRegValue(AXWINGIS_REGKEY, "FP", csFingerPrint);
         bRet = FALSE;
        }
     }
   return bRet;
}

BOOL DecodeAllLicenses(CString &csFingerPrint, CPtrList &cList)
{
structLicense *pLicense;
CString csLKey = "";
BOOL bReady = FALSE;
int i = 0;
CDebug Debug;

   
   ValidateFingerPrint(csFingerPrint);

   cList.RemoveAll();

   while (!bReady)
      {
       
       csLKey = GetLicValue(AXWINGIS_REGKEY, IntToStr(i), csFingerPrint);
       if (csLKey != "")
         { 
          // "A-D-30-1234567890"        fresh developer licensekey advanced
          // "S-FORST98-365-987612"    fresh runtime for forst98 standard
          // "A-GEOQUIZ-60-554672-131231231-123131323-13" used runtime for geoquiz advanced
          //  axversion - type & name - DaysLicensed - serial - first used - last used - daysleft

          // test of checksum is valid for this key
          if (ChecksumValid(csLKey, i))
            {
#ifdef DEBUG_LICMAN
             Debug.Print("Checksum valid for this key: " + csLKey);
#endif
             pLicense = new structLicense;
          
             pLicense->iKeyNr = i;    // store the registry-key name (is a numeric value)
             pLicense->bModified = FALSE;

             DecodeLicenseString(pLicense, csLKey);

             // valid AXMode found ?
             if ((pLicense->ulAXMode == AXMODE_ADVANCED) || (pLicense->ulAXMode == AXMODE_STANDARD))
               {
                cList.AddTail(pLicense);
               }
            }
          else
            {
#ifdef DEBUG_LICMAN
            Debug.Print("Checksum NOT valid for this key: " + csLKey); 
#endif
            }
          i++;
          bReady = FALSE;
         }
       else
         bReady = TRUE;

      }

   return (! cList.IsEmpty());
}

BOOL DecodeLicenseString(structLicense *pLicense, CString csLKey)
{
BOOL bRet = FALSE;

    if (pLicense != NULL)
      { 
       // Check the AXMode
       pLicense->ulAXMode = AXMODE_INVALID;
       if (GetSubString(csLKey, 1, '-') == "A")
         pLicense->ulAXMode = AXMODE_ADVANCED;
       else if (GetSubString(csLKey, 1, '-') == "S")
         pLicense->ulAXMode = AXMODE_STANDARD;

       pLicense->csAppName = GetSubString(csLKey, 2, '-');
       pLicense->lDaysLicensed = StrToInt(GetSubString(csLKey, 3, '-'));
       pLicense->csSerialNr = GetSubString(csLKey, 4, '-');

	   if (GetSubString(csLKey, 5, '-') != "")
         {
          // license was already activated
          pLicense->tFirstUsed = StrToInt(GetSubString(csLKey, 5, '-')); 
          pLicense->tLastUsed = StrToInt(GetSubString(csLKey, 6, '-')); 
          pLicense->lDaysLeft = StrToInt(GetSubString(csLKey, 7, '-')); 
         }
       else
         {
          // new license, not used till now
          pLicense->tFirstUsed = 0;
          pLicense->tLastUsed = 0;
          pLicense->lDaysLeft = pLicense->lDaysLicensed;
         }
       
       if (pLicense->lDaysLeft < 0) pLicense->lDaysLeft = 0;

       bRet = TRUE; 
      } 
    return bRet;
}


BOOL UpdateLicense(structLicense *pLicense, BOOL bLicenseActive)
{
BOOL bRet = FALSE;
long lDaysUsed;

      if (pLicense != NULL)    
        {

         // was this license used before ? 
         if (pLicense->tFirstUsed == 0)
           {
            if (bLicenseActive)
              {
               pLicense->tFirstUsed = time(NULL);
               pLicense->tLastUsed = pLicense->tFirstUsed;               
               pLicense->lDaysLeft = pLicense->lDaysLicensed;
               pLicense->bModified = TRUE;
              }
           }
         else  // if (pLicense->tFirstUsed == 0)
           {
            // is this license currently used to unlock the control ?
            if (bLicenseActive)
              {
               // is the system date valid ? (�hh, does it seem to be valid ? :)
               if (pLicense->tFirstUsed < time(NULL))
                 {
                  pLicense->tLastUsed = time(NULL);
                  pLicense->bModified = TRUE;
                 }
              }
            else
              {
              }
                       
            lDaysUsed = (long)difftime(time(NULL), pLicense->tFirstUsed);
            if (lDaysUsed >= 0)
               {
                // convert from seconds to days
                lDaysUsed = lDaysUsed / (24*60*60);

                if ((pLicense->lDaysLicensed-lDaysUsed) >= 0)  
                  pLicense->lDaysLeft = pLicense->lDaysLicensed - lDaysUsed;
                else 
                  pLicense->lDaysLeft = 0;

                pLicense->bModified = TRUE;

               }


           }
           /*  pLicense->csAppName 
             pLicense->lDaysLicensed 
             pLicense->csSerialNr 
             pLicense->tFirstUsed 
             pLicense->tLastUsed 
             pLicense->lDaysLeft 
           */
         //bRet = TRUE;
        }

 return pLicense->bModified;
}

// Return: AXLIC* constants
int IsLicenseValid(structLicense *pLicense, CString csAppName, BOOL bDaysMayBeZero)
{
BOOL iRet = AXLIC_INVALID;
BOOL bContinue = TRUE;
long lDaysUsed;
int i;

   if (pLicense != NULL)    
     {

      for(i = 0; bContinue; i++)
        {

         switch(i)
           {
            case 0:
              // test the appname
              {
              CString csLApp(pLicense->csAppName);
              csLApp.MakeUpper();
              csAppName.MakeUpper();
              if ( (csLApp != csAppName) && (csAppName != "") )
                {
                 bContinue = FALSE;
                 iRet = AXLIC_NOT_FOUND;
                }
              }
              break;

            case 1:
              // test if the firstdate and lastdate is konsistent
              if ( (pLicense->tFirstUsed > pLicense->tLastUsed) ||
                   (pLicense->tFirstUsed > time(NULL))          ||
                   (pLicense->tLastUsed > time(NULL)) )
                {
                 bContinue = FALSE;
                 iRet = AXLIC_INVALID_SYSTEMDATE;
                }
              break;

            case 2:
              // test if the license is still good for today
              if (pLicense->tFirstUsed == 0)
                {
                 // was not used jet
                 if (!(pLicense->lDaysLicensed > 0))
                   {
                    bContinue = FALSE;
                    if (bDaysMayBeZero)
                      iRet = AXLIC_OK;
                    else
                      iRet = AXLIC_TIMED_OUT;
                   }
                }
              else  
                {              
                lDaysUsed = (long)difftime(time(NULL), pLicense->tFirstUsed);
                if (lDaysUsed >= 0)
                  {
                   // convert from seconds to days
                   lDaysUsed = lDaysUsed / (24*60*60);

                   if ( ((pLicense->lDaysLicensed - lDaysUsed) <= 0) ||
                         (pLicense->lDaysLeft <= 0) )
                      {
                       bContinue = FALSE;
                       if (bDaysMayBeZero)
                         iRet = AXLIC_OK;
                       else
                         iRet = AXLIC_TIMED_OUT; 
                      }                                  

                  } // end if (lDaysUsed >= 0)                
                }
              break;

            default:
              iRet = AXLIC_OK;
              bContinue = FALSE;
              break;

           }
        }
         
      



     }

   return iRet;

}

BOOL StoreLicense(structLicense *pLicense, CString csFP, CString &csErrMsg)
{
CString csLC;
BOOL bStoreOK = TRUE;
BOOL bRet = FALSE;
CDebug Debug;

   if (csFP != "")
     {
      if (pLicense != NULL)    
        {
          // "A-D-30-1234567890"        fresh developer licensekey advanced
          // "S-FORST98-365-987612"    fresh runtime for forst98 standard
          // "A-GEOQUIZ-60-554672-131231231-123131323-13" used runtime for geoquiz advanced
          //  axversion - type & name - DaysLicensed - serial - first used - last used - daysleft
          
          
          if (pLicense->ulAXMode == AXMODE_ADVANCED)
            csLC = "A";
          else if (pLicense->ulAXMode == AXMODE_STANDARD)
            csLC = "S";
          else
            bStoreOK = FALSE;
          
          if (bStoreOK)
            {
             csLC += "-" + pLicense->csAppName;
             csLC += "-" + IntToStr(pLicense->lDaysLicensed);
             csLC += "-" + pLicense->csSerialNr;
             csLC += "-" + IntToStr(pLicense->tFirstUsed);
             csLC += "-" + IntToStr(pLicense->tLastUsed);
             csLC += "-" + IntToStr(pLicense->lDaysLeft);

             if (UpdateChecksum(csLC, pLicense->iKeyNr))
               {
               if (SetLicValue(AXWINGIS_REGKEY, IntToStr(pLicense->iKeyNr), csLC, csFP) == ERROR_SUCCESS)
                 {
                  pLicense->bModified = FALSE;
                  bRet = TRUE;
                 }
               else 
                 {
                  csErrMsg = "Cannot store license.";
#ifdef DEBUG_LICMAN
                  Debug.Print("Cannot store license for keynr: " + IntToStr(pLicense->iKeyNr) 
                              + ", key: " + csLC);      
#endif
                 }
               }
             else
               {
                csErrMsg = "Cannot update checksum."; 
#ifdef DEBUG_LICMAN
                Debug.Print("Cannot store checksum for keynr: " + IntToStr(pLicense->iKeyNr) 
                            + ", key: " + csLC);    
#endif
               }
            }

        }  // if (pLicense != NULL)
     }     // if (csFP != "")

 return bRet;
}


// returns TRUE if valid design-mode license was found
BOOL TestOnlyDesignLicense()
{
structLicense *pLicense;
BOOL bRet = FALSE;
CString csAppName = "D";
CString csFP;
CPtrList cLicenseList;
CDebug Debug;
//long lDaysUsed;
//long lDaysLicensed; // to delete


#ifdef AX_OPEN_VERSION
Debug.Print("TestOnlyDesignLicense: Using open version, no license needed.");
return TRUE;
#endif


   if (DecodeAllLicenses(csFP, cLicenseList))
     {
       
      while ( ! cLicenseList.IsEmpty())
        {
         pLicense = (structLicense *)cLicenseList.RemoveHead();
         if (pLicense != NULL)
           {
            
            // test if this license is valid
            if( IsLicenseValid(pLicense, csAppName) == AXLIC_OK)
              {
               // ok set it as used, update and store if modified
               bRet = TRUE;
               
               /*iActiveLicenseNr = pLicense->iKeyNr;
               if (UpdateLicense(pLicense, TRUE) == TRUE)
                 {
                  StoreLicense(pLicense, m_fingerprint);
                 }
               */
              }
            else
              {
               // dont use it, but update the days and store if modified
               /*
               if (UpdateLicense(pLicense, FALSE) == TRUE)
                 {
                  StoreLicense(pLicense, m_fingerprint);
                 }
               */
              }

            delete pLicense;
           }

        }
     }

   return bRet;
}


//  ************************************************************************                   
//  FUNCTION:  AddLicenseKey
//  PARAMETER: csLKey...the new license string
//  RETURN:    TRUE if added
//  DESCRIPTION: decodes the licensekey and stores it to, if valid
//  ************************************************************************                   
BOOL AddLicenseKey(CString csHex, CString &csErrMsg)
{
structLicense *pLicense;
BOOL bRet = FALSE;
CString csLKey;
CString csFP;
int iNewNr = 0;
BOOL bReady = FALSE;
CDebug Debug;

   
   ValidateFingerPrint(csFP);
   
   while (!bReady)
      {       
       if(GetRegValue(AXWINGIS_REGKEY, IntToStr(iNewNr)) == "")       
         {
          bReady = TRUE;           
         }
       else iNewNr++;              
      }



   pLicense = new structLicense;
   if (pLicense != NULL)
     {
       
      pLicense->iKeyNr = iNewNr;     // store the registry-key name (is a numeric value)
      pLicense->bModified = FALSE;

      csLKey = Decrypt(csFP, csHex);

      DecodeLicenseString(pLicense, csLKey);

      // valid AXMode found ?
      if ((pLicense->ulAXMode == AXMODE_ADVANCED) || (pLicense->ulAXMode == AXMODE_STANDARD))
        {
          //cList.AddTail(pLicense);

          if (IsLicenseSerialUnique(pLicense))
            {
             bRet = StoreLicense(pLicense, csFP, csErrMsg);
             if (bRet)
               Debug.Print("New License added");
             else 
               Debug.Print("New License NOT added");
            }
          else
            {
             csErrMsg = "A license with the same serial is already registered on your system.";
            }
        }
      else
        {
         csErrMsg = "Invalid license.";
        }

      delete pLicense;
     }
      
   return bRet;
}

BOOL IsLicenseSerialUnique(structLicense *pTestLicense)
{
structLicense *pLicense;
CString csFingerPrint = "";
CString csLKey = "";
BOOL bReady = FALSE;
BOOL bRet = TRUE;
int i = 0;
CDebug Debug;

   
   if (pTestLicense != NULL)
     {
      // if the new license key to be added is for one
      // of the following AppNames, multiple keys with
      // the same serial are ALLOWED !
      // for all others: you may NOT add the same key twice !!
      if ( ! ((pTestLicense->csAppName == "WINMON") ||
              (pTestLicense->csAppName == "KAERNTEN") ||
              (pTestLicense->csAppName == "GEOQUIZ") ||
              (pTestLicense->csAppName == "DEBUG") ||
              (pTestLicense->csAppName == "FORST98") ||
              (pTestLicense->csAppName == "FORST")
             )  
          )
        {
         ValidateFingerPrint(csFingerPrint);

         while ((!bReady) && bRet)
            {
       
             csLKey = GetLicValue(AXWINGIS_REGKEY, IntToStr(i), csFingerPrint);
             if (csLKey != "")
               { 
                // "A-D-30-1234567890"        fresh developer licensekey advanced
                // "S-FORST98-365-987612"    fresh runtime for forst98 standard
                // "A-GEOQUIZ-60-554672-131231231-123131323-13" used runtime for geoquiz advanced
                //  axversion - type & name - DaysLicensed - serial - first used - last used - daysleft

                // test of checksum is valid for this key
                pLicense = new structLicense;
    
                pLicense->iKeyNr = i;    // store the registry-key name (is a numeric value)
                pLicense->bModified = FALSE;

                DecodeLicenseString(pLicense, csLKey);

                // check is appname and serial are the same
                if ((pTestLicense->csAppName == pLicense->csAppName) &&
                    (pTestLicense->csSerialNr == pLicense->csSerialNr))
                  {     
                   bRet = FALSE;
                  }
              
                i++;
                bReady = FALSE;
               }
             else
               bReady = TRUE;

            }
        }
     }
   return (bRet);
}

//  ************************************************************************                   
//  FUNCTION:  GetSubString
//  PARAMETER: CmdStr...DDE-Befehl von WinGIS/DataLink
//             pos......Position des gew�nschten Elementes beginnend mit 1
//             cSep.....char Seperator 
//  RETURN:    a CString containing the substring at pos
//  BEISPIEL:  GetSubString("abc/123/def/456",1,'/') liefert "abc"
//  ************************************************************************                   
CString GetSubString (const char *CmdStr, int pos, char cSep)
{                
    static char token[255];
    int    i;
    const char * AktPos;
    const char * EndPos; 
    int    brace;    
    int    len;
    
    brace = (int)cSep;
    
    if (pos > GetSubStringCount(CmdStr,brace))
      { 
       //Die Anzahl der Substrings in CmdStr
       //ist kleiner als pos
       token[0] = 0;
      }
    else
      {
        AktPos = CmdStr;
        for (i = 1;(i < pos) && (AktPos != NULL);i++)
           { 
            AktPos = strchr((char *)(AktPos), brace);
            if (AktPos != NULL)
              AktPos = AktPos + 1;
           }


        memset(token,0,255);
        if (AktPos != NULL)
          {   

           EndPos = (char *)strchr((char *)(AktPos), brace);  
        
           if (EndPos == NULL)
             strncpy((char *)token,(char *)(AktPos),(strlen((char *)(AktPos)) > 255 ? 255 : strlen((char *)(AktPos))) );
           else
             {
             len = (int)(EndPos - AktPos);
             if (len < 0) len = 0;
             strncpy((char *)token,(char *)(AktPos), len);
             }
          }
      }
       
    return CString(token);   
}                                                                           
//  ************************************************************************                   
//  FUNCTION:  GetSubStringCount
//  PARAMETER: CmdStr...string
//             cSep.....char Seperator 
//  RETURN:    number of substrings found in CmdStr, seperated by cSep
//  BEISPIEL:  GetSubStringCount("abc/123/def/456",'/') liefert 4
//  ************************************************************************
int GetSubStringCount(const char *CmdStr, char cSep)
{
    int iCount;
    const char *AktPos;
    int brace;
    
    brace = (int)cSep;

    AktPos = CmdStr - 1;
    iCount = 0;

    do {
        iCount = iCount + 1;
        AktPos = strchr((char *)(AktPos + 1), brace);
       } while (AktPos != NULL);

    return iCount;
}

void ClearLicenseList(CPtrList &cLicenseList)
{
structLicense *pLicense;
       
      while ( ! cLicenseList.IsEmpty())
        {
         pLicense = (structLicense *)cLicenseList.RemoveHead();
         if (pLicense != NULL)
           {
            delete pLicense;
           }
        }
}


