// AXWingisPpg.cpp : Implementation of the CAXWingisPropPage property page class.

#include "stdafx.h"
#include "AXWingis.h"
#include "AXWingisPpg.h"

#include "interface_version.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CAXWingisPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CAXWingisPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CAXWingisPropPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

//IMPLEMENT_OLECREATE_EX(CAXWingisPropPage, "AXWINGIS.AXWingisPropPage.1",
//	0x6a65e784, 0x23ae, 0x11d1, 0xbd, 0x93, 0x44, 0x45, 0x53, 0x54, 0, 0)

CPP_CLASSID_PROPPAGE

/////////////////////////////////////////////////////////////////////////////
// CAXWingisPropPage::CAXWingisPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CAXWingisPropPage

BOOL CAXWingisPropPage::CAXWingisPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_AXWINGIS_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CAXWingisPropPage::CAXWingisPropPage - Constructor

CAXWingisPropPage::CAXWingisPropPage() :
	COlePropertyPage(IDD, IDS_AXWINGIS_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CAXWingisPropPage)
	m_sServer = _T("");
	m_sTopic = _T("");
    m_sAppname = _T("");
	m_bActive = FALSE;
	m_bUseSend = FALSE;
	m_DDEML = FALSE;
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CAXWingisPropPage::DoDataExchange - Moves data between page and properties

void CAXWingisPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CAXWingisPropPage)
	DDP_Check(pDX, IDC_COM, m_DDEML, _T("bUseCOM") );
	DDX_Check(pDX, IDC_COM, m_DDEML);
	DDP_Text(pDX, IDC_SERVER, m_sServer, _T("sServer") );
	DDX_Text(pDX, IDC_SERVER, m_sServer);
	DDP_Text(pDX, IDC_TOPIC, m_sTopic, _T("sTopic") );
	DDX_Text(pDX, IDC_TOPIC, m_sTopic);
	DDP_Text(pDX, IDC_APPNAME, m_sAppname, _T("sAppName") );
	DDX_Text(pDX, IDC_APPNAME, m_sAppname);
	DDP_Check(pDX, IDC_USESEND, m_bUseSend, _T("bUseSend") );
	DDX_Check(pDX, IDC_USESEND, m_bUseSend);
	DDP_Check(pDX, IDC_ACTIVE, m_bActive, _T("bActive") );
	DDX_Check(pDX, IDC_ACTIVE, m_bActive);
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CAXWingisPropPage message handlers

BOOL CAXWingisPropPage::OnInitDialog() 
{
	COlePropertyPage::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetPageName("AXWingis Settings");

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
