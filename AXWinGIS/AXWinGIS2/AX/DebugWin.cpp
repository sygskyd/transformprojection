
#include "stdafx.h"
#include "DebugWin.h"
#include "eMedes_C_tools.h"

#define SHARED_STRING_LEN 4096


#ifdef _DEBUGWIN_THREAD
#endif

UINT MyControllingFunction( LPVOID pParam );


CDebug::CDebug() : CObject()
{
#ifdef _DEBUGWIN_ON

    lpvMem = NULL;
    iDebugWinFindTry = 0;
    hDebugWin = 0;

#ifdef _DEBUGWIN_THREAD
    bInitialized = FALSE;
    pThread = NULL;
    bKilled = FALSE;
#endif
    hMap = CreateFileMapping(
         (HANDLE)0xFFFFFFFF,    //* use paging file    */
         NULL,                  //* no security attr.  */
         PAGE_READWRITE,        //* read/write access  */
         0,                     //* size: high 32-bits */
         SHARED_STRING_LEN,     //* size: low 32-bits  */
         "DebugWinFileMap");    //* name of map object */


          //* The first process to attach initializes memory. */

    bOk = ( ((GetLastError() != ERROR_ALREADY_EXISTS) && 
             (GetLastError() != ERROR_SUCCESS)) ? FALSE : TRUE);

    //* Get a pointer to the file-mapped shared memory. */

    lpvMem = MapViewOfFile(
                hMap,           //* object to map view of    */
                FILE_MAP_WRITE, //* read/write access        */
                0,              //* high offset:   map from  */
                0,              //* low offset:    beginning */
                0);             //* default: map entire file */

    hDebugWin = FindWindow("TfrmDebug", "eMEDES DebugWin");

#ifdef _DEBUGWIN_THREAD
    
    bExitThread = FALSE; 
 
    InitializeCriticalSection(&csSl);

    hSem = CreateSemaphore(NULL, 0, 5000, NULL);

    //if (hSem != NULL)
    //  AfxBeginThread(MyControllingFunction, &sDeb);
#endif
#endif
    return;
}

void CDebug::KillThread()
{
#ifdef _DEBUGWIN_THREAD

    if (bKilled == FALSE)
      {
       if ((bInitialized == TRUE) && (pThread != NULL))
         {
          HANDLE hThread = pThread->m_hThread;
          bExitThread = TRUE;
          ReleaseSemaphore(hSem, 1, NULL);
          WaitForSingleObject(hThread, 36000); 
          pThread = NULL;          
         }
       
          DeleteCriticalSection(&csSl);
          CloseHandle(hSem);
          slDebug.RemoveAll();
          bInitialized = FALSE;
          bKilled = TRUE;
      }
#endif  
}

CDebug::~CDebug()
{
#ifdef _DEBUGWIN_ON

#ifdef _DEBUGWIN_THREAD
       KillThread();
#endif

       if (lpvMem != NULL)
         UnmapViewOfFile(lpvMem);
       if (bOk)
         CloseHandle(hMap);

#endif
 return;
}

void CDebug::PrintInt(const char* sOut, int iVal, int iRadix)
{
#ifdef _DEBUGWIN_ON

 char buf[24];
 char *pBuf = NULL;

 __try
   {
    _ltoa(iVal, buf, iRadix);
    pBuf = (char *)malloc(lstrlen(sOut) + lstrlen(buf) + 1);
    lstrcpy(pBuf, sOut);
    lstrcat(pBuf, buf);
    Print(pBuf);
    if (pBuf != NULL)
      free(pBuf);
    pBuf = NULL;
   }
 __except(-1)
   {
    if (pBuf != NULL)
      free(pBuf);
    pBuf = NULL;
   }

#endif
 return;
}


UINT MyControllingFunction( LPVOID pParam )
{
   if (pParam != NULL)
     {
      AfxEndThread(((CDebug *)pParam)->ThreadProc());
      //((CDebug *)pParam)->ThreadProc();
     }
   return 0;
}

UINT CDebug::ThreadProc()
{
#ifdef _DEBUGWIN_ON
#ifdef _DEBUGWIN_THREAD
CString csOut;

  while ( ! bExitThread)
    {
     Sleep(10);
     if (WaitForSingleObject(hSem, 35000) == WAIT_OBJECT_0) 
       {
    
        EnterCriticalSection(&csSl);

        if ( ! slDebug.IsEmpty())
          {
           csOut = slDebug.RemoveHead();
           LeaveCriticalSection(&csSl);

           if ((lpvMem != NULL) && (csOut != ""))
             {
              if (!hDebugWin) hDebugWin = FindWindow("TfrmDebug", "eMEDES DebugWin"); 
              if (hDebugWin) 
                {
                 // ok, DebugWin is running
                 lstrcpyn((char*)lpvMem+1, (const char *)csOut, SHARED_STRING_LEN-1);
                 (char)(((char *)lpvMem)[SHARED_STRING_LEN-1]) = 0;
                 ::SendMessage(hDebugWin, WM_USER+1, 0, 0);
                }
             } 
          }
        else
          LeaveCriticalSection(&csSl);
       }
    }
   //CloseHandle(hSem);
#endif
#endif
return 0;
}

void CDebug::Print(const char* sOut, const BOOL bAlwaysLog)
{
#ifdef _DEBUGWIN_ON

 try 
 {
#ifdef _DEBUGWIN_THREAD
    
    SYSTEMTIME sysTime;
    if (bKilled == FALSE)
      {
        GetSystemTime( &sysTime);
        CString csTicks = IntToStr( sysTime.wMinute) + ":" + IntToStr( sysTime.wSecond ) + "." + IntToStr( sysTime.wMilliseconds ) + 
                       ": " + CString(sOut);

        EnterCriticalSection(&csSl);
        slDebug.AddTail( csTicks );
        LeaveCriticalSection(&csSl);

        if (hSem != NULL)
          {
           if ((bInitialized == FALSE) && (pThread == NULL))
             {
              pThread = AfxBeginThread(MyControllingFunction, this);
              bInitialized = TRUE;
             }
           ReleaseSemaphore(hSem, 1, NULL);
          }
      }
#else

  ULONG ulTicks = GetTickCount();
  CString csTicks;
  SYSTEMTIME sysTime;

  if ((_DEBUGWIN_CMDLOG == FALSE) || (bAlwaysLog == TRUE))  
    {
     if ((lpvMem != NULL) && (sOut != NULL))
       {

        if (iDebugWinFindTry < 10)
          {
          if (!hDebugWin) hDebugWin = FindWindow("TfrmDebug", "eMEDES DebugWin"); 
          if (hDebugWin) 
            {
             // ok, DebugWin is running
             GetSystemTime( &sysTime);
             csTicks = IntToStr( sysTime.wMinute) + ":" + IntToStr( sysTime.wSecond ) + "." + IntToStr( sysTime.wMilliseconds ) + 
                       ": " + CString(sOut);
             lstrcpyn((char*)lpvMem+1, (const char *)csTicks, SHARED_STRING_LEN-1);
             (char)(((char *)lpvMem)[SHARED_STRING_LEN-1]) = 0;
             ::SendMessage(hDebugWin, WM_USER+1, 0, 0);
            }
          else
            {
             // not running
             iDebugWinFindTry++;
            }
          }
       }
    }

#endif
 }
 catch(char *pchMsg)
 {
    (pchMsg == NULL);   // dummy, just for removing the unreferenced warning
	 iDebugWinFindTry++;
 }
#endif

 return;
}
