//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by AXWingis.rc
//
#define IDS_AXWINGIS                    1
#define IDD_ABOUTBOX_AXWINGIS           1
#define IDB_AXWINGIS                    1
#define IDI_ABOUTDLL                    1
#define IDS_AXWINGIS_PPG                2
#define IDS_AXWINGIS_PPG_CAPTION        100
#define IDD_PROPPAGE_AXWINGIS           100
#define IDS_AXWINGIS_HELP               101
#define IDS_AXWINGIS_PPG_LIC            102
#define IDS_AXWINGIS_PPG_LIC_CAPTION    103
#define IDS_AXWINGIS_PROP               104
#define IDC_SERVER                      201
#define IDC_TOPIC                       202
#define IDC_USESEND                     203
#define IDI_XLINK                       203
#define IDC_ACTIVE                      204
#define IDB_MASK                        204
#define IDC_CHECK1                      205
#define IDC_DDEML                       205
#define IDC_COM                         205
#define IDB_XLINKOLD                    206
#define IDC_FINGERPRINT                 206
#define IDC_APPNAME                     206
#define IDB_XLINK                       207
#define IDB_XLINKDISABLED               208
#define IDB_BITMAP1                     211
#define IDD_PROPPAGE_AXWINGIS_LICENSE   212
#define IDC_LISTVIEW                    212
#define IDC_LICMSG                      213
#define IDD_ADD_LICENSE                 213
#define IDC_LC                          214
#define IDC_FP                          215
#define IDC_FP2                         216
#define IDC_BUTTON_ADD_LKEY             216
#define IDC_AXIPC                       218

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        217
#define _APS_NEXT_COMMAND_VALUE         32770
#define _APS_NEXT_CONTROL_VALUE         219
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
