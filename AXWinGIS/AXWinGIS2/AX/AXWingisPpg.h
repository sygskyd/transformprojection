// AXWingisPpg.h : Declaration of the CAXWingisPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CAXWingisPropPage : See AXWingisPpg.cpp.cpp for implementation.

class CAXWingisPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CAXWingisPropPage)
	DECLARE_OLECREATE_EX(CAXWingisPropPage)

// Constructor
public:
	CAXWingisPropPage();

// Dialog Data
	//{{AFX_DATA(CAXWingisPropPage)
	enum { IDD = IDD_PROPPAGE_AXWINGIS };
	CString	m_sServer;
	CString	m_sTopic;
    CString	m_sAppname;
	BOOL	m_bActive;
	BOOL	m_bUseSend;
	BOOL	m_DDEML;
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CAXWingisPropPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};
