; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CAXWingisCtrl
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "AXWingis.h"
CDK=Y

ClassCount=4
Class1=CAXWingisCtrl
Class2=CAXWingisPropPage

ResourceCount=7
Resource1=IDD_PROPPAGE_AXWINGIS_LICENSE (Neutral)
LastPage=0
Resource2=IDD_PROPPAGE_AXWINGIS (German (Austria))
Resource3=IDD_ADD_LICENSE (Neutral)
Resource4=IDD_PROPPAGE_AXWINGIS (Neutral)
Class3=CAXWingisPropPageLicense
Resource5=IDD_ABOUTBOX_AXWINGIS
Class4=CAddLKeyDlg
Resource6=IDD_ADD_LICENSE
Resource7=IDD_ABOUTBOX_AXWINGIS (Neutral)

[CLS:CAXWingisCtrl]
Type=0
HeaderFile=AXWingisCtl.h
ImplementationFile=AXWingisCtl.cpp
Filter=W
BaseClass=COleControl
VirtualFilter=wWC
LastObject=CAXWingisCtrl

[CLS:CAXWingisPropPage]
Type=0
HeaderFile=AXWingisPpg.h
ImplementationFile=AXWingisPpg.cpp
Filter=D
BaseClass=COlePropertyPage
VirtualFilter=idWC
LastObject=CAXWingisPropPage

[DLG:IDD_ABOUTBOX_AXWINGIS]
Type=1
Class=?
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_PROPPAGE_AXWINGIS (German (Austria))]
Type=1
ControlCount=9
Control1=IDC_SERVER,edit,1350631552
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDC_TOPIC,edit,1350631552
Control5=IDC_USESEND,button,1342243075
Control6=IDC_ACTIVE,button,1342242819
Control7=IDC_STATIC,static,1342177283
Control8=IDC_STATIC,button,1342177287
Control9=IDC_DDEML,button,1342242819
Class=CAXWingisPropPage

[DLG:IDD_PROPPAGE_AXWINGIS_LICENSE (Neutral)]
Type=1
Class=CAXWingisPropPageLicense
ControlCount=5
Control1=IDC_FINGERPRINT,edit,1484849280
Control2=65535,static,1342308352
Control3=IDC_LISTVIEW,SysListView32,1350631489
Control4=IDC_LICMSG,static,1342308352
Control5=IDC_BUTTON_ADD_LKEY,button,1342242816

[DLG:IDD_PROPPAGE_AXWINGIS (Neutral)]
Type=1
Class=CAXWingisPropPage
ControlCount=14
Control1=IDC_SERVER,edit,1350631552
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDC_TOPIC,edit,1350631552
Control5=IDC_USESEND,button,1073807619
Control6=IDC_ACTIVE,button,1342242819
Control7=IDC_STATIC,button,1342177287
Control8=IDC_COM,button,1342242819
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342177283
Control12=IDC_APPNAME,edit,1350631552
Control13=IDC_STATIC,static,1342308352
Control14=IDC_STATIC,static,1342308352

[CLS:CAXWingisPropPageLicense]
Type=0
HeaderFile=AXWingisPropPageLicense.h
ImplementationFile=AXWingisPropPageLicense.cpp
BaseClass=COlePropertyPage
Filter=D
LastObject=65535
VirtualFilter=idWC

[DLG:IDD_ADD_LICENSE]
Type=1
Class=CAddLKeyDlg
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_LC,edit,1350631552
Control4=IDC_STATIC,static,1342308352
Control5=IDC_FP,static,1342308352
Control6=IDC_FP2,static,1342308352

[CLS:CAddLKeyDlg]
Type=0
HeaderFile=AddLKeyDlg.h
ImplementationFile=AddLKeyDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CAddLKeyDlg

[DLG:IDD_ADD_LICENSE (Neutral)]
Type=1
Class=CAddLKeyDlg
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_LC,edit,1350631552
Control4=IDC_STATIC,static,1342308352
Control5=IDC_FP,static,1342308352
Control6=IDC_FP2,static,1342308352

[DLG:IDD_ABOUTBOX_AXWINGIS (Neutral)]
Type=1
Class=?
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

