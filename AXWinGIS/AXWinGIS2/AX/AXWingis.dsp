# Microsoft Developer Studio Project File - Name="AXWingis" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=AXWingis - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "AXWingis.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "AXWingis.mak" CFG="AXWingis - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "AXWingis - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "AXWingis - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "AXWingis - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ".\Release"
# PROP BASE Intermediate_Dir ".\Release"
# PROP BASE Target_Ext "ocx"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\bin\Release"
# PROP Intermediate_Dir ".\Release"
# PROP Target_Ext "ocx"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /c
# ADD CPP /nologo /G5 /MT /W3 /Gi /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_USRDLL" /D "_MBCS" /D "_WINDLL" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0xc07 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0xc07 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 /nologo /subsystem:windows /dll /incremental:yes /machine:I386 /out:"..\bin\Release/AXWingis2.ocx" /OPT:REF
# SUBTRACT LINK32 /pdb:none
# Begin Custom Build - Registering OLE control...
OutDir=.\..\bin\Release
TargetPath=\work\AXWingis2\bin\Release\AXWingis2.ocx
InputPath=\work\AXWingis2\bin\Release\AXWingis2.ocx
SOURCE="$(InputPath)"

"$(OutDir)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	regsvr32 /s /c "$(TargetPath)" 
	echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg" 
	
# End Custom Build

!ELSEIF  "$(CFG)" == "AXWingis - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ".\Debug"
# PROP BASE Intermediate_Dir ".\Debug"
# PROP BASE Target_Ext "ocx"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\bin\Debug"
# PROP Intermediate_Dir ".\Debug"
# PROP Target_Ext "ocx"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /c
# ADD CPP /nologo /MTd /W3 /GR /GX /Z7 /Od /D "WIN32" /D "_WINDOWS" /D "_USRDLL" /D "_MBCS" /D "_WINDLL" /U "_UNICODE" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0xc07 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0xc07 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /debug /machine:I386
# ADD LINK32 /nologo /subsystem:windows /dll /pdb:none /debug /machine:I386 /force /out:"..\bin\Debug/AXWingis2.ocx"
# Begin Custom Build - Registering OLE control...
OutDir=.\..\bin\Debug
TargetPath=\work\AXWingis2\bin\Debug\AXWingis2.ocx
InputPath=\work\AXWingis2\bin\Debug\AXWingis2.ocx
SOURCE="$(InputPath)"

"$(OutDir)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	regsvr32 /s /c "$(TargetPath)" 
	echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg" 
	
# End Custom Build

!ENDIF 

# Begin Target

# Name "AXWingis - Win32 Release"
# Name "AXWingis - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;hpj;bat;for;f90"
# Begin Source File

SOURCE=.\AddLKeyDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\axipc.cpp
# End Source File
# Begin Source File

SOURCE=.\AXWingis.cpp
# End Source File
# Begin Source File

SOURCE=.\AXWingis.def
# End Source File
# Begin Source File

SOURCE=.\AXWingis.odl
# End Source File
# Begin Source File

SOURCE=.\AXWingis.rc
# End Source File
# Begin Source File

SOURCE=.\AXWingisCtl.cpp
# End Source File
# Begin Source File

SOURCE=.\AXWingisPpg.cpp
# End Source File
# Begin Source File

SOURCE=.\AXWingisPropPageLicense.cpp
# End Source File
# Begin Source File

SOURCE=.\DebugWin.cpp
# End Source File
# Begin Source File

SOURCE=.\eMedes_C_tools.cpp
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# Begin Source File

SOURCE=.\AddLKeyDlg.h
# End Source File
# Begin Source File

SOURCE=.\axipc.h
# End Source File
# Begin Source File

SOURCE=.\axipc_error.h
# End Source File
# Begin Source File

SOURCE=.\AXWingis.h
# End Source File
# Begin Source File

SOURCE=.\AXWingisCtl.h
# End Source File
# Begin Source File

SOURCE=.\AXWingisPpg.h
# End Source File
# Begin Source File

SOURCE=.\AXWingisPropPageLicense.h
# End Source File
# Begin Source File

SOURCE=.\DebugWin.h
# End Source File
# Begin Source File

SOURCE=.\eMedes_C_tools.h
# End Source File
# Begin Source File

SOURCE=.\interface_version.h
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\AXWingis.ico
# End Source File
# Begin Source File

SOURCE=.\AXWingisCtl.bmp
# End Source File
# Begin Source File

SOURCE=.\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\bitmap2.bmp
# End Source File
# Begin Source File

SOURCE=.\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\Icon1.ico
# End Source File
# Begin Source File

SOURCE=.\Mask.bmp
# End Source File
# Begin Source File

SOURCE=.\XLink1.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\Doku\AXHelp.hh
# End Source File
# End Target
# End Project
# Section AXWingis : {72ADFD62-2C39-11D0-9903-00A0C91BC942}
# 	2:27:ActiveX Control Containment:1
# End Section
# Section AXWingis : {044B6806-C387-11D3-8FA9-0000B4BDFE28}
# 	2:21:DefaultSinkHeaderFile:axipc.h
# 	2:16:DefaultSinkClass:CAXIpc
# End Section
# Section AXWingis : {044B6804-C387-11D3-8FA9-0000B4BDFE28}
# 	2:5:Class:CAXIpc
# 	2:10:HeaderFile:axipc.h
# 	2:8:ImplFile:axipc.cpp
# End Section
