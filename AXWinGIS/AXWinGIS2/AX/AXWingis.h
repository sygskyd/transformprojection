// AXWingis.h : main header file for AXWINGIS.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CAXWingisApp : See AXWingis.cpp for implementation.

class CAXWingisApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();

virtual void WinHelp( DWORD dwData, UINT nCmd = HELP_CONTEXT );
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;


