// AXWingisCtl.h : Declaration of the CAXWingisCtrl OLE control class.

/////////////////////////////////////////////////////////////////////////////
// CAXWingisCtrl : See AXWingisCtl.cpp for implementation.

#include <ddeml.h>
#include "DebugWin.h"
#include "axipc.h"
#include "axipc_error.h"

#define LIF_NONE  0
#define LIF_NAME  1
#define LIF_INDEX 2

#define SLIF_NONE  "None"
#define SLIF_NAME  "Text"
#define SLIF_INDEX "Index"

#define dde_AppReturnCode 0x00FF
#define dde_Busy          0x4000
#define dde_Ack           0x8000


#define NO_MSG 0
#define NO_ACK 0
#define DDE_ACK_NONE 0

#define DDE_ACK_INIT 1
#define DDE_ACK_EXEC 2 
#define DDE_ACK_TERM 3 



//********************************************************

class CAXWingisCtrl : public COleControl
{
	DECLARE_DYNCREATE(CAXWingisCtrl)

// Constructor
public:
	CAXWingisCtrl();
    

    // Events received from DDEWnd    
    void GotDDEExecute(const char * sCmd);
    void GotDDEInitiate();
    void GotDDETerminate();
    
    // Private message handlers
    LRESULT OnCmdAvailable(WPARAM wParam, LPARAM lParam);
    LRESULT OnPostedSendToWinGIS(WPARAM wParam, LPARAM lParam);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAXWingisCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CAXWingisCtrl();

	//DECLARE_OLECREATE_EX(CAXWingisCtrl)    // Class factory and guid  (without licensing)

    BEGIN_OLEFACTORY(CAXWingisCtrl)        // Class factory and guid
    virtual BOOL VerifyUserLicense();
    virtual BOOL VerifyLicenseKey(BSTR bstrKey);
    virtual BOOL GetLicenseKey(DWORD, BSTR FAR*);
    END_OLEFACTORY(CAXWingisCtrl)  

	 DECLARE_OLETYPELIB(CAXWingisCtrl)      // GetTypeInfo
	 DECLARE_PROPPAGEIDS(CAXWingisCtrl)     // Property page IDs
	 DECLARE_OLECTLTYPE(CAXWingisCtrl)		// Type name and misc status

    // DebugWin object
    CDebug Debug;

    //CRITICAL_SECTION csFireEvents;
    CStringList slCmdQueue;
    CStringList slDelayedCmdQueue;
    BOOL bInside;
    BOOL bInsideDelayedSend;
    CString csLastOutputCmd;

	 // **** DDEML members ****
    DWORD idInst;                         // DDEML Instance ID
    HCONV hcGis;                          // Conversion handle
    HSZ hsServer;                         // string handle server
    HSZ hsTopic;                          // string handle topic
    HSZ hsGIS;
    HSZ hsSystem;
    DWORD dwTransactionId;       
    long lAckFlag;
    CString csModuleName;

    BOOL bConnectionByAX;

    long m_lConnectTimeout;               // how long to wait for a connect ack
                                          // this value is read from the registry
                                          // HKLM/Software/Progis/AXWingis/ConnectTimeout
    long m_lSOKTimeout;                   // how long to wait for a [SOK][1]
                                          // this value is read from the registry
                                          // HKLM/Software/Progis/AXWingis/SOKTimeout

    void SetTimeoutValues();              // fill the 2 vars above from the registry

    CWinApp* theApp;
    CString csDelayedCmd;
    // Timer
    long lTimerIntervall;
    BOOL bTimerActive;

    // Implementation
    BOOL InitDDEML();
    BOOL ConnectToWinGIS();
    BOOL SendToWinGIS(const char *pData);

    // COM Declares
    BOOL InitIPC();
    BOOL ConnectIPC();    
    BOOL SendIPC(CString sCmd, BOOL bWait = FALSE);
    BOOL DisconnectIPC();

    CAXIpc axipc;

    void PreProcessMsg(CString csCmd);

    long m_lSessionID;
    BOOL bAssignedIpc;

    long SetErrorCode(long lError);
    long lLastError;


	// packing lists
	long OPLsID;
	long OPLdArea; 
	long OPLdLength; 
	long OPLdX; 
	long OPLdY; 
	long OPLsLayer; 
	long OPLsParam1; 
	long OPLsParam2; 
	long OPLsParam3;
	bool bGetObjectPropertiesList;
	long lOBPItems;
	 
    long lProgisIDs;

public:
    HDDEDATA CALLBACK DdeCallbackCtrl(UINT uType, UINT uFmt, HCONV hconv, 
                                  HSZ hsz1, HSZ hsz2, HDDEDATA hData, 
                                  DWORD dwData1, DWORD dwData2);
    // **** DDEML members END ****
protected:

	 CBitmap bmXlink;
    CBitmap bmXlinkDisabled;
    CBitmap bmMask;
    BITMAP bmInfo;
    BOOL bRecreateWindow;

    CObArray aBase;        
    CString csLastEvent;  // Token of last event comming in
    long hReturnList;     // list-handle for waiting commands
    CString csReturnString;// string for waiting commands
  
    // Internal managment functions
    long BuildCmdBlockFromList(long hList);
    long BuildCmdBlockFromListWithoutSpaces(long hList);
    CString BuildStringFromListEx(long hList, long lStart, long lCount);
    CString BuildStringFromList(long hList);
    long BuildListFromString(CString csCmd, int Start);
    long BuildListFromSubString(CString csCmd, int iStart);
    BOOL BuildDoubleListFromTokenString(CString csCmd, int Start, long& hList1, long& hList2);
    BOOL BuildDoubleListFromString(CString csCmd, int Start, long& hList1, long& hList2);
    BOOL AddListFromString(CString csCmd, int Start, long &hList);
    CString ConvertListToString(long hList);
    bool CompareListItems(const char * cs1, const char * cs2, BOOL bAsc);
    CString ModuleFileName();

    BOOL CheckLicense();
    void ClearLicenseList();

    //BOOL WaitForReply(CString csEvent);
    BOOL WaitForReply(const char* csEvent);
    //BOOL WaitForReplyTimeout(CString csEvent, long lTimeOut);
    BOOL WaitForReplyTimeout(const char* csEvent, long lTimeOut);
// ++ Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#22 25.09.00 
    //BOOL WaitForReplyWithCond(CString csEvent, CString csRetStrValue);
    BOOL WaitForReplyWithCond(const char* csEvent, const char* csRetStrValue);
// -- Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#22 25.09.00
    BOOL WaitForSendOk(long lTimeOut);

    CString GetLayerToken();
    int SetLayerToken(CString sCmd);
    //CString GetSubString (const char *CmdStr, int pos, char cSep);
    //int GetSubStringCount(const char *CmdStr, char cSep);
    // now in eMedes_C_tools !
    
    int GetTokenCount (const char *CmdStr);
    char* GetCmdToken (const char *CmdStr, int pos);

    void DumpList(long hList);
    long FindString(long hList, CString csSearch);

    BOOL SendChangeLayerSettings(CString sLayerName, CString sNewName, long lOnOff, 
                                 long lPosition, long lGenOnOff, double dGenMin, double dGenMax, 
                                 long lFixOnOff, long lLineStyle, long lLineWidth, 
                                 long lLineColor, long lPattern, long lPatternColor, 
                                 long lTransparent);

    
	// Subclassed control support
	BOOL PreCreateWindow(CREATESTRUCT& cs);
	BOOL IsSubclassedControl();
	LRESULT OnOcmCommand(WPARAM wParam, LPARAM lParam);

// Message maps
	//{{AFX_MSG(CAXWingisCtrl)
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnConnectAXIpc(long lSessionID, LPCTSTR sRemoteName);
	afx_msg void OnDisconnectAXIpc(long lSessionID);
	afx_msg void OnReceiveAXIpc(LPCTSTR sCmd, long sSessionID);
  afx_msg void OnAckAXIpc(long lTransID);
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
// Dispatch maps
	//{{AFX_DISPATCH(CAXWingisCtrl)
	CString m_server;
	afx_msg void OnServerChanged();
	CString m_topic;
	afx_msg void OnTopicChanged();
	BOOL m_useSend;
	afx_msg void OnUseSendChanged();
	CString m_layerName;
	afx_msg void OnLayerNameChanged();
	BOOL m_useCOM;
	afx_msg void OnUseCOMChanged();
	afx_msg BOOL GetActive();
	afx_msg void SetActive(BOOL bNewValue);
	afx_msg BOOL GetConnected();
	afx_msg void SetConnected(BOOL bNewValue);
	afx_msg BOOL GetReady();
	afx_msg long GetLayerNameList();
	afx_msg long GetLayerIndexList();
	afx_msg BSTR GetLayerInfo();
	afx_msg void SetLayerInfo(LPCTSTR lpszNewValue);
	afx_msg long GetLayerIndex();
	afx_msg void SetLayerIndex(long nNewValue);
	afx_msg long GetViewList();
	afx_msg BSTR GetAppName();
	afx_msg void SetAppName(LPCTSTR lpszNewValue);
	afx_msg BSTR GetFingerPrint();
	afx_msg void SetFingerPrint(LPCTSTR lpszNewValue);
	afx_msg BOOL DDEExecute(LPCTSTR pCmd);
	afx_msg long CreateList();
	afx_msg BOOL AddStringParam(long hList, LPCTSTR Value);
	afx_msg BSTR GetStringParam(long hList, long Index);
	afx_msg BOOL DeleteList(long hList);
	afx_msg BOOL IsValidList(long hList);
	afx_msg BOOL ClearList(long hList);
	afx_msg long GetListSize(long hList);
	afx_msg BOOL DoSelectObjects(long hProgisIDList, BOOL bAutoCommit);
	afx_msg BOOL DoCloseGIS();
	afx_msg BOOL DoShowGIS();
	afx_msg BOOL DoZoomObjects(long hProgisIDList, BOOL bAutoCommit);
	afx_msg BOOL DoDeselectAll();
	afx_msg BOOL DoSetView(LPCTSTR sView);
	afx_msg BOOL DoSetCaption(LPCTSTR sProgisID, long hColNameList, long hCaptionList, BOOL bAutoCommit);
	afx_msg long DoSearchObjects(long hProgisIDList, BOOL bAutoCommit);
	afx_msg BOOL DoOpenProject(LPCTSTR sProjectName);
	afx_msg BOOL DoCloseProject(LPCTSTR sProjectName);
	afx_msg BSTR DoSetActiveLayer(long lMode, LPCTSTR sLayer);
	afx_msg BSTR DoGetActiveLayer();
	afx_msg long CreateListEx(long hList);
	afx_msg BOOL DoCreateChart(LPCTSTR sProgisID, long hListCOLs, long hListValues, BOOL bAutoCommit);
	afx_msg BOOL DoDeleteObjects(long hProgisIDList, BOOL bAutoCommit);
	afx_msg BOOL DoSaveProject();
	afx_msg BOOL DoSetSelectionMode(long lMode);
	afx_msg BOOL DoSetText(LPCTSTR sText);
	afx_msg BOOL DoBeginMacro();
	afx_msg BOOL DoEndMacro();
	afx_msg BOOL DoCreateLayerDialog();
	afx_msg BOOL DoDeleteLayerDialog();
	afx_msg BOOL DoSelectObjectsD(long hProgisIDList, BOOL bAutoCommit);
	afx_msg BOOL DoZoomObjectsD(long hProgisIDList, BOOL bAutoCommit);
	afx_msg BSTR DoCreateTextEx(double dX, double dY, LPCTSTR sFontName, long lFontStyle, long lHeight, long lArc, LPCTSTR sText);
	afx_msg BOOL DoGetLayerSettings();
	afx_msg BOOL DoFindLayerByName(LPCTSTR sName);
	afx_msg BOOL DoFindLayerByIndex(long lIndex);
	afx_msg BOOL DoDeleteLayerByName(LPCTSTR sName);
	afx_msg BOOL DoDeleteLayerByIndex(long lIndex);
	afx_msg BSTR DoLoadBitmap(LPCTSTR sFileName, double dP1X, double dP1Y, double dP2X, double dP2Y, BOOL bShow);
   afx_msg BOOL DoChangeLayerSettingsEx(LPCTSTR sLayerName, LPCTSTR sNewName, long lOnOff, long lPosition, long lGenOnOff, double dGenMin, double dGenMax, long lFixOnOff, long lLineStyle, long lLineWidth, long lLineColor, long lPattern, long lPatternColor, long lTransparent);
	afx_msg BOOL DoChangeLayerSettingsByIndex(long lLayerIndex, LPCTSTR sNewName, BOOL bOn, long lPosition, BOOL bGenOn, double dGenMin, double dGenMax, BOOL bFixOn, long lLineStyle, long lLineWidth, long lLineColor, long lPattern, long lPatternColor, BOOL bTransparent);
	afx_msg BOOL DoCopyObjects(LPCTSTR sTargetLayer, long hProgisIDList, BOOL bAutoCommit);
	afx_msg BSTR DoCreateObject(LPCTSTR sObjectType, long lSymbolNumber, double dX, double dY, LPCTSTR sLayer, double dSize, long lTimeOut, BOOL bAutoCommit);
	afx_msg BOOL DoGetNextUnusedID(BOOL bWait, long lTimeout);
	afx_msg BOOL DoSendGPSPosition(LPCTSTR ID, LPCTSTR sLatitude, LPCTSTR sLongitude, double dAltitude, LPCTSTR sType, BOOL bFixed, double dQuality, LPCTSTR sDate, LPCTSTR sTime, long lSats, BOOL bOffline, BOOL bAlarm, long lObjecttype, long lObjNr, long lAddInfoCtr, LPCTSTR sAddInfo);
	afx_msg BOOL DoCreateLayerEx(LPCTSTR sNewLayer, long lOnOff, long lPosition, long lGenOnOff, double dGenMin, double dGenMax, long lFixOnOff, long lLineStyle, long lLineWidth, long lLineColor, long lPattern, long lPatternColor, long lTransparent);                                 
	afx_msg BOOL DoDisableScreenUpdate();
	afx_msg BOOL DoSetScreenUpdate(BOOL bEnabled);
	afx_msg BOOL DoReloadProject(LPCTSTR sProject);
	afx_msg BOOL DoChangeBitmapSettings(BOOL bBorderOnly, BOOL bTransBlack, long lTransparency);
	afx_msg BOOL DoSetOptionSettings(BOOL bCartesian, BOOL bShowCorners, BOOL bMakeBackups, BOOL bAutoSave, long lAutoSaveTime);
	afx_msg BOOL DoChangeObjectStyleEx(LPCTSTR sProgisID, long lObjectColor, long lLineStyle, long lLineWidth, long lPatternColor, long lPattern, long lTransparent, long lSymbolSize, long lVisible, BOOL bAutoCommit);
	afx_msg long DoGetNextSymbolNumber(long lLastNumber);
	afx_msg BOOL DoSetWindowPos(long lScreenWidth, long lScreenHeight, long lDBWinWidth, long lDBWinHeight, long lPos);
	afx_msg BOOL DoSetZoom(double dFactor);
	afx_msg BOOL DoNotifyTableChange();
	afx_msg BOOL DoInsertObjects(long hProgisIDList, BOOL bAutoCommit);
	afx_msg BOOL DoAsciiExport(LPCTSTR sProgisID, long hValueList);
	afx_msg BOOL DoAsciiImport(LPCTSTR sAsciiFilename, LPCTSTR sADFFilename, LPCTSTR sSymbolFilename, LPCTSTR sFontName, long lFontStyle, long lFontHeight, LPCTSTR sTextLayer, LPCTSTR sObjectLayer, double dXOffset, double dYOffset, double dScaleBy);
	afx_msg BOOL DoNotifyImportError(long hColumnNameList);
	afx_msg BOOL DoTempSetText(LPCTSTR sProgisID, LPCTSTR sText);
	afx_msg BOOL DoZoomAndSetText(long hProgisIDList, long hTextList);
	afx_msg BOOL WaitForReadyMsg(long lTimeout);
	afx_msg BOOL DoMoveObject(LPCTSTR sProgisID, double dRelX, double dRelY, BOOL bAutoCommit);
	afx_msg BOOL DoSetObjectPos(LPCTSTR sProgisID, double dAbsX, double dAbsY, BOOL bAutoCommit);
	afx_msg BOOL DoGetViewSettings();
	afx_msg BOOL AlignWindow(long hWnd, short sPos);
	afx_msg BOOL ShowWindow(long hWnd, BOOL bShow);
	afx_msg BOOL SetTimerIntervall(long lMilliseconds);
	afx_msg BOOL SetTimerState(BOOL bEnabled);
	afx_msg BOOL DoCommit();
	afx_msg long DoPingWinGIS();
	afx_msg BOOL DoConnect(LPCTSTR sConnectionName, LPCTSTR sDBLayerInfo);
	afx_msg BOOL DoConnectEx(LPCTSTR sConnectionName, LPCTSTR sDBLayerInfo, LPCTSTR sParamBlock);
	afx_msg BSTR DoCreateText(double dAbsx, double dAbsY, long lHeight, LPCTSTR sText);
	afx_msg BOOL DoChangeLayerSettings(LPCTSTR sLayerName, LPCTSTR sNewName, long lOnOff, long lPosition, long lPattern, long lPatternColor);
	afx_msg BOOL DoCreateLayer(LPCTSTR sNewName, long lOnOff, long lPosition, long lPattern, long lPatternColor);
	afx_msg BOOL DoChangeObjectStyle(LPCTSTR sProgisID, long lObjectColor, long lPatternColor, long lPattern, BOOL bAutoCommit);
	afx_msg BOOL DoDisconnect();
	afx_msg long DoGetLoadedProjects();
	afx_msg long DoPrint(long lMode, LPCTSTR sFileName, long lCopies, BOOL bCollateCopies, BOOL bShowProgress, BOOL bPrintMonitoring);
	afx_msg BOOL DoProjectSaveAs(LPCTSTR sProjectName, BOOL bOverwrite, BOOL bLTProject, LPCTSTR sPassword);
	afx_msg long DoSelectAndSearchEx(long lType, long lDrawSel, LPCTSTR sObjectID, LPCTSTR sObjectLayer, BOOL bSelFirstObj, long lSelObjectType, LPCTSTR sSelObjectLayer, double dSelBorders1, double dSelBorders2, double dSelBorders3, double dSelBorders4, BOOL bCutInside, double dOffset, long lEdges, long lLineEnds, bool bDrawSelLine, LPCTSTR sSelLineLayer, bool bZoom, bool bDeSelect, long lSelAddSubXOr);
	afx_msg BOOL DoSetGraphicMode(long lMode, BOOL bActivate);
	afx_msg BOOL DoSetSelectionOnGenerate(BOOL bOn);
	afx_msg BOOL DoCalcRoute(double dBeginX, double dBeginY, double dEndX, double dEndY);
	afx_msg BOOL DoActivateProject(LPCTSTR sProjectName);
	afx_msg BOOL DoSetVisibleMapArea(double dLLX, double dLLY, double dURX, double dURY);
	afx_msg BSTR GetModuleFileName();
	afx_msg void SetModuleFileName(LPCTSTR sFile);
	afx_msg long GetLicenseDays(BOOL bMustBeRuntime, BOOL bMustBeAdvanced);
	afx_msg BOOL AddLicense(LPCTSTR sLicenseKey);
	afx_msg BSTR GetLastErrorMsg();
	afx_msg BOOL DoGetObjectProperties(long lObjectType, long hObjectList);
	afx_msg BOOL WaitForMacroEnd(long lTimeOut);
	afx_msg BOOL DoSetActiveSymbol(long lSymbol);
	afx_msg BOOL DoSupressDialog(BOOL bSupress);
	afx_msg BOOL DoSetTextStyle(LPCTSTR sFontName, long lFontStyle, long lFontHeight, double dArc, long lColor, long lPos, LPCTSTR sLayer);
	afx_msg BOOL DoSetChartStyle(long lChartType, double dWidth, double dHeight, double dRadius, BOOL bRelative, LPCTSTR sFontName, long lFontStyle, long lFontHeight, double dArc, long lColor);
	afx_msg BOOL DoSetChartColor(long hColorList);
	afx_msg BOOL DoGetChartColor();
	afx_msg BOOL DoRequestSymbolInfo(BOOL bWaitForEvent);
	afx_msg BOOL DoRequestLineStyleInfo();
	afx_msg BOOL DoRequestHatchStyleInfo();
	afx_msg long DoGetSelectionState(long hProgisIDList);
	afx_msg BOOL DoSetNewObjectSelection(BOOL bSelect);
	afx_msg BSTR GetWinGisRelease();
	afx_msg BOOL GPSDelete(LPCTSTR sID);
	afx_msg BOOL GPSDbColumn(long hColumnNameList, long hColumnTypeList, long hColumnLengthList, long hPositionList);
	afx_msg BOOL GPSNew(LPCTSTR sID, long lObjectType, long lDisplayType, BOOL bAutoZoom, LPCTSTR sLayerName, long lSymbolindexStand, long lSymbolindexUp, long lSymbolindexDown, long lSymbolindexLeft, long lSymbolindexRight);
	afx_msg BOOL GPSOffset(double dOffset);
	afx_msg BOOL GPSProAsc(LPCTSTR sSourceAscFile, LPCTSTR sSourceAdfFile, LPCTSTR sDestAscFile);
	afx_msg BOOL GPSProCalc(double dX, double dY);
	afx_msg BOOL GPSShow(LPCTSTR sID);
	afx_msg BOOL GPSSymSet();
	afx_msg BOOL GPSUpdate(LPCTSTR sID, long lObjectType, long lDisplayType, BOOL bAutoZoom, LPCTSTR sLayerName, long lSymbolindexStand, long lSymbolindexUp, long lSymbolindexDown, long lSymbolindexLeft, long lSymbolindexRight);
	afx_msg BOOL GPSLoadGrf(LPCTSTR sFileName);
	afx_msg BOOL DoClearLayer(LPCTSTR sLayer, BOOL bSendDeleteEvents);
	afx_msg BOOL DoDenyWingisSession(LPCTSTR sSessionName, BOOL bDeny);
	afx_msg BOOL DoPolygonPartition(LPCTSTR sProgisID, long lPartMode, long lSizeMode, double dWantedArea, double dWantedDist, double WantedAngle, BOOL bFillPoly, LPCTSTR sDestLayer, LPCTSTR sName);
	afx_msg BOOL DoShowAllObjects();
	afx_msg BOOL DoTellProjectPart();
	afx_msg BOOL DoPolygonOverlay(LPCTSTR sSourceLayer1, LPCTSTR sSourceLayer2, LPCTSTR sTargetLayer, BOOL bSubtractive, BOOL bCheckAreas, BOOL bDBOverlay, BOOL bCombineAreas);
	afx_msg BOOL DoRedraw();
	afx_msg BOOL DeleteStringParam(long hList, long Index);
	afx_msg BOOL DoModifyView(LPCTSTR sName, double dLLX, double dLLY, double dURX, double dURY, long hXLayerList, long hIDList);
	afx_msg void SortList(long hList, BOOL bAsc);
	afx_msg double CStrToDouble(LPCTSTR sValue);
	afx_msg BSTR CDoubleToStr(double dValue);
	afx_msg void SortListEx(long hSortList, long hAssociatedList1, long hAssociatedList2, long hAssociatedList3, long hAssociatedList4, long hAssociatedList5, long hAssociatedList6, long hAssociatedList7, long hAssociatedList8);
	afx_msg long DoGetCoordinateSystem();
	afx_msg BOOL DoGetProjectSettings();
	afx_msg BSTR DoCreatePolyline(LPCTSTR sLayerName, long hCoordList);
	afx_msg long DoPrintSetPrintParam(LPCTSTR sDeviceName,  LPCTSTR sPaperFormat, long lOrientation);
	afx_msg long DoPrintSetPagesParam(long lLayoutModel, BOOL bAutoRepagination, long lHorizontalPages, long lVerticalPages, double dMarginsLeft, double dMarginsTop, double dMarginsRight, double dMarginsBottom, double dInnerLeft, double dInnerTop, double dInnerRight, double dInnerBottom, double dOverlapLeft, double dOverlapTop, double dOverlapRight, double dOverlapBottom, BOOL bNumberingOrder, long lNumberingFrom);
	afx_msg long DoPrintSetMapParam(long lMapID, long lRegion, LPCTSTR sViewName, double dRegionX1, double dRegionY1, double dRegionX2, double dRegionY2, BOOL bFitToPage, double PrintScale, BOOL bKeepScale, double dFrameWidth, double dFrameHeight, long lFrameCentering, double FrameLeft, double FrameTop);
	afx_msg long DoPrintSetLegendParam(long lLegendID, double dFrameWidth, double dFrameHeight, long lFrameCentering, long lFrameOridgin, double FrameLeft, double FrameTopOrBot);
	afx_msg long DoPrintSetPictureParam(long lPictureID, long lPictureLocation, long lFrameSizeInterpret, double dFrameWidth, double dFrameHeight, long lFrameCentering, long lFrameOridgin, double FrameLeft, double FrameTopOrBot);
	afx_msg long DoPrintSetSignatureParam(long lSignatureID, LPCTSTR sSignature, LPCTSTR sFontName, BOOL bItalic, BOOL bBold, BOOL bUnderlined, BOOL bTransparent, long lFontSize, long lFontColor, long lFontBackColor, long lTextAlignment, BOOL bOnPagesLocation, BOOL bOnFirstPage, BOOL bOnInternalPages, BOOL bOnLastPage, long lFrameCentering, long lFrameOridgin, double FrameLeft, double FrameTopOrBot);
	afx_msg long DoPrintGetPrintParam();
	afx_msg long DoPrintGetPagesParam();
	afx_msg long DoPrintGetMapParam(long lMapID);
	afx_msg long DoPrintGetLegendParam(long lLegendID);
	afx_msg long DoPrintGetPictureParam(long lPictureID);
	afx_msg long DoPrintGetSignatureParam(long lSignatureID);
	afx_msg long DoPrintInsertLegend(LPCTSTR sLegendName, double dFrameWidth, double dFrameHeight, long lFrameCentering, long lFrameOridgin, double FrameLeft, double FrameTopOrBot);
	afx_msg long DoPrintInsertPicture(LPCTSTR sPictureID, long lPictureLocation, long lFrameSizeInterpret, double dFrameWidth, double dFrameHeight, long lFrameCentering, long lFrameOridgin, double FrameLeft, double FrameTopOrBot);
	afx_msg long DoPrintInsertSignature(LPCTSTR sSignature, LPCTSTR sFontName, BOOL bItalic, BOOL bBold, BOOL bUnderlined, BOOL bTransparent, long lFontSize, long lFontColor, long lFontBackColor, long lTextAlignment, BOOL bOnPagesLocation, BOOL bOnFirstPage, BOOL bOnInternalPages, BOOL bOnLastPage, long lFrameCentering, long lFrameOridgin, double FrameLeft, double FrameTopOrBot);
	afx_msg long DoPrintStoreTemplate(LPCTSTR sFileName, long lOptions);
	afx_msg long DoPrintLoadTemplate(LPCTSTR sFileName);
	afx_msg long DoPrintGetObjectsInform();
	afx_msg long DoPrintDeleteObject(long lObjectType, long lObjectID);
	afx_msg long DoPrintMoveObject(long lObjectType, long lObjectID, long lOptions);
	afx_msg BOOL GPSProSet(LPCTSTR sSourceProj, LPCTSTR sSourceDate, LPCTSTR sDestinationProj, LPCTSTR sDestinationDate);
	afx_msg BOOL DoGetPolygonData(long lID);
	afx_msg BOOL DoExecuteMenuFunction(LPCTSTR sFunctionName);
  afx_msg BOOL DoGetCoordinates();  
	afx_msg BOOL DoSendMouseAction(double dX, double dY, long lAction);
	afx_msg BOOL DoCommitDigitalization();
	afx_msg BOOL DoGenerateObjects(long hSourceLayerList, LPCTSTR sDestLayerName, long lMode);
	afx_msg BOOL DoCutSelectedPolygons(LPCTSTR sDestLayerName, long lMode);
	afx_msg BOOL DoShowAllOnLayer(LPCTSTR sLayerName);
	afx_msg BOOL DoCreateSymbol(LPCTSTR sSymName, LPCTSTR sSymLibrary, double dX, double dY, double dAngle, double dSize);
	afx_msg BOOL DoTrimLine(LPCTSTR sProgisId, double dLength, long lMode);
	afx_msg BOOL DoCreateBuffer(LPCTSTR sProgisId, LPCTSTR sDestLayerName, long lCornerMode, long lBufferMode, double dDistance);
	afx_msg BOOL DoUndo(long lStepCount);
	afx_msg BOOL AddListItemString(long hList, LPCTSTR sValue);
	afx_msg BOOL AddListItemLong(long hList, long lValue);
	afx_msg BOOL AddListItemDouble(long hList, double dValue);
	afx_msg BSTR GetListItemString(long hList, long lIndex);
	afx_msg long GetListItemLong(long hList, long lIndex);
	afx_msg double GetListItemDouble(long hList, long lIndex);
	afx_msg BOOL DeleteListItem(long hList, long lIndex);
  afx_msg BOOL DoSetPicture(LPCTSTR sLayer, double x1, double y1, double x2, double y2, long lLoRu, LPCTSTR sPath, long lOptions);
  afx_msg BOOL DoCreateObjectEx(LPCTSTR sObjectType, long lSymbolNumber, double x1, double y1, long lUserId, LPCTSTR sLayer, double dSize, BOOL bAutoCommit);
	afx_msg long DoReversePolyLine(LPCTSTR sProgisID);
	afx_msg BOOL DoSetTooltipText(LPCTSTR sProgisID, LPCTSTR sText);
	afx_msg BOOL DoCreateView(LPCTSTR sViewName, BOOL bSaveRegion, BOOL bSaveLayers, BOOL bSetDefault);
	afx_msg BOOL DoDeleteView(LPCTSTR sViewName);
	afx_msg BOOL DoChangeImageSettings(LPCTSTR sProgisID, long lDisplayMode, long lTransparencyMode, long lTransparency);
	afx_msg BOOL DoSetMapCanvas(long hWnd, long lMode);
	afx_msg BOOL DoDeleteLayerList(long hLayerList);
	afx_msg BOOL DoSetLayerSnap(LPCTSTR sLayerName, BOOL bOnOff);	
	afx_msg BOOL DoSetMouseZoom(BOOL bOnOff);
	afx_msg BOOL DoSelectSymbol();
	afx_msg BOOL DoSetSnapRadius(double dRadius);
	afx_msg BOOL DoSetActiveSymbolEx(LPCTSTR sSymbolNumber, LPCTSTR sSymbolName, LPCTSTR sSymLibrary);
	afx_msg long DoCombinePolygons(LPCTSTR sDestLayerName, long hProgisIDList);
	afx_msg BOOL DoSetAreaDivider(long Divider);
	afx_msg BOOL DoSetDiagramColor(long iIndex, LPCTSTR sColor);
	afx_msg BOOL DoRedo(long lStepCount);
	afx_msg BOOL DoSetDiagramType(long iType);
	afx_msg BOOL DoSetDiagramSize(long lSize);
	afx_msg BOOL DoSetDiagramLabel(BOOL bOnOff, long iType);
	afx_msg BOOL DoSetGeotext(LPCTSTR sLayername, BOOL bOnOff);
	afx_msg BOOL DoGetLinePoint(LPCTSTR sProgisID, double dLength);
	afx_msg BOOL DoImportObjectFromFile(LPCTSTR sTxtFileName, LPCTSTR sDefFileName, long iObjType);
	afx_msg BOOL DoExplodeSymbol(LPCTSTR sProgisId, LPCTSTR sDestLayerName);
	afx_msg long DoCreateThematicMap(LPCTSTR sConnectionString, LPCTSTR sCommand, LPCTSTR sProgisIDFieldName, LPCTSTR sThematicFieldName, long iGroupingMode, long iGroupCount, LPCTSTR sGroupingModeName, LPCTSTR sSchemaName, LPCTSTR sDestLayerName, BOOL bDialog);
	afx_msg BOOL DoSetProjectID(LPCTSTR ProjectGUID);
	afx_msg BSTR DoGetProjectID();
	afx_msg BOOL DoCutPolyWithCircle(LPCTSTR sProgisId, double dX, double dY, double dRadius);
	afx_msg BOOL DoCopyLayer(LPCTSTR sSourceLayer, LPCTSTR sDestLayer);
	afx_msg double DoGetPolygonArea(LPCTSTR sProgisID);
	afx_msg BOOL DoExplodeObjects(LPCTSTR sDestLayername);
	afx_msg BOOL DoExplodePolygonsToPoints(LPCTSTR sDestLayername, LPCTSTR sSymbolName);
	afx_msg BOOL DoSetSelectionFilter(long iFilter);
	afx_msg BOOL DoGetPointOnLine(LPCTSTR sProgisID, long IDistance, BOOL bAutoCommit);
	afx_msg BOOL DoGetObjectCenter(LPCTSTR sProgisID, BOOL bAutoCommit);
	afx_msg long DoGetObjectPropertiesList(long lObjectType, long hObjectList,long sID, long dArea, long dLength, long dX, long dY, long sLayer, long sParam1, long sParam2, long sParam3);
	afx_msg long DoGetAttachedText(LPCTSTR sProgisID);
	afx_msg BOOL DoSetIDBOnOff(BOOL bActive);
	afx_msg BOOL DoSetTooltipDB(LPCTSTR sLayername, LPCTSTR sDBString);
	afx_msg long DoGetObjectPropertiesListEx(long IObjectType, long hObjectList, long sID, long dArea, long dLenght, long dX, long dY, long sLayer, long sParam1, long sParam2, long sParam3, long timeout);
	afx_msg BOOL DoPolyPartitionWithLine(LPCTSTR sDestLayer, LPCTSTR sPolyId, LPCTSTR sLineId);
	afx_msg BOOL DoSetLayerPrintable(LPCTSTR sLayername, BOOL bPrintable);
	afx_msg double DoGetLayerPolygonArea(LPCTSTR sLayername);
	afx_msg BOOL DoAttachText(LPCTSTR sTextProgisID, LPCTSTR sAttachProgisID);
	afx_msg BOOL DoSetDiagramFont(LPCTSTR sFontName, long iSize);
	afx_msg BOOL DoExecuteModule(LPCTSTR sModuleName);
    afx_msg BOOL DoCreateAdvancedTextEx(double dAbsX, double dAbsY,LPCTSTR sFontName, int iFontStyle, int lHeight, double dAngle, int iAlign, LPCTSTR sText, BOOL bAutoCommit);
    afx_msg BOOL DoSetDDEOutput(long iMode);
	afx_msg BOOL DoComparePoints(LPCTSTR sSourceLayername1, LPCTSTR sSourceLayername2, LPCTSTR sDestLayername, LPCTSTR sSymbolName);
    afx_msg BSTR DoGetTooltipDB(LPCTSTR sLayername);
	afx_msg BOOL DoSetTimeOut(long lTimeOutValue);
	//}}AFX_DISPATCH

    BOOL m_active;
    BOOL m_connected;
    VARIANT_BOOL m_ready;       // TRUE if a [RED] was received from WinGIS
    BOOL m_bSendOk;
    long m_layernameList;
    long m_layerindexList;
    BOOL bm_layernameListValid;
    long m_viewList;
    int m_layerinfo;
    CString m_layerIndex;
    CString m_csVersion;

    char m_chSep;
    CString m_csDDESeparator; 

    // License data
    CString m_fingerprint;
    CString m_licensekey;
    CString m_appname;

    long m_axmode;
    int iActiveLicenseNr;
    CPtrList cLicenseList;
    BOOL bDesignLicenseOk;


    time_t m_lLicDate;
    CString m_licensemsg;

 	  CString m_csLastErrorMsg;

    BOOL DesignPermissionOk();
    BOOL CheckDesignLicense();
    BOOL FunctionPermissionOk(CString csFunctionName);

    long m_lasterror;

	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CAXWingisCtrl)
	void FireOnDDEExecute(LPCTSTR sCmd)
		{FireEvent(eventidOnDDEExecute,EVENT_PARAM(VTS_BSTR), sCmd);}
	void FireOnConnect()
		{FireEvent(eventidOnConnect,EVENT_PARAM(VTS_NONE));}
	void FireOnDisconnect()
		{FireEvent(eventidOnDisconnect,EVENT_PARAM(VTS_NONE));}
	void FireOnMonitoring(long hProgisIDList)
		{FireEvent(eventidOnMonitoring,EVENT_PARAM(VTS_I4), hProgisIDList);}
	void FireOnCloseDB()
		{FireEvent(eventidOnCloseDB,EVENT_PARAM(VTS_NONE));}
	void FireOnShowDB()
		{FireEvent(eventidOnShowDB,EVENT_PARAM(VTS_NONE));}
	void FireOnProjectOpen(LPCTSTR sProjectName)
		{FireEvent(eventidOnProjectOpen,EVENT_PARAM(VTS_BSTR), sProjectName);}
	void FireOnProjectActivate(LPCTSTR sProjectName)
		{FireEvent(eventidOnProjectActivate,EVENT_PARAM(VTS_BSTR), sProjectName);}
	void FireOnProjectClose(LPCTSTR sProjectName)
		{FireEvent(eventidOnProjectClose,EVENT_PARAM(VTS_BSTR), sProjectName);}
	void FireOnGisReady(BOOL bReady, long lError)
		{FireEvent(eventidOnGisReady,EVENT_PARAM(VTS_BOOL  VTS_I4), bReady, lError);}
	void FireOnDelete(long hProgisIDList)
		{FireEvent(eventidOnDelete,EVENT_PARAM(VTS_I4), hProgisIDList);}
	void FireOnCreateChart(long hProgisIDList)
		{FireEvent(eventidOnCreateChart,EVENT_PARAM(VTS_I4), hProgisIDList);}
	void FireOnSetCaption(long hProgisIDList)
		{FireEvent(eventidOnSetCaption,EVENT_PARAM(VTS_I4), hProgisIDList);}
	void FireOnNotifyMoveObjects(long hProgisIDList)
		{FireEvent(eventidOnNotifyMoveObjects,EVENT_PARAM(VTS_I4), hProgisIDList);}
	void FireOnNotifyMoveDimension(double dDeltaX, double dDeltaY)
		{FireEvent(eventidOnNotifyMoveDimension,EVENT_PARAM(VTS_R8  VTS_R8), dDeltaX, dDeltaY);}
	void FireOnInsert(LPCTSTR sProgisID, double dArea, double dLength, double dX, double dY, LPCTSTR sLayer, LPCTSTR sSymbolNr, LPCTSTR sSymbolName, LPCTSTR sSymbolLib)
		{FireEvent(eventidOnInsert,EVENT_PARAM(VTS_BSTR  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR), sProgisID, dArea, dLength, dX, dY, sLayer, sSymbolNr, sSymbolName, sSymbolLib);}
	void FireOnUpdate(LPCTSTR sProgisID, double dArea, double dLength, double dX, double dY, LPCTSTR sLayer, LPCTSTR sSymbolNr, LPCTSTR sSymbolName, LPCTSTR sSymbolLib)
		{FireEvent(eventidOnUpdate,EVENT_PARAM(VTS_BSTR  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR), sProgisID, dArea, dLength, dX, dY, sLayer, sSymbolNr, sSymbolName, sSymbolLib);}
	void FireOnNotifyNewView(LPCTSTR sView)
		{FireEvent(eventidOnNotifyNewView,EVENT_PARAM(VTS_BSTR), sView);}
	void FireOnLinkPointToArea(LPCTSTR sIDPoint, LPCTSTR sIDArea, double dArea)
		{FireEvent(eventidOnLinkPointToArea,EVENT_PARAM(VTS_BSTR  VTS_BSTR  VTS_R8), sIDPoint, sIDArea, dArea);}
	void FireOnCutArea(long hIDFirstLayer, long hIDSecondLayer, long hIDTargetLayer, long hParam1List, long hParam2List, long hParam3List)
		{FireEvent(eventidOnCutArea,EVENT_PARAM(VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4), hIDFirstLayer, hIDSecondLayer, hIDTargetLayer, hParam1List, hParam2List, hParam3List);}
	void FireOnChangeTable()
		{FireEvent(eventidOnChangeTable,EVENT_PARAM(VTS_NONE));}
	void FireOnNotifyViewDeleted(long hViewList)
		{FireEvent(eventidOnNotifyViewDeleted,EVENT_PARAM(VTS_I4), hViewList);}
	void FireOnMacroBegin()
		{FireEvent(eventidOnMacroBegin,EVENT_PARAM(VTS_NONE));}
	void FireOnMacroEnd()
		{FireEvent(eventidOnMacroEnd,EVENT_PARAM(VTS_NONE));}
	void FireOnNotifyMainWindowReady()
		{FireEvent(eventidOnNotifyMainWindowReady,EVENT_PARAM(VTS_NONE));}
	void FireOnNotifySetView(LPCTSTR sView)
		{FireEvent(eventidOnNotifySetView,EVENT_PARAM(VTS_BSTR), sView);}
	void FireOnDeclareLayerSettings(long lLayerIndex, LPCTSTR sLayerName, long lPosition, BOOL bActive, BOOL bOn, BOOL bGenOn, double dGenMin, double dGenMax, BOOL bFixOn, long lLineStyle, long lLineWidth, long lLineColor, long lPattern, long lPatternColor, BOOL bTransparent)
		{FireEvent(eventidOnDeclareLayerSettings,EVENT_PARAM(VTS_I4  VTS_BSTR  VTS_I4  VTS_BOOL  VTS_BOOL  VTS_BOOL  VTS_R8  VTS_R8  VTS_BOOL  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_BOOL), lLayerIndex, sLayerName, lPosition, bActive, bOn, bGenOn, dGenMin, dGenMax, bFixOn, lLineStyle, lLineWidth, lLineColor, lPattern, lPatternColor, bTransparent);}
	void FireOnDeclareViewSettings(LPCTSTR sViewName, BOOL bDefaultView, BOOL bLayerPriorities, BOOL bProjPart, double dRectX1, double dRectY1, double dRectX2, double dRectY2)
		{FireEvent(eventidOnDeclareViewSettings,EVENT_PARAM(VTS_BSTR  VTS_BOOL  VTS_BOOL  VTS_BOOL  VTS_R8  VTS_R8  VTS_R8  VTS_R8), sViewName, bDefaultView, bLayerPriorities, bProjPart, dRectX1, dRectY1, dRectX2, dRectY2);}
	void FireOnDeclareViewLayerSettings(LPCTSTR sViewName, long lLayerIndex, LPCTSTR sLayerName, long lPosition, BOOL bOn, BOOL bGenOn, double dGenMin, double dGenMax, BOOL bFixOn, long lLineStyle, long lLineWidth, long lLineColor, long lPattern, long PatternColor, BOOL bTransparent)
		{FireEvent(eventidOnDeclareViewLayerSettings,EVENT_PARAM(VTS_BSTR  VTS_I4  VTS_BSTR  VTS_I4  VTS_BOOL  VTS_BOOL  VTS_R8  VTS_R8  VTS_BOOL  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_BOOL), sViewName, lLayerIndex, sLayerName, lPosition, bOn, bGenOn, dGenMin, dGenMax, bFixOn, lLineStyle, lLineWidth, lLineColor, lPattern, PatternColor, bTransparent);}
	void FireOnNotifyBoxSelection(LPCTSTR sLayerName, double dX1, double dY1, double dX2, double dY2)
		{FireEvent(eventidOnNotifyBoxSelection,EVENT_PARAM(VTS_BSTR  VTS_R8  VTS_R8  VTS_R8  VTS_R8), sLayerName, dX1, dY1, dX2, dY2);}
	void FireOnNotifyCircleSelection(LPCTSTR sLayerName, double dXM, double dYM, double dR)
		{FireEvent(eventidOnNotifyCircleSelection,EVENT_PARAM(VTS_BSTR  VTS_R8  VTS_R8  VTS_R8), sLayerName, dXM, dYM, dR);}
	void FireOnDeclareNextUnusedID(LPCTSTR sPointID, LPCTSTR sLineID, LPCTSTR sAreaID, LPCTSTR sCircleID, LPCTSTR sArcID, LPCTSTR sSymbolID, LPCTSTR sImageID, LPCTSTR sTextID, LPCTSTR sCaptionTextID, LPCTSTR sChartID, LPCTSTR sSplineID, LPCTSTR sMeasureID, LPCTSTR sOLEObjectID)
		{FireEvent(eventidOnDeclareNextUnusedID,EVENT_PARAM(VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR), sPointID, sLineID, sAreaID, sCircleID, sArcID, sSymbolID, sImageID, sTextID, sCaptionTextID, sChartID, sSplineID, sMeasureID, sOLEObjectID);}
	void FireOnBuildArea(long hSourceIDs, LPCTSTR sTargetID, double dNewArea, LPCTSTR sTargetLayer)
		{FireEvent(eventidOnBuildArea,EVENT_PARAM(VTS_I4  VTS_BSTR  VTS_R8  VTS_BSTR), hSourceIDs, sTargetID, dNewArea, sTargetLayer);}
	void FireOnAsciiExport(long hProgisIDList)
		{FireEvent(eventidOnAsciiExport,EVENT_PARAM(VTS_I4), hProgisIDList);}
	void FireOnAsciiImport(LPCTSTR sProgisID, double dArea, long hColumnValueList, long hColumnNameList, long hColumnWidthList)
		{FireEvent(eventidOnAsciiImport,EVENT_PARAM(VTS_BSTR  VTS_R8  VTS_I4  VTS_I4  VTS_I4), sProgisID, dArea, hColumnValueList, hColumnNameList, hColumnWidthList);}
	void FireOnFreeMonitoring(long hProgisIDList, LPCTSTR sMON)
		{FireEvent(eventidOnFreeMonitoring,EVENT_PARAM(VTS_I4  VTS_BSTR), hProgisIDList, sMON);}
	void FireOnTimer()
		{FireEvent(eventidOnTimer,EVENT_PARAM(VTS_NONE));}
	void FireOnPrintResult(LPCTSTR sCommandName, long lResult)
		{FireEvent(eventidOnPrintResult,EVENT_PARAM(VTS_BSTR  VTS_I4), sCommandName, lResult);}
	void FireOnPrintEvents(long lPage, long lOfPages, long lResult)
		{FireEvent(eventidOnPrintEvents,EVENT_PARAM(VTS_I4  VTS_I4  VTS_I4), lPage, lOfPages, lResult);}
	void FireOnProjectSaveAs(LPCTSTR sNewProjectName)
		{FireEvent(eventidOnProjectSaveAs,EVENT_PARAM(VTS_BSTR), sNewProjectName);}
	void FireOnGraphicStatus(LPCTSTR sActivity)
		{FireEvent(eventidOnGraphicStatus,EVENT_PARAM(VTS_BSTR), sActivity);}
	void FireOnActiveLayerStatus(LPCTSTR sLayerName, BOOL bFixed)
		{FireEvent(eventidOnActiveLayerStatus,EVENT_PARAM(VTS_BSTR  VTS_BOOL), sLayerName, bFixed);}
	void FireOnVisibleMapArea(double dLLX, double dLLY, double dURX, double dURY)
		{FireEvent(eventidOnVisibleMapArea,EVENT_PARAM(VTS_R8  VTS_R8  VTS_R8  VTS_R8), dLLX, dLLY, dURX, dURY);}
	void FireOnVisibleMapWidth(double dWidth)
		{FireEvent(eventidOnVisibleMapWidth,EVENT_PARAM(VTS_R8), dWidth);}
	void FireOnSelectionStatus(LPCTSTR sSelState)
		{FireEvent(eventidOnSelectionStatus,EVENT_PARAM(VTS_BSTR), sSelState);}
	void FireOnSelectionStatusXValue(LPCTSTR sValue)
		{FireEvent(eventidOnSelectionStatusXValue,EVENT_PARAM(VTS_BSTR), sValue);}
	void FireOnSelectionStatusXInfo(LPCTSTR sInfo)
		{FireEvent(eventidOnSelectionStatusXInfo,EVENT_PARAM(VTS_BSTR), sInfo);}
	void FireOnSelectionStatusYValue(LPCTSTR sValue)
		{FireEvent(eventidOnSelectionStatusYValue,EVENT_PARAM(VTS_BSTR), sValue);}
	void FireOnSelectionStatusYInfo(LPCTSTR sInfo)
		{FireEvent(eventidOnSelectionStatusYInfo,EVENT_PARAM(VTS_BSTR), sInfo);}
	void FireOnInsertList(long hIDList, long hAreaList, long hLengthList, long hXList, long hYList, long hLayerList, long hSymbolNrList, long hSymbolNameList, long hSymbolLibList)
		{FireEvent(eventidOnInsertList,EVENT_PARAM(VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4), hIDList, hAreaList, hLengthList, hXList, hYList, hLayerList, hSymbolNrList, hSymbolNameList, hSymbolLibList);}
	void FireOnInvalidMapArea(double dLLX, double dLLY, double dURX, double dURY)
		{FireEvent(eventidOnInvalidMapArea,EVENT_PARAM(VTS_R8  VTS_R8  VTS_R8  VTS_R8), dLLX, dLLY, dURX, dURY);}
	void FireOnAsciiImportEnd()
		{FireEvent(eventidOnAsciiImportEnd,EVENT_PARAM(VTS_NONE));}
	void FireOnCutAreaLayers(LPCTSTR sLayer1, LPCTSTR sLayer2, LPCTSTR sLayerTarget, long lOperation)
		{FireEvent(eventidOnCutAreaLayers,EVENT_PARAM(VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_I4), sLayer1, sLayer2, sLayerTarget, lOperation);}
	void FireOnError(LPCTSTR sMsg, BOOL bFatal)
		{FireEvent(eventidOnError,EVENT_PARAM(VTS_BSTR  VTS_BOOL), sMsg, bFatal);}
	void FireOnObjectProperties(LPCTSTR sID, double dArea, double dLength, double dX, double dY, LPCTSTR sLayer, LPCTSTR sParam1, LPCTSTR sParam2, LPCTSTR sParam3)
		{FireEvent(eventidOnObjectProperties,EVENT_PARAM(VTS_BSTR  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR), sID, dArea, dLength, dX, dY, sLayer, sParam1, sParam2, sParam3);}
	void FireOnDistance(LPCTSTR sDistance)
		{FireEvent(eventidOnDistance,EVENT_PARAM(VTS_BSTR), sDistance);}
	void FireOnChartColor(long hColorList)
		{FireEvent(eventidOnChartColor,EVENT_PARAM(VTS_I4), hColorList);}
	void FireOnLineStyleInfo(long hLineStyleNumberList, long hLineStyleNameList)
		{FireEvent(eventidOnLineStyleInfo,EVENT_PARAM(VTS_I4  VTS_I4), hLineStyleNumberList, hLineStyleNameList);}
	void FireOnHatchStyleInfo(long hHatchStyleNumberList, long hHatchStyleNameList)
		{FireEvent(eventidOnHatchStyleInfo,EVENT_PARAM(VTS_I4  VTS_I4), hHatchStyleNumberList, hHatchStyleNameList);}
	void FireOnGPSID(LPCTSTR sGPSID, LPCTSTR sProgisID)
		{FireEvent(eventidOnGPSID,EVENT_PARAM(VTS_BSTR  VTS_BSTR), sGPSID, sProgisID);}
	void FireOnGPSProAsc(LPCTSTR sStateType, LPCTSTR sStateValue)
		{FireEvent(eventidOnGPSProAsc,EVENT_PARAM(VTS_BSTR  VTS_BSTR), sStateType, sStateValue);}
	void FireOnGPSProCalc(double dX, double dY)
		{FireEvent(eventidOnGPSProCalc,EVENT_PARAM(VTS_R8  VTS_R8), dX, dY);}
	void FireOnGPSSymSet(long lSymbolID)
		{FireEvent(eventidOnGPSSymSet,EVENT_PARAM(VTS_I4), lSymbolID);}
	void FireOnAsciiImportStart(long hColumnNameList, long hColumnWidthList)
		{FireEvent(eventidOnAsciiImportStart,EVENT_PARAM(VTS_I4  VTS_I4), hColumnNameList, hColumnWidthList);}
	void FireOnAsciiExportStart(long hColumnNameList)
		{FireEvent(eventidOnAsciiExportStart,EVENT_PARAM(VTS_I4), hColumnNameList);}
	void FireOnAsciiExportEnd()
		{FireEvent(eventidOnAsciiExportEnd,EVENT_PARAM(VTS_NONE));}
	void FireOnActiveLayer(LPCTSTR sLayer)
		{FireEvent(eventidOnActiveLayer,EVENT_PARAM(VTS_BSTR), sLayer);}
	void FireOnCopyObjects(long hObjectIDList, long hProgisIDList)
		{FireEvent(eventidOnCopyObjects,EVENT_PARAM(VTS_I4  VTS_I4), hObjectIDList, hProgisIDList);}
	void FireOnPolygonPartition(long hSourceLayer, long hSourceID, long hDestLayer, long hNewID, long hArea, long hPerimeter)
		{FireEvent(eventidOnPolygonPartition,EVENT_PARAM(VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4), hSourceLayer, hSourceID, hDestLayer, hNewID, hArea, hPerimeter);}
	void FireOnActualProjectPart(double dLLX, double dLLY, double dURX, double dURY)
		{FireEvent(eventidOnActualProjectPart,EVENT_PARAM(VTS_R8  VTS_R8  VTS_R8  VTS_R8), dLLX, dLLY, dURX, dURY);}
	void FireOnCanvasRedraw(long lBitmapHandle, double dLLX, double dLLY, double dURX, double dURY)
		{FireEvent(eventidOnCanvasRedraw,EVENT_PARAM(VTS_I4  VTS_R8  VTS_R8  VTS_R8  VTS_R8), lBitmapHandle, dLLX, dLLY, dURX, dURY);}
	void FireOnDeclareProjectSettings(LPCTSTR sUnitsName, double dScale, LPCTSTR sProjectionName, LPCTSTR sProjectionDate)
		{FireEvent(eventidOnDeclareProjectSettings,EVENT_PARAM(VTS_BSTR  VTS_R8  VTS_BSTR  VTS_BSTR), sUnitsName, dScale, sProjectionName, sProjectionDate);}
	void FireOnPrinterParamGet(LPCTSTR sDeviceName, LPCTSTR sPaperFormat, long lOrientation, long lErrorCode)
		{FireEvent(eventidOnPrinterParamGet,EVENT_PARAM(VTS_BSTR  VTS_BSTR  VTS_I4  VTS_I4), sDeviceName, sPaperFormat, lOrientation, lErrorCode);}
	void FireOnPrintPagesParamGet(long lModel, long lRepaginate, long lHorizPages, long lVerticPages, double dMrLeft, double dMrBottom, double dMrRight, double dMrTop, double dInLeft, double dInBottom, double dInRight, double dInTop, double dOvLeft, double dOvBottom, double dOvRight, double dOvTop, long lNumbOrder, long lNumbFrom, long lErrorCode)
		{FireEvent(eventidOnPrintPagesParamGet,EVENT_PARAM(VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_I4  VTS_I4  VTS_I4), lModel, lRepaginate, lHorizPages, lVerticPages, dMrLeft, dMrBottom, dMrRight, dMrTop, dInLeft, dInBottom, dInRight, dInTop, dOvLeft, dOvBottom, dOvRight, dOvTop, lNumbOrder, lNumbFrom, lErrorCode);}
	void FireOnPrintMapParamGet(long lMapID, long lRegion, LPCTSTR sViewName, double dRegionX1, double dRegionY1, double dRegionX2, double dRegionY2, BOOL bFitToPage, double dPrintScale, BOOL bKeepScale, double dFrameWidth, double dFrameHeight, long lFrameCentering, double dFrameLeft, double dFrameTop, long lErrorCode)
		{FireEvent(eventidOnPrintMapParamGet,EVENT_PARAM(VTS_I4  VTS_I4  VTS_BSTR  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_BOOL  VTS_R8  VTS_BOOL  VTS_R8  VTS_R8  VTS_I4  VTS_R8  VTS_R8  VTS_I4), lMapID, lRegion, sViewName, dRegionX1, dRegionY1, dRegionX2, dRegionY2, bFitToPage, dPrintScale, bKeepScale, dFrameWidth, dFrameHeight, lFrameCentering, dFrameLeft, dFrameTop, lErrorCode);}
	void FireOnPrintLegendParamGet(long lLegendID, LPCTSTR sLegendName, double dFrameWidth, double dFrameHeight, long lFrameCentering, long lFrameOridgin, double dFrameLeft, double dFrameTopOrBot, long lErrorCode)
		{FireEvent(eventidOnPrintLegendParamGet,EVENT_PARAM(VTS_I4  VTS_BSTR  VTS_R8  VTS_R8  VTS_I4  VTS_I4  VTS_R8  VTS_R8  VTS_I4), lLegendID, sLegendName, dFrameWidth, dFrameHeight, lFrameCentering, lFrameOridgin, dFrameLeft, dFrameTopOrBot, lErrorCode);}
	void FireOnPrintPictureParamGet(long lPictureID, LPCTSTR sPictureFile, long lPictureLocation, BOOL bKeepAspect, double dFrameWidth, double dFrameHeight, long lFrameCentering, long lFrameOridgin, double dFrameLeft, double dFrameTopOrBot, long lErrorCode)
		{FireEvent(eventidOnPrintPictureParamGet,EVENT_PARAM(VTS_I4  VTS_BSTR  VTS_I4  VTS_BOOL  VTS_R8  VTS_R8  VTS_I4  VTS_I4  VTS_R8  VTS_R8  VTS_I4), lPictureID, sPictureFile, lPictureLocation, bKeepAspect, dFrameWidth, dFrameHeight, lFrameCentering, lFrameOridgin, dFrameLeft, dFrameTopOrBot, lErrorCode);}
	void FireOnPrintSignatureParamGet(long lSignatureID, LPCTSTR sSignature, LPCTSTR sFontName, BOOL bItalic, BOOL bBold, BOOL bUnderlined, BOOL bTransparent, long lFontSize, long lFontColor, long lFontBackColor, long lTextAlignment, BOOL bOnPagesLocation, BOOL bOnFirstPage, BOOL bOnInternalPages, BOOL bOnLastPage, long lFrameCentering, long lFrameOridgin, double dFrameLeft, double dFrameTopOrBot, long lErrorCode)
		{FireEvent(eventidOnPrintSignatureParamGet,EVENT_PARAM(VTS_I4  VTS_BSTR  VTS_BSTR  VTS_BOOL  VTS_BOOL  VTS_BOOL  VTS_BOOL  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_BOOL  VTS_BOOL  VTS_BOOL  VTS_BOOL  VTS_I4  VTS_I4  VTS_R8  VTS_R8  VTS_I4), lSignatureID, sSignature, sFontName, bItalic, bBold, bUnderlined, bTransparent, lFontSize, lFontColor, lFontBackColor, lTextAlignment, bOnPagesLocation, bOnFirstPage, bOnInternalPages, bOnLastPage, lFrameCentering, lFrameOridgin, dFrameLeft, dFrameTopOrBot, lErrorCode);}
	void FireOnPrintObjectsInformGet(long lObjectsNumber, long lMapsNumber, long lLegendsNumber, long lPicturesNumber, long lSignaturesNumber, long hTypesList, long hObjectsIDList, long lErrorCode)
		{FireEvent(eventidOnPrintObjectsInformGet,EVENT_PARAM(VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4), lObjectsNumber, lMapsNumber, lLegendsNumber, lPicturesNumber, lSignaturesNumber, hTypesList, hObjectsIDList, lErrorCode);}
	void FireOnSendPolygonData(long hID, double dArea, LPCTSTR sLayer, long dX, long dY)
		{FireEvent(eventidOnSendPolygonData,EVENT_PARAM(VTS_I4  VTS_R8  VTS_BSTR  VTS_I4  VTS_I4), hID, dArea, sLayer, dX, dY);}
	void OnPrintObjectsInformGet(long lObjectsNumber, long lMapsNumber, long lLegendsNumber, long lPicturesNumber, long lSignaturesNumber, long hTypesList, long hObjectsIDList, long lErrorCode)
		{FireEvent(eventidOnPrintObjectsInformGet,EVENT_PARAM(VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4), lObjectsNumber, lMapsNumber, lLegendsNumber, lPicturesNumber, lSignaturesNumber, hTypesList, hObjectsIDList, lErrorCode);}
	void FireOnLinkTextToArea(LPCTSTR sIDText, LPCTSTR sIDArea, LPCTSTR sText)
		{FireEvent(eventidOnLinkTextToArea,EVENT_PARAM(VTS_BSTR  VTS_BSTR  VTS_BSTR), sIDText, sIDArea, sText);}
	void FireOnMouseAction(double dX, double dY, LPCTSTR sProgisID, long lButton, long lMode, BOOL bDblClick)
		{FireEvent(eventidOnMouseAction,EVENT_PARAM(VTS_R8  VTS_R8  VTS_BSTR  VTS_I4  VTS_I4  VTS_BOOL), dX, dY, sProgisID, lButton, lMode, bDblClick);}
	void FireOnGetCoordinates(double dX, double dY, BOOL bCancel)
		{FireEvent(eventidOnGetCoordinates,EVENT_PARAM(VTS_R8  VTS_R8  VTS_BOOL), dX, dY, bCancel);}
	void FireOnSymbolInfo(long hSymbolNumberList, long hSymbolNameList, long hSymbolLibraryList, long hSizeList, long hFillStyleList, long hFillStyleFGColorList, long hFillStyleBGColorList, long hLineStyleList, long hLineStyleColorList)
		{FireEvent(eventidOnSymbolInfo,EVENT_PARAM(VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4), hSymbolNumberList, hSymbolNameList, hSymbolLibraryList, hSizeList, hFillStyleList, hFillStyleFGColorList, hFillStyleBGColorList, hLineStyleList, hLineStyleColorList);}
	void FireOnSetPicture(long lPicId, long lResFlag)
		{FireEvent(eventidOnSetPicture,EVENT_PARAM(VTS_I4  VTS_I4), lPicId, lResFlag);}
	void FireOnCreateObjectEx(long lObjId, long lUserId)
		{FireEvent(eventidOnCreateObjectEx,EVENT_PARAM(VTS_I4  VTS_I4), lObjId, lUserId);}
	void FireOnTooltipText(LPCTSTR sProgisID)
		{FireEvent(eventidOnTooltipText,EVENT_PARAM(VTS_BSTR), sProgisID);}
	void FireOnGetLinePoint(double dX, double dY, double dAngle)
		{FireEvent(eventidOnGetLinePoint,EVENT_PARAM(VTS_R8  VTS_R8  VTS_R8), dX, dY, dAngle);}
	void FireOnImpExpFinished()
		{FireEvent(eventidOnImpExpFinished,EVENT_PARAM(VTS_NONE));}
	void FireOnCreateThematicMap(short iReturnCode)
		{FireEvent(eventidOnCreateThematicMap,EVENT_PARAM(VTS_I2), iReturnCode);}
	void FireOnGetProjectID(LPCTSTR sID)
		{FireEvent(eventidOnGetProjectID,EVENT_PARAM(VTS_BSTR), sID);}
	void FireOnPolyCircleCut(long hXList, long hYList)
		{FireEvent(eventidOnPolyCircleCut,EVENT_PARAM(VTS_I4  VTS_I4), hXList, hYList);}
	void FireOnCopyLayer(long hSourceList, long hDestList)
		{FireEvent(eventidOnCopyLayer,EVENT_PARAM(VTS_I4  VTS_I4), hSourceList, hDestList);}
	void FireOnObjectCenter(long hProgisIDList, long hXList, long hYList)
		{FireEvent(eventidOnObjectCenter,EVENT_PARAM(VTS_I4  VTS_I4  VTS_I4), hProgisIDList, hXList, hYList);}
	void FireOnCreateText(long hProgisIdList)
		{FireEvent(eventidOnCreateText,EVENT_PARAM(VTS_I4), hProgisIdList);}
	void FireOnGetAttachedText(long ProgisIDList, long TextList)
		{FireEvent(eventidOnGetAttachedText,EVENT_PARAM(VTS_I4  VTS_I4), ProgisIDList, TextList);}
	void FireOnImpEnd()
		{FireEvent(eventidOnImpEnd,EVENT_PARAM(VTS_NONE));}
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CAXWingisCtrl)
	dispidSServer = 1L,
	dispidSTopic = 2L,
	dispidBUseSend = 3L,
	dispidBActive = 6L,
	dispidBConnected = 7L,
	dispidBReady = 8L,
	dispidLLayerNameList = 9L,
	dispidLLayerIndexList = 10L,
	dispidSLayerName = 4L,
	dispidLLayerIndex = 12L,
	dispidLViewList = 13L,
	dispidSLayerInfo = 11L,
	dispidBUseCOM = 5L,
	dispidSAppName = 14L,
	dispidSFingerPrint = 15L,
	dispidDDEExecute = 16L,
	dispidCreateList = 17L,
	dispidAddStringParam = 18L,
	dispidGetStringParam = 19L,
	dispidDeleteList = 20L,
	dispidIsValidList = 21L,
	dispidClearList = 22L,
	dispidGetListSize = 23L,
	dispidDoSelectObjects = 24L,
	dispidDoCloseGIS = 25L,
	dispidDoShowGIS = 26L,
	dispidDoZoomObjects = 27L,
	dispidDoDeselectAll = 28L,
	dispidDoSetView = 29L,
	dispidDoSetCaption = 30L,
	dispidDoSearchObjects = 31L,
	dispidDoOpenProject = 32L,
	dispidDoCloseProject = 33L,
	dispidDoSetActiveLayer = 34L,
	dispidDoGetActiveLayer = 35L,
	dispidCreateListEx = 36L,
	dispidDoCreateChart = 37L,
	dispidDoDeleteObjects = 38L,
	dispidDoSaveProject = 39L,
	dispidDoSetSelectionMode = 40L,
	dispidDoSetText = 41L,
	dispidDoBeginMacro = 42L,
	dispidDoEndMacro = 43L,
	dispidDoCreateLayerDialog = 44L,
	dispidDoSelectObjectsD = 46L,
	dispidDoZoomObjectsD = 47L,
	dispidDoCreateTextEx = 48L,
	dispidDoGetLayerSettings = 49L,
	dispidDoFindLayerByName = 50L,
	dispidDoFindLayerByIndex = 51L,
	dispidDoDeleteLayerByName = 52L,
	dispidDoDeleteLayerByIndex = 53L,
	dispidDoLoadBitmap = 54L,
	dispidDoChangeLayerSettingsEx = 55L,
	dispidDoChangeLayerSettingsByIndex = 56L,
	dispidDoCopyObjects = 57L,
	dispidDoCreateObject = 58L,
	dispidDoGetNextUnusedID = 59L,
	dispidDoSendGPSPosition = 60L,
	dispidDoCreateLayerEx = 61L,
	dispidDoDisableScreenUpdate = 62L,
	dispidDoSetScreenUpdate = 63L,
	dispidDoReloadProject = 64L,
	dispidDoChangeBitmapSettings = 65L,
	dispidDoSetOptionSettings = 66L,
	dispidDoChangeObjectStyleEx = 67L,
	dispidDoGetNextSymbolNumber = 68L,
	dispidDoSetWindowPos = 69L,
	dispidDoSetZoom = 70L,
	dispidDoNotifyTableChange = 71L,
	dispidDoInsertObjects = 72L,
	dispidDoAsciiExport = 73L,
	dispidDoAsciiImport = 74L,
	dispidDoNotifyImportError = 75L,
	dispidDoTempSetText = 76L,
	dispidDoZoomAndSetText = 77L,
	dispidWaitForReadyMsg = 78L,
	dispidDoMoveObject = 79L,
	dispidDoSetObjectPos = 80L,
	dispidDoGetViewSettings = 81L,
	dispidAlignWindow = 82L,
	dispidShowWindow = 83L,
	dispidSetTimerIntervall = 84L,
	dispidSetTimerState = 85L,
	dispidDoCommit = 86L,
	dispidDoPingWinGIS = 87L,
	dispidDoConnect = 88L,
	dispidDoConnectEx = 89L,
	dispidDoCreateText = 90L,
	dispidDoChangeLayerSettings = 91L,
	dispidDoCreateLayer = 92L,
	dispidDoChangeObjectStyle = 93L,
	dispidDoDisconnect = 94L,
	dispidDoGetLoadedProjects = 95L,
	dispidDoPrint = 96L,
	dispidDoProjectSaveAs = 97L,
	dispidDoSelectAndSearchEx = 98L,
	dispidDoSetGraphicMode = 99L,
	dispidDoSetSelectionOnGenerate = 100L,
	dispidDoCalcRoute = 101L,
	dispidDoActivateProject = 102L,
	dispidDoSetVisibleMapArea = 103L,
	dispidGetModuleFileName = 104L,
	dispidSetModuleFileName = 105L,
	dispidGetLicenseDays = 106L,
	dispidAddLicense = 107L,
	dispidGetLastErrorMsg = 108L,
	dispidDoGetObjectProperties = 109L,
	dispidWaitForMacroEnd = 110L,
	dispidDoSetActiveSymbol = 111L,
	dispidDoSupressDialog = 112L,
	dispidDoSetTextStyle = 113L,
	dispidDoSetChartStyle = 114L,
	dispidDoSetChartColor = 115L,
	dispidDoGetChartColor = 116L,
	dispidDoRequestSymbolInfo = 117L,
	dispidDoRequestLineStyleInfo = 118L,
	dispidDoRequestHatchStyleInfo = 119L,
	dispidDoGetSelectionState = 120L,
	dispidDoSetNewObjectSelection = 121L,
	dispidGetWinGisRelease = 122L,
	dispidGPSDelete = 123L,
	dispidGPSDbColumn = 124L,
	dispidGPSNew = 125L,
	dispidGPSOffset = 126L,
	dispidGPSProAsc = 127L,
	dispidGPSProCalc = 128L,
	dispidGPSShow = 129L,
	dispidGPSSymSet = 130L,
	dispidGPSUpdate = 131L,
	dispidGPSLoadGrf = 132L,
	dispidDoClearLayer = 133L,
	dispidDoDenyWingisSession = 134L,
	dispidDoPolygonPartition = 135L,
	dispidDoShowAllObjects = 136L,
	dispidDoTellProjectPart = 137L,
	dispidDoPolygonOverlay = 138L,
	dispidDoRedraw = 139L,
	dispidDeleteStringParam = 140L,
	dispidDoModifyView = 141L,
	dispidSortList = 142L,
	dispidCStrToDouble = 143L,
	dispidCDoubleToStr = 144L,
	dispidSortListEx = 145L,
	dispidDoGetCoordinateSystem = 146L,
	dispidDoGetProjectSettings = 147L,
	dispidDoCreatePolyline = 148L,
	dispidDoPrintSetPrintParam = 149L,
	dispidDoPrintSetPagesParam = 150L,
	dispidDoPrintSetMapParam = 151L,
	dispidDoPrintSetLegendParam = 152L,
	dispidDoPrintSetPictureParam = 153L,
	dispidDoPrintSetSignatureParam = 154L,
	dispidDoPrintGetPrintParam = 155L,
	dispidDoPrintGetPagesParam = 156L,
	dispidDoPrintGetMapParam = 157L,
	dispidDoPrintGetLegendParam = 158L,
	dispidDoPrintGetPictureParam = 159L,
	dispidDoPrintGetSignatureParam = 160L,
	dispidDoPrintInsertLegend = 161L,
	dispidDoPrintInsertPicture = 162L,
	dispidDoPrintInsertSignature = 163L,
	dispidDoPrintStoreTemplate = 164L,
	dispidDoPrintLoadTemplate = 165L,
	dispidDoPrintGetObjectsInform = 166L,
	dispidDoPrintDeleteObject = 167L,
	dispidDoPrintMoveObject = 168L,
	dispidGPSProSet = 169L,
	dispidDoGetPolygonData = 170L,
	dispidDoExecuteMenuFunction = 171L,
	dispidDoGetCoordinates = 172L,
	dispidDoSendMouseAction = 173L,
	dispidDoCommitDigitalization = 174L,
	dispidDoGenerateObjects = 175L,
	dispidDoCutSelectedPolygons = 176L,
	dispidDoShowAllOnLayer = 177L,
	dispidDoCreateSymbol = 178L,
	dispidDoTrimLine = 179L,
	dispidDoCreateBuffer = 180L,
	dispidDoUndo = 181L,
	dispidAddListItemString = 182L,
	dispidAddListItemLong = 183L,
	dispidAddListItemDouble = 184L,
	dispidGetListItemString = 185L,
	dispidGetListItemLong = 186L,
	dispidGetListItemDouble = 187L,
	dispidDeleteListItem = 188L,
	dispidDoSetPicture = 189L,
	dispidDoCreateObjectEx = 190L,
	dispidDoReversePolyLine = 191L,
	dispidDoSetTooltipText = 192L,
	dispidDoCreateView = 193L,
	dispidDoDeleteView = 194L,
	dispidDoChangeImageSettings = 195L,
	dispidDoSetMapCanvas = 196L,
	dispidDoDeleteLayerList = 197L,
	dispidDoSetLayerSnap = 198L,
	dispidDoSetMouseZoom = 199L,
	dispidDoSelectSymbol = 200L,
	dispidDoSetSnapRadius = 201L,
	dispidDoSetActiveSymbolEx = 202L,
	dispidDoCombinePolygons = 203L,
	dispidDoSetAreaDivider = 204L,
	dispidDoSetDiagramColor = 205L,
	dispidDoRedo = 206L,
	dispidDoSetDiagramType = 207L,
	dispidDoSetDiagramSize = 208L,
	dispidDoSetDiagramLabel = 209L,
	dispidDoSetGeotext = 210L,
	dispidDoGetLinePoint = 211L,
	dispidDoImportObjectFromFile = 212L,
	dispidDoExplodeSymbol = 213L,
	dispidDoCreateThematicMap = 214L,
	dispidDoSetProjectID = 215L,
	dispidDoGetProjectID = 216L,
	dispidDoCutPolyWithCircle = 217L,
	dispidDoCopyLayer = 218L,
	dispidDoGetPolygonArea = 219L,
	dispidDoExplodeObjects = 220L,
	dispidDoExplodePolygonsToPoints = 221L,
	dispidDoSetSelectionFilter = 222L,
	dispidDoGetPointOnLine = 223L,
	dispidDoGetObjectCenter = 224L,
	dispidDoGetObjectPropertiesList = 225L,
	dispidDoGetAttachedText = 226L,
	dispidDoSetIDBOnOff = 227L,
	dispidDoSetTooltipDB = 228L,
	dispidDoGetObjectPropertiesListEx = 229L,
	dispidDoPolyPartitionWithLine = 230L,
	dispidDoSetLayerPrintable = 231L,
	dispidDoGetLayerPolygonArea = 232L,
	dispidDoAttachText = 233L,
	dispidDoSetDiagramFont = 234L,
	dispidDoExecuteModule = 235L,
	dispidDoCreateAdvancedTextEx = 236L,
	dispidDoSetDDEOutput = 237L,
	dispidDoComparePoints = 238L,
	dispidDoGetTooltipDB = 239L,
	dispidDoSetTimeOut = 240L, 
	eventidOnDDEExecute = 1L,
	eventidOnConnect = 2L,
	eventidOnDisconnect = 3L,
	eventidOnMonitoring = 4L,
	eventidOnCloseDB = 5L,
	eventidOnShowDB = 6L,
	eventidOnProjectOpen = 7L,
	eventidOnProjectActivate = 8L,
	eventidOnProjectClose = 9L,
	eventidOnGisReady = 10L,
	eventidOnDelete = 11L,
	eventidOnCreateChart = 12L,
	eventidOnSetCaption = 13L,
	eventidOnNotifyMoveObjects = 14L,
	eventidOnNotifyMoveDimension = 15L,
	eventidOnInsert = 16L,
	eventidOnUpdate = 17L,
	eventidOnNotifyNewView = 18L,
	eventidOnLinkPointToArea = 19L,
	eventidOnCutArea = 20L,
	eventidOnChangeTable = 21L,
	eventidOnNotifyViewDeleted = 22L,
	eventidOnMacroBegin = 23L,
	eventidOnMacroEnd = 24L,
	eventidOnNotifyMainWindowReady = 25L,
	eventidOnNotifySetView = 26L,
	eventidOnDeclareLayerSettings = 27L,
	eventidOnDeclareViewSettings = 28L,
	eventidOnDeclareViewLayerSettings = 29L,
	eventidOnNotifyBoxSelection = 30L,
	eventidOnNotifyCircleSelection = 31L,
	eventidOnDeclareNextUnusedID = 32L,
	eventidOnBuildArea = 33L,
	eventidOnAsciiExport = 34L,
	eventidOnAsciiImport = 35L,
	eventidOnFreeMonitoring = 36L,
	eventidOnTimer = 37L,
	eventidOnPrintResult = 38L,
	eventidOnPrintEvents = 39L,
	eventidOnProjectSaveAs = 40L,
	eventidOnGraphicStatus = 41L,
	eventidOnActiveLayerStatus = 42L,
	eventidOnVisibleMapArea = 43L,
	eventidOnVisibleMapWidth = 44L,
	eventidOnSelectionStatus = 45L,
	eventidOnSelectionStatusXValue = 46L,
	eventidOnSelectionStatusXInfo = 47L,
	eventidOnSelectionStatusYValue = 48L,
	eventidOnSelectionStatusYInfo = 49L,
	eventidOnInsertList = 50L,
	eventidOnInvalidMapArea = 51L,
	eventidOnAsciiImportEnd = 52L,
	eventidOnCutAreaLayers = 53L,
	eventidOnError = 54L,
	eventidOnObjectProperties = 55L,
	eventidOnDistance = 56L,
	eventidOnChartColor = 57L,
	eventidOnLineStyleInfo = 58L,
	eventidOnHatchStyleInfo = 59L,
	eventidOnGPSID = 60L,
	eventidOnGPSProAsc = 61L,
	eventidOnGPSProCalc = 62L,
	eventidOnGPSSymSet = 63L,
	eventidOnAsciiImportStart = 64L,
	eventidOnAsciiExportStart = 65L,
	eventidOnAsciiExportEnd = 66L,
	eventidOnActiveLayer = 67L,
	eventidOnCopyObjects = 68L,
	eventidOnPolygonPartition = 69L,
	eventidOnActualProjectPart = 70L,
	eventidOnCanvasRedraw = 71L,
	eventidOnDeclareProjectSettings = 72L,
	eventidOnPrinterParamGet = 73L,
	eventidOnPrintPagesParamGet = 74L,
	eventidOnPrintMapParamGet = 75L,
	eventidOnPrintLegendParamGet = 76L,
	eventidOnPrintPictureParamGet = 77L,
	eventidOnPrintSignatureParamGet = 78L,
	eventidOnSendPolygonData = 80L,
	eventidOnPrintObjectsInformGet = 81L,
	eventidOnLinkTextToArea = 82L,
	eventidOnMouseAction = 83L,
	eventidOnGetCoordinates = 84L,
	eventidOnSymbolInfo = 85L,
	eventidOnSetPicture = 86L,
	eventidOnCreateObjectEx = 87L,
	eventidOnTooltipText = 88L,
	eventidOnGetLinePoint = 89L,
	eventidOnImpExpFinished = 90L,
	eventidOnCreateThematicMap = 91L,
	eventidOnGetProjectID = 92L,
	eventidOnPolyCircleCut = 93L,
	eventidOnCopyLayer = 94L,
	eventidOnObjectCenter = 95L,
	eventidOnCreateText = 96L,
	eventidOnGetAttachedText = 97L,
	eventidOnImpEnd = 98L,
	//}}AFX_DISP_ID
	//}}AFX_DISP_ID
	//}}AFX_DISP_ID
	//}}AFX_DISP_ID
	//}}AFX_DISP_ID
	//}}AFX_DISP_ID

	};
};


// *** ERROR Codes ***

long CAXWingisCtrl::SetErrorCode(long lError)
{
 switch (lError)
   {
    case AXIPCERR_NO_ERROR:
      lLastError = AXIPCERR_NO_ERROR;
      m_csLastErrorMsg = "";
      break;

    // A successful AfxSocketInit must occur before using this API.
    case AXIPCERR_WSANOTINITIALISED:  // 10093
      lLastError = AXIPCERR_WSANOTINITIALISED;
      m_csLastErrorMsg = "A successful AfxSocketInit must occur before using this API.";
      break;

    // The Windows Sockets implementation detected that the network subsystem failed.
    case AXIPCERR_WSAENETDOWN:   //10050      
      lLastError = AXIPCERR_WSAENETDOWN;
      m_csLastErrorMsg = "The Windows Sockets implementation detected that the network subsystem failed.";
      break;

    // The specified address is already in use.
    case AXIPCERR_WSAEADDRINUSE: //10048
      lLastError = AXIPCERR_WSAEADDRINUSE;
      m_csLastErrorMsg = "The specified address is already in use.";
      break;

// A blocking Windows Sockets call is in progress.
    case AXIPCERR_WSAEINPROGRESS:  //10036
      lLastError = AXIPCERR_WSAEINPROGRESS;
      m_csLastErrorMsg = "A blocking Windows Sockets call is in progress.";
      break;

// The specified address is not available from the local machine.
    case AXIPCERR_WSAEADDRNOTAVAIL:  //10049
      lLastError = AXIPCERR_WSAEADDRNOTAVAIL;
      m_csLastErrorMsg = "The specified address is not available from the local machine.";
      break;

// Addresses in the specified family cannot be used with this socket.
    case AXIPCERR_WSAEAFNOSUPPORT: //10047
      lLastError = AXIPCERR_WSAEAFNOSUPPORT;
      m_csLastErrorMsg = "Addresses in the specified family cannot be used with this socket.";
      break;

// The attempt to connect was rejected.
    case AXIPCERR_WSAECONNREFUSED:  //10061
      lLastError = AXIPCERR_WSAECONNREFUSED;
      m_csLastErrorMsg = "The attempt to connect was rejected.";
      break;

// A destination address is required.
    case AXIPCERR_WSAEDESTADDRREQ:  //10039
      lLastError = AXIPCERR_WSAEDESTADDRREQ;
      m_csLastErrorMsg = "A destination address is required.";
      break;

// The address is incorrect.
    case AXIPCERR_WSAEFAULT:  //10014
      lLastError = AXIPCERR_WSAEFAULT;
      m_csLastErrorMsg = "The address is incorrect.";
      break;

// Invalid host address.
    case AXIPCERR_WSAEINVAL:  //10022
      lLastError = AXIPCERR_WSAEINVAL;
      m_csLastErrorMsg = "Invalid host address.";
      break;

// The socket is already connected.
    case AXIPCERR_WSAEISCONN:  //10056
      lLastError = AXIPCERR_WSAEISCONN;
      m_csLastErrorMsg = "The socket is already connected.";
      break;

// No more file descriptors are available.
    case AXIPCERR_WSAEMFILE:   //10024
      lLastError = AXIPCERR_WSAEMFILE;
      m_csLastErrorMsg = "No more file descriptors are available.";
      break;

// The network cannot be reached from this host at this time.
    case AXIPCERR_WSAENETUNREACH:  //10051
      lLastError = AXIPCERR_WSAENETUNREACH;
      m_csLastErrorMsg = "The network cannot be reached from this host at this time.";
      break;

// No buffer space is available. The socket cannot be connected.
    case AXIPCERR_WSAENOBUFS:  //10055
      lLastError = AXIPCERR_WSAENOBUFS;
      m_csLastErrorMsg = "No buffer space is available. The socket cannot be connected.";
      break;

// The descriptor is not a socket.
    case AXIPCERR_WSAENOTSOCK:  //10038
      lLastError = AXIPCERR_WSAENOTSOCK;
      m_csLastErrorMsg = "The descriptor is not a socket.";
      break;

// Attempt to connect timed out without establishing a connection.
    case AXIPCERR_WSAETIMEDOUT:  //10060
      lLastError = AXIPCERR_WSAETIMEDOUT;
      m_csLastErrorMsg = "Attempt to connect timed out without establishing a connection.";
      break;

// The socket is marked as nonblocking and the connection cannot be completed immediately
    case AXIPCERR_WSAEWOULDBLOCK:  //10035    
      lLastError = AXIPCERR_WSAEWOULDBLOCK;
      m_csLastErrorMsg = "The socket is marked as nonblocking and the connection cannot be completed immediately.";
      break;

    default:      
      lLastError = AXIPCERR_UNKNOWN_ERRORCODE;
      m_csLastErrorMsg = "An unknown error was reported from the network subsystem.";
      break;
   }

 return lLastError;
}
