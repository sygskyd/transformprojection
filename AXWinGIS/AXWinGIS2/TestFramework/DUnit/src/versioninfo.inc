ReleaseNo : array[1..3] of Integer
              = (7,2,0);
ReleaseStr     = '7.2.0';
ReleaseName    = '';
ReleaseWhen : array[1..6] of Integer
              = (2003,05,07,21,19,45);
ReleaseYear    = 2003;
ReleaseMonth   = 05;
ReleaseDay     = 07;
ReleaseHour    = 21;
ReleaseMinute  = 19;

