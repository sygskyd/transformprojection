unit ApplicationSRC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, OleCtrls, AXWINGISLib_TLB, ExtCtrls, Math;

type
  TMainForm = class(TForm)
     ControlGroupBox    : TGroupBox;
       PrintBtn         : TButton;
       ConnectionLight  : TShape;
       ConnectionLab    : TLabel;
       ConnectBtn       : TButton;
       TextEdit         : TEdit;
       CreateTextBtn    : TButton;
       CreateObjBtn     : TButton;
       FileNameEdit     : TEdit;
       ToFileCheckBox   : TCheckBox;
       ToWMFCheckBox    : TCheckBox;
       DialogCheckBox     : TCheckBox;
       MonitoringCheckBox : TCheckBox;
       ProgressCheckBox   : TCheckBox;
       DevNameLbl       : TLabel;
       DeviceNameEdit   : TEdit;
       SetPrnBtn        : TButton;
       OrientGrBox      : TGroupBox;
         PortraitLbl    : TLabel;
         PortraitRBut   : TRadioButton;
         LandscapeLbl   : TLabel;
         LandscapeRBut  : TRadioButton;
       PaperEdit        : TEdit;
       MarginsGrBox     : TGroupBox;
         MarginEdit     : TEdit;
         InnerEdit      : TEdit;
         OverlapEdit    : TEdit;
     ReactionGroupBox   : TGroupBox;
       DoPrintLbl       : TLabel;
       DoPrintValLbl    : TLabel;
       CommandsGroupBox : TGroupBox;
         PrnResLbl      : TLabel;
         PrnResCmdLbl   : TLabel;
         PrnResValLbl   : TLabel;
         CmdCommentLbl  : TLabel;
         PrnMonLbl      : TLabel;
         PrnMonPageLbl  : TLabel;
         PrnMonPagesLbl : TLabel;
         PrnMonValLbl   : TLabel;
         MonCmdCmntLbl1 : TLabel;
         MonCmdCmntLbl2 : TLabel;
     AXWingis           : TAXWingis;
     ExitBtn            : TButton;
    SetPgsBtn: TButton;
    LayoutComBox: TComboBox;
    HorizEdit: TEdit;
    VerticEdit: TEdit;
    AutoCheckBox: TCheckBox;
    NumbEdit: TEdit;
    OrderCheckBox: TCheckBox;
    GetPgsBtn: TButton;
    GetPrnBtn: TButton;
    DoSetPrnLbl: TLabel;
    DoSetPrnValLbl: TLabel;
    DoSetPgsLbl: TLabel;
    DoSetPgsValLbl: TLabel;
    DoGetPgsLbl: TLabel;
    DoGetPgsValLbl: TLabel;
    DoGetPrnLbl: TLabel;
    DoGetPrnValLbl: TLabel;
    PrnSPPLbl: TLabel;
    PrnSPPValLbl: TLabel;
    PrnSPGLbl: TLabel;
    PrnSPGValLbl: TLabel;
    ControlGroupBox2: TGroupBox;
    RegionComBox: TComboBox;
    SetMapBtn: TButton;
    GetMapBtn: TButton;
    MapIDEdit: TEdit;
    ViewEdit: TEdit;
    X1Edit: TEdit;
    X1Lab: TLabel;
    Y1Edit: TEdit;
    Y1Lab: TLabel;
    X2Edit: TEdit;
    X2Lab: TLabel;
    Y2Lab: TLabel;
    Y2Edit: TEdit;
    ScaleLab: TLabel;
    ScaleEdit: TEdit;
    MapIDLab: TLabel;
    FitToPageCheckBox: TCheckBox;
    KeepScaleCheckBox: TCheckBox;
    CentrComBox: TComboBox;
    LeftLab: TLabel;
    TopLab: TLabel;
    LeftEdit: TEdit;
    TopEdit: TEdit;
    WidthLab: TLabel;
    HeightLab: TLabel;
    WidthEdit: TEdit;
    HeightEdit: TEdit;
    DoSetMapLbl: TLabel;
    DoSetMapValLbl: TLabel;
    DoGetMapLbl: TLabel;
    DoGetMapValLbl: TLabel;
    PrnSMLbl: TLabel;
    PrnSMValLbl: TLabel;
    GroupBox1: TGroupBox;
    LegendIDEdit: TEdit;
    LegendIDLab: TLabel;
    LegendNameEdit: TEdit;
    LegNameLab: TLabel;
    PositComBox: TComboBox;
    OriginComBox: TComboBox;
    LegLeftLab: TLabel;
    LegLeftEdit: TEdit;
    LegTopLab: TLabel;
    LegTopEdit: TEdit;
    LegWidthLab: TLabel;
    LegWidthEdit: TEdit;
    LegHeightLab: TLabel;
    LegHeightEdit: TEdit;
    SetLegBtn: TButton;
    GetLegBtn: TButton;
    DoSetLegLbl: TLabel;
    DoSetLegValLbl: TLabel;
    DoGetLegLbl: TLabel;
    DoGetLegValLbl: TLabel;
    PrnSLLbl: TLabel;
    PrnSLValLbl: TLabel;
    InsLegBtn: TButton;
    DoInsLegLbl: TLabel;
    DoInsLegValLbl: TLabel;
    GroupBox2: TGroupBox;
    PictureIDLab: TLabel;
    PicNameLab: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    PictureIDEdit: TEdit;
    PictureNameEdit: TEdit;
    PicPosComBox: TComboBox;
    PicOrigComBox: TComboBox;
    PicLeftEdit: TEdit;
    PicTopEdit: TEdit;
    PicWidthEdit: TEdit;
    PicHeightEdit: TEdit;
    SetPicBtn: TButton;
    GetPicBtn: TButton;
    InsPicBtn: TButton;
    KeepAspectCheckBox: TCheckBox;
    PicLocGrBox: TGroupBox;
    OneTimePicLab: TLabel;
    OnPagesPicLab: TLabel;
    OneTimePicRBut: TRadioButton;
    OnPagesPicRBut: TRadioButton;
    PicFirstChkBox: TCheckBox;
    PicIntChkBox: TCheckBox;
    PicLastChkBox: TCheckBox;
    PrnSPLbl: TLabel;
    PrnSPValLbl: TLabel;
    DoSetPicLbl: TLabel;
    DoSetPicValLbl: TLabel;
    DoGetPicLbl: TLabel;
    DoGetPicValLbl: TLabel;
    DoInsPicLbl: TLabel;
    DoInsPicValLbl: TLabel;
    PercentCheckBox: TCheckBox;
    GroupBox3: TGroupBox;
    SignatureIDLab: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    SignatureIDEdit: TEdit;
    SiganatureEdit: TEdit;
    SigPosComBox: TComboBox;
    SigOrigComBox: TComboBox;
    SigLeftEdit: TEdit;
    SigTopEdit: TEdit;
    SetSigBtn: TButton;
    GetSigBtn: TButton;
    InsSigBtn: TButton;
    SigLocGrBox: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    OneTimeSigRBut: TRadioButton;
    OnPagesSigRBut: TRadioButton;
    SigFirstChkBox: TCheckBox;
    SigIntChkBox: TCheckBox;
    SigLastChkBox: TCheckBox;
    GroupBox4: TGroupBox;
    FontNameEdit: TEdit;
    TextAlignComBox: TComboBox;
    ItalicChkBox: TCheckBox;
    BoldChkBox: TCheckBox;
    TranspChkBox: TCheckBox;
    UndlChkBox: TCheckBox;
    Label1: TLabel;
    FontSizeEdit: TEdit;
    FontColorEdit: TEdit;
    Label2: TLabel;
    Label9: TLabel;
    FontBackColorEdit: TEdit;
    DoSetSigLbl: TLabel;
    DoSetSigValLbl: TLabel;
    DoGetSigLbl: TLabel;
    DoGetSigValLbl: TLabel;
    DoInsSigLbl: TLabel;
    DoInsSigValLbl: TLabel;
    PrnSSLbl: TLabel;
    PrnSSValLbl: TLabel;
    GroupBox5: TGroupBox;
    OptEditLbl: TLabel;
    TempNameLab: TLabel;
    OptEdit: TEdit;
    TemplateNameEdit: TEdit;
    SaveTempBtn: TButton;
    OpenTempBtn: TButton;
    DoStrTmpLbl: TLabel;
    DoStrTmpValLbl: TLabel;
    DoRstTmpLbl: TLabel;
    DoRstTmpValLbl: TLabel;
    PrnSTLbl: TLabel;
    PrnSTValLbl: TLabel;
    PrnRTLbl: TLabel;
    PrnRTValLbl: TLabel;
    GroupBox6: TGroupBox;
    ObjectsListBox: TListBox;
    GetObjInfBtn: TButton;
    Label10: TLabel;
    MapNumEdit: TEdit;
    Label13: TLabel;
    LegNumEdit: TEdit;
    PicNumEdit: TEdit;
    Label14: TLabel;
    Label15: TLabel;
    SigNumEdit: TEdit;
    DoGetObjLbl: TLabel;
    DoGetObjValLbl: TLabel;
    PrnSOILbl: TLabel;
    PrnSOIValLbl: TLabel;
    Label16: TLabel;
    ObjNumEdit: TEdit;
    Label17: TLabel;
    DoDelObjLbl: TLabel;
    DoDelObjValLbl: TLabel;
    DoMovObjLbl: TLabel;
    DoMovObjValLbl: TLabel;
    GroupBox7: TGroupBox;
    Label18: TLabel;
    ObjectIDEdit: TEdit;
    DelObjBtn: TButton;
    MovObjBtn: TButton;
    OptionsComBox: TComboBox;
    OptLbl: TLabel;
    Label19: TLabel;
    TypeComBox: TComboBox;
    procedure PrintBtnClick(Sender: TObject);
    procedure ExitBtnClick(Sender: TObject);
    procedure CreateTextBtnClick(Sender: TObject);
    procedure AXWingisConnect(Sender: TObject);
    procedure AXWingisDisconnect(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ConnectBtnClick(Sender: TObject);
    procedure New(Sender: TObject);
    procedure ToFileCheckBoxClick(Sender: TObject);
    procedure ToWMFCheckBoxClick(Sender: TObject);
    procedure AXWingisPrintResult(Sender: TObject;
      const sCommandName: WideString; lResult: Integer);
    procedure DialogCheckBoxClick(Sender: TObject);
    procedure AXWingisPrintEvents(Sender: TObject; lPage, lOfPages,
      lResult: Integer);
    procedure SetPrnBtnClick(Sender: TObject);
    procedure SetPgsBtnClick(Sender: TObject);
    procedure GetPrnBtnClick(Sender: TObject);
    procedure GetPgsBtnClick(Sender: TObject);
    procedure AXWingisPrinterParamGet(Sender: TObject; const sDeviceName,
      sPaperFormat: WideString; lOrientation, lErrorCode: Integer);
    procedure AXWingisPrintPagesParamGet(Sender: TObject; lModel,
      lRepaginate, lHorizPages, lVerticPages: Integer; dMrLeft, dMrBottom,
      dMrRight, dMrTop, dInLeft, dInBottom, dInRight, dInTop, dOvLeft,
      dOvBottom, dOvRight, dOvTop: Double; lNumbOrder, lNumbFrom,
      lErrorCode: Integer);
    procedure SetMapBtnClick(Sender: TObject);
    procedure GetMapBtnClick(Sender: TObject);
    procedure AXWingisPrintMapParamGet(Sender: TObject; lMapID,
      lRegion: Integer; const sViewName: WideString; dRegionX1, dRegionY1,
      dRegionX2, dRegionY2: Double; bFitToPage: WordBool;
      dPrintScale: Double; bKeepScale: WordBool; dFrameWidth,
      dFrameHeight: Double; lFrameCentering: Integer; dFrameLeft,
      dFrameTop: Double; lErrorCode: Integer);
    procedure FitToPageCheckBoxClick(Sender: TObject);
    procedure KeepScaleCheckBoxClick(Sender: TObject);
    procedure GetLegBtnClick(Sender: TObject);
    procedure InsLegBtnClick(Sender: TObject);
    procedure SetLegBtnClick(Sender: TObject);
    procedure AXWingisPrintLegendParamGet(Sender: TObject;
      lLegendID: Integer; const sLegendName: WideString; dFrameWidth,
      dFrameHeight: Double; lFrameCentering, lFrameOridgin: Integer;
      dFrameLeft, dFrameTopOrBot: Double; lErrorCode: Integer);
    procedure PicLocRButClick(Sender: TObject);
    procedure SetPicBtnClick(Sender: TObject);
    procedure GetPicBtnClick(Sender: TObject);
    procedure InsPicBtnClick(Sender: TObject);
    procedure AXWingisPrintPictureParamGet(Sender: TObject;
      lPictureID: Integer; const sPictureFile: WideString;
      lPictureLocation: Integer; bKeepAspect: WordBool; dFrameWidth,
      dFrameHeight: Double; lFrameCentering, lFrameOridgin: Integer;
      dFrameLeft, dFrameTopOrBot: Double; lErrorCode: Integer);
    procedure SigLocRButClick(Sender: TObject);
    procedure SetSigBtnClick(Sender: TObject);
    procedure InsSigBtnClick(Sender: TObject);
    procedure GetSigBtnClick(Sender: TObject);
    procedure AXWingisPrintSignatureParamGet(Sender: TObject;
      lSignatureID: Integer; const sSignature, sFontName: WideString;
      bItalic, bBold, bUnderlined, bTransparent: WordBool; lFontSize,
      lFontColor, lFontBackColor, lTextAlignment: Integer;
      bOnPagesLocation, bOnFirstPage, bOnInternalPages,
      bOnLastPage: WordBool; lFrameCentering, lFrameOridgin: Integer;
      dFrameLeft, dFrameTopOrBot: Double; lErrorCode: Integer);
    procedure SaveTempBtnClick(Sender: TObject);
    procedure OpenTempBtnClick(Sender: TObject);
    procedure GetObjInfBtnClick(Sender: TObject);
    procedure AXWingisPrintObjectsInformGet(Sender: TObject;
      lObjectsNumber, lMapsNumber, lLegendsNumber, lPicturesNumber,
      lSignaturesNumber, hTypesList, hObjectsIDList, lErrorCode: Integer);
    procedure MovObjBtnClick(Sender: TObject);
    procedure DelObjBtnClick(Sender: TObject);
  private
    { Private declarations }
    procedure ResetControls();
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.DFM}
procedure TMainForm.FormCreate(Sender: TObject);
begin
   LayoutComBox.Items.Add('Unique');
   LayoutComBox.Items.Add('United');
   LayoutComBox.Items.Add('Book');
   LayoutComBox.ItemIndex:=0;
   RegionComBox.Items.Add('Whole project');
   RegionComBox.Items.Add('Saved view');
   RegionComBox.Items.Add('Current view');
   RegionComBox.Items.Add('Region');
   RegionComBox.ItemIndex:=0;
   CentrComBox.Items.Add('Custom');
   CentrComBox.Items.Add('Horizontally');
   CentrComBox.Items.Add('Vertically');
   CentrComBox.Items.Add('Center');
   CentrComBox.ItemIndex:=0;
   PositComBox.Items.Add('Custom');
   PositComBox.Items.Add('Horizontally');
   PositComBox.Items.Add('Vertically');
   PositComBox.Items.Add('Center');
   PositComBox.ItemIndex:=0;
   OriginComBox.Items.Add('Left and top');
   OriginComBox.Items.Add('Left and bottom');
   OriginComBox.ItemIndex:=0;
   PicPosComBox.Items.Add('Custom');
   PicPosComBox.Items.Add('Horizontally');
   PicPosComBox.Items.Add('Vertically');
   PicPosComBox.Items.Add('Center');
   PicPosComBox.ItemIndex:=0;
   PicOrigComBox.Items.Add('Left and top');
   PicOrigComBox.Items.Add('Left and bottom');
   PicOrigComBox.ItemIndex:=0;
   SigPosComBox.Items.Add('Custom');
   SigPosComBox.Items.Add('Horizontally');
   SigPosComBox.Items.Add('Vertically');
   SigPosComBox.Items.Add('Center');
   SigPosComBox.ItemIndex:=0;
   SigOrigComBox.Items.Add('Left and top');
   SigOrigComBox.Items.Add('Left and bottom');
   SigOrigComBox.ItemIndex:=0;
   TextAlignComBox.Items.Add('Left aligned');
   TextAlignComBox.Items.Add('Centered');
   TextAlignComBox.Items.Add('Right aligned');
   TextAlignComBox.ItemIndex:=0;
   TypeComBox.Items.Add('Map');
   TypeComBox.Items.Add('Legend');
   TypeComBox.Items.Add('Picture');
   TypeComBox.Items.Add('Signature');
   TypeComBox.ItemIndex:=0;
   OptionsComBox.Items.Add('Send to Back');
   OptionsComBox.Items.Add('Send Backward');
   OptionsComBox.Items.Add('Bring Forward');
   OptionsComBox.Items.Add('Bring to Front');
   OptionsComBox.ItemIndex:=0;
   AXWingis.bUseCOM:=TRUE;
   TextEdit.Text:='TEST text';
   CreateTextBtn.Enabled:=FALSE;
   CreateObjBtn.Enabled:=FALSE;
   PrintBtn.Enabled:=FALSE;
   SetPrnBtn.Enabled:=FALSE;
   SetPgsBtn.Enabled:=FALSE;
   SetMapBtn.Enabled:=FALSE;
   GetPrnBtn.Enabled:=FALSE;
   GetPgsBtn.Enabled:=FALSE;
   GetMapBtn.Enabled:=FALSE;
   InsLegBtn.Enabled:=FALSE;
   SetLegBtn.Enabled:=FALSE;
   GetLegBtn.Enabled:=FALSE;
   InsPicBtn.Enabled:=FALSE;
   SetPicBtn.Enabled:=FALSE;
   GetPicBtn.Enabled:=FALSE;
   InsSigBtn.Enabled:=FALSE;
   SetSigBtn.Enabled:=FALSE;
   GetSigBtn.Enabled:=FALSE;
   SaveTempBtn.Enabled:=FALSE;
   OpenTempBtn.Enabled:=FALSE;
   GetObjInfBtn.Enabled:=FALSE;
   MovObjBtn.Enabled:=FALSE;
   DelObjBtn.Enabled:=FALSE;
   PrnResCmdLbl.Caption:='';
   PrnResValLbl.Caption:='';
   MonCmdCmntLbl2.Caption:='';
   PrnMonPageLbl.Caption:='';
   PrnMonPagesLbl.Caption:='';
   PrnMonValLbl.Caption:='';
   PrnSPPValLbl.Caption:='';
   PrnSPGValLbl.Caption:='';
   FontColorEdit.Text:=IntToStr(clBlack);
   FontBackColorEdit.Text:=IntToStr(clGreen);
end;

procedure TMainForm.ResetControls();
begin
   PrnResCmdLbl.Caption:='';
   PrnResValLbl.Caption:='';
   CmdCommentLbl.Caption:='No comments';
   PrnResLbl.Font.Color:=clGrayText;
   PrnResCmdLbl.Font.Color:=clGrayText;
   PrnResValLbl.Font.Color:=clGrayText;
   CmdCommentLbl.Font.Color:=clGrayText;

   PrnMonLbl.Font.Color:=clGrayText;
   MonCmdCmntLbl1.Font.Color:=clGrayText;
   MonCmdCmntLbl2.Font.Color:=clGrayText;
   PrnMonPageLbl.Caption:='';
   PrnMonPagesLbl.Caption:='';
   PrnMonValLbl.Caption:='';
   MonCmdCmntLbl1.Caption:='No comments';
   MonCmdCmntLbl2.Caption:='';

   PrnSPPLbl.Font.Color:=clGrayText;
   PrnSPPValLbl.Font.Color:=clGrayText;
   PrnSPPValLbl.Caption:='';
   PrnSPGLbl.Font.Color:=clGrayText;
   PrnSPGValLbl.Font.Color:=clGrayText;
   PrnSPGValLbl.Caption:='';
   PrnSMLbl.Font.Color:=clGrayText;
   PrnSMValLbl.Font.Color:=clGrayText;
   PrnSMValLbl.Caption:='';
   PrnSLLbl.Font.Color:=clGrayText;
   PrnSLValLbl.Font.Color:=clGrayText;
   PrnSLValLbl.Caption:='';
   PrnSPLbl.Font.Color:=clGrayText;
   PrnSPValLbl.Font.Color:=clGrayText;
   PrnSPValLbl.Caption:='';
   PrnSSLbl.Font.Color:=clGrayText;
   PrnSSValLbl.Font.Color:=clGrayText;
   PrnSSValLbl.Caption:='';
   PrnSTLbl.Font.Color:=clGrayText;
   PrnSTValLbl.Font.Color:=clGrayText;
   PrnSTValLbl.Caption:='';
   PrnRTLbl.Font.Color:=clGrayText;
   PrnRTValLbl.Font.Color:=clGrayText;
   PrnRTValLbl.Caption:='';
   PrnSOILbl.Font.Color:=clGrayText;
   PrnSOIValLbl.Font.Color:=clGrayText;
   PrnSOIValLbl.Caption:='';

   DoPrintLbl.Font.Color:=clGrayText;
   DoPrintValLbl.Font.Color:=clGrayText;
   DoSetPrnLbl.Font.Color:=clGrayText;
   DoSetPrnValLbl.Font.Color:=clGrayText;
   DoGetPrnLbl.Font.Color:=clGrayText;
   DoGetPrnValLbl.Font.Color:=clGrayText;
   DoSetPgsLbl.Font.Color:=clGrayText;
   DoSetPgsValLbl.Font.Color:=clGrayText;
   DoGetPgsLbl.Font.Color:=clGrayText;
   DoGetPgsValLbl.Font.Color:=clGrayText;
   DoSetMapLbl.Font.Color:=clGrayText;
   DoSetMapValLbl.Font.Color:=clGrayText;
   DoGetMapLbl.Font.Color:=clGrayText;
   DoGetMapValLbl.Font.Color:=clGrayText;
   DoSetLegLbl.Font.Color:=clGrayText;
   DoSetLegValLbl.Font.Color:=clGrayText;
   DoGetLegLbl.Font.Color:=clGrayText;
   DoGetLegValLbl.Font.Color:=clGrayText;
   DoInsLegLbl.Font.Color:=clGrayText;
   DoInsLegValLbl.Font.Color:=clGrayText;
   DoSetPicLbl.Font.Color:=clGrayText;
   DoSetPicValLbl.Font.Color:=clGrayText;
   DoGetPicLbl.Font.Color:=clGrayText;
   DoGetPicValLbl.Font.Color:=clGrayText;
   DoInsPicLbl.Font.Color:=clGrayText;
   DoInsPicValLbl.Font.Color:=clGrayText;
   DoSetSigLbl.Font.Color:=clGrayText;
   DoSetSigValLbl.Font.Color:=clGrayText;
   DoGetSigLbl.Font.Color:=clGrayText;
   DoGetSigValLbl.Font.Color:=clGrayText;
   DoInsSigLbl.Font.Color:=clGrayText;
   DoInsSigValLbl.Font.Color:=clGrayText;
   DoStrTmpLbl.Font.Color:=clGrayText;
   DoStrTmpValLbl.Font.Color:=clGrayText;
   DoRstTmpLbl.Font.Color:=clGrayText;
   DoRstTmpValLbl.Font.Color:=clGrayText;
   DoGetObjLbl.Font.Color:=clGrayText;
   DoGetObjValLbl.Font.Color:=clGrayText;
   DoDelObjLbl.Font.Color:=clGrayText;
   DoDelObjValLbl.Font.Color:=clGrayText;
   DoMovObjLbl.Font.Color:=clGrayText;
   DoMovObjValLbl.Font.Color:=clGrayText;
end;


procedure TMainForm.PrintBtnClick(Sender: TObject);
var intResult        : Integer;
    Mode             : Integer;
    sFileName        : WideString;
    bCollateCopies   : WordBool;
    bShowProgress    : WordBool;
    bPrintMonitoring : WordBool;
begin

   ResetControls();

   bCollateCopies:=FALSE;
   sFileName:=FileNameEdit.Text;
   if ToFileCheckBox.Checked then
      Mode:=2
   else
      if ToWMFCheckBox.Checked then
         Mode:=4
      else
         Mode:=0;
   if DialogCheckBox.Checked then
      Mode:=Mode+1;
   bShowProgress:=ProgressCheckBox.Checked;
   bPrintMonitoring:=MonitoringCheckBox.Checked;

   intResult:=AXWingis.DoPrint(Mode, sFileName, 1, bCollateCopies, bShowProgress, bPrintMonitoring);
   DoPrintValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoPrintLbl.Font.Color:=clBlack;
         DoPrintValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoPrintLbl.Font.Color:=clRed;
         DoPrintValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.SetPrnBtnClick(Sender: TObject);
var intResult        : Integer;
    sDeviceName      : WideString;
    sPaperFormatName : WideString;
    intOrientation   : Integer;
begin

   ResetControls();

   sDeviceName:=DeviceNameEdit.Text;
   sPaperFormatName:=PaperEdit.Text;
   if PortraitRBut.Checked then
      intOrientation:=0
   else
      intOrientation:=1;
   intResult:=AXWingis.DoPrintSetPrintParam(sDeviceName,sPaperFormatName,intOrientation);
   DoSetPrnValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoSetPrnLbl.Font.Color:=clBlack;
         DoSetPrnValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoSetPrnLbl.Font.Color:=clRed;
         DoSetPrnValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.SetPgsBtnClick(Sender: TObject);
var intResult        : Integer;
    lLayoutModel     : Integer;
    bAutoRepagination: WordBool;
    lHorizontalPages : Integer;
    lVerticalPages   : Integer;
    dMarginsLeft     : Double;
    dMarginsBottom   : Double;
    dMarginsRight    : Double;
    dMarginsTop      : Double;
    dInnerLeft       : Double;
    dInnerBottom     : Double;
    dInnerRight      : Double;
    dInnerTop        : Double;
    dOverlapLeft     : Double;
    dOverlapBottom   : Double;
    dOverlapRight    : Double;
    dOverlapTop      : Double;
    bNumberingOrder  : WordBool;
    lNumberingFrom   : Integer;
begin

   ResetControls();

   lLayoutModel:=LayoutComBox.ItemIndex;
   bAutoRepagination:=AutoCheckBox.Checked;
   lHorizontalPages:=StrToInt(HorizEdit.Text);
   lVerticalPages:=StrToInt(VerticEdit.Text);
   dMarginsLeft:=StrToFloat(MarginEdit.Text);
   dMarginsBottom:=dMarginsLeft;
   dMarginsRight:=dMarginsLeft;
   dMarginsTop:=dMarginsLeft;
   dInnerLeft:=StrToFloat(InnerEdit.Text);
   dInnerBottom:=dInnerLeft;
   dInnerRight:=dInnerLeft;
   dInnerTop:=dInnerLeft;
   dOverlapLeft:=StrToFloat(OverlapEdit.Text);
   dOverlapBottom:=dOverlapLeft;
   dOverlapRight:=dOverlapLeft;
   dOverlapTop:=dOverlapLeft;
   bNumberingOrder:=OrderCheckBox.Checked;
   lNumberingFrom:=StrToInt(NumbEdit.Text);
   intResult:=AXWingis.DoPrintSetPagesParam(lLayoutModel,bAutoRepagination,lHorizontalPages,lVerticalPages,
              dMarginsLeft,dMarginsBottom,dMarginsRight,dMarginsTop,
              dInnerLeft,dInnerBottom,dInnerRight,dInnerTop,
              dOverlapLeft,dOverlapBottom,dOverlapRight,dOverlapTop,
              bNumberingOrder,lNumberingFrom);
   DoSetPgsValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoSetPgsLbl.Font.Color:=clBlack;
         DoSetPgsValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoSetPgsLbl.Font.Color:=clRed;
         DoSetPgsValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.GetPrnBtnClick(Sender: TObject);
var intResult        : Integer;
begin

   ResetControls();

   intResult:=AXWingis.DoPrintGetPrintParam;
   DoGetPrnValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoGetPrnLbl.Font.Color:=clBlack;
         DoGetPrnValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoGetPrnLbl.Font.Color:=clRed;
         DoGetPrnValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.GetPgsBtnClick(Sender: TObject);
var intResult        : Integer;
begin

   ResetControls();

   intResult:=AXWingis.DoPrintGetPagesParam;
   DoGetPgsValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoGetPgsLbl.Font.Color:=clBlack;
         DoGetPgsValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoGetPgsLbl.Font.Color:=clRed;
         DoGetPgsValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.ExitBtnClick(Sender: TObject);
begin
   if AXWingis.bConnected then
      AXWingis.bConnected:=FALSE;
   AXWingis.Destroy;
   Close;
end;

procedure TMainForm.CreateTextBtnClick(Sender: TObject);
var strResult: WideString;
begin
   if TextEdit.Text = '' then
      TextEdit.Text:='TEST text';
   strResult:=AXWingis.DoCreateText(500, 500, 50, TextEdit.Text);
   if strResult <> '' then
      strResult:='';
end;

procedure TMainForm.New(Sender: TObject);
var strResult: WideString;
begin
   if TextEdit.Text = '' then
      TextEdit.Text:='TEST text';
   strResult:=AXWingis.DoCreateObject('00', 0, 500, 500, '1', 100, 50);
   if strResult <> '' then
      strResult:='';
end;

procedure TMainForm.AXWingisConnect(Sender: TObject);
begin
   Beep;
   ConnectionLight.Brush.Color:=clGreen;
   ConnectBtn.Caption:='Disconnect';
   CreateTextBtn.Enabled:=TRUE;
   CreateObjBtn.Enabled:=TRUE;
   PrintBtn.Enabled:=TRUE;
   SetPrnBtn.Enabled:=TRUE;
   SetPgsBtn.Enabled:=TRUE;
   SetMapBtn.Enabled:=TRUE;
   InsLegBtn.Enabled:=TRUE;
   SetLegBtn.Enabled:=TRUE;
   GetPrnBtn.Enabled:=TRUE;
   GetPgsBtn.Enabled:=TRUE;
   GetMapBtn.Enabled:=TRUE;
   GetLegBtn.Enabled:=TRUE;
   InsPicBtn.Enabled:=TRUE;
   SetPicBtn.Enabled:=TRUE;
   GetPicBtn.Enabled:=TRUE;
   InsSigBtn.Enabled:=TRUE;
   SetSigBtn.Enabled:=TRUE;
   GetSigBtn.Enabled:=TRUE;
   SaveTempBtn.Enabled:=TRUE;
   OpenTempBtn.Enabled:=TRUE;
   GetObjInfBtn.Enabled:=TRUE;
   MovObjBtn.Enabled:=TRUE;
   DelObjBtn.Enabled:=TRUE;
end;

procedure TMainForm.AXWingisDisconnect(Sender: TObject);
begin
   Beep;
   ConnectionLight.Brush.Color:=clRed;
   ConnectBtn.Caption:='Connect';
   CreateObjBtn.Enabled:=FALSE;
   PrintBtn.Enabled:=FALSE;
   SetPrnBtn.Enabled:=FALSE;
   SetPgsBtn.Enabled:=FALSE;
   SetMapBtn.Enabled:=FALSE;
   InsLegBtn.Enabled:=FALSE;
   SetLegBtn.Enabled:=FALSE;
   GetPrnBtn.Enabled:=FALSE;
   GetPgsBtn.Enabled:=FALSE;
   GetMapBtn.Enabled:=FALSE;
   GetLegBtn.Enabled:=FALSE;
   InsPicBtn.Enabled:=FALSE;
   SetPicBtn.Enabled:=FALSE;
   GetPicBtn.Enabled:=FALSE;
   InsSigBtn.Enabled:=FALSE;
   SetSigBtn.Enabled:=FALSE;
   GetSigBtn.Enabled:=FALSE;
   SaveTempBtn.Enabled:=FALSE;
   OpenTempBtn.Enabled:=FALSE;
   GetObjInfBtn.Enabled:=FALSE;
   MovObjBtn.Enabled:=FALSE;
   DelObjBtn.Enabled:=FALSE;
end;

procedure TMainForm.ConnectBtnClick(Sender: TObject);
var boolResult: WordBool;
begin
   if AXWingis.bConnected then
      begin
         AXWingis.bConnected:=FALSE;
         ResetControls();
      end
   else
      begin
         if not AXWingis.bActive then
            AXWingis.bActive:=TRUE;
         boolResult:=AXWingis.DoConnectEx('TEST Application','Index','[TRUE,TRUE,FALSE][All Layers,TRUE,FALSE,FALSE,FALSE][1,1,1,1,1,1]')
      end;
end;

procedure TMainForm.ToFileCheckBoxClick(Sender: TObject);
begin
   if ToFileCheckBox.Checked then
      if ToWMFCheckBox.Checked then
         ToWMFCheckBox.Checked:=FALSE;
end;

procedure TMainForm.ToWMFCheckBoxClick(Sender: TObject);
begin
   if ToWMFCheckBox.Checked then
      if ToFileCheckBox.Checked then
         ToFileCheckBox.Checked:=FALSE;
end;

procedure TMainForm.AXWingisPrintResult(Sender: TObject;
          const sCommandName: WideString; lResult: Integer);
var varColor: TColor;
begin
   PrnResCmdLbl.Caption:='[' + sCommandName + ']';
   PrnResValLbl.Caption:='[' + IntToStr(lResult) + ']';
   case lResult of
      0 : begin
             varColor:=clGreen;
             CmdCommentLbl.Caption:='Everything is all right';
          end;
      1 : begin
             varColor:=clRed;
             if sCommandName = 'PRN' then
                CmdCommentLbl.Caption:='Parameter Mode is invalid'
             else
                if sCommandName = 'PRNSPP' then
                   CmdCommentLbl.Caption:='Parameter DeviceName is invalid'
                else
                   if sCommandName = 'PRNSPG' then
                      CmdCommentLbl.Caption:='Parameter LayoutModel is invalid'
                   else
                      if (sCommandName = 'PRNSM') or (sCommandName = 'PRNSL') or
                         (sCommandName = 'PRNSP') or (sCommandName = 'PRNSS') then
                         CmdCommentLbl.Caption:='Invalid object identification number'
                      else
                         if sCommandName = 'PRNIL' then
                            CmdCommentLbl.Caption:='Parameter LegendName is invalid'
                         else
                            if sCommandName = 'PRNIP' then
                               CmdCommentLbl.Caption:='Parameter PictureFile is invalid'
                            else
                               if sCommandName = 'PRNIS' then
                                  CmdCommentLbl.Caption:='Parameter Signature is invalid'
                               else
                                  if (sCommandName = 'PRNST') or (sCommandName = 'PRNRT') then
                                     CmdCommentLbl.Caption:='Parameter FileName is invalid'
                                  else
                                     if (sCommandName = 'PRNDO') or (sCommandName = 'PRNMO') then
                                        CmdCommentLbl.Caption:='Parameter ObjectType is invalid'
                                     else
                                        CmdCommentLbl.Caption:='Unknown command';
          end;
      2 : begin
             varColor:=clRed;
             if sCommandName = 'PRN' then
                CmdCommentLbl.Caption:='Parameter FileName is invalid'
             else
                if sCommandName = 'PRNSPP' then
                   CmdCommentLbl.Caption:='Parameter PaperFormat is invalid'
                else
                   if sCommandName = 'PRNSPG' then
                      CmdCommentLbl.Caption:='Parameter AutoRepagination is invalid'
                   else
                      if sCommandName = 'PRNSM' then
                         CmdCommentLbl.Caption:='Parameter PrintingRegion is invalid'
                      else
                         if (sCommandName = 'PRNIL') or (sCommandName = 'PRNSL') then
                            CmdCommentLbl.Caption:='Parameter FrameSize is invalid'
                         else
                            if (sCommandName = 'PRNIP') or (sCommandName = 'PRNSP') then
                               CmdCommentLbl.Caption:='Parameter PictureLocation is invalid'
                            else
                               if sCommandName = 'PRNSS' then
                                  CmdCommentLbl.Caption:='Parameter Signature is invalid'
                               else
                                  if sCommandName = 'PRNIS' then
                                     CmdCommentLbl.Caption:='Parameter FontName is invalid'
                                  else
                                     if sCommandName = 'PRNST' then
                                        CmdCommentLbl.Caption:='Parameter Options is invalid'
                                     else
                                        if sCommandName = 'PRNRT' then
                                           CmdCommentLbl.Caption:='No such file or directory'
                                        else
                                           if (sCommandName = 'PRNDO') or (sCommandName = 'PRNMO') then
                                              CmdCommentLbl.Caption:='Parameter ObjectID is invalid'
                                           else
                                              CmdCommentLbl.Caption:='Unknown command';
          end;
      3 : begin
             varColor:=clRed;
             if sCommandName = 'PRN' then
                CmdCommentLbl.Caption:='Parameter Copies is invalid'
             else
                if sCommandName = 'PRNSPP' then
                   CmdCommentLbl.Caption:='Parameter Orientation is invalid'
                else
                   if sCommandName = 'PRNSPG' then
                      CmdCommentLbl.Caption:='Parameter HorizontalPages is invalid'
                   else
                      if sCommandName = 'PRNSM' then
                         CmdCommentLbl.Caption:='Parameter ViewName is invalid'
                      else
                         if (sCommandName = 'PRNIL') or (sCommandName = 'PRNSL') then
                            CmdCommentLbl.Caption:='Parameter FrameCentering is invalid'
                         else
                            if (sCommandName = 'PRNIP') or (sCommandName = 'PRNSP') then
                               CmdCommentLbl.Caption:='Parameter FrameSizeInterpretation is invalid'
                            else
                               if sCommandName = 'PRNSS' then
                                  CmdCommentLbl.Caption:='Parameter FontName is invalid'
                               else
                                  if sCommandName = 'PRNIS' then
                                     CmdCommentLbl.Caption:='Parameter FontStyle is invalid'
                                  else
                                     if sCommandName = 'PRNST' then
                                        CmdCommentLbl.Caption:='Mentioned file already exist and value of the parameter Options is equal 0'
                                     else
                                        if sCommandName = 'PRNRT' then
                                           CmdCommentLbl.Caption:='Can''t load file or mentioned file is not a template'
                                        else
                                           if sCommandName = 'PRNMO' then
                                              CmdCommentLbl.Caption:='Parameter MoveOptions is invalid'
                                           else
                                              CmdCommentLbl.Caption:='Unknown command';
          end;
      4 : begin
             varColor:=clRed;
             if sCommandName = 'PRN' then
                CmdCommentLbl.Caption:='Parameter CollateCopies is invalid'
             else
                if sCommandName = 'PRNSPG' then
                   CmdCommentLbl.Caption:='Parameter VerticalPages is invalid'
                else
                   if sCommandName = 'PRNSM' then
                      CmdCommentLbl.Caption:='Parameter RegionBorders is invalid'
                   else
                      if (sCommandName = 'PRNIL') or (sCommandName = 'PRNSL') then
                         CmdCommentLbl.Caption:='Parameter FrameOridgin is invalid'
                      else
                         if (sCommandName = 'PRNIP') or (sCommandName = 'PRNSP') then
                            CmdCommentLbl.Caption:='Parameter FrameCentering is invalid'
                         else
                            if sCommandName = 'PRNSS' then
                               CmdCommentLbl.Caption:='Parameter FontStyle is invalid'
                            else
                               if sCommandName = 'PRNIS' then
                                  CmdCommentLbl.Caption:='Parameter FontSize is invalid'
                               else
                                  if sCommandName = 'PRNST' then
                                     CmdCommentLbl.Caption:='Can''t create file because of any reason'
                                  else
                                     if sCommandName = 'PRNMO' then
                                        CmdCommentLbl.Caption:='The operation defined by the MoveOptions parameter is impossible for the mentioned printing object'
                                     else
                                        CmdCommentLbl.Caption:='Unknown command';
          end;
      5 : begin
             varColor:=clRed;
             if sCommandName = 'PRN' then
                CmdCommentLbl.Caption:='Parameter ShowProgress is invalid'
             else
                if sCommandName = 'PRNSPG' then
                   CmdCommentLbl.Caption:='Parameter Margins is invalid'
                else
                   if sCommandName = 'PRNSM' then
                      CmdCommentLbl.Caption:='Parameter FitToPage is invalid'
                   else
                      if (sCommandName = 'PRNIL') or (sCommandName = 'PRNSL') then
                         CmdCommentLbl.Caption:='Parameter FramePosition is invalid'
                      else
                         if (sCommandName = 'PRNIP') or (sCommandName = 'PRNSP') then
                            CmdCommentLbl.Caption:='Parameter FrameOridgin is invalid'
                         else
                            if sCommandName = 'PRNSS' then
                               CmdCommentLbl.Caption:='Parameter FontSize is invalid'
                            else
                               if sCommandName = 'PRNIS' then
                                  CmdCommentLbl.Caption:='Parameter FontColor is invalid'
                               else
                                  CmdCommentLbl.Caption:='Unknown command';
          end;
      6 : begin
             varColor:=clRed;
             if sCommandName = 'PRN' then
                CmdCommentLbl.Caption:='Parameter PrintMonitoring is invalid'
             else
                if sCommandName = 'PRNSPG' then
                   CmdCommentLbl.Caption:='Parameter InnerMargins is invalid'
                else
                   if sCommandName = 'PRNSM' then
                      CmdCommentLbl.Caption:='Parameter PrintScale is invalid'
                   else
                      if (sCommandName = 'PRNIP') or (sCommandName = 'PRNSP') then
                         CmdCommentLbl.Caption:='Parameter FramePosition is invalid'
                      else
                         if sCommandName = 'PRNSS' then
                            CmdCommentLbl.Caption:='Parameter FontColor is invalid'
                         else
                            if sCommandName = 'PRNIS' then
                               CmdCommentLbl.Caption:='Parameter TextAlignment is invalid'
                            else
                               CmdCommentLbl.Caption:='Unknown command';
          end;
      7 : begin
             varColor:=clRed;
             if sCommandName = 'PRNSPG' then
                CmdCommentLbl.Caption:='Parameter Overlap is invalid'
             else
                if sCommandName = 'PRNSM' then
                   CmdCommentLbl.Caption:='Parameter KeepScale is invalid'
                else
                   if sCommandName = 'PRNSS' then
                      CmdCommentLbl.Caption:='Parameter TextAlignment is invalid'
                   else
                      if sCommandName = 'PRNIS' then
                         CmdCommentLbl.Caption:='Parameter SignatureLocation is invalid'
                      else
                         CmdCommentLbl.Caption:='Unknown command';
          end;
      8 : begin
             varColor:=clRed;
             if sCommandName = 'PRNSPG' then
                CmdCommentLbl.Caption:='Parameter NumberingOrder is invalid'
             else
                if sCommandName = 'PRNSM' then
                   CmdCommentLbl.Caption:='Parameter FrameSize is invalid'
                else
                   if sCommandName = 'PRNSS' then
                      CmdCommentLbl.Caption:='Parameter SignatureLocation is invalid'
                   else
                      if sCommandName = 'PRNIS' then
                         CmdCommentLbl.Caption:='Parameter OnPagesLocation is invalid'
                      else
                         CmdCommentLbl.Caption:='Unknown command';
          end;
      9 : begin
             varColor:=clRed;
             if sCommandName = 'PRNSPG' then
                CmdCommentLbl.Caption:='Parameter NumberingFrom is invalid'
             else
                if sCommandName = 'PRNSM' then
                   CmdCommentLbl.Caption:='Parameter FrameCentering is invalid'
                else
                   if sCommandName = 'PRNSS' then
                      CmdCommentLbl.Caption:='Parameter OnPagesLocation is invalid'
                   else
                      if sCommandName = 'PRNIS' then
                         CmdCommentLbl.Caption:='Parameter FrameCentering is invalid'
                      else
                         CmdCommentLbl.Caption:='Unknown command';
          end;
      10 : begin
             varColor:=clRed;
             if sCommandName = 'PRNSM' then
                CmdCommentLbl.Caption:='Parameter FramePosition is invalid'
             else
                if sCommandName = 'PRNSS' then
                   CmdCommentLbl.Caption:='Parameter FrameCentering is invalid'
                else
                   if sCommandName = 'PRNIS' then
                      CmdCommentLbl.Caption:='Parameter FrameOridgin is invalid'
                   else
                      CmdCommentLbl.Caption:='Unknown command';
          end;
      11 : begin
             varColor:=clRed;
             if sCommandName = 'PRNSS' then
                CmdCommentLbl.Caption:='Parameter FrameOridgin is invalid'
             else
                if sCommandName = 'PRNIS' then
                   CmdCommentLbl.Caption:='Parameter FramePosition is invalid'
                else
                   CmdCommentLbl.Caption:='Unknown command';
          end;
      12 : begin
             varColor:=clRed;
                if sCommandName = 'PRNSS' then
                   CmdCommentLbl.Caption:='Parameter FramePosition is invalid'
                else
                   CmdCommentLbl.Caption:='Unknown command';
          end;
      17: begin
             varColor:=clRed;
             CmdCommentLbl.Caption:='There is no printers istalled in system'
          end;
      18: begin
             varColor:=clRed;
             CmdCommentLbl.Caption:='Internal WinGIS Error'
          end;
   else
          begin
             varColor:=clRed;
             CmdCommentLbl.Caption:='Invalid returned result';
          end;
   end;
   PrnResLbl.Font.Color:=varColor;
   PrnResCmdLbl.Font.Color:=varColor;
   PrnResValLbl.Font.Color:=varColor;
   CmdCommentLbl.Font.Color:=varColor;
   Beep;
end;

procedure TMainForm.DialogCheckBoxClick(Sender: TObject);
begin
   if DialogCheckBox.Checked then
      begin
         if ProgressCheckBox.Checked then
            ProgressCheckBox.Checked:=FALSE;
         ProgressCheckBox.Enabled:=FALSE;
      end
   else
      ProgressCheckBox.Enabled:=TRUE;
end;

procedure TMainForm.AXWingisPrintEvents(Sender: TObject; lPage, lOfPages, lResult: Integer);
var varColor: TColor;
begin
   PrnMonPageLbl.Caption:='[' + IntToStr(lPage) + ']';
   PrnMonPagesLbl.Caption:='[' + IntToStr(lOfPages) + ']';
   PrnMonValLbl.Caption:='[' + IntToStr(lResult) + ']';
   case lResult of
      0 : begin
             varColor:=clGreen;
             MonCmdCmntLbl1.Caption:=Format('Current page %d from %d pages',[lPage,lOfPages]);
             MonCmdCmntLbl2.Caption:='has been printed successfully.';
          end;
      1 : begin
             varColor:=clGreen;
             MonCmdCmntLbl1.Caption:='User press CANCEL button after';
             MonCmdCmntLbl2.Caption:=Format('page %d from %d pages printing.',[lPage,lOfPages]);
          end;
      2 : begin
             varColor:=clGreen;
             MonCmdCmntLbl1.Caption:='User press CLOSE button on Print Dialog';
             MonCmdCmntLbl2.Caption:=Format('(page %d from %d pages)',[lPage,lOfPages]);
          end;
   else
          begin
             varColor:=clRed;
             MonCmdCmntLbl1.Caption:='Invalid returned result';
             MonCmdCmntLbl2.Caption:='';
          end;
   end;
   PrnMonLbl.Font.Color:=varColor;
   PrnMonPageLbl.Font.Color:=varColor;
   PrnMonPagesLbl.Font.Color:=varColor;
   PrnMonValLbl.Font.Color:=varColor;
   MonCmdCmntLbl1.Font.Color:=varColor;
   MonCmdCmntLbl2.Font.Color:=varColor;
   Beep;
end;

procedure TMainForm.AXWingisPrinterParamGet(Sender: TObject;
          const sDeviceName, sPaperFormat: WideString; lOrientation,
          lErrorCode: Integer);
var varColor: TColor;
begin
   PrnSPPValLbl.Caption:='[' + IntToStr(lErrorCode) + ']';
   if lErrorCode = 0 then
      begin
         varColor:=clGreen;
         DeviceNameEdit.Text:=sDeviceName;
         PaperEdit.Text:=sPaperFormat;
         if lOrientation = 0 then
            PortraitRBut.Checked:=TRUE
         else
            LandscapeRBut.Checked:=TRUE;
      end
   else
      varColor:=clRed;
   PrnSPPValLbl.Font.Color:=varColor;
   PrnSPPLbl.Font.Color:=varColor;
   Beep;
end;

procedure TMainForm.AXWingisPrintPagesParamGet(Sender: TObject; lModel,
          lRepaginate, lHorizPages, lVerticPages: Integer; dMrLeft, dMrBottom,
          dMrRight, dMrTop, dInLeft, dInBottom, dInRight, dInTop, dOvLeft,
          dOvBottom, dOvRight, dOvTop: Double; lNumbOrder, lNumbFrom,
          lErrorCode: Integer);
var varColor: TColor;
begin
   PrnSPGValLbl.Caption:='[' + IntToStr(lErrorCode) + ']';
   if lErrorCode = 0 then
      begin
         varColor:=clGreen;
         LayoutComBox.ItemIndex:=lModel;
         AutoCheckBox.Checked:=(lRepaginate = 1);
         HorizEdit.Text:=IntToStr(lHorizPages);
         VerticEdit.Text:=IntToStr(lVerticPages);
         MarginEdit.Text:=Format('%f',[dMrLeft]);
         InnerEdit.Text:=Format('%f',[dInLeft]);
         OverlapEdit.Text:=Format('%f',[dOvLeft]);
         OrderCheckBox.Checked:=(lNumbOrder = 1);
         NumbEdit.Text:=IntToStr(lNumbFrom);
      end
   else
      varColor:=clRed;
   PrnSPGValLbl.Font.Color:=varColor;
   PrnSPGLbl.Font.Color:=varColor;
   Beep;
end;

procedure TMainForm.SetMapBtnClick(Sender: TObject);
var intResult        : Integer;
    lMapID           : Integer;
    lRegion          : Integer;
    sViewName        : WideString;
    dRegionX1        : Double;
    dRegionY1        : Double;
    dRegionX2        : Double;
    dRegionY2        : Double;
    bFitToPage       : WordBool;
    PrintScale       : Double;
    bKeepScale       : WordBool;
    dFrameWidth      : Double;
    dFrameHeight     : Double;
    lFrameCentering  : Integer;
    FrameLeft        : Double;
    FrameTop         : Double;
begin

   ResetControls();

   lMapID:=StrToInt(MapIDEdit.Text);
   lRegion:=RegionComBox.ItemIndex;
   sViewName:=ViewEdit.Text;
   dRegionX1:=StrToFloat(X1Edit.Text);
   dRegionY1:=StrToFloat(Y1Edit.Text);
   dRegionX2:=StrToFloat(X2Edit.Text);
   dRegionY2:=StrToFloat(Y2Edit.Text);
   bFitToPage:=FitToPageCheckBox.Checked;
   PrintScale:=StrToFloat(ScaleEdit.Text);
   bKeepScale:=KeepScaleCheckBox.Checked;
   dFrameWidth:=StrToFloat(WidthEdit.Text);
   dFrameHeight:=StrToFloat(HeightEdit.Text);
   lFrameCentering:=CentrComBox.ItemIndex;
   FrameLeft:=StrToFloat(LeftEdit.Text);
   FrameTop:=StrToFloat(TopEdit.Text);

   intResult:=AXWingis.DoPrintSetMapParam(lMapID,lRegion,sViewName,dRegionX1,dRegionY1,dRegionX2,dRegionY2,
                       bFitToPage,PrintScale,bKeepScale,dFrameWidth,dFrameHeight,lFrameCentering,FrameLeft,FrameTop);
   DoSetMapValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoSetMapLbl.Font.Color:=clBlack;
         DoSetMapValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoSetMapLbl.Font.Color:=clRed;
         DoSetMapValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.GetMapBtnClick(Sender: TObject);
var intResult        : Integer;
    lMapID           : Integer;
begin

   ResetControls();

   lMapID:=StrToInt(MapIDEdit.Text);

   intResult:=AXWingis.DoPrintGetMapParam(lMapID);
   DoGetMapValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoGetMapLbl.Font.Color:=clBlack;
         DoGetMapValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoGetMapLbl.Font.Color:=clRed;
         DoGetMapValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.AXWingisPrintMapParamGet(Sender: TObject; lMapID,
          lRegion: Integer; const sViewName: WideString; dRegionX1, dRegionY1,
          dRegionX2, dRegionY2: Double; bFitToPage: WordBool; dPrintScale: Double;
          bKeepScale: WordBool; dFrameWidth, dFrameHeight: Double;
          lFrameCentering: Integer; dFrameLeft, dFrameTop: Double;
          lErrorCode: Integer);
var varColor: TColor;
begin
   PrnSMValLbl.Caption:='[' + IntToStr(lErrorCode) + ']';
   if lErrorCode = 0 then
      begin
         varColor:=clGreen;
         MapIDEdit.Text:=IntToStr(lMapID);
         RegionComBox.ItemIndex:=lRegion;
         ViewEdit.Text:=sViewName;
         X1Edit.Text:=FloatToStr(dRegionX1);
         Y1Edit.Text:=FloatToStr(dRegionY1);
         X2Edit.Text:=FloatToStr(dRegionX2);
         Y2Edit.Text:=FloatToStr(dRegionY2);
         FitToPageCheckBox.Checked:=bFitToPage;
         ScaleEdit.Text:=FloatToStr(dPrintScale);
         KeepScaleCheckBox.Checked:=bKeepScale;
         WidthEdit.Text:=FloatToStr(dFrameWidth);
         HeightEdit.Text:=FloatToStr(dFrameHeight);
         case lFrameCentering of
            0 : CentrComBox.ItemIndex:=0;
         else
            CentrComBox.ItemIndex:=3;
         end;
         LeftEdit.Text:=FloatToStr(dFrameLeft);
         TopEdit.Text:=FloatToStr(dFrameTop);
      end
   else
      varColor:=clRed;
   PrnSMValLbl.Font.Color:=varColor;
   PrnSMLbl.Font.Color:=varColor;
   Beep;
end;

procedure TMainForm.FitToPageCheckBoxClick(Sender: TObject);
begin
   if FitToPageCheckBox.Checked then
      if KeepScaleCheckBox.Checked then
         KeepScaleCheckBox.Checked:=FALSE;
end;

procedure TMainForm.KeepScaleCheckBoxClick(Sender: TObject);
begin
   if KeepScaleCheckBox.Checked then
      if FitToPageCheckBox.Checked then
         FitToPageCheckBox.Checked:=FALSE;

end;

procedure TMainForm.GetLegBtnClick(Sender: TObject);
var intResult        : Integer;
    lLegendID        : Integer;
begin

   ResetControls();

   lLegendID:=StrToInt(LegendIDEdit.Text);

   intResult:=AXWingis.DoPrintGetLegendParam(lLegendID);
   DoGetLegValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoGetLegLbl.Font.Color:=clBlack;
         DoGetLegValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoGetLegLbl.Font.Color:=clRed;
         DoGetLegValLbl.Font.Color:=clRed;
      end;

end;

procedure TMainForm.InsLegBtnClick(Sender: TObject);
var intResult        : Integer;
    sLegendName      : WideString;
    dFrameWidth      : Double;
    dFrameHeight     : Double;
    lFrameCentering  : Integer;
    lFrameOridgin    : Integer;
    FrameLeft        : Double;
    FrameTopOrBot    : Double;
begin

   ResetControls();

   sLegendName:=LegendNameEdit.Text;
   dFrameWidth:=StrToFloat(LegWidthEdit.Text);
   dFrameHeight:=StrToFloat(LegHeightEdit.Text);
   lFrameCentering:=PositComBox.ItemIndex;

   lFrameOridgin:=OriginComBox.ItemIndex;
   FrameLeft:=StrToFloat(LegLeftEdit.Text);
   FrameTopOrBot:=StrToFloat(LegTopEdit.Text);

   intResult:=AXWingis.DoPrintInsertLegend(sLegendName, dFrameWidth, dFrameHeight,
                       lFrameCentering, lFrameOridgin, FrameLeft, FrameTopOrBot);
   DoInsLegValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoInsLegLbl.Font.Color:=clBlack;
         DoInsLegValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoInsLegLbl.Font.Color:=clRed;
         DoInsLegValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.SetLegBtnClick(Sender: TObject);
var intResult        : Integer;
    lLegendID        : Integer;
    dFrameWidth      : Double;
    dFrameHeight     : Double;
    lFrameCentering  : Integer;
    lFrameOridgin    : Integer;
    FrameLeft        : Double;
    FrameTopOrBot    : Double;
begin

   ResetControls();

   lLegendID:=StrToInt(LegendIDEdit.Text);
   dFrameWidth:=StrToFloat(LegWidthEdit.Text);
   dFrameHeight:=StrToFloat(LegHeightEdit.Text);
   lFrameCentering:=PositComBox.ItemIndex;

   lFrameOridgin:=OriginComBox.ItemIndex;
   FrameLeft:=StrToFloat(LegLeftEdit.Text);
   FrameTopOrBot:=StrToFloat(LegTopEdit.Text);

   intResult:=AXWingis.DoPrintSetLegendParam(lLegendID, dFrameWidth, dFrameHeight,
                       lFrameCentering, lFrameOridgin, FrameLeft, FrameTopOrBot);
   DoSetLegValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoSetLegLbl.Font.Color:=clBlack;
         DoSetLegValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoSetLegLbl.Font.Color:=clRed;
         DoSetLegValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.AXWingisPrintLegendParamGet(Sender: TObject;
          lLegendID: Integer; const sLegendName: WideString; dFrameWidth,
          dFrameHeight: Double; lFrameCentering, lFrameOridgin: Integer;
          dFrameLeft, dFrameTopOrBot: Double; lErrorCode: Integer);
var varColor: TColor;
begin
   PrnSLValLbl.Caption:='[' + IntToStr(lErrorCode) + ']';
   if lErrorCode = 0 then
      begin
         varColor:=clGreen;
         LegendIDEdit.Text:=IntToStr(lLegendID);
         LegendNameEdit.Text:=sLegendName;
         LegWidthEdit.Text:=FloatToStr(dFrameWidth);
         LegHeightEdit.Text:=FloatToStr(dFrameHeight);
         PositComBox.ItemIndex:=lFrameCentering;
         OriginComBox.ItemIndex:=lFrameOridgin;
         LegLeftEdit.Text:=FloatToStr(dFrameLeft);
         LegTopEdit.Text:=FloatToStr(dFrameTopOrBot);
      end
   else
      varColor:=clRed;
   PrnSLValLbl.Font.Color:=varColor;
   PrnSLLbl.Font.Color:=varColor;
   Beep;
end;

procedure TMainForm.PicLocRButClick(Sender: TObject);
begin
   if OneTimePicRBut.Checked then
      begin
         PicFirstChkBox.Enabled:=FALSE;
         PicIntChkBox.Enabled:=FALSE;
         PicLastChkBox.Enabled:=FALSE;
      end
   else
      begin
         PicFirstChkBox.Enabled:=TRUE;
         PicIntChkBox.Enabled:=TRUE;
         PicLastChkBox.Enabled:=TRUE;
      end;
end;

procedure TMainForm.SigLocRButClick(Sender: TObject);
begin
   if OneTimeSigRBut.Checked then
      begin
         SigFirstChkBox.Enabled:=FALSE;
         SigIntChkBox.Enabled:=FALSE;
         SigLastChkBox.Enabled:=FALSE;
      end
   else
      begin
         SigFirstChkBox.Enabled:=TRUE;
         SigIntChkBox.Enabled:=TRUE;
         SigLastChkBox.Enabled:=TRUE;
      end;
end;

procedure TMainForm.SetPicBtnClick(Sender: TObject);
var intResult        : Integer;
    lPictureID       : Integer;
    lPictureLocation : Integer;
    lFrSizeInterpret : Integer;
    dFrameWidth      : Double;
    dFrameHeight     : Double;
    lFrameCentering  : Integer;
    lFrameOridgin    : Integer;
    FrameLeft        : Double;
    FrameTopOrBot    : Double;
begin

   ResetControls();

   lPictureID:=StrToInt(PictureIDEdit.Text);
   if OneTimePicRBut.Checked then
      lPictureLocation:=0
   else
      lPictureLocation:=1000;
   if PicFirstChkBox.Checked then
      lPictureLocation:=lPictureLocation+100;
   if PicIntChkBox.Checked then
      lPictureLocation:=lPictureLocation+10;
   if PicLastChkBox.Checked then
      lPictureLocation:=lPictureLocation+1;
   if KeepAspectCheckBox.Checked then
      lFrSizeInterpret:=1
   else
      lFrSizeInterpret:=0;
   if PercentCheckBox.Checked then
      lFrSizeInterpret:=lFrSizeInterpret+2;
   dFrameWidth:=StrToFloat(PicWidthEdit.Text);
   dFrameHeight:=StrToFloat(PicHeightEdit.Text);
   lFrameCentering:=PicPosComBox.ItemIndex;

   lFrameOridgin:=PicOrigComBox.ItemIndex;
   FrameLeft:=StrToFloat(PicLeftEdit.Text);
   FrameTopOrBot:=StrToFloat(PicTopEdit.Text);

   intResult:=AXWingis.DoPrintSetPictureParam(lPictureID, lPictureLocation,
                       lFrSizeInterpret, dFrameWidth, dFrameHeight,
                       lFrameCentering, lFrameOridgin, FrameLeft, FrameTopOrBot);
   DoSetPicValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoSetPicLbl.Font.Color:=clBlack;
         DoSetPicValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoSetPicLbl.Font.Color:=clRed;
         DoSetPicValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.GetPicBtnClick(Sender: TObject);
var intResult        : Integer;
    lPictureID       : Integer;
begin

   ResetControls();

   lPictureID:=StrToInt(PictureIDEdit.Text);

   intResult:=AXWingis.DoPrintGetPictureParam(lPictureID);
   DoGetPicValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoGetPicLbl.Font.Color:=clBlack;
         DoGetPicValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoGetPicLbl.Font.Color:=clRed;
         DoGetPicValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.InsPicBtnClick(Sender: TObject);
var intResult        : Integer;
    sPictureFile     : WideString;
    lPictureLocation : Integer;
    lFrSizeInterpret : Integer;
    dFrameWidth      : Double;
    dFrameHeight     : Double;
    lFrameCentering  : Integer;
    lFrameOridgin    : Integer;
    FrameLeft        : Double;
    FrameTopOrBot    : Double;
begin

   ResetControls();

   sPictureFile:=PictureNameEdit.Text;
   if OneTimePicRBut.Checked then
      lPictureLocation:=0
   else
      lPictureLocation:=1000;
   if PicFirstChkBox.Checked then
      lPictureLocation:=lPictureLocation+100;
   if PicIntChkBox.Checked then
      lPictureLocation:=lPictureLocation+10;
   if PicLastChkBox.Checked then
      lPictureLocation:=lPictureLocation+1;
   if KeepAspectCheckBox.Checked then
      lFrSizeInterpret:=1
   else
      lFrSizeInterpret:=0;
   if PercentCheckBox.Checked then
      lFrSizeInterpret:=lFrSizeInterpret+2;
   dFrameWidth:=StrToFloat(PicWidthEdit.Text);
   dFrameHeight:=StrToFloat(PicHeightEdit.Text);
   lFrameCentering:=PicPosComBox.ItemIndex;

   lFrameOridgin:=PicOrigComBox.ItemIndex;
   FrameLeft:=StrToFloat(PicLeftEdit.Text);
   FrameTopOrBot:=StrToFloat(PicTopEdit.Text);

   intResult:=AXWingis.DoPrintInsertPicture(sPictureFile, lPictureLocation,
                       lFrSizeInterpret, dFrameWidth, dFrameHeight,
                       lFrameCentering, lFrameOridgin, FrameLeft, FrameTopOrBot);
   DoInsPicValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoInsPicLbl.Font.Color:=clBlack;
         DoInsPicValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoInsPicLbl.Font.Color:=clRed;
         DoInsPicValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.AXWingisPrintPictureParamGet(Sender: TObject;
          lPictureID: Integer; const sPictureFile: WideString;
          lPictureLocation: Integer; bKeepAspect: WordBool; dFrameWidth,
          dFrameHeight: Double; lFrameCentering, lFrameOridgin: Integer;
          dFrameLeft, dFrameTopOrBot: Double; lErrorCode: Integer);
var varColor: TColor;
begin
   PrnSPValLbl.Caption:='[' + IntToStr(lErrorCode) + ']';
   if lErrorCode = 0 then
      begin
         varColor:=clGreen;
         PictureIDEdit.Text:=IntToStr(lPictureID);
         PictureNameEdit.Text:=sPictureFile;
         if lPictureLocation >= 1000 then
            begin
               OneTimePicRBut.Checked:=FALSE;
               OnPagesPicRBut.Checked:=TRUE;
               lPictureLocation:=lPictureLocation-1000;
            end
         else
            begin
               OnPagesPicRBut.Checked:=FALSE;
               OneTimePicRBut.Checked:=TRUE;
            end;
         if (lPictureLocation div 100) = 1 then
            begin
               PicFirstChkBox.Checked:=TRUE;
               lPictureLocation:=lPictureLocation mod 100;
            end
         else
            PicFirstChkBox.Checked:=FALSE;
         if (lPictureLocation div 10) = 1 then
            begin
               PicIntChkBox.Checked:=TRUE;
               lPictureLocation:=lPictureLocation mod 10;
            end
         else
            PicIntChkBox.Checked:=FALSE;
         if lPictureLocation = 1 then
            PicLastChkBox.Checked:=TRUE
         else
            PicLastChkBox.Checked:=FALSE;
         PercentCheckBox.Checked:=FALSE;
         KeepAspectCheckBox.Checked:=bKeepAspect;
         PicWidthEdit.Text:=FloatToStr(dFrameWidth);
         PicHeightEdit.Text:=FloatToStr(dFrameHeight);
         PicPosComBox.ItemIndex:=lFrameCentering;
         PicOrigComBox.ItemIndex:=lFrameOridgin;
         PicLeftEdit.Text:=FloatToStr(dFrameLeft);
         PicTopEdit.Text:=FloatToStr(dFrameTopOrBot);
      end
   else
      varColor:=clRed;
   PrnSPValLbl.Font.Color:=varColor;
   PrnSPLbl.Font.Color:=varColor;
   Beep;
end;

procedure TMainForm.SetSigBtnClick(Sender: TObject);
var intResult        : Integer;
    lSignatureID     : Integer;
    sSignature       : WideString;
    sFontName        : WideString;
    bItalic          : WordBool;
    bBold            : WordBool;
    bUnderlined      : WordBool;
    bTransparent     : WordBool;
    lFontSize        : Integer;
    lFontColor       : Integer;
    lFontBackColor   : Integer;
    lTextAlignment   : Integer;
    bOnPagesLocation : WordBool;
    bOnFirstPage     : WordBool;
    bOnInternalPages : WordBool;
    bOnLastPage      : WordBool;
    lFrameCentering  : Integer;
    lFrameOridgin    : Integer;
    FrameLeft        : Double;
    FrameTopOrBot    : Double;
begin

   ResetControls();

   lSignatureID:=StrToInt(SignatureIDEdit.Text);
   sSignature:=SiganatureEdit.Text;
   sFontName:=FontNameEdit.Text;
   bItalic:=ItalicChkBox.Checked;
   bBold:=BoldChkBox.Checked;
   bUnderlined:=UndlChkBox.Checked;
   bTransparent:=TranspChkBox.Checked;
   lFontSize:=StrToInt(FontSizeEdit.Text);
   lFontColor:=StrToInt(FontColorEdit.Text);
   lFontBackColor:=StrToInt(FontBackColorEdit.Text);
   lTextAlignment:=TextAlignComBox.ItemIndex;
   bOnPagesLocation:=OnPagesSigRBut.Checked;
   bOnFirstPage:=SigFirstChkBox.Checked;
   bOnInternalPages:=SigIntChkBox.Checked;
   bOnLastPage:=SigLastChkBox.Checked;
   lFrameCentering:=SigPosComBox.ItemIndex;
   lFrameOridgin:=SigOrigComBox.ItemIndex;
   FrameLeft:=StrToFloat(SigLeftEdit.Text);
   FrameTopOrBot:=StrToFloat(SigTopEdit.Text);

   intResult:=AXWingis.DoPrintSetSignatureParam(lSignatureID, sSignature,
                       sFontName, bItalic, bBold, bUnderlined, bTransparent,
                       lFontSize, lFontColor, lFontBackColor, lTextAlignment,
                       bOnPagesLocation, bOnFirstPage, bOnInternalPages, bOnLastPage,
                       lFrameCentering, lFrameOridgin, FrameLeft, FrameTopOrBot);
   DoSetSigValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoSetSigLbl.Font.Color:=clBlack;
         DoSetSigValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoSetSigLbl.Font.Color:=clRed;
         DoSetSigValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.InsSigBtnClick(Sender: TObject);
var intResult        : Integer;
    sSignature       : WideString;
    sFontName        : WideString;
    bItalic          : WordBool;
    bBold            : WordBool;
    bUnderlined      : WordBool;
    bTransparent     : WordBool;
    lFontSize        : Integer;
    lFontColor       : Integer;
    lFontBackColor   : Integer;
    lTextAlignment   : Integer;
    bOnPagesLocation : WordBool;
    bOnFirstPage     : WordBool;
    bOnInternalPages : WordBool;
    bOnLastPage      : WordBool;
    lFrameCentering  : Integer;
    lFrameOridgin    : Integer;
    FrameLeft        : Double;
    FrameTopOrBot    : Double;
begin

   ResetControls();

   sSignature:=SiganatureEdit.Text;
   sFontName:=FontNameEdit.Text;
   bItalic:=ItalicChkBox.Checked;
   bBold:=BoldChkBox.Checked;
   bUnderlined:=UndlChkBox.Checked;
   bTransparent:=TranspChkBox.Checked;
   lFontSize:=StrToInt(FontSizeEdit.Text);
   lFontColor:=StrToInt(FontColorEdit.Text);
   lFontBackColor:=StrToInt(FontBackColorEdit.Text);
   lTextAlignment:=TextAlignComBox.ItemIndex;
   bOnPagesLocation:=OnPagesSigRBut.Checked;
   bOnFirstPage:=SigFirstChkBox.Checked;
   bOnInternalPages:=SigIntChkBox.Checked;
   bOnLastPage:=SigLastChkBox.Checked;
   lFrameCentering:=SigPosComBox.ItemIndex;
   lFrameOridgin:=SigOrigComBox.ItemIndex;
   FrameLeft:=StrToFloat(SigLeftEdit.Text);
   FrameTopOrBot:=StrToFloat(SigTopEdit.Text);

   intResult:=AXWingis.DoPrintInsertSignature(sSignature, sFontName,
                       bItalic, bBold, bUnderlined, bTransparent, lFontSize,
                       lFontColor, lFontBackColor, lTextAlignment,
                       bOnPagesLocation,bOnFirstPage, bOnInternalPages, bOnLastPage,
                       lFrameCentering, lFrameOridgin, FrameLeft, FrameTopOrBot);
   DoInsSigValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoInsSigLbl.Font.Color:=clBlack;
         DoInsSigValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoInsSigLbl.Font.Color:=clRed;
         DoInsSigValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.GetSigBtnClick(Sender: TObject);
var intResult        : Integer;
    lSignatureID       : Integer;
begin

   ResetControls();

   lSignatureID:=StrToInt(SignatureIDEdit.Text);

   intResult:=AXWingis.DoPrintGetSignatureParam(lSignatureID);
   DoGetSigValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoGetSigLbl.Font.Color:=clBlack;
         DoGetSigValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoGetSigLbl.Font.Color:=clRed;
         DoGetSigValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.AXWingisPrintSignatureParamGet(Sender: TObject;
          lSignatureID: Integer; const sSignature, sFontName: WideString; bItalic,
          bBold, bUnderlined, bTransparent: WordBool; lFontSize, lFontColor,
          lFontBackColor, lTextAlignment: Integer; bOnPagesLocation, bOnFirstPage,
          bOnInternalPages, bOnLastPage: WordBool; lFrameCentering,
          lFrameOridgin: Integer; dFrameLeft, dFrameTopOrBot: Double;
          lErrorCode: Integer);
var varColor: TColor;
begin
   PrnSSValLbl.Caption:='[' + IntToStr(lErrorCode) + ']';
   if lErrorCode = 0 then
      begin
         varColor:=clGreen;
         SignatureIDEdit.Text:=IntToStr(lSignatureID);
         SiganatureEdit.Text:=sSignature;
         FontNameEdit.Text:=sFontName;
         ItalicChkBox.Checked:=bItalic;
         BoldChkBox.Checked:=bBold;
         UndlChkBox.Checked:=bUnderlined;
         TranspChkBox.Checked:=bTransparent;
         FontSizeEdit.Text:=IntToStr(lFontSize);
         FontColorEdit.Text:=IntToStr(lFontColor);
         FontBackColorEdit.Text:=IntToStr(lFontBackColor);
         TextAlignComBox.ItemIndex:=lTextAlignment;
         OnPagesSigRBut.Checked:=bOnPagesLocation;
         SigFirstChkBox.Checked:=bOnFirstPage;
         SigIntChkBox.Checked:=bOnInternalPages;
         SigLastChkBox.Checked:=bOnLastPage;
         SigPosComBox.ItemIndex:=lFrameCentering;
         SigOrigComBox.ItemIndex:=lFrameOridgin;
         SigLeftEdit.Text:=FloatToStr(dFrameLeft);
         SigTopEdit.Text:=FloatToStr(dFrameTopOrBot);
      end
   else
      varColor:=clRed;
   PrnSSValLbl.Font.Color:=varColor;
   PrnSSLbl.Font.Color:=varColor;
   Beep;
end;

procedure TMainForm.SaveTempBtnClick(Sender: TObject);
var intResult        : Integer;
    sFileName        : WideString;
    lOptions         : Integer;
begin

   ResetControls();

   sFileName:=TemplateNameEdit.Text;
   lOptions:=StrToInt(OptEdit.Text);

   intResult:=AXWingis.DoPrintStoreTemplate(sFileName, lOptions);

   DoStrTmpValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoStrTmpLbl.Font.Color:=clBlack;
         DoStrTmpValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoStrTmpLbl.Font.Color:=clRed;
         DoStrTmpValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.OpenTempBtnClick(Sender: TObject);
var intResult        : Integer;
    sFileName        : WideString;
begin

   ResetControls();

   sFileName:=TemplateNameEdit.Text;

   intResult:=AXWingis.DoPrintLoadTemplate(sFileName);

   DoRstTmpValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoRstTmpLbl.Font.Color:=clBlack;
         DoRstTmpValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoRstTmpLbl.Font.Color:=clRed;
         DoRstTmpValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.GetObjInfBtnClick(Sender: TObject);
var intResult        : Integer;
begin

   ResetControls();

   MapNumEdit.Text:='0';
   LegNumEdit.Text:='0';
   PicNumEdit.Text:='0';
   SigNumEdit.Text:='0';
   ObjNumEdit.Text:='0';
   ObjectsListBox.Clear;

   intResult:=AXWingis.DoPrintGetObjectsInform;
   DoGetObjValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoGetObjLbl.Font.Color:=clBlack;
         DoGetObjValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoGetObjLbl.Font.Color:=clRed;
         DoGetObjValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.AXWingisPrintObjectsInformGet(Sender: TObject;
          lObjectsNumber, lMapsNumber, lLegendsNumber, lPicturesNumber,
          lSignaturesNumber, hTypesList, hObjectsIDList, lErrorCode: Integer);
var i                 : Integer;
    ObjectsIDListSize : Integer;
    TypesListSize     : Integer;
    strValue          : String;
    varColor          : TColor;
begin
   PrnSOIValLbl.Caption:='[' + IntToStr(lErrorCode) + ']';
   if lErrorCode = 0 then
      begin
         TypesListSize:=AXWingis.GetListSize(hTypesList);
         ObjectsIDListSize:=AXWingis.GetListSize(hObjectsIDList);
         if (TypesListSize <> lObjectsNumber) or (ObjectsIDListSize <> lObjectsNumber) then
            begin
               lObjectsNumber:=Min(Min(TypesListSize,ObjectsIDListSize),lObjectsNumber);
               varColor:=clRed;
            end
         else
            varColor:=clGreen;
         MapNumEdit.Text:=IntToStr(lMapsNumber);
         LegNumEdit.Text:=IntToStr(lLegendsNumber);
         PicNumEdit.Text:=IntToStr(lPicturesNumber);
         SigNumEdit.Text:=IntToStr(lSignaturesNumber);
         ObjNumEdit.Text:=IntToStr(lObjectsNumber);
         ObjectsListBox.Clear;
         for i:=0 to lObjectsNumber-1 do
             begin
                strValue:=IntToStr(i+1);
                while Length(strValue) < 3 do
                   strValue:=' '+strValue;
                strValue:=strValue+'  '+AXWingis.GetStringParam(hTypesList,i);
                strValue:=strValue+'  '+AXWingis.GetStringParam(hObjectsIDList,i);
                ObjectsListBox.Items.Add(strValue);
             end;
         
      end
   else
      varColor:=clRed;
   PrnSOIValLbl.Font.Color:=varColor;
   PrnSOILbl.Font.Color:=varColor;
   Beep;
end;

procedure TMainForm.MovObjBtnClick(Sender: TObject);
var intResult        : Integer;
    ObjectType       : Integer;
    ObjectID         : Integer;
    Options          : Integer;
begin

   ResetControls();

   ObjectType:=TypeComBox.ItemIndex;
   ObjectID:=StrToInt(ObjectIDEdit.Text);
   Options:=OptionsComBox.ItemIndex;

   intResult:=AXWingis.DoPrintMoveObject(ObjectType, ObjectID, Options);

   DoMovObjValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoMovObjLbl.Font.Color:=clBlack;
         DoMovObjValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoMovObjLbl.Font.Color:=clRed;
         DoMovObjValLbl.Font.Color:=clRed;
      end;
end;

procedure TMainForm.DelObjBtnClick(Sender: TObject);
var intResult        : Integer;
    ObjectType       : Integer;
    ObjectID         : Integer;
begin

   ResetControls();

   ObjectType:=TypeComBox.ItemIndex;
   ObjectID:=StrToInt(ObjectIDEdit.Text);

   intResult:=AXWingis.DoPrintDeleteObject(ObjectType, ObjectID);

   DoDelObjValLbl.Caption:=IntToStr(intResult);
   if intResult = 0 then
      begin
         DoDelObjLbl.Font.Color:=clBlack;
         DoDelObjValLbl.Font.Color:=clBlack;
      end
   else
      begin
         DoDelObjLbl.Font.Color:=clRed;
         DoDelObjValLbl.Font.Color:=clRed;
      end;
end;

end.
