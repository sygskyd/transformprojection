�
 TMAINFORM 0��  TPF0	TMainFormMainFormLeftBTopBorderStylebsDialogCaptionTest ApplicationClientHeight�ClientWidthColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnCreate
FormCreatePixelsPerInch`
TextHeight 	TGroupBoxControlGroupBoxLeftTopWidth� HeightCaption ApplicationControls TabOrder  TShapeConnectionLightLeftTopWidth1Height!Brush.ColorclRedShapestCircle  TLabelConnectionLabLeftTopWidthaHeightCaption
ConnectionFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTransparent	  TLabel
DevNameLblLeftTopWidthDHeightCaptionDevice Name:FocusControlDeviceNameEditLayouttlCenter  TButtonPrintBtnLeft� Top� WidthWHeightCaptionPrintTabOrder OnClickPrintBtnClick  TButtonCreateTextBtnLeft8TopXWidth� HeightCaptionCreate TextTabOrderOnClickCreateTextBtnClick  TButton
ConnectBtnLeft� TopWidthWHeightCaptionConnectTabOrderOnClickConnectBtnClick  TEditTextEditLeftTop@Width� HeightTabOrder  TButtonCreateObjBtnLeft8TopxWidth� HeightCaptionCreate ObjectTabOrderOnClickNew  TEditFileNameEditLeftTop� Width� HeightTabOrderTextD:\TEMP\Test.wmf  	TCheckBoxToFileCheckBoxLeftTop� WidthYHeightCaptionPrint to FileTabOrderOnClickToFileCheckBoxClick  	TCheckBoxToWMFCheckBoxLeftTop� WidthaHeightCaptionPrint to WMFTabOrderOnClickToWMFCheckBoxClick  	TCheckBoxDialogCheckBoxLeftTop� WidthYHeightCaptionShow dialogTabOrderOnClickDialogCheckBoxClick  	TCheckBoxMonitoringCheckBoxLeftlTop� WidthsHeightCaptionPrinting MonitoringTabOrder	  	TCheckBoxProgressCheckBoxLeftlTop� WidthsHeightCaptionShow progressTabOrder
  TEditDeviceNameEditLeftXTopWidth� HeightTabOrderText\\Vist16\HP  TButton	SetPrnBtnLeft� Top=WidthWHeightCaptionSet PrinterTabOrderOnClickSetPrnBtnClick  	TGroupBoxOrientGrBoxLeftTop WidthiHeight9CaptionPage OrientationTabOrder TLabelPortraitLblLeftTopWidth!HeightCaptionPortraitFocusControlPortraitRButLayouttlCenter  TLabelLandscapeLblLeftTop Width5HeightCaption	LandscapeFocusControlLandscapeRButLayouttlCenter  TRadioButtonPortraitRButLeftTopWidthHeightCaptionPortraitRButChecked	TabOrder TabStop	  TRadioButtonLandscapeRButLeftTop WidthHeightCaptionPortraitRButTabOrder   TEdit	PaperEditLeft� Top"WidthYHeightTabOrderTextA4  	TGroupBoxMarginsGrBoxLeftTopxWidthiHeightqCaptionMarginsTabOrder TEdit
MarginEditLeftTopWidthAHeightTabOrder Text20  TEdit	InnerEditLeftTop,WidthAHeightTabOrderText15  TEditOverlapEditLeftTopDWidthAHeightTabOrderText0   TButton	SetPgsBtnLeft� Top�WidthWHeightCaptionSet Pages ParTabOrderOnClickSetPgsBtnClick  	TComboBoxLayoutComBoxLeftxTop�WidthaHeight
ItemHeightTabOrder  TEdit	HorizEditLeft� Top�Width!HeightTabOrderText1  TEdit
VerticEditLeft� Top�Width!HeightTabOrderText1  	TCheckBoxAutoCheckBoxLeft|Top�WidthHeightCaptionAutoCheckBoxTabOrder  TEditNumbEditLeft� Top�Width!HeightTabOrderText1  	TCheckBoxOrderCheckBoxLeft� Top�WidthHeightCaptionAutoCheckBoxTabOrder  TButton	GetPgsBtnLeft� Top�WidthWHeightCaptionGet Pages ParTabOrderOnClickGetPgsBtnClick  TButton	GetPrnBtnLeft� Top]WidthWHeightCaptionGet PrinterTabOrderOnClickGetPrnBtnClick   	TGroupBoxReactionGroupBoxLeft�TopWidth"Height�AnchorsakLeftakTopakBottom Caption%Commands from DBApplication to WinGISTabOrder TLabel
DoPrintLblLeftTopWidth� HeightCaptionAXWingis.DoPrint return result:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoPrintValLblLeft� TopWidth	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoSetPrnLblLeftTop Width� HeightCaptionAXWingis.DoPrintSetPrintParam:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoSetPrnValLblLeft� Top Width	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoSetPgsLblLeftTop@Width� HeightCaptionAXWingis.DoPrintSetPagesParam:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoSetPgsValLblLeft� Top@Width	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoGetPgsLblLeftTopPWidth� HeightCaptionAXWingis.DoPrintGetPagesParam:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoGetPgsValLblLeft� TopPWidth	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoGetPrnLblLeftTop0Width� HeightCaptionAXWingis.DoPrintGetPrintParam:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoGetPrnValLblLeft� Top0Width	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoSetMapLblLeftToppWidth� HeightCaptionAXWingis.DoPrintSetMapParam:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoSetMapValLblLeft� ToppWidth	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoGetMapLblLeftTop� Width� HeightCaptionAXWingis.DoPrintGetMapParam:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoGetMapValLblLeft� Top� Width	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoSetLegLblLeftTop� Width� HeightCaptionAXWingis.DoPrintSetLegendParam:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoSetLegValLblLeft Top� Width	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoGetLegLblLeftTop� Width� HeightCaptionAXWingis.DoPrintGetLegendParam:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoGetLegValLblLeft Top� Width	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoInsLegLblLeftTop� Width� HeightCaptionAXWingis.DoPrintInsertLegend:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoInsLegValLblLeft� Top� Width	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoSetPicLblLeftTop� Width� HeightCaption AXWingis.DoPrintSetPictureParam:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoSetPicValLblLeft Top� Width	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoGetPicLblLeftTop� Width� HeightCaption AXWingis.DoPrintGetPictureParam:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoGetPicValLblLeft Top� Width	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoInsPicLblLeftTop� Width� HeightCaptionAXWingis.DoPrintInsertPicture:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoInsPicValLblLeft� Top� Width	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoSetSigLblLeftTop� WidthHeightCaption"AXWingis.DoPrintSetSignatureParam:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoSetSigValLblLeftTop� Width	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoGetSigLblLeftTop WidthHeightCaption"AXWingis.DoPrintGetSignatureParam:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoGetSigValLblLeftTop Width	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoInsSigLblLeftTopWidth� HeightCaption AXWingis.DoPrintInsertSignature:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoInsSigValLblLeft� TopWidth	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoStrTmpLblLeftTop Width� HeightCaptionAXWingis.DoPrintStoreTemplate:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoStrTmpValLblLeft� Top Width	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoRstTmpLblLeftTop0Width� HeightCaptionAXWingis.DoPrintLoadTemplate:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoRstTmpValLblLeft� Top0Width	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoGetObjLblLeftTop`Width� HeightCaption!AXWingis.DoPrintGetObjectsInform:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoGetObjValLblLeft Top`Width	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoDelObjLblLeftTop@Width� HeightCaptionAXWingis.DoPrintDeleteObject:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoDelObjValLblLeft� Top@Width	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoMovObjLblLeftTopPWidth� HeightCaptionAXWingis.DoPrintMoveObject:Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelDoMovObjValLblLeft� TopPWidth	HeightCaption0Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  	TGroupBoxCommandsGroupBoxLeftTophWidthHeightICaptionWinGIS ReplyTabOrder  TLabel	PrnResLblLeftTopWidthJHeightCaption[PRNRES]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnResCmdLblLeftXTopWidthJHeightCaption[PRNRES]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnResValLblLeft� TopWidthHeightCaption[1]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelCmdCommentLblLeftTop Width_HeightCaptionNo commentsFont.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabel	PrnMonLblLeftTop8WidthMHeightCaption[PRNMON]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnMonPageLblLeftXTop8WidthHeightCaption[1]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnMonPagesLblLeftxTop8WidthHeightCaption[1]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnMonValLblLeft� Top8WidthHeightCaption[1]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelMonCmdCmntLbl1LeftTopHWidth_HeightCaptionNo commentsFont.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelMonCmdCmntLbl2LeftTopXWidth_HeightCaptionNo commentsFont.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabel	PrnSPPLblLeftToppWidthcHeightCaption[PRNSPP] [...]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnSPPValLblLeftpToppWidthHeightCaption[0]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabel	PrnSPGLblLeftTop� WidthdHeightCaption[PRNSPG] [...]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnSPGValLblLeftpTop� WidthHeightCaption[0]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnSMLblLeftTop� Width[HeightCaption[PRNSM] [...]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnSMValLblLefthTop� WidthHeightCaption[0]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnSLLblLeftTop� WidthWHeightCaption[PRNSL] [...]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnSLValLblLefthTop� WidthHeightCaption[0]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnSPLblLeftTop� WidthYHeightCaption[PRNSP] [...]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnSPValLblLefthTop� WidthHeightCaption[0]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnSSLblLeftTop WidthYHeightCaption[PRNSS] [...]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnSSValLblLefthTop WidthHeightCaption[0]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnSTLblLeftTopWidthYHeightCaption[PRNST] [...]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnSTValLblLefthTopWidthHeightCaption[0]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnRTLblLeftTop0WidthZHeightCaption[PRNRT] [...]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnRTValLblLefthTop0WidthHeightCaption[0]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabel	PrnSOILblLeftTop� WidthfHeightCaption[PRNSOI] [ ... ]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelPrnSOIValLblLeftpTop� WidthHeightCaption[0]Font.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont    TButtonExitBtnLefteTop�Width� HeightAnchorsakRightakBottom CaptionExitTabOrderOnClickExitBtnClick  	TAXWingisAXWingisLeft:Top�Width!Height!TabOrder	OnConnectAXWingisConnectOnDisconnectAXWingisDisconnectOnPrintResultAXWingisPrintResultOnPrintEventsAXWingisPrintEventsOnPrinterParamGetAXWingisPrinterParamGetOnPrintPagesParamGetAXWingisPrintPagesParamGetOnPrintMapParamGetAXWingisPrintMapParamGetOnPrintLegendParamGetAXWingisPrintLegendParamGetOnPrintPictureParamGetAXWingisPrintPictureParamGetOnPrintSignatureParamGetAXWingisPrintSignatureParamGetOnPrintObjectsInformGetAXWingisPrintObjectsInformGetControlData
      i  i      AMDBAX    	TGroupBoxControlGroupBox2Left� TopWidth� HeightCaptionMapSettingsTabOrder TLabelX1LabLeftTopPWidthHeightCaptionX1:FocusControlX1Edit  TLabelY1LabLeftTophWidthHeightCaptionY1:FocusControlY1Edit  TLabelX2LabLeft� TopPWidthHeightCaptionX2:FocusControlX2Edit  TLabelY2LabLeft� TophWidthHeightCaptionY2:FocusControlY2Edit  TLabelScaleLabLeft(TopWidthHeightCaptionScale:FocusControl	ScaleEdit  TLabelMapIDLabLeft� TopWidthHeightCaptionID:FocusControl	MapIDEdit  TLabelLeftLabLeftTop� WidthHeightCaptionLeft:FocusControlLeftEdit  TLabelTopLabLeftTop� WidthHeightCaptionTop:FocusControlTopEdit  TLabelWidthLabLefttTop� WidthHeightCaptionWidth:FocusControl	WidthEdit  TLabel	HeightLabLefttTop� Width"HeightCaptionHeight:FocusControl
HeightEdit  	TComboBoxRegionComBoxLeftxTop0WidthaHeight
ItemHeightTabOrder   TButton	SetMapBtnLeft� Top� WidthWHeightAnchorsakLeftakBottom CaptionSet Map ParTabOrderOnClickSetMapBtnClick  TButton	GetMapBtnLeft"Top� WidthWHeightAnchorsakLeftakBottom CaptionGet Map ParTabOrderOnClickGetMapBtnClick  TEdit	MapIDEditLeft� TopWidth!HeightTabOrderText0  TEditViewEditLeftTop0WidthYHeightTabOrderTextView  TEditX1EditLeft(TopLWidthAHeightTabOrderText	-20000000  TEditY1EditLeft(TopdWidthAHeightTabOrderText0  TEditX2EditLeft� TopLWidthAHeightTabOrderText20000000  TEditY2EditLeft� TopdWidthAHeightTabOrderText27000000  TEdit	ScaleEditLeftHTopWidthYHeightTabOrder	Text0,000000004  	TCheckBoxFitToPageCheckBoxLeftTopzWidthYHeightCaptionFit to PageChecked	State	cbCheckedTabOrder
OnClickFitToPageCheckBoxClick  	TCheckBoxKeepScaleCheckBoxLeftTop� WidthYHeightCaption
Keep ScaleTabOrderOnClickKeepScaleCheckBoxClick  	TComboBoxCentrComBoxLeftxTop� WidthaHeight
ItemHeightTabOrder  TEditLeftEditLeft(Top� WidthAHeightTabOrderText0  TEditTopEditLeft(Top� WidthAHeightTabOrderText0  TEdit	WidthEditLeft� Top� WidthAHeightTabOrderText170  TEdit
HeightEditLeft� Top� WidthAHeightTabOrderText110   	TGroupBox	GroupBox1Left� TopWidth� Height� CaptionLegendSettingsTabOrder TLabelLegendIDLabLeft� TopWidthHeightCaptionID:FocusControlLegendIDEdit  TLabel
LegNameLabLeft TopWidthHeightCaptionName:FocusControl	ScaleEdit  TLabel
LegLeftLabLeftTopPWidthHeightCaptionLeft:FocusControlLegLeftEdit  TLabel	LegTopLabLeftTophWidthHeightCaptionTop:FocusControl
LegTopEdit  TLabelLegWidthLabLefttTopPWidthHeightCaptionWidth:FocusControlLegWidthEdit  TLabelLegHeightLabLefttTophWidth"HeightCaptionHeight:FocusControlLegHeightEdit  TEditLegendIDEditLeft� TopWidth!HeightTabOrder Text0  TEditLegendNameEditLeftFTopWidthYHeightTabOrder  	TComboBoxPositComBoxLeftxTop0WidthaHeight
ItemHeightTabOrder  	TComboBoxOriginComBoxLeftTop0WidthaHeight
ItemHeightTabOrder  TEditLegLeftEditLeft(TopLWidthAHeightTabOrderText0  TEdit
LegTopEditLeft(TopdWidthAHeightTabOrderText0  TEditLegWidthEditLeft� TopLWidthAHeightTabOrderText100  TEditLegHeightEditLeft� TopdWidthAHeightTabOrderText70  TButton	SetLegBtnLeft� Top� WidthWHeightCaptionSet Legend ParTabOrderOnClickSetLegBtnClick  TButton	GetLegBtnLeft"Top� WidthWHeightCaptionGet Legend ParTabOrder	OnClickGetLegBtnClick  TButton	InsLegBtnLeft� Top� WidthWHeightCaptionInsert LegendTabOrder
OnClickInsLegBtnClick   	TGroupBox	GroupBox2Left�TopWidth� HeightCaptionPictureSettingsTabOrder TLabelPictureIDLabLeft� TopWidthHeightCaptionID:FocusControlPictureIDEdit  TLabel
PicNameLabLeft TopWidthHeightCaptionName:FocusControl	ScaleEdit  TLabelLabel3LeftTop� WidthHeightAnchorsakLeftakBottom CaptionLeft:FocusControlPicLeftEdit  TLabelLabel4LeftTop� WidthHeightAnchorsakLeftakBottom CaptionTop:FocusControl
PicTopEdit  TLabelLabel5LefttTop� WidthHeightAnchorsakLeftakBottom CaptionWidth:FocusControlPicWidthEdit  TLabelLabel6LefttTop� Width"HeightAnchorsakLeftakBottom CaptionHeight:FocusControlPicHeightEdit  TEditPictureIDEditLeft� TopWidth!HeightTabOrder Text0  TEditPictureNameEditLeftFTopWidthYHeightTabOrderTextD:\TEMP\Agree.wmf  	TComboBoxPicPosComBoxLeftxTopmWidthaHeightAnchorsakLeftakBottom 
ItemHeightTabOrder  	TComboBoxPicOrigComBoxLeftTopmWidthaHeightAnchorsakLeftakBottom 
ItemHeightTabOrder  TEditPicLeftEditLeft(Top� WidthAHeightAnchorsakLeftakBottom TabOrderText0  TEdit
PicTopEditLeft(Top� WidthAHeightAnchorsakLeftakBottom TabOrderText0  TEditPicWidthEditLeft� Top� WidthAHeightAnchorsakLeftakBottom TabOrderText125,5   TEditPicHeightEditLeft� Top� WidthAHeightAnchorsakLeftakBottom TabOrderText96,3  TButton	SetPicBtnLeft� Top� WidthWHeightAnchorsakLeftakBottom CaptionSet Picture ParTabOrderOnClickSetPicBtnClick  TButton	GetPicBtnLeft"Top� WidthWHeightAnchorsakLeftakBottom CaptionGet Picture ParTabOrder	OnClickGetPicBtnClick  TButton	InsPicBtnLeft"Top� WidthWHeightAnchorsakLeftakBottom CaptionInsert PictureTabOrder
OnClickInsPicBtnClick  	TCheckBoxKeepAspectCheckBoxLeft� Top� WidthYHeightAnchorsakLeftakBottom CaptionKeep AspectChecked	State	cbCheckedTabOrderOnClickKeepScaleCheckBoxClick  	TGroupBoxPicLocGrBoxLeftTop,Width� Height9CaptionPicture LocationTabOrder TLabelOneTimePicLabLeftTopWidth.HeightCaptionOne TimeFocusControlOneTimePicRButLayouttlCenter  TLabelOnPagesPicLabLeftTop Width/HeightCaptionOn PagesFocusControlOnPagesPicRButLayouttlCenter  TRadioButtonOneTimePicRButLeftTopWidthHeightCaptionPortraitRButChecked	TabOrder TabStop	OnClickPicLocRButClick  TRadioButtonOnPagesPicRButLeftTop WidthHeightCaptionPortraitRButTabOrderOnClickPicLocRButClick  	TCheckBoxPicFirstChkBoxLeftLTopWidth'HeightAnchorsakLeftakBottom CaptionFirstChecked	EnabledState	cbCheckedTabOrderOnClickKeepScaleCheckBoxClick  	TCheckBoxPicIntChkBoxLeft}TopWidth1HeightAnchorsakLeftakBottom CaptionInternChecked	EnabledState	cbCheckedTabOrderOnClickKeepScaleCheckBoxClick  	TCheckBoxPicLastChkBoxLeft� TopWidth)HeightAnchorsakLeftakBottom CaptionLastChecked	EnabledState	cbCheckedTabOrderOnClickKeepScaleCheckBoxClick   	TCheckBoxPercentCheckBoxLeft� Top� WidthYHeightAnchorsakLeftakBottom Caption
In PercentTabOrderOnClickKeepScaleCheckBoxClick   	TGroupBox	GroupBox3Left�TopWidth� HeighteCaptionSignatureSettingsTabOrder TLabelSignatureIDLabLeft� TopWidthHeightCaptionID:FocusControlSignatureIDEdit  TLabelLabel7Left~TopWidthHeightAnchorsakLeftakBottom CaptionLeft:FocusControlSigLeftEdit  TLabelLabel8Left~Top(WidthHeightAnchorsakLeftakBottom CaptionTop:FocusControl
SigTopEdit  TEditSignatureIDEditLeft� TopWidth!HeightTabOrder Text0  TEditSiganatureEditLeftTopWidth� HeightTabOrderTextToday is &Date..  	TComboBoxSigPosComBoxLeftTop� WidthaHeightAnchorsakLeftakBottom 
ItemHeightTabOrder  	TComboBoxSigOrigComBoxLeftxTop� WidthaHeightAnchorsakLeftakBottom 
ItemHeightTabOrder  TEditSigLeftEditLeft� TopWidthAHeightAnchorsakLeftakBottom TabOrderText0  TEdit
SigTopEditLeft� Top$WidthAHeightAnchorsakLeftakBottom TabOrderText0  TButton	SetSigBtnLeft� TopAWidthWHeightAnchorsakLeftakBottom CaptionSet Signat. ParTabOrderOnClickSetSigBtnClick  TButton	GetSigBtnLeftTopAWidthWHeightAnchorsakLeftakBottom CaptionGet Signat. ParTabOrderOnClickGetSigBtnClick  TButton	InsSigBtnLeftTop!WidthWHeightAnchorsakLeftakBottom CaptionInsert SignatureTabOrderOnClickInsSigBtnClick  	TGroupBoxSigLocGrBoxLeftTop,Width� Height9CaptionSignature LocationTabOrder	 TLabelLabel11LeftTopWidth.HeightCaptionOne TimeFocusControlOneTimeSigRButLayouttlCenter  TLabelLabel12LeftTop Width/HeightCaptionOn PagesFocusControlOnPagesSigRButLayouttlCenter  TRadioButtonOneTimeSigRButLeftTopWidthHeightCaptionPortraitRButChecked	TabOrder TabStop	OnClickSigLocRButClick  TRadioButtonOnPagesSigRButLeftTop WidthHeightCaptionPortraitRButTabOrderOnClickSigLocRButClick  	TCheckBoxSigFirstChkBoxLeftLTopWidth'HeightAnchorsakLeftakBottom CaptionFirstChecked	EnabledState	cbCheckedTabOrderOnClickKeepScaleCheckBoxClick  	TCheckBoxSigIntChkBoxLeft}TopWidth1HeightAnchorsakLeftakBottom CaptionInternChecked	EnabledState	cbCheckedTabOrderOnClickKeepScaleCheckBoxClick  	TCheckBoxSigLastChkBoxLeft� TopWidth)HeightAnchorsakLeftakBottom CaptionLastChecked	EnabledState	cbCheckedTabOrderOnClickKeepScaleCheckBoxClick   	TGroupBox	GroupBox4LeftTophWidth� Height~AnchorsakLeftakTopakBottom CaptionFont SettingsTabOrder
 TLabelLabel1Left� TopWidthHeightCaptionSize:FocusControlFontSizeEdit  TLabelLabel2Left
TopHWidthHeightCaptionColor:FocusControlFontColorEdit  TLabelLabel9Left
Top`WidthHeightCaptionBack:FocusControlFontBackColorEdit  TEditFontNameEditLeft
TopWidth_HeightTabOrder TextArial  	TComboBoxTextAlignComBoxLeft
Top,Width_Height
ItemHeightTabOrder  	TCheckBoxItalicChkBoxLeftxTop$WidthYHeightAnchorsakLeftakBottom CaptionItalicTabOrderOnClickToFileCheckBoxClick  	TCheckBox
BoldChkBoxLeftxTop8WidthYHeightAnchorsakLeftakBottom CaptionBoldTabOrderOnClickToFileCheckBoxClick  	TCheckBoxTranspChkBoxLeftxTop`WidthYHeightAnchorsakLeftakBottom CaptionTransparentTabOrderOnClickToFileCheckBoxClick  	TCheckBox
UndlChkBoxLeftxTopLWidthYHeightAnchorsakLeftakBottom Caption
UnderlinedTabOrderOnClickToFileCheckBoxClick  TEditFontSizeEditLeft� TopWidth!HeightTabOrderText12  TEditFontColorEditLeft(TopDWidthAHeightTabOrderText0  TEditFontBackColorEditLeft(Top\WidthAHeightTabOrderText0    	TGroupBox	GroupBox5Left� Top�Width� Height`CaptionTemplate Store/RestoreTabOrder TLabel
OptEditLblLeft� TopWidthHeightCaptionOpt:FocusControlOptEdit  TLabelTempNameLabLeftTopWidthHeightCaptionFile:FocusControl	ScaleEdit  TEditOptEditLeft� TopWidth!HeightTabOrder Text0  TEditTemplateNameEditLeft TopWidthHeightTabOrderTextD:\TEMP\Template.wpt  TButtonSaveTempBtnLeft� Top5WidthWHeightCaptionStore TemplateTabOrderOnClickSaveTempBtnClick  TButtonOpenTempBtnLeft"Top5WidthWHeightCaptionLoad TemplateTabOrderOnClickOpenTempBtnClick   	TGroupBox	GroupBox6LeftTop Width� Height� Caption)Common Information About Ptinting ObjectsTabOrder	 TLabelLabel10Left� TopWidthHeight	AlignmenttaRightJustifyCaptionMaps:FocusControl
MapNumEdit  TLabelLabel13Left� Top0Width,Height	AlignmenttaRightJustifyCaptionLegends:FocusControl
LegNumEdit  TLabelLabel14Left� TopHWidth)Height	AlignmenttaRightJustifyCaption	Pictures:FocusControl
PicNumEdit  TLabelLabel15Left}Top`Width5Height	AlignmenttaRightJustifyCaptionSignatures:FocusControl
SigNumEdit  TLabelLabel16Left� Top� Width'Height	AlignmenttaRightJustifyCaptionObjects:FocusControl
ObjNumEdit  TLabelLabel17LeftTopWidth� HeightCaptionNum:  Type:  ObjectID:       FocusControlLeftEdit  TListBoxObjectsListBoxLeftTop WidthqHeight� AnchorsakLeftakTopakBottom Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ItemHeight
ParentFontTabOrder   TButtonGetObjInfBtnLeft� Top� WidthWHeightAnchorsakLeftakBottom CaptionGet Objects InfTabOrderOnClickGetObjInfBtnClick  TEdit
MapNumEditLeft� TopWidth!HeightTabOrderText0  TEdit
LegNumEditLeft� Top,Width!HeightTabOrderText0  TEdit
PicNumEditLeft� TopDWidth!HeightTabOrderText0  TEdit
SigNumEditLeft� Top\Width!HeightTabOrderText0  TEdit
ObjNumEditLeft� Top|Width!HeightTabOrderText0   	TGroupBox	GroupBox7Left� TopCWidth� Height� CaptionMove/Delete Printing ObjectTabOrder
 TLabelLabel18Left� TopWidthHeightCaptionID:FocusControlObjectIDEdit  TLabelOptLblLeftTop3Width'HeightCaptionOptions:FocusControlOptionsComBox  TLabelLabel19LeftTopWidthHeightCaptionType:FocusControl
TypeComBox  TEditObjectIDEditLeft� TopWidth!HeightTabOrder Text0  TButton	DelObjBtnLeft� TopWWidthWHeightAnchorsakLeftakBottom CaptionDelete ObjectTabOrderOnClickDelObjBtnClick  TButton	MovObjBtnLeft"TopWWidthWHeightAnchorsakLeftakBottom CaptionMove ObjectTabOrderOnClickMovObjBtnClick  	TComboBoxOptionsComBoxLeft8Top0WidthiHeight
ItemHeightTabOrder  	TComboBox
TypeComBoxLeft8TopWidthiHeight
ItemHeightTabOrder    