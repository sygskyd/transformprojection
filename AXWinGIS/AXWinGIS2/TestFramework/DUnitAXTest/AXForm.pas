unit AXForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, AXWINGISLib2_TLB, StdCtrls,DeCal, AXWINGISLib2_TLB;

type
  TForm1 = class(TForm)
    AXWingis21: TAXWingis2;
    procedure AXWingis21ActiveLayer(Sender: TObject;
      const sLayer: WideString);
    procedure AXWingis21Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    bEventCame : Boolean;
    eventData : DList;  (*eine generische Liste mit variant objekten siehe DeCal doku! *)
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.AXWingis21ActiveLayer(Sender: TObject;
  const sLayer: WideString);
  var aLayer : String;
begin
  aLayer := sLayer; (*notwendig, da DeCal keine WideStrings kennt!*)
  self.eventData.pushFront([aLayer]); (*f�ge den Layernamen am begin der Liste ein*)
  self.bEventCame := True; (*zeige an das ein event gekommen ist*)
end;

procedure TForm1.AXWingis21Click(Sender: TObject);
begin
self.AXWingis21.DoDisconnect;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  self.eventData := DList.create;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  DeCal.FreeAll([ eventData (*hier alle DeCal objekte mit "," getrennt eintragen*) ]);
  self.AXWingis21.DoDisconnect;
end;

end.
