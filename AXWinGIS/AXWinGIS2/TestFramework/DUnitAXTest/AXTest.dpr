program AXTest;

uses
  Forms,
  AXForm in 'AXForm.pas' {Form1},
  TestCase in 'TestCase.pas',
  TestFramework,
  GUITestRunner;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  TGUITestRunner.runRegisteredTests;
  Application.Run;
end.
