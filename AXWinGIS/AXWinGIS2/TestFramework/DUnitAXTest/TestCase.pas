unit TestCase;

interface
uses Windows, Messages, SysUtils,Classes, Graphics, Controls, Forms,
     TestFrameWork, (*DUnit*)
     DeCal,  (*generische DeCal Funktionen*)
     AXForm;

type
  {Testfall f�r AXWingis.ocx}
  TTestCaseAX = class(TTestCase) (*wird von DUnit hergeleitet*)
  private
    {Add an instance variable for every fixture (i.e. starting situation)
     you wish to use.}
  protected
    procedure SetUp; override;
    procedure TearDown; override;
    {Hier k�nnen noch lokale variablen stehen...}
  published
    {Hier die Testmethoden eintragen _alle_ ohne Parameter!!}
    procedure testConnection;
    procedure testActiveLayer;
  end;

implementation

procedure TTestCaseAX.SetUp;
var counter : Integer;
    apath : string;
begin
(* hier wird alles eingetragen was _vor_ jedem Testfall auszuf�hren ist*)
 if not AXForm.Form1.AXWingis21.bConnected then
 begin
  AXForm.Form1.Show;
  AXForm.Form1.AXWingis21.bUseCOM := True;
  AXForm.Form1.AXWingis21.bActive := True;
  AXForm.Form1.AXWingis21.DoConnect('Test','');
  apath := extractfilepath(paramstr(0));
  AXForm.Form1.AXWingis21.DoOpenProject(apath + 'TestCase.amp');
  counter := 20;
  repeat
    sleep(50);
    Application.ProcessMessages;
    Dec(counter);
  until AXForm.Form1.AXWingis21.bConnected or (counter = 0);
 end;
end;

procedure TTestCaseAX.TearDown;
begin
(* hier wird alles eingetragen was _nach_ jedem Testfall auszuf�hren ist*)
end;

{ Ein Beispiel f�r Befehle mit einem R�ckgabewert}
procedure TTestCaseAX.testConnection;
begin
  {hier noch vorher Objekte im Wingis mit AXBefehlen selektieren}

  {jetzt �berpr�fen wir das Ergebnis syntax:= <boolscher Ausdruck> <Meldung bei FALSE>}
  self.Check(AXForm.Form1.AXWingis21.DoDeselectAll,'upsala');
  { nochmals die selektierten Objekte mit einem AXBefehl auslesen}
end;

{ Ein Beispiel f�r Event basierende Befehle}
procedure TTestCaseAX.testActiveLayer;
var counter : Integer;
    aLayer : String;
begin
  {der Befehl den wir Testen wollen ... }
  AXForm.Form1.AXWingis21.DoGetActiveLayer;

  {warte bis der Event gekommen ist "bEventCame" variable auf true AXWingis1ActiveLayer}
  counter := 20;
  repeat
   sleep(30);
   Application.ProcessMessages;
   Dec(counter);
  until AXForm.Form1.bEventCame or (counter = 0);

  if ( AXForm.Form1.bEventCame ) then
  begin
    {hat uns ein Event erreicht so testen wir das ergebnis...}

    {wir lesen das erste Element aus der Liste aus... (ist ein Variant
     daher m�ssen wir es in einen string konvertieren)}
    aLayer := DeCal.asString(AXForm.Form1.eventData.popFront);

    {jetzt �berpr�fen wir das Ergebnis syntax:= <erwarteter Wert> <tats�chlicher Wert> <Meldung bei Unterschied>}
    self.CheckEquals('bridge',aLayer,'upsala');
    AXForm.Form1.bEventCame := false;
  end
  else
  begin
    AXForm.Form1.bEventCame := false;
    self.Check(false,'no event came...');
  end;
end;

initialization
(* Damit das Programm weiss was alles zu testen ist registrieren wir das Objekt noch*)
  RegisterTest('', TTestCaseAX.Suite);

end.
