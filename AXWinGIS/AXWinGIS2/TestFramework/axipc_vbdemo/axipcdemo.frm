VERSION 5.00
Object = "{044B6803-C387-11D3-8FA9-0000B4BDFE28}#1.0#0"; "AXIpc.ocx"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   4860
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7260
   LinkTopic       =   "Form1"
   ScaleHeight     =   4860
   ScaleWidth      =   7260
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command6 
      Caption         =   "Stop"
      Height          =   390
      Left            =   5550
      TabIndex        =   23
      Top             =   3825
      Width           =   990
   End
   Begin VB.CheckBox cbStopStress 
      Caption         =   "Stop"
      Height          =   240
      Left            =   5700
      TabIndex        =   22
      Top             =   3075
      Width           =   915
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Stress Test"
      Height          =   390
      Left            =   5550
      TabIndex        =   20
      Top             =   2625
      Width           =   990
   End
   Begin VB.TextBox efServiceName 
      Height          =   315
      Left            =   225
      TabIndex        =   18
      Text            =   "MyOwnName"
      Top             =   1500
      Width           =   1665
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Disconnect"
      Height          =   315
      Left            =   2625
      TabIndex        =   17
      Top             =   1350
      Width           =   1065
   End
   Begin VB.TextBox efReceived 
      Height          =   1965
      Left            =   225
      MultiLine       =   -1  'True
      TabIndex        =   15
      Top             =   2775
      Width           =   5115
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Send"
      Height          =   315
      Left            =   5550
      TabIndex        =   14
      Top             =   2100
      Width           =   990
   End
   Begin VB.TextBox efSend 
      Height          =   315
      Left            =   225
      TabIndex        =   13
      Text            =   "Hello there"
      Top             =   2100
      Width           =   5115
   End
   Begin VB.Frame Frame2 
      Caption         =   "act as server"
      Height          =   990
      Left            =   825
      TabIndex        =   6
      Top             =   150
      Width           =   2865
      Begin VB.TextBox efSrvPort 
         Height          =   285
         Left            =   1800
         TabIndex        =   8
         Text            =   "0"
         Top             =   300
         Width           =   915
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Listen"
         Height          =   315
         Left            =   75
         TabIndex        =   7
         Top             =   300
         Width           =   915
      End
      Begin VB.Label Label4 
         Caption         =   "default(0) is 27077"
         Height          =   240
         Left            =   1125
         TabIndex        =   10
         Top             =   675
         Width           =   1665
      End
      Begin VB.Label Label1 
         Caption         =   "on Port:"
         Height          =   165
         Left            =   1125
         TabIndex        =   9
         Top             =   375
         Width           =   615
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "act as client"
      Height          =   1665
      Left            =   3975
      TabIndex        =   0
      Top             =   75
      Width           =   3015
      Begin VB.TextBox efCliDestAddr 
         Height          =   285
         Left            =   1950
         TabIndex        =   3
         Top             =   1050
         Width           =   915
      End
      Begin VB.TextBox efCliDestPort 
         Height          =   285
         Left            =   1950
         TabIndex        =   2
         Text            =   "0"
         Top             =   375
         Width           =   915
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Connect"
         Height          =   315
         Left            =   75
         TabIndex        =   1
         Top             =   375
         Width           =   915
      End
      Begin VB.Label Label6 
         Caption         =   "defaul is 127.0.0.1"
         Height          =   240
         Left            =   1050
         TabIndex        =   12
         Top             =   1350
         Width           =   1665
      End
      Begin VB.Label Label5 
         Caption         =   "default(0) is 27077"
         Height          =   240
         Left            =   1125
         TabIndex        =   11
         Top             =   675
         Width           =   1665
      End
      Begin VB.Label Label3 
         Caption         =   "to Address:"
         Height          =   240
         Left            =   1050
         TabIndex        =   5
         Top             =   1050
         Width           =   840
      End
      Begin VB.Label Label2 
         Caption         =   "to Port:"
         Height          =   240
         Left            =   1125
         TabIndex        =   4
         Top             =   375
         Width           =   765
      End
   End
   Begin AXIPCLib.AXIpc AXIpc 
      Left            =   75
      Top             =   75
      _Version        =   65536
      _ExtentX        =   820
      _ExtentY        =   688
      _StockProps     =   0
   End
   Begin VB.Label laStressCounter 
      Caption         =   "0"
      Height          =   240
      Left            =   5775
      TabIndex        =   21
      Top             =   3450
      Width           =   615
   End
   Begin VB.Label Label8 
      Caption         =   "ServiceName"
      Height          =   240
      Left            =   225
      TabIndex        =   19
      Top             =   1275
      Width           =   1515
   End
   Begin VB.Label Label7 
      Caption         =   "Received"
      Height          =   165
      Left            =   225
      TabIndex        =   16
      Top             =   2550
      Width           =   840
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const AXIPC_STATUS_NOT_CONNECTED = 0
Const AXIPC_STATUS_LISTENING = 1
Const AXIPC_STATUS_CONNECTED = 2

Dim bStop As Boolean

Private Sub AXIpc_OnConnect(ByVal lSessionID As Long, ByVal sRemoteName As String)
    efReceived.Text = efReceived.Text + vbCrLf + "OnConnect(" + CStr(lSessionID) + "," + sRemoteName + ")"
    UpdateStatus
End Sub

Private Sub AXIpc_OnDisconnect(ByVal lSessionID As Long)
    efReceived.Text = efReceived.Text + vbCrLf + "OnDisconnect(" + CStr(lSessionID) + ")"
    UpdateStatus
End Sub

Private Sub AXIpc_OnReceive(ByVal sCmd As String, ByVal sSessionID As Long)
Dim iStressCount As Integer

    If Left(sCmd, Len("STRESSTEST:")) = "STRESSTEST:" Then
       iStressCount = CInt(Right(sCmd, Len(sCmd) - Len("STRESSTEST:")))
       laStressCounter.Caption = CStr(iStressCount)
       DoEvents
    Else
       efReceived.Text = efReceived.Text + vbCrLf + sCmd
    End If
End Sub

Private Sub Command1_Click()
    AXIpc.ServiceName = efServiceName.Text
    Call AXIpc.Connect(efCliDestAddr.Text, CLng(efCliDestPort.Text))
    UpdateStatus
End Sub

Private Sub Command2_Click()
    AXIpc.ServiceName = efServiceName.Text
    AXIpc.Listen
    UpdateStatus
End Sub

Private Sub Command3_Click()
    efReceived.Text = efReceived.Text + vbCrLf + "Starting send ..."
    Call AXIpc.Send(efSend.Text)
    efReceived.Text = efReceived.Text + vbCrLf + "Send back"
End Sub

Private Sub Command4_Click()
    AXIpc.Disconnect
    UpdateStatus
End Sub

Private Sub Command5_Click()
Dim i As Long
Dim lErr As Long
   
    efReceived.Text = efReceived.Text + vbCrLf + "Starting stress test"
    
    cbStopStress.Value = 0
    bStop = False
    i = 0
    lErr = 0
    Do
      i = i + 1
      DoEvents
      'Me.SetFocus
      lErr = AXIpc.Send("STRESSTEST:" + CStr(i))
      'Me.Show
      Me.Refresh
      DoEvents
    Loop While (lErr = 0) And ((bStop = False) And (cbStopStress.Value = 0))
    
    efReceived.Text = efReceived.Text + vbCrLf + "We made " + CStr(i) + " loops"
    efReceived.Text = efReceived.Text + vbCrLf + AXIpc.GetLastErrorMsg
End Sub

Private Sub Command6_Click()
    bStop = True
End Sub

Private Sub Form_Load()
    Caption = "Not Connected"
End Sub

Sub UpdateStatus()
    If AXIpc.Status = AXIPC_STATUS_NOT_CONNECTED Then
       Caption = "Not Connected"
    ElseIf AXIpc.Status = AXIPC_STATUS_LISTENING Then
       Caption = "Waiting for clients"
    ElseIf AXIpc.Status = AXIPC_STATUS_CONNECTED Then
       If AXIpc.IsServer Then
          Caption = "Connected [" + CStr(AXIpc.GetSessionCount) + " Client(s)]"
       Else
          Caption = "Connected"
       End If
    Else
      Caption = ""
    End If
    
End Sub
