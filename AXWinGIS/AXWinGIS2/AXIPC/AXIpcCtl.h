#if !defined(AFX_AXIPCCTL_H__044B6814_C387_11D3_8FA9_0000B4BDFE28__INCLUDED_)
#define AFX_AXIPCCTL_H__044B6814_C387_11D3_8FA9_0000B4BDFE28__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DebugWin.h"



class CAXIpcSocket;


// AXIpcCtl.h : Declaration of the CAXIpcCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CAXIpcCtrl : See AXIpcCtl.cpp for implementation.

class CAXIpcCtrl : public COleControl
{
	DECLARE_DYNCREATE(CAXIpcCtrl)

// Constructor
public:
	CAXIpcCtrl();

    CDebug Debug;

    CAXIpcSocket *m_pSock;
    CPtrList m_connectionList;
    int m_status;
    BOOL m_isServer;

    BOOL lAckFlag;
    BOOL bInsideRead;
    LONG lInsideSend;

    // Error handling
    long lLastError;
    CString csLastErrorMsg;

    // mk - 
	// dynamic port allocation
	short getLastSocketFromRegistry(void);
	short getSocketInfoFromReg(void);
	void deleteSocketInfoFromReg(void);
	int m_slot;
	// - mk
    
    // socket-event handlers
    void ProcessPendingAccept();
	  void ProcessPendingRead(CAXIpcSocket* pSocket, int nErrorCode);
    void ProcessClose(CAXIpcSocket* pSocket);

    void CloseSocket(CAXIpcSocket* pSocket);
    void CloseAllSocket();
    
    long GetErrorCodeFromSocketError(int iError);

    BOOL WaitForAckTimeout(long lTimeOut);
    void GetAckTimeoutValue();

    UINT ThreadProc(CString csCmd);


    long m_lAckTimeout;

    CRITICAL_SECTION csSl;
    HANDLE hsemAXIPC_Send;

    // Worker Thread proc
    UINT ReceiveProc( LPVOID pParam );

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAXIpcCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CAXIpcCtrl();

	DECLARE_OLECREATE_EX(CAXIpcCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CAXIpcCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CAXIpcCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CAXIpcCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CAXIpcCtrl)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CAXIpcCtrl)
	short m_port;
	afx_msg void OnPortChanged();
	CString m_serviceName;
	afx_msg void OnServiceNameChanged();
	short m_protocol;
	afx_msg void OnProtocolChanged();
	afx_msg short GetStatus();
	afx_msg BOOL GetIsServer();
	afx_msg long Connect(LPCTSTR sDestination, short iRemotePort);
	afx_msg long Send(LPCTSTR sCmd);
	afx_msg long Disconnect();
	afx_msg long Listen();
	afx_msg long GetLastError();
	afx_msg BSTR GetLastErrorMsg();
	afx_msg long GetSessionCount();
	afx_msg long GetSessionID(long lIndex);
	afx_msg long SendTo(LPCTSTR sCmd, long lSessionID);
	afx_msg long SendWait(LPCTSTR sCmd);
	afx_msg long SendToWait(LPCTSTR sCmd, long lSessionID);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CAXIpcCtrl)
	void FireOnConnect(long lSessionID, LPCTSTR sRemoteName)
		{FireEvent(eventidOnConnect,EVENT_PARAM(VTS_I4  VTS_BSTR), lSessionID, sRemoteName);}
	void FireOnDisconnect(long lSessionID)
		{FireEvent(eventidOnDisconnect,EVENT_PARAM(VTS_I4), lSessionID);}
	void FireOnReceive(LPCTSTR sCmd, long sSessionID)
		{FireEvent(eventidOnReceive,EVENT_PARAM(VTS_BSTR  VTS_I4), sCmd, sSessionID);}
	void FireOnAck(long lTransID)
		{FireEvent(eventidOnAck,EVENT_PARAM(VTS_I4), lTransID);}
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CAXIpcCtrl)
	dispidPort = 1L,
	dispidStatus = 4L,
	dispidServiceName = 2L,
	dispidProtocol = 3L,
	dispidIsServer = 5L,
	dispidConnect = 6L,
	dispidSend = 7L,
	dispidDisconnect = 8L,
	dispidListen = 9L,
	dispidGetLastError = 10L,
	dispidGetLastErrorMsg = 11L,
	dispidGetSessionCount = 12L,
	dispidGetSessionID = 13L,
	dispidSendTo = 14L,
	dispidSendWait = 15L,
	dispidSendToWait = 16L,
	eventidOnConnect = 1L,
	eventidOnDisconnect = 2L,
	eventidOnReceive = 3L,
	eventidOnAck = 4L,
	//}}AFX_DISP_ID
	};
};




// Socket class

class CAXIpcSocket : public CAsyncSocket
{
	DECLARE_DYNAMIC(CAXIpcSocket);

private:
	  CAXIpcSocket(const CAXIpcSocket& rSrc);         // no implementation
	  void operator=(const CAXIpcSocket& rSrc);  // no implementation

// Construction
public:
	  CAXIpcSocket( CAXIpcCtrl *pOwner );
    
    BOOL Create(UINT nSocketPort = 0);
    long Send(LPCTSTR sCmd);

// Attributes
public:
    CAXIpcCtrl *pAXIpc;
	  long lSessionID;
    CString csRemoteName;
    BOOL bConnected;

// Overridable callbacks
protected:
	  virtual void OnAccept(int nErrorCode);
    virtual void OnReceive( int nErrorCode );
    virtual void OnClose( int nErrorCode );

// Implementation
public:
	virtual ~CAXIpcSocket();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

class CIPCThreadData : public CObject
{

// Constructor
public:
   CIPCThreadData(CString csIn, CAXIpcCtrl * pMain)
     {
      csCmd = csIn;
      pCtrl = pMain;
     };
   
   ~CIPCThreadData()
     {
     };

   CAXIpcCtrl * pCtrl;
   CString csCmd;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line

#endif // !defined(AFX_AXIPCCTL_H__044B6814_C387_11D3_8FA9_0000B4BDFE28__INCLUDED)
