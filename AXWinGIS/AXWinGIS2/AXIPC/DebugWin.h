
#ifndef DEBUGWIN_INCLUDED
#define DEBUGWIN_INCLUDED

#ifdef _DEBUG
#define _DEBUGWIN_ON
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//#define _DEBUGWIN_ON
//#define _DEBUGWIN_THREAD
#define _DEBUGWIN_CMDLOG FALSE


#define AXIPC_VERSION  "AXIpc 0.88"


typedef struct _sDebugInfo
{
   LPVOID lpvMemT;
   HWND hDebugWinT;

} sDebugInfo;


class CDebug : public CObject
{

// Constructor
public:
   CDebug();
   
   ~CDebug();
   
   void Print(const char* sOut, const BOOL bAlwaysLog = FALSE);
   void PrintInt(const char* sOut, int iVal, int iRadix);
   void KillThread();
   UINT ThreadProc();
   
protected:
   HANDLE hMap;
   LPVOID lpvMem;
   BOOL bOk;
   HWND hDebugWin;
   int iDebugWinFindTry;

#ifdef _DEBUGWIN_THREAD
   sDebugInfo sDeb;
   CStringList slDebug; 
   HANDLE hSem;
   BOOL bExitThread;
   CRITICAL_SECTION csSl;
   BOOL bInitialized;
   CWinThread* pThread;
   BOOL bKilled;
#endif

};
#endif //DEBUGWIN_INCLUDED