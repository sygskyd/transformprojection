
#ifndef EM_C_TOOLS_INCLUDED
#define EM_C_TOOLS_INCLUDED


// is unlocked (no license needed) version if defined
//#define AX_OPEN_VERSION 
// **************************************************

// if defined the licensing functions output debug messages, too 
// should NOT be defined for distributing axwingis debug-versions
// to customers
//#define DEBUG_LICMAN
// **************************************************


#define AXMODE_INVALID  0
#define AXMODE_STANDARD 1
#define AXMODE_ADVANCED 2

#define AXLIC_OK                 0
#define AXLIC_INVALID_SYSTEMDATE 1
#define AXLIC_TIMED_OUT          2
#define AXLIC_NOT_FOUND          3
#define AXLIC_INVALID            4

typedef struct _structLicense
{
 ULONG ulAXMode;
 int iKeyNr;         // the number, under which the regkey is stored in the registry
 CString csAppName;
 long lDaysLicensed;
 long lDaysLeft;
 time_t tFirstUsed;
 time_t tLastUsed;
 CString csSerialNr;
 BOOL bModified;
} structLicense;


#define AXWINGIS_REGKEY "Software\\Progis\\AXWingis"
#define HKEY_AXROOT HKEY_LOCAL_MACHINE

CString IntToStr(long lValue, int iBase = 10);
CString UIntToStr(long lValue, int iBase = 10);
long StrToInt(const char * csValue, int iBase = 10);

CString IntToStrPad(long lValue, char cPad, int iLen, int iBase = 10);
CString IntToStrCut(long lValue, char cPad, int iLen);

CString DToStr(double dValue);
double StrToD(CString csValue);

CString BoolToStr(BOOL bValue);
BOOL StrToBool(CString csValue);

long SetRegValue(CString cskey, CString csValue, CString csData);
CString GetRegValue(CString cskey, CString csValue);

CString Crypt(CString csKey, CString csData);
CString Decrypt(CString csFP, CString csHexStr );

CString GetSubString (const char *CmdStr, int pos, char cSep);
int GetSubStringCount(const char *CmdStr, char cSep);

long SetLicValue(CString cskey, CString csValue, CString csData, CString csFP);
CString GetLicValue(CString cskey, CString csValue, CString csFP);

BOOL DecodeAllLicenses(CString &csFingerPrint, CPtrList &cList);
BOOL DecodeLicenseString(structLicense *pLicense, CString csLKey);
BOOL UpdateLicense(structLicense *pLicense, BOOL bLicenseActive = FALSE);
BOOL StoreLicense(structLicense *pLicense, CString csFP, CString &csErrMsg);
int IsLicenseValid(structLicense *pLicense, CString csAppName = "", BOOL bDaysMayBeZero = FALSE);

BOOL TestOnlyDesignLicense();

BOOL ChecksumValid(CString csLKey, int iLKNr);
BOOL UpdateChecksum(CString csLKey, int iLKNr);

BOOL AddLicenseKey(CString csLKey, CString &csErrMsg);
BOOL ValidateFingerPrint(CString &csFingerPrint);

BOOL IsLicenseSerialUnique(structLicense *pTestLicense);

void ClearLicenseList(CPtrList &cLicenseList);

#endif