
#define AXWINGIS_VERSION  "1.1.0.61"


#define COMPILE_MAIN
//#define COMPILE_SPECIAL_BEV
//#define COMPILE_SPECIAL_FORST
//#define COMPILE_SPECIAL_WINMON
//#define COMPILE_SPECIAL_GEOQUIZ
//#define COMPILE_SPECIAL_ZRWIN
//#define COMPILE_SPECIAL_LABIS
//#define COMPILE_SPECIAL_HOFKARTE
//#define COMPILE_SPECIAL_LABISTRACK
//#define COMPILE_SPECIAL_HOFKARTE2
//#define COMPILE_SPECIAL_ZRWIN2
//#define COMPILE_SPECIAL_AGRO
//#define COMPILE_SPECIAL_HOFKARTE4

#ifdef COMPILE_MAIN
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Main Version

// this is the 7/8 digit of the UUID, MUST be 2 digits !!!
// by now it is possible to generate only 100 unique versions
// just select a different 2 digit number which has not been used before, if
// you want to generate a new version   (hint - use 09, not 9, if below 10)
#define ODL_ID_VERSION( n )      78##n

// These are only captions, choose anything you want
#define TL_NAME_MAIN             "AXWingis Control module"
#define CLASS_NAME_MAIN	         "AXWingis Control"
#define SPECIAL_MAIN             "Special Debug Build"

// The Programm IDs - change only the last part
#define PROG_ID                  "AXWINGIS.AXWingisCtrl.1"
#define PROG_ID_PPG              "AXWINGIS.AXWingisPropPage.1"
#define PROG_ID_PPG_LIC          "AXWINGIS.AXWingisPropPageLicense.1"
#endif

#ifdef COMPILE_SPECIAL_BEV
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// BEV Version

// this is the 7/8 digit of the UUID, MUST be 2 digits !!!
// by now it is possible to generate only 100 unique versions
// just select a different 2 digit number which has not been used before, if
// you want to generate a new version   (hint - use 09, not 9, if below 10)
#define ODL_ID_VERSION( n )      79##n                            

// These are only captions, choose anything you want
#define TL_NAME_MAIN             "AXWingis BEV Control module"
#define CLASS_NAME_MAIN	         "AXWingis BEV Control"
#define SPECIAL_MAIN             "BEV"

// The Programm IDs - change only the last part
#define PROG_ID                  "AXWINGIS.AXWingisCtrl.BEV"
#define PROG_ID_PPG              "AXWINGIS.AXWingisPropPage.BEV"
#define PROG_ID_PPG_LIC          "AXWINGIS.AXWingisPropPageLicense.BEV"

// this is used to give this version an open advanced functionality without a license-key
// the sAppName property must match this constant
#define AX_SPECIAL_VERSION       "BEV"
#endif


#ifdef COMPILE_SPECIAL_FORST
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// FORST Version

// this is the 7/8 digit of the UUID, MUST be 2 digits !!!
// by now it is possible to generate only 100 unique versions
// just select a different 2 digit number which has not been used before, if
// you want to generate a new version   (hint - use 09, not 9, if below 10)
#define ODL_ID_VERSION( n )       80##n                            

// These are only captions, choose anything you want
#define TL_NAME_MAIN             "AXWingis FORST Control module"
#define CLASS_NAME_MAIN	         "AXWingis FORST Control"
#define SPECIAL_MAIN             "Forst"

// The Programm IDs - change only the last part
#define PROG_ID                  "AXWINGIS.AXWingisCtrl.FORST"
#define PROG_ID_PPG              "AXWINGIS.AXWingisPropPage.FORST"
#define PROG_ID_PPG_LIC          "AXWINGIS.AXWingisPropPageLicense.FORST"

// this is used to give this version an open advanced functionality without a license-key
// the sAppName property must match this constant
#define AX_SPECIAL_VERSION       "FORST"
#endif



#ifdef COMPILE_SPECIAL_WINMON
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// WINMON Version

// this is the 7/8 digit of the UUID, MUST be 2 digits !!!
// by now it is possible to generate only 100 unique versions
// just select a different 2 digit number which has not been used before, if
// you want to generate a new version   (hint - use 09, not 9, if below 10)
#define ODL_ID_VERSION( n )       81##n                            

// These are only captions, choose anything you want
#define TL_NAME_MAIN             "AXWingis WINMON Control module"
#define CLASS_NAME_MAIN	         "AXWingis WINMON Control"
#define SPECIAL_MAIN             "WinMon"

// The Programm IDs - change only the last part
#define PROG_ID                  "AXWINGIS.AXWingisCtrl.WINMON"
#define PROG_ID_PPG              "AXWINGIS.AXWingisPropPage.WINMON"
#define PROG_ID_PPG_LIC          "AXWINGIS.AXWingisPropPageLicense.WINMON"

// this is used to give this version an open advanced functionality without a license-key
// the sAppName property must match this constant
#define AX_SPECIAL_VERSION       "WINMON"
#endif


#ifdef COMPILE_SPECIAL_GEOQUIZ
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// GEOQUIZ Version

// this is the 7/8 digit of the UUID, MUST be 2 digits !!!
// by now it is possible to generate only 100 unique versions
// just select a different 2 digit number which has not been used before, if
// you want to generate a new version   (hint - use 09, not 9, if below 10)
#define ODL_ID_VERSION( n )       82##n                            

// These are only captions, choose anything you want
#define TL_NAME_MAIN             "AXWingis GEOQUIZ Control module"
#define CLASS_NAME_MAIN	         "AXWingis GEOQUIZ Control"
#define SPECIAL_MAIN             "GeoQuiz"

// The Programm IDs - change only the last part
#define PROG_ID                  "AXWINGIS.AXWingisCtrl.GEOQUIZ"
#define PROG_ID_PPG              "AXWINGIS.AXWingisPropPage.GEOQUIZ"
#define PROG_ID_PPG_LIC          "AXWINGIS.AXWingisPropPageLicense.GEOQUIZ"

// this is used to give this version an open advanced functionality without a license-key
// the sAppName property must match this constant
#define AX_SPECIAL_VERSION       "GEOQUIZ"
#endif


#ifdef COMPILE_SPECIAL_ZRWIN
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ZRWIN Version

// this is the 7/8 digit of the UUID, MUST be 2 digits !!!
// by now it is possible to generate only 100 unique versions
// just select a different 2 digit number which has not been used before, if
// you want to generate a new version   (hint - use 09, not 9, if below 10)
#define ODL_ID_VERSION( n )       83##n                            

// These are only captions, choose anything you want
#define TL_NAME_MAIN             "AXWingis ZRWIN Control module"
#define CLASS_NAME_MAIN	         "AXWingis ZRWIN Control"
#define SPECIAL_MAIN             "ZrWin"

// The Programm IDs - change only the last part
#define PROG_ID                  "AXWINGIS.AXWingisCtrl.ZRWIN"
#define PROG_ID_PPG              "AXWINGIS.AXWingisPropPage.ZRWIN"
#define PROG_ID_PPG_LIC          "AXWINGIS.AXWingisPropPageLicense.ZRWIN"

// this is used to give this version an open advanced functionality without a license-key
#define AX_SPECIAL_VERSION       "ZRWIN"
#endif

#ifdef COMPILE_SPECIAL_LABIS
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// LABIS Version

// this is the 7/8 digit of the UUID, MUST be 2 digits !!!
// by now it is possible to generate only 100 unique versions
// just select a different 2 digit number which has not been used before, if
// you want to generate a new version   (hint - use 09, not 9, if below 10)
#define ODL_ID_VERSION( n )       84##n                            

// These are only captions, choose anything you want
#define TL_NAME_MAIN             "AXWingis LABIS Control module"
#define CLASS_NAME_MAIN	         "AXWingis LABIS Control"
#define SPECIAL_MAIN             "Labis"

// The Programm IDs - change only the last part
#define PROG_ID                  "AXWINGIS.AXWingisCtrl.LABIS"
#define PROG_ID_PPG              "AXWINGIS.AXWingisPropPage.LABIS"
#define PROG_ID_PPG_LIC          "AXWINGIS.AXWingisPropPageLicense.LABIS"

// this is used to give this version an open advanced functionality without a license-key
// the sAppName property must match this constant
#define AX_SPECIAL_VERSION       "LABIS"
#endif

#ifdef COMPILE_SPECIAL_HOFKARTE
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Hofkarte Version

// this is the 7/8 digit of the UUID, MUST be 2 digits !!!
// by now it is possible to generate only 100 unique versions
// just select a different 2 digit number which has not been used before, if
// you want to generate a new version   (hint - use 09, not 9, if below 10)
#define ODL_ID_VERSION( n )       85##n                            

// These are only captions, choose anything you want
#define TL_NAME_MAIN             "AXWingis HOFKARTE Control module"
#define CLASS_NAME_MAIN	         "AXWingis HOFKARTE Control"
#define SPECIAL_MAIN             "Hofkarte"

// The Programm IDs - change only the last part
#define PROG_ID                  "AXWINGIS.AXWingisCtrl.HOFKARTE"
#define PROG_ID_PPG              "AXWINGIS.AXWingisPropPage.HOFKARTE"
#define PROG_ID_PPG_LIC          "AXWINGIS.AXWingisPropPageLicense.HOFKARTE"

// this is used to give this version an open advanced functionality without a license-key
// the sAppName property must match this constant
#define AX_SPECIAL_VERSION       "HOFKARTE"
#endif



#ifdef COMPILE_SPECIAL_LABISTRACK
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// LABISTRACK Version

// this is the 7/8 digit of the UUID, MUST be 2 digits !!!
// by now it is possible to generate only 100 unique versions
// just select a different 2 digit number which has not been used before, if
// you want to generate a new version   (hint - use 09, not 9, if below 10)
#define ODL_ID_VERSION( n )       86##n                            

// These are only captions, choose anything you want
#define TL_NAME_MAIN             "AXWingis LABIS-TRACK Control module"
#define CLASS_NAME_MAIN	         "AXWingis LABIS-TRACK Control"
#define SPECIAL_MAIN             "Labis Track"

// The Programm IDs - change only the last part
#define PROG_ID                  "AXWINGIS.AXWingisCtrl.LABISTRACK"
#define PROG_ID_PPG              "AXWINGIS.AXWingisPropPage.LABISTRACK"
#define PROG_ID_PPG_LIC          "AXWINGIS.AXWingisPropPageLicense.LABISTRACK"

// this is used to give this version an open advanced functionality without a license-key
// the sAppName property must match this constant
#define AX_SPECIAL_VERSION       "LABISTRACK"
#endif

#ifdef COMPILE_SPECIAL_HOFKARTE2
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Hofkarte2 Version

// this is the 7/8 digit of the UUID, MUST be 2 digits !!!
// by now it is possible to generate only 100 unique versions
// just select a different 2 digit number which has not been used before, if
// you want to generate a new version   (hint - use 09, not 9, if below 10)
#define ODL_ID_VERSION( n )       87##n                            

// These are only captions, choose anything you want
#define TL_NAME_MAIN             "AXWingis HOFKARTE2 Control module"
#define CLASS_NAME_MAIN	         "AXWingis HOFKARTE2 Control"
#define SPECIAL_MAIN             "Hofkarte"

// The Programm IDs - change only the last part
#define PROG_ID                  "AXWINGIS.AXWingisCtrl.HOFKARTE2"
#define PROG_ID_PPG              "AXWINGIS.AXWingisPropPage.HOFKARTE2"
#define PROG_ID_PPG_LIC          "AXWINGIS.AXWingisPropPageLicense.HOFKARTE2"

// this is used to give this version an open advanced functionality without a license-key
// the sAppName property must match this constant
#define AX_SPECIAL_VERSION       "HOFKARTE"
#endif


#ifdef COMPILE_SPECIAL_ZRWIN2
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ZRWIN2 Version

// this is the 7/8 digit of the UUID, MUST be 2 digits !!!
// by now it is possible to generate only 100 unique versions
// just select a different 2 digit number which has not been used before, if
// you want to generate a new version   (hint - use 09, not 9, if below 10)
#define ODL_ID_VERSION( n )       88##n                            

// These are only captions, choose anything you want
#define TL_NAME_MAIN             "AXWingis ZRWIN2 Control module"
#define CLASS_NAME_MAIN	         "AXWingis ZRWIN2 Control"
#define SPECIAL_MAIN             "ZrWin2"

// The Programm IDs - change only the last part
#define PROG_ID                  "AXWINGIS.AXWingisCtrl.ZRWIN2"
#define PROG_ID_PPG              "AXWINGIS.AXWingisPropPage.ZRWIN2"
#define PROG_ID_PPG_LIC          "AXWINGIS.AXWingisPropPageLicense.ZRWIN2"

// this is used to give this version an open advanced functionality without a license-key
#define AX_SPECIAL_VERSION       "ZRWIN"
#endif

#ifdef COMPILE_SPECIAL_AGRO
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// AGRO Version

// this is the 7/8 digit of the UUID, MUST be 2 digits !!!
// by now it is possible to generate only 100 unique versions
// just select a different 2 digit number which has not been used before, if
// you want to generate a new version   (hint - use 09, not 9, if below 10)
#define ODL_ID_VERSION( n )       89##n                            

// These are only captions, choose anything you want
#define TL_NAME_MAIN             "AXWingis AGRO Control module"
#define CLASS_NAME_MAIN	         "AXWingis AGRO Control"
#define SPECIAL_MAIN             "AGRO"

// The Programm IDs - change only the last part
#define PROG_ID                  "AXWINGIS.AXWingisCtrl.AGRO"
#define PROG_ID_PPG              "AXWINGIS.AXWingisPropPage.AGRO"
#define PROG_ID_PPG_LIC          "AXWINGIS.AXWingisPropPageLicense.AGRO"

// this is used to give this version an open advanced functionality without a license-key
// the sAppName property must match this constant
#define AX_SPECIAL_VERSION       "AGRO"
#endif

#ifdef COMPILE_SPECIAL_HOFKARTE4
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Hofkarte3 Version

// this is the 7/8 digit of the UUID, MUST be 2 digits !!!
// by now it is possible to generate only 100 unique versions
// just select a different 2 digit number which has not been used before, if
// you want to generate a new version   (hint - use 09, not 9, if below 10)
#define ODL_ID_VERSION( n )       90##n                            

// These are only captions, choose anything you want
#define TL_NAME_MAIN             "AXWingis HOFKARTE4 Control module"
#define CLASS_NAME_MAIN	         "AXWingis HOFKARTE4 Control"
#define SPECIAL_MAIN             "Hofkarte"

// The Programm IDs - change only the last part
#define PROG_ID                  "AXWINGIS.AXWingisCtrl.HOFKARTE4"
#define PROG_ID_PPG              "AXWINGIS.AXWingisPropPage.HOFKARTE4"
#define PROG_ID_PPG_LIC          "AXWINGIS.AXWingisPropPageLicense.HOFKARTE4"

// this is used to give this version an open advanced functionality without a license-key
// the sAppName property must match this constant
#define AX_SPECIAL_VERSION       "HOFKARTE"
#endif

// ************************** END OF ROAD :) ****************************************************************
// **********************************************************************************************************
// please do not touch anything below this line
// to generate a new version it is sufficient to change the defines above

// defines used by the MIDL compiler
#define ODL_UUID_TLID_MAIN(n)           6A65E##n-23AE-11D1-BD93-444553540000
#define ODL_IID_DAXWingis_MAIN(n)       6A65E##n-23AE-11D1-BD93-444553540000
#define ODL_IID_DAXWingisEvents_MAIN(n) 6A65E##n-23AE-11D1-BD93-444553540000
#define ODL_UUID_CLASSID_MAIN(n)        6A65E##n-23AE-11D1-BD93-444553540000


// defines used by the C++ compiler
#define GETHEX( n ) 0x##n

#define CPP_UUID_TLID_MAIN(n)           { 0x6a65e##n, 0x23ae, 0x11d1, { 0xbd, 0x93, 0x44, 0x45, 0x53, 0x54, 0, 0 } }
#define CPP_UUID_CLASSID_MAIN(n)          IMPLEMENT_OLECREATE_EX(CAXWingisCtrl, PROG_ID, \
                                          0x6a65e000 + GETHEX(n) , 0x23ae, 0x11d1, 0xbd, 0x93, 0x44, 0x45, 0x53, 0x54, 0, 0)
#define CPP_IID_DAXWingis_MAIN(n)       { 0x6a65e##n, 0x23ae, 0x11d1, { 0xbd, 0x93, 0x44, 0x45, 0x53, 0x54, 0, 0 } }
#define CPP_IID_DAXWingisEvents_MAIN(n) { 0x6a65e##n, 0x23ae, 0x11d1, { 0xbd, 0x93, 0x44, 0x45, 0x53, 0x54, 0, 0 } }


#define CPP_CLASSID_PROPPAGE_MAIN(n)      IMPLEMENT_OLECREATE_EX(CAXWingisPropPage, PROG_ID_PPG, \
	                                        0x6a65e000 + GETHEX(n), 0x23ae, 0x11d1, 0xbd, 0x93, 0x44, 0x45, 0x53, 0x54, 0, 0)
                                          
#define CPP_CLASSID_PROPPAGE_LIC_MAIN(n)  IMPLEMENT_OLECREATE_EX(CAXWingisPropPageLicense, PROG_ID_PPG_LIC, \
	                                        0x6a65e000 + GETHEX(n), 0x23ae, 0x11d1, 0xbd, 0x93, 0x44, 0x45, 0x53, 0x54, 0, 0)

// referenced in axwingis.odl
#define ODL_UUID_TLID            ODL_UUID_TLID_MAIN( ODL_ID_VERSION(0) )
#define ODL_IID_DAXWingis        ODL_IID_DAXWingis_MAIN( ODL_ID_VERSION(1) )
#define ODL_IID_DAXWingisEvents  ODL_IID_DAXWingisEvents_MAIN( ODL_ID_VERSION(2) )
#define ODL_UUID_CLASSID         ODL_UUID_CLASSID_MAIN( ODL_ID_VERSION(3) )

// referenced in axwingisctl.cpp
#define CPP_UUID_TLID            CPP_UUID_TLID_MAIN( ODL_ID_VERSION(0) )
#define CPP_IID_DAXWingis        CPP_IID_DAXWingis_MAIN( ODL_ID_VERSION(1) )
#define CPP_IID_DAXWingisEvents  CPP_IID_DAXWingisEvents_MAIN( ODL_ID_VERSION(2) )
#define CPP_UUID_CLASSID         CPP_UUID_CLASSID_MAIN( ODL_ID_VERSION(3) )


#define CPP_CLASSID_PROPPAGE     CPP_CLASSID_PROPPAGE_MAIN( ODL_ID_VERSION(4) )
#define CPP_CLASSID_PROPPAGE_LIC CPP_CLASSID_PROPPAGE_LIC_MAIN( ODL_ID_VERSION(5) )


// referenced in axwingis.odl
#define TL_NAME                  TL_NAME_MAIN
#define CLASS_NAME               CLASS_NAME_MAIN
#define ODL_SPECIAL              SPECIAL_MAIN
#define AXWINGIS_ABOUT           AXWINGIS_VERSION

// referenced in axwingis.rc
#define ODL_IDS_AXWINGIS                 CLASS_NAME
#define ODL_IDS_AXWINGIS_FILEDESCRIPTION TL_NAME
