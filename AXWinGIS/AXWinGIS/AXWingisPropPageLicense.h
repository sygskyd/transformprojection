#if !defined(AFX_AXWINGISPROPPAGELICENSE_H__7C4283C8_9E6D_11D2_A5B3_4854E8298FC6__INCLUDED_)
#define AFX_AXWINGISPROPPAGELICENSE_H__7C4283C8_9E6D_11D2_A5B3_4854E8298FC6__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// AXWingisPropPageLicense.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAXWingisPropPageLicense : Property page dialog

class CAXWingisPropPageLicense : public COlePropertyPage
{
	DECLARE_DYNCREATE(CAXWingisPropPageLicense)
	DECLARE_OLECREATE_EX(CAXWingisPropPageLicense)

// Constructors
public:
	CAXWingisPropPageLicense();

// Dialog Data
	//{{AFX_DATA(CAXWingisPropPageLicense)
	enum { IDD = IDD_PROPPAGE_AXWINGIS_LICENSE };
	CListCtrl m_listview;
	CString	m_sFingerprint;
	CString	m_licmsg;
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);        // DDX/DDV support
    void UpdateListView();

// Message maps
protected:
	//{{AFX_MSG(CAXWingisPropPageLicense)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonAddLkey();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AXWINGISPROPPAGELICENSE_H__7C4283C8_9E6D_11D2_A5B3_4854E8298FC6__INCLUDED_)
