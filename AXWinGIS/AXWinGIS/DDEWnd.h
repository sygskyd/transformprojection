// DDEWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DDEWnd frame

class CDDEWnd : public CFrameWnd
{
	//DECLARE_DYNCREATE(DDEWnd)
protected:
	//CDDEWnd();           // protected constructor used by dynamic creation

// Attributes
public:
   CAXWingisCtrl *m_pParent;

   BOOL m_active;
   HWND WinGISWnd;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DDEWnd)
	//}}AFX_VIRTUAL

// Implementation
  
protected:
	//virtual ~CDDEWnd();

	// Generated message map functions
	//{{AFX_MSG(DDEWnd)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG


   BOOL Init(CAXWingisCtrl *pParent); 
   BOOL SetActive(BOOL Value);

   LRESULT OnDDEInitiate(WPARAM wParam, LPARAM lParam);
   LRESULT OnDDETerminate(WPARAM wParam, LPARAM lParam);
   LRESULT OnDDEAck(WPARAM wParam, LPARAM lParam);
   LRESULT OnDDEExecute(WPARAM wParam, LPARAM lParam);
   LRESULT SendDDECommand(const char *pszCmd);
	DECLARE_MESSAGE_MAP()
};

