// stdafx.h : include file for standard system include files,
//      or project specific include files that are used frequently,
//      but are changed infrequently

//#define OLE2ANSI
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxctl.h>         // MFC support for OLE Controls
#include <afxcmn.h>         // MFC support for common controls
//#include <afxtempl.h>
#include <afxdisp.h> // CG: added by ActiveX Control Containment component

#include <afxsock.h>

// Delete the two includes below if you do not wish to use the MFC
//  database classes
#ifndef _UNICODE
//#include <afxdb.h>			// MFC database classes
//#include <afxdao.h>			// MFC DAO database classes
#endif //_UNICODE
