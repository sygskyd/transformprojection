// AXWingis.cpp : Implementation of CAXWingisApp and DLL registration.

#include "stdafx.h"
#include "AXWingis.h"

#include "interface_version.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CAXWingisApp NEAR theApp;

const GUID CDECL BASED_CODE _tlid =
//		{ 0x6a65e780, 0x23ae, 0x11d1, { 0xbd, 0x93, 0x44, 0x45, 0x53, 0x54, 0, 0 } };
      CPP_UUID_TLID;
// #CLSID_CHANGE#



const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;


////////////////////////////////////////////////////////////////////////////
// CAXWingisApp::InitInstance - DLL initialization

BOOL CAXWingisApp::InitInstance()
{
	// CG: This line was added by the ActiveX Control Containment component
	AfxEnableControlContainer();

	BOOL bInit = COleControlModule::InitInstance();

	if (bInit)
	{
		// TODO: Add your own module initialization code here.
	}

	return bInit;
}


////////////////////////////////////////////////////////////////////////////
// CAXWingisApp::ExitInstance - DLL termination

int CAXWingisApp::ExitInstance()
{
	// TODO: Add your own module termination code here.

	return COleControlModule::ExitInstance();
}


/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}


/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}


void CAXWingisApp::WinHelp( DWORD dwData, UINT nCmd)
{
	// TODO: Add your message handler code here and/or call default
	AfxMessageBox("WINHELP",32);
#ifdef _DEBUGWIN_ON
   Debug.Print("WinHELP");
#endif
	 COleControlModule::WinHelp(dwData, nCmd );
}
