# Microsoft Developer Studio Generated NMAKE File, Format Version 4.20
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

!IF "$(CFG)" == ""
CFG=AXWingis - Win32 Debug
!MESSAGE No configuration specified.  Defaulting to AXWingis - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "AXWingis - Win32 Release" && "$(CFG)" !=\
 "AXWingis - Win32 Debug" && "$(CFG)" != "AXWingis - Win32 Unicode Debug" &&\
 "$(CFG)" != "AXWingis - Win32 Unicode Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE on this makefile
!MESSAGE by defining the macro CFG on the command line.  For example:
!MESSAGE 
!MESSAGE NMAKE /f "AXWingis.mak" CFG="AXWingis - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "AXWingis - Win32 Release" (based on\
 "Win32 (x86) Dynamic-Link Library")
!MESSAGE "AXWingis - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "AXWingis - Win32 Unicode Debug" (based on\
 "Win32 (x86) Dynamic-Link Library")
!MESSAGE "AXWingis - Win32 Unicode Release" (based on\
 "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 
################################################################################
# Begin Project
# PROP Target_Last_Scanned "AXWingis - Win32 Debug"
MTL=mktyplib.exe
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "AXWingis - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP BASE Target_Ext "ocx"
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# PROP Target_Ext "ocx"
OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\AXWingis.ocx" "$(OUTDIR)\regsvr32.trg"

CLEAN : 
	-@erase "$(INTDIR)\AXWingis.obj"
	-@erase "$(INTDIR)\AXWingis.pch"
	-@erase "$(INTDIR)\AXWingis.res"
	-@erase "$(INTDIR)\AXWingis.tlb"
	-@erase "$(INTDIR)\AXWingisCtl.obj"
	-@erase "$(INTDIR)\AXWingisPpg.obj"
	-@erase "$(INTDIR)\DDEWnd.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(OUTDIR)\AXWingis.exp"
	-@erase "$(OUTDIR)\AXWingis.lib"
	-@erase "$(OUTDIR)\AXWingis.ocx"
	-@erase "$(OUTDIR)\regsvr32.trg"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /c
# ADD CPP /nologo /MD /W3 /GR /GX /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_USRDLL" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /c
CPP_PROJ=/nologo /MD /W3 /GR /GX /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D\
 "_USRDLL" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /Fp"$(INTDIR)/AXWingis.pch"\
 /Yu"stdafx.h" /Fo"$(INTDIR)/" /c 
CPP_OBJS=.\Release/
CPP_SBRS=.\.
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /win32
MTL_PROJ=/nologo /D "NDEBUG" /win32 
# ADD BASE RSC /l 0xc07 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0xc07 /d "NDEBUG" /d "_AFXDLL"
RSC_PROJ=/l 0xc07 /fo"$(INTDIR)/AXWingis.res" /d "NDEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/AXWingis.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 /nologo /subsystem:windows /dll /machine:I386
# SUBTRACT LINK32 /nodefaultlib
LINK32_FLAGS=/nologo /subsystem:windows /dll /incremental:no\
 /pdb:"$(OUTDIR)/AXWingis.pdb" /machine:I386 /def:".\AXWingis.def"\
 /out:"$(OUTDIR)/AXWingis.ocx" /implib:"$(OUTDIR)/AXWingis.lib" 
DEF_FILE= \
	".\AXWingis.def"
LINK32_OBJS= \
	"$(INTDIR)\AXWingis.obj" \
	"$(INTDIR)\AXWingis.res" \
	"$(INTDIR)\AXWingisCtl.obj" \
	"$(INTDIR)\AXWingisPpg.obj" \
	"$(INTDIR)\DDEWnd.obj" \
	"$(INTDIR)\StdAfx.obj"

"$(OUTDIR)\AXWingis.ocx" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

# Begin Custom Build - Registering OLE control...
OutDir=.\Release
TargetPath=.\Release\AXWingis.ocx
InputPath=.\Release\AXWingis.ocx
SOURCE=$(InputPath)

"$(OutDir)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   regsvr32 /s /c "$(TargetPath)"
   echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg"

# End Custom Build

!ELSEIF  "$(CFG)" == "AXWingis - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP BASE Target_Ext "ocx"
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# PROP Target_Ext "ocx"
OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\AXWingis.ocx" "$(OUTDIR)\regsvr32.trg"

CLEAN : 
	-@erase "$(INTDIR)\AXWingis.obj"
	-@erase "$(INTDIR)\AXWingis.pch"
	-@erase "$(INTDIR)\AXWingis.res"
	-@erase "$(INTDIR)\AXWingis.tlb"
	-@erase "$(INTDIR)\AXWingisCtl.obj"
	-@erase "$(INTDIR)\AXWingisPpg.obj"
	-@erase "$(INTDIR)\DDEWnd.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\vc40.idb"
	-@erase "$(INTDIR)\vc40.pdb"
	-@erase "$(OUTDIR)\AXWingis.exp"
	-@erase "$(OUTDIR)\AXWingis.ilk"
	-@erase "$(OUTDIR)\AXWingis.lib"
	-@erase "$(OUTDIR)\AXWingis.ocx"
	-@erase "$(OUTDIR)\AXWingis.pdb"
	-@erase "$(OUTDIR)\regsvr32.trg"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /Yu"stdafx.h" /c
CPP_PROJ=/nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS"\
 /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /Fp"$(INTDIR)/AXWingis.pch"\
 /Yu"stdafx.h" /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.\.
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /win32
MTL_PROJ=/nologo /D "_DEBUG" /win32 
# ADD BASE RSC /l 0xc07 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0xc07 /d "_DEBUG" /d "_AFXDLL"
RSC_PROJ=/l 0xc07 /fo"$(INTDIR)/AXWingis.res" /d "_DEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/AXWingis.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /debug /machine:I386
# ADD LINK32 /nologo /subsystem:windows /dll /debug /machine:I386
LINK32_FLAGS=/nologo /subsystem:windows /dll /incremental:yes\
 /pdb:"$(OUTDIR)/AXWingis.pdb" /debug /machine:I386 /def:".\AXWingis.def"\
 /out:"$(OUTDIR)/AXWingis.ocx" /implib:"$(OUTDIR)/AXWingis.lib" 
DEF_FILE= \
	".\AXWingis.def"
LINK32_OBJS= \
	"$(INTDIR)\AXWingis.obj" \
	"$(INTDIR)\AXWingis.res" \
	"$(INTDIR)\AXWingisCtl.obj" \
	"$(INTDIR)\AXWingisPpg.obj" \
	"$(INTDIR)\DDEWnd.obj" \
	"$(INTDIR)\StdAfx.obj"

"$(OUTDIR)\AXWingis.ocx" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

# Begin Custom Build - Registering OLE control...
OutDir=.\Debug
TargetPath=.\Debug\AXWingis.ocx
InputPath=.\Debug\AXWingis.ocx
SOURCE=$(InputPath)

"$(OutDir)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   regsvr32 /s /c "$(TargetPath)"
   echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg"

# End Custom Build

!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "DebugU"
# PROP BASE Intermediate_Dir "DebugU"
# PROP BASE Target_Dir ""
# PROP BASE Target_Ext "ocx"
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "DebugU"
# PROP Intermediate_Dir "DebugU"
# PROP Target_Dir ""
# PROP Target_Ext "ocx"
OUTDIR=.\DebugU
INTDIR=.\DebugU
# Begin Custom Macros
OutDir=.\DebugU
# End Custom Macros

ALL : "$(OUTDIR)\AXWingis.ocx" "$(OUTDIR)\regsvr32.trg"

CLEAN : 
	-@erase "$(INTDIR)\AXWingis.obj"
	-@erase "$(INTDIR)\AXWingis.pch"
	-@erase "$(INTDIR)\AXWingis.res"
	-@erase "$(INTDIR)\AXWingis.tlb"
	-@erase "$(INTDIR)\AXWingisCtl.obj"
	-@erase "$(INTDIR)\AXWingisPpg.obj"
	-@erase "$(INTDIR)\DDEWnd.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\vc40.idb"
	-@erase "$(INTDIR)\vc40.pdb"
	-@erase "$(OUTDIR)\AXWingis.exp"
	-@erase "$(OUTDIR)\AXWingis.ilk"
	-@erase "$(OUTDIR)\AXWingis.lib"
	-@erase "$(OUTDIR)\AXWingis.ocx"
	-@erase "$(OUTDIR)\AXWingis.pdb"
	-@erase "$(OUTDIR)\regsvr32.trg"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /Yu"stdafx.h" /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_USRDLL" /D "_UNICODE" /Yu"stdafx.h" /c
CPP_PROJ=/nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS"\
 /D "_WINDLL" /D "_AFXDLL" /D "_USRDLL" /D "_UNICODE"\
 /Fp"$(INTDIR)/AXWingis.pch" /Yu"stdafx.h" /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c 
CPP_OBJS=.\DebugU/
CPP_SBRS=.\.
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /win32
MTL_PROJ=/nologo /D "_DEBUG" /win32 
# ADD BASE RSC /l 0xc07 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0xc07 /d "_DEBUG" /d "_AFXDLL"
RSC_PROJ=/l 0xc07 /fo"$(INTDIR)/AXWingis.res" /d "_DEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/AXWingis.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /debug /machine:I386
# ADD LINK32 /nologo /subsystem:windows /dll /debug /machine:I386
LINK32_FLAGS=/nologo /subsystem:windows /dll /incremental:yes\
 /pdb:"$(OUTDIR)/AXWingis.pdb" /debug /machine:I386 /def:".\AXWingis.def"\
 /out:"$(OUTDIR)/AXWingis.ocx" /implib:"$(OUTDIR)/AXWingis.lib" 
DEF_FILE= \
	".\AXWingis.def"
LINK32_OBJS= \
	"$(INTDIR)\AXWingis.obj" \
	"$(INTDIR)\AXWingis.res" \
	"$(INTDIR)\AXWingisCtl.obj" \
	"$(INTDIR)\AXWingisPpg.obj" \
	"$(INTDIR)\DDEWnd.obj" \
	"$(INTDIR)\StdAfx.obj"

"$(OUTDIR)\AXWingis.ocx" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

# Begin Custom Build - Registering OLE control...
OutDir=.\DebugU
TargetPath=.\DebugU\AXWingis.ocx
InputPath=.\DebugU\AXWingis.ocx
SOURCE=$(InputPath)

"$(OutDir)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   regsvr32 /s /c "$(TargetPath)"
   echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg"

# End Custom Build

!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "ReleaseU"
# PROP BASE Intermediate_Dir "ReleaseU"
# PROP BASE Target_Dir ""
# PROP BASE Target_Ext "ocx"
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "ReleaseU"
# PROP Intermediate_Dir "ReleaseU"
# PROP Target_Dir ""
# PROP Target_Ext "ocx"
OUTDIR=.\ReleaseU
INTDIR=.\ReleaseU
# Begin Custom Macros
OutDir=.\ReleaseU
# End Custom Macros

ALL : "$(OUTDIR)\AXWingis.ocx" "$(OUTDIR)\regsvr32.trg"

CLEAN : 
	-@erase "$(INTDIR)\AXWingis.obj"
	-@erase "$(INTDIR)\AXWingis.pch"
	-@erase "$(INTDIR)\AXWingis.res"
	-@erase "$(INTDIR)\AXWingis.tlb"
	-@erase "$(INTDIR)\AXWingisCtl.obj"
	-@erase "$(INTDIR)\AXWingisPpg.obj"
	-@erase "$(INTDIR)\DDEWnd.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(OUTDIR)\AXWingis.exp"
	-@erase "$(OUTDIR)\AXWingis.lib"
	-@erase "$(OUTDIR)\AXWingis.ocx"
	-@erase "$(OUTDIR)\regsvr32.trg"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /Yu"stdafx.h" /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_USRDLL" /D "_UNICODE" /Yu"stdafx.h" /c
CPP_PROJ=/nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D\
 "_WINDLL" /D "_AFXDLL" /D "_USRDLL" /D "_UNICODE" /Fp"$(INTDIR)/AXWingis.pch"\
 /Yu"stdafx.h" /Fo"$(INTDIR)/" /c 
CPP_OBJS=.\ReleaseU/
CPP_SBRS=.\.
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /win32
MTL_PROJ=/nologo /D "NDEBUG" /win32 
# ADD BASE RSC /l 0xc07 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0xc07 /d "NDEBUG" /d "_AFXDLL"
RSC_PROJ=/l 0xc07 /fo"$(INTDIR)/AXWingis.res" /d "NDEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/AXWingis.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 /nologo /subsystem:windows /dll /machine:I386
LINK32_FLAGS=/nologo /subsystem:windows /dll /incremental:no\
 /pdb:"$(OUTDIR)/AXWingis.pdb" /machine:I386 /def:".\AXWingis.def"\
 /out:"$(OUTDIR)/AXWingis.ocx" /implib:"$(OUTDIR)/AXWingis.lib" 
DEF_FILE= \
	".\AXWingis.def"
LINK32_OBJS= \
	"$(INTDIR)\AXWingis.obj" \
	"$(INTDIR)\AXWingis.res" \
	"$(INTDIR)\AXWingisCtl.obj" \
	"$(INTDIR)\AXWingisPpg.obj" \
	"$(INTDIR)\DDEWnd.obj" \
	"$(INTDIR)\StdAfx.obj"

"$(OUTDIR)\AXWingis.ocx" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

# Begin Custom Build - Registering OLE control...
OutDir=.\ReleaseU
TargetPath=.\ReleaseU\AXWingis.ocx
InputPath=.\ReleaseU\AXWingis.ocx
SOURCE=$(InputPath)

"$(OutDir)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   regsvr32 /s /c "$(TargetPath)"
   echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg"

# End Custom Build

!ENDIF 

.c{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.c{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

################################################################################
# Begin Target

# Name "AXWingis - Win32 Release"
# Name "AXWingis - Win32 Debug"
# Name "AXWingis - Win32 Unicode Debug"
# Name "AXWingis - Win32 Unicode Release"

!IF  "$(CFG)" == "AXWingis - Win32 Release"

!ELSEIF  "$(CFG)" == "AXWingis - Win32 Debug"

!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Debug"

!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Release"

!ENDIF 

################################################################################
# Begin Source File

SOURCE=.\ReadMe.txt

!IF  "$(CFG)" == "AXWingis - Win32 Release"

!ELSEIF  "$(CFG)" == "AXWingis - Win32 Debug"

!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Debug"

!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Release"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\StdAfx.cpp
DEP_CPP_STDAF=\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "AXWingis - Win32 Release"

# ADD CPP /Yc"stdafx.h"

BuildCmds= \
	$(CPP) /nologo /MD /W3 /GR /GX /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D\
 "_USRDLL" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /Fp"$(INTDIR)/AXWingis.pch"\
 /Yc"stdafx.h" /Fo"$(INTDIR)/" /c $(SOURCE) \
	

"$(INTDIR)\StdAfx.obj" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\AXWingis.pch" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "AXWingis - Win32 Debug"

# ADD CPP /Yc"stdafx.h"

BuildCmds= \
	$(CPP) /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS"\
 /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /Fp"$(INTDIR)/AXWingis.pch"\
 /Yc"stdafx.h" /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c $(SOURCE) \
	

"$(INTDIR)\StdAfx.obj" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\AXWingis.pch" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Debug"

# ADD BASE CPP /Yc"stdafx.h"
# ADD CPP /Yc"stdafx.h"

BuildCmds= \
	$(CPP) /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS"\
 /D "_WINDLL" /D "_AFXDLL" /D "_USRDLL" /D "_UNICODE"\
 /Fp"$(INTDIR)/AXWingis.pch" /Yc"stdafx.h" /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c\
 $(SOURCE) \
	

"$(INTDIR)\StdAfx.obj" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\AXWingis.pch" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Release"

# ADD BASE CPP /Yc"stdafx.h"
# ADD CPP /Yc"stdafx.h"

BuildCmds= \
	$(CPP) /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D\
 "_WINDLL" /D "_AFXDLL" /D "_USRDLL" /D "_UNICODE" /Fp"$(INTDIR)/AXWingis.pch"\
 /Yc"stdafx.h" /Fo"$(INTDIR)/" /c $(SOURCE) \
	

"$(INTDIR)\StdAfx.obj" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\AXWingis.pch" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\AXWingis.cpp
DEP_CPP_AXWIN=\
	".\AXWingis.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "AXWingis - Win32 Release"


"$(INTDIR)\AXWingis.obj" : $(SOURCE) $(DEP_CPP_AXWIN) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.pch"


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Debug"


"$(INTDIR)\AXWingis.obj" : $(SOURCE) $(DEP_CPP_AXWIN) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.pch"


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Debug"


"$(INTDIR)\AXWingis.obj" : $(SOURCE) $(DEP_CPP_AXWIN) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.pch"


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Release"


"$(INTDIR)\AXWingis.obj" : $(SOURCE) $(DEP_CPP_AXWIN) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\AXWingis.def

!IF  "$(CFG)" == "AXWingis - Win32 Release"

!ELSEIF  "$(CFG)" == "AXWingis - Win32 Debug"

!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Debug"

!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Release"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\AXWingis.rc

!IF  "$(CFG)" == "AXWingis - Win32 Release"

DEP_RSC_AXWING=\
	".\AXWingis.ico"\
	".\AXWingisCtl.bmp"\
	".\bitmap1.bmp"\
	".\bitmap2.bmp"\
	".\bmp00001.bmp"\
	".\Icon1.ico"\
	".\Mask.bmp"\
	".\Release\AXWingis.tlb"\
	".\XLink1.bmp"\
	

"$(INTDIR)\AXWingis.res" : $(SOURCE) $(DEP_RSC_AXWING) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.tlb"
   $(RSC) /l 0xc07 /fo"$(INTDIR)/AXWingis.res" /i "Release" /d "NDEBUG" /d\
 "_AFXDLL" $(SOURCE)


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Debug"

DEP_RSC_AXWING=\
	".\AXWingis.ico"\
	".\AXWingisCtl.bmp"\
	".\bitmap1.bmp"\
	".\bitmap2.bmp"\
	".\bmp00001.bmp"\
	".\Debug\AXWingis.tlb"\
	".\Icon1.ico"\
	".\Mask.bmp"\
	".\XLink1.bmp"\
	

"$(INTDIR)\AXWingis.res" : $(SOURCE) $(DEP_RSC_AXWING) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.tlb"
   $(RSC) /l 0xc07 /fo"$(INTDIR)/AXWingis.res" /i "Debug" /d "_DEBUG" /d\
 "_AFXDLL" $(SOURCE)


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Debug"

DEP_RSC_AXWING=\
	".\AXWingis.ico"\
	".\AXWingisCtl.bmp"\
	".\bitmap1.bmp"\
	".\bitmap2.bmp"\
	".\bmp00001.bmp"\
	".\Icon1.ico"\
	".\Mask.bmp"\
	".\XLink1.bmp"\
	
NODEP_RSC_AXWING=\
	".\DebugU\AXWingis.tlb"\
	

"$(INTDIR)\AXWingis.res" : $(SOURCE) $(DEP_RSC_AXWING) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.tlb"
   $(RSC) /l 0xc07 /fo"$(INTDIR)/AXWingis.res" /i "DebugU" /d "_DEBUG" /d\
 "_AFXDLL" $(SOURCE)


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Release"

DEP_RSC_AXWING=\
	".\AXWingis.ico"\
	".\AXWingisCtl.bmp"\
	".\bitmap1.bmp"\
	".\bitmap2.bmp"\
	".\bmp00001.bmp"\
	".\Icon1.ico"\
	".\Mask.bmp"\
	".\XLink1.bmp"\
	
NODEP_RSC_AXWING=\
	".\ReleaseU\AXWingis.tlb"\
	

"$(INTDIR)\AXWingis.res" : $(SOURCE) $(DEP_RSC_AXWING) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.tlb"
   $(RSC) /l 0xc07 /fo"$(INTDIR)/AXWingis.res" /i "ReleaseU" /d "NDEBUG" /d\
 "_AFXDLL" $(SOURCE)


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\AXWingis.odl

!IF  "$(CFG)" == "AXWingis - Win32 Release"


"$(OUTDIR)\AXWingis.tlb" : $(SOURCE) "$(OUTDIR)"
   $(MTL) /nologo /D "NDEBUG" /tlb "$(OUTDIR)/AXWingis.tlb" /win32 $(SOURCE)


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Debug"


"$(OUTDIR)\AXWingis.tlb" : $(SOURCE) "$(OUTDIR)"
   $(MTL) /nologo /D "_DEBUG" /tlb "$(OUTDIR)/AXWingis.tlb" /win32 $(SOURCE)


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Debug"


"$(OUTDIR)\AXWingis.tlb" : $(SOURCE) "$(OUTDIR)"
   $(MTL) /nologo /D "_DEBUG" /tlb "$(OUTDIR)/AXWingis.tlb" /win32 $(SOURCE)


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Release"


"$(OUTDIR)\AXWingis.tlb" : $(SOURCE) "$(OUTDIR)"
   $(MTL) /nologo /D "NDEBUG" /tlb "$(OUTDIR)/AXWingis.tlb" /win32 $(SOURCE)


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\AXWingisCtl.cpp
DEP_CPP_AXWINGI=\
	".\AXWingis.h"\
	".\AXWingisCtl.h"\
	".\AXWingisPpg.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "AXWingis - Win32 Release"


"$(INTDIR)\AXWingisCtl.obj" : $(SOURCE) $(DEP_CPP_AXWINGI) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.pch"


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Debug"


"$(INTDIR)\AXWingisCtl.obj" : $(SOURCE) $(DEP_CPP_AXWINGI) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.pch"


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Debug"


"$(INTDIR)\AXWingisCtl.obj" : $(SOURCE) $(DEP_CPP_AXWINGI) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.pch"


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Release"


"$(INTDIR)\AXWingisCtl.obj" : $(SOURCE) $(DEP_CPP_AXWINGI) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\AXWingisPpg.cpp
DEP_CPP_AXWINGIS=\
	".\AXWingis.h"\
	".\AXWingisPpg.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "AXWingis - Win32 Release"


"$(INTDIR)\AXWingisPpg.obj" : $(SOURCE) $(DEP_CPP_AXWINGIS) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.pch"


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Debug"


"$(INTDIR)\AXWingisPpg.obj" : $(SOURCE) $(DEP_CPP_AXWINGIS) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.pch"


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Debug"


"$(INTDIR)\AXWingisPpg.obj" : $(SOURCE) $(DEP_CPP_AXWINGIS) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.pch"


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Release"


"$(INTDIR)\AXWingisPpg.obj" : $(SOURCE) $(DEP_CPP_AXWINGIS) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\DDEWnd.cpp
DEP_CPP_DDEWN=\
	".\AXWingis.h"\
	".\AXWingisCtl.h"\
	".\StdAfx.h"\
	

!IF  "$(CFG)" == "AXWingis - Win32 Release"


"$(INTDIR)\DDEWnd.obj" : $(SOURCE) $(DEP_CPP_DDEWN) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.pch"


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Debug"


"$(INTDIR)\DDEWnd.obj" : $(SOURCE) $(DEP_CPP_DDEWN) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.pch"


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Debug"


"$(INTDIR)\DDEWnd.obj" : $(SOURCE) $(DEP_CPP_DDEWN) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.pch"


!ELSEIF  "$(CFG)" == "AXWingis - Win32 Unicode Release"


"$(INTDIR)\DDEWnd.obj" : $(SOURCE) $(DEP_CPP_DDEWN) "$(INTDIR)"\
 "$(INTDIR)\AXWingis.pch"


!ENDIF 

# End Source File
# End Target
# End Project
################################################################################
