#if !defined(AFX_ADDLKEYDLG_H__F213B504_B3D2_11D2_A5CF_4854E8298FC6__INCLUDED_)
#define AFX_ADDLKEYDLG_H__F213B504_B3D2_11D2_A5CF_4854E8298FC6__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// AddLKeyDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAddLKeyDlg dialog

class CAddLKeyDlg : public CDialog
{
// Construction
public:
	CAddLKeyDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAddLKeyDlg)
	enum { IDD = IDD_ADD_LICENSE };
	CString	m_LKey;
	CString	m_captionFP;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddLKeyDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAddLKeyDlg)
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDLKEYDLG_H__F213B504_B3D2_11D2_A5CF_4854E8298FC6__INCLUDED_)
