// AXWingisPropPageLicense.cpp : implementation file
//

#include "stdafx.h"
#include "AXWingis.h"
#include "AXWingisPropPageLicense.h"
#include "AddLKeyDlg.h"
#include "eMedes_C_tools.h"

#include "interface_version.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAXWingisPropPageLicense dialog

IMPLEMENT_DYNCREATE(CAXWingisPropPageLicense, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CAXWingisPropPageLicense, COlePropertyPage)
	//{{AFX_MSG_MAP(CAXWingisPropPageLicense)
	ON_BN_CLICKED(IDC_BUTTON_ADD_LKEY, OnButtonAddLkey)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

// {7C4283C7-9E6D-11D2-A5B3-4854E8298FC6}
//IMPLEMENT_OLECREATE_EX(CAXWingisPropPageLicense, "AXWingis.CAXWingisPropPageLicense",
//	0x7c4283c7, 0x9e6d, 0x11d2, 0xa5, 0xb3, 0x48, 0x54, 0xe8, 0x29, 0x8f, 0xc6)
CPP_CLASSID_PROPPAGE_LIC


/////////////////////////////////////////////////////////////////////////////
// CAXWingisPropPageLicense::CAXWingisPropPageLicenseFactory::UpdateRegistry -
// Adds or removes system registry entries for CAXWingisPropPageLicense

BOOL CAXWingisPropPageLicense::CAXWingisPropPageLicenseFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Define string resource for page type; replace '0' below with ID.

	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_AXWINGIS_PPG_LIC);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CAXWingisPropPageLicense::CAXWingisPropPageLicense - Constructor

// TODO: Define string resource for page caption; replace '0' below with ID.

CAXWingisPropPageLicense::CAXWingisPropPageLicense() :
    COlePropertyPage(IDD, IDS_AXWINGIS_PPG_LIC_CAPTION)
{
	//{{AFX_DATA_INIT(CAXWingisPropPageLicense)
	m_sFingerprint = _T("");
	m_licmsg = _T("");
	//}}AFX_DATA_INIT

    //m_licmsg = GetRegValue("Software\\Progis\\AXWingis", "LicMsg");
    //m_listview.SubClassDlgItem(IDC_LISTVIEW, this);
   
    //m_listview.InsertColumn( 1, "License Key", LVCFMT_LEFT, 50, 1);



}


/////////////////////////////////////////////////////////////////////////////
// CAXWingisPropPageLicense::DoDataExchange - Moves data between page and properties

void CAXWingisPropPageLicense::DoDataExchange(CDataExchange* pDX)
{
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//{{AFX_DATA_MAP(CAXWingisPropPageLicense)
	DDX_Control(pDX, IDC_LISTVIEW, m_listview);
	DDX_Text(pDX, IDC_FINGERPRINT, m_sFingerprint);
	DDV_MaxChars(pDX, m_sFingerprint, 260);
	DDX_Text(pDX, IDC_LICMSG, m_licmsg);
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CAXWingisPropPageLicense message handlers

BOOL CAXWingisPropPageLicense::OnInitDialog() 
{

	COlePropertyPage::OnInitDialog();
	
    m_listview.InsertColumn( 1, "Application", LVCFMT_LEFT, 100, 1);
    m_listview.InsertColumn( 2, "Functionality", LVCFMT_LEFT, 90, 1);
    m_listview.InsertColumn( 3, "Days", LVCFMT_LEFT, 48, 1);
    m_listview.InsertColumn( 4, "Days left", LVCFMT_LEFT, 56, 1);
    m_listview.InsertColumn( 5, "First used", LVCFMT_LEFT, 70, 1);

    UpdateListView();
    
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAXWingisPropPageLicense::OnButtonAddLkey() 
{
   CAddLKeyDlg addDlg;
   addDlg.DoModal();
   UpdateListView();	
}

void CAXWingisPropPageLicense::UpdateListView()
{
CPtrList cList;
structLicense *pLicense;
SYSTEMTIME systFirstUsed;
CString csLocaleDate;
CString csAppName;
CString csAXMode;
int iItemIndex = 0;


    if (DecodeAllLicenses(m_sFingerprint, cList))
      {
       m_listview.DeleteAllItems();
       while (!cList.IsEmpty())
         {
          pLicense = (structLicense *)cList.RemoveHead();
          if (pLicense != NULL)
            {


             if (pLicense->tFirstUsed > 0)
               {
                CTime ctFirstUsed(pLicense->tFirstUsed);
                systFirstUsed.wYear = ctFirstUsed.GetYear();
                systFirstUsed.wMonth = ctFirstUsed.GetMonth();    
                systFirstUsed.wDayOfWeek = ctFirstUsed.GetDayOfWeek();    
                systFirstUsed.wDay = ctFirstUsed.GetDay();    
                systFirstUsed.wHour = ctFirstUsed.GetHour();
                systFirstUsed.wMinute = ctFirstUsed.GetMinute();    
                systFirstUsed.wSecond = ctFirstUsed.GetSecond();    
                systFirstUsed.wMilliseconds = 0;

             
                GetDateFormat(LOCALE_USER_DEFAULT,
                              DATE_SHORTDATE,
                              &systFirstUsed,
                              NULL,
                              csLocaleDate.GetBuffer(250),
                              250);
                csLocaleDate.ReleaseBuffer(-1);
               }
             else 
               {
                csLocaleDate = "Not jet";
                pLicense->lDaysLeft = pLicense->lDaysLicensed;

               }

             if (pLicense->csAppName[0] == 'D')
               {
                csAppName = "Design mode";
               }
             //else if (pLicense->csAppName[0] == 'R')
             //  csAppName = pLicense->csAppName.Mid(1, pLicense->csAppName.GetLength() - 1);
             else
               csAppName = pLicense->csAppName;
               //csAppName = "Invalid license-type";

             if (pLicense->ulAXMode == AXMODE_STANDARD)
                csAXMode = "Standard";
             else if (pLicense->ulAXMode == AXMODE_ADVANCED)
                csAXMode = "Advanced";
             else
                csAXMode = "Invalid";



             m_listview.InsertItem(iItemIndex, csAppName);
             m_listview.SetItemText(iItemIndex, 1, csAXMode );
		     m_listview.SetItemText(iItemIndex, 2, IntToStr(pLicense->lDaysLicensed));
             m_listview.SetItemText(iItemIndex, 3, IntToStr(pLicense->lDaysLeft));
             m_listview.SetItemText(iItemIndex, 4, csLocaleDate );

          
             /*
             pLicense->csAppName 
             pLicense->lDaysLicensed 
             pLicense->csSerialNr 
             pLicense->tFirstUsed 
             pLicense->tLastUsed 
             pLicense->lDaysLeft
             */

             delete pLicense;
             iItemIndex++;
            }
         }
      }

	UpdateData(FALSE);  // initialize dialog data


}
