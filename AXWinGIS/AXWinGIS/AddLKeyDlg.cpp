// AddLKeyDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AXWingis.h"
#include "AddLKeyDlg.h"

#include "eMedes_C_tools.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAddLKeyDlg dialog


CAddLKeyDlg::CAddLKeyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAddLKeyDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAddLKeyDlg)
	m_LKey = _T("");
	m_captionFP = _T("");
	//}}AFX_DATA_INIT
    ValidateFingerPrint(m_captionFP);
}


void CAddLKeyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAddLKeyDlg)
	DDX_Text(pDX, IDC_LC, m_LKey);
	DDX_Text(pDX, IDC_FP, m_captionFP);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAddLKeyDlg, CDialog)
	//{{AFX_MSG_MAP(CAddLKeyDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAddLKeyDlg message handlers

void CAddLKeyDlg::OnOK() 
{
CString csErrMsg;

    UpdateData(TRUE);
	if (AddLicenseKey(m_LKey, csErrMsg))
	  CDialog::OnOK();
    else
      MessageBox(csErrMsg);
}

void CAddLKeyDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}


