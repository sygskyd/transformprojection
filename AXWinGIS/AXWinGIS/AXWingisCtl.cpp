// AXWingisCtl.cpp : Implementation of the CAXWingisCtrl OLE control class.

#include "stdafx.h"
#include <ddeml.h>
#include <time.h>

#define CPP_VERSION

#include "AXWingis.h"
#include "AXWingisCtl.h"
#include "AXWingisPpg.h"
#include "AXWingisPropPageLicense.h"
#include "AddLKeyDlg.h"

#include "axipc_error.h"


#include "eMedes_C_tools.h"



#include "interface_version.h"


#define CMD_TIMEOUT 5000
#define WG_MAX_CMDSIZE 200

#define WM_CMDAVAILABLE     WM_USER+0x9abc
#define WM_SEND_DELAYED_CMD WM_USER+0x9abd

#define TID_AX 99       // Timer ID for AXWingis.OnTimer


CAXWingisCtrl *pAXWingis;

IMPLEMENT_DYNCREATE(CAXWingisCtrl, COleControl)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CAXWingisCtrl, COleControl)
	//{{AFX_MSG_MAP(CAXWingisCtrl)
	ON_WM_HELPINFO()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP 
	ON_MESSAGE(OCM_COMMAND, OnOcmCommand)
   ON_MESSAGE(WM_CMDAVAILABLE, OnCmdAvailable)
   ON_MESSAGE(WM_SEND_DELAYED_CMD, OnPostedSendToWinGIS)   
	//ON_OLEVERB(AFX_IDS_VERB_EDIT, OnEdit)
   //ON_OLEVERB(IDS_AXWINGIS_HELP, OnEdit)
	//ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
   ON_OLEVERB(IDS_AXWINGIS_PROP, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CAXWingisCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CAXWingisCtrl)
	DISP_PROPERTY_NOTIFY(CAXWingisCtrl, "sServer", m_server, OnServerChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CAXWingisCtrl, "sTopic", m_topic, OnTopicChanged, VT_BSTR)
  DISP_PROPERTY_NOTIFY(CAXWingisCtrl, "bUseSend", m_useSend, OnUseSendChanged, VT_BOOL)
  DISP_PROPERTY_NOTIFY(CAXWingisCtrl, "sLayerName", m_layerName, OnLayerNameChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CAXWingisCtrl, "bUseCOM", m_useCOM, OnUseCOMChanged, VT_BOOL)
  DISP_PROPERTY_EX(CAXWingisCtrl, "bActive", GetActive, SetActive, VT_BOOL)
	DISP_PROPERTY_EX(CAXWingisCtrl, "bConnected", GetConnected, SetConnected, VT_BOOL)
	DISP_PROPERTY_EX(CAXWingisCtrl, "bReady", GetReady, SetNotSupported, VT_BOOL)
	DISP_PROPERTY_EX(CAXWingisCtrl, "lLayerNameList", GetLayerNameList, SetNotSupported, VT_I4)
	DISP_PROPERTY_EX(CAXWingisCtrl, "lLayerIndexList", GetLayerIndexList, SetNotSupported, VT_I4)
	DISP_PROPERTY_EX(CAXWingisCtrl, "sLayerInfo", GetLayerInfo, SetLayerInfo, VT_BSTR)
	DISP_PROPERTY_EX(CAXWingisCtrl, "lLayerIndex", GetLayerIndex, SetLayerIndex, VT_I4)
	DISP_PROPERTY_EX(CAXWingisCtrl, "lViewList", GetViewList, SetNotSupported, VT_I4)
	DISP_PROPERTY_EX(CAXWingisCtrl, "sAppName", GetAppName, SetAppName, VT_BSTR)
	DISP_PROPERTY_EX(CAXWingisCtrl, "sFingerPrint", GetFingerPrint, SetFingerPrint, VT_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DDEExecute", DDEExecute, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "CreateList", CreateList, VT_I4, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "AddStringParam", AddStringParam, VT_BOOL, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "GetStringParam", GetStringParam, VT_BSTR, VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DeleteList", DeleteList, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "IsValidList", IsValidList, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "ClearList", ClearList, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "GetListSize", GetListSize, VT_I4, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSelectObjects", DoSelectObjects, VT_BOOL, VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoCloseGIS", DoCloseGIS, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoShowGIS", DoShowGIS, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoZoomObjects", DoZoomObjects, VT_BOOL, VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoDeselectAll", DoDeselectAll, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetView", DoSetView, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetCaption", DoSetCaption, VT_BOOL, VTS_BSTR VTS_I4 VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoSearchObjects", DoSearchObjects, VT_I4, VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoOpenProject", DoOpenProject, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoCloseProject", DoCloseProject, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetActiveLayer", DoSetActiveLayer, VT_BSTR, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetActiveLayer", DoGetActiveLayer, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "CreateListEx", CreateListEx, VT_I4, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoCreateChart", DoCreateChart, VT_BOOL, VTS_BSTR VTS_I4 VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoDeleteObjects", DoDeleteObjects, VT_BOOL, VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoSaveProject", DoSaveProject, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetSelectionMode", DoSetSelectionMode, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetText", DoSetText, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoBeginMacro", DoBeginMacro, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoEndMacro", DoEndMacro, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoCreateLayerDialog", DoCreateLayerDialog, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoDeleteLayerDialog", DoDeleteLayerDialog, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoSelectObjectsD", DoSelectObjectsD, VT_BOOL, VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoZoomObjectsD", DoZoomObjectsD, VT_BOOL, VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoCreateTextEx", DoCreateTextEx, VT_BSTR, VTS_R8 VTS_R8 VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetLayerSettings", DoGetLayerSettings, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoFindLayerByName", DoFindLayerByName, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoFindLayerByIndex", DoFindLayerByIndex, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoDeleteLayerByName", DoDeleteLayerByName, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoDeleteLayerByIndex", DoDeleteLayerByIndex, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoLoadBitmap", DoLoadBitmap, VT_BSTR, VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoChangeLayerSettingsEx", DoChangeLayerSettingsEx, VT_BOOL, VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoChangeLayerSettingsByIndex", DoChangeLayerSettingsByIndex, VT_BOOL, VTS_I4 VTS_BSTR VTS_BOOL VTS_I4 VTS_BOOL VTS_R8 VTS_R8 VTS_BOOL VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoCopyObjects", DoCopyObjects, VT_BOOL, VTS_BSTR VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoCreateObject", DoCreateObject, VT_BSTR, VTS_BSTR VTS_I4 VTS_R8 VTS_R8 VTS_BSTR VTS_R8 VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetNextUnusedID", DoGetNextUnusedID, VT_BOOL, VTS_BOOL VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSendGPSPosition", DoSendGPSPosition, VT_BOOL, VTS_BSTR VTS_BSTR VTS_BSTR VTS_R8 VTS_BSTR VTS_BOOL VTS_R8 VTS_BSTR VTS_BSTR VTS_I4 VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoCreateLayerEx", DoCreateLayerEx, VT_BOOL, VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoDisableScreenUpdate", DoDisableScreenUpdate, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetScreenUpdate", DoSetScreenUpdate, VT_BOOL, VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoReloadProject", DoReloadProject, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoChangeBitmapSettings", DoChangeBitmapSettings, VT_BOOL, VTS_BOOL VTS_BOOL VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetOptionSettings", DoSetOptionSettings, VT_BOOL, VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoChangeObjectStyleEx", DoChangeObjectStyleEx, VT_BOOL, VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetNextSymbolNumber", DoGetNextSymbolNumber, VT_I4, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetWindowPos", DoSetWindowPos, VT_BOOL, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetZoom", DoSetZoom, VT_BOOL, VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "DoNotifyTableChange", DoNotifyTableChange, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoInsertObjects", DoInsertObjects, VT_BOOL, VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoAsciiExport", DoAsciiExport, VT_BOOL, VTS_BSTR VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoAsciiImport", DoAsciiImport, VT_BOOL, VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR VTS_R8 VTS_R8 VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "DoNotifyImportError", DoNotifyImportError, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoTempSetText", DoTempSetText, VT_BOOL, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoZoomAndSetText", DoZoomAndSetText, VT_BOOL, VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "WaitForReadyMsg", WaitForReadyMsg, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoMoveObject", DoMoveObject, VT_BOOL, VTS_BSTR VTS_R8 VTS_R8 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetObjectPos", DoSetObjectPos, VT_BOOL, VTS_BSTR VTS_R8 VTS_R8 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetViewSettings", DoGetViewSettings, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "AlignWindow", AlignWindow, VT_BOOL, VTS_I4 VTS_I2)
	DISP_FUNCTION(CAXWingisCtrl, "ShowWindow", ShowWindow, VT_BOOL, VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "SetTimerIntervall", SetTimerIntervall, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "SetTimerState", SetTimerState, VT_BOOL, VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoCommit", DoCommit, VT_BOOL, VTS_NONE)
  DISP_FUNCTION(CAXWingisCtrl, "DoPingWinGIS", DoPingWinGIS, VT_I4, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoConnect", DoConnect, VT_BOOL, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoConnectEx", DoConnectEx, VT_BOOL, VTS_BSTR VTS_BSTR VTS_BSTR)
  DISP_FUNCTION(CAXWingisCtrl, "DoCreateText", DoCreateText, VT_BSTR, VTS_R8 VTS_R8 VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoChangeLayerSettings", DoChangeLayerSettings, VT_BOOL, VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoCreateLayer", DoCreateLayer, VT_BOOL, VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoChangeObjectStyle", DoChangeObjectStyle, VT_BOOL, VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_BOOL)
  DISP_FUNCTION(CAXWingisCtrl, "DoDisconnect", DoDisconnect, VT_BOOL, VTS_NONE)    
	DISP_FUNCTION(CAXWingisCtrl, "DoGetLoadedProjects", DoGetLoadedProjects, VT_I4, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrint", DoPrint, VT_I4, VTS_I4 VTS_BSTR VTS_I4 VTS_BOOL VTS_BOOL VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoProjectSaveAs", DoProjectSaveAs, VT_BOOL, VTS_BSTR VTS_BOOL VTS_BOOL VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoSelectAndSearchEx", DoSelectAndSearchEx, VT_I4, VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR VTS_BOOL VTS_I4 VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_BOOL VTS_R8 VTS_I4 VTS_I4 VTS_BOOL VTS_BSTR VTS_BOOL VTS_BOOL VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetGraphicMode", DoSetGraphicMode, VT_BOOL, VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetSelectionOnGenerate", DoSetSelectionOnGenerate, VT_BOOL, VTS_BOOL)
  DISP_FUNCTION(CAXWingisCtrl, "DoCalcRoute", DoCalcRoute, VT_BOOL, VTS_R8 VTS_R8 VTS_R8 VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "DoActivateProject", DoActivateProject, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetVisibleMapArea", DoSetVisibleMapArea, VT_BOOL, VTS_R8 VTS_R8 VTS_R8 VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "GetModuleFileName", GetModuleFileName, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "SetModuleFileName", SetModuleFileName, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "GetLicenseDays", GetLicenseDays, VT_I4, VTS_BOOL VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "AddLicense", AddLicense, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "GetLastErrorMsg", GetLastErrorMsg, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetObjectProperties", DoGetObjectProperties, VT_BOOL, VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "WaitForMacroEnd", WaitForMacroEnd, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetActiveSymbol", DoSetActiveSymbol, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSupressDialog", DoSupressDialog, VT_BOOL, VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetTextStyle", DoSetTextStyle, VT_BOOL, VTS_BSTR VTS_I4 VTS_I4 VTS_R8 VTS_I4 VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetChartStyle", DoSetChartStyle, VT_BOOL, VTS_I4 VTS_R8 VTS_R8 VTS_R8 VTS_BOOL VTS_BSTR VTS_I4 VTS_I4 VTS_R8 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetChartColor", DoSetChartColor, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetChartColor", DoGetChartColor, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoRequestSymbolInfo", DoRequestSymbolInfo, VT_BOOL, VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoRequestLineStyleInfo", DoRequestLineStyleInfo, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoRequestHatchStyleInfo", DoRequestHatchStyleInfo, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetSelectionState", DoGetSelectionState, VT_I4, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetNewObjectSelection", DoSetNewObjectSelection, VT_BOOL, VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "GetWinGisRelease", GetWinGisRelease, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "GPSDelete", GPSDelete, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "GPSDbColumn", GPSDbColumn, VT_BOOL, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "GPSNew", GPSNew, VT_BOOL, VTS_BSTR VTS_I4 VTS_I4 VTS_BOOL VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "GPSOffset", GPSOffset, VT_BOOL, VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "GPSProAsc", GPSProAsc, VT_BOOL, VTS_BSTR VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "GPSProCalc", GPSProCalc, VT_BOOL, VTS_R8 VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "GPSShow", GPSShow, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "GPSSymSet", GPSSymSet, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "GPSUpdate", GPSUpdate, VT_BOOL, VTS_BSTR VTS_I4 VTS_I4 VTS_BOOL VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "GPSLoadGrf", GPSLoadGrf, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoClearLayer", DoClearLayer, VT_BOOL, VTS_BSTR VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoDenyWingisSession", DoDenyWingisSession, VT_BOOL, VTS_BSTR VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoPolygonPartition", DoPolygonPartition, VT_BOOL, VTS_BSTR VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_R8 VTS_BOOL VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoShowAllObjects", DoShowAllObjects, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoTellProjectPart", DoTellProjectPart, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoPolygonOverlay", DoPolygonOverlay, VT_BOOL, VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoRedraw", DoRedraw, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DeleteStringParam", DeleteStringParam, VT_BOOL, VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoModifyView", DoModifyView, VT_BOOL, VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "SortList", SortList, VT_EMPTY, VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "CStrToDouble", CStrToDouble, VT_R8, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "CDoubleToStr", CDoubleToStr, VT_BSTR, VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "SortListEx", SortListEx, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetCoordinateSystem", DoGetCoordinateSystem, VT_I4, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetProjectSettings", DoGetProjectSettings, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoCreatePolyline", DoCreatePolyline, VT_BSTR, VTS_BSTR VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintSetPrintParam", DoPrintSetPrintParam, VT_I4, VTS_BSTR VTS_BSTR VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintSetPagesParam", DoPrintSetPagesParam, VT_I4, VTS_I4 VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_BOOL VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintSetMapParam", DoPrintSetMapParam, VT_I4, VTS_I4 VTS_I4 VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_BOOL VTS_R8 VTS_BOOL VTS_R8 VTS_R8 VTS_I4 VTS_R8 VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintSetLegendParam", DoPrintSetLegendParam, VT_I4, VTS_I4 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_R8 VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintSetPictureParam", DoPrintSetPictureParam, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_R8 VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintSetSignatureParam", DoPrintSetSignatureParam, VT_I4, VTS_I4 VTS_BSTR VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintGetPrintParam", DoPrintGetPrintParam, VT_I4, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintGetPagesParam", DoPrintGetPagesParam, VT_I4, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintGetMapParam", DoPrintGetMapParam, VT_I4, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintGetLegendParam", DoPrintGetLegendParam, VT_I4, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintGetPictureParam", DoPrintGetPictureParam, VT_I4, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintGetSignatureParam", DoPrintGetSignatureParam, VT_I4, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintInsertLegend", DoPrintInsertLegend, VT_I4, VTS_BSTR VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_R8 VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintInsertPicture", DoPrintInsertPicture, VT_I4, VTS_BSTR VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_R8 VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintInsertSignature", DoPrintInsertSignature, VT_I4, VTS_BSTR VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintStoreTemplate", DoPrintStoreTemplate, VT_I4, VTS_BSTR VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintLoadTemplate", DoPrintLoadTemplate, VT_I4, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintGetObjectsInform", DoPrintGetObjectsInform, VT_I4, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintDeleteObject", DoPrintDeleteObject, VT_I4, VTS_I4  VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoPrintMoveObject", DoPrintMoveObject, VT_I4, VTS_I4  VTS_I4  VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "GPSProSet", GPSProSet, VT_BOOL, VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetPolygonData", DoGetPolygonData, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoExecuteMenuFunction", DoExecuteMenuFunction, VT_BOOL, VTS_BSTR)
    DISP_FUNCTION(CAXWingisCtrl, "DoGetCoordinates", DoGetCoordinates, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoSendMouseAction", DoSendMouseAction, VT_BOOL, VTS_R8 VTS_R8 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoCommitDigitalization", DoCommitDigitalization, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoGenerateObjects", DoGenerateObjects, VT_BOOL, VTS_I4 VTS_BSTR VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoCutSelectedPolygons", DoCutSelectedPolygons, VT_BOOL, VTS_BSTR VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoShowAllOnLayer", DoShowAllOnLayer, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoCreateSymbol", DoCreateSymbol, VT_BOOL, VTS_BSTR VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "DoTrimLine", DoTrimLine, VT_BOOL, VTS_BSTR VTS_R8 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoCreateBuffer", DoCreateBuffer, VT_BOOL, VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "DoUndo", DoUndo, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "AddListItemString", AddListItemString, VT_BOOL, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "AddListItemLong", AddListItemLong, VT_BOOL, VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "AddListItemDouble", AddListItemDouble, VT_BOOL, VTS_I4 VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "GetListItemString", GetListItemString, VT_BSTR, VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "GetListItemLong", GetListItemLong, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "GetListItemDouble", GetListItemDouble, VT_R8, VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DeleteListItem", DeleteListItem, VT_BOOL, VTS_I4 VTS_I4)
  DISP_FUNCTION(CAXWingisCtrl, "DoSetPicture", DoSetPicture, VT_BOOL, VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_BSTR VTS_I4) 
  DISP_FUNCTION(CAXWingisCtrl, "DoCreateObjectEx", DoCreateObjectEx, VT_BOOL, VTS_BSTR VTS_I4 VTS_R8 VTS_R8 VTS_I4 VTS_BSTR VTS_R8 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoReversePolyLine", DoReversePolyLine, VT_I4, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetTooltipText", DoSetTooltipText, VT_BOOL, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoCreateView", DoCreateView, VT_BOOL, VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoDeleteView", DoDeleteView, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoChangeImageSettings", DoChangeImageSettings, VT_BOOL, VTS_BSTR VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetMapCanvas", DoSetMapCanvas, VT_BOOL, VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoDeleteLayerList", DoDeleteLayerList, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetLayerSnap", DoSetLayerSnap, VT_BOOL, VTS_BSTR VTS_BOOL)	
	DISP_FUNCTION(CAXWingisCtrl, "DoSetMouseZoom", DoSetMouseZoom, VT_BOOL, VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoSelectSymbol", DoSelectSymbol, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetSnapRadius", DoSetSnapRadius, VT_BOOL, VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetActiveSymbolEx", DoSetActiveSymbolEx, VT_BOOL, VTS_BSTR VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoCombinePolygons", DoCombinePolygons, VT_I4, VTS_BSTR VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetAreaDivider", DoSetAreaDivider, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetDiagramColor", DoSetDiagramColor, VT_BOOL, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoRedo", DoRedo, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetDiagramType", DoSetDiagramType, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetDiagramSize", DoSetDiagramSize, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetDiagramLabel", DoSetDiagramLabel, VT_BOOL, VTS_BOOL VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetGeotext", DoSetGeotext, VT_BOOL, VTS_BSTR VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetLinePoint", DoGetLinePoint, VT_BOOL, VTS_BSTR VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "DoImportObjectFromFile", DoImportObjectFromFile, VT_BOOL, VTS_BSTR VTS_BSTR VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoExplodeSymbol", DoExplodeSymbol, VT_BOOL, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoCreateThematicMap", DoCreateThematicMap, VT_I4, VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetProjectID", DoSetProjectID, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetProjectID", DoGetProjectID, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(CAXWingisCtrl, "DoCutPolyWithCircle", DoCutPolyWithCircle, VT_BOOL, VTS_BSTR VTS_R8 VTS_R8 VTS_R8)
	DISP_FUNCTION(CAXWingisCtrl, "DoCopyLayer", DoCopyLayer, VT_BOOL, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetPolygonArea", DoGetPolygonArea, VT_R8, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoExplodeObjects", DoExplodeObjects, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoExplodePolygonsToPoints", DoExplodePolygonsToPoints, VT_BOOL, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetSelectionFilter", DoSetSelectionFilter, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetPointOnLine", DoGetPointOnLine, VT_BOOL, VTS_BSTR VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetObjectCenter", DoGetObjectCenter, VT_BOOL, VTS_BSTR VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetObjectPropertiesList", DoGetObjectPropertiesList,VT_I4,VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetAttachedText", DoGetAttachedText, VT_I4, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetIDBOnOff", DoSetIDBOnOff, VT_BOOL, VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetTooltipDB", DoSetTooltipDB, VT_BOOL, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetObjectPropertiesListEx", DoGetObjectPropertiesListEx, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(CAXWingisCtrl, "DoPolyPartitionWithLine", DoPolyPartitionWithLine, VT_BOOL, VTS_BSTR VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoSetLayerPrintable", DoSetLayerPrintable, VT_BOOL, VTS_BSTR VTS_BOOL)
	DISP_FUNCTION(CAXWingisCtrl, "DoGetLayerPolygonArea", DoGetLayerPolygonArea, VT_R8, VTS_BSTR)
	DISP_FUNCTION(CAXWingisCtrl, "DoAttachText", DoAttachText, VT_BOOL, VTS_BSTR VTS_BSTR)
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CAXWingisCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CAXWingisCtrl, COleControl)
	//{{AFX_EVENT_MAP(CAXWingisCtrl)
	EVENT_CUSTOM("OnDDEExecute", FireOnDDEExecute, VTS_BSTR)
	EVENT_CUSTOM("OnConnect", FireOnConnect, VTS_NONE)
	EVENT_CUSTOM("OnDisconnect", FireOnDisconnect, VTS_NONE)
	EVENT_CUSTOM("OnMonitoring", FireOnMonitoring, VTS_I4)
	EVENT_CUSTOM("OnCloseDB", FireOnCloseDB, VTS_NONE)
	EVENT_CUSTOM("OnShowDB", FireOnShowDB, VTS_NONE)
	EVENT_CUSTOM("OnProjectOpen", FireOnProjectOpen, VTS_BSTR)
	EVENT_CUSTOM("OnProjectActivate", FireOnProjectActivate, VTS_BSTR)
	EVENT_CUSTOM("OnProjectClose", FireOnProjectClose, VTS_BSTR)
	EVENT_CUSTOM("OnGisReady", FireOnGisReady, VTS_BOOL VTS_I4)
	EVENT_CUSTOM("OnDelete", FireOnDelete, VTS_I4)
	EVENT_CUSTOM("OnCreateChart", FireOnCreateChart, VTS_I4)
	EVENT_CUSTOM("OnSetCaption", FireOnSetCaption, VTS_I4)
	EVENT_CUSTOM("OnNotifyMoveObjects", FireOnNotifyMoveObjects, VTS_I4)
	EVENT_CUSTOM("OnNotifyMoveDimension", FireOnNotifyMoveDimension, VTS_R8  VTS_R8)
	EVENT_CUSTOM("OnInsert", FireOnInsert, VTS_BSTR  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR)
	EVENT_CUSTOM("OnUpdate", FireOnUpdate, VTS_BSTR  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR)
	EVENT_CUSTOM("OnNotifyNewView", FireOnNotifyNewView, VTS_BSTR)
	EVENT_CUSTOM("OnLinkPointToArea", FireOnLinkPointToArea, VTS_BSTR  VTS_BSTR VTS_R8)
	EVENT_CUSTOM("OnCutArea", FireOnCutArea, VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnChangeTable", FireOnChangeTable, VTS_NONE)
	EVENT_CUSTOM("OnNotifyViewDeleted", FireOnNotifyViewDeleted, VTS_I4)
	EVENT_CUSTOM("OnMacroBegin", FireOnMacroBegin, VTS_NONE)
	EVENT_CUSTOM("OnMacroEnd", FireOnMacroEnd, VTS_NONE)
	EVENT_CUSTOM("OnNotifyMainWindowReady", FireOnNotifyMainWindowReady, VTS_NONE)
	EVENT_CUSTOM("OnNotifySetView", FireOnNotifySetView, VTS_BSTR)
	EVENT_CUSTOM("OnDeclareLayerSettings", FireOnDeclareLayerSettings, VTS_I4  VTS_BSTR  VTS_I4  VTS_BOOL  VTS_BOOL  VTS_BOOL  VTS_R8  VTS_R8  VTS_BOOL  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_BOOL)
	EVENT_CUSTOM("OnDeclareViewSettings", FireOnDeclareViewSettings, VTS_BSTR  VTS_BOOL  VTS_BOOL  VTS_BOOL  VTS_R8  VTS_R8  VTS_R8  VTS_R8)
	EVENT_CUSTOM("OnDeclareViewLayerSettings", FireOnDeclareViewLayerSettings, VTS_BSTR  VTS_I4  VTS_BSTR  VTS_I4  VTS_BOOL  VTS_BOOL  VTS_R8  VTS_R8  VTS_BOOL  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_BOOL)
	EVENT_CUSTOM("OnNotifyBoxSelection", FireOnNotifyBoxSelection, VTS_BSTR  VTS_R8  VTS_R8  VTS_R8  VTS_R8)
	EVENT_CUSTOM("OnNotifyCircleSelection", FireOnNotifyCircleSelection, VTS_BSTR  VTS_R8  VTS_R8  VTS_R8)
	EVENT_CUSTOM("OnDeclareNextUnusedID", FireOnDeclareNextUnusedID, VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR)
	EVENT_CUSTOM("OnBuildArea", FireOnBuildArea, VTS_I4  VTS_BSTR  VTS_R8  VTS_BSTR)
	EVENT_CUSTOM("OnAsciiExport", FireOnAsciiExport, VTS_I4)
	EVENT_CUSTOM("OnAsciiImport", FireOnAsciiImport, VTS_BSTR  VTS_R8  VTS_I4  VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnFreeMonitoring", FireOnFreeMonitoring, VTS_I4  VTS_BSTR)
	EVENT_CUSTOM("OnTimer", FireOnTimer, VTS_NONE)
	EVENT_CUSTOM("OnPrintResult", FireOnPrintResult, VTS_BSTR  VTS_I4)
	EVENT_CUSTOM("OnPrintEvents", FireOnPrintEvents,  VTS_I4  VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnProjectSaveAs", FireOnProjectSaveAs, VTS_BSTR)
	EVENT_CUSTOM("OnGraphicStatus", FireOnGraphicStatus, VTS_BSTR)
	EVENT_CUSTOM("OnActiveLayerStatus", FireOnActiveLayerStatus, VTS_BSTR  VTS_BOOL)
	EVENT_CUSTOM("OnVisibleMapArea", FireOnVisibleMapArea, VTS_R8  VTS_R8  VTS_R8  VTS_R8)
	EVENT_CUSTOM("OnVisibleMapWidth", FireOnVisibleMapWidth, VTS_R8)
	EVENT_CUSTOM("OnSelectionStatus", FireOnSelectionStatus, VTS_BSTR)
	EVENT_CUSTOM("OnSelectionStatusXValue", FireOnSelectionStatusXValue, VTS_BSTR)
	EVENT_CUSTOM("OnSelectionStatusXInfo", FireOnSelectionStatusXInfo, VTS_BSTR)
	EVENT_CUSTOM("OnSelectionStatusYValue", FireOnSelectionStatusYValue, VTS_BSTR)
	EVENT_CUSTOM("OnSelectionStatusYInfo", FireOnSelectionStatusYInfo, VTS_BSTR)
	EVENT_CUSTOM("OnInsertList", FireOnInsertList, VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnInvalidMapArea", FireOnInvalidMapArea, VTS_R8  VTS_R8  VTS_R8  VTS_R8)
	EVENT_CUSTOM("OnAsciiImportEnd", FireOnAsciiImportEnd, VTS_NONE)
	EVENT_CUSTOM("OnCutAreaLayers", FireOnCutAreaLayers, VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_I4)
	EVENT_CUSTOM("OnError", FireOnError, VTS_BSTR  VTS_BOOL)
	EVENT_CUSTOM("OnObjectProperties", FireOnObjectProperties, VTS_BSTR  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR)
	EVENT_CUSTOM("OnDistance", FireOnDistance, VTS_BSTR)
	EVENT_CUSTOM("OnChartColor", FireOnChartColor, VTS_I4)
	EVENT_CUSTOM("OnLineStyleInfo", FireOnLineStyleInfo, VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnHatchStyleInfo", FireOnHatchStyleInfo, VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnGPSID", FireOnGPSID, VTS_BSTR  VTS_BSTR)
	EVENT_CUSTOM("OnGPSProAsc", FireOnGPSProAsc, VTS_BSTR  VTS_BSTR)
	EVENT_CUSTOM("OnGPSProCalc", FireOnGPSProCalc, VTS_R8  VTS_R8)
	EVENT_CUSTOM("OnGPSSymSet", FireOnGPSSymSet, VTS_I4)
	EVENT_CUSTOM("OnAsciiImportStart", FireOnAsciiImportStart, VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnAsciiExportStart", FireOnAsciiExportStart, VTS_I4)
	EVENT_CUSTOM("OnAsciiExportEnd", FireOnAsciiExportEnd, VTS_NONE)
	EVENT_CUSTOM("OnActiveLayer", FireOnActiveLayer, VTS_BSTR)
	EVENT_CUSTOM("OnCopyObjects", FireOnCopyObjects, VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnPolygonPartition", FireOnPolygonPartition, VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnActualProjectPart", FireOnActualProjectPart, VTS_R8  VTS_R8  VTS_R8  VTS_R8)
	EVENT_CUSTOM("OnCanvasRedraw", FireOnCanvasRedraw, VTS_I4  VTS_R8  VTS_R8  VTS_R8  VTS_R8)
	EVENT_CUSTOM("OnDeclareProjectSettings", FireOnDeclareProjectSettings, VTS_BSTR  VTS_R8  VTS_BSTR  VTS_BSTR)
	EVENT_CUSTOM("OnPrinterParamGet", FireOnPrinterParamGet,  VTS_BSTR  VTS_BSTR  VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnPrintPagesParamGet", FireOnPrintPagesParamGet,  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_I4  VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnPrintMapParamGet", FireOnPrintMapParamGet,  VTS_I4  VTS_I4  VTS_BSTR  VTS_R8  VTS_R8  VTS_R8  VTS_R8  VTS_BOOL  VTS_R8  VTS_BOOL  VTS_R8  VTS_R8  VTS_I4  VTS_R8  VTS_R8  VTS_I4)
	EVENT_CUSTOM("OnPrintLegendParamGet", FireOnPrintLegendParamGet,  VTS_I4  VTS_BSTR  VTS_R8  VTS_R8  VTS_I4  VTS_I4  VTS_R8  VTS_R8  VTS_I4)
	EVENT_CUSTOM("OnPrintPictureParamGet", FireOnPrintPictureParamGet,  VTS_I4  VTS_BSTR  VTS_I4  VTS_BOOL  VTS_R8  VTS_R8  VTS_I4  VTS_I4  VTS_R8  VTS_R8  VTS_I4)
	EVENT_CUSTOM("OnPrintSignatureParamGet", FireOnPrintSignatureParamGet, VTS_I4 VTS_BSTR VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8  VTS_I4)
	EVENT_CUSTOM("OnPrintObjectsInformGet", FireOnPrintObjectsInformGet, VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnSendPolygonData", FireOnSendPolygonData, VTS_I4 VTS_R8 VTS_BSTR VTS_I4 VTS_I4)
    EVENT_CUSTOM("OnPrintObjectsInformGet", OnPrintObjectsInformGet, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	EVENT_CUSTOM("OnLinkTextToArea", FireOnLinkTextToArea, VTS_BSTR  VTS_BSTR  VTS_BSTR)
	EVENT_CUSTOM("OnMouseAction", FireOnMouseAction, VTS_R8  VTS_R8  VTS_BSTR  VTS_I4  VTS_I4  VTS_BOOL)
  EVENT_CUSTOM("OnGetCoordinates", FireOnGetCoordinates, VTS_R8 VTS_R8 VTS_BOOL) 
	EVENT_CUSTOM("OnSymbolInfo", FireOnSymbolInfo, VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4  VTS_I4)
  EVENT_CUSTOM("OnSetPicture", FireOnSetPicture, VTS_I4  VTS_I4)  
  EVENT_CUSTOM("OnCreateObjectEx", FireOnCreateObjectEx, VTS_I4  VTS_I4)  
	EVENT_CUSTOM("OnTooltipText", FireOnTooltipText, VTS_BSTR)
	EVENT_CUSTOM("OnGetLinePoint", FireOnGetLinePoint, VTS_R8  VTS_R8  VTS_R8)
	EVENT_CUSTOM("OnImpExpFinished", FireOnImpExpFinished, VTS_NONE)
	EVENT_CUSTOM("OnCreateThematicMap", FireOnCreateThematicMap, VTS_I2)
	EVENT_CUSTOM("OnGetProjectID", FireOnGetProjectID, VTS_BSTR)
	EVENT_CUSTOM("OnPolyCircleCut", FireOnPolyCircleCut, VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnCopyLayer", FireOnCopyLayer, VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnObjectCenter", FireOnObjectCenter, VTS_I4  VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnCreateText", FireOnCreateText, VTS_I4)
	EVENT_CUSTOM("OnGetAttachedText", FireOnGetAttachedText, VTS_I4  VTS_I4)
	EVENT_CUSTOM("OnImpEnd", FireOnImpEnd, VTS_NONE)
	EVENT_STOCK_CLICK()
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CAXWingisCtrl, 2)
	PROPPAGEID(CAXWingisPropPage::guid)
  PROPPAGEID(CAXWingisPropPageLicense::guid)
END_PROPPAGEIDS(CAXWingisCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

//IMPLEMENT_OLECREATE_EX(CAXWingisCtrl, "AXWINGIS.AXWingisCtrl.1",
    //STD  0xb67defa5, 0x57da, 0x11d2, 0xa5, 0x33, 0x48, 0x54, 0xe8, 0x29, 0x8f, 0xc6)
	
  
  // 0x6a65e783, 0x23ae, 0x11d1, 0xbd, 0x93, 0x44, 0x45, 0x53, 0x54, 0, 0
  
  
//  )

CPP_UUID_CLASSID
//CPP_UUID_CLASSID_MAIN( ID_VERSION )

    // #CLSID_CHANGE#

/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CAXWingisCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DAXWingis =
        //STD{ 0xb67defa3, 0x57da, 0x11d2, { 0xa5, 0x33, 0x48, 0x54, 0xe8, 0x29, 0x8f, 0xc6 } };
		//{ 0x6a65e781, 0x23ae, 0x11d1, { 0xbd, 0x93, 0x44, 0x45, 0x53, 0x54, 0, 0 } };
    CPP_IID_DAXWingis;
        // #CLSID_CHANGE#

const IID BASED_CODE IID_DAXWingisEvents =
        //STD{ 0xb67defa4, 0x57da, 0x11d2, { 0xa5, 0x33, 0x48, 0x54, 0xe8, 0x29, 0x8f, 0xc6 } };		
      //  { 0x6a65e782, 0x23ae, 0x11d1, { 0xbd, 0x93, 0x44, 0x45, 0x53, 0x54, 0, 0 } };
      CPP_IID_DAXWingisEvents;
        // #CLSID_CHANGE#


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwAXWingisOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
   //OLEMISC_ONLYICONIC |
   OLEMISC_ALWAYSRUN |
   //OLEMISC_NOUIACTIVATE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CAXWingisCtrl, IDS_AXWINGIS, _dwAXWingisOleMisc)


static const TCHAR BASED_CODE _szLicFileName[] = _T("AXWingis.lic");

static const WCHAR BASED_CODE _szLicString[] =
	L"Copyright (c) 1999 EM";


BOOL CAXWingisCtrl::CAXWingisCtrlFactory::VerifyLicenseKey(BSTR bstrKey)
{

 //Debug.Print("VerifyLicenseKey TRUE");
#ifdef _DEBUGWIN_ON
   //CDebug Debug;
   //Debug.Print(AXWINGIS_VERSION);
#endif

 return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CAXWingisCtrl::CAXWingisCtrlFactory::VerifyUserLicense -
// Checks for existence of a user license

BOOL CAXWingisCtrl::CAXWingisCtrlFactory::VerifyUserLicense()
{
BOOL bRet;

#ifdef _DEBUGWIN_ON
   CDebug Debug;
   Debug.Print(AXWINGIS_VERSION); 
#endif

	bRet = AfxVerifyLicFile(AfxGetInstanceHandle(), _szLicFileName,
		_szLicString);

    if (bRet)
      {
#ifdef _DEBUGWIN_ON
      Debug.Print("VerifyUserLicense TRUE");
#else
      ;
#endif
      }
    else
      {      
      bRet = TestOnlyDesignLicense();
      if (bRet)
        {
#ifdef _DEBUGWIN_ON
        Debug.Print("TestOnlyDesignLicense TRUE");
#else
        ;
#endif
        }
      else
        {
         
         CAddLKeyDlg addDlg;
         addDlg.DoModal();
         
         bRet = TestOnlyDesignLicense();
        }

      }

    return bRet;
}

/////////////////////////////////////////////////////////////////////////////
// CLicenseCtrl::CLicenseCtrlFactory::GetLicenseKey -
// Returns a runtime licensing key

BOOL CAXWingisCtrl::CAXWingisCtrlFactory::GetLicenseKey(DWORD dwReserved,
    BSTR FAR* pbstrKey)    
{    

    if (pbstrKey == NULL)        
       return FALSE;

    *pbstrKey = SysAllocString(_szLicString);    

    return (*pbstrKey != NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CAXWingisCtrl::CAXWingisCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CAXWingisCtrl

BOOL CAXWingisCtrl::CAXWingisCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegInsertable | afxRegApartmentThreading to afxRegInsertable.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_AXWINGIS,
			IDB_AXWINGIS,
			/*afxRegInsertable |*/ afxRegApartmentThreading,
			_dwAXWingisOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CAXWingisCtrl::CAXWingisCtrl - Constructor

CAXWingisCtrl::CAXWingisCtrl()
{
   // is set to TRUE only before call to RecreateWindow
   // to prevent the DdeUninit
    bRecreateWindow = FALSE;
   
    InitializeIIDs(&IID_DAXWingis, &IID_DAXWingisEvents);

	// TODO: Initialize your control's instance data here.
	//SetText("TEST");

    bmXlink.LoadBitmap(IDB_XLINK);                  // Load our Bitmaps
    bmXlinkDisabled.LoadBitmap(IDB_XLINKDISABLED);
    bmMask.LoadBitmap(IDB_MASK);

    if (!bmXlink.GetBitmap(&bmInfo))               // Get Size
      {                                            // if error
       bmInfo.bmHeight = 32;                       // set to default size
       bmInfo.bmWidth = 32;
      }

    SetInitialSize(bmInfo.bmWidth+2,bmInfo.bmHeight+2);

    // DDEML
    idInst = 0;
    hcGis = 0;

    // COM
    bAssignedIpc = FALSE;

    m_connected = FALSE;
    m_ready = FALSE;
    m_bSendOk = TRUE;

    m_layernameList = -1;
    m_layerindexList = -1;
    bm_layernameListValid = FALSE;
    m_layerName = "";
    m_layerIndex = "";
    m_layerinfo = LIF_NONE;
    csReturnString = "";
    hReturnList = -1;
    m_csVersion = "";

    m_chSep = ',';
    m_csDDESeparator = ",";
    m_useSend = 0;

    m_lConnectTimeout = 0;

    bTimerActive = FALSE;

    pAXWingis = this;
    
    m_appname = "";
    m_axmode = AXMODE_INVALID;
    iActiveLicenseNr = -1;

    //aBase.SetSize(10,2);
    
    bInside = FALSE;             // flag if a cmd is currently being processed
    bInsideDelayedSend = FALSE;

    bDesignLicenseOk = CheckDesignLicense();   
    
    SetTimeoutValues();         // load the connect and SOK timeouts from the registry 

	  InternalSetReadyState(READYSTATE_COMPLETE);

#ifdef _DEBUGWIN_ON
    Debug.Print("AppName: " + m_appname);
#endif

}


/////////////////////////////////////////////////////////////////////////////
// CAXWingisCtrl::~CAXWingisCtrl - Destructor

CAXWingisCtrl::~CAXWingisCtrl()
{
	// TODO: Cleanup your control's instance data here

    try
      {
       SetActive(FALSE); 

       //if (ddeWnd != NULL) 
       //  ddeWnd->Terminate();
       

       //delete ddeWnd;
       if (bAssignedIpc)
         {
          DisconnectIPC();
          bAssignedIpc = FALSE;           
          axipc.DestroyWindow();
         }


      }
    //__except(EXCEPTION_EXECUTE_HANDLER) { }
    catch( CException* e) { e->Delete(); }
}


/////////////////////////////////////////////////////////////////////////////
// CAXWingisCtrl::OnDraw - Drawing function

void CAXWingisCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
  CDC dcMem;
  CDC dcMask;
  //CDC *pdcP;
  CBitmap* pOldBitmap;
  CBitmap* pOldMask;
  	
  pdc->FillSolidRect(rcBounds,GetSysColor(COLOR_BTNFACE));
  
  //pdcP = GetParent()->GetDC();
  if (dcMem.CreateCompatibleDC(pdc) && dcMask.CreateCompatibleDC(pdc))
    {
     pOldMask = dcMask.SelectObject(&bmMask);

     if (m_connected && m_active)
       pOldBitmap = dcMem.SelectObject(&bmXlink);
     else
       pOldBitmap = dcMem.SelectObject(&bmXlinkDisabled);
     
   /*  pdc->BitBlt(rcBounds.TopLeft().x + 1        // Mask the target dc
                ,rcBounds.TopLeft().y + 1        // using AND
                ,bmInfo.bmWidth ,bmInfo.bmHeight 
                ,pdcP,0,0,SRCCOPY);
  */
  /*   pdc->BitBlt(rcBounds.TopLeft().x + 1        // Mask the target dc
                ,rcBounds.TopLeft().y + 1        // using AND
                ,bmInfo.bmWidth ,bmInfo.bmHeight 
                ,&dcMask,0,0,SRCAND);

     pdc->BitBlt(rcBounds.TopLeft().x + 1        // Merge the target with our
                ,rcBounds.TopLeft().y + 1        // bitmap using OR
                ,bmInfo.bmWidth ,bmInfo.bmHeight 
                ,&dcMem,0,0,SRCPAINT);
*/     
  
     pdc->StretchBlt(rcBounds.TopLeft().x + 1        // Mask the target dc
                ,rcBounds.TopLeft().y + 1        // using AND
                ,rcBounds.Width() ,rcBounds.Height()
                ,&dcMask,0,0
                ,bmInfo.bmWidth ,bmInfo.bmHeight
                ,SRCAND);

     pdc->StretchBlt(rcBounds.TopLeft().x + 1        // Merge the target with our
                ,rcBounds.TopLeft().y + 1        // bitmap using OR
                ,rcBounds.Width() ,rcBounds.Height() 
                ,&dcMem,0,0
                ,bmInfo.bmWidth ,bmInfo.bmHeight
                ,SRCPAINT);
     
     dcMem.SelectObject(pOldBitmap);
     dcMask.SelectObject(pOldMask);
     dcMem.DeleteDC();
     dcMask.DeleteDC();
    }
  //pdc->DrawState(CPoint(40,4), CSize(32,32), &bmXlink, DSS_UNION);
 
 //	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
 //	pdc->Ellipse(rcBounds);

}


/////////////////////////////////////////////////////////////////////////////
// CAXWingisCtrl::DoPropExchange - Persistence support

void CAXWingisCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

   // TODO: Call PX_ functions for each persistent custom property.
   PX_String(pPX, "sServer", m_server, "AMDB");
   PX_String(pPX, "sTopic", m_topic, "AX");
   // PX_String(pPX, "DDESeparator", m_csDDESeparator, ","); 
   PX_Bool(pPX, "bUseSend", m_useSend, 0);
   PX_String(pPX, "sAppName", m_appname, "");
   PX_Bool(pPX, "bUseCOM", m_useCOM, 1);
   PX_Bool(pPX, "bActive", m_active, 0);

   if (m_active)
     {
      //m_active = FALSE;
 	   SetActive(FALSE);
      SetActive(TRUE);
     }

}


/////////////////////////////////////////////////////////////////////////////
// CAXWingisCtrl::OnResetState - Reset control to default state

void CAXWingisCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CAXWingisCtrl::AboutBox - Display an "About" box to the user

void CAXWingisCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_AXWINGIS);
	dlgAbout.DoModal();
}


/////////////////////////////////////////////////////////////////////////////
// CAXWingisCtrl::PreCreateWindow - Modify parameters for CreateWindowEx

BOOL CAXWingisCtrl::PreCreateWindow(CREATESTRUCT& cs)
{
	//cs.lpszClass = _T("STATIC");
	return COleControl::PreCreateWindow(cs);
}


/////////////////////////////////////////////////////////////////////////////
// CAXWingisCtrl::IsSubclassedControl - This is a subclassed control

BOOL CAXWingisCtrl::IsSubclassedControl()
{
	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// CAXWingisCtrl::OnOcmCommand - Handle command messages

LRESULT CAXWingisCtrl::OnOcmCommand(WPARAM wParam, LPARAM lParam)
{
#ifdef _WIN32
	WORD wNotifyCode = HIWORD(wParam);
#else
	WORD wNotifyCode = HIWORD(lParam);
#endif

	// TODO: Switch on wNotifyCode here.

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CAXWingisCtrl message handlers

BOOL CAXWingisCtrl::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	/*if (ddeWnd != NULL)
	  {   
	   ddeWnd->SetActive(FALSE);
       ddeWnd->DestroyWindow();         
	   ddeWnd = NULL;
	  } */

    try
      {
#ifdef _DEBUGWIN_ON                                
       Debug.Print("DestroyWindow");
#endif
       if (!bRecreateWindow)
         {
         SetActive(FALSE); 

         if (bAssignedIpc)
           {
            axipc.DestroyWindow();
            bAssignedIpc = FALSE;
           }

#ifdef _DEBUGWIN_ON                                
          Debug.Print("KillThread");
#endif

          Debug.KillThread(); 
         }
      }
    //__except(EXCEPTION_EXECUTE_HANDLER ) { }
    catch(int) { m_active = FALSE; }    

	return COleControl::DestroyWindow();
}
// ************************** DDEML functions **************************
HDDEDATA CALLBACK DdeCallback(UINT uType, UINT uFmt, HCONV hconv, 
                              HSZ hsz1, HSZ hsz2, HDDEDATA hData, 
                              DWORD dwData1, DWORD dwData2)
{    
return pAXWingis->DdeCallbackCtrl(uType, uFmt, hconv, hsz1, hsz2, hData, dwData1, dwData2);
}

HDDEDATA CALLBACK CAXWingisCtrl::DdeCallbackCtrl(
                              UINT uType,       /* transaction type                 */ 
                              UINT uFmt,        /* clipboard data format            */ 
                              HCONV hconv,      /* handle of conversation           */ 
                              HSZ hsz1,         /* handle of string                 */ 
                              HSZ hsz2,         /* handle of string                 */ 
                              HDDEDATA hData,   /* handle of global memory object   */ 
                              DWORD dwData1,    /* transaction-specific data        */ 
                              DWORD dwData2     /* transaction-specific data        */ 
)
{ 
CString csCmd;
DWORD dwDataLen;

try
  {

    switch (uType) { 
        case XTYP_CONNECT:
             if ((DdeCmpStringHandles(hsz1, hsTopic) == 0)
              && (DdeCmpStringHandles(hsz2, hsServer) == 0))
               {
#ifdef _DEBUGWIN_ON                                
                Debug.Print("DDEML Got XTYP_CONNECT");
#endif
                bInside = FALSE;
                bInsideDelayedSend = FALSE;
                return (HDDEDATA) TRUE; // Allow connect
               }
             else
                return (HDDEDATA) FALSE; 
 

        case XTYP_EXECUTE:
            
            dwDataLen = DdeGetData(hData, NULL, 0, 0);
            if (dwDataLen <= 0)
              {
               switch (DdeGetLastError(idInst))
                 {
                 case DMLERR_DLL_NOT_INITIALIZED:
                   Debug.Print("DMLERR_DLL_NOT_INITIALIZED");
                   break;

                 case DMLERR_INVALIDPARAMETER:
                   Debug.Print("DMLERR_INVALIDPARAMETER");
                   break;

                 case DMLERR_NO_CONV_ESTABLISHED:
                   Debug.Print("DMLERR_NO_CONV_ESTABLISHED");
                   break;

                 case DMLERR_NO_ERROR:
                   Debug.Print("Success");
                   break;

                 default:
                   Debug.Print("Unknown error on DdeGetData"); 
                   break;
                 }
               return (HDDEDATA) DDE_FNOTPROCESSED; 
               
              }
            else
              {
               DdeGetData(hData,(unsigned char *) csCmd.GetBuffer(dwDataLen+1), dwDataLen, 0);
               csCmd.ReleaseBuffer(-1);
              }

#ifdef _DEBUGWIN_ALL
            Debug.Print("IN: " + csCmd, TRUE);
#endif
            
            PreProcessMsg(csCmd);

#ifdef _DEBUGWIN_ALL
            Debug.Print("DDE_FACK on XTYP_EXECUTE for " + csCmd);
#endif
                                
            return (HDDEDATA) DDE_FACK; //DDE_FNOTPROCESSED; //DDE_FACK; 
 
        
        case XTYP_XACT_COMPLETE: 
            lAckFlag = TRUE;
#ifdef _DEBUGWIN_ON
            Debug.Print("XTYP_XACT_COMPLETE");
#endif
            return (HDDEDATA) NULL; 
 

        case XTYP_DISCONNECT: 
#ifdef _DEBUGWIN_ON
            if (hconv == hcGis)
              Debug.Print("XTYP_DISCONNECT from GisServer");
            else
              Debug.Print("XTYP_DISCONNECT from GisClient");
#endif            
            DoDisconnect();      // Terminate AX -> GIS Session
            m_ready = FALSE;
            m_bSendOk = FALSE;
            bConnectionByAX = FALSE;
            GotDDETerminate();
            return (HDDEDATA) NULL; 
 
        default: 
            return (HDDEDATA) NULL; 
    } 
  }
catch(char *pchMsg) 
  { 

#ifdef _DEBUGWIN_ON
   if (pchMsg != NULL)
     {      
      Debug.Print("Exception in DdeCallbackCtrl: " + CString(pchMsg));
     }
   else
     {      
      Debug.Print("Exception in DdeCallbackCtrl");
     }
#endif
   bRecreateWindow = FALSE; 
   return (HDDEDATA) NULL;
  }

} 

void CAXWingisCtrl::PreProcessMsg(CString csCmd)
{
CString csGIS;
CString csSystem;
CString csSeparator;
char sepsign;

/*			if (CString(GetCmdToken(csCmd,1)) == CString("SEP_S"))
			{
				sepsign = ',';
                Debug.Print("Got SEPERATOR SIGN: " + csCmd);
				sscanf(csCmd,"[SEP_S][%c]",&sepsign);
				this->m_csDDESeparator = sepsign;
			}*/
           // Debug.Print("<>" + csCmd);

            if (CString(GetCmdToken(csCmd, 1)) == CString("CIF"))
// CIF  **********************************************************       
              {
                Debug.Print("Got CIF: " + csCmd);
                csGIS = GetSubString(GetCmdToken(csCmd, 2), 2, *m_csDDESeparator.GetBuffer(1)); // mk separator ',');
                csSystem = GetSubString(GetCmdToken(csCmd, 2), 3, *m_csDDESeparator.GetBuffer(1)); // mk separator ',');
				
				// mk get the separator from the new giscomdll
				csSeparator = GetCmdToken(csCmd,3);

				 if ( csSeparator != "")	
					m_csDDESeparator = csSeparator;

                if (csGIS == "") csGIS = "WINGIS";
                if (csSystem == "") csSystem = "SYSTEM";

                if ( ! m_useCOM)
                  {
                   hsGIS = DdeCreateStringHandle(idInst, (const char *)csGIS,CP_WINANSI);
                   hsSystem = DdeCreateStringHandle(idInst, (const char *)csSystem,CP_WINANSI);
                  }

                if (bConnectionByAX == FALSE)
                  {
                   if (ConnectToWinGIS())
                     {
#ifdef _DEBUGWIN_ON
                      Debug.Print("Connect to " + csGIS + ", " + csSystem + " = OK");
#endif
					            GotDDEInitiate();        // Notify, fire OnConnect

                      m_ready = TRUE;
                      m_bSendOk = TRUE;
                      //SendToWinGIS("[CIF][" + ModuleFileName() + "," + 
                      //             m_server + "," + m_topic + ",TOPICS32]");
                      
                      
                      // workaround: if the control is not visible in the container, 
                      // we don't get windows-messages !!
                      // without a message-queue, commands from wingis would not be processed
                      if ((WORD)m_hWnd == 0) 
                        {
                         // force window creation
                         bRecreateWindow = TRUE;
                         RecreateControlWindow( );
                         bRecreateWindow = FALSE;
#ifdef _DEBUGWIN_ON
                         Debug.Print("RecreateControlWindow, hWnd = " + IntToStr((WORD)m_hWnd));
#endif
                        }
                      
                      // send the CIF delayed to WinGIS
// mk separator
/*
                      slDelayedCmdQueue.AddTail("[CIF][" + ModuleFileName() + "," + 
                                   m_server + "," + m_topic + ",TOPICS32," + m_appname + "]");
*/ 
                      slDelayedCmdQueue.AddTail("[CIF][" + ModuleFileName() + m_csDDESeparator +  
                                   m_server + m_csDDESeparator +  m_topic + m_csDDESeparator +  "TOPICS32" + m_csDDESeparator +  m_appname + "]");
                      PostMessage(WM_SEND_DELAYED_CMD, 0, 0L);
 				                               
                     }
                  }
                else
                  {
                   bConnectionByAX = FALSE;
                   GotDDEInitiate();        // Notify
                  }
              }
            else if (CString(GetCmdToken(csCmd, 1)) == CString("SOK"))
// SOK  **********************************************************       
              { 
               CString csReady = GetCmdToken(csCmd,2);

               if (csReady == "0")
                 {
                  m_bSendOk = FALSE;
                 }
               else if (csReady == "1")
                 {
                  m_bSendOk = TRUE;
                 }
              }  
            else
// all other  **********************************************************       
              {             
              // workaround: if the control is not visible in the container, 
               // we don't get windows-messages !!
               // without a message-queue, commands from wingis would not be processed
               if ((WORD)m_hWnd == 0) 
                 {
                  // force window creation
                  bRecreateWindow = TRUE;
                  RecreateControlWindow( );
                  bRecreateWindow = FALSE;
#ifdef _DEBUGWIN_ON
                  Debug.Print("RecreateControlWindow, hWnd = " + IntToStr((WORD)m_hWnd));
#endif
                 }
               
               slCmdQueue.AddTail(csCmd);
               PostMessage(WM_CMDAVAILABLE, 0, 0L);
              }
}


CString CAXWingisCtrl::ModuleFileName()
{
    if (csModuleName == "")
      {
       ::GetModuleFileName(NULL,(char *)((unsigned char *) csModuleName.GetBuffer(512)), 511);
       csModuleName.ReleaseBuffer(-1);        
      }
   return csModuleName;
}

BSTR CAXWingisCtrl::GetModuleFileName() 
{
	CString strResult;    
	strResult = ModuleFileName();
	return strResult.AllocSysString();
}

void CAXWingisCtrl::SetModuleFileName(LPCTSTR sFile) 
{
   
	csModuleName = sFile;
#ifdef _DEBUGWIN_ON
   Debug.Print("SetModuleFileName: " + csModuleName);
#endif
}


BOOL CAXWingisCtrl::InitDDEML()
{
 	
   hsServer = 0;
   hsTopic = 0;
   hsGIS = 0;
   hsSystem = 0;
	
	if (DdeInitialize(&idInst,          /* receives instance identifier */ 
         (PFNCALLBACK) DdeCallback,    /* address of callback function */ 
         APPCMD_FILTERINITS |          /* filter XTYPE_EXECUTE         */ 
         APPCLASS_STANDARD,            /* filter notifications         */ 
         0) == DMLERR_NO_ERROR)
	  {
	   hsServer = DdeCreateStringHandle(idInst, m_server, CP_WINANSI);
	   hsTopic = DdeCreateStringHandle(idInst, m_topic, CP_WINANSI);
	   DdeNameService(idInst, hsServer, 0L, DNS_REGISTER | DNS_FILTEROFF);

      hsGIS = DdeCreateStringHandle(idInst,"WINGIS",CP_WINANSI);
      hsSystem = DdeCreateStringHandle(idInst,"SYSTEM",CP_WINANSI);

	   return TRUE;
	  }
	else
      return FALSE; 
}

BOOL CAXWingisCtrl::ConnectToWinGIS()
{
#ifdef _DEBUGWIN_ON
  CString csGIS;
  CString csSystem;

  DdeQueryString(idInst, hsGIS, (char *) csGIS.GetBuffer(4048), 4047, 0);
  csGIS.ReleaseBuffer(-1);
  DdeQueryString(idInst, hsSystem, (char *) csSystem.GetBuffer(4048), 4047, 0);
  csSystem.ReleaseBuffer(-1);

  Debug.Print("Trying to connect to " + csGIS + "/" + csSystem + " ...");
#endif

  hcGis = DdeConnect(idInst, 
                     hsGIS,
                     hsSystem,
                     NULL);
#ifdef _DEBUGWIN_ON
  if (hcGis == 0)
    {
     switch (DdeGetLastError(idInst))
       {
       case DMLERR_DLL_NOT_INITIALIZED:
         Debug.Print("DMLERR_DLL_NOT_INITIALIZED");
         break;

       case DMLERR_INVALIDPARAMETER:
         Debug.Print("DMLERR_INVALIDPARAMETER");
         break;

       case DMLERR_NO_CONV_ESTABLISHED:
         Debug.Print("DMLERR_NO_CONV_ESTABLISHED");
         break;

       case DMLERR_NO_ERROR:
         Debug.Print("Success");
         break;

       default:
         Debug.Print("Unknown error on DdeConnect"); 
         break;
       }
    }
  else
    {
     Debug.Print("DdeConnect successfull");
    }
#endif

  return (hcGis ? TRUE : FALSE);
}

BOOL CAXWingisCtrl::DoDisconnect()
{
 BOOL bRet = FALSE;

  if (m_useCOM)
    {
     DisconnectIPC();
    }
  else
    {
    if (hcGis)
      {
       DdeDisconnect(hcGis);
       hcGis = 0; 

#ifdef _DEBUGWIN_ON
       Debug.Print("We Disconnected from GISServer");
#endif
       bRet = TRUE;
      }
    }
  return bRet;
}

BOOL CAXWingisCtrl::DoConnect(LPCTSTR sConnectionName, LPCTSTR sDBLayerInfo) 
{
	return DoConnectEx(sConnectionName, sDBLayerInfo, "[TRUE,TRUE,FALSE][All Layers,TRUE,FALSE,FALSE,FALSE][1,1,1,1,1,1]");
}

BOOL CAXWingisCtrl::DoConnectEx(LPCTSTR sConnectionName, LPCTSTR sDBLayerInfo, LPCTSTR sParamBlock) 
{
 BOOL bRet = FALSE;
 CString csItem;

#ifdef _DEBUGWIN_ON
    Debug.Print("Starting Connection...");
#endif                
       
       
    bConnectionByAX = TRUE;
                
    if ( ! m_useCOM)
      {
       CString csGIS = "WINGIS";
       CString csSystem = "SYSTEM";
       hsGIS = DdeCreateStringHandle(idInst, (const char *)csGIS,CP_WINANSI);
       hsSystem = DdeCreateStringHandle(idInst, (const char *)csSystem,CP_WINANSI);

       bRet = ConnectToWinGIS();

       csItem = "TOPICS32";
      }
    else
      {
       // Use AXIpc
       bRet = ConnectIPC();
#ifdef _DEBUGWIN_ON
       if (bRet)
         Debug.Print("ConnectIPC returns TRUE");
       else
         Debug.Print("Oh Ohh, ConnectIPC returns FALSE");
#endif              
  
       csItem = "COM";
      }

    if (CString(sDBLayerInfo) == "") sDBLayerInfo = LIF_NONE;
                
    

    
    if (bRet)
      {
#ifdef _DEBUGWIN_ON
       Debug.Print("Connect to WinGIS is OK");
#endif
       m_ready = TRUE; 
       m_bSendOk = TRUE;
       
       SetLayerInfo(sDBLayerInfo);

       CString csCallB;

       if (m_useCOM)
          csCallB = "";
       else
// mk separator
          csCallB = m_csDDESeparator + "1";

/*
       bRet = SendToWinGIS("[CIF][" + ModuleFileName() + "," + 
                    m_server + "," + m_topic + "," + csItem + "," + CString(sConnectionName) + 
                    "][TRUE,TRUE,Wingis," + CString(sDBLayerInfo) + ",#44" + csCallB + "]" + 
                    CString(sParamBlock));		         
*/
	  // Debug.Print(">>>>[CIF][" + ModuleFileName() + m_csDDESeparator +  
               //     m_server + m_csDDESeparator +  m_topic + m_csDDESeparator +  csItem + m_csDDESeparator +  CString(sConnectionName) + 
                 //   "][TRUE" + m_csDDESeparator + "TRUE"+ m_csDDESeparator +"Wingis"+ m_csDDESeparator + CString(sDBLayerInfo) + m_csDDESeparator +  "#44" + csCallB + "]" + 
                   // CString(sParamBlock));
       bRet = SendToWinGIS("[CIF][" + ModuleFileName() + "," +  
                    m_server + "," +  m_topic + "," +  csItem + "," +  CString(sConnectionName) + 
                    "][TRUE" + "," + "TRUE"+ "," +"Wingis"+ "," + CString(sDBLayerInfo) + "," +  "#44" + csCallB + "]" + 
                    CString(sParamBlock));		         
      }
     
	return bRet;
}


void CAXWingisCtrl::GotDDEInitiate()
{    
#ifdef _DEBUGWIN_ON
       Debug.Print("FireOnConnect");
#endif
    
    FireOnConnect();

    m_connected = TRUE;
    Invalidate();
}

void CAXWingisCtrl::GotDDETerminate()
{    
    //m_layerinfo = LIF_NONE;
    m_connected = FALSE;
    bm_layernameListValid = FALSE;
    FireOnDisconnect();
    Invalidate();
}
                
/*BOOL CAXWingisCtrl::SendToWinGIS(const char *pData)
{
 DWORD dwLen;
 HDDEDATA hData;


   if (!m_useDDEML)
     {
      if (ddeWnd != NULL)
        return ddeWnd->SendDDECommand(pData);
      else
        return FALSE; 
     }
   else
     {
     dwLen = lstrlen(pData) + 1;

     //hData = DdeCreateDataHandle(idInst, (LPBYTE) pData, dwLen, 
     //                            0,0L, CF_TEXT, 0 //HDATA_APPOWNED); 
     //if (hData)
     //  {
     //   Debug.Print("DDEML OUT: " + CString(pData));     
        //Debug.Print("DDEML OUT Len: " + IntToStr(dwLen));
     /*   return (BOOL)(DdeClientTransaction((LPBYTE)pData,
                               dwLen,//0xFFFFFFFF,
                               hcGis, 0L, 0,
                               XTYP_EXECUTE, 
                               TIMEOUT_ASYNC,
                               &dwTransactionId) ? TRUE : FALSE); 

         /*if (DdeClientTransaction((LPBYTE)pData,
                               dwLen,//0xFFFFFFFF,
                               hcGis, 0L, 0,
                               XTYP_EXECUTE, 
                               10000, //TIMEOUT_ASYNC,
                               &dwTransactionId) == 0)
           {

            if (DdeGetLastError(idInst) == DMLERR_NOTPROCESSED)
               return TRUE;
            else
               return FALSE;
           }
         else
            return TRUE;   
       }
     else
       return FALSE;
     }
}  */

BOOL CAXWingisCtrl::SendToWinGIS(const char *pData)
{
 DWORD dwLen;
 //HDDEDATA hData;
 ULONG ulWaiting;
 ULONG ulTicks;
 LONG lTimeOut;
 BOOL bRet;
 BOOL bWaitAck;
 

   // dont send duplicate ENDs
   if (csLastOutputCmd == "[END][]")
     {
      if (CString(pData) == csLastOutputCmd)
         {
#ifdef _DEBUGWIN_ON
          Debug.Print("Not sending [END][] - last cmd was [END][] too");
#endif
          return TRUE;
         }
     }
 
   // special debug CLO for DataMax
   //if (CString(pData) == "[CLO][]")
   //  {
   //   AfxMessageBox("Close (""[CLO][]"") is going to be sent by the DB",32);
   //  }

   if (m_bSendOk == FALSE)
     {
      // wait till we get a [SOK][1] from the gis
#ifdef _DEBUGWIN_ON
      Debug.Print("m_bSendOk == FALSE, not sending until we get [SOK][1] or timeout (8000 msec)");
#endif

      csLastEvent = "";
      WaitForSendOk(m_lSOKTimeout);

      //Sleep(200);
      m_bSendOk = TRUE;
     }

   lAckFlag = FALSE;

   // store the current outgoing cmd so we can supress duplicate ENDs
   csLastOutputCmd = pData;

   if (m_useCOM)
     {
      /*if (ddeWnd != NULL)
        return ddeWnd->SendDDECommand(pData);
      else
        return FALSE; */


      //bWaitAck = bRet = axipc.Send(pData);
      bWaitAck = FALSE;
      bRet = SendIPC(pData, FALSE);
     }
   else
     {

        dwLen = lstrlen(pData) + 1;
#ifdef _DEBUGWIN_ON
        Debug.Print("OUT: " + CString(pData), TRUE);     
        //Debug.Print("DDEML OUT Len: " + IntToStr(dwLen));
#endif
        bWaitAck = bRet = (BOOL)(DdeClientTransaction((LPBYTE)pData,
                               dwLen,//0xFFFFFFFF,
                               hcGis, 0L, 0,
                               XTYP_EXECUTE, 
                               TIMEOUT_ASYNC,
                               &dwTransactionId) ? TRUE : FALSE); 
          
        /* if (DdeClientTransaction((LPBYTE)pData,
                               dwLen,//0xFFFFFFFF,
                               hcGis, 0L, 0,
                               XTYP_EXECUTE, 
                               6000, //TIMEOUT_ASYNC,
                               &dwTransactionId) == 0)  */
        if (!bRet)
           {            
            switch (DdeGetLastError(idInst))
              {
               case DMLERR_NOTPROCESSED:
                 Debug.Print("DMLERR_NOTPROCESSED");
                 bRet = TRUE;
                 break;
               case DMLERR_EXECACKTIMEOUT:
                 Debug.Print("DMLERR_EXECACKTIMEOUT");
                 bRet = TRUE;
                 break;
               case DMLERR_POSTMSG_FAILED:
                 Debug.Print("DMLERR_POSTMSG_FAILED");
                 bRet = FALSE;
                 break;
               case DMLERR_REENTRANCY:
                 Debug.Print("DMLERR_REENTRANCY");
                 bRet = TRUE;
                 break;
               case DMLERR_SERVER_DIED:
                 Debug.Print("DMLERR_SERVER_DIED");
                 bRet = FALSE;
                 break;
               case DMLERR_BUSY:
                 Debug.Print("DMLERR_BUSY");
                 bRet = TRUE;
                 break;
               case DMLERR_NO_CONV_ESTABLISHED:
                 Debug.Print("DMLERR_NO_CONV_ESTABLISHED");
                 bRet = FALSE;
                 break;
               default:
                 Debug.Print("Unknown DDEML error on SendToWinGIS");
                 bRet = FALSE;
                 break; 
              }
           }


      }  // if (m_useCOM)


    if (bWaitAck)  
           {
            bRet = TRUE; 

         
            TRY
              {
               LockInPlaceActive(TRUE); 
               ulWaiting = 0;
               ulTicks = GetTickCount();
               SetTimer(1, 300, NULL);
               
               lTimeOut = 6000;
               if (AfxGetApp() != NULL) 
                  while ( ((ulWaiting < (unsigned long)lTimeOut) || (lTimeOut == -1)) 
                        && (lAckFlag == FALSE) && (AfxGetApp()->PumpMessage()) )
                     {         
                     //MsgWaitForMultipleObjects(0, NULL, FALSE, 200, QS_POSTMESSAGE);
                     ulWaiting = GetTickCount() - ulTicks;

                     }
               else
                  lAckFlag = TRUE;     // Some problems with Afx and PumpMessage ? Ignore !

               KillTimer(1);
              }
            CATCH_ALL(e)
              {
   	         e->Delete();
               //KillTimer(1);
              }
            END_CATCH_ALL
            LockInPlaceActive(FALSE); 
            
            if (lAckFlag)
               {
               bRet = TRUE;               
               }
            else
               {
               bRet = FALSE;
#ifdef _DEBUGWIN_ON
               Debug.Print("ACK TIMEOUT for " + CString(pData));
#endif
               }
           }
          
         
   if (! bRet)
     // clear last cmd because an error occured
     csLastOutputCmd = "";
      
   return bRet;
}
// *********************** DDEML functions END *************************

/////////////////////////////////////////////////////////////////////////////
// CAXWingisCtrl property handlers

void CAXWingisCtrl::OnServerChanged() 
{
	SetModifiedFlag();
}

void CAXWingisCtrl::OnTopicChanged() 
{
	SetModifiedFlag();
}

void CAXWingisCtrl::OnUseCOMChanged() 
{
	SetModifiedFlag();
}

BOOL CAXWingisCtrl::GetActive() 
{	
	return m_active;
}

//  ************************************************************************                   
//  FUNCTION:  SetActive
//  PARAMETER: bNewValue....flag setting DDE-Service on(T)/off(F)
//
//             This function loops set the DDE-Server service on or off.
//             If on the control accepts incoming DDE_INITIATES from the GIS
//  RETURN:    none
//  ************************************************************************
void CAXWingisCtrl::SetActive(BOOL bNewValue) 
{
	
   if (AmbientUserMode() == FALSE)
     { 
      // Design-mode active
#ifdef _DEBUGWIN_ON
      Debug.Print("State is DesignMode.");   
#endif
      if (CheckDesignLicense() == FALSE)
      //if (m_axmode == AXMODE_INVALID)
        {
         if (bNewValue)
           {
            PreModalDialog();
            MessageBox("No valid design-mode license found. Control is locked now. You will not be able to communicate with WinGIS until you enter a valid licensekey.", 
                       "AXWingis", MB_ICONEXCLAMATION | MB_OK);
            PostModalDialog();
            //ThrowError(CTL_E_PERMISSIONDENIED, "No valid license found.");
           }
         bNewValue = FALSE;

#ifdef _DEBUGWIN_ON
         Debug.Print("DesignLicense is INVALID !!.");   
#endif
      
        }
      else
        {
#ifdef _DEBUGWIN_ON
         Debug.Print("DesignLicense is valid.");   
#endif
        }
           
     }
   else
     {
      // runtime-mode active
#ifdef _DEBUGWIN_ON
      Debug.Print("State is RuntimeMode.");   
#endif

      if ((m_appname == "") && (CheckDesignLicense() == FALSE))
        {

#ifdef _DEBUGWIN_ON
         Debug.Print("Property sAppName is empty and no DesignLicense found. aborting.");
#endif

         if (bNewValue)
           {
            PreModalDialog();
            MessageBox("ERROR: AppName property not set. A runtime-license is always bound to a specific application, so you must set it.",
                       "AXWingis", MB_ICONEXCLAMATION | MB_OK);
            PostModalDialog();
            //ThrowError(CTL_E_PERMISSIONDENIED, "No valid license found.");
           }
         bNewValue = FALSE;
        }
      else
        {
         BOOL bLicOk(FALSE);

         bLicOk = CheckDesignLicense();
         if (bLicOk == FALSE) bLicOk = CheckLicense();

         if ( bLicOk == FALSE )
         //if (m_axmode == AXMODE_INVALID)
           {

            // no valid runtime-license found, ask the user to enter one
            PreModalDialog();
            CAddLKeyDlg addDlg;
            addDlg.DoModal();
            PostModalDialog();

            if ( (CheckLicense() == FALSE) && (CheckDesignLicense() == FALSE) )
              {
               if (bNewValue)
                 {
                  PreModalDialog();
                  MessageBox("No valid license for application " + m_appname + 
                             " found. Control is locked now. You will not be able to communicate with WinGIS.", 
                             "AXWingis", MB_ICONEXCLAMATION | MB_OK);
                  PostModalDialog();
                 }
               //ThrowError(CTL_E_PERMISSIONDENIED, "No valid license found.");
               bNewValue = FALSE;
              }
           }
#ifdef _DEBUGWIN_ON
         else
           {  // ( bLicOk == TRUE )
            if (m_axmode == AXMODE_INVALID)
              Debug.Print("License: AXMODE_INVALID");   
            else if (m_axmode == AXMODE_STANDARD)
              Debug.Print("License: AXMODE_STANDARD");  
            else if (m_axmode == AXMODE_ADVANCED)
              Debug.Print("License: AXMODE_ADVANCED");              
           }
#endif
        }

     }
 
    // TODO: Add your property handler here
   if (bNewValue)  // Activate
     {
      if ((m_server.GetLength() > 0) && !m_active)
        {
         bConnectionByAX = FALSE;   // the connection was not started by AX (till now)

         theApp = AfxGetApp();
         //InitializeCriticalSection(&csFireEvents);
         bInside = FALSE;
         if ( ! m_useCOM)
           {
            // Use DDEML
            SetTimer(4, 200, NULL);
            if (InitDDEML())
              {
               m_active = TRUE;
#ifdef _DEBUGWIN_ON
               Debug.Print("DDEML Init OK in SetActive");
#endif
               SetModifiedFlag();
              }
            else
              Debug.Print("DDEML Init FAILED");
           }
         else
           {
            // Use COM

            InitIPC();
            m_active = TRUE;

           }
        }
     }
   else            // Deactivate
     {
      if (m_active)
        {
         //DeleteCriticalSection(&csFireEvents);
         bInside = FALSE;
         if ( ! m_useCOM)
           {
            // Use DDEML
            DoDisconnect();

            KillTimer(4);

            if (hsServer)	
              DdeFreeStringHandle(idInst, hsServer);
            if (hsTopic)
              DdeFreeStringHandle(idInst, hsTopic);
            if (hsGIS)
              DdeFreeStringHandle(idInst, hsGIS);
            if (hsSystem)
              DdeFreeStringHandle(idInst, hsSystem);
            
            hsServer = 0;
            hsTopic = 0;
            hsGIS = 0;
            hsSystem = 0;

            DdeUninitialize(idInst);
            idInst = 0;
            Debug.Print("DDEML UnInit OK");
           }
         else
           {
            if (m_connected)
              DisconnectIPC();
           }
         m_active = FALSE;
         m_ready = FALSE;
         m_bSendOk = FALSE;
         SetModifiedFlag();
        }
     }
	
}

BOOL CAXWingisCtrl::GetConnected() 
{
	// TODO: Add your property handler here

	return m_connected;
}

void CAXWingisCtrl::SetConnected(BOOL bNewValue) 
{
	// TODO: Add your property handler here

	if (m_connected && !bNewValue)
      {
       //ddeWnd->SendDDETerminate();       
       SetActive(FALSE);
      }
}

void CAXWingisCtrl::OnUseSendChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

BOOL CAXWingisCtrl::GetReady() 
{
	// This property is set on [RED]
	return m_ready;
}

/////////////////////////////////////////////////////////////////////////////
// Events Battleground
LRESULT CAXWingisCtrl::OnPostedSendToWinGIS(WPARAM wParam, LPARAM lParam)
{
BOOL bNext;
#ifdef _DEBUGWIN_ON
   Debug.Print("Got WM_SEND_DELAYED_CMD");
#endif

 if (bInsideDelayedSend == FALSE)
   {
    bInsideDelayedSend = TRUE;

    if (! slDelayedCmdQueue.IsEmpty())
      {
       try
         {
          CString& csCmd = slDelayedCmdQueue.RemoveHead();
          SendToWinGIS((const char*)csCmd);
         }
       catch(char *pchMsg)
         {
          if (pchMsg != NULL)
            {
#ifdef _DEBUGWIN_ON

             Debug.Print("Exception in SendToWinGIS: " + CString(pchMsg));
#else
             ;
#endif
            }
         }
      }

    bNext = ! slDelayedCmdQueue.IsEmpty();    

    bInsideDelayedSend = FALSE;
    if (bNext) PostMessage(WM_SEND_DELAYED_CMD, 0, 0L);
   }
 else
   {
#ifdef _DEBUGWIN_ON
    Debug.Print("bInsideDelayedSend NOT FALSE");
#endif
   }

 return 0;
}

LRESULT CAXWingisCtrl::OnCmdAvailable(WPARAM wParam, LPARAM lParam)
{
BOOL bNext;
#ifdef _DEBUGWIN_ON
 //Debug.Print("Got WM_CMDAVAILABLE");
#endif

 if (bInside == FALSE)
   {
    bInside = TRUE;
    //EnterCriticalSection(&csFireEvents);
    if (! slCmdQueue.IsEmpty())
      {
       try
         {
          CString& csCmd = slCmdQueue.RemoveHead();
          GotDDEExecute((const char*)csCmd);
         }
       catch(char *pchMsg)
         {
          if (pchMsg != NULL)
            {
#ifdef _DEBUGWIN_ON

             Debug.Print("Exception in GotDDEExecute: " + CString(pchMsg));
#else
             ;
#endif
            }
         }
      }

    bNext = ! slCmdQueue.IsEmpty();    
    //LeaveCriticalSection(&csFireEvents);
    bInside = FALSE;
    if (bNext) PostMessage(WM_CMDAVAILABLE, 0, 0L);
   }
 else
   {
#ifdef _DEBUGWIN_ON
    CString csMsg("bInside NOT FALSE");
    if (! slCmdQueue.IsEmpty())
      {
       CString& csCmd = slCmdQueue.RemoveHead();
       csMsg = csMsg + ", next cmd in queue is " + csCmd;
      }
    Debug.Print(csMsg);
#endif
   }

 return 0;
}

void CAXWingisCtrl::GotDDEExecute(const char * sCmd)
{
 CString csItem;
 CString csToken;
 //int iStart;
 int iOffs;
 //int iCount;
 //int i;
 static long hList;
 static CString csBlockEvent;
 static long hIDFirst;
 static long hIDSecond;
 static long hIDTarget;
 static long hParam1List;
 static long hParam2List;
 static long hParam3List;
 static long hOBPList;
 static CString csLastLVCView;
 long hLocalList;
 

#ifdef _DEBUGWIN_ON
    Debug.Print((const char*)(CString("Start PARSE: ") + sCmd));
#endif

    csToken = GetCmdToken(sCmd,1);
// IMPEXPFINISHED **********************************************************   	
	if (csToken == "ONIMPEXPFINISHED")     
      {
       csLastEvent = csToken;

       if ( FunctionPermissionOk("OnImpExpFinished") )
         FireOnImpExpFinished();  
	} else

// END ********************************************************** 
   
// IMPEND **********************************************************   	
	if (csToken == "IMPEND")     
      {
       csLastEvent = csToken;

       if ( FunctionPermissionOk("OnImpEnd") )
         FireOnImpEnd();  
	} else

// END **********************************************************    
    if (csToken == "END")     
      {       
            if (csBlockEvent == "END")
              {
               Debug.Print("Unblocked END");
              }
            //!!! fire MON event 
            else if (csBlockEvent == "MON")
              {
               csBlockEvent = "END";

               if ( FunctionPermissionOk("OnMonitoring") )
                 {
                  FireOnMonitoring(hList);
#ifdef _DEBUGWIN_ON
                  Debug.Print("After OnMonitoring");
#endif
                 }

               DeleteList(hList);
               csBlockEvent = csToken;   // clear Block ID, set to END
              }

            else if (csBlockEvent == "AIN")
              {
               csBlockEvent = "END";

               if (IsValidList(hParam1List))
                 {

                  long hID = CreateList();
                  long hArea = CreateList();
                  long hLength = CreateList();
                  long hX = CreateList();
                  long hY = CreateList();
                  long hLayer = CreateList();
                  long hParam1 = CreateList();
                  long hParam2 = CreateList();
                  long hParam3 = CreateList();

                  int i = GetListSize(hParam1List);
                  while ( i > 0)
                    {
                     i--;
                     CString csAIN = GetStringParam(hParam1List, i);
               
                     AddStringParam(hID, GetCmdToken(csAIN, 2));
                     AddStringParam(hArea, GetCmdToken(csAIN, 3));
                     AddStringParam(hLength, GetCmdToken(csAIN, 4));
                     AddStringParam(hX, GetCmdToken(csAIN, 5));
                     AddStringParam(hY, GetCmdToken(csAIN, 6));
                     AddStringParam(hLayer, GetCmdToken(csAIN, 7));
                     AddStringParam(hParam1, GetCmdToken(csAIN, 8));
                     AddStringParam(hParam2, GetCmdToken(csAIN, 9));
                     AddStringParam(hParam3, GetCmdToken(csAIN, 10));                                 
                    }
#ifdef _DEBUGWIN_ON
                  Debug.Print("FireOnInsertList");
                  DumpList(hID);
#endif
                  FireOnInsertList(hID, hArea, hLength, hX, hY, hLayer, 
                                   hParam1, hParam2, hParam3);
#ifdef _DEBUGWIN_ON
                  Debug.Print("After FireOnInsertList");
#endif

                  DeleteList(hID);
                  DeleteList(hArea);
                  DeleteList(hLength);
                  DeleteList(hX);
                  DeleteList(hY);
                  DeleteList(hLayer);
                  DeleteList(hParam1);
                  DeleteList(hParam2);
                  DeleteList(hParam3);

                  DeleteList(hParam1List);
                 }

               csBlockEvent = csToken;   // clear Block ID, set to END
              }   // AIN end

            else if (csBlockEvent == "PCCP")
			{
				csBlockEvent = "END";

				if (IsValidList(hParam1List))
				{
					long hx = CreateList();
					long hy = CreateList();

					CString sx, sy;
					double dx, dy;

					
					int c = GetListSize(hParam1List);
					int	i;

					for (i = 0; i < c; i++)
					{
						CString scPCCP = GetStringParam(hParam1List, i);
						
						sx = GetCmdToken(scPCCP, 2);
						sy = GetCmdToken(scPCCP, 3);

						dx = CStrToDouble(sx) / 100.0;
						dy = CStrToDouble(sy) / 100.0;
						
						AddStringParam(hx, DToStr(dx));
						AddStringParam(hy, DToStr(dy));
												
					}
										
					#ifdef _DEBUGWIN_ON
						Debug.Print("FireOnPolyCircleCut");
					#endif
					
					FireOnPolyCircleCut(hx, hy);					

					#ifdef _DEBUGWIN_ON
						Debug.Print("After FireOnPolyCircleCut");
					#endif

					DeleteList(hx);
					DeleteList(hy);
					DeleteList(hParam1List);

				}

			}
			
			else if (csBlockEvent == "OBJCOP")
			{
				csBlockEvent = "END";

				if (IsValidList(hParam1List)) 
				{
				
					long hSource = CreateList();
					long hDest = CreateList();

					int c = GetListSize(hParam1List);
					int i, j, iCount;

					CString sSource, sDest;

					for (i = 0; i < c; i++)
					{
						CString sc = GetStringParam(hParam1List, i);

						iCount = StrToInt(GetCmdToken(sc, 2));
						
						for (j = 0; j < (iCount	* 2); j+=2)
						{
							sSource = GetCmdToken(sc, j + 3);
							sDest = GetCmdToken(sc, j + 4);
							
							AddStringParam(hSource, sSource);
							AddStringParam(hDest, sDest);
						}
						
					}

					#ifdef _DEBUGWIN_ON
						Debug.Print("FireOnCopyLayer");
					#endif
					
					FireOnCopyLayer(hSource, hDest);

					#ifdef _DEBUGWIN_ON
						Debug.Print("After FireOnCopyLayer");
					#endif


					DeleteList(hSource);
					DeleteList(hDest);
					DeleteList(hParam1List);

				}
			}

			else if ( (csBlockEvent == "MO0") || (csBlockEvent == "MO1") 
                   || (csBlockEvent == "MO2") || (csBlockEvent == "MO3") 
                   || (csBlockEvent == "MO4") || (csBlockEvent == "MO5") 
                   || (csBlockEvent == "MO6") || (csBlockEvent == "MO7")
                   || (csBlockEvent == "MO8") || (csBlockEvent == "MO9") 
                   || (csBlockEvent == "M00") || (csBlockEvent == "M01") 
                   || (csBlockEvent == "M02") || (csBlockEvent == "M03") 
                   || (csBlockEvent == "M04") || (csBlockEvent == "M05") 
                   || (csBlockEvent == "M06") || (csBlockEvent == "M07")
                   || (csBlockEvent == "M08") || (csBlockEvent == "M09") )
              {
               if ( FunctionPermissionOk("OnFreeMonitoring") )
                  FireOnFreeMonitoring(hList, csBlockEvent);
               DeleteList(hList);
               csBlockEvent = csToken;   // clear Block ID, set to END
              }
			else if (csBlockEvent == "POL")
			{
				csBlockEvent = "END";
				CString csX;
				CString csY;
				CString csAngle;
				if (IsValidList(hParam1List))
				{
					csX = CString(GetStringParam(hParam1List, 1));
					csY = CString(GetStringParam(hParam1List, 2));
					csAngle = CString(GetStringParam(hParam1List, 3));
					
					double dX,dY,dAngle;
					dX = StrToD(csX);
					dY = StrToD(csY);
					dAngle = StrToD(csAngle);
					
					FireOnGetLinePoint(dX,dY,dAngle);
					DeleteList(hParam1List);
				}

			}

            else if (csBlockEvent == "TMS")
			{
				csBlockEvent = "END";
				CString csRetval;
if (IsValidList(hParam1List))
				{
					csRetval = CString(GetStringParam(hParam1List, 1));
				
					
					int iRetval;
					iRetval = StrToInt(csRetval);
					
					FireOnCreateThematicMap(iRetval);
					DeleteList(hParam1List);
				}

			}
		/*	else if (csBlockEvent == "PGUID")
			{
				csBlockEvent = "END";
				CString  csX;	
				csX = CString(GetStringParam(hReturnList, 1));
				Debug.Print(csX);

			}*/
            //!!! fire DEL event 
            else if (csBlockEvent == "DEL")
              {
               csBlockEvent = "END";
               if ( FunctionPermissionOk("OnDelete") )
                  FireOnDelete(hList);
               DeleteList(hList);
               csBlockEvent = csToken;   // clear Block ID, set to END
              }
            else if (csBlockEvent == "GDI")
              {               
               csBlockEvent = "END";
               if ( FunctionPermissionOk("OnCreateChart") )
                  FireOnCreateChart(hList);
               DeleteList(hList);
               csBlockEvent = csToken;   // clear Block ID, set to END
              }  
            else if (csBlockEvent == "GDT")
              {               
               csBlockEvent = "END";
               if ( FunctionPermissionOk("OnSetCaption") )
                  FireOnSetCaption(hList);
               DeleteList(hList);
               csBlockEvent = csToken;   // clear Block ID, set to END
              }
            else if (csBlockEvent == "MOV")
              {               
               csBlockEvent = "END";
               if ( FunctionPermissionOk("OnNotifyMoveObjects") )
                  FireOnNotifyMoveObjects(hList);
               DeleteList(hList);
               csBlockEvent = csToken;   // clear Block ID, set to END
              } 
            else if (csBlockEvent == "CUT")
              {               
               csBlockEvent = "END";
               if ( FunctionPermissionOk("OnCutArea") )
                  FireOnCutArea(hIDFirst,hIDSecond,hIDTarget,
                                hParam1List,hParam2List,hParam3List);

               DeleteList(hIDFirst);
               DeleteList(hIDSecond);
               DeleteList(hIDTarget);
               DeleteList(hParam1List);
               DeleteList(hParam2List);
               DeleteList(hParam3List);
               csBlockEvent = csToken;   // clear Block ID, set to END
              } 
            else if (csBlockEvent == "ONE") // This event is not exported
              {                             // its only a response to COE
               hReturnList = hList;
               csLastEvent = csBlockEvent;  // Signal the waiting thread to go on
               csBlockEvent = csToken;   // clear Block ID, set to END
              }
			else if (csBlockEvent == "GRD")		//JS
			{
				hReturnList = hList;
				csLastEvent = csBlockEvent;
				csBlockEvent = csToken;
			}
			

            else if (csBlockEvent == "IMP")
              {
               csBlockEvent = "END";
#ifdef _DEBUGWIN_ON
               Debug.Print("FireOnAsciiImportEnd");
#endif
               FireOnAsciiImportEnd();

               DeleteList(hParam1List);
               DeleteList(hParam2List);
               DeleteList(hParam3List);
               
               csBlockEvent = csToken;   // clear Block ID, set to END
              }

            else if (csBlockEvent == "COL")
              {
               csBlockEvent = "END";
#ifdef _DEBUGWIN_ON
               Debug.Print("FireOnAsciiExportEnd could be fired at this point, if ever needed");
#endif
               FireOnAsciiExportEnd();

               DeleteList(hParam1List);
               
               csBlockEvent = csToken;   // clear Block ID, set to END
              }
            else if (csBlockEvent == "LMA")
              {
               csLastEvent = csBlockEvent;  
               csBlockEvent = csToken;   // clear Block ID, set to END
              }

            else if (csBlockEvent == "OBP")
              {
					if (this->bGetObjectPropertiesList)
				{
				//	csLastEvent = csBlockEvent;
					csBlockEvent = "END";
                   if (IsValidList(hOBPList))
                     {
                      int i = 0;
                      while ( i < GetListSize(hOBPList))
                        {
                         CString csOBP = GetStringParam(hOBPList, i);

                         CString csID = GetCmdToken(csOBP, 2);
                         double dArea = StrToD(GetCmdToken(csOBP, 3));
                         double dLength = StrToD(GetCmdToken(csOBP, 4));
                         double dX = StrToD(GetCmdToken(csOBP, 5));
                         double dY = StrToD(GetCmdToken(csOBP, 6));
                         CString csLayer = GetCmdToken(csOBP, 7);
                         CString csParam1 = GetCmdToken(csOBP, 8);
                         CString csParam2 = GetCmdToken(csOBP, 9);
                         CString csParam3 = GetCmdToken(csOBP, 10);
	                     this->AddListItemString(this->OPLsID, csID);
	                     this->AddListItemDouble(this->OPLdArea, dArea);
						 this->AddListItemDouble(this->OPLdLength, dLength);
						 this->AddListItemDouble(this->OPLdX,dX);
						 this->AddListItemDouble(this->OPLdY,dY);
						 this->AddListItemString(this->OPLsLayer,csLayer);
						 this->AddListItemString(this->OPLsParam1,csParam1);
						 this->AddListItemString(this->OPLsParam2,csParam2);
						 this->AddListItemString(this->OPLsParam3,csParam3);

						 this->lOBPItems++;	
                         i++;
                        }
					  csLastEvent = "OBP";
				   }
				

				} else
				{



               csLastEvent = csBlockEvent;  
               csBlockEvent = csToken;   // clear Block ID, set to END

               if ( FunctionPermissionOk("OnObjectProperties") )
                 {
                   if (IsValidList(hOBPList))
                     {
                      int i = 0;
                      while ( i < GetListSize(hOBPList))
                        {
                         CString csOBP = GetStringParam(hOBPList, i);

                         CString csID = GetCmdToken(csOBP, 2);
                         double dArea = StrToD(GetCmdToken(csOBP, 3));
                         double dLength = StrToD(GetCmdToken(csOBP, 4));
                         double dX = StrToD(GetCmdToken(csOBP, 5));
                         double dY = StrToD(GetCmdToken(csOBP, 6));
                         CString csLayer = GetCmdToken(csOBP, 7);
                         CString csParam1 = GetCmdToken(csOBP, 8);
                         CString csParam2 = GetCmdToken(csOBP, 9);
                         CString csParam3 = GetCmdToken(csOBP, 10);
#ifdef _DEBUGWIN_ON
                         Debug.Print("FireOnObjectProperties with ID " + csID);
#endif                         
                         FireOnObjectProperties(csID, dArea, dLength, dX, dY, csLayer, csParam1, csParam2, csParam3);
#ifdef _DEBUGWIN_ON
                         Debug.Print("After FireOnObjectProperties with ID " + csID);
#endif                         
                         i++;
                        }
                     }
                 }

               if (IsValidList(hOBPList)) DeleteList(hOBPList);
               hOBPList = -1;
				}
              }

            else if (csBlockEvent == "PPR")
              {
               csBlockEvent = "END";

               if (IsValidList(hParam1List))
                 {

                  long hSourceLayer = CreateList();
                  long hSourceID = CreateList();
                  long hDestLayer = CreateList();
                  long hNewID = CreateList();
                  long hArea = CreateList();
                  long hPerimeter = CreateList();

                  int i = GetListSize(hParam1List);
                  while ( i > 0)
                    {
                     i--;
                     CString csPPR = GetStringParam(hParam1List, i);
               
                     AddStringParam(hSourceLayer, GetCmdToken(csPPR,2));
                     AddStringParam(hSourceID, GetCmdToken(csPPR,3));
                     AddStringParam(hDestLayer, GetCmdToken(csPPR,4));
                     AddStringParam(hNewID, GetCmdToken(csPPR,5));
                     AddStringParam(hArea, GetCmdToken(csPPR,6));
                     AddStringParam(hPerimeter, GetCmdToken(csPPR,7));
                    }

                  if ( FunctionPermissionOk("OnPolygonPartition") )  
                    FireOnPolygonPartition(hSourceLayer, hSourceID,
                                           hDestLayer, hNewID,
                                           hArea, hPerimeter);

#ifdef _DEBUGWIN_ON
                  Debug.Print("After FireOnPolygonPartition");
#endif

                  DeleteList(hSourceLayer);
                  DeleteList(hSourceID);
                  DeleteList(hDestLayer);
                  DeleteList(hNewID);
                  DeleteList(hArea);
                  DeleteList(hPerimeter);

                  DeleteList(hParam1List);
                 }

               csBlockEvent = csToken;   // clear Block ID, set to END
              }   // PPR end
 
			//event onGetAttachedText
			else if (csBlockEvent == "SATT")
              {
               csBlockEvent = "END";
			
               if (IsValidList(hParam1List))
			   {
			
				  long hProgisIDList = CreateList();
				  long hTextList = CreateList();

				  int iSize = GetListSize(hParam1List);
				  for(int i=0; i<iSize; i++)
				  {
					  CString csSATT = GetStringParam(hParam1List, i);

					  AddStringParam(hProgisIDList, GetCmdToken(csSATT, 2));
					  AddStringParam(hTextList, GetCmdToken(csSATT, 3));
				  }

			      FireOnGetAttachedText(hProgisIDList, hTextList);
			   }
			}
            else if (csBlockEvent == "TSY")
              {
               csBlockEvent = "END";

               if (IsValidList(hParam1List))
                 {

                  long hSymbolNumberList = CreateList();
                  long hSymbolNameList = CreateList();
                  long hSymbolLibList = CreateList();
                  long hSizeList = CreateList();
                  long hFillStyleList = CreateList();
                  long hFillStyleFGColorList = CreateList();
                  long hFillStyleBGColorList = CreateList();
                  long hLineStyleList = CreateList();
                  long hLineStyleColorList = CreateList();

                  int iSize = GetListSize(hParam1List);
                  int i = 0;
                  while ( i < iSize)
                    {                     
                     CString csTSY = GetStringParam(hParam1List, i);
               
                     AddStringParam(hSymbolNumberList, GetCmdToken(csTSY,2));
                     AddStringParam(hSymbolNameList, GetCmdToken(csTSY,3));
                     AddStringParam(hSymbolLibList, GetCmdToken(csTSY,4));
                     AddStringParam(hSizeList, GetCmdToken(csTSY,5));
                     AddStringParam(hFillStyleList, GetCmdToken(csTSY,6));
                     AddStringParam(hFillStyleFGColorList, GetCmdToken(csTSY,7));
                     AddStringParam(hFillStyleBGColorList, GetCmdToken(csTSY,8));
                     AddStringParam(hLineStyleList, GetCmdToken(csTSY,9));
                     AddStringParam(hLineStyleColorList, GetCmdToken(csTSY,10));

                     i++;
                    }

                  if ( FunctionPermissionOk("FireOnSymbolInfo") )  
                    //FireOnSymbolInfo(hSymbolNumberList, hSymbolNameList, hSymbolLibList);
                    FireOnSymbolInfo(hSymbolNumberList, hSymbolNameList, hSymbolLibList,
                                     hSizeList, hFillStyleList, hFillStyleFGColorList, 
                                     hFillStyleBGColorList, hLineStyleList, hLineStyleColorList);

#ifdef _DEBUGWIN_ON
                  Debug.Print("After FireOnSymbolInfo");
#endif

                  DeleteList(hSymbolNumberList);
                  DeleteList(hSymbolNameList);
                  DeleteList(hSymbolLibList);
                  DeleteList(hSizeList);
                  DeleteList(hFillStyleList);
                  DeleteList(hFillStyleFGColorList);
                  DeleteList(hFillStyleBGColorList);
                  DeleteList(hLineStyleList);
                  DeleteList(hLineStyleColorList);

                  DeleteList(hParam1List);
                 }

               csLastEvent = "TSY";
               csBlockEvent = csToken;      // clear Block ID, set to END
              }   // TSY end

            else if (csBlockEvent == "PRL") // This event is not exported
              {                             // its only a response to LOA
               hReturnList = hList;
               csLastEvent = csBlockEvent;  // Signal the waiting thread to go on
               csBlockEvent = csToken;      // clear Block ID, set to END
              }

            else if (csBlockEvent == "OBS") // This event is not exported
              {                             // its only a response to SEL
               hReturnList = hList;
               csLastEvent = csBlockEvent;  // Signal the waiting thread to go on
               csBlockEvent = csToken;      // clear Block ID, set to END
              }
            else if (csBlockEvent == "SOB") // This event is not exported
              {                             // its only a response to IOS
               hReturnList = hList;
               csLastEvent = csBlockEvent;  // Signal the waiting thread to go on
               csBlockEvent = csToken;      // clear Block ID, set to END
              }

      }

// MON , DEL , GDI , GDT , MOV *********************************************
    else if ((csToken == "MON") 
          || (csToken == "DEL") 
          || (csToken == "GDI")
          || (csToken == "GDT")
          || (csToken == "MOV")
          || (csToken == "MO0") || (csToken == "MO1") 
          || (csToken == "MO2") || (csToken == "MO3") 
          || (csToken == "MO4") || (csToken == "MO5") 
          || (csToken == "MO6") || (csToken == "MO7")
          || (csToken == "MO8") || (csToken == "MO9") 
          || (csToken == "M00") || (csToken == "M01") 
          || (csToken == "M02") || (csToken == "M03") 
          || (csToken == "M04") || (csToken == "M05") 
          || (csToken == "M06") || (csToken == "M07")
          || (csToken == "M08") || (csToken == "M09") 
		  || (csToken == "OBS") )

      {
       // Some Commands can come in Blocks of 200 IDs followed by an [END][]
       // we must add all IDs from all Commands in the Block to a list 
       // and wait for the final END
       // to fire the event
       if (csBlockEvent == csToken)   
         {
          iOffs = SetLayerToken(sCmd);
          AddListFromString(sCmd, 3+iOffs, hList);
         }
       else    // First of a Command Block
         {
          csBlockEvent = csToken;
          iOffs = SetLayerToken(sCmd);
          if (IsValidList(hList))
             DeleteList(hList);

          hList = BuildListFromString(sCmd, 3+iOffs);
          if (!IsValidList(hList))
             {
              FireOnDDEExecute(sCmd);// Fire only if unable to create a new list     
             }
          else
             {
             // if (StrToInt(GetCmdToken(sCmd, 2+iOffs)) < 200)
             //    {
             //     GotDDEExecute("[END][]");    // Force FireEvent immediatly 
             //    }
             }
         }
      }
	else if (csToken == "PGUID")
	{
			//if(hParam1List > -1)//IsValidList(hParam1List))
			//	DeleteList(hReturnList);
			
			//hReturnList = CreateList();
			 //AddListFromString(sCmd,1,hReturnList);
			
			//csBlockEvent = csToken;
			//csToken = "END";
       csReturnString = GetCmdToken(sCmd, 2);
       csLastEvent = csToken;
		
       //if ( FunctionPermissionOk("OnActiveLayer") )
         FireOnGetProjectID(csReturnString);


//#ifdef _DEBUGWIN_ON
					Debug.Print(sCmd);
//#endif
	
	}
	else if (csToken == "POL")
		{
			if(hParam1List > -1)//IsValidList(hParam1List))
				DeleteList(hParam1List);

			 hParam1List = CreateList();
			 AddListFromString(sCmd,1,hParam1List);
			 AddListFromString(sCmd,2,hParam1List);
			 AddListFromString(sCmd,3,hParam1List);
				
			 csBlockEvent = csToken;
			 csToken = "END";

//#ifdef _DEBUGWIN_ON
					Debug.Print(sCmd);
//#endif

		}
	
	else if (csToken == "TMS")
	{
			if(hParam1List > -1)//IsValidList(hParam1List))
				DeleteList(hParam1List);

			 hParam1List = CreateList();
			 AddListFromString(sCmd,1,hParam1List);
			
			 csBlockEvent = csToken;
			 csToken = "END";

//#ifdef _DEBUGWIN_ON
					Debug.Print(sCmd);
//#endif

		}
	
	// COL  **********************************************************
    else if (csToken == "COL")   
      {
          if (IsValidList(hParam1List))
             DeleteList(hParam1List);

          hParam1List = BuildListFromString(sCmd, 3);
          if (!IsValidList(hParam1List))
            {
#ifdef _DEBUGWIN_ON
             Debug.Print("Invalid list in [COL][]");
#endif
            }
          else
            {
             csBlockEvent = csToken;  //store event, needed for deletion of hParam1List;
             if ( FunctionPermissionOk("OnAsciiExportStart") )
               {
                FireOnAsciiExportStart(hParam1List);
               }
            } 
      }
// EXP  **********************************************************
    else if (csToken == "EXP")   
      {
          if (IsValidList(hParam2List))
             DeleteList(hParam2List);

          iOffs = SetLayerToken(sCmd);
          hParam2List = BuildListFromString(sCmd, 3+iOffs);
          if ( ! IsValidList(hParam2List) )
            {
            //FireError
#ifdef _DEBUGWIN_ON
            Debug.Print("Invalid list, cannot fire OnAsciiExport");
#endif            
            }
          else
            {
             if ( FunctionPermissionOk("OnAsciiExport") )
               {
                FireOnAsciiExport(hParam2List); 
               }
             DoCommit();   // send [END][] anyway
            }
          //DeleteList(hParam1List);
          DeleteList(hParam2List);             
      }

// DEF  **********************************************************
    else if (csToken == "DEF")   
      {
          if (IsValidList(hParam1List))
            {
             DeleteList(hParam1List);
             hParam1List = -1;
            }
          if (IsValidList(hParam2List))
            {
             DeleteList(hParam2List);
             hParam2List = -1;
            }

          BuildDoubleListFromString(sCmd, 3, hParam1List, hParam2List);
          if ((!IsValidList(hParam2List)) || (!IsValidList(hParam1List)))
            {      
#ifdef _DEBUGWIN_ON
             Debug.Print("Error: no valid list in DEF");
#endif
            }
          else
            {
             FireOnAsciiImportStart(hParam1List, hParam2List);
            }
      }
// IMP  **********************************************************
    else if (csToken == "IMP")   
      {
         
          if (IsValidList(hParam3List))
            {
             DeleteList(hParam3List);
             hParam3List = -1;
            }

          iOffs = SetLayerToken(sCmd);
          hParam3List = BuildListFromString(sCmd, 4+iOffs);
          if ((!IsValidList(hParam2List)) || (!IsValidList(hParam1List)) || (!IsValidList(hParam3List)))
            {
            //FireError
            Debug.Print("Error: no valid list in IMP");
            }
          else
            {
             CString csArea = GetCmdToken(sCmd, 3+iOffs);
             double dArea = StrToD(csArea);

             if ( FunctionPermissionOk("OnAsciiImport") )             
                {
#ifdef _DEBUGWIN_ON
                 Debug.Print("Area double: " + DToStr(dArea) + " / Area string: " + csArea);
#endif
                 csBlockEvent = csToken;  //store event, needed for OnAsciiImportEnd
                 FireOnAsciiImport(GetCmdToken(sCmd, 2+iOffs),
                                   dArea,
                                   hParam3List,
                                   hParam1List,
                                   hParam2List);
#ifdef _DEBUGWIN_ON
                 Debug.Print("FireOnAsciiImport: " + CString(GetCmdToken(sCmd, 2+iOffs)) + "," +
                              DToStr(dArea) + "," +
                              IntToStr(hParam3List) + "," +
                              IntToStr(hParam1List) + "," +
                              IntToStr(hParam2List) );

#endif

                }
            }
      }

// CLO  **********************************************************
    else if (csToken == "CLO")
      {

   // special debug CLO for DataMax
   // AfxMessageBox("Close (""[CLO][]"") was received from WinGIS",32);
     

       if ( FunctionPermissionOk("OnCloseDB") )             
          FireOnCloseDB();
      }

// DBW  **********************************************************
    else if (csToken == "DBW")   
      {
       if ( FunctionPermissionOk("OnChangeTable") )             
          FireOnChangeTable();
      }

// DVW  **********************************************************
    else if (csToken == "DVW")   
      {
       long hViews = BuildListFromString(sCmd, 3);
       if ( FunctionPermissionOk("OnNotifyViewDeleted") )
          FireOnNotifyViewDeleted(hViews);
      }

// MAB  **********************************************************
    else if (csToken == "MAB")   
      {
       if ( FunctionPermissionOk("OnMacroBegin") )
          FireOnMacroBegin();
      }

// MAE  **********************************************************
    else if (csToken == "MAE")   
      {
       csLastEvent = csToken;
       Sleep(200);
       if ( FunctionPermissionOk("OnMacroEnd") )
          FireOnMacroEnd();
      }

// MWR  **********************************************************
    else if (csToken == "MWR")   
      {
       if ( FunctionPermissionOk("OnNotifyMainWindowReady") )
          FireOnNotifyMainWindowReady();
      }

// SHW  **********************************************************
    else if (csToken == "SHW")     
      {
       if ( FunctionPermissionOk("OnShowDB") )
          FireOnShowDB();
      }

// ONE  **********************************************************
    else if (csToken == "ONE")
      {

       // ONE can come in Blocks of 200 IDs followed by an [END][]
       // we must add all ONEs to a list and wait for the final END
       // its only a response to COE, no event is fired

       if (csBlockEvent == csToken)   
         {
          iOffs = SetLayerToken(sCmd);
          AddListFromString(sCmd, 3+iOffs, hList);
         }
       else    // First of a ONE Block
         {
          csBlockEvent = csToken;
          iOffs = SetLayerToken(sCmd);
          hList = BuildListFromString(sCmd, 3+iOffs);
          if (!IsValidList(hList))             
             FireOnDDEExecute(sCmd);// Fire only if unable to create a new list     
         }
      }

// AVW  **********************************************************
    else if (csToken == "AVW")     
      {
       if ( FunctionPermissionOk("OnNotifyNewView") )
          FireOnNotifyNewView(GetCmdToken(sCmd, 3));
      }

// GRD  **********************************************************    
// Glukhov: This code was changed to implement the asynchronous objects creating 
    else if (csToken == "GRD")     
      {
        //long id1, id2;
        long hProgisIDList, hUserIDList;
        iOffs = SetLayerToken(sCmd);   // Layer wirklich setzen ??    
//        csReturnString = GetSubString(GetCmdToken(sCmd, 3+iOffs), 2, ',');
        
		BuildDoubleListFromTokenString(sCmd, 3+iOffs, hUserIDList, hProgisIDList);
        hList = hProgisIDList;															//JS
        
        if ( FunctionPermissionOk("OnCreateObjectEx") )
          {
           FireOnCreateObjectEx(hProgisIDList, hUserIDList);
          }
        csReturnString = GetSubString(GetCmdToken(sCmd, 3+iOffs), 2, ',');       
        
       // AS 20010612  [GRD] can contain more than one progisid/userid pair
       /*
        csReturnString = GetCmdToken(sCmd, 3+iOffs);
        id1 = StrToInt( GetSubString(csReturnString, 1, ',') );
        csReturnString = GetSubString(csReturnString, 2, ',');       
        if (id1 != 0) {
          id2 = StrToInt( csReturnString );
          if ( FunctionPermissionOk("OnCreateObjectEx") ) FireOnCreateObjectEx( id2, id1 );
        }       
       */
		csBlockEvent = csToken;		//JS
        //csLastEvent = csToken;    //JS auskommentiert
      }

// NSS  **********************************************************    
    else if (csToken == "NSS")     
      {
       csItem = GetCmdToken(sCmd, 2);
       if ( FunctionPermissionOk("OnNotifySetView") )
          FireOnNotifySetView((const char*)csItem);
      }

// NTX  **********************************************************    
    else if (csToken == "NTX")     
      {
       iOffs = SetLayerToken(sCmd);   // Layer wirklich setzen ??    
       //FireOnNotifyNewText(GetCmdToken(sCmd, 2+iOffs));
       csReturnString = GetCmdToken(sCmd, 2+iOffs);
#ifdef _DEBUGWIN_ON
       Debug.Print("NTX ResultString: " + csReturnString);
#endif
       csLastEvent = csToken;
      }

// NON  **********************************************************    
    else if (csToken == "NON")     
      {
       CString csS = GetCmdToken(sCmd, 2);

       if ( FunctionPermissionOk("OnDeclareNextUnusedID") )
          FireOnDeclareNextUnusedID(GetSubString(csS, 1, *this->m_csDDESeparator.GetBuffer(1))      // sPointID
                                  , GetSubString(csS, 2, *this->m_csDDESeparator.GetBuffer(1))      // sLineID
                                  , GetSubString(csS, 3, *this->m_csDDESeparator.GetBuffer(1))      // sAreaID
                                  , GetSubString(csS, 4, *this->m_csDDESeparator.GetBuffer(1))      // sCircleID
                                  , GetSubString(csS, 5, *this->m_csDDESeparator.GetBuffer(1))      // sArcID
                                  , GetSubString(csS, 6, *this->m_csDDESeparator.GetBuffer(1))      // sSymbolID
                                  , GetSubString(csS, 7, *this->m_csDDESeparator.GetBuffer(1))      // sImageID
                                  , GetSubString(csS, 8, *this->m_csDDESeparator.GetBuffer(1))      // sTextID
                                  , GetSubString(csS, 9, *this->m_csDDESeparator.GetBuffer(1))      // sCaptionTextID
                                  , GetSubString(csS, 10, *this->m_csDDESeparator.GetBuffer(1))     // sChartID
                                  , GetSubString(csS, 11, *this->m_csDDESeparator.GetBuffer(1))     // sSplineID
                                  , GetSubString(csS, 12, *this->m_csDDESeparator.GetBuffer(1))     // sMeasureID
                                  , GetSubString(csS, 13, *this->m_csDDESeparator.GetBuffer(1)));   // sOLEObjectID

       csLastEvent = csToken;
      }


// PCL  **********************************************************    
    else if (csToken == "PCL")     
      {
       csItem = GetCmdToken(sCmd, 2);
       bm_layernameListValid = FALSE;
       if ( FunctionPermissionOk("OnProjectClose") )
          FireOnProjectClose((const char*)csItem);
      }
// PRO  **********************************************************
    else if (csToken == "PRO")     
      {
       csItem = GetCmdToken(sCmd,2);
     
       bm_layernameListValid = FALSE;

       if (csLastEvent == "RED") csLastEvent = "";
       
       WaitForReplyTimeout("RED", 5000);
     
     /*
       DoGetLayerSettings();
       if (WaitForReplyTimeout("LMA", 600) == TRUE)
          Debug.Print("OK, got LMA");
       else
          Debug.Print("NOT OK, no LMA received until timeout");
     */ 

       if ( FunctionPermissionOk("OnProjectOpen") )
         {
#ifdef _DEBUGWIN_ON
          Debug.Print("Before OnProjectOpen");
#endif
          FireOnProjectOpen((const char*)csItem);
#ifdef _DEBUGWIN_ON
          Debug.Print("After OnProjectOpen");
#endif
         }
      }
// PRW, NPR  *****************************************************
    else if ( (csToken == "PRW") || (csToken == "NPR") )
      {
       csItem = GetCmdToken(sCmd,2);
       bm_layernameListValid = FALSE;
       if (csLastEvent == "RED") csLastEvent = "";
       
       WaitForReplyTimeout("RED", 5000);

     /*
       DoGetLayerSettings();
       if (WaitForReplyTimeout("LMA", 600) == TRUE)
          Debug.Print("OK, got LMA");
       else
          Debug.Print("NOT OK, no LMA received until timeout");
     */
       if ( FunctionPermissionOk("OnProjectActivate") )
         {
#ifdef _DEBUGWIN_ON
          Debug.Print("Before OnProjectActivate");
#endif         
          FireOnProjectActivate((const char*)csItem);
#ifdef _DEBUGWIN_ON
          Debug.Print("After OnProjectActivate");
#endif
         }
      }
// PSA  **********************************************************
    else if (csToken == "PSA")     
      {
       csItem = GetCmdToken(sCmd,2);
       if ( FunctionPermissionOk("OnProjectSaveAs") )
          FireOnProjectSaveAs((const char*)csItem);
      }


// RED  **********************************************************
    else if (csToken == "RED")     
      { 
       
       CString csReady = GetCmdToken(sCmd,2);
       long lError = 0;
       if (GetCmdToken(sCmd,3) != "")
         lError = StrToInt(GetCmdToken(sCmd,3));

       if (csReady == "0")
         {
          m_ready = FALSE;
         }
       else if (csReady == "1")
         {
          m_ready = TRUE;
         }
       else
         {
          m_ready = TRUE;
         }

      if ( FunctionPermissionOk("OnGisReady") )
        FireOnGisReady(m_ready, lError);
      
      if (m_ready == TRUE)
        csLastEvent = csToken;
      }

// SOK  **********************************************************
    else if (csToken == "SOK")     
      { 
       
       CString csReady = GetCmdToken(sCmd,2);
       long lError = 0;
       if (GetCmdToken(sCmd,3) != "")
         lError = StrToInt(GetCmdToken(sCmd,3));

       if (csReady == "0")
         {
          m_bSendOk = FALSE;
         }
       else if (csReady == "1")
         {
          m_bSendOk = TRUE;
         }
      }

// SYM  **********************************************************    
    else if (csToken == "SYM")
      {
              
       hReturnList = StrToInt(GetCmdToken(sCmd, 2));
       csLastEvent = csToken;
      }

// SRE  **********************************************************    
    else if (csToken == "SRE")
      {              
       long lBitmapHandle = StrToInt(GetCmdToken(sCmd, 2));

       double dLLX = StrToD(GetCmdToken(sCmd, 3));
       double dLLY = StrToD(GetCmdToken(sCmd, 4));
       double dURX = StrToD(GetCmdToken(sCmd, 5));
       double dURY = StrToD(GetCmdToken(sCmd, 6));

       if ( FunctionPermissionOk("OnCanvasRedraw") )
          FireOnCanvasRedraw(lBitmapHandle, dLLX, dLLY, dURX, dURY);
      }

// SCI  **********************************************************    
    else if (csToken == "SCI")
      {                     
       double dX = StrToD(GetCmdToken(sCmd, 2));
       double dY = StrToD(GetCmdToken(sCmd, 3));
       CString csProgisID = GetCmdToken(sCmd, 4);
       long lButton = StrToInt(GetCmdToken(sCmd, 5));
       long lMode = StrToInt(GetCmdToken(sCmd, 6));
       BOOL bDblClick = StrToBool(GetCmdToken(sCmd, 7));

       if ( FunctionPermissionOk("OnMouseAction") )
          FireOnMouseAction(dX, dY, csProgisID, lButton, lMode, bDblClick);
      }


// MOD  **********************************************************
    else if (csToken == "MOD")     
      {
      double dDeltaX;
      double dDeltaY;

       dDeltaX = StrToD(GetCmdToken(sCmd, 2));
       dDeltaY = StrToD(GetCmdToken(sCmd, 3));
       if ( FunctionPermissionOk("OnNotifyMoveDimension") )
          FireOnNotifyMoveDimension(dDeltaX, dDeltaY);
      }

// APP  **********************************************************
    else if (csToken == "APP")     
      {
      double dLLX;
      double dLLY;
      double dURX;
      double dURY;

       dLLX = StrToD(GetCmdToken(sCmd, 2));
       dLLY = StrToD(GetCmdToken(sCmd, 3));
       dURX = StrToD(GetCmdToken(sCmd, 4));
       dURY = StrToD(GetCmdToken(sCmd, 5));
       if ( FunctionPermissionOk("OnActualProjectPart") )
          FireOnActualProjectPart(dLLX, dLLY, dURX, dURY);
      }

// VEW  **********************************************************
    else if (csToken == "VEW")     
      {

       if (IsValidList(m_viewList))
          ClearList(m_viewList);

       AddListFromString(sCmd, 3, m_viewList);
      }

// EIN , UPD *****************************************************
    else if ( (csToken == "EIN") || (csToken == "UPD") )
      {
       CString csID = GetCmdToken(sCmd, 2);
       double dArea = StrToD(GetCmdToken(sCmd, 3));
       double dLength = StrToD(GetCmdToken(sCmd, 4));
       double dX = StrToD(GetCmdToken(sCmd, 5));
       double dY = StrToD(GetCmdToken(sCmd, 6));
       CString csLayer = GetCmdToken(sCmd, 7);
       CString csSymbolNr = GetCmdToken(sCmd, 8);
       CString csSymbolName = GetCmdToken(sCmd, 9);
       CString csSymbolLib = GetCmdToken(sCmd, 10);
       if ((csToken == "UPD"))
         {
         if ( FunctionPermissionOk("OnUpdate") )
            FireOnUpdate(csID, dArea, dLength, dX, dY, csLayer, csSymbolNr, csSymbolName, csSymbolLib);
         }
       else
         {
         if ( FunctionPermissionOk("OnInsert") )
            FireOnInsert(csID, dArea, dLength, dX, dY, csLayer, csSymbolNr, csSymbolName, csSymbolLib);
         }
      }

// SPS *****************************************************
    else if ( csToken == "SPS" )
      {
       CString csUnitsName = GetCmdToken(sCmd, 2);
       double dScale = StrToD(GetCmdToken(sCmd, 3));
       CString csProjectionName = GetCmdToken(sCmd, 4);
       CString csProjectionDate = GetCmdToken(sCmd, 5);
   
       if ( FunctionPermissionOk("OnDeclareProjectSettings") )
          FireOnDeclareProjectSettings(csUnitsName, dScale, csProjectionName, csProjectionDate);
      }

// LFT *****************************************************
    else if ( csToken == "LFT" )
      {
       CString csID = GetCmdToken(sCmd, 2);
   
       if ( FunctionPermissionOk("OnTooltipText") )
          FireOnTooltipText(csID);
      }

// SMC ***************************************************** 
    else if ( csToken == "SMC" )  // Glukhov Bug#203 Build#151 08.02.01
    {
      double  dX, dY;
      bool    bCancel;
      long iX = StrToInt(GetCmdToken(sCmd, 2));
      long iY = StrToInt(GetCmdToken(sCmd, 3));
      if ( iX >= -int(2147483647) || iY >= -int(2147483647) ){
        dX = iX / 100.;
        dY = iY / 100.;
        bCancel = 0;
      } else {
        dX = iX;
        dY = iY;
        bCancel = 1;
      }      
      if ( FunctionPermissionOk("OnGetCoordinates") ) FireOnGetCoordinates( dX, dY, bCancel );
    }

// SPI ***************************************************** 
    else if ( csToken == "SPI" )  // Glukhov Bug#100 Build#152 05.03.01
    {
//      iOffs = SetLayerToken(sCmd);   
      iOffs = 1;    // Layer name/index is returned but not used
      long id = StrToInt(GetCmdToken(sCmd, 2+iOffs));
      long iRes = StrToInt(GetCmdToken(sCmd, 3+iOffs));
      if ( FunctionPermissionOk("OnSetPicture") ) FireOnSetPicture( id, iRes );
    }

// OBP *****************************************************
    else if ( (csToken == "OBP") )
      {

       if (csBlockEvent == csToken)   
         {
          if ( ! IsValidList(hOBPList)) hOBPList = CreateList();          
          if (IsValidList(hOBPList)) AddStringParam(hOBPList, sCmd);
         }
       else    // First of a OBP Block
         {
          csBlockEvent = csToken;

          if (IsValidList(hOBPList)) DeleteList(hOBPList);
          hOBPList = CreateList();
          if (IsValidList(hOBPList)) AddStringParam(hOBPList, sCmd);
         }

      }

// AIN  **********************************************************
    else if (csToken == "AIN")     
      {        
       if (!(csBlockEvent == csToken))
         {
          csBlockEvent = csToken;
          if (IsValidList(hParam1List)) DeleteList(hParam1List);
          hParam1List = CreateList();
         }  

       AddStringParam(hParam1List, sCmd);         
      }   

// PCCP **********************************************************
	else if (csToken == "PCCP")
	{
		if (csBlockEvent != csToken)
		{
			csBlockEvent = csToken;
			if (IsValidList(hParam1List)) DeleteList(hParam1List);
			hParam1List = CreateList();
		}
		AddStringParam(hParam1List, sCmd);
	}

//  OBJCOP
	else if (csToken == "OBJCOP")
	{
		if (csBlockEvent != csToken)
		{
			csBlockEvent = csToken;
			if (IsValidList(hParam1List)) DeleteList(hParam1List);
			hParam1List = CreateList();
		}
		AddStringParam(hParam1List, sCmd);
	}


// CHG  **********************************************************
    	
	else if (csToken == "CHG")     
      {        
       CString csIDPoint;
       CString csIDArea;
       CString csArea;
       CString csText;

       csIDPoint = GetCmdToken(sCmd, 2);
       csIDArea = GetCmdToken(sCmd, 3);
       csArea = GetCmdToken(sCmd, 4);
       csText = GetCmdToken(sCmd, 7);       
       
       if (csText == "")
         {
          if ( FunctionPermissionOk("OnLinkPointToArea") )
             FireOnLinkPointToArea(csIDPoint, csIDArea, StrToD(csArea));
         }
       else
         {
          if ( FunctionPermissionOk("OnLinkTextToArea") )
            FireOnLinkTextToArea(csIDPoint, csIDArea, csText);

         } 
        
      }   

// POV  **********************************************************
    else if (csToken == "POV")     
      {        
       CString csLayer1;
       CString csLayer2;
       CString csLayerTarget;
       CString csOperation;

       csLayer1 = GetCmdToken(sCmd, 2);
       csLayer2 = GetCmdToken(sCmd, 3);
       csLayerTarget = GetCmdToken(sCmd, 4);
       csOperation = GetCmdToken(sCmd, 5);

       if ( FunctionPermissionOk("OnCutAreaLayers") )
          FireOnCutAreaLayers(csLayer1, csLayer2, csLayerTarget, StrToInt(csOperation));

      }
// CUT  **********************************************************
    else if (csToken == "CUT")     
      {        
       if (!(csBlockEvent == csToken))
         {
          csBlockEvent = csToken;
          hIDFirst = CreateList();
          hIDSecond = CreateList();
          hIDTarget = CreateList();
          hParam1List = CreateList();
          hParam2List = CreateList();
          hParam3List = CreateList();
         }  

         AddStringParam(hIDFirst, GetCmdToken(sCmd, 2));
         AddStringParam(hIDSecond, GetCmdToken(sCmd, 3));
         AddStringParam(hIDTarget, GetCmdToken(sCmd, 4));
         AddStringParam(hParam1List, GetCmdToken(sCmd, 5));
         AddStringParam(hParam2List, GetCmdToken(sCmd, 6));
         AddStringParam(hParam3List, GetCmdToken(sCmd, 7));         
      }   

// NIA  **********************************************************
    else if (csToken == "NIA")     
      {        
       int iCount, i;
       CString csItem;
       CString csTargetLayer = "";

       hLocalList = CreateList();
       if (hLocalList >= 0)
         {
          if (m_layerinfo == LIF_NONE)
            iOffs = 0;
          else
            {
            iOffs = 1;
            csTargetLayer = GetCmdToken(sCmd, 2);
            }

          iCount = GetTokenCount(sCmd);
          for (i = 3+iOffs;i <= iCount - 2;i++)
            {
             csItem = GetCmdToken(sCmd,i);
             AddStringParam(hLocalList, csItem);
            }
         
          if ( FunctionPermissionOk("OnBuildArea") )  
             FireOnBuildArea(hLocalList, GetCmdToken(sCmd, iCount-1), 
                             StrToD(GetCmdToken(sCmd, iCount)), 
                             csTargetLayer);       

          DeleteList(hLocalList);
         }
      }   

     
// LQ1  **********************************************************
    else if ((csToken == "LMA") || (csToken == "LMS"))
      {        // new                  // old
       csBlockEvent = csToken;

       bm_layernameListValid = TRUE;

       CString csS = GetCmdToken(sCmd, 2);
                 
       long lLayerIndex = StrToInt(GetSubString(csS, 1, ','));
       CString sLayerName = GetSubString(csS, 2, ',');
       long lPosition = StrToInt(GetSubString(csS, 3, ','));
       BOOL bActive = StrToBool(GetSubString(csS, 4, ','));
       BOOL bOn = StrToBool(GetSubString(csS, 5, ','));
       BOOL bGenOn = StrToBool(GetSubString(csS, 6, ','));
       double dGenMin = StrToD(GetSubString(csS, 7, ','));
       double dGenMax = StrToD(GetSubString(csS, 8, ','));
       BOOL bFixOn = StrToBool(GetSubString(csS, 9, ','));
       long lLineStyle = StrToInt(GetSubString(csS, 10, ','));
       long lLineWidth = StrToInt(GetSubString(csS, 11, ','));
       long lLineColor = StrToInt(GetSubString(csS, 12, ','));
       long lPattern = StrToInt(GetSubString(csS, 13, ','));
       long lPatternColor = StrToInt(GetSubString(csS, 14, ','));
       BOOL bTransparent = StrToBool(GetSubString(csS, 15, ','));

       if (lPosition == 1)          // first layer-description ?
         {  
          if (IsValidList(m_layernameList))
             ClearList(m_layernameList);
          else
             m_layernameList = CreateList();

          if (IsValidList(m_layerindexList)) 
             ClearList(m_layerindexList);
          else
             m_layerindexList = CreateList();
         }
       if (IsValidList(m_layernameList))
         {
          AddStringParam(m_layernameList, sLayerName);
         }
       if (IsValidList(m_layerindexList)) 
         {
          AddStringParam(m_layerindexList, IntToStr(lLayerIndex));
         }

       if ( FunctionPermissionOk("OnDeclareLayerSettings") )  
         {
		    FireOnDeclareLayerSettings(lLayerIndex, sLayerName, lPosition, bActive, 
                                     bOn, bGenOn, dGenMin, dGenMax, bFixOn, 
                                     lLineStyle, lLineWidth, lLineColor, 
                                     lPattern, lPatternColor, bTransparent);
         }       
             
      }   

// LQ2  **********************************************************
    else if ((csToken == "LVD") || (csToken == "LQ2"))
      {        // new                  // old        
       CString csS = GetCmdToken(sCmd, 2);
         
         long lLayerIndex = StrToInt(GetSubString(csS, 1, ','));
         CString sLayerName = GetSubString(csS, 2, ',');
         long lPosition = StrToInt(GetSubString(csS, 3, ','));         
         BOOL bOn = StrToBool(GetSubString(csS, 4, ','));
         BOOL bGenOn = StrToBool(GetSubString(csS, 5, ','));
         double dGenMin = StrToD(GetSubString(csS, 6, ','));
         double dGenMax = StrToD(GetSubString(csS, 7, ','));
         BOOL bFixOn = StrToBool(GetSubString(csS, 8, ','));
         long lLineStyle = StrToInt(GetSubString(csS, 9, ','));
         long lLineWidth = StrToInt(GetSubString(csS, 10, ','));
         long lLineColor = StrToInt(GetSubString(csS, 11, ','));
         long lPattern = StrToInt(GetSubString(csS, 12, ','));
         long lPatternColor = StrToInt(GetSubString(csS, 13, ','));
         BOOL bTransparent = StrToBool(GetSubString(csS, 14, ','));
         
         if ( FunctionPermissionOk("OnDeclareViewLayerSettings") )  
            FireOnDeclareViewLayerSettings(csLastLVCView, lLayerIndex, sLayerName, lPosition,
                                           bOn, bGenOn, dGenMin, dGenMax, bFixOn, 
                                           lLineStyle, lLineWidth, lLineColor, 
                                           lPattern, lPatternColor, bTransparent);
             
      }   

// LVC  **********************************************************
    else if ((csToken == "LVN") || (csToken == "LVC"))
      {        // new                  // old
       CString csS = GetCmdToken(sCmd, 2);
                 
         
         CString sViewName = GetSubString(csS, 1, ',');         
         BOOL bDefaultView = StrToBool(GetSubString(csS, 2, ','));
         BOOL bLayerPriorities = StrToBool(GetSubString(csS, 3, ','));
         BOOL bProjPart = StrToBool(GetSubString(csS, 4, ','));
         double dRectX1 = StrToD(GetSubString(csS, 5, ','));
         double dRectY1 = StrToD(GetSubString(csS, 6, ','));
         double dRectX2 = StrToD(GetSubString(csS, 7, ','));
         double dRectY2 = StrToD(GetSubString(csS, 8, ','));

         csLastLVCView = sViewName;
         
         if ( FunctionPermissionOk("OnDeclareViewSettings") )
            FireOnDeclareViewSettings(sViewName, bDefaultView, bLayerPriorities, 
                                      bProjPart, dRectX1, dRectY1, dRectX2, dRectY2);            
      }   

// SEL  **********************************************************
    else if (csToken == "SEL")     
      {        
       CString csS = GetCmdToken(sCmd, 2);

       CString sLayerName = GetSubString(csS, 2, ',');
       double dP1 = StrToD(GetSubString(csS, 3, ','));
       double dP2 = StrToD(GetSubString(csS, 4, ','));
       double dP3 = StrToD(GetSubString(csS, 5, ','));
       double dP4 = StrToD(GetSubString(csS, 6, ','));

       if (GetSubString(csS, 1, ',') == "0")         // BOX Selection
         {
          if ( FunctionPermissionOk("OnNotifyBoxSelection") )
             FireOnNotifyBoxSelection(sLayerName, dP1, dP2, dP3, dP4);
         }
       else
         {
          if ( FunctionPermissionOk("OnNotifyCircleSelection") )
             FireOnNotifyCircleSelection(sLayerName, dP1, dP2, dP3);
         }

      }

// LEX  **********************************************************
    else if (csToken == "LEX")     
      {
      csReturnString = sCmd;       // the complete cmd must be returned to
                                   // the LRQ method (DoFindLayerBy[Name|Index])
      csLastEvent = csToken;       // is a response on LRQ
      }

// CPY  **********************************************************
    else if ((csToken == "CPY") || (csToken == "COI"))
      { 
      
       iOffs = SetLayerToken(sCmd);
       csItem = GetCmdToken(sCmd,3+iOffs);
       if (GetSubStringCount(csItem, ',') > 1)  
          {      
           if (IsValidList(hParam1List))
              ClearList(hParam1List);
           if (IsValidList(hParam2List)) 
              ClearList(hParam2List);

           BuildDoubleListFromTokenString(sCmd, 3+iOffs, hParam1List, hParam2List);
           if ( FunctionPermissionOk("OnCopyObjects") )
              FireOnCopyObjects(hParam1List, hParam2List); 
          }

      }
 
// LIF  **********************************************************
    else if (csToken == "LIF")     
      { 
       
       csItem = GetCmdToken(sCmd,3);
       if (GetSubStringCount(csItem, ',') > 1) //Index available too?       
          {      
           if (IsValidList(m_layernameList))
              ClearList(m_layernameList);
           if (IsValidList(m_layerindexList)) 
              ClearList(m_layerindexList);

           BuildDoubleListFromTokenString(sCmd, 3, m_layernameList, m_layerindexList);
           m_layerinfo = LIF_INDEX;
          }
       else
          {
           DeleteList(m_layerindexList);
           m_layerindexList = -1;

           if (IsValidList(m_layernameList))
              ClearList(m_layernameList);

           AddListFromString(sCmd, 3, m_layernameList);
           m_layerinfo = LIF_NAME;
          }

       Debug.Print(CString("LayerInfo: ") + GetLayerInfo());
       DumpList(m_layernameList);
       DumpList(m_layerindexList);       

      }

// GSA  **********************************************************
    else if (csToken == "GSA")     
      {
       CString csActivity = GetCmdToken(sCmd,2);

       if ( FunctionPermissionOk("OnGraphicStatus") )
          FireOnGraphicStatus(csActivity);
      }

// ALA  **********************************************************
    else if (csToken == "ALA")     
      {
       csReturnString = GetCmdToken(sCmd, 2);
       csLastEvent = csToken;

       if ( FunctionPermissionOk("OnActiveLayer") )
         FireOnActiveLayer(csReturnString);
      }

// SCS  **********************************************************
// is answer to RCS
    else if (csToken == "SCS")
      {
       csReturnString = GetCmdToken(sCmd, 2);
       csLastEvent = csToken;
      }
// SPA  ***********************************************************
// is answer to GPA	
	else if (csToken == "SPA")
	{
		csReturnString = GetCmdToken(sCmd, 2);
		csLastEvent = csToken;
	}

// NPL  **********************************************************
// is answer to CPL
    else if (csToken == "NPL")
      {
       csReturnString = GetCmdToken(sCmd, 2);
       csLastEvent = csToken;
      }

// CCO  **********************************************************
    else if (csToken == "CCO")     
      {
       csLastEvent = csToken;
      }

// GSL  **********************************************************
    else if (csToken == "GSL")     
      {
       CString csLayerName = GetCmdToken(sCmd, 2);
       BOOL bFixed = StrToBool(GetCmdToken(sCmd, 3));

       if ( FunctionPermissionOk("OnActiveLayerStatus") )
         FireOnActiveLayerStatus(csLayerName, bFixed);
      }

// GSP  **********************************************************
    else if (csToken == "GSP")     
      {
       double dLLX;
       double dLLY;
       double dURX;
       double dURY;

       dLLX = StrToD(GetCmdToken(sCmd, 2));
       dLLY = StrToD(GetCmdToken(sCmd, 3));
       dURX = StrToD(GetCmdToken(sCmd, 4));
       dURY = StrToD(GetCmdToken(sCmd, 5));

       if ( FunctionPermissionOk("OnVisibleMapArea") )
          FireOnVisibleMapArea(dLLX, dLLY, dURX, dURY);
      }
// FPP  **********************************************************
    else if (csToken == "FPP")
      {
       double dLLX;
       double dLLY;
       double dURX;
       double dURY;

       dLLX = StrToD(GetCmdToken(sCmd, 2));
       dLLY = StrToD(GetCmdToken(sCmd, 3));
       dURX = StrToD(GetCmdToken(sCmd, 4));
       dURY = StrToD(GetCmdToken(sCmd, 5));

       if ( FunctionPermissionOk("OnInvalidMapArea") )
          FireOnInvalidMapArea(dLLX, dLLY, dURX, dURY);
      }
      
// GSW  **********************************************************
    else if (csToken == "GSW")     
      {
       double dWidth = StrToD(GetCmdToken(sCmd, 2));

       if ( FunctionPermissionOk("OnVisibleMapWidth") )
          FireOnVisibleMapWidth(dWidth);
      }
// GSS  **********************************************************
    else if (csToken == "GSS")     
      {
       CString csSelState = GetCmdToken(sCmd,2);

       if ( FunctionPermissionOk("OnSelectionStatus") )
          FireOnSelectionStatus(csSelState);
      }
// GSX  **********************************************************
    else if (csToken == "GSX")     
      {
       CString csValue = GetCmdToken(sCmd,2);

       if ( FunctionPermissionOk("OnSelectionStatusXValue") )  
          FireOnSelectionStatusXValue(csValue);
      }
// GSY  **********************************************************
    else if (csToken == "GSY")     
      {
       CString csValue = GetCmdToken(sCmd,2);

       if ( FunctionPermissionOk("OnSelectionStatusYValue") )  
          FireOnSelectionStatusYValue(csValue);
      }
// GSU  **********************************************************
    else if (csToken == "GSU")     
      {
       CString csInfo = GetCmdToken(sCmd,2);

       if ( FunctionPermissionOk("OnSelectionStatusXInfo") )  
          FireOnSelectionStatusXInfo(csInfo);
      }
// GSV  **********************************************************
    else if (csToken == "GSV")     
      {
       CString csInfo = GetCmdToken(sCmd,2);

       if ( FunctionPermissionOk("OnSelectionStatusYInfo") )  
          FireOnSelectionStatusYInfo(csInfo);
      }

// GSD  **********************************************************
    else if (csToken == "GSD")     
      {
       CString csDistance = GetCmdToken(sCmd,2);

#ifdef _DEBUGWIN_ON
       Debug.Print("Before OnDistance = " + csDistance );
#endif

       if ( FunctionPermissionOk("OnDistance") )  
          FireOnDistance(csDistance);

#ifdef _DEBUGWIN_ON
       Debug.Print("After OnDistance");
#endif

      }

// ++ Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#22 25.09.00
// PRNRES  *******************************************************
    else if (csToken == "PRNRES")     
      {
       
       csReturnString = GetCmdToken(sCmd, 2);
       hReturnList	 = StrToInt(GetCmdToken(sCmd, 3));
        
       if ( FunctionPermissionOk("OnPrintResult") )  
          FireOnPrintResult(csReturnString, hReturnList);

       csLastEvent = csToken;
      }

// PRNMON  *******************************************************
    else if (csToken == "PRNMON")     
      {
       
       long lPage	   = StrToInt(GetCmdToken(sCmd, 2));
       long lOfPages	= StrToInt(GetCmdToken(sCmd, 3));
       long lResult	= StrToInt(GetCmdToken(sCmd, 4));

       hReturnList = lResult;
        
       if ( FunctionPermissionOk("OnPrintEvents") )  
          FireOnPrintEvents(lPage, lOfPages, lResult);

       csLastEvent = csToken;
      }
// -- Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#22 25.09.00 

// ++ Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#23 09.10.00 
// PRNSPP  *******************************************************
    else if (csToken == "PRNSPP")     
      {
       
		 CString sDeviceName  = GetCmdToken(sCmd,2);
		 CString sPaperFormat = GetCmdToken(sCmd,3);
       long lOrientation	 = StrToInt(GetCmdToken(sCmd, 4));
       hReturnList	       = StrToInt(GetCmdToken(sCmd, 5));

       if ( FunctionPermissionOk("OnPrinterParamGet") )  
          FireOnPrinterParamGet(sDeviceName, sPaperFormat, lOrientation, hReturnList);

       csLastEvent = csToken;
      }

// PRNSPG  *******************************************************
    else if (csToken == "PRNSPG")     
      {
                      
       long lModel	      = StrToInt(GetCmdToken(sCmd, 2));
       long lRepaginate	   = StrToInt(GetCmdToken(sCmd, 3));
       long lHorizPages	   = StrToInt(GetCmdToken(sCmd, 4));
       long lVerticPages	= StrToInt(GetCmdToken(sCmd, 5));
       CString csMargins	= GetCmdToken(sCmd, 6);
			double dMrLeft		= StrToD(GetSubString(csMargins, 1, ','));
         double dMrBottom	= StrToD(GetSubString(csMargins, 2, ','));
         double dMrRight	= StrToD(GetSubString(csMargins, 3, ','));
         double dMrTop		= StrToD(GetSubString(csMargins, 4, ','));
       CString csInMargins	= GetCmdToken(sCmd, 7);
			double dInLeft		= StrToD(GetSubString(csInMargins, 1, ','));
         double dInBottom	= StrToD(GetSubString(csInMargins, 2, ','));
         double dInRight	= StrToD(GetSubString(csInMargins, 3, ','));
         double dInTop		= StrToD(GetSubString(csInMargins, 4, ','));
       CString csOvMargins	= GetCmdToken(sCmd, 8);
			double dOvLeft		= StrToD(GetSubString(csOvMargins, 1, ','));
         double dOvBottom	= StrToD(GetSubString(csOvMargins, 2, ','));
         double dOvRight	= StrToD(GetSubString(csOvMargins, 3, ','));
         double dOvTop		= StrToD(GetSubString(csOvMargins, 4, ','));
       long lNumbOrder		= StrToInt(GetCmdToken(sCmd, 9));
       long lNumbFrom		= StrToInt(GetCmdToken(sCmd, 10));
       hReturnList	      = StrToInt(GetCmdToken(sCmd, 11));

       if ( FunctionPermissionOk("OnPrintPagesParamGet") )  
          FireOnPrintPagesParamGet(lModel, lRepaginate, lHorizPages, lVerticPages,
											dMrLeft, dMrBottom, dMrRight, dMrTop,
											dInLeft, dInBottom, dInRight, dInTop,
											dOvLeft, dOvBottom, dOvRight, dOvTop,
											lNumbOrder, lNumbFrom, hReturnList);

       csLastEvent = csToken;
      }

// PRNSM  *******************************************************
    else if (csToken == "PRNSM")     
      {
                      
       long lMapID				= StrToInt(GetCmdToken(sCmd, 2));
       long lRegion				= StrToInt(GetCmdToken(sCmd, 3));
       CString sViewName		= GetCmdToken(sCmd, 4);
       CString csRegion			= GetCmdToken(sCmd, 5);
			double dRegionX1		= StrToD(GetSubString(csRegion, 1, ','));
         double dRegionY1		= StrToD(GetSubString(csRegion, 2, ','));
         double dRegionX2		= StrToD(GetSubString(csRegion, 3, ','));
         double dRegionY2		= StrToD(GetSubString(csRegion, 4, ','));
       BOOL bFitToPage			= StrToBool(GetCmdToken(sCmd, 6));
       double dPrintScale		= StrToD(GetCmdToken(sCmd, 7));
       BOOL bKeepScale			= StrToBool(GetCmdToken(sCmd, 8));
       CString csFrameSize		= GetCmdToken(sCmd, 9);
			double dFrameWidth	= StrToD(GetSubString(csFrameSize, 1, ','));
         double dFrameHeight	= StrToD(GetSubString(csFrameSize, 2, ','));
       long lFrameCentering	= StrToInt(GetCmdToken(sCmd, 10));
       CString csFramePosition= GetCmdToken(sCmd, 11);
			double dFrameLeft		= StrToD(GetSubString(csFramePosition, 1, ','));
         double dFrameTop		= StrToD(GetSubString(csFramePosition, 2, ','));
       hReturnList				= StrToInt(GetCmdToken(sCmd, 12));

       if ( FunctionPermissionOk("OnPrintMapParamGet") )  
          FireOnPrintMapParamGet(lMapID, lRegion, sViewName,
											dRegionX1, dRegionY1, dRegionX2, dRegionY2,
											bFitToPage, dPrintScale, bKeepScale,
											dFrameWidth, dFrameHeight, lFrameCentering,
											dFrameLeft, dFrameTop, hReturnList);

       csLastEvent = csToken;
      }

// PRNSL  *******************************************************
    else if (csToken == "PRNSL")     
      {
                      
       long lLegendID			= StrToInt(GetCmdToken(sCmd, 2));
       CString sLegendName		= GetCmdToken(sCmd, 3);
       CString csFrameSize		= GetCmdToken(sCmd, 4);
			double dFrameWidth	= StrToD(GetSubString(csFrameSize, 1, ','));
         double dFrameHeight	= StrToD(GetSubString(csFrameSize, 2, ','));
       long lFrameCentering	= StrToInt(GetCmdToken(sCmd, 5));
       long lFrameOridgin		= StrToInt(GetCmdToken(sCmd, 6));
       CString csFramePosition= GetCmdToken(sCmd, 7);
			double dFrameLeft		= StrToD(GetSubString(csFramePosition, 1, ','));
         double dFrameTopOrBot= StrToD(GetSubString(csFramePosition, 2, ','));
       hReturnList				= StrToInt(GetCmdToken(sCmd, 8));

       if ( FunctionPermissionOk("OnPrintLegendParamGet") )  
          FireOnPrintLegendParamGet(lLegendID, sLegendName,
			                           dFrameWidth, dFrameHeight,
			                           lFrameCentering, lFrameOridgin,
											   dFrameLeft, dFrameTopOrBot, hReturnList);

       csLastEvent = csToken;
      }
// -- Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#23 09.10.00 

// ++ Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#24 23.10.00 
// PRNSP  *******************************************************
    else if (csToken == "PRNSP")     
      {
                      
       long lPictureID			= StrToInt(GetCmdToken(sCmd, 2));
       CString sPictureFile	= GetCmdToken(sCmd, 3);
       long lPictureLocation	= StrToInt(GetCmdToken(sCmd, 4));
       BOOL bKeepAspect			= StrToBool(GetCmdToken(sCmd, 5));
       CString csFrameSize		= GetCmdToken(sCmd, 6);
			double dFrameWidth	= StrToD(GetSubString(csFrameSize, 1, ','));
         double dFrameHeight	= StrToD(GetSubString(csFrameSize, 2, ','));
       long lFrameCentering	= StrToInt(GetCmdToken(sCmd, 7));
       long lFrameOridgin		= StrToInt(GetCmdToken(sCmd, 8));
       CString csFramePosition= GetCmdToken(sCmd, 9);
			double dFrameLeft		= StrToD(GetSubString(csFramePosition, 1, ','));
         double dFrameTopOrBot= StrToD(GetSubString(csFramePosition, 2, ','));
       hReturnList				= StrToInt(GetCmdToken(sCmd, 10));

       if ( FunctionPermissionOk("OnPrintPictureParamGet") )  
          FireOnPrintPictureParamGet(lPictureID, sPictureFile, 
												lPictureLocation, bKeepAspect,
			                           dFrameWidth, dFrameHeight,
			                           lFrameCentering, lFrameOridgin,
											   dFrameLeft, dFrameTopOrBot, hReturnList);

       csLastEvent = csToken;
      }

// PRNSS  *******************************************************
    else if (csToken == "PRNSS")     
      {
                      
       long lSignatureID		= StrToInt(GetCmdToken(sCmd, 2));
       CString sSignature		= GetCmdToken(sCmd, 3);
       CString sFontName		= GetCmdToken(sCmd, 4);
       CString csFontStyle		= GetCmdToken(sCmd, 5);
			BOOL bItalic			= StrToBool(GetSubString(csFontStyle, 1, ','));
			BOOL bBold				= StrToBool(GetSubString(csFontStyle, 2, ','));
			BOOL bUnderlined		= StrToBool(GetSubString(csFontStyle, 3, ','));
			BOOL bTransparent		= StrToBool(GetSubString(csFontStyle, 4, ','));
       long lFontSize			= StrToInt(GetCmdToken(sCmd, 6));
       CString csFontColor		= GetCmdToken(sCmd, 7);
			long lFontColor		= StrToInt(GetSubString(csFontColor, 1, ','));
			long lFontBackColor	= StrToInt(GetSubString(csFontColor, 2, ','));
       long lTextAlignment		= StrToInt(GetCmdToken(sCmd, 8));
       BOOL bOnPagesLocation	= StrToBool(GetCmdToken(sCmd, 9));
       CString csLocation		= GetCmdToken(sCmd, 10);
			BOOL bOnFirstPage		= StrToBool(GetSubString(csLocation, 1, ','));
			BOOL bOnInternalPages= StrToBool(GetSubString(csLocation, 2, ','));
			BOOL bOnLastPage		= StrToBool(GetSubString(csLocation, 3, ','));
       long lFrameCentering	= StrToInt(GetCmdToken(sCmd, 11));
       long lFrameOridgin		= StrToInt(GetCmdToken(sCmd, 12));
       CString csFramePosition= GetCmdToken(sCmd, 13);
			double dFrameLeft		= StrToD(GetSubString(csFramePosition, 1, ','));
         double dFrameTopOrBot= StrToD(GetSubString(csFramePosition, 2, ','));
       hReturnList				= StrToInt(GetCmdToken(sCmd, 14));

       if ( FunctionPermissionOk("OnPrintSignatureParamGet") )  
          FireOnPrintSignatureParamGet(lSignatureID, sSignature, sFontName,
												bItalic, bBold, bUnderlined, bTransparent,
			                           lFontSize, lFontColor, lFontBackColor,
												lTextAlignment, bOnPagesLocation,
												bOnFirstPage, bOnInternalPages, bOnLastPage,
			                           lFrameCentering, lFrameOridgin,
											   dFrameLeft, dFrameTopOrBot, hReturnList);

       csLastEvent = csToken;
      }
// PRNSOI  *******************************************************
    else if (csToken == "PRNSOI")     
      {
                      
       long lObjectsNumber		= StrToInt(GetCmdToken(sCmd, 2));
		 long lMapsNumber			= 0;
		 long lLegendsNumber		= 0;
		 long lPicturesNumber	= 0;
		 long lSignaturesNumber	= 0;
       CString csTypesList		= GetCmdToken(sCmd, 3);
		 long hTypesList			= CreateList();
		 long hObjectsIDList		= CreateList();
       hReturnList				= StrToInt(GetCmdToken(sCmd, 4));

		 long i						= 0;
		 CString sType				= '0';
		 long lErrorsNumber		= 0;

		 if (hReturnList == 0)
			 while ( i < lObjectsNumber)
				{
				 i++;
				 sType = GetSubString(csTypesList, i, ',');
				 AddStringParam(hTypesList, sType);
				 switch( StrToInt(sType) )
					{
					 case 0 : AddStringParam(hObjectsIDList, IntToStr(lMapsNumber));
								 lMapsNumber++;
								 break;
					 case 1 : AddStringParam(hObjectsIDList, IntToStr(lLegendsNumber));
								 lLegendsNumber++;
								 break;
					 case 2 : AddStringParam(hObjectsIDList, IntToStr(lPicturesNumber));
								 lPicturesNumber++;
								 break;
					 case 3 : AddStringParam(hObjectsIDList, IntToStr(lSignaturesNumber));
								 lSignaturesNumber++;
								 break;
					 default: AddStringParam(hObjectsIDList, IntToStr(lErrorsNumber));
								 lErrorsNumber++;
								 hReturnList = 18;	// WinGIS Internal ERROR (for developers only)
								 break;
					}

				}

       if ( FunctionPermissionOk("OnPrintObjectsInformGet") )  
          FireOnPrintObjectsInformGet(lObjectsNumber, lMapsNumber, lLegendsNumber,
												lPicturesNumber, lSignaturesNumber,
												hTypesList, hObjectsIDList, hReturnList); 

       csLastEvent = csToken;
      }
// -- Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#24 03.11.00 

// SOB  **********************************************************
    else if (csToken == "SOB")
      {
       // SOB can come in Blocks of 200 IDs followed by an [END][]
       // we must add all ONEs to a list and wait for the final END
       // its only a response to IOS, no event is fired

       if (csBlockEvent == csToken)   
         {
          AddListFromString(sCmd, 3, hList);
         }
       else    // First of a SOB Block
         {
          csBlockEvent = csToken;
          hList = BuildListFromString(sCmd, 3);
          if (!IsValidList(hList))             
             FireOnDDEExecute(sCmd);// Fire only if unable to create a new list     
         }
      }

// PRL  **********************************************************
    else if (csToken == "PRL")
      {
       // we must add all projectnames from LOA to a list and wait for the final END
       // its only a response to LOA, no event is fired

       if (csBlockEvent != csToken)   
         {
          csBlockEvent = csToken;
          
          if (IsValidList(hList))
            ClearList(hList);
          else
            hList = CreateList();          
         }

       AddStringParam(hList, GetCmdToken(sCmd,2));
      }

// GPSID  **********************************************************
    else if (csToken == "GPSID")     
      {
       CString sGpsID    = GetCmdToken(sCmd,2);
       CString sProgisID = GetCmdToken(sCmd,3);

       if ( FunctionPermissionOk("OnGPSID") )  
          FireOnGPSID(sGpsID, sProgisID);
      }
// GPSPROASC  **********************************************************
    else if (csToken == "GPSPROASC")     
      {
       CString sStateType    = GetCmdToken(sCmd,2);
       CString sStateValue   = GetCmdToken(sCmd,3);

       if ( FunctionPermissionOk("OnGPSProAsc") )  
          FireOnGPSProAsc(sStateType, sStateValue);
      }
// GPSPROCALC  **********************************************************
    else if (csToken == "GPSPROCALC")     
      {
       double dX = StrToD(GetCmdToken(sCmd,2));
       double dY = StrToD(GetCmdToken(sCmd,3));

       if ( FunctionPermissionOk("OnGPSProCalc") )  
          FireOnGPSProCalc(dX, dY);
      }
// GPSSYMSET  **********************************************************
    else if (csToken == "GPSSYMSET")     
      {
       long lSymbolID = StrToInt(GetCmdToken(sCmd,2));       

       if ( FunctionPermissionOk("OnGPSSymSet") )  
          FireOnGPSSymSet(lSymbolID);
      }

// VER  **********************************************************
    else if (csToken == "VER")     
      {
       m_csVersion = GetCmdToken(sCmd,2);
      }

// PPR  **********************************************************
    else if (csToken == "PPR")  // OnPolygonPartition   
      {
       if (!(csBlockEvent == csToken))
         {
          csBlockEvent = csToken;
          if (IsValidList(hParam1List)) DeleteList(hParam1List);
          hParam1List = CreateList();
         }  

       AddStringParam(hParam1List, sCmd);         
      }

// TSY  **********************************************************
    else if (csToken == "TSY")     
      {
       if (!(csBlockEvent == csToken))
         {
          csBlockEvent = csToken;
          if (IsValidList(hParam1List)) DeleteList(hParam1List);
          hParam1List = CreateList();
         }  

       AddStringParam(hParam1List, sCmd);         
      }

// SPD, EPD  **********************************************************
// ++ Slava2 AXWinGIS .BUILD#24 Event of receiving all corner point of polygon 23.10.00    
//	else if ((csToken == "SPD") || (csToken == "EPD"))
//      {
//#ifdef _DEBUGWIN_ON
//       Debug.Print("PolygonData is not handled by AXWingis");
//#endif
//       ;
//      }
    else if (csToken == "SPD")     
      {        
       if (!(csBlockEvent == csToken))
         {
          csBlockEvent = csToken;
          if (IsValidList(hParam1List)) DeleteList(hParam1List);
          hParam1List = CreateList();
         }  

       AddStringParam(hParam1List, sCmd);         
      }   
	else if (csToken == "EPD")     
      { //1       
       if (csBlockEvent == "SPD")
         {//2
          csBlockEvent = csToken;
          if (IsValidList(hParam1List))
          {//3
			  long hID;
			  double dArea;
			  CString sLayer;
			  long dX = CreateList();
			  long dY = CreateList();
			  long hNum;
			  long iNum; 
			  long iListSize;
			  long i = 0;


				
			  iListSize = GetListSize (hParam1List);

	      while ( i != iListSize)
                    {//4
                     
                     CString csEPD = GetStringParam(hParam1List, i);
               
                     hID = StrToInt(GetCmdToken(csEPD, 3));
                     dArea =  StrToD(GetCmdToken(csEPD, 4));
                     sLayer =  (GetCmdToken(csEPD, 2));
					 iNum = 6;
					 hNum = (StrToInt(GetCmdToken(csEPD, 6))) + 6;
					 while (iNum != hNum)
					 {//5
						 iNum = iNum + 1;

						 CString str  = GetCmdToken(csEPD, iNum);
						 CString X = GetSubString(str, 1, *this->m_csDDESeparator.GetBuffer(1));
						 CString Y = GetSubString(str, 2, *this->m_csDDESeparator.GetBuffer(1));
						 sscanf (X, " %s," , X);
						 sscanf (Y, " %s," , Y);
						 AddStringParam(dX, X);
						 AddStringParam(dY, Y);
						 
					 }//5
					 i= i + 1;
                                       
                    
                    }//4
			  FireOnSendPolygonData (hID, dArea, sLayer, dX, dY);

			  DeleteList (dX);
			  DeleteList (dY);

			  DeleteList(hParam1List);

		  }//3
		  
         }//2  

       DeleteList(hParam1List);         
      } //1

	else if(csToken = "SATT")
	{
		if(!(csBlockEvent == csToken))
		{
			csBlockEvent = csToken;
			if(IsValidList(hParam1List)) DeleteList(hParam1List);
			hParam1List = CreateList();
		}
		AddStringParam(hParam1List, sCmd);
	}

// -- Slava2 AXWinGIS .BUILD#24 Event of receiving all corner point of polygon 23.10.00
// ???  **********************************************************
    else
	  {
      try
        {
         LockInPlaceActive(TRUE); 
         if ( FunctionPermissionOk("OnDDEExecute") )  
           FireOnDDEExecute(sCmd);
	      LockInPlaceActive(FALSE);
        }
      catch(CException)
        {
         LockInPlaceActive(FALSE);
        }

      }

#ifdef _DEBUGWIN_ON
    Debug.Print((const char*)(CString("End PARSE: ") + sCmd));
#endif
    
}

/////////////////////////////////////////////////////////////////////////////
// CAXWingisCtrl methods

BOOL CAXWingisCtrl::DDEExecute(LPCTSTR pCmd) 
{
	// TODO: Add your dispatch handler code here
    if (m_active)
	  {
	   return SendToWinGIS(pCmd);
	  }
	else
      return FALSE;
}



long CAXWingisCtrl::CreateList() 
{
  long iNewIndex;
  CStringArray* pList;
    
    try
     {
      pList = NULL;
      pList = new CStringArray;    
      pList->SetSize(0,10);
	    iNewIndex = aBase.Add(pList);
      if (iNewIndex == 0)     // don't allow zero handles
        {
         iNewIndex = aBase.Add(pList);
         aBase.SetAt(0, NULL);
        }

     }
    catch(CException)
     {
      iNewIndex = -1;
     }
    if ((iNewIndex == -1) && (pList != NULL))
      {
       delete pList;
      }

    return iNewIndex;
}

long CAXWingisCtrl::CreateListEx(long hList) 
{
		// TODO: Add your dispatch handler code here
    long iNewIndex;
    CStringArray* pNewList;
    CStringArray* pList;
    
    try
     {
      pNewList = NULL;
      pNewList = new CStringArray;    
      if ((aBase.GetSize() > hList) && (hList >= 0))
        {
         pList = (CStringArray*) aBase.GetAt(hList);
         if (pList != NULL)
           {
            pNewList->SetSize(0,10);
            pNewList->Copy(*pList);
           }
        } 
      else 
        pNewList->SetSize(0,10);

	  iNewIndex = aBase.Add(pNewList);
     }
    catch(CException)
     {
      iNewIndex = -1;
     }
    if ((iNewIndex == -1) && (pNewList != NULL))
      {
       delete pNewList;
      }

    return iNewIndex;
}

// New list item handling functions since 1.1.0.30
// Supported types now include long and double too

// redundant, should be removed at next major upgrade
BOOL CAXWingisCtrl::AddStringParam(long hList, LPCTSTR Value) 
{
return AddListItemString(hList, Value);
}
BOOL CAXWingisCtrl::DeleteStringParam(long hList, long Index) 
{
return DeleteListItem(hList, Index);
}
BSTR CAXWingisCtrl::GetStringParam(long hList, long Index) 
{
return GetListItemString(hList, Index);
}
// EO redundant

BOOL CAXWingisCtrl::AddListItemString(long hList, LPCTSTR sValue) 
{
CStringArray* pList = NULL;
CString pString;
BOOL bRet = FALSE;	
    
	if(strcmp(sValue,"") !=0)
    // TODO: Add your dispatch handler code here
    if ((aBase.GetSize() > hList) && (hList >= 0))
      {
       pList = (CStringArray*) aBase.GetAt(hList);
       if (pList != NULL)
         {
          try
           {
            //pString = new CString;
            pString = sValue;
            pList->Add(pString);
            bRet = TRUE;
           }
          catch(CException) { bRet = FALSE; }
         }
      }

	return bRet;
}

BOOL CAXWingisCtrl::AddListItemLong(long hList, long lValue) 
{
	return AddListItemString(hList, IntToStr(lValue));
}

BOOL CAXWingisCtrl::AddListItemDouble(long hList, double dValue) 
{
	return AddListItemString(hList, DToStr(dValue));
}

BSTR CAXWingisCtrl::GetListItemString(long hList, long lIndex) 
{
CString strResult;
CStringArray* pList = NULL;
BOOL bRet = FALSE;	
    
    strResult = "";

    if ((aBase.GetSize() > hList) && (hList >= 0))
      {
       pList = (CStringArray*) aBase.GetAt(hList);
       if (pList != NULL)
         {
          try
           {
            if ((pList->GetSize() > lIndex) && (lIndex >= 0))
              strResult = (CString)pList->GetAt(lIndex);            
           }
          catch(CException) { }
         }
      }

	return strResult.AllocSysString();
}

long CAXWingisCtrl::GetListItemLong(long hList, long lIndex) 
{
	long lRes = 0;
  CString csRes;

  try
    {
     csRes = GetListItemString(hList, lIndex);
     lRes = StrToInt(csRes);
    }
  catch(CException) { }

  return lRes;
}

double CAXWingisCtrl::GetListItemDouble(long hList, long lIndex) 
{
	double dRes = 0;

  try
    {
     dRes = StrToD(GetListItemString(hList, lIndex));
    }
  catch(CException) { }

  return dRes;
}

BOOL CAXWingisCtrl::DeleteListItem(long hList, long lIndex) 
{
CStringArray* pList = NULL;
BOOL bRet = FALSE;	

    if ((aBase.GetSize() > hList) && (hList >= 0))
      {
       pList = (CStringArray*) aBase.GetAt(hList);
       if (pList != NULL)
         {
          try
           {
            if ((pList->GetSize() > lIndex) && (lIndex >= 0))
              {
               pList->RemoveAt(lIndex);
               bRet = TRUE;
              }              
           }
          catch(CException) { }
         }
      }

	return bRet;
}
// EO New list item handling functions since 1.1.0.30


// does a bubble-sort on the list hList
void CAXWingisCtrl::SortList(long hList, BOOL bAsc) 
{
  unsigned long i;
  CStringArray* pList = NULL;
  CString pString;
  long lRet = -1;	
  unsigned long lListSize;
  bool bModified = TRUE;
  CString csBuffer = "";
  CString * pcsBuffer;


    if ((aBase.GetSize() > hList) && (hList >= 0))
      {
       pList = (CStringArray*) aBase.GetAt(hList);
       if (pList != NULL)
         {
          try
           {          
            pcsBuffer = pList->GetData( );
 
            lListSize = pList->GetSize();
            // loop until no more need to modify, ie. the sort is finished
            while (bModified == TRUE)
             {
              bModified = FALSE;
              // loop through the whole list
              for (i=0;(i < lListSize-1);i++)
                {
                 // test if the two items need to be exchanged
                 //if (CompareListItems( pList->GetAt(i), pList->GetAt(i+1), bAsc) )
                 if (CompareListItems(*(pcsBuffer+i), *(pcsBuffer+i+1), bAsc) )
                   {
                    // swap the two items
                    csBuffer = pList->GetAt(i+1);
                    pList->SetAt(i+1, pList->GetAt(i));                    
                    pList->SetAt(i, csBuffer);
                    
                  /*
                    memcpy(&csBuffer, (pcsBuffer+i), sizeof(CString));
                    memcpy((pcsBuffer+i), (pcsBuffer+i+1), sizeof(CString));
                    memcpy((pcsBuffer+i+1), &csBuffer, sizeof(CString));
                  */

                    bModified = TRUE;
                   }               
                }
              // loop reverse through the whole list 
              if (bModified)
                {
                bModified = FALSE; 
                for (i=lListSize-2;(i > 0);i--)
                  {
                   // test if the two items need to be exchanged
                   //if (CompareListItems(pList->GetAt(i), pList->GetAt(i+1), bAsc) )
                   if (CompareListItems(*(pcsBuffer+i), *(pcsBuffer+i+1), bAsc) )
                     {
                      // swap the two items
                      csBuffer = pList->GetAt(i+1);
                      pList->SetAt(i+1, pList->GetAt(i));                    
                      pList->SetAt(i, csBuffer);


                      /*
                      memcpy(&csBuffer, (pcsBuffer+i), sizeof(CString));
                      memcpy((pcsBuffer+i), (pcsBuffer+i+1), sizeof(CString));
                      memcpy((pcsBuffer+i+1), &csBuffer, sizeof(CString));
                      */

                      bModified = TRUE; 
                     }               
                  }
                }

             }
           }
          catch(CException) { ; }
         }
      }

	return;
}

// compare the two strings and return TRUE if they need to be exchanged
// depends on bAsc (ascending or descending sort order)
__inline bool CAXWingisCtrl::CompareListItems(const char * cs1, const char * cs2, BOOL bAsc)
{
 unsigned long ul1;
 unsigned long ul2;
 bool bIsNumeric1, bIsNumeric2;
 bool bSwap = FALSE;
 char *stopstring;


  __try {        
       ul1 = strtol(cs1, &stopstring, 10);
       //ul1 = atol(cs1);
       bIsNumeric1 = TRUE;
     }
  __except(EXCEPTION_EXECUTE_HANDLER) {
      ul1 = 0;
      bIsNumeric1 = FALSE;
     }
  __try {        
       ul2 = strtol(cs2, &stopstring, 10);
       //ul2 = atol(cs2);
       bIsNumeric2 = TRUE;
     }
  __except(EXCEPTION_EXECUTE_HANDLER) {
      ul2 = 0;
      bIsNumeric2 = FALSE;
     }

  // both are numeric ?
  if (bIsNumeric1 && bIsNumeric2)
    {
     if (ul1 > ul2)
       {
        bSwap = TRUE;
       }
    }  
  // only 2 is numeric
  if (! bIsNumeric1 && bIsNumeric2)
    {
     // i want alphas to bubble to the end if Ascending
     bSwap = TRUE;
    }

  // sort order should be descending ? 
  if (! bAsc)
    {
     bSwap = ! bSwap;  // turn around
    }

  return bSwap;
}

BOOL CAXWingisCtrl::DeleteList(long hList) 
{
CStringArray* pList = NULL;
BOOL bRet = FALSE;	
    
    // TODO: Add your dispatch handler code here
    if ((aBase.GetSize() > hList) && (hList >= 0))
      {
       pList = (CStringArray*) aBase.GetAt(hList);
       if (pList != NULL)
         {
          try
           {
            ClearList(hList);
            //pList->RemoveAll();
            aBase.SetAt(hList, NULL);
            delete pList;
            bRet = TRUE;
           }
          catch(CException) { bRet = FALSE; }
         }
      }

	return bRet;
}

BOOL CAXWingisCtrl::IsValidList(long hList) 
{
CStringArray* pList = NULL;
BOOL bRet = FALSE;	
    
    // TODO: Add your dispatch handler code here
    if ((aBase.GetSize() > hList) && (hList >= 0))
      {
       pList = (CStringArray*) aBase.GetAt(hList);
       if (pList != NULL)
         {
          bRet = TRUE;
         }
      }

	return bRet;
}

BOOL CAXWingisCtrl::ClearList(long hList) 
{
CStringArray* pList = NULL;
//CString* pString;
BOOL bRet = FALSE;	
    

    if ((aBase.GetSize() > hList) && (hList >= 0))
      {
       pList = (CStringArray*) aBase.GetAt(hList);
       if (pList != NULL)
         {
          try
           {
            /*for (i=0;i < pList->GetSize();i++)
              {
               pString = (CString*) pList->GetAt(i);
               delete pString;
              }*/
            pList->RemoveAll();
            bRet = TRUE;
           }
          catch(CException) { bRet = FALSE; }
         }
      }

	return bRet;
}

void CAXWingisCtrl::DumpList(long hList) 
{
CStringArray* pList = NULL;
CString pString;
BOOL bRet = FALSE;	
INT i;
    
    // TODO: Add your dispatch handler code here
    if ((aBase.GetSize() > hList) && (hList >= 0))
      {
       pList = (CStringArray*) aBase.GetAt(hList);
       if (pList != NULL)
         {
          try
           {
            Debug.Print("DumpList " + IntToStr(hList));
            for (i=0;i < pList->GetSize();i++)
              {
               pString = (CString) pList->GetAt(i);
               Debug.Print(pString);
              }
           }
          catch(CException) { }
         }
       else
         Debug.Print("DumpList " + IntToStr(hList) + "  INVALID");  
      }
    else
      Debug.Print("DumpList " + IntToStr(hList) + "  INVALID");  

	return;
}


long CAXWingisCtrl::GetListSize(long hList) 
{
CStringArray* pList = NULL;
BOOL bRet = -1;	
    
    // TODO: Add your dispatch handler code here
    if ((aBase.GetSize() > hList) && (hList >= 0))
      {
       pList = (CStringArray*) aBase.GetAt(hList);
       if (pList != NULL)
         {
          bRet = pList->GetSize();
         }
      }

	return bRet;
}

long CAXWingisCtrl::FindString(long hList, CString csSearch) 
{
CStringArray* pList = NULL;
CString pString;
long lRet = -1;	
INT i;
    
    // TODO: Add your dispatch handler code here
    if ((aBase.GetSize() > hList) && (hList >= 0))
      {
       pList = (CStringArray*) aBase.GetAt(hList);
       if (pList != NULL)
         {
          try
           {          
            for (i=0;(i < pList->GetSize()) && (lRet == -1);i++)
              {
               if (csSearch == (CString) pList->GetAt(i))
                 {
                  lRet = i;
                 }               
              }
           }
          catch(CException) { lRet = -1; }
         }
      }
	return lRet;
}

///////////////////////////////////////////////////////////////////////////////
/////////////// METHODS ///////////////////////////////////////////////////////

BOOL CAXWingisCtrl::DoZoomObjects(long hProgisIDList, BOOL bAutoCommit) 
{
BOOL bRet = FALSE;	
long hBlock;
long lIndex;
CString sCmd;

 if ( ! FunctionPermissionOk("DoZoomObjects"))
    return FALSE;
    
 if (m_connected)
   {  
    if ((aBase.GetSize() > hProgisIDList) && (hProgisIDList >= 0))
      {
         
       hBlock = BuildCmdBlockFromList(hProgisIDList);
       if (hBlock > -1)
         {
          bRet = TRUE;
          for (lIndex=0; (lIndex < GetListSize(hBlock)) && bRet; lIndex++)
            {
             sCmd = CString("[ZOM]") + GetLayerToken() + GetStringParam(hBlock, lIndex);
             bRet = SendToWinGIS(sCmd);             
            }
          DeleteList(hBlock);
         }       
       if (bAutoCommit)
          bRet = SendToWinGIS("[END][]");
      }
   }
 return bRet;
}

BOOL CAXWingisCtrl::DoZoomObjectsD(long hProgisIDList, BOOL bAutoCommit) 
{
BOOL bRet = FALSE;	
long hBlock;
long lIndex;
CString sCmd;

 if ( ! FunctionPermissionOk("DoZoomObjectsD"))
    return FALSE;
    
 if (m_connected)
   {  
    if ((aBase.GetSize() > hProgisIDList) && (hProgisIDList >= 0))
      {
         
       hBlock = BuildCmdBlockFromList(hProgisIDList);
       if (hBlock > -1)
         {
          bRet = TRUE;
          for (lIndex=0; (lIndex < GetListSize(hBlock)) && bRet; lIndex++)
            {
             sCmd = CString("[ZOD]") + GetLayerToken() + GetStringParam(hBlock, lIndex);
             bRet = SendToWinGIS(sCmd);
            }
          DeleteList(hBlock);
         }       

       if (bAutoCommit)
          bRet = SendToWinGIS("[END][]");
      }
   }
 return bRet;
}

BOOL CAXWingisCtrl::DoSelectObjects(long hProgisIDList, BOOL bAutoCommit) 
{
BOOL bRet = FALSE;	
CString sCmd;
long hBlock;
long lIndex;

 if ( ! FunctionPermissionOk("DoSelectObjects"))
    return FALSE;
    
 if (m_connected)
   {  
    if ((aBase.GetSize() > hProgisIDList) && (hProgisIDList >= 0))
      {
       hBlock = BuildCmdBlockFromList(hProgisIDList);
       if (hBlock > -1)
         {
          bRet = TRUE;
          for (lIndex=0; (lIndex < GetListSize(hBlock)) && bRet; lIndex++)
            {
             sCmd = CString("[FIT]") + GetLayerToken() + GetStringParam(hBlock, lIndex);
             bRet = SendToWinGIS(sCmd);       
            }
          DeleteList(hBlock);
         }       

       if (bAutoCommit)
          bRet = SendToWinGIS("[END][]");
      }
   }
 return bRet;
}

BOOL CAXWingisCtrl::DoSelectObjectsD(long hProgisIDList, BOOL bAutoCommit) 
{
BOOL bRet = FALSE;	
CString sCmd;
long hBlock;
long lIndex;

 if ( ! FunctionPermissionOk("DoSelectObjectsD"))
    return FALSE;
    
 if (m_connected)
   {  
    if ((aBase.GetSize() > hProgisIDList) && (hProgisIDList >= 0))
      {
       hBlock = BuildCmdBlockFromList(hProgisIDList);
       if (hBlock > -1)
         {
          bRet = TRUE;
          for (lIndex=0; (lIndex < GetListSize(hBlock)) && bRet; lIndex++)
            {
             sCmd = CString("[FID]") + GetLayerToken() + GetStringParam(hBlock, lIndex);
             bRet = SendToWinGIS(sCmd);
             Debug.Print((const char*)sCmd);
            }
          DeleteList(hBlock);
         }       
       if (bAutoCommit)
          bRet = SendToWinGIS("[END][]");
      }
   }
 return bRet;
}
BOOL CAXWingisCtrl::DoGetObjectProperties(long lObjectType, long hObjectList) 
{
BOOL bRet = FALSE;	
CString sCmd;
long hBlock;
long lIndex;

 if ( ! FunctionPermissionOk("DoGetObjectProperties"))
    return FALSE;
    
 if (m_connected)
   {
    if ((aBase.GetSize() > hObjectList) && (hObjectList >= 0))
      {
       hBlock = BuildCmdBlockFromList(hObjectList);
       if (hBlock > -1)
         {
          bRet = TRUE;
          for (lIndex=0; (lIndex < GetListSize(hBlock)) && bRet; lIndex++)
            {
             sCmd = CString("[TOP][") + IntToStr(lObjectType) + "]" + GetStringParam(hBlock, lIndex);
             bRet = SendToWinGIS(sCmd);
#ifdef _DEBUGWIN_ON
             Debug.Print((const char*)sCmd);
#endif
            }
          DeleteList(hBlock);
          csLastEvent = "";
          if (SendToWinGIS("[END][]"))
            {             
             WaitForReply("OBP");
             bRet = TRUE;
            }

         }       
      }
   }
 return bRet;
}

BOOL CAXWingisCtrl::DoCloseGIS() 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoCloseGIS"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[CLO][]");
	   this->DoDisconnect();
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoShowGIS() 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoShowGIS"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[SHW][]");
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoDeselectAll() 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoDeselectAll"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[DSA][]");
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoSetView(LPCTSTR sView) 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoSetView"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[SHV][" + CString(sView) + "]");
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoSetCaption(LPCTSTR sProgisID, long hColNameList, long hCaptionList, 
                                 BOOL bAutoCommit) 
{
CStringArray* pList = NULL;    // list of values
CStringArray* pCols = NULL;    // list of columns
BOOL bRet = FALSE;	
CString sString;
CString sCmd;
CString sID;
CString sColName = ""; 
CString sLastColName = "";
int i;
UINT iPadLen;
char acPad[4] = {32,32,0,0};
char acPadID[10] = {32,32,32,32,32,32,32,32,32,0};
char acBuf[4] = {0,0,0,0};

 if ( ! FunctionPermissionOk("DoSetCaption"))
    return FALSE;
    
 if (m_connected)
   {  
    if ((aBase.GetSize() > hCaptionList) && (hCaptionList >= 0))
      {
       pList = (CStringArray*) aBase.GetAt(hCaptionList);
       
	   if ((aBase.GetSize() > hColNameList) && (hColNameList >= 0))
	     pCols = (CStringArray*) aBase.GetAt(hColNameList);

       if ((pList != NULL) && (pCols != NULL))
         {
          bRet = pList->GetSize();
		  sID = sProgisID;
          if (pList->GetSize() > 0)
            {
             // Set cmd token and targetlayer 
             sCmd = "[GBE]" + GetLayerToken() + "[";

			 iPadLen = sID.GetLength() - 1;
			 if (iPadLen > 9) iPadLen = 9;
             // Add padded ProgisID (to 10)
			 sCmd = sCmd + ((char *)(acPadID+iPadLen)) + sID + "][";

             for (i=0;i < pList->GetSize();i++)
               {
                // get value string i
                sString = (CString) pList->GetAt(i);
				if (i < pCols->GetSize())   // column string i available?
				  sColName = (CString) pCols->GetAt(i);
				else
				  sColName = sLastColName;  // no, use last column
                if (i > 0)
				  sCmd = sCmd + ",";

				sCmd = sCmd + sColName + "," + sString;
                
				sLastColName = sColName;    // set last column to current
                
               }
			 sCmd = sCmd + "]";
             bRet = SendToWinGIS(sCmd);
             Debug.Print((const char*)sCmd);
             if (bAutoCommit)
               SendToWinGIS("[END][]");

            }
         }
      }
   }
 return bRet;
}

BOOL CAXWingisCtrl::DoCreateChart(LPCTSTR sProgisID, long hListCOLs, long hListValues, BOOL bAutoCommit)
{
CStringArray* pList = NULL;    // list of values
CStringArray* pCols = NULL;    // list of columns
BOOL bRet = FALSE;	
CString sString;
CString sCmd;
CString sID;
CString sColName = ""; 
CString sLastColName = "";
int i;
UINT iPadLen;
char acPad[4] = {32,32,0,0};
char acPadID[10] = {32,32,32,32,32,32,32,32,32,0};
char acBuf[4] = {0,0,0,0};

 if ( ! FunctionPermissionOk("DoCreateChart"))
    return FALSE;
    
 if (m_connected)
   {  
    if ((aBase.GetSize() > hListValues) && (hListValues >= 0))
      {
       pList = (CStringArray*) aBase.GetAt(hListValues);
       
	   if ((aBase.GetSize() > hListCOLs) && (hListCOLs >= 0))
	     pCols = (CStringArray*) aBase.GetAt(hListCOLs);

       if ((pList != NULL) && (pCols != NULL))
         {
          bRet = pList->GetSize();
		  sID = sProgisID;
          if (pList->GetSize() > 0)
            {
             // Set cmd token and targetlayer 
             sCmd = "[DDD]" + GetLayerToken() + "[";

			 iPadLen = sID.GetLength() - 1;
			 if (iPadLen > 9) iPadLen = 9;
             // Add padded ProgisID (to 10)
			 sCmd = sCmd + ((char *)(acPadID+iPadLen)) + sID + "][";

             for (i=0;i < pList->GetSize();i++)
               {
                // get value string i
                sString = (CString) pList->GetAt(i);
				if (i < pCols->GetSize())   // column string i available?
				  sColName = (CString) pCols->GetAt(i);
				else
				  sColName = sLastColName;  // no, use last column
                if (i > 0)
				  sCmd = sCmd + ",";

				sCmd = sCmd + sColName + "," + sString;
                
				sLastColName = sColName;    // set last column to current
                
               }
			 sCmd = sCmd + "]";
             bRet = SendToWinGIS(sCmd);
             Debug.Print((const char*)sCmd);
             if (bAutoCommit)
               SendToWinGIS("[END][]");

            }
         }
      }
   }
 return bRet;
}

long CAXWingisCtrl::DoSearchObjects(long hProgisIDList, BOOL bAutoCommit) 
{
long lRet = FALSE;	
CString sCmd;
long hBlock;
long lIndex;

 if ( ! FunctionPermissionOk("DoSearchObjects"))
    return FALSE;
    
 if (m_connected)
   {  
    if ((aBase.GetSize() > hProgisIDList) && (hProgisIDList >= 0))
      {
       hBlock = BuildCmdBlockFromList(hProgisIDList);
       if (hBlock > -1)
         {
          Debug.Print("DoSearchObjects"); 
          lRet = TRUE;
          for (lIndex=0; (lIndex < GetListSize(hBlock)) && lRet; lIndex++)
            {
             sCmd = CString("[COE]") + GetLayerToken() + GetStringParam(hBlock, lIndex);
             lRet = SendToWinGIS(sCmd);
             Debug.Print((const char*)sCmd);
            }
          DeleteList(hBlock);
         }       

       if (bAutoCommit && lRet)
         {  
          lRet = -1; 
          if (SendToWinGIS("[END][]"))
            {
             csLastEvent = "";
             WaitForReply("ONE");
             lRet = hReturnList;
             hReturnList = -1;
            }
         }
       else
         lRet = -1;
      }
   }
 return lRet;
}

BOOL CAXWingisCtrl::DoOpenProject(LPCTSTR sProjectName) 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoOpenProject"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[OPR][" + CString(sProjectName) + "]");
       WaitForReplyTimeout("SOK", 500);
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoCloseProject(LPCTSTR sProjectName) 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoCloseProject"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[CPR][" + CString(sProjectName) + "]");
       WaitForReplyTimeout("SOK", 500);
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoSaveProject() 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoSaveProject"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[SPR][]");
       WaitForReplyTimeout("SOK", 500);
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoActivateProject(LPCTSTR sProjectName) 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoActivateProject"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[WPR][" + CString(sProjectName) + "]");
       WaitForReplyTimeout("SOK", 500);
      }
	return bRet;
}


BOOL CAXWingisCtrl::DoSetSelectionMode(long lMode) 
{
BOOL bRet = FALSE;
CString csMode;

    if ( ! FunctionPermissionOk("DoSetSelectionMode"))
       return FALSE;

    if (m_connected)
      {  
       csMode = IntToStr(lMode);
       bRet = SendToWinGIS("[SSL][" + csMode + "]");
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoDeleteObjects(long hProgisIDList, BOOL bAutoCommit) 
{
BOOL bRet = FALSE;	
CString sCmd;
long hBlock;
long lIndex;

 if ( ! FunctionPermissionOk("DoDeleteObjects"))
    return FALSE;
    
 if (m_connected)
   {  
    if ((aBase.GetSize() > hProgisIDList) && (hProgisIDList >= 0))
      {
       hBlock = BuildCmdBlockFromList(hProgisIDList);
       if (hBlock > -1)
         {
          bRet = TRUE;
          for (lIndex=0; (lIndex < GetListSize(hBlock)) && bRet; lIndex++)
            {
             sCmd = CString("[DEL]") + GetLayerToken() + GetStringParam(hBlock, lIndex);
             bRet = SendToWinGIS(sCmd);
             Debug.Print((const char*)sCmd);
            }
          DeleteList(hBlock);
         }       
       if (bAutoCommit)
          bRet = SendToWinGIS("[END][]");
      }
   }
 return bRet;
}

BOOL CAXWingisCtrl::DoSetObjectPos(LPCTSTR sProgisID, double dAbsX, double dAbsY, BOOL bAutoCommit)
{
BOOL bRet = FALSE;
CString sID;
CString sCmd;
CString sX, sY;
UINT iPadLen;
char acPadID[10] = {32,32,32,32,32,32,32,32,32,0};
char acBuf[4] = {0,0,0,0};

    if ( ! FunctionPermissionOk("DoSetObjectPos"))
       return FALSE;

    if (m_connected)
      {  
       sCmd = "[MOP]" + GetLayerToken() + "[";
       
       sID = sProgisID;
   	   iPadLen = sID.GetLength() - 1;
	   if (iPadLen > 9) iPadLen = 9;
       // Add padded ProgisID (to 10)
	   sCmd = sCmd + ((char *)(acPadID+iPadLen)) + sID + "][";

       sX = DToStr(dAbsX);
       sY = DToStr(dAbsY);
       sCmd = sCmd + sX + "," + sY + "]";

       bRet = SendToWinGIS(sCmd);
       
       if (bAutoCommit)
         bRet = SendToWinGIS("[END][]");
      }

	return bRet;
}
BOOL CAXWingisCtrl::DoMoveObject(LPCTSTR sProgisID, double dRelX, double dRelY, BOOL bAutoCommit)
{
BOOL bRet = FALSE;
CString sID;
CString sCmd;
CString sX, sY;
UINT iPadLen;
char acPadID[10] = {32,32,32,32,32,32,32,32,32,0};
char acBuf[4] = {0,0,0,0};

    if ( ! FunctionPermissionOk("DoMoveObject"))
       return FALSE;

    if (m_connected)
      {  
       sCmd = "[MOV]" + GetLayerToken() + "[";
       
       sID = sProgisID;
   	   iPadLen = sID.GetLength() - 1;
	   if (iPadLen > 9) iPadLen = 9;
       // Add padded ProgisID (to 10)
	   sCmd = sCmd + ((char *)(acPadID+iPadLen)) + sID + "][";

       sX = DToStr(dRelX);
       sY = DToStr(dRelY);
       sCmd = sCmd + sX + "," + sY + "]";

       bRet = SendToWinGIS(sCmd);

       if (bAutoCommit)
         bRet = SendToWinGIS("[END][]");
      }

	return bRet;
}

BSTR CAXWingisCtrl::DoCreateTextEx(double dX, double dY, LPCTSTR sFontName, long lFontStyle, long lHeight, long lArc, LPCTSTR sText) 
//BSTR CAXWingisCtrl::DoCreateTextEx(LPCTSTR sX, LPCTSTR sY, LPCTSTR sFontName, long lFontStyle, long lHeight, long lArc, LPCTSTR sText) 
{
CString strResult="";
CString sID;
CString sCmd;
CString csText;
BOOL bRet;

    if ( ! FunctionPermissionOk("DoCreateTextEx"))
       return FALSE;

    if (m_connected)
      {  

       csText = CString(sText).Left(254);   // max text len is only 255 chars
/***** mk - separator problem
       sCmd = "[TXT]" + GetLayerToken() + CString("[") + DToStr(dX) + CString(",") + DToStr(dY) + CString(",");
       sCmd = sCmd + sFontName + CString(",") + IntToStr(lFontStyle) + "," + IntToStr(lHeight) + ",";
       sCmd = sCmd + IntToStr(lArc) + "][" + csText + "]";
 */     
       sCmd = "[TXT]" + GetLayerToken() + "[" + DToStr(dX) + m_csDDESeparator + DToStr(dY) + m_csDDESeparator;
       sCmd = sCmd + sFontName + m_csDDESeparator + IntToStr(lFontStyle) + m_csDDESeparator + IntToStr(lHeight) + m_csDDESeparator;
       sCmd = sCmd + IntToStr(lArc) + "][" + csText + "]";

       if (csLastEvent == "NTX") csLastEvent = "";
       csReturnString = "";

       bRet = SendToWinGIS(sCmd);

       if (bRet)
         {  
          bRet = -1; 
          WaitForReplyTimeout("NTX", 4000);
          strResult = csReturnString;
          csReturnString = "";
         }       
      }
#ifdef _DEBUGWIN_ON
       Debug.Print("DoCreateText Result: " + strResult);
#endif

	return strResult.AllocSysString();
}

BOOL CAXWingisCtrl::DoSetText(LPCTSTR sText) 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoSetText"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[STX][" + CString(sText) + "]");
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoBeginMacro() 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoBeginMacro"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[MCB][]");
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoEndMacro() 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoEndMacro"))
       return FALSE;

    if (m_connected)
      {
       if (csLastEvent == "MAE") csLastEvent = "";
       bRet = SendToWinGIS("[MCE][]");
      }
	return bRet;
}


BOOL CAXWingisCtrl::WaitForMacroEnd(long lTimeOut) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("WaitForMacroEnd"))
      return FALSE;

   if (m_connected)
     {
       if (WaitForReplyTimeout("MAE", lTimeOut)) 
         {
          bRet = TRUE;
         }
     }

	return bRet;
}

BOOL CAXWingisCtrl::DoCreateLayerDialog() 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoCreateLayerDialog"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[LAA][]");
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoDeleteLayerDialog() 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoDeleteLayerDialog"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[LAD][]");
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoGetLayerSettings() 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoGetLayerSettings"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[LMR][]");
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoGetViewSettings() 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoGetViewSettings"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[LVR][]");
      }
	return bRet;
}

BSTR CAXWingisCtrl::DoLoadBitmap(LPCTSTR sFileName, double dP1X, double dP1Y, double dP2X, double dP2Y, BOOL bShow) 
{
BOOL bRet = FALSE;
CString strResult="";
CString csCmd;

    if ( ! FunctionPermissionOk("DoLoadBitmap"))
      {
       return strResult.AllocSysString();       
      }

    if (m_connected)
      {
		/*
       csCmd = CString("[LBM][") + sFileName;
       csCmd += "," + DToStr(dP1X) + "," + DToStr(dP1Y) + "," + DToStr(dP2X) + "," + DToStr(dP2Y) + ",";
       csCmd += BoolToStr(bShow) + "][1234]";
*/

       csCmd = CString("[LBM][") + sFileName;
       csCmd += m_csDDESeparator + DToStr(dP1X) + m_csDDESeparator + DToStr(dP1Y) + m_csDDESeparator + DToStr(dP2X) + m_csDDESeparator + DToStr(dP2Y) + m_csDDESeparator ;
       csCmd += BoolToStr(bShow) + "][1234]";

       if (csLastEvent == "GRD") 
         {
          csLastEvent = "";
          csReturnString = "";
         }

       bRet = SendToWinGIS(csCmd);       
       bRet = SendToWinGIS("[END][]");

       if (bRet)
         {             
          if (WaitForReplyTimeout("GRD", 12000))
            {
             strResult = csReturnString;
             csReturnString = "";
            }
         }       
      }
	return strResult.AllocSysString();
}


BOOL CAXWingisCtrl::DoChangeLayerSettingsEx(LPCTSTR sLayerName, LPCTSTR sNewName, long lOnOff, 
                                            long lPosition, long lGenOnOff, double dGenMin, double dGenMax, 
                                            long lFixOnOff, long lLineStyle, long lLineWidth, 
                                            long lLineColor, long lPattern, long lPatternColor, 
                                            long lTransparent) 
{
long lIndex;
CString csIndex;
CString csName="�$%not_set";
BOOL bRet = FALSE;

  if ( ! FunctionPermissionOk("DoChangeLayerSettingsEx"))
     return FALSE;


  if (m_connected)
    {

     if ((m_layerinfo == LIF_NAME) || (m_layerinfo == LIF_NONE))
       {
        
        csName = sLayerName;

        Debug.Print("DoChangeLayerSettingsEx: '" + csName + "' /LayerInfo = 'None' or 'Text'");     
       }
     else if (m_layerinfo == LIF_INDEX)
       {
        // Get the layer-index        
        lIndex = FindString(m_layernameList, sLayerName);
        if (lIndex > -1)
          {
           csName = GetStringParam(m_layerindexList, lIndex);
          }
        else 
          Debug.Print("Cannot find matching layer-index to layer-name: " + CString(sLayerName));
       }

     if (csName != "�$%not_set")  
       {
        bRet = SendChangeLayerSettings(csName, sNewName, lOnOff, lPosition, lGenOnOff, dGenMin, dGenMax, lFixOnOff, lLineStyle, lLineWidth, lLineColor, lPattern, lPatternColor, lTransparent);
       }
    }

  return bRet;
}

BOOL CAXWingisCtrl::DoChangeLayerSettingsByIndex(long lLayerIndex, LPCTSTR sNewName, BOOL bOn, long lPosition, BOOL bGenOn, double dGenMin, double dGenMax, BOOL bFixOn, long lLineStyle, long lLineWidth, long lLineColor, long lPattern, long lPatternColor, BOOL bTransparent) 
{	
    if ( ! FunctionPermissionOk("DoChangeLayerSettingsByIndex"))
       return FALSE;

	return SendChangeLayerSettings(IntToStr(lLayerIndex), sNewName, bOn, lPosition, bGenOn, dGenMin, dGenMax, bFixOn, lLineStyle, lLineWidth, lLineColor, lPattern, lPatternColor, bTransparent);
}

BOOL CAXWingisCtrl::SendChangeLayerSettings(CString sLayerName, CString sNewName, long lOnOff, 
                                            long lPosition, long lGenOnOff, double dGenMin, double dGenMax, 
                                            long lFixOnOff, long lLineStyle, long lLineWidth, 
                                            long lLineColor, long lPattern, long lPatternColor, 
                                            long lTransparent) 
{
BOOL bRet = FALSE;
CString csCmd;

    if (m_connected)
      {  
/* mk separator
       csCmd = "[LAC][" + sLayerName + "][";
       csCmd += sNewName + "," + IntToStr(lOnOff) + "," + IntToStr(lPosition) + "," + IntToStr(lGenOnOff) + ",";
       csCmd += DToStr(dGenMin) + "," + DToStr(dGenMax) + "," + IntToStr(lFixOnOff) + "," + IntToStr(lLineStyle) + ",";
       csCmd += IntToStr(lLineWidth) + "," + IntToStr(lLineColor) + "," + IntToStr(lPattern) + ",";
       csCmd += IntToStr(lPatternColor) + ",";
       csCmd += IntToStr(lTransparent) + "]";
*/

       csCmd = "[LAC][" + sLayerName + "][";
       csCmd += sNewName + m_csDDESeparator + IntToStr(lOnOff) + m_csDDESeparator + IntToStr(lPosition) + m_csDDESeparator + IntToStr(lGenOnOff) + m_csDDESeparator;
       csCmd += DToStr(dGenMin) + m_csDDESeparator + DToStr(dGenMax) + m_csDDESeparator + IntToStr(lFixOnOff) + m_csDDESeparator + IntToStr(lLineStyle) + m_csDDESeparator;
       csCmd += IntToStr(lLineWidth) + m_csDDESeparator + IntToStr(lLineColor) + m_csDDESeparator + IntToStr(lPattern) + m_csDDESeparator;
       csCmd += IntToStr(lPatternColor) + m_csDDESeparator;
       csCmd += IntToStr(lTransparent) + "]";

       bRet = SendToWinGIS(csCmd);

       Debug.Print("DoChangeLayerSettings: " + csCmd);

      }
	return bRet;
}

BOOL CAXWingisCtrl::DoCopyObjects(LPCTSTR sTargetLayer, long hProgisIDList, BOOL bAutoCommit) 
{
BOOL bRet = FALSE;	
CString sCmd;
long hBlock;
long lIndex;

 if ( ! FunctionPermissionOk("DoCopyObjects"))
    return FALSE;
    
 if (m_connected)
   {  
    if ((aBase.GetSize() > hProgisIDList) && (hProgisIDList >= 0))
      {
       hBlock = BuildCmdBlockFromList(hProgisIDList);
       if (hBlock > -1)
         {
          bRet = TRUE;
          for (lIndex=0; (lIndex < GetListSize(hBlock)) && bRet; lIndex++)
            {
             sCmd = CString("[CPY]") + GetLayerToken() + "[" + CString(sTargetLayer) + "]" + GetStringParam(hBlock, lIndex);
             bRet = SendToWinGIS(sCmd);
             Debug.Print((const char*)sCmd);
            }
          DeleteList(hBlock);
         }       
       if (bAutoCommit && bRet)
          bRet = SendToWinGIS("[END][]");
      }
   }
 return bRet;
}

// Glukhov: This function was created to create objects asynchronously 
// and to use it in function DoCreateObject (to create objects synchronously)
BOOL CAXWingisCtrl::DoCreateObjectEx(LPCTSTR sObjectType, long lSymbolNumber, 
                                     double dX, double dY, long lUserId,
                                     LPCTSTR sLayer, double dSize, BOOL bAutoCommit)
{
//++ Slava2 AXWinGIS .BUILD#24 23.10.00 This method update for creatring polyline and polygon
  long dXl, dYl;
  long i, iPart;
  long iSizeX, iSizeY;
  CString tempX, tempY;
//-- Slava2 AXWinGIS .BUILD#24 23.10.00 This method update for creatring polyline and polygon
  CString sID;
  CString sCmd;
  BOOL bRet = TRUE;

  if ( !FunctionPermissionOk("DoCreateObject") ) return FALSE;
  if ( !m_connected ) return FALSE;

//++Slava2 New Method

  switch (StrToInt(sObjectType))
    {//Switch 0 and 1 for point and symbol 2 and 3 for polyline and polygon
    case 0: 
    case 1: 
	/*
      sCmd = "[DGR][" + CString(sObjectType) + "," + IntToStr(lSymbolNumber) + CString(",") + 
//              DToStr(dX) + CString(",") + DToStr(dY) + CString(",1234");
              DToStr(dX) + CString(",") + DToStr(dY) + "," + IntToStr(lUserId);
      sCmd = sCmd + "," + CString(sLayer) + "," + DToStr(dSize) + "]";
	  */ 

      sCmd = "[DGR][" + CString(sObjectType) + m_csDDESeparator + IntToStr(lSymbolNumber) + m_csDDESeparator + 
              DToStr(dX) + m_csDDESeparator + DToStr(dY) + m_csDDESeparator + IntToStr(lUserId);
      sCmd = sCmd + m_csDDESeparator + CString(sLayer) + m_csDDESeparator + DToStr(dSize) + "]";

      bRet = SendToWinGIS(sCmd);
//      bRet = WaitForReplyTimeout("RED", 100);

      if (bAutoCommit)
        bRet = SendToWinGIS("[END][]");               
      break;


    case 2:
    case 3: 
      dXl = int(dX);
      dYl = int(dY);            
            
      iSizeX = GetListSize(dXl);    
      iSizeY = GetListSize(dYl);
               
      if ( (iSizeX != iSizeY) || (!IsValidList(dXl)) ) return FALSE;

            i = 0;
            iPart = 150;
            // First: For big object cut in parts (150 point)
                   
          while (iSizeY > 150)
          {//While 150
			  // mk separator
/*
			  sCmd = "[DGR][" + CString(sObjectType) + ","  + CString("1,") + 
//                  CString("1,") + CString("1,1");
                  CString("1,") + CString("1,") + IntToStr(lUserId);
                  sCmd = sCmd + "," + CString(sLayer) + "]";
                  // add number of point and number of point in sting --Slava2
                  sCmd = sCmd + "[" + IntToStr(iSizeX) + "]" + "[150]";
*/ 
			  sCmd = "[DGR][" + CString(sObjectType) + m_csDDESeparator + CString("1") + m_csDDESeparator + 
                  CString("1") + m_csDDESeparator + CString("1") + m_csDDESeparator + IntToStr(lUserId);
                  sCmd = sCmd + m_csDDESeparator + CString(sLayer) + "]";
                  // add number of point and number of point in sting --Slava2
                  sCmd = sCmd + "[" + IntToStr(iSizeX) + "]" + "[150]";

                  // add Coordinate --Slava2                  
                  while (i != iPart)
                  {
                       tempX = GetStringParam (dXl,i);
                       tempY = GetStringParam (dYl,i);
                       i = i + 1;
/* mk separator
                       sCmd = sCmd + "[" + tempX + "," + tempY +"]";
*/
					   
                       sCmd = sCmd + "[" + tempX + m_csDDESeparator + tempY +"]";
                  }
                
                  iPart = iPart + 150;
                  iSizeY = iSizeY - 150;

                  bRet = SendToWinGIS(sCmd);
//          bRet = WaitForReplyTimeout("RED", 100);              
          bRet = WaitForReplyTimeout("RED", 50);                 
            }//End While 150

               
          //Second : send last part of big object or small object    
/* mk separator
          sCmd = "[DGR][" + CString(sObjectType) + ","  + CString("1,") + 
//               CString("1,") + CString("1,1");
             CString("1,") + CString("1,") + IntToStr(lUserId);
             sCmd = sCmd + "," + CString(sLayer) + "]";
            // add number of point and number of point in sting --Slava2
          sCmd = sCmd + "[" + IntToStr(iSizeX) + "]" + "[" + IntToStr(iSizeY) + "]";
*/ 
          sCmd = "[DGR][" + CString(sObjectType) + m_csDDESeparator + CString("1") + m_csDDESeparator + 
             CString("1") + m_csDDESeparator + CString("1") + m_csDDESeparator + IntToStr(lUserId);
             sCmd = sCmd + m_csDDESeparator + CString(sLayer) + "]";
            // add number of point and number of point in sting --Slava2
          sCmd = sCmd + "[" + IntToStr(iSizeX) + "]" + "[" + IntToStr(iSizeY) + "]";

		  // add Coordinate --Slava2             
          while (i != iSizeX)
          {
                  tempX = GetStringParam (dXl,i);
                  tempY = GetStringParam (dYl,i);
                  i = i + 1;
/*
                  sCmd = sCmd + "[" + tempX + "," + tempY +"]";            
*/ 
                  sCmd = sCmd + "[" + tempX + m_csDDESeparator + tempY +"]";            
          }
               
        bRet = SendToWinGIS(sCmd);
//      bRet = WaitForReplyTimeout("RED", 100);

      if (bAutoCommit)
        bRet = SendToWinGIS("[END][]");

            break;
	
    case 4:
/*		mk separator
      sCmd = "[DGR][" + CString(sObjectType) + ",1," + DToStr(dX) + "," + DToStr(dY) + "," +
		  IntToStr(lUserId);
	  sCmd = sCmd + "," + CString(sLayer) + "]";
	  sCmd = sCmd + "[" + DToStr(dSize) + "]";
*/
      sCmd = "[DGR][" + CString(sObjectType) + m_csDDESeparator + "1" + m_csDDESeparator + DToStr(dX) + m_csDDESeparator + DToStr(dY) + m_csDDESeparator +
		  IntToStr(lUserId);
	  sCmd = sCmd + m_csDDESeparator + CString(sLayer) + "]";
	  sCmd = sCmd + "[" + DToStr(dSize) + "]";

      bRet = SendToWinGIS(sCmd);

      if (bAutoCommit)
        bRet = SendToWinGIS("[END][]");               
      break;

	  
  }//EndSwitch        
//--Slava2 New Method

  return bRet;
}

// Glukhov: This function was changed to use one common code to create objects synchronously or asynchronously 
BSTR CAXWingisCtrl::DoCreateObject(LPCTSTR sObjectType, long lSymbolNumber, 
                                   double dX, double dY, LPCTSTR sLayer, 
                                   double dSize, long lTimeOut, BOOL bAutoCommit)
{
  BOOL bRet;
  CString strResult = "";
  csReturnString = "";

  if (csLastEvent == "GRD") csLastEvent = "";
  
  bRet = DoCreateObjectEx( sObjectType, lSymbolNumber, dX, dY, 0, sLayer, dSize, bAutoCommit);
  if (bRet) 
    {  
     bRet = -1;                  
     WaitForReplyTimeout("GRD", lTimeOut);
     strResult = csReturnString;
    }       
  return strResult.AllocSysString();
}

BOOL CAXWingisCtrl::DoGetNextUnusedID(BOOL bWait, long lTimeout) 
{
CString sCmd;
BOOL bRet;

    if ( ! FunctionPermissionOk("DoGetNextUnusedID"))
       return FALSE;

    if (m_connected)
      {  
       
       bRet = SendToWinGIS("[GON][]");

       if (bRet && bWait)
         {  
          bRet = -1; 
          csLastEvent = "";       
          bRet = WaitForReplyTimeout("NON", lTimeout);
         }       
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoCreateLayerEx(LPCTSTR sNewLayer, long lOnOff, long lPosition, 
                                    long lGenOnOff, double dGenMin, double dGenMax, 
                                    long lFixOnOff, long lLineStyle, long lLineWidth, 
                                    long lLineColor, long lPattern, long lPatternColor, 
                                    long lTransparent) 
{
BOOL bRet = FALSE;
CString csCmd;

    if ( ! FunctionPermissionOk("DoCreateLayerEx"))
       return FALSE;

    if (m_connected)
      {  
       csCmd = "[LAN][";
/* mk separator
       csCmd += CString(sNewLayer) + "," + IntToStr(lOnOff) + "," + IntToStr(lPosition) + "," + IntToStr(lGenOnOff) + ",";
       csCmd += DToStr(dGenMin) + "," + DToStr(dGenMax) + "," + IntToStr(lFixOnOff) + "," + IntToStr(lLineStyle) + ",";
       csCmd += IntToStr(lLineWidth) + "," + IntToStr(lLineColor) + "," + IntToStr(lPattern) + ",";
       csCmd += IntToStr(lPatternColor) + ",";
       csCmd += IntToStr(lTransparent) + "]";
*/ 
       csCmd += CString(sNewLayer) + m_csDDESeparator + IntToStr(lOnOff) + m_csDDESeparator + IntToStr(lPosition) + m_csDDESeparator + IntToStr(lGenOnOff) + m_csDDESeparator;
       csCmd += DToStr(dGenMin) + m_csDDESeparator + DToStr(dGenMax) + m_csDDESeparator + IntToStr(lFixOnOff) + m_csDDESeparator + IntToStr(lLineStyle) + m_csDDESeparator;
       csCmd += IntToStr(lLineWidth) + "," + IntToStr(lLineColor) + "," + IntToStr(lPattern) + ",";
       csCmd += IntToStr(lPatternColor) + m_csDDESeparator;
       csCmd += IntToStr(lTransparent) + "]";

       bRet = SendToWinGIS(csCmd);

       Debug.Print("DoCreateLayerEx: " + csCmd);

       // force to load layerlist next time the user requests the layerlist
       bm_layernameListValid = FALSE;
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoDisableScreenUpdate() 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoDisableScreenUpdate"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[NRD][]");
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoSetScreenUpdate(BOOL bEnabled) 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoSetScreenUpdate"))
       return FALSE;

    if (m_connected)
      {  
       if (bEnabled == FALSE)
         bRet = SendToWinGIS("[NRD][]");
       else
         bRet = SendToWinGIS("[RDR][]");
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoReloadProject(LPCTSTR sProject) 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoReloadProject"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[REL][" + CString(sProject) + "]");
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoExecuteMenuFunction(LPCTSTR sFunctionName) 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoExecuteMenuFunction"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[EMF][" + CString(sFunctionName) + "]");
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoChangeBitmapSettings(BOOL bBorderOnly, BOOL bTransBlack, long lTransparency) 
{
BOOL bRet = FALSE;
CString csCmd;

    if ( ! FunctionPermissionOk("DoChangeBitmapSettings"))
       return FALSE;

    if (m_connected)
      {  
/*
       csCmd = "[OBM][" + BoolToStr( ! bBorderOnly) + "," + BoolToStr(bTransBlack) + "," + 
               IntToStr(lTransparency) + "]";
*/ 
       csCmd = "[OBM][" + BoolToStr( ! bBorderOnly) + m_csDDESeparator + BoolToStr(bTransBlack) + m_csDDESeparator + 
               IntToStr(lTransparency) + "]";
		
       bRet = SendToWinGIS(csCmd);

       Debug.Print("DoChangeBitmapSettings: " + csCmd);
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoSetOptionSettings(BOOL bCartesian, BOOL bShowCorners, BOOL bMakeBackups, 
                                        BOOL bAutoSave, long lAutoSaveTime) 
{
BOOL bRet = FALSE;
CString csCmd;

    if ( ! FunctionPermissionOk("DoSetOptionSettings"))
       return FALSE;

    if (m_connected)
      {  
/* mk separator
       csCmd = "[OES][" + BoolToStr(bCartesian) + "," + BoolToStr(bShowCorners) + "," + 
               BoolToStr(bMakeBackups) + "," + BoolToStr(bAutoSave) + "," + IntToStr(lAutoSaveTime) + "]";
*/ 
       csCmd = "[OES][" + BoolToStr(bCartesian) + m_csDDESeparator + BoolToStr(bShowCorners) + m_csDDESeparator + 
               BoolToStr(bMakeBackups) + m_csDDESeparator + BoolToStr(bAutoSave) + m_csDDESeparator + IntToStr(lAutoSaveTime) + "]";
		
       bRet = SendToWinGIS(csCmd);

       Debug.Print("DoSetOptionSettings: " + csCmd);
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoChangeObjectStyleEx(LPCTSTR sProgisID, long lObjectColor, long lLineStyle, 
                                        long lLineWidth, long lPatternColor, long lPattern, 
                                        long lTransparent, long lSymbolSize, long lVisible, BOOL bAutoCommit)
{
BOOL bRet = FALSE;
CString csCmd;

    if ( ! FunctionPermissionOk("DoChangeObjectStyleEx"))
       return FALSE;

    if (m_connected)
      {  
/* mk separator
       csCmd = "[SOS]" + GetLayerToken() + "[" + CString(sProgisID) + "][" +
                IntToStr(lObjectColor) + "," + IntToStr(lLineStyle) + "," +
                IntToStr(lLineWidth) + "," + IntToStr(lPatternColor) + "," +
                IntToStr(lPattern) + "," + IntToStr(lTransparent) + "," +
                IntToStr(lSymbolSize) + "," + IntToStr(lVisible) + "]";
*/
       csCmd = "[SOS]" + GetLayerToken() + "[" + CString(sProgisID) + "][" +
                IntToStr(lObjectColor) + m_csDDESeparator + IntToStr(lLineStyle) + m_csDDESeparator +
                IntToStr(lLineWidth) + m_csDDESeparator + IntToStr(lPatternColor) + m_csDDESeparator +
                IntToStr(lPattern) + m_csDDESeparator + IntToStr(lTransparent) + m_csDDESeparator +
                IntToStr(lSymbolSize) + m_csDDESeparator + IntToStr(lVisible) + "]";
		
       bRet = SendToWinGIS(csCmd);
       
       if (bAutoCommit)
         bRet = SendToWinGIS("[END][]");
      }
	return bRet;
}

long CAXWingisCtrl::DoGetNextSymbolNumber(long lLastNumber) 
{
long lRet = 0;
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoGetNextSymbolNumber"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[SRQ][" + IntToStr(lLastNumber) + "]");
       bRet = SendToWinGIS("[END][]");
       	
       if ( bRet)
         {
             csLastEvent = "";
             WaitForReply("SYM");
             lRet = hReturnList;
             hReturnList = -1;
         }
      }
    return lRet;
}

BOOL CAXWingisCtrl::DoSetActiveSymbol(long lSymbol) 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoSetActiveSymbol"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[SAS][" + IntToStr(lSymbol) + "]");
      }
    return bRet;
}

BOOL CAXWingisCtrl::DoSetWindowPos(long lScreenWidth, long lScreenHeight, 
                                   long lDBWinWidth, long lDBWinHeight, long lPos) 
{
BOOL bRet = FALSE;
CString csCmd;

    if ( ! FunctionPermissionOk("DoSetWindowPos"))
       return FALSE;

    if (m_connected)
      {  
/* mk separator
		csCmd = "[SWP][" + IntToStr(lScreenWidth) + "," + IntToStr(lScreenHeight) + "," +
                IntToStr(lDBWinWidth) + "," + IntToStr(lDBWinHeight) + "," +          
                IntToStr(lPos) + "]";
*/      
		csCmd = "[SWP][" + IntToStr(lScreenWidth) + m_csDDESeparator + IntToStr(lScreenHeight) + m_csDDESeparator +
                IntToStr(lDBWinWidth) + m_csDDESeparator + IntToStr(lDBWinHeight) + m_csDDESeparator +          
                IntToStr(lPos) + "]";

		bRet = SendToWinGIS(csCmd);       

       Debug.Print("DoSetWindowPos: " + csCmd);
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoSetZoom(double dFactor) 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoSetZoom"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[SZF][" + DToStr(dFactor) + "]");
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoNotifyTableChange() 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoNotifyTableChange"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[CHT][]");
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoInsertObjects(long hProgisIDList, BOOL bAutoCommit) 
{
BOOL bRet = FALSE;	
CString sCmd;
long hBlock;
long lIndex;

 if ( ! FunctionPermissionOk("DoInsertObjects"))
    return FALSE;
    
 if (m_connected)
   {  
    if ((aBase.GetSize() > hProgisIDList) && (hProgisIDList >= 0))
      {
       hBlock = BuildCmdBlockFromList(hProgisIDList);
       if (hBlock > -1)
         {
          bRet = TRUE;
          for (lIndex=0; (lIndex < GetListSize(hBlock)) && bRet; lIndex++)
            {
             sCmd = CString("[ASW]") + GetLayerToken() + GetStringParam(hBlock, lIndex);
             bRet = SendToWinGIS(sCmd);
             Debug.Print("DoInsertObjects: " + sCmd);
            }
          DeleteList(hBlock);
         }       
       if (bAutoCommit)
          bRet = SendToWinGIS("[END][]");
      }
   }
 return bRet;
}

BOOL CAXWingisCtrl::DoAsciiExport(LPCTSTR sProgisID, long hValueList) 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoAsciiExport"))
       return FALSE;

    if (m_connected)
      {  
       Debug.Print("DoAsciiExport");
       bRet = SendToWinGIS("[DBE][" + CString(sProgisID) + "]" + 
                           ConvertListToString(hValueList) );
             	
      }
    return bRet;    
}

BOOL CAXWingisCtrl::DoAsciiImport(LPCTSTR sAsciiFilename, LPCTSTR sADFFilename, 
                                  LPCTSTR sSymbolFilename, LPCTSTR sFontName, 
                                  long lFontStyle, long lFontHeight, 
                                  LPCTSTR sTextLayer, LPCTSTR sObjectLayer, 
                                  double dXOffset, double dYOffset, double dScaleBy)
{
BOOL bRet = FALSE;
CString csCmd;

    if ( ! FunctionPermissionOk("DoAsciiImport"))
       return FALSE;

    if (m_connected)
      {  
/* mk separator
		csCmd = "[AIM][" + CString(sAsciiFilename) + "," + CString(sADFFilename) + "," +
               CString(sSymbolFilename) + "," + CString(sFontName) + "," +
               IntToStr(lFontStyle) + "," + IntToStr(lFontHeight) + "," +
               CString(sTextLayer) + "," + CString(sObjectLayer) + "," + 
               DToStr(dXOffset) + "," + DToStr(dYOffset) + "," + DToStr(dScaleBy) + "]";
*/ 
		csCmd = "[AIM][" + CString(sAsciiFilename) + m_csDDESeparator + CString(sADFFilename) + m_csDDESeparator +
               CString(sSymbolFilename) + m_csDDESeparator + CString(sFontName) + m_csDDESeparator +
               IntToStr(lFontStyle) + m_csDDESeparator + IntToStr(lFontHeight) + m_csDDESeparator +
               CString(sTextLayer) + m_csDDESeparator + CString(sObjectLayer) + m_csDDESeparator + 
               DToStr(dXOffset) + m_csDDESeparator + DToStr(dYOffset) + m_csDDESeparator + DToStr(dScaleBy) + "]";

       bRet = SendToWinGIS(csCmd);       

       Debug.Print("DoAsciiImport: " + csCmd);
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoNotifyImportError(long hColumnNameList) 
{
BOOL bRet = FALSE;
long iSize, i;
CString csCmd;
CString csSep = "";

    if ( ! FunctionPermissionOk("DoNotifyImportError"))
       return FALSE;


    if (m_connected)
      {  
       if (IsValidList(hColumnNameList))
         {
          iSize = GetListSize(hColumnNameList);
          csCmd = "[ERR][" + IntToStr(iSize) + "][";
          for (i=0; i < iSize; i++)
            {
             csCmd = csCmd + csSep + GetStringParam(hColumnNameList, i);
// mk separator + m_csDDESeparator +             csSep = ",";
             csSep = m_csDDESeparator;  
            }
          csCmd = csCmd + "]";
          
          bRet = SendToWinGIS(csCmd);

          Debug.Print("DoNotifyImportError: " + csCmd);
         }
             	
      }
    return bRet;    
}

BOOL CAXWingisCtrl::DoTempSetText(LPCTSTR sProgisID, LPCTSTR sText)
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoTempSetText"))
       return FALSE;

    if (m_connected)
      {         
       bRet = SendToWinGIS("[LAB][" + CString(sProgisID) + "][" + CString(sText) + "]");             	
      }
    return bRet;    
}

BOOL CAXWingisCtrl::WaitForReadyMsg(long lTimeout) 
{
BOOL bRet = FALSE;

    if (m_connected)
      {
          csLastEvent = "";
          Debug.Print("WaitFor RED, timeout: " + IntToStr(lTimeout));
          bRet = WaitForReplyTimeout("RED", lTimeout);
          if (bRet)
             Debug.Print("Got RED"); 
          else
             Debug.Print("TIMEOUT");
      }
	return bRet;
}

BOOL CAXWingisCtrl::DoCommit() 
{
	return SendToWinGIS("[END][]");
}

//++ Slava2 AXWinGIS .BUILD#24 23.10.00 Requestions points of polygon
BOOL CAXWingisCtrl::DoGetPolygonData(long lID)
{
BOOL bRet = FALSE;
if ( ! FunctionPermissionOk("DoGetPolygonData"))
       return FALSE;
	if (m_connected)
      {         
       bRet = SendToWinGIS("[GPD][" + IntToStr(lID) + "]");             	
      }
    return bRet;    
}
//-- Slava2 AXWinGIS .BUILD#24 23.10.00

/////////////// END OF METHODS /////////////////////////////////////////////


//  ************************************************************************                   
//  FUNCTION:  BuildCmdBlockFromList
//  PARAMETER: hList....List-Handle to build string from
//
//             This function loops through the list and adds the list-items
//             to a string, each item enclosed by [], and the number of items
//             in front of the items themselfs
//             These strings are limited to 200 items, each string with max 200
//             is added to a list.
//  RETURN:    A list handle containing all strings, each with the number of items
//             in front.
//  ************************************************************************                   
long CAXWingisCtrl::BuildCmdBlockFromList(long hList)
{
long hBlock;
long i;
long lSize;

  hBlock = CreateList();
  if (hBlock > -1)
     {
      lSize = GetListSize(hList);
      for (i=0; i < lSize; i+=WG_MAX_CMDSIZE)
         {
          AddStringParam(hBlock, BuildStringFromListEx(hList, i, WG_MAX_CMDSIZE));
         }
     }

  return hBlock;
}

//  ************************************************************************                   
//  FUNCTION:  BuildStringFromListEx
//  PARAMETER: hList....List-Handle to build string from
//             lStart...Index of item to start with (0 is the first)
//             lCount...Number of items to add to the string (-1 means all items)
//
//             This function loops through the list and adds the list-items
//             to a string, each item enclosed by [], and the number of items
//             in front of the items themselfs
//  RETURN:    A CString containing all items
//  EXAMPLE    Output could be: [3][item1][item2][item3]
//  ************************************************************************                   
CString CAXWingisCtrl::BuildStringFromListEx(long hList, long lStart, long lCount)
{
CStringArray* pList = NULL;
BOOL bRet = FALSE;	
CString pString = "";
CString sCmd = "";
long lSize;
int i;
UINT iPadLen;
long lCount1;

char acPad[4] = {32,32,0,0};
char acPadID[10] = {32,32,32,32,32,32,32,32,32,0};
char acBuf[4] = {0,0,0,0};

	   lCount1 = lCount;   

       pList = (CStringArray*) aBase.GetAt(hList);
       if (pList != NULL)
         {
          lSize = pList->GetSize();

          if ((lSize > 0) && (lStart < lSize))
            {
             if ((lCount > lSize - lStart) || (lCount == -1))
                {
                 lCount = lSize - lStart;
                }
             ultoa((ULONG)lCount, acBuf, 10);
             iPadLen = strlen(acBuf) - 1;
             if (iPadLen > 2) iPadLen = 2;

             sCmd = CString("[") + ((char *)(acPad+iPadLen)) + 
                    CString(acBuf) + "]";
             for (i=lStart;(i-lStart) < lCount;i++)
               {
                pString = (CString) pList->GetAt(i);
                iPadLen = pString.GetLength() - 1;
                if (iPadLen > 9) iPadLen = 9;

                if(lCount1 == -1)
					sCmd = sCmd + "[" + pString + "]";
				else
					sCmd = sCmd + CString("[") + ((char *)(acPadID+iPadLen)) + pString + "]";
               }
            }
         }


       return sCmd;
}

//  ************************************************************************                   
//  FUNCTION:  ConvertListToString
//  PARAMETER: hList....List-Handle to build string from
//
//             This function loops through the list and adds the list-items
//             to a string, each item enclosed by []
//  RETURN:    A CString containing all items
//  EXAMPLE    Output could be: [item1][item2][item3]
//  ************************************************************************                   
CString CAXWingisCtrl::ConvertListToString(long hList)
{
CStringArray* pList = NULL;
BOOL bRet = FALSE;	
CString pString = "";
CString sCmd = "";
long lSize;
int i;
UINT iPadLen;

char acPadID[10] = {32,32,32,32,32,32,32,32,32,0};
char acBuf[4] = {0,0,0,0};

       sCmd = "";                    
       pList = (CStringArray*) aBase.GetAt(hList);
       if (pList != NULL)
         {
          lSize = pList->GetSize();
        
          if ( lSize > 0 ) 
            {
             
             for (i=0;i < lSize;i++)
               {
                pString = (CString) pList->GetAt(i);
                iPadLen = pString.GetLength() - 1;
                if (iPadLen > 9) iPadLen = 9;

                sCmd = sCmd + CString("[") + ((char *)(acPadID+iPadLen)) + 
                       pString + "]";
               }
            }
         }


       return sCmd;
}

//  ************************************************************************                   
//  FUNCTION:  BuildStringFromList
//  PARAMETER: hList....List-Handle to build string from
//
//             This function loops through the list and adds the list-items
//             to a string, each item enclosed by [], and the number of items
//             in front of the items themselfs
//  RETURN:    A CString containing all items
//  EXAMPLE    Output could be: [3][item1][item2][item3]
//  ************************************************************************                   
CString CAXWingisCtrl::BuildStringFromList(long hList)
{
return BuildStringFromListEx(hList, 0, -1);
}

//  ************************************************************************                   
//  FUNCTION:  BuildListFromString
//  PARAMETER: csCmd....String der in eine Liste geteilt werden soll
//             iStart...token-offset to start with, 1 is the first
//
//             This function loops through string and adds the tokens to the list
//  RETURN:    A list-handle containing all items
//  ************************************************************************                   
long CAXWingisCtrl::BuildListFromString(CString csCmd, int iStart)
{
long hList;
int iCount, i;
CString csItem;

       hList = CreateList();
       if (hList >= 0)
         {
          iCount = GetTokenCount(csCmd);
          for (i = iStart;i <= iCount;i++)
            {
             csItem = GetCmdToken(csCmd,i);
             AddStringParam(hList, csItem);
            }
         } 
       return hList;
}

//  ************************************************************************                   
//  FUNCTION:  BuildListFromSubString
//  PARAMETER: csCmd....String der in eine Liste geteilt werden soll
//             iStart...token-offset to start with, 1 is the first
//             abc,123,def,456
//             This function loops through string and adds the tokens to the list
//  RETURN:    A list-handle containing all items
//  ************************************************************************                   
long CAXWingisCtrl::BuildListFromSubString(CString csCmd, int iStart)
{
long hList;
int iCount, i;
CString csItem;

       hList = CreateList();
       if (hList >= 0)
         {
          iCount = GetSubStringCount(csCmd, ',');
          for (i = iStart;i <= iCount;i++)
            {
             csItem = GetSubString(csCmd,i, ',');
             AddStringParam(hList, csItem);
            }
         } 
       return hList;
}
//  ************************************************************************                   
//  FUNCTION:  AddListFromString
//  PARAMETER: csCmd....String der in eine Liste geteilt werden soll
//
//             This function loops through string and adds the tokens to the list
//  RETURN:    TRUE if ok, FALSE else
//  ************************************************************************                   
BOOL CAXWingisCtrl::AddListFromString(CString csCmd, int Start, long &hList)
{
int iCount, i;
BOOL bRet = FALSE;
CString csItem;

       if ( ! IsValidList(hList))         // Is already a list ?
         hList = CreateList();            // no, create new

       if (hList >= 0)
         {
          bRet = TRUE; 
          iCount = GetTokenCount(csCmd);
          for (i = Start;i <= iCount;i++)
            {
             csItem = GetCmdToken(csCmd,i);
             AddStringParam(hList, csItem);
            }
         } 
       return bRet;
}

//  ************************************************************************                   
//  FUNCTION:  BuildDoubleListFromTokenString
//  PARAMETER: csCmd....String der in eine Liste geteilt werden soll
//  FORMAT:    [List1Item,List2Item][List1Item,List2Item][List1Item,List2Item]
//             This function loops through string and adds the tokens to the list
//  RETURN:    TRUE if ok, FALSE on error
//  ************************************************************************                   
BOOL CAXWingisCtrl::BuildDoubleListFromTokenString(CString csCmd, int Start, long& hList1, long& hList2)
{
int iCount, i;
BOOL bRet = FALSE;
CString csItem;

       if ( ! IsValidList(hList1))         // Is already a list ?
         hList1 = CreateList();            // no, create new 

       if ( ! IsValidList(hList2))         // Is already a list ?
         hList2 = CreateList();            // no, create new 

       if ((hList1 >= 0) && (hList2 >= 0))
         {
          bRet = TRUE; 
          iCount = GetTokenCount(csCmd);
          for (i = Start;i <= iCount;i++)
            {
             csItem = GetCmdToken(csCmd,i);

             AddStringParam(hList1, GetSubString(csItem,1,','));
             AddStringParam(hList2, GetSubString(csItem,2,','));
            }
         } 
       return bRet;
}
//  ************************************************************************                   
//  FUNCTION:  BuildDoubleListFromString
//  PARAMETER: csCmd....String der in eine Liste geteilt werden soll
//  FORMAT:    [List1Item][List2Item][List1Item][List2Item][List1Item][List2Item]
//             This function loops through string and adds the tokens to the list
//  RETURN:    TRUE if ok, FALSE on error
//  ************************************************************************                   
BOOL CAXWingisCtrl::BuildDoubleListFromString(CString csCmd, int Start, long& hList1, long& hList2)
{
int iCount, i;
BOOL bRet = FALSE;

       if ( ! IsValidList(hList1))         // Is already a list ?
         hList1 = CreateList();            // no, create new 

       if ( ! IsValidList(hList2))         // Is already a list ?
         hList2 = CreateList();            // no, create new 

       if ((hList1 >= 0) && (hList2 >= 0))
         {
          bRet = TRUE; 
          iCount = GetTokenCount(csCmd);
          for (i = Start;i < iCount;i+=2)
            {           
             AddStringParam(hList1, GetCmdToken(csCmd,i));
             AddStringParam(hList2, GetCmdToken(csCmd,i+1));
            }
         } 
       return bRet;
}

       
//  ************************************************************************                   
//  FUNCTION:  GetCmdToken
//  PARAMETER: CmdStr...DDE-Befehl von WinGIS/DataLink
//             pos......Position des gew�nschten Elementes beginnend mit 1
//
//  BEISPIEL:  GetCmdToken("[MON][2][001][003]",4) liefert "003"
//  ************************************************************************                   
char * CAXWingisCtrl::GetCmdToken (const char *CmdStr, int pos)
{                
    static char token[255];
    int    i;
    const char * AktPos;
    const char * EndPos; 
    int    brace;    
    int    len;
    
    brace = (int)'[';
    
    if (pos > GetTokenCount(CmdStr))
      { 
       //Die Anzahl der Tokens in CmdStr
       //ist kleiner als pos
       token[0] = 0;
      }
    else
      {
        AktPos = CmdStr;
        for (i = 1;i < pos;i++)
            AktPos = strchr((char *)(AktPos + 1), brace);
    
        EndPos = (char *)strchr((char *)(AktPos + 1), brace);  
        
        memset(token,0,255);
        if (EndPos == NULL)
          strncpy((char *)token,(char *)(AktPos+1),strlen((char *)(AktPos+1))-1);
        else
          {
          len = (int)(EndPos - AktPos - 2);
          if (len < 0) len = 0;
          strncpy((char *)token,(char *)(AktPos+1), len);
          }
      }
       
    return (char *)token;   
}                                                                           

//  ************************************************************************                   
//  FUNCTION:  GetTokenCount
//  PARAMETER: CmdStr...DDE-Befehl von WinGIS/DataLink
//
//  BEISPIEL:  GetTokenCount("[MON][2][001][003]") liefert 4
//  ************************************************************************
int CAXWingisCtrl::GetTokenCount (const char *CmdStr)
{
    int TCount;
    const char *AktPos;
    int brace;
    
    brace = (int)'[';

    AktPos = CmdStr - 1;
    TCount = -1;

    do {
        TCount = TCount + 1;
        AktPos = strchr((char *)(AktPos + 1), brace);
       } while (AktPos != NULL);

    return TCount;
}


BOOL CAXWingisCtrl::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	// TODO: Add your message handler code here and/or call default
	AfxMessageBox("HELPINFO",32);
	return COleControl::OnHelpInfo(pHelpInfo);
}



BOOL CAXWingisCtrl::WaitForReply(const char* csEvent)
{
ULONG ulWaiting;
ULONG ulTicks;
int iStrCmpResult;

   //__try
   TRY
     {      
      LockInPlaceActive(TRUE); 
      bInside = FALSE;        // allow other events to be processed
#ifdef _DEBUGWIN_ON
      Debug.Print("WaitForReply " + CString(csEvent) + ", Timeout " + IntToStr(CMD_TIMEOUT));
      Debug.Print("Inside flag cleared");
#endif
      ulWaiting = 0;
      ulTicks = GetTickCount();
      SetTimer(2, 50, NULL);
      while ( ((ulWaiting < (unsigned long)CMD_TIMEOUT) || (CMD_TIMEOUT == -1))              
             && ( (iStrCmpResult = lstrcmp((const char*)csEvent,(const char*)csLastEvent)) != 0 )
             && (AfxGetApp()->PumpMessage()) )
        {         
         ulWaiting = GetTickCount() - ulTicks;
        }
      KillTimer(2);
     }
   //__except(EXCEPTION_EXECUTE_HANDLER)
   CATCH_ALL(e)
     {
   	  e->Delete();
      KillTimer(2);
      Debug.Print("Execption in WaitForReply");
     }
   END_CATCH_ALL
   LockInPlaceActive(FALSE); 
#ifdef _DEBUGWIN_ON
   Debug.Print("WaitForReply exits with " + BoolToStr(iStrCmpResult == 0) + ", was waiting for " + CString(csEvent) + ", LastEvent = " + csLastEvent);
#endif
   return (lstrcmp((const char*)csEvent,(const char*)csLastEvent) == 0);

   /* 
   TRY
     {
      LockInPlaceActive(TRUE); 
      ulWaiting = 0;
      ulTicks = GetTickCount();
      SetTimer(2, 400, NULL);
      while ((csEvent != csLastEvent) && (AfxGetApp()->PumpMessage()) && (ulWaiting < CMD_TIMEOUT))
        {         
         ulWaiting = GetTickCount() - ulTicks;
        }
      KillTimer(2);
     }
   CATCH_ALL(e)
     {
   	  e->Delete();
        KillTimer(2);
     }
   END_CATCH_ALL
   LockInPlaceActive(FALSE); 
   
   return ((csEvent == csLastEvent) ? TRUE : FALSE);
   */
}

// ++ Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#22 25.09.00 
BOOL CAXWingisCtrl::WaitForReplyWithCond(const char* csEvent, const char* csRetStrValue)
{
ULONG ulWaiting;
ULONG ulTicks;
int iStrCmpResult;

   TRY
		{      
      LockInPlaceActive(TRUE); 
      bInside = FALSE;        // allow other events to be processed
#ifdef _DEBUGWIN_ON
      Debug.Print("WaitForReply " + CString(csEvent) + " WithCond " + CString(csRetStrValue) + ", Timeout " + IntToStr(CMD_TIMEOUT));
      Debug.Print("Inside flag cleared");
#endif
      ulWaiting = 0;
      ulTicks = GetTickCount();
      SetTimer(2, 50, NULL);
      while ( ((ulWaiting < (unsigned long)CMD_TIMEOUT) || (CMD_TIMEOUT == -1))              
             && ( (iStrCmpResult = abs(lstrcmp((const char*)csEvent,(const char*)csLastEvent))
                                 + abs(lstrcmp((const char*)csRetStrValue,(const char*)csReturnString))) != 0 )
             && (AfxGetApp()->PumpMessage()) )
			{         
			ulWaiting = GetTickCount() - ulTicks;
			}
		KillTimer(2);
		}

	CATCH_ALL(e)
		{
		e->Delete();
		KillTimer(2);
		Debug.Print("Execption in WaitForReplyWithCond");
		}
	END_CATCH_ALL

	LockInPlaceActive(FALSE); 
#ifdef _DEBUGWIN_ON
	Debug.Print("WaitForReplyWithCond exits with " + BoolToStr(iStrCmpResult == 0) + ", was waiting for " + CString(csEvent) + " WithCond " + CString(csRetStrValue) +  ", LastEvent = " + csLastEvent+  ", ReturnString = " + csReturnString);
#endif
	return (lstrcmp((const char*)csEvent,(const char*)csLastEvent) == 0);
}
// -- Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#22 25.09.00 


BOOL CAXWingisCtrl::WaitForReplyTimeout(const char* csEvent, long lTimeOut)
{
ULONG ulWaiting;
ULONG ulTicks;
int iStrCmpResult;
    
   //__try
   TRY
     {
      LockInPlaceActive(TRUE); 
      bInside = FALSE;        // allow other events to be processed
#ifdef _DEBUGWIN_ON
      Debug.Print("WaitForReplyTimeout " + CString(csEvent) + ", Timeout " + IntToStr(lTimeOut));
      Debug.Print("Inside flag cleared");
#endif
      ulWaiting = 0;
      ulTicks = GetTickCount();
      SetTimer(1, 50, NULL);
      while ( ((ulWaiting < (unsigned long)lTimeOut) || (lTimeOut == -1))
             && ( (iStrCmpResult = lstrcmp((const char*)csEvent,(const char*)csLastEvent)) != 0 )
             && (AfxGetApp()->PumpMessage()) )
        {         
         ulWaiting = GetTickCount() - ulTicks;
        }
      KillTimer(1);
     }
   //__except(EXCEPTION_EXECUTE_HANDLER)
   CATCH_ALL(e)
     {
   	  e->Delete();
      KillTimer(1);
      Debug.Print("Execption in WaitForReplyTimeout");
     }
   END_CATCH_ALL
   LockInPlaceActive(FALSE); 
   
#ifdef _DEBUGWIN_ON
   Debug.Print("WaitForReplyTimeout exits with " + BoolToStr(iStrCmpResult == 0) + ", was waiting for " + CString(csEvent) + ", LastEvent = " + csLastEvent);
#endif
   return (lstrcmp((const char*)csEvent,(const char*)csLastEvent) == 0);
}

BOOL CAXWingisCtrl::WaitForSendOk(long lTimeOut)
{
ULONG ulWaiting;
ULONG ulTicks;
    
   //__try
   TRY
     {
      LockInPlaceActive(TRUE); 
      bInside = FALSE;        // allow other events to be processed
#ifdef _DEBUGWIN_ON
      Debug.Print("Enter WaitForSendOk, Timeout " + IntToStr(lTimeOut));
#endif
      ulWaiting = 0;
      ulTicks = GetTickCount();
      SetTimer(3, 50, NULL);
      while ( ((ulWaiting < (unsigned long)lTimeOut) || (lTimeOut == -1))
             && ( m_bSendOk == FALSE )
             && (AfxGetApp()->PumpMessage()) )
        {         
         ulWaiting = GetTickCount() - ulTicks;
         axipc.GetStatus();
        }
      KillTimer(3);
     }
   //__except(EXCEPTION_EXECUTE_HANDLER)
   CATCH_ALL(e)
     {
   	e->Delete();
      KillTimer(3);
      Debug.Print("Execption in WaitForSendOk");
     }
   END_CATCH_ALL
   LockInPlaceActive(FALSE); 
   
#ifdef _DEBUGWIN_ON
   Debug.Print("WaitForSendOk exits with " + BoolToStr(m_bSendOk));
#endif
   return (m_bSendOk);
}

/////////////////////////////////////////////////////////////////////
// View Managment

long CAXWingisCtrl::GetViewList() 
{

	return m_viewList;
}

/////////////////////////////////////////////////////////////////////
// Layer Managment

long CAXWingisCtrl::GetLayerNameList() 
{	

    if ((bm_layernameListValid == FALSE) && (m_connected))
      {
       if (csLastEvent == "LMA") csLastEvent = "";
       DoGetLayerSettings();
       if (WaitForReplyTimeout("LMA", 6000) == TRUE)
         { 
#ifdef _DEBUGWIN_ON
          Debug.Print("OK, got LMA");
#endif
         }
       else
         {
#ifdef _DEBUGWIN_ON
          Debug.Print("NOT OK, no LMA received until timeout");
#endif
         }
      }  

    if (bm_layernameListValid == FALSE)
      return -1;
    else
	   return m_layernameList;
}

long CAXWingisCtrl::GetLayerIndexList() 
{
    if ((bm_layernameListValid == FALSE) && (m_connected))
      {
       DoGetLayerSettings();
       if (WaitForReplyTimeout("LMA", 2000) == TRUE)
         {
#ifdef _DEBUGWIN_ON
          Debug.Print("OK, got LMA");
#endif
         }
       else
         {
#ifdef _DEBUGWIN_ON
          Debug.Print("NOT OK, no LMA received until timeout");
#endif
         }
      }  

    if (bm_layernameListValid == FALSE)
      return -1;
    else	  
	  return m_layerindexList;
}

void CAXWingisCtrl::OnLayerNameChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}


BSTR CAXWingisCtrl::GetLayerInfo() 
{
	CString strResult;

	switch(m_layerinfo)
      {
       case LIF_NAME:
         strResult = SLIF_NAME;
         break;

       case LIF_INDEX:
         strResult = SLIF_INDEX;
         break; 

       case LIF_NONE:
       default:
         strResult = SLIF_NONE;
         break;

      }
    
    return strResult.AllocSysString();
}

void CAXWingisCtrl::SetLayerInfo(LPCTSTR lpszNewValue) 
{
CString csNewValue(lpszNewValue);
CString csLIFNone(SLIF_NONE);
CString csLIFName(SLIF_NAME);
CString csLIFIndex(SLIF_INDEX);

  csNewValue.MakeUpper();
  csLIFNone.MakeUpper();
  csLIFName.MakeUpper();
  csLIFIndex.MakeUpper();
  
  if (csNewValue == csLIFNone)
    {
     m_layerinfo = LIF_NONE;
#ifdef _DEBUGWIN_ON
     Debug.Print("LayerInfo = " + CString(SLIF_NONE));
#endif
    }
  else if (csNewValue == csLIFName)
    { 
     m_layerinfo = LIF_NAME;  
#ifdef _DEBUGWIN_ON
     Debug.Print("LayerInfo = " + CString(SLIF_NAME));
#endif
    }
  else if (csNewValue == csLIFIndex)
    { 
     m_layerinfo = LIF_INDEX;  
#ifdef _DEBUGWIN_ON
     Debug.Print("LayerInfo = " + CString(SLIF_INDEX));
#endif
    }

  SetModifiedFlag();
}

CString CAXWingisCtrl::GetLayerToken()
{
	CString strResult;
	// TODO: Add your property handler here

	switch(m_layerinfo)
      {
       case LIF_NAME:
         strResult = "[" + m_layerName + "]";
#ifdef _DEBUGWIN_ON
         Debug.Print("LayerInfo = " + CString(SLIF_NAME) + " / LayerName = " + strResult);
#endif
         break;

       case LIF_INDEX:         
         strResult = "[" + m_layerIndex + "]";
#ifdef _DEBUGWIN_ON
         Debug.Print("LayerInfo = " + CString(SLIF_INDEX) + " / LayerIndex = " + strResult);
#endif
         break; 

       case LIF_NONE:
       default:
         strResult = "";
#ifdef _DEBUGWIN_ON
         Debug.Print("LayerInfo = " + CString(SLIF_NONE));
#endif
         break;
      }
    
    return strResult;
}

// Sets LayerName and LayerIndex to the values received in the
// last incoming command eg. MON
// Returns 0 if LIF_NONE or 1 if LIF_NAME, -INDEX
// Used to get the offset of the first ID in a Cmd String
int CAXWingisCtrl::SetLayerToken(CString sCmd)
{
 int iStart = 0;
 long lIndex;

     if (m_layerinfo == LIF_NONE)
       {        
        iStart = 0;
       }
     else if (m_layerinfo == LIF_NAME)
       {
        iStart = 1;
        m_layerName = GetCmdToken(sCmd,2);
        // Get the layer-index, too
        lIndex = FindString(m_layernameList, m_layerName);
        if (lIndex > -1)
          m_layerIndex = GetStringParam(m_layerindexList, lIndex);

       }
     else if (m_layerinfo == LIF_INDEX)
       {
        iStart = 1;
        m_layerIndex = GetCmdToken(sCmd,2);
        // Get the layer-name, too
        lIndex = FindString(m_layerindexList, m_layerIndex);
        if (lIndex > -1)
          m_layerName = GetStringParam(m_layernameList, lIndex);
       }

   return iStart;       
}

long CAXWingisCtrl::GetLayerIndex() 
{
	
	return atol((const char *)m_layerIndex);
}

void CAXWingisCtrl::SetLayerIndex(long nNewValue) 
{
 long lIndex;	

   if (nNewValue == -1)
     {
      m_layerIndex = "";
      m_layerName = "";
     }
   else
     {
      _ltoa(nNewValue, m_layerIndex.GetBuffer(15), 10);
      m_layerIndex.ReleaseBuffer(-1);

      if (IsValidList(m_layerindexList))
        { // Get the layer-name, too
         lIndex = FindString(m_layerindexList, m_layerIndex);
         if (lIndex > -1)
           m_layerName = GetStringParam(m_layernameList, lIndex);
        }
      }
#ifdef _DEBUGWIN_ON
    Debug.Print("LayerIndex = " + m_layerIndex + " / LayerName = " + m_layerName);
#endif
}

BSTR CAXWingisCtrl::DoSetActiveLayer(long lMode, LPCTSTR sLayer) 
{	
BOOL bRet = FALSE;
CString strResult("");

    if ( ! FunctionPermissionOk("DoSetActiveLayer"))
       return FALSE;


    if (m_connected)
      {
       if (csLastEvent == "ALA")
          csLastEvent = "";

       if (lMode == 2)
// mk separator          bRet = SendToWinGIS("[SAL][" + IntToStr(lMode) + "," + CString(sLayer) + "]");
         bRet = SendToWinGIS("[SAL][" + IntToStr(lMode) + m_csDDESeparator + CString(sLayer) + "]");
       else  
         bRet = SendToWinGIS("[SAL][" + IntToStr(lMode) + "]");

      }
	//return bRet;
    if (bRet)
	   {            
         WaitForReplyTimeout("ALA", 8000);
         strResult = csReturnString;
         csReturnString = "";
      }
#ifdef _DEBUGWIN_ON   
   Debug.Print("DoSetActiveLayer returns: '" + strResult + "'");
#endif
	return strResult.AllocSysString();
}

BSTR CAXWingisCtrl::DoGetActiveLayer() 
{
CString strResult;

    if ( ! FunctionPermissionOk("DoGetActiveLayer"))
       return FALSE;

	 strResult = "";
    if (m_connected)
      {  
       if (csLastEvent == "ALA") csLastEvent = "";
       if (SendToWinGIS("[TAL][]"))
	     {                
         WaitForReplyTimeout("ALA", 3000);
         strResult = csReturnString;
         csReturnString = "";
        }
      }
#ifdef _DEBUGWIN_ON
    Debug.Print("DoGetActiveLayer returns: '" + strResult + "'");
#endif
	 return strResult.AllocSysString();
}


BOOL CAXWingisCtrl::DoFindLayerByName(LPCTSTR sName) 
{
long lIndex;
CString csIndex;
BOOL bRet = FALSE;

  if ( ! FunctionPermissionOk("DoFindLayerByName"))
      return FALSE;


  if (m_connected)
    {

     if ((m_layerinfo == LIF_NAME) || (m_layerinfo == LIF_NONE))
       {
        if (SendToWinGIS("[LRQ][" + CString(sName) + "]"))
	      {   
             csLastEvent = "";
             WaitForReplyTimeout("LEX", 6000);
             if (CString(GetCmdToken(csReturnString, 2)) == CString(sName))
               {
                if (CString(GetCmdToken(csReturnString, 3)) == CString("1"))
                  {
                   bRet = TRUE;             
                  }
               }
             csReturnString = "";

#ifdef _DEBUGWIN_ON
             if (bRet)
               Debug.Print(sName + CString(" exists"));
             else
               Debug.Print(sName + CString(" exists NOT"));
#endif
          }
 
       }
     else if (m_layerinfo == LIF_INDEX)
       {
        // Get the layer-index        
        lIndex = FindString(m_layernameList, sName);
        if (lIndex > -1)
          {
           csIndex = GetStringParam(m_layerindexList, lIndex);

           if (SendToWinGIS("[LRQ][" + csIndex + "]"))
	        {   
             csLastEvent = "";
             WaitForReplyTimeout("LEX", 6000);
             if (CString(GetCmdToken(csReturnString, 2)) == csIndex)
               {
                if (CString(GetCmdToken(csReturnString, 3)) == CString("1"))
                  {
                   bRet = TRUE;             
                  }
               }
             csReturnString = "";

#ifdef _DEBUGWIN_ON
             if (bRet)
               Debug.Print(csIndex + CString(" exists"));
             else
               Debug.Print(csIndex + CString(" exists NOT"));
#endif
            } 
          }

       }
    }

  return bRet;
}

BOOL CAXWingisCtrl::DoFindLayerByIndex(long lIndex) 
{
CString csIndex;
BOOL bRet = FALSE;

  if ( ! FunctionPermissionOk("DoFindLayerByIndex"))
      return FALSE;

  if (m_connected)
    {

     if (m_layerinfo == LIF_INDEX)
       {
           csIndex = IntToStr(lIndex);

           if (SendToWinGIS("[LRQ][" + csIndex + "]"))
	        {   
             Debug.Print("FindLayerByIndex: '" + csIndex + "' /LayerInfo = 'Index'");
             csLastEvent = "";
             WaitForReply("LEX");
             if (CString(GetCmdToken(csReturnString, 2)) == csIndex)
               {
                if (CString(GetCmdToken(csReturnString, 3)) == CString("1"))
                  {
                   bRet = TRUE;             
                  }
               }
             csReturnString = "";

             if (bRet)
               Debug.Print(csIndex + CString(" exists"));
             else
               Debug.Print(csIndex + CString(" exists NOT"));
            } 
       

       }
    }

  return bRet;
}

BOOL CAXWingisCtrl::DoDeleteLayerByName(LPCTSTR sName) 
{
long lIndex;
CString csIndex;
BOOL bRet = FALSE;

  if ( ! FunctionPermissionOk("DoDeleteLayerByName"))
      return FALSE;

  if (m_connected)
    {
     if ((m_layerinfo == LIF_NAME) || (m_layerinfo == LIF_NONE))
       {
        bRet = SendToWinGIS("[LDN][" + CString(sName) + "]");

        Debug.Print("DeleteLayerByName: '" + CString(sName) + "' /LayerInfo = 'None' or 'Text'");     
       }
     else if (m_layerinfo == LIF_INDEX)
       {
        // Get the layer-index        
        lIndex = FindString(m_layernameList, sName);
        if (lIndex > -1)
          {
           csIndex = GetStringParam(m_layerindexList, lIndex);

           bRet = SendToWinGIS("[LDN][" + csIndex + "]");
	           
           Debug.Print("DeleteLayerByName: '" + csIndex + "' /LayerInfo = 'Index'");                      
          }
       }
     
     // force to load layerlist next time the user requests the layerlist
     bm_layernameListValid = FALSE;
    }

  return bRet;
}

BOOL CAXWingisCtrl::DoDeleteLayerByIndex(long lIndex) 
{
CString csIndex;
BOOL bRet = FALSE;

  if ( ! FunctionPermissionOk("DoDeleteLayerByIndex"))
      return FALSE;


  if (m_connected)
    {

     if (m_layerinfo == LIF_INDEX)
       {
           csIndex = IntToStr(lIndex);

           bRet = SendToWinGIS("[LDN][" + csIndex + "]");
	           
           Debug.Print("DeleteLayerByIndex: '" + csIndex + "' /LayerInfo = 'Index'");             
       }
    }

  return bRet;
}

// Layer Managment End
/////////////////////////////////////////////////////////////////////


BOOL CAXWingisCtrl::AlignWindow(long hWnd, short sPos) 
{
	RECT rect;
   RECT rDesk;
   BOOL bRet = FALSE;


   if (::GetWindowRect((HWND)hWnd, &rect))
      {
      if (::GetWindowRect(::GetDesktopWindow(), &rDesk))
         {          
         switch (sPos)
           {
           case 2:  // align to the right
            ::SetWindowPos((HWND)hWnd, HWND_TOP, rDesk.right +2 - rect.right + rect.left, rDesk.top, 0, 0, SWP_NOSIZE);
            bRet = DoSetWindowPos(0, 0, rect.right - rect.left, rect.bottom - rect.top, sPos);
            break;

           case 1:
            ::SetWindowPos((HWND)hWnd, HWND_TOP, rDesk.left, rDesk.top, 0, 0, SWP_NOSIZE);
            bRet = DoSetWindowPos(0, 0, rect.right - rect.left, rect.bottom - rect.top, sPos);
            break;

           case 3:
            ::SetWindowPos((HWND)hWnd, HWND_TOP, rDesk.left, rDesk.top, 0, 0, SWP_NOSIZE);
            bRet = DoSetWindowPos(0, 0, rect.right - rect.left, rect.bottom - rect.top, sPos);
            break;

           case 4:   // align to the bottom
            ::SetWindowPos((HWND)hWnd, HWND_TOP, rDesk.left, rDesk.bottom +2 - rect.bottom + rect.top, 0, 0, SWP_NOSIZE);
            bRet = DoSetWindowPos(0, 0, rect.right - rect.left, rect.bottom - rect.top, sPos);
            break;
        
           }
         }
      }

	return bRet;
}

BOOL CAXWingisCtrl::ShowWindow(long hWnd, BOOL bShow) 
{
   
   if (bShow)
     {
     //::ShowWindow((HWND)hWnd, SW_SHOW);  
     ::SetWindowPos((HWND)hWnd, HWND_TOP, 0, 0, 0, 0, SWP_NOREPOSITION | SWP_NOSIZE | SWP_NOSENDCHANGING | SWP_NOMOVE |
                                               SWP_SHOWWINDOW);
     }
   else
     {
     //::ShowWindow((HWND)hWnd, SW_HIDE);
     ::SetWindowPos((HWND)hWnd, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOREPOSITION | SWP_NOSIZE | SWP_NOSENDCHANGING | SWP_NOMOVE |
                                               SWP_HIDEWINDOW);
     }

	return TRUE;
}


BOOL CAXWingisCtrl::SetTimerState(BOOL bEnabled) 
{
   bTimerActive = bEnabled;
	if (bEnabled)
      return SetTimer(TID_AX, lTimerIntervall, NULL);
   else
      return KillTimer(TID_AX);
}

BOOL CAXWingisCtrl::SetTimerIntervall(long lMilliseconds) 
{
	lTimerIntervall = lMilliseconds;
	return TRUE;
}

void CAXWingisCtrl::OnTimer(UINT nIDEvent) 
{
 static BOOL bInsideEvent;
 static int iDelayedEventCount;

 __try
   {
	if (nIDEvent == TID_AX)
     {
      if ( ! bInsideEvent)
        {
         bInsideEvent = TRUE;
         Debug.PrintInt("Fire OnTimer, Queued: ", iDelayedEventCount, 10);
         FireOnTimer();
         
         while ((iDelayedEventCount > 0) && (bTimerActive))
           {
            Debug.PrintInt("Fire OnTimer, Queued: ", iDelayedEventCount, 10);
            FireOnTimer();
            iDelayedEventCount--;
           }
         iDelayedEventCount = 0;

         bInsideEvent = FALSE;
        }
      else
        {
         iDelayedEventCount++;
        }        
     }
   else
	  COleControl::OnTimer(nIDEvent);
   }
 __except(EXCEPTION_EXECUTE_HANDLER)
   {
    iDelayedEventCount = 0;
    bInsideEvent = FALSE;
   }
}

// Returns the time between call and reply from the GIS
// in milliseconds.
long CAXWingisCtrl::DoPingWinGIS()
{
 ULONG ulTicks;

  //if ( ! FunctionPermissionOk("DoPingWinGIS"))
  //  return -1;

 __try
   {
    ulTicks = -1;
   
    if (m_connected)
      {  
       ulTicks = GetTickCount();
       if (csLastEvent == "CCO") csLastEvent = "";
       if (SendToWinGIS("[CCO][]"))
         {                
          if (WaitForReply("CCO"))
            // A reply was received
            ulTicks = GetTickCount() - ulTicks;
          else
            // Timeout !
            ulTicks = -1;

          csReturnString = "";             
         }
       else
         ulTicks = -1;
      }    
	return ulTicks;
   }
 __except(EXCEPTION_EXECUTE_HANDLER)
   {
    return -1;
   }
}

BSTR CAXWingisCtrl::DoCreateText(double dAbsX, double dAbsY, long lHeight, LPCTSTR sText) 
{
CString strResult;

   if ( ! FunctionPermissionOk("DoCreateText"))
      return FALSE;

	strResult = DoCreateTextEx(dAbsX, dAbsY, "Arial", 0, lHeight, 0, sText);
   return strResult.AllocSysString();
}

BOOL CAXWingisCtrl::DoChangeLayerSettings(LPCTSTR sLayerName, LPCTSTR sNewName, long lOnOff, long lPosition, long lPattern, long lPatternColor) 
{
    if ( ! FunctionPermissionOk("DoChangeLayerSettings"))
       return FALSE;
	
	return DoChangeLayerSettingsEx(sLayerName, sNewName, lOnOff, 
                                   lPosition, -1, -1, 
                                   -1, -1, -1, 
                                   -1, -1, lPattern, 
                                   lPatternColor, -1);
}

BOOL CAXWingisCtrl::DoCreateLayer(LPCTSTR sNewName, long lOnOff, long lPosition, long lPattern, long lPatternColor) 
{
    if ( ! FunctionPermissionOk("DoCreateLayer"))
       return FALSE;

	return DoCreateLayerEx(sNewName, lOnOff, lPosition, 
                           0, 0, 42949672, 0, 0, 0, 0, lPattern, lPatternColor, 0);
}

BOOL CAXWingisCtrl::DoChangeObjectStyle(LPCTSTR sProgisID, long lObjectColor, long lPatternColor, long lPattern, BOOL bAutoCommit) 
{
    if ( ! FunctionPermissionOk("DoChangeObjectStyle"))
       return FALSE;
	
	return DoChangeObjectStyleEx(sProgisID, lObjectColor, -1, 
                                 -1, lPatternColor, lPattern,
                                 -1, -1, -1, bAutoCommit);

}


long CAXWingisCtrl::DoGetLoadedProjects() 
{
long lRet = FALSE;	
CString sCmd;

 if ( ! FunctionPermissionOk("DoGetLoadedProjects"))
    return FALSE;

    
 if (m_connected)
   {  
    Debug.Print("DoGetLoadedProjects"); 
    lRet = FALSE;
  
    if (csLastEvent == "PRL") csLastEvent = "";

    sCmd = CString("[LOA][]");
    lRet = SendToWinGIS(sCmd);
 
    if (lRet)
      {  
       lRet = -1;                     
       WaitForReply("PRL");
       lRet = hReturnList;
       hReturnList = -1;
       
      }
    else
      lRet = -1;
     
   }
 return lRet;
}


// ++ Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#22 02.10.00 
long CAXWingisCtrl::DoPrint(long lMode, LPCTSTR sFileName, long lCopies, BOOL bCollateCopies,
									 BOOL bShowProgress, BOOL bPrintMonitoring)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrint"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrint"); 
		lRet = TRUE;

		sCmd = "[PRN][" + IntToStr(lMode) + "][" + CString(sFileName) + "][" +
				IntToStr(lCopies) + "][" + BoolToStr(bCollateCopies) + "][" +
				BoolToStr(bShowProgress) + "][" + BoolToStr(bPrintMonitoring) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        
			if (csLastEvent == "PRNRES")
				{
				csLastEvent    = "";
				csReturnString = "";
				}
			if (WaitForReplyWithCond("PRNRES","PRN")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;
     
		}
	return lRet;
}

long CAXWingisCtrl::DoPrintSetPrintParam(LPCTSTR sDeviceName, LPCTSTR sPaperFormat, long lOrientation)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintSetPrintParam"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintSetPrintParam"); 
		lRet = TRUE;

		sCmd = "[PRNSPP][" + CString(sDeviceName) + "][" + CString(sPaperFormat) + "][" +
				 IntToStr(lOrientation) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNRES")
				{
				csLastEvent    = "";
				csReturnString = "";
				}

			if (WaitForReplyWithCond("PRNRES","PRNSPP")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}

long CAXWingisCtrl::DoPrintSetPagesParam(long lLayoutModel, BOOL bAutoRepagination,
						  long lHorizontalPages, long lVerticalPages, 
						  double dMarginsLeft, double dMarginsBottom, double dMarginsRight, double dMarginsTop, 
						  double dInnerLeft, double dInnerBottom, double dInnerRight, double dInnerTop,
						  double dOverlapLeft, double dOverlapBottom, double dOverlapRight, double dOverlapTop,
						  BOOL bNumberingOrder, long lNumberingFrom)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintSetPagesParam"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintSetPagesParam"); 
		lRet = TRUE;

/*		sCmd = "[PRNSPG][" + IntToStr(lLayoutModel) + "][" + BoolToStr(bAutoRepagination) + "][" + 
				IntToStr(lHorizontalPages) + "][" + IntToStr(lVerticalPages) + "][" +
				DToStr(dMarginsLeft) + "," + DToStr(dMarginsBottom) + "," +
				DToStr(dMarginsRight) + "," + DToStr(dMarginsTop) + "][" +
				DToStr(dInnerLeft) + "," + DToStr(dInnerBottom) + "," +
				DToStr(dInnerRight) + "," + DToStr(dInnerTop) + "][" +
				DToStr(dOverlapLeft) + "," + DToStr(dOverlapBottom) + "," +
				DToStr(dOverlapRight) + "," + DToStr(dOverlapTop) + "][" +
				BoolToStr(bNumberingOrder) + "][" + IntToStr(lNumberingFrom) + "]";
*/ 
		sCmd = "[PRNSPG][" + IntToStr(lLayoutModel) + "][" + BoolToStr(bAutoRepagination) + "][" + 
				IntToStr(lHorizontalPages) + "][" + IntToStr(lVerticalPages) + "][" +
				DToStr(dMarginsLeft) + m_csDDESeparator + DToStr(dMarginsBottom) + m_csDDESeparator +
				DToStr(dMarginsRight) + m_csDDESeparator + DToStr(dMarginsTop) + "][" +
				DToStr(dInnerLeft) + m_csDDESeparator + DToStr(dInnerBottom) + m_csDDESeparator +
				DToStr(dInnerRight) + m_csDDESeparator + DToStr(dInnerTop) + "][" +
				DToStr(dOverlapLeft) + m_csDDESeparator + DToStr(dOverlapBottom) + m_csDDESeparator +
				DToStr(dOverlapRight) + m_csDDESeparator + DToStr(dOverlapTop) + "][" +
				BoolToStr(bNumberingOrder) + "][" + IntToStr(lNumberingFrom) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNRES")
				{
				csLastEvent    = "";
				csReturnString = "";
				}

			if (WaitForReplyWithCond("PRNRES","PRNSPG")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}

long CAXWingisCtrl::DoPrintSetMapParam(long lMapID, long lRegion, LPCTSTR sViewName,
						  double dRegionX1, double dRegionY1, double dRegionX2, double dRegionY2,
						  BOOL bFitToPage, double PrintScale, BOOL bKeepScale,
						  double dFrameWidth, double dFrameHeight, long lFrameCentering,
						  double FrameLeft, double FrameTop)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintSetMapParam"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintSetMapParam"); 
		lRet = TRUE;
/* mk separator
		sCmd = "[PRNSM][" + IntToStr(lMapID) + "][" + IntToStr(lRegion) + "][" +
				CString(sViewName) + "][" + 
				DToStr(dRegionX1) + "," + DToStr(dRegionY1) + "," +
				DToStr(dRegionX2) + "," + DToStr(dRegionY2) + "][" +
				BoolToStr(bFitToPage) + "][" + DToStr(PrintScale) + "][" +
				BoolToStr(bKeepScale) + "][" +
				DToStr(dFrameWidth) + "," + DToStr(dFrameHeight) + "][" +
				IntToStr(lFrameCentering) + "][" + 
				DToStr(FrameLeft) + "," + DToStr(FrameTop) + "]";
*/ 
		sCmd = "[PRNSM][" + IntToStr(lMapID) + "][" + IntToStr(lRegion) + "][" +
				CString(sViewName) + "][" + 
				DToStr(dRegionX1) + m_csDDESeparator + DToStr(dRegionY1) + m_csDDESeparator +
				DToStr(dRegionX2) + m_csDDESeparator + DToStr(dRegionY2) + "][" +
				BoolToStr(bFitToPage) + "][" + DToStr(PrintScale) + "][" +
				BoolToStr(bKeepScale) + "][" +
				DToStr(dFrameWidth) + m_csDDESeparator + DToStr(dFrameHeight) + "][" +
				IntToStr(lFrameCentering) + "][" + 
				DToStr(FrameLeft) + m_csDDESeparator + DToStr(FrameTop) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNRES")
				{
				csLastEvent    = "";
				csReturnString = "";
				}

			if (WaitForReplyWithCond("PRNRES","PRNSM")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}


long CAXWingisCtrl::DoPrintSetLegendParam(long lLegendID,
						  double dFrameWidth, double dFrameHeight, long lFrameCentering,
						  long lFrameOridgin, double FrameLeft, double FrameTopOrBot)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintSetLegendParam"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintSetLegendParam"); 
		lRet = TRUE;
/* mk separator
		sCmd = "[PRNSL][" + IntToStr(lLegendID) + "][" +
				DToStr(dFrameWidth) + "," + DToStr(dFrameHeight) + "][" +
				IntToStr(lFrameCentering) + "][" + IntToStr(lFrameOridgin) + "][" +
				DToStr(FrameLeft) + "," + DToStr(FrameTopOrBot) + "]";
*/ 
		sCmd = "[PRNSL][" + IntToStr(lLegendID) + "][" +
				DToStr(dFrameWidth) + m_csDDESeparator + DToStr(dFrameHeight) + "][" +
				IntToStr(lFrameCentering) + "][" + IntToStr(lFrameOridgin) + "][" +
				DToStr(FrameLeft) + m_csDDESeparator + DToStr(FrameTopOrBot) + "]";
		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNRES")
				{
				csLastEvent    = "";
				csReturnString = "";
				}

			if (WaitForReplyWithCond("PRNRES","PRNSL")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}


long CAXWingisCtrl::DoPrintInsertLegend(LPCTSTR sLegendName,
						  double dFrameWidth, double dFrameHeight, long lFrameCentering,
						  long lFrameOridgin, double FrameLeft, double FrameTopOrBot)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintInsertLegend"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintInsertLegend"); 
		lRet = TRUE;
/* mk separator
		sCmd = "[PRNIL][" + CString(sLegendName) + "][" +
				DToStr(dFrameWidth) + "," + DToStr(dFrameHeight) + "][" +
				IntToStr(lFrameCentering) + "][" + IntToStr(lFrameOridgin) + "][" +
				DToStr(FrameLeft) + "," + DToStr(FrameTopOrBot) + "]";
*/ 
		sCmd = "[PRNIL][" + CString(sLegendName) + "][" +
				DToStr(dFrameWidth) + m_csDDESeparator + DToStr(dFrameHeight) + "][" +
				IntToStr(lFrameCentering) + "][" + IntToStr(lFrameOridgin) + "][" +
				DToStr(FrameLeft) + m_csDDESeparator + DToStr(FrameTopOrBot) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNRES")
				{
				csLastEvent    = "";
				csReturnString = "";
				}

			if (WaitForReplyWithCond("PRNRES","PRNIL")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}

long CAXWingisCtrl::DoPrintGetPrintParam()
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintGetPrintParam"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintGetPrintParam"); 
		lRet = TRUE;

		sCmd = "[PRNGPP]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNSPP")
				csLastEvent    = "";

			if (WaitForReply("PRNSPP")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}

long CAXWingisCtrl::DoPrintGetPagesParam()
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintGetPagesParam"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintGetPagesParam"); 
		lRet = TRUE;

		sCmd = "[PRNGPG]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNSPG")
				csLastEvent    = "";

			if (WaitForReply("PRNSPG")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}

long CAXWingisCtrl::DoPrintGetMapParam(long lMapID)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintGetMapParam"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintGetMapParam"); 
		lRet = TRUE;

		sCmd = "[PRNGM][" + IntToStr(lMapID) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNSM")
				csLastEvent    = "";

			if (WaitForReply("PRNSM")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}

long CAXWingisCtrl::DoPrintGetLegendParam(long lLegendID)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintGetLegendParam"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintGetLegendParam"); 
		lRet = TRUE;

		sCmd = "[PRNGL][" + IntToStr(lLegendID) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNSL")
				csLastEvent    = "";

			if (WaitForReply("PRNSL")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}
// -- Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#22 02.10.00 

// ++ Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#24 23.10.00 
long CAXWingisCtrl::DoPrintSetPictureParam(long lPictureID,
						  long lPictureLocation, long lFrameSizeInterpret,
						  double dFrameWidth, double dFrameHeight,
						  long lFrameCentering, long lFrameOridgin,
						  double FrameLeft, double FrameTopOrBot)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintSetPictureParam"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintSetPictureParam"); 
		lRet = TRUE;
/* mk separator
		sCmd = "[PRNSP][" + IntToStr(lPictureID) + "][" +
				IntToStr(lPictureLocation) + "][" + IntToStr(lFrameSizeInterpret) + "][" +
				DToStr(dFrameWidth) + "," + DToStr(dFrameHeight) + "][" +
				IntToStr(lFrameCentering) + "][" + IntToStr(lFrameOridgin) + "][" +
				DToStr(FrameLeft) + "," + DToStr(FrameTopOrBot) + "]";
*/ 
		sCmd = "[PRNSP][" + IntToStr(lPictureID) + "][" +
				IntToStr(lPictureLocation) + "][" + IntToStr(lFrameSizeInterpret) + "][" +
				DToStr(dFrameWidth) + m_csDDESeparator + DToStr(dFrameHeight) + "][" +
				IntToStr(lFrameCentering) + "][" + IntToStr(lFrameOridgin) + "][" +
				DToStr(FrameLeft) + m_csDDESeparator + DToStr(FrameTopOrBot) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNRES")
				{
				csLastEvent    = "";
				csReturnString = "";
				}

			if (WaitForReplyWithCond("PRNRES","PRNSP")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}

long CAXWingisCtrl::DoPrintGetPictureParam(long lPictureID)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintGetPictureParam"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintGetPictureParam"); 
		lRet = TRUE;

		sCmd = "[PRNGP][" + IntToStr(lPictureID) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNSP")
				csLastEvent    = "";

			if (WaitForReply("PRNSP")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}

long CAXWingisCtrl::DoPrintInsertPicture(LPCTSTR sPictureFile,
						  long lPictureLocation, long lFrameSizeInterpret,
						  double dFrameWidth, double dFrameHeight,
						  long lFrameCentering, long lFrameOridgin,
						  double FrameLeft, double FrameTopOrBot)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintInsertPicture"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintInsertPicture"); 
		lRet = TRUE;
/* mk separator
		sCmd = "[PRNIP][" + CString(sPictureFile) + "][" +
				IntToStr(lPictureLocation) + "][" + IntToStr(lFrameSizeInterpret) + "][" +
				DToStr(dFrameWidth) + "," + DToStr(dFrameHeight) + "][" +
				IntToStr(lFrameCentering) + "][" + IntToStr(lFrameOridgin) + "][" +
				DToStr(FrameLeft) + "," + DToStr(FrameTopOrBot) + "]";
*/ 
		sCmd = "[PRNIP][" + CString(sPictureFile) + "][" +
				IntToStr(lPictureLocation) + "][" + IntToStr(lFrameSizeInterpret) + "][" +
				DToStr(dFrameWidth) + m_csDDESeparator + DToStr(dFrameHeight) + "][" +
				IntToStr(lFrameCentering) + "][" + IntToStr(lFrameOridgin) + "][" +
				DToStr(FrameLeft) + m_csDDESeparator + DToStr(FrameTopOrBot) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNRES")
				{
				csLastEvent    = "";
				csReturnString = "";
				}

			if (WaitForReplyWithCond("PRNRES","PRNIP")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}

long CAXWingisCtrl::DoPrintSetSignatureParam(long lSignatureID,
						  LPCTSTR sSignature, LPCTSTR sFontName, BOOL bItalic,
						  BOOL bBold, BOOL bUnderlined, BOOL bTransparent,
						  long lFontSize, long lFontColor, long lFontBackColor,
						  long lTextAlignment, BOOL bOnPagesLocation,
						  BOOL bOnFirstPage, BOOL bOnInternalPages, BOOL bOnLastPage,
						  long lFrameCentering, long lFrameOridgin,
						  double FrameLeft, double FrameTopOrBot)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintSetSignatureParam"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintSetSignatureParam"); 
		lRet = TRUE;
/* mk separator
		sCmd = "[PRNSS][" + IntToStr(lSignatureID) + "][" + CString(sSignature) + "][" +
				CString(sFontName) + "][" + BoolToStr(bItalic) + "," + BoolToStr(bBold) + "," +
				BoolToStr(bUnderlined) + "," + BoolToStr(bTransparent) + "][" +
				IntToStr(lFontSize) + "][" + IntToStr(lFontColor) + "," + IntToStr(lFontBackColor) + "][" +
				IntToStr(lTextAlignment) + "][" + BoolToStr(bOnPagesLocation) + "][" +
				BoolToStr(bOnFirstPage) + "," + BoolToStr(bOnInternalPages) + "," + BoolToStr(bOnLastPage) + "][" +
				IntToStr(lFrameCentering) + "][" + IntToStr(lFrameOridgin) + "][" +
				DToStr(FrameLeft) + "," + DToStr(FrameTopOrBot) + "]";
*/  
		sCmd = "[PRNSS][" + IntToStr(lSignatureID) + "][" + CString(sSignature) + "][" +
				CString(sFontName) + "][" + BoolToStr(bItalic) + "," + BoolToStr(bBold) + m_csDDESeparator +
				BoolToStr(bUnderlined) + m_csDDESeparator + BoolToStr(bTransparent) + "][" +
				IntToStr(lFontSize) + "][" + IntToStr(lFontColor) + m_csDDESeparator + IntToStr(lFontBackColor) + "][" +
				IntToStr(lTextAlignment) + "][" + BoolToStr(bOnPagesLocation) + "][" +
				BoolToStr(bOnFirstPage) + m_csDDESeparator + BoolToStr(bOnInternalPages) + m_csDDESeparator + BoolToStr(bOnLastPage) + "][" +
				IntToStr(lFrameCentering) + "][" + IntToStr(lFrameOridgin) + "][" +
				DToStr(FrameLeft) + m_csDDESeparator + DToStr(FrameTopOrBot) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNRES")
				{
				csLastEvent    = "";
				csReturnString = "";
				}

			if (WaitForReplyWithCond("PRNRES","PRNSS")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}

long CAXWingisCtrl::DoPrintGetSignatureParam(long lSignatureID)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintGetSignatureParam"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintGetSignatureParam"); 
		lRet = TRUE;

		sCmd = "[PRNGS][" + IntToStr(lSignatureID) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNSS")
				csLastEvent    = "";

			if (WaitForReply("PRNSS")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}

long CAXWingisCtrl::DoPrintInsertSignature(LPCTSTR sSignature,
						  LPCTSTR sFontName, BOOL bItalic,
						  BOOL bBold, BOOL bUnderlined, BOOL bTransparent,
						  long lFontSize, long lFontColor, long lFontBackColor,
						  long lTextAlignment, BOOL bOnPagesLocation,
						  BOOL bOnFirstPage, BOOL bOnInternalPages, BOOL bOnLastPage,
						  long lFrameCentering, long lFrameOridgin,
						  double FrameLeft, double FrameTopOrBot)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintInsertSignature"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintInsertSignature"); 
		lRet = TRUE;
/* mk signature
		sCmd = "[PRNIS][" + CString(sSignature) + "][" + CString(sFontName) + "][" + 
				BoolToStr(bItalic) + "," + BoolToStr(bBold) + "," +
				BoolToStr(bUnderlined) + "," + BoolToStr(bTransparent) + "][" +
				IntToStr(lFontSize) + "][" + IntToStr(lFontColor) + "," + IntToStr(lFontBackColor) + "][" +
				IntToStr(lTextAlignment) + "][" + BoolToStr(bOnPagesLocation) + "][" +
				BoolToStr(bOnFirstPage) + "," + BoolToStr(bOnInternalPages) + "," + BoolToStr(bOnLastPage) + "][" +
				IntToStr(lFrameCentering) + "][" + IntToStr(lFrameOridgin) + "][" +
				DToStr(FrameLeft) + "," + DToStr(FrameTopOrBot) + "]";
*/ 
		sCmd = "[PRNIS][" + CString(sSignature) + "][" + CString(sFontName) + "][" + 
				BoolToStr(bItalic) + m_csDDESeparator + BoolToStr(bBold) + m_csDDESeparator +
				BoolToStr(bUnderlined) + m_csDDESeparator + BoolToStr(bTransparent) + "][" +
				IntToStr(lFontSize) + "][" + IntToStr(lFontColor) + m_csDDESeparator + IntToStr(lFontBackColor) + "][" +
				IntToStr(lTextAlignment) + "][" + BoolToStr(bOnPagesLocation) + "][" +
				BoolToStr(bOnFirstPage) + m_csDDESeparator + BoolToStr(bOnInternalPages) + m_csDDESeparator + BoolToStr(bOnLastPage) + "][" +
				IntToStr(lFrameCentering) + "][" + IntToStr(lFrameOridgin) + "][" +
				DToStr(FrameLeft) + m_csDDESeparator + DToStr(FrameTopOrBot) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNRES")
				{
				csLastEvent    = "";
				csReturnString = "";
				}

			if (WaitForReplyWithCond("PRNRES","PRNIS")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}

long CAXWingisCtrl::DoPrintStoreTemplate(LPCTSTR sFileName, long lOptions)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintStoreTemplate"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintStoreTemplate"); 
		lRet = TRUE;

		sCmd = "[PRNST][" + CString(sFileName) + "][" + IntToStr(lOptions) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNRES")
				{
				csLastEvent    = "";
				csReturnString = "";
				}

			if (WaitForReplyWithCond("PRNRES","PRNST")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}

long CAXWingisCtrl::DoPrintLoadTemplate(LPCTSTR sFileName)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintLoadTemplate"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintLoadTemplate"); 
		lRet = TRUE;

		sCmd = "[PRNRT][" + CString(sFileName) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNRES")
				{
				csLastEvent    = "";
				csReturnString = "";
				}

			if (WaitForReplyWithCond("PRNRES","PRNRT")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}

long CAXWingisCtrl::DoPrintGetObjectsInform()
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintGetObjectsInform"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintGetObjectsInform"); 
		lRet = TRUE;

		sCmd = "[PRNGOI]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNSOI")
				csLastEvent    = "";

			if (WaitForReply("PRNSOI")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}

long CAXWingisCtrl::DoPrintDeleteObject(long lObjectType, long lObjectID)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintDeleteObject"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintDeleteObject"); 
		lRet = TRUE;

		sCmd = "[PRNDO][" + IntToStr(lObjectType) + "][" + IntToStr(lObjectID) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNRES")
				{
				csLastEvent    = "";
				csReturnString = "";
				}

			if (WaitForReplyWithCond("PRNRES","PRNDO")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}

long CAXWingisCtrl::DoPrintMoveObject(long lObjectType, long lObjectID, long lOptions)
{
long lRet = FALSE;	
CString sCmd;

	if ( ! FunctionPermissionOk("DoPrintMoveObject"))
		return FALSE;
           
	if (m_connected)
		{  

		Debug.Print("DoPrintMoveObject"); 
		lRet = TRUE;

		sCmd = "[PRNMO][" + IntToStr(lObjectType) + "][" + IntToStr(lObjectID) + "][" +
					IntToStr(lOptions) + "]";

		lRet = SendToWinGIS(sCmd);

		if (lRet)
			{  
			lRet = -1;        

			if (csLastEvent == "PRNRES")
				{
				csLastEvent    = "";
				csReturnString = "";
				}

			if (WaitForReplyWithCond("PRNRES","PRNMO")) 
				{
				lRet = hReturnList;       
				hReturnList = -1;
				}
			}
		else
			lRet = -1;

		}	// End of the statement "if (m_connected)"

	return lRet;
}
// -- Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#24 03.11.00 

BOOL CAXWingisCtrl::DoProjectSaveAs(LPCTSTR sProjectName, BOOL bOverwrite, 
                                    BOOL bLTProject, LPCTSTR sPassword) 
{
BOOL bRet = FALSE;
CString csCmd;

    if ( ! FunctionPermissionOk("DoProjectSaveAs"))
       return FALSE;

    if (m_connected)
      { 
/* mk separator
       csCmd = "[PSA][" + CString(sProjectName) + "," + BoolToStr(bOverwrite) + "," +
               BoolToStr(bLTProject) + "," + CString(sPassword) + "]";
*/ 
       csCmd = "[PSA][" + CString(sProjectName) + m_csDDESeparator + BoolToStr(bOverwrite) + m_csDDESeparator +
               BoolToStr(bLTProject) + m_csDDESeparator + CString(sPassword) + "]";

       bRet = SendToWinGIS(csCmd);       

       Debug.Print("DoProjectSaveAs");

      }
	return bRet;
}

long CAXWingisCtrl::DoSelectAndSearchEx(long lType, long lDrawSel, LPCTSTR sObjectID, 
                                        LPCTSTR sObjectLayer, BOOL bSelFirstObj, 
                                        long lSelObjectType, LPCTSTR sSelObjectLayer, 
                                        double dSelBorders1, double dSelBorders2, 
                                        double dSelBorders3, double dSelBorders4, 
                                        BOOL bCutInside, double dOffset, long lEdges, 
                                        long lLineEnds, bool bDrawSelLine, 
                                        LPCTSTR sSelLineLayer, bool bZoom, bool bDeSelect, 
										long lSelAddSubXOr) 
{
long lRet = -1;	
CString sCmd;

 if ( ! FunctionPermissionOk("DoSelectAndSearchEx"))
    return -1;
    
 if (m_connected)
   {  

    Debug.Print("DoSelectAndSearchEx"); 

/* mk separator
    sCmd = "[SEL][" + IntToStr(lType) + "," + IntToStr(lDrawSel) + "," + CString(sObjectID) + "," +
           CString(sObjectLayer) + "," + BoolToStr(bSelFirstObj) + "," + 
           IntToStr(lSelObjectType) + "][" + CString(sSelObjectLayer) + "][" +
		   BoolToStr(bCutInside) + "," + DToStr(dOffset) + "," + IntToStr(lEdges) + "," +
           IntToStr(lLineEnds) + "][";
*/ 
    sCmd = "[SEL][" + IntToStr(lType) + m_csDDESeparator + IntToStr(lDrawSel) + m_csDDESeparator + CString(sObjectID) + m_csDDESeparator +
           CString(sObjectLayer) + m_csDDESeparator + BoolToStr(bSelFirstObj) + m_csDDESeparator + 
           IntToStr(lSelObjectType) + "][" + CString(sSelObjectLayer) + "][" +
		   BoolToStr(bCutInside) + m_csDDESeparator + DToStr(dOffset) + m_csDDESeparator + IntToStr(lEdges) + m_csDDESeparator +
           IntToStr(lLineEnds) + "][";

    if ((dSelBorders1 == 0) && (dSelBorders2 == 0) &&
        (dSelBorders3 == 0) && (dSelBorders4 == 0) )
      {
// mk separator        sCmd = sCmd + ",,,";
		sCmd = sCmd + m_csDDESeparator + m_csDDESeparator + m_csDDESeparator; 
      }
    else
      {
/*
        sCmd = sCmd + DToStr(dSelBorders1) + "," + DToStr(dSelBorders2) + "," +
                      DToStr(dSelBorders3) + "," + DToStr(dSelBorders4);
*/
        sCmd = sCmd + DToStr(dSelBorders1) + m_csDDESeparator + DToStr(dSelBorders2) + m_csDDESeparator +
                      DToStr(dSelBorders3) + m_csDDESeparator + DToStr(dSelBorders4);

      } 
/*
    sCmd = sCmd + "][" + BoolToStr(bDrawSelLine) + "," +
                  CString(sSelLineLayer) + "][" + BoolToStr(bZoom) + "," + "1][" + 
				  BoolToStr(bDeSelect) + "," + IntToStr(lSelAddSubXOr) + "]";
*/ 
    sCmd = sCmd + "][" + BoolToStr(bDrawSelLine) + m_csDDESeparator +
                  CString(sSelLineLayer) + "][" + BoolToStr(bZoom) + m_csDDESeparator + "1][" + 
				  BoolToStr(bDeSelect) + m_csDDESeparator + IntToStr(lSelAddSubXOr) + "]";

    lRet = SendToWinGIS(sCmd);


    if (lRet)
      {  
       lRet = -1;        
       csLastEvent = "";
       if (WaitForReply("OBS")) 
         {
          lRet = hReturnList;       
          hReturnList = -1;
         }
      }
    else
      lRet = -1;

   }
 return lRet;	
}

BOOL CAXWingisCtrl::DoSetGraphicMode(long lMode, BOOL bActivate) 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoSetGraphicMode"))
       return FALSE;

    if (m_connected)
      {  
       Debug.Print("DoSetGraphicMode");
// mk separator       bRet = SendToWinGIS("[SGM][" + IntToStr(lMode) + "," + BoolToStr(bActivate) + "]");

       bRet = SendToWinGIS("[SGM][" + IntToStr(lMode) + m_csDDESeparator + BoolToStr(bActivate) + "]");
	}
    return bRet;    
}

BOOL CAXWingisCtrl::DoSetSelectionOnGenerate(BOOL bOn) 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoSetSelectionOnGenerate"))
       return FALSE;

    if (m_connected)
      {  
       Debug.Print("DoSetSelectionOnGenerate");
       if (bOn)
          bRet = SendToWinGIS("[SMB][]");
       else
          bRet = SendToWinGIS("[SME][]");
      }
    return bRet;    
}

BOOL CAXWingisCtrl::DoCalcRoute(double dBeginX, double dBeginY, double dEndX, double dEndY)
{
 BOOL bRet = FALSE;
 
    if ( ! FunctionPermissionOk("DoCalcRoute"))
       return FALSE;

    if (m_connected)
      {  
       Debug.Print("DoCalcRoute");
/* mk separator
       bRet = SendToWinGIS("[STA][" + DToStr(dBeginX) + "," + DToStr(dBeginY) + "," +
                          DToStr(dEndX) + "," + DToStr(dEndY) + "," + "]");
*/ 
       bRet = SendToWinGIS("[STA][" + DToStr(dBeginX) + m_csDDESeparator + DToStr(dBeginY) + m_csDDESeparator +
                          DToStr(dEndX) + m_csDDESeparator + DToStr(dEndY) + m_csDDESeparator + "]");

      }
    return bRet;      
}

BOOL CAXWingisCtrl::DoSetVisibleMapArea(double dLLX, double dLLY, double dURX, double dURY) 
{
 BOOL bRet = FALSE;
 
    if ( ! FunctionPermissionOk("DoSetVisibleMapArea"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[SPP][" + DToStr(dLLX) + "][" + DToStr(dLLY) + "][" +
                           DToStr(dURX) + "][" + DToStr(dURY) + "]");
      }
    return bRet;      

}


BSTR CAXWingisCtrl::GetFingerPrint() 
{
	CString strResult;
	    
    strResult = m_fingerprint;

	return strResult.AllocSysString();
}

void CAXWingisCtrl::SetFingerPrint(LPCTSTR lpszNewValue) 
{	
    //m_fingerprint = lpszNewValue;

	//SetModifiedFlag();
}

BSTR CAXWingisCtrl::GetAppName() 
{
	CString strResult;
	
   strResult = m_appname;

	return strResult.AllocSysString();
}

void CAXWingisCtrl::SetAppName(LPCTSTR lpszNewValue) 
{	
    if (DesignPermissionOk())
      {
       m_appname = lpszNewValue;
	    SetModifiedFlag();
      }
}

// returns TRUE if valid license was found for m_appname
BOOL CAXWingisCtrl::CheckLicense()
{
structLicense *pLicense;
BOOL bRet = FALSE;
CString csErrMsg;
long lDays = -1;
ULONG ulLastAXMode = AXMODE_INVALID;

#ifdef AX_OPEN_VERSION
m_axmode = AXMODE_ADVANCED;
iActiveLicenseNr = 0;
Debug.Print("CheckLicense: Using open version, no license needed.");
return TRUE;
#endif


#ifdef AX_SPECIAL_VERSION
   CString csSpecialVersionAppName(AX_SPECIAL_VERSION);
   
   CString csUpperAppName = m_appname.Left(csSpecialVersionAppName.GetLength()); 
   
   csUpperAppName.MakeUpper();
   csSpecialVersionAppName.MakeUpper();
   if (csUpperAppName == csSpecialVersionAppName)
     {
      m_axmode = AXMODE_ADVANCED;
      iActiveLicenseNr = 0;
#ifdef _DEBUGWIN_ON
      Debug.Print("CheckLicense: Special " + csSpecialVersionAppName + " version - Advanced Runtime activated.");
#endif
      return TRUE;
     }
#endif

   iActiveLicenseNr = -1;
   if (DecodeAllLicenses(m_fingerprint, cLicenseList))
     {
       
      while ( ! cLicenseList.IsEmpty())
        {
         pLicense = (structLicense *)cLicenseList.RemoveHead();
         if (pLicense != NULL)
           {
            
            // test if this license is valid
            if( IsLicenseValid(pLicense, m_appname) == AXLIC_OK)
              {
               // use this license only, if it is better than the last valid
               // or if the last valid one is timedout
               if ((ulLastAXMode <= pLicense->ulAXMode ) || (lDays <= 0))
                 {
                  ulLastAXMode = pLicense->ulAXMode;
                  lDays = pLicense->lDaysLeft;
              
                  // ok set it as used, update and store if modified
                  bRet = TRUE;
                  m_axmode = pLicense->ulAXMode;
                  iActiveLicenseNr = pLicense->iKeyNr;
#ifdef DEBUG_LICMAN
                  Debug.Print("Valid License, AppName: " + m_appname + ", axmode: " + 
                              IntToStr(m_axmode) + ", iActLicNr: " + 
                              IntToStr(iActiveLicenseNr));
#endif
                  // update license as used only if appname not empty
                  if (UpdateLicense(pLicense, ((m_appname != "") ? TRUE : FALSE)) == TRUE)
                    {
                      if (StoreLicense(pLicense, m_fingerprint, csErrMsg))
                        Debug.Print("Updated and stored OK");

                    }
                 } // if ((ulLastAXMode <= pLicense->ulAXMode ) || (lDays <= 0))

              }
            else
              {
               // dont use it, but update the days and store if modified
               if (UpdateLicense(pLicense, FALSE) == TRUE)
                 {
                  StoreLicense(pLicense, m_fingerprint, csErrMsg);
                 }
              }

            delete pLicense;
           }

        }
     }

   // activate without a valid licensekey
   if (bRet == FALSE)   // no valid license found ?
     {
      // we have mercy, the user will be awarded a standard runtime license :))
      m_axmode = AXMODE_STANDARD;
      iActiveLicenseNr = 0;
#ifdef _DEBUGWIN_ON
      Debug.Print("CheckLicense: No valid license was found. Standard Runtime activated.");
#endif
      bRet = TRUE;
     }

   return bRet;
}

BOOL CAXWingisCtrl::FunctionPermissionOk(CString csFunctionName)
{
BOOL bRet = TRUE;

           if(csFunctionName == "DoAsciiExport") bRet = FALSE;
      else if(csFunctionName == "DoAsciiImport") bRet = FALSE;
//      else if(csFunctionName == "DoBeginMacro") bRet = FALSE;
//      else if(csFunctionName == "DoEndMacro") bRet = FALSE;   
      else if(csFunctionName == "DoLoadBitmap") bRet = FALSE;
      else if(csFunctionName == "DoCreateObject") bRet = FALSE;
      else if(csFunctionName == "DoGetNextUnusedID") bRet = FALSE;
      else if(csFunctionName == "DoChangeBitmapSettings") bRet = FALSE;
      else if(csFunctionName == "DoGetNextSymbolNumber") bRet = FALSE;
      else if(csFunctionName == "DoGetNextSymbolNumber") bRet = FALSE;
      else if(csFunctionName == "DoNotifyImportError") bRet = FALSE;
      else if(csFunctionName == "DoTempSetText") bRet = FALSE;
      else if(csFunctionName == "DoZoomAndSetText") bRet = FALSE;
      else if(csFunctionName == "DoMoveObject") bRet = FALSE;
      else if(csFunctionName == "DoSetObjectPos") bRet = FALSE;
      else if(csFunctionName == "DoGetLoadedProjects") bRet = FALSE;
// ++ Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#22 02.10.00 
      else if(csFunctionName == "DoPrint") bRet = FALSE;
      else if(csFunctionName == "DoPrintSetPrintParam") bRet = FALSE;
      else if(csFunctionName == "DoPrintSetPagesParam") bRet = FALSE;
      else if(csFunctionName == "DoPrintSetMapParam") bRet = FALSE;
      else if(csFunctionName == "DoPrintSetLegendParam") bRet = FALSE;
      else if(csFunctionName == "DoPrintSetPictureParam") bRet = FALSE;
      else if(csFunctionName == "DoPrintSetSignatureParam") bRet = FALSE;
      else if(csFunctionName == "DoPrintGetPrintParam") bRet = FALSE;
      else if(csFunctionName == "DoPrintGetPagesParam") bRet = FALSE;
      else if(csFunctionName == "DoPrintGetMapParam") bRet = FALSE;
      else if(csFunctionName == "DoPrintGetLegendParam") bRet = FALSE;
      else if(csFunctionName == "DoPrintGetPictureParam") bRet = FALSE;
      else if(csFunctionName == "DoPrintGetSignatureParam") bRet = FALSE;
      else if(csFunctionName == "DoPrintInsertLegend") bRet = FALSE;
      else if(csFunctionName == "DoPrintInsertPicture") bRet = FALSE;
      else if(csFunctionName == "DoPrintInsertSignature") bRet = FALSE;
      else if(csFunctionName == "DoPrintStoreTemplate") bRet = FALSE;
      else if(csFunctionName == "DoPrintLoadTemplate") bRet = FALSE;
      else if(csFunctionName == "DoPrintGetObjectsInform") bRet = FALSE;
      else if(csFunctionName == "DoPrintDeleteObject") bRet = FALSE;
      else if(csFunctionName == "DoPrintMoveObject") bRet = FALSE;
// -- Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#24 03.11.00 
      else if(csFunctionName == "DoProjectSaveAs") bRet = FALSE;
      else if(csFunctionName == "DoSelectAndSearchEx") bRet = FALSE;
      else if(csFunctionName == "DoSetGraphicMode") bRet = FALSE;
      else if(csFunctionName == "DoSetSelectionOnGenerate") bRet = FALSE;
      else if(csFunctionName == "DoCalcRoute") bRet = FALSE;
      else if(csFunctionName == "DoSetMapCanvas") bRet = FALSE;


      // GPS Functions
      else if(csFunctionName == "DoSendGPSPosition") bRet = FALSE;
      else if(csFunctionName == "GPSDelete") bRet = FALSE;
      else if(csFunctionName == "GPSDbColumn") bRet = FALSE;
      else if(csFunctionName == "GPSNew") bRet = FALSE;
      else if(csFunctionName == "GPSOffset") bRet = FALSE;
      else if(csFunctionName == "GPSProAsc") bRet = FALSE;
      else if(csFunctionName == "GPSProCalc") bRet = FALSE;
      else if(csFunctionName == "GPSProSet") bRet = FALSE;
      else if(csFunctionName == "GPSShow") bRet = FALSE;
      else if(csFunctionName == "GPSSymSet") bRet = FALSE;
      else if(csFunctionName == "GPSUpdate") bRet = FALSE;
      else if(csFunctionName == "GPSLoadGrf") bRet = FALSE;      
      


      else if(csFunctionName == "OnNotifyMoveObjects") bRet = FALSE;
      else if(csFunctionName == "OnNotifyMoveDimension") bRet = FALSE;
      else if(csFunctionName == "OnLinkPointToArea") bRet = FALSE;
      else if(csFunctionName == "OnCutArea") bRet = FALSE;
      else if(csFunctionName == "OnCutAreaLayers") bRet = FALSE;
//      else if(csFunctionName == "OnMacroBegin") bRet = FALSE;
//      else if(csFunctionName == "OnMacroEnd") bRet = FALSE;
      else if(csFunctionName == "OnNotifyBoxSelection") bRet = FALSE;
      else if(csFunctionName == "OnNotifyCircleSelection") bRet = FALSE;
      else if(csFunctionName == "OnDeclareNextUnusedID") bRet = FALSE;
      else if(csFunctionName == "OnBuildArea") bRet = FALSE;
      else if(csFunctionName == "OnAsciiExport") bRet = FALSE;
      else if(csFunctionName == "OnAsciiImport") bRet = FALSE;
// ++ Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#22 25.09.00 
      else if(csFunctionName == "OnPrintResult") bRet = FALSE;
      else if(csFunctionName == "OnPrintEvents") bRet = FALSE;
      else if(csFunctionName == "OnPrinterParamGet") bRet = FALSE;
      else if(csFunctionName == "OnPrintPagesParamGet") bRet = FALSE;
      else if(csFunctionName == "OnPrintMapParamGet") bRet = FALSE;
      else if(csFunctionName == "OnPrintLegendParamGet") bRet = FALSE;
      else if(csFunctionName == "OnPrintPictureParamGet") bRet = FALSE;
      else if(csFunctionName == "OnPrintSignatureParamGet") bRet = FALSE;
      else if(csFunctionName == "OnPrintObjectsInformGet") bRet = FALSE;
// -- Moskaliov AXWinGIS Printing Methods 1.1.0.BUILD#24 03.11.00 
      else if(csFunctionName == "OnProjectSaveAs") bRet = FALSE;
//++ Slava2 AXWinGIS .BUILD#24 23.10.00	  
	  else if(csFunctionName == "DoGetPolygonData") bRet = FALSE;
//-- Slava2 AXWinGIS .BUILD#24 23.10.00


/*
Only in Advanced Version available
[id(14)] void OnNotifyMoveObjects(long hProgisIDList);
[id(15)] void OnNotifyMoveDimension(double dDeltaX, double dDeltaY);
[id(19)] void OnLinkPointToArea(BSTR sIDPoint, BSTR sIDArea, double dArea);
[id(20)] void OnCutArea(long hIDFirstLayer, long hIDSecondLayer, long hIDTargetLayer, long hParam1List, long hParam2List, long hParam3List);
[id(23)] void OnMacroBegin();
[id(24)] void OnMacroEnd();
[id(30)] void OnNotifyBoxSelection(BSTR sLayerName, double dX1, double dY1, double dX2, double dY2);
[id(31)] void OnNotifyCircleSelection(BSTR sLayerName, double dXM, double dYM, double dR);
[id(32)] void OnDeclareNextUnusedID(BSTR sPointID, BSTR sLineID, BSTR sAreaID, BSTR sCircleID, BSTR sArcID, BSTR sSymbolID, BSTR sImageID, BSTR sTextID, BSTR sCaptionTextID, BSTR sChartID, BSTR sSplineID, BSTR sMeasureID, BSTR sOLEObjectID);
[id(33)] void OnBuildArea(long hSourceIDs, BSTR sTargetID, double dNewArea, BSTR sTargetLayer);
[id(34)] void OnAsciiExport(long hProgisIDList, long hCollumnList);
[id(35)] void OnAsciiImport(BSTR sProgisID, double dArea, long lColumnValueList, long lColumnNameList, long lColumnWidthList);
[id(38)] void OnPrintStatus(long lPrintStatus, long lMinScale, double dBottomLeftX, double dBottomLeftY, double dTopRightX, double dTopRightY);
[id(39)] void OnProjectSaveAs(BSTR sNewProjectName);

*/


#ifdef COMPILE_SPECIAL_HOFKARTE2
	bRet = TRUE;
#endif
#ifdef COMPILE_SPECIAL_HOFKARTE4
	bRet = TRUE;
#endif
#ifdef COMPILE_SPECIAL_AGRO
	bRet = TRUE;
#endif



      if (bRet == FALSE)
        {
         if (m_axmode == AXMODE_ADVANCED) 
           {
            bRet = TRUE;
           }
         else
           {
            m_csLastErrorMsg = "I'm sorry, the function " + csFunctionName + " is limited to the Advanced License model and not available in Standard Mode.";            
            AfxMessageBox(m_csLastErrorMsg,32);
           } 
        }

#ifdef COMPILE_SPECIAL_LABIS
      // functions locked in COMPILE_SPECIAL_LABIS
      // GPS Functions
           if(csFunctionName == "DoSendGPSPosition") bRet = FALSE;
      else if(csFunctionName == "GPSDelete") bRet = FALSE;
      else if(csFunctionName == "GPSDbColumn") bRet = FALSE;
      else if(csFunctionName == "GPSNew") bRet = FALSE;
      else if(csFunctionName == "GPSOffset") bRet = FALSE;
      else if(csFunctionName == "GPSProAsc") bRet = FALSE;
      else if(csFunctionName == "GPSProCalc") bRet = FALSE;
      else if(csFunctionName == "GPSProSet") bRet = FALSE;
      else if(csFunctionName == "GPSShow") bRet = FALSE;
      else if(csFunctionName == "GPSSymSet") bRet = FALSE;
      else if(csFunctionName == "GPSUpdate") bRet = FALSE;
      else if(csFunctionName == "GPSLoadGrf") bRet = FALSE;

      if (bRet == FALSE)
        {
         m_csLastErrorMsg = "I'm sorry, the function " + csFunctionName + " is limited to the Advanced License model and not available in LABIS Mode.";            
         AfxMessageBox(m_csLastErrorMsg,32);
        } 
#endif

#ifdef _DEBUGWIN_ON
      if (bRet == FALSE)
         Debug.Print("Asking to use: " + csFunctionName + ", NOT GRANTED!!");
      else
         Debug.Print("Asking to use: " + csFunctionName + ", ok");
#endif


  return bRet;
}

BOOL CAXWingisCtrl::DesignPermissionOk()
{
BOOL bRet = TRUE;

   if (AmbientUserMode() == FALSE)
     {
      // Design-mode is active
      if (bDesignLicenseOk == FALSE)
         bRet = FALSE;
     }

   return bRet;
}

// returns TRUE if valid design-mode license was found
BOOL CAXWingisCtrl::CheckDesignLicense()
{
structLicense *pLicense;
BOOL bRet = FALSE;
CString csAppName = "D";
CString csErrMsg;
long lDays = -1;
ULONG ulLastAXMode = AXMODE_INVALID;

#ifdef AX_OPEN_VERSION
m_axmode = AXMODE_ADVANCED;
iActiveLicenseNr = 0;
Debug.Print("CheckDesignLicense: Using open version, no license needed.");
return TRUE;
#endif


   iActiveLicenseNr = -1;
   if (DecodeAllLicenses(m_fingerprint, cLicenseList))
     {
       
      while ( ! cLicenseList.IsEmpty())
        {
         pLicense = (structLicense *)cLicenseList.RemoveHead();
         if (pLicense != NULL)
           {
            
            // test if this license is valid
            if( IsLicenseValid(pLicense, csAppName) == AXLIC_OK)
              {
               // use this license only, if it is better than the last valid
               // or if the last valid one is timedout
               if ((ulLastAXMode <= pLicense->ulAXMode ) || (lDays <= 0))
                 {
                  ulLastAXMode = pLicense->ulAXMode;
                  lDays = pLicense->lDaysLeft;

                  // ok set it as used, update and store if modified
                  bRet = TRUE;
                  m_axmode = pLicense->ulAXMode;
                  iActiveLicenseNr = pLicense->iKeyNr;
#ifdef DEBUG_LICMAN
                  Debug.Print("Valid License, AppName: " + csAppName + ", axmode: " + 
                               IntToStr(m_axmode) + ", iActLicNr: " + 
                               IntToStr(iActiveLicenseNr));
#endif
                   // update license as used 
                  if (UpdateLicense(pLicense, TRUE) == TRUE)
                    {
                     if (StoreLicense(pLicense, m_fingerprint, csErrMsg))
                       {
                        Debug.Print("Updated and stored OK");
                       }
                    }
                 }
              }
            else
              {
               // dont use it, but update the days and store if modified
               if (UpdateLicense(pLicense, FALSE) == TRUE)
                 {
                  if (StoreLicense(pLicense, m_fingerprint, csErrMsg))
                    {
                     ;
                    }
                 }
              }

            delete pLicense;
           }

        }
     }

   return bRet;
}

long CAXWingisCtrl::GetLicenseDays(BOOL bMustBeRuntime, BOOL bMustBeAdvanced) 
{
structLicense *pLicense;
BOOL bRet = FALSE;
CString csAppName;
CString csErrMsg;
long lDays = -1;
ULONG ulLastAXMode = AXMODE_INVALID;

	if (bMustBeRuntime)
	   csAppName = m_appname;
	else
      { 	
       if (m_appname == "")
	     csAppName = "D";
       else
         csAppName = m_appname;
	  }

    if (csAppName == "")
       return -1;   // AppName MUST be given if bMustBeRuntime == TRUE !
    
    if (DecodeAllLicenses(m_fingerprint, cLicenseList))
     {
       
      while ( ! cLicenseList.IsEmpty())
        {
         pLicense = (structLicense *)cLicenseList.RemoveHead();
         if (pLicense != NULL)
           {            
            // test if this license is valid
            if( IsLicenseValid(pLicense, csAppName, TRUE) == AXLIC_OK)
              {
               // license is ok for this AppName (or DesignMode) 
               if ( (bMustBeAdvanced && (pLicense->ulAXMode == AXMODE_ADVANCED)) 
               || (!bMustBeAdvanced) )  // correct axmode ?
                 {
                  // ok, get details
                  bRet = TRUE;
                  // use this license only, if it is better than the last valid
                  // or if the last valid one is timedout
                  if ((ulLastAXMode <= pLicense->ulAXMode ) || (lDays <= 0))
                    {
                     ulLastAXMode = pLicense->ulAXMode;
                     lDays = pLicense->lDaysLeft;
#ifdef DEBUG_LICMAN
               Debug.Print("GetLicenseDays: Using License, AppName: " + csAppName + ", axmode: " + 
                           IntToStr(ulLastAXMode) + ", iLicNr: " + 
                           IntToStr(pLicense->iKeyNr) + ", Days left: " + IntToStr(lDays));
#endif

                    }
                 }             
              }

            delete pLicense;
           }

	  } 
     }

   return lDays;
}

BOOL CAXWingisCtrl::AddLicense(LPCTSTR sLicenseKey) 
{
	CString csErrMsg;
	BOOL bRet = FALSE;

    m_csLastErrorMsg = "";
    if (CString(sLicenseKey) == "")
      {
       CAddLKeyDlg addDlg;
       if (addDlg.DoModal() == IDOK)
         bRet = TRUE;
      }
    else
      {
	   if (AddLicenseKey(sLicenseKey, csErrMsg))
	     bRet = TRUE;
       else
         m_csLastErrorMsg = csErrMsg;
      }

	return bRet;
}

BSTR CAXWingisCtrl::GetLastErrorMsg() 
{
	CString strResult = m_csLastErrorMsg;
	m_csLastErrorMsg = "";
	return strResult.AllocSysString();
}

/*
BSTR CAXWingisCtrl::GetSDDESeparator()
{
	CString strResult(m_chSep);
   
	return strResult.AllocSysString();
}

void CAXWingisCtrl::SetSDDESeparator(LPCTSTR lpszNewValue)
{

   if (lpszNewValue != NULL)
     {
      m_csDDESeparator = lpszNewValue;
      m_chSep = (char) lpszNewValue[0];
      SetModifiedFlag(); 
     }
}

*/

BOOL CAXWingisCtrl::DoSupressDialog(BOOL bSupress) 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoSupressDialog"))
       return FALSE;

    if (m_connected)
      {  
       if (bSupress)
         bRet = SendToWinGIS("[SDL][1]");
       else
         bRet = SendToWinGIS("[SDL][0]");
      }
	 return bRet;
}

BOOL CAXWingisCtrl::DoSetTextStyle(LPCTSTR sFontName, long lFontStyle, long lFontHeight, double dArc, long lColor, long lPos, LPCTSTR sLayer) 
{
	// [STS][Fontname][Fontstyle][Fontheight][Arc][Color][Pos][Layer]
CString csCmd;   
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoSetTextStyle"))
       return FALSE;

    if (m_connected)
      {  
       csCmd = "[STS][" + CString(sFontName) + "][" + IntToStr(lFontStyle) + "][" + 
               IntToStr(lFontHeight) + "][" + DToStr(dArc) + "][" + IntToStr(lColor) + "][" + 
               IntToStr(lPos) + "][" + CString(sLayer) + "]";
       
       bRet = SendToWinGIS(csCmd);
      }
	 return bRet;
}


long CAXWingisCtrl::DoGetSelectionState(long hProgisIDList) 
{
long lRet = -1;	
CString sCmd;
long hBlock;
long lIndex;
long i;
CString csID;
CString csState;
bool bFound;


 if ( ! FunctionPermissionOk("DoGetSelectionState"))
    return -1;
    
 if (m_connected)
   {
    if ((aBase.GetSize() > hProgisIDList) && (hProgisIDList >= 0))
      {
       hBlock = BuildCmdBlockFromList(hProgisIDList);
       if (hBlock > -1)
         {
          lRet = -1;
          for (lIndex=0; (lIndex < GetListSize(hBlock)) && lRet; lIndex++)
            {
             sCmd = CString("[IOS]") + GetStringParam(hBlock, lIndex);
             lRet = (BOOL)SendToWinGIS(sCmd);
#ifdef _DEBUGWIN_ON
             Debug.Print((const char*)sCmd);
#endif
            }
          DeleteList(hBlock);
         }       
      }
    else
      {
       SendToWinGIS("[IOS][]");
      }

    csLastEvent = "";
    if (SendToWinGIS("[END][]"))
      {             
       WaitForReply("SOB");
       lRet = CreateList();
       
       
       if (IsValidList(lRet) && IsValidList(hReturnList))
         {
          if (IsValidList(hProgisIDList))
            {
#ifdef _DEBUGWIN_ON
            Debug.Print("Input listhandle was valid. Return list contains the state of the requested IDs");
#endif

            for (lIndex=0; (lIndex < GetListSize(hProgisIDList)); lIndex++)
              {
               csID = GetStringParam(hProgisIDList, lIndex);
               i = 0;
               bFound = FALSE;
               do 
                {
                  if (csID == GetSubString(CString(GetStringParam(hReturnList, i)), 1, ','))
                    {
                     csState = GetSubString(CString(GetStringParam(hReturnList, i)), 2, ',');
                     AddStringParam(lRet, csState); 
                     bFound = TRUE;
  #ifdef _DEBUGWIN_ON
                     Debug.Print("Found: " + csID + ", State " + csState);
  #endif
                    }
                  i++;
                } while ((bFound == FALSE) && (i < GetListSize(hReturnList)) );
               if (bFound == FALSE)
                 {
                  AddStringParam(lRet, "-1"); 
  #ifdef _DEBUGWIN_ON
                     Debug.Print("NOT Found: " + csID);
  #endif
                 }
              } // for
            }
          else
            {
             // empty idlist was sent to wingis
             // result is a list of all selected objects
#ifdef _DEBUGWIN_ON
            Debug.Print("Input listhandle was invalid. Return list contains all IDs selected in wingis");
#endif
            for (lIndex=0; (lIndex < GetListSize(hReturnList)); lIndex++)
              {
               csID = GetSubString(CString(GetStringParam(hReturnList, lIndex)), 1, ',');
               AddStringParam(lRet, csID); 
              }
            }
         }
       else
         {
          if (IsValidList(lRet)) DeleteList(lRet);
          lRet = -1;
#ifdef _DEBUGWIN_ON
             Debug.Print("Invalid list in DoGetSelectionState");
#endif
         }

       DeleteList(hReturnList);
       hReturnList = -1;
      }
    else
      lRet = -1;

   }
 return lRet;
}



BOOL CAXWingisCtrl::DoSetNewObjectSelection(BOOL bSelect) 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoSetNewObjectSelection"))
       return FALSE;

    if (m_connected)
      {  
       if (bSelect)
         bRet = SendToWinGIS("[SCO][1]");
       else
         bRet = SendToWinGIS("[SCO][0]");
      }
	return bRet;
}


BOOL CAXWingisCtrl::DoRedraw() 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoRedraw"))
       return FALSE;

    if (m_connected)
      {  
       bRet = SendToWinGIS("[PRD][]");
      }
	return bRet;
}

BSTR CAXWingisCtrl::GetWinGisRelease() 
{
	CString strResult = m_csVersion;

	return strResult.AllocSysString();
}


// GPS functions

BOOL CAXWingisCtrl::DoSendGPSPosition(LPCTSTR ID, LPCTSTR sLatitude, LPCTSTR sLongitude, double dAltitude,
                                      LPCTSTR sType, BOOL bFixed, double dQuality, LPCTSTR sDate, 
                                      LPCTSTR sTime, long lSats, BOOL bOffline, BOOL bAlarm, 
                                      long lObjecttype, long lObjNr, long lAddInfoCtr, LPCTSTR sAddInfo)
{
CString sCmd;
BOOL bRet;

    if ( ! FunctionPermissionOk("DoSendGPSPosition"))
       return FALSE;

    if (m_connected)
      {  
       
       if (CString(ID) == "-1")
         sCmd = "[GPSPOS,EOD,";
       else
// mk separator  + m_csDDESeparator +
	   sCmd = "[GPSPOS," + CString(ID) + m_csDDESeparator;

       sCmd = sCmd + sLatitude + m_csDDESeparator + sLongitude + m_csDDESeparator + DToStr(dAltitude) + m_csDDESeparator + sType + m_csDDESeparator;
//       sCmd = sCmd + sLatitude + CString(",") + sLongitude + CString(",") + DToStr(dAltitude) + CString(",") + sType + CString(",");
       if (bFixed)
         sCmd = sCmd + "FIX,";
       else
         sCmd = sCmd + "NOFIX,";
                    
//       sCmd = sCmd + DToStr(dQuality) + CString(",") + sDate + CString(",") + sTime + CString(",") + IntToStrCut(lSats, '0', 2);
       sCmd = sCmd + DToStr(dQuality) + m_csDDESeparator + sDate + m_csDDESeparator + sTime + m_csDDESeparator + IntToStrCut(lSats, '0', 2);
//     sCmd = sCmd + "][" + BoolToStr(bOffline) + ",";
       sCmd = sCmd + "][" + BoolToStr(bOffline) + m_csDDESeparator;
       if (bAlarm)
          sCmd = sCmd + "T," ;
       else
          sCmd = sCmd + "F," ;

//     sCmd = sCmd + IntToStr(lObjecttype) + "," + IntToStr(lObjNr) + ",0]"; // + lAddInfoCtr + sAddInfo  (not used in WinGIS 3.4 R1)
       sCmd = sCmd + IntToStr(lObjecttype) + m_csDDESeparator +  IntToStr(lObjNr) + m_csDDESeparator +  "0]"; // + lAddInfoCtr + sAddInfo  (not used in WinGIS 3.4 R1)

       bRet = SendToWinGIS(sCmd);
      }
	return bRet;
}

// end GPS functions


BOOL CAXWingisCtrl::GPSDelete(LPCTSTR sID) 
{
 BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("GPSDelete"))
       return FALSE;

    bRet = SendToWinGIS("[GPSDEL," + CString(sID) + "]");
	return bRet;
}

BOOL CAXWingisCtrl::GPSDbColumn(long hColumnNameList, long hColumnTypeList, 
								long hColumnLengthList, long hPositionList) 
{
 BOOL bRet = FALSE;
 int i;
 CString csCmd;

    if ( ! FunctionPermissionOk("GPSDbColumn"))
       return FALSE;

	if ( IsValidList(hColumnNameList) && IsValidList(hColumnTypeList) &&
		 IsValidList(hColumnLengthList) && IsValidList(hPositionList) )
      {
       
       csCmd = "[GPSDBCOLUMN]";
       for (i=0; i < GetListSize(hColumnNameList); i++)
         {
/* mk separator          csCmd = csCmd + "[" + GetStringParam(hColumnNameList, i) + "," +
                                GetStringParam(hColumnTypeList, i) + "," +
                                GetStringParam(hColumnLengthList, i) + "," +
                                GetStringParam(hPositionList, i) + "]"; */
          csCmd = csCmd + "[" + GetStringParam(hColumnNameList, i) + m_csDDESeparator + 
                                GetStringParam(hColumnTypeList, i) + m_csDDESeparator + 
                                GetStringParam(hColumnLengthList, i) + m_csDDESeparator + 
                                GetStringParam(hPositionList, i) + "]";
         }
       
       bRet = SendToWinGIS(csCmd);

      }
    else
      m_csLastErrorMsg = "Invalid list handle(s)";


	return bRet;
}

BOOL CAXWingisCtrl::GPSNew(LPCTSTR sID, long lObjectType, long lDisplayType, 
						   BOOL bAutoZoom, LPCTSTR sLayerName, long lSymbolindexStand, 
						   long lSymbolindexUp, long lSymbolindexDown, 
						   long lSymbolindexLeft, long lSymbolindexRight) 
{
 BOOL bRet = FALSE;
 CString csTrueFalse;
 CString csCmd;

    if ( ! FunctionPermissionOk("GPSNew"))
      return FALSE;

	if (bAutoZoom)
		csTrueFalse = "T";
	else
		csTrueFalse = "F";
/* mk separator
    csCmd = "[GPSNEW," + CString(sID) + "," + IntToStr(lObjectType) + "," + 
             IntToStr(lDisplayType) + "," + csTrueFalse + "," + 
             CString(sLayerName) + "," + IntToStr(lSymbolindexStand) + "," +
             IntToStr(lSymbolindexUp) + "," + IntToStr(lSymbolindexDown) + "," +
             IntToStr(lSymbolindexLeft) + "," + IntToStr(lSymbolindexRight) + "]";
*/ 
    csCmd = "[GPSNEW," + CString(sID) + m_csDDESeparator +  IntToStr(lObjectType) + m_csDDESeparator +  
             IntToStr(lDisplayType) + m_csDDESeparator +  csTrueFalse + m_csDDESeparator +  
             CString(sLayerName) + m_csDDESeparator +  IntToStr(lSymbolindexStand) + m_csDDESeparator + 
             IntToStr(lSymbolindexUp) + m_csDDESeparator +  IntToStr(lSymbolindexDown) + m_csDDESeparator + 
             IntToStr(lSymbolindexLeft) + m_csDDESeparator +  IntToStr(lSymbolindexRight) + "]";

    bRet = SendToWinGIS(csCmd);

	return bRet;
}

BOOL CAXWingisCtrl::GPSOffset(double dOffset) 
{
 BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("GPSOffset"))
      return FALSE;

    bRet = SendToWinGIS("[GPSOFFSET][" + DToStr(dOffset) + "]");

    return bRet;
}

BOOL CAXWingisCtrl::GPSProAsc(LPCTSTR sSourceAscFile, LPCTSTR sSourceAdfFile, LPCTSTR sDestAscFile) 
{
 BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("GPSProAsc"))
      return FALSE;

    bRet = SendToWinGIS("[GPSPROASC][" + CString(sSourceAscFile) + "]" +
                                   "[" + CString(sSourceAdfFile) + "]" +
                                   "[" + CString(sDestAscFile) + "]");

    return bRet;
}

BOOL CAXWingisCtrl::GPSProCalc(double dX, double dY) 
{
 BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("GPSProCalc"))
      return FALSE;

    bRet = SendToWinGIS("[GPSPROCALC][" + DToStr(dX) + "][" + DToStr(dY) + "]");

    return bRet;
}

BOOL CAXWingisCtrl::GPSProSet(LPCTSTR sSourceProj, LPCTSTR sSourceDate, 
                              LPCTSTR sDestinationProj, LPCTSTR sDestinationDate) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("GPSProSet"))
     return FALSE;

   bRet = SendToWinGIS("[SETGPSPRO][" + CString(sSourceProj) + "][" + CString(sSourceDate) +
                       "][" + CString(sDestinationProj) + "][" + CString(sDestinationDate) + "]");
   return bRet;	
}


BOOL CAXWingisCtrl::GPSShow(LPCTSTR sID) 
{
 BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("GPSShow"))
       return FALSE;

    bRet = SendToWinGIS("[GPSSHOW][" + CString(sID) + "]");
	return bRet;
}

BOOL CAXWingisCtrl::GPSSymSet() 
{
 BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("GPSSymSet"))
       return FALSE;

    bRet = SendToWinGIS("[GPSSYMSET][]");

	return bRet;
}

BOOL CAXWingisCtrl::GPSUpdate(LPCTSTR sID, long lObjectType, long lDisplayType, 
                              BOOL bAutoZoom, LPCTSTR sLayerName, long lSymbolindexStand, 
                              long lSymbolindexUp, long lSymbolindexDown, 
                              long lSymbolindexLeft, long lSymbolindexRight) 
{
 BOOL bRet = FALSE;
 CString csCmd;
 CString csTrueFalse;
    if ( ! FunctionPermissionOk("GPSUpdate"))
      return FALSE;

	if (bAutoZoom)
		csTrueFalse = "T";
	else
		csTrueFalse = "F";
/* mk separator
    csCmd = "[GPSUPD," + CString(sID) + "," + IntToStr(lObjectType) + "," + 
             IntToStr(lDisplayType) + "," + csTrueFalse + "," + 
             CString(sLayerName) + "," + IntToStr(lSymbolindexStand) + "," +
             IntToStr(lSymbolindexUp) + "," + IntToStr(lSymbolindexDown) + "," +
             IntToStr(lSymbolindexLeft) + "," + IntToStr(lSymbolindexRight) + "]";
*/ 
    csCmd = "[GPSUPD," + CString(sID) + m_csDDESeparator +  IntToStr(lObjectType) + m_csDDESeparator +  
             IntToStr(lDisplayType) + m_csDDESeparator +  csTrueFalse + m_csDDESeparator +  
             CString(sLayerName) + m_csDDESeparator +  IntToStr(lSymbolindexStand) + m_csDDESeparator + 
             IntToStr(lSymbolindexUp) + m_csDDESeparator +  IntToStr(lSymbolindexDown) + m_csDDESeparator + 
             IntToStr(lSymbolindexLeft) + m_csDDESeparator +  IntToStr(lSymbolindexRight) + "]";

    bRet = SendToWinGIS(csCmd);

	return bRet;
}

BOOL CAXWingisCtrl::GPSLoadGrf(LPCTSTR sFileName) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("GPSLoadGrf"))
      return FALSE;

   bRet = SendToWinGIS("[LOADGRF][" + CString(sFileName) + "]");
	return bRet; 
}

BOOL CAXWingisCtrl::DoClearLayer(LPCTSTR sLayer, BOOL bSendDeleteEvents) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoClearLayer"))
     return FALSE;

   bRet = SendToWinGIS("[CLL][" + CString(sLayer) + "][" + BoolToStr(bSendDeleteEvents) + "]");
	return bRet;	
}


BOOL CAXWingisCtrl::DoPolygonPartition(LPCTSTR sProgisID, long lPartMode, long lSizeMode, 
									   double dWantedArea, double dWantedDist, double WantedAngle, 
									   BOOL bFillPoly, LPCTSTR sDestLayer, LPCTSTR sName) 
{
 BOOL bRet = FALSE;
 CString sCmd;

   sCmd = "[DPP]";

   if ( ! FunctionPermissionOk("DoPolygonPartition"))
     return FALSE;

/*  before 1.1.0.11 the Layer was sent as empty <[]> 
    since 1.1.0.11 the Layer from sLayerName or lLayerIndex or nothing is sent (depends on sLayerInfo)
    switch(m_layerinfo)
      {
       case LIF_NAME:
       case LIF_INDEX: 
         sCmd = sCmd + "[]";
         break;

       case LIF_NONE:
       default:         
         break;
      } */
    
   bRet = SendToWinGIS(sCmd + GetLayerToken() + "[" + CString(sProgisID) + "][" + IntToStr(lPartMode) + "][" + 
                      IntToStr(lSizeMode) + "][" + DToStr(dWantedArea) + "][" +  DToStr(dWantedDist) + "][" +
                      DToStr(WantedAngle) + "][" + BoolToStr(bFillPoly) + "][" +
                      sDestLayer + "][" + sName + "]");
   return bRet;	
}

BOOL CAXWingisCtrl::DoShowAllObjects() 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoShowAllObjects"))
     return FALSE;

   bRet = SendToWinGIS("[SAO][]");
	return bRet;	
}

BOOL CAXWingisCtrl::DoTellProjectPart() 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoTellProjectPart"))
     return FALSE;

   bRet = SendToWinGIS("[TPP][]");

	return bRet;
}

BOOL CAXWingisCtrl::DoDenyWingisSession(LPCTSTR sSessionName, BOOL bDeny) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoDenyWingisSession"))
     return FALSE;

   bRet = SendToWinGIS("[DWS][" + CString(sSessionName) + "][" + BoolToStr(bDeny) + "]");
	return bRet;	
}

BOOL CAXWingisCtrl::DoZoomAndSetText(long hProgisIDList, long hTextList) 
{
	// TODO: Add your dispatch handler code here

	return FALSE;
}

BOOL CAXWingisCtrl::DoSetChartStyle(long lChartType, double dWidth, double dHeight, 
                                    double dRadius, BOOL bRelative, 
                                    LPCTSTR sFontName, long lFontStyle, long lFontHeight, 
                                    double dArc, long lColor) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoSetChartStyle"))
     return FALSE;

   if (m_connected)
     {
      bRet = SendToWinGIS("[SCS][" + IntToStr(lChartType) + "][" + DToStr(dWidth)  + "][" + DToStr(dHeight) + "][" + 
                                     DToStr(dRadius) + "][" + BoolToStr(bRelative) + "][" + 
                                     CString(sFontName) + "][" + IntToStr(lFontStyle) + "][" + IntToStr(lFontHeight) + "][" + 
                                     DToStr(dArc) + "][" + IntToStr(lColor) + "]");
     }

	return bRet;
}

BOOL CAXWingisCtrl::DoSetChartColor(long hColorList) 
{
BOOL bRet = FALSE;	
long hBlock;
long lIndex;
CString sCmd;

 if ( ! FunctionPermissionOk("DoSetChartColor"))
    return FALSE;
    
 if (m_connected)
   {  
    if (IsValidList(hColorList))
      {         
       hBlock = BuildCmdBlockFromList(hColorList);
       if (IsValidList(hBlock))
         {
          bRet = TRUE;
          for (lIndex=0; (lIndex < GetListSize(hBlock)) && bRet; lIndex++)
            {
             sCmd = CString("[CCL]") + GetStringParam(hBlock, lIndex);
             bRet = SendToWinGIS(sCmd);             
            }
          DeleteList(hBlock);
         }       
       if (bRet)
         bRet = SendToWinGIS("[END][]");
      }
   }
 return bRet;
}

BOOL CAXWingisCtrl::DoPolygonOverlay(LPCTSTR sSourceLayer1, LPCTSTR sSourceLayer2, LPCTSTR sTargetLayer, 
                                     BOOL bSubtractive, BOOL bCheckAreas, 
                                     BOOL bDBOverlay, BOOL bCombineAreas) 
{
BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoPolygonOverlay"))
     return FALSE;

   if (m_connected)
     {
/* mk separator 
      bRet = SendToWinGIS("[DPO][" + CString(sSourceLayer1) + "][" + CString(sSourceLayer2)  + "][" + CString(sTargetLayer) + "]" + 
                               "[" + BoolToStr(bSubtractive) + "," + BoolToStr(bCheckAreas) + "," + 
                                     BoolToStr(bDBOverlay) + "," + BoolToStr(bCombineAreas) + 
                               "]");
*/ 
      bRet = SendToWinGIS("[DPO][" + CString(sSourceLayer1) + "][" + CString(sSourceLayer2)  + "][" + CString(sTargetLayer) + "]" + 
                               "[" + BoolToStr(bSubtractive) + m_csDDESeparator +  BoolToStr(bCheckAreas) + m_csDDESeparator +  
                                     BoolToStr(bDBOverlay) + m_csDDESeparator +  BoolToStr(bCombineAreas) + 
                               "]");
     }

	return bRet;
}

BOOL CAXWingisCtrl::DoRequestSymbolInfo(BOOL bWaitForEvent) 
{
BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoRequestSymbolInfo"))
     return FALSE;

   if (m_connected)
     {
      if (csLastEvent == "TSY") csLastEvent = "";

      bRet = SendToWinGIS("[RSY][]");
      if (bWaitForEvent && bRet)
        {
         bRet = WaitForReply("TSY");
        }
     }

	return bRet;
}


BOOL CAXWingisCtrl::DoModifyView(LPCTSTR sName, double dLLX, double dLLY, double dURX, double dURY, 
                                 long hXLayerList, long hIDList) 
{
 BOOL bRet = FALSE;
 CString csCmd;
 long i;
 long iLen;
 
    if ( ! FunctionPermissionOk("DoModifyView"))
       return FALSE;

    if (m_connected)
      {        
       csCmd = "[DMV][" + CString(sName) + "][" + DToStr(dLLX) + "][" + DToStr(dLLY) + "][" +
                                                  DToStr(dURX) + "][" + DToStr(dURY) + "]";

       if (IsValidList(hXLayerList))
         iLen = GetListSize(hXLayerList);
       else
         iLen = 0; 

       csCmd = csCmd + "[" + IntToStr(iLen) + "]";
       for(i=0;i < iLen; i++)
         {
          csCmd = csCmd + "[" + GetStringParam(hXLayerList, i) + "]";
         }

       if (IsValidList(hIDList))
         iLen = GetListSize(hIDList);
       else
         iLen = 0; 

       csCmd = csCmd + "[" + IntToStr(iLen) + "]";
       for(i=0;i < iLen; i++)
         {
          csCmd = csCmd + "[" + GetStringParam(hIDList, i) + "]";
         }

       bRet = SendToWinGIS(csCmd);
      }
    return bRet;
}

double CAXWingisCtrl::CStrToDouble(LPCTSTR sValue) 
{
  double dRet = 0.0;

	try
    {
     dRet = StrToD(sValue);
    }
  catch(CException* e)   
    {
     e->Delete();
    }

	return dRet;
}

BSTR CAXWingisCtrl::CDoubleToStr(double dValue) 
{
	CString strResult = "";

	try
    {
     strResult = DToStr(dValue);
    }
  catch(CException* e)   
    {
     e->Delete();
    }

	return strResult.AllocSysString();
}

void CAXWingisCtrl::SortListEx(long hSortList, long hAssociatedList1, long hAssociatedList2, long hAssociatedList3, long hAssociatedList4, long hAssociatedList5, long hAssociatedList6, long hAssociatedList7, long hAssociatedList8) 
{
#define SORTEX_LIST_COUNT 8

  unsigned long i;
  CStringArray* pList = NULL;
  CString pString;
  long lRet = -1;	
  unsigned long lListSize;
  bool bModified = TRUE;
  CString csBuffer = "";
  CString * pcsBuffer;
  CString * pcsAsocBuffer[SORTEX_LIST_COUNT];
  BOOL bAsc = TRUE;


    if ((aBase.GetSize() > hSortList) && (hSortList >= 0))
      {
       pList = (CStringArray*) aBase.GetAt(hSortList);
       if (pList != NULL)
         {
          try
           {          
            pcsBuffer = pList->GetData();
 
            lListSize = pList->GetSize();

            i = 0; 
            pcsAsocBuffer[i] = NULL; 
            pList = (CStringArray*) aBase.GetAt(hAssociatedList1);
            if (pList != NULL) pcsAsocBuffer[i] = pList->GetData();
            i++;
            pcsAsocBuffer[i] = NULL; 
            pList = (CStringArray*) aBase.GetAt(hAssociatedList2);
            if (pList != NULL) pcsAsocBuffer[i] = pList->GetData();
            i++;
            pcsAsocBuffer[i] = NULL; 
            pList = (CStringArray*) aBase.GetAt(hAssociatedList3);
            if (pList != NULL) pcsAsocBuffer[i] = pList->GetData();
            i++;
            pcsAsocBuffer[i] = NULL; 
            pList = (CStringArray*) aBase.GetAt(hAssociatedList4);
            if (pList != NULL) pcsAsocBuffer[i] = pList->GetData();
            i++;
            pcsAsocBuffer[i] = NULL; 
            pList = (CStringArray*) aBase.GetAt(hAssociatedList5);
            if (pList != NULL) pcsAsocBuffer[i] = pList->GetData();
            i++;
            pcsAsocBuffer[i] = NULL; 
            pList = (CStringArray*) aBase.GetAt(hAssociatedList6);
            if (pList != NULL) pcsAsocBuffer[i] = pList->GetData();
            i++;
            pcsAsocBuffer[i] = NULL; 
            pList = (CStringArray*) aBase.GetAt(hAssociatedList7);
            if (pList != NULL) pcsAsocBuffer[i] = pList->GetData();
            i++;
            pcsAsocBuffer[i] = NULL; 
            pList = (CStringArray*) aBase.GetAt(hAssociatedList8);
            if (pList != NULL) pcsAsocBuffer[i] = pList->GetData();
            i++;



             
            
            // loop until no more need to modify, ie. the sort is finished
            while (bModified == TRUE)
             {
              bModified = FALSE;
              // loop through the whole list
              for (i=0;(i < lListSize-1);i++)
                {
                 // test if the two items need to be exchanged
                 //if (CompareListItems( pList->GetAt(i), pList->GetAt(i+1), bAsc) )
                 if (CompareListItems(*(pcsBuffer+i), *(pcsBuffer+i+1), bAsc) )
                   {
                    // swap the two items
                    /*csBuffer = (*pList)[(i+1)];
                    pList->SetAt(i+1, pList->GetAt(i));                    
                    pList->SetAt(i, csBuffer);
                    */
                    memcpy(&csBuffer, (pcsBuffer+i), sizeof(CString));
                    memcpy((pcsBuffer+i), (pcsBuffer+i+1), sizeof(CString));
                    memcpy((pcsBuffer+i+1), &csBuffer, sizeof(CString));

                    for (int x=0;x < SORTEX_LIST_COUNT;x++)
                      {
                       memcpy(&csBuffer, (pcsAsocBuffer[x]+i), sizeof(CString));
                       memcpy((pcsAsocBuffer[x]+i), (pcsAsocBuffer[x]+i+1), sizeof(CString));
                       memcpy((pcsAsocBuffer[x]+i+1), &csBuffer, sizeof(CString));
                      }
              
                    bModified = TRUE;
                   }               
                }
              // loop reverse through the whole list 
              if (bModified)
                {
                bModified = FALSE; 
                for (i=lListSize-2;(i > 0);i--)
                  {
                   // test if the two items need to be exchanged
                   //if (CompareListItems(pList->GetAt(i), pList->GetAt(i+1), bAsc) )
                   if (CompareListItems(*(pcsBuffer+i), *(pcsBuffer+i+1), bAsc) )
                     {
                      // swap the two items
                      /*csBuffer = (*pList)[(i+1)];
                      pList->SetAt(i+1, pList->GetAt(i));
                      pList->SetAt(i, csBuffer);
                      */
                      memcpy(&csBuffer, (pcsBuffer+i), sizeof(CString));
                      memcpy((pcsBuffer+i), (pcsBuffer+i+1), sizeof(CString));
                      memcpy((pcsBuffer+i+1), &csBuffer, sizeof(CString));

                      for (int x=0;x < SORTEX_LIST_COUNT;x++)
                        {
                         memcpy(&csBuffer, (pcsAsocBuffer[x]+i), sizeof(CString));
                         memcpy((pcsAsocBuffer[x]+i), (pcsAsocBuffer[x]+i+1), sizeof(CString));
                         memcpy((pcsAsocBuffer[x]+i+1), &csBuffer, sizeof(CString));
                        }

                      bModified = TRUE; 
                     }               
                  }
                }

             }
           }
          catch(CException) { ; }
         }
      }

	return;
}

BOOL CAXWingisCtrl::DoRequestLineStyleInfo() 
{
	// TODO: Add your dispatch handler code here

	return FALSE;
}

BOOL CAXWingisCtrl::DoRequestHatchStyleInfo() 
{
	// TODO: Add your dispatch handler code here

	return FALSE;
}
 
BOOL CAXWingisCtrl::DoGetChartColor() 
{
	// TODO: Add your dispatch handler code here

	return FALSE;
}

BEGIN_EVENTSINK_MAP(CAXWingisCtrl, COleControl)
    //{{AFX_EVENTSINK_MAP(CAXWingisCtrl)
	ON_EVENT(CAXWingisCtrl, IDC_AXIPC , 1 /* OnConnect */, OnConnectAXIpc, VTS_I4 VTS_BSTR)
	ON_EVENT(CAXWingisCtrl, IDC_AXIPC , 2 /* OnDisconnect */, OnDisconnectAXIpc, VTS_I4)
	ON_EVENT(CAXWingisCtrl, IDC_AXIPC , 3 /* OnReceive */, OnReceiveAXIpc, VTS_BSTR VTS_I4)
   ON_EVENT(CAXWingisCtrl, IDC_AXIPC , 4 /* OnAck */, OnAckAXIpc, VTS_I4)  
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

BOOL CAXWingisCtrl::InitIPC()
{
  RECT rect; rect.bottom = 0;  rect.left = 0;  rect.right = 0;  rect.top = 0;
  
  m_bSendOk = TRUE;

  if (m_hWnd)
    {
     if ( ! bAssignedIpc)
       axipc.Create("", 0, rect, this, IDC_AXIPC );

     bAssignedIpc = TRUE;
     //return ConnectIPC();
     return TRUE;
    }
  else
    return FALSE;
}

BOOL CAXWingisCtrl::ConnectIPC()
{
  BOOL bRet = FALSE;
  long lError = AXIPCERR_STATUS_INVALID;
  CString csIP;
	
  SetTimeoutValues();

  if (!bAssignedIpc) InitIPC();
  
  // mk for remote axaccess
  csIP = GetRegValue("Software\\Progis\\AXWingis", "Remote");
  lError = axipc.Connect(csIP, 0);

//  lError = axipc.Connect("", 0);
  if (lError != AXIPCERR_NO_ERROR)
    {
     if (lError == AXIPCERR_WSAEWOULDBLOCK)
       {
        if (csLastEvent == "CONNECT") csLastEvent = "";
        if (WaitForReplyTimeout("CONNECT", m_lConnectTimeout))
          {
           bRet = TRUE;
           m_connected = TRUE;
          }
        else
          {
           SetErrorCode(AXIPCERR_WSAETIMEDOUT);
           bRet = FALSE;
          }
       }
     else
       {
        m_csLastErrorMsg = axipc.GetLastErrorMsg();
        bRet = FALSE;
       }
    }
  else
    bRet = TRUE;

#ifdef _DEBUGWIN_ON
    if (!bRet)
      Debug.Print("ConnectIPC failed: " + m_csLastErrorMsg);    
#endif

  return bRet;
}

BOOL CAXWingisCtrl::DisconnectIPC()
{
  if (bAssignedIpc)
    {
#ifdef _DEBUGWIN_ON
     Debug.Print("DisconnectIPC");
#endif
	 if (m_connected) // by mk
	 {
     m_connected = FALSE; 
     axipc.Disconnect();
     Invalidate();
	 }
    }
  return TRUE;
}

BOOL CAXWingisCtrl::SendIPC(CString sCmd, BOOL bWait)
{
  BOOL bRet = FALSE;
  long lResult = AXIPCERR_STATUS_INVALID;
  
  if (!bAssignedIpc) InitIPC();

  if (bAssignedIpc)
    {
     if (bWait)
       lResult = axipc.SendWait(sCmd);
     else
       lResult = axipc.Send(sCmd);

     if (lResult != AXIPCERR_NO_ERROR)
       {
        m_csLastErrorMsg = axipc.GetLastErrorMsg();
        bRet = FALSE;
       }
     else
       {
#ifdef _DEBUGWIN_ON
        Debug.Print("OutIPC: " + sCmd);
#endif

        bRet = TRUE;
       }
   }
  return bRet;
}

void CAXWingisCtrl::OnConnectAXIpc(long lSessionID, LPCTSTR sRemoteName) 
{
#ifdef _DEBUGWIN_ON
  Debug.Print("OnConnectAXIpc");
#endif

  m_lSessionID = lSessionID;
  //FireOnConnect();
  Invalidate();
  csLastEvent = "CONNECT";
}

void CAXWingisCtrl::OnDisconnectAXIpc(long lSessionID) 
{
  m_connected = FALSE;	
  bm_layernameListValid = FALSE;
  FireOnDisconnect();
  Invalidate();
}

void CAXWingisCtrl::OnReceiveAXIpc(LPCTSTR sCmd, long sSessionID) 
{
#ifdef _DEBUGWIN_ON
   Debug.Print("InIPC: " + CString(sCmd));
#endif

   PreProcessMsg(sCmd);
}

void CAXWingisCtrl::OnAckAXIpc(long lTransID) 
{
    lAckFlag = TRUE;
#ifdef _DEBUGWIN_ON
    Debug.Print("AckAXIpc for TransID " + IntToStr(lTransID));
#endif
}


void CAXWingisCtrl::SetTimeoutValues()
{
  CString csTO = "";

  // get the connect timeout value
  csTO = GetRegValue("Software\\Progis\\AXWingis", "ConnectTimeout");
  try
    {
     m_lConnectTimeout = 0;
     m_lConnectTimeout = StrToInt(csTO);
    }
  catch(int)
    {
     m_lConnectTimeout = 0;
    }

  if (m_lConnectTimeout == 0)
    {
     csTO = "2000";
     SetRegValue("Software\\Progis\\AXWingis", "ConnectTimeout", csTO);
     m_lConnectTimeout = StrToInt(csTO);
    }

  // get the SOK timeout value
  csTO = GetRegValue("Software\\Progis\\AXWingis", "SOKTimeout");
  try
    {
     m_lSOKTimeout = 0;
     m_lSOKTimeout = StrToInt(csTO);
    }
  catch(int)
    {
     m_lSOKTimeout = 0;
    }

  if (m_lSOKTimeout == 0)
    {
     csTO = "18000";
     SetRegValue("Software\\Progis\\AXWingis", "SOKTimeout", csTO);
     m_lSOKTimeout = StrToInt(csTO);
    }


}


long CAXWingisCtrl::DoGetCoordinateSystem() 
{
long lRet = -1;	

   if ( ! FunctionPermissionOk("DoGetCoordinateSystem"))
     return lRet;

   if (m_connected)
     {
      if (csLastEvent == "SCS") csLastEvent = "";
      csReturnString = "";

      if (SendToWinGIS("[RCS][]"))
        {  
         WaitForReplyTimeout("SCS", 4000);
         lRet = StrToInt(csReturnString);
         csReturnString = "";
        }      
     } 

	return lRet;
}

BOOL CAXWingisCtrl::DoGetCoordinates() 
{
  BOOL bRet = FALSE;

  if ( !FunctionPermissionOk("DoGetCoordinates") ) return FALSE;
  bRet = SendToWinGIS("[RMC][]");
    return bRet;
}

// Glukhov Bug#100 Build#152 05.03.01
// lOptions: 1=frame,2=invisible,4=black_transp., bits 8-15: transp.value
BOOL CAXWingisCtrl::DoSetPicture(LPCTSTR sLayer, double x1, double y1, double x2, double y2, long lLoRu, LPCTSTR sPath, long lOptions) 
{
  CString sCmd;
  BOOL bRet = FALSE;

  if ( lLoRu == 0 ){  // x1,y1 - left bottom corner
    if ( x1 >= x2 || y1 >= y2 ){
      if ( FunctionPermissionOk("OnSetPicture") ) FireOnSetPicture( 0, 0 );
      return bRet;
    }
  }
  if ( lLoRu == 1 ){  // x1,y1 - left top corner
    if ( x1 >= x2 || y1 <= y2 ){
      if ( FunctionPermissionOk("OnSetPicture") ) FireOnSetPicture( 0, 0 );
      return bRet;
    }
  }

  if ( !FunctionPermissionOk("DoSetPicture") ) return FALSE;
//  sCmd = "[SPI]" + GetLayerToken() + "[";       
/* mk separator
  sCmd = "[SPI][" + CString(sLayer) + "][";       
  sCmd += DToStr(x1) + "," + DToStr(y1) + "," + DToStr(x2) + "," + DToStr(y2) + "][";
  sCmd += CString(sPath) + "][" + IntToStr(lOptions) + "]";
*/ 
  sCmd = "[SPI][" + CString(sLayer) + "][";       
  sCmd += DToStr(x1) + m_csDDESeparator +  DToStr(y1) + m_csDDESeparator +  DToStr(x2) + m_csDDESeparator +  DToStr(y2) + "][";
  sCmd += CString(sPath) + "][" + IntToStr(lOptions) + "]";

  bRet = SendToWinGIS(sCmd);
  return bRet;
}

BOOL CAXWingisCtrl::DoGetProjectSettings() 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoGetProjectSettings"))
     return FALSE;

   bRet = SendToWinGIS("[RPS][]");

	return bRet;
}

BSTR CAXWingisCtrl::DoCreatePolyline(LPCTSTR sLayerName, long hCoordList) 
{
CString strResult="0";
CString sID;
CString sCmd;
BOOL bRet;

    if ( ! FunctionPermissionOk("DoCreatePolyline"))
       return strResult.AllocSysString();

    if ( ! IsValidList(hCoordList))
       return strResult.AllocSysString();

    
    if (m_connected)
      {  
       strResult="";

       sCmd = "[CPL][" + CString(sLayerName) + "]";       
       sCmd += BuildStringFromList(hCoordList);
       
       
       if (csLastEvent == "NPL") csLastEvent = "";
       csReturnString = "";

       bRet = SendToWinGIS(sCmd);
       bRet = WaitForReplyTimeout("RED", 100);
       bRet = SendToWinGIS("[END][]");
        
       if (bRet)
         {  
          WaitForReplyTimeout("NPL", 6000);
          strResult = csReturnString;
          csReturnString = "";
         }       
      }

	return strResult.AllocSysString();
}


BOOL CAXWingisCtrl::DoSendMouseAction(double dX, double dY, long lAction) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoSendMouseAction"))
     return FALSE;

   bRet = SendToWinGIS("[SMC][" + DToStr(dX) + "][" + DToStr(dY) + "][" + IntToStr(lAction) + "]");

	return bRet;
}

BOOL CAXWingisCtrl::DoCommitDigitalization() 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoCommitDigitalization"))
     return FALSE;

   bRet = SendToWinGIS("[CCD][]");

	return bRet;
}

BOOL CAXWingisCtrl::DoGenerateObjects(long hSourceLayerList, LPCTSTR sDestLayerName, long lMode) 
{
 BOOL bRet = FALSE;
 CString csCmd;

   if ( ! FunctionPermissionOk("DoGenerateObjects"))
     return FALSE;

   if (IsValidList(hSourceLayerList))
     {
      csCmd = "[GOB]";
      csCmd += BuildStringFromList(hSourceLayerList);
      csCmd += "[" + CString(sDestLayerName) + "][" + IntToStr(lMode) + "]";
      bRet = SendToWinGIS(csCmd);
     }

	return bRet;
}


BOOL CAXWingisCtrl::DoCutSelectedPolygons(LPCTSTR sDestLayerName, long lMode) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoCutSelectedPolygons"))
     return FALSE;

   bRet = SendToWinGIS("[CSO][" + CString(sDestLayerName) + "][" + IntToStr(lMode) + "]");

	return bRet;
}


BOOL CAXWingisCtrl::DoShowAllOnLayer(LPCTSTR sLayerName) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoShowAllOnLayer"))
     return FALSE;

   bRet = SendToWinGIS("[SOL][" + CString(sLayerName) + "]");

	return bRet;
}


BOOL CAXWingisCtrl::DoCreateSymbol(LPCTSTR sSymName, LPCTSTR sSymLibrary, double dX, double dY, double dAngle, double dSize) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoCreateSymbol"))
     return FALSE;

   bRet = SendToWinGIS("[CSY][" + CString(sSymName) + "][" + CString(sSymLibrary) + "][" + DToStr(dX) + "][" + DToStr(dY) + "][" + DToStr(dAngle) + "][" + DToStr(dSize) + "]");

	return bRet;
}


BOOL CAXWingisCtrl::DoTrimLine(LPCTSTR sProgisId, double dLength, long lMode) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoTrimLine"))
     return FALSE;

   bRet = SendToWinGIS("[LLN][" + CString(sProgisId) + "][" + DToStr(dLength) + "][" + IntToStr(lMode) + "]");

	return bRet;	
}


BOOL CAXWingisCtrl::DoCreateBuffer(LPCTSTR sProgisId, LPCTSTR sDestLayerName, long lCornerMode, 
                                   long lBufferMode, double dDistance) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoCreateBuffer"))
     return FALSE;

   bRet = SendToWinGIS("[CBU][" + CString(sProgisId) + "][" + CString(sDestLayerName) + "][" + IntToStr(lCornerMode) + 
                        "][" + IntToStr(lBufferMode) + "][" + DToStr(dDistance) + "]");

	return bRet;	
}


BOOL CAXWingisCtrl::DoUndo(long lStepCount) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoUndo"))
     return FALSE;

   bRet = SendToWinGIS("[UNDO][" + IntToStr (lStepCount) + "]");

	return bRet;
}

BOOL CAXWingisCtrl::DoRedo(long lStepCount) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoRedo"))
     return FALSE;

   bRet = SendToWinGIS("[REDO][" + IntToStr (lStepCount) + "]");

	return bRet;
}

long CAXWingisCtrl::DoReversePolyLine(LPCTSTR sProgisID) 
{
 long lRet = -1;

   if ( ! FunctionPermissionOk("DoReversePolyLine"))
     return lRet;

   if (m_connected)
     {
      if (csLastEvent == "RPL") csLastEvent = "";
      csReturnString = "";

      if (SendToWinGIS("[RPL][" + CString(sProgisID) + "]"))
        {  
         WaitForReplyTimeout("RPL", 4000);
         lRet = StrToInt(csReturnString);
         csReturnString = "";
        }      
     } 

	return lRet;
}


BOOL CAXWingisCtrl::DoSetTooltipText(LPCTSTR sProgisID, LPCTSTR sText) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoSetTooltipText"))
     return FALSE;

   bRet = SendToWinGIS("[SFT][" + CString(sProgisID) + "][" + CString(sText) + "]");

	return bRet;
}

BOOL CAXWingisCtrl::DoCreateView(LPCTSTR sViewName, BOOL bSaveRegion, BOOL bSaveLayers, BOOL bSetDefault) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoCreateView"))
     return FALSE;

   bRet = SendToWinGIS("[CVW][" + CString(sViewName) + "][" + BoolToStr(bSaveRegion) + "][" + 
                       BoolToStr(bSaveLayers) + "][" + BoolToStr(bSetDefault) + "]");

	return bRet;
}

BOOL CAXWingisCtrl::DoDeleteView(LPCTSTR sViewName) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoDeleteView"))
     return FALSE;

   bRet = SendToWinGIS("[DVW][" + CString(sViewName) + "]");

	return bRet;
}

BOOL CAXWingisCtrl::DoChangeImageSettings(LPCTSTR sProgisID, long lDisplayMode, long lTransparencyMode, long lTransparency) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoChangeImageSettings"))
     return FALSE;

   bRet = SendToWinGIS("[CIS][" + CString(sProgisID) + "][" + IntToStr(lDisplayMode) + "][" + 
                       IntToStr(lTransparencyMode) + "][" + IntToStr(lTransparency) + "]");

	return bRet;
}



BOOL CAXWingisCtrl::DoSetMapCanvas(long hWnd, long lMode) 
{
 BOOL bRet = FALSE;
 
    if ( ! FunctionPermissionOk("DoSetMapCanvas"))
       return FALSE;

    if (m_connected)
      {         
       bRet = SendToWinGIS("[SGP][" + IntToStr(hWnd) + "][" + IntToStr(lMode) + "]");
      }
    return bRet; 
}

BOOL CAXWingisCtrl::DoDeleteLayerList(long hLayerList) 
{
	
BOOL bRet = FALSE;
 
    if ( ! FunctionPermissionOk("DoSetMapCanvas"))
       return FALSE;

    if ((m_connected) && IsValidList(hLayerList))
      {         
       bRet = SendToWinGIS("[DLL]" + BuildStringFromList(hLayerList));
      }
    return bRet; 
}

BOOL CAXWingisCtrl::DoSetLayerSnap(LPCTSTR sLayerName, BOOL bOnOff)		
{
	BOOL bRet = FALSE;

	if(!FunctionPermissionOk("DoSetLayerSnap"))
		return FALSE;

	if(m_connected)
		bRet = SendToWinGIS("[LSNAP][" + CString(sLayerName) + "][" + BoolToStr(bOnOff) + "]");

	return bRet;
}

BOOL CAXWingisCtrl::DoSetMouseZoom(BOOL bOnOff)
{
	BOOL bRet = FALSE;

	if(!FunctionPermissionOk("DoSetMouseZoom"))
		return FALSE;

	if(m_connected)
		bRet = SendToWinGIS("[SETMZ][" + BoolToStr(bOnOff) + "]");

	return bRet;
}

BOOL CAXWingisCtrl::DoSelectSymbol()
{
	BOOL bRet = FALSE;

	if(!FunctionPermissionOk("DoSelectSymbol"))
		return FALSE;

	if(m_connected)
		bRet = SendToWinGIS("[SSD][]");

	return bRet;
}

BOOL CAXWingisCtrl::DoSetSnapRadius(double dRadius)
{
	BOOL bRet = FALSE;

	if(!FunctionPermissionOk("DoSetSnapRadius"))
		return FALSE;

	if(m_connected)
		bRet = SendToWinGIS("[SSR][" + DToStr(dRadius) + "]");

	return bRet;
}

BOOL CAXWingisCtrl::DoSetActiveSymbolEx(LPCTSTR sSymbolNumber, LPCTSTR sSymbolName, LPCTSTR sSymLibrary)
{
	BOOL bRet = FALSE;

	if(!FunctionPermissionOk("DoSetActiveSymbolEx"))
		return FALSE;

	if(m_connected)
		bRet = SendToWinGIS("[SAS][" + CString(sSymbolNumber) + "][" + CString(sSymbolName) + "][" + CString(sSymLibrary) +"]");

	return bRet;
}


long CAXWingisCtrl::DoCombinePolygons(LPCTSTR sDestLayerName, long hProgisIDList) 
{
	long lRet = FALSE;
	CString sCmd;
	long hBlock;
	long lIndex;
	
	if (!FunctionPermissionOk("DoSelectObjects"))
		return FALSE;
    
	if (m_connected)
	{  
		if ((aBase.GetSize() > hProgisIDList) && (hProgisIDList >= 0))
		{
			hBlock = BuildCmdBlockFromList(hProgisIDList);
			if (hBlock > -1)
			{
				lRet = TRUE;
				for (lIndex=0; (lIndex < GetListSize(hBlock)) && lRet; lIndex++)
				{
					sCmd = CString("[COMBP]") + CString("[") + CString(sDestLayerName) + CString("]") + GetStringParam(hBlock, lIndex);
					lRet = SendToWinGIS(sCmd);       
				}
				DeleteList(hBlock);
			
			}       

			lRet = -1; 
			csLastEvent = "";
			WaitForReply("GRD");
			lRet = hReturnList;
			hReturnList = -1;
		
		}
	}

	return lRet;

}

BOOL CAXWingisCtrl::DoSetAreaDivider(long Divider) 
{
	BOOL bRet = FALSE;

	if(!FunctionPermissionOk("DoSetAreaDivider"))
		return FALSE;

	if(m_connected)
		bRet = SendToWinGIS("[SETAD][" + IntToStr(Divider) +"]");

	return bRet;
}

BOOL CAXWingisCtrl::DoSetDiagramColor(long iIndex, LPCTSTR sColor) 
{
	BOOL bRet = FALSE;
	
	if(!FunctionPermissionOk("DoSetAreaDivider"))
		return FALSE;

	if(m_connected)
		bRet = SendToWinGIS("[SETDC][" + IntToStr(iIndex) +"][" + CString(sColor) + "]");

	return bRet;
}

BOOL CAXWingisCtrl::DoSetDiagramType(long iType) 
{
	BOOL bRet = FALSE;
	
	if(!FunctionPermissionOk("DoSetAreaDivider"))
		return FALSE;

	if(m_connected)
		bRet = SendToWinGIS("[DGTYP][" + IntToStr(iType) +"]");

	return bRet;
}

BOOL CAXWingisCtrl::DoSetDiagramSize(long lSize) 
{
	BOOL bRet = FALSE;
	
	if(!FunctionPermissionOk("DoSetAreaDivider"))
		return FALSE;

	if(m_connected)
		bRet = SendToWinGIS("[DGSIZ][" + IntToStr(lSize) +"]");

	return bRet;
}


BOOL CAXWingisCtrl::DoSetDiagramLabel(BOOL bOnOff, long iType) 
{
	BOOL bRet = FALSE;
	
	if(!FunctionPermissionOk("DoSetAreaDivider"))
		return FALSE;

	if(m_connected)
		if(bOnOff)
			bRet = SendToWinGIS("[DGLAB][1][" + IntToStr(iType) +"]");
		else
			bRet = SendToWinGIS("[DGLAB][0][" + IntToStr(iType) +"]");

	return bRet;
}



BOOL CAXWingisCtrl::DoSetGeotext(LPCTSTR sLayername, BOOL bOnOff) 
{
	
	BOOL bRet = FALSE;
	
	if(!FunctionPermissionOk("DoSetAreaDivider"))
		return FALSE;

	if(m_connected)
		if(bOnOff)
			bRet = SendToWinGIS("[SETGT][" + CString(sLayername) +"][1]");
		else
			bRet = SendToWinGIS("[SETGT][" + CString(sLayername) +"][0]");

	return bRet;
}

BOOL CAXWingisCtrl::DoGetLinePoint(LPCTSTR sProgisID, double dLength) 
{
	
	BOOL bRet = FALSE;
	if(m_connected)
		bRet = SendToWinGIS("[CLPNT][" + CString(sProgisID) + "][" + DToStr(dLength) + "]" );
		SendToWinGIS("[END][]" );
	return bRet;
}




BOOL CAXWingisCtrl::DoImportObjectFromFile(LPCTSTR sTxtFileName, LPCTSTR sDefFileName, long iObjType) 
{
	
    BOOL bRet = FALSE;
	if(m_connected)
		bRet = SendToWinGIS("[IMPTXT][" + CString(sTxtFileName) + "][" + CString(sDefFileName) +"][" + IntToStr(iObjType) +"]");
		SendToWinGIS("[END][]" );
	return bRet;
	
}

BOOL CAXWingisCtrl::DoExplodeSymbol(LPCTSTR sProgisId, LPCTSTR sDestLayerName) 
{
	BOOL bRet = FALSE;
	if(m_connected)
		bRet = SendToWinGIS("[EXPSYM][" + CString(sProgisId) + "][" + CString(sDestLayerName) + "]" );
		SendToWinGIS("[END][]" );
	return bRet;
}


long CAXWingisCtrl::DoCreateThematicMap(LPCTSTR sConnectionString, LPCTSTR sCommand, LPCTSTR sProgisIDFieldName, LPCTSTR sThematicFieldName, long iGroupingMode, long iGroupCount, LPCTSTR sGroupingModeName, LPCTSTR sSchemaName, LPCTSTR sDestLayerName, BOOL bDialog) 
{
    BOOL bRet = FALSE;
	if(m_connected)
		bRet = SendToWinGIS("[CTM][" + CString(sConnectionString)+"][" + CString(sCommand)+"][" + CString(sProgisIDFieldName)+"][" + (sThematicFieldName) +"]["  + IntToStr(iGroupingMode) +"][" + IntToStr(iGroupCount) +"][" + CString(sGroupingModeName) + "][" + CString(sSchemaName) + "][" + CString(sDestLayerName) +"][" + BoolToStr(bDialog) +"]");
		SendToWinGIS("[END][]" );
	return bRet;
}

BOOL CAXWingisCtrl::DoSetProjectID(LPCTSTR ProjectGUID) 
{
	BOOL bRet = FALSE;
	if(m_connected)
		bRet = SendToWinGIS("[SETUID][" + CString(ProjectGUID) +"]");
		SendToWinGIS("[END][]" );
	return bRet;
}


BSTR CAXWingisCtrl::DoGetProjectID() 
{
	CString strResult;
	BOOL bRet = FALSE;
	if(m_connected)
		bRet = SendToWinGIS("[GETUID][]");
		SendToWinGIS("[END][]" );
       

       if ( bRet)
         {  
             csLastEvent = "";
			 WaitForReplyTimeout("PGUID", 3000);
			 return csReturnString.AllocSysString();// csReturnString;
	   }
return 0;
}

BOOL CAXWingisCtrl::DoCutPolyWithCircle(LPCTSTR sProgisId, double dX, double dY, double dRadius) 
{

 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoCutPolyWithCircle"))
     return FALSE;

   bRet = SendToWinGIS("[CPC][" + CString(sProgisId) + "][" + DToStr(dX * 100.0) + "][" + DToStr(dY * 100.0) + "][" + DToStr(dRadius * 100.0) + "]");

	return bRet;

}

BOOL CAXWingisCtrl::DoCopyLayer(LPCTSTR sSourceLayer, LPCTSTR sDestLayer) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoCopyLayer"))
     return FALSE;

   bRet = SendToWinGIS("[COPLAY][" + CString(sSourceLayer) + "][" + CString(sDestLayer) + "]");

	return bRet;

}


double CAXWingisCtrl::DoGetPolygonArea(LPCTSTR sProgisID) 

{
	
	BOOL bRet = FALSE;
	double dRet = 0;

	if(m_connected)
	{
		//if (csLastEvent == "SPA") csLastEvent = "";
		
		bRet = SendToWinGIS("[GPA][" + CString(sProgisID) + "]" );
	//	SendToWinGIS("[END][]" );
		
		if (bRet)
		{
			WaitForReplyTimeout("SPA", 5000);
			dRet = CStrToDouble(csReturnString);
			csReturnString = "";
		}
	
	}
	return dRet;

}


BOOL CAXWingisCtrl::DoExplodeObjects(LPCTSTR sDestLayername) 
{

	BOOL bRet = FALSE;
	if(m_connected)
		bRet = SendToWinGIS("[EXPLODE][" + CString(sDestLayername) + "]" );
		SendToWinGIS("[END][]" );
	return bRet;
}

BOOL CAXWingisCtrl::DoExplodePolygonsToPoints(LPCTSTR sDestLayername, LPCTSTR sSymbolName) 
{
	BOOL bRet = FALSE;
	if(m_connected)
		bRet = SendToWinGIS("[PTP][" + CString(sDestLayername) + "][" + CString(sSymbolName) + "]" );
		SendToWinGIS("[END][]" );
	return bRet;
}

BOOL CAXWingisCtrl::DoSetSelectionFilter(long iFilter) 
{
   BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoSetSelectionFilter"))
     return FALSE;

   bRet = SendToWinGIS("[SOTF][" + IntToStr(iFilter) + "]");

	return bRet;
}


BOOL CAXWingisCtrl::DoGetPointOnLine(LPCTSTR sProgisID, long IDistance, BOOL bAutoCommit) 
{
   BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoGetPointOnLine"))
     return FALSE;

   bRet = SendToWinGIS("[CLPNT][" + CString(sProgisID) + "][" + IntToStr(IDistance) + "]");
	return bRet;	
}


BOOL CAXWingisCtrl::DoGetObjectCenter(LPCTSTR sProgisID, BOOL bAutoCommit) 
{
   BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoGetObjectCenter"))
     return FALSE;

   bRet = SendToWinGIS("[GCP][" + CString(sProgisID) + "][" + BoolToStr(bAutoCommit) + "]");
	return bRet;	
}

//  ************************************************************************                   
//  FUNCTION:  BuildCmdBlockFromListWithoutSpaces
//  PARAMETER: hList....List-Handle to build string from
//
//             This function loops through the list and adds the list-items
//             to a string, each item enclosed by [], and the number of items
//             in front of the items themselfs
//             These strings are limited to 200 items, each string with max 200
//             is added to a list.
//  RETURN:    A list handle containing all strings, each with the number of items
//             in front.
//  ************************************************************************                   
long CAXWingisCtrl::BuildCmdBlockFromListWithoutSpaces(long hList)
{
long hBlock;
long i;
long lSize;

  hBlock = CreateList();
  if (hBlock > -1)
     {
      lSize = GetListSize(hList);
      for (i=0; i < lSize; i+=WG_MAX_CMDSIZE)
         {
          AddStringParam(hBlock, BuildStringFromList(hList));
         }
     }

  return hBlock;
}

long CAXWingisCtrl::DoGetObjectPropertiesList(long lObjectType, long hObjectList, long sID, long dArea, long dLength, long dX, long dY, long sLayer, long sParam1, long sParam2, long sParam3) 
{
	// ZU ERLEDIGEN: F�gen Sie hier den Code f�r Ihre Dispatch-Behandlungsroutine ein

long lRet = -1;	
CString sCmd;
long hBlock;
long lIndex;

 
 if (m_connected)
   {
	 this->lOBPItems = 0;
    if ((aBase.GetSize() > hObjectList) && (hObjectList >= 0))
      {
       hBlock = BuildCmdBlockFromListWithoutSpaces(hObjectList);
       if (hBlock > -1)
         {
          lRet = -1;
		  	OPLsID = sID;
			OPLdArea = dArea; 
			OPLdLength = dLength; 
			OPLdX = dX; 
			OPLdY = dY; 
			OPLsLayer = sLayer; 
			OPLsParam1 = sParam1; 
			OPLsParam2 = sParam2; 
			OPLsParam3 = sParam3;
			bGetObjectPropertiesList = true;

          for (lIndex=0; (lIndex < GetListSize(hBlock)) ; lIndex++)
            {
			 CString dumms = "";
			 dumms.Format("%i",lObjectType);
             sCmd = CString("[TOP][") + dumms + "]" + GetStringParam(hBlock, lIndex);
             if ( !SendToWinGIS(sCmd) ) lRet = -1;
	#ifdef _DEBUGWIN_ON
             Debug.Print((const char*)sCmd);
	#endif
            }
          DeleteList(hBlock);
          csLastEvent = "";
			if (SendToWinGIS("[END][]"))
			{             
				lRet = -1;        
				csLastEvent = "";
				if (WaitForReply("OBP")) 
					lRet = lOBPItems; 
				lRet = lOBPItems; 
			}
			else
             lRet = -1;
            

         }       
      }
   }
 this->bGetObjectPropertiesList = false;
 return lRet;
}


long CAXWingisCtrl::DoGetAttachedText(LPCTSTR sProgisID) 

{
 long lRet = -1;

   if ( ! FunctionPermissionOk("DoGetAttachedText"))
     return lRet;

   if (m_connected)
     {
      if (csLastEvent == "SATT") csLastEvent = "";
      csReturnString = "";

      if (SendToWinGIS("[GATT][" + CString(sProgisID) + "]"))
        {  
         WaitForReplyTimeout("SATT", 2000);
         lRet = StrToInt(csReturnString);
         csReturnString = "";
        }      
     } 

	return lRet;
}

BOOL CAXWingisCtrl::DoSetIDBOnOff(BOOL bActive) 
{
BOOL bRet = FALSE;

    if ( ! FunctionPermissionOk("DoSetIDBOnOff"))
       return FALSE;

    if (m_connected)
      {  
       Debug.Print("DoSetIDBOnOff");
       bRet = SendToWinGIS("[IDB][" + BoolToStr(bActive) + "]");
      }

    return bRet;    
}


BOOL CAXWingisCtrl::DoSetTooltipDB(LPCTSTR sLayername, LPCTSTR sDBString) 
{
	BOOL bRet = FALSE;

	if(!FunctionPermissionOk("DoSetTooltipDB"))
		return FALSE;

	if(m_connected)
		bRet = SendToWinGIS("[SGTDB][" + CString(sLayername) + "][" + CString(sDBString) + "]");

	return bRet;
}



long CAXWingisCtrl::DoGetObjectPropertiesListEx(long lObjectType, long hObjectList, long sID, long dArea, long dLength, long dX, long dY, long sLayer, long sParam1, long sParam2, long sParam3, long timeout) 
{
            // TODO: Add your dispatch handler code here
long lRet = -1;   
CString sCmd;
long hBlock;
long lIndex;
 
 
 if (m_connected)
   {
             this->lOBPItems = 0;
    if ((aBase.GetSize() > hObjectList) && (hObjectList >= 0))
      {
       hBlock = BuildCmdBlockFromListWithoutSpaces(hObjectList);
       if (hBlock > -1)
         {
          lRet = -1;
								   
                                   OPLsID = sID;
                                   OPLdArea = dArea; 
                                   OPLdLength = dLength; 								   
								   OPLdX = dX; 
                                   OPLdY = dY; 
                                   OPLsLayer = sLayer; 
                                   OPLsParam1 = sParam1; 
                                   OPLsParam2 = sParam2; 
                                   OPLsParam3 = sParam3;
                                   bGetObjectPropertiesList = true;
 
          for (lIndex=0; (lIndex < GetListSize(hBlock)) ; lIndex++)
            {
                                    CString dumms = "";
                                    dumms.Format("%i",lObjectType);
             sCmd = CString("[TOP][") + dumms + "]" + GetStringParam(hBlock, lIndex);
             if ( !SendToWinGIS(sCmd) ) lRet = -1;
            #ifdef _DEBUGWIN_ON
             Debug.Print((const char*)sCmd);
            #endif
            }
          DeleteList(hBlock);
          csLastEvent = "";
                                   if (SendToWinGIS("[END][]"))
                                   {             
                                               lRet = -1;        
                                               csLastEvent = "";
                                               if (WaitForReplyTimeout("OBP",timeout)) 
                                                           lRet = lOBPItems; 
                                               lRet = lOBPItems; 
                                   }
                                   else
             lRet = -1;
            
 
         }       
      }
   }
 this->bGetObjectPropertiesList = false;
 return lRet;

} 

BOOL CAXWingisCtrl::DoPolyPartitionWithLine(LPCTSTR sDestLayer, LPCTSTR sPolyId, LPCTSTR sLineId) 
{
BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoPolyPartitionWithLine"))
     return FALSE;

   if (m_connected)
     {
      bRet = SendToWinGIS("[PPL][" + CString(sDestLayer) + "][" + CString(sPolyId)  + "][" + CString(sLineId) + "]"); 

     }

	return bRet;
}

BOOL CAXWingisCtrl::DoSetLayerPrintable(LPCTSTR sLayername, BOOL bPrintable) 
{
	
	BOOL bRet = FALSE;
	
	if(!FunctionPermissionOk("DoSetLayerPrintable"))
		return FALSE;

	if(m_connected)
		if(bPrintable)
			bRet = SendToWinGIS("[SLP][" + CString(sLayername) +"][1]");
		else
			bRet = SendToWinGIS("[SLP][" + CString(sLayername) +"][0]");

	return bRet;
}

double CAXWingisCtrl::DoGetLayerPolygonArea(LPCTSTR sLayername) 
{
	
	BOOL bRet = FALSE;
	double dRet = 0;

	if(m_connected)
	{
		//if (csLastEvent == "SPA") csLastEvent = "";
		
		bRet = SendToWinGIS("[GLPA][" + CString(sLayername) + "]" );
	//	SendToWinGIS("[END][]" );
		
		if (bRet)
		{
			WaitForReplyTimeout("SLPA", 5000);
			dRet = CStrToDouble(csReturnString);
			csReturnString = "";
		}
	
	}
	return dRet;

}


BOOL CAXWingisCtrl::DoAttachText(LPCTSTR sTextProgisID, LPCTSTR sAttachProgisID) 
{
 BOOL bRet = FALSE;

   if ( ! FunctionPermissionOk("DoAttachText"))
     return FALSE;

   bRet = SendToWinGIS("[ATT][" + CString(sTextProgisID) + "][" + CString(sAttachProgisID) + "]");

	return bRet;

}
