// DDEWnd.cpp : implementation file
//


#include "stdafx.h"
#include "AXWinGIS.h"
#include "AXWingisCtl.h"
#include <dde.h>

#define DDE_TIMEOUT 5000
#define DDE_RETRY   500

#define MAX_CMD_LEN 4096

CDebug Debug2;


BEGIN_MESSAGE_MAP(CDDEWnd, CFrameWnd)
	//{{AFX_MSG_MAP(DDEWnd)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
   ON_MESSAGE(WM_DDE_INITIATE, OnDDEInitiate)
   ON_MESSAGE(WM_DDE_TERMINATE, OnDDETerminate)
   ON_MESSAGE(WM_DDE_ACK, OnDDEAck)
   ON_MESSAGE(WM_DDE_EXECUTE, OnDDEExecute)

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DDEWnd message handlers
/////////////////////////////////////////////////////////////////////////////
// CDDEWnd


CDDEWnd::CDDEWnd() : CFrameWnd()
{
  //CFrameWnd::CFrameWnd()
  bActive = FALSE;
  bConnected = FALSE;
}

/*CDDEWnd::~CDDEWnd()
{
}*/


BOOL CDDEWnd::Init(CAXWingisCtrl *pParent)
{
  RECT rec;
   
  rec.top = 50;
  rec.left = 50;
  rec.right = 530-200;
  rec.bottom = 340-150;

  m_pParent = pParent;
  	 
  if (!bActive )
    if (Create(NULL,"DDE Frame Window"
              ,WS_OVERLAPPEDWINDOW,rec,NULL
              ,WS_EX_LEFT,NULL))
      {     
       bActive = TRUE;
#ifdef _DEBUG       
       ShowWindow(SW_SHOW);
#else
       ShowWindow(SW_HIDE); 
#endif
       Debug2.Print("INIT OK");
      }

  return(bActive);
}

BOOL CDDEWnd::Terminate(void)
{
  if (bActive)
    {
     if (bConnected)
        SendDDETerminate();
        
     DestroyWindow();
     bActive = FALSE;
     Debug2.Print("DEACTIVATED OK");
    }
  return(0);
}


/////////////////////////////////////////////////////////////////////////////
// CDDEWnd message handlers

//  ************************************************************************                   
//  FUNCTION:  OnDDEInitiate
//  PARAMETER: WPARAM wParam, LPARAM lParam   
//             wParam : WinGIS hWnd
//             lParam : Server & Topic
//  RETURN:    0
//  DESC: This function checks for Server and Topic on WM_DDE_INITIATE
//        and sends WM_DDE_ACK if they match
//  ************************************************************************
LRESULT CDDEWnd::OnDDEInitiate(WPARAM wParam, LPARAM lParam) 
{
ATOM aApp;       // Server application name.
ATOM aTop;       // Server topic name.

Debug2.Print("DDE_INITIATE OK");

if (bActive) 
  {
   aApp=GlobalAddAtom((const char*)m_pParent->m_server);//Server and 
   aTop=GlobalAddAtom((const char*)m_pParent->m_topic);// topic names that 
                            // match the names used in the WinGIS.INI file
   // If the App and Topic match.
   if (LOWORD(lParam) == aApp && HIWORD(lParam) == aTop)
    {
     // Save the window handle of calling app (WinGIS).
     WinGISWnd = (HWND)wParam; // WinGISWnd is a DDEWindow member variable
     // Acknowledge receipt of the initiate message with SEND message.
     ::SendMessage (WinGISWnd, WM_DDE_ACK, (unsigned int)m_hWnd
                  , lParam);
     
     Debug2.Print("AFTER ACK"); 

     m_pParent->GotDDEInitiate();        // Notify

     Debug2.Print("AFTER Notify init"); 

#ifdef _DEBUG               
   //::MessageBox(NULL, "INITIATE to me!", "JO", MB_OK);
#endif  

     bConnected = TRUE;
    }

   // Otherwise this is not an application that we want to talk to.
   // Delete the global atoms just created
   GlobalDeleteAtom(aApp);
   GlobalDeleteAtom(aTop);
    
  }  
  return 0;
}
//  ************************************************************************                   
//  FUNCTION:  OnDDETerminate
//  PARAMETER: WPARAM wParam, LPARAM lParam
//             wParam : WinGIS hWnd
//  RETURN:    0
//  DESC: This function sends WM_DDE_TERMINATE to the WinGIS hWnd
//  ************************************************************************
LRESULT CDDEWnd::OnDDETerminate(WPARAM wParam, LPARAM lParam) 
{
   
   if ((uiLastDDEMsg != WM_DDE_TERMINATE) && bConnected) 	
     {

	  if (m_pParent->m_useSend)
	    ::SendMessage((HWND)wParam, WM_DDE_TERMINATE, (WPARAM)m_hWnd, 0L);
	  else
	    ::PostMessage((HWND)wParam, WM_DDE_TERMINATE, (WPARAM)m_hWnd, 0L);
	 }
   else
     {
	  uiLastAck = DDE_ACK_TERM;
	  uiLastDDEMsg = NO_MSG;
     }
   
   bConnected = FALSE;

   m_pParent->GotDDETerminate();

   return 0;
}             

//  ************************************************************************                   
//  FUNCTION:  OnDDEAck
//  PARAMETER: WPARAM wParam, LPARAM lParam
//  RETURN:    0
//  DESC: This function does nothing in this demo
//  ************************************************************************
LRESULT CDDEWnd::OnDDEAck(WPARAM wParam, LPARAM lParam)
{
UINT ulStatus;
ULONG hCommands;

   if (uiLastDDEMsg == WM_DDE_EXECUTE)
     {
      uiLastDDEMsg = NO_MSG;
      uiLastAck = DDE_ACK_EXEC;
      __try 
        { 
         if ( ! m_pParent->m_useSend)
           {
            UnpackDDElParam(WM_DDE_ACK, lParam, &ulStatus,(PUINT) &hCommands);
            GlobalFree((HGLOBAL)hCommands);
            FreeDDElParam(WM_DDE_EXECUTE, lParam);        
           }
        }
      __except(EXCEPTION_EXECUTE_HANDLER) { uiLastDDEMsg = NO_MSG; }
     } 

   return 0;
}             
                                                               
                                                               
//  ************************************************************************                   
//  FUNCTION:  OnDDEExecute
//  PARAMETER: WPARAM wParam, LPARAM lParam   
//             wParam : WinGIS hWnd
//             lParam : pointer to DDE Execute cmd
//  RETURN:    0
//  DESC: This function receives a execute cmd string and parses it.
//        Only received following a successful initiate.
//  ************************************************************************
LRESULT CDDEWnd::OnDDEExecute(WPARAM wParam, LPARAM lParam)
// wparam is the window handle of the client (calling) application.
// lparam has the command string.
{
   UINT hDummy;          // not used
   HGLOBAL hCommands = 0;  // handle to the global element we may have to delete
   char* pszCommands;   // pointer to the command string
   char acCmd[MAX_CMD_LEN];


   UnpackDDElParam(WM_DDE_EXECUTE, lParam, &hDummy,(PUINT) &hCommands);
   pszCommands = (char*)GlobalLock(hCommands);

    // Here you have a command string, commands, containing something like:
    // [MON][  3][1000000018][1000000021][1000000023]
    // or [SHW][]
    // Extract the command and do some processing    
   
#ifdef _DEBUG               
   //MessageBox(pszCommands,"Progis ActiveX VC++ [Recv]",0); 
#endif
   
   acCmd[0] = 0;
   if (pszCommands)
     lstrcpynA(acCmd, pszCommands, MAX_CMD_LEN);   

   GlobalUnlock(hCommands);

   // Acknowledge that we did handle the command
   if (m_pParent->m_useSend)
     {
     if (!::SendMessage((HWND)wParam, WM_DDE_ACK,(WPARAM)m_hWnd,
                        ReuseDDElParam(lParam,WM_DDE_EXECUTE,WM_DDE_ACK
                                      ,dde_Ack, (UINT)hCommands)))
                    
       {
        // If WinGIS cannot handle our acknowledgment (it was closed or something) then
        // we need to free the memory used for the command string.
        __try 
          { 
           GlobalFree(hCommands);
          }
        __except(EXCEPTION_EXECUTE_HANDLER) { hDummy = 0L; }

   
       }
     } 
   else
     {
      ::PostMessage((HWND)wParam,WM_DDE_ACK,(WPARAM)m_hWnd,
                    ReuseDDElParam(lParam,WM_DDE_EXECUTE,WM_DDE_ACK
                                  ,dde_Ack, (UINT)hCommands));
     } 
   
   m_pParent->GotDDEExecute(acCmd);        // Notify
   return 0;
} // End OnDDEExecute

//  ************************************************************************                   
//  FUNCTION:  SendDDECommand
//  PARAMETER: const char *pszCmd  
//             pszCmd : pointer to DDE Execute cmd
//  RETURN:    0
//  DESC: This function sends a execute cmd string to the WinGIS hWnd
//  ************************************************************************
LRESULT CDDEWnd::SendDDECommand(const char *pszCmd)
{
   UINT len = strlen(pszCmd);
   HGLOBAL hCommands;
   char* pszDDEShare;
   
   if (!bConnected)
     return FALSE;

#ifdef _DEBUG
   //MessageBox(pszCmd,"Progis ActiveX VC++ [Send]",0);
#endif   
      
   // Allocate storage for the command string from GLOBAL store.
   // Required for DDE messages
   hCommands = GlobalAlloc(GMEM_DDESHARE, len + 1);
   // Lock the newly allocated storage so we can copy the command string to it.
   pszDDEShare = (char*)GlobalLock(hCommands);
   strcpy(pszDDEShare, pszCmd);
   GlobalUnlock(hCommands);
   
   // Send the EXECUTE command to WinGIS (WinGISWnd) from this window (HWindow)
   // WinGIS expects Execute commands to be SENT rather that POSTED as is the
   // Windows norm.
   
   uiLastDDEMsg = WM_DDE_EXECUTE;
   uiLastAck = DDE_ACK_NONE;
   if (m_pParent->m_useSend)
     {
      if(!::SendMessage(WinGISWnd, WM_DDE_EXECUTE, (WPARAM)m_hWnd
                  ,PackDDElParam(WM_DDE_EXECUTE, 0, (UINT)hCommands)))
         {  
          MessageBox("The Graphic Editor is not responding",
                     "DDE ERROR", MB_OK | MB_ICONEXCLAMATION);
          GlobalFree(hCommands);
          return FALSE;
         }
      else
        GlobalFree(hCommands);

     }
   else
     {
     ::PostMessage(WinGISWnd, WM_DDE_EXECUTE, (WPARAM)m_hWnd
                  ,PackDDElParam(WM_DDE_EXECUTE, 0, (UINT)hCommands));
     }

   Debug2.Print("OUT: " + CString(pszCmd));
   return TRUE;
}// End OnDDESendCommand                         

//  ************************************************************************                   
//  FUNCTION:  SendDDETerminate
//  PARAMETER: none
//  RETURN:    0
//  DESC: This function sends a DDE_TERMINATE to the WinGIS hWnd
//  ************************************************************************
LRESULT CDDEWnd::SendDDETerminate(void)
{
ULONG ulWaiting;
ULONG ulTicks;

   if (bConnected)
     {
	  uiLastDDEMsg = WM_DDE_TERMINATE;
	  uiLastAck = NO_ACK;
	  ::PostMessage(WinGISWnd, WM_DDE_TERMINATE, (WPARAM)m_hWnd, 0L);
      ulWaiting = 0;
      ulTicks = GetTickCount();
      while (AfxGetApp()->PumpMessage() && (ulWaiting < DDE_TIMEOUT) && bConnected)
        {         
         ulWaiting = GetTickCount() - ulTicks;
        }
      if (bConnected) // We didnt got a TERM from WinGIS
        {             // but we set bConnected to false anyway
         bConnected = FALSE;
         m_pParent->GotDDETerminate();
        }

     }
    return 0;
}

