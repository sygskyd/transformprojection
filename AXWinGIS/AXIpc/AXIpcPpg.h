#if !defined(AFX_AXIPCPPG_H__044B6816_C387_11D3_8FA9_0000B4BDFE28__INCLUDED_)
#define AFX_AXIPCPPG_H__044B6816_C387_11D3_8FA9_0000B4BDFE28__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// AXIpcPpg.h : Declaration of the CAXIpcPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CAXIpcPropPage : See AXIpcPpg.cpp.cpp for implementation.

class CAXIpcPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CAXIpcPropPage)
	DECLARE_OLECREATE_EX(CAXIpcPropPage)

// Constructor
public:
	CAXIpcPropPage();

// Dialog Data
	//{{AFX_DATA(CAXIpcPropPage)
	enum { IDD = IDD_PROPPAGE_AXIPC };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CAXIpcPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AXIPCPPG_H__044B6816_C387_11D3_8FA9_0000B4BDFE28__INCLUDED)
