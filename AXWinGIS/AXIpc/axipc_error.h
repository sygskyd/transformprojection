#if !defined(AFX_AXIPC_H__044B680A_C387_11D3_8FA9_0000B4BDFE28__INCLUDED_)
#define AFX_AXIPC_H__044B680A_C387_11D3_8FA9_0000B4BDFE28__INCLUDED_


#define AXIPC_STATUS_NOT_CONNECTED 0
#define AXIPC_STATUS_LISTENING     1
#define AXIPC_STATUS_CONNECTED     2


// Error codes
#define AXIPCERR_NO_ERROR           0
#define AXIPCERR_NO_CONNECTION      1
#define AXIPCERR_WRITE_FAILED       2
#define AXIPCERR_READ_FAILED        3
#define AXIPCERR_CONNECTION_FAILED  4
#define AXIPCERR_UNKNOWN_ERRORCODE  9998
#define AXIPCERR_STATUS_INVALID     9999


// Windows-Socket errors

// A successful AfxSocketInit must occur before using this API.
#define AXIPCERR_WSANOTINITIALISED WSANOTINITIALISED  // 10093

// The Windows Sockets implementation detected that the network subsystem failed.
#define AXIPCERR_WSAENETDOWN WSAENETDOWN   //10050

// The specified address is already in use.
#define AXIPCERR_WSAEADDRINUSE WSAEADDRINUSE //10048

// A blocking Windows Sockets call is in progress.
#define AXIPCERR_WSAEINPROGRESS WSAEINPROGRESS  //10036

// The specified address is not available from the local machine.
#define AXIPCERR_WSAEADDRNOTAVAIL WSAEADDRNOTAVAIL  //10049

// Addresses in the specified family cannot be used with this socket.
#define AXIPCERR_WSAEAFNOSUPPORT WSAEAFNOSUPPORT  //10047

// The attempt to connect was rejected.
#define AXIPCERR_WSAECONNREFUSED WSAECONNREFUSED  //10061

// A destination address is required.
#define AXIPCERR_WSAEDESTADDRREQ WSAEDESTADDRREQ  //10039

// The address is incorrect.
#define AXIPCERR_WSAEFAULT WSAEFAULT  //10014

// Invalid host address.
#define AXIPCERR_WSAEINVAL WSAEINVAL  //10022

// The socket is already connected.
#define AXIPCERR_WSAEISCONN WSAEISCONN  //10056

// No more file descriptors are available.
#define AXIPCERR_WSAEMFILE WSAEMFILE   //10024

// The network cannot be reached from this host at this time.
#define AXIPCERR_WSAENETUNREACH WSAENETUNREACH  //10051

// No buffer space is available. The socket cannot be connected.
#define AXIPCERR_WSAENOBUFS WSAENOBUFS  //10055

// The descriptor is not a socket.
#define AXIPCERR_WSAENOTSOCK WSAENOTSOCK //10038

// Attempt to connect timed out without establishing a connection.
#define AXIPCERR_WSAETIMEDOUT WSAETIMEDOUT  //10060

// The socket is marked as nonblocking and the connection cannot be completed immediately
#define AXIPCERR_WSAEWOULDBLOCK WSAEWOULDBLOCK  //10035


#endif  // AFX_AXIPC_H__044B680A_C387_11D3_8FA9_0000B4BDFE28__INCLUDED_