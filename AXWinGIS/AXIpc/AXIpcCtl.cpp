// AXIpcCtl.cpp : Implementation of the CAXIpcCtrl ActiveX Control class.

#include "stdafx.h"
#include "AXIpc.h"
#include "AXIpcCtl.h"
#include "AXIpcPpg.h"
#include "emedes_C_tools.h"
#include "axipc_error.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define MAX_BUFFER (4048*4)


UINT IPCControllingFunction( LPVOID pParam );

// mk - for terminal server sessions

// get the info for the client
short CAXIpcCtrl::getLastSocketFromRegistry(void)
{
  CString csEnableMultiport = "";
  CString csPort = "";
  int iNextSlot = 0;
  short iPort = 0;
  DWORD lSize;
  HKEY hKey;
  long lResult;
  char * pBuffer;
  csEnableMultiport = GetRegValue("Software\\Progis\\AXWingis", "EnableMultiport");
  if (csEnableMultiport == "yes") 
  {
       lSize = MAX_PATH;       
       pBuffer = csPort.GetBuffer((lSize + 1));

		lResult = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PROGIS\\AXWingis", 0, KEY_ENUMERATE_SUB_KEYS || KEY_QUERY_VALUE, &hKey);
		if (lResult == ERROR_SUCCESS)
		{

			if (!RegQueryValueEx(hKey, "Port", NULL, NULL, (unsigned char *)pBuffer, &lSize))
			{          
				csPort.ReleaseBuffer(-1);
			}
		
			RegCloseKey(hKey);
		}
		
		iPort = StrToInt(csPort,10);
//		iPort = StrToInt(GetRegValue("Software\\Progis\\AXWingis", "Port " + IntToStr(iPort,10)),10);
//		if  ( csPort == "") 
//				iPort = getSocketInfoFromReg();

		return iPort;

  } else 
	  return 27077;
}

short CAXIpcCtrl::getSocketInfoFromReg(void)
{
  CString csEnableMultiport;
  CString csNextSlot;
  CString csActualSlot;
  CString csFreePorts;// = "";
  CString csPort;
  int iStringCursor = 0;
  int iNextSlot = 0;
  short iPort = 0;
  HKEY hKey;
  long lResult;
  char * pBuffer;
  DWORD lSize;
 
  csEnableMultiport = GetRegValue("Software\\Progis\\AXWingis", "EnableMultiport");
  if (csEnableMultiport == "yes") 
  {
/*	
       lSize = MAX_PATH;       
       pBuffer = csPort.GetBuffer((lSize + 1));

		lResult = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PROGIS\\AXWingis", 0, KEY_ENUMERATE_SUB_KEYS || KEY_QUERY_VALUE, &hKey);
		if (lResult == ERROR_SUCCESS)
		{

		  if (!RegQueryValueEx(hKey, "Port", NULL, NULL, (unsigned char *)pBuffer, &lSize))
          {
             csPort.ReleaseBuffer(-1);
          }
		
		  RegCloseKey(hKey);
		}

		// 
		if (csPort != "") 
		{
			m_slot = StrToInt(csPort,10);


			return StrToInt(GetRegValue("Software\\Progis\\AXWingis","Port " + csPort),10);
		} else
        csPort = GetRegValue("Software\\Progis\\AXWingis", "Port " + csPort);
*/
	csFreePorts = GetRegValue("Software\\Progis\\AXWingis","PortsFree");
	if ( iStringCursor = csFreePorts.Find("/",0) != -1)
	{
		iNextSlot = StrToInt((const char*)(csFreePorts.Left(iStringCursor )),10);
		if (iNextSlot != 1)  
		{
			csActualSlot = GetRegValue("Software\\Progis\\AXWingis","Port " + IntToStr(iNextSlot - 1));
			iPort = StrToInt((const char*)csActualSlot,10) + 1;
			csFreePorts.Delete(0,iStringCursor+1);
			SetRegValue("Software\\Progis\\AXWingis", "PortsFree", csFreePorts);
		} else // handle the special case that the free slot is the first slot
		{
			iNextSlot = 1;
			iPort = 27077;
			csFreePorts.Delete(0,iStringCursor+1);
			SetRegValue("Software\\Progis\\AXWingis", "PortsFree", csFreePorts);
		} 
		m_slot = iNextSlot;
		SetRegValue("Software\\Progis\\AXWingis", "Port " + IntToStr(iNextSlot), IntToStr(iPort));
	// store the port in the user section
	csPort = IntToStr(iPort,10); //HKEY_CURRENT_USER\Software\PROGIS\AXWingis
	lResult = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PROGIS\\AXWingis" /*(const char *)cskey*/, 0, KEY_WRITE, &hKey);
	if (lResult == ERROR_SUCCESS)
	{
		RegSetValueEx(hKey, "Port", NULL, REG_SZ, (const BYTE *)(const char *)csPort, csPort.GetLength() + 1);
	}
	
	RegCloseKey(hKey);

		return iPort;
	} else // no free ports ... use the next one
	{

	csNextSlot = GetRegValue("Software\\Progis\\AXWingis", "NextSlot");
	sscanf((const char*)csNextSlot,"%i",&iNextSlot);
	if (iNextSlot != 1) 
	{
		csActualSlot = GetRegValue("Software\\Progis\\AXWingis","Port " + IntToStr(iNextSlot - 1));
		iPort = StrToInt(csActualSlot,10) + 1;
	} else
	{
		iPort = 27077;
	}
	SetRegValue("Software\\Progis\\AXWingis", "NextSlot", IntToStr(iNextSlot + 1));
	SetRegValue("Software\\Progis\\AXWingis", "Port " + IntToStr(iNextSlot), IntToStr(iPort));

	// store the port in the user section
	csPort = IntToStr(iPort,10); //HKEY_CURRENT_USER\Software\PROGIS\AXWingis
	lResult = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PROGIS\\AXWingis" /*(const char *)cskey*/, 0, KEY_WRITE, &hKey);
	if (lResult == ERROR_SUCCESS)
	{
		RegSetValueEx(hKey, "Port", NULL, REG_SZ, (const BYTE *)(const char *)csPort, csPort.GetLength() + 1);
	}
	
	RegCloseKey(hKey);
	
	m_slot = iNextSlot;
	return iPort;

	}

  } else 
	  return 27077;
	
}

void CAXIpcCtrl::deleteSocketInfoFromReg(void)
{
  CString csEnableMultiport;
  CString csNextSlot;
  CString csActualSlot;
  CString csFreePorts;

  int iStringCursor = 0;
  int iNextSlot = 0;
  short iPort = 0;

	HKEY hKey;
	long lResult;
  
  csEnableMultiport = GetRegValue("Software\\Progis\\AXWingis", "EnableMultiport");
  if (csEnableMultiport == "yes") 
  {

	csFreePorts = GetRegValue("Software\\Progis\\AXWingis", "PortsFree");
	if (csFreePorts.Find((const char*)IntToStr(m_slot,10),0) == -1)
	{
		csFreePorts = csFreePorts + IntToStr(m_slot,10) + "/";
		SetRegValue("Software\\Progis\\AXWingis", "PortsFree", csFreePorts);

		csActualSlot = "Port " + IntToStr(m_slot,10);


		lResult = RegOpenKeyEx(HKEY_AXROOT, AXWINGIS_REGKEY, 0, KEY_WRITE, &hKey);
		if (lResult == ERROR_SUCCESS)
		{
			RegDeleteValue( hKey, csActualSlot);
		}
	
		RegCloseKey(hKey);
	}

	CString csPort = "";
	lResult = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\PROGIS\\AXWingis" /*(const char *)cskey*/, 0, KEY_WRITE, &hKey);
	if (lResult == ERROR_SUCCESS)
	{
		RegSetValueEx(hKey, "Port", NULL, REG_SZ, (const BYTE *)(const char *)csPort, csPort.GetLength() + 1);
	}
	
	RegCloseKey(hKey);

  }
}

// - mk


CAXIpcSocket::CAXIpcSocket( CAXIpcCtrl *pOwner )
{
 

	  pAXIpc = pOwner;
    csRemoteName = "";
    lSessionID = 0;
    bConnected = FALSE;


}

BOOL CAXIpcSocket::Create(UINT nSocketPort)
{
  BOOL bRet;
  BOOL bTrue = TRUE;
  int iRcvSize = MAX_BUFFER;
  int iSndSize = MAX_BUFFER;
  // mk - for remote AxIpc ver. 0.90
  bRet = CAsyncSocket::Create(nSocketPort,SOCK_STREAM, 
                              FD_READ | FD_WRITE | FD_OOB | FD_ACCEPT | FD_CONNECT | FD_CLOSE
                              );//"127.0.0.1");
  // - mk
  if (bRet)
   {
    SetSockOpt( TCP_NODELAY, &bTrue, sizeof(BOOL) );
    SetSockOpt( SO_RCVBUF, &iRcvSize, sizeof(int) );
    SetSockOpt( SO_SNDBUF, &iSndSize, sizeof(int) );
   }
  else
   {
    pAXIpc->GetErrorCodeFromSocketError(GetLastError());
    if(GetLastError() != WSAEADDRINUSE)
		AfxMessageBox("Create Socket failed: " + pAXIpc->csLastErrorMsg );    
   }

  return bRet;
}

/////////////////////////////////////////////////////////////////////////////
// CListeningSocket Overridable callbacks

void CAXIpcSocket::OnAccept(int nErrorCode)
{
	CAsyncSocket::OnAccept(nErrorCode);
	pAXIpc->ProcessPendingAccept();
}

void CAXIpcSocket::OnReceive( int nErrorCode )
{
  if (nErrorCode != 0)
	  CAsyncSocket::OnReceive(nErrorCode);

	pAXIpc->ProcessPendingRead(this, nErrorCode);
}

void CAXIpcSocket::OnClose( int nErrorCode )
{
	CAsyncSocket::OnClose(nErrorCode);
	pAXIpc->ProcessClose(this);
}

long CAXIpcSocket::Send(LPCTSTR sCmd) 
{
 long lRet = AXIPCERR_UNKNOWN_ERRORCODE;
 CString csCmd(sCmd);
 DWORD dwZero = 0;

 try
   {
    //AsyncSelect(0);
    //if ( ! IOCtl(FIONBIO, &dwZero))
    //  {
    //   pAXIpc->GetErrorCodeFromSocketError(GetLastError());
    //   AfxMessageBox("IOCtl failed: " + pAXIpc->csLastErrorMsg );
    //  }

    csCmd = IntToStr(csCmd.GetLength()) + "#" + csCmd;
    lRet = CAsyncSocket::Send(csCmd, csCmd.GetLength());    
   }
 catch(int)
   { ; }
  
 try
   {
    if (lRet != SOCKET_ERROR)
      //AsyncSelect(FD_READ | FD_WRITE | FD_OOB | FD_ACCEPT | FD_CONNECT | FD_CLOSE);     
      ;
   }
 catch(int)
   { ; }

  return lRet;
}
/////////////////////////////////////////////////////////////////////////////
// CSocket Implementation

CAXIpcSocket::~CAXIpcSocket()
{
}

#ifdef _DEBUG
void CAXIpcSocket::AssertValid() const
{
	CAsyncSocket::AssertValid();
}

void CAXIpcSocket::Dump(CDumpContext& dc) const
{
	CAsyncSocket::Dump(dc);
}
#endif //_DEBUG

IMPLEMENT_DYNAMIC(CAXIpcSocket, CAsyncSocket)

IMPLEMENT_DYNCREATE(CAXIpcCtrl, COleControl)

//CAXIpcCtrl *pAXIpc;

/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CAXIpcCtrl, COleControl)
	//{{AFX_MSG_MAP(CAXIpcCtrl)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CAXIpcCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CAXIpcCtrl)
	DISP_PROPERTY_NOTIFY(CAXIpcCtrl, "Port", m_port, OnPortChanged, VT_I2)
	DISP_PROPERTY_NOTIFY(CAXIpcCtrl, "ServiceName", m_serviceName, OnServiceNameChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CAXIpcCtrl, "Protocol", m_protocol, OnProtocolChanged, VT_I2)
	DISP_PROPERTY_EX(CAXIpcCtrl, "Status", GetStatus, SetNotSupported, VT_I2)
	DISP_PROPERTY_EX(CAXIpcCtrl, "IsServer", GetIsServer, SetNotSupported, VT_BOOL)
	DISP_FUNCTION(CAXIpcCtrl, "Connect", Connect, VT_I4, VTS_BSTR VTS_I2)
	DISP_FUNCTION(CAXIpcCtrl, "Send", Send, VT_I4, VTS_BSTR)
	DISP_FUNCTION(CAXIpcCtrl, "Disconnect", Disconnect, VT_I4, VTS_NONE)
	DISP_FUNCTION(CAXIpcCtrl, "Listen", Listen, VT_I4, VTS_NONE)
	DISP_FUNCTION(CAXIpcCtrl, "GetLastError", GetLastError, VT_I4, VTS_NONE)
	DISP_FUNCTION(CAXIpcCtrl, "GetLastErrorMsg", GetLastErrorMsg, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(CAXIpcCtrl, "GetSessionCount", GetSessionCount, VT_I4, VTS_NONE)
	DISP_FUNCTION(CAXIpcCtrl, "GetSessionID", GetSessionID, VT_I4, VTS_I4)
	DISP_FUNCTION(CAXIpcCtrl, "SendTo", SendTo, VT_I4, VTS_BSTR VTS_I4)
	DISP_FUNCTION(CAXIpcCtrl, "SendWait", SendWait, VT_I4, VTS_BSTR)
	DISP_FUNCTION(CAXIpcCtrl, "SendToWait", SendToWait, VT_I4, VTS_BSTR VTS_I4)
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CAXIpcCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CAXIpcCtrl, COleControl)
	//{{AFX_EVENT_MAP(CAXIpcCtrl)
	EVENT_CUSTOM("OnConnect", FireOnConnect, VTS_I4  VTS_BSTR)
	EVENT_CUSTOM("OnDisconnect", FireOnDisconnect, VTS_I4)
	EVENT_CUSTOM("OnReceive", FireOnReceive, VTS_BSTR  VTS_I4)
	EVENT_CUSTOM("OnAck", FireOnAck, VTS_I4)
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CAXIpcCtrl, 1)
	PROPPAGEID(CAXIpcPropPage::guid)
END_PROPPAGEIDS(CAXIpcCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CAXIpcCtrl, "AXIPC.AXIpcCtrl.1",
	0x44b6806, 0xc387, 0x11d3, 0x8f, 0xa9, 0, 0, 0xb4, 0xbd, 0xfe, 0x28)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CAXIpcCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DAXIpc =
		{ 0x44b6804, 0xc387, 0x11d3, { 0x8f, 0xa9, 0, 0, 0xb4, 0xbd, 0xfe, 0x28 } };
const IID BASED_CODE IID_DAXIpcEvents =
		{ 0x44b6805, 0xc387, 0x11d3, { 0x8f, 0xa9, 0, 0, 0xb4, 0xbd, 0xfe, 0x28 } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwAXIpcOleMisc =
	OLEMISC_INVISIBLEATRUNTIME |
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CAXIpcCtrl, IDS_AXIPC, _dwAXIpcOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CAXIpcCtrl::CAXIpcCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CAXIpcCtrl

BOOL CAXIpcCtrl::CAXIpcCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegApartmentThreading to 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_AXIPC,
			IDB_AXIPC,
			afxRegApartmentThreading,
			_dwAXIpcOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CAXIpcCtrl::CAXIpcCtrl - Constructor

CAXIpcCtrl::CAXIpcCtrl()
{
    m_isServer = FALSE;
    m_port = 0;
    m_serviceName = "";

    m_lAckTimeout = 2000;

    bInsideRead = FALSE;
    lInsideSend = 0;

    csLastErrorMsg = "";
    lLastError = AXIPCERR_NO_ERROR;
    m_status = AXIPC_STATUS_NOT_CONNECTED;

	  InitializeIIDs(&IID_DAXIpc, &IID_DAXIpcEvents);

    if (!AfxSocketInit())
      {
		   AfxMessageBox("IDP_SOCKETS_INIT_FAILED");
		   return;
      }
        
    InitializeCriticalSection(&csSl);

    hsemAXIPC_Send = CreateSemaphore( NULL,
                                      1,  // initial count
                                      1,  // maximum count
                                      "AXIPCSend" );// pointer to semaphore-object name


    //pAXIpc = this;

	  // Create the socket
    m_pSock = new CAXIpcSocket(this);

    //if (m_isServer)
    //   m_pSock->Create( m_port );
    //else
       m_pSock->Create( );

#ifdef _DEBUGWIN_ON
    CString csModuleName;
    ::GetModuleFileName(NULL,(char *)((unsigned char *) csModuleName.GetBuffer(512)), 511);
    csModuleName.ReleaseBuffer(-1);        

    Debug.Print(CString(AXIPC_VERSION) + " (loaded from " + csModuleName + ")");
#endif
}


/////////////////////////////////////////////////////////////////////////////
// CAXIpcCtrl::~CAXIpcCtrl - Destructor

CAXIpcCtrl::~CAXIpcCtrl()
{

   try
     {
      try { CloseAllSocket(); }
      catch( CException* e ) { e->Delete(); }

      try { 
      if (m_pSock != NULL)
        { 
         if (m_pSock->bConnected == TRUE)
           m_pSock->ShutDown(2);
         m_pSock->Close();
         //m_pSock->Detach();
         m_pSock->bConnected = FALSE;

         delete m_pSock;
         m_pSock = NULL;
        }
      } catch( CException* e ) { e->Delete(); }

#ifdef _DEBUGWIN_ON
      try { 
      Debug.KillThread();
      } catch( CException* e ) { e->Delete(); }
#endif

      try { CloseHandle(hsemAXIPC_Send); } catch( CException* e ) { e->Delete(); }
      try { DeleteCriticalSection(&csSl); } catch( CException* e ) { e->Delete(); }

     }
  catch( CException* e )
     {    // Handle the exception here.
          // "e" contains information about the exception.    
      e->Delete();
     }
}


/////////////////////////////////////////////////////////////////////////////
// CAXIpcCtrl::OnDraw - Drawing function

void CAXIpcCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	// TODO: Replace the following code with your own drawing code.
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	pdc->Ellipse(rcBounds);
}


/////////////////////////////////////////////////////////////////////////////
// CAXIpcCtrl::DoPropExchange - Persistence support

void CAXIpcCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

  PX_Bool(pPX, "IsServer", m_isServer, 0);
  PX_Short(pPX, "Port", m_port, getLastSocketFromRegistry() /* 27077 */);
  PX_String(pPX, "ServiceName", m_serviceName, "");
 
}


/////////////////////////////////////////////////////////////////////////////
// CAXIpcCtrl::OnResetState - Reset control to default state

void CAXIpcCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CAXIpcCtrl::AboutBox - Display an "About" box to the user

void CAXIpcCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_AXIPC);
	dlgAbout.DoModal();
}


/////////////////////////////////////////////////////////////////////////////
// CAXIpcCtrl message handlers

void CAXIpcCtrl::OnPortChanged() 
{
 BOOL bRet = FALSE;
  
  if (m_isServer)
    bRet = m_pSock->Bind( m_port );

	SetModifiedFlag();
}

short CAXIpcCtrl::GetStatus() 
{

  if (m_isServer)
    {
     if ( ! m_connectionList.IsEmpty())
       {
        POSITION pos,temp;
        for(pos = m_connectionList.GetHeadPosition(); pos != NULL;)
          {
	         temp = pos;
	         CAXIpcSocket* pSock = (CAXIpcSocket*)m_connectionList.GetNext(pos);
	         if (pSock->bConnected == TRUE)
             {
              ProcessPendingRead(pSock, 0);
             }
          }
       }
    }
  else
    {
     if (m_pSock->bConnected == TRUE)
       {
        ProcessPendingRead(m_pSock, 0);
       }
    }

	return m_status;
}

void CAXIpcCtrl::OnServiceNameChanged() 
{
	SetModifiedFlag();
}

void CAXIpcCtrl::OnProtocolChanged() 
{
	SetModifiedFlag();
}

// *** socket handlers ***

long CAXIpcCtrl::Listen() 
{
 BOOL bRet = FALSE;
 long lError = AXIPCERR_STATUS_INVALID;

    // read the ack timeout value from the registry
    GetAckTimeoutValue();

  //  if (m_port == 0)  by mk -
  //    {
  //     m_port = 27077;
  //     SetModifiedFlag();
  //	}else  - mk
	
		m_port = getSocketInfoFromReg();
       SetModifiedFlag();
	

    m_isServer = TRUE;

    m_pSock->Close();
    m_pSock->bConnected = FALSE;

	// mk-
	
    bRet = m_pSock->Create( m_port );
	//-mk
    bRet = m_pSock->Listen();

    //AfxBeginThread(MyReceiveProc, NULL); 
    
    if (bRet)  // no error ?
      {
       lError = GetErrorCodeFromSocketError(AXIPCERR_NO_ERROR);
       m_status = AXIPC_STATUS_LISTENING;
#ifdef _DEBUGWIN_ON
       Debug.Print("Socket is listening now.");
#endif
      }
    else
      {
       lError = GetErrorCodeFromSocketError(m_pSock->GetLastError());
#ifdef _DEBUGWIN_ON
       Debug.Print("Listen failed: " + csLastErrorMsg);
#endif

      }

	return lError;
}

long CAXIpcCtrl::Connect(LPCTSTR sDestination, short iRemotePort) 
{
 BOOL bRet = FALSE;
 long lError = AXIPCERR_STATUS_INVALID;
 DWORD dwZero = 0L;

  // read the ack timeout value from the registry
  GetAckTimeoutValue();

  // if (m_isServer)
  //  {
  m_pSock->Close();
  m_pSock->bConnected = FALSE;
  m_status = AXIPC_STATUS_NOT_CONNECTED;

  bRet = m_pSock->Create( );
  //  }
  
  if (iRemotePort == 0) iRemotePort = getLastSocketFromRegistry();// 27077;

  m_isServer = FALSE;

 try
   {
    //m_pSock->AsyncSelect(0);
    //if ( ! m_pSock->IOCtl(FIONBIO, &dwZero))
    //  {
    //   GetErrorCodeFromSocketError(m_pSock->GetLastError());
    //   AfxMessageBox("IOCtl failed: " + csLastErrorMsg );
    //  }


    if (CString(sDestination) == "")
       bRet = m_pSock->Connect("127.0.0.1", iRemotePort);
    else
	     bRet = m_pSock->Connect(sDestination, iRemotePort); // mk - for Remote IPC - mk
   }
 catch(int)
   {
    ;
   }
  
  if (bRet)  // no error ?
     {
      lError = GetErrorCodeFromSocketError(AXIPCERR_NO_ERROR);
#ifdef _DEBUGWIN_ON
      Debug.Print("Connect to port " + IntToStr(iRemotePort) + " ok");
#endif
     }
  else
     {
      lError = GetErrorCodeFromSocketError(m_pSock->GetLastError());
#ifdef _DEBUGWIN_ON
      Debug.Print("Connect failed: " + csLastErrorMsg);
#endif
     }

  //m_pSock->AsyncSelect(FD_READ | FD_WRITE | FD_OOB | FD_ACCEPT | FD_CONNECT | FD_CLOSE); 

	return lError;
}


long CAXIpcCtrl::Send(LPCTSTR sCmd) 
{
 long lRes = AXIPCERR_STATUS_INVALID;
 BOOL bFound = FALSE;
 int iBytesWritten = 0;
 int i;
 DWORD dwWaitResult;


  //EnterCriticalSection(&csSl);
 

  i = 0;  
  while ((lInsideSend == 1) && ((i*25) < m_lAckTimeout+1000))
    {
     AfxGetApp()->PumpMessage();
     i = i + 1;
     Sleep(50);
    }
  
  InterlockedExchange(&lInsideSend, 1L);


  //SetTimer(2, 10, NULL);
  dwWaitResult = WAIT_TIMEOUT;
  //while ((dwWaitResult == WAIT_TIMEOUT) && (i < 110))
  //  {
  //   dwWaitResult = WaitForSingleObject(hsemAXIPC_Send, 100);
  //   AfxGetApp()->PumpMessage();
  //   i = i + 1;
  //   Sleep(50);
  //  }
  //KillTimer(2);
  if (dwWaitResult == WAIT_FAILED)
    {
#ifdef _DEBUGWIN_ON    
     Debug.Print("WaitForSingleObject FAILD");
#endif    
    }
      

 try
  { 
       
  if (m_isServer)
    {
    
#ifdef _DEBUGWIN_ON    
     Debug.Print("SERVER_AXIPC_Send: " + CString(sCmd));
#endif    
     // send to clients 
     if (m_connectionList.IsEmpty())
       {
        lRes = AXIPCERR_NO_CONNECTION;
       }
     else
       {
        POSITION pos,temp;
        lAckFlag = FALSE; 
        for(pos = m_connectionList.GetHeadPosition(); pos != NULL;)
          {
	         temp = pos;
	         CAXIpcSocket* pSock = (CAXIpcSocket*)m_connectionList.GetNext(pos);
	         if (pSock->bConnected == TRUE)
             {              
		          iBytesWritten = pSock->Send(sCmd);
              if ( iBytesWritten == SOCKET_ERROR )
                {
                 lRes = GetErrorCodeFromSocketError(pSock->GetLastError());
                 //pSock->AsyncSelect(FD_READ | FD_WRITE | FD_OOB | FD_ACCEPT | FD_CONNECT | FD_CLOSE);
#ifdef _DEBUGWIN_ON    
                 Debug.Print("SOCKET_ERROR: " + csLastErrorMsg);
#endif
                }
              else
                {                
                 lRes = GetErrorCodeFromSocketError(AXIPCERR_NO_ERROR);
                }
             }
          }
       }
    }
  else
    {

#ifdef _DEBUGWIN_ON    
     Debug.Print("CLIENT_AXIPC_Send: " + CString(sCmd));
#endif    

     // send to server
     lAckFlag = FALSE; 
     iBytesWritten = m_pSock->Send(sCmd);
     if ( iBytesWritten == SOCKET_ERROR )
       {
        lRes = GetErrorCodeFromSocketError(m_pSock->GetLastError());
        //m_pSock->AsyncSelect(FD_READ | FD_WRITE | FD_OOB | FD_ACCEPT | FD_CONNECT | FD_CLOSE);
#ifdef _DEBUGWIN_ON    
        Debug.Print("SOCKET_ERROR: " + csLastErrorMsg);
#endif
       }
     else
       {
        lRes = GetErrorCodeFromSocketError(AXIPCERR_NO_ERROR);
       }
    }

   if (lRes == AXIPCERR_NO_ERROR)
     {
      
      BOOL bAckRes = WaitForAckTimeout(m_lAckTimeout);

#ifdef _DEBUGWIN_ON    
      if (bAckRes )
        {
         //Debug.Print("ACK OK: " + CString(sCmd));
        }
      else
        {
         Debug.Print("ACK FAILED: " + CString(sCmd));
         lRes = AXIPCERR_WSAETIMEDOUT;
        }
#endif

     }
   else
     {
#ifdef _DEBUGWIN_ON    
      Debug.Print("AXIPC_Send FAILED: " + CString(sCmd));
#endif
     }

  }
 catch(CException* e )
  {
   e->Delete();   
  }


  //ReleaseSemaphore( hsemAXIPC_Send, 1, NULL );
  //LeaveCriticalSection(&csSl);

  InterlockedExchange(&lInsideSend, 0L);

	return lRes;
}

long CAXIpcCtrl::Disconnect() 
{

  if (m_isServer)
    {
     CloseAllSocket();
    }
  else
    {
     if (m_pSock->bConnected == TRUE)
       {
        m_pSock->ShutDown(2);
       }
     m_pSock->Close();
     m_pSock->bConnected = FALSE;
     FireOnDisconnect(m_pSock->lSessionID);
     m_pSock->lSessionID = 0;
    }

  m_status = AXIPC_STATUS_NOT_CONNECTED;

	return GetErrorCodeFromSocketError(AXIPCERR_NO_ERROR);;
}


void CAXIpcCtrl::ProcessPendingAccept()
{
	CAXIpcSocket* pSocket = new CAXIpcSocket(this);

	if (m_pSock->Accept(*pSocket))
      {	
       // generate a session ID
	     pSocket->lSessionID = GetTickCount();

       // add session to the list of all active sessions
       m_connectionList.AddTail(pSocket);

       // start init-handshake
       pSocket->Send("AXIPC_SESSION_INIT|" + m_serviceName);

#ifdef _DEBUGWIN_ON    
       Debug.Print("Got connect, starting init");
#endif 
      }
	else
     {
	   delete pSocket;
#ifdef _DEBUGWIN_ON    
     Debug.Print("Got connect, but accept failed");
#endif 

     }
}

void CAXIpcCtrl::ProcessPendingRead(CAXIpcSocket* pSocket, int nErrorCode)
{
 CString csMsg = "";
 CString csIn = "";
 CString csMsgLen = "";
 
 int iBytesRead = 0; 
 long lMsgLen = 0;
 long lInOffset = 0;
 long lFullLen = 0;

 
 try
   {

     if (bInsideRead)
       return;

     bInsideRead = TRUE;

     if ( nErrorCode != 0 )
       {
        GetErrorCodeFromSocketError(pSocket->GetLastError());
#ifdef _DEBUGWIN_ON    
        Debug.Print("**** ONRECEIVE nErrorCode: " + csLastErrorMsg);
#endif
        return;
       }

	 do
	  {		
     csMsg = "";
     csIn = "";
     csMsgLen = "";

     iBytesRead = 0; 
     lMsgLen = 0;
     lInOffset = 0;
     lFullLen = 0;

     iBytesRead = pSocket->Receive( csIn.GetBuffer(MAX_BUFFER+1), MAX_BUFFER);
     if ( iBytesRead == SOCKET_ERROR )
       {
        long lSockErr = GetErrorCodeFromSocketError(pSocket->GetLastError());
#ifdef _DEBUGWIN_ON    
        // AXIPCERR_WSAEWOULDBLOCK here means no data available for us
        if (lSockErr != AXIPCERR_WSAEWOULDBLOCK)
          Debug.Print("**** ONRECEIVE SOCKET_ERROR: " + csLastErrorMsg);
#endif
       }
     else
       {
        if (iBytesRead == 0) 
          {
#ifdef _DEBUGWIN_ON    
           Debug.Print("**** ONRECEIVE RETURNED NULL");
#endif           
          }        
       }

     if (iBytesRead <= 0) 
       {
        csIn.ReleaseBuffer(0);   
        break;
       }

     csIn.ReleaseBuffer(iBytesRead);
 
 
#ifdef _DEBUGWIN_ON
        Debug.Print("AXIPC_READ: " + csIn);
#endif
       
 
     lFullLen = csIn.GetLength();

     do
      { 
        csMsgLen = GetSubString(csIn.Mid(lInOffset),1,'#');
        lMsgLen = StrToInt(csMsgLen);
        if (lMsgLen <= 0) break;
        if (lMsgLen >= lFullLen) break;

        lInOffset = lInOffset + csMsgLen.GetLength() + 1;   // len + #

        csMsg = csIn.Mid(lInOffset, lMsgLen);
        lInOffset = lInOffset + lMsgLen;

#ifdef _DEBUGWIN_ON
        Debug.Print("AXIPC_IN: " + csMsg);
#endif

        CString csCmd = GetSubString(csMsg, 1, '|');

        if (csCmd == "AXIPC_SESSION_INIT")
          {
           pSocket->csRemoteName = GetSubString(csMsg, 2, '|');
           pSocket->bConnected = TRUE;
           m_status = AXIPC_STATUS_CONNECTED;
           FireOnConnect(pSocket->lSessionID, pSocket->csRemoteName);
           if ( ! m_isServer)
             {
              pSocket->Send("AXIPC_SESSION_INIT|" + m_serviceName);
             } 
          }
        else if (csCmd == "AXIPC_SESSION_CLOSE")
          {
           CloseSocket(pSocket);
          }
        else if (csCmd == "AXIPC_SESSION_ACK")
          {
           lAckFlag = TRUE;
           //FireOnAck(0);           
          }
        else
          {
           pSocket->Send("AXIPC_SESSION_ACK|" + IntToStr(pSocket->lSessionID));
          //pSocket->AsyncSelect(FD_READ | FD_WRITE | FD_OOB | FD_ACCEPT | FD_CONNECT | FD_CLOSE);
           
          

           //CIPCThreadData * pThreadData;
           //pThreadData = new CIPCThreadData(csMsg, this);
          
           //pThread = AfxBeginThread(IPCControllingFunction, pThreadData);
             
           //if (csMsg == "[SHW][]")
           //  {
           //   int dummy = 1;
           //  }

           FireOnReceive(csMsg, pSocket->lSessionID);
          }        

      } while(lInOffset < lFullLen);

	 }
	 while (iBytesRead > 0);
   //while (false);
   bInsideRead = FALSE;
  }
 catch(char *pchMsg)
  {
   bInsideRead = FALSE; 
   AfxMessageBox("CATCH");
   (pchMsg == NULL);   // dummy, just for removing the unreferenced warning	    
  }
 bInsideRead = FALSE;
}


UINT IPCControllingFunction( LPVOID pParam )
{
   if (pParam != NULL)
     {
      //AfxEndThread(((CIPCThreadData *)pParam)->pCtrl->ThreadProc(((CIPCThreadData *)pParam)->csCmd) );
      ((CIPCThreadData *)pParam)->pCtrl->ThreadProc(((CIPCThreadData *)pParam)->csCmd);
      //((CDebug *)pParam)->ThreadProc();
      //delete (CIPCThreadData *)pParam;
     }
   return 0;
}

UINT CAXIpcCtrl::ThreadProc(CString csCmd)
{
  CString * csMsg = new CString(csCmd);
  FireOnReceive("", 0); 
  return 0;
}

void CAXIpcCtrl::ProcessClose(CAXIpcSocket* pSocket)
{
 long lSessionID = pSocket->lSessionID;

    
    if (m_isServer)
      {
       CloseSocket(pSocket);      
       if ( m_connectionList.IsEmpty())
         m_status = AXIPC_STATUS_NOT_CONNECTED;

#ifdef _DEBUGWIN_ON
       Debug.Print("AXIPC_SERVER_DISCONNECT");
#endif

      }
    else
      {       
       m_status = AXIPC_STATUS_NOT_CONNECTED;
#ifdef _DEBUGWIN_ON
       Debug.Print("AXIPC_CLIENT_DISCONNECT");
#endif
      }

    FireOnDisconnect(lSessionID);
}

void CAXIpcCtrl::CloseSocket(CAXIpcSocket* pSocket)
{
  pSocket->ShutDown(2);
	pSocket->Close();
  pSocket->bConnected = FALSE;

	POSITION pos,temp;
	for(pos = m_connectionList.GetHeadPosition(); pos != NULL;)
	  {
		  temp = pos;
		  CAXIpcSocket* pSock = (CAXIpcSocket*)m_connectionList.GetNext(pos);
		  if (pSock == pSocket)
		    {
			   m_connectionList.RemoveAt(temp);
			   break;
		    }
	  }
  FireOnDisconnect(pSocket->lSessionID);
	delete pSocket;
}

// Wingis is shutting down ...
void CAXIpcCtrl::CloseAllSocket()
{
    while ( ! m_connectionList.IsEmpty())
      {
       CAXIpcSocket* pSock = (CAXIpcSocket*)m_connectionList.RemoveHead();
       if (pSock->bConnected == TRUE)
       {
          pSock->ShutDown(2);
       }
       pSock->Close();
       FireOnDisconnect(m_pSock->lSessionID);
       delete pSock;
      }	
// mk -
// remove the current port
//	this->m_port
	if( m_isServer )
		deleteSocketInfoFromReg();
// - mk
}

// *** end socket handlers ***

long CAXIpcCtrl::GetLastError() 
{
	return lLastError;
}

BSTR CAXIpcCtrl::GetLastErrorMsg() 
{
	CString strResult = csLastErrorMsg;
	return strResult.AllocSysString();
}

long CAXIpcCtrl::GetSessionCount() 
{
	return m_connectionList.GetCount();
}

long CAXIpcCtrl::GetSessionID(long lIndex) 
{
 long lResult = -1;

  if ((lIndex < GetSessionCount()) && (lIndex > -1))
    {
	   CAXIpcSocket* pSock = (CAXIpcSocket*)m_connectionList.GetAt(m_connectionList.FindIndex(lIndex));
     if (pSock != NULL)
       {
        lResult = pSock->lSessionID;
       }
    }

	return lResult;
}


BOOL CAXIpcCtrl::GetIsServer() 
{
	return m_isServer;
}

long CAXIpcCtrl::SendTo(LPCTSTR sCmd, long lSessionID) 
{
	// TODO: Add your dispatch handler code here

	return 0;
}

long CAXIpcCtrl::SendWait(LPCTSTR sCmd) 
{

  //lAckFlag


	return 0;
}

long CAXIpcCtrl::SendToWait(LPCTSTR sCmd, long lSessionID) 
{
	// TODO: Add your dispatch handler code here

	return 0;
}


BOOL CAXIpcCtrl::WaitForAckTimeout(long lTimeOut)
{
ULONG ulWaiting;
ULONG ulTicks;
    
   //__try
   TRY
     {
      LockInPlaceActive(TRUE); 
      
#ifdef _DEBUGWIN_ON
      Debug.Print("IN WaitForAckTimeout: " + IntToStr(lTimeOut));
      
#endif
      ulWaiting = 0;
      ulTicks = GetTickCount();
      //SetTimer(1, 10, NULL);
      while ( ((ulWaiting < (unsigned long)lTimeOut) || (lTimeOut == -1))
             && (lAckFlag == FALSE )
             //&& (AfxGetApp()->PumpMessage()) 
             )
             
        {         
         ulWaiting = GetTickCount() - ulTicks;
         Sleep(20);
         GetStatus();
        }
      //KillTimer(1);
     }
   //__except(EXCEPTION_EXECUTE_HANDLER)
   CATCH_ALL(e)
     {
   	  e->Delete();
      KillTimer(1);
#ifdef _DEBUGWIN_ON
      Debug.Print("Execption in WaitForAckTimeout");
#endif
     }
   END_CATCH_ALL
   LockInPlaceActive(FALSE); 
   
#ifdef _DEBUGWIN_ON
   Debug.Print("OUT WaitForAckTimeout exits with " + BoolToStr(lAckFlag));
#endif

   return (lAckFlag);
}



// *** ERROR Codes ***

long CAXIpcCtrl::GetErrorCodeFromSocketError(int iError)
{
 switch (iError)
   {
    case AXIPCERR_NO_ERROR:
      lLastError = AXIPCERR_NO_ERROR;
      csLastErrorMsg = "";
      break;

    // A successful AfxSocketInit must occur before using this API.
    case WSANOTINITIALISED:  // 10093
      lLastError = AXIPCERR_WSANOTINITIALISED;
      csLastErrorMsg = "A successful AfxSocketInit must occur before using this API.";
      break;

    // The Windows Sockets implementation detected that the network subsystem failed.
    case WSAENETDOWN:   //10050      
      lLastError = AXIPCERR_WSAENETDOWN;
      csLastErrorMsg = "The Windows Sockets implementation detected that the network subsystem failed.";
      break;

    // The specified address is already in use.
    case WSAEADDRINUSE: //10048
      lLastError = AXIPCERR_WSAEADDRINUSE;
      csLastErrorMsg = "The specified address is already in use.";
      break;

// A blocking Windows Sockets call is in progress.
    case WSAEINPROGRESS:  //10036
      lLastError = AXIPCERR_WSAEINPROGRESS;
      csLastErrorMsg = "A blocking Windows Sockets call is in progress.";
      break;

// The specified address is not available from the local machine.
    case WSAEADDRNOTAVAIL:  //10049
      lLastError = AXIPCERR_WSAEADDRNOTAVAIL;
      csLastErrorMsg = "The specified address is not available from the local machine.";
      break;

// Addresses in the specified family cannot be used with this socket.
    case WSAEAFNOSUPPORT: //10047
      lLastError = AXIPCERR_WSAEAFNOSUPPORT;
      csLastErrorMsg = "Addresses in the specified family cannot be used with this socket.";
      break;

// The attempt to connect was rejected.
    case WSAECONNREFUSED:  //10061
      lLastError = AXIPCERR_WSAECONNREFUSED;
      csLastErrorMsg = "The attempt to connect was rejected.";
      break;

// A destination address is required.
    case WSAEDESTADDRREQ:  //10039
      lLastError = AXIPCERR_WSAEDESTADDRREQ;
      csLastErrorMsg = "A destination address is required.";
      break;

// The address is incorrect.
    case WSAEFAULT:  //10014
      lLastError = AXIPCERR_WSAEFAULT;
      csLastErrorMsg = "The address is incorrect.";
      break;

// Invalid host address.
    case WSAEINVAL:  //10022
      lLastError = AXIPCERR_WSAEINVAL;
      csLastErrorMsg = "Invalid host address.";
      break;

// The socket is already connected.
    case WSAEISCONN:  //10056
      lLastError = AXIPCERR_WSAEISCONN;
      csLastErrorMsg = "The socket is already connected.";
      break;

// No more file descriptors are available.
    case WSAEMFILE:   //10024
      lLastError = AXIPCERR_WSAEMFILE;
      csLastErrorMsg = "No more file descriptors are available.";
      break;

// The network cannot be reached from this host at this time.
    case WSAENETUNREACH:  //10051
      lLastError = AXIPCERR_WSAENETUNREACH;
      csLastErrorMsg = "The network cannot be reached from this host at this time.";
      break;

// No buffer space is available. The socket cannot be connected.
    case WSAENOBUFS:  //10055
      lLastError = AXIPCERR_WSAENOBUFS;
      csLastErrorMsg = "No buffer space is available. The socket cannot be connected.";
      break;

// The descriptor is not a socket.
    case WSAENOTSOCK:  //10038
      lLastError = AXIPCERR_WSAENOTSOCK;
      csLastErrorMsg = "The descriptor is not a socket.";
      break;

// Attempt to connect timed out without establishing a connection.
    case WSAETIMEDOUT:  //10060
      lLastError = AXIPCERR_WSAETIMEDOUT;
      csLastErrorMsg = "Attempt to connect timed out without establishing a connection.";
      break;

// The socket is marked as nonblocking and the connection cannot be completed immediately
    case WSAEWOULDBLOCK:  //10035    
      lLastError = AXIPCERR_WSAEWOULDBLOCK;
      csLastErrorMsg = "The socket is marked as nonblocking and the connection cannot be completed immediately.";
      break;

    default:      
//      lLastError = AXIPCERR_UNKNOWN_ERRORCODE;
//      csLastErrorMsg = "An unknown error (" + IntToStr(iError) + ") was reported from the network subsystem.";

	  // workaround for com network error by mk...
      lLastError = AXIPCERR_NO_ERROR;
      csLastErrorMsg = "";

      break;
   }

 return lLastError;
}


void CAXIpcCtrl::GetAckTimeoutValue()
{
  CString csTO = "";


  csTO = GetRegValue("Software\\Progis\\AXWingis", "AckTimeout");

  try
    {
     m_lAckTimeout = 0;
     m_lAckTimeout = StrToInt(csTO);
    }
  catch(int)
    {
     m_lAckTimeout = 0;
    }

  if (m_lAckTimeout < 10)
    {
     csTO = "2000";
     SetRegValue("Software\\Progis\\AXWingis", "AckTimeout", csTO);
     m_lAckTimeout = StrToInt(csTO);
    }

}