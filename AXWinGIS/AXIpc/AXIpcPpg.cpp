// AXIpcPpg.cpp : Implementation of the CAXIpcPropPage property page class.

#include "stdafx.h"
#include "AXIpc.h"
#include "AXIpcPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CAXIpcPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CAXIpcPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CAXIpcPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CAXIpcPropPage, "AXIPC.AXIpcPropPage.1",
	0x44b6807, 0xc387, 0x11d3, 0x8f, 0xa9, 0, 0, 0xb4, 0xbd, 0xfe, 0x28)


/////////////////////////////////////////////////////////////////////////////
// CAXIpcPropPage::CAXIpcPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CAXIpcPropPage

BOOL CAXIpcPropPage::CAXIpcPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_AXIPC_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CAXIpcPropPage::CAXIpcPropPage - Constructor

CAXIpcPropPage::CAXIpcPropPage() :
	COlePropertyPage(IDD, IDS_AXIPC_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CAXIpcPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT

	SetHelpInfo(_T("Names to appear in the control"), _T("AXIPC.HLP"), 0);
}


/////////////////////////////////////////////////////////////////////////////
// CAXIpcPropPage::DoDataExchange - Moves data between page and properties

void CAXIpcPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CAXIpcPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CAXIpcPropPage message handlers
