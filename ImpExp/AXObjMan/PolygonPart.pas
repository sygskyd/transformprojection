unit PolygonPart;

interface

uses Obj_Def, Rectangle;

type TPolygonPartType=(None, Main, Island, OuterIsland); // set of possible Polygon types

const  Tolerance = 0.0000001;               // Tolerance for CutLines method

type
  TPolygonPart = class // class to store polygon part object
  public
     // public properties
     CreateIslandAssignements:integer;
     constructor Create;                                                       // constructor of class
     destructor  Destroy;                                                      // destructor of class
     procedure   SetPolygonPartType(other:TPolygonPartType);                   // set polygon type
     function    GetPolygonPartType:TPolygonPartType;                          // get polygon type
     procedure   InsertPoint(X:double; Y:double); overload;                    // add point to polygon
     function    IsInside(X:double; Y:double):boolean; overload;               // check if point is inside polygon
     function    IsInside(other:TPolygonPart):boolean; overload;               // check if other Polygon is inside own
     function    IsEqual(other:TPolygonPart):boolean;                          // check if other Polygon is equal to own
     function    Combine(var other:TPolygonPart; HandleHoles:boolean):integer; // method to combine two polygons
     procedure   CalculateArea;
     function    GetArea:double; overload;
     function    GetRectangle:TRectangle;
     function    GetNumCorners:integer;
     function    GetCornerByIndex(aIndex:integer; var X:double; var Y:double):boolean; overload;
     procedure   SetupWithCombineResult(CombRes:p_CombineResult_record);
     function    GetCombineResult(aIndex:integer):p_CombineResult_record;
     function    NumCombineResultPolys:integer;
     procedure   WriteDebugInfo(var aFile:TextFile);
  private
     PolyType       :TPolygonPartType;      // Type of Poly. (Main, Island, OuterIsland)
     Rectangle      :TRectangle;            // rectangle of Poly
     FirstCorner    :p_Point_record;        // pointer to first corner
     LastCorner     :p_Point_record;        // pointer to last corner
     NumCorners     :integer;               // number of corners
     Area           :double;                // Area of the Polygon Part
     PointOnLineCnt :integer;
     HasProcessItems:boolean;
     HasFoundItems  :boolean;

     CombineResult   : array of p_CombineResult_record; // List of polygons that representate combine result
     NumCombineResult:integer;

     // used for search a corner by index
     Search_CurrentIndex   :integer;
     Search_CurrentCorner  :p_Point_record;

     function  FindCorner(aPoint:PCPoint):p_Point_record; overload;
     function  FindCorner(aPoint:PCPoint; var aIndex:integer):p_Point_record; overload;
     function  GetCornerByIndex(aIndex:integer):p_Point_record; overload;
     function  AddIntersectionCorner(aPoint:PCPoint):integer;
     procedure InsertPoint(X:double; Y:double; var First:p_Point_record; var Last:p_Point_record); overload;
     function  GetArea(var aList:p_Point_record):double; overload;
     procedure GetPointWithin(var X:double; var Y:double);

     // methods to handle connection lines
     procedure InsertConnectionLine(Start_Own:integer; Start_Other:integer; End_Own:integer; End_Other:integer; OtherDir:integer; aPolyType:integer;
                                    var aListStart:p_ConnectionLine_record; var aListEnd:p_ConnectionLine_record);
     procedure RemoveConnectionLines(var aList:p_ConnectionLine_record);

     // methods that are used for combine polygon parts
     function  CheckMayCombineWith(var other:TPolygonPart;
                                   HandleHoles:boolean;
                                   Min_OwnTouchPointIdx:integer;
                                   Max_OwnTouchPointIdx:integer;
                                   Min_OtherTouchPointIdx:integer;
                                   Max_OtherTouchPointIdx:integer;
                                   NumTouchPoints:integer;
                                   var ConnectionLines:p_ConnectionLine_record):boolean;
     function DoesPolygonTypesFitForCombine(var other:TPolygonPart; NumTouchPoints:integer; var ConnectionLine:p_ConnectionLine_record):boolean;
     function  FindNextConnectionLineEnd(aStartIdx:integer; var ConnectionLines:p_ConnectionLine_record):p_ConnectionLine_record;
     procedure CreateCombineResult(var other:TPolygonPart; var ConnectionLines:p_ConnectionLine_record);
     procedure Revert;
     procedure RemoveCorners(var First:p_Point_record; var Last:p_Point_record);
     function CutLines(L1P1:PCPoint;L1P2:PCPoint;L2P1:PCPoint;L2P2:PCPoint;MayOverlap:boolean):integer;
     function IsPointOnLine(L1P1:pcpoint;L1P2:pcpoint;Point:pcpoint):boolean;
  end;

implementation

uses ObjectFifoBuffers, SysUtils;

constructor TPolygonPart.Create;
begin
   PolyType:=Main;
   Rectangle:=TRectangle.Create;
   FirstCorner:=nil;
   LastCorner:=nil;
   PointOnLineCnt:=0;
   NumCorners:=0;
   HasProcessItems:=FALSE;
   HasFoundItems:=FALSE;
   Search_CurrentIndex:=-1;
   Search_CurrentCorner:=nil;
   NumCombineResult:=0;
   CreateIslandAssignements:=0;
end;

destructor TPolygonPart.Destroy;
var iterate1, iterate2:p_Point_record;
    i:integer;
begin
   // remove the corners
   iterate1:=FirstCorner; iterate2:=FirstCorner;
   while (iterate1 <> nil) do
   begin
      iterate2:=iterate1;
      iterate1:=iterate1^.next;
      Release_p_Point_record(iterate2);
   end;
   Rectangle.Destroy;
   NumCorners:=0;
   if (NumCombineResult > 0) then
   begin
      // destroy only the list items not the corners (they have been assigned to another polygonpart)
      for i:=0 to NumCombineResult-1 do
         dispose(CombineResult[i]);
      SetLength(CombineResult,0);
   end;
end;

procedure TPolygonPart.RemoveCorners(var First:p_Point_record; var Last:p_Point_record);
var iterate1, iterate2:p_Point_record;
begin
   // remove the corners
   iterate1:=First; iterate2:=First;
   while (iterate1 <> nil) do
   begin
      iterate2:=iterate1;
      iterate1:=iterate1^.next;
      Release_p_Point_record(iterate2);
   end;
   First:=nil;
   Last:=nil;
end;

procedure TPolygonPart.SetPolygonPartType(other:TPolygonPartType);
begin
   PolyType:=other;
end;

function TPolygonPart.GetPolygonPartType:TPolygonPartType;
begin
   result:=PolyType;
end;

procedure TPolygonPart.InsertConnectionLine(Start_Own:integer; Start_Other:integer; End_Own:integer; End_Other:integer; OtherDir:integer; aPolyType:integer;
                                            var aListStart:p_ConnectionLine_record; var aListEnd:p_ConnectionLine_record);
var newitem:p_ConnectionLine_record;
begin
   newitem:=new(p_ConnectionLine_record);
   newitem^.Start_IdxOwn:=Start_Own;
   newitem^.Start_IdxOther:=Start_Other;
   newitem^.End_IdxOwn:=End_Own;
   newitem^.End_IdxOther:=End_Other;
   newitem^.OtherDir:=OtherDir;
   newitem^.ResultPolyType:=aPolyType;
   newitem^.prev:=nil;
   newitem^.next:=nil;
   if aListStart = nil then
   begin
      aListStart:=newitem;
      aListEnd:=newitem;
   end
   else
   begin
      newitem^.prev:=aListEnd;
      aListEnd^.next:=newitem;
      aListEnd:=newitem;
   end;
end;

procedure TPolygonPart.RemoveConnectionLines(var aList:p_ConnectionLine_record);
var iterate1, iterate2:p_ConnectionLine_record;
begin
   // remove the entries
   iterate1:=aList; iterate2:=aList;
   while (iterate1 <> nil) do
   begin
      iterate2:=iterate1;
      iterate1:=iterate1^.next;
      dispose(iterate2);
   end;
   aList:=nil;
end;

function TPolygonPart.GetArea(var aList:p_Point_record):double;
var iterate1,iterate2:p_Point_record;
    retVal:double;
begin
   iterate1:=aList; iterate2:=aList; retVal:=0;
   if aList = nil then exit;
   repeat
      iterate2:=iterate2^.next;
      if iterate2 = nil then iterate2:=aList; // to process connection line between end and begin
      retVal:=retVal+ ((iterate2^.Point.X  + iterate1^.Point.X) * (iterate2^.Point.Y - iterate1^.Point.Y));
      iterate1:=iterate1^.next;
   until iterate1 = nil;
   retVal:=ABS(retVal/2);
   result:=retVal;
end;

procedure TPolygonPart.CalculateArea;
var iterate1,iterate2:p_Point_record;
begin
   iterate1:=FirstCorner; iterate2:=FirstCorner; Area:=0;
   if FirstCorner = nil then exit;
   repeat
      iterate2:=iterate2^.next;
      if iterate2 = nil then iterate2:=FirstCorner; // to process connection line between end and begin
      Area:=Area+ ((iterate2^.Point.X  + iterate1^.Point.X) * (iterate2^.Point.Y - iterate1^.Point.Y));
      iterate1:=iterate1^.next;
   until iterate1 = nil;
   Area:=ABS(Area/2);
end;

function TPolygonPart.GetArea:double;
begin
   result:=Area;
end;

function TPolygonPart.GetRectangle:TRectangle;
begin
   result:=Rectangle;
end;

function TPolygonPart.GetNumCorners:integer;
begin
   result:=NumCorners;
end;

function TPolygonPart.GetCornerByIndex(aIndex:integer; var X:double; var Y:double):boolean;
var retVal:boolean;
    acorner:p_Point_record;
begin
   retVal:=FALSE;
   acorner:=GetCornerByIndex(aIndex);
   if (acorner <> nil) then
   begin
      X:=acorner^.Point.x;
      Y:=acorner^.Point.y;
   end;
   result:=retVal;
end;


procedure TPolygonPart.InsertPoint(X:double; Y:double; var First:p_Point_record; var Last:p_Point_record);
var newitem:p_Point_record;
begin
   newitem:=Request_p_Point_record;
   newitem^.Point.X:=X;
   newitem^.Point.Y:=Y;
   newitem^.addedforprocess:=FALSE;
   newitem^.found:=FALSE;
   newitem^.prev:=nil;
   newitem^.next:=nil;
   if First = nil then
   begin
      First:=newitem;
      Last:=newitem;
   end
   else
   begin
      newitem^.prev:=Last;
      Last^.next:=newitem;
      Last:=newitem;
   end;
end;

procedure TPolygonPart.InsertPoint(X:double; Y:double);
begin
   InsertPoint(X, Y, FirstCorner, LastCorner);
   // setup the rectangle
   Rectangle.SetupRect(X, Y);
   NumCorners:=NumCorners+1;
   Search_CurrentIndex:=-1;
   Search_CurrentCorner:=nil;
end;

// ------------------------------------------------------------------------
// NAME: FindCorner
// ------------------------------------------------------------------------
// DESCRIPTION : Method to search for corner in Polygon
//
// PARAMETERS  : aPoint -> Point that should be searched
//
// RETURNVALUE : if found return pointer to found corner, else nil
// ------------------------------------------------------------------------
function TPolygonPart.FindCorner(aPoint:PCPoint):p_Point_record;
var retVal :p_Point_record;
    iterate:p_Point_record;
begin
   retVal:=nil;
   iterate:=FirstCorner;
   while (iterate <> nil) and (retVal = nil) do
   begin
      if (iterate^.Point.x = aPoint.x) and
         (iterate^.Point.y = aPoint.y) then retVal:=iterate;
      iterate:=iterate^.next;
   end;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: FindCorner
// ------------------------------------------------------------------------
// DESCRIPTION : Method to search for corner in Polygon
//
// PARAMETERS  : aPoint -> Point that should be searched
//               aIndex -> returns index of the found corner in Polygon
//
// RETURNVALUE : if found return pointer to found corner, else nil
// ------------------------------------------------------------------------
function TPolygonPart.FindCorner(aPoint:PCPoint; var aIndex:integer):p_Point_record;
var retVal :p_Point_record;
    iterate:p_Point_record;
begin
   retVal:=nil;  aIndex:=0;
   // first check if new searched corner is previous or next of Search_CurrentCorner
   if Search_CurrentIndex <> -1 then
   begin
      // check if searched point is previous of current corner
      if Search_CurrentCorner^.prev <> nil then
      begin
         if (Search_CurrentCorner^.prev^.Point.x = aPoint.x) and
            (Search_CurrentCorner^.prev^.Point.y = aPoint.y) then
         begin
            aIndex:=Search_CurrentIndex-1;
            retVal:=Search_CurrentCorner^.prev;
         end;
      end
      else
      begin
         // check if search point is last corner
         if (LastCorner^.Point.x = aPoint.x) and
            (LastCorner^.Point.y = aPoint.y) then
         begin
            aIndex:=NumCorners-1;
            retVal:=LastCorner;
         end;
      end;
      if retVal = nil then
      begin
         if Search_CurrentCorner^.next <> nil then
         begin
            if (Search_CurrentCorner^.next^.Point.x = aPoint.x) and
               (Search_CurrentCorner^.next^.Point.y = aPoint.y) then
            begin
               aIndex:=Search_CurrentIndex+1;
               retVal:=Search_CurrentCorner^.next;
            end;
         end
         else
         begin
            if (FirstCorner^.Point.x = aPoint.x) and
               (FirstCorner^.Point.y = aPoint.y) then
            begin
               aIndex:=0;
               retVal:=FirstCorner;
            end;
         end;
      end;
   end;

   iterate:=FirstCorner;
   while (iterate <> nil) and (retVal = nil) do
   begin
      if (iterate^.Point.x = aPoint.x) and
         (iterate^.Point.y = aPoint.y) then retVal:=iterate
      else
      begin
         iterate:=iterate^.next; aIndex:=aIndex+1;
      end;
   end;
   if (retVal <> nil) then
   begin
      Search_CurrentIndex:=aIndex;
      Search_CurrentCorner:=retVal;
   end;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: GetCornerByIndex
// ------------------------------------------------------------------------
// DESCRIPTION : Method to get a corner from Poly by index
//
// PARAMETERS  : Index of Corner (First is Index 0)
//
// RETURNVALUE : if found return pointer to found corner, else nil
// ------------------------------------------------------------------------
function TPolygonPart.GetCornerByIndex(aIndex:integer):p_Point_record;
var retVal :p_Point_record;
    SearchStart:p_Point_record;
    SearchCnt  :integer;
    SearchFwd  :boolean;
    Mid        :integer;
    cnt        :integer;
begin
   retVal:=nil;
   if (aIndex < 0) or (aIndex >= NumCorners) then begin result:=retVal; exit; end;
   if (aIndex = Search_CurrentIndex) then begin result:=Search_CurrentCorner; exit; end;

   // first find the quickest way to get the corner
   Mid:=NumCorners div 2;
   if (aIndex < Mid) then
   begin
      // Search from the Begin
      SearchStart:=FirstCorner; SearchCnt:=aIndex; SearchFwd:=TRUE;
   end
   else
   begin
      // Search from the End
      SearchStart:=LastCorner; SearchCnt:=NumCorners-1-aIndex; SearchFwd:=FALSE;
   end;

   if (Search_CurrentIndex <> -1) then
   begin
      if (aIndex < Search_CurrentIndex) then
      begin
         // item that should be searched for is before current index
         if ((Search_CurrentIndex-aIndex) < SearchCnt) then
         begin
            SearchCnt:=Search_CurrentIndex-aIndex;
            SearchStart:=Search_CurrentCorner;
            SearchFwd:=FALSE;
         end;
      end
      else
      begin
         // item that should be searched for is after current index
         if ((aIndex - Search_CurrentIndex) < SearchCnt) then
         begin
            SearchCnt:=aIndex-Search_CurrentIndex;
            SearchStart:=Search_CurrentCorner;
            SearchFwd:=TRUE;
         end;
      end;
   end;

   // now go to the position
   retVal:=SearchStart; cnt:=0;
   while (cnt < SearchCnt) do
   begin
      if (SearchFwd) then retVal:=retVal^.next
      else retVal:=retVal^.prev;
      cnt:=cnt+1;
   end;
   // set the search index
   Search_CurrentIndex:=aIndex;
   Search_CurrentCorner:=retVal;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: IsEqual
// ------------------------------------------------------------------------
// DESCRIPTION : Method to check if other polygon is equal to own
//
// PARAMETERS  : other -> Polygon that should be checked
//
// RETURNVALUE : TRUE -> other poly is equal to own, else FALSE
// ------------------------------------------------------------------------
function TPolygonPart.IsEqual(other:TPolygonPart):boolean;
var retVal :boolean;
    iterate:p_Point_record;
    docheck:boolean;
begin
   retVal:=FALSE;
   result:=retVal;
   // first check if the rectangles are equal
   if not Rectangle.IsEqual(other.Rectangle) then begin result:=FALSE; exit; end;
   // the rectangles are equal -> Check the number of corners
   if NumCorners <> other.NumCorners then begin result:=FALSE; exit; end;
   // the number of corners is also equal -> Check corners itself
   iterate:=FirstCorner; docheck:=TRUE;
   while (iterate <> nil) and (docheck) do
   begin
      if other.FindCorner(iterate^.Point) = nil then docheck:=FALSE;
      iterate:=iterate^.next;
   end;
   if docheck then retVal:=TRUE else retVal:=FALSE;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: Combine
// ------------------------------------------------------------------------
// DESCRIPTION : Method to combine two polygon parts
//
// PARAMETERS  : other       -> Polygon that should be combined with own
//               HandleHoles -> Flag that shows if Holes between Polygonparts
//                              should be handeled.
//
// RETURNVALUE : Code how polygons have been combined
// ------------------------------------------------------------------------
function TPolygonPart.Combine(var other:TPolygonPart; HandleHoles:boolean):integer;
var retVal                :integer;
    iterate               :p_Point_record;
    OwnCornerIdx          :integer;
    OtherCornerIdx        :integer;
    OwnTouchPointIdx      :integer;
    OtherTouchPointIdx    :integer;
    Min_OwnTouchPointIdx  :integer;
    Max_OwnTouchPointIdx  :integer;
    Min_OtherTouchPointIdx:integer;
    Max_OtherTouchPointIdx:integer;
    NumTouchPoints        :integer;
    ConnectionLines       :p_ConnectionLine_record;
    DebugFile             :Textfile;
begin
   retVal:=No_Combine;
   result:=retVal;
   // first check if the rectangles do overlap
   if Rectangle.GetOverLapType(other.Rectangle, FSnapRadius) = No_Overlap then exit;
   // rectangles do overlap
   // Now create the intersection points between own polygon that
   // still do not exist in other polygon
   iterate:=FirstCorner;
   while (iterate <> nil) do
   begin
      other.AddIntersectionCorner(iterate^.Point);
      iterate:=iterate^.next;
   end;
   // Now create intersection points between other polygon that
   // still do not exist in own polygon
   // here we can count the number of touching points between the polygons
   Min_OwnTouchPointIdx:=-1;
   Max_OwnTouchPointIdx:=-1;
   Min_OtherTouchPointIdx:=-1;
   Max_OtherTouchPointIdx:=-1;
   OtherTouchPointIdx:=0;
   NumTouchPoints:=0;
   iterate:=other.FirstCorner;
   while (iterate <> nil) do
   begin
      OwnTouchPointIdx:=AddIntersectionCorner(iterate^.Point);
      if OwnTouchPointIdx <> -1 then
      begin
         NumTouchPoints:=NumTouchPoints+1;
         // setup Own Min/Max touch-point index
         if Min_OwnTouchPointIdx = -1 then
            Min_OwnTouchPointIdx:=OwnTouchPointIdx;
         if Max_OwnTouchPointIdx = -1 then
            Max_OwnTouchPointIdx:=OwnTouchPointIdx;
         if OwnTouchPointIdx < Min_OwnTouchPointIdx then
            Min_OwnTouchPointIdx:=OwnTouchPointIdx;
         if OwnTouchPointIdx > Max_OwnTouchPointIdx then
            Max_OwnTouchPointIdx:=OwnTouchPointIdx;
         // setup other Min/Max touch-point index
         if Min_OtherTouchPointidx = -1 then
            Min_OtherTouchPointIdx:=OtherTouchPointIdx;
         if Max_OtherTouchPointIdx = -1 then
            Max_OtherTouchPointIdx:=OtherTouchPointIdx;
         if OtherTouchPointIdx < Min_OtherTouchPointIdx then
            Min_OtherTouchPointIdx:=OtherTouchPointIdx;
         if OtherTouchPointIdx > Max_OtherTouchPointIdx then
            Max_OtherTouchPointIdx:=OtherTouchPointIdx;
      end;
      iterate:=iterate^.next; OtherTouchPointIdx:=OtherTouchPointIdx+1;
   end;

   // now check if there are touch - points between the polygons
   if (NumTouchPoints < 2) then // at least two touch points are necessary to get a combination line
   begin
      // we got no point that touches other polygon so no combine is possible
      result:=No_Combine;
      Revert;
      other.Revert;
      exit;
   end;

   if FDoDebug then
   begin
      AssignFile(DebugFile, 'comb.dbg');
      Rewrite(DebugFile);
      WriteDebugInfo(DebugFile);
      other.WriteDebugInfo(DebugFile);
      Closefile(Debugfile);
   end;

   // check if all corners of both polygons are touching points
   if (NumCorners = NumTouchPoints) and (other.NumCorners = NumTouchPoints) then
   begin
      // the polygons are equal -> Check if one is an island and the other one
      // is not an island -> in this case both polygons are obsulete
      if ((PolyType = Island) and (other.PolyType <> Island)) or
          (PolyType <> Island) and (other.PolyType = Island) then
      begin
         retVal:=Remove_Both;
         result:=retVal;
         exit; // due to both polygons can be removed -> no Revert is neccessary
      end
      else
      begin
         // polygons can be combined due to they are equal
         retVal:=No_Combine;
         result:=retVal;
         Revert;
         other.Revert;
         exit;
      end;
   end;

   // now check if the two polygons may be combined
   ConnectionLines:=nil;
   if not CheckMayCombineWith(other, HandleHoles,
                              Min_OwnTouchPointIdx, Max_OwnTouchPointIdx,
                              Min_OtherTouchPointIdx, Max_OtherTouchPointIdx,
                              NumTouchPoints, ConnectionLines) then
   begin
      // The Polygons can not be combined
      RemoveConnectionLines(ConnectionLines);
      retVal:=No_Combine;
      result:=retVal;
      Revert;
      other.Revert;
      exit;
   end;
   // create the resulting polygons
   CreateCombineResult(other, ConnectionLines);
   RemoveConnectionLines(ConnectionLines);
   retVal:=Remove_Other;
   result:=retVal;
end;

function TPolygonPart.FindNextConnectionLineEnd(aStartIdx:integer; var ConnectionLines:p_ConnectionLine_record):p_ConnectionLine_record;
var retVal:p_ConnectionLine_record;
    MinDelta:integer;
    iterate :p_ConnectionLine_record;
    CurDelta:integer;
begin
   MinDelta:=-1; retVal:=nil;
   iterate:=ConnectionLines;
   while (iterate <> nil) do
   begin
      // calculate the number of necessary steps if we go against clock direction till current
      // connection line end
      if (iterate^.End_IdxOwn > aStartIdx) then
      begin
         CurDelta:=aStartIdx + (NumCorners - iterate^.End_IdxOwn);
      end
      else
      begin
         CurDelta:=aStartIdx -iterate^.End_IdxOwn;
      end;
      if (MinDelta = -1) then
      begin
         MinDelta:=CurDelta;
         retVal:=iterate;
      end
      else if (CurDelta < MinDelta) then
      begin
         MinDelta:=CurDelta;
         retVal:=iterate;
      end;
      iterate:=iterate^.next;
   end;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: SetupWithCombineResult
// ------------------------------------------------------------------------
// DESCRIPTION : Method is used to setup the own polygon by using a combine
//               result
//
// PARAMETERS  : CombRes             -> Pointer to combine result
//
// RETURNVALUE : NONE
// ------------------------------------------------------------------------
procedure TPolygonPart.SetupWithCombineResult(CombRes:p_CombineResult_record);
var iterate:p_Point_record;
begin
   // get the corners
   FirstCorner:=CombRes^.FirstCorner;
   LastCorner:=CombRes^.LastCorner;
   NumCorners:=CombRes^.NumCorners;
   Area:=CombRes^.Area;
   if CombRes^.PolyType = 0 then PolyType:=Main;
   if CombRes^.PolyType = 1 then PolyType:=Island;
   if CombRes^.PolyType = 2 then PolyType:=OuterIsland;
   // now calculate the rectangle
   iterate:=FirstCorner;
   while (iterate <> nil) do
   begin
      Rectangle.SetupRect(iterate^.Point.X, iterate^.Point.Y);
      iterate:=iterate^.next;
   end;
end;

// ------------------------------------------------------------------------
// NAME: NumCombineResultPolys
// ------------------------------------------------------------------------
// DESCRIPTION : Method is used to get number of combine result polygonparts
//
// PARAMETERS  : NONE
//
// RETURNVALUE : Number of combine result polygon parts
// ------------------------------------------------------------------------
function TPolygonPart.NumCombineResultPolys:integer;
begin
   result:=NumCombineResult;
end;

// ------------------------------------------------------------------------
// NAME: GetCombineResult
// ------------------------------------------------------------------------
// DESCRIPTION : Method is used to get a combine result entry
//               result
//
// PARAMETERS  : aIndex -> Index of the combine result entry
//
// RETURNVALUE : Pointer to a combine result entry
// ------------------------------------------------------------------------
function TPolygonPart.GetCombineResult(aIndex:integer):p_CombineResult_record;
var retVal:p_CombineResult_record;
begin
   retVal:=nil;
   if aIndex < NumCombineResult then
      retVal:=CombineResult[aIndex];
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: CreateCombineResult
// ------------------------------------------------------------------------
// DESCRIPTION : Method to create combine result between two polygons
//
// PARAMETERS  : other               -> Polygon that should be combined with own
//               ConnectionLines     -> Pointer to List of Connection lines
//
// RETURNVALUE : NONE
// ------------------------------------------------------------------------
procedure TPolygonPart.CreateCombineResult(var other:TPolygonPart;
                                           var ConnectionLines:p_ConnectionLine_record);
var newcorner:p_Point_record;
    PointIdx:integer;
    oldcorner:p_Point_record;
    CLineIterate:p_ConnectionLine_record;
    ResPoly     :p_CombineResult_record;
    CombRes     :array of p_CombineResult_record;
    NumCombRes  :integer;
    OtherLine   :p_ConnectionLine_record;
    CurMaxIdx   :integer;
    i           :integer;
begin
   // create polygons for all connection lines
   CLineIterate:=ConnectionLines;
   NumCombRes:=0;
   while (CLineIterate <> nil) do
   begin
      ResPoly:=new(p_CombineResult_record);
      ResPoly^.FirstCorner:=nil;
      ResPoly^.LastCorner:=nil;
      ResPoly^.NumCorners:=0;
      ResPoly^.PolyType:=CLineIterate^.ResultPolyType;
      // add it to temporary result list
      // must be temporary due to result has to be sorted by area at the end to get the new
      // created islands/outer islands
      NumCombRes:=NumCombRes+1;
      SetLength(CombRes, NumCombRes);
      CombRes[NumCombRes-1]:=ResPoly;

      // first we go in own polygon against clock direction from StartIdx till we reach the next connection-line end
      // so we first need to find the next connection line end that we reach
      OtherLine:=FindNextConnectionLineEnd(CLineIterate^.Start_IdxOwn, ConnectionLines);
      PointIdx:=CLineIterate^.Start_IdxOwn;
      while (PointIdx <> OtherLine^.End_IdxOwn) do
      begin
         oldcorner:=GetCornerByIndex(PointIdx);
         InsertPoint(oldcorner^.Point.X, oldcorner^.Point.Y, ResPoly^.FirstCorner, ResPoly^.LastCorner);
         ResPoly^.NumCorners:=ResPoly^.NumCorners+1;
         PointIdx:=PointIdx-1;
         if (Pointidx < 0) then
            PointIdx:=NumCorners-1;
      end;
      // now we go in the other polygon from the connection line end
      PointIdx:=OtherLine^.End_IdxOther;
      while (PointIdx <> CLineIterate^.Start_IdxOther) do
      begin
         oldcorner:=other.GetCornerByIndex(PointIdx);
         InsertPoint(oldcorner^.Point.X, oldcorner^.Point.Y, ResPoly^.FirstCorner, ResPoly^.LastCorner);
         ResPoly^.NumCorners:=ResPoly^.NumCorners+1;
         if (CLineIterate^.OtherDir = 0) then
         begin
            // connection line goes in clock direction -> So we have to go also in clock direction
            PointIdx:=PointIdx+1;
            if (PointIdx > other.NumCorners-1) then
               PointIdx:=0;
         end
         else if (CLineIterate^.Otherdir = 1) then
         begin
            PointIdx:=PointIdx-1;
            if (PointIdx < 0) then
               PointIdx:=other.NumCorners-1;
         end;
      end;

      // we get a resulting polygon now
      // -> calculate the Area that is necessary for sorting the result then
      ResPoly^.Area:=GetArea(ResPoly^.FirstCorner);
      CLineIterate:=CLineIterate^.next;
   end;
   // now the result will be sorted
   NumCombineResult:=0;
   while (NumCombineResult <> NumCombRes) do
   begin
      CurMaxIdx:=-1;
      for i:=0 to NumCombRes-1 do
      begin
         if (CurMaxIdx = -1) and (CombRes[i] <> nil) then
            CurMaxIdx:=i
         else
         begin
            if (CombRes[i] <> nil) and (CombRes[i]^.Area > CombRes[CurMaxIdx]^.Area) then
               CurMaxIdx:=i;
         end;
      end;
      // add item at CurMax Idx to result
      NumCombineResult:=NumCombineResult+1;
      SetLength(CombineResult, NumCombineResult);
      CombineResult[NumCombineResult-1]:=CombRes[CurMaxIdx];
      if NumCombineResult > 1 then
      begin
         // handle the Polygon Types of the holes that have been created to Polygons
         if (CombineResult[0]^.PolyType = 0) then CombineResult[NumCombineResult-1]^.PolyType:=1; // original is Main - > hole becomes Island
         if (CombineResult[0]^.PolyType = 1) then CombineResult[NumCombineResult-1]^.PolyType:=2; // original is Island -> hole becomes OuterIsland
         if (CombineResult[0]^.PolyType = 2) then CombineResult[NumCombineResult-1]^.PolyType:=1; // original is OuterIsland -> hole becomes Island
      end;
      CombRes[CurMaxIdx]:=nil;
   end;
   SetLength(CombRes, 0);
   Search_CurrentIndex:=-1; // reset the search index
   Search_CurrentCorner:=nil;
end;

// ------------------------------------------------------------------------
// NAME: DoesPolygonTypesFitForCombine
// ------------------------------------------------------------------------
// DESCRIPTION : Method to check if two polygons that have a connection
//               line may be combined (type could not fit, or polygons do overlep)
//
// PARAMETERS  : other               -> other polygon with that should be checked
//               NumTouchPoints      -> Number of touching points between the two polygons
//               ConnectionLine      -> Pointer to connection line entry
//
// RETURNVALUE : TRUE -> Polygons may be combined, else FALSE
// ------------------------------------------------------------------------
function TPolygonPart.DoesPolygonTypesFitForCombine(var other:TPolygonPart;
                                                    NumTouchPoints:integer;
                                                    var ConnectionLine:p_ConnectionLine_record):boolean;
var ownisinside   :boolean;
    otherisinside :boolean;
    iterate       :p_Point_record;
    OwnPointIdx   :integer;
    OtherPointIdx :integer;
    retVal        :boolean;
    hadfound      :boolean;
begin
   retVal:=FALSE;
   ownisinside:=FALSE;
   otherisinside:=FALSE;
   iterate:=GetCornerByIndex(ConnectionLine^.Start_IdxOwn);
   other.FindCorner(iterate^.Point, ConnectionLine^.Start_IdxOther);
   iterate:=GetCornerByIndex(ConnectionLine^.End_IdxOwn);
   other.FindCorner(iterate^.Point, ConnectionLine^.End_IdxOther);
   // check if own corner is inside other polygon or
   // other corner is inside own
   if (NumTouchPoints <> NumCorners) then
   begin
      // find next corner that is not a touching point in clock direction
      // beginning at End_IdxOwn
      OwnPointIdx:=ConnectionLine^.End_IdxOwn;
      hadfound:=FALSE;
      while (hadfound = FALSE) do
      begin
         OwnPointIdx:=OwnPointIdx+1;
         if (OwnPointIdx > NumCorners-1) then
            OwnPointIdx:=0;
         iterate:=GetCornerByIndex(OwnPointIdx);
         if not iterate^.Found then
            hadfound:=TRUE;   // -> we got the first corner outside of the connection line
      end;
      // check if corner of own polygon is inside the other polygon
      iterate:=GetCornerByIndex(OwnPointIdx);
      ownisinside:=other.IsInside(iterate^.Point.X, iterate^.Point.Y);
   end;
   if (NumTouchPoints <> other.NumCorners) then
   begin
      OtherPointIdx:=ConnectionLine^.End_IdxOther;
      if (ConnectionLine^.OtherDir = 0) then
      begin
         hadfound:=FALSE;
         while (hadfound = FALSE) do
         begin
            OtherPointIdx:=OtherPointIdx+1;
            if (OtherPointIdx > other.NumCorners-1) then
               OtherPointIdx:=0;
            iterate:=other.GetCornerByIndex(OtherPointIdx);
            if not iterate^.Found then
               hadfound:=TRUE; // we got the first corner outside of the connection line
         end;
      end;
      if (ConnectionLine^.OtherDir = 1) then
      begin
         hadfound:=FALSE;
         while (hadfound = FALSE) do
         begin
            OtherPointIdx:=OtherPointIdx-1;
            if (OtherPointIdx < 0) then
               OtherPointIdx:=other.NumCorners-1;
            iterate:=other.GetCornerByIndex(OtherPointIdx);
            if not iterate^.Found then
               hadfound:=TRUE; // we got the first corner outside of the connection line
         end;
      end;
      // check if corner of own polygon is inside the other polygon
      iterate:=other.GetCornerByIndex(OtherPointIdx);
      otherisinside:=IsInside(iterate^.Point.X, iterate^.Point.Y);
   end;
   if (otherisinside or ownisinside) then
   begin
      // one polygon is inside of the other
      if ((PolyType = Island) and (other.PolyType = Island)) or
         ((PolyType <> Island) and (other.PolyType <> Island)) then
      begin
         // Polygons with same type may not overlap
         retVal:=FALSE;
      end
      else if (PolyType = Island) and (other.PolyType <> Island) and (otherisinside) then
      begin
         // Island will be made smaller with main polygon
         ConnectionLine^.ResultPolyType:=1; // -> Island result is again an island
         retVal:=TRUE;
      end
      else if (PolyType <> Island) and (other.PolyType = Island) and (ownisinside) then
      begin
         // Island will be made smaller with main polygon
         ConnectionLine^.ResultPolyType:=1; // -> Island result is an island
         retVal:=TRUE;
      end;
   end
   else
   begin
      // none of the polygons is inside of the other
      // the polygons must have same type that they may be combined
      if not (((PolyType = Island) and (other.PolyType = Island)) or
             ((PolyType <> Island) and (other.PolyType <> Island))) then
         retVal:=FALSE
      else
      begin
         // polygons do not have point inside the other and have same type
         // --> They may be combined
         retVal:=TRUE;
         // Polygon type keeps the same
         if PolyType = Main        then ConnectionLine^.ResultPolyType:=0;
         if PolyType = Island      then ConnectionLine^.ResultPolyType:=1;
         if PolyType = OuterIsland then ConnectionLine^.ResultPolyType:=2;
      end;
   end;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: CheckMayCombineWith
// ------------------------------------------------------------------------
// DESCRIPTION : Method to check if there is a connection line between own
//               and other polygon
//
// PARAMETERS  : other                  -> other polygon with that should be checked
//               HandleHoles            -> Flag if holes within the Polygon parts should be handeled
//               Min_OwnTouchPointIdx   -> Index of first touchpoint of own polygon
//               Max_OwnTouchPointIdx   -> Index of the last touchpoint of own polygon
//               Min_OtherTouchPointIdx -> Minimal touchpoint index of other polygon
//               Max_OtherTouchPointIdx -> Maximal touchpoint index of other polygon
//               NumTouchPoints         -> Number of all touch points between the polygons
//               ConnectionLines        -> Pointer to List of Connectionlines
//
// RETURNVALUE : TRUE -> Polygons have a connection line
// ------------------------------------------------------------------------
function  TPolygonPart.CheckMayCombineWith(
                                           var other:TPolygonPart;
                                           HandleHoles:boolean;
                                           Min_OwnTouchPointIdx:integer;
                                           Max_OwnTouchPointIdx:integer;
                                           Min_OtherTouchPointIdx:integer;
                                           Max_OtherTouchPointIdx:integer;
                                           NumTouchPoints:integer;
                                           var ConnectionLines:p_ConnectionLine_record):boolean;
var retVal           :boolean;
    iterate          :p_Point_record;
    othercorner      :p_Point_record;
    PointsLeft       :integer;
    LastOwnPointIdx  :integer;
    LastOtherPointIdx:integer;
    OtherPointIdx    :integer;
    OwnPointIdx      :integer;
    docheck          :boolean;
    CurOwnStartIdx   :integer;
    CurOwnEndIdx     :integer;
    CLineIterate     :p_ConnectionLine_record;
    OtherDir         :integer; // current other dir
    ConnectionLineEnd:p_ConnectionLine_record; // end (needed for insert)
begin
   // first check if all of own corners are touching points
   // in this case the connection line has to be searched on the other polygon
   if (NumTouchPoints = NumCorners) then
   begin
      retVal:=other.CheckMayCombineWith(self, HandleHoles,
                                        Min_OtherTouchPointIdx, Max_OtherTouchPointIdx,
                                        Min_OwnTouchPointIdx, Max_OwnTouchPointIdx,
                                        NumTouchPoints, ConnectionLines);
      if retVal then
      begin
         // if the polygons could be combined -> process the connectionlines
         // due to they refer to other polygonpart, but should for own
         CLineIterate:=ConnectionLines;
         while (CLineIterate <> nil) do
         begin
            // due to the polygons have been changed for comination the Connection line
            // has to be setuped
            if CLineIterate^.OtherDir = 1 then
            begin
               CurOwnStartIdx:=CLineIterate^.End_IdxOther;
               CurOwnEndIdx:=CLineIterate^.Start_IdxOther;
               CLineIterate^.Start_IdxOther:=CLineIterate^.End_IdxOwn;
               CLineIterate^.End_IdxOther:=CLineIterate^.Start_IdxOwn;
               CLineIterate^.Start_IdxOwn:=CurOwnStartIdx;
               CLineIterate^.End_IdxOwn:=CurOwnEndIdx;
            end
            else
            begin
               CurOwnStartIdx:=CLineIterate^.Start_IdxOther;
               CurOwnEndIdx:=CLineIterate^.End_IdxOther;
               CLineIterate^.Start_IdxOther:=CLineIterate^.Start_IdxOwn;
               CLineIterate^.End_IdxOther:=CLineIterate^.End_IdxOwn;
               CLineIterate^.Start_IdxOwn:=CurOwnStartIdx;
               CLineIterate^.End_IdxOwn:=CurOwnEndIdx;
            end;
            CLineIterate:=CLineIterate^.next;
         end;
      end;
      result:=retVal;
      exit;
   end;
   retVal:=FALSE;
   ConnectionLineEnd:=ConnectionLines;
   PointsLeft:=NumTouchPoints; // count the number touching points that are left
   if (Min_OwnTouchPointidx = 0) and (Max_OwnTouchPointidx = NumCorners-1) then
   begin
      // set the CurOwnStartIdx where it should be started to search
      // due to the polygon has been rotated till no connection line between start<->end
      // exists the starting corner of the polygon is 0
      while (LastCorner^.found) do
      begin
         // rotate last corner before first corner
         iterate:=LastCorner^.prev; // to store current last corner
         LastCorner^.next:=FirstCorner;
         FirstCorner^.prev:=LastCorner;
         FirstCorner:=LastCorner;
         FirstCorner^.prev:=nil;
         LastCorner:=iterate;
         LastCorner^.next:=nil;
      end;
      CurOwnStartIdx:=-1;  // connection line start could only be first corner
      OwnPointIdx:=0;      // we start with first corner
      LastOwnPointIdx:=-1; // we still got no last corner
   end
   else
   begin
      CurOwnStartIdx:=-1;
      OwnPointIdx:=Min_OwnTouchPointIdx;
      LastOwnPointIdx:=-1; // we still got no last corner
   end;

   Otherdir:=-1; // Other direction is not evaluated yet
   // reset the Seach indizess

   Search_CurrentIndex:=-1;
   Search_CurrentCorner:=nil;
   other.Search_CurrentIndex:=-1;
   other.Search_CurrentCorner:=nil;

   iterate:=GetCornerByIndex(OwnPointIdx); docheck:=TRUE;
   while (iterate <> nil) and (docheck) do
   begin
      othercorner:=other.FindCorner(iterate^.Point, OtherPointIdx);
      if (othercorner <> nil) then
      begin
         // we found a touching point
         PointsLeft:=PointsLeft-1;
         if CurOwnStartIdx = -1 then
         begin
            // we found a new possible start of a connection line
            CurOwnStartIdx:=OwnPointIdx;
         end
         else
         begin
            // check if current corner is on a connection line
            if OwnPointIdx = LastOwnPointIdx+1 then
            begin
               // we have a connection line on the own polygon
               // now check if there is also a connection line on the other polygon
               if (OtherDir = -1) then
               begin
                  // we still got no direction from the corners of the other polygon
                  if (LastOtherPointIdx+1 = OtherPointIdx)      then OtherDir:=0  // other polygon has connection line in clock direction
                  else if (LastOtherPointIdx-1 = OtherPointIdx) then OtherDir:=1  // other polygon is not in clock direction
                  else if (LastOtherPointIdx   = 0) and (OtherPointIdx = other.NumCorners -1) then OtherDir:=1 // other polygon is not in clock direction
                  else if (LastOtherPointIdx = other.NumCorners-1) and (OtherPointIdx = 0)    then OtherDir:=0;
                  if (OtherDir = -1) then
                  begin
                     // if no direction could be evaluated there is no connection line on the other polygon
                     retVal:=FALSE;
                     docheck:=FALSE;
                  end;
               end
               else
               begin
                  if (OtherDir = 0) then // other polygon is in clock direction
                  begin
                     if not ((LastOtherPointIdx+1 = OtherPointIdx) or ((LastOtherPointIdx = other.NumCorners-1) and (OtherPointIdx = 0))) then
                     begin
                        if (HandleHoles) then
                        begin
                           // it could be that there is a hole on the other polygon
                           if CurOwnStartIdx < LastOwnPointIdx then
                           begin
                              // we had a connection line from CurOwnStartIdx till LastOwnPointIdx !!
                              // -> add it to the connection line list
                             InsertConnectionLine(CurOwnStartIdx, -1, LastOwnPointIdx, -1, OtherDir, -1,ConnectionLines, ConnectionLineEnd);
                             CurOwnStartIdx:=OwnPointIdx; // new Start of possible connection line is own point index
                           end
                           else
                           begin
                              // we got a single connection point -> may not combine polygons
                              retVal:=FALSE;
                              docheck:=FALSE;
                           end;
                        end
                        else
                        begin
                           // there is no connection line on other polygon
                           retVal:=FALSE;
                           docheck:=FALSE;
                        end;
                     end;
                  end;
                  if (OtherDir = 1) then // other polygon is not in clock direction
                  begin
                     if not ((LastOtherPointIdx-1 = OtherPointIdx) or ((LastOtherPointIdx   = 0) and (OtherPointIdx = other.NumCorners -1))) then
                     begin
                        if (HandleHoles) then
                        begin
                           // it could be that there is a hole on the other polygon
                           if CurOwnStartIdx < LastOwnPointIdx then
                           begin
                              // we had a connection line from CurOwnStartIdx till LastOwnPointIdx !!
                              // -> add it to the connection line list
                             InsertConnectionLine(CurOwnStartIdx, -1, LastOwnPointIdx, -1, OtherDir, -1,ConnectionLines, ConnectionLineEnd);
                             CurOwnStartIdx:=OwnPointIdx; // new Start of possible connection line is own point index
                           end
                           else
                           begin
                              // we got a single connection point -> may not combine polygons
                              retVal:=FALSE;
                              docheck:=FALSE;
                           end;
                        end
                        else
                        begin
                           // there is no connection line on other polygon
                           retVal:=FALSE;
                           docheck:=FALSE;
                        end;
                     end;
                  end;
               end;
               if (docheck) and (PointsLeft = 0) then
               begin
                  // we found all connection lines!
                  // it goes from CurOwnStartIdx to OwnPointIdx
                  if CurOwnStartIdx < OwnPointIdx then
                  begin
                     InsertConnectionLine(CurOwnStartIdx, -1, OwnPointIdx, -1, OtherDir, -1,ConnectionLines, ConnectionLineEnd);
                     retVal:=TRUE;
                     docheck:=FALSE;
                  end
                  else
                  begin
                     // last connection point is single point
                     // -> may not combine
                     retVal:=FALSE;
                     docheck:=FALSE;
                  end;
               end;
            end
            else
            begin
               // current point is not the connection line
               docheck:=FALSE;
            end;
         end;
         LastOtherPointIdx:=OtherPointIdx;
      end
      else
      begin
         if (not HandleHoles) then
         begin
            // if holes should not be handeled there may only be one connection line
            // -> Parts could not be combined
            docheck:=FALSE;
         end
         else
         begin
            // we found a point that is not on a connection line
            // check if previous corners built a connection line
            if (CurOwnStartIdx <> -1) then
            begin
               if CurOwnStartIdx < LastOwnPointIdx then
               begin
                  // we had a connection line from CurOwnStartIdx till LastOwnPointIdx !!
                  // -> add it to the connection line list
                  InsertConnectionLine(CurOwnStartIdx, -1, LastOwnPointIdx, -1, OtherDir, -1,ConnectionLines, ConnectionLineEnd);
               end
               else
               begin
                  // previous corner did not build a connection line
                  // -> polygons could not be combined
                  docheck:=FALSE;
               end;
               CurOwnStartIdx:=-1; // search for next start of connection line
            end;
         end;
      end;
      LastOwnPointIdx:=OwnPointIdx;
      iterate:=iterate^.next; OwnPointIdx:=OwnPointIdx+1;
   end;
   if (retVal) then
   begin
      CLineIterate:=ConnectionLines;
      while (CLineIterate <> nil) and (retVal) do
      begin
         // check if the two polygons are allowed to be combined ( could be overlap, or PolygonType does not fit)
         retVal:=DoesPolygonTypesFitForCombine(other,NumTouchPoints, CLineIterate);
         CLineIterate:=CLineIterate^.next;
      end;
   end;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: Revert
// ------------------------------------------------------------------------
// DESCRIPTION : Method to revert the Polygon
//               all found flags and corners that have been added for
//               processing will be removed
//
// PARAMETERS  : NONE
//
// RETURNVALUE : NONE
// ------------------------------------------------------------------------
procedure TPolygonPart.Revert;
var iterate:p_Point_record;
    old:p_Point_record;
begin
   if (HasProcessItems=FALSE) and (HasFoundItems = FALSE) then
      exit; // nothing to revert -> do nothing

   Rectangle.Destroy;
   Rectangle:=TRectangle.Create;
   iterate:=FirstCorner;
   while (iterate <> nil) do
   begin
      if iterate^.found then iterate^.found:=FALSE;
      if iterate^.addedforprocess then
      begin
         // item must be removed from list
         if iterate^.prev = nil then // its the first corner in list
         begin
            FirstCorner:=iterate^.next;
            if FirstCorner <> nil then FirstCorner^.prev:=nil;
            Release_p_Point_record(iterate);
            iterate:=FirstCorner;
            NumCorners:=NumCorners-1;
         end
         else if iterate^.next = nil then // its the last corner in list
         begin
            LastCorner:=iterate^.prev;
            LastCorner^.next:=nil;
            Release_p_Point_record(iterate);
            iterate:=nil;
            NumCorners:=NumCorners-1;
         end
         else
         begin // it�s in the middle of the list
            iterate^.next^.prev:=iterate^.prev;
            iterate^.prev^.next:=iterate^.next;
            old:=iterate;
            iterate:=iterate^.next;
            Release_p_Point_record(old);
            NumCorners:=NumCorners-1;
         end;
      end
      else
      begin
         Rectangle.SetupRect(iterate^.Point.x, iterate^.Point.y);
         iterate:=iterate^.next;
      end;
   end;
   HasProcessItems:=FALSE;
   HasFoundItems:=FALSE;
   // reset the search index
   Search_CurrentIndex:=-1;
   Search_CurrentCorner:=nil;
end;

// ------------------------------------------------------------------------
// NAME: AddIntersectionCorner
// ------------------------------------------------------------------------
// DESCRIPTION : Method to add intersection corner if it does not already
//               exist in polygon
//
// PARAMETERS  : aPoint -> Point that should be added if it is an
//                         intersection corner
//
// RETURNVALUE : Index of the Corner if it has been found or inserted
// ------------------------------------------------------------------------
function TPolygonPart.AddIntersectionCorner(aPoint:PCPoint):integer;
var L1P1, L1P2:PCPoint;
    iterate:p_Point_record;
    docheck:boolean;
    isintersection:boolean;
    newitem:p_Point_record;
    retVal:integer;
    curpointidx:integer;
begin
   // first check if Point is inside rectangle of polygon
   retVal:=-1; result:=retVal;
   if not Rectangle.IsInside(aPoint.x, aPoint.y, FSnapRadius) then exit; // Point is not inside rectangle and can not be a intersection corner
   iterate:=FirstCorner; docheck:=TRUE; curpointidx:=0;
   while (iterate <> nil) and (docheck) do
   begin
      L1P1:=iterate^.Point;
      if (iterate^.next = nil) then L1P2:=FirstCorner^.Point // <-- to get combination line between last corner and first corner
      else L1P2:=iterate^.next^.Point;
      // the apoint may not be equal to a corner point of line due to it is already there than
      if (L1P1.x = aPoint.x) and (L1P1.y = aPoint.y) then
      begin
         docheck:=FALSE;
         retVal:=curpointidx;
         iterate^.found:=TRUE;
         HasFoundItems:=TRUE;
      end;
      if (L1P2.x = aPoint.x) and (L1P2.y = aPoint.y) then
      begin
         docheck:=FALSE;
         retVal:=curpointidx+1;
         if iterate^.next = nil then
         begin
            retVal:=0;
            FirstCorner^.Found:=TRUE;
            HasFoundItems:=TRUE;
         end
         else
         begin
            iterate^.next^.Found:=TRUE;
            HasFoundItems:=TRUE;
         end;
      end;
      if (docheck) then
      begin
         // Check if Point is on line
         if IsPointOnLine(L1P1,L1P2,aPoint) then
         begin
            // aPoint has to be inserted between L1P1 and P1P2
            newitem:=Request_p_Point_record;
            newitem^.Point.x:=aPoint.x;
            newitem^.Point.y:=aPoint.y;
            newitem^.addedforprocess:=TRUE;
            newitem^.found:=TRUE;
            newitem^.next:=iterate^.next;
            newitem^.prev:=iterate;
            if (iterate^.next <> nil) then
               iterate^.next^.prev:=newitem
            else
               LastCorner:=newitem; // new item is new last corner
            iterate^.next:=newitem;
            NumCorners:=NumCorners+1;
            HasProcessItems:=TRUE; // Polygon contains process items
            HasFoundItems:=TRUE;   // Polygon has found items
            docheck:=FALSE;        // intersection point already inserted -> nothing to do anymore
            retVal:=curpointidx+1; // return the index inside of the polygon of the new inserted corner

            // Update the rectangle (could be necessary due to Snapradius)
            Rectangle.SetupRect(aPoint.x, aPoint.y);

            // reset the search index
            Search_CurrentIndex:=-1;
            Search_CurrentCorner:=nil;
         end;
      end;
      if docheck then
      begin
         iterate:=iterate^.next; curpointidx:=curpointidx+1;
      end;
   end;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: GetPointWithin
// ------------------------------------------------------------------------
// DESCRIPTION : Method to get point that is within polygon
//
// PARAMETERS  : X -> X-Coordinate of Point (OUT)
//               Y -> Y-Coordinate of Point (OUT)
//
// RETURNVALUE : NONE
// ------------------------------------------------------------------------
procedure TPolygonPart.GetPointWithin(var X:double; var Y:double);
var middlex, middley:double;
    step_x:double;
    step_y:double;
    pointfound:boolean;
    numsteps,i:integer;
begin
   // we start in middle
   middlex:=(Rectangle.Rect_LU.x + Rectangle.Rect_RB.x)/2;
   middley:=(Rectangle.Rect_LU.y + Rectangle.Rect_RB.y)/2;
   pointfound:=false;
   numsteps:=10; // we start with 10 steps from middle
   while (not pointfound) do
   begin
      step_x:=(Rectangle.Rect_RB.x - middlex)/numsteps;
      step_y:=(Rectangle.Rect_LU.y - middley)/numsteps;
      for i:=0 to numsteps-1 do
      begin
         // check right up
         X:=middlex+(step_x*i); Y:=middley+(step_y*i);
         if IsInside(X, Y) then begin pointfound:=true; break; end;
         // check right down
         X:=middlex+(step_x*i); Y:=middley-(step_y*i);
         if IsInside(X, Y) then begin pointfound:=true; break; end;
         // check left up
         X:=middlex-(step_x*i); Y:=middley+(step_y*i);
         if IsInside(X, Y) then begin pointfound:=true; break; end;
         // check left down
         X:=middlex-(step_x*i); Y:=middley-(step_y*i);
         if IsInside(X, Y) then begin pointfound:=true; break; end;
      end;
      if not pointfound then
         numsteps:=numsteps+10; // increase by 10 steps
   end;
end;

// ------------------------------------------------------------------------
// NAME: IsInside
// ------------------------------------------------------------------------
// DESCRIPTION : Method to check if own polygon is inside other polygon
//
// PARAMETERS  : X -> X-Coordinate of point
//               Y -> Y-Coordinate of point
//
// RETURNVALUE : TRUE -> If point is inside Poly, else FALSE
// ------------------------------------------------------------------------
function TPolygonPart.IsInside(other:TPolygonPart):boolean;
var retVal:boolean;
    iterate:p_Point_record;
    docheck:boolean;
    middlex, middley:double;
begin
   retVal:=FALSE;
   result:=retVal;
   // first check if the two polygons are equal
   if IsEqual(other) then
   begin
      if EqualPartIsAlsoInside then // for some methods a part is also inside if it is equal
         result:=TRUE;
      exit; // <-- if the two polygons are equal other could not be inside own
   end;

   // The two polygons are not eqaul -> now check if the rectangle of the other polygon is inside the own
   if Rectangle.GetOverLapType(other.Rectangle) <> Own_Inside then exit;

   // it could be that all corners of own polygon are corners of other polygon
   // in this case the own polygon is eighter equal to the other or is outside, or inside
   docheck:=FALSE;
   iterate:=FirstCorner;
   while (iterate <> nil) and (not docheck) do
   begin
      if other.FindCorner(iterate^.Point) = nil then
         docheck:=TRUE;
      iterate:=iterate^.next;
   end;
   if (not docheck) then
   begin
      // own polygon could eighter be inside or outside of other poly
      // but all corners of own poly exist in other
      // -> we need to find a point that is inside the own polygonpart
      //    only check with the middle of rectangle is not enough
      //    do to this point could be outside of own polygon part
      GetPointWithin(middlex, middley);
      docheck:=other.IsInside(middlex, middley);
      result:=docheck;
      exit;
   end;

   // The own rectangle is inside the other -> now check if all corners of own polygon are inside the other
   iterate:=FirstCorner;
   docheck:=TRUE;
   while (iterate <> nil) and (doCheck) do
   begin
      docheck:=other.IsInside(iterate^.Point.x, iterate^.Point.y);
      iterate:=iterate^.next;
   end;
   if docheck then retVal:=TRUE else retVal:=FALSE;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: IsInside
// ------------------------------------------------------------------------
// DESCRIPTION : Method to check if point is inside Poly
//
// PARAMETERS  : X -> X-Coordinate of point
//               Y -> Y-Coordinate of point
//
// RETURNVALUE : TRUE -> If point is inside Poly, else FALSE
// ------------------------------------------------------------------------
function TPolygonPart.IsInside(X:double; Y:double):boolean;
var retVal:boolean;
    CheckPoint1  :pcpoint;
    CheckPoint2  :pcpoint;
    PointIterate :p_point_record;
    intersections:integer;
    IntersectionPoint1:pcpoint;
    IntersectionPoint2:pcpoint;
    CutXOffset:double;
    reCheck:boolean;
    reCheckCnt:integer;
    LastHadPointOnLine:boolean;
    OldPointOnLineCnt:integer;
    Point:PcPoint;
    OldSnapRadius:double;
begin
   retVal:=FALSE;
   // first check if point is inside of rectangle
   if not Rectangle.IsInside(X,Y) then
   begin
      result:=FALSE;
      exit;
   end;
   Point.X:=x; Point.Y:=Y;
   OldSnapRadius:=FSnapRadius;  // Store current Snap-Radius
   FSnapRadius:=0.5;             // Point inside poly will always been checked with Snap = 0.5m
   CutXOffset:=0; reCheck:=FALSE; reCheckCnt:=0;
   repeat
      retVal:=FALSE; reCheck:=FALSE;
      // the point is inside the rectangle
      CheckPoint1.X:=Point.X; CheckPoint1.Y:=Point.Y;
      CheckPoint2.X:=Point.X+CutXOffset; CheckPoint2.Y:=1000000.00;

      // now cut with all lines of poly
      PointOnLineCnt:=0;
      intersections:=0;
      Pointiterate:=FirstCorner;
      LastHadPointOnLine:=FALSE;
      while (Pointiterate <> nil) and (not reCheck) do
      begin
         IntersectionPoint1:=Pointiterate^.Point;
         if PointIterate^.next <> nil then IntersectionPoint2:=Pointiterate^.next^.Point
         else IntersectionPoint2:=FirstCorner^.Point; // to get connection line between last corner and first corner

         // Check if one of the Intersectionpoints is equal to the CheckPoint1
         if ((InterSectionPoint1.x = CheckPoint1.x) and (InterSectionPoint1.y = CheckPoint1.Y)) or
            ((InterSectionPoint2.x = CheckPoint1.x) and (InterSectionPoint2.y = CheckPoint1.Y)) then
         begin
            // the point is on the CPoly -> no need to check anymore
            retVal:=TRUE;
            result:=retVal;
            PointOnLineCnt:=0;
            FSnapRadius:=OldSnapRadius; // restore current Snap-Radius
            exit;
         end;
         OldPointOnLineCnt:=PointOnLineCnt;
         intersections:=intersections + Cutlines (IntersectionPoint1, IntersectionPoint2, CheckPoint1, CheckPoint2,FALSE);
         if LastHadPointOnLine then
         begin
            if (PointOnLineCnt > OldPointOnLineCnt) then
            begin
               // the Angle of the Cutline has to be changed to avoid
               // problems
               CutXOffset:=CutXOffset+100000;
               ReCheck:=TRUE; reCheckCnt:=reCheckCnt+1;
            end;
            LastHadPointOnLine:=FALSE;
         end;
         if (PointOnLineCnt > OldPointOnLineCnt) then
            LastHadPointOnLine:=TRUE;
         Pointiterate:=Pointiterate^.next;
      end;

      if (PointOnLineCnt = 1) and (intersections mod 2 = 0) then
         intersections:=intersections-1; // due to point is on one line and intersections exist the point is inside
      // check intersection points
      if (intersections mod 2 = 1)and (intersections > 0) then
         retVal:=true
      else
         retVal:=false;
   until (not reCheck) or (reCheckCnt = 5); // maximum 5 rechecks

   if (reCheckCnt = 5) then
      retVal:=FALSE; // algorithm fails -> return FALSE
   PointOnLineCnt:=0;
   FSnapRadius:=OldSnapRadius; // restore current Snap-Radius
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: IsPointOnLine
// ------------------------------------------------------------------------
// DESCRIPTION : Method to check if point is on line
//
// PARAMETERS  : L1P1 - > Start point of line
//               L1P2 - > End point of line
//               Point -> Point that should be checked
//
// RETURNVALUE : TRUE -> If point is on line
// ------------------------------------------------------------------------
function TPolygonPart.IsPointOnLine(L1P1:pcpoint;L1P2:pcpoint;Point:pcpoint):boolean;
var k:double;
    d:double;
    h1:double;
    h2:double;
begin
   // check if it�s possible that point is on line
   if not(((Point.X >= L1P1.X-FSnapRadius) and (Point.X <= L1P2.X+FSnapRadius)) or
          ((Point.X >= L1P2.X-FSnapRadius) and (Point.X <= L1P1.X+FSnapRadius))) then
   begin
      result:=FALSE;
      exit;
   end;

   if not(((Point.Y >= L1P1.Y-FSnapRadius) and (Point.Y <= L1P2.Y+FSnapRadius)) or
          ((Point.Y >= L1P2.Y-FSnapRadius) and (Point.Y <= L1P1.Y+FSnapRadius))) then
   begin
      result:=FALSE;
      exit;
   end;


   // calculate k and d
   h1:=(L1P1.X - L1P2.X);
   if h1 = 0 then
   begin
      if (Point.X = L1P1.X) and
         (((Point.Y >= L1P1.Y) and (Point.Y <= L1P2.Y)) or ((Point.Y >= L1P2.Y) and (Point.Y <= L1P1.Y))) then
         result:=true else result:=false;
      exit;
   end;
   h2:=(L1P1.Y - L1P2.Y);
   if h2 = 0 then
   begin
      if not (((Point.X >= L1P1.X) and (Point.X <= L1P2.X)) or
         ((Point.X <= L1P1.X) and (Point.X >= L1P2.X))) then
      begin
         result:=FALSE;
         exit;
      end;
   end;

   k:=(L1P1.Y-L1P2.Y) / h1;
   d:=L1P1.Y - (k * L1P1.X);

   h2:=((k * Point.X) + d);
   // a tolerance of 0.1 meter is allowed
   if FSnapRadius > 0 then
   begin
      if (h2 >= Point.Y - FSnapRadius) and (h2 <= Point.Y + FSnapRadius) then result:=true else result:=false;
   end
   else
   begin
      if (h2 >= Point.Y) then result:=true else result:=false;
   end;
end;

// ------------------------------------------------------------------------
// NAME: CutLines
// ------------------------------------------------------------------------
// DESCRIPTION : Method to calculate intersections points between two lines
//
// PARAMETERS  : L1P1       -> Start point of first line
//               L1P2       -> End point of first line
//               L2P1       -> Start point of second line
//               L2P2       -> End point of second line
//               MayOverlap -> if TRUE -> Overlap of the two lines is cut point
//                             if FALSE -> Overlap is not a cut point
// RETURNVALUE : Number of intersection points
// ------------------------------------------------------------------------
Function TPolygonPart.CutLines(
                         L1P1         : PCPoint;
                         L1P2         : PCPoint;
                         L2P1         : PCPoint;
                         L2P2         : PCPoint;
                         MayOverlap   : boolean
                       ):integer;
var D            : Double;
    DX1          : Double;
    DY1          : Double;
    DX2          : Double;
    DY2          : Double;
    t0           : Double;
    u0           : Double;
    tc           : Double;
    td           : Double;
    PointsOnLine : integer;
begin
    CutLines:=0;
    PointsOnLine:=0;
    DX1:=L1P2.X-L1P1.X; if dx1 = 0 then dx1:=0.000001;
    DX2:=L2P2.X-L2P1.X; if dx2 = 0 then dx2:=0.000001;
    DY1:=L1P2.Y-L1P1.Y; if dy1 = 0 then dy1:=0.000001;
    DY2:=L2P2.Y-L2P1.Y; if dy2 = 0 then dy2:=0.000001;
    D:=DX1*DY2-DY1*DX2;
    if Abs(D)>=Tolerance then begin
      t0:=((L2P1.X-L1P1.X)*DY2-(L2P1.Y-L1P1.Y)*DX2)/D;
      if (t0>-Tolerance) and (t0<1.0+Tolerance) then begin
        if Abs(DX2)>Abs(DY2) then u0:=((L1P1.X-L2P1.X)+DX1*t0)/DX2
        else u0:=((L1P1.Y-L2P1.Y)+DY1*t0)/DY2;
        if (u0>-Tolerance) and (u0<1.0+Tolerance) then begin
           CutLines:=1; PointsOnLine:=1;
        end;
      end;
    end
    else if (Abs(DX1) < Tolerance) and (Abs(DY1) < Tolerance) then begin
      PointsOnLine:=0;
      CutLines:=0;
    end
    else if Abs(DX1*(L2P1.Y-L1P1.Y)-DY1*(L2P1.X-L1P1.X))<Tolerance then begin
      if Abs(DX1)>Abs(DY1) then begin
        tc:=(L2P1.X-L1P1.X)/DX1;
        td:=(L2P2.X-L1P1.X)/DX1;
      end
      else begin
           tc:=(L2P1.Y-L1P1.Y)/DY1;
           td:=(L2P2.Y-L1P1.Y)/DY1;
      end;
      if ((tc>-Tolerance) and (td<1.0+Tolerance))
          or ((tc<1.0+Tolerance) and (td>-Tolerance)) then begin
        if tc<0.0 then tc:=0.0
        else if tc>1.0 then tc:=1.0;
        if td<0.0 then td:=0.0
        else if td>1.0 then td:=1.0;

        if Abs(tc-td)<Tolerance then begin CutLines:=1; PointsOnLine:=1; end
        else begin
          PointsOnLine:=2;
          CutLines:=2;
        end;
      end;
    end;

    if ((PointsOnLine = 1) and (L2P1.Y >= L1P1.Y) and (L2P1.Y >= L1P2.Y)) then
       CutLines:=0;

    if (MayOverlap = false) and (PointsOnLine = 2) then Cutlines:=0;
    if (MayOverlap = false) and (PointsOnLine = 1) then
    begin {Kontrolle, ob der Schnittpunkt nicht auf der Linie liegt }
       if PointsOnLine <> 0 then if IsPointOnLine(L1P1,L1P2,L2P1) then begin PointOnLineCnt:=PointOnLineCnt+1; end;
       if PointsOnLine <> 0 then if IsPointOnLine(L1P1,L1P2,L2P2) then begin PointOnLineCnt:=PointOnLineCnt+1; end;
       if PointsOnLine <> 0 then if IsPointOnLine(L2P1,L2P2,L1P1) then begin PointOnLineCnt:=PointOnLineCnt+1; end;
       if PointsOnLine <> 0 then if IsPointOnLine(L2P1,L2P2,L1P2) then begin PointOnLineCnt:=PointOnLineCnt+1; end;
       Cutlines:=PointsOnLine;
    end;

    if (MayOverlap = false) and (L1P1.X = L2P1.X) and (L1P1.Y = L2P1.Y) then Cutlines:=0;
    if (MayOverlap = false) and (L1P1.X = L2P2.X) and (L1P1.Y = L2P2.Y) then Cutlines:=0;
    if (MayOverlap = false) and (L1P2.X = L2P1.X) and (L1P2.Y = L2P1.Y) then Cutlines:=0;
    if (MayOverlap = false) and (L1P2.X = L2P2.X) and (L1P2.Y = L2P2.Y) then Cutlines:=0;
end;

procedure TPolygonPart.WriteDebugInfo(var aFile:TextFile);
var iterate:p_Point_record;
    PolyTypeStr:string;
    corneridx:integer;
    PostFix:string;
begin
   if PolyType = Main   then PolyTypeStr:='MAIN';
   if PolyType = Island then PolyTypeStr:='ISLAND';
   if PolyType = OuterIsland then PolyTypeStr:='OUTERISLAND';
   writeln(aFile,'   --------------------------------------');
   writeln(aFile,'   POLYGONPART');
   writeln(aFile,'   Corners :'+inttostr(NumCorners));
   writeln(aFile,'   Type    :'+PolyTypeStr);
   iterate:=FirstCorner; corneridx:=0;
   while iterate <> nil do
   begin
      PostFix:=' PA:';
      if iterate^.addedforprocess then PostFix:=PostFix+'T' else PostFix:=PostFix+'F';
      PostFix:=PostFix+' F:';
      if iterate^.found then PostFix:=PostFix+'T' else PostFix:=PostFix+'F';
      writeln(aFile,'   IDX:'+inttostr(corneridx)+' X:'+floattostr(iterate^.Point.X) + ' Y:'+floattostr(iterate^.Point.Y) + PostFix);
      iterate:=iterate^.next; corneridx:=corneridx+1;
   end;
   writeln(aFile,'   --------------------------------------');
end;




end.
