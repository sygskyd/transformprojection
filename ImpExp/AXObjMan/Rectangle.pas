unit Rectangle;

interface

uses Obj_Def;

type
  TRectangle = class // class to store rectange object
  public
     Rect_LU   :pcpoint;        // left upper corner of rectangle
     Rect_RB   :pcpoint;        // right bottom corner of rectangle

     constructor Create;                                 // constructor of class
     destructor Destroy;                                 // destructor of class
     procedure SetupRect(X:double; Y:double); overload;  // method to setup rectangle with new corner
     procedure SetupRect(other:TRectangle); overload;    // method to setup rectange with other rectangle
     function  GetOverLapType(other:TRectangle):integer;overload; // method to check if other rectangle overlaps own
     function  GetOverLapType(other:TRectangle; Snap:double):integer;overload; // method to check if other rectangle overlaps own (with Snap)
     function  IsInside(X:double; Y:double):boolean;overload;               // method to check if point is inside rectangle
     function  IsInside(X:double; Y:double; Snap:double):boolean; overload; // method to check if point is inside rectangle by using Snap
     function  IsEqual(other:TRectangle):boolean;        // method to check if two rectangles are equal
  private
     isset     :boolean;          // Flag that is used to set if rectange is already set
  end;
implementation

constructor TRectangle.Create;
begin
   isset:=FALSE;
end;

destructor TRectangle.Destroy;
begin
   // nothing to do
end;

procedure TRectangle.SetupRect(X:double; Y:double);
begin
   if not isset then
   begin
      Rect_LU.X:=x;
      Rect_LU.Y:=y;
      Rect_RB.X:=x;
      Rect_RB.Y:=Y;
   end
   else
   begin
      if X < Rect_LU.x then Rect_LU.x:=x;
      if X > Rect_RB.x then Rect_RB.x:=x;
      if Y < Rect_RB.y then Rect_RB.Y:=Y;
      if Y > Rect_LU.Y then Rect_LU.Y:=Y;
   end;
   isset:=TRUE;
end;

procedure TRectangle.SetupRect(other:TRectangle);
begin
   if not other.isset then exit;

   if not isset then
   begin
      Rect_LU.x:=other.Rect_LU.x;
      Rect_LU.y:=other.Rect_LU.y;
      Rect_RB.x:=other.Rect_RB.x;
      Rect_RB.y:=other.Rect_RB.y;
   end
   else
   begin
      if other.Rect_LU.x < Rect_LU.x then Rect_LU.x:=other.Rect_LU.x;
      if other.Rect_LU.y > Rect_LU.y then Rect_LU.y:=other.Rect_LU.y;
      if other.Rect_RB.x > Rect_RB.x then Rect_RB.x:=other.Rect_RB.x;
      if other.Rect_RB.y < Rect_RB.y then Rect_RB.y:=other.Rect_RB.y;
   end;
   isset:=TRUE;
end;

// ------------------------------------------------------------------------
// NAME: GetOverLapType
// ------------------------------------------------------------------------
// DESCRIPTION : Method to check if two rects do overlap (with Snap)
//
// PARAMETERS  : other         -> Other rectangle object
//               Snap          -> Snap-Value
//
// RETURNVALUE : Overlap type
// ------------------------------------------------------------------------
function TRectangle.GetOverLapType(other:TRectangle; Snap:double):integer;
var Point     :pcpoint;
    intersect1:integer;
    intersect2:integer;
    retVal    :integer;
begin
   retVal:=No_Overlap;
   result:=retVal;
   if (not isset) or (not other.isset) then exit;

   intersect1:=0; intersect2:=0; // for counting the overlaps

   // check if point is inside area
   if (Rect_LU.X >= other.Rect_LU.X-Snap) and (Rect_LU.X <= other.Rect_RB.X+Snap) and
      (Rect_RB.Y >= other.Rect_RB.Y-Snap) and (Rect_RB.Y <= other.Rect_LU.Y+Snap) then intersect1:=intersect1+1;

   // check if point is inside area
   if (Rect_RB.X >= other.Rect_LU.x-Snap) and (Rect_RB.X <= other.Rect_RB.X+Snap) and
      (Rect_RB.Y >= other.Rect_RB.Y-Snap) and (Rect_RB.Y <= other.Rect_LU.Y+Snap) then intersect1:=intersect1+1;

   // check if point is inside area
   if (Rect_RB.X >= other.Rect_LU.X-Snap) and (Rect_RB.X <= other.Rect_RB.X+Snap) and
      (Rect_LU.Y >= other.Rect_RB.Y-Snap) and (Rect_LU.Y <= other.Rect_LU.Y+Snap) then intersect1:=intersect1+1;

   // check if point is inside area
   if (Rect_LU.X >= other.Rect_LU.X-Snap) and (Rect_LU.X <= other.Rect_RB.X+Snap) and
      (Rect_LU.Y >= other.Rect_RB.Y-Snap) and (Rect_LU.Y <= other.Rect_LU.Y+Snap) then intersect1:=intersect1+1;

   // check if point is inside area
   if (other.Rect_LU.X >= Rect_LU.X-Snap) and (other.Rect_LU.X <= Rect_RB.X+Snap) and
      (other.Rect_RB.Y >= Rect_RB.Y-Snap) and (other.Rect_RB.Y <= Rect_LU.Y+Snap) then intersect2:=intersect2+1;

   // check if point is inside area
   if (other.Rect_RB.X >= Rect_LU.X-Snap) and (other.Rect_RB.X <= Rect_RB.X+Snap) and
      (other.Rect_Rb.Y >= Rect_RB.Y-Snap) and (other.Rect_Rb.Y <= Rect_LU.Y+Snap) then intersect2:=intersect2+1;

   // check if point is inside area
   if (other.Rect_RB.X >= Rect_LU.X-Snap) and (other.Rect_RB.X <= Rect_RB.X+Snap) and
      (other.Rect_LU.Y >= Rect_RB.Y-Snap) and (other.Rect_LU.Y <= Rect_LU.Y+Snap) then intersect2:=intersect2+1;

   // check if point is inside area
   if (other.Rect_LU.X >= Rect_LU.X-Snap) and (other.Rect_LU.X <= Rect_RB.X+Snap) and
      (other.Rect_LU.Y >= Rect_RB.Y-Snap) and (other.Rect_LU.Y <= Rect_LU.Y+Snap) then intersect2:=intersect2+1;

   if (intersect1 = 4 ) then retVal:=Own_Inside;
   if (intersect2 = 4 ) then retVal:=Other_Inside;
   if (retVal = No_Overlap) and ((intersect1 > 0) or (intersect2 > 0)) then retVal:=Do_Overlap;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: GetOverLapType
// ------------------------------------------------------------------------
// DESCRIPTION : Method to check if two rects do overlap
//
// PARAMETERS  : other         -> Other rectangle object
//
// RETURNVALUE : Overlap type
// ------------------------------------------------------------------------
function TRectangle.GetOverLapType(other:TRectangle):integer;
var Point     :pcpoint;
    intersect1:integer;
    intersect2:integer;
    retVal    :integer;
begin
   retVal:=No_Overlap;
   result:=retVal;
   if (not isset) or (not other.isset) then exit;

   intersect1:=0; intersect2:=0; // for counting the overlaps

   // check if point is inside area
   if (Rect_LU.X >= other.Rect_LU.X) and (Rect_LU.X <= other.Rect_RB.X) and
      (Rect_RB.Y >= other.Rect_RB.Y) and (Rect_RB.Y <= other.Rect_LU.Y) then intersect1:=intersect1+1;

   // check if point is inside area
   if (Rect_RB.X >= other.Rect_LU.x) and (Rect_RB.X <= other.Rect_RB.X) and
      (Rect_RB.Y >= other.Rect_RB.Y) and (Rect_RB.Y <= other.Rect_LU.Y) then intersect1:=intersect1+1;

   // check if point is inside area
   if (Rect_RB.X >= other.Rect_LU.X) and (Rect_RB.X <= other.Rect_RB.X) and
      (Rect_LU.Y >= other.Rect_RB.Y) and (Rect_LU.Y <= other.Rect_LU.Y) then intersect1:=intersect1+1;

   // check if point is inside area
   if (Rect_LU.X >= other.Rect_LU.X) and (Rect_LU.X <= other.Rect_RB.X) and
      (Rect_LU.Y >= other.Rect_RB.Y) and (Rect_LU.Y <= other.Rect_LU.Y) then intersect1:=intersect1+1;

   // check if point is inside area
   if (other.Rect_LU.X >= Rect_LU.X) and (other.Rect_LU.X <= Rect_RB.X) and
      (other.Rect_RB.Y >= Rect_RB.Y) and (other.Rect_RB.Y <= Rect_LU.Y) then intersect2:=intersect2+1;

   // check if point is inside area
   if (other.Rect_RB.X >= Rect_LU.X) and (other.Rect_RB.X <= Rect_RB.X) and
      (other.Rect_Rb.Y >= Rect_RB.Y) and (other.Rect_Rb.Y <= Rect_LU.Y) then intersect2:=intersect2+1;

   // check if point is inside area
   if (other.Rect_RB.X >= Rect_LU.X) and (other.Rect_RB.X <= Rect_RB.X) and
      (other.Rect_LU.Y >= Rect_RB.Y) and (other.Rect_LU.Y <= Rect_LU.Y) then intersect2:=intersect2+1;

   // check if point is inside area
   if (other.Rect_LU.X >= Rect_LU.X) and (other.Rect_LU.X <= Rect_RB.X) and
      (other.Rect_LU.Y >= Rect_RB.Y) and (other.Rect_LU.Y <= Rect_LU.Y) then intersect2:=intersect2+1;

   if (intersect1 = 4 ) then retVal:=Own_Inside;
   if (intersect2 = 4 ) then retVal:=Other_Inside;
   if (intersect1 = 4) and (intersect2 = 4) then retVal:=Own_Inside; // if both rectangles are equal -> return Own-Inside
   if (retVal = No_Overlap) and ((intersect1 > 0) or (intersect2 > 0)) then retVal:=Do_Overlap;
   result:=retVal;
end;

function TRectangle.IsInside(X:double; Y:double):boolean;
var retVal:boolean;
begin
   retVal:=FALSE;
   result:=retVal;
   if not isset then exit;

   if (X >= Rect_LU.X) and (X <= Rect_RB.X) and
      (Y >= Rect_RB.Y) and (Y <= Rect_LU.Y) then retVal:=TRUE;

   result:=retVal;
end;

function TRectangle.IsInside(X:double; Y:double; Snap:double):boolean;
var retVal:boolean;
begin
   retVal:=FALSE;
   result:=retVal;
   if not isset then exit;
   if (X >= Rect_LU.X-Snap) and (X <= Rect_RB.X+Snap) and
      (Y >= Rect_RB.Y-Snap) and (Y <= Rect_LU.Y+Snap) then retVal:=TRUE;
   result:=retVal;
end;


function TRectangle.IsEqual(other:TRectangle):boolean;
var retVal:boolean;
begin
   retVal:=FALSE;
   result:=retVal;
   if (not isset) or (not other.isset) then exit;
   if (other.Rect_LU.x = Rect_LU.x) and
      (other.Rect_LU.y = Rect_LU.y) and
      (other.Rect_RB.x = Rect_RB.x) and
      (other.Rect_RB.y = Rect_RB.y) then retVal:=TRUE;
   result:=retVal;
end;

end.
