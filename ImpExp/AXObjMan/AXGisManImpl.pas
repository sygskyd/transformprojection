unit AXGisManImpl;

interface

uses
  Windows, ActiveX, Classes, Controls, Graphics, Menus, Forms, StdCtrls,
  ComServ, StdVCL, AXCtrls, AXGisMan_TLB, ExtCtrls, StrFile, PolygonList, AXDLL_TLB;

type
  TAXGisObjMan = class(TActiveXControl, IAXGisObjMan)
  private
    { Private declarations }
    FDelphiControl: TPanel;
    FEvents: IAXGisObjManEvents;

    // private variables
    StrFileObj     :ClassStrFile;          // String-File object
    App            :OleVariant;            // Pointer to current DX-object
    Document       :IDocument;             // Pointer to current document
    CurrPercentInfo:integer;               // string that contains current percent-info

    // private variables for properties
    FLngCode     :string;               // current language-code

    // private methods
    procedure ReadCPolysFromLayer(SourceLayer:string; var aList:TPolygonList);         // method to read CPolys from a layer
    procedure ReadCPolysFromSelected(var aList:TPolygonList;CheckForOtherRef:boolean); // method to get all CPolys from the selected items
    procedure ShowPercent(MaxElems:integer; CurrElems:integer);
    function  CheckLayerExists(Layername:string):boolean;
    procedure CopyCPoly(var OldObject:IBase; var NewObject:IBase);
    procedure CopyObjectStyle(var oldObject:OleVariant; var newObject:OleVariant);
    procedure SplitObjectToLayer(var aBase:IBase; var aTargetLayer:ILayer; const SplitMask:integer);
    procedure SplitCPolyToLayer(var aBase:IBase; var aTargetLayer:ILayer; const SplitMask:integer);

    procedure CanResizeEvent(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure ClickEvent(Sender: TObject);
    procedure ConstrainedResizeEvent(Sender: TObject; var MinWidth, MinHeight,
      MaxWidth, MaxHeight: Integer);
    procedure DblClickEvent(Sender: TObject);
    procedure ResizeEvent(Sender: TObject);
  protected
    { Protected declarations }
    procedure DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage); override;
    procedure EventSinkChanged(const EventSink: IUnknown); override;
    procedure InitializeControl; override;
    function DrawTextBiDiModeFlagsReadingOnly: Integer; safecall;
    function Get_Alignment: TxAlignment; safecall;
    function Get_AutoSize: WordBool; safecall;
    function Get_BevelInner: TxBevelCut; safecall;
    function Get_BevelOuter: TxBevelCut; safecall;
    function Get_BorderStyle: TxBorderStyle; safecall;
    function Get_Caption: WideString; safecall;
    function Get_Color: OLE_COLOR; safecall;
    function Get_Ctl3D: WordBool; safecall;
    function Get_Cursor: Smallint; safecall;
    function Get_DockSite: WordBool; safecall;
    function Get_DoubleBuffered: WordBool; safecall;
    function Get_DragCursor: Smallint; safecall;
    function Get_DragMode: TxDragMode; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Font: IFontDisp; safecall;
    function Get_FullRepaint: WordBool; safecall;
    function Get_Locked: WordBool; safecall;
    function Get_ParentColor: WordBool; safecall;
    function Get_ParentCtl3D: WordBool; safecall;
    function Get_UseDockManager: WordBool; safecall;
    function Get_Visible: WordBool; safecall;
    function Get_VisibleDockClientCount: Integer; safecall;
    function IsRightToLeft: WordBool; safecall;
    function UseRightToLeftReading: WordBool; safecall;
    function UseRightToLeftScrollBar: WordBool; safecall;
    procedure _Set_Font(const Value: IFontDisp); safecall;
    procedure InitiateAction; safecall;
    procedure Set_Alignment(Value: TxAlignment); safecall;
    procedure Set_AutoSize(Value: WordBool); safecall;
    procedure Set_BevelInner(Value: TxBevelCut); safecall;
    procedure Set_BevelOuter(Value: TxBevelCut); safecall;
    procedure Set_BorderStyle(Value: TxBorderStyle); safecall;
    procedure Set_Caption(const Value: WideString); safecall;
    procedure Set_Color(Value: OLE_COLOR); safecall;
    procedure Set_Ctl3D(Value: WordBool); safecall;
    procedure Set_Cursor(Value: Smallint); safecall;
    procedure Set_DockSite(Value: WordBool); safecall;
    procedure Set_DoubleBuffered(Value: WordBool); safecall;
    procedure Set_DragCursor(Value: Smallint); safecall;
    procedure Set_DragMode(Value: TxDragMode); safecall;
    procedure Set_Enabled(Value: WordBool); safecall;
    procedure Set_Font(var Value: IFontDisp); safecall;
    procedure Set_FullRepaint(Value: WordBool); safecall;
    procedure Set_Locked(Value: WordBool); safecall;
    procedure Set_ParentColor(Value: WordBool); safecall;
    procedure Set_ParentCtl3D(Value: WordBool); safecall;
    procedure Set_UseDockManager(Value: WordBool); safecall;
    procedure Set_Visible(Value: WordBool); safecall;
    function Get_WorkingDir: WideString; safecall;
    procedure Set_WorkingDir(const Value: WideString); safecall;
    function Get_LngCode: WideString; safecall;
    procedure Set_LngCode(const Value: WideString); safecall;
    function Get_SnapRadius: Double; safecall;
    procedure Set_SnapRadius(Value: Double); safecall;
    procedure SetDocument(var aDocument: OleVariant); safecall;
    procedure SetApp(var aApp: OleVariant); safecall;
    procedure CreateIslandLayer(const SourceLayer, DestLayer: WideString);  safecall;
    function CombineCPolysOnLayer(const SourceLayer, TargetLayer: WideString; SourceIsSelected: WordBool): Integer; safecall;
    function CopyObjectToLayer(var Obj: OleVariant; const TargetLayer: WideString): Integer; safecall;
    function CheckIdHasDuplicateOnLayer(Id: Integer; const Layer: WideString): Integer; safecall;
    function CheckIsDuplicate(var Ori, Old: OleVariant): WordBool; safecall;
    function CheckPolyInsidePoly(var Poly1, Poly2: OleVariant): WordBool; safecall;
    function CheckPointInsidePoly(X, Y: Double; var Poly: OleVariant; NoDkmCheck: WordBool): WordBool; safecall;
    procedure CopyNonDuplicatesToLayer(const Layer1, Layer2, TargetLayer: WideString); safecall;
    function Get_DoDebug: WordBool; safecall;
    procedure Set_DoDebug(Value: WordBool); safecall;
    function GetIdOfSurroundingCPolyOnLayer(Id: Integer;
      const Layer: WideString): Integer; safecall;
    procedure SplitObjectsToLayer(const SourceLayers, TargetLayer: WideString;
      Splitmask: Integer; SourceIsSelected: WordBool); safecall;  procedure IAXGisObjMan._Set_Font = IAXGisObjMan__Set_Font;
    procedure IAXGisObjMan.Set_Font = IAXGisObjMan_Set_Font;
  
    procedure IAXGisObjMan__Set_Font(var Value: IFontDisp); safecall;
    procedure IAXGisObjMan_Set_Font(const Value: IFontDisp); safecall;
  end;

implementation

uses ComObj, Obj_Def, CheckDuplicate, Polygon, PolygonPart, ObjectFifoBuffers, SysUtils,Dialogs;

{ TAXGisObjMan }

procedure TAXGisObjMan.DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage);
begin
  {TODO: Define property pages here.  Property pages are defined by calling
    DefinePropertyPage with the class id of the page.  For example,
      DefinePropertyPage(Class_AXGisObjManPage); }
end;

procedure TAXGisObjMan.EventSinkChanged(const EventSink: IUnknown);
begin
  FEvents := EventSink as IAXGisObjManEvents;
end;

procedure TAXGisObjMan.InitializeControl;
begin
  FDelphiControl := Control as TPanel;
  FDelphiControl.OnCanResize := CanResizeEvent;
  FDelphiControl.OnClick := ClickEvent;
  FDelphiControl.OnConstrainedResize := ConstrainedResizeEvent;
  FDelphiControl.OnDblClick := DblClickEvent;
  FDelphiControl.OnResize := ResizeEvent;
  FDelphiControl.Height:=25;
  FDelphiControl.Width:=50;
  FDelphiControl.Caption:='GisMan';
  CurrPercentInfo:=0;
  FSnapRadius:=0.01;
  FDoDebug:=FALSE;
  EqualPartIsAlsoInside:=FALSE;
end;

function TAXGisObjMan.DrawTextBiDiModeFlagsReadingOnly: Integer;
begin
  Result := FDelphiControl.DrawTextBiDiModeFlagsReadingOnly;
end;

function TAXGisObjMan.Get_Alignment: TxAlignment;
begin
  Result := Ord(FDelphiControl.Alignment);
end;

function TAXGisObjMan.Get_AutoSize: WordBool;
begin
  Result := FDelphiControl.AutoSize;
end;

function TAXGisObjMan.Get_BevelInner: TxBevelCut;
begin
  Result := Ord(FDelphiControl.BevelInner);
end;

function TAXGisObjMan.Get_BevelOuter: TxBevelCut;
begin
  Result := Ord(FDelphiControl.BevelOuter);
end;

function TAXGisObjMan.Get_BorderStyle: TxBorderStyle;
begin
  Result := Ord(FDelphiControl.BorderStyle);
end;

function TAXGisObjMan.Get_Caption: WideString;
begin
  Result := WideString(FDelphiControl.Caption);
end;

function TAXGisObjMan.Get_Color: OLE_COLOR;
begin
  Result := OLE_COLOR(FDelphiControl.Color);
end;

function TAXGisObjMan.Get_Ctl3D: WordBool;
begin
  Result := FDelphiControl.Ctl3D;
end;

function TAXGisObjMan.Get_Cursor: Smallint;
begin
  Result := Smallint(FDelphiControl.Cursor);
end;

function TAXGisObjMan.Get_DockSite: WordBool;
begin
  Result := FDelphiControl.DockSite;
end;

function TAXGisObjMan.Get_DoubleBuffered: WordBool;
begin
  Result := FDelphiControl.DoubleBuffered;
end;

function TAXGisObjMan.Get_DragCursor: Smallint;
begin
  Result := Smallint(FDelphiControl.DragCursor);
end;

function TAXGisObjMan.Get_DragMode: TxDragMode;
begin
  Result := Ord(FDelphiControl.DragMode);
end;

function TAXGisObjMan.Get_Enabled: WordBool;
begin
  Result := FDelphiControl.Enabled;
end;

function TAXGisObjMan.Get_Font: IFontDisp;
begin
  GetOleFont(FDelphiControl.Font, Result);
end;

function TAXGisObjMan.Get_FullRepaint: WordBool;
begin
  Result := FDelphiControl.FullRepaint;
end;

function TAXGisObjMan.Get_Locked: WordBool;
begin
  Result := FDelphiControl.Locked;
end;

function TAXGisObjMan.Get_ParentColor: WordBool;
begin
  Result := FDelphiControl.ParentColor;
end;

function TAXGisObjMan.Get_ParentCtl3D: WordBool;
begin
  Result := FDelphiControl.ParentCtl3D;
end;

function TAXGisObjMan.Get_UseDockManager: WordBool;
begin
  Result := FDelphiControl.UseDockManager;
end;

function TAXGisObjMan.Get_Visible: WordBool;
begin
  Result := FDelphiControl.Visible;
end;

function TAXGisObjMan.Get_VisibleDockClientCount: Integer;
begin
  Result := FDelphiControl.VisibleDockClientCount;
end;

function TAXGisObjMan.IsRightToLeft: WordBool;
begin
  Result := FDelphiControl.IsRightToLeft;
end;

function TAXGisObjMan.UseRightToLeftReading: WordBool;
begin
  Result := FDelphiControl.UseRightToLeftReading;
end;

function TAXGisObjMan.UseRightToLeftScrollBar: WordBool;
begin
  Result := FDelphiControl.UseRightToLeftScrollBar;
end;

procedure TAXGisObjMan._Set_Font(const Value: IFontDisp);
begin
  SetOleFont(FDelphiControl.Font, Value);
end;

procedure TAXGisObjMan.CanResizeEvent(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
var
  TempNewWidth: Integer;
  TempNewHeight: Integer;
  TempResize: WordBool;
begin
  TempNewWidth := Integer(NewWidth);
  TempNewHeight := Integer(NewHeight);
  TempResize := WordBool(Resize);
  if FEvents <> nil then FEvents.OnCanResize(TempNewWidth, TempNewHeight, TempResize);
  NewWidth := Integer(TempNewWidth);
  NewHeight := Integer(TempNewHeight);
  Resize := Boolean(TempResize);
end;

procedure TAXGisObjMan.ClickEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnClick;
end;

procedure TAXGisObjMan.ConstrainedResizeEvent(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
var
  TempMinWidth: Integer;
  TempMinHeight: Integer;
  TempMaxWidth: Integer;
  TempMaxHeight: Integer;
begin
  TempMinWidth := Integer(MinWidth);
  TempMinHeight := Integer(MinHeight);
  TempMaxWidth := Integer(MaxWidth);
  TempMaxHeight := Integer(MaxHeight);
  if FEvents <> nil then FEvents.OnConstrainedResize(TempMinWidth, TempMinHeight, TempMaxWidth, TempMaxHeight);
  MinWidth := Integer(TempMinWidth);
  MinHeight := Integer(TempMinHeight);
  MaxWidth := Integer(TempMaxWidth);
  MaxHeight := Integer(TempMaxHeight);
end;

procedure TAXGisObjMan.DblClickEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnDblClick;
end;

procedure TAXGisObjMan.InitiateAction;
begin
  FDelphiControl.InitiateAction;
end;

procedure TAXGisObjMan.ResizeEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnResize;
end;

procedure TAXGisObjMan.Set_Alignment(Value: TxAlignment);
begin
  FDelphiControl.Alignment := TAlignment(Value);
end;

procedure TAXGisObjMan.Set_AutoSize(Value: WordBool);
begin
  FDelphiControl.AutoSize := Value;
end;

procedure TAXGisObjMan.Set_BevelInner(Value: TxBevelCut);
begin
  FDelphiControl.BevelInner := TBevelCut(Value);
end;

procedure TAXGisObjMan.Set_BevelOuter(Value: TxBevelCut);
begin
  FDelphiControl.BevelOuter := TBevelCut(Value);
end;

procedure TAXGisObjMan.Set_BorderStyle(Value: TxBorderStyle);
begin
  FDelphiControl.BorderStyle := TBorderStyle(Value);
end;

procedure TAXGisObjMan.Set_Caption(const Value: WideString);
begin
  FDelphiControl.Caption := TCaption(Value);
end;

procedure TAXGisObjMan.Set_Color(Value: OLE_COLOR);
begin
  FDelphiControl.Color := TColor(Value);
end;

procedure TAXGisObjMan.Set_Ctl3D(Value: WordBool);
begin
  FDelphiControl.Ctl3D := Value;
end;

procedure TAXGisObjMan.Set_Cursor(Value: Smallint);
begin
  FDelphiControl.Cursor := TCursor(Value);
end;

procedure TAXGisObjMan.Set_DockSite(Value: WordBool);
begin
  FDelphiControl.DockSite := Value;
end;

procedure TAXGisObjMan.Set_DoubleBuffered(Value: WordBool);
begin
  FDelphiControl.DoubleBuffered := Value;
end;

procedure TAXGisObjMan.Set_DragCursor(Value: Smallint);
begin
  FDelphiControl.DragCursor := TCursor(Value);
end;

procedure TAXGisObjMan.Set_DragMode(Value: TxDragMode);
begin
  FDelphiControl.DragMode := TDragMode(Value);
end;

procedure TAXGisObjMan.Set_Enabled(Value: WordBool);
begin
  FDelphiControl.Enabled := Value;
end;

procedure TAXGisObjMan.Set_Font(var Value: IFontDisp);
begin
  SetOleFont(FDelphiControl.Font, Value);
end;

procedure TAXGisObjMan.Set_FullRepaint(Value: WordBool);
begin
  FDelphiControl.FullRepaint := Value;
end;

procedure TAXGisObjMan.Set_Locked(Value: WordBool);
begin
  FDelphiControl.Locked := Value;
end;

procedure TAXGisObjMan.Set_ParentColor(Value: WordBool);
begin
  FDelphiControl.ParentColor := Value;
end;

procedure TAXGisObjMan.Set_ParentCtl3D(Value: WordBool);
begin
  FDelphiControl.ParentCtl3D := Value;
end;

procedure TAXGisObjMan.Set_UseDockManager(Value: WordBool);
begin
  FDelphiControl.UseDockManager := Value;
end;

procedure TAXGisObjMan.Set_Visible(Value: WordBool);
begin
  FDelphiControl.Visible := Value;
end;

function TAXGisObjMan.Get_WorkingDir: WideString;
begin
   result:=FWorkingDir;
end;

procedure TAXGisObjMan.Set_WorkingDir(const Value: WideString);
begin
   FWorkingDir:=Value;
end;

function TAXGisObjMan.Get_LngCode: WideString;
begin
   result:=FLngCode;
end;

procedure TAXGisObjMan.Set_LngCode(const Value: WideString);
begin
   FLngCode:=Value;
end;

function TAXGisObjMan.Get_SnapRadius: Double;
begin
   result:=FSnapRadius;
end;

procedure TAXGisObjMan.Set_SnapRadius(Value: Double);
begin
   FSnapRadius:=Value;
end;

// ------------------------------------------------------------------------
// NAME: ReadCPolysFromLayer
// ------------------------------------------------------------------------
// DESCRIPTION : Method to read all Polygons from a Layer
//
// PARAMETERS  : SourceLayer -> name of the layer that should be read
//               aList       -> List to that the Polygons should be added
//
// RETURNVALUE : NONE
// ------------------------------------------------------------------------
procedure TAXGisObjMan.ReadCPolysFromLayer(SourceLayer:string;var aList:TPolygonList);
var aLayer :ILayer;
    aObject:IBase;
    aCPoly :IPolygon;
    layercnt:integer;
    cnt:integer;
    aLayername:string;
    MaxElems:integer;
    CurrElems:integer;
    aPolygon:TPolygon;
begin
   for layercnt:=0 to Document.Layers.Count-1 do
   begin
      aLayer:=Document.Layers.Items[layercnt];
      aLayername:=aLayer.Layername;
      if aLayername = SourceLayer then
      begin
         MaxElems:=aLayer.count-1;
         CurrElems:=0;
         // now the objects from the layer will be read
         for cnt:=0 to aLayer.count-1 do
         begin
            aObject:=ALayer.Items[cnt];
            if (aObject.ObjectType = ot_Cpoly) then // read polygon
            begin
               aCPoly:=aObject as IPolygon;
               aPolygon:=TPolygon.Create; // create new Polygon object
               aPolygon.Read(aCPoly);     // get the data
               aList.Add(aPolygon);       // add it to list
            end;
            CurrElems:=CurrElems+1;
            ShowPercent(MaxElems, CurrElems);
         end;
      end;
   end;
end;

// ------------------------------------------------------------------------
// NAME: ReadCPolysFromSelected
// ------------------------------------------------------------------------
// DESCRIPTION : Method to read all CPolys from the selected objects
//
// PARAMETERS  : aList            -> List to that Polygons should be added
//               CheckForOtherRef -> Flag if it should be searched for references
//                                   on other layers.
//
// RETURNVALUE : NONE
// ------------------------------------------------------------------------
procedure TAXGisObjMan.ReadCPolysFromSelected(var aList:TPolygonList;CheckForOtherRef:boolean);
var ObjectDisp:IDispatch;
    aObject   :IBase;
    aCPoly    :IPolygon;
    cnt:integer;
    MaxElems:integer;
    CurrElems:integer;
    ProcessedIds:array of integer;
    NumDupl:integer;
    aId:integer;
    isDupl:boolean;
    i:integer;
    aPolygon:TPolygon;
begin
   SetLength(ProcessedIds, 0); NumDupl:=0;
   MaxElems:=Document.NrOfSelectedObjects-1;
   CurrElems:=0;
   for cnt:=0 to Document.NrOfSelectedObjects-1 do
   begin
      if Document.NrOfSelectedObjects = 0 then break;
      ObjectDisp:=Document.SelItems[cnt];
      if ObjectDisp = nil then
         continue;
      AObject:=ObjectDisp as IBase;
      if (aObject.ObjectType = ot_Cpoly) then // read polygon
      begin
         if CheckForOtherRef then
         begin
            // it will be checked if the object has a reference on other layer and
            // may be ignored (avoid duplicates in List)
            aId:=aObject.Index;
            // now check if this Id has already been processed
            isDupl:=FALSE;
            if NumDupl > 0 then
            begin
               for i:=0 to NumDupl-1 do
               begin
                  if (ProcessedIds[i] = aId) then
                  begin
                     isDupl:=TRUE;
                     break;
                  end;
               end;
            end;
            if not isDupl then
            begin
               // add Id to Processed Id List
               NumDupl:=NumDupl+1;
               SetLength(ProcessedIds, NumDupl);
               ProcessedIds[NumDupl-1]:=aId;
               aCPoly:=aObject as IPolygon;

               aPolygon:=TPolygon.Create;
               aPolygon.Read(aCPoly);
               aList.Add(aPolygon);
            end;
            // if it has already been processed do nothing
         end
         else
         begin
            aCPoly:=aObject as IPolygon;
            aPolygon:=TPolygon.Create;
            aPolygon.Read(aCPoly);
            aList.Add(aPolygon);
         end;
      end;
      CurrElems:=CurrElems+1;
      ShowPercent(MaxElems, CurrElems);
   end;
   SetLength(ProcessedIds, 0); // remove the processed id�s list
end;

//------------------------------------------------------------------------------
// NAME : ShowPercent
//------------------------------------------------------------------------------
// Method is used to show the current process percent status
//
// PARAMETERS: MaxElems  -> Maximum number of elements
//             CurrElems -> Current number of processed elements
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------
procedure TAXGisObjMan.ShowPercent(MaxElems:integer; CurrElems:integer);
var Percent:integer;
begin
   if MaxElems > 0 then Percent:= Round (CurrElems / MaxElems * 100 ) else Percent:= 100;
   if (Percent <> CurrPercentInfo) then
   begin
      CurrPercentInfo:=Percent;
      FEvents.OnDisplayPercent(Percent);
   end;
end;

//------------------------------------------------------------------------------
// NAME : CheckLayerExists
//------------------------------------------------------------------------------
// Method is used to check if a layer already exists
//
// PARAMETERS : Layername -> Name of the layer
//
// RETURNVALUE: TRUE  -> Layer exists
//              FALSE -> Layer does not exist
//------------------------------------------------------------------------------
function TAXGisObjMan.CheckLayerExists(Layername:string):boolean;
var dispatch:IDispatch;
begin
   dispatch:=Document.Layers.GetLayerByName(Layername);
   if dispatch <> nil then result:=true else result:=false;
end;

// ----------------------------------------------------------------------
// METHOD    : CopyCPoly
// ----------------------------------------------------------------------
// Method is used to create a copy of a CPoly object
//
// PARAMETERS: OldObject   -> pointer to CPoly that should be copied
//             NewObject   -> pointer to new CPoly that is a copy
//
// RETURNVALUE: NONE
// ----------------------------------------------------------------------
procedure TAXGisObjMan.CopyCPoly(var OldObject:IBase; var NewObject:IBase);
var IslandList:IList;
    APoint:IPoint;
    RotatePoint:IPoint;
    IslandPoly:IPolygon;
    PointCount:integer;
    SkipIndex :integer;
    FirstIsland:boolean;
    IslandCount:integer;
    IsIsland:boolean;
    IslandInfoIdx:integer;
    NextIslandIdx:integer;
    X,Y:double;
    Cnt:integer;
    First_X,First_Y:double;
    rotatelastpoly:boolean;
    retVal:boolean;

    oldCPoly:IPolygon;
    newCPoly:IPolygon;

    aObjectStyleDisp:IDispatch;
    oldStyleNumber  :integer;
    newStyleNumber  :integer;
    StyleName       :string;
begin
   retVal:=TRUE;
   PointCount:=0; SkipIndex:=0; FirstIsland:=TRUE; IsIsland:=FALSE;
   rotatelastpoly:=FALSE;
   oldCPoly:=oldObject as IPolygon;
   IslandCount:=oldCPoly.IslandCount;
   if IslandCount > 1 then
   begin
      IsIsland:=true;
      IslandInfoIdx:=0;
      NextIslandIdx:=oldCPoly.IslandInfo[IslandInfoIdx];
      // an island object has to be created
      IslandList:=Document.CreateList;
   end;

   newCPoly:=Document.CreatePolygon;

   // insert first point to new CPoly
   APoint:=oldCPoly.Points[0];
   X:=APoint.X/100; Y:=APoint.Y/100;

   First_X:=X; First_Y:=Y;
   APoint.X:=round(X*100); APoint.Y:=round(Y*100);

   newCPoly.InsertPoint(APoint);
   PointCount:=PointCount+1;

   for Cnt:=1 to oldCPoly.Count - 1 do
   begin
      if SkipIndex = 0 then
         PointCount:=PointCount + 1;
      if (IsIsland) and (PointCount = NextIslandIdx) then // next island area will be created
      begin
         IslandInfoIdx:=IslandInfoIdx+1;
         if FirstIsland then
            SkipIndex:=1
         else
            SkipIndex:=2;

         FirstIsland:=FALSE;
         if IslandInfoIdx >= IslandCount then
         begin
            // all islands have been inserted
            NextIslandIdx:=0;
            IslandInfoIdx:=IslandCount;
         end
         else
         begin
            // the polygon has to be inserted to the island
            newCPoly.ClosePoly;
            IslandList.Add(newCPoly);

            // and a new polygon has to be created
            newCPoly:=Document.CreatePolygon;
            NextIslandIdx:=oldCPoly.IslandInfo[IslandInfoIdx];

            // insert point to polygon
            APoint:=oldCPoly.Points[Cnt+SkipIndex];
            X:=APoint.X/100; Y:=APoint.Y/100;

            if (IslandCount-1 = IslandInfoIdx) then
            begin
               // last polygon reached
               if (X = First_X) and (Y = First_Y) then
               begin
                  rotatelastpoly:=true;
                  RotatePoint:=APoint;
                  RotatePoint.X:=round(X*100); RotatePoint.Y:=round(Y*100);
               end;
            end;

            APoint.X:=round(X*100); APoint.Y:=round(Y*100);
            if not rotatelastpoly then
               newCPoly.InsertPoint(APoint);
            PointCount:=1;
         end;
      end
      else
      begin
         if (SkipIndex=0) then
         begin
            if (IsIsland) and (Cnt = oldCPoly.Count -2) then //avoid exporting last connectionline
               continue;

            // insert point to polygon
            APoint:=oldCPoly.Points[Cnt];
            X:=APoint.X/100; Y:=APoint.Y/100;
            APoint.X:=round(X*100); APoint.Y:=round(Y*100);
            newCPoly.InsertPoint(APoint);
         end
         else
            SkipIndex:=SkipIndex-1;
      end;
   end;

   if IsIsland then
   begin
      if rotatelastpoly then
         newCPoly.InsertPoint(RotatePoint);
      newCPoly.ClosePoly;
      IslandList.Add(newCPoly);
      IslandPoly:=Document.CreatePolygon;
      IslandPoly:=Document.CreateIslandArea(IslandList);

      // copy objectstyle of CPoly
      aObjectStyleDisp:=oldCPoly.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         IslandPoly.ObjectStyle:=oldCPoly.ObjectStyle;
         if (oldCPoly.ObjectStyle.Linestyle.Style >= lt_UserDefined) then
         begin
            // old Object has user defined Line Style
            OldStyleNumber:=oldCPoly.ObjectStyle.Linestyle.Style;
            StyleName:=Document.GetUserLineStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=Document.GetUserLineStyleNumberByName(StyleName);
            IslandPoly.ObjectStyle.Linestyle.Style:=NewStyleNumber;
         end;
         OldStyleNumber:=oldCPoly.ObjectStyle.Fillstyle.Pattern;
         if (oldCPoly.ObjectStyle.Fillstyle.Pattern >= pt_UserDefined) then
         begin
            // old Layer has user defined Fill Style
            OldStyleNumber:=oldCPoly.ObjectStyle.Fillstyle.Pattern;
            StyleName:=Document.GetUserFillStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=Document.GetUserFillStyleNumberByName(StyleName);
            IslandPoly.ObjectStyle.Fillstyle.Pattern:=NewStyleNumber;
         end;
      end;

      newObject:=IslandPoly;
   end
   else
   begin
      newCPoly.ClosePoly;

      // copy objectstyle of CPoly
      aObjectStyleDisp:=oldCPoly.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newCPoly.ObjectStyle:=oldCPoly.ObjectStyle;
      end;
      newObject:=newCPoly;
   end;
end;

// ----------------------------------------------------------------------
// METHOD    : CopyObjectStyle
// ----------------------------------------------------------------------
// Method is used to copy the objectstyle from oldObject to newObject
//
// PARAMETERS: oldObject   -> pointer to old object
//             newObject   -> pointer to new object
//
// RETURNVALUE: NONE
// ----------------------------------------------------------------------
procedure TAXGisObjMan.CopyObjectStyle(var oldObject:OleVariant; var newObject:OleVariant);
var AObjectStyleDisp:IDispatch;
begin
   AObjectStyleDisp:=oldObject.ObjectStyle;
   if (AObjectStyleDisp <> nil) then
   begin
      newObject.ObjectStyle:=oldObject.ObjectStyle;
   end;
end;

// ------------------------------------------------------------------------
// NAME: SetDocument
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to set the document reference
// PARAMETERS : aDocument -> Pointer to Document object
// ------------------------------------------------------------------------
procedure TAXGisObjMan.SetDocument(var aDocument: OleVariant); safecall;
var Doc:IDispatch;
begin
   Doc:=aDocument;
   Document:=Doc as IDocument;
end;

// ------------------------------------------------------------------------
// NAME: SetApp
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to set the DX-object
// PARAMETERS : aApp -> Pointer to DX-object
// ------------------------------------------------------------------------
procedure TAXGisObjMan.SetApp(var aApp: OleVariant);
begin
   App:=aApp;
end;

// ------------------------------------------------------------------------
// NAME: CreateIslandLayer
// ------------------------------------------------------------------------
// DESCRIPTION : Method is used to create Island Layer
// PARAMETERS  : SourceLayer -> Layer that contains CPolys
//               DestLayer   -> Layer for result
//
// RETURNVALUE : NONE
// ------------------------------------------------------------------------
procedure TAXGisObjMan.CreateIslandLayer(const SourceLayer, DestLayer: WideString);
var aCPoly1    :p_Polygon_record;
    aCPoly2    :p_Polygon_record;
    aLayer     :ILayer;
    MaxElems   :integer;
    CurrElems  :integer;
    aCPolyList :TPolygonList;
begin
   StrFileObj:=ClassStrFile.Create;
   StrFileObj.CreateList(FWorkingDir, FLngCode, 'OBJMAN');
   // first the CPolys of layer Sourcelayer have to be read
   FEvents.OnDisplayPass(StrfileObj.Assign(1)+SourceLayer); // read CPolys from layer:

   // first init the FIFO buffers
   Init_Fifo_Buffers;

   aCPolyList:=TPolygonList.Create;
   // now read all CPolys from Layer and add it to the aPolyList
   ReadCPolysFromLayer(SourceLayer, aCPolyList);

   // now the island calculation has to be done
   MaxElems:=aCPolyList.GetNumEntries;
   CurrElems:=0;
   FEvents.OnDisplayPass(StrfileObj.Assign(2)); // Calculate Islands

   aCPoly1:=aCPolyList.GetFirst;
   while (aCPoly1 <> nil) do
   begin
      aCPoly2:=aCPoly1^.next;
      while (aCPoly2 <> nil) do
      begin
         // check if aCPoly1 is inside aCPoly2
         // aCPoly1 only has to be checked if it has not been already added to another Polygon
         if {(not aCPoly1^.Poly.IsIslandPart) and } (aCPoly1^.Poly.IsInside(aCPoly2^.Poly)) then
         begin
            // CPoly1 is inside of aCPoly2
            // now add all parts from aCPoly1 to aCPoly2
            aCPoly2^.Poly.AddPartsForCreateIslandLayer(aCPoly1^.Poly);
         end
         // check if Poly2 is inside of Poly1
         // aCPoly2 only has to be checked if it has not been already added to another Polygon
         else if {(not aCPoly2^.Poly.IsIslandPart) and } (aCPoly2^.Poly.IsInside(aCPoly1^.Poly)) then
         begin
            // CPoly2 is inside of aCPoly1
            // now add all parts from aCPoly2 to aCPoly1
            aCPoly1^.Poly.AddPartsForCreateIslandLayer(aCPoly2^.Poly);

            if FDoDebug then
            begin
               aCPoly2^.Poly.WriteDebugInfo;
               aCPoly1^.Poly.WriteDebugInfo;
            end;
         end;
         aCPoly2:=aCPoly2^.next;
      end;
      aCPoly1:=aCPoly1^.next;
      CurrElems:=CurrElems+1;
      ShowPercent(MaxElems, CurrElems);
   end;

   // now write the new calculated islands to layer TargetLayer
   if CheckLayerExists(DestLayer) then
      aLayer:=Document.Layers.GetLayerByName(DestLayer)
   else
      aLayer:=Document.Layers.InsertLayerByName(DestLayer,True);

   FEvents.OnDisplayPass(StrfileObj.Assign(3)+DestLayer); // Create Islands on Layer:
   MaxElems:=aCPolyList.GetNumEntries;
   CurrElems:=0;
   aCPoly1:=aCPolyList.GetFirst;
   while (aCPoly1 <> nil) do
   begin
      aCPoly1^.Poly.WriteToLayer(Document, aLayer);
      aCPoly1:=aCPoly1^.next;
      CurrElems:=CurrElems+1;
      ShowPercent(MaxElems, CurrElems);
   end;
   FEvents.OnDisplayPass(StrfileObj.Assign(4)); // Island Calculation finished

   // destroy objects
   aCPolyList.Destroy;
   Release_Fifo_Buffers;
end;

// ----------------------------------------------------------------------
// METHOD    : CombineCPolysOnLayer
// ----------------------------------------------------------------------
// Method is used to combine all CPolys on laye SourceLayer and
// create result on layer TargetLayer
//
// PARAMETERS : SourceLayer      -> Layer that contains cpolys
//              TargetLayer      -> Layer on that the result will be created
//              SourceIsSelected -> if TRUE source is selected objects
//
// RETURNVALUE: Id of the new created object if all objects could be
//              combined to one object.
// ----------------------------------------------------------------------
function TAXGisObjMan.CombineCPolysOnLayer(const SourceLayer,
  TargetLayer: WideString; SourceIsSelected: WordBool): Integer;
var aCPoly1     :p_Polygon_record;
    aCPoly2     :p_Polygon_record;
    aLayer      :ILayer;
    hadchanged  :boolean;
    MaxElems    :integer;
    CurrElems   :integer;
    retVal      :integer;
    aList       :TPolygonList;
begin
   retVal:=-1; // means not all objects could be combined to one
   result:=retVal;
   StrFileObj:=ClassStrFile.Create;
   StrFileObj.CreateList(FWorkingDir, FLngCode, 'OBJMAN');
   // first init the FIFO buffers
   Init_Fifo_Buffers;

   // now add the Polygons to a List
   aList:=TPolygonList.Create;
   if (SourceIsSelected) then
   begin
      FEvents.OnDisplayPass(StrfileObj.Assign(5)); // Read selected Areas
      ReadCPolysFromSelected(aList, TRUE);         // ignore references on other layers
   end
   else
   begin
      // first the CPolys of layer Sourcelayer have to be read
      FEvents.OnDisplayPass(StrfileObj.Assign(1)+SourceLayer); // Read Areas from Layer
      ReadCPolysFromLayer(SourceLayer, aList);
   end;

   if aList.GetNumEntries < 2 then // there must be at least two Polygons for combine
   begin
      // copy object that has been selected to target layer
      if CheckLayerExists(TargetLayer) then
         aLayer:=Document.Layers.GetLayerByName(TargetLayer)
      else
         aLayer:=Document.Layers.InsertLayerByName(TargetLayer,True);

      FEvents.OnDisplayPass(StrfileObj.Assign(7)+TargetLayer); // Create combining result on layer

      MaxElems:=aList.GetNumEntries;
      CurrElems:=0;
      aCPoly1:=aList.GetFirst;
      while (aCPoly1 <> nil) do
      begin
         retVal:=aCPoly1^.Poly.WriteToLayer(Document, aLayer);
         aCPoly1:=aCPoly1^.next;
         CurrElems:=CurrElems+1;
         Showpercent(MaxElems, CurrElems);
      end;
      if (CurrElems > 1) then
         retVal:=-1;
      FEvents.OnDisplayPass(StrfileObj.Assign(8)); // Combining finished
      // destroy the objects
      aList.Destroy;
      Release_Fifo_Buffers;
      StrFileObj.Destroy;
      exit;
   end;

   // now the combining of objects has to be done
   FEvents.OnDisplayPass(StrfileObj.Assign(6)); // Calculate combined Areas without holes
   CurrElems:=0;
   aCPoly1:=aList.GetFirst;
   while (aCPoly1^.next <> nil) do
   begin
      aCPoly2:=aCPoly1^.next; hadchanged:=FALSE;
      while (aCPoly2 <> nil) and (not hadchanged) do
      begin
         // try to combine aCPoly1 with aCPoly2
         if aCPoly1^.Poly.Combine(aCPoly2^.Poly, FALSE) then
         begin
            hadchanged:=TRUE;
            // the Polygons could have been combined
            // aCPoly2 is obsulete now
            aList.Remove(aCPoly2);
         end;
         if not hadchanged then
            aCPoly2:=aCPoly2^.next;
      end;
      MaxElems:=aList.GetNumEntries;
      CurrElems:=CurrElems+1;
      ShowPercent(MaxElems, CurrElems);

      if not hadchanged then
         aCPoly1:=aCPoly1^.next;
   end;

   // now process the holes on the resulting polygons
   FEvents.OnDisplayPass(StrfileObj.Assign(10)); // Calculate holes on combined Polygons
   CurrElems:=0;
   aCPoly1:=aList.GetFirst;
   while (aCPoly1^.next <> nil) do
   begin
      aCPoly2:=aCPoly1^.next; hadchanged:=FALSE;
      while (aCPoly2 <> nil) and (not hadchanged) do
      begin
         // try to combine aCPoly1 with aCPoly2
         if aCPoly1^.Poly.Combine(aCPoly2^.Poly, TRUE) then
         begin
            hadchanged:=TRUE;
            // the Polygons could have been combined
            // aCPoly2 is obsulete now
            aList.Remove(aCPoly2);
         end;
         if not hadchanged then
            aCPoly2:=aCPoly2^.next;
      end;
      MaxElems:=aList.GetNumEntries;
      CurrElems:=CurrElems+1;
      ShowPercent(MaxElems, CurrElems);

      if not hadchanged then
         aCPoly1:=aCPoly1^.next;
   end;


   // now write the result of the combining to the TargetLayer
   if CheckLayerExists(TargetLayer) then
      aLayer:=Document.Layers.GetLayerByName(TargetLayer)
   else
      aLayer:=Document.Layers.InsertLayerByName(TargetLayer,True);

   FEvents.OnDisplayPass(StrfileObj.Assign(7)+TargetLayer); // Create combining result on layer

   MaxElems:=aList.GetNumEntries;
   CurrElems:=0;
   aCPoly1:=aList.GetFirst;
   while (aCPoly1 <> nil) do
   begin
      retVal:=aCPoly1^.Poly.WriteToLayer(Document, aLayer);
      aCPoly1:=aCPoly1^.next;
      CurrElems:=CurrElems+1;
      Showpercent(MaxElems, CurrElems);
   end;
   if (CurrElems > 1) then
      retVal:=-1;
   FEvents.OnDisplayPass(StrfileObj.Assign(8)); // Combining finished

   // destroy objects
   aList.Destroy;
   Release_Fifo_Buffers;
   StrFileObj.Destroy;
   result:=retVal;
end;

// ----------------------------------------------------------------------
// METHOD    : CopyObjectToLayer
// ----------------------------------------------------------------------
// Method is used to copy an object to an other layer
//
// PARAMETERS: Obj         -> Object that should be copied
//             TargetLayer -> Layer on that the new object should be
//                            created.
//
// RETURNVALUE: ID of the new created object
// ----------------------------------------------------------------------
function TAXGisObjMan.CopyObjectToLayer(var Obj: OleVariant; const TargetLayer: WideString): Integer;
var aLayer          :ILayer;
    aDisp           :IDispatch;
    aObjectStyleDisp:IDispatch;

    newIndex :integer;
    i        :integer;
    ImageFilename:string;

    ignoreobject :boolean;

    oldObject    :IBase;
    newObject    :IBase;

    aPoint       :IPoint;
    oldPixel     :IPixel;
    newPixel     :IPixel;
    oldPolyline  :IPolyline;
    newPolyline  :IPolyline;
    oldSpline    :ISpline;
    newSpline    :ISpline;
    oldArc       :IArc;
    newArc       :IArc;
    oldText      :IText;
    newText      :IText;
    oldCircle    :ICircle;
    newCircle    :ICircle;
    oldSymbol    :ISymbol;
    newSymbol    :ISymbol;
    oldImage     :IImage;
    newImage     :IImage;
begin
   // first check if the TargetLayer is already created
   if CheckLayerExists(TargetLayer) then
      aLayer:=Document.Layers.GetLayerByName(TargetLayer)
   else
      aLayer:=Document.Layers.InsertLayerByName(TargetLayer,True);
   ignoreobject:=FALSE;
   aDisp:=Obj;
   oldObject:=aDisp as IBase;

   if (oldObject.ObjectType = ot_Pixel) then // copy Pixel object
   begin
      oldPixel:=oldObject as IPixel;
      newPixel:=Document.CreatePixel;
      aPoint:=Document.CreatePoint;
      aPoint.X:=oldPixel.Position.X;
      aPoint.Y:=oldPixel.Position.Y;
      newPixel.Position:=aPoint;

      // copy objectstyle of Pixel
      aObjectStyleDisp:=oldPixel.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newPixel.ObjectStyle:=oldPixel.ObjectStyle;
      end;
      newObject:=newPixel;
   end
   else if (oldObject.ObjectType = ot_Cpoly) then // copy area object
   begin
      CopyCPoly(oldObject, newObject);
   end
   else if (oldObject.ObjectType = ot_poly) then // copy line object
   begin
      oldPolyline:=oldObject as IPolyline;
      newPolyline:=Document.CreatePolyline;
      // create new point list
      for i:=0 to oldPolyline.Count - 1 do
      begin
         APoint:=oldPolyline.Points[i];
         newPolyline.InsertPoint(APoint);
      end;
      // copy objectstyle of Polyline
      aObjectStyleDisp:=oldPolyline.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newPolyline.ObjectStyle:=oldPolyline.ObjectStyle;
      end;
      newObject:=newPolyline;
   end
   else if (oldObject.ObjectType = ot_Spline) then // copy spline object
   begin
      oldSpline:=oldObject as ISpline;
      newSpline:=Document.CreateSpline;
      // create new point list
      for i:=0 to oldSpline.Count - 1 do
      begin
         APoint:=oldSpline.Points[i];
         newSpline.InsertPoint(APoint);
      end;

      // copy objectstyle of Spline
      aObjectStyleDisp:=oldSpline.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newSpline.ObjectStyle:=oldSpline.ObjectStyle;
      end;
      newObject:=newSpline;
   end
   else if (oldObject.ObjectType = ot_Arc) then    // copy arc object
   begin
      oldArc:=oldObject as IArc;
      newArc:=Document.CreateArc;
      APoint:=Document.CreatePoint;
      APoint.X:=oldArc.Position.X;
      APoint.Y:=oldArc.Position.Y;
      newArc.Position:=APoint;
      newArc.PrimaryAxis:=oldArc.PrimaryAxis;
      newArc.SecondaryAxis:=oldArc.SecondaryAxis;
      newArc.Angle:=oldArc.Angle;
      newArc.BeginAngle:=oldArc.BeginAngle;
      newArc.EndAngle:=oldArc.EndAngle;
      newArc.Radius:=oldArc.Radius;

      // copy objectstyle of Arc
      aObjectStyleDisp:=oldArc.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newArc.ObjectStyle:=oldArc.ObjectStyle;
      end;
      newObject:=newArc;
   end
   else if (oldObject.ObjectType = ot_Text) then // copy text object
   begin
      oldText:=oldObject as IText;
      newText:=Document.CreateText;
      APoint:=Document.CreatePoint;
      APoint.X:=oldText.Position.X;
      APoint.Y:=oldText.Position.Y;
      newText.Position:=APoint;
      newText.Angle:=oldText.Angle;
      newText.Width:=oldText.Width;
      newText.Align:=oldText.Align;
      newText.Text:=oldText.Text;
      newText.FontStyle:=oldText.FontStyle;
      newText.FontHeight:=oldText.FontHeight;
      newText.FontName:=oldText.FontName;

      // copy objectstyle of Text
      aObjectStyleDisp:=oldText.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newText.ObjectStyle:=oldText.ObjectStyle;
      end;
      newObject:=newText;
   end
   else if (oldObject.ObjectType = ot_Circle) then // copy circle object
   begin
      oldCircle:=oldObject as ICircle;
      newCircle:=Document.CreateCircle;
      APoint:=Document.CreatePoint;
      APoint.X:=oldCircle.Position.X;
      APoint.Y:=oldCircle.Position.X;
      newCircle.Position:=APoint;
      newCircle.PrimaryAxis:=oldCircle.PrimaryAxis;
      newCircle.SecondaryAxis:=oldCircle.SecondaryAxis;
      newCircle.Angle:=oldCircle.Angle;
      newCircle.Radius:=oldCircle.Radius;

      // copy objectstyle of Circle
      aObjectStyleDisp:=oldCircle.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newCircle.ObjectStyle:=oldCircle.ObjectStyle;
      end;
      newObject:=newCircle;
   end
   else if (oldObject.ObjectType = ot_Symbol) then // copy symbol object
   begin
      oldSymbol:=oldObject as ISymbol;
      newSymbol:=Document.CreateSymbol;
      APoint:=Document.CreatePoint;
      APoint.X:=oldSymbol.Position.X;
      APoint.Y:=oldSymbol.Position.Y;
      newSymbol.Position:=APoint;
      newSymbol.SymIndex:=oldSymbol.SymIndex;
      newSymbol.Size:=oldSymbol.Size;
      newSymbol.SizeType:=oldSymbol.SizeType;
      newSymbol.Angle:=oldSymbol.Angle;
      newSymbol.SymbolName:=oldSymbol.Symbolname;

      // copy objectstyle of Symbol
      aObjectStyleDisp:=oldSymbol.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newSymbol.ObjectStyle:=oldSymbol.ObjectStyle;
      end;
      newObject:=newSymbol;
   end
   else if (oldObject.ObjectType = ot_Image) then // copy image object
   begin
      oldImage:=oldObject as IImage;
      ImageFilename:=oldImage.Filename;
      if Fileexists(Imagefilename) then
      begin
         // create image object
         newImage:=Document.CreateImage(Imagefilename);
         newImage.SetPosition(oldImage.X, oldImage.Y, round(oldImage.Width), round(oldImage.Height));
         // set image-style
         newImage.ShowOpt:=oldImage.ShowOpt;
         newImage.Options:=oldImage.Options;
         newImage.TransparencyType:=oldImage.TransparencyType;
         newImage.Transparency:=oldImage.Transparency;

         newObject:=newImage;
      end
      else
         ignoreobject:=true;
   end
   else
      ignoreobject:=true;

   if (not ignoreobject) then
   begin
      aLayer.InsertObject(newObject);
      newIndex:=newObject.Index;
      result:=newIndex;
   end
   else
      newIndex:=-1;
   result:=newIndex;
end;

//------------------------------------------------------------------------------
// NAME : CheckIdHasDuplicateOnLayer
//------------------------------------------------------------------------------
// Method is used to check if a object specified by it�s id has a duplicate on
// the specified layer.
//
// PARAMETERS : Id    -> Id of the object that should be checked.
//              Layer -> Layer that should be checked
//
// RETURNVALUE: Id of object that is duplicate on layer
//              If no duplicate found return -1
//------------------------------------------------------------------------------
function TAXGisObjMan.CheckIdHasDuplicateOnLayer(Id: Integer;
  const Layer: WideString): Integer;
var AnalyzeObj:TCheckDuplicate;
    retVal:integer;
begin
   AnalyzeObj:=TCheckDuplicate.Create;
   AnalyzeObj.App:=App;
   AnalyzeObj.Document:=Document;
   AnalyzeObj.DoCheckId:=TRUE;
   retVal:=AnalyzeObj.CheckIdHasDuplicateOnLayer(Id, Layer);
   AnalyzeObj.Destroy;
   result:=retVal;
end;

//------------------------------------------------------------------------------
// NAME : CheckIsDuplicate
//------------------------------------------------------------------------------
// Method is used to check if a object is a duplicate of an other object.
//
// PARAMETERS : Ori   -> First Object
//              Old   -> Layer that should be checked
//
// RETURNVALUE: TRUE  -> Ori is a Duplicate of Old
//              FALSE -> Ori is no Duplicate of Old
//------------------------------------------------------------------------------
function TAXGisObjMan.CheckIsDuplicate(var Ori, Old: OleVariant): WordBool;
var AnalyzeObj:TCheckDuplicate;
    retVal:boolean;
    aDisp :IDispatch;
    Obj1, Obj2:IBase;
begin
   AnalyzeObj:=TCheckDuplicate.Create;
   AnalyzeObj.DoCheckId:=FALSE;
   aDisp:=Ori;
   Obj1:=aDisp as IBase;
   aDisp:=Old;
   Obj2:=aDisp as IBase;
   retVal:=AnalyzeObj.CheckIsDuplicate(Obj1, Obj2);
   AnalyzeObj.Destroy;
   result:=retVal;
end;

//------------------------------------------------------------------------------
// NAME : CheckPolyInsidePoly
//------------------------------------------------------------------------------
// Method is used to check if Poly1 is inside Poly2
//
// PARAMETERS : Poly1 -> Reference to Poly1
//              Poly2 -> Reference to Poly2
//
// RETURNVALUE: TRUE  -> Poly1 is inside Poly2
//              FALSE -> Poly1 is not inside Poly2
//------------------------------------------------------------------------------
function TAXGisObjMan.CheckPolyInsidePoly(var Poly1, Poly2: OleVariant): WordBool;
var retVal:boolean;
    aPoly1, aPoly2:IPolygon;
    aDisp:IDispatch;
    Polygon1, Polygon2:TPolygon;
begin
   // first init the FIFO buffers
   Init_Fifo_Buffers;
   // get first polygon
   aDisp:=Poly1; aPoly1:=aDisp as IPolygon;
   Polygon1:=TPolygon.Create;
   Polygon1.Read(aPoly1);
   // get second polygon
   aDisp:=Poly2; aPoly2:=aDisp as IPolygon;
   Polygon2:=TPolygon.Create;
   Polygon2.Read(aPoly2);
   // check if Polygon1 is inside Polygon2
   retVal:=Polygon1.IsInside(Polygon2);
   // destroy objects
   Polygon1.Destroy;
   Polygon2.Destroy;
   Release_Fifo_Buffers;
   result:=retVal;
end;

//------------------------------------------------------------------------------
// NAME : CheckPointInsidePoly
//------------------------------------------------------------------------------
// Method is used to check if a point that is referenced by it�s coordinates
// is inside a poly.
//
// PARAMETERS : X    -> X-Coordinate of Point
//              Y    -> Y-Coordinate of Point
//              Poly -> Poly that should be checked
//              NODKMCHECK -> In Dkm a point can not be on an island inside an
//                            island. So if NODKMCHECK is set to FALSE, the
//                            islands will be handeled special.
//
// RETURNVALUE: TRUE  -> Point is inside Poly
//              FALSE -> Point is not inside Poly
//------------------------------------------------------------------------------
function TAXGisObjMan.CheckPointInsidePoly(X, Y: Double; var Poly: OleVariant; NoDkmcheck: WordBool): WordBool;
var retVal:boolean;
    aPolygon:TPolygon;
    X1,X2,Y1,Y2:double;
    SmallX, SmallY, LargeX, LargeY:double;
    aRect:OleVariant;
    aDisp:IDispatch;
    aPoly:IPolygon;
    insidetype:TPolygonPartType;
begin
   // first check if the point is inside the Cliprect of the Poly
   retVal:=FALSE;
   aRect:=Poly.ClipRect;
   X1:=aRect.A.X; X1:=X1 / 100;
   Y1:=aRect.A.Y; Y1:=Y1 / 100;
   X2:=aRect.B.X; X2:=X2 / 100;
   Y2:=aRect.B.Y; Y2:=Y2 / 100;
   if X1 < X2 then begin SmallX:=X1; LargeX:=X2; end else begin SmallX:=X2; LargeX:=X1; end;
   if Y1 < Y2 then begin SmallY:=Y1; LargeY:=Y2; end else begin SmallY:=Y2; LargeY:=Y1; end;
   if X >= SmallX then
   if Y >= SmallY then
   if X <= LargeX then
   if Y <= LargeY then
   begin
      // first init the FIFO buffers
      Init_Fifo_Buffers;
      aDisp:=Poly;
      aPoly:=aDisp as IPolygon;
      aPolygon:=TPolygon.Create;
      aPolygon.Read(aPoly);
      insidetype:=aPolygon.IsInside(X, Y);
      retVal:=FALSE;
      if (insidetype = OuterIsland) then
      begin
         if (NoDkmCheck) then retVal:=TRUE
         else retVal:=FALSE; // in Dkm-Check a corner that is on outer Island is not inside
      end
      else if (insidetype = Main) then
         retVal:=TRUE;
      aPolygon.Destroy;
      Release_Fifo_Buffers;
   end;
   result:=retVal;
end;

//------------------------------------------------------------------------------
// NAME : CopyNonDuplicatesToLayer
//------------------------------------------------------------------------------
// Method is used to check if an object on Layer1 has a duplicate on Layer2 and
// copy the object to Layer TargetLayer if it has no duplicate.
//
// PARAMETERS : Layer1      -> Name of Layer1
//              Layer2      -> Name of Layer2
//              TargetLayer -> Targetlayer on that the objects with no duplicate
//                             should be created.
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------
procedure TAXGisObjMan.CopyNonDuplicatesToLayer(const Layer1, Layer2,
  TargetLayer: WideString);
var ALayer   :Variant;
    cnt      :integer;
    Layername:string;
    aObject  :OleVariant;
    aIndex   :integer;
    MaxElems :integer;
    CurrElems:integer;
begin
   StrFileObj:=ClassStrFile.Create;
   StrFileObj.CreateList(FWorkingDir, FLngCode, 'OBJMAN');
   FEvents.OnDisplayPass(StrfileObj.Assign(9)); // Check Layer for duplicates
   // now the pointer to the Layer1 has to be received
   for cnt:=0 to Document.Layers.Count-1 do
   begin
      ALayer:=Document.Layers.Items[cnt];
      Layername:=ALayer.Layername;
      if (Layername = Layer1) then
      begin
         break;
      end;
   end;
   MaxElems:=aLayer.count-1;
   CurrElems:=0;
   for cnt:=0 to aLayer.count-1 do
   begin
      aObject:=aLayer.Items[cnt];
      aIndex:=aObject.Index;
      if CheckIdHasDuplicateOnLayer(aIndex, Layer2) = 0 then
      begin
         CopyObjectToLayer(aObject, TargetLayer);
      end;
      CurrElems:=CurrElems+1;
      ShowPercent(MaxElems, CurrElems);
   end;
   StrFileObj.Destroy;
end;

function TAXGisObjMan.Get_DoDebug: WordBool;
begin
   result:=FDoDebug;
end;

procedure TAXGisObjMan.Set_DoDebug(Value: WordBool);
begin
   FDoDebug:=Value;
end;

//------------------------------------------------------------------------------
// NAME : GetIdOfSurroundingCPolyOnLayer
//------------------------------------------------------------------------------
// Method is used to get the Id of a CPoly that is on specified layer and around
// the Object that is specified by it's Id
//
// PARAMETERS : Id    -> Id of the object that should be checked.
//              Layer -> Layer that should be checked
//
// RETURNVALUE: Id of the CPoly that is around object
//              If no CPoly found, or more than one return 0
//------------------------------------------------------------------------------
function TAXGisObjMan.GetIdOfSurroundingCPolyOnLayer(Id: Integer;
  const Layer: WideString): Integer;
var AnalyzeObj:TCheckDuplicate;
    retVal:integer;
    i:integer;
    CheckObj:IBase;
    CheckObjVar:OleVariant;
    OtherObj:IBase;
    OtherObjVar:OleVariant;
    APixel:IPixel;
    AText :IText;
    ASymbol:ISymbol;
    X,Y:double;
    docheck:boolean;
    isinside:boolean;
    oldflagvalue:boolean;
begin
   retVal:=0;
   AnalyzeObj:=TCheckDuplicate.Create;
   AnalyzeObj.App:=App;
   AnalyzeObj.Document:=Document;
   AnalyzeObj.DoCheckId:=TRUE;
   // first get all CPolys that touch object
   AnalyzeObj.GetIdsOfTouchingCPolysOnLayer(Id, Layer);
   // now we iterate through the CPolys that thouch CPoly
   // and find that CPoly that is around
   docheck:=FALSE;
   if (AnalyzeObj.NumObjectsOfIdList > 0) then
   begin
      CheckObj:=Document.GetObjectByIndex(Id);
      if CheckObj.ObjectType = ot_CPoly then
      begin
         CheckObjVar:=CheckObj;  // we need to check if polygon is inside polygon
         docheck:=TRUE;
      end
      else if CheckObj.ObjectType = ot_Pixel then // we need to check if point is inside polygon
      begin
         APixel:=CheckObj as IPixel;
         X:=APixel.Position.x/100;
         Y:=APixel.Position.y/100;
         docheck:=TRUE;
      end
      else if CheckObj.ObjectType = ot_Text then // we need to check if point is inside polygon
      begin
         AText:=CheckObj as IText;
         X:=AText.Position.X/100;
         Y:=AText.Position.Y/100;
         docheck:=TRUE;
      end
      else if CheckObj.ObjectType = ot_Symbol then // we need to check if point is inside polygon
      begin
         ASymbol:=CheckObj as ISymbol;
         X:=ASymbol.Position.X/100;
         Y:=ASymbol.Position.Y/100;
         docheck:=TRUE;
      end
      else
         docheck:=FALSE;
   end;


   if docheck then
   begin
      oldflagvalue:=EqualPartIsAlsoInside; // store flag value
      EqualPartIsAlsoInside:=TRUE; // set flag that equal parts are also inside for searching the surrounding cpoly
      for i:=0 to AnalyzeObj.NumObjectsOfIdList-1 do
      begin
         OtherObj:=Document.GetObjectByIndex(AnalyzeObj.ObjectIdList[i]);
         OtherObjVar:=OtherObj;
         // other object could only be a Polygon
         isinside:=FALSE;
         if (CheckObj.ObjectType = ot_CPoly) then // check if cpoly is inside cpoly
            isinside:=CheckPolyInsidePoly(CheckObjVar, OtherObjVar)
         else
            isinside:=CheckPointInsidePoly(X,Y, OtherObjVar, FALSE);
         if isinside then
         begin
            if (retVal <> 0) then
            begin
               // -> more than one CPoly found -> not unique -> return 0
               retVal:=0;
               break;
            end
            else
               retVal:=AnalyzeObj.ObjectIdList[i];
         end;
      end;
      EqualPartIsAlsoInside:=oldflagvalue; // reset flag back to old value
   end;
   AnalyzeObj.Destroy;
   result:=retVal;
end;

//------------------------------------------------------------------------------
// NAME : SplitObjectToLayer
//------------------------------------------------------------------------------
// Method is used to split object to a layer
//
// PARAMETERS : aBase        -> Object that should be splitted
//              aTargetLayer -> Layer to that object should be splitted
//              SplitMask    -> Mask how objects should be splitted
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------
procedure TAXGisObjMan.SplitObjectToLayer(var aBase:IBase; var  aTargetLayer:ILayer; const SplitMask:integer);
var aPolyline:IPolyline;
    newPolyline:IPolyline;
    Point1:IPoint;
    Point2:IPoint;
    i:integer;
    aIndex:integer;
begin
   // only polylines and polygons will be splitted
   if aBase.ObjectType = ot_Poly then
   begin
      aPolyline:=aBase as IPolyline;
      if (SplitMask and smSplitToSingleLines) <> smSplitToSingleLines then
         exit; // if it should not be splitted to single lines -> do nothing
      // create new point list
      for i:=0 to aPolyline.Count - 1 do
      begin
         Point1:=aPolyline.Points[i];
         if i < aPolyline.Count-1 then
         begin
            Point2:=aPolyline.Points[i+1];
            newPolyLine:=Document.CreatePolyLine;
            newPolyLine.InsertPoint(Point1);
            newPolyLine.InsertPoint(Point2);
            aTargetLayer.InsertObject(newPolyLine);
            aIndex:=newPolyLine.Index;
            if (SplitMask and smIgnoreDuplicates) = smIgnoreDuplicates then
            begin
               if CheckIdHasDuplicateOnLayer(aIndex, aTargetLayer.LayerName) > 0 then
                  Document.DeleteObjectByIndex(aIndex);
            end;
         end;
      end;
   end
   else if aBase.ObjectType = ot_CPoly then
   begin
      // if polygon should not be split to single lines or to an island -> do nothing
      if not (((SplitMask and smSplitToSingleLines) = smSplitToSingleLines) or
              ((SplitMask and smSplitIslands) = smSplitIslands)) then
         exit;
      // split the polygon
      SplitCPolyToLayer(aBase, aTargetLayer, SplitMask);
   end;
end;

//------------------------------------------------------------------------------
// NAME : SplitCPolyToLayer
//------------------------------------------------------------------------------
// Method is used to split polygon to a layer
//
// PARAMETERS : aBase        -> Object that should be splitted
//              aTargetLayer -> Layer to that object should be splitted
//              SplitMask    -> Mask how objects should be splitted
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------
procedure TAXGisObjMan.SplitCPolyToLayer(var aBase:IBase; var aTargetLayer:ILayer; const SplitMask:integer);
var PointCount:integer;
    SkipIndex :integer;
    FirstIsland:boolean;
    IslandCount:integer;
    IsIsland:boolean;
    IslandInfoIdx:integer;
    NextIslandIdx:integer;
    Cnt:integer;
    oldCPoly  :IPolygon;
    newCPoly  :IPolygon;
    aPoint    :IPoint;
    newPoly   :IPolyline;
    LastPolyBeginIdx:integer;
    aIndex:integer;
begin
   oldCPoly:=aBase as IPolygon;
   PointCount:=0; SkipIndex:=0; FirstIsland:=TRUE; IsIsland:=FALSE;
   IslandCount:=oldCPoly.IslandCount;
   if IslandCount > 1 then
   begin
      IsIsland:=true;
      IslandInfoIdx:=0;
      NextIslandIdx:=oldCPoly.IslandInfo[IslandInfoIdx];
   end;

   if (SplitMask and smSplitIslands) = smSplitIslands then
      newCPoly:=Document.CreatePolygon;
   if (SplitMask and smSplitToSingleLines) = smSplitToSingleLines then
      newPoly:=Document.CreatePolyLine;

   aPoint:=oldCPoly.Points[0]; LastPolyBeginIdx:=0;

   if (SplitMask and smSplitIslands) = smSplitIslands then
      newCPoly.InsertPoint(aPoint);

   if (SplitMask and smSplitToSingleLines) = smSplitToSingleLines then
      newPoly.InsertPoint(aPoint);

   PointCount:=PointCount+1;

   for Cnt:=1 to oldCPoly.Count - 1 do
   begin
      if SkipIndex = 0 then
         PointCount:=PointCount + 1;
      if (IsIsland) and (PointCount = NextIslandIdx) then // next island area will be created
      begin
         IslandInfoIdx:=IslandInfoIdx+1;
         if FirstIsland then
            SkipIndex:=1
         else
            SkipIndex:=2;

         FirstIsland:=FALSE;
         if IslandInfoIdx >= IslandCount then
         begin
            // all islands have been inserted
            NextIslandIdx:=0;
            IslandInfoIdx:=IslandCount;
         end
         else
         begin
            // polygon has to be closed
            if (SplitMask and smSplitIslands) = smSplitIslands then
            begin
               // add Polygon to layer
               aTargetLayer.InsertObject(newCPoly);
               aIndex:=newCPoly.Index;
               if (SplitMask and smIgnoreDuplicates) = smIgnoreDuplicates then
               begin
                  if CheckIdHasDuplicateOnLayer(aIndex, aTargetLayer.LayerName) > 0 then
                     Document.DeleteObjectByIndex(aIndex);
               end;
               newCPoly:=Document.CreatePolygon;
            end;

            if (SplitMask and smSplitToSingleLines) = smSplitToSingleLines then
            begin
               if newPoly.Count = 1 then
               begin
                  // add connection line to first point
                  aPoint:=oldCPoly.Points[LastPolyBeginIdx];
                  newPoly.InsertPoint(aPoint);
                  aTargetLayer.InsertObject(newPoly);

                  aIndex:=newPoly.Index;
                  if (SplitMask and smIgnoreDuplicates) = smIgnoreDuplicates then
                  begin
                     if CheckIdHasDuplicateOnLayer(aIndex, aTargetLayer.LayerName) > 0 then
                        Document.DeleteObjectByIndex(aIndex);
                  end;
                  newPoly:=Document.CreatePolyLine;
               end;
            end;
            NextIslandIdx:=oldCPoly.IslandInfo[IslandInfoIdx];

            // insert point to polygon
            aPoint:=oldCPoly.Points[Cnt+SkipIndex]; LastPolyBeginIdx:=Cnt+SkipIndex;

            if (SplitMask and smSplitIslands) = smSplitIslands then
               NewCPoly.InsertPoint(aPoint);
            if (SplitMask and smSplitToSingleLines) = smSplitToSingleLines then
               NewPoly.InsertPoint(aPoint);
            PointCount:=1;
         end;
      end
      else
      begin
         if (SkipIndex=0) then
         begin
            if (IsIsland) and (Cnt = oldCPoly.Count -2) then //avoid exporting last connectionline
               continue;

            // insert point to polygon
            APoint:=oldCPoly.Points[Cnt];
            if (SplitMask and smSplitIslands) = smSplitIslands then
               NewCPoly.InsertPoint(aPoint);

            if (SplitMask and smSplitToSingleLines) = smSplitToSingleLines then
            begin
               if newPoly.Count = 1 then
               begin
                  // add connection line to first point
                  newPoly.InsertPoint(aPoint);
                  aTargetLayer.InsertObject(newPoly);

                  aIndex:=newPoly.Index;
                  if (SplitMask and smIgnoreDuplicates) = smIgnoreDuplicates then
                  begin
                     if CheckIdHasDuplicateOnLayer(aIndex, aTargetLayer.LayerName) > 0 then
                        Document.DeleteObjectByIndex(aIndex);
                  end;

                  newPoly:=Document.CreatePolyLine;
                  newPoly.InsertPoint(aPoint); // new start point
               end;
            end;
         end
         else
            SkipIndex:=SkipIndex-1;
      end;
   end;

   if (SplitMask and smSplitIslands) = smSplitIslands then
   begin
      aTargetLayer.InsertObject(newCPoly); // close last polygon
      aIndex:=newPoly.Index;
      if (SplitMask and smIgnoreDuplicates) = smIgnoreDuplicates then
      begin
         if CheckIdHasDuplicateOnLayer(aIndex, aTargetLayer.LayerName) > 0 then
            Document.DeleteObjectByIndex(aIndex);
      end;
   end;

   if (SplitMask and smSplitToSingleLines) = smSplitToSingleLines then
   begin
      if newPoly.Count = 1 then
      begin
         // add connection line to first point
         aPoint:=oldCPoly.Points[LastPolyBeginIdx];
         newPoly.InsertPoint(aPoint);
         aTargetLayer.InsertObject(newPoly);
         aIndex:=newPoly.Index;
         if (SplitMask and smIgnoreDuplicates) = smIgnoreDuplicates then
         begin
            if CheckIdHasDuplicateOnLayer(aIndex, aTargetLayer.LayerName) > 0 then
               Document.DeleteObjectByIndex(aIndex);
         end;
      end;
   end;
end;

//------------------------------------------------------------------------------
// NAME : SplitObjectsToLayer
//------------------------------------------------------------------------------
// Method is used to split objects from a layer to another layer
//
// PARAMETERS : SourceLayers     -> List of layers that contain the objects
//              TargetLayer      -> Layer to that target should be generated
//              Splitmask        -> Mask how the objects should be splitted
//              SourceIsSelected -> Flag if selected objects should be used as source
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------
procedure TAXGisObjMan.SplitObjectsToLayer(const SourceLayers, TargetLayer: WideString; Splitmask: Integer; SourceIsSelected: WordBool);
var SourceLayerList:TStrings;
    i,j:integer;
    ElemsToWork, ElemsWorked:integer;
    aLayer:ILayer;
    aTargetLayer:ILayer;
    aBase:IBase;
    dummy:string;

    Selected_Stack:array of integer;
    Selected_StackSize:integer;
    Selected_StackLayernames:TStrings;
    aIndex:integer;
begin
   StrFileObj:=ClassStrFile.Create;
   StrFileObj.CreateList(FWorkingDir, FLngCode, 'OBJMAN');
   dummy:=SourceLayers;
   Selected_StackLayernames:=nil;

   SourceLayerList:=TStringList.Create;
   SourceLayerList.SetText(PCHAR(dummy));

   FEvents.OnDisplayPass(StrfileObj.Assign(11)); // Split objects on layer

   if SourceIsSelected then
   begin
      // create stack that contains selected objects
      Selected_StackSize:=Document.NrOfSelectedObjects;
      SetLength(Selected_Stack, Selected_StackSize);

      for i:=0 to Selected_StackSize-1 do
      begin
         aBase:=Document.SelItems[i];
         if aBase.ObjectType = ot_Layer then
         begin
            aLayer:=aBase as ILayer;
            if Selected_StackLayernames = nil then
               Selected_StackLayernames:=TStringList.Create;
            aIndex:=(ot_Layer * -1) - Selected_StackLayernames.Count;
            Selected_StackLayernames.Add(aLayer.Layername);
            // add the name to the stack
            Selected_Stack[i]:=aIndex;
         end
         else
         begin
            aIndex:=aBase.Index;
            Selected_Stack[i]:=aIndex;
         end;
      end;
   end;

   if CheckLayerExists(TargetLayer) then
      aTargetLayer:=Document.Layers.GetLayerByName(TargetLayer)
   else
      aTargetLayer:=Document.Layers.InsertLayerByName(TargetLayer,True);

   // first get number of objects
   ElemsToWork:=0; ElemsWorked:=0;
   if SourceIsSelected then
      ElemsToWork:=Selected_StackSize
   else
   begin
      for i:=0 to Document.Layers.Count-1 do
      begin
         aLayer:=Document.Layers.Items[i];
         if SourceLayerList.IndexOf(aLayer.Layername) >= 0 then
            ElemsToWork:=ElemsToWork+aLayer.Count;
      end;
   end;

   if SourceIsSelected then
   begin
      aLayer:=nil;
      for i:=0 to Selected_StackSize-1 do
      begin
         if Selected_Stack[i] < 0 then
         begin
            // we got a layer
            aIndex:=(aIndex+ot_Layer)*-1;
            aLayer:=Document.Layers.GetLayerByName(Selected_StackLayernames[aIndex]);
            if SourceLayerList.IndexOf(aLayer.Layername) < 0 then
               aLayer:=nil; // <- do not process
         end
         else if aLayer <> nil then
         begin
            aBase:=Document.GetObjectByIndex(Selected_Stack[i]);
            SplitObjectToLayer(aBase, aTargetLayer, SplitMask);
         end;
         ElemsWorked:=ElemsWorked+1;
         ShowPercent(ElemsToWork, ElemsWorked);
      end;
   end
   else
   begin
      for i:=0 to Document.Layers.Count-1 do
      begin
         aLayer:=Document.Layers.Items[i];
         if SourceLayerList.IndexOf(aLayer.Layername) < 0 then
            continue;
         for j:=0 to aLayer.Count-1 do
         begin
            aBase:=aLayer.Items[j];
            SplitObjectToLayer(aBase, aTargetLayer, SplitMask);
            ElemsWorked:=ElemsWorked+1;
            ShowPercent(ElemsToWork, ElemsWorked);
         end;
      end;
   end;

   if SourceIsSelected then
   begin
      Selected_StackSize:=0;
      SetLength(Selected_Stack, Selected_StackSize);
      if Selected_StackLayernames <> nil then
         Selected_StackLayernames.Destroy;
   end;

   ShowPercent(ElemsToWork, ElemsToWork); // Show 100 percent
   FEvents.OnDisplayPass(StrfileObj.Assign(12)); // Split of Objects finished
   SourceLayerList.Destroy;
   StrFileObj.Destroy;
end;

procedure TAXGisObjMan.IAXGisObjMan__Set_Font(var Value: IFontDisp);
begin
  SetOleFont(FDelphiControl.Font, Value);
end;

procedure TAXGisObjMan.IAXGisObjMan_Set_Font(const Value: IFontDisp);
begin
  SetOleFont(FDelphiControl.Font, Value);
end;

initialization
  TActiveXControlFactory.Create(
    ComServer,
    TAXGisObjMan,
    TPanel,
    Class_AXGisObjMan,
    1,
    '',
    OLEMISC_SIMPLEFRAME or OLEMISC_ACTSLIKELABEL,
    tmApartment);
end.
