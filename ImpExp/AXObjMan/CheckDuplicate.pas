unit CheckDuplicate;

interface
uses SysUtils, AXDLL_TLB;

const
       // Object types
       ot_Pixel     = 1;
       ot_Poly      = 2;
       ot_LabelText = 3;
       ot_CPoly     = 4;
       ot_Group     = 5;
       ot_Proj      = 6;
       ot_Text      = 7;
       ot_Symbol    = 8;
       ot_Spline    = 9;
       ot_Image     = 10;
       ot_MesLine   = 11;
       ot_Circle    = 12;
       ot_Arc       = 13;
       ot_OLEObj    = 14;
       ot_SymDef    = 15;
       ot_RText     = 16;
       ot_BusGraph  = 17;
       ot_Layer     = 18;

type TCheckDuplicate = class
   public
      // public variables
      App:OleVariant;       // pointer to DX object
      Document:IDocument;   // pointer to current document
      DoCheckId:boolean;    // flag that defines if also the ID should be checked

      // properties for check-duplicate routine
      OriCheckObject:Variant;
      OldCheckObject:Variant;

      // list of id's
      ObjectIdList:array of integer;
      NumObjectsOfIdList:integer;

      // public functions
      constructor Create;
      destructor Destroy;
      function CheckIdHasDuplicateOnLayer(id:integer; layer:string):integer;
      function CheckIsDuplicate(var oriObject:IBase; var checkObject:IBase):boolean;
      procedure GetIdsOfTouchingCPolysOnLayer(id:integer; layer:string);
   private
      function CheckObjectHasDuplicateOnLayer(var aObject:IBase; Layer:string):integer;
      function CheckIsDuplicate_Pixel(var Object1:IBase; var Object2:IBase):boolean;
      function CheckIsDuplicate_Symbol(var Object1:IBase; var Object2:IBase):boolean;
      function CheckIsDuplicate_CPoly(var Object1:IBase; var Object2:IBase):boolean;
      function CheckIsDuplicate_Poly(var Object1:IBase; var Object2:IBase):boolean;
      function CheckIsDuplicate_Text(var Object1:IBase; var Object2:IBase):boolean;
      function CheckIsDuplicate_Circle(var Object1:IBase; var Object2:IBase):boolean;
      function AreaCPolyCornersEqual(var Object1:IPolygon; var Object2:IPolygon):boolean;
      function CheckIsDuplicate_ObjectStyle(var Object1:IBase; var Object2:IBase):boolean;
end;

implementation

constructor TCheckDuplicate.Create;
begin
   App:=OleVariant(0);
end;

destructor TCheckDuplicate.Destroy;
begin
   App:=OleVariant(0);
   // destroy the ObjectIdList
   SetLength( ObjectIdList, 0);
end;

// method is used to get all id's of CPolys that are on specified layer
// which touch the CPoly that is specified by it's Id
// the list of Id's is stored in ObjectIdList
procedure TCheckDuplicate.GetIdsOfTouchingCPolysOnLayer(id:integer; layer:string);
var aObjectDisp:IDispatch;
    aLayerDisp :IDispatch;
    aObject:IBase;
    ARect  :IRect;
    OldActiveLayer:string;
    cnt:integer;
    aSelObject:IBase;
    SelObjectid:integer;
begin
   NumObjectsOfIdList:=0;
   SetLength( ObjectIdList, 0);
   // first the object will be got by using its id
   aLayerDisp :=Document.Layers.GetLayerByName(Layer);
   aObjectDisp:=Document.GetObjectByIndex(id);
   if (aObjectDisp <> nil) and (aLayerDisp <> nil) then
   begin
      // id and layer exists
      aObject:=aObjectDisp as IBase;
      ARect:=aObject.ClipRect;
      // the layer that has to be checked has to be set as active layer
      OldActiveLayer:=Document.Layers.ActiveLayer.LayerName;
      Document.Layers.SetActiveLayer(Layer);
      // now do touch selection on active layer
      Document.SelectByRectTouch(ARect);

      // create list of the selected objects
      Document.CreateSelectedList;

      // now search the selected objects if they is a CPoly on that
      // object that has to be searched for is located
      for cnt:=0 to Document.NrOfSelectedObjects-1 do
      begin
         aSelObject:=Document.SelItems[cnt];
         if (aSelObject.ObjectType <> ot_Layer) and (aSelObject.ObjectType = ot_CPoly) then
         begin
            SelObjectid:=aSelObject.Index;
            // we found a CPoly that touches -> add it to list
            NumObjectsOfIdList:=NumObjectsOfIdList+1;
            SetLength( ObjectIdList, NumObjectsOfIdList);
            ObjectIdList[NumObjectsOfIdList-1]:=SelObjectid;
         end;
      end;
      Document.DestroySelectedList;
      // now all objects that have been selected can
      // be deselected
      Document.DeselectAll;
      Document.Layers.SetActiveLayer(OldActiveLayer);
   end;
end;

function TCheckDuplicate.CheckIdHasDuplicateOnLayer(id:integer; layer:string):integer;
var retVal:integer;
    aObjectDisp:IDispatch;
    aLayerDisp :IDispatch;
    aObject:IBase;
begin
   retVal:=0;
   // first the object will be got by using its id
   aLayerDisp :=Document.Layers.GetLayerByName(Layer);
   aObjectDisp:=Document.GetObjectByIndex(id);
   if (aObjectDisp <> nil) and (aLayerDisp <> nil) then
   begin
      // id and layer exists
      aObject:=aObjectDisp as IBase;
      retVal:=CheckObjectHasDuplicateOnLayer(aObject, layer);
   end;
   result:=retVal;
end;

function TCheckDuplicate.CheckObjectHasDuplicateOnLayer(var aObject:IBase; Layer:string):integer;
var ARect:IRect;
    retVal:integer;
    cnt:integer;
    aSelObject:IBase;
    SelObjectid:integer;
    aText      :IText;
    OldActiveLayer   :string;
begin
   retVal:=0;
   // now the cliprect of the Object has to be evaluated
   ARect:=aObject.ClipRect;

   // the layer that has to be checked has to be set as active layer
   OldActiveLayer:=Document.Layers.ActiveLayer.LayerName;
   Document.Layers.SetActiveLayer(Layer);
   // now the objects on the layer that has to be searched for will
   // be selected
   if aObject.ObjectType <> ot_Text then
      Document.SelectByRect(ARect)
   else
   begin
      // Text objects need to be handeled seperate by using the
      // SelectSearchText method
      aText:=aObject as IText;
      Document.SelectSearchText(aText.Text);
   end;

   // create list of the selected objects
   Document.CreateSelectedList;

   // now search the selected objects if they are a duplicate of the
   // object that has to be searched for
   for cnt:=0 to Document.NrOfSelectedObjects-1 do
   begin
      aSelObject:=Document.SelItems[cnt];
      if aSelObject.ObjectType <> ot_Layer then
      begin
         SelObjectid:=aSelObject.Index;
         if (CheckIsDuplicate(aObject, aSelObject)) then
         begin
            retVal:=SelObjectid;
            break;
         end;
      end;
   end;
   Document.DestroySelectedList;
   // now all objects that have been selected can
   // be deselected
   Document.DeselectAll;
   Document.Layers.SetActiveLayer(OldActiveLayer);
   result:=retVal;
end;

// function is used to check if two objects are equal
function TCheckDuplicate.CheckIsDuplicate(var oriObject:IBase; var checkObject:IBase):boolean;
var retVal:boolean;
    ignoreObject:boolean;
begin
   retVal:=FALSE; ignoreObject:=FALSE;
   if (oriObject.ObjectType = checkObject.ObjectType) then
   begin
      if oriObject.ObjectType = ot_Pixel then
         retVal:=CheckIsDuplicate_Pixel(oriObject, checkObject)
      else if oriObject.ObjectType = ot_Symbol then
         retVal:=CheckIsDuplicate_Symbol(oriObject, checkObject)
      else if oriObject.ObjectType = ot_Cpoly then
         retVal:=CheckIsDuplicate_CPoly(oriObject, checkObject)
      else if oriObject.ObjectType =  ot_poly then
         retVal:=CheckIsDuplicate_Poly(oriObject, checkObject)
      else if oriObject.ObjectType = ot_Text then
         retVal:=CheckIsDuplicate_Text(oriObject, checkObject)
      else if oriObject.ObjectType = ot_Circle then
         retVal:=CheckIsDuplicate_Circle(oriObject, checkObject)
      else
      begin
         // other object types will not be supported
         ignoreObject:=TRUE;
         retVal:=TRUE;
      end;

      if (retVal = TRUE) and (not ignoreObject) then
      begin
         // check also if the object-style is equal
         if OriObject.ObjectType <> ot_Text then // for texts the objectstyle will not be checked
            retVal:=CheckIsDuplicate_ObjectStyle(oriObject, checkObject);
      end;
   end;
   result:=retVal;
end;

function TCheckDuplicate.CheckIsDuplicate_ObjectStyle(var Object1:IBase; var Object2:IBase):boolean;
var retVal:boolean;
    ObjectStyle1:IObjectStyle;
    ObjectStyle2:IObjectStyle;
    color1, color2:integer;
    aPixel:IPixel;
    aSymbol:ISymbol;
    aCPoly:IPolygon;
    aPoly :IPolyline;
    aText :IText;
    aCircle:ICircle;
begin
   retVal:=FALSE;
   if Object1.ObjectType <> Object2.ObjectType then
   begin
      result:=retVal;
      exit;
   end;
   // first get the objectstyles
   ObjectStyle1:=nil;
   ObjectStyle2:=nil;
   if Object1.ObjectType = ot_Pixel then
   begin
      aPixel:=Object1 as IPixel;
      ObjectStyle1:=aPixel.ObjectStyle;
      aPixel:=Object2 as IPixel;
      ObjectStyle2:=aPixel.ObjectStyle;
   end
   else if Object1.ObjectType = ot_Symbol then
   begin
      aSymbol:=Object1 as ISymbol;
      ObjectStyle1:=aSymbol.ObjectStyle;
      aSymbol:=Object2 as ISymbol;
      ObjectStyle2:=aSymbol.ObjectStyle;
   end
   else if Object1.ObjectType = ot_Cpoly then
   begin
      aCPoly:=Object1 as IPolygon;
      ObjectStyle1:=aCPoly.ObjectStyle;
      aCPoly:=Object2 as IPolygon;
      ObjectStyle2:=aCPoly.ObjectStyle;
   end
   else if Object1.ObjectType =  ot_poly then
   begin
      aPoly:=Object1 as IPolyline;
      ObjectStyle1:=aPoly.ObjectStyle;
      aPoly:=Object2 as IPolyline;
      ObjectStyle2:=aPoly.ObjectStyle;
   end
   else if Object1.ObjectType = ot_Text then
   begin
      aText:=Object1 as IText;
      ObjectStyle1:=aText.ObjectStyle;
      aText:=Object2 as IText;
      ObjectStyle2:=aText.ObjectStyle;
   end
   else if Object1.ObjectType = ot_Circle then
   begin
      aCircle:=Object1 as ICircle;
      ObjectStyle1:=aCircle.ObjectStyle;
      aCircle:=Object2 as ICircle;
      ObjectStyle2:=aCircle.ObjectStyle;
   end;


   // first check if both objects have an objectstyle
   if (ObjectStyle1 = nil) and (ObjectStyle2 = nil) then
   begin
      // both objects have no objectstyle
      retVal:=TRUE;
   end;
   if (ObjectStyle1 <> nil) and (ObjectStyle2 <> nil) then
   begin
      // both objects have objectstyle
      // now it has to be checked if the objectstyle is equal
      retVal:=TRUE;
      if (ObjectStyle1.LineStyle.Style <> ObjectStyle2.LineStyle.Style) then retVal:=FALSE;

      color1:=ObjectStyle1.LineStyle.Color; color2:=ObjectStyle2.LineStyle.Color;
      if (color1 <> color2) then retVal:=FALSE;

      color1:=ObjectStyle1.LineStyle.FillColor; color2:=ObjectStyle2.LineStyle.FillColor;
      if (color1 <> color2) then retVal:=FALSE;

      if (ObjectStyle1.FillStyle.Pattern <> ObjectStyle2.FillStyle.Pattern) then retVal:=FALSE;

      color1:=ObjectStyle1.FillStyle.ForeColor; color2:=ObjectStyle2.FillStyle.ForeColor;
      if (color1 <> color2) then retVal:=FALSE;

      color1:=ObjectStyle1.FillStyle.BackColor; color2:=ObjectStyle2.FillStyle.BackColor;
      if (color1 <> color2) then retVal:=FALSE;
   end;
   result:=retVal;
end;

function TCheckDuplicate.CheckIsDuplicate_Pixel(var Object1:IBase; var Object2:IBase):boolean;
var retVal:boolean;
    Pixel1, Pixel2:IPixel;
begin
   // function is used to check if Object2 is a duplicate of Object1
   retVal:=FALSE;
   if (Object1.Index <> Object2.Index) or (DoCheckId = FALSE) then
   begin
      Pixel1:=Object1 as IPixel;
      Pixel2:=Object2 as IPixel;
      if ((Pixel1.Position.X = Pixel2.Position.X) and
         (Pixel1.Position.Y = Pixel2.Position.Y)) then
         retVal:=TRUE;
   end;
   result:=retVal;
end;

function TCheckDuplicate.CheckIsDuplicate_Symbol(var Object1:IBase; var Object2:IBase):boolean;
var retVal:boolean;
    Symbol1, Symbol2:ISymbol;
begin
   // function is used to check if Object2 is a duplicate of Object1
   retVal:=FALSE;
   if (Object1.Index <> Object2.Index) or (DoCheckId = FALSE) then
   begin
      Symbol1:=Object1 as ISymbol;
      Symbol2:=Object2 as ISymbol;
      if ((Symbol1.Position.X = Symbol2.Position.X) and
         (Symbol1.Position.Y = Symbol2.Position.Y) and
         (Symbol1.Size = Symbol2.Size) and
         (Symbol1.SizeType = Symbol2.SizeType) and
         (Symbol1.Angle = Symbol2.Angle) and
         (Symbol1.SymIndex = Symbol2.SymIndex)) then
         retVal:=TRUE;
   end;
   result:=retVal;
end;

function TCheckDuplicate.CheckIsDuplicate_CPoly(var Object1:IBase; var Object2:IBase):boolean;
var retVal:boolean;
    CPoly1, CPoly2:IPolygon;
begin
   // function is used to check if Object2 is a duplicate of Object1
   retVal:=FALSE;
   if (Object1.Index <> Object2.Index) or (DoCheckId = FALSE) then
   begin
      CPoly1:=Object1 as IPolygon;
      CPoly2:=Object2 as IPolygon;
      // first check if they have the same area
      if (CPoly1.Area = CPoly2.Area) then
      begin
         // now check if they have the same rectangles
         if ((CPoly1.ClipRect.X1 = CPoly2.ClipRect.X1) and
            (CPoly1.ClipRect.Y1 = CPoly2.ClipRect.Y1) and
            (CPoly1.ClipRect.X2 = CPoly2.ClipRect.X2) and
            (CPoly1.ClipRect.Y2 = CPoly2.ClipRect.Y2)) then
         begin
            // both objects have same cliprect
            // now the corners have to be checked
            if CPoly1.Count = CPoly2.Count then
            begin
               retVal:=TRUE;

               // if both object have equal number of corners
               // the corners itself have to be checked
               // retVal:=AreaCPolyCornersEqual(Object1, Object2);
            end;
         end;
      end;
   end;
   result:=retVal;
end;

function TCheckDuplicate.AreaCPolyCornersEqual(var Object1:IPolygon; var Object2:IPolygon):boolean;
var retVal:boolean;
    Cnt1,Cnt2:integer;
    APoint1,APoint2:Variant;
    found:boolean;
begin
   // function is used to check if two CPolys have equal corners
   retVal:=TRUE;
   for Cnt1:=0 to Object1.Count - 1 do
   begin
      APoint1:=Object1.Points[Cnt1];
      // check if this point exists in Object2
      found:=FALSE;
      for Cnt2:=0 to Object1.Count - 1 do
      begin
         APoint2:=Object2.Points[Cnt2];
         if ((APoint1.X = APoint2.X) and
            (APoint1.Y = APoint2.Y)) then
         begin
            found:=TRUE;
            break;
         end;
      end;
      if (found = FALSE) then
      begin
         retVal:=FALSE;
         break;
      end;
   end;
   result:=retVal;
end;

function TCheckDuplicate.CheckIsDuplicate_Poly(var Object1:IBase; var Object2:IBase):boolean;
var retVal:boolean;
    i:integer;
    APoint1, APoint2:IPoint;
    Poly1, Poly2:IPolyline;
begin
   // function is used to check if Object2 is a duplicate of Object1
   retVal:=FALSE;
   if (Object1.Index <> Object2.Index) or (DoCheckId = FALSE) then
   begin
      Poly1:=Object1 as IPolyline;
      Poly2:=Object2 as IPolyline;
      // first check if they have the same length
      if (Poly1.Length = Poly2.Length) then
      begin
         // now it will be checked if the corners are equal
         if (Poly1.Count = Poly2.Count) then
         begin
            retVal:=TRUE;
            for i:=0 to Poly1.Count-1 do
            begin
               APoint1:=Poly1.Points[i]; APoint2:=Poly2.Points[i];
               if ((APoint1.X <> APoint2.X) or (APoint1.Y <> APoint2.Y)) then
               begin
                  retVal:=FALSE;
                  break;
               end;
            end;
         end;
      end;
   end;
   result:=retVal;
end;

function TCheckDuplicate.CheckIsDuplicate_Text(var Object1:IBase; var Object2:IBase):boolean;
var retVal:boolean;
    Text1, Text2:IText;
begin
   // function is used to check if Object2 is a duplicate of Object1
   retVal:=FALSE;
   if (Object1.Index <> Object2.Index) or (DoCheckId = FALSE) then
   begin
      Text1:=Object1 as IText;
      Text2:=Object2 as IText;
      // first check if they have the same position
      if (Text1.Position.X = Text2.Position.X) and
         (Text1.Position.Y = Text2.Position.Y) then
      begin
         if (Text1.Text = Text2.Text) then
            if (Text1.Angle = Text2.Angle) then
               if (Text1.FontHeight = Text2.FontHeight) then
                  if (Text1.FontStyle = Text2.FontStyle) then
                     if (Text1.FontName = Text2.FontName) then
                        retVal:=TRUE;
      end;
   end;
   result:=retVal;
end;

function TCheckDuplicate.CheckIsDuplicate_Circle(var Object1:IBase; var Object2:IBase):boolean;
var retVal:boolean;
    Circle1, Circle2:ICircle;
begin
   // function is used to check if Object2 is a duplicate of Object1
   retVal:=FALSE;
   if (Object1.Index <> Object2.Index) or (DoCheckId = FALSE) then
   begin
      Circle1:=Object1 as ICircle;
      Circle2:=Object2 as ICircle;
      if (Circle1.Position.X = Circle2.Position.X) and
         (Circle1.Position.Y = Circle2.Position.Y) then
      begin
         if (Circle1.Angle = Circle2.Angle) then
            if (Circle1.PrimaryAxis = Circle2.PrimaryAxis) then
               if (Circle1.SecondaryAxis = Circle2.SecondaryAxis) then
                  if (Circle1.Area = Circle2.Area) then
                     if (Circle1.Radius = Circle2.Radius) then
                        retVal:=TRUE;
      end;
   end;
   result:=retVal;
end;
end.
