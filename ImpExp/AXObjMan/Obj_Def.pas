unit Obj_Def;

interface

const
       // Object types
       ot_Pixel     = 1;
       ot_Poly      = 2;
       ot_CPoly     = 4;
       ot_Text      = 7;
       ot_Symbol    = 8;
       ot_Spline    = 9;
       ot_Image     = 10;
       ot_MesLine   = 11;
       ot_Circle    = 12;
       ot_Arc       = 13;

             //TextSTyles
       ts_Left           =  0;
       ts_Center         =  1;
       ts_Right          =  2;
       ts_Italic         =  4;
       ts_Bold           =  8;
       ts_Underl         = 16;
       ts_FixHeight      = 32;
       ts_Transparent    = 64;

       //LineTypes
       lt_Solid     = 0;
       lt_Dash      = 1;
       lt_Dot       = 2;
       lt_DashDot   = 3;
       lt_DashDDot  = 4;
       lt_UserDefined    = 1000;

       //FillTypes
       pt_NoPattern = 0;
       pt_FDiagonal = 1;
       pt_Cross     = 2;
       pt_DiagCross = 3;
       pt_BDiagonal = 4;
       pt_Horizontal= 5;
       pt_Vertical  = 6;
       pt_Solid     = 7;
       pt_UserDefined = 8;

       stDrawingUnits    = 0;
       stScaleInDependend= 1;
       stProjectScale    = 2;
       stPercent         = 3;

       // Rectangle Overlap types
       No_Overlap     = 0;    // rectangles do not overlap
       Own_Inside     = 1;    // own rectangle is inside the other
       Other_Inside   = 2;    // other rectangle is inside the own
       Do_Overlap     = 3;    // rectangles do overlap

       // Combine Polygon parts types
       No_Combine     = 0;
       Remove_Other   = 1;
       Remove_Own     = 2;
       Remove_Both    = 3;
type
  // structure for point
  pcpoint = record
      x     :double;
      y     :double;
  end;

  // record structure for corner of PolygonPart
  p_Point_record=^Point_record;
  Point_record = record
      Point          :pcpoint;         // point itself
      addedforprocess:boolean;         // flag if point has been added temporary for processing
      found          :boolean;         // flag if point has been found (temporary for processing)
      next           :p_Point_record;
      prev           :p_Point_record;
  end;

  // structure for combine result of PolygonParts
  p_CombineResult_record=^CombineResult_record;
  CombineResult_record = record
     FirstCorner:p_Point_record;          // Pointer to list of corners
     LastCorner :p_Point_record;
     NumCorners :integer;
     PolyType   :integer;                 // Type of Polygon (Main/Island/OuterIsland)
     Area       :double;                  // Area of Polygon
  end;

  // structure for Connectionline for combine PolygonParts
  p_ConnectionLine_record=^ConnectionLine_record;
  ConnectionLine_record = record
     Start_IdxOwn  :integer;
     Start_IdxOther:integer;
     End_IdxOwn    :integer;
     End_IdxOther  :integer;
     OtherDir      :integer;
     ResultPolyType:integer;
     next          :p_ConnectionLine_record;
     prev          :p_ConnectionLine_record;
  end;



var FSnapRadius:double;   // public Snap-Radius for whole control
    FDoDebug   :boolean;
    FWorkingDir:string;
    EqualPartIsAlsoInside:boolean;

implementation


end.
