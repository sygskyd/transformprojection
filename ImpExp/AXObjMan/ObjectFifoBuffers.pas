unit ObjectFifoBuffers;

interface
uses Obj_Def;

procedure Init_Fifo_Buffers;
procedure Release_Fifo_Buffers;
function  Request_p_Point_record:p_Point_record;
procedure Release_p_Point_record(var other:p_Point_record);

implementation

// point records get Fifo-Buffer due to they allocated and removed very often
var Fifo_p_Point_record     :array of p_Point_record; // Fifo buffer for p_Point_record
    Num_Fifo_p_Point_record:integer;

// init Fifo buffers
procedure Init_Fifo_Buffers;
begin
   Num_Fifo_p_Point_record:=0;
   SetLength(Fifo_p_Point_record, 0);
end;

// release Fifo Buffers
procedure Release_Fifo_Buffers;
var i:integer;
    iterate_p_Point_record:p_Point_record;
begin
   if Num_Fifo_p_Point_record > 0 then
   begin
      for i:=0 to Num_Fifo_p_Point_record-1 do
      begin
         iterate_p_Point_record:=Fifo_p_Point_record[i];
         dispose(iterate_p_Point_record);
      end;
      SetLength(Fifo_p_Point_record, 0);
   end;
end;

// request p_Point_record
function Request_p_Point_record:p_Point_record;
var retVal:p_Point_record;
begin
   if Num_Fifo_p_Point_record > 0 then
   begin
      // get p_Point_record from Fifo buffer
      retVal:=Fifo_p_Point_record[Num_Fifo_p_Point_record-1];
      Fifo_p_Point_record[Num_Fifo_p_Point_record-1]:=nil;
      Num_Fifo_p_Point_record:=Num_Fifo_p_Point_record-1;
      SetLength(Fifo_p_Point_record, Num_Fifo_p_Point_record);
   end
   else
   begin
      // due to no entry is in Fifo buffer a new entry has to be created
      retVal:=new(p_Point_record);
   end;
   result:=retVal;
end;

// release p_Point_record
procedure Release_p_Point_record(var other:p_Point_record);
begin
   // add p_Point_record to Fifo buffer
   Num_Fifo_p_Point_record:=Num_Fifo_p_Point_record+1;
   SetLength(Fifo_p_Point_record, Num_Fifo_p_Point_record);
   Fifo_p_Point_record[Num_Fifo_p_Point_record-1]:=other;
end;

end.
