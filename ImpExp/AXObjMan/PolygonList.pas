unit PolygonList;

interface
uses Polygon;

// define structure to store Polygon Parts that belong to Polygon
type p_Polygon_record=^Polygon_record;
     Polygon_record = record
        Poly     :TPolygon;            // Pointer to Polygon
        prev     :p_Polygon_record;
        next     :p_Polygon_record;
     end;

type
  TPolygonList = class // class to store list of polygons
  public
     constructor Create;                              // constructor of class
     destructor  Destroy;                             // destructor of class
     procedure   Add(var aPoly:TPolygon);             // Add Polygon to list
     procedure   Remove(var aPoly:p_Polygon_record);  // Remove Polygon from list
     function    GetFirst:p_Polygon_record;           // Get Pointer to first Polygon
     function    GetNumEntries:integer;               // return size of list
  private
     FirstPoly:p_Polygon_record;
     LastPoly :p_Polygon_record;
     NumElems :integer;
  end;

implementation

constructor TPolygonList.Create;
begin
   FirstPoly:=nil;
   LastPoly:=nil;
   NumElems:=0;
end;

destructor TPolygonList.Destroy;
var iterate1:p_Polygon_record;
    iterate2:p_Polygon_record;
begin
   iterate1:=FirstPoly; iterate2:=FirstPoly;
   while (iterate1 <> nil) do
   begin
      iterate2:=iterate1;
      iterate1:=iterate1^.next;
      if iterate2^.Poly <> nil then iterate2^.Poly.Destroy;
      dispose(iterate2);
   end;
   FirstPoly:=nil;
   LastPoly:=nil;
   NumElems:=0;
end;

procedure TPolygonList.Add(var aPoly:TPolygon);
var newentry:p_Polygon_record;
begin
   newentry:=new(p_Polygon_record);
   newentry^.Poly:=aPoly;
   newentry^.prev:=nil;
   newentry^.next:=nil;
   if FirstPoly = nil then
   begin
      FirstPoly:=newentry;
      LastPoly:=newentry;
   end
   else
   begin
      newentry^.prev:=LastPoly;
      LastPoly^.next:=newentry;
      LastPoly:=newentry;
   end;
   NumElems:=NumElems+1;
end;

procedure TPolygonList.Remove(var aPoly:p_Polygon_record);
begin
   if aPoly^.prev = nil then // its the first polygon in list
   begin
      FirstPoly:=aPoly^.next;
      if FirstPoly <> nil then FirstPoly^.prev:=nil;

      // delete aPoly
      if aPoly^.Poly <> nil then aPoly^.Poly.Destroy;
      dispose(aPoly);

      NumElems:=NumElems-1;
   end
   else if aPoly^.next = nil then // its the last polygon in list
   begin
      LastPoly:=aPoly^.prev;
      LastPoly^.next:=nil;

      // delete aPoly
      if aPoly^.Poly <> nil then aPoly^.Poly.Destroy;
      dispose(aPoly);

      NumElems:=NumElems-1;
   end
   else
   begin // it�s in the middle of the list
      aPoly^.next^.prev:=aPoly^.prev;
      aPoly^.prev^.next:=aPoly^.next;

      // delete aPoly
      if aPoly^.Poly <> nil then aPoly^.Poly.Destroy;
      dispose(aPoly);
      NumElems:=NumElems-1;
   end;
end;

function TPolygonList.GetFirst:p_Polygon_record;
begin
   result:=FirstPoly;
end;

function TPolygonList.GetNumEntries:integer;
begin
   result:=NumElems;
end;

end.
