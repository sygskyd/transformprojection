unit Polygon;

interface

uses PolygonPart, Rectangle, AXDLL_TLB;

// define structure to store Polygon Parts that belong to Polygon
type p_PolygonPart_record=^Polygon_Part_record;
     Polygon_Part_record = record
        PolyPart :TPolygonPart; // Pointer to Polygon Part
        prev     :p_PolygonPart_record;
        next     :p_PolygonPart_record;
     end;

type p_PolygonPart_nesting=^PolygonPart_nesting;
     PolygonPart_nesting = record
        nesting     : byte;    // nesting of part
        envelopeidx : integer; // part index of envelope polygon part
     end;

type
  TPolygon = class // class to store polygon object
  public
     NumInnerIslands:integer;            // count number of inner islands (holes)
     constructor Create;                                                  // constructor of class
     destructor  Destroy;                                                 // destructor of class
     function    IsInside(X:double; Y:double):TPolygonPartType; overload; // Method to check if point is inside polygon
     function    IsInside(var other:TPolygon):boolean; overload;          // Method to check if own polyogn is inside other
     function    Combine(var other:TPolygon; HandleHoles:boolean):boolean;// Method to combine two polygons
     procedure   AddParts(var other:TPolygon);                            // Method to add all parts from other polygon to own
     procedure   AddPartsForCreateIslandLayer(var other:TPolygon);                   // Method to add parts from other to own parts as Islands
     procedure   Read(var aPoly:IPolygon);                                // Method to read polygon
     function    WriteToLayer(var aDocument:IDocument; var aLayer:ILayer):integer; // Method to write polygon
     procedure   WriteDebugInfo;
  private
     Rectangle   :TRectangle;
     FirstPart   :p_PolygonPart_record;
     LastPart    :p_PolygonPart_record;
     NumParts    :integer;
     ProgisId    :integer;

     NestingWriteFilter:byte;                            // Filter for write the Polygon
     IslandPartsNestings:array of p_PolygonPart_nesting; // store nesting of parts of Polygon parts

     function  AddPolygonPart(var PolyPart:TPolygonPart):boolean;
     procedure SetupRectangle;
     procedure RemovePartByIndex(aIndex:integer);
     procedure DeleteParts;
  end;

implementation

uses Obj_Def, SysUtils;

constructor TPolygon.Create;
begin
   Rectangle:=nil;
   FirstPart:=nil;
   LastPart:=nil;
   NumParts:=0;
   NumInnerIslands:=0;
   NestingWriteFilter:=255; // set maximum allowed nesting as default
end;

destructor TPolygon.Destroy;
var iterate1, iterate2:p_PolygonPart_record;
    i:integer;
begin
   if Rectangle <> nil then
      Rectangle.Destroy;
   // now destroy the Polygon Parts
   iterate1:=FirstPart; iterate2:=FirstPart;
   while (iterate1 <> nil) do
   begin
      iterate2:=iterate1;
      iterate1:=iterate1^.next;
      // if Parts already belong to other Polygon as Islands they may not be removed
      // they will be removed in destructor of other Polygon
      if (iterate2^.PolyPart <> nil) and (iterate2^.PolyPart.CreateIslandAssignements = 0) then
      begin
         iterate2^.PolyPart.Destroy;
         iterate2^.PolyPart:=nil;
      end
      else
      begin
         if iterate2^.PolyPart <> nil then
            iterate2^.PolyPart.CreateIslandAssignements:=iterate2^.PolyPart.CreateIslandAssignements-1;
         iterate2^.PolyPart:=nil;
      end;
      if iterate2^.PolyPart <> nil then iterate2^.PolyPart.Destroy; // destroy the PolyPart itself
      dispose(iterate2);
   end;
   FirstPart:=nil;
   LastPart:=nil;

   if NumParts > 0 then
   begin
      for i:=0 to NumParts -1 do
         dispose(IslandPartsNestings[i]);
   end;
   SetLength(IslandPartsNestings, 0);
end;

// ------------------------------------------------------------------------
// NAME: AddPolygonPart
// ------------------------------------------------------------------------
// DESCRIPTION : Method to add new Polygon Part to Polygon
//
// PARAMETERS  : PolyPart -> New Polygon Part that should be added
//
// RETURNVALUE : TRUE -> If Polygon part could be inserted, else FALSE
// ------------------------------------------------------------------------
function TPolygon.AddPolygonPart(var PolyPart:TPolygonPart):boolean;
var newpart:p_PolygonPart_record;
    iterate:p_PolygonPart_record;
    CurrEnvelopePart:p_PolygonPart_record;
    NewPartType:TPolygonPartType;
    retVal:boolean;
    docheck:boolean;
    partidx:integer;
    EnvelopePartidx:integer;

    NewPartnesting:byte;
    NewPartsParent:integer;
    nestedparts:array of integer;
    numnestedparts:integer;

    newnesting:p_PolygonPart_nesting;
begin
   retVal:=FALSE;
   result:=retVal;
   if PolyPart = nil then exit;

   PolyPart.CalculateArea;
   if PolyPart.GetArea = 0 then exit; // do not insert polygon parts that have no area

   iterate:=FirstPart;
   while (iterate <> nil) do // first check if part already exists (in this case part needs not to be inserted again)
   begin
      // if the new Polygon Part is equal it has not to be inserted
      if iterate^.PolyPart.IsEqual(PolyPart) then
         exit;
      iterate:=iterate^.next;
   end;

   // new part always has to be inserted before largest polygon to which is it self the envelope
   // (needed to create envelope polygon parts before the Islands)

   // first evaluate the type of the new PolyPart
   numnestedparts:=0;
   iterate:=FirstPart; NewPartType:=Main; CurrEnvelopePart:=nil; EnvelopePartidx:=-1; partidx:=0;
   NewPartnesting:=0; NewPartsParent:=-1; // as default nesting of new part is 0, and no parent (envelope exists)
   while iterate <> nil do
   begin
      // check if own polygon part is inside of new polygon part
      if (iterate^.PolyPart.GetArea < PolyPart.GetArea) then
      begin
         if (iterate^.PolyPart.IsInside(PolyPart)) then
         begin
            // own part is inside other -> Setup the type
            if iterate^.PolyPart.GetPolygonPartType = Main then // own polygon part becomes island
            begin
               iterate^.PolyPart.SetPolygonPartType(Island);
               NumInnerIslands:=NumInnerIslands+1;
            end
            else if iterate^.PolyPart.GetPolygonPartType = Island then // own polygon part becomes outer island
            begin
               iterate^.PolyPart.SetPolygonPartType(OuterIsland);
               NumInnerIslands:=NumInnerIslands-1;
            end
            else
            begin
               iterate^.PolyPart.SetPolygonPartType(Island);
               NumInnerIslands:=NumInnerIslands+1;
            end;

            // store parts that are within new part to process nesting
            SetLength(nestedparts, numnestedparts+1);
            nestedparts[numnestedparts]:=partidx;
            numnestedparts:=numnestedparts+1;

            if numnestedparts = 1 then // we got the first part that is within new part
               NewPartnesting:=IslandPartsNestings[partidx]^.nesting
            else if (IslandPartsNestings[partidx]^.nesting < NewPartnesting) then
               NewPartnesting:=IslandPartsNestings[partidx]^.nesting;
            IslandPartsNestings[partidx]^.nesting:=IslandPartsNestings[partidx]^.nesting+1;
         end;
      end
      else
      begin
         if (CurrEnvelopePart <> nil) and (CurrEnvelopePart^.PolyPart.GetArea < iterate^.PolyPart.GetArea) then
         begin
            iterate:=iterate^.next; partidx:=partidx+1;
            continue; // due to current envelope is smaller than current own part it has not to be checked
         end;
         if (PolyPart.IsInside(iterate^.PolyPart)) then
         begin
            if CurrEnvelopePart = nil then
            begin
               CurrEnvelopePart:=iterate;
               EnvelopePartidx:=partidx;
            end
            else
            begin
               // check if the area of the iterate PolygonPart is smaller than
               // the area of the CurrEnvelopePart
               if (CurrEnvelopePart^.PolyPart.GetArea > iterate^.PolyPart.GetArea) then
               begin
                  CurrEnvelopePart:=iterate;
                  EnvelopePartidx:=partidx;
               end;
            end;
         end;
      end;
      iterate:=iterate^.next; partidx:=partidx+1;
   end;

   if CurrEnvelopePart <> nil then
   begin
      if CurrEnvelopePart^.PolyPart.GetPolygonPartType = Main then
      begin
         NewPartType:=Island;
         NumInnerIslands:=NumInnerIslands+1;
      end
      else if CurrEnvelopePart^.PolyPart.GetPolygonPartType = Island then
      begin
         NewPartType:=OuterIsland;
         NumInnerIslands:=NumInnerIslands-1;
      end
      else if CurrEnvelopePart^.PolyPart.GetPolygonPartType = OuterIsland then
      begin
         NewPartType:=Island;
         NumInnerIslands:=NumInnerIslands+1;
      end;
      NewPartnesting:=IslandPartsNestings[EnvelopePartidx]^.nesting+1; // due to new part is within Envelope it gets increased nesting
   end;
   PolyPart.SetPolygonPartType(NewPartType);
   newpart:=new(p_PolygonPart_record);
   newpart^.PolyPart:=PolyPart;
   newpart^.prev:=nil;
   newpart^.next:=nil;
   if FirstPart = nil then // first polygon part inserted
   begin
      FirstPart:=newpart;
      LastPart:=newpart;
   end
   else
   begin
      newpart^.prev:=LastPart;
      LastPart^.next:=newpart;
      LastPart:=newpart;
   end;
   NumParts:=NumParts+1;

   // now process the nesting
   SetLength(IslandPartsNestings, NumParts);
   newnesting:=new(p_PolygonPart_nesting);
   newnesting^.nesting:=NewPartnesting;
   newnesting^.envelopeidx:=EnvelopePartidx;
   IslandPartsNestings[NumParts-1]:=newnesting;

   // now process the nesting of the nested parts
   if numnestedparts > 0 then
   begin
      for partidx:=0 to numnestedparts-1 do
      begin
         if (IslandPartsNestings[nestedparts[partidx]]^.nesting = NewPartnesting+1) then
            IslandPartsNestings[nestedparts[partidx]]^.envelopeidx:=NumParts-1; // part has new part as envelope
      end;
   end;

   numnestedparts:=0;
   SetLength(nestedparts, numnestedparts);

   retVal:=TRUE;
   // setup the rectangle
   SetupRectangle;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: IsInside
// ------------------------------------------------------------------------
// DESCRIPTION : Method to check if Point is inside Polygon
//
// PARAMETERS  : X -> X-Coordinate of Point
//               Y -> Y-Coordinate of Point
//
// RETURNVALUE : Type of the Polygon in that Point is (None if not inside)
// ------------------------------------------------------------------------
function TPolygon.IsInside(X:double; Y:double):TPolygonPartType;
var retVal:TPolygonPartType;
    iterate:p_PolygonPart_record;
    CurrEnvelopePart:p_PolygonPart_record;
    docheck:boolean;
begin
   retVal:=None;
   iterate:=FirstPart; CurrEnvelopePart:=nil;
   while iterate <> nil do
   begin
      docheck:=TRUE;
      if (CurrEnvelopePart <> nil) and (CurrEnvelopePart^.PolyPart.GetArea < iterate^.PolyPart.GetArea) then
         docheck:=FALSE; // due to current Envelope Area is smaller than the current iterate Area it has not to be checked

      if (docheck and iterate^.PolyPart.IsInside(X,Y)) then
      begin
         if CurrEnvelopePart = nil then
            CurrEnvelopePart:=iterate
         else
         begin
            // check if the area of the iterate PolygonPart is smaller than
            // the area of the CurrEnvelopePart
            if (CurrEnvelopePart^.PolyPart.GetArea > iterate^.PolyPart.GetArea) then
               CurrEnvelopePart:=iterate;
         end;
      end;
      iterate:=iterate^.next;
   end;
   if CurrEnvelopePart <> nil then
   begin
      retVal:=CurrEnvelopePart^.PolyPart.GetPolygonPartType;
   end
   else
      retVal:=None;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: SetupRectangle
// ------------------------------------------------------------------------
// DESCRIPTION : Method to setup current rectangle
//
// PARAMETERS  : NONE
//
// RETURNVALUE : NONE
// ------------------------------------------------------------------------
procedure TPolygon.SetupRectangle;
var iterate:p_PolygonPart_record;
begin
   if Rectangle <> nil then
      Rectangle.Destroy;
   Rectangle:=TRectangle.Create;
   iterate:=FirstPart;
   while (iterate <> nil) do
   begin
      if (iterate^.PolyPart <> nil) then
         Rectangle.SetupRect(iterate^.PolyPart.GetRectangle);
      iterate:=iterate^.next;
   end;
end;

// ------------------------------------------------------------------------
// NAME: IsInside
// ------------------------------------------------------------------------
// DESCRIPTION : Method to check if own polygon is inside other
//
// PARAMETERS  : other -> Polygon that should be checked
//
// RETURNVALUE : TRUE -> if own Polygon is inside other, else FALSE
// ------------------------------------------------------------------------
function TPolygon.IsInside(var other:TPolygon):boolean;
var retVal:boolean;
    iterate_own  :p_PolygonPart_record;
    iterate_other:p_PolygonPart_record;
    hadinsidemode:integer;
    lastinsidepart:p_PolygonPart_record;
begin
   retVal:=FALSE;
   result:=FALSE;
   if (NumParts < 1) or (other.NumParts < 1) then exit;

   if Rectangle.GetOverLapType(other.Rectangle) = No_Overlap then exit;
   // all Polygon parts of own Polygon must be at least inside of one in other polygon
   iterate_own:=FirstPart;
   retVal:=TRUE;
   while (iterate_own <> nil) and (retVal = TRUE) do
   begin
      lastinsidepart:=nil;
      hadinsidemode:=0; // 0 means not inside, 1 means inside (but search also other polygon parts), 2 means inside
      iterate_other:=other.FirstPart;
      while (iterate_other <> nil) and (hadinsidemode < 2) do
      begin
         if (iterate_own^.PolyPart.IsInside(iterate_other^.PolyPart)) then
         begin
            if lastinsidepart = nil then // we got the first part that is inside
            begin
               lastinsidepart:=iterate_other;
               if (lastinsidepart^.PolyPart.GetPolygonPartType <> Island) then
               begin
                  if (other.NumInnerIslands = 0) then // due to other polygon has no islands -> own part is for shure inside other polygon
                     hadinsidemode:=2
                  else
                     hadinsidemode:=1;
               end
               else
               begin
                  // first part is within an island -> so we are not inside other polygon
                  hadinsidemode:=0;
               end;
            end
            else
            begin
               // own polygon part is also inside other iterate polygon part
               // now check if last inside part is within current iterate polygon part
               if (lastinsidepart^.PolyPart.IsInside(iterate_other^.PolyPart)) then
               begin
                  // no need to change hadinside mode due to lastinsidepart is inside of current other iterate part
               end
               else
               begin
                  // due to current other iterate part is within last inside part we got a new inside part
                  lastinsidepart:=iterate_other;
                  if (lastinsidepart^.PolyPart.GetPolygonPartType <> Island) then
                     hadinsidemode:=1 // -> own part is within outer part from other polygon
                  else
                  begin
                     // own part is within other island (only outside if own part is not an island)
                     if (iterate_own^.PolyPart.GetPolygonPartType <> Island) then
                        hadinsidemode:=0; // -> own part is within island from other polygon
                  end;
               end;
            end;
         end;
         iterate_other:=iterate_other^.next;
      end;
      if (hadinsidemode = 0) then
        retVal:=FALSE;
      iterate_own:=iterate_own^.next;
   end;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: RemovePartByIndex
// ------------------------------------------------------------------------
// DESCRIPTION : Method to remove a Polygon part
//
// PARAMETERS  : aIndex -> Index of the Polygon part that should be deleted
//
// RETURNVALUE : NONE
// ------------------------------------------------------------------------
procedure TPolygon.RemovePartByIndex(aIndex:integer);
var iterate:p_PolygonPart_record;
    PolyIdx:integer;
begin
   if aIndex >= numparts then exit;
   PolyIdx:=0;
   iterate:=FirstPart;
   while (PolyIdx < aIndex) do
   begin
      iterate:=iterate^.next;
      PolyIdx:=PolyIdx+1;
   end;
   // now remove the item
   if iterate^.prev = nil then // its the first corner in list
   begin
      FirstPart:=iterate^.next;
      if FirstPart <> nil then FirstPart^.prev:=nil;
      if iterate^.PolyPart <> nil then
      begin
         if iterate^.PolyPart.GetPolygonPartType = Island then  NumInnerIslands:=NumInnerIslands-1;
         iterate^.PolyPart.Destroy;
      end;
      dispose(iterate);
   end
   else if iterate^.next = nil then // its the last corner in list
   begin
      LastPart:=iterate^.prev;
      LastPart^.next:=nil;
      if iterate^.PolyPart <> nil then
      begin
         if iterate^.PolyPart.GetPolygonPartType = Island then  NumInnerIslands:=NumInnerIslands-1;
         iterate^.PolyPart.Destroy;
      end;
      dispose(iterate);
   end
   else
   begin // it�s in the middle of the list
      iterate^.next^.prev:=iterate^.prev;
      iterate^.prev^.next:=iterate^.next;
      if iterate^.PolyPart <> nil then
      begin
         if iterate^.PolyPart.GetPolygonPartType = Island then  NumInnerIslands:=NumInnerIslands-1;
         iterate^.PolyPart.Destroy;
      end;
      dispose(iterate);
   end;

   // remove part from the nesting list
   // first process the nesting of all polygons that had deleted polygon as parent
   // these get decreased nesting and parent from deleted polygon part as parent
   for PolyIdx:=0 to NumParts-1 do
   begin
      if IslandPartsNestings[PolyIdx]^.envelopeidx = aIndex then
      begin
         IslandPartsNestings[PolyIdx]^.nesting:=IslandPartsNestings[PolyIdx]^.nesting-1;
         IslandPartsNestings[PolyIdx]^.envelopeidx:=IslandPartsNestings[aIndex]^.envelopeidx;
      end;
   end;
   dispose(IslandPartsNestings[aIndex]); // remove the nesting of the part
   if (aIndex <> NumParts-1) and (NumParts > 1) then
   begin
      // now remove nesting from the list
      for PolyIdx:=aIndex to NumParts-2 do
         IslandPartsNestings[PolyIdx]:=IslandPartsNestings[PolyIdx+1];
   end;
   NumParts:=NumParts-1;

   SetLength(IslandPartsNestings, NumParts);
   // due to the nesting the type of the polygons can be set now
   PolyIdx:=0; iterate:=FirstPart;
   while (iterate <> nil) do
   begin
      if (IslandPartsNestings[PolyIdx]^.nesting = 0) then iterate^.PolyPart.SetPolygonPartType(MAIN)
      else if (IslandPartsNestings[PolyIdx]^.nesting mod 2 = 1) then iterate^.PolyPart.SetPolygonPartType(ISLAND)
      else iterate^.PolyPart.SetPolygonPartType(OuterIsland);
      iterate:=iterate^.next;
      PolyIdx:=PolyIdx+1;
   end;
   SetupRectangle;
end;

// ------------------------------------------------------------------------
// NAME: AddParts
// ------------------------------------------------------------------------
// DESCRIPTION : Method to add all parts from other polygon to own polygon
//
// PARAMETERS  : other -> Polygon that contain parts that should be added
//
// RETURNVALUE : NONE
// ------------------------------------------------------------------------
procedure TPolygon.AddParts(var other:TPolygon);
var iterate_other:p_PolygonPart_record;
begin
   // add left polygons from other Polygon to own Polygon
   if (other.NumParts < 1) then exit; // there are no parts to add
   iterate_other:=other.FirstPart;
   while (iterate_other <> nil) do
   begin
      if not AddPolygonPart(iterate_other^.PolyPart) then
      begin
         iterate_other^.PolyPart.Destroy; // due to Polygon Part could not be added it can be destroyed
      end;
      iterate_other^.PolyPart:=nil; // reset the Polygon Part of the other (to avoid that it can be deleted -> it belongs to own Polygon now)
      iterate_other:=iterate_other^.next;
   end;
   other.DeleteParts;
end;

// ------------------------------------------------------------------------
// NAME: AddPartsAsIslands
// ------------------------------------------------------------------------
// DESCRIPTION : Method to add all parts from other as Islands to own polygon
//               The Parts of the other Polygon will not be deleted
//               Is used to add polygon parts in CreateIslandLayer method
//
// PARAMETERS  : other -> Polygon that contain parts that should be added
//
// RETURNVALUE : NONE
// ------------------------------------------------------------------------
procedure TPolygon.AddPartsForCreateIslandLayer(var other:TPolygon);
var iterate_other:p_PolygonPart_record;
    aparttype    :TPolygonPartType;
begin
   iterate_other:=other.FirstPart;
   while (iterate_other <> nil) do
   begin
      // store old parttype to avoid problems if part is also part from other polygon
      aparttype:=iterate_other^.PolyPart.GetPolygonPartType;
      if AddPolygonPart(iterate_other^.PolyPart) then
      begin
         // count assignements to avoid delete of Island Part twice
         iterate_other^.PolyPart.CreateIslandAssignements:=iterate_other^.PolyPart.CreateIslandAssignements+1;
         NestingWriteFilter:=1; // write only main and Islands with nesting is 0 (Main parts) or 1 ( first island nesting )
      end;
      iterate_other^.PolyPart.SetPolygonPartType(aparttype);
      iterate_other:=iterate_other^.next;
   end;
end;

procedure TPolygon.DeleteParts;
var iterate1, iterate2:p_PolygonPart_record;
    partidx:integer;
begin
   iterate1:=FirstPart;
   iterate2:=FirstPart;
   partidx:=0;
   while (iterate1 <> nil) do
   begin
      iterate2:=iterate1;
      iterate1:=iterate1^.next;
      if iterate2^.PolyPart <> nil then iterate2^.PolyPart.Destroy;
      dispose(iterate2);
      dispose(IslandPartsNestings[partidx]);
      partidx:=partidx+1;
   end;
   FirstPart:=nil; // to avoid that Polygon parts could be deleted in destructor of own
   LastPart:=nil;  // due to they have been added to current polygon
   NumParts:=0;
   NumInnerIslands:=0;
   SetLength(IslandPartsNestings, 0);
end;

// ------------------------------------------------------------------------
// NAME: Combine
// ------------------------------------------------------------------------
// DESCRIPTION : Method to combine own polygon with other
//
// PARAMETERS  : other       -> Polygon that should be combined
//               HandleHoles -> Flag if holes should be handeled
//
// RETURNVALUE : TRUE -> if Polygons could be combined
// ------------------------------------------------------------------------
function TPolygon.Combine(var other:TPolygon; HandleHoles:boolean):boolean;
var retVal:boolean;
    iterate_own  :p_PolygonPart_record;
    iterate_other:p_PolygonPart_record;
    couldcombine:integer;
    OwnPartIdx  :integer;
    OtherPartIdx:integer;
    newPolyPart :TPolygonPart;
    oldPolyPart :TPolygonPart;
    i           :integer;
    newentry    :p_PolygonPart_record;
    newnesting  :p_PolygonPart_nesting;
begin
   retVal:=FALSE;
   result:=FALSE;

   // if one of the polygons contains no parts -> they could not be combined
   if (NumParts < 1) or (other.NumParts < 1) then exit;
   if FDoDebug then
   begin
      WriteDebugInfo;
      other.WriteDebugInfo;
   end;

   if Rectangle.GetOverLapType(other.Rectangle, FSnapRadius) = No_Overlap then exit;
   iterate_own:=FirstPart; OwnPartIdx:=0;
   while (iterate_own <> nil) do
   begin
      couldcombine:=No_Combine;
      iterate_other:=other.FirstPart; OtherPartidx:=0;
      while (iterate_other <> nil) and (couldcombine = No_Combine) do
      begin
         couldcombine:=iterate_own^.PolyPart.Combine(iterate_other^.PolyPart, HandleHoles);
         if (couldcombine <> No_Combine) then
         begin
            retVal:=TRUE;
            if couldcombine = Remove_Both then
            begin
               iterate_own:=iterate_own^.next;        // go to next item
               RemovePartByIndex(OwnPartIdx);         // remove Part from List
               other.RemovePartByIndex(OtherPartIdx); // remove Part from List in other polygon
            end
            else if couldcombine = Remove_Other then
            begin
               // get the resulting polygons from own polygons and replace own
               // with resulting polygons
               oldPolyPart:=iterate_own^.PolyPart;

               newPolyPart:=TPolygonPart.Create;
               newPolyPart.SetupWithCombineResult(oldPolyPart.GetCombineResult(0));
               iterate_own^.PolyPart:=newPolyPart;
               // now check if there are further combine result Polygon parts to create
               if oldPolyPart.NumCombineResultPolys > 1 then
               begin
                  for i:=1 to oldPolyPart.NumCombineResultPolys-1 do
                  begin
                     newentry:=new(p_PolygonPart_record);
                     newentry^.prev:=iterate_own;
                     newentry^.next:=iterate_own^.next;
                     if iterate_own^.next <> nil then
                        iterate_own^.next^.prev:=newentry
                     else
                        LastPart:=newentry;
                     iterate_own^.next:=newentry;
                     NumParts:=NumParts+1;
                     newPolyPart:=TPolygonPart.Create;
                     newPolyPart.SetupWithCombineResult(oldPolyPart.GetCombineResult(i));
                     newentry^.PolyPart:=newPolyPart;

                     // add also to nesting list
                     SetLength(IslandPartsNestings, NumParts);
                     newnesting:=new(p_PolygonPart_nesting);
                     newnesting^.nesting:=0; // add as main parts
                     newnesting^.envelopeidx:=-1;
                     IslandPartsNestings[NumParts-1]:=newnesting;

                     // keep iterate_own as it is
                  end;
               end;
               oldPolyPart.Destroy; // the old polygon can be removed now due to it has been replaced with the result
               other.RemovePartByIndex(OtherPartIdx); // remove Part from List in other polygon
            end;
         end
         else
         begin
            iterate_other:=iterate_other^.next;
            OtherPartidx:=OtherPartIdx+1;
         end;
         SetupRectangle;
      end;
      if couldcombine <> Remove_Both then
      begin
         iterate_own:=iterate_own^.next; OwnPartIdx:=OwnPartIdx+1;
      end;
   end;

   if retVal then
   begin
      AddParts(other); // add left parts to own polygon
   end;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: Read
// ------------------------------------------------------------------------
// DESCRIPTION : Method to read data from IPolygon object
//
// PARAMETERS  : aPoly -> Pointer to IPolygon object
//
// RETURNVALUE : NONE
// ------------------------------------------------------------------------
procedure TPolygon.Read(var aPoly:IPolygon);
var APoint:IPoint;
    PointCount:integer;
    SkipIndex :integer;
    FirstIsland:boolean;
    IslandCount:integer;
    IsIsland:boolean;
    IslandInfoIdx:integer;
    NextIslandIdx:integer;
    X,Y:double;
    Cnt:integer;
    aPolygonPart:TPolygonPart;
begin
   ProgisId:=aPoly.Index;
   PointCount:=0; SkipIndex:=0; FirstIsland:=TRUE; IsIsland:=FALSE;
   IslandCount:=aPoly.IslandCount;
   if IslandCount > 1 then
   begin
      IsIsland:=true;
      IslandInfoIdx:=0;
      NextIslandIdx:=aPoly.IslandInfo[IslandInfoIdx];
   end;

   aPolygonPart:=TPolygonPart.Create;

   // insert first point to new CPoly
   APoint:=aPoly.Points[0];
   X:=APoint.X/100; Y:=APoint.Y/100;

   aPolygonPart.InsertPoint(X, Y);
   PointCount:=PointCount+1;

   for Cnt:=1 to aPoly.Count - 1 do
   begin
      if SkipIndex = 0 then
         PointCount:=PointCount + 1;
      if (IsIsland) and (PointCount = NextIslandIdx) then // next island area will be created
      begin
         IslandInfoIdx:=IslandInfoIdx+1;
         if FirstIsland then
            SkipIndex:=1
         else
            SkipIndex:=2;

         FirstIsland:=FALSE;
         if IslandInfoIdx >= IslandCount then
         begin
            // all islands have been inserted
            NextIslandIdx:=0;
            IslandInfoIdx:=IslandCount;
         end
         else
         begin
            // and a new polygon has to be created
            AddPolygonPart(aPolygonPart);
            aPolygonPart:=TPolygonPart.Create;
            NextIslandIdx:=aPoly.IslandInfo[IslandInfoIdx];

            // insert point to polygon
            APoint:=aPoly.Points[Cnt+SkipIndex];
            X:=APoint.X/100; Y:=APoint.Y/100;
            aPolygonPart.InsertPoint(X, Y);
            PointCount:=1;
         end;
      end
      else
      begin
         if (SkipIndex=0) then
         begin
            if (IsIsland) and (Cnt = aPoly.Count-2) then //avoid exporting last connectionline
               continue;

            if (not IsIsland) and (Cnt = aPoly.Count-1) then // we do not need last corner due to this is equal than first corner of polygon
               continue;

            // insert point to polygon
            APoint:=aPoly.Points[Cnt];
            X:=APoint.X/100; Y:=APoint.Y/100;
            // calculate the projection
            aPolygonPart.InsertPoint(X, Y);
         end
         else
            SkipIndex:=SkipIndex-1;
      end;
   end;
   AddPolygonPart(aPolygonPart);
   if FDoDebug then
   begin
      WriteDebugInfo;
   end;
end;

procedure TPolygon.WriteDebugInfo;
var DebugFile:Textfile;
    iterate:p_PolygonPart_record;
begin
   AssignFile(DebugFile, FWorkingDir+'Debug.txt');
   if not FileExists(FWorkingDir+'Debug.txt') then
      Rewrite(DebugFile)
   else
      Append(Debugfile);

   writeln(DebugFile,'--------------------------------------');
   writeln(DebugFile,'POLYGON');
   writeln(Debugfile,'Parts:'+inttostr(NumParts));
   writeln(Debugfile,'ID   :'+inttostr(ProgisID));
   iterate:=FirstPart;
   while (iterate <> nil) do
   begin
      iterate^.PolyPart.WriteDebugInfo(Debugfile);
      iterate:=iterate^.next;
   end;
   writeln(DebugFile,'--------------------------------------');
   CloseFile(DebugFile);
end;

// ------------------------------------------------------------------------
// NAME: WriteToLayer
// ------------------------------------------------------------------------
// DESCRIPTION : Method to write Polygon to Layer
//
// PARAMETERS  : aDocument -> Pointer to current document
//               aLayer    -> Layer to that it should be written
//
// RETURNVALUE : Index of the new created Polygon (-1 if write failed)
// ------------------------------------------------------------------------
function TPolygon.WriteToLayer(var aDocument:IDocument; var aLayer:ILayer):integer;
var retVal     :integer;
    aIslandList:IList;
    iterate    :p_PolygonPart_record;
    aPoly      :IPolygon;
    aPoint     :IPoint;
    i          :integer;
    X, Y       :double;
    partnr     :integer;
begin
   retVal:=-1;
   result:=-1;
   if (NumParts < 1) then // polygon is invalid
      exit;
   if NumParts > 1 then
      aIslandList:=aDocument.CreateList;

   iterate:=FirstPart; partnr:=0;
   while (iterate <> nil) do
   begin
      // in this case the island-islands may not be created to avoid that polygons exist more than one time
      if (IslandPartsNestings[partnr]^.nesting > NestingWriteFilter) then
      begin
         iterate:=iterate^.next;
         partnr:=partnr+1;
         continue;
      end;

      if FDoDebug then
         WriteDebugInfo;
      aPoly:=aDocument.CreatePolygon;
      for i:=0 to iterate^.PolyPart.GetNumCorners -1 do
      begin
         iterate^.PolyPart.GetCornerByIndex(i, X, Y);
         aPoint:=aDocument.CreatePoint;
         aPoint.X:=round(X*100);
         aPoint.Y:=round(Y*100);
         aPoly.InsertPoint(APoint);
      end;
      if (NumParts = 1) then
      begin
         // add polygon to layer
         aLayer.InsertObject(aPoly);
         retVal:=aPoly.Index;
      end
      else
      begin
         aPoly.ClosePoly;
         aIslandList.Add(aPoly);
      end;
      iterate:=iterate^.next; partnr:=partnr+1;
   end;
   if (NumParts > 1) then
   begin
      aPoly:=aDocument.CreateIslandArea(aIslandList);
      aLayer.InsertObject(aPoly);
      retVal:=aPoly.Index;
   end;
   result:=retVal;
end;

end.
