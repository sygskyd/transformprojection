library AXGisMan;

uses
  ComServ,
  AXGisMan_TLB in 'AXGisMan_TLB.pas',
  AXGisManImpl in 'AXGisManImpl.pas' {AXGisObjMan: CoClass},
  Strfile in 'Strfile.pas',
  PolygonPart in 'PolygonPart.pas',
  Rectangle in 'Rectangle.pas',
  ObjectFifoBuffers in 'ObjectFifoBuffers.pas',
  Polygon in 'Polygon.pas',
  PolygonList in 'PolygonList.pas';

{$E ocx}

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
