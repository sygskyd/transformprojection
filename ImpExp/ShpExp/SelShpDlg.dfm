object SelShpWnd: TSelShpWnd
  Left = 218
  Top = 150
  BorderStyle = bsDialog
  Caption = 'PROGIS ARCVIEW-SHP Export 2000'
  ClientHeight = 351
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 8
    Top = 8
    Width = 441
    Height = 305
    ActivePage = TabSheetSource
    TabIndex = 0
    TabOrder = 0
    object TabSheetSource: TTabSheet
      Caption = 'Source Project'
      ImageIndex = 1
      object LayerLB: TListBox
        Left = 4
        Top = 24
        Width = 169
        Height = 217
        DragMode = dmAutomatic
        ItemHeight = 13
        MultiSelect = True
        TabOrder = 0
        OnClick = LayerLBClick
      end
      object LayerCB: TCheckBox
        Left = 4
        Top = 0
        Width = 169
        Height = 17
        Caption = 'Export only selected layers'
        TabOrder = 1
        OnClick = LayerCBClick
      end
      object SelectedCB: TCheckBox
        Left = 176
        Top = 0
        Width = 249
        Height = 17
        Caption = 'Export only selected objects'
        TabOrder = 2
        OnClick = SelectedCBClick
      end
      object Panel1: TPanel
        Left = 4
        Top = 248
        Width = 425
        Height = 25
        TabOrder = 3
        object SourceDbLabel: TLabel
          Left = 8
          Top = 8
          Width = 74
          Height = 13
          Caption = 'SourceDbLabel'
        end
      end
    end
    object TabSheetDest: TTabSheet
      Caption = 'Destination ARCVIEW-Shp'
      ImageIndex = 1
      object Panel2: TPanel
        Left = 4
        Top = 0
        Width = 426
        Height = 273
        TabOrder = 0
        object FileSelectionPanel: TFileSelectionPanel
          Left = 4
          Top = 4
          Width = 418
          Height = 265
          TabOrder = 0
          Mask = '*.*'
          Directory = 'C:\Users\aigner\Desktop'
          DirectoryShellTreeView = ShellTreeView
          OnDirectoryChanged = FileSelectionPanelDirectoryChanged
          object ShellTreeView: TShellTreeView
            Left = 2
            Top = 2
            Width = 414
            Height = 261
            ObjectTypes = [otFolders]
            Root = 'rfDesktop'
            UseShellImages = True
            AutoRefresh = False
            Indent = 19
            ParentColor = False
            RightClickSelect = True
            ShowRoot = False
            TabOrder = 0
          end
        end
      end
    end
  end
  object OKBtn: TButton
    Left = 240
    Top = 320
    Width = 93
    Height = 23
    Caption = 'OK'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 348
    Top = 320
    Width = 101
    Height = 23
    Cancel = True
    Caption = 'Abbrechen'
    ModalResult = 2
    TabOrder = 2
    OnClick = CancelBtnClick
  end
  object GeoBtn: TButton
    Left = 8
    Top = 320
    Width = 93
    Height = 23
    Caption = 'Projection'
    Default = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = GeoBtnClick
  end
  object DbBtn: TButton
    Left = 104
    Top = 320
    Width = 93
    Height = 23
    Caption = 'Database'
    TabOrder = 4
    OnClick = DbBtnClick
  end
  object SetupWindowTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = SetupWindowTimerTimer
    Left = 208
    Top = 320
  end
end
