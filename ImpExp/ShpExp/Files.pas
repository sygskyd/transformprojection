unit Files;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Math, Struct, Strfile, Db;

type
  { Hier k�nnen Strukturen definiert werden }
  p_double = ^doublewert;
  doublewert = record
     Zahl :array [0..7] of byte;
  end;

  p_integer = ^integerwert;
  integerwert = record
     Zahl:array [0..3] of byte;
  end;


ClassFile = class
   public
      { public Definitionen }
      constructor Create;
      destructor Destroy;
      function GetMaxPass2:longint;
      procedure SetSHPDirectory(Verzeichnis:string);
      procedure CloseAllFiles;
      procedure ResetLayerZeiger;
      function  GetAktLayerName:string;
      function  WriteAktLayer:longint;
      procedure WriteHeader(Shapetype:integer;Groesse:longint;X_min:double;Y_min:double;X_max:double;Y_max:double; numelems:integer);
      procedure WriteRecordHeader(Record_Number:longint; Content_length:longint);
      procedure WriteShapetype(Typ:longint);
      procedure WriteXY(X:double;Y:double);
      procedure WriteSHXBlock(Offset:longint;Content_length:longint);
      procedure WriteInteger(Typ:string;Wert:longint;Ziel:string);
      procedure WriteDouble(Typ:string;Wert:double;Ziel:string);
      procedure New_Point(X:double;Y:double;Layername:string);
      procedure New_Area(Layername:string);
      procedure AddIslandIdx(Index:integer);
      procedure InsertPointToArea(X:double;Y:double);
      procedure New_Line(Layername:string);
      procedure InsertPointToLine(X:double;Y:double);
      procedure New_Layer(Layername:string);
      procedure ValDatabase(Index:integer; ObjType:integer;Layername:string);
   private
      { private Definitionen }
      PolygonObj:ClassPolygon;
      PunktObj  :ClassPunkt;
      LayerObj  :ClassLayer;
      procedure CreateDir(var Name:string);
      procedure MakeCorrectName(var Name:string);
      procedure CopyAFile(Source:string; Dest:string);
end;
implementation
uses MainDlg, ActiveX;

var  ShpDatei,ShxDatei: file;
     DatenbankObj:ClassDatenbank;
     StrFileObj:ClassStrFile;
     ShpDirectory:string;

constructor ClassFile.Create;
begin
   LayerObj:=ClassLayer.Create;
   PolygonObj:=ClassPolygon.Create;
   PunktObj:=ClassPunkt.Create;
   DatenbankObj:=ClassDatenbank.Create;
end;

destructor ClassFile.Destroy;
begin
   LayerObj.Destroy;
   PolygonObj.Destroy;
   PunktObj.Destroy;
   DatenbankObj.Destroy;
end;

{*******************************************************************************
* PROZEDUR : SetSHPDirectory                                                   *
********************************************************************************
* Prozedur dient zum Setzen des Shapefile Directories.                         *
*                                                                              *
* PARAMETER: Verzeichnis -> Shapefile Verzeichnis.                             *
*                                                                              *
********************************************************************************}
procedure ClassFile.SetSHPDirectory(Verzeichnis:string);
begin
   ShpDirectory:=Verzeichnis;
end;

procedure ClassFile.MakeCorrectName(var name:string);
var pdummy:PCHAR;
    pdummy2:PCHAR;
    i,pos:integer;
    res:string;
    canadd:boolean;
begin
   //function is used to set the layername that it can be set as database or controlname
   pdummy:=stralloc(length(name)+1); strpcopy(pdummy,name);
   pdummy2:=stralloc(length(name)+1); strpcopy(pdummy2,name);
   pos:=0;
   for i:=0 to strlen(pdummy)-1 do
   begin
      canadd:=true;
      if (pdummy[i] = '\') then canadd:=false;
      if (pdummy[i] = '/') then canadd:=false;
      if (pdummy[i] = ':') then canadd:=false;
      if (pdummy[i] = '*') then canadd:=false;
      if (pdummy[i] = '?') then canadd:=false;
      if (pdummy[i] = '"') then canadd:=false;
      if (pdummy[i] = '<') then canadd:=false;
      if (pdummy[i] = '>') then canadd:=false;
      if (pdummy[i] = '|') then canadd:=false;
      if (canadd) then
      begin
         pdummy2[pos]:=pdummy[i];
         pos:=pos+1;
      end;
   end;
   name:=strpas(pdummy2);
   name:=copy(name,1,pos);
   strdispose(pdummy); strdispose(pdummy2);
end;

{*******************************************************************************
* PROZEDUR : WriteShpHeader                                                    *
********************************************************************************
* Prozedur dient zum Erzeugen eines Headers in einem Shapefile.                *
*                                                                              *
* PARAMETER: Shapetype -> Typ des Shapes 1-Punkt, 3-Arc, 5-Area                *
*            Groesse   -> Dateigroesse des zu erzeugenden Shapefiles           *
*            X_min, Y_min, X_max, Y_max -> Umrahmendes Rechteck, aller Daten.  *
*            NumElems  -> Number of elements of the Shape
********************************************************************************}
procedure ClassFile.WriteHeader(Shapetype:integer;Groesse:longint;X_min:double;Y_min:double;X_max:double;Y_max:double; numelems:integer);
var i:integer;
begin
   { Schreiben der Filekennung }
   WriteInteger('Big',9994,'SHP'); WriteInteger('Big',9994,'SHX');

   { Nun folgen 5 Integerwerte ohne Bedeutung }
   for i:=1 to 5 do begin WriteInteger('Big',0,'SHP'); WriteInteger('Big',0,'SHX'); end;

   { Nun wird die Dateigroesse geschrieben }
   WriteInteger('Big',Groesse,'SHP'); WriteInteger('Big',50+(NumElems*4),'SHX');

   { Nun wird die Version des Shapes geschrieben }
   WriteInteger('Little',1000,'SHP'); WriteInteger('Little',1000,'SHX');

   { Nun wird der Shapetype geschrieben }
   WriteInteger('Little',Shapetype,'SHP'); WriteInteger('Little',Shapetype,'SHX');

   { Nun werden die Boundingbox Daten geschrieben }
   WriteDouble('Little',X_min,'SHP'); WriteDouble('Little',Y_min,'SHP'); WriteDouble('Little',X_min,'SHX'); WriteDouble('Little',Y_min,'SHX');
   WriteDouble('Little',X_max,'SHP'); WriteDouble('Little',Y_max,'SHP'); WriteDouble('Little',X_max,'SHX'); WriteDouble('Little',Y_max,'SHX');

   { Schreiben der Unused Daten }
   for i:=1 to 8 do begin WriteInteger('Big',0,'SHP'); WriteInteger('Big',0,'SHX'); end;
end;

{*******************************************************************************
* PROZEDUR : WriteRecordHeader                                                 *
********************************************************************************
* Prozedur dient zum schreiben eines Objektblockes im SHX-File.                *
*                                                                              *
* PARAMETER: Offset        -> Offset des Objekts im SHP-File                   *
*            Content_length -> L�nge in Bytes des Objekts.                     *
*                                                                              *
********************************************************************************}
procedure ClassFile.WriteSHXBlock(Offset:longint;Content_length:longint);
begin
   WriteInteger('Big',Offset,'SHX');
   WriteInteger('Big',Content_length,'SHX');
end;


{*******************************************************************************
* PROZEDUR : WriteRecordHeader                                                 *
********************************************************************************
* Prozedur dient zum schreiben des Recordheaders eines Objekts.                *
*                                                                              *
* PARAMETER: Record_Number -> Nummer des Objekts innerhalb des Shapes          *
*            Content_length -> L�nge in Bytes des Objekts.                     *
*                                                                              *
********************************************************************************}
procedure ClassFile.WriteRecordHeader(Record_Number:longint; Content_length:longint);
begin
   WriteInteger('Big',Record_Number,'SHP');
   WriteInteger('Big',Content_length,'SHP');
end;

{*******************************************************************************
* PROZEDUR : WriteShapetype                                                    *
********************************************************************************
* Prozedur dient zum Schreiben des Shapetyps eines Objekts.                    *
*                                                                              *
* PARAMETER: Typ -> Shapetyp                                                   *
*                                                                              *
********************************************************************************}
procedure ClassFile.WriteShapetype(Typ:longint);
begin
   WriteInteger('Little',Typ,'SHP');
end;

{*******************************************************************************
* PROZEDUR : WriteXY                                                           *
********************************************************************************
* Prozedur dient zum Schreiben eines Koordinatenpaares.                        *
*                                                                              *
* PARAMETER: X,Y -> X und Y Koordinate.                                        *
*                                                                              *
********************************************************************************}
procedure ClassFile.WriteXY(X:double;Y:double);
begin
   WriteDouble('Little',X,'SHP');
   WriteDouble('Little',Y,'SHP');
end;

{*******************************************************************************
* FUNKTION : WriteAktLayer                                                     *
********************************************************************************
* Prozedur dient zum Erzeugen der Shapefiles f�r den Aktuellen Layer.          *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Layer (hier immer 1)                   *
********************************************************************************}
function ClassFile.WriteAktLayer:longint;
var Name,DirName:string;
    hpchar:pchar;
    HasData:boolean;
    CorrectLayername:string;
    OriginalDbfName:string;
    newDbfName:string;
begin
   HasData:=false;
   hpchar:=stralloc(255);

   // create point shape
   // check if layer contains points
   MainWnd.ObjectsToWrite:=LayerObj.GetNumObjectsToWrite;
   MainWnd.ObjectsWritten:=0;

   if LayerObj.CheckPoints then
   begin
      HasData:=true;
      DirName:='points';
      CreateDir(DirName);
      Name:=LayerObj.GetaktLayerName;
      CorrectLayername:=Name;
      MakeCorrectName(CorrectLayername);
      Name:=CorrectLayername + '.shp';
      strpcopy(hpchar,Name);
      if Fileexists(Name) then DeleteFile(hpchar);
      if Fileexists(Changefileext(Name,'.shx')) then
         begin strpcopy(hpchar,Changefileext(Name,'.shx')); DeleteFile(hpchar); end;
      if Fileexists(Changefileext(Name,'.dbf')) then
         begin strpcopy(hpchar,Changefileext(Name,'.dbf')); DeleteFile(hpchar); end;

      // create database for shape
      OriginalDbfName:=ShpDirectory+'points\'+Changefileext(Name,'.dbf');
      newDbfName:=MainWnd.AXImpExpDbc.TmpDBaseDir+'shpexp.dbf';
      LayerObj.CreateDBF(newDbfName);

      {$i-}
      Assignfile(ShpDatei,Name); Rewrite(ShpDatei,1);
      Assignfile(ShxDatei,Changefileext(Name,'.shx')); Rewrite(ShxDatei,1);
      {$i+}
      PunktObj.WritePoints;
      {$i-}
      Closefile(ShpDatei); Closefile(ShxDatei);
      {$i+}

      // close database
      MainWnd.ADOTable.First;
      MainWnd.ADOTable.Close;
      MainWnd.ADOConnection.Connected:=FALSE;
      // now copy the temporary .dbf name to the original .dbf name
      CopyAFile(newDbfName, OriginalDbfName);
      DeleteFile(PCHAR(newDbfName)); // the temporary file can be deleted
   end;

   // create line shape
   // check if laye contains lines
   if LayerObj.CheckLines then
   begin
      HasData:=true;
      DirName:='lines';
      CreateDir(DirName);
      Name:=LayerObj.GetaktLayerName;
      CorrectLayername:=Name;
      MakeCorrectName(CorrectLayername);
      Name:=CorrectLayername + '.shp';
      strpcopy(hpchar,Name);
      if Fileexists(Name) then DeleteFile(hpchar);
      if Fileexists(Changefileext(Name,'.shx')) then
         begin strpcopy(hpchar,Changefileext(Name,'.shx')); DeleteFile(hpchar); end;
      if Fileexists(Changefileext(Name,'.dbf')) then
         begin strpcopy(hpchar,Changefileext(Name,'.dbf')); DeleteFile(hpchar); end;

      OriginalDbfName:=ShpDirectory+'lines\'+Changefileext(Name,'.dbf');
      newDbfName:=MainWnd.AXImpExpDbc.TmpDBaseDir+'shpexp.dbf';

      // create database for line-shape
      LayerObj.CreateDBF(newDbfName);

      {$i-}
      Assignfile(ShpDatei,Name); Rewrite(ShpDatei,1);
      Assignfile(ShxDatei,Changefileext(Name,'.shx')); Rewrite(ShxDatei,1);
      {$i+}
      PolygonObj.WritePolygons(1);
      {$i-}
      Closefile(ShpDatei); Closefile(ShxDatei);
      {$i+}

      // close database
      MainWnd.ADOTable.First;
      MainWnd.ADOTable.Close;
      MainWnd.ADOConnection.Connected:=FALSE;

      // now copy the temporary .dbf name to the original .dbf name
      CopyAFile(newDbfName, OriginalDbfName);
      DeleteFile(PCHAR(newDbfName)); // the temporary file can be deleted
   end;

   // create area shape
   // first check if layer contains areas
   if LayerObj.CheckAreas then
   begin
      HasData:=true;
      DirName:='areas';
      CreateDir(DirName);
      Name:=LayerObj.GetaktLayerName;
      CorrectLayername:=Name;
      MakeCorrectName(CorrectLayername);
      Name:=CorrectLayername + '.shp';
      strpcopy(hpchar,Name);
      if Fileexists(Name) then DeleteFile(hpchar);
      if Fileexists(Changefileext(Name,'.shx')) then
         begin strpcopy(hpchar,Changefileext(Name,'.shx')); DeleteFile(hpchar); end;
      if Fileexists(Changefileext(Name,'.dbf')) then
         begin strpcopy(hpchar,Changefileext(Name,'.dbf')); DeleteFile(hpchar); end;

      // create dBase-Table for areas
      OriginalDbfName:=ShpDirectory+'areas\'+Changefileext(Name,'.dbf');
      newDbfName:=MainWnd.AXImpExpDbc.TmpDBaseDir+'shpexp.dbf';
      LayerObj.CreateDBF(newDbfName);
      {$i-}
      Assignfile(ShpDatei,Name); Rewrite(ShpDatei,1);
      Assignfile(ShxDatei,Changefileext(Name,'.shx')); Rewrite(ShxDatei,1);
      {$i+}
      PolygonObj.WritePolygons(2);
      {$i-}
      Closefile(ShpDatei); Closefile(ShxDatei);
      {$i+}

      // close database
      MainWnd.ADOTable.First;
      MainWnd.ADOTable.Close;
      MainWnd.ADOConnection.Connected:=FALSE;
      // now copy the temporary .dbf name to the original .dbf name
      CopyAFile(newDbfName, OriginalDbfName);
      DeleteFile(PCHAR(newDbfName)); // the temporary file can be deleted
   end;
   if not HasData then
   begin
      // no point / poly / line object exists for this layer
      MainWnd.DisplayMessage(StrfileObj.Assign(41)); // Warning: No point/polyline/polygon information exists for this layer
      MainWnd.DisplayMessage(StrfileObj.Assign(42)); //          No shp/shx/dbf file will be created.
   end;
   strdispose(hpchar);
   LayerObj.NextLayer;
   result:=1;
end;

// ------------------------------------------------------------------------
// NAME: CopyAFile
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to copy a file
//
// PARAMETERS : Source -> Filename of the source file
//              Dest   -> Filename of the destination file
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure ClassFile.CopyAFile(Source:string; Dest:string);
var SourceFile:File;
    DestFile  :File;
    Actualbytes:array [1..4096] of char;
    numread      :integer;
    numwritten   :integer;
    OldFilemode  :integer;
begin
   OldFileMode:=Filemode;
   Filemode:=0; Assignfile(SourceFile,Source); Reset(SourceFile,1);
   Filemode:=1; Assignfile(DestFile,Dest); Rewrite(DestFile,1);
   repeat
      BlockRead(SourceFile, ActualBytes, SizeOf(ActualBytes), NumRead);
      BlockWrite(DestFile, ActualBytes, NumRead, NumWritten);
   until (NumRead = 0) or (NumWritten <> NumRead);
   Closefile(SourceFile);
   Closefile(DestFile);
   FileMode:=OldFilemode;
end;

{*******************************************************************************
* PROZEDUR : CreateDir                                                         *
********************************************************************************
* Prozedur dient zum Erzeugen eines Unterverzeichnisses                        *
*                                                                              *
* PARAMETER: Name -> Name des Verzeichnisses das erstellt werden soll.         *
*                                                                              *
********************************************************************************}
procedure ClassFile.CreateDir(var Name:string);
var hstr:array [1..255] of char;
    i:integer;
begin
   { Nun muss der Name auf Sonderzeichen �berpr�ft werden }
   strpcopy(@hstr,Name);
   for i:=1 to length(Name) do
   begin
      if hstr[i] = '�' then hstr[i]:=CHR(148);
      if hstr[i] = '�' then hstr[i]:=CHR(153);
      if hstr[i] = '�' then hstr[i]:=CHR(132);
      if hstr[i] = '�' then hstr[i]:=CHR(142);
      if hstr[i] = '�' then hstr[i]:=CHR(129);
      if hstr[i] = '�' then hstr[i]:=CHR(154);
      if hstr[i] = '�' then hstr[i]:=CHR(225);
   end;
   Name:=strpas(@hstr);
   {$i-}
   if not DirectoryExists(ShpDirectory+'\'+Name) then
   begin
      MKDir(ShpDirectory+'\'+Name);
   end;
   ChDir(ShpDirectory+'\'+Name);
   {$i+}
end;

{*******************************************************************************
* PROZEDUR : CloseallFiles                                                     *
********************************************************************************
* Prozedur dient zum Schliessen aller Files.                                   *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassFile.CloseallFiles;
begin
   MainWnd.AXImpExpDbc.CloseExpTables;
end;

procedure ClassFile.ResetLayerZeiger;
begin
   LayerObj.ResetLayerzeiger;
end;

{*******************************************************************************
* FUNKTION : GetMaxPass2                                                       *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu schreibeinden Layer           *
*                                                                              *
* R�ckgabewert: Anzahl der zu Bearbeitenden Bytes.                             *
********************************************************************************}
function ClassFile.GetMaxPass2:longint;
begin
   result:=LayerObj.GetAnz;
end;

{*******************************************************************************
* FUNKTION : GetAktLayer                                                       *
********************************************************************************
* Funktion dient zum Ermitteln welcher Layer gerade geschrieben wird.          *
*                                                                              *
* R�ckgabewert: Name des Layers der gerade geschrieben wird.                   *
********************************************************************************}
function ClassFile.GetAktLayerName:string;
begin
   Result:=LayerObj.GetAktLayerName;
end;

{*******************************************************************************
* PROZEDUR : WriteInteger                                                      *
********************************************************************************
* Prozedur dient zum schreiben eines Integerwertes in ein Shapefile.           *
*                                                                              *
* PARAMETER: Typ -> Typ, wie die Auswertung erfolgen soll. (Big, oder Little)  *
*            Wert -> Zahl die geschrieben werden soll                          *
*            Ziel -> 'SHP' wenn auf Shapefile geschrieben werden soll, sonst   *
*                    'SHX' wenn auf SHX-file geschrieben werden soll.          *
*                                                                              *
********************************************************************************}
procedure ClassFile.WriteInteger(Typ:string;Wert:longint;Ziel:string);
var Buffer: array [0..3] of byte;
    Byteswrite,i:integer;
    Zeiger    :p_integer;
    Zahl      :longint;
begin
   Zahl:=Wert;
   if Typ = 'Big' then
   begin
      Zeiger:=@Zahl;
      { Beim Typ Big muss der Buffer jedoch umgedreht werden }
      for i:=0 to 3 do Buffer[i]:=Zeiger^.Zahl[3 - i];
   end;
   if Typ = 'Little' then
   begin
      Zeiger:=@Zahl;
      for i:=0 to 3 do Buffer[i]:=Zeiger^.Zahl[i];
   end;
   {$i-}
   if Ziel = 'SHP' then Blockwrite(ShpDatei,Buffer,4,Byteswrite);
   if Ziel = 'SHX' then Blockwrite(ShxDatei,Buffer,4,Byteswrite);
   {$i+}
end;


{*******************************************************************************
* PROZEDUR : WriteDouble                                                       *
********************************************************************************
* Prozedur dient zum schreiben eines Integerwertes in ein File                 *
*                                                                              *
* PARAMETER: Typ -> Typ, wie die Auswertung erfolgen soll. (Big, oder Little)  *
*            Wert -> Zahl die geschrieben werden soll                          *
*            Ziel -> 'SHP' wenn auf Shapefile geschrieben werden soll, sonst   *
*                    'SHX' wenn auf SHX-file geschrieben werden soll.          *
*                                                                              *
********************************************************************************}
procedure ClassFile.WriteDouble(Typ:string;Wert:double;Ziel:string);
var Buffer: array [0..7] of byte;
    Byteswrite,i:integer;
    Zeiger    :p_double;
    Zahl      :double;
begin
   Zahl:=Wert;
   if Typ = 'Big' then
   begin
      Zeiger:=@Zahl;
      { Beim Typ Big muss der Buffer jedoch umgedreht werden }
      for i:=0 to 7 do Buffer[i]:=Zeiger^.Zahl[3 - i];
   end;
   if Typ = 'Little' then
   begin
      Zeiger:=@Zahl;
      for i:=0 to 7 do Buffer[i]:=Zeiger^.Zahl[i];
   end;
   {$i-}
   if Ziel = 'SHP' then Blockwrite(ShpDatei,Buffer,8,Byteswrite);
   if Ziel = 'SHX' then Blockwrite(ShxDatei,Buffer,8,Byteswrite);
   {$i+}
end;

procedure ClassFile.New_Layer(Layername:string);
var Columnname:widestring;
    Columntype:TOleEnum;
    Columnlength:integer;
begin
   LayerObj.Neu(Layername);

   MainWnd.ProzessBalken2.Progress:=0; // to reset ProgressBar2

   // read database column for this layer
   MainWnd.AXImpExpDbc.CheckAndCreateExpTable(Layername);
   while MainWnd.AXImpExpDbc.GetExportColumn(Layername, Columnname, Columntype, Columnlength) do
   begin
      DatenbankObj.Insertcolumn(Columnname,Columnlength,Columntype,'',0,0,true);
      LayerObj.InsertDBColumn(Columnname);
   end;
   MainWnd.AXImpExpDbc.OpenExpTable(Layername);
end;

procedure Classfile.ValDatabase(Index:integer; ObjType:integer; Layername:string);
var column, info:widestring;
    iterate:p_Datenbank_record;
begin
   // function is used to get database information for object
   if not MainWnd.AXImpExpDbc.DoExportData(Layername,Index) then
   begin
      // add all empty values for this database entry
      iterate:=LayerObj.GetColumnDefinition;
      while (iterate <> nil) do
      begin
         column:=iterate^.Bezeichnung;
         Info:='0';
         if      (ObjType = ot_Pixel  ) then PunktObj.InsertAttrib(column,Info)
         else if (ObjType = ot_Symbol ) then PunktObj.InsertAttrib(column,Info)
         else if (ObjType = ot_Cpoly  ) then PolygonObj.InsertAttrib(column,Info,2)
         else if (ObjType = ot_poly   ) then PolygonObj.InsertAttrib(column,Info,1);
         iterate:=iterate^.next;
      end;
      exit; // if no databaseentry has been found for this object exit
   end;

   while MainWnd.AXImpExpDbc.GetExportData(Layername, column, info) do
   begin
      if      (ObjType = ot_Pixel  ) then PunktObj.InsertAttrib(column,Info)
      else if (ObjType = ot_Cpoly  ) then PolygonObj.InsertAttrib(column,Info,2)
      else if (ObjType = ot_poly   ) then PolygonObj.InsertAttrib(column,Info,1);
   end;
end;

procedure ClassFile.New_Point(X:double;Y:double;Layername:string);
begin
   PunktObj.Neu(Layername,X,Y);
end;

procedure ClassFile.New_Area(Layername:string);
begin
   PolygonObj.Neu(Layername,2);
end;

procedure ClassFile.AddIslandIdx(Index:integer);
begin
   PolygonObj.AddIslandIdx(Index);
end;

procedure ClassFile.New_Line(Layername:string);
begin
   PolygonObj.Neu(Layername,1);
end;

procedure ClassFile.InsertPointToArea(X:double;Y:double);
begin
   PolygonObj.InsertPoint(X,Y,2);
end;

procedure ClassFile.InsertPointToLine(X:double;Y:double);
begin
   PolygonObj.InsertPoint(X,Y,1);
end;
end.
