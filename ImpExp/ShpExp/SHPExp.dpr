library ShpExp;

{ Wichtiger Hinweis zur DLL-Speicherverwaltung: Sharemem mu� die erste
 Unit in der uses-Klausel Ihrer Bibliothek und Ihres Projekts sein,
 wenn Ihre DLL Prozeduren und Funktionen exportiert, die
 Strings als Parameter oder Funktionsergebnisse �bergeben. Dies
 betrifft alle Strings, die an Ihre DLL oder von Ihrer DLL �bergeben werden--selbst diejenigen,
 die sich in Records oder Klassen befinden. Sharemem ist die Schnittstelle zu
 BORLNDMM.DLL, die den gemeinsamen Zugriff auf den Speicher verwaltet; DELPHIMM.DLL
 mu� mit Ihrer DLL weitergegeben werden. Wenn Sie BORLNDMM.DLL nicht verwenden m�chten, m�ssen
 Strings als PChar oder ShortString �bergeben werden.}

uses
  SysUtils,
  Classes,
  Dialogs,
  Forms,
  Maindlg in 'MAINDLG.PAS' {MainWnd},
  Strfile in 'STRFILE.PAS',
  Files in 'Files.pas',
  struct in 'struct.pas',
  comobj,
  AXDLL_TLB,
  SelShpDlg in 'SelShpDlg.pas' {SelShpWnd};

{$R *.RES}

{*******************************************************************************
* PROZEDURE * SHPEXPORT                                                        *
********************************************************************************
* Procedure is used to import ARCVIEW SHP data into GIS                        *
*                                                                              *
********************************************************************************}
procedure SHPEXPORT(DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var
aDisp:IDispatch;
begin
   // create MainWnd
   MainWnd:=TMainWnd.Create(nil);
   aDisp:=SOURCE;
   MainWnd.SourceDocument:=aDisp as IDocument;
   MainWnd.App:=AXHANDLE;
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.Regmode:=REGMODE;
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.CreateStringFile;

   // create SelShpWnd
   SelShpWnd:=TSelShpWnd.Create(nil);
   SelShpWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);

   SelShpWnd.Show;
end;

{*******************************************************************************
* PROCEDURE * CREATEEXP                                                        *
********************************************************************************
* Procedure is used to prepare an SHP-export for Batchmode                     *
*                                                                              *
********************************************************************************}
procedure CREATEEXP(DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var aDisp:IDispatch;
begin
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.App:=AXHANDLE;
   aDisp:=SOURCE;
   MainWnd.SourceDocument:=aDisp as IDocument;
   MainWnd.RegMode:=REGMODE;
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.CreateStringFile;

   // create SelShpWnd
   SelShpWnd:=TSelShpWnd.Create(nil);
   SelShpWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);
   SelShpWnd.SetupForBatch;
end;

{*******************************************************************************
* PROCEDURE * SETEXPSETTINGS                                                   *
********************************************************************************
* Procedure is used to set export project name, export mode and list of layers *
* that should be exported.                                                     *
*                                                                              *
* PARAMETERS: EXPPROJ     -> Name of the diretory into that should be exported *
*             EXPPROJMODE -> Mode how it should be exported.                   *
*                            0 -> Export all layers                            *
*                            1 -> Export selected layers                       *
*                            2 -> Export selected objects                      *
*                                                                              *
********************************************************************************}
procedure SETEXPSETTINGS(EXPPROJ:PCHAR; EXPPROJMODE:INTEGER);stdcall;
begin
   SelShpWnd.SetExpSettings(strpas(EXPPROJ), EXPPROJMODE);
end;

// procedure is used to add a layer that should be exported
// if only selected layers should be exported
procedure ADDEXPLAYER(ALAYER:PCHAR);stdcall;
begin
   SelShpWnd.AddExpLayer(strpas(ALAYER));
end;

// procedure is used to set database-settings
procedure SETDBSETTINGS(SOURCEDBMODE:INTEGER; SOURCEDBNAME:PCHAR; SOURCEDBTABLE:PCHAR;
                        DESTDBMODE  :INTEGER; DESTDBNAME  :PCHAR; DESTDBTABLE  :PCHAR);stdcall;
begin
   // destination will be ignored at SHP-Export
   MainWnd.AXImpExpDbc.ExpDBMode:=SOURCEDBMODE;
   MainWnd.AXImpExpDbc.ExpDatabase:=strpas(SOURCEDBNAME);
   MainWnd.AXImpExpDbc.ExpTable:=strpas(SOURCEDBTABLE);
end;

// procedure is used to set projection for batch mode
procedure SETPROJECTION(SOURCECOORDINATESYSTEM:INTEGER; // source projection
                        SOURCEPROJECTION      :PCHAR;
                        SOURCEDATE            :PCHAR;
                        SOURCEPROJSETTINGS    :PCHAR;
                        SOURCEXOFFSET         :DOUBLE;
                        SOURCEYOFFSET         :DOUBLE;
                        SOURCESCALE           :DOUBLE;
                        TARGETCOORDINATESYSTEM:INTEGER; // target projection
                        TARGETPROJECTION      :PCHAR;
                        TARGETDATE            :PCHAR;
                        TARGETPROJSETTINGS    :PCHAR;
                        TARGETXOFFSET         :DOUBLE;
                        TARGETYOFFSET         :DOUBLE;
                        TARGETSCALE           :DOUBLE);stdcall;
begin
   // source projection
   MainWnd.AxGisProjection.SourceCoordinateSystem:=SOURCECOORDINATESYSTEM;
   MainWnd.AxGisProjection.SourceProjection:=strpas(SOURCEPROJECTION);
   MainWnd.AxGisProjection.SourceDate:=strpas(SOURCEDATE);
   MainWnd.AxGisProjection.SourceProjSettings:=strpas(SOURCEPROJSETTINGS);
   MainWnd.AxGisProjection.SourceXOffset:=SOURCEXOFFSET;
   MainWnd.AxGisProjection.SourceYOffset:=SOURCEYOFFSET;
   MainWnd.AxGisProjection.SourceScale:=SOURCESCALE;

   // target projection
   MainWnd.AxGisProjection.TargetCoordinateSystem:=TARGETCOORDINATESYSTEM;
   MainWnd.AxGisProjection.TargetProjection:=strpas(TARGETPROJECTION);
   MainWnd.AxGisProjection.TargetDate:=strpas(TARGETDATE);
   MainWnd.AxGisProjection.TargetProjSettings:=strpas(TARGETPROJSETTINGS);
   MainWnd.AxGisProjection.TargetXOffset:=TARGETXOFFSET;
   MainWnd.AxGisProjection.TargetYOffset:=TARGETYOFFSET;
   MainWnd.AxGisProjection.TargetScale:=TARGETSCALE;
end;

// function is used to execute an SHP-export
procedure EXECEXP;stdcall;
begin
   MainWnd.ExitNormal:=true;
   MainWnd.Show;
end;

function FREEDLL:boolean; stdcall;
var retVal:boolean;
begin
   retVal:=MainWnd.ExitNormal;
   MainWnd.Free;
   SelShpWnd.Free;
   result:=retVal;
end;

exports SHPEXPORT, CREATEEXP, SETEXPSETTINGS, ADDEXPLAYER,
        SETDBSETTINGS, SETPROJECTION, EXECEXP, FREEDLL;
begin
end.
