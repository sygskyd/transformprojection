unit struct;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, db;

const
       Col_Int      = 0;
       Col_SmallInt = 1;
       Col_Float    = 2;
       Col_Date     = 3;
       Col_String   = 4;

type
  { Hier k�nnen Strukturen definiert werden }

   BoundingBox = record
      X_min :double;
      Y_min :double;
      X_max :double;
      Y_max :double;
   end;

   p_Punkt_record=^Punkt_record;
   Punkt_record = record
       X:double;
       Y:double;
       next :p_Punkt_record;
   end;

   p_IslandIdx_record=^IslandIdx_record;
   IslandIdx_record = record
      Index:integer;
      next :p_IslandIdx_record;
   end;


   p_Datenbank_record =^Datenbank_record;
   Datenbank_record = record
      Bezeichnung : string;
      Laenge      : integer;
      Typ         : integer;
      Inhalt      : string;
      von         : integer;
      bis         : integer;
      Art         : boolean;
      MappedName  : string;
      next        : p_Datenbank_record;
   end;

   p_Polygon_record =^Polygon_record;
   Polygon_record = record
      Punktanz :integer;
      Box       :Boundingbox;
      Punkte    :p_Punkt_record;
      IslandsIdx:p_IslandIdx_record;
      NumIslands:integer;
      DBInfo    :p_Datenbank_record;
      next      :p_Polygon_record;
   end;

   p_PunktObj_record = ^PunktObj_record;
   PunktObj_record = record
      X :double;
      Y :double;
      DBInfo:p_Datenbank_record;
      next :p_Punktobj_record;
   end;

   p_Layer_record = ^Layer_record;
   Layer_record = record
      Layername:string;
      Punkte        :p_PunktObj_record;
      PunkteBox     :Boundingbox;
      Punktesize    :longint;
      NumPoints     :integer;
      Linien        :p_Polygon_record;
      LinienBox     :Boundingbox;
      Liniensize    :longint;
      NumLines      :integer;
      Flaechen      :p_Polygon_record;
      FlaechenBox   :Boundingbox;
      Flaechensize  :longint;
      NumCPolys     :integer;
      DBColumns     :p_Datenbank_record;
      next          :p_Layer_record;
   end;

ClassPolygon = class
   public
      { public Definitionen }
      constructor Create;
      procedure Neu(Layername:string;Objekttyp:integer);
      procedure AddIslandIdx(Index:integer);
      procedure InsertPoint(X:double;Y:double;Typ:integer);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string;Objekttyp:integer);
      procedure WritePolygons(Art:integer);
   private
      { private Definitionen }
end;

ClassDatenbank = class
   public
      { public Definitionen }
      constructor Create;
      procedure Neu(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string; von:integer; bis:integer;Art:boolean; var Anfang:p_datenbank_record);
      function  Checkexists(Eintrag:string):boolean;
      procedure SetLength(Bezeichnung:string; Laenge:integer);
      function  GetLength(Bezeichnung:string):integer;
      function  GetDataType(Bezeichnung:string):integer;
      function  GetItem(Bezeichnung:string):string;
      procedure Getvonbis(Bezeichnung:string;var von:integer; var bis:integer);
      procedure Insertcolumn(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string;von:integer;bis:integer;Art:boolean);
      procedure ResetColumn;
      procedure ValDatenbank(Zeile:string;Objekttyp:integer);
      function  GetMappedColName(ColName:string; var ColInfos:p_datenbank_record):string;
      procedure Init;
   private
      { private Definitionen }
end;

ClassPunkt = class
   public
      { public Definitionen }
      constructor Create;
      procedure Neu(Layername:string; X:double; Y:double);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      procedure WritePoints;
   private
      { private Definitionen }
end;

ClassLayer = class
   public
      { public Definitionen }
      constructor Create;
      destructor Destroy;
      procedure Neu(Layername:string);
      function Checkexists(Layername:string):boolean;
      procedure ResetLayerZeiger;
      function GetAnz:longint;
      function GetAktLayername:string;
      function CheckAreas:boolean;
      function CheckPoints:boolean;
      function CheckLines:boolean;
      function GetNumObjectsToWrite:integer;
      procedure NextLayer;
      procedure InsertDBColumn(Spaltenname:string);
      function CreateDBF(Name:string):boolean;
      function GetColumnDefinition:p_Datenbank_record;
      function InsertColumnToTable(Tablename:string; ColName:string; ColLen:integer; ColType:integer; InsertPos:integer):boolean;
   private
      { private Definitionen }
      ColumnAnzahl:integer;
      DBObj:ClassDatenbank;
end;

implementation
uses files,maindlg;

var Akt_Punkt  :p_Punktobj_record;
    Akt_Linie  :p_Polygon_record;
    Akt_Flaeche:p_Polygon_record;
    Datenbank_Anfang, Akt_Datenbank:p_Datenbank_record;
    Layer_Anfang, Akt_Layer:p_Layer_record;
    Akt_ColumnInfo:p_Datenbank_record;
    FileObj     :ClassFile;
    PunktObj    :ClassPunkt;
    PolygonObj  :ClassPolygon;
    DatenbankObj:ClassDatenbank;
    LayerObj    :ClassLayer;
    curmapsearch     :p_datenbank_record;
    OldMapSearchStart:p_datenbank_record;

constructor ClassPolygon.Create;
begin
end;

constructor ClassLayer.Create;
begin
   DBObj:=ClassDatenbank.Create;
   ColumnAnzahl:=0;
   Layer_anfang:=nil; Akt_layer:=nil;
   DatenbankObj.Init;
end;

destructor ClassLayer.Destroy;
begin
   DBObj.Destroy;
end;

constructor ClassDatenbank.Create;
begin
end;

procedure ClassDatenbank.Init;
begin
   Akt_Flaeche:=nil;
   Akt_Punkt:=nil;
   Akt_Linie:=nil;
   Datenbank_Anfang:=nil;
   Akt_Datenbank:=nil;
   Layer_Anfang:=nil;
   Akt_Layer:=nil;
   curmapsearch:=nil;
   OldMapSearchStart:=nil;
end;

constructor ClassPunkt.Create;
begin

end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.Checkexists                                        *
********************************************************************************
* Funktion �berpr�ft, ob der entsprechende Spalteneintrag bereits in der       *
* Datenbankliste ist.                                                          *
*                                                                              *
* R�ckgabewert: TRUE  -> Eintrag ist bereits in Liste.                         *
*               FALSE -> Eintrag ist noch nicht in Liste.                      *
*                                                                              *
********************************************************************************}
function ClassDatenbank.Checkexists(Eintrag:string):boolean;
var Zeiger:p_Datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   if Zeiger = nil then begin Result:=false; exit; end;
   repeat
      if Zeiger^.Bezeichnung = Eintrag then gefunden:= true;
      Zeiger:=Zeiger^.next;
   until (Zeiger = nil) or (gefunden = true);
   Result:=gefunden;
end;

{*******************************************************************************
* PROZEDUR : ClassPolygon.InsertAttrib                                         *
********************************************************************************
* Prozedur f�gt einen Datenbankinformationseintrag zu einem Polygon ein.       *
*                                                                              *
* PARAMETER: Spaltenname   -> Name der Spalte die eingef�gt werden soll.       *
*            Spalteninhalt -> Inhalt der Datenbankspalte.                      *
*            Objekttyp     -> Objekttyp 1 ist Linie, Objekttyp 2 ist Fl�che    *
*                                                                              *
********************************************************************************}
procedure ClassPolygon.InsertAttrib(Spaltenname:string;Spalteninhalt:string;Objekttyp:integer);
begin
   case Objekttyp of
      1:begin DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,0,0,TRUE,Akt_Linie^.DBInfo); DatenbankObj.Setlength(Spaltenname,length(Spalteninhalt)); end;
      2:begin DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,0,0,TRUE,Akt_Flaeche^.DBInfo); DatenbankObj.Setlength(Spaltenname,length(Spalteninhalt)); end;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.ValDatenbank                                       *
********************************************************************************
* Prozedur wertet die Datenbankinformation aus der erhaltenen Zeile aus.       *
*                                                                              *
* PARAMETER: Zeile -> String aus der die Datenbankinformation ausgelesen wird. *
*            Objekttyp     -> Objekttyp 1 ist Linie, Objekttyp 2 ist Fl�che    *
*                             Objekttyp 0 ist Punkt.                           *
********************************************************************************}
procedure ClassDatenbank.ValDatenbank(Zeile:string;Objekttyp:integer);
var Zeiger:p_Datenbank_record;
    hstr:string;
    von,bis:integer;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_Anfang; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   repeat
      zeiger:=zeiger^.next;
      if Zeiger <> nil then if Zeiger^.Art = TRUE then gefunden:=true;
   until (Zeiger = nil) or (gefunden = true);
   if zeiger <> nil then
   repeat
      Getvonbis(Zeiger^.Bezeichnung,von,bis); hstr:=copy(Zeile,von,bis-von + 1);
      hstr:=Trimleft(hstr); hstr:=Trimright(hstr);
      case Objekttyp of
         0,3,6:PunktObj.InsertAttrib(Zeiger^.Bezeichnung,hstr);
         1,2: PolygonObj.InsertAttrib(Zeiger^.Bezeichnung,hstr,Objekttyp);
      end;
      Zeiger:=Zeiger^.next;
   until Zeiger = nil;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.ResetLayerZeiger                                       *
********************************************************************************
* Prozedur setzt den Akt_Layer zeiger auf den Anfang                           *
*                                                                              *
********************************************************************************}
procedure ClassLayer.ResetLayerZeiger;
begin
   Akt_layer:=Layer_Anfang;
   Akt_ColumnInfo:=Akt_layer^.DBColumns;
end;

function ClassLayer.GetColumnDefinition:p_Datenbank_record;
begin
   result:=Akt_layer^.DBColumns;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.GetAktLayerName                                        *
********************************************************************************
* Funktion ermittelt den Namen des Akt_Layer                                   *
*                                                                              *
********************************************************************************}
function ClassLayer.GetAktLayerName:string;
var hpchar:pchar;
    i:integer;
begin
   { Es wird �berpr�ft, ob sich nicht irgendeine Sonderzeichen im Namen befinden, die nicht erlaubt sind }
   hpchar:=stralloc(length(Akt_layer^.Layername)+1); strpcopy(hpchar,Akt_layer^.Layername);
   for i:=1 to length(Akt_layer^.Layername) do
   begin
      if hpchar[i] = CHR(148) then hpchar[i]:='�';
      if hpchar[i] = CHR(153) then hpchar[i]:='�';
      if hpchar[i] = CHR(132) then hpchar[i]:='�';
      if hpchar[i] = CHR(142) then hpchar[i]:='�';
      if hpchar[i] = CHR(129) then hpchar[i]:='�';
      if hpchar[i] = CHR(154) then hpchar[i]:='�';
      if hpchar[i] = CHR(225) then hpchar[i]:='�';
      if hpchar[i] = '/' then hpchar[i]:='_';
      if hpchar[i] = '\' then hpchar[i]:='_';
   end;
   Akt_Layer^.Layername:=strpas(hpchar);
   if pos('"',Akt_Layer^.Layername) <> 0 then repeat delete(Akt_Layer^.Layername,Pos('"',Akt_Layer^.Layername),1); until Pos('"',Akt_Layer^.Layername) = 0;
   strdispose(hpchar);
   Result:=Akt_layer^.Layername;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.CheckPoints                                            *
********************************************************************************
* Funktion �berpr�ft ob auf dem Akt_Layer Punkte vorhanden sind.               *
*                                                                              *
* R�CKGABEWERT: TRUE  -> es sind Punkte vorhanden                              *
*               FALSE -> es sind keine Punkte vorhanden                        *
*                                                                              *
********************************************************************************}
function ClassLayer.CheckPoints:boolean;
begin
   if Akt_Layer^.Punkte <> nil then result:=true else result:=false;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.CheckLines                                             *
********************************************************************************
* Funktion �berpr�ft ob auf dem Akt_Layer Linien vorhanden sind.               *
*                                                                              *
* R�CKGABEWERT: TRUE  -> es sind Linien vorhanden                              *
*               FALSE -> es sind keine Linien vorhanden                        *
*                                                                              *
********************************************************************************}
function ClassLayer.CheckLines:boolean;
begin
   if Akt_Layer^.Linien <> nil then result:=true else result:=false;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.CheckAreas                                             *
********************************************************************************
* Funktion �berpr�ft ob auf dem Akt_Layer Fl�chen vorhanden sind.              *
*                                                                              *
* R�CKGABEWERT: TRUE  -> es sind Fl�chen vorhanden                             *
*               FALSE -> es sind keine Fl�chen vorhanden                       *
*                                                                              *
********************************************************************************}
function ClassLayer.CheckAreas:boolean;
begin
   if Akt_Layer^.Flaechen <> nil then result:=true else result:=false;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.GetAnz                                                 *
********************************************************************************
* Funktion ermittelt die Anzahl der Layer die sich in der Liste befinden.      *
*                                                                              *
********************************************************************************}
function ClassLayer.GetAnz:longint;
var anz:longint;
    Zeiger:p_Layer_record;
begin
   anz:=0; zeiger:=Layer_Anfang;
   if zeiger <> nil then
   repeat
      Zeiger:=Zeiger^.next; anz:=anz + 1;
   until Zeiger = nil;
   result:=anz;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.nextlayer                                              *
********************************************************************************
* Prozedur setzt den Akt_Layer zeiger weiter.                                  *
*                                                                              *
********************************************************************************}
procedure ClassLayer.NextLayer;
begin
   Akt_Layer:=Akt_Layer^.next;
   if Akt_Layer <> nil then Akt_ColumnInfo:=Akt_layer^.DBColumns;
end;

{*******************************************************************************
* PROZEDUR : ClassPunkt.InsertAttrib                                           *
********************************************************************************
* Prozedur f�gt einen Datenbankinformationseintrag zum Akt_Punkt ein.          *
*                                                                              *
* PARAMETER: Spaltenname   -> Name der Spalte die eingef�gt werden soll.       *
*            Spalteninhalt -> Inhalt der Datenbankspalte.                      *
*                                                                              *
********************************************************************************}
procedure ClassPunkt.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,0,0,TRUE,Akt_Punkt^.DBInfo);
   DatenbankObj.Setlength(Spaltenname,length(Spalteninhalt));
end;

{*******************************************************************************
* PROZEDUR : ClassPunkt.WritePoints                                            *
********************************************************************************
* Prozedur erzeugt eine Punkt-Shapedatei des aktuellen Layers.                 *
*                                                                              *
********************************************************************************}
procedure ClassPunkt.WritePoints;
var Record_number:longint;
    Zeiger :p_PunktObj_record;
    DBZeiger:p_Datenbank_record;
    Offset :longint;
    Content_length:longint;
    ColName:string;
begin
   { Schreiben des Headers }
   FileObj.WriteHeader(1,Akt_Layer^.Punktesize,Akt_Layer^.PunkteBox.X_min,Akt_Layer^.PunkteBox.Y_min,
                       Akt_Layer^.PunkteBox.X_max,Akt_Layer^.PunkteBox.Y_max, Akt_layer^.NumPoints);

   { Nun k�nnen die Punktdaten geschrieben werden }
   Zeiger:=Akt_Layer^.Punkte; Record_number:=1; Offset:=50;
   Content_length:=10; // 2 is shapetype, 8 is X/Y
   repeat
       FileObj.WriteSHXBlock(Offset,Content_length);

       FileObj.WriteRecordHeader(Record_number,Content_length);  Offset:=Offset + 4;
       FileObj.WriteShapetype(1); Offset:=Offset + 2;
       FileObj.WriteXY(Zeiger^.X,Zeiger^.Y); Offset:=Offset + 8;

       { Schreiben der Datenbankinformation }
       if Zeiger^.DBInfo <> nil then
       begin
          MainWnd.ADOTable.Append;
          DBZeiger:=Zeiger^.DBInfo;
          repeat
             ColName:=DatenbankObj.GetMappedColName(DBZeiger^.Bezeichnung, Akt_Layer^.DbColumns);
             if ColName <> '' then
                MainWnd.ADOTable.FieldByName(ColName).AsString:=DBZeiger^.Inhalt;
             DBZeiger:=DBZeiger^.next;
          until DBZeiger = nil;
          MainWnd.ADOTable.Post;
       end
       else
       begin
          MainWnd.ADOTable.Append;
          MainWnd.ADOTable.FieldByName('INFO').AsString:='0';
          MainWnd.ADOTable.Post;
       end;
       Zeiger:=Zeiger^.next;
       Record_Number:=Record_Number+1;
       MainWnd.ObjectsWritten:=MainWnd.ObjectsWritten+1;
       MainWnd.DisplayObjectsWrittenPercent;
   until (Zeiger = nil) or (MainWnd.Abort);
end;

function ClassLayer.GetNumObjectsToWrite:integer;
var retVal:Integer;
begin
   retVal:=Akt_Layer^.NumPoints+Akt_Layer^.NumLines+Akt_Layer^.NumCPolys;
   result:=retVal;
end;

function ClassLayer.InsertColumnToTable(Tablename:string; ColName:string; ColLen:integer; ColType:integer; InsertPos:integer):boolean;
var CommandStr:string;
    wasok:boolean;
    retrycnt:integer;
    CurColName:string;
begin
   retrycnt:=0; CurColName:=ColName;
   result:=TRUE;
   repeat
      wasok:=TRUE;
      // insert column to table
      if InsertPos = 0 then
         Commandstr:='Create Table '+'['+Tablename+'] ('
      else
         Commandstr:='Alter Table '+'['+Tablename+'] ADD COLUMN ';
      // now add the column information itself
           if ColType = Col_Int      then Commandstr:=Commandstr + '['+CurColName+']'+' integer'
      else if ColType = Col_Float    then Commandstr:=Commandstr + '['+CurColName+']'+ ' double'
      else if ColType = Col_SmallInt then Commandstr:=Commandstr + '['+CurColName+']'+ ' integer'
      else Commandstr:=Commandstr + '['+CurColName+']'+ ' Text(' +inttostr(ColLen)+ ') ';

      if InsertPos = 0 then
         Commandstr:=Commandstr+')'
      else
         Commandstr:=Commandstr+';';
      MainWnd.ADOCommand.CommandText:=Commandstr;
      try
         MainWnd.ADOCommand.Execute;
      except
         // insert of column failed
         if retrycnt = 0 then
         begin
            // maybe the Fieldname is not correct
            // -> make correct dBase Fieldname
            CurColName:=ColName;
            MainWnd.MakeCorrectdBaseColumnname(CurColName);
         end
         else
         begin
            CurColName:='FIELD'+inttostr(retrycnt);
         end;
         wasok:=FALSE;
         retrycnt:=retrycnt+1;
      end;
   until (wasok = TRUE) or (retrycnt = 10);
   if (retrycnt = 10) then
      result:=FALSE;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.CreateDBF                                              *
********************************************************************************
* Prozedur erzeugt ein DBF File mit den entsprechenden Datenbankspalten.       *
*                                                                              *
* PARAMETER: Name -> Name des DBF-Files das erzeugt werden soll.               *
*                                                                              *
* R�CKGABEWERT: TRUE-> Wenn DBF File erzeugt wurde, FALSE wenn nicht.          *
********************************************************************************}
function ClassLayer.CreateDBF(Name:string):boolean;
var Zeiger:p_Datenbank_record;
    allok:boolean;
    Laenge:integer;
    Typ:integer;
    ColName:string;
    CommandStr:string;
    colpos    :integer;
begin
   Zeiger:=Akt_ColumnInfo; allok:=false;
   allok:=true;

   if (FileExists(Name)) then // if the table already exists delete it
      DeleteFile(PCHAR(Name));
   if not DirectoryExists(ExtractFilePath(Name)) then
      CreateDir(ExtractFilePath(Name));

   MainWnd.ADOConnection.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False';
   MainWnd.ADOConnection.Properties['Extended Properties'].Value := 'dBASE 5.0';
   MainWnd.ADOConnection.Properties['Data Source'].Value :=extractfiledir(Name);
   MainWnd.ADOConnection.Connected:=True;

   MainWnd.ADOTable.ConnectionString:='Provider=MSDASQL.1;Persist Security Info=False;Data Source=dBASE Files';

   // now create the table
   colpos:=0;
   if Zeiger <> nil then
   begin
      while (Zeiger <> nil) do
      begin
         Laenge:=DatenbankObj.GetLength(Zeiger^.Bezeichnung);
         if Laenge > 128 then Laenge:=128;
         Typ:=DatenbankObj.GetDataType(Zeiger^.Bezeichnung);
         ColName:=Zeiger^.Bezeichnung;
         if not InsertColumnToTable(ExtractFilename(Name), ColName, Laenge, Typ, colpos) then
            Zeiger^.MappedName:=''
         else
            Zeiger^.MappedName:=ColName;
         colpos:=colpos+1;
         Zeiger:=Zeiger^.next;
      end;
   end
   else
   begin
      // insert dummy INFO column Col_String
      InsertColumnToTable(ExtractFilename(Name), 'INFO' , 20, Col_String, 0);
   end;

   // the table has been created now
   MainWnd.ADOTable.TableName:='['+ExtractFilename(Name)+']';
   MainWnd.ADOTable.Connection:=MainWnd.ADOConnection;
   MainWnd.ADOTable.Open;
   // now check if the columns that have been created have same name than in Column-Info
   Zeiger:=Akt_ColumnInfo;
   colpos:=0;
   if Zeiger <> nil then
   begin
      while (Zeiger <> nil) do
      begin
         if Zeiger^.MappedName <> '' then
         begin
            if colpos < MainWnd.ADOTable.FieldDefs.Count then
            begin
               ColName:=MainWnd.ADOTable.FieldDefs[colpos].Name;
               Zeiger^.MappedName:=ColName;
            end;
            colpos:=colpos+1;
         end;
         Zeiger:=Zeiger^.next;
      end;
   end;
   result:=allok;
end;

{*******************************************************************************
* PROZEDUR : ClassPolygon.WritePolygons                                        *
********************************************************************************
* Prozedur erzeugt ein Polygonshape des aktuellen Layers.                      *
*                                                                              *
* PARAMETER: Art -> Art des Polygons 1 ist Linie, 2 ist Flaeche                *
*                                                                              *
********************************************************************************}
procedure ClassPolygon.WritePolygons(Art:integer);
var Zeiger:p_Polygon_record;
    Punktzeiger:p_Punkt_record;
    DBZeiger:p_Datenbank_record;
    Shapetyp:integer;
    Record_Number,Offset:longint;
    Content_length:longint;
    numparts:integer;
    IslandIdxIterate:p_IslandIdx_record;
    ColName:string;
begin
   Shapetyp:=0;
   Case Art of
      1: begin
            Shapetyp:=3;
            FileObj.WriteHeader(Shapetyp,Akt_Layer^.Liniensize,Akt_Layer^.LinienBox.X_min,Akt_Layer^.LinienBox.Y_min,
                                Akt_Layer^.LinienBox.X_max,Akt_Layer^.LinienBox.Y_max, Akt_Layer^.NumLines);
            Zeiger:=Akt_Layer^.Linien;
         end;
      2: begin
            Shapetyp:=5;
            FileObj.WriteHeader(Shapetyp,Akt_Layer^.Flaechensize,Akt_Layer^.FlaechenBox.X_min,Akt_Layer^.FlaechenBox.Y_min,
                                Akt_Layer^.FlaechenBox.X_max,Akt_Layer^.FlaechenBox.Y_max, Akt_Layer^.NumCPolys);
            Zeiger:=Akt_Layer^.Flaechen;
         end;
   end;

   { Nun k�nnen die Punktdaten geschrieben werden }
   Record_number:=1; Offset:=50;
   repeat
       // calculate length of used bytes for this poly
       Content_length:=0;
       Content_length:=content_length+2;  // ShapeType
       Content_length:=content_length+16; // Bounding Box (4 * double)
       Content_length:=content_length+2;  // Numparts
       Content_length:=content_length+2;  // Numpoints
       if Zeiger^.IslandsIdx = nil then Content_length:=Content_length+2 // Partsinformation
       else Content_Length:=Content_length+(Zeiger^.NumIslands*2);
       Content_length:=Content_length+(Zeiger^.Punktanz * 8); // Items of the poly (X = 4, Y = 4)
       // write entry to SHX-File
       FileObj.WriteSHXBlock(Offset,Content_length);

       { Schreiben des Recordheaders und des Shapetypes }
       FileObj.WriteRecordHeader(Record_number,Content_length);  Offset:=Offset + 4;
       FileObj.WriteShapetype(Shapetyp); Offset:=Offset + 2;

       { Schreiben der Boundingboxinformation }
       FileObj.WriteDouble('Little',Zeiger^.Box.X_min,'SHP'); Offset:=Offset + 4;
       FileObj.WriteDouble('Little',Zeiger^.Box.Y_min,'SHP'); Offset:=Offset + 4;
       FileObj.WriteDouble('Little',Zeiger^.Box.X_max,'SHP'); Offset:=Offset + 4;
       FileObj.WriteDouble('Little',Zeiger^.Box.Y_max,'SHP'); Offset:=Offset + 4;

       if Zeiger^.NumIslands=0 then numparts:=1
       else numparts:=Zeiger^.NumIslands;
       { Schreiben der Numpartsinformation }
       FileObj.WriteInteger('Little',numparts,'SHP'); Offset:=Offset + 2;

       { Schreiben der NumPointsinformation }
       FileObj.WriteInteger('Little',Zeiger^.Punktanz,'SHP'); Offset:=Offset + 2;

       if Zeiger^.IslandsIdx = nil then
       begin
          { Schreiben der Partsinformation - hier immer 0 }
          FileObj.WriteInteger('Little',0,'SHP'); Offset:=Offset + 2;
       end
       else
       begin
          IslandIdxIterate:=Zeiger^.IslandsIdx;
          while IslandIdxIterate <> nil do
          begin
             FileObj.WriteInteger('Little',IslandIdxIterate^.Index,'SHP'); Offset:=Offset+2;
             IslandidxIterate:=IslandIdxIterate^.next;
          end;
       end;

       { Schreiben der Koordinatenpaare }
       Punktzeiger:=Zeiger^.Punkte;
       repeat
          FileObj.WriteXY(PunktZeiger^.X,PunktZeiger^.Y); Offset:=Offset + 8;
          Punktzeiger:=Punktzeiger^.next;
       until PunktZeiger = nil;

       { Schreiben der Datenbankinformation }
       if Zeiger^.DBInfo <> nil then
       begin
          MainWnd.ADOTable.Append;
          DBZeiger:=Zeiger^.DBInfo;
          repeat
             ColName:=DatenbankObj.GetMappedColName(DBZeiger^.Bezeichnung, Akt_Layer^.DbColumns);
             if ColName <> '' then
                MainWnd.ADOTable.FieldByName(ColName).AsString:=DBZeiger^.Inhalt;
             DBZeiger:=DBZeiger^.next;
          until DBZeiger = nil;
          MainWnd.ADOTable.Post;
       end
       else
       begin
          // insert dummy item
          MainWnd.ADOTable.Append;
          MainWnd.ADOTable.FieldByName('INFO').AsString:='0';
          MainWnd.ADOTable.Post;
       end;
       Zeiger:=Zeiger^.next;
       Record_Number:=Record_Number+1;
       MainWnd.ObjectsWritten:=MainWnd.ObjectsWritten+1;
       MainWnd.DisplayObjectsWrittenPercent;
   until (Zeiger = nil) or (MainWnd.Abort)
end;


{*******************************************************************************
* PROZEDUR : ClassDatenbank.InsertColumn                                       *
********************************************************************************
* Prozedur erzeugt einen neuen Spalteneintrag f�r die Datenbank und f�gt diesen*
* in die Datenbank_anfang Liste ein.                                           *
*                                                                              *
* PARAMETER: Bezeichnung -> Spaltenbezeichnung                                 *
*            Laenge      -> L�nge der Spalte                                   *
*            Typ         -> Typ der Spalte                                     *
*            Inhalt      -> Inhalt der Spalte                                  *
*            von         -> Beginn der Spalte im ASC-File                      *
*            bis         -> Ende der Spalte im ASC-File                        *
*            Art         -> TRUE, wenn es sich um DB-Spalte handelt, sonst F   *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.Insertcolumn(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string;von:integer;bis:integer;Art:boolean);
begin
   Neu(Bezeichnung, Laenge, Typ, Inhalt,von,bis,Art,Datenbank_anfang);
end;


{*******************************************************************************
* PROZEDUR : ClassDatenbank.Getvonbis                                          *
********************************************************************************
* Prozedur dient dazu den von und bis Bereich aus der Datenbankliste zu        *
* erhalten.                                                                    *
*                                                                              *
* PARAMETER: Bezeichnung -> Name der Spalte aus dem ADF-File                   *
*            von         -> von Bereich aus dem ASC-File                       *
*            bis         -> bis Bereich aus dem ASC-File                       *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.Getvonbis(Bezeichnung:string;var von:integer; var bis:integer);
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then
   begin
      von:=Zeiger^.von;
      bis:=Zeiger^.bis;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.ResetColumn                                        *
********************************************************************************
* Prozedur dient dazu den Aktellen Spalteneintragszeiger auf den Anfang zu     *
* setzen.                                                                      *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.ResetColumn;
begin
   Akt_Datenbank:=Datenbank_Anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassPunkt.Neu                                                    *
********************************************************************************
* Prozedur erzeugt einen neuen Punkteintrag.                                   *
*                                                                              *
* PARAMETER: Layername -> Layer auf dem sich der Punkt befindet                *
*            X -> X-Koordinate des Punkts                                      *
*            Y -> Y-Koordinate des Punkts                                      *
*                                                                              *
********************************************************************************}
procedure ClassPunkt.Neu(Layername:string; X:double; Y:double);
var neuer_record,Zeiger:p_punktobj_record;
    ret:boolean;
begin
   ret:=LayerObj.Checkexists(Layername); { Wenn er exisitiert steht sein Wert schon im Akt_Layer }
   if ret = false then
      LayerObj.Neu(Layername);

   Akt_Layer^.Punktesize:=Akt_Layer^.Punktesize + 6; // for number (2), length(2), shapetype(2)
   Akt_Layer^.Punktesize:=Akt_Layer^.Punktesize + 8; // 4 for X, 4 for Y coordinates
   Akt_Layer^.NumPoints:=Akt_Layer^.NumPoints+1;

   { Setzen der Boundingbox-Daten }
   if Akt_Layer^.Punkte = NIL then
   begin
       Akt_Layer^.PunkteBox.X_min:=X;
       Akt_Layer^.PunkteBox.X_max:=X;
       Akt_Layer^.PunkteBox.Y_min:=Y;
       Akt_Layer^.PunkteBox.Y_max:=Y;
   end;

   if Akt_Layer^.PunkteBox.X_min > X then Akt_Layer^.PunkteBox.X_min:=X;
   if Akt_Layer^.PunkteBox.X_max < X then Akt_Layer^.PunkteBox.X_max:=X;
   if Akt_Layer^.PunkteBox.Y_min > Y then Akt_Layer^.PunkteBox.Y_min:=Y;
   if Akt_Layer^.PunkteBox.Y_max < Y then Akt_Layer^.PunkteBox.Y_max:=Y;

   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.DBInfo:=nil;

   if Akt_Layer^.Punkte = NIL then
   begin
      Akt_Layer^.Punkte:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=Akt_Layer^.Punkte;
      repeat
         if Zeiger^.next <> nil then Zeiger:=Zeiger^.next;
      until Zeiger^.next = nil;
      Zeiger^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
   end;
   Akt_Punkt:=neuer_record;
end;

{*******************************************************************************
* PROZEDUR : ClassPolygon.Neu                                                  *
********************************************************************************
* Prozedur erzeugt einen neuen Polygoneintrag. Die Punkte, die zum Polygon     *
* geh�ren, werden erst sp�ter mit der Prozedur InsertPoint eingetragen.        *
*                                                                              *
* PARAMETER: Layername -> Layer auf der sich das Objekt befindet               *
*            Objekttyp -> 1 bedeutet Linie, 2 bedeutet Fl�che                  *
*                                                                              *
********************************************************************************}
procedure ClassPolygon.Neu(Layername:string;Objekttyp:integer);
var neuer_record,Zeiger:p_polygon_record;
    ret:boolean;
    Anfang:p_polygon_record;
begin
   Anfang:=nil;
   { Es muss kontrolliert werden, ob der Eintrag f�r diesen Layer bereits existiert }
   ret:=LayerObj.Checkexists(Layername); { Wenn er exisitiert steht sein Wert schon im Akt_Layer }
   if ret = false then
      LayerObj.Neu(Layername);

   case Objekttyp of
      1: Anfang:=Akt_Layer^.Linien;
      2: Anfang:=Akt_Layer^.Flaechen;
   end;

   // calculate the filesize
   case Objekttyp of
      1: begin { Linie }
            Akt_Layer^.Liniensize:=Akt_Layer^.Liniensize + 6; { F�r den Header einer Linie }
            Akt_Layer^.Liniensize:=Akt_Layer^.Liniensize + 16; // 4 * Bounding box = 4
            Akt_Layer^.Liniensize:=Akt_Layer^.Liniensize + 4; // numparts=2, numpoints = 2
            Akt_Layer^.Liniensize:=Akt_Layer^.Liniensize + 2; // numparts information = 2 (only one for polyline)
            Akt_Layer^.NumLines:=Akt_Layer^.NumLines+1;
         end;
      2: begin { Fl�che }
            Akt_Layer^.Flaechensize:=Akt_Layer^.Flaechensize + 6; // number=2, length=2, shapetype=2
            Akt_Layer^.Flaechensize:=Akt_Layer^.Flaechensize + 16; // 4 * Bounding box = 4
            Akt_Layer^.Flaechensize:=Akt_Layer^.Flaechensize + 4; // numparts=2, numpoints = 2
            Akt_Layer^.NumCPolys:=Akt_Layer^.NumCPolys+1;
         end;
   end;
   new(neuer_record);
   neuer_record^.Punkte:=nil;
   neuer_record^.DBInfo:=nil;
   neuer_record^.IslandsIdx:=nil;
   neuer_record^.Punktanz:=0;
   neuer_record^.NumIslands:=0;

   neuer_record^.Box.X_min:=0; neuer_record^.Box.Y_min:=0;
   neuer_record^.Box.X_max:=0; neuer_record^.Box.Y_max:=0;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=Anfang;
      repeat
         if Zeiger^.next <> nil then Zeiger:=Zeiger^.next;
      until Zeiger^.next = nil;
      Zeiger^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
   end;
   case Objekttyp of
      1: begin Akt_Linie:=neuer_record; Akt_Layer^.Linien:=Anfang; end;
      2: begin Akt_Flaeche:=neuer_record; Akt_Layer^.Flaechen:=Anfang; end;
   end;
end;



{*******************************************************************************
* FUNKTION : ClassLayer.Checkexists                                            *
********************************************************************************
* Funktion �berpr�ft, ob dieser Layer bereits in der Liste vorkommt.           *
*                                                                              *
* PARAMETER: Layername -> Layername, dessen Vorkommen �berpr�ft werden soll.   *
*                                                                              *
* R�CKGABEWERT: TRUE -> Layer existiert bereits                                *
*               FALSE -> Layer existiert noch nicht                            *
********************************************************************************}
function ClassLayer.CheckExists(Layername:string):boolean;
var Zeiger:p_Layer_record;
    gefunden:boolean;
begin
   Zeiger:=Layer_Anfang; gefunden:=false;
   if Zeiger = nil then result:=false
   else
   begin
      repeat
         if Zeiger^.Layername <> Layername then Zeiger:=Zeiger^.next else gefunden:=true;
      until (gefunden = true) or (Zeiger = nil);
   end;
   if (Zeiger = nil) then result:=false else begin result:=true; Akt_Layer:=Zeiger; end;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.Neu                                                    *
********************************************************************************
* Prozedur erzeugt einen neuen Eintrag in der Layerlist.                       *
*                                                                              *
* PARAMETER: Layername -> Name des Layers, der neu eingef�gt werden soll.      *
*                                                                              *
********************************************************************************}
procedure ClassLayer.Neu(Layername:string);
var neuer_record,Zeiger:p_Layer_record;
begin
   new(neuer_record);
   neuer_record^.Layername:=Layername;

   neuer_record^.Punktesize:=50; neuer_record^.Liniensize:=50; neuer_record^.Flaechensize:=50;
   neuer_record^.NumPoints:=0;
   neuer_record^.NumLines:=0;
   neuer_record^.NumCPolys:=0;

   neuer_record^.Punkte:=nil;
   neuer_record^.PunkteBox.X_min:=0; neuer_record^.PunkteBox.Y_min:=0;
   neuer_record^.PunkteBox.X_max:=0; neuer_record^.PunkteBox.Y_max:=0;

   neuer_record^.Linien:=nil;
   neuer_record^.LinienBox.X_min:=0; neuer_record^.LinienBox.Y_min:=0;
   neuer_record^.LinienBox.X_max:=0; neuer_record^.LinienBox.Y_max:=0;

   neuer_record^.Flaechen:=nil;
   neuer_record^.FlaechenBox.X_min:=0; neuer_record^.FlaechenBox.Y_min:=0;
   neuer_record^.FlaechenBox.X_max:=0; neuer_record^.FlaechenBox.Y_max:=0;

   if Layer_Anfang = NIL then
   begin
      Layer_Anfang:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=Layer_Anfang;
      repeat
         if Zeiger^.next <> nil then Zeiger:=Zeiger^.next;
      until Zeiger^.next = nil;
      Zeiger^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
   end;
   Akt_Layer:=neuer_record;
   Akt_Layer^.DBColumns:=nil;
end;

procedure ClassLayer.InsertDBColumn(Spaltenname:string);
begin
   DBObj.Neu(Spaltenname,0,Col_String,'',0,0,true,Akt_Layer^.DBColumns);
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.Neu                                                *
********************************************************************************
* Prozedur erzeugt einen neuen Spalteneintrag f�r die Datenbank.               *
*                                                                              *
* PARAMETER: Bezeichnung -> Spaltenbezeichnung                                 *
*            Laenge      -> L�nge der Spalte                                   *
*            Typ         -> Typ der Spalte                                     *
*            Inhalt      -> Inhalt der Spalte                                  *
*            von         -> Beginn der Spalte im ASC-File                      *
*            bis         -> Ende der Spalte im ADF-File                        *
*            Art         -> Art der Spalte T -> DB-Spalte F -> ADF-Spalte      *
*            Anfang      -> Zeiger auf Anfang der Liste in die eingef�gt wird  *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.Neu(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string; von:integer; bis:integer; Art:boolean; var Anfang:p_datenbank_record);
var neuer_record,Zeiger:p_datenbank_record;
begin
   new(neuer_record);

   neuer_record^.Bezeichnung:=Bezeichnung;
   neuer_record^.Laenge:=Laenge;
   neuer_record^.Typ:=Typ;
   neuer_record^.Inhalt:=Inhalt;
   neuer_record^.von:=von;
   neuer_record^.bis:=bis;
   neuer_record^.Art:=Art;
   neuer_record^.Mappedname:='';

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=Anfang;
      repeat
         if Zeiger^.next <> nil then Zeiger:=Zeiger^.next;
      until Zeiger^.next = nil;
      Zeiger^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
   end;
end;


{*******************************************************************************
* PROZEDUR : ClassDatenbank.SetLength                                          *
********************************************************************************
* Prozedur dient dazu die erforderliche Spaltenbreite einer Spalte zu setzen.  *
*                                                                              *
* InPARAMETER: Bezeichnung -> Name der Spalte                                  *
*              Laenge      -> Breite der Spalte                                *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.SetLength(Bezeichnung:string; Laenge:integer);
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if Zeiger^.Laenge < Laenge then Zeiger^.Laenge:=Laenge;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.GetLength                                          *
********************************************************************************
* Funktion dient zum ermitteln der erforderlichen Spaltenbreite f�r einen ADF  *
* Eintrag.                                                                     *
*                                                                              *
* PARAMETER:   Bezeichnung -> Name der Spalte                                  *
*                                                                              *
* R�ckgabewert: Spaltenbreite f�r die entsprechende Spalte.                    *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetLength(Bezeichnung:string):integer;
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then Result:=Zeiger^.Laenge else Result:=0;
end;

function ClassDatenbank.GetDataType(Bezeichnung:string):integer;
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then Result:=Zeiger^.Typ else Result:=Col_String;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.GetItem                                            *
********************************************************************************
* Funktion dient zum Ermitteln eines Datenbankwertes.                          *
*                                                                              *
*                                                                              *
* PARAMETER:   Bezeichnung -> Name der Spalte                                  *
*                                                                              *
* R�ckgabewert: Inhalt dieser Spalte.                                          *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetItem(Bezeichnung:string):string;
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then Result:=Zeiger^.Inhalt else Result:='';
end;

procedure ClassPolygon.AddIslandIdx(Index:integer);
var iterate    :p_IslandIdx_record;
    new_record :p_IslandIdx_record;
begin
   iterate:=Akt_Flaeche^.IslandsIdx;
   Akt_Flaeche^.NumIslands:=Akt_Flaeche^.NumIslands+1;
   new(new_record);
   new_record^.Index:=Index;
   if iterate = nil then
   begin
      Akt_Flaeche^.IslandsIdx:=new_record;
      Akt_Flaeche^.IslandsIdx^.next:=nil;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      while (iterate^.next <> nil) do iterate:=iterate^.next;
      iterate^.next:=new_record;
      new_record^.next:=nil;
   end;
   Akt_Layer^.Flaechensize:=Akt_Layer^.Flaechensize+2; // parts information
end;

{*******************************************************************************
* PROZEDUR : ClassPolygon.InsertPoint                                          *
********************************************************************************
* Prozedur f�gt ein Punktpaar zu einem Polygon ein. Akt_Flaeche oder Akt_Linie *
*                                                                              *
* InPARAMETER: X -> X-Koordinate des neuen Eckpunktes                          *
*              Y -> Y-Koordinate des neuen Eckpunktes                          *
*              Typ -> 1 ist Linie, 2 ist Fl�che                                *
*                                                                              *
********************************************************************************}
procedure ClassPolygon.InsertPoint(X:double;Y:double;Typ:integer);
var Zeiger    :p_Punkt_record;
    new_Zeiger:p_Punkt_record;
begin
   { Erh�hen der Filesize um diese 16 Byte, die neu dazukommen }
   case Typ of
      1: Akt_Layer^.Liniensize:=Akt_Layer^.Liniensize + 8; { Hier wird 8 addiert, weil in 16 Bit werten gerechnet wird }
      2: Akt_Layer^.Flaechensize:=Akt_Layer^.Flaechensize + 8; { Hier wird 8 addiert, weil in 16 Bit werten gerechnet wird }
   end;

   { Setzen der Boundingbox Daten  }
   if Typ = 1 then { Es handelt sich um eine Flaeche }
   begin
      { Setzen der Boundingbox f�r den gesamten Shape }
      if Akt_Layer^.NumLines = 1 then // it�s the first line
      begin
         Akt_Layer^.LinienBox.X_min:=X;
         Akt_Layer^.LinienBox.X_max:=X;
         Akt_Layer^.LinienBox.Y_min:=Y;
         Akt_Layer^.LinienBox.Y_max:=Y;
      end;
      if Akt_Layer^.LinienBox.X_min > X then Akt_Layer^.LinienBox.X_min:=X;
      if Akt_Layer^.LinienBox.X_max < X then Akt_Layer^.LinienBox.X_max:=X;
      if Akt_Layer^.LinienBox.Y_min > Y then Akt_Layer^.LinienBox.Y_min:=Y;
      if Akt_Layer^.LinienBox.Y_max < Y then Akt_Layer^.LinienBox.Y_max:=Y;

      if Akt_Linie^.Punkte = nil then // it is the first corner
      begin
         Akt_Linie^.Box.X_min:=X;
         Akt_Linie^.Box.X_max:=X;
         Akt_Linie^.Box.Y_min:=Y;
         Akt_Linie^.Box.Y_max:=Y;
      end;

      if Akt_Linie^.Box.X_min > X then Akt_Linie^.Box.X_min:=X;
      if Akt_Linie^.Box.X_max < X then Akt_Linie^.Box.X_max:=X;
      if Akt_Linie^.Box.Y_min > Y then Akt_Linie^.Box.Y_min:=Y;
      if Akt_Linie^.Box.Y_max < Y then Akt_Linie^.Box.Y_max:=Y;
   end;

   if Typ = 2 then { Es handelt sich um eine Flaeche }
   begin
      { Setzen der Boundingbox f�r den gesamten Shape }
      if Akt_Layer^.NumCPolys = 1 then // it is the first cpoly
      begin
         Akt_Layer^.FlaechenBox.X_min:=X;
         Akt_Layer^.FlaechenBox.X_max:=X;
         Akt_Layer^.FlaechenBox.Y_min:=Y;
         Akt_Layer^.FlaechenBox.Y_max:=Y;
      end;

      if Akt_Layer^.FlaechenBox.X_min > X then Akt_Layer^.FlaechenBox.X_min:=X;
      if Akt_Layer^.FlaechenBox.X_max < X then Akt_Layer^.FlaechenBox.X_max:=X;
      if Akt_Layer^.FlaechenBox.Y_min > Y then Akt_Layer^.FlaechenBox.Y_min:=Y;
      if Akt_Layer^.FlaechenBox.Y_max < Y then Akt_Layer^.FlaechenBox.Y_max:=Y;

      if Akt_Flaeche^.Punkte = nil then // it is the first corner
      begin
         Akt_Flaeche^.Box.X_min:=X;
         Akt_Flaeche^.Box.X_max:=X;
         Akt_Flaeche^.Box.Y_min:=Y;
         Akt_Flaeche^.Box.Y_max:=Y;
      end;

      if Akt_Flaeche^.Box.X_min > X then Akt_Flaeche^.Box.X_min:=X;
      if Akt_Flaeche^.Box.X_max < X then Akt_Flaeche^.Box.X_max:=X;
      if Akt_Flaeche^.Box.Y_min > Y then Akt_Flaeche^.Box.Y_min:=Y;
      if Akt_Flaeche^.Box.Y_max < Y then Akt_Flaeche^.Box.Y_max:=Y;
   end;

   new(new_Zeiger);
   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   case Typ of
      1: begin zeiger:=Akt_Linie^.Punkte; Akt_Linie^.Punktanz:=Akt_Linie^.Punktanz + 1; end;
      2: begin zeiger:=Akt_Flaeche^.Punkte; Akt_Flaeche^.Punktanz:=Akt_Flaeche^.Punktanz + 1; end;
   end;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      case Typ of
         1: begin Akt_Linie^.Punkte:=new_Zeiger; Akt_Linie^.Punkte^.next:=nil; end;
         2: begin Akt_Flaeche^.Punkte:=new_Zeiger; Akt_Flaeche^.Punkte^.next:=nil; end;
      end;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
   end;
end;

function ClassDatenbank.GetMappedColName(ColName:string; var ColInfos:p_datenbank_record):string;
var iterate:p_Datenbank_record;
    retVal:string;
    found:boolean;
begin
   retVal:='';
   if OldMapSearchStart <> ColInfos then
      curmapsearch:=nil;
   OldMapSearchStart:=ColInfos;
   if (curmapsearch <> nil) and (curmapsearch^.next <> nil) then
   begin
      if curmapsearch^.Bezeichnung = ColName then begin retVal:=curmapsearch^.MappedName; result:=retVal; exit; end;
      if curmapsearch^.next^.Bezeichnung = ColName then
      begin
         curmapsearch:=curmapsearch^.next;
         retVal:=curmapsearch^.MappedName;
         result:=retVal;
         exit;
      end;
   end;
   iterate:=ColInfos; found:=FALSE;
   while (iterate <> nil) and (not found) do
   begin
      if iterate^.Bezeichnung = ColName then
      begin
         retVal:=iterate^.MappedName;
         curmapsearch:=iterate;
         found:=TRUE;
      end;
      iterate:=iterate^.next;
   end;
   result:=retVal;
end;

end.
