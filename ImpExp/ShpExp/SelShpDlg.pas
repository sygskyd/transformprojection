unit SelShpDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StrFile, StdCtrls, Buttons, FileCtrl, ComCtrls, ExtCtrls, ShellCtrls,
  FileSelectionPanel, WinOSInfo;

type
  TSelShpWnd = class(TForm)
    PageControl: TPageControl;
    TabSheetSource: TTabSheet;
    LayerLB: TListBox;
    TabSheetDest: TTabSheet;
    LayerCB: TCheckBox;
    OKBtn: TButton;
    CancelBtn: TButton;
    GeoBtn: TButton;
    Panel2: TPanel;
    SelectedCB: TCheckBox;
    SetupWindowTimer: TTimer;
    DbBtn: TButton;
    Panel1: TPanel;
    SourceDbLabel: TLabel;
    FileSelectionPanel: TFileSelectionPanel;
    ShellTreeView: TShellTreeView;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure LayerCBClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GeoBtnClick(Sender: TObject);
    procedure SelectedCBClick(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure DbBtnClick(Sender: TObject);
    procedure LayerLBClick(Sender: TObject);
    procedure FileSelectionPanelDirectoryChanged(Sender: TObject);
  private
    { Private declarations }
    DestinationShpDirectory :string;
    FirstActivate           :boolean;
    procedure CheckWindow;
  public
    { Public declarations }
    winheight:integer;
    winwidth :integer;
    wintop   :integer;
    winleft  :integer;
    OkBtnMode:boolean;
    procedure GetShpDir(var ShpDir:string);
    procedure CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);

    procedure SetupForBatch;
    procedure SetExpSettings(ProjectDir:string; ExpMode:integer);
    procedure AddExpLayer(ExpLayer:string);
  end;

var
  SelShpWnd     : TSelShpWnd;
  StrFileObj    : ClassStrfile;

implementation

uses Maindlg, inifiles;

{$R *.DFM}

procedure TSelShpWnd.FormCreate(Sender: TObject);
var myScale:double;
    oldHeight:integer;
    oldWidth:integer;
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   Self.Caption:=StrfileObj.Assign(1);                 { PROGIS ESRI-SHP Export 2000 }
   TabsheetSource.Caption:=StrfileObj.Assign(2);       { Source Project              }
   TabsheetDest.Caption:=StrfileObj.Assign(3);         { Destination ESRI-SHP        }
   LayerCB.Caption:=StrfileObj.Assign(4);              { Export only selected layers }
   SelectedCB.Caption:=StrfileObj.Assign(43);          { Export only selected objects }

   OkBtn.Caption:=StrfileObj.Assign(7);                { Ok }
   CancelBtn.Caption:=StrfileObj.Assign(8);            { Cancel }
   GeoBtn.Caption:=StrfileObj.Assign(9);               { Projection }
   DbBtn.Caption:=StrfileObj.Assign(5);                // Database

   LayerLB.enabled:=false;
   DestinationShpDirectory:='';

   FirstActivate:=true;
   oldHeight:=Self.Height;
   oldWidth:=Self.Width;
   Self.Height:=1;
   Self.Width:=1;

   myScale:=1; //100000 / Trunc(9600000/(Self.PixelsPerInch));
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
   
   // now adapt winheigt and winwidth
   winheight:=round(oldHeight*myScale);
   winwidth:=round(oldWidth*myScale);
end;

procedure TSelShpWnd.OKBtnClick(Sender: TObject);
var inifile:Tinifile;
begin
   MainWnd.ExitNormal:=true;

   // write the current settings to the ConGIS.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'ConGIS.ini');
   inifile.WriteString('SHPEXP','ExpDbMode',inttostr(MainWnd.AXImpExpDbc.ExpDbMode));
   inifile.WriteString('SHPEXP','ExpDatabase',MainWnd.AXImpExpDbc.ExpDatabase);
   inifile.WriteString('SHPEXP','ExpTable',MainWnd.AXImpExpDbc.ExpTable);
   inifile.WriteString('SHPEXP','LastUsedPath',FileSelectionPanel.Directory);
   inifile.Destroy;


   // now the export window can be opened and the export executed
   Self.Close;
   MainWnd.Show;
end;

procedure TSelShpWnd.CancelBtnClick(Sender: TObject);
begin
   MainWnd.ExitNormal:=false;
   MainWnd.CloseTimer.Enabled:=true;
end;

procedure TSelShpWnd.GetShpDir(var ShpDir:string);
begin
   ShpDir:=DestinationShpDirectory;
end;

procedure TSelShpWnd.LayerCBClick(Sender: TObject);
begin
   if LayerCB.Checked then
   begin
      LayerLB.enabled:=true;
      SelectedCB.Checked:=false;
   end
   else
   begin
      LayerLB.enabled:=false;
   end;
   CheckWindow;
end;

procedure TSelShpWnd.CheckWindow;
var allok:boolean;
    i:integer;
begin
   allok:=true;
   { Procedure checks all settings in the window }
   if DestinationShpDirectory = '' then allok:=false;
   if allok and LayerCb.Checked then
   begin
      allok:=false;
      for i:=0 to LayerLb.Items.Count-1 do
      begin
         if LayerLb.Selected[i] then
         begin
            allok:=true;
            break;
         end;
      end;
   end;
   if allok then OKBtn.Enabled:=true
   else OkBtn.Enabled:=false;
end;

procedure TSelShpWnd.FormActivate(Sender: TObject);
var Layername:string;
    layercnt:integer;
    ALayer:Variant;
begin
   if FirstActivate then
   begin
      // fill layerlist
      LayerLB.Items.Clear;
      for layercnt:=0 to MainWnd.SourceDocument.Layers.Count-1 do
      begin
         ALayer:=MainWnd.SourceDocument.Layers.Items[layercnt];
         Layername:=ALayer.Layername;
         LayerLb.Items.Add(Layername);
      end;
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TSelShpWnd.GeoBtnClick(Sender: TObject);
begin
   MainWnd.AxGisProjection.SetupByDialog;
end;

procedure TSelShpWnd.SelectedCBClick(Sender: TObject);
begin
   if SelectedCB.Checked then
      LayerCB.Checked:=false;
   CheckWindow;
end;

procedure TSelShpWnd.CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
begin
   wintop:=(WHEIGHT div 2) - (winheight div 2);
   winleft:=(WWIDTH div 2) - (winwidth div 2);
end;

procedure TSelShpWnd.SetupWindowTimerTimer(Sender: TObject);
var dummy:string;
    intval, count:integer;
    inifile:TIniFile;
begin
   SetupWindowTimer.Enabled:=false;
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,winleft,wintop,winwidth, winheight,SWP_SHOWWINDOW);
   Self.Height:=winheight;
   Self.Width:=winwidth;

   if MainWnd.SourceDocument.NrOfSelectedObjects = 0 then
      SelectedCb.Visible:=FALSE;
   // read the Settings from the ConGIS.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'ConGIS.ini');
   dummy:=inifile.ReadString('GENERAL','TMPDIR','');
   if dummy <> '' then
      MainWnd.AXImpExpDbc.TmpDBaseDir:=dummy;
   dummy:=MainWnd.SourceDocument.Name;
   dummy:=ChangeFileExt(dummy, '.wgi');
   if FileExists(dummy) then
   begin
      MainWnd.AXImpExpDbc.ExpDbMode:=5; // Idb-Database
      MainWnd.AXImpExpDbc.ExpDatabase:='';
      MainWnd.AXImpExpDbc.ExpTable:='';
   end
   else
   begin
      dummy:=inifile.ReadString('SHPEXP','ExpDbMode','0');
      val(dummy, intval, count);
      if intval <> 5 then // Idb-Database
      begin
         MainWnd.AXImpExpDbc.ExpDbMode:=intval;
         dummy:=inifile.ReadString('SHPEXP','ExpDatabase','');
         MainWnd.AXImpExpDbc.ExpDatabase:=dummy;
         dummy:=inifile.ReadString('SHPEXP','ExpTable','');
         MainWnd.AXImpExpDbc.ExpTable:=dummy;
      end;
   end;
   SourceDbLabel.Caption:=MainWnd.AXImpExpDbc.ExpDbInfo;
   dummy:=inifile.ReadString('SHPEXP','LastUsedPath','');
   if dummy = '' then
      GetDir(0,dummy);
   FileSelectionPanel.Directory:=dummy;
   inifile.Destroy;

   Self.Repaint;
end;

// procedure is used to setup the SelShpWnd for batch mode
procedure TSelShpWnd.SetupForBatch;
var Layername:string;
    layercnt:integer;
    ALayer:Variant;
begin
   if FirstActivate then
   begin
      // fill layerlist
      LayerLB.Items.Clear;
      for layercnt:=0 to MainWnd.SourceDocument.Layers.Count-1 do
      begin
         ALayer:=MainWnd.SourceDocument.Layers.Items[layercnt];
         Layername:=ALayer.Layername;
         LayerLb.Items.Add(Layername);
      end;
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
   end;
end;

procedure TSelShpWnd.SetExpSettings(ProjectDir:string; ExpMode:integer);
begin
   DestinationShpDirectory:=ProjectDir;
   if (copy(DestinationShpDirectory,length(DestinationShpDirectory),1) <> '\') then
      DestinationShpDirectory:=DestinationShpDirectory+'\';
   if ExpMode = 2 then // export only selected objects
      SelectedCB.Checked:=true;
   if ExpMode = 1 then // export only selected layers
      LayerCB.Checked:=true;
end;

procedure TSelShpWnd.AddExpLayer(ExpLayer:string);
var i:integer;
begin
   // find the Layername in the list and select the item
   for i:=0 to LayerLB.Items.Count-1 do
   begin
      if (LayerLB.Items[i] = ExpLayer) then
      begin
         LayerLB.Selected[i]:=true;
      end;
   end;
end;

procedure TSelShpWnd.DbBtnClick(Sender: TObject);
begin
   MainWnd.AXImpExpDbc.SetupByDialog(Self.Handle, 5, 5);
   PageControl.Enabled:=FALSE;
   GeoBtn.Enabled:=FALSE;
   DbBtn.Enabled:=FALSE;
   OkBtnMode:=OkBtn.Enabled;
   OkBtn.Enabled:=FALSE;
   CancelBtn.Enabled:=FALSE;
end;

procedure TSelShpWnd.LayerLBClick(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSelShpWnd.FileSelectionPanelDirectoryChanged(Sender: TObject);
begin
   DestinationShpDirectory:=FileSelectionPanel.Directory;
   if DestinationShpDirectory <> '' then
   begin
      if DestinationShpDirectory[length(DestinationShpDirectory)] <> '\' then
         DestinationShpDirectory:=DestinationShpDirectory+'\';
   end;
   CheckWindow;
end;

end.
