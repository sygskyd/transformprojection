unit Maindlg;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Strfile, StdCtrls, Buttons, ExtCtrls, Gauges, Files, Db,
  DBTables, Windows, OleCtrls, AXImpExpDbcXControl_TLB, AxGisPro_TLB, AXDLL_TLB,
  WinOSInfo;

const
       //registration modes
       NO_LICENCE        = 0;
       FULL_LICENCE      = 1;
       DEMO_LICENCE      = 2;

       // Object types
       ot_Pixel     = 1;
       ot_Poly      = 2;
       ot_CPoly     = 4;
       ot_Text      = 7;
       ot_Symbol    = 8;
       ot_Spline    = 9;
       ot_Image     = 10;
       ot_MesLine   = 11;
       ot_Circle    = 12;
       ot_Arc       = 13;
       ot_Layer     = 18;

             //TextSTyles
       ts_Left           =  0;
       ts_Center         =  1;
       ts_Right          =  2;
       ts_Italic         =  4;
       ts_Bold           =  8;
       ts_Underl         = 16;
       ts_FixHeight      = 32;
       ts_Transparent    = 64;

       //LineTypes
       lt_Solid     = 0;
       lt_Dash      = 1;
       lt_Dot       = 2;
       lt_DashDot   = 3;
       lt_DashDDot  = 4;
       lt_UserDefined    = 1000;

       //FillTypes
       pt_NoPattern = 0;
       pt_FDiagonal = 1;
       pt_Cross     = 2;
       pt_DiagCross = 3;
       pt_BDiagonal = 4;
       pt_Horizontal= 5;
       pt_Vertical  = 6;
       pt_Solid     = 7;
       pt_UserDefined = 8;

       stDrawingUnits    = 0;
       stScaleInDependend= 1;
       stProjectScale    = 2;
       stPercent         = 3;

type
  TMainWnd = class(TForm)
    Panel1: TPanel;
    Progresslabel: TLabel;
    Prozessbalken: TGauge;
    StatusLB: TListBox;
    SaveReportBtn: TButton;
    CloseTimer: TTimer;
    SaveDialogRep: TSaveDialog;
    OKBtn: TButton;
    CancelBtn: TButton;
    SetupWindowTimer: TTimer;
    AXImpExpDbc: TAXImpExpDbc;
    AxGisProjection: TAxGisProjection;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CloseTimerTimer(Sender: TObject);
    procedure SaveReportBtnClick(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AXImpExpDbcDialogSetuped(Sender: TObject);
    procedure AxGisProjectionWarning(Sender: TObject;
      const Info: WideString);
    procedure AxGisProjectionError(Sender: TObject;
      const Info: WideString);
    procedure AXImpExpDbcRequestExpLayerIndex(Sender: TObject;
      const Layername: WideString; var LayerIndex: Integer);
    procedure AXImpExpDbcRequestExpProjectName(Sender: TObject;
      var ProjectName: WideString);
  private
    { Private-Deklarationen }
    StrFileObj :ClassStrfile;
    FileObj    :ClassFile;
    Pass       :integer;
    mifDirectory:string;
    Bytes2work  :longint;
    Bytesworked :longint;

    Bytes2work_current:longint;
    Bytesworked_current:longint;

    Aktiv         :boolean;
    Abort         :boolean;
    FirstActivate :boolean;
    ObjectsExported :longint;

    function CalculateElementsToExport:integer;
    function CalculateMapinfoColor(color:longint):longint;
    procedure Showpercent;

    procedure RunPass(AktPass:integer);
    procedure ExecutePass;
    procedure ExportCPoly(var OldObject:IBase;Layername:string);
    procedure GetLayerProperties(var ALayer:ILayer;var Linienfarbe:longint; var Linienstil:longint; var Flaechenfarbe:longint; var Flaechenstil:longint);
    procedure StartExport;
    function  ConvertMultiLineText(var AText:string):integer;
  public
    { Public-Deklarationen }
    App             :Variant;
    SourceDocument  :IDocument;
    RegMode :integer;
    LngCode :string;
    ExitNormal:boolean;
    GisHandle:integer;

    function ReadElement:integer;
    procedure ExportObject(var AObject:IBase;Layername:string);

    procedure SetProjectionSettings;
    procedure CheckProjectionSettings;
    procedure CreateStringFile;

    function FloatToMyStr(value:double):string;
  end;

var
  MainWnd: TMainWnd;

implementation

uses comobj, SelMifDlg;

{$R *.DFM}

procedure TMainWnd.CreateStringFile;
begin
   StrFileObj:=ClassStrfile.Create;
   StrFileObj.CreateList(OSInfo.WingisDir,LngCode,'MIFEXP');
   Self.Caption:=StrFileObj.Assign(1);           { PROGIS MAPINFO MIF-Export 2000 }
   ProgressLabel.Caption:=StrFileObj.Assign(13); { Process messages }
   SaveReportBtn.Caption:=StrFileObj.Assign(14); { Save report      }
   OkBtn.Caption:=StrFileObj.Assign(7);          { Ok }
   CancelBtn.Caption:=StrFileObj.Assign(8);      { Cancel }
end;

procedure TMainWnd.FormCreate(Sender: TObject);
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   Self.Height:=1;
   Self.Width:=1;
   Self.Update;
   Aktiv:=false;
   FirstActivate:=true;
   ObjectsExported:=0;
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);   
end;

procedure TMainWnd.CheckProjectionSettings;
var different:boolean;
begin
   different:=false;
   // function is used to check if the settings in the
   if (AxGisProjection.SourceCoordinateSystem <> SourceDocument.Projection) then
      different:=TRUE;

   if (AxGisProjection.SourceXOffset <> SourceDocument.ProjectionXOffset) then
      different:=TRUE;

   if (AxGisProjection.SourceYOffset <> SourceDocument.ProjectionYOffset) then
      different:=TRUE;

   if (AxGisProjection.SourceScale <> SourceDocument.ProjectionScale) then
      different:=TRUE;

   // check if the projections are different
   if SourceDocument.ProjectProjSettings <> AxGisProjection.SourceProjSettings then
      different:=TRUE;

   if SourceDocument.ProjectProjection <> AxGisProjection.SourceProjection then
      different:=TRUE;

   if SourceDocument.Projectdate <> AxGisProjection.SourceDate then
      different:=TRUE;

   if different then
   begin
      Showmessage(StrfileObj.Assign(43)); // Warning: Projection does not fit to project projection
      AxGisProjection.SetupByDialog;
   end;
   // the used output coordinate-system is equal to the
   // target-coordinate-system
   AxGisProjection.UsedOutputCoordSystem:=AxGisProjection.TargetCoordinateSystem;
end;

procedure TMainWnd.SetProjectionSettings;
begin
   // procedure is used to set the projection settings
   // of the current project
   AxGisProjection.WorkingPath:=OSInfo.WingisDir;
   AxGisProjection.LngCode:=LngCode;
   AxImpExpDbc.WorkingDir:=OSInfo.WingisDir;
   AxImpExpDbc.LngCode:=LngCode;
   AxImpExpDbc.ShowMode:=1; // Shw_Source
   SelMifWnd.SrcDbLabel.Caption:=AxImpExpDbc.ExpDbInfo;
   AxGisProjection.SourceCoordinateSystem:=SourceDocument.Projection;
   AxGisProjection.TargetCoordinateSystem:=cs_Carthesic; // Mapinfo data is normaly carthesic
   AxGisProjection.SourceProjSettings:=SourceDocument.ProjectProjSettings;
   AxGisProjection.TargetProjSettings:=SourceDocument.ProjectProjSettings;
   AxGisProjection.SourceProjection:=SourceDocument.ProjectProjection;
   AxGisProjection.TargetProjection:=SourceDocument.ProjectProjection;
   AxGisProjection.SourceDate:=SourceDocument.Projectdate;
   AxGisProjection.TargetDate:=SourceDocument.Projectdate;
   AxGisProjection.SourceXOffset:=SourceDocument.ProjectionXOffset;
   AxGisProjection.TargetXOffset:=SourceDocument.ProjectionXOffset;
   AxGisProjection.SourceYOffset:=SourceDocument.ProjectionYOffset;
   AxGisProjection.TargetYOffset:=SourceDocument.ProjectionYOffset;
   AxGisProjection.SourceScale:=SourceDocument.ProjectionScale;
   AxGisProjection.TargetScale:=SourceDocument.ProjectionScale;
   // if source and target is .amp - file
   // the used input coordinates and used output coordinates
   // for the projection calculation are in carthesic
   AxGisProjection.UsedInputCoordSystem:=cs_Carthesic;
   AxGisProjection.UsedOutputCoordSystem:=AxGisProjection.TargetCoordinateSystem;
end;

function TMainWnd.CalculateElementsToExport:integer;
var numelems:integer;
    layercnt:integer;
    ALayer:ILayer;
    i:integer;
    Layername:string;
begin
   numelems:=0;
   if SelMifWnd.SelectedCB.checked then
   begin
       SourceDocument.CreateSelectedList;
       numelems:=SourceDocument.NrOfSelectedObjects;
       result:=numelems;
       exit;
   end;
   for layercnt:=0 to SourceDocument.Layers.Count-1 do
   begin
      ALayer:=SourceDocument.Layers.Items[layercnt];
      { Nun mu� kontrolliert werden ob dieser Layer �berhaupt exportiert werden soll }
      if (SelMifWnd.LayerCB.checked) then
      begin
         Layername:=ALayer.Layername;
         for i:=0 to SelMifWnd.LayerLB.Items.Count-1 do
         begin
            if (SelMifWnd.LayerLB.Items[i] = Layername) and (SelMifWnd.LayerLB.Selected[i]) then
            begin
               numelems:=numelems+ALayer.count-1;
               break;
            end;
         end;
      end
      else
         numelems:=numelems+ALayer.count-1;
   end;
   result:=numelems;
end;

procedure TMainWnd.GetLayerProperties(var ALayer:ILayer;var Linienfarbe:longint; var Linienstil:longint; var Flaechenfarbe:longint; var Flaechenstil:longint);
begin
   // get linecolor
   Linienfarbe:=CalculateMapinfoColor(ALayer.Linestyle.Color);
   // get linetype
   case ALayer.Linestyle.Style of
      lt_Dot            :Linienstil:=3;
      lt_Dash           :Linienstil:=5;
      lt_DashDot        :Linienstil:=14;
      lt_DashDDot       :Linienstil:=20;
   else
      Linienstil:=0;
   end;

   // get area color
   Flaechenfarbe:=CalculateMapinfoColor(ALayer.Fillstyle.ForeColor);
   // get area filltype
   case ALayer.Fillstyle.Pattern of
      pt_Solid           :Flaechenstil:=2;
      pt_Horizontal      :Flaechenstil:=3;
      pt_Vertical        :Flaechenstil:=4;
      pt_FDiagonal       :Flaechenstil:=5;
      pt_BDiagonal       :Flaechenstil:=6;
      pt_Cross           :Flaechenstil:=7;
      pt_DiagCross       :Flaechenstil:=8;
   else
      Flaechenstil:=0;
   end;
end;

function TMainWnd.ReadElement:integer;
var numelems:integer;
    layercnt:integer;
    ALayer :ILayer;
    AObject:IBase;
    AObjectDisp:IDispatch;
    i,cnt:integer;
    Layername:string;
    found:boolean;
    Linienstil:longint;
    Linienfarbe:longint;
    Flaechenstil:longint;
    Flaechenfarbe:longint;
    DoExportLayer:boolean;
    MayExportObject:boolean;
    SelectedLayername:string;
begin
   MayExportObject:=true;

      // check if only selected objects should be exported
   if SelMifWnd.SelectedCB.checked then
   begin
      StatusLB.Items.Add('-------------------------------------------');
      StatusLB.Items.Add(StrfileObj.Assign(42));  // reading selected objects
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      SelectedLayername:='';
      for cnt:=0 to SourceDocument.NrOfSelectedObjects-1 do
      begin
         if Abort then exit;
         if SourceDocument.NrOfSelectedObjects = 0 then break;
         AObjectDisp:=SourceDocument.SelItems[cnt];
         if AObjectDisp = nil then
         begin
            Bytesworked:=Bytesworked+1;
            ShowPercent;
            continue;
         end;
         AObject:=AObjectDisp as IBase;
         if AObject.ObjectType = ot_Layer then // Layer Object received
         begin
            ALayer:=AObject as ILayer;
            Layername:=ALayer.Layername;
            if Layername <> SelectedLayername then
            begin
               if SelectedLayername <> '' then
               begin
                  // write mif-file
                  RunPass(2);
               end;
               SelectedLayername:=Layername;
               // first an entry for the layer has to be created
               FileObj:=ClassFile.Create;
               FileObj.CreateDatabaseEntries;
               GetLayerProperties(ALayer,Linienfarbe,Linienstil,Flaechenfarbe,Flaechenstil);
               FileObj.New_Layer(Layername, Linienstil,  Linienfarbe, Flaechenstil, Flaechenfarbe);
            end;
            Bytesworked:=Bytesworked+1;
            ShowPercent;
         end
         else
         begin
            ExportObject(AObject,Layername);
            ObjectsExported:=ObjectsExported+1;

            // check if in demomode
            if (RegMode = DEMO_LICENCE) and (ObjectsExported = 100) then
            begin
               StatusLB.Items.Add(StrfileObj.Assign(48));  // 100 objects exported, following objects will be ignored
               StatusLB.ItemIndex := StatusLB.Items.Count-1;
               StatusLB.Update;
               MayExportObject:=false;
               break;
            end;
         end;
      end;
      // write objects of last layer
      if SelectedLayername <> '' then
      begin
         RunPass(2);
      end;
      if not Abort then
         SourceDocument.DestroySelectedList;
      exit;
   end;

   for layercnt:=0 to SourceDocument.Layers.Count-1 do
   begin
      if Abort then exit;
      if not MayExportObject then exit;
      ALayer:=SourceDocument.Layers.Items[layercnt];
      Layername:=ALayer.Layername;
      // check if layer has to be exported
      if (SelMifWnd.LayerCB.Checked) then
      begin
         DoExportLayer:=false;
         for i:=0 to SelMifWnd.LayerLB.Items.Count-1 do
         begin
            if (SelMifWnd.LayerLB.Items[i] = Layername) and (SelMifWnd.LayerLB.Selected[i]) then
            begin
               DoExportLayer:=true;
               break;
            end;
         end;
      end
      else
         DoExportLayer:=true;

      if (DoExportLayer) then
      begin
         FileObj:=ClassFile.Create;
         FileObj.CreateDatabaseEntries;


         // reading data from layer
         StatusLB.Items.Add('-------------------------------------------');
         StatusLB.Items.Add(StrfileObj.Assign(15) + Layername);  // reading data from layer:
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;

         GetLayerProperties(ALayer,Linienfarbe,Linienstil,Flaechenfarbe,Flaechenstil);
         // create layer object
         FileObj.New_Layer(Layername, Linienstil,  Linienfarbe, Flaechenstil, Flaechenfarbe);
         // read objects of layer
         for cnt:=0 to ALayer.count-1 do
         begin
            if Abort then exit;
            AObjectDisp:=ALayer.Items[cnt];
            if AObjectDisp <> nil then
            begin
               AObject:=AObjectDisp as IBase;
               ExportObject(AObject,Layername);
            end;
            ObjectsExported:=ObjectsExported+1;

            // check if in demomode
            if (RegMode = DEMO_LICENCE) and (ObjectsExported = 100) then
            begin
               StatusLB.Items.Add(StrfileObj.Assign(40));  // 100 objects exported, following objects will be ignored
               StatusLB.ItemIndex := StatusLB.Items.Count-1;
               StatusLB.Update;
               MayExportObject:=false;
               break;
            end;
         end;

         // write Mif/Mid file
         RunPass(2);
      end;
   end;
end;

procedure TMainWnd.ExportObject(var AObject:IBase;Layername:string);
var AFont:string;
    ATextinfo:string;
    X,Y:double;
    ASize:integer;
    Radius:integer;
    i:integer;
    APoint:IPoint;
    OldIndex:integer;
    numlines:integer;

    aPixel  :IPixel;
    aSymbol :ISymbol;
    aPoly   :IPolyline;
    aText   :IText;
    aCircle :ICircle;
begin
   OldIndex:=AObject.Index;
   if (AObject.ObjectType = ot_Pixel) then // export point object
   begin
      aPixel:=aObject as IPixel;
      X:=aPixel.Position.X / 100;
      Y:=aPixel.Position.Y / 100;

      // calculate the projection
      AxGisProjection.Calculate(X,Y);

      FileObj.New_Point(X,Y);
      FileObj.ValDatabase(oldindex,AObject.ObjectType,Layername);
   end
   else if (AObject.ObjectType = ot_Symbol) then // export symbol object
   begin
      // Symbols will be exported as points due to WinGIS
      // symbols can not directly exported to MAPINFO
      aSymbol:=aObject as ISymbol;
      X:=aSymbol.Position.X / 100;
      Y:=aSymbol.Position.Y / 100;

      // calculate the projection
      AxGisProjection.Calculate(X,Y);

      FileObj.New_Point(X,Y);
      FileObj.ValDatabase(oldindex,AObject.ObjectType,Layername);

   end
   else if (AObject.ObjectType = ot_Cpoly) then // export area object
   begin
      ExportCPoly(AObject,Layername);
   end
   else if (AObject.ObjectType = ot_poly) then // export line object
   begin
      aPoly:=AObject as IPolyline;
      FileObj.New_Line;
      for i:=0 to aPoly.Count - 1 do
      begin
         APoint:=aPoly.Points[i];
         X:=APoint.X/100;
         Y:=APoint.Y/100;
         // calculate projection
         AxGisProjection.Calculate(X,Y);

         FileObj.InsertPointToLine(X,Y);
      end;
      // get database information
      FileObj.ValDatabase(oldindex,AObject.ObjectType,Layername);
   end
   else if (AObject.ObjectType = ot_Text) then // export text object
   begin
      aText:=AObject as IText;
      AFont:=aText.Fontname;
      X:=aText.PXFP1_X / 100;
      Y:=aText.PXFP1_Y / 100;

      // calculate projection
      AxGisProjection.Calculate(X,Y);

      ATextinfo:=aText.Text;
      numlines:=ConvertMultiLineText(ATextinfo);

      ASize:=round(aText.FontHeight/100);
      FileObj.New_Text(X,Y,ATextinfo,numLines,AFont,Asize,aText.Angle);

      // get database information
      FileObj.ValDatabase(oldindex,AObject.ObjectType,Layername);
   end
   else if (AObject.ObjectType = ot_Circle) then // export circle object
   begin
      aCircle:=aObject as ICircle;
      APoint:=aCircle.Position;
      X:=APoint.X / 100;
      Y:=APoint.Y / 100;

      // calculate projection
      AxGisProjection.Calculate(X,Y);

      Radius:=round(aCircle.PrimaryAxis/100);
      FileObj.New_Circle(X,Y,Radius);

      // get database information
      FileObj.ValDatabase(oldindex,AObject.ObjectType,Layername);
   end;
   Bytesworked:=Bytesworked+1;
   ShowPercent;
end;

procedure TMainWnd.ExportCPoly(var OldObject:IBase;Layername:string);
var APoint:IPoint;
    PointCount:integer;
    SkipIndex :integer;
    FirstIsland:boolean;
    IslandCount:integer;
    IsIsland:boolean;
    IslandInfoIdx:integer;
    NextIslandIdx:integer;
    X,Y:double;
    Cnt:integer;
    InsertToIsland:boolean;
    aCPoly:IPolygon;
begin
   aCPoly:=OldObject as IPolygon;
   PointCount:=0; SkipIndex:=0; FirstIsland:=TRUE; IsIsland:=FALSE;
   InsertToIsland:=FALSE;
   IslandCount:=aCPoly.IslandCount;
   if IslandCount > 1 then
   begin
      IsIsland:=true;
      IslandInfoIdx:=0;
      NextIslandIdx:=aCPoly.IslandInfo[IslandInfoIdx];
   end;

   FileObj.New_Area;
   FileObj.ValDatabase(OldObject.Index,OldObject.ObjectType,Layername);

   // insert first point to new CPoly
   APoint:=aCPoly.Points[0];
   X:=APoint.X/100; Y:=APoint.Y/100;

   // calculate the projection
   AxGisProjection.Calculate(X,Y);
   FileObj.InsertPointToArea(X,Y);

   PointCount:=PointCount+1;

   for Cnt:=1 to aCPoly.Count - 1 do
   begin
      if SkipIndex = 0 then
         PointCount:=PointCount + 1;
      if (IsIsland) and (PointCount = NextIslandIdx) then // next island area will be created
      begin
         IslandInfoIdx:=IslandInfoIdx+1;
         if FirstIsland then
            SkipIndex:=1
         else
            SkipIndex:=2;

         FirstIsland:=FALSE;
         if IslandInfoIdx >= IslandCount then
         begin
            // all islands have been inserted
            NextIslandIdx:=0;
            IslandInfoIdx:=IslandCount;
         end
         else
         begin
            // and a new polygon has to be created
            InsertToIsland:=true;
            FileObj.InsertIsland;

            // FileObj.New_Area;
            // FileObj.ValDatabase(OldObject.Index,OldObject.ObjectType,Layername);

            NextIslandIdx:=aCPoly.IslandInfo[IslandInfoIdx];

            // insert point to polygon
            APoint:=aCPoly.Points[Cnt+SkipIndex];
            X:=APoint.X/100; Y:=APoint.Y/100;
            // calculate the projection
            AxGisProjection.Calculate(X,Y);
            FileObj.InsertPointToIsland(X,Y);

            PointCount:=1;
         end;
      end
      else
      begin
         if (SkipIndex=0) then
         begin
            if (IsIsland) and (Cnt = aCPoly.Count -2) then //avoid exporting last connectionline
               continue;

            // insert point to polygon
            APoint:=aCPoly.Points[Cnt];
            X:=APoint.X/100; Y:=APoint.Y/100;
            // calculate the projection
            AxGisProjection.Calculate(X,Y);
            if (InsertToIsland) then
               FileObj.InsertPointToIsland(X,Y)
            else
               FileObj.InsertPointToArea(X,Y);
         end
         else
            SkipIndex:=SkipIndex-1;
      end;
   end;
end;

function TMainWnd.CalculateMapinfoColor(color:longint):longint;
var Farbnr:longint;
    blau,gruen,rot:integer;
    Farbstr:string;
begin
   Farbnr:=color;
   { Ermitteln des rot Anteils }
   blau:=Farbnr div 65536; Farbnr:=Farbnr - (blau * 65536);
   { Ermitteln des gruen Anteils }
   gruen:=Farbnr div 256; Farbnr:=Farbnr - (gruen * 256);
   { Ermitteln des blau Anteils }
   rot:=Farbnr;
   result:=(rot * 65536)+(gruen * 256)+(blau);
end;

{*******************************************************************************
* PROZEDUR : RunPass                                                           *
********************************************************************************
* Prozedur leitet den jeweiligen Konvertierungs-Pass ein.                      *
*                                                                              *
* PARAMETER: Pass -> Gibt den Konvertierungs-Pass an.                          *
*                                                                              *
********************************************************************************}
procedure TMainWnd.RunPass(AktPass:integer);
begin
   if AktPass = 1 then { �ffnen der Dateien zum Auslesen des ASC-Files }
   begin
      StatusLB.Items.Add(StrfileObj.Assign(10) + MifDirectory);  // export Mif data to directory
      // display database settings
      StatusLB.Items.Add(AxImpExpDbc.ExpDbInfo);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      CheckProjectionSettings;

      Aktiv:=true;
      Bytes2Work:=CalculateElementsToExport; Bytesworked:=0;

      // read data from project
      Pass:=1;
      ExecutePass;

      if not Abort then
      begin
         // project has been successfully exported
         OkBtn.Enabled:=true;
         SaveReportBtn.Enabled:=true;
         CancelBtn.Enabled:=false;
         Abort:=true;
         Aktiv:=false;
         StatusLB.Items.Add(StrfileObj.Assign(11));  // project exported successfully!
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
      end;
      exit;
   end;

   if AktPass = 2 then
   begin
       Bytes2work_current:=FileObj.GetMaxLayers; Bytesworked_current:=0;
       Pass:=2;
       ExecutePass;
       exit;
   end;

   if AktPass = 3 then // layer exported
   begin
      aktiv:=false;
      FileObj.CloseAllFiles;
      FileObj.Destroy;
      Self.Caption:=StrFileObj.Assign(1);  { PROGIS MAPINFO-MIF Export 2000 }
   end;
end;

procedure TMainWnd.Showpercent;
var Prozent:integer;
begin
   if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
   ProzessBalken.Progress :=Prozent;
   ProzessBalken.Update;
   Application.Processmessages;
end;

{*******************************************************************************
* PROZEDUR : ExecutePass                                                       *
********************************************************************************
* Prozedur die zur Abarbeitung eines Konvertierungsschritts dient              *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure TMainWnd.ExecutePass;
var Prozent:integer;
begin
   if Pass = 1 then // read data from project
   begin
      MainWnd.ReadElement;
      exit;
   end;
   if Pass = 2 then // write Mif-files
   begin
      if Bytes2work_current > 0 then
      repeat
         // set output directory
         FileObj.SetMifDirectory(MifDirectory);

         // set mask settings
         StatusLB.Items.Add(StrfileObj.Assign(12)+ FileObj.GetCurrentLayername);  // write Mif file for Layer
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;

         Bytesworked_current:=Bytesworked_current + FileObj.WriteCurrentLayer;
         Application.Processmessages;
      until (Bytesworked_current >= Bytes2work_current);
      if (Bytesworked_current >= Bytes2work_current) and (Abort = false) then begin RunPass(3); exit; end;
      if Abort = true then exit;
   end;
end;

procedure TMainWnd.StartExport;
begin
   if MainWnd.ExitNormal then
   begin
      SelMifWnd.GetMifDir(MifDirectory);
      RunPass(1);
   end;
end;

procedure TMainWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TMainWnd.CancelBtnClick(Sender: TObject);
begin
   StatusLB.Items.Add(StrfileObj.Assign(31));  { Warning: Export canceled }
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;

   aktiv:=false; Abort:=true;
   CancelBtn.Enabled:=false;
   SaveReportBtn.enabled:=true;
   OkBtn.Enabled:=true;
   FileObj.CloseAllFiles;
   FileObj.Destroy;
   if SelMifWnd.SelectedCB.checked then
      SourceDocument.DestroySelectedList;
end;

procedure TMainWnd.OKBtnClick(Sender: TObject);
begin
   Closetimer.enabled:=true;
end;

procedure TMainWnd.CloseTimerTimer(Sender: TObject);
begin
   MainWnd.Close;
   SelMifWnd.Close;
   SourceDocument.FinishImpExpRoutine(ExitNormal,'EXPORTMODULE');
   CloseTimer.Enabled:=false;
end;

procedure TMainWnd.SaveReportBtnClick(Sender: TObject);
var Datei:Textfile;
    Zeile:string;
    i:integer;
begin
   if SaveDialogRep.Execute then
   begin
      Assignfile(Datei,SaveDialogRep.Filename);
      Rewrite(Datei);
      for i:=0 to StatusLB.Items.Count-1 do
      begin
         Zeile:=StatusLB.items[i];
         Writeln(Datei,Zeile);
      end;
      Closefile(Datei);
   end;
end;

procedure TMainWnd.SetupWindowTimerTimer(Sender: TObject);
var IdbProcessMask:integer;
begin
   SetupWindowTimer.Enabled:=false;
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,SelMifWnd.winleft,SelMifWnd.wintop,SelMifWnd.winwidth, SelMifWnd.winheight,SWP_SHOWWINDOW);
   MainWnd.Height:=SelMifWnd.winheight;
   MainWnd.Width:=SelMifWnd.winwidth;
   Self.Repaint;
   IdbProcessMask:=SourceDocument.IdbProcessMask;
   SourceDocument.IdbProcessMask:=0;
   StartExport;
   SourceDocument.IdbProcessMask:=IdbProcessMask;
end;

procedure TMainWnd.FormDestroy(Sender: TObject);
begin
   StrFileObj.Destroy;
end;

function TMainWnd.FloatToMyStr(value:double):string;
var retVal:string;
    dummy:pchar;
    i:integer;
begin
   retVal:=floattostr(value);
   // now check if there is a comma inside
   dummy:=stralloc(length(retVal)+1);
   strpcopy(dummy,retVal);
   for i:=0 to strlen(dummy)-1 do
      if dummy[i] = ',' then dummy[i]:='.';
   retVal:=strpas(dummy);
   result:=retVal;
end;

function TMainWnd.ConvertMultiLineText(var AText:string):integer;
var retVal:integer;
    dummy:pchar;
    i:integer;
begin
   retVal:=1; // normaly a text contains only of one line
      // items of multiline texts are seperated with <CR> <LF>
   // these have to be exchanged by space characers
   dummy:=stralloc(length(aText)+1);
   strpcopy(dummy,aText);
   for i:=0 to length(aText)-1 do
   begin
      if (ord(dummy[i]) = 10) then begin retVal:=retVal+1; dummy[i]:='\'; end;
      if (ord(dummy[i]) = 13) then dummy[i]:='n';
   end;
   aText:=strpas(dummy);
   strdispose(dummy);
   result:=retVal;
end;
procedure TMainWnd.FormShow(Sender: TObject);
begin
   AXImpExpDbc.visible:=FALSE;
   AxGisProjection.visible:=FALSE;
end;

procedure TMainWnd.AXImpExpDbcDialogSetuped(Sender: TObject);
begin
   SelMifWnd.SrcDbLabel.Caption:=AxImpExpDbc.ExpDbInfo;
   SelMifWnd.PageControl.Enabled:=TRUE;
   SelMifWnd.GeoBtn.Enabled:=TRUE;
   SelMifWnd.DbBtn.Enabled:=TRUE;
   SelMifWnd.OkBtn.Enabled:=SelMifWnd.OkBtnMode;
   SelMifWnd.CancelBtn.Enabled:=TRUE;
end;

procedure TMainWnd.AxGisProjectionWarning(Sender: TObject;
  const Info: WideString);
begin
   if not Active then
   begin
      Showmessage(Info);
   end
   else
   begin
      StatusLB.Items.Add(Info);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
   end;
end;

procedure TMainWnd.AxGisProjectionError(Sender: TObject;
  const Info: WideString);
begin
   if not Active then
   begin
      Showmessage(Info);
   end
   else
   begin
      StatusLB.Items.Add(Info);
      StatusLB.Items.Add(StrfileObj.Assign(31));  { Warning: Export canceled }
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      aktiv:=false; Abort:=true;
      CancelBtn.Enabled:=false;
      SaveReportBtn.enabled:=true;
      OkBtn.Enabled:=true;
      FileObj.CloseAllFiles;
      FileObj.Destroy;
      if SelMifWnd.SelectedCB.checked then
         SourceDocument.DestroySelectedList;
   end;
end;

procedure TMainWnd.AXImpExpDbcRequestExpLayerIndex(Sender: TObject;
  const Layername: WideString; var LayerIndex: Integer);
var i:integer;
begin
   // the Layerindex of the Layername has to be evaluated
   for i:=0 to SourceDocument.Layers.Count-1 do
   begin
      if SourceDocument.Layers.Items[i].Layername = Layername then
      begin
         LayerIndex:=SourceDocument.Layers.Items[i].Index;
         break;
      end;
   end;
end;

procedure TMainWnd.AXImpExpDbcRequestExpProjectName(Sender: TObject;
  var ProjectName: WideString);
var ProjName:string;
begin
   ProjName:=SourceDocument.Name;
   ProjectName:=ProjName;
end;

end.
