unit Files;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Math, Struct, Strfile;

const
       Col_Int      = 0;
       Col_SmallInt = 1;
       Col_Float    = 2;
       Col_Date     = 3;
       Col_String   = 4;

type

ClassFile = class
   public
      { public Definitionen }
      constructor Create;
      destructor Destroy;
      procedure CloseAllFiles;
      procedure WritemifLine(Text:string);
      procedure WriteMIDLine(Text:string);
      function GetFilesize(Dateiname:string):longint;
      function GetMaxLayers:longint;
      function GetCurrentLayername:string;
      function WriteCurrentLayer:longint;
      procedure CreateDatabaseEntries;
      procedure New_Layer(Layername:string;Line_Type:integer;Line_Color:integer;Fill_Type:integer;Fill_Color:integer);
      procedure New_Point(X:double;Y:double);
      procedure New_Line;
      procedure InsertPointToLine(X:double;Y:double);
      procedure New_Area;
      procedure InsertPointToArea(X:double;Y:double);
      procedure InsertIsland;
      procedure InsertPointToIsland(X:double;Y:double);
      procedure New_Text(X:double;Y:double;Text:string;numlines:integer;Font:string;size:integer;angle:integer);
      procedure New_Circle(X:double;Y:double;Radius:integer);
      procedure ValDatabase(Index:integer; ObjType:integer; Layername:string);
      procedure SetMifDirectory(Name:string);
   private
      { private Definitionen }
      procedure CreateDir(var Name:string);
      procedure MakeCorrectName(var name:string);
end;
implementation
uses MainDlg, ActiveX;

var  MIFDatei       :Textfile;
     MIDDatei       :Textfile;

     AreaObj        :ClassArea;
     PointObj       :ClassPoint;
     LineObj        :ClassLine;
     LayerObj       :ClassLayer;
     TextObj        :ClassText;
     CircleObj      :ClassCircle;
     DatenbankObj   :ClassDatenbank;
     StrFileObj     :ClassStrFile;

     MIFDirectory   :string;
     AktObjektnummer:integer;
     AktObjekttyp   :integer;

constructor ClassFile.Create;
begin
   AreaObj:=ClassArea.Create;
   PointObj:=ClassPoint.Create;
   LineObj:=ClassLine.Create;
   TextObj:=ClassText.Create;
   CircleObj:=ClassCircle.Create;
   LayerObj:=ClassLayer.Create;
   DatenbankObj:=ClassDatenbank.Create;
end;

destructor ClassFile.Destroy;
begin
   AreaObj.Destroy;
   PointObj.Destroy;
   LineObj.Destroy;
   TextObj.Destroy;
   CircleObj.Destroy;
   LayerObj.Destroy;
   DatenbankObj.Destroy;
end;

{*******************************************************************************
* PROZEDUR : CreateDir                                                         *
********************************************************************************
* Prozedur dient zum Erzeugen eines Unterverzeichnisses                        *
*                                                                              *
* PARAMETER: Name -> Name des Verzeichnisses das erstellt werden soll.         *
*                                                                              *
********************************************************************************}
procedure ClassFile.CreateDir(var Name:string);
begin
   {$i-}
   MKDir(mifDirectory+'\'+Name);
   ChDir(mifDirectory+'\'+Name);
   {$i+}
end;

procedure ClassFile.SetMifDirectory(Name:string);
begin
   mifDirectory:=Name;
end;


{*******************************************************************************
* PROZEDUR : WriteMIFLine                                                      *
********************************************************************************
* Prozedur dient zum Schreiben einer Zeile in die mif-Datei                    *
*                                                                              *
* PARAMETER: Text -> Text der in die mif-Datei geschrieben werden soll.        *
*                                                                              *
********************************************************************************}
procedure Classfile.WriteMIFLine(Text:string);
begin
   {$i-} Writeln(mifDatei,Text); {$i+}
end;

{*******************************************************************************
* PROZEDUR : WriteMIDLine                                                      *
********************************************************************************
* Prozedur dient zum Schreiben einer Zeile in die mid-Datei                    *
*                                                                              *
* PARAMETER: Text -> Text der in die mid-Datei geschrieben werden soll.        *
*                                                                              *
********************************************************************************}
procedure Classfile.WriteMIDLine(Text:string);
begin
   {$i-} Writeln(midDatei,Text); {$i+}
end;

procedure Classfile.ValDatabase(Index:integer; ObjType:integer; Layername:string);
var column, info:widestring;
    iterate:p_Datenbank_record;
begin
   // function is used to get database information for object
   if not MainWnd.AXImpExpDbc.DoExportData(Layername,Index) then
   begin
      // create empty database entry to keep output mid-file synchronisized
      iterate:=LayerObj.GetDataBaseDefinition;
      while (iterate <> nil) do
      begin
         column:=iterate^.Bezeichnung;
         info:='';
         if      (ObjType = ot_Pixel  ) then PointObj.InsertInfoColumn(column,Info)
         else if (ObjType = ot_Symbol ) then PointObj.InsertInfoColumn(column,Info)
         else if (ObjType = ot_Cpoly  ) then AreaObj.InsertInfoColumn(column,Info)
         else if (ObjType = ot_poly   ) then LineObj.InsertInfoColumn(column,Info)
         else if (ObjType = ot_Text   ) then TextObj.InsertInfoColumn(column,Info)
         else if (ObjType = ot_Circle ) then CircleObj.InsertInfoColumn(column,Info);
         iterate:=iterate^.next;
      end;
      exit; // if no databaseentry has been found for this object exit
   end;

   // get database information
   while MainWnd.AXImpExpDbc.GetExportData(Layername, column, info) do
   begin
      if      (ObjType = ot_Pixel  ) then PointObj.InsertInfoColumn(column,Info)
      else if (ObjType = ot_Symbol ) then PointObj.InsertInfoColumn(column,Info)
      else if (ObjType = ot_Cpoly  ) then AreaObj.InsertInfoColumn(column,Info)
      else if (ObjType = ot_poly   ) then LineObj.InsertInfoColumn(column,Info)
      else if (ObjType = ot_Text   ) then TextObj.InsertInfoColumn(column,Info)
      else if (ObjType = ot_Circle ) then CircleObj.InsertInfoColumn(column,Info);
   end;
end;

{*******************************************************************************
* PROZEDUR : CloseallFiles                                                     *
********************************************************************************
* Prozedur dient zum Schliessen aller Files.                                   *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassFile.CloseallFiles;
begin
   MainWnd.AXImpExpDbc.CloseExpTables;
end;

{*******************************************************************************
* FUNKTION : GetFilesize                                                       *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Bytes des       *
* ASC-Files                                                                    *
*                                                                              *
* PARAMETER: Dateiname -> Dateiname von der die Bytegr��e ermittelt werden soll*
*                                                                              *
* R�ckgabewert: Anzahl der zu Bearbeitenden Bytes.                             *
********************************************************************************}
function ClassFile.GetFilesize(Dateiname:string):longint;
var Datei:file of byte;
begin
   AssignFile(Datei, Dateiname);
   Reset(Datei);
   Result:=FileSize(Datei);
   Closefile(Datei);
end;

{*******************************************************************************
* FUNKTION : GetMaxLayers                                                      *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Layer           *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden Layer.                             *
********************************************************************************}
function ClassFile.GetMaxLayers:longint;
begin
   LayerObj.ResetCurrent;
   Result:=LayerObj.GetAnzahl;
end;

{*******************************************************************************
* FUNKTION : GetCurrentLayername                                               *
********************************************************************************
* Funktion dient zum Ermitteln des Namens des aktuellen Layers.                *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Name des aktuell bearbeiteten Layers.                          *
********************************************************************************}
function ClassFile.GetCurrentLayername:string;
begin
   Result:=LayerObj.GetCurrentLayername;
end;

procedure ClassFile.CreateDatabaseEntries;
begin
   { Die X und Y Koordiante muss fuer das Datenbankobjekt erzeugt werden }
   DatenbankObj.Insertcolumn('X-KOORDINATE',0,Col_String,'',0,0,FALSE);
   DatenbankObj.Insertcolumn('Y-KOORDINATE',0,Col_String,'',0,0,FALSE);
end;


{*******************************************************************************
* FUNKTION : WriteCurrentLayer                                                 *
********************************************************************************
* Funktion dient zum schreiben der Daten des aktuellen Layers.                 *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Layer.                                 *
********************************************************************************}
function ClassFile.WriteCurrentLayer:longint;
var CorrectLayername:string;
    ProjectionStr:string;
begin
   { Erzeugen der MID und MIF Datei }
   CorrectLayername:=LayerObj.GetCurrentLayername;
   MakeCorrectName(CorrectLayername);
   Assignfile(MIFDatei,MIFDirectory + CorrectLayername + '.MIF');
   Rewrite(MIFDatei);
   Assignfile(MIDDatei,MIFDirectory + CorrectLayername + '.MID');
   Rewrite(MIDDatei);

   { Nun wird der Dateikopf geschrieben }
   writeln(MIFDatei,'Version 300');
   writeln(MIFDatei,'Charset "WindowsLatin1"');
   writeln(MIFDatei,'Delimiter ","');
   ProjectionStr:=LayerObj.GetCurrentProjectionString;
   writeln(MIFDatei,ProjectionStr);

   { Nun muss die Datenbankdefinition geschrieben werden }
   LayerObj.WriteDatabasecolumns;

   writeln(MIFDatei,'Data'); writeln(MIFDatei,'');

   { Nun werden die Fl�cheneintr�ge f�r diesen Layer geschrieben }
   AreaObj.Writedata(LayerObj.GetCurrentLayername);
   { Nun werden die Linieneintr�ge f�r diesen Layer geschrieben }
   LineObj.Writedata(LayerObj.GetCurrentLayername);
   { Nun werden die Punkteintr�ge f�r diesen Layer geschrieben }
   PointObj.Writedata(LayerObj.GetCurrentLayername);
   { Nun werden die Texteintr�ge f�r diesen Layer geschrieben }
   TextObj.WriteData(LayerObj.GetCurrentLayername);
   { Nun werden die Kreiseintr�ge f�r diesen Layer geschrieben }
   CircleObj.WriteData(LayerObj.GetCurrentLayername);

   Closefile(MifDatei); Closefile(MidDatei);

   { Nun wird weiter zum n�chsten Layer gegangen }
   LayerObj.SetCurrentnext;

   Result:=1;
end;

procedure ClassFile.MakeCorrectName(var name:string);
var pdummy:PCHAR;
    pdummy2:PCHAR;
    i,pos:integer;
    res:string;
    canadd:boolean;
begin
   //function is used to set the layername that it can be set as database or controlname
   pdummy:=stralloc(length(name)+1); strpcopy(pdummy,name);
   pdummy2:=stralloc(length(name)+1); strpcopy(pdummy2,name);
   pos:=0;
   for i:=0 to strlen(pdummy)-1 do
   begin
      canadd:=true;
      if (pdummy[i] = '\') then canadd:=false;
      if (pdummy[i] = '/') then canadd:=false;
      if (pdummy[i] = ':') then canadd:=false;
      if (pdummy[i] = '*') then canadd:=false;
      if (pdummy[i] = '?') then canadd:=false;
      if (pdummy[i] = '"') then canadd:=false;
      if (pdummy[i] = '<') then canadd:=false;
      if (pdummy[i] = '>') then canadd:=false;
      if (pdummy[i] = '|') then canadd:=false;

      if (canadd) then
      begin
         pdummy2[pos]:=pdummy[i];
         pos:=pos+1;
      end;
   end;
   name:=strpas(pdummy2);
   name:=copy(name,1,pos);
   strdispose(pdummy); strdispose(pdummy2);
end;

procedure ClassFile.New_Layer(
                                 Layername :string;
                                 Line_Type :integer;
                                 Line_Color:integer;
                                 Fill_Type :integer;
                                 Fill_Color:integer
                              );
var Columnname:widestring;
    Columntype:TOleEnum;
    Columnlength:integer;
begin
   LayerObj.Neu(Layername);
   LayerObj.SetLineStyle(Line_Type,Line_Color);
   LayerObj.SetAreaStyle(Fill_Type,Fill_Color);

   // read database column for this layer
   MainWnd.AXImpExpDbc.CheckAndCreateExpTable(Layername);
   while MainWnd.AXImpExpDbc.GetExportColumn(Layername, Columnname, Columntype, Columnlength) do
   begin
      DatenbankObj.Insertcolumn(Columnname,Columnlength,Columntype,'',0,0,true);
      LayerObj.InsertDBColumn(Columnname);
   end;
   MainWnd.AXImpExpDbc.OpenExpTable(Layername);
end;

procedure ClassFile.New_Point(X:double;Y:double);
begin
   PointObj.Neu(X,Y,LayerObj.GetCurrent);
   LayerObj.SetupBoundary(X, Y);
end;

procedure ClassFile.New_Line;
begin
   LineObj.Neu(LayerObj.GetCurrent);
end;

procedure ClassFile.InsertPointToLine(X:double;Y:double);
begin
   LineObj.InsertPoint(X,Y);
   DatenbankObj.SetLength('X-KOORDINATE',length(floattostr(X)));
   DatenbankObj.SetLength('Y-KOORDINATE',length(floattostr(Y)));
   LayerObj.SetupBoundary(X, Y);
end;

procedure ClassFile.New_Area;
begin
   AreaObj.Neu(LayerObj.GetCurrent);
end;

procedure ClassFile.InsertIsland;
begin
   AreaObj.InsertIsland;
end;

procedure ClassFile.InsertPointToIsland(X:double; Y:double);
begin
   AreaObj.InsertPointToIsland(X,Y);
   DatenbankObj.SetLength('X-KOORDINATE',length(floattostr(X)));
   DatenbankObj.SetLength('Y-KOORDINATE',length(floattostr(Y)));
   LayerObj.SetupBoundary(X, Y);
end;

procedure ClassFile.InsertPointToArea(X:double;Y:double);
begin
   AreaObj.InsertPoint(X,Y);
   DatenbankObj.SetLength('X-KOORDINATE',length(floattostr(X)));
   DatenbankObj.SetLength('Y-KOORDINATE',length(floattostr(Y)));
   LayerObj.SetupBoundary(X, Y);
end;

procedure ClassFile.New_Text(X:double;Y:double;Text:string;numlines:integer;Font:string;size:integer;angle:integer);
begin
   TextObj.Neu(X,Y,X,Y,Text,numlines,Font,size,angle,LayerObj.GetCurrent);
   DatenbankObj.SetLength('X-KOORDINATE',length(floattostr(X)));
   DatenbankObj.SetLength('Y-KOORDINATE',length(floattostr(Y)));
   LayerObj.SetupBoundary(X, Y);
end;

procedure ClassFile.New_Circle(X:double;Y:double;Radius:integer);
begin
   CircleObj.Neu(X,Y,inttostr(Radius),LayerObj.GetCurrent);
   DatenbankObj.SetLength('X-KOORDINATE',length(floattostr(X)));
   DatenbankObj.SetLength('Y-KOORDINATE',length(floattostr(Y)));
   LayerObj.SetupBoundary(X, Y);
end;

end.
