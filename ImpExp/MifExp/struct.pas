unit struct;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, db, Math;

type
  { Hier k�nnen Strukturen definiert werden }

   p_Punkt_record=^Punkt_record;
   Punkt_record = record
       X:double;
       Y:double;
       next :p_Punkt_record;
   end;

   p_Datenbank_record =^Datenbank_record;
   Datenbank_record = record
      Bezeichnung : string;
      Laenge      : integer;
      Typ         : integer;
      Inhalt      : string;
      von         : integer;
      bis         : integer;
      Art         : boolean;
      next        : p_Datenbank_record;
   end;

   p_Layer_record = ^Layer_record;
   Layer_record = record
      Layername     :string;
      Linestyle     :integer;
      Linecolor     :longint;
      Areastyle     :integer;
      Areacolor     :longint;
      Objectsonlayer:integer;
      boundaryset   :boolean;
      Min_X         :double;
      Min_Y         :double;
      Max_X         :double;
      Max_Y         :double;
      DBColumns     :p_Datenbank_record;
      next          :p_Layer_record;
   end;

   p_Point_record = ^Point_record;
   Point_record = record
      X        :double;
      Y        :double;
      DBInfo   :p_Datenbank_record;
      Layer    :p_Layer_record;
      next     :p_Point_record;
   end;

   p_Line_record = ^Line_record;
   Line_record = record
      Punktanz :integer;
      Punkte   :p_Punkt_record;
      DBInfo   :p_Datenbank_record;
      Layer    :p_Layer_record;
      next     :p_Line_record;
   end;

   p_area_record =^area_record;
   area_record = record
      Punktanz     :integer;
      Punkte       :p_Punkt_record;

      CurrentIsland:p_area_record;
      Islandcount  :integer;
      Islands      :p_area_record;

      DBInfo   :p_Datenbank_record;
      Layer    :p_Layer_record;
      next     :p_area_record;
   end;

   p_text_record = ^text_record;
   text_record = record
      X1        :double;
      Y1        :double;
      X2        :double;
      Y2        :double;
      Text      :string;
      numlines  :integer;
      Font      :string;
      size      :double;
      Angle     :double;
      Layer     :p_Layer_record;
      DBInfo    :p_Datenbank_record;
      next      :p_text_record;
   end;

   p_circle_record = ^circle_record;
   circle_record = record
      X         :double;
      Y         :double;
      Radius    :double;
      Layer     :p_Layer_record;
      DBInfo    :p_Datenbank_record;
      next      :p_circle_record;
   end;

ClassArea = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(Layer:p_Layer_record);
      procedure InsertPoint(X:double;Y:double);
      procedure InsertIsland;
      procedure InsertPointToIsland(X:double; Y:double);
      procedure WriteData(Layername:string);
      procedure InsertInfoColumn(Spaltenname:string; Inhalt:string);
   private
      { private Definitionen }
      Anfang,Ende,Current:p_Area_record;
      Anzahl:integer;
end;

ClassLine = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(Layer:p_Layer_record);
      procedure InsertPoint(X:double;Y:double);
      procedure WriteData(Layername:string);
      procedure InsertInfoColumn(Spaltenname:string; Inhalt:string);
   private
      { private Definitionen }
      Anfang,Ende,Current:p_Line_record;
      Anzahl:integer;
end;

ClassPoint = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X:double;Y:double;Layer:p_Layer_record);
      procedure InsertInfoColumn(Spaltenname:string; Inhalt:string);
      procedure WriteData(Layername:string);
    private
      { private Definitionen }
      Anfang,Ende,Current:p_point_record;
      Anzahl:integer;
end;

ClassText = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X1:double;Y1:double;X2:double;Y2:double;Text:string;numlines:integer;Font:string;size:double;angle:double;Layer:p_Layer_record);
      procedure InsertInfoColumn(Spaltenname:string; Inhalt:string);
      procedure WriteData(Layername:string);
    private
      { private Definitionen }
      Anfang,Ende,Current:p_text_record;
      Anzahl:integer;
      function  GetAngle(X1:double;Y1:double;X2:double;Y2:double):double;
      function  GetLength(Text:string;Size:double):double;
      function  GetHeight(Size:double;numlines:integer):double;
end;

ClassCircle = class
    public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X:double;Y:double;Radius:string;Layer:p_Layer_record);
      procedure InsertInfoColumn(Spaltenname:string; Inhalt:string);
      procedure WriteData(Layername:string);
    private
      { private Definitionen }
      Anfang,Ende,Current:p_circle_record;
      Anzahl:integer;
end;


ClassDatenbank = class
   public
      { public Definitionen }
      constructor Create;
      destructor Destroy;
      procedure Neu(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string; von:integer; bis:integer;Art:boolean; var Anfang:p_datenbank_record);
      function  Checkexists(Eintrag:string):boolean;
      procedure SetLength(Bezeichnung:string; Laenge:integer);
      function  GetLength(Bezeichnung:string):integer;
      function  GetItem(Bezeichnung:string):string;
      procedure Insertcolumn(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string;von:integer;bis:integer;Art:boolean);
      procedure ResetColumn;
      function  GetDataType(Bezeichnung:string):integer;
   private
      { private Definitionen }
end;

ClassLayer = class
   public
      { public Definitionen }
      constructor Create;
      destructor Destroy;
      procedure Neu(Layername:string);
      function Checkexists(Layername:string):boolean;
      function GetCurrent:p_Layer_record;
      procedure SetLinestyle(Linetype:longint;LineColor:longint);
      procedure SetAreastyle(Areatype:longint;AreaColor:longint);
      function Hextoint(Wert:string):longint;
      procedure ResetCurrent;
      function GetAnzahl:longint;
      function GetCurrentLayername:string;
      procedure SetCurrentnext;
      procedure InsertDBColumn(Spaltenname:string);
      function CheckDBColumnExists(Spaltenname:string):boolean;
      procedure WriteDatabasecolumns;
      function  GetDataBaseDefinition:p_Datenbank_record;
      function GetCurrentProjectionString:string;
      procedure SetupBoundary(X:double; Y:double);
   private
      { private Definitionen }
      Anfang,Ende,Current:p_Layer_record;
      Anzahl:integer;
      ColumnAnzahl:integer;
      DBObj:ClassDatenbank;
end;

implementation
uses files,maindlg;

var Datenbank_Anfang, Akt_Datenbank:p_Datenbank_record;
    FileObj                         :ClassFile;
    DatenbankObj                    :ClassDatenbank;

constructor ClassDatenbank.Create;
begin
   Datenbank_Anfang:=new(p_Datenbank_record); Datenbank_Anfang:=nil;
   Akt_Datenbank:=new(p_Datenbank_record); Akt_Datenbank:=nil;
end;

constructor ClassArea.Create;
begin
   Anfang:=new(p_Area_record); Ende:=new(p_Area_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Area_record); Current:=nil; Anzahl:=0;
end;

constructor ClassText.Create;
begin
   Anfang:=new(p_Text_record); Ende:=new(p_Text_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Text_record); Current:=nil; Anzahl:=0;
end;

constructor ClassCircle.Create;
begin
   Anfang:=new(p_circle_record); Ende:=new(p_circle_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_circle_record); Current:=nil; Anzahl:=0;
end;

procedure ClassLayer.InsertDBColumn(Spaltenname:string);
begin
   DBObj.Neu(Spaltenname,0,Col_String,'',0,0,true,Current^.DBColumns);
   ColumnAnzahl:=ColumnAnzahl+1;
end;

function  ClassLayer.GetDataBaseDefinition:p_Datenbank_record;
begin
   result:=Current^.DBColumns;
end;

function ClassLayer.CheckDBColumnExists(Spaltenname:string):boolean;
var found:boolean;
    Zeiger:p_Datenbank_record;
begin
   Zeiger:=Current^.DBColumns;
   found:=false;
   if (Zeiger <> nil) then
   repeat
      if (Zeiger^.Bezeichnung = Spaltenname) then found:=true;
   until (Zeiger = nil) or (found = true);
   result:=found;
end;

constructor ClassLine.Create;
begin
   Anfang:=new(p_Line_record); Ende:=new(p_Line_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Line_record); Current:=nil; Anzahl:=0;
end;

constructor ClassPoint.Create;
begin
   Anfang:=new(p_Point_record); Ende:=new(p_Point_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Point_record); Current:=nil; Anzahl:=0;
end;

constructor ClassLayer.Create;
begin
   Anfang:=new(p_Layer_record); Ende:=new(p_Layer_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Layer_record); Current:=nil; Anzahl:=0; ColumnAnzahl:=0;
   DBObj:=ClassDatenbank.Create;
end;

destructor ClassDatenbank.Destroy;
var Zeiger1,Zeiger2:p_Datenbank_record;
begin
   Zeiger1:=Datenbank_Anfang;
   if Datenbank_Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      dispose(Zeiger2);
   until Zeiger1 = nil;
   Datenbank_Anfang:=nil; Akt_Datenbank:=nil;
end;

destructor ClassPoint.Destroy;
var Zeiger1,Zeiger2:p_Point_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      {DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil; }

      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassText.Destroy;
var Zeiger1,Zeiger2:p_Text_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      {DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil; }

      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassCircle.Destroy;
var Zeiger1,Zeiger2:p_Circle_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      {DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil; }

      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassLayer.Destroy;
var Zeiger1,Zeiger2:p_Layer_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      dispose(Zeiger2);
   until Zeiger1 = nil;
   DbObj.Destroy;
end;

destructor ClassArea.Destroy;
var Zeiger1,Zeiger2:p_Area_record;
    Punkt1,Punkt2: p_Punkt_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Punkte }
      Punkt1:=Zeiger2^.Punkte; Punkt2:=Zeiger2^.Punkte;
      if Punkt1 <> nil then
      repeat
         Punkt2:=Punkt1;
         Punkt1:=Punkt1^.next;
         dispose(Punkt2);
      until Punkt1 = nil;

      { L�schen der Datenbankinformation }
      {DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;}
      dispose(Zeiger2);
   until Zeiger1 = nil;
   Anfang:=nil;
end;

destructor ClassLine.Destroy;
var Zeiger1,Zeiger2:p_Line_record;
    Punkt1,Punkt2: p_Punkt_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Punkte }
      Punkt1:=Zeiger2^.Punkte; Punkt2:=Zeiger2^.Punkte;
      if Punkt1 <> nil then
      repeat
         Punkt2:=Punkt1;
         Punkt1:=Punkt1^.next;
         dispose(Punkt2);
      until Punkt1 = nil;

      { L�schen der Datenbankinformation }
      {DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil; }
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;


{*******************************************************************************
* FUNKTION : ClassDatenbank.Checkexists                                        *
********************************************************************************
* Funktion �berpr�ft, ob der entsprechende Spalteneintrag bereits in der       *
* Datenbankliste ist.                                                          *
*                                                                              *
* R�ckgabewert: TRUE  -> Eintrag ist bereits in Liste.                         *
*               FALSE -> Eintrag ist noch nicht in Liste.                      *
*                                                                              *
********************************************************************************}
function ClassDatenbank.Checkexists(Eintrag:string):boolean;
var Zeiger:p_Datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   if Zeiger = nil then begin Result:=false; exit; end;
   repeat
      if Zeiger^.Bezeichnung = Eintrag then gefunden:= true;
      Zeiger:=Zeiger^.next;
   until (Zeiger = nil) or (gefunden = true);
   Result:=gefunden;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.GetDataType                                        *
********************************************************************************
* Funktion ermittelt den Datentyp, den eine Datenbankspalte verwendet.         *
*                                                                              *
* PARAMETER: Bezeichnung   -> Spaltenname von der der Typ ermittelt werden soll*
*                                                                              *
* R�CKGABEWERT: Art des Datentyps                                              *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetDataType(Bezeichnung:string):integer;
var gefunden:boolean;
    Zeiger:p_Datenbank_record;
begin
   Zeiger:=Datenbank_Anfang; gefunden:=false;
   { Setzen des Zeigers an die entsprechende Datenbankspalte  }
   repeat
      if Zeiger <> nil then if Zeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
      if gefunden = false then zeiger:=zeiger^.next;
   until (gefunden = TRUE) or (Zeiger = nil);

   if gefunden = true then Result:=Zeiger^.Typ;
end;

{*******************************************************************************
* PROZEDUR : ClassArea.ValDatenbank                                            *
********************************************************************************
* Prozedur wertet die Datenbankinformation aus der erhaltenen Zeile aus.       *
*                                                                              *
* PARAMETER: Zeile    -> String aus der die Datenbankinformation ausgelesen    *
*                        wird.                                                 *
*                                                                              *
********************************************************************************}
procedure ClassArea.InsertInfoColumn(Spaltenname:string; Inhalt:string);
begin
   { Nun muss auch der Datentyp ermittelt werden }
   DatenbankObj.SetLength(Spaltenname,length(Inhalt));
   DatenbankObj.Neu(Spaltenname,0,Col_String,Inhalt,0,0,TRUE,Current^.DBInfo);
end;

{*******************************************************************************
* PROZEDUR : ClassCircle.ValDatenbank                                          *
********************************************************************************
* Prozedur wertet die Datenbankinformation aus der erhaltenen Zeile aus.       *
*                                                                              *
* PARAMETER: Zeile    -> String aus der die Datenbankinformation ausgelesen    *
*                        wird.                                                 *
*                                                                              *
********************************************************************************}
procedure ClassCircle.InsertInfoColumn(Spaltenname:string; Inhalt:string);
begin
   { Nun muss auch der Datentyp ermittelt werden }
   DatenbankObj.SetLength(Spaltenname,length(Inhalt));
   DatenbankObj.Neu(Spaltenname,0,Col_String,Inhalt,0,0,TRUE,Current^.DBInfo);
end;

{*******************************************************************************
* PROZEDUR : ClassText.ValDatenbank                                            *
********************************************************************************
*                                                                              *
********************************************************************************}
procedure ClassText.InsertInfoColumn(Spaltenname:string;Inhalt:string);
begin
   { Nun muss auch der Datentyp ermittelt werden }
   DatenbankObj.SetLength(Spaltenname,length(Inhalt));
   DatenbankObj.Neu(Spaltenname,0,Col_String,Inhalt,0,0,TRUE,Current^.DBInfo);
end;

{*******************************************************************************
* PROZEDUR : ClassPoint.ValDatenbank                                           *
********************************************************************************
*                                                                              *
********************************************************************************}
procedure ClassPoint.InsertInfoColumn(Spaltenname:string; Inhalt:string);
var Zeiger:p_Datenbank_record;
    hstr:string;
    von,bis:integer;
    gefunden:boolean;
begin
   { Nun muss auch der Datentyp ermittelt werden }
   DatenbankObj.SetLength(Spaltenname,length(Inhalt));
   DatenbankObj.Neu(Spaltenname,0,Col_String,Inhalt,0,0,TRUE,Current^.DBInfo);
end;

{*******************************************************************************
* PROZEDUR : ClassLine.ValDatenbank                                            *
********************************************************************************
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassLine.InsertInfoColumn(Spaltenname:string; Inhalt:string);
begin
   DatenbankObj.SetLength(Spaltenname,length(Inhalt));
   DatenbankObj.Neu(Spaltenname,0,Col_String,Inhalt,0,0,TRUE,Current^.DBInfo);
end;

procedure ClassDatenbank.Insertcolumn(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string;von:integer;bis:integer;Art:boolean);
begin
   Neu(Bezeichnung, Laenge, Typ, Inhalt,von,bis,Art,Datenbank_anfang);
end;


{*******************************************************************************
* PROZEDUR : ClassDatenbank.ResetColumn                                        *
********************************************************************************
* Prozedur dient dazu den Aktellen Spalteneintragszeiger auf den Anfang zu     *
* setzen.                                                                      *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.ResetColumn;
begin
   Akt_Datenbank:=Datenbank_Anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.ResetCurrent                                           *
********************************************************************************
* Proezdur setzt den Current Layerzeiger auf den Anfang.                       *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassLayer.ResetCurrent;
begin
   Current:=Anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.SetCurrentnext                                         *
********************************************************************************
* Proezdur setzt den Current Layerzeiger auf den n�chsten Eintrag.             *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassLayer.SetCurrentnext;
begin
   if Current^.next <> nil then Current:=Current^.next;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.GetAnzahl                                              *
********************************************************************************
* Funktion ermittelt die Anzahl der vorhandenen Layer.                         *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�CKGABEWERT: Anzahl der Layer                                               *
*                                                                              *
********************************************************************************}
function ClassLayer.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.GetCurrentLayername                                    *
********************************************************************************
* Funktion ermittelt den aktuellen Layernamen.                                 *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�CKGABEWERT: Name des Layers.                                               *
*                                                                              *
********************************************************************************}
function ClassLayer.GetCurrentLayername:string;
begin
   Result:=Current^.Layername;
end;

procedure ClassLayer.SetupBoundary(X:double; Y:double);
begin
   // the boundary data of the current layer will be processed
   if not Current^.boundaryset then
   begin
      Current^.Min_X:=X;
      Current^.Max_X:=X;
      Current^.Min_Y:=Y;
      Current^.Max_Y:=Y;
      Current^.boundaryset:=TRUE;
   end
   else
   begin
      if X < Current^.Min_X then Current^.Min_X:=X;
      if X > Current^.Max_X then Current^.Max_X:=X;
      if Y < Current^.Min_Y then Current^.Min_Y:=Y;
      if Y > Current^.Max_Y then Current^.Max_Y:=Y;
   end;
end;

function ClassLayer.GetCurrentProjectionString:string;
var retVal:string;
begin
   // the projection will be set to no projection for MapInfo
   retVal:='CoordSys NonEarth Units "m" Bounds('+MainWnd.FloatToMyStr(Current^.Min_X)+','+MainWnd.FloatToMyStr(Current^.Min_Y)+')'+
                                             '('+MainWnd.FloatToMyStr(Current^.Max_X)+','+MainWnd.FloatToMyStr(Current^.Max_Y)+')';
   result:=retVal;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.Checkexists                                            *
********************************************************************************
* Funktion �berpr�ft, ob dieser Layer bereits in der Liste vorkommt.           *
*                                                                              *
* PARAMETER: Layername -> Layername, dessen Vorkommen �berpr�ft werden soll.   *
*                                                                              *
* R�CKGABEWERT: TRUE -> Layer existiert bereits                                *
*               FALSE -> Layer existiert noch nicht                            *
********************************************************************************}
function ClassLayer.CheckExists(Layername:string):boolean;
var Zeiger:p_Layer_record;
    gefunden:boolean;
begin
   Zeiger:=Anfang; gefunden:=false;
   if Zeiger = nil then result:=false
   else
   begin
      repeat
         if Zeiger^.Layername <> Layername then Zeiger:=Zeiger^.next else gefunden:=true;
      until (gefunden = true) or (Zeiger = nil);
   end;
   if (Zeiger = nil) then result:=false else begin result:=true; Current:=Zeiger; end;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.SetLinestyle                                           *
********************************************************************************
* Prozedur dient zum setzen des Linientyps und der Linienfarbe auf dem Current *
* Layer.                                                                       *
*                                                                              *
* PARAMETER: Linetype  -> Typ, der Linie                                       *
*            Linecolor -> Farbe der Linie                                      *
*                                                                              *
********************************************************************************}
procedure ClassLayer.SetLinestyle(Linetype:longint;LineColor:longint);
begin
   Current^.Linestyle:=Linetype;
   Current^.Linecolor:=LineColor;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.Hextoint                                               *
********************************************************************************
* Prozedur dient zum Auswerten einer zweistelligen Hexadezimalzahl             *
*                                                                              *
* PARAMETER: Wert -> String, der Hexadezimalzahl beinhaltet.                   *
*                                                                              *
* R�CKGABEWERT: Integerwert der Hexzahl                                        *
*                                                                              *
********************************************************************************}
function ClassLayer.Hextoint(Wert:string):longint;
var Zeichen1,Zeichen2:string;
    Wert1,Wert2:longint;
    count:integer;
begin
   Zeichen1:=copy(Wert,1,1); Zeichen2:=copy(Wert,2,1);
   if Zeichen1 = 'A' then Wert1:=10 else
   if Zeichen1 = 'B' then Wert1:=11 else
   if Zeichen1 = 'C' then Wert1:=12 else
   if Zeichen1 = 'D' then Wert1:=13 else
   if Zeichen1 = 'E' then Wert1:=14 else
   if Zeichen1 = 'F' then Wert1:=15 else
      val(Zeichen1,Wert1,count);
   Wert1:=Wert1 * 16;

   if Zeichen2 = 'A' then Wert2:=10 else
   if Zeichen2 = 'B' then Wert2:=11 else
   if Zeichen2 = 'C' then Wert2:=12 else
   if Zeichen2 = 'D' then Wert2:=13 else
   if Zeichen2 = 'E' then Wert2:=14 else
   if Zeichen2 = 'F' then Wert2:=15 else
      val(Zeichen2,Wert2,count);

   Result:=Wert1 + Wert2;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.SetAreastyle                                           *
********************************************************************************
* Prozedur dient zum setzen des Areatyps und der Areafarbe auf dem Current     *
* Layer.                                                                       *
*                                                                              *
* PARAMETER: Areatype  -> Typ, der Fl�che                                      *
*            Areacolor -> Farbe der Fl�che                                     *
*                                                                              *
********************************************************************************}
procedure ClassLayer.SetAreastyle(Areatype:longint;AreaColor:longint);
begin
   Current^.Areastyle:=Areatype;
   Current^.Areacolor:=Areacolor;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.GetCurrent                                             *
********************************************************************************
* Funktion gibt den Current Layerzeiger zur�ck.                                *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�CKGABEWERT: Zeiger auf den Current Layer.                                  *
********************************************************************************}
function ClassLayer.GetCurrent:p_Layer_record;
begin
   Result:=Current;
end;

{*******************************************************************************
* PROZEDUR : ClassText.Neu                                                     *
********************************************************************************
* Prozedur erzeugt einen neuen Text Eintrag in der Liste.                      *
*                                                                              *
* PARAMETER: X1    -> Erste X-Koordinate des Texts                             *
*            Y1    -> Erste Y-Koordinate des Texts                             *
*            X2    -> Zweite X-Koordinate des Texts                            *
*            Y2    -> Zweite Y-Koordinate des Texts                            *
*            Text  -> Inhalt des Texts                                         *
*            numlines -> Number of lines text-contains (multiline-texts)       *
*            Font  -> Font, der f�r diesen Text verwendet wird                 *
*            Size  -> Gr��e des Texts                                          *
*            Layer -> Zeiger auf den Layer auf dem sich diese Area befindet.   *
*                                                                              *
********************************************************************************}
procedure ClassText.Neu(X1:double;Y1:double;X2:double;Y2:double;Text:string;numlines:integer;Font:string;size:double;angle:double;Layer:p_Layer_record);
var neuer_record:p_Text_record;
begin
   new(neuer_record);
   neuer_record^.X1:=X1;
   neuer_record^.Y1:=Y1;
   neuer_record^.X2:=X2;
   neuer_record^.Y2:=Y2;
   neuer_record^.Text:=Text;
   neuer_record^.numlines:=numlines;
   neuer_record^.Font:=Font;
   neuer_record^.Size:=size;
   neuer_record^.Angle:=Angle;
   neuer_record^.Layer:=Layer;
   neuer_record^.DBInfo:=nil;

   // calculate the X2 and Y2 Corner
   // first setup the text
   neuer_record^.Text:=TrimRight(neuer_record^.Text);
   if Pos('"',neuer_record^.Text) <> 0 then repeat Delete(neuer_record^.Text,Pos('"',neuer_record^.Text),1); until Pos('"',neuer_record^.Text) = 0;

   neuer_record^.X2:=X1+GetLength(neuer_record^.Text,size);
   neuer_record^.Y2:=Y1+GetHeight(size,numlines);

   // setup the layer boundary by using the new calculated corners
   if not Layer^.boundaryset then
   begin
      Layer^.Min_X:=neuer_record^.X2;
      Layer^.Max_X:=neuer_record^.X2;
      Layer^.Min_Y:=neuer_record^.Y2;
      Layer^.Max_Y:=neuer_record^.Y2;
      Layer^.boundaryset:=TRUE;
   end
   else
   begin
      if neuer_record^.X2 < Layer^.Min_X then Layer^.Min_X:=neuer_record^.X2;
      if neuer_record^.X2 > Layer^.Max_X then Layer^.Max_X:=neuer_record^.X2;
      if neuer_record^.Y2 < Layer^.Min_Y then Layer^.Min_Y:=neuer_record^.Y2;
      if neuer_record^.Y2 > Layer^.Max_Y then Layer^.Max_Y:=neuer_record^.Y2;
   end;

   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassCircle.Neu                                                   *
********************************************************************************
* Prozedur erzeugt einen neuen Circle Eintrag in der Liste.                    *
*                                                                              *
* PARAMETER: X     -> X-Koordinate des Kreiseinsetzpunktes                     *
*            Y     -> Y-Koordinate des Kreiseinsetzpunktes                     *
*            Radius-> Radius des Kreises                                       *
*            Layer -> Zeiger auf den Layer auf dem sich dieser Kreis befindet. *
*                                                                              *
********************************************************************************}
procedure ClassCircle.Neu(X:double;Y:double;Radius:string;Layer:p_Layer_record);
var neuer_record:p_Circle_record;
    count:integer;
begin
   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.Layer:=Layer;
   neuer_record^.DBInfo:=nil;
   val(Radius,neuer_record^.Radius,count);
   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

procedure ClassArea.InsertIsland;
var new_record,iterate:p_Area_record;
begin
   new(new_record);
   new_record^.Punkte:=nil;
   Current^.IslandCount:=Current^.IslandCount+1;
   if Current^.Islands = NIL then
   begin
      Current^.Islands:=new_record;
      new_record^.next:=NIL;
   end
   else
   begin
      iterate:=Current^.Islands;
      while (iterate^.next <> nil) do iterate:=iterate^.next;
      iterate^.next:=new_record;
      new_record^.next:=nil;
   end;
   Current^.CurrentIsland:=new_record;
   Current^.CurrentIsland^.Punktanz:=0;
end;

procedure ClassArea.InsertPointToIsland(X:double;Y:double);
var iterate   :p_Punkt_record;
    new_record:p_Punkt_record;
    hstr      :string;
begin
   new(new_record);
   new_record^.X:=X; new_record^.Y:=Y;
   iterate:=Current^.CurrentIsland^.Punkte;
   if iterate = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current^.CurrentIsland^.Punkte:=new_record;
      Current^.CurrentIsland^.Punkte^.next:=NIL;
   end
   else
   begin
      while (iterate^.next <> nil) do iterate:=iterate^.next;
      iterate^.next:=new_record;
      new_record^.next:=nil;
   end;
   Current^.CurrentIsland^.Punktanz:=Current^.CurrentIsland^.Punktanz+1;
end;

{*******************************************************************************
* PROZEDUR : ClassArea.Neu                                                     *
********************************************************************************
* Prozedur erzeugt einen neuen Area Eintrag in der Liste.                      *
*                                                                              *
* PARAMETER: Layer -> Zeiger auf den Layer auf dem sich diese Area befindet.   *
*                                                                              *
********************************************************************************}
procedure ClassArea.Neu(Layer:p_Layer_record);
var neuer_record:p_Area_record;
begin
   new(neuer_record);
   neuer_record^.DBInfo:=nil;
   neuer_record^.Punkte:=nil;
   neuer_record^.Layer:=Layer;

   neuer_record^.CurrentIsland:=nil;
   neuer_record^.Islandcount  :=0;
   neuer_record^.Islands      :=nil;

   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;
   neuer_record^.Punktanz:=0;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassLine.Neu                                                     *
********************************************************************************
* Prozedur erzeugt einen neuen Line Eintrag in der Liste.                      *
*                                                                              *
* PARAMETER: Layer -> Zeiger auf den Layer auf dem sich diese Line befindet.   *
*                                                                              *
********************************************************************************}
procedure ClassLine.Neu(Layer:p_Layer_record);
var neuer_record:p_Line_record;
begin
   new(neuer_record);
   neuer_record^.DBInfo:=nil;
   neuer_record^.Punkte:=nil;
   neuer_record^.Layer:=Layer;
   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;
   neuer_record^.Punktanz:=0;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassPoint.Neu                                                    *
********************************************************************************
* Prozedur erzeugt einen neuen Point Eintrag in der Liste.                     *
*                                                                              *
* PARAMETER: X -> X-Koordinate des Punkts                                      *
*            Y -> Y-Koordinate des Punkts                                      *
*            Layer -> Zeiger auf den Layer auf dem sich dieser Point befindet. *
*                                                                              *
********************************************************************************}
procedure ClassPoint.Neu(X:double;Y:double;Layer:p_Layer_record);
var neuer_record:p_Point_record;
begin
   new(neuer_record);
   neuer_record^.DBInfo:=nil;
   neuer_record^.Layer:=Layer;
   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassArea.InsertPoint                                             *
********************************************************************************
* Prozedur f�gt einen Punkt in die Current Area ein.                           *
*                                                                              *
* PARAMETER: X -> X-Koordinate des Punktes                                     *
*            Y -> Y-Koordinate des Punktes                                     *
*                                                                              *
********************************************************************************}
procedure ClassArea.InsertPoint(X:double;Y:double);
var Zeiger:p_Punkt_record;
    new_Zeiger:p_Punkt_record;
    hstr      :string;
begin
   new(new_Zeiger);
   { Kontrolle, ob die Spalten X und Y bereits in der Datenbankliste sind }
   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   Zeiger:=Current^.Punkte;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current^.Punkte:=New_Zeiger;
      Current^.Punkte^.next:=NIL;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
   end;
   Current^.Punktanz:=Current^.Punktanz + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassArea.WriteData                                               *
********************************************************************************
* Prozedur schreibt die Daten der Area, die sich auf einem bestimmten          *
*                                                                              *
* PARAMETER: Layername -> Name des Layers auf dem sich die Objekte befinden    *
*                         sollen.
*                                                                              *
********************************************************************************}
procedure ClassArea.WriteData(Layername:string);
var Zeiger:p_Area_record;
    Punktzeiger:p_Punkt_record;
    DBZeiger:p_Datenbank_record;
    Zeile,hstr:string;
    Current_Island:p_Area_record;
    laenge:integer;
begin
   Zeiger:=Anfang;
   while (Zeiger <> nil) do
   begin
      if Zeiger^.Layer^.Layername = Layername then
      begin
         hstr:='Region  '+inttostr(Zeiger^.Islandcount+1);
         FileObj.WritemifLine(hstr);

         // now write first polygon
         FileObj.WritemifLine('  '+inttostr(Zeiger^.Punktanz));
         { Nun werden die eigentlichen Punkte geschrieben }
         Punktzeiger:=Zeiger^.Punkte;
         while (Punktzeiger <> nil) do
         begin
            laenge:=DatenbankObj.GetLength('X-KOORDINATE');
            hstr:=MainWnd.FloatToMyStr(Punktzeiger^.X); if length(hstr) < laenge then repeat hstr:=' '+hstr; until length(hstr) >= laenge;
            Zeile:=hstr;

            laenge:=DatenbankObj.GetLength('Y-KOORDINATE');
            hstr:=MainWnd.FloatToMyStr(Punktzeiger^.Y); if length(hstr) < laenge then repeat hstr:=' '+hstr; until length(hstr) >= laenge;
            Zeile:=Zeile + ' '+ hstr;

            FileObj.WritemifLine(Zeile);
            Punktzeiger:=Punktzeiger^.next;
         end;

         // write islands
         Current_Island:=Zeiger^.Islands;
         while (Current_Island <> nil) do
         begin
            FileObj.WritemifLine('  '+inttostr(Current_Island^.Punktanz));

            Punktzeiger:=Current_Island^.Punkte;
            while (Punktzeiger <> nil) do
            begin
               laenge:=DatenbankObj.GetLength('X-KOORDINATE');
               hstr:=MainWnd.FloatToMyStr(Punktzeiger^.X); if length(hstr) < laenge then repeat hstr:=' '+hstr; until length(hstr) >= laenge;
               Zeile:=hstr;

               laenge:=DatenbankObj.GetLength('Y-KOORDINATE');
               hstr:=MainWnd.FloatToMyStr(Punktzeiger^.Y); if length(hstr) < laenge then repeat hstr:=' '+hstr; until length(hstr) >= laenge;
               Zeile:=Zeile + ' '+ hstr;

               FileObj.WritemifLine(Zeile);
               Punktzeiger:=Punktzeiger^.next;
            end;
            Current_Island:=Current_Island^.next;
         end;

         { Nun wird die Pen-Information geschrieben }
         FileObj.WritemifLine('   Pen (1,'+ inttostr(Zeiger^.Layer^.Linestyle)+',' +inttostr(Zeiger^.Layer^.Linecolor)+')');
         { Nun wird die Brush-Information geschrieben }
         FileObj.WritemifLine('   Brush ('+inttostr(Zeiger^.Layer^.Areastyle)+','+inttostr(Zeiger^.Layer^.Areacolor)+',16777215)');

         { Nun muss noch die Datenbankinformation geschrieben werden }
         DBZeiger:=Zeiger^.DBInfo; Zeile:='';
         if DBZeiger = nil then Zeile:='""'
         else
         while (DBZeiger <> nil) do
         begin
            if DatenbankObj.GetDataType(DBZeiger^.Bezeichnung) = Col_String then Zeile:=Zeile + '"'+DBZeiger^.Inhalt+'"'
            else Zeile:=Zeile + DBZeiger^.Inhalt;
            DBZeiger:=DBZeiger^.next;
            if DBZeiger <> nil then Zeile:=Zeile + ',';
         end;
         FileObj.WritemidLine(Zeile);
      end;
      Zeiger:=Zeiger^.next;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassLine.WriteData                                               *
********************************************************************************
* Prozedur schreibt die Daten der Line, die sich auf einem bestimmten          *
*                                                                              *
* PARAMETER: Layername -> Name des Layers auf dem sich die Objekte befinden    *
*                         sollen.
*                                                                              *
********************************************************************************}
procedure ClassLine.WriteData(Layername:string);
var Zeiger:p_Line_record;
    Punktzeiger:p_Punkt_record;
    DBZeiger:p_Datenbank_record;
    laenge:integer;
    Zeile,hstr:string;
begin
   Zeiger:=Anfang;
   while (Zeiger <> nil) do
   begin
      if Zeiger^.Layer^.Layername = Layername then
      begin
         FileObj.WritemifLine('Pline  '+inttostr(Zeiger^.Punktanz));
         { Nun werden die eigentlichen Punkte geschrieben }
         Punktzeiger:=Zeiger^.Punkte;
         while (Punktzeiger <> nil) do
         begin
            laenge:=DatenbankObj.GetLength('X-KOORDINATE');
            hstr:=MainWnd.FloatToMyStr(Punktzeiger^.X); if length(hstr) < laenge then repeat hstr:=' '+hstr; until length(hstr) >= laenge;
            Zeile:=hstr;

            laenge:=DatenbankObj.GetLength('Y-KOORDINATE');
            hstr:=MainWnd.FloatToMyStr(Punktzeiger^.Y); if length(hstr) < laenge then repeat hstr:=' '+hstr; until length(hstr) >= laenge;
            Zeile:=Zeile +' '+hstr;

            FileObj.WritemifLine(Zeile);
            Punktzeiger:=Punktzeiger^.next;
         end;
         { Nun wird die Pen-Information geschrieben }
         FileObj.WritemifLine('   Pen (1,'+ inttostr(Zeiger^.Layer^.Linestyle)+',' +inttostr(Zeiger^.Layer^.Linecolor)+')');

         { Nun muss noch die Datenbankinformation geschrieben werden }
         DBZeiger:=Zeiger^.DBInfo; Zeile:='';
         if DBZeiger = nil then Zeile:='""'
         else
         while (DBZeiger <> nil) do
         begin
            if DatenbankObj.GetDataType(DBZeiger^.Bezeichnung) = Col_String then Zeile:=Zeile + '"'+DBZeiger^.Inhalt+'"'
            else Zeile:=Zeile + DBZeiger^.Inhalt;
            DBZeiger:=DBZeiger^.next;
            if DBZeiger <> nil then Zeile:=Zeile + ',';
         end;
         FileObj.WritemidLine(Zeile);
      end;
      Zeiger:=Zeiger^.next;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassText.WriteData                                               *
********************************************************************************
* Prozedur schreibt die Daten des Textes, die sich auf einem bestimmten        *
* Layer befinden.                                                              *
*                                                                              *
* PARAMETER: Layername -> Name des Layers auf dem sich die Objekte befinden    *
*                         sollen.                                              *
*                                                                              *
********************************************************************************}
procedure ClassText.WriteData(Layername:string);
var Zeiger:p_Text_record;
    DBZeiger:p_Datenbank_record;
    Zeile,hstr:string;
begin
   Zeiger:=Anfang;
   while (Zeiger <> nil) do
   begin
      if Zeiger^.Layer^.Layername = Layername then
      begin
         FileObj.WriteMIFLine('Text');
         FileObj.WriteMIFLine('    "'+Zeiger^.Text+'"');
         FileObj.WriteMIFLine('    '+MainWnd.FloatToMyStr(Zeiger^.X1)+' '+MainWnd.FloatToMyStr(Zeiger^.Y1)+' '+MainWnd.FloatToMyStr(Zeiger^.X2)+' '+MainWnd.FloatToMyStr(Zeiger^.Y2));
         FileObj.WriteMIFLine('    Font ("'+Zeiger^.Font+'",0,0,0)');

         FileObj.WriteMIFLine('    Angle('+MainWnd.FloatToMyStr(Zeiger^.Angle)+')');
         { Nun muss noch die Datenbankinformation geschrieben werden }
         DBZeiger:=Zeiger^.DBInfo; Zeile:='';
         if DBZeiger = nil then Zeile:='""'
         else
         while (DBZeiger <> nil) do
         begin
            if DatenbankObj.GetDataType(DBZeiger^.Bezeichnung) = Col_String then Zeile:=Zeile + '"'+DBZeiger^.Inhalt+'"'
            else Zeile:=Zeile + DBZeiger^.Inhalt;
            DBZeiger:=DBZeiger^.next;
            if DBZeiger <> nil then Zeile:=Zeile + ',';
         end;
         FileObj.WritemidLine(Zeile);
      end;
      Zeiger:=Zeiger^.next;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassCircle.WriteData                                             *
********************************************************************************
* Prozedur schreibt die Daten des Kreises, die sich auf einem bestimmten       *
* Layer befinden.                                                              *
*                                                                              *
* PARAMETER: Layername -> Name des Layers auf dem sich die Objekte befinden    *
*                         sollen.                                              *
*                                                                              *
********************************************************************************}
procedure ClassCircle.WriteData(Layername:string);
var Zeiger:p_Circle_record;
    DBZeiger:p_Datenbank_record;
    Zeile,hstr:string;
begin
   Zeiger:=Anfang;
   while (Zeiger <> nil) do
   begin
      if Zeiger^.Layer^.Layername = Layername then
      begin
         FileObj.WriteMIFLine('Ellipse '+MainWnd.FloatToMyStr(Zeiger^.X - Zeiger^.Radius)+' '+MainWnd.FloatToMyStr(Zeiger^.Y - Zeiger^.Radius)+' '+MainWnd.FloatToMyStr(Zeiger^.X + Zeiger^.Radius)+ ' ' + MainWnd.FloatToMyStr(Zeiger^.Y + Zeiger^.Radius));

         { Nun wird die Pen-Information geschrieben }
         FileObj.WritemifLine('   Pen (1,'+ inttostr(Zeiger^.Layer^.Linestyle)+',' +inttostr(Zeiger^.Layer^.Linecolor)+')');
         { Nun wird die Brush-Information geschrieben }
         FileObj.WritemifLine('   Brush ('+inttostr(Zeiger^.Layer^.Areastyle)+','+inttostr(Zeiger^.Layer^.Areacolor)+',16777215)');

         { Nun muss noch die Datenbankinformation geschrieben werden }
         DBZeiger:=Zeiger^.DBInfo; Zeile:='';
         if DBZeiger = nil then Zeile:='""'
         else
         while (DBZeiger <> nil) do
         begin
            if DatenbankObj.GetDataType(DBZeiger^.Bezeichnung) = Col_String then Zeile:=Zeile + '"'+DBZeiger^.Inhalt+'"';
            if DatenbankObj.GetDataType(DBZeiger^.Bezeichnung) = Col_Int    then Zeile:=Zeile + DBZeiger^.Inhalt;
            if DatenbankObj.GetDataType(DBZeiger^.Bezeichnung) = Col_Float  then Zeile:=Zeile + DBZeiger^.Inhalt;
            DBZeiger:=DBZeiger^.next;
            if DBZeiger <> nil then Zeile:=Zeile + ',';
         end;
         FileObj.WritemidLine(Zeile);
      end;
      Zeiger:=Zeiger^.next;
   end;
end;

{*******************************************************************************
* FUNKTION : ClassText.GetLength                                               *
********************************************************************************
* Funktion ermittelt die L�nge eines Textes in Meter                           *
*                                                                              *
* PARAMETER: Text -> Text, von dem die L�nge ermittelt werden soll.            *
*            Size -> Gr��e, die der Text hat.                                  *
*                                                                              *
* R�CKGABEWERT: L�nge des Texts in Meter.                                      *
*                                                                              *
********************************************************************************}
function ClassText.GetLength(Text:string;Size:double):double;
begin
    Result:=length(Text) * (Size * 4/10);
end;


{*******************************************************************************
* FUNKTION : ClassText.GetHeight                                               *
********************************************************************************
* Funktion ermittelt die H�he eines Textes in Meter                            *
*                                                                              *
* PARAMETER: Size -> Gr��e, die der Text hat.                                  *
*                                                                              *
* R�CKGABEWERT: H�he des Texts in Meter.                                       *
*                                                                              *
********************************************************************************}
function ClassText.GetHeight(Size:double;numlines:integer):double;
var retVal:double;
begin
   retVal:=Size * 6 / 10;
   retVal:=retVal*numlines;
   Result:=retVal;
end;

{*******************************************************************************
* FUNKTION : ClassText.GetAngle                                                *
********************************************************************************
* Funktion ermittelt den Winkel mit dem ein Text rotiert ist.                  *
*                                                                              *
* PARAMETER: X1 -> Erste X-Koordinate des Texts                                *
*            Y1 -> Erste Y-Koordinate des Texts                                *
*            X2 -> Zweite X-Koordinate des Texts                               *
*            Y2 -> Zweite Y-Koordinate des Texts                               *
*                                                                              *
* R�CKGABEWERT: Winkel mit den dieser Text rotiert ist                         *
*                                                                              *
********************************************************************************}
function ClassText.GetAngle(X1:double;Y1:double;X2:double;Y2:double):double;
var A,B,C:double;
    Alpha:double;
begin
   { Zuwerst werden die Kantenl�ngen des Dreiecks ermittelt }
   A:=ABS(Y2 - Y1); B:=ABS(X2 - X1); C:=SQRT(SQR(A) + SQR(B));

   if C <> 0 then
   begin
      Alpha:=(A / C);  { Nun ist der Sinus von Alpha bekannt }

      { Nun muss man noch den Invers Sinus von Alpha berechnen }
      Alpha:=ArcSin(Alpha);
      Result:=(Alpha*180) / PI;
   end
   else
      Result:=0;
end;

{*******************************************************************************
* PROZEDUR : ClassPoint.WriteData                                              *
********************************************************************************
* Prozedur schreibt die Daten des Points, die sich auf einem bestimmten        *
* Layer befinden.                                                              *
*                                                                              *
* PARAMETER: Layername -> Name des Layers auf dem sich die Objekte befinden    *
*                         sollen.                                              *
*                                                                              *
********************************************************************************}
procedure ClassPoint.WriteData(Layername:string);
var Zeiger:p_Point_record;
    DBZeiger:p_Datenbank_record;
    laenge:integer;
    Zeile,hstr:string;
begin
   Zeiger:=Anfang;
   while (Zeiger <> nil) do
   begin
      if Zeiger^.Layer^.Layername = Layername then
      begin
         FileObj.WritemifLine('Point  '+MainWnd.FloatToMyStr(Zeiger^.X)+' '+MainWnd.FloatToMyStr(Zeiger^.Y));
         { Nun muss noch die Datenbankinformation geschrieben werden }
         DBZeiger:=Zeiger^.DBInfo; Zeile:='';
         if DBZeiger = nil then Zeile:='""'
         else
         while (DBZeiger <> nil) do
         begin
            if DatenbankObj.GetDataType(DBZeiger^.Bezeichnung) = Col_String then Zeile:=Zeile + '"'+DBZeiger^.Inhalt+'"'
            else Zeile:=Zeile + DBZeiger^.Inhalt;
            DBZeiger:=DBZeiger^.next;
            if DBZeiger <> nil then Zeile:=Zeile + ',';
         end;
         FileObj.WritemidLine(Zeile);
      end;
      Zeiger:=Zeiger^.next;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassLine.InsertPoint                                             *
********************************************************************************
* Prozedur f�gt einen Punkt in die Current Line ein.                           *
*                                                                              *
* PARAMETER: X -> X-Koordinate des Punktes                                     *
*            Y -> Y-Koordinate des Punktes                                     *
*                                                                              *
********************************************************************************}
procedure ClassLine.InsertPoint(X:double;Y:double);
var Zeiger:p_Punkt_record;
    new_Zeiger:p_Punkt_record;
    hstr      :string;
begin
   new(new_Zeiger);
   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   Zeiger:=Current^.Punkte;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current^.Punkte:=New_Zeiger;
      Current^.Punkte^.next:=NIL;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
   end;
   Current^.Punktanz:=Current^.Punktanz + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.Neu                                                    *
********************************************************************************
* Prozedur erzeugt einen neuen Eintrag in der Layerlist.                       *
*                                                                              *
* PARAMETER: Layername -> Name des Layers, der neu eingef�gt werden soll.      *
*                                                                              *
********************************************************************************}
procedure ClassLayer.Neu(Layername:string);
var neuer_record,Zeiger:p_Layer_record;
begin
   new(neuer_record);
   neuer_record^.Layername:=Layername;
   neuer_record^.Linestyle:=1;
   neuer_record^.Linecolor:=16777215;
   neuer_record^.Areastyle:=1;
   neuer_record^.Areacolor:=16777215;
   neuer_record^.ObjectsonLayer:=0;
   neuer_record^.boundaryset:=FALSE;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
   Current^.DBColumns:=nil;
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.Neu                                                *
********************************************************************************
* Prozedur erzeugt einen neuen Spalteneintrag f�r die Datenbank.               *
*                                                                              *
* PARAMETER: Bezeichnung -> Spaltenbezeichnung                                 *
*            Laenge      -> L�nge der Spalte                                   *
*            Typ         -> Typ der Spalte                                     *
*            Inhalt      -> Inhalt der Spalte                                  *
*            von         -> Beginn der Spalte im ASC-File                      *
*            bis         -> Ende der Spalte im ADF-File                        *
*            Art         -> Art der Spalte T -> DB-Spalte F -> ADF-Spalte      *
*            Anfang      -> Zeiger auf Anfang der Liste in die eingef�gt wird  *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.Neu(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string; von:integer; bis:integer; Art:boolean; var Anfang:p_datenbank_record);
var neuer_record,Zeiger:p_datenbank_record;
begin
   new(neuer_record);

   neuer_record^.Bezeichnung:=Bezeichnung;
   neuer_record^.Laenge:=Laenge;
   neuer_record^.Typ:=Typ;
   neuer_record^.Inhalt:=Inhalt;
   neuer_record^.von:=von;
   neuer_record^.bis:=bis;
   neuer_record^.Art:=Art;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=Anfang;
      repeat
         if Zeiger^.next <> nil then Zeiger:=Zeiger^.next;
      until Zeiger^.next = nil;
      Zeiger^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
   end;
end;


{*******************************************************************************
* PROZEDUR : ClassDatenbank.SetLength                                          *
********************************************************************************
* Prozedur dient dazu die erforderliche Spaltenbreite einer Spalte zu setzen.  *
*                                                                              *
* InPARAMETER: Bezeichnung -> Name der Spalte                                  *
*              Laenge      -> Breite der Spalte                                *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.SetLength(Bezeichnung:string; Laenge:integer);
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if Zeiger^.Laenge < Laenge then Zeiger^.Laenge:=Laenge;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.GetLength                                          *
********************************************************************************
* Funktion dient zum ermitteln der erforderlichen Spaltenbreite f�r einen ADF  *
* Eintrag.                                                                     *
*                                                                              *
* PARAMETER:   Bezeichnung -> Name der Spalte                                  *
*                                                                              *
* R�ckgabewert: Spaltenbreite f�r die entsprechende Spalte.                    *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetLength(Bezeichnung:string):integer;
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then Result:=Zeiger^.Laenge else Result:=0;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.GetItem                                            *
********************************************************************************
* Funktion dient zum Ermitteln eines Datenbankwertes.                          *
*                                                                              *
*                                                                              *
* PARAMETER:   Bezeichnung -> Name der Spalte                                  *
*                                                                              *
* R�ckgabewert: Inhalt dieser Spalte.                                          *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetItem(Bezeichnung:string):string;
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then Result:=Zeiger^.Inhalt else Result:='';
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.WriteDatabasecolumns                                   *
********************************************************************************
* Prozedur dient zum Schreiben der Datenbankdefinition in das Mapinfo file.    *
*                                                                              *
* PARAMETER:   keine                                                           *
*                                                                              *
********************************************************************************}
procedure ClassLayer.WriteDatabasecolumns;
var Zeiger,Zeiger2:p_Datenbank_record;
    Zeile,hstr:string;
    anzahl,Laenge:integer;
    gefunden:boolean;
    Typ:integer;
begin
   Zeiger:=Current^.DBColumns; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }

   if (Columnanzahl > 0) and (Zeiger <> nil) then { Es sind Datenbankspalten vorhanden }
   begin
      FileObj.WritemifLine('Columns '+inttostr(Columnanzahl));
      repeat
         // write database definitions
         Zeile:='  '+Zeiger^.Bezeichnung+' ';
         Laenge:=DatenbankObj.GetLength(Zeiger^.Bezeichnung);
         Typ:=DatenbankObj.GetDataType(Zeiger^.Bezeichnung);
         if Typ = Col_String then hstr:='Char('+inttostr(Laenge)+')'
         else if Typ = Col_Int then hstr:='Integer'
         else if Typ = Col_SmallInt then hstr:='Smallint'
         else if Typ = Col_Float then hstr:='Float'
         else if Typ = Col_Date then hstr:='Date'
         else hstr:='Char(10)';
         Zeile:=Zeile + hstr;
         FileObj.WritemifLine(Zeile);
         Zeiger:=Zeiger^.next;
      until Zeiger = nil;
   end
   else
   begin
      { Wenn keine Datenbankspalte vorhanden ist muss eine erzeugt werden }
      FileObj.WritemifLine('Columns 1');
      FileObj.WritemifLine('  Info Char(10)');
   end;
end;

end.

