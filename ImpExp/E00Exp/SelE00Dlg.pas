unit SelE00Dlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StrFile, StdCtrls, Buttons, FileCtrl, ComCtrls, ExtCtrls, Db, ADODB,
  ShellCtrls, FileSelectionPanel, WinOSInfo;

type
  TSelE00Wnd = class(TForm)
    PageControl: TPageControl;
    TabSheetSource: TTabSheet;
    LayerLB: TListBox;
    TabSheetDest: TTabSheet;
    LayerCB: TCheckBox;
    OKBtn: TButton;
    CancelBtn: TButton;
    GeoBtn: TButton;
    Panel2: TPanel;
    SelectedCB: TCheckBox;
    SetupWindowTimer: TTimer;
    Panel1: TPanel;
    SrcDbLabel: TLabel;
    DbBtn: TButton;
    FileSelectionPanel: TFileSelectionPanel;
    ShellTreeView: TShellTreeView;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure LayerCBClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GeoBtnClick(Sender: TObject);
    procedure SelectedCBClick(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure LayerLBClick(Sender: TObject);
    procedure DbBtnClick(Sender: TObject);
    procedure FileSelectionPanelDirectoryChanged(Sender: TObject);
  private
    { Private declarations }
    DestinationE00Directory :string;
    FirstActivate           :boolean;
    procedure CheckWindow;
  public
    { Public declarations }
    winheight:integer;
    winwidth :integer;
    wintop   :integer;
    winleft  :integer;
    OkBtnMode:boolean;
    procedure GetE00Dir(var E00Dir:string);
    procedure CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
    procedure SetupForBatch;
    procedure SetExpSettings(ProjectDir:string; ExpMode:integer);
    procedure AddExpLayer(ExpLayer:string);
  end;

var
  SelE00Wnd     : TSelE00Wnd;
  StrFileObj    : ClassStrfile;

implementation

uses Maindlg, inifiles;

{$R *.DFM}

procedure TSelE00Wnd.FormCreate(Sender: TObject);
var myScale:double;
    oldHeight:integer;
    oldWidth :integer;
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   Self.Caption:=StrfileObj.Assign(1);                 { PROGIS ESRI-E00 Export 2000 }
   TabsheetSource.Caption:=StrfileObj.Assign(2);       { Source Project              }
   TabsheetDest.Caption:=StrfileObj.Assign(3);         { Destination ESRI-E00        }
   LayerCB.Caption:=StrfileObj.Assign(4);              { Export only selected layers }
   SelectedCB.Caption:=StrfileObj.Assign(43);          { Export only selected objects }

   OkBtn.Caption:=StrfileObj.Assign(7);                { Ok }
   CancelBtn.Caption:=StrfileObj.Assign(8);            { Cancel }
   GeoBtn.Caption:=StrfileObj.Assign(9);               { Projection }
   DbBtn.Caption:=StrfileObj.Assign(5);                // Database

   LayerLB.enabled:=false;

   DestinationE00Directory:='';

   oldHeight:=Self.Height;
   oldWidth:=Self.Width;
   Self.Height:=1;
   Self.Width:=1;

   myScale:=1; //100000 / Trunc(9600000/(Self.PixelsPerInch));
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
   
   // now adapt winheigt and winwidth
   winheight:=round(oldHeight*myScale);
   winwidth:=round(OldWidth*myScale);
   FirstActivate:=true;
end;

procedure TSelE00Wnd.OKBtnClick(Sender: TObject);
var inifile:TIniFile;
begin
   MainWnd.ExitNormal:=true;
   // write the current settings to the ConGIS.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'ConGIS.ini');
   inifile.WriteString('E00EXP','ExpDbMode',inttostr(MainWnd.AXImpExpDbc.ExpDbMode));
   inifile.WriteString('E00EXP','ExpDatabase',MainWnd.AXImpExpDbc.ExpDatabase);
   inifile.WriteString('E00EXP','ExpTable',MainWnd.AXImpExpDbc.ExpTable);
   inifile.WriteString('E00EXP','LastUsedPath',FileSelectionPanel.Directory);
   inifile.Destroy;

   // now the export window can be opened and the export executed
   Self.Close;
   MainWnd.Show;
end;

procedure TSelE00Wnd.CancelBtnClick(Sender: TObject);
begin
   MainWnd.ExitNormal:=false;
   MainWnd.CloseTimer.Enabled:=true;
end;

procedure TSelE00Wnd.GetE00Dir(var E00Dir:string);
begin
   E00Dir:=DestinationE00Directory;
end;

procedure TSelE00Wnd.LayerCBClick(Sender: TObject);
begin
   if LayerCB.Checked then
   begin
      LayerLB.enabled:=true;
      SelectedCB.checked:=false;
   end
   else
   begin
      LayerLB.enabled:=false;
   end;
   CheckWindow;
end;

procedure TSelE00Wnd.CheckWindow;
var allok:boolean;
    i    :integer;
begin
   allok:=true;
   { Procedure checks all settings in the window }
   if DestinationE00Directory = '' then allok:=false;
   if LayerCb.Checked and allok then
   begin
      allok:=FALSE;
      for i:=0 to LayerLb.Items.Count - 1 do
      begin
         if LayerLb.Selected[i] then
         begin
            allok:=TRUE;
            break;
         end;
      end;
   end;
   if allok then OKBtn.Enabled:=true
   else OkBtn.Enabled:=false;
end;

procedure TSelE00Wnd.FormActivate(Sender: TObject);
var Layername:string;
    layercnt:integer;
    ALayer:Variant;
begin
   if FirstActivate then
   begin
      // fill layerlist
      LayerLB.Items.Clear;
      for layercnt:=0 to MainWnd.SourceDocument.Layers.Count-1 do
      begin
         ALayer:=MainWnd.SourceDocument.Layers.Items[layercnt];
         Layername:=ALayer.Layername;
         LayerLb.Items.Add(Layername);
      end;
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TSelE00Wnd.GeoBtnClick(Sender: TObject);
begin
   MainWnd.AxGisProjection.SetupByDialog;
end;

procedure TSelE00Wnd.SelectedCBClick(Sender: TObject);
begin
   if SelectedCB.Checked then
      LayerCB.Checked:=false;
end;

procedure TSelE00Wnd.CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
begin
   wintop:=(WHEIGHT div 2) - (winheight div 2);
   winleft:=(WWIDTH div 2) - (winwidth div 2);
end;

procedure TSelE00Wnd.SetupWindowTimerTimer(Sender: TObject);
var dummy:string;
    intval, count:integer;
    inifile:TIniFile;
begin
   SetupWindowTimer.Enabled:=false;
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,winleft,wintop,winwidth, winheight,SWP_SHOWWINDOW);
   Self.Height:=winheight;
   Self.Width:=winwidth;
   if MainWnd.SourceDocument.NrOfSelectedObjects = 0 then
      SelectedCb.Visible:=FALSE;
   // read the Settings from the ConGIS.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'ConGIS.ini');
   dummy:=MainWnd.SourceDocument.Name;
   dummy:=ChangeFileExt(dummy, '.wgi');
   if FileExists(dummy) then
   begin
      MainWnd.AXImpExpDbc.ExpDbMode:=5; // Idb-Database
      MainWnd.AXImpExpDbc.ExpDatabase:='';
      MainWnd.AXImpExpDbc.ExpTable:='';
   end
   else
   begin
      dummy:=inifile.ReadString('E00EXP','ExpDbMode','0');
      val(dummy, intval, count);
      if intval <> 5 then // Idb-Database
      begin
         MainWnd.AXImpExpDbc.ExpDbMode:=intval;
         dummy:=inifile.ReadString('E00EXP','ExpDatabase','');
         MainWnd.AXImpExpDbc.ExpDatabase:=dummy;
         dummy:=inifile.ReadString('E00EXP','ExpTable','');
         MainWnd.AXImpExpDbc.ExpTable:=dummy;
      end;
   end;
   SrcDbLabel.Caption:=MainWnd.AXImpExpDbc.ExpDbInfo;
   dummy:=inifile.ReadString('E00EXP','LastUsedPath','');
   if dummy = '' then
      GetDir(0,dummy);
   FileSelectionPanel.Directory:=dummy;
   inifile.Destroy;
   Self.Repaint;
end;

// procedure is used to setup the SelE00Wnd for batch mode
procedure TSelE00Wnd.SetupForBatch;
var Layername:string;
    layercnt:integer;
    ALayer:Variant;
begin
   if FirstActivate then
   begin
      // fill layerlist
      LayerLB.Items.Clear;
      for layercnt:=0 to MainWnd.SourceDocument.Layers.Count-1 do
      begin
         ALayer:=MainWnd.SourceDocument.Layers.Items[layercnt];
         Layername:=ALayer.Layername;
         LayerLb.Items.Add(Layername);
      end;
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
   end;
end;

procedure TSelE00Wnd.SetExpSettings(ProjectDir:string; ExpMode:integer);
begin
   DestinationE00Directory:=ProjectDir;
   if (copy(DestinationE00Directory,length(DestinationE00Directory),1) <> '\') then
      DestinationE00Directory:=DestinationE00Directory+'\';
   if ExpMode = 2 then // export only selected objects
      SelectedCB.Checked:=true;
   if ExpMode = 1 then // export only selected layers
      LayerCB.Checked:=true;
end;

procedure TSelE00Wnd.AddExpLayer(ExpLayer:string);
var i:integer;
begin
   // find the Layername in the list and select the item
   for i:=0 to LayerLB.Items.Count-1 do
   begin
      if (LayerLB.Items[i] = ExpLayer) then
      begin
         LayerLB.Selected[i]:=true;
      end;
   end;
end;

procedure TSelE00Wnd.LayerLBClick(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSelE00Wnd.DbBtnClick(Sender: TObject);
begin
   MainWnd.AxImpExpDbc.SetupByDialog(Self.Handle,5,5);
   PageControl.Enabled:=FALSE;
   GeoBtn.Enabled:=FALSE;
   DbBtn.Enabled:=FALSE;
   OkBtnMode:=OkBtn.Enabled;
   OkBtn.Enabled:=FALSE;
   CancelBtn.Enabled:=FALSE;
end;

procedure TSelE00Wnd.FileSelectionPanelDirectoryChanged(Sender: TObject);
begin
   DestinationE00Directory:=FileSelectionPanel.Directory;
   if DestinationE00Directory <> '' then
   begin
      if DestinationE00Directory[length(DestinationE00Directory)] <> '\' then
         DestinationE00Directory:=DestinationE00Directory+'\';
   end;
   CheckWindow;
end;

end.
