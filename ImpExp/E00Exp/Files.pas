unit Files;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Math, Struct, StrFile;

const
       Col_Int      = 0;
       Col_SmallInt = 1;
       Col_Float    = 2;
       Col_Date     = 3;
       Col_String   = 4;

type

ClassFile = class
   public
      { public Definitionen }
      constructor Create;
      destructor Destroy;
      function GetMaxPass1(Dateiname:string):longint;
      function GetMaxPass2:longint;
      procedure SetE00Directory(Verzeichnis:string);
      procedure CloseAllFiles;
      procedure ResetLayerZeiger;
      function  GetAktLayerName:string;
      function  WriteAktLayer:longint;
      procedure WriteE00Line(Text:string);
      function  GetFloatStr(wert:double):string;
      procedure New_Point(X:double;Y:double;Layername:string);
      procedure New_Area(Layername:string);
      procedure InsertPointToArea(X:double;Y:double);
      procedure New_Line(Layername:string);
      procedure InsertPointToLine(X:double;Y:double);
      procedure New_Layer(Layername:string);
      procedure ValDatabase(Index:integer; ObjType:integer; Layername:string);
   private
      { private Definitionen }
      procedure CreateDir(var Name:string);
      procedure MakeCorrectName(var name:string);
end;
implementation
uses MainDlg, ActiveX;

var  E00Datei  :Textfile;
     PolygonObj:ClassPolygon;
     LinienObj :ClassLinie;
     PunktObj  :ClassPunkt;
     LayerObj  :ClassLayer;
     DatenbankObj:ClassDatenbank;
     E00Directory:string;

constructor ClassFile.Create;
begin
   PolygonObj:=ClassPolygon.Create;
   LinienObj:=ClassLinie.Create;
   PunktObj:=ClassPunkt.Create;
   LayerObj:=ClassLayer.Create;
   DatenbankObj:=ClassDatenbank.Create;
   DatenbankObj.Init;
end;

destructor Classfile.Destroy;
begin
   PolygonObj.Destroy;
   LinienObj.Destroy;
   PunktObj.Destroy;
   LayerObj.Destroy;
   DatenbankObj.Destroy;
end;

{*******************************************************************************
* PROZEDUR : SetE00Directory                                                   *
********************************************************************************
* Prozedur dient zum Setzen des E00 - Verzeichnises.                           *
*                                                                              *
* PARAMETER: Verzeichnis -> E00 - Verzeichnis.                                 *
*                                                                              *
********************************************************************************}
procedure ClassFile.SetE00Directory(Verzeichnis:string);
begin
   E00Directory:=Verzeichnis;
end;

{*******************************************************************************
* PROZEDUR : CreateDir                                                         *
********************************************************************************
* Prozedur dient zum Erzeugen eines Unterverzeichnisses                        *
*                                                                              *
* PARAMETER: Name -> Name des Verzeichnisses das erstellt werden soll.         *
*                                                                              *
********************************************************************************}
procedure ClassFile.CreateDir(var Name:string);
begin
   {$i-}
   MKDir(E00Directory+'\'+Name);
   ChDir(E00Directory+'\'+Name);
   {$i+}
end;


{*******************************************************************************
* FUNKTION : WriteAktLayer                                                     *
********************************************************************************
* Prozedur dient zum Erzeugen der Shapefiles f�r den Aktuellen Layer.          *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Layer (hier immer 1)                   *
********************************************************************************}
function ClassFile.WriteAktLayer:longint;
var Name,DirName:string;
    hpchar:pchar;
    Coverage_id:longint;
    HasData:boolean;
    CorrectLayername:string;
begin
   HasData:=false;
   if LayerObj.Checklines then { Kontrolle, ob auf diesem Layer Linien vorhanden sind }
   begin
      HasData:=true;
      { Erzeugen eines neuen Unterverzeichnisses mit dem Layernamen }
      DirName:='lines';
      CreateDir(DirName);
      Name:=LayerObj.GetaktLayerName;
      CorrectLayername:=Name;
      MakeCorrectName(CorrectLayername);
      hpchar:=stralloc(255); Name:=CorrectLayername + '.e00';
      strpcopy(hpchar,Name); if Fileexists(Name) then DeleteFile(hpchar);
      {$i-} Assignfile(E00Datei,Name); Rewrite(E00Datei); {$i+}
      { Schreiben des E00 - Kopfes }
      WriteE00Line('EXP  0');
      WriteE00Line('ARC  2');
      { Nun wird die Graphische Information in die E00-Datei geschrieben }
      coverage_id:=1; LinienObj.WriteLinesGraphic(Coverage_id);
      WriteE00Line('        -1         0         0         0         0         0         0');
      { Schreiben der Attributinformation in die E00-Datei }
      WriteE00Line('IFO  2');
      LayerObj.WriteBoundaries('LINE'); { Schreiben der Boundary-Daten }
      LinienObj.WriteLinesAttributes; { Schreiben der Attributdaten f�r Linien }
      LayerObj.WriteTIC('LINE'); { Schreiben der Passpunktdaten }
      WriteE00Line('EOI');
      WriteE00Line('EOS');
      {$i-} Closefile(E00Datei); {$i+}
   end;

   if LayerObj.CheckAreas then { Schreiben der Fl�cheninformation }
   begin
      HasData:=true;
      { Erzeugen eines neuen Unterverzeichnisses mit dem Layernamen }
      DirName:='areas';
      CreateDir(DirName);
      Name:=LayerObj.GetaktLayerName; hpchar:=stralloc(255);
      CorrectLayername:=Name;
      MakeCorrectName(CorrectLayername);
      Name:=CorrectLayername + '.e00';
      strpcopy(hpchar,Name); if Fileexists(Name) then DeleteFile(hpchar);
      {$i-} Assignfile(E00Datei,Name); Rewrite(E00Datei); {$i+}

      WriteE00Line('EXP  0');
      WriteE00Line('ARC  2');
      { Nun wird die Graphische Information in die E00-Datei geschrieben }
      Coverage_id:=1;PolygonObj.WritePolygonsArcs(Coverage_id);
      WriteE00Line('        -1         0         0         0         0         0         0');
      Coverage_id:=1;PolygonObj.WritePolygonsPALS(Coverage_id);
      { Schreiben der Attributinformation in die E00-Datei }
      WriteE00Line('IFO  2');
      LayerObj.WriteBoundaries('POLY'); { Schreiben der Boundary-Daten }
      PolygonObj.WritePolygonAttributes; { Schreiben der Attributdaten f�r Fl�chen }
      LayerObj.WriteTIC('POLY'); { Schreiben der Passpunktdaten }
      WriteE00Line('EOI');
      WriteE00Line('EOS');
      {$i-} Closefile(E00Datei); {$i+}
   end;

   if LayerObj.CheckPoints then { Schreiben der Punktdaten }
   begin
      HasData:=true;
      { Erzeugen eines neuen Unterverzeichnisses mit dem Layernamen }
      DirName:='points';
      CreateDir(DirName);
      Name:=LayerObj.GetaktLayerName; hpchar:=stralloc(255);
      CorrectLayername:=Name;
      MakeCorrectName(CorrectLayername);
      Name:=CorrectLayername + '.e00';
      strpcopy(hpchar,Name); if Fileexists(Name) then DeleteFile(hpchar);
      {$i-} Assignfile(E00Datei,Name); Rewrite(E00Datei); {$i+}
      WriteE00Line('EXP  0');
      PunktObj.WritePointsGraphic(Coverage_id);
      { Schreiben der Attributinformation in die E00-Datei }
      WriteE00Line('IFO  2');
      LayerObj.WriteBoundaries('POINT'); { Schreiben der Boundary-Daten }
      PunktObj.WritePointsAttributes; { Schreiben der Attributdaten f�r Punkte }
      LayerObj.WriteTIC('POINT'); { Schreiben der Passpunktdaten }
      WriteE00Line('EOI');
      WriteE00Line('EOS');
      {$i-} Closefile(E00Datei); {$i+}
   end;

   if not HasData then
   begin
      // no point / poly / line object exists for this layer
      MainWnd.DisplayMessage(StrfileObj.Assign(41)); // Warning: No point/polyline/polygon information exists for this layer
      MainWnd.DisplayMessage(StrfileObj.Assign(42)); //          No E00 file will be created.
   end;
   LayerObj.NextLayer;
   result:=1;
end;

procedure ClassFile.MakeCorrectName(var name:string);
var pdummy:PCHAR;
    pdummy2:PCHAR;
    i,pos:integer;
    res:string;
    canadd:boolean;
begin
   //function is used to set the layername that it can be set as database or controlname
   pdummy:=stralloc(length(name)+1); strpcopy(pdummy,name);
   pdummy2:=stralloc(length(name)+1); strpcopy(pdummy2,name);
   pos:=0;
   for i:=0 to strlen(pdummy)-1 do
   begin
      canadd:=true;
      if (pdummy[i] = '\') then canadd:=false;
      if (pdummy[i] = '/') then canadd:=false;
      if (pdummy[i] = ':') then canadd:=false;
      if (pdummy[i] = '*') then canadd:=false;
      if (pdummy[i] = '?') then canadd:=false;
      if (pdummy[i] = '"') then canadd:=false;
      if (pdummy[i] = '<') then canadd:=false;
      if (pdummy[i] = '>') then canadd:=false;
      if (pdummy[i] = '|') then canadd:=false;
      if (canadd) then
      begin
         pdummy2[pos]:=pdummy[i];
         pos:=pos+1;
      end;
   end;
   name:=strpas(pdummy2);
   name:=copy(name,1,pos);
   strdispose(pdummy); strdispose(pdummy2);
end;



{*******************************************************************************
* PROZEDUR : WriteE00Line                                                      *
********************************************************************************
* Prozedur dient zum Schreiben einer Zeile in die E00-Datei                    *
*                                                                              *
* PARAMETER: Text -> Text der in die E00-Datei geschrieben werden soll.        *
*                                                                              *
********************************************************************************}
procedure Classfile.WriteE00Line(Text:string);
begin
   {$i-} Writeln(E00Datei,Text); {$i+}
end;

{*******************************************************************************
* PROZEDUR : CloseallFiles                                                     *
********************************************************************************
* Prozedur dient zum Schliessen aller Files.                                   *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassFile.CloseallFiles;
begin
   MainWnd.AXImpExpDbc.CloseExpTables;
end;

procedure ClassFile.ResetLayerZeiger;
begin
   LayerObj.ResetLayerzeiger;
end;

{*******************************************************************************
* FUNKTION : GetMaxPass1                                                       *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Bytes des       *
* Shapefiles.                                                                  *
*                                                                              *
* PARAMETER: Dateiname -> Dateiname von der die Bytegr��e ermittelt werden soll*
*                                                                              *
* R�ckgabewert: Anzahl der zu Bearbeitenden Bytes.                             *
********************************************************************************}
function ClassFile.GetMaxPass1(Dateiname:string):longint;
var Datei:file of byte;
begin
   AssignFile(Datei, Dateiname);
   Reset(Datei);
   Result:=FileSize(Datei);
   Closefile(Datei);
   DatenbankObj.init; { Die Datenbankzeiger werden initialisiert }
end;

{*******************************************************************************
* FUNKTION : GetMaxPass2                                                       *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu schreibeinden Layer           *
*                                                                              *
* R�ckgabewert: Anzahl der zu Bearbeitenden Bytes.                             *
********************************************************************************}
function ClassFile.GetMaxPass2:longint;
begin
   result:=LayerObj.GetAnz;
end;

{*******************************************************************************
* FUNKTION : GetAktLayer                                                       *
********************************************************************************
* Funktion dient zum Ermitteln welcher Layer gerade geschrieben wird.          *
*                                                                              *
* R�ckgabewert: Name des Layers der gerade geschrieben wird.                   *
********************************************************************************}
function ClassFile.GetAktLayerName:string;
begin
   Result:=LayerObj.GetAktLayerName;
end;

{*******************************************************************************
* FUNKTION : GetFloatStr                                                       *
********************************************************************************
* Prozedur dient zum erhalten eines Gleitkommastrings aus einem Floatwert      *
*                                                                              *
* PARAMETER: Wert  -> Floatwert, der umzuwandeln ist.                          *
*                                                                              *
* R�CKGABEWERT: Zeichenkette des Floatwerts im Gleitkommaformat.               *
********************************************************************************}
function ClassFile.GetFloatStr(wert:double):string;
var Erg,Teil1,Teil2:string;
    Kommapos:integer;
begin
   Str(Wert:10:9,Erg);

   { Nun muss das Kommazeichen an die Zweite Position im String geschoben werden }
   Kommapos:=Pos('.',Erg); Delete(Erg,Kommapos,1);
   if Wert > 0 then
      begin Teil1:=copy(Erg,1,1); Teil2:=copy(Erg,2,length(Erg)); Erg:=Teil1 + '.' + Teil2; end;
   if Wert < 0 then
      begin Kommapos:=Kommapos - 1; Teil1:=copy(Erg,1,2); Teil2:=copy(Erg,3,length(Erg)); Erg:=Teil1 + '.' + Teil2; end;

   Erg:=copy(Erg,1,9);
   {Nun muss noch die Potzenz hinzugef�gt werden }
   Kommapos:=Kommapos - 2; Teil1:='E+';
   Teil2:=inttostr(Kommapos);
   if length(Teil2) < 2 then Teil2:='0'+Teil2;
   Erg:=Erg+Teil1+Teil2;
   if length(Erg) < 14 then repeat Erg:=' '+Erg; until length(Erg)>=14;
   if Wert = 0 then Erg:=' 0.0000000E+00';

   Result:=Erg;
end;

procedure ClassFile.New_Layer(Layername:string);
var Columnname:widestring;
    Columntype:TOleEnum;
    Columnlength:integer;
begin
   LayerObj.Neu(Layername);
   // read database column for this layer
   MainWnd.AXImpExpDbc.CheckAndCreateExpTable(Layername);
   while MainWnd.AXImpExpDbc.GetExportColumn(Layername, Columnname, Columntype, Columnlength) do
   begin
      if ColumnType = Col_Float then ColumnType:=Col_Float
      else if ColumnType = Col_Int then ColumnType:=Col_Int
      else if ColumnType = Col_String then ColumnType:=Col_String
      else begin ColumnType:=Col_String; Columnlength:=10; end; // all other columntypes will be mapped to char
      DatenbankObj.Insertcolumn(Columnname,Columnlength,Columntype,'',0,0,true);
      LayerObj.InsertDBColumn(Columnname);
   end;
   MainWnd.AXImpExpDbc.OpenExpTable(Layername);
end;

procedure Classfile.ValDatabase(Index:integer; ObjType:integer; Layername:string);
var column, info:widestring;
begin
   // function is used to get database information for object
   if not MainWnd.AXImpExpDbc.DoExportData(Layername,Index) then
      exit; // if no databaseentry has been found for this object exit

   while MainWnd.AXImpExpDbc.GetExportData(Layername, column, info) do
   begin
      if      (ObjType = ot_Pixel  ) then begin PunktObj.InsertAttrib(Column,Info);   end
      else if (ObjType = ot_Cpoly  ) then begin PolygonObj.InsertAttrib(Column,Info); end
      else if (ObjType = ot_poly   ) then begin LinienObj.InsertAttrib(Column,Info);  end;
   end;
end;


procedure ClassFile.New_Point(X:double;Y:double;Layername:string);
begin
   PunktObj.Neu(Layername,X,Y);
end;

procedure ClassFile.New_Area(Layername:string);
begin
   PolygonObj.Neu(Layername);
end;

procedure ClassFile.New_Line(Layername:string);
begin
   LinienObj.Neu(Layername);
end;

procedure ClassFile.InsertPointToArea(X:double;Y:double);
begin
   PolygonObj.InsertPoint(X,Y);
end;

procedure ClassFile.InsertPointToLine(X:double;Y:double);
begin
   LinienObj.InsertPoint(X,Y);
end;



end.
