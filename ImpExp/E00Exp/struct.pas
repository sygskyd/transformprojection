unit struct;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, db;

type
  { Hier k�nnen Strukturen definiert werden }

   BoundingBox = record
      X_min :double;
      Y_min :double;
      X_max :double;
      Y_max :double;
   end;

   p_Punkt_record=^Punkt_record;
   Punkt_record = record
       X:double;
       Y:double;
       next :p_Punkt_record;
   end;

   p_Datenbank_record =^Datenbank_record;
   Datenbank_record = record
      Bezeichnung : string;
      Laenge      : integer;
      Typ         : integer;
      Inhalt      : string;
      von         : integer;
      bis         : integer;
      Art         : boolean;
      next        : p_Datenbank_record;
   end;

   p_Polygon_record =^Polygon_record;
   Polygon_record = record
      Numarcs  :integer;
      Punktanz :integer;
      Box      :Boundingbox;
      Punkte   :p_Punkt_record;
      DBInfo   :p_Datenbank_record;
      next     :p_Polygon_record;
   end;

   p_PunktObj_record = ^PunktObj_record;
   PunktObj_record = record
      X :double;
      Y :double;
      DBInfo:p_Datenbank_record;
      next :p_Punktobj_record;
   end;

   p_Layer_record = ^Layer_record;
   Layer_record = record
      Layername:string;
      Punkte  :p_PunktObj_record;
      PunkteBox:Boundingbox;
      Punktesize:longint;
      Linien  :p_Polygon_record;
      LinienBox:Boundingbox;
      Liniensize:longint;
      LinienArcs:longint;
      Flaechen:p_Polygon_record;
      FlaechenBox:Boundingbox;
      Flaechensize:longint;
      FlaechenArcs:longint;
      DBColumns     :p_Datenbank_record;
      next    :p_Layer_record;
   end;

ClassPolygon = class
   public
      { public Definitionen }
      constructor Create;
      procedure Neu(Layername:string);
      procedure InsertPoint(X:double;Y:double);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      procedure WritePolygonsArcs(var Coverage_id:longint);
      procedure WritePolygonsPals(var Coverage_id:longint);
      procedure WritePolygonAttributes;
   private
      { private Definitionen }
      function GetArea(Zeiger:p_Punkt_record):double;
      function GetPerimeter(Zeiger:p_Punkt_record):double;

end;

ClassLinie = class
   public
      { public Definitionen }
      constructor Create;
      procedure Neu(Layername:string);
      procedure InsertPoint(X:double;Y:double);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      procedure WriteLinesGraphic(var Coverage_id:longint);
      procedure WriteLinesAttributes;
   private
      { private Definitionen }
end;



ClassDatenbank = class
   public
      { public Definitionen }
      constructor Create;
      procedure Neu(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string; von:integer; bis:integer;Art:boolean; var Anfang:p_datenbank_record);
      function  Checkexists(Eintrag:string):boolean;
      function  GetLength(Bezeichnung:string):integer;
      function  GetType(Bezeichnung:string):integer;
      function  GetItem(Bezeichnung:string):string;
      procedure Getvonbis(Bezeichnung:string;var von:integer; var bis:integer);
      procedure Insertcolumn(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string;von:integer;bis:integer;Art:boolean);
      procedure ResetColumn;
      procedure Init;
   private
      { private Definitionen }
end;

ClassPunkt = class
   public
      { public Definitionen }
      constructor Create;
      procedure Neu(Layername:string; X:double; Y:double);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      procedure WritePointsGraphic(var Coverage_id:longint);
      procedure WritePointsAttributes;
   private
      { private Definitionen }
end;

ClassLayer = class
   public
      { public Definitionen }
      constructor Create;
      destructor Destroy;
      procedure Neu(Layername:string);
      function Checkexists(Layername:string):boolean;
      procedure ResetLayerZeiger;
      function GetAnz:longint;
      function GetAktLayername:string;
      function CheckAreas:boolean;
      function CheckPoints:boolean;
      function CheckLines:boolean;
      procedure WriteBoundaries(Art:string);
      procedure WriteTIC(Art:string);
      procedure NextLayer;
      procedure InsertDBColumn(Spaltenname:string);
   private
      { private Definitionen }
      ColumnAnzahl:integer;
      DBObj:ClassDatenbank;
end;

implementation
uses files,maindlg;

var Akt_Punkt  :p_Punktobj_record;
    Akt_Linie  :p_Polygon_record;
    Akt_Flaeche:p_Polygon_record;
    Datenbank_Anfang, Akt_Datenbank:p_Datenbank_record;
    Layer_Anfang, Akt_Layer:p_Layer_record;
    Akt_ColumnInfo:p_Datenbank_record;
    FileObj     :ClassFile;
    PunktObj    :ClassPunkt;
    PolygonObj  :ClassPolygon;
    LinienObj   :ClassLinie;
    DatenbankObj:ClassDatenbank;
    LayerObj    :ClassLayer;

constructor ClassPolygon.Create;
begin
end;

constructor ClassLayer.Create;
begin
   DBObj:=ClassDatenbank.Create;
   ColumnAnzahl:=0;
end;

destructor ClassLayer.Destroy;
begin
   DBObj.Destroy;
end;

constructor ClassLinie.Create;
begin
end;

constructor ClassDatenbank.Create;
begin
end;

procedure ClassDatenbank.Init;
begin
   Akt_Flaeche:=nil;
   Akt_Punkt:=nil;
   Akt_Linie:=nil;
   Datenbank_Anfang:=nil;
   Akt_Datenbank:=nil;
   Layer_Anfang:=nil;
   Akt_Layer:=nil;
end;

constructor ClassPunkt.Create;
begin
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.Checkexists                                        *
********************************************************************************
* Funktion �berpr�ft, ob der entsprechende Spalteneintrag bereits in der       *
* Datenbankliste ist.                                                          *
*                                                                              *
* R�ckgabewert: TRUE  -> Eintrag ist bereits in Liste.                         *
*               FALSE -> Eintrag ist noch nicht in Liste.                      *
*                                                                              *
********************************************************************************}
function ClassDatenbank.Checkexists(Eintrag:string):boolean;
var Zeiger:p_Datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   if Zeiger = nil then begin Result:=false; exit; end;
   repeat
      if Zeiger^.Bezeichnung = Eintrag then gefunden:= true;
      Zeiger:=Zeiger^.next;
   until (Zeiger = nil) or (gefunden = true);
   Result:=gefunden;
end;

{*******************************************************************************
* PROZEDUR : ClassPolygon.InsertAttrib                                         *
********************************************************************************
* Prozedur f�gt einen Datenbankinformationseintrag zu einem Polygon ein.       *
*                                                                              *
* PARAMETER: Spaltenname   -> Name der Spalte die eingef�gt werden soll.       *
*            Spalteninhalt -> Inhalt der Datenbankspalte.                      *
*            Objekttyp     -> Objekttyp 1 ist Linie, Objekttyp 2 ist Fl�che    *
*                                                                              *
********************************************************************************}
procedure ClassPolygon.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,0,0,TRUE,Akt_Flaeche^.DBInfo);
end;

{*******************************************************************************
* PROZEDUR : ClassLinie.InsertAttrib                                           *
********************************************************************************
* Prozedur f�gt einen Datenbankinformationseintrag zu einem Polygon ein.       *
*                                                                              *
* PARAMETER: Spaltenname   -> Name der Spalte die eingef�gt werden soll.       *
*            Spalteninhalt -> Inhalt der Datenbankspalte.                      *
*                                                                              *
********************************************************************************}
procedure ClassLinie.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,0,0,TRUE,Akt_Linie^.DBInfo);
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.ResetLayerZeiger                                       *
********************************************************************************
* Prozedur setzt den Akt_Layer zeiger auf den Anfang                           *
*                                                                              *
********************************************************************************}
procedure ClassLayer.ResetLayerZeiger;
begin
   Akt_layer:=Layer_Anfang;
   Akt_ColumnInfo:=Akt_layer^.DBColumns;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.WriteBoundaries                                        *
********************************************************************************
* Prozedur schreibt den Boundaries-Datensatz in die E00-Datei.                 *
*                                                                              *
********************************************************************************}
procedure ClassLayer.WriteBoundaries(Art:string);
var Zeile,hZeile:string;
begin
   hZeile:=Akt_layer^.Layername; if length(hZeile) > 13 then hZeile:=copy(hZeile,1,13);
   hZeile:=hZeile +'.BND'; repeat hZeile:=hZeile+' '; until length(hZeile) = 32;
   { Schreiben des Boundaries Namen }
   Zeile:=hZeile + 'XX   4   4  16         1';
   FileObj.WriteE00Line(Zeile);
   FileObj.WriteE00Line('XMIN              4-1   14-1  12 3 60-1  -1  -1-1                   1');
   FileObj.WriteE00Line('YMIN              4-1   54-1  12 3 60-1  -1  -1-1                   2');
   FileObj.WriteE00Line('XMAX              4-1   94-1  12 3 60-1  -1  -1-1                   3');
   FileObj.WriteE00Line('YMAX              4-1  134-1  12 3 60-1  -1  -1-1                   4');

   if Art = 'LINE' then
      FileObj.WriteE00Line(FileObj.GetFloatStr(Akt_Layer^.LinienBox.X_min)+FileObj.GetFloatStr(Akt_Layer^.LinienBox.Y_min)+
                           FileObj.GetFloatStr(Akt_Layer^.LinienBox.X_max)+FileObj.GetFloatStr(Akt_Layer^.LinienBox.Y_max));

   if Art = 'POLY' then
      FileObj.WriteE00Line(FileObj.GetFloatStr(Akt_Layer^.FlaechenBox.X_min)+FileObj.GetFloatStr(Akt_Layer^.FlaechenBox.Y_min)+
                           FileObj.GetFloatStr(Akt_Layer^.FlaechenBox.X_max)+FileObj.GetFloatStr(Akt_Layer^.FlaechenBox.Y_max));

   if Art = 'POINT' then
      FileObj.WriteE00Line(FileObj.GetFloatStr(Akt_Layer^.PunkteBox.X_min)+FileObj.GetFloatStr(Akt_Layer^.PunkteBox.Y_min)+
                           FileObj.GetFloatStr(Akt_Layer^.PunkteBox.X_max)+FileObj.GetFloatStr(Akt_Layer^.PunkteBox.Y_max));
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.WriteTIC                                               *
********************************************************************************
* Prozedur schreibt den Passpunkt Datensatz in die E00-Datei.                  *
*                                                                              *
********************************************************************************}
procedure ClassLayer.WriteTIC(Art:string);
var Zeile,hZeile:string;
begin
   hZeile:=Akt_layer^.Layername; if length(hZeile) > 13 then hZeile:=copy(hZeile,1,13);
   hZeile:=hZeile +'.TIC'; repeat hZeile:=hZeile+' '; until length(hZeile) = 32;
   { Schreiben des Boundaries Namen }
   Zeile:=hZeile + 'XX   3   3  12         4';
   FileObj.WriteE00Line(Zeile);
   FileObj.WriteE00Line('IDTIC             4-1   14-1   5-1 50-1  -1  -1-1                   1');
   FileObj.WriteE00Line('XTIC              4-1   54-1  12 3 60-1  -1  -1-1                   2');
   FileObj.WriteE00Line('YTIC              4-1   94-1  12 3 60-1  -1  -1-1                   3');

   if Art = 'LINE' then
   begin
      FileObj.WriteE00Line('          1'+FileObj.GetFloatStr(Akt_Layer^.LinienBox.X_Min)+FileObj.GetFloatStr(akt_Layer^.LinienBox.Y_Min));
      FileObj.WriteE00Line('          2'+FileObj.GetFloatStr(Akt_Layer^.LinienBox.X_Max)+FileObj.GetFloatStr(akt_Layer^.LinienBox.Y_Max));
      FileObj.WriteE00Line('          3'+FileObj.GetFloatStr(Akt_Layer^.LinienBox.X_Max)+FileObj.GetFloatStr(akt_Layer^.LinienBox.Y_Min));
      FileObj.WriteE00Line('          4'+FileObj.GetFloatStr(Akt_Layer^.LinienBox.X_Min)+FileObj.GetFloatStr(akt_Layer^.LinienBox.Y_Max));
   end;

   if Art = 'POLY' then
   begin
      FileObj.WriteE00Line('          1'+FileObj.GetFloatStr(Akt_Layer^.FlaechenBox.X_Min)+FileObj.GetFloatStr(akt_Layer^.FlaechenBox.Y_Min));
      FileObj.WriteE00Line('          2'+FileObj.GetFloatStr(Akt_Layer^.FlaechenBox.X_Max)+FileObj.GetFloatStr(akt_Layer^.FlaechenBox.Y_Max));
      FileObj.WriteE00Line('          3'+FileObj.GetFloatStr(Akt_Layer^.FlaechenBox.X_Max)+FileObj.GetFloatStr(akt_Layer^.FlaechenBox.Y_Min));
      FileObj.WriteE00Line('          4'+FileObj.GetFloatStr(Akt_Layer^.FlaechenBox.X_Min)+FileObj.GetFloatStr(akt_Layer^.FlaechenBox.Y_Max));
   end;

   if Art = 'POINT' then
   begin
      FileObj.WriteE00Line('          1'+FileObj.GetFloatStr(Akt_Layer^.PunkteBox.X_Min)+FileObj.GetFloatStr(akt_Layer^.PunkteBox.Y_Min));
      FileObj.WriteE00Line('          2'+FileObj.GetFloatStr(Akt_Layer^.PunkteBox.X_Max)+FileObj.GetFloatStr(akt_Layer^.PunkteBox.Y_Max));
      FileObj.WriteE00Line('          3'+FileObj.GetFloatStr(Akt_Layer^.PunkteBox.X_Max)+FileObj.GetFloatStr(akt_Layer^.PunkteBox.Y_Min));
      FileObj.WriteE00Line('          4'+FileObj.GetFloatStr(Akt_Layer^.PunkteBox.X_Min)+FileObj.GetFloatStr(akt_Layer^.PunkteBox.Y_Max));
   end;
end;


{*******************************************************************************
* FUNKTION : ClassLayer.GetAktLayerName                                        *
********************************************************************************
* Funktion ermittelt den Namen des Akt_Layer                                   *
*                                                                              *
********************************************************************************}
function ClassLayer.GetAktLayerName:string;
var hpchar:pchar;
    i:integer;
begin
   { Es wird �berpr�ft, ob sich nicht irgendeine Sonderzeichen im Namen befinden, die nicht erlaubt sind }
   hpchar:=stralloc(length(Akt_layer^.Layername)+1); strpcopy(hpchar,Akt_layer^.Layername);
   for i:=1 to length(Akt_layer^.Layername) do
   begin
      if hpchar[i] = CHR(148) then hpchar[i]:='�';
      if hpchar[i] = CHR(153) then hpchar[i]:='�';
      if hpchar[i] = CHR(132) then hpchar[i]:='�';
      if hpchar[i] = CHR(142) then hpchar[i]:='�';
      if hpchar[i] = CHR(129) then hpchar[i]:='�';
      if hpchar[i] = CHR(154) then hpchar[i]:='�';
      if hpchar[i] = CHR(225) then hpchar[i]:='�';
      if hpchar[i] = '/' then hpchar[i]:='_';
      if hpchar[i] = '\' then hpchar[i]:='_';
   end;
   Akt_Layer^.Layername:=strpas(hpchar);
   if pos('"',Akt_Layer^.Layername) <> 0 then repeat delete(Akt_Layer^.Layername,Pos('"',Akt_Layer^.Layername),1); until Pos('"',Akt_Layer^.Layername) = 0;
   strdispose(hpchar);
   Result:=Akt_layer^.Layername;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.CheckPoints                                            *
********************************************************************************
* Funktion �berpr�ft ob auf dem Akt_Layer Punkte vorhanden sind.               *
*                                                                              *
* R�CKGABEWERT: TRUE  -> es sind Punkte vorhanden                              *
*               FALSE -> es sind keine Punkte vorhanden                        *
*                                                                              *
********************************************************************************}
function ClassLayer.CheckPoints:boolean;
begin
   if Akt_Layer^.Punkte <> nil then result:=true else result:=false;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.CheckLines                                             *
********************************************************************************
* Funktion �berpr�ft ob auf dem Akt_Layer Linien vorhanden sind.               *
*                                                                              *
* R�CKGABEWERT: TRUE  -> es sind Linien vorhanden                              *
*               FALSE -> es sind keine Linien vorhanden                        *
*                                                                              *
********************************************************************************}
function ClassLayer.CheckLines:boolean;
begin
   if Akt_Layer^.Linien <> nil then result:=true else result:=false;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.CheckAreas                                             *
********************************************************************************
* Funktion �berpr�ft ob auf dem Akt_Layer Fl�chen vorhanden sind.              *
*                                                                              *
* R�CKGABEWERT: TRUE  -> es sind Fl�chen vorhanden                             *
*               FALSE -> es sind keine Fl�chen vorhanden                       *
*                                                                              *
********************************************************************************}
function ClassLayer.CheckAreas:boolean;
begin
   if Akt_Layer^.Flaechen <> nil then result:=true else result:=false;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.GetAnz                                                 *
********************************************************************************
* Funktion ermittelt die Anzahl der Layer die sich in der Liste befinden.      *
*                                                                              *
********************************************************************************}
function ClassLayer.GetAnz:longint;
var anz:longint;
    Zeiger:p_Layer_record;
begin
   anz:=0; zeiger:=Layer_Anfang;
   if zeiger <> nil then
   repeat
      Zeiger:=Zeiger^.next; anz:=anz + 1;
   until Zeiger = nil;
   result:=anz;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.nextlayer                                              *
********************************************************************************
* Prozedur setzt den Akt_Layer zeiger weiter.                                  *
*                                                                              *
********************************************************************************}
procedure ClassLayer.NextLayer;
begin
   Akt_Layer:=Akt_Layer^.next;
   if Akt_Layer <> nil then Akt_ColumnInfo:=Akt_layer^.DBColumns;
end;

{*******************************************************************************
* PROZEDUR : ClassPunkt.InsertAttrib                                           *
********************************************************************************
* Prozedur f�gt einen Datenbankinformationseintrag zum Akt_Punkt ein.          *
*                                                                              *
* PARAMETER: Spaltenname   -> Name der Spalte die eingef�gt werden soll.       *
*            Spalteninhalt -> Inhalt der Datenbankspalte.                      *
*                                                                              *
********************************************************************************}
procedure ClassPunkt.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,0,0,TRUE,Akt_Punkt^.DBInfo);
end;

{*******************************************************************************
* PROZEDUR : ClassPunkt.WritePointsAttributes                                  *
********************************************************************************
* Prozedur schreibt die Punktinformation in die E00-Datei.                     *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassPunkt.WritePointsAttributes;
var Zeiger:p_PunktObj_record;
    DBZeiger,AttDBZeiger:p_Datenbank_record;
    gefunden:boolean;
    DBAnz,DBlength,Startpos,count,Coverage_id,Spaltenbreite:integer;
    Zeile,hstr,hZeile:string;
    hdouble,Area,Perimeter:double;
    Laenge:integer;
    Typ:integer;
    DBLen:integer;
begin
   Zeiger:=Akt_Layer^.Punkte;
   { Schreiben der Datenbankspalteninformation }
   DBZeiger:=Akt_ColumnInfo; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   if DBZeiger <> nil then
   repeat
      if DBZeiger <> nil then if DBZeiger^.Art = TRUE then gefunden:=true;
      if gefunden = false then DBzeiger:=DBzeiger^.next;
   until (gefunden = TRUE) or (DBZeiger = nil);

   DBAnz:=0; DBlength:=16; { DBLength ist 16 weil AREA,PERIMETER,# und ID standartm��ig vorhanden sind }

   if DBzeiger <> nil then
   repeat
      Laenge:=DatenbankObj.GetLength(DBZeiger^.Bezeichnung);
      Typ:=DatenbankObj.GetType(DBZeiger^.Bezeichnung);
      { Suchen wie viele Datenbankspalten vorhanden sind und ermitteln der Spaltenl�nge }
      if Typ = Col_Float  then DBlength:=DBlength + 4;
      if Typ = Col_Int    then DBlength:=DBlength + 4;
      if Typ = Col_String then DBlength:=DBlength + Laenge;
      DBAnz:=DBAnz + 1;
      DBZeiger:=DBZeiger^.next;
   until DBzeiger = nil;

   DBAnz := DBAnz + 4; { Wegen der # und -ID Spalte, AREA und Surface, Spalte }

   hZeile:=Akt_Layer^.Layername; if length(Akt_Layer^.Layername) > 13 then hZeile:=copy(hZeile,1,13);
   hZeile:=hZeile + '.PAT'; repeat hZeile:=hZeile+' '; until length(hZeile) = 32;
   Zeile:=hZeile + 'XX';
   hZeile:=inttostr(DBAnz); if length(hZeile) < 4 then repeat hZeile:=' '+hZeile; until length(hZeile) = 4;
   Zeile:=Zeile + hZeile + hZeile;
   hZeile:=inttostr(DBlength); if length(hZeile) < 4 then repeat hZeile:=' '+hZeile; until length(hZeile) = 4;
   Zeile:=Zeile + hZeile;
   hZeile:=inttostr(Akt_Layer^.Punktesize); repeat hZeile:=' '+hZeile; until length(hZeile) = 10;  Zeile:=Zeile + hZeile;
   FileObj.WriteE00Line(Zeile);

   DBZeiger:=Akt_ColumnInfo; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   if DBZeiger <> nil then
   repeat
      if DBZeiger <> nil then if DBZeiger^.Art = TRUE then gefunden:=true;
      if gefunden = false then DBzeiger:=DBzeiger^.next;
   until (gefunden = TRUE) or (DBZeiger = nil);

   { Schreiben der AREA und Perimeter Spalte }
   Zeile:='AREA              4-1   14-1  12 3 60-1  -1  -1-1                   1';
   FileObj.WriteE00Line(Zeile);
   Zeile:='PERIMETER         4-1   54-1  12 3 60-1  -1  -1-1                   2';
   FileObj.WriteE00Line(Zeile);

   { Schreiben der # und -ID Spalte }
   hstr:=Akt_Layer^.Layername; if length(hstr) > 13 then hstr:=copy(hstr,1,13);
   hstr:=hstr + '#';
   if length(hstr) < 16 then repeat hstr:=hstr+' '; until length(hstr) = 16;
   Zeile:=hstr; Zeile:=Zeile + '  4-1   94-1   5-1 50-1  -1  -1-1                   3';
   FileObj.WriteE00Line (Zeile);
   hstr:=Akt_Layer^.Layername; if length(hstr) > 13 then hstr:=copy(hstr,1,13);
   hstr:=hstr + '-ID';
   if length(hstr) < 16 then repeat hstr:=hstr+' '; until length(hstr) = 16;
   Zeile:=hstr; Zeile:=Zeile + '  4-1  134-1   5-1 50-1  -1  -1-1                   4';
   FileObj.WriteE00Line (Zeile);

   Startpos:=17; DBAnz:=5;
   if DBZeiger <> nil then { Nun k�nnen die eigentlichen Spaltendefinitionen geschrieben werden }
   repeat
      Laenge:=DatenbankObj.GetLength(DBZeiger^.Bezeichnung);
      Typ:=DatenBankObj.GetType(DBZeiger^.Bezeichnung);
      { Schreiben des Spaltennamens }
      hstr:=DBZeiger^.Bezeichnung; if length(hstr) < 16 then repeat hstr:=hstr+' '; until length(hstr) = 16;
      Zeile:=hstr;

      { Schreiben des Spaltenl�ngenfelds }
      if Typ = Col_Float  then hstr:='4-1';
      if Typ = Col_Int    then hstr:='4-1';
      if Typ = Col_String then hstr:=inttostr(Laenge)+'-1';
      if length(hstr)<5 then repeat hstr:=' '+hstr; until length(hstr)=5;
      Zeile:=Zeile + hstr;

      { Schreiben der Spaltenstartspalte }
      hstr:=inttostr(Startpos) + '4-1';
      if length(hstr)<7 then repeat hstr:=' '+hstr; until length(hstr)=7;
      Zeile:=Zeile + hstr;

      { Ermitteln der neuen Startpos }
      if Typ = Col_Float  then Startpos:=Startpos + 4;
      if Typ = Col_Int    then Startpos:=Startpos + 4;
      if Typ = Col_String then Startpos:=Startpos + Laenge;

      { Ermitteln des output Formats }
      if Typ = Col_Float  then hstr:='12 3';
      if Typ = Col_Int    then hstr:='5-1';
      if Typ = Col_String then hstr:=inttostr(Laenge)+'-1';
      if length(hstr)<6 then repeat hstr:=' '+hstr; until length(hstr)=6;
      Zeile:=Zeile + hstr;

      { Ermitteln des Datentyps }
      if Typ = Col_Float  then Zeile:=Zeile + ' 60-1  -1  -1-1';
      if Typ = Col_Int    then Zeile:=Zeile + ' 50-1  -1  -1-1';
      if Typ = Col_String then Zeile:=Zeile + ' 20-1  -1  -1-1';

      { Ermitteln der Spaltennummer }
      hstr:=inttostr(DBAnz);
      if length(hstr)<20 then repeat hstr:=' '+hstr; until length(hstr)=20;
      Zeile:=Zeile + hstr;
      FileObj.WriteE00Line(Zeile);

      DBZeiger:=DBZeiger^.next; DBAnz:=DBAnz + 1;
   until DBZeiger = nil;
   Coverage_id:=1;

   { Schreiben der Attributinformation }
   repeat
      { Erhalten des Fl�cheninhalts und des Umfangs  }
      Area:=0; Perimeter:=0;
      hstr:=FileObj.GetFloatStr(Area);
      if length(hstr)< 14 then repeat hstr:=' '+hstr; until length(hstr) = 14;
      Zeile:=hstr;
      hstr:=FileObj.GetFloatStr(Perimeter);
      if length(hstr)< 14 then repeat hstr:=' '+hstr; until length(hstr) = 14;
      Zeile:=Zeile + hstr;

      { Erhalten des # und ID Eintrages }
      hstr:=inttostr(Coverage_id);
      if length(hstr)< 11 then repeat hstr:=' '+hstr; until length(hstr) = 11;
      Zeile:=Zeile + hstr; Zeile:=Zeile + hstr;

      spaltenbreite:=50;

      DBZeiger:=Akt_ColumnInfo; gefunden:=false;
      { Setzen des Zeigers an die Datenbankinformation }
      if DBZeiger <> nil then
      repeat
         if DBZeiger <> nil then if DBZeiger^.Art = TRUE then gefunden:=true;
         if gefunden = false then DBzeiger:=DBzeiger^.next;
      until (gefunden = TRUE) or (DBZeiger = nil);

      if DBZeiger <> nil then
      begin
         AttDBZeiger:=Zeiger^.DBInfo;
         repeat
            Laenge:=DatenbankObj.GetLength(DBZeiger^.Bezeichnung);
            Typ:=DatenbankObj.GetType(DBZeiger^.Bezeichnung);
            if AttDBZeiger = nil then hstr:=''
            else hstr:=AttDBZeiger^.Inhalt;
            if Typ = Col_Float  then begin val(hstr,hdouble,count); hstr:=FileObj.GetFloatStr(hdouble); SpaltenBreite:=SpaltenBreite + 14; end;
            if Typ = Col_Int    then begin if length(hstr) < 11 then repeat hstr:=' '+hstr; until length(hstr) = 11; SpaltenBreite:=SpaltenBreite + 11; end;
            if Typ = Col_String then begin if length(hstr) < Laenge then repeat hstr:= ' '+hstr; until length(hstr) = Laenge; SpaltenBreite:=SpaltenBreite + Laenge; end;

            { Die Zeilenl�nge eines Informationseintrags darf 80 Zeichen nicht �berschreiten, sonst muss neue Zeile begonnen werden }
            if SpaltenBreite > 80 then
            begin
               DBLen:=80 - length(Zeile);
               Zeile:=Zeile + copy(hstr,1,DBLen);
               FileObj.WriteE00Line(Zeile);
               Zeile:=copy(hstr,DBLen+1,length(hstr));
               SpaltenBreite:=length(Zeile);
            end
            else
               Zeile:=Zeile + hstr;
            if DBZeiger <> nil then DBZeiger:=DBZeiger^.next;
            if AttDBZeiger <> nil then AttDBZeiger:=AttDBZeiger^.next;
         until DBZeiger = nil;
      end;
      FileObj.WriteE00Line(Zeile);
      Zeiger:=Zeiger^.next; Coverage_id:=Coverage_id + 1;
   until Zeiger = nil;
end;

{*******************************************************************************
* PROZEDUR : ClassPunkt.WritePointsGraphic                                     *
********************************************************************************
* Prozedur schreibt die Punktinformation in die E00-Datei.                     *
*                                                                              *
* PARAMETER: Coverage_id -> Coverage_id mit der die Punktdaten beginnen.       *
*                                                                              *
********************************************************************************}
procedure ClassPunkt.WritePointsGraphic(var Coverage_id:longint);
var Zeiger :p_PunktObj_record;
    Zeile,hZeile:string;
begin
   Zeiger:=Akt_Layer^.Punkte;

   { Schreiben des Headers }
   FileObj.WriteE00Line('LAB  2'); Coverage_id:=1; Zeile:='';

   { Nun k�nnen die Punktdaten geschrieben werden }
   repeat
       hZeile:=inttostr(Coverage_id); repeat hZeile:=' '+hZeile; until length(hZeile) = 10;
       Zeile:=hZeile + '         0';
       Zeile:=Zeile + FileObj.GetFloatStr(Zeiger^.X)+FileObj.GetFloatStr(Zeiger^.Y);
       FileObj.WriteE00Line(Zeile);
       Zeile:=FileObj.GetFloatStr(Zeiger^.X)+FileObj.GetFloatStr(Zeiger^.Y)+FileObj.GetFloatStr(Zeiger^.X)+FileObj.GetFloatStr(Zeiger^.Y);
       FileObj.WriteE00Line(Zeile);
       Zeiger:=Zeiger^.next; Coverage_id:=Coverage_id + 1;
   until Zeiger = nil;
   FileObj.WriteE00Line('        -1         0 0.0000000E+00 0.0000000E+00');
end;

{*******************************************************************************
* PROZEDUR : ClassPolygon.WritePolygonsPals                                    *
********************************************************************************
* Prozedur schreibt die PAL-Objekte    des aktuellen Layers in die E00-Datei.  *
*                                                                              *
* PARAMETER: Coverage_id -> Coverage_id, mit der die PALs beginnen sollen.
*                                                                              *
********************************************************************************}
procedure ClassPolygon.WritePolygonsPals(var Coverage_id:longint);
var Zeiger:p_Polygon_record;
    Zeile,hZeile:string;
    worked:longint;
begin
   Zeiger:=Akt_Layer^.Flaechen;
   FileObj.WriteE00Line('PAL  2');

   { Zuerst wird der Null PAL, der aus allen ARC Z�gen besteht geschrieben }
   Zeile:=inttostr(Akt_Layer^.Flaechenarcs+1); repeat Zeile:=' '+Zeile; until length(Zeile) = 10;
   Zeile:=Zeile + FileObj.GetFloatStr(Akt_Layer^.FlaechenBox.X_min)+FileObj.GetFloatStr(Akt_Layer^.FlaechenBox.X_max);
   Zeile:=Zeile + FileObj.GetFloatStr(Akt_Layer^.FlaechenBox.Y_min)+FileObj.GetFloatStr(Akt_Layer^.FlaechenBox.Y_max);
   FileObj.WriteE00Line(Zeile);

   Zeile:='         0         0         0';
   hZeile:=inttostr(Coverage_id);repeat hZeile:= ' '+hZeile; until length(hZeile) = 10;
   Zeile:=Zeile + hZeile + hZeile + hZeile;
   FileObj.WriteE00Line(Zeile);


   worked:=1;
   if Akt_Layer^.Flaechenarcs <> 1 then
   repeat
      Zeile:='';
      hZeile:=inttostr(Coverage_id+worked);repeat hZeile:= ' '+hZeile; until length(hZeile) = 10;
      Zeile:=Zeile + hZeile + hZeile + hZeile; worked:=worked + 1;
      if worked <> Akt_Layer^.Flaechenarcs then
      begin
         hZeile:=inttostr(Coverage_id+worked);repeat hZeile:= ' '+hZeile; until length(hZeile) = 10;
         Zeile:=Zeile + hZeile + hZeile + hZeile; worked:=worked + 1;
      end;
      FileObj.WriteE00Line(Zeile);
   until worked >= Akt_Layer^.Flaechenarcs;

   { Nun werden die einzelnen PAL Elemente geschrieben }
   Zeiger:=Akt_Layer^.Flaechen;
   repeat
      hZeile:=inttostr(Zeiger^.Numarcs); repeat hZeile:=' '+hZeile; until length(hZeile) = 10;

      Zeile:=hZeile+FileObj.GetFloatStr(Zeiger^.Box.X_Min)+FileObj.GetFloatStr(Zeiger^.Box.X_Max);
      Zeile:=Zeile + FileObj.GetFloatStr(Zeiger^.Box.Y_Min)+FileObj.GetFloatStr(Zeiger^.Box.Y_Max);
      FileObj.WriteE00Line(Zeile);

      { Nun werden die einzelnen ARC-Elemente dieses Pals geschrieben }
      worked:=0;
      repeat
         Zeile:='';
         hZeile:=inttostr(Coverage_id);repeat hZeile:= ' '+hZeile; until length(hZeile) = 10;
         Zeile:=Zeile + hZeile + hZeile + hZeile; worked:=worked + 1; Coverage_id:=Coverage_id + 1;
         if worked <> Zeiger^.Numarcs then
         begin
            hZeile:=inttostr(Coverage_id);repeat hZeile:= ' '+hZeile; until length(hZeile) = 10;
            Zeile:=Zeile + hZeile + hZeile + hZeile; Coverage_id:=Coverage_id + 1; worked:=worked + 1;
         end;
         FileObj.WriteE00Line(Zeile);
      until worked = Zeiger^.Numarcs;
      Zeiger:=Zeiger^.next;
   until Zeiger = nil;
   FileObj.WriteE00Line('        -1         0         0         0         0         0         0');
end;

{*******************************************************************************
* PROZEDUR : ClassPolygon.WritePolygonsArcs                                    *
********************************************************************************
* Prozedur schreibt die ARC-Linienz�ge des aktuellen Layers in die E00-Datei.  *
*                                                                              *
* PARAMETER: Coverage_id -> Coverage_id, mit der die ARCS beginnen sollen.
*                                                                              *
********************************************************************************}
procedure ClassPolygon.WritePolygonsArcs(var Coverage_id:longint);
var Zeiger:p_Polygon_record;
    Punktzeiger:p_Punkt_record;
    Zeile,hZeile:string;
    CurrentARC,verarbeitet:integer;
    OldZeile:string;
    NewArcFlag:boolean;
begin
   Zeiger:=Akt_Layer^.Flaechen;
   { Nun werden die ARC�s geschrieben  }
   repeat
       { Erhalten der ARC - Definitionszeile }
       Zeile:=inttostr(Coverage_id); repeat Zeile:=' '+Zeile; until length(Zeile) = 10;
       Zeile:=Zeile + '         0         0         0         0         0';
       if Zeiger^.Numarcs = 1 then { Wenn diese Linie nur aus einem ARC besteht }
       begin
          hZeile:=inttostr(Zeiger^.Punktanz); repeat hZeile:=' '+hZeile; until length(hZeile) = 10;
       end
       else { Wenn die Linie aus mehreren ARC besteht }
       begin
          hZeile:=inttostr(500); repeat hZeile:=' '+hZeile; until length(hZeile) = 10;
       end;
       Akt_Layer^.Flaechenarcs:=Akt_Layer^.Flaechenarcs + 1;

       Zeile:=Zeile + hZeile;
       { Schreiben der ARC - Definitionszeile in die E00-Datei }
       FileObj.WriteE00Line(Zeile);

       { Schreiben der Koordinatenpaare }
       Punktzeiger:=Zeiger^.Punkte; CurrentARC:=1; verarbeitet:=0; OldZeile:='';
       if Punktzeiger <> nil then
       repeat
          { Wenn die Linie aus mehreren ARCS besteht muss ein neuer ARC-Definitionskopf geschrieben werden }
          Zeile:=''; NewARCFlag:=false;
          if verarbeitet = 500 then
          begin
             Coverage_id:=Coverage_id + 1; CurrentArc:=CurrentArc + 1;
             { Erhalten der ARC - Definitionszeile }
             Zeile:=inttostr(Coverage_id); repeat Zeile:=' '+Zeile; until length(Zeile) = 10;
             Zeile:=Zeile + '         0         0         0         0         0';

             if Zeiger^.Numarcs = CurrentArc then { Wenn gerade der letzte Teil dieser Linie geschrieben wird }
             begin
                { Damit die Verbindungspunkte auch ber�cksichtigt werden }
                hZeile:=inttostr(Zeiger^.Punktanz+Zeiger^.Numarcs - 1); repeat hZeile:=' '+hZeile; until length(hZeile) = 10;
             end
             else { Wenn die Linie aus mehreren ARC besteht }
             begin
                hZeile:=inttostr(500); repeat hZeile:=' '+hZeile; until length(hZeile) = 10;
             end;
             Zeile:=Zeile + hZeile;
             { Schreiben der ARC - Definitionszeile in die E00-Datei }
             FileObj.WriteE00Line(Zeile);
             verarbeitet:=0;
             Akt_Layer^.Flaechenarcs:=Akt_Layer^.Flaechenarcs + 1;
             Zeile:=OldZeile; verarbeitet:=2;
             newArcFlag:=true; Punktzeiger:=Punktzeiger^.next;
          end;

          { Schreiben des ersten Koordinatenpaares in die E00-Datei }
          Zeile:=Zeile + FileObj.GetFloatStr(PunktZeiger^.X) + FileObj.GetFloatStr(PunktZeiger^.Y);

          if NewArcFlag = false then
          begin
             { Schreiben des zweiten Koordinatenpaares in die E00-Datei }
             Punktzeiger:=Punktzeiger^.next; verarbeitet:=verarbeitet + 1;
             if Punktzeiger <> nil then begin OldZeile:=FileObj.GetFloatStr(PunktZeiger^.X) + FileObj.GetFloatStr(PunktZeiger^.Y); Zeile:=Zeile + OldZeile; end;
             if Punktzeiger <> nil then begin Punktzeiger:=Punktzeiger^.next;verarbeitet:=verarbeitet + 1;end;
          end;
          FileObj.WriteE00Line(Zeile);
       until PunktZeiger = nil;
       Zeiger:=Zeiger^.next; Coverage_id:=Coverage_id + 1;
   until Zeiger = nil;
end;


{*******************************************************************************
* FUNKTION : ClassPolygon.GetPerimeter                                         *
********************************************************************************
* Prozedur errechnet den Fl�chenumfang der Fl�che, die durch den Zeiger        *
* gekennzeichnet ist.                                                          *
*                                                                              *
* PARAMETER: Zeiger -> Zeiger auf den Anfang der Punktliste f�r ein Polygon    *
*                                                                              *
* R�ckgabewert: Umfang                                                         *
*                                                                              *
********************************************************************************}
function ClassPolygon.GetPerimeter(Zeiger:p_Punkt_record):double;
var P:double;
    Zeiger1,Zeiger2:p_Punkt_record;
begin
   Zeiger1:=Zeiger; Zeiger2:=Zeiger; P:=0;
   repeat
      Zeiger2:=Zeiger2^.next;
      P:=P + sqrt(sqr(abs(Zeiger2^.X - Zeiger1^.X)) + sqr(abs(Zeiger2^.Y - Zeiger1^.Y)));
      Zeiger1:=Zeiger1^.next;
   until Zeiger2^.next = nil;
   { Damit die Verbindungslinie zwischen Anfangs und Endpunkt nicht vergessen wird }
   P:=P + sqrt(sqr(abs(Zeiger2^.X - Zeiger^.X)) + sqr(abs(Zeiger2^.Y - Zeiger^.Y)));
   result:=P;
end;

{*******************************************************************************
* FUNKTION : ClassPolygon.GetArea                                              *
********************************************************************************
* Prozedur errechnet den Fl�cheninhalt der Fl�che, die durch den Zeiger        *
* gekennzeichnet ist.                                                          *
*                                                                              *
* PARAMETER: Zeiger -> Zeiger auf den Anfang der Punktliste f�r ein Polygon    *
*                                                                              *
* R�ckgabewert: Fl�cheninhalt                                                  *
*                                                                              *
********************************************************************************}
function ClassPolygon.GetArea(Zeiger:p_Punkt_record):double;
var F:double;
    Zeiger1,Zeiger2:p_Punkt_record;
begin
   Zeiger1:=Zeiger; Zeiger2:=Zeiger; F:=0;
   repeat
      Zeiger2:=Zeiger2^.next;
      F:=F+ ((Zeiger2^.X + Zeiger1^.X) * (Zeiger2^.Y - Zeiger1^.Y));
      Zeiger1:=Zeiger1^.next;
   until Zeiger2^.next = nil;
   F:=ABS(F/2);
   result:=F;
end;

{*******************************************************************************
* PROZEDUR : ClassPolygon.WritePolygonAttributes                               *
********************************************************************************
* Prozedur schreibt die Fl�chenattributdaten des Layers in die E00-Datei.      *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassPolygon.WritePolygonAttributes;
var Zeiger:p_Polygon_record;
    DBZeiger,AttDBZeiger:p_Datenbank_record;
    gefunden:boolean;
    DBAnz,DBlength,Startpos,count,Coverage_id,Spaltenbreite:integer;
    Zeile,hstr,hZeile:string;
    hdouble,Area,Perimeter:double;
    Laenge:integer;
    Typ:integer;
    IsNullPal:boolean;
    DBLen:integer;
begin
   Zeiger:=Akt_Layer^.Flaechen;
   { Schreiben der Datenbankspalteninformation }
   DBZeiger:=Akt_ColumnInfo; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   if DBZeiger <> nil then
   repeat
      if DBZeiger <> nil then if DBZeiger^.Art = TRUE then gefunden:=true;
      if gefunden = false then DBzeiger:=DBzeiger^.next;
   until (gefunden = TRUE) or (DBZeiger = nil);

   DBAnz:=0; DBlength:=16;

   if DBzeiger <> nil then
   repeat
      Laenge:=DatenbankObj.GetLength(DBZeiger^.Bezeichnung);
      Typ:=DatenbankObj.GetType(DBZeiger^.Bezeichnung);
      { Suchen wie viele Datenbankspalten vorhanden sind und ermitteln der Spaltenl�nge }
      if Typ = Col_Float  then DBlength:=DBlength + 4;
      if Typ = Col_Int    then DBlength:=DBlength + 4;
      if Typ = Col_String then DBlength:=DBlength + Laenge;
      DBAnz:=DBAnz + 1;
      DBZeiger:=DBZeiger^.next;
   until DBzeiger = nil;

   DBAnz := DBAnz + 4; { Wegen der # und -ID Spalte, AREA und Surface, Spalte }

   hZeile:=Akt_Layer^.Layername; if length(Akt_Layer^.Layername) > 13 then hZeile:=copy(hZeile,1,13);
   hZeile:=hZeile + '.PAT'; repeat hZeile:=hZeile+' '; until length(hZeile) = 32;
   Zeile:=hZeile + 'XX';
   hZeile:=inttostr(DBAnz); if length(hZeile) < 4 then repeat hZeile:=' '+hZeile; until length(hZeile) >= 4;
   Zeile:=Zeile + hZeile + hZeile;
   hZeile:=inttostr(DBlength); if length(hZeile) < 4 then repeat hZeile:=' '+hZeile; until length(hZeile) >= 4;
   Zeile:=Zeile + hZeile;
   hZeile:=inttostr(Akt_Layer^.Flaechensize + 1); repeat hZeile:=' '+hZeile; until length(hZeile) = 10;
   Zeile:=Zeile + hZeile;
   FileObj.WriteE00Line(Zeile);

   DBZeiger:=Akt_ColumnInfo; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   if DBZeiger <> nil then
   repeat
      if DBZeiger <> nil then if DBZeiger^.Art = TRUE then gefunden:=true;
      if gefunden = false then DBzeiger:=DBzeiger^.next;
   until (gefunden = TRUE) or (DBZeiger = nil);

   { Schreiben der AREA und Perimeter Spalte }
   Zeile:='AREA              4-1   14-1  12 3 60-1  -1  -1-1                   1';
   FileObj.WriteE00Line(Zeile);
   Zeile:='PERIMETER         4-1   54-1  12 3 60-1  -1  -1-1                   2';
   FileObj.WriteE00Line(Zeile);

   { Schreiben der # und -ID Spalte }
   hstr:=Akt_Layer^.Layername; if length(hstr) > 13 then hstr:=copy(hstr,1,13);
   hstr:=hstr + '#';
   if length(hstr) < 16 then repeat hstr:=hstr+' '; until length(hstr) = 16;
   Zeile:=hstr; Zeile:=Zeile + '  4-1   94-1   5-1 50-1  -1  -1-1                   3';
   FileObj.WriteE00Line (Zeile);
   hstr:=Akt_Layer^.Layername; if length(hstr) > 13 then hstr:=copy(hstr,1,13);
   hstr:=hstr + '-ID';
   if length(hstr) < 16 then repeat hstr:=hstr+' '; until length(hstr) = 16;
   Zeile:=hstr; Zeile:=Zeile + '  4-1  134-1   5-1 50-1  -1  -1-1                   4';
   FileObj.WriteE00Line (Zeile);

   Startpos:=17; DBAnz:=5;
   if DBZeiger <> nil then { Nun k�nnen die eigentlichen Spaltendefinitionen geschrieben werden }
   repeat
      Laenge:=DatenbankObj.GetLength(DBZeiger^.Bezeichnung);
      Typ:=DatenbankObj.GetType(DBZeiger^.Bezeichnung);
      { Schreiben des Spaltennamens }
      hstr:=DBZeiger^.Bezeichnung; if length(hstr) < 16 then repeat hstr:=hstr+' '; until length(hstr) = 16;
      Zeile:=hstr;

      { Schreiben des Spaltenl�ngenfelds }
      if Typ = Col_Float  then hstr:='4-1';
      if Typ = Col_Int    then hstr:='4-1';
      if Typ = Col_String then hstr:=inttostr(Laenge)+'-1';
      if length(hstr)<5 then repeat hstr:=' '+hstr; until length(hstr)=5;
      Zeile:=Zeile + hstr;

      { Schreiben der Spaltenstartspalte }
      hstr:=inttostr(Startpos) + '4-1';
      if length(hstr)<7 then repeat hstr:=' '+hstr; until length(hstr)=7;
      Zeile:=Zeile + hstr;

      { Ermitteln der neuen Startpos }
      if Typ = Col_Float  then Startpos:=Startpos + 4;
      if Typ = Col_Int    then Startpos:=Startpos + 4;
      if Typ = Col_String then Startpos:=Startpos + Laenge;

      { Ermitteln des output Formats }
      if Typ = Col_Float  then hstr:='12 3';
      if Typ = Col_Int    then hstr:='5-1';
      if Typ = Col_String then hstr:=inttostr(Laenge)+'-1';
      if length(hstr)<6 then repeat hstr:=' '+hstr; until length(hstr)=6;
      Zeile:=Zeile + hstr;

      { Ermitteln des Datentyps }
      if Typ = Col_Float  then Zeile:=Zeile + ' 60-1  -1  -1-1';
      if Typ = Col_Int    then Zeile:=Zeile + ' 50-1  -1  -1-1';
      if Typ = Col_String then Zeile:=Zeile + ' 20-1  -1  -1-1';

      { Ermitteln der Spaltennummer }
      hstr:=inttostr(DBAnz);
      if length(hstr)<20 then repeat hstr:=' '+hstr; until length(hstr)=20;
      Zeile:=Zeile + hstr;
      FileObj.WriteE00Line(Zeile);

      DBZeiger:=DBZeiger^.next; DBAnz:=DBAnz + 1;
   until DBZeiger = nil;

   // now the database information itself will be written
   Coverage_id:=1;
   IsNullPal:=TRUE;
   repeat
      { Erhalten des Fl�cheninhalts und des Umfangs der Fl�che }
      if IsNullPal then Area:=0 else Area:=GetArea(Zeiger^.Punkte);
      if IsNullPal then Perimeter:=0 else Perimeter:=GetPerimeter(Zeiger^.Punkte);
      hstr:=FileObj.GetFloatStr(Area);
      if length(hstr)< 14 then repeat hstr:=' '+hstr; until length(hstr) = 14;
      Zeile:=hstr;
      hstr:=FileObj.GetFloatStr(Perimeter);
      if length(hstr)< 14 then repeat hstr:=' '+hstr; until length(hstr) = 14;
      Zeile:=Zeile + hstr;

      { Erhalten des # und ID Eintrages }
      hstr:=inttostr(Coverage_id);
      if length(hstr)< 11 then repeat hstr:=' '+hstr; until length(hstr) = 11;
      Zeile:=Zeile + hstr; Zeile:=Zeile + hstr;

      spaltenbreite:=50;

      DBZeiger:=Akt_ColumnInfo; gefunden:=false;
      { Setzen des Zeigers an die Datenbankinformation }
      if DBZeiger <> nil then
      repeat
         if DBZeiger <> nil then if DBZeiger^.Art = TRUE then gefunden:=true;
         if gefunden = false then DBzeiger:=DBzeiger^.next;
      until (gefunden = TRUE) or (DBZeiger = nil);

      if DBZeiger <> nil then
      begin
         if IsNullPal then AttDbZeiger:=nil else AttDBZeiger:=Zeiger^.DBInfo;
         repeat
            if AttDBZeiger = nil then hstr:=''
            else hstr:=AttDBZeiger^.Inhalt;
            Laenge:=DatenbankObj.GetLength(DBZeiger^.Bezeichnung);
            Typ:=DatenbankObj.GetType(DBZeiger^.Bezeichnung);
            if IsNullPal then
            begin
               if Typ = Col_Float then hstr:='0';
               if Typ = Col_Int   then hstr:='0';
            end;
            if Typ = Col_Float  then begin val(hstr,hdouble,count); hstr:=FileObj.GetFloatStr(hdouble); SpaltenBreite:=SpaltenBreite + 14; end;
            if Typ = Col_Int    then begin if length(hstr) < 11 then repeat hstr:=' '+hstr; until length(hstr) = 11; SpaltenBreite:=SpaltenBreite + 11; end;
            if Typ = Col_String then begin if length(hstr) < Laenge then repeat hstr:= ' '+hstr; until length(hstr) = Laenge; SpaltenBreite:=SpaltenBreite + Laenge; end;

            { Die Zeilenl�nge eines Informationseintrags darf 80 Zeichen nicht �berschreiten, sonst muss neue Zeile begonnen werden }
            if SpaltenBreite > 80 then
            begin
               DBLen:=80 - length(Zeile);
               Zeile:=Zeile + copy(hstr,1,DBLen);
               FileObj.WriteE00Line(Zeile);
               Zeile:=copy(hstr,DBLen+1,length(hstr));
               SpaltenBreite:=length(Zeile);
            end
            else
               Zeile:=Zeile + hstr;
            if DBZeiger <> nil then DBZeiger:=DBZeiger^.next;
            if AttDBZeiger <> nil then AttDBZeiger:=AttDBZeiger^.next;
         until (DBZeiger = nil);
      end;
      FileObj.WriteE00Line(Zeile);
      if not IsNullPal then
         Zeiger:=Zeiger^.next;
      Coverage_id:=Coverage_id + 1;
      IsNullPal:=FALSE;
   until Zeiger = nil;
end;

{*******************************************************************************
* PROZEDUR : ClassLinie.WriteLinesAttributes                                   *
********************************************************************************
* Prozedur schreibt die Linienattributdaten des Layers in die E00-Datei.       *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassLinie.WriteLinesAttributes;
var Zeiger:p_Polygon_record;
    DBZeiger,AttDBZeiger:p_Datenbank_record;
    gefunden:boolean;
    DBAnz,DBlength,Startpos,count,Coverage_id,Spaltenbreite,i:integer;
    Laenge:integer;
    Typ:integer;
    Zeile,hstr,hZeile:string;
    hdouble:double;
    DBLen:integer;
begin
   Zeiger:=Akt_Layer^.Linien;
   { Schreiben der Datenbankspalteninformation }
   DBZeiger:=Akt_ColumnInfo; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   if DBZeiger <> nil then
   repeat
      if DBZeiger <> nil then if DBZeiger^.Art = TRUE then gefunden:=true;
      if gefunden = false then DBzeiger:=DBzeiger^.next;
   until (gefunden = TRUE) or (DBZeiger = nil);

   DBAnz:=0; DBlength:=8;

   if DBzeiger <> nil then
   begin
      repeat
         Laenge:=DatenbankObj.GetLength(DBZeiger^.Bezeichnung);
         Typ:=DatenbankObj.GetType(DBZeiger^.Bezeichnung);
         { Suchen wie viele Datenbankspalten vorhanden sind und ermitteln der Spaltenl�nge }
         if Typ = Col_Float  then DBlength:=DBlength + 4;
         if Typ = Col_Int    then DBlength:=DBlength + 4;
         if Typ = Col_String then DBlength:=DBlength + Laenge;

         if Laenge > 0 then DBAnz:=DBAnz + 1;
         DBZeiger:=DBZeiger^.next;
      until DBzeiger = nil;
   end;

   DBAnz := DBAnz + 2; { Wegen der # und -ID Spalte }
   hZeile:=Akt_Layer^.Layername; if length(hZeile) > 13 then hZeile:=copy(hZeile,1,13);
   hZeile:=hZeile + '.AAT';
   repeat hZeile:=hZeile+' '; until length(hZeile) = 32;
   Zeile:=hZeile + 'XX';
   hZeile:=inttostr(DBAnz); if length(hZeile) < 4 then repeat hZeile:=' '+hZeile; until length(hZeile) = 4;
   Zeile:=Zeile + hZeile + hZeile;
   hZeile:=inttostr(DBlength); if length(hZeile) < 4 then repeat hZeile:=' '+hZeile; until length(hZeile) = 4;
   Zeile:=Zeile + hZeile;
   hZeile:=inttostr(Akt_Layer^.LinienArcs); repeat hZeile:=' '+hZeile; until length(hZeile) = 10;
   Zeile:=Zeile + hZeile;
   FileObj.WriteE00Line(Zeile);

   DBZeiger:=Akt_ColumnInfo; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   if DBZeiger <> nil then
   repeat
      if DBZeiger <> nil then if DBZeiger^.Art = TRUE then gefunden:=true;
      if gefunden = false then DBzeiger:=DBzeiger^.next;
   until (gefunden = TRUE) or (DBZeiger = nil);

   { Schreiben der # und -ID Spalte }
   hstr:=Akt_Layer^.Layername; if length(hstr) > 13 then hstr:=copy(hstr,1,13);
   hstr:=hstr + '#';
   if length(hstr) < 16 then repeat hstr:=hstr+' '; until length(hstr) = 16;
   Zeile:=hstr; Zeile:=Zeile + '  4-1   14-1   5-1 50-1  -1  -1-1                   1';
   FileObj.WriteE00Line (Zeile);
   hstr:=Akt_Layer^.Layername; if length(hstr) > 13 then hstr:=copy(hstr,1,13);
   hstr:=hstr + '-ID';
   if length(hstr) < 16 then repeat hstr:=hstr+' '; until length(hstr) = 16;
   Zeile:=hstr; Zeile:=Zeile + '  4-1   54-1   5-1 50-1  -1  -1-1                   2';
   FileObj.WriteE00Line (Zeile);

   Startpos:=9; DBAnz:=3;
   if DBZeiger <> nil then { Nun k�nnen die eigentlichen Spaltendefinitionen geschrieben werden }
   repeat
      Laenge:=DatenBankObj.GetLength(DBZeiger^.Bezeichnung);
      Typ:=DatenbankObj.GetType(DBZeiger^.Bezeichnung);
      { Schreiben des Spaltennamens }
      hstr:=DBZeiger^.Bezeichnung; if length(hstr) < 16 then repeat hstr:=hstr+' '; until length(hstr) = 16;
      Zeile:=hstr;

      { Schreiben des Spaltenl�ngenfelds }
      if Typ = Col_Float  then hstr:='4-1';
      if Typ = Col_Int    then hstr:='4-1';
      if Typ = Col_String then hstr:=inttostr(Laenge)+'-1';
      if length(hstr)<5 then repeat hstr:=' '+hstr; until length(hstr)=5;
      Zeile:=Zeile + hstr;

      { Schreiben der Spaltenstartspalte }
      hstr:=inttostr(Startpos) + '4-1';
      if length(hstr)<7 then repeat hstr:=' '+hstr; until length(hstr)=7;
      Zeile:=Zeile + hstr;

      { Ermitteln der neuen Startpos }
      if Typ = Col_Float  then Startpos:=Startpos + 4;
      if Typ = Col_Int    then Startpos:=Startpos + 4;
      if Typ = Col_String then Startpos:=Startpos + Laenge;

      { Ermitteln des output Formats }
      if Typ = Col_Float  then hstr:='12 3';
      if Typ = Col_Int    then hstr:='5-1';
      if Typ = Col_String then hstr:=inttostr(Laenge) + '-1';
      if length(hstr)<6 then repeat hstr:=' '+hstr; until length(hstr)=6;
      Zeile:=Zeile + hstr;

      { Ermitteln des Datentyps }
      if Typ = Col_Float  then Zeile:=Zeile + ' 60-1  -1  -1-1';
      if Typ = Col_Int    then Zeile:=Zeile + ' 50-1  -1  -1-1';
      if Typ = Col_String then Zeile:=Zeile + ' 20-1  -1  -1-1';

      { Ermitteln der Spaltennummer }
      hstr:=inttostr(DBAnz);
      if length(hstr)<20 then repeat hstr:=' '+hstr; until length(hstr)=20;
      Zeile:=Zeile + hstr;

      if Laenge > 0 then FileObj.WriteE00Line(Zeile);
      DBZeiger:=DBZeiger^.next; DBAnz:=DBAnz + 1;
   until DBZeiger = nil;

   Coverage_id:=1;
   { Schreiben der Attributinformation }
   repeat
      { Wenn eine Linie aus mehreren ARC�s besteht, so erh�lt jeder ARC-die selben Attribute }
      for i:=1 to Zeiger^.Numarcs do
      begin
         { Erhalten des # und ID Eintrages }
         hstr:=inttostr(Coverage_id);
         if length(hstr)< 11 then repeat hstr:=' '+hstr; until length(hstr) = 11;
         Zeile:=hstr; Zeile:=Zeile + hstr;

         spaltenbreite:=22;

         DBZeiger:=Akt_ColumnInfo; gefunden:=false;
         { Setzen des Zeigers an die Datenbankinformation }
         if (DBZeiger <> nil) then
         repeat
            if DBZeiger <> nil then if DBZeiger^.Art = TRUE then gefunden:=true;
            if gefunden = false then DBzeiger:=DBzeiger^.next;
         until (gefunden = TRUE) or (DBZeiger = nil);

         if DBZeiger <> nil then
         begin
            AttDBZeiger:=Zeiger^.DBInfo;
            repeat
               Laenge:=DatenbankObj.GetLength(DBZeiger^.Bezeichnung);
               Typ:=DatenbankObj.GetType(DBZeiger^.Bezeichnung);
               if AttDBZeiger = nil then hstr:=''
               else hstr:=AttDBZeiger^.Inhalt;
               if Typ = Col_Float  then begin val(hstr,hdouble,count); hstr:=FileObj.GetFloatStr(hdouble); SpaltenBreite:=SpaltenBreite + 14; end;
               if Typ = Col_Int    then begin if length(hstr) < 11 then repeat hstr:=' '+hstr; until length(hstr) = 11; SpaltenBreite:=SpaltenBreite + 11; end;
               if Typ = Col_String then begin if length(hstr) < Laenge then repeat hstr:= ' '+hstr; until length(hstr) = Laenge; SpaltenBreite:=SpaltenBreite + Laenge; end;

               { Die Zeilenl�nge eines Informationseintrags darf 80 Zeichen nicht �berschreiten, sonst muss neue Zeile begonnen werden }
               if SpaltenBreite > 80 then
               begin
                  DBLen:=80 - length(Zeile);
                  Zeile:=Zeile + copy(hstr,1,DBLen);
                  FileObj.WriteE00Line(Zeile);
                  Zeile:=copy(hstr,DBLen+1,length(hstr));
                  SpaltenBreite:=length(Zeile);
               end
               else
                  Zeile:=Zeile + hstr;
               if DBZeiger <> nil then DBZeiger:=DBZeiger^.next;
               if AttDBZeiger <> nil then AttDBZeiger:=AttDBZeiger^.next;
            until DBZeiger = nil;
         end;
         FileObj.WriteE00Line(Zeile);
         Coverage_id:=Coverage_id + 1;
      end;
      Zeiger:=Zeiger^.next;
   until Zeiger = nil;
end;

{*******************************************************************************
* PROZEDUR : ClassLinie.WriteLinesGraphic                                      *
********************************************************************************
* Prozedur schreibt die Linien des aktuellen Layers in die E00-Datei.          *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassLinie.WriteLinesGraphic(var Coverage_id:longint);
var Zeiger:p_Polygon_record;
    Punktzeiger:p_Punkt_record;
    Zeile,hZeile:string;
    verarbeitet,CurrentARC:integer;
    OldZeile:string;
    NewARCFlag:boolean;
begin
   Zeiger:=Akt_Layer^.Linien; Zeile:='';
   { Nun werden die ARC�s geschrieben  }
   repeat
       { Erhalten der ARC - Definitionszeile }
       Zeile:=inttostr(Coverage_id); repeat Zeile:=' '+Zeile; until length(Zeile) = 10;
       Zeile:=Zeile + '         0         0         0         0         0';
       if Zeiger^.Numarcs = 1 then { Wenn diese Linie nur aus einem ARC besteht }
       begin
          hZeile:=inttostr(Zeiger^.Punktanz); repeat hZeile:=' '+hZeile; until length(hZeile) = 10;
       end
       else { Wenn die Linie aus mehreren ARC besteht }
       begin
          hZeile:=inttostr(500); repeat hZeile:=' '+hZeile; until length(hZeile) = 10;
       end;
       Akt_Layer^.Flaechenarcs:=Akt_Layer^.Flaechenarcs + 1;

       Zeile:=Zeile + hZeile;
       { Schreiben der ARC - Definitionszeile in die E00-Datei }
       FileObj.WriteE00Line(Zeile);

       { Schreiben der Koordinatenpaare }
       Punktzeiger:=Zeiger^.Punkte; CurrentARC:=1; verarbeitet:=0; OldZeile:='';
       if Punktzeiger <> nil then
       repeat
          { Wenn die Linie aus mehreren ARCS besteht muss ein neuer ARC-Definitionskopf geschrieben werden }
          Zeile:=''; NewARCFlag:=false;
          if verarbeitet = 500 then
          begin
             Coverage_id:=Coverage_id + 1; CurrentArc:=CurrentArc + 1;
             { Erhalten der ARC - Definitionszeile }
             Zeile:=inttostr(Coverage_id); repeat Zeile:=' '+Zeile; until length(Zeile) = 10;
             Zeile:=Zeile + '         0         0         0         0         0';

             if Zeiger^.Numarcs = CurrentArc then { Wenn gerade der letzte Teil dieser Linie geschrieben wird }
             begin
                { Damit die Verbindungspunkte ber�cksichtigt werden m�ssen NumArcs - 1 Punkte dazugez�hlt werden }
                hZeile:=inttostr(Zeiger^.Punktanz+Zeiger^.NumArcs - 1); repeat hZeile:=' '+hZeile; until length(hZeile) = 10;
             end
             else { Wenn die Linie aus mehreren ARC besteht }
             begin
                hZeile:=inttostr(500); repeat hZeile:=' '+hZeile; until length(hZeile) = 10;
             end;
             Zeile:=Zeile + hZeile;
             { Schreiben der ARC - Definitionszeile in die E00-Datei }
             FileObj.WriteE00Line(Zeile);
             verarbeitet:=0;
             Akt_Layer^.Flaechenarcs:=Akt_Layer^.Flaechenarcs + 1;
             Zeile:=OldZeile; verarbeitet:=2;
             newArcFlag:=true; Punktzeiger:=Punktzeiger^.next;
          end;

          { Schreiben des ersten Koordinatenpaares in die E00-Datei }
          Zeile:=Zeile + FileObj.GetFloatStr(PunktZeiger^.X) + FileObj.GetFloatStr(PunktZeiger^.Y);

          if NewArcFlag = false then
          begin
             { Schreiben des zweiten Koordinatenpaares in die E00-Datei }
             Punktzeiger:=Punktzeiger^.next; verarbeitet:=verarbeitet + 1;
             if Punktzeiger <> nil then begin OldZeile:=FileObj.GetFloatStr(PunktZeiger^.X) + FileObj.GetFloatStr(PunktZeiger^.Y); Zeile:=Zeile + OldZeile; end;
             if Punktzeiger <> nil then begin Punktzeiger:=Punktzeiger^.next;verarbeitet:=verarbeitet + 1;end;
          end;
          FileObj.WriteE00Line(Zeile);
       until PunktZeiger = nil;
       Zeiger:=Zeiger^.next; Coverage_id:=Coverage_id + 1;
   until Zeiger = nil;
end;


{*******************************************************************************
* PROZEDUR : ClassDatenbank.InsertColumn                                       *
********************************************************************************
* Prozedur erzeugt einen neuen Spalteneintrag f�r die Datenbank und f�gt diesen*
* in die Datenbank_anfang Liste ein.                                           *
*                                                                              *
* PARAMETER: Bezeichnung -> Spaltenbezeichnung                                 *
*            Laenge      -> L�nge der Spalte                                   *
*            Typ         -> Typ der Spalte                                     *
*            Inhalt      -> Inhalt der Spalte                                  *
*            von         -> Beginn der Spalte im ASC-File                      *
*            bis         -> Ende der Spalte im ASC-File                        *
*            Art         -> TRUE, wenn es sich um DB-Spalte handelt, sonst F   *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.Insertcolumn(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string;von:integer;bis:integer;Art:boolean);
begin
   Neu(Bezeichnung, Laenge, Typ, Inhalt,von,bis,Art,Datenbank_anfang);
   Akt_ColumnInfo:=Datenbank_Anfang;
end;


{*******************************************************************************
* PROZEDUR : ClassDatenbank.Getvonbis                                          *
********************************************************************************
* Prozedur dient dazu den von und bis Bereich aus der Datenbankliste zu        *
* erhalten.                                                                    *
*                                                                              *
* PARAMETER: Bezeichnung -> Name der Spalte aus dem ADF-File                   *
*            von         -> von Bereich aus dem ASC-File                       *
*            bis         -> bis Bereich aus dem ASC-File                       *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.Getvonbis(Bezeichnung:string;var von:integer; var bis:integer);
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then
   begin
      von:=Zeiger^.von;
      bis:=Zeiger^.bis;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.ResetColumn                                        *
********************************************************************************
* Prozedur dient dazu den Aktellen Spalteneintragszeiger auf den Anfang zu     *
* setzen.                                                                      *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.ResetColumn;
begin
   Akt_Datenbank:=Datenbank_Anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassPunkt.Neu                                                    *
********************************************************************************
* Prozedur erzeugt einen neuen Punkteintrag.                                   *
*                                                                              *
* PARAMETER: Layername -> Layer auf dem sich der Punkt befindet                *
*            X -> X-Koordinate des Punkts                                      *
*            Y -> Y-Koordinate des Punkts                                      *
*                                                                              *
********************************************************************************}
procedure ClassPunkt.Neu(Layername:string; X:double; Y:double);
var neuer_record,Zeiger:p_punktobj_record;
    ret:boolean;
begin
   ret:=LayerObj.Checkexists(Layername); { Wenn er exisitiert steht sein Wert schon im Akt_Layer }
   if ret = false then
      LayerObj.Neu(Layername);

   Akt_Layer^.Punktesize:=Akt_Layer^.Punktesize + 1;

   { Setzen der Boundingbox-Daten }
   if Akt_Layer^.PunkteBox.X_min = 0 then Akt_Layer^.PunkteBox.X_min:=X;
   if Akt_Layer^.PunkteBox.X_max = 0 then Akt_Layer^.PunkteBox.X_max:=X;
   if Akt_Layer^.PunkteBox.Y_min = 0 then Akt_Layer^.PunkteBox.Y_min:=Y;
   if Akt_Layer^.PunkteBox.Y_max = 0 then Akt_Layer^.PunkteBox.Y_max:=Y;

   if Akt_Layer^.PunkteBox.X_min > X then Akt_Layer^.PunkteBox.X_min:=X;
   if Akt_Layer^.PunkteBox.X_max < X then Akt_Layer^.PunkteBox.X_max:=X;
   if Akt_Layer^.PunkteBox.Y_min > Y then Akt_Layer^.PunkteBox.Y_min:=Y;
   if Akt_Layer^.PunkteBox.Y_max < Y then Akt_Layer^.PunkteBox.Y_max:=Y;

   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.DBInfo:=nil;

   if Akt_Layer^.Punkte = NIL then
   begin
      Akt_Layer^.Punkte:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=Akt_Layer^.Punkte;
      repeat
         if Zeiger^.next <> nil then Zeiger:=Zeiger^.next;
      until Zeiger^.next = nil;
      Zeiger^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
   end;
   Akt_Punkt:=neuer_record;
end;

{*******************************************************************************
* PROZEDUR : ClassPolygon.Neu                                                  *
********************************************************************************
* Prozedur erzeugt einen neuen Polygoneintrag. Die Punkte, die zum Polygon     *
* geh�ren, werden erst sp�ter mit der Prozedur InsertPoint eingetragen.        *
*                                                                              *
* PARAMETER: Layername -> Layer auf der sich das Objekt befindet               *
*            Objekttyp -> 1 bedeutet Linie, 2 bedeutet Fl�che                  *
*                                                                              *
********************************************************************************}
procedure ClassPolygon.Neu(Layername:string);
var neuer_record,Zeiger:p_polygon_record;
    ret:boolean;
    Anfang:p_polygon_record;
begin
   Anfang:=nil;
   { Es muss kontrolliert werden, ob der Eintrag f�r diesen Layer bereits existiert }
   ret:=LayerObj.Checkexists(Layername); { Wenn er exisitiert steht sein Wert schon im Akt_Layer }
   if ret = false then
      LayerObj.Neu(Layername);

   {Anfang:=Akt_Layer^.Linien; }
   Anfang:=Akt_Layer^.Flaechen; Akt_Layer^.Flaechensize:=Akt_Layer^.Flaechensize + 1;

   new(neuer_record);
   neuer_record^.Punkte:=nil;
   neuer_record^.DBInfo:=nil;
   neuer_record^.Punktanz:=0;
   neuer_record^.Numarcs:=1;

   neuer_record^.Box.X_min:=0; neuer_record^.Box.Y_min:=0;
   neuer_record^.Box.X_max:=0; neuer_record^.Box.Y_max:=0;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=Anfang;
      repeat
         if Zeiger^.next <> nil then Zeiger:=Zeiger^.next;
      until Zeiger^.next = nil;
      Zeiger^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
   end;
   { Akt_Linie:=neuer_record; Akt_Layer^.Linien:=Anfang; }
   Akt_Flaeche:=neuer_record; Akt_Layer^.Flaechen:=Anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassLinie.Neu                                                    *
********************************************************************************
* Prozedur erzeugt einen neuen Linieneintrag. Die Punkte, die zur Linie        *
* geh�ren, werden erst sp�ter mit der Prozedur InsertPoint eingetragen.        *
*                                                                              *
* PARAMETER: Layername -> Layer auf der sich das Objekt befindet               *
*                                                                              *
********************************************************************************}
procedure ClassLinie.Neu(Layername:string);
var neuer_record,Zeiger:p_polygon_record;
    ret:boolean;
    Anfang:p_polygon_record;
begin
   Anfang:=nil;
   { Es muss kontrolliert werden, ob der Eintrag f�r diesen Layer bereits existiert }
   ret:=LayerObj.Checkexists(Layername); { Wenn er exisitiert steht sein Wert schon im Akt_Layer }
   if ret = false then
      LayerObj.Neu(Layername);

   Anfang:=Akt_Layer^.Linien; Akt_Layer^.Liniensize:=Akt_Layer^.Liniensize + 1;

   new(neuer_record);
   neuer_record^.Punkte:=nil;
   neuer_record^.DBInfo:=nil;
   neuer_record^.Punktanz:=0;
   neuer_record^.Numarcs:=1;

   neuer_record^.Box.X_min:=0; neuer_record^.Box.Y_min:=0;
   neuer_record^.Box.X_max:=0; neuer_record^.Box.Y_max:=0;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=Anfang;
      repeat
         if Zeiger^.next <> nil then Zeiger:=Zeiger^.next;
      until Zeiger^.next = nil;
      Zeiger^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
   end;
   Akt_Linie:=neuer_record; Akt_Layer^.Linien:=Anfang;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.Checkexists                                            *
********************************************************************************
* Funktion �berpr�ft, ob dieser Layer bereits in der Liste vorkommt.           *
*                                                                              *
* PARAMETER: Layername -> Layername, dessen Vorkommen �berpr�ft werden soll.   *
*                                                                              *
* R�CKGABEWERT: TRUE -> Layer existiert bereits                                *
*               FALSE -> Layer existiert noch nicht                            *
********************************************************************************}
function ClassLayer.CheckExists(Layername:string):boolean;
var Zeiger:p_Layer_record;
    gefunden:boolean;
begin
   Zeiger:=Layer_Anfang; gefunden:=false;
   if Zeiger = nil then result:=false
   else
   begin
      repeat
         if Zeiger^.Layername <> Layername then Zeiger:=Zeiger^.next else gefunden:=true;
      until (gefunden = true) or (Zeiger = nil);
   end;
   if (Zeiger = nil) then result:=false else begin result:=true; Akt_Layer:=Zeiger; end;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.Neu                                                    *
********************************************************************************
* Prozedur erzeugt einen neuen Eintrag in der Layerlist.                       *
*                                                                              *
* PARAMETER: Layername -> Name des Layers, der neu eingef�gt werden soll.      *
*                                                                              *
********************************************************************************}
procedure ClassLayer.Neu(Layername:string);
var neuer_record,Zeiger:p_Layer_record;
begin
   new(neuer_record);
   neuer_record^.Layername:=Layername;

   neuer_record^.Punktesize:=0; neuer_record^.Liniensize:=0; neuer_record^.Flaechensize:=0;
   neuer_record^.Linienarcs:=0; neuer_record^.Flaechenarcs:=0;

   neuer_record^.Punkte:=nil;
   neuer_record^.PunkteBox.X_min:=0; neuer_record^.PunkteBox.Y_min:=0;
   neuer_record^.PunkteBox.X_max:=0; neuer_record^.PunkteBox.Y_max:=0;

   neuer_record^.Linien:=nil;
   neuer_record^.LinienBox.X_min:=0; neuer_record^.LinienBox.Y_min:=0;
   neuer_record^.LinienBox.X_max:=0; neuer_record^.LinienBox.Y_max:=0;

   neuer_record^.Flaechen:=nil;
   neuer_record^.FlaechenBox.X_min:=0; neuer_record^.FlaechenBox.Y_min:=0;
   neuer_record^.FlaechenBox.X_max:=0; neuer_record^.FlaechenBox.Y_max:=0;

   if Layer_Anfang = NIL then
   begin
      Layer_Anfang:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=Layer_Anfang;
      repeat
         if Zeiger^.next <> nil then Zeiger:=Zeiger^.next;
      until Zeiger^.next = nil;
      Zeiger^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
   end;
   Akt_Layer:=neuer_record;
   Akt_Layer^.DBColumns:=nil;
end;

procedure ClassLayer.InsertDBColumn(Spaltenname:string);
begin
   DBObj.Neu(Spaltenname,0,Col_String,'',0,0,true,Akt_Layer^.DBColumns);
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.Neu                                                *
********************************************************************************
* Prozedur erzeugt einen neuen Spalteneintrag f�r die Datenbank.               *
*                                                                              *
* PARAMETER: Bezeichnung -> Spaltenbezeichnung                                 *
*            Laenge      -> L�nge der Spalte                                   *
*            Typ         -> Typ der Spalte                                     *
*            Inhalt      -> Inhalt der Spalte                                  *
*            von         -> Beginn der Spalte im ASC-File                      *
*            bis         -> Ende der Spalte im ADF-File                        *
*            Art         -> Art der Spalte T -> DB-Spalte F -> ADF-Spalte      *
*            Anfang      -> Zeiger auf Anfang der Liste in die eingef�gt wird  *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.Neu(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string; von:integer; bis:integer; Art:boolean; var Anfang:p_datenbank_record);
var neuer_record,Zeiger:p_datenbank_record;
begin
   new(neuer_record);

   neuer_record^.Bezeichnung:=Bezeichnung;
   neuer_record^.Laenge:=Laenge;
   neuer_record^.Typ:=Typ;
   neuer_record^.Inhalt:=Inhalt;
   neuer_record^.von:=von;
   neuer_record^.bis:=bis;
   neuer_record^.Art:=Art;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=Anfang;
      repeat
         if Zeiger^.next <> nil then Zeiger:=Zeiger^.next;
      until Zeiger^.next = nil;
      Zeiger^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
   end;
end;

function ClassDatenbank.GetType(Bezeichnung:string):integer;
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then Result:=Zeiger^.Typ;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.GetLength                                          *
********************************************************************************
* Funktion dient zum ermitteln der erforderlichen Spaltenbreite f�r einen ADF  *
* Eintrag.                                                                     *
*                                                                              *
* PARAMETER:   Bezeichnung -> Name der Spalte                                  *
*                                                                              *
* R�ckgabewert: Spaltenbreite f�r die entsprechende Spalte.                    *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetLength(Bezeichnung:string):integer;
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then Result:=Zeiger^.Laenge else Result:=0;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.GetItem                                            *
********************************************************************************
* Funktion dient zum Ermitteln eines Datenbankwertes.                          *
*                                                                              *
*                                                                              *
* PARAMETER:   Bezeichnung -> Name der Spalte                                  *
*                                                                              *
* R�ckgabewert: Inhalt dieser Spalte.                                          *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetItem(Bezeichnung:string):string;
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then Result:=Zeiger^.Inhalt else Result:='';
end;

{*******************************************************************************
* PROZEDUR : ClassPolygon.InsertPoint                                          *
********************************************************************************
* Prozedur f�gt ein Punktpaar zu einem Polygon ein. Akt_Flaeche                *
*                                                                              *
* InPARAMETER: X -> X-Koordinate des neuen Eckpunktes                          *
*              Y -> Y-Koordinate des neuen Eckpunktes                          *
*                                                                              *
********************************************************************************}
procedure ClassPolygon.InsertPoint(X:double;Y:double);
var Zeiger    :p_Punkt_record;
    new_Zeiger:p_Punkt_record;
begin
   { Setzen der Boundingbox Daten  }
   { Setzen der Boundingbox f�r den gesamten E00 }
   if Akt_Layer^.FlaechenBox.X_min = 0 then Akt_Layer^.FlaechenBox.X_min:=X;
   if Akt_Layer^.FlaechenBox.X_max = 0 then Akt_Layer^.FlaechenBox.X_max:=X;
   if Akt_Layer^.FlaechenBox.Y_min = 0 then Akt_Layer^.FlaechenBox.Y_min:=Y;
   if Akt_Layer^.FlaechenBox.Y_max = 0 then Akt_Layer^.FlaechenBox.Y_max:=Y;

   if Akt_Layer^.FlaechenBox.X_min > X then Akt_Layer^.FlaechenBox.X_min:=X;
   if Akt_Layer^.FlaechenBox.X_max < X then Akt_Layer^.FlaechenBox.X_max:=X;
   if Akt_Layer^.FlaechenBox.Y_min > Y then Akt_Layer^.FlaechenBox.Y_min:=Y;
   if Akt_Layer^.FlaechenBox.Y_max < Y then Akt_Layer^.FlaechenBox.Y_max:=Y;

   if Akt_Flaeche^.Box.X_min = 0 then Akt_Flaeche^.Box.X_min:=X;
   if Akt_Flaeche^.Box.X_max = 0 then Akt_Flaeche^.Box.X_max:=X;
   if Akt_Flaeche^.Box.Y_min = 0 then Akt_Flaeche^.Box.Y_min:=Y;
   if Akt_Flaeche^.Box.Y_max = 0 then Akt_Flaeche^.Box.Y_max:=Y;

   if Akt_Flaeche^.Box.X_min > X then Akt_Flaeche^.Box.X_min:=X;
   if Akt_Flaeche^.Box.X_max < X then Akt_Flaeche^.Box.X_max:=X;
   if Akt_Flaeche^.Box.Y_min > Y then Akt_Flaeche^.Box.Y_min:=Y;
   if Akt_Flaeche^.Box.Y_max < Y then Akt_Flaeche^.Box.Y_max:=Y;

   new(new_Zeiger);
   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   zeiger:=Akt_Flaeche^.Punkte; Akt_Flaeche^.Punktanz:=Akt_Flaeche^.Punktanz + 1;

   { Weil in ARCINFO eine Linie nicht mehr als 500 Ecken haben kann }
   if Akt_Flaeche^.Punktanz = 500 then
   begin
      Akt_Flaeche^.Numarcs:=Akt_Flaeche^.Numarcs + 1; Akt_Flaeche^.Punktanz:=0;
   end;

   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Akt_Flaeche^.Punkte:=new_Zeiger; Akt_Flaeche^.Punkte^.next:=nil;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
   end;
end;


{*******************************************************************************
* PROZEDUR : ClassLinie.InsertPoint                                            *
********************************************************************************
* Prozedur f�gt ein Punktpaar zu einer Linie ein. Akt_Linie                    *
*                                                                              *
* InPARAMETER: X -> X-Koordinate des neuen Eckpunktes                          *
*              Y -> Y-Koordinate des neuen Eckpunktes                          *
*                                                                              *
********************************************************************************}
procedure ClassLinie.InsertPoint(X:double;Y:double);
var Zeiger    :p_Punkt_record;
    new_Zeiger:p_Punkt_record;
begin
   { Setzen der Boundingbox Daten  }
   if Akt_Layer^.LinienBox.X_min = 0 then Akt_Layer^.LinienBox.X_min:=X;
   if Akt_Layer^.LinienBox.X_max = 0 then Akt_Layer^.LinienBox.X_max:=X;
   if Akt_Layer^.LinienBox.Y_min = 0 then Akt_Layer^.LinienBox.Y_min:=Y;
   if Akt_Layer^.LinienBox.Y_max = 0 then Akt_Layer^.LinienBox.Y_max:=Y;

   if Akt_Layer^.LinienBox.X_min > X then Akt_Layer^.LinienBox.X_min:=X;
   if Akt_Layer^.LinienBox.X_max < X then Akt_Layer^.LinienBox.X_max:=X;
   if Akt_Layer^.LinienBox.Y_min > Y then Akt_Layer^.LinienBox.Y_min:=Y;
   if Akt_Layer^.LinienBox.Y_max < Y then Akt_Layer^.LinienBox.Y_max:=Y;

   if Akt_Linie^.Box.X_min = 0 then Akt_Linie^.Box.X_min:=X;
   if Akt_Linie^.Box.X_max = 0 then Akt_Linie^.Box.X_max:=X;
   if Akt_Linie^.Box.Y_min = 0 then Akt_Linie^.Box.Y_min:=Y;
   if Akt_Linie^.Box.Y_max = 0 then Akt_Linie^.Box.Y_max:=Y;

   if Akt_Linie^.Box.X_min > X then Akt_Linie^.Box.X_min:=X;
   if Akt_Linie^.Box.X_max < X then Akt_Linie^.Box.X_max:=X;
   if Akt_Linie^.Box.Y_min > Y then Akt_Linie^.Box.Y_min:=Y;
   if Akt_Linie^.Box.Y_max < Y then Akt_Linie^.Box.Y_max:=Y;


   new(new_Zeiger);
   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   zeiger:=Akt_Linie^.Punkte; Akt_Linie^.Punktanz:=Akt_Linie^.Punktanz + 1;

   { Weil in ARCINFO eine Linie nicht mehr als 500 Ecken haben kann }
   if Akt_Linie^.Punktanz = 500 then
   begin
      Akt_Linie^.Numarcs:=Akt_Linie^.Numarcs + 1; Akt_Linie^.Punktanz:=0;
      Akt_Layer^.LinienArcs:=Akt_Layer^.LinienArcs + 1;
   end;

   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Akt_Linie^.Punkte:=new_Zeiger; Akt_Linie^.Punkte^.next:=nil;
      Akt_Layer^.LinienArcs:=Akt_Layer^.LinienArcs + 1;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
   end;
end;

end.

