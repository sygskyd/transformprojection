unit UTMDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, jpeg;

type
  TUTMForm = class(TForm)
    Image: TImage;
    Shape: TShape;
    Zone: TStaticText;
    procedure ImageMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ShapeMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure GetUTMZone(X,Y: Integer);
  public
    UTMProjection: AnsiString;
  end;

var
  UTMForm: TUTMForm;

implementation

{$R *.dfm}

procedure TUTMForm.ImageMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
        GetUTMZone(X,Y);
end;

procedure TUTMForm.GetUTMZone(X, Y: Integer);
var
s: AnsiString;
W,H: Integer;
n: Integer;
g: Integer;
we: AnsiString;
begin
        W:=Image.Width;
        H:=Image.Height;

        Shape.Width:=W div 60;

        s:='UTM-';

        n:=((X*60) div W) + 1;

        Shape.Left:=(n-1) * (W div 60);

        if n < 10 then begin
                s:=s + '0' + IntToStr(n);
        end
        else begin
                s:=s + IntToStr(n);
        end;

        if (Y > (H div 2)) then begin
                s:=s + 'S';
                Shape.Top:=(H div 2);
                Shape.Height:=(H div 2);
        end
        else begin
                s:=s + 'N';
                Shape.Top:=0;
                Shape.Height:=(H div 2);
        end;

        g:=((n-1) * 6) - 180;

        if g < 0 then begin
                we:='W';
                g:=-g;
        end
        else begin
                we:='E';
        end;

        s:=s + ' (' + IntToStr(g) + ' ' + we + ' to ';

        if (we = 'W') then begin
                s:=s + IntToStr(g-6) + ' ' + we + ')';
        end
        else begin
                s:=s + IntToStr(g+6) + ' ' + we + ')';
        end;

        Zone.Caption:=' ' + s + ' ';

        Shape.Visible:=True;
end;

procedure TUTMForm.ShapeMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
        UTMProjection:=Trim(Zone.Caption);
        Close;
end;

procedure TUTMForm.FormShow(Sender: TObject);
begin
        UTMProjection:='';
end;

procedure TUTMForm.FormCreate(Sender: TObject);
begin
        Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

end.
