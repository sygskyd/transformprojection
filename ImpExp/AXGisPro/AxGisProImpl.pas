unit AxGisProImpl;

interface

uses
  Windows, ActiveX, Classes, Controls, Graphics, Menus, Forms, StdCtrls,
  ComServ, StdVCL, AXCtrls, AxGisPro_TLB, ExtCtrls, ProjectionCalculation;

type
  TAxGisProjection = class(TActiveXControl, IAxGisProjection)
  private
    { Private declarations }
    FDelphiControl: TPanel;
    FEvents: IAxGisProjectionEvents;
    CurrProjection             : TProjectionCalculation;
    ShowedNoProjectionWarning  : boolean;
    // private objects for storing the settings
    AxWorkingPath           :string;         // general settings
    AxLngCode               :string;
    AxShowOnlySource        :boolean;

    AxSourceCoordinateSystem:TxCoordSystem;  // source projection
    AxSourceProjection      :string;
    AxSourceDate            :string;
    AxSourceProjSettings    :string;
    AxSourceXOffset         :double;
    AxSourceYOffset         :double;
    AxSourceScale           :double;
    AxUsedInputCoordinateSystem:TxCoordSystem;

    AxTargetCoordinateSystem:TxCoordSystem;  // target projection
    AxTargetProjection      :string;
    AxTargetDate            :string;
    AxTargetProjSettings    :string;
    AxTargetXOffset         :double;
    AxTargetYOffset         :double;
    AxTargetScale           :double;
    AxUsedOutputCoordinateSystem:TxCoordSystem;

    procedure CanResizeEvent(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure ClickEvent(Sender: TObject);
    procedure ConstrainedResizeEvent(Sender: TObject; var MinWidth, MinHeight,
      MaxWidth, MaxHeight: Integer);
    procedure DblClickEvent(Sender: TObject);
    procedure ResizeEvent(Sender: TObject);
    procedure SetCurrentProjection;
    procedure GetCurrentProjection;
  protected
    { Protected declarations }
    procedure DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage); override;
    procedure EventSinkChanged(const EventSink: IUnknown); override;
    procedure InitializeControl; override;
    destructor Destroy; override;
    function DrawTextBiDiModeFlagsReadingOnly: Integer; safecall;
    function Get_Alignment: TxAlignment; safecall;
    function Get_AutoSize: WordBool; safecall;
    function Get_BevelInner: TxBevelCut; safecall;
    function Get_BevelOuter: TxBevelCut; safecall;
    function Get_BorderStyle: TxBorderStyle; safecall;
    function Get_Caption: WideString; safecall;
    function Get_Color: OLE_COLOR; safecall;
    function Get_Ctl3D: WordBool; safecall;
    function Get_Cursor: Smallint; safecall;
    function Get_DockSite: WordBool; safecall;
    function Get_DoubleBuffered: WordBool; safecall;
    function Get_DragCursor: Smallint; safecall;
    function Get_DragMode: TxDragMode; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Font: IFontDisp; safecall;
    function Get_FullRepaint: WordBool; safecall;
    function Get_Locked: WordBool; safecall;
    function Get_ParentColor: WordBool; safecall;
    function Get_ParentCtl3D: WordBool; safecall;
    function Get_UseDockManager: WordBool; safecall;
    function Get_Visible: WordBool; safecall;
    function Get_VisibleDockClientCount: Integer; safecall;
    function IsRightToLeft: WordBool; safecall;
    function UseRightToLeftReading: WordBool; safecall;
    function UseRightToLeftScrollBar: WordBool; safecall;
    procedure _Set_Font(const Value: IFontDisp); safecall;
    procedure InitiateAction; safecall;
    procedure Set_Alignment(Value: TxAlignment); safecall;
    procedure Set_AutoSize(Value: WordBool); safecall;
    procedure Set_BevelInner(Value: TxBevelCut); safecall;
    procedure Set_BevelOuter(Value: TxBevelCut); safecall;
    procedure Set_BorderStyle(Value: TxBorderStyle); safecall;
    procedure Set_Caption(const Value: WideString); safecall;
    procedure Set_Color(Value: OLE_COLOR); safecall;
    procedure Set_Ctl3D(Value: WordBool); safecall;
    procedure Set_Cursor(Value: Smallint); safecall;
    procedure Set_DockSite(Value: WordBool); safecall;
    procedure Set_DoubleBuffered(Value: WordBool); safecall;
    procedure Set_DragCursor(Value: Smallint); safecall;
    procedure Set_DragMode(Value: TxDragMode); safecall;
    procedure Set_Enabled(Value: WordBool); safecall;
    procedure Set_Font(var Value: IFontDisp); safecall;
    procedure Set_FullRepaint(Value: WordBool); safecall;
    procedure Set_Locked(Value: WordBool); safecall;
    procedure Set_ParentColor(Value: WordBool); safecall;
    procedure Set_ParentCtl3D(Value: WordBool); safecall;
    procedure Set_UseDockManager(Value: WordBool); safecall;
    procedure Set_Visible(Value: WordBool); safecall;
    function Get_SourceCoordinateSystem: TxCoordSystem; safecall;
    procedure Set_SourceCoordinateSystem(Value: TxCoordSystem); safecall;
    function Get_SourceProjection: WideString; safecall;
    procedure Set_SourceProjection(const Value: WideString); safecall;
    function Get_SourceDate: WideString; safecall;
    procedure Set_SourceDate(const Value: WideString); safecall;
    function Get_SourceProjSettings: WideString; safecall;
    procedure Set_SourceProjSettings(const Value: WideString); safecall;
    function Get_SourceXOffset: Double; safecall;
    procedure Set_SourceXOffset(Value: Double); safecall;
    function Get_SourceYOffset: Double; safecall;
    procedure Set_SourceYOffset(Value: Double); safecall;
    function Get_SourceScale: Double; safecall;
    procedure Set_SourceScale(Value: Double); safecall;
    function Get_WorkingPath: WideString; safecall;
    procedure Set_WorkingPath(const Value: WideString); safecall;
    function Get_LngCode: WideString; safecall;
    procedure Set_LngCode(const Value: WideString); safecall;
    function Get_TargetCoordinateSystem: TxCoordSystem; safecall;
    procedure Set_TargetCoordinateSystem(Value: TxCoordSystem); safecall;
    function Get_TargetProjection: WideString; safecall;
    procedure Set_TargetProjection(const Value: WideString); safecall;
    function Get_TargetDate: WideString; safecall;
    procedure Set_TargetDate(const Value: WideString); safecall;
    function Get_TargetProjSettings: WideString; safecall;
    procedure Set_TargetProjSettings(const Value: WideString); safecall;
    function Get_TargetXOffset: Double; safecall;
    procedure Set_TargetXOffset(Value: Double); safecall;
    function Get_TargetYOffset: Double; safecall;
    procedure Set_TargetYOffset(Value: Double); safecall;
    function Get_TargetScale: Double; safecall;
    procedure Set_TargetScale(Value: Double); safecall;
    function Get_UsedInputCoordSystem: TxCoordSystem; safecall;
    procedure Set_UsedInputCoordSystem(Value: TxCoordSystem); safecall;
    function Get_UsedOutputCoordSystem: TxCoordSystem; safecall;
    procedure Set_UsedOutputCoordSystem(Value: TxCoordSystem); safecall;
    procedure Calculate(var X, Y: Double); safecall;
    procedure SetupByDialog; safecall;
    function Get_ShowOnlySource: WordBool; safecall;
    procedure Set_ShowOnlySource(Value: WordBool); safecall;
    procedure IAxGisProjection._Set_Font = IAxGisProjection__Set_Font;
    procedure IAxGisProjection.Set_Font = IAxGisProjection_Set_Font;
  
    procedure IAxGisProjection__Set_Font(var Value: IFontDisp); safecall;
    procedure IAxGisProjection_Set_Font(const Value: IFontDisp); safecall;
  end;

implementation

uses ComObj, GeoDlg, SysUtils, Dialogs, StrFile;

{ TAxGisProjection }

procedure TAxGisProjection.DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage);
begin
  {TODO: Define property pages here.  Property pages are defined by calling
    DefinePropertyPage with the class id of the page.  For example,
      DefinePropertyPage(Class_AxGisProjectionPage); }
end;

procedure TAxGisProjection.EventSinkChanged(const EventSink: IUnknown);
begin
  FEvents := EventSink as IAxGisProjectionEvents;
end;

destructor TAxGisProjection.Destroy;
begin
   inherited destroy;
   CurrProjection.Destroy;
end;


procedure TAxGisProjection.InitializeControl;
begin
  FDelphiControl := Control as TPanel;
  FDelphiControl.OnCanResize := CanResizeEvent;
  FDelphiControl.OnClick := ClickEvent;
  FDelphiControl.OnConstrainedResize := ConstrainedResizeEvent;
  FDelphiControl.OnDblClick := DblClickEvent;
  FDelphiControl.OnResize := ResizeEvent;
  FDelphiControl.Height:=25;
  FDelphiControl.Width:=50;
  FDelphiControl.Caption:='GisPro';
  // create the projection calculation object
  CurrProjection:=TProjectionCalculation.Create;
  ShowedNoProjectionWarning:=FALSE;
  // setup the default projection
  AxSourceCoordinateSystem:=cs_Carthesic;
  AxSourceProjection:='NONE';
  AxSourceDate:='NONE';
  AxSourceProjSettings:='NONE';
  AxSourceXOffset:=0;
  AxSourceYOffset:=0;
  AxSourceScale:=1;
  AxUsedInputCoordinateSystem:=cs_Carthesic;

  // Target projection
  AxTargetCoordinateSystem:=cs_Carthesic;
  AxTargetProjection:='NONE';
  AxTargetDate:='NONE';
  AxTargetProjSettings:='NONE';
  AxTargetXOffset:=0;
  AxTargetYOffset:=0;
  AxTargetScale:=1;
  AxUsedOutputCoordinateSystem:=cs_Carthesic;
end;

function TAxGisProjection.DrawTextBiDiModeFlagsReadingOnly: Integer;
begin
  Result := FDelphiControl.DrawTextBiDiModeFlagsReadingOnly;
end;

function TAxGisProjection.Get_Alignment: TxAlignment;
begin
  Result := Ord(FDelphiControl.Alignment);
end;

function TAxGisProjection.Get_AutoSize: WordBool;
begin
  Result := FDelphiControl.AutoSize;
end;

function TAxGisProjection.Get_BevelInner: TxBevelCut;
begin
  Result := Ord(FDelphiControl.BevelInner);
end;

function TAxGisProjection.Get_BevelOuter: TxBevelCut;
begin
  Result := Ord(FDelphiControl.BevelOuter);
end;

function TAxGisProjection.Get_BorderStyle: TxBorderStyle;
begin
  Result := Ord(FDelphiControl.BorderStyle);
end;

function TAxGisProjection.Get_Caption: WideString;
begin
  Result := WideString(FDelphiControl.Caption);
end;

function TAxGisProjection.Get_Color: OLE_COLOR;
begin
  Result := OLE_COLOR(FDelphiControl.Color);
end;

function TAxGisProjection.Get_Ctl3D: WordBool;
begin
  Result := FDelphiControl.Ctl3D;
end;

function TAxGisProjection.Get_Cursor: Smallint;
begin
  Result := Smallint(FDelphiControl.Cursor);
end;

function TAxGisProjection.Get_DockSite: WordBool;
begin
  Result := FDelphiControl.DockSite;
end;

function TAxGisProjection.Get_DoubleBuffered: WordBool;
begin
  Result := FDelphiControl.DoubleBuffered;
end;

function TAxGisProjection.Get_DragCursor: Smallint;
begin
  Result := Smallint(FDelphiControl.DragCursor);
end;

function TAxGisProjection.Get_DragMode: TxDragMode;
begin
  Result := Ord(FDelphiControl.DragMode);
end;

function TAxGisProjection.Get_Enabled: WordBool;
begin
  Result := FDelphiControl.Enabled;
end;

function TAxGisProjection.Get_Font: IFontDisp;
begin
  GetOleFont(FDelphiControl.Font, Result);
end;

function TAxGisProjection.Get_FullRepaint: WordBool;
begin
  Result := FDelphiControl.FullRepaint;
end;

function TAxGisProjection.Get_Locked: WordBool;
begin
  Result := FDelphiControl.Locked;
end;

function TAxGisProjection.Get_ParentColor: WordBool;
begin
  Result := FDelphiControl.ParentColor;
end;

function TAxGisProjection.Get_ParentCtl3D: WordBool;
begin
  Result := FDelphiControl.ParentCtl3D;
end;

function TAxGisProjection.Get_UseDockManager: WordBool;
begin
  Result := FDelphiControl.UseDockManager;
end;

function TAxGisProjection.Get_Visible: WordBool;
begin
  Result := FDelphiControl.Visible;
end;

function TAxGisProjection.Get_VisibleDockClientCount: Integer;
begin
  Result := FDelphiControl.VisibleDockClientCount;
end;

function TAxGisProjection.IsRightToLeft: WordBool;
begin
  Result := FDelphiControl.IsRightToLeft;
end;

function TAxGisProjection.UseRightToLeftReading: WordBool;
begin
  Result := FDelphiControl.UseRightToLeftReading;
end;

function TAxGisProjection.UseRightToLeftScrollBar: WordBool;
begin
  Result := FDelphiControl.UseRightToLeftScrollBar;
end;

procedure TAxGisProjection._Set_Font(const Value: IFontDisp);
begin
  SetOleFont(FDelphiControl.Font, Value);
end;

procedure TAxGisProjection.CanResizeEvent(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
var
  TempNewWidth: Integer;
  TempNewHeight: Integer;
  TempResize: WordBool;
begin
  TempNewWidth := Integer(NewWidth);
  TempNewHeight := Integer(NewHeight);
  TempResize := WordBool(Resize);
  if FEvents <> nil then FEvents.OnCanResize(TempNewWidth, TempNewHeight, TempResize);
  NewWidth := Integer(TempNewWidth);
  NewHeight := Integer(TempNewHeight);
  Resize := Boolean(TempResize);
end;

procedure TAxGisProjection.ClickEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnClick;
end;

procedure TAxGisProjection.ConstrainedResizeEvent(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
var
  TempMinWidth: Integer;
  TempMinHeight: Integer;
  TempMaxWidth: Integer;
  TempMaxHeight: Integer;
begin
  TempMinWidth := Integer(MinWidth);
  TempMinHeight := Integer(MinHeight);
  TempMaxWidth := Integer(MaxWidth);
  TempMaxHeight := Integer(MaxHeight);
  if FEvents <> nil then FEvents.OnConstrainedResize(TempMinWidth, TempMinHeight, TempMaxWidth, TempMaxHeight);
  MinWidth := Integer(TempMinWidth);
  MinHeight := Integer(TempMinHeight);
  MaxWidth := Integer(TempMaxWidth);
  MaxHeight := Integer(TempMaxHeight);
end;

procedure TAxGisProjection.DblClickEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnDblClick;
end;

procedure TAxGisProjection.InitiateAction;
begin
  FDelphiControl.InitiateAction;
end;

procedure TAxGisProjection.ResizeEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnResize;
end;

procedure TAxGisProjection.Set_Alignment(Value: TxAlignment);
begin
  FDelphiControl.Alignment := TAlignment(Value);
end;

procedure TAxGisProjection.Set_AutoSize(Value: WordBool);
begin
  FDelphiControl.AutoSize := Value;
end;

procedure TAxGisProjection.Set_BevelInner(Value: TxBevelCut);
begin
  FDelphiControl.BevelInner := TBevelCut(Value);
end;

procedure TAxGisProjection.Set_BevelOuter(Value: TxBevelCut);
begin
  FDelphiControl.BevelOuter := TBevelCut(Value);
end;

procedure TAxGisProjection.Set_BorderStyle(Value: TxBorderStyle);
begin
  FDelphiControl.BorderStyle := TBorderStyle(Value);
end;

procedure TAxGisProjection.Set_Caption(const Value: WideString);
begin
  FDelphiControl.Caption := TCaption(Value);
end;

procedure TAxGisProjection.Set_Color(Value: OLE_COLOR);
begin
  FDelphiControl.Color := TColor(Value);
end;

procedure TAxGisProjection.Set_Ctl3D(Value: WordBool);
begin
  FDelphiControl.Ctl3D := Value;
end;

procedure TAxGisProjection.Set_Cursor(Value: Smallint);
begin
  FDelphiControl.Cursor := TCursor(Value);
end;

procedure TAxGisProjection.Set_DockSite(Value: WordBool);
begin
  FDelphiControl.DockSite := Value;
end;

procedure TAxGisProjection.Set_DoubleBuffered(Value: WordBool);
begin
  FDelphiControl.DoubleBuffered := Value;
end;

procedure TAxGisProjection.Set_DragCursor(Value: Smallint);
begin
  FDelphiControl.DragCursor := TCursor(Value);
end;

procedure TAxGisProjection.Set_DragMode(Value: TxDragMode);
begin
  FDelphiControl.DragMode := TDragMode(Value);
end;

procedure TAxGisProjection.Set_Enabled(Value: WordBool);
begin
  FDelphiControl.Enabled := Value;
end;

procedure TAxGisProjection.Set_Font(var Value: IFontDisp);
begin
  SetOleFont(FDelphiControl.Font, Value);
end;

procedure TAxGisProjection.Set_FullRepaint(Value: WordBool);
begin
  FDelphiControl.FullRepaint := Value;
end;

procedure TAxGisProjection.Set_Locked(Value: WordBool);
begin
  FDelphiControl.Locked := Value;
end;

procedure TAxGisProjection.Set_ParentColor(Value: WordBool);
begin
  FDelphiControl.ParentColor := Value;
end;

procedure TAxGisProjection.Set_ParentCtl3D(Value: WordBool);
begin
  FDelphiControl.ParentCtl3D := Value;
end;

procedure TAxGisProjection.Set_UseDockManager(Value: WordBool);
begin
  FDelphiControl.UseDockManager := Value;
end;

procedure TAxGisProjection.Set_Visible(Value: WordBool);
begin
  FDelphiControl.Visible := Value;
end;

function TAxGisProjection.Get_SourceCoordinateSystem: TxCoordSystem;
begin
   result:=AxSourceCoordinateSystem;
end;

procedure TAxGisProjection.Set_SourceCoordinateSystem(
  Value: TxCoordSystem);
begin
   SetCurrentProjection;
   AxSourceCoordinateSystem:=Value;
   CurrProjection.Set_SourceCoordinateSystem(Value);
end;

function TAxGisProjection.Get_SourceProjection: WideString;
begin
   result:=AxSourceProjection;
end;

procedure TAxGisProjection.Set_SourceProjection(const Value: WideString);
begin
   SetCurrentProjection;
   AxSourceProjection:=Value;
   CurrProjection.Set_SourceProjection(Value);
end;

function TAxGisProjection.Get_SourceDate: WideString;
begin
   result:=AxSourceDate;
end;

procedure TAxGisProjection.Set_SourceDate(const Value: WideString);
begin
   SetCurrentProjection;
   AxSourceDate:=Value;
   CurrProjection.Set_SourceDate(Value);
end;

function TAxGisProjection.Get_SourceProjSettings: WideString;
begin
   result:=AxSourceProjSettings;
end;

procedure TAxGisProjection.Set_SourceProjSettings(const Value: WideString);
begin
   SetCurrentProjection;
   AxSourceProjSettings:=Value;
   CurrProjection.Set_SourceProjSettings(Value);
end;

function TAxGisProjection.Get_SourceXOffset: Double;
begin
   result:=AxSourceXOffset;
end;

procedure TAxGisProjection.Set_SourceXOffset(Value: Double);
begin
   SetCurrentProjection;
   AxSourceXOffset:=Value;
   CurrProjection.Set_SourceXOffset(Value);
end;

function TAxGisProjection.Get_SourceYOffset: Double;
begin
   result:=AxSourceYOffset;
end;

procedure TAxGisProjection.Set_SourceYOffset(Value: Double);
begin
   SetCurrentProjection;
   AxSourceYOffset:=Value;
   CurrProjection.Set_SourceYOffset(Value);
end;

function TAxGisProjection.Get_SourceScale: Double;
begin
   result:=AxSourceScale;
end;

procedure TAxGisProjection.Set_SourceScale(Value: Double);
begin
   SetCurrentProjection;
   AxSourceScale:=Value;
   CurrProjection.Set_SourceScale(Value);
end;

function TAxGisProjection.Get_WorkingPath: WideString;
begin
   result:=AxWorkingPath;
end;

procedure TAxGisProjection.Set_WorkingPath(const Value: WideString);
begin
   SetCurrentProjection;
   AxWorkingPath:=Value;
   if AxWorkingPath[Length(AxWorkingPath)] <> '\' then begin
        AxWorkingPath:=AxWorkingPath + '\';
   end;
   CurrProjection.WorkingPath:=AxWorkingPath;
end;

function TAxGisProjection.Get_LngCode: WideString;
begin
   result:=AxLngCode;
end;

procedure TAxGisProjection.Set_LngCode(const Value: WideString);
begin
   SetCurrentProjection;
   AxLngCode:=Value;
end;

function TAxGisProjection.Get_TargetCoordinateSystem: TxCoordSystem;
begin
   result:=AxTargetCoordinateSystem;
end;

procedure TAxGisProjection.Set_TargetCoordinateSystem(
  Value: TxCoordSystem);
begin
   SetCurrentProjection;
   AxTargetCoordinateSystem:=Value;
   CurrProjection.Set_TargetCoordinateSystem(Value);
end;

function TAxGisProjection.Get_TargetProjection: WideString;
begin
   result:=AxTargetProjection;
end;

procedure TAxGisProjection.Set_TargetProjection(const Value: WideString);
begin
   SetCurrentProjection;
   AxTargetProjection:=Value;
   CurrProjection.Set_TargetProjection(Value);
end;

function TAxGisProjection.Get_TargetDate: WideString;
begin
   result:=AxTargetDate;
end;

procedure TAxGisProjection.Set_TargetDate(const Value: WideString);
begin
   SetCurrentProjection;
   AxTargetDate:=Value;
   CurrProjection.Set_TargetDate(Value);
end;

function TAxGisProjection.Get_TargetProjSettings: WideString;
begin
   result:=AxTargetProjSettings;
end;

procedure TAxGisProjection.Set_TargetProjSettings(const Value: WideString);
begin
   SetCurrentProjection;
   AxTargetProjSettings:=Value;
   CurrProjection.Set_TargetProjSettings(Value);
end;

function TAxGisProjection.Get_TargetXOffset: Double;
begin
   result:=AxTargetXOffset;
end;

procedure TAxGisProjection.Set_TargetXOffset(Value: Double);
begin
   SetCurrentProjection;
   AxTargetXOffset:=Value;
   CurrProjection.Set_TargetXOffset(Value);
end;

function TAxGisProjection.Get_TargetYOffset: Double;
begin
   result:=AxTargetYOffset;
end;

procedure TAxGisProjection.Set_TargetYOffset(Value: Double);
begin
   SetCurrentProjection;
   AxTargetYOffset:=Value;
   CurrProjection.Set_TargetYOffset(Value);
end;

function TAxGisProjection.Get_TargetScale: Double;
begin
   result:=AxTargetScale;
end;

procedure TAxGisProjection.Set_TargetScale(Value: Double);
begin
   SetCurrentProjection;
   AxTargetScale:=Value;
   CurrProjection.Set_TargetScale(Value);
end;

function TAxGisProjection.Get_UsedInputCoordSystem: TxCoordSystem;
begin
   result:=AxUsedInputCoordinateSystem;
end;

procedure TAxGisProjection.Set_UsedInputCoordSystem(Value: TxCoordSystem);
begin
   SetCurrentProjection;
   AxUsedInputCoordinateSystem:=Value;
   CurrProjection.Set_UsedInputCoordinateSystem(Value);
end;

function TAxGisProjection.Get_UsedOutputCoordSystem: TxCoordSystem;
begin
   result:=AxUsedOutputCoordinateSystem;
end;

procedure TAxGisProjection.Set_UsedOutputCoordSystem(Value: TxCoordSystem);
begin
   SetCurrentProjection;
   AxUsedOutputCoordinateSystem:=Value;
   CurrProjection.Set_UsedOutputCoordinateSystem(Value);
end;

procedure TAxGisProjection.Calculate(var X, Y: Double);
var StrFileObj:ClassStrFile;
begin
   if FileExists(AxWorkingPath+'COORD32.DLL') then
   begin
      SetCurrentProjection;
      if not CurrProjection.Calculate(X, Y) then
      begin
         StrFileObj:=ClassStrFile.Create;
         StrFileObj.CreateList(AxWorkingPath, AxLngCode, 'GISPRO');
         if FEvents <> nil then begin
                FEvents.OnWarning(StrFileObj.Assign(16)); // Warning: Coordinates over limit
         end;
         StrFileObj.Destroy;
      end;
      if (not ShowedNoProjectionWarning) and (not CurrProjection.DoProjection) then
      begin
         StrFileObj:=ClassStrFile.Create;
         StrFileObj.CreateList(AxWorkingPath, AxLngCode, 'GISPRO');
         if FEvents <> nil then begin
                FEvents.OnWarning(StrFileObj.Assign(17)); // Warning: No Projetion will be used for calculation
         end;
         StrFileObj.Destroy;
         ShowedNoProjectionWarning:=TRUE;
      end;
   end
   else
   begin
      FEvents.OnError('Error: Coord32.dll not found!');
   end;
end;

procedure TAxGisProjection.SetupByDialog;
var
AHandle: Integer;
begin
   // projection should be set by using the dialog
   if FileExists(AxWorkingPath+'COORD32.DLL') then
   begin
      SetCurrentProjection;
      GeoWnd:=TGeoWnd.Create(Application);
      GeoWnd.LngCode:=AxLngCode;
      GeoWnd.WorkingPath:=AxWorkingPath;
      GeoWnd.CurrProjection:=CurrProjection;
      GeoWnd.ShowOnlySource:=AxShowOnlySource;
      Windows.SetParent(GeoWnd.Handle,AHandle);
      GeoWnd.ShowModal;
      GeoWnd.Destroy;
      GetCurrentProjection;
   end
   else
   begin
      FEvents.OnError('Error: Coord32.dll not found!');
   end;
end;

function TAxGisProjection.Get_ShowOnlySource: WordBool;
begin
   result:=AxShowOnlySource;
end;

procedure TAxGisProjection.Set_ShowOnlySource(Value: WordBool);
begin
   SetCurrentProjection;
   AxShowOnlySource:=Value;
end;

procedure TAxGisProjection.GetCurrentProjection;
begin
   // get source projection
   AxSourceCoordinateSystem:=CurrProjection.Get_SourceCoordinateSystem;
   AxSourceProjection:=CurrProjection.Get_SourceProjection;
   AxSourceDate:=CurrProjection.Get_SourceDate;
   AxSourceProjSettings:=CurrProjection.Get_SourceProjSettings;
   AxSourceXOffset:=CurrProjection.Get_SourceXOffset;
   AxSourceYOffset:=CurrProjection.Get_SourceYOffset;
   AxSourceScale:=CurrProjection.Get_SourceScale;
   AxUsedInputCoordinateSystem:=CurrProjection.Get_UsedInputCoordinateSystem;
   // get target projection
   AxTargetCoordinateSystem:=CurrProjection.Get_TargetCoordinateSystem;
   AxTargetProjection:=CurrProjection.Get_TargetProjection;
   AxTargetDate:=CurrProjection.Get_TargetDate;
   AxTargetProjSettings:=CurrProjection.Get_TargetProjSettings;
   AxTargetXOffset:=CurrProjection.Get_TargetXOffset;
   AxTargetYOffset:=CurrProjection.Get_TargetYOffset;
   AxTargetScale:=CurrProjection.Get_TargetScale;
   AxUsedOutputCoordinateSystem:=CurrProjection.Get_UsedOutputCoordinateSystem;
end;

procedure TAxGisProjection.SetCurrentProjection;
begin
   // set source projection
   CurrProjection.Set_SourceCoordinateSystem(AxSourceCoordinateSystem);
   CurrProjection.Set_SourceProjection(AxSourceProjection);
   CurrProjection.Set_SourceDate(AxSourceDate);
   CurrProjection.Set_SourceProjSettings(AxSourceProjSettings);
   CurrProjection.Set_SourceXOffset(AxSourceXOffset);
   CurrProjection.Set_SourceYOffset(AxSourceYOffset);
   CurrProjection.Set_SourceScale(AxSourceScale);
   CurrProjection.Set_UsedInputCoordinateSystem(AxUsedInputCoordinateSystem);

   // set target projection
   CurrProjection.Set_TargetCoordinateSystem(AxTargetCoordinateSystem);
   CurrProjection.Set_TargetProjection(AxTargetProjection);
   CurrProjection.Set_TargetDate(AxTargetDate);
   CurrProjection.Set_TargetProjSettings(AxTargetProjSettings);
   CurrProjection.Set_TargetXOffset(AxTargetXOffset);
   CurrProjection.Set_TargetYOffset(AxTargetYOffset);
   CurrProjection.Set_TargetScale(AxTargetScale);
   CurrProjection.Set_UsedOutputCoordinateSystem(AxUsedOutputCoordinateSystem);
end;

procedure TAxGisProjection.IAxGisProjection__Set_Font(
  var Value: IFontDisp);
begin
  SetOleFont(FDelphiControl.Font, Value);
end;

procedure TAxGisProjection.IAxGisProjection_Set_Font(
  const Value: IFontDisp);
begin
  SetOleFont(FDelphiControl.Font, Value);
end;

initialization
  TActiveXControlFactory.Create(
    ComServer,
    TAxGisProjection,
    TPanel,
    Class_AxGisProjection,
    1,
    '',
    OLEMISC_SIMPLEFRAME or OLEMISC_ACTSLIKELABEL,
    tmApartment);
end.
