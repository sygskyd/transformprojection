library AxGisPro;

uses
  ComServ,
  AxGisPro_TLB in 'AxGisPro_TLB.pas',
  AxGisProImpl in 'AxGisProImpl.pas' {AxGisProjection: CoClass},
  GeoDlg in 'GeoDlg.pas' {GeoWnd},
  Strfile in 'Strfile.pas',
  ProjectionCalculation in 'ProjectionCalculation.pas';

{$E ocx}

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
