unit GeoDlg;

interface

uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   StdCtrls, ExtCtrls, Buttons, Strfile, ComCtrls, inifiles, ProjectionCalculation, UTMDlg;

type
  TGeoWnd = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    PageControl1: TPageControl;
    SourceTabSheet: TTabSheet;
    GroupBox3: TGroupBox;
    SourceCarthesicRB: TRadioButton;
    SourceGeodeticRB: TRadioButton;
    Panel8: TPanel;
    Panel9: TPanel;
    SourceProjektionCB: TComboBox;
    Panel10: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    SourceMeridianCB: TComboBox;
    SourceDatumCB: TComboBox;
    DestTabSheet: TTabSheet;
    GroupBox2: TGroupBox;
    TargetCarthesicRB: TRadioButton;
    TargetGeodeticRB: TRadioButton;
    Panel4: TPanel;
    Panel5: TPanel;
    TargetProjektionCB: TComboBox;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    TargetMeridianCB: TComboBox;
    TargetDatumCB: TComboBox;
    GisNoProCB: TCheckBox;
    Panel1: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    SourceXOffsetEdit: TEdit;
    SourceYOffsetEdit: TEdit;
    Label10: TLabel;
    SourceScaleCB: TComboBox;
    UseProjCB: TCheckBox;
    ProjCB: TComboBox;
    NewProjBtn: TButton;
    Panel2: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    TargetXOffsetEdit: TEdit;
    TargetYOffsetEdit: TEdit;
    TargetScaleCB: TComboBox;
    ResetBtn: TButton;
    ButtonUTM: TButton;
    procedure FormCreate(Sender: TObject);
    procedure TargetProjektionCBChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GisnoproCBClick(Sender: TObject);
    procedure SourceProjektionCBChange(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure UseProjCBClick(Sender: TObject);
    procedure NewProjBtnClick(Sender: TObject);
    procedure ProjCBChange(Sender: TObject);
    procedure SourceXOffsetEditChange(Sender: TObject);
    procedure SourceYOffsetEditChange(Sender: TObject);
    procedure SourceScaleCBChange(Sender: TObject);
    procedure TargetXOffsetEditChange(Sender: TObject);
    procedure TargetYOffsetEditChange(Sender: TObject);
    procedure TargetScaleCBChange(Sender: TObject);
    procedure TargetDatumCBChange(Sender: TObject);
    procedure SourceDatumCBChange(Sender: TObject);
    procedure ResetBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ButtonUTMClick(Sender: TObject);
  private
    // private declarations
    StrFileObj    :ClassStrFile;  // stringfile object
    procedure FillProjCB;
    procedure DisplayStoredProjection;
    function  CheckWindow:boolean;
    procedure DisplayCurrentProjection;
  public
    // public declarations
    WorkingPath   :string;      // current working-path
    LngCode       :string;      // current lng-Code
    ShowOnlySource:boolean;     // Flag that shows if only the Source-Projection should be displayed
    CurrProjection:TProjectionCalculation; // object that stores the current projection
  end;

var GeoWnd: TGeoWnd;

implementation

uses WinOSInfo;

{$R *.DFM}

procedure TGeoWnd.FormCreate(Sender: TObject);
begin
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   ShowOnlySource:=FALSE;
   StrFileObj:=nil;
   CurrProjection:=nil;
end;

procedure TGeoWnd.TargetProjektionCBChange(Sender: TObject);
var DefaultDate:string;
    Datums     :TStrings;
    Meridians  :TStrings;
    i          :integer;
begin
   Datums:=TStringList.Create;
   Meridians:=TStringList.Create;
   CurrProjection.GetProjectionItems(TargetProjektionCB.Text, Datums, Meridians, DefaultDate);
   // copy the Datums to the TargetDatumCb
   TargetDatumCB.clear;
   for i:=0 to Datums.Count-1 do
      TargetDatumCb.Items.Add(Datums[i]);
   Datums.Destroy;
   // copy the Merdians to the TargetMeridianCb
   TargetMeridianCB.Items.Clear;
   for i:=0 to Meridians.Count-1 do
      TargetMeridianCB.Items.Add(Meridians[i]);
   Meridians.Destroy;

   TargetDatumCB.itemindex := TargetDatumCB.items.IndexOf(DefaultDate); // set the default date
   TargetMeridianCB.ItemIndex := 0;
   CheckWindow;
end;

procedure TGeoWnd.DisplayCurrentProjection;
var Projections:TStrings;
    i:integer;
begin
   // Fill the SourceProjektionCB and TargetProjektionCB
   Projections:=TStringList.Create;
   CurrProjection.GetProjections(Projections);
   SourceProjektionCB.clear;
   TargetProjektionCB.clear;
   for i:=0 to Projections.Count-1 do
   begin
      SourceProjektionCB.Items.Add(Projections[i]);
      TargetProjektionCB.Items.Add(Projections[i]);
   end;
   Projections.Destroy;

   SourceProjektionCB.itemindex := 0;
   SourceProjektionCBChange(nil);

   TargetProjektionCB.itemindex := 0;
   TargetProjektionCBChange(nil);

   GisnoproCB.checked:=true;
   SourceMeridianCB.enabled:=false;
   SourceDatumCB.enabled:=false;
   TargetMeridianCB.enabled:=false;
   TargetDatumCB.enabled:=false;

   UseProjCB.Checked:=false;
   // set display settings
   if CurrProjection.Get_SourceCoordinateSystem = cs_Carthesic then SourceCarthesicRB.Checked:=true;
   if CurrProjection.Get_SourceCoordinateSystem = cs_Geodetic  then SourceGeodeticRB.Checked:=true;

   if CurrProjection.Get_TargetCoordinateSystem = cs_Carthesic then TargetCarthesicRB.Checked:=true;
   if CurrProjection.Get_TargetCoordinateSystem = cs_Geodetic  then TargetGeodeticRB.Checked:=true;

   if (CurrProjection.Get_SourceProjSettings <> '') and (CurrProjection.Get_SourceProjSettings <> 'NONE') then begin
        SourceProjektionCB.ItemIndex:=SourceProjektionCB.Items.IndexOf(CurrProjection.Get_SourceProjSettings);
   end
   else begin
        CurrProjection.DoProjection:=false;
   end;

   if not ShowOnlySource then begin
      if (CurrProjection.Get_TargetProjSettings <> '') and (CurrProjection.Get_TargetProjSettings <> 'NONE') then begin
        TargetProjektionCB.ItemIndex:=TargetProjektionCB.Items.IndexOf(CurrProjection.Get_TargetProjSettings);
      end
      else begin
        CurrProjection.DoProjection:=false;
      end;
   end;

   // fill the projection comboboxes
   SourceProjektionCBChange(nil);
   TargetProjektionCBChange(nil);

   if (CurrProjection.Get_SourceProjection   <> '') and (CurrProjection.Get_SourceProjection <> 'NONE') then begin
        SourceMeridianCB.ItemIndex:=SourceMeridianCB.Items.IndexOf(CurrProjection.Get_SourceProjection);
   end
   else begin
        CurrProjection.DoProjection:=false;
   end;

   if not ShowOnlySource then begin
      if (CurrProjection.Get_TargetProjection   <> '') and (CurrProjection.Get_TargetProjection <> 'NONE') then begin
        TargetMeridianCB.ItemIndex:=TargetMeridianCB.Items.IndexOf(CurrProjection.Get_TargetProjection);
      end
      else begin
        CurrProjection.DoProjection:=false;
      end;
   end;

   if (CurrProjection.Get_SourceDate <> '') and (CurrProjection.Get_SourceDate <> 'NONE') then begin
        SourceDatumCB.ItemIndex:=SourceDatumCB.Items.IndexOf(CurrProjection.Get_SourceDate);
   end
   else begin
        CurrProjection.DoProjection:=false;
   end;
   
   if not ShowOnlySource then begin
        if (CurrProjection.Get_TargetDate <> '') and (CurrProjection.Get_TargetDate <> 'NONE') then begin
                TargetDatumCB.ItemIndex:=TargetDatumCB.Items.IndexOf(CurrProjection.Get_TargetDate);
        end
        else begin
                CurrProjection.DoProjection:=false;
        end;
   end;

   SourceXOffsetEdit.Text:=floattostr(CurrProjection.Get_SourceXOffset);
   SourceYOffsetEdit.Text:=floattostr(CurrProjection.Get_SourceYOffset);
   SourceScaleCB.Text:=floattostr(CurrProjection.Get_SourceScale);

   TargetXOffsetEdit.Text:=floattostr(CurrProjection.Get_TargetXOffset);
   TargetYOffsetEdit.Text:=floattostr(CurrProjection.Get_TargetYOffset);
   TargetScaleCB.Text:=floattostr(CurrProjection.Get_TargetScale);

   if ShowOnlySource then
   begin
      if not CurrProjection.IsValidProjection(CurrProjection.Get_SourceDate, CurrProjection.Get_SourceProjection) then
         GisNoProCB.Checked:=true
      else
         GisNoProCB.Checked:=false;
   end
   else
   begin
      if CurrProjection.IsValidProjection(CurrProjection.Get_SourceDate, CurrProjection.Get_SourceProjection) and
         CurrProjection.IsValidProjection(CurrProjection.Get_TargetDate, CurrProjection.Get_TargetProjection) then
         GisNoProCB.Checked:=FALSE
      else
         GisNoProCB.Checked:=TRUE;
   end;
   CheckWindow;
end;

procedure TGeoWnd.FormActivate(Sender: TObject);
begin
   PageControl1.ActivePage:=SourceTabSheet;
end;

procedure TGeoWnd.GisnoproCBClick(Sender: TObject);
begin
   if GisnoproCB.checked then
   begin
      TargetProjektionCB.Enabled:=false;
      TargetDatumCB.enabled:=false;
      TargetMeridianCB.enabled:=false;

      SourceProjektionCB.Enabled:=false;
      SourceDatumCB.enabled:=false;
      SourceMeridianCB.enabled:=false;
   end
   else
   begin
      TargetProjektionCB.Enabled:=true;
      TargetDatumCB.enabled:=true;
      TargetMeridianCB.enabled:=true;

      SourceProjektionCB.Enabled:=true;
      SourceDatumCB.enabled:=true;
      SourceMeridianCB.enabled:=true;
   end;
   CheckWindow;
end;

procedure TGeoWnd.SourceProjektionCBChange(Sender: TObject);
var DefaultDate:string;
    Datums     :TStrings;
    Meridians  :TStrings;
    i          :integer;
begin
   Datums:=TStringList.Create;
   Meridians:=TStringList.Create;
   CurrProjection.GetProjectionItems(SourceProjektionCB.Text, Datums, Meridians, DefaultDate);
   // copy the Datums to the TargetDatumCb
   SourceDatumCB.clear;
   for i:=0 to Datums.Count-1 do
      SourceDatumCB.Items.Add(Datums[i]);
   Datums.Destroy;
   // copy the Merdians to the TargetMeridianCb
   SourceMeridianCB.Items.Clear;
   for i:=0 to Meridians.Count-1 do
      SourceMeridianCB.Items.Add(Meridians[i]);
   Meridians.Destroy;

   SourceDatumCB.itemindex := SourceDatumCB.items.IndexOf(DefaultDate); // set the default date
   SourceMeridianCB.ItemIndex := 0;
   CheckWindow;
end;

procedure TGeoWnd.OKBtnClick(Sender: TObject);
var count:integer;
    dummydouble:double;
begin
   if SourceCarthesicRB.Checked then CurrProjection.Set_SourceCoordinateSystem(cs_Carthesic);
   if SourceGeodeticRB.Checked  then CurrProjection.Set_SourceCoordinateSystem(cs_Geodetic);
   if TargetCarthesicRB.Checked then CurrProjection.Set_TargetCoordinateSystem(cs_Carthesic);
   if TargetGeodeticRB.Checked  then CurrProjection.Set_TargetCoordinateSystem(cs_Geodetic);

   val(SourceXOffsetEdit.Text, dummydouble, count);   CurrProjection.Set_SourceXOffset(dummydouble);
   val(SourceYOffsetEdit.Text, dummydouble, count);   CurrProjection.Set_SourceYOffset(dummydouble);
   val(SourceScaleCB.Text, dummydouble, count);       CurrProjection.Set_SourceScale(dummydouble);

   val(TargetXOffsetEdit.Text, dummydouble, count);  CurrProjection.Set_TargetXOffset(dummydouble);
   val(TargetYOffsetEdit.Text, dummydouble, count);  CurrProjection.Set_TargetYOffset(dummydouble);
   val(TargetScaleCB.Text, dummydouble, count);      CurrProjection.Set_TargetScale(dummydouble);

   if GisNoProCB.Checked then
   begin
      CurrProjection.DoProjection:=false;
      CurrProjection.Set_SourceProjSettings('NONE');
      CurrProjection.Set_SourceProjection('NONE');
      CurrProjection.Set_SourceDate('NONE');
      CurrProjection.Set_TargetProjSettings('NONE');
      CurrProjection.Set_TargetProjection('NONE');
      CurrProjection.Set_TargetDate('NONE');
   end
   else
   begin
      CurrProjection.Set_SourceProjSettings(SourceProjektionCB.Text);
      if ShowOnlySource then CurrProjection.Set_TargetProjSettings('NONE') else CurrProjection.Set_TargetProjSettings(TargetProjektionCB.Text);

      CurrProjection.Set_SourceProjection(SourceMeridianCB.Text);
      if ShowOnlySource then CurrProjection.Set_TargetProjection('NONE') else CurrProjection.Set_TargetProjection(TargetMeridianCB.Text);

      CurrProjection.Set_SourceDate(SourceDatumCB.Text);
      if ShowOnlySource then CurrProjection.Set_TargetDate('NONE') else CurrProjection.Set_TargetDate(TargetDatumCB.Text);
      CurrProjection.SetupProjection;
   end;
end;

procedure TGeoWnd.UseProjCBClick(Sender: TObject);
begin
   if UseProjCB.Checked then
   begin
      ProjCB.Enabled:=true;
      FillProjCB;
   end
   else
   begin
      ProjCB.Items.Clear;
      ProjCB.Text:='';
      NewProjBtn.Enabled:=false;
      ProjCB.Enabled:=false;
   end;
   CheckWindow;
end;

procedure TGeoWnd.FillProjCB;
var Datei:Textfile;
    inifile:TInifile;
    NumItems,i:integer;
    ItemName:string;
begin
   // first it will be checked if a Projection.ini already exists
   if not FileExists(OSInfo.LocalAppDataDir + 'Projection.ini') then
   begin
      // a new Projection.ini has to be created
      AssignFile(Datei,OSInfo.LocalAppDataDir + 'Projection.ini');
      Rewrite(Datei);
      writeln(Datei, '[SETTINGS]');
      writeln(Datei, 'NumItems=0');
      writeln(Datei, 'LastItem=');
      writeln(Datei, '[ITEMS]');
      Closefile(Datei);
   end
   else
   begin
      // first the items will be read out
      inifile:=Tinifile.Create(OSInfo.LocalAppDataDir + 'Projection.ini');
      NumItems:=inifile.ReadInteger('SETTINGS','NumItems',0);
      ProjCB.Items.Clear;
      // now the items will be read out
      for i:=1 to NumItems do
      begin
         ItemName:=inifile.ReadString('ITEMS','Item'+inttostr(i),'');
         ProjCB.Items.Add(ItemName);
      end;
      // now the last used item will be set
      ProjCB.Text:=inifile.ReadString('SETTINGS','LastItem','');
      inifile.free;
      DisplayStoredProjection;
   end;
end;

procedure TGeoWnd.DisplayStoredProjection;
var inifile:TInifile;
    UsedSection,strvalue:string;
    LastItem,i:integer;
    exists:boolean;
    ItemName:string;
begin
   // the settings that are stored to the entry that is set in the
   // ProjCB.Text will be displayed
   UsedSection:=ProjCB.Text;
   if UsedSection = '' then
      exit;

   inifile:=Tinifile.Create(OSInfo.LocalAppDataDir + 'Projection.ini');

   // check if the UsedSection exists
   Lastitem:=inifile.ReadInteger('SETTINGS','NumItems',0);

   //first it will be checked if this item already exists
   exists:=false;
   for i:=1 to LastItem do
   begin
      ItemName:=inifile.ReadString('ITEMS','Item'+inttostr(i),'');
      if ItemName = UsedSection then
         exists:=true;
   end;

   if not exists then
   begin
      inifile.free;
      exit;
   end;

   strvalue:=inifile.ReadString(UsedSection,'SourceCoordinateSystem','');
   if strtoint(strvalue) = cs_Carthesic then SourceCarthesicRB.checked:=true;
   if strtoint(strvalue) = cs_Geodetic  then SourceGeodeticRB.checked:=true;

   strvalue:=inifile.ReadString(UsedSection,'DoProjection','');
   if strvalue = 'TRUE' then
      GisNoProCB.Checked:=false
   else
      GisNoProCB.Checked:=true;

   strvalue:=inifile.ReadString(UsedSection,'SourceProjection','');
   i:=SourceProjektionCB.Items.IndexOf(strvalue);
   if i >= 0 then begin
        SourceProjektionCB.ItemIndex:=i;
        SourceProjektionCBChange(nil);
   end;

   strvalue:=inifile.ReadString(UsedSection,'SourceMeridian','');
   i:=SourceMeridianCB.Items.IndexOf(strvalue);
   if i >= 0 then begin
        SourceMeridianCB.ItemIndex:=i;
   end;

   strvalue:=inifile.ReadString(UsedSection,'SourceDatum','');
   i:=SourceDatumCB.Items.IndexOf(strvalue);
   if i >= 0 then begin
        SourceDatumCB.ItemIndex:=i;
   end;

   strvalue:=inifile.ReadString(UsedSection,'SourceXOffset','');
   SourceXOffsetEdit.Text:=strvalue;

   strvalue:=inifile.ReadString(UsedSection,'SourceYOffset','');
   SourceYOffsetEdit.Text:=strvalue;

   strvalue:=inifile.ReadString(UsedSection,'SourceScale','');
   SourceScaleCB.Text:=strvalue;

   strvalue:=inifile.ReadString(UsedSection,'TargetCoordinateSystem','');

   if strtoint(strvalue) = cs_Carthesic then TargetCarthesicRB.checked:=true;
   if strtoint(strvalue) = cs_Geodetic  then TargetGeodeticRB.checked:=true;

   strvalue:=inifile.ReadString(UsedSection,'TargetProjektion','');
   i:=TargetProjektionCB.Items.IndexOf(strvalue);
   if i >= 0 then begin
        TargetProjektionCB.ItemIndex:=i;
        TargetProjektionCBChange(nil);
   end;

   strvalue:=inifile.ReadString(UsedSection,'TargetMeridian','');
   i:=TargetMeridianCB.Items.IndexOf(strvalue);
   if i >= 0 then begin
        TargetMeridianCB.ItemIndex:=i;
   end;

   strvalue:=inifile.ReadString(UsedSection,'TargetDatum','');
   i:=TargetDatumCB.Items.IndexOf(strvalue);
   if i >= 0 then begin
        TargetDatumCB.ItemIndex:=i;
   end;

   strvalue:=inifile.ReadString(UsedSection,'TargetXOffset','');
   TargetXOffsetEdit.Text:=strvalue;

   strvalue:=inifile.ReadString(UsedSection,'TargetYOffset','');
   TargetYOffsetEdit.Text:=strvalue;

   strvalue:=inifile.ReadString(UsedSection,'TargetScale','');
   TargetScaleCB.Text:=strvalue;

   inifile.WriteString('SETTINGS','LastItem',UsedSection);
   inifile.free;
end;

procedure TGeoWnd.NewProjBtnClick(Sender: TObject);
var Lastitem:integer;
    inifile:TInifile;
    UsedSection:string;
    i:integer;
    ItemName:string;
    exists:boolean;
begin
   // now new projectionsettings will be stored
   // first the number of the item as to be evaluated
   if ProjCB.Text='' then
      exit;

   UsedSection:=ProjCB.Text;
   inifile:=Tinifile.Create(OSInfo.LocalAppDataDir + 'Projection.ini');
   Lastitem:=inifile.ReadInteger('SETTINGS','NumItems',0);

   //first it will be checked if this item already exists
   exists:=false;
   for i:=1 to LastItem do
   begin
      ItemName:=inifile.ReadString('ITEMS','Item'+inttostr(i),'');
      if ItemName = UsedSection then
         exists:=true;
   end;

   if not exists then
      Lastitem:=Lastitem+1;

   // write settings to ini-file
   inifile.WriteString('ITEMS','Item'+inttostr(LastItem),UsedSection);

   // now the entrys will be written into the ini-file
   if SourceCarthesicRB.Checked then inifile.WriteString(UsedSection,'SourceCoordinateSystem',inttostr(cs_Carthesic));
   if SourceGeodeticRB.Checked  then inifile.WriteString(UsedSection,'SourceCoordinateSystem',inttostr(cs_Geodetic));

   if GisNoProCB.Checked then inifile.WriteString(UsedSection,'DoProjection','FALSE')
   else inifile.WriteString(UsedSection,'DoProjection','TRUE');

   inifile.WriteString(UsedSection,'SourceProjection',SourceProjektionCB.Text);
   inifile.WriteString(UsedSection,'SourceMeridian',SourceMeridianCB.Text);
   inifile.WriteString(UsedSection,'SourceDatum',SourceDatumCB.Text);
   inifile.WriteString(UsedSection,'SourceXOffset',SourceXOffsetEdit.Text);
   inifile.WriteString(UsedSection,'SourceYOffset',SourceYOffsetEdit.Text);
   inifile.WriteString(UsedSection,'SourceScale',SourceScaleCB.Text);

   if TargetCarthesicRB.Checked then inifile.WriteString(UsedSection,'TargetCoordinateSystem',inttostr(cs_Carthesic));
   if TargetGeodeticRB.Checked  then inifile.WriteString(UsedSection,'TargetCoordinateSystem',inttostr(cs_Geodetic));

   inifile.WriteString(UsedSection,'TargetProjektion',TargetProjektionCB.Text);
   inifile.WriteString(UsedSection,'TargetMeridian',TargetMeridianCB.Text);
   inifile.WriteString(UsedSection,'TargetDatum',TargetDatumCB.Text);
   inifile.WriteString(UsedSection,'TargetXOffset',TargetXOffsetEdit.Text);
   inifile.WriteString(UsedSection,'TargetYOffset',TargetYOffsetEdit.Text);
   inifile.WriteString(UsedSection,'TargetScale',TargetScaleCB.Text);

   inifile.WriteInteger('SETTINGS','NumItems',LastItem);
   inifile.WriteString('SETTINGS','LastItem',UsedSection);
   inifile.free;
   FillProjCB;
end;

procedure TGeoWnd.ProjCBChange(Sender: TObject);
begin
   if ProjCB.Text = '' then
      exit;
   DisplayStoredProjection;
end;

// ------------------------------------------------------------------------
// NAME: CheckWindow
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to check if the current-settings in the
//              window are correct
//
// PARAMETERS : NONE
//
// RETURNVALUE: TRUE  -> Window-Settings are correct
//              FALSE -> Window-Settings are not correct
// ------------------------------------------------------------------------
function TGeoWnd.CheckWindow:boolean;
var dummy:double;
    count:integer;
    retVal:boolean;
begin
   retVal:=true;

   val(SourceXOffsetEdit.Text, dummy, count); if count <> 0 then retVal:=false;
   val(SourceYOffsetEdit.Text, dummy, count); if count <> 0 then retVal:=false;
   val(SourceScaleCB.Text, dummy, count); if count <> 0 then retVal:=false;

   if not ShowOnlySource then
   begin
      val(TargetXOffsetEdit.Text, dummy, count); if count <> 0 then retVal:=false;
      val(TargetYOffsetEdit.Text, dummy, count); if count <> 0 then retVal:=false;
      val(TargetScaleCB.Text, dummy, count); if count <> 0 then retVal:=false;
   end;

   if not GisNoProCB.Checked then
   begin
      // check the source-projection and date
      if not CurrProjection.IsValidProjection(SourceDatumCb.Text, SourceMeridianCB.Text) then retVal:=FALSE;
      if not ShowOnlySource then
      begin
         // check the target-projection and date
         if not CurrProjection.IsValidProjection(TargetDatumCB.Text, TargetMeridianCB.Text) then retVal:=FALSE;
      end;
   end;

   if not retVal then
   begin
      OkBtn.Enabled:=false;
      if UseProjCB.Checked then
         NewProjBtn.Enabled:=false;
   end
   else
   begin
      OkBtn.Enabled:=true;
      if UseProjCB.Checked then
         NewProjBtn.Enabled:=true;
   end;
   result:=retVal;
end;

procedure TGeoWnd.SourceXOffsetEditChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TGeoWnd.SourceYOffsetEditChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TGeoWnd.SourceScaleCBChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TGeoWnd.TargetXOffsetEditChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TGeoWnd.TargetYOffsetEditChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TGeoWnd.TargetScaleCBChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TGeoWnd.TargetDatumCBChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TGeoWnd.SourceDatumCBChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TGeoWnd.ResetBtnClick(Sender: TObject);
begin
   DisplayCurrentProjection;
end;


procedure TGeoWnd.FormShow(Sender: TObject);
begin
   StrfileObj:=ClassStrFile.Create;
   StrfileObj.CreateList(WorkingPath, LngCode, 'GISPRO');
   Self.Caption:=StrfileObj.Assign(1);               { Setzen der Projektion }
   DestTabSheet.Caption:=StrfileObj.Assign(2);       { Projekt Projektion: }
   SourceTabSheet.Caption:=StrfileObj.Assign(11);    { Externe Projektion: }
   TargetCarthesicRB.Caption:=StrfileObj.Assign(3);  { karthesisch }
   SourceCarthesicRB.Caption:=StrfileObj.Assign(3);  { karthesisch }
   TargetGeodeticRB.Caption:=StrfileObj.Assign(4);   { geodätisch  }
   SourceGeodeticRB.Caption:=StrfileObj.Assign(4);   { geodätisch  }
   GisnoProCB.Caption:=StrfileObj.Assign(5);         { Keine Projektion }
   Label1.Caption:=StrfileObj.Assign(6);             { Projektion: }
   Label2.Caption:=StrfileObj.Assign(7);             { Datum     : }
   Label5.Caption:=StrfileObj.Assign(6);             { Projektion: }
   Label6.Caption:=StrfileObj.Assign(7);             { Datum     : }

   Label3.Caption:=StrfileObj.Assign(8);             { X-Offset:   }
   Label7.Caption:=StrfileObj.Assign(8);             { X-Offset:   }
   Label4.Caption:=StrfileObj.Assign(9);             { Y-Offset:   }
   Label8.Caption:=StrfileObj.Assign(9);             { Y-Offset:   }
   Label9.Caption:=StrfileObj.Assign(10);            { Skalierung:   }
   Label10.Caption:=StrfileObj.Assign(10);           { Skalierung:   }


   OkBtn.Caption:=StrfileObj.Assign(12);             { OK }
   CancelBtn.Caption:=StrfileObj.Assign(13);         { Abbruch }
   NewProjBtn.Hint:=StrfileObj.Assign(14);           { store projection }
   UseProjCB.Caption:=StrfileObj.Assign(15);         { use stored projection settings }

   Self.Update;
   GisnoproCB.checked:=true;

   ProjCB.Enabled:=false;
   ProjCB.Text:='';
   NewProjBtn.Enabled:=false;
   OkBtn.Enabled:=true;

   SourceScaleCB.Items.Add('1');
   SourceScaleCB.Items.Add('2');
   SourceScaleCB.Items.Add('10');
   SourceScaleCB.Items.Add('100');
   SourceScaleCB.Text:='1';

   TargetScaleCB.Items.Add('1');
   TargetScaleCB.Items.Add('2');
   TargetScaleCB.Items.Add('10');
   TargetScaleCB.Items.Add('100');
   TargetScaleCB.Text:='1';

   // check if the Destination-Tabsheet may be selected
   if (ShowOnlySource) then
   begin
      DestTabSheet.TabVisible:=FALSE;
      SourceTabSheet.Caption:=StrfileObj.Assign(18); // Projection
   end
   else
   begin
      DestTabSheet.TabVisible:=TRUE;
      SourceTabSheet.Caption:=StrfileObj.Assign(11);    { Externe Projektion: }
   end;

   ButtonUTM.Visible:=ShowOnlySource;
   PageControl1.ActivePage:=SourceTabSheet;
   DisplayCurrentProjection;
   CheckWindow;
end;

procedure TGeoWnd.FormDestroy(Sender: TObject);
begin
   if (StrFileObj <> nil) then
      StrFileObj.Destroy;
   CurrProjection:=nil;
end;

procedure TGeoWnd.ButtonUTMClick(Sender: TObject);
var
i: Integer;
s: AnsiString;
begin
        UTMForm:=TUTMForm.Create(Self);
        try
                UTMForm.ShowModal;
                if UTMForm.UTMProjection <> '' then begin

                        GisNoProCB.Checked:=False;
                        GisNoProCBClick(nil);

                        s:='Universal Transverse Mercator';
                        i:=SourceProjektionCB.Items.IndexOf(s);
                        if i > -1 then begin
                                SourceProjektionCB.ItemIndex:=i;
                                SourceProjektionCBChange(nil);
                        end;

                        s:=UTMForm.UTMProjection;
                        i:=SourceMeridianCB.Items.IndexOf(s);
                        if i > -1 then begin
                                SourceMeridianCB.ItemIndex:=i;
                        end;

                        s:='WGS84';
                        i:=SourceDatumCB.Items.IndexOf(s);
                        if i > -1 then begin
                                SourceDatumCB.ItemIndex:=i;
                                SourceDatumCBChange(nil);
                        end;
                end;
        finally
                UTMForm.Free;
        end;
end;

end.
