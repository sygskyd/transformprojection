unit ProjectionCalculation;

interface

uses  Windows, SysUtils, Classes;

const Cnamelength = 60;
      cs_carthesic = 0;
      cs_geodetic  = 1;

type Tszname =array[0..Cnamelength] of char;
     pszname = ^tszname;
type telipsoid = record
    SZNAME : Tszname;                 // name des Elipsoids
    ae : double;                      // gro�e Halbachse
    fe : double;                      // inverse flattening
    pHint : pchar;                    // optionaler hinweistext
    end;
    pelipsoid = ^telipsoid;

type tdatum = record
    szname   : Tszname;               // name des Datums
    elipsoid : pelipsoid;             // zugeh�riges Elipsoid
    IDMethod   : array[0..2] of char; // Konvertierungsmethode 'MOL' : Molodensky, '7PA' : 7 parameter
    params : array of double;         // shift parameter 4 bei molodensky, 8 bei _7params
    pHint : pchar;                    // optionaler hinweistext
    end;
    pdatum = ^tdatum;

type tprojection = record
    szname : Tszname;                 // name der Projektion
    IDProc : array[0..2] of char ;    // 'TMK' := transverse Mercator;
                                      // 'GDG' := geographic Lat/Lon;
                                      // 'CHC' : swiss coordiante system ;
    projection_params : array of double;
                                      // bei TMK 5 :Origin_lat,origin_lon,false_northing,false_easting,sacaling
                                      // bei GDG NIL
                                      // bei CHC NIL
    end;
    pprojection = ^tprojection;

type tprojectiongroup = record
      szname : Tszname;              // name der Projektionsgruppe
      datums : array of pdatum;      // liste aller zugelassenen Datums, Nil falls alle zugelassen sind
      default_datum : pdatum;        // zeiger auf das default_datum
      Projections : array of pprojection;  // Projektionen die dieser Gruppe zugeordent sind
    end;
    pprojectiongroup = ^tprojectiongroup;

type tcoordsystem = record
    szname : Tszname;                 // name des Koordinatensystems ( z.Z nicht benutzt)
    projection : pprojection;         // zeiger auf zugeh�rige Mathematische Projektion
    datum  : pdatum;                  // zeiger auf zugeh�riges Datum
    end;
    pcoordsystem = ^tcoordsystem;

// callbackfunktion f�r aufz�hlungs-funktionen
// p ist ist abh�ngig von aufz�hlungsfunktion
// data wird lediglich von den Aufz�hlungsfunktionen durchgeschleust
type tcallback_function = procedure(p:pointer;data :pointer);

// functions for communicatin with coord32.dll
type Tcreate_Transform=function(var p : pointer):integer;stdcall; // constructor function for coord32.dll
type Tfree_transform=function(var p : pointer):integer;stdcall;   // destructor for coord32.dll
type Tset_FromSystem=function(p:pointer;projection:pprojection;datum :pdatum):integer;stdcall; // set From_System
type Tset_ToSytem=function(p:pointer;projection:pprojection;datum :pdatum):integer;stdcall; // set To_System
type TExchange_Systems=function(p : pointer): integer; stdcall; // exchanges from - to system
type Tget_FromSystem=function(p:pointer) :pcoordsystem; stdcall; // returns from-system
type Tget_ToSystem=function(p:pointer) :pcoordsystem; stdcall;   // return from-system
type Ttransform=function(p: pointer;Xin,Yin : double;var Xout,Yout : double):integer;stdcall; // calculates projection
type Tset_ToWgs84LL=function(p:pointer):integer; stdcall; // to system is wgs84 Lat/Lon
type Tset_FromWgs84LL=function(p:pointer):integer; stdcall; // from-System is wgs84 Lat/Lon
type TenumProjectionGroup=function(func:tcallback_function;data :pointer):integer; stdcall;
type TenumElipsoids=function(func:tcallback_function;data :pointer):integer; stdcall;
type TenumDatums=function(func:tcallback_function;data :pointer):integer; stdcall;
type TenumCompatDatums=function(func:tcallback_function;datum:pdatum;data:pointer):integer;stdcall;
type TenumCompatProjection=function(func:tcallback_function;projection:pprojection;data:pointer):integer;stdcall;
type Tget_projection_Group=function(group:shortstring):pprojectiongroup; stdcall;
type Tget_projection=function(projection : shortstring): pprojection; stdcall;
type Tget_datum_params=function(datum:shortstring):pdatum; stdcall;
type Tget_elipsoid_params=function(elips:shortstring):pelipsoid; stdcall;
type Tget_projection_group_By_Projection=function(projection : shortstring):pprojectiongroup; stdcall;
type Tset_New_Projection=function(group : pprojectiongroup; projection : pprojection;save_flag : boolean):integer; stdcall;
type Tset_New_Elipsoid=function(p:pelipsoid;save_flag : boolean):integer; stdcall;
type Tset_New_Datum=function(p:pdatum;save_flag : boolean):integer; stdcall;
type Tset_New_ProjectionGroup=function(p: pprojectiongroup;save_flag : boolean):integer; stdcall;
type Tget_Version=function(var major,minor,release : integer):integer; stdcall;

type TProjectionCalculation = class
public
   // public declarations
   WorkingPath  :string;   // current working-path
   DoProjection :boolean;  // Flag that shows if a Projection should be calculated
   constructor Create;
   destructor Destroy;
   // methods to setup Source-Projection
   procedure Set_SourceCoordinateSystem(Value:integer);
   function  Get_SourceCoordinateSystem:integer;
   procedure Set_SourceProjection(Value:string);
   function  Get_SourceProjection:string;
   procedure Set_SourceDate(Value:string);
   function  Get_SourceDate:string;
   procedure Set_SourceProjSettings(Value:string);
   function  Get_SourceProjSettings:string;
   procedure Set_SourceXOffset(Value:double);
   function  Get_SourceXOffset:double;
   procedure Set_SourceYOffset(Value:double);
   function  Get_SourceYOffset:double;
   procedure Set_SourceScale(Value:double);
   function  Get_SourceScale:double;
   procedure Set_UsedInputCoordinateSystem(Value:integer);
   function  Get_UsedInputCoordinateSystem:integer;

   // methods to setup Target projection
   procedure Set_TargetCoordinateSystem(Value:integer);
   function  Get_TargetCoordinateSystem:integer;
   procedure Set_TargetProjection(Value:string);
   function  Get_TargetProjection:string;
   procedure Set_TargetDate(Value:string);
   function  Get_TargetDate:string;
   procedure Set_TargetProjSettings(Value:string);
   function  Get_TargetProjSettings:string;
   procedure Set_TargetXOffset(Value:double);
   function  Get_TargetXOffset:double;
   procedure Set_TargetYOffset(Value:double);
   function  Get_TargetYOffset:double;
   procedure Set_TargetScale(Value:double);
   function  Get_TargetScale:double;
   procedure Set_UsedOutputCoordinateSystem(Value:integer);
   function  Get_UsedOutputCoordinateSystem:integer;

   procedure GetProjectionItems(Projection:string; var Datums:TStrings; var Meridians:TStrings; var DefaultDate:string);
   procedure GetProjections(var Projections:TStrings);
   procedure SetupProjection;
   function  Calculate(var X:double; var Y:double):boolean;
   function  IsValidProjection(aDatum:string; aMeridian:string):boolean;
private
    // source projection
    SourceCoordinateSystem    :integer;
    SourceProjection          :string;
    SourceDate                :string;
    SourceProjSettings        :string;
    SourceXOffset             :double;
    SourceYOffset             :double;
    SourceScale               :double;
    UsedInputCoordinateSystem :integer;

    // Target projection
    TargetCoordinateSystem    :integer;
    TargetProjection          :string;
    TargetDate                :string;
    TargetProjSettings        :string;
    TargetXOffset             :double;
    TargetYOffset             :double;
    TargetScale               :double;
    UsedOutputCoordinateSystem:integer;

    transform_pointer1        :pointer; // projection pointer

    ProjectionSetuped         :boolean; // Flag that shows that the Projection has been setuped with the Dll

    // declarations for coord32.dll
    Coord32DLL         :THandle;
    create_Transform   :Tcreate_Transform;
    free_transform     :Tfree_transform;
    set_FromSystem     :Tset_FromSystem;
    set_ToSytem        :Tset_ToSytem;
    Exchange_Systems   :TExchange_Systems;
    get_FromSystem     :Tget_FromSystem;
    get_ToSystem       :Tget_ToSystem;
    transform          :Ttransform;
    set_ToWgs84LL      :Tset_ToWgs84LL;
    set_FromWgs84LL    :Tset_FromWgs84LL;
    enumProjectionGroup :TenumProjectionGroup;
    enumElipsoids      :TenumElipsoids;
    enumDatums         :TenumDatums;
    enumCompatDatums   :TenumCompatDatums;
    enumCompatProjection :TenumCompatProjection;
    get_projection_Group :Tget_projection_Group;
    get_projection     :Tget_projection;
    get_datum_params   :Tget_datum_params;
    get_elipsoid_params :Tget_elipsoid_params;
    get_projection_group_By_Projection:Tget_projection_group_By_Projection;
    set_New_Projection :Tset_New_Projection;
    set_New_Elipsoid   :Tset_New_Elipsoid;
    set_New_Datum      :Tset_New_Datum;
    set_New_ProjectionGroup :Tset_New_ProjectionGroup;
    get_Version        : Tget_Version;
    procedure GetFunctionsOfCoord32; // method to setup coord32.dll
end;

procedure callback(p: pointer;data :pointer); // callback function of coord32.dll

var LastErrorText: AnsiString = '';

implementation

procedure callback(p: pointer;data :pointer);
begin
  TStrings(data).add(strpas(pszname(p)^));
end;

constructor TProjectionCalculation.Create;
begin
    // set the defaults
   SourceCoordinateSystem:=cs_Carthesic;
   SourceProjection:='NONE';
   SourceDate:='NONE';
   SourceProjSettings:='NONE';
   SourceXOffset:=0;
   SourceYOffset:=0;
   SourceScale:=1;
   UsedInputCoordinateSystem:=cs_Carthesic;

   // Target projection
   TargetCoordinateSystem:=cs_Carthesic;
   TargetProjection:='NONE';
   TargetDate:='NONE';
   TargetProjSettings:='NONE';
   TargetXOffset:=0;
   TargetYOffset:=0;
   TargetScale:=1;
   UsedOutputCoordinateSystem:=cs_Carthesic;

   DoProjection:=FALSE;
   ProjectionSetuped:=FALSE;
   Coord32Dll:=0;
end;

destructor TProjectionCalculation.Destroy;
begin
   if Coord32Dll <> 0 then
      FreeLibrary(Coord32Dll);
   Coord32Dll:=0;
end;

// method to get list of supported projections
procedure TProjectionCalculation.GetProjections(var Projections:TStrings);
begin
   if (Coord32Dll = 0) then // Check if the Coord32.dll has to be loaded
      GetFunctionsOfCoord32;
   enumProjectionGroup(callback,Projections);
end;

// method to get date and meridian of a projection
procedure TProjectionCalculation.GetProjectionItems(Projection:string; var Datums:TStrings; var Meridians:TStrings; var DefaultDate:string);
var pproject : pprojectiongroup;
    pdat     : pdatum;
    pproj    : pprojection;
    i        : integer;
begin
   if (Coord32Dll = 0) then // Check if the Coord32.dll has to be loaded
      GetFunctionsOfCoord32;

   // get pointer to set projection-group
   pproject := get_projection_Group(Projection);
   if pproject <> nil then
   begin
      if pproject^.datums = nil then // alle Datums sind g�ltig also komplette aufz�hlung
      begin
         enumDatums(callback,Datums)
      end
      else
      begin
         for i := 0 to high(pproject^.datums) do
         begin
            // only some datums are regular -> fetch them
            pdat := pproject^.datums[i];
            Datums.Add(strpas(pdat^.szname));
         end;
      end;
      if pproject^.Projections <>  nil then
      begin
         // fetch the projections
         for i := 0 to high(pproject^.Projections) do
         begin
            pproj := pproject^.Projections[i];
            Meridians.add(strpas(pproj^.szname));
         end;
      end;
      DefaultDate:=strpas(pproject^.default_datum^.szname);
   end;
end;

function TProjectionCalculation.IsValidProjection(aDatum:string; aMeridian:string):boolean;
var retVal:boolean;
    pproj : pprojection;
    datum : pdatum;
begin
   retVal:=TRUE;
   pproj:=Get_Projection(aMeridian);
   if pproj = nil then retVal:=false;
   datum:=get_datum_params(aDatum);
   if datum = nil then retVal:=false;
   result:=retVal;
end;

// method is used to calculate the projection
// if something went wrong during calculation the return value is false
function TProjectionCalculation.Calculate(var X:double; var Y:double):boolean;
var X_in:double;
    Y_in:double;
    X_out:double;
    Y_out:double;
    dummy:double;
    retVal:boolean;
begin
   if not ProjectionSetuped then // check if the projection has been setuped
      SetupProjection;

   retVal:=TRUE;
   X_in:=X;
   Y_in:=Y;
   // first the source-offsets have to be subtracted
   if ((UsedInputCoordinateSystem = cs_Carthesic) and (SourceCoordinateSystem = cs_Geodetic))  then
   begin
      // if the input-coordinates are carthesic, but should be handeled
      // as they are geodetic, the Y-Offset will be subtraced from
      // the X-Coordinate and the X-Offset will be subtracted from
      // the Y-Coordinate
      X_in:=X_in - SourceYOffset;
      Y_in:=Y_in - SourceXOffset;
   end
   else if ((UsedInputCoordinateSystem = cs_Geodetic) and (SourceCoordinateSystem = cs_Carthesic))  then
   begin
      // if the input-coordinates are geodetc, but should be handeled
      // as they are carthesic, the Y-Offset will be subtraced from
      // the X-Coordinate and the X-Offset will be subtracted from
      // the Y-Coordinate
      X_in:=X_in - SourceYOffset;
      Y_in:=Y_in - SourceXOffset;
   end
   else
   begin
      // otherwise the X-Offset will be subtracted from the X-Coordinate and
      // the Y-Offset will be subtracted from the Y-Coordinate
      X_in:=X_in - SourceXOffset;
      Y_in:=Y_in - SourceYOffset;
   end;

   // now the source-scale has to be divided
   X_in:=X_in / SourceScale;
   Y_in:=Y_in / SourceScale;

   X_out:=X_in;
   Y_out:=Y_in;
   // now it will be checked if a projection has to be calculated
   if DoProjection then
   begin
      // if a projection has to be calculated
      // the coordinates have to be in carthesic
      if UsedInputCoordinateSystem = cs_Geodetic then
      begin
         // if the input coordinates are in geodetic, x and y have to be changed
         dummy:=X_in;
         X_in:=Y_in;
         Y_in:=dummy;
      end;

      // now the projection will be calculated
      // only if sourceprojection and targetprojection are different
      // otherwise keep as it is
      if (SourceProjection <> TargetProjection) or (SourceDate <> TargetDate) then
         transform(transform_pointer1,X_in,Y_In,X_out,Y_out)
      else
      begin
         X_out:=X_in;
         Y_out:=Y_in;
      end;

      if UsedInputCoordinateSystem = cs_Geodetic then
      begin
         // if the input coordinates are geodetic they
         // have to be changed back
         dummy:=X_out;
         X_out:=Y_out;
         Y_out:=dummy;
      end;
   end;

   if (UsedInputCoordinateSystem = cs_Carthesic) and (UsedOutputCoordinateSystem = cs_Geodetic) then
   begin
      // X and Y coordinates have to be changed
      dummy:=X_out;
      X_out:=Y_out;
      Y_out:=dummy;
   end
   else if (UsedInputCoordinateSystem = cs_Geodetic) and (UsedOutputCoordinateSystem = cs_Carthesic) then
   begin
      // X and Y coordinates have to be changed
      dummy:=X_out;
      X_out:=Y_out;
      Y_out:=dummy;
   end;

   // now the target-scalefactor has to be set
   X_out:=X_out * TargetScale;
   Y_out:=Y_out * TargetScale;

   // then the offsets have to be added
   if ((UsedOutputCoordinateSystem = cs_Carthesic) and (TargetCoordinateSystem = cs_Geodetic))  then
   begin
      // if the output-coordinates are carthesic, but should be handeled
      // as they are geodetic, the Y-Offset will be added to the
      // the X-Coordinate and the X-Offset will be added to the
      // the Y-Coordinate
      X_out:=X_out + TargetYOffset;
      Y_out:=Y_out + TargetXOffset;
   end
   else if ((UsedOutputCoordinateSystem = cs_Geodetic) and (TargetCoordinateSystem = cs_Carthesic))  then
   begin
      // if the output-coordinates are geodetc, but should be handeled
      // as they are carthesic, the Y-Offset will be added to the
      // the X-Coordinate and the X-Offset will be added to the
      // the Y-Coordinate
      X_out:=X_out + TargetYOffset;
      Y_out:=Y_out + TargetXOffset;
   end
   else
   begin
      // otherwise the X-Offset will be added to the X-Coordinate and
      // the Y-Offset will be added to the Y-Coordinate
      X_out:=X_out + TargetXOffset;
      Y_out:=Y_out + TargetYOffset;
   end;

   X:=X_out;
   Y:=Y_out;

   // check if the coordinates are in the limit
   if not ((X > -21800000)  and (X < 21800000) and
      (Y > -21800000)  and (Y < 21800000)) then
   begin
      retVal:=FALSE;
      X:=0;
      Y:=0;
   end;
   result:=retVal;
end;

// method is used to setup the Coord32.dll with the current projection
procedure TProjectionCalculation.SetupProjection;
var pproj : pprojection;
    datum : pdatum;
begin
   // check if it is necessary to calculate a projection
   if (SourceProjection = 'NONE') or (SourceProjSettings = 'NONE') or (SourceDate = 'NONE') or
      (TargetProjection = 'NONE') or (TargetProjSettings = 'NONE') or (TargetDate = 'NONE') or
      (
        (SourceProjection = TargetProjection) and
        (SourceProjSettings = TargetProjSettings) and
        (SourceDate = TargetDate)
       ) then
   begin
      // no projection calculation has to be done
      DoProjection:=FALSE;
      ProjectionSetuped:=TRUE;
      exit;
   end;

   // check if the transformation pointer exists
   if Coord32Dll = 0 then begin
        GetFunctionsOfCoord32;
   end;

   DoProjection:=true;
   // check the source-projection and date
   pproj:=Get_Projection(SourceProjection);
   if pproj = nil then DoProjection:=false;
   datum:=get_datum_params(SourceDate);
   if datum = nil then DoProjection:=false;

   // check the target-projection and date
   if (DoProjection) then
   begin
      pproj:=Get_Projection(TargetProjection);
      if pproj = nil then DoProjection:=false;
      datum:=get_datum_params(TargetDate);
      if datum = nil then DoProjection:=false;
   end
   else
      DoProjection:=FALSE;

   if (DoProjection) then
   begin
      //set the source projection
      pproj:=Get_Projection(SourceProjection);
      datum:=get_datum_params(SourceDate);
      set_FromSystem(transform_pointer1,pproj,datum);

      // set the target projection
      pproj:=Get_Projection(TargetProjection);
      datum:=get_datum_params(TargetDate);
      set_ToSytem(transform_pointer1,pproj,datum);
   end;
   ProjectionSetuped:=TRUE;
end;


// method is used to load the coord32.dll
procedure TProjectionCalculation.GetFunctionsOfCoord32;
var create_TransformPtr :TFarProc;
    free_transformPtr   :TFarProc;
    set_FromSystemPtr   :TFarProc;
    set_ToSytemPtr      :TFarProc;
    Exchange_SystemsPtr :TFarProc;
    get_FromSystemPtr   :TFarProc;
    get_ToSystemPtr     :TFarProc;
    transformPtr        :TFarProc;
    set_ToWgs84LLPtr    :TFarProc;
    set_FromWgs84LLPtr  :TFarProc;
    enumProjectionGroupPtr:TFarProc;
    enumElipsoidsPtr    :TFarProc;
    enumDatumsPtr       :TFarProc;
    enumCompatDatumsPtr :TFarProc;
    enumCompatProjectionPtr:TFarProc;
    get_projection_GroupPtr:TFarProc;
    get_projectionPtr      :TFarProc;
    get_datum_paramsPtr    :TFarProc;
    get_elipsoid_paramsPtr :TFarProc;
    get_projection_group_By_ProjectionPtr:TFarProc;
    set_New_ProjectionPtr :TFarProc;
    set_New_ElipsoidPtr   :TFarProc;
    set_New_DatumPtr      :TFarProc;
    set_New_ProjectionGroupPtr:TFarProc;
    get_VersionPtr :TFarProc;
begin
   try
        Coord32Dll:=LoadLibrary(PCHAR(WorkingPath+'COORD32.DLL'));
   except
        on E: Exception do begin
                LastErrorText:=E.Message;
        end;
   end;
   // get create_Transform pointer
   create_TransformPtr:=GetProcAddress(Coord32Dll,'create_Transform');
   if create_TransformPtr <> nil then @create_Transform:=create_TransformPtr;

   // get free_transform pointer
   free_transformPtr:=GetProcAddress(Coord32Dll,'free_transform');
   if free_transformPtr <> nil then @free_transform:=free_transformPtr;

   // get set_FromSystem pointer
   set_FromSystemPtr:=GetProcAddress(Coord32Dll,'set_FromSystem');
   if set_FromSystemPtr <> nil then @set_FromSystem:=set_FromSystemPtr;

   // get set_ToSystem pointer
   set_ToSytemPtr:=GetProcAddress(Coord32Dll,'set_ToSytem');
   if set_ToSytemPtr <> nil then @set_ToSytem:=set_ToSytemPtr;

   // get Exchange_Systems pointer
   Exchange_SystemsPtr:=GetProcAddress(Coord32Dll,'Exchange_Systems');
   if Exchange_SystemsPtr <> nil then @Exchange_Systems:=Exchange_SystemsPtr;

   // get get_FromSystem pointer
   get_FromSystemPtr:=GetProcAddress(Coord32Dll,'get_FromSystem');
   if get_FromSystemPtr <> nil then @get_FromSystem:=get_FromSystemPtr;

   // get get_ToSystem pointer
   get_ToSystemPtr:=GetProcAddress(Coord32Dll,'get_ToSystem');
   if get_ToSystemPtr <> nil then @get_ToSystem:=get_ToSystemPtr;

   // get transform pointer
   transformPtr:=GetProcAddress(Coord32Dll,'transform');
   if transformPtr <> nil then @transform:=transformPtr;

   // get set_ToWgs84LL pointer
   set_ToWgs84LLPtr:=GetProcAddress(Coord32Dll,'set_ToWgs84LL');
   if set_ToWgs84LLPtr <> nil then @set_ToWgs84LL:=set_ToWgs84LLPtr;

   // get set_ToWgs84LL pointer
   set_ToWgs84LLPtr:=GetProcAddress(Coord32Dll,'set_ToWgs84LL');
   if set_ToWgs84LLPtr <> nil then @set_ToWgs84LL:=set_ToWgs84LLPtr;

   // get set_FromWgs84LL pointer
   set_FromWgs84LLPtr:=GetProcAddress(Coord32Dll,'set_FromWgs84LL');
   if set_FromWgs84LLPtr <> nil then @set_FromWgs84LL:=set_FromWgs84LLPtr;

   // get enumProjectionGroup pointer
   enumProjectionGroupPtr:=GetProcAddress(Coord32Dll,'enumProjectionGroup');
   if enumProjectionGroupPtr <> nil then @enumProjectionGroup:=enumProjectionGroupPtr;

   // get enumElipsoids pointer
   enumElipsoidsPtr:=GetProcAddress(Coord32Dll,'enumElipsoids');
   if enumElipsoidsPtr <> nil then @enumElipsoids:=enumElipsoidsPtr;

   // get enumDatums pointer
   enumDatumsPtr:=GetProcAddress(Coord32Dll,'enumDatums');
   if enumDatumsPtr <> nil then @enumDatums:=enumDatumsPtr;

   // get enumCompatDatums pointer
   enumCompatDatumsPtr:=GetProcAddress(Coord32Dll,'enumCompatDatums');
   if enumCompatDatumsPtr <> nil then @enumCompatDatums:=enumCompatDatumsPtr;

   // get enumCompatProjection pointer
   enumCompatProjectionPtr:=GetProcAddress(Coord32Dll,'enumCompatProjection');
   if enumCompatProjectionPtr <> nil then @enumCompatProjection:=enumCompatProjectionPtr;

   // get get_projection_Group pointer
   get_projection_GroupPtr:=GetProcAddress(Coord32Dll,'get_projection_Group');
   if get_projection_GroupPtr <> nil then @get_projection_Group:=get_projection_GroupPtr;

   // get get_projection pointer
   get_projectionPtr:=GetProcAddress(Coord32Dll,'get_projection');
   if get_projectionPtr <> nil then @get_projection:=get_projectionPtr;

   // get get_datum_params pointer
   get_datum_paramsPtr:=GetProcAddress(Coord32Dll,'get_datum_params');
   if get_datum_paramsPtr <> nil then @get_datum_params:=get_datum_paramsPtr;

   // get get_elipsoid_params pointer
   get_elipsoid_paramsPtr:=GetProcAddress(Coord32Dll,'get_elipsoid_params');
   if get_elipsoid_paramsPtr <> nil then @get_elipsoid_params:=get_elipsoid_paramsPtr;

   // get get_projection_group_By_Projection pointer
   get_projection_group_By_ProjectionPtr:=GetProcAddress(Coord32Dll,'get_projection_group_By_Projection');
   if get_projection_group_By_ProjectionPtr <> nil then @get_projection_group_By_Projection:=get_projection_group_By_ProjectionPtr;

   // get set_New_Projection pointer
   set_New_ProjectionPtr:=GetProcAddress(Coord32Dll,'set_New_Projection');
   if set_New_ProjectionPtr <> nil then @set_New_Projection:=set_New_ProjectionPtr;

   // get set_New_Elipsoid pointer
   set_New_ElipsoidPtr:=GetProcAddress(Coord32Dll,'set_New_Elipsoid');
   if set_New_ElipsoidPtr <> nil then @set_New_Elipsoid:=set_New_ElipsoidPtr;

   // get set_New_Datum pointer
   set_New_DatumPtr:=GetProcAddress(Coord32Dll,'set_New_Datum');
   if set_New_DatumPtr <> nil then @set_New_Datum:=set_New_DatumPtr;

   // get set_New_ProjectionGroup pointer
   set_New_ProjectionGroupPtr:=GetProcAddress(Coord32Dll,'set_New_ProjectionGroup');
   if set_New_ProjectionGroupPtr <> nil then @set_New_ProjectionGroup:=set_New_ProjectionGroupPtr;

   // get get_Version pointer
   get_VersionPtr:=GetProcAddress(Coord32Dll,'get_Version');
   if get_VersionPtr <> nil then @get_Version:=get_VersionPtr;

   // initialize transformation pointer
   create_transform(transform_pointer1);
end;


procedure TProjectionCalculation.Set_SourceCoordinateSystem(Value:integer);
begin
   SourceCoordinateSystem:=Value;
end;

function  TProjectionCalculation.Get_SourceCoordinateSystem:integer;
begin
   result:=SourceCoordinateSystem;
end;

procedure TProjectionCalculation.Set_SourceProjection(Value:string);
begin
   if (SourceProjection <> Value) then // check if Source-Projection has bee changed
      ProjectionSetuped:=FALSE;
   SourceProjection:=Value;
end;

function TProjectionCalculation.Get_SourceProjection:string;
begin
   result:=SourceProjection;
end;

procedure TProjectionCalculation.Set_SourceDate(Value:string);
begin
   if (SourceDate <> Value) then // check if Source-Date has bee changed
      ProjectionSetuped:=FALSE;
   SourceDate:=Value;
end;

function TProjectionCalculation.Get_SourceDate:string;
begin
   result:=SourceDate;
end;

procedure TProjectionCalculation.Set_SourceProjSettings(Value:string);
begin
   if (SourceProjSettings <> Value) then // check if Source-Projectionsettings has bee changed
      ProjectionSetuped:=FALSE;
   SourceProjSettings:=Value;
end;

function TProjectionCalculation.Get_SourceProjSettings:string;
begin
   result:=SourceProjSettings;
end;

procedure TProjectionCalculation.Set_SourceXOffset(Value:double);
begin
   SourceXOffset:=Value;
end;

function TProjectionCalculation.Get_SourceXOffset:double;
begin
   result:=SourceXOffset;
end;

procedure TProjectionCalculation.Set_SourceYOffset(Value:double);
begin
   SourceYOffset:=Value;
end;

function TProjectionCalculation.Get_SourceYOffset:double;
begin
   result:=SourceYOffset;
end;

procedure TProjectionCalculation.Set_SourceScale(Value:double);
begin
   SourceScale:=Value;
end;

function  TProjectionCalculation.Get_SourceScale:double;
begin
   result:=SourceScale;
end;

procedure TProjectionCalculation.Set_UsedInputCoordinateSystem(Value:integer);
begin
   UsedInputCoordinateSystem:=Value;
end;

function TProjectionCalculation.Get_UsedInputCoordinateSystem:integer;
begin
   result:=UsedInputCoordinateSystem;
end;

procedure TProjectionCalculation.Set_TargetCoordinateSystem(Value:integer);
begin
   TargetCoordinateSystem:=Value;
end;

function TProjectionCalculation.Get_TargetCoordinateSystem:integer;
begin
   result:=TargetCoordinateSystem;
end;

procedure TProjectionCalculation.Set_TargetProjection(Value:string);
begin
   if (TargetProjection <> Value) then // check if TargetProjection has been changed
      ProjectionSetuped:=FALSE;
   TargetProjection:=Value;
end;

function TProjectionCalculation.Get_TargetProjection:string;
begin
   result:=TargetProjection;
end;

procedure TProjectionCalculation.Set_TargetDate(Value:string);
begin
   if (TargetDate <> Value) then // check if TargetDate has been changed
      ProjectionSetuped:=FALSE;
   TargetDate:=Value;
end;

function TProjectionCalculation.Get_TargetDate:string;
begin
   result:=TargetDate;
end;

procedure TProjectionCalculation.Set_TargetProjSettings(Value:string);
begin
   if (TargetProjSettings <> Value) then // check if TargetDate has been changed
      ProjectionSetuped:=FALSE;
   TargetProjSettings:=Value;
end;

function TProjectionCalculation.Get_TargetProjSettings:string;
begin
   result:=TargetProjSettings;
end;

procedure TProjectionCalculation.Set_TargetXOffset(Value:double);
begin
   TargetXOffset:=Value;
end;

function TProjectionCalculation.Get_TargetXOffset:double;
begin
   result:=TargetXOffset;
end;

procedure TProjectionCalculation.Set_TargetYOffset(Value:double);
begin
   TargetYOffset:=Value;
end;

function TProjectionCalculation.Get_TargetYOffset:double;
begin
   result:=TargetYOffset;
end;

procedure TProjectionCalculation.Set_TargetScale(Value:double);
begin
   TargetScale:=Value;
end;

function TProjectionCalculation.Get_TargetScale:double;
begin
   result:=TargetScale;
end;

procedure TProjectionCalculation.Set_UsedOutputCoordinateSystem(Value:integer);
begin
   UsedOutputCoordinateSystem:=Value;
end;

function TProjectionCalculation.Get_UsedOutputCoordinateSystem:integer;
begin
   result:=UsedOutputCoordinateSystem;
end;

end.
