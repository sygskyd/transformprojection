object GeoWnd: TGeoWnd
  Left = 359
  Top = 654
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Setting the projection'
  ClientHeight = 369
  ClientWidth = 354
  Color = clBtnFace
  Constraints.MaxHeight = 500
  Constraints.MaxWidth = 454
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object OKBtn: TButton
    Left = 151
    Top = 340
    Width = 93
    Height = 23
    Caption = 'Ok'
    Default = True
    DragCursor = crDefault
    ModalResult = 1
    TabOrder = 0
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 253
    Top = 340
    Width = 93
    Height = 23
    Cancel = True
    Caption = 'Abbrechen'
    Default = True
    ModalResult = 2
    TabOrder = 1
  end
  object PageControl1: TPageControl
    Left = 8
    Top = 64
    Width = 337
    Height = 273
    ActivePage = SourceTabSheet
    TabOrder = 2
    object SourceTabSheet: TTabSheet
      Caption = 'Source'
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 329
        Height = 29
        TabOrder = 0
        object SourceCarthesicRB: TRadioButton
          Left = 8
          Top = 9
          Width = 97
          Height = 17
          Caption = 'carthesic'
          Checked = True
          TabOrder = 0
          TabStop = True
        end
        object SourceGeodeticRB: TRadioButton
          Left = 224
          Top = 9
          Width = 89
          Height = 17
          Caption = 'geodetic'
          TabOrder = 1
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 92
        Width = 329
        Height = 149
        TabOrder = 1
        object Panel9: TPanel
          Left = 8
          Top = 32
          Width = 313
          Height = 38
          TabOrder = 0
          object SourceProjektionCB: TComboBox
            Left = 8
            Top = 8
            Width = 297
            Height = 21
            Style = csDropDownList
            DropDownCount = 20
            ItemHeight = 13
            TabOrder = 0
            OnChange = SourceProjektionCBChange
          end
        end
        object Panel10: TPanel
          Left = 8
          Top = 74
          Width = 313
          Height = 70
          TabOrder = 1
          object Label5: TLabel
            Left = 8
            Top = 12
            Width = 50
            Height = 13
            Caption = 'Projektion:'
          end
          object Label6: TLabel
            Left = 8
            Top = 44
            Width = 34
            Height = 13
            Caption = 'Datum:'
          end
          object SourceMeridianCB: TComboBox
            Left = 64
            Top = 8
            Width = 241
            Height = 21
            Style = csDropDownList
            DropDownCount = 20
            ItemHeight = 13
            TabOrder = 0
          end
          object SourceDatumCB: TComboBox
            Left = 64
            Top = 40
            Width = 241
            Height = 21
            Style = csDropDownList
            DropDownCount = 20
            ItemHeight = 13
            TabOrder = 1
            OnChange = SourceDatumCBChange
          end
        end
        object GisNoProCB: TCheckBox
          Left = 8
          Top = 8
          Width = 113
          Height = 17
          Caption = 'no projection'
          TabOrder = 2
          OnClick = GisNoProCBClick
        end
        object ButtonUTM: TButton
          Left = 264
          Top = 6
          Width = 57
          Height = 22
          Caption = 'UTM...'
          TabOrder = 3
          OnClick = ButtonUTMClick
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 32
        Width = 329
        Height = 57
        TabOrder = 2
        object Label3: TLabel
          Left = 8
          Top = 8
          Width = 41
          Height = 13
          Caption = 'X-Offset:'
        end
        object Label4: TLabel
          Left = 8
          Top = 36
          Width = 41
          Height = 13
          Caption = 'Y-Offset:'
        end
        object Label10: TLabel
          Left = 160
          Top = 8
          Width = 63
          Height = 13
          Caption = 'Scale-Factor:'
        end
        object SourceXOffsetEdit: TEdit
          Left = 56
          Top = 4
          Width = 65
          Height = 24
          TabOrder = 0
          Text = '0.00'
          OnChange = SourceXOffsetEditChange
        end
        object SourceYOffsetEdit: TEdit
          Left = 56
          Top = 32
          Width = 65
          Height = 24
          TabOrder = 1
          Text = '0.00'
          OnChange = SourceYOffsetEditChange
        end
        object SourceScaleCB: TComboBox
          Left = 256
          Top = 3
          Width = 65
          Height = 21
          ItemHeight = 13
          TabOrder = 2
          Text = '1.00'
          OnChange = SourceScaleCBChange
        end
      end
    end
    object DestTabSheet: TTabSheet
      Caption = 'Target'
      ImageIndex = 1
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 329
        Height = 29
        TabOrder = 0
        object TargetCarthesicRB: TRadioButton
          Left = 8
          Top = 9
          Width = 113
          Height = 17
          Caption = 'carthesic'
          Checked = True
          TabOrder = 0
          TabStop = True
        end
        object TargetGeodeticRB: TRadioButton
          Left = 224
          Top = 9
          Width = 89
          Height = 17
          Caption = 'geodetic'
          TabOrder = 1
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 92
        Width = 329
        Height = 149
        TabOrder = 1
        object Panel5: TPanel
          Left = 8
          Top = 32
          Width = 313
          Height = 38
          TabOrder = 0
          object TargetProjektionCB: TComboBox
            Left = 8
            Top = 8
            Width = 297
            Height = 21
            Style = csDropDownList
            DropDownCount = 20
            ItemHeight = 0
            TabOrder = 0
            OnChange = TargetProjektionCBChange
          end
        end
        object Panel6: TPanel
          Left = 8
          Top = 74
          Width = 313
          Height = 70
          TabOrder = 1
          object Label1: TLabel
            Left = 8
            Top = 12
            Width = 50
            Height = 13
            Caption = 'Projektion:'
          end
          object Label2: TLabel
            Left = 8
            Top = 44
            Width = 34
            Height = 13
            Caption = 'Datum:'
          end
          object TargetMeridianCB: TComboBox
            Left = 64
            Top = 8
            Width = 241
            Height = 21
            Style = csDropDownList
            DropDownCount = 20
            ItemHeight = 0
            TabOrder = 0
          end
          object TargetDatumCB: TComboBox
            Left = 64
            Top = 40
            Width = 241
            Height = 21
            Style = csDropDownList
            DropDownCount = 20
            ItemHeight = 0
            TabOrder = 1
            OnChange = TargetDatumCBChange
          end
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 32
        Width = 329
        Height = 57
        TabOrder = 2
        object Label7: TLabel
          Left = 8
          Top = 8
          Width = 41
          Height = 13
          Caption = 'X-Offset:'
        end
        object Label8: TLabel
          Left = 8
          Top = 36
          Width = 41
          Height = 13
          Caption = 'Y-Offset:'
        end
        object Label9: TLabel
          Left = 160
          Top = 8
          Width = 63
          Height = 13
          Caption = 'Scale-Factor:'
        end
        object TargetXOffsetEdit: TEdit
          Left = 56
          Top = 4
          Width = 65
          Height = 21
          TabOrder = 0
          Text = '0.00'
          OnChange = TargetXOffsetEditChange
        end
        object TargetYOffsetEdit: TEdit
          Left = 56
          Top = 32
          Width = 65
          Height = 21
          TabOrder = 1
          Text = '0.00'
          OnChange = TargetYOffsetEditChange
        end
        object TargetScaleCB: TComboBox
          Left = 256
          Top = 3
          Width = 65
          Height = 21
          ItemHeight = 0
          TabOrder = 2
          Text = '1.00'
          OnChange = TargetScaleCBChange
        end
      end
    end
  end
  object UseProjCB: TCheckBox
    Left = 8
    Top = 8
    Width = 305
    Height = 17
    Caption = 'use stored projection'
    TabOrder = 3
    OnClick = UseProjCBClick
  end
  object ProjCB: TComboBox
    Left = 8
    Top = 30
    Width = 305
    Height = 21
    ItemHeight = 13
    TabOrder = 4
    OnChange = ProjCBChange
  end
  object NewProjBtn: TButton
    Left = 320
    Top = 28
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 5
    OnClick = NewProjBtnClick
  end
  object ResetBtn: TButton
    Left = 8
    Top = 340
    Width = 93
    Height = 23
    Caption = 'Reset'
    TabOrder = 6
    OnClick = ResetBtnClick
  end
end
