(*
  Interface-Unit zum einbinden der coord32.dll in andere Applikationen
  Autor:         Dietmar Bruckner
  Datum:         25.04.1999
  Compiler :     Delphi 4.0
  Hinweise:
  x-Achse ist horizontal, y-Achse ist vertikal
  Koordinatenangaben entweder in Meter oder in Decimalgrad (nicht radiant)
  Anderungen:
  27.4.1999
    set_ToWgs84LL(p:pointer):integer; stdcall;external 'coord32.dll';
    set_FromWgs84LL(p:pointer):integer; stdcall;external 'coord32.dll';
  03.05.1999
    function enumCompatDatums(func:tcallback_function;datum:pdatum;data:pointer):integer;stdcall; external 'coord32.dll';  
*)

unit coord32pas;

interface
const Cnamelength = 60;

type Tszname =array[0..Cnamelength] of char;
     pszname = ^tszname;
type telipsoid = record
    SZNAME : Tszname;                 // name des Elipsoids
    ae : double;                      // gro�e Halbachse
    fe : double;                      // inverse flattening
    pHint : pchar;                    // optionaler hinweistext
    end;
    pelipsoid = ^telipsoid;

type tdatum = record
    szname   : Tszname;               // name des Datums
    elipsoid : pelipsoid;             // zugeh�riges Elipsoid
    IDMethod   : array[0..2] of char; // Konvertierungsmethode 'MOL' : Molodensky, '7PA' : 7 parameter
    params : array of double;         // shift parameter 4 bei molodensky, 8 bei _7params
    pHint : pchar;                    // optionaler hinweistext
    end;
    pdatum = ^tdatum;

type tprojection = record
    szname : Tszname;                 // name der Projektion
    IDProc : array[0..2] of char ;    // 'TMK' := transverse Mercator;
                                      // 'GDG' := geographic Lat/Lon;
                                      // 'CHC' : swiss coordiante system ;
    projection_params : array of double;
                                      // bei TMK 5 :Origin_lat,origin_lon,false_northing,false_easting,sacaling
                                      // bei GDG NIL
                                      // bei CHC NIL
    end;
    pprojection = ^tprojection;

type tprojectiongroup = record
      szname : Tszname;              // name der Projektionsgruppe
      datums : array of pdatum;      // liste aller zugelassenen Datums, Nil falls alle zugelassen sind
      default_datum : pdatum;        // zeiger auf das default_datum
      Projections : array of pprojection;  // Projektionen die dieser Gruppe zugeordent sind
    end;
    pprojectiongroup = ^tprojectiongroup;

type tcoordsystem = record
    szname : Tszname;                 // name des Koordinatensystems ( z.Z nicht benutzt)
    projection : pprojection;         // zeiger auf zugeh�rige Mathematische Projektion
    datum  : pdatum;                  // zeiger auf zugeh�riges Datum
    end;
    pcoordsystem = ^tcoordsystem;

// callbackfunktion f�r aufz�hlungs-funktionen
// p ist ist abh�ngig von aufz�hlungsfunktion
// data wird lediglich von den Aufz�hlungsfunktionen durchgeschleust
type tcallback_function = procedure(p:pointer;data :pointer);


// exportierte Funktionen:
// R�ckgabewert bei einwandfreier ausf�hrung : 0
// R�ckgabewert bei exception innerhalb der Funktion : -1
// weitere R�ckgabewerte bei den einzelnen Funktionen (noch nicht komplett)

// Funktionen die Transformation direkt betreffend
// p ist ein Zeiger der mit create_Transform erzeugt wird
// und als referenz f�r alle anderen Funktionen benutzt wird
// dadurch ist es m�glich paralel mehrere Transformationen durchzuf�hren

function create_Transform(var p : pointer):integer;stdcall; external 'coord32.dll';// constructor funktion

function free_transform(var p : pointer):integer;stdcall; external 'coord32.dll';// free_transform als destructor am Ende aufrufen p ist danach NIL

function set_FromSystem(p:pointer;projection:pprojection;datum :pdatum):integer;stdcall; external 'coord32.dll';// setzt das From-System
function set_ToSytem(p:pointer;projection:pprojection;datum :pdatum):integer;stdcall; external 'coord32.dll';   // setzt das To-System
function Exchange_Systems(p : pointer): integer; stdcall;  external 'coord32.dll';                              // tauscht From/To -System
function get_FromSystem(p:pointer) :pcoordsystem; stdcall; external 'coord32.dll';                              // Gibt zeiger auf From-Systen zur�ck
function get_ToSystem(p:pointer) :pcoordsystem; stdcall; external 'coord32.dll';                                // Gibt zeiger auf To-Systen zur�ck
function transform(p: pointer;Xin,Yin : double;var Xout,Yout : double):integer;stdcall; external 'coord32.dll'; // F�hrt die eigentliche Koordinatentransformation durch
function set_ToWgs84LL(p:pointer):integer; stdcall;external 'coord32.dll';                                      // To system ist wgs84 Lat/Lon
function set_FromWgs84LL(p:pointer):integer; stdcall;external 'coord32.dll';                                    // from-System ist wgs84 Lat/Lon

// Aufz�hlungs-Funktionen die einer callbackfunktion
// die Pointer aus Elipsoid.dat zu den jeweiligen Daten zur�ckliefert
// func ist eine deklarierte tcallback_function
function enumProjectionGroup(func:tcallback_function;data :pointer):integer; stdcall; external 'coord32.dll';
function enumElipsoids(func:tcallback_function;data :pointer):integer; stdcall; external 'coord32.dll';
function enumDatums(func:tcallback_function;data :pointer):integer; stdcall; external 'coord32.dll';

function enumCompatDatums(func:tcallback_function;datum:pdatum;data:pointer):integer;stdcall; external 'coord32.dll';
function enumCompatProjection(func:tcallback_function;projection:pprojection;data:pointer):integer;stdcall; external 'coord32.dll';

// einzelne R�ckgaben der Pointer nach Stringvorgaben
// ist der String nicht vorhanden wird NIL zur�ckgeliefert
function get_projection_Group(group:shortstring):pprojectiongroup; stdcall; external 'coord32.dll';
function get_projection(projection : shortstring): pprojection; stdcall; external 'coord32.dll';
function get_datum_params(datum:shortstring):pdatum; stdcall; external 'coord32.dll';
function get_elipsoid_params(elips:shortstring):pelipsoid; stdcall; external 'coord32.dll';
function get_projection_group_By_Projection(projection : shortstring):pprojectiongroup; stdcall;external 'coord32.dll';

// Funktionen zum setzen von neuen Werten
// save_flag := true  bewirkt abspeichern in Elipsoid.dat
// save_flag := false werden die Werte nur im Speicher behalten und sind bis zum Programmende verf�gbar
//******** ACHTUNG : alle Funktionen noch nicht �berpr�ft, Fehler!!!!
function set_New_Projection(group : pprojectiongroup; projection : pprojection;save_flag : boolean):integer; stdcall;external 'coord32.dll';
function set_New_Elipsoid(p:pelipsoid;save_flag : boolean):integer; stdcall;external 'coord32.dll';
function set_New_Datum(p:pdatum;save_flag : boolean):integer; stdcall;external 'coord32.dll';
function set_New_ProjectionGroup(p: pprojectiongroup;save_flag : boolean):integer; stdcall;external 'coord32.dll';

function get_Version(var major,minor,release : integer):integer; stdcall;external 'coord32.dll';

implementation


end.
