unit Files;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Math, Struct, StrFile;

const
       Col_Int      = 0;
       Col_SmallInt = 1;
       Col_Float    = 2;
       Col_Date     = 3;
       Col_String   = 4;
       // Object types
       ot_Pixel     = 1;
       ot_Poly      = 2;
       ot_CPoly     = 4;
       ot_Text      = 7;
       ot_Symbol    = 8;
       ot_Spline    = 9;
       ot_Image     = 10;
       ot_MesLine   = 11;
       ot_Circle    = 12;
       ot_Arc       = 13;
       ot_Layer     = 18;

             //TextSTyles
       ts_Left           =  0;
       ts_Center         =  1;
       ts_Right          =  2;
       ts_Italic         =  4;
       ts_Bold           =  8;
       ts_Underl         = 16;
       ts_FixHeight      = 32;
       ts_Transparent    = 64;

       //LineTypes
       lt_Solid     = 0;
       lt_Dash      = 1;
       lt_Dot       = 2;
       lt_DashDot   = 3;
       lt_DashDDot  = 4;
       lt_UserDefined    = 1000;

       //FillTypes
       pt_NoPattern = 0;
       pt_FDiagonal = 1;
       pt_Cross     = 2;
       pt_DiagCross = 3;
       pt_BDiagonal = 4;
       pt_Horizontal= 5;
       pt_Vertical  = 6;
       pt_Solid     = 7;
       pt_UserDefined = 8;

       stDrawingUnits    = 0;
       stScaleInDependend= 1;
       stProjectScale    = 2;
       stPercent         = 3;


type
  { Hier k�nnen Strukturen definiert werden }
  p_double = ^doublewert;
  doublewert = record
     Zahl :double;
  end;

  p_integer = ^integerwert;

  integerwert = record
     Zahl :integer;
  end;

ClassFile = class
   public
      { public Definitionen }
      Linienstil,Linienfarbe:string;
      Flaechenstil,Flaechenfarbe:string;
      Layername:string;

      constructor Create;
      procedure Destroy;
      function GetFilesize(Dateiname:string):longint;
      function  OpenASCFile(Dateiname:string):boolean;
      procedure CloseAllFiles;

      procedure ResetObjectNumber;

      function  GetRegionElements:longint;
      function  GetPLineElements:longint;
      function  GetSplineElements:longint;
      function  GetArcElements:longint;
      function  GetPointElements:longint;
      function  GetEllipseElements:longint;
      function  GetTextElements:longint;
      function  GetSymbolElements:longint;
      function  GetImageElements:longint;

      function  WriteCurrentPLine:longint;
      function  WriteCurrentSpline:longint;
      function  WriteCurrentArc:longint;
      function  WriteCurrentPoint:longint;
      function  WriteCurrentRegion:longint;
      function  WriteCurrentEllipse:longint;
      function  WriteCurrentText   :longint;
      function  WriteCurrentSymbol:longint;
      function  WriteCurrentImage:longint;

      procedure WriteLayerDefinition;

      procedure WriteASC(Zeile:string);
      procedure CreateADF(Dateiname:string);
      procedure CreateSym(Dateiname:string);

      function  Check_odoa(Dateiname:string):boolean;
      function  to_odoa:longint;

      procedure OpenFile(Dateiname:string);
      procedure Closefiles;

      procedure New_Layer(LName:string;LStil:string;LFarbe:string;FStil:string;FFarbe:string);
      procedure ValDatabase(Index:integer; ObjType:integer;Layername:string);
      procedure New_Point(X:double;Y:double);
      procedure New_Area;
      procedure InsertPointToArea(X:double;Y:double);
      procedure New_Line;
      procedure InsertPointToLine(X:double;Y:double);
      procedure New_Spline;
      procedure InsertPointToSpline(X:double;Y:double);
      procedure New_Text(X1:double;Y1:double;X2:double;Y2:double;Text:string;Font:string;size:double;angle:integer;DisplayType:string; Alignement:string);
      procedure New_Circle(X:double;Y:double;Radius:integer);
      procedure New_Symbol(X:double;Y:double;Size:double;Angle:double;SymNr:integer);
      procedure New_Image(X:double; Y:double; Height:double; Width:double; Settings:string; Filename:string);
      procedure New_Arc(X:double; Y:double; PrimAxis:double; SecAxis:double; aAngle:double; BeginAngle:double; EndAngle:double);
   private
      { private Definitionen }
      function ParseString(Source:string;Pos:integer):string;
      function ExtractData(Text:string;Pos :integer;Delimiter:char):string;
end;
implementation
uses MainDlg, ActiveX;

var  AscDatei  :Textfile;

     LastObject:string;

     DatenbankObj   :ClassDatenbank;
     RegionObj      :ClassRegion;
     PLineObj       :ClassPLine;
     SplineObj      :ClassSpline;
     PointObj       :ClassPoint;
     EllipseObj     :ClassEllipse;
     TextObj        :ClassText;
     SymbolObj      :ClassSymbol;
     SymbolRefObj   :ClassSymbolRef;
     ImageObj       :ClassImage;
     ArcObj         :ClassArc;

     Layername      :string;
     Bytesrest_anz  :integer;
     Zeile          :string;
     inFile         :file;
     outFile        :Textfile;
     to_odoa_flag   :boolean;
     to_odoa_filename:string;
     Restbytes      :array [1..5080] of char;
     StrFileObj     :ClassStrFile;

constructor ClassFile.Create;
begin
   DatenbankObj:=ClassDatenbank.Create;
   RegionObj:=ClassRegion.Create;
   PLineObj:=ClassPLine.Create;
   SplineObj:=ClassSpline.Create;

   PointObj:=ClassPoint.Create;
   EllipseObj:=ClassEllipse.Create;
   TextObj:=ClassText.Create;
   SymbolObj:=ClassSymbol.Create;
   SymbolRefObj:=ClassSymbolRef.Create;
   ImageObj:=ClassImage.Create;
   ArcObj:=ClassArc.Create;

   Linienstil:='Solid'; Linienfarbe:='$000000';
   Flaechenstil:='None'; Flaechenfarbe:='$000000';
   LastObject:='';
end;

procedure ClassFile.Destroy;
begin
   DatenbankObj.Destroy;
   RegionObj.Destroy;
   PLineObj.Destroy;
   SplineObj.Destroy;
   PointObj.Destroy;
   EllipseObj.Destroy;
   TextObj.Destroy;
   SymbolObj.Destroy;
   SymbolRefObj.Destroy;
   ImageObj.Destroy;
   ArcObj.Destroy;
end;

procedure ClassFile.New_Layer(LName:string;LStil:string;LFarbe:string;FStil:string;FFarbe:string);
var Columnname:widestring;
    Columntype:TOleEnum;
    Columnlength:integer;
begin
   Linienstil:=LStil;
   Linienfarbe:=LFarbe;
   Flaechenstil:=FStil;
   Flaechenfarbe:=FFarbe;
   Layername:=LName;

   { Einf�gen der X und Y Koordinaten in die Datenbank }
   DatenbankObj.InsertColumn('LAYERNAME',length(Layername),Col_String,Layername,false);
   DatenbankObj.InsertColumn('X-KOORDINATE',0,Col_String,'',false);
   DatenbankObj.InsertColumn('Y-KOORDINATE',0,Col_String,'',false);
   DatenbankObj.InsertColumn('Bitmapfilename',0,Col_String,'',false);
   DatenbankObj.InsertColumn('GRAFIK',0,Col_String,'C',true);

   // read database column for this layer
   MainWnd.AXImpExpDbcObj.CheckAndCreateExpTable(Layername);
   while MainWnd.AXImpExpDbcObj.GetExportColumn(Layername, Columnname, Columntype, Columnlength) do
   begin
      DatenbankObj.Insertcolumn(Columnname,Columnlength,Columntype,'',true)
   end;
   MainWnd.AXImpExpDbcObj.OpenExpTable(Layername);
end;

{*******************************************************************************
* PROZEDUR : OpenFile                                                          *
********************************************************************************
* Prozedur dient zum �ffnen eine E00-Files, das sich im im UNIX-Format befindet*
*                                                                              *
* PARAMETER: Dateiname -> Dateiname des E00-Files.                             *
*                                                                              *
********************************************************************************}
procedure ClassFile.OpenFile(Dateiname:string);
begin
   {$i-} Assignfile(InFile,Dateiname); Reset(InFile,1); {$i+}
   {$i-} Assignfile(OutFile,Changefileext(Dateiname,'.tmp')); Rewrite(OutFile); {$i+}
   Zeile:=''; Bytesrest_anz:=0;
end;

{*******************************************************************************
* PROZEDUR : Closefile                                                         *
********************************************************************************
* Prozedur dient zum schliessen der beiden Dateien, die f�r die DOS nach       *
* UNIX Konvertierung ben�tigt wurden.                                          *
*                                                                              *
********************************************************************************}
procedure ClassFile.Closefiles;
begin
   {$i-} Closefile(inFile); Closefile(outFile); {$i+}
end;


{*******************************************************************************
* FUNKTION : Check_odoa                                                        *
********************************************************************************
* Funktion �berpr�ft, ob die Datei vom UNIX ins DOS-Format konvertiert werden  *
* muss.                                                                        *
*                                                                              *
* PARAMETER: Dateiname -> Name der zu pr�fenden Datei                          *
*                                                                              *
* R�CKGABEWERT: TRUE -> wenn konvertiert werden muss, sonst FALSE              *
********************************************************************************}
function ClassFile.Check_odoa(Dateiname:string):boolean;
var Datei:file;
    Actualbytes:array [1..256] of char;
    Bytesread,i:integer;
begin
   Filemode:=0;
   {$i-}
   Assignfile(Datei,Dateiname); Reset(Datei,1);
   {$i+}
   { Einlesen eines 256 Byte Blockes }
   {$i-}Blockread(Datei,Actualbytes,256,Bytesread);{$i+}

   to_odoa_flag:=true;

   if Bytesread > 100 then Bytesread:=100; { Es werden nur

   { Nun wird kontrolliert, ob sich ein <CR> in diesem Buffer befindet }
   for i:=1 to Bytesread do
   begin
      if Actualbytes[i] = CHR(13) then to_odoa_flag:=false;
   end;

   {$i-} Closefile(Datei); {$i+}
   Bytesrest_anz:=0;
   to_odoa_filename:=Changefileext(Dateiname,'.tmp');
   Result:=to_odoa_flag;
end;

{*******************************************************************************
* FUNKTION : OpenASCFile                                                       *
********************************************************************************
* Prozedur dient zum Erzeugen des ASC-Files.                                   *
*                                                                              *
* PARAMETER: Dateiname -> Dateiname des ASC-Files.                             *
*                                                                              *
* R�CKGABEWERT: True-> Wenn Datei ge�ffnet werden konnte, sonst FALSE          *
*                                                                              *
********************************************************************************}
function ClassFile.OpenASCFile(Dateiname:string):boolean;
var allok:boolean;
begin
   Assignfile(AscDatei,Dateiname); allok:=true; Filemode:=1;
   {$i-}
   try
      Rewrite(AscDatei);
   except
      allok:=false;
   end;
   {$i+}
   if IOResult <> 0 then allok:=false;
   Result:=allok;
end;

{*******************************************************************************
* PROZEDUR : CloseallFiles                                                     *
********************************************************************************
* Prozedur dient zum Schliessen aller Files.                                   *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassFile.CloseallFiles;
begin
   {$i-}
   Closefile(AscDatei);
   {$i+}
   MainWnd.AXImpExpDbcObj.CloseExpTables;
end;

{*******************************************************************************
* FUNKTION : ClassFile.GetFilesize                                             *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Bytes des       *
* MIF-Files.                                                                   *
*                                                                              *
* PARAMETER: Dateiname -> Dateiname von der die Bytegr��e ermittelt werden soll*
*                                                                              *
* R�ckgabewert: Anzahl der zu Bearbeitenden Bytes.                             *
********************************************************************************}
function ClassFile.GetFilesize(Dateiname:string):longint;
var Datei:file of byte;
begin
   {$i-}
   AssignFile(Datei, Dateiname);
   {$i+}
   Filemode:=0;
   {$i-}
   Reset(Datei); Result:=FileSize(Datei); Closefile(Datei);
   {$i+}
end;

{*******************************************************************************
* FUNKTION : ClassFile.GetRegionelements                                       *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Region Elemente *
* MIF-Files.                                                                   *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden Region Elemente.                   *
********************************************************************************}
function Classfile.GetRegionElements:longint;
begin
   Result:=RegionObj.GetAnzahl;
   RegionObj.ResetCurrent;
end;

procedure ClassFile.ResetObjectNumber;
begin
   RegionObj.ResetObjectNumber;
end;

function Classfile.GetPlineElements:longint;
begin
   Result:=PLineObj.GetAnzahl;
   PlineObj.ResetCurrent;
end;

function Classfile.GetSplineElements:longint;
begin
   Result:=SplineObj.GetAnzahl;
   SplineObj.ResetCurrent;
end;

function Classfile.GetArcElements:longint;
begin
   Result:=ArcObj.GetAnzahl;
   ArcObj.ResetCurrent;
end;

{*******************************************************************************
* FUNKTION : ClassFile.GetPointElements                                        *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Point  Elemente *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden Point  Elemente.                   *
********************************************************************************}
function Classfile.GetPointElements:longint;
begin
   Result:=PointObj.GetAnzahl;
   PointObj.ResetCurrent;
end;

{*******************************************************************************
* FUNKTION : ClassFile.GetTextElements                                         *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Text Elemente   *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden Text Elemente.                     *
********************************************************************************}
function Classfile.GetTextElements:longint;
begin
   Result:=TextObj.GetAnzahl;
   TextObj.ResetCurrent;
end;

function Classfile.GetSymbolElements:longint;
begin
   Result:=SymbolObj.GetAnzahl;
   SymbolObj.ResetCurrent;
end;

{*******************************************************************************
* FUNKTION : ClassFile.GetEllipseElements                                      *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Ellipse Elemente*
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden Ellipse Elemente.                  *
********************************************************************************}
function Classfile.GetEllipseElements:longint;
begin
   Result:=EllipseObj.GetAnzahl;
   EllipseObj.ResetCurrent;
end;

function Classfile.GetImageElements:longint;
begin
   result:=ImageObj.GetAnzahl;
   ImageObj.ResetCurrent;
end;

{*******************************************************************************
* FUNKTION : ClassFile.WriteCurrentRegion                                      *
********************************************************************************
* Funktion dient zum Schreiben des aktuellen Region Objekts.                   *
*                                                                              *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Region Elemente.                       *
********************************************************************************}
function Classfile.WriteCurrentRegion:longint;
begin
   RegionObj.WriteCurrent;
   RegionObj.SetCurrentNext;
   Result:=1;
end;

function Classfile.WriteCurrentImage:longint;
begin
   ImageObj.WriteCurrent;
   ImageObj.SetCurrentNext;
   Result:=1;
end;

function Classfile.WriteCurrentSymbol:longint;
begin
   SymbolObj.WriteCurrent;
   SymbolObj.SetCurrentNext;
   Result:=1;
end;

{*******************************************************************************
* FUNKTION : ClassFile.WriteCurrentText                                        *
********************************************************************************
* Funktion dient zum Schreiben des aktuellen Text Objekts.                     *
*                                                                              *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Text Elemente.                         *
********************************************************************************}
function Classfile.WriteCurrentText:longint;
begin
   TextObj.WriteCurrent;
   TextObj.SetCurrentNext;
   Result:=1;
end;

{*******************************************************************************
* FUNKTION : ClassFile.WriteCurrentEllipse                                     *
********************************************************************************
* Funktion dient zum Schreiben des aktuellen Ellipse Objekts.                  *
*                                                                              *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Ellipse Elemente.                      *
********************************************************************************}
function Classfile.WriteCurrentEllipse:longint;
begin
   EllipseObj.WriteCurrent;
   EllipseObj.SetCurrentNext;
   Result:=1;
end;

{*******************************************************************************
* FUNKTION : ClassFile.WriteCurrentPoint                                       *
********************************************************************************
* Funktion dient zum Schreiben des aktuellen Point  Objekts.                   *
*                                                                              *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Point Elemente.                        *
********************************************************************************}
function Classfile.WriteCurrentPoint:longint;
begin
   PointObj.WriteCurrent;
   PointObj.SetCurrentNext;
   Result:=1;
end;

function Classfile.WriteCurrentPLine:longint;
begin
   PLineObj.WriteCurrent;
   PLineObj.SetCurrentNext;
   Result:=1;
end;

function Classfile.WriteCurrentSpline:longint;
begin
   SplineObj.WriteCurrent;
   SplineObj.SetCurrentNext;
   Result:=1;
end;

function Classfile.WriteCurrentArc:longint;
begin
   ArcObj.WriteCurrent;
   ArcObj.SetCurrentNext;
   Result:=1;
end;

{*******************************************************************************
* FUNKTION : ClassFile.to_odoa                                                 *
********************************************************************************
* Funktion dient zum Konvertieren des MIF - Files ins DOS-Format.              *
*                                                                              *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Bytes                                  *
********************************************************************************}
function Classfile.to_odoa:longint;
var Actualbytes    :array [1..5080] of char;
    Hilfsbytes     :array [1..5080] of char;
    Bytesread      :integer;
    counter2       :integer;
    Zeile          :string;
    i,j            :integer;
begin
   Zeile:='';

   { Einlesen eines 4096 Byte Blockes }
   {$i-} Blockread(InFile,Actualbytes,4096,Bytesread); {$i+}

   { Rest des vorigen Blockes mu� ber�cksichtigt werden }
   if (Bytesrest_anz <> 0) then
   begin
      { Zeile mu� vor Actualbytes kopiert werden }
      for i:=1 to Bytesrest_anz do Hilfsbytes[i]:=Restbytes[i];

      for j:=1 to Bytesread do
      begin
         Hilfsbytes[i]:=Actualbytes[j]; i:=i + 1;
      end;
      Bytesread:=Bytesread + Bytesrest_anz;
      for i:=1 to Bytesread do Actualbytes[i]:=Hilfsbytes[i];
      Bytesrest_anz:=0;
   end;

   counter2:=1;
   { Nun werden die durch Chr-10 getrennten Zeilen, die sich im Buffer befinden ermittelt }
   repeat
      if Actualbytes[counter2] = CHR(10) then
      begin
         {$i-} writeln(OutFile,Zeile); {$i+}
         counter2:=counter2 + 1;
         Bytesrest_anz:=0;
         Zeile:='';
      end
      else
      begin
         Zeile:=Zeile + Actualbytes[counter2];
         Bytesrest_anz:=Bytesrest_anz + 1;
         Restbytes[Bytesrest_anz]:=Actualbytes[counter2];
         counter2:=counter2 + 1;
      end;
   until counter2 = Bytesread + 1;

   Result:=Bytesread;
end;

{*******************************************************************************
* PROZEDUR : ClassFile.WriteLayerDefinition                                    *
********************************************************************************
* Prozedur schreibt die Layerdefinitionszeile in die ASC-Datei                 *
*                                                                              *
* PARANETER: Keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassFile.WriteLayerDefinition;
var Laenge:integer;
    Zeile,hZeile:string;
begin
   Zeile:='';
   { Schreiben der X-Koordinate }
   Laenge:=DatenbankObj.Getlength('X-KOORDINATE'); hZeile:='';
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben der Y-Koordinate }
   Laenge:=DatenbankObj.Getlength('Y-KOORDINATE'); hZeile:='';
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Parameters A }
   hZeile:=Linienstil; repeat hZeile:=' '+hZeile; until length(hZeile) >= LengthParA;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Parameter B }
   hZeile:=Linienfarbe; repeat hZeile:=' '+hZeile; until length(hZeile) >= LengthParB;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Parameter C }
   hZeile:=''; repeat hZeile:=' ' + hZeile; until length(hZeile) >= LengthParC;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Layernamens }
   hZeile:=DatenbankObj.GetItem('LAYERNAME');
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Objekttyps }
   hZeile:='10';
   Zeile:=Zeile + hZeile;

   { Schreiben der Objektnummer }
   Laenge:=DatenbankObj.GetLength('OBJEKTNR'); Laenge:=6; { Die L�nge der Objektnummer ist immer 5 }
   hZeile:=inttostr(1);
   repeat if length(hZeile) < Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile;

   WriteASC(Zeile);

   Zeile:='';
   { Schreiben der X-Koordinate }
   Laenge:=DatenbankObj.Getlength('X-KOORDINATE'); hZeile:='';
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben der Y-Koordinate }
   Laenge:=DatenbankObj.Getlength('Y-KOORDINATE'); hZeile:='';
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Parameters A }
   hZeile:=Flaechenstil; repeat hZeile:=' '+hZeile; until length(hZeile) >= LengthParA;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Parameter B }
   hZeile:=Flaechenfarbe; repeat hZeile:=' '+hZeile; until length(hZeile) >= LengthParB;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Parameter C }
   hZeile:=''; repeat hZeile:=' ' + hZeile; until length(hZeile) >= LengthParC;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Layernamens }
   hZeile:=DatenbankObj.GetItem('LAYERNAME');
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Objekttyps }
   hZeile:='10';
   Zeile:=Zeile + hZeile;

   { Schreiben der Objektnummer }
   Laenge:=DatenbankObj.GetLength('OBJEKTNR'); Laenge:=6; { Die L�nge der Objektnummer ist immer 5 }
   hZeile:=inttostr(1);
   repeat if length(hZeile) < Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile;

   WriteASC(Zeile);
end;

procedure ClassFile.CreateSym(Dateiname:string);
var Datei:Textfile;
    zeile:string;
    hzeile:string;
    i:integer;
begin
   { Schreiben einer .SYM Datei }
   Assignfile(Datei,Dateiname); Rewrite(Datei);
   for i:=1 to SymbolRefObj.GetAnzahl do
   begin
      hZeile:=inttostr(SymbolRefObj.GetCurrentSymbolref);
      repeat hZeile:='0'+hZeile; until length(hZeile) >= 10;
      Zeile:=hZeile + ' ' + hZeile;
      Writeln(Datei,Zeile);
      SymbolRefObj.SetCurrentNext;
   end;
   if SymbolRefObj.GetAnzahl = 0 then
   begin
      writeln(Datei,'0000000001 0000000001');
   end;
   Closefile(Datei);
end;


{*******************************************************************************
* PROZEDUR : ClassFile.CreateADF                                               *
********************************************************************************
* Prozedur dient zum Erzeugen des ADF-Files                                    *
*                                                                              *
* PARAMETER: Dateiname-> Dateiname der ADF-Datei, die erzeugt werden soll.     *
*                                                                              *
********************************************************************************}
procedure ClassFile.CreateADF(Dateiname:string);
var Datei:Textfile;
    von, bis,Laenge,i,DBAnz:integer;
    vonstr, bisstr :string;
    Bezeichnung:string;
begin
   Assignfile(Datei,Dateiname);
   Rewrite(Datei);
   von:=0; bis:=0;

   { Schreiben der Symbolscale }
   Writeln(Datei,'SYMBOLSCALE');
   Writeln(Datei,'1.0');

   { Schreiben der X-Koordinate }
   Laenge:=DatenbankObj.GetLength('X-KOORDINATE'); bis:=von + Laenge; von:=von + 1; { Damit von nicht 0 ist }
   vonstr:=inttostr(von); repeat if length(vonstr) < 3 then vonstr:='0' + vonstr; until length(vonstr) = 3;
   bisstr:=inttostr(bis); repeat if length(bisstr) < 3 then bisstr:='0' + bisstr; until length(bisstr) = 3;
   Writeln(Datei,'X-COORDINATE'); Writeln(Datei,vonstr + ' ' + bisstr); von:=bis + 1;

   { Schreiben der Y-Koordinate }
   Laenge:=DatenbankObj.GetLength('Y-KOORDINATE'); bis:=von + Laenge;
   vonstr:=inttostr(von); repeat if length(vonstr) < 3 then vonstr:='0' + vonstr; until length(vonstr) = 3;
   bisstr:=inttostr(bis); repeat if length(bisstr) < 3 then bisstr:='0' + bisstr; until length(bisstr) = 3;
   Writeln(Datei,'Y-COORDINATE'); Writeln(Datei,vonstr + ' ' + bisstr); von:=bis + 1;

   { Schreiben des PARAMETERA }
   bis:=von + LengthParA;
   vonstr:=inttostr(von); repeat if length(vonstr) < 3 then vonstr:='0' + vonstr; until length(vonstr) = 3;
   bisstr:=inttostr(bis); repeat if length(bisstr) < 3 then bisstr:='0' + bisstr; until length(bisstr) = 3;
   Writeln(Datei,'PARAMETERA'); Writeln(Datei,vonstr + ' ' + bisstr); von:=bis + 1;

   { Schreiben des PARAMETERB }
   bis:=von + LengthParB;
   vonstr:=inttostr(von); repeat if length(vonstr) < 3 then vonstr:='0' + vonstr; until length(vonstr) = 3;
   bisstr:=inttostr(bis); repeat if length(bisstr) < 3 then bisstr:='0' + bisstr; until length(bisstr) = 3;
   Writeln(Datei,'PARAMETERB'); Writeln(Datei,vonstr + ' ' + bisstr); von:=bis + 1;

   { Schreiben des PARAMETERC }
   bis:=von + LengthParC;
   vonstr:=inttostr(von); repeat if length(vonstr) < 3 then vonstr:='0' + vonstr; until length(vonstr) = 3;
   bisstr:=inttostr(bis); repeat if length(bisstr) < 3 then bisstr:='0' + bisstr; until length(bisstr) = 3;
   Writeln(Datei,'PARAMETERC'); Writeln(Datei,vonstr + ' ' + bisstr); von:=bis + 1;

   { Schreiben des Layernamens }
   Laenge:=DatenbankObj.GetLength('LAYERNAME'); bis:=von + Laenge;
   vonstr:=inttostr(von); repeat if length(vonstr) < 3 then vonstr:='0' + vonstr; until length(vonstr) = 3;
   bisstr:=inttostr(bis); repeat if length(bisstr) < 3 then bisstr:='0' + bisstr; until length(bisstr) = 3;
   Writeln(Datei,'LAYERNAME'); Writeln(Datei,vonstr + ' ' + bisstr); von:=bis + 1;

   { Schreiben des Objekttyps }
   bis:=von + 2; { Die L�nge des Objekttyps ist immer 2 }
   vonstr:=inttostr(von); repeat if length(vonstr) < 3 then vonstr:='0' + vonstr; until length(vonstr) = 3;
   bisstr:=inttostr(bis); repeat if length(bisstr) < 3 then bisstr:='0' + bisstr; until length(bisstr) = 3;
   Writeln(Datei,'OBJEKTTYP'); Writeln(Datei, vonstr + ' ' + bisstr);
   DatenbankObj.Insertcolumn('OBJEKTTYP',1,Col_Int,'',false); von:=bis + 1;

   { Schreiben der Objektnummer }
   bis:=von + 5; { Die L�nge der Objektnummer ist immer 5 }
   vonstr:=inttostr(von); repeat if length(vonstr) < 3 then vonstr:='0' + vonstr; until length(vonstr) = 3;
   bisstr:=inttostr(bis); repeat if length(bisstr) < 3 then bisstr:='0' + bisstr; until length(bisstr) = 3;
   Writeln(Datei,'OBJEKTNR'); Writeln(Datei, vonstr + ' ' + bisstr);
   DatenbankObj.Insertcolumn('OBJEKTNR',5,Col_Int,'',false); von:=bis + 1;

   { Schreiben des Bitmapfilenames }
   if (DatenbankObj.GetLength('Bitmapfilename') > 0) then
   begin
      vonstr:=inttostr(1); repeat if length(vonstr) < 3 then vonstr:='0' + vonstr; until length(vonstr) = 3;
      bisstr:=inttostr(DatenbankObj.GetLength('Bitmapfilename')); repeat if length(bisstr) < 3 then bisstr:='0' + bisstr; until length(bisstr) = 3;
      Writeln(Datei,'Bitmapfilename'); Writeln(Datei, vonstr + ' ' + bisstr);
   end;

   Writeln(Datei,'');

   { Erhalten der Datenbankspalten }
   DBAnz:=DatenbankObj.SetDBFirst;
   if DBAnz > 0 then
   begin
      for i := 1 to DBAnz do { Einf�gen der Spaltennamen in die Datenbankliste }
      begin
         Bezeichnung:=DatenbankObj.GetDBColumnname;
         Laenge:=DatenbankObj.GetLength(Bezeichnung); bis:=von + Laenge;
         if Bezeichnung = 'GRAFIK' then von:=von+1;
         vonstr:=inttostr(von); repeat if length(vonstr) < 3 then vonstr:='0' + vonstr; until length(vonstr) = 3;
         bisstr:=inttostr(bis); repeat if length(bisstr) < 3 then bisstr:='0' + bisstr; until length(bisstr) = 3;
         if Bezeichnung = 'GRAFIK' then von:=von-1;
         if Laenge <> 0 then
         begin
            Writeln(Datei,Bezeichnung); Writeln(Datei,vonstr + ' ' + bisstr);
         end;
         von:=bis + 2;
         DatenbankObj.SetToNextDBInfo;
      end;
   end;
   Closefile(Datei);
end;

{*******************************************************************************
* PROZEDUR : WriteASC                                                          *
********************************************************************************
* Prozedur schreibt einen Eintrag in das ASC-File.                             *
*                                                                              *
* Parameter: Zeile -> Eintrag, der in die ASC-Datei geschrieben wird           *
*                                                                              *
********************************************************************************}
procedure Classfile.WriteASC(Zeile:string);
begin
   writeln(ASCDatei,Zeile);
end;

procedure Classfile.ValDatabase(Index:integer; ObjType:integer; Layername:string);
var column, info:widestring;
begin
   // function is used to get database information for object
   if not MainWnd.AXImpExpDbcObj.DoExportData(Layername,Index) then
      exit; // if no databaseentry has been found for this object exit

   while MainWnd.AXImpExpDbcObj.GetExportData(Layername, column, info) do
   begin
      if      (ObjType = ot_Pixel  ) then begin PointObj.InsertAttrib(Column,Info); DatenbankObj.SetItemLength(Column,length(Info)); end
      else if (ObjType = ot_Cpoly  ) then begin RegionObj.InsertAttrib(Column,Info); DatenbankObj.SetItemLength(Column,length(Info)); end
      else if (ObjType = ot_poly   ) then begin PLineObj.InsertAttrib(Column,Info); DatenbankObj.SetItemLength(Column,length(Info)); end
      else if (ObjType = ot_Spline ) then begin SplineObj.InsertAttrib(Column,Info); DatenbankObj.SetItemLength(Column,length(Info)); end
      else if (ObjType = ot_Text   ) then begin TextObj.InsertAttrib(Column,Info); DatenbankObj.SetItemLength(Column,length(Info)); end
      else if (ObjType = ot_Circle ) then begin EllipseObj.InsertAttrib(Column,Info); DatenbankObj.SetItemLength(Column,length(Info)); end
      else if (ObjType = ot_Symbol ) then begin SymbolObj.InsertAttrib(Column,Info); DatenbankObj.SetItemLength(Column,length(Info)); end
      else if (ObjType = ot_Image  ) then begin ImageObj.InsertAttrib(Column,Info); DatenbankObj.SetItemLength(Column,length(Info)); end;
   end;
end;

procedure ClassFile.New_Point(X:double;Y:double);
begin
   PointObj.Neu(X,Y);
end;

procedure ClassFile.New_Arc(X:double; Y:double; PrimAxis:double; SecAxis:double; aAngle:double; BeginAngle:double; EndAngle:double);
begin
   ArcObj.Neu(X,Y, PrimAxis, SecAxis, aAngle, BeginAngle, EndAngle);
end;

procedure ClassFile.New_Text(X1:double;Y1:double;X2:double;Y2:double;Text:string;Font:string;size:double;angle:integer;DisplayType:string;Alignement:string);
begin
   TextObj.Neu(X1,Y1,X2,Y2,Font,size,angle,DisplayType,Alignement);
   TextObj.InsertAttrib('GRAFIK',Text);
   DatenbankObj.SetItemLength('GRAFIK',length(Text));
end;

procedure ClassFile.New_Line;
begin
   PLineObj.Neu;
end;

procedure ClassFile.InsertPointToLine(X:double;Y:double);
begin
   PLineObj.InsertPoint(X,Y);
end;

procedure ClassFile.New_Spline;
begin
   SplineObj.Neu;
end;

procedure ClassFile.InsertPointToSpline(X:double;Y:double);
begin
   SplineObj.InsertPoint(X,Y);
end;

procedure Classfile.New_Area;
begin
   RegionObj.Neu;
end;

procedure Classfile.InsertPointToArea(X:double;Y:double);
begin
   RegionObj.InsertPoint(X,Y);
end;

procedure Classfile.New_Circle(X:double;Y:double;Radius:integer);
begin
   EllipseObj.Neu(X,Y,Radius);
   DatenbankObj.SetItemLength('X-KOORDINATE',length(floattostr(X)));
   DatenbankObj.SetItemLength('Y-KOORDINATE',length(floattostr(Y)));
end;

procedure Classfile.New_Symbol(X:double; Y:double; Size:double; Angle:double; SymNr:integer);
begin
   SymbolObj.Neu(X,Y,Size,Angle,SymNr);
   DatenbankObj.SetItemLength('X-KOORDINATE',length(floattostr(X)));
   DatenbankObj.SetItemLength('Y-KOORDINATE',length(floattostr(Y)));
   if (not SymbolRefObj.CheckExists(SymNr)) then
      SymbolRefObj.Neu(SymNr);
end;

procedure Classfile.New_Image(X:double; Y:double; Height:double; Width:double; Settings:string; Filename:string);
begin
   ImageObj.Neu(X,Y,Height,Width,Settings, Filename);
   DatenbankObj.SetItemLength('X-KOORDINATE',length(floattostr(X)));
   DatenbankObj.SetItemLength('Y-KOORDINATE',length(floattostr(Y)));
   DatenbankObj.SetItemLength('Bitmapfilename',length(Filename));
end;

{*******************************************************************************
* FUNKTION  : ClassFile.ExtractData                                            *
********************************************************************************
* Prozedur extrahiert aus einem durch Delimiter getrennten String einen        *
* Teilstring                                                                   *
*                                                                              *
* PARAMETER    : Text -> Quellstring aus dem der Teilstring extrahiert wird.   *
*                Pos  -> Position des Teilstrings                              *
*                Delimiter -> Trennzeichen                                     *
*                                                                              *
* R�CKGABEPARAMTER: Teilstring                                                 *
*                                                                              *
********************************************************************************}
function ClassFile.ExtractData(Text:string;Pos :integer;Delimiter:char):string;
var Pos1,Pos2,Stelle,i:integer;
    MyText:pchar;
    gefunden:boolean;
begin
   MyText:=stralloc(length(text)+1);
   strpcopy(MyText,Text);

   { Nun werden die Positionen der Delimiter ermittelt }
   Stelle:=0; Pos1:=0; Pos2:=0; gefunden:=false;
   for i:=0 to strlen(MyText) do
   begin
      if (MyText[i] = Delimiter) or (i = strlen(MyText)) then
      begin
         if gefunden = false then begin Pos1:=Pos2; Pos2:=i; Stelle:=Stelle + 1; end;
         if Stelle = Pos then gefunden:=true;
      end;
   end;
   strdispose(MyText);
   if Pos = 1 then Pos1:=Pos1 - 1;
   if gefunden = true then Result:=copy(Text,Pos1 + 2,Pos2 - Pos1 - 1) else Result:='';
end;

{*******************************************************************************
* FUNKTION : ClassFile.ParseString                                             *
********************************************************************************
* Funktion extrahiert aus der Zeichenkette Source das durch Pos bestimmte      *
* Element.                                                                     *
*                                                                              *
* PARAMETER: Source      -> Text, von dem Ausgegangen wird.                    *
*            Pos         -> Position des Eintrages der erhalten werden soll.   *
*                                                                              *
* R�ckgabewert: Extrahierte Zeichenkette.                                      *
********************************************************************************}
function ClassFile.ParseString(Source:string;Pos:integer):string;
var hpchar:pchar;
    pos1,pos2,stelle:integer;
    hstr:string;
begin
   hpchar:=stralloc(length(Source)+1);
   strpcopy(hpchar,Source);

   pos1:=0; pos2:=0; stelle:=0;
   repeat
      { Zuerst wird bis zum ersten nicht Leerzeichen gegangen }
      repeat
         if hpchar[pos2] = ' ' then pos2:=pos2 + 1;
      until hpchar[pos2] <> ' ';
      pos1:=pos2;
      { Nun wird wieder bis zum ersten Leerzeichen gegangen }
      repeat
         if (hpchar[pos2] <> ' ') and (pos2 <= strlen(hpchar)) then pos2:=pos2 + 1;
      until (hpchar[pos2] = ' ') or (pos2 >= strlen(hpchar)) or
            ((hpchar[pos2] = '-') and (not( (hpchar[pos2-1] = 'E') or (hpchar[pos2-1] = 'e'))));
      stelle:=stelle + 1;
   until (stelle = pos) or (pos2 >=strlen(hpchar));

   if stelle = pos then hstr:=copy(source,pos1 + 1,pos2 - pos1) else hstr:='';
   result:=hstr;
   strdispose(hpchar);
end;

end.
