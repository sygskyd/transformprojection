unit SelAscDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StrFile, StdCtrls, Buttons, FileCtrl, ComCtrls, ExtCtrls, Db, ADODB,
  ShellCtrls, FileSelectionPanel, WinOSInfo;

type
  TSelAscWnd = class(TForm)
    PageControl: TPageControl;
    TabSheetSource: TTabSheet;
    LayerLB: TListBox;
    TabSheetDest: TTabSheet;
    LayerCB: TCheckBox;
    OKBtn: TButton;
    CancelBtn: TButton;
    GeoBtn: TButton;
    Panel2: TPanel;
    SelectedCB: TCheckBox;
    SetupWindowTimer: TTimer;
    Panel1: TPanel;
    SourceDbLabel: TLabel;
    DbBtn: TButton;
    FileSelectionPanel: TFileSelectionPanel;
    ShellTreeView: TShellTreeView;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure LayerCBClick(Sender: TObject);
    procedure SourceDBPCChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GeoBtnClick(Sender: TObject);
    procedure SelectedCBClick(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure DbBtnClick(Sender: TObject);
    procedure LayerLBClick(Sender: TObject);
    procedure FileSelectionPanelDirectoryChanged(Sender: TObject);
  private
    { Private declarations }
    DestinationAscDirectory :string;
    FirstActivate           :boolean;
    procedure CheckWindow;
  public
    { Public declarations }
    winheight:integer;
    winwidth :integer;
    wintop   :integer;
    winleft  :integer;
    OkBtnMode:boolean;
    procedure GetAscDir(var AscDir:string);
    procedure CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
    procedure SetupForBatch;
    procedure SetExpSettings(ProjectDir:string; ExpMode:integer);
    procedure AddExpLayer(ExpLayer:string);
  end;

var
  SelAscWnd     : TSelAscWnd;
  StrFileObj    : ClassStrfile;

implementation

uses Maindlg, inifiles;

{$R *.DFM}

procedure TSelAscWnd.FormCreate(Sender: TObject);
var myScale:double;
    dummy  :double;
    dummystr:string;
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   if MainWnd.REGMODE = DEMO_LICENCE then
      Self.Caption:=StrfileObj.Assign(55)              // PROGIS PXF-ASC Export 2000 Limited
   else
      Self.Caption:=StrfileObj.Assign(1);              // PROGIS PXF-ASC Export 2000
   TabsheetSource.Caption:=StrfileObj.Assign(2);       // Source Project
   TabsheetDest.Caption:=StrfileObj.Assign(3);         // Destination PXF-ASC
   LayerCB.Caption:=StrfileObj.Assign(4);              // Export only selected layers
   SelectedCB.Caption:=StrfileObj.Assign(51);          // Export only selected objects

   OkBtn.Caption:=StrfileObj.Assign(7);                // Ok
   CancelBtn.Caption:=StrfileObj.Assign(8);            // Cancel
   GeoBtn.Caption:=StrfileObj.Assign(9);               // Projection
   DbBtn.Caption:=StrfileObj.Assign(5);                // Database

   LayerLB.enabled:=false;

   DestinationAscDirectory:='';
   FirstActivate:=true;

   Self.Height:=1;
   Self.Width:=1;

   myScale:=1;//100000 / Trunc(9600000/(Self.PixelsPerInch));
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);

   // now adapt winheigt and winwidth
   dummy:=376*myScale; dummystr:=floattostr(dummy);
   if (Pos('.',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos('.',dummystr)-1);
   if (Pos(',',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos(',',dummystr)-1);
   winheight:=strtoint(dummystr);
   dummy:=462*myScale; dummystr:=floattostr(dummy);
   if (Pos('.',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos('.',dummystr)-1);
   if (Pos(',',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos(',',dummystr)-1);
   winwidth:=strtoint(dummystr);
end;

procedure TSelAscWnd.OKBtnClick(Sender: TObject);
var inifile:TIniFile;
begin
   MainWnd.ExitNormal:=true;
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'ConGIS.ini');
   inifile.WriteString('ASCEXP','ExpDbMode',inttostr(MainWnd.AXImpExpDbcObj.ExpDbMode));
   inifile.WriteString('ASCEXP','ExpDatabase',MainWnd.AXImpExpDbcObj.ExpDatabase);
   inifile.WriteString('ASCEXP','ExpTable',MainWnd.AXImpExpDbcObj.ExpTable);
   inifile.WriteString('ASCEXP','LastUsedPath',FileSelectionPanel.Directory);
   inifile.Destroy;
   // now the export window can be opened and the export executed
   Self.Close;
   MainWnd.Show;
end;

procedure TSelAscWnd.CancelBtnClick(Sender: TObject);
begin
   MainWnd.ExitNormal:=false;
   MainWnd.CloseTimer.Enabled:=true;
end;

procedure TSelAscWnd.GetAscDir(var AscDir:string);
begin
   AscDir:=DestinationAscDirectory;
end;

procedure TSelAscWnd.LayerCBClick(Sender: TObject);
begin
   if LayerCB.Checked then
   begin
      LayerLB.enabled:=true;
      SelectedCB.checked:=false;
   end
   else
   begin
      LayerLB.enabled:=false;
   end;
   CheckWindow;
end;

procedure TSelAscWnd.CheckWindow;
var allok:boolean;
    i:integer;
begin
   allok:=true;
   { Procedure checks all settings in the window }
   if DestinationAscDirectory = '' then allok:=false;
   if allok and LayerCb.Checked then
   begin
      allok:=FALSE;
      for i:=0 to LayerLb.Items.Count-1 do
      begin
         if LayerLb.Selected[i] then
         begin
            allok:=True;
            break;
         end;
      end;
   end;
   if allok then OKBtn.Enabled:=true
   else OkBtn.Enabled:=false;
end;


procedure TSelAscWnd.SourceDBPCChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSelAscWnd.FormActivate(Sender: TObject);
var Layername:string;
    layercnt:integer;
    ALayer:Variant;
begin
   if FirstActivate then
   begin
      // fill layerlist
      LayerLB.Items.Clear;
      for layercnt:=0 to MainWnd.SourceDocument.Layers.Count-1 do
      begin
         ALayer:=MainWnd.SourceDocument.Layers.Items[layercnt];
         Layername:=ALayer.Layername;
         LayerLb.Items.Add(Layername);
      end;
      MainWnd.AXImpExpDbcObj.WorkingDir:=OSInfo.WingisDir;
      MainWnd.AXImpExpDbcObj.LngCode:=MainWnd.LngCode;
      MainWnd.AXImpExpDbcObj.ShowMode:=1; // show only source
      SourceDbLabel.Caption:=MainWnd.AxImpExpDbcObj.ExpDbInfo;
      MainWnd.AxGisProjection.WorkingPath:=OSInfo.WingisDir;
      MainWnd.AxGisProjection.LngCode:=MainWnd.LngCode;
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TSelAscWnd.GeoBtnClick(Sender: TObject);
begin
   MainWnd.AxGisProjection.SetupByDialog;
end;

procedure TSelAscWnd.SelectedCBClick(Sender: TObject);
begin
   if SelectedCB.Checked then
   begin
      LayerCB.checked:=false;
   end;
   CheckWindow;
end;

procedure TSelAscWnd.CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
begin
   wintop:=(WHEIGHT div 2) - (winheight div 2);
   winleft:=(WWIDTH div 2) - (winwidth div 2);
end;

procedure TSelAscWnd.SetupWindowTimerTimer(Sender: TObject);
var curdir:string;
    dummy:string;
    inifile:TIniFile;
    intval, count:integer;
begin
   SetupWindowTimer.Enabled:=false;
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,winleft,wintop,winwidth, winheight,SWP_SHOWWINDOW);
   Self.Height:=winheight;
   Self.Width:=winwidth;
   // check if there are objects selected
   if MainWnd.SourceDocument.NrOfSelectedObjects = 0 then
   begin
      SelectedCb.Visible:=FALSE;
   end;
   // read settings from the ConGIS.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'ConGIS.ini');
   dummy:=MainWnd.SourceDocument.Name;
   dummy:=ChangeFileExt(dummy,'.wgi');
   if FileExists(dummy) then
   begin
      MainWnd.AXImpExpDbcObj.ExpDbMode:=5; // idb-Database
      MainWnd.AXImpExpDbcObj.ExpDatabase:='';
      MainWnd.AXImpExpDbcObj.ExpTable:='';
   end
   else
   begin
      dummy:=inifile.ReadString('ASCEXP','ExpDbMode','0');
      val (dummy, intval, count);
      if (intval <> 5) then // Idb-Database
      begin
         MainWnd.AXImpExpDbcObj.ExpDbMode:=intval;
         dummy:=inifile.ReadString('ASCEXP','ExpDatabase','');
         MainWnd.AXImpExpDbcObj.ExpDatabase:=dummy;
         dummy:=inifile.ReadString('ASCEXP','ExpTable','');
         MainWnd.AXImpExpDbcObj.ExpTable:='';
      end;
   end;
   SourceDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ExpDbInfo;
   dummy:=inifile.ReadString('ASCEXP','LastUsedPath','');
   if dummy = '' then
      Getdir(0,curdir)
   else
      curdir:=dummy;
   FileSelectionPanel.Directory:=curdir;
   inifile.Destroy;
   Self.Repaint;
end;

// procedure is used to setup the SelAscWnd for batch mode
procedure TSelAscWnd.SetupForBatch;
var Layername:string;
    layercnt:integer;
    ALayer:Variant;
begin
   if FirstActivate then
   begin
      // fill layerlist
      LayerLB.Items.Clear;
      for layercnt:=0 to MainWnd.SourceDocument.Layers.Count-1 do
      begin
         ALayer:=MainWnd.SourceDocument.Layers.Items[layercnt];
         Layername:=ALayer.Layername;
         LayerLb.Items.Add(Layername);
      end;
      MainWnd.AXImpExpDbcObj.WorkingDir:=OSInfo.WingisDir;
      MainWnd.AXImpExpDbcObj.LngCode:=OSInfo.WingisDir;
      MainWnd.AXImpExpDbcObj.ShowMode:=1; // show only source
      MainWnd.AxGisProjection.WorkingPath:=OSInfo.WingisDir;
      MainWnd.AxGisProjection.LngCode:=OSInfo.WingisDir;
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
   end;
end;

procedure TSelAscWnd.SetExpSettings(ProjectDir:string; ExpMode:integer);
begin
   DestinationAscDirectory:=ProjectDir;
   if (copy(DestinationAscDirectory,length(DestinationAscDirectory),1) <> '\') then
      DestinationAscDirectory:=DestinationAscDirectory+'\';
   if ExpMode = 2 then // export only selected objects
      SelectedCB.Checked:=true;
   if ExpMode = 1 then // export only selected layers
      LayerCB.Checked:=true;
end;

procedure TSelAscWnd.AddExpLayer(ExpLayer:string);
var i:integer;
begin
   // find the Layername in the list and select the item
   for i:=0 to LayerLB.Items.Count-1 do
   begin
      if (LayerLB.Items[i] = ExpLayer) then
      begin
         LayerLB.Selected[i]:=true;
      end;
   end;
end;

procedure TSelAscWnd.DbBtnClick(Sender: TObject);
begin
   MainWnd.AXImpExpDbcObj.SetupByDialog(Self.Handle,5,5);
   PageControl.Enabled:=FALSE;
   GeoBtn.Enabled:=FALSE;
   DbBtn.Enabled:=FALSE;
   OkBtnMode:=OkBtn.Enabled;
   OkBtn.Enabled:=FALSE;
   CancelBtn.Enabled:=FALSE;
end;

procedure TSelAscWnd.LayerLBClick(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSelAscWnd.FileSelectionPanelDirectoryChanged(Sender: TObject);
begin
   DestinationAscDirectory:=FileSelectionPanel.Directory;
   if DestinationAscDirectory <> '' then
   begin
      if DestinationAscDirectory[length(DestinationAscDirectory)] <> '\' then
         DestinationAscDirectory:=DestinationAscDirectory+'\';
   end;
   CheckWindow;
end;

end.
