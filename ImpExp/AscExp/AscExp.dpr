library ascexp;

{ Wichtiger Hinweis zur DLL-Speicherverwaltung: Sharemem mu� die erste
 Unit in der uses-Klausel Ihrer Bibliothek und Ihres Projekts sein,
 wenn Ihre DLL Prozeduren und Funktionen exportiert, die
 Strings als Parameter oder Funktionsergebnisse �bergeben. Dies
 betrifft alle Strings, die an Ihre DLL oder von Ihrer DLL �bergeben werden--selbst diejenigen,
 die sich in Records oder Klassen befinden. Sharemem ist die Schnittstelle zu
 BORLNDMM.DLL, die den gemeinsamen Zugriff auf den Speicher verwaltet; DELPHIMM.DLL
 mu� mit Ihrer DLL weitergegeben werden. Wenn Sie BORLNDMM.DLL nicht verwenden m�chten, m�ssen
 Strings als PChar oder ShortString �bergeben werden.}

uses
  Forms,
  SysUtils,
  Classes,
  Dialogs,
  Maindlg in 'MAINDLG.PAS' {MainWnd},
  Strfile in 'STRFILE.PAS',
  Files in 'Files.pas',
  struct in 'struct.pas',
  comobj,
  AxDll_TLB,
  SelAscDlg in 'SelAscDlg.pas' {SelAscWnd};

{$R *.RES}

{*******************************************************************************
* PROZEDURE * ASCEXPORT                                                        *
********************************************************************************
* Procedure is used to export PXF-ASC data out of GIS                          *
*                                                                              *
********************************************************************************}
procedure ASCEXPORT(DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var aDocument:IDispatch;
begin
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.App:=AXHANDLE;
   aDocument:=SOURCE;
   MainWnd.SourceDocument:=aDocument as IDocument;
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.RegMode:=REGMODE;
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.CreateStringFile;

   SelAscWnd:=TSelAscWnd.Create(nil);
   SelAscWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);

   if (REGMODE = 2) then // check if in Demo-Mode
      Showmessage(StrfileObj.Assign(47)); // PXF-Ascii export in demo mode - only limited number of objects will be exported

   if (REGMODE = 1) or (REGMODE = 2) then   // start only when registered or demo-mode
   begin
      SelAscWnd.Show;
   end
   else
      Showmessage(StrfileObj.Assign(46)); // PXF-Ascii export not registered
end;

{*******************************************************************************
* PROCEDURE * CREATEEXP                                                        *
********************************************************************************
* Procedure is used to prepare an asc-export for Batchmode                     *
*                                                                              *
********************************************************************************}
procedure CREATEEXP(DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var aDocument:IDispatch;
begin
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.App:=AXHANDLE;
   aDocument:=SOURCE;
   MainWnd.SourceDocument:=aDocument as IDocument;
   MainWnd.RegMode:=REGMODE;
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.CreateStringFile;

   // create SelAscWnd
   SelAscWnd:=TSelAscWnd.Create(nil);
   SelAscWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);
   SelAscWnd.SetupForBatch;
end;

{*******************************************************************************
* PROCEDURE * SETEXPSETTINGS                                                   *
********************************************************************************
* Procedure is used to set export project name, export mode and list of layers *
* that should be exported.                                                     *
*                                                                              *
* PARAMETERS: EXPPROJ     -> Name of the diretory into that should be exported *
*             EXPPROJMODE -> Mode how it should be exported.                   *
*                            0 -> Export all layers                            *
*                            1 -> Export selected layers                       *
*                            2 -> Export selected objects                      *
*                                                                              *
********************************************************************************}
procedure SETEXPSETTINGS(EXPPROJ:PCHAR; EXPPROJMODE:INTEGER);stdcall;
begin
   SelAscWnd.SetExpSettings(strpas(EXPPROJ), EXPPROJMODE);
end;

// procedure is used to add a layer that should be exported
// if only selected layers should be exported
procedure ADDEXPLAYER(ALAYER:PCHAR);stdcall;
begin
   SelAscWnd.AddExpLayer(strpas(ALAYER));
end;

// procedure is used to set database-settings
procedure SETDBSETTINGS(SOURCEDBMODE:INTEGER; SOURCEDBNAME:PCHAR; SOURCEDBTABLE:PCHAR;
                        DESTDBMODE  :INTEGER; DESTDBNAME  :PCHAR; DESTDBTABLE  :PCHAR);stdcall;
begin
   // destination will be ignored at ASC-Export
   MainWnd.AXImpExpDbcObj.ExpDbMode:=SOURCEDBMODE;
   MainWnd.AXImpExpDbcObj.ExpDatabase:=strpas(SOURCEDBNAME);
   MainWnd.AXImpExpDbcObj.ExpTable:=strpas(SOURCEDBTABLE);
end;

// procedure is used to set projection for batch mode
procedure SETPROJECTION(SOURCECOORDINATESYSTEM:INTEGER; // source projection
                        SOURCEPROJECTION      :PCHAR;
                        SOURCEDATE            :PCHAR;
                        SOURCEPROJSETTINGS    :PCHAR;
                        SOURCEXOFFSET         :DOUBLE;
                        SOURCEYOFFSET         :DOUBLE;
                        SOURCESCALE           :DOUBLE;
                        TARGETCOORDINATESYSTEM:INTEGER; // target projection
                        TARGETPROJECTION      :PCHAR;
                        TARGETDATE            :PCHAR;
                        TARGETPROJSETTINGS    :PCHAR;
                        TARGETXOFFSET         :DOUBLE;
                        TARGETYOFFSET         :DOUBLE;
                        TARGETSCALE           :DOUBLE);stdcall;
begin
   // setup source coordinate system
   MainWnd.AxGisProjection.SourceCoordinateSystem:=SOURCECOORDINATESYSTEM;
   MainWnd.AxGisProjection.SourceProjection:=strpas(SOURCEPROJECTION);
   MainWnd.AxGisProjection.SourceDate:=strpas(SOURCEDATE);
   MainWnd.AxGisProjection.SourceProjSettings:=strpas(SOURCEPROJSETTINGS);
   MainWnd.AxGisProjection.SourceXOffset:=SOURCEXOFFSET;
   MainWnd.AxGisProjection.SourceYOffset:=SOURCEYOFFSET;
   MainWnd.AxGisProjection.SourceScale:=SOURCESCALE;
   // setup target coordinate system
   MainWnd.AxGisProjection.TargetCoordinateSystem:=TARGETCOORDINATESYSTEM;
   MainWnd.AxGisProjection.TargetProjection:=strpas(TARGETPROJECTION);
   MainWnd.AxGisProjection.TargetDate:=strpas(TARGETDATE);
   MainWnd.AxGisProjection.TargetProjSettings:=strpas(TARGETPROJSETTINGS);
   MainWnd.AxGisProjection.TargetXOffset:=TARGETXOFFSET;
   MainWnd.AxGisProjection.TargetYOffset:=TARGETYOFFSET;
   MainWnd.AxGisProjection.TargetScale:=TARGETSCALE;
end;

// function is used to execute an asc-export
procedure EXECEXP;stdcall;
begin
   MainWnd.ExitNormal:=true;
   MainWnd.Show;
end;

function FREEDLL:boolean; stdcall;
var retVal:boolean;
begin
   retVal:=MainWnd.ExitNormal;
   MainWnd.Free;
   SelASCWnd.Free;
   result:=retVal;
end;

exports ASCEXPORT, CREATEEXP, SETEXPSETTINGS, ADDEXPLAYER,
        SETDBSETTINGS, SETPROJECTION, EXECEXP, FREEDLL;

begin
end.
