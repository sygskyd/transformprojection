unit struct;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs;

const LengthParA = 17;
      LengthParB = 15;
      LengthParC = 15;

      Idx_X_Coordinate      = 0;
      Idx_Y_Coordinate      = 1;
      Num_Reserved_Elems    = 2;

type
  { Hier k�nnen Strukturen definiert werden }
   p_Punkt_record=^Punkt_record;
   Punkt_record = record
       X:double;
       Y:double;
       next :p_Punkt_record;
   end;

   p_Datenbank_record =^Datenbank_record;
   Datenbank_record = record
      Bezeichnung : string;
      Laenge      : integer;
      Typ         : integer;
      Inhalt      : string;
      DBInfo      : boolean;
      next        : p_Datenbank_record;
   end;

   p_Region_record =^Region_record;
   Region_record = record
      CenterX   :double;
      CenterY   :double;
      Punkte    :p_Punkt_record;
      LastCorner:p_Punkt_record;
      DBInfo    :p_Datenbank_record;
      next      :p_Region_record;
   end;

   p_Pline_record = ^Pline_record;
   Pline_record = record
      Punkte   :p_Punkt_record;
      LastCorner:p_Punkt_record;
      DBInfo   :p_Datenbank_record;
      next     :p_PLine_record;
   end;

   p_Spline_record = ^Spline_record;
   Spline_record = record
      Punkte   :p_Punkt_record;
      DBInfo   :p_Datenbank_record;
      next     :p_Spline_record;
   end;

   p_Point_record = ^Point_record;
   Point_record = record
      X     :double;
      Y     :double;
      DBInfo:p_Datenbank_record;
      next  :p_Point_record;
   end;

   p_Ellipse_record = ^Ellipse_record;
   Ellipse_record = record
      X      :double;
      Y      :double;
      Radius :double;
      Labeled:boolean;
      DBInfo :p_Datenbank_record;
      next   :p_Ellipse_record;
   end;

   p_Symbol_record = ^Symbol_record;
   Symbol_record = record
      X      :double;
      Y      :double;
      Size   :double;
      Angle  :double;
      SymNr  :integer;
      DBInfo :p_Datenbank_record;
      next   :p_Symbol_record;
   end;

   p_SymbolRef_record = ^SymbolRef_record;
   SymbolRef_record = record
      SymNr  :integer;
      next   :p_SymbolRef_record;
   end;

   p_Text_record = ^Text_record;
   Text_record = record
      X1     :double;
      Y1     :double;
      X2     :double;
      Y2     :double;
      Font   :string;
      Size   :double;
      Angle  :double;
      DisplayType:string;
      Alignement:string;
      DBInfo :p_Datenbank_record;
      next   :p_Text_record;
   end;

   p_Image_record = ^Image_record;
   Image_record = record
      X:double;
      Y:double;
      Height:double;
      Width:double;
      Settings:string;
      Filename:string;
      DBInfo  :p_Datenbank_record;
      next    :p_Image_record;
   end;

   p_Arc_record = ^Arc_record;
   Arc_record = record
      X:double;
      Y:double;
      PrimAxis:double;
      SecAxis:double;
      Angle:double;
      BeginAngle:double;
      EndAngle:double;
      DBInfo  :p_Datenbank_record;
      next    :p_Arc_record;
   end;


ClassPoint = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X:double;Y:double);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      function  GetAnzahl:longint;
      procedure ResetCurrent;
      procedure SetCurrentNext;
      procedure WriteCurrent;
   private
      { private Definitionen }
      Anfang,Ende,Current :p_Point_record;
      Anzahl              :longint;
end;

ClassArc = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X:double;Y:double;PrimAxis:double;SecAxis:double;Angle:double; BeginAngle:double; EndAngle:double);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      function  GetAnzahl:longint;
      procedure ResetCurrent;
      procedure SetCurrentNext;
      procedure WriteCurrent;
   private
      { private Definitionen }
      Anfang,Ende,Current :p_Arc_record;
      Anzahl              :longint;
end;

ClassSymbolRef = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(SymNr:integer);
      function Checkexists(SymNr:integer):boolean;
      function GetCurrentSymbolRef:integer;
      function GetAnzahl:longint;
      procedure SetCurrentNext;
   private
      { private Definitionen }
      Anfang,Ende,Current :p_SymbolRef_record;
      Anzahl              :longint;
   end;

ClassSymbol = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X:double;Y:double;Size:double;Angle:double;SymNr:integer);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      function  GetAnzahl:longint;
      procedure ResetCurrent;
      procedure SetCurrentNext;
      procedure WriteCurrent;
   private
      { private Definitionen }
      Anfang,Ende,Current :p_Symbol_record;
      Anzahl              :longint;
end;

ClassText = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X1:double;Y1:double;X2:double;Y2:double;Font:string;Size:double;Angle:double;DisplayType:string; Alignement:string);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      procedure SetAngle(Angle:double);
      function  GetAnzahl:longint;
      procedure ResetCurrent;
      procedure SetCurrentNext;
      procedure WriteCurrent;
   private
      { private Definitionen }
      Anfang,Ende,Current :p_Text_record;
      Anzahl              :longint;
end;

ClassEllipse = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X:double;Y:double;Radius:double);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      function  GetAnzahl:longint;
      procedure ResetCurrent;
      procedure SetCurrentNext;
      procedure WriteCurrent;
   private
      { private Definitionen }
      Anfang,Ende,Current :p_Ellipse_record;
      Anzahl              :longint;
end;

ClassRegion = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu;
      procedure InsertPoint(X:double;Y:double);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      procedure SetCenter(X:double;Y:double);
      function  GetAnzahl:longint;
      procedure ResetCurrent;
      procedure SetCurrentNext;
      procedure WriteCurrent;
      procedure ResetObjectNumber;
   private
      { private Definitionen }
      Anfang,Ende,Current :p_Region_record;
      Anzahl              :longint;
end;

ClassPline = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu;
      procedure InsertPoint(X:double;Y:double);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      function  GetAnzahl:longint;
      procedure ResetCurrent;
      procedure SetCurrentNext;
      procedure WriteCurrent;
   private
      { private Definitionen }
      Anfang,Ende,Current :p_PLine_record;
      Anzahl              :longint;
end;

ClassSpline = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu;
      procedure InsertPoint(X:double;Y:double);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      function  GetAnzahl:longint;
      procedure ResetCurrent;
      procedure SetCurrentNext;
      procedure WriteCurrent;
   private
      { private Definitionen }
      Anfang,Ende,Current :p_Spline_record;
      Anzahl              :longint;
end;

ClassImage = class
   public
      // public difintions
      constructor Create;
      destructor Destroy;
      procedure Neu(X:double; Y:double; Height:double; Width:double; Settings:string; Filename:string);
      procedure InsertAttrib(Spaltenname:string; Spalteninhalt:string);
      function GetAnzahl:longint;
      procedure ResetCurrent;
      procedure WriteCurrent;
      procedure SetCurrentNext;
   private
      Anfang, Ende, Current: p_Image_record;
      Anzahl:longint;
end;

ClassDatenbank = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure   Neu(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string;DBInfo:boolean;var MyAnfang:p_datenbank_record);
      function    Checkexists(Eintrag:string):boolean;
      procedure   SetItemLength(Bezeichnung:string; Laenge:integer);
      function    GetLength(Bezeichnung:string):integer;
      function    GetItem(Bezeichnung:string):string;
      procedure   Insertcolumn(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string; DBInfo:boolean);
      function    SetDBFirst:integer;
      function    GetDBColumnname:string;
      procedure   SetCurrentnext;
      procedure   SetDataType(Bezeichnung:string;Inhalt:string);
      procedure   SetToNextDBInfo;
   private
      { private Definitionen }
      function SetReservedColumnLength(const ColName:string; ColLength:integer):boolean;
      function GetReservedColumnLength(const ColName:string):integer;
end;

implementation
uses files, MainDlg;

var FileObj     :ClassFile;
    DatenbankObj:ClassDatenbank;
    Objektnummer:integer;
    DB_Anfang,DB_Current:p_Datenbank_record;
    reservedcolumns:array of integer;

constructor ClassSymbolRef.Create;
begin
   Anfang:=new(p_SymbolRef_record); Ende:=new(p_SymbolRef_record); Current:=new(p_SymbolRef_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassImage.Create;
begin
   Anfang:=new(p_Image_record); Ende:=new(p_Image_record); Current:=new(p_Image_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassArc.Create;
begin
   Anfang:=new(p_Arc_record); Ende:=new(p_Arc_record); Current:=new(p_Arc_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassPoint.Create;
begin
   Anfang:=new(p_Point_record); Ende:=new(p_Point_record); Current:=new(p_Point_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassSymbol.Create;
begin
   Anfang:=new(p_Symbol_record); Ende:=new(p_Symbol_record); Current:=new(p_Symbol_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassText.Create;
begin
   Anfang:=new(p_Text_record); Ende:=new(p_Text_record); Current:=new(p_Text_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassEllipse.Create;
begin
   Anfang:=new(p_Ellipse_record); Ende:=new(p_Ellipse_record); Current:=new(p_Ellipse_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassRegion.Create;
begin
   Anfang:=new(p_Region_record); Ende:=new(p_Region_record); Current:=new(p_Region_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassPLine.Create;
begin
   Anfang:=new(p_PLine_record); Ende:=new(p_PLine_record); Current:=new(p_PLine_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassSpline.Create;
begin
   Anfang:=new(p_Spline_record); Ende:=new(p_Spline_record); Current:=new(p_Spline_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassDatenbank.Create;
var i:integer;
begin
   DB_Anfang:=new(p_Datenbank_record); DB_Current:=new(p_Datenbank_record);
   DB_Anfang:=nil; DB_Current:=nil;
   SetLength(reservedcolumns, Num_Reserved_Elems);
   // reset the reserved columns
   for i:=0 to Num_Reserved_Elems-1 do
   begin
      reservedcolumns[i]:=0;
   end;
end;

destructor ClassDatenbank.Destroy;
var Zeiger1,Zeiger2:p_Datenbank_record;
begin
   Zeiger1:=DB_Anfang; Zeiger2:=DB_Anfang;
   if DB_Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      dispose(Zeiger2);
   until Zeiger1 = nil;

   DB_Anfang:=nil; DB_Current:=nil;
end;

destructor ClassPoint.Destroy;
var Zeiger1,Zeiger2:p_Point_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;

   Anfang:=nil; Current:=nil;
end;

destructor ClassArc.Destroy;
var Zeiger1,Zeiger2:p_Arc_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;

   Anfang:=nil; Current:=nil;
end;

destructor ClassSymbol.Destroy;
var Zeiger1,Zeiger2:p_Symbol_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;

   Anfang:=nil; Current:=nil;
end;

destructor ClassImage.Destroy;
var Zeiger1,Zeiger2:p_Image_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;

   Anfang:=nil; Current:=nil;
end;

destructor ClassSymbolRef.Destroy;
var Zeiger1,Zeiger2:p_SymbolRef_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      dispose(Zeiger2);
   until Zeiger1 = nil;

   Anfang:=nil; Current:=nil;
end;


destructor ClassText.Destroy;
var Zeiger1,Zeiger2:p_Text_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;

   Anfang:=nil; Current:=nil;
end;

destructor ClassEllipse.Destroy;
var Zeiger1,Zeiger2:p_Ellipse_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;

   Anfang:=nil; Current:=nil;
end;


destructor ClassRegion.Destroy;
var Zeiger1,Zeiger2:p_Region_record;
    Punkt1,Punkt2: p_Punkt_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;


      { L�schen der Punkte }
      Punkt1:=Zeiger2^.Punkte; Punkt2:=Zeiger2^.Punkte;
      if Punkt1 <> nil then
      repeat
         Punkt2:=Punkt1;
         Punkt1:=Punkt1^.next;
         dispose(Punkt2);
      until Punkt1 = nil;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassPLine.Destroy;
var Zeiger1,Zeiger2:p_PLine_record;
    Punkt1,Punkt2: p_Punkt_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;


      { L�schen der Punkte }
      Punkt1:=Zeiger2^.Punkte; Punkt2:=Zeiger2^.Punkte;
      if Punkt1 <> nil then
      repeat
         Punkt2:=Punkt1;
         Punkt1:=Punkt1^.next;
         dispose(Punkt2);
      until Punkt1 = nil;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassSpline.Destroy;
var Zeiger1,Zeiger2:p_Spline_record;
    Punkt1,Punkt2: p_Punkt_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;


      { L�schen der Punkte }
      Punkt1:=Zeiger2^.Punkte; Punkt2:=Zeiger2^.Punkte;
      if Punkt1 <> nil then
      repeat
         Punkt2:=Punkt1;
         Punkt1:=Punkt1^.next;
         dispose(Punkt2);
      until Punkt1 = nil;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.Checkexists                                        *
********************************************************************************
* Funktion �berpr�ft, ob der entsprechende Spalteneintrag bereits in der       *
* Datenbankliste ist.                                                          *
*                                                                              *
* PARAMTER: Eintrag -> Name der Spalte, die �berpr�ft werden soll.             *
*                                                                              *
* R�ckgabewert: TRUE  -> Eintrag ist bereits in Liste.                         *
*               FALSE -> Eintrag ist noch nicht in Liste.                      *
*                                                                              *
********************************************************************************}
function ClassDatenbank.Checkexists(Eintrag:string):boolean;
var Zeiger:p_Datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=DB_Anfang; gefunden:=false;
   if DB_Anfang = nil then begin Result:=false; exit; end;
   repeat
      if (Zeiger^.Bezeichnung = Eintrag) then gefunden:= true;
      Zeiger:=Zeiger^.next;
   until (Zeiger = nil) or (gefunden = true);
   Result:=gefunden;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.SetDBFirst                                         *
********************************************************************************
* Funktion setzt den Akt_Datenbank Zeiger auf den ersten Datenbankspalten-     *
* eintrag in der Datenbankliste.                                               *
*                                                                              *
* R�ckgabewert: Anzahl der vorhandenen Datenbankspalten.                       *
*                                                                              *
********************************************************************************}
function ClassDatenbank.SetDBFirst;
var Zeiger:p_Datenbank_record;
    anz:integer;
begin
   Zeiger:=DB_Anfang; anz:=0;
   repeat
      if (Zeiger^.DBInfo) and (anz = 0) then  DB_Current:=Zeiger;
      if Zeiger^.DBInfo then anz:=anz + 1;
      Zeiger:=Zeiger^.next;
   until Zeiger=nil;
   result:=anz;
end;

procedure ClassDatenbank.SetToNextDBInfo;
var found:boolean;
begin
   if DB_Current <> nil then
   begin
      DB_Current:=DB_Current^.next;
      if DB_Current <> nil then
      begin
         found:=false;
         if DB_Current^.DBInfo then found:=TRUE;
         while (not found) do
         begin
            DB_Current:=DB_Current^.next;
            if DB_Current = nil then found:=TRUE;
            if DB_Current <> nil then
            begin
               if DB_Current^.DBInfo then found:=TRUE;
            end;
         end;
      end;
   end;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.GetDBColumnname                                    *
********************************************************************************
* Funktion gibt den Spaltenname auf dem sich gerade der Akt_Datenbank Zeiger   *
* befindet zur�ck.                                                             *
*                                                                              *
* R�ckgabewert: Spaltenname der sich auf Akt_Datenbank befindet.               *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetDBColumnname:string;
begin
   Result:=DB_Current^.Bezeichnung;
end;


{*******************************************************************************
* PROZEDUR : ClassDatenbank.SetCurrentnext                                     *
********************************************************************************
* Prozedur setzt den Akt_Datenbank Zeiger um eines weiter.                     *
*                                                                              *
* R�ckgabewert: KEINER                                                         *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.SetCurrentnext;
begin
   DB_Current:=DB_Current^.next;
end;

{*******************************************************************************
* FUNKTION : ClassRegion.GetAnzahl                                             *
********************************************************************************
* Funktion gibt die Anzahl der eingelesenen Region Elemente.                   *
*                                                                              *
* R�ckgabewert: Anzahl der eingelesenen Region Elemente                        *
*                                                                              *
********************************************************************************}
function ClassRegion.GetAnzahl:longint;
begin
   Result:=Anzahl;
   Objektnummer:=1;
end;

procedure ClassRegion.ResetObjectNumber;
begin
   Objektnummer:=0;
end;

function ClassSymbolRef.GetAnzahl:longint;
begin
   Result:=Anzahl;
   current:=anfang;
end;

function ClassImage.GetAnzahl:longint;
begin
   Result:=Anzahl;
   current:=Anfang;
end;

function ClassSymbolRef.GetCurrentSymbolRef:integer;
begin
   result:=Current^.SymNr;
end;

procedure ClassSymbolRef.SetCurrentNext;
begin
   Current:=Current^.next;
end;

procedure ClassArc.SetCurrentNext;
begin
   Current:=Current^.next;
end;

procedure ClassImage.SetCurrentNext;
begin
   Current:=Current^.next;
end;


function ClassSymbol.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;


function ClassPoint.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassArc.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

{*******************************************************************************
* FUNKTION : ClassText.GetAnzahl                                               *
********************************************************************************
* Funktion gibt die Anzahl der eingelesenen Text Elemente.                     *
*                                                                              *
* R�ckgabewert: Anzahl der eingelesenen Text Elemente                          *
*                                                                              *
********************************************************************************}
function ClassText.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

{*******************************************************************************
* FUNKTION : ClassEllipse.GetAnzahl                                            *
********************************************************************************
* Funktion ermittelt die Anzahl der eingelesenen Ellipse Elemente.             *
*                                                                              *
* R�ckgabewert: Anzahl der eingelesenen Ellipse Elemente.                      *
*                                                                              *
********************************************************************************}
function ClassEllipse.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

{*******************************************************************************
* PROZEDUR : ClassRegion.SetCenter                                             *
********************************************************************************
* Prozedur setzt die Koordinaten des Centerpunktes.                            *
*                                                                              *
* PARAMETER: X -> X Koordinate des Centerpunktes.                              *
*            Y -> Y Koordinate des Centerpunktes.
*                                                                              *
********************************************************************************}
procedure ClassRegion.SetCenter(X:double;Y:double);
begin
   Current^.CenterX:=X;
   Current^.CenterY:=Y;
end;

{*******************************************************************************
* PROZEDUR : ClassText.SetAngle                                                *
********************************************************************************
* Prozedur setzt den Rotationswinkel bei einem Text.                           *
*                                                                              *
* PARAMETER: Angle -> Rotationswinkel                                          *
*                                                                              *
********************************************************************************}
procedure ClassText.SetAngle(Angle:double);
begin
   Current^.Angle:=Angle;
end;

function ClassPLine.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassSpline.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

procedure ClassRegion.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,true,Current^.DBInfo);
   DatenbankObj.SetDataType(Spaltenname,Spalteninhalt);
   DatenbankObj.SetItemLength(Spaltenname,length(Spalteninhalt));
end;

procedure ClassImage.InsertAttrib(Spaltenname:string; Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,true,Current^.DBInfo);
   DatenbankObj.SetDataType(Spaltenname,Spalteninhalt);
   DatenbankObj.SetItemLength(Spaltenname,length(Spalteninhalt));

end;

procedure ClassPoint.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,true,Current^.DBInfo);
   DatenbankObj.SetDataType(Spaltenname,Spalteninhalt);
   DatenbankObj.SetItemLength(Spaltenname,length(Spalteninhalt));
end;

procedure ClassArc.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,true,Current^.DBInfo);
   DatenbankObj.SetDataType(Spaltenname,Spalteninhalt);
   DatenbankObj.SetItemLength(Spaltenname,length(Spalteninhalt));
end;

procedure ClassSymbol.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,true,Current^.DBInfo);
   DatenbankObj.SetDataType(Spaltenname,Spalteninhalt);
   DatenbankObj.SetItemLength(Spaltenname,length(Spalteninhalt));
end;

{*******************************************************************************
* PROZEDUR : ClassText.InsertAttrib                                            *
********************************************************************************
* Prozedur f�gt einen Datenbankinformationseintrag zum Current Text ein.       *
*                                                                              *
* PARAMETER: Spaltenname   -> Name der Spalte die eingef�gt werden soll.       *
*            Spalteninhalt -> Inhalt der Datenbankspalte.                      *
*                                                                              *
********************************************************************************}
procedure ClassText.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,true,Current^.DBInfo);
   DatenbankObj.SetDataType(Spaltenname,Spalteninhalt);
   DatenbankObj.SetItemLength(Spaltenname,length(Spalteninhalt));
end;

{*******************************************************************************
* PROZEDUR : ClassEllipse.InsertAttrib                                         *
********************************************************************************
* Prozedur f�gt einen Datenbankinformationseintrag zum Current Ellipse ein.    *
*                                                                              *
* PARAMETER: Spaltenname   -> Name der Spalte die eingef�gt werden soll.       *
*            Spalteninhalt -> Inhalt der Datenbankspalte.                      *
*                                                                              *
********************************************************************************}
procedure ClassEllipse.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,true,Current^.DBInfo);
   DatenbankObj.SetDataType(Spaltenname,Spalteninhalt);
   DatenbankObj.SetItemLength(Spaltenname,length(Spalteninhalt));
end;

procedure ClassPLine.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,true,Current^.DBInfo);
   DatenbankObj.SetDataType(Spaltenname,Spalteninhalt);
   DatenbankObj.SetItemLength(Spaltenname,length(Spalteninhalt));
end;

procedure ClassSpline.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,true,Current^.DBInfo);
   DatenbankObj.SetDataType(Spaltenname,Spalteninhalt);
   DatenbankObj.SetItemLength(Spaltenname,length(Spalteninhalt));
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.InsertColumn                                       *
********************************************************************************
* Prozedur erzeugt einen neuen Spalteneintrag f�r die Datenbank und f�gt diesen*
* in die Datenbank_anfang Liste ein.                                           *
*                                                                              *
* PARAMETER: Bezeichnung -> Spaltenbezeichnung                                 *
*            Laenge      -> L�nge der Spalte                                   *
*            Typ         -> Typ der Spalte                                     *
*            Inhalt      -> Inhalt der Spalte                                  *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.Insertcolumn(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string; DBInfo:boolean);
begin
   Neu(Bezeichnung, Laenge, Typ, Inhalt,DBInfo,DB_Anfang);
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.SetDataType                                        *
********************************************************************************
* Prozedur wertet das erhaltende Datenbankattribut aus und bestimmt so den     *
* Datentyp f�r die gesamte Spalte.                                             *
*                                                                              *
* PARAMETER: Bezeichnung   -> Spaltenname in die das Attribut eingetragen wird *
*            Inhalt        -> Attribut f�r diese Spalte.                       *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.SetDataType(Bezeichnung:string;Inhalt:string);
var intpossible,floatpossible,gefunden:boolean;
    hint,count:longint;
    hfloat:double;
    Zeiger:p_Datenbank_record;
begin
   intpossible:=false; floatpossible:=false;

   Zeiger:=DB_Anfang; gefunden:=false;
   { Setzen des Zeigers an die entsprechende Datenbankspalte  }
   repeat
      if Zeiger <> nil then if Zeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
      if gefunden = false then zeiger:=zeiger^.next;
   until (gefunden = TRUE) or (Zeiger = nil);

   if gefunden = true then
   begin
      { Kontrolle, ob der Attributwert eine Integervariable ist }
      val(Inhalt,hint,count); if count = 0 then intpossible:=true;

      { Kontrolle, ob der Attributwert eine Floatvariable ist }
      val(Inhalt,hfloat,count); if count = 0 then floatpossible:=true;

      { Nun wird der Datentyp zugewiesen }
      if (Zeiger^.Typ = Col_Int) and (intpossible = FALSE) then Zeiger^.Typ:=Col_Float;
      if (Zeiger^.Typ = Col_Float) and (floatpossible = FALSE) then Zeiger^.Typ:=Col_String;
   end;
end;

procedure ClassRegion.WriteCurrent;
var Zeile,hZeile,Bezeichnung:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i:integer;
    First,gefunden :boolean;
    PunktZeiger:p_Punkt_record;
begin
   if Current = nil then exit;

   { Schreiben der aktuellen Punktdaten }
   Objektnummer:=Objektnummer + 1; first:=true; PunktZeiger:=Current^.Punkte;

   if Punktzeiger = nil then exit;
   repeat
      Zeile:='';
      { Schreiben der X-Koordinate }
      Laenge:=DatenbankObj.Getlength('X-KOORDINATE');
      hZeile:=MainWnd.MyFloatToStr(PunktZeiger^.X);
      repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
      Zeile:=Zeile + hZeile + ' ';

      { Schreiben der Y-Koordinate }
      Laenge:=DatenbankObj.Getlength('Y-KOORDINATE');
      hZeile:=MainWnd.MyFloatToStr(PunktZeiger^.Y);
      repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
      Zeile:=Zeile + hZeile + ' ';

      { Schreiben der Parameter A,B,C }
      hZeile:=''; repeat hZeile:=' '+hZeile; until length(hZeile) >= (LengthParA + LengthParB + LengthParC + 2);
      Zeile:=Zeile + hZeile + ' ';


      { Schreiben des Layernamens }
      hZeile:=DatenbankObj.GetItem('LAYERNAME');
      Zeile:=Zeile + hZeile + ' ';

      { Schreiben des Objekttyps }
      hZeile:=' 2 ';
      Zeile:=Zeile + hZeile;

      { Schreiben der Objektnummer }
      Laenge:=DatenbankObj.GetLength('OBJEKTNR'); Laenge:=5; { Die L�nge der Objektnummer ist immer 5 }
      hZeile:=inttostr(Objektnummer);
      repeat if length(hZeile) < Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
      Zeile:=Zeile + hZeile;

      if first = true then
      begin
         DBAnz:=DatenbankObj.SetDBFirst;
         DBZeiger:=Current^.DBInfo; first:=false;

         for i:=1 to DBAnz do { Nun m�ssen alle Datenbankspalten, die im ADF-vorkommen durchlaufen werden }
         begin
            Bezeichnung:=DatenbankObj.GetDBColumnname;
            Zeile:=Zeile + ' '; DBZeiger:=Current^.DBInfo;
            if DBZeiger <> nil then
            begin
               { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
               DBZeiger:=Current^.DBInfo; gefunden:=false;
               repeat
                  if DBZeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
                  if gefunden = false then DBZeiger:=DBZeiger^.next;
               until (gefunden = true) or (DBZeiger = nil);
               if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
                  hZeile:=DBZeiger^.Inhalt
               else hZeile:='';
            end
            else hZeile:='';
            Laenge:=DatenbankObj.GetLength(Bezeichnung);
            repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
            Zeile:=Zeile + hZeile + ' ';
            DatenbankObj.SetToNextDBInfo;
         end;
      end;
      FileObj.WriteASC(Zeile);
      Punktzeiger:=Punktzeiger^.next;
   until PunktZeiger = nil;
end;

procedure ClassPoint.WriteCurrent;
var Zeile,hZeile,Bezeichnung:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i:integer;
    First,gefunden :boolean;
begin
   if Current = nil then exit;

   { Schreiben der aktuellen Punktdaten }
   Objektnummer:=Objektnummer + 1; first:=true;
   Zeile:='';
   { Schreiben der X-Koordinate }
   Laenge:=DatenbankObj.Getlength('X-KOORDINATE');
   hZeile:=MainWnd.MyFloatToStr(Current^.X);
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben der Y-Koordinate }
   Laenge:=DatenbankObj.Getlength('Y-KOORDINATE');
   hZeile:=MainWnd.MyFloatToStr(Current^.Y);
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben der Parameter A,B,C }
   hZeile:=''; repeat hZeile:=' '+hZeile; until length(hZeile) >= (LengthParA + LengthParB + LengthParC + 2);
   Zeile:=Zeile + hZeile + ' ';


   { Schreiben des Layernamens }
   hZeile:=DatenbankObj.GetItem('LAYERNAME');
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Objekttyps }
   hZeile:=' 0 ';
   Zeile:=Zeile + hZeile;

   { Schreiben der Objektnummer }
   Laenge:=DatenbankObj.GetLength('OBJEKTNR'); Laenge:=5; { Die L�nge der Objektnummer ist immer 5 }
   hZeile:=inttostr(Objektnummer);
   repeat if length(hZeile) < Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile;

   { Schreiben der Datenbankinformation }
   DBAnz:=DatenbankObj.SetDBFirst;
   DBZeiger:=Current^.DBInfo; first:=false;

   for i:=1 to DBAnz do { Nun m�ssen alle Datenbankspalten, die im ADF-vorkommen durchlaufen werden }
   begin
      Bezeichnung:=DatenbankObj.GetDBColumnname;
      Zeile:=Zeile + ' '; DBZeiger:=Current^.DBInfo;
      if DBZeiger <> nil then
      begin
         { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
         DBZeiger:=Current^.DBInfo; gefunden:=false;
         repeat
            if DBZeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
            if gefunden = false then DBZeiger:=DBZeiger^.next;
         until (gefunden = true) or (DBZeiger = nil);
         if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
            hZeile:=DBZeiger^.Inhalt
         else hZeile:='';
      end
      else
         hZeile:='';
      Laenge:=DatenbankObj.GetLength(Bezeichnung);
      repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
      Zeile:=Zeile + hZeile + ' ';
      DatenbankObj.SetToNextDBInfo;
   end;
   FileObj.WriteASC(Zeile);
end;

procedure ClassArc.WriteCurrent;
var Zeile,hZeile,Bezeichnung:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i:integer;
    First,gefunden :boolean;
begin
   if Current = nil then exit;

   Objektnummer:=Objektnummer + 1; first:=true;
   Zeile:='';
   // write X-Coordinate
   Laenge:=DatenbankObj.Getlength('X-KOORDINATE');
   hZeile:=MainWnd.MyFloatToStr(Current^.X);
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   // write Y-Coordinate
   Laenge:=DatenbankObj.Getlength('Y-KOORDINATE');
   hZeile:=MainWnd.MyFloatToStr(Current^.Y);
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   // write Parameter-A (PrimaryAxis)
   hZeile:=MainWnd.MyFloatToStr(Current^.PrimAxis); repeat hZeile:=' ' + hZeile; until length(hZeile) >= LengthParA;
   if (length(hZeile) > LengthParA) then hZeile:=copy(hZeile,1,LengthParA);
   Zeile:=Zeile + hZeile + ' ';

   // write Parameter-B (Beginangle)
   hZeile:=MainWnd.MyFloatToStr(Current^.Beginangle*180/Pi); repeat hZeile:=' ' + hZeile; until length(hZeile) >= LengthParB;
   if (length(hZeile) > LengthParB) then hZeile:=copy(hZeile,1,LengthParB);
   Zeile:=Zeile + hZeile + ' ';

   // write Parameter-C (Angle)
   hZeile:=MainWnd.MyFloatToStr(Current^.Angle*180/Pi); repeat hZeile:=' ' + hZeile; until length(hZeile) >= LengthParC;
   if (length(hZeile) > LengthParC) then hZeile:=copy(hZeile,1,LengthParC);
   Zeile:=Zeile + hZeile + ' ';

   // write layername
   hZeile:=DatenbankObj.GetItem('LAYERNAME');
   Zeile:=Zeile + hZeile + ' ';

   // write objecttype
   hZeile:=' 5 ';
   Zeile:=Zeile + hZeile;

   // write object number
   Laenge:=DatenbankObj.GetLength('OBJEKTNR'); Laenge:=5; // length of objectnumber is 5
   hZeile:=inttostr(Objektnummer);
   repeat if length(hZeile) < Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile;

   // write database information
   DBAnz:=DatenbankObj.SetDBFirst;
   DBZeiger:=Current^.DBInfo; first:=false;

   for i:=1 to DBAnz do { Nun m�ssen alle Datenbankspalten, die im ADF-vorkommen durchlaufen werden }
   begin
      Bezeichnung:=DatenbankObj.GetDBColumnname;
      Zeile:=Zeile + ' '; DBZeiger:=Current^.DBInfo;
      if DBZeiger <> nil then
      begin
         { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
         DBZeiger:=Current^.DBInfo; gefunden:=false;
         repeat
            if DBZeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
            if gefunden = false then DBZeiger:=DBZeiger^.next;
         until (gefunden = true) or (DBZeiger = nil);
         if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
            hZeile:=DBZeiger^.Inhalt
         else hZeile:='';
      end
      else
         hZeile:='';
      Laenge:=DatenbankObj.GetLength(Bezeichnung);
      repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
      Zeile:=Zeile + hZeile + ' ';
      DatenbankObj.SetToNextDBInfo;
   end;
   FileObj.WriteASC(Zeile);

   // now write second line
   Zeile:='';
   // write X-Coordinate
   Laenge:=DatenbankObj.Getlength('X-KOORDINATE');
   hZeile:=MainWnd.MyFloatToStr(Current^.X);
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   // write Y-Coordinate
   Laenge:=DatenbankObj.Getlength('Y-KOORDINATE');
   hZeile:=MainWnd.MyFloatToStr(Current^.Y);
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   // write Parameter-A (SecAxis)
   hZeile:=MainWnd.MyFloatToStr(Current^.SecAxis); repeat hZeile:=' ' + hZeile; until length(hZeile) >= LengthParA;
   if (length(hZeile) > LengthParA) then hZeile:=copy(hZeile,1,LengthParA);
   Zeile:=Zeile + hZeile + ' ';

   // write Parameter-B (Endangle)
   hZeile:=MainWnd.MyFloatToStr(Current^.Endangle*180/Pi); repeat hZeile:=' ' + hZeile; until length(hZeile) >= LengthParB;
   if (length(hZeile) > LengthParB) then hZeile:=copy(hZeile,1,LengthParB);
   Zeile:=Zeile + hZeile + ' ';

   // write Parameter-C (empty)
   hZeile:=''; repeat hZeile:=' ' + hZeile; until length(hZeile) >= LengthParC;
   Zeile:=Zeile + hZeile + ' ';

   // write layername
   hZeile:=DatenbankObj.GetItem('LAYERNAME');
   Zeile:=Zeile + hZeile + ' ';

   // write objecttype
   hZeile:=' 5 ';
   Zeile:=Zeile + hZeile;

   // write object number
   Laenge:=DatenbankObj.GetLength('OBJEKTNR'); Laenge:=5; // length of objectnumber is 5
   hZeile:=inttostr(Objektnummer);
   repeat if length(hZeile) < Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile;
   FileObj.WriteASC(Zeile);
end;

procedure ClassImage.WriteCurrent;
var Zeile,hZeile,Bezeichnung:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i:integer;
    First,gefunden :boolean;
begin
   if Current = nil then exit;

   { Schreiben der aktuellen Punktdaten }
   Objektnummer:=Objektnummer + 1; first:=true;
   Zeile:='';
   { Schreiben der X-Koordinate }
   Laenge:=DatenbankObj.Getlength('X-KOORDINATE');
   hZeile:=MainWnd.MyFloatToStr(Current^.X);
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben der Y-Koordinate }
   Laenge:=DatenbankObj.Getlength('Y-KOORDINATE');
   hZeile:=MainWnd.MyFloatToStr(Current^.Y);
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben der Parameter A - Hier wird die Width gespeichert }
   hZeile:=MainWnd.MyFloatToStr(Current^.Height); repeat hZeile:=' ' + hZeile; until length(hZeile) >= LengthParA;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Parameter B - Hier wird die Height gespeichert }
   hZeile:=MainWnd.MyFloatToStr(Current^.Width);repeat hZeile:=' '+hZeile; until length(hZeile) >= LengthParB;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Parameter C - Hier wirden die Settings gespeichert }
   hZeile:=Current^.Settings;
   repeat hZeile:=' '+hZeile; until length(hZeile) >= LengthParC;
   Zeile:=Zeile + hZeile + ' ';


   { Schreiben des Layernamens }
   hZeile:=DatenbankObj.GetItem('LAYERNAME');
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Objekttyps }
   hZeile:='11 ';
   Zeile:=Zeile + hZeile;

   { Schreiben der Objektnummer }
   Laenge:=DatenbankObj.GetLength('OBJEKTNR'); Laenge:=5; { Die L�nge der Objektnummer ist immer 5 }
   hZeile:=inttostr(Objektnummer);
   repeat if length(hZeile) < Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile;

   { Schreiben der Datenbankinformation }
   DBAnz:=DatenbankObj.SetDBFirst;
   DBZeiger:=Current^.DBInfo; first:=false;

   for i:=1 to DBAnz do { Nun m�ssen alle Datenbankspalten, die im ADF-vorkommen durchlaufen werden }
   begin
      Bezeichnung:=DatenbankObj.GetDBColumnname;
      Zeile:=Zeile + ' '; DBZeiger:=Current^.DBInfo;
      if DBZeiger <> nil then
      begin
         { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
         DBZeiger:=Current^.DBInfo; gefunden:=false;
         repeat
            if DBZeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
            if gefunden = false then DBZeiger:=DBZeiger^.next;
         until (gefunden = true) or (DBZeiger = nil);
         if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
            hZeile:=DBZeiger^.Inhalt
         else hZeile:='';
      end
      else
         hZeile:='';
      Laenge:=DatenbankObj.GetLength(Bezeichnung);
      repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
      Zeile:=Zeile + hZeile + ' ';
      DatenbankObj.SetToNextDBInfo;
   end;
   FileObj.WriteASC(Zeile);

   // in the second line of the definition the filename will be defined
   FileObj.WriteASC(Current^.Filename);
end;

procedure ClassSymbol.WriteCurrent;
var Zeile,hZeile,Bezeichnung:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i:integer;
    First,gefunden :boolean;
begin
   if Current = nil then exit;

   { Schreiben der aktuellen Punktdaten }
   Objektnummer:=Objektnummer + 1; first:=true;

   if (Objektnummer = 137) then
   begin
      Zeile:='';
   end;

   Zeile:='';
   { Schreiben der X-Koordinate }
   Laenge:=DatenbankObj.Getlength('X-KOORDINATE');
   hZeile:=MainWnd.MyFloatToStr(Current^.X);
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben der Y-Koordinate }
   Laenge:=DatenbankObj.Getlength('Y-KOORDINATE');
   hZeile:=MainWnd.MyFloatToStr(Current^.Y);
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben der Parameter A,B,C }
   hZeile:=inttostr(Current^.SymNr);
   repeat hZeile:= ' ' + hZeile; until length(hZeile) >= LengthParA;
   hZeile:=copy(hZeile,1,LengthParA);
   Zeile:=Zeile + hZeile + ' ';

   hZeile:=MainWnd.MyFloatToStr(Current^.Size);
   repeat hZeile:= ' ' + hZeile; until length(hZeile) >= LengthParB;
   hZeile:=copy(hZeile,1,LengthParB);
   Zeile:=Zeile + hZeile + ' ';

   hZeile:=MainWnd.MyFloatToStr(Current^.Angle);
   repeat hZeile:= ' ' + hZeile; until length(hZeile) >= LengthParC;
   hZeile:=copy(hZeile,1,LengthParC);
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Layernamens }
   hZeile:=DatenbankObj.GetItem('LAYERNAME');
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Objekttyps }
   hZeile:=' 3 ';
   Zeile:=Zeile + hZeile;

   { Schreiben der Objektnummer }
   Laenge:=DatenbankObj.GetLength('OBJEKTNR'); Laenge:=5; { Die L�nge der Objektnummer ist immer 5 }
   hZeile:=inttostr(Objektnummer);
   repeat if length(hZeile) < Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile;

   { Schreiben der Datenbankinformation }
   DBAnz:=DatenbankObj.SetDBFirst;
   DBZeiger:=Current^.DBInfo; first:=false;

   for i:=1 to DBAnz do { Nun m�ssen alle Datenbankspalten, die im ADF-vorkommen durchlaufen werden }
   begin
      Bezeichnung:=DatenbankObj.GetDBColumnname;
      Zeile:=Zeile + ' '; DBZeiger:=Current^.DBInfo;
      if DBZeiger <> nil then
      begin
         { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
         DBZeiger:=Current^.DBInfo; gefunden:=false;
         repeat
            if DBZeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
            if gefunden = false then DBZeiger:=DBZeiger^.next;
         until (gefunden = true) or (DBZeiger = nil);
         if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
            hZeile:=DBZeiger^.Inhalt
         else hZeile:='';
      end
      else
         hZeile:='';
      Laenge:=DatenbankObj.GetLength(Bezeichnung);
      repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
      Zeile:=Zeile + hZeile + ' ';
      DatenbankObj.SetToNextDBInfo;
   end;
   FileObj.WriteASC(Zeile);
end;

{*******************************************************************************
* PROZEDUR : ClassText.WriteCurrent                                            *
********************************************************************************
* Prozedur ermittelt die Daten des aktuellen Text und gibt diese an die        *
* Schreibfunktion in der Unit files weiter.                                    *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassText.WriteCurrent;
var Zeile,hZeile,Bezeichnung:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i:integer;
    First,gefunden,WriteTextFlag:boolean;
begin
   if Current = nil then exit;

   Objektnummer:=Objektnummer + 1; first:=true;
   Zeile:=''; WriteTextFlag:=true;

   { Schreiben der ersten Textdefinitionszeile }
   { Schreiben der X-Koordinate }
   Laenge:=DatenbankObj.Getlength('X-KOORDINATE');
   hZeile:=MainWnd.MyFloatToStr(Current^.X1);
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben der Y-Koordinate }
   Laenge:=DatenbankObj.Getlength('Y-KOORDINATE');
   hZeile:=MainWnd.MyFloatToStr(Current^.Y1);
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben der Parameter A - Hier wird der Font gespeichert }
   hZeile:=Current^.Font; repeat hZeile:=hZeile + ' '; until length(hZeile) >= LengthParA;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Parameter B - Hier wird die Fontgr��e gespeichert }
   hZeile:=MainWnd.MyFloatToStr(Current^.size);repeat hZeile:=' '+hZeile; until length(hZeile) >= LengthParB;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Parameter C - Hier wird der Rotationswinkel gespeichert }
   hZeile:=MainWnd.MyFloatToStr(current^.Angle);
   repeat hZeile:=' '+hZeile; until length(hZeile) >= LengthParC;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Layernamens }
   hZeile:=DatenbankObj.GetItem('LAYERNAME');
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Objekttyps }
   hZeile:=' 6 ';
   Zeile:=Zeile + hZeile;

   { Schreiben der Objektnummer }
   Laenge:=DatenbankObj.GetLength('OBJEKTNR'); Laenge:=5; { Die L�nge der Objektnummer ist immer 5 }
   hZeile:=inttostr(Objektnummer);
   repeat if length(hZeile) < Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile;

   { Schreiben der Datenbankinformation }
   DBAnz:=DatenbankObj.SetDBFirst;
   DBZeiger:=Current^.DBInfo; first:=false;

   for i:=1 to DBAnz do { Nun m�ssen alle Datenbankspalten, die im ADF-vorkommen durchlaufen werden }
   begin
      Bezeichnung:=DatenbankObj.GetDBColumnname;
      Zeile:=Zeile + ' '; DBZeiger:=Current^.DBInfo;
      if DBZeiger <> nil then
      begin
         { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
         DBZeiger:=Current^.DBInfo; gefunden:=false;
         repeat
            if DBZeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
            if gefunden = false then DBZeiger:=DBZeiger^.next;
         until (gefunden = true) or (DBZeiger = nil);
         if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
            hZeile:=DBZeiger^.Inhalt
         else hZeile:='';
      end
      else
         hZeile:='';

      { Hier wird abgefangen, dass leere Texte geschrieben werden }
      Laenge:=DatenbankObj.GetLength(Bezeichnung);
      if (Bezeichnung = 'GRAFIK') and (hZeile = '') then WriteTextFlag:=false;
      if (Bezeichnung <> 'GRAFIK') then // Datenbankspalten werden rechtsb�ndig geschrieben
      begin
         repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
      end
      else
      begin // Text wird linksb�ndig geschrieben
         repeat if length(hZeile) <> Laenge then hZeile:=hZeile+ ' '; until length(hZeile) >= Laenge;
      end;
      Zeile:=Zeile + hZeile + ' ';
      DatenbankObj.SetToNextDBInfo;
   end;

   if WriteTextFlag then FileObj.WriteASC(Zeile);

   { Schreiben der zweiten Textdefinitionszeile }
   Zeile:='';
   { Schreiben der X-Koordinate }
   Laenge:=DatenbankObj.Getlength('X-KOORDINATE');
   hZeile:=MainWnd.MyFloatToStr(Current^.X2);
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben der Y-Koordinate }
   Laenge:=DatenbankObj.Getlength('Y-KOORDINATE');
   hZeile:=MainWnd.MyFloatToStr(Current^.Y2);
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben der Parameter A - Hier wird die Fontart gespeichert }
   hZeile:=Current^.DisplayType;
   repeat hZeile:=hZeile + ' '; until length(hZeile) >= LengthParA;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Parameter B - Hier wird die Fontausrichtung gespeichert }
   hZeile:=Current^.Alignement; repeat hZeile:=' '+hZeile; until length(hZeile) >= LengthParB;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Parameter C - Hier wird nichts gespeichert }
   hZeile:=''; repeat hZeile:=' '+hZeile; until length(hZeile) >= LengthParC;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Layernamens }
   hZeile:=DatenbankObj.GetItem('LAYERNAME');
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Objekttyps }
   hZeile:=' 6 ';
   Zeile:=Zeile + hZeile;

   { Schreiben der Objektnummer }
   Laenge:=DatenbankObj.GetLength('OBJEKTNR'); Laenge:=5; { Die L�nge der Objektnummer ist immer 5 }
   hZeile:=inttostr(Objektnummer);
   repeat if length(hZeile) < Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile;

   if WriteTextFlag then FileObj.WriteASC(Zeile);
end;

{*******************************************************************************
* PROZEDUR : ClassEllipse.WriteCurrent                                         *
********************************************************************************
* Prozedur ermittelt die Daten der aktuellen Ellipse  und gibt diese an die    *
* Schreibfunktion in der Unit files weiter.                                    *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassEllipse.WriteCurrent;
var Zeile,hZeile,Bezeichnung,ZeileohneDB:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i:integer;
    First,gefunden :boolean;
begin
   if Current = nil then exit;

   { Schreiben der aktuellen Punktdaten }
   Objektnummer:=Objektnummer + 1; first:=true;
   Zeile:='';
   { Schreiben der X-Koordinate }
   Laenge:=DatenbankObj.Getlength('X-KOORDINATE');
   hZeile:=MainWnd.MyFloatToStr(Current^.X);
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben der Y-Koordinate }
   Laenge:=DatenbankObj.Getlength('Y-KOORDINATE');
   hZeile:=MainWnd.MyFloatToStr(Current^.Y);
   repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile + ' ';


   { Schreiben des Parameter A - Im Parameter A wird Radius gespeichert }
   hZeile:=MainWnd.MyFloatToStr(Current^.Radius);repeat hZeile:=' '+hZeile; until length(hZeile) >= LengthParA;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben der Parameter B }
   hZeile:=''; repeat hZeile:=' '+hZeile; until length(hZeile) >= LengthParB;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Parameter C }
   hZeile:='0.00'; repeat hZeile:=' '+hZeile; until length(hZeile) >= LengthParC;
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Layernamens }
   hZeile:=DatenbankObj.GetItem('LAYERNAME');
   Zeile:=Zeile + hZeile + ' ';

   { Schreiben des Objekttyps }
   hZeile:=' 4 ';
   Zeile:=Zeile + hZeile;

   { Schreiben der Objektnummer }
   Laenge:=DatenbankObj.GetLength('OBJEKTNR'); Laenge:=5; { Die L�nge der Objektnummer ist immer 5 }
   hZeile:=inttostr(Objektnummer);
   repeat if length(hZeile) < Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
   Zeile:=Zeile + hZeile;

   ZeileohneDB:=Zeile;

   { Schreiben der Datenbankinformation }
   DBAnz:=DatenbankObj.SetDBFirst;
   DBZeiger:=Current^.DBInfo; first:=false;

   for i:=1 to DBAnz do { Nun m�ssen alle Datenbankspalten, die im ADF-vorkommen durchlaufen werden }
   begin
      Bezeichnung:=DatenbankObj.GetDBColumnname;
      Zeile:=Zeile + ' '; DBZeiger:=Current^.DBInfo;
      if DBZeiger <> nil then
      begin
         { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
         DBZeiger:=Current^.DBInfo; gefunden:=false;
         repeat
            if DBZeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
            if gefunden = false then DBZeiger:=DBZeiger^.next;
         until (gefunden = true) or (DBZeiger = nil);
         if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
            hZeile:=DBZeiger^.Inhalt
         else hZeile:='';
      end
      else
         hZeile:='';
      Laenge:=DatenbankObj.GetLength(Bezeichnung);
      repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
      Zeile:=Zeile + hZeile + ' ';
      DatenbankObj.SetToNextDBInfo;
   end;
   FileObj.WriteASC(Zeile);
   FileObj.WriteASC(ZeileohneDB);
end;

procedure ClassSpline.WriteCurrent;
var Zeile,hZeile,Bezeichnung:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i:integer;
    First,gefunden :boolean;
    PunktZeiger:p_Punkt_record;
begin
   if Current = nil then exit;

   { Schreiben der aktuellen Punktdaten }
   Objektnummer:=Objektnummer + 1; first:=true; PunktZeiger:=Current^.Punkte;

   if Punktzeiger = nil then exit;
   repeat
      Zeile:='';
      { Schreiben der X-Koordinate }
      Laenge:=DatenbankObj.Getlength('X-KOORDINATE');
      hZeile:=MainWnd.MyFloatToStr(PunktZeiger^.X);
      repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
      Zeile:=Zeile + hZeile + ' ';

      { Schreiben der Y-Koordinate }
      Laenge:=DatenbankObj.Getlength('Y-KOORDINATE');
      hZeile:=MainWnd.MyFloatToStr(PunktZeiger^.Y);
      repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
      Zeile:=Zeile + hZeile + ' ';

      { Schreiben der Parameter A,B,C }
      hZeile:=''; repeat hZeile:=' '+hZeile; until length(hZeile) >= (LengthParA + LengthParB + LengthParC + 2);
      Zeile:=Zeile + hZeile + ' ';


      { Schreiben des Layernamens }
      hZeile:=DatenbankObj.GetItem('LAYERNAME');
      Zeile:=Zeile + hZeile + ' ';

      { Schreiben des Objekttyps }
      hZeile:=' 8 ';
      Zeile:=Zeile + hZeile;

      { Schreiben der Objektnummer }
      Laenge:=DatenbankObj.GetLength('OBJEKTNR'); Laenge:=5; { Die L�nge der Objektnummer ist immer 5 }
      hZeile:=inttostr(Objektnummer);
      repeat if length(hZeile) < Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
      Zeile:=Zeile + hZeile;

      if first = true then
      begin
         DBAnz:=DatenbankObj.SetDBFirst;
         DBZeiger:=Current^.DBInfo; first:=false;

         for i:=1 to DBAnz do { Nun m�ssen alle Datenbankspalten, die im ADF-vorkommen durchlaufen werden }
         begin
            Bezeichnung:=DatenbankObj.GetDBColumnname;
            Zeile:=Zeile + ' '; DBZeiger:=Current^.DBInfo;
            if DBZeiger <> nil then
            begin
               { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
               DBZeiger:=Current^.DBInfo; gefunden:=false;
               repeat
                  if DBZeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
                  if gefunden = false then DBZeiger:=DBZeiger^.next;
               until (gefunden = true) or (DBZeiger = nil);
               if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
                  hZeile:=DBZeiger^.Inhalt
               else hZeile:='';
            end
            else hZeile:='';
            Laenge:=DatenbankObj.GetLength(Bezeichnung);
            repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
            Zeile:=Zeile + hZeile + ' ';
            DatenbankObj.SetToNextDBInfo;
         end;
      end;
      FileObj.WriteASC(Zeile);
      Punktzeiger:=Punktzeiger^.next;
   until PunktZeiger = nil;
end;

procedure ClassPLine.WriteCurrent;
var Zeile,hZeile,Bezeichnung:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i:integer;
    First,gefunden :boolean;
    PunktZeiger:p_Punkt_record;
begin
   if Current = nil then exit;

   { Schreiben der aktuellen Punktdaten }
   Objektnummer:=Objektnummer + 1; first:=true; PunktZeiger:=Current^.Punkte;

   if Punktzeiger = nil then exit;
   repeat
      Zeile:='';
      { Schreiben der X-Koordinate }
      Laenge:=DatenbankObj.Getlength('X-KOORDINATE');
      hZeile:=MainWnd.MyFloatToStr(PunktZeiger^.X);
      repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
      Zeile:=Zeile + hZeile + ' ';

      { Schreiben der Y-Koordinate }
      Laenge:=DatenbankObj.Getlength('Y-KOORDINATE');
      hZeile:=MainWnd.MyFloatToStr(PunktZeiger^.Y);
      repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
      Zeile:=Zeile + hZeile + ' ';

      { Schreiben der Parameter A,B,C }
      hZeile:=''; repeat hZeile:=' '+hZeile; until length(hZeile) >= (LengthParA + LengthParB + LengthParC + 2);
      Zeile:=Zeile + hZeile + ' ';


      { Schreiben des Layernamens }
      hZeile:=DatenbankObj.GetItem('LAYERNAME');
      Zeile:=Zeile + hZeile + ' ';

      { Schreiben des Objekttyps }
      hZeile:=' 1 ';
      Zeile:=Zeile + hZeile;

      { Schreiben der Objektnummer }
      Laenge:=DatenbankObj.GetLength('OBJEKTNR'); Laenge:=5; { Die L�nge der Objektnummer ist immer 5 }
      hZeile:=inttostr(Objektnummer);
      repeat if length(hZeile) < Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
      Zeile:=Zeile + hZeile;

      if first = true then
      begin
         DBAnz:=DatenbankObj.SetDBFirst;
         DBZeiger:=Current^.DBInfo; first:=false;

         for i:=1 to DBAnz do { Nun m�ssen alle Datenbankspalten, die im ADF-vorkommen durchlaufen werden }
         begin
            Bezeichnung:=DatenbankObj.GetDBColumnname;
            Zeile:=Zeile + ' '; DBZeiger:=Current^.DBInfo;
            if DBZeiger <> nil then
            begin
               { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
               DBZeiger:=Current^.DBInfo; gefunden:=false;
               repeat
                  if DBZeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
                  if gefunden = false then DBZeiger:=DBZeiger^.next;
               until (gefunden = true) or (DBZeiger = nil);
               if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
                  hZeile:=DBZeiger^.Inhalt
               else hZeile:='';
            end
            else hZeile:='';
            Laenge:=DatenbankObj.GetLength(Bezeichnung);
            repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
            Zeile:=Zeile + hZeile + ' ';
            DatenbankObj.SetToNextDBInfo;
         end;
      end;
      FileObj.WriteASC(Zeile);
      Punktzeiger:=Punktzeiger^.next;
   until PunktZeiger = nil;
end;

procedure ClassRegion.ResetCurrent;
begin
   current:=anfang;
end;

procedure ClassArc.ResetCurrent;
begin
   current:=anfang;
end;

procedure ClassSymbol.ResetCurrent;
begin
   current:=anfang;
end;

procedure ClassImage.ResetCurrent;
begin
   Current:=Anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassPoint.ResetCurrent                                           *
********************************************************************************
* Prozedur setzt den Current Point Zeiger auf den Anfang.                      *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassPoint.ResetCurrent;
begin
   current:=anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassText.ResetCurrent                                            *
********************************************************************************
* Prozedur setzt den Current Text Zeiger auf den Anfang.                       *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassText.ResetCurrent;
begin
   current:=anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassEllipse.ResetCurrent                                         *
********************************************************************************
* Prozedur setzt den Current Ellipse Zeiger auf den Anfang.                    *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassEllipse.ResetCurrent;
begin
   current:=anfang;
end;

procedure ClassPLine.ResetCurrent;
begin
   current:=anfang;
end;

procedure ClassSpline.ResetCurrent;
begin
   current:=anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassRegion.SetCurrentnext                                        *
********************************************************************************
* Prozedur setzt den Current Region Zeiger auf den n�chsten Eintrag.           *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassRegion.SetCurrentNext;
begin
   if Current <> nil then if Current^.next <> nil then Current:=Current^.next;
end;

procedure ClassSymbol.SetCurrentNext;
begin
   if Current <> nil then if Current^.next <> nil then Current:=Current^.next;
end;

{*******************************************************************************
* PROZEDUR : ClassPoint.SetCurrentnext                                         *
********************************************************************************
* Prozedur setzt den Current Point Zeiger auf den n�chsten Eintrag.            *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassPoint.SetCurrentNext;
begin
   if Current <> nil then if Current^.next <> nil then Current:=Current^.next;
end;

{*******************************************************************************
* PROZEDUR : ClassText.SetCurrentnext                                          *
********************************************************************************
* Prozedur setzt den Current Text Zeiger auf den n�chsten Eintrag.             *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassText.SetCurrentNext;
begin
   if Current <> nil then if Current^.next <> nil then Current:=Current^.next;
end;

{*******************************************************************************
* PROZEDUR : ClassEllipse.SetCurrentnext                                       *
********************************************************************************
* Prozedur setzt den Current Ellipse Zeiger auf den n�chsten Eintrag.          *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassEllipse.SetCurrentNext;
begin
   if Current <> nil then if Current^.next <> nil then Current:=Current^.next;
end;

procedure ClassPline.SetCurrentNext;
begin
   if Current <> nil then if Current^.next <> nil then Current:=Current^.next;
end;

procedure ClassSpline.SetCurrentNext;
begin
   if Current <> nil then if Current^.next <> nil then Current:=Current^.next;
end;

{*******************************************************************************
* PROZEDUR : ClassRegion.Neu                                                   *
********************************************************************************
* Prozedur erzeugt einen neuen Region Eintrag.                                 *
*                                                                              *
* PARAMETER: Keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassRegion.Neu;
var neuer_record,Zeiger:p_Region_record;
begin
   new(neuer_record);
   neuer_record^.Punkte:=nil;
   neuer_record^.DBInfo:=nil;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

procedure ClassSymbol.Neu(X:double;Y:double;Size:double;Angle:double;SymNr:integer);
var neuer_record,Zeiger:p_Symbol_record;
    hstr:string;
begin
   hstr:=MainWnd.MyFloatToStr(X); DatenbankObj.SetItemLength('X-KOORDINATE',length(hstr));
   hstr:=MainWnd.MyFloatToStr(Y); DatenbankObj.SetItemLength('Y-KOORDINATE',length(hstr));

   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.DBInfo:=nil;
   neuer_record^.Size:=Size;
   neuer_record^.Angle:=Angle;
   neuer_record^.SymNr:=SymNr;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

procedure ClassArc.Neu(X:double;Y:double;PrimAxis:double;SecAxis:double;Angle:double; BeginAngle:double; EndAngle:double);
var neuer_record,Zeiger:p_Arc_record;
    hstr:string;
begin
   hstr:=MainWnd.MyFloatToStr(X); DatenbankObj.SetItemLength('X-KOORDINATE',length(hstr));
   hstr:=MainWnd.MyFloatToStr(Y); DatenbankObj.SetItemLength('Y-KOORDINATE',length(hstr));

   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.PrimAxis:=PrimAxis;
   neuer_record^.SecAxis:=SecAxis;
   neuer_record^.Angle:=Angle;
   neuer_record^.BeginAngle:=BeginAngle;
   neuer_record^.EndAngle:=EndAngle;

   neuer_record^.DBInfo:=nil;

      if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

procedure ClassPoint.Neu(X:double;Y:double);
var neuer_record,Zeiger:p_Point_record;
    hstr:string;
begin
   hstr:=MainWnd.MyFloatToStr(X); DatenbankObj.SetItemLength('X-KOORDINATE',length(hstr));
   hstr:=MainWnd.MyFloatToStr(Y); DatenbankObj.SetItemLength('Y-KOORDINATE',length(hstr));

   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.DBInfo:=nil;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

function ClassSymbolRef.Checkexists(SymNr:integer):boolean;
var retVal:boolean;
    Zeiger:p_SymbolRef_record;
begin
   retVal:=false; Zeiger:=Anfang;
   while (Zeiger <> nil) and (not retVal) do
   begin
      if Zeiger^.SymNr = SymNr then retVal:=TRUE;
      Zeiger:=Zeiger^.next;
   end;
   result:=retVal;
end;

procedure ClassSymbolRef.Neu(SymNr:integer);
var neuer_record,Zeiger:p_SymbolRef_record;
begin
   new(neuer_record);
   neuer_record^.SymNr:=SymNr;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassText.Neu                                                     *
********************************************************************************
* Prozedur erzeugt einen neuen Text Eintrag.                                  *
*                                                                              *
* PARAMETER: X,Y -> Koordinaten des neuen Point Objekts.                       *
*                                                                              *
********************************************************************************}
procedure ClassText.Neu(X1:double;Y1:double;X2:double;Y2:double;Font:string;Size:double;Angle:double;DisplayType:string;Alignement:string);
var neuer_record,Zeiger:p_Text_record;
    hstr:string;
begin
   hstr:=MainWnd.MyFloatToStr(X1); DatenbankObj.SetItemLength('X-KOORDINATE',length(hstr));
   hstr:=MainWnd.MyFloatToStr(Y1); DatenbankObj.SetItemLength('Y-KOORDINATE',length(hstr));
   hstr:=MainWnd.MyFloatToStr(X2); DatenbankObj.SetItemLength('X-KOORDINATE',length(hstr));
   hstr:=MainWnd.MyFloatToStr(Y2); DatenbankObj.SetItemLength('Y-KOORDINATE',length(hstr));

   new(neuer_record);
   neuer_record^.X1:=X1; neuer_record^.Y1:=Y1;
   neuer_record^.X2:=X2; neuer_record^.Y2:=Y2;
   neuer_record^.Font:=Font; neuer_record^.size:=Size;
   neuer_record^.Angle:=Angle;
   if neuer_record^.Angle = 360 then neuer_record^.Angle:=0;
   neuer_record^.Alignement:=Alignement;
   neuer_record^.DisplayType:=DisplayType;
   neuer_record^.DBInfo:=nil;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

procedure ClassEllipse.Neu(X:double;Y:double;Radius:double);
var neuer_record,Zeiger:p_Ellipse_record;
    hstr:string;
begin
   hstr:=MainWnd.MyFloatToStr(X); DatenbankObj.SetItemLength('X-KOORDINATE',length(hstr));
   hstr:=MainWnd.MyFloatToStr(Y); DatenbankObj.SetItemLength('Y-KOORDINATE',length(hstr));

   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.Radius:=Radius;
   neuer_record^.DBInfo:=nil;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

procedure ClassImage.Neu(X:double; Y:double; Height:double; Width:double; Settings:string; Filename:string);
var neuer_record,Zeiger:p_Image_record;
    hstr:string;
begin
   hstr:=MainWnd.MyFloatToStr(X); DatenbankObj.SetItemLength('X-KOORDINATE',length(hstr));
   hstr:=MainWnd.MyFloatToStr(Y); DatenbankObj.SetItemLength('Y-KOORDINATE',length(hstr));

   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.Height:=Height;
   neuer_record^.Width:=Width;
   neuer_record^.Settings:=Settings;
   neuer_record^.Filename:=Filename;
   neuer_record^.DBInfo:=nil;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

procedure ClassPLine.Neu;
var neuer_record,Zeiger:p_PLine_record;
begin
   new(neuer_record);
   neuer_record^.Punkte:=nil;
   neuer_record^.DBInfo:=nil;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

procedure ClassSpline.Neu;
var neuer_record,Zeiger:p_Spline_record;
begin
   new(neuer_record);
   neuer_record^.Punkte:=nil;
   neuer_record^.DBInfo:=nil;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.Neu                                                *
********************************************************************************
* Prozedur erzeugt einen neuen Spalteneintrag f�r die Datenbank.               *
*                                                                              *
* PARAMETER: Bezeichnung -> Spaltenbezeichnung                                 *
*            Laenge      -> L�nge der Spalte                                   *
*            Typ         -> Typ der Spalte                                     *
*            Inhalt      -> Inhalt der Spalte                                  *
*            von         -> Vonspalte f�r das Auslesen des E00-Files           *
*            bis         -> Bisspalte f�r das Auslesen des E00-Files           *
*            Anfang      -> Zeiger auf Anfang der Liste in die eingef�gt wird  *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.Neu(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string; DBInfo:boolean; var MyAnfang:p_datenbank_record);
var neuer_record,Zeiger:p_datenbank_record;
begin
   new(neuer_record);

   neuer_record^.Bezeichnung:=Bezeichnung;
   neuer_record^.Laenge:=Laenge;
   neuer_record^.Typ:=Typ;
   neuer_record^.Inhalt:=Inhalt;
   neuer_record^.DBInfo:=DBInfo;

   if MyAnfang = NIL then
   begin
      MyAnfang:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=MyAnfang;
      repeat
         if Zeiger^.next <> nil then Zeiger:=Zeiger^.next;
      until Zeiger^.next = nil;
      Zeiger^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
   end;
end;

function ClassDatenbank.SetReservedColumnLength(const ColName:string; ColLength:integer):boolean;
var retVal:boolean;
    ColIdx:integer;
begin
   retVal:=FALSE;
   ColIdx:=-1;
   if ColName = 'X-KOORDINATE' then
      ColIdx:=Idx_X_Coordinate
   else if ColName = 'Y-KOORDINATE' then
      ColIdx:=Idx_Y_Coordinate;
   if ColIdx <> -1 then
   begin
      if reservedcolumns[ColIdx] < ColLength then
         reservedcolumns[ColIdx]:=ColLength;
      retVal:=TRUE;
   end;
   result:=retVal;
end;

function ClassDatenbank.GetReservedColumnLength(const ColName:string):integer;
var ColIdx:integer;
begin
   result:=-1;
   ColIdx:=-1;
   if ColName = 'X-KOORDINATE' then
      ColIdx:=Idx_X_Coordinate
   else if ColName = 'Y-KOORDINATE' then
      ColIdx:=Idx_Y_Coordinate;
   if ColIdx <> -1 then
   begin
      result:=reservedcolumns[ColIdx];
   end;
end;

procedure ClassDatenbank.SetItemLength(Bezeichnung:string; Laenge:integer);
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   // check if it is a reserved column
   if SetReservedColumnLength(Bezeichnung, Laenge) then
      exit;
   Zeiger:=DB_Anfang; gefunden:=false;
   repeat
      if (Zeiger^.Bezeichnung = Bezeichnung) then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if Zeiger <> nil then
      if Zeiger^.Laenge < Laenge then Zeiger^.Laenge:=Laenge;
end;

function ClassDatenbank.GetLength(Bezeichnung:string):integer;
var Zeiger:p_datenbank_record;
    gefunden:boolean;
    retVal:integer;
begin
   retVal:=GetReservedColumnLength(Bezeichnung);
   if retVal >= 0 then
   begin
      result:=retVal;
      exit;
   end;

   Zeiger:=DB_Anfang; gefunden:=false;
   repeat
      if (Zeiger^.Bezeichnung = Bezeichnung) then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then Result:=Zeiger^.Laenge else Result:=0;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.GetItem                                            *
********************************************************************************
* Funktion dient zum Ermitteln eines Datenbankwertes.                          *
*                                                                              *
*                                                                              *
* PARAMETER:   Bezeichnung -> Name der Spalte                                  *
*                                                                              *
* R�ckgabewert: Inhalt dieser Spalte.                                          *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetItem(Bezeichnung:string):string;
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=DB_Anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then Result:=Zeiger^.Inhalt else Result:='';
end;

{*******************************************************************************
* PROZEDUR : ClassRegion.InsertPoint                                           *
********************************************************************************
* Prozedur f�gt ein Punktpaar in die Punktliste des Current     ein.           *
*                                                                              *
* InPARAMETER: X -> X-Koordinate des neuen Eckpunktes                          *
*              Y -> Y-Koordinate des neuen Eckpunktes                          *
*                                                                              *
********************************************************************************}
procedure ClassRegion.InsertPoint(X:double;Y:double);
var Zeiger:p_Punkt_record;
    new_Zeiger:p_Punkt_record;
    hstr      :string;
begin
   new(new_Zeiger);
   hstr:=MainWnd.MyFloatToStr(X); DatenbankObj.SetItemLength('X-KOORDINATE',length(hstr));
   hstr:=MainWnd.MyFloatToStr(Y); DatenbankObj.SetItemLength('Y-KOORDINATE',length(hstr));

   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   Zeiger:=Current^.Punkte;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current^.Punkte:=New_Zeiger;
      Current^.Punkte^.next:=NIL;
      Current^.LastCorner:=New_Zeiger;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      Current^.LastCorner^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
      Current^.LastCorner:=new_Zeiger;
   end;
end;

procedure ClassSpline.InsertPoint(X:double;Y:double);
var Zeiger:p_Punkt_record;
    new_Zeiger:p_Punkt_record;
    hstr      :string;
begin
   new(new_Zeiger);
   hstr:=MainWnd.MyFloatToStr(X); DatenbankObj.SetItemLength('X-KOORDINATE',length(hstr));
   hstr:=MainWnd.MyFloatToStr(Y); DatenbankObj.SetItemLength('Y-KOORDINATE',length(hstr));

   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   Zeiger:=Current^.Punkte;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current^.Punkte:=New_Zeiger;
      Current^.Punkte^.next:=NIL;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
   end;
end;

procedure ClassPLine.InsertPoint(X:double;Y:double);
var Zeiger:p_Punkt_record;
    new_Zeiger:p_Punkt_record;
    hstr      :string;
begin
   new(new_Zeiger);

   hstr:=MainWnd.MyFloatToStr(X); DatenbankObj.SetItemLength('X-KOORDINATE',length(hstr));
   hstr:=MainWnd.MyFloatToStr(Y); DatenbankObj.SetItemLength('Y-KOORDINATE',length(hstr));

   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   Zeiger:=Current^.Punkte;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current^.Punkte:=New_Zeiger;
      Current^.Punkte^.next:=NIL;
      Current^.LastCorner:=New_Zeiger;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      Current^.LastCorner^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
      Current^.LastCorner:=new_Zeiger;
   end;
end;

end.
