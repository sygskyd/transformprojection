object SetShpfileWnd: TSetShpfileWnd
  Left = 340
  Top = 174
  BorderStyle = bsDialog
  Caption = 'WinGIS SHP-Import 2000'
  ClientHeight = 351
  ClientWidth = 456
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 8
    Top = 8
    Width = 441
    Height = 305
    ActivePage = TabSheetFiles
    TabIndex = 0
    TabOrder = 0
    object TabSheetFiles: TTabSheet
      Caption = 'Files'
      object AprCB: TCheckBox
        Left = 0
        Top = 224
        Width = 73
        Height = 17
        Caption = 'Apr-File:'
        TabOrder = 0
        OnClick = AprCBClick
      end
      object AprPanel: TPanel
        Left = 72
        Top = 220
        Width = 250
        Height = 25
        TabOrder = 1
        object AprLabel: TLabel
          Left = 8
          Top = 8
          Width = 42
          Height = 13
          Caption = 'AprLabel'
        end
      end
      object AprBtn: TButton
        Left = 320
        Top = 220
        Width = 25
        Height = 25
        Caption = '...'
        TabOrder = 2
        OnClick = AprBtnClick
      end
      object Panel1: TPanel
        Left = 0
        Top = 248
        Width = 432
        Height = 25
        TabOrder = 3
        object DestDbLabel: TLabel
          Left = 8
          Top = 8
          Width = 62
          Height = 13
          Caption = 'DestDbLabel'
        end
      end
      object FileSelectionPanel: TFileSelectionPanel
        Left = 0
        Top = 0
        Width = 433
        Height = 217
        BevelOuter = bvLowered
        TabOrder = 4
        Mask = '*.shp'
        Directory = 'C:\Users\aigner\Desktop'
        DirectoryShellTreeView = ShellTreeView
        FileListbox = FileListBox
        ConvertListbox = ListBox
        AddAllBtn = AddAllBtn
        RemoveAllBtn = DeleteAllBtn
        TrashImage = TrashImage
        TrashPanel = TrashPanel
        OnFilesToConvertChanged = FileSelectionPanelFilesToConvertChanged
        object ShellTreeView: TShellTreeView
          Left = 2
          Top = 2
          Width = 157
          Height = 211
          ObjectTypes = [otFolders]
          Root = 'rfDesktop'
          UseShellImages = True
          AutoRefresh = False
          Indent = 19
          ParentColor = False
          RightClickSelect = True
          ShowRoot = False
          TabOrder = 0
        end
        object FileListBox: TFileListBox
          Left = 160
          Top = 2
          Width = 125
          Height = 211
          DragMode = dmAutomatic
          ItemHeight = 13
          Mask = '*.shp'
          MultiSelect = True
          TabOrder = 1
        end
        object ListBox: TListBox
          Left = 314
          Top = 2
          Width = 115
          Height = 211
          DragMode = dmAutomatic
          ItemHeight = 13
          MultiSelect = True
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object TrashPanel: TPanel
          Left = 286
          Top = 2
          Width = 27
          Height = 211
          BevelOuter = bvLowered
          DragMode = dmAutomatic
          TabOrder = 3
          object TrashImage: TImage
            Left = 1
            Top = 88
            Width = 25
            Height = 25
            AutoSize = True
            DragMode = dmAutomatic
            Picture.Data = {
              07544269746D6170F2060000424DF20600000000000036040000280000001900
              0000190000000100080000000000BC020000202E0000202E0000000100000000
              000000000000007B00007B7B7B00BDBDBD00C6C6C6000000FF00002B0000002B
              5500002B8000002BAA00002BD500002BFF000055000000555500005580000055
              AA000055D5000055FF000080000000805500008080000080AA000080D5000080
              FF0000AA000000AA550000AA800000AAAA0000AAD50000AAFF0000D5000000D5
              550000D5800000D5AA0000D5D50000D5FF0000FF000000FF550000FF800000FF
              AA0000FFD50000FFFF005500000055005500550080005500AA005500D5005500
              FF00552B0000552B5500552B8000552BAA00552BD500552BFF00555500005555
              5500555580005555AA005555D5005555FF005580000055805500558080005580
              AA005580D5005580FF0055AA000055AA550055AA800055AAAA0055AAD50055AA
              FF0055D5000055D5550055D5800055D5AA0055D5D50055D5FF0055FF000055FF
              550055FF800055FFAA0055FFD50055FFFF008000000080005500800080008000
              AA008000D5008000FF00802B0000802B5500802B8000802BAA00802BD500802B
              FF008055000080555500805580008055AA008055D5008055FF00808000008080
              5500808080008080AA008080D5008080FF0080AA000080AA550080AA800080AA
              AA0080AAD50080AAFF0080D5000080D5550080D5800080D5AA0080D5D50080D5
              FF0080FF000080FF550080FF800080FFAA0080FFD50080FFFF00AA000000AA00
              5500AA008000AA00AA00AA00D500AA00FF00AA2B0000AA2B5500AA2B8000AA2B
              AA00AA2BD500AA2BFF00AA550000AA555500AA558000AA55AA00AA55D500AA55
              FF00AA800000AA805500AA808000AA80AA00AA80D500AA80FF00AAAA0000AAAA
              5500AAAA8000AAAAAA00AAAAD500AAAAFF00AAD50000AAD55500AAD58000AAD5
              AA00AAD5D500AAD5FF00AAFF0000AAFF5500AAFF8000AAFFAA00AAFFD500AAFF
              FF00D5000000D5005500D5008000D500AA00D500D500D500FF00D52B0000D52B
              5500D52B8000D52BAA00D52BD500D52BFF00D5550000D5555500D5558000D555
              AA00D555D500D555FF00D5800000D5805500D5808000D580AA00D580D500D580
              FF00D5AA0000D5AA5500D5AA8000D5AAAA00D5AAD500D5AAFF00D5D50000D5D5
              5500D5D58000D5D5AA00D5D5D500D5D5FF00D5FF0000D5FF5500D5FF8000D5FF
              AA00D5FFD500D5FFFF00FF000000FF005500FF008000FF00AA00FF00D500FF00
              FF00FF2B0000FF2B5500FF2B8000FF2BAA00FF2BD500FF2BFF00FF550000FF55
              5500FF558000FF55AA00FF55D500FF55FF00FF800000FF805500FF808000FF80
              AA00FF80D500FF80FF00FFAA0000FFAA5500FFAA8000FFAAAA00FFAAD500FFAA
              FF00FFD50000FFD55500FFD58000FFD5AA00FFD5D500FFD5FF00FFFF0000FFFF
              5500FFFF8000FFFFAA00FFFFD5003F3F3F006B6B6B0095959500C3C3C300FFFF
              FF00040404040404040404040404040400000404040404040404040000000404
              0404040404040404040002020302020200040404040404000000040404040404
              0404040400020303030202020200040404040400000004040404040404040000
              0203030303020202020004040404040000000404040404000002030303030303
              0302020202000404040404000000040404040202020303020303030303020202
              0202000404040400000004040404020303030201010103030302020202020004
              0404040000000404040402030103030103030103030202020202000404040400
              0000040404040301010303030303010303020202020202040404040000000404
              0404030101030303030301030302020202020204040404000000040404040301
              0101030301010103030202020202020404040400000004040402030301010303
              0301010303020202020202000404040000000404040203010101030301010103
              0302020202020200040404000000040404020303030303020103FFFFFFFF0202
              020202000404040000000404020303030303030303FF02020303FFFF02020202
              0004040000000404020303030303FFFF02020302FFFFFFFF030303FF00040400
              000004040203030303FF0202FF0202FFFFFFFFFF030303FF0004040000000402
              030303FFFF02FFFFFF02FF02FFFFFFFFFF02FF020404040000000402FFFF02FF
              FF02FFFF03FFFF0203FFFFFF020404040404040000000402FF0202FFFF02FF02
              FFFFFF02FFFFFF02020404040404040000000402FFFF0202FF02FF02FFFF02FF
              FF0202FF02040404040404000000040404040202FFFF0202FFFF020202FFFF02
              040404040404040000000404040404040202FFFF020202040402020404040404
              0404040000000404040404040404020204040204040404040404040404040400
              000004040404040404040404040404040404040404040404040404000000}
            Visible = False
          end
          object AddAllBtn: TButton
            Left = 1
            Top = 44
            Width = 25
            Height = 25
            Caption = '>'
            TabOrder = 0
          end
          object DeleteAllBtn: TButton
            Left = 1
            Top = 132
            Width = 25
            Height = 25
            Caption = '<'
            TabOrder = 1
          end
        end
      end
      object DkmCb: TCheckBox
        Left = 352
        Top = 224
        Width = 73
        Height = 17
        Caption = 'DKM'
        TabOrder = 5
        Visible = False
      end
    end
  end
  object GeoBtn: TButton
    Left = 7
    Top = 320
    Width = 93
    Height = 23
    Caption = 'Projection'
    Default = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = GeoBtnClick
  end
  object OKBtn: TButton
    Left = 240
    Top = 320
    Width = 93
    Height = 23
    Caption = 'Ok'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 2
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 348
    Top = 320
    Width = 101
    Height = 23
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
    OnClick = CancelBtnClick
  end
  object DbBtn: TButton
    Left = 104
    Top = 320
    Width = 93
    Height = 23
    Caption = 'Database'
    TabOrder = 4
    OnClick = DbBtnClick
  end
  object SetupWindowTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = SetupWindowTimerTimer
    Left = 208
    Top = 320
  end
  object OpenDialogApr: TOpenDialog
    DefaultExt = '*.apr'
    FileName = '*.apr'
    Filter = 'ArcView Project File|*.apr'
    Left = 324
  end
end
