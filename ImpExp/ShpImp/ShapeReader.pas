unit ShapeReader;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Math, ImpExpObjects, {ShpDkmManager,}  Db;

const
       Col_Int      = 0;
       Col_SmallInt = 1;
       Col_Float    = 2;
       Col_Date     = 3;
       Col_String   = 4;

type
  { Hier k�nnen Strukturen definiert werden }
  p_double = ^doublewert;
  doublewert = record
     Zahl :double;
  end;

  p_integer = ^integerwert;

  integerwert = record
     Zahl :integer;
  end;

TShapeReader = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy; override;
      function    OpenSHPFile(Filename :string):longint; // open Shape-File and return size
      function    OpenDBFFile(Filename :string):boolean; // open database file that belongs to Shape
      procedure   CloseAllFiles;                         // close all files
      function    ReadHeader:longint;                    // read header of Shape-File
      function    ReadElement:longint;                   // read Shape-Object
      procedure   AddObjectToLayer(const aObject:TImpExpBaseObj);
      procedure   SetObjectKeeper(const aKeeper:TImpExpObjectKeeper);
      //procedure   SetDkmManager(const aDkmMgr:TShpDkmManager);
   private
      { private Definitionen }
      ShpFileLayer     :TImpExpLayer;    // layer object that is used store objects of shape-file
      DataMgr          :TImpExpDataMgr;  // data manager for read objects
      //DkmMgr           :TShpDkmManager;
      ShpFile          :file;    // File that will be parsed
      Database_ok      :boolean; // flag if database should be used
      NewDbfFilename   :string;  // name of new database
      ObjectKeeper     :TImpExpObjectKeeper;

      // base parsing methods
      procedure ValInteger(Feld :array of byte;Typ:string;var Erg:longint);
      function  ReadInteger(IntType:string;var IntValue:longint):boolean;
      procedure ReadDouble(Typ:string;var Erg:double);
      procedure ValDouble(Feld :array of byte;Typ:string;var Erg:double);

      // methods to parse shape-objects
      function Read_Null:longint;
      function Read_Point:longint;
      function Read_Polyline:longint;
      function Read_Polygon:longint;
      function Read_MultiPoint:longint;
      function Read_PointZ:longint;
      function Read_PolylineZ:longint;
      function Read_PolygonZ:longint;
      function Read_MultiPointZ:longint;
      function Read_PointM:longint;
      function Read_PolylineM:longint;
      function Read_PolygonM:longint;
      function Read_MultiPointM:longint;
      function Read_MultiPatch:longint;
      function ReadRecordHeader(var RecordNumber:longint; var RecordLength:longint):boolean;
      procedure  ReadDatabaseInfo(var aObject:TImpExpBaseObj);
      // other methods
      procedure   CopyAFile(Source:string; Dest:string); // Copy file method
      procedure   ProcessReadObjects;
      procedure ImportElements;
end;
implementation
uses MainDlg;

// constructor of Shape-Reader
constructor TShapeReader.Create;
begin
   ShpFileLayer:=TImpExpLayer.Create;  // create layer object to store Shape objects
   DataMgr:=TImpExpDataMgr.Create;
   //DkmMgr:=nil;
   ObjectKeeper:=nil;
end;

// destructor of Shape-Reader
destructor TShapeReader.Destroy;
begin
   if ShpFileLayer <> nil then ShpFileLayer.Destroy; // remove Shape-layer object
   if DataMgr <> nil then DataMgr.Destroy;
end;

// method is used to open Shape-File and return number of Bytes to process
function TShapeReader.OpenSHPFile(Filename:string):longint;
var aLayername:string;
    aFile:file of byte;
begin
   // first get number of bytes to process
   {$I-}
   AssignFile(aFile, Filename);
   Filemode:=0;
   Reset(aFile);
   Result:=FileSize(aFile);
   Closefile(aFile);
   {$I+}

   // now open file for parsing
   {$I-}
   Assignfile(ShpFile,Filename);
   Reset(ShpFile,1); // open binary
   {$I+}

   // name of layer is same than of Shapefile
   aLayername:=ExtractFilename(Filename); aLayername:=copy(aLayername,1,Pos('.',aLayername)-1);
   ShpFileLayer.Name:=aLayername;
end;

// method is used to open Shape database file
function TShapeReader.OpenDbfFile(Filename:string):boolean;
var i:integer;
    ColName:string;
    ColType:integer;
    ColLength:integer;
    dummy:string;
begin
   result:=TRUE;
   if MainWnd.AXImpExpDbc.ImpDbMode = 0 then // Db_None
   begin
      Database_ok:=false;
      exit;
   end;

   if Fileexists(Filename) then
   begin
      // the dBase file will always copyied to C:\temp\shpimp.dbf during import to
      // avoid conflicts of during import due to long path or filename
      NewDbfFilename:=MainWnd.AXImpExpDbc.TmpDBaseDir+'shpimp.dbf';
      if (FileExists(NewDbfFilename)) then // if the table already exists delete it
         DeleteFile(PCHAR(NewDbfFilename));

      CopyAFile(Filename, NewDbfFilename);
      if not MainWnd.ADOConnection.Connected then
      begin
         // open ADO Connection
         MainWnd.ADOConnection.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False';
         MainWnd.ADOConnection.Properties['Extended Properties'].Value := 'dBASE 5.0';
         dummy:=ExtractFileDir(ExtractShortPathName(Filename));
         MainWnd.ADOConnection.Properties['Data Source'].Value :=ExtractFileDir(NewDbfFilename);;
         MainWnd.ADOConnection.Connected:=True;
      end;
      MainWnd.ADOTable.TableName:='['+ExtractFilename(NewDbfFilename)+']';
      MainWnd.ADOTable.Connection:=MainWnd.ADOConnection;
      // read out the database columns
      try
         MainWnd.ADOTable.Open;
      except
         Database_ok:=FALSE;
         MainWnd.ADOConnection.Connected:=FALSE;
         DeleteFile(PCHAR(NewDbfFilename)); // delete the temporary .dbf file
         // Show warning that there had been problems during open of database
         result:=FALSE;
         exit;
      end;
      MainWnd.ADOTable.First;
      for i := 0 to MainWnd.ADOTable.FieldCount - 1 do { Einf�gen der Spaltennamen in die Datenbankliste }
      begin
         ColName:=MainWnd.ADOTable.Fields[i].FieldName;
         ColLength:=MainWnd.ADOTable.Fields[i].Size;
              if (MainWnd.ADOTable.Fields[i].DataType = ftInteger)  then ColType:=Col_Int
         else if (MainWnd.ADOTable.Fields[i].DataType = ftSmallint) then ColType:=Col_SmallInt
         else if (MainWnd.ADOTable.Fields[i].DataType = ftFloat)    then ColType:=Col_Float
         else if (MainWnd.ADOTable.Fields[i].DataType = ftDate)     then ColType:=Col_Date
         else ColType:=Col_String;
         ShpFileLayer.AddColumnDef(ColName, ColType, ColLength); // add attrib column to layer
      end;
      Database_ok:=true;
   end
   else
      Database_ok:=false;
end;

// method to copy file
procedure TShapeReader.CopyAFile(Source:string; Dest:string);
var SourceFile:File;
    DestFile  :File;
    Actualbytes:array [1..4096] of char;
    numread      :integer;
    numwritten   :integer;
    OldFilemode  :integer;
begin
   OldFileMode:=Filemode;
   {$I-}
   if not DirectoryExists(ExtractFilePath(Dest)) then
      CreateDir(ExtractFilePath(Dest));

   Filemode:=0; Assignfile(SourceFile,Source); Reset(SourceFile,1);
   Filemode:=1; Assignfile(DestFile,Dest); Rewrite(DestFile,1);
   repeat
      BlockRead(SourceFile, ActualBytes, SizeOf(ActualBytes), NumRead);
      BlockWrite(DestFile, ActualBytes, NumRead, NumWritten);
   until (NumRead = 0) or (NumWritten <> NumRead);
   Closefile(SourceFile);
   Closefile(DestFile);
   {$I+}
   FileMode:=OldFilemode;
end;

// method to close all files
procedure TShapeReader.CloseallFiles;
begin
   {$i-}
   Closefile(ShpFile);
   {$i+}
   if Database_ok = true then
   begin
      if MainWnd.AdoTable.Active then
      begin
         MainWnd.ADOTable.First;
         MainWnd.ADOTable.Close;
         MainWnd.ADOTable.Active:=FALSE;
      end;
      if MainWnd.ADOConnection.Connected then
      begin
         MainWnd.ADOConnection.Connected:=FALSE;
         MainWnd.ADOConnection.Close;
      end;
      Deletefile(PCHAR(NewDbfFilename)); // remove the temporary dBase file
      MainWnd.AXImpExpDbc.CloseImpTables;
   end;
end;

// function is used to parse header of Shape-File
// -> return number of bytes that have been read (if 0 -> not a correct Shape)
function TShapeReader.ReadHeader:longint;
var FileCode,Filelength,Version,hvarint:longint;
    i:integer;
    hvardouble:double;
    ShapeType:longint;
begin
   ReadInteger('Big',FileCode);  // read File-Code (must be 9994)
   if (FileCode = 9994) then
   begin
      for i:=1 to 5 do ReadInteger('Big',hvarint); // read 5 dummy integers
      ReadInteger('Big',Filelength);               // read File-Length
      ReadInteger('Little',Version);               // read Version of File
      ReadInteger('Little',Shapetype);             // read Type of Shape
      for i:=1 to 4 do ReadDouble('Little',hvardouble); // read Bounding box data
      for i:=1 to 8 do ReadInteger('Big',hvarint); // read unused data
      Result:=100; // header is 100 bytes long
   end
   else
      Result:=0;
end;

// method is used to add object to layer
procedure TShapeReader.AddObjectToLayer(const aObject:TImpExpBaseObj);
begin
   ShpFileLayer.AddItem(aObject);
end;

procedure TShapeReader.SetObjectKeeper(const aKeeper:TImpExpObjectKeeper);
begin
   ObjectKeeper:=aKeeper;
   DataMgr.ObjectKeeper:=aKeeper;
end;

{
procedure TShapeReader.SetDkmManager(const aDkmMgr:TShpDkmManager);
begin
   DkmMgr:=aDkmMgr;
   DkmMgr.SetDataMgr(datamgr); // apply current data manager to Dkm-Manager
end;
}

procedure TShapeReader.ImportElements;
var aPolyline:TImpExpPoly;
    aPolygon :TImpExpCPoly;
    aPixel   :TImpExpPixel;
    aText    :TImpExpText;
    aObject  :TImpExpBaseObj;
    aLayer   :TImpExpLayer;
    newId    :integer;
    aAttrib  :p_ImpExpAttrib;
    LayerIdx, ObjIdx:integer;
    i        :integer;
    OriLayername:string;
begin
   for LayerIdx:=0 to DataMgr.Numitems-1 do
   begin
      aLayer:=DataMgr.Items[LayerIdx];
      if not aLayer.CreatedInGis then
      begin
         MainWnd.Create_Layer(aLayer);
         aLayer.CreatedInGis:=true;
      end;

      for ObjIdx:=0 to aLayer.NumItems-1 do
      begin
         newId:=-1;
         aObject:=aLayer.Items[ObjIdx];
         if aObject = nil then exit;
         case aObject.ObjectType of
            impexp_Pixel: begin // Import Pixel object
                             aPixel:=aObject as TImpExpPixel;
                             newId:=MainWnd.Create_Pixel(aPixel);
                          end;
            impexp_Poly : begin // Import Polyline object
                             aPolyline:=aObject as TImpExpPoly;
                             newId:=MainWnd.Create_Poly(aPolyline);
                          end;
            impexp_CPoly: begin // Import Polygon object
                             aPolygon:=aObject as TImpExpCPoly;
                             newId:=MainWnd.Create_CPoly(aPolygon);
                          end;
            impexp_Text : begin // Import Text object
                             aText:=aObject as TImpExpText;
                             newId:=MainWnd.Create_Text(aText);
                          end;
         end;

         // Import database information
         if (Database_ok) and (aObject.NumAttribs > 0) and (newId > 0) then
         begin
            if aObject.TagText <> '' then
            begin
               OriLayername:=aLayer.Name;
               aLayer.Name:=aObject.TagText;
               MainWnd.Create_Layer(aLayer);
            end;

            MainWnd.AXImpExpDbc.AddImpIdInfo(aLayer.Name, newId);
            for i:=0 to aObject.NumAttribs-1 do
            begin
               aAttrib:=aObject.Attribs[i];
               MainWnd.AXImpExpDbc.AddImpInfo(aLayer.Name, aAttrib^.Column, aAttrib^.Info);
            end;
            if aObject.TagText <> '' then
               aLayer.Name:=OriLayername;
         end;
      end;
      aLayer.ClearItems;
   end;
end;

// function to read element of Shape file
function TShapeReader.ReadElement:longint;
var BytesRead:longint;
    ShapeType:integer;
    Record_Number, Content_Length:longint;
begin
   ReadRecordHeader(Record_Number,Content_Length); Bytesread:=8; // read Record-header (allways 8 bytes)
   ReadInteger('Little',ShapeType); Bytesread:=Bytesread + 4;    // read Shapetype (4 bytes)
   Case Shapetype of
      0: Bytesread:=Bytesread+Read_Null;        // read Null-Shape
      1 :Bytesread:=Bytesread+Read_Point;       // read Point-Shape
      3 :Bytesread:=Bytesread+Read_Polyline;    // read Polyline-Shape
      5 :Bytesread:=Bytesread+Read_Polygon;     // read Polygon-Shape
      8 :Bytesread:=Bytesread+Read_MultiPoint;  // read Multipoint-Shape
      11:Bytesread:=Bytesread+Read_PointZ;      // read PointZ-Shape
      13:Bytesread:=Bytesread+Read_PolylineZ;   // read PolylineZ-Shape
      15:Bytesread:=Bytesread+Read_PolygonZ;    // read PolygonZ-Shape
      18:Bytesread:=Bytesread+Read_MultiPointZ; // read MultipointZ-Shape
      21:Bytesread:=Bytesread+Read_PointM;      // read PointM-Shape
      23:Bytesread:=Bytesread+Read_PolylineM;   // read PolylineM-Shape
      25:Bytesread:=Bytesread+Read_PolygonM;    // read PolygonM-Shape
      28:Bytesread:=Bytesread+Read_MultiPointM; // read MultiPointM-Shape
      31:Bytesread:=Bytesread+Read_MultiPatch;  // read MultiPatch-Shape
   end;
   Result:=Bytesread;
end;

// function is used to read MultiPatch object
function TShapeReader.Read_MultiPatch:longint;
var Bytesread, NumParts, NumPoints:longint;
    i,j:integer;
    dummydouble:double;
    Parts:array of longint;
    Types:array of longint;
    Pointsworked:integer;
    NumPolyCorners:integer;
    aPolygon, aPolygon2:TImpExpCPoly;
    X,Y:double;
    aCorner:p_ImpExpCorner;
    aAttribObj:TImpExpBaseObj;
begin
   Bytesread:=0;
   for i:=1 to 4 do begin ReadDouble('Big',dummydouble); Bytesread:=Bytesread + 8; end; // read bounding box information
   ReadInteger('Little',NumParts); Bytesread:=Bytesread + 4; // read numparts information
   SetLength(Parts, NumParts+1); SetLength(Types,NumParts+1);
   ReadInteger('Little',NumPoints); Bytesread:=Bytesread + 4; // read numpoints information

   Pointsworked:=0; aAttribObj:=nil;
   // read Parts information
   for i:=1 to NumParts do
   begin
      ReadInteger('Little',Parts[i]); Bytesread:=Bytesread + 4;
   end;
   // read Types information
   for i:=1 to NumParts do
   begin
      ReadInteger('Little',Types[i]); Bytesread:=Bytesread + 4;
   end;

   for i:=1 to NumParts do   // now the polyline parts will be read
   begin
      // calculate number of corners for polygon
      if i = NumParts then
         NumPolyCorners:=NumPoints - Pointsworked
      else
         NumPolyCorners:=Parts[i+1] - Parts[i];

      if NumPolyCorners = 0 then continue; // if no corners belong to polygon -> continue
      aPolygon:=nil;
      if (Types[i] = 0) then // we have to create a Triangle-Strip
      begin
         // -> we have to create a starting Polygon
         aPolygon:=ObjectKeeper.Request_CPoly;
         ShpFileLayer.AddItem(aPolygon);
      end
      else if (Types[i] = 1) then // we have to create a Triangle-Fan
      begin
         // -> we have to create a starting Polygon
         aPolygon:=ObjectKeeper.Request_CPoly;
         ShpFileLayer.AddItem(aPolygon);
      end
      else
      begin
         // other types ( 2 = outer ring, 3 = inner ring, 4 = first ring, 5 = ring )
         // will be handeled as normal polygons
         aPolygon:=ObjectKeeper.Request_CPoly;
         ShpFileLayer.AddItem(aPolygon);
      end;
      if (aAttribObj = nil) then
      begin
         aAttribObj:=TImpExpBaseObj.Create;
         ReadDataBaseInfo(aAttribObj);
      end;
      aPolygon.CopyAttribsFrom(aAttribObj);

      for j:=1 to NumPolyCorners do
      begin
         // read coordintates
         ReadDouble('Little',X); ReadDouble('Little',Y); Bytesread:=Bytesread + 16; Pointsworked:=Pointsworked + 1;
         if (Types[i] = 0) then // process corners for  Triangle-Strip
         begin
            if aPolygon.NumCorners >= 3 then
            begin
               aPolygon2:=ObjectKeeper.Request_CPoly;
               ShpFileLayer.AddItem(aPolygon2);
               aCorner:=aPolygon.Corners[1]; aPolygon2.AddCorner(aCorner^.X, aCorner^.Y);
               aCorner:=aPolygon.Corners[2]; aPolygon2.AddCorner(aCorner^.X, aCorner^.Y);
               aPolygon:=aPolygon2;
            end;
            aPolygon.AddCorner(X,Y);
         end
         else if (Types[i] = 1) then // process corners for Triangle-Fan
         begin
            if aPolygon.NumCorners >= 3 then
            begin
               aPolygon2:=ObjectKeeper.Request_CPoly;
               ShpFileLayer.AddItem(aPolygon2);
               aCorner:=aPolygon.Corners[0]; aPolygon2.AddCorner(aCorner^.X, aCorner^.Y);
               aCorner:=aPolygon.Corners[2]; aPolygon2.AddCorner(aCorner^.X, aCorner^.Y);
               aPolygon:=aPolygon2;
            end;
            aPolygon.AddCorner(X,Y);
         end
         else
            aPolygon.AddCorner(X,Y);
      end;
   end;

   // read Z-Min (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Z-Max (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Z-Information for Points (not used)
   for i:=1 to NumPoints do
   begin
      ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   end;

   // read Meashure-Min (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Meashure-Max (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Meashure-Information for Points (not used)
   for i:=1 to NumPoints do
   begin
      ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   end;

   SetLength(Parts, 0); SetLength(Types,0);
   if aAttribObj <> nil then aAttribObj.Destroy;

   // copy current read objects to datamgr
   ProcessReadObjects;
   result:=Bytesread;
end;

// function is used to read Null-Shape
function TShapeReader.Read_Null:longint;
begin
  if (Database_ok = true) then // to keep synchronisized with database
      MainWnd.ADOTable.Next;
  result:=0; // due to no bytes have been read
end;

// function is used to read Polyline shape
function TShapeReader.Read_Polyline:longint;
var NumParts,NumPoints,i,j:longint;
    Bytesread,NumPolyCorners,Pointsworked:longint;
    Parts:array of longint;
    X,Y,dummydouble:double;
    FirstPart:boolean;
    aPolyline:TImpExpPoly;
    aBaseObj:TImpExpBaseObj;
begin
   Bytesread:=0;
   for i:=1 to 4 do begin ReadDouble('Big',dummydouble); Bytesread:=Bytesread + 8; end; // read bounding box information
   ReadInteger('Little',NumParts); Bytesread:=Bytesread + 4; // read numparts information
   SetLength(Parts, NumParts+1);
   ReadInteger('Little',NumPoints); Bytesread:=Bytesread + 4; // read numpoints information

   Pointsworked:=0;
   for i:=1 to NumParts do  // read numparts
   begin
      ReadInteger('Little',Parts[i]); Bytesread:=Bytesread + 4;
   end;

   aPolyline:=nil; FirstPart:=TRUE;
   for i:=1 to NumParts do   // now the polyline parts will be read
   begin
      // calculate number of corners for polygon
      if i = NumParts then
         NumPolyCorners:=NumPoints - Pointsworked
      else
         NumPolyCorners:=Parts[i+1] - Parts[i];

      if NumPolyCorners = 0 then continue; // if no corners belong to polygon -> continue

      // create new Polyline
      aPolyline:=ObjectKeeper.Request_Poly;
      ShpFileLayer.AddItem(aPolyline);
      if (FirstPart = true) and (Database_ok = true) then // add database information to new polygon
      begin
         aBaseObj:=aPolyline;
         ReadDatabaseInfo(aBaseObj);
         FirstPart:=false;
      end;

      // reserve space for corners
      aPolyline.CornerSpace:=NumPolyCorners;

      for j:=1 to NumPolyCorners do
      begin
         { Einlesen der Punktkoorindaten X,Y };
         ReadDouble('Little',X);
         ReadDouble('Little',Y);
         Bytesread:=Bytesread + 16; Pointsworked:=Pointsworked + 1;
         aPolyline.AddCorner(X,Y);
      end;
   end;
   SetLength(Parts,0);
   ProcessReadObjects;
   Result:=Bytesread;
end;


// function to read polygon shape object
function TShapeReader.Read_Polygon:longint;
var NumParts,NumPoints,i,j:longint;
    Bytesread,NumCPolyCorners,Pointsworked:longint;
    Parts:array of longint;
    X,Y,dummydouble:double;

    DoSplitIsland:boolean;

    aBaseCPoly  :TImpExpCPoly;
    aIslandCPoly:TImpExpCPoly;
    aAttribObj  :TImpExpBaseObj;
begin
   Bytesread:=0;
   for i:=1 to 4 do begin ReadDouble('Big',dummydouble); Bytesread:=Bytesread + 8; end; // read bounding box information
   ReadInteger('Little',NumParts); Bytesread:=Bytesread + 4;  // read numparts information
   SetLength(Parts, NumParts+1); // create array for Parts information

   ReadInteger('Little',NumPoints); Bytesread:=Bytesread + 4; // read numpoints information

   DoSplitIsland:=false;
   // check if number of points are not more than 16384 if it�s an island
   if ((Numparts > 1) and (NumPoints > 16384)) then
   begin
      // display message that island-object has to be splitted into
      // its polygon elements
      MainWnd.StatusLb.Items.Add('Warning: Island object contains more that 16384 corners. It has to be splitted!');
      MainWnd.StatusLB.ItemIndex := MainWnd.StatusLB.Items.Count-1;
      MainWnd.StatusLB.Update;
      DoSplitIsland:=true;
   end;

   Pointsworked:=0;
   for i:=1 to NumParts do  // read numparts
   begin
      ReadInteger('Little',Parts[i]); Bytesread:=Bytesread + 4;
   end;

   // check parts
   if NumParts > 1 then
   begin
      for i:=1 to NumParts-1 do
      begin
         if (Parts[i] > Parts[i+1]) then
         begin
            // Invalid Parts (maybe currupt)
            // -> Set NumParts to 1 and create only one Polygon
            NumParts:=1;
            // show warning that polygon is maybe corrupt due to invalid parts information
            break;
         end;
      end;
   end;

   aIslandCPoly:=nil;aBaseCPoly:=nil;aAttribObj:=nil;
   for i:=1 to NumParts do     // now the polygon parts will be read
   begin
      // calculate number of corners for polygon
      if i = NumParts then
         NumCPolyCorners:=NumPoints - Pointsworked
      else
         NumCPolyCorners:=Parts[i+1] - Parts[i];
      if NumCPolyCorners <= 0 then continue; // if no corners belong to polygon -> continue
      if (aBaseCPoly = nil) or (DoSplitIsland) then
      begin
         // create base CPolygon
         aBaseCPoly:=ObjectKeeper.Request_CPoly;
         ShpFileLayer.AddItem(aBaseCPoly);
      end
      else
      begin
         // create island CPolygon
         aIslandCPoly:=ObjectKeeper.Request_CPoly;
         aBaseCPoly.AddIsland(aIslandCPoly);
      end;

      if (aIslandCPoly = nil) and (Database_ok = true) then // add database information to new polygon
      begin
         if aAttribObj = nil then
         begin
            aAttribObj:=TImpExpBaseObj.Create;
            ReadDatabaseInfo(aAttribObj);
         end;
         aBaseCPoly.CopyAttribsFrom(aAttribObj);
      end;

      // set reserved length for polygons
      if (aIslandCPoly = nil) then
         aBaseCPoly.CornerSpace:=NumCPolyCorners
      else
         aIslandCPoly.CornerSpace:=NumCPolyCorners;

      for j:=1 to NumCPolyCorners do
      begin
         { Einlesen der Punktkoorindaten X,Y };
         ReadDouble('Little',X); ReadDouble('Little',Y); Bytesread:=Bytesread + 16;
         Pointsworked:=Pointsworked + 1;
         if (aIslandCPoly = nil) then
            aBaseCPoly.AddCorner(X,Y)
         else
            aIslandCPoly.AddCorner(X,Y);
      end;
   end;
   SetLength(Parts, 0); // remove parts array
   if aAttribObj <> nil then aAttribObj.Destroy;
   ProcessReadObjects;
   Result:=Bytesread;
end;

// method to parse PointM-shape
function TShapeReader.Read_PointM:longint;
var Bytesread:longint;
    X,Y,dummydouble:double;
    aPixel:TImpExpPixel;
    aBaseObj:TImpExpBaseObj;
begin
   Bytesread:=0;
   // read X/Y Coordinates
   ReadDouble('Little',X); ReadDouble('Little',Y); Bytesread:=Bytesread + 16;
   // read Meashure (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   aPixel:=ObjectKeeper.Request_Pixel;
   aPixel.X:=X; aPixel.Y:=Y;
   ShpFileLayer.AddItem(aPixel);
   aBaseObj:=aPixel;
   ReadDatabaseInfo(aBaseObj);
   ProcessReadObjects;
   Result:=Bytesread;
end;

// function to parse PointZ-Shape
function TShapeReader.Read_PointZ:longint;
var Bytesread:longint;
    X,Y,dummydouble:double;
    aPixel:TImpExpPixel;
    aBase:TImpExpBaseObj;
begin
   Bytesread:=0;
   // read X/Y Coordinates
   ReadDouble('Little',X); ReadDouble('Little',Y); Bytesread:=Bytesread + 16;
   // read Z/Meashure (not used)
   ReadDouble('Little',dummydouble); ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 16;
   aPixel:=ObjectKeeper.Request_Pixel;
   aPixel.X:=X; aPixel.Y:=Y;
   ShpFileLayer.AddItem(aPixel);
   aBase:=aPixel;
   ReadDatabaseInfo(aBase);
   ProcessReadObjects;
   Result:=Bytesread;
end;

procedure TShapeReader.ReadDatabaseInfo(var aObject:TImpExpBaseObj);
var dbcount:integer;
    ColName,ColInfo:string;
begin
   if Database_ok = true then
   begin
      for dbcount := 0 to MainWnd.ADOTable.FieldCount - 1 do { Einf�gen der Spaltennamen in die Datenbankliste }
      begin
         ColName:=MainWnd.ADOTable.Fields[dbcount].Fieldname;
              if (MainWnd.ADOTable.Fields[dbcount].DataType = ftInteger)  then ColInfo:=inttostr(MainWnd.ADOTable.Fields[dbcount].AsInteger)
         else if (MainWnd.ADOTable.Fields[dbcount].DataType = ftSmallint) then ColInfo:=inttostr(MainWnd.ADOTable.Fields[dbcount].AsInteger)
         else if (MainWnd.ADOTable.Fields[dbcount].DataType = ftFloat)    then ColInfo:=floattostr(MainWnd.ADOTable.Fields[dbcount].AsFloat)
         else ColInfo:=MainWnd.ADOTable.Fields[dbcount].AsString;
         aObject.AddAttrib(ColName, ColInfo);
      end;
      MainWnd.ADOTable.Next;
   end;
end;

// method to parse point shape
function TShapeReader.Read_Point:longint;
var Bytesread:longint;
    X,Y:double;
    aPixel:TImpExpPixel;
    aObject:TImpExpBaseObj;
begin
   Bytesread:=0;
   // read X/Y Coordinates
   ReadDouble('Little',X); ReadDouble('Little',Y); Bytesread:=Bytesread + 16;
   aPixel:=ObjectKeeper.Request_Pixel;
   aPixel.X:=X; aPixel.Y:=Y;
   ShpFileLayer.AddItem(aPixel);
   aObject:=aPixel;
   ReadDataBaseInfo(aObject);
   ProcessReadObjects;
   Result:=Bytesread;
end;

// function is used to parse PolygonM-Shape
function TShapeReader.Read_PolygonM:longint;
var NumParts,NumPoints,i,j:longint;
    Bytesread,NumCPolyCorners,Pointsworked:longint;
    Parts:array of longint;
    X,Y,dummydouble:double;

    DoSplitIsland:boolean;

    aBaseCPoly  :TImpExpCPoly;
    aIslandCPoly:TImpExpCPoly;
    aAttribObj:TImpExpBaseObj;
begin
   Bytesread:=0; aAttribObj:=nil;
   for i:=1 to 4 do begin ReadDouble('Big',dummydouble); Bytesread:=Bytesread + 8; end; // read bounding box information
   ReadInteger('Little',NumParts); Bytesread:=Bytesread + 4;  // read numparts information
   SetLength(Parts, NumParts+1); // create array for Parts information

   ReadInteger('Little',NumPoints); Bytesread:=Bytesread + 4; // read numpoints information

   DoSplitIsland:=false;
   // check if number of points are not more than 16384 if it�s an island
   if ((Numparts > 1) and (NumPoints > 16384)) then
   begin
      // display message that island-object has to be splitted into
      // its polygon elements
      MainWnd.StatusLb.Items.Add('Warning: Island object contains more that 16384 corners. It has to be splitted!');
      MainWnd.StatusLB.ItemIndex := MainWnd.StatusLB.Items.Count-1;
      MainWnd.StatusLB.Update;
      DoSplitIsland:=true;
   end;

   Pointsworked:=0;
   for i:=1 to NumParts do  // read numparts
   begin
      ReadInteger('Little',Parts[i]); Bytesread:=Bytesread + 4;
   end;

   aIslandCPoly:=nil;aBaseCPoly:=nil;
   for i:=1 to NumParts do     // now the polygon parts will be read
   begin
      // calculate number of corners for polygon
      if i = NumParts then
         NumCPolyCorners:=NumPoints - Pointsworked
      else
         NumCPolyCorners:=Parts[i+1] - Parts[i];
      if NumCPolyCorners = 0 then continue; // if no corners belong to polygon -> continue
      if (aBaseCPoly = nil) or (DoSplitIsland) then
      begin
         // create base CPolygon
         aBaseCPoly:=ObjectKeeper.Request_CPoly;
         ShpFileLayer.AddItem(aBaseCPoly);
      end
      else
      begin
         // create island CPolygon
         aIslandCPoly:=ObjectKeeper.Request_CPoly;
         aBaseCPoly.AddIsland(aIslandCPoly);
      end;

      if (aIslandCPoly = nil) then // add database information to new polygon
      begin
        if (aAttribObj = nil) then
        begin
           aAttribObj:=TImpExpBaseObj.Create;
           ReadDatabaseInfo(aAttribObj);
        end;
        aBaseCPoly.CopyAttribsFrom(aAttribObj);
      end;

      // set reserved length for polygons
      if (aIslandCPoly = nil) then
         aBaseCPoly.CornerSpace:=NumCPolyCorners
      else
         aIslandCPoly.CornerSpace:=NumCPolyCorners;

      for j:=1 to NumCPolyCorners do
      begin
         { Einlesen der Punktkoorindaten X,Y };
         ReadDouble('Little',X); ReadDouble('Little',Y); Bytesread:=Bytesread + 16;
         Pointsworked:=Pointsworked + 1;
         if (aIslandCPoly = nil) then
            aBaseCPoly.AddCorner(X,Y)
         else
            aIslandCPoly.AddCorner(X,Y);
      end;
   end;

   // read Meashure-Min (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Meashure-Max (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Meashure-Information for Points (not used)
   for i:=1 to NumPoints do
   begin
      ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   end;
   SetLength(Parts, 0); // remove parts array
   if aAttribObj <> nil then aAttribObj.Destroy;
   ProcessReadObjects;
   Result:=Bytesread;
end;

// method is used to parse PolygonZ-Shape
function TShapeReader.Read_PolygonZ:longint;
var NumParts,NumPoints,i,j:longint;
    Bytesread,NumCPolyCorners,Pointsworked:longint;
    Parts:array of longint;
    X,Y,dummydouble:double;

    DoSplitIsland:boolean;

    aBaseCPoly  :TImpExpCPoly;
    aIslandCPoly:TImpExpCPoly;

    aAttribObj:TImpExpBaseObj;
begin
   Bytesread:=0;
   for i:=1 to 4 do begin ReadDouble('Big',dummydouble); Bytesread:=Bytesread + 8; end; // read bounding box information
   ReadInteger('Little',NumParts); Bytesread:=Bytesread + 4;  // read numparts information
   SetLength(Parts, NumParts+1); // create array for Parts information

   ReadInteger('Little',NumPoints); Bytesread:=Bytesread + 4; // read numpoints information

   DoSplitIsland:=false;
   // check if number of points are not more than 16384 if it�s an island
   if ((Numparts > 1) and (NumPoints > 16384)) then
   begin
      // display message that island-object has to be splitted into
      // its polygon elements
      MainWnd.StatusLb.Items.Add('Warning: Island object contains more that 16384 corners. It has to be splitted!');
      MainWnd.StatusLB.ItemIndex := MainWnd.StatusLB.Items.Count-1;
      MainWnd.StatusLB.Update;
      DoSplitIsland:=true;
   end;

   Pointsworked:=0;
   for i:=1 to NumParts do  // read numparts
   begin
      ReadInteger('Little',Parts[i]); Bytesread:=Bytesread + 4;
   end;

   aIslandCPoly:=nil;aBaseCPoly:=nil; aAttribObj:=nil;
   for i:=1 to NumParts do     // now the polygon parts will be read
   begin
      // calculate number of corners for polygon
      if i = NumParts then
         NumCPolyCorners:=NumPoints - Pointsworked
      else
         NumCPolyCorners:=Parts[i+1] - Parts[i];
      if NumCPolyCorners = 0 then continue; // if no corners belong to polygon -> continue
      if (aBaseCPoly = nil) or (DoSplitIsland) then
      begin
         // create base CPolygon
         aBaseCPoly:=ObjectKeeper.Request_CPoly;
         ShpFileLayer.AddItem(aBaseCPoly);
      end
      else
      begin
         // create island CPolygon
         aIslandCPoly:=ObjectKeeper.Request_CPoly;
         aBaseCPoly.AddIsland(aIslandCPoly);
      end;

      if (aIslandCPoly = nil) and (Database_ok = true) then // add database information to new polygon
      begin
        if (aAttribObj = nil) then
        begin
           aAttribObj:=TImpExpBaseObj.Create;
           ReadDatabaseInfo(aAttribObj);
        end;
        aBaseCPoly.CopyAttribsFrom(aAttribObj);
      end;

      // set reserved length for polygons
      if (aIslandCPoly = nil) then
         aBaseCPoly.CornerSpace:=NumCPolyCorners
      else
         aIslandCPoly.CornerSpace:=NumCPolyCorners;

      for j:=1 to NumCPolyCorners do
      begin
         { Einlesen der Punktkoorindaten X,Y };
         ReadDouble('Little',X); ReadDouble('Little',Y); Bytesread:=Bytesread + 16;
         Pointsworked:=Pointsworked + 1;
         if (aIslandCPoly = nil) then
            aBaseCPoly.AddCorner(X,Y)
         else
            aIslandCPoly.AddCorner(X,Y);
      end;
   end;

   // read Z-Min (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Z-Max (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Z-Information for Points (not used)
   for i:=1 to NumPoints do
   begin
      ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   end;
   SetLength(Parts, 0); // remove parts array
   if aAttribObj <> nil then aAttribObj.Destroy;
   ProcessReadObjects;
   Result:=Bytesread;
end;

// function is used to read PolylineM-Shape
function TShapeReader.Read_PolylineM:longint;
var NumParts,NumPoints,i,j:longint;
    Bytesread,NumPolyCorners,Pointsworked:longint;
    Parts:array of longint;
    X,Y,dummydouble:double;
    FirstPart:boolean;
    aPolyline:TImpExpPoly;
    aBaseObj:TImpExpBaseObj;
begin
   Bytesread:=0;
   for i:=1 to 4 do begin ReadDouble('Big',dummydouble); Bytesread:=Bytesread + 8; end; // read bounding box information
   ReadInteger('Little',NumParts); Bytesread:=Bytesread + 4; // read numparts information
   SetLength(Parts, NumParts+1);
   ReadInteger('Little',NumPoints); Bytesread:=Bytesread + 4; // read numpoints information

   Pointsworked:=0;
   for i:=1 to NumParts do  // read numparts
   begin
      ReadInteger('Little',Parts[i]); Bytesread:=Bytesread + 4;
   end;

   aPolyline:=nil; FirstPart:=TRUE;
   for i:=1 to NumParts do   // now the polygon parts will be read
   begin
      // calculate number of corners for polygon
      if i = NumParts then
         NumPolyCorners:=NumPoints - Pointsworked
      else
         NumPolyCorners:=Parts[i+1] - Parts[i];

      if NumPolyCorners = 0 then continue; // if no corners belong to polygon -> continue

      // create new Polyline
      aPolyline:=ObjectKeeper.Request_Poly;
      ShpFileLayer.AddItem(aPolyline);
      if (FirstPart = true) then // add database information to new polyline
      begin
         aBaseObj:=aPolyLine;
         ReadDatabaseInfo(aBaseObj);
         FirstPart:=false;
      end;

      // reserve space for corners
      aPolyline.CornerSpace:=NumPolyCorners;

      for j:=1 to NumPolyCorners do
      begin
         // read coordinates
         ReadDouble('Little',X); ReadDouble('Little',Y); Bytesread:=Bytesread + 16; Pointsworked:=Pointsworked + 1;
         aPolyline.AddCorner(X,Y);
      end;
   end;

   // read Meashure-Min (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Meashure-Max (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Meashure-Information for Points (not used)
   for i:=1 to NumPoints do
   begin
      ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   end;

   SetLength(Parts,0);
   ProcessReadObjects;
   Result:=Bytesread;
end;

// function to parse PolylineZ-shape
function TShapeReader.Read_PolylineZ:longint;
var NumParts,NumPoints,i,j:longint;
    Bytesread,NumPolyCorners,Pointsworked:longint;
    Parts:array of longint;
    X,Y,dummydouble:double;
    FirstPart:boolean;
    aPolyline:TImpExpPoly;
    aBaseObj:TImpExpBaseObj;
begin
   Bytesread:=0;
   for i:=1 to 4 do begin ReadDouble('Big',dummydouble); Bytesread:=Bytesread + 8; end; // read bounding box information
   ReadInteger('Little',NumParts); Bytesread:=Bytesread + 4; // read numparts information
   SetLength(Parts, NumParts+1);
   ReadInteger('Little',NumPoints); Bytesread:=Bytesread + 4; // read numpoints information

   Pointsworked:=0;
   for i:=1 to NumParts do  // read numparts
   begin
      ReadInteger('Little',Parts[i]); Bytesread:=Bytesread + 4;
   end;

   aPolyline:=nil; FirstPart:=TRUE;
   for i:=1 to NumParts do   // now the polygon parts will be read
   begin
      // calculate number of corners for polygon
      if i = NumParts then
         NumPolyCorners:=NumPoints - Pointsworked
      else
         NumPolyCorners:=Parts[i+1] - Parts[i];

      if NumPolyCorners = 0 then continue; // if no corners belong to polyline -> continue

      // create new Polyline
      aPolyline:=ObjectKeeper.Request_Poly;
      ShpFileLayer.AddItem(aPolyline);
      if (FirstPart = true) then // add database information to new polygon
      begin
         aBaseObj:=aPolyline;
         ReadDatabaseInfo(aBaseObj);
         FirstPart:=false;
      end;

      // reserve space for corners
      aPolyline.CornerSpace:=NumPolyCorners;

      for j:=1 to NumPolyCorners do
      begin
         // read coordinates
         ReadDouble('Little',X); ReadDouble('Little',Y); Bytesread:=Bytesread + 16; Pointsworked:=Pointsworked + 1;
         aPolyline.AddCorner(X,Y);
      end;
   end;

   // read Z-Min (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Z-Max (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Z-Information for Points (not used)
   for i:=1 to NumPoints do
   begin
      ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   end;

   // read Meashure-Min (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Meashure-Max (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Meashure-Information for Points (not used)
   for i:=1 to NumPoints do
   begin
      ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   end;

   SetLength(Parts,0);
   ProcessReadObjects;
   Result:=Bytesread;
end;

// function is used to parse MultiPointM object
function TShapeReader.Read_MultiPointM:longint;
var Bytesread:longint;
    i:integer;
    dummydouble:double;
    NumPoints:integer;
    X,Y:double;
    aPixel:TImpExpPixel;
    aAttribObj:TImpExpBaseObj;
begin
   Bytesread:=0;
   for i:=1 to 4 do begin ReadDouble('Big',dummydouble); Bytesread:=Bytesread + 8; end; // read bounding box information
   ReadInteger('Little',NumPoints); Bytesread:=Bytesread + 4;  // read numpoints information
   aAttribObj:=nil;
   // now read the points
   for i:=1 to NumPoints do
   begin
      // read X/Y Coordinates
      ReadDouble('Little',X); ReadDouble('Little',Y); Bytesread:=Bytesread + 16;
      aPixel:=ObjectKeeper.Request_Pixel;
      aPixel.X:=X; aPixel.Y:=Y;
      ShpFileLayer.AddItem(aPixel);
      if (aAttribObj = nil) then
      begin
         aAttribObj:=TImpExpBaseObj.Create;
         ReadDatabaseInfo(aAttribObj);
      end;
      aPixel.CopyAttribsFrom(aAttribObj);
   end;

   // read Meashure-Min (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Meashure-Max (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Meashure-Information for Points (not used)
   for i:=1 to NumPoints do
   begin
      ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   end;
   if (aAttribObj <> nil) then aAttribObj.Destroy;
   ProcessReadObjects;
   result:=BytesRead;
end;



// method is used to parse MultiPointZ-Shape
function TShapeReader.Read_MultiPointZ:longint;
var Bytesread:longint;
    i:integer;
    dummydouble:double;
    NumPoints:integer;
    X,Y:double;
    aPixel:TImpExpPixel;
    aAttribObj:TImpExpBaseObj;
begin
   Bytesread:=0;
   for i:=1 to 4 do begin ReadDouble('Big',dummydouble); Bytesread:=Bytesread + 8; end; // read bounding box information
   ReadInteger('Little',NumPoints); Bytesread:=Bytesread + 4;  // read numpoints information
   aAttribObj:=nil;
   // now read the points
   for i:=1 to NumPoints do
   begin
      // read X/Y Coordinates
      ReadDouble('Little',X); ReadDouble('Little',Y); Bytesread:=Bytesread + 16;
      aPixel:=ObjectKeeper.Request_Pixel;
      aPixel.X:=X; aPixel.Y:=Y;
      ShpFileLayer.AddItem(aPixel);
      if (aAttribObj = nil) then
      begin
         aAttribObj:=TImpExpBaseObj.Create;
         ReadDatabaseInfo(aAttribObj);
      end;
      aPixel.CopyAttribsFrom(aAttribObj);
   end;

   // read Z-Min (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Z-Max (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Z-Information for Points (not used)
   for i:=1 to NumPoints do
   begin
      ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   end;

   // read Meashure-Min (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Meashure-Max (not used)
   ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   // read Meashure-Information for Points (not used)
   for i:=1 to NumPoints do
   begin
      ReadDouble('Little',dummydouble); Bytesread:=Bytesread + 8;
   end;
   if (aAttribObj <> nil) then aAttribObj.Destroy;
   ProcessReadObjects;
   result:=BytesRead;
end;

procedure TShapeReader.ProcessReadObjects;
var i:integer;
    aObj:TImpExpBaseObj;
    ImpLayer:TImpExpLayer;
begin
   //if DkmMgr = nil then
   //begin
      for i:=0 to ShpFileLayer.NumItems-1 do
      begin
         aObj:= ShpFileLayer.Items[i];
         ImpLayer:=Datamgr.RequestLayer(ShpFileLayer.Name);
         ImpLayer.CopyColumnDefs(ShpFileLayer);
         ImpLayer.AddItem(aObj);
         ShpFileLayer.Items[i]:=nil;
      end;
   //end
   //else
   //   DkmMgr.ProcessObjects(ShpFileLayer);

   // clear items from shape layer
   ShpFileLayer.ClearItems;
   ImportElements;
end;

// method is used to read multipoint shape
function TShapeReader.Read_MultiPoint:longint;
var Bytesread:longint;
    i:integer;
    dummydouble:double;
    NumPoints:integer;
    X,Y:double;
    aPixel:TImpExpPixel;
    aAttribObj:TImpExpBaseObj;
begin
   Bytesread:=0;
   for i:=1 to 4 do begin ReadDouble('Big',dummydouble); Bytesread:=Bytesread + 8; end; // read bounding box information
   ReadInteger('Little',NumPoints); Bytesread:=Bytesread + 4;  // read numpoints information
   aAttribObj:=nil;
   // now read the points
   for i:=1 to NumPoints do
   begin
      // read X/Y Coordinates
      ReadDouble('Little',X); ReadDouble('Little',Y); Bytesread:=Bytesread + 16;
      aPixel:=ObjectKeeper.Request_Pixel;
      aPixel.X:=X; aPixel.Y:=Y;
      ShpFileLayer.AddItem(aPixel);
      if (aAttribObj = nil) then
      begin
         aAttribObj:=TImpExpBaseObj.Create;
         ReadDatabaseInfo(aAttribObj);
      end;
      aPixel.CopyAttribsFrom(aAttribObj);
   end;
   if (aAttribObj <> nil) then aAttribObj.Destroy;
   ProcessReadObjects;
   result:=BytesRead;
end;

// method to read Record-Header
function TShapeReader.ReadRecordHeader(var RecordNumber:longint; var RecordLength:longint):boolean;
var ret:boolean;
begin
   ret:=ReadInteger('Big',RecordNumber); if (not ret) then begin result:=FALSE; exit; end;
   ret:=ReadInteger('Big',RecordLength); if (not ret) then begin result:=FALSE; exit; end;
   result:=TRUE;
end;

// method to read integer value from file
// IntType -> Big or Little
function TShapeReader.ReadInteger(IntType:string;var IntValue:longint):boolean;
var Buffer: array [0..3] of byte;
    Bytesread :longint;
    retVal    :boolean;
begin
   retVal:=TRUE;
   {$I-}
   if (Eof(ShpFile)) then
   begin
      retVal:=FALSE;
      IntValue:=0;
   end
   else
   begin
      Blockread(ShpFile,Buffer,4,Bytesread);
      ValInteger(Buffer,IntType,IntValue);
   end;
   {$I+}
   result:=retVal;
end;


{*******************************************************************************
* PROZEDUR : ReadDouble                                                        *
********************************************************************************
* Prozedur dient zum Einlesen eines Doubledatensatzes aus dem Shapefile.       *
*                                                                              *
* InPARAMETER:  Typ -> Typ, wie die Auswertung erfolgt. (Big, oder Little)     *
*                                                                              *
* OutPARAMETER: Erg -> Doublewert der aus Datei eingelesen wurde.              *
*                                                                              *
********************************************************************************}
procedure TShapeReader.ReadDouble(Typ:string;var Erg:double);
var Buffer: array [0..7] of byte;
    Bytesread:longint;
begin
   {$I-}
   if (Eof(ShpFile)) then
   begin
      Erg:=0;
   end
   else
   begin
      Blockread(ShpFile,Buffer,8,Bytesread);
      ValDouble(Buffer,Typ,Erg);
   end;
   {$I+}
end;

{*******************************************************************************
* PROZEDUR : ValInteger                                                        *
********************************************************************************
* Prozedur dient zum auswerten einer Zeichenkette, die einen Integerwert       *
* beinhaltet.                                                                  *
*                                                                              *
* InPARAMETER:  Feld -> 4-Byte lange Zeichenkette, die Integerwert beinhaltet  *
*               Typ  -> Gibt an, wie die Zeichenkette ausgewertet werden soll  *
*                                                                              *
* OutPARAMETER: Erg  -> Integerwert der Zeichenkette                           *
*                                                                              *
********************************************************************************}
procedure TShapeReader.ValInteger(Feld :array of byte;Typ:string;var Erg:longint);
var HFeld:array [0..3] of byte;
    Zeiger:p_integer;
    i:integer;
begin
   if Typ = 'Little' then
   begin
      Zeiger:=@Feld;
      Erg:=Zeiger^.Zahl;
   end;
   if Typ = 'Big' then
   begin
      for i:=0 to 3 do hFeld[i]:=Feld[3-i];
      Zeiger:=@hFeld;
      Erg:=zeiger^.Zahl;
   end;
end;


{*******************************************************************************
* PROZEDUR : ValDouble                                                         *
********************************************************************************
* Prozedur dient zum Auswerten einer Zeichenkette, die einen Doublewert        *
* beinhaltet.                                                                  *
*                                                                              *
* InPARAMETER:  Feld -> 8-Byte lange Zeichenkette, die Doublewert beinhaltet   *
*               Typ  -> Gibt an, wie Zeichenkette ausgewertet wird.            *
*                                                                              *
* OutPARAMETER: Erg  -> Doublewert der ausgewerteten Zeichenkette.             *
*                                                                              *
********************************************************************************}
procedure TShapeReader.ValDouble(Feld :array of byte;Typ:string;var Erg:double);
var hvar1,hvar2,hvar3,hvar4,hvar5,hvar6,hvar7,hvar8:double;
    hzeiger :p_double;
begin
   if Typ = 'Big' then
   begin
      hvar1:=Feld[0]; hvar1:=hvar1 * (power(256,7));
      hvar2:=Feld[1]; hvar2:=hvar2 * (power(256,6));
      hvar3:=Feld[2]; hvar3:=hvar3 * (power(256,5));
      hvar4:=Feld[3]; hvar4:=hvar4 * (power(256,4));
      hvar5:=Feld[4]; hvar5:=hvar5 * (power(256,3));
      hvar6:=Feld[5]; hvar6:=hvar6 * (power(256,2));
      hvar7:=Feld[6]; hvar7:=hvar7 * 256;
      hvar8:=Feld[7]; hvar8:=hvar8 * 1;
      Erg:=hvar1 + hvar2 + hvar3 + hvar4 + hvar5 + hvar6 + hvar7 + hvar8;
   end;
   if Typ = 'Little' then
   begin
      { Umkasten des Inhalts des Feldes }
      hzeiger:=@Feld;
      Erg:=hzeiger^.Zahl;
   end;
end;

end.
