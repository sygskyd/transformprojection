unit DkmManager;

interface

uses SysUtils, ImpExpObjects;

type
TDkmManager = class
   private
      datamgr:TImpExpDataMgr;
   public
      constructor Create(dm:TImpExpDataMgr);
      destructor Destroy;
      procedure ProcessObject(var aObject:TImpExpBaseObj);
end;


implementation

constructor TDkmManager.Create(dm:TImpExpDataMgr);
begin
   datamgr:=dm;
end;

destructor TDkmManager.Destroy;
begin
end;

procedure TDkmManager.ProcessObject(var aObject:TImpExpBaseObj);
begin

end;



end.
