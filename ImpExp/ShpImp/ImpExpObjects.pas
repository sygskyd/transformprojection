unit ImpExpObjects;

interface
uses SysUtils;

const
      // Object types
      impexp_Pixel     = 1;
      impexp_Poly      = 2;
      impexp_CPoly     = 4;
      impexp_Text      = 7;
      impexp_Symbol    = 8;
      impexp_Spline    = 9;
      impexp_Image     = 10;
      impexp_MesLine   = 11;
      impexp_Circle    = 12;
      impexp_Arc       = 13;
      impexp_Layer     = 18;
      impexp_DataMgr   = 1000;
      impexp_ObjectKeeper = 1001;

      // Text Alignments
      Align_Top_Left      = 0;
      Align_Center_Left   = 1;
      Align_Bottom_Left   = 2;
      Align_Bottom_Center = 3;
      Align_Bottom_Right  = 4;
      Align_Center_Right  = 5;
      Align_Top_Right     = 6;
      Align_Top_Center    = 7;
      Align_Center        = 8;

type

// record to store attributes
p_ImpExpAttrib = ^ImpExpAttrib;
ImpExpAttrib = record
   Column:string;
   Info  :string;
end;

// record to store database column definition
p_ImpExpDbColumn = ^ImpExpDbColumn;
ImpExpDbColumn = record
   ColName   :string;
   ColType   :integer;
   ColLength:integer;
end;

p_ImpExpCorner=^ImpExpCorner;
ImpExpCorner = record
   X:double;
   Y:double;
end;

type

TImpExpObjStyle = class
   private
      FLineType:integer;
      FLineColor:integer;
      FLineWidth:integer;
      FFillType:integer;
      FFillColor:integer;

      // methods to support properties
      procedure SetLineType(const value:integer);
      procedure SetLineColor(const value:integer);
      procedure SetLineWidth(const value:integer);
      procedure SetFillType(const value:integer);
      procedure SetFillColor(const value:integer);
      function GetLineType:integer;
      function GetLineColor:integer;
      function GetLineWidth:integer;
      function GetFillType:integer;
      function GetFillColor:integer;
   public
      constructor Create;
      destructor Destroy;

      property LineType:integer read GetLineType write SetLineType;
      property LineColor:integer read GetLineColor write SetLineColor;
      property LineWidth:integer read GetLineWidth write SetLineWidth;
      property FillType:integer read GetFillType write SetFillType;
      property FillColor:integer read GetFillColor write SetFillColor;

      function HasLineType:boolean;
      function HasFillType:boolean;

      procedure Reset;
end;

// root base class for all objects
TImpExpRoot = class
   private
      function GetObjectType:integer;
      function GetParent:TImpExpRoot;
   protected
      FObjectType:integer;  // object type
      FParent:TImpExpRoot;  // parent of object
      procedure   Reset; virtual;
   public
      constructor Create;
      destructor Destroy; virtual;
      property ObjectType:integer read GetObjectType;
      property Parent:TImpExpRoot read GetParent;
end;

// base class for import/export objects
TImpExpBaseObj = class (TImpExpRoot)
   private
      // private definitions
      FAttribs    :array of p_ImpExpAttrib; // reference to attributes
      FNumAttribs :integer;                 // Number of attributes
      FObjectStyle:TImpExpObjStyle;         // Object Style
      FSpaceAttribs:integer;
      FTagText    :string;

      // methods to support properties
      function  GetNumAttribs:integer;
      function  GetAttrib(Index:integer):p_ImpExpAttrib;
      function  GetObjectStyle:TImpExpObjStyle;
      procedure SetTagText(const o:string);
      function  GetTagText:string;
   protected
      procedure Reset;
   public
      // public definitions
      constructor Create;
      destructor  Destroy; virtual;

      property    NumAttribs:integer read GetNumAttribs;
      property    Attribs[Index: Integer]:p_ImpExpAttrib read GetAttrib;
      property    ObjectStyle:TImpExpObjStyle read GetObjectStyle;
      property    TagText:string read GetTagText write SetTagText;

      procedure   AddAttrib(const aColumn:string; const aInfo:string);
      function    GetAttribInfoByColumn(const aColumn:string):string;
      procedure   CopyAttribsFrom(other:TImpExpBaseObj);
end;

// class for pixel
TImpExpPixel = class (TImpExpBaseObj)
   private
      // private definitions
      FX,FY:double;
      procedure SetX(const value:double);
      function  GetX:double;
      procedure SetY(const value:double);
      function  GetY:double;
   protected
      procedure   Reset;
   public
      // public definitions
      constructor Create;
      destructor Destroy;
      property X:double read GetX write SetX;
      property Y:double read GetY write SetY;
end;

// class for polyline
TImpExpPoly = class (TImpExpBaseObj)
   private
      // private definitions
      FCorners    :array of p_ImpExpCorner;
      FNumCorners:integer;
      FCornerSpace:integer;
      function GetCorners(Index:integer):p_ImpExpCorner;
      function GetNumCorners:integer;
      procedure SetCornerSpace(const value:integer);
   protected
      procedure Reset;
   public
      // public definitions
      constructor Create;
      destructor Destroy;
      property Corners[Index: Integer]:p_ImpExpCorner read GetCorners;
      property NumCorners:integer read GetNumCorners;
      procedure AddCorner(const X:double; const Y:double);
      property  CornerSpace:integer write SetCornerSpace;
end;

// class for polygon
TImpExpCPoly = class (TImpExpBaseObj)
   private
      // private definitions
      FCorners    :array of p_ImpExpCorner;
      FNumCorners:integer;
      FCornerSpace:integer;
      FIslands    :array of TImpExpBaseObj; // collection of Islands that belong to polygon
      FNumIslands :integer;
      FSpaceIslands:integer;
      function GetCorners(Index:integer):p_ImpExpCorner;
      function GetNumCorners:integer;
      function  GetIslands(Index:integer):TImpExpBaseObj;
      function  GetNumIslands:integer;
      procedure SetCornerSpace(const value:integer);
   protected
      procedure Reset;
   public
      // public definitions
      constructor Create;
      destructor Destroy;
      property Corners[Index: Integer]:p_ImpExpCorner read GetCorners;
      property NumCorners:integer read GetNumCorners;
      procedure AddCorner(const X:double; const Y:double);
      property  Islands[Index: Integer]:TImpExpBaseObj read GetIslands;
      property NumIslands:integer read GetNumIslands;
      procedure AddIsland(const aObject:TImpExpBaseObj);
      property  CornerSpace:integer write SetCornerSpace;
end;

// class for texts
TImpExpText = class (TImpExpBaseObj)
   private
      // private definitions
      FAngle:double;
      FFont:string;
      FText:string;
      FX,FY:double;
      FHeigth:double;
      FAlign:integer;

      function GetX:double;
      procedure SetX(const value:double);
      function GetY:double;
      procedure SetY(const value:double);
      function GetAngle:double;
      procedure SetAngle(const value:double);
      function GetFont:string;
      procedure SetFont(const value:string);
      function GetText:string;
      procedure SetText(const value:string);
      procedure SetHeight(const value:double);
      function GetHeigth:double;
      procedure SetAlign(const value:integer);
      function GetAlign:integer;
   public
      // public definitions
      constructor Create;
      destructor Destroy;
      property X:double read GetX write SetX;
      property Y:double read GetY write SetY;
      property Height:double read GetHeigth write SetHeight;
      property Angle:double read GetAngle write SetAngle;
      property Font:string read GetFont write SetFont;
      property Text:string read GetText write SetText;
      property Align:integer read GetAlign write SetAlign;
end;

// class for symbol
TImpExpSymbol = class (TImpExpBaseObj)
   private
      // private definitions
      FAngle:double;
      FName:string;
      FX,FY:double;
      FSize:double;

      function GetX:double;
      procedure SetX(const value:double);
      function GetY:double;
      procedure SetY(const value:double);
      function GetAngle:double;
      procedure SetAngle(const value:double);
      function GetName:string;
      procedure SetName(const value:string);
      procedure SetSize(const value:double);
      function GetSize:double;
   public
      // public definitions
      constructor Create;
      destructor Destroy;
      property X:double read GetX write SetX;
      property Y:double read GetY write SetY;
      property Angle:double read GetAngle write SetAngle;
      property Name:string read GetName write SetName;
      property Size:double read GetSize write SetSize;
end;

// class for layer
TImpExpLayer = class (TImpExpRoot)
   private
      ColDefs    :array of p_ImpExpDbColumn; // database column definitions
      NumColDefs:integer;

      FName      :string; // name of layer
      FCreatedInGis:boolean;

      Objects    :array of TImpExpBaseObj; // collection of objects
      NumObjects :integer;
      SpaceObjects:integer;

      Num_Pixel :integer;
      Num_Poly  :integer;
      Num_CPoly :integer;
      Num_Text  :integer;
      Num_Symbol:integer;

      procedure SetName(const aName:string);
      function  GetName:string;
      function  GetItems(Index:integer):TImpExpBaseObj;
      procedure SetItems(Index:integer; const aObj:TImpExpBaseObj);
      function  GetNumItems:integer;
      function  GetNumColumnDefs:integer;
      function  GetColumnDef(Index:integer):p_ImpExpDbColumn;
      function  GetCreatedInGis:boolean;
      procedure SetCreatedInGis(const value:boolean);
   public
      // public definitions
      constructor Create;
      destructor  Destroy;

      property  Name:string read GetName write SetName;
      property  Items[Index: Integer]:TImpExpBaseObj read GetItems write SetItems;
      property  NumItems:integer read GetNumItems;
      property  NumPixels:integer  read Num_Pixel;
      property  NumCPolys:integer  read Num_CPoly;
      property  NumPolys:integer   read Num_Poly;
      property  NumTexts:integer   read Num_Text;
      property  NumSymbols:integer read Num_Symbol;
      property  NumColumnDefs:integer read GetNumColumnDefs;
      property  CreatedInGis:boolean read GetCreatedInGis write SetCreatedInGis;
      property  ColumnDefs[Index: Integer]:p_ImpExpDbColumn read GetColumnDef;
      function  ColExists(const aColname:string):boolean;
      procedure  AddColumnDef(const aColname:string; const aColType:integer; const aColLength:integer);
      procedure  AddItem(const aObject:TImpExpBaseObj);
      procedure ClearItems;
      procedure ClearColumnDefs;
      procedure CopyColumnDefs(var o:TImpExpLayer);
end;

// datamanager of project
TImpExpDataMgr = class (TImpExpRoot)
   private
      Layers:array of TImpExpLayer; // collection of layers
      NumLayers :integer;
      SearchIdx :integer;
      FObjectKeeper:TImpExpRoot;

      function  GetItems(Index:integer):TImpExpLayer;
      function  GetNumItems:integer;

      procedure SetObjectKeeper(const o:TImpExpRoot);
      function  GetObjectKeeper: TImpExpRoot;
   public
      constructor Create;
      destructor Destroy;
      property Items[Index: Integer]:TImpExpLayer read GetItems;
      property NumItems:integer read GetNumItems;
      property ObjectKeeper:TImpExpRoot read GetObjectKeeper write SetObjectKeeper;
      function GetLayer(const aLayername:string):TImpExpLayer;
      function RequestLayer(const aLayername:string):TImpExpLayer;
end;

TImpExpObjectKeeper = class (TImpExpRoot)
   private
      CPoly_Num   :integer;
      CPoly_Space :integer;
      CPoly_Buffer:array of TImpExpCPoly;

      Poly_Num   :integer;
      Poly_Space :integer;
      Poly_Buffer:array of TImpExpPoly;

      Pixel_Num   :integer;
      Pixel_Space :integer;
      Pixel_Buffer:array of TImpExpPixel;

      Text_Num    :integer;
      Text_Space  :integer;
      Text_Buffer :array of TImpExpText;
   public
      constructor Create;
      destructor Destroy;

      function Request_CPoly:TImpExpCPoly;
      function Request_Poly:TImpExpPoly;
      function Request_Pixel:TImpExpPixel;
      function Request_Text:TImpExpText;

      procedure ReleaseObject(var obj:TImpExpBaseObj);
end;


implementation

// ------------------------ IMPEXPOBJSTYLE -------------------------------------
constructor TImpExpObjStyle.Create;
begin
   Reset;
end;

destructor TImpExpObjStyle.Destroy;
begin
end;

procedure TImpExpObjStyle.SetLineType(const value:integer);
begin
   FLineType:=value;
end;

procedure TImpExpObjStyle.SetLineColor(const value:integer);
begin
   FLineColor:=value;
end;

procedure TImpExpObjStyle.SetLineWidth(const value:integer);
begin
   FLineWidth:=value;
end;

procedure TImpExpObjStyle.SetFillType(const value:integer);
begin
   FFillType:=value;
end;

procedure TImpExpObjStyle.SetFillColor(const value:integer);
begin
   FFillColor:=value;
end;

function TImpExpObjStyle.GetLineType:integer;
begin
   result:=FLineType;
end;

function TImpExpObjStyle.GetLineColor:integer;
begin
   result:=FLineColor;
end;

function TImpExpObjStyle.GetLineWidth:integer;
begin
   result:=FLineWidth;
end;

function TImpExpObjStyle.GetFillType:integer;
begin
   result:=FFillType;
end;

function TImpExpObjStyle.GetFillColor:integer;
begin
   result:=FFillColor;
end;

function TImpExpObjStyle.HasLineType:boolean;
begin
   result:=false;
   if (FLineType <> -1) or (FLineColor <> -1) or (FLineWidth <> -1) then
      result:=true;
end;

function TImpExpObjStyle.HasFillType:boolean;
begin
   result:=false;
   if (FFillType <> -1) or (FFillColor <> -1) then
      result:=true;
end;

procedure TImpExpObjStyle.Reset;
begin
   FLineType:=-1;
   FLineColor:=-1;
   FLineWidth:=-1;
   FFillType:=-1;
   FFillColor:=-1;
end;

// -------------------------- IMPEXPROOT ---------------------------------------
constructor TImpExpRoot.Create;
begin
   FObjectType:=0;
   FParent:=nil;
end;

destructor TImpExpRoot.Destroy;
begin
end;

function TImpExpRoot.GetObjectType:integer;
begin
   result:=FObjectType;
end;

function TImpExpRoot.GetParent:TImpExpRoot;
begin
   result:=FParent;
end;

procedure TImpExpRoot.Reset;
begin
   // empty dummy implementation
end;

// ------------------------ IMPEXPBASEOBJ --------------------------------------
constructor TImpExpBaseObj.Create;
begin
   inherited Create;
   FNumAttribs:=0;
   FSpaceAttribs:=0;
   FObjectStyle:=TImpExpObjStyle.Create;
end;

destructor TImpExpBaseObj.Destroy;
var i:integer;
begin
   if FSpaceAttribs > 0 then
   begin
      for i:=0 to FSpaceAttribs-1 do
         dispose (FAttribs[i]);
   end;
   SetLength(FAttribs, 0);
   FObjectStyle.Destroy;
end;

procedure TImpExpBaseObj.Reset;
begin
   FNumAttribs:=0;
   FObjectStyle.Reset;
end;

procedure TImpExpBaseObj.AddAttrib(const aColumn:string; const aInfo:string);
var new_attrib:p_ImpExpAttrib;
begin
   if (FNumAttribs < FSpaceAttribs) then
   begin
      new_attrib:=FAttribs[FNumAttribs];
      new_attrib^.Column:=aColumn;
      new_attrib^.Info:=aInfo;
      FNumAttribs:=FNumAttribs+1;
      exit;
   end;
   new_attrib:=new(p_ImpExpAttrib);
   new_attrib^.Column:=aColumn;
   new_attrib^.Info:=aInfo;
   FNumAttribs:=FNumAttribs+1;
   FSpaceAttribs:=FNumAttribs;
   SetLength(FAttribs, FSpaceAttribs);
   FAttribs[FNumAttribs-1]:=new_attrib;
end;

procedure TImpExpBaseObj.CopyAttribsFrom(other:TImpExpBaseObj);
var i:integer;
begin
   FNumAttribs:=0;
   if (other.FNumAttribs > 0) then
   begin
      for i:=0 to other.FNumAttribs-1 do
         AddAttrib(other.FAttribs[i]^.Column, other.FAttribs[i]^.Info);
   end;
end;

function TImpExpBaseObj.GetNumAttribs:integer;
begin
   result:=FNumAttribs;
end;

function TImpExpBaseObj.GetObjectStyle:TImpExpObjStyle;
begin
   result:=FObjectStyle;
end;

function TImpExpBaseObj.GetAttrib(Index:integer):p_ImpExpAttrib;
begin
   result:=nil;
   if (Index >= FNumAttribs) or (Index < 0) then exit;
   result:=FAttribs[Index];
end;

function TImpExpBaseObj.GetAttribInfoByColumn(const aColumn:string):string;
var retVal:string;
    i:integer;
begin
   retVal:='';
   for i:=0 to FNumAttribs-1 do
   begin
      if FAttribs[i]^.Column = aColumn then
      begin
         retVal:=FAttribs[i]^.Info;
         break;
      end;
   end;
   result:=retVal;
end;

procedure TImpExpBaseObj.SetTagText(const o:string);
begin
   FTagText:=o;
end;

function TImpExpBaseObj.GetTagText:string;
begin
   result:=FTagText;
end;

// ------------------------ IMPEXPPIXEL ----------------------------------------

constructor TImpExpPixel.Create;
begin
   inherited Create;
   FObjectType:=impexp_Pixel;
   X:=0;
   Y:=0;
end;

destructor TImpExpPixel.Destroy;
begin
   inherited Destroy;
end;

procedure TImpExpPixel.SetX(const value:double);
begin
   FX:=value;
end;

function TImpExpPixel.GetX:double;
begin
   result:=FX;
end;

procedure TImpExpPixel.SetY(const value:double);
begin
   FY:=value;
end;

function TImpExpPixel.GetY:double;
begin
   result:=FY;
end;

procedure TImpExpPixel.Reset;
begin
   inherited Reset;
   FX:=0;
   FY:=0;
end;

// ------------------------ IMPEXPPOLY -----------------------------------------
constructor TImpExpPoly.Create;
begin
   inherited Create;
   FObjectType:=impexp_Poly;
   FNumCorners:=0;
   FCornerSpace:=0;
end;

destructor TImpExpPoly.Destroy;
var i:integer;
begin
   inherited Destroy;
   if FCornerSpace > 0 then
   begin
      for i:=0 to FCornerSpace-1 do
         dispose(FCorners[i]);
      Setlength(FCorners,0);
   end;
end;

procedure TImpExpPoly.Reset;
begin
   inherited Reset;
   FNumCorners:=0;
end;

function TImpExpPoly.GetCorners(Index:integer):p_ImpExpCorner;
var retVal:p_ImpExpCorner;
begin
   result:=nil;
   if (Index < 0) or (Index >= FNumCorners) then exit;
   retVal:=FCorners[Index];
   result:=retVal;
end;

function TImpExpPoly.GetNumCorners:integer;
begin
   result:=FNumCorners;
end;

procedure TImpExpPoly.SetCornerSpace(const value:integer);
var i:integer;
begin
   // set space that is reserved for corners
   if (value > FCornerSpace) then
   begin
      SetLength(FCorners, value);
      for i:=FCornerSpace to value-1 do
         FCorners[i]:=new(p_ImpExpCorner);
      FCornerSpace:=value;
   end;
end;

procedure TImpExpPoly.AddCorner(const X:double; const Y:double);
begin
   if (FNumCorners < FCornerSpace) then
   begin
      FCorners[FNumCorners]^.X:=X;
      FCorners[FNumCorners]^.Y:=Y;
      FNumCorners:=FNumCorners+1;
      exit;
   end;

   // increase corner space and add item
   SetCornerSpace(FCornerSpace+5);
   AddCorner(X,Y);
end;

// ------------------------ IMPEXPCPOLY -----------------------------------------

constructor TImpExpCPoly.Create;
begin
   inherited Create;
   FObjectType:=impexp_CPoly;
   FNumCorners:=0;
   FNumIslands:=0;
   FCornerSpace:=0;
   FSpaceIslands:=0;
end;

destructor TImpExpCPoly.Destroy;
var i:integer;
    AIsland:TImpExpCPoly;
begin
   inherited Destroy;
   // remove corners of base CPoly
   if FNumCorners > 0 then
   begin
      for i:=0 to FNumCorners-1 do
         dispose(FCorners[i]);
      Setlength(FCorners,0);
   end;
   // remove islands
   if FNumIslands > 0 then
   begin
      for i:=0 to FNumIslands-1 do
      begin
         AIsland:=FIslands[i] as TImpExpCPoly;
         AIsland.Destroy;
      end;
      SetLength(FIslands, 0);
   end;
end;

function TImpExpCPoly.GetCorners(Index:integer):p_ImpExpCorner;
var retVal:p_ImpExpCorner;
begin
   result:=nil;
   if (Index < 0) or (Index >= FNumCorners) then exit;
   retVal:=FCorners[Index];
   result:=retVal;
end;

function TImpExpCPoly.GetNumCorners:integer;
begin
   result:=FNumCorners;
end;

procedure TImpExpCPoly.SetCornerSpace(const value:integer);
var i:integer;
begin
   // set space that is reserved for corners
   if (value > FCornerSpace) then
   begin
      SetLength(FCorners, value);
      for i:=FCornerSpace to value -1 do
         FCorners[i]:=new(p_ImpExpCorner);
      FCornerSpace:=value;
   end;
end;

procedure TImpExpCPoly.AddCorner(const X:double; const Y:double);
begin
   if (FNumCorners < FCornerSpace) then
   begin
      FCorners[FNumCorners]^.X:=X;
      FCorners[FNumCorners]^.Y:=Y;
      FNumCorners:=FNumCorners+1;
      exit;
   end;

   // increase corner space and add item
   SetCornerSpace(FCornerSpace+5);
   AddCorner(X,Y);
end;

procedure TImpExpCPoly.Reset;
var i:integer;
    aLayer:TImpExpLayer;
begin
   inherited Reset;
   FNumCorners:=0;
   FNumIslands:=0;
end;

function TImpExpCPoly.GetIslands(Index:integer):TImpExpBaseObj;
begin
   result:=nil;
   if (Index < 0) or (Index > FNumIslands) then exit;
   result:=FIslands[Index];
end;

function TImpExpCPoly.GetNumIslands:integer;
begin
   result:=FNumIslands;
end;

procedure TImpExpCPoly.AddIsland(const aObject:TImpExpBaseObj);
begin
   if FNumIslands < FSpaceIslands then
   begin
      FIslands[FNumIslands]:=aObject;
      FNumIslands:=FNumIslands+1;
      exit;
   end;
   FNumIslands:=FNumIslands+1; FSpaceIslands:=FNumIslands;
   SetLength(FIslands, FNumIslands);
   aObject.FParent:=self;
   FIslands[FNumIslands-1]:=aObject;
end;

// ------------------------ IMPEXPTEXT ----------------------------------------

constructor TImpExpText.Create;
begin
   inherited Create;
   FObjectType:=impexp_Text;
   FX:=0;
   FY:=0;
   FFont:='Courier New';
   FText:='';
   FHeigth:=10; // default height is 10
   FAlign:=Align_Top_Left;
end;

destructor TImpExpText.Destroy;
begin
   inherited Destroy;
end;

procedure TImpExpText.SetX(const value:double);
begin
   FX:=value;
end;

function TImpExpText.GetX:double;
begin
   result:=FX;
end;

procedure TImpExpText.SetY(const value:double);
begin
   FY:=value;
end;

function TImpExpText.GetY:double;
begin
   result:=FY;
end;

function TImpExpText.GetAngle:double;
begin
   result:=FAngle;
end;

procedure TImpExpText.SetAngle(const value:double);
begin
   FAngle:=value;
end;

function TImpExpText.GetFont:string;
begin
   result:=FFont;
end;

procedure TImpExpText.SetFont(const value:string);
begin
   FFont:=value;
end;

function TImpExpText.GetText:string;
begin
   result:=FText;
end;

procedure TImpExpText.SetText(const value:string);
begin
   FText:=value;
end;


procedure TImpExpText.SetHeight(const value:double);
begin
   FHeigth:=value;
end;

function TImpExpText.GetHeigth:double;
begin
   result:=FHeigth;
end;

procedure TImpExpText.SetAlign(const value:integer);
begin
   FAlign:=value;
end;

function TImpExpText.GetAlign:integer;
begin
   result:=FAlign;
end;

// ------------------------ IMPEXPSYMBOL ---------------------------------------

constructor TImpExpSymbol.Create;
begin
   inherited Create;
   FObjectType:=impexp_Symbol;
   FX:=0;
   FY:=0;
   FName:='';
   FSize:=0;
end;

destructor TImpExpSymbol.Destroy;
begin
   inherited Destroy;
end;

procedure TImpExpSymbol.SetX(const value:double);
begin
   FX:=value;
end;

function TImpExpSymbol.GetX:double;
begin
   result:=FX;
end;

procedure TImpExpSymbol.SetY(const value:double);
begin
   FY:=value;
end;

function TImpExpSymbol.GetY:double;
begin
   result:=FY;
end;

function TImpExpSymbol.GetAngle:double;
begin
   result:=FAngle;
end;

procedure TImpExpSymbol.SetAngle(const value:double);
begin
   FAngle:=value;
end;

function TImpExpSymbol.GetName:string;
begin
   result:=FName;
end;

procedure TImpExpSymbol.SetName(const value:string);
begin
   FName:=value;
end;


procedure TImpExpSymbol.SetSize(const value:double);
begin
   FSize:=value;
end;

function TImpExpSymbol.GetSize:double;
begin
   result:=FSize;
end;

// ------------------------ IMPEXPLAYER ----------------------------------------

constructor TImpExpLayer.Create;
begin
   inherited Create;
   FObjectType:=impexp_Layer;
   FCreatedInGis:=false;
   NumColDefs:=0;
   FName:='';
   NumObjects:=0;
   Num_Pixel:=0;
   Num_Poly:=0;
   Num_CPoly:=0;
   Num_Text:=0;
   Num_Symbol:=0;
end;

destructor TImpExpLayer.Destroy;
begin
   ClearColumnDefs;
   ClearItems;
   SetLength(Objects, 0);
end;

procedure TImpExpLayer.ClearItems;
var i:integer;
    aObjKeeper:TImpExpObjectKeeper;
    aDataMgr  :TImpExpDataMgr;
begin
   if NumObjects > 0 then
   begin
      // check if object can be given back to object keeper
      aObjKeeper:=nil;
      if (FParent <> nil) and (FParent.ObjectType = impexp_Datamgr) then
      begin
         aDataMgr:=FParent as TImpExpDataMgr;
         if (aDataMgr.ObjectKeeper <> nil) and (aDataMgr.ObjectKeeper.ObjectType = impexp_ObjectKeeper) then
            aObjKeeper:=aDataMgr.ObjectKeeper as TImpExpObjectKeeper;
      end;
      for i:=0 to NumObjects-1 do
      begin
         if (Objects[i] <> nil) then
         begin
            if aObjKeeper <> nil then
               aObjKeeper.ReleaseObject(Objects[i])  // give object back to datamgr
            else
               Objects[i].Destroy;
            Objects[i]:=nil;
         end;
      end;
   end;
   NumObjects:=0;
   Num_Pixel:=0;
   Num_Poly:=0;
   Num_CPoly:=0;
   Num_Text:=0;
   Num_Symbol:=0;
end;

procedure TImpExpLayer.ClearColumnDefs;
var i:integer;
begin
   if NumColDefs > 0 then
   begin
      for i:=0 to NumColDefs-1 do
      begin
         dispose(ColDefs[i]);
      end;
      SetLength(ColDefs, 0);
   end;
   NumColDefs:=0;
end;

procedure TImpExpLayer.SetName(const aName:string);
begin
   FName:=aName;
end;

function TImpExpLayer.GetName:string;
begin
   result:=FName;
end;

function TImpExpLayer.GetNumColumnDefs:integer;
begin
   result:=NumColDefs;
end;

procedure TImpExpLayer.AddColumnDef(const aColname:string; const aColType:integer; const aColLength:integer);
var new_item:p_ImpExpDbColumn;
begin
   new_item:=new(p_ImpExpDbColumn);
   new_item^.ColName:=aColName;
   new_item^.ColType:=aColType;
   new_item^.ColLength:=aColLength;
   NumColDefs:=NumColDefs+1;
   SetLength(ColDefs, NumColDefs);
   ColDefs[NumColDefs-1]:=new_item;
end;

function TImpExpLayer.GetColumnDef(Index:integer):p_ImpExpDbColumn;
begin
   result:=nil;
   if (Index >= NumColDefs) or (Index < 0) then exit;
   result:=ColDefs[Index];
end;

procedure TImpExpLayer.AddItem(const aObject:TImpExpBaseObj);
begin
   if NumObjects < SpaceObjects then
   begin
      Objects[NumObjects]:=aObject;
   end
   else
   begin
      SpaceObjects:=SpaceObjects+1;
      SetLength(Objects, SpaceObjects);
      Objects[SpaceObjects-1]:=aObject;
   end;
   NumObjects:=NumObjects+1;
   aObject.FParent:=self;
   // update counters
   if aObject.ObjectType = impexp_Pixel then Num_Pixel:=Num_Pixel+1
   else if aObject.ObjectType = impexp_Poly then Num_Poly:=Num_Poly+1
   else if aObject.ObjectType = impexp_CPoly then Num_CPoly:=Num_CPoly+1
   else if aObject.ObjectType = impexp_Text then Num_Text:=Num_Text+1
   else if aObject.ObjectType = impexp_Symbol then Num_Symbol:=Num_Symbol+1;
end;

function TImpExpLayer.GetItems(Index:integer):TImpExpBaseObj;
begin
   result:=nil;
   if (Index >= NumObjects) or (Index < 0) then exit;
   result:=Objects[Index];
end;

procedure TImpExpLayer.SetItems(Index:integer; const aObj:TImpExpBaseObj);
begin
   if (Index >= NumObjects) or (Index < 0) then exit;
   Objects[Index]:=aObj;
end;

function TImpExpLayer.GetNumItems:integer;
begin
   result:=NumObjects;
end;

function  TImpExpLayer.GetCreatedInGis:boolean;
begin
   result:=FCreatedInGis;
end;

procedure TImpExpLayer.SetCreatedInGis(const value:boolean);
begin
   FCreatedInGis:=value;
end;

function TImpExpLayer.ColExists(const aColname:string):boolean;
var retVal:boolean;
    i:integer;
begin
   retVal:=FALSE;
   if NumColDefs > 0 then
   begin
      for i:=0 to NumColDefs -1 do
      begin
         if ColDefs[i]^.ColName = aColname then
         begin
            retVal:=TRUE;
            break;
         end;
      end;
   end;
   result:=retVal;
end;

procedure TImpExpLayer.CopyColumnDefs(var o:TImpExpLayer);
var i:integer;
    aColDef:p_ImpExpDbColumn;
begin
   if (NumColDefs = 0) and (o.NumColDefs > 0) then
   begin
      for i:=0 to o.NumColDefs-1 do
      begin
         aColDef:=o.ColDefs[i];
         AddColumnDef(aColDef^.ColName, aColDef^.ColType, aColDef^.ColLength);
      end;
   end;
end;

// ----------------------- IMPEXPDATAMGR ---------------------------------------
constructor TImpExpDataMgr.Create;
begin
   inherited Create;
   FObjectType:=impexp_DataMgr;
   FObjectKeeper:=nil;
   NumLayers:=0;
   SearchIdx:=-1;
end;

destructor TImpExpDataMgr.Destroy;
var i:integer;
begin
   if NumLayers > 0 then
   begin
      for i:=0 to NumLayers-1 do
      begin
         Layers[i].Destroy;
      end;
      SetLength(Layers,0);
   end;
end;

function TImpExpDataMgr.GetItems(Index:integer):TImpExpLayer;
begin
   if (Index < 0) or (Index >= NumLayers) then
      result:=nil
   else
      result:=Layers[Index];
end;

function TImpExpDataMgr.GetNumItems:integer;
begin
   result:=NumLayers;
end;

function TImpExpDataMgr.GetLayer(const aLayername:string):TImpExpLayer;
var i:integer;
    retVal:TImpExpLayer;
begin
   retVal:=nil;
   if (SearchIdx >= 0) and (SearchIdx < NumLayers) and (Layers[SearchIdx].Name = aLayername) then
      retVal:=Layers[SearchIdx];

   if (retVal = nil) then
   begin
      for i:=0 to NumLayers-1 do
      begin
         if Layers[i].Name = aLayername then
         begin
            retVal:=Layers[i];
            SearchIdx:=i;
            break;
         end;
      end;
   end;
   result:=retVal;
end;

function TImpExpDataMgr.RequestLayer(const aLayername:string):TImpExpLayer;
var retVal:TImpExpLayer;
begin
   retVal:=GetLayer(aLayername);
   if (retVal = nil) then
   begin
      // create new layer
      retVal:=TImpExpLayer.Create;
      retVal.Name:=aLayername;
      retVal.FParent:=self;
      NumLayers:=NumLayers+1;
      SetLength(Layers, NumLayers);
      Layers[NumLayers-1]:=retVal;
   end;
   result:=retVal;
end;

procedure TImpExpDataMgr.SetObjectKeeper(const o:TImpExpRoot);
begin
   FObjectKeeper:=o;
end;

function TImpExpDataMgr.GetObjectKeeper: TImpExpRoot;
begin
   result:=FObjectKeeper;
end;

// --------------------- IMPEXPOBJECTKEEPER ------------------------------------
constructor TImpExpObjectKeeper.Create;
begin
   inherited Create;
   FObjectType:=impexp_ObjectKeeper;

   CPoly_Num   :=0;
   CPoly_Space :=0;

   Poly_Num   :=0;
   Poly_Space :=0;

   Pixel_Num  :=0;
   Pixel_Space:=0;

   Text_Num   :=0;
   Text_Space :=0;
end;

destructor TImpExpObjectKeeper.Destroy;
var i:integer;
begin
   if CPoly_Num > 0 then
   begin
      for i:=0 to CPoly_Num -1 do
        CPoly_Buffer[i].Destroy;
   end;
   SetLength(CPoly_Buffer, 0);

   if Poly_Num > 0 then
   begin
      for i:=0 to Poly_Num -1 do
        Poly_Buffer[i].Destroy;
   end;
   SetLength(Poly_Buffer, 0);

   if Pixel_Num > 0 then
   begin
      for i:=0 to Pixel_Num -1 do
        Pixel_Buffer[i].Destroy;
   end;
   SetLength(Pixel_Buffer, 0);

   if Text_Num > 0 then
   begin
      for i:=0 to Text_Num -1 do
        Text_Buffer[i].Destroy;
   end;
   SetLength(Text_Buffer, 0);
end;

function TImpExpObjectKeeper.Request_Pixel:TImpExpPixel;
var retVal:TImpExpPixel;
begin
   retVal:=nil;
   if (Pixel_Num > 0) then
   begin
      retVal:=Pixel_Buffer[Pixel_Num-1];
      Pixel_Num:=Pixel_Num-1;
      result:=retVal;
      exit;
   end;
   retVal:=TImpExpPixel.Create;
   result:=retVal;
end;

function TImpExpObjectKeeper.Request_CPoly:TImpExpCPoly;
var retVal:TImpExpCPoly;
begin
   retVal:=nil;
   if (CPoly_Num > 0) then
   begin
      retVal:=CPoly_Buffer[CPoly_Num-1];
      CPoly_Num:=CPoly_Num-1;
      result:=retVal;
      exit;
   end;
   retVal:=TImpExpCPoly.Create;
   result:=retVal;
end;

function TImpExpObjectKeeper.Request_Poly:TImpExpPoly;
var retVal:TImpExpPoly;
begin
   retVal:=nil;
   if (Poly_Num > 0) then
   begin
      retVal:=Poly_Buffer[Poly_Num-1];
      Poly_Num:=Poly_Num-1;
      result:=retVal;
      exit;
   end;
   retVal:=TImpExpPoly.Create;
   result:=retVal;
end;

function TImpExpObjectKeeper.Request_Text:TImpExpText;
var retVal:TImpExpText;
begin
   retVal:=nil;
   if (Text_Num > 0) then
   begin
      retVal:=Text_Buffer[Text_Num-1];
      Text_Num:=Text_Num-1;
      result:=retVal;
      exit;
   end;
   retVal:=TImpExpText.Create;
   result:=retVal;
end;

procedure TImpExpObjectKeeper.ReleaseObject(var obj:TImpExpBaseObj);
var i:integer;
    aCPoly:TImpExpCPoly;
    aPoly :TImpExpPoly;
    aPixel:TImpExpPixel;
    aText :TImpExpText;
    aBase :TImpExpBaseObj;
begin
   if obj.ObjectType = impexp_CPoly then
   begin
      // first release all islands
      aCPoly:=obj as TImpExpCPoly;
      for i:=0 to aCPoly.NumIslands-1 do
      begin
         aBase:=aCPoly.Islands[i];
         ReleaseObject(aBase);
      end;
      aCPoly.Reset;
      if CPoly_Num < CPoly_Space then
      begin
         CPoly_Buffer[CPoly_Num]:=aCPoly;
         CPoly_Num:=CPoly_Num+1;
      end
      else
      begin
         SetLength(CPoly_Buffer, CPoly_Space+1);
         CPoly_Buffer[CPoly_Space]:=aCPoly;
         CPoly_Space:=CPoly_Space+1;
         CPoly_Num:=CPoly_Num+1;
      end;
   end
   else if obj.ObjectType = impexp_Poly then
   begin
      // first release all islands
      aPoly:=obj as TImpExpPoly;
      aPoly.Reset;
      if Poly_Num < Poly_Space then
      begin
         Poly_Buffer[Poly_Num]:=aPoly;
         Poly_Num:=Poly_Num+1;
      end
      else
      begin
         SetLength(Poly_Buffer, Poly_Space+1);
         Poly_Buffer[Poly_Space]:=aPoly;
         Poly_Space:=Poly_Space+1;
         Poly_Num:=Poly_Num+1;
      end;
   end
   else if obj.ObjectType = impexp_Pixel then
   begin
      // first release all islands
      aPixel:=obj as TImpExpPixel;
      aPixel.Reset;
      if Pixel_Num < Pixel_Space then
      begin
         Pixel_Buffer[Pixel_Num]:=aPixel;
         Pixel_Num:=Pixel_Num+1;
      end
      else
      begin
         SetLength(Pixel_Buffer, Pixel_Space+1);
         Pixel_Buffer[Pixel_Space]:=aPixel;
         Pixel_Space:=Pixel_Space+1;
         Pixel_Num:=Pixel_Num+1;
      end;
   end
   else if obj.ObjectType = impexp_Text then
   begin
      // first release all islands
      aText:=obj as TImpExpText;
      aText.Reset;
      if Text_Num < Text_Space then
      begin
         Text_Buffer[Text_Num]:=aText;
         Text_Num:=Text_Num+1;
      end
      else
      begin
         SetLength(Text_Buffer, Text_Space+1);
         Text_Buffer[Text_Space]:=aText;
         Text_Space:=Text_Space+1;
         Text_Num:=Text_Num+1;
      end;
   end;
end;

end.
