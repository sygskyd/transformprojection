unit SetShpfileDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,StrFile,
  StdCtrls, Buttons, ExtCtrls, FileCtrl, ComCtrls, Db, ADODB, ShellCtrls,
  FileSelectionPanel, WinOSInfo;

type
  TSetShpfileWnd = class(TForm)
    PageControl: TPageControl;
    TabSheetFiles: TTabSheet;
    GeoBtn: TButton;
    OKBtn: TButton;
    CancelBtn: TButton;
    SetupWindowTimer: TTimer;
    AprCB: TCheckBox;
    AprPanel: TPanel;
    AprBtn: TButton;
    AprLabel: TLabel;
    OpenDialogApr: TOpenDialog;
    Panel1: TPanel;
    DbBtn: TButton;
    DestDbLabel: TLabel;
    FileSelectionPanel: TFileSelectionPanel;
    ShellTreeView: TShellTreeView;
    FileListBox: TFileListBox;
    ListBox: TListBox;
    TrashPanel: TPanel;
    TrashImage: TImage;
    AddAllBtn: TButton;
    DeleteAllBtn: TButton;
    DkmCb: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GeoBtnClick(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure AprBtnClick(Sender: TObject);
    procedure AprCBClick(Sender: TObject);
    procedure DbBtnClick(Sender: TObject);
    procedure FileSelectionPanelFilesToConvertChanged(Sender: TObject);
  private
    { Private-Deklarationen}
    FirstActivate:boolean;
    AprFileName  :string;
    CurrentFileIdx:integer;
    procedure CheckWindow;
  public
    { Public-Deklarationen}
    winheight:integer;
    winwidth :integer;
    wintop   :integer;
    winleft  :integer;
    OkBtnMode:boolean;
    function  GetFilename(var Dateiname:string):boolean;
    procedure CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
    function  GetAprFileName:string;
    procedure SetupForBatch;
    procedure SetImpSettings(AprMode:integer; AprFname:string);
    procedure AddImpFile(aFilename:string);
  end;

var SetShpfileWnd: TSetShpfileWnd;

implementation

uses Maindlg, inifiles;

{$R *.DFM}

procedure TSetShpfileWnd.FormCreate(Sender: TObject);
var myScale:double;
    oldHeight:integer;
    oldWidth:integer;
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   Self.Caption:=MainWnd.StrFileObj.Assign(1);     { ARCVIEW-SHP Import 2000 }
   OKBtn.Caption:=MainWnd.StrFileObj.Assign(4);            { Ok }
   CancelBtn.Caption:=MainWnd.StrFileObj.Assign(5);        { Cancel }

   TabSheetFiles.Caption:=MainWnd.StrFileObj.Assign(6);    { Files    }
   GeoBtn.Caption:=MainWnd.StrFileObj.Assign(11);         { Projection }
   DbBtn.Caption:=MainWnd.StrFileObj.Assign(7);           // Database
   AprCB.Caption:=MainWnd.StrFileObj.Assign(46);          { Apr-File:  }

   AprPanel.visible:=false;
   AprBtn.visible:=false;
   AprLabel.Caption:='';
   AprFileName:='';

   FirstActivate:=true;
   CurrentFileIdx:=0;

   oldHeight:=Self.Height;
   oldWidth:=Self.Width;

   Self.Height:=1;
   Self.Width:=1;

   myScale:=1; //100000 / Trunc(9600000/(Self.PixelsPerInch));
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
   
   // now adapt winheigt and winwidth
   winwidth:=round(oldWidth*myScale);
   winheight:=round(oldHeight*myScale)
end;

procedure TSetShpfileWnd.GeoBtnClick(Sender: TObject);
begin
   MainWnd.AxGisProjection.SetupByDialog;
end;

procedure TSetShpfileWnd.OKBtnClick(Sender: TObject);
var inifile:TIniFile;
begin
   MainWnd.ExitNormal:=true;

   // write current settings to the ConGIS.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir + 'ConGIS.ini');
   inifile.WriteString('SHPIMP','ImpDbMode',inttostr(MainWnd.AXImpExpDbc.ImpDbMode));
   inifile.WriteString('SHPIMP','ImpDatabase',MainWnd.AXImpExpDbc.ImpDatabase);
   inifile.WriteString('SHPIMP','ImpTable',MainWnd.AXImpExpDbc.ImpTable);

   inifile.WriteString('SHPIMP','SourceCoordinateSystem',inttostr(MainWnd.AxGisProjection.SourceCoordinateSystem));
   inifile.WriteString('SHPIMP','SourceProjSettings',MainWnd.AxGisProjection.SourceProjSettings);
   inifile.WriteString('SHPIMP','SourceProjection',MainWnd.AxGisProjection.SourceProjection);
   inifile.WriteString('SHPIMP','SourceDate',MainWnd.AxGisProjection.SourceDate);
   inifile.WriteString('SHPIMP','SourceXOffset',floattostr(MainWnd.AxGisProjection.SourceXOffset));
   inifile.WriteString('SHPIMP','SourceYOffset',floattostr(MainWnd.AxGisProjection.SourceYOffset));
   inifile.WriteString('SHPIMP','SourceScale',floattostr(MainWnd.AxGisProjection.SourceScale));

   inifile.WriteString('SHPIMP','LastUsedPath',FileSelectionPanel.Directory);
   if DkmCb.Checked then
      inifile.WriteString('SHPIMP', 'ProcessAsDkm', 'TRUE')
   else
      inifile.WriteString('SHPIMP', 'ProcessAsDkm', 'TRUE');
   inifile.Destroy;

   // now the import window can be opened and the import executed
   Self.Close;
   MainWnd.Show;
end;

procedure TSetShpfileWnd.CancelBtnClick(Sender: TObject);
begin
   MainWnd.ExitNormal:=false;
   MainWnd.CloseTimer.Enabled:=true;
end;

procedure TSetShpfileWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TSetShpfileWnd.CheckWindow;
var allok:boolean;
begin
   allok:=true;
   if FileSelectionPanel.NumFilesToConvert = 0 then allok:=false;
   if ((AprCB.Checked) and (AprFileName = '')) then allok:=false;
   if allok then OkBtn.Enabled:=true
   else OkBtn.Enabled:=false;
end;

function TSetShpfileWnd.GetFilename(var Dateiname:string):boolean;
var retVal:boolean;
begin
   retVal:=TRUE;
   if CurrentFileIdx < FileSelectionPanel.NumFilesToConvert then
      Dateiname:=FileSelectionPanel.GetFilename(CurrentFileIdx)
   else
      retVal:=FALSE;
   result:=retVal;
   CurrentFileIdx:=CurrentFileIdx+1;
end;

function  TSetShpfileWnd.GetAprFileName:string;
begin
   result:=AprFileName;
end;

procedure TSetShpfileWnd.CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
begin
   wintop:=(WHEIGHT div 2) - (winheight div 2);
   winleft:=(WWIDTH div 2) - (winwidth div 2);
end;

procedure TSetShpfileWnd.SetupWindowTimerTimer(Sender: TObject);
var dummy:string;
    intval, count:integer;
    doubleval:double;
    inifile:TIniFile;
begin
   SetupWindowTimer.Enabled:=false;
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,winleft,wintop,winwidth, winheight,SWP_SHOWWINDOW);
   Self.Height:=winheight;
   Self.Width:=winwidth;

   // Setup the window from the ConGIS.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'ConGIS.ini');

   // read the Temporary .dbf Path
   dummy:=inifile.ReadString('GENERAL','TMPDIR','');
   if dummy <> '' then
      MainWnd.AXImpExpDbc.TmpDBaseDir:=dummy;

   dummy:=MainWnd.DestDocument.Name;
   dummy:=ChangeFileExt(dummy,'.wgi');
   if FileExists(dummy) then
   begin
      MainWnd.AXImpExpDbc.ImpDbMode:=5; // Idb-Database
      MainWnd.AXImpExpDbc.ImpDatabase:='';
      MainWnd.AXImpExpDbc.ImpTable:='';
   end
   else
   begin
      dummy:=inifile.ReadString('SHPIMP','ImpDbMode','0');
      val(dummy, intval, count);
      if intval <> 5 then // Idb-Database
      begin
         MainWnd.AXImpExpDbc.ImpDbMode:=intval;
         dummy:=inifile.ReadString('SHPIMP','ImpDatabase','');
         MainWnd.AXImpExpDbc.ImpDatabase:=dummy;
         dummy:=inifile.ReadString('SHPIMP','ImpTable','');
         MainWnd.AXImpExpDbc.ImpTable:=dummy;
      end;
   end;
   DestDbLabel.Caption:=MainWnd.AXImpExpDbc.ImpDbInfo;

   // setup the projection
   dummy:=inifile.ReadString('SHPIMP','SourceCoordinateSystem','');
   if dummy <> '' then
   begin
      val(dummy, intval, count);
      MainWnd.AxGisProjection.SourceCoordinateSystem:=intval;
      dummy:=inifile.ReadString('SHPIMP','SourceProjSettings','NONE');
      MainWnd.AxGisProjection.SourceProjSettings:=dummy;
      dummy:=inifile.ReadString('SHPIMP','SourceProjection','NONE');
      MainWnd.AxGisProjection.SourceProjection:=dummy;
      dummy:=inifile.ReadString('SHPIMP','SourceDate','NONE');
      MainWnd.AxGisProjection.SourceDate:=dummy;
      dummy:=inifile.ReadString('SHPIMP','SourceXOffset','0');
      val(dummy, doubleval, count);
      MainWnd.AxGisProjection.SourceXOffset:=doubleval;
      dummy:=inifile.ReadString('SHPIMP','SourceYOffset','0');
      val(dummy, doubleval, count);
      MainWnd.AxGisProjection.SourceYOffset:=doubleval;
      dummy:=inifile.ReadString('SHPIMP','SourceScale','1');
      val(dummy, doubleval, count);
      MainWnd.AxGisProjection.SourceScale:=doubleval;
   end;

   dummy:=inifile.ReadString('SHPIMP','LastUsedPath','');
   if dummy = '' then
      GetDir(0, dummy);
   FileSelectionPanel.Directory:=dummy;
   dummy:=inifile.ReadString('SHPIMP', 'ProcessAsDkm', 'FALSE');
   if MainWnd.DestDocument.IsModuleRegistered(106) then
   begin
      AprPanel.Width:=250;
      AprBtn.Left:=320;
      //DkmCb.Visible:=true;
   end
   else
   begin
      AprPanel.Width:=337;
      AprBtn.Left:=407;
      //DkmCb.Visible:=false;
      dummy:='FALSE';
   end;
   if dummy = 'TRUE' then
      DkmCb.Checked:=true
   else
      DkmCb.Checked:=false;
   inifile.Destroy;

   Self.Repaint;
end;

procedure TSetShpfileWnd.AprBtnClick(Sender: TObject);
begin
   if OpenDialogApr.Execute then
   begin
      AprFileName:=OpenDialogApr.Filename;
      AprLabel.Caption:=AprFileName;
   end;
end;

procedure TSetShpfileWnd.AprCBClick(Sender: TObject);
begin
   if AprCB.checked then
   begin
     AprPanel.visible:=true;
     AprBtn.visible:=true;
   end
   else
   begin
     AprPanel.visible:=false;
     AprBtn.visible:=false;
   end;
end;

// procedure is used to setup the SetShpfileWnd for batch mode
procedure TSetShpfileWnd.SetupForBatch;
begin
   if FirstActivate then
   begin
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
   end;
end;

procedure TSetShpfileWnd.SetImpSettings(AprMode:integer; AprFname:string);
begin
   if AprMode = 0 then
   begin
      AprCB.Checked:=false;
      AprFileName:='';
   end;
   if AprMode = 1 then
   begin
      AprCB.Checked:=true;
      AprFileName:=AprFname;
   end;
end;

procedure TSetShpfileWnd.AddImpFile(aFilename:string);
begin
   FileSelectionPanel.AddFileToConvert(aFilename);
end;

procedure TSetShpfileWnd.DbBtnClick(Sender: TObject);
begin
   MainWnd.AXImpExpDbc.SetupByDialog(Self.Handle, 5, 5);
   PageControl.Enabled:=FALSE;
   GeoBtn.Enabled:=FALSE;
   DbBtn.Enabled:=FALSE;
   OkBtnMode:=OkBtn.Enabled;
   OkBtn.Enabled:=FALSE;
   CancelBtn.Enabled:=FALSE;
end;

procedure TSetShpfileWnd.FileSelectionPanelFilesToConvertChanged(
  Sender: TObject);
begin
   CheckWindow;
end;

end.
