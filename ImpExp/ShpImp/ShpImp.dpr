library ShpImp;

{ Wichtiger Hinweis zur DLL-Speicherverwaltung: Sharemem mu� die erste
 Unit in der uses-Klausel Ihrer Bibliothek und Ihres Projekts sein,
 wenn Ihre DLL Prozeduren und Funktionen exportiert, die
 Strings als Parameter oder Funktionsergebnisse �bergeben. Dies
 betrifft alle Strings, die an Ihre DLL oder von Ihrer DLL �bergeben werden--selbst diejenigen,
 die sich in Records oder Klassen befinden. Sharemem ist die Schnittstelle zu
 BORLNDMM.DLL, die den gemeinsamen Zugriff auf den Speicher verwaltet; DELPHIMM.DLL
 mu� mit Ihrer DLL weitergegeben werden. Wenn Sie BORLNDMM.DLL nicht verwenden m�chten, m�ssen
 Strings als PChar oder ShortString �bergeben werden.}

uses
  SysUtils,
  Classes,
  Dialogs,
  Maindlg in 'maindlg.pas' {MainWnd},
  SetShpfileDlg in 'SetShpfileDlg.pas' {SetshpfileWnd},
  ShapeReader in 'ShapeReader.pas',
  Strfile in 'Strfile.pas',
  comobj,
  AXDLL_TLB,
  AprReader in 'AprReader.pas',
  ImpExpObjects in 'ImpExpObjects.pas';
  //ShpDkmManager in 'ShpDkmManager.pas';

{$R *.RES}

{*******************************************************************************
* PROZEDURE * SHPIMPORT                                                        *
********************************************************************************
* Procedure is used to import ARCVIEW data into GIS                            *
*                                                                              *
********************************************************************************}
procedure SHPIMPORT(DIR:PCHAR;var AXHANDLE:VARIANT;var DEST:VARIANT;REGMODE:integer;LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var aDisp:IDispatch;
begin
   // create MainWnd
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.App:=AXHANDLE;
   aDisp:=DEST;
   MainWnd.DestDocument:=aDisp as IDocument;
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.Regmode:=REGMODE;
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.CreateStringFile;

   // create SetShpFileWnd
   SetShpFileWnd:=TSetShpFileWnd.Create(nil);
   SetShpFileWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);

   if (REGMODE = 2) then // check if in Demo-Mode
      Showmessage('Shape import in demo mode - only 100 objects will be imported');

   if (REGMODE = 1) or (REGMODE = 2) then // start only when registered or demo-mode
   begin
      SetShpFileWnd.Show;
   end
   else
      Showmessage('Shape import not registered');
end;

{*******************************************************************************
* PROCEDURE * CREATEIMP                                                        *
********************************************************************************
* Procedure is used to prepare a SHP-import for Batchmode                      *
*                                                                              *
********************************************************************************}
procedure CREATEIMP(DIR:PCHAR;var AXHANDLE:VARIANT;var DEST:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var aDisp:IDispatch;
begin
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.App:=AXHANDLE;
   aDisp:=DEST;
   MainWnd.DestDocument:=aDisp as IDocument;
   MainWnd.Regmode:=REGMODE;
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.CreateStringFile;

   // create SetShpFileWnd
   SetShpFileWnd:=TSetShpFileWnd.Create(nil);
   SetShpFileWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);
   SetShpFileWnd.SetupForBatch;
end;

{*******************************************************************************
* PROCEDURE * SETSHPIMPSETTINGS                                                *
********************************************************************************
* Procedure is used to set type of import                                      *
*                                                                              *
* PARAMETERS: APRMODE     -> Type if an .apr file should be used or not        *
*                            0 -> no .apr file should be used.                 *
*                            1 -> .apr file should be used.                    *
*             APRNAME     -> Name of the .apr file that should be usec.        *
*                                                                              *
********************************************************************************}
procedure SETSHPIMPSETTINGS(APRMODE:INTEGER; APRNAME:PCHAR);stdcall;
begin
   SetShpFileWnd.SetImpSettings(APRMODE, strpas(APRNAME));
end;

// procedure is used to add a .shp file that should be imported
procedure ADDIMPFILE(AFILE:PCHAR);stdcall;
begin
   SetShpFileWnd.AddImpFile(strpas(AFILE));
end;

// procedure is used to set database-settings
procedure SETDBSETTINGS(SOURCEDBMODE:INTEGER; SOURCEDBNAME:PCHAR; SOURCEDBTABLE:PCHAR;
                        DESTDBMODE  :INTEGER; DESTDBNAME  :PCHAR; DESTDBTABLE  :PCHAR);stdcall;
begin
   // source database will be ignored for SHP-Import
   MainWnd.AXImpExpDbc.ImpDbMode:=DESTDBMODE;
   MainWnd.AXImpExpDbc.ImpDatabase:=strpas(DESTDBNAME);
   MainWnd.AxImpExpDbc.ImpTable:=strpas(DESTDBTABLE);
end;

// procedure is used to set projection for batch mode
procedure SETPROJECTION(SOURCECOORDINATESYSTEM:INTEGER; // source projection
                        SOURCEPROJECTION      :PCHAR;
                        SOURCEDATE            :PCHAR;
                        SOURCEPROJSETTINGS    :PCHAR;
                        SOURCEXOFFSET         :DOUBLE;
                        SOURCEYOFFSET         :DOUBLE;
                        SOURCESCALE           :DOUBLE;
                        TARGETCOORDINATESYSTEM:INTEGER; // target projection
                        TARGETPROJECTION      :PCHAR;
                        TARGETDATE            :PCHAR;
                        TARGETPROJSETTINGS    :PCHAR;
                        TARGETXOFFSET         :DOUBLE;
                        TARGETYOFFSET         :DOUBLE;
                        TARGETSCALE           :DOUBLE);stdcall;
begin
   // source projection
   MainWnd.AxGisProjection.SourceCoordinateSystem:=SOURCECOORDINATESYSTEM;
   MainWnd.AxGisProjection.SourceProjection:=strpas(SOURCEPROJECTION);
   MainWnd.AxGisProjection.SourceDate:=strpas(SOURCEDATE);
   MainWnd.AxGisProjection.SourceProjSettings:=strpas(SOURCEPROJSETTINGS);
   MainWnd.AxGisProjection.SourceXOffset:=SOURCEXOFFSET;
   MainWnd.AxGisProjection.SourceYOffset:=SOURCEYOFFSET;
   MainWnd.AxGisProjection.SourceScale:=SOURCESCALE;

   // target projection
   MainWnd.AxGisProjection.TargetCoordinateSystem:=TARGETCOORDINATESYSTEM;
   MainWnd.AxGisProjection.TargetProjection:=strpas(TARGETPROJECTION);
   MainWnd.AxGisProjection.TargetDate:=strpas(TARGETDATE);
   MainWnd.AxGisProjection.TargetProjSettings:=strpas(TARGETPROJSETTINGS);
   MainWnd.AxGisProjection.TargetXOffset:=TARGETXOFFSET;
   MainWnd.AxGisProjection.TargetYOffset:=TARGETYOFFSET;
   MainWnd.AxGisProjection.TargetScale:=TARGETSCALE;
end;

// function is used to execute a shp-import
procedure EXECIMP;stdcall;
begin
   MainWnd.ExitNormal:=true;
   MainWnd.Show;
end;


function FREEDLL:boolean; stdcall;
var retVal:boolean;
begin
   retVal:=MainWnd.ExitNormal;
   MainWnd.Free;
   SetShpFileWnd.Free;
   result:=retVal;
end;

exports SHPIMPORT, CREATEIMP, SETSHPIMPSETTINGS, ADDIMPFILE,
        SETDBSETTINGS, SETPROJECTION, EXECIMP, FREEDLL;

begin
end.
