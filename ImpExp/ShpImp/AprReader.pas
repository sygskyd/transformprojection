unit AprReader;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Math, ShapeReader, ImpExpObjects, Db, Strfile;

type
   // define structures
   p_Block_item=^Block_item;
   Block_item = record
       Code :string;
       Info :string;
       next :p_Block_item;
   end;

   p_Block=^Block;
   Block = record
       Number:integer; // number of block
       Name:string;    // name of block
       Items:p_Block_item;
       next :p_Block;
   end;

   TAprReader = class
   public
      constructor Create;
      destructor  Destroy;
      procedure SetFileName(filename:string; var aShpReader:TShapeReader);
      procedure GetLayerNameAndProperties(Filename:string; var Layername:string; var LineType:integer; var LineColor:integer; var FillType:integer; var FillColor:integer);
      procedure Read;
   private
      AprFilename:string;
      AprFile    :TextFile;
      FirstBlock :p_Block;
      LastBlock  :p_Block;

      ShpReader  :TShapeReader;
      aText      :TImpExpText;

      procedure ReadBlock(Line:string);
      procedure Read_BlockItems(var BlockRef:p_Block);
      procedure InsertBlock(BlockRef:p_Block);
      procedure InsertBlockItem(var BlockRef:p_Block; Code:string; Info:string);
      function  GetBlockByNumber(Nr:integer):p_Block;

      procedure Read_GText(BlockRef:p_Block);
      procedure Read_RectD(BlockRef:p_Block; var left:double; var top:double; var right:double; var bottom:double);
      procedure Read_TxSym(BlockRef:p_Block);
      procedure GetCodeAndInfo(Line:string; var Code:string; var Info:string);

      function Read_TClr(BlockRef:p_Block):integer;
      function Read_NFont(BlockRef:p_Block):string;
      function Read_AVStr(BlockRef:p_Block):string;
      procedure Read_FTheme(Blockref:p_Block; var ShapeName:string; var LayerName:string; var LineType:integer; var LineColor:integer; var FillType:integer; var FillColor:integer);
      function Read_ShpSrc(BlockRef:p_Block):string;
      function Read_SrcName(BlockRef:p_Block):string;
      procedure Read_Legend(BlockRef:p_Block; var LineType:integer; var LineColor:integer; var FillType:integer; var FillColor:integer);
      procedure Read_SymList(BlockRef:p_Block; var LineType:integer; var LineColor:integer; var FillType:integer; var FillColor:integer);
      procedure Read_BShSym(BlockRef:p_Block; var LineType:integer; var LineColor:integer; var FillType:integer; var FillColor:integer);
      procedure Read_BMkSym(BlockRef:p_Block; var LineType:integer; var LineColor:integer; var FillType:integer; var FillColor:integer);
      procedure Read_BLnSym(BlockRef:p_Block; var LineType:integer; var LineColor:integer; var FillType:integer; var FillColor:integer);
      function Hextoint(Value:string):longint;
end;

implementation
uses MainDlg;

constructor TAprReader.Create;
begin
   FirstBlock:=new(p_Block); FirstBlock:=nil;
   LastBlock:=new(p_Block); LastBlock:=nil;
end;

destructor TAprReader.Destroy;
var b_iterate1, b_iterate2:p_Block;
    i_iterate1, i_iterate2:p_Block_Item;
begin
   b_iterate1:=FirstBlock;
   b_iterate2:=FirstBlock;
   while (b_iterate1 <> nil) do
   begin
      b_iterate2:=b_iterate1;
      b_iterate1:=b_iterate1^.next;
      i_iterate1:=b_iterate2^.Items;
      i_iterate2:=b_iterate2^.Items;
      while (i_iterate1 <> nil) do
      begin
         i_iterate2:=i_iterate1;
         i_iterate1:=i_iterate1^.next;
         dispose(i_iterate2);
      end;
      dispose(b_iterate2);
   end;
end;

procedure TAprReader.SetFileName(filename:string; var aShpReader:TShapeReader);
begin
   AprFilename:=filename;
   ShpReader:=aShpReader;
end;

procedure TAprReader.Read;
var Line:string;
    iterate:p_Block;
    IsEof:boolean;
begin
   // function is used to parse ArcView .apr file
   IsEof:=FALSE;
   {$I-}
   Assign(AprFile, AprFileName);
   Reset(AprFile);
   {$I+}
   repeat
      {$I-}
      readln(AprFile, Line);
      {$I+}
      // check if it�s a begin of a block
      if (copy(Line,1,1) = '(') then
      begin
         ReadBlock(Line);
      end;
      {$I-}
      if Eof(AprFile) then
         IsEof:=TRUE;
      {$I+}
   until IsEof=TRUE;
   {$I-}
   CloseFile(AprFile);
   {$I+}

   iterate:=FirstBlock;
   // now read the text items
   while (iterate <> nil) do
   begin
      if iterate^.Name = 'GText' then
         Read_GText(iterate); // evaluate text item
      iterate:=iterate^.next;
   end;
end;

procedure TAprReader.ReadBlock(Line:string);
var BlockType:string;
    dummy    :string;
    BlockNr  :integer;
    count    :integer;
    newBlock :p_Block;
begin
   // first check which type of block it is
   BlockType:=copy(Line,2,pos('.',Line)-2);
   // get the number of block
   dummy:=copy(Line,pos('.',Line)+1,length(Line));
   val(dummy, BlockNr, count);
   newBlock:=new(p_Block);
   newBlock^.Number:=BlockNr;
   newBlock^.Name:=BlockType;
   newBlock^.Items:=nil;

   Read_BlockItems(newBlock); // now the items of the block will be added

   InsertBlock(newBlock);
end;

function TAprReader.GetBlockByNumber(Nr:integer):p_Block;
var iterate:p_Block;
    found:boolean;
begin
   iterate:=FirstBlock;
   found:=FALSE;
   while ((iterate <> nil) and (found = FALSE)) do
   begin
      if (iterate^.Number = Nr) then
         Found:=TRUE
      else
         iterate:=iterate^.next;
   end;
   result:=iterate;
end;

// insert block to list
procedure TAprReader.InsertBlock(BlockRef:p_Block);
begin
   if FirstBlock = NIL then
   begin
      FirstBlock:=BlockRef;
      BlockRef^.next:=NIL;
      LastBlock:=BlockRef;
   end
   else
   begin
      LastBlock^.next:=BlockRef;
      BlockRef^.next:=NIL;
      LastBlock:=BlockRef;
   end;
end;

// procedure is used to read items of block
procedure TAprReader.Read_BlockItems(var BlockRef:p_Block);
var Line, Code, Info:string;
begin
   {$I-}
   repeat
      readln(AprFile, Line);
      if (Line <> ')') and (not Eof(AprFile)) then
      begin
         GetCodeAndInfo(Line, Code, Info);
         InsertBlockItem(BlockRef, Code, Info);
      end;
   until (Line = ')') or (Eof(AprFile)); // read till end of block
   {$I+}
end;

procedure TAprReader.InsertBlockItem(var BlockRef:p_Block; Code:string; Info:string);
var newentry,iterate:p_Block_item;
begin
   new(newentry);
   newentry^.Code:=Code;
   newentry^.Info:=Info;
   if BlockRef^.Items = nil then
   begin
      // insert at begin of list
      BlockRef^.Items:=newentry;
      BlockRef^.Items^.next:=NIL;
   end
   else
   begin
      iterate:=BlockRef^.Items;
      while (iterate^.next <> nil) do iterate:=iterate^.next;
      iterate^.next:=newentry;
      newentry^.next:=nil;
   end;
end;

procedure TAprReader.GetCodeAndInfo(Line:string; var Code:string; var Info:string);
begin
   // get Code of Line: Syntax: <CODE>: Info
   Line:=TrimLeft(Line); // skip leading spaces
   Code:=copy(Line,1,Pos(':',Line)-1);
   Info:=copy(Line,Pos(':',Line)+1,length(Line));
   Info:=TrimLeft(Info);
end;

procedure TAprReader.Read_TxSym(BlockRef:p_Block);
var ColorItem, FontItem, BgColorItem, OlColorItem:integer;
    Color:integer;
    Font:string;
    BgColor, OlColor:integer;
    iterate:p_Block_item;
    aBlock:p_Block;
    dummy:string;
    value:double;
    count:integer;
begin
   iterate:=BlockRef^.Items;
   while (iterate <> nil) do
   begin
      if iterate^.Code = 'Color'   then ColorItem:=strtoint(iterate^.Info);
      if iterate^.Code = 'Font'    then FontItem:=strtoint(iterate^.Info);
      if iterate^.Code = 'BgColor' then BgColorItem:=strtoint(iterate^.Info);
      if iterate^.Code = 'OlColor' then OlColorItem:=strtoint(iterate^.Info);
      if iterate^.Code = 'Size'    then
      begin
         dummy:=iterate^.Info;
         dummy:=StringReplace(dummy,',','.',[rfreplaceall]);
         val(dummy,value,count); if count <> 0 then value:=10;
         aText.Height:=value;
      end;
      iterate:=iterate^.next;
   end;
   // get the color
   aBlock:=GetBlockByNumber(ColorItem);
   Color:=Read_TClr(aBlock);
   // ignore the color
   // TextObj.SetColor(Color);

   // get the font
   aBlock:=GetBlockByNumber(FontItem);
   Font:=Read_NFont(aBlock);
   aText.Font:=Font;

   // get the BgColor
   aBlock:=GetBlockByNumber(BgColorItem);
   BgColor:=Read_TClr(aBlock);

   // get the OlColor
   aBlock:=GetBlockByNumber(OlColorItem);
   OlColor:=Read_TClr(aBlock);
end;

function TAprReader.Read_NFont(BlockRef:p_Block):string;
var Font:string;
    FamilyItem, NameItem, StyleItem, CommonFamilyItem:integer;
    Family, Name, Style, CommonFamily:string;
    iterate:p_Block_item;
    aBlock:p_Block;
begin
   iterate:=BlockRef^.Items;
   Font:='Courier New'; // default is Courier New
   while (iterate <> nil) do
   begin
      if iterate^.Code = 'Family'       then FamilyItem:=strtoint(iterate^.Info);
      if iterate^.Code = 'Name'         then NameItem:=strtoint(iterate^.Info);
      if iterate^.Code = 'Style'        then StyleItem:=strtoint(iterate^.Info);
      if iterate^.Code = 'CommonFamily' then CommonFamilyItem:=strtoint(iterate^.Info);
      iterate:=iterate^.next;
   end;
   // get the Familyitem
   aBlock:=GetBlockByNumber(FamilyItem);
   Family:=Read_AVStr(aBlock);

   // get the NameItem
   aBlock:=GetBlockByNumber(NameItem);
   Name:=Read_AVStr(aBlock);
   Font:=Name;

   // get the StyleItem
   aBlock:=GetBlockByNumber(StyleItem);
   Style:=Read_AVStr(aBlock);

   // get the CommonFamilyItem
   aBlock:=GetBlockByNumber(CommonFamilyItem);
   CommonFamily:=Read_AVStr(aBlock);

   result:=Font;
end;

function TAprReader.Read_AVStr(BlockRef:p_Block):string;
var AVStr:string;
    iterate:p_Block_item;
begin
   iterate:=BlockRef^.Items;
   while (iterate <> nil) do
   begin
      if (iterate^.Code = 'S') then AVStr:=iterate^.Info;
      iterate:=iterate^.next;
   end;
   result:=AVStr;
end;

function TAprReader.Hextoint(Value:string):longint;
var sign1,sign2:string;
    value1,value2:longint;
    count:integer;
begin
   Value:=UpperCase(Value);
   sign1:=copy(Value,1,1); sign1:=copy(Value,2,1);
   if sign1 = 'A' then value1:=10 else
   if sign1 = 'B' then value1:=11 else
   if sign1 = 'C' then value1:=12 else
   if sign1 = 'D' then value1:=13 else
   if sign1 = 'E' then value1:=14 else
   if sign1 = 'F' then value1:=15 else
      val(sign1,value1,count);
   value1:=value1 * 16;

   if sign2 = 'A' then value2:=10 else
   if sign2 = 'B' then value2:=11 else
   if sign2 = 'C' then value2:=12 else
   if sign2 = 'D' then value2:=13 else
   if sign2 = 'E' then value2:=14 else
   if sign2 = 'F' then value2:=15 else
      val(sign2,value2,count);

   Result:=value1 + value2;
end;

function TAprReader.Read_TClr(BlockRef:p_Block):integer;
var Color:integer;
    r,g,b:integer;
    iterate:p_Block_item;
begin
   iterate:=BlockRef^.Items;
   Color:=0;
   r:=0;
   g:=0;
   b:=0;
   while (iterate <> nil) do
   begin
      if (iterate^.Code = 'Red')   then r:=Hextoint(copy(iterate^.Info,3,2));
      if (iterate^.Code = 'Green') then g:=Hextoint(copy(iterate^.Info,3,2));
      if (iterate^.Code = 'Blue')  then b:=Hextoint(copy(iterate^.Info,3,2));
      iterate:=iterate^.next;
   end;
   Color:=(b * 65536)+(g * 256)+(r);
   result:=Color;
end;

procedure TAprReader.Read_RectD(BlockRef:p_Block; var left:double; var top:double; var right:double; var bottom:double);
var count:integer;
    iterate:p_Block_item;
begin
   iterate:=BlockRef^.Items;
   while (iterate <> nil) do
   begin
      if (iterate^.Code = 'Left')   then val(iterate^.Info, left, count);
      if (iterate^.Code = 'Right')  then val(iterate^.Info, right, count);
      if (iterate^.Code = 'Bottom') then val(iterate^.Info, bottom, count);
      if (iterate^.Code = 'Top')    then val(iterate^.Info, top, count);
      iterate:=iterate^.next;
   end;
end;

// procedure is used to read GText Block
procedure TAprReader.Read_GText(BlockRef:p_Block);
var iterate:p_Block_item;
    aBlock :p_Block;

    BoundsItem:integer;
    SymItem:integer;
    Left, Top, Right, Bottom:double;
begin
   aText:=TImpExpText.Create;
   ShpReader.AddObjectToLayer(aText);

   iterate:=BlockRef^.Items;
   while (iterate <> nil) do
   begin
      if (iterate^.Code = 'Text')   then aText.Text:=iterate^.Info;
      if (iterate^.Code = 'Bounds') then BoundsItem:=strtoint(iterate^.Info);
      if (iterate^.Code = 'Sym')    then SymItem:=strtoint(iterate^.Info);
      iterate:=iterate^.next;
   end;

   // get position
   aBlock:=GetBlockByNumber(BoundsItem);
   Read_RectD(aBlock,left, top, right, bottom);
   aText.X:=left;
   aText.Y:=top;


   // get sym
   aBlock:=GetBlockByNumber(SymItem);
   Read_TxSym(aBlock);
end;

procedure TAprReader.GetLayerNameAndProperties(Filename:string; var Layername:string; var LineType:integer; var LineColor:integer; var FillType:integer; var FillColor:integer);
var iterate:p_Block;
    ShpName:string;
    found:boolean;
begin
   if (FirstBlock = nil) then // no .apr file has been read
   begin
      LineType:=-1;
      LineColor:=-1;
      FillType:=-1;
      FillColor:=-1;
      Layername:=ExtractFilename(Filename);
      Layername:=copy(Layername,1,Pos('.',Layername)-1);
   end
   else  // get name of layer and layer priorities
   begin
      Filename:=Extractfilename(Filename);
      iterate:=FirstBlock;
      found:=false;
      while (iterate <> nil) and (found = FALSE) do
      begin
         if (iterate^.Name = 'FTheme') then
         begin
            Read_FTheme(iterate, ShpName, Layername, LineType, LineColor, FillType, FillColor);
            Filename:=UpperCase(Filename);
            ShpName:=UpperCase(ShpName);
            if (ShpName = Filename) then
               found:=true
         end;
         if not found then
            iterate:=iterate^.next;
      end;
      if (not found) then
      begin
         LineType:=-1;
         LineColor:=-1;
         FillType:=-1;
         FillColor:=-1;
         Layername:=ExtractFilename(Filename);
         Layername:=copy(Layername,1,Pos('.',Layername)-1);
      end;
   end;
end;

procedure TAprReader.Read_FTheme(Blockref:p_Block; var ShapeName:string; var LayerName:string; var LineType:integer; var LineColor:integer; var FillType:integer; var FillColor:integer);
var iterate:p_Block_item;
    SourceItem:integer;
    LegendItem:integer;
    count:integer;
    aBlock:p_Block;
begin
   iterate:=Blockref^.Items;
   while (iterate <> nil) do
   begin
      if (iterate^.Code = 'Name') then begin Layername:=iterate^.Info; delete(Layername,1,1); Layername:=copy(Layername,1,length(Layername)-1); end;
      if (iterate^.Code = 'Source') then val(iterate^.Info, SourceItem, count);
      if (iterate^.Code = 'Legend') then val(iterate^.Info, LegendItem, count);
      iterate:=iterate^.next;
   end;
   // get the SourceItem
   aBlock:=GetBlockByNumber(SourceItem);
   ShapeName:=Read_ShpSrc(aBlock);

   // get LegendItem
   aBlock:=GetBlockByNumber(LegendItem);
   Read_Legend(aBlock, LineType, LineColor, FillType, FillColor);
end;

procedure TAprReader.Read_Legend(BlockRef:p_Block; var LineType:integer; var LineColor:integer; var FillType:integer; var FillColor:integer);
var aBlock:p_Block;
    iterate:p_Block_item;
    SymbolsItem:integer;
    count:integer;
begin
   iterate:=BlockRef^.Items;
   while (iterate <> nil) do
   begin
      if (iterate^.Code = 'Symbols') then val(iterate^.Info, SymbolsItem, count);
      iterate:=iterate^.next;
   end;

   // get SymbolsItem
   aBlock:=GetBlockByNumber(SymbolsItem);
   Read_SymList(aBlock, LineType, LineColor, FillType, FillColor);
end;

procedure TAprReader.Read_SymList(BlockRef:p_Block; var LineType:integer; var LineColor:integer; var FillType:integer; var FillColor:integer);
var iterate:p_Block_item;
    ChildItem:integer;
    count:integer;
    aBlock:p_Block;
begin
   iterate:=BlockRef^.Items;
   while (iterate <> nil) do
   begin
      if (iterate^.Code = 'Child') then val(iterate^.Info, ChildItem, count);
      iterate:=iterate^.next;
   end;

   // get SymbolsItem
   aBlock:=GetBlockByNumber(ChildItem);
   if      (aBlock^.Name = 'BShSym') then Read_BShSym(aBlock, LineType, LineColor, FillType, FillColor)
   else if (aBlock^.Name = 'BMkSym') then Read_BMkSym(aBlock, LineType, LineColor, FillType, FillColor)
   else if (aBlock^.Name = 'BLnSym') then Read_BLnSym(aBlock, LineType, LineColor, FillType, FillColor);
end;

procedure TAprReader.Read_BLnSym(BlockRef:p_Block; var LineType:integer; var LineColor:integer; var FillType:integer; var FillColor:integer);
var iterate:p_Block_item;
    aBlock:p_Block;
    ColorItem:integer;
    count:integer;
begin
   iterate:=BlockRef^.Items;
   while (iterate <> nil) do
   begin
      if (iterate^.Code = 'Color') then val(iterate^.Info, ColorItem, count);
      iterate:=iterate^.next;
   end;
   // get the ColorItem
   aBlock:=GetBlockByNumber(ColorItem);
   LineColor:=Read_TClr(aBlock);
   LineType:=0;

   FillType:=-1;
   FillColor:=-1;
end;

procedure TAprReader.Read_BMkSym(BlockRef:p_Block; var LineType:integer; var LineColor:integer; var FillType:integer; var FillColor:integer);
var iterate:p_Block_item;
    aBlock:p_Block;
    ColorItem:integer;
    count:integer;
begin
   iterate:=BlockRef^.Items;
   while (iterate <> nil) do
   begin
      if (iterate^.Code = 'Color') then val(iterate^.Info, ColorItem, count);
      iterate:=iterate^.next;
   end;
   // get the ColorItem
   aBlock:=GetBlockByNumber(ColorItem);
   LineColor:=Read_TClr(aBlock);
   LineType:=0;

   FillType:=-1;
   FillColor:=-1;
end;

procedure TAprReader.Read_BShSym(BlockRef:p_Block; var LineType:integer; var LineColor:integer; var FillType:integer; var FillColor:integer);
var iterate:p_Block_item;
    aBlock:p_Block;
    ColorItem:integer;
    OutlineItem:integer;
    OutlineColorItem:integer;
    BgColorItem:integer;
    count:integer;
begin
   iterate:=BlockRef^.Items;
   while (iterate <> nil) do
   begin
      if (iterate^.Code = 'Color')        then val(iterate^.Info, ColorItem, count);
      if (iterate^.Code = 'Outline')      then val(iterate^.Info, OutLineItem, count);
      if (iterate^.Code = 'OutlineColor') then val(iterate^.Info, OutlineColorItem, count);
      if (iterate^.Code = 'BgColor')      then val(iterate^.Info, BgColorItem, count);
      iterate:=iterate^.next;
   end;
   // get the ColorItem
   aBlock:=GetBlockByNumber(ColorItem);
   FillColor:=Read_TClr(aBlock);
   FillType:=7;

   LineType:=-1;
   LineColor:=-1;
end;

function TAprReader.Read_ShpSrc(BlockRef:p_Block):string;
var retVal:string;
    iterate:p_Block_item;
    aBlock:p_Block;
    NameItem, count:integer;
begin
   retVal:='';
   iterate:=BlockRef^.Items;
   while (iterate <> nil) do
   begin
      if (iterate^.Code = 'Name') then val(iterate^.Info, NameItem, count);
      iterate:=iterate^.next;
   end;
   // get NameItem
   aBlock:=GetBlockByNumber(NameItem);
   retVal:=Read_SrcName(aBlock);

   result:=retVal;
end;

function TAprReader.Read_SrcName(BlockRef:p_Block):string;
var iterate:p_Block_item;
    retVal:string;
begin
   iterate:=BlockRef^.Items;
   while (iterate <> nil) do
   begin
      if (iterate^.Code = 'Name') then begin retVal:=iterate^.Info; delete(retVal,1,1); retVal:=copy(retVal,1,length(retVal)-1); end;
      iterate:=iterate^.next;
   end;
   result:=retVal;
end;


end.
