unit Maindlg;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Strfile, StdCtrls, Buttons, ExtCtrls, Gauges, ShapeReader, ImpExpObjects,
  SetShpfileDlg,Windows, AprReader, {ShpDkmManager,} ADODB, Db, OleCtrls,
  AXImpExpDbcXControl_TLB, AxGisPro_TLB, AXDLL_TLB, AXGisMan_TLB, WinOSInfo;

const      //registration modes
           NO_LICENCE        = 0;
           FULL_LICENCE      = 1;
           DEMO_LICENCE      = 2;

type
  TMainWnd = class(TForm)
    Label1: TLabel;
    CloseTimer: TTimer;
    Panel1: TPanel;
    Label2: TLabel;
    Prozessbalken: TGauge;
    StatusLB: TListBox;
    SaveReportBtn: TButton;
    OK: TButton;
    Cancel: TButton;
    SaveDialogRep: TSaveDialog;
    SetupWindowTimer: TTimer;
    AXImpExpDbc: TAXImpExpDbc;
    ADOTable: TADOTable;
    ADOConnection: TADOConnection;
    AxGisProjection: TAxGisProjection;
    AXGisObjMan: TAXGisObjMan;
    procedure FormCreate(Sender: TObject);
    procedure CloseTimerTimer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure CancelClick(Sender: TObject);
    procedure SaveReportBtnClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AXImpExpDbcDialogSetuped(Sender: TObject);
    procedure AxGisProjectionWarning(Sender: TObject;
      const Info: WideString);
    procedure AxGisProjectionError(Sender: TObject;
      const Info: WideString);
    procedure AXImpExpDbcRequestImpLayerIndex(Sender: TObject;
      const Layername: WideString; var LayerIndex: Integer);
    procedure AXImpExpDbcRequestImpProjectName(Sender: TObject;
      var ProjectName: WideString);
    procedure AXImpExpDbcRequestImpProjectGUIDs(Sender: TObject;
      var StartGUID, CurrentGUID: WideString);
    procedure AXImpExpDbcWarning(Sender: TObject; const Info: WideString);
  private
    { Private-Deklarationen }
    AprReaderObj:TAprReader;
    ObjectKeeper:TImpExpObjectKeeper;
    //DkmMgr      :TShpDkmManager;
    ShpReader  :TShapeReader;
    Pass       :integer;
    SHPDateiname:string;
    SHXDateiname:string;
    DBFDateiname:string;
    Bytes2work  :longint;
    Bytesworked :longint;
    Aktiv       :boolean;
    Abort       :boolean;
    FirstActivate:boolean;
    ObjectCount :longint;
    FileWasOK   :boolean;

    DX_Layer      :ILayer;

    FirstShpFile:boolean;

    procedure RunPass(AktPass:integer);
    procedure ExecutePass;
    procedure DisplayDemoWarning;
    procedure StartImport;

    // procedure to handle DX dispatch interface
    procedure DX_CreateLayer(Layer:string);
    function  DX_CheckLayerExists(Layer:string):boolean;

    procedure DX_SetObjectStyle(const aObject:TImpExpBaseObj; var DX_Object:IBase);
    function Handle_Dkm_Duplicates(var DX_Object:IBase; const aObject:TImpExpBaseObj; const LayerName:string):integer;
  public
    { Public-Deklarationen }
    App         :Variant;
    DestDocument:IDocument;

    RegMode     :integer;
    LngCode     :string;
    ExitNormal  :boolean;
    GisHandle   :integer;

    Layername   :string;
    StrFileObj :ClassStrfile;

    procedure Create_Layer(const aLayer:TImpExpLayer);
    function Create_Pixel(const aPixel:TImpExpPixel):integer;
    function Create_Poly(const aPolyline:TImpExpPoly):integer;
    function Create_CPoly(const aPolygon:TImpExpCPoly):integer;
    function Create_Text(const aText:TImpExpText):integer;

    procedure SetProjectionSettings;
    procedure CheckProjectionSettings;
    procedure CreateStringFile;
  end;

var
  MainWnd: TMainWnd;

implementation

uses comobj;

{$R *.DFM}

procedure TMainWnd.CreateStringFile;
begin
   StrFileObj:=ClassStrfile.Create;
   AprReaderObj:=TAprReader.Create;
   ObjectKeeper:=TImpExpObjectKeeper.Create;
   StrFileObj.CreateList(OSInfo.WingisDir,LngCode,'SHPIMP');
   Self.Caption:=StrFileObj.Assign(1);          { ARCVIEW SHP Import 4.0 }
   Label2.Caption:=StrFileObj.Assign(2);        { Processmessages: }
   SaveReportBtn.Caption:=StrFileObj.Assign(3); { save report }
   Ok.Caption:=StrFileObj.Assign(4);            { Ok }
   Cancel.Caption:=StrFileObj.Assign(5);        { Cancel }
   Self.Update;
end;

procedure TMainWnd.FormCreate(Sender: TObject);
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   FirstActivate:=true;
   FirstShpFile:=true;
   Self.Height:=1;
   Self.Width:=1;

   Aktiv:=false;
   FirstActivate:=true;
   ObjectCount:=0;

   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

procedure TMainWnd.CheckProjectionSettings;
var different:boolean;
begin
   different:=false;
   // function is used to check if the settings in the
   if (AxGisProjection.TargetCoordinateSystem <> DestDocument.Projection) then
      different:=TRUE;

   if (AxGisProjection.TargetXOffset <> DestDocument.ProjectionXOffset) then
      different:=TRUE;

   if (AxGisProjection.TargetYOffset <> DestDocument.ProjectionYOffset) then
      different:=TRUE;

   if (AxGisProjection.TargetScale <> DestDocument.ProjectionScale) then
      different:=TRUE;

   // check if the projections are different
   if DestDocument.ProjectProjSettings <> AxGisProjection.TargetProjSettings then
      different:=TRUE;

   if DestDocument.ProjectProjection <> AxGisProjection.TargetProjection then
      different:=TRUE;

   if DestDocument.Projectdate <> AxGisProjection.TargetDate then
      different:=TRUE;

   if different then
   begin
      Showmessage(StrfileObj.Assign(44)); // Warning: Projection does not fit to project projection
      AxGisProjection.SetupByDialog;
   end;
   // the used output coordinate-system is equal to the
   // target-coordinate-system
   AxGisProjection.UsedInputCoordSystem:=AxGisProjection.SourceCoordinateSystem;
end;

procedure TMainWnd.SetProjectionSettings;
var aApp:OleVariant;
    aDoc:OleVariant;
begin
   // procedure is used to set the projection settings
   // of the current project
   AxGisProjection.WorkingPath:=OSInfo.WingisDir;
   AxGisProjection.LngCode:=LngCode;

   AXGisObjMan.WorkingDir:=OSInfo.WingisDir;
   AXGisObjMan.LngCode:=LngCode;
   aApp:=App; AxGisObjMan.SetApp(aApp);
   aDoc:=DestDocument; AxGisObjMan.SetDocument(aDoc);

   AxImpExpDbc.WorkingDir:=OSInfo.WingisDir;
   AxImpExpDbc.LngCode:=LngCode;
   AxImpExpDbc.ShowMode:=2; // Shw_Target
   SetShpFileWnd.DestDbLabel.Caption:=AxImpExpDbc.ImpDbInfo;

   AxGisProjection.SourceCoordinateSystem:=cs_Carthesic; // shp-data normaly is carthesic
   AxGisProjection.TargetCoordinateSystem:=DestDocument.Projection;
   AxGisProjection.SourceProjSettings:=DestDocument.ProjectProjSettings;
   AxGisProjection.TargetProjSettings:=DestDocument.ProjectProjSettings;
   AxGisProjection.SourceProjection:=DestDocument.ProjectProjection;
   AxGisProjection.TargetProjection:=DestDocument.ProjectProjection;
   AxGisProjection.SourceDate:=DestDocument.Projectdate;
   AxGisProjection.TargetDate:=DestDocument.Projectdate;
   AxGisProjection.SourceXOffset:=DestDocument.ProjectionXOffset;
   AxGisProjection.TargetXOffset:=DestDocument.ProjectionXOffset;
   AxGisProjection.SourceYOffset:=DestDocument.ProjectionYOffset;
   AxGisProjection.TargetYOffset:=DestDocument.ProjectionYOffset;
   AxGisProjection.SourceScale:=DestDocument.ProjectionScale;
   AxGisProjection.TargetScale:=DestDocument.ProjectionScale;
   // if source and target is .amp - file
   // the used input coordinates and used output coordinates
   // for the projection calculation are in carthesic
   AxGisProjection.UsedInputCoordSystem:=AxGisProjection.SourceCoordinateSystem;
   AxGisProjection.UsedOutputCoordSystem:=cs_Carthesic;
end;

{*******************************************************************************
* PROZEDUR : RunPass                                                           *
********************************************************************************
* Prozedur leitet den jeweiligen Konvertierungs-Pass ein.                      *
*                                                                              *
* PARAMETER: Pass -> Gibt den Konvertierungs-Pass an.                          *
*                                                                              *
********************************************************************************}
procedure TMainWnd.RunPass(AktPass:integer);
begin
   if AktPass = 1 then // open files and read header
   begin
      // do mask settings
      if not FirstShpFile then
      begin
         if FileWasOK then
         begin
            StatusLB.Items.Add(StrfileObj.Assign(36)); { File correct imported! }
            StatusLB.Items.Add('--------------------------------------');
            StatusLB.ItemIndex := StatusLB.Items.Count-1;
            StatusLB.Update;
         end;
      end;

      if not SetShpFileWnd.GetFilename(ShpDateiname) then
      begin
         aktiv:=false;
         Abort:=true;
         Cancel.Enabled:=false;
         Ok.Enabled:=true;
         SaveReportBtn.Enabled:=true;
         exit;
      end;
      ShpReader:=TShapeReader.Create;
      ShpReader.SetObjectKeeper(ObjectKeeper); // apply object keeper to Shp-Reader
      ShxDateiname:=ShpDateiname;ShxDateiname:=Changefileext(ShxDateiname,'.shx');
      DbfDateiname:=ShpDateiname;DbfDateiname:=Changefileext(DbfDateiname,'.dbf');
      if Fileexists(ShpDateiname) then StatusLb.Items.Add(StrfileObj.Assign(30) + Extractfilename(ShpDateiname));
      if Fileexists(ShxDateiname) then StatusLb.Items.Add(StrfileObj.Assign(31) + Extractfilename(ShxDateiname));
      if Fileexists(DbfDateiname) then StatusLb.Items.Add(StrfileObj.Assign(32) + Extractfilename(DbfDateiname));

      // display database information
      StatusLB.items.add(AxImpExpDbc.ImpDbInfo);
      Aktiv:=true;

      StatusLB.Items.Add(StrfileObj.Assign(33)); // read header data
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      if FirstShpFile then
      begin
         CheckProjectionSettings;
         {
         if SetShpFileWnd.DkmCb.Checked then
         begin
            DkmMgr:=TShpDkmManager.Create;
            DkmMgr.ReadSetup(StartDir+'DkmComb.ini');
         end;
         }
         FirstShpFile:=false;
      end;
      {
      if DkmMgr <> nil then
         ShpReader.SetDkmManager(DkmMgr);
      }

      // check if Apr-File has to be read
      if SetShpFileWnd.AprCB.checked then
      begin
         AprReaderObj.SetFileName(SetShpFileWnd.GetAprFileName, ShpReader);
         StatusLB.items.add(StrfileObj.Assign(47) + SetShpFileWnd.GetAprFileName);
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         // read project file
         AprReaderObj.Read;
         SetShpFileWnd.AprCB.Checked:=false; // avoid importing .apr file at each file
      end;

      // evaluate number of bytes that have to be worked
      Bytes2Work:=ShpReader.OpenShpFile(ShpDateiname); Bytesworked:=0;
      if not ShpReader.OpenDBFFile(DbfDateiname) then
      begin
         // Show warning that there had been a problem at open of database
         StatusLB.items.add(StrfileObj.Assign(50)+ExtractFilename(DbfDateiname)); // Error at open of database
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
      end;
      Pass:=1;
      ExecutePass; exit;
   end;

   if AktPass = 2 then // read object data
   begin
      // do window settings
      StatusLB.Items.Add(StrfileObj.Assign(34)); // read object data
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      Pass:=2;
      ExecutePass;
      // close the shp dbf-database (if necessary)
      if AdoTable.Active then
      begin
         ADOTable.First;
         ADOTable.Close;
         if ADOConnection.Connected then
            ADOConnection.Connected:=FALSE;
      end;
      exit;
   end;

   if AktPass = 3 then // end of import
   begin
      ShpReader.CloseallFiles;
      ShpReader.Destroy;
      RunPass(1); // convert next file
      exit;
   end;
end;

procedure TMainWnd.ExecutePass;
var Prozent:integer;
begin
   if Pass = 1 then // read header of shape
   begin
      FileWasOK:=TRUE;
      if Bytes2Work > 4 then // check if it could be a shape-file
         Bytesworked:=Bytesworked + ShpReader.ReadHeader;
      if (Bytesworked <> 100) then
      begin
         // file is not a Shape-File
         StatusLB.Items.Add(StrfileObj.Assign(48)); // Warning: Not a Shape file.
         StatusLB.Items.Add('--------------------------------------');
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         FileWasOK:=FALSE;
         RunPass(3); exit;
      end;

      { Anzeigen des Prozessstatus }
      if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
      ProzessBalken.Progress :=Prozent;
      ProzessBalken.Update;
      if Bytesworked < Bytes2work then
      begin
         RunPass(2); exit;
      end
      else
      begin
         // it is an empty shape
         StatusLB.Items.Add(StrfileObj.Assign(49)); // Warning: Shape contains no data!
         StatusLB.Items.Add('--------------------------------------');
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         FileWasOk:=FALSE;
         RunPass(3); exit;
      end;
   end;
   if Pass = 2 then // read data from file and import it to Gis
   begin
      repeat
         Bytesworked:=Bytesworked + ShpReader.ReadElement;
         { Anzeigen des Prozessstatus }
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then RunPass(3);
      if Abort = true then exit;
      exit;
   end;
end;

// create pixel in document
function TMainWnd.Create_Pixel(const aPixel:TImpExpPixel):integer;
var AIndex:integer;
    X,Y:double;
    DX_Pixel:IPixel;
    DX_Point:IPoint;
    DX_Base :IBase;
begin
   //check licence
   result:=-1;
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;
   //do the projection
   X:=aPixel.X; Y:=aPixel.Y;
   AxGisProjection.Calculate(X, Y);
   DX_Pixel:=DestDocument.CreatePixel;
   DX_Point:=DestDocument.CreatePoint;
   DX_Point.X:=round(X*100);
   DX_Point.Y:=round(Y*100);
   DX_Pixel.Position:=DX_Point;

   DX_Base:=DX_Pixel;
   DX_SetObjectStyle(aPixel, DX_Base);

   DX_Layer.InsertObject(DX_Pixel);
   AIndex:=DX_Pixel.Index;
   result:=AIndex;
end;

procedure TMainWnd.DX_SetObjectStyle(const aObject:TImpExpBaseObj; var DX_Object:IBase);
var aPixel   :IPixel;
    aText    :IText;
    aPoly    :IPolyline;
    aCPoly   :IPolygon;
    aObjectStyle:IObjectStyle;
begin
   if not ((aObject.ObjectStyle.HasLineType) or (aObject.ObjectStyle.HasFillType)) then
      exit; // nothing to do -> return

        if aObject.ObjectType = impexp_Pixel then begin aPixel:=DX_Object as IPixel; aPixel.CreateObjectStyle; aObjectStyle:=aPixel.ObjectStyle; end
   else if aObject.ObjectType = impexp_Text  then begin aText:=DX_Object as IText; aText.CreateObjectStyle; aObjectStyle:=aText.ObjectStyle; end
   else if aObject.ObjectType = impexp_Poly  then begin aPoly:=DX_Object as IPolyline; aPoly.CreateObjectStyle; aObjectStyle:=aPoly.ObjectStyle; end
   else if aObject.ObjectType = impexp_CPoly then begin aCPoly:=DX_Object as IPolygon; aCPoly.CreateObjectStyle; aObjectStyle:=aCPoly.ObjectStyle; end
   else
      exit; // not supported object type

   // first apply defaults from layer
   aObjectStyle.LineStyle.Style:=DX_Layer.LineStyle.Style;
   aObjectStyle.LineStyle.Color:=DX_Layer.LineStyle.Color;
   aObjectStyle.LineStyle.Width:=DX_Layer.LineStyle.Width;
   aObjectStyle.FillStyle.Pattern:=DX_Layer.FillStyle.Pattern;
   aObjectStyle.FillStyle.ForeColor:=DX_Layer.FillStyle.ForeColor;
   aObjectStyle.FillStyle.BackColor:=DX_Layer.FillStyle.BackColor;

   if aObject.ObjectStyle.LineType <> -1 then aObjectStyle.LineStyle.Style:=aObject.ObjectStyle.LineType;
   if aObject.ObjectStyle.LineColor <> -1 then aObjectStyle.LineStyle.Color:=aObject.ObjectStyle.LineColor;
   if aObject.ObjectStyle.LineWidth <> -1 then aObjectStyle.LineStyle.Width:=aObject.ObjectStyle.LineWidth;

   if aObject.ObjectStyle.FillType <> -1 then aObjectStyle.FillStyle.Pattern:=aObject.ObjectStyle.FillType;
   if aObject.ObjectStyle.FillColor <> -1 then aObjectStyle.FillStyle.ForeColor:=aObject.ObjectStyle.FillColor;
end;

function TMainWnd.Handle_Dkm_Duplicates(var DX_Object:IBase; const aObject:TImpExpBaseObj; const LayerName:string):integer;
var CheckLayer:ILayer;
    Invisible_Layer:ILayer;
    CheckId:integer;
    DX_Base:IBase;
    DX_ObjectStyle_new:IObjectStyle;
    DX_ObjectStyle_check:IObjectStyle;
    aObjectStyleDisp:IDispatch;
    CheckLayers:TStrings;
    CopyIndex:integer;
    aOleVariant:OleVariant;
    MoveType:integer;
    AIndex:integer;
    i:integer;
begin
   AIndex:=DX_Object.Index;
   CheckLayers:=TStringList.Create;
   for i:=0 to DestDocument.Layers.Count-1 do
   begin
      CheckLayer:=DestDocument.Layers.Items[i];
      if CheckLayer.LayerName = LayerName then
         continue; // do no check on own layer
      CheckLayers.Add(CheckLayer.LayerName);
   end;

   DX_ObjectStyle_new:=nil;
   aObjectStyleDisp:=DX_Object.ObjectStyle;
   if (aObjectStyleDisp <> nil) then
      DX_ObjectStyle_new:=DX_Object.ObjectStyle;

   // evaluate if own object has to be moved to invisible layer or if other object has to be moved
   for i:=0 to CheckLayers.Count-1 do
   begin
      CheckLayer:=DestDocument.Layers.GetLayerByName(CheckLayers[i]);
      if not CheckLayer.Visible then
         continue;
      CheckId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(DX_Object.Index, CheckLayer.LayerName);
      if CheckId > 0 then
      begin
         DX_Base:=DestDocument.GetObjectByIndex(CheckId);

         DX_ObjectStyle_check:=nil;
         aObjectStyleDisp:=DX_Base.ObjectStyle;
         if (aObjectStyleDisp <> nil) then
            DX_ObjectStyle_check:=DX_Base.ObjectStyle;


         if (DX_ObjectStyle_check = nil) and (DX_ObjectStyle_new = nil) then
            continue;

         MoveType:=0; // 1-> Move new object to invisible Layer, 2 -> move old object to invsible layer

         if DX_ObjectStyle_new = nil then
         begin
            if DX_ObjectStyle_check.LineStyle.Style > 0 then
               MoveType:=1;
         end
         else if DX_ObjectStyle_check = nil then
         begin
            if DX_ObjectStyle_new.LineStyle.Style > 0 then
               MoveType:=2;
         end
         else
         begin
            if DX_ObjectStyle_new.LineStyle.Style <> DX_ObjectStyle_check.LineStyle.Style then
            begin
               if DX_ObjectStyle_new.LineStyle.Style = 0 then
                  MoveType:=1
               else if DX_ObjectStyle_check.LineStyle.Style = 0 then
                  MoveType:=2;
            end;
         end;

         if MoveType = 1 then
         begin
            // New object has to be moved to layer Invisible_<layername>
            if DX_CheckLayerExists('Invisible_'+LayerName) then
               Invisible_Layer:=DestDocument.Layers.GetLayerByName('Invisible_'+LayerName)
            else
            begin
               Invisible_Layer:=DestDocument.Layers.InsertLayerByName('Invisible_'+LayerName,True);
               Invisible_Layer.Visible:=false;
            end;

            aOleVariant:=DX_Object;
            CopyIndex:=AxGisObjMan.CopyObjectToLayer(aOleVariant, 'Invisible_'+LayerName);
            DestDocument.DeleteObjectByIndex(AIndex);
            AIndex:=CopyIndex;
            aObject.TagText:='Invisible_'+LayerName;
         end
         else if MoveType = 2 then
         begin
            // Check object has to be moved to layer Invisible_<layername>
            if DX_CheckLayerExists('Invisible_'+CheckLayers[i]) then
               Invisible_Layer:=DestDocument.Layers.GetLayerByName('Invisible_'+CheckLayers[i])
            else
            begin
               Invisible_Layer:=DestDocument.Layers.InsertLayerByName('Invisible_'+CheckLayers[i],True);
               Invisible_Layer.Visible:=false;
            end;
            aOleVariant:=DX_Base;
            AxGisObjMan.CopyObjectToLayer(aOleVariant, 'Invisible_'+CheckLayers[i]);
            DestDocument.DeleteObjectByIndex(DX_Base.Index);
         end;
      end;
   end;
   CheckLayers.Destroy;
   result:=AIndex;
end;

function TMainWnd.Create_Poly(const aPolyline:TImpExpPoly):integer;
var DX_PolyLine:IPolyline;
    DX_Point:IPoint;
    DX_Base:IBase;
    i:integer;
    X, Y:double;
    aCorner:p_ImpExpCorner;
    AIndex:integer;
begin
   //check licence
   result:=-1;
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;
   // create Polygon
   DX_Polyline:=DestDocument.CreatePolyline;
   // add corners to polyline
   for i:=0 to aPolyline.NumCorners-1 do
   begin
      aCorner:=aPolyline.Corners[i];
      X:=aCorner^.X; Y:=aCorner^.Y;
      //do the projection
      AxGisProjection.Calculate(X,Y);
      DX_Point:=DestDocument.CreatePoint;
      DX_Point.X:=round(X*100);
      DX_Point.Y:=round(Y*100);
      DX_PolyLine.InsertPoint(DX_Point);
   end;

   DX_Base:=DX_Polyline;
   DX_SetObjectStyle(aPolyline, DX_Base);

   DX_Layer.InsertObject(DX_Polyline);

   AIndex:=DX_Polyline.Index;
   {
   if DkmMgr <> nil then
   begin
      DX_Base:=DX_Polyline;
      AIndex:=Handle_Dkm_Duplicates(DX_Base, aPolyline, DX_Layer.LayerName);
   end;
   }
   result:=AIndex;
end;

function TMainWnd.Create_CPoly(const aPolygon:TImpExpCPoly):integer;
var DX_IslandList :IList;
    DX_Polygon    :IPolygon;
    DX_Point      :IPoint;
    DX_IslandPoly :IPolygon;
    DX_Base       :IBase;
    X,Y           :double;
    StartIndex    :integer;
    SPoints       :array of p_ImpExpCorner;
    NumSPoints    :integer;
    i,AIndex,j,k  :integer;
    aCorner       :p_ImpExpCorner;
    aIsland       :TImpExpCPoly;
begin
   //check licence
   result:=-1;
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;


   if aPolygon.NumIslands > 0 then
      DX_IslandList:=DestDocument.CreateList;

   // first create base polygon (we have to take care that no island part may have same startingpoint than another
   DX_Polygon:=DestDocument.CreatePolygon;
   StartIndex:=0; // is always 0 for base-polygon
   NumSPoints:=0;
   for i:=StartIndex to aPolygon.NumCorners-1 do
   begin
      aCorner:=aPolygon.Corners[i];
      if (i = StartIndex) and (aPolygon.NumIslands > 0) then
      begin
         // insert starting point to list
         NumSPoints:=NumSPoints+1;
         SetLength(SPoints,NumSPoints);
         SPoints[NumSPoints-1]:=aCorner;
      end;

      //do the projection
      X:=aCorner^.X; Y:=aCorner^.Y;
      AxGisProjection.Calculate(X,Y);
      DX_Point:=DestDocument.CreatePoint;
      DX_Point.X:=round(X*100);
      DX_Point.Y:=round(Y*100);
      DX_Polygon.InsertPoint(DX_Point);
   end;
   if aPolygon.NumIslands = 0 then
   begin
      DX_Polygon.ClosePoly;

      DX_Base:=DX_Polygon;
      DX_SetObjectStyle(aPolygon, DX_Base);

      DX_Layer.InsertObject(DX_Polygon);
      AIndex:=DX_Polygon.Index;
   end
   else
   begin
      // close Polygon and add to island list
      DX_Polygon.ClosePoly;
      DX_IslandList.Add(DX_Polygon);

      // now add other Islands
      for i:=0 to aPolygon.NumIslands-1 do
      begin
         aIsland:=aPolygon.Islands[i] as TImpExpCPoly;

         // now evaluate StartIndex
         StartIndex:=-1;
         for j:=0 to aIsland.NumCorners-1 do
         begin
            aCorner:=aIsland.Corners[j];
            // now check if this corner is already in the Starting Points
            StartIndex:=-2;
            for k:=0 to NumSPoints-1 do
            begin
               if (SPoints[k]^.X = aCorner^.X) and (SPoints[k]^.Y = aCorner^.Y) then
               begin
                  StartIndex:=-1;
                  break;
               end;
            end;
            if StartIndex = -2 then // corner is not yet in Starting Corners
            begin
               // -> we got a new starting-corner -> add it to list
               NumSPoints:=NumSPoints+1;
               SetLength(SPoints,NumSPoints);
               SPoints[NumSPoints-1]:=aCorner;
               StartIndex:=j;
               break;
            end;
         end;
         if StartIndex < 0 then // we could not add Island due to no StartIndex could be found
            continue;

         DX_Polygon:=DestDocument.CreatePolygon;

         // add Corners from Start-Index
         for j:=StartIndex to aIsland.NumCorners-1 do
         begin
            aCorner:=aIsland.Corners[j];
            //do the projection
            X:=aCorner^.X; Y:=aCorner^.Y;
            AxGisProjection.Calculate(X,Y);
            DX_Point:=DestDocument.CreatePoint;
            DX_Point.X:=round(X*100);
            DX_Point.Y:=round(Y*100);
            DX_Polygon.InsertPoint(DX_Point);
         end;
         if StartIndex > 0 then // add corners at begin (due to StartIndex was not 0)
         begin
            for j:=0 to StartIndex-1 do
            begin
               aCorner:=aIsland.Corners[j];
               //do the projection
               X:=aCorner^.X; Y:=aCorner^.Y;
               AxGisProjection.Calculate(X,Y);
               DX_Point:=DestDocument.CreatePoint;
               DX_Point.X:=round(X*100);
               DX_Point.Y:=round(Y*100);
               DX_Polygon.InsertPoint(DX_Point);
            end;
         end;
         DX_Polygon.ClosePoly;
         DX_IslandList.Add(DX_Polygon);
      end;

      DX_IslandPoly:=DestDocument.CreateIslandArea(DX_IslandList);

      DX_Base:=DX_IslandPoly;
      DX_SetObjectStyle(aPolygon, DX_Base);

      DX_Layer.InsertObject(DX_IslandPoly);
      AIndex:=DX_IslandPoly.Index;
      SetLength(SPoints,0);
   end;
   result:=AIndex;
end;

function TMainWnd.Create_Text(const aText:TImpExpText):integer;
var DX_Text:IText;
    DX_Base:IBase;
    fontexists:boolean;
    i:integer;
    X,Y:double;
    dummy:double;
    AIndex:integer;
begin
   //check licence
   result:=-1;
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;

   DX_Text:=DestDocument.CreateText;

   // check if font is installed
   fontexists:=FALSE;
   for i:=0 to Screen.Fonts.Count-1 do
   begin
      if Screen.Fonts[i] = aText.Font then
      begin
         fontexists:=TRUE;
         break;
      end;
   end;

   if not fontexists then
   begin
      aText.Font:='Courier New'; //set default font
   end;

   DX_Text.FontStyle:=64; // text is transparent
   DX_Text.Fontname:=aText.Font;

   X:=aText.X; Y:=aText.Y;
   AxGisProjection.Calculate(X, Y);

   DX_Text.Position.X:=round(X*100);
   DX_Text.Position.Y:=round(Y*100);

   DX_Text.Angle:=0; // Text has no angle
   DX_Text.Text:=aText.Text;
   DX_Text.Align:=aText.Align;

   // the height can be scaled
   dummy:=aText.Height;
   dummy:=dummy / AxGisProjection.SourceScale;
   dummy:=dummy * AxGisProjection.TargetScale;
   DX_Text.FontHeight:=round(dummy);

   DX_Base:=DX_Text;
   DX_SetObjectStyle(aText, DX_Base);

   DX_Layer.Insertobject(DX_Text);
   AIndex:=DX_Text.Index;
   result:=AIndex;
end;

procedure TMainWnd.Create_Layer(const aLayer:TImpExpLayer);
var Layername:string;
    LineStyle, LineColor, FillStyle, FillColor:integer;
    i:integer;
    aColDef  :p_ImpExpDbColumn;
begin
   AprReaderObj.GetLayerNameAndProperties(ShpDateiname, Layername, LineStyle, LineColor, FillStyle, FillColor);
   if (not DX_CheckLayerExists(aLayer.Name)) then
   begin
      // create new layer and set properites
      DX_CreateLayer(aLayer.Name);
      if (LineStyle <> -1) and (LineColor <> -1) then
      begin
         DX_Layer.Linestyle.Style:=LineStyle;
         DX_Layer.Linestyle.Color:=LineColor;
         DX_Layer.Linestyle.FillColor:=LineColor;
      end;
      if (FillStyle <> -1) and (FillColor <> -1) then
      begin
         DX_Layer.Fillstyle.Pattern:=FillStyle;
         DX_Layer.Fillstyle.ForeColor:=FillColor;
      end;
   end;
   // create database
   if (aLayer.NumColumnDefs > 0) then
   begin
      AXImpExpDbc.CheckAndCreateImpTable(aLayer.Name);
      AXImpExpDbc.AddImpIdColumn(aLayer.Name);
      for i:=0 to aLayer.NumColumnDefs-1 do
      begin
         aColDef:=aLayer.ColumnDefs[i];
         AXImpExpDbc.AddImpColumn(aLayer.Name, aColDef^.ColName, aColDef^.ColType,aColDef^.ColLength);
      end;
      AXImpExpDbc.CreateImpTable(aLayer.Name);
      AXImpExpDbc.OpenImpTable(aLayer.Name);
   end;
end;

procedure TMainWnd.DX_CreateLayer(Layer:string);
begin
   DX_Layer:=DestDocument.Layers.InsertLayerByName(Layer,True);
end;

function TMainWnd.DX_CheckLayerExists(Layer:string):boolean;
var dispatch:IDispatch;
    retVal:boolean;
begin
   dispatch:=DestDocument.Layers.GetLayerByName(Layer);
   if dispatch <> nil then retVal:=true else retVal:=false;
   if (retVal) then
      DX_Layer:=DestDocument.Layers.GetLayerByName(Layer);
   result:=retVal;
end;

procedure TMainWnd.DisplayDemoWarning;
begin
   StatusLB.Items.Add(StrfileObj.Assign(29)); { 100 Objects created - following objects will be skiped. }
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;
   // also the database information has to be ignored
   AXImpExpDbc.ImpDbMode:=0; // Db_None
end;

procedure TMainWnd.CloseTimerTimer(Sender: TObject);
begin
   CloseTimer.Enabled:=false;
   MainWnd.Close;
   SetShpFileWnd.Close;
   DestDocument.FinishImpExpRoutine(ExitNormal,'IMPORTMODULE');
end;

procedure TMainWnd.StartImport;
begin
   if ExitNormal then
   begin
      RunPass(1);
   end;
end;

procedure TMainWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TMainWnd.CancelClick(Sender: TObject);
begin
   if aktiv = true then
   begin
      StatusLB.Items.Add(StrfileObj.Assign(37)); { Warning: Import canceled! }
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      ShpReader.CloseallFiles;
      aktiv:=false;
      Abort:=true;
      ShpReader.Destroy;
      Cancel.Enabled:=false;
      Ok.Enabled:=true;
      SaveReportBtn.Enabled:=true;
   end;
end;

procedure TMainWnd.SaveReportBtnClick(Sender: TObject);
var Datei:Textfile;
    Zeile:string;
    i:integer;
begin
   if SaveDialogRep.Execute then
   begin
      {$I-}
      Assignfile(Datei,SaveDialogRep.Filename);
      Rewrite(Datei);
      for i:=0 to StatusLB.Items.Count-1 do
      begin
         Zeile:=StatusLB.items[i];
         Writeln(Datei,Zeile);
      end;
      Closefile(Datei);
      {$I+}
   end;
end;

procedure TMainWnd.OKClick(Sender: TObject);
begin
   closetimer.Enabled:=true;
end;

procedure TMainWnd.FormDestroy(Sender: TObject);
begin
   StrFileObj.Destroy;
   AprReaderObj.Destroy;
   ObjectKeeper.Destroy;
   {
   if DkmMgr <> nil then
      DkmMgr.Destroy;
   }
end;

procedure TMainWnd.SetupWindowTimerTimer(Sender: TObject);
var IdbProcessmask:integer;
begin
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,SetShpFileWnd.winleft,SetShpFileWnd.wintop,SetShpFileWnd.winwidth, SetShpFileWnd.winheight,SWP_SHOWWINDOW);
   MainWnd.Height:=SetShpFileWnd.winheight;
   MainWnd.Width:=SetShpFileWnd.winwidth;
   Self.Repaint;
   SetupWindowTimer.Enabled:=false;
   IdbProcessmask:=DestDocument.IdbProcessMask;
   DestDocument.IdbProcessMask:=0;
   StartImport;
   DestDocument.IdbProcessMask:=IdbProcessMask;
end;

procedure TMainWnd.FormShow(Sender: TObject);
begin
   AXImpExpDbc.Visible:=FALSE;
   AxGisProjection.Visible:=FALSE;
   AxGisObjMan.Visible:=FALSE;
end;

procedure TMainWnd.AXImpExpDbcDialogSetuped(Sender: TObject);
begin
   SetShpFileWnd.DestDbLabel.Caption:=AxImpExpDbc.ImpDbInfo;
   SetShpFileWnd.PageControl.Enabled:=TRUE;
   SetShpFileWnd.GeoBtn.Enabled:=TRUE;
   SetShpFileWnd.DbBtn.Enabled:=TRUE;
   SetShpFileWnd.OkBtn.Enabled:=SetShpFileWnd.OkBtnMode;
   SetShpFileWnd.CancelBtn.Enabled:=TRUE;
end;

procedure TMainWnd.AxGisProjectionWarning(Sender: TObject;
  const Info: WideString);
begin
   if not Aktiv then
   begin
      Showmessage(Info);
   end
   else
   begin
      StatusLB.Items.Add(Info);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
   end;
end;

procedure TMainWnd.AxGisProjectionError(Sender: TObject;
  const Info: WideString);
begin
   if not Aktiv then
   begin
      Showmessage(Info);
   end
   else
   begin
      StatusLB.Items.Add(Info);
      StatusLB.Items.Add(StrfileObj.Assign(37)); { Warning: Import canceled! }
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      ShpReader.CloseallFiles;
      aktiv:=false;
      Abort:=true;
      ShpReader.Destroy;
      Cancel.Enabled:=false;
      Ok.Enabled:=true;
      SaveReportBtn.Enabled:=true;
   end;
end;

procedure TMainWnd.AXImpExpDbcRequestImpLayerIndex(Sender: TObject;
  const Layername: WideString; var LayerIndex: Integer);
var i:integer;
begin
   // the Layerindex of the Layername has to be evaluated
   for i:=0 to DestDocument.Layers.Count-1 do
   begin
      if DestDocument.Layers.Items[i].Layername = Layername then
      begin
         LayerIndex:=DestDocument.Layers.Items[i].Index;
         break;
      end;
   end;
end;

procedure TMainWnd.AXImpExpDbcRequestImpProjectName(Sender: TObject;
  var ProjectName: WideString);
var ProjName:string;
begin
   ProjName:=DestDocument.Name;
   ProjectName:=ProjName;
end;

procedure TMainWnd.AXImpExpDbcRequestImpProjectGUIDs(Sender: TObject;
  var StartGUID, CurrentGUID: WideString);
var aStartGUID, aCurrentGUID:string;
begin
   aStartGUID:=DestDocument.StartProjectGUID;
   aCurrentGUID:=DestDocument.CurrentProjectGUID;
   StartGUID:=aStartGUID;
   CurrentGUID:=aCurrentGUID;
end;

procedure TMainWnd.AXImpExpDbcWarning(Sender: TObject;
  const Info: WideString);
begin
   StatusLb.Items.Add(Info);
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;
end;

end.
