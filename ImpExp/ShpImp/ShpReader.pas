unit ShpReader;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Math, ImpExpObjects, Db;

const
       Col_Int      = 0;
       Col_SmallInt = 1;
       Col_Float    = 2;
       Col_Date     = 3;
       Col_String   = 4;

type
  { Hier k�nnen Strukturen definiert werden }
  p_double = ^doublewert;
  doublewert = record
     Zahl :double;
  end;

  p_integer = ^integerwert;

  integerwert = record
     Zahl :integer;
  end;

ShapeReader = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      function    OpenSHPFile(Filename :string):longint; // open Shape-File and return size
      function    OpenDBFFile(Filename :string):boolean; // open database file that belongs to Shape
      procedure   CloseAllFiles;                         // close all files
      function    ReadHeader:longint;                    // read header of Shape-File
      function    ReadElement:longint;                   // read Shape-Object
      function    GetNumObjects:longint;                 // get number of objects
      procedure   ImportElement;                         // Import Shape element
      procedure   CreateDatabase;
   private
      { private Definitionen }
      ShpLayer :TImpExpLayer;    // layer object to store objects of shape-file
      ShpFile          :file;    // File that will be parsed
      Database_ok      :boolean; // flag if database should be used
      DatabaseCopied   :boolean; // flag if database has been copied to tmp-dir
      NewDbfFilename   :string;  // name of new database
      ImportIndex      :integer; // Import index
      // methods to base objects
      procedure ValInteger(Feld :array of byte;Typ:string;var Erg:longint);
      function ReadInteger(Typ:string;var Erg:longint):boolean;
      procedure ReadDouble(Typ:string;var Erg:double);
      procedure ValDouble(Feld :array of byte;Typ:string;var Erg:double);

      // methods to parse shape-objects
      function ReadNullShape:longint;
      function ReadPointShape:longint;
      function ReadPolylineShape:longint;
      function ReadPolygonShape:longint;
      function ReadRecordHeader(var Nummer:longint; var Laenge:longint):boolean;

      // other methods
      procedure   CopyAFile(Source:string; Dest:string); // Copy file method
end;
implementation
uses MainDlg;

// constructor of Shape-Reader
constructor ShapeReader.Create;
begin
   ImportIndex:=0;                 // reset export iterator index
   ShpLayer:=TImpExpLayer.Create;  // create layer object to store Shape objects
end;

// destructor of Shape-Reader
destructor ShapeReader.Destroy;
begin
   if ShpLayer <> nil then ShpLayer.Destroy; // remove Shape-layer object
end;

// method is used to open Shape-File and return number of Bytes to process
function ShapeReader.OpenSHPFile(Filename:string):longint;
var aLayername:string;
    aFile:file of byte;
begin
   // first get number of bytes to process
   {$I-}
   AssignFile(aFile, Filename);
   Filemode:=0;
   Reset(aFile);
   Result:=FileSize(aFile);
   Closefile(aFile);
   {$I+}

   // now open file for parsing
   {$I-}
   Assignfile(ShpFile,Filename);
   Reset(ShpFile,1); // open binary
   {$I+}

   // name of layer is same than of Shapefile
   aLayername:=ExtractFilename(Filename); aLayername:=copy(aLayername,1,Pos('.',aLayername)-1);
   ShpLayer.Name:=aLayername;
end;

// method is used to open Shape database file
function ShapeReader.OpenDbfFile(Filename:string):boolean;
var i:integer;
    ColName:string;
    ColType:integer;
    ColLength:integer;
    dummy:string;
begin
   result:=TRUE;
   if MainWnd.AXImpExpDbc.ImpDbMode = 0 then // Db_None
   begin
      Database_ok:=false;
      exit;
   end;

   if Fileexists(Filename) then
   begin
      // the dBase file will always copyied to C:\temp\shpimp.dbf during import to
      // avoid conflicts of during import due to long path or filename
      NewDbfFilename:=MainWnd.AXImpExpDbc.TmpDBaseDir+'shpimp.dbf';
      if (FileExists(NewDbfFilename)) then // if the table already exists delete it
         DeleteFile(PCHAR(NewDbfFilename));

      CopyAFile(Filename, NewDbfFilename);
      if not MainWnd.ADOConnection.Connected then
      begin
         // open ADO Connection
         MainWnd.ADOConnection.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False';
         MainWnd.ADOConnection.Properties['Extended Properties'].Value := 'dBASE 5.0';
         dummy:=ExtractFileDir(ExtractShortPathName(Filename));
         MainWnd.ADOConnection.Properties['Data Source'].Value :=ExtractFileDir(NewDbfFilename);;
         MainWnd.ADOConnection.Connected:=True;
      end;
      MainWnd.ADOTable.TableName:='['+ExtractFilename(NewDbfFilename)+']';
      MainWnd.ADOTable.Connection:=MainWnd.ADOConnection;
      // read out the database columns
      try
         MainWnd.ADOTable.Open;
      except
         Database_ok:=FALSE;
         MainWnd.ADOConnection.Connected:=FALSE;
         DeleteFile(PCHAR(NewDbfFilename)); // delete the temporary .dbf file
         // Show warning that there had been problems during open of database
         result:=FALSE;
         exit;
      end;
      MainWnd.ADOTable.First;
      for i := 0 to MainWnd.ADOTable.FieldCount - 1 do { Einf�gen der Spaltennamen in die Datenbankliste }
      begin
         ColName:=MainWnd.ADOTable.Fields[i].FieldName;
         ColLength:=MainWnd.ADOTable.Fields[i].Size;
              if (MainWnd.ADOTable.Fields[i].DataType = ftInteger)  then ColType:=Col_Int
         else if (MainWnd.ADOTable.Fields[i].DataType = ftSmallint) then ColType:=Col_SmallInt
         else if (MainWnd.ADOTable.Fields[i].DataType = ftFloat)    then ColType:=Col_Float
         else if (MainWnd.ADOTable.Fields[i].DataType = ftDate)     then ColType:=Col_Date
         else ColType:=Col_String;
         ShpLayer.AddColumnDef(ColName, ColType, ColLength); // add attrib column to layer
      end;
      Database_ok:=true;
   end
   else
      Database_ok:=false;
end;

// method to copy file
procedure ShapeReader.CopyAFile(Source:string; Dest:string);
var SourceFile:File;
    DestFile  :File;
    Actualbytes:array [1..4096] of char;
    numread      :integer;
    numwritten   :integer;
    OldFilemode  :integer;
begin
   OldFileMode:=Filemode;
   {$I-}
   if not DirectoryExists(ExtractFilePath(Dest)) then
      CreateDir(ExtractFilePath(Dest));

   Filemode:=0; Assignfile(SourceFile,Source); Reset(SourceFile,1);
   Filemode:=1; Assignfile(DestFile,Dest); Rewrite(DestFile,1);
   repeat
      BlockRead(SourceFile, ActualBytes, SizeOf(ActualBytes), NumRead);
      BlockWrite(DestFile, ActualBytes, NumRead, NumWritten);
   until (NumRead = 0) or (NumWritten <> NumRead);
   Closefile(SourceFile);
   Closefile(DestFile);
   {$I+}
   FileMode:=OldFilemode;
end;

// method to close all files
procedure ShapeReader.CloseallFiles;
begin
   {$i-}
   Closefile(ShpFile);
   {$i+}
   if Database_ok = true then
   begin
      if MainWnd.AdoTable.Active then
      begin
         MainWnd.ADOTable.First;
         MainWnd.ADOTable.Close;
         MainWnd.ADOTable.Active:=FALSE;
      end;
      if MainWnd.ADOConnection.Connected then
      begin
         MainWnd.ADOConnection.Connected:=FALSE;
         MainWnd.ADOConnection.Close;
      end;
      Deletefile(PCHAR(NewDbfFilename)); // remove the temporary dBase file
      MainWnd.AXImpExpDbc.CloseImpTables;
   end;
end;

// return number of objects on layer
function ShapeReader.GetNumObjects:longint;
begin
   // return number of objects
   result:=ShpLayer.NumItems;
end;

// function is used to parse header of Shape-File
// -> return number of bytes that have been read (if 0 -> not a correct Shape)
function ShapeReader.ReadHeader:longint;
var FileCode,Filelength,Version,hvarint:longint;
    i:integer;
    hvardouble:double;
    ShapeType:longint;
begin
   ReadInteger('Big',FileCode);  // read File-Code (must be 9994)
   if (FileCode = 9994) then
   begin
      for i:=1 to 5 do ReadInteger('Big',hvarint); // read 5 dummy integers
      ReadInteger('Big',Filelength);               // read File-Length
      ReadInteger('Little',Version);               // read Version of File
      ReadInteger('Little',Shapetype);             // read Type of Shape
      for i:=1 to 4 do ReadDouble('Little',hvardouble); // read Bounding box data
      for i:=1 to 8 do ReadInteger('Big',hvarint); // read unused data
      Result:=100; // header is 100 bytes long
   end
   else
      Result:=0;
end;

procedure ShapeReader.ImportElement;
var aPolyline:TImpExpPoly;
    aPolygon :TImpExpCPoly;
    aPixel   :TImpExpPixel;
    aObject  :TImpExpBaseObj;
begin
   aObject:=ShpLayer.Items[ImportIndex];
   if aObject = nil then exit;
   case aObject.ObjectType of
      impexp_Pixel: begin // Import Pixel object
                       aPixel:=aObject as TImpExpPixel;
                       // Import_Pixel(aPixel);
                    end;
      impexp_Poly : begin // Import Polyline object
                       aPolyline:=aObject as TImpExpPoly;
                       // Import_Poly(aPolyline);
                    end;
      impexp_CPoly: begin // Import Polygon object
                       aPolygon:=aObject as TImpExpCPoly;
                       // Import_CPoly(aPolygon);
                    end;
   end;
end;

// create import database
procedure ShapeReader.CreateDatabase;
var aColDef  :p_ImpExpDbColumn;
    i        :integer;
begin
   if (Database_ok = true) and (ShpLayer.NumColumnDefs > 0) then
   begin
      MainWnd.AXImpExpDbc.CheckAndCreateImpTable(ShpLayer.Name);
      MainWnd.AXImpExpDbc.AddImpIdColumn(ShpLayer.Name);
      for i:=0 to ShpLayer.NumColumnDefs-1 do
      begin
         aColDef:=ShpLayer.ColumnDefs[i];
         MainWnd.AXImpExpDbc.AddImpColumn(ShpLayer.Name, aColDef^.ColName, aColDef^.ColType,aColDef^.ColLength);
      end;
      MainWnd.AXImpExpDbc.CreateImpTable(ShpLayer.Name);
      MainWnd.AXImpExpDbc.OpenImpTable(ShpLayer.Name);
   end;
end;

// function to read element of Shape file
function ShapeReader.ReadElement:longint;
var BytesRead:longint;
    ShapeType:integer;
    Record_Number, Content_Length:longint;
begin
   ReadRecordHeader(Record_Number,Content_Length); Bytesread:=8; // read Record-header (allways 8 bytes)
   ReadInteger('Little',ShapeType); Bytesread:=Bytesread + 4;    // read Shapetype (4 bytes)
   Case Shapetype of
      0: Bytesread:=Bytesread+ReadNullShape;     // read Null-Shape
      1 :Bytesread:=Bytesread+ReadPointShape;    // read Point-Shape
      3 :Bytesread:=Bytesread+ReadPolylineShape; // read Polyline-Shape
      5 :Bytesread:=Bytesread+ReadPolygonShape;  // read Polygon-Shape
   end;
   Result:=Bytesread;
end;

// function is used to read Null-Shape
function ShapeReader.ReadNullShape:longint;
begin
  if (Database_ok = true) then // to keep synchronisized with database
      MainWnd.ADOTable.Next;
  result:=0; // due to no bytes have been read
end;

// function is used to read Polyline shape
function ShapeReader.ReadPolylineShape:longint;
var NumParts,NumPoints,i,j,dbcount,hvarint:longint;
    Bytesread,Punktanz,Pointsworked:longint;
    Parts:array of longint;
    X,Y,hvardouble:double;
    first:boolean;
    Spaltenname, Spalteninhalt:string;
    PolysCreated:integer;
    ret:boolean;

    aPolyline:TImpExpPoly;
begin
   Bytesread:=0;
   for i:=1 to 4 do begin ReadDouble('Big',hvardouble); Bytesread:=Bytesread + 8; end; // read bounding box information
   ReadInteger('Little',NumParts); Bytesread:=Bytesread + 4; // read numparts information
   SetLength(Parts, NumParts+1);
   ReadInteger('Little',NumPoints); Bytesread:=Bytesread + 4; // read numpoints information

   Pointsworked:=0;
   for i:=1 to NumParts do  // read numparts
   begin
      ret:=ReadInteger('Little',Parts[i]); Bytesread:=Bytesread + 4;
      if (not ret) then begin result:=Bytesread-4; exit; end;
   end;

   first:=true;
   PolysCreated:=0;
   for i:=1 to NumParts do     // now the polygon parts will be read
   begin
      // calculate number of corners for polygon
      if i = NumParts then
         Punktanz:=NumPoints - Pointsworked
      else
         Punktanz:=Parts[i+1] - Parts[i];

      if Punktanz = 0 then continue; // if no corners belong to polygon -> continue

      // create new Polyline
      aPolyline:=TImpExpPoly.Create;
      ShpLayer.AddItem(aPolyline);
      if (first = true) and (Database_ok = true) then // add database information to new polygon
      begin
        for dbcount := 0 to MainWnd.ADOTable.FieldCount - 1 do
        begin
           Spaltenname:=MainWnd.ADOTable.Fields[dbcount].Fieldname;
           Spalteninhalt:=MainWnd.ADOTable.FieldByName(Spaltenname).AsString;
           aPolyline.AddAttrib(Spaltenname, Spalteninhalt);
         end;
         first:=false;
         MainWnd.ADOTable.Next;
      end;

      for j:=1 to Punktanz do
      begin
         { Einlesen der Punktkoorindaten X,Y };
         ReadDouble('Little',X);
         ReadDouble('Little',Y);
         Bytesread:=Bytesread + 16; Pointsworked:=Pointsworked + 1;
         aPolyline.AddCorner(X,Y);
      end;
   end;
   Result:=Bytesread;
end;


// function to read polygon shape object
function ShapeReader.ReadPolygonShape:longint;
var NumParts,NumPoints,i,j,dbcount,hvarint:longint;
    Bytesread,Punktanz,Pointsworked:longint;
    Parts:array of longint;
    X,Y,hvardouble:double;
    first:boolean;

    Spaltenname, Spalteninhalt:string;

    DoSplitIsland:boolean;
    PolysCreated:integer;
    ret:boolean;

    aBaseCPoly  :TImpExpCPoly;
    aIslandCPoly:TImpExpCPoly;
begin
   Bytesread:=0;
   for i:=1 to 4 do begin ReadDouble('Big',hvardouble); Bytesread:=Bytesread + 8; end; // read bounding box information
   ReadInteger('Little',NumParts); Bytesread:=Bytesread + 4;  // read numparts information
   SetLength(Parts, NumParts+1); // create array for Parts information

   ReadInteger('Little',NumPoints); Bytesread:=Bytesread + 4; // read numpoints information

   DoSplitIsland:=false;
   // check if number of points are not more than 16384 if it�s an island
   if ((Numparts > 1) and (NumPoints > 16384)) then
   begin
      // display message that island-object has to be splitted into
      // its polygon elements
      MainWnd.StatusLb.Items.Add('Warning: Island object contains more that 16384 corners. It has to be splitted!');
      MainWnd.StatusLB.ItemIndex := MainWnd.StatusLB.Items.Count-1;
      MainWnd.StatusLB.Update;
      DoSplitIsland:=true;
   end;

   Pointsworked:=0;

   for i:=1 to NumParts do  // read numparts
   begin
      ret:=ReadInteger('Little',Parts[i]); Bytesread:=Bytesread + 4;
      if (not ret) then begin result:=Bytesread-4; exit; end;
   end;
   first:=true;
   PolysCreated:=0;

   aIslandCPoly:=nil;aBaseCPoly:=nil;

   for i:=1 to NumParts do     // now the polygon parts will be read
   begin
      // calculate number of corners for polygon
      if i = NumParts then
         Punktanz:=NumPoints - Pointsworked
      else
         Punktanz:=Parts[i+1] - Parts[i];

      if Punktanz = 0 then continue; // if no corners belong to polygon -> continue

      if (PolysCreated = 0) or (DoSplitIsland) then
      begin
         // create base CPolygon
         aBaseCPoly:=TImpExpCPoly.Create;
         ShpLayer.AddItem(aBaseCPoly);
         if DoSplitIsland then
            First:=true;       // that each element of the splitted island will get database-information
      end
      else
      begin
         // create island CPolygon
         aIslandCPoly:=TImpExpCPoly.Create;
         aBaseCPoly.AddIsland(aIslandCPoly);
      end;

      if (first = true) and (Database_ok = true) then // add database information to new polygon
      begin
        for dbcount := 0 to MainWnd.ADOTable.FieldCount - 1 do
        begin
           Spaltenname:=MainWnd.ADOTable.Fields[dbcount].Fieldname;
           Spalteninhalt:=MainWnd.ADOTable.FieldByName(Spaltenname).AsString;
           aBaseCPoly.AddAttrib(Spaltenname, Spalteninhalt);
         end;
         first:=false;
         if (not DoSplitIsland) then
            MainWnd.ADOTable.Next;
      end;

      for j:=1 to Punktanz do
      begin
         { Einlesen der Punktkoorindaten X,Y };
         ReadDouble('Little',X);
         ReadDouble('Little',Y);
         Bytesread:=Bytesread + 16; Pointsworked:=Pointsworked + 1;
         if (PolysCreated = 1) or (DoSplitIsland) then
            aBaseCPoly.AddCorner(X,Y)
         else
            aIslandCPoly.AddCorner(X,Y);
      end;
   end;
   if (Database_ok and DoSplitIsland) then
      MainWnd.ADOTable.Next;
   SetLength(Parts, 0); // remove parts array
   Result:=Bytesread;
end;

// method to parse point shape
function ShapeReader.ReadPointShape:longint;
var Bytesread                    :longint;
    hvarint,dbcount:integer;
    X,Y:double;
    Spaltenname,Spalteninhalt    :string;
    aPixel:TImpExpPixel;
begin
   Bytesread:=0;
   // read X/Y Coordinates
   ReadDouble('Little',X); ReadDouble('Little',Y); Bytesread:=Bytesread + 16;
   aPixel:=TImpExpPixel.Create;
   aPixel.X:=X; aPixel.Y:=Y;
   ShpLayer.AddItem(aPixel);
   if Database_ok = true then
   begin
      for dbcount := 0 to MainWnd.ADOTable.FieldCount - 1 do { Einf�gen der Spaltennamen in die Datenbankliste }
      begin
         Spaltenname:=MainWnd.ADOTable.Fields[dbcount].Fieldname;
         Spalteninhalt:=MainWnd.ADOTable.FieldByName(Spaltenname).AsString;
         aPixel.AddAttrib(Spaltenname, SpaltenInhalt);
      end;
      MainWnd.ADOTable.Next;
   end;
   Result:=Bytesread;
end;

// method to read Record-Header
function ShapeReader.ReadRecordHeader(var Nummer:longint; var Laenge:longint):boolean;
var ret:boolean;
begin
   ret:=ReadInteger('Big',Nummer); if (not ret) then begin result:=FALSE; exit; end;
   ret:=ReadInteger('Big',Laenge); if (not ret) then begin result:=FALSE; exit; end;
   result:=TRUE;
end;

{*******************************************************************************
* PROZEDUR : ReadInteger                                                       *
********************************************************************************
* Prozedur dient zum Einlesen eines Integerdatensatzes aus dem Shapefile.      *
*                                                                              *
* InPARAMETER: Typ -> Typ, wie die Auswertung erfolgen soll. (Big, oder Little)*
*                                                                              *
********************************************************************************}
function ShapeReader.ReadInteger(Typ:string;var Erg:longint):boolean;
var Buffer: array [0..3] of byte;
    Bytesread :longint;
    retVal    :boolean;
begin
   retVal:=TRUE;
   {$I-}
   if (Eof(ShpFile)) then
   begin
      retVal:=FALSE;
      Erg:=0;
   end
   else
   begin
      Blockread(ShpFile,Buffer,4,Bytesread);
      ValInteger(Buffer,Typ,Erg);
   end;
   {$I+}
   result:=retVal;
end;


{*******************************************************************************
* PROZEDUR : ReadDouble                                                        *
********************************************************************************
* Prozedur dient zum Einlesen eines Doubledatensatzes aus dem Shapefile.       *
*                                                                              *
* InPARAMETER:  Typ -> Typ, wie die Auswertung erfolgt. (Big, oder Little)     *
*                                                                              *
* OutPARAMETER: Erg -> Doublewert der aus Datei eingelesen wurde.              *
*                                                                              *
********************************************************************************}
procedure ShapeReader.ReadDouble(Typ:string;var Erg:double);
var Buffer: array [0..7] of byte;
    Bytesread:longint;
begin
   {$I-}
   if (Eof(ShpFile)) then
   begin
      Erg:=0;
   end
   else
   begin
      Blockread(ShpFile,Buffer,8,Bytesread);
      ValDouble(Buffer,Typ,Erg);
   end;
   {$I+}
end;

{*******************************************************************************
* PROZEDUR : ValInteger                                                        *
********************************************************************************
* Prozedur dient zum auswerten einer Zeichenkette, die einen Integerwert       *
* beinhaltet.                                                                  *
*                                                                              *
* InPARAMETER:  Feld -> 4-Byte lange Zeichenkette, die Integerwert beinhaltet  *
*               Typ  -> Gibt an, wie die Zeichenkette ausgewertet werden soll  *
*                                                                              *
* OutPARAMETER: Erg  -> Integerwert der Zeichenkette                           *
*                                                                              *
********************************************************************************}
procedure ShapeReader.ValInteger(Feld :array of byte;Typ:string;var Erg:longint);
var HFeld:array [0..3] of byte;
    Zeiger:p_integer;
    i:integer;
begin
   if Typ = 'Little' then
   begin
      Zeiger:=@Feld;
      Erg:=Zeiger^.Zahl;
   end;
   if Typ = 'Big' then
   begin
      for i:=0 to 3 do hFeld[i]:=Feld[3-i];
      Zeiger:=@hFeld;
      Erg:=zeiger^.Zahl;
   end;
end;


{*******************************************************************************
* PROZEDUR : ValDouble                                                         *
********************************************************************************
* Prozedur dient zum Auswerten einer Zeichenkette, die einen Doublewert        *
* beinhaltet.                                                                  *
*                                                                              *
* InPARAMETER:  Feld -> 8-Byte lange Zeichenkette, die Doublewert beinhaltet   *
*               Typ  -> Gibt an, wie Zeichenkette ausgewertet wird.            *
*                                                                              *
* OutPARAMETER: Erg  -> Doublewert der ausgewerteten Zeichenkette.             *
*                                                                              *
********************************************************************************}
procedure ShapeReader.ValDouble(Feld :array of byte;Typ:string;var Erg:double);
var hvar1,hvar2,hvar3,hvar4,hvar5,hvar6,hvar7,hvar8:double;
    hzeiger :p_double;
begin
   if Typ = 'Big' then
   begin
      hvar1:=Feld[0]; hvar1:=hvar1 * (power(256,7));
      hvar2:=Feld[1]; hvar2:=hvar2 * (power(256,6));
      hvar3:=Feld[2]; hvar3:=hvar3 * (power(256,5));
      hvar4:=Feld[3]; hvar4:=hvar4 * (power(256,4));
      hvar5:=Feld[4]; hvar5:=hvar5 * (power(256,3));
      hvar6:=Feld[5]; hvar6:=hvar6 * (power(256,2));
      hvar7:=Feld[6]; hvar7:=hvar7 * 256;
      hvar8:=Feld[7]; hvar8:=hvar8 * 1;
      Erg:=hvar1 + hvar2 + hvar3 + hvar4 + hvar5 + hvar6 + hvar7 + hvar8;
   end;
   if Typ = 'Little' then
   begin
      { Umkasten des Inhalts des Feldes }
      hzeiger:=@Feld;
      Erg:=hzeiger^.Zahl;
      {hvar1:=Feld[7]; hvar1:=hvar1 * (power(256,7));
      hvar2:=Feld[6]; hvar2:=hvar2 * (power(256,6));
      hvar3:=Feld[5]; hvar3:=hvar3 * (power(256,5));
      hvar4:=Feld[4]; hvar4:=hvar4 * (power(256,4));
      hvar5:=Feld[3]; hvar5:=hvar5 * (power(256,3));
      hvar6:=Feld[2]; hvar6:=hvar6 * (power(256,2));
      hvar7:=Feld[1]; hvar7:=hvar7 * 256;
      hvar8:=Feld[0]; hvar8:=hvar8 * 1;
      Erg:=hvar1 + hvar2 + hvar3 + hvar4 + hvar5 + hvar6 + hvar7 + hvar8; }
   end;
end;
end.
