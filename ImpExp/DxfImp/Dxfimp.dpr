library Dxfimp;

uses
  Forms,
  Main in 'MAIN.PAS',
  Cells in 'CELLS.PAS',
  Files in 'FILES.PAS',
  Misctool in 'MISCTOOL.PAS',
  Struct in 'STRUCT.PAS',
  Objects in 'OBJECTS.PAS',
  SysUtils,
  Dialogs,
  comobj,
  AXDLL_TLB,
  Selfiles in 'SELFILES.PAS' {SelFilesForm},
  Seldbatt in 'SELDBATT.PAS' {SelDBAttForm},
  Sellayer in 'SELLAYER.PAS' {SelLayerForm},
  Seltxatt in 'SELTXATT.PAS' {SelTextAttForm},
  Selfonts in 'SELFONTS.PAS' {SelfontsForm},
  SelVFont in 'SELVFONT.PAS' {SelVFontForm},
  Sellinet in 'SELLINET.PAS' {SellinetForm},
  Miscopt in 'MISCOPT.PAS' {MiscOptForm},
  SaveOptWnd in 'SAVEOPTWND.PAS' { SaveOptWnd },
  Dkm in 'DKM.PAS',
  SetdxffileDlg in 'SetdxffileDlg.pas' {SetdxffileWnd},
  Strfile in 'Strfile.pas',
  MainDlg in 'MainDlg.pas' {MainWnd},
  DkmDetailsDlg in 'DkmDetailsDlg.pas' {DkmDetailsWnd};

{$R *.RES}

{*******************************************************************************
* PROZEDURE * DXFIMPORT                                                        *
********************************************************************************
* Procedure is used to import DXF data into GIS                                *
*                                                                              *
********************************************************************************}
procedure DXFIMPORT(DIR:PCHAR;var AXHANDLE:VARIANT;var DEST:VARIANT;REGMODE:integer;LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer;DKMREG:integer);stdcall;
var
aDocument :IDispatch;
begin
   // create SelFilesForm
   SelFilesForm:=TSelFilesForm.Create(nil);
   SelFilesForm.App:=AXHANDLE;
   aDocument:=DEST;
   SelFilesForm.DestDocument:=aDocument as IDocument;
   SelFilesForm.CreateStringFile(strpas(DIR), strpas(LNGCODE),'DXFIMP');
   SelFilesForm.Regmode:=REGMODE;

   // create MainWnd
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.GisHandle:=WINHANDLE;
 if DKMREG = 1 then
      MainWnd.DkmOn:=TRUE
 else
    MainWnd.DkmOn:=FALSE;

   MainWnd.InitWindow;

   // create SetdxffileWnd
   SetdxffileWnd:=TSetdxffileWnd.Create(nil);
   SetdxffileWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);

   SetdxffileWnd.Show;
end;

{*******************************************************************************
* PROCEDURE * CREATEIMP                                                        *
********************************************************************************
* Procedure is used to prepare a dxf-import for Batchmode                      *
*                                                                              *
********************************************************************************}
procedure CREATEIMP(DIR:PCHAR;var AXHANDLE:VARIANT;var DEST:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer;DKMREG:integer);stdcall;
var aDocument:IDispatch;
begin
   // create SelFilesForm
   SelFilesForm:=TSelFilesForm.Create(nil);
   SelFilesForm.App:=AXHANDLE;
   aDocument:=DEST;
   SelFilesForm.DestDocument:=aDocument as IDocument;
   SelFilesForm.CreateStringFile(strpas(DIR), strpas(LNGCODE),'DXFIMP');
   SelFilesForm.Regmode:=REGMODE;

   // create MainWnd
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.GisHandle:=WINHANDLE;
   if DKMREG = 1 then
      MainWnd.DkmOn:=TRUE
   else
      MainWnd.DkmOn:=FALSE;
   MainWnd.InitWindow;

   // create SetdxffileWnd
   SetdxffileWnd:=TSetdxffileWnd.Create(nil);
   SetdxffileWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);
   SetdxffileWnd.SetupForBatch;
end;

{*******************************************************************************
* PROCEDURE * SETDXFIMPSETTINGS                                                *
********************************************************************************
* Procedure is used to set type of import                                      *
*                                                                              *
* PARAMETERS: DKMMODE     -> Type how the .dxf file should be handeled         *
*                            0 -> The files should not be handeled as DKM files*
*                            1 -> The files should be handeled as DKM files    *
*             SELMODE     -> Type if it should be selective imported or not    *
*                            0 -> It should not be selective imported.         *
*                            1 -> It should be imported selective.             *
*             UMLAUTMODE  -> Type if the umlauts should be converted           *
*                            0 -> The umlauts should not be converted.         *
*                            1 -> The umlauts should be converted.             *
*             HEADERMODE  -> Type if a header file should be used or not.      *
*                            0 -> No header file should be used                *
*                            1 -> A header file should be used                 *
*             HEADERNAME   -> Name of the Dxf-Header file.                     *
*             ADDSETTINGS  -> String that contains additional settings         *
*                                                                              *
********************************************************************************}
procedure SETDXFIMPSETTINGS(DKMMODE:INTEGER; SELMODE:INTEGER; UMLAUTMODE:INTEGER; HEADERMODE:INTEGER; HEADERNAME:PCHAR;ADDSETTINGS:PCHAR);stdcall;
begin
   SetdxffileWnd.SetImpSettings(DKMMODE, SELMODE, UMLAUTMODE, HEADERMODE, strpas(HEADERNAME), strpas(ADDSETTINGS));
end;

// procedure is used to add a .dxf file that should be imported
procedure ADDIMPFILE(AFILE:PCHAR);stdcall;
begin
   SetdxffileWnd.AddImpFile(strpas(AFILE));
end;

// procedure is used to set database-settings
procedure SETDBSETTINGS(SOURCEDBMODE:INTEGER; SOURCEDBNAME:PCHAR; SOURCEDBTABLE:PCHAR;
                        DESTDBMODE  :INTEGER; DESTDBNAME  :PCHAR; DESTDBTABLE  :PCHAR);stdcall;
begin
   // source database will be ignored for Dxf-Import
   MainWnd.AXImpExpDbc.ImpDbMode:=DESTDBMODE;
   MainWnd.AxImpExpDbc.ImpDatabase:=strpas(DESTDBNAME);
   MainWnd.AXImpExpDbc.ImpTable:=strpas(DESTDBTABLE);
   SelFilesForm.SetDatabaseSettings;
end;

// procedure is used to set projection for batch mode
procedure SETPROJECTION(SOURCECOORDINATESYSTEM:INTEGER; // source projection
                        SOURCEPROJECTION      :PCHAR;
                        SOURCEDATE            :PCHAR;
                        SOURCEPROJSETTINGS    :PCHAR;
                        SOURCEXOFFSET         :DOUBLE;
                        SOURCEYOFFSET         :DOUBLE;
                        SOURCESCALE           :DOUBLE;
                        TARGETCOORDINATESYSTEM:INTEGER; // target projection
                        TARGETPROJECTION      :PCHAR;
                        TARGETDATE            :PCHAR;
                        TARGETPROJSETTINGS    :PCHAR;
                        TARGETXOFFSET         :DOUBLE;
                        TARGETYOFFSET         :DOUBLE;
                        TARGETSCALE           :DOUBLE);stdcall;
begin
   // source projection
   MainWnd.AxGisProjection.SourceCoordinateSystem:=SOURCECOORDINATESYSTEM;
   MainWnd.AxGisProjection.SourceProjection:=strpas(SOURCEPROJECTION);
   MainWnd.AxGisProjection.SourceDate:=strpas(SOURCEDATE);
   MainWnd.AxGisProjection.SourceProjSettings:=strpas(SOURCEPROJSETTINGS);
   MainWnd.AxGisProjection.SourceXOffset:=SOURCEXOFFSET;
   MainWnd.AxGisProjection.SourceYOffset:=SOURCEYOFFSET;
   MainWnd.AxGisProjection.SourceScale:=SOURCESCALE;

   // target projection
   MainWnd.AxGisProjection.TargetCoordinateSystem:=TARGETCOORDINATESYSTEM;
   MainWnd.AxGisProjection.TargetProjection:=strpas(TARGETPROJECTION);
   MainWnd.AxGisProjection.TargetDate:=strpas(TARGETDATE);
   MainWnd.AxGisProjection.TargetProjSettings:=strpas(TARGETPROJSETTINGS);
   MainWnd.AxGisProjection.TargetXOffset:=TARGETXOFFSET;
   MainWnd.AxGisProjection.TargetYOffset:=TARGETYOFFSET;
   MainWnd.AxGisProjection.TargetScale:=TARGETSCALE;
end;

// function is used to execute an asc-import
procedure EXECIMP;stdcall;
begin
   MainWnd.ExitNormal:=true;
   MainWnd.Show;
end;

function FREEDLL:boolean; stdcall;
var retVal:boolean;
begin
   SelFilesForm.Free;
   retVal:=MainWnd.ExitNormal;
   MainWnd.Free;
   SetdxffileWnd.Free;
   result:=retVal;
end;

exports DXFIMPORT, CREATEIMP, SETDXFIMPSETTINGS, ADDIMPFILE,
        SETDBSETTINGS, SETPROJECTION, EXECIMP, FREEDLL;

begin
end.
