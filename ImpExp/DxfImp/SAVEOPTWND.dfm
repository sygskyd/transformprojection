object SaveOptDlg: TSaveOptDlg
  Left = 230
  Top = 262
  BorderStyle = bsDialog
  Caption = 'Save Convert Options'
  ClientHeight = 92
  ClientWidth = 334
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object OptFileLabel: TLabel
    Left = 184
    Top = 18
    Width = 59
    Height = 13
    Caption = 'OptFileLabel'
  end
  object OptCB: TCheckBox
    Left = 8
    Top = 16
    Width = 137
    Height = 17
    Caption = 'Save Options to File'
    TabOrder = 0
    OnClick = OptCBClick
  end
  object OkButton: TButton
    Left = 233
    Top = 63
    Width = 93
    Height = 23
    Caption = 'Ok'
    Default = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 1
  end
  object LoadBtn: TButton
    Left = 152
    Top = 12
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 2
    OnClick = LoadBtnClick
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Options file|*.opt|hugo|*.pas'
    Left = 56
    Top = 48
  end
end
