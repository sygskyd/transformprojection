object MainWnd: TMainWnd
  Left = 214
  Top = 166
  BorderStyle = bsDialog
  Caption = 'DXF-Import 2000'
  ClientHeight = 350
  ClientWidth = 458
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 441
    Height = 305
    Caption = 'Panel1'
    TabOrder = 0
    object Label2: TLabel
      Left = 8
      Top = 8
      Width = 88
      Height = 13
      Caption = 'Progressmessages'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Prozessbalken: TGauge
      Left = 8
      Top = 256
      Width = 425
      Height = 20
      Progress = 0
    end
    object OverallProgress: TGauge
      Left = 8
      Top = 232
      Width = 425
      Height = 20
      Progress = 0
    end
    object StatusLB: TListBox
      Left = 8
      Top = 24
      Width = 425
      Height = 169
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
    end
    object SaveReportBtn: TBitBtn
      Left = 152
      Top = 200
      Width = 145
      Height = 25
      Caption = 'Save report'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = SaveReportBtnClick
      Style = bsNew
    end
  end
  object OK: TBitBtn
    Left = 240
    Top = 320
    Width = 93
    Height = 23
    Caption = 'OK'
    Default = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 1
    OnClick = OKClick
    NumGlyphs = 2
    Style = bsNew
  end
  object Cancel: TBitBtn
    Left = 348
    Top = 320
    Width = 101
    Height = 23
    Cancel = True
    Caption = 'Abbrechen'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 2
    OnClick = CancelClick
    NumGlyphs = 2
    Style = bsNew
  end
  object AXImpExpDbc: TAXImpExpDbc
    Left = 8
    Top = 320
    Width = 33
    Height = 23
    ParentFont = False
    TabOrder = 3
    OnDialogSetuped = AXImpExpDbcDialogSetuped
    OnRequestImpLayerIndex = AXImpExpDbcRequestImpLayerIndex
    OnRequestImpProjectName = AXImpExpDbcRequestImpProjectName
    OnRequestImpProjectGUIDs = AXImpExpDbcRequestImpProjectGUIDs
    OnWarning = AXImpExpDbcWarning
    OnInfo = AXImpExpDbcInfo
    OnShowPercent = AXImpExpDbcShowPercent
    ControlData = {
      54504630065450616E656C00044C656674020803546F70034001055769647468
      02210648656967687402170743617074696F6E06034462630000}
  end
  object AxGisProjection: TAxGisProjection
    Left = 48
    Top = 320
    Width = 50
    Height = 25
    ParentFont = False
    TabOrder = 4
    OnWarning = AxGisProjectionWarning
    OnError = AxGisProjectionError
    ControlData = {
      54504630065450616E656C00044C656674023003546F70034001055769647468
      02320648656967687402190743617074696F6E060647697350726F0000}
  end
  object AXGisObjMan: TAXGisObjMan
    Left = 104
    Top = 320
    Width = 50
    Height = 25
    ParentFont = False
    TabOrder = 5
    ControlData = {
      54504630065450616E656C00044C656674026803546F70034001055769647468
      02320648656967687402190743617074696F6E06064769734D616E0000}
  end
  object SaveDialogRep: TSaveDialog
    DefaultExt = '*.rep'
    FileName = '*.rep'
    Filter = 'Import report file|*.rep'
    Left = 128
    Top = 208
  end
  object CloseTimer: TTimer
    Enabled = False
    Interval = 50
    OnTimer = CloseTimerTimer
    Left = 176
    Top = 312
  end
  object SetupWindowTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = SetupWindowTimerTimer
    Left = 200
    Top = 312
  end
end
