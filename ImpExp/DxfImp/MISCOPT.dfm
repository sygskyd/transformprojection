object MiscOptForm: TMiscOptForm
  Left = 268
  Top = 178
  HelpContext = 500
  BorderIcons = [biMinimize]
  BorderStyle = bsDialog
  Caption = 'Options for DXF-Import'
  ClientHeight = 382
  ClientWidth = 289
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'System'
  Font.Style = []
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    00000000000000000000000000000000000000004C4C4C4C0000000000000000
    000004C4C4C4CCCC22300000000000000002CC4C4C4C4CC22233300000000000
    0022C4CCC4C4CC3233B32A00000000000232CC4CCCCCCC223B3B3B3000000000
    AA32C4C4C4CCC222B3B3B3B300000002A332CCCC4CC322223B3AA2A330000002
    A22CCCC4C4CC3333322222222000002222CCCC4C4C4CCCC2222222A2A20000CC
    CCC4C4CCC4CCCCC4C4C4CCAAA20000CCCCCC4C4C4C4C4C4CCCCC4CCCAA0002C4
    C4C4C4CCC4C4C4C4CCCACCC4CA2002CCCC4CCC4CCC4C222C4C4AAACCCCA002C4
    CCC4C4C4C4C22222C4CC2ACCC22002CCC2CCCC4CCC4222A22C4C2ACCC2200232
    C22CCCC4C4CC22AA222A2ACC22200A32222CCCCC4C4CCC22AAAAA2A22AA00A32
    3322CCC4C4C4CC22A22AAAA22A2002333A22CC4C4C4C3CC2222AA222A2200033
    2AA22CC4C4C322CC22A2AA2AA200002AAAA2CC4C4C4C223C22AA22AAA200002A
    AA2CCCC4C4CC23CCC22A2AAA2200000AA22CCC4CCC4C3CCCC20AAAAA20000002
    22CCCCC4C4C4C4CCC222AAAA200000002CC04C4CCC4C4C4CCC22AA0200000000
    00C4C4C4C4CCCCCCCC222A2000000000004C4C4C4CCCF0CCCC22A20000000000
    0004C4CCC4CFFF04CC2220000000000000000C4C4CFFFFF04C20000000000000
    00000000C0FFFFF000000000000000000000000000000000000000000000FFF0
    0FFFFF8001FFFE00007FFC00003FF800001FF000000FE0000007C0000003C000
    0003800000018000000180000001000000000000000000000000000000000000
    0000000000000000000000000000800000018000000180000001C0000003C000
    0003E0000007F000000FF800001FFC00003FFE00007FFF8001FFFFF00FFF}
  OldCreateOrder = True
  Position = poScreenCenter
  Scaled = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 273
    Height = 329
    TabOrder = 11
    object MaxAttrLabel: TLabel
      Left = 12
      Top = 200
      Width = 89
      Height = 13
      Caption = 'max. Attributsl'#228'nge'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object OptFileLabel: TLabel
      Left = 168
      Top = 304
      Width = 59
      Height = 13
      Caption = 'OptFileLabel'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object IgnoreDuplicatesCb: TCheckBox
      Left = 11
      Top = 112
      Width = 214
      Height = 17
      Caption = 'Ignoriere bereits vorhandene Objekte'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object DkmPostprocessingCb: TCheckBox
      Left = 32
      Top = 72
      Width = 129
      Height = 17
      Caption = 'Postprocessing'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object SplitSymbolsCb: TCheckBox
      Left = 11
      Top = 133
      Width = 254
      Height = 17
      Caption = 'Aufspalten der Symbole'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object DoNotShowTextAttribsInGraphicCb: TCheckBox
      Left = 11
      Top = 176
      Width = 246
      Height = 17
      Caption = 'Erzeuge sichtbare Attribute nur in Datenbank'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
    object UseFilenameAsLayerCb: TCheckBox
      Left = 11
      Top = 152
      Width = 254
      Height = 17
      Caption = 'Verwende Dateinamen als Layernamen'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
  end
  object DestGroupBox: TGroupBox
    Left = 162
    Top = 24
    Width = 111
    Height = 49
    Caption = 'Ziel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'System'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    object DestDebugRBut: TRadioButton
      Left = 12
      Top = 50
      Width = 69
      Height = 17
      Caption = 'Debug'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'System'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object DestASCDDERBut: TRadioButton
      Left = 12
      Top = 34
      Width = 61
      Height = 17
      Caption = 'DDE'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'System'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object DestASCFileRBut: TRadioButton
      Left = 12
      Top = 18
      Width = 85
      Height = 17
      Caption = 'ASC - File'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'System'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
  end
  object ZKoordCheckBox: TCheckBox
    Left = 19
    Top = 13
    Width = 201
    Height = 17
    Caption = 'Z Koordinaten ausgeben'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object OkButton: TButton
    Left = 83
    Top = 352
    Width = 93
    Height = 23
    Caption = 'Ok'
    Default = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 0
    OnClick = OkButtonClick
  end
  object CancelButton: TButton
    Left = 188
    Top = 352
    Width = 93
    Height = 23
    Caption = 'Cancel'
    Default = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 3
    ParentFont = False
    TabOrder = 1
    OnClick = CancelButtonClick
  end
  object BlocknameCheckBox: TCheckBox
    Left = 19
    Top = 36
    Width = 197
    Height = 17
    Caption = 'Blocknamen ausgeben'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
  object GroupBox1: TGroupBox
    Left = 19
    Top = 224
    Width = 254
    Height = 81
    TabOrder = 7
    object SeperatorLabel: TLabel
      Left = 12
      Top = 53
      Width = 65
      Height = 13
      Caption = 'Trennzeichen'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object ConcatAttCheckBox: TCheckBox
      Left = 12
      Top = 29
      Width = 189
      Height = 17
      Caption = ' Attribute verketten'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = ConcatAttCheckBoxClick
    end
    object SeparatorEdit: TEdit
      Left = 128
      Top = 49
      Width = 113
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
  end
  object DKMCheckBox: TCheckBox
    Left = 19
    Top = 60
    Width = 209
    Height = 17
    Caption = 'behandle als DKM File'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnClick = DKMCheckBoxClick
  end
  object MaxAttEdit: TSpinEdit
    Left = 122
    Top = 202
    Width = 57
    Height = 22
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxValue = 255
    MinValue = 0
    ParentFont = False
    TabOrder = 6
    Value = 0
  end
  object OptFileCB: TCheckBox
    Left = 19
    Top = 312
    Width = 118
    Height = 17
    Caption = 'Use Optionsfile'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    OnClick = OptFileCBClick
  end
  object OptFileBtn: TButton
    Left = 147
    Top = 308
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 10
    OnClick = OptFileBtnClick
  end
  object EEDCheckBox: TCheckBox
    Left = 19
    Top = 99
    Width = 213
    Height = 17
    Caption = 'schreibe EED in Datenbank'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
  end
  object DkmDetailsBtn: TButton
    Left = 176
    Top = 80
    Width = 50
    Height = 20
    Caption = 'Details...'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
    OnClick = DkmDetailsBtnClick
  end
end
