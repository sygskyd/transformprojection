object DkmDetailsWnd: TDkmDetailsWnd
  Left = 326
  Top = 305
  BorderStyle = bsDialog
  Caption = 'Dkm Details'
  ClientHeight = 127
  ClientWidth = 244
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 2
    Top = 2
    Width = 239
    Height = 90
    BevelOuter = bvLowered
    TabOrder = 0
    object ParzellenCb: TCheckBox
      Left = 4
      Top = 4
      Width = 225
      Height = 17
      Caption = 'Process Parzellen'
      TabOrder = 0
    end
    object MappenBlattCb: TCheckBox
      Left = 4
      Top = 20
      Width = 229
      Height = 17
      Caption = 'Process Mappenblatt'
      TabOrder = 1
    end
    object NutzungCb: TCheckBox
      Left = 4
      Top = 36
      Width = 225
      Height = 17
      Caption = 'Process Nutzungen'
      TabOrder = 2
      OnClick = NutzungCbClick
    end
    object NutzungCreateCb: TCheckBox
      Left = 22
      Top = 52
      Width = 195
      Height = 17
      Caption = 'Create Nutzungen'
      TabOrder = 3
    end
    object NutzungCombineCb: TCheckBox
      Left = 22
      Top = 68
      Width = 195
      Height = 17
      Caption = 'Combine Nutzungen'
      TabOrder = 4
    end
  end
  object OkBtn: TButton
    Left = 84
    Top = 97
    Width = 75
    Height = 25
    Caption = 'Ok'
    TabOrder = 1
    OnClick = OkBtnClick
  end
end
