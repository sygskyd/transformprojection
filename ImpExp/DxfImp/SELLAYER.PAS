unit Sellayer;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Buttons, Main, ExtCtrls;

type
  TSelLayerForm = class(TForm)
    LayerOffLabel: TLabel;
    LayerOnLabel: TLabel;
    ListLayerOff: TListBox;
    ListLayerOn: TListBox;
    ButCancel: TButton;
    ButBegin: TBitBtn;
    ButPrev: TBitBtn;
    ButNext: TBitBtn;
    ButEnd: TBitBtn;
    Panel1: TPanel;
    procedure FillLists;
    procedure FormCreate(Sender: TObject);
    procedure ButCancelClick(Sender: TObject);
    procedure ButBeginClick(Sender: TObject);
    procedure ButPrevClick(Sender: TObject);
    procedure ButNextClick(Sender: TObject);
    procedure ButEndClick(Sender: TObject);
    procedure ListLayerOffDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ListLayerOnDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ListLayerOffDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ListLayerOnDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure FormShow(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    procedure SetAttributesFromFile(Filename:string);
  end;

var
  SelLayerForm: TSelLayerForm;

implementation

uses MainDlg;

{$R *.DFM}

procedure TSelLayerForm.FillLists;
var
  layer,linetype : string;
  ison,isused,isvisible : boolean;
  color : integer;
begin
  ListLayerOn.Clear; ListLayerOff.Clear;
  LayerObj.FetchReset;
  while LayerObj.FetchLayer (layer,linetype,color,ison,isused,isvisible) = TRUE do
  begin
    if (ison = TRUE) and (isused = TRUE) then ListLayerOn.Items.Add (layer);
    if (ison = FALSE) and (isused = TRUE) then ListLayerOff.Items.Add (layer);
  end;
end;

procedure TSelLayerForm.FormCreate(Sender: TObject);
begin
  Caption := StrFileObj.Assign (61);
  ButCancel.Caption := StrFileObj.Assign (2);
  LayerOffLabel.Caption := StrFileObj.Assign (62);
  LayerOnLabel.Caption := StrFileObj.Assign (63);
end;

procedure TSelLayerForm.ButCancelClick(Sender: TObject);
begin
  if InterfaceObj.DoReallyQuit then ModalResult := mresABORT;
end;

procedure TSelLayerForm.ButBeginClick(Sender: TObject);
begin
  ModalResult := mresBEGIN;
end;

procedure TSelLayerForm.ButPrevClick(Sender: TObject);
begin
  ModalResult := mresPREV;
end;

procedure TSelLayerForm.ButNextClick(Sender: TObject);
begin
  ModalResult := mresNEXT;
end;

procedure TSelLayerForm.ButEndClick(Sender: TObject);
begin
  ModalResult := mresEND;
end;

procedure TSelLayerForm.ListLayerOffDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
   Accept:=true;
end;

procedure TSelLayerForm.ListLayerOnDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
   Accept:=true;
end;

procedure TSelLayerForm.ListLayerOffDragDrop(Sender, Source: TObject; X,
  Y: Integer);
var maxitem,loop:integer;
    line:string;
begin
   if Source = ListLayerOn then
   begin
      maxitem := ListLayerOn.Items.Count - 1;
      for loop := 0 to maxitem do
          if ListLayerOn.Selected [loop] = TRUE then
          begin
             line := ListLayerOn.Items [loop];
             LayerObj.SetLayerOnOff (line,FALSE);
          end;
      FillLists;
   end;
end;

procedure TSelLayerForm.SetAttributesFromFile(Filename:string);
var Datei:Textfile;
    Zeile:string;
    linestr,onoffstr:string;
    onoff:boolean;
begin
   {$I-}
   Assignfile(Datei,Filename);
   Reset(Datei);
   { Zuerst mu� die Section mit den Eintr�gen f�r die Textattribute gefunden werden }
   repeat
      readln(Datei,Zeile);
   until ((Zeile = '[SELLAYER]') or (EOF(Datei)));
   if (Zeile <> '[SELLAYER]') then exit;
   repeat
      readln(Datei,Zeile);
      if ((Zeile <> '[SELFONTS]') and (not Eof(Datei))) then
      begin
         linestr:=copy(Zeile,1,pos(',',Zeile)-1);
         onoffstr:=copy(Zeile,pos(',',Zeile)+1,1);
         if onoffstr = 'T' then onoff:=TRUE else onoff:=FALSE;
         LayerObj.SetLayerOnOff (linestr,onoff);
      end;
   until ((Zeile = '[SELFONTS]') or (Eof(Datei)));
   Closefile(Datei);
   {$I+}
end;

procedure TSelLayerForm.ListLayerOnDragDrop(Sender, Source: TObject; X,
  Y: Integer);
var maxitem,loop:integer;
    line:string;
begin
   if Source = Listlayeroff then
   begin
      maxitem := ListLayerOff.Items.Count - 1;
      for loop := 0 to maxitem do
      if ListLayerOff.Selected [loop] = TRUE then
      begin
         line := ListLayerOff.Items [loop];
         LayerObj.SetLayerOnOff (line,TRUE);
      end;
      FillLists;
   end;
end;

procedure TSelLayerForm.FormShow(Sender: TObject);
begin
   FillLists;
   ButNext.SetFocus;
end;

initialization


end.
