unit DkmDetailsDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, WinOSInfo;

type
  TDkmDetailsWnd = class(TForm)
    Panel1: TPanel;
    ParzellenCb: TCheckBox;
    MappenBlattCb: TCheckBox;
    NutzungCb: TCheckBox;
    NutzungCreateCb: TCheckBox;
    NutzungCombineCb: TCheckBox;
    OkBtn: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OkBtnClick(Sender: TObject);
    procedure NutzungCbClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DkmDetailsWnd: TDkmDetailsWnd;

implementation

uses MainDlg, inifiles;

{$R *.dfm}

procedure TDkmDetailsWnd.FormCreate(Sender: TObject);
begin
   Self.Caption:=StrFileObj.Assign(272);
   ParzellenCb.Caption:=StrFileObj.Assign(273);
   MappenBlattCb.Caption:=StrFileObj.Assign(274);
   NutzungCb.Caption:=StrFileObj.Assign(275);
   NutzungCreateCb.Caption:=StrFileObj.Assign(276);
   NutzungCombineCb.Caption:=StrFileObj.Assign(277);
   OkBtn.Caption:=StrFileObj.Assign(1);
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

procedure TDkmDetailsWnd.FormShow(Sender: TObject);
var inifile:TIniFile;
    dummy:string;
begin
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'dkmcomb.ini');
   dummy:=inifile.ReadString('SETTINGS','ProcessParzellen','TRUE');
   if dummy = 'TRUE' then ParzellenCb.Checked:=TRUE else ParzellenCb.Checked:=FALSE;
   dummy:=inifile.ReadString('SETTINGS','ProcessMappenBlatt','TRUE');
   if dummy = 'TRUE' then MappenBlattCb.Checked:=TRUE else MappenBlattCb.Checked:=FALSE;
   dummy:=inifile.ReadString('SETTINGS','ProcessNutzungen','TRUE');
   if dummy = 'TRUE' then
   begin
      NutzungCb.Checked:=TRUE;
      NutzungCreateCb.Enabled:=TRUE;
      NutzungCombineCb.Enabled:=TRUE;
   end
   else
   begin
      NutzungCb.Checked:=FALSE;
      NutzungCreateCb.Enabled:=FALSE;
      NutzungCombineCb.Enabled:=FALSE;
   end;

   dummy:=inifile.ReadString('SETTINGS','CreateNutzungen','TRUE');
   if dummy = 'TRUE' then NutzungCreateCb.Checked:=TRUE else NutzungCreateCb.Checked:=FALSE;

   dummy:=inifile.ReadString('SETTINGS','CombineNutzungen','TRUE');
   if dummy = 'TRUE' then NutzungCombineCb.Checked:=TRUE else NutzungCombineCb.Checked:=FALSE;
   inifile.Destroy;
end;

procedure TDkmDetailsWnd.OkBtnClick(Sender: TObject);
var inifile:TIniFile;
begin
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'dkmcomb.ini');
   if ParzellenCb.Checked      then inifile.WriteString('SETTINGS','ProcessParzellen','TRUE')   else inifile.WriteString('SETTINGS','ProcessParzellen','FALSE');
   if MappenBlattCb.Checked    then inifile.WriteString('SETTINGS','ProcessMappenBlatt','TRUE') else inifile.WriteString('SETTINGS','ProcessMappenBlatt','FALSE');
   if NutzungCb.Checked        then inifile.WriteString('SETTINGS','ProcessNutzungen','TRUE')   else inifile.WriteString('SETTINGS','ProcessNutzungen','FALSE');
   if NutzungCreateCb.Checked  then inifile.WriteString('SETTINGS','CreateNutzungen','TRUE')    else inifile.WriteString('SETTINGS','CreateNutzungen','FALSE');
   if NutzungCombineCb.Checked then inifile.WriteString('SETTINGS','CombineNutzungen','TRUE')   else inifile.WriteString('SETTINGS','CombineNutzungen','FALSE');
   inifile.Destroy;
   Self.Close;
end;

procedure TDkmDetailsWnd.NutzungCbClick(Sender: TObject);
begin
   NutzungCreateCb.Enabled:=NutzungCb.Checked;
   NutzungCombineCb.Enabled:=NutzungCb.Checked;
end;

end.
