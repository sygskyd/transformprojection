unit Display;

interface

procedure WriteXY(X, Y: Integer; Message: String);

implementation
uses
  WinCrt, Error;

procedure WriteXY(X, Y: Integer; Message: String);
begin
  if (X in [1..80]) and (Y in [1..25] ) then
  begin
    GoToXY(X, Y);
    Write(Message);
  end

  else
    ShowError('Falsche WriteXY Koordinaten');
end;

end.
 