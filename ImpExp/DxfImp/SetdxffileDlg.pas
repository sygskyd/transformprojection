unit SetdxffileDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, FileCtrl, ComCtrls, Db, ADODB, StrFile,
  ShellCtrls, FileSelectionPanel, WinOSInfo;

type
  TSetdxffileWnd = class(TForm)
    PageControl: TPageControl;
    TabSheetFiles: TTabSheet;
    OKBtn: TButton;
    CancelBtn: TButton;
    UseHeaderCB: TCheckBox;
    HeaderBtn: TButton;
    HeaderPanel: TPanel;
    HeaderLabel: TLabel;
    SelectivCB: TCheckBox;
    OptionsBtn: TButton;
    OpenDialogHeaderDxf: TOpenDialog;
    GeoBtn: TButton;
    SetupWindowTimer: TTimer;
    Panel1: TPanel;
    DestDbLabel: TLabel;
    DbBtn: TButton;
    FileSelectionPanel: TFileSelectionPanel;
    ShellTreeView: TShellTreeView;
    FileListBox: TFileListBox;
    ListBox: TListBox;
    TrashPanel: TPanel;
    TrashImage: TImage;
    AddAllBtn: TButton;
    DeleteAllBtn: TButton;
    procedure FormCreate(Sender: TObject);
    procedure UseHeaderCBClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure OptionsBtnClick(Sender: TObject);
    procedure HeaderBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GeoBtnClick(Sender: TObject);
    procedure HeaderPanelDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure DbBtnClick(Sender: TObject);
    procedure HeaderPanelDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure FileSelectionPanelFilesToConvertChanged(Sender: TObject);
  private
    { Private-Deklarationen}
    HeaderFilename:string;
    FirstActivate :boolean;
    CurrentFileIdx:integer;
    procedure CheckWindow;
  public
    { Public-Deklarationen}
    winheight:integer;
    winwidth :integer;
    wintop   :integer;
    winleft  :integer;
    OkBtnMode:boolean;
    procedure SetNext;
    function  GetFilename(var Dateiname:string):boolean;
    function  GetHeaderFilename:string;
    procedure CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
    procedure SetupForBatch;
    procedure SetImpSettings(DkmMode:integer; SelMode:integer; UmlautMode:integer; HeaderMode:integer; HeaderName:string; AddSettings:string);
    procedure AddImpFile(aFilename:string);
  end;

var
  SetdxffileWnd: TSetdxffileWnd;
  StrFileObj   : ClassStrFile;

implementation

uses Main, Miscopt, Selfiles, MainDlg, inifiles;

{$R *.DFM}

procedure TSetdxffileWnd.SetNext;
begin
   CurrentFileIdx:=CurrentFileIdx+1;
end;

function TSetdxffileWnd.GetFilename(var Dateiname:string):boolean;
var aLayername:string;
    aPos:integer;
begin
   if CurrentFileIdx < FileSelectionPanel.NumFilesToConvert then
   begin
      Dateiname:=FileselectionPanel.GetFilename(CurrentFileIdx);
      // set also name of layer if filename should be used as layername
      aLayername:=ExtractFilename(Dateiname);
      aPos:=Pos('.',aLayername);
      Delete(aLayername,aPos, length(aLayername)-aPos+1);
      MainWnd.CurrentFilenameLayer:=aLayername;
      result:=true;
   end
   else
      Result:=false;
end;

procedure TSetdxffileWnd.FormCreate(Sender: TObject);
var myScale:double;
    oldHeight:integer;
    oldWidth :integer;
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   Self.Caption:=StrFileObj.Assign (11);        { WinGIS DXF-Import 2000 }
   UseHeaderCB.Caption:=StrFileObj.Assign (12); { use headerfile }
   OKBtn.Caption:=StrFileObj.Assign (1);        { Ok }
   CancelBtn.Caption:=StrFileObj.Assign (2);    { Cancel }
   SelectivCB.Caption:=StrFileObj.Assign(6);    { convert selective }
   OptionsBtn.Caption:=StrFileObj.Assign(18);   { options  }
   TabSheetFiles.Caption:=StrFileObj.Assign(7); { Files    }
   GeoBtn.Caption:=StrFileObj.Assign(210);       // Projection
   DbBtn.Caption:=StrFileObj.Assign(8);          // Database

   HeaderLabel.Caption:='';
   HeaderFilename:='';
   CurrentFileIdx:=0;
   FirstActivate:=true;

   oldHeight:=Self.Height;
   oldWidth :=Self.Width;

   Self.Height:=1;
   Self.Width:=1;

   myScale:=1; //100000 / Trunc(9600000/(Self.PixelsPerInch));
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
   
   // now adapt winheigt and winwidth
   winheight:=round(oldHeight*myScale);
   winwidth :=round(oldWidth *myScale);
end;

procedure TSetdxffileWnd.UseHeaderCBClick(Sender: TObject);
begin
   if UseHeaderCB.checked then
   begin
      HeaderBtn.Visible:=true;
      HeaderPanel.Visible:=true;
   end
   else
   begin
      HeaderBtn.Visible:=false;
      HeaderPanel.Visible:=false;
   end;
   CheckWindow;
end;



procedure TSetdxffileWnd.OKBtnClick(Sender: TObject);
var inifile:TIniFile;
begin
   // write current setup to the ConGIS.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+ 'ConGIS.ini');

   // write the header-file settings
   if UseHeaderCb.Checked then
      inifile.WriteString('DXFIMP','DxfHeader',HeaderFilename)
   else
   begin
      inifile.WriteString('DXFIMP','DxfHeader','');
      HeaderFilename:='';
   end;

   // write the database settings
   inifile.WriteString('DXFIMP','ImpDbMode',inttostr(MainWnd.AXImpExpDbc.ImpDbMode));
   inifile.WriteString('DXFIMP','ImpDatabase',MainWnd.AXImpExpDbc.ImpDatabase);
   inifile.WriteString('DXFIMP','ImpTable',MainWnd.AXImpExpDbc.ImpTable);

   // write the last used path
   inifile.WriteString('DXFIMP','LastUsedPath',FileSelectionPanel.Directory);

   // write last used Source-Projection
   inifile.WriteString('DXFIMP','SourceCoordinateSystem',inttostr(MainWnd.AxGisProjection.SourceCoordinateSystem));
   inifile.WriteString('DXFIMP','SourceProjSettings',MainWnd.AxGisProjection.SourceProjSettings);
   inifile.WriteString('DXFIMP','SourceProjection',MainWnd.AxGisProjection.SourceProjection);
   inifile.WriteString('DXFIMP','SourceDate',MainWnd.AxGisProjection.SourceDate);
   inifile.WriteString('DXFIMP','SourceXOffset',floattostr(MainWnd.AxGisProjection.SourceXOffset));
   inifile.WriteString('DXFIMP','SourceYOffset',floattostr(MainWnd.AxGisProjection.SourceYOffset));
   inifile.WriteString('DXFIMP','SourceScale',floattostr(MainWnd.AxGisProjection.SourceScale));
   inifile.Destroy;

   SelFilesForm.SetDatabaseSettings;
   MainWnd.ExitNormal:=true;
   // now the import window can be opened and the import executed
   Self.Close;
   MainWnd.Show;
end;

procedure TSetdxffileWnd.CancelBtnClick(Sender: TObject);
begin
   MainWnd.ExitNormal:=false;
   MainWnd.CloseTimer.Enabled:=true;
end;

procedure TSetdxffileWnd.OptionsBtnClick(Sender: TObject);
begin
   MiscOptForm.ShowModal;
end;

procedure TSetdxffileWnd.HeaderBtnClick(Sender: TObject);
begin
   if OpendialogHeaderDXF.Execute then
   begin
      HeaderFilename:=OpenDialogHeaderDXF.FileName;
      HeaderLabel.Caption:=ExtractFilename(HeaderFilename);
   end;
   CheckWindow;
end;

function TSetDxffileWnd.GetHeaderFilename:string;
begin
   result:=HeaderFilename;
end;

procedure TSetDxffileWnd.CheckWindow;
var allok:boolean;
begin
   allok:=true;
   if FileSelectionPanel.NumFilesToConvert = 0 then
   begin
      allok:=FALSE;
      OkBtn.ShowHint:=FALSE;
   end;
   if (UseHeaderCB.checked) and (not Fileexists(HeaderFilename)) then allok:=false;
   if allok then
   begin
      OkBtn.Enabled:=true;
   end
   else
      OkBtn.Enabled:=false;
end;

procedure TSetdxffileWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TSetdxffileWnd.GeoBtnClick(Sender: TObject);
begin
   MainWnd.AxGisProjection.SetupByDialog;
end;

procedure TSetdxffileWnd.HeaderPanelDragDrop(Sender, Source: TObject; X,
  Y: Integer);
var i:integer;
    Dateiname:string;
begin
   if Source = FileListBox then
   begin
      { Nun m�ssen alle Selektierten Eintr�ge der FileLB in die Liste �bernommen werden }
      for i:=0 to FileListBox.Items.Count-1 do
      begin
         Dateiname:=FileListBox.Directory; if Dateiname[length(Dateiname)] <> '\' then Dateiname:=Dateiname+'\';
         Dateiname:=Dateiname + FileListBox.items[i];
         if FileListBox.selected[i] = true then
         begin
            HeaderFilename:=Dateiname;
            HeaderLabel.Caption:=ExtractFilename(HeaderFilename);
         end;
      end;
   end;
   CheckWindow;
end;

procedure TSetdxffileWnd.CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
begin
   wintop:=(WHEIGHT div 2) - (winheight div 2);
   winleft:=(WWIDTH div 2) - (winwidth div 2);
end;

procedure TSetdxffileWnd.SetupWindowTimerTimer(Sender: TObject);
var inifile:TIniFile;
    dummy  :string;
    intval :integer;
    doubleval:double;
    count  :integer;
begin
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,winleft,wintop,winwidth, winheight,SWP_SHOWWINDOW);
   Self.Height:=winheight;
   Self.Width:=winwidth;
   Self.Repaint;
   SetupWindowTimer.Enabled:=false;

   // read out the last used settings from the ConGIS.ini file from section [DXFIMP]
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'ConGIS.ini');
   // read out the last used header file
   dummy:=inifile.ReadString('DXFIMP','DxfHeader','');
   if (dummy <> '') and (FileExists(dummy)) then // setup the header file
   begin
      HeaderFilename:=dummy;
      HeaderLabel.Caption:=ExtractFilename(dummy);
      UseHeaderCb.Checked:=TRUE;
      UseHeaderCBClick(nil);
   end;
   // if no header file should be used do nothing

   // check if a .wgi file for this project exists
   // in this case always the .wgi file will be set as target database
   dummy:=SelFilesForm.DestDocument.Name;
   dummy:=ChangeFileExt(dummy,'.wgi');
   if FileExists(dummy) then // an internal database exists for this project
   begin
      MainWnd.AXImpExpDbc.ImpDbMode:=5; // Idb_Database
      DestDbLabel.Caption:=MainWnd.AXImpExpDbc.ImpDbInfo;
   end
   else
   begin
      // in all other cases the last used database settings
      // will be read out from the .ini file
      dummy:=inifile.ReadString('DXFIMP','ImpDbMode','0');
      val (dummy, intval, count);
      if (intval <> 5) and (intval <> 0) then // if a database is set and it is not an IDB
      begin
         MainWnd.AxImpExpDbc.ImpDbMode:=intval;
         dummy:=inifile.ReadString('DXFIMP','ImpDatabase','');
         MainWnd.AXImpExpDbc.ImpDatabase:=dummy;
         dummy:=inifile.ReadString('DXFIMP','ImpTable','');
         MainWnd.AXImpExpDbc.ImpTable:=dummy;
      end;
      DestDbLabel.Caption:=MainWnd.AXImpExpDbc.ImpDbInfo;
   end;

   // read out the last used path
   dummy:=inifile.ReadString('DXFIMP','LastUsedPath','');
   if dummy <> '' then
      FileSelectionPanel.Directory:=dummy
   else
   begin // else set the current path as LastUsedPath
      GetDir(0, dummy);
      FileSelectionPanel.Directory:=dummy;
   end;

   // read last used Source-Projection
   dummy:=inifile.ReadString('DXFIMP','SourceCoordinateSystem',''); val(dummy,intval,count);
   if (dummy <> '') and (count <> 0) then MainWnd.AxGisProjection.SourceCoordinateSystem:=intval;
   dummy:=inifile.ReadString('DXFIMP','SourceProjSettings','');
   if dummy <> '' then MainWnd.AxGisProjection.SourceProjSettings:=dummy;
   dummy:=inifile.ReadString('DXFIMP','SourceProjection','');
   if dummy <> '' then MainWnd.AxGisProjection.SourceProjection:=dummy;
   dummy:=inifile.ReadString('DXFIMP','SourceDate','');
   if dummy <> '' then MainWnd.AxGisProjection.SourceDate:=dummy;
   dummy:=inifile.ReadString('DXFIMP','SourceXOffset',''); val(dummy, doubleval, count);
   if (dummy <> '') and (count <> 0) then MainWnd.AxGisProjection.SourceXOffset:=doubleval;
   dummy:=inifile.ReadString('DXFIMP','SourceYOffset',''); val(dummy, doubleval, count);
   if (dummy <> '') and (count <> 0) then MainWnd.AxGisProjection.SourceYOffset:=doubleval;
   dummy:=inifile.ReadString('DXFIMP','SourceScale',''); val(dummy, doubleval, count);
   if (dummy <> '') and (count <> 0) then MainWnd.AxGisProjection.SourceScale:=doubleval;

   // read out the ignoremask
   dummy:=inifile.ReadString('DXFIMP','DebugIgnoreMask','0'); val(dummy, intval, count);
   MainWnd.IgnoreMask:=intval;
   // read out the Debug-filename
   MainWnd.DebugFilename:=inifile.ReadString('DXFIMP','DebugFilename','');
   dummy:=inifile.ReadString('DXFIMP','DebugModeActive','FALSE');
   if dummy = 'TRUE' then MainWnd.DebugModeActive:=TRUE else MainWnd.DebugModeActive:=FALSE;
   if not MainWnd.DebugModeActive then
   begin
      MainWnd.DebugFilename:='';
      MainWnd.IgnoreMask:=0;
   end;
   if MainWnd.DebugFilename <> '' then
   begin
      MainWnd.DebugFilename:=OSInfo.TempDataDir + MainWnd.DebugFilename;
      if FileExists(MainWnd.DebugFilename) then
         DeleteFile(MainWnd.DebugFilename);
   end;

   inifile.Destroy;
end;

// procedure is used to setup the SetdxffileWnd for batch mode
procedure TSetdxffileWnd.SetupForBatch;
begin
   if FirstActivate then
   begin
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
   end;
end;

// procedure is used to do the settings for a Dxf-Import in Batchmode
procedure TSetdxffileWnd.SetImpSettings(DkmMode:integer; SelMode:integer; UmlautMode:integer; HeaderMode:integer; HeaderName:string; AddSettings:string);
begin
   MiscOptForm.LoadOptions;
   // set DKM-Mode
   if DkmMode = 0 then
      MiscOptForm.DkmCheckBox.Checked:=false;
   if DkmMode = 1 then
   begin
      // check if the DKM-Mode is registered
      if RegistrationObj.IsDKM then
         MiscOptForm.DkmCheckBox.Checked:=TRUE
      else
         MiscOptForm.DkmCheckBox.Checked:=false;
   end;
   // set Selective-Mode
   if SelMode = 0 then
      SelectivCB.Checked:=false;
   if SelMode = 1 then
      SelectivCB.Checked:=true;

   // set Header-Mode
   if HeaderMode = 0 then
   begin
      UseHeaderCB.checked:=FALSE;
      HeaderFilename:='';
   end;
   if HeaderMode = 1 then
   begin
      UseHeaderCB.checked:=TRUE;
      HeaderFilename:=HeaderName;
   end;

   if AddSettings <> '' then
      MiscOptForm.EvaluateAddImpSettings(AddSettings);
   // save the options
   MiscOptForm.SaveOptions;
end;

// procedure is used to add a dxf-file that should be imported in batch-mode
procedure TSetdxffileWnd.AddImpFile(aFilename:string);
begin
   FileSelectionPanel.AddFileToConvert(aFilename);
end;

procedure TSetdxffileWnd.DbBtnClick(Sender: TObject);
begin
   MainWnd.AXImpExpDbc.SetupByDialog(Self.Handle, 5,5);
   PageControl.Enabled:=FALSE;
   GeoBtn.Enabled:=FALSE;
   DbBtn.Enabled:=FALSE;
   OkBtnMode:=OkBtn.Enabled;
   OkBtn.Enabled:=FALSE;
   CancelBtn.Enabled:=FALSE;
end;

procedure TSetdxffileWnd.HeaderPanelDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
   Accept:=FALSE;
   if Source = FileListBox then
      Accept:=TRUE;
end;

procedure TSetdxffileWnd.FileSelectionPanelFilesToConvertChanged(
  Sender: TObject);
begin
   CheckWindow;
end;

end.
