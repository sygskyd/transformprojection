unit MainDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Gauges,Main,StrFile, OleCtrls,
  AXImpExpDbcXControl_TLB, AxGisPro_TLB, AXGisMan_TLB, AXDLL_TLB, WinOSInfo;


type
  TMainWnd = class(TForm)
    Panel1: TPanel;
    Label2: TLabel;
    Prozessbalken: TGauge;
    StatusLB: TListBox;
    SaveReportBtn: TBitBtn;
    OK: TBitBtn;
    Cancel: TBitBtn;
    SaveDialogRep: TSaveDialog;
    CloseTimer: TTimer;
    SetupWindowTimer: TTimer;
    OverallProgress: TGauge;
    AXImpExpDbc: TAXImpExpDbc;
    AxGisProjection: TAxGisProjection;
    AXGisObjMan: TAXGisObjMan;
    procedure FormCreate(Sender: TObject);
    procedure SaveReportBtnClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure CloseTimerTimer(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AXImpExpDbcDialogSetuped(Sender: TObject);
    procedure AxGisProjectionWarning(Sender: TObject;
      const Info: WideString);
    procedure AxGisProjectionError(Sender: TObject;
      const Info: WideString);
    procedure AXImpExpDbcRequestImpLayerIndex(Sender: TObject;
      const Layername: WideString; var LayerIndex: Integer);
    procedure AXImpExpDbcRequestImpProjectName(Sender: TObject;
      var ProjectName: WideString);
    procedure AXImpExpDbcRequestImpProjectGUIDs(Sender: TObject;
      var StartGUID, CurrentGUID: WideString);
    procedure AXImpExpDbcWarning(Sender: TObject; const Info: WideString);
    procedure AXImpExpDbcInfo(Sender: TObject; const Info: WideString);
    procedure AXImpExpDbcShowPercent(Sender: TObject; Percent: Integer);
  private
    { Private declarations }
    FirstActivate:boolean;
    firstfile    : boolean;
    MappenBlattInfo:string;

    ImportActive   :boolean;
    DkmTest        :boolean;
    DkmPostprocessing:boolean;
    ProjectionAlreadyChecked:boolean;
    MappenBlattSymbolChecked:boolean;
    LastReadLine   :string;
    LastReadLineCnt:integer;
    DebugFile      :TextFile;

    // private time properties
    EndTime        :TDateTime;
    StartTime      :TDateTime;

    procedure StartConvertation(Convtype:boolean);
    procedure StartImport;
    procedure WriteConvertOptionsToFile(Filename:string);
    procedure ReadConvertOptionsFromFile(Filename:string);
    procedure ObjCombDisplayPass(Sender: TObject;  const aPass: WideString);
    procedure ObjCombDisplayPercent(Sender: TObject; aValue: Integer);
    procedure CreateMappenBlattArchive(var aRect:IRect; ArchiveDate:string; MappenBlattLayer:string; MappenBlattSymbolname:string);
    procedure OnRequestArchiveImpLayerIndex(Sender: TObject; const Layername: WideString; var LayerIndex: Integer);
    procedure OnRequestArchiveImpProjectName(Sender: TObject;  var ProjectName: WideString);
    procedure OnRequestArchiveImpProjectGUIDs(Sender: TObject;  var StartGUID, CurrentGUID: WideString);
  public
    { Public declarations }
    hadaborted:boolean;
    LngCode   :string;
    GisHandle :integer;
    ExitNormal:boolean;
    DkmOn     :boolean;
    IgnoreDuplicates:boolean;
    DoNotShowTextAttribsInGraphic:boolean;
    UseFilenameAsLayername:boolean;
    CurrentFilenameLayer:string;

    // Debug Properties
    IgnoreMask:integer;
    DebugFilename:string;
    Debug_NumReadPolylines:integer;
    Debug_LastWrittenObject:integer;
    DebugModeActive:boolean;

    // counters for statistics
    NumSymbolDefs:integer;
    NumSymbols   :integer;
    NumPolys     :integer;
    NumCPolys    :integer;
    NumCircles   :integer;
    NumArcs      :integer;
    NumSplines   :integer;
    NumTexts     :integer;
    NumPoints    :integer;
    Numlayers    :integer;
    NumIgnoredObjects:integer;

    MappenBlattCanBeIgnored:boolean;
    TablesCreated:boolean;

    procedure CheckProjectionSettings;
    procedure SetProjectionSettings;
    procedure AddMappenBlattInfo(Layer:string;Area:double);
    procedure HandleMessages;
    procedure InitWindow;
    function  DoDkmTest:boolean;
    function  DoDkmPostprocessing:boolean;
    procedure SetDkmTesting(dotest:boolean);
    procedure SetDkmPostProcessing(dotest:boolean);
    function  CheckHasDuplicate(id:integer; layer:string; var DuplId:integer):boolean;
    procedure CheckMappenBlattCanBeIgnored(Blockname:string; X:double; Y:double; Value:string);
    procedure WriteDebug(line:string);
    procedure WriteDebugInfo(const debuginfo:string);
    function  GetUserDefStyleByNumber(const idx:integer):string;
    function  GetUserDefStyleByName(const aName:string):integer;
    procedure WriteSummary;
  end;

var
  MainWnd: TMainWnd;
  DXF2ASCObj    : ClassDXF2ASC;
  InterfaceObj  : ClassInterface;
  StrFileObj    : ClassStrfile;

implementation

uses Seldbatt, Sellayer, Seltxatt, Selfonts, SelVFont, Sellinet, Miscopt,
     Selfiles, SetdxffileDlg, SaveOptWnd, AxGisCmb_TLB, inifiles;

var ObjCombObj     :TAxGisCombine;

{$R *.DFM}

procedure TMainWnd.InitWindow;
begin
   DXF2ASCObj := ClassDXF2ASC.Create;
   InterfaceObj := ClassInterface.Create;

   SelDBAttForm:=TSelDBAttForm.Create(Self);
   SelLayerForm:=TSelLayerForm.Create(Self);
   SelTextAttForm:=TSelTextAttForm.Create(Self);
   SelfontsForm:=TSelfontsForm.Create(Self);
   SelvfontForm:=TSelvfontForm.Create(Self);
   SellinetForm:=TSellinetForm.Create(Self);
   MiscOptForm:=TMiscOptForm.Create(Self);
   SaveOptDlg:=TSaveOptDlg.Create(Self);
end;

procedure TMainWnd.FormCreate(Sender: TObject);
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   AxImpExpDbc.Height:=1;
   AxImpExpDbc.Width:=1;

   Caption := StrFileObj.Assign (31);
   Cancel.Caption := StrFileObj.Assign (2);
   Cancel.Hint:=StrFileObj.Assign (32);
   SaveReportBtn.Caption:=StrFileObj.Assign (140);
   Label2.Caption:=StrFileObj.Assign (141);

   hadaborted:=false;
   Self.Height:=1;
   Self.Width:=1;
   FirstActivate:=TRUE;
   Ok.Enabled:=false;
   ImportActive:=false;
   ProjectionAlreadyChecked:=FALSE;
   MappenBlattSymbolChecked:=FALSE;
   TablesCreated:=FALSE;
   SaveReportBtn.Enabled:=false;

   NumSymbolDefs:=0;
   NumSymbols:=0;
   NumPolys:=0;
   NumCPolys:=0;
   NumCircles:=0;
   NumArcs:=0;
   NumSplines:=0;
   NumTexts:=0;
   NumPoints:=0;
   NumLayers:=0;
   NumIgnoredObjects:=0;
   LastReadLineCnt:=0;
   IgnoreMask:=0; // default is ignore nothing
   Debug_NumReadPolylines:=0;

   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

function TMainWnd.DoDkmTest:boolean;
begin
   result:=DkmTest;
end;

function TMainWnd.DoDkmPostProcessing:boolean;
begin
   result:=DkmPostprocessing;
end;

procedure TMainWnd.WriteDebug(line:string);
begin
   LastReadLine:=line; LastReadLineCnt:=LastReadLineCnt+1;
end;

procedure TMainWnd.WriteDebugInfo(const debuginfo:string);
begin
   if DebugFilename = '' then exit; // if no debug info should be created -> exit
   {$I-}
   if not FileExists(DebugFilename) then
   begin
      AssignFile(DebugFile, DebugFilename);
      Rewrite(DebugFile);
   end
   else
   begin
      AssignFile(DebugFile, DebugFilename);
      Append(DebugFile);
   end;
   writeln(DebugFile, DebugInfo);
   Closefile(DebugFile);
   {$I+}
end;

procedure TMainWnd.SetDkmTesting(dotest:boolean);
begin
   if not ImportActive then
   begin
      DkmTest:=dotest and DkmOn;
   end;
end;

procedure TMainWnd.SetDkmPostProcessing(dotest:boolean);
begin
   if not ImportActive then
   begin
      DkmPostprocessing:=dotest and DkmTest;
   end;
end;

procedure TMainWnd.SaveReportBtnClick(Sender: TObject);
var Datei:Textfile;
    i:integer;
    Zeile:string;
begin
   if SaveDialogRep.Execute then
   begin
      {$I-}
      Assignfile(Datei,SaveDialogRep.Filename);
      Rewrite(Datei);
      for i:=0 to StatusLB.Items.Count-1 do
      begin
         Zeile:=StatusLB.items[i];
         writeln(Datei,Zeile);
      end;

      if DebugModeActive then
      begin
         Zeile:='Last written object:'+inttostr(Debug_LastWrittenObject);
         writeln(Datei,Zeile);
         Zeile:='Last number of object:';
         case Debug_LastWrittenObject of
            ot_Pixel : Zeile:=Zeile+inttostr(NumPoints);
            ot_Poly  : Zeile:=Zeile+inttostr(NumPolys);
            ot_CPoly : Zeile:=Zeile+inttostr(NumCPolys);
            ot_Text  : Zeile:=Zeile+inttostr(NumTexts);
            ot_Symbol: Zeile:=Zeile+inttostr(NumSymbols);
            ot_Spline: Zeile:=Zeile+inttostr(NumSplines);
            ot_Circle: Zeile:=Zeile+inttostr(NumCircles);
            ot_Arc   : Zeile:=Zeile+inttostr(NumArcs);
         end;
         writeln(Datei,Zeile);
      end;
      Closefile(Datei);
      {$I+}
   end;
end;

procedure TMainWnd.FormDestroy(Sender: TObject);
begin
   InterfaceObj.Destroy;
   SelDBAttForm.Free;
   SelLayerForm.Free;
   SelTextAttForm.Free;
   SelfontsForm.Free;
   SelvfontForm.Free;
   SellinetForm.Free;
   MiscOptForm.Free;
   SaveOptDlg.Free;
end;

procedure TMainWnd.ObjCombDisplayPass(Sender: TObject;  const aPass: WideString);
begin
   StatusLB.Items.Add(aPass);
   StatusLB.ItemIndex := MainWnd.StatusLB.Items.Count-1;
   StatusLB.Update;
end;

procedure TMainWnd.ObjCombDisplayPercent(Sender: TObject; aValue: Integer);
begin
   Prozessbalken.Progress:=avalue;
   Prozessbalken.Update;
end;

procedure TMainWnd.StartImport;
var aApp           :OleVariant;
    aDocument      :OleVariant;
    IdbProcessMask :integer;
begin
   if ExitNormal then
   begin
      SelFilesForm.InitControl;
      IdbProcessMask:=SelFilesForm.DestDocument.IdbProcessMask;
      SelFilesForm.DestDocument.IdbProcessMask:=0;
      StartConvertation(SetDxfFileWnd.SelectivCB.checked);
      // close the tables
      if TablesCreated then
      begin
         Ok.Enabled:=FALSE;
         SaveReportBtn.Enabled:=FALSE;
         AXImpExpDbc.CloseImpTables;
         AXImpExpDbc.CloseExpTables;
         Ok.Enabled:=TRUE;
         SaveReportBtn.Enabled:=TRUE;
      end;
      WriteSummary;

      // check if a DKM-Post processing has to be done
      if (DoDkmTest) and (DoDkmPostprocessing) and (not hadaborted) then
      begin
         Ok.Enabled:=FALSE;
         ObjCombObj:=TAxGisCombine.Create(self);
         ObjCombObj.WorkingDir:=OSInfo.WingisDir;
         ObjCombObj.LngCode:=LngCode;
         ObjCombObj.OnDisplayPercent:=ObjCombDisplayPercent;
         ObjCombObj.OnDisplayPass:=ObjCombDisplayPass;

         if SelFilesForm.UseExternalApp then
         begin
            aApp:=SelFilesForm.ExternalApp;
            ObjCombObj.SetApp(aApp);
            if SelFilesForm.Ori_ImpDbMode <> db_None then
            begin
               if FileExists(OSInfo.TempDataDir + 'TMP_DKMIMPORT\dkm.mdb') then
               begin
                  ObjCombObj.DbName:=OSInfo.TempDataDir + 'TMP_DKMIMPORT\dkm.mdb';
                  ObjCombObj.DbTable:='';
                  ObjCombObj.DbMode:=ACCESS_Tables;
               end
               else
                  ObjCombObj.DbMode:=db_None; // no database
            end
            else
               ObjCombObj.DbMode:=db_None; // no database
         end
         else
         begin
            aApp:=SelFilesForm.App;
            ObjCombObj.SetApp(aApp);
            ObjCombObj.DbName:=AXImpExpDbc.ImpDatabase;
            ObjCombObj.DbTable:=AXImpExpDbc.ImpTable;
            ObjCombObj.DbMode:=AXImpExpDbc.ImpDbMode;
         end;
         aDocument:=SelFilesForm.Document;
         ObjCombObj.SetDocument(aDocument);
         // check if there have been some objects created
         if SelFilesForm.Document.Layers.Count > 1 then
            ObjCombObj.BEV_Combine(OSInfo.LocalAppDataDir+'dkmcomb.ini', ds_Layers);
         ObjCombObj.Destroy;
         if (SelFilesForm.UseExternalApp) then
         begin
            SelFilesForm.Document.Save;
            CloseTimer.Enabled:=true;
         end
         else
         begin
            Ok.Enabled:=TRUE;
         end;
      end;
      SelFilesForm.DestDocument.IdbProcessMask:=IdbProcessMask;
   end;
end;

procedure TMainWnd.SetupWindowTimerTimer(Sender: TObject);
begin
   windows.setparent (Self.Handle, GisHandle);
   SetWindowPos(Handle,HWND_TOP,SetdxffileWnd.winleft,SetdxffileWnd.wintop,SetdxffileWnd.winwidth, SetdxffileWnd.winheight,SWP_SHOWWINDOW);
   Self.Height:=SetdxffileWnd.winheight;
   Self.Width:=SetdxffileWnd.winwidth;

   Self.Repaint;
   SetupWindowTimer.Enabled:=false;
   StartImport;
end;

procedure TMainWnd.StartConvertation(Convtype:boolean);
var doclose : boolean;
  mr,formnr,mresult : integer;
  formcaption,dxffile : string;
  status_odoa:boolean;
  status_umlaut:boolean;
  NumDxfFiles:integer;
  WorkedDxfFiles:integer;
  SkippedDxfFiles:integer;
  Percent:integer;
  DispDate,aDate:string;
  DispTime,aTime:string;
  FileStartTime, UsedTime:TDateTime;
begin
   if Convtype = true then mr:=mrYES;
   if Convtype = false then mr:=mrNO;
   formcaption := Caption;
   doclose := FALSE;
   firstfile := TRUE;
   ImportActive:=true;
   NumDxfFiles:=SetdxfFileWnd.FileSelectionPanel.NumFilesToConvert;
   WorkedDxfFiles:=0;
   SkippedDxfFiles:=0;
   StartTime:=Now;
   IgnoreDuplicates:=MiscOptForm.IgnoreDuplicatesCb.Checked;
   DoNotShowTextAttribsInGraphic:=MiscOptForm.DoNotShowTextAttribsInGraphicCb.Checked;
   UseFilenameAsLayername:=MiscOptForm.UseFilenameAsLayerCb.Checked;
   if DoDkmTest then
   begin
      DoNotShowTextAttribsInGraphic:=FALSE; // if DKM mode is active visible attributes will be created
      AxImpExpDbc.RemoveEmptyColumnsOnClose:=FALSE; // in DKM mode the empty columns may not be deleted
   end;

   SelFilesForm.ExpandSymbols:=MiscOptForm.SplitSymbolsCb.Checked;
   if DoDkmTest then
   begin
      // in case of Dkm create a mapping for layer <-> font <-> offset
      SelFilesForm.CreateDkmFontMapping;
   end;

   if SetdxfFileWnd.GetFilename(dxffile) then
   repeat
      Ok.Enabled:=false;
      SaveReportBtn.enabled:=false;
      Cancel.Enabled:=true;
      if not firstfile then DXF2ASCObj := ClassDXF2ASC.Create;
      firstfile := FALSE;
      Cancel.Visible := TRUE; ActiveControl := Cancel;

      MappenBlattCanBeIgnored:=FALSE;
      MappenBlattSymbolChecked:=FALSE;

      // display start date/time
      FileStartTime:=now;
      aDate:=FormatDateTime('ddmmyy',now);
      DispDate:=copy(aDate,1,2)+'.'+copy(aDate,3,2)+'.'+copy(aDate,5,2);
      aTime:=Formatdatetime('hhnnss',Now);
      DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
      StatusLB.Items.Add(StrFileObj.Assign(253) + DispDate+' / '+DispTime); // start time:

      if SetdxffileWnd.GetHeaderFilename <> '' then
         StatusLB.Items.Add(StrFileObj.Assign(220) + ExtractFilename(SetdxffileWnd.GetHeaderFilename));

      StatusLB.Items.Add(StrFileObj.Assign(221)+' '+Extractfilename(dxffile));



      MappenBlattInfo:=ExtractFilename(dxffile);
      MappenBlattInfo:=copy(MappenBlattInfo,1,Pos('.',MappenBlattInfo)-1);

      // display database settings
      StatusLB.Items.Add(AxImpExpDbc.ImpDbInfo);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      { Laden der Optionen wie das DXF-File konvertiert werden soll }
      MiscOptForm.LoadOptions;

      CheckProjectionSettings;

      if SetdxffileWnd.GetHeaderFilename <> '' then
        DXF2ASCObj.SetHeaderFile (SetdxffileWnd.GetHeaderFilename);
      status_odoa:=status_umlaut;
      DXF2ASCObj.SetDXFFile (dxffile,status_odoa);
      // Caption := formcaption + '  ' + UpperCase(ExtractFilename (dxffile));
      // DXF2ASCObj.SetOutFile (SelFilesForm.GetASCFile);

      { Wenn das File 0D 0A konvertiert werden muss, oder die Umlaute
        konvertiert werden m�ssen muss ein Pass 0 durchgef�hrt werden }
      if (Status_odoa = false) or (Status_umlaut = true) then
      begin
         { File mu� ins Dosformat umkonvertiert werden }
         InterfaceObj.ShowPass (0);
         DXF2ASCObj.RunPass(0);
      end;
      InterfaceObj.ShowPass (1);
      if not Dxf2AscObj.IsAbort then
      begin
         if not DXF2ASCObj.RunPass (1) then doclose := TRUE
         else
         if (not Dxf2AscObj.IsAbort) and (MappenBlattCanBeIgnored = FALSE) then
         begin
            InterfaceObj.ShowPass (2);
            if not DXF2ASCObj.RunPass (2) then doclose := TRUE;
         end;
      end;

      if (not Dxf2AscObj.IsAbort) and (not Dxf2AscObj.IsSkipFile) then
         if MiscOptForm.OptFileCB.Checked then ReadConvertOptionsFromFile(MiscOptForm.OptionsFilename);

      if (not doclose) and ((mr = mrYes) or (RegistrationObj.IsJapan = true)) and
         (not Dxf2AscObj.IsAbort) and (MappenBlattCanBeIgnored = FALSE) and (not Dxf2AscObj.IsSkipFile) then
      begin
        formnr := 1; mresult:=mresNEXT;
        repeat
          { Wenn DKM konvertiert wird, soll keine h�ndische Symbolzuordnung m�glich sein }
          if (formnr=1) and (DoDkmTest = true) then formnr:=2;
          case formnr of
            1 : begin
                   if RegistrationObj.IsJAPAN then
                   begin
                      init:=true;  mresult:=SelVFontForm.ShowModal; init:=false;
                   end;
                end;
            2 : begin init:=true;
                      // prev button has to be disabled if registration JAPAN is not supported
                      if not RegistrationObj.IsJAPAN then
                      begin
                         SelTextAttForm.ButBegin.Enabled:=FALSE;
                         SelTextAttForm.ButPrev.Enabled:=FALSE;
                      end;
                      mresult := SelTextAttForm.ShowModal;
                end;
            3 : begin init:=true; mresult := SelDBAttForm.ShowModal; end;
            4 : begin init:=true; mresult := SelLineTForm.ShowModal; end;
            5 : begin init:=true; mresult := SelLayerForm.ShowModal; end;
            6 : begin init:=true; mresult := SelFontsForm.ShowModal; end;
          end;
          case mresult of
            mresNEXT   : begin
                         Inc (formnr);
                         if (formnr = 3) and (not AttribStructObj.GetIsWriteDB) then
                           formnr := 4;
                         if formnr = 3 then formnr:= 4;
                         if (formnr = 2) and (mr <> mrYes) then formnr:=7;
                       end;
            mresPREV   : begin
                         Dec (formnr);
                         if (formnr = 3) and (not AttribStructObj.GetIsWriteDB) then
                           formnr := 2;
                         if formnr = 3 then formnr:=2;
                       end;
            mresBEGIN  : formnr := 1;
            mresEND    : formnr := 7;
            mresABORT  : doclose := TRUE;
            else doclose := TRUE;
          end;
        until (doclose) or (formnr = 7);
        { Nun m�ssen die Einstellungen falls verlangt in ein File gespeichert werden }
        if not doclose then
        begin
           SaveOptDlg.ShowModal;
           if SaveOptDlg.OptCB.Checked then
           begin
              WriteConvertOptionsToFile(SaveOptDlg.Filename);
              // the next files use now also this convert-options
              MiscOptForm.OptFileCB.Checked:=TRUE;
              MiscOptForm.OptionsFilename:=SaveOptDlg.Filename;
              MiscOptForm.SaveOptions;
              mr:=mrNO;
           end;
        end;
        if doclose then hadaborted:=true;
      end;
      if (not doclose) and (MappenBlattCanBeIgnored = FALSE) then
      begin
        if (not Dxf2AscObj.IsAbort) and (not Dxf2AscObj.IsSkipFile) then
        begin
           InterfaceObj.ShowPass (3);
           if DXF2ASCObj.RunPass (3) then
           begin
              if not Dxf2AscObj.IsAbort then
              begin
                 InterfaceObj.ShowPass (4);
                 if DXF2ASCObj.RunPass (4) then
                 begin
                    if not Dxf2AscObj.IsAbort then
                    begin
                       InterfaceObj.ShowPass (5);
                       DXF2ASCObj.RunPass (5);
                    end;
                 end;
              end;
           end;
         end;
      end;
      { Cancel.Visible := FALSE; }
      if not doclose then
      begin
         // display endtime
         aDate:=FormatDateTime('ddmmyy',now);
         DispDate:=copy(aDate,1,2)+'.'+copy(aDate,3,2)+'.'+copy(aDate,5,2);
         aTime:=Formatdatetime('hhnnss',Now);
         DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
         StatusLB.Items.Add(StrFileObj.Assign(254) + DispDate + ' / '+DispTime); // end time:
         // display ellapsed time
         EndTime:=Now;
         UsedTime:=EndTime-FileStartTime;
         aTime:=Formatdatetime('hhnnss',UsedTime);
         DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
         StatusLB.Items.Add(StrFileObj.Assign(266) + DispTime); // Used time:
         if not Dxf2AscObj.IsAbort then
         begin
            if Dxf2AscObj.IsSkipFile then
            begin
               SkippedDxfFiles:=SkippedDxfFiles+1;
               StatusLB.Items.Add(StrFileObj.Assign(292));
            end
            else
            begin
               if InterfaceObj.GetWarningCounter > 0 then
                  StatusLB.Items.Add(StrFileObj.Assign(252))
               else
                  StatusLB.Items.Add(StrFileObj.Assign(226));
               end;
         end;
         StatusLB.Items.Add('---------------------------------------------------');
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         InterfaceObj.ResetWarningCounter;
      end;
      SetdxfFileWnd.SetNext;
      if not SetdxfFileWnd.GetFilename(dxffile) then dxffile:='';
      DXF2ASCObj.Destroy;

      WorkedDxfFiles:=WorkedDxfFiles + 1;
      if NumDxfFiles > 0 then Percent:= Round (WorkedDxfFiles / NumDxfFiles * 100 ) else Percent:= 100;
      OverallProgress.Progress :=Percent;
      OverallProgress.Update;
    until (dxffile = '') or (doclose = true) or (hadaborted) ;
    if doclose then
    begin
      StatusLB.Items.Add('---------------------------------------------------');
      StatusLB.ItemIndex :=StatusLB.Items.Count-1;
      StatusLB.Update;
    end;

    // display Statistics
    StatusLB.Items.Add(StrFileObj.Assign(267) + inttostr(WorkedDxfFiles-SkippedDxfFiles));     // Imported Files:
    if SkippedDxfFiles > 0 then
       StatusLB.Items.Add(StrFileObj.Assign(293) + inttostr(SkippedDxfFiles));     // Skipped Files :
end;

procedure TMainWnd.WriteSummary;
var UsedTime:TDateTime;
    aTime, Disptime:string;
begin
    EndTime:=Now;
    UsedTime:=EndTime-StartTime;
    aTime:=Formatdatetime('hhnnss',UsedTime);
    DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
    StatusLB.Items.Add(StrFileObj.Assign(256) + DispTime); // overall time:

    if NumLayers     > 0 then StatusLB.Items.Add(StrFileObj.Assign(255) + inttostr(NumLayers));     // created layers:
    if NumSymbolDefs > 0 then StatusLB.Items.Add(StrFileObj.Assign(257) + inttostr(NumSymbolDefs)); // created symbol definitions:
    if NumSymbols    > 0 then StatusLB.Items.Add(StrFileObj.Assign(258) + inttostr(NumSymbols));    // created symbols:
    if NumPolys      > 0 then StatusLB.Items.Add(StrFileObj.Assign(259) + inttostr(NumPolys));      // created polylines:
    if NumCPolys     > 0 then StatusLB.Items.Add(StrFileObj.Assign(260) + inttostr(NumCPolys));     // created polygons:
    if NumCircles    > 0 then StatusLB.Items.Add(StrFileObj.Assign(261) + inttostr(NumCircles));    // created circles:
    if Numarcs       > 0 then StatusLB.Items.Add(StrFileObj.Assign(262) + inttostr(NumArcs));       // created arcs:
    if NumSplines    > 0 then StatusLB.Items.Add(StrFileObj.Assign(263) + inttostr(NumSplines));    // created splines:
    if NumTexts      > 0 then StatusLB.Items.Add(StrFileObj.Assign(264) + inttostr(NumTexts));      // created texts:
    if NumPoints     > 0 then StatusLB.Items.Add(StrFileObj.Assign(265) + inttostr(NumPoints));     // created points:
    if NumIgnoredObjects > 0 then StatusLB.Items.Add(StrFileObj.Assign(269) + inttostr(NumIgnoredObjects)); // Ignored objects:
    StatusLB.Items.Add('---------------------------------------------------');
    StatusLB.ItemIndex :=StatusLB.Items.Count-1;
    StatusLB.Update;
    Ok.Enabled:=true;
    SaveReportBtn.enabled:=true;
    Cancel.Enabled:=false;
end;

procedure TMainWnd.HandleMessages;
begin
   Application.ProcessMessages;
end;

procedure TMainWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TMainWnd.CloseTimerTimer(Sender: TObject);
begin
   CloseTimer.Enabled:=false;
   MainWnd.Close;
   SetdxffileWnd.Close;
   if (SelFilesForm.UseExternalApp) and (DoDkmTest) and (DkmPostProcessing) and (not hadaborted) then
   begin
      // inform GIS that an AMP-Import has to be done
      // and the DXFIMP.DLL has to be released
      if not FileExists(OSInfo.TempDataDir+'TMP_DKMIMPORT\dkm.mdb') then
         SelFilesForm.Ori_ImpDbMode:=db_None;
      if FileExists(OSInfo.TempDataDir + 'TMP_DKMIMPORT\DKM.AMP') then
         SelFilesForm.DestDocument.DoAmpDkmImport(OSInfo.TempDataDir, OSInfo.TempDataDir + 'TMP_DKMIMPORT\DKM.AMP', OSInfo.TempDataDir+'TMP_DKMIMPORT\dkm.mdb', SelFilesForm.Ori_ImpDbMode, SelFilesForm.Ori_ImpDatabase, SelFilesForm.Ori_ImpTable, LngCode);
   end
   else
      SelFilesForm.DestDocument.FinishImpExpRoutine(ExitNormal,'IMPORTMODULE');
end;

procedure TMainWnd.OKClick(Sender: TObject);
begin
   CloseTimer.Enabled:=true;
end;

procedure TMainWnd.CancelClick(Sender: TObject);
begin
//   StatusLb.Items.Add('Last Read Line ' + inttostr(LastReadLineCnt)+':'+LastReadLine);
   hadaborted:=true;
   DXF2ASCObj.SetAbort;
   Cancel.Enabled:=false;
   Ok.Enabled:=true;
   SaveReportBtn.Enabled:=true;
end;

function TMainWnd.CheckHasDuplicate(id:integer; layer:string; var DuplId:integer):boolean;
var retVal:boolean;
begin
   retVal:=FALSE;
   Duplid:=0;
   if IgnoreDuplicates then
   begin
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(id, layer);
      if (DuplId > 0) then
         retVal:=TRUE;
      if retVal then NumIgnoredObjects:=NumIgnoredObjects+1;
   end;
   result:=retVal;
end;

procedure TMainWnd.SetProjectionSettings;
var aDoc:OleVariant;
    aApp:OleVariant;
begin
   // procedure is used to set the projection settings
   // of the current project to the GeoWnd
   aDoc:=SelFilesForm.DestDocument;
   aApp:=SelFilesForm.App;
   AxGisObjMan.SetDocument(aDoc);
   AxGisObjMan.SetApp(aApp);
   AxGisObjMan.WorkingDir:=OSInfo.WingisDir;
   AxGisObjMan.LngCode:=LngCode;
   AxGisProjection.WorkingPath:=OSInfo.WingisDir;
   AxGisProjection.LngCode:=LngCode;
   AXImpExpDbc.LngCode:=LngCode;
   AXImpExpDbc.WorkingDir:=OSInfo.WingisDir;
   SetdxffileWnd.DestDbLabel.Caption:=AXImpExpDbc.ImpDbInfo;
   AxImpExpDbc.ShowMode:=2; // Shw_Target
   AxGisProjection.SourceCoordinateSystem:=cs_Carthesic;   // ACAD-Data normaly is in carthesic
   AxGisProjection.TargetCoordinateSystem:=SelFilesForm.DestDocument.Projection;
   AxGisProjection.SourceProjSettings:=SelFilesForm.DestDocument.ProjectProjSettings;
   AxGisProjection.TargetProjSettings:=SelFilesForm.DestDocument.ProjectProjSettings;
   AxGisProjection.SourceProjection:=SelFilesForm.DestDocument.ProjectProjection;
   AxGisProjection.TargetProjection:=SelFilesForm.DestDocument.ProjectProjection;
   AxGisProjection.SourceDate:=SelFilesForm.DestDocument.Projectdate;
   AxGisProjection.TargetDate:=SelFilesForm.DestDocument.Projectdate;
   AxGisProjection.SourceXOffset:=SelFilesForm.DestDocument.ProjectionXOffset;
   AxGisProjection.TargetXOffset:=SelFilesForm.DestDocument.ProjectionXOffset;
   AxGisProjection.SourceYOffset:=SelFilesForm.DestDocument.ProjectionYOffset;
   AxGisProjection.TargetYOffset:=SelFilesForm.DestDocument.ProjectionYOffset;
   AxGisProjection.SourceScale:=SelFilesForm.DestDocument.ProjectionScale;
   AxGisProjection.TargetScale:=SelFilesForm.DestDocument.ProjectionScale;
   // if source and target is .amp - file
   // the used input coordinates and used output coordinates
   // for the projection calculation are in carthesic
   AxGisProjection.UsedInputCoordSystem:=AxGisProjection.SourceCoordinateSystem;
   AxGisProjection.UsedOutputCoordSystem:=cs_Carthesic;
end;

procedure TMainWnd.CheckProjectionSettings;
var different:boolean;
begin
   different:=false;
   // function is used to check if the settings in the
   if (AxGisProjection.TargetCoordinateSystem <> SelFilesForm.DestDocument.Projection) then
      different:=TRUE;

   if (AxGisProjection.TargetXOffset <> SelFilesForm.DestDocument.ProjectionXOffset) then
      different:=TRUE;

   if (AxGisProjection.TargetYOffset <> SelFilesForm.DestDocument.ProjectionYOffset) then
      different:=TRUE;

   if (AxGisProjection.TargetScale <> SelFilesForm.DestDocument.ProjectionScale) then
      different:=TRUE;

   // check if the projections are different
   if (SelFilesForm.DestDocument.ProjectProjSettings <> AxGisProjection.TargetProjSettings) then
      different:=TRUE;

   if (SelFilesForm.DestDocument.ProjectProjection <> AxGisProjection.TargetProjection) then
      different:=TRUE;

   if (SelFilesForm.DestDocument.Projectdate <> AxGisProjection.TargetDate) then
      different:=TRUE;

   if (different) and (not ProjectionAlreadyChecked) then
   begin
      Showmessage(StrFileObj.Assign(245)); // Warning: Projection does not fit to project projection
      AxGisProjection.SetupByDialog;
      ProjectionAlreadyChecked:=TRUE;
   end;
   // the used output coordinate-system is equal to the
   // target-coordinate-system
   AxGisProjection.UsedInputCoordSystem:=AxGisProjection.SourceCoordinateSystem;
end;

procedure TMainWnd.WriteConvertOptionsToFile(Filename:string);
var Datei:Textfile;
    attrib:string;
    acadltype,description,winltype:string;
    isused,show:boolean;

    layer,linetype : string;
    ison  : boolean;
    isvisible:boolean;
    color : integer;

    textstyle:string;
    fontfile,winfont:string;
    isvert:boolean;
begin
   if Filename = '' then exit;
   {$I-}
   Assignfile(Datei,Filename);
   Rewrite(Datei);

   { Zuerst m�ssen die Textattributseinstellungen geschrieben werden }
   writeln(Datei,'[SELTXATT]');
   AttribStructObj.FetchResetTextAttribs;
   while AttribStructObj.FetchTextAttrib (attrib,show) = TRUE do
   begin
      if show = TRUE then attrib:=attrib + ',T'
      else attrib:=attrib + ',F';
      writeln(Datei,attrib);
   end;

   { Nun m�ssen die Linetypeeinstellungen geschrieben werden }
   writeln(Datei,'[SELLINET]');
   LineTypeObj.FetchReset;
   while LineTypeObj.FetchLineType (acadltype,description,winltype,isused) do
   begin
      writeln(Datei,acadltype+','+winltype);
   end;

   { Nun m�ssen die Layereinstellungen geschrieben werden }
   writeln(Datei,'[SELLAYER]');
   LayerObj.FetchReset;
   while LayerObj.FetchLayer (layer,linetype,color,ison,isused,isvisible) = TRUE do
   begin
      if (ison = TRUE) and (isused = TRUE) then layer:=layer + ',T'
      else layer:=layer + ',F';
      writeln(Datei,layer);
   end;

   { Nun m�ssen die Fonteinstellungen geschrieben werden }
   writeln(Datei,'[SELFONTS]');
   TextStyleObj.FetchReset;
   while TextStyleObj.FetchTextStyle (textstyle,fontfile,winfont,isused,isvert) do
   begin
      writeln(Datei,textstyle + ',' + winfont);
   end;
   CloseFile(Datei);
   {$I+}
end;

procedure TMainWnd.ReadConvertOptionsFromFile(Filename:string);
begin
   if Fileexists(Filename) then
   begin
      SelTextAttForm.SetAttributesFromFile(Filename);
      SellinetForm.SetAttributesFromFile(Filename);
      SelLayerForm.SetAttributesFromFile(Filename);
      SelfontsForm.SetAttributesFromFile(Filename);
   end;
end;

procedure TMainWnd.AddMappenBlattInfo(Layer:string;Area:double);
begin
   if DoDkmTest then
   begin
      if UseFilenameAsLayername then
         Layer:=CurrentFilenameLayer;
      AXImpExpDbc.AddImpInfo(Layer,'AREA',floattostr(Area));
      AXImpExpDbc.AddImpInfo(Layer,'MAPPENBLATT',MappenBlattInfo);
   end;
end;

procedure TMainWnd.CheckMappenBlattCanBeIgnored(Blockname:string; X:double; Y:double; Value:string);
var inifile:TIniFile;
    inifilename:string;
    MBSymbolname:string;
    MBLayer     :string;
    MBHeight    :double;
    MBWidth     :double;
    dummy       :string;
    count       :integer;
    dispatch    :IDispatch;
    aRect       :IRect;
    cnt         :integer;
    aSelObject  :Variant;
    MBSym_X     :double;
    MBSym_Y     :double;
    DoArchiveMappenBlatt:boolean;
    ProjMBlattNr:string;
    ProjMBlattDate:string;
    CurrMBlattNr:string;
    CurrMBlattDate:string;
    YearCurr     :integer;
    YearProj     :integer;
    MonthCurr    :integer;
    MonthProj    :integer;
    DayCurr      :integer;
    DayProj      :integer;
begin
   MappenBlattCanBeIgnored:=FALSE;
   DoArchiveMappenBlatt:=FALSE;
   if DoDkmTest then
   begin
      if not MappenBlattSymbolChecked then
      begin
         // check if the blockname is the mappenblatt symbol
         inifilename:=OSInfo.LocalAppDataDir+'dkmcomb.ini';
         inifile:=TIniFile.Create(inifilename);
         // get the mappenblatt symbol-name
         MBSymbolname:=inifile.ReadString('PARZELLENSETTINGS','MappenblattSymbol','');
         // get the layer on that the Mappenblatt-Info is stored
         MBLayer:=inifile.ReadString('PARZELLENSETTINGS','MappenblattLayer','');
         // get height and width of a MappenBlatt
         dummy:=inifile.ReadString('PARZELLENSETTINGS','MappenblattHeight',''); val(dummy,MBHeight,count); if count <> 0 then MBHeight:=0;
         dummy:=inifile.ReadString('PARZELLENSETTINGS','MappenblattWidth',''); val(dummy,MBWidth,count); if count <> 0 then MBWidth:=0;
         inifile.Destroy;
         if Blockname = MBSymbolname then
         begin
            // it�s a mappenblatt symbol
            // now check if this mappenblatt already exists
            MappenBlattSymbolChecked:=true;
            // get the Mappenblatt Text from the project
            // half the height and with of the mappenblatt a rectangle
            // will be made for selection
            MBSym_X:=0;
            MBSym_Y:=0;
            dispatch:=SelFilesForm.DestDocument.Layers.GetLayerByName(MBLayer);
            if dispatch <> nil then
            begin
               SelFilesForm.DestDocument.Layers.SetActiveLayer(MBLayer);
               // if the layer exists the selection can be done
               aRect:=SelFilesForm.DestDocument.CreateRect;
               // setup the Rect
               aRect.X1:=round((X-(MBWidth/2)) * 100);
               aRect.Y1:=round((Y-(MBHeight/2))* 100);
               aRect.X2:=round((X+(MBWidth/2)) * 100);
               aRect.Y2:=round((Y+(MBHeight/2))* 100);
               SelFilesForm.DestDocument.SelectByRect(aRect);
               // now evaluate the selection
               SelFilesForm.DestDocument.CreateSelectedList;
               for cnt:=0 to SelFilesForm.DestDocument.NrOfSelectedObjects-1 do
               begin
                  aSelObject:=SelFilesForm.DestDocument.SelItems[cnt];
                  if aSelObject.ObjectType <> 18 then // 18 is ot_layer
                  begin
                     if aSelObject.ObjectType = ot_Text then
                     begin
                        // check if it is the Mappenblatt Text
                        dummy:=aSelObject.Text;
                        if Pos('*',dummy) < 1 then // could not be a mappenblatt-text
                           continue;
                        // extract number and date of project MappenBlatt
                        ProjMBlattNr:=copy(dummy,1,Pos('*',dummy)-1);
                        delete(dummy,1,Pos('*',dummy));
                        ProjMBlattDate:=copy(dummy,1,Pos('*',dummy)-1);
                        // get the Version of the current to be imported
                        // Mappenblatt
                        dummy:=Value;
                        CurrMBlattNr:=copy(dummy,1,Pos('*',dummy)-1);
                        delete(dummy,1,Pos('*',dummy));
                        CurrMBlattDate:=copy(dummy,1,Pos('*',dummy)-1);
                        // check if we got the same mappenblatt
                        if CurrMBlattNr = ProjMBlattNr then
                        begin
                           MappenBlattCanBeIgnored:=TRUE;
                           DoArchiveMappenBlatt:=FALSE;
                           // check if the manipulation date of the current to
                           // be imported Mappenblatt is newer than the manipulation
                           // date of the Mappenblatt in the project
                           // in this case the Mappenblatt has to be updated

                           // now compare if the current imported mappenblatt is newer
                           dummy:=copy(CurrMBlattDate,1,4); val(dummy,YearCurr,count);  if count <> 0 then YearCurr:=0;
                           dummy:=copy(ProjMBlattDate,1,4); val(dummy,YearProj,count);  if count <> 0 then YearProj:=0;
                           dummy:=copy(CurrMBlattDate,5,2); val(dummy,MonthCurr,count); if count <> 0 then MonthCurr:=0;
                           dummy:=copy(ProjMBlattDate,5,2); val(dummy,MonthProj,count); if count <> 0 then MonthProj:=0;
                           dummy:=copy(CurrMBlattDate,7,2); val(dummy,DayCurr,count);   if count <> 0 then DayCurr:=0;
                           dummy:=copy(ProjMBlattDate,7,2); val(dummy,DayProj,count);   if count <> 0 then DayProj:=0;
                           if (YearProj < YearCurr) then // the current mappenblatt is newer -> it has to be reimported and the old one archived
                           begin
                              MappenBlattCanBeIgnored:=FALSE;
                              DoArchiveMappenBlatt:=TRUE;
                           end
                           else if (YearProj = YearCurr) and (MonthProj < MonthCurr) then
                           begin
                              MappenBlattCanBeIgnored:=FALSE;
                              DoArchiveMappenBlatt:=TRUE;
                           end
                           else if (YearProj = YearCurr) and (MonthProj = MonthCurr) and (DayProj < DayCurr) then
                           begin
                              MappenBlattCanBeIgnored:=FALSE;
                              DoArchiveMappenBlatt:=TRUE;
                           end;
                        end;
                     end
                     else if aSelObject.ObjectType = ot_Symbol then
                     begin
                        // check if it is the Mappenblatt Symbol
                        if aSelObject.Symbolname = MBSymbolname then
                        begin
                           // get the position of the Mappenblatt Symbol
                           // to make a cut if the Version of this Mappenblatt
                           // is older than the version of this Mappenblatt
                           MBSym_X:=aSelObject.Position.X/100;
                           MBSym_Y:=aSelObject.Position.Y/100;
                        end;
                     end;
                  end;
               end;
               // now all objects that have been selected can
               // be deselected
               SelFilesForm.DestDocument.DeselectAll;
               SelFilesForm.DestDocument.DestroySelectedList;
               if (DoArchiveMappenBlatt) and ((MBSym_X <> 0) or (MBSym_Y <> 0)) then
               begin
                  // calculate the rectangle that should be used
                  // to get the objects that should be archived
                  aRect.X1:=round(MBSym_X * 100);
                  aRect.Y1:=round(MBSym_Y * 100);
                  aRect.X2:=round((MBSym_X+MBWidth) * 100);
                  aRect.Y2:=round((MBSym_Y+MBHeight)* 100);
                  StatusLB.Items.Add(StrFileObj.Assign(212)); // Mappenblatt with elder version version exists and will be archived
                  StatusLB.ItemIndex := MainWnd.StatusLB.Items.Count-1;
                  StatusLB.Update;
                  CreateMappenBlattArchive(aRect, ProjMBlattDate,MBLayer,MBSymbolname);
               end;
            end;
         end;
      end;
      if (MappenBlattCanBeIgnored) then
      begin
         StatusLB.Items.Add(StrFileObj.Assign(211)); // Mappenblatt already exists and can be ignored
         StatusLB.ItemIndex := MainWnd.StatusLB.Items.Count-1;
         StatusLB.Update;
      end;
   end;
end;

procedure TMainWnd.CreateMappenBlattArchive(var aRect:IRect; ArchiveDate:string; MappenBlattLayer:string; MappenBlattSymbolname:string);
var ArchiveDbc     :TAxImpExpDbc;
    inifile        :TIniFile;
    iniSectionItems:TStrings;
    inicnt         :integer;
    selcnt         :integer;
    CurrLayername  :string;
    aSelObject     :OleVariant;
    old_id         :integer;
    new_id         :integer;
    column,info    :widestring;
    dispatch       :IDispatch;
    ArchiveLayer   :Variant;
    Colname        :Widestring;
    ColType        :TOleEnum;
    ColLength      :integer;
    layercnt       :integer;
    aLayer         :Variant;
    needtoarchive  :boolean;
    IniLayername   :string;
    ProjLayernames :array of string;
    numprojlayers  :integer;
    dummylayername :string;
    Objects2Delete :array of integer;
    NumObjects2Delete:integer;
begin
   // setup the database connection
   ArchiveDbc:=TAxImpExpDbc.Create(self);
   ArchiveDbc.WorkingDir:=OSInfo.WingisDir;
   ArchiveDbc.LngCode:=LngCode;
   ArchiveDbc.ImpDbMode:=SelFilesForm.Ori_ImpDbMode;
   ArchiveDbc.ImpDatabase:=SelFilesForm.Ori_ImpDatabase;
   ArchiveDbc.ImpTable:=SelFilesForm.Ori_ImpTable;
   ArchiveDbc.ExpDbMode:=SelFilesForm.Ori_ImpDbMode;
   ArchiveDbc.ExpDatabase:=SelFilesForm.Ori_ImpDatabase;
   ArchiveDbc.ExpTable:=SelFilesForm.Ori_ImpTable;
   ArchiveDbc.IgnoreNonExistExpTables:=TRUE;
   ArchiveDbc.OnRequestImpLayerIndex:=OnRequestArchiveImpLayerIndex;
   ArchiveDbc.OnRequestExpLayerIndex:=OnRequestArchiveImpLayerIndex;
   ArchiveDbc.OnRequestImpProjectName:=OnRequestArchiveImpProjectName;
   ArchiveDbc.OnRequestExpProjectName:=OnRequestArchiveImpProjectName;
   ArchiveDbc.OnRequestImpProjectGUIDs:=OnRequestArchiveImpProjectGUIDs;

   // First the Layer that contains the parzellen, the mappenblatt data
   // and the parzellen-symbols will be copied to archive layers
   // the name of the archive layers is <layername>+ArchiveDate
   // Also the database information has to be updated and ArchiveTables
   // will be created

   // the items that have to be archived are in the MAPPENBLATTARCHIVELAYERS
   // section of the dkmcomb.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'dkmcomb.ini');
   iniSectionItems:=TStringList.Create;
   inifile.ReadSection('MAPPENBLATTARCHIVELAYERS',iniSectionItems);

   // read out the layers from the project
   numprojlayers:=SelFilesForm.DestDocument.Layers.Count;
   SetLength(ProjLayernames, numprojlayers);
   for layercnt:=0 to SelFilesForm.DestDocument.Layers.Count-1 do
   begin
      aLayer:=SelFilesForm.DestDocument.Layers.Items[layercnt];
      CurrLayername:=aLayer.Layername;
      ProjLayernames[layercnt]:=CurrLayername;
   end;

   for layercnt:=0 to numprojlayers-1 do
   begin
      CurrLayername:=ProjLayernames[layercnt];
      // check if it is an archive layer, because this may not be deleted
      dummyLayername:=CurrLayername;
      needtoarchive:=FALSE;
      if pos('_',dummyLayername) <> 0 then
      begin
         dummyLayername:=copy(dummyLayername,1,pos('_',dummyLayername)-1);
         needtoarchive:=FALSE;
         for inicnt:=0 to iniSectionItems.Count-1 do
         begin
            IniLayername:=inifile.ReadString('MAPPENBLATTARCHIVELAYERS',iniSectionItems[inicnt],'');
            if dummyLayername=IniLayername then
            begin
               needtoarchive:=true;
               break;
            end;
         end;
      end;
      if needtoarchive then
         continue;

      // first it has to be checked if this layer has to be archived
      needtoarchive:=FALSE;
      for inicnt:=0 to iniSectionItems.Count-1 do
      begin
         IniLayername:=inifile.ReadString('MAPPENBLATTARCHIVELAYERS',iniSectionItems[inicnt],'');
         if CurrLayername=IniLayername then
         begin
            needtoarchive:=true;
            break;
         end;
      end;
      // now select the objects of this layer for this Mappenblatt
      SelFilesForm.DestDocument.Layers.SetActiveLayer(CurrLayername);
      SelFilesForm.DestDocument.SelectByRectTouch(aRect); // now select the objects where a part of it is in the rectangle
      SelFilesForm.DestDocument.CreateSelectedList;
      // set length for the objects 2 delete
      NumObjects2Delete:=SelFilesForm.DestDocument.NrOfSelectedObjects;
      SetLength(Objects2Delete, NumObjects2Delete);
      // reset objects 2 delete
      for selcnt:=0 to NumObjects2Delete-1 do
         Objects2Delete[selcnt]:=0;

      for selcnt:=0 to SelFilesForm.DestDocument.NrOfSelectedObjects-1 do
      begin
         aSelObject:=SelFilesForm.DestDocument.SelItems[selcnt];
         if (aSelObject.ObjectType = 18) then // 18 is ot_layer
         begin
            dummylayername:=aSelObject.LayerName;
            if dummylayername <> CurrLayername then
               continue;
            ArchiveDbc.CheckAndCreateExpTable(CurrLayername);
            if (needtoarchive) then
            begin
               // the layer properties have to be copied
               ArchiveLayer:=SelFilesForm.DestDocument.Layers.InsertLayerByName(CurrLayername+'_'+ArchiveDate,True);
               ArchiveLayer.FillStyle:=aSelObject.FillStyle;
               ArchiveLayer.GeneralLower:=aSelObject.GeneralLower;
               ArchiveLayer.GeneralUpper:=aSelObject.GeneralUpper;
               ArchiveLayer.LineStyle:=aSelObject.LineStyle;
               ArchiveLayer.Locked:=aSelObject.Locked;
               ArchiveLayer.SymbolFill:=aSelObject.SymbolFill;
               ArchiveLayer.Visible:=aSelObject.Visible;

               // if the layer should be archived an archive table has to be created
               ArchiveDbc.CheckAndCreateImpTable(CurrLayername+'_'+ArchiveDate);
               ArchiveDbc.AddImpIdColumn(CurrLayername+'_'+ArchiveDate);
               while ArchiveDbc.GetExportColumn(CurrLayername,colname,coltype,collength) do
               begin
                  ArchiveDbc.AddImpColumn(CurrLayername+'_'+ArchiveDate,colname,coltype,collength);
               end;
               ArchiveDbc.CreateImpTable(CurrLayername+'_'+ArchiveDate);
               ArchiveDbc.OpenImpTable(CurrLayername+'_'+ArchiveDate);
            end;
            ArchiveDbc.OpenExpTable(CurrLayername);
            continue;
         end
         else
         begin
            if dummyLayername <> CurrLayername then
               continue;
            old_id:=aSelObject.Index;
            if needtoarchive then
            begin
               // check if it is a symbol on the Mappenblattlayer
               // to avoid that Mappenblattsymbols from other Mappenblatts that
               // are around current Mappenblatt will be deleted
               if CurrLayername = MappenBlattLayer then
               begin
                  if (aSelObject.ObjectType = ot_Symbol) and
                     (aSelObject.SymbolName = MappenBlattSymbolname) then
                  begin
                     // a Mappenblatt Symbol will be processed
                     // if it is not the current Mappenblatt symbol it may not
                     // be archived and deleted
                     if not ((aRect.X1 = aSelObject.Position.X) and (aRect.Y1 = aSelObject.Position.Y)) then
                        continue;
                  end;
               end;
               // copy the object to the archive other layer
               new_id:=AxGisObjMan.CopyObjectToLayer(aSelObject,CurrLayername+'_'+ArchiveDate);
               Objects2Delete[selcnt]:=old_id;

               // copy the database information
               if (ArchiveDbc.DoExportData(CurrLayername, old_id)) then
               begin
                  ArchiveDbc.AddImpIDInfo(CurrLayername+'_'+ArchiveDate, new_id);
                  while ArchiveDbc.GetExportData(CurrLayername, column, info) do
                  begin
                     info:=TrimRight(info);
                     ArchiveDbc.AddImpInfo(CurrLayername+'_'+ArchiveDate,column,Info);
                  end;
               end;
            end;
            // the database information also can be deleted
            ArchiveDbc.DeleteExpDBEntry(CurrLayername,old_id);
         end;
      end;

      // now all objects that have been selected can
      // be deselected
      SelFilesForm.DestDocument.DeselectAll;
      SelFilesForm.DestDocument.DestroySelectedList;

      // delete the objects from the orginal layer
      for selcnt:=0 to NumObjects2Delete-1 do
      begin
         if Objects2Delete[selcnt] <> 0 then
            SelFilesForm.DestDocument.DeleteObjectByIndex(Objects2Delete[selcnt]);
      end;
   end;
   inifile.Destroy;
   iniSectionItems.Destroy;

   // close the database connection
   ArchiveDbc.CloseImpTables;
   ArchiveDbc.CloseExpTables;
   ArchiveDbc.Destroy;

   // display that it will be continued with reading the file
   StatusLB.Items.Add(StrFileObj.Assign(213)); // Reading of dxf-file will be continued
   StatusLB.ItemIndex := MainWnd.StatusLB.Items.Count-1;
   StatusLB.Update;
end;

procedure TMainWnd.FormShow(Sender: TObject);
begin
   AXImpExpDbc.visible:=FALSE;
   AxGisProjection.visible:=FALSE;
   AxGisObjMan.visible:=FALSE;
end;

procedure TMainWnd.AXImpExpDbcDialogSetuped(Sender: TObject);
begin
   SetdxffileWnd.DestDbLabel.Caption:=AXImpExpDbc.ImpDbInfo;
   SetdxffileWnd.PageControl.Enabled:=TRUE;
   SetdxffileWnd.GeoBtn.Enabled:=TRUE;
   SetdxffileWnd.DbBtn.Enabled:=TRUE;
   SetdxffileWnd.OkBtn.Enabled:=SetdxffileWnd.OkBtnMode;
   SetdxffileWnd.CancelBtn.Enabled:=TRUE;
end;

procedure TMainWnd.AxGisProjectionWarning(Sender: TObject;
  const Info: WideString);
begin
   if ImportActive=FALSE then
   begin
      ShowMessage(Info);
   end
   else
   begin
      StatusLB.Items.Add(Info);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
   end;
end;

procedure TMainWnd.AxGisProjectionError(Sender: TObject;
  const Info: WideString);
begin
   if ImportActive=FALSE then
   begin
      ShowMessage(Info);
   end
   else
   begin
      StatusLB.Items.Add(Info);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      hadaborted:=true;
      DXF2ASCObj.SetAbort;
      Cancel.Enabled:=false;
      Ok.Enabled:=true;
      SaveReportBtn.Enabled:=true;
   end;
end;

procedure TMainWnd.AXImpExpDbcRequestImpLayerIndex(Sender: TObject;
  const Layername: WideString; var LayerIndex: Integer);
var i:integer;
begin
   // the Layerindex of the Layername has to be evaluated
   for i:=0 to SelFilesForm.Document.Layers.Count-1 do
   begin
      if SelFilesForm.Document.Layers.Items[i].Layername = Layername then
      begin
         LayerIndex:=SelFilesForm.Document.Layers.Items[i].Index;
         break;
      end;
   end;
end;

procedure TMainWnd.OnRequestArchiveImpLayerIndex(Sender: TObject; const Layername: WideString; var LayerIndex: Integer);
var i:integer;
begin
   // the Layerindex of the Layername has to be evaluated
   for i:=0 to SelFilesForm.DestDocument.Layers.Count-1 do
   begin
      if SelFilesForm.DestDocument.Layers.Items[i].Layername = Layername then
      begin
         LayerIndex:=SelFilesForm.DestDocument.Layers.Items[i].Index;
         break;
      end;
   end;
end;

procedure TMainWnd.AXImpExpDbcRequestImpProjectName(Sender: TObject;
  var ProjectName: WideString);
var ProjName:string;
    aDisp:IDispatch;
begin
   aDisp:=SelFilesForm.Document;
   if aDisp <> nil then
      ProjName:=SelFilesForm.Document.Name
   else
      ProjName:=SelFilesForm.DestDocument.Name;
   ProjectName:=ProjName;
end;

procedure TMainWnd.AXImpExpDbcRequestImpProjectGUIDs(Sender: TObject;
  var StartGUID, CurrentGUID: WideString);
var aStartGUID, aCurrentGUID:string;
begin
   aStartGUID:=SelFilesForm.Document.StartProjectGUID;
   aCurrentGUID:=SelFilesForm.Document.CurrentProjectGUID;
   StartGUID:=aStartGUID;
   CurrentGUID:=aCurrentGUID;
end;

function TMainWnd.GetUserDefStyleByNumber(const idx:integer):string;
var retVal:string;
begin
   retVal:='';
   retVal:=SelFilesForm.Document.GetUserLineStyleNameByNumber(idx);
   result:=retVal;
end;

function TMainWnd.GetUserDefStyleByName(const aName:string):integer;
var retVal:integer;
begin
   retVal:=lt_Solid; // default is Solid
   retVal:=SelFilesForm.Document.GetUserLineStyleNumberByName(aName);
   result:=retVal;
end;

procedure TMainWnd.OnRequestArchiveImpProjectName(Sender: TObject;  var ProjectName: WideString);
var ProjName:string;
begin
   ProjName:=SelFilesForm.DestDocument.Name;
   ProjectName:=ProjName;
end;

procedure TMainWnd.OnRequestArchiveImpProjectGUIDs(Sender: TObject;  var StartGUID, CurrentGUID: WideString);
var aStartGUID, aCurrentGUID:string;
begin
   aStartGUID:=SelFilesForm.DestDocument.StartProjectGUID;
   aCurrentGUID:=SelFilesForm.DestDocument.CurrentProjectGUID;
   StartGUID:=aStartGUID;
   CurrentGUID:=aCurrentGUID;
end;

procedure TMainWnd.AXImpExpDbcWarning(Sender: TObject;
  const Info: WideString);
begin
   InterfaceObj.ShowWarning(Info);
end;

procedure TMainWnd.AXImpExpDbcInfo(Sender: TObject;
  const Info: WideString);
begin
   StatusLB.Items.Add(Info);
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;
end;

procedure TMainWnd.AXImpExpDbcShowPercent(Sender: TObject;
  Percent: Integer);
begin
   Prozessbalken.Progress := percent;
   MainWnd.HandleMessages;
end;

end.
