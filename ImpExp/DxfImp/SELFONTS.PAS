unit Selfonts;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Buttons,Main, ExtCtrls;

type
  TSelFontsForm = class(TForm)
    SrcLabel: TLabel;
    DstLabel: TLabel;
    ListACADFonts: TListBox;
    ListWinFonts: TListBox;
    ButCancel: TButton;
    WinFontLabel: TLabel;
    ButBegin: TBitBtn;
    ButPrev: TBitBtn;
    ButNext: TBitBtn;
    ButEnd: TBitBtn;
    Panel1: TPanel;
{    function FontFileExists ( fontfile : string ) : boolean; }
    procedure FillLists;
    procedure ListACADFontsClick(Sender: TObject);
    function FindWinFont ( winfont : string ) : integer;
    procedure ListWinFontsClick(Sender: TObject);
    procedure DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure ListWinFontsMeasureItem(Control: TWinControl; Index: Integer;
      var Height: Integer);
    procedure FormCreate(Sender: TObject);
    procedure ButCancelClick(Sender: TObject);
    procedure ButBeginClick(Sender: TObject);
    procedure ButPrevClick(Sender: TObject);
    procedure ButNextClick(Sender: TObject);
    procedure ButEndClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    procedure SetAttributesFromFile(Filename:string);
  end;

var
  SelFontsForm: TSelFontsForm;

implementation

uses MainDlg;

{$R *.DFM}

{function TSelFontsForm.FontFileExists ( fontfile : string ) : boolean;
var
  loop : integer;
begin
  Result := FALSE;
  for loop := 0 to ListACADFonts.Items.Count - 1 do
  begin
    if ListACADFonts.Items[loop] = fontfile then begin Result := TRUE; break; end;
  end
end; }

procedure TSelFontsForm.FillLists;
var
  textstyle,fontfile,winfont : string;
  isused,isvert : boolean;
begin
  ListACADFonts.Clear; ListWinFonts.Clear;
  TextStyleObj.FetchReset;
  while TextStyleObj.FetchTextStyle (textstyle,fontfile,winfont,isused,isvert) do
  begin
    ListACADFonts.Items.Add (textstyle);
  end;
  ListWinFonts.Items := Screen.Fonts;
  if ListACADFonts.Items.Count > 0 then
  begin
    ListACADFonts.ItemIndex := 0;
    textstyle := ListACADFonts.Items[ListACADFonts.ItemIndex];
    TextStyleObj.GetWinFontFromTextStyle (textstyle,winfont);
    ListWinFonts.ItemIndex := FindWinFont (winfont);
    WinFontLabel.Caption := winfont;
  end;
end;

procedure TSelFontsForm.SetAttributesFromFile(Filename:string);
var Datei:Textfile;
    Zeile:string;
    textstyle,winfont:string;
begin
   {$I-}
   Assignfile(Datei,Filename);
   Reset(Datei);
   { Zuerst mu� die Section mit den Eintr�gen f�r die Textattribute gefunden werden }
   repeat
      readln(Datei,Zeile);
   until ((Zeile = '[SELFONTS]') or (EOF(Datei)));

   if (Zeile <> '[SELFONTS]') then exit;
   repeat
      readln(Datei,Zeile);
      if (not Eof(Datei)) then
      begin
         textstyle:=copy(Zeile,1,pos(',',Zeile)-1);
         winfont:=copy(Zeile,pos(',',Zeile)+1,length(Zeile));
         TextStyleObj.SetWinFont (textstyle,winfont);
      end;
   until (Eof(Datei));
   Closefile(Datei);
   {$I+}
end;

function TSelFontsForm.FindWinFont ( winfont : string ) : integer;
var
  loop : integer;
begin
  Result := -1;
  for loop := 0 to ListWinFonts.Items.Count - 1 do
  begin
    if ListWinFonts.Items[loop] = winfont then
    begin Result := loop; break; end;
  end
end;

procedure TSelfontsForm.ListACADFontsClick(Sender: TObject);
var
  textstyle,winfont : string;
begin
  if ListACADFonts.ItemIndex <> -1 then
  begin
    textstyle := ListACADFonts.Items[ListACADFonts.ItemIndex];
    TextStyleObj.GetWinFontFromTextStyle (textstyle,winfont);
    ListWinFonts.ItemIndex := FindWinFont (winfont);
    WinFontLabel.Caption := winfont;
  end;
end;

procedure TSelfontsForm.ListWinFontsClick(Sender: TObject);
var
  textstyle,winfont : string;
begin
  if ListACADFonts.ItemIndex <> -1 then
  begin
    textstyle := ListACADFonts.Items[ListACADFonts.ItemIndex];
    winfont := ListWinFonts.Items[ListWinFonts.ItemIndex];
    TextStyleObj.SetWinFont (textstyle,winfont);
  end;
  WinFontLabel.Caption := ListWinFonts.Items[ListWinFonts.ItemIndex];
end;

procedure TSelfontsForm.DrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  with ListWinFonts.Canvas do
  begin
    FillRect(Rect);
    Font.Name := ListWinFonts.Items[Index];
    TextOut(Rect.Left, Rect.Top, ListWinFonts.Items[Index]);
  end;
end;

procedure TSelfontsForm.ListWinFontsMeasureItem(Control: TWinControl;
  Index: Integer; var Height: Integer);
begin
  with ListWinFonts.Canvas do
  begin
    Font.Name := ListWinFonts.Items[Index];
    Height := TextHeight('A');
  end;
end;

procedure TSelfontsForm.FormCreate(Sender: TObject);
begin
  Caption := StrFileObj.Assign (91);
  ButCancel.Caption := StrFileObj.Assign (2);
  SrcLabel.Caption := StrFileObj.Assign (92);
  DstLabel.Caption := StrFileObj.Assign (93);
end;

procedure TSelfontsForm.ButCancelClick(Sender: TObject);
begin
  if InterfaceObj.DoReallyQuit then ModalResult := mresABORT;
end;

procedure TSelfontsForm.ButBeginClick(Sender: TObject);
begin
  ModalResult := mresBEGIN;
end;

procedure TSelfontsForm.ButPrevClick(Sender: TObject);
begin
  ModalResult := mresPREV;
end;

procedure TSelfontsForm.ButNextClick(Sender: TObject);
begin
  ModalResult := mresNEXT;
end;

procedure TSelfontsForm.ButEndClick(Sender: TObject);
begin
  ModalResult := mresEND;
end;

procedure TSelFontsForm.FormShow(Sender: TObject);
begin
   FillLists;
   ButEnd.SetFocus;
end;

end.
