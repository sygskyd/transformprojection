object MainWnd: TMainWnd
  Left = 222
  Top = 121
  BorderStyle = bsDialog
  Caption = 'PROGIS AMP-Import 2000'
  ClientHeight = 436
  ClientWidth = 570
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'System'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  Position = poDefault
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 20
  object Panel1: TPanel
    Left = 10
    Top = 10
    Width = 551
    Height = 381
    Caption = 'Panel1'
    TabOrder = 0
    object Progresslabel: TLabel
      Left = 10
      Top = 10
      Width = 119
      Height = 16
      Caption = 'Progressmessages'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Prozessbalken: TGauge
      Left = 10
      Top = 320
      Width = 531
      Height = 25
      Progress = 0
    end
    object StatusLB: TListBox
      Left = 10
      Top = 30
      Width = 531
      Height = 241
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 16
      ParentFont = False
      TabOrder = 0
    end
    object SaveReportBtn: TButton
      Left = 190
      Top = 280
      Width = 181
      Height = 31
      Caption = 'Save report'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = SaveReportBtnClick
    end
  end
  object OKBtn: TButton
    Left = 310
    Top = 400
    Width = 116
    Height = 29
    Caption = 'OK'
    Default = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 435
    Top = 400
    Width = 126
    Height = 29
    Cancel = True
    Caption = 'Abbrechen'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 2
    OnClick = CancelBtnClick
  end
  object AXImpExpDbcObj: TAXImpExpDbc
    Left = 10
    Top = 400
    Width = 41
    Height = 31
    ParentFont = False
    TabOrder = 3
    OnDialogSetuped = AXImpExpDbcObjDialogSetuped
    OnRequestImpLayerIndex = AXImpExpDbcObjRequestImpLayerIndex
    OnRequestExpLayerIndex = AXImpExpDbcObjRequestExpLayerIndex
    OnRequestImpProjectName = AXImpExpDbcObjRequestImpProjectName
    OnRequestExpProjectName = AXImpExpDbcObjRequestExpProjectName
    OnRequestImpProjectGUIDs = AXImpExpDbcObjRequestImpProjectGUIDs
    ControlData = {
      54504630065450616E656C00044C656674020A03546F70039001055769647468
      022906486569676874021F0743617074696F6E06034462630C466F6E742E4368
      6172736574070C414E53495F434841525345540A466F6E742E436F6C6F72070C
      636C57696E646F77546578740B466F6E742E48656967687402EF09466F6E742E
      4E616D65060D4D532053616E732053657269660A466F6E742E5374796C650B00
      0A506172656E74466F6E74080000}
  end
  object AxGisProjection: TAxGisProjection
    Left = 60
    Top = 400
    Width = 63
    Height = 31
    ParentFont = False
    TabOrder = 4
    OnWarning = AxGisProjectionWarning
    OnError = AxGisProjectionError
    ControlData = {
      54504630065450616E656C00044C656674023C03546F70039001055769647468
      023F06486569676874021F0743617074696F6E060647697350726F0C466F6E74
      2E43686172736574070C414E53495F434841525345540A466F6E742E436F6C6F
      72070C636C57696E646F77546578740B466F6E742E48656967687402EF09466F
      6E742E4E616D65060D4D532053616E732053657269660A466F6E742E5374796C
      650B000A506172656E74466F6E74080000}
  end
  object AXGisObjMan: TAXGisObjMan
    Left = 130
    Top = 400
    Width = 63
    Height = 31
    ParentFont = False
    TabOrder = 5
    ControlData = {
      54504630065450616E656C00044C65667403820003546F700390010557696474
      68023F06486569676874021F0743617074696F6E06064769734D616E0C466F6E
      742E43686172736574070C414E53495F434841525345540A466F6E742E436F6C
      6F72070C636C57696E646F77546578740B466F6E742E48656967687402EF0946
      6F6E742E4E616D65060D4D532053616E732053657269660A466F6E742E537479
      6C650B000A506172656E74466F6E74080000}
  end
  object CloseTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = CloseTimerTimer
    Left = 160
    Top = 320
  end
  object SaveDialogRep: TSaveDialog
    DefaultExt = '*.rep'
    FileName = '*.rep'
    Filter = 'Import report file|*.rep'
    Left = 56
    Top = 232
  end
  object SetupWindowTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = SetupWindowTimerTimer
    Left = 216
    Top = 320
  end
end
