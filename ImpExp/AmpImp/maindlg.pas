unit Maindlg;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Strfile, StdCtrls, Buttons, ExtCtrls, Gauges, Db,
  DBTables,Windows, OleCtrls, AXImpExpDbcXControl_TLB, AxGisPro_TLB,
  AXGisMan_TLB, AxDll_TLB, WinOSInfo;

const
       // import modes
       ImportMode_Normal = 0;
       ImportMode_DKM    = 1;
       ImportMode_Batch  = 2;

       ShowMode_Show     = 0;
       ShowMode_Hide     = 1;

       //registration modes
       NO_LICENCE        = 0;
       FULL_LICENCE      = 1;
       DEMO_LICENCE      = 2;

       // Object types
       ot_Pixel     = 1;
       ot_Poly      = 2;
       ot_LabelText = 3;
       ot_CPoly     = 4;
       ot_Group     = 5;
       ot_Proj      = 6;
       ot_Text      = 7;
       ot_Symbol    = 8;
       ot_Spline    = 9;
       ot_Image     = 10;
       ot_MesLine   = 11;
       ot_Circle    = 12;
       ot_Arc       = 13;
       ot_OLEObj    = 14;
       ot_SymDef    = 15;
       ot_RText     = 16;
       ot_BusGraph  = 17;

       //TextSTyles
       ts_Left           =  0;
       ts_Center         =  1;
       ts_Right          =  2;
       ts_Italic         =  4;
       ts_Bold           =  8;
       ts_Underl         = 16;
       ts_FixHeight      = 32;
       ts_Transparent    = 64;

       //LineTypes
       lt_Solid     = 0;
       lt_Dash      = 1;
       lt_Dot       = 2;
       lt_DashDot   = 3;
       lt_DashDDot  = 4;
       lt_UserDefined    = 1000;

       //FillTypes
       pt_NoPattern = 0;
       pt_FDiagonal = 1;
       pt_Cross     = 2;
       pt_DiagCross = 3;
       pt_BDiagonal = 4;
       pt_Horizontal= 5;
       pt_Vertical  = 6;
       pt_Solid     = 7;
       pt_UserDefined = 8;

       stDrawingUnits    = 0;
       stScaleInDependend= 1;
       stProjectScale    = 2;
       stPercent         = 3;

type
  TMainWnd = class(TForm)
    CloseTimer: TTimer;
    Panel1: TPanel;
    Progresslabel: TLabel;
    Prozessbalken: TGauge;
    StatusLB: TListBox;
    SaveReportBtn: TButton;
    OKBtn: TButton;
    CancelBtn: TButton;
    SaveDialogRep: TSaveDialog;
    SetupWindowTimer: TTimer;
    AXImpExpDbcObj: TAXImpExpDbc;
    AxGisProjection: TAxGisProjection;
    AXGisObjMan: TAXGisObjMan;
    procedure FormCreate(Sender: TObject);
    procedure StartBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure CloseTimerTimer(Sender: TObject);
    procedure SaveReportBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AXImpExpDbcObjDialogSetuped(Sender: TObject);
    procedure AxGisProjectionWarning(Sender: TObject;
      const Info: WideString);
    procedure AxGisProjectionError(Sender: TObject;
      const Info: WideString);
    procedure AXImpExpDbcObjRequestExpProjectName(Sender: TObject;
      var ProjectName: WideString);
    procedure AXImpExpDbcObjRequestImpProjectName(Sender: TObject;
      var ProjectName: WideString);
    procedure AXImpExpDbcObjRequestImpLayerIndex(Sender: TObject;
      const Layername: WideString; var LayerIndex: Integer);
    procedure AXImpExpDbcObjRequestExpLayerIndex(Sender: TObject;
      const Layername: WideString; var LayerIndex: Integer);
    procedure AXImpExpDbcObjRequestImpProjectGUIDs(Sender: TObject;
      var StartGUID, CurrentGUID: WideString);
  private
    { Private-Deklarationen }
    StrFileObj :ClassStrfile;
    Pass       :integer;

    Bytes2work  :longint;
    Bytesworked :longint;
    DatabaseEntries2write:longint;
    DatabaseEntrieswritten:longint;
    Aktiv       :boolean;
    Abort       :boolean;
    FirstActivate:boolean;
    ObjectsImported :longint;

    NewSymbolDef   :ISymbolDef;
    SymbolDefMap   :array of integer;
    SymbolDefMapCnt:integer;

    FileStartTime:TDateTime;
    TabFile      :TextFile;

    UsedFonts    :TStrings;
    UnknownFonts :TStrings;

    // counters for statistics
    NumPixels    :integer;
    NumCPolys    :integer;
    NumPolys     :integer;
    NumSplines   :integer;
    NumArcs      :integer;
    NumTexts     :integer;
    NumCircles   :integer;
    NumSymbols   :integer;
    NumImages    :integer;
    NumLayers    :integer;
    NumViews     :integer;
    NumSymbolDefs:integer;
    NumMesLines  :integer;
    NumIgnoredObjects:integer;

    procedure StartImport;
    function  CalculateElementsToImport:integer;
    procedure Showpercent;
    procedure ShowPercentWithValues(Elems2Work:integer; ElemsWorked:integer);
    procedure RunPass(AktPass:integer);
    procedure ExecutePass;
    procedure CopySymbolLibrary;
    procedure CopyUserStyles;

    procedure ImportSymbolObject(var aObject:IBase);
    procedure ImportObject(var aObject:IBase; var aLayer:ILayer; const aLayername:string);
    function  CopyObject(var oldObject:IBase; var newObject:IBase; issymbol:boolean):boolean;
    function  CopyCPoly(var OldObject:IBase; var NewObject:IBase;issymbol:boolean):boolean;
    procedure CopyDataBaseStructure(Layername:string);

    procedure CopyViews;
    function  GetNewLayerIndex(aIndex:integer):integer;
    procedure CheckLayerStyles(var NewLayer:ILayer; var OldLayer:ILayer);
    procedure DisplayStatistics;
    function  GetFontToUse(const aFontname:string):string;
  public
    { Public-Deklarationen }
    App          :Variant;    // Pointer to external DX-Handle
    WinGISApp    :Variant;    // Pointer to WinGIS AX-Handle
    DestDocument :IDocument;  // Pointer to Project into that should be imported
    OldDocument  :IDocument;  // Pointer to Project that should be imported
    LastOpenendAmpFilename:string;
    GisHandle    :integer;

    SourceAmpfilename:string; { Name of the Project that sould be imported }

    Importmode   :integer;   { Defines the Importmode (DKM, Batch or Normal  }
    ShowMode     :integer;   // Defines if the Window should be shown during import

    RegMode      :integer;
    LngCode      :string;
    ExitNormal   :boolean;

    ImagePath           :string;
    IgnoreNotFoundImages:boolean;
    AmpOpened           :boolean;

    { Procedures that are used to handle AX-Control }
    function  CreateLayer(aLayer:ILayer):ILayer;
    procedure ImportObjects;
    function  CheckLayerExists(const aLayername:string;var aLayer:ILayer):boolean;
    procedure SetProjectionSettings;
    procedure CheckProjectionSettings;
    procedure CreateStringFile;
  end;

var
  MainWnd: TMainWnd;

implementation

uses comobj, SelAmpDlg, SetImgDlg;

{$R *.DFM}

procedure TMainWnd.CreateStringFile;
begin
   StrFileObj:=ClassStrfile.Create;
   StrFileObj.CreateList(OSInfo.WingisDir,LngCode,'AMPIMP');

   Self.Caption:=StrFileObj.Assign(1);           { PROGIS AMP-Import 2000 }
   ProgressLabel.Caption:=StrFileObj.Assign(14); { Process messages }
   SaveReportBtn.Caption:=StrFileObj.Assign(15); { Save report      }
   OkBtn.Caption:=StrFileObj.Assign(12);         { Ok }
   CancelBtn.Caption:=StrFileObj.Assign(13);     { Cancel }
end;

procedure TMainWnd.FormCreate(Sender: TObject);
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];

   Self.Height:=1;
   Self.Width:=1;
   Self.Update;
   Aktiv:=false;
   FirstActivate:=true;

   Importmode:=ImportMode_Normal;
   ShowMode:=ShowMode_Show;

   App:=CreateoleObject('AXDLL.Core');

   ObjectsImported:=0;
   ImagePath:='';
   IgnoreNotFoundImages:=false;
   LastOpenendAmpFilename:='';

   NumPixels:=0;
   NumCPolys:=0;
   NumPolys:=0;
   NumSplines:=0;
   NumArcs:=0;
   NumTexts:=0;
   NumCircles:=0;
   NumSymbols:=0;
   NumImages:=0;
   NumLayers:=0;
   NumViews:=0;
   NumSymbolDefs:=0;
   NumMesLines:=0;
   NumIgnoredObjects:=0;
   UsedFonts:=nil;
   UnknownFonts:=nil;

   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

{*******************************************************************************
* PROZEDUR : StartBtnClick                                                     *
********************************************************************************
* Prozedur wird aufgerufen wenn der Start-Button im MainWnd                    *
* gedr�ckt wurde. Damit wird die Konvertierung gestartet.                      *
*                                                                              *
********************************************************************************}
procedure TMainWnd.StartBtnClick(Sender: TObject);
begin
   RunPass(1);
end;

function TMainWnd.CalculateElementsToImport:integer;
var numelems:integer;
    layercnt:integer;
    ALayer:ILayer;
    i:integer;
    Layername:string;
    found:boolean;
    doimportlayer:boolean;
begin
   numelems:=0;
   for layercnt:=0 to OldDocument.Layers.Count-1 do
   begin
      ALayer:=OldDocument.Layers.Items[layercnt];
      { Nun wird �berpr�ft ob der Layer importiert werden darf }
      if Importmode = ImportMode_DKM then doimportlayer:=true
      else
      begin
         if (SelAmpWnd.LayerCB.Checked) then
         begin
            Layername:=ALayer.Layername; doimportlayer:=false;
            for i:=0 to SelAmpWnd.LayerLB.Items.Count-1 do
            begin
               if (SelAmpWnd.LayerLB.Items[i] = Layername) and (SelAmpWnd.LayerLB.Selected[i]) then
               begin
                  doimportlayer:=true;
                  break;
               end;
            end;
         end
         else
            doimportlayer:=true;
      end;
      if doimportlayer then numelems:=numelems+ALayer.count;
   end;
   result:=numelems;
end;

procedure TMainWnd.CopySymbolLibrary;
var i,j:integer;
    OldSymbolDef   :ISymbolDef;
    ASymbolDef     :ISymbolDef;
    AObject        :IBase;
    found          :boolean;
    aDisp          :IDispatch;
    OldSymbolDefIdx:integer;
    Elems2Work     :integer;
begin
   SymbolDefMapCnt:=0;
   Elems2Work:=OldDocument.Symbols.Count-1;

   StatusLB.Items.Add(StrFileObj.Assign(78)); // Import Symbol-Library
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;

   for i:=0 to OldDocument.Symbols.Count-1 do
   begin
      if OldDocument.Symbols.Count = 0 then break;
      ADisp:=OldDocument.Symbols.Items[i];
      OldSymbolDef:=ADisp as ISymbolDef;

      OldSymbolDefIdx:=OldSymboldef.Index - 600000000;
      if SymbolDefMapCnt < OldSymbolDefIdx+1 then
         SetLength(SymbolDefMap,OldSymbolDefIdx+1);

      // check if symbol exists.
      found:=false;
      for j:=0 to DestDocument.Symbols.Count-1 do
      begin
         ASymbolDef:=DestDocument.Symbols.Items[j];
         if ASymbolDef.Name = OldSymbolDef.Name then
         begin
            SymbolDefMap[OldSymbolDefIdx]:=ASymbolDef.Index;
            found:=true;
            break;
         end;
      end;
      if not found then
      begin
         // the symbol has to be created in the new project
         NumSymbolDefs:=NumSymbolDefs+1;
         NewSymbolDef:=DestDocument.CreateSymbolDef;
         NewSymbolDef.Name:=OldSymbolDef.Name;
         NewSymbolDef.DefaultSizeType:=OldSymbolDef.DefaultSizeType;
         NewSymbolDef.DefaultSize:=OldSymbolDef.DefaultSize;
         // copy the definition line
         NewSymbolDef.RefLine.X1:=OldSymbolDef.RefLine.X1;
         NewSymbolDef.RefLine.Y1:=OldSymbolDef.RefLine.Y1;
         NewSymbolDef.RefLine.X2:=OldSymbolDef.RefLine.X2;
         NewSymbolDef.RefLine.Y2:=OldSymbolDef.RefLine.Y2;
         NewSymbolDef.RefPoint.x:=OldSymbolDef.RefPoint.x;
         NewSymbolDef.RefPoint.y:=OldSymbolDef.RefPoint.y;


         // now the elements of the Symboldefinition have to be copied
         for j:=0 to OldSymbolDef.Count-1 do
         begin
            AObject:=OldSymbolDef.Items[j];
            ImportSymbolObject(AObject);
         end;

         // insert new symbol
         DestDocument.Symbols.InsertSymbolDef(NewSymbolDef);
         SymbolDefMap[OldSymbolDefIdx]:=NewSymbolDef.Index;
      end;
      ShowPercentWithValues(Elems2Work, i);
      if Abort then
         break;
   end;
end;

procedure TMainWnd.ImportSymbolObject(var aObject:IBase);
var newObject:IBase;
begin
   // import symbol-definition
   if aObject = nil then exit;
   if CopyObject(aObject, newObject, TRUE {is for symbols} ) then
      NewSymbolDef.InsertSymbolItem(newObject);
end;

procedure TMainWnd.ImportObject(var aObject:IBase; var aLayer:ILayer; const aLayername:string);
var newObject:IBase;
    oldIndex :integer;
    newIndex :integer;
    ColName,ColInfo:widestring;
    HasDuplicate:boolean;
    DuplId:integer;
    aline:string;
begin
    if aObject = nil then exit;
    if CopyObject(aObject, newObject, FALSE) then
    begin
       newObject.GUID:=aObject.GUID;
       aLayer.InsertObject(newObject);
       oldIndex:=aObject.Index;
       newIndex:=newObject.Index;
       HasDuplicate:=FALSE;
       if SelAmpWnd.IgnoreDuplicatesCb.Checked then
       begin
          // check if the object has a duplicate
          DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(newIndex, aLayername);
          if DuplId > 0 then
          begin
             // remove the object
             DestDocument.DeleteObjectByIndex(newIndex);
             newIndex:=DuplId;
             HasDuplicate:=TRUE;
             NumIgnoredObjects:=NumIgnoredObjects+1;
          end;
       end;

       // now the database information will be evaluated
       if oldIndex > 0 then
       begin
          // Check if the Id�s should be written to a Tab-seperated File
          if SelAmpWnd.WriteDbTabFilename <> '' then
          begin
             aline:=aLayername+chr(9)+inttostr(oldIndex)+chr(9)+inttostr(newIndex);
             {$I-}
             writeln(TabFile,aline);
             {$I+}
          end;
          if not HasDuplicate then
          begin
             if (aObject.ObjectType = ot_Pixel) then
                NumPixels:=NumPixels+1
             else if (aObject.ObjectType = ot_Cpoly) then
                NumCPolys:=NumCPolys+1
             else if (aObject.ObjectType = ot_poly) then
                NumPolys:=NumPolys+1
             else if (aObject.ObjectType = ot_Spline) then
                NumSplines:=NumSplines+1
             else if (aObject.ObjectType = ot_Arc) then
                NumArcs:=NumArcs+1
             else if (aObject.ObjectType = ot_Text) then
                NumTexts:=NumTexts+1
             else if (aObject.ObjectType = ot_Circle) then
                NumCircles:=NumCircles+1
             else if (aObject.ObjectType = ot_Symbol) then
                NumSymbols:=NumSymbols+1
             else if (aObject.ObjectType = ot_Image) then
                NumImages:=NumImages+1
             else if (aObject.ObjectType = ot_MesLine) then
                NumMeslines:=NumMeslines+1;
          end;

          // now copy the database information
          if AXImpExpDbcObj.ExpDbMode = 0 then exit;
          if AXImpExpDbcObj.DoExportData(aLayername,oldIndex) then
          begin
             AxImpExpDbcObj.AddImpIdInfo(aLayername, newIndex);
             while AXImpExpDbcObj.GetExportData(aLayername, ColName, ColInfo) do
             begin
                AxImpExpDbcObj.AddImpInfo(aLayername, ColName, ColInfo);
             end;
          end;
       end;
    end;
end;

procedure TMainWnd.ShowPercentWithValues(Elems2Work:integer; ElemsWorked:integer);
var Prozent:integer;
begin
   if Elems2Work > 0 then Prozent:= Round (ElemsWorked / Elems2Work * 100 ) else Prozent:= 100;
   ProzessBalken.Progress :=Prozent;
   ProzessBalken.Update;
   Application.Processmessages;
end;

procedure TMainWnd.ImportObjects;
var layercnt:integer;
    ALayer:ILayer;
    New_Layer:ILayer;
    oldObject:IBase;
    i,cnt:integer;
    Layername:string;
    doimportlayer:boolean;
    MayImportObject:boolean;
begin
   MayImportObject:=true;
   // iterate through all layers
   for layercnt:=0 to OldDocument.Layers.Count-1 do
   begin
      if Abort then exit;
      if not MayImportObject then exit;
      ALayer:=OldDocument.Layers.Items[layercnt];
      doimportlayer:=false;
      // check if layer may be imported
      if Importmode = ImportMode_DKM then doimportlayer:=true
      else
      begin
         if (SelAmpWnd.LayerCB.Checked) then
         begin
            Layername:=ALayer.Layername; doimportlayer:=false;
            for i:=0 to SelAmpWnd.LayerLB.Items.Count-1 do
            begin
               if (SelAmpWnd.LayerLB.Items[i] = Layername) and (SelAmpWnd.LayerLB.Selected[i]) then
               begin
                  doimportlayer:=true;
                  break;
               end;
            end;
         end
         else
            doimportlayer:=true;
      end;

      if doimportlayer then
      begin
         if not CheckLayerExists(ALayer.Layername,New_Layer) then
            New_Layer:=CreateLayer(ALayer);
         // now it will be checked if there are user defined Line/Fill
         // styles used and they will be mapped
         CheckLayerStyles(New_Layer, ALayer);

         Layername:=New_Layer.Layername;

         StatusLB.Items.Add(StrfileObj.Assign(58)+Layername); // importing project data from layer:
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         Application.Processmessages;

         // copy the database structure
         CopyDatabaseStructure(Layername);

         // iterate through objects of layer
         for cnt:=0 to ALayer.count-1 do
         begin
            if Abort then break;
            if ALayer.count = 0 then break;
            oldObject:=ALayer.Items[cnt];
            ImportObject(oldObject, New_layer, Layername);
            Bytesworked:=Bytesworked+1;
            ShowPercent;
            ObjectsImported:=ObjectsImported+1;

            // check if in demomode
            if (RegMode = DEMO_LICENCE) and (ObjectsImported = 100) then
            begin
               StatusLB.Items.Add(StrfileObj.Assign(47));  // 100 objects imported, following objects will be ignored
               StatusLB.ItemIndex := StatusLB.Items.Count-1;
               StatusLB.Update;
               MayImportObject:=false;
               break;
            end;
         end;
         AXImpExpDbcObj.CloseImpTables;
         AXImpExpDbcObj.CloseExpTables;
      end;
   end;
end;

procedure TMainWnd.CopyViews;
var ViewX:double;
    ViewY:double;
    aSight:ISight;
    i,j:integer;
    newSight:ISight;
    aViewLData:ISightLData;
    LayerIndex:integer;
    exists    :boolean;
begin
   for i:=0 to OldDocument.Sights.Count-1 do
   begin
      aSight:=OldDocument.Sights.Items[i];
      ViewX:=aSight.X/100;
      ViewY:=aSight.Y/100;
      AxGisProjection.Calculate(ViewX, ViewY);

      // first check if a View with that name does not already exist
      exists:=FALSE;
      for j:=0 to DestDocument.Sights.Count-1 do
      begin
         if DestDocument.Sights.Items[j].Name = aSight.Name then
         begin
            exists:=TRUE;
            break;
         end;
      end;
      if exists then // if the View already exist it need not to be created
         continue;

      NumViews:=NumViews+1;

      newSight:=DestDocument.CreateSight(aSight.Name, ViewX*100, ViewY*100,
                                         aSight.Height, aSight.Width,
                                         aSight.Rotation);
      newSight.Description:=aSight.Description;

      // copy the layer-data
      for j:=0 to aSight.Count -1 do
      begin
         aViewLData:=aSight.SightLData[j];
         LayerIndex:=GetNewLayerIndex(aViewLData.LIndex);
         if (j = 0) then
         begin
            // add dummy information for Active-Layer that has been created
            newSight.InsertSightLData(0, aViewLData.LFlags, aViewLData.GeneralMin, aViewLData.GeneralMax);
         end;
         if LayerIndex <> -1 then
         begin
            newSight.InsertSightLData(LayerIndex, aViewLData.LFlags, aViewLData.GeneralMin, aViewLData.GeneralMax);
         end;
      end;
      DestDocument.Sights.InsertSight(newSight);
   end;
end;

function TMainWnd.GetNewLayerIndex(aIndex:integer):integer;
var Layername:string;
    retVal   :integer;
    found    :boolean;
    i:integer;
begin
   retVal:=-1;
   // first the Name of the Layer has to be got
   found:=FALSE;
   for i:=0 to OldDocument.Layers.Count-1 do
   begin
      Layername:=OldDocument.Layers.Items[i].LayerName;
      if (OldDocument.Layers.Items[i].Index = aIndex) then
      begin
         found:=TRUE;
         break;
      end;
   end;
   if not found then
   begin
      result:=retVal;
      exit;
   end;

   // now the new index in the DestinationDocument has to be found
   for i:=0 to DestDocument.Layers.Count-1 do
   begin
      if (Layername = DestDocument.Layers.Items[i].LayerName) then
      begin
         retVal:=DestDocument.Layers.Items[i].Index;
         break;
      end;
   end;
   result:=retVal;
end;

procedure TMainWnd.CheckLayerStyles(var NewLayer:ILayer; var OldLayer:ILayer);
var StyleName:string;
    OldStyleNumber:integer;
    NewStyleNumber:integer;
begin
   if OldLayer.Linestyle.Style >= lt_UserDefined then
   begin
      // old Layer has user defined Line Style
      OldStyleNumber:=OldLayer.Linestyle.Style;
      StyleName:=OldDocument.GetUserLineStyleNameByNumber(OldStyleNumber);
      NewStyleNumber:=DestDocument.GetUserLineStyleNumberByName(StyleName);
      NewLayer.Linestyle.Style:=NewStyleNumber;
   end;
   if OldLayer.Fillstyle.Pattern >= pt_UserDefined then
   begin
      // old Layer has user defined Fill Style
      OldStyleNumber:=OldLayer.Fillstyle.Pattern;
      StyleName:=OldDocument.GetUserFillStyleNameByNumber(OldStyleNumber);
      NewStyleNumber:=DestDocument.GetUserFillStyleNumberByName(StyleName);
      NewLayer.Fillstyle.Pattern:=NewStyleNumber;
   end;
end;

procedure TMainWnd.CopyDataBaseStructure(Layername:string);
var colname:widestring;
    ColType :TOleEnum;
    collength:integer;
begin
   // copy the database structure
   MainWnd.AXImpExpDbcObj.CheckAndCreateExpTable(Layername);
   MainWnd.AxImpExpDbcObj.CheckAndCreateImpTable(Layername);
   MainWnd.AxImpExpDbcObj.AddImpIdColumn(Layername);
   while MainWnd.AXImpExpDbcObj.GetExportColumn(Layername,colname,coltype,collength) do
   begin
      MainWnd.AXImpExpDbcObj.AddImpColumn(Layername, colname, coltype, collength);
   end;
   MainWnd.AXImpExpDbcObj.OpenExpTable(Layername);
   MainWnd.AXImpExpDbcObj.CreateImpTable(Layername);
   MainWnd.AXImpExpDbcObj.OpenImpTable(Layername);
end;

function TMainWnd.GetFontToUse(const aFontname:string):string;
var retVal:string;
    i     :integer;
begin
   retVal:='';
   if UsedFonts = nil then
      UsedFonts:=TStringList.Create;
   if UnknownFonts = nil then
      UnknownFonts:=TStringList.Create;
   // check if font exists in the used fonts
   for i:=0 to UsedFonts.Count-1 do
   begin
      if UsedFonts[i] = aFontname then
      begin
         retVal:=UsedFonts[i];
         break;
      end;
   end;
   // check if font exists in the Unknown fonts
   if retVal = '' then
   begin
       for i:=0 to UnknownFonts.Count -1 do
       begin
          if UnknownFonts[i] = aFontname then
          begin
             retVal:='Arial';
             break;
          end;
       end;
   end;

   if retVal = '' then
   begin
      // check if font is installed on this system
      for i:=0 to Screen.Fonts.Count-1 do
      begin
         if Screen.Fonts[i] = aFontname then
         begin
            retVal:=aFontname; // font is installed -> use it
            UsedFonts.Add(aFontname);
            break;
         end;
      end;
   end;
   if retVal = '' then // font is not installed on this system -> Use Arial Font
   begin
      UnKnownFonts.Add(aFontname);
      retVal:='Arial';
      // Show warning that font will be mapped to Arial
      StatusLb.Items.Add(StrFileObj.Assign(80) + aFontname + ' -> Arial');
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
   end;
   result:=retVal;
end;

function TMainWnd.CopyObject(var oldObject:IBase; var newObject:IBase; issymbol:boolean):boolean;
var i:integer;
    X:double;
    Y:double;

    DoSkipObject:boolean;

    aHeight:integer;
    aWidth:integer;
    OldSymIdx:integer;
    PrimAxis:integer;
    aFontname:string;

    oldPoint:IPoint;
    newPoint:IPoint;
    oldPixel:IPixel;
    newPixel:IPixel;

    oldPoly :IPolyline;
    newPoly :IPolyline;

    oldSpline:ISpline;
    newSpline:ISpline;

    oldArc:IArc;
    newArc:IArc;

    oldText:IText;
    newText:IText;

    oldCircle:ICircle;
    newCircle:ICircle;

    oldSymbol:ISymbol;
    newSymbol:ISymbol;

    oldImage:IImage;
    newImage:IImage;

    oldMesline:IMesLine;
    newMesline:IMesLine;

    aObjectStyleDisp:IDispatch;
    oldStyleNumber  :integer;
    newStyleNumber  :integer;
    StyleName       :string;
begin
   DoSkipObject:=false;
   if (oldObject.ObjectType = ot_Pixel) then      // copy point object
   begin
      oldPixel:=oldObject as IPixel;
      newPixel:=DestDocument.CreatePixel;
      newPoint:=DestDocument.CreatePoint;
      X:=oldPixel.Position.X / 100; Y:=oldPixel.Position.Y / 100;
      if not issymbol then    // calculate the projection
         AxGisProjection.Calculate(X, Y);
      newPoint.X:=round(X*100);
      newPoint.Y:=round(Y*100);
      newPixel.Position:=newPoint;

      // copy objectstyle of Pixel
      aObjectStyleDisp:=oldPixel.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newPixel.ObjectStyle:=oldPixel.ObjectStyle;
         if (oldPixel.ObjectStyle.Linestyle.Style >= lt_UserDefined) then
         begin
            // old Object has user defined Line Style
            OldStyleNumber:=oldPixel.ObjectStyle.Linestyle.Style;
            StyleName:=OldDocument.GetUserLineStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserLineStyleNumberByName(StyleName);
            newPixel.ObjectStyle.Linestyle.Style:=NewStyleNumber;
         end;
         OldStyleNumber:=oldPixel.ObjectStyle.Fillstyle.Pattern;
         if (oldPixel.ObjectStyle.Fillstyle.Pattern >= pt_UserDefined) then
         begin
            // old Layer has user defined Fill Style
            OldStyleNumber:=oldPixel.ObjectStyle.Fillstyle.Pattern;
            StyleName:=OldDocument.GetUserFillStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserFillStyleNumberByName(StyleName);
            newPixel.ObjectStyle.Fillstyle.Pattern:=NewStyleNumber;
         end;
      end;

      newObject:=newPixel;
   end
   else if (oldObject.ObjectType = ot_Cpoly) then // copy area object
   begin
      if not CopyCPoly(oldObject, newObject, issymbol) then DoSkipObject:=TRUE;
   end
   else if (oldObject.ObjectType = ot_poly) then // copy line object
   begin
      oldPoly:=oldObject as IPolyline;
      newPoly:=DestDocument.CreatePolyline;
      // create new point list
      for i:=0 to oldPoly.Count - 1 do
      begin
         oldPoint:=oldPoly.Points[i];
         X:=oldPoint.X/100; Y:=oldPoint.Y/100;

         if not issymbol then       // calculate the projection
            AxGisProjection.Calculate(X, Y);

         newPoint:=DestDocument.CreatePoint;
         newPoint.X:=round(X*100); newPoint.Y:=round(Y*100);
         newPoly.InsertPoint(newPoint);
      end;

      // copy objectstyle of Polyline
      aObjectStyleDisp:=oldPoly.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newPoly.ObjectStyle:=oldPoly.ObjectStyle;
         if (oldPoly.ObjectStyle.Linestyle.Style >= lt_UserDefined) then
         begin
            // old Object has user defined Line Style
            OldStyleNumber:=oldPoly.ObjectStyle.Linestyle.Style;
            StyleName:=OldDocument.GetUserLineStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserLineStyleNumberByName(StyleName);
            newPoly.ObjectStyle.Linestyle.Style:=NewStyleNumber;
         end;
         OldStyleNumber:=oldPoly.ObjectStyle.Fillstyle.Pattern;
         if (oldPoly.ObjectStyle.Fillstyle.Pattern >= pt_UserDefined) then
         begin
            // old Layer has user defined Fill Style
            OldStyleNumber:=oldPoly.ObjectStyle.Fillstyle.Pattern;
            StyleName:=OldDocument.GetUserFillStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserFillStyleNumberByName(StyleName);
            newPoly.ObjectStyle.Fillstyle.Pattern:=NewStyleNumber;
         end;
      end;
      newObject:=newPoly;
   end
   else if (oldObject.ObjectType = ot_Spline) then // copy spline object
   begin
      oldSpline:=oldObject as ISpline;
      newSpline:=DestDocument.CreateSpline;
      // create new point list
      for i:=0 to oldSpline.Count - 1 do
      begin
         oldPoint:=oldSpline.Points[i];
         X:=oldPoint.X/100; Y:=oldPoint.Y/100;

         if not issymbol then // calculate the projection
            AxGisProjection.Calculate(X, Y);

         newPoint:=DestDocument.CreatePoint;
         newPoint.X:=round(X*100); newPoint.Y:=round(Y*100);
         newSpline.InsertPoint(newPoint);
      end;

      // copy objectstyle of Spline
      aObjectStyleDisp:=oldSpline.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newSpline.ObjectStyle:=oldSpline.ObjectStyle;
         if (oldSpline.ObjectStyle.Linestyle.Style >= lt_UserDefined) then
         begin
            // old Object has user defined Line Style
            OldStyleNumber:=oldSpline.ObjectStyle.Linestyle.Style;
            StyleName:=OldDocument.GetUserLineStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserLineStyleNumberByName(StyleName);
            newSpline.ObjectStyle.Linestyle.Style:=NewStyleNumber;
         end;
         OldStyleNumber:=oldSpline.ObjectStyle.Fillstyle.Pattern;
         if (oldSpline.ObjectStyle.Fillstyle.Pattern >= pt_UserDefined) then
         begin
            // old Layer has user defined Fill Style
            OldStyleNumber:=oldPoly.ObjectStyle.Fillstyle.Pattern;
            StyleName:=OldDocument.GetUserFillStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserFillStyleNumberByName(StyleName);
            newSpline.ObjectStyle.Fillstyle.Pattern:=NewStyleNumber;
         end;
      end;

      newObject:=newSpline;
   end
   else if (oldObject.ObjectType = ot_Arc) then // copy Arc object
   begin
      oldArc:=oldObject as IArc;
      newArc:=DestDocument.CreateArc;
      newPoint:=DestDocument.CreatePoint;
      X:=oldArc.Position.X / 100; Y:=oldArc.Position.Y / 100;
      if not issymbol then    // calculate the projection
         AxGisProjection.Calculate(X, Y);
      newPoint.X:=round(X*100);  newPoint.Y:=round(Y*100);
      newArc.Position:=newPoint;

      newArc.PrimaryAxis:=oldArc.PrimaryAxis;
      newArc.SecondaryAxis:=oldArc.SecondaryAxis;
      newArc.Angle:=oldArc.Angle;
      newArc.BeginAngle:=oldArc.BeginAngle;
      newArc.EndAngle:=oldArc.EndAngle;
      newArc.Radius:=oldArc.Radius;

      // copy objectstyle of Arc
      aObjectStyleDisp:=oldArc.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newArc.ObjectStyle:=oldArc.ObjectStyle;
         if (oldArc.ObjectStyle.Linestyle.Style >= lt_UserDefined) then
         begin
            // old Object has user defined Line Style
            OldStyleNumber:=oldArc.ObjectStyle.Linestyle.Style;
            StyleName:=OldDocument.GetUserLineStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserLineStyleNumberByName(StyleName);
            newArc.ObjectStyle.Linestyle.Style:=NewStyleNumber;
         end;
         OldStyleNumber:=oldArc.ObjectStyle.Fillstyle.Pattern;
         if (oldArc.ObjectStyle.Fillstyle.Pattern >= pt_UserDefined) then
         begin
            // old Layer has user defined Fill Style
            OldStyleNumber:=oldArc.ObjectStyle.Fillstyle.Pattern;
            StyleName:=OldDocument.GetUserFillStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserFillStyleNumberByName(StyleName);
            newArc.ObjectStyle.Fillstyle.Pattern:=NewStyleNumber;
         end;
      end;
      newObject:=newArc;
   end
   else if (oldObject.ObjectType = ot_Text) then // copy text object
   begin
      oldText:=oldObject as IText;
      newText:=DestDocument.CreateText;
      newPoint:=DestDocument.CreatePoint;

      X:=oldText.Position.X / 100; Y:=oldText.Position.Y / 100;
      if not issymbol then    // calculate the projection
         AxGisProjection.Calculate(X, Y);

      newPoint.X:=round(X*100); newPoint.Y:=round(Y*100);
      newText.Position:=newPoint;

      newText.Angle:=oldText.Angle;
      newText.Align:=oldText.Align;
      newText.Width:=oldText.Width;
      newText.Text:=oldText.Text;
      newText.FontStyle:=oldText.FontStyle;
      newText.FontHeight:=oldText.FontHeight;
      // get font that should be used
      aFontname:=GetFontToUse(oldText.Fontname);
      newText.FontName:=aFontname;

      // copy objectstyle of Text
      aObjectStyleDisp:=oldText.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newText.ObjectStyle:=oldText.ObjectStyle;
         if (oldText.ObjectStyle.Linestyle.Style >= lt_UserDefined) then
         begin
            // old Object has user defined Line Style
            OldStyleNumber:=oldText.ObjectStyle.Linestyle.Style;
            StyleName:=OldDocument.GetUserLineStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserLineStyleNumberByName(StyleName);
            newText.ObjectStyle.Linestyle.Style:=NewStyleNumber;
         end;
         OldStyleNumber:=oldText.ObjectStyle.Fillstyle.Pattern;
         if (oldText.ObjectStyle.Fillstyle.Pattern >= pt_UserDefined) then
         begin
            // old Layer has user defined Fill Style
            OldStyleNumber:=oldText.ObjectStyle.Fillstyle.Pattern;
            StyleName:=OldDocument.GetUserFillStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserFillStyleNumberByName(StyleName);
            newText.ObjectStyle.Fillstyle.Pattern:=NewStyleNumber;
         end;
      end;
      newObject:=newText;
   end
   else if (oldObject.ObjectType = ot_Circle) then // copy circle object
   begin
      oldCircle:=oldObject as ICircle;
      newCircle:=DestDocument.CreateCircle;
      newPoint:=DestDocument.CreatePoint;
      X:=oldCircle.Position.X / 100; Y:=oldCircle.Position.Y / 100;
      if not issymbol then    // calculate the projection
         AxGisProjection.Calculate(X, Y);
      newPoint.X:=round(X*100); newPoint.Y:=round(Y*100);
      newCircle.Position:=newPoint;
      PrimAxis:=oldCircle.PrimaryAxis;
      newCircle.PrimaryAxis:=PrimAxis;
      newCircle.SecondaryAxis:=PrimAxis;
      newCircle.Angle:=0; // circle has no Angle
      newCircle.Radius:=PrimAxis;

      // copy objectstyle of Circle
      aObjectStyleDisp:=oldCircle.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newCircle.ObjectStyle:=oldCircle.ObjectStyle;
         if (oldCircle.ObjectStyle.Linestyle.Style >= lt_UserDefined) then
         begin
            // old Object has user defined Line Style
            OldStyleNumber:=oldCircle.ObjectStyle.Linestyle.Style;
            StyleName:=OldDocument.GetUserLineStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserLineStyleNumberByName(StyleName);
            newCircle.ObjectStyle.Linestyle.Style:=NewStyleNumber;
         end;
         OldStyleNumber:=oldCircle.ObjectStyle.Fillstyle.Pattern;
         if (oldCircle.ObjectStyle.Fillstyle.Pattern >= pt_UserDefined) then
         begin
            // old Layer has user defined Fill Style
            OldStyleNumber:=oldCircle.ObjectStyle.Fillstyle.Pattern;
            StyleName:=OldDocument.GetUserFillStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserFillStyleNumberByName(StyleName);
            newCircle.ObjectStyle.Fillstyle.Pattern:=NewStyleNumber;
         end;
      end;

      newObject:=newCircle;
   end
   else if (oldObject.ObjectType = ot_Symbol) then // copy symbol object
   begin
      oldSymbol:=oldObject as ISymbol;
      newSymbol:=DestDocument.CreateSymbol;
      newPoint:=DestDocument.CreatePoint;

      X:=oldSymbol.Position.X / 100; Y:=oldSymbol.Position.Y / 100;
      if not issymbol then    // calculate the projection
         AxGisProjection.Calculate(X, Y);
      newPoint.X:=round(X*100); newPoint.Y:=round(Y*100);
      newSymbol.Position:=newPoint;

      //get new Symbolnr.
      OldSymIdx:=oldSymbol.SymIndex - 600000000;
      newSymbol.SymIndex:=SymbolDefMap[OldSymIdx];
      newSymbol.Size:=oldSymbol.Size;
      newSymbol.SizeType:=oldSymbol.SizeType;
      newSymbol.Angle:=oldSymbol.Angle;
      newSymbol.SymbolName:=oldSymbol.Symbolname;

      // copy objectstyle of Symbol
      aObjectStyleDisp:=oldSymbol.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newSymbol.ObjectStyle:=oldSymbol.ObjectStyle;
         if (oldSymbol.ObjectStyle.Linestyle.Style >= lt_UserDefined) then
         begin
            // old Object has user defined Line Style
            OldStyleNumber:=oldSymbol.ObjectStyle.Linestyle.Style;
            StyleName:=OldDocument.GetUserLineStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserLineStyleNumberByName(StyleName);
            newSymbol.ObjectStyle.Linestyle.Style:=NewStyleNumber;
         end;
         OldStyleNumber:=oldSymbol.ObjectStyle.Fillstyle.Pattern;
         if (oldSymbol.ObjectStyle.Fillstyle.Pattern >= pt_UserDefined) then
         begin
            // old Layer has user defined Fill Style
            OldStyleNumber:=oldSymbol.ObjectStyle.Fillstyle.Pattern;
            StyleName:=OldDocument.GetUserFillStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserFillStyleNumberByName(StyleName);
            newSymbol.ObjectStyle.Fillstyle.Pattern:=NewStyleNumber;
         end;
      end;

      newObject:=newSymbol;
   end
   else if (oldObject.ObjectType = ot_Image) then // copy image object
   begin
      oldImage:=oldObject as IImage;
      if issymbol and (not FileExists(oldImage.Filename)) then
      begin
         DoSkipObject:=TRUE;
         result:=not DoSkipObject;
         exit;
      end;
      if not FileExists(oldImage.Filename) then
      begin
         // Show message that source of image has to be changed
         StatusLB.Items.Add(StrfileObj.Assign(81)+' '+ExtractFilename(oldImage.Filename)+' '+StrfileObj.Assign(82));  // Warning: Image <name> not found. Please set image path in Edit->Links
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
      end;

      newImage:=DestDocument.CreateImage(oldImage.Filename);
      X:=oldImage.X / 100; Y:=oldImage.Y / 100;
      if not issymbol then    // calculate the projection
         AxGisProjection.Calculate(X, Y);

      // get the height and width
      aWidth:=round(oldImage.Width / 100);
      aHeight:=round(oldImage.Height / 100);

      newImage.SetPosition(round(X*100), round(Y*100), round(aWidth*100), round(aHeight*100));

      // set image-style
      newImage.ShowOpt:=oldImage.ShowOpt;
      newImage.Options:=oldImage.Options;
      newImage.TransparencyType:=oldImage.TransparencyType;
      newImage.Transparency:=oldImage.Transparency;

      // copy objectstyle of Image
      aObjectStyleDisp:=oldImage.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newImage.ObjectStyle:=oldImage.ObjectStyle;
         if (oldImage.ObjectStyle.Linestyle.Style >= lt_UserDefined) then
         begin
            // old Object has user defined Line Style
            OldStyleNumber:=oldImage.ObjectStyle.Linestyle.Style;
            StyleName:=OldDocument.GetUserLineStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserLineStyleNumberByName(StyleName);
            newImage.ObjectStyle.Linestyle.Style:=NewStyleNumber;
         end;
         OldStyleNumber:=oldImage.ObjectStyle.Fillstyle.Pattern;
         if (oldImage.ObjectStyle.Fillstyle.Pattern >= pt_UserDefined) then
         begin
            // old Layer has user defined Fill Style
            OldStyleNumber:=oldImage.ObjectStyle.Fillstyle.Pattern;
            StyleName:=OldDocument.GetUserFillStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserFillStyleNumberByName(StyleName);
            newImage.ObjectStyle.Fillstyle.Pattern:=NewStyleNumber;
         end;
      end;
      newObject:=newImage;
   end
   else if (oldObject.ObjectType = ot_Mesline) then      // copy meashurement line
   begin
      oldMesLine:=oldObject as IMesLine;
      newMesLine:=DestDocument.CreateMesLine;

      X:=oldMesLine.StartPoint.x / 100; Y:=oldMesLine.StartPoint.y / 100;
      if not issymbol then    // calculate the projection
         AxGisProjection.Calculate(X, Y);
      newMesLine.StartPoint.x:=round(X*100);
      newMesLine.StartPoint.y:=round(Y*100);

      X:=oldMesLine.EndPoint.x / 100; Y:=oldMesLine.EndPoint.y / 100;
      if not issymbol then    // calculate the projection
         AxGisProjection.Calculate(X, Y);
      newMesLine.EndPoint.x:=round(X*100);
      newMesLine.EndPoint.y:=round(Y*100);

      // copy objectstyle of meashurement line
      aObjectStyleDisp:=oldMesLine.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newMesLine.ObjectStyle:=oldMesLine.ObjectStyle;
         if (oldMesLine.ObjectStyle.Linestyle.Style >= lt_UserDefined) then
         begin
            // old Object has user defined Line Style
            OldStyleNumber:=oldMesLine.ObjectStyle.Linestyle.Style;
            StyleName:=OldDocument.GetUserLineStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserLineStyleNumberByName(StyleName);
            newMesLine.ObjectStyle.Linestyle.Style:=NewStyleNumber;
         end;
         if (oldMesLine.ObjectStyle.Fillstyle.Pattern >= pt_UserDefined) then
         begin
            // old Layer has user defined Fill Style
            OldStyleNumber:=oldMesLine.ObjectStyle.Fillstyle.Pattern;
            StyleName:=OldDocument.GetUserFillStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserFillStyleNumberByName(StyleName);
            newMesLine.ObjectStyle.Fillstyle.Pattern:=NewStyleNumber;
         end;
      end;
      newObject:=newMesLine;
   end
   else if oldObject.ObjectType = ot_LabelText then // a label-text should be imported
   begin
      StatusLB.Items.Add(StrfileObj.Assign(52));  // Warning: Label Text not supported!
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      DoSkipObject:=true;
   end
   else if oldObject.ObjectType = ot_Group then // a group object will be imported
   begin
      StatusLB.Items.Add(StrfileObj.Assign(53));  // Warning: Group objects not supported!
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      DoSkipObject:=true;
   end
   else if oldObject.ObjectType = ot_Proj then // a project object will be imported
   begin
      StatusLB.Items.Add(StrfileObj.Assign(54));  // Warning: Project objects not supported!
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      DoSkipObject:=true;
   end
   else if oldObject.ObjectType = ot_OLEObj then    // an ole object will be imported
   begin
      StatusLB.Items.Add(StrfileObj.Assign(55));  // Warning: OLE objects not supported!
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      DoSkipObject:=true;
   end
   else if oldObject.ObjectType = ot_RText then    // an RText object will be imported
   begin
      StatusLB.Items.Add(StrfileObj.Assign(56));  // Warning: Rtext objects not supported!
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      DoSkipObject:=true;
   end
   else if oldObject.ObjectType = ot_BusGraph then   // a Buisness Graphic object will be imported
   begin
      StatusLB.Items.Add(StrfileObj.Assign(57));   // Warning: Buisness graphic objects not supported!
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      DoSkipObject:=true;
   end
   else
      DoSkipObject:=true;

   result:=not DoSkipObject;
end;


function TMainWnd.CopyCPoly(var OldObject:IBase; var NewObject:IBase;issymbol:boolean):boolean;
var IslandList:IList;
    APoint:IPoint;
    RotatePoint:IPoint;
    IslandPoly:IPolygon;
    PointCount:integer;
    SkipIndex :integer;
    FirstIsland:boolean;
    IslandCount:integer;
    IsIsland:boolean;
    IslandInfoIdx:integer;
    NextIslandIdx:integer;
    X,Y:double;
    Cnt:integer;
    First_X,First_Y:double;
    rotatelastpoly:boolean;
    retVal:boolean;

    oldCPoly:IPolygon;
    newCPoly:IPolygon;

    aObjectStyleDisp:IDispatch;
    oldStyleNumber  :integer;
    newStyleNumber  :integer;
    StyleName       :string;
begin
   retVal:=TRUE;
   PointCount:=0; SkipIndex:=0; FirstIsland:=TRUE; IsIsland:=FALSE;
   rotatelastpoly:=FALSE;
   oldCPoly:=oldObject as IPolygon;
   IslandCount:=oldCPoly.IslandCount;
   if IslandCount > 1 then
   begin
      IsIsland:=true;
      IslandInfoIdx:=0;
      NextIslandIdx:=oldCPoly.IslandInfo[IslandInfoIdx];
      // an island object has to be created
      IslandList:=DestDocument.CreateList;
   end;

   newCPoly:=DestDocument.CreatePolygon;

   // insert first point to new CPoly
   APoint:=oldCPoly.Points[0];
   X:=APoint.X/100; Y:=APoint.Y/100;

   if not issymbol then
   begin
      // calculate the projection
      AxGisProjection.Calculate(X, Y);
   end;

   First_X:=X; First_Y:=Y;
   APoint.X:=round(X*100); APoint.Y:=round(Y*100);

   newCPoly.InsertPoint(APoint);
   PointCount:=PointCount+1;

   for Cnt:=1 to oldCPoly.Count - 1 do
   begin
      if SkipIndex = 0 then
         PointCount:=PointCount + 1;
      if (IsIsland) and (PointCount = NextIslandIdx) then // next island area will be created
      begin
         IslandInfoIdx:=IslandInfoIdx+1;
         if FirstIsland then
            SkipIndex:=1
         else
            SkipIndex:=2;

         FirstIsland:=FALSE;
         if IslandInfoIdx >= IslandCount then
         begin
            // all islands have been inserted
            NextIslandIdx:=0;
            IslandInfoIdx:=IslandCount;
         end
         else
         begin
            // the polygon has to be inserted to the island
            newCPoly.ClosePoly;
            IslandList.Add(newCPoly);

            // and a new polygon has to be created
            newCPoly:=DestDocument.CreatePolygon;
            NextIslandIdx:=oldCPoly.IslandInfo[IslandInfoIdx];

            // insert point to polygon
            APoint:=oldCPoly.Points[Cnt+SkipIndex];
            X:=APoint.X/100; Y:=APoint.Y/100;

            if not issymbol then
            begin
               // calculate the projection
               AxGisProjection.Calculate(X, Y);
            end;

            if (IslandCount-1 = IslandInfoIdx) then
            begin
               // last polygon reached
               if (X = First_X) and (Y = First_Y) then
               begin
                  rotatelastpoly:=true;
                  RotatePoint:=APoint;
                  RotatePoint.X:=round(X*100); RotatePoint.Y:=round(Y*100);
               end;
            end;

            APoint.X:=round(X*100); APoint.Y:=round(Y*100);
            if not rotatelastpoly then
               newCPoly.InsertPoint(APoint);
            PointCount:=1;
         end;
      end
      else
      begin
         if (SkipIndex=0) then
         begin
            if (IsIsland) and (Cnt = oldCPoly.Count -2) then //avoid exporting last connectionline
               continue;

            // insert point to polygon
            APoint:=oldCPoly.Points[Cnt];
            X:=APoint.X/100; Y:=APoint.Y/100;

            if not issymbol then
            begin
               // calculate the projection
               AxGisProjection.Calculate(X, Y);
            end;
            APoint.X:=round(X*100); APoint.Y:=round(Y*100);
            newCPoly.InsertPoint(APoint);
         end
         else
            SkipIndex:=SkipIndex-1;
      end;
   end;

   if IsIsland then
   begin
      if rotatelastpoly then
         newCPoly.InsertPoint(RotatePoint);
      newCPoly.ClosePoly;
      IslandList.Add(newCPoly);
      IslandPoly:=DestDocument.CreatePolygon;
      IslandPoly:=DestDocument.CreateIslandArea(IslandList);

      // copy objectstyle of CPoly
      aObjectStyleDisp:=oldCPoly.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         IslandPoly.ObjectStyle:=oldCPoly.ObjectStyle;
         if (oldCPoly.ObjectStyle.Linestyle.Style >= lt_UserDefined) then
         begin
            // old Object has user defined Line Style
            OldStyleNumber:=oldCPoly.ObjectStyle.Linestyle.Style;
            StyleName:=OldDocument.GetUserLineStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserLineStyleNumberByName(StyleName);
            IslandPoly.ObjectStyle.Linestyle.Style:=NewStyleNumber;
         end;
         OldStyleNumber:=oldCPoly.ObjectStyle.Fillstyle.Pattern;
         if (oldCPoly.ObjectStyle.Fillstyle.Pattern >= pt_UserDefined) then
         begin
            // old Layer has user defined Fill Style
            OldStyleNumber:=oldCPoly.ObjectStyle.Fillstyle.Pattern;
            StyleName:=OldDocument.GetUserFillStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserFillStyleNumberByName(StyleName);
            IslandPoly.ObjectStyle.Fillstyle.Pattern:=NewStyleNumber;
         end;
      end;

      newObject:=IslandPoly;
   end
   else
   begin
      newCPoly.ClosePoly;

      // copy objectstyle of CPoly
      aObjectStyleDisp:=oldCPoly.ObjectStyle;
      if aObjectStyleDisp <> nil then
      begin
         newCPoly.ObjectStyle:=oldCPoly.ObjectStyle;
         if (oldCPoly.ObjectStyle.Linestyle.Style >= lt_UserDefined) then
         begin
            // old Object has user defined Line Style
            OldStyleNumber:=oldCPoly.ObjectStyle.Linestyle.Style;
            StyleName:=OldDocument.GetUserLineStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserLineStyleNumberByName(StyleName);
            newCPoly.ObjectStyle.Linestyle.Style:=NewStyleNumber;
         end;
         OldStyleNumber:=oldCPoly.ObjectStyle.Fillstyle.Pattern;
         if (oldCPoly.ObjectStyle.Fillstyle.Pattern >= pt_UserDefined) then
         begin
            // old Layer has user defined Fill Style
            OldStyleNumber:=oldCPoly.ObjectStyle.Fillstyle.Pattern;
            StyleName:=OldDocument.GetUserFillStyleNameByNumber(OldStyleNumber);
            NewStyleNumber:=DestDocument.GetUserFillStyleNumberByName(StyleName);
            newCPoly.ObjectStyle.Fillstyle.Pattern:=NewStyleNumber;
         end;
      end;

      newObject:=newCPoly;
   end;

   if PointCount < 4 then retVal:=FALSE; // a Poly must have at least 3 corners
   result:=retVal;
end;




{*******************************************************************************
* PROZEDUR : RunPass                                                           *
********************************************************************************
* Prozedur leitet den jeweiligen Konvertierungs-Pass ein.                      *
*                                                                              *
* PARAMETER: Pass -> Gibt den Konvertierungs-Pass an.                          *
*                                                                              *
********************************************************************************}
procedure TMainWnd.RunPass(AktPass:integer);
var aDate,aTime,DispDate, DispTime:string;
    aApp:IDispatch;
    aCore:ICore;
begin
   if AktPass = 1 then // reading source project
   begin
      // set mask settings
      OkBtn.Enabled:=false; CancelBtn.Enabled:=true; SaveReportBtn.Enabled:=false;
      Aktiv:=true;

      if SourceAmpFilename <> LastOpenendAmpFilename then
      begin
         Screen.Cursor:=crHourGlass;
         aApp:=App;
         aCore:=aApp as ICore;

         OldDocument:=aCore.OpenDocument(SourceAmpFilename);
         Screen.Cursor:=crDefault;
      end;

      AmpOpened:=true;

      if Importmode = ImportMode_DKM then
         SetProjectionSettings;

      // now it will be checked if the projection fits to the projection
      // that is set in the projects
      CheckProjectionSettings;

      // display start date/time
      FileStartTime:=now;
      aDate:=FormatDateTime('ddmmyy',now);
      DispDate:=copy(aDate,1,2)+'.'+copy(aDate,3,2)+'.'+copy(aDate,5,2);
      aTime:=Formatdatetime('hhnnss',Now);
      DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
      StatusLB.Items.Add(StrFileObj.Assign(61) + DispDate+' / '+DispTime); // start time:

      if Importmode = ImportMode_DKM then
      begin
         AXImpExpDbcObj.IgnoreNonExistExpTables:=TRUE;
         StatusLB.Items.Add(StrFileObj.Assign(16));        { Import DKM Data }
      end
      else
      begin
         if ImportMode = ImportMode_Batch then
            AXImpExpDbcObj.IgnoreNonExistExpTables:=TRUE;
         StatusLB.Items.Add(StrFileObj.Assign(17) + Extractfilename(SourceAmpfilename)); { Import WinGIS Project: }
      end;

      // display source database settings
      StatusLb.Items.Add(AXImpExpDbcObj.ExpDbInfo);
      StatusLb.Items.Add(AXImpExpDbcObj.ImpDbInfo);

      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      { Die Anzahl der zu bearbeitenden Bytes wird ermittelt }
      Bytes2Work:=CalculateElementsToImport; Bytesworked:=0;

      Pass:=1;
      ExecutePass;
      exit;
   end;

   if AktPass = 2 then // import finished
   begin
      StatusLB.Items.Add(StrfileObj.Assign(18));  { Project correctly imported! }
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      DisplayStatistics;
      aktiv:=false; Abort:=true;
      Aktiv:=false;
      CancelBtn.Enabled:=false; OkBtn.Enabled:=true;
      SaveReportBtn.Enabled:=true;
      if (Importmode = ImportMode_DKM) or (ImportMode = ImportMode_Batch) then
         CloseTimer.Enabled:=true;
      exit;
   end;
end;

procedure TMainWnd.DisplayStatistics;
var aDate, aTime, DispTime, DispDate:string;
    EndTime, UsedTime:TDateTime;
begin
   StatusLB.Items.Add('---------------------------------------------------');
   // display endtime
   aDate:=FormatDateTime('ddmmyy',now);
   DispDate:=copy(aDate,1,2)+'.'+copy(aDate,3,2)+'.'+copy(aDate,5,2);
   aTime:=Formatdatetime('hhnnss',Now);
   DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
   StatusLB.Items.Add(StrFileObj.Assign(62) + DispDate + ' / '+DispTime); // end time:

   // display ellapsed time
   EndTime:=Now;
   UsedTime:=EndTime-FileStartTime;
   aTime:=Formatdatetime('hhnnss',UsedTime);
   DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
   StatusLB.Items.Add(StrFileObj.Assign(63) + DispTime); // Used time:

   // display imported data statistics
   if NumLayers     > 0 then StatusLB.Items.Add(StrFileObj.Assign(64) + inttostr(NumLayers));     // created layers:
   if NumSymbolDefs > 0 then StatusLB.Items.Add(StrFileObj.Assign(65) + inttostr(NumSymbolDefs)); // created symbol definitions:
   if NumViews      > 0 then StatusLB.Items.Add(StrFileObj.Assign(66) + inttostr(NumViews));      // created views :
   if NumSymbols    > 0 then StatusLB.Items.Add(StrFileObj.Assign(67) + inttostr(NumSymbols));    // created symbols:
   if NumPolys      > 0 then StatusLB.Items.Add(StrFileObj.Assign(68) + inttostr(NumPolys));      // created polylines:
   if NumCPolys     > 0 then StatusLB.Items.Add(StrFileObj.Assign(69) + inttostr(NumCPolys));     // created polygons:
   if NumCircles    > 0 then StatusLB.Items.Add(StrFileObj.Assign(70) + inttostr(NumCircles));    // created circles:
   if Numarcs       > 0 then StatusLB.Items.Add(StrFileObj.Assign(71) + inttostr(NumArcs));       // created arcs:
   if NumSplines    > 0 then StatusLB.Items.Add(StrFileObj.Assign(72) + inttostr(NumSplines));    // created splines:
   if NumTexts      > 0 then StatusLB.Items.Add(StrFileObj.Assign(73) + inttostr(NumTexts));      // created texts:
   if NumPixels     > 0 then StatusLB.Items.Add(StrFileObj.Assign(74) + inttostr(NumPixels));     // created points:
   if NumImages     > 0 then StatusLB.Items.Add(StrFileObj.Assign(75) + inttostr(NumImages));     // created images :
   if NumMeslines   > 0 then StatusLB.Items.Add(StrFileObj.Assign(79) + inttostr(NumMesLines));   // created meashurement lines :
   if NumIgnoredObjects > 0 then StatusLB.Items.Add(StrFileObj.Assign(76) + inttostr(NumIgnoredObjects)); // Ignored objects:
   StatusLB.Items.Add('---------------------------------------------------');
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;
end;

procedure TMainWnd.Showpercent;
var Prozent:integer;
begin
   if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
   ProzessBalken.Progress :=Prozent;
   ProzessBalken.Update;
   Application.Processmessages;
end;

procedure TMainWnd.ExecutePass;
var Prozent:integer;
    aline  :string;
begin
   if Pass = 1 then { Einlesen der Daten  }
   begin
      CopyUserStyles;
      CopySymbolLibrary;
      if SelAmpWnd.WriteDbTabFilename <> '' then
      begin
         {$I-}
         // create Tab-Seperated File
         AssignFile(TabFile,SelAmpWnd.WriteDbTabFilename);
         Rewrite(TabFile);
         // write header of Tab-seperated file
         aline:='LAYER'+chr(9)+'OLDID'+chr(9)+'NEWID';
         writeln(TabFile,aline);
         {$I+}
      end;
      ImportObjects;
      if (Bytesworked >= Bytes2work) and (Abort = false) then
      begin
         CopyViews; // if all layers have been created the Views will be copied
         if SelAmpWnd.WriteDbTabFilename <> '' then
         begin
            {$I-}
            CloseFile(TabFile);
            {$I+}
         end;
         RunPass(2);
         exit;
      end;
      if Abort = true then exit;
   end;
end;

function TMainWnd.CreateLayer(aLayer:ILayer):ILayer;
begin
   NumLayers:=NumLayers+1;
   result:=DestDocument.Layers.InsertLayer(aLayer,FALSE);
end;

function TMainWnd.CheckLayerExists(const aLayername:string;var aLayer:ILayer):boolean;
var dispatch:IDispatch;
    retVal:boolean;
begin
   dispatch:=DestDocument.Layers.GetLayerByName(aLayername);
   if dispatch <> nil then retVal:=true else retVal:=false;
   if retVal then
      aLayer:=DestDocument.Layers.GetLayerByName(aLayername);
   result:=retVal;
end;

procedure TMainWnd.StartImport;
begin
   if Importmode = ImportMode_DKM then
      ExitNormal:=true;
   if ExitNormal then
   begin
      AmpOpened:=false;
      AXImpExpDbcObj.WorkingDir:=OSInfo.WingisDir;
      AXImpExpDbcObj.LngCode:=LngCode;
      if Importmode = ImportMode_DKM then
      begin
         AxGisProjection.WorkingPath:=OSInfo.WingisDir;
         AxGisProjection.LngCode:=LngCode;
         MainWnd.SetProjectionSettings;

         // if it has been started in DKM-Mode import can be executed
         RunPass(1);

         CloseTimer.Enabled:=true;
      end
      else
      begin
         SelAmpWnd.GetProjectName(SourceAmpfilename);
         RunPass(1);
      end;
   end;
end;

procedure TMainWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TMainWnd.CloseTimerTimer(Sender: TObject);
begin
   MainWnd.Close;
   SelAmpWnd.Close;
   DestDocument.FinishImpExpRoutine(ExitNormal,'IMPORTMODULE');
   CloseTimer.Enabled:=false;
end;

procedure TMainWnd.CheckProjectionSettings;
var SourceProjection:integer;
    different:boolean;
begin
   different:=false;
   // function is used to check if the settings in the
   if (AxGisProjection.SourceCoordinateSystem <> OldDocument.Projection) then
      different:=TRUE;

   if (AxGisProjection.SourceXOffset <> OldDocument.ProjectionXOffset) then
      different:=TRUE;

   if (AxGisProjection.SourceYOffset <> OldDocument.ProjectionYOffset) then
      different:=TRUE;

   if (AxGisProjection.SourceScale <> OldDocument.ProjectionScale) then
      different:=TRUE;

   if (AxGisProjection.TargetCoordinateSystem <> DestDocument.Projection) then
      different:=TRUE;

   if (AxGisProjection.TargetXOffset <> DestDocument.ProjectionXOffset) then
      different:=TRUE;

   if (AxGisProjection.TargetYOffset <> DestDocument.ProjectionYOffset) then
      different:=TRUE;

   if (AxGisProjection.TargetScale <> DestDocument.ProjectionScale) then
      different:=TRUE;

   // check if the projections are different
   if OldDocument.ProjectProjSettings <> AxGisProjection.SourceProjSettings then
      different:=TRUE;

   if OldDocument.ProjectProjection <> AxGisProjection.SourceProjection then
      different:=TRUE;

   if OldDocument.Projectdate <> AxGisProjection.SourceDate then
      different:=TRUE;

   if DestDocument.ProjectProjSettings <> AxGisProjection.TargetProjSettings then
      different:=TRUE;

   if DestDocument.ProjectProjection <> AxGisProjection.TargetProjection then
      different:=TRUE;

   if DestDocument.Projectdate <> AxGisProjection.TargetDate then
      different:=TRUE;

   if (different) and (Importmode <> ImportMode_DKM) then
   begin
      Showmessage(StrfileObj.Assign(59)); // Warning: Projection does not fit to project projection
      AxGisProjection.SetupByDialog;
   end;
end;

procedure TMainWnd.SetProjectionSettings;
var aApp:OleVariant;
    aDoc:OleVariant;
begin
   // procedure is used to set the projection settings
   // of the current project to the GeoWnd
   AxGisProjection.WorkingPath:=OSInfo.WingisDir;
   AxGisProjection.LngCode:=LngCode;
   AXGisObjMan.WorkingDir:=OSInfo.WingisDir;
   AXGisObjMan.LngCode:=LngCode;
   aApp:=App; AxGisObjMan.SetApp(aApp);
   aDoc:=DestDocument; AxGisObjMan.SetDocument(aDoc);
   if not AmpOpened then
   begin
      AxGisProjection.SourceCoordinateSystem:=DestDocument.Projection;
      AxGisProjection.TargetCoordinateSystem:=DestDocument.Projection;
      AxGisProjection.SourceProjSettings:=DestDocument.ProjectProjSettings;
      AxGisProjection.TargetProjSettings:=DestDocument.ProjectProjSettings;
      AxGisProjection.SourceProjection:=DestDocument.ProjectProjection;
      AxGisProjection.TargetProjection:=DestDocument.ProjectProjection;
      AxGisProjection.SourceDate:=DestDocument.Projectdate;
      AxGisProjection.TargetDate:=DestDocument.Projectdate;
      AxGisProjection.SourceXOffset:=DestDocument.ProjectionXOffset;
      AxGisProjection.TargetXOffset:=DestDocument.ProjectionXOffset;
      AxGisProjection.SourceYOffset:=DestDocument.ProjectionYOffset;
      AxGisProjection.TargetYOffset:=DestDocument.ProjectionYOffset;
      AxGisProjection.SourceScale:=DestDocument.ProjectionScale;
      AxGisProjection.TargetScale:=DestDocument.ProjectionScale;
      // if source and target is .amp - file
      // the used input coordinates and used output coordinates
      // for the projection calculation are in carthesic
      AxGisProjection.UsedInputCoordSystem:=cs_Carthesic;
      AxGisProjection.UsedOutputCoordSystem:=cs_Carthesic;
   end
   else
   begin
      AxGisProjection.SourceCoordinateSystem:=OldDocument.Projection;
      AxGisProjection.TargetCoordinateSystem:=DestDocument.Projection;
      AxGisProjection.SourceProjSettings:=OldDocument.ProjectProjSettings;
      AxGisProjection.TargetProjSettings:=DestDocument.ProjectProjSettings;
      AxGisProjection.SourceProjection:=OldDocument.ProjectProjection;
      AxGisProjection.TargetProjection:=DestDocument.ProjectProjection;
      AxGisProjection.SourceDate:=OldDocument.Projectdate;
      AxGisProjection.TargetDate:=DestDocument.Projectdate;
      AxGisProjection.SourceXOffset:=OldDocument.ProjectionXOffset;
      AxGisProjection.TargetXOffset:=DestDocument.ProjectionXOffset;
      AxGisProjection.SourceYOffset:=OldDocument.ProjectionYOffset;
      AxGisProjection.TargetYOffset:=DestDocument.ProjectionYOffset;
      AxGisProjection.SourceScale:=OldDocument.ProjectionScale;
      AxGisProjection.TargetScale:=DestDocument.ProjectionScale;
      // if source and target is .amp - file
      // the used input coordinates and used output coordinates
      // for the projection calculation are in carthesic
      AxGisProjection.UsedInputCoordSystem:=cs_Carthesic;
      AxGisProjection.UsedOutputCoordSystem:=cs_Carthesic;
   end;

   if ImportMode = ImportMode_DKM then
   begin
      // the source projection is the same then the target projection
      AxGisProjection.SourceCoordinateSystem:=DestDocument.Projection;
      AxGisProjection.TargetCoordinateSystem:=DestDocument.Projection;
      AxGisProjection.SourceProjSettings:=DestDocument.ProjectProjSettings;
      AxGisProjection.TargetProjSettings:=DestDocument.ProjectProjSettings;
      AxGisProjection.SourceProjection:=DestDocument.ProjectProjection;
      AxGisProjection.TargetProjection:=DestDocument.ProjectProjection;
      AxGisProjection.SourceDate:=DestDocument.Projectdate;
      AxGisProjection.TargetDate:=DestDocument.Projectdate;
      AxGisProjection.SourceXOffset:=DestDocument.ProjectionXOffset;
      AxGisProjection.TargetXOffset:=DestDocument.ProjectionXOffset;
      AxGisProjection.SourceYOffset:=DestDocument.ProjectionYOffset;
      AxGisProjection.TargetYOffset:=DestDocument.ProjectionYOffset;
      AxGisProjection.SourceScale:=DestDocument.ProjectionScale;
      AxGisProjection.TargetScale:=DestDocument.ProjectionScale;
      // if source and target is .amp - file
      // the used input coordinates and used output coordinates
      // for the projection calculation are in carthesic
      AxGisProjection.UsedInputCoordSystem:=cs_Carthesic;
      AxGisProjection.UsedOutputCoordSystem:=cs_Carthesic;
   end;
end;

procedure TMainWnd.SaveReportBtnClick(Sender: TObject);
var Datei:Textfile;
    Zeile:string;
    i:integer;
begin
   if SaveDialogRep.Execute then
   begin
      {$I-}
      Assignfile(Datei,SaveDialogRep.Filename);
      Rewrite(Datei);
      for i:=0 to StatusLB.Items.Count-1 do
      begin
         Zeile:=StatusLB.items[i];
         Writeln(Datei,Zeile);
      end;
      Closefile(Datei);
      {$I+}
   end;
end;

procedure TMainWnd.CancelBtnClick(Sender: TObject);
begin
   StatusLB.Items.Add(StrfileObj.Assign(36));  { Warning: Import canceled }
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;

   if SelAmpWnd.WriteDbTabFilename <> '' then
      CloseFile(TabFile);

   DisplayStatistics;

   aktiv:=false; Abort:=true;
   CancelBtn.Enabled:=false;
   OkBtn.Enabled:=true;
   SaveReportBtn.Enabled:=true;
end;

procedure TMainWnd.FormDestroy(Sender: TObject);
begin
   if UsedFonts <> nil then
      UsedFonts.Destroy;
   if UnknownFonts <> nil then
      UnknownFonts.Destroy;
   StrfileObj.Destroy;
end;

procedure TMainWnd.SetupWindowTimerTimer(Sender: TObject);
var IdbProcessMask:integer;
begin
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   if ShowMode = ShowMode_Show then
   begin
      SetWindowPos(Handle,HWND_TOP,SelAmpWnd.winleft,SelAmpWnd.wintop,SelAmpWnd.winwidth, SelAmpWnd.winheight,SWP_SHOWWINDOW);
      MainWnd.Height:=SelAmpWnd.winheight;
      MainWnd.Width:=SelAmpWnd.winwidth;
   end
   else if ShowMode =ShowMode_Hide then
   begin
      SetWindowPos(Handle,HWND_TOP,SelAmpWnd.winleft,SelAmpWnd.wintop,1,1,SWP_SHOWWINDOW);
      MainWnd.Height:=1;
      MainWnd.Width:=1;
   end;
   Self.Repaint;
   SetupWindowTimer.Enabled:=false;
   // get the current idb-setup
   IdbProcessMask:=DestDocument.IdbProcessMask;
   // reset the idb-setup
   DestDocument.IdbProcessMask:=0;
   StartImport;
   // switch back to old idb-setup
   DestDocument.IdbProcessMask:=IdbProcessMask;
end;

procedure TMainWnd.OKBtnClick(Sender: TObject);
begin
   CloseTimer.Enabled:=true;
end;

procedure TMainWnd.FormShow(Sender: TObject);
begin
   AXImpExpDbcObj.visible:=FALSE;
   AxGisProjection.visible:=FALSE;
   AXGisObjMan.visible:=FALSE;
end;

procedure TMainWnd.CopyUserStyles;
var wlffilename:string;
begin
   wlffilename:=OSInfo.TempDataDir+'ampimp.wlf';
   // export the user defined styles from the OldDocument
   OldDocument.ExportUserDefStylesToWlf(wlffilename);
   // and import it into the new document
   DestDocument.ImportUserDefStylesFromWlf(wlffilename);
   DeleteFile(PCHAR(wlffilename));
end;

procedure TMainWnd.AXImpExpDbcObjDialogSetuped(Sender: TObject);
begin
   SelAmpWnd.PageControl.Enabled:=TRUE;
   SelAmpWnd.GeoBtn.Enabled:=TRUE;
   SelAmpWnd.DbBtn.Enabled:=TRUE;
   SelAmpWnd.OkBtn.Enabled:=SelAmpWnd.OkBtnMode;
   SelAmpWnd.CancelBtn.Enabled:=TRUE;
   SelAmpWnd.DestDbLabel.Caption:=AXImpExpDbcObj.ImpDbInfo;
   SelAmpWnd.SourceDbLabel.Caption:=AXImpExpDbcObj.ExpDbInfo;
end;

procedure TMainWnd.AxGisProjectionWarning(Sender: TObject;
  const Info: WideString);
begin
   if not Aktiv then
   begin
      Showmessage(Info);
   end
   else
   begin
      StatusLB.Items.Add(Info);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
   end;
end;

procedure TMainWnd.AxGisProjectionError(Sender: TObject;
  const Info: WideString);
begin
   if not Aktiv then
   begin
      Showmessage(Info);
   end
   else
   begin
      StatusLB.Items.Add(Info);
      StatusLB.Items.Add(StrfileObj.Assign(36));  { Warning: Import canceled }
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      aktiv:=false; Abort:=true;
      CancelBtn.Enabled:=false;
      OkBtn.Enabled:=true;
      SaveReportBtn.Enabled:=true;
   end;
end;

procedure TMainWnd.AXImpExpDbcObjRequestExpProjectName(Sender: TObject;
  var ProjectName: WideString);
var ProjName:string;
begin
   SelAmpWnd.GetProjectName(ProjName);
   ProjectName:=ProjName;
end;

procedure TMainWnd.AXImpExpDbcObjRequestImpProjectName(Sender: TObject;
  var ProjectName: WideString);
var ProjName:string;
begin
   ProjName:=DestDocument.Name;
   ProjectName:=ProjName;
end;

procedure TMainWnd.AXImpExpDbcObjRequestImpLayerIndex(Sender: TObject;
  const Layername: WideString; var LayerIndex: Integer);
var i:integer;
begin
   // the Layerindex of the Layername has to be evaluated
   for i:=0 to DestDocument.Layers.Count-1 do
   begin
      if DestDocument.Layers.Items[i].Layername = Layername then
      begin
         LayerIndex:=DestDocument.Layers.Items[i].Index;
         break;
      end;
   end;
end;

procedure TMainWnd.AXImpExpDbcObjRequestExpLayerIndex(Sender: TObject;
  const Layername: WideString; var LayerIndex: Integer);
var i:integer;
begin
   // the Layerindex of the Layername has to be evaluated
   for i:=0 to OldDocument.Layers.Count-1 do
   begin
      if OldDocument.Layers.Items[i].Layername = Layername then
      begin
         LayerIndex:=OldDocument.Layers.Items[i].Index;
         break;
      end;
   end;
end;

procedure TMainWnd.AXImpExpDbcObjRequestImpProjectGUIDs(Sender: TObject;
  var StartGUID, CurrentGUID: WideString);
var aStartGUID, aCurrentGUID:string;
begin
   aStartGUID:=DestDocument.StartProjectGUID;
   aCurrentGUID:=DestDocument.CurrentProjectGUID;
   StartGUID:=aStartGUID;
   CurrentGUID:=aCurrentGUID;
end;

end.
