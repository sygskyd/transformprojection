unit Strfile;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs;

type
  // define stringfile structure
  PStrInfo = ^StrInfo;
  StrInfo = record
     Info     :string;
     next     :PStrInfo;
  end;

ClassStrfile = class
   public
      { public Definitionen }
      constructor Create;
      destructor Destroy;
      procedure  CreateList(Dir:string; LngCode:string; Section:string);
      function  Assign(Index:integer):string;
   private
      { private Definitionen }
      procedure Insert_item(Info:string);

end;

implementation

var StrBegin,StrEnd:PStrInfo;

constructor ClassStrFile.Create;
begin
   new(StrBegin); new(StrEnd);
   StrBegin:=nil; StrEnd:=nil;
end;

destructor ClassStrFile.Destroy;
var iterate1,iterate2:PStrInfo;
begin
   iterate1:=StrBegin;
   iterate2:=StrBegin;
   while (iterate1 <> nil) do
   begin
      iterate2:=iterate1;
      iterate1:=iterate1^.next;
      dispose(iterate2);
   end;
   StrBegin:=nil;
   StrEnd:=nil;
end;

procedure ClassStrFile.CreateList(Dir:string;LngCode:string;Section:string);
var StringFile:Textfile;
    Line:string;
    Filename:string;
    SectionFound:boolean;
begin
   StrBegin:=nil; StrEnd:=nil;
   Filemode:=0;
   Filename:=Dir+'ConGIS.'+LngCode;
   if Fileexists(Filename) then
   begin
      Assignfile(StringFile,Filename);
      Reset(StringFile);
      // first the Section has to be found
      SectionFound:=FALSE;
      repeat
         readln(StringFile, Line);
         if (Line = '['+Section+']') then
            SectionFound:=TRUE;
      until (EOF(StringFile)) or (SectionFound);
      if SectionFound then
      begin
         repeat
            readln(StringFile,Line);
            if (copy(Line,1,1) <> '[') then // check if entry is a next section item
               Insert_item(Line)
            else
               SectionFound:=FALSE;
         until (EOF(StringFile)) or (SectionFound = FALSE);
      end;
      CloseFile(StringFile);
   end;
end;

procedure ClassStrFile.Insert_item(Info:string);
var new_record: PStrInfo;
begin
   new(new_record);
   new_record^.Info:=Info;
   if StrBegin = NIL then
   begin
      StrEnd:= new_record;
      StrBegin:= new_record;
      new_record^.next:=NIL;
   end
   else
   begin
      StrEnd^.next:=new_record;
      StrEnd:=new_record;
      new_record^.next:=NIL;
      if StrBegin^.next = NIL then StrBegin^.next:= new_record;
   end;
end;

function ClassStrFile.Assign(Index:integer):string;
var iterate       : PStrInfo;
    number,count  : integer;
    found         : boolean;
    retVal        : string;
begin
   iterate:=StrBegin; found:=false;
   if StrBegin <> nil then
   repeat
      { Extrahieren des Index }
      val(copy(iterate^.info,1,pos(' ',iterate^.info)-1),number,count);
      if number = Index then begin found:=true; retVal:=COPY(iterate^.info,5,length(iterate^.Info)); end;
      iterate:=iterate^.next;
   until (found = true) or (iterate = nil);
   if found = false then retVal:='Code:'+inttostr(Index);
   retVal:=TrimLeft(retVal);
   result:=retVal;
end;
end.

