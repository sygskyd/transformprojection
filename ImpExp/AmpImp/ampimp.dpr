library ampimp;

{ Wichtiger Hinweis zur DLL-Speicherverwaltung: Sharemem mu� die erste
 Unit in der uses-Klausel Ihrer Bibliothek und Ihres Projekts sein,
 wenn Ihre DLL Prozeduren und Funktionen exportiert, die
 Strings als Parameter oder Funktionsergebnisse �bergeben. Dies
 betrifft alle Strings, die an Ihre DLL oder von Ihrer DLL �bergeben werden--selbst diejenigen,
 die sich in Records oder Klassen befinden. Sharemem ist die Schnittstelle zu
 BORLNDMM.DLL, die den gemeinsamen Zugriff auf den Speicher verwaltet; DELPHIMM.DLL
 mu� mit Ihrer DLL weitergegeben werden. Wenn Sie BORLNDMM.DLL nicht verwenden m�chten, m�ssen
 Strings als PChar oder ShortString �bergeben werden.}

uses
  Forms,
  SysUtils,
  Classes,
  Dialogs,
  Maindlg in 'maindlg.pas' {MainWnd},
  Strfile in 'Strfile.pas',
  AxDll_TLB,
  comobj,
  SelAmpDlg in 'SelAmpDlg.pas' {SelAmpWnd},
  SetImgDlg in 'SetImgDlg.pas' {SetImgWnd};

{$R *.RES}

{*******************************************************************************
* PROZEDURE * AMPIMPORT                                                        *
********************************************************************************
* Procedure is used to import AMP-Data into GIS                                *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure AMPIMPORT(DIR:PCHAR;var AXHANDLE:Variant;var DEST:Variant;REGMODE:integer;LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var
aDocument:IDispatch;
begin
   // create MainWnd
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.WinGISApp:=AXHANDLE;
   aDocument:=DEST;
   MainWnd.DestDocument:=aDocument as IDocument;
   MainWnd.Regmode:=REGMODE;
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.CreateStringFile;

   // create SelAmpWnd
   SelAmpWnd:=TSelAmpWnd.Create(nil);
   SelAmpWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);

   SelAmpWnd.Show;
end;

{*******************************************************************************
* PROCEDURE * CREATEIMP                                                        *
********************************************************************************
* Procedure is used to prepare an amp-import for Batchmode                     *
*                                                                              *
********************************************************************************}
procedure CREATEIMP(DIR:PCHAR;var AXHANDLE:Variant;var DEST:Variant;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var aDoc:IDispatch;
begin
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.WinGISApp:=AXHANDLE;
   aDoc:=DEST;
   MainWnd.DestDocument:=aDoc as IDocument;
   MainWnd.Regmode:=REGMODE;
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.CreateStringFile;

   // create SelAmpWnd
   SelAmpWnd:=TSelAmpWnd.Create(nil);
   SelAmpWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);
   SelAmpWnd.SetupForBatch;
end;

{*******************************************************************************
* PROCEDURE * SETAMPIMPSETTINGS                                                *
********************************************************************************
* Procedure is used to set import project name and import mode                 *
*                                                                              *
* PARAMETERS: IMPPROJ     -> Name of the project that should be imported       *
*             IMPPROJMODE -> Mode how it should be exported.                   *
*                            0 -> Import all layers                            *
*                            1 -> Import selected layers                       *
*             SHOWMODE    -> 0 -> Normal                                       *
*                            1 -> Hide Main Window                             *
*                                                                              *
********************************************************************************}
procedure SETAMPIMPSETTINGS(IMPPROJ:PCHAR; IMPPROJMODE:INTEGER; SHOWMODE:INTEGER);stdcall;
begin
   SelAmpWnd.SetImpSettings(strpas(IMPPROJ), IMPPROJMODE, SHOWMODE);
end;

// procedure is used to add a layer that should be imported
// if only selected layers should be imported
procedure ADDIMPLAYER(ALAYER:PCHAR);stdcall;
begin
   SelAmpWnd.AddImpLayer(strpas(ALAYER));
end;

// procedure is used to set database-settings
procedure SETDBSETTINGS(SOURCEDBMODE:INTEGER; SOURCEDBNAME:PCHAR; SOURCEDBTABLE:PCHAR;
                        DESTDBMODE  :INTEGER; DESTDBNAME  :PCHAR; DESTDBTABLE  :PCHAR);stdcall;
begin
   MainWnd.AXImpExpDbcObj.ExpDbMode:=SOURCEDBMODE;
   MainWnd.AXImpExpDbcObj.ExpDatabase:=strpas(SOURCEDBNAME);
   MainWnd.AxImpExpDbcObj.ExpTable:=strpas(SOURCEDBTABLE);
   MainWnd.AXImpExpDbcObj.ImpDbMode:=DESTDBMODE;
   MainWnd.AxImpExpDbcObj.ImpDatabase:=strpas(DESTDBNAME);
   MainWnd.AxImpExpDbcObj.ImpTable:=strpas(DESTDBTABLE);
end;

// procedure is used to set projection for batch mode
procedure SETPROJECTION(SOURCECOORDINATESYSTEM:INTEGER; // source projection
                        SOURCEPROJECTION      :PCHAR;
                        SOURCEDATE            :PCHAR;
                        SOURCEPROJSETTINGS    :PCHAR;
                        SOURCEXOFFSET         :DOUBLE;
                        SOURCEYOFFSET         :DOUBLE;
                        SOURCESCALE           :DOUBLE;
                        TARGETCOORDINATESYSTEM:INTEGER; // target projection
                        TARGETPROJECTION      :PCHAR;
                        TARGETDATE            :PCHAR;
                        TARGETPROJSETTINGS    :PCHAR;
                        TARGETXOFFSET         :DOUBLE;
                        TARGETYOFFSET         :DOUBLE;
                        TARGETSCALE           :DOUBLE);stdcall;
begin
   // set source projection
   MainWnd.AxGisProjection.SourceCoordinateSystem:=SOURCECOORDINATESYSTEM;
   MainWnd.AxGisProjection.SourceProjection:=strpas(SOURCEPROJECTION);
   MainWnd.AxGisProjection.SourceDate:=strpas(SOURCEDATE);
   MainWnd.AxGisProjection.SourceProjSettings:=strpas(SOURCEPROJSETTINGS);
   MainWnd.AxGisProjection.SourceXOffset:=SOURCEXOFFSET;
   MainWnd.AxGisProjection.SourceYOffset:=SOURCEYOFFSET;
   MainWnd.AxGisProjection.SourceScale:=SOURCESCALE;
   MainWnd.AxGisProjection.TargetCoordinateSystem:=TARGETCOORDINATESYSTEM;
   MainWnd.AxGisProjection.TargetProjection:=strpas(TARGETPROJECTION);
   MainWnd.AxGisProjection.TargetDate:=strpas(TARGETDATE);
   MainWnd.AxGisProjection.TargetProjSettings:=strpas(TARGETPROJSETTINGS);
   MainWnd.AxGisProjection.TargetXOffset:=TARGETXOFFSET;
   MainWnd.AxGisProjection.TargetYOffset:=TARGETYOFFSET;
   MainWnd.AxGisProjection.TargetScale:=TARGETSCALE;
end;

// function is used to execute an amp-import
procedure EXECIMP;stdcall;
begin
   MainWnd.ExitNormal:=true;
   MainWnd.Importmode:=2;  { Informs that Import is in Batch Mode }
   MainWnd.Show;
end;

{*******************************************************************************
* PROZEDUR * AMPDKMIMPORT                                                      *
********************************************************************************
* Prozedur dient dazu, DKM - Daten ins GIS zu importieren                      *
*                                                                              *
* PARAMETER: DIR            : Current work directory                           *
*            AXHANDLE       : Pointer to DX-Control                            *
*            DESTAMPFILE    : Pointer to Project into that should be imported  *
*            SOURCEAMPFILENAME:Name of the Project that should be imported     *
*            DKMDBFFILENAME   :Name of the Table that belongs to DKM-Project   *
*            DBDESTMODE     : Mode of the Destination Database                 *
*            DBDESTNAME     : Name of the Destination Database                 *
*                                                                              *
********************************************************************************}
procedure AMPDKMIMPORT(DIR:PCHAR;var AXHANDLE:Variant;DESTAMPFILE:Variant;SOURCEAMPFILENAME:PCHAR;DKMDBFFILENAME:PCHAR;DBDESTMODE:integer;DBDESTNAME:PCHAR;DBDESTTABLE:PCHAR;LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var aDoc:IDispatch;
begin
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.WinGISApp:=AXHANDLE;
   aDoc:=DESTAMPFILE;
   MainWnd.DestDocument:=aDoc as IDocument;
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.Regmode:=1; // full licence
   MainWnd.LngCode:=strpas(LNGCODE);

   MainWnd.Importmode:=1;                                         { Informs that Import is DKM-Import           }
   MainWnd.AXImpExpDbcObj.ExpDbMode:=4;                           { Set the Sourcedatabase to Access-Tables     }
   MainWnd.AXImpExpDbcObj.ExpDatabase:=strpas(DKMDBFFILENAME);    { Set the name of the Source Database         }
   MainWnd.AXImpExpDbcObj.ExpTable:='';
   MainWnd.AXImpExpDbcObj.ImpDbMode:=DBDESTMODE;                    { Set the Destinationmode of the Database     }
   if DBDESTMODE = 0 then MainWnd.AXImpExpDbcObj.ExpDbMode:=0;    { Wenn keine Zieldatenbank angegeben wurde wird auch keine Quelldatenbank ausgelesen }
   MainWnd.AXImpExpDbcObj.ImpDatabase:=strpas(DBDESTNAME);          { Set the Name of the Destination database    }
   MainWnd.AXImpExpDbcObj.ImpTable:=strpas(DBDESTTABLE);    // set name of target database-table
   MainWnd.SourceAmpfilename:=strpas(SOURCEAMPFILENAME); { Set the Name of the Project that should be imported }
   MainWnd.CreateStringFile;

   // create SelAmpWnd
   SelAmpWnd:=TSelAmpWnd.Create(nil);
   SelAmpWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);
   MainWnd.Show;
end;

function FREEDLL:boolean; stdcall;
var retVal:boolean;
begin
   retVal:=MainWnd.ExitNormal;
   MainWnd.Free;
   SelAmpWnd.Free;
   result:=retVal;
end;


exports AMPIMPORT, CREATEIMP, SETAMPIMPSETTINGS, ADDIMPLAYER,
        SETDBSETTINGS, SETPROJECTION, EXECIMP, AMPDKMIMPORT,FREEDLL;

begin
end.
