object SetImgWnd: TSetImgWnd
  Left = 288
  Top = 190
  BorderStyle = bsDialog
  Caption = 'Image not found!'
  ClientHeight = 265
  ClientWidth = 333
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object OKBtn: TButton
    Left = 125
    Top = 235
    Width = 93
    Height = 23
    Caption = 'OK'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 0
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 224
    Top = 235
    Width = 101
    Height = 23
    Cancel = True
    Caption = 'Abbrechen'
    ModalResult = 2
    TabOrder = 1
    OnClick = CancelBtnClick
  end
  object UseDirCB: TCheckBox
    Left = 4
    Top = 40
    Width = 317
    Height = 12
    Caption = 'Use Default Image Path'
    TabOrder = 2
    OnClick = UseDirCBClick
  end
  object Panel2: TPanel
    Left = 4
    Top = 56
    Width = 325
    Height = 143
    TabOrder = 3
    object PathDirLB: TDirectoryListBox
      Left = 4
      Top = 4
      Width = 195
      Height = 113
      FileList = BitmapFileLb
      ItemHeight = 16
      TabOrder = 0
    end
    object PathDriveCB: TDriveComboBox
      Left = 4
      Top = 119
      Width = 195
      Height = 19
      DirList = PathDirLB
      TabOrder = 1
    end
    object BitmapFileLb: TFileListBox
      Left = 200
      Top = 4
      Width = 121
      Height = 133
      ItemHeight = 13
      TabOrder = 2
      OnChange = BitmapFileLbChange
      OnClick = BitmapFileLbClick
    end
  end
  object IgnoreAllCB: TCheckBox
    Left = 4
    Top = 208
    Width = 317
    Height = 17
    Caption = 'Ignore all not found images'
    TabOrder = 4
    OnClick = IgnoreAllCBClick
  end
  object ImgNamePanel: TPanel
    Left = 4
    Top = 4
    Width = 325
    Height = 25
    Caption = 'ImgNamePanel'
    TabOrder = 5
  end
end
