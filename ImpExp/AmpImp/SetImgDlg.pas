unit SetImgDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, FileCtrl,StrFile, ExtDlgs;

type
  TSetImgWnd = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    UseDirCB: TCheckBox;
    Panel2: TPanel;
    PathDirLB: TDirectoryListBox;
    PathDriveCB: TDriveComboBox;
    IgnoreAllCB: TCheckBox;
    BitmapFileLb: TFileListBox;
    ImgNamePanel: TPanel;
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure UseDirCBClick(Sender: TObject);
    procedure IgnoreAllCBClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitmapFileLbChange(Sender: TObject);
    procedure BitmapFileLbClick(Sender: TObject);
  private
    { Private declarations }
    ImageName:string;
    procedure CheckWindow;
  public
    { Public declarations }
    procedure SetImageName(ImgName:string);
    function  GetImageName:string;
  end;

var
  SetImgWnd: TSetImgWnd;
  StrFileObj    :ClassStrfile;

implementation

uses MainDlg;

{$R *.DFM}

procedure TSetImgWnd.OKBtnClick(Sender: TObject);
var Dir:string;
begin
   if UseDirCB.Checked then
   begin
      Dir:=PathDirLB.Directory;
      if copy(Dir,length(Dir)-1,1) <> '\' then Dir:=Dir + '\';
      MainWnd.ImagePath:=Dir;
   end;
   if IgnoreAllCb.Checked then
   begin
      MainWnd.IgnoreNotFoundImages:=true;
   end;
   ModalResult:=mrOK;
end;

procedure TSetImgWnd.CancelBtnClick(Sender: TObject);
begin
   ModalResult:=mrCancel;
end;

procedure TSetImgWnd.FormCreate(Sender: TObject);
begin
   Self.Caption:=StrFileObj.Assign(48);        // Image not found!
   UseDirCB.Caption:=StrfileObj.Assign(49);    // Use default image path
   IgnoreAllCB.Caption:=StrfileObj.Assign(50); // Ignore not found images
   OkBtn.Caption:=StrfileObj.Assign(12);       // Ok
   CancelBtn.Caption:=StrfileObj.Assign(13);   // Cancel
   ImgNamePanel.Caption:='';
   UseDirCB.Checked:=false;
   IgnoreAllCB.Checked:=false;
end;

procedure TSetImgWnd.SetImageName(ImgName:string);
begin
   ImageName:=ImgName;
   ImgNamePanel.Caption:=ExtractFilename(ImgName);
end;

function TSetImgWnd.GetImageName:string;
begin
   result:=ImageName;
end;

procedure TSetImgWnd.UseDirCBClick(Sender: TObject);
begin
   if UseDirCB.Checked then
   begin
      BitmapFileLb.visible:=FALSE;
      IgnoreAllCb.checked:=false;
   end
   else
   begin
      BitmapFileLb.visible:=TRUE;
      IgnoreAllCb.checked:=false;
   end;
   CheckWindow;
end;

procedure TSetImgWnd.IgnoreAllCBClick(Sender: TObject);
begin
   if IgnoreAllCB.Checked then
   begin
      UseDirCB.checked:=false;
      UseDirCB.enabled:=false;
      Panel2.visible:=false;
   end
   else
   begin
      UseDirCB.enabled:=true;
      Panel2.visible:=true;
   end;
   CheckWindow;
end;

procedure TSetImgWnd.CheckWindow;
var allok:boolean;
    i    :integer;
begin
   allok:=FALSE;
   if IgnoreAllCB.Checked then
   begin
      allok:=TRUE;
      ImageName:='';
   end
   else
   begin
      if UseDirCb.Checked then
      begin
         allok:=TRUE;
         ImageName:='';
      end
      else
      begin
         // check if there is a filename selected in the BitmapFileLb
         ImageName:='';
         for i:=0 to BitmapFileLb.items.count-1 do
         begin
            if BitmapFileLb.Selected[i] then
            begin
               ImageName:=PathDirLb.Directory;
               if copy(ImageName,length(ImageName)-1,1) <> '\' then
                  ImageName:=ImageName+'\';
               ImageName:=ImageName+BitmapfileLb.Items[i];
               allok:=TRUE;
               break;
            end;
         end;
      end;
   end;
   if allok then OkBtn.Enabled:=TRUE else OKBtn.Enabled:=FALSE;
end;

procedure TSetImgWnd.FormShow(Sender: TObject);
var aMask:string;
begin
   // set the Filter for the BitmapFileLb
   aMask:=ExtractFileExt(ImgNamePanel.Caption);
   aMask:='*'+aMask;
   BitMapFileLb.Mask:=aMask;
end;

procedure TSetImgWnd.BitmapFileLbChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSetImgWnd.BitmapFileLbClick(Sender: TObject);
begin
   CheckWindow;
end;

end.
