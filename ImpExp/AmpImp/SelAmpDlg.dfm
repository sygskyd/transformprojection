object SelAmpWnd: TSelAmpWnd
  Left = 218
  Top = 150
  BorderStyle = bsDialog
  Caption = 'PROGIS AMP-Import 2000'
  ClientHeight = 350
  ClientWidth = 456
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 8
    Top = 8
    Width = 441
    Height = 305
    ActivePage = TabSheetSource
    TabIndex = 0
    TabOrder = 0
    object TabSheetSource: TTabSheet
      Caption = 'Source project'
      ImageIndex = 1
      object FilesFileLB: TFileListBox
        Left = 144
        Top = 0
        Width = 113
        Height = 177
        DragMode = dmAutomatic
        ItemHeight = 13
        Mask = '*.amp'
        MultiSelect = True
        TabOrder = 0
        OnClick = FilesFileLBClick
        OnDblClick = FilesFileLBDblClick
      end
      object LayerLB: TListBox
        Left = 264
        Top = 24
        Width = 169
        Height = 153
        DragMode = dmAutomatic
        ItemHeight = 13
        MultiSelect = True
        TabOrder = 1
        OnClick = LayerLBClick
      end
      object LayerCB: TCheckBox
        Left = 264
        Top = 0
        Width = 169
        Height = 17
        Caption = 'Import only selected layers'
        TabOrder = 2
        OnClick = LayerCBClick
      end
      object Panel1: TPanel
        Left = 0
        Top = 204
        Width = 433
        Height = 25
        TabOrder = 3
        object SourceDbLabel: TLabel
          Left = 8
          Top = 8
          Width = 74
          Height = 13
          Caption = 'SourceDbLabel'
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 178
        Width = 433
        Height = 25
        TabOrder = 4
        object DestDbLabel: TLabel
          Left = 8
          Top = 8
          Width = 62
          Height = 13
          Caption = 'DestDbLabel'
        end
      end
      object IgnoreDuplicatesCb: TCheckBox
        Left = 0
        Top = 256
        Width = 425
        Height = 17
        Caption = 'Ignore already existing objects'
        TabOrder = 5
      end
      object Panel3: TPanel
        Left = 0
        Top = 230
        Width = 433
        Height = 25
        TabOrder = 6
        object WriteDbTabLabel: TLabel
          Left = 256
          Top = 8
          Width = 84
          Height = 13
          Caption = 'WriteDbTabLabel'
        end
        object WriteDbTabCb: TCheckBox
          Left = 2
          Top = 4
          Width = 223
          Height = 17
          Caption = 'Write PROGIS Id'#180's to Tab File'
          TabOrder = 0
          OnClick = WriteDbTabCbClick
        end
        object WriteDbTabBtn: TButton
          Left = 228
          Top = 0
          Width = 25
          Height = 23
          Caption = '...'
          TabOrder = 1
          OnClick = WriteDbTabBtnClick
        end
      end
      object FileSelectionPanel: TFileSelectionPanel
        Left = 0
        Top = 0
        Width = 143
        Height = 177
        BevelOuter = bvLowered
        TabOrder = 7
        Mask = '*.*'
        Directory = 'C:\Users\aigner\Desktop'
        DirectoryShellTreeView = ShellTreeView
        OnDirectoryChanged = FileSelectionPanelDirectoryChanged
        object ShellTreeView: TShellTreeView
          Left = 0
          Top = 0
          Width = 143
          Height = 177
          ObjectTypes = [otFolders]
          Root = 'rfDesktop'
          UseShellImages = True
          AutoRefresh = False
          Indent = 19
          ParentColor = False
          RightClickSelect = True
          ShowRoot = False
          TabOrder = 0
          OnDblClick = ShellTreeViewDblClick
          OnKeyDown = ShellTreeViewKeyDown
        end
      end
    end
  end
  object OKBtn: TButton
    Left = 240
    Top = 320
    Width = 93
    Height = 23
    Caption = 'OK'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 348
    Top = 320
    Width = 101
    Height = 23
    Cancel = True
    Caption = 'Abbrechen'
    ModalResult = 2
    TabOrder = 2
    OnClick = CancelBtnClick
  end
  object GeoBtn: TButton
    Left = 8
    Top = 320
    Width = 93
    Height = 23
    Caption = 'Projection'
    Default = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = GeoBtnClick
  end
  object DbBtn: TButton
    Left = 104
    Top = 320
    Width = 93
    Height = 23
    Caption = 'Database'
    TabOrder = 4
    OnClick = DbBtnClick
  end
  object SetupWindowTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = SetupWindowTimerTimer
    Left = 208
    Top = 312
  end
  object SaveDialogTab: TSaveDialog
    DefaultExt = '*.tab'
    FileName = '*.tab'
    Filter = 'TAB-Seperated File|*.tab'
    Left = 200
    Top = 320
  end
end
