unit SelAmpDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StrFile, StdCtrls, Buttons, FileCtrl, ComCtrls, ExtCtrls, Db, ADODB,
  ShellCtrls, FileSelectionPanel, WinOSInfo;

type
  TSelAmpWnd = class(TForm)
    PageControl: TPageControl;
    TabSheetSource: TTabSheet;
    FilesFileLB: TFileListBox;
    LayerLB: TListBox;
    LayerCB: TCheckBox;
    OKBtn: TButton;
    CancelBtn: TButton;
    GeoBtn: TButton;
    SetupWindowTimer: TTimer;
    Panel1: TPanel;
    Panel2: TPanel;
    DestDbLabel: TLabel;
    SourceDbLabel: TLabel;
    DbBtn: TButton;
    IgnoreDuplicatesCb: TCheckBox;
    Panel3: TPanel;
    WriteDbTabCb: TCheckBox;
    WriteDbTabBtn: TButton;
    WriteDbTabLabel: TLabel;
    SaveDialogTab: TSaveDialog;
    FileSelectionPanel: TFileSelectionPanel;
    ShellTreeView: TShellTreeView;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure FilesFileLBDblClick(Sender: TObject);
    procedure FilesFileLBClick(Sender: TObject);
    procedure LayerCBClick(Sender: TObject);
    procedure SourceDBPCChange(Sender: TObject);
    procedure DestDBPCChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GeoBtnClick(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure DbBtnClick(Sender: TObject);
    procedure LayerLBClick(Sender: TObject);
    procedure WriteDbTabBtnClick(Sender: TObject);
    procedure WriteDbTabCbClick(Sender: TObject);
    procedure ShellTreeViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ShellTreeViewDblClick(Sender: TObject);
    procedure FileSelectionPanelDirectoryChanged(Sender: TObject);
  private
    { Private declarations }
    FilterStr           :string;
    FirstActivate       :boolean;
    SourceAmpFilename   :string;
    procedure CheckWindow;
  public
    { Public declarations }
    winheight:integer;
    winwidth :integer;
    wintop   :integer;
    winleft  :integer;
    OkBtnMode:boolean;
    WriteDbTabFilename  :string;
    procedure GetProjectName(var Ampfile:string);
    procedure CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
    procedure SetupForBatch;
    procedure SetImpSettings(ImpProj:string; ImpProjMode:integer; ShowMode:integer);
    procedure AddImpLayer(aLayer:string);
  end;

var
  SelAmpWnd     : TSelAmpWnd;
  StrFileObj    : ClassStrfile;

implementation

uses Maindlg, inifiles, AxDLL_TLB;

{$R *.DFM}

procedure TSelAmpWnd.FormCreate(Sender: TObject);
var myScale:double;
    oldWidth:integer;
    oldHeight:integer;
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   Self.Caption:=StrfileObj.Assign(1);                 // Progis AMP-Import 2000
   TabsheetSource.Caption:=StrfileObj.Assign(2);       // Source Project
   LayerCB.Caption:=StrfileObj.Assign(4);              // Import only selected layers
   OkBtn.Caption:=StrfileObj.Assign(12);               // Ok
   CancelBtn.Caption:=StrfileObj.Assign(13);           // Cancel
   GeoBtn.Caption:=StrfileObj.Assign(20);              // Projection
   DbBtn.Caption:=StrfileObj.Assign(5);                // Database
   IgnoreDuplicatesCb.Caption:=StrfileObj.Assign(60);  // Ignore already existing objects
   WriteDbTabCb.Caption:=StrFileObj.Assign(77);        // Write PROGIS Id�s to Tab File

   LayerCB.enabled:=false; LayerLB.enabled:=false;

   SourceAmpFilename:='';
   FilterStr:='*.amp';
   WriteDbTabFilename:='';
   WriteDbTabLabel.Caption:='';
   WriteDbTabBtn.Enabled:=FALSE;

   FirstActivate:=true;

   oldWidth:=Self.Width;
   oldHeight:=Self.Height;

   Self.Height:=1;
   Self.Width:=1;

   myScale:=1;//100000 / Trunc(9600000/(Self.PixelsPerInch));
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
   
   // now adapt winheigt and winwidth
   winheight:=round(oldHeight*myScale);
   winwidth:=round(oldWidth*myScale);
end;

procedure TSelAmpWnd.OKBtnClick(Sender: TObject);
var inifile:TIniFile;
begin
   MainWnd.ExitNormal:=true;
   if (MainWnd.AXImpExpDbcObj.ImpDbMode = 0) or (MainWnd.AXImpExpDbcObj.ExpDbMode = 0) then
   begin
      MainWnd.AXImpExpDbcObj.ImpDbMode:=0;
      MainWnd.AXImpExpDbcObj.ImpDatabase:='';
      MainWnd.AXImpExpDbcObj.ImpTable:='';
      MainWnd.AXImpExpDbcObj.ExpDbMode:=0;
      MainWnd.AXImpExpDbcObj.ExpDatabase:='';
      MainWnd.AXImpExpDbcObj.ExpTable:='';
   end;

   // write the current settings to the ConGIS.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'ConGIS.ini');
   inifile.WriteString('AMPIMP','ImpDbMode',inttostr(MainWnd.AXImpExpDbcObj.ImpDbMode));
   inifile.WriteString('AMPIMP','ImpDatabase',MainWnd.AXImpExpDbcObj.ImpDatabase);
   inifile.WriteString('AMPIMP','ImpTable',MainWnd.AXImpExpDbcObj.ImpTable);
   inifile.WriteString('AMPIMP','ExpDbMode',inttostr(MainWnd.AXImpExpDbcObj.ExpDbMode));
   inifile.WriteString('AMPIMP','ExpDatabase',MainWnd.AXImpExpDbcObj.ExpDatabase);
   inifile.WriteString('AMPIMP','ExpTable',MainWnd.AXImpExpDbcObj.ExpTable);
   if IgnoreDuplicatesCb.Checked then inifile.WriteString('AMPIMP','IgnoreExistingObjects','T')
   else inifile.WriteString('AMPIMP','IgnoreExistingObjects','F');
   inifile.WriteString('AMPIMP','LastUsedPath',FileSelectionPanel.Directory);

   inifile.Destroy;
   // now the import window can be opened and the import executed
   Self.Close;
   MainWnd.Show;
end;

procedure TSelAmpWnd.CancelBtnClick(Sender: TObject);
begin
   MainWnd.ExitNormal:=false;
   MainWnd.CloseTimer.Enabled:=true;
end;

procedure TSelAmpWnd.FilesFileLBDblClick(Sender: TObject);
var i:integer;
    Dateiname:string;
    DocumentDisp:IDispatch;
    cnt:integer;
    ALayer:Variant;
    Layername:string;
    dummy:string;
begin
   for i:=0 to FilesFileLB.Items.Count-1 do
   begin
      Dateiname:=FilesFileLB.Directory; if length(Dateiname) > 3 then Dateiname:=Dateiname + '\';
      Dateiname:=Dateiname + FilesFileLB.items[i];
      if FilesFileLB.selected[i] = true then SourceAmpFilename:=Dateiname;
   end;
   { Now the Layerlist has to be filled }
   Self.Cursor:=crHourGlass;
   Screen.Cursor:=crHourGlass;
   DocumentDisp:=MainWnd.App.OpenDocument(SourceAmpFilename);
   Self.Cursor:=crDefault;
   if DocumentDisp = nil then
   begin
      SourceAmpFilename:='';
      MainWnd.AmpOpened:=false;
   end
   else
   begin
      MainWnd.OldDocument:=DocumentDisp as IDocument;
      MainWnd.AmpOpened:=true;
      MainWnd.SetProjectionSettings;
      MainWnd.LastOpenendAmpFilename:=SourceAmpFilename;
      SelAmpWnd.Cursor:=crDefault;
      LayerCB.enabled:=true;
      LayerLB.Items.Clear;
      for cnt:=0 to MainWnd.OldDocument.Layers.Count-1 do
      begin
         ALayer:=MainWnd.OldDocument.Layers.Items[cnt];
         Layername:=ALayer.Layername;
         LayerLB.Items.Add(Layername);
      end;
      // now check if the old database has an idb-> in this case the idb will
      // be used for the old project
      if (MainWnd.AXImpExpDbcObj.ExpDbMode = 0) or (MainWnd.AXImpExpDbcObj.ExpDbMode = 5) then
      begin
         dummy:=SourceAmpFilename;
         dummy:=ChangeFileExt(dummy,'.wgi');
         if FileExists(dummy) then
         begin
            MainWnd.AXImpExpDbcObj.ExpDbMode:=5;  // idb-database
            MainWnd.AXImpExpDbcObj.ExpDatabase:=dummy;
            MainWnd.AXImpExpDbcObj.ExpTable:='';
         end
         else
            MainWnd.AXImpExpDbcObj.ExpDbMode:=0; // no-database
         SourceDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ExpDbInfo;
      end;
   end;
   Self.Cursor:=crDefault;
   Screen.Cursor:=crDefault;
   CheckWindow;
end;

procedure TSelAmpWnd.GetProjectName(var Ampfile:string);
begin
   Ampfile:=SourceAmpFilename;
end;

procedure TSelAmpWnd.FilesFileLBClick(Sender: TObject);
var i:integer;
    Dateiname:string;
    dummy:string;
begin
   for i:=0 to FilesFileLB.Items.Count-1 do
   begin
      Dateiname:=FilesFileLB.Directory; if Dateiname[length(Dateiname)] <> '\' then Dateiname:=Dateiname+'\';
      Dateiname:=Dateiname + FilesFileLB.items[i];
      if FilesFileLB.selected[i] = true then
      begin
         if Dateiname <> SourceAmpFilename then
         begin
            LayerLB.Items.Clear;
            LayerCB.Checked:=false;
            LayerCB.enabled:=false;
         end;
         SourceAmpFilename:=Dateiname;
      end;
   end;
   if SourceAmpFilename <> MainWnd.LastOpenendAmpFilename then
   begin
      MainWnd.AmpOpened:=false;
      MainWnd.SetProjectionSettings;
   end;

   if (MainWnd.AXImpExpDbcObj.ExpDbMode = 0) or (MainWnd.AXImpExpDbcObj.ExpDbMode = 5) then
   begin
      dummy:=SourceAmpFilename;
      dummy:=ChangeFileExt(dummy,'.wgi');
      if FileExists(dummy) then
      begin
         MainWnd.AXImpExpDbcObj.ExpDbMode:=5;  // idb-database
         MainWnd.AXImpExpDbcObj.ExpDatabase:=dummy;
         MainWnd.AXImpExpDbcObj.ExpTable:='';
      end
      else
         MainWnd.AXImpExpDbcObj.ExpDbMode:=0; // no-database
      SourceDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ExpDbInfo;
   end;
   CheckWindow;
end;

procedure TSelAmpWnd.LayerCBClick(Sender: TObject);
begin
   if LayerCB.Checked then
   begin
      LayerLB.enabled:=true;
   end
   else
   begin
      LayerLB.enabled:=false;
   end;
   CheckWindow;
end;

procedure TSelAmpWnd.CheckWindow;
var allok:boolean;
    i    :integer;
begin
   allok:=true;
   { Procedure checks all settings in the window }
   if not Fileexists(SourceAmpFilename) then allok:=false;
   if allok and LayerCb.Checked then
   begin
      allok:=FALSE;
      for i:=0 to LayerLb.Items.Count - 1 do
      begin
         if LayerLb.Selected[i] then
         begin
            allok:=TRUE;
            break;
         end;
      end;
   end;

   if (WriteDbTabCb.Checked) and (WriteDbTabFilename = '') then allok:=FALSE;

   if allok then OKBtn.Enabled:=true
   else OkBtn.Enabled:=false;
end;


procedure TSelAmpWnd.SourceDBPCChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSelAmpWnd.DestDBPCChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSelAmpWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      MainWnd.AxGisProjection.WorkingPath:=OSInfo.WingisDir;
      MainWnd.AxGisProjection.LngCode:=MainWnd.LngCode;
      MainWnd.AXImpExpDbcObj.WorkingDir:=OSInfo.WingisDir;
      MainWnd.AXImpExpDbcObj.LngCode:=MainWnd.LngCode;
      DestDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ImpDbInfo;
      SourceDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ExpDbInfo;
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TSelAmpWnd.GeoBtnClick(Sender: TObject);
begin
   MainWnd.AxGisProjection.SetupByDialog;
end;

procedure TSelAmpWnd.CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
begin
   wintop:=(WHEIGHT div 2) - (winheight div 2);
   winleft:=(WWIDTH div 2) - (winwidth div 2);
end;

procedure TSelAmpWnd.SetupWindowTimerTimer(Sender: TObject);
var curdir:string;
    dummy:string;
    inifile:TIniFile;
    intval, count:integer;
begin
   SetupWindowTimer.Enabled:=false;
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,winleft,wintop,winwidth, winheight,SWP_SHOWWINDOW);
   Self.Height:=winheight;
   Self.Width:=winwidth;

   // read out last settings from ConGIS.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'ConGIS.ini');
   dummy:=MainWnd.DestDocument.Name;
   dummy:=ChangeFileExt(dummy,'.wgi');
   if (FileExists(dummy)) then // there exists an idb for this project
   begin
      MainWnd.AXImpExpDbcObj.ImpDbMode:=5; // idb-database
      MainWnd.AXImpExpDbcObj.ImpDatabase:=dummy;
      MainWnd.AXImpExpDbcObj.ImpTable:='';
   end
   else
   begin
      // read out last settings from the ConGIS.ini file
      dummy:=inifile.ReadString('AMPIMP','ImpDbMode','0');
      val(dummy, intval, count);
      MainWnd.AXImpExpDbcObj.ImpDbMode:=0; // default is no database
      if intval <> 5 then
      begin
         MainWnd.AXImpExpDbcObj.ImpDbMode:=intval;
         dummy:=inifile.ReadString('AMPIMP','ImpDatabase','');
         MainWnd.AXImpExpDbcObj.ImpDatabase:=dummy;
         dummy:=inifile.ReadString('AMPIMP','ImpTable','');
         MainWnd.AXImpExpDbcObj.ImpTable:=dummy;
      end;
   end;
   DestDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ImpDbInfo;

   MainWnd.AXImpExpDbcObj.ExpDbMode:=0; // default is no database
   dummy:=inifile.ReadString('AMPIMP','ExpDbMode','0');
   val(dummy, intval, count);
   if intval <> 5 then
   begin
      MainWnd.AXImpExpDbcObj.ExpDbMode:=intval;
      dummy:=inifile.ReadString('AMPIMP','ExpDatabase','');
      MainWnd.AXImpExpDbcObj.ExpDatabase:=dummy;
      dummy:=inifile.ReadString('AMPIMP','ExpTable','');
      MainWnd.AXImpExpDbcObj.ExpTable:=dummy;
   end;
   SourceDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ExpDbInfo;

   dummy:=inifile.ReadString('AMPIMP','IgnoreExistingObjects','F');
   if dummy = 'T' then IgnoreDuplicatesCb.Checked:=TRUE else IgnoreDuplicatesCb.Checked:=FALSE;

   dummy:=inifile.ReadString('AMPIMP','LastUsedPath','');
   if dummy <> '' then
      FileSelectionPanel.Directory:=dummy
   else
   begin
      GetDir(0,curdir);
      FileSelectionPanel.Directory:=curdir;
   end;
   inifile.Destroy;
   Self.Repaint;
end;

// procedure is used to setup the SelAmpWnd for batch mode
procedure TSelAmpWnd.SetupForBatch;
begin
   if FirstActivate then
   begin
      MainWnd.AxGisProjection.WorkingPath:=OSInfo.WingisDir;
      MainWnd.AxGisProjection.LngCode:=MainWnd.LngCode;
      MainWnd.AXImpExpDbcObj.WorkingDir:=OSInfo.WingisDir;
      MainWnd.AXImpExpDbcObj.LngCode:=MainWnd.LngCode;
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
   end;
end;

procedure TSelAmpWnd.SetImpSettings(ImpProj:string; ImpProjMode:integer; ShowMode:integer);
var i:integer;
    DocumentDisp:IDispatch;
    cnt:integer;
    ALayer:Variant;
    Layername:string;
begin
   SourceAmpFilename:=ImpProj;
   // Now the Layerlist has to be filled
   DocumentDisp:=MainWnd.App.OpenDocument(SourceAmpFilename);
   if DocumentDisp = nil then
   begin
      SourceAmpFilename:='';
      MainWnd.AmpOpened:=false;
   end
   else
   begin
      MainWnd.OldDocument:=DocumentDisp as IDocument;
      MainWnd.AmpOpened:=true;
      MainWnd.SetProjectionSettings;
      MainWnd.LastOpenendAmpFilename:=SourceAmpFilename;
      SelAmpWnd.Cursor:=crDefault;
      LayerCB.enabled:=true;
      LayerLB.Items.Clear;
      for cnt:=0 to MainWnd.OldDocument.Layers.Count-1 do
      begin
         ALayer:=MainWnd.OldDocument.Layers.Items[cnt];
         Layername:=ALayer.Layername;
         LayerLB.Items.Add(Layername);
      end;
   end;

   if ImpProjMode = 1 then
      LayerCB.Checked:=true;
   MainWnd.ShowMode:=ShowMode;
end;

procedure TSelAmpWnd.AddImpLayer(aLayer:string);
var i:integer;
begin
   // find the Layername in the list and select the item
   for i:=0 to LayerLB.Items.Count-1 do
   begin
      if (LayerLB.Items[i] = aLayer) then
      begin
         LayerLB.Selected[i]:=true;
      end;
   end;
end;

procedure TSelAmpWnd.DbBtnClick(Sender: TObject);
begin
   MainWnd.AXImpExpDbcObj.SetupByDialog(Self.Handle, 5,5);
   PageControl.Enabled:=FALSE;
   GeoBtn.Enabled:=FALSE;
   DbBtn.Enabled:=FALSE;
   OkBtnMode:=OkBtn.Enabled;
   OkBtn.Enabled:=FALSE;
   CancelBtn.Enabled:=FALSE;
end;

procedure TSelAmpWnd.LayerLBClick(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSelAmpWnd.WriteDbTabBtnClick(Sender: TObject);
begin
   if SaveDialogTab.Execute then
   begin
      WriteDbTabFilename:=SaveDialogTab.Filename;
      WriteDbTabLabel.Caption:=ExtractFilename(WriteDbTabFilename);
   end;
   CheckWindow;
end;

procedure TSelAmpWnd.WriteDbTabCbClick(Sender: TObject);
begin
   if WriteDbTabCb.Checked then
   begin
      WriteDbTabBtn.Enabled:=TRUE;
      WriteDbTabLabel.Caption:=ExtractFilename(WriteDbTabFilename);
   end
   else
   begin
      WriteDbTabBtn.Enabled:=FALSE;
      WriteDbTabLabel.Caption:='';
      WriteDbTabFilename:='';
   end;
   CheckWindow;
end;

procedure TSelAmpWnd.ShellTreeViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if ssShift in Shift then
  begin
     if FilterStr ='*.amp' then
        FilterStr:= '*.ssp'
     else
        FilterStr:= '*.amp';
  end;
end;

procedure TSelAmpWnd.ShellTreeViewDblClick(Sender: TObject);
begin
   FilesFileLB.Mask:=FilterStr;
   FilesFileLB.Refresh;
end;

procedure TSelAmpWnd.FileSelectionPanelDirectoryChanged(Sender: TObject);
begin
   if FileSelectionPanel.Directory <> '' then
   begin
      FilesFileLb.Directory:=FileSelectionPanel.Directory;
      FilesFileLb.Update;
      FilesFileLb.Mask:=FilterStr;
   end
   else
      FilesFileLb.Mask:='*.dummy';
end;

end.
