unit AXImpExpDbcImpl;

interface

uses
  Windows, ActiveX, Classes, Controls, Graphics, Menus, Forms, StdCtrls,
  ComServ, StdVCL, AXCtrls, AXImpExpDbcXControl_TLB, ExtCtrls, ADODB,
  SysUtils, StrFile,TextImpTable;

const DOEXPORT = 0;
      DOIMPORT = 1;

type
  pColumn_rec = ^Column_rec;
  Column_rec = record
     Columnname  :string;
     ColumnType  :TDatabaseColTypes;
     ColumnLength:integer;
     DoIgnoreColumn:boolean; // flag if column should be ignored
     HasData     :boolean;   // flag that shows if column contains data
     IsCreated   :boolean;   // flag that shows if column has been created during this import
     next        :pColumn_rec;
  end;
  pInfo_rec = ^Info_rec;
  Info_rec = record
     ColName   :string;
     Info      :string;
     next      :pInfo_rec;
  end;

  pMapping_rec = ^Mapping_rec;
  Mapping_rec = record
     OriginalName:string;
     MappedName  :string;
     next        :pMapping_rec;
  end;

  pTable_rec = ^Table_rec;
  Table_rec = record
     Layername:string;              // name of the layer to that the table belongs
     OriginalLayername:string;      // name of the layer in the project
     Tablename:string;              // name of the table
     needtobecreated :boolean;      // flag if table has to be created
     hadbeencreated  :boolean;      // flag that shows if table current had been created
     opened          :boolean;      // flag if table has been opened
     ProgisID        :string;       // name of the Column of the Progis-ID
     ProgisIDset     :boolean;      // shows if the ProgisID has been set
     IgnoreData      :boolean;      // shows if data should be ignored
     DoPost          :boolean;      // shows if it should be posted before insert
     UseLayerTable   :string;       // redirection if layer is more layers use same table
     isdeleted       :boolean;      // flag that shows if corresponding TTable object already is deleted
     IndexInList     :integer;      // index of corresponding TADOTable in ADOTable List
     TmpdBaseName    :string;       // Name of the tempoary dBase file (if necessary)
     HasMappedCols   :boolean;      // Flag if table contains mapped columns
     ProcessedRecCnt :integer;      // Counter how many records still have been processed (needed for refresh of Table)
     Columns         :pColumn_rec;  // Pointer to list where tablecolumns are stored
     UpdateColumns   :pColumn_rec;  // Pointer to list of tablecolumns if the table structure should be updated
     Colmappings     :pMapping_rec; // Pointer to Column mapping list for this table
     Infos           :pInfo_rec;    // Pointer to temporary list of information
     Current_Column  :pColumn_rec;  // Pointer to current Column
     Current_Info    :pInfo_rec;    // Pointer to current Info
     next            :pTable_rec;
  end;

type
  TAXImpExpDbc = class(TActiveXControl, IAXImpExpDbc)
  private
    // private declarations
    FDelphiControl: TPanel;
    FEvents: IAXImpExpDbcEvents;

    // private variables
    ADOAccessImp    : TADOConnection;  // ADO-Reference to Access import tables
    ADOAccessExp    : TADOConnection;  // ADO-Reference to Access export tables
    ADOTable        : TADOTable;       // Reference to ADO-Table
    ADOCmdAccessImp : TADOCommand;     // ADO-Import Access SQL Command
    ADOdBaseImp     : TADOConnection;  // ADO-Reference to dBase import tables
    ADOdBaseExp     : TADOConnection;  // ADO-Reference to dBase export tables
    ADOCmddBaseImp  : TADOCommand;     // ADO-Import Access SQL Command
    ADOCmddBaseExp  : TADOCommand;     // ADO-Export dBase SQL Command
    ADOCmdAccessExp : TADOCommand;     // ADO-Export Access SQL Command

    TextImpTable    : TTextImpTable;   // reference to Text import table

    ImpTables       :pTable_rec;       // Pointer to List of import-Tables
    ExpTables       :pTable_rec;       // Pointer to List of export-Tables

    ImpLayerMappings:pMapping_rec;     // Pointer to List of import-Layermappings
    ImpMappingNr    :integer;          // Number for import-Layermappings
    ExpLayerMappings:pMapping_rec;     // Pointer to List of export-Layermappings
    ExpMappingNr    :integer;          // Number for export-Layermappings

    ADOAccessExpTables:TStrings;       // List of Tables that are in export Access-Database
    ADOAccessImpTables:TStrings;       // List of Tables that are in import Access-Database

    AdoExpTables   : array of TAdoTable;  // List of ADOTables for Export
    NumAdoExpTables: integer;             // number of Tables that are current used for Export
    AdoImpTables   : array of TAdoTable;  // List of ADOTables for Import
    NumAdoImpTables: integer;             // number of Tables that are current used for Import

    TextImpTables  : array of TTextImpTable; // List of Textfiles for Import
    NumTextImpTables:integer;               // number of Textfiles that are current used for Import

    IndexFixNr     : integer;

    FTmpDBaseDir   : string;              // Temporary dBase directory
    TmpDBaseImpNr  : integer;             // Postfix for temporary Import dBase databases
    TmpDBaseExpNr  : integer;             // Postfix for temporary Export dBase databases

    // properties to improve speed of GetTable method
    LastUsedImpTableLayername:string;
    LastUsedImpTableRec      :pTable_rec;
    LastUsedImpTable         :TAdoTable;
    LastUsedImpTextTable     :TTextImpTable;
    LastUsedExpTableLayername:string;
    LastUsedExpTableRec      :pTable_rec;
    LastUsedExpTable         :TAdoTable;

    StrFileObj  : ClassStrFile;

    // private variables for properties
    FWorkingDir  :string;               // current working-dir
    FLngCode     :string;               // current language-code

    FImpDatabase :string;               // used import database
    FImpTable    :string;               // used import database-table
    FImpDBMode   :TDatabaseTypes;       // used import database-mode

    FExpDatabase :string;               // used export database
    FExpTable    :string;               // used export database-table
    FExpDBMode   :TDatabaseTypes;       // used export database-mode
    FIgnoreNonExistExpTables:boolean;   // Flag that shows if not existing export tables should be ignored
    FAllowModifyImpData     :boolean;   // Flag that shows if already existing import-data can be modified
    FShowMode    :TDatabaseShowmodes;
    FCreateIndexOnClose:boolean;        // Flag that shows if Index on PROGIS-Id should be created on closing the table
    FRemoveEmptyColumnsOnClose:boolean; // Flag that shows if columns that have been left empty should be removed on close of table
    FRefreshThreshold :integer;         // Threshold after how many insert/update processed the Table has to be refreshed

    procedure CanResizeEvent(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure ClickEvent(Sender: TObject);
    procedure ConstrainedResizeEvent(Sender: TObject; var MinWidth, MinHeight,
      MaxWidth, MaxHeight: Integer);
    procedure DblClickEvent(Sender: TObject);
    procedure ResizeEvent(Sender: TObject);

    // private methods
    function  ReadFilterData(var myTable:pTable_rec;ID:integer):boolean;      // method to read just-read filter data
    function  ReadRawData(var myTable:pTable_rec):boolean;                    // method to read out just read raw table data
    procedure DeleteInfoData(var myTable:pTable_rec);                         // method to delete the info data of table
    procedure CloseTables(var TableList:pTable_rec;Mode:integer);             // method to free tables
    procedure AddInfo(var myTable:pTable_rec; Colname:string; Info:string);   // method to add info to table
    function  GetTableHandle(Layername:string;Mode:integer;IndexInList:integer):TADOTable;        // method is used to get handle of an ADO-Table
    procedure DeleteTable(Tablename:string;Mode:integer);                     // method is used to delete a table
    function  CheckColumnAndType(myTable:pTable_rec;Columnname:string; var Info:string):boolean; // method is used to check column and type
    function  AddColumn(var myTable:pTable_rec; Columnname:string; Columntype:TDatabaseColTypes;ColumnLength:integer):boolean; // method is used to add a column to Table
    function  AddUpdateColumn(var myTable:pTable_rec; Columnname:string; Columntype:TDatabaseColTypes; ColumnLength:integer):boolean; // method is used to add update column to Table
    function  InsertTable(var TableList:pTable_rec;Layername:string; OriginalLayername:string; Tablename:string; Tableexists:boolean;Mode:integer):pTable_rec; // method to add new table to list
    procedure FindProgisIDColName(var myTable:pTable_rec;Mode:integer); // method to find the PROGIS-ID column in a Table
    procedure GetRawTableColumnInformation(var myTable:pTable_rec;Mode:integer); // read out table column information of raw data table
    procedure CheckExpTableIsUsedByOtherLayer(var myTable:pTable_rec);  // method to check if a table is linked to more layers
    procedure InsertMappingEntry(var MapList:pMapping_rec;OriginalName:string; MappedName:string); // method is used to add a new mapping entry
    function  GetMappedName(var MapList:pMapping_rec; OriginalName:string):string; // method is used to get the mapped name of a layer
    function  GetTable(var TableList:pTable_rec;Layername:string;Mode:integer):pTable_rec; // method is used to get pointer to table object
    procedure MakeCorrectdBaseColumnname(var ColName:string);  // method to make correct dBase column name
    procedure MakeCorrectAccessColumnname(var ColName:string); // method to make correct Access column name
    procedure MakeCorrectFilename(var name:string);            // method to make correct filename
    procedure CreateAccessImpTable(var myTable:pTable_rec);    // method is used to create Access-import table
    procedure CheckImpColumnMappings(var aTable:pTable_rec; var aColname:string); // method is used to check if mapping for column has already been done for other table
    procedure ShowPercent(ObjectsToWork:integer; ObjectsWorked:integer); // method to raise percentage event
    procedure AlterAccessImpTable(var myTable:pTable_rec);     // method is used to alter Access-import table (if neccessary)
    procedure CreatedBaseImpTable(var myTable:pTable_rec);     // method is used to create dBase-import table
    procedure CreateTextImpTable(var myTable:pTable_rec);      // method is used to create header of text import table
    procedure CopyAFile(Source:string; Dest:string);                  // method is used to copy a file
    procedure SetupTableObjects;                                      // method is used to setup the ADO objects
    procedure AddIdAndCheckForIndexConflict(var myTable:pTable_rec; Id:integer); // method is used to check for index conflicts
    procedure OnDbWndClose(Sender: TObject; var Action: TCloseAction);
    procedure AssignInfoToField(var myTable:pTable_rec; const aColumnname:string; const aInfo:string);
    function  PostDataToTable(var myTable:pTable_rec):boolean;
    procedure SetRegForDBF;
  protected
    { Protected declarations }
    procedure DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage); override;
    procedure EventSinkChanged(const EventSink: IUnknown); override;
    procedure InitializeControl; override;
    function DrawTextBiDiModeFlagsReadingOnly: Integer; safecall;
    function Get_Alignment: TxAlignment; safecall;
    function Get_AutoSize: WordBool; safecall;
    function Get_BevelInner: TxBevelCut; safecall;
    function Get_BevelOuter: TxBevelCut; safecall;
    function Get_BorderStyle: TxBorderStyle; safecall;
    function Get_Caption: WideString; safecall;
    function Get_Color: OLE_COLOR; safecall;
    function Get_Ctl3D: WordBool; safecall;
    function Get_Cursor: Smallint; safecall;
    function Get_DockSite: WordBool; safecall;
    function Get_DoubleBuffered: WordBool; safecall;
    function Get_DragCursor: Smallint; safecall;
    function Get_DragMode: TxDragMode; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Font: IFontDisp; safecall;
    function Get_FullRepaint: WordBool; safecall;
    function Get_Locked: WordBool; safecall;
    function Get_ParentColor: WordBool; safecall;
    function Get_ParentCtl3D: WordBool; safecall;
    function Get_UseDockManager: WordBool; safecall;
    function Get_Visible: WordBool; safecall;
    function Get_VisibleDockClientCount: Integer; safecall;
    function IsRightToLeft: WordBool; safecall;
    function UseRightToLeftReading: WordBool; safecall;
    function UseRightToLeftScrollBar: WordBool; safecall;
    procedure _Set_Font(var Value: IFontDisp); safecall;
    procedure InitiateAction; safecall;
    procedure Set_Alignment(Value: TxAlignment); safecall;
    procedure Set_AutoSize(Value: WordBool); safecall;
    procedure Set_BevelInner(Value: TxBevelCut); safecall;
    procedure Set_BevelOuter(Value: TxBevelCut); safecall;
    procedure Set_BorderStyle(Value: TxBorderStyle); safecall;
    procedure Set_Caption(const Value: WideString); safecall;
    procedure Set_Color(Value: OLE_COLOR); safecall;
    procedure Set_Ctl3D(Value: WordBool); safecall;
    procedure Set_Cursor(Value: Smallint); safecall;
    procedure Set_DockSite(Value: WordBool); safecall;
    procedure Set_DoubleBuffered(Value: WordBool); safecall;
    procedure Set_DragCursor(Value: Smallint); safecall;
    procedure Set_DragMode(Value: TxDragMode); safecall;
    procedure Set_Enabled(Value: WordBool); safecall;
    procedure Set_Font(const Value: IFontDisp); safecall;
    procedure Set_FullRepaint(Value: WordBool); safecall;
    procedure Set_Locked(Value: WordBool); safecall;
    procedure Set_ParentColor(Value: WordBool); safecall;
    procedure Set_ParentCtl3D(Value: WordBool); safecall;
    procedure Set_UseDockManager(Value: WordBool); safecall;
    procedure Set_Visible(Value: WordBool); safecall;
    function Get_WorkingDir: WideString; safecall;
    procedure Set_WorkingDir(const Value: WideString); safecall;
    function Get_LngCode: WideString; safecall;
    procedure Set_LngCode(const Value: WideString); safecall;
    function Get_ImpDatabase: WideString; safecall;
    procedure Set_ImpDatabase(const Value: WideString); safecall;
    function Get_ImpTable: WideString; safecall;
    procedure Set_ImpTable(const Value: WideString); safecall;
    function Get_ImpDbMode: TDataBaseTypes; safecall;
    procedure Set_ImpDbMode(Value: TDataBaseTypes); safecall;
    function Get_ExpDatabase: WideString; safecall;
    procedure Set_ExpDatabase(const Value: WideString); safecall;
    function Get_ExpTable: WideString; safecall;
    procedure Set_ExpTable(const Value: WideString); safecall;
    function Get_ExpDbMode: TDataBaseTypes; safecall;
    procedure Set_ExpDbMode(Value: TDataBaseTypes); safecall;
    function Get_IgnoreNonExistExpTables: WordBool; safecall;
    procedure Set_IgnoreNonExistExpTables(Value: WordBool); safecall;
    function GetExportColumn(const Layername: WideString;
      out Columnname: WideString; out Columntype: TDataBaseColTypes;
      out ColumnLength: Integer): WordBool; safecall;
    function GetExportData(const Layername: WideString; out Columnname,
      Info: WideString): WordBool; safecall;
    function DoExportData(const Layername: WideString; Id: Integer): WordBool;
      safecall;
    procedure OpenExpTable(const Layername: WideString); safecall;
    procedure CloseExpTables; safecall;
    procedure CloseImpTables; safecall;
    procedure AddImpInfo(const Layername, Columnname, Info: WideString);
      safecall;
    procedure AddImpIdInfo(const Layername: WideString; Id: Integer); safecall;
    procedure OpenImpTable(const Layername: WideString); safecall;
    procedure CheckAndCreateImpTable(const Layername: WideString); safecall;
    procedure CheckAndCreateExpTable(const Layername: WideString); safecall;
    procedure AddImpIdColumn(const Layername: WideString); safecall;
    procedure AddImpColumn(const Layername, ColumnName: WideString;
      ColumnType: TDataBaseColTypes; ColumnLength: Integer); safecall;
    procedure CreateImpTable(const Layername: WideString); safecall;
    procedure DeleteExpDBEntry(const Layername: WideString; Id: Integer);
      safecall;
    procedure DeleteExpTable(const Layername: WideString); safecall;
    procedure SetupByDialog(ParentWinHandle, WinLeft, WinTop: Integer);
      safecall;
    function Get_ShowMode: TDatabaseShowmodes; safecall;
    procedure Set_ShowMode(Value: TDatabaseShowmodes); safecall;
    function Get_ImpDbInfo: WideString; safecall;
    function Get_ExpDbInfo: WideString; safecall;
    procedure DoEditImpInfo(const Layername: WideString; Id: Integer);
      safecall;
    function Get_AllowModifyImpData: WordBool; safecall;
    procedure Set_AllowModifyImpData(Value: WordBool); safecall;
    function DoExportRawData(const Layername: WideString): WordBool; safecall;
    function DoExportFromImpTable(const Layername: WideString;
      Id: Integer): WordBool; safecall;
    function GetExportDataFromImpTable(const Layername: WideString;
      out Columnname, Info: WideString): WordBool; safecall;
    function Get_CreateIndexOnClose: WordBool; safecall;
    procedure Set_CreateIndexOnClose(Value: WordBool); safecall;
    function GetImpNumRecords(const Layername: WideString): Integer; safecall;
    function GetExpNumRecords(const Layername: WideString): Integer; safecall;
    procedure SetupByModalDialog; safecall;
    function Get_RemoveEmptyColumnsOnClose: WordBool; safecall;
    procedure Set_RemoveEmptyColumnsOnClose(Value: WordBool); safecall;
    function Get_TmpDBaseDir: WideString; safecall;
    procedure Set_TmpDBaseDir(const Value: WideString); safecall;
    procedure EmptyExpTable(const Layername: WideString); safecall;
    function Get_RefreshThreshold: Integer; safecall;
    procedure Set_RefreshThreshold(Value: Integer); safecall;
  end;

implementation

uses ComObj, Dialogs, Db, SetTableDlg, SetIdDlg, SetDbDlg, FileCtrl, WinOSInfo, Registry;

{ TAXImpExpDbc }

procedure TAXImpExpDbc.DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage);
begin
  {TODO: Define property pages here.  Property pages are defined by calling
    DefinePropertyPage with the class id of the page.  For example,
      DefinePropertyPage(Class_AXImpExpDbcPage); }
end;

procedure TAXImpExpDbc.EventSinkChanged(const EventSink: IUnknown);
begin
  FEvents := EventSink as IAXImpExpDbcEvents;
end;

procedure TAXImpExpDbc.SetRegForDBF;
var
Reg: TRegistry;
AByte: Byte;
begin
     Reg:=TRegistry.Create;
     try
        Reg.RootKey:=HKEY_LOCAL_MACHINE;
        if Reg.OpenKey('SOFTWARE\Microsoft\Jet\4.0\Engines\xBase',False) then begin
           Reg.WriteInteger('BDE',2);
           Reg.WriteString('DataCodePage','ANSI');
           Reg.CloseKey;
        end;
        if Reg.OpenKey('SOFTWARE\Microsoft\Jet\4.0\ISAM Formats\dBase III',False) then begin
           AByte:=1;
           Reg.WriteBinaryData( 'SupportsLongNames',AByte,1);
           Reg.CloseKey;
        end;
        if Reg.OpenKey('SOFTWARE\Microsoft\Jet\4.0\ISAM Formats\dBase IV',False) then begin
           AByte:=1;
           Reg.WriteBinaryData( 'SupportsLongNames',AByte,1);
           Reg.CloseKey;
        end;
        if Reg.OpenKey('SOFTWARE\Microsoft\Jet\4.0\ISAM Formats\dBase 5.0',False) then begin
           AByte:=1;
           Reg.WriteBinaryData( 'SupportsLongNames',AByte,1);
           Reg.CloseKey;
        end;
     finally
        Reg.Free;
     end;
end;

procedure TAXImpExpDbc.InitializeControl;
begin
  FDelphiControl := Control as TPanel;
  FDelphiControl.OnCanResize := CanResizeEvent;
  FDelphiControl.OnClick := ClickEvent;
  FDelphiControl.OnConstrainedResize := ConstrainedResizeEvent;
  FDelphiControl.OnDblClick := DblClickEvent;
  FDelphiControl.OnResize := ResizeEvent;
  FDelphiControl.Height:=25;
  FDelphiControl.Width:=50;
  FDelphiControl.Caption:='GisDbc';

   NumAdoExpTables:=0;
   NumAdoImpTables:=0;

   FImpDatabase:='';
   FImpTable:='';
   FImpDBMode:=DB_NONE;

   FExpDatabase:='';
   FExpTable:='';
   FExpDBMode:=DB_NONE;
   FIgnoreNonExistExpTables:=FALSE;
   FAllowModifyImpData:=TRUE;

   ImpTables:=nil;
   ExpTables:=nil;
   ImpLayerMappings:=nil;
   ImpMappingNr:=0;
   ExpLayerMappings:=nil;
   ExpMappingNr:=0;

   ADOAccessImpTables:=TStringList.Create;
   ADOAccessExpTables:=TStringList.Create;

   StrFileObj:=nil;

   ADOdBaseImp:=nil;
   ADOCmddBaseImp:=nil;
   AdoAccessImp:=nil;
   ADOCmdAccessImp:=nil;
   ADOdBaseExp:=nil;
   ADOCmddBaseExp:=nil;
   ADOAccessExp:=nil;
   ADOCmdAccessExp:=nil;
   IndexFixNr:=666666666; // start with 666666666

   // reset the last used properties that are used for performance improvement of GetTable method
   LastUsedImpTableLayername:='';
   LastUsedImpTableRec:=nil;
   LastUsedImpTable:=nil;
   LastUsedExpTableLayername:='';
   LastUsedExpTableRec:=nil;
   LastUsedExpTable:=nil;

   FCreateIndexOnClose:=TRUE;        // For Default the Index on a Table will be created on closing the Table
   FRemoveEmptyColumnsOnClose:=TRUE; // For Default empty columns will be deleted on close of table

   FTmpDBaseDir:=OSInfo.TempDataDir;  // all tempoary dBase Tables will be kept in this directory
   TmpDBaseImpNr:=0;                  // Import dBase Postfix starts with 0
   TmpDBaseExpNr:=0;                  // Export dBase Postfix starts with 0
   FRefreshThreshold:=100;            // Default threshold for refresh of table is 100

   SetRegForDBF;
end;

function TAXImpExpDbc.DrawTextBiDiModeFlagsReadingOnly: Integer;
begin
  Result := FDelphiControl.DrawTextBiDiModeFlagsReadingOnly;
end;

function TAXImpExpDbc.Get_Alignment: TxAlignment;
begin
  Result := Ord(FDelphiControl.Alignment);
end;

function TAXImpExpDbc.Get_AutoSize: WordBool;
begin
  Result := FDelphiControl.AutoSize;
end;

function TAXImpExpDbc.Get_BevelInner: TxBevelCut;
begin
  Result := Ord(FDelphiControl.BevelInner);
end;

function TAXImpExpDbc.Get_BevelOuter: TxBevelCut;
begin
  Result := Ord(FDelphiControl.BevelOuter);
end;

function TAXImpExpDbc.Get_BorderStyle: TxBorderStyle;
begin
  Result := Ord(FDelphiControl.BorderStyle);
end;

function TAXImpExpDbc.Get_Caption: WideString;
begin
  Result := WideString(FDelphiControl.Caption);
end;

function TAXImpExpDbc.Get_Color: OLE_COLOR;
begin
  Result := OLE_COLOR(FDelphiControl.Color);
end;

function TAXImpExpDbc.Get_Ctl3D: WordBool;
begin
  Result := FDelphiControl.Ctl3D;
end;

function TAXImpExpDbc.Get_Cursor: Smallint;
begin
  Result := Smallint(FDelphiControl.Cursor);
end;

function TAXImpExpDbc.Get_DockSite: WordBool;
begin
  Result := FDelphiControl.DockSite;
end;

function TAXImpExpDbc.Get_DoubleBuffered: WordBool;
begin
  Result := FDelphiControl.DoubleBuffered;
end;

function TAXImpExpDbc.Get_DragCursor: Smallint;
begin
  Result := Smallint(FDelphiControl.DragCursor);
end;

function TAXImpExpDbc.Get_DragMode: TxDragMode;
begin
  Result := Ord(FDelphiControl.DragMode);
end;

function TAXImpExpDbc.Get_Enabled: WordBool;
begin
  Result := FDelphiControl.Enabled;
end;

function TAXImpExpDbc.Get_Font: IFontDisp;
begin
  GetOleFont(FDelphiControl.Font, Result);
end;

function TAXImpExpDbc.Get_FullRepaint: WordBool;
begin
  Result := FDelphiControl.FullRepaint;
end;

function TAXImpExpDbc.Get_Locked: WordBool;
begin
  Result := FDelphiControl.Locked;
end;

function TAXImpExpDbc.Get_ParentColor: WordBool;
begin
  Result := FDelphiControl.ParentColor;
end;

function TAXImpExpDbc.Get_ParentCtl3D: WordBool;
begin
  Result := FDelphiControl.ParentCtl3D;
end;

function TAXImpExpDbc.Get_UseDockManager: WordBool;
begin
  Result := FDelphiControl.UseDockManager;
end;

function TAXImpExpDbc.Get_Visible: WordBool;
begin
  Result := FDelphiControl.Visible;
end;

function TAXImpExpDbc.Get_VisibleDockClientCount: Integer;
begin
  Result := FDelphiControl.VisibleDockClientCount;
end;

function TAXImpExpDbc.IsRightToLeft: WordBool;
begin
  Result := FDelphiControl.IsRightToLeft;
end;

function TAXImpExpDbc.UseRightToLeftReading: WordBool;
begin
  Result := FDelphiControl.UseRightToLeftReading;
end;

function TAXImpExpDbc.UseRightToLeftScrollBar: WordBool;
begin
  Result := FDelphiControl.UseRightToLeftScrollBar;
end;

procedure TAXImpExpDbc._Set_Font(var Value: IFontDisp);
begin
  SetOleFont(FDelphiControl.Font, Value);
end;

procedure TAXImpExpDbc.CanResizeEvent(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
var
  TempNewWidth: Integer;
  TempNewHeight: Integer;
  TempResize: WordBool;
begin
  TempNewWidth := Integer(NewWidth);
  TempNewHeight := Integer(NewHeight);
  TempResize := WordBool(Resize);
  if FEvents <> nil then FEvents.OnCanResize(TempNewWidth, TempNewHeight, TempResize);
  NewWidth := Integer(TempNewWidth);
  NewHeight := Integer(TempNewHeight);
  Resize := Boolean(TempResize);
end;

procedure TAXImpExpDbc.ClickEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnClick;
end;

procedure TAXImpExpDbc.ConstrainedResizeEvent(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
var
  TempMinWidth: Integer;
  TempMinHeight: Integer;
  TempMaxWidth: Integer;
  TempMaxHeight: Integer;
begin
  TempMinWidth := Integer(MinWidth);
  TempMinHeight := Integer(MinHeight);
  TempMaxWidth := Integer(MaxWidth);
  TempMaxHeight := Integer(MaxHeight);
  if FEvents <> nil then FEvents.OnConstrainedResize(TempMinWidth, TempMinHeight, TempMaxWidth, TempMaxHeight);
  MinWidth := Integer(TempMinWidth);
  MinHeight := Integer(TempMinHeight);
  MaxWidth := Integer(TempMaxWidth);
  MaxHeight := Integer(TempMaxHeight);
end;

procedure TAXImpExpDbc.DblClickEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnDblClick;
end;

procedure TAXImpExpDbc.InitiateAction;
begin
  FDelphiControl.InitiateAction;
end;

procedure TAXImpExpDbc.ResizeEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnResize;
end;

procedure TAXImpExpDbc.Set_Alignment(Value: TxAlignment);
begin
  FDelphiControl.Alignment := TAlignment(Value);
end;

procedure TAXImpExpDbc.Set_AutoSize(Value: WordBool);
begin
  FDelphiControl.AutoSize := Value;
end;

procedure TAXImpExpDbc.Set_BevelInner(Value: TxBevelCut);
begin
  FDelphiControl.BevelInner := TBevelCut(Value);
end;

procedure TAXImpExpDbc.Set_BevelOuter(Value: TxBevelCut);
begin
  FDelphiControl.BevelOuter := TBevelCut(Value);
end;

procedure TAXImpExpDbc.Set_BorderStyle(Value: TxBorderStyle);
begin
  FDelphiControl.BorderStyle := TBorderStyle(Value);
end;

procedure TAXImpExpDbc.Set_Caption(const Value: WideString);
begin
  FDelphiControl.Caption := TCaption(Value);
end;

procedure TAXImpExpDbc.Set_Color(Value: OLE_COLOR);
begin
  FDelphiControl.Color := TColor(Value);
end;

procedure TAXImpExpDbc.Set_Ctl3D(Value: WordBool);
begin
  FDelphiControl.Ctl3D := Value;
end;

procedure TAXImpExpDbc.Set_Cursor(Value: Smallint);
begin
  FDelphiControl.Cursor := TCursor(Value);
end;

procedure TAXImpExpDbc.Set_DockSite(Value: WordBool);
begin
  FDelphiControl.DockSite := Value;
end;

procedure TAXImpExpDbc.Set_DoubleBuffered(Value: WordBool);
begin
  FDelphiControl.DoubleBuffered := Value;
end;

procedure TAXImpExpDbc.Set_DragCursor(Value: Smallint);
begin
  FDelphiControl.DragCursor := TCursor(Value);
end;

procedure TAXImpExpDbc.Set_DragMode(Value: TxDragMode);
begin
  FDelphiControl.DragMode := TDragMode(Value);
end;

procedure TAXImpExpDbc.Set_Enabled(Value: WordBool);
begin
  FDelphiControl.Enabled := Value;
end;

procedure TAXImpExpDbc.Set_Font(const Value: IFontDisp);
begin
  SetOleFont(FDelphiControl.Font, Value);
end;

procedure TAXImpExpDbc.Set_FullRepaint(Value: WordBool);
begin
  FDelphiControl.FullRepaint := Value;
end;

procedure TAXImpExpDbc.Set_Locked(Value: WordBool);
begin
  FDelphiControl.Locked := Value;
end;

procedure TAXImpExpDbc.Set_ParentColor(Value: WordBool);
begin
  FDelphiControl.ParentColor := Value;
end;

procedure TAXImpExpDbc.Set_ParentCtl3D(Value: WordBool);
begin
  FDelphiControl.ParentCtl3D := Value;
end;

procedure TAXImpExpDbc.Set_UseDockManager(Value: WordBool);
begin
  FDelphiControl.UseDockManager := Value;
end;

procedure TAXImpExpDbc.Set_Visible(Value: WordBool);
begin
  FDelphiControl.Visible := Value;
end;

function TAXImpExpDbc.Get_WorkingDir: WideString;
begin
   result:=FWorkingDir;
end;

procedure TAXImpExpDbc.Set_WorkingDir(const Value: WideString);
begin
   FWorkingDir:=Value;
end;

function TAXImpExpDbc.Get_LngCode: WideString;
begin
   result:=FLngCode;
end;

procedure TAXImpExpDbc.Set_LngCode(const Value: WideString);
begin
   FLngCode:=Value;
end;

function TAXImpExpDbc.Get_ImpDatabase: WideString;
begin
   result:=FImpDatabase;
end;

procedure TAXImpExpDbc.Set_ImpDatabase(const Value: WideString);
begin
   // check if the database exists
   FImpDatabase:=Value;
   if FImpDbMode = Dbf_Single then
   begin
      // Check if Directory exists where the dBase file should be created
   if not DirectoryExists(ExtractFilePath(FImpDatabase)) then begin FImpDbMode:=DB_NONE; FImpDataBase:=''; end;
   end
   else if FImpDbMode = Dbf_Directory then
   begin
      if not DirectoryExists(FImpDatabase) then begin FImpDbMode:=DB_NONE; FImpDataBase:=''; end;
   end
   else if FImpDbMode = Access_Single then
   begin
      if not DirectoryExists(ExtractFilePath(FImpDatabase)) then begin FImpDbMode:=DB_NONE; FImpDataBase:=''; end;
   end
   else if FImpDbMode = Access_Tables then
   begin
      if not DirectoryExists(ExtractFilePath(FImpDatabase)) then begin FImpDbMode:=DB_NONE; FImpDataBase:=''; end;
   end
   else if FImpDbMode = Idb_Database then
   begin
      if not DirectoryExists(ExtractFilePath(FImpDatabase)) then begin FImpDbMode:=DB_NONE; FImpDataBase:=''; end;
   end;
end;

function TAXImpExpDbc.Get_ImpTable: WideString;
begin
   result:=FImpTable;
end;

procedure TAXImpExpDbc.Set_ImpTable(const Value: WideString);
begin
   FImpTable:=Value;
end;

function TAXImpExpDbc.Get_ImpDbMode: TDataBaseTypes;
begin
   result:=FImpDBMode;
end;

procedure TAXImpExpDbc.Set_ImpDbMode(Value: TDataBaseTypes);
begin
   FImpDBMode:=Value;
end;

function TAXImpExpDbc.Get_ExpDatabase: WideString;
begin
   result:=FExpDatabase;
end;

procedure TAXImpExpDbc.Set_ExpDatabase(const Value: WideString);
begin
   if (FExpDbMode = Access_Single) or (FExpDbMode = Access_Tables) or (FExpDbMode = dbf_Single) or (FExpDbMode = Idb_Database) then
   begin
      // check if the ExpDatabase exists
      if not FileExists(Value) then
         FExpDbMode:=Db_None;
   end;
   if (FExpDbMode = dbf_Directory) then
   begin
      // check if the directory exists
      if not DirectoryExists(Value) then
         FExpDbMode:=Db_None;
   end;
   FExpDatabase:=Value;
end;

function TAXImpExpDbc.Get_ExpTable: WideString;
begin
   result:=FExpTable;
end;

procedure TAXImpExpDbc.Set_ExpTable(const Value: WideString);
begin
   FExpTable:=Value;
end;

function TAXImpExpDbc.Get_ExpDbMode: TDataBaseTypes;
begin
   result:=FExpDBMode;
end;

procedure TAXImpExpDbc.Set_ExpDbMode(Value: TDataBaseTypes);
begin
   FExpDBMode:=Value;
end;

function TAXImpExpDbc.Get_IgnoreNonExistExpTables: WordBool;
begin
   result:=FIgnoreNonExistExpTables;
end;

procedure TAXImpExpDbc.Set_IgnoreNonExistExpTables(Value: WordBool);
begin
   FIgnoreNonExistExpTables:=Value;
end;

// ------------------------------------------------------------------------
// NAME: GetExportColumn
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to get a columnname of an export table
//
// PARAMETERS : Layername    -> Name of the layer
//              Columnname   -> Name of the column out of the export table
//              Columntype   -> Type of the column
//              Columnlength -> Length of the column
//
// RETURNVALUE: TRUE  -> Database column received
//              FALSE -> No database column received
// ------------------------------------------------------------------------
function TAXImpExpDbc.GetExportColumn(const Layername: WideString;
  out Columnname: WideString; out Columntype: TDataBaseColTypes;
  out ColumnLength: Integer): WordBool;
var retVal:boolean;
    myTable:pTable_rec;
    aLayername:string;
begin
   aLayername:=Layername;
   ColumnName:='';
   ColumnType:=0;
   ColumnLength:=0;
   retVal:=false;
   if (FExpDBMode = DB_None) then
   begin
      result:=retVal;
      exit;
   end;

   // get mapped name
   aLayername:=GetMappedName(ExpLayerMappings,Layername);

   if FExpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.IgnoreData then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      // the PROGISID column will not be exported
      if myTable^.Current_Column^.Columnname = myTable^.ProgisID then
         myTable^.Current_Column:=myTable^.Current_Column^.next;

      if myTable^.Current_Column <> nil then
      begin
         ColumnName:=myTable^.Current_Column^.Columnname;
         ColumnType:=myTable^.Current_Column^.ColumnType;
         ColumnLength:=myTable^.Current_Column^.ColumnLength;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         retVal:=true;
      end;
   end
   else if FExpDBMode = DBF_Single_Raw then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.IgnoreData then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      if myTable^.Current_Column <> nil then
      begin
         ColumnName:=myTable^.Current_Column^.Columnname;
         ColumnType:=myTable^.Current_Column^.ColumnType;
         ColumnLength:=myTable^.Current_Column^.ColumnLength;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         retVal:=true;
      end;
   end
   else if FExpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.IgnoreData then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      // the PROGISID column will not be exported
      if myTable^.Current_Column^.Columnname = myTable^.ProgisID then
         myTable^.Current_Column:=myTable^.Current_Column^.next;

      if myTable^.Current_Column <> nil then
      begin
         ColumnName:=myTable^.Current_Column^.Columnname;
         ColumnType:=myTable^.Current_Column^.ColumnType;
         ColumnLength:=myTable^.Current_Column^.ColumnLength;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         retVal:=true;
      end;
   end
   else if FExpDBMode = DBF_Directory_Raw then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.IgnoreData then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      if myTable^.Current_Column <> nil then
      begin
         ColumnName:=myTable^.Current_Column^.Columnname;
         ColumnType:=myTable^.Current_Column^.ColumnType;
         ColumnLength:=myTable^.Current_Column^.ColumnLength;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         retVal:=true;
      end;
   end
   else if FExpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.IgnoreData then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      // the PROGISID column will not be exported
      if myTable^.Current_Column^.Columnname = myTable^.ProgisID then
         myTable^.Current_Column:=myTable^.Current_Column^.next;

      if myTable^.Current_Column <> nil then
      begin
         ColumnName:=myTable^.Current_Column^.Columnname;
         ColumnType:=myTable^.Current_Column^.ColumnType;
         ColumnLength:=myTable^.Current_Column^.ColumnLength;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         retVal:=true;
      end;
   end
   else if FExpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.IgnoreData then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      // the PROGISID column will not be exported
      if myTable^.Current_Column^.Columnname = myTable^.ProgisID then
         myTable^.Current_Column:=myTable^.Current_Column^.next;

      if myTable^.Current_Column <> nil then
      begin
         ColumnName:=myTable^.Current_Column^.Columnname;
         ColumnType:=myTable^.Current_Column^.ColumnType;
         ColumnLength:=myTable^.Current_Column^.ColumnLength;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         retVal:=true;
      end;
   end
   else if FExpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.IgnoreData then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      // the PROGISID column will not be exported
      if myTable^.Current_Column^.Columnname = myTable^.ProgisID then
         myTable^.Current_Column:=myTable^.Current_Column^.next;
      // the _WG_STATE column will not be exported
      if myTable^.Current_Column^.Columnname = '_WG_STATE' then
         myTable^.Current_Column:=myTable^.Current_Column^.next;
      // the _WG_INTERNAL_ID column will not be exported
      if myTable^.Current_Column^.Columnname = '_WG_INTERNAL_ID' then
         myTable^.Current_Column:=myTable^.Current_Column^.next;

      if myTable^.Current_Column <> nil then
      begin
         ColumnName:=myTable^.Current_Column^.Columnname;
         ColumnType:=myTable^.Current_Column^.ColumnType;
         ColumnLength:=myTable^.Current_Column^.ColumnLength;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         retVal:=true;
      end;
   end;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: ReadRawData
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to read out the current data of raw table
//
// PARAMETERS : myTable -> Pointer to current Table
//
// RETURNVALUE: TRUE    -> Could read data
//              FALSE   -> Could not read data
// ------------------------------------------------------------------------
function TAXImpExpDbc.ReadRawData(var myTable:pTable_rec):boolean;
var coliterate:pColumn_rec;
    infoiterate1,infoiterate2:pInfo_rec;
    Columnname:string;
    Info:string;
    retVal:boolean;
begin
   retVal:=true;

   // first the old information entries for this layer have to be deleted
   // first the old information entries for this layer have to be deleted
   DeleteInfoData(myTable);

   if ADOTable.Eof then
   begin
      retVal:=false;
      result:=retVal;
   end;

   if retVal then
   begin
      // the name of the columns are stored in the
      coliterate:=myTable^.Columns;
      while (coliterate <> nil) do
      begin
         Columnname:=coliterate^.Columnname;
         Info:=ADOTable.FieldByName(Columnname).AsString;

         // the information will be trimed on the left side to
         // avoid to huge information strings
         Info:=TrimRight(Info);
         AddInfo(myTable,Columnname, Info);

         coliterate:=coliterate^.next;
      end;
      AdoTable.Next; // set current to next entry
   end;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: DeleteInfoData
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to delete the Info-Items of the Table
//
// PARAMETERS : myTable -> Pointer to current Table
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAxImpExpDbc.DeleteInfoData(var myTable:pTable_rec);
var infoiterate1,infoiterate2:pInfo_rec;
begin
   infoiterate1:=myTable^.Infos;
   infoiterate2:=myTable^.Infos;
   while (infoiterate1 <> nil) do
   begin
      infoiterate2:=infoiterate1;
      infoiterate1:=infoiterate1^.next;
      dispose(infoiterate2);
   end;
   myTable^.Infos:=nil;
end;

// ------------------------------------------------------------------------
// NAME: ReadFilterData
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to read out the filter-data of the ADO-Table
//
// PARAMETERS : myTable -> Pointer to current Table
//              Id      -> Id of the object
//
// RETURNVALUE: TRUE    -> Could read Filterdata
//              FALSE   -> Could not read Filterdata
// ------------------------------------------------------------------------
function TAXImpExpDbc.ReadFilterData(var myTable:pTable_rec;ID:integer):boolean;
var coliterate:pColumn_rec;
    infoiterate1,infoiterate2:pInfo_rec;
    Columnname:string;
    Info:string;
    retVal:boolean;
begin
   retVal:=true;

   // first the old information entries for this layer have to be deleted
   DeleteInfoData(myTable);

   // first it will be checked if the entry has been found
   if ADOTable.FieldByName(myTable^.ProgisID).AsString <> inttostr(ID) then
   begin
      retVal:=false;
      result:=retVal;
   end;

   if retVal then
   begin
      // the name of the columns are stored in the
      coliterate:=myTable^.Columns;
      while (coliterate <> nil) do
      begin
         Columnname:=coliterate^.Columnname;
         if Columnname <> myTable^.ProgisID then // id column will not be exported
         begin
            Info:=ADOTable.FieldByName(Columnname).AsString;
            // the information will be trimed on the left side to
            // avoid to huge information strings
            Info:=TrimRight(Info);
            AddInfo(myTable,Columnname, Info);
         end;
         coliterate:=coliterate^.next;
      end;
   end;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: ShowPercent
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to raise percentage event
//
// PARAMETERS : ObjectsToWork -> Maximum of objects to process
//              ObjectsWorked -> Number of objects that are current processed
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.ShowPercent(ObjectsToWork:integer; ObjectsWorked:integer);
var percent:integer;
begin
   if ObjectsToWork > 0 then percent:= Round (ObjectsWorked / ObjectsToWork * 100 ) else percent:= 100;
   FEvents.OnShowPercent(percent);
end;

// ------------------------------------------------------------------------
// NAME: CloseTables
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to close the tables and dispose the List
//
// PARAMETERS : TableList -> Pointer to List of Tables
//              Mode      -> Mode of the List (IMP/EXP)
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.CloseTables(var TableList:pTable_rec;Mode:integer);
var tabiterate1,tabiterate2:pTable_rec;
    maydelete:boolean;
    StartGUID, CurrentGUID:widestring;
    IdbLayerList :array of string;
    IdbTableList :array of string;
    NumIdbLayerListItems:integer;
    i,j:integer;
    LayerIndex:integer;
    dummy:string;
    found:boolean;
    intval,count:integer;
    CommandStr:string;
    coliterate:pColumn_rec;
    ObjectsToWork, ObjectsWorked:integer;
begin
   NumIdbLayerListItems:=0;
   tabiterate1:=TableList;
   tabiterate2:=TableList;

   if (Mode = DOIMPORT) and (NumAdoImpTables > 0) then
   begin
      // Show info that tables have to be organisied
      if StrFileObj <> nil then  FEvents.OnInfo(StrFileObj.Assign(33)); // organisize tables
      ObjectsToWork:=NumAdoImpTables;
      ObjectsWorked:=0;
   end;

   while (tabiterate1 <> nil) do
   begin
      if (Mode = DOIMPORT) and (FImpDBMode = Text_File) then
      begin
         TextImpTable:= TextImpTables[tabiterate1^.IndexInList];
         if tabiterate1^.DoPost then
            TextImpTable.CloseLine;
         TextImpTable.Destroy;
         TextImpTables[tabiterate1^.IndexInList]:=nil;
      end
      else
      if (
         ((Mode = DOIMPORT) and ((FImpDBMode = DBF_Single) or (FImpDBMode = DBF_Directory))) or
         ((Mode = DOEXPORT) and ((FExpDBMode = DBF_Single) or (FExpDBMode = DBF_Single_Raw) or (FExpDBMode = DBF_Directory) or (FExpDBMode = DBF_Directory_Raw)))
         ) then
      begin
         if tabiterate1^.opened then
         begin
            ADOTable:=GetTableHandle(tabiterate1^.Layername,Mode,tabiterate1^.IndexInList);
            if tabiterate1^.DoPost then
            begin
               PostDataToTable(tabiterate1);
            end;

            maydelete:=false;
            if (ADOTable.Recordcount = 0) and (Mode = DOIMPORT) then
            begin
               // add dummy field that table can be deleted
               ADOTable.Append;
               ADOTable.FieldByName(tabiterate1^.ProgisID).AsString:='1';
               PostDataToTable(tabiterate1);
               maydelete:=true;
            end;

            if (Mode = DOEXPORT) then
            begin
               ADOTable.Close;
               ADOTable.Free; ADOTable:=nil;
            end;

            if (Mode = DOIMPORT) then
            begin
               ADOTable.Close;
               ADOTable.Free; ADOTable:=nil;
            end;

            if maydelete then
            begin
               DeleteTable(tabiterate1^.TmpdBaseName, DOIMPORT);
            end
            else
            begin
               if Mode = DoImport then
               begin
                  // the table has to be copied back to the original position
                  CopyAFile(FTmpDBaseDir+tabiterate1^.TmpdBaseName, tabiterate1^.Tablename);
               end;
            end;
         end
         else
         begin
            ADOTable:=GetTableHandle(tabiterate1^.Layername,Mode, tabiterate1^.IndexInList);
            if AdoTable <> nil then
            begin
               ADOTable.Free; ADOTable:=nil;
            end;
         end;
         // remove the temporary database if it does already exist
         if (FileExists(FTmpDBaseDir+tabiterate1^.TmpdBaseName)) then
         begin
            DeleteFile(FTmpDBaseDir+tabiterate1^.TmpdBaseName);
         end;
      end;

      if (
         ((Mode = DOIMPORT) and ((FImpDBMode = ACCESS_Single) or (FImpDBMode = ACCESS_Tables))) or
         ((Mode = DOEXPORT) and ((FExpDBMode = ACCESS_Single) or (FExpDBMode = ACCESS_Tables))) or
         ((Mode = DOIMPORT) and (FImpDBMode = Idb_Database)) or
         ((Mode = DOEXPORT) and (FExpDBMode = Idb_Database))
         ) then
      begin
         // add entry to layerlist
         NumIdbLayerListItems:=NumIdbLayerListItems+1;
         SetLength(IdbLayerList,NumIdbLayerListItems);
         SetLength(IdbTableList,NumIdbLayerListItems);
         IdbLayerList[NumIdbLayerListitems-1]:=tabiterate1^.OriginalLayername;
         IdbTableList[NumIdbLayerListitems-1]:=tabiterate1^.Tablename;

         if tabiterate1^.opened then
         begin
            ADOTable:=GetTableHandle(tabiterate1^.Layername,Mode, tabiterate1^.IndexInList);
            if tabiterate1^.DoPost then
               PostDataToTable(tabiterate1);

            maydelete:=false;
            if (ADOTable.Recordcount = 0) and (Mode = DOIMPORT) then
            begin
               // add dummy field that table can be deleted
               ADOTable.Append;
               ADOTable.FieldByName(tabiterate1^.ProgisID).AsString:='1';
               PostDataToTable(tabiterate1);
               maydelete:=true;
            end;
            if (Mode = DOEXPORT) then
            begin
               ADOTable.Close;
               ADOTable.Free; ADOTable:=nil;
            end;

            if Mode = DOIMPORT then
            begin
               ADOTable.Close;
               ADOTable.Free; ADOTable:=nil;

               if (not maydelete) and (FCreateIndexOnClose) and (tabiterate1^.hadbeencreated) then
               begin
                  // the index has to be created on the PROGIS-ID Field
                  // now set the PROGISID column as index
                  if FImpDbMode = Idb_Database then
                     CommandStr:='Create Index GisIndex on '+ '['+tabiterate1^.Tablename+']' + ' (_WG_INTERNAL_ID) WITH PRIMARY'
                  else
                     CommandStr:='Create Index GisIndex on '+ '['+tabiterate1^.Tablename+']' + ' ('+tabiterate1^.ProgisID+') with PRIMARY';
                  ADOCmdAccessImp.CommandText:=Commandstr;
                  ADOCmdAccessImp.Execute;
               end;

               if (not maydelete) and (FRemoveEmptyColumnsOnClose) then
               begin
                  // remove empty columns from table
                  coliterate:=tabiterate1^.Columns;
                  while (coliterate <> nil) do
                  begin
                     // column has no data and is not a reserved column, and has been current created it may be dropped from table
                     if (not coliterate^.HasData) and (coliterate^.Columnname <> tabiterate1^.ProgisID) and
                        (coliterate^.Columnname <> 'GUID') and
                        (coliterate^.Columnname <> '_WG_STATE') and (coliterate^.Columnname <> '_WG_INTERNAL_ID') and
                        (not coliterate^.DoIgnoreColumn) and (coliterate^.IsCreated) then
                     begin
                        // remove column from table
                        CommandStr:='ALTER TABLE ['+tabiterate1^.Tablename+'] DROP COLUMN ['+coliterate^.Columnname+'];';
                        ADOCmdAccessImp.CommandText:=Commandstr;
                        ADOCmdAccessImp.Execute;
                     end;
                     coliterate:=coliterate^.next;
                  end;
               end;
            end;

            if maydelete then
            begin
               DeleteTable(tabiterate1^.Tablename, DOIMPORT);
            end;
         end
         else
         begin
            ADOTable:=GetTableHandle(tabiterate1^.Layername,Mode, tabiterate1^.IndexInList);
            if AdoTable <> nil then
            begin
               ADOTable.Free; ADOTable:=nil;
            end;
         end;
      end;
      tabiterate2:=tabiterate1;
      tabiterate1:=tabiterate1^.next;
      if (Mode = DOIMPORT) and (FImpDBMode <> Text_File) then
      begin
         ObjectsWorked:=ObjectsWorked+1;
         ShowPercent(ObjectsToWork, ObjectsWorked);
      end;
      dispose(tabiterate2);
   end;
   if Mode = DOIMPORT then
   begin
      NumAdoImpTables:=0;
      SetLength(AdoImpTables,NumAdoImpTables);
      // close ADO connection to Access Database
      if (FImpDBMode = ACCESS_Single) or (FImpDbMode = ACCESS_Tables) or (FImpDbMode = Idb_Database) then
      begin
         if ADOAccessImp = nil then exit;
         if ADOAccessImp.Connected then
         begin
            if FImpDBMode = Idb_Database then
            begin
               // if it has been written to a Idb_Database the GUID�s have
               // to be set
               FEvents.OnRequestImpProjectGUIDs(StartGUID, CurrentGUID);
               // open Table USYS_WGIDB_SPTT where GUID�s are stored
               ADOTable:=TADOTable.Create(nil);
               ADOTable.Name:='IDBLAYER';
               AdoTable.Tablename:='[USYS_WGIDB_SPTT]';
               ADOTable.Connection:=ADOAccessImp;
               ADOTable.active:=true;
               ADOTable.First;
               while (not ADOTable.Eof) do
               begin
                  if ADOTable.FieldByName('SPID').AsString = '0' then
                  begin
                     // set StartGUID Value
                     ADOTable.Edit;
                     ADOTable.FieldByName('_WG_SPF').AsString:=StartGUID;
                     ADOTable.Post;
                  end
                  else if ADOTable.FieldByName('SPID').AsString = '1' then
                  begin
                     // set StartGUID Value
                     ADOTable.Edit;
                     ADOTable.FieldByName('_WG_SPF').AsString:=CurrentGUID;
                     ADOTable.Post;
                  end;
                  ADOTable.Next;
               end;
               ADOTable.Close;
               ADOTable.Free; ADOTable:=nil;

               // get the current table names
               ADOAccessImpTables.Clear;
               ADOAccessImp.GetTableNames(ADOAccessImpTables,False);

               found:=FALSE;
               // check if the USYS_WGIDB_LTN table exists
               for i:=0 to ADOAccessImpTables.Count-1 do
               begin
                  if ADOAccessImpTables[i] = 'USYS_WGIDB_LTN' then
                  begin
                     found:=TRUE;
                     break;
                  end;
               end;
               if not found then
               begin
                  // create the USYS_WGIDB_LTN Table
                  ADOCmdAccessImp.CommandText:='Create Table [USYS_WGIDB_LTN] ([INDEX] integer, [TABLENAME] Text(64), [LAYERNAME] Text(64))';
                  ADOCmdAccessImp.Execute;
                  found:=TRUE; // table exists now
               end;
               if found then
               begin
                  // setup the database-index to layername relationship table
                  ADOTable:=TADOTable.Create(nil);
                  ADOTable.Name:='IDBLAYER';
                  AdoTable.Tablename:='[USYS_WGIDB_LTN]';
                  ADOTable.Connection:=ADOAccessImp;
                  ADOTable.active:=true;
                  for i:=0 to NumIdbLayerListItems-1 do
                  begin
                     // check if the table exists
                     found:=FALSE;
                     for j:=0 to ADOAccessImpTables.Count - 1 do
                     begin
                        if ADOAccessImpTables[j] = IdbTableList[i] then
                        begin
                           found:=TRUE;
                           break;
                        end;
                     end;
                     if not found then
                        continue;

                     if Mode = DOIMPORT then
                        FEvents.OnRequestImpLayerIndex(IdbLayerList[i],LayerIndex);
                     if Mode = DOEXPORT then
                        FEvents.OnRequestExpLayerIndex(IdbLayerList[i],LayerIndex);
                     // now check if an item for this LayerIndex already exists
                     found:=FALSE;
                     ADOTable.First;
                     while (not ADOTable.Eof) do
                     begin
                        dummy:=ADOTable.FieldByName('INDEX').AsString;
                        val(dummy, intval, count); if count <> 0 then intval:=0;
                        if (intval = LayerIndex) then
                           found:=TRUE;
                        ADOTable.Next;
                     end;
                     if not found then
                     begin
                        // add new entry for layer
                        ADOTable.Append;
                        ADOTable.FieldByName('INDEX').AsString:=inttostr(LayerIndex);
                        ADOTable.FieldByName('TABLENAME').AsString:=IdbTableList[i];
                        ADOTable.FieldByName('LAYERNAME').AsString:=IdbLayerList[i];
                        ADOTable.Post;
                     end;
                  end;
                  ADOTable.Close;
                  ADOTable.Free;
                  ADOTable:=nil;
               end;
            end;
            ADOAccessImp.Close;
            ADOAccessImp.Connected:=false;
         end;
      end;
      // close ADO connection to dBase Database
      if (FImpDBMode = DBF_Single) or (FImpDbMode = DBF_Directory) then
      begin
         if ADOdBaseImp = nil then exit;
         if ADOdBaseImp.Connected then
         begin
            ADOdBaseImp.Close;
            ADOdBaseImp.Connected:=false;
         end;
      end;
   end;
   if Mode = DOEXPORT then
   begin
      NumAdoExpTables:=0;
      SetLength(AdoExpTables,NumAdoExpTables);
      // close ADO connection to Access Database
      if (FExpDBMode = ACCESS_Single) or (FExpDbMode = ACCESS_Tables) or (FExpDbMode = Idb_Database) then
      begin
         if ADOAccessExp = nil then exit;
         if ADOAccessExp.Connected then
         begin
            ADOAccessExp.Close;
            ADOAccessExp.Connected:=false;
         end;
      end;
      // close ADO connection to dBase Database
      if (FExpDBMode = DBF_Single) or  (FExpDbMode = DBF_Single_Raw) or (FExpDbMode = DBF_Directory) or (FExpDbMode = DBF_Directory_Raw) then
      begin
         if ADOdBaseExp=nil then exit;
         if ADOdBaseExp.Connected then
         begin
            ADOdBaseExp.Close;
            ADOdBaseExp.Connected:=false;
         end;
      end;
   end;
   SetLength(IdbLayerList,0); SetLength(IdbTableList,0);

   TableList:=nil;
   // destroy the table objects
   if (Mode = DOIMPORT) then
   begin
      if ADOdBaseImp <> nil then ADOdBaseImp.Free; ADOdBaseImp:=nil;
      if ADOCmddBaseImp <> nil then ADOCmddBaseImp.Free; ADOCmddBaseImp:=nil;
      if AdoAccessImp <> nil then AdoAccessImp.Free; AdoAccessImp:=nil;
      if ADOCmdAccessImp <> nil then ADOCmdAccessImp.Free; ADOCmdAccessImp:=nil;

      // reset the last used properties that are used for performance improvement of GetTable method
      LastUsedImpTableLayername:='';
      LastUsedImpTableRec:=nil;
      LastUsedImpTable:=nil;
      LastUsedImpTextTable:=nil;

      NumAdoImpTables:=0; SetLength(AdoImpTables,0);
      NumTextImpTables:=0; SetLength(TextImpTables,0);
   end;

   if (Mode = DOEXPORT) then
   begin
      if ADOdBaseExp <> nil then ADOdBaseExp.Free; ADOdBaseExp:=nil;
      if ADOCmddBaseExp <> nil then ADOCmddBaseExp.Free; ADOCmddBaseExp:=nil;
      if ADOAccessExp <> nil then ADOAccessExp.Free; ADOAccessExp:=nil;
      if ADOCmdAccessExp <> nil then ADOCmdAccessExp.Free; ADOCmdAccessExp:=nil;

      // reset the last used properties that are used for performance improvement of GetTable method
      LastUsedExpTableLayername:='';
      LastUsedExpTableRec:=nil;
      LastUsedExpTable:=nil;

      NumAdoExpTables:=0; SetLength(AdoExpTables,0);
   end;

   if StrFileObj <> nil then
   begin
      StrFileObj.Destroy;
      StrFileObj:=nil;
   end;
end;

// ------------------------------------------------------------------------
// NAME: AddInfo
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to add information to Table
//
// PARAMETERS : myTable -> Pointer to current Table
//              Info    -> Information that should be added
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.AddInfo(var myTable:pTable_rec; ColName:string; Info:string);
var new_entry:pInfo_rec;
    iterate:pInfo_rec;
begin
   iterate:=myTable^.Infos;
   //new column has to be created
   new(new_entry);
   new_entry^.ColName:=ColName;
   new_entry^.Info:=Info;

   if myTable^.Infos = nil then
   begin
      // first entry
      myTable^.Infos:= new_entry; myTable^.Infos^.next:=NIL;
      iterate:=new_entry;
   end
   else
   begin
      iterate:=myTable^.Infos;
      while (iterate^.next <> nil) do iterate:=iterate^.next;
      iterate^.next:=new_entry;
      new_entry^.next:=nil;
      iterate:=new_entry;
   end;
end;

// ------------------------------------------------------------------------
// NAME: GetTableHandle
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to get ADO Table Handle
//
// PARAMETERS : Layername     -> Name of the layer
//              Mode          -> Mode of the List (IMP/EXP)
//              Index in List -> Index of the TAdoTable in the List
//
// RETURNVALUE: Pointer to ADO-Table
// ------------------------------------------------------------------------
function TAXImpExpDbc.GetTableHandle(Layername:string;Mode:integer;IndexInList:integer):TADOTable;
var ObjName:string;
begin
   if (NumAdoExpTables = 0) and (Mode = DOEXPORT) then
   begin
      result:=nil;
      exit;
   end;
   if (NumAdoImpTables = 0) and (Mode = DOIMPORT) then
   begin
      result:=nil;
      exit;
   end;
   if Mode = DOIMPORT then
   begin
      ObjName:='IMPLAYER'+LayerName;
      // check if the Index is correct
      if (IndexInList < NumAdoImpTables) then
      begin
         if (AdoImpTables[IndexInList].Name <> ObjName) then
            Showmessage('Problem in GetTableHandle of Importtable - Index seems not to be correct!')
         else
            result:=AdoImpTables[IndexInList];
      end
      else
         result:=nil;
   end;
   if Mode = DOEXPORT then
   begin
      ObjName:='EXPLAYER'+LayerName;
      if (IndexInList < NumAdoExpTables) then
      begin
         // check if the Index is correct
         if (AdoExpTables[IndexInList].Name <> ObjName) then
            Showmessage('Problem in GetTableHandle of Exporttable - Index seems not to be correct!')
         else
            result:=AdoExpTables[IndexInList];
      end
      else
         result:=nil;
   end;
end;

// ------------------------------------------------------------------------
// NAME: DeleteTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to delete table
//
// PARAMETERS : Tablename -> Name of the layer that should be deleted
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.DeleteTable(Tablename:string; Mode:integer);
var  CommandStr:string;
begin
   if (FImpDBMode = DB_None) then exit;
   if Mode = DOIMPORT then
   begin
      if FImpDBMode = DBF_Single then
      begin
         Commandstr:='Drop Table '+ ExtractFilename(Tablename);
         ADOCmddBaseImp.CommandText:=Commandstr;
         ADOCmddBaseImp.Execute;
      end
      else if FImpDBMode = DBF_Directory then
      begin
         Commandstr:='Drop Table '+ ExtractFilename(Tablename);
         ADOCmddBaseImp.CommandText:=Commandstr;
         ADOCmddBaseImp.Execute;
      end
      else if FImpDBMode = ACCESS_Single then
      begin
         Commandstr:='Drop Table '+ '['+Tablename+']';
         ADOCmdAccessImp.CommandText:=Commandstr;
         ADOCmdAccessImp.Execute;
      end
      else if FImpDBMode = ACCESS_Tables then
      begin
         Commandstr:='Drop Table '+ '['+Tablename+']';
         ADOCmdAccessImp.CommandText:=Commandstr;
         ADOCmdAccessImp.Execute;
      end
      else if FImpDBMode = Idb_Database then
      begin
         Commandstr:='Drop Table '+ '['+Tablename+']';
         ADOCmdAccessImp.CommandText:=Commandstr;
         ADOCmdAccessImp.Execute;
      end;
   end
   else if Mode = DOEXPORT then
   begin
      if FExpDBMode = DBF_Single then
      begin
         Commandstr:='Drop Table '+ '['+ExtractFilename(Tablename)+']';
         ADOCmddBaseExp.CommandText:=Commandstr;
         ADOCmddBaseExp.Execute;
      end
      else if FExpDBMode = DBF_Directory then
      begin
         Commandstr:='Drop Table '+ '['+ExtractFilename(Tablename)+']';
         ADOCmddBaseExp.CommandText:=Commandstr;
         ADOCmddBaseExp.Execute;
      end
      else if FExpDBMode = ACCESS_Single then
      begin
         Commandstr:='Drop Table '+ '['+Tablename+']';
         ADOCmdAccessExp.CommandText:=Commandstr;
         ADOCmdAccessExp.Execute;
      end
      else if FExpDBMode = ACCESS_Tables then
      begin
         Commandstr:='Drop Table '+ '['+Tablename+']';
         ADOCmdAccessExp.CommandText:=Commandstr;
         ADOCmdAccessExp.Execute;
      end
      else if FExpDBMode = Idb_Database then
      begin
         Commandstr:='Drop Table '+ '['+Tablename+']';
         ADOCmdAccessExp.CommandText:=Commandstr;
         ADOCmdAccessExp.Execute;
      end;
   end;
end;

// ------------------------------------------------------------------------
// NAME: CheckColumnAndType
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to check if the information is correct for a
//              specified column and table.
//
// PARAMETERS : myTable    -> Pointer to Table
//              Columnname -> Name of the Database Column
//              Info       -> Information that should be checked
//
// RETURNVALUE: TRUE  -> Information is correct
//              FALSE -> Information is not correct
// ------------------------------------------------------------------------
function TAXImpExpDbc.CheckColumnAndType(myTable:pTable_rec;Columnname:string; var Info:string):boolean;
var retVal:boolean;
    iterate:pColumn_rec;
    found:boolean;
    intdummy:integer;
    smallintdummy:smallint;
    count:integer;
    floatdummy:double;
begin
   retVal:=false;
   //first the Columndefinition for this Column has to be found
   found:=false;
   iterate:=myTable^.Columns;
   while (iterate <> nil) and (not found) do
   begin
      if uppercase(iterate^.Columnname) = uppercase(Columnname) then found:=TRUE
      else iterate:=iterate^.next;
   end;

   if found then
   begin
      //now the type has to be checked
      if iterate^.DoIgnoreColumn then // column should be ignored
      begin
         retVal:=false;
      end
      else if iterate^.ColumnType = Col_int then // check integer type
      begin
         val(Info,intdummy,count); if count = 0 then retVal:=TRUE;
      end
      else if iterate^.ColumnType = Col_String then // check character type
      begin
         if (length(Info) > 0) and (length(Info) <= iterate^.ColumnLength) then retVal:=TRUE;
         if (length(Info) > iterate^.ColumnLength) then Info:=copy(Info,1,iterate^.ColumnLength);
         retVal:=TRUE;
      end
      else if iterate^.ColumnType = Col_Smallint then // check smallint type
      begin
         val(Info,smallintdummy,count); if count = 0 then retVal:=TRUE;
      end
      else if iterate^.ColumnType = Col_Float then // check float type
      begin
         // remove the ',' signs and replace it with '.' to make correct
         // validation
         Info:=StringReplace(Info,',','.',[rfreplaceall]);
         val(Info,floatdummy,count); if count = 0 then retVal:=TRUE;
         Info:=floattostr(floatdummy);
      end
      else if iterate^.ColumnType = Col_Date then // check date type
      begin
         retVal:=TRUE;
      end;
      if (retVal = TRUE) and (Info <> '') then // Check if Info is Ok and not empty
      begin
         iterate^.HasData:=TRUE; // --> Column contains info
      end;
   end;
   result:=retVal;
end;


// ------------------------------------------------------------------------
// NAME: AddUpdateColumn
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to add a update Column to table
//
// PARAMETERS : myTable      -> Pointer to Table
//              Columnname   -> Name of the Database Column
//              ColumnType   -> Type of column
//              ColumnLength -> Length of column
//
// RETURNVALUE: TRUE  -> Column could be inserted and does not exist
//              FALSE -> Column already exists and could not be inserted
// ------------------------------------------------------------------------
function TAXImpExpDbc.AddUpdateColumn(var myTable:pTable_rec; Columnname:string; Columntype:TDatabaseColTypes; ColumnLength:integer):boolean;
var new_entry:pColumn_rec;
    iterate:pColumn_rec;
    exist:boolean;
begin
   //if column still exists return FALSE otherwise TRUE
   exist:=false;
   iterate:=myTable^.UpdateColumns;
   while (iterate <> nil) and (not exist) do
   begin
      if iterate^.Columnname = Columnname then exist:=true
      else iterate:=iterate^.next;
   end;
   if not exist then
   begin
      //new column has to be created
      new(new_entry);
      new_entry^.Columnname:=Columnname;
      new_entry^.ColumnType:=Columntype;
      new_entry^.ColumnLength:=ColumnLength;
      new_entry^.DoIgnoreColumn:=FALSE;
      new_entry^.HasData:=FALSE;
      new_entry^.IsCreated:=FALSE;

      if myTable^.UpdateColumns = nil then
      begin
         // first entry
         myTable^.UpdateColumns:= new_entry; myTable^.UpdateColumns^.next:=NIL;
         iterate:=new_entry;
      end
      else
      begin
         iterate:=myTable^.UpdateColumns;
         while (iterate^.next <> nil) do iterate:=iterate^.next;
         iterate^.next:=new_entry;
         new_entry^.next:=nil;
         iterate:=new_entry;
      end;
   end;
   result:=not exist;
end;

// ------------------------------------------------------------------------
// NAME: AddColumn
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to add a Column to table
//
// PARAMETERS : myTable      -> Pointer to Table
//              Columnname   -> Name of the Database Column
//              ColumnType   -> Type of column
//              ColumnLength -> Length of column
//
// RETURNVALUE: TRUE  -> Column could be inserted and does not exist
//              FALSE -> Column already exists and could not be inserted
// ------------------------------------------------------------------------
function TAXImpExpDbc.AddColumn(var myTable:pTable_rec; Columnname:string; Columntype:TDatabaseColTypes; ColumnLength:integer):boolean;
var new_entry:pColumn_rec;
    iterate:pColumn_rec;
    exist:boolean;
begin
   //if column still exists return FALSE otherwise TRUE
   exist:=false;
   iterate:=myTable^.Columns;
   while (iterate <> nil) and (not exist) do
   begin
      if iterate^.Columnname = Columnname then exist:=true
      else iterate:=iterate^.next;
   end;
   if not exist then
   begin
      //new column has to be created
      new(new_entry);
      new_entry^.Columnname:=Columnname;
      new_entry^.ColumnType:=Columntype;
      new_entry^.ColumnLength:=ColumnLength;
      new_entry^.DoIgnoreColumn:=FALSE;
      new_entry^.HasData:=FALSE;
      new_entry^.IsCreated:=FALSE;

      if myTable^.Columns = nil then
      begin
         // first entry
         myTable^.Columns:= new_entry; myTable^.Columns^.next:=NIL;
         iterate:=new_entry;
      end
      else
      begin
         iterate:=myTable^.Columns;
         while (iterate^.next <> nil) do iterate:=iterate^.next;
         iterate^.next:=new_entry;
         new_entry^.next:=nil;
         iterate:=new_entry;
      end;
      myTable^.Current_Column:=new_entry;
   end;
   result:=not exist;
end;

// ------------------------------------------------------------------------
// NAME: InsertTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to add new Table
//
// PARAMETERS : TableList         -> List into that the new table should be added
//              Layername         -> Name of the Layer that belongs to this table
//              OriginalLayername -> Original name of the linked Layer in GIS
//              Tablename         -> Name for the Table
//              TableExists       -> Flag if table already exists
//              Mode              -> Mode of the Table (IMP/EXP)
//
// RETURNVALUE: Pointer to new created table
// ------------------------------------------------------------------------
function TAXImpExpDbc.InsertTable(var TableList:pTable_rec;Layername:string; OriginalLayername:string; Tablename:string; Tableexists:boolean;Mode:integer):pTable_rec;
var iterate:pTable_rec;
    new_entry:pTable_rec;
    exist:boolean;
    mr:integer;
    i :integer;
begin
   exist:=false;

   //first it will be checked if an entry in the tablelist has to be added
   iterate:=TableList;
   while (iterate <> nil) and (not exist) do
   begin
      if iterate^.Layername = Layername then exist:=true
      else iterate:=iterate^.next;
   end;

   if exist then // if the table already exists -> do nothing and return
   begin
      result:=iterate;
      exit;
   end;

   //a new table object has to be created
   new(new_entry);
   new_entry^.Layername:=Layername;
   new_entry^.OriginalLayername:=OriginalLayername;
   new_entry^.Tablename:=Tablename;
   new_entry^.UseLayerTable:='';
   new_entry^.opened:=false;
   new_entry^.DoPost:=false;
   new_entry^.ProgisIDset:=false;
   new_entry^.Columns:=nil;
   new_entry^.UpdateColumns:=nil;
   new_entry^.Infos:=nil;
   new_entry^.Colmappings:=nil;
   new_entry^.isdeleted:=FALSE;
   new_entry^.hadbeencreated:=FALSE;
   new_entry^.TmpdBaseName:='';
   new_entry^.HasMappedCols:=FALSE;
   new_entry^.ProcessedRecCnt:=0;
   if TableList = nil then
   begin
      // first entry
      TableList:= new_entry; TableList^.next:=NIL;
      iterate:=new_entry;
   end
   else
   begin
      // insert to bottom of list
      iterate:=TableList;
      while (iterate^.next <> nil) do iterate:=iterate^.next;
      iterate^.next:=new_entry;
      new_entry^.next:=nil;
      iterate:=new_entry;
   end;

   if (Mode = DOIMPORT) and (FImpDBMode = Text_File) then
   begin
      // Create new TTextImpTable object
      TextImpTable:=TTextImpTable.Create;   // reference to Text import table
      TextImpTable.Filename:=iterate^.Tablename;
      TextImpTable.Delimeter:=FImpTable; // table name contains seperator here

      NumTextImpTables:=NumTextImpTables+1;
      SetLength(TextImpTables, NumTextImpTables);
      TextImpTables[NumTextImpTables-1]:=TextImpTable;
      iterate^.IndexInList:=NumTextImpTables-1;

      iterate^.needtobecreated:=true;
      iterate^.ProgisID:='PROGISID'; //default for ID-Columnname
      iterate^.ProgisIDset:=true;
      iterate^.IgnoreData:=false;

      result:=iterate;
      exit;
   end;

   // if it is not an text-import file -> handle ado-tables

   // an ADO Table has to be created
   ADOTable:=TADOTable.Create(nil);

   // add the ADO-Table to the List
   if Mode = DOIMPORT then
   begin
      NumAdoImpTables:=NumAdoImpTables+1;
      SetLength(AdoImpTables, NumAdoImpTables);
      AdoImpTables[NumAdoImpTables-1]:=ADOTable;
      iterate^.IndexInList:=NumAdoImpTables-1;
   end;
   if Mode = DOEXPORT then
   begin
      NumAdoExpTables:=NumAdoExpTables+1;
      SetLength(AdoExpTables, NumAdoExpTables);
      AdoExpTables[NumAdoExpTables-1]:=ADOTable;
      iterate^.IndexInList:=NumAdoExpTables-1;
   end;

   if Mode = DOIMPORT then  // Setup import databases
   begin
      ADOTable.Tablename:=Tablename;
      ADOTable.Name:='IMPLAYER'+Layername;
      ADOTable.Filtered:=true; // to allow also exports on import tables

      if ((FImpDBMode = ACCESS_Single) or (FImpDBMode = ACCESS_Tables)) then
      begin
         // setup import Access tables
         AdoTable.Tablename:='['+Tablename+']';
         ADOTable.Connection:=ADOAccessImp;
      end
      else if ((FImpDBMode = DBF_Single) or (FImpDBMode = DBF_Directory)) then
      begin
         // temporary import databases will be created in temporary directory
         new_entry^.TmpdBaseName:='imp'+inttostr(TmpDBaseImpnr)+'.dbf';
         ADOTable.Tablename:='['+new_entry^.TmpdBaseName+']';
         TmpDBaseImpNr:=TmpDBaseImpNr+1; // increment temporary dBase database postfix
         ADOTable.ConnectionString:='Provider=MSDASQL.1;Persist Security Info=False;Data Source=dBASE Files';
         ADOTable.Connection:=ADOdBaseImp;
      end
      else if (FImpDBMode = Idb_Database) then
      begin
         AdoTable.Tablename:='['+Tablename+']';
         ADOTable.Connection:=ADOAccessImp;
      end;

      if TableExists then
      begin
         if ((FImpDBMode = DBF_Single) or (FImpDBMode = DBF_Directory)) then
         begin
            // before processing dBase tables have to be copied to the temporary directory
            CopyAFile(Tablename, FTmpDBaseDir+new_entry^.TmpdBaseName);
         end;
         FindProgisIDColName(iterate,DOIMPORT); // due to table already exists find the Progis-Id
         iterate^.needtobecreated:=false;
      end
      else
      begin
         iterate^.needtobecreated:=true;
         iterate^.ProgisID:='PROGISID'; //default for ID-Columnname
         iterate^.ProgisIDset:=true;
         iterate^.IgnoreData:=false;
         if (FImpDBMode = Idb_Database) then
            iterate^.ProgisID:='ProgisID'; // default for Idb is ProgisID
         ADOTable.FieldDefs.Clear;
      end;
   end;

   if Mode = DOEXPORT then     // setup export databases
   begin
      ADOTable.Tablename:=Tablename;
      ADOTable.Name:='EXPLAYER'+Layername;
      ADOTable.Filtered:=true;
      if ((FExpDBMode = ACCESS_Single) or (FExpDBMode = ACCESS_Tables)) then
      begin
         ADOTable.Tablename:='['+Tablename+']';
         ADOTable.Connection:=ADOAccessExp;
      end
      else if ((FExpDBMode = DBF_Single) or (FExpDBMode = DBF_Single_Raw) or (FExpDBMode = DBF_Directory) or (FExpDBMode = DBF_Directory_Raw)) then
      begin
         // temporary import databases will be created in temporary directory
         new_entry^.TmpdBaseName:='exp'+inttostr(TmpDBaseExpNr)+'.dbf';
         ADOTable.Tablename:='['+new_entry^.TmpdBaseName+']';
         TmpDBaseExpNr:=TmpDBaseExpNr+1; // increment temporary dBase database postfix
         ADOTable.ConnectionString:='Provider=MSDASQL.1;Persist Security Info=False;Data Source=dBASE Files';
         ADOTable.Connection:=ADOdBaseExp;
      end
      else if (FExpDBMode = Idb_Database) then
      begin
         ADOTable.Tablename:='['+Tablename+']';
         ADOTable.Connection:=ADOAccessExp;
      end;

      if Tableexists then
      begin
         if ((FExpDBMode = DBF_Single) or (FExpDBMode = DBF_Single_Raw) or (FExpDBMode = DBF_Directory) or (FExpDBMode = DBF_Directory_Raw)) then
         begin
            // before processing dBase tables have to be copied to the temporary directory
            CopyAFile(Tablename, FTmpDBaseDir+new_entry^.TmpdBaseName);
         end;
         CheckExpTableIsUsedByOtherLayer(iterate);
         if (FExpDBMode = DBF_Single_Raw) or (FExpDbMode = DBF_Directory_Raw) then
            GetRawTableColumnInformation(iterate, DOEXPORT)
         else
            FindProgisIDColName(iterate,DOEXPORT);
      end
      else
      begin
         // table out of that should be exported does not exist
         // set table for export data
         // it no table has been set the data for this layer
         // has to be ignored
         // if a table has been set it must be checked if there is still
         // not other layer that uses this table to avoid conflicts
         // at open and close

         //show window that table has not been found
         SetTableWnd:=TSetTableWnd.Create(nil);
         SetTableWnd.WorkingDir:=FWorkingDir;
         SetTableWnd.LngCode:=FLngCode;
         SetTableWnd.LayerLabel.Caption:='Layer:'+OriginalLayername;
         SetTableWnd.SetTableSettings(FExpDBMode,FExpDatabase);
         // add the tables that are possible to the SetTable Window
         if (FExpDBMode = ACCESS_Tables) or (FExpDBMode = ACCESS_Single) or (FExpDBMode = Idb_Database) then
         begin
            for i:=0 to ADOAccessExpTables.Count-1 do
               SetTableWnd.AccessTableLB.Items.Add(ADOAccessExpTables[i]);
         end;
         mr:=SetTableWnd.ShowModal;
         if (SetTableWnd.CloseType = 1) and (SetTableWnd.IgnoreTablesCb.Checked = FALSE) then
         begin
            iterate^.Tablename:=SetTableWnd.GetTableName;
            if (FExpDBMode = DBF_Single) or (FExpDBMode = DBF_Directory) then
            begin
               // ADOTable.Tablename keeps the same for dBase objects
            end;
            if (FExpDBMode = ACCESS_Single) or (FExpDBMode = ACCESS_Tables) then
            begin
               ADOTable.Tablename:='['+iterate^.Tablename+']';
               ADOTable.Connection:=ADOAccessExp;
            end;
            if (FExpDBMode = Idb_Database) then
            begin
               ADOTable.Tablename:='['+iterate^.Tablename+']';
               ADOTable.Connection:=ADOAccessExp;
            end;
            // now it must be checked if this Table is not used by another Layer
            if ((FExpDBMode = DBF_Single) or (FExpDBMode = DBF_Single_Raw) or (FExpDBMode = DBF_Directory) or (FExpDBMode = DBF_Directory_Raw)) then
            begin
               // before processing dBase tables have to be copied to the temporary directory
               CopyAFile(iterate^.Tablename, FTmpDBaseDir+new_entry^.TmpdBaseName);
            end;
            CheckExpTableIsUsedByOtherLayer(iterate);
         end
         else
         begin
            // Database information for this layer has to be ignored
            iterate^.IgnoreData:=true;
            if (SetTableWnd.IgnoreTablesCb.Checked) then
               FIgnoreNonExistExpTables:=TRUE
            else
               if StrFileObj <> nil then Showmessage(StrfileObj.Assign(5)); // Database information for this layer will be ignored
            ADOTable.Free; // TTable object can be deleted
            ADOTable:=nil;
            NumAdoExpTables:=NumAdoExpTables-1;
            SetLength(AdoExpTables,NumAdoExpTables);
         end;
         SetTableWnd.Free;
      end;
   end;
   result:=iterate;
end;

// ------------------------------------------------------------------------
// NAME: GetRawTableColumnInformation
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to read out header information of raw table
//              data.
//
// PARAMETERS : myTable -> pointer to table
//              Mode    -> Mode of the Table (IMP/EXP)
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.GetRawTableColumnInformation(var myTable:pTable_rec;Mode:integer);
var i:integer;
    found:boolean;
    Columnname:string;
    OriColumnname:string;
    Columntype:TDatabaseColTypes;
    mr:integer;
    ColLength:integer;
begin
   // first it will be checked if the Column information already has been read out
   if myTable^.Columns <> nil then
   begin
      myTable^.Current_Column:=myTable^.Columns;
      exit;
   end;

   //first the table has to be opened
   ADOTable.Open;
   for i := 0 to ADOTable.FieldCount - 1 do // add the columns to the column-list
   begin
      OriColumnname:=ADOTable.Fields[i].FieldName;
      ColLength:=ADOTable.Fields[i].Size;

           if (ADOTable.Fields[i].DataType = ftInteger)  then ColumnType:=Col_Int
      else if (ADOTable.Fields[i].DataType = ftSmallint) then ColumnType:=Col_SmallInt
      else if (ADOTable.Fields[i].DataType = ftFloat)    then ColumnType:=Col_Float
      else if (ADOTable.Fields[i].DataType = ftDate)     then ColumnType:=Col_Date
      else ColumnType:=Col_String;
      // add column to columnlist of table
      AddColumn(myTable,OriColumnname,ColumnType,ColLength);
   end;
   myTable^.Current_Column:=myTable^.Columns;
   ADOTable.Close;
end;

// ------------------------------------------------------------------------
// NAME: FindProgisIDColName
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to find PROGIS-ID column an a Table
//
// PARAMETERS : myTable -> pointer to table
//              Mode    -> Mode of the Table (IMP/EXP)
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.FindProgisIDColName(var myTable:pTable_rec;Mode:integer);
var i:integer;
    found:boolean;
    Columnname:string;
    OriColumnname:string;
    Columntype:TDatabaseColTypes;
    mr:integer;
    ColLength:integer;
begin
   // first it will be checked if the ID-Column still has been set
   if myTable^.ProgisIDset then
   begin
      myTable^.Current_Column:=myTable^.Columns;
      exit;
   end;

   //first the table has to be opened
   found:=false;

   ADOTable.Open;

   for i := 0 to ADOTable.FieldCount - 1 do { Einf�gen der Spaltennamen in die Datenbankliste }
   begin
      OriColumnname:=ADOTable.Fields[i].FieldName; Columnname:=uppercase(OriColumnname);
      ColLength:=ADOTable.Fields[i].Size;

           if (ADOTable.Fields[i].DataType = ftInteger)  then ColumnType:=Col_Int
      else if (ADOTable.Fields[i].DataType = ftSmallint) then ColumnType:=Col_SmallInt
      else if (ADOTable.Fields[i].DataType = ftFloat)    then ColumnType:=Col_Float
      else if (ADOTable.Fields[i].DataType = ftDate)     then ColumnType:=Col_Date
      else ColumnType:=Col_String;

      //now the name and the column will be checked if this could be an id column
      if ((Columnname = 'PROGISID') or
         (Columnname = 'FEATUREID') or
         (Columnname = 'WINGISID') or
         (Columnname = 'ID')) and (not myTable^.ProgisIDset) then
      begin
         myTable^.ProgisID:=OriColumnname;
         myTable^.ProgisIDset:=true;
         myTable^.IgnoreData:=false;
         found:=true;
      end;

      // add column to columnlist of table
      AddColumn(myTable,OriColumnname,ColumnType,ColLength);
   end;
   if not found then
   begin
      // no id-column found - user has to set the id-column
      SetidWnd:=TSetIdWnd.Create(nil);
      SetidWnd.WorkingDir:=FWorkingDir;
      SetidWnd.LngCode:=FLngCode;
      // now the numeric columns will be inserted into the ID-List
      for i := 0 to ADOTable.FieldCount - 1 do { Einf�gen der Spaltennamen in die Datenbankliste }
      begin
         Columnname:=ADOTable.Fields[i].FieldName;
         if (ADOTable.Fields[i].DataType = ftInteger) or
            (ADOTable.Fields[i].DataType = ftFloat)   or
            (ADOTable.Fields[i].DataType = ftString) then
         begin
            SetIdWnd.IdLB.Items.Add(Columnname);
         end;
      end;
      mr:=0;
      if (SetIdWnd.IdLB.Items.Count > 0) then
      begin
         SetIdWnd.IdLB.ItemIndex := 0;
         SetIdWnd.LayerLabel.Caption:='Layer:'+myTable^.OriginalLayername;
         mr:=SetIdWnd.Showmodal;
         mr:=SetIdWnd.CloseType;
         if mr = 1 then
         begin
            myTable^.ProgisID:=SetIdWnd.GetIDColName;
            myTable^.ProgisIDset:=true;
            myTable^.IgnoreData:=false;
         end;
      end;
      if mr = 0 then
      begin
         if StrFileObj <> nil then ShowMessage(StrfileObj.Assign(4));
         myTable^.ProgisID:='';
         myTable^.ProgisIDset:=true;
         myTable^.IgnoreData:=true;
         ADOTable:=GetTableHandle(myTable^.Layername,Mode, myTable^.IndexInList);
      end;
   end;
   myTable^.Current_Column:=myTable^.Columns;
   ADOTable.Close;
end;

// ------------------------------------------------------------------------
// NAME: CheckExpTableIsUsedByOtherLayer
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to check if a Table is linked to more Layers
//
// PARAMETERS : myTable -> pointer to table
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.CheckExpTableIsUsedByOtherLayer(var myTable:pTable_rec);
var iterate:pTable_rec;
    found:boolean;
begin
   iterate:=ExpTables;
   found:=false;
   while (iterate <> nil) and (not found) do
   begin
      if (iterate^.Tablename = myTable^.Tablename) and (iterate^.Layername <> myTable^.Layername) then
      begin
         //another Layer uses same Table
         if (FExpDBMode = DBF_Single) or (FExpDBMode = DBF_Directory) or
            (FExpDBMode = ACCESS_Single) or (FExpDBMode = ACCESS_Tables) then
         begin
            ADOTable:=GetTableHandle(myTable^.Layername,DOEXPORT,myTable^.IndexInList);
            ADOTable.Free; ADOTable:=nil; // TTable object can be deleted
            NumAdoExpTables:=NumAdoExpTables-1;
            SetLength(AdoExpTables,NumAdoExpTables);
         end;

         myTable^.Tablename:='';
         myTable^.UseLayerTable:=iterate^.Layername;
         ADOTable:=GetTablehandle(iterate^.Layername,DOEXPORT, iterate^.IndexInList);

         if (FExpDBMode = DBF_Single)    or (FExpDBMode = DBF_Directory) or
            (FExpDBMode = ACCESS_Single) or (FExpDBMode = ACCESS_Tables) then
            FindProgisIDColName(iterate,DOEXPORT);

         found:=true;
      end;
      iterate:=iterate^.next;
   end;
   if not found then
   begin
      if (FExpDBMode = DBF_Single)    or (FExpDBMode = DBF_Directory) or
         (FExpDBMode = ACCESS_Single) or (FExpDBMode = ACCESS_Tables) then
         FindProgisIDColName(myTable,DOEXPORT);
   end;
end;

// ------------------------------------------------------------------------
// NAME: InsertMappingEntry
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to add new mapping entry
//
// PARAMETERS : MapList      -> Pointer to Mapping List
//              OriginalName -> Original name of item
//              MappedName   -> Mapped name of item
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.InsertMappingEntry(var MapList:pMapping_rec;OriginalName:string; MappedName:string);
var iterate:pMapping_rec;
    new_entry:pMapping_rec;
    found:boolean;
begin
   // first it must be checked if an entry for the Originalname exists
   iterate:=MapList;
   found:=false;
   while (iterate <> nil) and (not found) do
   begin
      if iterate^.OriginalName = OriginalName then
         found:=true
      else
         iterate:=iterate^.next;
   end;
   if not found then
   begin
      // insert new mapping entry
      new(new_entry);
      new_entry^.OriginalName:=OriginalName;
      new_entry^.MappedName:=MappedName;

      if MapList = nil then
      begin
         // first entry
         MapList:= new_entry;
         MapList^.next:=NIL;
      end
      else
      begin
         iterate:=MapList;
         while (iterate^.next <> nil) do iterate:=iterate^.next;
         iterate^.next:=new_entry;
         new_entry^.next:=nil;
      end;
   end;
end;

// ------------------------------------------------------------------------
// NAME: GetMappedName
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to get the mapped name of the ADO-Table
//              that belongs to a Layer,
//
// PARAMETERS : MapList      -> Pointer to Mapping List
//              OriginalName -> Original name of the layer in GIS
//
// RETURNVALUE: Name of the ADO-Table object
// ------------------------------------------------------------------------
function TAXImpExpDbc.GetMappedName(var MapList:pMapping_rec;OriginalName:string):string;
var retVal:string;
    iterate:pMapping_rec;
    found:boolean;
    dummy1:string;
    dummy2:string;
begin
   retVal:=OriginalName;
   found:=false;
   iterate:=MapList;
   while (iterate <> nil) and (not found) do
   begin
      dummy1:=uppercase(iterate^.OriginalName);
      dummy2:=uppercase(OriginalName);
      if dummy1 = dummy2 then
      begin
         retVal:=iterate^.MappedName;
         found:=true;
      end
      else
         iterate:=iterate^.next;
   end;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: GetTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to get pointer to table object by using the
//              name of the layer.
//
// PARAMETERS : TableList -> Pointer to List of Tables
//              Layername -> Original name of the layer in GIS
//              Mode      -> Mode of Table (IMP/EXP)
//
// RETURNVALUE: Pointer to table object
// ------------------------------------------------------------------------
function TAXImpExpDbc.GetTable(var TableList:pTable_rec;Layername:string;Mode:integer):pTable_rec;
var iterate:pTable_rec;
    exist:boolean;
    myTable:pTable_rec;
begin
   if (Mode = DOIMPORT) then
   begin
      // check if last used import Table is same than current searched Table for Layer
      if (LastUsedImpTableLayername = Layername) then
      begin
         result:=LastUsedImpTableRec;
         if (FImpDbMode = Text_File) then
            TextImpTable:=LastUsedImpTextTable
         else
            ADOTable:=LastUsedImpTable;
         exit;
      end;
   end
   else if (Mode = DOEXPORT) then
   begin
      if (LastUsedExpTableLayername = Layername) then
      begin
         result:=LastUsedExpTableRec;
         ADOTable:=LastUsedExpTable;
         exit;
      end;
   end;

   exist:=false;
   //first it will be checked
   iterate:=TableList;
   while (iterate <> nil) and (not exist) do
   begin
      if iterate^.Layername = Layername then exist:=true
      else iterate:=iterate^.next;
   end;
   if iterate <> nil then
   begin
      if not iterate^.IgnoreData then
      begin
         if iterate^.UseLayerTable <> '' then
         begin
            // the other table has to be found
            result:=GetTable(TableList,iterate^.UseLayerTable,Mode);
            if result = nil then result:=iterate;
         end
         else
         begin
            if (MODE = DOIMPORT) and (FImpDbMode = Text_File) then
            begin
               if iterate^.IndexInList < NumTextImpTables then
                  TextImpTable:= TextImpTables[iterate^.IndexInList]
               else
                  TextImpTable:=nil; // incorrect index
            end
            else
               ADOTable:=GetTableHandle(Layername,Mode, iterate^.IndexInList);
            result:=iterate;
         end;
      end;
   end
   else
      result:=nil;

   // store last found settings to improve performace if it will be worked more steps with same table
   if Mode = DOIMPORT then
   begin
      LastUsedImpTableLayername:=Layername;
      LastUsedImpTableRec:=result;
      if (FImpDbMode = Text_File) then
         LastUsedImpTextTable:=TextImpTable
      else
         LastUsedImpTable:=ADOTable;
   end
   else if Mode = DOEXPORT then
   begin
      LastUsedExpTableLayername:=Layername;
      LastUsedExpTableRec:=result;
      LastUsedExpTable:=ADOTable;
   end;
end;

// ------------------------------------------------------------------------
// NAME: MakeCorrectdBaseColumnname
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to make correct dBase column name
//
// PARAMETERS : Colname -> Name of the column
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.MakeCorrectdBaseColumnname(var ColName:string);
var pdummy:PCHAR;
    pdummy2:PCHAR;
    i,pos:integer;
    res:string;
    canadd:boolean;
begin
   //function is used to set the layername that it can be set as database or controlname
   pdummy:=stralloc(length(ColName)+1); strpcopy(pdummy,ColName);
   pdummy2:=stralloc(length(ColName)+1); strpcopy(pdummy2,ColName);
   pos:=0;
   for i:=0 to strlen(pdummy)-1 do
   begin
      canadd:=true;
           if (Ord(pdummy[i]) >= Ord ('A')) and (Ord(pdummy[i]) <= Ord ('Z')) then canadd:=true
      else if (Ord(pdummy[i]) >= Ord ('a')) and (Ord(pdummy[i]) <= Ord ('z')) then canadd:=true
      else if (Ord(pdummy[i]) >= Ord ('0')) and (Ord(pdummy[i]) <= Ord ('9')) then canadd:=true
      else if (pdummy[i] = ' ') and (i = 0) then canadd:=FALSE
      else begin pdummy[i]:='_'; canadd:=TRUE; end;
      if (canadd) then
      begin
         pdummy2[pos]:=pdummy[i];
         pos:=pos+1;
      end;
   end;
   ColName:=strpas(pdummy2);
   ColName:=copy(ColName,1,pos);
   if length(ColName) > 10 then
      ColName:=copy(ColName,1,10);
   strdispose(pdummy); strdispose(pdummy2);
end;

// ------------------------------------------------------------------------
// NAME: MakeCorrectAccessColumnname
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to make correct Access column name
//
// PARAMETERS : Colname -> Name of the column
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.MakeCorrectAccessColumnname(var ColName:string);
var pdummy:PCHAR;
    pdummy2:PCHAR;
    i,pos:integer;
    res:string;
    canadd:boolean;
begin
   // remove special characters from name
   pdummy:=stralloc(length(ColName)+1); strpcopy(pdummy,ColName);
   pdummy2:=stralloc(length(ColName)+1); strpcopy(pdummy2,ColName);
   pos:=0;
   for i:=0 to strlen(pdummy)-1 do
   begin
      canadd:=true;
           if (Ord(pdummy[i]) >= Ord ('A')) and (Ord(pdummy[i]) <= Ord ('Z')) then canadd:=true
      else if (Ord(pdummy[i]) >= Ord ('a')) and (Ord(pdummy[i]) <= Ord ('z')) then canadd:=true
      else if (Ord(pdummy[i]) >= Ord ('0')) and (Ord(pdummy[i]) <= Ord ('9')) then canadd:=true
      else if (pdummy[i] = ' ') and (i = 0) then canadd:=FALSE
      else begin pdummy[i]:='_'; canadd:=TRUE; end;
      if (canadd) then
      begin
         pdummy2[pos]:=pdummy[i];
         pos:=pos+1;
      end;
   end;
   ColName:=strpas(pdummy2);
   ColName:=copy(ColName,1,pos);
   strdispose(pdummy); strdispose(pdummy2);
end;


// ------------------------------------------------------------------------
// NAME: MakeCorrectFilename
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to make correct filename
//
// PARAMETERS : name -> Name of the file
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.MakeCorrectFilename(var name:string);
var pdummy:PCHAR;
    pdummy2:PCHAR;
    i,pos:integer;
    res:string;
    canadd:boolean;
begin
   //function is used to make correct filename with name
   pdummy:=stralloc(length(name)+1); strpcopy(pdummy,name);
   pdummy2:=stralloc(length(name)+1); strpcopy(pdummy2,name);
   pos:=0;
   for i:=0 to strlen(pdummy)-1 do
   begin
      canadd:=true;
      if (pdummy[i] = '\') then canadd:=false;
      if (pdummy[i] = '/') then canadd:=false;
      if (pdummy[i] = ':') then canadd:=false;
      if (pdummy[i] = '*') then canadd:=false;
      if (pdummy[i] = '?') then canadd:=false;
      if (pdummy[i] = '"') then canadd:=false;
      if (pdummy[i] = '<') then canadd:=false;
      if (pdummy[i] = '>') then canadd:=false;
      if (pdummy[i] = '|') then canadd:=false;
      if (canadd) then
      begin
         pdummy2[pos]:=pdummy[i];
         pos:=pos+1;
      end;
   end;
   name:=strpas(pdummy2);
   name:=copy(name,1,pos);
   strdispose(pdummy); strdispose(pdummy2);
end;

// ------------------------------------------------------------------------
// NAME: AlterAccessImpTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to alter Access import table if neccessary
//
// PARAMETERS : myTable -> pointer to table object
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAxImpExpDbc.AlterAccessImpTable(var myTable:pTable_rec);
var iterate_ori:pColumn_rec;
    iterate_update:pColumn_rec;
    Commandstr:string;
    found:boolean;
    needtoaltertable:boolean;
    RetryCnt:integer;
    MappedColName:string;
    MapPostFix:integer;
    OriginalColName:string;
    haderror:boolean;
begin
   iterate_update:=myTable^.UpdateColumns;
   // check which columns do not exist in the original column definitions
   RetryCnt:=0; MappedColName:=''; MapPostFix:=0;
   while (iterate_update <> nil) do
   begin
      iterate_ori:=myTable^.Columns;
      found:=FALSE;
      while (iterate_ori <> nil) and (not found) do
      begin
         if iterate_ori^.Columnname = iterate_update^.Columnname then
            found:=TRUE;
         iterate_ori:=iterate_ori^.next;
      end;

      if found then
      begin
         if RetryCnt > 0 then // the column already exists due to mapping
         begin
            InsertMappingEntry(myTable^.Colmappings,OriginalColName, MappedColName);
         end;
      end;

      if not found then
      begin
         haderror:=FALSE;
         if RetryCnt = 0 then OriginalColName:=iterate_update^.Columnname; // store original column name
         Commandstr:='Alter Table '+'['+myTable^.Tablename+'] '+'ADD COLUMN ';
         if iterate_update^.ColumnType = Col_Int then  Commandstr:=Commandstr + '['+iterate_update^.Columnname+']'+' integer, '
         else if iterate_update^.ColumnType = Col_Float then Commandstr:=Commandstr + '['+iterate_update^.Columnname+']'+ ' double, '
         else if iterate_update^.ColumnType = Col_SmallInt then Commandstr:=Commandstr + '['+iterate_update^.Columnname+']'+ ' integer, '
         else if iterate_update^.ColumnType = Auto_Inc then Commandstr:=Commandstr + '['+iterate_update^.Columnname+']'+' Autoincrement, '
         else Commandstr:=Commandstr + '['+iterate_update^.Columnname+']'+ ' TEXT(' +inttostr(iterate_update^.Columnlength)+ '), ';

         Commandstr:=copy(Commandstr,1,length(Commandstr)-2);
         Commandstr:=Commandstr + ';';
         ADOCmdAccessImp.CommandText:=Commandstr;
         try
            ADOCmdAccessImp.Execute;
         except
            haderror:=TRUE;
            if RetryCnt = 0 then // creation of original columnname failed
            begin
               // remove special characters from name
               MappedColName:=OriginalColName;
               MakeCorrectAccessColumnname(MappedColName);
               iterate_update^.Columnname:=MappedColName;
               RetryCnt:=RetryCnt+1;
            end
            else if RetryCnt < 10 then
            begin
               MappedColName:='Field'+inttostr(MapPostFix);
               iterate_update^.Columnname:=MappedColName;
               MapPostFix:=MapPostFix+1;
               RetryCnt:=RetryCnt+1;
            end
            else
               iterate_update^.DoIgnoreColumn:=TRUE;
         end;

         if RetryCnt > 0 then
         begin
            if (not haderror) then
            begin
               InsertMappingEntry(myTable^.Colmappings,OriginalColName, MappedColName);
               myTable^.HasMappedCols:=TRUE;
               // Show warning that column had to be mapped
               if StrFileObj <> nil then FEvents.OnWarning(StrFileObj.Assign(31)+' '+OriginalColName+' '+StrFileObj.Assign(32)+' '+MappedColname);
            end;
         end;
         if iterate_update^.DoIgnoreColumn then // if Column could not be created -> Show warning
         begin
            iterate_update^.Columnname:=OriginalColName;
            // Table for layer: <layername> Could not add column: <column-name>
            if StrFileObj <> nil then FEvents.OnWarning(StrfileObj.Assign(28)+' '+myTable^.OriginalLayername+' '+StrFileObj.Assign(29)+' '+iterate_update^.Columnname);
         end;
         if not haderror then
         begin
            // the table has to be added to the column definition
            if (AddColumn(myTable, iterate_update^.Columnname, iterate_update^.ColumnType, iterate_update^.ColumnLength)) then
               myTable^.Current_Column^.IsCreated:=TRUE; // last column has been current created
         end;
      end;
      if (haderror = FALSE) or (iterate_update^.DoIgnoreColumn) then
      begin
         iterate_update:=iterate_update^.next;
         RetryCnt:=0; // reset the RetryCnt
      end;
   end;

   ADOTable.Connection:=ADOAccessImp;
   ADOTable.Tablename:='['+myTable^.Tablename+']';
end;

// ------------------------------------------------------------------------
// NAME: CreateAccessImpTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to create Access import table
//
// PARAMETERS : myTable -> pointer to table object
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.CreateAccessImpTable(var myTable:pTable_rec);
var iterate:pColumn_rec;
    Commandstr:string;
    RetryCnt:integer;
    haderror:boolean;
    MappedColName:string;
    OriginalColName:string;
    MapPostFix:integer;
begin
   iterate:=myTable^.Columns;
   // first create only table with Progis-Id column
   if FImpDbMode = Idb_Database then // Idb supports PROGISID that could be null
      Commandstr:='Create Table '+'['+myTable^.Tablename+']' + '(['+myTable^.ProgisID+']'+' integer)'
   else
      Commandstr:='Create Table '+'['+myTable^.Tablename+']' + '(['+myTable^.ProgisID+']'+' integer not null)';

   ADOCmdAccessImp.CommandText:=Commandstr;
   ADOCmdAccessImp.Execute;

   // now alter all other columns to the table
   Commandstr:='Alter Table '+'['+myTable^.Tablename+'] ';
   RetryCnt:=0; MappedColName:=''; MapPostFix:=0;
   while (iterate <> nil) do
   begin
      haderror:=FALSE;
      if iterate^.Columnname <> myTable^.ProgisID then
      begin
         if RetryCnt = 0 then
         begin
            CheckImpColumnMappings(myTable, iterate^.Columnname);
            OriginalColName:=iterate^.Columnname; // store original column name
         end;

         Commandstr:='Alter Table '+'['+myTable^.Tablename+'] ADD COLUMN ';
         if iterate^.ColumnType = Col_Int then  Commandstr:=Commandstr + '['+iterate^.Columnname+']'+' integer, '
         else if iterate^.ColumnType = Col_Float then Commandstr:=Commandstr + '['+iterate^.Columnname+']'+ ' double, '
         else if iterate^.ColumnType = Col_SmallInt then Commandstr:=Commandstr + '['+iterate^.Columnname+']'+ ' integer, '
         else if iterate^.ColumnType = Auto_Inc then Commandstr:=Commandstr + '['+iterate^.Columnname+']'+' Autoincrement, '
         else Commandstr:=Commandstr + '['+iterate^.Columnname+']'+ ' Text(' +inttostr(iterate^.Columnlength)+ '), ';
         // now execute the alter table command command
         Commandstr:=copy(Commandstr,1,length(Commandstr)-2);
         Commandstr:=Commandstr + ';';
         ADOCmdAccessImp.CommandText:=Commandstr;
         try
            ADOCmdAccessImp.Execute;
         except
            haderror:=TRUE;
            if RetryCnt = 0 then // creation of original columnname failed
            begin
               // remove special characters from name
               MappedColName:=OriginalColName;
               MakeCorrectAccessColumnname(MappedColName);
               iterate^.Columnname:=MappedColName;
               RetryCnt:=RetryCnt+1;
            end
            else if RetryCnt < 10 then
            begin
               MappedColName:='Field'+inttostr(MapPostFix);
               iterate^.Columnname:=MappedColName;
               MapPostFix:=MapPostFix+1;
               RetryCnt:=RetryCnt+1;
            end
            else
               iterate^.DoIgnoreColumn:=TRUE;
         end;
      end;
      if RetryCnt > 0 then
      begin
         if (not haderror) then
         begin
            InsertMappingEntry(myTable^.Colmappings,OriginalColName, MappedColName);
            myTable^.HasMappedCols:=TRUE;
            // Show warning that column had to be mapped
            // Column: <ori-colname> will be renamed to: <mapped-colname>
            if StrFileObj <> nil then FEvents.OnWarning(StrFileObj.Assign(31)+' '+OriginalColName+' '+StrFileObj.Assign(32)+' '+MappedColname);
         end;
      end;
      if iterate^.DoIgnoreColumn then // if Column could not be created -> Show warning
      begin
         iterate^.Columnname:=OriginalColName;
         // Table for layer: <layername> Could not add column: <column-name>
         if StrFileObj <> nil then FEvents.OnWarning(StrfileObj.Assign(28)+' '+myTable^.OriginalLayername+' '+StrFileObj.Assign(29)+' '+iterate^.Columnname);
      end;
      if (haderror = FALSE) or (iterate^.DoIgnoreColumn) then
      begin
         if (not iterate^.DoIgnoreColumn) then iterate^.IsCreated:=TRUE; // set flag that column has been created
         iterate:=iterate^.next;
         RetryCnt:=0; // reset the RetryCnt
      end;
   end;

   if not FCreateIndexOnClose then
   begin
      // now set the PROGISID column as index ( IDB uses _WG_INTERNAL_IG as index )
      if FImpDbMode = Idb_Database then
         CommandStr:='Create Index GisIndex on '+ '['+myTable^.Tablename+']' + ' (_WG_INTERNAL_ID) WITH PRIMARY'
      else
         CommandStr:='Create Index GisIndex on '+ '['+myTable^.Tablename+']' + ' ('+myTable^.ProgisID+') with PRIMARY';
      ADOCmdAccessImp.CommandText:=Commandstr;
      ADOCmdAccessImp.Execute;
   end;
   ADOTable.Connection:=ADOAccessImp;
   ADOTable.Tablename:='['+myTable^.Tablename+']';
end;

// ------------------------------------------------------------------------
// NAME: CreateTextImpTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to create header of text-file table
//
// PARAMETERS : myTable -> pointer to table object
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.CreateTextImpTable(var myTable:pTable_rec);
var iterate:pColumn_rec;
begin
   iterate:=myTable^.Columns;
   while (iterate <> nil) do
   begin
      TextImpTable.WriteData(iterate^.Columnname);
      iterate:=iterate^.next;
   end;
   TextImpTable.CloseLine;
end;

// ------------------------------------------------------------------------
// NAME: CreatedBaseImpTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to create dBase import table
//
// PARAMETERS : myTable -> pointer to table object
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.CreatedBaseImpTable(var myTable:pTable_rec);
var iterate:pColumn_rec;
    Commandstr:string;
    RetryCnt:integer;
    MappedColname:string;
    MapPostFix:integer;
    haderror:boolean;
    OriginalColname:string;
    MappingList:TStrings;
    i, colidx:integer;
begin
   // to create a table the current directory must be the
   // directory of the dbase-table
   ADOTable.ConnectionString:='Provider=MSDASQL.1;Persist Security Info=False;Data Source=dBASE Files';
   iterate:=myTable^.Columns;
   // first create the PROGISID
   Commandstr:='Create Table '+'['+myTable^.TmpdBaseName+']' + '(['+myTable^.ProgisID+']'+' integer)';
   ADOCmddBaseImp.CommandText:=Commandstr;
   try
      ADOCmddBaseImp.Execute;
   except
      myTable^.IgnoreData:=TRUE;  // table can not be created -> ignore table
      exit;
   end;

   // now add the columns to the table
   RetryCnt:=0; MappedColName:=''; MapPostFix:=0; MappingList:=TStringList.Create;
   while (iterate <> nil) do
   begin
      haderror:=FALSE;
      if iterate^.Columnname <> myTable^.ProgisID then
      begin
         if RetryCnt = 0 then
         begin
            CheckImpColumnMappings(myTable, iterate^.Columnname);
            OriginalColName:=iterate^.Columnname; // store original column name
            MappingList.Add(OriginalColName);
            MappedColName:=OriginalColName;
            MakeCorrectdBaseColumnname(MappedColName);
            iterate^.Columnname:=MappedColName;
         end;
         if iterate^.ColumnLength > 128 then iterate^.ColumnLength:=128; // dBase column may be maximum 128 long
         Commandstr:='Alter Table '+'['+myTable^.TmpdBaseName+'] ADD COLUMN ';
         if iterate^.ColumnType = Col_Int then  Commandstr:=Commandstr + '['+iterate^.Columnname+']'+' integer'
         else if iterate^.ColumnType = Col_Float then Commandstr:=Commandstr + '['+iterate^.Columnname+']'+ ' double'
         else if iterate^.ColumnType = Col_SmallInt then Commandstr:=Commandstr + '['+iterate^.Columnname+']'+ ' numeric'
         else Commandstr:=Commandstr + '['+iterate^.Columnname+']'+ ' char('+inttostr(iterate^.Columnlength)+')';
         // now execute the alter table command command
         Commandstr:=Commandstr + ';';
         ADOCmddBaseImp.CommandText:=Commandstr;
         try
            ADOCmddBaseImp.Execute;
         except
            haderror:=TRUE;
            if RetryCnt = 0 then // creation of original columnname failed
            begin
               // remove special characters from name
               MappedColName:=OriginalColName;
               MakeCorrectdBaseColumnname(MappedColName);
               iterate^.Columnname:=MappedColName;
               RetryCnt:=RetryCnt+1;
            end
            else if RetryCnt < 10 then
            begin
               MappedColName:='Field'+inttostr(MapPostFix);
               iterate^.Columnname:=MappedColName;
               MapPostFix:=MapPostFix+1;
               RetryCnt:=RetryCnt+1;
            end
            else
               iterate^.DoIgnoreColumn:=TRUE;
         end;
      end;
      if iterate^.DoIgnoreColumn then // if Column could not be created -> Show warning
      begin
         iterate^.Columnname:=OriginalColName;
         // Table for layer: <layername> Could not add column: <column-name>
         if StrFileObj <> nil then FEvents.OnWarning(StrfileObj.Assign(28)+' '+myTable^.OriginalLayername+' '+StrFileObj.Assign(29)+' '+iterate^.Columnname);
      end;
      if (haderror = FALSE) or (iterate^.DoIgnoreColumn) then
      begin
         if (not iterate^.DoIgnoreColumn) then iterate^.IsCreated:=TRUE; // set flag that column has been created
         iterate:=iterate^.next;
         RetryCnt:=0; // reset the RetryCnt
      end;
   end;
   ADOTable.Connection:=AdodBaseImp;
   ADOTable.Tablename:='['+myTable^.TmpdBaseName+']';
   // now open the table and iterate through the columns to get the real column names in the table
   // due to dBase does also internal mapping of columns names
   ADOTable.Open; colidx:=1; { start with 1 to ignore PROGIS-ID field} iterate:=myTable^.Columns; i:=0;
   while (iterate <> nil) do
   begin
      if (iterate^.Columnname <> myTable^.ProgisID) then
      begin
         if iterate^.DoIgnoreColumn = FALSE then
         begin
            if colidx < AdoTable.FieldDefs.Count then
            begin
               OriginalColname:=MappingList[i];
               MappedColname:=AdoTable.FieldDefs.Items[colidx].Name;
               if (OriginalColname <> MappedColname) then
               begin
                  InsertMappingEntry(myTable^.Colmappings,OriginalColName, MappedColName);
                  myTable^.HasMappedCols:=TRUE;
                  iterate^.ColumnName:=MappedColname;
                  // Show warning that column had to be mapped
                  // <ori-colname> will be renamed to: <mapped-colname>
                  if StrFileObj <> nil then FEvents.OnWarning(StrFileObj.Assign(31)+' '+OriginalColName+' '+StrFileObj.Assign(32)+' '+MappedColname);
               end;
               colidx:=colidx+1;
            end
            else
               iterate^.DoIgnoreColumn:=TRUE; // due to a problem the columns in table does not fit
         end;
         i:=i+1;
      end;
      iterate:=iterate^.next;
   end;
   ADOTable.Close;
end;

// ------------------------------------------------------------------------
// NAME: CopyAFile
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to copy a file
//
// PARAMETERS : Source -> Filename of the source file
//              Dest   -> Filename of the destination file
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.CopyAFile(Source:string; Dest:string);
var SourceFile:File;
    DestFile  :File;
    Actualbytes:array [1..4096] of char;
    numread      :integer;
    numwritten   :integer;
    OldFilemode  :integer;
begin
   OldFileMode:=Filemode;
   if not DirectoryExists(ExtractFilePath(Dest)) then // create target directory if necessary
      CreateDir(ExtractFilePath(Dest));
   Filemode:=0; Assignfile(SourceFile,Source); Reset(SourceFile,1);
   Filemode:=1; Assignfile(DestFile,Dest); Rewrite(DestFile,1);
   repeat
      BlockRead(SourceFile, ActualBytes, SizeOf(ActualBytes), NumRead);
      BlockWrite(DestFile, ActualBytes, NumRead, NumWritten);
   until (NumRead = 0) or (NumWritten <> NumRead);
   Closefile(SourceFile);
   Closefile(DestFile);
   FileMode:=OldFilemode;
end;

// ------------------------------------------------------------------------
// NAME: SetupTableObjects
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to setup the objects that are used for the
//              database connection.
//
// PARAMETERS : NONE
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.SetupTableObjects;
begin
   if StrFileObj = nil then
   begin
      StrFileObj:=ClassStrFile.Create;
      StrfileObj.CreateList(FWorkingDir, FLngCode, 'IMPEXPDBC');
   end;
   // setup the components that are neccessary for dBase databases
   if (FImpDBMode = DBF_Single) or (FImpDBMode = DBF_Directory) then
   begin
      if ADOdBaseImp = nil then
      begin
         ADOdBaseImp:=TADOConnection.Create(nil);
         ADOdBaseImp.Provider:='Microsoft.Jet.OLEDB.4.0';
         ADOdBaseImp.LoginPrompt:=FALSE;
      end;
      if ADOCmddBaseImp = nil then
      begin
         ADOCmddBaseImp:=TADOCommand.Create(nil);
         ADOCmddBaseImp.Connection:=ADOdBaseImp;
      end;
   end;

   if (FImpDBMode = ACCESS_Single) or (FImpDBMode = ACCESS_Tables) or (FImpDBMode = Idb_Database) then
   begin
      if AdoAccessImp = nil then
      begin
         AdoAccessImp:=TAdoConnection.Create(nil);
         AdoAccessImp.Provider:='Microsoft.Jet.OLEDB.4.0';
         AdoAccessImp.LoginPrompt:=FALSE;
      end;
      if ADOCmdAccessImp=nil then
      begin
         ADOCmdAccessImp:=TADOCommand.Create(nil);
         ADOCmdAccessImp.Connection:=AdoAccessImp;
      end;
   end;

   if (FExpDBMode = DBF_Single) or (FExpDBMode = DBF_Directory) or
      (FExpDBMode = DBF_Single_Raw) or (FExpDBMode = DBF_Directory_Raw) then
   begin
      if ADOdBaseExp = nil then
      begin
         ADOdBaseExp:=TADOConnection.Create(nil);
         ADOdBaseExp.Provider:='Microsoft.Jet.OLEDB.4.0';
         ADOdBaseExp.LoginPrompt:=FALSE;
      end;
      if ADOCmddBaseExp=nil then
      begin
         ADOCmddBaseExp:=TADOCommand.Create(nil);
         ADOCmddBaseExp.Connection:=ADOdBaseExp;
      end;
   end;

   if (FExpDBMode = ACCESS_Single) or (FExpDBMode = ACCESS_Tables) or (FExpDBMode = Idb_Database) then
   begin
      if ADOAccessExp = nil then
      begin
         ADOAccessExp:=TADOConnection.Create(nil);
         ADOAccessExp.Provider:='Microsoft.Jet.OLEDB.4.0';
         ADOAccessExp.LoginPrompt:=FALSE;
      end;
      if ADOCmdAccessExp = nil then
      begin
         ADOCmdAccessExp:=TADOCommand.Create(nil);
         ADOCmdAccessExp.Connection:=ADOAccessExp;
      end;
   end;
end;

// ------------------------------------------------------------------------
// NAME: GetExportData
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to get a info of a column of an export table
//
// PARAMETERS : Layername    -> Name of the layer
//              Column       -> Name of the column out of the export table
//              Info         -> Information of the column
//
// RETURNVALUE: TRUE  -> Database info received
//              FALSE -> No database info received
// ------------------------------------------------------------------------
function TAXImpExpDbc.GetExportData(const Layername: WideString;
  out Columnname, Info: WideString): WordBool;
var retVal:boolean;
    myTable:pTable_rec;
    aLayername:string;
begin
   retVal:=false;
   if (FExpDBMode = DB_None) then
   begin
      result:=retVal;
      exit;
   end;

   aLayername:=GetMappedName(ExpLayerMappings,Layername);

   Columnname:=''; Info:='';
   if FExpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      // the PROGISID column will not be exported
      if myTable^.Current_Column^.Columnname = myTable^.ProgisID then
         myTable^.Current_Column:=myTable^.Current_Column^.next;

      if myTable^.Current_Column <> nil then
      begin
         Columnname:=myTable^.Current_Column^.Columnname;
         Info:=myTable^.Current_Info^.Info;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
         retVal:=true;
      end;
   end
   else if FExpDBMode = DBF_Single_Raw then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      if myTable^.Current_Column <> nil then
      begin
         Columnname:=myTable^.Current_Column^.Columnname;
         Info:=myTable^.Current_Info^.Info;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
         retVal:=true;
      end;
   end
   else if FExpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      // the PROGISID column will not be exported
      if myTable^.Current_Column^.Columnname = myTable^.ProgisID then
         myTable^.Current_Column:=myTable^.Current_Column^.next;

      if myTable^.Current_Column <> nil then
      begin
         Columnname:=myTable^.Current_Column^.Columnname;
         Info:=myTable^.Current_Info^.Info;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
         retVal:=true;
      end;
   end
   else if FExpDBMode = DBF_Directory_Raw then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column <> nil then
      begin
         Columnname:=myTable^.Current_Column^.Columnname;
         Info:=myTable^.Current_Info^.Info;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
         retVal:=true;
      end;
   end
   else if FExpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      // the PROGISID column will not be exported
      if myTable^.Current_Column^.Columnname = myTable^.ProgisID then
         myTable^.Current_Column:=myTable^.Current_Column^.next;

      if myTable^.Current_Column <> nil then
      begin
         Columnname:=myTable^.Current_Column^.Columnname;
         Info:=myTable^.Current_Info^.Info;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
         retVal:=true;
      end;
   end
   else if FExpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      // the PROGISID column will not be exported
      if myTable^.Current_Column^.Columnname = myTable^.ProgisID then
         myTable^.Current_Column:=myTable^.Current_Column^.next;

      if myTable^.Current_Column <> nil then
      begin
         Columnname:=myTable^.Current_Column^.Columnname;
         Info:=myTable^.Current_Info^.Info;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
         retVal:=true;
      end;
   end
   else if FExpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      // the PROGISID column will not be exported
      if myTable^.Current_Column^.Columnname = myTable^.ProgisID then
         myTable^.Current_Column:=myTable^.Current_Column^.next;
      // the _WG_STATE column will not be exported
      if myTable^.Current_Column^.Columnname = '_WG_STATE' then
      begin
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
      end;
      // the _WG_INTERNAL_ID column will not be exported
      if myTable^.Current_Column^.Columnname = '_WG_INTERNAL_ID' then
      begin
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
      end;

      if myTable^.Current_Column <> nil then
      begin
         Columnname:=myTable^.Current_Column^.Columnname;
         Info:=myTable^.Current_Info^.Info;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
         retVal:=true;
      end;
   end;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: DoExportData
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to export data
//
// PARAMETERS : Layername -> Name of the layer
//              ID        -> Id of the object that should be exported
//
// RETURNVALUE: TRUE  -> Id has database info
//              FALSE -> Id has no database info
// ------------------------------------------------------------------------
function TAXImpExpDbc.DoExportData(const Layername: WideString; Id: Integer): WordBool;
var retVal:boolean;
    myTable:pTable_rec;
    Filterstring:string;
    aLayername:string;
begin
   retVal:=false;
   if (FExpDBMode = DB_None) then
   begin
      result:=retVal;
      exit;
   end;
   aLayername:=GetMappedName(ExpLayerMappings,Layername);
   if FExpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then begin result:=FALSE; exit; end;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         retVal:=ReadFilterData(myTable,ID);
         myTable^.Current_Column:=myTable^.Columns;
         myTable^.Current_Info:=myTable^.Infos;
      end;
   end
   else if FExpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then begin result:=FALSE; exit; end;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         retVal:=ReadFilterData(myTable,ID);
         myTable^.Current_Column:=myTable^.Columns;
         myTable^.Current_Info:=myTable^.Infos;
      end;
   end
   else if FExpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then begin result:=false; exit; end;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         retVal:=ReadFilterData(myTable,ID);
         myTable^.Current_Column:=myTable^.Columns;
         myTable^.Current_Info:=myTable^.Infos;
      end;
   end
   else if FExpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then begin result:=false; exit; end;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         retVal:=ReadFilterData(myTable,ID);
         myTable^.Current_Column:=myTable^.Columns;
         myTable^.Current_Info:=myTable^.Infos;
      end;
   end
   else if FExpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then begin result:=false; exit; end;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         retVal:=ReadFilterData(myTable,ID);
         myTable^.Current_Column:=myTable^.Columns;
         myTable^.Current_Info:=myTable^.Infos;
      end;
   end;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: OpenExpTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to open export table
//
// PARAMETERS : Layername -> Name of the layer
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.OpenExpTable(const Layername: WideString);
var myTable:pTable_rec;
    aLayername:string;
begin
   if (FExpDBMode = DB_None) then exit;

   aLayername:=GetMappedName(ExpLayerMappings,Layername);
   if FExpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then exit;
      if (not myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         ADOTable.Open;
         myTable^.opened:=true;
      end;
   end
   else if FExpDBMode = DBF_Single_Raw then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then exit;
      if (not myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         ADOTable.Open;
         ADOTable.First; // go to first entry
         myTable^.opened:=true;
      end;
   end
   else if FExpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then exit;
      if (not myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         ADOTable.Open;
         myTable^.opened:=true;
      end;
   end
   else if FExpDBMode = DBF_Directory_Raw then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then exit;
      if (not myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         ADOTable.Open;
         myTable^.opened:=true;
      end;
   end
   else if FExpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then exit;
      if (not myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         if ADOTable.active = FALSE then
            ADOTable.active:=true;
         myTable^.opened:=true;
      end;
   end
   else if FExpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then exit;
      if (not myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         if ADOTable.active = FALSE then
            ADOTable.active:=true;
         myTable^.opened:=true;
      end;
   end
   else if FExpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then exit;
      if (not myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         if ADOTable.active = FALSE then
            ADOTable.active:=true;
         myTable^.opened:=true;
      end;
   end;
end;

// ------------------------------------------------------------------------
// NAME: CloseExpTables
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to close export tables
//
// PARAMETERS : NONE
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.CloseExpTables;
begin
   //close all tables and destroy Tablelist
   CloseTables(ExpTables,DOEXPORT);
   ExpMappingNr:=0;
end;

// ------------------------------------------------------------------------
// NAME: CloseImpTables
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to close import tables
//
// PARAMETERS : NONE
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.CloseImpTables;
begin
    //close all tables and destroy Tablelist
   CloseTables(ImpTables,DOIMPORT);
   ImpMappingNr:=0;
end;

// ------------------------------------------------------------------------
// NAME: AssignInfoToField
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to assign info to column
//
// PARAMETERS : myTable     -> reference to table
//              aColumnname -> Name of the column
//              aInfo        -> Info that should be inserted
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.AssignInfoToField(var myTable:pTable_rec; const aColumnname:string; const aInfo:string);
var coliterate:pColumn_rec;
    found:boolean;
begin
   try
      ADOTable.FieldByName(aColumnname).AsString:=aInfo;
   except
      // The column will be ignored due to it could not be found in table-definition
      coliterate:=myTable^.Columns;
      // now find the column
      found:=TRUE;
      while (coliterate <> nil) and (not found) do
      begin
         if coliterate^.Columnname = aColumnname then
         begin
            coliterate^.DoIgnoreColumn:=TRUE;
            found:=TRUE;
         end;
         coliterate:=coliterate^.next;
      end;
   end;
end;

// ------------------------------------------------------------------------
// NAME: AddImpInfo
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to add info to a column of an import table
//
// PARAMETERS : Layername  -> Name of the layer in GIS
//              Columnname -> Name of the column
//              Info       -> Info that should be inserted
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.AddImpInfo(const Layername, Columnname,
  Info: WideString);
var myTable:pTable_rec;
    mappedColName:string;
    aLayername:string;
    aInfo:string;
    dummy:string;
begin
   if (FImpDBMode = DB_None) then exit;
   // the information will be trimed on the right
   // to avoid huge information strings
   aInfo:=TrimRight(Info);
   aLayername:=GetMappedName(ImpLayerMappings,Layername);
   if FImpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then exit;
      if ADOTable.Connection = nil then
         ADOTable.Connection:=ADOdBaseImp;
      mappedColname:=GetMappedName(myTable^.Colmappings,Columnname);
      if (myTable^.opened) and (not myTable^.Ignoredata) and (mappedColname <> '') then
      begin
         // first the column and the type have to be checked
         if CheckColumnAndType(myTable,mappedColname,aInfo) then
         begin
            // check if there is already info in this Field and if this may be modified
            if not FAllowModifyImpData then
            begin
               dummy:=ADOTable.FieldByName(mappedColname).AsString;
               if (dummy = '') or ((dummy <> '') and (mappedColName = MyTable^.ProgisID)) then
                  ADOTable.FieldByName(mappedColname).AsString:=aInfo;
            end
            else
               ADOTable.FieldByName(mappedColname).AsString:=aInfo;
         end;
      end;
   end
   else if FImpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if ADOTable.Connection = nil then
         ADOTable.Connection:=ADOdBaseImp;
      mappedColname:=GetMappedName(myTable^.Colmappings,Columnname);
      if (myTable^.opened) and (not myTable^.Ignoredata) and (mappedColname <> '') then
      begin
         if CheckColumnAndType(myTable,mappedColname,aInfo) then
         begin
            // check if there is already info in this Field and if this may be modified
            if not FAllowModifyImpData then
            begin
               dummy:=ADOTable.FieldByName(mappedColname).AsString;
               if (dummy = '') or ((dummy <> '') and (mappedColName = MyTable^.ProgisID)) then
                  ADOTable.FieldByName(mappedColname).AsString:=aInfo;
            end
            else
               ADOTable.FieldByName(mappedColname).AsString:=aInfo;
         end;
      end;
   end
   else if FImpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then exit;
      mappedColname:=GetMappedName(myTable^.Colmappings,Columnname);
      if (myTable^.opened) and (not myTable^.Ignoredata) and (mappedColname <> '') then
      begin
         // first the column and the type have to be checked
         if CheckColumnAndType(myTable,mappedColname,aInfo) then
         begin
            // check if there is already info in this Field and if this may be modified
            if not FAllowModifyImpData then
            begin
               dummy:=ADOTable.FieldByName(mappedColname).AsString;
               if (dummy = '') or ((dummy <> '') and (mappedColName = MyTable^.ProgisID)) then
                  AssignInfoToField(myTable, mappedColname, aInfo);
            end
            else
               AssignInfoToField(myTable, mappedColname, aInfo);
         end;
      end;
   end
   else if FImpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      mappedColname:=GetMappedName(myTable^.Colmappings,Columnname);
      if (myTable^.opened) and (not myTable^.Ignoredata) and (mappedColname <> '') then
      begin
         if CheckColumnAndType(myTable,mappedColname,aInfo) then
         begin
            // check if there is already info in this Field and if this may be modified
            if not FAllowModifyImpData then
            begin
               dummy:=ADOTable.FieldByName(mappedColname).AsString;
               if (dummy = '') or ((dummy <> '') and (mappedColName = MyTable^.ProgisID)) then
                  AssignInfoToField(myTable, mappedColname, aInfo);
            end
            else
               AssignInfoToField(myTable, mappedColname, aInfo);
         end;
      end;
   end
   else if FImpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      mappedColname:=GetMappedName(myTable^.Colmappings,Columnname);
      if (myTable^.opened) and (not myTable^.Ignoredata) and (mappedColname <> '') then
      begin
         if CheckColumnAndType(myTable,mappedColname,aInfo) then
         begin
            // check if there is already info in this Field and if this may be modified
            if not FAllowModifyImpData then
            begin
               dummy:=ADOTable.FieldByName(mappedColname).AsString;
               if (dummy = '') or ((dummy <> '') and (mappedColName = MyTable^.ProgisID)) then
                  AssignInfoToField(myTable, mappedColname, aInfo);
            end
            else
               AssignInfoToField(myTable, mappedColname, aInfo);
         end;
      end;
   end
   else if FImpDBMode = Text_File then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      TextImpTable.WriteData(aInfo);
   end;
end;

// ------------------------------------------------------------------------
// NAME: AddIdAndCheckForIndexConflict
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to check if an index error could occur
//
// PARAMETERS : myTable    -> reference to Table object
//              Id         -> Id of the object that should be checked
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.AddIdAndCheckForIndexConflict(var myTable:pTable_rec; Id:integer);
var FilterString:string;
    noerror:boolean;
    dummy  :integer;
begin
   // first check if the table contains an Index
   if (myTable^.hadbeencreated and FCreateIndexOnClose) then
   begin
      // if no index-definitions exist the index check not has to be done
      ADOTable.FieldByName(myTable^.ProgisID).AsString:=inttostr(ID);
      if (FImpDBMode = Idb_Database) then
         ADOTable.FieldByName('_WG_STATE').AsString:='1';
      exit;
   end;

   // now check if an index error could occur
   noerror:=TRUE;
   ADOTable.FieldByName(myTable^.ProgisID).AsString:=inttostr(ID);
   if not PostDataToTable(myTable) then // try to post the data
   begin
      // remove the old Id from database that it can be inserted new
      ADOTable.FieldByName(myTable^.ProgisID).AsString:=inttostr(IndexFixNr);
      // add to info field
      if (myTable^.Columns^.next <> nil) then
         ADOTable.FieldByName(myTable^.Columns^.next^.Columnname).AsString:='1';
      ADOTable.Post;
      ADOTable.Delete;
      Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(IndexFixNr) + chr(39);
      ADOTable.Filter:=FilterString;
      if ADOTable.FieldByName(myTable^.ProgisID).AsString = inttostr(IndexFixNr) then
         ADOTable.Delete;

      Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
      ADOTable.Filter:=FilterString;
      noerror:=FALSE;
   end;
   AdoTable.Edit;
   // if no error occured the entry for the idb has to be inserted
   if (noerror) and (FImpDBMode = Idb_Database) then
      ADOTable.FieldByName('_WG_STATE').AsString:='1';
end;

// ------------------------------------------------------------------------
// NAME: AddImpIDInfo
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to add ID info to import table
//
// PARAMETERS : Layername  -> Name of the layer in GIS
//              Id         -> Id of the object that should be inserted
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.AddImpIdInfo(const Layername: WideString;
  Id: Integer);
var myTable:pTable_rec;
    aLayername:string;
begin
   if (FImpDBMode = DB_None) then exit;

   aLayername:=GetMappedName(ImpLayerMappings,Layername);
   if FImpDBMode = DBF_Single then
   begin
      // call the DoEditImpInfo method to avoid multiple index
      DoEditImpInfo(Layername, Id);
      exit;

      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then exit;
      if ADOTable.Connection = nil then
         ADOTable.Connection:=ADOdBaseImp;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         // add id info to table
         if myTable^.DoPost then
            PostDataToTable(myTable);

         myTable^.DoPost:=true;
         ADOTable.Append;
         AddImpInfo(Layername,MyTable^.ProgisID,inttostr(ID));
      end;
   end
   else if FImpDBMode = DBF_Directory then
   begin
      // call the DoEditImpInfo method to avoid multiple index
      DoEditImpInfo(Layername, Id);
      exit;

      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if ADOTable.Connection = nil then
         ADOTable.Connection:=ADOdBaseImp;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         // add id info to table
         if myTable^.DoPost then
            PostDataToTable(myTable);
         myTable^.DoPost:=true;
         ADOTable.Append;
         AddImpInfo(Layername,MyTable^.ProgisID,inttostr(ID));
      end;
   end
   else if FImpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         // add id info to table
         if myTable^.DoPost then
            PostDataToTable(myTable);
         myTable^.DoPost:=true;
         ADOTable.Append;
         AddIdAndCheckForIndexConflict(myTable, Id);
      end;
   end
   else if FImpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         // add id info to table
         if myTable^.DoPost then
            PostDataToTable(myTable);
         myTable^.DoPost:=true;
         ADOTable.Append;
         AddIdAndCheckForIndexConflict(myTable, Id);
      end;
   end
   else if FImpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if not myTable^.hadbeencreated then
      begin
         // edit import info to avoid PROGIS-ID conflicts
         // due to PROGIS-ID is not primary key
         DoEditImpInfo(Layername, Id);
         exit;
      end;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         // add id info to table
         if myTable^.DoPost then
            PostDataToTable(myTable);
         myTable^.DoPost:=true;
         ADOTable.Append;
         AddIdAndCheckForIndexConflict(myTable, Id);
      end;
   end
   else if FImpDBMode = Text_File then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      // add id info to table
      if myTable^.DoPost then
         TextImpTable.CloseLine;
      myTable^.DoPost:=true;
      TextImpTable.WriteData(inttostr(Id));
   end;
end;

// ------------------------------------------------------------------------
// NAME: OpenImpTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to post data to table
//
// PARAMETERS : myTable -> Reference to table object
//
// RETURNVALUE: TRUE if post was ok, else FALSE
// ------------------------------------------------------------------------
function TAXImpExpDbc.PostDataToTable(var myTable:pTable_rec):boolean;
var wasok:boolean;
    iterate:pColumn_rec;
    oldvalue:string;
begin
   wasok:=TRUE;
   try
      ADOTable.Post;
   except
      wasok:=FALSE;
   end;
   if not wasok then
   begin
      // it could be that a field was required that has no data
      iterate:=myTable^.Columns;
      while (iterate <> nil) and (not wasok) do
      begin
         if (not iterate^.IsCreated) and
            (iterate^.Columnname <> '_WG_INTERNAL_ID') and
            (iterate^.Columnname <> '_WG_STATE') then
         begin
            oldvalue:=ADOTable.FieldByName(iterate^.Columnname).AsString;
            if oldvalue = '' then
            begin
               // get the default value from the column
               oldvalue:=ADOTable.FieldByName(iterate^.Columnname).DefaultExpression;
               if oldvalue = '' then
               begin
                  // if there is no default expression fill with dummy value
                       if iterate^.ColumnType = Col_Int then oldvalue:='0'
                  else if iterate^.ColumnType = Col_SmallInt then oldvalue:='0'
                  else if iterate^.ColumnType = Col_Float then oldvalue:='0'
                  else if iterate^.ColumnType = Col_String then oldvalue:=' ';
               end;
               AssignInfoToField(myTable, iterate^.Columnname, oldvalue);

               // now try if the record can be posted
               wasok:=TRUE;
               try
                  ADOTable.Post;
               except
                  wasok:=FALSE; // we still have not found the column that requires data
               end;
            end;
         end;
         iterate:=iterate^.next;
      end;
   end;
   if wasok then
   begin
      // increase the number of processed records
      myTable^.ProcessedRecCnt:=myTable^.ProcessedRecCnt+1;
      if (myTable^.ProcessedRecCnt >= FRefreshThreshold) then
      begin
//         ADOTable.Refresh;   // ignore till refresh works correct
         myTable^.ProcessedRecCnt:=0; // reset processed record count
      end;
   end;
   result:=wasok;
end;

// ------------------------------------------------------------------------
// NAME: OpenImpTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to open import Table
//
// PARAMETERS : Layername  -> Name of the layer in GIS
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.OpenImpTable(const Layername: WideString);
var myTable   :pTable_rec;
    aLayername:string;
    i:integer;
begin
   if (FImpDBMode = DB_None) then exit;
   aLayername:=GetMappedName(ImpLayerMappings,Layername);
   if FImpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then exit;
      if (not myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         ADOTable.Open;
         myTable^.opened:=true;
      end;
   end
   else if FImpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if (not myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         ADOTable.Open;
         myTable^.opened:=true;
      end;
   end
   else if FImpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then exit;
      if (not myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         if ADOTable.active = FALSE then
            ADOTable.active:=true;
         myTable^.opened:=true;
      end;
   end
   else if FImpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if (not myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         if ADOTable.active = FALSE then
            ADOTable.active:=true;
         myTable^.opened:=true;
      end;
   end
   else if FImpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if (not myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         if ADOTable.active = FALSE then
            ADOTable.active:=true;
         myTable^.opened:=true;
      end;
   end
   else if FImpDbMode = Text_File then
   begin
      // nothing to do here
   end;
end;

// ------------------------------------------------------------------------
// NAME: CheckAndCreateImpTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to check and create an import table
//
// PARAMETERS : Layername  -> Name of the layer in GIS
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.CheckAndCreateImpTable(const Layername: WideString);
var exists:boolean;
    myTable:pTable_rec;
    Tablename:string;
    OriginalLayername:string;
    i:integer;
    dummy1,dummy2:string;
    aLayername:string;
    LayerIndex:integer;
    intval, count:integer;
    ProjectName:widestring;
begin
   if (FImpDBMode = DB_None) then exit;

   SetupTableObjects;

   if FImpDBMode = DBF_Single then
   begin
      // check if Data Connection is opened
      if  not ADOdBaseImp.Connected then
      begin
         // open .mdb and get Tablenames
         ADOdBaseImp.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False';
         ADOdBaseImp.Properties['Extended Properties'].Value := 'dBASE 5.0';
         ADOdBaseImp.Properties['Data Source'].Value :=ExtractFileDir(FTmpDBaseDir); // dBase tables only will be processed in temporary directory
         ADOdBaseImp.Connected:=True;
      end;
      exists:=FileExists(FImpDatabase);
      Tablename:=FImpDatabase;
      myTable:=InsertTable(ImpTables,'SINGLE',Layername,Tablename,exists,DOIMPORT);
   end
   else if FImpDBMode = DBF_Directory then
   begin
      TableName:=Layername;
      Tablename:=FImpDatabase+'\'+TableName+'.dbf';
      exists:=FileExists(Tablename);
      // check if Data Connection is opened
      if  not ADOdBaseImp.Connected then
      begin
         // open ADO connection
         ADOdBaseImp.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False';
         ADOdBaseImp.Properties['Extended Properties'].Value := 'dBASE 5.0';
         ADOdBaseImp.Properties['Data Source'].Value :=ExtractFileDir(FTmpDBaseDir); // dBase tables only will be processed in temporary directory
         ADOdBaseImp.Connected:=True;
      end;

      if not exists then
      begin
         // check again with corrected tablename
         TableName:=Layername;
         MakeCorrectFilename(TableName);
         Tablename:=FImpDatabase+'\'+TableName+'.dbf';
         exists:=FileExists(Tablename);
      end;
      OriginalLayername:=Layername;
      aLayername:=GetMappedName(ImpLayerMappings,OriginalLayername);
      if (aLayername = OriginalLayername) then // layername still has no mapping entry
      begin
         // create mapping entry
         aLayername:='LAYER'+inttostr(ImpMappingNr);
         ImpMappingNr:=ImpMappingNr+1;
         InsertMappingEntry(ImpLayerMappings,OriginalLayername, aLayername);
      end;
      myTable:=InsertTable(ImpTables,aLayername,OriginalLayername,Tablename,exists,DOIMPORT);
   end
   else if FImpDBMode = ACCESS_Single then
   begin
      // check if Access-mdb is opened, if not it will be opened
      if  not ADOAccessImp.Connected then
      begin
         // check if .mdb file does exist
         if (not Fileexists(FImpdatabase)) then
         begin
            // copy template Access-mdb-Template (impexpdbc.tpl) to ImpDatabase
            if not FileExists(FWorkingDir+'impexpdbc.tpl') then
               Showmessage('Error: Impexpdbc.tpl does not exist!');
            CopyAFile(FWorkingDir+'impexpdbc.tpl', FImpDataBase);
         end;
         // open .mdb and get Tablenames
         ADOAccessImp.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + FImpdatabase;
         ADOAccessImp.Connected:=True;
         ADOAccessImpTables.Clear;
         ADOAccessImp.GetTableNames(ADOAccessImpTables,False);
      end;
      // check if Importtable exists
      exists:=false;
      for i:=0 to ADOAccessImpTables.Count - 1 do
      begin
         dummy1:=uppercase(ADOAccessImpTables[i]);
         dummy2:=uppercase(FImpTable);
         if dummy1 = dummy2 then
         begin
            FImpTable:=ADOAccessImpTables[i];
            exists:=true;
            break;
         end;
      end;
      myTable:=InsertTable(ImpTables,'SINGLE',Layername,FImpTable,exists,DOIMPORT);
   end
   else if FImpDBMode = ACCESS_Tables then
   begin
      // check if Access-mdb is opened, if not it will be opened
      if  not ADOAccessImp.Connected then
      begin
         // check if .mdb file does exist
         if (not Fileexists(FImpdatabase)) then
         begin
            // copy template Access-mdb-Template (impexpdbc.tpl) to ImpDatabase
            if not FileExists(FWorkingDir+'impexpdbc.tpl') then
               Showmessage('Error: Impexpdbc.tpl does not exist!');
            CopyAFile(FWorkingDir+'impexpdbc.tpl', FImpDataBase);
         end;
         // open .mdb and get Tablenames
         ADOAccessImp.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + FImpdatabase;
         ADOAccessImp.Connected:=True;
         ADOAccessImp.GetTableNames(ADOAccessImpTables,False);
      end;
      Tablename:=Layername;
      OriginalLayername:=Layername;
      aLayername:=GetMappedName(ImpLayerMappings,OriginalLayername);
      if (aLayername = OriginalLayername) then // layername still has no mapping entry
      begin
         // create mapping entry
         aLayername:='LAYER'+inttostr(ImpMappingNr);
         ImpMappingNr:=ImpMappingNr+1;
         InsertMappingEntry(ImpLayerMappings,OriginalLayername, aLayername);
      end;

      // check if Importtable exists
      exists:=false;
      for i:=0 to ADOAccessImpTables.Count - 1 do
      begin
         dummy1:=uppercase(ADOAccessImpTables[i]);
         dummy2:=uppercase(Tablename);
         if dummy1 = dummy2 then
         begin
            exists:=true;
            Tablename:=ADOAccessImpTables[i];
            break;
         end;
      end;
      myTable:=InsertTable(ImpTables,aLayername,OriginalLayername,Tablename,exists,DOIMPORT);
   end
   else if FImpDBMode = Idb_Database then
   begin
      // check if Access-mdb is opened, if not it will be opened
      if  not ADOAccessImp.Connected then
      begin
         // check if .mdb file does exist
         if (not Fileexists(FImpdatabase)) then
         begin
            // copy template Access-mdb-Template (impexpdbc.tpl) to ImpDatabase
            if not FileExists(FWorkingDir+'impexpidb.tpl') then
               Showmessage('Error: Impexpidb.tpl does not exist!');
            CopyAFile(FWorkingDir+'impexpidb.tpl', FImpDataBase);
         end;
         // open .mdb and get Tablenames
         ADOAccessImp.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + FImpdatabase;
         ADOAccessImp.Connected:=True;
         ADOAccessImp.GetTableNames(ADOAccessImpTables,False);
      end;
      OriginalLayername:=Layername;
      aLayername:=GetMappedName(ImpLayerMappings,OriginalLayername);
      if (aLayername = OriginalLayername) then // layername still has no mapping entry
      begin
         // create mapping entry
         aLayername:='LAYER'+inttostr(ImpMappingNr);
         ImpMappingNr:=ImpMappingNr+1;
         InsertMappingEntry(ImpLayerMappings,OriginalLayername, aLayername);
      end;

      // check if LTN table exists exists
      exists:=false;
      for i:=0 to ADOAccessImpTables.Count - 1 do
      begin
         if ADOAccessImpTables[i] = 'USYS_WGIDB_LTN' then
         begin
            exists:=TRUE;
            break;
         end;
      end;

      if exists then
      begin
         // first check if the table exists in the LTN_TABLE of the idb-database
         FEvents.OnRequestImpLayerIndex(Layername,LayerIndex);
         ADOTable:=TADOTable.Create(nil);
         ADOTable.Name:='IDBLAYER';
         AdoTable.Tablename:='[USYS_WGIDB_LTN]';
         ADOTable.Connection:=ADOAccessImp;
         ADOTable.active:=true;
         exists:=FALSE;
         ADOTable.First;
         while (not ADOTable.Eof) and (not exists) do
         begin
            dummy1:=ADOTable.FieldByName('INDEX').AsString;
            val(dummy1, intval, count); if count <> 0 then intval:=0;
            if (intval = LayerIndex) then
            begin
               Tablename:=ADOTable.FieldByName('TABLENAME').AsString; // We found the tablename in the LTN-table!
               exists:=TRUE;
            end;
            ADOTable.Next;
         end;
         ADOTable.Close;
         ADOTable.Free; ADOTable:=nil;

         if exists then
         begin
            // the reference to the table has been found in the LTN table
            // but it also has to be checked if the table exists in the database
            exists:=FALSE;
            for i:=0 to ADOAccessImpTables.Count - 1 do
            begin
               dummy1:=uppercase(ADOAccessImpTables[i]);
               dummy2:=uppercase(Tablename);
               if dummy1 = dummy2 then
               begin
                  exists:=true;
                  Tablename:=ADOAccessImpTables[i];
                  break;
               end;
            end;
         end;
      end;

      // if the table still has not been found
      if exists = FALSE then
      begin
         Tablename:='ATT_'+Layername;
         for i:=0 to ADOAccessImpTables.Count - 1 do
         begin
            dummy1:=uppercase(ADOAccessImpTables[i]);
            dummy2:=uppercase(Tablename);
            if dummy1 = dummy2 then
            begin
               exists:=true;
               Tablename:=ADOAccessImpTables[i];
               break;
            end;
         end;
      end;
      myTable:=InsertTable(ImpTables,aLayername,OriginalLayername,Tablename,exists,DOIMPORT);
   end
   else if FImpDbMode = Text_File then
   begin
      FEvents.OnRequestImpProjectName(ProjectName);
      dummy1:=ExtractFilename(ProjectName); dummy1:=copy(dummy1,1,Pos('.',dummy1)-1);
      // get name of the Text-File
      // Syntax: FImpDatabase + <name of project>_<Layername>.txt
      TableName:=FImpDatabase+'\'+dummy1+'_'+Layername+'.txt';
      OriginalLayername:=Layername;
      aLayername:=GetMappedName(ImpLayerMappings,OriginalLayername);
      if (aLayername = OriginalLayername) then // layername still has no mapping entry
      begin
         // create mapping entry
         aLayername:='LAYER'+inttostr(ImpMappingNr);
         ImpMappingNr:=ImpMappingNr+1;
         InsertMappingEntry(ImpLayerMappings,OriginalLayername, aLayername);
      end;
      myTable:=InsertTable(ImpTables,aLayername,OriginalLayername,Tablename,FALSE { exist is always false here} ,DOIMPORT);
   end;
end;

// ------------------------------------------------------------------------
// NAME: CheckAndCreateExpTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to check and create an export table
//
// PARAMETERS : Layername  -> Name of the layer in GIS
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.CheckAndCreateExpTable(const Layername: WideString);
var exists:boolean;
    myTable:pTable_rec;
    Tablename:string;
    i:integer;
    OriginalLayername:string;
    dummy1,dummy2:string;
    aLayername:string;
    LayerIndex:integer;
    intval, count:integer;
begin
   if (FExpDBMode = DB_None) then exit;

   SetupTableObjects;

   if FExpDBMode = DBF_Single then
   begin
      // check if Data Connection is opened
      if  not ADOdBaseExp.Connected then
      begin
         // open ADO Connection
         ADOdBaseExp.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False';
         ADOdBaseExp.Properties['Extended Properties'].Value := 'dBASE 5.0';
         ADOdBaseExp.Properties['Data Source'].Value :=ExtractFileDir(FTmpDBaseDir); // dBase database will be kept in temporary directory to avoid problems with filename
         ADOdBaseExp.Connected:=True;
      end;
      exists:=FileExists(FExpDatabase);
      if (exists = FALSE) and (FIgnoreNonExistExpTables=TRUE) then
         exit;
      Tablename:=FExpDatabase;
      myTable:=InsertTable(ExpTables,'SINGLE',Layername,Tablename,exists,DOEXPORT);
   end
   else if FExpDbMode = DBF_Single_Raw then // use raw dBase Table for Export
   begin
      // check if Data Connection is opened
      if  not ADOdBaseExp.Connected then
      begin
         // open ADO Connection
         ADOdBaseExp.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False';
         ADOdBaseExp.Properties['Extended Properties'].Value := 'dBASE 5.0';
         ADOdBaseExp.Properties['Data Source'].Value :=ExtractFileDir(FTmpDBaseDir); // dBase database will be kept in temporary directory to avoid problems with filename
         ADOdBaseExp.Connected:=True;
      end;
      exists:=FileExists(FExpDatabase);
      // raw dBase single table that does not exist may be ignored if it
      // does not exist
      FIgnoreNonExistExpTables:=TRUE;

      if (exists = FALSE) and (FIgnoreNonExistExpTables=TRUE) then
         exit;
      Tablename:=FExpDatabase;
      myTable:=InsertTable(ExpTables,'SINGLE',Layername,Tablename,exists,DOEXPORT);
   end
   else if FExpDBMode = DBF_Directory then
   begin
      TableName:=Layername;
      Tablename:=FExpDatabase+'\'+TableName+'.dbf';
      exists:=FileExists(Tablename);

      // check if Data Connection is opened
      if  not ADOdBaseExp.Connected then
      begin
         // open ADO connection
         ADOdBaseExp.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False';
         ADOdBaseExp.Properties['Extended Properties'].Value := 'dBASE 5.0';
         ADOdBaseExp.Properties['Data Source'].Value :=ExtractFileDir(FTmpDBaseDir); // dBase database will be kept in temporary directory to avoid problems with filename
         ADOdBaseExp.Connected:=True;
      end;

      if not exists then
      begin
         // check again with corrected tablename
         TableName:=Layername;
         MakeCorrectFilename(TableName);
         Tablename:=FExpDatabase+'\'+TableName+'.dbf';
         exists:=FileExists(Tablename);
      end;

      // if export databases that do not exist should be ignored then exit
      if (exists = FALSE) and (FIgnoreNonExistExpTables=TRUE) then
         exit;

      OriginalLayername:=Layername;
      aLayername:=GetMappedName(ExpLayerMappings,OriginalLayername);
      if (aLayername = OriginalLayername) then // layername still has no mapping entry
      begin
         // create mapping entry
         aLayername:='LAYER'+inttostr(ExpMappingNr);
         ExpMappingNr:=ExpMappingNr+1;
         InsertMappingEntry(ExpLayerMappings,OriginalLayername, aLayername);
      end;
      myTable:=InsertTable(ExpTables,aLayername,OriginalLayername,Tablename,exists,DOEXPORT);
   end
   else if FExpDBMode = DBF_Directory_Raw then
   begin
      TableName:=Layername;
      Tablename:=FExpDatabase+'\'+TableName+'.dbf';
      exists:=FileExists(Tablename);

      // check if Data Connection is opened
      if  not ADOdBaseExp.Connected then
      begin
         // open ADO connection
         ADOdBaseExp.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False';
         ADOdBaseExp.Properties['Extended Properties'].Value := 'dBASE 5.0';
         ADOdBaseExp.Properties['Data Source'].Value :=ExtractFileDir(FTmpDBaseDir); // dBase database will be kept in temporary directory to avoid problems with filename
         ADOdBaseExp.Connected:=True;
      end;

      if not exists then
      begin
         // check again with corrected tablename
         TableName:=Layername;
         MakeCorrectFilename(TableName);
         Tablename:=FExpDatabase+'\'+TableName+'.dbf';
         exists:=FileExists(Tablename);
      end;

      // raw dBase directory table that does not exist may be ignored if it
      // does not exist
      FIgnoreNonExistExpTables:=TRUE;

      // if export databases that do not exist should be ignored then exit
      if (exists = FALSE) and (FIgnoreNonExistExpTables=TRUE) then
         exit;

      OriginalLayername:=Layername;
      aLayername:=GetMappedName(ExpLayerMappings,OriginalLayername);
      if (aLayername = OriginalLayername) then // layername still has no mapping entry
      begin
         // create mapping entry
         aLayername:='LAYER'+inttostr(ExpMappingNr);
         ExpMappingNr:=ExpMappingNr+1;
         InsertMappingEntry(ExpLayerMappings,OriginalLayername, aLayername);
      end;
      myTable:=InsertTable(ExpTables,aLayername,OriginalLayername,Tablename,exists,DOEXPORT);
   end
   else if FExpDBMode = ACCESS_Single then
   begin
      // check if Access-mdb is opened, if not it will be opened
      if  not ADOAccessExp.Connected then
      begin
         // open .mdb and get Tablenames
         ADOAccessExp.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + FExpdatabase;
         ADOAccessExp.Connected:=True;
         ADOAccessExpTables.Clear;
         ADOAccessExp.GetTableNames(ADOAccessExpTables,False);
      end;
      // check if Exporttable exists
      exists:=false;
      for i:=0 to ADOAccessExpTables.Count - 1 do
      begin
         dummy1:=uppercase(ADOAccessExpTables[i]);
         dummy2:=uppercase(FExpTable);
         if dummy1 = dummy2 then
         begin
            FExpTable:=ADOAccessExpTables[i];
            exists:=true;
            break;
         end;
      end;

      // if export databases that do not exist should be ignored then exit
      if (exists = FALSE) and (FIgnoreNonExistExpTables=TRUE) then
         exit;

      myTable:=InsertTable(ExpTables,'SINGLE',Layername,FExpTable,exists,DOEXPORT);
   end
   else if FExpDBMode = ACCESS_Tables then
   begin
      // check if Access-mdb is opened, if not it will be opened
      if  not ADOAccessExp.Connected then
      begin
         // open .mdb and get Tablenames
         ADOAccessExp.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + FExpdatabase;
         ADOAccessExp.Connected:=True;
         ADOAccessExpTables.Clear;
         ADOAccessExp.GetTableNames(ADOAccessExpTables,False);
      end;

      Tablename:=Layername;
      // check if Importtable exists
      exists:=false;
      for i:=0 to ADOAccessExpTables.Count - 1 do
      begin
         dummy1:=uppercase(Tablename);
         dummy2:=uppercase(ADOAccessExpTables[i]);
         if dummy1 = dummy2 then
         begin
            Tablename:=ADOAccessExpTables[i];
            exists:=true;
            break;
         end;
      end;

      // if export databases that do not exist should be ignored then exit
      if (exists = FALSE) and (FIgnoreNonExistExpTables=TRUE) then
         exit;

      OriginalLayername:=Layername;
      aLayername:=GetMappedName(ExpLayerMappings,OriginalLayername);
      if (aLayername = OriginalLayername) then // layername still has no mapping entry
      begin
         // create mapping entry
         aLayername:='LAYER'+inttostr(ExpMappingNr);
         ExpMappingNr:=ExpMappingNr+1;
         InsertMappingEntry(ExpLayerMappings,OriginalLayername, aLayername);
      end;

      myTable:=InsertTable(ExpTables,aLayername,OriginalLayername,Tablename,exists,DOEXPORT);
   end
   else if FExpDBMode = Idb_Database then
   begin
      // check if Access-wgi is opened, if not it will be opened
      if  not ADOAccessExp.Connected then
      begin
         // open .mdb and get Tablenames
         ADOAccessExp.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + FExpdatabase;
         ADOAccessExp.Connected:=True;
         ADOAccessExpTables.Clear;
         ADOAccessExp.GetTableNames(ADOAccessExpTables,False);
      end;

      // check if LTN table exists exists
      exists:=false;
      for i:=0 to ADOAccessExpTables.Count - 1 do
      begin
         if ADOAccessExpTables[i] = 'USYS_WGIDB_LTN' then
         begin
            exists:=TRUE;
            break;
         end;
      end;

      if exists then
      begin
         // first check if the table exists in the LTN_TABLE of the idb-database
         FEvents.OnRequestExpLayerIndex(Layername,LayerIndex);
         ADOTable:=TADOTable.Create(nil);
         ADOTable.Name:='IDBLAYER';
         AdoTable.Tablename:='[USYS_WGIDB_LTN]';
         ADOTable.Connection:=ADOAccessExp;
         ADOTable.active:=true;
         exists:=FALSE;
         ADOTable.First;
         while (not ADOTable.Eof) and (not exists) do
         begin
            dummy1:=ADOTable.FieldByName('INDEX').AsString;
            val(dummy1, intval, count); if count <> 0 then intval:=0;
            if (intval = LayerIndex) then
            begin
               Tablename:=ADOTable.FieldByName('TABLENAME').AsString; // We found the tablename in the LTN-table!
               exists:=TRUE;
            end;
            ADOTable.Next;
         end;
         ADOTable.Close;
         ADOTable.Free; ADOTable:=nil;
         if exists then
         begin
            // the reference to the table has been found in the LTN table, but
            // it also has to be checked if it exists in the database
            exists:=FALSE;
            for i:=0 to ADOAccessExpTables.Count - 1 do
            begin
               dummy1:=uppercase(Tablename);
               dummy2:=uppercase(ADOAccessExpTables[i]);
               if dummy1 = dummy2 then
               begin
                  Tablename:=ADOAccessExpTables[i];
                  exists:=true;
                  break;
               end;
            end;
         end;
      end;

      if not exists then
      begin
         Tablename:='ATT_'+Layername;
         // check if Importtable exists
         for i:=0 to ADOAccessExpTables.Count - 1 do
         begin
            dummy1:=uppercase(Tablename);
            dummy2:=uppercase(ADOAccessExpTables[i]);
            if dummy1 = dummy2 then
            begin
               Tablename:=ADOAccessExpTables[i];
               exists:=true;
               break;
            end;
         end;
      end;

      // idb-database that does not have corresponding table always can be deleted
      FIgnoreNonExistExpTables:=TRUE;

      // if export databases that do not exist should be ignored then exit
      if (exists = FALSE) and (FIgnoreNonExistExpTables=TRUE) then
         exit;

      OriginalLayername:=Layername;
      aLayername:=GetMappedName(ExpLayerMappings,OriginalLayername);
      if (aLayername = OriginalLayername) then // layername still has no mapping entry
      begin
         // create mapping entry
         aLayername:='LAYER'+inttostr(ExpMappingNr);
         ExpMappingNr:=ExpMappingNr+1;
         InsertMappingEntry(ExpLayerMappings,OriginalLayername, aLayername);
      end;

      myTable:=InsertTable(ExpTables,aLayername,OriginalLayername,Tablename,exists,DOEXPORT);
   end;
end;

// ------------------------------------------------------------------------
// NAME: AddImpIDColumn
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to add id column to import table
//
// PARAMETERS : Layername  -> Name of the layer in GIS
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.AddImpIdColumn(const Layername: WideString);
var myTable:pTable_rec;
    aLayername:string;
begin
   if (FImpDBMode = DB_None) then exit;
   aLayername:=GetMappedName(ImpLayerMappings,Layername);
   if FImpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         // add column to list
         AddColumn(myTable,myTable^.ProgisId,Col_Int,0);
      end;
   end
   else if FImpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         // add column to list
         AddColumn(myTable,myTable^.ProgisId,Col_Int,0);
      end;
   end
   else if FImpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         // add column to list
         AddColumn(myTable,myTable^.ProgisId,Col_Int,0);
      end;
   end
   else if FImpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         // add column to list
         AddColumn(myTable,myTable^.ProgisId,Col_Int,0);
      end;
   end
   else if FImpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         // add column to list
         AddColumn(myTable,myTable^.ProgisId,Col_Int,0);
         AddColumn(myTable,'GUID',Col_String,50);
         AddColumn(myTable,'_WG_STATE',Col_Int,0);        // add column for state
         AddColumn(myTable,'_WG_INTERNAL_ID',Auto_Inc,0); // add auto-increment column
      end;
   end
   else if FImpDBMode = Text_File then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         // add column to list
         AddColumn(myTable,myTable^.ProgisId,Col_Int,0);
      end;
   end
end;

// ------------------------------------------------------------------------
// NAME: CheckImpColumnMappings
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to check if a mapping for a column has been
//              already done somewhere else to avoid creation of mapping at
//              each create of table
//
// PARAMETERS : aTable      -> Pointer to current table
//              aColumnname -> Name of the column that should be mapped
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.CheckImpColumnMappings(var aTable:pTable_rec; var aColname:string);
var t_iterate:pTable_rec;
    mapped   :boolean;
    m_iterate:pMapping_rec;
begin
   t_iterate:=ImpTables; mapped:=FALSE;
   while (t_iterate <> nil) and (not mapped) do
   begin
      if (t_iterate^.HasMappedCols) then
      begin
         // check if this table already has mapping for this column
         m_iterate:=t_iterate^.Colmappings;
         while (m_iterate <> nil) and (not mapped) do
         begin
            if (m_iterate^.OriginalName = aColname) then
            begin
               // we found a mapping that has been already done for this column
               // --> add new mapping entry for this table
               InsertMappingEntry(aTable^.Colmappings,aColname, m_iterate^.MappedName);
               aTable^.HasMappedCols:=TRUE;
               aColname:=m_iterate^.MappedName;
               mapped:=TRUE;
            end;
            m_iterate:=m_iterate^.next;
         end;
      end;
      t_iterate:=t_iterate^.next;
   end;
end;

// ------------------------------------------------------------------------
// NAME: AddImpColumn
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to add column to import table
//
// PARAMETERS : Layername  -> Name of the layer in GIS
//              Columnname -> Name of the column
//              ColType    -> Type of the column
//              ColLength  -> Length of the column
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.AddImpColumn(const Layername,
  ColumnName: WideString; ColumnType: TDataBaseColTypes;
  ColumnLength: Integer);
var myTable:pTable_rec;
    aLayername:string;
begin
   if (FImpDBMode = DB_None) then exit;
   aLayername:=GetMappedName(ImpLayerMappings,Layername);
   if FImpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         AddColumn(myTable,Columnname,ColumnType,ColumnLength);
      end
      else
         AddUpdateColumn(myTable, Columnname, ColumnType, ColumnLength);
   end
   else if FImpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         AddColumn(myTable,Columnname,ColumnType,ColumnLength);
      end
      else
         AddUpdateColumn(myTable, Columnname, ColumnType, ColumnLength);
   end
   else if FImpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         AddColumn(myTable,Columnname,ColumnType,ColumnLength);
      end
      else
         AddUpdateColumn(myTable, Columnname, ColumnType, ColumnLength);
   end
   else if FImpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         AddColumn(myTable,Columnname,ColumnType,ColumnLength);
      end
      else
         AddUpdateColumn(myTable, Columnname, ColumnType, ColumnLength);
   end
   else if FImpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         AddColumn(myTable,Columnname,ColumnType,ColumnLength);
      end
      else
         AddUpdateColumn(myTable, Columnname, ColumnType, ColumnLength);
   end
   else if FImpDbMode = Text_File then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         AddColumn(myTable,Columnname,ColumnType,ColumnLength);
      end
      else
         AddUpdateColumn(myTable, Columnname, ColumnType, ColumnLength);
   end;
end;

// ------------------------------------------------------------------------
// NAME: CreateImpTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to create import table
//
// PARAMETERS : Layername  -> Name of the layer in GIS
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.CreateImpTable(const Layername: WideString);
var myTable   :pTable_rec;
    aLayername:string;
begin
   if (FImpDBMode = DB_None) then exit;
   aLayername:=GetMappedName(ImpLayerMappings,Layername);
   if FImpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         // now the table has to be created
         CreatedBaseImpTable(myTable);
         myTable^.needtobecreated:=false;
         myTable^.hadbeencreated:=true;
      end;
   end
   else if FImpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         // now the table has to be created
         CreatedBaseImpTable(myTable);
         myTable^.needtobecreated:=false;
         myTable^.hadbeencreated:=true;
      end;
   end
   else if FImpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         // now the table has to be created
         CreateAccessImpTable(myTable);
         myTable^.needtobecreated:=false;
         myTable^.hadbeencreated:=true;
      end
      else
         AlterAccessImpTable(myTable); // the table could be altered (if neccessary)
   end
   else if FImpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         // now the table has to be created
         CreateAccessImpTable(myTable);
         myTable^.needtobecreated:=false;
         myTable^.hadbeencreated:=true;
      end
      else
         AlterAccessImpTable(myTable); // the table could be altered (if neccessary)
   end
   else if FImpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         // now the table has to be created
         CreateAccessImpTable(myTable);
         myTable^.needtobecreated:=false;
         myTable^.hadbeencreated:=true;
      end
      else
         AlterAccessImpTable(myTable); // the table could be altered (if neccessary)
   end
   else if FImpDBMode = Text_File then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if myTable^.needtobecreated then
      begin
         // now the table has to be created
         CreateTextImpTable(myTable);
         myTable^.needtobecreated:=false;
         myTable^.hadbeencreated:=true;
      end;
   end
end;

// ------------------------------------------------------------------------
// NAME: DeleteExpDBEntry
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to delete an entry in an export table
//
// PARAMETERS : Layername  -> Name of the layer in GIS
//              Id         -> Id of the entry that should be deleted
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.DeleteExpDBEntry(const Layername: WideString;
  Id: Integer);
var myTable:pTable_rec;
    Filterstring:string;
    aLayername  :string;
begin
   if (FExpDBMode = DB_None) then
      exit;

   aLayername:=GetMappedName(ExpLayerMappings,Layername);
   if FExpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         if ADOTable.FieldByName(myTable^.ProgisID).AsString <> inttostr(ID) then exit;
         ADOTable.Delete;
      end;
   end
   else if FExpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;

         if ADOTable.FieldByName(myTable^.ProgisID).AsString <> inttostr(ID) then exit;
         ADOTable.Delete;
      end;
   end
   else if FExpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;

         if ADOTable.FieldByName(myTable^.ProgisID).AsString <> inttostr(ID) then exit;
         ADOTable.Delete;
      end;
   end
   else if FExpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         if ADOTable.FieldByName(myTable^.ProgisID).AsString <> inttostr(ID) then exit;
         if ADOTable.Supports([coDelete]) then
         begin
            if ADOTable.RecordCount = 1 then
               ADOTable.Delete;
         end;
      end;
   end
   else if FExpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         if ADOTable.FieldByName(myTable^.ProgisID).AsString <> inttostr(ID) then exit;
         if ADOTable.Supports([coDelete]) then
         begin
            if ADOTable.RecordCount = 1 then
               ADOTable.Delete;
         end;
      end;
   end;
end;

// ------------------------------------------------------------------------
// NAME: DeleteExpTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to delete an export table
//
// PARAMETERS : Layername -> Name of the layer
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.DeleteExpTable(const Layername: WideString);
var myTable:pTable_rec;
    aLayername:string;
begin
   if (FExpDBMode = DB_None) then
      exit;
   aLayername:=GetMappedName(ExpLayerMappings,Layername);
   if FExpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         // check if the record-count is 0 -> then add dummy entry
         if myTable^.DoPost then
            PostDataToTable(myTable);
         if (ADOTable.Recordcount = 0) then
         begin
            // add dummy field that table can be deleted
            ADOTable.Append;
            ADOTable.FieldByName(myTable^.ProgisID).AsString:='1';
            PostDataToTable(myTable);
         end;
         ADOTable.active:=FALSE;
      end;
      DeleteTable(myTable^.Tablename, DOEXPORT);
      myTable^.opened:=FALSE;
      myTable^.isdeleted:=TRUE;
   end
   else if FExpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         // check if the record-count is 0 -> then add dummy entry
         if myTable^.DoPost then
            PostDataToTable(myTable);
         if (ADOTable.Recordcount = 0) then
         begin
            // add dummy field that table can be deleted
            ADOTable.Append;
            ADOTable.FieldByName(myTable^.ProgisID).AsString:='1';
            PostDataToTable(myTable);
         end;
         ADOTable.active:=FALSE;
      end;
      DeleteTable(myTable^.Tablename, DOEXPORT);
      myTable^.opened:=FALSE;
      myTable^.isdeleted:=TRUE;
   end
   else if FExpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         // check if the record-count is 0 -> then add dummy entry
         if myTable^.DoPost then
            PostDataToTable(myTable);
         if (ADOTable.Recordcount = 0) then
         begin
            // add dummy field that table can be deleted
            ADOTable.Append;
            ADOTable.FieldByName(myTable^.ProgisID).AsString:='1';
            PostDataToTable(myTable);
         end;
         ADOTable.active:=FALSE;
      end;
      DeleteTable(myTable^.Tablename, DOEXPORT);
      myTable^.opened:=FALSE;
      myTable^.isdeleted:=TRUE;
   end
   else if FExpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         // check if the record-count is 0 -> then add dummy entry
         if myTable^.DoPost then
            PostDataToTable(myTable);
         if (ADOTable.Recordcount = 0) then
         begin
            // add dummy field that table can be deleted
            ADOTable.Append;
            ADOTable.FieldByName(myTable^.ProgisID).AsString:='1';
            PostDataToTable(myTable);
         end;
         ADOTable.active:=FALSE;
      end;
      DeleteTable(myTable^.Tablename, DOEXPORT);
      myTable^.opened:=FALSE;
      myTable^.isdeleted:=TRUE;
   end
   else if FExpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         // check if the record-count is 0 -> then add dummy entry
         if myTable^.DoPost then
            PostDataToTable(myTable);
         if (ADOTable.Recordcount = 0) then
         begin
            // add dummy field that table can be deleted
            ADOTable.Append;
            ADOTable.FieldByName(myTable^.ProgisID).AsString:='1';
            PostDataToTable(myTable);
         end;
         ADOTable.active:=FALSE;
      end;
      DeleteTable(myTable^.Tablename, DOEXPORT);
      myTable^.opened:=FALSE;
      myTable^.isdeleted:=TRUE;
   end;
end;

// method is used to do the settings by using a dialog
procedure TAXImpExpDbc.SetupByDialog(ParentWinHandle, WinLeft,
  WinTop: Integer);
var ProjName:WideString;
begin
   if SetDbWnd <> nil then
      SetDbWnd.Free;
   SetDbWnd:=TSetDbWnd.Create(nil);
   SetDbWnd.LngCode:=FLngCode;
   // set the database properties
   SetDbWnd.WorkingDir:=FWorkingDir;
   SetDbWnd.ParentWinHandle:=ParentWinHandle;
   SetDbWnd.WinLeft:=WinLeft;
   SetDbWnd.WinTop:=WinTop;
   SetDbWnd.AllowModifyImpCb.Checked:=FAllowModifyImpData;
   if (FShowMode = Shw_SourceTarget) or (FShowMode = Shw_Source) then
   begin
      SetDbWnd.SourceDbMode:=FExpDbMode;
      FEvents.OnRequestExpProjectName(ProjName);
      SetDbWnd.ExpAmpFilename:=ProjName;
      SetDbWnd.SourceDatabase:=FExpDatabase;
      SetDbWnd.SourceTable:=FExpTable;
   end;
   if (FShowMode = Shw_SourceTarget) or (FShowMode = Shw_Target) then
   begin
      SetDbWnd.DestDbMode:=FImpDbMode;
      FEvents.OnRequestImpProjectName(ProjName);
      SetDbWnd.ImpAmpFilename:=ProjName;
      SetDbWnd.DestDatabase:=FImpDatabase;
      SetDbWnd.DestTable:=FImpTable;
   end;
   SetDbWnd.ShowMode:=FShowMode;
   SetDbWnd.OnClose:=OnDbWndClose;
   SetDbWnd.SetupByDialog;  // now execute the dialog
end;

procedure TAxImpExpDbc.OnDbWndClose(Sender: TObject; var Action: TCloseAction);
begin
   // get the database properties
   if (FShowMode = Shw_SourceTarget) or (FShowMode = Shw_Source) then
   begin
      FExpDbMode:=SetDbWnd.SourceDbMode;
      FExpDatabase:=SetDbWnd.SourceDatabase;
      FExpTable:=SetDbWnd.SourceTable;
   end;
   if (FShowMode = Shw_SourceTarget) or (FShowMode = Shw_Target) then
   begin
      FImpDbMode:=SetDbWnd.DestDbMode;
      FImpDatabase:=SetDbWnd.DestDatabase;
      FImpTable:=SetDbWnd.DestTable;
   end;
   FAllowModifyImpData:=SetDbWnd.AllowModifyImpCb.Checked;
   if FEvents <> nil then FEvents.OnDialogSetuped;
end;

function TAXImpExpDbc.Get_ShowMode: TDatabaseShowmodes;
begin
   result:=FShowMode;
end;

procedure TAXImpExpDbc.Set_ShowMode(Value: TDatabaseShowmodes);
begin
   FShowMode:=Value;
end;

function TAXImpExpDbc.Get_ImpDbInfo: WideString;
var retVal:string;
    aStrFileObj:ClassStrFile;
    ampFilename:string;
    wgiFilename:string;
    ProjName:wideString;
begin
   aStrFileObj:=ClassStrFile.Create;
   aStrfileObj.CreateList(FWorkingDir, FLngCode, 'IMPEXPDBC');
   retVal:=aStrfileObj.Assign(22); // Target database:
   if FImpDbMode = Db_None then
   begin
      retVal:=retVal+aStrFileObj.Assign(17); // none
   end
   else if FImpDbMode = DBF_Single then
   begin
      retVal:=retVal+aStrFileObj.Assign(18) + ' ' + FImpDataBase; // dBase table
   end
   else if FImpDbMode = DBF_Directory then
   begin
      retVal:=retVal+aStrFileObj.Assign(19) + ' ' + FImpDataBase; // dBase directory
   end
   else if FImpDbMode = ACCESS_Single then
   begin
      retVal:=retVal + aStrFileObj.Assign(20) + ' ' + FImpTable + ', '  +aStrFileObj.Assign(21) + ' '+ FImpDatabase; // Access table of Access database
   end
   else if FImpDbMode = ACCESS_Tables then
   begin
      retVal:=retVal+aStrFileObj.Assign(21) + ' '+ FImpDatabase; // Access database
   end
   else if FImpDbMode = Idb_Database then
   begin
      FEvents.OnRequestImpProjectName(ProjName);
      ampFilename:=ProjName;
      if ampFilename = '' then
      begin
         retVal:=retVal+aStrFileObj.Assign(26);
      end
      else
      begin
         wgiFilename:=ChangeFileExt(ampFilename,'.wgi');
         if FImpDatabase = '' then FImpDatabase:=wgiFilename;

         if (wgiFilename = FImpDatabase) then
            retVal:=retVal+aStrFileObj.Assign(25) + ' ' + ExtractFilename(ampFilename)
         else
            retVal:=retVal+aStrFileObj.Assign(26) + ' '+ FImpDatabase;
      end;
   end
   else if FImpDbMode = Text_File then
   begin
      retVal:=retVal+aStrFileObj.Assign(40) + ' ' + FImpDataBase; // Textfile directory
   end;
   aStrfileObj.Destroy;
   result:=retVal;
end;

function TAXImpExpDbc.Get_ExpDbInfo: WideString;
var retVal:string;
    aStrFileObj:ClassStrFile;
    ampFilename:string;
    wgiFilename:string;
    ProjName:widestring;
begin
   aStrFileObj:=ClassStrFile.Create;
   aStrfileObj.CreateList(FWorkingDir, FLngCode, 'IMPEXPDBC');
   retVal:=aStrfileObj.Assign(16); // Source database:
   if FExpDbMode = Db_None then
   begin
      retVal:=retVal+aStrFileObj.Assign(17); // none
   end
   else if FExpDbMode = DBF_Single then
   begin
      retVal:=retVal+aStrFileObj.Assign(18) + ' ' + FExpDataBase; // dBase table
   end
   else if FExpDbMode = DBF_Directory then
   begin
      retVal:=retVal+aStrFileObj.Assign(19) + ' ' + FExpDataBase; // dBase directory
   end
   else if FExpDbMode = ACCESS_Single then
   begin
      retVal:=retVal + aStrFileObj.Assign(20) + ' ' + FExpTable + ', '  +aStrFileObj.Assign(21) + ' ' +FExpDataBase; // Access table of Access database
   end
   else if FExpDbMode = ACCESS_Tables then
   begin
      retVal:=retVal+aStrFileObj.Assign(21) + ' ' + FExpDataBase; // Access database
   end
   else if FExpDbMode = Idb_Database then
   begin
      FEvents.OnRequestExpProjectName(ProjName);
      ampFilename:=ProjName;
      if ampFilename = '' then
      begin
         retVal:=retVal+aStrFileObj.Assign(26);
      end
      else
      begin
         wgiFilename:=ChangeFileExt(ampFilename,'.wgi');
         if FExpDataBase = '' then FExpDataBase:=wgiFilename;
         if (wgiFilename = FExpDataBase) then
            retVal:=retVal+aStrFileObj.Assign(25) + ' ' + ExtractFilename(ampFilename)
         else
            retVal:=retVal+aStrFileObj.Assign(26) + ' '+ FExpDataBase;
      end;
   end;
   aStrfileObj.Destroy;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: DoEditImpInfo
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to edit data from an import-table
//
// PARAMETERS : Layername -> Name of the layer
//              ID        -> Id of the object that should be edited
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.DoEditImpInfo(const Layername: WideString;
  Id: Integer);
var myTable:pTable_rec;
    aLayername:string;
    FilterString:string;
begin
   if (FImpDBMode = DB_None) then exit;
   aLayername:=GetMappedName(ImpLayerMappings,Layername);
   if FImpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then exit;
      if ADOTable.Connection = nil then
         ADOTable.Connection:=ADOdBaseImp;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         // check if the table should be posted
         if myTable^.DoPost then
            PostDataToTable(myTable);
         // check if an entry for this Id already exists
         // if not it will be added
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         if ADOTable.FieldByName(myTable^.ProgisID).AsString <> inttostr(ID) then
            ADOTable.Append
         else
            ADOTable.Edit;
         myTable^.DoPost:=true;
         AddImpInfo(Layername,MyTable^.ProgisID,inttostr(ID));
      end;
   end
   else if FImpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if ADOTable.Connection = nil then
         ADOTable.Connection:=ADOdBaseImp;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         // check if the table should be posted
         if myTable^.DoPost then
            PostDataToTable(myTable);
         // check if an entry for this Id already exists
         // if not it will be added
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         if ADOTable.FieldByName(myTable^.ProgisID).AsString <> inttostr(ID) then
            ADOTable.Append
         else
            ADOTable.Edit;
         myTable^.DoPost:=true;
         AddImpInfo(Layername,MyTable^.ProgisID,inttostr(ID));
      end;
   end
   else if FImpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         // check if the table should be posted
         if myTable^.DoPost then
            PostDataToTable(myTable);
         // check if an entry for this Id already exists
         // if not it will be added
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         if ADOTable.FieldByName(myTable^.ProgisID).AsString <> inttostr(ID) then
            ADOTable.Append
         else
            ADOTable.Edit;
         myTable^.DoPost:=true;
         AddImpInfo(Layername,MyTable^.ProgisID,inttostr(ID));
      end;
   end
   else if FImpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         // check if the table should be posted
         if myTable^.DoPost then
            PostDataToTable(myTable);
         // check if an entry for this Id already exists
         // if not it will be added
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         if ADOTable.FieldByName(myTable^.ProgisID).AsString <> inttostr(ID) then
            ADOTable.Append
         else
            ADOTable.Edit;
         myTable^.DoPost:=true;
         AddImpInfo(Layername,MyTable^.ProgisID,inttostr(ID));
      end;
   end
   else if FImpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         // check if the table should be posted
         if myTable^.DoPost then
            PostDataToTable(myTable);

         // check if an entry for this Id already exists
         // if not it will be added
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         if ADOTable.FieldByName(myTable^.ProgisID).AsString <> inttostr(ID) then
         begin
            ADOTable.Append;
            AddImpInfo(Layername,MyTable^.ProgisID,inttostr(ID));
            AddImpInfo(Layername,'_WG_STATE','1'); // add state info for idb
         end
         else
            ADOTable.Edit;
         myTable^.DoPost:=true;
      end;
   end;
end;

function TAXImpExpDbc.Get_AllowModifyImpData: WordBool;
begin
   result:=FAllowModifyImpData;
end;

procedure TAXImpExpDbc.Set_AllowModifyImpData(Value: WordBool);
begin
   FAllowModifyImpData:=Value;
end;

// ------------------------------------------------------------------------
// NAME: DoExportRawData
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to read out current line of raw data table
//
// PARAMETERS : Layername -> Name of the layer
//
// RETURNVALUE: TRUE  -> Got database information
//              FALSE -> Got no database information
// ------------------------------------------------------------------------
function TAXImpExpDbc.DoExportRawData(const Layername: WideString): WordBool;
var retVal:boolean;
    myTable:pTable_rec;
    aLayername:string;
begin
   retVal:=false;
   if (FExpDBMode = DB_None) then
   begin
      result:=retVal;
      exit;
   end;
   aLayername:=GetMappedName(ExpLayerMappings,Layername);
   if FExpDBMode = DBF_Single_Raw then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then begin result:=FALSE; exit; end;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         retVal:=ReadRawData(myTable);
         myTable^.Current_Column:=myTable^.Columns;
         myTable^.Current_Info:=myTable^.Infos;
      end;
   end
   else if FExpDBMode = DBF_Directory_Raw then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then begin result:=FALSE; exit; end;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         retVal:=ReadRawData(myTable);
         myTable^.Current_Column:=myTable^.Columns;
         myTable^.Current_Info:=myTable^.Infos;
      end;
   end;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: DoExportFromImpTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to export data from an import table
//
// PARAMETERS : Layername -> Name of the layer
//              ID        -> Id of the object that should be exported
//
// RETURNVALUE: TRUE  -> Id has database info
//              FALSE -> Id has no database info
// ------------------------------------------------------------------------
function TAXImpExpDbc.DoExportFromImpTable(const Layername: WideString; Id: Integer): WordBool;
var retVal:boolean;
    myTable:pTable_rec;
    Filterstring:string;
    aLayername:string;
begin
   retVal:=false;
   if (FImpDBMode = DB_None) then
   begin
      result:=retVal;
      exit;
   end;
   aLayername:=GetMappedName(ImpLayerMappings,Layername);
   if FImpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then begin result:=FALSE; exit; end;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         if myTable^.DoPost then // post the last data if necessary
            PostDataToTable(myTable);
         myTable^.DoPost:=FALSE;
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         retVal:=ReadFilterData(myTable,ID);
         myTable^.Current_Column:=myTable^.Columns;
         myTable^.Current_Info:=myTable^.Infos;
      end;
   end
   else if FImpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then begin result:=FALSE; exit; end;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         if myTable^.DoPost then // post the last data if necessary
            PostDataToTable(myTable);
         myTable^.DoPost:=FALSE;
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         retVal:=ReadFilterData(myTable,ID);
         myTable^.Current_Column:=myTable^.Columns;
         myTable^.Current_Info:=myTable^.Infos;
      end;
   end
   else if FImpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then begin result:=false; exit; end;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         if myTable^.DoPost then // post the last data if necessary
            PostDataToTable(myTable);
         myTable^.DoPost:=FALSE;
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         retVal:=ReadFilterData(myTable,ID);
         myTable^.Current_Column:=myTable^.Columns;
         myTable^.Current_Info:=myTable^.Infos;
      end;
   end
   else if FImpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then begin result:=false; exit; end;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         if myTable^.DoPost then // post the last data if necessary
            PostDataToTable(myTable);
         myTable^.DoPost:=FALSE;
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         retVal:=ReadFilterData(myTable,ID);
         myTable^.Current_Column:=myTable^.Columns;
         myTable^.Current_Info:=myTable^.Infos;
      end;
   end
   else if FImpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then begin result:=false; exit; end;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         if myTable^.DoPost then // post the last data if necessary
            PostDataToTable(myTable);
         myTable^.DoPost:=FALSE;
         Filterstring:=myTable^.ProgisID+'='+ chr(39) +inttostr(ID) + chr(39);
         ADOTable.Filter:=FilterString;
         retVal:=ReadFilterData(myTable,ID);
         myTable^.Current_Column:=myTable^.Columns;
         myTable^.Current_Info:=myTable^.Infos;
      end;
   end;
   result:=retVal;
end;

// ------------------------------------------------------------------------
// NAME: GetExportDataFromImpTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to get a info of a column of an import table
//
// PARAMETERS : Layername    -> Name of the layer
//              Column       -> Name of the column out of the import table
//              Info         -> Information of the column
//
// RETURNVALUE: TRUE  -> Database info received
//              FALSE -> No database info received
// ------------------------------------------------------------------------
function TAXImpExpDbc.GetExportDataFromImpTable(const Layername: WideString; out Columnname, Info: WideString): WordBool;
var retVal:boolean;
    myTable:pTable_rec;
    aLayername:string;
begin
   retVal:=false;
   if (FImpDBMode = DB_None) then
   begin
      result:=retVal;
      exit;
   end;

   aLayername:=GetMappedName(ImpLayerMappings,Layername);

   Columnname:=''; Info:='';
   if FImpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      // the PROGISID column will not be exported
      if myTable^.Current_Column^.Columnname = myTable^.ProgisID then
         myTable^.Current_Column:=myTable^.Current_Column^.next;

      if myTable^.Current_Column <> nil then
      begin
         Columnname:=myTable^.Current_Column^.Columnname;
         Info:=myTable^.Current_Info^.Info;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
         retVal:=true;
      end;
   end
   else if FImpDBMode = DBF_Single_Raw then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      if myTable^.Current_Column <> nil then
      begin
         Columnname:=myTable^.Current_Column^.Columnname;
         Info:=myTable^.Current_Info^.Info;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
         retVal:=true;
      end;
   end
   else if FImpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      // the PROGISID column will not be exported
      if myTable^.Current_Column^.Columnname = myTable^.ProgisID then
         myTable^.Current_Column:=myTable^.Current_Column^.next;

      if myTable^.Current_Column <> nil then
      begin
         Columnname:=myTable^.Current_Column^.Columnname;
         Info:=myTable^.Current_Info^.Info;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
         retVal:=true;
      end;
   end
   else if FImpDBMode = DBF_Directory_Raw then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column <> nil then
      begin
         Columnname:=myTable^.Current_Column^.Columnname;
         Info:=myTable^.Current_Info^.Info;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
         retVal:=true;
      end;
   end
   else if FImpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      // the PROGISID column will not be exported
      if myTable^.Current_Column^.Columnname = myTable^.ProgisID then
         myTable^.Current_Column:=myTable^.Current_Column^.next;

      if myTable^.Current_Column <> nil then
      begin
         Columnname:=myTable^.Current_Column^.Columnname;
         Info:=myTable^.Current_Info^.Info;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
         retVal:=true;
      end;
   end
   else if FImpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      // the PROGISID column will not be exported
      if myTable^.Current_Column^.Columnname = myTable^.ProgisID then
         myTable^.Current_Column:=myTable^.Current_Column^.next;

      if myTable^.Current_Column <> nil then
      begin
         Columnname:=myTable^.Current_Column^.Columnname;
         Info:=myTable^.Current_Info^.Info;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
         retVal:=true;
      end;
   end
   else if FImpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;
      if myTable^.Current_Column = nil then
      begin
         retVal:=false;
         result:=retVal;
         exit;
      end;

      // the PROGISID column will not be exported
      if myTable^.Current_Column^.Columnname = myTable^.ProgisID then
         myTable^.Current_Column:=myTable^.Current_Column^.next;
      // the _WG_STATE column will not be exported
      if myTable^.Current_Column^.Columnname = '_WG_STATE' then
      begin
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
      end;
      // the _WG_INTERNAL_ID column will not be exported
      if myTable^.Current_Column^.Columnname = '_WG_INTERNAL_ID' then
      begin
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
      end;

      if myTable^.Current_Column <> nil then
      begin
         Columnname:=myTable^.Current_Column^.Columnname;
         Info:=myTable^.Current_Info^.Info;
         myTable^.Current_Column:=myTable^.Current_Column^.next;
         myTable^.Current_Info:=myTable^.Current_Info^.next;
         retVal:=true;
      end;
   end;
   result:=retVal;
end;

function TAXImpExpDbc.Get_CreateIndexOnClose: WordBool;
begin
   result:=FCreateIndexOnClose;
end;

procedure TAXImpExpDbc.Set_CreateIndexOnClose(Value: WordBool);
begin
   FCreateIndexOnClose:=Value;
end;

function TAXImpExpDbc.GetImpNumRecords(const Layername: WideString): Integer;
var myTable   :pTable_rec;
    curdir    :string;
    aLayername:string;
begin
   result:=0; // default return value is 0
   if (FImpDBMode = DB_None) then exit;
   aLayername:=GetMappedName(ImpLayerMappings,Layername);
   if FImpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then exit;
      if (myTable^.opened) then
      begin
         result:=AdoTable.RecordCount;
      end;
   end
   else if FImpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if (myTable^.opened) then
      begin
         result:=AdoTable.RecordCount;
      end;
   end
   else if FImpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ImpTables,'SINGLE',DOIMPORT);
      if myTable = nil then exit;
      if (myTable^.opened) then
      begin
         result:=AdoTable.RecordCount;
      end;
   end
   else if FImpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if (myTable^.opened) then
      begin
         result:=AdoTable.RecordCount;
      end;
   end
   else if FImpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ImpTables,aLayername,DOIMPORT);
      if myTable = nil then exit;
      if (myTable^.opened) then
      begin
         result:=AdoTable.RecordCount;
      end;
   end;
end;

function TAXImpExpDbc.GetExpNumRecords(const Layername: WideString): Integer;
var myTable   :pTable_rec;
    curdir    :string;
    aLayername:string;
begin
   result:=0; // default return value is 0
   if (FExpDBMode = DB_None) then exit;
   aLayername:=GetMappedName(ExpLayerMappings,Layername);
   if FExpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) then
      begin
         result:=AdoTable.RecordCount;
      end;
   end
   else if FExpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) then
      begin
         result:=AdoTable.RecordCount;
      end;
   end
   else if FExpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) then
      begin
         result:=AdoTable.RecordCount;
      end;
   end
   else if FExpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) then
      begin
         result:=AdoTable.RecordCount;
      end;
   end
   else if FExpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) then
      begin
         result:=AdoTable.RecordCount;
      end;
   end;
end;

procedure TAXImpExpDbc.SetupByModalDialog;
var ProjName:WideString;
begin
   if SetDbWnd <> nil then
      SetDbWnd.Free;
   SetDbWnd:=TSetDbWnd.Create(nil);
   SetDbWnd.LngCode:=FLngCode;
   // set the database properties
   SetDbWnd.WorkingDir:=FWorkingDir;
   SetDbWnd.BorderStyle:=bsDialog;
   SetDbWnd.FormStyle:=fsNormal;
   SetDbWnd.Position:=poScreenCenter;
   SetDbWnd.Visible:=False;
   SetDbWnd.WindowState:=wsNormal;
   SetDbWnd.Height:=SetDbWnd.winheight;
   SetDbWnd.Width:=SetDbWnd.winwidth;
   SetDbWnd.DoShowModal:=TRUE;
   SetDbWnd.AllowModifyImpCb.Checked:=FAllowModifyImpData;
   if (FShowMode = Shw_SourceTarget) or (FShowMode = Shw_Source) then
   begin
      SetDbWnd.SourceDbMode:=FExpDbMode;
      FEvents.OnRequestExpProjectName(ProjName);
      SetDbWnd.ExpAmpFilename:=ProjName;
      SetDbWnd.SourceDatabase:=FExpDatabase;
      SetDbWnd.SourceTable:=FExpTable;
   end;
   if (FShowMode = Shw_SourceTarget) or (FShowMode = Shw_Target) then
   begin
      SetDbWnd.DestDbMode:=FImpDbMode;
      FEvents.OnRequestImpProjectName(ProjName);
      SetDbWnd.ImpAmpFilename:=ProjName;
      SetDbWnd.DestDatabase:=FImpDatabase;
      SetDbWnd.DestTable:=FImpTable;
   end;
   SetDbWnd.ShowMode:=FShowMode;
   SetDbWnd.ShowModal;      // now show the Dialog

   // get the database properties
   if (FShowMode = Shw_SourceTarget) or (FShowMode = Shw_Source) then
   begin
      FExpDbMode:=SetDbWnd.SourceDbMode;
      FExpDatabase:=SetDbWnd.SourceDatabase;
      FExpTable:=SetDbWnd.SourceTable;
   end;
   if (FShowMode = Shw_SourceTarget) or (FShowMode = Shw_Target) then
   begin
      FImpDbMode:=SetDbWnd.DestDbMode;
      FImpDatabase:=SetDbWnd.DestDatabase;
      FImpTable:=SetDbWnd.DestTable;
   end;
   FAllowModifyImpData:=SetDbWnd.AllowModifyImpCb.Checked;
end;

function TAXImpExpDbc.Get_RemoveEmptyColumnsOnClose: WordBool;
begin
   result:=FRemoveEmptyColumnsOnClose;
end;

procedure TAXImpExpDbc.Set_RemoveEmptyColumnsOnClose(Value: WordBool);
begin
   FRemoveEmptyColumnsOnClose:=Value;
end;

function TAXImpExpDbc.Get_TmpDBaseDir: WideString;
begin
   result:=FTmpDBaseDir;
end;

procedure TAXImpExpDbc.Set_TmpDBaseDir(const Value: WideString);
begin
   FTmpDBaseDir:=Value;
   // check if last character is a \
   if FTmpDBaseDir[length(FTmpDBaseDir)] <> '\' then
      FTmpDBasedir:=FTmpDBaseDir+'\';
end;

// ------------------------------------------------------------------------
// NAME: EmptyExpTable
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to remove all data items from export table
//
// PARAMETERS : Layername -> Name of the layer
//
// RETURNVALUE: NONE
// ------------------------------------------------------------------------
procedure TAXImpExpDbc.EmptyExpTable(const Layername: WideString);
var myTable:pTable_rec;
    aLayername:string;
begin
   if (FExpDBMode = DB_None) then
      exit;
   aLayername:=GetMappedName(ExpLayerMappings,Layername);
   if FExpDBMode = DBF_Single then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         if myTable^.DoPost then
            PostDataToTable(myTable);
         while ADOTable.RecordCount > 0 do
         begin
            ADOTable.First;
            ADOTable.Delete;
         end;
      end
      else
      begin
         ADOTable.active:=TRUE;
         while ADOTable.RecordCount > 0 do
         begin
            ADOTable.First;
            ADOTable.Delete;
         end;
      end;

      // add dummy field that table is not empty at close of tables
      ADOTable.Append;
      ADOTable.FieldByName(myTable^.ProgisID).AsString:='1';
      PostDataToTable(myTable);
      ADOTable.active:=FALSE; // close the table
      myTable^.opened:=FALSE;
   end
   else if FExpDBMode = DBF_Directory then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then exit;
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         if myTable^.DoPost then
            PostDataToTable(myTable);
         while ADOTable.RecordCount > 0 do
         begin
            ADOTable.First;
            ADOTable.Delete;
         end;
      end
      else
      begin
         ADOTable.active:=TRUE;
         while ADOTable.RecordCount > 0 do
         begin
            ADOTable.First;
            ADOTable.Delete;
         end;
      end;
      // add dummy field that table is not empty at close of tables
      ADOTable.Append;
      ADOTable.FieldByName(myTable^.ProgisID).AsString:='1';
      PostDataToTable(myTable);
      ADOTable.active:=FALSE; // close the table
      myTable^.opened:=FALSE;
   end
   else if FExpDBMode = ACCESS_Single then
   begin
      myTable:=GetTable(ExpTables,'SINGLE',DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         if myTable^.DoPost then
            PostDataToTable(myTable);
         while ADOTable.RecordCount > 0 do
         begin
            ADOTable.First;
            ADOTable.Delete;
         end;
      end
      else
      begin
         ADOTable.active:=TRUE;
         while ADOTable.RecordCount > 0 do
         begin
            ADOTable.First;
            ADOTable.Delete;
         end;
      end;

      // add dummy field that table is not empty at close of tables
      ADOTable.Append;
      ADOTable.FieldByName(myTable^.ProgisID).AsString:='1';
      PostDataToTable(myTable);
      ADOTable.active:=FALSE; // close the table
      myTable^.opened:=FALSE;
   end
   else if FExpDBMode = ACCESS_Tables then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         if myTable^.DoPost then
            PostDataToTable(myTable);
         while ADOTable.RecordCount > 0 do
         begin
            ADOTable.First;
            ADOTable.Delete;
         end;
      end
      else
      begin
         ADOTable.active:=TRUE;
         while ADOTable.RecordCount > 0 do
         begin
            ADOTable.First;
            ADOTable.Delete;
         end;
      end;

      // add dummy field that table is not empty at close of tables
      ADOTable.Append;
      ADOTable.FieldByName(myTable^.ProgisID).AsString:='1';
      PostDataToTable(myTable);
      ADOTable.active:=FALSE; // close the table
      myTable^.opened:=FALSE;
   end
   else if FExpDBMode = Idb_Database then
   begin
      myTable:=GetTable(ExpTables,aLayername,DOEXPORT);
      if myTable = nil then exit;
      if (myTable^.opened) and (not myTable^.Ignoredata) then
      begin
         if myTable^.DoPost then
            PostDataToTable(myTable);
         while ADOTable.RecordCount > 0 do
         begin
            ADOTable.First;
            ADOTable.Delete;
         end;
      end
      else
      begin
         ADOTable.active:=TRUE;
         while ADOTable.RecordCount > 0 do
         begin
            ADOTable.First;
            ADOTable.Delete;
         end;
      end;
      // add dummy field that table is not empty at close of tables
      ADOTable.Append;
      ADOTable.FieldByName(myTable^.ProgisID).AsString:='1';
      PostDataToTable(myTable);
      ADOTable.active:=FALSE; // close the table
      myTable^.opened:=FALSE;
   end;
end;

function TAXImpExpDbc.Get_RefreshThreshold: Integer;
begin
   result:=FRefreshThreshold;
end;

procedure TAXImpExpDbc.Set_RefreshThreshold(Value: Integer);
begin
   FRefreshThreshold:=Value;
end;

initialization
  TActiveXControlFactory.Create(
    ComServer,
    TAXImpExpDbc,
    TPanel,
    Class_AXImpExpDbc,
    1,
    '',
    OLEMISC_SIMPLEFRAME or OLEMISC_ACTSLIKELABEL,
    tmApartment);
end.
