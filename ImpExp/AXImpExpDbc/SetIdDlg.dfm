object SetIdWnd: TSetIdWnd
  Left = 392
  Top = 148
  BorderStyle = bsDialog
  Caption = 'Setze ID Spalte'
  ClientHeight = 211
  ClientWidth = 205
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object IdLB: TListBox
    Left = 4
    Top = 33
    Width = 195
    Height = 144
    ItemHeight = 13
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 4
    Top = 4
    Width = 195
    Height = 25
    TabOrder = 1
    object LayerLabel: TLabel
      Left = 8
      Top = 8
      Width = 32
      Height = 13
      Caption = 'Layer: '
    end
  end
  object OKBtn: TButton
    Left = 4
    Top = 181
    Width = 93
    Height = 23
    Caption = 'Ok'
    Default = True
    DragCursor = crDefault
    ModalResult = 1
    TabOrder = 2
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 106
    Top = 181
    Width = 93
    Height = 23
    Cancel = True
    Caption = 'Abbrechen'
    Default = True
    ModalResult = 2
    TabOrder = 3
    OnClick = CancelBtnClick
  end
end
