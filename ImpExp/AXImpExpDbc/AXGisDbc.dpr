library AXGisDbc;

uses
  ComServ,
  AXImpExpDbcXControl_TLB in 'AXImpExpDbcXControl_TLB.pas',
  AXImpExpDbcImpl in 'AXImpExpDbcImpl.pas' {AXImpExpDbc: CoClass},
  SetTableDlg in 'SetTableDlg.pas' {SetTableWnd},
  Strfile in 'Strfile.pas',
  SetIdDlg in 'SetIdDlg.pas' {SetIdWnd},
  SetDbDlg in 'SetDbDlg.pas' {SetDbWnd},
  TextImpTable in 'TextImpTable.pas';

{$E ocx}

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}
{$R *.RES}

begin
end.
