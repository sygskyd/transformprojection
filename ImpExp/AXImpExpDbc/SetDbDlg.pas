unit SetDbDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, FileCtrl, ComCtrls, StrFile, Db, ADODB, ExtCtrls, OpenDialogsModule;

const
      DB_None           = 0;      // no database
      DBF_Single        = 1;      // single dBase File
      DBF_Directory     = 2;      // dBase Directory
      ACCESS_Single     = 3;      // Access Single Database
      ACCESS_Tables     = 4;      // Access Tables
      IDB_Database      = 5;      // IDB Database
      Text_File         = 8;      // Text file

      Show_SourceTarget = 0;
      Show_Source       = 1;
      Show_Target       = 2;

type
  TSetDbWnd = class(TForm)
    MainPageControl: TPageControl;
    SourceDbSheet: TTabSheet;
    DestDbSheet: TTabSheet;
    UseDbCb: TCheckBox;
    SourceDbPageControl: TPageControl;
    SourcedBaseSheet: TTabSheet;
    SourceAccessSheet: TTabSheet;
    SourcedBaseDirLB: TDirectoryListBox;
    SourcedBaseDriveCB: TDriveComboBox;
    SourcedBaseSingleCB: TCheckBox;
    SourcedBaseFileLB: TFileListBox;
    SourceAccessDirLB: TDirectoryListBox;
    SourceAccessDriveCB: TDriveComboBox;
    SourceAccessFileLB: TFileListBox;
    SourceAccessTableLB: TListBox;
    SourceAccessSingleCB: TCheckBox;
    DestDbPageControl: TPageControl;
    DestdBaseSheet: TTabSheet;
    DestAccessSheet: TTabSheet;
    DestdBaseDirLB: TDirectoryListBox;
    DestdBaseDriveCB: TDriveComboBox;
    DestdBaseSingleCB: TCheckBox;
    DestdBaseFileLB: TFileListBox;
    DestdBaseNewSingleBtn: TButton;
    DestdBaseSingleLabel: TLabel;
    DestAccessDirLB: TDirectoryListBox;
    DestAccessDriveCB: TDriveComboBox;
    DestAccessFileLB: TFileListBox;
    NewMdbBtn: TButton;
    newMdbLabel: TLabel;
    DestAccessSingleCB: TCheckBox;
    AccessNewSingleEdit: TEdit;
    DestAccessTableLB: TListBox;
    OKBtn: TButton;
    CancelBtn: TButton;
    ADOConnection: TADOConnection;
    SaveDialogDBase: TSaveDialog;
    SaveDialogAccess: TSaveDialog;
    SetupWindowTimer: TTimer;
    SourceIdbSheet: TTabSheet;
    DestIdbSheet: TTabSheet;
    SourceIdbCb: TCheckBox;
    SourceIdbPanel: TPanel;
    SourceIdbDirLb: TDirectoryListBox;
    SourceIdbDriveCb: TDriveComboBox;
    SourceIdbFileLb: TFileListBox;
    DestIdbCb: TCheckBox;
    DestIdbPanel: TPanel;
    Panel1: TPanel;
    DestIdbLabel: TLabel;
    DestIdbBtn: TButton;
    SaveDialogWgi: TSaveDialog;
    AllowModifyImpCb: TCheckBox;
    DestTextSheet: TTabSheet;
    DestTextDirLB: TDirectoryListBox;
    DestTextDriveCB: TDriveComboBox;
    Panel2: TPanel;
    Label1: TLabel;
    ImpSepTabCb: TCheckBox;
    ImpSepCommaCb: TCheckBox;
    ImpSepSpaceCb: TCheckBox;
    ImpSepSemicolonCb: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure UseDbCbClick(Sender: TObject);
    procedure SourcedBaseSingleCBClick(Sender: TObject);
    procedure SourceAccessSingleCBClick(Sender: TObject);
    procedure SourceAccessFileLBChange(Sender: TObject);
    procedure SourceAccessFileLBClick(Sender: TObject);
    procedure SourcedBaseDirLBChange(Sender: TObject);
    procedure SourcedBaseFileLBChange(Sender: TObject);
    procedure SourcedBaseFileLBClick(Sender: TObject);
    procedure SourcedBaseDirLBClick(Sender: TObject);
    procedure SourceAccessDirLBChange(Sender: TObject);
    procedure SourceAccessTableLBClick(Sender: TObject);
    procedure DestdBaseSingleCBClick(Sender: TObject);
    procedure DestdBaseFileLBChange(Sender: TObject);
    procedure DestdBaseFileLBClick(Sender: TObject);
    procedure DestdBaseNewSingleBtnClick(Sender: TObject);
    procedure DestAccessFileLBChange(Sender: TObject);
    procedure DestAccessFileLBClick(Sender: TObject);
    procedure NewMdbBtnClick(Sender: TObject);
    procedure DestAccessSingleCBClick(Sender: TObject);
    procedure AccessNewSingleEditChange(Sender: TObject);
    procedure AccessNewSingleEditClick(Sender: TObject);
    procedure DestAccessTableLBClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure DestDbPageControlChange(Sender: TObject);
    procedure SourceDbPageControlChange(Sender: TObject);
    procedure DestdBaseDirLBChange(Sender: TObject);
    procedure DestdBaseDirLBClick(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure SourceAccessDirLBClick(Sender: TObject);
    procedure DestAccessDirLBChange(Sender: TObject);
    procedure DestAccessDirLBClick(Sender: TObject);
    procedure SourceIdbCbClick(Sender: TObject);
    procedure DestIdbCbClick(Sender: TObject);
    procedure DestIdbBtnClick(Sender: TObject);
    procedure SourceIdbDirLbChange(Sender: TObject);
    procedure SourceIdbDirLbClick(Sender: TObject);
    procedure SourceIdbFileLbChange(Sender: TObject);
    procedure SourceIdbFileLbClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DestTextDirLBChange(Sender: TObject);
    procedure DestTextDirLBClick(Sender: TObject);
    procedure ImpSepTabCbClick(Sender: TObject);
    procedure ImpSepCommaCbClick(Sender: TObject);
    procedure ImpSepSpaceCbClick(Sender: TObject);
    procedure ImpSepSemicolonCbClick(Sender: TObject);
  private
    { Private declarations }
    firstshow   :boolean;
    StrfileObj  :ClassStrFile;
    NewMdbFilename:string;
    NewDbfFilename:string;
    OpenDialogsModuleWnd:TOpenDialogsModuleWnd; // window to select filename/fonts

    procedure SetupWindow;
    procedure CheckWindow;
    procedure OnOpenDialogsModuleWndClose(Sender: TObject;  var Action: TCloseAction);
  public
    { Public declarations }
    WorkingDir:string;
    LngCode   :string;
    ShowMode  :integer;
    ImpAmpFilename:string;
    ExpAmpFilename:string;
    ParentWinHandle:integer;
    WinLeft        :integer;
    WinTop         :integer;
    winheight      :integer;
    winwidth       :integer;
    DoShowModal    :boolean;


    // source database
    SourceDbMode  :integer;
    SourceDatabase:string;
    SourceTable   :string;
    // destination database
    DestDbMode    :integer;
    DestDatabase  :string;
    DestTable     :string;

    procedure SetupByDialog;
  end;

var
  SetDbWnd: TSetDbWnd;

implementation

{$R *.DFM}

procedure TSetDbWnd.FormCreate(Sender: TObject);
var myScale:double;
    oldHeight:integer;
    oldWidth :integer;
begin
   BorderIcons:= BorderIcons - [biMaximize] - [biSystemMenu];
   firstshow:=TRUE;

   // setup the defaults
   SourceDbMode:=Db_None;
   SourceDatabase:='';
   SourceTable:='';
   DestDbMode:=Db_None;
   DestDatabase:='';
   DestTable:='';
   UseDbCb.Checked:=FALSE;
   SourcedBaseFileLB.Visible:=FALSE;
   SourceAccessSingleCB.Checked:=FALSE;
   SourceAccessTableLB.Visible:=FALSE;
   DestdBaseSingleCB.Checked:=FALSE;
   DestdBaseFileLB.Visible:=FALSE;
   DestdBaseNewSingleBtn.Visible:=FALSE;
   DestdBaseSingleLabel.Visible:=FALSE;
   DestdBaseSingleLabel.Caption:=''; NewDbfFilename:='';
   newMdbLabel.Caption:=''; NewMdbFilename:='';
   SourceIdbCb.Checked:=FALSE;
   DestIdbCb.Checked:=FALSE;
   DestIdbLabel.Caption:='';
   SourceIdbPanel.Visible:=FALSE;
   DestIdbPanel.Visible:=FALSE;
   DestAccessSingleCB.Checked:=FALSE;
   AccessNewSingleEdit.Visible:=FALSE;
   AccessNewSingleEdit.Text:='';
   DestAccessTableLB.Visible:=FALSE;
   ShowMode:=Show_SourceTarget;
   OldHeight:=Self.Height;
   OldWidth:=Self.Width;

   Self.Height:=1;
   Self.Width:=1;

   myScale:=1;//100000 / Trunc(9600000/(Self.PixelsPerInch));
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
   
   // now adapt winheigt and winwidth
   winheight:=round(oldHeight*myScale);
   winwidth:=round(oldWidth*myScale);
   DoShowModal:=FALSE;
   OpenDialogsModuleWnd:=nil;
end;

procedure TSetDbWnd.SetupByDialog;
begin
   Self.Show;
end;

procedure TSetDbWnd.FormShow(Sender: TObject);
begin
   if FirstShow then
   begin
      StrfileObj:=ClassStrFile.Create;
      StrfileObj.CreateList(WorkingDir, LngCode, 'IMPEXPDBC');
      Self.Caption:=StrfileObj.Assign(7);                  // Setup the database
      UseDbCb.Caption:=StrfileObj.Assign(8);               // Use database
      SourceDbSheet.Caption:=StrfileObj.Assign(9);         // Source database
      DestDbSheet.Caption:=StrfileObj.Assign(10);          // Destination database
      SourcedBaseSheet.Caption:=StrfileObj.Assign(11);     // dBase
      DestdBaseSheet.Caption:=StrfileObj.Assign(11);       // dBase
      SourceAccessSheet.Caption:=StrfileObj.Assign(12);    // Access
      DestAccessSheet.Caption:=StrfileObj.Assign(12);      // Access
      SourceAccessSingleCB.Caption:=StrfileObj.Assign(13); // single table
      SourcedBaseSingleCB.Caption:=StrfileObj.Assign(13);  // single table
      DestdBaseSingleCB.Caption:=StrfileObj.Assign(13);    // single table
      DestAccessSingleCB.Caption:=StrfileObj.Assign(13);   // single table
      OkBtn.Caption:=StrfileObj.Assign(14);                // Ok
      CancelBtn.Caption:=StrfileObj.Assign(15);            // Cancel
      SourceIdBCb.Caption:=StrfileObj.Assign(23);          // Idb has not same name as project
      DestIdbCb.Caption:=StrfileObj.Assign(23);            // Idb has not same name as project
      SourceIdbSheet.Caption:=StrfileObj.Assign(24);       // IDB
      DestIdbSheet.Caption:=StrfileObj.Assign(24);         // IDB
      DestTextSheet.Caption:=StrfileObj.Assign(34);        // Text file
      Label1.Caption:=StrfileObj.Assign(35);               // Seperator
      ImpSepTabCb.Caption:=StrfileObj.Assign(36);          // Tabulator
      ImpSepCommaCb.Caption:=StrfileObj.Assign(37);        // Comma
      ImpSepSpaceCb.Caption:=StrfileObj.Assign(38);        // Space
      ImpSepSemicolonCb.Caption:=StrfileObj.Assign(39);    // Semicolon

      AllowModifyImpCb.Caption:=StrfileObj.Assign(27);     // Overwrite existing fields
      FirstShow:=FALSE;
      SetupWindow;
      if not DoShowModal then
         SetupWindowTimer.Enabled:=TRUE;
   end;
end;

// method is used to check the window
procedure TSetDbWnd.CheckWindow;
var allok      :boolean;
    i          :integer;
    wgifilename:string;
begin
   allok:=TRUE;
   if UseDbCb.Checked = TRUE then
   begin
      // check the source-database settings
      if (SourceDbPageControl.ActivePage = SourcedBaseSheet) and
         ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Source)) then
      begin
         // source database is dBase
         if SourcedBaseSingleCB.Checked then // a dBase single database is set
         begin
            // a single table must be selected
            allok:=FALSE;
            for i:=0 to SourcedBaseFileLB.Items.Count -1 do
            begin
               if SourcedBaseFileLB.Selected[i] then
                  allok:=TRUE;
            end;
         end
         else
         begin
            if SourcedBaseDirLB.Directory = '' then
               allok:=FALSE;
         end;
      end
      else if (SourceDbPageControl.ActivePage = SourceAccessSheet) and
              ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Source)) then
      begin
         // a source Access table has been set
         if SourceAccessSingleCB.Checked then
         begin
            // a mdb-file must be selected
            allok:=FALSE;
            for i:=0 to SourceAccessFileLB.Items.Count -1 do
            begin
               if SourceAccessFileLB.Selected[i] then
                  allok:=TRUE;
            end;

            if allok then
            begin
               // a single table must be selected
               allok:=FALSE;
               for i:=0 to SourceAccessTableLB.Items.Count -1 do
               begin
                  if SourceAccessTableLB.Selected[i] then
                     allok:=TRUE;
               end;
            end;
         end
         else
         begin
            // a mdb-file must be selected
            allok:=FALSE;
            for i:=0 to SourceAccessFileLB.Items.Count -1 do
            begin
               if SourceAccessFileLB.Selected[i] then
                  allok:=TRUE;
            end;
         end;
      end
      else if (SourceDbPageControl.ActivePage = SourceIdbSheet) and
              ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Source)) then
      begin
         if SourceIdbCb.Checked then
         begin
            // idb has not the same name than project name
            // check if there is a item selected
            allok:=FALSE;
            for i:=0 to SourceIdbFileLB.Items.Count -1 do
            begin
               if SourceIdbFileLB.Selected[i] then
                  allok:=TRUE;
            end;
         end
         else
         begin
            // name of the idb is same name than the project
            wgifilename:=ExpAmpFilename;
            wgifilename:=ChangeFileExt(wgifilename,'.wgi');
            if not (FileExists(wgifilename)) then
               allok:=FALSE;
         end;
      end;

      // check the destination database settings
      if (DestDbPageControl.ActivePage = DestdBaseSheet) and
         ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Target)) then
      begin
         if DestdBaseSingleCB.Checked then
         begin
            // a single dBase table has been set
            if NewDbfFilename = '' then
            begin
               // an existing table has been selected in the list
               // a single table must be selected
               allok:=FALSE;
               for i:=0 to DestdBaseFileLB.Items.Count -1 do
               begin
                  if DestdBaseFileLB.Selected[i] then
                     allok:=TRUE;
               end;
            end;
         end
         else
         begin
            // a dbase Directory has been set
            if DestdBaseDirLb.Directory = '' then
               allok:=FALSE;
         end;
      end
      else if (DestDbPageControl.ActivePage = DestAccessSheet) and
              ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Target)) then
      begin
         if DestAccessSingleCB.Checked then
         begin
            // an Access-Single Table has been set
            if AccessNewSingleEdit.Text = '' then
            begin
               // a single table must be selected
               allok:=FALSE;
               for i:=0 to DestAccessTableLB.Items.Count -1 do
               begin
                  if DestAccessTableLB.Selected[i] then
                     allok:=TRUE;
               end;
            end;
         end
         else
         begin
            // Access-Tables has been set
            if NewMdbFilename = '' then
            begin
               // an mdb-Filename has to be set
               allok:=FALSE;
               for i:=0 to DestAccessFileLB.Items.Count -1 do
               begin
                  if DestAccessFileLB.Selected[i] then
                     allok:=TRUE;
               end;
            end;
         end;
      end
      else if (DestDbPageControl.ActivePage = DestIdbSheet) and
              ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Target)) then
      begin
         if DestIdbCb.Checked then
         begin
            if DestIdbLabel.Caption = '' then
               allok:=FALSE;
         end;
      end
      else if (DestDbPageControl.ActivePage = DestTextSheet) and
              ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Target)) then
      begin
         if (ImpSepTabCb.Checked or ImpSepCommaCb.Checked or ImpSepSpaceCb.Checked or ImpSepSemicolonCb.Checked) then
            allok:=TRUE
         else
            allok:=FALSE;
      end;
   end;
   if allok then OkBtn.Enabled:=TRUE else OkBtn.Enabled:=FALSE;
end;

// method is used to setup the window
procedure TSetDbWnd.SetupWindow;
var i    : integer;
    found: boolean;
    wgifilename:string;
begin
   // setup the source-database
   if (SourcedBMode = Db_None) and ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Source)) then
   begin
      UseDbCb.checked:=FALSE; UseDbCbClick(nil);// no database is used
   end;
   if (SourceDbMode <> Db_None) and ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Source)) then
   begin
      UseDbCb.checked:=TRUE; UseDbCbClick(nil);// a database is used
   end;
   if (DestDbMode = Db_None) and ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Target)) then
   begin
      UseDbCb.checked:=FALSE; UseDbCbClick(nil);// no database is used
   end;
   if (DestDbMode <> Db_None) and ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Target)) then
   begin
      UseDbCb.checked:=TRUE; UseDbCbClick(nil);// a database is used
   end;

   if (SourcedBMode = DBF_Directory) and ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Source)) then
   begin
      SourceDbPageControl.ActivePage:=SourcedBaseSheet;
      SourcedBaseDirLB.Directory:=SourceDatabase;
      SourcedBaseSingleCB.Checked:=FALSE; SourcedBaseSingleCBClick(nil);
   end
   else if (SourceDbMode = DBF_Single) and ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Source)) then
   begin
      SourceDbPageControl.ActivePage:=SourcedBaseSheet;
      SourcedBaseDirLb.Directory:=ExtractFilePath(SourceDatabase);
      SourcedBaseSingleCB.Checked:=TRUE; SourcedBaseSingleCBClick(nil);

      // select the name of the single-database in the list
      for i:=0 to SourcedBaseFileLB.Items.Count-1 do
      begin
         if (SourcedBaseFileLb.Items[i] = ExtractFileName(SourceDatabase)) then
         begin
            SourcedBaseFileLb.ItemIndex:=i;
            break;
         end;
      end;
   end
   else if (SourceDbMode = Access_Single) and ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Source)) then
   begin
      SourceDbPageControl.ActivePage:=SourceAccessSheet;
      SourceDbPageControl.Update;
      SourceAccessDirLb.Directory:=ExtractFilePath(SourceDatabase);
      SourceAccessSingleCb.Checked:=TRUE;  SourceAccessSingleCBClick(nil);
      // select the name of the Access mdb in the list
      for i:=0 to SourceAccessFileLb.Items.Count -1 do
      begin
         if (SourceAccessFileLb.Items[i] = ExtractFileName(SourceDatabase)) then
         begin
            SourceAccessFileLb.ItemIndex:=i;
            break;
         end;
      end;
      SourceAccessSingleCB.visible:=TRUE;
      // fill the table-list
      ADOConnection.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + SourceDatabase;
      ADOConnection.Connected:=true;
      ADOConnection.GetTableNames(SourceAccessTableLB.Items,False);
      ADOConnection.Connected:=false;
      // select the name of the table in the list
      for i:=0 to SourceAccessTableLB.Items.Count-1 do
      begin
         if (SourceAccessTableLB.Items[i] = SourceTable) then
         begin
            SourceAccessTableLB.ItemIndex:=i;
            break;
         end;
      end;
   end
   else if (SourceDbMode = ACCESS_Tables) and ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Source)) then
   begin
      SourceDbPageControl.ActivePage:=SourceAccessSheet;
      SourceDbPageControl.Update;
      SourceAccessDirLb.Directory:=ExtractFilePath(SourceDatabase);
      SourceAccessSingleCb.Checked:=FALSE; SourceAccessSingleCBClick(nil);
      // select the name of the Access mdb in the list
      for i:=0 to SourceAccessFileLb.Items.Count -1 do
      begin
         if (SourceAccessFileLb.Items[i] = ExtractFileName(SourceDatabase)) then
         begin
            SourceAccessFileLb.ItemIndex:=i;
            break;
         end;
      end;
      SourceAccessSingleCB.visible:=TRUE;
      // fill the table-list
      ADOConnection.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + SourceDatabase;
      ADOConnection.Connected:=true;
      ADOConnection.GetTableNames(SourceAccessTableLB.Items,False);
      ADOConnection.Connected:=false;
   end
   else if (SourceDbMode = IDB_Database) and ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Source)) then
   begin
      SourceDbPageControl.ActivePage:=SourceIdbSheet;
      SourceDbPageControl.Update;
      wgifilename:=ExpAmpFilename;
      wgifilename:=ChangeFileExt(wgifilename,'.wgi');
      if (SourceDatabase = wgifilename) and (FileExists(wgifilename)) then // check if wgi-file is same than project name
      begin
         SourceIdbCb.Checked:=FALSE; SourceIdbCbClick(nil);
      end
      else
      begin
         if Fileexists(SourceDatabase) then
         begin
            SourceIdbCb.Checked:=TRUE; SourceIdbCbClick(nil);
            SourceIdbDirLb.Directory:=ExtractFilePath(SourceDatabase);
            // now select the .wgi file in the list
            for i:=0 to SourceAccessFileLb.Items.Count -1 do
            begin
               if (SourceIdbFileLb.Items[i] = ExtractFileName(SourceDatabase)) then
               begin
                  SourceIdbFileLb.ItemIndex:=i;
                  break;
               end;
            end;
         end
         else
         begin
            SourceIdbCb.Checked:=TRUE; SourceIdbCbClick(nil);
         end;
      end;
   end;

   if (DestDbMode = DBF_Directory) and ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Target)) then
   begin
      DestDbPageControl.ActivePage:=DestdBaseSheet;
      DestdBaseDirLB.Directory:=DestDatabase;
      DestdBaseSingleCB.Checked:=FALSE; DestdBaseSingleCBClick(nil);
   end
   else if (DestDbMode = DBF_Single) and ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Target)) then
   begin
      DestDbPageControl.ActivePage:=DestdBaseSheet;
      DestdBaseDirLB.Directory:=ExtractFilePath(DestDatabase);
      DestdBaseSingleCb.Checked:=TRUE; DestdBaseSingleCBClick(nil);
      // first check if the database exists
      if FileExists(DestDatabase) then
      begin
         for i:=0 to DestDBaseFileLb.Items.Count -1 do
         begin
            if (DestDBaseFileLb.Items[i] = ExtractFileName(DestDatabase)) then
            begin
               DestDBaseFileLb.ItemIndex:=i;
               break;
            end;
         end;
      end
      else
      begin
         NewDbfFilename:=DestDatabase;
         DestdBaseSingleLabel.Caption:=ExtractFilename(NewDbfFilename);
      end;
   end
   else if (DestDbMode = Access_Single) and ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Target)) then
   begin
      DestDbPageControl.ActivePage:=DestAccessSheet;
      if not FileExists(DestDatabase) then
      begin
         NewMdbFilename:=DestDatabase; newMdbLabel.Caption:=ExtractFilename(newMdbFilename);
         DestAccessSingleCB.visible:=TRUE;
         DestAccessSingleCB.checked:=TRUE; DestAccessSingleCBClick(nil);
         AccessNewSingleEdit.Text:=DestTable;
         DestAccessTableLB.visible:=FALSE;
      end
      else
      begin
         DestAccessDirLB.Directory:=ExtractFilePath(DestDatabase);
         // select the mdb in the list
         for i:=0 to DestAccessFileLB.Items.Count -1 do
         begin
            if (DestAccessFileLB.Items[i] = ExtractFileName(DestDatabase)) then
            begin
               DestAccessFileLB.ItemIndex:=i;
               break;
            end;
         end;
         DestAccessSingleCB.visible:=TRUE;
         DestAccessSingleCB.checked:=TRUE; DestAccessSingleCBClick(nil);
         DestAccessTableLB.visible:=TRUE;
         // fill the table list
         ADOConnection.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + DestDatabase;
         ADOConnection.Connected:=true;
         ADOConnection.GetTableNames(DestAccessTableLB.Items,False);
         ADOConnection.Connected:=false;
         // select the table in the list
         found:=FALSE;
         for i:=0 to DestAccessTableLB.Items.Count -1 do
         begin
            if DestAccessTableLb.Items[i] = DestTable then
            begin
               DestAccessTableLb.ItemIndex:=i;
               found:=TRUE;
               break;
            end;
         end;
         if not found then
            AccessNewSingleEdit.Text:=DestTable
         else
            AccessNewSingleEdit.Text:='';
      end;
   end
   else if (DestDbMode = Access_Tables) and ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Target)) then
   begin
      DestDbPageControl.ActivePage:=DestAccessSheet;
      DestAccessDirLB.Directory:=ExtractFilePath(DestDatabase);
      if not FileExists(DestDatabase) then
      begin
         NewMdbFilename:=DestDatabase; newMdbLabel.Caption:=ExtractFilename(newMdbFilename);
         DestAccessSingleCB.visible:=TRUE;
         DestAccessSingleCB.checked:=FALSE; DestAccessSingleCBClick(nil);
         AccessNewSingleEdit.Text:='';
      end
      else
      begin
         // select the mdb in the list
         for i:=0 to DestAccessFileLB.Items.Count -1 do
         begin
            if (DestAccessFileLB.Items[i] = ExtractFileName(DestDatabase)) then
            begin
               DestAccessFileLB.ItemIndex:=i;
               break;
            end;
         end;
         DestAccessSingleCB.visible:=TRUE;
         DestAccessSingleCB.checked:=FALSE; DestAccessSingleCBClick(nil);
         // fill the table list
         ADOConnection.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + DestDatabase;
         ADOConnection.Connected:=true;
         ADOConnection.GetTableNames(DestAccessTableLB.Items,False);
         ADOConnection.Connected:=false;
      end;
   end
   else if (DestDbMode = Idb_Database) and ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Target)) then
   begin
      DestDbPageControl.ActivePage:=DestIdbSheet;
      DestDbPageControl.Update;
      wgifilename:=ImpAmpFilename;
      wgifilename:=ChangeFileExt(wgifilename,'.wgi');
      if (wgifilename = DestDatabase) then
      begin
         DestIdbCb.Checked:=FALSE; DestIdbCbClick(nil);
      end
      else
      begin
         DestIdbCb.Checked:=TRUE; DestIdbCbClick(nil);
         DestIdbLabel.Caption:=DestDatabase;
      end;
   end
   else if (DestDbMode = Text_File) and ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Target)) then
   begin
      DestDbPageControl.ActivePage:=DestTextSheet;
      DestDbPageControl.Update;
      DestTextDirLB.Directory:=DestDatabase;
      // now setup the Seperator
      if DestTable = 'SPACE' then
      begin
         ImpSepSpaceCb.Checked:=TRUE;
         ImpSepSpaceCbClick(nil);
      end
      else if DestTable = ';' then
      begin
         ImpSepSemicolonCb.Checked:=TRUE;
         ImpSepSemicolonCbClick(nil);
      end
      else if DestTable = ',' then
      begin
         ImpSepCommaCb.Checked:=TRUE;
         ImpSepCommaCbClick(nil);
      end
      else
      begin
         ImpSepTabCb.Checked:=TRUE;
         ImpSepTabCbClick(nil);
      end;
   end;


   if (ShowMode = Show_Source) then
   begin
      DestDbSheet.TabVisible:=FALSE;
      if not FileExists(WorkingDir + 'Idb.dll') then
      begin
         SourceDBPageControl.ActivePageIndex:=0;
         SourceDBPageControl.Pages[2].TabVisible:=FALSE;
      end;
   end
   else
   if (ShowMode = Show_Target) then
   begin
      SourceDbSheet.TabVisible:=FALSE;
      if not FileExists(WorkingDir + 'Idb.dll') then
      begin
         DestDBPageControl.ActivePageIndex:=0;
         DestDBPageControl.Pages[2].TabVisible:=FALSE;
      end;
   end
   else
   begin
      if not FileExists(WorkingDir + 'Idb.dll') then
      begin
         DestDBPageControl.ActivePageIndex:=0;
         DestDBPageControl.Pages[2].TabVisible:=FALSE;
         SourceDBPageControl.ActivePageIndex:=0;
         SourceDBPageControl.Pages[2].TabVisible:=FALSE;
      end;
   end;
   CheckWindow;
end;

procedure TSetDbWnd.UseDbCbClick(Sender: TObject);
begin
   if UseDbCb.Checked then
   begin
      MainPageControl.Visible:=TRUE;
      if (ShowMode = Show_SourceTarget) or (ShowMode = Show_Source) then
      begin
         MainPageControl.ActivePage:=SourceDbSheet;        // default Page is Source Database Sheet
         SourceDbPageControl.ActivePage:=SourcedBaseSheet; // default Source Page is dBase
      end;
      if (ShowMode = Show_SourceTarget) or (ShowMode = Show_Target) then
         DestDbPageControl.ActivePage:=DestdBaseSheet;     // default Destination Page is dBase
   end
   else
   begin
      MainPageControl.Visible:=FALSE;
   end;
   CheckWindow;
end;

procedure TSetDbWnd.SourcedBaseSingleCBClick(Sender: TObject);
begin
   if SourcedBaseSingleCb.Checked then
      SourcedBaseFileLb.visible:=TRUE
   else
      SourcedBaseFileLb.visible:=FALSE;
   CheckWindow;
end;

procedure TSetDbWnd.SourceAccessSingleCBClick(Sender: TObject);
begin
   if SourceAccessSingleCb.Checked then
      SourceAccessTableLb.visible:=TRUE
   else
      SourceAccessTableLb.visible:=FALSE;
   CheckWindow;
end;

procedure TSetDbWnd.SourceAccessFileLBChange(Sender: TObject);
var i:integer;
    MdbName:string;
begin
   for i:=0 to SourceAccessFileLB.Items.Count - 1 do
   begin
      if SourceAccessFileLB.Selected[i] then
      begin
         MdbName:=SourceAccessDirLB.Directory; if length(MdbName) > 3 then MdbName:=MdbName + '\';
         MdbName:=MdbName+SourceAccessFileLB.Items[i];
         ADOConnection.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + MdbName;
         ADOConnection.Connected:=true;
         ADOConnection.GetTableNames(SourceAccessTableLB.Items,False);
         ADOConnection.Connected:=false;
         SourceAccessSingleCB.visible:=true;
         break;
      end;
   end;
   CheckWindow;
end;

procedure TSetDbWnd.SourceAccessFileLBClick(Sender: TObject);
var i:integer;
    MdbName:string;
begin
   for i:=0 to SourceAccessFileLB.Items.Count - 1 do
   begin
      if SourceAccessFileLB.Selected[i] then
      begin
         MdbName:=SourceAccessDirLB.Directory; if length(MdbName) > 3 then MdbName:=MdbName + '\';
         MdbName:=MdbName+SourceAccessFileLB.Items[i];
         ADOConnection.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + MdbName;
         ADOConnection.Connected:=true;
         ADOConnection.GetTableNames(SourceAccessTableLB.Items,False);
         ADOConnection.Connected:=false;
         SourceAccessSingleCB.visible:=true;
         break;
      end;
   end;
   CheckWindow;
end;

procedure TSetDbWnd.SourcedBaseDirLBChange(Sender: TObject);
begin
   SourcedBaseSingleCB.Checked:=FALSE;
   SourceAccessDirLb.Directory:=SourcedBaseDirLb.Directory;
   SourceIdbDirLb.Directory:=SourcedBaseDirLb.Directory;
   CheckWindow;
end;

procedure TSetDbWnd.SourcedBaseFileLBChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSetDbWnd.SourcedBaseFileLBClick(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSetDbWnd.SourcedBaseDirLBClick(Sender: TObject);
begin
   SourcedBaseSingleCB.Checked:=FALSE;
   SourceAccessDirLb.Directory:=SourcedBaseDirLb.Directory;
   SourceIdbDirLb.Directory:=SourcedBaseDirLb.Directory;
   CheckWindow;
end;

procedure TSetDbWnd.SourceAccessDirLBChange(Sender: TObject);
begin
   SourceAccessSingleCB.Checked:=FALSE;
   SourceAccessSingleCB.Visible:=FALSE;
   SourcedBaseDirLb.Directory:=SourceAccessDirLb.Directory;
   SourceIdbDirLb.Directory:=SourceAccessDirLb.Directory;
   CheckWindow;
end;

procedure TSetDbWnd.SourceAccessTableLBClick(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSetDbWnd.DestdBaseSingleCBClick(Sender: TObject);
begin
   if DestdBaseSingleCb.Checked then
   begin
      DestdBaseFileLB.visible:=TRUE;
      DestdBaseNewSingleBtn.visible:=TRUE;
      DestdBaseSingleLabel.visible:=TRUE;
      DestdBaseSingleLabel.Caption:='';  NewDbfFilename:='';
   end
   else
   begin
      DestdBaseFileLB.visible:=FALSE;
      DestdBaseNewSingleBtn.visible:=FALSE;
      DestdBaseSingleLabel.visible:=FALSE;
      DestdBaseSingleLabel.Caption:=''; NewDbfFilename:='';
   end;
   CheckWindow;
end;

procedure TSetDbWnd.DestdBaseFileLBChange(Sender: TObject);
begin
   DestdBaseSingleLabel.Caption:=''; NewDbfFilename:='';
   CheckWindow;
end;

procedure TSetDbWnd.DestdBaseFileLBClick(Sender: TObject);
begin
   DestdBaseSingleLabel.Caption:=''; NewDbfFilename:='';
   CheckWindow;
end;

procedure TSetDbWnd.DestdBaseNewSingleBtnClick(Sender: TObject);
begin
   if not DoShowModal then
   begin
      if SaveDialogDBase.Execute then
      begin
         NewDbfFilename:=SaveDialogDBase.Filename;
         DestdBaseSingleLabel.Caption:=ExtractFilename(NewDbfFilename);
      end
      else
      begin
         NewDbfFilename:=''; DestdBaseSingleLabel.Caption:='';
      end;
      CheckWindow;
   end
   else
   begin
      Self.Tag:=0; // 0 stands for SaveDialogDbase
      if OpenDialogsModuleWnd <> nil then
         OpenDialogsModuleWnd.Free;
      OpenDialogsModuleWnd:=TOpenDialogsModuleWnd.Create(nil);
      OpenDialogsModuleWnd.ParentHandle:=Handle;
      OpenDialogsModuleWnd.OpenMode:=file_save;
      OpenDialogsModuleWnd.OnClose:=OnOpenDialogsModuleWndClose;
      // setup the filter for the open-dialog
      OpenDialogsModuleWnd.SaveDialog.DefaultExt:='*.dbf';
      OpenDialogsModuleWnd.SaveDialog.Filename:='*.dbf';
      OpenDialogsModuleWnd.SaveDialog.Filter:='dBase File|*.dbf';
      OpenDialogsModuleWnd.Show;
   end;
end;

procedure TSetDbWnd.DestAccessFileLBChange(Sender: TObject);
var i:integer;
    MdbName:string;
begin
   for i:=0 to DestAccessFileLB.Items.Count - 1 do
   begin
      if DestAccessFileLB.Selected[i] then
      begin
         MdbName:=DestAccessDirLB.Directory; if length(MdbName) > 3 then MdbName:=MdbName + '\';
         MdbName:=MdbName+DestAccessFileLB.Items[i];
         ADOConnection.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + MdbName;
         ADOConnection.Connected:=true;
         ADOConnection.GetTableNames(DestAccessTableLB.Items,False);
         ADOConnection.Connected:=false;
         DestAccessSingleCB.visible:=TRUE;
         DestAccessSingleCB.visible:=true;
         break;
      end;
   end;
   CheckWindow;
end;

procedure TSetDbWnd.DestAccessFileLBClick(Sender: TObject);
var i:integer;
    MdbName:string;
begin
   for i:=0 to DestAccessFileLB.Items.Count - 1 do
   begin
      if DestAccessFileLB.Selected[i] then
      begin
         MdbName:=DestAccessDirLB.Directory; if length(MdbName) > 3 then MdbName:=MdbName + '\';
         MdbName:=MdbName+DestAccessFileLB.Items[i];
         ADOConnection.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + MdbName;
         ADOConnection.Connected:=true;
         ADOConnection.GetTableNames(DestAccessTableLB.Items,False);
         ADOConnection.Connected:=false;
         DestAccessSingleCB.visible:=true;
         break;
      end;
   end;
   CheckWindow;
end;

procedure TSetDbWnd.NewMdbBtnClick(Sender: TObject);
begin
   if not DoShowModal then
   begin
      if SaveDialogAccess.Execute then
      begin
         NewMdbFilename:=SaveDialogAccess.FileName; newMdbLabel.Caption:=ExtractFilename(NewMdbFilename);
         if not FileExists(SaveDialogAccess.FileName) then
         begin
            DestAccessSingleCB.Checked:=FALSE;
            DestAccessSingleCB.Visible:=TRUE;
            AccessNewSingleEdit.Text:='';
         end
         else
         begin
            DestAccessSingleCB.Checked:=FALSE;
            DestAccessSingleCB.Visible:=TRUE;
            ADOConnection.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + SaveDialogAccess.FileName;
            ADOConnection.Connected:=true;
            ADOConnection.GetTableNames(DestAccessTableLB.Items,False);
            ADOConnection.Connected:=false;
            AccessNewSingleEdit.Text:='';
         end;
      end;
      CheckWindow;
   end
   else
   begin
      Self.Tag:=1; // 1 stands for SaveDialog Access
      if OpenDialogsModuleWnd <> nil then
         OpenDialogsModuleWnd.Free;
      OpenDialogsModuleWnd:=TOpenDialogsModuleWnd.Create(nil);
      OpenDialogsModuleWnd.ParentHandle:=Handle;
      OpenDialogsModuleWnd.OpenMode:=file_save;
      OpenDialogsModuleWnd.OnClose:=OnOpenDialogsModuleWndClose;
      // setup the filter for the open-dialog
      OpenDialogsModuleWnd.SaveDialog.DefaultExt:='*.mdb';
      OpenDialogsModuleWnd.SaveDialog.Filename:='*.mdb';
      OpenDialogsModuleWnd.SaveDialog.Filter:='Access database|*.mdb';
      OpenDialogsModuleWnd.Show;
   end;
end;

procedure TSetDbWnd.DestAccessSingleCBClick(Sender: TObject);
begin
   if DestAccessSingleCb.Checked then
   begin
      AccessNewSingleEdit.visible:=TRUE;
      AccessNewSingleEdit.Text:='';
      // check if the database exists
      if NewMdbFilename <> '' then
      begin
         if FileExists(NewMdbFilename) then
            DestAccessTableLb.visible:=TRUE
         else
            DestAccessTableLb.visible:=FALSE;
      end
      else
         DestAccessTableLb.visible:=TRUE;
   end
   else
   begin
      AccessNewSingleEdit.visible:=FALSE;
      AccessNewSingleEdit.Text:='';
      DestAccessTableLb.visible:=FALSE;
   end;
   CheckWindow;
end;

procedure TSetDbWnd.AccessNewSingleEditChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSetDbWnd.AccessNewSingleEditClick(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSetDbWnd.DestAccessTableLBClick(Sender: TObject);
begin
   AccessNewSingleEdit.Text:='';
   CheckWindow;
end;

procedure TSetDbWnd.OKBtnClick(Sender: TObject);
var i:integer;
begin
   // read the database-settings out of the Dialog
   if not UseDbCb.Checked then
   begin
      // reset source and destination database
      if (ShowMode = Show_SourceTarget) or (ShowMode = Show_Source) then
      begin
         SourceDbMode:=Db_None;
         SourceDatabase:='';
         SourceTable:='';
      end;
      if (ShowMode = Show_SourceTarget) or (ShowMode = Show_Target) then
      begin
         DestDbMode:=Db_None;
         DestDatabase:='';
         DestTable:='';
      end;
   end
   else
   begin
      // read the Source-Database Settings
      if (SourceDbPageControl.ActivePage = SourcedBaseSheet) and
         ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Source)) then
      begin
         SourceDatabase:=SourcedBaseDirLB.Directory;
         SourceTable:='';
         if SourcedBaseSingleCB.Checked then
         begin
            // a dBase single database has been set
            SourceDbMode:=DBF_Single;
            // get the name of the database
            if length(SourceDatabase) > 3 then SourceDatabase:=SourceDatabase + '\';
            for i:=0 to SourcedBaseFileLB.Items.Count - 1 do
            begin
               if SourcedBaseFileLB.Selected[i] then
               begin
                  SourceDatabase:=SourceDatabase+SourcedBaseFileLB.Items[i];
                  break;
               end;
            end;
         end
         else
         begin
            SourceDbMode:=DBF_Directory;
         end;
      end
      else if (SourceDbPageControl.ActivePage = SourceAccessSheet) and
              ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Source)) then
      begin
         // get the name of the .mdb file
         SourceDatabase:=SourceAccessDirLB.Directory;
         if length(SourceDatabase) > 3 then SourceDatabase:=SourceDatabase + '\';
         for i:=0 to SourceAccessFileLB.Items.Count - 1 do
         begin
            if SourceAccessFileLB.Selected[i] then
            begin
               SourceDatabase:=SourceDatabase+SourceAccessFileLB.Items[i];
               break;
            end;
         end;
         if SourceAccessSingleCB.Checked then
         begin
            SourceDbMode:=ACCESS_Single;
            // get the name of the table
            for i:=0 to SourceAccessTableLB.Items.Count - 1 do
            begin
               if SourceAccessTableLB.Selected[i] then
               begin
                  SourceTable:=SourceAccessTableLB.Items[i];
                  break;
               end;
            end;
         end
         else
         begin
            SourceDbMode:=ACCESS_Tables;
            SourceTable:='';
         end;
      end
      else if (SourceDbPageControl.ActivePage = SourceIdbSheet) and
              ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Source)) then
      begin
         if SourceIdbCb.Checked then
         begin
            // get name of wgi file
            SourceDatabase:=SourceIdbDirLB.Directory;
            if length(SourceDatabase) > 3 then SourceDatabase:=SourceDatabase + '\';
            for i:=0 to SourceIdbFileLB.Items.Count - 1 do
            begin
               if SourceIdbFileLB.Selected[i] then
               begin
                  SourceDatabase:=SourceDatabase+SourceIdbFileLB.Items[i];
                  break;
               end;
            end;
         end
         else
         begin
            SourceDatabase:=ExpAmpFilename;
            if ExpAmpFilename <> '' then
               SourceDatabase:=ChangeFileExt(SourceDatabase,'.wgi');
         end;
         SourceDbMode:=Idb_Database;
         SourceTable:='';
      end;

      // read the destination database settings
      if (DestDbPageControl.ActivePage = DestdBaseSheet) and
         ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Target)) then
      begin
         DestTable:='';
         if DestdBaseSingleCB.Checked then
         begin
            // an dBase-Single has been set
            DestDbMode:=DBF_Single;
            if NewDbfFilename <> '' then
               DestDatabase:=NewDbfFilename
            else
            begin
               // get the tablename out of the list
               DestDatabase:=DestdBaseDirLB.Directory;
               if length(DestDatabase) > 3 then DestDatabase:=DestDatabase + '\';
               for i:=0 to DestdBaseFileLB.Items.Count - 1 do
               begin
                  if DestdBaseFileLB.Selected[i] then
                  begin
                     DestDatabase:=DestDatabase+DestdBaseFileLB.Items[i];
                     break;
                  end;
               end;
            end;
         end
         else
         begin
            DestDbMode:=DBF_Directory;
            DestDatabase:=DestdBaseDirLB.Directory;
         end;
      end
      else if (DestDbPageControl.ActivePage = DestAccessSheet) and
              ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Target)) then
      begin
         // get the name of the .mdb file
         if NewMdbFilename = '' then
         begin
            DestDatabase:=DestAccessDirLB.Directory;
            if length(DestDatabase) > 3 then DestDatabase:=DestDatabase + '\';
            for i:=0 to DestAccessFileLB.Items.Count-1 do
            begin
               if DestAccessFileLB.Selected[i] then
               begin
                  DestDatabase:=DestDatabase+DestAccessFileLB.Items[i];
                  break;
               end;
            end;
         end
         else
            DestDatabase:=NewMdbFilename;
         if DestAccessSingleCB.Checked then
         begin
            DestDbMode:=Access_Single;
            // get the name of the Table
            if AccessNewSingleEdit.Text = '' then
            begin
               for i:=0 to DestAccessTableLB.Items.Count -1 do
               begin
                  if DestAccessTableLB.Selected[i] then
                  begin
                     DestTable:=DestAccessTableLB.Items[i];
                     break;
                  end;
               end;
            end
            else
               DestTable:=AccessNewSingleEdit.Text;
         end
         else
         begin
            DestDbMode:=Access_Tables;
            DestTable:='';
         end;
      end
      else if (DestDbPageControl.ActivePage = DestIdbSheet) and
              ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Target)) then
      begin
         if DestIdbCb.Checked then
         begin
            DestDatabase:=DestIdbLabel.Caption;
         end
         else
         begin
            DestDatabase:=ImpAmpFilename;
            if ImpAmpFilename <> '' then
               DestDatabase:=ChangeFileExt(DestDatabase,'.wgi');
         end;
         DestDbMode:=Idb_Database;
         DestTable:='';
      end
      else if (DestDbPageControl.ActivePage = DestTextSheet) and
              ((ShowMode = Show_SourceTarget) or (ShowMode = Show_Target)) then
      begin
         DestDatabase:=DestTextDirLB.Directory;
         DestDbMode:=Text_File;
         // get the seperator
         if ImpSepTabCb.checked then
            DestTable:='TAB'
         else if ImpSepCommaCb.Checked then
            DestTable:=','
         else if ImpSepSpaceCb.Checked then
            DestTable:='SPACE'
         else if ImpSepSemicolonCb.Checked then
            DestTable:=';';
      end;
   end;
   Self.Close;
end;

procedure TSetDbWnd.DestDbPageControlChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSetDbWnd.SourceDbPageControlChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSetDbWnd.DestdBaseDirLBChange(Sender: TObject);
begin
   DestdBaseSingleCB.checked:=FALSE; DestdBaseSingleCBClick(nil);
   DestAccessDirLb.Directory:=DestdBaseDirLb.Directory; DestTextDirLb.Directory:=DestdBaseDirLb.Directory;
   CheckWindow;
end;

procedure TSetDbWnd.DestdBaseDirLBClick(Sender: TObject);
begin
   DestdBaseSingleCB.checked:=FALSE; DestdBaseSingleCBClick(nil);
   DestAccessDirLb.Directory:=DestdBaseDirLb.Directory; DestTextDirLb.Directory:=DestdBaseDirLb.Directory;
   CheckWindow;
end;

procedure TSetDbWnd.SetupWindowTimerTimer(Sender: TObject);
begin
   windows.setparent (Self.Handle, ParentWinHandle);
   SetWindowPos(Handle,HWND_TOP,winleft,wintop,winwidth, winheight,SWP_SHOWWINDOW);
   Self.Height:=winheight;
   Self.Width:=winwidth;
   Self.Repaint;
   SetupWindowTimer.Enabled:=false;
end;

procedure TSetDbWnd.CancelBtnClick(Sender: TObject);
begin
   Self.Close;
end;

procedure TSetDbWnd.SourceAccessDirLBClick(Sender: TObject);
begin
   SourceAccessSingleCB.Checked:=FALSE;
   SourceAccessSingleCB.Visible:=FALSE;
   SourcedBaseDirLb.Directory:=SourceAccessDirLb.Directory;
   SourceIdbDirLb.Directory:=SourceAccessDirLb.Directory;
   CheckWindow;
end;

procedure TSetDbWnd.DestAccessDirLBChange(Sender: TObject);
begin
   DestAccessSingleCB.Checked:=FALSE; DestAccessSingleCBClick(nil);
   DestdBaseDirLb.Directory:=DestAccessDirLb.Directory; DestTextDirLb.Directory:=DestAccessDirLb.Directory;
   newMdbLabel.Caption:='';
   CheckWindow;
end;

procedure TSetDbWnd.DestAccessDirLBClick(Sender: TObject);
begin
   DestAccessSingleCB.Checked:=FALSE; DestAccessSingleCBClick(nil);
   DestdBaseDirLb.Directory:=DestAccessDirLb.Directory; DestTextDirLb.Directory:=DestAccessDirLb.Directory;
   newMdbLabel.Caption:='';
   CheckWindow;
end;

procedure TSetDbWnd.SourceIdbCbClick(Sender: TObject);
begin
   if SourceIdbCb.Checked then
   begin
      SourceIdbPanel.Visible:=TRUE;
   end
   else
   begin
     SourceIdbPanel.Visible:=FALSE;
   end;
   CheckWindow;
end;

procedure TSetDbWnd.DestIdbCbClick(Sender: TObject);
begin
   if DestIdbCb.Checked then
   begin
      DestIdbPanel.Visible:=TRUE;
      DestIdbLabel.Caption:='';
   end
   else
   begin
      DestIdbPanel.Visible:=FALSE;
      DestIdbLabel.Caption:='';
   end;
   CheckWindow;
end;

procedure TSetDbWnd.DestIdbBtnClick(Sender: TObject);
begin
   if not DoShowModal then
   begin
      if SaveDialogWgi.Execute then
      begin
         DestIdbLabel.Caption:=SaveDialogWgi.Filename;
      end;
      CheckWindow;
   end
   else
   begin
      Self.Tag:=2; // 1 stands for SaveDialog Wgi
      if OpenDialogsModuleWnd <> nil then
         OpenDialogsModuleWnd.Free;
      OpenDialogsModuleWnd:=TOpenDialogsModuleWnd.Create(nil);
      OpenDialogsModuleWnd.ParentHandle:=Handle;
      OpenDialogsModuleWnd.OpenMode:=file_save;
      OpenDialogsModuleWnd.OnClose:=OnOpenDialogsModuleWndClose;
      // setup the filter for the open-dialog
      OpenDialogsModuleWnd.SaveDialog.DefaultExt:='*.wgi';
      OpenDialogsModuleWnd.SaveDialog.Filename:='*.wgi';
      OpenDialogsModuleWnd.SaveDialog.Filter:='Idb database|*.wgi';
      OpenDialogsModuleWnd.Show;
   end;
end;

procedure TSetDbWnd.SourceIdbDirLbChange(Sender: TObject);
begin
   SourcedBaseDirLb.Directory:=SourceIdbDirLb.Directory;
   SourceAccessDirLb.Directory:=SourceIdbDirLb.Directory;
   CheckWindow;
end;

procedure TSetDbWnd.SourceIdbDirLbClick(Sender: TObject);
begin
   SourcedBaseDirLb.Directory:=SourceIdbDirLb.Directory;
   SourceAccessDirLb.Directory:=SourceIdbDirLb.Directory;
   CheckWindow;
end;

procedure TSetDbWnd.SourceIdbFileLbChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSetDbWnd.SourceIdbFileLbClick(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSetDbWnd.OnOpenDialogsModuleWndClose(Sender: TObject;  var Action: TCloseAction);
begin
   if Self.Tag=0 then // callback for SaveDialogDBase
   begin
      if OpenDialogsModuleWnd.Filename <> '' then
      begin
         // a file has been set
         NewDbfFilename:=OpenDialogsModuleWnd.Filename;
         DestdBaseSingleLabel.Caption:=ExtractFilename(NewDbfFilename);
      end
      else
      begin
         NewDbfFilename:=''; DestdBaseSingleLabel.Caption:='';
      end;
      CheckWindow;
   end
   else if Self.Tag = 1 then
   begin
      if OpenDialogsModuleWnd.Filename <> '' then
      begin
         NewMdbFilename:=OpenDialogsModuleWnd.FileName; newMdbLabel.Caption:=ExtractFilename(NewMdbFilename);
         if not FileExists(NewMdbFilename) then
         begin
            DestAccessSingleCB.Checked:=FALSE;
            DestAccessSingleCB.Visible:=TRUE;
            AccessNewSingleEdit.Text:='';
         end
         else
         begin
            DestAccessSingleCB.Checked:=FALSE;
            DestAccessSingleCB.Visible:=TRUE;
            ADOConnection.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + SaveDialogAccess.FileName;
            ADOConnection.Connected:=true;
            ADOConnection.GetTableNames(DestAccessTableLB.Items,False);
            ADOConnection.Connected:=false;
            AccessNewSingleEdit.Text:='';
         end;
      end;
      CheckWindow;
   end
   else if Self.Tag = 2 then
   begin
      if OpenDialogsModuleWnd.Filename <> '' then
      begin
         DestIdbLabel.Caption:=OpenDialogsModuleWnd.Filename;
      end;
      CheckWindow;
   end;
end;

procedure TSetDbWnd.FormDestroy(Sender: TObject);
begin
   if OpenDialogsModuleWnd <> nil then
      OpenDialogsModuleWnd.Destroy;
end;

procedure TSetDbWnd.DestTextDirLBChange(Sender: TObject);
begin
   DestdBaseDirLb.Directory:=DestTextDirLb.Directory; DestAccessDirLb.Directory:=DestTextDirLb.Directory;
   CheckWindow;
end;

procedure TSetDbWnd.DestTextDirLBClick(Sender: TObject);
begin
   DestdBaseDirLb.Directory:=DestTextDirLb.Directory; DestAccessDirLb.Directory:=DestTextDirLb.Directory;
   CheckWindow;
end;

procedure TSetDbWnd.ImpSepTabCbClick(Sender: TObject);
begin
   // disable other seperators
   ImpSepCommaCb.Checked:=false;
   ImpSepSpaceCb.Checked:=false;
   ImpSepSemicolonCb.Checked:=false;
   CheckWindow;
end;

procedure TSetDbWnd.ImpSepCommaCbClick(Sender: TObject);
begin
   // disable other seperators
   ImpSepTabCb.Checked:=false;
   ImpSepSpaceCb.Checked:=false;
   ImpSepSemicolonCb.Checked:=false;
   CheckWindow;
end;

procedure TSetDbWnd.ImpSepSpaceCbClick(Sender: TObject);
begin
   // disable other seperators
   ImpSepCommaCb.Checked:=false;
   ImpSepTabCb.Checked:=false;
   ImpSepSemicolonCb.Checked:=false;
   CheckWindow;
end;

procedure TSetDbWnd.ImpSepSemicolonCbClick(Sender: TObject);
begin
   // disable other seperators
   ImpSepCommaCb.Checked:=false;
   ImpSepSpaceCb.Checked:=false;
   ImpSepTabCb.Checked:=false;
   CheckWindow;
end;

end.
