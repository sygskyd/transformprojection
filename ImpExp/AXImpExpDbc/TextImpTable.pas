unit TextImpTable;

interface

uses SysUtils;

type
TTextImpTable=class
   private
      // private variables
      TableFile :TextFile;
      fileopened:boolean;
      CurrentLine:string;

      // variables for properties
      FFilename:string;
      FDelimeter:string;
      // methods to support properties
      function GetFilename:string;
      procedure SetFilename(value:string);
      function GetDelimeter:string;
      procedure SetDelimeter(value:string);
   public
      constructor Create;
      destructor Destroy;
      property Filename:string read GetFilename write SetFilename;
      property Delimeter:string read GetDelimeter write SetDelimeter;
      procedure WriteData(const Data:string);
      procedure CloseLine;
end;

implementation

// constructor
constructor TTextImpTable.Create;
begin
   fileopened:=FALSE;
   CurrentLine:='';
   FFilename:='';
   FDelimeter:='';
end;

// destructor
destructor TTextImpTable.Destroy;
begin
   if (fileopened) then
      CloseFile(TableFile);
end;

function TTextImpTable.GetFilename:string;
begin
   result:=FFilename;
end;

procedure TTextImpTable.SetFilename(value:string);
begin
   FFilename:=value;
end;

function TTextImpTable.GetDelimeter:string;
var retVal:string;
begin
   if FDelimeter = ' ' then
      retVal:='SPACE'
   else if FDelimeter = chr(9) then
      retVal:='TAB'
   else
      retVal:=FDelimeter;
   result:=retVal;
end;

procedure TTextImpTable.SetDelimeter(value:string);
begin
   if value = 'SPACE' then
      FDelimeter:=' '
   else if value = 'TAB' then
      FDelimeter:=chr(9)
   else
      FDelimeter:=value;
end;

procedure TTextImpTable.WriteData(const Data:string);
begin
   if CurrentLine <> '' then
      CurrentLine:=CurrentLine+FDelimeter;
   CurrentLine:=CurrentLine+Data;
end;

procedure TTextImpTable.CloseLine;
begin
   if not fileopened then
   begin
      AssignFile(TableFile, FFilename);
      Rewrite(TableFile);
      fileopened:=TRUE;
   end;
   writeln(TableFile, CurrentLine);
   CurrentLine:=''; // reset current line
end;

end.
