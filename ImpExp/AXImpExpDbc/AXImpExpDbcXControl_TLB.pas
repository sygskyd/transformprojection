unit AXImpExpDbcXControl_TLB;

// ************************************************************************ //
// WARNING                                                                  //
// -------                                                                  //
// The types declared in this file were generated from data read from a     //
// Type Library. If this type library is explicitly or indirectly (via      //
// another type library referring to this type library) re-imported, or the //
// 'Refresh' command of the Type Library Editor activated while editing the //
// Type Library, the contents of this file will be regenerated and all      //
// manual modifications will be lost.                                       //
// ************************************************************************ //

// PASTLWTR : $Revision:   1.11.1.75  $
// File generated on 07.02.2019 18:59:23 from Type Library described below.

// ************************************************************************ //
// Type Lib: D:\Cadmensky\Projects\Progis\ImpExp\AXImpExpDbc\AXGisDbc.tlb
// IID\LCID: {87E05AE6-E34A-11D5-9D07-00C0F057241A}\0
// Helpfile: 
// HelpString: AXImpExpDbc Library
// Version:    1.0
// ************************************************************************ //

interface

uses Windows, ActiveX, Classes, Graphics, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:      //
//   Type Libraries     : LIBID_xxxx                                    //
//   CoClasses          : CLASS_xxxx                                    //
//   DISPInterfaces     : DIID_xxxx                                     //
//   Non-DISP interfaces: IID_xxxx                                      //
// *********************************************************************//
const
  LIBID_AXImpExpDbcXControl: TGUID = '{87E05AE6-E34A-11D5-9D07-00C0F057241A}';
  IID_IAXImpExpDbc: TGUID = '{87E05AE7-E34A-11D5-9D07-00C0F057241A}';
  DIID_IAXImpExpDbcEvents: TGUID = '{87E05AE9-E34A-11D5-9D07-00C0F057241A}';
  CLASS_AXImpExpDbc: TGUID = '{87E05AEB-E34A-11D5-9D07-00C0F057241A}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                  //
// *********************************************************************//
// TxAlignment constants
type
  TxAlignment = TOleEnum;
const
  taLeftJustify = $00000000;
  taRightJustify = $00000001;
  taCenter = $00000002;

// TxBevelCut constants
type
  TxBevelCut = TOleEnum;
const
  bvNone = $00000000;
  bvLowered = $00000001;
  bvRaised = $00000002;
  bvSpace = $00000003;

// TxBorderStyle constants
type
  TxBorderStyle = TOleEnum;
const
  bsNone = $00000000;
  bsSingle = $00000001;

// TxDragMode constants
type
  TxDragMode = TOleEnum;
const
  dmManual = $00000000;
  dmAutomatic = $00000001;

// TxMouseButton constants
type
  TxMouseButton = TOleEnum;
const
  mbLeft = $00000000;
  mbRight = $00000001;
  mbMiddle = $00000002;

// TDataBaseTypes constants
type
  TDataBaseTypes = TOleEnum;
const
  Db_None = $00000000;
  Dbf_Single = $00000001;
  Dbf_Directory = $00000002;
  Access_Single = $00000003;
  Access_Tables = $00000004;
  Idb_Database = $00000005;
  Dbf_Single_Raw = $00000006;
  Dbf_Directory_Raw = $00000007;
  Text_File = $00000008;

// TDataBaseColTypes constants
type
  TDataBaseColTypes = TOleEnum;
const
  Col_Int = $00000000;
  Col_SmallInt = $00000001;
  Col_Float = $00000002;
  Col_Date = $00000003;
  Col_String = $00000004;
  Auto_Inc = $00000005;

// TDatabaseShowmodes constants
type
  TDatabaseShowmodes = TOleEnum;
const
  Shw_SourceTarget = $00000000;
  Shw_Source = $00000001;
  Shw_Target = $00000002;

type

// *********************************************************************//
// Forward declaration of interfaces defined in Type Library            //
// *********************************************************************//
  IAXImpExpDbc = interface;
  IAXImpExpDbcDisp = dispinterface;
  IAXImpExpDbcEvents = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                     //
// (NOTE: Here we map each CoClass to its Default Interface)            //
// *********************************************************************//
  AXImpExpDbc = IAXImpExpDbc;


// *********************************************************************//
// Declaration of structures, unions and aliases.                       //
// *********************************************************************//
  PPUserType1 = ^IFontDisp; {*}


// *********************************************************************//
// Interface: IAXImpExpDbc
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {87E05AE7-E34A-11D5-9D07-00C0F057241A}
// *********************************************************************//
  IAXImpExpDbc = interface(IDispatch)
    ['{87E05AE7-E34A-11D5-9D07-00C0F057241A}']
    function Get_Alignment: TxAlignment; safecall;
    procedure Set_Alignment(Value: TxAlignment); safecall;
    function Get_AutoSize: WordBool; safecall;
    procedure Set_AutoSize(Value: WordBool); safecall;
    function Get_BevelInner: TxBevelCut; safecall;
    procedure Set_BevelInner(Value: TxBevelCut); safecall;
    function Get_BevelOuter: TxBevelCut; safecall;
    procedure Set_BevelOuter(Value: TxBevelCut); safecall;
    function Get_BorderStyle: TxBorderStyle; safecall;
    procedure Set_BorderStyle(Value: TxBorderStyle); safecall;
    function Get_Caption: WideString; safecall;
    procedure Set_Caption(const Value: WideString); safecall;
    function Get_Color: OLE_COLOR; safecall;
    procedure Set_Color(Value: OLE_COLOR); safecall;
    function Get_Ctl3D: WordBool; safecall;
    procedure Set_Ctl3D(Value: WordBool); safecall;
    function Get_UseDockManager: WordBool; safecall;
    procedure Set_UseDockManager(Value: WordBool); safecall;
    function Get_DockSite: WordBool; safecall;
    procedure Set_DockSite(Value: WordBool); safecall;
    function Get_DragCursor: Smallint; safecall;
    procedure Set_DragCursor(Value: Smallint); safecall;
    function Get_DragMode: TxDragMode; safecall;
    procedure Set_DragMode(Value: TxDragMode); safecall;
    function Get_Enabled: WordBool; safecall;
    procedure Set_Enabled(Value: WordBool); safecall;
    function Get_FullRepaint: WordBool; safecall;
    procedure Set_FullRepaint(Value: WordBool); safecall;
    function Get_Font: IFontDisp; safecall;
    procedure _Set_Font(const Value: IFontDisp); safecall;
    procedure Set_Font(var Value: IFontDisp); safecall;
    function Get_Locked: WordBool; safecall;
    procedure Set_Locked(Value: WordBool); safecall;
    function Get_ParentColor: WordBool; safecall;
    procedure Set_ParentColor(Value: WordBool); safecall;
    function Get_ParentCtl3D: WordBool; safecall;
    procedure Set_ParentCtl3D(Value: WordBool); safecall;
    function Get_Visible: WordBool; safecall;
    procedure Set_Visible(Value: WordBool); safecall;
    function Get_DoubleBuffered: WordBool; safecall;
    procedure Set_DoubleBuffered(Value: WordBool); safecall;
    function Get_VisibleDockClientCount: Integer; safecall;
    function DrawTextBiDiModeFlagsReadingOnly: Integer; safecall;
    procedure InitiateAction; safecall;
    function IsRightToLeft: WordBool; safecall;
    function UseRightToLeftReading: WordBool; safecall;
    function UseRightToLeftScrollBar: WordBool; safecall;
    function Get_Cursor: Smallint; safecall;
    procedure Set_Cursor(Value: Smallint); safecall;
    function Get_WorkingDir: WideString; safecall;
    procedure Set_WorkingDir(const Value: WideString); safecall;
    function Get_LngCode: WideString; safecall;
    procedure Set_LngCode(const Value: WideString); safecall;
    function Get_ImpDatabase: WideString; safecall;
    procedure Set_ImpDatabase(const Value: WideString); safecall;
    function Get_ImpTable: WideString; safecall;
    procedure Set_ImpTable(const Value: WideString); safecall;
    function Get_ImpDbMode: TDataBaseTypes; safecall;
    procedure Set_ImpDbMode(Value: TDataBaseTypes); safecall;
    function Get_ExpDatabase: WideString; safecall;
    procedure Set_ExpDatabase(const Value: WideString); safecall;
    function Get_ExpTable: WideString; safecall;
    procedure Set_ExpTable(const Value: WideString); safecall;
    function Get_ExpDbMode: TDataBaseTypes; safecall;
    procedure Set_ExpDbMode(Value: TDataBaseTypes); safecall;
    function Get_IgnoreNonExistExpTables: WordBool; safecall;
    procedure Set_IgnoreNonExistExpTables(Value: WordBool); safecall;
    function GetExportColumn(const Layername: WideString; out Columnname: WideString; 
                             out Columntype: TDataBaseColTypes; out ColumnLength: Integer): WordBool; safecall;
    function GetExportData(const Layername: WideString; out Columnname: WideString; 
                           out Info: WideString): WordBool; safecall;
    function DoExportData(const Layername: WideString; Id: Integer): WordBool; safecall;
    procedure OpenExpTable(const Layername: WideString); safecall;
    procedure CloseExpTables; safecall;
    procedure CloseImpTables; safecall;
    procedure AddImpInfo(const Layername: WideString; const Columnname: WideString; 
                         const Info: WideString); safecall;
    procedure AddImpIdInfo(const Layername: WideString; Id: Integer); safecall;
    procedure OpenImpTable(const Layername: WideString); safecall;
    procedure CheckAndCreateImpTable(const Layername: WideString); safecall;
    procedure CheckAndCreateExpTable(const Layername: WideString); safecall;
    procedure AddImpIdColumn(const Layername: WideString); safecall;
    procedure AddImpColumn(const Layername: WideString; const Columnname: WideString; 
                           Columntype: TDataBaseColTypes; ColumnLength: Integer); safecall;
    procedure CreateImpTable(const Layername: WideString); safecall;
    procedure DeleteExpDBEntry(const Layername: WideString; Id: Integer); safecall;
    procedure DeleteExpTable(const Layername: WideString); safecall;
    procedure SetupByDialog(ParentWinHandle: Integer; WinLeft: Integer; WinTop: Integer); safecall;
    function Get_ShowMode: TDatabaseShowmodes; safecall;
    procedure Set_ShowMode(Value: TDatabaseShowmodes); safecall;
    function Get_ImpDbInfo: WideString; safecall;
    function Get_ExpDbInfo: WideString; safecall;
    procedure DoEditImpInfo(const Layername: WideString; Id: Integer); safecall;
    function Get_AllowModifyImpData: WordBool; safecall;
    procedure Set_AllowModifyImpData(Value: WordBool); safecall;
    function DoExportRawData(const Layername: WideString): WordBool; safecall;
    function DoExportFromImpTable(const Layername: WideString; Id: Integer): WordBool; safecall;
    function GetExportDataFromImpTable(const Layername: WideString; out Columnname: WideString; 
                                       out Info: WideString): WordBool; safecall;
    function Get_CreateIndexOnClose: WordBool; safecall;
    procedure Set_CreateIndexOnClose(Value: WordBool); safecall;
    function GetImpNumRecords(const Layername: WideString): Integer; safecall;
    function GetExpNumRecords(const Layername: WideString): Integer; safecall;
    procedure SetupByModalDialog; safecall;
    function Get_RemoveEmptyColumnsOnClose: WordBool; safecall;
    procedure Set_RemoveEmptyColumnsOnClose(Value: WordBool); safecall;
    function Get_TmpDBaseDir: WideString; safecall;
    procedure Set_TmpDBaseDir(const Value: WideString); safecall;
    procedure EmptyExpTable(const Layername: WideString); safecall;
    function Get_RefreshThreshold: Integer; safecall;
    procedure Set_RefreshThreshold(Value: Integer); safecall;
    property Alignment: TxAlignment read Get_Alignment write Set_Alignment;
    property AutoSize: WordBool read Get_AutoSize write Set_AutoSize;
    property BevelInner: TxBevelCut read Get_BevelInner write Set_BevelInner;
    property BevelOuter: TxBevelCut read Get_BevelOuter write Set_BevelOuter;
    property BorderStyle: TxBorderStyle read Get_BorderStyle write Set_BorderStyle;
    property Caption: WideString read Get_Caption write Set_Caption;
    property Color: OLE_COLOR read Get_Color write Set_Color;
    property Ctl3D: WordBool read Get_Ctl3D write Set_Ctl3D;
    property UseDockManager: WordBool read Get_UseDockManager write Set_UseDockManager;
    property DockSite: WordBool read Get_DockSite write Set_DockSite;
    property DragCursor: Smallint read Get_DragCursor write Set_DragCursor;
    property DragMode: TxDragMode read Get_DragMode write Set_DragMode;
    property Enabled: WordBool read Get_Enabled write Set_Enabled;
    property FullRepaint: WordBool read Get_FullRepaint write Set_FullRepaint;
    property Font: IFontDisp read Get_Font write _Set_Font;
    property Locked: WordBool read Get_Locked write Set_Locked;
    property ParentColor: WordBool read Get_ParentColor write Set_ParentColor;
    property ParentCtl3D: WordBool read Get_ParentCtl3D write Set_ParentCtl3D;
    property Visible: WordBool read Get_Visible write Set_Visible;
    property DoubleBuffered: WordBool read Get_DoubleBuffered write Set_DoubleBuffered;
    property VisibleDockClientCount: Integer read Get_VisibleDockClientCount;
    property Cursor: Smallint read Get_Cursor write Set_Cursor;
    property WorkingDir: WideString read Get_WorkingDir write Set_WorkingDir;
    property LngCode: WideString read Get_LngCode write Set_LngCode;
    property ImpDatabase: WideString read Get_ImpDatabase write Set_ImpDatabase;
    property ImpTable: WideString read Get_ImpTable write Set_ImpTable;
    property ImpDbMode: TDataBaseTypes read Get_ImpDbMode write Set_ImpDbMode;
    property ExpDatabase: WideString read Get_ExpDatabase write Set_ExpDatabase;
    property ExpTable: WideString read Get_ExpTable write Set_ExpTable;
    property ExpDbMode: TDataBaseTypes read Get_ExpDbMode write Set_ExpDbMode;
    property IgnoreNonExistExpTables: WordBool read Get_IgnoreNonExistExpTables write Set_IgnoreNonExistExpTables;
    property ShowMode: TDatabaseShowmodes read Get_ShowMode write Set_ShowMode;
    property ImpDbInfo: WideString read Get_ImpDbInfo;
    property ExpDbInfo: WideString read Get_ExpDbInfo;
    property AllowModifyImpData: WordBool read Get_AllowModifyImpData write Set_AllowModifyImpData;
    property CreateIndexOnClose: WordBool read Get_CreateIndexOnClose write Set_CreateIndexOnClose;
    property RemoveEmptyColumnsOnClose: WordBool read Get_RemoveEmptyColumnsOnClose write Set_RemoveEmptyColumnsOnClose;
    property TmpDBaseDir: WideString read Get_TmpDBaseDir write Set_TmpDBaseDir;
    property RefreshThreshold: Integer read Get_RefreshThreshold write Set_RefreshThreshold;
  end;

// *********************************************************************//
// DispIntf:  IAXImpExpDbcDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {87E05AE7-E34A-11D5-9D07-00C0F057241A}
// *********************************************************************//
  IAXImpExpDbcDisp = dispinterface
    ['{87E05AE7-E34A-11D5-9D07-00C0F057241A}']
    property Alignment: TxAlignment dispid 1;
    property AutoSize: WordBool dispid 2;
    property BevelInner: TxBevelCut dispid 3;
    property BevelOuter: TxBevelCut dispid 4;
    property BorderStyle: TxBorderStyle dispid 5;
    property Caption: WideString dispid -518;
    property Color: OLE_COLOR dispid -501;
    property Ctl3D: WordBool dispid 6;
    property UseDockManager: WordBool dispid 7;
    property DockSite: WordBool dispid 8;
    property DragCursor: Smallint dispid 9;
    property DragMode: TxDragMode dispid 10;
    property Enabled: WordBool dispid -514;
    property FullRepaint: WordBool dispid 11;
    property Font: IFontDisp dispid -512;
    property Locked: WordBool dispid 12;
    property ParentColor: WordBool dispid 13;
    property ParentCtl3D: WordBool dispid 14;
    property Visible: WordBool dispid 15;
    property DoubleBuffered: WordBool dispid 16;
    property VisibleDockClientCount: Integer readonly dispid 17;
    function DrawTextBiDiModeFlagsReadingOnly: Integer; dispid 19;
    procedure InitiateAction; dispid 20;
    function IsRightToLeft: WordBool; dispid 21;
    function UseRightToLeftReading: WordBool; dispid 24;
    function UseRightToLeftScrollBar: WordBool; dispid 25;
    property Cursor: Smallint dispid 26;
    property WorkingDir: WideString dispid 33;
    property LngCode: WideString dispid 34;
    property ImpDatabase: WideString dispid 35;
    property ImpTable: WideString dispid 36;
    property ImpDbMode: TDataBaseTypes dispid 18;
    property ExpDatabase: WideString dispid 22;
    property ExpTable: WideString dispid 23;
    property ExpDbMode: TDataBaseTypes dispid 27;
    property IgnoreNonExistExpTables: WordBool dispid 28;
    function GetExportColumn(const Layername: WideString; out Columnname: WideString; 
                             out Columntype: TDataBaseColTypes; out ColumnLength: Integer): WordBool; dispid 31;
    function GetExportData(const Layername: WideString; out Columnname: WideString; 
                           out Info: WideString): WordBool; dispid 29;
    function DoExportData(const Layername: WideString; Id: Integer): WordBool; dispid 30;
    procedure OpenExpTable(const Layername: WideString); dispid 32;
    procedure CloseExpTables; dispid 37;
    procedure CloseImpTables; dispid 38;
    procedure AddImpInfo(const Layername: WideString; const Columnname: WideString; 
                         const Info: WideString); dispid 39;
    procedure AddImpIdInfo(const Layername: WideString; Id: Integer); dispid 40;
    procedure OpenImpTable(const Layername: WideString); dispid 41;
    procedure CheckAndCreateImpTable(const Layername: WideString); dispid 42;
    procedure CheckAndCreateExpTable(const Layername: WideString); dispid 43;
    procedure AddImpIdColumn(const Layername: WideString); dispid 44;
    procedure AddImpColumn(const Layername: WideString; const Columnname: WideString; 
                           Columntype: TDataBaseColTypes; ColumnLength: Integer); dispid 45;
    procedure CreateImpTable(const Layername: WideString); dispid 46;
    procedure DeleteExpDBEntry(const Layername: WideString; Id: Integer); dispid 47;
    procedure DeleteExpTable(const Layername: WideString); dispid 48;
    procedure SetupByDialog(ParentWinHandle: Integer; WinLeft: Integer; WinTop: Integer); dispid 49;
    property ShowMode: TDatabaseShowmodes dispid 50;
    property ImpDbInfo: WideString readonly dispid 52;
    property ExpDbInfo: WideString readonly dispid 53;
    procedure DoEditImpInfo(const Layername: WideString; Id: Integer); dispid 51;
    property AllowModifyImpData: WordBool dispid 54;
    function DoExportRawData(const Layername: WideString): WordBool; dispid 55;
    function DoExportFromImpTable(const Layername: WideString; Id: Integer): WordBool; dispid 57;
    function GetExportDataFromImpTable(const Layername: WideString; out Columnname: WideString; 
                                       out Info: WideString): WordBool; dispid 58;
    property CreateIndexOnClose: WordBool dispid 56;
    function GetImpNumRecords(const Layername: WideString): Integer; dispid 59;
    function GetExpNumRecords(const Layername: WideString): Integer; dispid 60;
    procedure SetupByModalDialog; dispid 61;
    property RemoveEmptyColumnsOnClose: WordBool dispid 62;
    property TmpDBaseDir: WideString dispid 63;
    procedure EmptyExpTable(const Layername: WideString); dispid 64;
    property RefreshThreshold: Integer dispid 65;
  end;

// *********************************************************************//
// DispIntf:  IAXImpExpDbcEvents
// Flags:     (4096) Dispatchable
// GUID:      {87E05AE9-E34A-11D5-9D07-00C0F057241A}
// *********************************************************************//
  IAXImpExpDbcEvents = dispinterface
    ['{87E05AE9-E34A-11D5-9D07-00C0F057241A}']
    procedure OnCanResize(var NewWidth: Integer; var NewHeight: Integer; var Resize: WordBool); dispid 1;
    procedure OnClick; dispid 2;
    procedure OnConstrainedResize(var MinWidth: Integer; var MinHeight: Integer; 
                                  var MaxWidth: Integer; var MaxHeight: Integer); dispid 3;
    procedure OnDblClick; dispid 7;
    procedure OnResize; dispid 16;
    procedure OnDialogSetuped; dispid 4;
    procedure OnRequestImpLayerIndex(const Layername: WideString; var LayerIndex: Integer); dispid 5;
    procedure OnRequestExpLayerIndex(const Layername: WideString; var LayerIndex: Integer); dispid 6;
    procedure OnRequestImpProjectName(var ProjectName: WideString); dispid 8;
    function OnRequestExpProjectName(var ProjectName: WideString): WideString; dispid 9;
    procedure OnRequestImpProjectGUIDs(var StartGUID: WideString; var CurrentGUID: WideString); dispid 10;
    procedure OnWarning(const Info: WideString); dispid 11;
    procedure OnInfo(const Info: WideString); dispid 12;
    procedure OnShowPercent(Percent: Integer); dispid 13;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TAXImpExpDbc
// Help String      : AXImpExpDbc Control
// Default Interface: IAXImpExpDbc
// Def. Intf. DISP? : No
// Event   Interface: IAXImpExpDbcEvents
// TypeFlags        : (34) CanCreate Control
// *********************************************************************//
  TAXImpExpDbcOnCanResize = procedure(Sender: TObject; var NewWidth: Integer; 
                                                       var NewHeight: Integer; var Resize: WordBool) of object;
  TAXImpExpDbcOnConstrainedResize = procedure(Sender: TObject; var MinWidth: Integer; 
                                                               var MinHeight: Integer; 
                                                               var MaxWidth: Integer; 
                                                               var MaxHeight: Integer) of object;
  TAXImpExpDbcOnRequestImpLayerIndex = procedure(Sender: TObject; const Layername: WideString; 
                                                                  var LayerIndex: Integer) of object;
  TAXImpExpDbcOnRequestExpLayerIndex = procedure(Sender: TObject; const Layername: WideString; 
                                                                  var LayerIndex: Integer) of object;
  TAXImpExpDbcOnRequestImpProjectName = procedure(Sender: TObject; var ProjectName: WideString) of object;
  TAXImpExpDbcOnRequestExpProjectName = procedure(Sender: TObject; var ProjectName: WideString) of object;
  TAXImpExpDbcOnRequestImpProjectGUIDs = procedure(Sender: TObject; var StartGUID: WideString; 
                                                                    var CurrentGUID: WideString) of object;
  TAXImpExpDbcOnWarning = procedure(Sender: TObject; const Info: WideString) of object;
  TAXImpExpDbcOnInfo = procedure(Sender: TObject; const Info: WideString) of object;
  TAXImpExpDbcOnShowPercent = procedure(Sender: TObject; Percent: Integer) of object;

  TAXImpExpDbc = class(TOleControl)
  private
    FOnCanResize: TAXImpExpDbcOnCanResize;
    FIAXImpExpDbcEvents_OnClick: TNotifyEvent;
    FOnConstrainedResize: TAXImpExpDbcOnConstrainedResize;
    FIAXImpExpDbcEvents_OnDblClick: TNotifyEvent;
    FOnResize: TNotifyEvent;
    FOnDialogSetuped: TNotifyEvent;
    FOnRequestImpLayerIndex: TAXImpExpDbcOnRequestImpLayerIndex;
    FOnRequestExpLayerIndex: TAXImpExpDbcOnRequestExpLayerIndex;
    FOnRequestImpProjectName: TAXImpExpDbcOnRequestImpProjectName;
    FOnRequestExpProjectName: TAXImpExpDbcOnRequestExpProjectName;
    FOnRequestImpProjectGUIDs: TAXImpExpDbcOnRequestImpProjectGUIDs;
    FOnWarning: TAXImpExpDbcOnWarning;
    FOnInfo: TAXImpExpDbcOnInfo;
    FOnShowPercent: TAXImpExpDbcOnShowPercent;
    FIntf: IAXImpExpDbc;
    function  GetControlInterface: IAXImpExpDbc;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
  public
    function DrawTextBiDiModeFlagsReadingOnly: Integer;
    procedure InitiateAction;
    function IsRightToLeft: WordBool;
    function UseRightToLeftReading: WordBool;
    function UseRightToLeftScrollBar: WordBool;
    function GetExportColumn(const Layername: WideString; out Columnname: WideString; 
                             out Columntype: TDataBaseColTypes; out ColumnLength: Integer): WordBool;
    function GetExportData(const Layername: WideString; out Columnname: WideString; 
                           out Info: WideString): WordBool;
    function DoExportData(const Layername: WideString; Id: Integer): WordBool;
    procedure OpenExpTable(const Layername: WideString);
    procedure CloseExpTables;
    procedure CloseImpTables;
    procedure AddImpInfo(const Layername: WideString; const Columnname: WideString; 
                         const Info: WideString);
    procedure AddImpIdInfo(const Layername: WideString; Id: Integer);
    procedure OpenImpTable(const Layername: WideString);
    procedure CheckAndCreateImpTable(const Layername: WideString);
    procedure CheckAndCreateExpTable(const Layername: WideString);
    procedure AddImpIdColumn(const Layername: WideString);
    procedure AddImpColumn(const Layername: WideString; const Columnname: WideString; 
                           Columntype: TDataBaseColTypes; ColumnLength: Integer);
    procedure CreateImpTable(const Layername: WideString);
    procedure DeleteExpDBEntry(const Layername: WideString; Id: Integer);
    procedure DeleteExpTable(const Layername: WideString);
    procedure SetupByDialog(ParentWinHandle: Integer; WinLeft: Integer; WinTop: Integer);
    procedure DoEditImpInfo(const Layername: WideString; Id: Integer);
    function DoExportRawData(const Layername: WideString): WordBool;
    function DoExportFromImpTable(const Layername: WideString; Id: Integer): WordBool;
    function GetExportDataFromImpTable(const Layername: WideString; out Columnname: WideString; 
                                       out Info: WideString): WordBool;
    function GetImpNumRecords(const Layername: WideString): Integer;
    function GetExpNumRecords(const Layername: WideString): Integer;
    procedure SetupByModalDialog;
    procedure EmptyExpTable(const Layername: WideString);
    property  ControlInterface: IAXImpExpDbc read GetControlInterface;
    property DoubleBuffered: WordBool index 16 read GetWordBoolProp write SetWordBoolProp;
    property VisibleDockClientCount: Integer index 17 read GetIntegerProp;
    property ImpDbInfo: WideString index 52 read GetWideStringProp;
    property ExpDbInfo: WideString index 53 read GetWideStringProp;
  published
    property  ParentFont;
    property  Align;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property Alignment: TOleEnum index 1 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property AutoSize: WordBool index 2 read GetWordBoolProp write SetWordBoolProp stored False;
    property BevelInner: TOleEnum index 3 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property BevelOuter: TOleEnum index 4 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property BorderStyle: TOleEnum index 5 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property Caption: WideString index -518 read GetWideStringProp write SetWideStringProp stored False;
    property Color: TColor index -501 read GetTColorProp write SetTColorProp stored False;
    property Ctl3D: WordBool index 6 read GetWordBoolProp write SetWordBoolProp stored False;
    property UseDockManager: WordBool index 7 read GetWordBoolProp write SetWordBoolProp stored False;
    property DockSite: WordBool index 8 read GetWordBoolProp write SetWordBoolProp stored False;
    property DragCursor: Smallint index 9 read GetSmallintProp write SetSmallintProp stored False;
    property DragMode: TOleEnum index 10 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property Enabled: WordBool index -514 read GetWordBoolProp write SetWordBoolProp stored False;
    property FullRepaint: WordBool index 11 read GetWordBoolProp write SetWordBoolProp stored False;
    property Font: TFont index -512 read GetTFontProp write SetTFontProp stored False;
    property Locked: WordBool index 12 read GetWordBoolProp write SetWordBoolProp stored False;
    property ParentColor: WordBool index 13 read GetWordBoolProp write SetWordBoolProp stored False;
    property ParentCtl3D: WordBool index 14 read GetWordBoolProp write SetWordBoolProp stored False;
    property Visible: WordBool index 15 read GetWordBoolProp write SetWordBoolProp stored False;
    property Cursor: Smallint index 26 read GetSmallintProp write SetSmallintProp stored False;
    property WorkingDir: WideString index 33 read GetWideStringProp write SetWideStringProp stored False;
    property LngCode: WideString index 34 read GetWideStringProp write SetWideStringProp stored False;
    property ImpDatabase: WideString index 35 read GetWideStringProp write SetWideStringProp stored False;
    property ImpTable: WideString index 36 read GetWideStringProp write SetWideStringProp stored False;
    property ImpDbMode: TOleEnum index 18 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ExpDatabase: WideString index 22 read GetWideStringProp write SetWideStringProp stored False;
    property ExpTable: WideString index 23 read GetWideStringProp write SetWideStringProp stored False;
    property ExpDbMode: TOleEnum index 27 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property IgnoreNonExistExpTables: WordBool index 28 read GetWordBoolProp write SetWordBoolProp stored False;
    property ShowMode: TOleEnum index 50 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property AllowModifyImpData: WordBool index 54 read GetWordBoolProp write SetWordBoolProp stored False;
    property CreateIndexOnClose: WordBool index 56 read GetWordBoolProp write SetWordBoolProp stored False;
    property RemoveEmptyColumnsOnClose: WordBool index 62 read GetWordBoolProp write SetWordBoolProp stored False;
    property TmpDBaseDir: WideString index 63 read GetWideStringProp write SetWideStringProp stored False;
    property RefreshThreshold: Integer index 65 read GetIntegerProp write SetIntegerProp stored False;
    property OnCanResize: TAXImpExpDbcOnCanResize read FOnCanResize write FOnCanResize;
    property IAXImpExpDbcEvents_OnClick: TNotifyEvent read FIAXImpExpDbcEvents_OnClick write FIAXImpExpDbcEvents_OnClick;
    property OnConstrainedResize: TAXImpExpDbcOnConstrainedResize read FOnConstrainedResize write FOnConstrainedResize;
    property IAXImpExpDbcEvents_OnDblClick: TNotifyEvent read FIAXImpExpDbcEvents_OnDblClick write FIAXImpExpDbcEvents_OnDblClick;
    property OnResize: TNotifyEvent read FOnResize write FOnResize;
    property OnDialogSetuped: TNotifyEvent read FOnDialogSetuped write FOnDialogSetuped;
    property OnRequestImpLayerIndex: TAXImpExpDbcOnRequestImpLayerIndex read FOnRequestImpLayerIndex write FOnRequestImpLayerIndex;
    property OnRequestExpLayerIndex: TAXImpExpDbcOnRequestExpLayerIndex read FOnRequestExpLayerIndex write FOnRequestExpLayerIndex;
    property OnRequestImpProjectName: TAXImpExpDbcOnRequestImpProjectName read FOnRequestImpProjectName write FOnRequestImpProjectName;
    property OnRequestExpProjectName: TAXImpExpDbcOnRequestExpProjectName read FOnRequestExpProjectName write FOnRequestExpProjectName;
    property OnRequestImpProjectGUIDs: TAXImpExpDbcOnRequestImpProjectGUIDs read FOnRequestImpProjectGUIDs write FOnRequestImpProjectGUIDs;
    property OnWarning: TAXImpExpDbcOnWarning read FOnWarning write FOnWarning;
    property OnInfo: TAXImpExpDbcOnInfo read FOnInfo write FOnInfo;
    property OnShowPercent: TAXImpExpDbcOnShowPercent read FOnShowPercent write FOnShowPercent;
  end;

procedure Register;

implementation

uses ComObj;

procedure TAXImpExpDbc.InitControlData;
const
  CEventDispIDs: array [0..13] of DWORD = (
    $00000001, $00000002, $00000003, $00000007, $00000010, $00000004,
    $00000005, $00000006, $00000008, $00000009, $0000000A, $0000000B,
    $0000000C, $0000000D);
  CTFontIDs: array [0..0] of DWORD = (
    $FFFFFE00);
  CControlData: TControlData2 = (
    ClassID: '{87E05AEB-E34A-11D5-9D07-00C0F057241A}';
    EventIID: '{87E05AE9-E34A-11D5-9D07-00C0F057241A}';
    EventCount: 14;
    EventDispIDs: @CEventDispIDs;
    LicenseKey: nil;
    Flags: $0000001D;
    Version: 401;
    FontCount: 1;
    FontIDs: @CTFontIDs);
begin
  ControlData := @CControlData;
  TControlData2(CControlData).FirstEventOfs := Cardinal(@@FOnCanResize) - Cardinal(Self);
end;

procedure TAXImpExpDbc.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as IAXImpExpDbc;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TAXImpExpDbc.GetControlInterface: IAXImpExpDbc;
begin
  CreateControl;
  Result := FIntf;
end;

function TAXImpExpDbc.DrawTextBiDiModeFlagsReadingOnly: Integer;
begin
  Result := ControlInterface.DrawTextBiDiModeFlagsReadingOnly;
end;

procedure TAXImpExpDbc.InitiateAction;
begin
  ControlInterface.InitiateAction;
end;

function TAXImpExpDbc.IsRightToLeft: WordBool;
begin
  Result := ControlInterface.IsRightToLeft;
end;

function TAXImpExpDbc.UseRightToLeftReading: WordBool;
begin
  Result := ControlInterface.UseRightToLeftReading;
end;

function TAXImpExpDbc.UseRightToLeftScrollBar: WordBool;
begin
  Result := ControlInterface.UseRightToLeftScrollBar;
end;

function TAXImpExpDbc.GetExportColumn(const Layername: WideString; out Columnname: WideString; 
                                      out Columntype: TDataBaseColTypes; out ColumnLength: Integer): WordBool;
begin
  Result := ControlInterface.GetExportColumn(Layername, Columnname, Columntype, ColumnLength);
end;

function TAXImpExpDbc.GetExportData(const Layername: WideString; out Columnname: WideString; 
                                    out Info: WideString): WordBool;
begin
  Result := ControlInterface.GetExportData(Layername, Columnname, Info);
end;

function TAXImpExpDbc.DoExportData(const Layername: WideString; Id: Integer): WordBool;
begin
  Result := ControlInterface.DoExportData(Layername, Id);
end;

procedure TAXImpExpDbc.OpenExpTable(const Layername: WideString);
begin
  ControlInterface.OpenExpTable(Layername);
end;

procedure TAXImpExpDbc.CloseExpTables;
begin
  ControlInterface.CloseExpTables;
end;

procedure TAXImpExpDbc.CloseImpTables;
begin
  ControlInterface.CloseImpTables;
end;

procedure TAXImpExpDbc.AddImpInfo(const Layername: WideString; const Columnname: WideString; 
                                  const Info: WideString);
begin
  ControlInterface.AddImpInfo(Layername, Columnname, Info);
end;

procedure TAXImpExpDbc.AddImpIdInfo(const Layername: WideString; Id: Integer);
begin
  ControlInterface.AddImpIdInfo(Layername, Id);
end;

procedure TAXImpExpDbc.OpenImpTable(const Layername: WideString);
begin
  ControlInterface.OpenImpTable(Layername);
end;

procedure TAXImpExpDbc.CheckAndCreateImpTable(const Layername: WideString);
begin
  ControlInterface.CheckAndCreateImpTable(Layername);
end;

procedure TAXImpExpDbc.CheckAndCreateExpTable(const Layername: WideString);
begin
  ControlInterface.CheckAndCreateExpTable(Layername);
end;

procedure TAXImpExpDbc.AddImpIdColumn(const Layername: WideString);
begin
  ControlInterface.AddImpIdColumn(Layername);
end;

procedure TAXImpExpDbc.AddImpColumn(const Layername: WideString; const Columnname: WideString; 
                                    Columntype: TDataBaseColTypes; ColumnLength: Integer);
begin
  ControlInterface.AddImpColumn(Layername, Columnname, Columntype, ColumnLength);
end;

procedure TAXImpExpDbc.CreateImpTable(const Layername: WideString);
begin
  ControlInterface.CreateImpTable(Layername);
end;

procedure TAXImpExpDbc.DeleteExpDBEntry(const Layername: WideString; Id: Integer);
begin
  ControlInterface.DeleteExpDBEntry(Layername, Id);
end;

procedure TAXImpExpDbc.DeleteExpTable(const Layername: WideString);
begin
  ControlInterface.DeleteExpTable(Layername);
end;

procedure TAXImpExpDbc.SetupByDialog(ParentWinHandle: Integer; WinLeft: Integer; WinTop: Integer);
begin
  ControlInterface.SetupByDialog(ParentWinHandle, WinLeft, WinTop);
end;

procedure TAXImpExpDbc.DoEditImpInfo(const Layername: WideString; Id: Integer);
begin
  ControlInterface.DoEditImpInfo(Layername, Id);
end;

function TAXImpExpDbc.DoExportRawData(const Layername: WideString): WordBool;
begin
  Result := ControlInterface.DoExportRawData(Layername);
end;

function TAXImpExpDbc.DoExportFromImpTable(const Layername: WideString; Id: Integer): WordBool;
begin
  Result := ControlInterface.DoExportFromImpTable(Layername, Id);
end;

function TAXImpExpDbc.GetExportDataFromImpTable(const Layername: WideString; 
                                                out Columnname: WideString; out Info: WideString): WordBool;
begin
  Result := ControlInterface.GetExportDataFromImpTable(Layername, Columnname, Info);
end;

function TAXImpExpDbc.GetImpNumRecords(const Layername: WideString): Integer;
begin
  Result := ControlInterface.GetImpNumRecords(Layername);
end;

function TAXImpExpDbc.GetExpNumRecords(const Layername: WideString): Integer;
begin
  Result := ControlInterface.GetExpNumRecords(Layername);
end;

procedure TAXImpExpDbc.SetupByModalDialog;
begin
  ControlInterface.SetupByModalDialog;
end;

procedure TAXImpExpDbc.EmptyExpTable(const Layername: WideString);
begin
  ControlInterface.EmptyExpTable(Layername);
end;

procedure Register;
begin
  RegisterComponents('ActiveX',[TAXImpExpDbc]);
end;

end.


