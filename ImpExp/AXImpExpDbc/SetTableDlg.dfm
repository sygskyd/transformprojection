object SetTableWnd: TSetTableWnd
  Left = 407
  Top = 211
  BorderStyle = bsDialog
  Caption = 'Table not found!'
  ClientHeight = 238
  ClientWidth = 283
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object DestDBPC: TPageControl
    Left = 4
    Top = 36
    Width = 272
    Height = 140
    ActivePage = dBaseTabSheet
    TabIndex = 0
    TabOrder = 0
    OnChange = DestDBPCChange
    object dBaseTabSheet: TTabSheet
      Caption = 'dBase'
      object dBaseDirLB: TDirectoryListBox
        Left = 4
        Top = 4
        Width = 129
        Height = 85
        FileList = dBaseFileLB
        ItemHeight = 16
        TabOrder = 0
        OnChange = dBaseDirLBChange
      end
      object dBaseDriveCB: TDriveComboBox
        Left = 4
        Top = 90
        Width = 129
        Height = 19
        DirList = dBaseDirLB
        TabOrder = 1
        OnChange = dBaseDriveCBChange
      end
      object dBaseFileLB: TFileListBox
        Left = 132
        Top = 4
        Width = 129
        Height = 105
        ItemHeight = 13
        Mask = '*.dbf'
        TabOrder = 2
        OnChange = dBaseFileLBChange
        OnClick = dBaseFileLBClick
      end
    end
    object AccessTabSheet: TTabSheet
      Caption = 'Access'
      ImageIndex = 1
      object AccessTableLB: TListBox
        Left = 4
        Top = 4
        Width = 257
        Height = 105
        ItemHeight = 13
        TabOrder = 0
        OnClick = AccessTableLBClick
      end
    end
  end
  object Panel1: TPanel
    Left = 4
    Top = 4
    Width = 272
    Height = 25
    TabOrder = 1
    object LayerLabel: TLabel
      Left = 8
      Top = 8
      Width = 32
      Height = 13
      Caption = 'Layer: '
    end
  end
  object OKBtn: TButton
    Left = 74
    Top = 208
    Width = 93
    Height = 23
    Caption = 'Ok'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 2
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 175
    Top = 208
    Width = 101
    Height = 23
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
    OnClick = CancelBtnClick
  end
  object IgnoreTablesCb: TCheckBox
    Left = 4
    Top = 184
    Width = 269
    Height = 17
    Caption = 'IgnoreTablesCb'
    TabOrder = 4
    OnClick = IgnoreTablesCbClick
  end
end
