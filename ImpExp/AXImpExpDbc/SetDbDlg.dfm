object SetDbWnd: TSetDbWnd
  Left = 361
  Top = 252
  BorderStyle = bsDialog
  Caption = 'Setup database'
  ClientHeight = 352
  ClientWidth = 543
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object MainPageControl: TPageControl
    Left = 10
    Top = 39
    Width = 519
    Height = 258
    ActivePage = DestDbSheet
    TabIndex = 1
    TabOrder = 0
    object SourceDbSheet: TTabSheet
      Caption = 'Source Database'
      object SourceDbPageControl: TPageControl
        Left = 0
        Top = 0
        Width = 507
        Height = 218
        ActivePage = SourcedBaseSheet
        TabIndex = 0
        TabOrder = 0
        OnChange = SourceDbPageControlChange
        object SourcedBaseSheet: TTabSheet
          Caption = 'dBase'
          object SourcedBaseDirLB: TDirectoryListBox
            Left = 0
            Top = 5
            Width = 159
            Height = 154
            FileList = SourcedBaseFileLB
            ItemHeight = 16
            TabOrder = 0
            OnChange = SourcedBaseDirLBChange
            OnClick = SourcedBaseDirLBClick
          end
          object SourcedBaseDriveCB: TDriveComboBox
            Left = 0
            Top = 158
            Width = 159
            Height = 19
            DirList = SourcedBaseDirLB
            TabOrder = 1
          end
          object SourcedBaseSingleCB: TCheckBox
            Left = 167
            Top = 10
            Width = 120
            Height = 21
            Caption = 'single Table'
            TabOrder = 2
            OnClick = SourcedBaseSingleCBClick
          end
          object SourcedBaseFileLB: TFileListBox
            Left = 167
            Top = 34
            Width = 159
            Height = 144
            DragMode = dmAutomatic
            ItemHeight = 16
            Mask = '*.dbf'
            TabOrder = 3
            Visible = False
            OnChange = SourcedBaseFileLBChange
            OnClick = SourcedBaseFileLBClick
          end
        end
        object SourceAccessSheet: TTabSheet
          Caption = 'Access'
          ImageIndex = 1
          object SourceAccessDirLB: TDirectoryListBox
            Left = 0
            Top = 5
            Width = 159
            Height = 154
            FileList = SourceAccessFileLB
            ItemHeight = 16
            TabOrder = 0
            OnChange = SourceAccessDirLBChange
            OnClick = SourceAccessDirLBClick
          end
          object SourceAccessDriveCB: TDriveComboBox
            Left = 0
            Top = 158
            Width = 159
            Height = 19
            DirList = SourceAccessDirLB
            TabOrder = 1
          end
          object SourceAccessFileLB: TFileListBox
            Left = 167
            Top = 5
            Width = 159
            Height = 173
            DragMode = dmAutomatic
            ItemHeight = 16
            Mask = '*.mdb'
            TabOrder = 2
            OnChange = SourceAccessFileLBChange
            OnClick = SourceAccessFileLBClick
          end
          object SourceAccessTableLB: TListBox
            Left = 335
            Top = 34
            Width = 159
            Height = 144
            ItemHeight = 16
            TabOrder = 3
            Visible = False
            OnClick = SourceAccessTableLBClick
          end
          object SourceAccessSingleCB: TCheckBox
            Left = 335
            Top = 10
            Width = 99
            Height = 21
            Caption = 'single table'
            TabOrder = 4
            Visible = False
            OnClick = SourceAccessSingleCBClick
          end
        end
        object SourceIdbSheet: TTabSheet
          Caption = 'IDB'
          ImageIndex = 2
          object SourceIdbPanel: TPanel
            Left = 5
            Top = 30
            Width = 489
            Height = 148
            TabOrder = 1
            object SourceIdbDirLb: TDirectoryListBox
              Left = 5
              Top = 5
              Width = 159
              Height = 114
              FileList = SourceIdbFileLb
              ItemHeight = 16
              TabOrder = 0
              OnChange = SourceIdbDirLbChange
              OnClick = SourceIdbDirLbClick
            end
            object SourceIdbDriveCb: TDriveComboBox
              Left = 5
              Top = 121
              Width = 159
              Height = 19
              DirList = SourceIdbDirLb
              TabOrder = 1
            end
            object SourceIdbFileLb: TFileListBox
              Left = 172
              Top = 5
              Width = 159
              Height = 139
              ItemHeight = 16
              Mask = '*.wgi'
              TabOrder = 2
              OnChange = SourceIdbFileLbChange
              OnClick = SourceIdbFileLbClick
            end
          end
          object SourceIdbCb: TCheckBox
            Left = 5
            Top = 5
            Width = 449
            Height = 21
            Caption = 'IDB has not same name as project'
            TabOrder = 0
            OnClick = SourceIdbCbClick
          end
        end
      end
    end
    object DestDbSheet: TTabSheet
      Caption = 'Destination Database'
      ImageIndex = 1
      object DestDbPageControl: TPageControl
        Left = 0
        Top = 0
        Width = 507
        Height = 198
        ActivePage = DestTextSheet
        TabIndex = 3
        TabOrder = 0
        OnChange = DestDbPageControlChange
        object DestdBaseSheet: TTabSheet
          Caption = 'dBase'
          object DestdBaseSingleLabel: TLabel
            Left = 335
            Top = 10
            Width = 140
            Height = 16
            Caption = 'DestdBaseSingleLabel'
          end
          object DestdBaseDirLB: TDirectoryListBox
            Left = 0
            Top = 5
            Width = 159
            Height = 132
            FileList = DestdBaseFileLB
            ItemHeight = 16
            TabOrder = 0
            OnChange = DestdBaseDirLBChange
            OnClick = DestdBaseDirLBClick
          end
          object DestdBaseDriveCB: TDriveComboBox
            Left = 0
            Top = 138
            Width = 159
            Height = 19
            DirList = DestdBaseDirLB
            TabOrder = 1
          end
          object DestdBaseSingleCB: TCheckBox
            Left = 167
            Top = 10
            Width = 120
            Height = 21
            Caption = 'single Table'
            TabOrder = 2
            OnClick = DestdBaseSingleCBClick
          end
          object DestdBaseFileLB: TFileListBox
            Left = 167
            Top = 34
            Width = 159
            Height = 128
            DragMode = dmAutomatic
            ItemHeight = 16
            Mask = '*.dbf'
            TabOrder = 3
            Visible = False
            OnChange = DestdBaseFileLBChange
            OnClick = DestdBaseFileLBClick
          end
          object DestdBaseNewSingleBtn: TButton
            Left = 295
            Top = 0
            Width = 31
            Height = 31
            Caption = '...'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnClick = DestdBaseNewSingleBtnClick
          end
        end
        object DestAccessSheet: TTabSheet
          Caption = 'Access'
          ImageIndex = 1
          object newMdbLabel: TLabel
            Left = 207
            Top = 10
            Width = 85
            Height = 16
            Caption = 'newMdbLabel'
          end
          object DestAccessDirLB: TDirectoryListBox
            Left = 0
            Top = 5
            Width = 159
            Height = 132
            FileList = DestAccessFileLB
            ItemHeight = 16
            TabOrder = 0
            OnChange = DestAccessDirLBChange
            OnClick = DestAccessDirLBClick
          end
          object DestAccessDriveCB: TDriveComboBox
            Left = 0
            Top = 138
            Width = 159
            Height = 19
            DirList = DestAccessDirLB
            TabOrder = 1
          end
          object DestAccessFileLB: TFileListBox
            Left = 167
            Top = 34
            Width = 159
            Height = 130
            DragMode = dmAutomatic
            ItemHeight = 16
            Mask = '*.mdb'
            TabOrder = 2
            OnChange = DestAccessFileLBChange
            OnClick = DestAccessFileLBClick
          end
          object NewMdbBtn: TButton
            Left = 167
            Top = 0
            Width = 31
            Height = 31
            Caption = '...'
            TabOrder = 3
            OnClick = NewMdbBtnClick
          end
          object DestAccessSingleCB: TCheckBox
            Left = 335
            Top = 10
            Width = 159
            Height = 21
            Caption = 'single table'
            TabOrder = 4
            Visible = False
            OnClick = DestAccessSingleCBClick
          end
          object AccessNewSingleEdit: TEdit
            Left = 335
            Top = 34
            Width = 159
            Height = 21
            TabOrder = 5
            Visible = False
            OnChange = AccessNewSingleEditChange
            OnClick = AccessNewSingleEditClick
          end
          object DestAccessTableLB: TListBox
            Left = 335
            Top = 62
            Width = 159
            Height = 102
            ItemHeight = 16
            TabOrder = 6
            Visible = False
            OnClick = DestAccessTableLBClick
          end
        end
        object DestIdbSheet: TTabSheet
          Caption = 'IDB'
          ImageIndex = 2
          object DestIdbCb: TCheckBox
            Left = 5
            Top = 5
            Width = 479
            Height = 21
            Caption = 'IDB has not same name as project'
            TabOrder = 0
            OnClick = DestIdbCbClick
          end
          object DestIdbPanel: TPanel
            Left = 5
            Top = 30
            Width = 489
            Height = 40
            TabOrder = 1
            object Panel1: TPanel
              Left = 5
              Top = 5
              Width = 449
              Height = 31
              TabOrder = 0
              object DestIdbLabel: TLabel
                Left = 10
                Top = 10
                Width = 81
                Height = 16
                Caption = 'DestIdbLabel'
              end
            end
            object DestIdbBtn: TButton
              Left = 453
              Top = 5
              Width = 31
              Height = 31
              Caption = '...'
              TabOrder = 1
              OnClick = DestIdbBtnClick
            end
          end
        end
        object DestTextSheet: TTabSheet
          Caption = 'Textfile'
          ImageIndex = 3
          object DestTextDirLB: TDirectoryListBox
            Left = 0
            Top = 5
            Width = 159
            Height = 132
            ItemHeight = 16
            TabOrder = 0
            OnChange = DestTextDirLBChange
            OnClick = DestTextDirLBClick
          end
          object DestTextDriveCB: TDriveComboBox
            Left = 0
            Top = 138
            Width = 159
            Height = 22
            DirList = DestTextDirLB
            TabOrder = 1
          end
          object Panel2: TPanel
            Left = 167
            Top = 5
            Width = 327
            Height = 157
            TabOrder = 2
            object Label1: TLabel
              Left = 10
              Top = 10
              Width = 124
              Height = 16
              Caption = 'Spaltentrennzeichen:'
            end
            object ImpSepTabCb: TCheckBox
              Left = 20
              Top = 30
              Width = 198
              Height = 20
              Caption = 'Tabulator'
              TabOrder = 0
              OnClick = ImpSepTabCbClick
            end
            object ImpSepCommaCb: TCheckBox
              Left = 20
              Top = 49
              Width = 227
              Height = 21
              Caption = 'Beistrich'
              TabOrder = 1
              OnClick = ImpSepCommaCbClick
            end
            object ImpSepSpaceCb: TCheckBox
              Left = 20
              Top = 69
              Width = 218
              Height = 21
              Caption = 'Leerzeichen'
              TabOrder = 2
              OnClick = ImpSepSpaceCbClick
            end
            object ImpSepSemicolonCb: TCheckBox
              Left = 20
              Top = 89
              Width = 218
              Height = 21
              Caption = 'Strichpunkt'
              TabOrder = 3
              OnClick = ImpSepSemicolonCbClick
            end
          end
        end
      end
      object AllowModifyImpCb: TCheckBox
        Left = 1
        Top = 202
        Width = 325
        Height = 21
        Caption = 'Overwrite existing fields'
        TabOrder = 1
      end
    end
  end
  object UseDbCb: TCheckBox
    Left = 10
    Top = 10
    Width = 178
    Height = 21
    Caption = 'Use Database'
    TabOrder = 1
    OnClick = UseDbCbClick
  end
  object OKBtn: TButton
    Left = 286
    Top = 313
    Width = 114
    Height = 28
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 407
    Top = 313
    Width = 125
    Height = 28
    Cancel = True
    Caption = 'Abbrechen'
    ModalResult = 2
    TabOrder = 3
    OnClick = CancelBtnClick
  end
  object ADOConnection: TADOConnection
    LoginPrompt = False
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 312
  end
  object SaveDialogDBase: TSaveDialog
    DefaultExt = '*.dbf'
    FileName = '*.dbf'
    Filter = 'dBase-File|*.dbf'
    Left = 184
  end
  object SaveDialogAccess: TSaveDialog
    DefaultExt = '*.mdb'
    FileName = '*.mdb'
    Filter = 'Access-mdb|*.mdb'
    Left = 216
  end
  object SetupWindowTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = SetupWindowTimerTimer
    Top = 248
  end
  object SaveDialogWgi: TSaveDialog
    DefaultExt = '*.wgi'
    FileName = '*.wgi'
    Filter = 'Idb-File|*.wgi'
    Left = 248
  end
end
