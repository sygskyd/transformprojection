unit SetIdDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Strfile, Buttons, ExtCtrls;

type
  TSetIdWnd = class(TForm)
    IdLB: TListBox;
    Panel1: TPanel;
    LayerLabel: TLabel;
    OKBtn: TButton;
    CancelBtn: TButton;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    First       :boolean;
    IdColumnName:string;
    StrfileObj  :ClassStrFile;
  public
    { Public declarations }
    CloseType :integer;
    WorkingDir:string;
    LngCode   :string;
    function GetIDColName:string;
  end;

var SetIdWnd: TSetIdWnd;

implementation

{$R *.DFM}

procedure TSetIdWnd.FormCreate(Sender: TObject);
begin
   IdColumnName:='';
   CloseType:=0;
   First:=true;
end;

function TSetIdWnd.GetIDColName:string;
begin
   result:=IdColumnname;
end;

procedure TSetIdWnd.OKBtnClick(Sender: TObject);
var i:integer;
begin
   // set selected column
   for i:=0 to IdLB.Items.Count-1 do
   begin
      if IdLB.selected[i] = true then
      begin
         IdColumnname:=IdLB.items[i];
         break;
      end;
   end;
   Modalresult:=mrOk;
   CloseType:=1;
end;

procedure TSetIdWnd.CancelBtnClick(Sender: TObject);
begin
   Modalresult:=mrCancel;
   CloseType:=0;
end;

procedure TSetIdWnd.FormActivate(Sender: TObject);
begin
   if (First) then
   begin
      StrfileObj:=ClassStrFile.Create;
      StrfileObj.CreateList(WorkingDir, LngCode, 'IMPEXPDBC');
      Self.Caption:=StrfileObj.Assign(1);      // set ID column
      OkBtn.Caption:=StrfileObj.Assign(2);     // Ok
      CancelBtn.Caption:=StrfileObj.Assign(3); // Cancel
      First:=FALSE;
   end;
end;

procedure TSetIdWnd.FormDestroy(Sender: TObject);
begin
   StrFileObj.Destroy;
end;

end.
