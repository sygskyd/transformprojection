unit SetTableDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, FileCtrl, ComCtrls, Strfile;

type
  TSetTableWnd = class(TForm)
    DestDBPC: TPageControl;
    dBaseTabSheet: TTabSheet;
    dBaseDirLB: TDirectoryListBox;
    dBaseDriveCB: TDriveComboBox;
    dBaseFileLB: TFileListBox;
    AccessTabSheet: TTabSheet;
    AccessTableLB: TListBox;
    Panel1: TPanel;
    LayerLabel: TLabel;
    OKBtn: TButton;
    CancelBtn: TButton;
    IgnoreTablesCb: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure dBaseFileLBChange(Sender: TObject);
    procedure dBaseDirLBChange(Sender: TObject);
    procedure dBaseDriveCBChange(Sender: TObject);
    procedure dBaseFileLBClick(Sender: TObject);
    procedure AccessTableLBClick(Sender: TObject);
    procedure DestDBPCChange(Sender: TObject);
    procedure IgnoreTablesCbClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    TableSettings:integer;
    Tablename    :string;
    First        :boolean;
    StrfileObj   :ClassStrFile;
    procedure CheckWindow;
  public
    { Public declarations }
    CloseType :integer;
    WorkingDir:string;
    LngCode   :string;
    procedure SetTableSettings(TableMode:integer;Databasename:string);
    function  GetTableName:string;
  end;

var
  SetTableWnd: TSetTableWnd;

implementation

{$R *.DFM}

procedure TSetTableWnd.FormCreate(Sender: TObject);
begin
   IgnoreTablesCb.Checked:=FALSE;
   CloseType:=0;
   First:=true;
end;

function TSetTableWnd.GetTablename:string;
begin
   result:=Tablename;
end;

procedure TSetTableWnd.SetTableSettings(TableMode:integer;Databasename:string);
var i:integer;
begin
   TableSettings:=TableMode;
   if (TableSettings = 1) or (TableSettings = 2) then // DBF_Single or DBF_Directory
   begin
      DestDBPC.ActivePage:=dBaseTabSheet;
      dBaseTabSheet.Tabvisible:=true;
      AccessTabSheet.TabVisible:=false;
      dBaseDirLb.Directory:=Databasename;
   end;
   if (TableSettings = 3) or (TableSettings = 4) or (TableSettings = 5) then // ACCESS_Single or ACCESS_Tables or Idb_Database
   begin
      DestDBPC.ActivePage:=AccessTabSheet;
      dBaseTabSheet.Tabvisible:=false;
      AccessTabSheet.Tabvisible:=true;
   end;
end;


procedure TSetTableWnd.CheckWindow;
var allok:boolean;
begin
   allok:=true;
   if ((TableSettings = 1) or (TableSettings = 2)) and (DestDBPC.ActivePage <> dBaseTabSheet) then allok:=false;
   if ((TableSettings = 3) or (TableSettings = 4) or (TableSettings = 5)) and (DestDBPC.ActivePage <> AccessTabSheet) then allok:=false;
   if Tablename = '' then allok:=false;
   if IgnoreTablesCb.Checked then allok:=TRUE;
   if allok then OkBtn.Enabled:=true else OkBtn.Enabled:=false;
end;

procedure TSetTableWnd.dBaseFileLBChange(Sender: TObject);
var i:integer;
begin
   // find selected dBase File
   Tablename:='';
   for i:=0 to dBaseFileLb.Items.Count - 1 do
   begin
      if dBaseFileLb.selected[i] then
      begin
         Tablename:=dBaseDirLB.Directory; if length(Tablename) > 3 then Tablename:=Tablename + '\';
         Tablename:=Tablename + dBaseFileLb.items[i];
         break;
      end;
   end;
   CheckWindow;
end;

procedure TSetTableWnd.dBaseDirLBChange(Sender: TObject);
begin
   Tablename:='';
   CheckWindow;
end;

procedure TSetTableWnd.dBaseDriveCBChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSetTableWnd.dBaseFileLBClick(Sender: TObject);
var i:integer;
begin
   // find selected dBase File
   Tablename:='';
   for i:=0 to dBaseFileLb.Items.Count - 1 do
   begin
      if dBaseFileLb.selected[i] then
      begin
         Tablename:=dBaseDirLB.Directory; if length(Tablename) > 3 then Tablename:=Tablename + '\';
         Tablename:=Tablename + dBaseFileLb.items[i];
         break;
      end;
   end;
   CheckWindow;
end;

procedure TSetTableWnd.AccessTableLBClick(Sender: TObject);
var i:integer;
begin
   // find selected dBase File
   Tablename:='';
   for i:=0 to AccessTableLB.Items.Count - 1 do
   begin
      if AccessTableLB.selected[i] then
      begin
         Tablename:=AccessTableLB.items[i];
         break;
      end;
   end;
   CheckWindow;
end;

procedure TSetTableWnd.DestDBPCChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSetTableWnd.IgnoreTablesCbClick(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSetTableWnd.OKBtnClick(Sender: TObject);
begin
   CloseType:=1;
end;

procedure TSetTableWnd.CancelBtnClick(Sender: TObject);
begin
   CloseType:=0;
end;

procedure TSetTableWnd.FormActivate(Sender: TObject);
begin
   if (First) then
   begin
      StrfileObj:=ClassStrFile.Create;
      StrfileObj.CreateList(WorkingDir, LngCode, 'IMPEXPDBC');
      OkBtn.Caption:=StrFileObj.Assign(2);     // Ok
      CancelBtn.Caption:=StrFileObj.Assign(3); // Cancel
      IgnoreTablesCb.Caption:=StrFileObj.Assign(6); // Ignore check for all layers that are not combined
      if TableSettings = 5 then
         AccessTabSheet.Caption:=StrFileObj.Assign(24); // Idb
      First:=FALSE;
   end;
end;

procedure TSetTableWnd.FormDestroy(Sender: TObject);
begin
   StrFileObj.Destroy;
end;

end.
