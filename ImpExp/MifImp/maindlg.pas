unit Maindlg;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Strfile, StdCtrls, Buttons, ExtCtrls, Gauges, Files, Db,
  DBTables,OptionDlg,SetMIFFileDlg, Math, Windows, OleCtrls,
  AXImpExpDbcXControl_TLB, AxGisPro_TLB, struct, AXGisMan_TLB, AXDLL_TLB,
  WinOSInfo;

const
      //registration modes
      NO_LICENCE        = 0;
      FULL_LICENCE      = 1;
      DEMO_LICENCE      = 2;

      // Object types
      ot_Pixel     = 1;
      ot_Poly      = 2;
      ot_CPoly     = 4;
      ot_Text      = 7;
      ot_Symbol    = 8;
      ot_Spline    = 9;
      ot_Image     = 10;
      ot_MesLine   = 11;
      ot_Circle    = 12;
      ot_Arc       = 13;

            //TextSTyles
      ts_Left           =  0;
      ts_Center         =  1;
      ts_Right          =  2;
      ts_Italic         =  4;
      ts_Bold           =  8;
      ts_Underl         = 16;
      ts_FixHeight      = 32;
      ts_Transparent    = 64;

      //LineTypes
      lt_Solid     = 0;
      lt_Dash      = 1;
      lt_Dot       = 2;
      lt_DashDot   = 3;
      lt_DashDDot  = 4;
      lt_UserDefined    = 1000;

      //FillTypes
      pt_NoPattern = 0;
      pt_FDiagonal = 1;
      pt_Cross     = 2;
      pt_DiagCross = 3;
      pt_BDiagonal = 4;
      pt_Horizontal= 5;
      pt_Vertical  = 6;
      pt_Solid     = 7;
      pt_UserDefined = 8;

      stDrawingUnits    = 0;
      stScaleInDependend= 1;
      stProjectScale    = 2;
      stPercent         = 3;

type
  TMainWnd = class(TForm)
    Label1: TLabel;
    CloseTimer: TTimer;
    Panel1: TPanel;
    Label2: TLabel;
    Prozessbalken: TGauge;
    StatusLB: TListBox;
    SaveReportBtn: TButton;
    SaveDialogRep: TSaveDialog;
    OK: TButton;
    Cancel: TButton;
    SetupWindowTimer: TTimer;
    AXImpExpDbc: TAXImpExpDbc;
    AxGisProjection: TAxGisProjection;
    AXGisObjMan: TAXGisObjMan;
    procedure FormCreate(Sender: TObject);
    procedure StartBtnClick(Sender: TObject);
    procedure CloseTimerTimer(Sender: TObject);
    procedure CancelClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SaveReportBtnClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AXImpExpDbcDialogSetuped(Sender: TObject);
    procedure AxGisProjectionWarning(Sender: TObject;
      const Info: WideString);
    procedure AxGisProjectionError(Sender: TObject;
      const Info: WideString);
    procedure AXImpExpDbcRequestImpLayerIndex(Sender: TObject;
      const Layername: WideString; var LayerIndex: Integer);
    procedure AXImpExpDbcRequestImpProjectName(Sender: TObject;
      var ProjectName: WideString);
    procedure AXImpExpDbcRequestImpProjectGUIDs(Sender: TObject;
      var StartGUID, CurrentGUID: WideString);
  private
    { Private-Deklarationen }
    StrFileObj :ClassStrfile;
    FileObj    :ClassFile;
    Pass       :integer;
    MIFDateiname:string;
    Bytes2work  :longint;
    Bytesworked :longint;

    ObjectCount :longint;

    Aktiv        :boolean;
    Abort        :boolean;
    FirstActivate:boolean;

    oldFilename :string;

    aLayer      :ILayer;
    APolyline   :IPolyline;
    APolygon    :IPolygon;
    AIslandList :IList;

    LayerCreated:boolean;

    // statistic properties
    NumPolys         :integer;
    NumCPolys        :integer;
    NumIslands       :integer;
    NumPixels        :integer;
    NumTexts         :integer;
    NumCircles       :integer;
    NumLayers        :integer;
    NumSymbols       :integer;
    NumSymbolTexts   :integer;
    FilesProcessed   :integer;
    NumIgnoredObjects:integer;
    FileStartTime    :TDateTime;
    OverAllStartTime :TDateTime;


    procedure RunPass(AktPass:integer);
    procedure ExecutePass;
    function  GetAngle(X1:double;Y1:double;X2:double;Y2:double):double;
    procedure DisplayDemoWarning;
    procedure StartImport;
    procedure DisplayStatistics;
  public
    { Public-Deklarationen }
    App           :Variant;
    DestDocument  :IDocument;

    RegMode       :integer;
    LngCode       :string;
    GisHandle     :integer;

    MIDOK     :boolean;
    ExitNormal:boolean;

    { Procedures that are used to handle AX-Control }
    procedure CreateLayer(Layer:string);
    function  InsertPoint(X:double;Y:double;var HasDuplicate:boolean):integer;
    function  CheckLayerExists(Layer:string):boolean;
    procedure CreatePolyline;
    procedure InsertPointToPolyLine(X:double; Y:double);
    function  ClosePolyline(var HasDuplicate:boolean):integer;
    procedure CreatePolygon;
    procedure InsertPointToPolygon(X:double; Y:double);
    function  ClosePolygon(var HasDuplicate:boolean):integer;
    procedure SetLayerPriorities(Linienstil:longint;Linienfarbe:longint;Flaechenstil:longint;Flaechenfarbe:longint);
    function  InsertText(X1:double;Y1:double;X2:double; Y2:double; aAngle:integer;Textinfo:string;aHeight:integer;font:string;Textstyle:integer;var HasDuplicate:boolean):integer;
    function  InsertSymbolText(X:double;Y:double; SymText:string;SymHeight:integer;Symfont:string;var HasDuplicate:boolean):integer;
    function  CreateEllipse(X:double;Y:double;Radius1:integer; Radius2:integer; var HasDuplicate:boolean):integer;
    function  InsertSymbol(X:double;Y:double;Symname:string;aSize:double;Angle:double;var HasDuplicate:boolean):integer;
    procedure CreateIslandList;
    procedure ClosePolyAndAddToIsland;
    function CloseIsland(var HasDuplicate:boolean):integer;
    procedure ShowProcent(Wert:longint);
    function GetMIFFilename:string;

    procedure SetProjectionSettings;
    procedure CheckProjectionSettings;
    procedure CreateStringFile;

    procedure GetSymbolRefData(Fontname:string; SymIdx:integer; var GisSymbolName:string);
    procedure SetSymbolRefData(Fontname:string; SymIdx:integer; GisSymbolName:string);
    procedure ResetCurrentSymbolRef;
    function  GetCurrentSymbolRefData(var Fontname:string; var SymIdx:integer; var GisSymbolName:string):boolean;
    function  GetSymbolRefDataByIdx(Index:integer):p_SymbolRef_record;
  end;

var
  MainWnd: TMainWnd;

implementation

uses comobj, SymMapDlg;
{$R *.DFM}

procedure TMainWnd.CreateStringFile;
begin
   StrFileObj:=ClassStrfile.Create;
   StrFileObj.CreateList(OSInfo.WingisDir,LngCode,'MIFIMP');

   Self.Caption:=StrFileObj.Assign(1);          { MAPINFO MIF Import 4.0 }
   Label2.Caption:=StrFileObj.Assign(2);        { Processmessages: }
   SaveReportBtn.Caption:=StrFileObj.Assign(3); { save report }
   Ok.Caption:=StrFileObj.Assign(4);            { Ok }
   Cancel.Caption:=StrFileObj.Assign(5);        { Cancel }
   Self.Update;

   // set screen settings
   OptionWnd.SetScreenSettings;
   SymMapWnd.SetScreenSettings;
end;
procedure TMainWnd.FormCreate(Sender: TObject);
begin
   OptionWnd:=TOptionWnd.Create(Self);
   SymMapWnd:=TSymMapWnd.Create(Self);
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   Self.Height:=1;
   Self.Width:=1;
   Aktiv:=false;
   ObjectCount:=0;
   FirstActivate:=true;

   // reset statistic values
   NumPolys:=0;
   NumCPolys:=0;
   NumIslands:=0;
   NumPixels:=0;
   NumTexts:=0;
   NumCircles:=0;
   FilesProcessed:=0;
   NumSymbols:=0;
   NumSymbolTexts:=0;
   NumIgnoredObjects:=0;
   NumLayers:=0;

   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

procedure TMainWnd.CheckProjectionSettings;
var SourceProjection:integer;
    hstr:string;
    different:boolean;
begin
   different:=false;
   // function is used to check if the settings in the
   if (AxGisProjection.TargetCoordinateSystem <> DestDocument.Projection) then
      different:=TRUE;

   if (AxGisProjection.TargetXOffset <> DestDocument.ProjectionXOffset) then
      different:=TRUE;

   if (AxGisProjection.TargetYOffset <> DestDocument.ProjectionYOffset) then
      different:=TRUE;

   if (AxGisProjection.TargetScale <> DestDocument.ProjectionScale) then
      different:=TRUE;

   // check if the projections are different
   if DestDocument.ProjectProjSettings <> AxGisProjection.TargetProjSettings then
      different:=TRUE;

   if DestDocument.ProjectProjection <> AxGisProjection.TargetProjection then
      different:=TRUE;

   if DestDocument.Projectdate <> AxGisProjection.TargetDate then
      different:=TRUE;

   if different then
   begin
      Showmessage(StrfileObj.Assign(58)); // Warning: Projection does not fit to project projection
      AxGisProjection.SetupByDialog;
   end;
   // the used output coordinate-system is equal to the
   // target-coordinate-system
   AxGisProjection.UsedInputCoordSystem:=AxGisProjection.SourceCoordinateSystem;
end;

procedure TMainWnd.SetProjectionSettings;
var aApp, aDoc:OleVariant;
begin
   // procedure is used to set the projection settings
   // of the current project
   AxGisProjection.WorkingPath:=OSInfo.WingisDir;
   AxGisProjection.LngCode:=LngCode;
   AxImpExpDbc.WorkingDir:=OSInfo.WingisDir;
   AxImpExpDbc.LngCode:=LngCode;
   AxImpExpDbc.ShowMode:=2; // Shw_Target
   SetMifFileWnd.DestDbLabel.Caption:=AxImpExpDbc.ImpDbInfo;
   AXGisObjMan.WorkingDir:=OSInfo.WingisDir;
   AXGisObjMan.LngCode:=LngCode;
   aApp:=App;
   AxGisObjMan.SetApp(aApp);
   aDoc:=DestDocument;
   AXGisObjMan.SetDocument(aDoc);
   AxGisProjection.SourceCoordinateSystem:=cs_Carthesic; // mif data is normaly in carthesic
   AxGisProjection.TargetCoordinateSystem:=DestDocument.Projection;
   AxGisProjection.SourceProjSettings:=DestDocument.ProjectProjSettings;
   AxGisProjection.TargetProjSettings:=DestDocument.ProjectProjSettings;
   AxGisProjection.SourceProjection:=DestDocument.ProjectProjection;
   AxGisProjection.TargetProjection:=DestDocument.ProjectProjection;
   AxGisProjection.SourceDate:=DestDocument.Projectdate;
   AxGisProjection.TargetDate:=DestDocument.Projectdate;
   AxGisProjection.SourceXOffset:=DestDocument.ProjectionXOffset;
   AxGisProjection.TargetXOffset:=DestDocument.ProjectionXOffset;
   AxGisProjection.SourceYOffset:=DestDocument.ProjectionYOffset;
   AxGisProjection.TargetYOffset:=DestDocument.ProjectionYOffset;
   AxGisProjection.SourceScale:=DestDocument.ProjectionScale;
   AxGisProjection.TargetScale:=DestDocument.ProjectionScale;
   // if source and target is .amp - file
   // the used input coordinates and used output coordinates
   // for the projection calculation are in carthesic
   AxGisProjection.UsedInputCoordSystem:=AxGisProjection.SourceCoordinateSystem;
   AxGisProjection.UsedOutputCoordSystem:=cs_Carthesic;
end;

function TMainWnd.GetMIFFilename:string;
begin
   Result:=MIFDateiname;
end;

{*******************************************************************************
* PROZEDUR : StartBtnClick                                                     *
********************************************************************************
* Prozedur wird aufgerufen wenn der Start-Button im MainWnd                    *
* gedr�ckt wurde. Damit wird die Konvertierung gestartet.                      *
*                                                                              *
********************************************************************************}
procedure TMainWnd.StartBtnClick(Sender: TObject);
begin
   LayerCreated:=FALSE;
   RunPass(1);
end;

{*******************************************************************************
* PROZEDUR : RunPass                                                           *
********************************************************************************
* Prozedur leitet den jeweiligen Konvertierungs-Pass ein.                      *
*                                                                              *
* PARAMETER: Pass -> Gibt den Konvertierungs-Pass an.                          *
*                                                                              *
********************************************************************************}
procedure TMainWnd.RunPass(AktPass:integer);
var Ende:boolean;
    Layer:string;
    i:integer;
    aDate, DispDate, aTime, DispTime:string;
    EllapsedTime:TDateTime;
begin
   if AktPass = 1 then { Kontrolle, ob die Datei ins Dos-Format konvertiert werden muss }
   begin
      if FilesProcessed > 0 then
      begin
         StatusLB.Items.Add(StrfileObj.Assign(21)); //  File correct imported!
         // display end time for this file
         EllapsedTime:=now-FileStartTime;
         aDate:=FormatDateTime('ddmmyy',now);
         DispDate:=copy(aDate,1,2)+'.'+copy(aDate,3,2)+'.'+copy(aDate,5,2);
         aTime:=Formatdatetime('hhnnss',now);
         DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
         StatusLB.Items.Add(StrFileObj.Assign(67) + DispDate+' / '+DispTime); // end time:
         // display ellapsed time for this file
         aTime:=Formatdatetime('hhnnss',EllapsedTime);
         DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
         StatusLB.Items.Add(StrFileObj.Assign(80) + DispTime); // ellapsed time:
         StatusLB.Items.Add('------------------------------------------');

         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
      end;

      if not SetMIFFileWnd.GetFilename(MIFDateiname) then
      begin
         aktiv:=false;
         Abort:=true;
         Cancel.Enabled:=false;
         Ok.Enabled:=true;
         SaveReportBtn.Enabled:=true;
         DisplayStatistics;
         exit;
      end;

      FileObj:=ClassFile.Create;
      Aktiv:=true;

      // display the starttime
      if (FilesProcessed = 0) then
         OverAllStartTime:=now;
      FilesProcessed:=FilesProcessed+1;
      FileStartTime:=now;
      aDate:=FormatDateTime('ddmmyy',now);
      DispDate:=copy(aDate,1,2)+'.'+copy(aDate,3,2)+'.'+copy(aDate,5,2);
      aTime:=Formatdatetime('hhnnss',Now);
      DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
      StatusLB.Items.Add(StrFileObj.Assign(65) + DispDate+' / '+DispTime); // start time:

      StatusLB.Items.Add(StrfileObj.Assign(13) + ExtractFilename(MifDateiname)); // Mif file:
      if Fileexists(Changefileext(MIFDateiname,'.MID')) then
      begin
         StatusLB.Items.Add(StrfileObj.Assign(49) + ExtractFilename(Changefileext(MIFDateiname,'.MID'))); // Mid File:
      end;

      // display database information
      StatusLB.items.add(AxImpExpDbc.ImpDbInfo);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      CheckProjectionSettings;

      oldFilename:=MIFDateiname;
      Bytes2Work:=FileObj.GetFilesize(MIFDateiname); Bytesworked:=0;
      if FileObj.Check_odoa(MIFDateiname) then
      begin
         FileObj.OpenFile(MIFDateiname);
         StatusLB.Items.Add(StrfileObj.Assign(14)); { convert file to DOS format }
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         Pass:=1; ExecutePass;
         exit;
      end
      else
         begin RunPass(2); exit; end;
   end;

   if AktPass = 2 then { �ffnen der Dateien und starten des Einlesens  }
   begin
      { Setzen der Maskeneinstellungen }
      StatusLB.Items.Add(StrfileObj.Assign(15));
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      { Die Anzahl der zu bearbeitenden Bytes wird ermittelt }
      Bytes2Work:=FileObj.GetFilesize(MIFDateiname); Bytesworked:=0;
      { Die Dateien werden ge�ffnet }
      FileObj.OpenMIFFile(MIFDateiname);
      Pass:=2;
      ExecutePass; exit;
   end;

   if AktPass = 3 then // write REGION objects
   begin
      if (SetmiffileWnd.SelectivCB.checked) and (FileObj.CheckSymbolsExist) then
      begin
         if SymMapWnd.PageControl1.PageCount = 2 then
         begin
            // fill the Symbol-List
            SymMapWnd.SymbolLb.Items.Clear;
            for i:=0 to DestDocument.Symbols.Count-1 do
            begin
               SymMapWnd.SymbolLb.Items.Add(DestDocument.Symbols.Items[i].Name);
            end;
         end;
         SymMapWnd.ShowModal;
      end;
      Layer:=Extractfilename(MIFDateiname); Layer:=copy(Layer,1,pos('.',Layer)-1);

      FileObj.WriteLayerDefinition;
      FileObj.CreateDatabase(Layer);

      Bytes2Work:=FileObj.GetRegionElements; Bytesworked:=0;
      if Bytes2work > 0 then
      begin
         // set mask settings
         StatusLB.Items.Add(StrfileObj.Assign(16));
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
      end;
      Pass:=3;
      ExecutePass; exit;
   end;

   if AktPass = 4 then // write PLINE objects
   begin
      Bytes2Work:=FileObj.GetPLineElements; Bytesworked:=0;
      if Bytes2Work > 0 then
      begin
         // set mask settings
         StatusLB.Items.Add(StrfileObj.Assign(17));
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
      end;
      Pass:=4;
      ExecutePass; exit;
   end;

   if AktPass = 5 then // write POINT objects
   begin
      Bytes2Work:=FileObj.GetPointElements; Bytesworked:=0;
      if Bytes2Work > 0 then
      begin
         // set mask settings
         StatusLB.Items.Add(StrfileObj.Assign(18));
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
      end;
      Pass:=5;
      ExecutePass; exit;
   end;

   if AktPass = 6 then // write ELLIPSE objects
   begin
      Bytes2Work:=FileObj.GetEllipseElements; Bytesworked:=0;
      if Bytes2Work > 0 then
      begin
         // set mask settings
         StatusLB.Items.Add(StrfileObj.Assign(19));
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
      end;
      Pass:=6;
      ExecutePass; exit;
   end;

   if AktPass = 7 then // write TEXT objects
   begin
      Bytes2Work:=FileObj.GetTextElements; Bytesworked:=0;
      if Bytes2Work > 0 then
      begin
         // set mask settings
         StatusLB.Items.Add(StrfileObj.Assign(20));
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
      end;
      Pass:=7;
      ExecutePass; exit;
   end;

   if AktPass = 8 then // MIF-file imported
   begin
      FileObj.CloseallFiles;
      FileObj.Destroy;
      RunPass(1); // Import next file
      exit;
   end;
end;

procedure TMainWnd.ShowProcent(Wert:longint);
var Prozent:integer;
begin
   if Bytes2work > 0 then Prozent:= Round (((Bytesworked + Wert) / Bytes2work) * 100 ) else Prozent:= 100;
   ProzessBalken.Progress :=Prozent;
   ProzessBalken.Update;
end;

{*******************************************************************************
* PROZEDUR : ExecutePass                                                       *
********************************************************************************
*                                                                              *
*                                                                              *
********************************************************************************}
procedure TMainWnd.ExecutePass;
var Prozent:integer;
begin
   if Pass = 1 then { Konvertieren der MIF-Datei aus dem Dos-Format in das Unix-Format }
   begin
      repeat
         Bytesworked:=Bytesworked + FileObj.To_odoa;
         { Anzeigen des Prozessstatus }
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then
      begin
         FileObj.Closefiles;
         MIFDateiname:=Changefileext(MIFDateiname,'.tmp');
         RunPass(2);
         exit;
      end;
      if Abort = true then begin FileObj.Closefiles; exit; end;
   end;

   if Pass = 2 then { Einlesen der Objektdaten aus dem MIF-File }
   begin
      if Bytes2work > 0 then
      repeat
         Bytesworked:=Bytesworked + FileObj.ReadElement;
         { Anzeigen des Prozessstatus }
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then
      begin
         RunPass(3);
      end;
      if Abort = true then exit;
      exit;
   end;

   if Pass = 3 then { Schreiben der Region Objekte }
   begin
      if Bytes2work > 0 then
      repeat
         Bytesworked:=Bytesworked + FileObj.WriteCurrentRegion;
         { Anzeigen des Prozessstatus }
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then RunPass(4);
      if Abort = true then exit;
      exit;
   end;

   if Pass = 4 then { Schreiben der PLine Objekte }
   begin
      if Bytes2work > 0 then
      repeat
         Bytesworked:=Bytesworked + FileObj.WriteCurrentPLine;
         { Anzeigen des Prozessstatus }
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then RunPass(5);
      if Abort = true then exit;
      exit;
   end;

   if Pass = 5 then { Schreiben der Point Objekte }
   begin
      if Bytes2work > 0 then
      repeat
         Bytesworked:=Bytesworked + FileObj.WriteCurrentPoint;
         { Anzeigen des Prozessstatus }
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then RunPass(6);
      if Abort = true then exit;
      exit;
   end;

   if Pass = 6 then { Schreiben der Ellipse Objekte }
   begin
      if Bytes2work > 0 then
      repeat
         Bytesworked:=Bytesworked + FileObj.WriteCurrentEllipse;
         { Anzeigen des Prozessstatus }
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then RunPass(7);
      if Abort = true then exit;
      exit;
   end;

   if Pass = 7 then { Schreiben der Text Objekte }
   begin
      if Bytes2work > 0 then
      repeat
         Bytesworked:=Bytesworked + FileObj.WriteCurrentText;
         { Anzeigen des Prozessstatus }
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then RunPass(8);
      if Abort = true then exit;
      exit;
   end;
end;

procedure TMainWnd.CreateLayer(Layer:string);
begin
   NumLayers:=NumLayers+1;
   aLayer:=DestDocument.Layers.InsertLayerByName(Layer,True);
end;

function TMainWnd.CheckLayerExists(Layer:string):boolean;
var dispatch:IDispatch;
    retVal:boolean;
begin
   dispatch:=DestDocument.Layers.GetLayerByName(Layer);
   if dispatch <> nil then retVal:=true else retVal:=false;
   if retVal then
      aLayer:=DestDocument.Layers.GetLayerByName(Layer);
   result:=retVal;
end;

procedure TMainWnd.DisplayDemoWarning;
begin
   StatusLB.Items.Add(StrfileObj.Assign(48)); { 100 Objects created - following objects will be skiped. }
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;

   // set database to DB_None to avoid further importing
   AXImpExpDbc.ImpDbMode:=0; // Db_None
end;

procedure TMainWnd.CreatePolyline;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;
   APolyline:=DestDocument.CreatePolyline;
end;

procedure TMainWnd.InsertPointToPolyLine(X:double; Y:double);
var APoint:IPoint;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;

   AxGisProjection.Calculate(X,Y);
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   APolyLine.InsertPoint(APoint);
end;

function TMainWnd.CloseIsland(var HasDuplicate:boolean):integer;
var AIndex:integer;
    IslandPoly:IPolygon;
    DuplId    :integer;
begin
   IslandPoly:=DestDocument.CreateIslandArea(AIslandList);
   aLayer.InsertObject(IslandPoly);
   AIndex:=IslandPoly.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;
   // check if it is a duplicate
   if SetMifFileWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, aLayer.Layername);
      if DuplId <> 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;
   if (AIndex <> 0) and (DuplId = 0) then
      NumIslands:=NumIslands+1;

   result:=AIndex;
end;

function TMainWnd.ClosePolyline(var HasDuplicate:boolean):integer;
var AIndex:integer;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;
   aLayer.InsertObject(APolyline);
   AIndex:=APolyline.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;
   // check if it is a duplicate
   if SetMifFileWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, aLayer.Layername);
      if DuplId <> 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;
   if (AIndex <> 0) and (DuplId = 0) then
      NumPolys:=NumPolys+1;

   result:=AIndex;
end;

procedure TMainWnd.CreateIslandList;
begin
   AIslandList:=DestDocument.CreateList;
end;

procedure TMainWnd.CreatePolygon;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;
   APolygon:=DestDocument.CreatePolygon;
end;

procedure TMainWnd.InsertPointToPolygon(X:double; Y:double);
var APoint:IPoint;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;
   AxGisProjection.Calculate(X,Y);
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   APolygon.InsertPoint(APoint);
end;

procedure TMainWnd.ClosePolyAndAddToIsland;
begin
   // close polygon of island and add to list
   APolygon.ClosePoly;
   AIslandList.Add(APolygon);
end;

function TMainWnd.ClosePolygon(var HasDuplicate:boolean):integer;
var AIndex:integer;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;
   APolygon.ClosePoly;
   aLayer.InsertObject(APolygon);
   AIndex:=APolygon.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;
   // check if it is a duplicate
   if SetMifFileWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, aLayer.Layername);
      if DuplId <> 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;

   if (AIndex <> 0) and (DuplId = 0) then
      NumCPolys:=NumCPolys+1;

   result:=AIndex;
end;

function TMainWnd.InsertPoint(X:double;Y:double;var HasDuplicate:boolean):integer;
var APixel:IPixel;
    APoint:IPoint;
    AIndex:integer;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;
   AxGisProjection.Calculate(X,Y);
   APixel:=DestDocument.CreatePixel;
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   APixel.Position:=APoint;
   aLayer.InsertObject(APixel);
   AIndex:=APixel.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;
   // check if it is a duplicate
   if SetMifFileWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, aLayer.Layername);
      if DuplId <> 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;
   if (AIndex <> 0) and (DuplId = 0) then
      NumPixels:=NumPixels+1;
   result:=AIndex;
end;

procedure TMainWnd.SetLayerPriorities(Linienstil:longint;Linienfarbe:longint;Flaechenstil:longint;Flaechenfarbe:longint);
begin
   aLayer.Linestyle.Style:=Linienstil;
   aLayer.Linestyle.Color:=Linienfarbe;
   aLayer.Linestyle.FillColor:=Linienfarbe;

   aLayer.Fillstyle.Pattern:=Flaechenstil;
   aLayer.Fillstyle.ForeColor:=Flaechenfarbe;
   aLayer.Fillstyle.BackColor:=Flaechenfarbe;
end;

function TMainWnd.InsertSymbol(X:double;Y:double;Symname:string;aSize:double;Angle:double;var HasDuplicate:boolean):integer;
var ASymbol:ISymbol;
    APoint :IPoint;
    MySymboldef:ISymbolDef;
    i:integer;
    gefunden:boolean;
    Symnr:integer;
    new_id:integer;
    SymSize_int:integer;
    SymSizeType:integer;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;

   { Now the Symbolnr has to be evaluated }
   gefunden:=false;
   for i:=0 to DestDocument.Symbols.Count-1 do
   begin
      MySymboldef:=DestDocument.Symbols.Items[i];
      if MySymboldef.Name = Symname then
      begin
         Symnr:=MySymboldef.Index;
         SymSizeType:=MySymbolDef.DefaultSizeType;
         SymSize_int:=round(MySymbolDef.DefaultSize * aSize);
         gefunden:=true;
         break;
      end;
   end;
   if (gefunden) then
   begin
      //do the projection
      AxGisProjection.Calculate(X,Y);
      ASymbol:=DestDocument.CreateSymbol;
      ASymbol.Symbolname:=Symname;
      ASymbol.Position.X:=round(X*100);
      ASymbol.Position.Y:=round(Y*100);
      ASymbol.SizeType:=SymSizeType;
      ASymbol.Size:=SymSize_int;
      ASymbol.Angle:=Angle * PI / 180;

      aLayer.InsertObject(ASymbol);
      new_id:=ASymbol.Index;
      DuplId:=0;
      HasDuplicate:=FALSE;

      // check if it is a duplicate
      if SetMifFileWnd.IgnoreDuplicatesCb.Checked then
      begin
         // check if the object has a duplicate
         DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(new_id, aLayer.Layername);
         if DuplId <> 0 then
         begin
            // remove the object
            DestDocument.DeleteObjectByIndex(new_id);
            new_id:=DuplId;
            HasDuplicate:=TRUE;
            NumIgnoredObjects:=NumIgnoredObjects+1;
         end;
      end;
      if (new_id <> 0) and (DuplId = 0) then
         NumSymbols:=NumSymbols+1;
   end
   else
   begin
      new_id:=InsertPoint(X,Y,HasDuplicate);
   end;
   result:=new_id;
end;

function TMainWnd.InsertText(X1:double;Y1:double;X2:double; Y2:double; aAngle:integer;Textinfo:string;aHeight:integer;font:string;TextStyle:integer;var HasDuplicate:boolean):integer;
var AText:IText;
    AIndex:integer;
    hX,hY:double;
    hAngle:integer;
    i:integer;
    fontexists:boolean;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;

   //calculate the projection
   AxGisProjection.Calculate(X1,Y1);
   AxGisProjection.Calculate(X2,Y2);

   AText:=DestDocument.CreateText;
   AText.FontStyle:=TextStyle+ts_Transparent;

   // check if font is installed
   fontexists:=FALSE;
   for i:=0 to Screen.Fonts.Count-1 do
   begin
      if Screen.Fonts[i] = Font then
      begin
         fontexists:=TRUE;
         break;
      end;
   end;

   if not fontexists then
   begin
      StatusLB.Items.Add(StrfileObj.Assign(29) + ' '+ Font +' '+ StrfileObj.Assign(30)); //Font not known ... use Courier New as default
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      Font:='Courier New'; //set default timing
   end;

   AText.Fontname:=Font;

   // calculate the projection
   AText.Position.X:=round(X1);
   AText.Position.Y:=round(X2);
   hAngle:=aAngle;
   if ((hAngle = 0) and ((X1 <> X2) or (Y1 <> Y2))) then
   begin
      hangle:=round(GetAngle(X1,Y1,X2,Y2));
   end;

   AText.Angle:=360 - hAngle;
   AText.Text:=Textinfo;
   AText.FontHeight:=aHeight;
   AText.PositionateOnLine(round(X1*100),round(Y1*100),round(X2*100),round(Y2*100));
   AText.Angle:=360 - hAngle;
   aLayer.Insertobject(AText);
   AIndex:=AText.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

   // check if it is a duplicate
   if SetMifFileWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, aLayer.Layername);
      if DuplId <> 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;

   if (AIndex <> 0) and (DuplId = 0) then
      NumTexts:=NumTexts+1;

   result:=AIndex;
end;

function TMainWnd.InsertSymbolText(X:double;Y:double; SymText:string;SymHeight:integer;Symfont:string;var HasDuplicate:boolean):integer;
var AText:IText;
    AIndex:integer;
    i:integer;
    fontexists:boolean;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;

   // check if font is installed
   fontexists:=FALSE;
   for i:=0 to Screen.Fonts.Count-1 do
   begin
      if Screen.Fonts[i] = SymFont then
      begin
         fontexists:=TRUE;
         break;
      end;
   end;

   if not fontexists then
   begin
      StatusLB.Items.Add(StrfileObj.Assign(56) + ' '+ SymFont +' '+ StrfileObj.Assign(57)); // Symbol font not known ... Symbol will be inserted as point object
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      SymFont:='Wingdings';
   end;
   AText:=DestDocument.CreateText;
   AText.Fontname:=SymFont;
   AText.FontStyle:=ts_Transparent;
   //calculate the projection
   AxGisProjection.Calculate(X,Y);
   AText.Position.X:=round(X*100);
   AText.Position.Y:=round(Y*100);
   AText.Angle:=0;
   AText.Text:=SymText;
   AText.FontHeight:=SymHeight;
   AText.PositionateOnLine(round(X*100),round(Y*100),round(X*100),round(Y*100));
   aLayer.Insertobject(AText);
   AIndex:=AText.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

      // check if it is a duplicate
   if SetMifFileWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, aLayer.Layername);
      if DuplId <> 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;

   if (AIndex <> 0) and (DuplId = 0) then
      NumSymbolTexts:=NumSymbolTexts+1;

   result:=AIndex;
end;

function TMainWnd.GetAngle(X1:double;Y1:double;X2:double;Y2:double):double;
var A,B,C:double;
    Alpha:double;
    retVal:double;
begin
   { Zuwerst werden die Kantenl�ngen des Dreiecks ermittelt }
   if (X1 = X2) and (Y1 = Y2) then
   begin
      retVal:=0;
   end
   else
   begin
      A:=ABS(Y2 - Y1); B:=ABS(X2 - X1); C:=SQRT(SQR(A) + SQR(B));
      Alpha:=(A / C);  { Nun ist der Sinus von Alpha bekannt }
      { Nun muss man noch den Invers Sinus von Alpha berechnen }
      Alpha:=ArcSin(Alpha);
      retVal:=(Alpha*180) / PI;
      if (Y2 >= Y1) then { Man befindet sich im oberen Halbkreis des Kreises }
      begin
         if (X2 < X1) then   { Man geht nach oben links   }
         begin
            retVal:=180 - retVal;
         end;
         if (X2 > X1) and (Y1 = Y2) then { Man geht gerade nach rechts }
         begin
            retVal:=360;
         end;
      end;
      if (Y2 < Y1) then { Man befindet sich im unteren Halbkreis des Kreises }
      begin
         if (X2 >= X1) then { Man geht nach rechts }
            retVal:=360 - retVal
         else               { Man geht nach links }
            retVal:=retVal + 180;
      end;
   end;

   result:=retVal;
end;

function TMainWnd.CreateEllipse(X:double;Y:double;Radius1:integer; Radius2:integer; var HasDuplicate:boolean):integer;
var ACircle:ICircle;
    APoint:IPoint;
    AIndex:integer;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;
   AxGisProjection.Calculate(X,Y);
   ACircle:=DestDocument.CreateCircle;
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   ACircle.PrimaryAxis:=Radius1*100;
   ACircle.SecondaryAxis:=Radius2*100;
   ACircle.Position:=APoint;
   aLayer.InsertObject(ACircle);
   AIndex:=ACircle.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

   // check if it is a duplicate
   if SetMifFileWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, aLayer.Layername);
      if DuplId <> 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         NumIgnoredObjects:=NumIgnoredObjects+1;
         HasDuplicate:=TRUE;
      end;
   end;
   if (AIndex <> 0) and (DuplId = 0) then
      NumCircles:=NumCircles+1;

   result:=AIndex;
end;


procedure TMainWnd.CloseTimerTimer(Sender: TObject);
begin
   MainWnd.Close;
   SetMifFileWnd.Close;
   DestDocument.FinishImpExpRoutine(ExitNormal,'IMPORTMODULE');
   CloseTimer.Enabled:=false;
end;

procedure TMainWnd.CancelClick(Sender: TObject);
begin
   if aktiv = true then
   begin
      StatusLB.Items.Add(StrfileObj.Assign(22)); { Warning: Import canceled! }
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      FileObj.CloseallFiles;
      aktiv:=false;
      Abort:=true;
      FileObj.Destroy;
      Cancel.Enabled:=false;
      Ok.Enabled:=true;
      SaveReportBtn.Enabled:=true;
      DisplayStatistics;
   end;
end;

procedure TMainWnd.OKClick(Sender: TObject);
begin
   CloseTimer.Enabled:=true;
end;

procedure TMainWnd.StartImport;
begin
   if ExitNormal then
   begin
      RunPass(1);
   end;
end;

procedure TMainWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TMainWnd.SaveReportBtnClick(Sender: TObject);
var Datei:Textfile;
    Zeile:string;
    i:integer;
begin
   if SaveDialogRep.Execute then
   begin
      {$I-}
      Assignfile(Datei,SaveDialogRep.Filename);
      Rewrite(Datei);
      for i:=0 to StatusLB.Items.Count-1 do
      begin
         Zeile:=StatusLB.items[i];
         Writeln(Datei,Zeile);
      end;
      Closefile(Datei);
      {$I+}
   end;
end;

procedure TMainWnd.FormPaint(Sender: TObject);
begin
   Self.BringtoFront;
end;

procedure TMainWnd.FormDestroy(Sender: TObject);
begin
   OptionWnd.Free;
   SymMapWnd.Free;
   StrFileObj.Destroy;
end;

procedure TMainWnd.SetupWindowTimerTimer(Sender: TObject);
var IdbProcessmask:integer;
begin
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,SetMifFileWnd.winleft,SetMifFileWnd.wintop,SetMifFileWnd.winwidth, SetMifFileWnd.winheight,SWP_SHOWWINDOW);
   MainWnd.Height:=SetMifFileWnd.winheight;
   MainWnd.Width:=SetMifFileWnd.winwidth;
   Self.Repaint;
   SetupWindowTimer.Enabled:=false;
   IdbProcessmask:=DestDocument.IdbProcessMask;
   DestDocument.IdbProcessMask:=0;
   StartImport;
   DestDocument.IdbProcessMask:=IdbProcessMask;
end;

procedure TMainWnd.FormShow(Sender: TObject);
begin
   AXImpExpDbc.Visible:=FALSE;
   AxGisProjection.visible:=FALSE;
   AxGisObjMan.visible:=FALSE;
end;

procedure TMainWnd.AXImpExpDbcDialogSetuped(Sender: TObject);
begin
   SetMifFileWnd.DestDbLabel.Caption:=AxImpExpDbc.ImpDbInfo;
   SetMifFileWnd.PageControl.Enabled:=TRUE;
   SetMifFileWnd.GeoBtn.Enabled:=TRUE;
   SetMifFileWnd.DbBtn.Enabled:=TRUE;
   SetMifFileWnd.OkBtn.Enabled:=SetMifFileWnd.OkBtnMode;
   SetMifFileWnd.CancelBtn.Enabled:=TRUE;
end;

procedure TMainWnd.AxGisProjectionWarning(Sender: TObject;
  const Info: WideString);
begin
   if not Active then
   begin
      Showmessage(Info);
   end
   else
   begin
      StatusLB.items.add(Info);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
   end;
end;

procedure TMainWnd.AxGisProjectionError(Sender: TObject;
  const Info: WideString);
begin
   if not Active then
   begin
      Showmessage(Info);
   end
   else
   begin
      StatusLB.items.add(Info);
      StatusLB.Items.Add(StrfileObj.Assign(22)); { Warning: Import canceled! }
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      FileObj.CloseallFiles;
      aktiv:=false;
      Abort:=true;
      FileObj.Destroy;
      Cancel.Enabled:=false;
      Ok.Enabled:=true;
      SaveReportBtn.Enabled:=true;
   end;
end;

procedure TMainWnd.AXImpExpDbcRequestImpLayerIndex(Sender: TObject;
  const Layername: WideString; var LayerIndex: Integer);
var i:integer;
begin
   // the Layerindex of the Layername has to be evaluated
   for i:=0 to DestDocument.Layers.Count-1 do
   begin
      if DestDocument.Layers.Items[i].Layername = Layername then
      begin
         LayerIndex:=DestDocument.Layers.Items[i].Index;
         break;
      end;
   end;
end;

procedure TMainWnd.AXImpExpDbcRequestImpProjectName(Sender: TObject;
  var ProjectName: WideString);
var ProjName:string;
begin
   ProjName:=DestDocument.Name;
   ProjectName:=ProjName;
end;

procedure TMainWnd.AXImpExpDbcRequestImpProjectGUIDs(Sender: TObject;
  var StartGUID, CurrentGUID: WideString);
var aStartGUID, aCurrentGUID:string;
begin
   aStartGUID:=DestDocument.StartProjectGUID;
   aCurrentGUID:=DestDocument.CurrentProjectGUID;
   StartGUID:=aStartGUID;
   CurrentGUID:=aCurrentGUID;
end;

procedure TMainWnd.GetSymbolRefData(Fontname:string; SymIdx:integer; var GisSymbolName:string);
begin
   FileObj.GetSymbolRefData(Fontname, SymIdx, GisSymbolName);
end;

procedure TMainWnd.SetSymbolRefData(Fontname:string; SymIdx:integer; GisSymbolName:string);
begin
   FileObj.SetSymbolRefData(Fontname, SymIdx, GisSymbolName);
end;

procedure TMainWnd.ResetCurrentSymbolRef;
begin
   FileObj.ResetCurrentSymbolRef;
end;

function TMainWnd.GetCurrentSymbolRefData(var Fontname:string; var SymIdx:integer; var GisSymbolName:string):boolean;
begin
   result:=FileObj.GetCurrentSymbolRefData(Fontname, SymIdx, GisSymbolName);
end;

function TMainWnd.GetSymbolRefDataByIdx(Index:integer):p_SymbolRef_record;
begin
   result:=FileObj.GetSymbolRefDataByIdx(Index);
end;

procedure TMainWnd.DisplayStatistics;
var aDate, aTime, DispTime, DispDate:string;
    EndTime, UsedTime:TDateTime;
begin
   StatusLB.Items.Add('---------------------------------------------------');
   // display end time for this file
   aDate:=FormatDateTime('ddmmyy',now);
   DispDate:=copy(aDate,1,2)+'.'+copy(aDate,3,2)+'.'+copy(aDate,5,2);
   aTime:=Formatdatetime('hhnnss',now);
   DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
   StatusLB.Items.Add(StrFileObj.Assign(67) + DispDate+' / '+DispTime); // end time:

   // display number of imported files
   StatusLb.Items.Add(StrFileObj.Assign(68)+inttostr(FilesProcessed));
   // display ellapsed time
   EndTime:=Now;
   UsedTime:=EndTime-OverallStartTime;
   aTime:=Formatdatetime('hhnnss',UsedTime);
   DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
   StatusLB.Items.Add(StrFileObj.Assign(69) + DispTime); // overall used time:

   // display imported data statistics
   if NumLayers      > 0 then StatusLB.Items.Add(StrFileObj.Assign(70) + inttostr(NumLayers));       // created layers:
   if NumSymbols     > 0 then StatusLB.Items.Add(StrFileObj.Assign(71) + inttostr(NumSymbols));      // created symbols:
   if NumSymbolTexts > 0 then StatusLB.Items.Add(StrFileObj.Assign(72) + inttostr(NumSymbolTexts));  // created symbol texts:
   if NumPolys       > 0 then StatusLB.Items.Add(StrFileObj.Assign(73) + inttostr(NumPolys));        // created polylines:
   if NumCPolys      > 0 then StatusLB.Items.Add(StrFileObj.Assign(74) + inttostr(NumCPolys));       // created polygons:
   if NumIslands     > 0 then StatusLB.Items.Add(StrFileObj.Assign(75) + inttostr(NumIslands));      // created islands:
   if NumCircles     > 0 then StatusLB.Items.Add(StrFileObj.Assign(76) + inttostr(NumCircles));      // created circles:
   if NumTexts       > 0 then StatusLB.Items.Add(StrFileObj.Assign(77) + inttostr(NumTexts));        // created texts:
   if NumPixels      > 0 then StatusLB.Items.Add(StrFileObj.Assign(78) + inttostr(NumPixels));       // created points:
   if NumIgnoredObjects > 0 then StatusLB.Items.Add(StrFileObj.Assign(79) + inttostr(NumIgnoredObjects)); // number of ignored duplicates:
   StatusLB.Items.Add('---------------------------------------------------');
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;
end;

end.
