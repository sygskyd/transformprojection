object MainWnd: TMainWnd
  Left = 219
  Top = 172
  BorderStyle = bsDialog
  Caption = 'MAPINFO MIF Import 2000'
  ClientHeight = 351
  ClientWidth = 458
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'System'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnPaint = FormPaint
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 35
    Top = 24
    Width = 4
    Height = 16
  end
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 441
    Height = 305
    Caption = 'Panel1'
    TabOrder = 0
    object Label2: TLabel
      Left = 8
      Top = 8
      Width = 88
      Height = 13
      Caption = 'Progressmessages'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Prozessbalken: TGauge
      Left = 8
      Top = 256
      Width = 425
      Height = 20
      Progress = 0
    end
    object StatusLB: TListBox
      Left = 8
      Top = 24
      Width = 425
      Height = 193
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
    end
    object SaveReportBtn: TButton
      Left = 152
      Top = 224
      Width = 145
      Height = 25
      Caption = 'Save report'
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = SaveReportBtnClick
    end
  end
  object OK: TButton
    Left = 240
    Top = 320
    Width = 93
    Height = 23
    Caption = 'OK'
    Default = True
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 1
    OnClick = OKClick
  end
  object Cancel: TButton
    Left = 348
    Top = 320
    Width = 101
    Height = 23
    Cancel = True
    Caption = 'Abbrechen'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 2
    OnClick = CancelClick
  end
  object AXImpExpDbc: TAXImpExpDbc
    Left = 8
    Top = 320
    Width = 50
    Height = 25
    ParentFont = False
    TabOrder = 3
    OnDialogSetuped = AXImpExpDbcDialogSetuped
    OnRequestImpLayerIndex = AXImpExpDbcRequestImpLayerIndex
    OnRequestImpProjectName = AXImpExpDbcRequestImpProjectName
    OnRequestImpProjectGUIDs = AXImpExpDbcRequestImpProjectGUIDs
    ControlData = {
      54504630065450616E656C00044C656674020803546F70034001055769647468
      02320648656967687402190743617074696F6E06064769734462630000}
  end
  object AxGisProjection: TAxGisProjection
    Left = 64
    Top = 320
    Width = 50
    Height = 25
    ParentFont = False
    TabOrder = 4
    OnWarning = AxGisProjectionWarning
    OnError = AxGisProjectionError
    ControlData = {
      54504630065450616E656C00044C656674024003546F70034001055769647468
      02320648656967687402190743617074696F6E060647697350726F0000}
  end
  object AXGisObjMan: TAXGisObjMan
    Left = 120
    Top = 320
    Width = 50
    Height = 25
    ParentFont = False
    TabOrder = 5
    ControlData = {
      54504630065450616E656C00044C656674027803546F70034001055769647468
      02320648656967687402190743617074696F6E06064769734D616E0000}
  end
  object CloseTimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = CloseTimerTimer
    Left = 224
    Top = 312
  end
  object SaveDialogRep: TSaveDialog
    DefaultExt = '*.rep'
    FileName = '*.rep'
    Filter = 'Import report file|*.rep'
    Left = 128
    Top = 232
  end
  object SetupWindowTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = SetupWindowTimerTimer
    Left = 192
    Top = 312
  end
end
