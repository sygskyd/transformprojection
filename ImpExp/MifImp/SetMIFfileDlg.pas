unit SetmiffileDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, FileCtrl, ComCtrls, StrFile, ShellCtrls,
  FileSelectionPanel, WinOSInfo;

type

  TSetmiffileWnd = class(TForm)
    PageControl: TPageControl;
    TabSheetFiles: TTabSheet;
    OKBtn: TButton;
    CancelBtn: TButton;
    SelectivCB: TCheckBox;
    GeoBtn: TButton;
    SetupWindowTimer: TTimer;
    Panel1: TPanel;
    DestDbLabel: TLabel;
    DbBtn: TButton;
    IgnoreDuplicatesCb: TCheckBox;
    FileSelectionPanel: TFileSelectionPanel;
    ShellTreeView: TShellTreeView;
    FileListBox: TFileListBox;
    ListBox: TListBox;
    TrashPanel: TPanel;
    TrashImage: TImage;
    AddAllBtn: TButton;
    DeleteAllBtn: TButton;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GeoBtnClick(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure DbBtnClick(Sender: TObject);
    procedure FileSelectionPanelFilesToConvertChanged(Sender: TObject);
  private
    { Private-Deklarationen}
    StrFileObj :ClassStrfile;
    firstactivate:boolean;
    CurrentFileIdx:integer;
    procedure CheckWindow;
  public
    { Public-Deklarationen}
    winheight:integer;
    winwidth :integer;
    wintop   :integer;
    winleft  :integer;
    OkBtnMode:boolean;
    function  GetFilename(var Dateiname:string):boolean;
    procedure CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
    procedure SetupForBatch;
    procedure SetImpSettings(ImpMode:integer);
    procedure AddImpFile(aFilename:string);
  end;

var
  SetmiffileWnd: TSetmiffileWnd;

implementation

uses MainDlg,inifiles;

{$R *.DFM}

function TSetmiffileWnd.GetFilename(var Dateiname:string):boolean;
var retVal:boolean;
begin
   retVal:=TRUE;
   if CurrentFileIdx < FileSelectionPanel.NumFilesToConvert then
      Dateiname:=FileSelectionPanel.GetFilename(CurrentFileIdx)
   else
      retVal:=FALSE;
   result:=retVal;
   CurrentFileIdx:=CurrentFileIdx+1;
end;

procedure TSetmiffileWnd.FormCreate(Sender: TObject);
var myScale:double;
    oldHeight:integer;
    oldWidth:integer;
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   Self.Caption:=StrFileObj.Assign(1);                // MAPINFO-MIF Import 2000
   OKBtn.Caption:=StrFileObj.Assign(4);               // Ok
   CancelBtn.Caption:=StrFileObj.Assign(5);           // Cancel
   SelectivCB.Caption:=StrFileObj.Assign(6);          // convert selective
   TabSheetFiles.Caption:=StrFileObj.Assign(7);       // Files
   GeoBtn.Caption:=StrFileObj.Assign(12);             // Projection
   DbBtn.Caption:=StrFileObj.Assign(8);               // Database
   IgnoreDuplicatesCb.Caption:=StrFileObj.Assign(66); // ignore already existing objects
   FirstActivate:=true;
   CurrentFileIdx:=0;

   oldHeight:=Self.Height;
   oldWidth:=Self.Width;

   Self.Height:=1;
   Self.Width:=1;

   myScale:=1; //100000 / Trunc(9600000/(Self.PixelsPerInch));
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
   
   // now adapt winheigt and winwidth
   winheight:=round(oldHeight*myScale);
   winwidth:=round(oldWidth*myScale);
end;

procedure TSetmiffileWnd.OKBtnClick(Sender: TObject);
var inifile:TIniFile;
begin
   MainWnd.ExitNormal:=true;

   // write current settings to the ConGIS.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir + 'ConGIS.ini');
   inifile.WriteString('MIFIMP','ImpDbMode',inttostr(MainWnd.AXImpExpDbc.ImpDbMode));
   inifile.WriteString('MIFIMP','ImpDatabase',MainWnd.AXImpExpDbc.ImpDatabase);
   inifile.WriteString('MIFIMP','ImpTable',MainWnd.AXImpExpDbc.ImpTable);

   inifile.WriteString('MIFIMP','SourceCoordinateSystem',inttostr(MainWnd.AxGisProjection.SourceCoordinateSystem));
   inifile.WriteString('MIFIMP','SourceProjSettings',MainWnd.AxGisProjection.SourceProjSettings);
   inifile.WriteString('MIFIMP','SourceProjection',MainWnd.AxGisProjection.SourceProjection);
   inifile.WriteString('MIFIMP','SourceDate',MainWnd.AxGisProjection.SourceDate);
   inifile.WriteString('MIFIMP','SourceXOffset',floattostr(MainWnd.AxGisProjection.SourceXOffset));
   inifile.WriteString('MIFIMP','SourceYOffset',floattostr(MainWnd.AxGisProjection.SourceYOffset));
   inifile.WriteString('MIFIMP','SourceScale',floattostr(MainWnd.AxGisProjection.SourceScale));

   inifile.WriteString('MIFIMP','LastUsedPath',FileSelectionPanel.Directory);
   if IgnoreDuplicatesCb.Checked then
      inifile.WriteString('MIFIMP','IgnoreExistingObjects','T')
   else
      inifile.WriteString('MIFIMP','IgnoreExistingObjects','F');

   inifile.Destroy;

   // now the import window can be opened and the import executed
   Self.Close;
   MainWnd.Show;
end;

procedure TSetmiffileWnd.CancelBtnClick(Sender: TObject);
begin
   MainWnd.ExitNormal:=false;
   MainWnd.CloseTimer.Enabled:=true;
end;

procedure TSetmiffileWnd.CheckWindow;
var allok:boolean;
begin
   allok:=true;
   if FileSelectionPanel.NumFilesToConvert = 0 then allok:=false;
   if allok then OkBtn.Enabled:=true
   else OkBtn.Enabled:=false;
end;

procedure TSetmiffileWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TSetmiffileWnd.GeoBtnClick(Sender: TObject);
begin
   MainWnd.AxGisProjection.SetupByDialog;
end;

procedure TSetMifFileWnd.CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
begin
   wintop:=(WHEIGHT div 2) - (winheight div 2);
   winleft:=(WWIDTH div 2) - (winwidth div 2);
end;

procedure TSetmiffileWnd.SetupWindowTimerTimer(Sender: TObject);
var dummy:string;
    intval, count:integer;
    doubleval:double;
    inifile:TIniFile;
begin
   SetupWindowTimer.Enabled:=false;
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,winleft,wintop,winwidth, winheight,SWP_SHOWWINDOW);
   Self.Height:=winheight;
   Self.Width:=winwidth;

   // Setup the window from the ConGIS.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'ConGIS.ini');
   dummy:=MainWnd.DestDocument.Name;
   dummy:=ChangeFileExt(dummy,'.wgi');
   if FileExists(dummy) then
   begin
      MainWnd.AXImpExpDbc.ImpDbMode:=5; // Idb-Database
      MainWnd.AXImpExpDbc.ImpDatabase:='';
      MainWnd.AXImpExpDbc.ImpTable:='';
   end
   else
   begin
      dummy:=inifile.ReadString('MIFIMP','ImpDbMode','0');
      val(dummy, intval, count);
      if intval <> 5 then // Idb-Database
      begin
         MainWnd.AXImpExpDbc.ImpDbMode:=intval;
         dummy:=inifile.ReadString('MIFIMP','ImpDatabase','');
         MainWnd.AXImpExpDbc.ImpDatabase:=dummy;
         dummy:=inifile.ReadString('MIFIMP','ImpTable','');
         MainWnd.AXImpExpDbc.ImpTable:=dummy;
      end;
   end;
   DestDbLabel.Caption:=MainWnd.AXImpExpDbc.ImpDbInfo;

   // setup the projection
   dummy:=inifile.ReadString('MIFIMP','SourceCoordinateSystem','');
   if dummy <> '' then
   begin
      val(dummy, intval, count);
      MainWnd.AxGisProjection.SourceCoordinateSystem:=intval;
      dummy:=inifile.ReadString('MIFIMP','SourceProjSettings','NONE');
      MainWnd.AxGisProjection.SourceProjSettings:=dummy;
      dummy:=inifile.ReadString('MIFIMP','SourceProjection','NONE');
      MainWnd.AxGisProjection.SourceProjection:=dummy;
      dummy:=inifile.ReadString('MIFIMP','SourceDate','NONE');
      MainWnd.AxGisProjection.SourceDate:=dummy;
      dummy:=inifile.ReadString('MIFIMP','SourceXOffset','0');
      val(dummy, doubleval, count);
      MainWnd.AxGisProjection.SourceXOffset:=doubleval;
      dummy:=inifile.ReadString('MIFIMP','SourceYOffset','0');
      val(dummy, doubleval, count);
      MainWnd.AxGisProjection.SourceYOffset:=doubleval;
      dummy:=inifile.ReadString('MIFIMP','SourceScale','1');
      val(dummy, doubleval, count);
      MainWnd.AxGisProjection.SourceScale:=doubleval;
   end;

   dummy:=inifile.ReadString('MIFIMP','LastUsedPath','');
   if dummy = '' then
      GetDir(0, dummy);
   FileSelectionPanel.Directory:=dummy;

   dummy:=inifile.ReadString('MIFIMP','IgnoreExistingObjects','F');
   if dummy = 'T' then IgnoreDuplicatesCb.Checked:=TRUE else IgnoreDuplicatesCb.Checked:=FALSE;

   inifile.Destroy;


   Self.Repaint;
end;

// procedure is used to setup the SetmiffileWnd for batch mode
procedure TSetmiffileWnd.SetupForBatch;
begin
   if FirstActivate then
   begin
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
   end;
end;

procedure TSetmiffileWnd.SetImpSettings(ImpMode:integer);
begin
   if (ImpMode = 0) then
      SelectivCB.Checked:=FALSE;
   if (ImpMode = 1) then
      SelectivCB.Checked:=TRUE;
end;

procedure TSetmiffileWnd.AddImpFile(aFilename:string);
begin
   FileSelectionPanel.AddFileToConvert(aFilename);
end;

procedure TSetmiffileWnd.DbBtnClick(Sender: TObject);
begin
   MainWnd.AXImpExpDbc.SetupByDialog(Self.Handle, 5, 5);
   PageControl.Enabled:=FALSE;
   GeoBtn.Enabled:=FALSE;
   DbBtn.Enabled:=FALSE;
   OkBtnMode:=OkBtn.Enabled;
   OkBtn.Enabled:=FALSE;
   CancelBtn.Enabled:=FALSE;
end;

procedure TSetmiffileWnd.FileSelectionPanelFilesToConvertChanged(
  Sender: TObject);
begin
   CheckWindow;
end;

end.
