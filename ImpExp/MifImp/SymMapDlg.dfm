object SymMapWnd: TSymMapWnd
  Left = 275
  Top = 188
  Width = 267
  Height = 298
  Caption = 'Symbol mapping'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 39
    Height = 13
    Caption = 'MapInfo'
  end
  object Label2: TLabel
    Left = 64
    Top = 8
    Width = 37
    Height = 13
    Caption = 'WinGIS'
  end
  object MifIdxLb: TListBox
    Left = 8
    Top = 24
    Width = 41
    Height = 177
    ItemHeight = 13
    TabOrder = 0
    OnClick = MifIdxLbClick
  end
  object Panel1: TPanel
    Left = 8
    Top = 200
    Width = 41
    Height = 25
    TabOrder = 1
  end
  object PageControl1: TPageControl
    Left = 64
    Top = 24
    Width = 177
    Height = 201
    ActivePage = TextTabSheet
    TabIndex = 0
    TabOrder = 2
    object TextTabSheet: TTabSheet
      Caption = 'Text'
      object FontLb: TListBox
        Left = 0
        Top = 0
        Width = 169
        Height = 169
        ItemHeight = 13
        TabOrder = 0
        OnClick = FontLbClick
      end
    end
    object SymbolTabSheet: TTabSheet
      Caption = 'Symbol'
      ImageIndex = 1
      object SymbolLb: TListBox
        Left = 0
        Top = 0
        Width = 169
        Height = 169
        ItemHeight = 13
        TabOrder = 0
        OnClick = SymbolLbClick
      end
    end
  end
  object OKBtn: TButton
    Left = 150
    Top = 230
    Width = 93
    Height = 23
    Caption = 'Ok'
    Default = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 3
  end
end
