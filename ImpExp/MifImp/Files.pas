unit Files;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Math, Struct, Db;

const
       Col_Int      = 0;
       Col_SmallInt = 1;
       Col_Float    = 2;
       Col_Date     = 3;
       Col_String   = 4;

      lt_Solid     = 0;
      lt_Dash      = 1;
      lt_Dot       = 2;
      lt_DashDot   = 3;
      lt_DashDDot  = 4;

      lt_UserDefined    = 1000;    { number of first user-defined linestyle }

      pt_NoPattern = 0;
      pt_FDiagonal = 1;
      pt_Cross     = 2;
      pt_DiagCross = 3;
      pt_BDiagonal = 4;
      pt_Horizontal= 5;
      pt_Vertical  = 6;
      pt_Solid     = 7;

      pt_UserDefined = 8;

type

ClassFile = class
   public
      { public Definitionen }
      Linienstil,Linienfarbe:longint;
      Flaechenstil,Flaechenfarbe:longint;

      constructor Create;
      procedure Destroy;
      function GetFilesize(Dateiname:string):longint;
      procedure OpenMIFFile(Dateiname:string);
      procedure CloseAllFiles;

      function  ReadElement:longint;

      function  GetRegionElements:longint;
      function  GetPLineElements:longint;
      function  GetPointElements:longint;
      function  GetEllipseElements:longint;
      function  GetTextElements:longint;

      function  WriteCurrentPLine:longint;
      function  WriteCurrentPoint:longint;
      function  WriteCurrentRegion:longint;
      function  WriteCurrentEllipse:longint;
      function  WriteCurrentText   :longint;

      procedure WriteLayerDefinition;

      procedure CreateDatabase(Layer:string);

      function  Check_odoa(Dateiname:string):boolean;
      function  to_odoa:longint;

      procedure OpenFile(Dateiname:string);
      procedure Closefiles;
      procedure GetSymbolRefData(Fontname:string; SymIdx:integer; var GisSymbolName:string);    // method to get the name of the Gis-Symbol by Font and Index
      procedure SetSymbolRefData(Fontname:string; SymIdx:integer; GisSymbolName:string);        // method to set the name of the Gis-Symbol by Font and Index
      function  CheckSymbolsExist:boolean;
      procedure ResetCurrentSymbolRef;
      function  GetCurrentSymbolRefData(var Fontname:string; var SymIdx:integer; var GisSymbolName:string):boolean;
      function  GetSymbolRefDataByIdx(Index:integer):p_SymbolRef_record;
   private
      { private Definitionen }
      function ReadDelimiter(Zeile:string):longint;
      function ReadColumns(Zeile:string):longint;
      function ReadRegion(Zeile:string):longint;
      function ReadPLine(Zeile:string):longint;
      function ReadPen(Zeile:string):longint;
      function ReadBrush(Zeile:string):longint;
      function ReadCenter(Zeile:string):longint;
      function ReadPoint(Zeile:string):longint;
      function ReadLine(Zeile:string):longint;
      function ReadRect(Zeile:string):longint;
      function ReadEllipse(Zeile:string):longint;
      function ReadText(Zeile:string):longint;
      function ReadAngle(Zeile:string):longint;
      function ReadSymbol(Zeile:string):longint;

      procedure NewRegionLabel;
      procedure NewPLineLabel;
      procedure NewPointLabel;
      procedure NewEllipseLabel;

      function GetMifLine(var Zeile:string):longint;
      function GetMidLine:string;
      function FormatText(Text:string):string;
      procedure Convert2Dos(var Zeile:string);

      function ParseString(Source:string;Pos:integer):string;
      function ExtractData(Text:string;Pos :integer;Delimiter:char):string;
end;
implementation
uses MainDlg,OptionDlg,SetmiffileDlg, Strfile;

var  MIFDatei  :Textfile;
     MIDDatei  :Textfile;
     OLDDBF    :boolean;

     Delimiter :char;
     LastObject:string;

     DatenbankObj   :ClassDatenbank;
     RegionObj      :ClassRegion;
     PLineObj       :ClassPLine;
     PointObj       :ClassPoint;
     EllipseObj     :ClassEllipse;
     TextObj        :ClassText;
     SymbolRefObj   :ClassSymbolRef;

     Layername      :string;
     Bytesrest_anz  :integer;
     Zeile          :string;
     inFile         :file;
     outFile        :Textfile;
     to_odoa_flag   :boolean;
     to_odoa_filename:string;
     Restbytes      :array [1..5080] of char;

     StrfileObj     :ClassStrfile;

constructor ClassFile.Create;
begin
   DatenbankObj:=ClassDatenbank.Create;
   RegionObj:=ClassRegion.Create;
   PLineObj:=ClassPLine.Create;
   PointObj:=ClassPoint.Create;
   EllipseObj:=ClassEllipse.Create;
   TextObj:=ClassText.Create;
   SymbolRefObj:=ClassSymbolRef.Create;

   OldDBF:=false;
   Linienstil:=lt_Solid; Linienfarbe:=0;
   Flaechenstil:=pt_NoPattern; Flaechenfarbe:=0;
   LastObject:='';
end;

procedure ClassFile.Destroy;
begin
   DatenbankObj.Destroy;
   RegionObj.Destroy;
   PLineObj.Destroy;
   PointObj.Destroy;
   EllipseObj.Destroy;
   TextObj.Destroy;
   SymbolRefObj.Destroy;
end;

{*******************************************************************************
* PROZEDUR : OpenMIFFile                                                       *
********************************************************************************
* Prozedur dient zum �ffnen des MIF-files.                                     *
*                                                                              *
* PARAMETER: Dateiname -> Dateiname des MIF-Files.                             *
*                                                                              *
********************************************************************************}
procedure ClassFile.OpenMIFFile(Dateiname:string);
begin
   {$i-} Assignfile(MIFDatei,Dateiname); Reset(MIFDatei); {$i+}

   if Fileexists(Changefileext(Dateiname,'.MID')) then
   begin
      MainWnd.MIDOK:=true;
      {$i-} Assignfile(MIDDatei,Changefileext(Dateiname,'.MID')); Reset(MIDDatei); {$i+}
   end
   else
      MainWnd.MIDOK:=false;

   { Der Layer, auf den die Daten kommen heisst gleich wie der Dateiname }
   Layername:=Extractfilename(Dateiname); Layername:=copy(Layername,1,pos('.',Layername)-1);
   DatenbankObj.InsertColumn('LAYERNAME',length(Layername),Col_String,Layername);
end;

{*******************************************************************************
* PROZEDUR : OpenFile                                                          *
********************************************************************************
* Prozedur dient zum �ffnen eine E00-Files, das sich im im UNIX-Format befindet*
*                                                                              *
* PARAMETER: Dateiname -> Dateiname des E00-Files.                             *
*                                                                              *
********************************************************************************}
procedure ClassFile.OpenFile(Dateiname:string);
begin
   {$i-} Assignfile(InFile,Dateiname); Reset(InFile,1); {$i+}
   {$i-} Assignfile(OutFile,Changefileext(Dateiname,'.tmp')); Rewrite(OutFile); {$i+}
   Zeile:=''; Bytesrest_anz:=0;
end;

{*******************************************************************************
* PROZEDUR : Closefile                                                         *
********************************************************************************
* Prozedur dient zum schliessen der beiden Dateien, die f�r die DOS nach       *
* UNIX Konvertierung ben�tigt wurden.                                          *
*                                                                              *
********************************************************************************}
procedure ClassFile.Closefiles;
begin
   {$i-} Closefile(inFile); Closefile(outFile); {$i+}
end;


{*******************************************************************************
* FUNKTION : Check_odoa                                                        *
********************************************************************************
* Funktion �berpr�ft, ob die Datei vom UNIX ins DOS-Format konvertiert werden  *
* muss.                                                                        *
*                                                                              *
* PARAMETER: Dateiname -> Name der zu pr�fenden Datei                          *
*                                                                              *
* R�CKGABEWERT: TRUE -> wenn konvertiert werden muss, sonst FALSE              *
********************************************************************************}
function ClassFile.Check_odoa(Dateiname:string):boolean;
var Datei:file;
    Actualbytes:array [1..256] of char;
    Bytesread,i:integer;
begin
   Filemode:=0;
   {$i-}
   Assignfile(Datei,Dateiname); Reset(Datei,1);
   {$i+}
   { Einlesen eines 256 Byte Blockes }
   {$i-}Blockread(Datei,Actualbytes,256,Bytesread);{$i+}

   to_odoa_flag:=true;

   if Bytesread > 100 then Bytesread:=100; { Es werden nur

   { Nun wird kontrolliert, ob sich ein <CR> in diesem Buffer befindet }
   for i:=1 to Bytesread do
   begin
      if Actualbytes[i] = CHR(13) then to_odoa_flag:=false;
   end;

   {$i-} Closefile(Datei); {$i+}
   Bytesrest_anz:=0;
   to_odoa_filename:=Changefileext(Dateiname,'.tmp');
   Result:=to_odoa_flag;
end;

{*******************************************************************************
* PROZEDUR : CloseallFiles                                                     *
********************************************************************************
* Prozedur dient zum Schliessen aller Files.                                   *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassFile.CloseallFiles;
var hpchar:pchar;
begin
   hpchar:=stralloc(255);
   {$i-}
   Closefile(MIFDatei);
   {$i+}

   if MainWnd.MIDOK then begin {$i-} Closefile(MIDDatei); {$i+} end;
   MainWnd.AXImpExpDbc.CloseImpTables;
   strpcopy(hpchar,to_odoa_filename);
   if to_odoa_flag = true then Deletefile(hpchar);
   strdispose(hpchar);
end;

{*******************************************************************************
* FUNKTION : ClassFile.GetFilesize                                             *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Bytes des       *
* MIF-Files.                                                                   *
*                                                                              *
* PARAMETER: Dateiname -> Dateiname von der die Bytegr��e ermittelt werden soll*
*                                                                              *
* R�ckgabewert: Anzahl der zu Bearbeitenden Bytes.                             *
********************************************************************************}
function ClassFile.GetFilesize(Dateiname:string):longint;
var Datei:file of byte;
begin
   {$i-}
   AssignFile(Datei, Dateiname);
   {$i+}
   Filemode:=0;
   {$i-}
   Reset(Datei); Result:=FileSize(Datei); Closefile(Datei);
   {$i+}
end;

{*******************************************************************************
* FUNKTION : ClassFile.GetRegionelements                                       *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Region Elemente *
* MIF-Files.                                                                   *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden Region Elemente.                   *
********************************************************************************}
function Classfile.GetRegionElements:longint;
begin
   Result:=RegionObj.GetAnzahl;
   RegionObj.ResetCurrent;
end;

{*******************************************************************************
* FUNKTION : ClassFile.GetPlineelements                                        *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden PLine  Elemente *
* MIF-Files.                                                                   *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden PLine  Elemente.                   *
********************************************************************************}
function Classfile.GetPlineElements:longint;
begin
   Result:=PLineObj.GetAnzahl;
   PlineObj.ResetCurrent;
end;

{*******************************************************************************
* FUNKTION : ClassFile.GetPointElements                                        *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Point  Elemente *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden Point  Elemente.                   *
********************************************************************************}
function Classfile.GetPointElements:longint;
begin
   Result:=PointObj.GetAnzahl;
   PointObj.ResetCurrent;
end;

{*******************************************************************************
* FUNKTION : ClassFile.GetTextElements                                         *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Text Elemente   *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden Text Elemente.                     *
********************************************************************************}
function Classfile.GetTextElements:longint;
begin
   Result:=TextObj.GetAnzahl;
   TextObj.ResetCurrent;
end;

{*******************************************************************************
* FUNKTION : ClassFile.GetEllipseElements                                      *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Ellipse Elemente*
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden Ellipse Elemente.                  *
********************************************************************************}
function Classfile.GetEllipseElements:longint;
begin
   Result:=EllipseObj.GetAnzahl;
   EllipseObj.ResetCurrent;
end;

{*******************************************************************************
* FUNKTION : ClassFile.WriteCurrentRegion                                      *
********************************************************************************
* Funktion dient zum Schreiben des aktuellen Region Objekts.                   *
*                                                                              *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Region Elemente.                       *
********************************************************************************}
function Classfile.WriteCurrentRegion:longint;
begin
   RegionObj.WriteCurrent;
   RegionObj.SetCurrentNext;
   Result:=1;
end;

{*******************************************************************************
* FUNKTION : ClassFile.WriteCurrentText                                        *
********************************************************************************
* Funktion dient zum Schreiben des aktuellen Text Objekts.                     *
*                                                                              *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Text Elemente.                         *
********************************************************************************}
function Classfile.WriteCurrentText:longint;
begin
   TextObj.WriteCurrent;
   TextObj.SetCurrentNext;
   Result:=1;
end;

{*******************************************************************************
* FUNKTION : ClassFile.WriteCurrentEllipse                                     *
********************************************************************************
* Funktion dient zum Schreiben des aktuellen Ellipse Objekts.                  *
*                                                                              *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Ellipse Elemente.                      *
********************************************************************************}
function Classfile.WriteCurrentEllipse:longint;
begin
   EllipseObj.WriteCurrent;
   EllipseObj.SetCurrentNext;
   Result:=1;
end;

{*******************************************************************************
* FUNKTION : ClassFile.WriteCurrentPoint                                       *
********************************************************************************
* Funktion dient zum Schreiben des aktuellen Point  Objekts.                   *
*                                                                              *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Point Elemente.                        *
********************************************************************************}
function Classfile.WriteCurrentPoint:longint;
begin
   PointObj.WriteCurrent;
   PointObj.SetCurrentNext;
   Result:=1;
end;

{*******************************************************************************
* FUNKTION : ClassFile.WriteCurrentPline                                       *
********************************************************************************
* Funktion dient zum Schreiben des aktuellen Pline  Objekts.                   *
*                                                                              *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden PLine  Elemente.                   *
********************************************************************************}
function Classfile.WriteCurrentPLine:longint;
begin
   PLineObj.WriteCurrent;
   PLineObj.SetCurrentNext;
   Result:=1;
end;

{*******************************************************************************
* FUNKTION : ClassFile.to_odoa                                                 *
********************************************************************************
* Funktion dient zum Konvertieren des MIF - Files ins DOS-Format.              *
*                                                                              *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Bytes                                  *
********************************************************************************}
function Classfile.to_odoa:longint;
var Actualbytes    :array [1..5080] of char;
    Hilfsbytes     :array [1..5080] of char;
    Bytesread      :integer;
    counter2       :integer;
    Zeile          :string;
    i,j            :integer;
begin
   Zeile:='';

   { Einlesen eines 4096 Byte Blockes }
   {$i-} Blockread(InFile,Actualbytes,4096,Bytesread); {$i+}

   { Rest des vorigen Blockes mu� ber�cksichtigt werden }
   if (Bytesrest_anz <> 0) then
   begin
      { Zeile mu� vor Actualbytes kopiert werden }
      for i:=1 to Bytesrest_anz do Hilfsbytes[i]:=Restbytes[i];

      for j:=1 to Bytesread do
      begin
         Hilfsbytes[i]:=Actualbytes[j]; i:=i + 1;
      end;
      Bytesread:=Bytesread + Bytesrest_anz;
      for i:=1 to Bytesread do Actualbytes[i]:=Hilfsbytes[i];
      Bytesrest_anz:=0;
   end;

   counter2:=1;
   { Nun werden die durch Chr-10 getrennten Zeilen, die sich im Buffer befinden ermittelt }
   repeat
      if Actualbytes[counter2] = CHR(10) then
      begin
         {$i-} writeln(OutFile,Zeile); {$i+}
         counter2:=counter2 + 1;
         Bytesrest_anz:=0;
         Zeile:='';
      end
      else
      begin
         Zeile:=Zeile + Actualbytes[counter2];
         Bytesrest_anz:=Bytesrest_anz + 1;
         Restbytes[Bytesrest_anz]:=Actualbytes[counter2];
         counter2:=counter2 + 1;
      end;
   until counter2 = Bytesread + 1;

   Result:=Bytesread;
end;

{*******************************************************************************
* PROZEDUR : ClassFile.WriteLayerDefinition                                    *
********************************************************************************
* Prozedur schreibt die Layerdefinitionszeile in die ASC-Datei                 *
*                                                                              *
* PARANETER: Keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassFile.WriteLayerDefinition;
var Laenge:integer;
    Zeile,hZeile:string;
begin
   Layername:=DatenbankObj.GetItem('LAYERNAME');
   if MainWnd.CheckLayerExists(Layername) = FALSE then
      MainWnd.CreateLayer(Layername);

   MainWnd.SetLayerPriorities(Linienstil,Linienfarbe,Flaechenstil,Flaechenfarbe);
end;

procedure ClassFile.CreateDatabase(Layer:string);
var Laenge,i,DBAnz:integer;
    Bezeichnung:string;
    ColType:integer;
begin
   MainWnd.AXImpExpDbc.CheckAndCreateImpTable(Layer);
   MainWnd.AXImpExpDbc.AddImpIdColumn(Layer);
   DBAnz:=DatenbankObj.SetDBFirst;
   if DBAnz > 0 then
   begin
      for i := 1 to DBAnz do { Einf�gen der Spaltennamen in die Datenbankliste }
      begin
         Bezeichnung:=DatenbankObj.GetDBColumnname; DatenbankObj.SetCurrentnext;
         Laenge:=DatenbankObj.GetLength(Bezeichnung);
         ColType:=DatenbankObj.GetType(Bezeichnung);
         MainWnd.AXImpExpDbc.AddImpColumn(Layer,Bezeichnung,ColType,Laenge);
      end;
   end;
   MainWnd.AXImpExpDbc.CreateImpTable(Layer);
   MainWnd.AXImpExpDbc.OpenImpTable(Layer);
end;

{*******************************************************************************
* FUNKTION : ReadElement                                                       *
********************************************************************************
* Funktion dient zum einlesen eines Shapefile-Objekts.                         *
*                                                                              *
* PARAMETER: Art wie die Datei gelesen werden soll (Selektiv, nicht Selektiv)  *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Bytes.                                 *
********************************************************************************}
function ClassFile.ReadElement:longint;
var Zeile:string;
    anz  :longint;
    MifTyp:string;
begin
   { Hier werden die Labels gesetzt }
   if LastObject = 'ROUNDRECT' then NewRegionLabel;
   if LastObject = 'PLINE'     then NewPLineLabel;
   if LastObject = 'LINE'      then NewPLineLabel;
   if LastObject = 'POINT'     then NewPointLabel;
   if LastObject = 'ELLIPSE'   then NewEllipseLabel;

   anz:=GetMIFLine(Zeile); MifTyp:=ExtractData(Zeile,1,' '); MifTyp:=strupper(PCHAR(MifTyp));
   if MifTyp = 'DELIMITER' then begin anz:=anz + ReadDelimiter(Zeile); LastObject:=MifTyp; end else
   if MifTyp = 'COLUMNS'   then begin anz:=anz + ReadColumns(Zeile);LastObject:=MifTyp;    end else
   if MifTyp = 'REGION'    then begin anz:=anz + ReadRegion(Zeile);LastObject:=MifTyp;     end else
   if MifTyp = 'POINT'     then begin anz:=anz + ReadPoint(Zeile); LastObject:=MifTyp;     end else
   if MifTyp = 'LINE'      then begin anz:=anz + ReadLine(Zeile); LastObject:=MifTyp;      end else
   if MifTyp = 'PLINE'     then begin anz:=anz + ReadPLine(Zeile);LastObject:=MifTyp;      end else
   if MifTyp = 'ARC'       then begin LastObject:=MifTyp;                                  end else
   if MifTyp = 'TEXT'      then begin anz:=anz + ReadText(Zeile); LastObject:=MifTyp;      end else
   if MifTyp = 'RECT'      then begin anz:=anz + ReadRect(Zeile);LastObject:=MifTyp;       end else
   if MifTyp = 'ROUNDRECT' then begin anz:=anz + ReadRect(Zeile);LastObject:=MifTyp;       end else
   if MifTyp = 'ELLIPSE'   then begin anz:=anz + ReadEllipse(Zeile);LastObject:=MifTyp;    end else
   if MifTyp = 'PEN'       then begin anz:=anz + ReadPen(Zeile);                           end else
   if MifTyp = 'BRUSH'     then begin anz:=anz + ReadBrush(Zeile);                         end else
   if MifTyp = 'CENTER'    then begin anz:=anz + ReadCenter(Zeile); NewRegionLabel;        end else
   if MifTyp = 'ANGLE'     then begin anz:=anz + ReadAngle(Zeile);                         end else
   if MifTyp = 'SYMBOL'    then begin anz:=anz + ReadSymbol(Zeile);                        end;
   Result:=anz;
end;

{*******************************************************************************
* PROZEDUR  : ClassFile.NewRegionLabel                                         *
********************************************************************************
* Funktion dient dazu einen Label f�r ein Region Objekt zu erzeugen.           *
*                                                                              *
* PARAMETER    : keine                                                         *
*                                                                              *
********************************************************************************}
procedure ClassFile.NewRegionLabel;
var X1,X2,Y1,Y2:double;
    Text,hstr:string;
begin
   if OptionWnd.DoLabel = true then
   begin
      { Zuwerst werden die Koordinaten dieses Labels ermittelt }
      if not RegionObj.GetLabelData(X1,Y1,X2,Y2,OptionWnd.GetCol,Text) then exit;
      TextObj.Neu(X1,Y1,X2,Y2,OptionWnd.GetFont,OptionWnd.GetSize,'Mittig',Text);
    end;
end;

{*******************************************************************************
* PROZEDUR  : ClassFile.NewPointLabel                                          *
********************************************************************************
* Funktion dient dazu einen Label f�r ein Point Objekt zu erzeugen.            *
*                                                                              *
* PARAMETER    : keine                                                         *
*                                                                              *
********************************************************************************}
procedure ClassFile.NewPointLabel;
var X1,X2,Y1,Y2:double;
    Text,hstr:string;
begin
   if OptionWnd.DoLabel = true then
   begin
      { Zuwerst werden die Koordinaten dieses Labels ermittelt }
      if not PointObj.GetLabelData(X1,Y1,X2,Y2,OptionWnd.GetCol,Text) then exit;
      TextObj.Neu(X1,Y1,X2,Y2,OptionWnd.GetFont,OptionWnd.GetSize,'Mittig',Text);
    end;
end;

{*******************************************************************************
* PROZEDUR  : ClassFile.NewPointLabel                                          *
********************************************************************************
* Funktion dient dazu einen Label f�r ein Ellipse Objekt zu erzeugen.          *
*                                                                              *
* PARAMETER    : keine                                                         *
*                                                                              *
********************************************************************************}
procedure ClassFile.NewEllipseLabel;
var X1,X2,Y1,Y2:double;
    Text,hstr:string;
begin
   if OptionWnd.DoLabel = true then
   begin
      { Zuwerst werden die Koordinaten dieses Labels ermittelt }
      if not EllipseObj.GetLabelData(X1,Y1,X2,Y2,OptionWnd.GetCol,Text) then exit;
      TextObj.Neu(X1,Y1,X2,Y2,OptionWnd.GetFont,OptionWnd.GetSize,'Mittig',Text);
    end;
end;

{*******************************************************************************
* PROZEDUR  : ClassFile.NewPLineLabel                                          *
********************************************************************************
* Funktion dient dazu einen Label f�r ein PLine Objekt zu erzeugen.            *
*                                                                              *
* PARAMETER    : keine                                                         *
*                                                                              *
********************************************************************************}
procedure ClassFile.NewPlineLabel;
var X1,X2,Y1,Y2:double;
    Text,hstr:string;
begin
   if OptionWnd.DoLabel = true then
   begin
      { Zuwerst werden die Koordinaten dieses Labels ermittelt }
      if not PLineObj.GetLabelData(X1,Y1,X2,Y2,OptionWnd.GetCol,Text) then exit;
      TextObj.Neu(X1,Y1,X2,Y2,OptionWnd.GetFont,OptionWnd.GetSize,'Mittig',Text);
    end;
end;

{*******************************************************************************
* FUNKTION  : ClassFile.ReadDelimiter                                          *
********************************************************************************
* Funktion dient dazu den im Mapinfofile Verwendeten Delimiter zu ermitteln.   *
*                                                                              *
* PARAMETER    : Zeile-> Zeile aus der der Delimiter ermittelt wird.           *
*                                                                              *
* R�CKGABEPARAMTER: Anzahl der gelesenen Bytes.                                *
*                                                                              *
********************************************************************************}
function ClassFile.ReadDelimiter(Zeile:string):longint;
var hstr:string;
    hpchar:pchar;
begin
   { Nun wird der Delimiter aus der Zeile extrahiert }
   hstr:=ExtractData(Zeile,2,' '); hpchar:=stralloc(length(hstr)+1);
   strpcopy(hpchar,hstr); Delimiter:=hpchar[1];
   strdispose(hpchar);
   Result:=0;
end;

{*******************************************************************************
* FUNKTION  : ClassFile.ReadPen                                                *
********************************************************************************
* Funktion dient dazu den im Mapinfofile Verwendeten Lineinstil zu ermitteln.  *
*                                                                              *
* PARAMETER    : Zeile-> Zeile aus der der Linienstil ausgelesen wird.         *
*                                                                              *
* R�CKGABEPARAMTER: Anzahl der gelesenen Bytes.                                *
*                                                                              *
********************************************************************************}
function ClassFile.ReadPen(Zeile:string):longint;
var Typstr,Farbstr:string;
    Typnr,count,rot,blau,gruen:integer;
    Farbnr:longint;
begin
   { Ermitteln des Linienstils }
   Typstr:=ExtractData(Zeile,2,',');
   val(Typstr,Typnr,count);

   case Typnr of
      3,4,10            :Linienstil:=lt_Dot;
      5..9,11..13,17..19:Linienstil:=lt_Dash;
      14..16            :Linienstil:=lt_DashDot;
      20..25            :Linienstil:=lt_DashDDot;
   else
      Linienstil:=lt_Solid;
   end;

   { Ermitteln der Farbe }
   Farbstr:=ExtractData(Zeile,3,','); Farbstr:=copy(Farbstr,1,Pos(')',Farbstr)-1);
   val(Farbstr,Farbnr,count);

   { Ermitteln des rot Anteils }
   rot:=Farbnr div 65536; Farbnr:=Farbnr - (rot * 65536);
   { Ermitteln des gruen Anteils }
   gruen:=Farbnr div 256; Farbnr:=Farbnr - (gruen * 256);
   { Ermitteln des blau Anteils }
   blau:=Farbnr;
   Farbstr:=IntToHex (blau,2) + IntToHex (gruen,2) + IntToHex (rot,2);
   Linienfarbe:=strtoint('$'+Farbstr);
   Result:=0;
end;

{*******************************************************************************
* FUNKTION  : ClassFile.ReadBrush                                              *
********************************************************************************
* Funktion dient dazu den im Mapinfofile Verwendeten Fl�chenstil zu ermitteln. *
*                                                                              *
* PARAMETER    : Zeile-> Zeile aus der der Fl�chenstil ausgelesen wird.        *
*                                                                              *
* R�CKGABEPARAMTER: Anzahl der gelesenen Bytes.                                *
*                                                                              *
********************************************************************************}
function ClassFile.ReadBrush(Zeile:string):longint;
var Typstr,Farbstr:string;
    Typnr,count,rot,gruen,blau:integer;
    Farbnr:longint;
begin
   { Ermitteln des Schraffurtyps }
   Typstr:=ExtractData(Zeile,1,','); Typstr:=copy(Typstr,Pos('(',Typstr)+1,length(Typstr));
   val(Typstr,Typnr,count);

   case Typnr of
      2       :Flaechenstil:=pt_Solid;
      3,16..20:Flaechenstil:=pt_Horizontal;
      4,21..25:Flaechenstil:=pt_Vertical;
      5,26..30:Flaechenstil:=pt_FDiagonal;
      6,31..35:Flaechenstil:=pt_BDiagonal;
      7,36..40:Flaechenstil:=pt_Cross;
      8,41..44:Flaechenstil:=pt_DiagCross;
   else
      Flaechenstil:=pt_NoPattern;
   end;

   { Ermitteln der Schraffurfarbe }
   { Ermitteln der Farbe }
   Farbstr:=ExtractData(Zeile,2,',');
   val(Farbstr,Farbnr,count);

   { Ermitteln des rot Anteils }
   rot:=Farbnr div 65536; Farbnr:=Farbnr - (rot * 65536);
   { Ermitteln des gruen Anteils }
   gruen:=Farbnr div 256; Farbnr:=Farbnr - (gruen * 256);
   { Ermitteln des blau Anteils }
   blau:=Farbnr;
   Farbstr:=IntToHex (blau,2) + IntToHex (gruen,2) + IntToHex (rot,2);
   Flaechenfarbe:=strtoint('$'+Farbstr);
   Result:=0;
end;

{*******************************************************************************
* FUNKTION  : ClassFile.ReadCenter                                             *
********************************************************************************
* Funktion dient dazu eine Centerinformation aus der MIF Datei auszulesen.     *
*                                                                              *
* PARAMETER    : Zeile-> Zeile aus die Centerinformation ausgelesen wird.      *
*                                                                              *
* R�CKGABEPARAMTER: Anzahl der gelesenen Bytes.                                *
*                                                                              *
********************************************************************************}
function ClassFile.ReadCenter(Zeile:string):longint;
var Xstr,Ystr:string;
    X,Y:double;
    count:integer;
begin
   XStr:=ParseString(Zeile,2); YStr:=ParseString(Zeile,3);
   val(XStr,X,count); val(YStr,Y,count);
   if LastObject = 'REGION' then RegionObj.SetCenter(X,Y);
   Result:=0;
end;

{*******************************************************************************
* FUNKTION  : ClassFile.ReadCenter                                             *
********************************************************************************
* Funktion dient dazu eine Angleinformation aus der MIF Datei auszulesen.      *
*                                                                              *
* PARAMETER    : Zeile-> Zeile aus die Angleinformation ausgelesen wird.       *
*                                                                              *
* R�CKGABEPARAMTER: Anzahl der gelesenen Bytes.                                *
*                                                                              *
********************************************************************************}
function ClassFile.ReadAngle(Zeile:string):longint;
var hstr:string;
    Winkel:double;
    count:integer;
begin
   hStr:=ParseString(Zeile,2); val(hStr,Winkel,count);
   if LastObject = 'TEXT' then TextObj.SetAngle(Winkel);
   Result:=0;
end;

function ClassFile.ReadSymbol(Zeile:string):longint;
var Text:string;
    dummy,SymFont:string;
    anz:longint;
    SymIdx,SymSize,SymAngle,count:integer;
    SymRef:p_SymbolRef_record;
begin
   // now the symbol information will be parsed
   Text:=Zeile;
   delete(Text,1,Pos('(',Text));     // delete first (
   Text:=copy(Text,1,Pos(')',Text)-1); // get till )
   // get SymIdx
   dummy:=ExtractData(Text,1,',');
   val(dummy,SymIdx,count); if count <> 0 then SymIdx:=0;
   // color will be ignored
   // get size
   dummy:=ExtractData(Text,3,',');
   val(dummy,SymSize,count); if count <> 0 then SymSize:=0;
   // get font
   dummy:=ExtractData(Text,4,',');
   if (dummy <> '') then
   begin
     // delete " signs at begin and end
     Delete(dummy,1,1);
     dummy:=copy(dummy,1,Pos('"',dummy)-1);
   end;
   if dummy='' then dummy:='Mapinfo 3.0 Compatible'; // Mapinfo 3.0 Compatible is default if no font is set
   SymFont:=dummy;

   // add new font and idx to symbolreferencating
   SymRef:=SymbolRefObj.AddMifSymbol(SymFont,SymIdx);

   if LastObject = 'POINT' then PointObj.SetSymbol(SymRef,SymSize);

   Result:=0;
end;

procedure ClassFile.GetSymbolRefData(Fontname:string; SymIdx:integer; var GisSymbolName:string);
begin
   SymbolRefObj.GetSymbolRefData(Fontname,SymIdx, GisSymbolName);
end;

procedure ClassFile.SetSymbolRefData(Fontname:string; SymIdx:integer; GisSymbolName:string);
begin
   SymbolRefObj.SetSymbolRefData(Fontname,SymIdx, GisSymbolName);
end;

function ClassFile.CheckSymbolsExist:boolean;
begin
   result:=SymbolRefObj.CheckSymbolsExist;
end;

procedure ClassFile.ResetCurrentSymbolRef;
begin
   SymbolRefObj.ResetCurrent;
end;

function ClassFile.GetCurrentSymbolRefData(var Fontname:string; var SymIdx:integer; var GisSymbolName:string):boolean;
begin
   result:=SymbolRefObj.GetCurrentData(Fontname, SymIdx, GisSymbolName);
end;

function ClassFile.GetSymbolRefDataByIdx(Index:integer):p_SymbolRef_record;
begin
   result:=SymbolRefObj.GetSymbolRefDataByIdx(Index);
end;

{*******************************************************************************
* FUNKTION  : ClassFile.ReadPoint                                              *
********************************************************************************
* Funktion dient dazu ein Point Objekt aus der MIF Datei auszulesen.           *
*                                                                              *
* PARAMETER    : Zeile-> Zeile aus die Pointinformation ausgelesen wird.       *
*                                                                              *
* R�CKGABEPARAMTER: Anzahl der gelesenen Bytes.                                *
*                                                                              *
********************************************************************************}
function ClassFile.ReadPoint(Zeile:string):longint;
var Xstr,Ystr:string;
    X,Y:double;
    count,DBanz,k:integer;
    Spaltenname,Spalteninhalt,MidZeile:string;
begin
   if MainWnd.MIDOK then MIDZeile:=GetMidLine;

   Xstr:=ParseString(Zeile,2); val(XStr,X,count);
   Ystr:=ParseString(Zeile,3); val(YStr,Y,count);
   PointObj.Neu(X,Y);

   { Nun werden die Datenbankattribute hinzugef�gt }
   if MainWnd.MIDOK then
   begin
      DBAnz:=DatenbankObj.SetDBFirst;
      for k:=1 to DBAnz do
      begin
         Spaltenname:=DatenbankObj.GetDBColumnname;
         Spalteninhalt:=ExtractData(MIDZeile,k,DELIMITER);
         { Nun mu� der Spalteninhalt selbst ermittelt werden }
         if (pos('"',Spalteninhalt) <> 0) then
            Spalteninhalt:=copy(Spalteninhalt,2,length(Spalteninhalt)-2);

         PointObj.InsertAttrib(Spaltenname,Spalteninhalt);
         DatenbankObj.SetCurrentnext;
      end;
   end;

   Result:=0;
end;

function ClassFile.FormatText(Text:string):string;
var MyText:string;
begin
   MyText:='';
   { Wenn der Text mehrzeilig ist so wird er durch \n getrennt }
   repeat
      if Pos('\n',Text) <> 0 then
      begin
         MyText:=MyText + copy(Text,1,Pos('\n',Text)-1);
         Delete(Text,1,Pos('\n',Text)+1); MyText:=MyText + ' - ';
      end
      else
      begin
         MyText:=MyText + Text; Text:='';
      end;
   until Text = '';
   result:=myText;
end;

{*******************************************************************************
* FUNKTION  : ClassFile.ReadText                                               *
********************************************************************************
* Funktion dient dazu ein Text Objekt aus der MIF Datei auszulesen.            *
*                                                                              *
* PARAMETER    : Zeile-> Erste Zeile des Textobjekts                           *
*                                                                              *
* R�CKGABEPARAMTER: Anzahl der gelesenen Bytes.                                *
*                                                                              *
********************************************************************************}
function ClassFile.ReadText(Zeile:string):longint;
var Xstr,Ystr:string;
    X1,Y1:double;
    X2,Y2:double;
    TextHeight:double;
    count,DBanz,k:integer;
    Spaltenname,Spalteninhalt,MidZeile:string;
    Text,MyText,Font:string;
    anz:longint;
begin
   anz:=0;
   if MainWnd.MIDOK then MIDZeile:=GetMIDLine;

   { Nun wird die Textinformation ermittelt }
   Text:=ExtractData(Zeile,2,'"');
   if Text <> '' then
   begin
      myText:=FormatText(Text);
   end
   else
   begin
      // read Text out of next line
      anz:=anz+GetMifLine(Text);
      Text:=copy(Text,2,length(Text)-2); // delete ""
      myText:=FormatText(Text);
   end;

   // evaluate position of text
   anz:=anz + GetMIFLine(Text);
   Xstr:=ParseString(Text,1); val(XStr,X1,count); Ystr:=ParseString(Text,2); val(YStr,Y1,count);
   Xstr:=ParseString(Text,3); val(XStr,X2,count); Ystr:=ParseString(Text,4); val(YStr,Y2,count);
   TextHeight:=Y2-Y1;
   Y2:=Y1;

   { Nun wird der Font ausgelesen }
   anz:=anz + GetMIFLine(Text); Font:=ExtractData(Text,2,'"');

   { Da die Gr��e nicht errechnet werden kann, verwende ich Standartm��ig die Textgr��e 10 }
   TextObj.Neu(X1,Y1,X2,Y2,Font,TextHeight,'Links',MyText);

   { Nun werden die Datenbankattribute hinzugef�gt }
   if MainWnd.MIDOK then
   begin
      DBAnz:=DatenbankObj.SetDBFirst;
      for k:=1 to DBAnz do
      begin
         Spaltenname:=DatenbankObj.GetDBColumnname;
         Spalteninhalt:=ExtractData(MIDZeile,k,DELIMITER);
         { Nun mu� der Spalteninhalt selbst ermittelt werden }
         if (pos('"',Spalteninhalt) <> 0) then
            Spalteninhalt:=copy(Spalteninhalt,2,length(Spalteninhalt)-2);
         TextObj.InsertAttrib(Spaltenname,Spalteninhalt);
         DatenbankObj.SetCurrentnext;
      end;
   end;

   Result:=anz;
end;

{*******************************************************************************
* FUNKTION  : ClassFile.ReadLine                                               *
********************************************************************************
* Funktion dient dazu ein Line  Objekt aus der MIF Datei auszulesen.           *
*                                                                              *
* PARAMETER    : Zeile-> Zeile aus die Line Information ausgelesen wird.       *
*                                                                              *
* R�CKGABEPARAMTER: Anzahl der gelesenen Bytes.                                *
*                                                                              *
********************************************************************************}
function ClassFile.ReadLine(Zeile:string):longint;
var Xstr,Ystr:string;
    X1,Y1,X2,Y2:double;
    count,DBanz,k:integer;
    Spaltenname,Spalteninhalt,MidZeile:string;
begin

   if MainWnd.MIDOK then MIDZeile:=GetMIDLine;

   Xstr:=ParseString(Zeile,2); val(XStr,X1,count);
   Ystr:=ParseString(Zeile,3); val(YStr,Y1,count);
   Xstr:=ParseString(Zeile,4); val(XStr,X2,count);
   Ystr:=ParseString(Zeile,5); val(YStr,Y2,count);

   PLineObj.Neu;
   PLineObj.InsertPoint(X1,Y1);
   PLineObj.InsertPoint(X2,Y2);

   { Nun werden die Datenbankattribute hinzugef�gt }
   if MainWnd.MIDOK then
   begin
      DBAnz:=DatenbankObj.SetDBFirst;
      for k:=1 to DBAnz do
      begin
         Spaltenname:=DatenbankObj.GetDBColumnname;
         Spalteninhalt:=ExtractData(MIDZeile,k,DELIMITER);
         { Nun mu� der Spalteninhalt selbst ermittelt werden }
         if (pos('"',Spalteninhalt) <> 0) then
            Spalteninhalt:=copy(Spalteninhalt,2,length(Spalteninhalt)-2);
         PLineObj.InsertAttrib(Spaltenname,Spalteninhalt);
         DatenbankObj.SetCurrentnext;
      end;
   end;
   Result:=0;
end;

{*******************************************************************************
* FUNKTION  : ClassFile.ReadRect                                               *
********************************************************************************
* Funktion dient dazu ein Rect Objekt aus der MIF Datei auszulesen.            *
*                                                                              *
* PARAMETER    : Zeile-> Zeile aus die Rect Information ausgelesen wird.       *
*                                                                              *
* R�CKGABEPARAMTER: Anzahl der gelesenen Bytes.                                *
*                                                                              *
********************************************************************************}
function ClassFile.ReadRect(Zeile:string):longint;
var Xstr,Ystr:string;
    X1,Y1,X2,Y2,CenterX,CenterY:double;
    count,DBanz,k:integer;
    Spaltenname,Spalteninhalt,MidZeile:string;
begin

   if MainWnd.MIDOK then MIDZeile:=GetMIDLine;

   Xstr:=ParseString(Zeile,2); val(XStr,X1,count);
   Ystr:=ParseString(Zeile,3); val(YStr,Y1,count);
   Xstr:=ParseString(Zeile,4); val(XStr,X2,count);
   Ystr:=ParseString(Zeile,5); val(YStr,Y2,count);

   RegionObj.Neu;
   RegionObj.InsertPoint(X1,Y1);
   RegionObj.InsertPoint(X2,Y1);
   RegionObj.InsertPoint(X2,Y2);
   RegionObj.InsertPoint(X1,Y2);

   { Nun werden die Datenbankattribute hinzugef�gt }
   if MainWnd.MIDOK then
   begin
      DBAnz:=DatenbankObj.SetDBFirst;
      for k:=1 to DBAnz do
      begin
         Spaltenname:=DatenbankObj.GetDBColumnname;
         Spalteninhalt:=ExtractData(MIDZeile,k,DELIMITER);
         { Nun mu� der Spalteninhalt selbst ermittelt werden }
         if (pos('"',Spalteninhalt) <> 0) then
            Spalteninhalt:=copy(Spalteninhalt,2,length(Spalteninhalt)-2);
         RegionObj.InsertAttrib(Spaltenname,Spalteninhalt);
         DatenbankObj.SetCurrentnext;
      end;
   end;

   { Nun wird noch der Center zum Rechteck hinzugef�gt }
   if (X1 < 0) and (X2 < 0) then CenterX:=((ABS(X1) + ABS(X2))/2)*-1 else CenterX:=(X1 + X2)/2;
   if (Y1 < 0) and (Y2 < 0) then CenterY:=((ABS(Y1) + ABS(Y2))/2)*-1 else CenterY:=(Y1 + Y2)/2;
   RegionObj.SetCenter(CenterX,CenterY);
   Result:=0;
end;

{*******************************************************************************
* FUNKTION  : ClassFile.ReadEllipse                                            *
********************************************************************************
* Funktion dient dazu ein Ellipse Objekt aus der MIF Datei auszulesen.         *
*                                                                              *
* PARAMETER    : Zeile-> Zeile aus die Ellipse Information ausgelesen wird.    *
*                                                                              *
* R�CKGABEPARAMTER: Anzahl der gelesenen Bytes.                                *
*                                                                              *
********************************************************************************}
function ClassFile.ReadEllipse(Zeile:string):longint;
var Xstr,Ystr:string;
    X1,Y1,X2,Y2,Radius1,Radius2:double;
    count,DBanz,k:integer;
    Spaltenname,Spalteninhalt,MidZeile:string;
begin

   if MainWnd.MIDOK then MIDZeile:=GetMIDLine;

   { Da wir im WinGIS keine Ellipse importieren k�nnen, wandle ich diese
     einfach in einen Kreis um, dessen Radius der k�rzeren Kantenbreite
     der Ellipse entspricht                                              }

   Xstr:=ParseString(Zeile,2); val(XStr,X1,count);
   Ystr:=ParseString(Zeile,3); val(YStr,Y1,count);
   Xstr:=ParseString(Zeile,4); val(XStr,X2,count);
   Ystr:=ParseString(Zeile,5); val(YStr,Y2,count);

   { Nun wird der Radius des Kreises ermittelt }
   Radius1:=(X1+X2)/2 - X1; { Die X-Kante des umrahmenden Rechtecks ist l�nger }
   Radius2:=(Y1+Y2)/2 - Y1; { Die Y-Kante des umrahmenden Rechtecks ist l�nger }

   EllipseObj.Neu((X1+X2)/2,(Y1+Y2)/2,Radius1,Radius2);

   { Nun werden die Datenbankattribute hinzugef�gt }
   if MainWnd.MIDOK then
   begin
      DBAnz:=DatenbankObj.SetDBFirst;
      for k:=1 to DBAnz do
      begin
         Spaltenname:=DatenbankObj.GetDBColumnname;
         Spalteninhalt:=ExtractData(MIDZeile,k,DELIMITER);
         { Nun mu� der Spalteninhalt selbst ermittelt werden }
         if (pos('"',Spalteninhalt) <> 0) then
            Spalteninhalt:=copy(Spalteninhalt,2,length(Spalteninhalt)-2);
         EllipseObj.InsertAttrib(Spaltenname,Spalteninhalt);
         DatenbankObj.SetCurrentnext;
      end;
   end;
   Result:=0;
end;

{*******************************************************************************
* FUNKTION  : ClassFile.ReadColumns                                            *
********************************************************************************
* Funktion dient dazu den im Mapinfofile Verwendeten Columns   zu ermitteln.   *
*                                                                              *
* PARAMETER    : Zeile-> Erste Zeile der Columns Definition.                   *
*                                                                              *
* R�CKGABEPARAMTER: Anzahl der gelesenen Bytes.                                *
*                                                                              *
********************************************************************************}
function ClassFile.ReadColumns(Zeile:string):longint;
var anz:longint;
    numcol,count,i,hint:integer;
    Spaltenname,Text:string;
    SpaltenTyp,dummy:string;
    ColType:integer;
    ColLength:integer;
begin
   anz:=0;

   { Nun wird die Anzahl der Datenbankspalten ermittelt }
   val(ParseString(Zeile,2),numcol,count); if count <> 0 then numcol:=0;
   { Nun werden die eigentlichen Datenbankspaltendefinitionen ausgelesen }
   for i:=1 to numcol do
   begin
      anz:=anz+GetMIFLine(Text);
      Spaltenname:=ParseString(Text,1); { Ermitteln des Spaltennamens }

      // nun wird Spaltentyp ermittelt
      SpaltenTyp:=ParseString(Text,2); SpaltenTyp:=strpas(strupper(PCHAR(SpaltenTyp)));
      ColLength:=0;

      if SpaltenTyp = 'INTEGER' then ColType:=Col_Int
      else if copy(SpaltenTyp,1,4) = 'CHAR' then
      begin
         ColType:=Col_String;
         SpaltenTyp:=Text;
         // nun wird noch die Spaltenl�nge ermittelt
         delete (SpaltenTyp,1,Pos('(',SpaltenTyp));
         SpaltenTyp:=copy(SpaltenTyp,1,pos(')',SpaltenTyp)-1);
         ColLength:=strtoint(SpaltenTyp);
      end
      else if copy(SpaltenTyp,1,7) = 'DECIMAL' then
      begin
         // float values will be stored as char in database
         ColType:=Col_String;
         SpaltenTyp:=copy(Text,Pos('Decimal',Text),length(Text));
         // evaluate column-length
         delete(SpaltenTyp,1,8); // delete inclusive ( sign
         dummy:=copy(SpaltenTyp,1,Pos(',',SpaltenTyp)-1);
         val(dummy,hint,count); if count <> 0 then hint:=0;
         ColLength:=hint;
         delete(SpaltenTyp,1,Pos(',',SpaltenTyp)); // delete inclusive , sign
         dummy:=copy(SpaltenTyp,1,Pos(')',SpaltenTyp)-1);
         val(dummy,hint,count); if count <> 0 then hint:=0;
         ColLength:=ColLength + hint + 1; // +1 because of comma sign
      end
      else if SpaltenTyp = 'SMALLINT' then ColType:=Col_SmallInt
      else if SpaltenTyp = 'FLOAT'    then ColType:=Col_Float
      else if SpaltenTyp = 'DATE'     then ColType:=Col_Date
      else if SpaltenTyp = 'LOGICAL'  then begin ColType:=Col_String; ColLength:=10; end;

      if DatenbankObj.CheckExists(Spaltenname) = false then
      begin
         DatenbankObj.InsertColumn(Spaltenname,0,ColType,'');
         DatenbankObj.SetLength(Spaltenname,ColLength);
      end;
   end;
   { Nun muss definiert werden, ob ein Labeling durchgef�hrt wird }
   if SetmiffileWnd.SelectivCB.checked = true then OptionWnd.ShowModal;
   Result:=anz;
end;

{*******************************************************************************
* FUNKTION  : ClassFile.ReadRegion                                             *
********************************************************************************
* Funktion dient dazu ein Region-Objekt aus dem Mapinfo File auszulesen.       *
*                                                                              *
* PARAMETER    : Zeile-> Erste Zeile der REGION Definition.                    *
*                                                                              *
* R�CKGABEPARAMTER: Anzahl der gelesenen Bytes.                                *
*                                                                              *
********************************************************************************}
function ClassFile.ReadRegion(Zeile:string):longint;
var anz:longint;
    num_region,num_points,count,i,j,k,DBAnz:integer;
    Text,MIDZeile,Spaltenname,Spalteninhalt:string;
    X,Y:double;
begin
   anz:=0;

   // evaluate number of islands this region object contains
   val(ParseString(Zeile,2),num_region,count);
   if (count <> 0) then
   begin
      num_region:=0;
      // numisland is not correct -> display warning
      MainWnd.StatusLB.items.add(StrfileObj.Assign(59)); // Warning: Region counter not correct. REGION will be ignored!
      MainWnd.StatusLB.ItemIndex := MainWnd.StatusLB.Items.Count-1;
      MainWnd.StatusLB.Update;
   end;

   if MainWnd.MIDOK then MIDZeile:=GetMIDLine;

   if (num_region > 0) then
   for i:=1 to num_region do
   begin
      { Erzeugen eines neuen Region-Objekts }
      if (i=1) then RegionObj.Neu
      else RegionObj.InsertIsland;

      { Nun wird die Anzahl der Eckpunkte f�r diesen Regionteil ausgelesen }
      anz:=anz + GetMIFLine(Text);
      val(ParseString(Text,1),num_points,count);

      for j:=1 to num_points do
      begin
         { Nun werden diese Eckpunkte ausgelesen }
         anz:=anz + GetMifLine(Text);
         val(ParseString(Text,1),X,count); val(ParseString(Text,2),Y,count);
         if (i=1) then RegionObj.InsertPoint(X,Y)
         else RegionObj.InsertPointToIsland(X,Y);
      end;
   end;
   // add database attributes
   if ((MainWnd.MIDOK) and (num_region > 0)) then
   begin
      DBAnz:=DatenbankObj.SetDBFirst;
      for k:=1 to DBAnz do
      begin
         Spaltenname:=DatenbankObj.GetDBColumnname;
         Spalteninhalt:=ExtractData(MIDZeile,k,DELIMITER);
         { Nun mu� der Spalteninhalt selbst ermittelt werden }
         if (pos('"',Spalteninhalt) <> 0) then
            Spalteninhalt:=copy(Spalteninhalt,2,length(Spalteninhalt)-2);
         RegionObj.InsertAttrib(Spaltenname,Spalteninhalt);
         DatenbankObj.SetCurrentnext;
      end;
   end;
   Result:=anz;
end;

{*******************************************************************************
* FUNKTION  : ClassFile.ReadPline                                              *
********************************************************************************
* Funktion dient dazu ein Pline Objekt  aus dem Mapinfo File auszulesen.       *
*                                                                              *
* PARAMETER    : Zeile-> Erste Zeile der PLINE  Definition.                    *
*                                                                              *
* R�CKGABEPARAMTER: Anzahl der gelesenen Bytes.                                *
*                                                                              *
********************************************************************************}
function ClassFile.ReadPLine(Zeile:string):longint;
var anz:longint;
    num_points,num_polygons,count,i,j,k,DBAnz:integer;
    Text,MIDZeile,Spaltenname,Spalteninhalt:string;
    X,Y:double;
    hString:string;
begin
   anz:=0;
   { Nun wird ermittelt aus wie vielen Linienelementen diese Pline besteht }

   { Nun wird ermittelt aus wie vielen Punkten diese Pline besteht }
   hString:=ParseString(Zeile,2);
   if hString = 'Multiple' then val(ParseString(Zeile,3),num_polygons,count)
   else
   begin
      num_polygons:=1;
      val(ParseString(Zeile,2),num_points,count);
   end;

   if MainWnd.MIDOK then MIDZeile:=GetMIDLine;
   for i:=1 to num_polygons do
   begin
      { Erzeugen eines neuen Pline-Objekts }
      if num_polygons > 1 then
      begin
         anz:=anz + GetMifLine(Text);
         val(ParseString(Text,1),num_points,count);
      end;
      PlineObj.Neu;

      for j:=1 to num_points do
      begin
         { Nun werden diese Eckpunkte ausgelesen }
         anz:=anz + GetMifLine(Text);
         val(ParseString(Text,1),X,count); val(ParseString(Text,2),Y,count);
         PlineObj.InsertPoint(X,Y);
      end;

      { Nun werden die Datenbankattribute hinzugef�gt }
      if MainWnd.MIDOK then
      begin
         DBAnz:=DatenbankObj.SetDBFirst;
         for k:=1 to DBAnz do
         begin
            Spaltenname:=DatenbankObj.GetDBColumnname;
            Spalteninhalt:=ExtractData(MIDZeile,k,DELIMITER);
            { Nun mu� der Spalteninhalt selbst ermittelt werden }
            if (pos('"',Spalteninhalt) <> 0) then
               Spalteninhalt:=copy(Spalteninhalt,2,length(Spalteninhalt)-2);
            PLineObj.InsertAttrib(Spaltenname,Spalteninhalt);
            DatenbankObj.SetCurrentnext;
         end;
      end;
   end;
   Result:=anz;
end;
{*******************************************************************************
* FUNKTION  : ClassFile.ExtractData                                            *
********************************************************************************
* Prozedur extrahiert aus einem durch Delimiter getrennten String einen        *
* Teilstring                                                                   *
*                                                                              *
* PARAMETER    : Text -> Quellstring aus dem der Teilstring extrahiert wird.   *
*                Pos  -> Position des Teilstrings                              *
*                Delimiter -> Trennzeichen                                     *
*                                                                              *
* R�CKGABEPARAMTER: Teilstring                                                 *
*                                                                              *
********************************************************************************}
function ClassFile.ExtractData(Text:string;Pos :integer;Delimiter:char):string;
var Pos1,Pos2,Stelle,i:integer;
    MyText:pchar;
    gefunden:boolean;
begin
   MyText:=stralloc(length(text)+1);
   strpcopy(MyText,Text);

   { Nun werden die Positionen der Delimiter ermittelt }
   Stelle:=0; Pos1:=0; Pos2:=0; gefunden:=false;
   for i:=0 to strlen(MyText) do
   begin
      if (MyText[i] = Delimiter) or (i = strlen(MyText)) then
      begin
         if gefunden = false then begin Pos1:=Pos2; Pos2:=i; Stelle:=Stelle + 1; end;
         if Stelle = Pos then gefunden:=true;
      end;
   end;
   strdispose(MyText);
   if Pos = 1 then Pos1:=Pos1 - 1;
   if gefunden = true then Result:=copy(Text,Pos1 + 2,Pos2 - Pos1 - 1) else Result:='';
end;

{*******************************************************************************
* FUNKTION : ClassFile.GetMifLine                                              *
********************************************************************************
* Funktion dient zum Einlesen einer Zeile aus dem MIF-File.                    *
*                                                                              *
* PARAMETER: Zeile -> Zeile, die aus dem MIF, File ausgelesen wurde.           *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der gelesenen Bytes.                                    *
********************************************************************************}
function ClassFile.GetMifLine(var Zeile:string):longint;
var Text:string;
begin
   {$i-} readln(MIFDatei,Text); {$i+}
   Result:=length(Text) + 2;

   { Nun m�ssen die f�hrenden Leerzeichen gel�scht werden }
   Text:=TRIMLEFT(Text);
   Zeile:=Text;
end;

procedure Classfile.Convert2Dos(var Zeile:string);
var Actualbytes:pchar;
    i:integer;
begin
   { Nun wird der Text noch ins DOS-Format umkonvertiert }
   Actualbytes:=stralloc(length(Zeile)+1); strpcopy(Actualbytes,Zeile);
   for i:=1 to length(Zeile) do
   begin
      if Actualbytes[i] = '�' then Actualbytes[i]:=CHR(148);
      if Actualbytes[i] = '�' then Actualbytes[i]:=CHR(153);
      if Actualbytes[i] = '�' then Actualbytes[i]:=CHR(132);
      if Actualbytes[i] = '�' then Actualbytes[i]:=CHR(142);
      if Actualbytes[i] = '�' then Actualbytes[i]:=CHR(129);
      if Actualbytes[i] = '�' then Actualbytes[i]:=CHR(154);
      if Actualbytes[i] = '�' then Actualbytes[i]:=CHR(225);
   end;
   Zeile:=strpas(Actualbytes);
   strdispose(Actualbytes);
end;

{*******************************************************************************
* FUNKTION : ClassFile.GetMidLine                                              *
********************************************************************************
* Funktion dient zum Einlesen einer Zeile aus dem MID-File.                    *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
*                                                                              *
* R�ckgabewert: Ausgelesene Zeile aus der MIDDATEI                             *
********************************************************************************}
function ClassFile.GetMidLine:string;
var Text:string;
    Actualbytes:pchar;
    i:integer;
begin
   {$i-} readln(MIdDatei,Text); {$i+}

   { Nun wird der Text noch ins DOS-Format umkonvertiert }
   Actualbytes:=stralloc(length(Text)+1); strpcopy(Actualbytes,Text);
   for i:=1 to length(Text) do
   begin
      if Actualbytes[i] = '�' then Actualbytes[i]:=CHR(148);
      if Actualbytes[i] = '�' then Actualbytes[i]:=CHR(153);
      if Actualbytes[i] = '�' then Actualbytes[i]:=CHR(132);
      if Actualbytes[i] = '�' then Actualbytes[i]:=CHR(142);
      if Actualbytes[i] = '�' then Actualbytes[i]:=CHR(129);
      if Actualbytes[i] = '�' then Actualbytes[i]:=CHR(154);
      if Actualbytes[i] = '�' then Actualbytes[i]:=CHR(225);
   end;
   Result:=strpas(Actualbytes);
   strdispose(Actualbytes);
end;

{*******************************************************************************
* FUNKTION : ClassFile.ParseString                                             *
********************************************************************************
* Funktion extrahiert aus der Zeichenkette Source das durch Pos bestimmte      *
* Element.                                                                     *
*                                                                              *
* PARAMETER: Source      -> Text, von dem Ausgegangen wird.                    *
*            Pos         -> Position des Eintrages der erhalten werden soll.   *
*                                                                              *
* R�ckgabewert: Extrahierte Zeichenkette.                                      *
********************************************************************************}
function ClassFile.ParseString(Source:string;Pos:integer):string;
var hpchar:pchar;
    pos1,pos2,stelle:integer;
    hstr:string;
begin
   hpchar:=stralloc(length(Source)+1);
   strpcopy(hpchar,Source);

   pos1:=0; pos2:=0; stelle:=0;
   repeat
      { Zuerst wird bis zum ersten nicht Leerzeichen gegangen }
      repeat
         if hpchar[pos2] = ' ' then pos2:=pos2 + 1;
      until hpchar[pos2] <> ' ';
      pos1:=pos2;
      { Nun wird wieder bis zum ersten Leerzeichen gegangen }
      repeat
         if (hpchar[pos2] <> ' ') and (pos2 <= strlen(hpchar)) then pos2:=pos2 + 1;
      until (hpchar[pos2] = ' ') or (pos2 >= strlen(hpchar)) or
             ((hpchar[pos2] = '-') and (not( (hpchar[pos2-1] = 'E') or (hpchar[pos2-1] = 'e'))));
      stelle:=stelle + 1;
   until (stelle = pos) or (pos2 >=strlen(hpchar));

   if stelle = pos then hstr:=copy(source,pos1 + 1,pos2 - pos1) else hstr:='';
   result:=hstr;
   strdispose(hpchar);
end;

end.
