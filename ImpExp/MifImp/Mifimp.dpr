library Mifimp;

{ Wichtiger Hinweis zur DLL-Speicherverwaltung: Sharemem mu� die erste
 Unit in der uses-Klausel Ihrer Bibliothek und Ihres Projekts sein,
 wenn Ihre DLL Prozeduren und Funktionen exportiert, die
 Strings als Parameter oder Funktionsergebnisse �bergeben. Dies
 betrifft alle Strings, die an Ihre DLL oder von Ihrer DLL �bergeben werden--selbst diejenigen,
 die sich in Records oder Klassen befinden. Sharemem ist die Schnittstelle zu
 BORLNDMM.DLL, die den gemeinsamen Zugriff auf den Speicher verwaltet; DELPHIMM.DLL
 mu� mit Ihrer DLL weitergegeben werden. Wenn Sie BORLNDMM.DLL nicht verwenden m�chten, m�ssen
 Strings als PChar oder ShortString �bergeben werden.}

uses
  SysUtils,
  Classes,
  Dialogs,
  Maindlg in 'maindlg.pas' {MainWnd},
  OptionDlg in 'OptionDlg.pas' {OptionWnd},
  SetMIFfileDlg in 'SetMIFfileDlg.pas' {SetmiffileWnd},
  Files in 'Files.pas',
  Strfile in 'Strfile.pas',
  struct in 'Struct.pas',
  comobj,
  AXDLL_TLB,
  SymMapDlg in 'SymMapDlg.pas' {SymMapWnd};

{$R *.RES}
{*******************************************************************************
* PROZEDURE * MIFIMPORT                                                        *
********************************************************************************
* Procedure is used to import MAPINFO MIF data into GIS                        *
*                                                                              *
********************************************************************************}
procedure MIFIMPORT(DIR:PCHAR;var AXHANDLE:VARIANT;var DEST:VARIANT;REGMODE:integer;LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var
aDisp     :IDispatch;
begin
   // create MainWnd
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.App:=AXHANDLE;
   MainWnd.Regmode:=REGMODE;
   aDisp:=DEST;
   MainWnd.DestDocument:=aDisp as IDocument;
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.CreateStringFile;

   // create SetMifFileWnd
   SetMifFileWnd:=TSetMifFileWnd.Create(nil);
   SetMifFileWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);

   SetMifFileWnd.Show;
end;

{*******************************************************************************
* PROCEDURE * CREATEIMP                                                        *
********************************************************************************
* Procedure is used to prepare an MIF-import for Batchmode                     *
*                                                                              *
********************************************************************************}
procedure CREATEIMP(DIR:PCHAR;var AXHANDLE:VARIANT;var DEST:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var aDisp:IDispatch;
begin
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.App:=AXHANDLE;
   aDisp:=DEST;
   MainWnd.DestDocument:=aDisp as IDocument;
   MainWnd.Regmode:=REGMODE;
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.CreateStringFile;

   // create SetMifFileWnd
   SetMifFileWnd:=TSetMifFileWnd.Create(nil);
   SetMifFileWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);
   SetMifFileWnd.SetupForBatch;
end;

{*******************************************************************************
* PROCEDURE * SETMIFIMPSETTINGS                                                *
********************************************************************************
* Procedure is used to set type of import                                      *
*                                                                              *
* PARAMETERS: IMPMODE     -> Type if the import should be selective or not     *
*                            0 -> Import should not be selective.              *
*                            1 -> Import should be selective.                  *
*                                                                              *
********************************************************************************}
procedure SETMIFIMPSETTINGS(IMPMODE:INTEGER);stdcall;
begin
   SetmiffileWnd.SetImpSettings(IMPMODE);
end;

// procedure is used to add a .MIF file that should be imported
procedure ADDIMPFILE(AFILE:PCHAR);stdcall;
begin
   SetmiffileWnd.AddImpFile(strpas(AFILE));
end;

// procedure is used to set database-settings
procedure SETDBSETTINGS(SOURCEDBMODE:INTEGER; SOURCEDBNAME:PCHAR; SOURCEDBTABLE:PCHAR;
                        DESTDBMODE  :INTEGER; DESTDBNAME  :PCHAR; DESTDBTABLE  :PCHAR);stdcall;
begin
   // source database will be ignored for MIF-Import
   MainWnd.AXImpExpDbc.ImpDbMode:=DESTDBMODE;
   MainWnd.AxImpExpDbc.ImpDatabase:=strpas(DESTDBNAME);
   MainWnd.AXImpExpDbc.ImpTable:=strpas(DESTDBTABLE);
end;

// procedure is used to set projection for batch mode
procedure SETPROJECTION(SOURCECOORDINATESYSTEM:INTEGER; // source projection
                        SOURCEPROJECTION      :PCHAR;
                        SOURCEDATE            :PCHAR;
                        SOURCEPROJSETTINGS    :PCHAR;
                        SOURCEXOFFSET         :DOUBLE;
                        SOURCEYOFFSET         :DOUBLE;
                        SOURCESCALE           :DOUBLE;
                        TARGETCOORDINATESYSTEM:INTEGER; // target projection
                        TARGETPROJECTION      :PCHAR;
                        TARGETDATE            :PCHAR;
                        TARGETPROJSETTINGS    :PCHAR;
                        TARGETXOFFSET         :DOUBLE;
                        TARGETYOFFSET         :DOUBLE;
                        TARGETSCALE           :DOUBLE);stdcall;
begin
   // source projection
   MainWnd.AxGisProjection.SourceCoordinateSystem:=SOURCECOORDINATESYSTEM;
   MainWnd.AxGisProjection.SourceProjection:=strpas(SOURCEPROJECTION);
   MainWnd.AxGisProjection.SourceDate:=strpas(SOURCEDATE);
   MainWnd.AxGisProjection.SourceProjSettings:=strpas(SOURCEPROJSETTINGS);
   MainWnd.AxGisProjection.SourceXOffset:=SOURCEXOFFSET;
   MainWnd.AxGisProjection.SourceYOffset:=SOURCEYOFFSET;
   MainWnd.AxGisProjection.SourceScale:=SOURCESCALE;

   // target projection
   MainWnd.AxGisProjection.TargetCoordinateSystem:=TARGETCOORDINATESYSTEM;
   MainWnd.AxGisProjection.TargetProjection:=strpas(TARGETPROJECTION);
   MainWnd.AxGisProjection.TargetDate:=strpas(TARGETDATE);
   MainWnd.AxGisProjection.TargetProjSettings:=strpas(TARGETPROJSETTINGS);
   MainWnd.AxGisProjection.TargetXOffset:=TARGETXOFFSET;
   MainWnd.AxGisProjection.TargetYOffset:=TARGETYOFFSET;
   MainWnd.AxGisProjection.TargetScale:=TARGETSCALE;
end;

// function is used to execute an mif-import
procedure EXECIMP;stdcall;
begin
   MainWnd.ExitNormal:=true;
   MainWnd.Show;
end;

function FREEDLL:boolean; stdcall;
var retVal:boolean;
begin
   retVal:=MainWnd.ExitNormal;
   MainWnd.Free;
   SetMiffileWnd.Free;
   result:=retVal;
end;

exports MIFIMPORT, CREATEIMP, SETMIFIMPSETTINGS, ADDIMPFILE,
        SETDBSETTINGS, SETPROJECTION, EXECIMP, FREEDLL;

begin
end.
