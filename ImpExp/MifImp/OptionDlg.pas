unit OptionDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Struct;

type
  TOptionWnd = class(TForm)
    Status: TPanel;
    OKBtn: TButton;
    UseLabelCB: TCheckBox;
    UseLabelPanel: TPanel;
    UseLabelLB: TListBox;
    FontDialog: TFontDialog;
    SetFontBtn: TButton;
    CurrentFontLabel1: TLabel;
    CurrentSizeLabel1: TLabel;
    CurrentFontLabel: TLabel;
    CurrentsizeLabel: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure UseLabelCBClick(Sender: TObject);
    procedure SetFontBtnClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    function DoLabel:boolean;
    function GetFont:string;
    function GetSize:double;
    function GetCol:string;
    procedure SetScreenSettings;
  end;

var
  OptionWnd: TOptionWnd;

implementation

{$R *.DFM}

uses StrFile, Maindlg, files, SymMapDlg;

var StrFileObj  :ClassStrfile;
    FileObj     :ClassFile;
    DatenbankObj:ClassDatenbank;

procedure TOptionWnd.SetScreenSettings;
begin
   Self.Caption:=StrfileObj.Assign(23); { Options for import }
   UseLabelCB.Caption:=StrFileObj.Assign(24); { Verwende Datenbankspalte als Label }
   CurrentFontLabel1.Caption:=StrFileObj.Assign(25); { Aktueller Font: }
   CurrentSizeLabel1.Caption:=StrFileObj.Assign(26); { Aktuelle Gr��e: }
   SetFontBtn.Caption:=StrFileObj.Assign(27); { �ndere Font und Gr��e }
end;

procedure TOptionWnd.FormCreate(Sender: TObject);
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   { Setzen der Maskeneinstellungen }
   DatenbankObj:=ClassDatenbank.Create;
end;

procedure TOptionWnd.FormActivate(Sender: TObject);
var i,DBanz:integer;
    spaltenname:string;
begin
   { Setzen der Maskeneinstellungen }
   Status.Caption:=StrFileObj.Assign(28)+Extractfilename(MainWnd.GetMifFilename); { Datei die gerade konvertiert wird:}
   UseLabelCB.checked:=false;UseLabelPanel.visible:=false;

   { Nun muss die Labelliste gef�llt werden }
   DBAnz:=DatenbankObj.SetDBFirst; UseLabelLB.Clear;
   for i:=1 to DBAnz do
   begin
      Spaltenname:=DatenbankObj.GetDBColumnname;
      if Spaltenname <> 'GRAFIK' then UseLabelLB.Items.add(Spaltenname);
      DatenbankObj.SetCurrentnext;
   end;
end;

procedure TOptionWnd.UseLabelCBClick(Sender: TObject);
begin
   if UseLabelCB.checked = true then UseLabelPanel.visible:=true else UseLabelPanel.visible:=false;
end;

procedure TOptionWnd.SetFontBtnClick(Sender: TObject);
begin
   if FontDialog.Execute then
   begin
      { Nun muss der neue Font und die neue Gr��e ermittelt werden }
      CurrentFontLabel.Font:=FontDialog.Font;
      CurrentFontLabel.Caption:=FontDialog.Font.Name;
      CurrentFontLabel.Font.size:=FontDialog.Font.Size;
      CurrentSizeLabel.Caption:=inttostr(CurrentFontLabel.Font.size);
   end;
end;

function TOptionWnd.DoLabel:boolean;
begin
   Result:=UseLabelCB.checked;
end;

function TOptionWnd.GetFont:string;
begin
   Result:=CurrentFontLabel.Caption;
end;

function TOptionWnd.GetSize:double;
begin
   Result:=Strtofloat(CurrentSizeLabel.Caption);
end;

function TOptionWnd.GetCol:string;
var i:integer;
    Spalte:string;
begin
   Spalte:='';
   for i:=0 to UseLabelLB.Items.Count - 1 do
   begin
      if UseLabelLB.selected[i] = true then Spalte:=UseLabelLB.Items[i];
   end;
   Result:=Spalte;
end;

end.
