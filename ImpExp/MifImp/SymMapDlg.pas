unit SymMapDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, ExtCtrls;

type
  TSymMapWnd = class(TForm)
    MifIdxLb: TListBox;
    Label1: TLabel;
    Panel1: TPanel;
    PageControl1: TPageControl;
    TextTabSheet: TTabSheet;
    SymbolTabSheet: TTabSheet;
    FontLb: TListBox;
    SymbolLb: TListBox;
    OKBtn: TButton;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MifIdxLbClick(Sender: TObject);
    procedure FontLbClick(Sender: TObject);
    procedure SymbolLbClick(Sender: TObject);
  private
    { Private declarations }
    procedure DisplaySymbolRefSettings;
  public
    { Public declarations }
    procedure SetScreenSettings;
  end;

var
  SymMapWnd: TSymMapWnd;

implementation

uses StrFile, Maindlg, struct;

var StrFileObj     :ClassStrfile;
{$R *.DFM}

procedure TSymMapWnd.FormCreate(Sender: TObject);
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
end;

procedure TSymMapWnd.SetScreenSettings;
begin
   Self.Caption:=StrfileObj.Assign(60);           // Symbol mapping
   Label1.Caption:=StrfileObj.Assign(61);         // Mapinfo
   Label2.Caption:=StrfileObj.Assign(62);         // WinGIS
   TextTabSheet.Caption:=StrfileObj.Assign(63);   // Text
   SymbolTabSheet.Caption:=StrfileObj.Assign(64); // Symbol
   OkBtn.Caption:=StrFileObj.Assign(4);           // Ok
end;

procedure TSymMapWnd.FormShow(Sender: TObject);
var Fontname,GisSymbolName:string;
    SymIdx:integer;
    i:integer;
    SymFont:string;
begin
   // fill the MifList
   MainWnd.ResetCurrentSymbolRef;
   MifIdxLb.Items.Clear;
   while (MainWnd.GetCurrentSymbolRefData(Fontname, SymIdx, GisSymbolName)) do
   begin
      MifIdxLb.Items.Add(inttostr(SymIdx));
   end;

   // HUGO
   for i:=1 to 400 do
   begin
      MifIdxLb.Items.Add(inttostr(i));
   end;
   // HUGO

   // fill the FontList
   FontLb.Items.Clear;
   for i:=0 to Screen.Fonts.Count-1 do
   begin
      SymFont:=Screen.Fonts[i];
      FontLb.Items.Add(SymFont);
   end;

   // check if symbols exist
   if (PageControl1.PageCount = 2) then
   begin
      if SymbolLb.Items.count = 0 then
         PageControl1.Pages[1].Destroy; // destroy the SymbolTabSheet
   end;

   MifIdxLb.ItemIndex:=0; // select first item and display the settings
   DisplaySymbolRefSettings;
end;

procedure TSymMapWnd.MifIdxLbClick(Sender: TObject);
begin
   DisplaySymbolRefSettings;
end;

procedure TSymMapWnd.DisplaySymbolRefSettings;
var i,j:integer;
    symref:p_SymbolRef_record;
    found:boolean;
begin
   for i:=0 to MifIdxLb.Items.Count-1 do
   begin
      if MifIdxLb.Selected[i] then
      begin
         // HUGO
         if FontLb.ItemIndex >= 0 then
            Panel1.Font.Name:=FontLb.Items[FontLb.ItemIndex];
         Panel1.Caption:=chr(i);
         exit;
         // HUGO

         symref:=MainWnd.GetSymbolRefDataByIdx(i);
         if symref^.GisSymname = '' then
         begin
            // symbol should be displayed as Text
            PageControl1.ActivePage:=TextTabSheet;
            // select the font that will be used
            found:=FALSE;
            for j:=0 to FontLb.Items.Count-1 do
            begin
               if FontLb.Items[j] = symref^.Fontname then
               begin
                  FontLb.ItemIndex:=j;
                  found:=TRUE;
                  break;
               end;
            end;
            if not found then
            begin
               Showmessage(StrfileObj.Assign(56) + ' '+ symref^.Fontname +' '+ StrfileObj.Assign(57)); // Symbol font not known ... Symbol will be inserted as point object
               symref^.Fontname:='Wingdings';
               for j:=0 to FontLb.Items.Count-1 do
               begin
                  if FontLb.Items[j] = symref^.Fontname then
                  begin
                     FontLb.ItemIndex:=j;
                     break;
                  end;
               end;
            end;
            Panel1.Font.Name:=symref^.Fontname;
            Panel1.Caption:=chr(SymRef^.SymIdx);
         end
         else
         begin
            PageControl1.ActivePage:=SymbolTabSheet;
            for j:=0 to SymbolLb.Items.Count-1 do
            begin
               if SymbolLb.Items[j] = symref^.GisSymname then
               begin
                  SymbolLb.ItemIndex:=j;
                  break;
               end;
            end;
            Panel1.Caption:='';
         end;
         break;
      end;
   end;
end;

procedure TSymMapWnd.FontLbClick(Sender: TObject);
var symref:p_SymbolRef_record;
    curfont:string;
begin
   // first get the symbol reference
   symref:=MainWnd.GetSymbolRefDataByIdx(MifIdxLb.ItemIndex);
   curfont:=FontLb.Items[FontLb.ItemIndex];
   symref^.Fontname:=curfont;
   symref^.GisSymName:='';
   DisplaySymbolRefSettings;
end;

procedure TSymMapWnd.SymbolLbClick(Sender: TObject);
var symref:p_SymbolRef_record;
    symname:string;
begin
   // first get the symbol reference
   symref:=MainWnd.GetSymbolRefDataByIdx(MifIdxLb.ItemIndex);
   symname:=SymbolLb.Items[SymbolLb.ItemIndex];
   symref^.GisSymname:=symname;
   DisplaySymbolRefSettings;
end;

end.
