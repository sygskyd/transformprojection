object OptionWnd: TOptionWnd
  Left = 211
  Top = 208
  BorderStyle = bsDialog
  Caption = 'OptionWnd'
  ClientHeight = 188
  ClientWidth = 544
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Status: TPanel
    Left = 8
    Top = 8
    Width = 529
    Height = 25
    Caption = 'File that currently is worked: TEST.MIF'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object OKBtn: TButton
    Left = 443
    Top = 158
    Width = 93
    Height = 23
    Caption = 'Ok'
    Default = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 1
  end
  object UseLabelCB: TCheckBox
    Left = 8
    Top = 40
    Width = 521
    Height = 17
    Caption = 'Use Database column as label'
    TabOrder = 2
    OnClick = UseLabelCBClick
  end
  object UseLabelPanel: TPanel
    Left = 8
    Top = 64
    Width = 529
    Height = 89
    TabOrder = 3
    object CurrentFontLabel1: TLabel
      Left = 211
      Top = 8
      Width = 61
      Height = 13
      Caption = 'Current Font:'
    end
    object CurrentSizeLabel1: TLabel
      Left = 211
      Top = 32
      Width = 60
      Height = 13
      Caption = 'Current Size:'
    end
    object CurrentFontLabel: TLabel
      Left = 296
      Top = 8
      Width = 88
      Height = 16
      Caption = 'Courier New'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
    end
    object CurrentsizeLabel: TLabel
      Left = 296
      Top = 32
      Width = 12
      Height = 13
      Caption = '10'
    end
    object UseLabelLB: TListBox
      Left = 8
      Top = 8
      Width = 185
      Height = 73
      ItemHeight = 13
      TabOrder = 0
    end
    object SetFontBtn: TButton
      Left = 211
      Top = 49
      Width = 161
      Height = 33
      Caption = 'Change Font and size'
      TabOrder = 1
      OnClick = SetFontBtnClick
    end
  end
  object FontDialog: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MinFontSize = 0
    MaxFontSize = 0
    Left = 392
    Top = 96
  end
end
