�
 TMAINWND 0k  TPF0TMainWndMainWndLeft� Top� BorderStylebsDialogCaptionPROGIS AMP-Import 2000ClientHeight�ClientWidth:Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSystem
Font.Style 	FormStylefsStayOnTopOldCreateOrder	PositionpoScreenCenter
OnActivateFormActivateOnCreate
FormCreate	OnDestroyFormDestroyOnShowFormShowPixelsPerInchx
TextHeight TPanelPanel1Left
Top
Width'Height}CaptionPanel1TabOrder  TLabelProgresslabelLeft
Top
WidthwHeightCaptionProgressmessagesFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TGaugeProzessbalkenLeft
Top@WidthHeightProgress   TListBoxStatusLBLeft
TopWidthHeight� Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontTabOrder   TButtonSaveReportBtnLeft� TopWidth� HeightCaptionSave reportEnabledFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickSaveReportBtnClick   TButtonOKBtnLeft6Top�WidthtHeightCaptionOKDefault	EnabledFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ModalResult
ParentFontTabOrderOnClick
OKBtnClick  TButton	CancelBtnLeft�Top�Width~HeightCancel	Caption	AbbrechenFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ModalResult
ParentFontTabOrderOnClickCancelBtnClick  TAXImpExpDbcAXImpExpDbcObjLeft
Top�WidthHeight
ParentFontTabOrderOnDialogSetupedAXImpExpDbcObjDialogSetupedOnRequestImpLayerIndex"AXImpExpDbcObjRequestImpLayerIndexOnRequestExpLayerIndex"AXImpExpDbcObjRequestExpLayerIndexOnRequestImpProjectName#AXImpExpDbcObjRequestImpProjectNameOnRequestExpProjectName#AXImpExpDbcObjRequestExpProjectNameOnRequestImpProjectGUIDs$AXImpExpDbcObjRequestImpProjectGUIDsControlData
�   TPF0TPanel Left
Top�WidthHeightCaptionDbCFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont    TAxGisProjectionAxGisProjectionLeft2Top�Width?Height
ParentFontTabOrder	OnWarningAxGisProjectionWarningOnErrorAxGisProjectionErrorControlData
�   TPF0TPanel Left2Top�Width?HeightCaptionGisProFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont    TTimer
CloseTimerEnabledIntervaldOnTimerCloseTimerTimerLeft� Top0  TSaveDialogSaveDialogRep
DefaultExt*.repFileName*.repFilterImport report file|*.repLeft8Top�   TTimerSetupWindowTimerEnabledIntervaldOnTimerSetupWindowTimerTimerLeft� Top0   