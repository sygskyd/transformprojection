unit Maindlg;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Strfile, StdCtrls, Buttons, ExtCtrls, Gauges, Db, WinOSInfo,
  DBTables, windows, OleCtrls, AXImpExpDbcXControl_TLB, AxGisPro_TLB, AxDLL_TLB;

const
       // number of objects that can be imported in demo-mode
       MaxObjectsInDemoMode = 3000;

       //registration modes
       NO_LICENCE        = 0;
       FULL_LICENCE      = 1;
       DEMO_LICENCE      = 2;

       // Object types
       ot_Pixel     = 1;
       ot_Poly      = 2;
       ot_LabelText = 3;
       ot_CPoly     = 4;
       ot_Group     = 5;
       ot_Proj      = 6;
       ot_Text      = 7;
       ot_Symbol    = 8;
       ot_Spline    = 9;
       ot_Image     = 10;
       ot_MesLine   = 11;
       ot_Circle    = 12;
       ot_Arc       = 13;
       ot_OLEObj    = 14;
       ot_SymDef    = 15;
       ot_RText     = 16;
       ot_BusGraph  = 17;
       ot_Layer     = 18;

       //TextSTyles
       ts_Left           =  0;
       ts_Center         =  1;
       ts_Right          =  2;
       ts_Italic         =  4;
       ts_Bold           =  8;
       ts_Underl         = 16;
       ts_FixHeight      = 32;
       ts_Transparent    = 64;

       //LineTypes
       lt_Solid     = 0;
       lt_Dash      = 1;
       lt_Dot       = 2;
       lt_DashDot   = 3;
       lt_DashDDot  = 4;
       lt_UserDefined    = 1000;

       //FillTypes
       pt_NoPattern = 0;
       pt_FDiagonal = 1;
       pt_Cross     = 2;
       pt_DiagCross = 3;
       pt_BDiagonal = 4;
       pt_Horizontal= 5;
       pt_Vertical  = 6;
       pt_Solid     = 7;
       pt_UserDefined = 8;

       stDrawingUnits    = 0;
       stScaleInDependend= 1;
       stProjectScale    = 2;
       stPercent         = 3;

type
  TMainWnd = class(TForm)
    CloseTimer: TTimer;
    Panel1: TPanel;
    Progresslabel: TLabel;
    Prozessbalken: TGauge;
    StatusLB: TListBox;
    SaveReportBtn: TButton;
    OKBtn: TButton;
    CancelBtn: TButton;
    SaveDialogRep: TSaveDialog;
    SetupWindowTimer: TTimer;
    AXImpExpDbcObj: TAXImpExpDbc;
    AxGisProjection: TAxGisProjection;
    procedure FormCreate(Sender: TObject);
    procedure StartBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure CloseTimerTimer(Sender: TObject);
    procedure SaveReportBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AXImpExpDbcObjDialogSetuped(Sender: TObject);
    procedure AxGisProjectionWarning(Sender: TObject;
      const Info: WideString);
    procedure AxGisProjectionError(Sender: TObject;
      const Info: WideString);
    procedure AXImpExpDbcObjRequestExpLayerIndex(Sender: TObject;
      const Layername: WideString; var LayerIndex: Integer);
    procedure AXImpExpDbcObjRequestExpProjectName(Sender: TObject;
      var ProjectName: WideString);
    procedure AXImpExpDbcObjRequestImpLayerIndex(Sender: TObject;
      const Layername: WideString; var LayerIndex: Integer);
    procedure AXImpExpDbcObjRequestImpProjectName(Sender: TObject;
      var ProjectName: WideString);
    procedure AXImpExpDbcObjRequestImpProjectGUIDs(Sender: TObject;
      var StartGUID, CurrentGUID: WideString);
  private
    { Private-Deklarationen }
    StrFileObj :ClassStrfile;
    Pass       :integer;

    Bytes2work  :longint;
    Bytesworked :longint;
    Aktiv       :boolean;
    Abort       :boolean;
    FirstActivate:boolean;
    ObjectsExported:longint;

    DestinationDocument  :IDocument;

    NewSymbolDef         :ISymbolDef;

    SymbolDefMap         :array of integer;
    SymbolDefMapCnt      :integer;


    function CalculateElementsToExport:integer;
    procedure Showpercent;
    procedure ShowPercentWithValues(Elems2Work:integer; ElemsWorked:integer);

    procedure RunPass(AktPass:integer);
    procedure ExecutePass;
    procedure CopySymbolLibrary;
    procedure CopyUserStyles;
    procedure CopyViews;
    function  GetNewLayerIndex(aIndex:integer):integer;

    function  GetImpLayernameByIndex(aIndex:integer):string;
    function  GetNumObjectsOfLayer(aLayername:string):integer;
    procedure CreateLeftTablesForImpLayers;
    procedure CheckProjectionSettings;
    procedure WriteProjectionSettings;
    procedure StartExport;
    procedure CopyDataBaseStructure(Layername:string);
    procedure DumpObjectStyle(aObject:Variant);

    function  CopyObject(var oldObject:IBase; var newObject:IBase; issymbol:boolean):boolean;
    function  CopyCPoly(var OldObject:IBase; var NewObject:IBase;issymbol:boolean):boolean;
    procedure ExportObject(var aObject:IBase; var aLayer:ILayer; const aLayername:string);
    procedure ExportSymbolObject(var aObject:IBase; var aSymbolDef:ISymbolDef);
    function  CheckLayerExists(Layername:string;var aLayer:ILayer):boolean;
    function  CreateLayer(Layer:ILayer):ILayer;
  public
    { Public-Deklarationen }
    App            :Variant;       { Pointer to AX-Handle }
    WinGISApp      :Variant;       { Pointer to WinGIS AX-Handle }
    SourceDocument :IDocument;   { Pointer to Project into that should be imported }
    ExitNormal     :boolean;

    DestinationAmpfilename:string; { Name of the Project into that sould be exported }

    RegMode:integer;
    LngCode:string;
    GisHandle:integer;

    { Procedures that are used to handle AX-Control }
    procedure ReadElement;
    procedure SetProjectionSettings;
    procedure CreateStringFile;
  end;

var
  MainWnd: TMainWnd;

implementation

uses comobj, SelAmpDlg;
{$R *.DFM}

procedure TMainWnd.CreateStringFile;
begin
   StrFileObj:=ClassStrfile.Create;
   StrFileObj.CreateList(OSInfo.WingisDir,LngCode,'AMPEXP');

   if REGMODE = DEMO_LICENCE then
      Self.Caption:=StrFileObj.Assign(57)           // PROGIS AMP-Export 2000 Limited Mode
   else
      Self.Caption:=StrFileObj.Assign(1);           // PROGIS AMP-Export 2000

   ProgressLabel.Caption:=StrFileObj.Assign(14); { Process messages }
   SaveReportBtn.Caption:=StrFileObj.Assign(15); { Save report      }
   OkBtn.Caption:=StrFileObj.Assign(12);         { Ok }
   CancelBtn.Caption:=StrFileObj.Assign(13);     { Cancel }
end;

procedure TMainWnd.FormCreate(Sender: TObject);
var aApp:IDispatch;
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   Self.Height:=1;
   Self.Width:=1;
   Self.Update;
   Aktiv:=false;
   FirstActivate:=true;
   ObjectsExported:=0;
   aApp:=CreateoleObject('AXDLL.Core');
   App:=aApp as ICore;
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

{*******************************************************************************
* PROZEDUR : StartBtnClick                                                     *
********************************************************************************
* Prozedur wird aufgerufen wenn der Start-Button im MainWnd                    *
* gedr�ckt wurde. Damit wird die Konvertierung gestartet.                      *
*                                                                              *
********************************************************************************}
procedure TMainWnd.StartBtnClick(Sender: TObject);
begin
   RunPass(1);
end;

function TMainWnd.GetNumObjectsOfLayer(aLayername:string):integer;
var numelems:integer;
    layercnt:integer;
    ALayer  :ILayer;
begin
   numelems:=0;
   for layercnt:=0 to DestinationDocument.Layers.Count-1 do
   begin
      ALayer:=DestinationDocument.Layers.Items[layercnt];
      if ALayer.Layername = aLayername then
      begin
         numelems:=numelems+ALayer.count;
         break;
      end;
   end;
   result:=numelems;
end;

function TMainWnd.CalculateElementsToExport:integer;
var numelems:integer;
    layercnt:integer;
    ALayer:ILayer;
    i:integer;
    Layername:string;
    doexportlayer:boolean;
begin
   numelems:=0;
   if SelAmpWnd.SelectedCB.checked then
   begin
       SourceDocument.CreateSelectedList;
       numelems:=SourceDocument.NrOfSelectedObjects;
       result:=numelems;
       exit;
   end;

   for layercnt:=0 to SourceDocument.Layers.Count-1 do
   begin
      ALayer:=SourceDocument.Layers.Items[layercnt];
      // check if layer may be exported
      if (SelAmpWnd.LayerCB.Checked) then
      begin
         Layername:=ALayer.Layername; doexportlayer:=false;
         for i:=0 to SelAmpWnd.LayerLB.Items.Count-1 do
         begin
            if (SelAmpWnd.LayerLB.Items[i] = Layername) and (SelAmpWnd.LayerLB.Selected[i]) then
            begin
               doexportlayer:=true;
               break;
            end;
         end;
      end
      else
         doexportlayer:=true;

      if doexportlayer then
         numelems:=numelems+ALayer.count;
   end;
   result:=numelems;
end;

procedure TMainWnd.CopyViews;
var ViewX:double;
    ViewY:double;
    aSight:ISight;
    i,j:integer;
    newSight:ISight;
    aViewLData:ISightLData;
    LayerIndex:integer;
    Elems2Work:integer;
begin
   Elems2Work:=SourceDocument.Sights.Count-1;
   StatusLB.Items.Add(StrFileObj.Assign(59)); // Export Views
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;

   for i:=0 to SourceDocument.Sights.Count-1 do
   begin
      aSight:=SourceDocument.Sights.Items[i];
      ViewX:=aSight.X/100;
      ViewY:=aSight.Y/100;
      AxGisProjection.Calculate(ViewX, ViewY);
      newSight:=DestinationDocument.CreateSight(aSight.Name, ViewX*100, ViewY*100,
                                         aSight.Height, aSight.Width,
                                         aSight.Rotation);
      newSight.Description:=aSight.Description;

      // copy the layer-data
      for j:=0 to aSight.Count -1 do
      begin
         aViewLData:=aSight.SightLData[j];
         LayerIndex:=GetNewLayerIndex(aViewLData.LIndex);
         if (j = 0) then
         begin
            // add dummy information for Active-Layer that has been created
            newSight.InsertSightLData(0, aViewLData.LFlags, aViewLData.GeneralMin, aViewLData.GeneralMax);
         end;
         if LayerIndex <> -1 then
         begin
            newSight.InsertSightLData(LayerIndex, aViewLData.LFlags, aViewLData.GeneralMin, aViewLData.GeneralMax);
         end;
      end;
      DestinationDocument.Sights.InsertSight(newSight);
      ShowPercentWithValues(Elems2Work, i);
      if Abort then break;
   end;
end;

function TMainWnd.GetNewLayerIndex(aIndex:integer):integer;
var Layername:string;
    retVal   :integer;
    found    :boolean;
    i:integer;
begin
   retVal:=-1;
   // first the Name of the Layer has to be got
   found:=FALSE;
   for i:=0 to SourceDocument.Layers.Count-1 do
   begin
      Layername:=SourceDocument.Layers.Items[i].LayerName;
      if (SourceDocument.Layers.Items[i].Index = aIndex) then
      begin
         found:=TRUE;
         break;
      end;
   end;
   if not found then
   begin
      result:=retVal;
      exit;
   end;

   // now the new index in the DestinationDocument has to be found
   for i:=0 to DestinationDocument.Layers.Count-1 do
   begin
      if (Layername = DestinationDocument.Layers.Items[i].LayerName) then
      begin
         retVal:=DestinationDocument.Layers.Items[i].Index;
         break;
      end;
   end;
   result:=retVal;
end;

procedure TMainWnd.CopySymbolLibrary;
var i,j:integer;
    OldSymbolDef:ISymbolDef;
    ASymbolDef:ISymbolDef;
    AObject:IBase;
    found:boolean;
    OldSymbolDefIndex:integer;
    aSymbolDefName:string;
    Elems2Work:integer;
    ADisp:IDispatch;
begin
   SymbolDefMapCnt:=0;
   Elems2Work:=SourceDocument.Symbols.Count-1;

   StatusLB.Items.Add(StrFileObj.Assign(58)); // Export Symbol-Library
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;

   for i:=0 to SourceDocument.Symbols.Count-1 do
   begin
      if SourceDocument.Symbols.Count = 0 then break;
      ADisp:=SourceDocument.Symbols.Items[i];
      OldSymbolDef:=ADisp as ISymbolDef;

      OldSymbolDefIndex:=OldSymboldef.Index-600000000;
      if SymbolDefMapCnt < OldSymbolDefIndex+1 then
         SetLength(SymbolDefMap,OldSymbolDefIndex+1);

      // check if symbol exists.
      found:=false;
      for j:=0 to DestinationDocument.Symbols.Count-1 do
      begin
         ASymbolDef:=DestinationDocument.Symbols.Items[j];
         if ASymbolDef.Name = OldSymbolDef.Name then
         begin
            SymbolDefMap[OldSymbolDefIndex]:=ASymbolDef.Index;
            found:=true;
            break;
         end;
      end;
      if not found then
      begin
         // the symbol has to be created in the new project
         NewSymbolDef:=DestinationDocument.CreateSymbolDef;
         aSymbolDefName:=OldSymbolDef.Name;
         NewSymbolDef.Name:=OldSymbolDef.Name;
         NewSymbolDef.DefaultSizeType:=OldSymbolDef.DefaultSizeType;
         NewSymbolDef.DefaultSize:=OldSymbolDef.DefaultSize;
         // copy the definition line
         NewSymbolDef.RefLine.X1:=OldSymbolDef.RefLine.X1;
         NewSymbolDef.RefLine.Y1:=OldSymbolDef.RefLine.Y1;
         NewSymbolDef.RefLine.X2:=OldSymbolDef.RefLine.X2;
         NewSymbolDef.RefLine.Y2:=OldSymbolDef.RefLine.Y2;
         NewSymbolDef.RefPoint.x:=oldSymbolDef.RefPoint.x;
         NewSymbolDef.RefPoint.y:=oldSymbolDef.RefPoint.y;

         // now the elements of the Symboldefinition have to be copied
         for j:=0 to OldSymbolDef.Count-1 do
         begin
            AObject:=OldSymbolDef.Items[j];
            ExportSymbolObject(AObject,oldSymbolDef);
         end;

         // insert new symbol
         DestinationDocument.Symbols.InsertSymbolDef(NewSymbolDef);
         SymbolDefMap[OldSymbolDefIndex]:=NewSymbolDef.Index;
      end;
      ShowPercentWithValues(Elems2Work, i);
      if Abort then
         break;
   end;
end;

procedure TMainWnd.CopyDataBaseStructure(Layername:string);
var colname:widestring;
    ColType :TOleEnum;
    collength:integer;
begin
   // copy the database structure
   MainWnd.AXImpExpDbcObj.CheckAndCreateExpTable(Layername);
   MainWnd.AxImpExpDbcObj.CheckAndCreateImpTable(Layername);
   MainWnd.AxImpExpDbcObj.AddImpIdColumn(Layername);
   while MainWnd.AXImpExpDbcObj.GetExportColumn(Layername,colname,coltype,collength) do
   begin
      MainWnd.AXImpExpDbcObj.AddImpColumn(Layername, colname, coltype, collength);
   end;
   MainWnd.AXImpExpDbcObj.OpenExpTable(Layername);
   MainWnd.AXImpExpDbcObj.CreateImpTable(Layername);
   MainWnd.AXImpExpDbcObj.OpenImpTable(Layername);
end;

procedure TMainWnd.ReadElement;
var layercnt:integer;
    ALayer:ILayer;
    New_Layer:ILayer;
    oldObject:IBase;
    i,cnt:integer;
    Layername:string;
    doexportlayer:boolean;
    MayExportObject:boolean;
    SelectedLayername:string;
begin
   MayExportObject:=true;
   // check if only selected objects should be exported
   if SelAmpWnd.SelectedCB.checked then
   begin
      SelectedLayername:='';
      for cnt:=0 to SourceDocument.NrOfSelectedObjects-1 do
      begin
         if Abort then exit;
         if SourceDocument.NrOfSelectedObjects = 0 then break;
         oldObject:=SourceDocument.SelItems[cnt];
         if oldObject.ObjectType = ot_Layer then // Layer Object received
         begin
            aLayer:=oldObject as ILayer;
            Layername:=aLayer.Layername;
            if Layername <> SelectedLayername then
            begin
               if SelectedLayername <> '' then
               begin
                  AXImpExpDbcObj.CloseExpTables;
                  AxImpExpDbcObj.CloseImpTables;
               end;
               SelectedLayername:=Layername;
               // first an entry for the layer has to be created
               if not CheckLayerExists(aLayer.Layername,New_Layer) then
                  New_Layer:=CreateLayer(aLayer);

               Layername:=New_Layer.Layername;
               StatusLB.Items.Add(StrFileObj.Assign(60) + Layername); // Export Project Data of Layer:
               StatusLB.ItemIndex := StatusLB.Items.Count-1;
               StatusLB.Update;

               CopyDatabaseStructure(Layername);
            end;
            Bytesworked:=Bytesworked+1;
            ShowPercent;
         end
         else
         begin
            ExportObject(OldObject,new_Layer,Layername);
            Bytesworked:=Bytesworked+1;
            ShowPercent;
            ObjectsExported:=ObjectsExported+1;

            // check if in demomode
            if (RegMode = DEMO_LICENCE) and (ObjectsExported = MaxObjectsInDemoMode) then
            begin
               StatusLB.Items.Add(inttostr(MaxObjectsInDemoMode)+' '+StrfileObj.Assign(46));  //  objects exported, following objects will be ignored
               StatusLB.ItemIndex := StatusLB.Items.Count-1;
               StatusLB.Update;
               Bytesworked:=Bytes2work;
               MayExportObject:=false;
               break;
            end;
         end;
      end;
      if SelectedLayername <> '' then
      begin
         AXImpExpDbcObj.CloseExpTables;
         AxImpExpDbcObj.CloseImpTables;
      end;
      if not Abort then
         SourceDocument.DestroySelectedList;
      exit;
   end;

   // iterate through all layers
   for layercnt:=0 to SourceDocument.Layers.Count-1 do
   begin
      if Abort then exit;
      if not MayExportObject then exit;

      ALayer:=SourceDocument.Layers.Items[layercnt];
      doexportLayer:=false;
      // now it will be checked if this layer should be exported
      if (SelAmpWnd.LayerCB.Checked) then
      begin
         Layername:=ALayer.Layername; doexportlayer:=false;
         for i:=0 to SelAmpWnd.LayerLB.Items.Count-1 do
         begin
            if (SelAmpWnd.LayerLB.Items[i] = Layername) and (SelAmpWnd.LayerLB.Selected[i]) then
            begin
               doexportlayer:=true;
               break;
            end;
         end;
      end
      else
         doexportlayer:=true;

      if doexportlayer then
      begin
         if not CheckLayerExists(ALayer.Layername,New_Layer) then
            New_Layer:=CreateLayer(ALayer);

         Layername:=New_Layer.Layername;
         StatusLB.Items.Add(StrFileObj.Assign(60) + Layername); // Export Project Data of Layer:
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         CopyDatabaseStructure(Layername);
         // read elements of layer
         for cnt:=0 to ALayer.count-1 do
         begin
            if Abort then exit;
            if ALayer.Count = 0 then break;
            OldObject:=ALayer.Items[cnt];
            ExportObject(OldObject,new_Layer,Layername);
            Bytesworked:=Bytesworked+1;
            ShowPercent;
            ObjectsExported:=ObjectsExported+1;

            // check if in demomode
            if (RegMode = DEMO_LICENCE) and (ObjectsExported = MaxObjectsInDemoMode) then
            begin
               StatusLB.Items.Add(inttostr(MaxObjectsInDemoMode)+' '+StrfileObj.Assign(46));  //  objects exported, following objects will be ignored
               StatusLB.ItemIndex := StatusLB.Items.Count-1;
               StatusLB.Update;
               Bytesworked:=Bytes2work;
               MayExportObject:=false;
               break;
            end;
         end;
         AXImpExpDbcObj.CloseExpTables;
         AxImpExpDbcObj.CloseImpTables;
      end;
   end;
end;

procedure TMainWnd.ExportSymbolObject(var aObject:IBase; var aSymbolDef:ISymbolDef);
var newObject:IBase;
begin
   if aObject = nil then exit;
   // export symbol-definition
   if CopyObject(aObject, newObject, TRUE {is for symbols} ) then begin
        NewSymbolDef.InsertSymbolItem(newObject);
   end;
end;

procedure TMainWnd.ExportObject(var aObject:IBase; var aLayer:ILayer; const aLayername:string);
var newObject:IBase;
    oldIndex :integer;
    newIndex :integer;
    ColName,ColInfo:widestring;
begin
   if aObject = nil then exit;
   if CopyObject(aObject, newObject, FALSE) then
   begin
      oldIndex:=aObject.Index;
      newObject.GUID:=aObject.GUID;
      aLayer.InsertObject(newObject);
      newIndex:=newObject.Index;
      // now the database information will be evaluated
      if AXImpExpDbcObj.ExpDbMode = 0 then exit;
      if AXImpExpDbcObj.DoExportData(aLayername,oldIndex) then
      begin
         AxImpExpDbcObj.AddImpIdInfo(aLayername, newIndex);
         while AXImpExpDbcObj.GetExportData(aLayername, ColName, ColInfo) do
         begin
            AxImpExpDbcObj.AddImpInfo(aLayername, ColName, ColInfo);
         end;
      end;
   end;
end;

function TMainWnd.CopyObject(var oldObject:IBase; var newObject:IBase; issymbol:boolean):boolean;
var i:integer;
    X:double;
    Y:double;

    DoSkipObject:boolean;

    aHeight:integer;
    aWidth:integer;
    OldSymIdx:integer;
    PrimAxis:integer;

    oldPoint:IPoint;
    newPoint:IPoint;
    oldPixel:IPixel;
    newPixel:IPixel;

    oldPoly :IPolyline;
    newPoly :IPolyline;

    oldSpline:ISpline;
    newSpline:ISpline;

    oldArc:IArc;
    newArc:IArc;

    oldText:IText;
    newText:IText;

    oldCircle:ICircle;
    newCircle:ICircle;

    oldSymbol:ISymbol;
    newSymbol:ISymbol;

    oldImage:IImage;
    newImage:IImage;

    oldMesLine:IMesLine;
    newMesLine:IMesLine;

    aObjectStyleDisp:IDispatch;
begin
   DoSkipObject:=false;
   if (oldObject.ObjectType = ot_Pixel) then      // copy point object
   begin
      oldPixel:=oldObject as IPixel;
      newPixel:=DestinationDocument.CreatePixel;
      newPoint:=DestinationDocument.CreatePoint;
      X:=oldPixel.Position.X / 100; Y:=oldPixel.Position.Y / 100;
      if not issymbol then    // calculate the projection
         AxGisProjection.Calculate(X, Y);
      newPoint.X:=round(X*100);
      newPoint.Y:=round(Y*100);
      newPixel.Position:=newPoint;

      // copy objectstyle of Pixel
      aObjectStyleDisp:=oldPixel.ObjectStyle;
      if aObjectStyleDisp <> nil then
         newPixel.ObjectStyle:=oldPixel.ObjectStyle;

      newObject:=newPixel;
   end
   else if (oldObject.ObjectType = ot_Cpoly) then // copy area object
   begin
      if not CopyCPoly(oldObject, newObject, issymbol) then DoSkipObject:=TRUE;
   end
   else if (oldObject.ObjectType = ot_poly) then // copy line object
   begin
      oldPoly:=oldObject as IPolyline;
      newPoly:=DestinationDocument.CreatePolyline;
      // create new point list
      for i:=0 to oldPoly.Count - 1 do
      begin
         oldPoint:=oldPoly.Points[i];
         X:=oldPoint.X/100; Y:=oldPoint.Y/100;

         if not issymbol then       // calculate the projection
            AxGisProjection.Calculate(X, Y);

         newPoint:=DestinationDocument.CreatePoint;
         newPoint.X:=round(X*100); newPoint.Y:=round(Y*100);
         newPoly.InsertPoint(newPoint);
      end;

      // copy objectstyle of Polyline
      aObjectStyleDisp:=oldPoly.ObjectStyle;
      if aObjectStyleDisp <> nil then
         newPoly.ObjectStyle:=oldPoly.ObjectStyle;

      newObject:=newPoly;
   end
   else if (oldObject.ObjectType = ot_Spline) then // copy spline object
   begin
      oldSpline:=oldObject as ISpline;
      newSpline:=DestinationDocument.CreateSpline;
      // create new point list
      for i:=0 to oldSpline.Count - 1 do
      begin
         oldPoint:=oldSpline.Points[i];
         X:=oldPoint.X/100; Y:=oldPoint.Y/100;

         if not issymbol then // calculate the projection
            AxGisProjection.Calculate(X, Y);

         newPoint:=DestinationDocument.CreatePoint;
         newPoint.X:=round(X*100); newPoint.Y:=round(Y*100);
         newSpline.InsertPoint(newPoint);
      end;

      // copy objectstyle of Spline
      aObjectStyleDisp:=oldSpline.ObjectStyle;
      if aObjectStyleDisp <> nil then
         newSpline.ObjectStyle:=oldSpline.ObjectStyle;

      newObject:=newSpline;
   end
   else if (oldObject.ObjectType = ot_Arc) then // copy Arc object
   begin
      oldArc:=oldObject as IArc;
      newArc:=DestinationDocument.CreateArc;
      newPoint:=DestinationDocument.CreatePoint;
      X:=oldArc.Position.X / 100; Y:=oldArc.Position.Y / 100;
      if not issymbol then    // calculate the projection
         AxGisProjection.Calculate(X, Y);
      newPoint.X:=round(X*100);  newPoint.Y:=round(Y*100);
      newArc.Position:=newPoint;

      newArc.PrimaryAxis:=oldArc.PrimaryAxis;
      newArc.SecondaryAxis:=oldArc.SecondaryAxis;
      newArc.Angle:=oldArc.Angle;
      newArc.BeginAngle:=oldArc.BeginAngle;
      newArc.EndAngle:=oldArc.EndAngle;
      newArc.Radius:=oldArc.Radius;

      // copy objectstyle of Arc
      aObjectStyleDisp:=oldArc.ObjectStyle;
      if aObjectStyleDisp <> nil then
         newArc.ObjectStyle:=oldArc.ObjectStyle;

      newObject:=newArc;
   end
   else if (oldObject.ObjectType = ot_Text) then // copy text object
   begin
      oldText:=oldObject as IText;
      newText:=DestinationDocument.CreateText;
      newPoint:=DestinationDocument.CreatePoint;

      X:=oldText.Position.X / 100; Y:=oldText.Position.Y / 100;
      if not issymbol then    // calculate the projection
         AxGisProjection.Calculate(X, Y);

      newPoint.X:=round(X*100); newPoint.Y:=round(Y*100);
      newText.Position:=newPoint;

      newText.Angle:=oldText.Angle;
      newText.Align:=oldText.Align;
      newText.Width:=oldText.Width;
      newText.Text:=oldText.Text;
      newText.FontStyle:=oldText.FontStyle;
      newText.FontHeight:=oldText.FontHeight;
      newText.FontName:=oldText.FontName;

      // copy objectstyle of Text
      aObjectStyleDisp:=oldText.ObjectStyle;
      if aObjectStyleDisp <> nil then
         newText.ObjectStyle:=oldText.ObjectStyle;

      newObject:=newText;
   end
   else if (oldObject.ObjectType = ot_Circle) then // copy circle object
   begin
      oldCircle:=oldObject as ICircle;
      newCircle:=DestinationDocument.CreateCircle;
      newPoint:=DestinationDocument.CreatePoint;
      X:=oldCircle.Position.X / 100; Y:=oldCircle.Position.Y / 100;
      if not issymbol then    // calculate the projection
         AxGisProjection.Calculate(X, Y);
      newPoint.X:=round(X*100); newPoint.Y:=round(Y*100);
      newCircle.Position:=newPoint;
      PrimAxis:=oldCircle.PrimaryAxis;
      newCircle.PrimaryAxis:=PrimAxis;
      newCircle.SecondaryAxis:=PrimAxis;
      newCircle.Angle:=0; // circle has no Radius
      newCircle.Radius:=PrimAxis;

      // copy objectstyle of Circle
      aObjectStyleDisp:=oldCircle.ObjectStyle;
      if aObjectStyleDisp <> nil then
         newCircle.ObjectStyle:=oldCircle.ObjectStyle;

      newObject:=newCircle;
   end
   else if (oldObject.ObjectType = ot_Symbol) then // copy symbol object
   begin
      oldSymbol:=oldObject as ISymbol;
      newSymbol:=DestinationDocument.CreateSymbol;
      newPoint:=DestinationDocument.CreatePoint;

      X:=oldSymbol.Position.X / 100; Y:=oldSymbol.Position.Y / 100;
      if not issymbol then    // calculate the projection
         AxGisProjection.Calculate(X, Y);
      newPoint.X:=round(X*100); newPoint.Y:=round(Y*100);
      newSymbol.Position:=newPoint;

      //get new Symbolnr.
      OldSymIdx:=oldSymbol.SymIndex - 600000000;
      newSymbol.SymIndex:=SymbolDefMap[OldSymIdx];
      newSymbol.Size:=oldSymbol.Size;
      newSymbol.SizeType:=oldSymbol.SizeType;
      newSymbol.Angle:=oldSymbol.Angle;
      newSymbol.SymbolName:=oldSymbol.Symbolname;

      // copy objectstyle of Symbol
      aObjectStyleDisp:=oldSymbol.ObjectStyle;
      if aObjectStyleDisp <> nil then
         newSymbol.ObjectStyle:=oldSymbol.ObjectStyle;

      newObject:=newSymbol;
   end
   else if (oldObject.ObjectType = ot_Image) then // copy image object
   begin
      oldImage:=oldObject as IImage;
      if issymbol and (not FileExists(oldImage.Filename)) then
         DoSkipObject:=TRUE;

      newImage:=DestinationDocument.CreateImage(oldImage.Filename);
      X:=oldImage.X / 100; Y:=oldImage.Y / 100;
      if not issymbol then    // calculate the projection
         AxGisProjection.Calculate(X, Y);

      // get the height and width
      aWidth:=round(oldImage.Width / 100);
      aHeight:=round(oldImage.Height / 100);

      newImage.SetPosition(round(X*100), round(Y*100), round(aWidth*100), round(aHeight*100));

      // set image-style
      newImage.ShowOpt:=oldImage.ShowOpt;
      newImage.Options:=oldImage.Options;
      newImage.TransparencyType:=oldImage.TransparencyType;
      newImage.Transparency:=oldImage.Transparency;

      // copy objectstyle of Image
      aObjectStyleDisp:=oldImage.ObjectStyle;
      if aObjectStyleDisp <> nil then
         newImage.ObjectStyle:=oldImage.ObjectStyle;

      newObject:=newImage;
   end
   else if oldObject.ObjectType = ot_MesLine then // export meshurement line
   begin
      oldMesLine:=oldObject as IMesLine;
      newMesLine:=DestinationDocument.CreateMesLine;

      X:=oldMesLine.StartPoint.x / 100; Y:=oldMesLine.StartPoint.y / 100;
      if not issymbol then    // calculate the projection
         AxGisProjection.Calculate(X, Y);
      newMesLine.StartPoint.x:=round(X*100);
      newMesLine.StartPoint.y:=round(Y*100);

      X:=oldMesLine.EndPoint.x / 100; Y:=oldMesLine.EndPoint.y / 100;
      if not issymbol then    // calculate the projection
         AxGisProjection.Calculate(X, Y);
      newMesLine.EndPoint.x:=round(X*100);
      newMesLine.EndPoint.y:=round(Y*100);

      // copy objectstyle of meashurement line
      aObjectStyleDisp:=oldMesLine.ObjectStyle;
      if aObjectStyleDisp <> nil then
         newMesLine.ObjectStyle:=oldMesLine.ObjectStyle;

      newObject:=newMesLine;
   end
   else if oldObject.ObjectType = ot_LabelText then // a label-text should be exported
   begin
      StatusLB.Items.Add(StrfileObj.Assign(49));  // Warning: Label Text not supported!
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      DoSkipObject:=true;
   end
   else if oldObject.ObjectType = ot_Group then // a group object will be exported
   begin
      StatusLB.Items.Add(StrfileObj.Assign(50));  // Warning: Group objects not supported!
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      DoSkipObject:=true;
   end
   else if oldObject.ObjectType = ot_Proj then // a project object will be exported
   begin
      StatusLB.Items.Add(StrfileObj.Assign(51));  // Warning: Project objects not supported!
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      DoSkipObject:=true;
   end
   else if oldObject.ObjectType = ot_OLEObj then    // an ole object will be exported
   begin
      StatusLB.Items.Add(StrfileObj.Assign(52));  // Warning: OLE objects not supported!
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      DoSkipObject:=true;
   end
   else if oldObject.ObjectType = ot_RText then    // an RText object will be exported
   begin
      StatusLB.Items.Add(StrfileObj.Assign(53));  // Warning: Rtext objects not supported!
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      DoSkipObject:=true;
   end
   else if oldObject.ObjectType = ot_BusGraph then   // a Buisness Graphic object will be exported
   begin
      StatusLB.Items.Add(StrfileObj.Assign(54));   // Warning: Buisness graphic objects not supported!
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      DoSkipObject:=true;
   end
   else
      DoSkipObject:=true;

   result:=not DoSkipObject;
end;

procedure TMainWnd.DumpObjectStyle(aObject:Variant);
var DebugFile:TextFile;
    intval:longint;
    doubleval:double;
    AObjectStyleDisp:IDispatch;
begin
   // dump the object-style to a file (for debugging)

   AObjectStyleDisp:=aObject.ObjectStyle;
   if (AObjectStyleDisp = nil) then
      exit;

   AssignFile(DebugFile, OSInfo.TempDataDir + 'Debug.txt');
   Rewrite(DebugFile);
   // dump the FillStyle
   writeln(DebugFile, 'FillStyle:');
   intval:=aObject.ObjectStyle.FillStyle.BackColor; writeln(DebugFile, '   BackColor:'+inttostr(intval));
   intval:=aObject.ObjectStyle.FillStyle.ForeColor; writeln(DebugFile, '   ForeColor:'+inttostr(intval));
   intval:=aObject.ObjectStyle.FillStyle.Pattern;   writeln(DebugFile, '   Pattern  :'+inttostr(intval));
   doubleval:=aObject.ObjectStyle.FillStyle.Scale;  writeln(DebugFile, '   Scale    :'+floattostr(doubleval));
   intval:=aObject.ObjectStyle.FillStyle.ScaleType; writeln(DebugFile, '   ScaleType:'+inttostr(intval));
   // dump the LineStyle
   writeln(DebugFile, 'LineStyle:');
   intval:=aObject.ObjectStyle.LineStyle.Color;     writeln(DebugFile, '   Color    :'+inttostr(intval));
   intval:=aObject.ObjectStyle.LineStyle.FillColor; writeln(DebugFile, '   FillColor:'+inttostr(intval));
   doubleval:=aObject.ObjectStyle.LineStyle.Scale;  writeln(DebugFile, '   Scale    :'+floattostr(doubleval));
   intval:=aObject.ObjectStyle.LineStyle.Style;     writeln(DebugFile, '   Style    :'+inttostr(intval));
   doubleval:=aObject.ObjectStyle.LineStyle.Width;  writeln(DebugFile, '   Width    :'+floattostr(doubleval));
   intval:=aObject.ObjectStyle.LineStyle.WidthType; writeln(DebugFile, '   WidthType:'+inttostr(intval));

   // dump the SymbolFill
   writeln(DebugFile, 'SymbolFill:');
   doubleval:=aObject.ObjectStyle.SymbolFill.Angle;       writeln(DebugFile, '   Angle       :'+floattostr(doubleval));
   doubleval:=aObject.ObjectStyle.SymbolFill.Distance;    writeln(DebugFile, '   Distance    :'+floattostr(doubleval));
   intval:=aObject.ObjectStyle.SymbolFill.DistanceType;   writeln(DebugFile, '   DistanceType:'+inttostr(intval));
   intval:=aObject.ObjectStyle.SymbolFill.FillType;       writeln(DebugFile, '   FillType    :'+inttostr(intval));
   doubleval:=aObject.ObjectStyle.SymbolFill.Offset;      writeln(DebugFile, '   Offset      :'+floattostr(doubleval));
   intval:=aObject.ObjectStyle.SymbolFill.OffsetType;     writeln(DebugFile, '   OffsetType  :'+inttostr(intval));
   doubleval:=aObject.ObjectStyle.SymbolFill.Size;        writeln(DebugFile, '   Size        :'+floattostr(doubleval));
   intval:=aObject.ObjectStyle.SymbolFill.SizeType;       writeln(DebugFile, '   SizeType    :'+inttostr(intval));
   intval:=aObject.ObjectStyle.SymbolFill.Symbol;         writeln(DebugFile, '   Symbol      :'+inttostr(intval));
   doubleval:=aObject.ObjectStyle.SymbolFill.SymbolAngle; writeln(DebugFile, '   SymbolAngle :'+floattostr(doubleval));

   writeln(DebugFile, 'SymbolFill - FillStyle:');
   intval:=aObject.ObjectStyle.SymbolFill.FillStyle.BackColor; writeln(DebugFile, '   BackColor:'+inttostr(intval));
   intval:=aObject.ObjectStyle.SymbolFill.FillStyle.ForeColor; writeln(DebugFile, '   ForeColor:'+inttostr(intval));
   intval:=aObject.ObjectStyle.SymbolFill.FillStyle.Pattern;   writeln(DebugFile, '   Pattern  :'+inttostr(intval));
   doubleval:=aObject.ObjectStyle.SymbolFill.FillStyle.Scale;  writeln(DebugFile, '   Scale    :'+floattostr(doubleval));
   intval:=aObject.ObjectStyle.SymbolFill.FillStyle.ScaleType; writeln(DebugFile, '   ScaleType:'+inttostr(intval));

   writeln(DebugFile, 'SymbolFill - LineStyle:');
   intval:=aObject.ObjectStyle.SymbolFill.LineStyle.Color;     writeln(DebugFile, '   Color    :'+inttostr(intval));
   intval:=aObject.ObjectStyle.SymbolFill.LineStyle.FillColor; writeln(DebugFile, '   FillColor:'+inttostr(intval));
   doubleval:=aObject.ObjectStyle.SymbolFill.LineStyle.Scale;  writeln(DebugFile, '   Scale    :'+floattostr(doubleval));
   intval:=aObject.ObjectStyle.SymbolFill.LineStyle.Style;     writeln(DebugFile, '   Style    :'+inttostr(intval));
   doubleval:=aObject.ObjectStyle.SymbolFill.LineStyle.Width;  writeln(DebugFile, '   Width    :'+floattostr(doubleval));
   intval:=aObject.ObjectStyle.SymbolFill.LineStyle.WidthType; writeln(DebugFile, '   WidthType:'+inttostr(intval));
   CloseFile(DebugFile);
end;

function TMainWnd.CopyCPoly(var OldObject:IBase; var NewObject:IBase;issymbol:boolean):boolean;
var IslandList:IList;
    APoint:IPoint;
    RotatePoint:IPoint;
    IslandPoly:IPolygon;
    PointCount:integer;
    SkipIndex :integer;
    FirstIsland:boolean;
    IslandCount:integer;
    IsIsland:boolean;
    IslandInfoIdx:integer;
    NextIslandIdx:integer;
    X,Y:double;
    Cnt:integer;
    First_X,First_Y:double;
    rotatelastpoly:boolean;
    retVal:boolean;

    oldCPoly:IPolygon;
    newCPoly:IPolygon;

    aObjectStyleDisp:IDispatch;
begin
   retVal:=TRUE;
   PointCount:=0; SkipIndex:=0; FirstIsland:=TRUE; IsIsland:=FALSE;
   rotatelastpoly:=FALSE;
   oldCPoly:=oldObject as IPolygon;

   IsIsland:=(oldCPoly.IslandArea) and (oldCPoly.IslandCount > 0) and (oldCPoly.IslandInfo[0] <= oldCPoly.Count);
   if IsIsland then
   begin
      IslandCount:=oldCPoly.IslandCount;
      IslandInfoIdx:=0;
      NextIslandIdx:=oldCPoly.IslandInfo[IslandInfoIdx];
      // an island object has to be created
      IslandList:=DestinationDocument.CreateList;
   end;

   newCPoly:=DestinationDocument.CreatePolygon;

   // insert first point to new CPoly
   APoint:=oldCPoly.Points[0];
   X:=APoint.X/100; Y:=APoint.Y/100;

   if not issymbol then
   begin
      // calculate the projection
      AxGisProjection.Calculate(X, Y);
   end;

   First_X:=X; First_Y:=Y;
   APoint.X:=round(X*100); APoint.Y:=round(Y*100);

   newCPoly.InsertPoint(APoint);
   PointCount:=PointCount+1;

   for Cnt:=1 to oldCPoly.Count - 1 do
   begin
      if SkipIndex = 0 then
         PointCount:=PointCount + 1;
      if (IsIsland) and (PointCount = NextIslandIdx) then // next island area will be created
      begin
         IslandInfoIdx:=IslandInfoIdx+1;
         if FirstIsland then
            SkipIndex:=1
         else
            SkipIndex:=2;

         FirstIsland:=FALSE;
         if IslandInfoIdx >= IslandCount then
         begin
            // all islands have been inserted
            NextIslandIdx:=0;
            IslandInfoIdx:=IslandCount;
         end
         else
         begin
            // the polygon has to be inserted to the island
            newCPoly.ClosePoly;
            IslandList.Add(newCPoly);

            // and a new polygon has to be created
            newCPoly:=DestinationDocument.CreatePolygon;
            NextIslandIdx:=oldCPoly.IslandInfo[IslandInfoIdx];

            // insert point to polygon
            APoint:=oldCPoly.Points[Cnt+SkipIndex];
            X:=APoint.X/100; Y:=APoint.Y/100;

            if not issymbol then
            begin
               // calculate the projection
               AxGisProjection.Calculate(X, Y);
            end;

            if (IslandCount-1 = IslandInfoIdx) then
            begin
               // last polygon reached
               if (X = First_X) and (Y = First_Y) then
               begin
                  rotatelastpoly:=true;
                  RotatePoint:=APoint;
                  RotatePoint.X:=round(X*100); RotatePoint.Y:=round(Y*100);
               end;
            end;

            APoint.X:=round(X*100); APoint.Y:=round(Y*100);
            if not rotatelastpoly then
               newCPoly.InsertPoint(APoint);
            PointCount:=1;
         end;
      end
      else
      begin
         if (SkipIndex=0) then
         begin
            if (IsIsland) and (Cnt = oldCPoly.Count -2) then //avoid exporting last connectionline
               continue;
            // insert point to polygon
            APoint:=oldCPoly.Points[Cnt];
            X:=APoint.X/100; Y:=APoint.Y/100;

            if not issymbol then
            begin
               // calculate the projection
               AxGisProjection.Calculate(X, Y);
            end;
            APoint.X:=round(X*100); APoint.Y:=round(Y*100);
            newCPoly.InsertPoint(APoint);
         end
         else
            SkipIndex:=SkipIndex-1;
      end;
   end;

   if IsIsland then
   begin
      if rotatelastpoly then
         newCPoly.InsertPoint(RotatePoint);
      newCPoly.ClosePoly;

      // copy objectstyle of CPoly
      aObjectStyleDisp:=oldCPoly.ObjectStyle;
      if aObjectStyleDisp <> nil then
        newCPoly.ObjectStyle:=oldCPoly.ObjectStyle;

      IslandList.Add(newCPoly);
      //IslandPoly:=DestinationDocument.CreatePolygon;
      IslandPoly:=DestinationDocument.CreateIslandArea(IslandList);

      newObject:=IslandPoly;
   end
   else
   begin
      newCPoly.ClosePoly;

      // copy objectstyle of CPoly
      aObjectStyleDisp:=oldCPoly.ObjectStyle;
      if aObjectStyleDisp <> nil then
         newCPoly.ObjectStyle:=oldCPoly.ObjectStyle;

      newObject:=newCPoly;
   end;

   if PointCount < 4 then retVal:=FALSE; // a Poly must have at least 3 corners
   result:=retVal;
end;

{*******************************************************************************
* PROZEDUR : RunPass                                                           *
********************************************************************************
* Prozedur leitet den jeweiligen Konvertierungs-Pass ein.                      *
*                                                                              *
* PARAMETER: Pass -> Gibt den Konvertierungs-Pass an.                          *
*                                                                              *
********************************************************************************}
procedure TMainWnd.RunPass(AktPass:integer);
var aApp:IDispatch;
    aCore:ICore;
begin
   if AktPass = 1 then // read project
   begin
      { Setzen der Maskeneinstellungen }
      OkBtn.Enabled:=false; CancelBtn.Enabled:=true; SaveReportBtn.Enabled:=false;
      Aktiv:=true;

      aApp:=App;
      aCore:=aApp as ICore;

      DestinationDocument:=aCore.CreateDocument(DestinationAmpFilename);

      // check the projection-settings
      CheckProjectionSettings;
      WriteProjectionSettings;
      // display database settings and projectname
      StatusLB.items.add( AXImpExpDbcObj.ExpDbInfo);
      StatusLB.items.add( AXImpExpDbcObj.ImpDbInfo);
      StatusLB.Items.Add(StrFileObj.Assign(17) + Extractfilename(DestinationAmpfilename)); { Export to WinGIS Project: }

      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      // calculate number of to be exported objects
      Bytes2Work:=CalculateElementsToExport; Bytesworked:=0;

      Pass:=1;
      ExecutePass;
      exit;
   end;

   if AktPass = 2 then { Ende der Konvertierung }
   begin
      AXImpExpDbcObj.CloseImpTables;
      AXImpExpDbcObj.CloseExpTables;
      StatusLB.Items.Add(StrfileObj.Assign(18));  { Project correctly imported! }
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      aktiv:=false; Abort:=true;
      Aktiv:=false;
      CancelBtn.Enabled:=false; OkBtn.Enabled:=true;
      SaveReportBtn.Enabled:=true;
      DestinationDocument.Save;
   end;
end;

procedure TMainWnd.SetProjectionSettings;
begin
   // procedure is used to set the projection settings
   // of the current project to the GeoWnd
   AxGisProjection.WorkingPath:=OSInfo.WingisDir;
   AxGisProjection.SourceCoordinateSystem:=SourceDocument.Projection;
   AxGisProjection.TargetCoordinateSystem:=SourceDocument.Projection;
   AxGisProjection.SourceProjSettings:=SourceDocument.ProjectProjSettings;
   AxGisProjection.TargetProjSettings:='NONE';
   AxGisProjection.SourceProjection:=SourceDocument.ProjectProjection;
   AxGisProjection.TargetProjection:='NONE';
   AxGisProjection.SourceDate:=SourceDocument.Projectdate;
   AxGisProjection.TargetDate:='NONE';
   AxGisProjection.SourceXOffset:=SourceDocument.ProjectionXOffset;
   AxGisProjection.TargetXOffset:=SourceDocument.ProjectionXOffset;
   AxGisProjection.SourceYOffset:=SourceDocument.ProjectionYOffset;
   AxGisProjection.TargetYOffset:=SourceDocument.ProjectionYOffset;
   AxGisProjection.SourceScale:=SourceDocument.ProjectionScale;
   AxGisProjection.TargetScale:=SourceDocument.ProjectionScale;
   // if source and target is .amp - file
   // the used input coordinates and used output coordinates
   // for the projection calculation are in carthesic
   AxGisProjection.UsedInputCoordSystem:=cs_Carthesic;
   AxGisProjection.UsedOutputCoordSystem:=cs_Carthesic;
end;

procedure TMainWnd.CheckProjectionSettings;
var hstr:string;
    different:boolean;
begin
   different:=false;
   // function is used to check if the settings in the
   if (AxGisProjection.SourceCoordinateSystem <> SourceDocument.Projection) then
      different:=TRUE;

   if (AxGisProjection.SourceXOffset <> SourceDocument.ProjectionXOffset) then
      different:=TRUE;

   if (AxGisProjection.SourceYOffset <> SourceDocument.ProjectionYOffset) then
      different:=TRUE;

   if (AxGisProjection.SourceScale <> SourceDocument.ProjectionScale) then
      different:=TRUE;

   // check if the projections are different
   hstr:=SourceDocument.ProjectProjSettings;
   if hstr <> AxGisProjection.SourceProjSettings then
      different:=TRUE;

   hstr:=SourceDocument.ProjectProjection;
   if hstr <> AxGisProjection.SourceProjection then
      different:=TRUE;

   hstr:=SourceDocument.Projectdate;
   if hstr <> AxGisProjection.SourceDate then
      different:=TRUE;

   if different then
   begin
      Showmessage(StrfileObj.Assign(56)); // Warning: Projection does not fit to project projection
      AxGisProjection.SetupByDialog;
   end;
end;

procedure TMainWnd.WriteProjectionSettings;
begin
   // procedure is used to write projection
   DestinationDocument.Projection:=AxGisProjection.TargetCoordinateSystem;
   DestinationDocument.ProjectionXOffset:=AxGisProjection.TargetXOffset;
   DestinationDocument.ProjectionYOffset:=AxGisProjection.TargetYOffset;
   DestinationDocument.ProjectionScale:=AxGisProjection.TargetScale;

   if AxGisProjection.TargetProjSettings <> 'NONE' then
   begin
      DestinationDocument.ProjectProjSettings:=AxGisProjection.TargetProjSettings;
      DestinationDocument.ProjectProjection:=AxGisProjection.TargetProjection;
      DestinationDocument.Projectdate:=AxGisProjection.TargetDate;
   end
   else
   begin
      // copy projection from original project
      DestinationDocument.ProjectProjSettings:=SourceDocument.ProjectProjSettings;
      DestinationDocument.ProjectProjection:=SourceDocument.ProjectProjection;
      DestinationDocument.Projectdate:=SourceDocument.Projectdate;
   end;
end;

procedure TMainWnd.ShowPercentWithValues(Elems2Work:integer; ElemsWorked:integer);
var Prozent:integer;
begin
   if Elems2Work > 0 then Prozent:= Round (ElemsWorked / Elems2Work * 100 ) else Prozent:= 100;
   ProzessBalken.Progress :=Prozent;
   ProzessBalken.Update;
   Application.Processmessages;
end;

procedure TMainWnd.Showpercent;
var Prozent:integer;
begin
   if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
   ProzessBalken.Progress :=Prozent;
   ProzessBalken.Update;
   Application.Processmessages;
end;

{*******************************************************************************
* PROZEDUR : ExecutePass                                                       *
********************************************************************************
* Prozedur die zur Abarbeitung eines Konvertierungsschritts dient              *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure TMainWnd.ExecutePass;
var Prozent:integer;
    aLayername:string;
begin
   if Pass = 1 then // read data
   begin
      CopyUserStyles;
      CopySymbolLibrary;
      if Abort then exit;
      MainWnd.ReadElement;
      if (Bytesworked >= Bytes2work) and (Abort = false) then
      begin
         CopyViews;
         if Abort then exit;
         RunPass(2); exit;
      end;
      if Abort = true then exit;
   end;
end;

function TMainWnd.CreateLayer(Layer:ILayer):ILayer;
begin
   result:=DestinationDocument.Layers.InsertLayer(Layer,FALSE);
end;

function TMainWnd.CheckLayerExists(Layername:string;var aLayer:ILayer):boolean;
var dispatch:IDispatch;
    retVal:boolean;
begin
   dispatch:=DestinationDocument.Layers.GetLayerByName(Layername);
   if dispatch <> nil then retVal:=true else retVal:=false;
   if retVal then
      aLayer:=DestinationDocument.Layers.GetLayerByName(Layername);
   result:=retVal;
end;

procedure TMainWnd.StartExport;
begin
   if ExitNormal then
   begin
      AXImpExpDbcObj.WorkingDir:=OSInfo.WingisDir;
      AXImpExpDbcObj.LngCode:=LngCode;
      SelAmpWnd.GetProjectName(DestinationAmpfilename);
      RunPass(1);
   end;
end;

procedure TMainWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TMainWnd.CloseTimerTimer(Sender: TObject);
begin
   MainWnd.Close;
   SelAmpWnd.Close;
   SourceDocument.FinishImpExpRoutine(ExitNormal,'EXPORTMODULE');
   CloseTimer.Enabled:=false;
end;

procedure TMainWnd.SaveReportBtnClick(Sender: TObject);
var Datei:Textfile;
    Zeile:string;
    i:integer;
begin
   if SaveDialogRep.Execute then
   begin
      Assignfile(Datei,SaveDialogRep.Filename);
      Rewrite(Datei);
      for i:=0 to StatusLB.Items.Count-1 do
      begin
         Zeile:=StatusLB.items[i];
         Writeln(Datei,Zeile);
      end;
      Closefile(Datei);
   end;
end;

procedure TMainWnd.CancelBtnClick(Sender: TObject);
begin
   AXImpExpDbcObj.CloseImpTables;
   AXImpExpDbcObj.CloseExpTables;
   StatusLB.Items.Add(StrfileObj.Assign(36));  { Warning: Export canceled }
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;
   aktiv:=false; Abort:=true;
   CancelBtn.Enabled:=false;
   OkBtn.Enabled:=true;
   DestinationDocument.Save;
   SaveReportBtn.Enabled:=true;
   if SelAmpWnd.SelectedCB.checked then
      SourceDocument.DestroySelectedList;
end;

procedure TMainWnd.FormDestroy(Sender: TObject);
begin
   StrFileObj.Destroy;
end;

procedure TMainWnd.OKBtnClick(Sender: TObject);
begin
   CloseTimer.Enabled:=true;
end;

procedure TMainWnd.SetupWindowTimerTimer(Sender: TObject);
var IdbProcessMask:integer;
begin
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,SelAmpWnd.winleft,SelAmpWnd.wintop,SelAmpWnd.winwidth, SelAmpWnd.winheight,SWP_SHOWWINDOW);
   MainWnd.Height:=SelAmpWnd.winheight;
   MainWnd.Width:=SelAmpWnd.winwidth;
   Self.Repaint;
   SetupWindowTimer.Enabled:=false;
   IdbProcessMask:=SourceDocument.IdbProcessMask;
   SourceDocument.IdbProcessMask:=0;
   StartExport;
   SourceDocument.IdbProcessMask:=IdbProcessMask;
end;

procedure TMainWnd.FormShow(Sender: TObject);
begin
   AXImpExpDbcObj.visible:=FALSE;
   AxGisProjection.visible:=FALSE;
end;

procedure TMainWnd.CopyUserStyles;
var wlffilename:string;
begin
   wlffilename:=OSInfo.TempDataDir+'ampexp.wlf';
   // export the user defined styles from the OldDocument
   SourceDocument.ExportUserDefStylesToWlf(wlffilename);
   // and import it into the new document
   DestinationDocument.ImportUserDefStylesFromWlf(wlffilename);
   DeleteFile(PCHAR(wlffilename));
end;

procedure TMainWnd.AXImpExpDbcObjDialogSetuped(Sender: TObject);
begin
   SelAmpWnd.SourceDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ExpDbInfo;
   SelAmpWnd.DestDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ImpDbInfo;
   SelAmpWnd.PageControl.Enabled:=TRUE;
   SelAmpWnd.GeoBtn.Enabled:=TRUE;
   SelAmpWnd.DbBtn.Enabled:=TRUE;
   SelAmpWnd.OkBtn.Enabled:=SelAmpWnd.OkBtnMode;
   SelAmpWnd.CancelBtn.Enabled:=TRUE;
end;

procedure TMainWnd.AxGisProjectionWarning(Sender: TObject;
  const Info: WideString);
begin
   if not Aktiv then
   begin
      Showmessage(Info);
   end
   else
   begin
      StatusLB.Items.Add(Info);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
   end;
end;

procedure TMainWnd.AxGisProjectionError(Sender: TObject;
  const Info: WideString);
begin
   if not Aktiv then
   begin
      Showmessage(Info);
   end
   else
   begin
      StatusLB.Items.Add(Info);
      AXImpExpDbcObj.CloseImpTables;
      AXImpExpDbcObj.CloseExpTables;
      StatusLB.Items.Add(StrfileObj.Assign(36));  { Warning: Export canceled }
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      aktiv:=false; Abort:=true;
      CancelBtn.Enabled:=false;
      OkBtn.Enabled:=true;
      DestinationDocument.Save;
      SaveReportBtn.Enabled:=true;
      if SelAmpWnd.SelectedCB.checked then
         SourceDocument.DestroySelectedList;
   end;
end;

procedure TMainWnd.AXImpExpDbcObjRequestExpLayerIndex(Sender: TObject;
  const Layername: WideString; var LayerIndex: Integer);
var i:integer;
begin
   // the Layerindex of the Layername has to be evaluated
   for i:=0 to SourceDocument.Layers.Count-1 do
   begin
      if SourceDocument.Layers.Items[i].Layername = Layername then
      begin
         LayerIndex:=SourceDocument.Layers.Items[i].Index;
         break;
      end;
   end;
end;

procedure TMainWnd.AXImpExpDbcObjRequestExpProjectName(Sender: TObject;
  var ProjectName: WideString);
var ProjName:string;
begin
   ProjName:=SourceDocument.Name;
   ProjectName:=ProjName;
end;

procedure TMainWnd.AXImpExpDbcObjRequestImpLayerIndex(Sender: TObject;
  const Layername: WideString; var LayerIndex: Integer);
var i:integer;
begin
   // the Layerindex of the Layername has to be evaluated
   for i:=0 to DestinationDocument.Layers.Count-1 do
   begin
      if DestinationDocument.Layers.Items[i].Layername = Layername then
      begin
         LayerIndex:=DestinationDocument.Layers.Items[i].Index;
         break;
      end;
   end;
end;

function TMainWnd.GetImpLayernameByIndex(aIndex:integer):string;
var i:integer;
    retVal:string;
begin
   // the Layerindex of the Layername has to be evaluated
   retVal:='';
   for i:=0 to DestinationDocument.Layers.Count-1 do
   begin
      if DestinationDocument.Layers.Items[i].Index = aIndex then
      begin
         retVal:=DestinationDocument.Layers.Items[i].Layername;
         break;
      end;
   end;
   result:=retVal;
end;

procedure TMainWnd.CreateLeftTablesForImpLayers;
var i:integer;
    aLayername:string;
begin
   for i:=0 to DestinationDocument.Layers.Count-1 do
   begin
      aLayername:=DestinationDocument.Layers.Items[i].Layername;
      MainWnd.AXImpExpDbcObj.CheckAndCreateImpTable(aLayername);
      MainWnd.AXImpExpDbcObj.AddImpIdColumn(aLayername);
      MainWnd.AXImpExpDbcObj.CreateImpTable(aLayername);
      MainWnd.AXImpExpDbcObj.OpenImpTable(aLayername);
      MainWnd.AXImpExpDbcObj.CloseImpTables;
   end;
end;

procedure TMainWnd.AXImpExpDbcObjRequestImpProjectName(Sender: TObject;
  var ProjectName: WideString);
var ProjName:string;
begin
   SelAmpWnd.GetProjectName(ProjName);
   ProjectName:=ProjName;
end;

procedure TMainWnd.AXImpExpDbcObjRequestImpProjectGUIDs(Sender: TObject;
  var StartGUID, CurrentGUID: WideString);
var aStartGUID, aCurrentGUID:string;
begin
   aStartGUID:=DestinationDocument.StartProjectGUID;
   aCurrentGUID:=DestinationDocument.CurrentProjectGUID;
   StartGUID:=aStartGUID;
   CurrentGUID:=aCurrentGUID;
end;

end.
