object SelAmpWnd: TSelAmpWnd
  Left = 256
  Top = 211
  BorderStyle = bsDialog
  Caption = 'PROGIS AMP-Export 2000'
  ClientHeight = 427
  ClientWidth = 561
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 16
  object PageControl: TPageControl
    Left = 10
    Top = 10
    Width = 543
    Height = 375
    ActivePage = TabSheetSource
    TabIndex = 0
    TabOrder = 0
    object TabSheetSource: TTabSheet
      Caption = 'Source Project'
      ImageIndex = 1
      object LayerLB: TListBox
        Left = 5
        Top = 30
        Width = 223
        Height = 267
        DragMode = dmAutomatic
        ItemHeight = 16
        MultiSelect = True
        TabOrder = 0
        OnClick = LayerLBClick
      end
      object LayerCB: TCheckBox
        Left = 5
        Top = 0
        Width = 208
        Height = 21
        Caption = 'Export only selected layers'
        TabOrder = 1
        OnClick = LayerCBClick
      end
      object SelectedCB: TCheckBox
        Left = 246
        Top = 0
        Width = 257
        Height = 21
        Caption = 'Export only selected objects'
        TabOrder = 2
        OnClick = SelectedCBClick
      end
      object Panel2: TPanel
        Left = 5
        Top = 305
        Width = 523
        Height = 31
        TabOrder = 3
        object SourceDbLabel: TLabel
          Left = 10
          Top = 10
          Width = 95
          Height = 16
          Caption = 'SourceDbLabel'
        end
      end
    end
    object TabSheetDest: TTabSheet
      Caption = 'Destination Project'
      ImageIndex = 1
      object Label1: TLabel
        Left = 5
        Top = 18
        Width = 93
        Height = 16
        Caption = 'WinGIS Project:'
      end
      object Panel7: TPanel
        Left = 108
        Top = 10
        Width = 386
        Height = 31
        TabOrder = 0
        object AmpLabel: TLabel
          Left = 10
          Top = 7
          Width = 62
          Height = 16
          Caption = 'AmpLabel'
        end
      end
      object AmpBtn: TButton
        Left = 498
        Top = 10
        Width = 31
        Height = 31
        Caption = '...'
        TabOrder = 1
        OnClick = AmpBtnClick
      end
      object Panel1: TPanel
        Left = 5
        Top = 305
        Width = 523
        Height = 31
        TabOrder = 2
        object DestDbLabel: TLabel
          Left = 10
          Top = 10
          Width = 80
          Height = 16
          Caption = 'DestDbLabel'
        end
      end
    end
  end
  object OKBtn: TButton
    Left = 295
    Top = 394
    Width = 115
    Height = 28
    Caption = 'OK'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 428
    Top = 394
    Width = 125
    Height = 28
    Cancel = True
    Caption = 'Abbrechen'
    ModalResult = 2
    TabOrder = 2
    OnClick = CancelBtnClick
  end
  object GeoBtn: TButton
    Left = 10
    Top = 394
    Width = 114
    Height = 28
    Caption = 'Projection'
    Default = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = GeoBtnClick
  end
  object DbBtn: TButton
    Left = 128
    Top = 394
    Width = 114
    Height = 28
    Caption = 'Database'
    TabOrder = 4
    OnClick = DbBtnClick
  end
  object SaveDialogAmp: TSaveDialog
    DefaultExt = '*.amp'
    FileName = '*.amp'
    Filter = 'WinGIS Project|*.amp'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 224
  end
  object SetupWindowTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = SetupWindowTimerTimer
    Left = 256
  end
end
