unit SelAmpDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StrFile, StdCtrls, Buttons, FileCtrl, ComCtrls, ExtCtrls, Db, ADODB,
  Grids, Outline, DirOutln, WinOSInfo;

type
  TSelAmpWnd = class(TForm)
    PageControl: TPageControl;
    TabSheetSource: TTabSheet;
    LayerLB: TListBox;
    TabSheetDest: TTabSheet;
    LayerCB: TCheckBox;
    OKBtn: TButton;
    CancelBtn: TButton;
    GeoBtn: TButton;
    Panel7: TPanel;
    Label1: TLabel;
    AmpBtn: TButton;
    AmpLabel: TLabel;
    SaveDialogAmp: TSaveDialog;
    SelectedCB: TCheckBox;
    SetupWindowTimer: TTimer;
    DbBtn: TButton;
    Panel2: TPanel;
    SourceDbLabel: TLabel;
    Panel1: TPanel;
    DestDbLabel: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure LayerCBClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GeoBtnClick(Sender: TObject);
    procedure AmpBtnClick(Sender: TObject);
    procedure SelectedCBClick(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure DbBtnClick(Sender: TObject);
    procedure LayerLBClick(Sender: TObject);
  private
    DestinationAmpFilename  :string;
    FirstActivate      :boolean;
    procedure CheckWindow;
  public
    { Public declarations }
    winheight:integer;
    winwidth :integer;
    wintop   :integer;
    winleft  :integer;
    OkBtnMode:boolean;
    procedure GetProjectName(var Ampfile:string);
    procedure CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
    procedure SetupForBatch;
    procedure SetProjectAndExportMode(ProjectName:string; ExpMode:integer);
    procedure AddExpLayer(ExpLayer:string);
  end;

var
  SelAmpWnd     : TSelAmpWnd;
  StrFileObj    : ClassStrfile;

implementation

uses Maindlg, inifiles;

{$R *.DFM}

procedure TSelAmpWnd.FormCreate(Sender: TObject);
var myScale:double;
    dummy  :double;
    dummystr:string;
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   if MainWnd.REGMODE = DEMO_LICENCE then
      Self.Caption:=StrFileObj.Assign(57)           // PROGIS AMP-Export 2000 Limited Mode
   else
      Self.Caption:=StrFileObj.Assign(1);           // PROGIS AMP-Export 2000

   TabsheetSource.Caption:=StrfileObj.Assign(2); { Source Project              }
   TabsheetDest.Caption:=StrfileObj.Assign(3);   { Destination Database        }
   LayerCB.Caption:=StrfileObj.Assign(4);        { Export only selected layers }
   SelectedCB.Caption:=StrfileObj.Assign(55);    { Export only selected objects }
   OkBtn.Caption:=StrfileObj.Assign(12);         { Ok }
   CancelBtn.Caption:=StrfileObj.Assign(13);     { Cancel }
   GeoBtn.Caption:=StrfileObj.Assign(20);        { Projection }
   Label1.Caption:=StrfileObj.Assign(16);        { WinGIS Project: }
   DbBtn.Caption:=StrfileObj.Assign(6);          // Database
   SourceDbLabel.Caption:='';
   DestDbLabel.Caption:='';

   LayerLB.enabled:=false;

   DestinationAmpFilename:='';
   AmpLabel.Caption:='';

   FirstActivate:=true;

   Self.Height:=1;
   Self.Width:=1;

   myScale:=1;//100000 / Trunc(9600000/(Self.PixelsPerInch));
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
   
      // now adapt winheigt and winwidth
   dummy:=376*myScale; dummystr:=floattostr(dummy);
   if (Pos('.',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos('.',dummystr)-1);
   if (Pos(',',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos(',',dummystr)-1);
   winheight:=strtoint(dummystr);
   dummy:=462*myScale; dummystr:=floattostr(dummy);
   if (Pos('.',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos('.',dummystr)-1);
   if (Pos(',',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos(',',dummystr)-1);
   winwidth:=strtoint(dummystr);
end;

procedure TSelAmpWnd.CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
begin
   wintop:=(WHEIGHT div 2) - (winheight div 2);
   winleft:=(WWIDTH div 2) - (winwidth div 2);
end;

procedure TSelAmpWnd.OKBtnClick(Sender: TObject);
var inifile:TInifile;
begin
   MainWnd.ExitNormal:=true;

   // read out last used database settings from the .ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir + 'ConGIS.ini');
   inifile.WriteString('AMPEXP','ExpDbMode',inttostr(MainWnd.AXImpExpDbcObj.ExpDbMode));
   inifile.WriteString('AMPEXP','ExpDatabase',MainWnd.AXImpExpDbcObj.ExpDatabase);
   inifile.WriteString('AMPEXP','ExpTable', MainWnd.AXImpExpDbcObj.ExpTable);
   inifile.WriteString('AMPEXP','ImpDbMode',inttostr(MainWnd.AXImpExpDbcObj.ImpDbMode));
   inifile.WriteString('AMPEXP','ImpDatabase',MainWnd.AXImpExpDbcObj.ImpDatabase);
   inifile.WriteString('AMPEXP','ImpTable',MainWnd.AXImpExpDbcObj.ImpTable);
   inifile.Destroy;

   // now the export window can be opened and the export executed
   Self.Close;
   MainWnd.Show;
end;

procedure TSelAmpWnd.CancelBtnClick(Sender: TObject);
begin
   MainWnd.ExitNormal:=false;
   MainWnd.CloseTimer.Enabled:=true;
end;

procedure TSelAmpWnd.GetProjectName(var Ampfile:string);
begin
   Ampfile:=DestinationAmpFilename;
end;

procedure TSelAmpWnd.LayerCBClick(Sender: TObject);
begin
   if LayerCB.Checked then
   begin
      LayerLB.enabled:=true;
      SelectedCB.checked:=false;
   end
   else
   begin
      LayerLB.enabled:=false;
   end;
   CheckWindow;
end;

procedure TSelAmpWnd.CheckWindow;
var allok:boolean;
    i    :integer;
begin
   allok:=true;
   // check if filename is set
   if DestinationAmpFilename = '' then allok:=false;
   if LayerCB.Checked and allok  then
   begin
      // check if a layer is selected
      allok:=FALSE;
      for i:=0 to LayerLb.Items.Count -1 do
      begin
         if LayerLb.Selected[i] = TRUE then
         begin
            allok:=TRUE;
            break;
         end;
      end;
   end;

   if allok then OKBtn.Enabled:=true
   else OkBtn.Enabled:=false;
end;

// procedure is used to setup the SelAmpWnd for batch mode
procedure TSelAmpWnd.SetupForBatch;
var Layername:string;
    layercnt:integer;
    ALayer:Variant;
begin
   if FirstActivate then
   begin
      // fill layerlist
      LayerLB.Items.Clear;
      for layercnt:=0 to MainWnd.SourceDocument.Layers.Count-1 do
      begin
         ALayer:=MainWnd.SourceDocument.Layers.Items[layercnt];
         Layername:=ALayer.Layername;
         LayerLb.Items.Add(Layername);
      end;
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
   end;
end;

procedure TSelAmpWnd.FormActivate(Sender: TObject);
var Layername:string;
    layercnt:integer;
    ALayer:Variant;
begin
   if FirstActivate then
   begin
      // fill layerlist
      LayerLB.Items.Clear;
      for layercnt:=0 to MainWnd.SourceDocument.Layers.Count-1 do
      begin
         ALayer:=MainWnd.SourceDocument.Layers.Items[layercnt];
         Layername:=ALayer.Layername;
         LayerLb.Items.Add(Layername);
      end;
      MainWnd.AxGisProjection.WorkingPath:=OSInfo.WingisDir;
      MainWnd.AxGisProjection.LngCode:=MainWnd.LngCode;
      MainWnd.AXImpExpDbcObj.WorkingDir:=OSInfo.WingisDir;
      MainWnd.AXImpExpDbcObj.LngCode:=MainWnd.LngCode;
      SourceDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ExpDbInfo;
      DestDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ImpDbInfo;
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TSelAmpWnd.GeoBtnClick(Sender: TObject);
begin
   MainWnd.AxGisProjection.SetupByDialog;
end;

procedure TSelAmpWnd.AmpBtnClick(Sender: TObject);
begin
   if SaveDialogAmp.Execute then
   begin
      DestinationAmpFilename:=SaveDialogAmp.Filename;
      AmpLabel.Caption:=DestinationAmpFilename;
      DestDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ImpDbInfo;
   end;
   CheckWindow;
end;

procedure TSelAmpWnd.SelectedCBClick(Sender: TObject);
begin
   if SelectedCB.Checked then
      LayerCB.Checked:=false;
   CheckWindow;
end;

procedure TSelAmpWnd.SetupWindowTimerTimer(Sender: TObject);
var inifile:TIniFile;
    dummy:string;
    intval,count:integer;
begin
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,winleft,wintop,winwidth, winheight,SWP_SHOWWINDOW);
   Self.Height:=winheight;
   Self.Width:=winwidth;

   // now setup the items of the window in dependence of the ConGIS.ini file
   if MainWnd.SourceDocument.NrOfSelectedObjects > 0 then
   begin
      // if there are objects selected suggest export of selected objects
      SelectedCb.Checked:=TRUE;
      SelectedCBClick(nil);
   end
   else
   begin
      SelectedCb.visible:=FALSE;
   end;

   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir +'ConGIS.ini');
   // now setup the database
   dummy:=MainWnd.SourceDocument.Name;
   dummy:=ChangeFileExt(dummy,'.wgi');
   if FileExists(dummy) then
   begin
      MainWnd.AXImpExpDbcObj.ExpDbMode:=5; // Idb_Database
      MainWnd.AXImpExpDbcObj.ExpDatabase:=dummy;
      SourceDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ExpDbInfo;
      // read out the target database from the .ini file
      dummy:=inifile.ReadString('AMPEXP','ImpDbMode','0'); val(dummy, intval, count);
      if intval = 0 then
      begin
         // if no database has been set yet suggest the Idb_Database due to source is also Idb
         MainWnd.AXImpExpDbcObj.ImpDbMode:=5; // Idb_Database
         DestDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ImpDbInfo;
      end
      else
      begin
         MainWnd.AXImpExpDbcObj.ImpDbMode:=intval;
         if intval <> 5 then
         begin
            dummy:=inifile.ReadString('AMPEXP','ImpDatabase','');
            MainWnd.AXImpExpDbcObj.ImpDatabase:=dummy;
            dummy:=inifile.ReadString('AMPEXP','ImpTable','');
            MainWnd.AXImpExpDbcObj.ImpTable:=dummy;
         end;
         DestDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ImpDbInfo;
      end;
      if (MainWnd.AXImpExpDbcObj.ImpDbMode = 0) or (MainWnd.AXImpExpDbcObj.ExpDbMode = 0) then
      begin
         MainWnd.AXImpExpDbcObj.ImpDbMode:=0;
         MainWnd.AXImpExpDbcObj.ExpDbMode:=0;
         DestDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ImpDbInfo;
         SourceDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ExpDbInfo;
      end;
   end
   else
   begin
      // read out last used database settings from the .ini file
      dummy:=inifile.ReadString('AMPEXP','ExpDbMode','0'); val (dummy, intval, count);
      if intval <> 5 then // Idb-Database
      begin
         MainWnd.AXImpExpDbcObj.ExpDbMode:=intval;
         dummy:=inifile.ReadString('AMPEXP','ExpDatabase','');
         MainWnd.AXImpExpDbcObj.ExpDatabase:=dummy;
         dummy:=inifile.ReadString('AMPEXP','ExpTable','');
         MainWnd.AXImpExpDbcObj.ExpTable:=dummy;
      end
      else
         MainWnd.AXImpExpDbcObj.ExpDbMode:=0;

      SourceDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ExpDbInfo;
      dummy:=inifile.ReadString('AMPEXP','ImpDbMode','0'); val(dummy, intval, count);
      MainWnd.AXImpExpDbcObj.ImpDbMode:=intval;
      if intval <> 5 then
      begin
         dummy:=inifile.ReadString('AMPEXP','ImpDatabase','');
         MainWnd.AXImpExpDbcObj.ImpDatabase:=dummy;
         dummy:=inifile.ReadString('AMPEXP','ImpTable','');
         MainWnd.AXImpExpDbcObj.ImpTable:=dummy;
      end;
      DestDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ImpDbInfo;
      if (MainWnd.AXImpExpDbcObj.ImpDbMode = 0) or (MainWnd.AXImpExpDbcObj.ExpDbMode = 0) then
      begin
         MainWnd.AXImpExpDbcObj.ImpDbMode:=0;
         MainWnd.AXImpExpDbcObj.ExpDbMode:=0;
         DestDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ImpDbInfo;
         SourceDbLabel.Caption:=MainWnd.AXImpExpDbcObj.ExpDbInfo;
      end;
   end;
   inifile.Destroy;
   Self.Repaint;
   SetupWindowTimer.Enabled:=false;
end;


procedure TSelAmpWnd.SetProjectAndExportMode(ProjectName:string; ExpMode:integer);
begin
   DestinationAmpFilename:=ProjectName;
   if ExpMode = 2 then // export only selected objects
      SelectedCB.Checked:=true;
   if ExpMode = 1 then // export only selected layers
      LayerCB.Checked:=true;
end;

procedure TSelAmpWnd.AddExpLayer(ExpLayer:string);
var i:integer;
begin
   // find the Layername in the list and select the item
   for i:=0 to LayerLB.Items.Count-1 do
   begin
      if (LayerLB.Items[i] = ExpLayer) then
      begin
         LayerLB.Selected[i]:=true;
      end;
   end;
end;

procedure TSelAmpWnd.DbBtnClick(Sender: TObject);
begin
   MainWnd.AXImpExpDbcObj.SetupByDialog(Self.Handle, 5, 5);
   OkBtnMode:=OkBtn.Enabled;
   PageControl.Enabled:=FALSE;
   GeoBtn.Enabled:=FALSE;
   DbBtn.Enabled:=FALSE;
   OkBtn.Enabled:=FALSE;
   CancelBtn.Enabled:=FALSE;
end;

procedure TSelAmpWnd.LayerLBClick(Sender: TObject);
begin
   CheckWindow;
end;

end.
