unit LayerTools;

interface

uses GisAnalyzeDef, CPoly, CPolyTools;

type
  TLayerTools = class
  public
     Document   :Variant;        // pointer to current document
     constructor Create;
     destructor Destroy;
     procedure  CreateIslandLayer(SourceLayer:string; TargetLayer:string);      // method to create islands on layer
     procedure  CombineCPolysOnLayer(SourceLayer:string; TargetLayer:string; SourceIsSelected:boolean);   // method to combine CPolys on layer
     procedure  SetSnapRadius(sr:double);
  private
     CPolyObj      :TCPoly;
     CPolyToolsObj :TCPolyTools;
     SnapRadius    :double;
     procedure ReadCPolysFromLayer(SourceLayer:string);
     procedure ReadCPolysFromSelected(CheckForOtherRef:boolean);
     function  CheckLayerExists(Layername:string):boolean;
  end;

implementation

// ----------------------------------------------------------------------
// METHOD    : Create
// ----------------------------------------------------------------------
// Method is constructor of TLayerTools class
//
// PARAMETERS: NONE
// ----------------------------------------------------------------------
constructor TLayerTools.Create;
begin

end;

// ----------------------------------------------------------------------
// METHOD    : Destroy
// ----------------------------------------------------------------------
// Method is destructor of TLayerTools class
//
// PARAMETERS: NONE
// ----------------------------------------------------------------------
destructor TLayerTools.Destroy;
begin
end;

procedure TLayerTools.SetSnapRadius(sr:double);
begin
   SnapRadius:=sr;
end;

// ----------------------------------------------------------------------
// METHOD    : CombineCPolysOnLayer
// ----------------------------------------------------------------------
// Method is used to combine all CPolys on laye SourceLayer and
// create result on layer TargetLayer
//
// PARAMETERS: SourceLayer -> Layer that contains cpolys
//             TargetLayer -> Layer on that the result will be created
//             SourceIsSelected -> if TRUE source is selected objects
// ----------------------------------------------------------------------
procedure TLayerTools.CombineCPolysOnLayer(SourceLayer:string; TargetLayer:string; SourceIsSelected:boolean);
var aCPoly1:p_CPoly_record;
    aCPoly2:p_CPoly_record;
    resCPoly:p_CPoly_record;
    pre_CPoly1:p_CPoly_record;
    pre_CPoly2:p_CPoly_record;
    dochangePre2:boolean;
    aLayer :Variant;
begin
   CPolyObj:=TCPoly.Create;
   CPolyToolsObj:=TCPolyTools.Create;
   CPolyToolsObj.SetSnapRadius(SnapRadius);
   if (SourceIsSelected) then
   begin
      ReadCPolysFromSelected(TRUE); // Ignore references on other layers
   end
   else
   begin
      // first the CPolys of layer Sourcelayer have to be read
      ReadCPolysFromLayer(SourceLayer);
   end;

   // now the combining of objects has to be done
   CPolyObj.Reset;
   aCPoly1:=CPolyObj.GetCurrent;
   if (aCPoly1 = nil) then // if no objects have been read
      exit;

   pre_CPoly1:=nil;
   while (aCPoly1^.next <> nil) do
   begin
      pre_CPoly2:=aCPoly1;
      aCPoly2:=aCPoly1^.next;
      while (aCPoly2 <> nil) do
      begin
         // now combine CPoly1 with CPoly2
         CPolyToolsObj.PrepareForCombineCPoly;
         if (CPolyToolsObj.CombinePolys(aCPoly1, aCPoly2)) then
         begin
            // combining was successful
            resCPoly:=CPolyToolsObj.GetCPolyCombineResult;
            // now remove CPoly1 with resCPoly the result is the
            // pointer to new inserted CPoly
            dochangePre2:=FALSE;
            if (pre_CPoly2 = aCPoly1) then
               dochangePre2:=TRUE;
            aCPoly1:=CPolyObj.ReplaceCPolyAfterCPoly(pre_CPoly1, resCPoly);
            if dochangePre2 then
               pre_CPoly2:=aCPoly1;
            // now CPoly2 has to be removed
            CPolyObj.RemoveCPolyAfterCPoly(pre_CPoly2);
            // if the combining was successful it has to
            // be restarted from the first poly after CPoly1
            pre_CPoly2:=aCPoly1;
            aCPoly2:=aCPoly1^.next;
         end
         else
         begin
            pre_CPoly2:=aCPoly2;
            aCPoly2:=aCPoly2^.next;
         end;
         CPolyToolsObj.RemoveAfterCombineCPoly;
      end;
      pre_CPoly1:=aCPoly1;
      if (aCPoly1^.next <> nil) then
         aCPoly1:=aCPoly1^.next;
   end;

   // now write the result of the combining to the TargetLayer
   if CheckLayerExists(TargetLayer) then
      aLayer:=Document.Layers.GetLayerByName(TargetLayer)
   else
      aLayer:=Document.Layers.InsertLayerByName(TargetLayer,True);

   CPolyObj.Reset;
   aCPoly1:=CPolyObj.GetCurrent;
   while (aCPoly1 <> nil) do
   begin
      CPolyObj.WriteToLayer(Document, aLayer, aCPoly1);
      aCPoly1:=aCPoly1^.next;
   end;

   // destroy objects
   CPolyObj.Destroy;
   CPolyToolsObj.Destroy;
end;

// ----------------------------------------------------------------------
// METHOD    : CreateIslandLayer
// ----------------------------------------------------------------------
// Method is used to calculate all islands on layer SourceLayer and
// create result on layer TargetLayer
//
// PARAMETERS: SourceLayer -> Layer that contains cpolys
//             TargetLayer -> Layer on that the islands will be generated
// ----------------------------------------------------------------------
procedure TLayerTools.CreateIslandLayer(SourceLayer:string; TargetLayer:string);
var aCPoly1:p_CPoly_record;
    aCPoly2:p_CPoly_record;
    aLayer :Variant;
begin
   CPolyObj:=TCPoly.Create;
   CPolyToolsObj:=TCPolyTools.Create;
   // first the CPolys of layer Sourcelayer have to be read
   ReadCPolysFromLayer(SourceLayer);

   // now the island calculation has to be done
   CPolyObj.Reset;
   aCPoly1:=CPolyObj.GetCurrent;
   while (aCPoly1 <> nil) do
   begin
      // write the debug-info
      CPolyObj.WriteDebugInfo('debug.txt',aCPoly1);
      aCPoly2:=aCPoly1^.next;
      while (aCPoly2 <> nil) do
      begin
         // check if aCPoly1 is inside aCPoly2
         if (CPolyToolsObj.CheckPolyInsidePoly(aCPoly1, aCPoly2)) then
         begin
            // check if aCPoly1 is not already a Islandpoly of aCPoly2
            if not CPolyToolsObj.CheckPolyIsIslandPoly(aCPoly1, aCPoly2) then
               if (not aCPoly1^.isIsland) and (aCPoly1^.Islands = nil) then // island area can not be added as island
                  CPolyObj.AddIslandToCPoly(aCPoly2, aCPoly1)
         end
         else if (CPolyToolsObj.CheckPolyInsidePoly(aCPoly2, aCPoly1)) then
         begin
            // check if aCPoly2 is not already a Islandpoly of aCPoly1
            if not CPolyToolsObj.CheckPolyIsIslandPoly(aCPoly2, aCPoly1) then
               // island area can not be added as island
               if (not aCPoly2^.isIsland) and (aCPoly2^.Islands = nil) then
                  CPolyObj.AddIslandToCPoly(aCPoly1, aCPoly2);
         end;
         aCPoly2:=aCPoly2^.next;
      end;
      aCPoly1:=aCPoly1^.next;
   end;

   // now write the new calculated islands to layer TargetLayer
   if CheckLayerExists(TargetLayer) then
      aLayer:=Document.Layers.GetLayerByName(TargetLayer)
   else
      aLayer:=Document.Layers.InsertLayerByName(TargetLayer,True);

   CPolyObj.Reset;
   aCPoly1:=CPolyObj.GetCurrent;
   while (aCPoly1 <> nil) do
   begin
      CPolyObj.WriteToLayer(Document, aLayer, aCPoly1);
      aCPoly1:=aCPoly1^.next;
   end;

   // destroy objects
   CPolyObj.Destroy;
   CPolyToolsObj.Destroy;
end;

function TLayerTools.CheckLayerExists(Layername:string):boolean;
var dispatch:IDispatch;
begin
   dispatch:=Document.Layers.GetLayerByName(Layername);
   if dispatch <> nil then result:=true else result:=false;
end;

// ----------------------------------------------------------------------
// METHOD    : ReadCPolysFromLayer
// ----------------------------------------------------------------------
// Method is used to read all CPoly objects from layer sourcelayer
//
// PARAMETERS: SourceLayer -> Layer that contains cpolys
// ----------------------------------------------------------------------
procedure TLayerTools.ReadCPolysFromLayer(SourceLayer:string);
var aLayer:Variant;
    aObject:Variant;
    layercnt:integer;
    cnt:integer;
    aLayername:string;
begin
   for layercnt:=0 to Document.Layers.Count-1 do
   begin
      aLayer:=Document.Layers.Items[layercnt];
      aLayername:=aLayer.Layername;
      if aLayername = SourceLayer then
      begin
         // now the objects from the layer will be read
         for cnt:=0 to aLayer.count-1 do
         begin
            aObject:=ALayer.Items[cnt];
            if (aObject.ObjectType = ot_Cpoly) then // read polygon
               CPolyObj.ReadCPoly(aObject);
         end;
      end;
   end;
end;

// ----------------------------------------------------------------------
// METHOD    : ReadCPolysFromSelected
// ----------------------------------------------------------------------
// Method is used to read all CPoly from selected objects
//
// PARAMETERS: CheckForOtherRef -> Check if object has references on
//                                 other layers and ignore this
// ----------------------------------------------------------------------
procedure TLayerTools.ReadCPolysFromSelected(CheckForOtherRef:boolean);
var ObjectDisp:IDispatch;
    aObject:Variant;
    cnt:integer;
    ProcessedIds:array of integer;
    NumDupl:integer;
    aId:integer;
    isDupl:boolean;
    i:integer;
begin
   SetLength(ProcessedIds, 0); NumDupl:=0;
   for cnt:=0 to Document.NrOfSelectedObjects-1 do
   begin
      if Document.NrOfSelectedObjects = 0 then break;
      ObjectDisp:=Document.SelItems[cnt];
      if ObjectDisp = nil then
         continue;
      AObject:=ObjectDisp;
      if (aObject.ObjectType = ot_Cpoly) then // read polygon
      begin
         if CheckForOtherRef then
         begin
            // it will be checked if the object has a reference on other layer and
            // may be ignored (avoid duplicates in List)
            aId:=aObject.Index;
            // now check if this Id has already been processed
            isDupl:=FALSE;
            if NumDupl > 0 then
            begin
               for i:=0 to NumDupl-1 do
               begin
                  if (ProcessedIds[i] = aId) then
                  begin
                     isDupl:=TRUE;
                     break;
                  end;
               end;
            end;
            if not isDupl then
            begin
               // add Id to Processed Id List
               NumDupl:=NumDupl+1;
               SetLength(ProcessedIds, NumDupl);
               ProcessedIds[NumDupl-1]:=aId;
               CPolyObj.ReadCPoly(aObject);
            end;
            // if it has already been processed do nothing
         end
         else
            CPolyObj.ReadCPoly(aObject);
      end;
   end;
   SetLength(ProcessedIds, 0); // remove the processed id�s list
end;

end.
