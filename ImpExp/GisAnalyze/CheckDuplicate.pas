unit CheckDuplicate;

interface
uses Dialogs,SysUtils;

const
       // Object types
       ot_Pixel     = 1;
       ot_Poly      = 2;
       ot_CPoly     = 4;
       ot_Text      = 7;
       ot_Symbol    = 8;
       ot_Spline    = 9;
       ot_Image     = 10;
       ot_MesLine   = 11;
       ot_Circle    = 12;
       ot_Arc       = 13;

type ClassCheckDuplicate = class
   public
      // public variables
      App:Variant;        // pointer to DX object
      Document:Variant;   // pointer to current document
      DoCheckId:boolean;  // flag that defines if also the ID should be checked

      // properties for check-duplicate routine
      OriCheckObject:Variant;
      OldCheckObject:Variant;

      // public functions
      constructor Create;
      destructor Destroy;
      function CheckIdHasDuplicateOnLayer(id:integer; layer:string):boolean;
      function CheckIsDuplicate(var oriObject:Variant; var checkObject:Variant):boolean;
   private
      function CheckObjectHasDuplicateOnLayer(var aObject:Variant; Layer:string):boolean;
      function CheckIsDuplicate_Pixel(var Object1:Variant; var Object2:Variant):boolean;
      function CheckIsDuplicate_Symbol(var Object1:Variant; var Object2:Variant):boolean;
      function CheckIsDuplicate_CPoly(var Object1:Variant; var Object2:Variant):boolean;
      function CheckIsDuplicate_Poly(var Object1:Variant; var Object2:Variant):boolean;
      function CheckIsDuplicate_Text(var Object1:Variant; var Object2:Variant):boolean;
      function CheckIsDuplicate_Circle(var Object1:Variant; var Object2:Variant):boolean;
      function AreaCPolyCornersEqual(var Object1:Variant; var Object2:Variant):boolean;
      function CheckIsDuplicate_ObjectStyle(var Object1:Variant; var Object2:Variant):boolean;
end;

implementation

constructor ClassCheckDuplicate.Create;
begin
end;

destructor ClassCheckDuplicate.Destroy;
begin
end;

function ClassCheckDuplicate.CheckIdHasDuplicateOnLayer(id:integer; layer:string):boolean;
var retVal:boolean;
    aObjectDisp:IDispatch;
    aLayerDisp :IDispatch;
    aObject:Variant;
begin
   retVal:=FALSE;
   // first the object will be got by using its id
   aLayerDisp :=Document.Layers.GetLayerByName(Layer);
   aObjectDisp:=Document.GetObjectByIndex(id);
   if (aObjectDisp <> nil) and (aLayerDisp <> nil) then
   begin
      // id and layer exists
      aObject:=aObjectDisp;
      retVal:=CheckObjectHasDuplicateOnLayer(aObject, layer);
   end;
   result:=retVal;
end;

function ClassCheckDuplicate.CheckObjectHasDuplicateOnLayer(var aObject:Variant; Layer:string):boolean;
var ARect:Variant;
    retVal:boolean;
    cnt:integer;
    aSelObject:Variant;
    SelObjectid:integer;
begin
   retVal:=FALSE;
   // now the cliprect of the Object has to be evaluated
   ARect:=aObject.ClipRect;

   // the layer that has to be checked has to be set as active layer
   Document.Layers.SetActiveLayer(Layer);

   // now the objects on the layer that has to be searched for will
   // be selected
   Document.SelectByRect(ARect);

   // create list of the selected objects
   Document.CreateSelectedList;

   // now search the selected objects if they are a duplicate of the
   // object that has to be searched for
   for cnt:=0 to Document.NrOfSelectedObjects-1 do
   begin
      aSelObject:=Document.SelItems[cnt];
      SelObjectid:=aSelObject.Index;
      retVal:=CheckIsDuplicate(aObject, aSelObject);
      if (retVal) then
         break;
   end;
   Document.DestroySelectedList;

   // now all objects that have been selected can
   // be deselected
   Document.DeselectAll;
   result:=retVal;
end;

// function is used to check if two objects are equal
function ClassCheckDuplicate.CheckIsDuplicate(var oriObject:Variant; var checkObject:Variant):boolean;
var retVal:boolean;
    ignoreObject:boolean;
    ObjectType:integer;
begin
   retVal:=FALSE; ignoreObject:=FALSE;
   if (oriObject.ObjectType = checkObject.ObjectType) then
   begin
      ObjectType:=oriObject.ObjectType;
      if oriObject.ObjectType = ot_Pixel then
         retVal:=CheckIsDuplicate_Pixel(oriObject, checkObject)
      else if oriObject.ObjectType = ot_Symbol then
         retVal:=CheckIsDuplicate_Symbol(oriObject, checkObject)
      else if oriObject.ObjectType = ot_Cpoly then
         retVal:=CheckIsDuplicate_CPoly(oriObject, checkObject)
      else if oriObject.ObjectType =  ot_poly then
         retVal:=CheckIsDuplicate_Poly(oriObject, checkObject)
      else if oriObject.ObjectType = ot_Text then
         retVal:=CheckIsDuplicate_Text(oriObject, checkObject)
      else if oriObject.ObjectType = ot_Circle then
         retVal:=CheckIsDuplicate_Circle(oriObject, checkObject)
      else
      begin
         // other object types will not be supported
         ignoreObject:=TRUE;
         retVal:=TRUE;
      end;

      if (retVal = TRUE) and (not ignoreObject) then
      begin
         // check also if the object-style is equal
         retVal:=CheckIsDuplicate_ObjectStyle(oriObject, checkObject);
      end;
   end;
   result:=retVal;
end;

function ClassCheckDuplicate.CheckIsDuplicate_ObjectStyle(var Object1:Variant; var Object2:Variant):boolean;
var retVal:boolean;
    styledisp1:IDispatch;
    styledisp2:IDispatch;
    style1:Variant;
    style2:Variant;
    color1, color2:integer;
begin
   retVal:=FALSE;
   // first check if both objects have an objectstyle
   styledisp1:=Object1.ObjectStyle;
   styledisp2:=Object2.ObjectStyle;
   if (styledisp1 = nil) and (styledisp2 = nil) then
   begin
      // both objects have no objectstyle
      retVal:=TRUE;
   end;
   if (styledisp1 <> nil) and (styledisp2 <> nil) then
   begin
      // both objects have objectstyle
      // now it has to be checked if the objectstyle is equal
      style1:=styledisp1;
      style2:=styledisp2;
      retVal:=TRUE;
      if (style1.LineStyle.Style <> style2.LineStyle.Style) then retVal:=FALSE;

      color1:=style1.LineStyle.Color; color2:=style2.LineStyle.Color;
      if (color1 <> color2) then retVal:=FALSE;

      color1:=style1.LineStyle.FillColor; color2:=style2.LineStyle.FillColor;
      if (color1 <> color2) then retVal:=FALSE;

      if (style1.FillStyle.Pattern <> style2.FillStyle.Pattern) then retVal:=FALSE;

      color1:=style1.FillStyle.ForeColor; color2:=style2.FillStyle.ForeColor;
      if (color1 <> color2) then retVal:=FALSE;

      color1:=style1.FillStyle.BackColor; color2:=style2.FillStyle.BackColor;
      if (color1 <> color2) then retVal:=FALSE;
   end;
   result:=retVal;
end;

function ClassCheckDuplicate.CheckIsDuplicate_Pixel(var Object1:Variant; var Object2:Variant):boolean;
var retVal:boolean;
begin
   // function is used to check if Object2 is a duplicate of Object1
   retVal:=FALSE;
   if (Object1.Index <> Object2.Index) or (DoCheckId = FALSE) then
   begin
      if ((Object1.Position.X = Object2.Position.X) and
         (Object1.Position.Y = Object2.Position.Y)) then
         retVal:=TRUE;
   end;
   result:=retVal;
end;

function ClassCheckDuplicate.CheckIsDuplicate_Symbol(var Object1:Variant; var Object2:Variant):boolean;
var retVal:boolean;
begin
   // function is used to check if Object2 is a duplicate of Object1
   retVal:=FALSE;
   if (Object1.Index <> Object2.Index) or (DoCheckId = FALSE) then
   begin
      if ((Object1.Position.X = Object2.Position.X) and
         (Object1.Position.Y = Object2.Position.Y) and
         (Object1.Size = Object2.Size) and
         (Object1.SizeType = Object2.SizeType) and
         (Object1.Angle = Object2.Angle) and
         (Object1.SymIndex = Object2.SymIndex)) then
         retVal:=TRUE;
   end;
   result:=retVal;
end;

function ClassCheckDuplicate.CheckIsDuplicate_CPoly(var Object1:Variant; var Object2:Variant):boolean;
var retVal:boolean;
begin
   // function is used to check if Object2 is a duplicate of Object1
   retVal:=FALSE;
   if (Object1.Index <> Object2.Index) or (DoCheckId = FALSE) then
   begin
      // first check if they have the same area
      if (Object1.Area = Object2.Area) then
      begin
         // now check if they have the same rectangles
         if ((Object1.ClipRect.X1 = Object2.ClipRect.X1) and
            (Object1.ClipRect.Y1 = Object2.ClipRect.Y1) and
            (Object1.ClipRect.X2 = Object2.ClipRect.X2) and
            (Object1.ClipRect.Y2 = Object2.ClipRect.Y2)) then
         begin
            // both objects have same cliprect
            // now the corners have to be checked
            if Object1.Count = Object2.Count then
            begin
               retVal:=TRUE;

               // if both object have equal number of corners
               // the corners itself have to be checked
               // retVal:=AreaCPolyCornersEqual(Object1, Object2);
            end;
         end;
      end;
   end;
   result:=retVal;
end;

function ClassCheckDuplicate.AreaCPolyCornersEqual(var Object1:Variant; var Object2:Variant):boolean;
var retVal:boolean;
    Cnt1,Cnt2:integer;
    APoint1,APoint2:Variant;
    found:boolean;
begin
   // function is used to check if two CPolys have equal corners
   retVal:=TRUE;
   for Cnt1:=0 to Object1.Count - 1 do
   begin
      APoint1:=Object1.Points[Cnt1];
      // check if this point exists in Object2
      found:=FALSE;
      for Cnt2:=0 to Object1.Count - 1 do
      begin
         APoint2:=Object2.Points[Cnt2];
         if ((APoint1.X = APoint2.X) and
            (APoint1.Y = APoint2.Y)) then
         begin
            found:=TRUE;
            break;
         end;
      end;
      if (found = FALSE) then
      begin
         retVal:=FALSE;
         break;
      end;
   end;
   result:=retVal;
end;

function ClassCheckDuplicate.CheckIsDuplicate_Poly(var Object1:Variant; var Object2:Variant):boolean;
var retVal:boolean;
    i:integer;
    APoint1, APoint2:Variant;
begin
   // function is used to check if Object2 is a duplicate of Object1
   retVal:=FALSE;
   if (Object1.Index <> Object2.Index) or (DoCheckId = FALSE) then
   begin
      // first check if they have the same length
      if (Object1.Length = Object2.Length) then
      begin
         // now it will be checked if the corners are equal
         if (Object1.Count = Object2.Count) then
         begin
            retVal:=TRUE;
            for i:=0 to Object1.Count-1 do
            begin
               APoint1:=Object1.Points[i]; APoint2:=Object2.Points[i];
               if ((APoint1.X <> APoint2.X) or (APoint1.Y <> APoint2.Y)) then
               begin
                  retVal:=FALSE;
                  break;
               end;
            end;
         end;
      end;
   end;
   result:=retVal;
end;

function ClassCheckDuplicate.CheckIsDuplicate_Text(var Object1:Variant; var Object2:Variant):boolean;
var retVal:boolean;
begin
   // function is used to check if Object2 is a duplicate of Object1
   retVal:=FALSE;
   if (Object1.Index <> Object2.Index) or (DoCheckId = FALSE) then
   begin
      // first check if they have the same position
      if (Object1.Position.X = Object2.Position.X) and
         (Object1.Position.Y = Object2.Position.Y) then
      begin
         if (Object1.Text = Object2.Text) then
            if (Object1.Angle = Object2.Angle) then
               if (Object1.FontHeight = Object2.FontHeight) then
                  if (Object1.FontStyle = Object2.FontStyle) then
                     if (Object1.FontName = Object2.FontName) then
                        retVal:=TRUE;
      end;
   end;
   result:=retVal;
end;

function ClassCheckDuplicate.CheckIsDuplicate_Circle(var Object1:Variant; var Object2:Variant):boolean;
var retVal:boolean;
begin
   // function is used to check if Object2 is a duplicate of Object1
   retVal:=FALSE;
   if (Object1.Index <> Object2.Index) or (DoCheckId = FALSE) then
   begin
      if (Object1.Position.X = Object2.Position.X) and
         (Object1.Position.Y = Object2.Position.Y) then
      begin
         if (Object1.Angle = Object2.Angle) then
            if (Object1.PrimaryAxis = Object2.PrimaryAxis) then
               if (Object1.SecondaryAxis = Object2.SecondaryAxis) then
                  if (Object1.Area = Object2.Area) then
                     if (Object1.Radius = Object2.Radius) then
                        retVal:=TRUE;
      end;
   end;
   result:=retVal;
end;
end.
