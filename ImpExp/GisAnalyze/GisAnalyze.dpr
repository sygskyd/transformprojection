library GisAnalyze;

{ Wichtiger Hinweis zur DLL-Speicherverwaltung: Sharemem mu� die erste
 Unit in der uses-Klausel Ihrer Bibliothek und Ihres Projekts sein,
 wenn Ihre DLL Prozeduren und Funktionen exportiert, die
 Strings als Parameter oder Funktionsergebnisse �bergeben. Dies
 betrifft alle Strings, die an Ihre DLL oder von Ihrer DLL �bergeben werden--selbst diejenigen,
 die sich in Records oder Klassen befinden. Sharemem ist die Schnittstelle zu
 BORLNDMM.DLL, die den gemeinsamen Zugriff auf den Speicher verwaltet; DELPHIMM.DLL
 mu� mit Ihrer DLL weitergegeben werden. Wenn Sie BORLNDMM.DLL nicht verwenden m�chten, m�ssen
 Strings als PChar oder ShortString �bergeben werden.}

uses
  Forms,
  SysUtils,
  Classes,
  Dialogs,
  comobj,
  CheckDuplicate in 'CheckDuplicate.pas',
  CPolyTools in 'CPolyTools.pas',
  CPoly in 'CPoly.pas',
  GisAnalyzeDef in 'GisAnalyzeDef.pas',
  LayerTools in 'LayerTools.pas';

{$R *.RES}
{*******************************************************************************
* FUNCTION  * CHECKIDHASDUPLICATEONLAYER                                       *
********************************************************************************
* Function is used to check if an object that is specified by its id has a     *
* duplicate on a specified layer                                               *
* DKM-Import tool.                                                             *
*                                                                              *
* PARAMETERS: ID       -> ID of the object that should be checked              *
*             LAYER    -> LAYER on that the duplicate should be searched for   *
*             PROJECT  -> Pointer to document that has to be checked           *
*             AXHANDLE -> Pointer to DX                                        *
*                                                                              *
********************************************************************************}
function CHECKIDHASDUPLICATEONLAYER(ID:INTEGER; LAYER:PCHAR; var PROJECT:VARIANT; var AXHANDLE:VARIANT):boolean;stdcall;
var AnalyzeObj:ClassCheckDuplicate;
    retVal:boolean;
begin
   AnalyzeObj:=ClassCheckDuplicate.Create;
   AnalyzeObj.App:=AXHANDLE;
   AnalyzeObj.Document:=PROJECT;
   AnalyzeObj.DoCheckId:=TRUE;
   retVal:=AnalyzeObj.CheckIdHasDuplicateOnLayer(ID, strpas(LAYER));
   AnalyzeObj.Destroy;
   result:=retVal;
end;

function CHECKISDUPLICATE(var ORI:VARIANT; var OLD:VARIANT):boolean; stdcall;
var AnalyzeObj:ClassCheckDuplicate;
    retVal:boolean;
begin
   AnalyzeObj:=ClassCheckDuplicate.Create;
   AnalyzeObj.DoCheckId:=FALSE;
   retVal:=AnalyzeObj.CheckIsDuplicate(ORI, OLD);
   AnalyzeObj.Destroy;
   result:=retVal;
end;

// function is used to check if one poly contains the other
// return code: TRUE -> Poly1 is inside Poly2, else return FALSE
function CHECKPOLYINSIDEPOLY(var POLY1:VARIANT; var POLY2:VARIANT):boolean; stdcall;
var tPoly1, tPoly2:p_CPoly_record;
    CPolyObj:TCPoly;
    CPolyToolsObj:TCPolyTools;
    retVal:boolean;
begin
   CPolyObj:=TCPoly.Create;
   // insert first poly to list
   tPoly1:=CPolyObj.ReadCPoly(POLY1);
   // insert second poly to list
   tPoly2:=CPolyObj.ReadCPoly(POLY2);
   CPolyToolsObj:=TCPolyTools.Create;
   retVal:=CPolyToolsObj.CheckPolyInsidePoly(tPoly1, tPoly2);

   // destroy objects
   CPolyObj.Destroy;
   CPolyToolsObj.Destroy;
   result:=retVal;
end;

// function is used to check if a point is inside a poly
function CHECKPOINTINSIDEPOLY(X:DOUBLE; Y:DOUBLE; var POLY:VARIANT):boolean; stdcall;
var retVal:boolean;
    aPoint:pcpoint;
    CPolyObj:TCPoly;
    CPolyToolsObj:TCPolyTools;
    tPoly:p_CPoly_record;
begin
   aPoint.X:=X; aPoint.Y:=Y;
   CPolyObj:=TCPoly.Create;
   tPoly:=CPolyObj.ReadCPoly(POLY);
   CPolyToolsObj:=TCPolyTools.Create;
   retVal:=CPolyToolsObj.CheckPointInsidePoly(aPoint, tPoly);

   // destroy objects
   CPolyObj.Destroy;
   CPolyToolsObj.Destroy;
   result:=retVal;
end;

// function is used to check if there are polys inside polys on a layer SRCLAYER
// and creates islands on layer DSTLAYER
procedure CREATEISLANDLAYER(SRCLAYER:PCHAR; DSTLAYER:PCHAR; var PROJECT:VARIANT); stdcall;
var aLayerToolsObj:TLayerTools;
begin
   aLayerToolsObj:=TLayerTools.Create;
   aLayerToolsObj.Document:=PROJECT;
   aLayerToolsObj.CreateIslandLayer(strpas(SRCLAYER), strpas(DSTLAYER));
   aLayerToolsObj.Destroy;
end;

// function is used to combine all polys on a layer SRCLayer and create
// the result on layer DSTLAYER
procedure COMBINECPOLYSONLAYER(SRCLAYER:PCHAR; DSTLAYER:PCHAR; SNAPRADIUS:DOUBLE; var PROJECT:VARIANT); stdcall;
var aLayerToolsObj:TLayerTools;
begin
   aLayerToolsObj:=TLayerTools.Create;
   aLayerToolsObj.Document:=PROJECT;
   aLayerToolsObj.SetSnapRadius(SNAPRADIUS);
   aLayerToolsObj.CombineCPolysOnLayer(strpas(SRCLAYER), strpas(DSTLAYER), FALSE); // FALSE -> source is not selected objects
   aLayerToolsObj.Destroy;
end;

// function is used to combine all polys that are selected
// the result on layer DSTLAYER
procedure COMBINESELECTEDCPOLYS(DSTLAYER:PCHAR; SNAPRADIUS:DOUBLE; var PROJECT:VARIANT); stdcall;
var aLayerToolsObj:TLayerTools;
begin
   aLayerToolsObj:=TLayerTools.Create;
   aLayerToolsObj.Document:=PROJECT;
   aLayerToolsObj.SetSnapRadius(SNAPRADIUS);
   aLayerToolsObj.CombineCPolysOnLayer('', strpas(DSTLAYER), TRUE); // TRUE -> source is selected objects
   aLayerToolsObj.Destroy;
end;

exports CHECKIDHASDUPLICATEONLAYER,
        CHECKISDUPLICATE,
        CHECKPOLYINSIDEPOLY,
        CHECKPOINTINSIDEPOLY,
        CREATEISLANDLAYER,
        COMBINECPOLYSONLAYER,
        COMBINESELECTEDCPOLYS;
begin
end.
