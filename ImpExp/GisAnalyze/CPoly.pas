unit CPoly;

interface

uses GisAnalyzeDef, SysUtils;

type
  TCPoly = class // class to process poly objects
  public
     constructor Create;                                                        // constructor of class
     destructor Destroy;                                                        // destructor of class
     procedure  NewEntry;                                                       // add new CPoly element
     procedure  InsertPoint(X:double; Y:double;hidden:boolean);                 // add point to new CPoly element
     function   GetNumEntries:longint;                                          // get number of CPoly elements
     function   GetCurrent:p_CPoly_record;                                      // get current CPoly element
     procedure  Reset;                                                          // set current CPoly pointer to first CPoly element in list
     procedure  SetCurrentNext;                                                 // set current CPoly pointer to next CPoly element in list
     function   ReadCPoly(var Poly:Variant):p_CPoly_record;                     // procedure to read WinGIS CPoly and add it to list
     procedure  AddIslandToCPoly(aPoly:p_CPoly_record; aIsland:p_CPoly_record); // method to add island to CPoly (for Island calculation)
     procedure  WriteToLayer(var aDocument:Variant; var aLayer:Variant; aPoly:p_CPoly_record); // write aPoly to layer
     procedure  WriteDebugInfo(aFilename:string; aCPoly:p_CPoly_record);
     function   GetCPolyCornerByIdx(var parentpoly:p_CPoly_record; idx:integer):p_Point_record;
     procedure  AddIslandCornersToCurrCPoly(aPoly:p_CPoly_record; doCreateIslandConnection:boolean);
     function   ReplaceCPolyAfterCPoly(var pre_CPoly:p_CPoly_record; var newCPoly:p_CPoly_record):p_CPoly_record;
     procedure  RemoveCPolyAfterCPoly(var pre_CPoly:p_CPoly_record);
  private
     FirstEntry         :p_CPoly_record;     // pointer to first CPoly element in list
     LastEntry          :p_CPoly_record;     // pointer to last CPoly element in list
     Current            :p_CPoly_record;     // pointer to current CPoly element in list
     NumEntries         :integer;            // number of CPoly elements in list
  end;

implementation

constructor TCPoly.Create;
begin
   FirstEntry:=new(p_CPoly_record);
   LastEntry :=new(p_CPoly_record);
   Current   :=new(p_CPoly_record);
   FirstEntry:=nil;
   LastEntry :=nil;
   Current   :=nil;
   NumEntries:=0;
end;

destructor TCPoly.Destroy;
var iterate1, iterate2:p_CPoly_record;
    Point1,Point2     :p_Point_record;
    link1, link2      :p_LinkedCPoly_record;
begin
   iterate1:=FirstEntry;
   if FirstEntry <> nil then
   repeat
      iterate2:=iterate1;
      iterate1:=iterate1^.next;

      // delete points
      Point1:=iterate2^.FirstPoint;
      if Point1 <> nil then
      repeat
         Point2:=Point1;
         Point1:=Point1^.next;
         dispose(Point2);
      until Point1 = nil;

      // delete island links
      link1:=iterate2^.Islands;
      if link1 <> nil then
      repeat
         link2:=link1;
         link1:=link1^.next;
         dispose(link2);
      until Point1 = nil;

      dispose(iterate2);
   until iterate1 = nil;
end;

// procedure is used to add new entry
procedure TCPoly.NewEntry;
var new_entry:p_CPoly_record;
begin
   new(new_entry);
   new_entry^.FirstPoint:=nil;
   new_entry^.LastPoint :=nil;
   new_entry^.Islands   :=nil;
   new_entry^.isIsland  :=false;
   new_entry^.numcorners:=0;
   new_entry^.LastParentCornerIdx:=-1;

   if FirstEntry = NIL then // first element - insert at top
   begin
      FirstEntry:= new_entry;
      new_entry^.next:=NIL;
      LastEntry:=new_entry;
   end
   else
   begin                    // insert at end of list
      LastEntry^.next:=new_entry;
      new_entry^.next:=NIL;
      LastEntry:=new_entry;
   end;
   NumEntries:=NumEntries + 1;
   Current:=new_entry;
end;

// procedure is used to add position to current CPoly element in list
procedure TCPoly.InsertPoint(X:double; Y:double;hidden:boolean);
var new_entry:p_Point_record;
begin
   new(new_entry);
   new_entry^.Point.X:=X;
   new_entry^.Point.Y:=Y;
   new_entry^.hidden:=hidden;
   new_entry^.addedforcombine:=FALSE;
   if hidden then Current^.isIsland:=true;
   if Current^.FirstPoint = nil then // first point of CPoly object - insert at begin of list
   begin
      Current^.FirstPoint:=new_entry;
      new_entry^.next:=NIL;
      Current^.LastPoint:=new_entry;
      Current^.Rect_LU.X:=X; // set rectangle to current coordinates
      Current^.Rect_LU.Y:=Y;
      Current^.Rect_RB.X:=X;
      Current^.Rect_RB.Y:=Y;
   end
   else
   begin
      Current^.LastPoint^.next:=new_entry;
      new_entry^.next:=NIL;
      Current^.LastPoint:=new_entry;
      // check if it is the last corner of the parent cpoly
      if (X = Current^.FirstPoint^.Point.X) and
         (Y = Current^.FirstPoint^.Point.Y) and
         (Current^.LastParentCornerIdx = -1) then
         Current^.LastParentCornerIdx:=Current^.numcorners;
   end;
   // set the rectangle data
   if (X < Current^.Rect_LU.X) then
      Current^.Rect_LU.X:=X;
   if (Y > Current^.Rect_LU.Y) then
      Current^.Rect_LU.Y:=Y;
   if (X > Current^.Rect_RB.X) then
      Current^.Rect_RB.X:=X;
   if (Y < Current^.Rect_RB.Y) then
      Current^.Rect_RB.Y:=Y;
   Current^.numcorners:=Current^.numcorners+1;
end;

function TCPoly.GetNumEntries:longint;
begin
   result:=NumEntries;
end;

function TCPoly.GetCurrent:p_CPoly_record;
begin
   result:=Current;
end;

procedure TCPoly.Reset;
begin
   Current:=FirstEntry;
end;

procedure TCPoly.SetCurrentNext;
begin
   Current:=Current^.next;
end;

// read out WinGIS OLE CPoly and insert it to list
function TCPoly.ReadCPoly(var Poly:Variant):p_CPoly_record;
var APoint:Variant;
    PointCount:integer;
    SkipIndex :integer;
    FirstIsland:boolean;
    IslandCount:integer;
    IsIsland:boolean;
    IslandInfoIdx:integer;
    NextIslandIdx:integer;
    X,Y:double;
    Cnt:integer;
    First_X,First_Y:double;
    i:integer;
begin
   PointCount:=0; SkipIndex:=0; FirstIsland:=TRUE; IsIsland:=FALSE; NextIslandIdx:=0;
   IslandCount:=Poly.IslandCount;
   if IslandCount > 1 then
   begin
      IsIsland:=true;
      IslandInfoIdx:=0;
      NextIslandIdx:=Poly.IslandInfo[IslandInfoIdx];
   end;

   // create new CPoly object
   NewEntry;

   // insert first point to new CPoly
   APoint:=Poly.Points[0];
   X:=APoint.X/100; Y:=APoint.Y/100;
   First_X:=X; First_Y:=Y;
   InsertPoint(X,Y,FALSE); // first point is not hidden
   PointCount:=PointCount+1;

   for Cnt:=1 to Poly.Count - 1 do
   begin
      if SkipIndex = 0 then
         PointCount:=PointCount + 1;
      if (IsIsland) and (PointCount = NextIslandIdx) then // next island area will be created
      begin
         IslandInfoIdx:=IslandInfoIdx+1;
         if FirstIsland then
            SkipIndex:=1
         else
            SkipIndex:=2;

         FirstIsland:=FALSE;
         if IslandInfoIdx >= IslandCount then
         begin
            // all islands have been inserted
            NextIslandIdx:=0;
            IslandInfoIdx:=IslandCount;
         end
         else
         begin
            // to close the polygon a line back to the first point has to be done
            InsertPoint(First_X, First_Y, FALSE); // last point is not hidden
            NextIslandIdx:=Poly.IslandInfo[IslandInfoIdx];

            for i:=0 to SkipIndex-1 do
            begin
               APoint:=Poly.Points[Cnt+i];
               X:=APoint.X; Y:=APoint.Y;
               InsertPoint(X/100,Y/100,TRUE); // it�s a hidden point
            end;

            // insert point to polygon
            APoint:=Poly.Points[Cnt+SkipIndex];
            X:=APoint.X/100; Y:=APoint.Y/100;
            First_X:=X; First_Y:=Y;

            InsertPoint(X, Y, FALSE); // point is not hidden
            PointCount:=1;
         end;
      end
      else
      begin
         if (SkipIndex=0) then
         begin
            if (IsIsland) and (Cnt = Poly.Count -2) then //avoid exporting last connectionline
               continue;

            // insert point to polygon
            APoint:=Poly.Points[Cnt];
            X:=APoint.X/100; Y:=APoint.Y/100;
            InsertPoint(X, Y, FALSE);  // it�s not a hidden point
         end
         else
            SkipIndex:=SkipIndex-1;
      end;
   end;
   // to close polygon a line to the first point has to be done
   if (IsIsland) then
      InsertPoint(First_X, First_Y, FALSE); // it�s not a hidden point
   result:=Current;
end;

procedure TCPoly.AddIslandToCPoly(aPoly:p_CPoly_record; aIsland:p_CPoly_record);
var new_Link:p_LinkedCPoly_record;
    iterate :p_LinkedCPoly_record;
    exist   :boolean;
begin
   new(new_Link);
   new_Link^.LinkedCPoly:=aIsland;

   iterate:=aPoly^.Islands;
   if iterate = nil then
   begin
      // new linked island will get on top of list
      aPoly^.Islands:=New_Link;
      aPoly^.Islands^.next:=NIL;
   end
   else
   begin
      // check if link already exists
      exist:=false;
      while (iterate <> nil) and (exist = false) do
      begin
         if iterate^.LinkedCPoly = aIsland then exist:=true;
         iterate:=iterate^.next;
      end;
      if not exist then
      begin
         // new link will get to bottom of list
         iterate:=aPoly^.Islands;
         while (iterate^.next <> nil) do iterate:=iterate^.next;
         iterate^.next:=new_Link;
         new_Link^.next:=nil;
      end;
   end;
end;

procedure TCPoly.WriteToLayer(var aDocument:Variant; var aLayer:Variant; aPoly:p_CPoly_record); // write aPoly to layer
var aIslandList  : Variant;
    aPolygon     : Variant;
    aPoint       : Variant;
    aIslandPoly  : Variant;
    isIslandPoly : boolean;
    PointIterate : p_Point_record;
    IslandIterate: p_LinkedCPoly_record;
begin
   if (aPoly^.isIsland) or (aPoly^.Islands <> nil) then
      isIslandPoly:=TRUE
   else
      isIslandPoly:=FALSE;

   if isIslandPoly then
   begin
      aIslandList:=aDocument.CreateList;
      aPolygon:=aDocument.CreatePolygon;

   end
   else
      aPolygon:=aDocument.CreatePolygon;

   // first create parent area
   PointIterate:=aPoly^.FirstPoint;
   while (PointIterate <> nil) do
   begin
      if (PointIterate^.hidden) then
      begin
         aPolygon.ClosePoly;
         aIslandList.Add(APolygon);
         // check if it is the last poly
         if PointIterate^.next <> nil then
         begin
            aPolygon:=aDocument.CreatePolygon;
            if PointIterate^.next^.hidden then
               PointIterate:=PointIterate^.next;
         end;
      end
      else
      begin
         aPoint:=aDocument.CreatePoint;
         aPoint.X:=PointIterate^.Point.X*100;
         aPoint.Y:=PointIterate^.Point.Y*100;
         aPolygon.InsertPoint(aPoint);
      end;
      PointIterate:=PointIterate^.next;
   end;
   aPolygon.ClosePoly;
   if (isIslandPoly) then
      aIslandList.Add(APolygon);

   // now add islands to list
   IslandIterate:=aPoly^.Islands;
   while (IslandIterate <> nil) do
   begin
      PointIterate:=IslandIterate^.LinkedCPoly^.FirstPoint;
      aPolygon:=aDocument.CreatePolygon;
      while(PointIterate <> nil) do
      begin
         if (PointIterate^.hidden) then
         begin
            aPolygon.ClosePoly;
            aIslandList.Add(APolygon);
            // check if it is the last poly
            if PointIterate^.next <> nil then
            begin
               aPolygon:=aDocument.CreatePolygon;
               if PointIterate^.next^.hidden then
                  PointIterate:=PointIterate^.next;
            end;
         end
         else
         begin
            aPoint:=aDocument.CreatePoint;
            aPoint.X:=PointIterate^.Point.X*100;
            aPoint.Y:=PointIterate^.Point.Y*100;
            aPolygon.InsertPoint(aPoint);
         end;
         PointIterate:=PointIterate^.next;
      end;
      aPolygon.ClosePoly;
      aIslandList.Add(APolygon);
      IslandIterate:=IslandIterate^.next;
   end;

   if isIslandPoly then
   begin
      aIslandPoly:=aDocument.CreateIslandArea(aIslandList);
      aLayer.InsertObject(aIslandPoly);
   end
   else
   begin
      aLayer.InsertObject(aPolygon);
   end;
end;

procedure TCPoly.WriteDebugInfo(aFilename:string; aCPoly:p_CPoly_record);
var DebugFile:TextFile;
    iterate  :p_Point_record;
    hiddenstr:string;
begin
   AssignFile(DebugFile, aFilename);
   if (FileExists(aFilename)) then
      Append(DebugFile)
   else
      Rewrite(DebugFile);
   writeln(DebugFile, '-----------------------------------------');
   iterate:=aCPoly^.FirstPoint;
   while (iterate <> nil) do
   begin
      hiddenstr:='';
      if iterate^.hidden then
         hiddenstr:='HIDDEN';
      if iterate^.addedforcombine then
         hiddenstr:=hiddenstr+' ADDEDFORCOMBINE';
      writeln(DebugFile,'X:' + floattostr(iterate^.Point.X) + ' Y:'+floattostr(iterate^.Point.Y)+' '+hiddenstr);
      iterate:=iterate^.next;
   end;
   writeln(DebugFile, '-----------------------------------------');
   CloseFile(DebugFile);
end;

// function is used to get a pointer to a corner by its index in the
// corner list of the parentpoly
function TCPoly.GetCPolyCornerByIdx(var parentpoly:p_CPoly_record; idx:integer):p_Point_record;
var retVal:p_Point_record;
    idx2go:integer;
    curridx:integer;
begin
   retVal:=parentpoly^.FirstPoint;
   curridx:=1;
   if (idx < 1) then // if the index is less than one then go back from the end
      idx2go:=parentpoly^.LastParentCornerIdx+idx
   else
      idx2go:=idx;

   while (curridx < idx2go) do
   begin
      retVal:=retVal^.next;
      curridx:=curridx+1;
   end;
   result:=retVal;
end;

// procedure is used to add islands of aPoly to current CPoly
procedure  TCPoly.AddIslandCornersToCurrCPoly(aPoly:p_CPoly_record; doCreateIslandConnection:boolean);
var iterate:p_Point_record;
begin
   if (aPoly^.isIsland = FALSE) then
      exit;
   if (Current^.isIsland) and (doCreateIslandConnection) then // check if it is already an island
   begin
      // create connection to next island items
      InsertPoint(Current^.LastPoint^.Point.x,  Current^.LastPoint^.Point.x, TRUE); // create connection point
      InsertPoint(Current^.FirstPoint^.Point.x, Current^.FirstPoint^.Point.x, TRUE); // create connection point
   end;
   iterate:=aPoly^.FirstPoint;
   while (iterate^.hidden = FALSE) do
      iterate:=iterate^.next;
   // now go to first island corner
   iterate:=iterate^.next;
   while (iterate <> nil) do
   begin
      if (iterate^.hidden) and
         (iterate^.Point.x = aPoly^.FirstPoint^.Point.x) and
         (iterate^.Point.y = aPoly^.FirstPoint^.Point.y) then
         InsertPoint(Current^.FirstPoint^.Point.x, Current^.FirstPoint^.Point.x, TRUE) // create connection point
      else
         InsertPoint(iterate^.Point.x, iterate^.Point.y, iterate^.hidden);
      iterate:=iterate^.next;
   end;
   Current^.isIsland:=TRUE;
end;

// function is used to replace the CPoly after pre_CPoly with newCPoly
// return value is pointer to copy of CPoly that has been inserted to list
function TCPoly.ReplaceCPolyAfterCPoly(var pre_CPoly:p_CPoly_record; var newCPoly:p_CPoly_record):p_CPoly_record;
var retVal:p_CPoly_record;
    new_entry:p_CPoly_record;
    iterate1:p_Point_record;
    iterate2:p_Point_record;
    post_CPoly:p_CPoly_record;
begin
   new_entry:=new(p_CPoly_record);
   new_entry^.FirstPoint:=nil;
   new_entry^.LastPoint :=nil;
   new_entry^.Islands   :=nil;
   new_entry^.isIsland  :=false;
   new_entry^.numcorners:=0;
   new_entry^.LastParentCornerIdx:=-1;
   Current:=new_entry;
   iterate1:=newCPoly^.FirstPoint;
   while (iterate1 <> nil) do
   begin
      InsertPoint(iterate1^.Point.x, iterate1^.Point.y, iterate1^.hidden);
      iterate1:=iterate1^.next;
   end;

   // now the record after pre_CPoly can be deleted
   // delete points
   if (pre_CPoly = nil) then
   begin
      iterate1:=FirstEntry^.FirstPoint;
      post_CPoly:=FirstEntry^.next;
   end
   else
   begin
      post_CPoly:=pre_CPoly^.next^.next;
      iterate1:=pre_CPoly^.next^.FirstPoint;
   end;
   if iterate1 <> nil then
   repeat
      iterate2:=iterate1;
      iterate1:=iterate1^.next;
      dispose(iterate2);
   until iterate1 = nil;
   new_entry^.next:=post_CPoly;
   if (pre_CPoly = nil) then
   begin
      dispose(FirstEntry);
      FirstEntry:=new_entry;
   end
   else
   begin
      dispose(pre_CPoly^.next);
      pre_CPoly^.next:=new_entry;
   end;
   result:=new_entry;
end;

// procedure is used to delete entry after pre_CPoly pointer
procedure TCPoly.RemoveCPolyAfterCPoly(var pre_CPoly:p_CPoly_record);
var iterate1:p_Point_record;
    iterate2:p_Point_record;
    post_CPoly:p_CPoly_record;
begin
   if (pre_CPoly = nil) then
   begin
      iterate1:=FirstEntry^.FirstPoint;
      post_CPoly:=FirstEntry^.next;
   end
   else
   begin
      post_CPoly:=pre_CPoly^.next^.next;
      iterate1:=pre_CPoly^.next^.FirstPoint;
   end;
   if iterate1 <> nil then
   repeat
      iterate2:=iterate1;
      iterate1:=iterate1^.next;
      dispose(iterate2);
   until iterate1 = nil;
   if (pre_CPoly = nil) then
   begin
      FirstEntry:=post_CPoly;
   end
   else
   begin
      pre_CPoly^.next:=post_CPoly;
   end;
end;
end.
