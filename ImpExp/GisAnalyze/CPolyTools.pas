unit CPolyTools;

interface

uses GisAnalyzeDef, CPoly;

const  Tolerance = 0.0000001;

type
  TCPolyTools = class
  public
     constructor Create;
     destructor Destroy;
     function CheckPolyInsidePoly (Poly1:p_CPoly_record;Poly2:p_CPoly_record):boolean;                 // function is used to check if poly1 is inside poly2
     function CheckPointInsidePoly(Point:pcpoint; Poly:p_CPoly_record):boolean;                        // function is used to check if a point is inside a poly
     function CheckPolyIsIslandPoly(Poly1:p_CPoly_record;Poly2:p_CPoly_record):boolean;                // function is used to check if Poly1 is already an island of Poly2
     function AddIntersectionNodesToPoly(var Poly1:p_CPoly_record; var Poly2:p_CPoly_record):integer;  // function is used to add all intersection nodes between poly1 and poly2 to poly1 and poly2
     procedure PrepareForCombineCPoly;
     function CombinePolys(var Poly1:p_CPoly_record; var Poly2:p_CPoly_record):boolean;                // function is used to combine two polys on their intersection line
     function GetCPolyCombineResult:p_CPoly_record;
     procedure RemoveAfterCombineCPoly;
     procedure SetSnapRadius(sr:double);
  private
     PointOnLineCnt:integer;
     FoundIdxArray: array of boolean;
     TmpCPolyObj:TCPoly;
     SnapRadius :double;
     function Checkrectoverlapped(Point1_LU:pcpoint;Point1_RB:pcpoint;Point2_LU:pcpoint;Point2_RB:pcpoint;var Inside:integer):integer;
     function CutLines(L1P1:PCPoint;L1P2:PCPoint;L2P1:PCPoint;L2P2:PCPoint;Art:boolean):Integer;
     function Cutpoint(L1P1:pcpoint;L1P2:pcpoint;Point:pcpoint):boolean;
     function CheckPointIsIntersectionNodeOnPoly(Point:pcpoint; var Poly:p_CPoly_record; doinsert:boolean):boolean;
     function CheckPointIsCornerOfPoly(Point:pcpoint; Poly:p_CPoly_record;var PointIdx:integer):p_Point_record;
     procedure RemoveAddedForCombineCornersFromCPoly(var Poly:p_CPoly_record);
     procedure AddCornersToNewCombinedPoly(var parentpoly:p_CPoly_record; startcorner: p_Point_record; endcorner: p_Point_record; doclockdir:boolean);
     function CheckHasConnectionLine(numelems:integer; var firstcorneridx:integer; var lastcorneridx:integer):boolean;
     procedure RotateCombineCPoly(var aPoly:p_CPoly_record; numrotations:integer);
  end;

implementation

constructor TCPolyTools.Create;
begin
   PointOnLineCnt:=0;
   SnapRadius:=0.01; // default snap radius is 0.01 meter
end;

destructor TCPolyTools.Destroy;
begin
end;

procedure TCPolyTools.SetSnapRadius(sr:double);
begin
   SnapRadius:=sr;
end;

{*******************************************************************************
* FUNKTION: TCPolyTools.CheckRectOverlapped                                    *
********************************************************************************
* Description:  Function is used to cut two rectangles and returns number of   *
*               intersection points.                                           *
*                                                                              *
* PARAMETERS    Point1_LU -> Point of left upper corner of first rectangle     *
*               Point1_RB -> Point of right bottom corner of first rectangle   *
*               Point2_LU -> Point of left upper corner of second rectangle    *
*               Point2_RB -> Point of right bottom corner of second rectangle  *
*                                                                              *
*               Inside    -> 0 no rectangle is inside the other                *
*                            1 first rectangle is inside the other             *
*                            2 second rectangle is inside the other            *
*                                                                              *
* RETURN VALUE: number of intersection points                                  *
********************************************************************************}
function TCPolyTools.CheckRectOverlapped(Point1_LU:pcpoint;Point1_RB:pcpoint;Point2_LU:pcpoint;Point2_RB:pcpoint;var Inside:integer):integer;
var Point:pcpoint;
    intersect1:integer;
    intersect2:integer;
begin
   intersect1:=0; intersect2:=0;
   Point.X:=Point1_LU.x; Point.Y:=Point1_RB.y;
   // check if point is inside area
   if (Point.X >= Point2_LU.X) and (Point.X <= Point2_RB.X) and
      (Point.Y >= Point2_RB.Y) and (Point.Y <= Point2_LU.Y) then intersect1:=intersect1+1;

   Point.X:=Point1_RB.x; Point.Y:=Point1_RB.y;
   // check if point is inside area
   if (Point.X >= Point2_LU.X) and (Point.X <= Point2_RB.X) and
      (Point.Y >= Point2_RB.Y) and (Point.Y <= Point2_LU.Y) then intersect1:=intersect1+1;

   Point.X:=Point1_RB.x; Point.Y:=Point1_LU.y;
   // check if point is inside area
   if (Point.X >= Point2_LU.X) and (Point.X <= Point2_RB.X) and
      (Point.Y >= Point2_RB.Y) and (Point.Y <= Point2_LU.Y) then intersect1:=intersect1+1;

   Point.X:=Point1_LU.x; Point.Y:=Point1_LU.y;
   // check if point is inside area
   if (Point.X >= Point2_LU.X) and (Point.X <= Point2_RB.X) and
      (Point.Y >= Point2_RB.Y) and (Point.Y <= Point2_LU.Y) then intersect1:=intersect1+1;

   Point.X:=Point2_LU.x; Point.Y:=Point2_RB.y;
   // check if point is inside area
   if (Point.X >= Point1_LU.X) and (Point.X <= Point1_RB.X) and
      (Point.Y >= Point1_RB.Y) and (Point.Y <= Point1_LU.Y) then intersect2:=intersect2+1;

   Point.X:=Point2_RB.x; Point.Y:=Point2_RB.y;
   // check if point is inside area
   if (Point.X >= Point1_LU.X) and (Point.X <= Point1_RB.X) and
      (Point.Y >= Point1_RB.Y) and (Point.Y <= Point1_LU.Y) then intersect2:=intersect2+1;

   Point.X:=Point2_RB.x; Point.Y:=Point2_LU.y;
   // check if point is inside area
   if (Point.X >= Point1_LU.X) and (Point.X <= Point1_RB.X) and
      (Point.Y >= Point1_RB.Y) and (Point.Y <= Point1_LU.Y) then intersect2:=intersect2+1;

   Point.X:=Point2_LU.x; Point.Y:=Point2_LU.y;
   // check if point is inside area
   if (Point.X >= Point1_LU.X) and (Point.X <= Point1_RB.X) and
      (Point.Y >= Point1_RB.Y) and (Point.Y <= Point1_LU.Y) then intersect2:=intersect2+1;

   Inside:=0;
   if (intersect1 = 4 ) then Inside:=1;
   if (intersect2 = 4 ) then Inside:=2;
   result:=intersect1+intersect2;
end;

// function is used to check if poly1 is inside poly2
function TCPolyTools.CheckPolyinsidePoly(Poly1:p_CPoly_record;Poly2:p_CPoly_record):boolean;
var Points1      :p_Point_record;
    Points2      :p_Point_record;
    intersections:integer;
    inside       :boolean;
    InPoint1     :pcpoint;
    InPoint2     :pcpoint;
    IntersectionPoint1:pcpoint;
    IntersectionPoint2:pcpoint;
    PointsOnLine:integer;
    rectOverlap :integer;
begin
   // first it will be checked if the rectangles overlap
   Checkrectoverlapped(Poly1^.Rect_LU,Poly1^.Rect_RB,Poly2^.Rect_LU,Poly2^.Rect_RB,rectOverlap);
   if (rectOverlap <> 1) then
   begin
      result:=false;
      PointOnLineCnt:=0;
      exit;
   end;

   inside:=true;
   Points1:=Poly1^.FirstPoint;
   repeat
      InPoint1.X:=Points1^.Point.X; InPoint1.Y:=Points1^.Point.Y;
      InPoint2.X:=InPoint1.X; InPoint2.Y:=1000000.00;

      PointOnLineCnt:=0;
      intersections:=0;
      Points2:=Poly2^.FirstPoint;
      repeat
         IntersectionPoint1:=Points2^.Point;
         if (Points2^.next <> NIL) then
         begin
            Points2:=Points2^.next;
            IntersectionPoint2:=Points2^.Point;
            if Points2^.hidden = FALSE then
               intersections:=intersections + Cutlines (IntersectionPoint1, IntersectionPoint2, InPoint1, InPoint2, FALSE);
         end;
      until (Points2^.next = NIL) or (Points2^.hidden);

      // cut with line between first and last point of area
      intersections:=intersections + CutLines(Poly2^.FirstPoint^.Point, Points2^.Point,InPoint1,InPoint2,TRUE);
      PointsOnLine:=PointOnLineCnt;

      // if there were corners of polygons that have have been directly on the
      // line to cut one Schnittpunkt has to be subtracted
      if (PointsOnLine mod 2 = 0) and (PointsOnLine > 0) then
         intersections:=intersections-1;

      if (intersections mod 2 = 0) then
         inside:=false;

      Points1:=Points1^.next;
   until (Points1^.next = nil) or (inside = FALSE);
   // reset the PointsOnLine
   PointOnLineCnt:=0;
   result:=inside;
end;

// function to check if a point is inside a poly
function TCPolyTools.CheckPointInsidePoly(Point:pcpoint; Poly:p_CPoly_record):boolean;
var retVal       :boolean;
    CheckPoint1  :pcpoint;
    CheckPoint2  :pcpoint;
    PointIterate :p_point_record;
    intersections:integer;
    IntersectionPoint1:pcpoint;
    IntersectionPoint2:pcpoint;
    DoNotCheck:boolean;
begin
   retVal:=FALSE;
   // first check if the point is inside the bounding-rectangle of the CPoly
   if (Point.X >= Poly^.Rect_LU.X) and (Point.X <= Poly^.Rect_RB.X) and
      (Point.Y <= Poly^.Rect_LU.Y) and (Point.Y >= Poly^.Rect_RB.Y) then
   begin
      // the point is inside the rectangle
      CheckPoint1.X:=Point.X; CheckPoint1.Y:=Point.Y;
      CheckPoint2.X:=Point.X; CheckPoint2.Y:=1000000.00;

      // now cut with all lines of poly
      PointOnLineCnt:=0;
      intersections:=0;
      Pointiterate:=Poly^.FirstPoint;

      repeat
         IntersectionPoint1:=Pointiterate^.Point;
         DoNotCheck:=false;
         if Pointiterate^.hidden then DoNotCheck:=true;
         if (Pointiterate^.next <> NIL) then
         begin
            Pointiterate:=Pointiterate^.next;
            if Pointiterate^.hidden then DoNotCheck:=true;
            IntersectionPoint2:=Pointiterate^.Point;
            if not DoNotCheck then
               intersections:=intersections + Cutlines (IntersectionPoint1, IntersectionPoint2, CheckPoint1, CheckPoint2,FALSE);
         end;
      until (Pointiterate^.next = NIL);

      // if there were corners of polygons that have have been directly on the
      // line to cut one Schnittpunkt has to be subtracted
      if (PointOnLineCnt mod 2 = 0) and (PointOnLineCnt > 0) then
         intersections:=intersections-1;

      if (intersections mod 2 = 1)and (intersections > 0) then
         Result:=true
      else
         Result:=false;
   end
   else
      result:=false;
   // reset the PointOnLineCnt
   PointOnLineCnt:=0;
end;

{*******************************************************************************
* FUNCTION: TCPolyTools.Cutpoint                                               *
********************************************************************************
* Function is used to check if a point is positionated on a line               *
*                                                                              *
* Parameters: L1P1  -> Point1 of line                                          *
*             L1P2  -> Point2 of line                                          *
*             Point -> Point that should be checked                            *
*                                                                              *
* Returnvalue: TRUE  -> Point exists on line                                   *
*              FALSE -> Point exists not on line                               *
********************************************************************************}
function TCPolyTools.Cutpoint(L1P1:pcpoint;L1P2:pcpoint;Point:pcpoint):boolean;
var k:double;
    d:double;
    h1:double;
    h2:double;
begin
   // check if it�s possible that point is on line
   if not(((Point.X >= L1P1.X) and (Point.X <= L1P2.X)) or
          ((Point.X >= L1P2.X) and (Point.X <= L1P1.X))) then
   begin
      result:=FALSE;
      exit;
   end;

   if not(((Point.Y >= L1P1.Y) and (Point.Y <= L1P2.Y)) or
          ((Point.Y >= L1P2.Y) and (Point.Y <= L1P1.Y))) then
   begin
      result:=FALSE;
      exit;
   end;


   // calculate k and d
   h1:=(L1P1.X - L1P2.X);
   if h1 = 0 then
   begin
      if (Point.X = L1P1.X) and
         (((Point.Y >= L1P1.Y) and (Point.Y <= L1P2.Y)) or ((Point.Y >= L1P2.Y) and (Point.Y <= L1P1.Y))) then
         result:=true else result:=false;
      exit;
   end;
   h2:=(L1P1.Y - L1P2.Y);
   if h2 = 0 then
   begin
      if not (((Point.X >= L1P1.X) and (Point.X <= L1P2.X)) or
         ((Point.X <= L1P1.X) and (Point.X >= L1P2.X))) then
      begin
         result:=FALSE;
         exit;
      end;
   end;

   k:=(L1P1.Y-L1P2.Y) / h1;
   d:=L1P1.Y - (k * L1P1.X);

   h2:=((k * Point.X) + d);
   // a tolerance of 0.1 meter is allowed
   if (h2 >= Point.Y - SnapRadius) and (h2 <= Point.Y + SnapRadius) then result:=true else result:=false;
end;

{*******************************************************************************
* FUNCTION: TCPolyTools.CutLines                                               *
********************************************************************************
* Function is used to calculate intersection points und L2P1->L2P2.            *
* L1P1             i = Anfangspunkt erste Linie                                *
* L1P2             i = Endpunkt erste Linie                                    *
* L2P1             i = Anfangspunkt zweite Linie                               *
* L2P2             i = Endpunkt zweite Linie                                   *
*                      als zwei Schnittpunkte                                  *
* Art              Wenn TRUE gilt �berlappen zweier Linien als Schnittpunkt    *
*                  Wenn FALSE gilt �berlappen zweier Linien nicht als Schnitt  *
********************************************************************************
* Ergebnis: Anzahl der Schnittpunkte.                                          *
*******************************************************************************}
Function TCPolyTools.CutLines
   (
   L1P1         : PCPoint;
   L1P2         : PCPoint;
   L2P1         : PCPoint;
   L2P2         : PCPoint;
   Art          : boolean
   )
   : Integer;
  var D            : Double;
      DX1          : Double;
      DY1          : Double;
      DX2          : Double;
      DY2          : Double;
      t0           : Double;
      u0           : Double;
      tc           : Double;
      td           : Double;
      Schnittpunkt : integer;
  begin
    CutLines:=0;
    Schnittpunkt:=0;
    DX1:=L1P2.X-L1P1.X; if dx1 = 0 then dx1:=0.000001;
    DX2:=L2P2.X-L2P1.X; if dx2 = 0 then dx2:=0.000001;
    DY1:=L1P2.Y-L1P1.Y; if dy1 = 0 then dy1:=0.000001;
    DY2:=L2P2.Y-L2P1.Y; if dy2 = 0 then dy2:=0.000001;
    D:=DX1*DY2-DY1*DX2;
    if Abs(D)>=Tolerance then begin
      t0:=((L2P1.X-L1P1.X)*DY2-(L2P1.Y-L1P1.Y)*DX2)/D;
      if (t0>-Tolerance) and (t0<1.0+Tolerance) then begin
        if Abs(DX2)>Abs(DY2) then u0:=((L1P1.X-L2P1.X)+DX1*t0)/DX2
        else u0:=((L1P1.Y-L2P1.Y)+DY1*t0)/DY2;
        if (u0>-Tolerance) and (u0<1.0+Tolerance) then begin
           CutLines:=1; Schnittpunkt:=1;
        end;
      end;
    end
    else if (Abs(DX1) < Tolerance) and (Abs(DY1) < Tolerance) then begin
      Schnittpunkt:=0;
      CutLines:=0;
    end
    else if Abs(DX1*(L2P1.Y-L1P1.Y)-DY1*(L2P1.X-L1P1.X))<Tolerance then begin
      if Abs(DX1)>Abs(DY1) then begin
        tc:=(L2P1.X-L1P1.X)/DX1;
        td:=(L2P2.X-L1P1.X)/DX1;
      end
      else begin
           tc:=(L2P1.Y-L1P1.Y)/DY1;
           td:=(L2P2.Y-L1P1.Y)/DY1;
      end;
      if ((tc>-Tolerance) and (td<1.0+Tolerance))
          or ((tc<1.0+Tolerance) and (td>-Tolerance)) then begin
        if tc<0.0 then tc:=0.0
        else if tc>1.0 then tc:=1.0;
        if td<0.0 then td:=0.0
        else if td>1.0 then td:=1.0;

        if Abs(tc-td)<Tolerance then begin CutLines:=1; Schnittpunkt:=1; end
        else begin
          Schnittpunkt:=2;
          CutLines:=2;
        end;
      end;
    end;
    if ((Schnittpunkt = 1) and (L2P1.Y >= L1P1.Y) and (L2P1.Y >= L1P2.Y)) then
       CutLines:=0;

    if (Art = false) and (Schnittpunkt = 2) then Cutlines:=0;
    if (Art = false) and (Schnittpunkt = 1) then
    begin {Kontrolle, ob der Schnittpunkt nicht auf der Linie liegt }
       if Schnittpunkt <> 0 then if CutPoint(L1P1,L1P2,L2P1) then begin {Schnittpunkt:=0;} PointOnLineCnt:=PointOnLineCnt+1; end;
       if Schnittpunkt <> 0 then if CutPoint(L1P1,L1P2,L2P2) then begin {Schnittpunkt:=0;} PointOnLineCnt:=PointOnLineCnt+1; end;
       if Schnittpunkt <> 0 then if CutPoint(L2P1,L2P2,L1P1) then begin {Schnittpunkt:=0;} PointOnLineCnt:=PointOnLineCnt+1; end;
       if Schnittpunkt <> 0 then if CutPoint(L2P1,L2P2,L1P2) then begin {Schnittpunkt:=0;} PointOnLineCnt:=PointOnLineCnt+1; end;
       Cutlines:=Schnittpunkt;
    end;

    if (Art = false) and (L1P1.X = L2P1.X) and (L1P1.Y = L2P1.Y) then Cutlines:=0;
    if (Art = false) and (L1P1.X = L2P2.X) and (L1P1.Y = L2P2.Y) then Cutlines:=0;
    if (Art = false) and (L1P2.X = L2P1.X) and (L1P2.Y = L2P1.Y) then Cutlines:=0;
    if (Art = false) and (L1P2.X = L2P2.X) and (L1P2.Y = L2P2.Y) then Cutlines:=0;
end;

// function is used to check if Poly1 is already an island of Poly2
function TCPolyTools.CheckPolyIsIslandPoly(Poly1:p_CPoly_record;Poly2:p_CPoly_record):boolean;
var retVal:boolean;
    iteratepoly1:p_Point_record;
    iteratepoly2:p_Point_record;
    pointsequal :boolean;
begin
   retVal:=FALSE;
   iteratepoly2:=Poly2^.FirstPoint;
   while (iteratepoly2 <> nil) and (not retVal) do
   begin
      if (iteratepoly2^.hidden) then
      begin
         iteratepoly2:=iteratepoly2^.next;
         iteratepoly1:=Poly1^.FirstPoint;
         // now check if the polys are equal
         pointsequal:=TRUE;
         while (iteratepoly2 <> nil) and (pointsequal) and (iteratepoly1 <> nil) do
         begin
            if (iteratepoly2^.Point.x <> iteratepoly1^.Point.x) or
               (iteratepoly2^.Point.y <> iteratepoly1^.Point.y) or
               (iteratepoly2^.hidden  <> iteratepoly1^.hidden) then
               pointsequal:=FALSE
            else
            begin
               iteratepoly1:=iteratepoly1^.next;
               iteratepoly2:=iteratepoly2^.next;
            end;
         end;
         if (pointsequal) then
         begin
            if (iteratepoly1 = nil) and (iteratepoly2 <> nil) then
            begin
               if (not iteratepoly2^.hidden) then
                  pointsequal:=FALSE;
            end
            else if (iteratepoly2 = nil) and (iteratepoly1 <> nil) then
            begin
               if (not iteratepoly1^.hidden) then
                  pointsequal:=FALSE;
            end
            else if (iteratepoly2 <> nil) and (iteratepoly1 <> nil) then
               if (not iteratepoly1^.hidden) and (not iteratepoly2^.hidden) then
                  pointsequal:=FALSE;
         end;
         if (pointsequal) then
            retVal:=TRUE;
      end
      else
         iteratepoly2:=iteratepoly2^.next;
   end;
   result:=retVal;
end;

// function is used to add all intersection nodes between poly1 and poly2 to poly1 and poly2
function TCPolyTools.AddIntersectionNodesToPoly(var Poly1:p_CPoly_record; var Poly2:p_CPoly_record):integer;
var retVal:integer;
    iterate:p_Point_record;
    inside:integer;
    onpoly1:boolean;
    onpoly2:boolean;
    docheck:boolean;
begin
   // return code is 0 if there were no intersection nodes between Poly1 and Poly2
   // return code is 1 if there have been added intersection nodes only on Poly1
   // return code is 2 if there have been added intersection nodes only on Poly2
   // return code is 3 if there have been added intersection nodes on Poly1 and on Poly2
   retVal:=0;
   // first check if the rectangles of the two polys overlap
   if Checkrectoverlapped(Poly1^.Rect_LU,Poly1^.Rect_RB,Poly2^.Rect_LU,Poly2^.Rect_RB, inside) = 0 then
   begin
      result:=retVal;
      exit;
   end;
   // Now check if if poly1 has intersection corners in poly2
   onpoly1:=FALSE;
   iterate:=Poly1^.FirstPoint;
   docheck:=TRUE;
   while (iterate <> nil) and (docheck) do
   begin
      if (iterate^.hidden = FALSE) then
      begin
         if CheckPointIsIntersectionNodeOnPoly(iterate^.Point, Poly2, TRUE) then
            onpoly1:=TRUE;
      end
      else
         docheck:=FALSE;
      iterate:=iterate^.next;
   end;
   // Now check if if poly2 has intersection corners in poly1
   onpoly2:=FALSE;
   iterate:=Poly2^.FirstPoint;
   docheck:=TRUE;
   while (iterate <> nil) and (docheck) do
   begin
      if (iterate^.hidden = FALSE) then // the island corners must not be checked
      begin
         if CheckPointIsIntersectionNodeOnPoly(iterate^.Point, Poly1, TRUE) then
            onpoly2:=TRUE;
      end
      else
         docheck:=FALSE;
      iterate:=iterate^.next;
   end;
   if (onpoly1 and onpoly2) then retVal:=3
   else if (onpoly2 and not onpoly1) then retVal:=2
   else if (onpoly1 and not onpoly2) then retval:=1;
   result:=retVal;
end;

// function is used to check if Point is on line of Poly
// if doinsert = TRUE then insert Point to Poly if it is on line
function TCPolyTools.CheckPointIsIntersectionNodeOnPoly(Point:pcpoint; var Poly:p_CPoly_record; doinsert:boolean):boolean;
var retVal:boolean;
    iterate:p_Point_record;
    LPoint1, LPoint2:pcpoint;
    newcorner:p_Point_record;
    docheck:boolean;
begin
   retVal:=FALSE;
   // first check if Point is inside rect of Poly
   if (Point.x >= Poly^.Rect_LU.x) and (Point.x <= Poly^.Rect_RB.x) and
      (Point.y >= Poly^.Rect_RB.y) and (Point.y <= Poly^.Rect_LU.y) then
   begin
      iterate:=Poly^.FirstPoint;
      docheck:=TRUE;
      while (iterate^.next <> nil) and (not retVal) and (docheck) do
      begin
         LPoint1:=iterate^.Point;
         LPoint2:=iterate^.next^.Point;
         if (iterate^.next^.hidden) then docheck:=FALSE;
         if docheck then
         begin
            // the point may not be equal to a corner point of poly
            if not(((LPoint1.x = Point.x) and (LPoint1.y = Point.y)) or
               ((LPoint2.x = Point.x) and (LPoint2.y = Point.y))) then
            begin
               // check if point is on line
               retVal:=Cutpoint(LPoint1,LPoint2,Point);
               if (retVal and doinsert) then
               begin
                  // the corner has to be inserted to Poly
                  new(newcorner);
                  newcorner^.hidden:=FALSE;
                  newcorner^.Point:=Point;
                  newcorner^.next:=iterate^.next;
                  newcorner^.addedforcombine:=TRUE;
                  iterate^.next:=newcorner;
                  if newcorner^.next = nil then
                     Poly^.LastPoint:=newcorner;
                  Poly^.numcorners:=Poly^.numcorners+1;
                  Poly^.LastParentCornerIdx:=Poly^.LastParentCornerIdx+1;
               end;
            end;
         end;
         iterate:=iterate^.next;
      end;
   end;
   result:=retVal;
end;

// function is used to check if a point is a corner of a poly
function TCPolyTools.CheckPointIsCornerOfPoly(Point:pcpoint; Poly:p_CPoly_record;var PointIdx:integer):p_Point_record;
var retVal :p_Point_record;
    iterate:p_Point_record;
    docheck:boolean;
begin
   // returns pointer to corner of Poly that is equal to Point
   // return NIL if the Poly does not contain the Point
   retVal:=nil;
   PointIdx:=1;
   // first check if the corner is inside the rect of poly
   if (Point.x >= Poly^.Rect_LU.x) and (Point.x <= Poly^.Rect_RB.x) and
      (Point.y >= Poly^.Rect_RB.y) and (Point.y <= Poly^.Rect_LU.y) then
   begin
      iterate:=Poly^.FirstPoint;
      docheck:=TRUE;
      while (iterate <> nil) and (docheck) and (retVal = nil) do
      begin
         // the island corners need not to be checked
         if (iterate^.hidden) then docheck:=FALSE;
         if docheck then
         begin
            if (iterate^.Point.x = Point.X) and
               (iterate^.Point.y = Point.Y) then
            begin
               // the corner exists
               retVal:=iterate;
            end;
         end;
         if (retVal = nil) then
         begin
            PointIdx:=PointIdx+1;
            iterate:=iterate^.next;
         end;
      end;
   end;
   result:=retVal;
end;

// function is used to combine two polys on their intersection line
// the result is TRUE if the two polys could be combined
// the result is FALSE if the two polys could not be combined
// if the result is TRUE the result is stored in the CombinePolyCPolyObj
function TCPolyTools.CombinePolys(var Poly1:p_CPoly_record; var Poly2:p_CPoly_record):boolean;
var inside    :integer;
    iterate   :p_Point_record;
    polycorner:p_Point_record;
    firstlinecorner:p_Point_record;
    lastlinecorner :p_Point_record;
    lastcpolycorner:p_Point_record;
    docheck:boolean;
    connectionlines:integer;
    lastpointwasonpoly:boolean;
    numpointsonconnectionline:integer;
    intersectiontype:integer;
    doclockdir      :boolean;
    pointidx        :integer;
    firstcorneridx, lastcorneridx:integer;
    mayaddconnectionpoint:boolean;
    numcutlines:integer;
    lastpointwasonline:boolean;
    firstpointwascutpoint:boolean;
    lastpointwascutpoint:boolean;
    maycombinecpolys:boolean;
    Poly2Pointidx:integer;
    LastPoly2PointIdx:integer;
    LastFirstCornerIdx:integer;
    OverAllCutPoints:integer;
    found:boolean;
begin
   // function returns pointer to new created cpoly that is the result of
   // the combination of poly1 and poly2
   // if the polys could not be combined the function returns NIL
   // check if the rectangles do overlap, if not the polys can not be combined
   if Checkrectoverlapped(Poly1^.Rect_LU,Poly1^.Rect_RB,Poly2^.Rect_LU,Poly2^.Rect_RB, inside) = 0 then
   begin
      result:=FALSE;
      exit;
   end;
   // first the intersection nodes will be added to both polys
   intersectiontype:=AddIntersectionNodesToPoly(Poly1,Poly2);
   // now find the line that combines Poly1 and Poly2
   iterate:=Poly1^.FirstPoint;
   pointidx:=1;
   numcutlines:=0;
   pointonlinecnt:=0;
   lastpointwasonline:=FALSE;
   firstpointwascutpoint:=FALSE;
   lastpointwascutpoint:=FALSE;
   docheck:=TRUE;
   OverAllCutPoints:=0;
   SetLength(FoundIdxArray, Poly1^.LastParentCornerIdx+1);

   while (iterate <> nil) and (pointidx <= Poly1^.LastParentCornerIdx) and (docheck) do
   begin
      if (CheckPointIsCornerOfPoly(iterate^.Point, Poly2, Poly2Pointidx) <> nil) then
      begin
         FoundIdxArray[pointidx]:=TRUE;
         pointonlinecnt:=pointonlinecnt+1;
         if (pointidx = 1) then
            firstpointwascutpoint:=TRUE;
         if (pointidx = Poly1^.LastParentCornerIdx) then
            lastpointwascutpoint:=TRUE;
         OverAllCutPoints:=OverAllCutPoints+1; // count complete number of Cut-Points
         if (lastpointwasonline) then
         begin
            firstcorneridx:=LastFirstCornerIdx;
            // check if the LastPoly2PointIdx is a neighbour of Poly2PointIdx
            if (LastPoly2Pointidx + 1 = Poly2PointIdx) or
               (LastPoly2Pointidx - 1 = Poly2PointIdx) or
               ((LastPoly2Pointidx = Poly2^.LastParentCornerIdx) and (Poly2PointIdx = 1)) or
               ((LastPoly2Pointidx = 1) and (Poly2PointIdx = Poly2^.LastParentCornerIdx)) then
            begin
               if (pointonlinecnt = 2) then
                  numcutlines:=numcutlines+1;
               if (LastPoly2Pointidx + 1 = Poly2PointIdx) then
                  doclockdir:=FALSE
               else if (LastPoly2Pointidx - 1 = Poly2PointIdx) then
                  doclockdir:=TRUE
               else if (LastPoly2Pointidx = Poly2^.LastParentCornerIdx) and (Poly2PointIdx = 1) then
                  doclockdir:=FALSE
               else
                  doclockdir:=TRUE;
               lastcorneridx:=pointidx;
               lastpointwasonline:=TRUE;
               LastPoly2Pointidx:=Poly2PointIdx;
            end
            else
            begin
               lastpointwasonline:=TRUE;
               pointonlinecnt:=1;
               LastPoly2Pointidx:=Poly2PointIdx;
            end;
         end
         else
         begin
            LastPoly2PointIdx:=Poly2PointIdx;
            LastFirstCornerIdx:=pointidx;
            lastpointwasonline:=TRUE;
         end;

         if (numcutlines > 1) and (firstpointwascutpoint = FALSE) then
            docheck:=FALSE;
      end
      else
      begin
         FoundIdxArray[pointidx]:=FALSE;
         lastpointwasonline:=FALSE;
         pointonlinecnt:=0;
      end;
      pointidx:=pointidx+1;
      iterate:=iterate^.next;
   end;


   // if there was no cutline found and also not last and first point are cutpoints the
   // polys can not be combined
   if (numcutlines < 1) then
   begin
      if (lastpointwascutpoint = TRUE) and (firstpointwascutpoint = TRUE) then
         docheck:=TRUE
      else
         docheck:=FALSE;
   end
   else
   begin
      if (numcutlines > 1) and (not lastpointwascutpoint) then
         docheck:=FALSE
      else
      begin
         if (numcutlines = 1) then
         begin
            // check if one of the polys is an island of the other
            if (Poly1^.LastParentCornerIdx <= Poly2^.LastParentCornerIdx) and (OverAllCutPoints >= Poly1^.LastParentCornerIdx) then // Poly1 is envolved by Poly2
               docheck:=FALSE
            else if (Poly2^.LastParentCorneridx <= Poly1^.LastParentCornerIdx) and (OverAllCutPoints >= Poly2^.LastParentCornerIdx) then // Poly2 is envolved by Poly1
               docheck:=FALSE;
            if docheck then
            begin
               // one poly could be inside the other poly but with one cutline
               // find the first point of Poly1 that is not on the cutline and check if it is
               // inside of Poly2
               pointidx:=1;  OverAllCutPoints:=0; numpointsonconnectionline:=0;
               iterate:=Poly1^.FirstPoint;
               while (iterate <> nil) and (pointidx <= Poly1^.LastParentCornerIdx) do
               begin
                  if (CheckPointIsCornerOfPoly(iterate^.Point, Poly2, Poly2Pointidx) = nil) then
                  begin
                     if CheckPointInsidePoly(iterate^.Point, Poly2) then
                        OverAllCutPoints:=OverAllCutPoints+1;
                  end
                  else
                     numpointsonconnectionline:=numpointsonconnectionline+1;
                  iterate:=iterate^.next;
                  pointidx:=pointidx+1;
               end;
               if OverAllCutPoints+numpointsonconnectionline = Poly1^.LastParentCornerIdx then
                  docheck:=FALSE;
               if docheck then
               begin
                  // find the first point of Poly2 that is not on the cutline and check if it is
                  // inside of Poly1
                  pointidx:=1;  OverAllCutPoints:=0; numpointsonconnectionline:=0;
                  iterate:=Poly2^.FirstPoint;
                  while (iterate <> nil) and (pointidx <= Poly2^.LastParentCornerIdx) do
                  begin
                     if (CheckPointIsCornerOfPoly(iterate^.Point, Poly1, Poly2Pointidx) = nil) then
                     begin
                        if CheckPointInsidePoly(iterate^.Point, Poly1) then
                           OverAllCutPoints:=OverAllCutPoints+1;
                     end
                     else
                        numpointsonconnectionline:=numpointsonconnectionline+1;
                     iterate:=iterate^.next;
                     pointidx:=pointidx+1;
                  end;
                  if OverAllCutPoints+numpointsonconnectionline = Poly2^.LastParentCornerIdx then
                     docheck:=FALSE;
               end;
            end;
         end;
      end;
   end;

   if (not docheck) then // the polys can not be combined
   begin
      // remove intersection points if there have been added
      if (intersectiontype = 1) then // remove from Poly1
      begin
         RemoveAddedForCombineCornersFromCPoly(Poly1);
      end;
      if (intersectiontype = 2) then // remove from Poly2
      begin
         RemoveAddedForCombineCornersFromCPoly(Poly2);
      end;
      if (intersectiontype = 3) then // remove from Poly1 and Poly2
      begin
         RemoveAddedForCombineCornersFromCPoly(Poly1);
         RemoveAddedForCombineCornersFromCPoly(Poly2);
      end;
      result:=FALSE;
      exit;
   end;

   maycombinecpolys:=TRUE;
   // if the firstpoint and the lastpoint was on the cpoly check
   // if there exists only one connection line
   if (firstpointwascutpoint and lastpointwascutpoint) then
   begin
      maycombinecpolys:=CheckHasConnectionLine(Poly1^.LastParentCornerIdx, firstcorneridx, lastcorneridx);
      // get direction how it should be handeled
      firstlinecorner:=TmpCPolyObj.GetCPolyCornerByIdx(Poly1, firstcorneridx);
      CheckPointIsCornerOfPoly(firstlinecorner^.Point, Poly2, LastPoly2Pointidx);
      lastlinecorner :=TmpCPolyObj.GetCPolyCornerByIdx(Poly1, lastcorneridx);
      CheckPointIsCornerOfPoly(lastlinecorner^.Point, Poly2, Poly2Pointidx);
      if (LastPoly2Pointidx < Poly2Pointidx) then
         doclockdir:=TRUE
      else
         doclockdir:=FALSE;
   end;

   if (maycombinecpolys) then
   begin
      // reset the addedforcombine flag on CPoly1 and CPoly2
      if (intersectiontype = 1) or (intersectiontype = 3) then
      begin
         iterate:=Poly1^.FirstPoint;
         docheck:=TRUE;
         while (iterate <> nil) and (docheck) do
         begin
            if (iterate^.hidden) then docheck:=FALSE;
            iterate^.addedforcombine:=FALSE;
            iterate:=iterate^.next;
         end;
      end;

      if (intersectiontype = 2) or (intersectiontype = 3) then
      begin
         iterate:=Poly2^.FirstPoint;
         docheck:=TRUE;
         while (iterate <> nil) and (docheck) do
         begin
            if (iterate^.hidden) then docheck:=FALSE;
            iterate^.addedforcombine:=FALSE;
            iterate:=iterate^.next;
         end;
      end;
      // check if the firstcorner is behind the last corner
      // if the lastcorneridx is less than the firstcorneridx the
      // CPoly1 has to be rotated against clock that the firstcorner is
      // before the lastcorner
      firstlinecorner:=TmpCPolyObj.GetCPolyCornerByIdx(Poly1, firstcorneridx);
      lastlinecorner :=TmpCPolyObj.GetCPolyCornerByIdx(Poly1, lastcorneridx);

      if (firstcorneridx > lastcorneridx) then
         RotateCombineCPoly(Poly1, Poly1^.LastParentCornerIdx-firstcorneridx+1);
      // the other poly is always against clock-dir
      CheckPointIsCornerOfPoly(firstlinecorner^.Point, Poly2, Poly2Pointidx);
      CheckPointIsCornerOfPoly(lastlinecorner^.Point,  Poly2, LastPoly2Pointidx);
      if (Poly2Pointidx > LastPoly2Pointidx) then
         RotateCombineCPoly(Poly2, Poly2^.LastParentCornerIdx-Poly2Pointidx+1);

      // check if first corner of Poly2 is a Cutline-corner and last corner of Poly2 is a
      // Cutline corner. In this case the rotation for combining the two polys is in clockdir
      // in all other cases it is against clockdir
      doclockdir:=FALSE;

      lastcpolycorner:=TmpCPolyObj.GetCPolyCornerByIdx(Poly2, Poly2^.LastParentCornerIdx);
      if (CheckPointIsCornerOfPoly(Poly2^.FirstPoint^.Point, Poly1, Poly2Pointidx) <> nil) and
         (CheckPointIsCornerOfPoly(lastcpolycorner^.Point, Poly1, Poly2Pointidx) <> nil) then
         doclockdir:=TRUE;


      // create new entry CPoly entry that stores the
      // result of the combining

      // first set all corners that have the flag that they are added
      // for combining to addedforcombine=FALSE
      lastcpolycorner:=TmpCPolyObj.GetCPolyCornerByIdx(Poly1, Poly1^.LastParentCornerIdx);

      TmpCPolyObj.NewEntry;
      // if there was only one connection line the polys can be combined
      // firstlinecorner contains the start point of the intersection line
      // lastlinecorner contains the end point of the intersection line

      // add all corners of Poly1 to newpoly from first point of Poly1 to
      // first intersection point (firstlinecorner) and go in clock direction
      AddCornersToNewCombinedPoly(Poly1, Poly1^.FirstPoint, firstlinecorner, TRUE);

      // now add the corners of Poly 2 to the resulting poly
      AddCornersToNewCombinedPoly(Poly2, firstlinecorner, lastlinecorner, doclockdir);
      // at least the left corners of Poly1 will be added to the resulting poly
      AddCornersToNewCombinedPoly(Poly1, lastlinecorner, lastcpolycorner, TRUE);

      // now the last corner has to be added
      TmpCPolyObj.InsertPoint(lastcpolycorner^.Point.x, lastcpolycorner^.Point.y, lastcpolycorner^.hidden);
      TmpCPolyObj.InsertPoint(Poly1^.FirstPoint^.Point.x,Poly1^.FirstPoint^.Point.y, Poly1^.FirstPoint^.hidden);
      // if one of the polys contains islands add last corner as hidden point as
      // starting point for islands
      if (Poly1^.isIsland or Poly2^.isIsland) then
      begin
         TmpCPolyObj.InsertPoint(Poly1^.FirstPoint^.Point.x,Poly1^.FirstPoint^.Point.y, TRUE);
      end;
      // now check if Poly1 or Poly2 have island polys. These have to be added
      // to the resulting Poly now.
      mayaddconnectionpoint:=FALSE;
      if (Poly1^.isIsland) then
      begin
         TmpCPolyObj.AddIslandCornersToCurrCPoly(Poly1, mayaddconnectionpoint);
         mayaddconnectionpoint:=TRUE;
      end;
      if (Poly2^.isIsland) then
         TmpCPolyObj.AddIslandCornersToCurrCPoly(Poly2, mayaddconnectionpoint);

      result:=TRUE;
   end
   else
   begin
      // remove intersection points if there have been added
      if (intersectiontype = 1) then // remove from Poly1
      begin
         RemoveAddedForCombineCornersFromCPoly(Poly1);
      end;
      if (intersectiontype = 2) then // remove from Poly2
      begin
         RemoveAddedForCombineCornersFromCPoly(Poly2);
      end;
      if (intersectiontype = 3) then // remove from Poly1 and Poly2
      begin
         RemoveAddedForCombineCornersFromCPoly(Poly1);
         RemoveAddedForCombineCornersFromCPoly(Poly2);
      end;
      result:=FALSE;
   end;
end;

// function is used to check if the found flags in the FoundIdxArray contains
// one connection line and returns the index of the first and the last corner of
// this connection line
function TCPolyTools.CheckHasConnectionLine(numelems:integer; var firstcorneridx:integer; var lastcorneridx:integer):boolean;
var i:integer;
    retVal:boolean;
    numlinesfound:integer;
    numpointsfound:integer;
    startidx:integer;
    searchidx:integer;
    foundcorner:boolean;
    hadlinefound:boolean;
begin
   retVal:=FALSE;
   numlinesfound:=0;
   // first check if the last element has been found in this case
   // all elements that are found at the beginning have to be skipped first
   // these will be handeled at the end
   startidx:=1;
   if (FoundIdxArray[numelems] = TRUE) then
   begin
      for i:=1 to numelems do
      begin
         if (FoundIdxArray[i] = TRUE) then
            startidx:=startidx+1
         else
            break;
      end;
   end;

   for i:=startidx to numelems do
   begin
      if (FoundIdxArray[i] = TRUE) then
      begin
         // now search for a line
         firstcorneridx:=i;
         searchidx:=i+1;
         foundcorner:=TRUE;
         numpointsfound:=1;
         hadlinefound:=FALSE;
         while (foundcorner) do
         begin
            // go in clock direction and search for points
            if (searchidx > numelems) then
            begin
               if (FoundIdxArray[searchidx - numelems] = TRUE) then
               begin
                  numpointsfound:=numpointsfound+1;
                  lastcorneridx:=searchidx - numelems;
               end
               else
                  foundcorner:=FALSE;
            end
            else
            begin
               if (FoundIdxArray[searchidx] = TRUE) then
               begin
                  numpointsfound:=numpointsfound+1;
                  lastcorneridx:=searchidx;
               end
               else
                  foundcorner:=FALSE;
            end;
            if (numpointsfound = 2) and (foundcorner) then // a line has been found
            begin
               numlinesfound:=numlinesfound+1;
               hadlinefound:=TRUE;
            end;
            searchidx:=searchidx+1;
         end;
         if (hadlinefound) then
         begin
            // reset all found points to not found that
            // they will not be checked again
            FoundIdxArray[i]:=FALSE;
            searchidx:=i+1;
            foundcorner:=TRUE;
            while (foundcorner) do
            begin
               // go in clock direction and search for points
               if (searchidx > numelems) then
               begin
                  if (FoundIdxArray[searchidx - numelems] = TRUE) then
                     FoundIdxArray[searchidx - numelems]:=FALSE
                  else
                     foundcorner:=FALSE;
               end
               else
               begin
                  if (FoundIdxArray[searchidx] = TRUE) then
                     FoundIdxArray[searchidx]:=FALSE
                  else
                     foundcorner:=FALSE;
               end;
               searchidx:=searchidx+1;
            end;
         end;
      end;
      if numlinesfound > 1 then break;
   end;
   if (numlinesfound = 1) then retVal:=TRUE else retVal:=FALSE;
   result:=retVal;
end;

// procedure is used to rotate the Poly that the firstline corner is before the lastlinecorner
procedure TCPolyTools.RotateCombineCPoly(var aPoly:p_CPoly_record; numrotations:integer);
var rotatearray:array of p_Point_record;
    iterate:p_Point_record;
    arrayidx:integer;
    last_post:p_Point_record;
    i,j:integer;
begin
   SetLength(rotatearray, aPoly^.LastParentCornerIdx+1);
   for i:=1 to numrotations do
   begin
      // copy to array
      arrayidx:=2;
      iterate:=aPoly^.FirstPoint;
      while (arrayidx <= aPoly^.LastParentCornerIdx+1) do
      begin
         if (arrayidx <= aPoly^.LastParentCornerIdx) then
            rotatearray[arrayidx]:=iterate
         else
            rotatearray[1]:=iterate;
         iterate:=iterate^.next;
         arrayidx:=arrayidx+1;
      end;
      last_post:=iterate;
      // now copy back
      for j:=1 to aPoly^.LastParentCornerIdx do
      begin
         if j = 1 then
         begin
            aPoly^.FirstPoint:=rotatearray[j];
            iterate:=aPoly^.FirstPoint;
         end
         else
         begin
            iterate^.next:=rotatearray[j];
            iterate:=iterate^.next;
         end;
      end;
      iterate^.next:=last_post;
   end;
end;

// procedure is used to create object that is used
// for combining two CPolys
procedure TCPolyTools.PrepareForCombineCPoly;
begin
   // create temporary CPoly object
   TmpCPolyObj:=TCPoly.Create;
end;

// procedure is used to destroy object
// that was created for combining two CPolys
procedure TCPolyTools.RemoveAfterCombineCPoly;
begin
   // delete temporary CPoly object
   TmpCPolyObj.Destroy;
end;

// function is used to get pointer to CPoly that
// is the result of combining two CPolys
function TCPolyTools.GetCPolyCombineResult:p_CPoly_record;
begin
   result:=TmpCPolyObj.GetCurrent;
end;

// method is used to add corners to new created combined poly
// parentpoly  -> pointer to CPoly from where the corners should be get
// startcorner -> pointer to corner where it should be started
// endcorner   -> pointer to corner till where it should be added (exclusive)
// doclockdir  -> direction how the corners should be itereated (in clock dir or not)
procedure TCPolyTools.AddCornersToNewCombinedPoly(var parentpoly:p_CPoly_record; startcorner: p_Point_record; endcorner: p_Point_record; doclockdir:boolean);
var iterate:p_Point_record;
    found:boolean;
    corneridx:integer;
begin
   iterate:=parentpoly^.FirstPoint;
   found:=FALSE;
   corneridx:=1;
   // now find the startcorner in parentpoly
   while (iterate <> nil) and (not found) do
   begin
      if (iterate^.Point.x = startcorner^.Point.x) and
         (iterate^.Point.y = startcorner^.Point.y) then
      begin
         found:=TRUE;
      end
      else
      begin
         iterate:=iterate^.next;
         corneridx:=corneridx+1;
      end;
   end;
   // iterate is now on startcorner
   if (doclockdir = TRUE) then
   begin
      // add till end-corner in clock-direction
      while not ((iterate^.Point.x = endcorner^.Point.x) and
                 (iterate^.Point.y = endcorner^.Point.y)) do
      begin
         TmpCPolyObj.InsertPoint(iterate^.Point.x, iterate^.Point.y, iterate^.hidden);
         iterate:=iterate^.next;
         // check if the next corner is at begin of list
         if (iterate <> nil) then
         begin
            if (iterate^.Point.x = parentpoly^.FirstPoint^.Point.x) and
               (iterate^.Point.y = parentpoly^.FirstPoint^.Point.y) then
            begin
               iterate:=parentpoly^.FirstPoint;
            end;
         end;
      end;
   end
   else
   begin
      // the corners have to be added not clockwise
      while not ((iterate^.Point.x = endcorner^.Point.x) and
                 (iterate^.Point.y = endcorner^.Point.y)) do
      begin
         TmpCPolyObj.InsertPoint(iterate^.Point.x, iterate^.Point.y, iterate^.hidden);
         corneridx:=corneridx-1;
         iterate:=TmpCPolyObj.GetCPolyCornerByIdx(parentpoly, corneridx);
      end;
   end;
end;

// method is used to remove the corners that have been inserted
// due to the insertion of intersection nodes
procedure TCPolyTools.RemoveAddedForCombineCornersFromCPoly(var Poly:p_CPoly_record);
var iterate1:p_Point_record;
    iterate2:p_Point_record;
    docheck:boolean;
begin
   docheck:=TRUE;
   iterate1:=Poly^.FirstPoint;
   iterate2:=Poly^.FirstPoint;
   while (iterate1^.next <> nil) and (docheck) do
   begin
      iterate2:=iterate1;
      iterate1:=iterate1^.next;
      if (iterate1^.hidden) then docheck:=FALSE;
      if (docheck) then
      begin
         if (iterate1^.addedforcombine) then
         begin
            iterate2^.next:=iterate1^.next;
            dispose(iterate1);
            iterate1:=iterate2;
            Poly^.numcorners:=Poly^.numcorners-1;
            Poly^.LastParentCornerIdx:=Poly^.LastParentCornerIdx-1;
         end
         else
            if iterate1^.next <> nil then
               iterate1:=iterate1^.next;
      end;
   end;
end;
end.
