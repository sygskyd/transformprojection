unit GisAnalyzeDef;

interface

const
       // Object types
       ot_Pixel     = 1;
       ot_Poly      = 2;
       ot_CPoly     = 4;
       ot_Text      = 7;
       ot_Symbol    = 8;
       ot_Spline    = 9;
       ot_Image     = 10;
       ot_MesLine   = 11;
       ot_Circle    = 12;
       ot_Arc       = 13;

             //TextSTyles
       ts_Left           =  0;
       ts_Center         =  1;
       ts_Right          =  2;
       ts_Italic         =  4;
       ts_Bold           =  8;
       ts_Underl         = 16;
       ts_FixHeight      = 32;
       ts_Transparent    = 64;

       //LineTypes
       lt_Solid     = 0;
       lt_Dash      = 1;
       lt_Dot       = 2;
       lt_DashDot   = 3;
       lt_DashDDot  = 4;
       lt_UserDefined    = 1000;

       //FillTypes
       pt_NoPattern = 0;
       pt_FDiagonal = 1;
       pt_Cross     = 2;
       pt_DiagCross = 3;
       pt_BDiagonal = 4;
       pt_Horizontal= 5;
       pt_Vertical  = 6;
       pt_Solid     = 7;
       pt_UserDefined = 8;

       stDrawingUnits    = 0;
       stScaleInDependend= 1;
       stProjectScale    = 2;
       stPercent         = 3;

type
  // structure for point
  pcpoint = record
      x     :double;
      y     :double;
  end;

  p_Point_record=^Point_record;
  Point_record = record
      Point:pcpoint;
      hidden:boolean;
      addedforcombine:boolean;
      next :p_Point_record;
  end;
  p_LinkedCPoly_record = ^LinkedCPoly_record;
  p_CPoly_record =^CPoly_record;
  CPoly_record = record
      Rect_LU   :pcpoint;        // left upper corner of rectangle
      Rect_RB   :pcpoint;        // right bottom corner of rectangle

      FirstPoint:p_Point_record; // pointer to first point element in list
      LastPoint :p_Point_record; // pointer to last point element in list
      isIsland  :boolean;        // flag that defines if CPoly contains islands

      numcorners:integer;          // counts number of corners of CPoly
      LastParentCornerIdx:integer; // sets the index of the last corner of the parent CPoly

      Islands   :p_LinkedCPoly_record; // list of CPolys that belong to this CPoly (for island calculation)
      next      :p_CPoly_record;
  end;

  LinkedCPoly_record = record
     LinkedCPoly   :p_CPoly_record;
     next          :p_LinkedCPoly_record;
  end;

implementation

end.
