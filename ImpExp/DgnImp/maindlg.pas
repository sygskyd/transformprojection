unit Maindlg;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Strfile, StdCtrls, Buttons, ExtCtrls, Gauges, Files, Db,
  DBTables, SetDgnFileDlg, Math, windows, OleCtrls, AxGisPro_TLB,
  AXImpExpDbcXControl_TLB, AXDLL_TLB, WinOSInfo;

const
      //registration modes
      NO_LICENCE        = 0;
      FULL_LICENCE      = 1;
      DEMO_LICENCE      = 2;

      // number of objects that can be imported in demo-mode
      MaxObjectsInDemoMode = 100;

      // Object types
      ot_Pixel     = 1;
      ot_Poly      = 2;
      ot_CPoly     = 4;
      ot_Text      = 7;
      ot_Symbol    = 8;
      ot_Spline    = 9;
      ot_Image     = 10;
      ot_MesLine   = 11;
      ot_Circle    = 12;
      ot_Arc       = 13;

            //TextSTyles
      ts_Left           =  0;
      ts_Center         =  1;
      ts_Right          =  2;
      ts_Italic         =  4;
      ts_Bold           =  8;
      ts_Underl         = 16;
      ts_FixHeight      = 32;
      ts_Transparent    = 64;

      //LineTypes
      lt_Solid     = 0;
      lt_Dash      = 1;
      lt_Dot       = 2;
      lt_DashDot   = 3;
      lt_DashDDot  = 4;
      lt_UserDefined    = 1000;

      //FillTypes
      pt_NoPattern = 0;
      pt_FDiagonal = 1;
      pt_Cross     = 2;
      pt_DiagCross = 3;
      pt_BDiagonal = 4;
      pt_Horizontal= 5;
      pt_Vertical  = 6;
      pt_Solid     = 7;
      pt_UserDefined = 8;

      stDrawingUnits    = 0;
      stScaleInDependend= 1;
      stProjectScale    = 2;
      stPercent         = 3;

type
  TMainWnd = class(TForm)
    Label1: TLabel;
    CloseTimer: TTimer;
    Panel1: TPanel;
    Label2: TLabel;
    Prozessbalken: TGauge;
    StatusLB: TListBox;
    SaveReportBtn: TButton;
    SaveDialogRep: TSaveDialog;
    OK: TButton;
    Cancel: TButton;
    SetupWindowTimer: TTimer;
    AxGisProjection: TAxGisProjection;
    AXImpExpDbc: TAXImpExpDbc;
    procedure FormCreate(Sender: TObject);
    procedure StartBtnClick(Sender: TObject);
    procedure CloseTimerTimer(Sender: TObject);
    procedure CancelClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SaveReportBtnClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private-Deklarationen }
    StrFileObj :ClassStrfile;
    FileObj    :ClassFile;
    Pass       :integer;
    DGNFilename :string;
    Bytes2work  :longint;
    Bytesworked :longint;

    ObjectCount :longint;

    Aktiv       :boolean;
    Abort       :boolean;
    FirstActivate:boolean;

    LastMessage:string;
    LastMessageCnt:integer;

    ALayer      :ILayer;
    APolyline   :IPolyline;
    APolygon    :IPolygon;
    ASymboldef  :ISymbolDef;
    ASpline     :ISpline;

    AIslandList :IList;

    MayCreateSymbol:boolean;

    SymbolDefXOffset:double;
    SymbolDefYOffset:double;

    procedure RunPass(AktPass:integer);
    procedure ExecutePass;
    procedure DisplayDemoWarning;
    procedure SetToLayerPriorities(var aObject:IBase);
    procedure StartImport;
  public
    { Public-Deklarationen }
    App           :Variant;
    DestDocument  :IDocument;

    RegMode       :integer;
    LngCode       :string;

    ExitNormal:boolean;
    LastError :integer;

    GisHandle :integer;

    { Procedures that are used to handle AX-Control }
    procedure CreateLayer(Layer:string);
    function  CreatePoint(X:double;Y:double;rgbvalue:longint;LineType:longint;issymbol:boolean):integer;
    function  CheckLayerExists(Layer:string):boolean;
    procedure CreatePolyline(rgbvalue:longint;LineType:longint;issymbol:boolean);
    procedure CreateSpline(rgbvalue:longint;LineType:longint;issymbol:boolean);
    procedure InsertPointToPolyLine(X:double; Y:double;issymbol:boolean);
    procedure InsertPointToSpline(X:double; Y:double;issymbol:boolean);
    function  ClosePolyline(issymbol:boolean):integer;
    function  CloseSpline(issymbol:boolean):integer;
    procedure CreatePolygon(LineRgbValue:longint; LineType:longint; FillRgbValue:longint; FillType:longint;issymbol:boolean);
    procedure InsertPointToPolygon(X:double; Y:double;issymbol:boolean);
    function  ClosePolygon(issymbol:boolean):integer;
    procedure SetLayerPriorities(Linienstil:longint;Linienfarbe:longint;Flaechenstil:longint;Flaechenfarbe:longint);
    function  CreateText(X:double;Y:double; aText:string; aFont:string; aHeight:double; aAngle:double; disptype:integer; LineRgbValue:longint; LineType:longint; FillRgbValue:longint; FillType:longint;issymbol:boolean):integer;
    function  CreateCircle(X:double;Y:double;Radius:double; LineRgbValue:longint; LineType:longint; FillRgbValue:longint; FillType:longint):integer;
    function  CreateArc(X:double; Y:double; PrimAxis:double; SecAxis:double; aAngle:double; BeginAngle:double; EndAngle:double; LineRgbValue:longint; LineType:longint; FillRgbValue:longint; FillType:longint;issymbol:boolean):integer;

    procedure CreateSymbolDef(Symname:string;X:double;Y:double);
    procedure EndSymbolDef;

    function InsertSymbol(X:double;Y:double;Symname:string;Size:double;Angle:double;LineRgbValue:longint; LineType:longint; FillRgbValue:longint; FillType:longint):integer;

    procedure CreateIslandList;
    procedure ClosePolyAndAddToIsland;
    function CloseIsland:integer;

    procedure ShowProcent(Wert:longint);
    procedure HandleError;

    procedure SetProjectionSettings;
    procedure CheckProjectionSettings;

    procedure CreateStringFile;
    procedure ShowMessage(aMessage:string);
  end;

var
  MainWnd: TMainWnd;

implementation

uses comobj;
{$R *.DFM}

procedure TMainWnd.CreateStringFile;
begin
   StrFileObj:=ClassStrfile.Create;
   StrFileObj.CreateList(OSInfo.WingisDir,LngCode,'DGNIMP');
   Self.Caption:=StrFileObj.Assign(1);          // INTERGRAPH DGN-Import 2000
   Label2.Caption:=StrFileObj.Assign(2);        // Processmessages
   SaveReportBtn.Caption:=StrFileObj.Assign(3); // save report
   Ok.Caption:=StrFileObj.Assign(4);            // Ok
   Cancel.Caption:=StrFileObj.Assign(5);        // Cancel
end;

procedure TMainWnd.FormCreate(Sender: TObject);
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   FirstActivate:=true;
   Self.Height:=1;
   Self.Width:=1;
   Self.Update;

   Aktiv:=false;
   ObjectCount:=0;
   LastMessage:='';
   LastMessageCnt:=0;

   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

procedure TMainWnd.CheckProjectionSettings;
var SourceProjection:integer;
    hstr:string;
    different:boolean;
begin
   different:=false;
   // function is used to check if the settings in the
   if (AxGisProjection.TargetCoordinateSystem <> DestDocument.Projection) then
      different:=TRUE;

   if (AxGisProjection.TargetXOffset <> DestDocument.ProjectionXOffset) then
      different:=TRUE;

   if (AxGisProjection.TargetYOffset <> DestDocument.ProjectionYOffset) then
      different:=TRUE;

   if (AxGisProjection.TargetScale <> DestDocument.ProjectionScale) then
      different:=TRUE;

   // check if the projections are different
   if DestDocument.ProjectProjSettings <> AxGisProjection.TargetProjSettings then
      different:=TRUE;

   if DestDocument.ProjectProjection <> AxGisProjection.TargetProjection then
      different:=TRUE;

   if DestDocument.Projectdate <> AxGisProjection.TargetDate then
      different:=TRUE;

   if different then
   begin
      Showmessage(StrfileObj.Assign(48)); // Warning: Projection does not fit to project projection
      AxGisProjection.SetupByDialog;
   end;
   // the used output coordinate-system is equal to the
   // target-coordinate-system
   AxGisProjection.UsedInputCoordSystem:=AxGisProjection.SourceCoordinateSystem;
end;

procedure TMainWnd.SetProjectionSettings;
begin
   // procedure is used to set the projection settings
   // of the current project to the GeoWnd
   AxGisProjection.SourceCoordinateSystem:=cs_Carthesic;  // Intergraph information normaly is in carthesic
   AxGisProjection.TargetCoordinateSystem:=DestDocument.Projection;
   AxGisProjection.SourceProjSettings:=DestDocument.ProjectProjSettings;
   AxGisProjection.TargetProjSettings:=DestDocument.ProjectProjSettings;
   AxGisProjection.SourceProjection:=DestDocument.ProjectProjection;
   AxGisProjection.TargetProjection:=DestDocument.ProjectProjection;
   AxGisProjection.SourceDate:=DestDocument.Projectdate;
   AxGisProjection.TargetDate:=DestDocument.Projectdate;
   AxGisProjection.SourceXOffset:=DestDocument.ProjectionXOffset;
   AxGisProjection.TargetXOffset:=DestDocument.ProjectionXOffset;
   AxGisProjection.SourceYOffset:=DestDocument.ProjectionYOffset;
   AxGisProjection.TargetYOffset:=DestDocument.ProjectionYOffset;
   AxGisProjection.SourceScale:=DestDocument.ProjectionScale;
   AxGisProjection.TargetScale:=DestDocument.ProjectionScale;
   // if source and target is .amp - file
   // the used input coordinates and used output coordinates
   // for the projection calculation are in carthesic
   AxGisProjection.UsedInputCoordSystem:=AxGisProjection.SourceCoordinateSystem;
   AxGisProjection.UsedOutputCoordSystem:=cs_Carthesic;
end;

procedure TMainWnd.HandleError;
begin
   if LastError <> 0 then
   begin
      if LastError = 1 then // Error: Wrong dgn-name
         StatusLB.items.Add(StrfileObj.Assign(26));
      if LastError = 2 then // Error: Invalid parameters
         StatusLB.items.Add(StrfileObj.Assign(27));
      if LastError = 3 then // Error: Cannot open file
         StatusLB.items.Add(StrfileObj.Assign(28));
      if LastError = 4 then // Error: Cannot access file
         StatusLB.items.Add(StrfileObj.Assign(29));
      if LastError = 5 then // Error: file to big
         StatusLB.items.Add(StrfileObj.Assign(30));
      if LastError = 6 then // Error: File already opened
         StatusLB.items.Add(StrfileObj.Assign(31));
      if LastError = 7 then // Error: No Filename set
         StatusLB.items.Add(StrfileObj.Assign(32));
      if LastError = 8 then // Error: File not opened
         StatusLB.items.Add(StrfileObj.Assign(33));
      if LastError = 9 then // Error: can�t map file
         StatusLB.items.Add(StrfileObj.Assign(34));
      if LastError =10 then // Error: can�t map view of file
         StatusLB.items.Add(StrfileObj.Assign(35));
      if LastError =11 then // Error: bad file structure
         StatusLB.items.Add(StrfileObj.Assign(36));
      if LastError =12 then // Error: end of dgn file
         StatusLB.items.Add(StrfileObj.Assign(37));
      if LastError =13 then // Error: no memory
         StatusLB.items.Add(StrfileObj.Assign(38));
      if LastError =14 then // Error: can�t read element
         StatusLB.items.Add(StrfileObj.Assign(39));
      if LastError = 15 then // Error: Bad complex info
         StatusLB.items.Add(StrfileObj.Assign(49));
      if LastError = 16 then // Error: Bad complex shared
         StatusLB.items.Add(StrfileObj.Assign(50));
      if LastError = 4096 then // Error: Internal error in DgnLayer.dll
         StatusLB.items.Add(StrfileObj.Assign(51));

      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      FileObj.CloseallFiles;
      aktiv:=false;
      Abort:=true;
      FileObj.Destroy;
      Cancel.Enabled:=false;
      Ok.Enabled:=true;
      SaveReportBtn.Enabled:=true;
   end;
end;

procedure TMainWnd.StartBtnClick(Sender: TObject);
begin
   RunPass(1);
end;

procedure TMainWnd.RunPass(AktPass:integer);
var Ende:boolean;
    Layer:string;
begin
   if AktPass = 1 then // reading dgn-file
   begin
      if not SetDgnFileWnd.GetFilename(Dgnfilename) then
      begin
         aktiv:=false;
         Abort:=true;
         Cancel.Enabled:=false;
         Ok.Enabled:=true;
         SaveReportBtn.Enabled:=true;
         exit;
      end;
      FileObj:=ClassFile.Create;
      Aktiv:=true;
      StatusLB.Items.Add(StrfileObj.Assign(13) + ExtractFilename(DgnFilename)); // Dgn file:

      // display database information
      StatusLb.Items.Add(AXImpExpDbc.ImpDbInfo);

      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      // now the projection will be checked
      CheckProjectionSettings;

      FileObj.SetDgnFilename(DgnFilename); if Abort then exit;
      Bytes2Work:=FileObj.GetNrOfObjectsToRead; Bytesworked:=0; if Abort then exit;
      FileObj.OpenFile; if Abort then exit;
      if Bytes2Work > 0 then
      begin
         StatusLB.Items.Add(StrfileObj.Assign(20)); // read data from DGN-File
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         Pass:=1;
         ExecutePass;
         exit;
      end
      else
         Runpass(2);
      exit;
   end;

   if AktPass = 2 then // importing polyline information
   begin
      // first the symbol library has to be read out and created
      FileObj.ReadSymbolLibrary;

      Bytes2work:=FileObj.GetNrOfPolylinesToRead;
      Bytesworked:=0;
      if Abort then exit;
      if Bytes2work > 0 then
      begin
         StatusLB.Items.Add(StrfileObj.Assign(40)); // importing Polylines into Gis
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         Pass:=2;
         ExecutePass;
         exit;
      end
      else
         Runpass(3);
      exit;
   end;

   if AktPass = 3 then // importing polygon information
   begin
      Bytes2work:=FileObj.GetNrOfPolygonsToRead; Bytesworked:=0; if Abort then exit;
      if Bytes2work > 0 then
      begin
         StatusLB.Items.Add(StrfileObj.Assign(41)); // importing Polygons into Gis
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         Pass:=3;
         ExecutePass;
         exit;
      end
      else
         Runpass(4);
      exit;
   end;

   if AktPass = 4 then // importing point information
   begin
      Bytes2work:=FileObj.GetNrOfPointsToRead; Bytesworked:=0; if Abort then exit;
      if Bytes2work > 0 then
      begin
         StatusLB.Items.Add(StrfileObj.Assign(42)); // importing Points into Gis
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         Pass:=4;
         ExecutePass;
         exit;
      end
      else
         Runpass(5);
      exit;
   end;

   if AktPass = 5 then // importing circle information
   begin
      Bytes2work:=FileObj.GetNrOfCirclesToRead; Bytesworked:=0; if Abort then exit;
      if Bytes2work > 0 then
      begin
         StatusLB.Items.Add(StrfileObj.Assign(43)); // importing Circles into Gis
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         Pass:=5;
         ExecutePass;
         exit;
      end
      else
         Runpass(6);
      exit;
   end;

   if AktPass = 6 then // importing text information
   begin
      Bytes2work:=FileObj.GetNrOfTextsToRead; Bytesworked:=0; if Abort then exit;
      if Bytes2work > 0 then
      begin
         StatusLB.Items.Add(StrfileObj.Assign(44)); // importing Texts into Gis
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         Pass:=6;
         ExecutePass;
         exit;
      end
      else
         Runpass(7);
      exit;
   end;

   if AktPass = 7 then // importing symbol information
   begin
      Bytes2work:=FileObj.GetNrOfSymbolRefsToRead; Bytesworked:=0; if Abort then exit;
      if Bytes2work > 0 then
      begin
         StatusLB.Items.Add(StrfileObj.Assign(47)); // importing symbols into Gis
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         Pass:=7;
         ExecutePass;
         exit;
      end
      else
         Runpass(8);
      exit;
   end;

   if AktPass = 8 then // importing Arc information
   begin
      Bytes2work:=FileObj.GetNrOfArcsToRead; Bytesworked:=0; if Abort then exit;
      // ignore arcs due to problems in generate graphics
      // Bytes2work:=0;
      if Bytes2work > 0 then
      begin
         StatusLB.Items.Add(StrfileObj.Assign(52)); // importing Arcs into Gis
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         Pass:=8;
         ExecutePass;
         exit;
      end
      else
         Runpass(9);
      exit;
   end;

   if AktPass = 9 then // DGN-file imported
   begin
      StatusLB.Items.Add(StrfileObj.Assign(21)); //  File correct imported!
      StatusLB.Items.Add('------------------------------------------');
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      FileObj.CloseallFiles;
      aktiv:=false;
      FileObj.Destroy;
      RunPass(1); // convert next file
   end;
end;

procedure TMainWnd.ShowProcent(Wert:longint);
var Prozent:integer;
begin
   if Bytes2work > 0 then Prozent:= Round (((Bytesworked + Wert) / Bytes2work) * 100 ) else Prozent:= 100;
   ProzessBalken.Progress :=Prozent;
   ProzessBalken.Update;
end;

procedure TMainWnd.ExecutePass;
var Prozent:integer;
begin
   if Pass = 1 then // reading data out of dgnfile
   begin
      if Bytes2work > 0 then
      repeat
         Bytesworked:=FileObj.ReadDgnElement;
         // display process status
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then
      begin
         RunPass(2);
      end;
      if Abort = true then exit;
      exit;
   end;
   if Pass = 2 then // importing polyline information
   begin
      if Bytes2work > 0 then
      repeat
         Bytesworked:=Bytesworked + FileObj.ImportPolyline(FALSE);
         // display process status
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then
      begin
         RunPass(3);
      end;
      if Abort = true then exit;
      exit;
   end;
   if Pass = 3 then // importing polygon information
   begin
      if Bytes2work > 0 then
      repeat
         Bytesworked:=Bytesworked + FileObj.ImportPolygon(FALSE);
         // display process status
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then
      begin
         RunPass(4);
      end;
      if Abort = true then exit;
      exit;
   end;
   if Pass = 4 then // importing point information
   begin
      if Bytes2work > 0 then
      repeat
         Bytesworked:=Bytesworked + FileObj.ImportPoint(FALSE);
         // display process status
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then
      begin
         RunPass(5);
      end;
      if Abort = true then exit;
      exit;
   end;
   if Pass = 5 then // importing circle information
   begin
      if Bytes2work > 0 then
      repeat
         Bytesworked:=Bytesworked + FileObj.ImportCircle;
         // display process status
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then
      begin
         RunPass(6);
      end;
      if Abort = true then exit;
      exit;
   end;

   if Pass = 6 then // importing text information
   begin
      if Bytes2work > 0 then
      repeat
         Bytesworked:=Bytesworked + FileObj.ImportText(FALSE);
         // display process status
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then
      begin
         RunPass(7);
      end;
      if Abort = true then exit;
      exit;
   end;

   if Pass = 7 then // importing symbol information
   begin
      if Bytes2work > 0 then
      repeat
         Bytesworked:=Bytesworked + FileObj.ImportSymbol;
         // display process status
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then
      begin
         RunPass(8);
      end;
      if Abort = true then exit;
      exit;
   end;

   if Pass = 8 then // importing arc information
   begin
      if Bytes2work > 0 then
      repeat
         Bytesworked:=Bytesworked + FileObj.ImportArc(FALSE);
         // display process status
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then
      begin
         RunPass(9);
      end;
      if Abort = true then exit;
      exit;
   end;
end;

procedure TMainWnd.CreateLayer(Layer:string);
begin
   ALayer:=DestDocument.Layers.InsertLayerByName(Layer,True);
end;

function TMainWnd.CheckLayerExists(Layer:string):boolean;
var dispatch:IDispatch;
    retVal:boolean;
begin
   dispatch:=DestDocument.Layers.GetLayerByName(Layer);
   if dispatch <> nil then retVal:=true else retVal:=false;
   if retVal then
      ALayer:=DestDocument.Layers.GetLayerByName(Layer);
   result:=retVal;
end;

procedure TMainWnd.DisplayDemoWarning;
begin
   StatusLB.Items.Add(inttostr(MaxObjectsInDemoMode)+ ' ' + StrfileObj.Assign(22)); // Objects created - following objects will be skiped.
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;
end;

procedure TMainWnd.SetToLayerPriorities(var aObject:IBase);
var aObjectStyle:IObjectStyle;
    aPixel  :IPixel;
    aPoly   :IPolyline;
    aCPoly  :IPolygon;
    aSymbol :ISymbol;
    mySpline:ISpline;
    aArc    :IArc;
    aText   :IText;
    aCircle :ICircle;
begin
        if aObject.ObjectType = ot_Pixel    then begin aPixel:=aObject as IPixel; aPixel.CreateObjectStyle; aObjectStyle:=aPixel.ObjectStyle;     end
   else if aObject.ObjectType = ot_Poly     then begin aPoly:=aObject as IPolyline; aPoly.CreateObjectStyle; aObjectStyle:=aPoly.ObjectStyle;     end
   else if aObject.ObjectType = ot_CPoly    then begin aCPoly:=aObject as IPolygon; aCPoly.CreateObjectStyle; aObjectStyle:=aCPoly.ObjectStyle;   end
   else if aObject.ObjectType = ot_Symbol   then begin aSymbol:=aObject as ISymbol; aSymbol.CreateObjectStyle; aObjectStyle:=aSymbol.ObjectStyle; end
   else if aObject.ObjectType = ot_Spline   then begin mySpline:=aObject as ISpline; mySpline.CreateObjectStyle; aObjectStyle:=mySpline.ObjectStyle; end
   else if aObject.ObjectType = ot_Arc      then begin aArc:=aObject as IArc; aArc.CreateObjectStyle; aObjectStyle:=aArc.ObjectStyle;             end
   else if aObject.ObjectType = ot_Text     then begin aText:=aObject as IText; aText.CreateObjectStyle; aObjectStyle:=aText.ObjectStyle;         end
   else if aObject.ObjectType = ot_Circle   then begin aCircle:=aObject as ICircle; aCircle.CreateObjectStyle; aObjectStyle:=aCircle.ObjectStyle; end
   else exit;


   aObjectStyle.Linestyle.Style    :=ALayer.Linestyle.Style;
   aObjectStyle.LineStyle.Color    :=ALayer.Linestyle.Color;
   aObjectStyle.LineStyle.FillColor:=ALayer.Linestyle.FillColor;

   aObjectStyle.Fillstyle.Pattern  :=ALayer.Fillstyle.Pattern;
   aObjectStyle.Fillstyle.ForeColor:=ALayer.Fillstyle.ForeColor;
   aObjectStyle.Fillstyle.BackColor:=ALayer.Fillstyle.BackColor;
end;

procedure TMainWnd.CreatePolyline(rgbvalue:longint;LineType:longint;issymbol:boolean);
var aBase:IBase;
begin
   if issymbol and not MayCreateSymbol then
   begin
      exit;
   end;

   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = MaxObjectsInDemoMode then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > MaxObjectsInDemoMode then
         exit;
   end;
   APolyline:=DestDocument.CreatePolyline;

   if not issymbol then
   begin
      aBase:=APolyline;
      SetToLayerPriorities(aBase);

      // set color
      APolyline.ObjectStyle.LineStyle.Color:=rgbvalue;
      APolyline.ObjectStyle.LineStyle.FillColor:=rgbvalue;

      // set linetype
      APolyline.ObjectStyle.LineStyle.Style:=LineType;
   end;
end;

procedure TMainWnd.CreateSpline(rgbvalue:longint;LineType:longint;issymbol:boolean);
var ABase:IBase;
begin
   if issymbol and not MayCreateSymbol then
   begin
      exit;
   end;

   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = MaxObjectsInDemoMode then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > MaxObjectsInDemoMode then
         exit;
   end;

   ASpline:=DestDocument.CreateSpline;

   if not issymbol then
   begin
      ABase:=ASpline;
      SetToLayerPriorities(ABase);

      // set color
      ASpline.ObjectStyle.LineStyle.Color:=rgbvalue;
      ASpline.ObjectStyle.LineStyle.FillColor:=rgbvalue;

      // set linetype
      ASpline.ObjectStyle.LineStyle.Style:=LineType;
   end;
end;

procedure TMainWnd.InsertPointToSpline(X:double; Y:double;issymbol:boolean);
var APoint:IPoint;
begin
   if issymbol and not MayCreateSymbol then
      exit;
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > MaxObjectsInDemoMode then
         exit;
   end;

   if not issymbol then
      AxGisProjection.Calculate(X,Y)
   else
   begin
      X:=X - SymbolDefXOffset;
      Y:=Y - SymbolDefYOffset;
   end;
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   ASpline.InsertPoint(APoint);
end;

procedure TMainWnd.InsertPointToPolyLine(X:double; Y:double;issymbol:boolean);
var APoint:IPoint;
begin
   if issymbol and not MayCreateSymbol then
      exit;
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > MaxObjectsInDemoMode then
         exit;
   end;

   if not issymbol then
      AxGisProjection.Calculate(X,Y)
   else
   begin
      X:=X - SymbolDefXOffset;
      Y:=Y - SymbolDefYOffset;
   end;
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   APolyLine.InsertPoint(APoint);
end;

function TMainWnd.CloseIsland:integer;
var AIndex:integer;
    IslandPoly:IPolygon;
begin
   IslandPoly:=DestDocument.CreateIslandArea(AIslandList);
   ALayer.InsertObject(IslandPoly);
   AIndex:=IslandPoly.Index;
   result:=AIndex;
end;

function TMainWnd.ClosePolyline(issymbol:boolean):integer;
var AIndex:integer;
begin
   if issymbol and not MayCreateSymbol then
   begin
      result:=0;
      exit;
   end;
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > MaxObjectsInDemoMode then
         exit;
   end;
   if not issymbol then
      ALayer.InsertObject(APolyline)
   else
      ASymboldef.InsertSymbolItem(APolyline);

   AIndex:=APolyline.Index;
   result:=AIndex;
end;

function TMainWnd.CloseSpline(issymbol:boolean):integer;
var AIndex:integer;
begin
   if issymbol and not MayCreateSymbol then
   begin
      result:=0;
      exit;
   end;
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > MaxObjectsInDemoMode then
         exit;
   end;
   if not issymbol then
      ALayer.InsertObject(ASpline)
   else
      ASymboldef.InsertSymbolItem(ASpline);

   AIndex:=ASpline.Index;
   result:=AIndex;
end;

procedure TMainWnd.CreateIslandList;
begin
   AIslandList:=DestDocument.CreateList;
end;

procedure TMainWnd.CreatePolygon(LineRgbValue:longint; LineType:longint; FillRgbValue:longint; FillType:longint;issymbol:boolean);
var ABase:IBase;
begin
   if issymbol and not MayCreateSymbol then
      exit;

   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = MaxObjectsInDemoMode then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > MaxObjectsInDemoMode then
         exit;
   end;
   APolygon:=DestDocument.CreatePolygon;

   if not issymbol then
   begin
      ABase:=APolygon;
      SetToLayerPriorities(ABase);

      // set line color
      APolygon.ObjectStyle.LineStyle.Color:=LineRgbValue;
      APolygon.ObjectStyle.LineStyle.FillColor:=LineRgbValue;

      // set linetype
      APolygon.ObjectStyle.LineStyle.Style:=LineType;

      // set fill type
      APolygon.ObjectStyle.Fillstyle.Pattern  :=FillType;

      // set fill style
      APolygon.ObjectStyle.Fillstyle.ForeColor:=FillRgbValue;
   end;
end;

procedure TMainWnd.InsertPointToPolygon(X:double; Y:double;issymbol:boolean);
var APoint:IPoint;
begin
   if issymbol and not MayCreateSymbol then
      exit;
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > MaxObjectsInDemoMode then
         exit;
   end;
   if not issymbol then
      AxGisProjection.Calculate(X,Y)
   else
   begin
      X:=X - SymbolDefXOffset;
      Y:=Y - SymbolDefYOffset;
   end;
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   APolygon.InsertPoint(APoint);
end;

procedure TMainWnd.ClosePolyAndAddToIsland;
begin
   // close polygon of island and add to list
   APolygon.ClosePoly;
   AIslandList.Add(APolygon);
end;

function TMainWnd.ClosePolygon(issymbol:boolean):integer;
var AIndex:integer;
begin
   if issymbol and not MaycreateSymbol then
   begin
      result:=0;
      exit;
   end;
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > MaxObjectsInDemoMode then
         exit;
   end;
   APolygon.ClosePoly;
   if not issymbol then
      ALayer.InsertObject(APolygon)
   else
      ASymboldef.InsertSymbolItem(APolygon);

   AIndex:=APolygon.Index;
   result:=AIndex;
end;

function TMainWnd.CreatePoint(X:double;Y:double;rgbvalue:longint;LineType:longint;issymbol:boolean):integer;
var APixel:IPixel;
    APoint:IPoint;
    ABase :IBase;
    AIndex:integer;
begin
   if issymbol and not MayCreateSymbol then
   begin
      result:=0;
      exit;
   end;

   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = MaxObjectsInDemoMode then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > MaxObjectsInDemoMode then
         exit;
   end;
   if not issymbol then
      AxGisProjection.Calculate(X,Y)
   else
   begin
      X:=X - SymbolDefXOffset;
      Y:=Y - SymbolDefYOffset;
   end;
   APixel:=DestDocument.CreatePixel;
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   APixel.Position:=APoint;

   if not issymbol then
   begin
      ABase:=APixel;
      SetToLayerPriorities(ABase);

      // set color
      APixel.ObjectStyle.LineStyle.Color:=rgbvalue;
      APixel.ObjectStyle.LineStyle.FillColor:=rgbvalue;

      // set linetype
      APixel.ObjectStyle.LineStyle.Style:=LineType;
   end;

   if not issymbol then
      ALayer.InsertObject(APixel)
   else
      ASymboldef.InsertSymbolItem(APixel);

   AIndex:=APixel.Index;
   result:=AIndex;
end;

procedure TMainWnd.SetLayerPriorities(Linienstil:longint;Linienfarbe:longint;Flaechenstil:longint;Flaechenfarbe:longint);
begin
   ALayer.Linestyle.Style:=Linienstil;
   ALayer.Linestyle.Color:=Linienfarbe;
   ALayer.Linestyle.FillColor:=Linienfarbe;

   ALayer.Fillstyle.Pattern:=Flaechenstil;
   ALayer.Fillstyle.ForeColor:=Flaechenfarbe;
   ALayer.Fillstyle.BackColor:=Flaechenfarbe;
end;

function TMainWnd.InsertSymbol(X:double;Y:double;Symname:string;Size:double;Angle:double;LineRgbValue:longint; LineType:longint; FillRgbValue:longint; FillType:longint):integer;
var ASymbol:ISymbol;
    APoint :IPoint;
    MySymboldef:ISymbolDef;
    i:integer;
    gefunden:boolean;
    Symnr:integer;
    new_id:integer;
    SymSize_int:integer;
    SymSizeType:integer;
    ABase:IBase;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = MaxObjectsInDemoMode then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > MaxObjectsInDemoMode then
         exit;
   end;

   { Now the Symbolnr has to be evaluated }
   gefunden:=false;
   for i:=0 to DestDocument.Symbols.Count-1 do
   begin
      MySymboldef:=DestDocument.Symbols.Items[i];
      if MySymboldef.Name = Symname then
      begin
         Symnr:=MySymboldef.Index;
         SymSize_int:=round(MySymbolDef.DefaultSize);
         SymSizeType:=MySymbolDef.DefaultSizeType;
         SymSize_int:=round(SymSize_int * Size);
         gefunden:=true;
         break;
      end;
   end;
   if (gefunden) then
   begin
      //do the projection
      AxGisProjection.Calculate(X,Y);
      ASymbol:=DestDocument.CreateSymbol;
      ASymbol.Symbolname:=Symname;
      ASymbol.Position.X:=round(X*100);
      ASymbol.Position.Y:=round(Y*100);
      ASymbol.SizeType:=SymSizeType;
      ASymbol.Size:=SymSize_int;
      ASymbol.Angle:=Angle * PI / 180;
      ABase:=ASymbol;
      SetToLayerPriorities(ABase);

      // set line color
      ASymbol.ObjectStyle.LineStyle.Color:=LineRgbValue;
      ASymbol.ObjectStyle.LineStyle.FillColor:=LineRgbValue;

      // set linetype
      ASymbol.ObjectStyle.LineStyle.Style:=LineType;

      // set fill type
      ASymbol.ObjectStyle.Fillstyle.Pattern  :=FillType;

      // set fill style
      ASymbol.ObjectStyle.Fillstyle.ForeColor:=FillRgbValue;

      ALayer.InsertObject(ASymbol);
      new_id:=ASymbol.Index;
   end
   else
   begin
      new_id:=CreatePoint(X,Y,0,0,FALSE);
   end;
   result:=new_id;
end;

procedure TMainWnd.ShowMessage(aMessage:string);
begin
   if (LastMessage = aMessage) then
   begin
      LastMessageCnt:=LastMessageCnt+1;
      StatusLb.Items[StatusLb.Items.Count-1]:=aMessage+' '+inttostr(LastMessageCnt)+'X';
   end
   else
   begin
      LastMessage:=aMessage;
      LastMessageCnt:=1;
      StatusLB.Items.Add(aMessage);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
   end;
end;

function TMainWnd.CreateText(X:double;Y:double; aText:string; aFont:string; aHeight:double; aAngle:double; disptype:integer;LineRgbValue:longint; LineType:longint; FillRgbValue:longint; FillType:longint;issymbol:boolean):integer;
var newText:IText;
    AIndex:integer;
    hX,hY:double;
    hAngle:integer;
    i:integer;
    fontexists:boolean;
    ABase:IBase;
begin
   if issymbol and not MayCreateSymbol then
   begin
      result:=0;
      exit;
   end;

   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = MaxObjectsInDemoMode then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > MaxObjectsInDemoMode then
         exit;
   end;

   //calculate the projection
   if not issymbol then
      AxGisProjection.Calculate(X,Y)
   else
   begin
      X:=X - SymbolDefXOffset;
      Y:=Y - SymbolDefYOffset;
   end;

   newText:=DestDocument.CreateText;
   newText.FontStyle:=disptype;

   // check if font is installed
   fontexists:=FALSE;
   for i:=0 to Screen.Fonts.Count-1 do
   begin
      if Screen.Fonts[i] = aFont then
      begin
         fontexists:=TRUE;
         break;
      end;
   end;

   if not fontexists then
   begin
      if not issymbol then
         ShowMessage(StrfileObj.Assign(45) + ' '+ aFont +' '+ StrfileObj.Assign(46));
      aFont:='Courier New'; //set default timing
   end;

   newText.Fontname:=aFont;

   // calculate the projection
   newText.Position.X:=round(X*100);
   newText.Position.Y:=round(Y*100);

   newText.Angle:=round(360 - aAngle);
   newText.Text:=aText;
   newText.FontHeight:=round(aHeight*100);

   if not issymbol then
   begin
      ABase:=newText;
      SetToLayerPriorities(ABase);

      // set line color
      newText.ObjectStyle.LineStyle.Color:=LineRgbValue;
      newText.ObjectStyle.LineStyle.FillColor:=LineRgbValue;

      // set linetype
      newText.ObjectStyle.LineStyle.Style:=LineType;

      // set fill type
      newText.ObjectStyle.Fillstyle.Pattern  :=FillType;

      // set fill style
      newText.ObjectStyle.Fillstyle.ForeColor:=FillRgbValue;
   end;

   if not issymbol then
      ALayer.Insertobject(newText)
   else
      ASymboldef.InsertSymbolItem(newText);

   AIndex:=newText.Index;
   result:=AIndex;
end;

function TMainWnd.CreateArc(X:double; Y:double; PrimAxis:double; SecAxis:double; aAngle:double; BeginAngle:double; EndAngle:double; LineRgbValue:longint; LineType:longint; FillRgbValue:longint; FillType:longint;issymbol:boolean):integer;
var newArc:IArc;
    aPoint:IPoint;
    AIndex:integer;
    ABase:IBAse;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = MaxObjectsInDemoMode then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > MaxObjectsInDemoMode then
         exit;
   end;

   //calculate the projection
   if not issymbol then
      AxGisProjection.Calculate(X,Y)
   else
   begin
      X:=X - SymbolDefXOffset;
      Y:=Y - SymbolDefYOffset;
   end;

   newArc:=DestDocument.CreateArc;
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   newArc.Position:=APoint;
   newArc.PrimaryAxis:=round(PrimAxis*100);
   newArc.SecondaryAxis:=round(SecAxis*100);
   newArc.Angle:=aAngle* Pi / 180;
   newArc.BeginAngle:=BeginAngle * Pi / 180;
   newArc.EndAngle:=EndAngle * Pi / 180;

   if not issymbol then
   begin
      ABase:=newArc;
      SetToLayerPriorities(ABase);

      // set line color
      newArc.ObjectStyle.LineStyle.Color:=LineRgbValue;
      newArc.ObjectStyle.LineStyle.FillColor:=LineRgbValue;

      // set linetype
      newArc.ObjectStyle.LineStyle.Style:=LineType;

      // set fill type
      newArc.ObjectStyle.Fillstyle.Pattern  :=FillType;

      // set fill style
      newArc.ObjectStyle.Fillstyle.ForeColor:=FillRgbValue;
   end;

   if not issymbol then
      ALayer.InsertObject(newArc)
   else
      ASymboldef.InsertSymbolItem(newArc);

   AIndex:=newArc.Index;
   result:=AIndex;
end;

function TMainWnd.CreateCircle(X:double;Y:double;Radius:double;LineRgbValue:longint; LineType:longint; FillRgbValue:longint; FillType:longint):integer;
var ACircle:ICircle;
    APoint:IPoint;
    AIndex:integer;
    ABase:IBase;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = MaxObjectsInDemoMode then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > MaxObjectsInDemoMode then
         exit;
   end;
   AxGisProjection.Calculate(X,Y);
   ACircle:=DestDocument.CreateCircle;
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   ACircle.PrimaryAxis:=round(Radius*100);
   ACircle.SecondaryAxis:=round(Radius*100);
   ACircle.Position:=APoint;

   ABase:=ACircle;
   SetToLayerPriorities(ABase);

   // set line color
   ACircle.ObjectStyle.LineStyle.Color:=LineRgbValue;
   ACircle.ObjectStyle.LineStyle.FillColor:=LineRgbValue;

   // set linetype
   ACircle.ObjectStyle.LineStyle.Style:=LineType;

   // set fill type
   ACircle.ObjectStyle.Fillstyle.Pattern  :=FillType;

   // set fill style
   ACircle.ObjectStyle.Fillstyle.ForeColor:=FillRgbValue;

   ALayer.InsertObject(ACircle);
   AIndex:=ACircle.Index;
   result:=AIndex;
end;


procedure TMainWnd.CreateSymbolDef(Symname:string;X:double;Y:double);
var APoint:Variant;
    i:integer;
    MySymbolDef:Variant;
begin
   // check if symbol already exists
   MayCreateSymbol:=true;
   for i:=0 to DestDocument.Symbols.Count-1 do
   begin
      MySymboldef:=DestDocument.Symbols.Items[i];
      if MySymboldef.Name = Symname then
      begin
         MayCreateSymbol:=false;
         break;
      end;
   end;
   if MayCreateSymbol then
   begin
      ASymboldef:=DestDocument.CreateSymbolDef;
      ASymboldef.Name:=Symname;

      ASymboldef.DefaultSizeType:=0;
      ASymboldef.DefaultSize:=100000 / SymbolScale; { Is one meter }
      SymbolDefXOffset:=X*SymbolScale;
      SymbolDefYOffset:=Y*SymbolScale;
   end;
end;

procedure TMainWnd.EndSymbolDef;
begin
   if not MayCreateSymbol then
      exit;
   DestDocument.Symbols.InsertSymbolDef(ASymboldef);
end;

procedure TMainWnd.CloseTimerTimer(Sender: TObject);
begin
   MainWnd.Close;
   SetDgnfileWnd.Close;
   DestDocument.FinishImpExpRoutine(ExitNormal,'IMPORTMODULE');
   CloseTimer.Enabled:=false;
end;

procedure TMainWnd.CancelClick(Sender: TObject);
begin
   if aktiv = true then
   begin
      StatusLB.Items.Add(StrfileObj.Assign(23)); { Warning: Import canceled! }
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      FileObj.CloseallFiles;
      aktiv:=false;
      Abort:=true;
      FileObj.Destroy;
      Cancel.Enabled:=false;
      Ok.Enabled:=true;
      SaveReportBtn.Enabled:=true;
   end;
end;

procedure TMainWnd.OKClick(Sender: TObject);
begin
   CloseTimer.Enabled:=true;
end;

procedure TMainWnd.StartImport;
begin
   if ExitNormal then
   begin
      // set database settings
      RunPass(1);
   end;
end;

procedure TMainWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TMainWnd.SaveReportBtnClick(Sender: TObject);
var Datei:Textfile;
    Zeile:string;
    i:integer;
begin
   if SaveDialogRep.Execute then
   begin
      {$I-}
      Assignfile(Datei,SaveDialogRep.Filename);
      Rewrite(Datei);
      for i:=0 to StatusLB.Items.Count-1 do
      begin
         Zeile:=StatusLB.items[i];
         Writeln(Datei,Zeile);
      end;
      Closefile(Datei);
      {$I+}
   end;
end;

procedure TMainWnd.FormPaint(Sender: TObject);
begin
   Self.BringtoFront;
end;

procedure TMainWnd.FormDestroy(Sender: TObject);
begin
   StrFileObj.Destroy;
end;

procedure TMainWnd.SetupWindowTimerTimer(Sender: TObject);
begin
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,SetDgnfileWnd.winleft,SetDgnfileWnd.wintop,SetDgnfileWnd.winwidth, SetDgnfileWnd.winheight,SWP_SHOWWINDOW);
   MainWnd.Height:=SetDgnfileWnd.winheight;
   MainWnd.Width:=SetDgnfileWnd.winwidth;
   Self.Repaint;
   SetupWindowTimer.Enabled:=false;
   StartImport;
end;

procedure TMainWnd.FormShow(Sender: TObject);
begin
   AxGisProjection.visible:=FALSE;
   AxImpExpDbc.Visible:=FALSE;
end;

end.
