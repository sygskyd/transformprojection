unit struct;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs;

type
  { Hier k�nnen Strukturen definiert werden }
   p_Punkt_record=^Punkt_record;
   Punkt_record = record
       X:double;
       Y:double;
       next :p_Punkt_record;
   end;

   p_Datenbank_record =^Datenbank_record;
   Datenbank_record = record
      Bezeichnung : string;
      Laenge      : integer;
      Typ         : char;
      Inhalt      : string;
      next        : p_Datenbank_record;
   end;

   p_Region_record =^Region_record;
   Region_record = record
      CenterX  :double;
      CenterY  :double;
      Punkte   :p_Punkt_record;
      DBInfo   :p_Datenbank_record;
      Labeled  :boolean;
      CurrentIsland:p_Region_record;
      Islandcount:integer;
      PointsCount:integer;
      Islands  :p_Region_record;
      next     :p_Region_record;
   end;

   p_Pline_record = ^Pline_record;
   Pline_record = record
      Punkte   :p_Punkt_record;
      DBInfo   :p_Datenbank_record;
      Labeled  :boolean;
      next     :p_PLine_record;
   end;

   p_Point_record = ^Point_record;
   Point_record = record
      X     :double;
      Y     :double;
      SymFont:string;
      SymIdx :integer;
      SymSize:integer;
      Labeled:boolean;
      DBInfo:p_Datenbank_record;
      next  :p_Point_record;
   end;

   p_Ellipse_record = ^Ellipse_record;
   Ellipse_record = record
      X      :double;
      Y      :double;
      Radius1:double;
      Radius2:double;
      Labeled:boolean;
      DBInfo :p_Datenbank_record;
      next   :p_Ellipse_record;
   end;

   p_Text_record = ^Text_record;
   Text_record = record
      X1     :double;
      Y1     :double;
      X2     :double;
      Y2     :double;
      Font   :string;
      Text   :string;
      Size   :double;
      Angle  :double;
      Ausrichtung:string;
      DBInfo :p_Datenbank_record;
      next   :p_Text_record;
   end;

   p_SymbolRef_record = ^SymbolRef_record;
   SymbolRef_record = record
       GisSymname:string;
       Fontname:string;
       SymIdx:integer;
       ShowAsPoint:boolean;
       next:p_SymbolRef_record;
   end;

ClassSymbolRef = class
   public
      constructor Create;
      destructor Destroy;
      function  DisplayMifAsSymbol(Fontname:string; SymIdx:integer):boolean;
      procedure AddMifSymbol(Fontname:string; SymIdx:integer);
      procedure SetReference(Fontname:string; SymIdx:integer;SymName:string);
      procedure GetGisReference(Fontname:string; SymIdx:integer;var SymName:string);
      procedure GetMifReference(var Fontname:string; var SymIdx:integer;SymName:string);
      function CheckSymbolsExist:boolean;
   private
      Anfang,Ende,Current :p_SymbolRef_record;
      Anzahl              :longint;
      function CheckMifSymbolExists(Fontname:string; SymIdx:integer):boolean;
   end;

ClassPoint = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X:double;Y:double);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      procedure SetSymbol(SymFont:string;SymIdx:integer;SymSize:integer);
      function  GetAnzahl:longint;
      function  GetLabelData(var X1:double;var Y1:double;var X2:double;var Y2:double; Spaltenname:string; var Text:string):boolean;
      procedure ResetCurrent;
      procedure SetCurrentNext;
      procedure WriteCurrent;
   private
      { private Definitionen }
      Anfang,Ende,Current :p_Point_record;
      Anzahl              :longint;
end;

ClassText = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X1:double;Y1:double;X2:double;Y2:double;Font:string;Size:double;Ausrichtung:string;Text:string);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      procedure SetAngle(Angle:double);
      function  GetAnzahl:longint;
      procedure ResetCurrent;
      procedure SetCurrentNext;
      procedure WriteCurrent;
   private
      { private Definitionen }
      Anfang,Ende,Current :p_Text_record;
      Anzahl              :longint;
end;

ClassEllipse = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X:double;Y:double;Radius1:double;Radius2:double);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      function  GetAnzahl:longint;
      function  GetLabelData(var X1:double;var Y1:double;var X2:double;var Y2:double; Spaltenname:string; var Text:string):boolean;
      procedure ResetCurrent;
      procedure SetCurrentNext;
      procedure WriteCurrent;
   private
      { private Definitionen }
      Anfang,Ende,Current :p_Ellipse_record;
      Anzahl              :longint;
end;

ClassRegion = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu;
      procedure InsertPoint(X:double;Y:double);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      procedure InsertIsland;
      procedure InsertPointToIsland(X:double;Y:double);
      procedure SetCenter(X:double;Y:double);
      function  GetAnzahl:longint;
      function GetLabelData(var X1:double;var Y1:double;var X2:double;var Y2:double;Spaltenname:string;var Text:string):boolean;
      procedure ResetCurrent;
      procedure SetCurrentNext;
      procedure WriteCurrent;
   private
      { private Definitionen }
      Anfang,Ende,Current :p_Region_record;
      Anzahl              :longint;
end;

ClassPline = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu;
      procedure InsertPoint(X:double;Y:double);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      function  GetAnzahl:longint;
      function  GetLabelData(var X1:double;var Y1:double;var X2:double;var Y2:double;Spaltenname:string;var Text:string):boolean;
      procedure GetCenter(var X1:double;var Y1:double; var X2:double; var Y2:double);
      procedure ResetCurrent;
      procedure SetCurrentNext;
      procedure WriteCurrent;
   private
      { private Definitionen }
      Anfang,Ende,Current :p_PLine_record;
      Anzahl              :longint;
end;

ClassDatenbank = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure   Neu(Bezeichnung:string; Laenge:integer; Typ:char; Inhalt:string;var MyAnfang:p_datenbank_record);
      function    Checkexists(Eintrag:string):boolean;
      procedure   SetLength(Bezeichnung:string; Laenge:integer);
      function    GetLength(Bezeichnung:string):integer;
      function    GetType(Bezeichnung:string):char;
      function    GetItem(Bezeichnung:string):string;
      procedure   Insertcolumn(Bezeichnung:string; Laenge:integer; Typ:char; Inhalt:string);
      function    SetDBFirst:integer;
      function    GetDBColumnname:string;
      procedure   SetCurrentnext;
   private
      { private Definitionen }
end;

implementation
uses files, MainDlg;

var FileObj     :ClassFile;
    DatenbankObj:ClassDatenbank;
    Objektnummer:integer;
    DB_Anfang,DB_Current:p_Datenbank_record;


constructor ClassSymbolRef.Create;
begin
   Anfang:=new(p_SymbolRef_record); Ende:=new(p_SymbolRef_record); Current:=new(p_SymbolRef_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassPoint.Create;
begin
   Anfang:=new(p_Point_record); Ende:=new(p_Point_record); Current:=new(p_Point_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassText.Create;
begin
   Anfang:=new(p_Text_record); Ende:=new(p_Text_record); Current:=new(p_Text_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassEllipse.Create;
begin
   Anfang:=new(p_Ellipse_record); Ende:=new(p_Ellipse_record); Current:=new(p_Ellipse_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassRegion.Create;
begin
   Anfang:=new(p_Region_record); Ende:=new(p_Region_record); Current:=new(p_Region_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassPLine.Create;
begin
   Anfang:=new(p_PLine_record); Ende:=new(p_PLine_record); Current:=new(p_PLine_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassDatenbank.Create;
begin
   DB_Anfang:=new(p_Datenbank_record); DB_Current:=new(p_Datenbank_record);
   DB_Anfang:=nil; DB_Current:=nil;
end;

destructor ClassDatenbank.Destroy;
var Zeiger1,Zeiger2:p_Datenbank_record;
begin
   Zeiger1:=DB_Anfang; Zeiger2:=DB_Anfang;
   if DB_Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      dispose(Zeiger2);
   until Zeiger1 = nil;

   DB_Anfang:=nil; DB_Current:=nil;
end;

destructor ClassSymbolRef.Destroy;
var Zeiger1,Zeiger2:p_SymbolRef_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      dispose(Zeiger2);
   until Zeiger1 = nil;
   Anfang:=nil; Current:=nil;
end;

destructor ClassPoint.Destroy;
var Zeiger1,Zeiger2:p_Point_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;

   Anfang:=nil; Current:=nil;
end;

destructor ClassText.Destroy;
var Zeiger1,Zeiger2:p_Text_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;

   Anfang:=nil; Current:=nil;
end;

destructor ClassEllipse.Destroy;
var Zeiger1,Zeiger2:p_Ellipse_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;

   Anfang:=nil; Current:=nil;
end;


destructor ClassRegion.Destroy;
var Zeiger1,Zeiger2:p_Region_record;
    Punkt1,Punkt2: p_Punkt_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;


      { L�schen der Punkte }
      Punkt1:=Zeiger2^.Punkte; Punkt2:=Zeiger2^.Punkte;
      if Punkt1 <> nil then
      repeat
         Punkt2:=Punkt1;
         Punkt1:=Punkt1^.next;
         dispose(Punkt2);
      until Punkt1 = nil;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassPLine.Destroy;
var Zeiger1,Zeiger2:p_PLine_record;
    Punkt1,Punkt2: p_Punkt_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;


      { L�schen der Punkte }
      Punkt1:=Zeiger2^.Punkte; Punkt2:=Zeiger2^.Punkte;
      if Punkt1 <> nil then
      repeat
         Punkt2:=Punkt1;
         Punkt1:=Punkt1^.next;
         dispose(Punkt2);
      until Punkt1 = nil;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.Checkexists                                        *
********************************************************************************
* Funktion �berpr�ft, ob der entsprechende Spalteneintrag bereits in der       *
* Datenbankliste ist.                                                          *
*                                                                              *
* PARAMTER: Eintrag -> Name der Spalte, die �berpr�ft werden soll.             *
*                                                                              *
* R�ckgabewert: TRUE  -> Eintrag ist bereits in Liste.                         *
*               FALSE -> Eintrag ist noch nicht in Liste.                      *
*                                                                              *
********************************************************************************}
function ClassDatenbank.Checkexists(Eintrag:string):boolean;
var Zeiger:p_Datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=DB_Anfang; gefunden:=false;
   if DB_Anfang = nil then begin Result:=false; exit; end;
   repeat
      if (Zeiger^.Bezeichnung = Eintrag) then gefunden:= true;
      Zeiger:=Zeiger^.next;
   until (Zeiger = nil) or (gefunden = true);
   Result:=gefunden;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.SetDBFirst                                         *
********************************************************************************
* Funktion setzt den Akt_Datenbank Zeiger auf den ersten Datenbankspalten-     *
* eintrag in der Datenbankliste.                                               *
*                                                                              *
* R�ckgabewert: Anzahl der vorhandenen Datenbankspalten.                       *
*                                                                              *
********************************************************************************}
function ClassDatenbank.SetDBFirst;
var Zeiger:p_Datenbank_record;
    anz:integer;
begin
   Zeiger:=DB_Anfang; anz:=0;
   { Setzen des Zeigers auf den ersten Datenbankspalteneintrag }
   Zeiger:=Zeiger^.next; { Um den Layernamen zu �berspringen }
   DB_Current:=Zeiger;
   repeat
      if Zeiger^.Bezeichnung <> 'X-KOORDINATE' then
      begin
         anz:=anz + 1;
         Zeiger:=Zeiger^.next;
      end;
   until Zeiger^.Bezeichnung = 'X-KOORDINATE';
   result:=anz;
end;


{*******************************************************************************
* FUNKTION : ClassDatenbank.GetDBColumnname                                    *
********************************************************************************
* Funktion gibt den Spaltenname auf dem sich gerade der Akt_Datenbank Zeiger   *
* befindet zur�ck.                                                             *
*                                                                              *
* R�ckgabewert: Spaltenname der sich auf Akt_Datenbank befindet.               *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetDBColumnname:string;
begin
   Result:=DB_Current^.Bezeichnung;
end;


{*******************************************************************************
* PROZEDUR : ClassDatenbank.SetCurrentnext                                     *
********************************************************************************
* Prozedur setzt den Akt_Datenbank Zeiger um eines weiter.                     *
*                                                                              *
* R�ckgabewert: KEINER                                                         *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.SetCurrentnext;
begin
   DB_Current:=DB_Current^.next;
end;

{*******************************************************************************
* FUNKTION : ClassRegion.GetAnzahl                                             *
********************************************************************************
* Funktion gibt die Anzahl der eingelesenen Region Elemente.                   *
*                                                                              *
* R�ckgabewert: Anzahl der eingelesenen Region Elemente                        *
*                                                                              *
********************************************************************************}
function ClassRegion.GetAnzahl:longint;
begin
   Result:=Anzahl;
   Objektnummer:=1;
end;

{*******************************************************************************
* FUNKTION : ClassRegion.GetLabelData                                          *
********************************************************************************
* Prozedur ermittelt die notwendigen Daten um einen Label zu setzen.           *
*                                                                              *
* PARAMETER: X1,X2,Y1,Y2 -> Koordinaten, die f�r diesen Label notwendig sind   *
*            Spaltenname -> Name der Spalte, die zum Labeln verwendet wird     *
*            Text        -> Inhalt des Labels                                  *
*                                                                              *
* R�CKGABEWERT: TRUE -> Objekt darf gelabeled werden                           *
*               FALSE-> Objekt darf nicht gelabeled werden                     *
*                                                                              *
********************************************************************************}
function ClassRegion.GetLabelData(var X1:double;var Y1:double;var X2:double;var Y2:double; Spaltenname:string; var Text:string):boolean;
var DBZeiger:p_Datenbank_record;
begin
   if (Current^.Labeled = false) and (Spaltenname <> '') then
   begin
      X1:=Current^.CenterX; X2:=Current^.CenterX;
      Y1:=Current^.CenterY; Y2:=Current^.CenterY;

      { Nun muss das Attribut ermittelt werden }
      DBZeiger:=Current^.DBInfo;
      while (DBZeiger^.Bezeichnung <> Spaltenname) do DBZeiger:=DBZeiger^.next;
      Text:=DBZeiger^.Inhalt;
      Current^.Labeled:=true;
      Result:=true;
   end
   else
      Result:=false;
end;

{*******************************************************************************
* FUNKTION : ClassPoint.GetLabelData                                           *
********************************************************************************
* Prozedur ermittelt die notwendigen Daten um einen Label zu setzen.           *
*                                                                              *
* PARAMETER: X1,X2,Y1,Y2 -> Koordinaten, die f�r diesen Label notwendig sind   *
*            Spaltenname -> Name der Spalte, die zum Labeln verwendet wird     *
*            Text        -> Inhalt des Labels                                  *
*                                                                              *
* R�CKGABEWERT: TRUE -> Objekt darf gelabeled werden                           *
*               FALSE-> Objekt darf nicht gelabeled werden                     *
*                                                                              *
********************************************************************************}
function ClassPoint.GetLabelData(var X1:double;var Y1:double;var X2:double;var Y2:double; Spaltenname:string; var Text:string):boolean;
var DBZeiger:p_Datenbank_record;
begin
   if (Current^.Labeled = false) and (Spaltenname <> '') then
   begin
      X1:=Current^.X; X2:=Current^.X;
      Y1:=Current^.Y; Y2:=Current^.Y;

      { Nun muss das Attribut ermittelt werden }
      DBZeiger:=Current^.DBInfo;
      while (DBZeiger^.Bezeichnung <> Spaltenname) do DBZeiger:=DBZeiger^.next;
      Text:=DBZeiger^.Inhalt;
      Current^.Labeled:=true;
      Result:=true;
   end
   else
      Result:=false;
end;

{*******************************************************************************
* FUNKTION : ClassEllipse.GetLabelData                                         *
********************************************************************************
* Prozedur ermittelt die notwendigen Daten um einen Label zu setzen.           *
*                                                                              *
* PARAMETER: X1,X2,Y1,Y2 -> Koordinaten, die f�r diesen Label notwendig sind   *
*            Spaltenname -> Name der Spalte, die zum Labeln verwendet wird     *
*            Text        -> Inhalt des Labels                                  *
*                                                                              *
* R�CKGABEWERT: TRUE -> Objekt darf gelabeled werden                           *
*               FALSE-> Objekt darf nicht gelabeled werden                     *
*                                                                              *
********************************************************************************}
function ClassEllipse.GetLabelData(var X1:double;var Y1:double;var X2:double;var Y2:double; Spaltenname:string; var Text:string):boolean;
var DBZeiger:p_Datenbank_record;
begin
   if (Current^.Labeled = false) and (Spaltenname <> '') then
   begin
      X1:=Current^.X; X2:=Current^.X;
      Y1:=Current^.Y; Y2:=Current^.Y;

      { Nun muss das Attribut ermittelt werden }
      DBZeiger:=Current^.DBInfo;
      while (DBZeiger^.Bezeichnung <> Spaltenname) do DBZeiger:=DBZeiger^.next;
      Text:=DBZeiger^.Inhalt;
      Current^.Labeled:=true;
      Result:=true;
   end
   else
      Result:=false;
end;

{*******************************************************************************
* FUNKTION : ClassPLine.GetLabelData                                           *
********************************************************************************
* Prozedur ermittelt die notwendigen Daten um einen Label zu setzen.           *
*                                                                              *
* PARAMETER: X1,X2,Y1,Y2 -> Koordinaten, die f�r diesen Label notwendig sind   *
*            Spaltenname -> Name der Spalte, die zum Labeln verwendet wird     *
*            Text        -> Inhalt des Labels                                  *
*                                                                              *
* R�CKGABEWERT: TRUE -> Objekt darf gelabeled werden                           *
*               FALSE-> Objekt darf nicht gelabeled werden                     *
*                                                                              *
********************************************************************************}
function ClassPLine.GetLabelData(var X1:double;var Y1:double;var X2:double;var Y2:double; Spaltenname:string; var Text:string):boolean;
var DBZeiger:p_Datenbank_record;
begin
   if (Current^.Labeled = false) and (Spaltenname <> '') then
   begin
      { Ermitteln der Einsetzpunkte f�r den Label }
      GetCenter(X1,Y1,X2,Y2);

      { Nun muss das Attribut ermittelt werden }
      DBZeiger:=Current^.DBInfo;
      while (DBZeiger^.Bezeichnung <> Spaltenname) do DBZeiger:=DBZeiger^.next;
      Text:=DBZeiger^.Inhalt;
      Current^.Labeled:=true;
      Result:=true;
   end
   else
      Result:=false;
end;

{*******************************************************************************
* PROZEDUR : ClassPLine.GetCenter                                              *
********************************************************************************
* Prozedur ermittelt den Einsetzpunkt eines Labels in einem PLine Objekt.      *
*                                                                              *
* PARAMETER: X1,X2,Y1,Y2 -> Koordinaten, die f�r diesen Label notwendig sind   *
*                                                                              *
********************************************************************************}
procedure ClassPLine.GetCenter(var X1:double;var Y1:double;var X2:double; var Y2:double);
var AktX1,AktX2,AktY1,AktY2:double;
    MaxX1,MaxX2,MaxY1,MaxY2:double;
    AktLength,Maxlength:double;
    Zeiger:p_Punkt_record;
begin
   Zeiger:=Current^.Punkte; MaxLength:=0;
   repeat
      AktX1:=Zeiger^.X; AktY1:=Zeiger^.Y;
      AktX2:=Zeiger^.next^.X; AktY2:=Zeiger^.next^.Y;

      { Nun muss die L�nge der Strecke, die zwischen diesen beiden Punkte liegt errechnet werden }
      AktLength:=SQRT(SQR(ABS(AktX1 - AktX2)) + SQR(ABS(AktY1 - AktY2)));

      if AktLength > MaxLength then
      begin
         MaxX1:=AktX1; MaxX2:=AktX2; MaxY1:=AktY1; MaxY2:=AktY2;
         MaxLength:=AktLength;
      end;
      Zeiger:=Zeiger^.next;
   until Zeiger^.next = nil;

   if (MaxX1 > MaxX2) and (MaxY1 > MaxY2) then { Richtung geht von rechts oben nach links unten }
      begin X1:=MaxX2; Y1:=MaxY2; X2:=MaxX1; Y2:=MaxY1; end else
   if (MaxX1 > MaxX2) and (MaxY1 < MaxY2) then { Richtung geht von rechts unten nach links oben }
      begin X1:=MaxX2; Y1:=MaxY2; X2:=MaxX1; Y2:=MaxY1; end else
   begin X1:=MaxX1; Y1:=MaxY1; X2:=MaxX2; Y2:=MaxY2; end; { Sonst braucht man X und Y nur �bernehmen }
end;

{*******************************************************************************
* FUNKTION : ClassPoint.GetAnzahl                                              *
********************************************************************************
* Funktion gibt die Anzahl der eingelesenen Point Elemente.                    *
*                                                                              *
* R�ckgabewert: Anzahl der eingelesenen Point Elemente                         *
*                                                                              *
********************************************************************************}
function ClassPoint.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

{*******************************************************************************
* FUNKTION : ClassText.GetAnzahl                                               *
********************************************************************************
* Funktion gibt die Anzahl der eingelesenen Text Elemente.                     *
*                                                                              *
* R�ckgabewert: Anzahl der eingelesenen Text Elemente                          *
*                                                                              *
********************************************************************************}
function ClassText.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

{*******************************************************************************
* FUNKTION : ClassEllipse.GetAnzahl                                            *
********************************************************************************
* Funktion ermittelt die Anzahl der eingelesenen Ellipse Elemente.             *
*                                                                              *
* R�ckgabewert: Anzahl der eingelesenen Ellipse Elemente.                      *
*                                                                              *
********************************************************************************}
function ClassEllipse.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

{*******************************************************************************
* PROZEDUR : ClassRegion.SetCenter                                             *
********************************************************************************
* Prozedur setzt die Koordinaten des Centerpunktes.                            *
*                                                                              *
* PARAMETER: X -> X Koordinate des Centerpunktes.                              *
*            Y -> Y Koordinate des Centerpunktes.
*                                                                              *
********************************************************************************}
procedure ClassRegion.SetCenter(X:double;Y:double);
begin
   Current^.CenterX:=X;
   Current^.CenterY:=Y;
end;

procedure ClassPoint.SetSymbol(SymFont:string;SymIdx:integer;SymSize:integer);
begin
   Current^.SymFont:=SymFont;
   Current^.SymIdx:=SymIdx;
   Current^.SymSize:=SymSize;
end;

{*******************************************************************************
* PROZEDUR : ClassText.SetAngle                                                *
********************************************************************************
* Prozedur setzt den Rotationswinkel bei einem Text.                           *
*                                                                              *
* PARAMETER: Angle -> Rotationswinkel                                          *
*                                                                              *
********************************************************************************}
procedure ClassText.SetAngle(Angle:double);
begin
   Current^.Angle:=Angle;
end;

{*******************************************************************************
* FUNKTION : ClassPLine.GetAnzahl                                              *
********************************************************************************
* Funktion gibt die Anzahl der eingelesenen PLine  Elemente.                   *
*                                                                              *
* R�ckgabewert: Anzahl der eingelesenen PLine  Elemente                        *
*                                                                              *
********************************************************************************}
function ClassPLine.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

{*******************************************************************************
* PROZEDUR : ClassRegion.InsertAttrib                                          *
********************************************************************************
* Prozedur f�gt einen Datenbankinformationseintrag zum Current Region          *
*                                                                              *
* PARAMETER: Spaltenname   -> Name der Spalte die eingef�gt werden soll.       *
*            Spalteninhalt -> Inhalt der Datenbankspalte.                      *
*                                                                              *
********************************************************************************}
procedure ClassRegion.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,'C',Spalteninhalt,Current^.DBInfo);
end;

{*******************************************************************************
* PROZEDUR : ClassPoint.InsertAttrib                                           *
********************************************************************************
* Prozedur f�gt einen Datenbankinformationseintrag zum Current Point           *
*                                                                              *
* PARAMETER: Spaltenname   -> Name der Spalte die eingef�gt werden soll.       *
*            Spalteninhalt -> Inhalt der Datenbankspalte.                      *
*                                                                              *
********************************************************************************}
procedure ClassPoint.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,'C',Spalteninhalt,Current^.DBInfo);
end;

{*******************************************************************************
* PROZEDUR : ClassText.InsertAttrib                                            *
********************************************************************************
* Prozedur f�gt einen Datenbankinformationseintrag zum Current Text ein.       *
*                                                                              *
* PARAMETER: Spaltenname   -> Name der Spalte die eingef�gt werden soll.       *
*            Spalteninhalt -> Inhalt der Datenbankspalte.                      *
*                                                                              *
********************************************************************************}
procedure ClassText.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,'C',Spalteninhalt,Current^.DBInfo);
end;

{*******************************************************************************
* PROZEDUR : ClassEllipse.InsertAttrib                                         *
********************************************************************************
* Prozedur f�gt einen Datenbankinformationseintrag zum Current Ellipse ein.    *
*                                                                              *
* PARAMETER: Spaltenname   -> Name der Spalte die eingef�gt werden soll.       *
*            Spalteninhalt -> Inhalt der Datenbankspalte.                      *
*                                                                              *
********************************************************************************}
procedure ClassEllipse.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,'C',Spalteninhalt,Current^.DBInfo);
end;

{*******************************************************************************
* PROZEDUR : ClassPLine.InsertAttrib                                          *
********************************************************************************
* Prozedur f�gt einen Datenbankinformationseintrag zum Current PLine           *
*                                                                              *
* PARAMETER: Spaltenname   -> Name der Spalte die eingef�gt werden soll.       *
*            Spalteninhalt -> Inhalt der Datenbankspalte.                      *
*                                                                              *
********************************************************************************}
procedure ClassPLine.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,'C',Spalteninhalt,Current^.DBInfo);
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.InsertColumn                                       *
********************************************************************************
* Prozedur erzeugt einen neuen Spalteneintrag f�r die Datenbank und f�gt diesen*
* in die Datenbank_anfang Liste ein.                                           *
*                                                                              *
* PARAMETER: Bezeichnung -> Spaltenbezeichnung                                 *
*            Laenge      -> L�nge der Spalte                                   *
*            Typ         -> Typ der Spalte (C oder N)                          *
*            Inhalt      -> Inhalt der Spalte                                  *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.Insertcolumn(Bezeichnung:string; Laenge:integer; Typ:char; Inhalt:string);
begin
   Neu(Bezeichnung, Laenge, Typ, Inhalt,DB_Anfang);
end;

{*******************************************************************************
* PROZEDUR : ClassRegion.WriteCurrent                                          *
********************************************************************************
* Prozedur ermittelt die Daten der aktuellen Region    und gibt diese an die   *
* Schreibfunktion in der Unit files weiter.                                    *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassRegion.WriteCurrent;
var hZeile,Bezeichnung:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i:integer;
    gefunden :boolean;
    PunktZeiger:p_Punkt_record;
    Layername:string;
    new_ID:integer;
    CurrentIsland:p_Region_record;
    First_X,First_Y:double;
    rotatepolygon:boolean;
begin
   if Current = nil then exit;
   rotatepolygon:=FALSE;
   { Schreiben der aktuellen Punktdaten }
   Objektnummer:=Objektnummer + 1; PunktZeiger:=Current^.Punkte;

   Layername:=DatenbankObj.GetItem('LAYERNAME');
   if MainWnd.CheckLayerExists(Layername) = FALSE then
      MainWnd.CreateLayer(Layername);

   if Punktzeiger = nil then exit;

   if Current^.Islands <> nil then
      MainWnd.CreateIslandList;

   // now the main polygon will be created
   First_X:=PunktZeiger^.X; First_Y:=PunktZeiger^.Y;
   MainWnd.CreatePolygon;
   repeat
      MainWnd.InsertPointToPolygon(PunktZeiger^.X,PunktZeiger^.Y);
      Punktzeiger:=Punktzeiger^.next;
   until PunktZeiger = nil;
   if Current^.Islands <> nil then
   begin
      MainWnd.ClosePolyAndAddToIsland;
      // now create the other islands
      CurrentIsland:=Current^.Islands;
      while (CurrentIsland <> nil) do
      begin
         MainWnd.CreatePolygon;
         PunktZeiger:=CurrentIsland^.Punkte;
         if CurrentIsland^.next = nil then
         begin
            // last polygon of the islands
            // check if the first point has same coordinates than first
            // point of the parent polygon
            if (PunktZeiger^.X = First_X) and (PunktZeiger^.Y = First_Y) then
            begin
               rotatepolygon:=true;
               PunktZeiger:=PunktZeiger^.next;
            end;
         end;

         repeat
            MainWnd.InsertPointToPolygon(PunktZeiger^.X,PunktZeiger^.Y);
            Punktzeiger:=Punktzeiger^.next;
         until PunktZeiger = nil;
         if rotatepolygon then
            MainWnd.InsertPointToPolygon(First_X,First_Y);
         MainWnd.ClosePolyAndAddToIsland;
         CurrentIsland:=CurrentIsland^.next;
      end;
      new_ID:=MainWnd.CloseIsland;
   end
   else
      new_ID:=MainWnd.ClosePolygon;

   MainWnd.ADDIMPIDINFO(Layername, new_ID);

   // write database-information
   DBAnz:=DatenbankObj.SetDBFirst;
   DBZeiger:=Current^.DBInfo;
   if DBAnz > 0 then
   begin
      for i:=1 to DBAnz do { Nun m�ssen alle Datenbankspalten, die im ADF-vorkommen durchlaufen werden }
      begin
         Bezeichnung:=DatenbankObj.GetDBColumnname;
         DatenbankObj.SetCurrentnext;
         DBZeiger:=Current^.DBInfo;
         if DBZeiger <> nil then
         begin
            { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
            DBZeiger:=Current^.DBInfo; gefunden:=false;
            repeat
               if DBZeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
               if gefunden = false then DBZeiger:=DBZeiger^.next;
            until (gefunden = true) or (DBZeiger = nil);
            if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
               hZeile:=DBZeiger^.Inhalt
            else
               hZeile:='';
         end;
         MainWnd.ADDIMPINFO(Layername, Bezeichnung, hZeile);
      end;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassPoint.WriteCurrent                                           *
********************************************************************************
* Prozedur ermittelt die Daten des aktuellen Point    und gibt diese an die    *
* Schreibfunktion in der Unit files weiter.                                    *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassPoint.WriteCurrent;
var Zeile,hZeile,Bezeichnung:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i:integer;
    First,gefunden :boolean;
    Layername:string;
    new_ID:integer;
    Symbolname:string;
    SymText:string;
begin
   if Current = nil then exit;

   Layername:=DatenbankObj.GetItem('LAYERNAME');
   if MainWnd.CheckLayerExists(Layername) = FALSE then
      MainWnd.CreateLayer(Layername);

   // check if point should be displayed as Symbol or as Point
   if FileObj.DisplayMifAsSymbol(Current^.SymFont, Current^.SymIdx) then
   begin
      // create Symbol
      Symbolname:=FileObj.GetGisSymbolName(Current^.SymFont, Current^.SymIdx);
      new_ID:=MainWnd.InsertSymbol(Current^.X,Current^.Y,Symbolname,Current^.SymSize,0);
   end
   else
   begin
      if Current^.SymFont <> '' then // write Symbol as Text
      begin
         SymText:=chr(Current^.SymIdx);
         new_ID:=MainWnd.InsertSymbolText(Current^.X,Current^.Y,SymText, Current^.SymSize, Current^.SymFont);
      end
      else
         new_ID:=MainWnd.InsertPoint(Current^.X,Current^.Y);
   end;

   { Schreiben der Datenbankinformation }
   DBAnz:=DatenbankObj.SetDBFirst;
   DBZeiger:=Current^.DBInfo;

   MainWnd.ADDIMPIDINFO(Layername, new_ID);
   if DBAnz > 0 then
   begin
      for i:=1 to DBAnz do { Nun m�ssen alle Datenbankspalten, die im ADF-vorkommen durchlaufen werden }
      begin
         Bezeichnung:=DatenbankObj.GetDBColumnname;
         DatenbankObj.SetCurrentnext;
         Zeile:=Zeile + ' '; DBZeiger:=Current^.DBInfo;
         if DBZeiger <> nil then
         begin
            { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
            DBZeiger:=Current^.DBInfo; gefunden:=false;
            repeat
               if DBZeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
               if gefunden = false then DBZeiger:=DBZeiger^.next;
            until (gefunden = true) or (DBZeiger = nil);
            if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
               hZeile:=DBZeiger^.Inhalt
            else hZeile:='';
         end
         else
            hZeile:='';

         MainWnd.ADDIMPINFO(Layername, Bezeichnung, hZeile);
      end;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassText.WriteCurrent                                            *
********************************************************************************
* Prozedur ermittelt die Daten des aktuellen Text und gibt diese an die        *
* Schreibfunktion in der Unit files weiter.                                    *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassText.WriteCurrent;
var hZeile,Bezeichnung:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i,Angle,Height:integer;
    First,gefunden,WriteTextFlag:boolean;
    Layername:string;
    new_ID:integer;
begin
   if Current = nil then exit;

   Objektnummer:=Objektnummer + 1; first:=true;
   WriteTextFlag:=true;

   Layername:=DatenbankObj.GetItem('LAYERNAME');
   if MainWnd.CheckLayerExists(Layername) = FALSE then
      MainWnd.CreateLayer(Layername);

   hZeile:=floattostr(current^.Angle);
   if Pos('.',hZeile) <> 0 then hZeile:=copy(hZeile,1,Pos('.',hZeile)-1);
   Angle:=strtoint(hZeile);

   hZeile:=floattostr(current^.Size);
   if Pos('.',hZeile) <> 0 then hZeile:=copy(hZeile,1,Pos('.',hZeile)-1);
   Height:=strtoint(hZeile); Height:=Height*100;

   if Current^.Text = '' then exit;

   new_ID:=MainWnd.InsertText(Current^.X1,Current^.Y1,Current^.X2,Current^.Y2, Angle,Current^.Text,Height,Current^.Font,false);

   { Schreiben der Datenbankinformation }
   DBAnz:=DatenbankObj.SetDBFirst;
   DBZeiger:=Current^.DBInfo; first:=false;

   MainWnd.ADDIMPIDINFO(Layername, new_ID);

   if DBAnz > 0 then
   begin
      for i:=1 to DBAnz do { Nun m�ssen alle Datenbankspalten, die im ADF-vorkommen durchlaufen werden }
      begin
         Bezeichnung:=DatenbankObj.GetDBColumnname;
         DatenbankObj.SetCurrentnext;
         DBZeiger:=Current^.DBInfo;
         if DBZeiger <> nil then
         begin
            { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
            DBZeiger:=Current^.DBInfo; gefunden:=false;
            repeat
               if DBZeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
               if gefunden = false then DBZeiger:=DBZeiger^.next;
            until (gefunden = true) or (DBZeiger = nil);
            if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
               hZeile:=DBZeiger^.Inhalt
            else hZeile:='';
         end
         else
            hZeile:='';

         MainWnd.ADDIMPINFO(Layername, Bezeichnung, hZeile);
      end;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassEllipse.WriteCurrent                                         *
********************************************************************************
* Prozedur ermittelt die Daten der aktuellen Ellipse  und gibt diese an die    *
* Schreibfunktion in der Unit files weiter.                                    *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassEllipse.WriteCurrent;
var hZeile,Bezeichnung,ZeileohneDB:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i,Rad1,Rad2:integer;
    First,gefunden :boolean;
    Layername:string;
    new_ID:integer;
begin
   if Current = nil then exit;

   Layername:=DatenbankObj.GetItem('LAYERNAME');
   if MainWnd.CheckLayerExists(Layername) = FALSE then
      MainWnd.CreateLayer(Layername);

   hZeile:=floattostr(Current^.Radius1);
   if Pos('.',hZeile) <> 0 then hZeile:=copy(hZeile,1,Pos('.',hZeile)-1);
   Rad1:=strtoint(hZeile);

   hZeile:=floattostr(Current^.Radius2);
   if Pos('.',hZeile) <> 0 then hZeile:=copy(hZeile,1,Pos('.',hZeile)-1);
   Rad2:=strtoint(hZeile);

   new_ID:=MainWnd.CreateEllipse(Current^.X,Current^.Y,Rad1,Rad2);

   { Schreiben der Datenbankinformation }
   DBAnz:=DatenbankObj.SetDBFirst;
   DBZeiger:=Current^.DBInfo; first:=false;

   MainWnd.ADDIMPIDINFO(Layername, new_ID);

   if DBAnz > 0 then
   begin
      for i:=1 to DBAnz do { Nun m�ssen alle Datenbankspalten, die im ADF-vorkommen durchlaufen werden }
      begin
         Bezeichnung:=DatenbankObj.GetDBColumnname;
         DatenbankObj.SetCurrentnext;
         DBZeiger:=Current^.DBInfo;
         if DBZeiger <> nil then
         begin
            { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
            DBZeiger:=Current^.DBInfo; gefunden:=false;
            repeat
               if DBZeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
               if gefunden = false then DBZeiger:=DBZeiger^.next;
            until (gefunden = true) or (DBZeiger = nil);
            if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
               hZeile:=DBZeiger^.Inhalt
            else hZeile:='';
         end
         else
            hZeile:='';

         MainWnd.ADDIMPINFO(Layername, Bezeichnung, hZeile);
      end;
   end;
end;


{*******************************************************************************
* PROZEDUR : ClassPLine.WriteCurrent                                           *
********************************************************************************
* Prozedur ermittelt die Daten der aktuellen PLine     und gibt diese an die   *
* Schreibfunktion in der Unit files weiter.                                    *
*                                                                              *
* PARAMETER: DBAnfang -> Zeiger auf den Anfang der Datenbankliste              *
*            Art      -> Gibt an welche Datenbankspalten verwendet werden      *
*                                                                              *
********************************************************************************}
procedure ClassPLine.WriteCurrent;
var Zeile,hZeile,Bezeichnung:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i:integer;
    First,gefunden :boolean;
    PunktZeiger:p_Punkt_record;
    Layername:string;
    new_ID:integer;
begin
   if Current = nil then exit;

   Layername:=DatenbankObj.GetItem('LAYERNAME');
   if MainWnd.CheckLayerExists(Layername) = FALSE then
      MainWnd.CreateLayer(Layername);

   { Schreiben der aktuellen Punktdaten }
   Objektnummer:=Objektnummer + 1; first:=true; PunktZeiger:=Current^.Punkte;
   if Punktzeiger = nil then exit;
   MainWnd.CreatePolyline;

   repeat
      MainWnd.InsertPointToPolyLine(PunktZeiger^.X,PunktZeiger^.Y);
      Punktzeiger:=Punktzeiger^.next;
   until PunktZeiger = nil;

   new_ID:=MainWnd.ClosePolyline;

   DBAnz:=DatenbankObj.SetDBFirst;
   DBZeiger:=Current^.DBInfo; first:=false;

   MainWnd.ADDIMPIDINFO(Layername, new_ID);

   if DBAnz > 0 then
   begin
      for i:=1 to DBAnz do { Nun m�ssen alle Datenbankspalten, die im ADF-vorkommen durchlaufen werden }
      begin
         Bezeichnung:=DatenbankObj.GetDBColumnname;
         DatenbankObj.SetCurrentnext;
         DBZeiger:=Current^.DBInfo;
         if DBZeiger <> nil then
         begin
            { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
            DBZeiger:=Current^.DBInfo; gefunden:=false;
            repeat
               if DBZeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
               if gefunden = false then DBZeiger:=DBZeiger^.next;
            until (gefunden = true) or (DBZeiger = nil);
            if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
               hZeile:=DBZeiger^.Inhalt
            else
               hZeile:='';
         end
         else
            hZeile:='';

         MainWnd.ADDIMPINFO(Layername, Bezeichnung, hZeile);
      end;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassRegion.ResetCurrent                                          *
********************************************************************************
* Prozedur setzt den Current Region Zeiger auf den Anfang.                     *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassRegion.ResetCurrent;
begin
   current:=anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassPoint.ResetCurrent                                           *
********************************************************************************
* Prozedur setzt den Current Point Zeiger auf den Anfang.                      *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassPoint.ResetCurrent;
begin
   current:=anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassText.ResetCurrent                                            *
********************************************************************************
* Prozedur setzt den Current Text Zeiger auf den Anfang.                       *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassText.ResetCurrent;
begin
   current:=anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassEllipse.ResetCurrent                                         *
********************************************************************************
* Prozedur setzt den Current Ellipse Zeiger auf den Anfang.                    *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassEllipse.ResetCurrent;
begin
   current:=anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassPLine.ResetCurrent                                           *
********************************************************************************
* Prozedur setzt den Current Pline  Zeiger auf den Anfang.                     *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassPLine.ResetCurrent;
begin
   current:=anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassRegion.SetCurrentnext                                        *
********************************************************************************
* Prozedur setzt den Current Region Zeiger auf den n�chsten Eintrag.           *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassRegion.SetCurrentNext;
begin
   if Current <> nil then if Current^.next <> nil then Current:=Current^.next;
end;

{*******************************************************************************
* PROZEDUR : ClassPoint.SetCurrentnext                                         *
********************************************************************************
* Prozedur setzt den Current Point Zeiger auf den n�chsten Eintrag.            *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassPoint.SetCurrentNext;
begin
   if Current <> nil then if Current^.next <> nil then Current:=Current^.next;
end;

{*******************************************************************************
* PROZEDUR : ClassText.SetCurrentnext                                          *
********************************************************************************
* Prozedur setzt den Current Text Zeiger auf den n�chsten Eintrag.             *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassText.SetCurrentNext;
begin
   if Current <> nil then if Current^.next <> nil then Current:=Current^.next;
end;

{*******************************************************************************
* PROZEDUR : ClassEllipse.SetCurrentnext                                       *
********************************************************************************
* Prozedur setzt den Current Ellipse Zeiger auf den n�chsten Eintrag.          *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassEllipse.SetCurrentNext;
begin
   if Current <> nil then if Current^.next <> nil then Current:=Current^.next;
end;

{*******************************************************************************
* PROZEDUR : ClassPline.SetCurrentnext                                         *
********************************************************************************
* Prozedur setzt den Current Pline  Zeiger auf den n�chsten Eintrag.           *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassPline.SetCurrentNext;
begin
   if Current <> nil then if Current^.next <> nil then Current:=Current^.next;
end;

procedure ClassSymbolRef.AddMifSymbol(Fontname:string; SymIdx:integer);
var neuer_record,Zeiger:p_SymbolRef_record;
begin
   // only insert mif-symbol that still does not exist
   if CheckMifSymbolExists(Fontname,Symidx) then exit;

   new(neuer_record);
   neuer_record^.GisSymname:='';
   neuer_record^.Fontname:=Fontname;
   neuer_record^.SymIdx:=SymIdx;
   neuer_record^.ShowAsPoint:=true; // display symbol as point (default)

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

procedure ClassSymbolRef.SetReference(Fontname:string; SymIdx:integer;SymName:string);
var iterate:p_SymbolRef_record;
    found:boolean;
begin
   iterate:=Anfang;
   found:=false;
   while ((iterate <> nil) and (not found)) do
   begin
      if ((iterate^.Fontname = Fontname) and (iterate^.SymIdx = SymIdx)) then
      begin
         found:=true;
         iterate^.GisSymname:=SymName;
      end;
   end;
end;

function ClassSymbolRef.CheckSymbolsExist:boolean;
var retVal:boolean;
begin
   if Anfang = nil then retVal:=false else retVal:=true;
   result:=retVal;
end;

function  ClassSymbolRef.DisplayMifAsSymbol(Fontname:string; SymIdx:integer):boolean;
var iterate:p_SymbolRef_record;
    found:boolean;
begin
   iterate:=Anfang;
   found:=false;
   while ((iterate <> nil) and (not found)) do
   begin
      if ((iterate^.Fontname = Fontname) and (iterate^.SymIdx = SymIdx)) then
      begin
         found:=true;
         if iterate^.ShowAsPoint then result:=false else result:=true;
         if iterate^.Fontname <> '' then result:=false;
      end;
      iterate:=iterate^.next;
   end;
end;

procedure ClassSymbolRef.GetGisReference(Fontname:string; SymIdx:integer;var SymName:string);
var iterate:p_SymbolRef_record;
    found:boolean;
begin
   iterate:=Anfang;
   found:=false;
   while ((iterate <> nil) and (not found)) do
   begin
      if ((iterate^.Fontname = Fontname) and (iterate^.SymIdx = SymIdx)) then
      begin
         found:=true;
         SymName:=iterate^.GisSymname;
      end;
      iterate:=iterate^.next;
   end;
end;

procedure ClassSymbolRef.GetMifReference(var Fontname:string; var SymIdx:integer;SymName:string);
var iterate:p_SymbolRef_record;
    found:boolean;
begin
   iterate:=Anfang;
   found:=false;
   while ((iterate <> nil) and (not found)) do
   begin
      if (iterate^.GisSymname = Symname) then
      begin
         found:=true;
         Fontname:=iterate^.Fontname;
         SymIdx:=iterate^.SymIdx;
      end;
      iterate:=iterate^.next;
   end;
end;

function ClassSymbolRef.CheckMifSymbolExists(Fontname:string; SymIdx:integer):boolean;
var iterate:p_SymbolRef_record;
    found:boolean;
begin
   iterate:=Anfang;
   found:=false;
   while ((iterate <> nil) and (not found)) do
   begin
      if ((iterate^.Fontname = Fontname) and (iterate^.SymIdx = SymIdx)) then
      begin
         found:=true;
      end;
      iterate:=iterate^.next;
   end;
   result:=found;
end;

procedure ClassRegion.InsertIsland;
var new_record,iterate:p_Region_record;
begin
   new(new_record);
   new_record^.Punkte:=nil;
   Current^.IslandCount:=Current^.IslandCount+1;
   if Current^.Islands = NIL then
   begin
      Current^.Islands:=new_record;
      new_record^.next:=NIL;
   end
   else
   begin
      iterate:=Current^.Islands;
      while (iterate^.next <> nil) do iterate:=iterate^.next;
      iterate^.next:=new_record;
      new_record^.next:=nil;
   end;
   Current^.CurrentIsland:=new_record;
   Current^.CurrentIsland^.PointsCount:=0;
end;

procedure ClassRegion.Neu;
var neuer_record,Zeiger:p_Region_record;
begin
   new(neuer_record);
   neuer_record^.Punkte:=nil;
   neuer_record^.DBInfo:=nil;
   neuer_record^.Labeled:=false;
   neuer_record^.CurrentIsland:=nil;
   neuer_record^.Islands:=nil;
   neuer_record^.IslandCount:=0;
   neuer_record^.PointsCount:=0;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

procedure ClassPoint.Neu(X:double;Y:double);
var neuer_record,Zeiger:p_Point_record;
    hstr:string;
begin
   if DatenbankObj.Checkexists('X-KOORDINATE') = false then DatenbankObj.Insertcolumn('X-KOORDINATE',0,'N','');
   if DatenbankObj.Checkexists('Y-KOORDINATE') = false then DatenbankObj.Insertcolumn('Y-KOORDINATE',0,'N','');
   hstr:=floattostr(X); DatenbankObj.SetLength('X-KOORDINATE',length(hstr));
   hstr:=floattostr(Y); DatenbankObj.SetLength('Y-KOORDINATE',length(hstr));

   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.DBInfo:=nil;
   neuer_record^.Labeled:=false;
   neuer_record^.SymFont:='';
   neuer_record^.SymIdx:=0;
   neuer_record^.SymSize:=0;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassText.Neu                                                     *
********************************************************************************
* Prozedur erzeugt einen neuen Text Eintrag.                                  *
*                                                                              *
* PARAMETER: X,Y -> Koordinaten des neuen Point Objekts.                       *
*                                                                              *
********************************************************************************}
procedure ClassText.Neu(X1:double;Y1:double;X2:double;Y2:double;Font:string;Size:double;Ausrichtung:string;Text:string);
var neuer_record,Zeiger:p_Text_record;
    hstr:string;
begin
   if DatenbankObj.Checkexists('X-KOORDINATE') = false then DatenbankObj.Insertcolumn('X-KOORDINATE',0,'N','');
   if DatenbankObj.Checkexists('Y-KOORDINATE') = false then DatenbankObj.Insertcolumn('Y-KOORDINATE',0,'N','');
   hstr:=floattostr(X1); DatenbankObj.SetLength('X-KOORDINATE',length(hstr));
   hstr:=floattostr(Y1); DatenbankObj.SetLength('Y-KOORDINATE',length(hstr));
   hstr:=floattostr(X2); DatenbankObj.SetLength('X-KOORDINATE',length(hstr));
   hstr:=floattostr(Y2); DatenbankObj.SetLength('Y-KOORDINATE',length(hstr));

   new(neuer_record);
   neuer_record^.X1:=X1; neuer_record^.Y1:=Y1;
   neuer_record^.X2:=X2; neuer_record^.Y2:=Y2;
   neuer_record^.Font:=Font; neuer_record^.size:=Size;
   neuer_record^.Angle:=0;
   neuer_record^.Ausrichtung:=Ausrichtung;
   neuer_record^.Text:=Text;
   neuer_record^.DBInfo:=nil;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassEllipse.Neu                                                  *
********************************************************************************
* Prozedur erzeugt einen neuen Ellipse Eintrags.                               *
*                                                                              *
* PARAMETER: X,Y    -> Einsetzpunkt des neuen Ellipse Objekts.                 *
*            Radius -> Radius des Ellipse Objekts.                             *
*                                                                              *
********************************************************************************}
procedure ClassEllipse.Neu(X:double;Y:double;Radius1:double;Radius2:double);
var neuer_record,Zeiger:p_Ellipse_record;
    hstr:string;
begin
   if DatenbankObj.Checkexists('X-KOORDINATE') = false then DatenbankObj.Insertcolumn('X-KOORDINATE',0,'N','');
   if DatenbankObj.Checkexists('Y-KOORDINATE') = false then DatenbankObj.Insertcolumn('Y-KOORDINATE',0,'N','');
   hstr:=floattostr(X); DatenbankObj.SetLength('X-KOORDINATE',length(hstr));
   hstr:=floattostr(Y); DatenbankObj.SetLength('Y-KOORDINATE',length(hstr));

   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.Radius1:=Radius1;
   neuer_record^.Radius2:=Radius2;
   neuer_record^.DBInfo:=nil;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassPline.Neu                                                    *
********************************************************************************
* Prozedur erzeugt einen neuen Pline  Eintrag.                                 *
*                                                                              *
* PARAMETER: Keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassPLine.Neu;
var neuer_record,Zeiger:p_PLine_record;
begin
   new(neuer_record);
   neuer_record^.Punkte:=nil;
   neuer_record^.DBInfo:=nil;
   neuer_record^.Labeled:=false;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.Neu                                                *
********************************************************************************
* Prozedur erzeugt einen neuen Spalteneintrag f�r die Datenbank.               *
*                                                                              *
* PARAMETER: Bezeichnung -> Spaltenbezeichnung                                 *
*            Laenge      -> L�nge der Spalte                                   *
*            Typ         -> Typ der Spalte (C oder N)                          *
*            Inhalt      -> Inhalt der Spalte                                  *
*            von         -> Vonspalte f�r das Auslesen des E00-Files           *
*            bis         -> Bisspalte f�r das Auslesen des E00-Files           *
*            Anfang      -> Zeiger auf Anfang der Liste in die eingef�gt wird  *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.Neu(Bezeichnung:string; Laenge:integer; Typ:char; Inhalt:string;var MyAnfang:p_datenbank_record);
var neuer_record,Zeiger:p_datenbank_record;
begin
   new(neuer_record);

   neuer_record^.Bezeichnung:=Bezeichnung;
   neuer_record^.Laenge:=Laenge;
   neuer_record^.Typ:=Typ;
   neuer_record^.Inhalt:=Inhalt;

   if MyAnfang = NIL then
   begin
      MyAnfang:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=MyAnfang;
      repeat
         if Zeiger^.next <> nil then Zeiger:=Zeiger^.next;
      until Zeiger^.next = nil;
      Zeiger^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
   end;
end;


{*******************************************************************************
* PROZEDUR : ClassDatenbank.SetLength                                          *
********************************************************************************
* Prozedur dient dazu die erforderliche Spaltenbreite einer Spalte zu setzen.  *
*                                                                              *
* InPARAMETER: Bezeichnung -> Name der Spalte                                  *
*              Laenge      -> Breite der Spalte                                *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.SetLength(Bezeichnung:string; Laenge:integer);
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=DB_Anfang; gefunden:=false;
   repeat
      if (Zeiger^.Bezeichnung = Bezeichnung) then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if Zeiger <> nil then
      if Zeiger^.Laenge < Laenge then Zeiger^.Laenge:=Laenge;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.GetLength                                          *
********************************************************************************
* Funktion dient zum ermitteln der erforderlichen Spaltenbreite f�r einen ADF  *
* Eintrag.                                                                     *
*                                                                              *
* PARAMETER:   Bezeichnung -> Name der Spalte                                  *
*                                                                              *
* R�ckgabewert: Spaltenbreite f�r die entsprechende Spalte.                    *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetLength(Bezeichnung:string):integer;
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=DB_Anfang; gefunden:=false;
   repeat
      if (Zeiger^.Bezeichnung = Bezeichnung) then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then Result:=Zeiger^.Laenge else Result:=0;
end;

function ClassDatenbank.GetType(Bezeichnung:string):char;
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=DB_Anfang; gefunden:=false;
   repeat
      if (Zeiger^.Bezeichnung = Bezeichnung) then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then Result:=Zeiger^.Typ else Result:='C';
end;


{*******************************************************************************
* FUNKTION : ClassDatenbank.GetItem                                            *
********************************************************************************
* Funktion dient zum Ermitteln eines Datenbankwertes.                          *
*                                                                              *
*                                                                              *
* PARAMETER:   Bezeichnung -> Name der Spalte                                  *
*                                                                              *
* R�ckgabewert: Inhalt dieser Spalte.                                          *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetItem(Bezeichnung:string):string;
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=DB_Anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then Result:=Zeiger^.Inhalt else Result:='';
end;

procedure ClassRegion.InsertPoint(X:double;Y:double);
var Zeiger:p_Punkt_record;
    new_Zeiger:p_Punkt_record;
    hstr      :string;
begin
   new(new_Zeiger);

   { Kontrolle, ob die Spalten X und Y bereits in der Datenbankliste sind }
   if DatenbankObj.Checkexists('X-KOORDINATE') = false then DatenbankObj.Insertcolumn('X-KOORDINATE',0,'N','');
   if DatenbankObj.Checkexists('Y-KOORDINATE') = false then DatenbankObj.Insertcolumn('Y-KOORDINATE',0,'N','');
   { Setzen des L�ngenfelds f�r die X und Y Koordinaten }
   hstr:=floattostr(X); DatenbankObj.SetLength('X-KOORDINATE',length(hstr));
   hstr:=floattostr(Y); DatenbankObj.SetLength('Y-KOORDINATE',length(hstr));

   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   Current^.PointsCount:=Current^.PointsCount+1;
   Zeiger:=Current^.Punkte;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current^.Punkte:=New_Zeiger;
      Current^.Punkte^.next:=NIL;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
   end;
end;

procedure ClassRegion.InsertPointToIsland(X:double;Y:double);
var iterate   :p_Punkt_record;
    new_record:p_Punkt_record;
    hstr      :string;
begin
   new(new_record);
   { Kontrolle, ob die Spalten X und Y bereits in der Datenbankliste sind }
   if DatenbankObj.Checkexists('X-KOORDINATE') = false then DatenbankObj.Insertcolumn('X-KOORDINATE',0,'N','');
   if DatenbankObj.Checkexists('Y-KOORDINATE') = false then DatenbankObj.Insertcolumn('Y-KOORDINATE',0,'N','');
   { Setzen des L�ngenfelds f�r die X und Y Koordinaten }
   hstr:=floattostr(X); DatenbankObj.SetLength('X-KOORDINATE',length(hstr));
   hstr:=floattostr(Y); DatenbankObj.SetLength('Y-KOORDINATE',length(hstr));

   new_record^.X:=X; new_record^.Y:=Y;
   iterate:=Current^.CurrentIsland^.Punkte;
   if iterate = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current^.CurrentIsland^.Punkte:=new_record;
      Current^.CurrentIsland^.Punkte^.next:=NIL;
   end
   else
   begin
      while (iterate^.next <> nil) do iterate:=iterate^.next;
      iterate^.next:=new_record;
      new_record^.next:=nil;
   end;
   Current^.CurrentIsland^.PointsCount:=Current^.CurrentIsland^.PointsCount+1;
end;

{*******************************************************************************
* PROZEDUR : ClassPline.InsertPoint                                            *
********************************************************************************
* Prozedur f�gt ein Punktpaar in die Punktliste des Current     ein.           *
*                                                                              *
* InPARAMETER: X -> X-Koordinate des neuen Eckpunktes                          *
*              Y -> Y-Koordinate des neuen Eckpunktes                          *
*                                                                              *
********************************************************************************}
procedure ClassPLine.InsertPoint(X:double;Y:double);
var Zeiger:p_Punkt_record;
    new_Zeiger:p_Punkt_record;
    hstr      :string;
begin
   new(new_Zeiger);

   { Kontrolle, ob die Spalten X und Y bereits in der Datenbankliste sind }
   if DatenbankObj.Checkexists('X-KOORDINATE') = false then DatenbankObj.Insertcolumn('X-KOORDINATE',0,'N','');
   if DatenbankObj.Checkexists('Y-KOORDINATE') = false then DatenbankObj.Insertcolumn('Y-KOORDINATE',0,'N','');
   { Setzen des L�ngenfelds f�r die X und Y Koordinaten }
   hstr:=floattostr(X); DatenbankObj.SetLength('X-KOORDINATE',length(hstr));
   hstr:=floattostr(Y); DatenbankObj.SetLength('Y-KOORDINATE',length(hstr));

   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   Zeiger:=Current^.Punkte;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current^.Punkte:=New_Zeiger;
      Current^.Punkte^.next:=NIL;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
   end;
end;

end.
