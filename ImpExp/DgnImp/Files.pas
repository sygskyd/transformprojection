unit Files;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Math, Db;

const
      SymbolScale  = 100;
      lt_Solid     = 0;
      lt_Dash      = 1;
      lt_Dot       = 2;
      lt_DashDot   = 3;
      lt_DashDDot  = 4;

      lt_UserDefined    = 1000;    { number of first user-defined linestyle }

      pt_NoPattern = 0;
      pt_FDiagonal = 1;
      pt_Cross     = 2;
      pt_DiagCross = 3;
      pt_BDiagonal = 4;
      pt_Horizontal= 5;
      pt_Vertical  = 6;
      pt_Solid     = 7;
      pt_UserDefined = 8;

      CUE_LINEARELEMENT = 0;
      CUE_CURVEELEMENT  = 1;
      CUE_BSPLINEPOLE   = 2;
      CUE_BSPLINEKNOT   = 3;

function GetLayerError:integer;stdcall;external 'dgnlayer.dll';
function SetDGNFile(filename:array of char):integer; stdcall;external 'dgnlayer.dll';
function GetNumberOfBytes:longword;stdcall;external 'dgnlayer.dll';
function OpenDGNFile:integer;stdcall;external 'dgnlayer.dll';
function ReadElement:longword;stdcall;external 'dgnlayer.dll';
function CloseDGNFile:integer;stdcall;external 'dgnlayer.dll';
function GetNrOfPolylines:integer;stdcall;external 'dgnlayer.dll';
function GetPolylineEx(iIndex:integer; var iID:longint; var iLevel:integer; var iNumCorners:integer;var iLineType:integer; var iLineColor:integer; var iLineWidth:integer; var iFillType:integer; var iFillColor:integer;var iCurved:integer):integer;stdcall;external 'dgnlayer.dll';
function GetPolylineCorner(iIndex:integer; iPoint:integer; var dX:double; var dY:double):integer;stdcall;external 'dgnlayer.dll';
function GetNrOfPolygons:integer;stdcall;external 'dgnlayer.dll';
function GetPolygon(iIndex:integer; var iID:longint; var iLevel:integer;var iNumCorners:integer;var iLineType:integer; var iLineColor:integer; var iLineWidth:integer; var iFillType:integer; var iFillColor:integer):integer;stdcall;external 'dgnlayer.dll';
function GetPolygonCorner(iIndex:integer; iPoint:integer; var dX:double; var dY:double):integer;stdcall;external 'dgnlayer.dll';
function GetNrOfPoints:integer;stdcall;external 'dgnlayer.dll';
function GetPoint(iIndex:integer; var iID:longint; var iLevel:integer; var dX:double; var dY:double; var iLineType:integer; var iLineColor:integer; var iLineWidth:integer; var iFillType:integer; var iFillColor:integer):integer;stdcall;external 'dgnlayer.dll';
function GetNrOfCircles:integer;stdcall;external 'dgnlayer.dll';
function GetCircle(iIndex:integer; var iID:longint; var iLevel:integer; var dX:double; var dY:double; var dRadius:double; var iLineType:integer; var iLineColor:integer; var iLineWidth:integer; var iFillType:integer; var iFillColor:integer):integer;stdcall;external 'dgnlayer.dll';
function GetNrOfTexts:integer;stdcall;external 'dgnlayer.dll';
function GetText(iIndex:integer; var iID:longint; var iLevel:integer; var dX:double; var dY:double; var iRefPos:integer; var psFont:pchar; var dHeight:double; var dWidth:double; var dAngle:double; var psText:pchar; var iDisplayType:integer; var iLineType:integer; var iLineColor:integer; var iLineWidth:integer; var iFillType:integer; var iFillColor:integer):integer;stdcall;external 'dgnlayer.dll';
function GetNrOfSymbolRefs:integer;stdcall;external 'dgnlayer.dll';
function GetSymbolRef(iIndexOrg:integer;var iID:longint; var iLevel:integer; var dX:double; var dY:double; var  ppsName:pchar; var dScale:double; var dRotation:double; var iLineType:integer; var iLineColor:integer; var iLineWidth:integer; var iFillType:integer; var iFillColor:integer):integer;stdcall;external 'dgnlayer.dll';
function GetNrOfArcs:integer;stdcall;external 'dgnlayer.dll';
function GetArc(iIndex:integer; var iID:longint; var iLevel:integer; var dX:double; var dY:double; var dPrimaryRay:double; var dSecondaryRay:double; var dRotation:double; var dStart:double; var dSweep:double; var iLineType:integer; var iLineColor:integer; var iLineWidth:integer; var iFillType:integer; var iFillColor:integer):integer;stdcall;external 'dgnlayer.dll';

function GetNrOfSymbolDefs:integer;stdcall;external 'dgnlayer.dll';
function GetSymbolDef(iSymIndexOrg:integer; var dX:double; var dY:double; var ppsName:pchar):integer;stdcall;external 'dgnlayer.dll';
function GetNrOfSymbolDefPoints(iSymIndex:integer):integer;stdcall;external 'dgnlayer.dll';
function GetSymbolDefPoint(iSymIndex:integer; iIndex:integer; var iID:longint; var iLevel:integer; var dX:double; var dY:double; var iLineType:integer; var iLineColor:integer; var iLineWidth:integer; var iFillType:integer; var iFillColor:integer):integer;stdcall;external 'dgnlayer.dll';
function GetNrOfSymbolDefPolylines(iSymIndex:integer):integer;stdcall;external 'dgnlayer.dll';
function GetSymbolDefPolyline(iSymIndex:integer;iIndex:integer; var iID:longint; var iLevel:integer; var iNumCorners:integer;var iLineType:integer; var iLineColor:integer; var iLineWidth:integer; var iFillType:integer; var iFillColor:integer):integer;stdcall;external 'dgnlayer.dll';
function GetSymbolDefPolylineCorner(iSymIndexOrg:integer; iIndex:integer; iPoint:integer; var dX:double; var dY:double):integer;stdcall;external 'dgnlayer.dll';
function GetNrOfSymbolDefPolygons(iSymIndex:integer):integer;stdcall;external 'dgnlayer.dll';
function GetSymbolDefPolygon(iSymIndex:integer;iIndex:integer; var iID:longint; var iLevel:integer; var iNumCorners:integer;var iLineType:integer; var iLineColor:integer; var iLineWidth:integer; var iFillType:integer; var iFillColor:integer):integer;stdcall;external 'dgnlayer.dll';
function GetSymbolDefPolygonCorner(iSymIndexOrg:integer; iIndex:integer; iPoint:integer; var dX:double; var dY:double):integer;stdcall;external 'dgnlayer.dll';
function GetNrOfSymbolDefTexts(iSymIndex:integer):integer;stdcall;external 'dgnlayer.dll';
function GetSymbolDefText(iSymIndex:integer;iIndex:integer; var iID:longint; var iLevel:integer; var dX:double; var dY:double; var iRefPos:integer; var psFont:pchar; var dHeight:double; var dWidth:double; var dAngle:double; var psText:pchar; var iDisplayType:integer; var iLineType:integer; var iLineColor:integer; var iLineWidth:integer; var iFillType:integer; var iFillColor:integer):integer;stdcall;external 'dgnlayer.dll';
function GetNrOfSymbolDefArcs(iSymIndex:integer):integer;stdcall;external 'dgnlayer.dll';
function GetSymbolDefArc(iSymIndexOrg:integer; iIndex:integer; var iID:longint; var iLevel:integer; var dX:double; var dY:double; var dPrimaryRay:double; var dSecondaryRay:double; var dRotation:double; var dStart:double; var dSweep:double; var iLineType:integer; var iLineColor:integer; var iLineWidth:integer; var iFillType:integer; var iFillColor:integer):integer;stdcall;external 'dgnlayer.dll';
function FreeDGNMemory:integer;stdcall;external 'dgnlayer.dll';

type
   ClassFile = class
   public
      // public declarations
      constructor Create;
      procedure Destroy;
      procedure SetDgnFilename(Filename:string);
      function GetNrOfObjectsToRead:longint;
      function GetNrOfPolylinesToRead:longint;
      function GetNrOfPolygonsToRead:longint;
      function GetNrOfPointsToRead:longint;
      function GetNrOfCirclesToRead:longint;
      function GetNrOfTextsToRead:longint;
      function GetNrOfSymbolRefsToRead:longint;
      function GetNrOfArcsToRead:longint;
      procedure OpenFile;
      procedure CloseAllFiles;
      function  ReadDgnElement:longint;
      function ImportPolyline(issymbol:boolean):longint;
      function ImportPolygon(issymbol:boolean):longint;
      function ImportPoint(issymbol:boolean):longint;
      function ImportCircle:longint;
      function ImportText(issymbol:boolean):longint;
      function ImportSymbol:longint;
      function ImportArc(issymbol:boolean):longint;
      procedure ReadSymbolLibrary;
   private
      // private functions
      objectidx:integer;
      symbolidx:integer;
end;
implementation
uses MainDlg,SetdgnfileDlg;
constructor ClassFile.Create;
begin
   objectidx:=0; // index to iterate through the objects
end;

procedure ClassFile.Destroy;
begin
end;

procedure ClassFile.SetDgnfilename(Filename:string);
var hfilename:array [0..255] of char;
begin
   strpcopy(@hfilename,Filename);
   SETDGNFILE(hfilename);
   MainWnd.Lasterror:=GETLAYERERROR;
   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;
end;

function ClassFile.GetNrOfObjectsToRead:longint;
var retVal:longword;
begin
   retVal:=GetNumberOfBytes;
   result:=retVal;
   MainWnd.Lasterror:=GetLayerError;
   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;
end;

procedure ClassFile.OpenFile;
begin
   MainWnd.Lasterror:=OpenDGNFile;
   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;
end;

procedure ClassFile.CloseallFiles;
begin
   // close file
   CloseDGNFile;
   // release memory
   FreeDGNMemory;
end;

function ClassFile.ReadDgnElement:longint;
var anz:longword;
begin
   anz:=ReadElement();
   MainWnd.Lasterror:=GetLayerError;
   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;
   Result:=anz;
end;

procedure ClassFile.ReadSymbolLibrary;
var numofsymboldefs:integer;
    symdefidx:integer;
    X,Y:double;
    psymdefname:pchar;
    symdefname:string;
    numobjects:integer;
    i:integer;
begin
   numofsymboldefs:=GetNrOfSymbolDefs;
   MainWnd.Lasterror:=GetLayerError;
   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;
   for symdefidx:=0 to numofsymboldefs-1 do
   begin
      // get symboldef information
      GetSymbolDef(symdefidx,X,Y,psymdefname);

      MainWnd.Lasterror:=GetLayerError;
      if MainWnd.Lasterror <> 0 then
         MainWnd.HandleError;
      symdefname:=strpas(psymdefname);
      MainWnd.CreateSymbolDef(symdefname,X,Y);

      symbolidx:=symdefidx;

      // create point elements of symbol
      numobjects:=GetNrOfSymbolDefPoints(symdefidx);
      objectidx:=0;
      for i:=0 to numobjects-1 do
      begin
         ImportPoint(TRUE);
      end;

      // create polyline elements of symbol
      numobjects:=GetNrOfSymbolDefPolylines(symdefidx);
      objectidx:=0;
      for i:=0 to numobjects-1 do
      begin
         ImportPolyline(TRUE);
      end;

      // create polygon elements of symbol
      numobjects:=GetNrOfSymbolDefPolygons(symdefidx);
      objectidx:=0;
      for i:=0 to numobjects-1 do
      begin
         ImportPolygon(TRUE);
      end;

      // create text elements of symbol
      numobjects:=GetNrOfSymbolDefTexts(symdefidx);
      objectidx:=0;
      for i:=0 to numobjects-1 do
      begin
         ImportText(TRUE);
      end;

      // create arc elements of symbol
      numobjects:=GetNrOfSymbolDefArcs(symdefidx);
      objectidx:=0;
      for i:=0 to numobjects-1 do
      begin
         ImportArc(TRUE);
      end;

      // insert symboldef to project
      MainWnd.EndSymbolDef;
   end;
end;

function ClassFile.GetNrOfPolylinesToRead:longint;
begin
   result:=GetNrOfPolylines;
   MainWnd.Lasterror:=GetLayerError;
   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;
   objectidx:=0;
end;

function ClassFile.GetNrOfPolygonsToRead:longint;
begin
   result:=GetNrOfPolygons;
   MainWnd.Lasterror:=GetLayerError;
   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;
   objectidx:=0;
end;

function ClassFile.GetNrOfPointsToRead:longint;
begin
   result:=GetNrOfPoints;
   MainWnd.Lasterror:=GetLayerError;
   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;
   objectidx:=0;
end;

function ClassFile.GetNrOfCirclesToRead:longint;
begin
   result:=GetNrOfCircles;
   MainWnd.Lasterror:=GetLayerError;
   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;
   objectidx:=0;
end;

function ClassFile.GetNrOfTextsToRead:longint;
begin
   result:=GetNrOfTexts;
   MainWnd.Lasterror:=GetLayerError;
   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;
   objectidx:=0;
end;

function ClassFile.GetNrOfSymbolRefsToRead:longint;
begin
   result:=GetNrOfSymbolRefs;
   MainWnd.Lasterror:=GetLayerError;
   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;
   objectidx:=0;
end;

function ClassFile.GetNrOfArcsToRead:longint;
begin
   result:=GetNrOfArcs;
   MainWnd.Lasterror:=GetLayerError;
   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;
   objectidx:=0;
end;

function ClassFile.ImportPolyline(issymbol:boolean):longint;
var objid:longint;
    numcorners:integer;
    linetype:integer;
    linecolor:integer;
    linewidth:integer;
    filltype:integer;
    fillcolor:integer;
    X:double;
    Y:double;
    i:integer;
    level:integer;
    curvetype:integer;
    LevelAsStr:string;
    new_id:longint;
    FirstX,FirstY:double;
begin
   numcorners:=0;
   X:=0;
   Y:=0;

   if not issymbol then
      GetPolylineEx(objectidx,objid,level,numcorners,linetype,linecolor,linewidth, filltype,fillcolor, curvetype)
   else
   begin
      // symbol only contains normal polys
      GetSymbolDefPolyline(symbolidx,objectidx,objid,level,numcorners,linetype,linecolor,linewidth, filltype,fillcolor);
      curvetype:=CUE_LINEARELEMENT;
   end;

   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;

   LevelAsStr:=inttostr(level);

   if not issymbol then
   begin
      // check if layer exists
      if not MainWnd.CheckLayerExists(LevelAsStr) then
         MainWnd.CreateLayer(LevelAsStr);
   end;

   // change black and white
   if linecolor = 16777215 then linecolor:=0
   else if linecolor = 0 then linecolor:=16777215;
   if fillcolor = 16777215 then fillcolor:=0
   else if fillcolor = 0 then fillcolor:=16777215;

   if (curvetype = CUE_LINEARELEMENT) then // create normal polyline
      MainWnd.CreatePolyline(linecolor, linetype,issymbol)
   else
      MainWnd.CreateSpline(linecolor, linetype,issymbol);

   for i:=0 to numcorners-1 do
   begin
      // now the corners will be read out
      if not issymbol then
         GetPolylineCorner(objectidx,i,X,Y)
      else
      begin
         GetSymbolDefPolylineCorner(symbolidx, objectidx, i, X, Y);
         X:=X*SymbolScale;
         Y:=Y*SymbolScale;
         if i = 0 then
         begin
            FirstX:=X;
            FirstY:=Y;
         end;
         if (numcorners = 2) and (i=1) then
         begin
            if X=FirstX then X:=X + 0.01;
            if Y=FirstY then Y:=Y + 0.01;
         end;
      end;
      if MainWnd.Lasterror <> 0 then
         MainWnd.HandleError;

      if (curvetype = CUE_LINEARELEMENT) then // insert point to normal polyline
         MainWnd.InsertPointToPolyLine(X,Y,issymbol)
      else
         MainWnd.InsertPointToSpline(X,Y,issymbol)
   end;
   if (curvetype = CUE_LINEARELEMENT) then
      new_id:=MainWnd.ClosePolyline(issymbol)
   else
      new_id:=MainWnd.CloseSpline(issymbol);
   objectidx:=objectidx+1;
   result:=1;
end;

function ClassFile.ImportPoint(issymbol:boolean):longint;
var objid:longint;
    linetype:integer;
    linecolor:integer;
    linewidth:integer;
    filltype:integer;
    fillcolor:integer;
    X:double;
    Y:double;
    i:integer;
    level:integer;
    LevelAsStr:string;
    new_id:longint;
begin
   X:=0;
   Y:=0;

   if (not issymbol) then
      GetPoint(objectidx, objid, level, X, Y, linetype,linecolor,linewidth, filltype,fillcolor)
   else
   begin
      GetSymbolDefPoint(symbolidx,objectidx, objid, level, X, Y, linetype,linecolor,linewidth, filltype,fillcolor);
      X:=X*SymbolScale;
      Y:=Y*SymbolScale;
   end;

   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;

   LevelAsStr:=inttostr(level);

   if not issymbol then
   begin
      // check if layer exists
      if not MainWnd.CheckLayerExists(LevelAsStr) then
         MainWnd.CreateLayer(LevelAsStr);
   end;

   // change black and white
   if linecolor = 16777215 then linecolor:=0
   else if linecolor = 0 then linecolor:=16777215;
   if fillcolor = 16777215 then fillcolor:=0
   else if fillcolor = 0 then fillcolor:=16777215;

   new_id:=MainWnd.CreatePoint(X,Y,linecolor, linetype, issymbol);

   objectidx:=objectidx+1;
   result:=1;
end;

function ClassFile.ImportSymbol:longint;
var objid:longint;
    linetype:integer;
    linecolor:integer;
    linewidth:integer;
    filltype:integer;
    fillcolor:integer;
    X:double;
    Y:double;
    level:integer;
    LevelAsStr:string;
    new_id:longint;
    psymname:pchar;
    symname:string;
    symsize:double;
    symrotation:double;
begin
   GetSymbolRef(objectidx,objid,level,X,Y,psymname,symsize,symrotation, linetype,linecolor,linewidth, filltype,fillcolor);
   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;

   symname:=strpas(psymname);
   LevelAsStr:=inttostr(level);

      // check if layer exists
   if not MainWnd.CheckLayerExists(LevelAsStr) then
      MainWnd.CreateLayer(LevelAsStr);

   // change black and white
   if linecolor = 16777215 then linecolor:=0
   else if linecolor = 0 then linecolor:=16777215;
   if fillcolor = 16777215 then fillcolor:=0
   else if fillcolor = 0 then fillcolor:=16777215;

   // create symbol
   new_id:=MainWnd.InsertSymbol(X,Y,symname,symsize,symrotation,linecolor, linetype,fillcolor,filltype);

   objectidx:=objectidx+1;
   result:=1;
end;

function ClassFile.ImportCircle:longint;
var objid:longint;
    linetype:integer;
    linecolor:integer;
    linewidth:integer;
    filltype:integer;
    fillcolor:integer;
    X:double;
    Y:double;
    Radius:double;
    level:integer;
    LevelAsStr:string;
    new_id:longint;
begin
   X:=0;
   Y:=0;

   GetCircle(objectidx, objid, level, X, Y, radius, linetype,linecolor,linewidth, filltype,fillcolor);

   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;

   LevelAsStr:=inttostr(level);

   // check if layer exists
   if not MainWnd.CheckLayerExists(LevelAsStr) then
      MainWnd.CreateLayer(LevelAsStr);

   // change black and white
   if linecolor = 16777215 then linecolor:=0
   else if linecolor = 0 then linecolor:=16777215;
   if fillcolor = 16777215 then fillcolor:=0
   else if fillcolor = 0 then fillcolor:=16777215;

   new_id:=MainWnd.CreateCircle(X,Y,Radius,linecolor, linetype,fillcolor,filltype);

   objectidx:=objectidx+1;
   result:=1;
end;

function ClassFile.ImportArc(issymbol:boolean):longint;
var objid:longint;
    linetype:integer;
    linecolor:integer;
    linewidth:integer;
    filltype:integer;
    fillcolor:integer;
    X:double;
    Y:double;
    level:integer;
    PrimaryRay:double;
    SecondaryRay:double;
    Rotation:double;
    Start:double;
    Sweep:double;
    LevelAsStr:string;
    new_id:integer;
begin
   if not issymbol then
      GetArc(objectidx, objid, level, X, Y, PrimaryRay, SecondaryRay, Rotation, Start,  Sweep,  linetype, linecolor, linewidth, filltype, fillcolor)
   else
   begin
      GetSymbolDefArc(symbolidx,objectidx, objid, level, X, Y, PrimaryRay, SecondaryRay, Rotation, Start,  Sweep,  linetype, linecolor, linewidth, filltype, fillcolor);
      X:=X*SymbolScale;
      Y:=Y*SymbolScale;
      PrimaryRay:=PrimaryRay*SymbolScale;
      SecondaryRay:=SecondaryRay*SymbolScale;
   end;

   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;

   if not issymbol then
   begin
      LevelAsStr:=inttostr(level);
      // check if layer exists
      if not MainWnd.CheckLayerExists(LevelAsStr) then
         MainWnd.CreateLayer(LevelAsStr);
   end;

   // change black and white
   if linecolor = 16777215 then linecolor:=0
   else if linecolor = 0 then linecolor:=16777215;
   if fillcolor = 16777215 then fillcolor:=0
   else if fillcolor = 0 then fillcolor:=16777215;

   new_id:=MainWnd.CreateArc(X, Y, PrimaryRay, SecondaryRay, Rotation, Start, Sweep, linetype, linecolor, filltype, fillcolor,issymbol);

   objectidx:=objectidx+1;
   result:=1;
end;

function ClassFile.ImportText(issymbol:boolean):longint;
var objid:longint;
    linetype:integer;
    linecolor:integer;
    linewidth:integer;
    filltype:integer;
    fillcolor:integer;
    X:double;
    Y:double;
    level:integer;
    refpos:integer;
    displaytype:integer;
    fontheight:double;
    fontwidth:double;
    fontangle:double;
    LevelAsStr:string;
    new_id:longint;
    pfont:pchar;
    ptext:pchar;
    font:string;
    text:string;
begin
   X:=0;
   Y:=0;

   if not issymbol then
      GetText(objectidx, objid, level, X, Y, refpos, pfont, fontheight,  fontwidth, fontangle, ptext, displaytype, linetype, linecolor, linewidth, filltype, fillcolor)
   else
   begin
      GetSymbolDefText(symbolidx,objectidx, objid, level, X, Y, refpos, pfont, fontheight,  fontwidth, fontangle, ptext, displaytype, linetype, linecolor, linewidth, filltype, fillcolor);
      X:=X*SymbolScale;
      Y:=Y*SymbolScale;
   end;

   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;

   font:=strpas(pfont);
   text:=strpas(ptext);


   if not issymbol then
   begin
      LevelAsStr:=inttostr(level);
      // check if layer exists
      if not MainWnd.CheckLayerExists(LevelAsStr) then
         MainWnd.CreateLayer(LevelAsStr);
   end;

   // change black and white
   if linecolor = 16777215 then linecolor:=0
   else if linecolor = 0 then linecolor:=16777215;
   if fillcolor = 16777215 then fillcolor:=0
   else if fillcolor = 0 then fillcolor:=16777215;

   new_id:=MainWnd.CreateText(X,Y,Text,Font,fontheight,fontangle,displaytype, linetype, linecolor, filltype, fillcolor,issymbol);

   objectidx:=objectidx+1;
   result:=1;
end;

function ClassFile.ImportPolygon(issymbol:boolean):longint;
var objid:longint;
    numcorners:integer;
    linetype:integer;
    linecolor:integer;
    linewidth:integer;
    filltype:integer;
    fillcolor:integer;
    X:double;
    Y:double;
    i:integer;
    level:integer;
    LevelAsStr:string;
    new_id:longint;
begin
   numcorners:=0;
   X:=0;
   Y:=0;
   // the level-information is not implemented yet
   // so level-0 will be set as default
   if not issymbol then
      GetPolygon(objectidx,objid,level,numcorners,linetype,linecolor,linewidth, filltype,fillcolor)
   else
      GetSymbolDefPolygon(symbolidx,objectidx,objid,level,numcorners,linetype,linecolor,linewidth, filltype,fillcolor);

   if MainWnd.Lasterror <> 0 then
      MainWnd.HandleError;

   if not issymbol then
   begin
      LevelAsStr:=inttostr(level);
      // check if layer exists
      if not MainWnd.CheckLayerExists(LevelAsStr) then
         MainWnd.CreateLayer(LevelAsStr);
   end;

   // change black and white
   if linecolor = 16777215 then linecolor:=0
   else if linecolor = 0 then linecolor:=16777215;
   if fillcolor = 16777215 then fillcolor:=0
   else if fillcolor = 0 then fillcolor:=16777215;

   MainWnd.CreatePolygon(linecolor, linetype, fillcolor, filltype,issymbol);

   // now the corners will be read out
   for i:=0 to numcorners-1 do
   begin
      if not issymbol then
         GetPolygonCorner(objectidx,i,X,Y)
      else
      begin
         GetSymbolDefPolygonCorner(symbolidx, objectidx, i, X, Y);
         X:=X*SymbolScale;
         Y:=Y*SymbolScale;
      end;

      MainWnd.InsertPointToPolygon(X,Y,issymbol);
      if MainWnd.Lasterror <> 0 then
         MainWnd.HandleError;
   end;
   new_id:=MainWnd.ClosePolygon(issymbol);
   objectidx:=objectidx+1;
   result:=1;
end;

end.
