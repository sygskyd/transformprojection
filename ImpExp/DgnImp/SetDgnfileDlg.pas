unit SetDgnfileDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, FileCtrl, ComCtrls, StrFile, Db, ADODB,
  ShellCtrls, FileSelectionPanel, inifiles, WinOSInfo;

type
  TSetDgnfileWnd = class(TForm)
    PageControl: TPageControl;
    TabSheetFiles: TTabSheet;
    OKBtn: TButton;
    CancelBtn: TButton;
    SelectivCB: TCheckBox;
    GeoBtn: TButton;
    SetupWindowTimer: TTimer;
    DbBtn: TButton;
    Panel1: TPanel;
    TargetDbLabel: TLabel;
    FileSelectionPanel: TFileSelectionPanel;
    ShellTreeView: TShellTreeView;
    FileListBox: TFileListBox;
    ListBox: TListBox;
    TrashPanel: TPanel;
    TrashImage: TImage;
    AddAllBtn: TButton;
    DeleteAllBtn: TButton;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GeoBtnClick(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure FileSelectionPanelFilesToConvertChanged(Sender: TObject);
  private
    { Private-Deklarationen}
    StrFileObj :ClassStrfile;
    firstactivate:boolean;
    CurrentFileIdx:integer;
    procedure CheckWindow;
  public
    { Public-Deklarationen}
    winheight:integer;
    winwidth :integer;
    wintop   :integer;
    winleft  :integer;

    function  GetFilename(var Dateiname:string):boolean;
    procedure CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
    procedure SetupForBatch;
    procedure AddImpFile(aFilename:string);
  end;

var
  SetDgnfileWnd: TSetDgnfileWnd;

implementation

uses MainDlg;

{$R *.DFM}

function TSetDgnfileWnd.GetFilename(var Dateiname:string):boolean;
begin
   if CurrentFileIdx < FileSelectionPanel.NumFilesToConvert then
   begin
      Dateiname:=FileSelectionPanel.GetFilename(CurrentFileIdx);
      Result:=true;
   end
   else
      Result:=false;
   CurrentFileIdx:=CurrentFileIdx+1;
end;

procedure TSetDgnfileWnd.FormCreate(Sender: TObject);
var myScale:double;
    OldWidth:integer;
    OldHeight:integer;
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];

   Self.Caption:=StrFileObj.Assign(1);             // INTERGRAPH DGN-Import 2000
   OKBtn.Caption:=StrFileObj.Assign(4);            // Ok
   CancelBtn.Caption:=StrFileObj.Assign(5);        // Cancel
   SelectivCB.Caption:=StrFileObj.Assign(6);       // convert selective

   TabSheetFiles.Caption:=StrFileObj.Assign(7);    // Files
   GeoBtn.Caption:=StrFileObj.Assign(12);          // Projection
   DbBtn.Caption:=StrFileObj.Assign(8);            // Database

   Firstactivate:=true;
   CurrentFileIdx:=0;

   OldWidth:=Self.Width;
   OldHeight:=Self.Height;

   Self.Height:=1;
   Self.Width:=1;

   myScale:=1; //100000 / Trunc(9600000/(Self.PixelsPerInch));
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);

   // now adapt winheigt and winwidth
   winheight:=round(OldHeight*myScale);
   winwidth:=round(OldWidth*myScale);
end;

procedure TSetDgnfileWnd.CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
begin
   wintop:=(WHEIGHT div 2) - (winheight div 2);
   winleft:=(WWIDTH div 2) - (winwidth div 2);
end;

procedure TSetDgnfileWnd.OKBtnClick(Sender: TObject);
var inifile:TIniFile;
begin
   MainWnd.ExitNormal:=true;
   // write settings to .ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir + 'ConGIS.ini');
   inifile.WriteString('DGNIMP','LastUsedPath',FileSelectionPanel.Directory);
   inifile.Free;
   Self.Close;
   MainWnd.Show;
end;

procedure TSetDgnfileWnd.CancelBtnClick(Sender: TObject);
begin
   MainWnd.ExitNormal:=false;
   MainWnd.CloseTimer.Enabled:=true;
end;

procedure TSetDgnfileWnd.CheckWindow;
var allok:boolean;
begin
   allok:=true;
   if FileSelectionPanel.NumFilesToConvert = 0 then allok:=false;
   if allok then OkBtn.Enabled:=true
   else OkBtn.Enabled:=false;
end;

procedure TSetDgnfileWnd.FormActivate(Sender: TObject);
begin
   if firstactivate then
   begin
      MainWnd.AxGisProjection.WorkingPath:=OSInfo.WingisDir;
      MainWnd.AxGisProjection.LngCode:=MainWnd.LngCode;
      MainWnd.AXImpExpDbc.WorkingDir:=OSInfo.WingisDir;
      MainWnd.AXImpExpDbc.LngCode:=MainWnd.LngCode;
      TargetDbLabel.Caption:=MainWnd.AXImpExpDbc.ImpDbInfo;
      MainWnd.SetProjectionSettings;
      firstactivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TSetDgnfileWnd.GeoBtnClick(Sender: TObject);
begin
   MainWnd.AxGisProjection.SetupByDialog;
end;

procedure TSetDgnfileWnd.SetupWindowTimerTimer(Sender: TObject);
var curdir:string;
    inifile:TIniFile;
    dummy:string;
begin
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,winleft,wintop,winwidth, winheight,SWP_SHOWWINDOW);
   Self.Height:=winheight;
   Self.Width:=winwidth;
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'ConGIS.ini');
   dummy:=inifile.ReadString('DGNIMP','LastUsedPath','');
   inifile.Free;
   if dummy <> '' then
      CurDir:=dummy
   else
      GetDir(0, CurDir);

   FileSelectionPanel.Directory:=CurDir;
   Self.Repaint;
   SetupWindowTimer.Enabled:=false;
end;

// procedure is used to setup the SetDgnfileWnd for batch mode
procedure TSetDgnfileWnd.SetupForBatch;
begin
   if FirstActivate then
   begin
      MainWnd.AxGisProjection.WorkingPath:=OSInfo.WingisDir;
      MainWnd.AxGisProjection.LngCode:=MainWnd.LngCode;
      MainWnd.AXImpExpDbc.WorkingDir:=OSInfo.WingisDir;
      MainWnd.AXImpExpDbc.LngCode:=MainWnd.LngCode;
      TargetDbLabel.Caption:=MainWnd.AXImpExpDbc.ImpDbInfo;
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
   end;
end;

procedure TSetDgnfileWnd.AddImpFile(aFilename:string);
begin
   FileSelectionPanel.AddFileToConvert(aFilename);
end;

procedure TSetDgnfileWnd.FileSelectionPanelFilesToConvertChanged(
  Sender: TObject);
begin
   CheckWindow;
end;

end.
