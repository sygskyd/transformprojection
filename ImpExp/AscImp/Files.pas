unit Files;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Math, Struct, SetImgDlg;

type
  { Hier k�nnen Strukturen definiert werden }
  p_double = ^doublewert;
  doublewert = record
     Zahl :array [0..7] of byte;
  end;

  p_integer = ^integerwert;
  integerwert = record
     Zahl:array [0..3] of byte;
  end;


ClassFile = class
   public
      { public Definitionen }
      constructor Create;
      destructor Destroy;
      procedure OpenASCFile(Dateiname:string);
      procedure CloseAllFiles;
      function  ReadElement:longint;
      procedure ScanADF(Dateiname:string);
      procedure ScanSYM(Dateiname:string);
      procedure ValASC(Zeile:string;var X:double;var Y:double;var PAR_A:string; var PAR_B:string; var PAR_C:string; var Layername:string;var ObjTyp:integer; var ObjNr:integer;var Grafik:string);
      function GetFilesize(Dateiname:string):longint;
      function GetMaxLayers:longint;
      function GetMaxObjects:longint;
      function GetCurrentLayername:string;
      function WriteCurrentLayer:longint;
      procedure CreateDatabase(Layer:string);
   private
      { private Definitionen }
end;
implementation
uses MainDlg;

var  AscDatei       :Textfile;

     AreaObj        :ClassArea;
     PointObj       :ClassPoint;
     LineObj        :ClassLine;
     SplineObj      :ClassSpline;
     LayerObj       :ClassLayer;
     TextObj        :ClassText;
     CircleObj      :ClassCircle;
     SymRefObj      :ClassSymref;
     SymbolObj      :ClassSymbol;
     ArcObj         :ClassArc;
     ImageObj       :ClassImage;
     DatenbankObj   :ClassDatenbank;

     MIFDirectory   :string;
     AktObjektnummer:integer;
     AktObjekttyp   :integer;

constructor ClassFile.Create;
begin
   DatenbankObj:=ClassDatenbank.Create;
   AreaObj:=ClassArea.Create;
   PointObj:=ClassPoint.Create;
   LineObj:=ClassLine.Create;
   SplineObj:=ClassSpline.Create;
   TextObj:=ClassText.Create;
   CircleObj:=ClassCircle.Create;
   ArcObj:=ClassArc.Create;
   LayerObj:=ClassLayer.Create;
   SymRefObj:=ClassSymref.Create;
   SymbolObj:=ClassSymbol.Create;
   ImageObj:=ClassImage.Create;
end;

destructor ClassFile.Destroy;
begin
   DatenbankObj.Destroy;
   AreaObj.Destroy;
   PointObj.Destroy;
   LineObj.Destroy;
   SplineObj.Destroy;
   TextObj.Destroy;
   CircleObj.Destroy;
   ArcObj.Destroy;
   LayerObj.Destroy;
   SymRefObj.Destroy;
   SymbolObj.Destroy;
   ImageObj.Destroy;
end;

{*******************************************************************************
* PROZEDUR : OpenASCFile                                                       *
********************************************************************************
* Prozedur dient zum Erzeugen des ASC-Files.                                   *
*                                                                              *
* PARAMETER: Dateiname -> Dateiname des ASC-Files.                             *
*                                                                              *
********************************************************************************}
procedure ClassFile.OpenASCFile(Dateiname:string);
begin
   Filemode:=0;
   {$I-}
   Assignfile(AscDatei,Dateiname);
   Reset(AscDatei);
   {$I+}
   AktObjektnummer:=0; AktObjekttyp:=0;
end;

{*******************************************************************************
* PROZEDUR : CloseallFiles                                                     *
********************************************************************************
* Prozedur dient zum Schliessen aller Files.                                   *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassFile.CloseallFiles;
begin
   {$i-}
   Closefile(AscDatei); 
   {$i+}
end;

{*******************************************************************************
* FUNKTION : GetFilesize                                                       *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Bytes des       *
* ASC-Files                                                                    *
*                                                                              *
* PARAMETER: Dateiname -> Dateiname von der die Bytegr��e ermittelt werden soll*
*                                                                              *
* R�ckgabewert: Anzahl der zu Bearbeitenden Bytes.                             *
********************************************************************************}
function ClassFile.GetFilesize(Dateiname:string):longint;
var Datei:file of byte;
begin
   Filemode:=0;
   {$I-}
   AssignFile(Datei, Dateiname);
   Reset(Datei);
   Result:=FileSize(Datei);
   Closefile(Datei);
   {$I+}
end;

{*******************************************************************************
* FUNKTION : GetMaxLayers                                                      *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Layer           *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden Layer.                             *
********************************************************************************}
function ClassFile.GetMaxObjects:longint;
var retVal:longint;
begin
   retVal:=0;
   retVal:=retVal + AreaObj.GetAnzahl;
   retVal:=retVal + LineObj.GetAnzahl;
   retVal:=retVal + PointObj.GetAnzahl;
   retVal:=retVal + TextObj.GetAnzahl;
   retVal:=retVal + CircleObj.GetAnzahl;
   retVal:=retVal + SymbolObj.GetAnzahl;
   retVal:=retVal + ArcObj.GetAnzahl;
   retVal:=retVal + SplineObj.GetAnzahl;
   retVal:=retVal + ImageObj.GetAnzahl;
   result:=retVal;
end;

function ClassFile.GetMaxLayers:longint;
begin
   LayerObj.SetToLastLayer;
   result:=LayerObj.GetAnzahl;
end;

{*******************************************************************************
* FUNKTION : GetCurrentLayername                                               *
********************************************************************************
* Funktion dient zum Ermitteln des Namens des aktuellen Layers.                *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Name des aktuell bearbeiteten Layers.                          *
********************************************************************************}
function ClassFile.GetCurrentLayername:string;
begin
   Result:=LayerObj.GetCurrentLayername;
end;

procedure ClassFile.CreateDatabase(Layer:string);
begin
   DatenbankObj.WriteDatabasecolumns(Layer);
end;

{*******************************************************************************
* FUNKTION : WriteCurrentLayer                                                 *
********************************************************************************
* Funktion dient zum schreiben der Daten des aktuellen Layers.                 *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Layer.                                 *
********************************************************************************}
function ClassFile.WriteCurrentLayer:longint;
begin
   { Nun werden die Fl�cheneintr�ge f�r diesen Layer geschrieben }
   AreaObj.Writedata(LayerObj.GetCurrentLayername);
   { Nun werden die Linieneintr�ge f�r diesen Layer geschrieben }
   LineObj.Writedata(LayerObj.GetCurrentLayername);
   { Nun werden die Punkteintr�ge f�r diesen Layer geschrieben }
   PointObj.Writedata(LayerObj.GetCurrentLayername);
   { Nun werden die Texteintr�ge f�r diesen Layer geschrieben }
   TextObj.WriteData(LayerObj.GetCurrentLayername);
   { Nun werden die Kreiseintr�ge f�r diesen Layer geschrieben }
   CircleObj.WriteData(LayerObj.GetCurrentLayername);
   { Nun werden die Symboleintr�ge f�r diesen Layer geschrieben }
   SymbolObj.WriteData(LayerObj.GetCurrentLayername);
   { Nun werden die Arceintr�ge f�r diesen Layer geschrieben }
   ArcObj.WriteData(LayerObj.GetCurrentLayername);
   { Nun werden die Splineeintr�ge f�r diesen Layer geschrieben }
   SplineObj.WriteData(LayerObj.GetCurrentLayername);
   { Nun werden die Imageeintr�ge f�r diesen Layer geschrieben }
   ImageObj.WriteData(LayerObj.GetCurrentLayername);

   MainWnd.AXImpExpDbc.CloseImpTables;

   { Nun wird weiter zum n�chsten Layer gegangen }
   LayerObj.SetCurrentprevious;

   Result:=1;
end;

{*******************************************************************************
* PROZEDUR : ScanSYM                                                           *
********************************************************************************
* Prozedur dient zum Abtasten des -Files.                                   *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure Classfile.ScanSym(Dateiname:string);
var Datei:Textfile;
    Zeile:string;
    hstr:string;
    SymNr_Gis:longint;
    SymNr_Sym:longint;
    count:integer;
begin
   Filemode:=0;
   {$I-}
   Assignfile(Datei,Dateiname);
   Reset(Datei);
   repeat
      readln(Datei,Zeile);
      hstr:=copy(Zeile,1,10);
      val(hstr,SymNr_Gis,count); if count <> 0 then SymNr_Gis:=0;
      hstr:=copy(zeile,12,10);
      val(hstr,SymNr_Sym,count); if count <> 0 then SymNr_Sym:=0;
      SymRefObj.Neu(SymNr_Gis,SymNr_sym);
   until EOF(Datei);
   Closefile(Datei);
   {$I+}
end;

{*******************************************************************************
* PROZEDUR : ScanADF                                                           *
********************************************************************************
* Prozedur dient zum Abtasten des ADF-Files.                                   *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassFile.ScanADF(Dateiname:string);
var Datei:Textfile;
    von, bis, count:integer;
    vonstr, bisstr :string;
    Database:boolean;
    DBSpalte,Zeile:string;
begin
   Database:=false;
   Filemode:=0;
   {$I-}
   Assignfile(Datei,Dateiname);
   Reset(Datei);
   repeat
      readln(Datei,Zeile); Zeile:=uppercase(Zeile);
      if (Zeile = 'X-KOORDINATE') or (Zeile = 'X-COORDINATE') then { Einlesen der X-Koordinate }
      begin
         readln(Datei,Zeile);
         vonstr:=copy(Zeile,1,3); bisstr:=copy(Zeile,5,3);
         val(vonstr,von,count); val(bisstr,bis,count);
         DatenbankObj.Insertcolumn('X-KOORDINATE',0,'N','',von,bis,FALSE);
      end;

      if (Zeile = 'Y-KOORDINATE') or (Zeile ='Y-COORDINATE') then { Einlesen der Y-Koordinate }
      begin
         readln(Datei,Zeile);
         vonstr:=copy(Zeile,1,3); bisstr:=copy(Zeile,5,3);
         val(vonstr,von,count); val(bisstr,bis,count);
         DatenbankObj.Insertcolumn('Y-KOORDINATE',0,'N','',von,bis,FALSE);
      end;

      if (Zeile = 'PARAMETERA') then { Einlesen der Paramter A }
      begin
         readln(Datei,Zeile);
         vonstr:=copy(Zeile,1,3); bisstr:=copy(Zeile,5,3);
         val(vonstr,von,count); val(bisstr,bis,count);
         DatenbankObj.Insertcolumn('PARAMETERA',0,'C','',von,bis,FALSE);
      end;

      if (Zeile = 'PARAMETERB') then { Einlesen der Paramter B }
      begin
         readln(Datei,Zeile);
         vonstr:=copy(Zeile,1,3); bisstr:=copy(Zeile,5,3);
         val(vonstr,von,count); val(bisstr,bis,count);
         DatenbankObj.Insertcolumn('PARAMETERB',0,'C','',von,bis,FALSE);
      end;

      if (Zeile = 'PARAMETERC') then { Einlesen der Paramter C }
      begin
         readln(Datei,Zeile);
         vonstr:=copy(Zeile,1,3); bisstr:=copy(Zeile,5,3);
         val(vonstr,von,count); val(bisstr,bis,count);
         DatenbankObj.Insertcolumn('PARAMETERC',0,'C','',von,bis,FALSE);
      end;

      if (Zeile = 'LAYERNAME') then { Einlesen des Layernamens }
      begin
         readln(Datei,Zeile);
         vonstr:=copy(Zeile,1,3); bisstr:=copy(Zeile,5,3);
         val(vonstr,von,count); val(bisstr,bis,count);
         DatenbankObj.Insertcolumn('LAYERNAME',0,'N','',von,bis,FALSE);
      end;

      if (Zeile = 'OBJEKTTYP') or (Zeile = 'OBJECTTYPE') then { Einlesen des Objekttyps }
      begin
         readln(Datei,Zeile);
         vonstr:=copy(Zeile,1,3); bisstr:=copy(Zeile,5,3);
         val(vonstr,von,count); val(bisstr,bis,count);
         DatenbankObj.Insertcolumn('OBJEKTTYP',0,'N','',von,bis,FALSE);
      end;

      if (Zeile = 'OBJEKTNR') or (Zeile = 'OBJECTNR') then { Einlesen der Objektnummer }
      begin
         readln(Datei,Zeile);
         vonstr:=copy(Zeile,1,3); bisstr:=copy(Zeile,5,3);
         val(vonstr,von,count); val(bisstr,bis,count);
         DatenbankObj.Insertcolumn('OBJEKTNR',0,'N','',von,bis,FALSE);
      end;

      if Zeile = '' then Database:=true;

      if (Database = true) then { Einlesen der Datenbankspalten }
      begin
         repeat
            readln(Datei,Zeile); DBSpalte:=Zeile;
            if (DBSpalte <> '') then readln(Datei,Zeile);
            vonstr:=copy(Zeile,1,3); bisstr:=copy(Zeile,5,3);
            val(vonstr,von,count); val(bisstr,bis,count);
            if (DBSpalte <> '') and (DBSpalte <> 'GRAFIK') and (DBSpalte <> 'GRAPHIC') then DatenbankObj.Insertcolumn(DBSpalte,bis-von+1,'N','',von,bis,TRUE);
            if (DBSpalte = 'GRAFIK') or (DBSpalte = 'GRAPHIC') then DatenbankObj.Insertcolumn('GRAFIK',bis-von+1,'C','',von,bis,FALSE);
         until EOF(Datei);
      end;
   until EOF(Datei);
   CloseFile(Datei);
   {$I+}
end;

{*******************************************************************************
* FUNKTION : ReadElement                                                       *
********************************************************************************
* Funktion dient zum Einlesen eines ASC-Objekts.                               *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Bytes.                                 *
********************************************************************************}
function ClassFile.ReadElement:longint;
var ObjTyp   :integer;
    ObjNr    :integer;
    count    :integer;
    Layername,Zeile:string;
    PAR_A    :string;
    PAR_B    :string;
    PAR_C    :string;
    Grafik   :string;
    X,X1,X2  :double;
    Y,Y1,Y2  :double;
    Asize     :double;
    Aangle    :double;

    imgheight:double;
    imgwidth :double;

    worked   :longint;
    Font     :string;
    AText     :string;
    SymNr    :integer;
    ASCSymNr :integer;
    PrimAxis :double;
    SecAxis  :double;
    StartAngle:double;
    EndAngle:double;
    FirstLine:string;
    Imagefilename:string;
    mr:integer;
    von,bis:integer;
    DisplayType:string;
    Alignement:string;
begin
   worked:=0;
   {$I-}
   repeat
      readln(ASCDatei,Zeile); worked:=worked + length(Zeile) + 2;
      valASC(Zeile,X,Y,PAR_A,PAR_B,PAR_C,Layername,ObjTyp,ObjNr,Grafik);
      if ((ObjTyp = 1) or (ObjTyp = 2) or (ObjTyp = 7) or (ObjTyp = 8)) and (AktObjektnummer = ObjNr) then
      begin
         if ObjTyp = 1 then LineObj.InsertPoint(X,Y);   { Einf�gen eines Punktes in das Line Objekt   }
         if ObjTyp = 2 then AreaObj.InsertPoint(X,Y);   { Einf�gen eines Punktes in das Area Objekt   }
         if ObjTyp = 7 then SplineObj.InsertPoint(X,Y); { Einf�gen eines Punktes in das Spline Objekt }
         if ObjTyp = 8 then SplineObj.InsertPoint(X,Y); { Einf�gen eines Punktes in das Spline Objekt }
      end;
   until (AktObjektnummer <> ObjNr) or EOF(ASCDatei);
   {$I+}


   { Wenn sich die Objektnummer ge�ndert hat muss ein neues Objekt erzeugt werden }
   if (ObjNr <> AktObjektnummer) and (Layername <> '') then
   begin
      { Nun wird der Layername �berpr�ft }
      AktObjektnummer:=ObjNr;
      case ObjTyp of
         0:   begin
                 { Einlesen eines Punkt Eintrages }
                 if not LayerObj.Checkexists(Layername) then LayerObj.Neu(Layername);
                 PointObj.Neu(X,Y,LayerObj.GetCurrent);
                 PointObj.ValDatenbank(Zeile);
              end;

         1:   begin
                 { Einlesen eines Linieneintrages }
                 if not LayerObj.Checkexists(Layername) then LayerObj.Neu(Layername);
                 LineObj.Neu(LayerObj.GetCurrent); LineObj.InsertPoint(X,Y);
                 LineObj.ValDatenbank(Zeile);
              end;

         2:   begin
                 { Einlesen eines Fl�cheneintrages }
                 if not LayerObj.Checkexists(Layername) then LayerObj.Neu(Layername);
                 AreaObj.Neu(LayerObj.GetCurrent); AreaObj.InsertPoint(X,Y);
                 AreaObj.ValDatenbank(Zeile);
              end;

         3:   begin
                 { Einlesen eines Symboleintrages }
                 if not LayerObj.Checkexists(Layername) then LayerObj.Neu(Layername);
                 val(Par_A,ASCSymnr,count); if count <> 0 then ASCSymnr:=0;
                 SymNr:=SymRefObj.GetGisSymNr(ASCSymnr);
                 SymbolObj.Neu(X,Y,Par_B,Par_C,SymNr,LayerObj.Getcurrent);
                 SymbolObj.ValDatenbank(Zeile);
              end;

         4:   begin
                 { Einlesen eines Kreiseintrags }
                 if not LayerObj.Checkexists(Layername) then LayerObj.Neu(Layername);
                 CircleObj.Neu(X,Y,PAR_A,LayerObj.GetCurrent);
                 CircleObj.ValDatenbank(Zeile);
                 {$i-} readln(ASCDatei,Zeile); {$i+} worked:=worked + length(Zeile) + 2;
              end;

         5:   begin
                 { Einlesen eines Arceintrages }
                 if not LayerObj.Checkexists(Layername) then LayerObj.Neu(Layername);
                 val(Par_A,PrimAxis,count); if count <> 0 then PrimAxis:=0;
                 val(Par_B,StartAngle,count); if count <> 0 then StartAngle:=0;
                 val(Par_C,AAngle,count); if count <> 0 then AAngle:=0;
                 FirstLine:=Zeile;
                 {$i-} readln(ASCDatei,Zeile); {$i+} worked:=worked + length(Zeile) + 2;
                 valASC(Zeile,X,Y,PAR_A,PAR_B,PAR_C,Layername,ObjTyp,ObjNr,Grafik);
                 val(Par_A,SecAxis,count); if count <> 0 then SecAxis:=0;
                 val(Par_B,EndAngle,count); if count <> 0 then EndAngle:=0;
                 ArcObj.Neu(X,Y,PrimAxis,StartAngle,AAngle,SecAxis,EndAngle,LayerObj.GetCurrent);
                 ArcObj.ValDatenbank(FirstLine);
              end;

         6:   begin
                 { Einlesen eines Text Eintrages }
                 if not LayerObj.Checkexists(Layername) then LayerObj.Neu(Layername);
                 X1:=X; Y1:=Y;
                 Font:=Par_A;
                 val(Par_B,Asize,count); if count <> 0 then Asize:=0;
                 val(Par_C,Aangle,count); if count <> 0 then Aangle:=0;
                 AText:=Grafik;
                 {$i-} readln(ASCDatei,Zeile); {$i+} worked:=worked + length(Zeile) + 2;
                 valASC(Zeile,X,Y,PAR_A,PAR_B,PAR_C,Layername,ObjTyp,ObjNr,Grafik);
                 X2:=X; Y2:=Y;

                 // get displaytype and alignement
                 DisplayType:=PAR_A;
                 Alignement:=PAR_B;

                 TextObj.Neu(X1,Y1,X2,Y2,AText,Font,asize,aangle,DisplayType,Alignement,LayerObj.GetCurrent);
                 TextObj.ValDatenbank(Zeile);
              end;
          7:  begin
                 { Einlesen einer 2d Spline }
                 if not LayerObj.Checkexists(Layername) then LayerObj.Neu(Layername);
                 SplineObj.Neu(LayerObj.GetCurrent); SplineObj.InsertPoint(X,Y);
                 SplineObj.ValDatenbank(Zeile);
              end;
          8:  begin
                 { Einlesen einer 3d Spline }
                 if not LayerObj.Checkexists(Layername) then LayerObj.Neu(Layername);
                 SplineObj.Neu(LayerObj.GetCurrent); SplineObj.InsertPoint(X,Y);
                 SplineObj.ValDatenbank(Zeile);
              end;
         10:  begin
                 { Einlesen eines Layereintrages - in der ersten Zeile erh�lt man die Linienart und Linienfarbe }
                 LayerObj.Neu(Layername); LayerObj.SetLineStyle(Par_A,Par_B);
                 { Nun wird die F�llung und dessen Farbe ermittelt }
                 {$i-} readln(ASCDatei,Zeile); {$i+} worked:=worked + length(Zeile) + 2;
                 valASC(Zeile,X,Y,PAR_A,PAR_B,PAR_C,Layername,ObjTyp,ObjNr,Grafik);
                 LayerObj.SetAreaStyle(Par_A,Par_B);
              end;
         11:  begin
                 { Einlesen eines Imageeintrages }
                 if not LayerObj.Checkexists(Layername) then LayerObj.Neu(Layername);
                 val (PAR_A,imgheight,count); if count <> 0 then imgheight:=0;
                 val (PAR_B,imgwidth,count);  if count <> 0 then imgwidth:=0;

                 // now the name of the image will be read
                 {$i-} readln(ASCDatei,Zeile); {$i+} worked:=worked + length(Zeile) + 2;

                 DatenbankObj.Getvonbis('OBJEKTTYP',von,bis);
                 Zeile:=copy(Zeile,1,von-1);
                 Imagefilename:=Zeile; Imagefilename:=TrimRight(Imagefilename);

                 // MainWnd.StatusLB.Items.Add(StrFileObj.Zuweisen(45) + Extractfilename(Imagefilename)); // ignore image:
                 // MainWnd.StatusLB.ItemIndex := MainWnd.StatusLB.Items.Count-1;
                 // MainWnd.StatusLB.Update;

                 if (MainWnd.ImagePath <> '') then // set new filename for image
                 begin
                    Imagefilename:=MainWnd.ImagePath + Extractfilename(Imagefilename);
                 end;

                 if not Fileexists (Imagefilename) and not MainWnd.IgnoreNotFoundImages then
                 begin
                     SetImgWnd:=TSetImgWnd.Create(nil);
                     SetImgWnd.SetImageName(Imagefilename);
                     mr:=SetImgWnd.ShowModal;
                     if mr = mrOK then
                     begin
                        if SetImgWnd.UseDirCB.Checked then
                           Imagefilename:=MainWnd.ImagePath + Extractfilename(Imagefilename)
                        else
                        begin
                           if not MainWnd.IgnoreNotFoundImages then
                              Imagefilename:=SetImgWnd.GetImageName;
                        end;
                     end;
                     SetImgWnd.Free;
                 end;
                 ImageObj.Neu(X,Y,imgheight,imgwidth,PAR_C,Imagefilename,LayerObj.GetCurrent);
              end;
      end;
   end;
   Result:=worked;
   {$i-}
   if Eof(ASCDatei) then Result:=9999999;
   {$i+}
end;

{*******************************************************************************
* PROZEDUR : ValASC                                                            *
********************************************************************************
* Prozedur dient zum Auswerten einer eingelesenen Zeile aus dem ASC-File.      *
*                                                                              *
* PARAMETER: Zeile -> Zeile, die auszuwerten ist.                              *
*            X     -> X-Koordinate aus der Zeile                               *
*            Y     -> Y-Koordinate aus der Zeile                               *
*            PAR_A -> Parameter A aus der Zeile                                *
*            PAR_B -> Parameter B aus der Zeile                                *
*            PAR_C -> Parameter C aus der Zeile                                *
*            Layername -> Layername aus der Zeile                              *
*            ObjTyp -> Objekttyp                                               *
*            ObjNr  -> Objektnummer                                            *
*            Grafik -> Grafik aus der Zeile.                                   *
********************************************************************************}
procedure ClassFile.ValASC
(
   Zeile:string;var X:double;var Y:double;var PAR_A:string; var PAR_B:string; var PAR_C:string; var Layername:string;var ObjTyp:integer; var ObjNr:integer;var Grafik:string
);
var von,bis,count:integer;
    hstr:string;
begin
   { Erhalten der X-Koordiante }
   DatenbankObj.Getvonbis('X-KOORDINATE',von,bis);
   hstr:=copy(Zeile,von,bis-von + 1); hstr:=Trim(hstr);
   // replace ',' signs with '.'
   hstr:=StringReplace(hstr,',','.',[rfreplaceall]);
   val(hstr,X,count);
   DatenbankObj.SetLength('X-KOORDINATE',length(floattostr(X)));

   { Erhalten der Y-Koordiante }
   DatenbankObj.Getvonbis('Y-KOORDINATE',von,bis);
   hstr:=copy(Zeile,von,bis-von + 1); hstr:=Trim(hstr);
   hstr:=StringReplace(hstr,',','.',[rfreplaceall]);
   val(hstr,Y,count);
   DatenbankObj.SetLength('Y-KOORDINATE',length(floattostr(Y)));

   { Erhalten von Parameter-A }
   DatenbankObj.Getvonbis('PARAMETERA',von,bis); PAR_A:=copy(Zeile,von,bis-von+1); PAR_A:=Trim(PAR_A);
   PAR_A:=StringReplace(PAR_A,',','.',[rfreplaceall]);

   { Erhalten von Parameter-B }
   DatenbankObj.Getvonbis('PARAMETERB',von,bis); PAR_B:=copy(Zeile,von,bis-von+1); PAR_B:=Trim(PAR_B);
   PAR_B:=StringReplace(PAR_B,',','.',[rfreplaceall]);

   { Erhalten von Parameter-C }
   DatenbankObj.Getvonbis('PARAMETERC',von,bis); PAR_C:=copy(Zeile,von,bis-von+1); PAR_C:=Trim(PAR_C);
   PAR_C:=StringReplace(PAR_C,',','.',[rfreplaceall]);

   { Erhalten des Layernamens }
   DatenbankObj.Getvonbis('LAYERNAME',von,bis); Layername:=copy(Zeile,von,bis-von+1);
   Layername:=TrimRight(Layername);
   Layername:=TrimLeft (Layername);

   { Erhalten des Objekttyps }
   DatenbankObj.Getvonbis('OBJEKTTYP',von,bis); hstr:=copy(Zeile,von,bis-von + 1); val(hstr,ObjTyp,count);

   { Erhalten der Objektnummer }
   DatenbankObj.Getvonbis('OBJEKTNR',von,bis); hstr:=copy(Zeile,von,bis-von + 1); val(hstr,ObjNr,count);

   { Erhalten von Grafik }
   DatenbankObj.Getvonbis('GRAFIK',von,bis); Grafik:=copy(Zeile,von,bis-von+1); TRIMLEFT(TRIMRIGHT(Grafik));
end;


end.
