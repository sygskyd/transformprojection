unit Maindlg;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Strfile, StdCtrls, Buttons, ExtCtrls, Gauges, Files, Db,
  DBTables,Math, windows, OleCtrls, AXImpExpDbcXControl_TLB, AxGisPro_TLB,
  AXGisMan_TLB, AxDll_TLB, WinOSInfo;

const

      //registration modes
      NO_LICENCE        = 0;
      FULL_LICENCE      = 1;
      DEMO_LICENCE      = 2;

      // Object types
      ot_Pixel     = 1;
      ot_Poly      = 2;
      ot_CPoly     = 4;
      ot_Text      = 7;
      ot_Symbol    = 8;
      ot_Spline    = 9;
      ot_Image     = 10;
      ot_MesLine   = 11;
      ot_Circle    = 12;
      ot_Arc       = 13;

            //TextSTyles
      ts_Left           =  0;
      ts_Center         =  1;
      ts_Right          =  2;
      ts_Italic         =  4;
      ts_Bold           =  8;
      ts_Underl         = 16;
      ts_FixHeight      = 32;
      ts_Transparent    = 64;

      //LineTypes
      lt_Solid     = 0;
      lt_Dash      = 1;
      lt_Dot       = 2;
      lt_DashDot   = 3;
      lt_DashDDot  = 4;
      lt_UserDefined    = 1000;

      //FillTypes
      pt_NoPattern = 0;
      pt_FDiagonal = 1;
      pt_Cross     = 2;
      pt_DiagCross = 3;
      pt_BDiagonal = 4;
      pt_Horizontal= 5;
      pt_Vertical  = 6;
      pt_Solid     = 7;
      pt_UserDefined = 8;

      stDrawingUnits    = 0;
      stScaleInDependend= 1;
      stProjectScale    = 2;
      stPercent         = 3;

type
  TMainWnd = class(TForm)
    Label1: TLabel;
    Panel1: TPanel;
    Progresslabel: TLabel;
    Prozessbalken: TGauge;
    StatusLB: TListBox;
    SaveReportBtn: TButton;
    OKBtn: TButton;
    CancelBtn: TButton;
    SaveDialogRep: TSaveDialog;
    CloseTimer: TTimer;
    SetupWindowTimer: TTimer;
    AXImpExpDbc: TAXImpExpDbc;
    AxGisProjection: TAxGisProjection;
    AXGisObjMan: TAXGisObjMan;
    procedure FormCreate(Sender: TObject);
    procedure StopBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure SaveReportBtnClick(Sender: TObject);
    procedure CloseTimerTimer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AXImpExpDbcDialogSetuped(Sender: TObject);
    procedure AXImpExpDbcRequestImpLayerIndex(Sender: TObject;
      const Layername: WideString; var LayerIndex: Integer);
    procedure AXImpExpDbcRequestImpProjectName(Sender: TObject;
      var ProjectName: WideString);
    procedure AXImpExpDbcRequestImpProjectGUIDs(Sender: TObject;
      var StartGUID, CurrentGUID: WideString);
  private
    { Private-Deklarationen }
    StrFileObj :ClassStrfile;
    FileObj    :ClassFile;
    Pass       :integer;

    AscDateiname:string;
    AdfDateiname:string;
    SymDateiname:string;

    Bytes2work  :longint;
    Bytesworked :longint;

    Layers2work :longint;
    Layersworked:longint;
    Aktiv       :boolean;

    ObjectCount :longint;

    FirstActivate  :boolean;

    NumPolys  :integer;
    NumCPolys :integer;
    NumPixels :integer;
    NumTexts  :integer;
    NumArcs   :integer;
    NumSplines:integer;
    NumImages :integer;
    NumCircles:integer;
    NumLayers :integer;
    NumSymbols:integer;
    FilesProcessed:integer;
    NumIgnoredObjects:integer;
    FileStartTime   :TDateTime;
    OverAllStartTime:TDateTime;

    procedure RunPass(AktPass:integer);
    procedure ExecutePass;
    function GetAngle(X1:double;Y1:double;X2:double;Y2:double):double;
    procedure DisplayDemoWarning;
    procedure DisplayStatistics;
  public
    { Public-Deklarationen }
    App         :Variant;
    DestDocument:IDocument;
    Layers      :ILayer;
    APolyline   :IPolyline;
    APolygon    :IPolygon;
    ASpline     :ISpline;
    DestDBMode  :integer;
    RegMode     :integer;
    ImagePath   :string;
    IgnoreNotFoundImages:boolean;
    LngCode     :string;
    ExitNormal  :boolean;
    Abort       :boolean;

    GisHandle   :integer;

    { Procedures that are used to handle AX-Control }
    procedure CreateLayer(Layer:string);
    function  InsertPoint(X:double;Y:double; var HasDuplicate:boolean):integer;
    function  CheckLayerExists(Layer:string):boolean;
    procedure CreatePolyline;
    procedure InsertPointToPolyLine(X:double; Y:double);
    function  ClosePolyline(var HasDuplicate:boolean):integer;
    procedure CreatePolygon;
    procedure InsertPointToPolygon(X:double; Y:double);
    function  ClosePolygon(var HasDuplicate:boolean):integer;
    procedure CreateSpline;
    procedure InsertPointToSpline(X:double; Y:double);
    function  CloseSpline(var HasDuplicate:boolean):integer;
    procedure SetLayerPriorities(Linienstil:longint;Linienfarbe:longint;Flaechenstil:longint;Flaechenfarbe:longint);
    function  InsertText(X1:double;Y1:double;X2:double;Y2:double;AAngle:integer;ATextInfo:string;AHeight:integer;Afont:string;TextStyle:integer; var HasDuplicate:boolean):integer;
    function  CreateEllipse(X:double;Y:double;Radius1:integer; Radius2:integer; var HasDuplicate:boolean):integer;
    function InsertSymbol(X:double;Y:double;Size:double;Angle:double;Symnr:integer; var HasDuplicate:boolean):integer;
    function InsertArc(X:double;Y:double; PrimAxis:integer; Angle: double; StartAngle: Double; SecAxis:integer; EndAngle: Double; var HasDuplicate:boolean):integer;
    function InsertImage(X:double;Y:double;aHeight:double; aWidth:double; Settings:string; Filename:string; var HasDuplicate:boolean):integer;

    procedure AddAndShowPercent;
    procedure SetProjectionSettings;
    procedure CheckProjectionSettings;

    procedure StartImport;
    procedure CreateStringFile;
  end;

var
  MainWnd: TMainWnd;

implementation

uses SelASCFileDlg;

{$R *.DFM}

procedure TMainWnd.CreateStringFile;
begin
   StrFileObj:=ClassStrfile.Create;
   StrFileObj.CreateList(OSInfo.WingisDir,LngCode,'ASCIMP');
   Self.Caption:=StrFileObj.Assign(1);           { WinGIS PXF-ASC Import 2000 }
   OkBtn.Caption:=StrFileObj.Assign(6);          { Ok }
   CancelBtn.Caption:=StrFileObj.Assign(7);      { Cancel }
   SaveReportBtn.Caption:=StrFileObj.Assign(16); { Save Report }
   ProgressLabel.Caption:=StrFileObj.Assign(18); { Progress messages }
end;

procedure TMainWnd.FormCreate(Sender: TObject);
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   FirstActivate:=true;
   Self.Height:=1;
   Self.Width:=1;
   Self.Update;
   Aktiv:=false;

   ImagePath:='';
   IgnoreNotFoundImages:=false;
   ObjectCount:=0;

   NumPolys:=0;
   NumCPolys:=0;
   NumPixels:=0;
   NumTexts:=0;
   NumArcs:=0;
   NumSplines:=0;
   NumImages:=0;
   NumCircles:=0;
   FilesProcessed:=0;
   NumSymbols:=0;
   NumIgnoredObjects:=0;
   NumLayers:=0;

   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

procedure TMainWnd.CheckProjectionSettings;
var SourceProjection:integer;
    hstr:string;
    different:boolean;
begin
   different:=false;
   // function is used to check if the settings in the
   if (AxGisProjection.TargetCoordinateSystem <> DestDocument.Projection) then
      different:=TRUE;

   if (AxGisProjection.TargetXOffset <> DestDocument.ProjectionXOffset) then
      different:=TRUE;

   if (AxGisProjection.TargetYOffset <> DestDocument.ProjectionYOffset) then
      different:=TRUE;

   if (AxGisProjection.TargetScale <> DestDocument.ProjectionScale) then
      different:=TRUE;

   // check if the projections are different
   if DestDocument.ProjectProjSettings <> AxGisProjection.TargetProjSettings then
      different:=TRUE;

   if DestDocument.ProjectProjection <> AxGisProjection.TargetProjection then
      different:=TRUE;

   if DestDocument.Projectdate <> AxGisProjection.TargetDate then
      different:=TRUE;

   if different then
   begin
      Showmessage(StrfileObj.Assign(53)); // Warning: Projection does not fit to project projection
      AxGisProjection.SetupByDialog;
   end;
   // the used output coordinate-system is equal to the
   // target-coordinate-system
   AxGisProjection.UsedInputCoordSystem:=AxGisProjection.SourceCoordinateSystem;
end;

procedure TMainWnd.SetProjectionSettings;
var aApp:OleVariant;
    aDoc:OleVariant;
begin
   // procedure is used to set the projection settings
   // of the current project to the GeoWnd
   AxGisProjection.WorkingPath:=OSInfo.WingisDir;
   AxGisProjection.LngCode:=LngCode;
   AXImpExpDbc.WorkingDir:=OSInfo.WingisDir;
   AXImpExpDbc.LngCode:=LngCode;
   AxImpExpDbc.ShowMode:=2; // show only Target
   AXGisObjMan.WorkingDir:=OSInfo.WingisDir;
   AXGisObjMan.LngCode:=LngCode;
   aApp:=App; AxGisObjMan.SetApp(aApp);
   aDoc:=DestDocument; AxGisObjMan.SetDocument(aDoc);

   SelAscFilesWnd.DestDbLabel.Caption:=AXImpExpDbc.ImpDbInfo;
   AxGisProjection.SourceCoordinateSystem:=DestDocument.Projection;
   AxGisProjection.TargetCoordinateSystem:=DestDocument.Projection;
   AxGisProjection.SourceProjSettings:=DestDocument.ProjectProjSettings;
   AxGisProjection.TargetProjSettings:=DestDocument.ProjectProjSettings;
   AxGisProjection.SourceProjection:=DestDocument.ProjectProjection;
   AxGisProjection.TargetProjection:=DestDocument.ProjectProjection;
   AxGisProjection.SourceDate:=DestDocument.Projectdate;
   AxGisProjection.TargetDate:=DestDocument.Projectdate;
   AxGisProjection.SourceXOffset:=DestDocument.ProjectionXOffset;
   AxGisProjection.TargetXOffset:=DestDocument.ProjectionXOffset;
   AxGisProjection.SourceYOffset:=DestDocument.ProjectionYOffset;
   AxGisProjection.TargetYOffset:=DestDocument.ProjectionYOffset;
   AxGisProjection.SourceScale:=DestDocument.ProjectionScale;
   AxGisProjection.TargetScale:=DestDocument.ProjectionScale;
   // if source and target is .amp - file
   // the used input coordinates and used output coordinates
   // for the projection calculation are in carthesic
   AxGisProjection.UsedInputCoordSystem:=DestDocument.Projection;
   AxGisProjection.UsedOutputCoordSystem:=cs_Carthesic;
end;


{*******************************************************************************
* PROZEDUR : StopBtnClick                                                      *
********************************************************************************
* Prozedur wird aufgerufen wenn der Stopbutton im MainWnd gedr�ckt wird.       *
* Damit wird entweder der Konvertierung gestoppt oder die Applikation beendet. *
*                                                                              *
********************************************************************************}
procedure TMainWnd.StopBtnClick(Sender: TObject);
begin
   if aktiv = true then
   begin
      FileObj.CloseallFiles;
      aktiv:=false; Abort:=true;
   end
   else
      MainWnd.Close;
end;

{*******************************************************************************
* PROZEDUR : RunPass                                                           *
********************************************************************************
* Prozedur leitet den jeweiligen Konvertierungs-Pass ein.                      *
*                                                                              *
* PARAMETER: Pass -> Gibt den Konvertierungs-Pass an.                          *
*                                                                              *
********************************************************************************}
procedure TMainWnd.RunPass(AktPass:integer);
var Status:string;
    aDate, DispDate, aTime, DispTime:string;
    EllapsedTime:TDateTime;
begin
   if AktPass = 1 then { �ffnen der Dateien zum Auslesen des ASC-Files }
   begin
      OkBtn.Enabled:=false;
      SaveReportBtn.Enabled:=false;
      Aktiv:=true;
      if not SelASCfilesWnd.GetFilename(ASCDateiname,ADFDateiname,SYMDateiname) then
      begin
         Aktiv:=false;
         DisplayStatistics;
         CancelBtn.enabled:=false;
         OKBtn.enabled:=true;
         SaveReportBtn.enabled:=true;
         exit;
      end;
      // check if all files exist
      if (not FileExists(ASCDateiname)) or (not FileExists(ADFDateiname)) or (not FileExists(SYMDateiname)) then
      begin
         // Skip the File
         Runpass(1);
         exit;
      end;
      FileObj:=ClassFile.Create;
      // display the starttime
      if (FilesProcessed = 0) then
         OverAllStartTime:=now;
      FilesProcessed:=FilesProcessed+1;
      FileStartTime:=now;
      aDate:=FormatDateTime('ddmmyy',now);
      DispDate:=copy(aDate,1,2)+'.'+copy(aDate,3,2)+'.'+copy(aDate,5,2);
      aTime:=Formatdatetime('hhnnss',Now);
      DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
      StatusLB.Items.Add(StrFileObj.Assign(72) + DispDate+' / '+DispTime); // start time:

      // display PXF-Files that are currently read
      Status:=StrfileObj.Assign(10)+ExtractFilename(ASCDateiname); { ASC Datei: }
      StatusLB.Items.Add(Status);
      Status:=StrfileObj.Assign(11)+ExtractFilename(ADFDateiname); { ADF Datei: }
      StatusLB.Items.Add(Status);
      Status:=StrfileObj.Assign(12)+ExtractFilename(SYMDateiname); { SYM Datei: }
      StatusLB.Items.Add(Status);
      StatusLb.Items.Add(AXImpExpDbc.ImpDbInfo);
      Status:=StrfileObj.Assign(19);
      StatusLB.Items.Add(Status);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      { Die Anzahl der zu bearbeitenden Bytes wird ermittelt }
      Bytes2Work:=FileObj.GetFilesize(AscDateiname);  Bytesworked:=0;

      // now the projection will be checked
      CheckProjectionSettings;

      { Die Dateien werden ge�ffnet }
      FileObj.OpenASCFile(AscDateiname);
      FileObj.ScanADF(AdfDateiname);
      FileObj.ScanSym(SymDateiname);
      Pass:=1;
      ExecutePass;
      exit;
   end;

   if AktPass = 2 then
   begin
       { Ermitteln der Anzahl der Layer }
       Layers2work:=FileObj.GetMaxLayers; Layersworked:=0;
       Bytes2work:=FileObj.GetMaxObjects; Bytesworked:=0;

       { Setzen der Maskeneinstellung }
       StatusLB.Items.Add(StrfileObj.Assign(15)); { Import PXF-ASCII data }
       StatusLB.ItemIndex := StatusLB.Items.Count-1;
       StatusLB.Update;

       Pass:=2;
       ExecutePass;
       exit;
   end;

   if AktPass = 3 then { Ende der Konvertierung }
   begin
      FileObj.CloseallFiles;
      StatusLB.Items.Add(StrFileObj.Assign(14)); { File correct imported! }
      // display end time for this file
      EllapsedTime:=now-FileStartTime;
      aDate:=FormatDateTime('ddmmyy',now);
      DispDate:=copy(aDate,1,2)+'.'+copy(aDate,3,2)+'.'+copy(aDate,5,2);
      aTime:=Formatdatetime('hhnnss',now);
      DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
      StatusLB.Items.Add(StrFileObj.Assign(58) + DispDate+' / '+DispTime); // end time:
      // display ellapsed time for this file
      aTime:=Formatdatetime('hhnnss',EllapsedTime);
      DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
      StatusLB.Items.Add(StrFileObj.Assign(57) + DispTime); // ellapsed time:
      StatusLB.Items.Add('--------------------------------------------------------');
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      Aktiv:=false;
      FileObj.Destroy;
      Runpass(1); // go to next file
   end;
end;

{*******************************************************************************
* PROZEDUR : ExecutePass                                                       *
********************************************************************************
* Prozedur die zur Abarbeitung eines Konvertierungsschritts dient              *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure TMainWnd.ExecutePass;
var Prozent:integer;
begin
   if Pass = 1 then // read pxf-asc data
   begin
      repeat
         Bytesworked:=Bytesworked + FileObj.ReadElement;
         { Anzeigen des Prozessstatus }
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then begin RunPass(2); exit; end;
      if Abort = true then exit;
   end;

   if Pass = 2 then // importing into GIS
   begin
      if Layers2work > 0 then
      begin
         repeat
            // first table will be created
            FileObj.CreateDatabase(FileObj.GetCurrentLayername);

            Layersworked:=Layersworked + FileObj.WriteCurrentLayer;
            Application.Processmessages;
         until (Layersworked >= Layers2work) or (Abort = true);
      end
      else
      begin
         // layer contains no objects - maybe .adf file incorrect
         StatusLB.Items.Add(StrfileObj.Assign(52)); // Warning: Layer contains no data - maybe .adf file incorrect!
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
      end;
      if (Layersworked >= Layers2work) and (Abort = false) then begin RunPass(3); exit; end;
      if Abort = true then exit;
   end;
end;

procedure TMainWnd.CreateLayer(Layer:string);
begin
   NumLayers:=NumLayers+1;
   Layers:=DestDocument.Layers.InsertLayerByName(Layer,True);
end;

function TMainWnd.CheckLayerExists(Layer:string):boolean;
var dispatch:IDispatch;
    retVal:boolean;
begin
   dispatch:=DestDocument.Layers.GetLayerByName(Layer);
   if dispatch <> nil then retVal:=true else retVal:=false;
   if retVal then
      Layers:=DestDocument.Layers.GetLayerByName(Layer);
   result:=retVal;
end;

procedure TMainWnd.CreatePolyline;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;
   APolyline:=DestDocument.CreatePolyline;
end;

procedure TMainWnd.DisplayDemoWarning;
begin
   StatusLB.Items.Add(StrfileObj.Assign(41)); { 100 Objects created - following objects will be skiped. }
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;
   // also the database must be ignored after this point
   DestDBMode:=DB_NONE;
   AXImpExpDbc.ImpDbMode:=DestDBMode;
end;

procedure TMainWnd.CreateSpline;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;
   ASpline:=DestDocument.CreateSpline;
end;

procedure TMainWnd.InsertPointToSpline(X:double;Y:double);
var APoint:IPoint;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;

   //do the projection
   AxGisProjection.Calculate(X,Y);

   APoint:=DestDocument.CreatePoint;
   APoint.X:=Round(X*100);
   APoint.Y:=Round(Y*100);
   ASpline.InsertPoint(APoint);
end;

procedure TMainWnd.InsertPointToPolyLine(X:double; Y:double);
var APoint:IPoint;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;

   //do the projection
   AxGisProjection.Calculate(X,Y);

   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   APolyLine.InsertPoint(APoint);
end;

function TMainWnd.ClosePolyline(var HasDuplicate:boolean):integer;
var AIndex:integer;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;

   Layers.InsertObject(APolyline);
   AIndex:=APolyline.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

   // check if it is a duplicate
   if SelASCFilesWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, Layers.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;
   if (AIndex <> 0) and (DuplId = 0) then
      NumPolys:=NumPolys+1;
   result:=AIndex;
end;

function TMainWnd.CloseSpline(var HasDuplicate:boolean):integer;
var AIndex:integer;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;
   Layers.InsertObject(ASpline);
   AIndex:=ASpline.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;
   // check if it is a duplicate
   if SelASCFilesWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, Layers.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;

   if (AIndex <> 0) and (DuplId = 0) then
      NumSplines:=NumSplines+1;
   result:=AIndex;
end;

procedure TMainWnd.CreatePolygon;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;
   APolygon:=DestDocument.CreatePolygon;
end;

procedure TMainWnd.InsertPointToPolygon(X:double; Y:double);
var APoint:IPoint;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;
   //do the projection
   AxGisProjection.Calculate(X,Y);

   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   APolygon.InsertPoint(APoint);
end;

function TMainWnd.ClosePolygon(var HasDuplicate:boolean):integer;
var AIndex:integer;
    Duplid:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;

   APolygon.ClosePoly;
   Layers.InsertObject(APolygon);
   AIndex:=APolygon.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

   // check if it is a duplicate
   if SelASCFilesWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, Layers.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;
   if (AIndex <> 0) and (DuplId = 0) then
      NumCPolys:=NumCPolys+1;
   result:=AIndex;
end;

function TMainWnd.InsertImage(X:double;Y:double;aHeight:double; aWidth:double; Settings:string; Filename:string;var HasDuplicate:boolean):integer;
var AImage:IImage;
    AIndex:integer;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;
   //do the projection
   AxGisProjection.Calculate(X,Y);

   AImage:=DestDocument.CreateImage(Filename);

   AImage.SetPosition(round(X*100), round(Y*100), round(aWidth*100), round(aHeight*100));
   AImage.PXFStyle:=Settings;

   Layers.InsertObject(AImage);
   AIndex:=AImage.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

   // check if it is a duplicate
   if SelASCFilesWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, Layers.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;

   if (AIndex <> 0) and (DuplId = 0) then
      NumImages:=NumImages+1;
   result:=AIndex;
end;

function TMainWnd.InsertPoint(X:double;Y:double; var HasDuplicate:boolean):integer;
var APixel:IPixel;
    APoint:IPoint;
    AIndex:integer;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;
   //do the projection
   AxGisProjection.Calculate(X,Y);

   APixel:=DestDocument.CreatePixel;
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   APixel.Position:=APoint;
   Layers.InsertObject(APixel);
   AIndex:=APixel.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

   // check if it is a duplicate
   if SelASCFilesWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, Layers.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;
   if (AIndex <> 0) and (DuplId = 0) then
      NumPixels:=NumPixels+1;

   result:=AIndex;
end;

procedure TMainWnd.SetLayerPriorities(Linienstil:longint;Linienfarbe:longint;Flaechenstil:longint;Flaechenfarbe:longint);
var ALinestyle:Variant;
    AFillstyle:Variant;
begin
   Layers.Linestyle.Style:=Linienstil;
   Layers.Linestyle.Color:=Linienfarbe;
   Layers.Linestyle.FillColor:=Linienfarbe;

   Layers.Fillstyle.Pattern:=Flaechenstil;
   Layers.Fillstyle.ForeColor:=Flaechenfarbe;
//   Layers.Fillstyle.BackColor:=Flaechenfarbe;
end;


function TMainWnd.InsertArc(X:double;Y:double; PrimAxis:integer; Angle: double; StartAngle: Double; SecAxis:integer; EndAngle: Double; var HasDuplicate:boolean):integer;
var AArc:IArc;
    APoint:IPoint;
    retVal:integer;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;

   //do the projection
   AxGisProjection.Calculate(X,Y);

   retVal:=0;
   AArc:=DestDocument.CreateArc;
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   AArc.Position:=APoint;
   AArc.PrimaryAxis:=PrimAxis;
   AArc.Angle:=Angle * PI / 180;
   AArc.BeginAngle:=StartAngle * PI / 180;
   AArc.SecondaryAxis:=SecAxis;
   AArc.EndAngle:=EndAngle * PI / 180;
   Layers.InsertObject(AArc);
   retVal:=AArc.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

   // check if it is a duplicate
   if SelASCFilesWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(retVal, Layers.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(retVal);
         retVal:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;

   if (retVal <> 0) and (DuplId = 0) then
      Numarcs:=Numarcs+1;

   result:=retVal;
end;


function TMainWnd.InsertText(X1:double;Y1:double;X2:double; Y2:double; AAngle:integer;ATextinfo:string;AHeight:integer;Afont:string;TextStyle:integer;var HasDuplicate:boolean):integer;
var AText:IText;
    AIndex:integer;
    hX,hY:double;
    hAngle:integer;
    hstr:string;
    i:integer;
    fontexists:boolean;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;

   //do the projection
   AxGisProjection.Calculate(X1,Y1);
   AxGisProjection.Calculate(X2,Y2);

   AText:=DestDocument.CreateText;

   AText.FontStyle:=TextStyle;

   // check if font is installed
   fontexists:=FALSE;
   for i:=0 to Screen.Fonts.Count-1 do
   begin
      if Screen.Fonts[i] = AFont then
      begin
         fontexists:=TRUE;
         break;
      end;
   end;

   if not fontexists then
   begin
      StatusLB.Items.Add(StrfileObj.Assign(22) + ' '+ AFont +' '+ StrfileObj.Assign(23)); //Font not known ... use Courier New as default
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      AFont:='Courier New'; //set default font
   end;

   AText.Fontname:=AFont;
   AText.Position.X:=round(X1*100);
   AText.Position.Y:=round(Y1*100);

   AText.Text:=ATextInfo;
   AText.FontHeight:=AHeight;
   AText.Angle:=aAngle;

   AText.PositionateOnLine(round(X1*100),round(Y1*100),round(X2*100),round(Y2*100));
   Layers.Insertobject(AText);
   AIndex:=AText.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

   // check if it is a duplicate
   if SelASCFilesWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, Layers.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;

   if (AIndex <> 0) and (DuplId = 0) then
      NumTexts:=NumTexts+1;
   result:=AIndex;
end;

function TMainWnd.GetAngle(X1:double;Y1:double;X2:double;Y2:double):double;
var A,B,C:double;
    Alpha:double;
    retVal:double;
begin
   { Zuwerst werden die Kantenl�ngen des Dreiecks ermittelt }
   if (X1 = X2) and (Y1 = Y2) then
   begin
      retVal:=0;
   end
   else
   begin
      A:=ABS(Y2 - Y1); B:=ABS(X2 - X1); C:=SQRT(SQR(A) + SQR(B));
      Alpha:=(A / C);  { Nun ist der Sinus von Alpha bekannt }
      { Nun muss man noch den Invers Sinus von Alpha berechnen }
      Alpha:=ArcSin(Alpha);
      retVal:=(Alpha*180) / PI;
      if (Y2 >= Y1) then { Man befindet sich im oberen Halbkreis des Kreises }
      begin
         if (X2 < X1) then   { Man geht nach oben links   }
         begin
            retVal:=180 - retVal;
         end;
         if (X2 > X1) and (Y1 = Y2) then { Man geht gerade nach rechts }
         begin
            retVal:=360;
         end;
      end;
      if (Y2 < Y1) then { Man befindet sich im unteren Halbkreis des Kreises }
      begin
         if (X2 >= X1) then { Man geht nach rechts }
            retVal:=360 - retVal
         else               { Man geht nach links }
            retVal:=retVal + 180;
      end;
   end;

   result:=retVal;
end;

function TMainWnd.InsertSymbol(X:double;Y:double;Size:double;Angle:double;Symnr:integer; var HasDuplicate:boolean):integer;
var ASymbol:ISymbol;
    APoint :IPoint;
    MySymboldef:ISymbolDef;
    i:integer;
    gefunden:boolean;
    new_id:integer;
    SymName:string;
    cmpSymNr:integer;
    SymSize_int:integer;
    SymSize_double:double;
    SymSizeType:integer;
    hstr:string;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;

   { Now the Symbolnr has to be evaluated }
   gefunden:=false;
   for i:=0 to DestDocument.Symbols.Count-1 do
   begin
      MySymboldef:=DestDocument.Symbols.Items[i];
      cmpSymNr:=MySymboldef.Index;
      if cmpSymNr = SymNr then
      begin
         SymName:=MySymbolDef.Name;
         SymSize_double:=MySymbolDef.DefaultSize; // get the default-size
         SymSizeType:=MySymbolDef.DefaultSizeType;
         SymSize_int:=round(SymSize_double * Size);
         gefunden:=true;
         break;
      end;
   end;
   if (gefunden) then
   begin
      //do the projection
      AxGisProjection.Calculate(X,Y);

      ASymbol:=DestDocument.CreateSymbol;
      APoint:=DestDocument.CreatePoint;
      ASymbol.SymIndex:=Symnr;
      ASymbol.Angle:=Angle * PI / 180;
      ASymbol.Symbolname:=Symname;
      ASymbol.Position.X:=round(X*100);
      ASymbol.Position.Y:=round(Y*100);
      ASymbol.SizeType:=SymSizeType;
      ASymbol.Size:=SymSize_int;
      Layers.InsertObject(ASymbol);
      new_id:=ASymbol.Index;
      DuplId:=0;
      HasDuplicate:=FALSE;
      // check if it is a duplicate
      if SelASCFilesWnd.IgnoreDuplicatesCb.Checked then
      begin
         // check if the object has a duplicate
         DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(new_id, Layers.Layername);
         if DuplId > 0 then
         begin
            // remove the object
            DestDocument.DeleteObjectByIndex(new_id);
            new_id:=DuplId;
            HasDuplicate:=TRUE;
            NumIgnoredObjects:=NumIgnoredObjects+1;
         end;
      end;
      if (new_id <> 0) and (DuplId = 0) then
         NumSymbols:=NumSymbols+1;
   end
   else
   begin
      StatusLB.Items.Add(StrfileObj.Assign(20) + inttostr(SymNr-600000000));
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      new_id:=InsertPoint(X,Y, HasDuplicate);
   end;
   result:=new_id;
end;

function TMainWnd.CreateEllipse(X:double;Y:double;Radius1:integer; Radius2:integer; var HasDuplicate:boolean):integer;
var ACircle:ICircle;
    APoint:IPoint;
    AIndex:integer;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;

   //do the projection
   AxGisProjection.Calculate(X,Y);

   ACircle:=DestDocument.CreateCircle;
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   ACircle.PrimaryAxis:=Radius1;
   ACircle.SecondaryAxis:=Radius2;
   ACircle.Position:=APoint;
   Layers.InsertObject(ACircle);
   AIndex:=ACircle.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

   // check if it is a duplicate
   if SelASCFilesWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, Layers.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;
   if (AIndex <> 0) and (DuplId = 0) then
      NumCircles:=NumCircles+1;

   result:=AIndex;
end;

procedure TMainWnd.StartImport;
begin
   if ExitNormal then
   begin
      // init database connection
      AXImpExpDbc.WorkingDir:=OSInfo.WingisDir;
      AXImpExpDbc.LngCode:=LngCode;
      RunPass(1);
   end;
end;

procedure TMainWnd.CancelBtnClick(Sender: TObject);
var Status:string;
begin
   if aktiv = true then
   begin
      Status:=StrFileObj.Assign(13); { Import canceled! }
      StatusLB.Items.Add(Status);
      StatusLB.Items.Add('--------------------------------------------------------');
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      FileObj.CloseallFiles;
      aktiv:=false; Abort:=true;
      OkBtn.Enabled:=true;
      SaveReportBtn.Enabled:=true;
      CancelBtn.Enabled:=false;
      DisplayStatistics;
   end
end;

procedure TMainWnd.OKBtnClick(Sender: TObject);
begin
   CloseTimer.Enabled:=true;
end;

procedure TMainWnd.SaveReportBtnClick(Sender: TObject);
var Datei:Textfile;
    Zeile:string;
    i:integer;
begin
   if SaveDialogRep.Execute then
   begin
      {$I-}
      Assignfile(Datei,SaveDialogRep.Filename);
      Rewrite(Datei);
      for i:=0 to StatusLB.Items.Count-1 do
      begin
         Zeile:=StatusLB.items[i];
         Writeln(Datei,Zeile);
      end;
      Closefile(Datei);
      {$I+}
   end;
end;

procedure TMainWnd.AddAndShowPercent;
var Prozent:integer;
begin
    Bytesworked:=Bytesworked + 1;
    { Anzeigen des Prozessstatus }
    if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
    ProzessBalken.Progress :=Prozent;
    ProzessBalken.Update;
    Application.ProcessMessages;
end;

procedure TMainWnd.CloseTimerTimer(Sender: TObject);
begin
   MainWnd.Close;
   SelASCFilesWnd.Close;
   DestDocument.FinishImpExpRoutine(ExitNormal,'IMPORTMODULE');
   CloseTimer.Enabled:=false;
end;

procedure TMainWnd.FormDestroy(Sender: TObject);
begin
   StrfileObj.Destroy;
end;

procedure TMainWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TMainWnd.SetupWindowTimerTimer(Sender: TObject);
var IdbProcessMask:integer;
begin
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,SelAscFilesWnd.winleft,SelASCFilesWnd.wintop,SelAscFilesWnd.winwidth, SelAscFilesWnd.winheight,SWP_SHOWWINDOW);
   MainWnd.Height:=SelAscFilesWnd.winheight;
   MainWnd.Width:=SelAscFilesWnd.winwidth;
   Self.Repaint;
   SetupWindowTimer.Enabled:=false;
   IdbProcessmask:=DestDocument.IdbProcessMask;
   DestDocument.IdbProcessMask:=0;
   StartImport;
   DestDocument.IdbProcessMask:=IdbProcessMask;
end;

procedure TMainWnd.FormShow(Sender: TObject);
begin
   AXImpExpDbc.Visible:=FALSE;
   AxGisProjection.Visible:=FALSE;
   AxGisObjMan.Visible:=FALSE;
end;

procedure TMainWnd.AXImpExpDbcDialogSetuped(Sender: TObject);
begin
   SelAscFilesWnd.DestDbLabel.Caption:=AXImpExpDbc.ImpDbInfo;
   SelAscFilesWnd.PageControl.Enabled:=TRUE;
   SelAscFilesWnd.GeoBtn.Enabled:=TRUE;
   SelAscFilesWnd.DbBtn.Enabled:=TRUE;
   SelAscFilesWnd.OkBtn.Enabled:=SelAscFilesWnd.OkBtnMode;
   SelAscFilesWnd.CancelBtn.Enabled:=TRUE;
end;

procedure TMainWnd.AXImpExpDbcRequestImpLayerIndex(Sender: TObject;
  const Layername: WideString; var LayerIndex: Integer);
var i:integer;
begin
   // the Layerindex of the Layername has to be evaluated
   for i:=0 to DestDocument.Layers.Count-1 do
   begin
      if DestDocument.Layers.Items[i].Layername = Layername then
      begin
         LayerIndex:=DestDocument.Layers.Items[i].Index;
         break;
      end;
   end;
end;

procedure TMainWnd.AXImpExpDbcRequestImpProjectName(Sender: TObject;
  var ProjectName: WideString);
var ProjName:string;
begin
   ProjName:=DestDocument.Name;
   ProjectName:=ProjName;
end;

procedure TMainWnd.AXImpExpDbcRequestImpProjectGUIDs(Sender: TObject;
  var StartGUID, CurrentGUID: WideString);
var aStartGUID, aCurrentGUID:string;
begin
   aStartGUID:=DestDocument.StartProjectGUID;
   aCurrentGUID:=DestDocument.CurrentProjectGUID;
   StartGUID:=aStartGUID;
   CurrentGUID:=aCurrentGUID;
end;

procedure TMainWnd.DisplayStatistics;
var aDate, aTime, DispTime, DispDate:string;
    EndTime, UsedTime:TDateTime;
begin
   StatusLB.Items.Add('---------------------------------------------------');
   // display end time for this file
   aDate:=FormatDateTime('ddmmyy',now);
   DispDate:=copy(aDate,1,2)+'.'+copy(aDate,3,2)+'.'+copy(aDate,5,2);
   aTime:=Formatdatetime('hhnnss',now);
   DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
   StatusLB.Items.Add(StrFileObj.Assign(58) + DispDate+' / '+DispTime); // end time:

   // display number of imported files
   StatusLb.Items.Add(StrFileObj.Assign(59)+inttostr(FilesProcessed));
   // display ellapsed time
   EndTime:=Now;
   UsedTime:=EndTime-OverallStartTime;
   aTime:=Formatdatetime('hhnnss',UsedTime);
   DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
   StatusLB.Items.Add(StrFileObj.Assign(60) + DispTime); // overall used time:

   // display imported data statistics
   if NumLayers     > 0 then StatusLB.Items.Add(StrFileObj.Assign(61) + inttostr(NumLayers));     // created layers:
   if NumSymbols    > 0 then StatusLB.Items.Add(StrFileObj.Assign(62) + inttostr(NumSymbols));    // created symbols:
   if NumPolys      > 0 then StatusLB.Items.Add(StrFileObj.Assign(63) + inttostr(NumPolys));      // created polylines:
   if NumCPolys     > 0 then StatusLB.Items.Add(StrFileObj.Assign(64) + inttostr(NumCPolys));     // created polygons:
   if NumCircles    > 0 then StatusLB.Items.Add(StrFileObj.Assign(65) + inttostr(NumCircles));    // created circles:
   if Numarcs       > 0 then StatusLB.Items.Add(StrFileObj.Assign(66) + inttostr(NumArcs));       // created arcs:
   if NumSplines    > 0 then StatusLB.Items.Add(StrFileObj.Assign(67) + inttostr(NumSplines));    // created splines:
   if NumTexts      > 0 then StatusLB.Items.Add(StrFileObj.Assign(68) + inttostr(NumTexts));      // created texts:
   if NumPixels     > 0 then StatusLB.Items.Add(StrFileObj.Assign(69) + inttostr(NumPixels));     // created points:
   if NumImages     > 0 then StatusLB.Items.Add(StrFileObj.Assign(70) + inttostr(NumImages));     // created images :
   if NumIgnoredObjects > 0 then StatusLB.Items.Add(StrFileObj.Assign(71) + inttostr(NumIgnoredObjects)); // number of ignored duplicates:
   StatusLB.Items.Add('---------------------------------------------------');
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;
end;

end.
