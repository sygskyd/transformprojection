unit struct;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, db, Math;

const
      Col_Int      = 0;
      Col_SmallInt = 1;
      Col_Float    = 2;
      Col_Date     = 3;
      Col_String   = 4;
      // Object types
      ot_Pixel     = 1;
      ot_Poly      = 2;
      ot_CPoly     = 4;
      ot_Text      = 7;
      ot_Symbol    = 8;
      ot_Spline    = 9;
      ot_Image     = 10;
      ot_MesLine   = 11;
      ot_Circle    = 12;
      ot_Arc       = 13;

            //TextSTyles
      ts_Left           =  0;
      ts_Center         =  1;
      ts_Right          =  2;
      ts_Italic         =  4;
      ts_Bold           =  8;
      ts_Underl         = 16;
      ts_FixHeight      = 32;
      ts_Transparent    = 64;

      //LineTypes
      lt_Solid     = 0;
      lt_Dash      = 1;
      lt_Dot       = 2;
      lt_DashDot   = 3;
      lt_DashDDot  = 4;
      lt_UserDefined    = 1000;

      //FillTypes
      pt_NoPattern = 0;
      pt_FDiagonal = 1;
      pt_Cross     = 2;
      pt_DiagCross = 3;
      pt_BDiagonal = 4;
      pt_Horizontal= 5;
      pt_Vertical  = 6;
      pt_Solid     = 7;
      pt_UserDefined = 8;

      stDrawingUnits    = 0;
      stScaleInDependend= 1;
      stProjectScale    = 2;
      stPercent         = 3;

type
  { Hier k�nnen Strukturen definiert werden }

   p_Punkt_record=^Punkt_record;
   Punkt_record = record
       X:double;
       Y:double;
       next :p_Punkt_record;
   end;

   p_Datenbank_record =^Datenbank_record;
   Datenbank_record = record
      Bezeichnung : string;
      Laenge      : integer;
      Typ         : integer;
      Inhalt      : string;
      von         : integer;
      bis         : integer;
      Art         : boolean;
      next        : p_Datenbank_record;
   end;

   p_Layer_record = ^Layer_record;
   Layer_record = record
      Layername:string;
      Linestyle:integer;
      Linecolor:longint;
      Areastyle:integer;
      Areacolor:longint;
      Objectsonlayer:integer;
      next    :p_Layer_record;
   end;

   p_Point_record = ^Point_record;
   Point_record = record
      X        :double;
      Y        :double;
      DBInfo   :p_Datenbank_record;
      Layer    :p_Layer_record;
      next     :p_Point_record;
   end;

   p_Line_record = ^Line_record;
   Line_record = record
      Punktanz :integer;
      Punkte   :p_Punkt_record;
      LastCorner:p_Punkt_record;
      DBInfo   :p_Datenbank_record;
      Layer    :p_Layer_record;
      next     :p_Line_record;
   end;

   p_area_record =^area_record;
   area_record = record
      Punktanz :integer;
      Punkte   :p_Punkt_record;
      LastCorner:p_Punkt_record;
      DBInfo   :p_Datenbank_record;
      Layer    :p_Layer_record;
      next     :p_area_record;
   end;

   p_text_record = ^text_record;
   text_record = record
      X1        :double;
      Y1        :double;
      X2        :double;
      Y2        :double;
      Text      :string;
      Font      :string;
      textsize  :double;
      angle     :double;
      DisplayType:string;
      Alignement:string;
      Layer     :p_Layer_record;
      DBInfo    :p_Datenbank_record;
      next      :p_text_record;
   end;

   p_circle_record = ^circle_record;
   circle_record = record
      X         :double;
      Y         :double;
      Radius    :double;
      Layer     :p_Layer_record;
      DBInfo    :p_Datenbank_record;
      next      :p_circle_record;
   end;

   p_image_record = ^image_record;
   image_record = record
      X         :double;
      Y         :double;
      height    :double;
      width     :double;
      settings  :string;
      Filename  :string;
      Layer     :p_Layer_record;
      DBInfo    :p_Datenbank_record;
      next      :p_image_record;
   end;

   p_arc_record = ^arc_record;
   arc_record = record
      X         :double;
      Y         :double;
      PrimAxis  :double;
      StartAngle:double;
      Angle     :double;
      SecAxis   :double;
      EndAngle  :double;
      Layer     :p_Layer_record;
      DBInfo    :p_Datenbank_record;
      next      :p_arc_record;
   end;


   p_symref_record = ^symref_record;
   symref_record = record
      SymNr_gis   :longint;
      SymNr_sym   :longint;
      next        :p_symref_record;
   end;

   p_symbol_record = ^symbol_record;
   symbol_record = record
      X           :double;
      Y           :double;
      Size        :double;
      Angle       :double;
      SymNr       :integer;
      Layer       :p_Layer_record;
      DBInfo      :p_Datenbank_record;
      next        :p_Symbol_record;
   end;

ClassSymRef = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(Idx_gis:longint;Idx_sym:longint);
      function GetGisSymnr(ASCSymnr:integer):integer;
   private
      { private Definitionen }
      Anfang,Ende,Current:p_Symref_record;
      Anzahl:integer;
end;

ClassSymbol = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X:double;Y:double;Size:string;Angle:string;SymNr:integer;Layer:p_Layer_record);
      procedure WriteData(Layername:string);
      procedure ValDatenbank(Zeile:string);
      function  GetAnzahl:longint;
   private
      { private Definitionen }
      Anfang,Ende,Current:p_Symbol_record;
      Anzahl:integer;
end;

ClassArc = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X:double;Y:double;PrimAxis:double;StartAngle:double;Angle:double; SecAxis:double; EndAngle:double; Layer:p_Layer_record);
      procedure WriteData(Layername:string);
      procedure ValDatenbank(Zeile:string);
      function GetAnzahl:longint;
   private
      { private Definitionen }
      Anfang,Ende,Current:p_Arc_record;
      Anzahl:integer;
end;

ClassImage = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X:double;Y:double; Height:double; Width:double; Settings:string; Filename:string; Layer:p_Layer_record);
      procedure WriteData(Layername:string);
      procedure ValDatenbank(Zeile:string);
      function GetAnzahl:longint;
   private
      { private Definitionen }
      Anfang,Ende,Current:p_Image_record;
      Anzahl:integer;
end;



ClassArea = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(Layer:p_Layer_record);
      procedure InsertPoint(X:double;Y:double);
      procedure WriteData(Layername:string);
      procedure ValDatenbank(Zeile:string);
      function GetAnzahl:longint;
   private
      { private Definitionen }
      Anfang,Ende,Current:p_Area_record;
      Anzahl:integer;
end;

ClassLine = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(Layer:p_Layer_record);
      procedure InsertPoint(X:double;Y:double);
      procedure WriteData(Layername:string);
      procedure ValDatenbank(Zeile:string);
      function GetAnzahl:longint;
   private
      { private Definitionen }
      Anfang,Ende,Current:p_Line_record;
      Anzahl:integer;
end;

ClassSpline = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(Layer:p_Layer_record);
      procedure InsertPoint(X:double;Y:double);
      procedure WriteData(Layername:string);
      procedure ValDatenbank(Zeile:string);
      function GetAnzahl:longint;
   private
      { private Definitionen }
      Anfang,Ende,Current:p_Line_record;
      Anzahl:integer;
end;

ClassPoint = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X:double;Y:double;Layer:p_Layer_record);
      procedure ValDatenbank(Zeile:string);
      procedure WriteData(Layername:string);
      function GetAnzahl:longint;
    private
      { private Definitionen }
      Anfang,Ende,Current:p_point_record;
      Anzahl:integer;
end;

ClassText = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X1:double;Y1:double;X2:double;Y2:double;Text:string;Font:string;size:double;angle:double;DisplayType:string; Alignement:string; Layer:p_Layer_record);
      procedure ValDatenbank(Zeile:string);
      procedure WriteData(Layername:string);
      function GetAnzahl:longint;
    private
      { private Definitionen }
      Anfang,Ende,Current:p_text_record;
      Anzahl:integer;
      function  GetAngle(X1:double;Y1:double;X2:double;Y2:double):double;
      function  GetLength(Text:string;Size:double):double;
      function  GetHeight(Size:double):double;
end;

ClassCircle = class
    public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X:double;Y:double;Radius:string;Layer:p_Layer_record);
      procedure ValDatenbank(Zeile:string);
      procedure WriteData(Layername:string);
      function GetAnzahl:longint;
    private
      { private Definitionen }
      Anfang,Ende,Current:p_circle_record;
      Anzahl:integer;
end;

ClassDatenbank = class
   public
      { public Definitionen }
      constructor Create;
      destructor Destroy;
      procedure Neu(Bezeichnung:string; Laenge:integer; Typ:char; Inhalt:string; von:integer; bis:integer;Art:boolean; var Anfang:p_datenbank_record);
      function  Checkexists(Eintrag:string):boolean;
      procedure SetLength(Bezeichnung:string; Laenge:integer);
      function  GetLength(Bezeichnung:string):integer;
      function  GetItem(Bezeichnung:string):string;
      procedure Getvonbis(Bezeichnung:string;var von:integer; var bis:integer);
      procedure Insertcolumn(Bezeichnung:string; Laenge:integer; Typ:char; Inhalt:string;von:integer;bis:integer;Art:boolean);
      procedure ResetColumn;
      procedure SetDataType(Bezeichnung:string;Inhalt:string);
      function  GetDataType(Bezeichnung:string):integer;

      procedure WriteDatabasecolumns(Layername:string);
   private
      { private Definitionen }
end;

ClassLayer = class
   public
      { public Definitionen }
      constructor Create;
      destructor Destroy;
      procedure Neu(Layername:string);
      function Checkexists(Layername:string):boolean;
      function GetCurrent:p_Layer_record;
      procedure SetLinestyle(Linetype:string;LineColor:string);
      procedure SetAreastyle(Areatype:string;AreaColor:string);
      function Hextoint(Wert:string):longint;
      procedure ResetCurrent;
      procedure SetToLastLayer;
      function GetAnzahl:longint;
      function GetCurrentLayername:string;
      procedure SetCurrentnext;
      procedure SetCurrentPrevious;
   private
      { private Definitionen }
      Anfang,Ende,Current:p_Layer_record;
      CurrLayerIndex:integer;
      Anzahl:integer;
end;

implementation
uses files,maindlg;

var Datenbank_Anfang, Akt_Datenbank:p_Datenbank_record;
    FileObj                         :ClassFile;
    DatenbankObj                    :ClassDatenbank;

constructor ClassDatenbank.Create;
begin
   Datenbank_Anfang:=new(p_Datenbank_record); Datenbank_Anfang:=nil;
   Akt_Datenbank:=new(p_Datenbank_record); Akt_Datenbank:=nil;
end;

constructor ClassArea.Create;
begin
   Anfang:=new(p_Area_record); Ende:=new(p_Area_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Area_record); Current:=nil; Anzahl:=0; 
end;

constructor ClassSymbol.Create;
begin
   Anfang:=new(p_Symbol_record); Ende:=new(p_Symbol_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Symbol_record); Current:=nil; Anzahl:=0;
end;

constructor ClassArc.Create;
begin
   Anfang:=new(p_Arc_record); Ende:=new(p_Arc_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Arc_record); Current:=nil; Anzahl:=0;
end;

constructor ClassImage.Create;
begin
   Anfang:=new(p_Image_record); Ende:=new(p_Image_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Image_record); Current:=nil; Anzahl:=0;
end;

constructor ClassSymRef.Create;
begin
   Anfang:=new(p_Symref_record); Ende:=new(p_Symref_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Symref_record); Current:=nil; Anzahl:=0;
end;

constructor ClassText.Create;
begin
   Anfang:=new(p_Text_record); Ende:=new(p_Text_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Text_record); Current:=nil; Anzahl:=0;
end;

constructor ClassCircle.Create;
begin
   Anfang:=new(p_circle_record); Ende:=new(p_circle_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_circle_record); Current:=nil; Anzahl:=0;
end;

constructor ClassLine.Create;
begin
   Anfang:=new(p_Line_record); Ende:=new(p_Line_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Line_record); Current:=nil; Anzahl:=0;
end;

constructor ClassSpline.Create;
begin
   Anfang:=new(p_Line_record); Ende:=new(p_Line_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Line_record); Current:=nil; Anzahl:=0;
end;

constructor ClassPoint.Create;
begin
   Anfang:=new(p_Point_record); Ende:=new(p_Point_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Point_record); Current:=nil; Anzahl:=0;
end;

constructor ClassLayer.Create;
begin
   Anfang:=new(p_Layer_record); Ende:=new(p_Layer_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Layer_record); Current:=nil; Anzahl:=0; CurrLayerIndex:=0;
end;

destructor ClassDatenbank.Destroy;
var Zeiger1,Zeiger2:p_Datenbank_record;
begin
   Zeiger1:=Datenbank_Anfang;
   if Datenbank_Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      dispose(Zeiger2);
   until Zeiger1 = nil;
   Datenbank_Anfang:=nil; Akt_Datenbank:=nil;
end;

destructor ClassPoint.Destroy;
var Zeiger1,Zeiger2:p_Point_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassSymbol.Destroy;
var Zeiger1,Zeiger2:p_Symbol_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassArc.Destroy;
var Zeiger1,Zeiger2:p_Arc_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassImage.Destroy;
var Zeiger1,Zeiger2:p_Image_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassText.Destroy;
var Zeiger1,Zeiger2:p_Text_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassSymRef.Destroy;
var Zeiger1,Zeiger2:p_SymRef_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassCircle.Destroy;
var Zeiger1,Zeiger2:p_Circle_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassLayer.Destroy;
var Zeiger1,Zeiger2:p_Layer_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassArea.Destroy;
var Zeiger1,Zeiger2:p_Area_record;
    Punkt1,Punkt2: p_Punkt_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Punkte }
      Punkt1:=Zeiger2^.Punkte; Punkt2:=Zeiger2^.Punkte;
      if Punkt1 <> nil then
      repeat
         Punkt2:=Punkt1;
         Punkt1:=Punkt1^.next;
         dispose(Punkt2);
      until Punkt1 = nil;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassLine.Destroy;
var Zeiger1,Zeiger2:p_Line_record;
    Punkt1,Punkt2: p_Punkt_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Punkte }
      Punkt1:=Zeiger2^.Punkte; Punkt2:=Zeiger2^.Punkte;
      if Punkt1 <> nil then
      repeat
         Punkt2:=Punkt1;
         Punkt1:=Punkt1^.next;
         dispose(Punkt2);
      until Punkt1 = nil;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassSpline.Destroy;
var Zeiger1,Zeiger2:p_Line_record;
    Punkt1,Punkt2: p_Punkt_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Punkte }
      Punkt1:=Zeiger2^.Punkte; Punkt2:=Zeiger2^.Punkte;
      if Punkt1 <> nil then
      repeat
         Punkt2:=Punkt1;
         Punkt1:=Punkt1^.next;
         dispose(Punkt2);
      until Punkt1 = nil;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;


{*******************************************************************************
* FUNKTION : ClassDatenbank.Checkexists                                        *
********************************************************************************
* Funktion �berpr�ft, ob der entsprechende Spalteneintrag bereits in der       *
* Datenbankliste ist.                                                          *
*                                                                              *
* R�ckgabewert: TRUE  -> Eintrag ist bereits in Liste.                         *
*               FALSE -> Eintrag ist noch nicht in Liste.                      *
*                                                                              *
********************************************************************************}
function ClassDatenbank.Checkexists(Eintrag:string):boolean;
var Zeiger:p_Datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   if Zeiger = nil then begin Result:=false; exit; end;
   repeat
      if Zeiger^.Bezeichnung = Eintrag then gefunden:= true;
      Zeiger:=Zeiger^.next;
   until (Zeiger = nil) or (gefunden = true);
   Result:=gefunden;
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.SetDataType                                        *
********************************************************************************
* Prozedur wertet das erhaltende Datenbankattribut aus und bestimmt so den     *
* Datentyp f�r die gesamte Spalte.                                             *
*                                                                              *
* PARAMETER: Bezeichnung   -> Spaltenname in die das Attribut eingetragen wird *
*            Inhalt        -> Attribut f�r diese Spalte.                       *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.SetDataType(Bezeichnung:string;Inhalt:string);
var intpossible,floatpossible,gefunden:boolean;
    hint,count:longint;
    hfloat:double;
    Zeiger:p_Datenbank_record;
begin
   intpossible:=false; floatpossible:=false;

   Zeiger:=Datenbank_Anfang; gefunden:=false;
   { Setzen des Zeigers an die entsprechende Datenbankspalte  }
   repeat
      if Zeiger <> nil then if Zeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
      if gefunden = false then zeiger:=zeiger^.next;
   until (gefunden = TRUE) or (Zeiger = nil);

   if gefunden = true then
   begin
      { Kontrolle, ob der Attributwert eine Integervariable ist }
      val(Inhalt,hint,count); if count = 0 then intpossible:=true;
      { Kontrolle, ob der Attributwert eine Floatvariable ist }
      val(Inhalt,hfloat,count); if count = 0 then floatpossible:=true;
      { Nun wird der Datentyp zugewiesen }
      if (Zeiger^.Typ = Col_Int) and (intpossible = FALSE) then Zeiger^.Typ:=Col_Float;
      if (Zeiger^.Typ = Col_Float) and (floatpossible = FALSE) then Zeiger^.Typ:=Col_String;
   end;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.GetDataType                                        *
********************************************************************************
* Funktion ermittelt den Datentyp, den eine Datenbankspalte verwendet.         *
*                                                                              *
* PARAMETER: Bezeichnung   -> Spaltenname von der der Typ ermittelt werden soll*
*                                                                              *
* R�CKGABEWERT: Art des Datentyps                                              *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetDataType(Bezeichnung:string):integer;
var gefunden:boolean;
    Zeiger:p_Datenbank_record;
begin
   Zeiger:=Datenbank_Anfang; gefunden:=false;
   { Setzen des Zeigers an die entsprechende Datenbankspalte  }
   repeat
      if Zeiger <> nil then if Zeiger^.Bezeichnung = Bezeichnung then gefunden:=true;
      if gefunden = false then zeiger:=zeiger^.next;
   until (gefunden = TRUE) or (Zeiger = nil);

   if gefunden = true then Result:=Zeiger^.Typ;
end;

{*******************************************************************************
* PROZEDUR : ClassArea.ValDatenbank                                            *
********************************************************************************
* Prozedur wertet die Datenbankinformation aus der erhaltenen Zeile aus.       *
*                                                                              *
* PARAMETER: Zeile    -> String aus der die Datenbankinformation ausgelesen    *
*                        wird.                                                 *
*                                                                              *
********************************************************************************}
procedure ClassArea.ValDatenbank(Zeile:string);
var Zeiger:p_Datenbank_record;
    hstr:string;
    von,bis:integer;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_Anfang; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   if Zeiger <> nil then
   repeat
      if Zeiger^.Art = TRUE then gefunden:=true;
      if gefunden = false then zeiger:=zeiger^.next;
   until (gefunden = TRUE) or (Zeiger = nil);
   if zeiger <> nil then
   repeat
      DatenbankObj.Getvonbis(Zeiger^.Bezeichnung,von,bis); hstr:=copy(Zeile,von,bis-von + 1);
      hstr:=TrimLeft(hstr); hstr:=TrimRight(hstr);
      { Nun muss auch der Datentyp ermittelt werden }
      DatenbankObj.SetDataType(Zeiger^.Bezeichnung,hstr);
      DatenbankObj.Neu(Zeiger^.Bezeichnung,0,'C',hstr,0,0,TRUE,Current^.DBInfo);
      Zeiger:=Zeiger^.next;
   until Zeiger = nil;
end;

{*******************************************************************************
* PROZEDUR : ClassSymbol.ValDatenbank                                          *
********************************************************************************
* Prozedur wertet die Datenbankinformation aus der erhaltenen Zeile aus.       *
*                                                                              *
* PARAMETER: Zeile    -> String aus der die Datenbankinformation ausgelesen    *
*                        wird.                                                 *
*                                                                              *
********************************************************************************}
procedure ClassSymbol.ValDatenbank(Zeile:string);
var Zeiger:p_Datenbank_record;
    hstr:string;
    von,bis:integer;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_Anfang; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   if Zeiger <> nil then
   repeat
      if Zeiger^.Art = TRUE then gefunden:=true;
      if gefunden = false then zeiger:=zeiger^.next;
   until (gefunden = TRUE) or (Zeiger = nil);
   if zeiger <> nil then
   repeat
      DatenbankObj.Getvonbis(Zeiger^.Bezeichnung,von,bis); hstr:=copy(Zeile,von,bis-von + 1);
      hstr:=TrimLeft(hstr); hstr:=TrimRight(hstr);
      { Nun muss auch der Datentyp ermittelt werden }
      DatenbankObj.SetDataType(Zeiger^.Bezeichnung,hstr);
      DatenbankObj.Neu(Zeiger^.Bezeichnung,0,'C',hstr,0,0,TRUE,Current^.DBInfo);
      Zeiger:=Zeiger^.next;
   until Zeiger = nil;
end;

{*******************************************************************************
* PROZEDUR : ClassArc.ValDatenbank                                             *
********************************************************************************
* Prozedur wertet die Datenbankinformation aus der erhaltenen Zeile aus.       *
*                                                                              *
* PARAMETER: Zeile    -> String aus der die Datenbankinformation ausgelesen    *
*                        wird.                                                 *
*                                                                              *
********************************************************************************}
procedure ClassArc.ValDatenbank(Zeile:string);
var Zeiger:p_Datenbank_record;
    hstr:string;
    von,bis:integer;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_Anfang; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   if Zeiger <> nil then
   repeat
      if Zeiger^.Art = TRUE then gefunden:=true;
      if gefunden = false then zeiger:=zeiger^.next;
   until (gefunden = TRUE) or (Zeiger = nil);
   if zeiger <> nil then
   repeat
      DatenbankObj.Getvonbis(Zeiger^.Bezeichnung,von,bis); hstr:=copy(Zeile,von,bis-von + 1);
      hstr:=TrimLeft(hstr); hstr:=TrimRight(hstr);
      { Nun muss auch der Datentyp ermittelt werden }
      DatenbankObj.SetDataType(Zeiger^.Bezeichnung,hstr);
      DatenbankObj.Neu(Zeiger^.Bezeichnung,0,'C',hstr,0,0,TRUE,Current^.DBInfo);
      Zeiger:=Zeiger^.next;
   until Zeiger = nil;
end;

{*******************************************************************************
* PROZEDUR : ClassImage.ValDatenbank                                           *
********************************************************************************
* Prozedur wertet die Datenbankinformation aus der erhaltenen Zeile aus.       *
*                                                                              *
* PARAMETER: Zeile    -> String aus der die Datenbankinformation ausgelesen    *
*                        wird.                                                 *
*                                                                              *
********************************************************************************}
procedure ClassImage.ValDatenbank(Zeile:string);
var Zeiger:p_Datenbank_record;
    hstr:string;
    von,bis:integer;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_Anfang; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   if Zeiger <> nil then
   repeat
      if Zeiger^.Art = TRUE then gefunden:=true;
      if gefunden = false then zeiger:=zeiger^.next;
   until (gefunden = TRUE) or (Zeiger = nil);
   if zeiger <> nil then
   repeat
      DatenbankObj.Getvonbis(Zeiger^.Bezeichnung,von,bis); hstr:=copy(Zeile,von,bis-von + 1);
      hstr:=TrimLeft(hstr); hstr:=TrimRight(hstr);
      { Nun muss auch der Datentyp ermittelt werden }
      DatenbankObj.SetDataType(Zeiger^.Bezeichnung,hstr);
      DatenbankObj.Neu(Zeiger^.Bezeichnung,0,'C',hstr,0,0,TRUE,Current^.DBInfo);
      Zeiger:=Zeiger^.next;
   until Zeiger = nil;
end;

{*******************************************************************************
* PROZEDUR : ClassCircle.ValDatenbank                                          *
********************************************************************************
* Prozedur wertet die Datenbankinformation aus der erhaltenen Zeile aus.       *
*                                                                              *
* PARAMETER: Zeile    -> String aus der die Datenbankinformation ausgelesen    *
*                        wird.                                                 *
*                                                                              *
********************************************************************************}
procedure ClassCircle.ValDatenbank(Zeile:string);
var Zeiger:p_Datenbank_record;
    hstr:string;
    von,bis:integer;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_Anfang; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   if Zeiger <> nil then
   repeat
      if Zeiger^.Art = TRUE then gefunden:=true;
      if gefunden = false then zeiger:=zeiger^.next;
   until (gefunden = TRUE) or (Zeiger = nil);
   if zeiger <> nil then
   repeat
      DatenbankObj.Getvonbis(Zeiger^.Bezeichnung,von,bis); hstr:=copy(Zeile,von,bis-von + 1);
      hstr:=TrimLeft(hstr); hstr:=TrimRight(hstr);
      
      { Nun muss auch der Datentyp ermittelt werden }
      DatenbankObj.SetDataType(Zeiger^.Bezeichnung,hstr);

      DatenbankObj.Neu(Zeiger^.Bezeichnung,0,'C',hstr,0,0,TRUE,Current^.DBInfo);
      Zeiger:=Zeiger^.next;
   until Zeiger = nil;
end;

{*******************************************************************************
* PROZEDUR : ClassText.ValDatenbank                                            *
********************************************************************************
* Prozedur wertet die Datenbankinformation aus der erhaltenen Zeile aus.       *
*                                                                              *
* PARAMETER: Zeile    -> String aus der die Datenbankinformation ausgelesen    *
*                        wird.                                                 *
*                                                                              *
********************************************************************************}
procedure ClassText.ValDatenbank(Zeile:string);
var Zeiger:p_Datenbank_record;
    hstr:string;
    von,bis:integer;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_Anfang; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   if Zeiger <> nil then
   repeat
      if Zeiger^.Art = TRUE then gefunden:=true;
      if gefunden = false then zeiger:=zeiger^.next;
   until (gefunden = TRUE) or (Zeiger = nil);
   if zeiger <> nil then
   repeat
      DatenbankObj.Getvonbis(Zeiger^.Bezeichnung,von,bis); hstr:=copy(Zeile,von,bis-von + 1);
      hstr:=TrimLeft(hstr); hstr:=TrimRight(hstr);
      { Nun muss auch der Datentyp ermittelt werden }
      DatenbankObj.SetDataType(Zeiger^.Bezeichnung,hstr);
      DatenbankObj.Neu(Zeiger^.Bezeichnung,0,'C',hstr,0,0,TRUE,Current^.DBInfo);
      Zeiger:=Zeiger^.next;
   until Zeiger = nil;
end;

{*******************************************************************************
* PROZEDUR : ClassPoint.ValDatenbank                                           *
********************************************************************************
* Prozedur wertet die Datenbankinformation aus der erhaltenen Zeile aus.       *
*                                                                              *
* PARAMETER: Zeile    -> String aus der die Datenbankinformation ausgelesen    *
*                        wird.                                                 *
*                                                                              *
********************************************************************************}
procedure ClassPoint.ValDatenbank(Zeile:string);
var Zeiger:p_Datenbank_record;
    hstr:string;
    von,bis:integer;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_Anfang; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   if Zeiger <> nil then
   repeat
      if Zeiger^.Art = TRUE then gefunden:=true;
      if gefunden = false then zeiger:=zeiger^.next;
   until (gefunden = TRUE) or (Zeiger = nil);
   if zeiger <> nil then
   repeat
      DatenbankObj.Getvonbis(Zeiger^.Bezeichnung,von,bis); hstr:=copy(Zeile,von,bis-von + 1);
      hstr:=TrimLeft(hstr); hstr:=TrimRight(hstr);
      { Nun muss auch der Datentyp ermittelt werden }
      DatenbankObj.SetDataType(Zeiger^.Bezeichnung,hstr);
      DatenbankObj.Neu(Zeiger^.Bezeichnung,0,'C',hstr,0,0,TRUE,Current^.DBInfo);
      Zeiger:=Zeiger^.next;
   until Zeiger = nil;
end;

{*******************************************************************************
* PROZEDUR : ClassLine.ValDatenbank                                            *
********************************************************************************
* Prozedur wertet die Datenbankinformation aus der erhaltenen Zeile aus.       *
*                                                                              *
* PARAMETER: Zeile    -> String aus der die Datenbankinformation ausgelesen    *
*                        wird.                                                 *
*                                                                              *
********************************************************************************}
procedure ClassLine.ValDatenbank(Zeile:string);
var Zeiger:p_Datenbank_record;
    hstr:string;
    von,bis:integer;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_Anfang; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   if Zeiger <> nil then
   repeat
      if Zeiger^.Art = TRUE then gefunden:=true;
      if gefunden = false then zeiger:=zeiger^.next;
   until (gefunden = TRUE) or (Zeiger = nil);
   if zeiger <> nil then
   repeat
      DatenbankObj.Getvonbis(Zeiger^.Bezeichnung,von,bis); hstr:=copy(Zeile,von,bis-von + 1);
      hstr:=TrimLeft(hstr); hstr:=TrimRight(hstr);
      { Nun muss auch der Datentyp ermittelt werden }
      DatenbankObj.SetDataType(Zeiger^.Bezeichnung,hstr);

      DatenbankObj.Neu(Zeiger^.Bezeichnung,0,'C',hstr,0,0,TRUE,Current^.DBInfo);
      Zeiger:=Zeiger^.next;
   until Zeiger = nil;
end;

{*******************************************************************************
* PROZEDUR : ClassSpline.ValDatenbank                                          *
********************************************************************************
* Prozedur wertet die Datenbankinformation aus der erhaltenen Zeile aus.       *
*                                                                              *
* PARAMETER: Zeile    -> String aus der die Datenbankinformation ausgelesen    *
*                        wird.                                                 *
*                                                                              *
********************************************************************************}
procedure ClassSpline.ValDatenbank(Zeile:string);
var Zeiger:p_Datenbank_record;
    hstr:string;
    von,bis:integer;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_Anfang; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   if Zeiger <> nil then
   repeat
      if Zeiger^.Art = TRUE then gefunden:=true;
      if gefunden = false then zeiger:=zeiger^.next;
   until (gefunden = TRUE) or (Zeiger = nil);
   if zeiger <> nil then
   repeat
      DatenbankObj.Getvonbis(Zeiger^.Bezeichnung,von,bis); hstr:=copy(Zeile,von,bis-von + 1);
      hstr:=TrimLeft(hstr); hstr:=TrimRight(hstr);
      { Nun muss auch der Datentyp ermittelt werden }
      DatenbankObj.SetDataType(Zeiger^.Bezeichnung,hstr);
      DatenbankObj.Neu(Zeiger^.Bezeichnung,0,'C',hstr,0,0,TRUE,Current^.DBInfo);
      Zeiger:=Zeiger^.next;
   until Zeiger = nil;
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.InsertColumn                                       *
********************************************************************************
* Prozedur erzeugt einen neuen Spalteneintrag f�r die Datenbank und f�gt diesen*
* in die Datenbank_anfang Liste ein.                                           *
*                                                                              *
* PARAMETER: Bezeichnung -> Spaltenbezeichnung                                 *
*            Laenge      -> L�nge der Spalte                                   *
*            Typ         -> Typ der Spalte (C oder N)                          *
*            Inhalt      -> Inhalt der Spalte                                  *
*            von         -> Beginn der Spalte im ASC-File                      *
*            bis         -> Ende der Spalte im ASC-File                        *
*            Art         -> TRUE, wenn es sich um DB-Spalte handelt, sonst F   *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.Insertcolumn(Bezeichnung:string; Laenge:integer; Typ:char; Inhalt:string;von:integer;bis:integer;Art:boolean);
begin
   Neu(Bezeichnung, Laenge, Typ, Inhalt,von,bis,Art,Datenbank_anfang);
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.Getvonbis                                          *
********************************************************************************
* Prozedur dient dazu den von und bis Bereich aus der Datenbankliste zu        *
* erhalten.                                                                    *
*                                                                              *
* PARAMETER: Bezeichnung -> Name der Spalte aus dem ADF-File                   *
*            von         -> von Bereich aus dem ASC-File                       *
*            bis         -> bis Bereich aus dem ASC-File                       *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.Getvonbis(Bezeichnung:string;var von:integer; var bis:integer);
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then
   begin
      von:=Zeiger^.von;
      bis:=Zeiger^.bis;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.ResetColumn                                        *
********************************************************************************
* Prozedur dient dazu den Aktellen Spalteneintragszeiger auf den Anfang zu     *
* setzen.                                                                      *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.ResetColumn;
begin
   Akt_Datenbank:=Datenbank_Anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.ResetCurrent                                           *
********************************************************************************
* Proezdur setzt den Current Layerzeiger auf den Anfang.                       *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassLayer.ResetCurrent;
begin
   Current:=Anfang;
end;

procedure ClassLayer.SetToLastLayer;
begin
   Current:=Ende; CurrLayerIndex:=Anzahl;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.SetCurrentnext                                         *
********************************************************************************
* Proezdur setzt den Current Layerzeiger auf den n�chsten Eintrag.             *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassLayer.SetCurrentnext;
begin
   if Current^.next <> nil then Current:=Current^.next;
end;

procedure ClassLayer.SetCurrentPrevious;
var i:integer;
begin
    CurrLayerIndex:=CurrLayerIndex-1;
    Current:=Anfang;
    if CurrLayerIndex > 1 then
    begin
       for i:=1 to CurrLayerIndex - 1 do
          Current:=Current^.next;
    end;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.GetAnzahl                                              *
********************************************************************************
* Funktion ermittelt die Anzahl der vorhandenen Layer.                         *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�CKGABEWERT: Anzahl der Layer                                               *
*                                                                              *
********************************************************************************}
function ClassLayer.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassSymbol.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassImage.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassArc.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassArea.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassLine.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassSpline.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassPoint.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassText.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassCircle.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;


{*******************************************************************************
* FUNKTION : ClassLayer.GetCurrentLayername                                    *
********************************************************************************
* Funktion ermittelt den aktuellen Layernamen.                                 *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�CKGABEWERT: Name des Layers.                                               *
*                                                                              *
********************************************************************************}
function ClassLayer.GetCurrentLayername:string;
begin
   Result:=Current^.Layername;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.Checkexists                                            *
********************************************************************************
* Funktion �berpr�ft, ob dieser Layer bereits in der Liste vorkommt.           *
*                                                                              *
* PARAMETER: Layername -> Layername, dessen Vorkommen �berpr�ft werden soll.   *
*                                                                              *
* R�CKGABEWERT: TRUE -> Layer existiert bereits                                *
*               FALSE -> Layer existiert noch nicht                            *
********************************************************************************}
function ClassLayer.CheckExists(Layername:string):boolean;
var Zeiger:p_Layer_record;
    gefunden:boolean;
begin
   Zeiger:=Anfang; gefunden:=false;
   if Zeiger = nil then result:=false
   else
   begin
      repeat
         if Zeiger^.Layername <> Layername then Zeiger:=Zeiger^.next else gefunden:=true;
      until (gefunden = true) or (Zeiger = nil);
   end;
   if (Zeiger = nil) then result:=false else begin result:=true; Current:=Zeiger; end;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.SetLinestyle                                           *
********************************************************************************
* Prozedur dient zum setzen des Linientyps und der Linienfarbe auf dem Current *
* Layer.                                                                       *
*                                                                              *
* PARAMETER: Linetype  -> Typ, der Linie                                       *
*            Linecolor -> Farbe der Linie                                      *
*                                                                              *
********************************************************************************}
procedure ClassLayer.SetLinestyle(Linetype:string;LineColor:string);
var rot,gruen,blau:longint;
begin
   { Nun muss aus dem WinGIS Linientyp und dessen Farbe ermittelt werden }
   if (Linetype = 'Dot' )       or (Linetype = 'Punktiert'  )      then Current^.Linestyle:=lt_Dot     else
   if (Linetype = 'Dash')       or (Linetype = 'Strichliert')      then Current^.Linestyle:=lt_Dash    else
   if (Linetype = 'DashDot')    or (Linetype = 'StrichPunkt')      then Current^.Linestyle:=lt_DashDot else
   if (Linetype = 'DashDotDot') or (Linetype = 'StrichPunktPunkt') then Current^.Linestyle:=lt_DashDDot else
      Current^.Linestyle:=lt_Solid;

   {if LineColor = '$000000' then LineColor:='$FFFFFF' else
   if LineColor = '$FFFFFF' then LineColor:='$000000';  }

   blau :=Hextoint(copy(LineColor,2,2));
   gruen:=Hextoint(copy(LineColor,4,2));
   rot  :=Hextoint(copy(LineColor,6,2));

   Current^.Linecolor:=(blau * 65536)+(gruen * 256)+(rot);
end;

{*******************************************************************************
* FUNKTION : ClassLayer.Hextoint                                               *
********************************************************************************
* Prozedur dient zum Auswerten einer zweistelligen Hexadezimalzahl             *
*                                                                              *
* PARAMETER: Wert -> String, der Hexadezimalzahl beinhaltet.                   *
*                                                                              *
* R�CKGABEWERT: Integerwert der Hexzahl                                        *
*                                                                              *
********************************************************************************}
function ClassLayer.Hextoint(Wert:string):longint;
var Zeichen1,Zeichen2:string;
    Wert1,Wert2:longint;
    count:integer;
begin
   Zeichen1:=copy(Wert,1,1); Zeichen2:=copy(Wert,2,1);
   if Zeichen1 = 'A' then Wert1:=10 else
   if Zeichen1 = 'B' then Wert1:=11 else
   if Zeichen1 = 'C' then Wert1:=12 else
   if Zeichen1 = 'D' then Wert1:=13 else
   if Zeichen1 = 'E' then Wert1:=14 else
   if Zeichen1 = 'F' then Wert1:=15 else
      val(Zeichen1,Wert1,count);
   Wert1:=Wert1 * 16;

   if Zeichen2 = 'A' then Wert2:=10 else
   if Zeichen2 = 'B' then Wert2:=11 else
   if Zeichen2 = 'C' then Wert2:=12 else
   if Zeichen2 = 'D' then Wert2:=13 else
   if Zeichen2 = 'E' then Wert2:=14 else
   if Zeichen2 = 'F' then Wert2:=15 else
      val(Zeichen2,Wert2,count);

   Result:=Wert1 + Wert2;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.SetAreastyle                                           *
********************************************************************************
* Prozedur dient zum setzen des Areatyps und der Areafarbe auf dem Current     *
* Layer.                                                                       *
*                                                                              *
* PARAMETER: Areatype  -> Typ, der Fl�che                                      *
*            Areacolor -> Farbe der Fl�che                                     *
*                                                                              *
********************************************************************************}
procedure ClassLayer.SetAreastyle(Areatype:string;AreaColor:string);
var rot,gruen,blau:longint;
begin
   { Nun muss aus dem WinGIS Linientyp und dessen Farbe der Mapinfo Farbtyp und dessen Farbe ermittelt werden }
   if (Areatype = 'Solid' )        or (Areatype = 'Massiv'  )         then Current^.Areastyle:=pt_Solid       else
   if (Areatype = 'Horizontal')    or (Areatype = 'Waagrecht')        then Current^.Areastyle:=pt_Horizontal  else
   if (Areatype = 'Vertical')      or (Areatype = 'Senkrecht')        then Current^.Areastyle:=pt_Vertical    else
   if (Areatype = 'DiagonalLeft')  or (Areatype = 'DiagonalLinks')    then Current^.Areastyle:=pt_FDiagonal   else
   if (Areatype = 'DiagonalRigth') or (Areatype = 'DiagonalRechts')   then Current^.Areastyle:=pt_BDiagonal   else
   if (Areatype = 'Cross')         or (Areatype = 'Gitter')           then Current^.Areastyle:=pt_Cross       else
   if (Areatype = 'DiagonalCross') or (Areatype = 'GitterDiagonal')   then Current^.Areastyle:=pt_DiagCross  else
      Current^.Areastyle:=pt_NoPattern;

   {if AreaColor = '$000000' then AreaColor:='$FFFFFF' else
   if AreaColor = '$FFFFFF' then AreaColor:='$000000'; }

   blau :=Hextoint(copy(AreaColor,2,2));
   gruen:=Hextoint(copy(AreaColor,4,2));
   rot  :=Hextoint(copy(AreaColor,6,2));

   Current^.Areacolor:=(blau * 65536)+(gruen * 256)+(rot);
end;

{*******************************************************************************
* FUNKTION : ClassLayer.GetCurrent                                             *
********************************************************************************
* Funktion gibt den Current Layerzeiger zur�ck.                                *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�CKGABEWERT: Zeiger auf den Current Layer.                                  *
********************************************************************************}
function ClassLayer.GetCurrent:p_Layer_record;
begin
   Result:=Current;
end;

procedure ClassArc.Neu(X:double;Y:double;PrimAxis:double;StartAngle:double;Angle:double; SecAxis:double; EndAngle:double; Layer:p_Layer_record);
var neuer_record:p_Arc_record;
begin
   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.PrimAxis:=PrimAxis;
   neuer_record^.StartAngle:=StartAngle;
   neuer_record^.Angle:=Angle;
   neuer_record^.SecAxis:=SecAxis;
   neuer_record^.EndAngle:=EndAngle;
   neuer_record^.Layer:=Layer;
   neuer_record^.DBInfo:=nil;

   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

procedure ClassImage.Neu(X:double;Y:double; Height:double; Width:double; Settings:string; Filename:string; Layer:p_Layer_record);
var neuer_record:p_Image_record;
begin
   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.Height:=Height;
   neuer_record^.Width:=Width;
   neuer_record^.Settings:=Settings;
   neuer_record^.Filename:=Filename;
   neuer_record^.Layer:=Layer;
   neuer_record^.DBInfo:=nil;

   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

procedure ClassSymbol.Neu(X:double;Y:double;Size:string;Angle:string;SymNr:integer;Layer:p_Layer_record);
var neuer_record:p_Symbol_record;
    value:double;
    count:integer;
begin
   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=y;
   val(Size,value,count); if count <> 0 then value:=1; neuer_record^.Size:=value;
   val(Angle,value,count); if count <> 0 then value:=0; neuer_record^.Angle:=value;
   neuer_record^.Symnr:=SymNr+600000000;
   neuer_record^.Layer:=Layer;
   neuer_record^.DBInfo:=nil;

   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;


{*******************************************************************************
* PROZEDUR : ClassText.Neu                                                     *
********************************************************************************
* Prozedur erzeugt einen neuen Text Eintrag in der Liste.                      *
*                                                                              *
* PARAMETER: X1    -> Erste X-Koordinate des Texts                             *
*            Y1    -> Erste Y-Koordinate des Texts                             *
*            X2    -> Zweite X-Koordinate des Texts                            *
*            Y2    -> Zweite Y-Koordinate des Texts                            *
*            Text  -> Inhalt des Texts                                         *
*            Font  -> Font, der f�r diesen Text verwendet wird                 *
*            Size  -> Gr��e des Texts                                          *
*            DisplayType -> Art wie angezeigt wird (bold/italic/underlined)    *
*            Alignement  -> Art wie Text ausgerichtet ist (left/right/centered)*
*            Layer -> Zeiger auf den Layer auf dem sich dieser Text befindet.  *
*                                                                              *
********************************************************************************}
procedure ClassText.Neu(X1:double;Y1:double;X2:double;Y2:double;Text:string;Font:string;size:double;angle:double;DisplayType:string; Alignement:string; Layer:p_Layer_record);
var neuer_record:p_Text_record;
begin
   new(neuer_record);
   neuer_record^.X1:=X1;
   neuer_record^.Y1:=Y1;
   neuer_record^.X2:=X2;
   neuer_record^.Y2:=Y2;

   // truncate leading and following spaces
   neuer_record^.Text:=TrimRight(Text);
   neuer_record^.Text:=TrimLeft(neuer_record^.Text);

   neuer_record^.Font:=Font;
   neuer_record^.TextSize:=size;
   neuer_record^.angle:=angle;
   neuer_record^.DisplayType:=DisplayType;
   neuer_record^.Alignement:=Alignement;
   neuer_record^.Layer:=Layer;
   neuer_record^.DBInfo:=nil;

   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

function ClassSymRef.GetGisSymnr(ASCSymNr:integer):integer;
var Zeiger:p_Symref_record;
    found:boolean;
    retVal:integer;
begin
   Zeiger:=Anfang; retVal:=0; found:=false;
   while (Zeiger <> nil) and (not found) do
   begin
      if Zeiger^.SymNr_sym = ASCSymNr then
      begin
         retVal:=Zeiger^.SymNr_gis;
         found:=true;
      end;
      Zeiger:=Zeiger^.next;
   end;
   result:=retVal;
end;

procedure ClassSymRef.Neu(Idx_gis:longint;Idx_sym:longint);
var neuer_record:p_Symref_record;
begin
   new(neuer_record);
   neuer_record^.SymNr_Gis:=Idx_gis;
   neuer_record^.SymNr_sym:=Idx_sym;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassCircle.Neu                                                   *
********************************************************************************
* Prozedur erzeugt einen neuen Circle Eintrag in der Liste.                    *
*                                                                              *
* PARAMETER: X     -> X-Koordinate des Kreiseinsetzpunktes                     *
*            Y     -> Y-Koordinate des Kreiseinsetzpunktes                     *
*            Radius-> Radius des Kreises                                       *
*            Layer -> Zeiger auf den Layer auf dem sich dieser Kreis befindet. *
*                                                                              *
********************************************************************************}
procedure ClassCircle.Neu(X:double;Y:double;Radius:string;Layer:p_Layer_record);
var neuer_record:p_Circle_record;
    count:integer;
begin
   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.Layer:=Layer;
   neuer_record^.DBInfo:=nil;
   val(Radius,neuer_record^.Radius,count);
   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassArea.Neu                                                     *
********************************************************************************
* Prozedur erzeugt einen neuen Area Eintrag in der Liste.                      *
*                                                                              *
* PARAMETER: Layer -> Zeiger auf den Layer auf dem sich diese Area befindet.   *
*                                                                              *
********************************************************************************}
procedure ClassArea.Neu(Layer:p_Layer_record);
var neuer_record:p_Area_record;
begin
   new(neuer_record);
   neuer_record^.DBInfo:=nil;
   neuer_record^.Punkte:=nil;
   neuer_record^.Layer:=Layer;
   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;
   neuer_record^.Punktanz:=0;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassLine.Neu                                                     *
********************************************************************************
* Prozedur erzeugt einen neuen Line Eintrag in der Liste.                      *
*                                                                              *
* PARAMETER: Layer -> Zeiger auf den Layer auf dem sich diese Line befindet.   *
*                                                                              *
********************************************************************************}
procedure ClassLine.Neu(Layer:p_Layer_record);
var neuer_record:p_Line_record;
begin
   new(neuer_record);
   neuer_record^.DBInfo:=nil;
   neuer_record^.Punkte:=nil;
   neuer_record^.Layer:=Layer;
   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;
   neuer_record^.Punktanz:=0;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassSpline.Neu                                                   *
********************************************************************************
* Prozedur erzeugt einen neuen Line Eintrag in der Liste.                      *
*                                                                              *
* PARAMETER: Layer -> Zeiger auf den Layer auf dem sich diese Line befindet.   *
*                                                                              *
********************************************************************************}
procedure ClassSpline.Neu(Layer:p_Layer_record);
var neuer_record:p_Line_record;
begin
   new(neuer_record);
   neuer_record^.DBInfo:=nil;
   neuer_record^.Punkte:=nil;
   neuer_record^.Layer:=Layer;
   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;
   neuer_record^.Punktanz:=0;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassPoint.Neu                                                    *
********************************************************************************
* Prozedur erzeugt einen neuen Point Eintrag in der Liste.                     *
*                                                                              *
* PARAMETER: X -> X-Koordinate des Punkts                                      *
*            Y -> Y-Koordinate des Punkts                                      *
*            Layer -> Zeiger auf den Layer auf dem sich dieser Point befindet. *
*                                                                              *
********************************************************************************}
procedure ClassPoint.Neu(X:double;Y:double;Layer:p_Layer_record);
var neuer_record:p_Point_record;
begin
   new(neuer_record);
   neuer_record^.DBInfo:=nil;
   neuer_record^.Layer:=Layer;
   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassArea.InsertPoint                                             *
********************************************************************************
* Prozedur f�gt einen Punkt in die Current Area ein.                           *
*                                                                              *
* PARAMETER: X -> X-Koordinate des Punktes                                     *
*            Y -> Y-Koordinate des Punktes                                     *
*                                                                              *
********************************************************************************}
procedure ClassArea.InsertPoint(X:double;Y:double);
var Zeiger:p_Punkt_record;
    new_Zeiger:p_Punkt_record;
    hstr      :string;
begin
   if Current = nil then exit;
   new(new_Zeiger);
   { Kontrolle, ob die Spalten X und Y bereits in der Datenbankliste sind }
   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   Zeiger:=Current^.Punkte;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current^.Punkte:=New_Zeiger;
      Current^.Punkte^.next:=NIL;
      Current^.LastCorner:=New_zeiger;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      Current^.LastCorner^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
      Current^.LastCorner:=new_Zeiger;
   end;
   Current^.Punktanz:=Current^.Punktanz + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassArea.WriteData                                               *
********************************************************************************
* Prozedur schreibt die Daten der Area, die sich auf einem bestimmten          *
*                                                                              *
* PARAMETER: Layername -> Name des Layers auf dem sich die Objekte befinden    *
*                         sollen.
*                                                                              *
********************************************************************************}
procedure ClassArea.WriteData(Layername:string);
var Zeiger:p_Area_record;
    Punktzeiger:p_Punkt_record;
    DBZeiger:p_Datenbank_record;
    hstr:string;
    laenge:integer;
    new_ID:integer;
    FirstObject:boolean;
    HasDuplicate:boolean;
begin
   Zeiger:=Anfang; FirstObject:=true;
   while (Zeiger <> nil) and (not MainWnd.Abort) do
   begin
      if Zeiger^.Layer^.Layername = Layername then
      begin
         if (FirstObject) then
         begin
            if MainWnd.CheckLayerExists(Layername) = FALSE then
            begin
               if Zeiger <> nil then
               begin
                  MainWnd.CreateLayer(Layername);
                  MainWnd.SetLayerPriorities(Zeiger^.Layer^.Linestyle,Zeiger^.Layer^.Linecolor,Zeiger^.Layer^.Areastyle,Zeiger^.Layer^.Areacolor);
               end;
            end;
            FirstObject:=False;
         end;
         MainWnd.AddAndShowPercent;
         MainWnd.CreatePolygon;
         { Nun werden die eigentlichen Punkte geschrieben }
         Punktzeiger:=Zeiger^.Punkte;
         while (Punktzeiger <> nil) do
         begin
            MainWnd.InsertPointToPolygon(Punktzeiger^.X,Punktzeiger^.Y);
            Punktzeiger:=Punktzeiger^.next;
         end;

         new_ID:=MainWnd.ClosePolygon(HasDuplicate);

         if (new_id <> 0) then
         begin
            // write database information
            if HasDuplicate then
               MainWnd.AXImpExpDbc.DoEditImpInfo(Layername,new_ID)
            else
               MainWnd.AXImpExpDbc.AddImpIdInfo(Layername,new_ID);
            DBZeiger:=Zeiger^.DBInfo;
            while (DBZeiger <> nil) do
            begin
               MainWnd.AXImpExpDbc.AddImpInfo(Layername,DBZeiger^.Bezeichnung,DBZeiger^.Inhalt);
               DBZeiger:=DBZeiger^.next;
            end;
         end;
      end;
      Zeiger:=Zeiger^.next;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassLine.WriteData                                               *
********************************************************************************
* Prozedur schreibt die Daten der Line, die sich auf einem bestimmten          *
*                                                                              *
* PARAMETER: Layername -> Name des Layers auf dem sich die Objekte befinden    *
*                         sollen.
*                                                                              *
********************************************************************************}
procedure ClassLine.WriteData(Layername:string);
var Zeiger:p_Line_record;
    Punktzeiger:p_Punkt_record;
    DBZeiger:p_Datenbank_record;
    laenge:integer;
    Zeile,hstr:string;
    new_ID:integer;
    FirstObject:boolean;
    HasDuplicate:boolean;
begin
   Zeiger:=Anfang; FirstObject:=true;
   while (Zeiger <> nil) and (not MainWnd.Abort) do
   begin
      if Zeiger^.Layer^.Layername = Layername then
      begin
         if (FirstObject) then
         begin
            if MainWnd.CheckLayerExists(Layername) = FALSE then
            begin
               if Zeiger <> nil then
               begin
                  MainWnd.CreateLayer(Layername);
                  MainWnd.SetLayerPriorities(Zeiger^.Layer^.Linestyle,Zeiger^.Layer^.Linecolor,Zeiger^.Layer^.Areastyle,Zeiger^.Layer^.Areacolor);
               end;
            end;
            FirstObject:=False;
         end;
         MainWnd.AddAndShowPercent;

         { Nun werden die eigentlichen Punkte geschrieben }
         Punktzeiger:=Zeiger^.Punkte;
         MainWnd.CreatePolyline;
         while (Punktzeiger <> nil) do
         begin
            MainWnd.InsertPointToPolyLine(Punktzeiger^.X,Punktzeiger^.Y);
            Punktzeiger:=Punktzeiger^.next;
         end;
         { Nun wird die Pen-Information geschrieben }
         new_ID:=MainWnd.ClosePolyline(HasDuplicate);

         if new_ID <> 0 then
         begin
            // write database information
            if HasDuplicate then
               MainWnd.AXImpExpDbc.DoEditImpInfo(Layername,new_ID)
            else
               MainWnd.AXImpExpDbc.AddImpIdInfo(Layername,new_ID);
            DBZeiger:=Zeiger^.DBInfo;
            while (DBZeiger <> nil) do
            begin
               MainWnd.AXImpExpDbc.AddImpInfo(Layername,DBZeiger^.Bezeichnung,DBZeiger^.Inhalt);
               DBZeiger:=DBZeiger^.next;
            end;
         end;
      end;
      Zeiger:=Zeiger^.next;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassLine.WriteData                                               *
********************************************************************************
* Prozedur schreibt die Daten der Line, die sich auf einem bestimmten          *
*                                                                              *
* PARAMETER: Layername -> Name des Layers auf dem sich die Objekte befinden    *
*                         sollen.
*                                                                              *
********************************************************************************}
procedure ClassSpline.WriteData(Layername:string);
var Zeiger:p_Line_record;
    Punktzeiger:p_Punkt_record;
    DBZeiger:p_Datenbank_record;
    laenge:integer;
    Zeile,hstr:string;
    new_ID:integer;
    FirstObject:boolean;
    HasDuplicate:boolean;
begin
   Zeiger:=Anfang; FirstObject:=true;
   while (Zeiger <> nil) and (not MainWnd.Abort) do
   begin
      if Zeiger^.Layer^.Layername = Layername then
      begin
         if (FirstObject) then
         begin
            if MainWnd.CheckLayerExists(Layername) = FALSE then
            begin
               if Zeiger <> nil then
               begin
                  MainWnd.CreateLayer(Layername);
                  MainWnd.SetLayerPriorities(Zeiger^.Layer^.Linestyle,Zeiger^.Layer^.Linecolor,Zeiger^.Layer^.Areastyle,Zeiger^.Layer^.Areacolor);
               end;
            end;
            FirstObject:=False;
         end;
         { Nun werden die eigentlichen Punkte geschrieben }
         Punktzeiger:=Zeiger^.Punkte;
         MainWnd.AddAndShowPercent;
         MainWnd.CreateSpline;
         while (Punktzeiger <> nil) do
         begin
            MainWnd.InsertPointToSpline(Punktzeiger^.X,Punktzeiger^.Y);
            Punktzeiger:=Punktzeiger^.next;
         end;
         { Nun wird die Pen-Information geschrieben }
         new_ID:=MainWnd.CloseSpline(HasDuplicate);

         if (new_id <> 0) then
         begin
            // write database information
            if HasDuplicate then
               MainWnd.AXImpExpDbc.DoEditImpInfo(Layername,new_ID)
            else
               MainWnd.AXImpExpDbc.AddImpIdInfo(Layername,new_ID);
            DBZeiger:=Zeiger^.DBInfo;
            while (DBZeiger <> nil) do
            begin
               MainWnd.AXImpExpDbc.AddImpInfo(Layername,DBZeiger^.Bezeichnung,DBZeiger^.Inhalt);
               DBZeiger:=DBZeiger^.next;
            end;
         end;
      end;
      Zeiger:=Zeiger^.next;
   end;
end;

procedure ClassSymbol.WriteData(Layername:string);
var Zeiger:p_Symbol_record;
    DBZeiger:p_Datenbank_record;
    Zeile,hstr,hZeile:string;
    new_ID,Height,Angle:integer;
    FirstObject:boolean;
    HasDuplicate:boolean;
begin
   Zeiger:=Anfang; FirstObject:=true;
   while (Zeiger <> nil) and (not MainWnd.Abort) do
   begin
      if Zeiger^.Layer^.Layername = Layername then
      begin
         if (FirstObject) then
         begin
            if MainWnd.CheckLayerExists(Layername) = FALSE then
            begin
               if Zeiger <> nil then
               begin
                  MainWnd.CreateLayer(Layername);
                  MainWnd.SetLayerPriorities(Zeiger^.Layer^.Linestyle,Zeiger^.Layer^.Linecolor,Zeiger^.Layer^.Areastyle,Zeiger^.Layer^.Areacolor);
               end;
            end;
            FirstObject:=False;
         end;
         MainWnd.AddAndShowPercent;
         new_ID:=MainWnd.InsertSymbol(Zeiger^.X,Zeiger^.Y,Zeiger^.Size,Zeiger^.Angle,Zeiger^.Symnr, HasDuplicate);

         if (new_Id <> 0) then
         begin
            // write database information
            if HasDuplicate then
               MainWnd.AXImpExpDbc.DoEditImpInfo(Layername,new_ID)
            else
               MainWnd.AXImpExpDbc.AddImpIdInfo(Layername,new_ID);
            DBZeiger:=Zeiger^.DBInfo;
            while (DBZeiger <> nil) do
            begin
               MainWnd.AXImpExpDbc.AddImpInfo(Layername,DBZeiger^.Bezeichnung, DBZeiger^.Inhalt);
               DBZeiger:=DBZeiger^.next;
            end;
         end;
      end;
      Zeiger:=Zeiger^.next;
   end;
end;

procedure ClassArc.WriteData(Layername:string);
var Zeiger:p_Arc_record;
    DBZeiger:p_Datenbank_record;
    Zeile,hstr,hZeile:string;
    new_ID:integer;
    dummy:string;
    PrimAxis,SecAxis:integer;
    FirstObject:boolean;
    HasDuplicate:boolean;
begin
   Zeiger:=Anfang; FirstObject:=true;
   while (Zeiger <> nil) and (not MainWnd.Abort) do
   begin
      if Zeiger^.Layer^.Layername = Layername then
      begin
         if (FirstObject) then
         begin
            if MainWnd.CheckLayerExists(Layername) = FALSE then
            begin
               if Zeiger <> nil then
               begin
                  MainWnd.CreateLayer(Layername);
                  MainWnd.SetLayerPriorities(Zeiger^.Layer^.Linestyle,Zeiger^.Layer^.Linecolor,Zeiger^.Layer^.Areastyle,Zeiger^.Layer^.Areacolor);
               end;
            end;
            FirstObject:=False;
         end;
         dummy:=floattostr(Zeiger^.PrimAxis*100);
         if (pos('.',dummy) <> 0) then dummy:=copy(dummy,1,pos('.',dummy)-1);
         if (pos(',',dummy) <> 0) then dummy:=copy(dummy,1,pos(',',dummy)-1);

         PrimAxis:=strtoint(dummy);
         dummy:=floattostr(Zeiger^.SecAxis*100);
         if (pos('.',dummy) <> 0) then dummy:=copy(dummy,1,pos('.',dummy)-1);
         if (pos(',',dummy) <> 0) then dummy:=copy(dummy,1,pos(',',dummy)-1);
         SecAxis:=strtoint(dummy);

         MainWnd.AddAndShowPercent;
         new_ID:=MainWnd.InsertArc(Zeiger^.X,Zeiger^.Y,PrimAxis,Zeiger^.Angle,Zeiger^.StartAngle,SecAxis,Zeiger^.EndAngle, HasDuplicate);

         if (new_id <> 0) then
         begin
            // write database information
            if HasDuplicate then
               MainWnd.AXImpExpDbc.DoEditImpInfo(Layername,new_ID)
            else
               MainWnd.AXImpExpDbc.AddImpIdInfo(Layername,new_ID);
            DBZeiger:=Zeiger^.DBInfo;
            while (DBZeiger <> nil) do
            begin
               MainWnd.AXImpExpDbc.AddImpInfo(Layername,DBZeiger^.Bezeichnung, DBZeiger^.Inhalt);
               DBZeiger:=DBZeiger^.next;
            end;
         end;
      end;
      Zeiger:=Zeiger^.next;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassText.WriteData                                               *
********************************************************************************
* Prozedur schreibt die Daten des Textes, die sich auf einem bestimmten        *
* Layer befinden.                                                              *
*                                                                              *
* PARAMETER: Layername -> Name des Layers auf dem sich die Objekte befinden    *
*                         sollen.                                              *
*                                                                              *
********************************************************************************}
procedure ClassText.WriteData(Layername:string);
var Zeiger:p_Text_record;
    DBZeiger:p_Datenbank_record;
    Zeile,hstr,hZeile:string;
    new_ID,Height,Angle:integer;
    FirstObject:boolean;
    DisplayType:integer;
    Alignement:integer;
    HasDuplicate:boolean;
begin
   Zeiger:=Anfang; FirstObject:=true;
   while (Zeiger <> nil) and (not MainWnd.Abort) do
   begin
      if Zeiger^.Layer^.Layername = Layername then
      begin
         if (FirstObject) then
         begin
            if MainWnd.CheckLayerExists(Layername) = FALSE then
            begin
               if Zeiger <> nil then
               begin
                  MainWnd.CreateLayer(Layername);
                  MainWnd.SetLayerPriorities(Zeiger^.Layer^.Linestyle,Zeiger^.Layer^.Linecolor,Zeiger^.Layer^.Areastyle,Zeiger^.Layer^.Areacolor);
               end;
            end;
            FirstObject:=False;
         end;
         Zeiger^.Text:=TRIMRIGHT(Zeiger^.Text);
         if Pos('"',Zeiger^.Text) <> 0 then repeat Delete(Zeiger^.Text,Pos('"',Zeiger^.Text),1); until Pos('"',Zeiger^.Text) = 0;

         hZeile:=floattostr(Zeiger^.TextSize*100);
         if Pos('.',hZeile) <> 0 then hZeile:=copy(hZeile,1,Pos('.',hZeile)-1);
         if Pos(',',hZeile) <> 0 then hZeile:=copy(hZeile,1,Pos(',',hZeile)-1);
         Height:=strtoint(hZeile);

         if Zeiger^.Angle <> 0 then
            hZeile:=floattostr(Zeiger^.Angle)
         else
            hZeile:=floattostr(GetAngle(Zeiger^.X1,Zeiger^.Y1,Zeiger^.X2,Zeiger^.Y2));
         if Pos('.',hZeile) <> 0 then hZeile:=copy(hZeile,1,Pos('.',hZeile)-1);
         if Pos(',',hZeile) <> 0 then hZeile:=copy(hZeile,1,Pos(',',hZeile)-1);
         Angle:=strtoint(hZeile);

         // get displaytype
         DisplayType:=0;
         // check if italic
         if (Pos('K',Zeiger^.DisplayType) <> 0) or
            (Pos('Kursiv',Zeiger^.DisplayType) <> 0) or
            (Pos('Italic',Zeiger^.DisplayType) <> 0) or
            (Pos('I',Zeiger^.DisplayType) <> 0) then DisplayType:=Displaytype + ts_Italic;

         // check if underlined
         if (Pos('U',Zeiger^.DisplayType) <> 0) or
            (Pos('Unterstrichen',Zeiger^.DisplayType) <> 0) or
            (Pos('Underlined',Zeiger^.DisplayType) <> 0) then DisplayType:=Displaytype + ts_Underl;

         // check if bold
         if (Pos('F',Zeiger^.DisplayType) <> 0) or
            (Pos('Fett',Zeiger^.DisplayType) <> 0) or
            (Pos('Bold',Zeiger^.DisplayType) <> 0) or
            (Pos('B',Zeiger^.DisplayType) <> 0) then DisplayType:=DisplayType + ts_Bold;

         // check if transparent
         if (Pos('T',Zeiger^.DisplayType) <> 0) or
            (Pos('Transparent',Zeiger^.DisplayType) <> 0) then DisplayType:=DisplayType+ts_Transparent;

         // check if fixed height
         if (Pos('H',Zeiger^.DisplayType) <> 0) or
            (Pos('FixeH�he',Zeiger^.DisplayType) <> 0) or
            (Pos('FixHeight',Zeiger^.DisplayType) <> 0) then DisplayType:=DisplayType+ts_FixHeight;

         // get alignement
         // check if left aligned
         if (Pos('Links',Zeiger^.Alignement) <> 0) or
            (Pos('Linksb�ndig',Zeiger^.Alignement) <> 0) or
            (Pos('L',Zeiger^.Alignement) <> 0) or
            (Pos('Left',Zeiger^.Alignement) <> 0) or
            (Pos('LeftAligned',Zeiger^.Alignement) <> 0) then DisplayType:=DisplayType+ts_Left;

         // check if right aligned
         if (Pos('Rechtsb�ndig',Zeiger^.Alignement) <> 0) or
            (Pos('Rechts',Zeiger^.Alignement) <> 0) or
            (Pos('R',Zeiger^.Alignement) <> 0) or
            (Pos('RightAligned',Zeiger^.Alignement) <> 0) or
            (Pos('Right',Zeiger^.Alignement) <> 0) then DisplayType:=DisplayType+ts_Right;

         // check if centered
         if (Pos('Mittig',Zeiger^.Alignement) <> 0) or
            (Pos('Zentriert',Zeiger^.Alignement) <> 0) or
            (Pos('M',Zeiger^.Alignement) <> 0) or
            (Pos('Z',Zeiger^.Alignement) <> 0) or
            (Pos('Centered',Zeiger^.Alignement) <> 0) or
            (Pos('C',Zeiger^.Alignement) <> 0) then DisplayType:=DisplayType+ts_Center;

         MainWnd.AddAndShowPercent;
         new_ID:=MainWnd.InsertText(Zeiger^.X1,Zeiger^.Y1,Zeiger^.X2,Zeiger^.Y2,Angle,Zeiger^.Text,Height,Zeiger^.font,DisplayType, HasDuplicate);

         if (new_id <> 0) then
         begin
            // write database information
            if HasDuplicate then
               MainWnd.AXImpExpDbc.DoEditImpInfo(Layername,new_ID)
            else
               MainWnd.AXImpExpDbc.AddImpIdInfo(Layername,new_ID);
            DBZeiger:=Zeiger^.DBInfo;
            while (DBZeiger <> nil) do
            begin
               MainWnd.AXImpExpDbc.AddImpInfo(Layername,DBZeiger^.Bezeichnung, DBZeiger^.Inhalt);
               DBZeiger:=DBZeiger^.next;
            end;
         end;
      end;
      Zeiger:=Zeiger^.next;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassCircle.WriteData                                             *
********************************************************************************
* Prozedur schreibt die Daten des Kreises, die sich auf einem bestimmten       *
* Layer befinden.                                                              *
*                                                                              *
* PARAMETER: Layername -> Name des Layers auf dem sich die Objekte befinden    *
*                         sollen.                                              *
*                                                                              *
********************************************************************************}
procedure ClassCircle.WriteData(Layername:string);
var Zeiger:p_Circle_record;
    DBZeiger:p_Datenbank_record;
    Zeile,hstr:string;
    Radius,new_id:integer;
    FirstObject:boolean;
    HasDuplicate:boolean;
begin
   Zeiger:=Anfang; FirstObject:=true;
   while (Zeiger <> nil) and (not MainWnd.Abort) do
   begin
      if Zeiger^.Layer^.Layername = Layername then
      begin
         if (FirstObject) then
         begin
            if MainWnd.CheckLayerExists(Layername) = FALSE then
            begin
               if Zeiger <> nil then
               begin
                  MainWnd.CreateLayer(Layername);
                  MainWnd.SetLayerPriorities(Zeiger^.Layer^.Linestyle,Zeiger^.Layer^.Linecolor,Zeiger^.Layer^.Areastyle,Zeiger^.Layer^.Areacolor);
               end;
            end;
            FirstObject:=False;
         end;
         hstr:=floattostr(Zeiger^.Radius * 100);
         if Pos('.',hstr) <> 0 then hstr:=copy(hstr,1,Pos('.',hstr)-1);
         if Pos(',',hstr) <> 0 then hstr:=copy(hstr,1,Pos(',',hstr)-1);
         Radius:=strtoint(hstr);
         MainWnd.AddAndShowPercent;
         new_id:=MainWnd.CreateEllipse(Zeiger^.X,Zeiger^.Y,Radius,Radius, HasDuplicate);

         if (new_id <> 0) then
         begin
            // write database information
            if HasDuplicate then
               MainWnd.AXImpExpDbc.DoEditImpInfo(Layername,new_ID)
            else
               MainWnd.AXImpExpDbc.AddImpIdInfo(Layername,new_ID);
            DBZeiger:=Zeiger^.DBInfo;
            while (DBZeiger <> nil) do
            begin
               MainWnd.AXImpExpDbc.AddImpInfo(Layername,DBZeiger^.Bezeichnung, DBZeiger^.Inhalt);
               DBZeiger:=DBZeiger^.next;
            end;
         end;
      end;
      Zeiger:=Zeiger^.next;
   end;
end;

{*******************************************************************************
* FUNKTION : ClassText.GetLength                                               *
********************************************************************************
* Funktion ermittelt die L�nge eines Textes in Meter                           *
*                                                                              *
* PARAMETER: Text -> Text, von dem die L�nge ermittelt werden soll.            *
*            Size -> Gr��e, die der Text hat.                                  *
*                                                                              *
* R�CKGABEWERT: L�nge des Texts in Meter.                                      *
*                                                                              *
********************************************************************************}
function ClassText.GetLength(Text:string;Size:double):double;
begin
    Result:=length(Text) * (Size * 4/10);
end;


{*******************************************************************************
* FUNKTION : ClassText.GetHeight                                               *
********************************************************************************
* Funktion ermittelt die H�he eines Textes in Meter                            *
*                                                                              *
* PARAMETER: Size -> Gr��e, die der Text hat.                                  *
*                                                                              *
* R�CKGABEWERT: H�he des Texts in Meter.                                       *
*                                                                              *
********************************************************************************}
function ClassText.GetHeight(Size:double):double;
begin
   Result:=Size * 6 / 10;
end;

{*******************************************************************************
* FUNKTION : ClassText.GetAngle                                                *
********************************************************************************
* Funktion ermittelt den Winkel mit dem ein Text rotiert ist.                  *
*                                                                              *
* PARAMETER: X1 -> Erste X-Koordinate des Texts                                *
*            Y1 -> Erste Y-Koordinate des Texts                                *
*            X2 -> Zweite X-Koordinate des Texts                               *
*            Y2 -> Zweite Y-Koordinate des Texts                               *
*                                                                              *
* R�CKGABEWERT: Winkel mit den dieser Text rotiert ist                         *
*                                                                              *
********************************************************************************}
function ClassText.GetAngle(X1:double;Y1:double;X2:double;Y2:double):double;
var A,B,C:double;
    Alpha:double;
begin
   { Zuwerst werden die Kantenl�ngen des Dreiecks ermittelt }
   A:=ABS(Y2 - Y1); B:=ABS(X2 - X1); C:=SQRT(SQR(A) + SQR(B));

   if C <> 0 then
   begin
      Alpha:=(A / C);  { Nun ist der Sinus von Alpha bekannt }

      { Nun muss man noch den Invers Sinus von Alpha berechnen }
      Alpha:=ArcSin(Alpha);
      Result:=(Alpha*180) / PI;
   end
   else
      Result:=0;
end;

{*******************************************************************************
* PROZEDUR : ClassPoint.WriteData                                              *
********************************************************************************
* Prozedur schreibt die Daten des Points, die sich auf einem bestimmten        *
* Layer befinden.                                                              *
*                                                                              *
* PARAMETER: Layername -> Name des Layers auf dem sich die Objekte befinden    *
*                         sollen.                                              *
*                                                                              *
********************************************************************************}
procedure ClassPoint.WriteData(Layername:string);
var Zeiger:p_Point_record;
    DBZeiger:p_Datenbank_record;
    laenge,new_ID:integer;
    Zeile,hstr:string;
    FirstObject:boolean;
    HasDuplicate:boolean;
begin
   Zeiger:=Anfang; FirstObject:=true;
   while (Zeiger <> nil) and (not MainWnd.Abort) do
   begin
      if Zeiger^.Layer^.Layername = Layername then
      begin
         if (FirstObject) then
         begin
            if MainWnd.CheckLayerExists(Layername) = FALSE then
            begin
               if Zeiger <> nil then
               begin
                  MainWnd.CreateLayer(Layername);
                  MainWnd.SetLayerPriorities(Zeiger^.Layer^.Linestyle,Zeiger^.Layer^.Linecolor,Zeiger^.Layer^.Areastyle,Zeiger^.Layer^.Areacolor);
               end;
            end;
            FirstObject:=False;
         end;

         MainWnd.AddAndShowPercent;

         new_ID:=MainWnd.InsertPoint(Zeiger^.X,Zeiger^.Y,HasDuplicate);

         if (new_id <> 0) then
         begin
            // write database information
            if HasDuplicate then
               MainWnd.AXImpExpDbc.DoEditImpInfo(Layername,new_ID)
            else
               MainWnd.AXImpExpDbc.AddImpIdInfo(Layername,new_ID);
            DBZeiger:=Zeiger^.DBInfo;
            while (DBZeiger <> nil) do
            begin
               MainWnd.AXImpExpDbc.AddImpInfo(Layername,DBZeiger^.Bezeichnung, DBZeiger^.Inhalt);
               DBZeiger:=DBZeiger^.next;
            end;
         end;
      end;
      Zeiger:=Zeiger^.next;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassImage.WriteData                                              *
********************************************************************************
* Prozedur schreibt die Daten des Images, die sich auf einem bestimmten        *
* Layer befinden.                                                              *
*                                                                              *
* PARAMETER: Layername -> Name des Layers auf dem sich die Objekte befinden    *
*                         sollen.                                              *
*                                                                              *
********************************************************************************}
procedure ClassImage.WriteData(Layername:string);
var Zeiger:p_Image_record;
    DBZeiger:p_Datenbank_record;
    laenge,new_ID:integer;
    Zeile,hstr:string;
    FirstObject:boolean;
    HasDuplicate:boolean;
begin
   Zeiger:=Anfang; FirstObject:=true;
   while (Zeiger <> nil) and (not MainWnd.Abort) do
   begin
      if Zeiger^.Layer^.Layername = Layername then
      begin
         if (FirstObject) then
         begin
            if MainWnd.CheckLayerExists(Layername) = FALSE then
            begin
               if Zeiger <> nil then
               begin
                  MainWnd.CreateLayer(Layername);
                  MainWnd.SetLayerPriorities(Zeiger^.Layer^.Linestyle,Zeiger^.Layer^.Linecolor,Zeiger^.Layer^.Areastyle,Zeiger^.Layer^.Areacolor);
               end;
            end;
            FirstObject:=False;
         end;

         MainWnd.AddAndShowPercent;

         new_ID:=MainWnd.InsertImage(Zeiger^.X,Zeiger^.Y,Zeiger^.Height,Zeiger^.Width,Zeiger^.Settings,Zeiger^.Filename, HasDuplicate);

         if (new_id <> 0) then
         begin
            // write database information
            if HasDuplicate then
               MainWnd.AXImpExpDbc.DoEditImpInfo(Layername,new_ID)
            else
               MainWnd.AXImpExpDbc.AddImpIdInfo(Layername,new_ID);
            DBZeiger:=Zeiger^.DBInfo;
            while (DBZeiger <> nil) do
            begin
               MainWnd.AXImpExpDbc.AddImpInfo(Layername,DBZeiger^.Bezeichnung, DBZeiger^.Inhalt);
               DBZeiger:=DBZeiger^.next;
            end;
         end;
      end;
      Zeiger:=Zeiger^.next;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassLine.InsertPoint                                             *
********************************************************************************
* Prozedur f�gt einen Punkt in die Current Line ein.                           *
*                                                                              *
* PARAMETER: X -> X-Koordinate des Punktes                                     *
*            Y -> Y-Koordinate des Punktes                                     *
*                                                                              *
********************************************************************************}
procedure ClassLine.InsertPoint(X:double;Y:double);
var Zeiger:p_Punkt_record;
    new_Zeiger:p_Punkt_record;
    hstr      :string;
begin
   if Current = nil then exit;
   new(new_Zeiger);
   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   Zeiger:=Current^.Punkte;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current^.Punkte:=New_Zeiger;
      Current^.Punkte^.next:=NIL;
      Current^.LastCorner:=New_Zeiger;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      Current^.LastCorner^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
      Current^.LastCorner:=new_Zeiger;
   end;
   Current^.Punktanz:=Current^.Punktanz + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassSpline.InsertPoint                                           *
********************************************************************************
* Prozedur f�gt einen Punkt in die Current Spline ein.                         *
*                                                                              *
* PARAMETER: X -> X-Koordinate des Punktes                                     *
*            Y -> Y-Koordinate des Punktes                                     *
*                                                                              *
********************************************************************************}
procedure ClassSpline.InsertPoint(X:double;Y:double);
var Zeiger:p_Punkt_record;
    new_Zeiger:p_Punkt_record;
    hstr      :string;
begin
   new(new_Zeiger);
   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   Zeiger:=Current^.Punkte;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current^.Punkte:=New_Zeiger;
      Current^.Punkte^.next:=NIL;
      Current^.LastCorner:=New_Zeiger;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      Current^.LastCorner^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
      Current^.LastCorner:=new_Zeiger;
   end;
   Current^.Punktanz:=Current^.Punktanz + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.Neu                                                    *
********************************************************************************
* Prozedur erzeugt einen neuen Eintrag in der Layerlist.                       *
*                                                                              *
* PARAMETER: Layername -> Name des Layers, der neu eingef�gt werden soll.      *
*                                                                              *
********************************************************************************}
procedure ClassLayer.Neu(Layername:string);
var neuer_record,Zeiger:p_Layer_record;
begin
   new(neuer_record);
   neuer_record^.Layername:=Layername;
   neuer_record^.Linestyle:=0;
   neuer_record^.Linecolor:=0;
   neuer_record^.Areastyle:=0;
   neuer_record^.Areacolor:=0;
   neuer_record^.ObjectsonLayer:=0;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.Neu                                                *
********************************************************************************
* Prozedur erzeugt einen neuen Spalteneintrag f�r die Datenbank.               *
*                                                                              *
* PARAMETER: Bezeichnung -> Spaltenbezeichnung                                 *
*            Laenge      -> L�nge der Spalte                                   *
*            Typ         -> Typ der Spalte (C oder N)                          *
*            Inhalt      -> Inhalt der Spalte                                  *
*            von         -> Beginn der Spalte im ASC-File                      *
*            bis         -> Ende der Spalte im ADF-File                        *
*            Art         -> Art der Spalte T -> DB-Spalte F -> ADF-Spalte      *
*            Anfang      -> Zeiger auf Anfang der Liste in die eingef�gt wird  *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.Neu(Bezeichnung:string; Laenge:integer; Typ:char; Inhalt:string; von:integer; bis:integer; Art:boolean; var Anfang:p_datenbank_record);
var neuer_record,Zeiger:p_datenbank_record;
begin
   new(neuer_record);

   neuer_record^.Bezeichnung:=Bezeichnung;
   neuer_record^.Laenge:=Laenge;
   neuer_record^.Typ:=Col_Int;
   neuer_record^.Inhalt:=Inhalt;
   neuer_record^.von:=von;
   neuer_record^.bis:=bis;
   neuer_record^.Art:=Art;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=Anfang;
      repeat
         if Zeiger^.next <> nil then Zeiger:=Zeiger^.next;
      until Zeiger^.next = nil;
      Zeiger^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
   end;
end;


{*******************************************************************************
* PROZEDUR : ClassDatenbank.SetLength                                          *
********************************************************************************
* Prozedur dient dazu die erforderliche Spaltenbreite einer Spalte zu setzen.  *
*                                                                              *
* InPARAMETER: Bezeichnung -> Name der Spalte                                  *
*              Laenge      -> Breite der Spalte                                *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.SetLength(Bezeichnung:string; Laenge:integer);
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if Zeiger^.Laenge < Laenge then Zeiger^.Laenge:=Laenge;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.GetLength                                          *
********************************************************************************
* Funktion dient zum ermitteln der erforderlichen Spaltenbreite f�r einen ADF  *
* Eintrag.                                                                     *
*                                                                              *
* PARAMETER:   Bezeichnung -> Name der Spalte                                  *
*                                                                              *
* R�ckgabewert: Spaltenbreite f�r die entsprechende Spalte.                    *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetLength(Bezeichnung:string):integer;
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then Result:=Zeiger^.Laenge else Result:=0;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.GetItem                                            *
********************************************************************************
* Funktion dient zum Ermitteln eines Datenbankwertes.                          *
*                                                                              *
*                                                                              *
* PARAMETER:   Bezeichnung -> Name der Spalte                                  *
*                                                                              *
* R�ckgabewert: Inhalt dieser Spalte.                                          *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetItem(Bezeichnung:string):string;
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=Datenbank_anfang; gefunden:=false;
   repeat
      if Zeiger^.Bezeichnung = Bezeichnung then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then Result:=Zeiger^.Inhalt else Result:='';
end;

procedure ClassDatenbank.WriteDatabasecolumns(Layername:string);
var Zeiger,Zeiger2:p_Datenbank_record;
    Zeile,hstr:string;
    anzahl,i:integer;
    gefunden:boolean;
begin
   MainWnd.AXImpExpDbc.CheckAndCreateImpTable(Layername);
   MainWnd.AXImpExpDbc.AddImpIdColumn(Layername);

   Zeiger:=Datenbank_Anfang; gefunden:=false;
   { Setzen des Zeigers an die Datenbankinformation }
   if Zeiger <> nil then
   repeat
      if Zeiger^.Art = TRUE then gefunden:=true;
      if gefunden = false then zeiger:=zeiger^.next;
   until (gefunden = TRUE) or (Zeiger = nil);

   if zeiger <> nil then { Es sind Datenbankspalten vorhanden }
   begin
      { Nun muss noch ermittelt werden, wie viele Datenbankspalten vorhanden sind }
      Zeiger2:=Zeiger; anzahl:=0;
      while Zeiger2 <> nil do begin Zeiger2:=Zeiger2^.next; anzahl:=anzahl + 1; end;
      repeat
         { Nun werden die Spaltendefinitionen geschrieben }
         MainWnd.AXImpExpDbc.AddImpColumn(Layername,Zeiger^.Bezeichnung,Zeiger^.Typ,Zeiger^.Laenge);
         Zeiger:=Zeiger^.next;
      until Zeiger = nil;
   end;
   MainWnd.AXImpExpDbc.CreateImpTable(Layername);
   MainWnd.AXImpExpDbc.OpenImpTable(Layername);
end;

end.

