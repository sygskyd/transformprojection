unit SelASCFileDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ComCtrls, FileCtrl, StrFile, Gauges,
  ShellCtrls, FileSelectionPanel, WinOSInfo;

type
  TSelASCFilesWnd = class(TForm)
    PageControl: TPageControl;
    TabSheetFiles: TTabSheet;
    RadioGroup1: TRadioGroup;
    OKBtn: TButton;
    CancelBtn: TButton;
    OpenDialogsameADF: TOpenDialog;
    OpenDialogSameSYM: TOpenDialog;
    TabSheetDB: TTabSheet;
    Panel1: TPanel;
    GeoBtn: TButton;
    SetupWindowTimer: TTimer;
    Panel2: TPanel;
    DestDbLabel: TLabel;
    DbBtn: TButton;
    SameAdfCb: TCheckBox;
    SameAdfPanel: TPanel;
    SameAdfBtn: TButton;
    SameAdfLabel: TLabel;
    EqualAdfCb: TCheckBox;
    SameSymCb: TCheckBox;
    SameSymPanel: TPanel;
    SameSymBtn: TButton;
    SameSymLabel: TLabel;
    EqualSymCb: TCheckBox;
    IgnoreDuplicatesCb: TCheckBox;
    FileSelectionPanel: TFileSelectionPanel;
    ShellTreeView: TShellTreeView;
    FileListBox: TFileListBox;
    ListBox: TListBox;
    TrashPanel: TPanel;
    AddAllBtn: TButton;
    DeleteAllBtn: TButton;
    TrashImage: TImage;
    procedure FormCreate(Sender: TObject);
    procedure sameADFBtnClick(Sender: TObject);
    procedure sameSYMBtnClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure GeoBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure DbBtnClick(Sender: TObject);
    procedure SameAdfCbClick(Sender: TObject);
    procedure EqualAdfCbClick(Sender: TObject);
    procedure SameSymCbClick(Sender: TObject);
    procedure EqualSymCbClick(Sender: TObject);
    procedure FileSelectionPanelFilesToConvertChanged(Sender: TObject);
  private
    { Private-Deklarationen }
    CurrentASCFilename:string;
    CurrentADFFilename:string;
    CurrentSYMFilename:string;

    SameAdfFilename:string;
    SameSymFilename:string;

    FirstActivate:boolean;
    CurrentFileIdx:integer;

    procedure CheckWindow;
  public
    { Public-Deklarationen }
    winheight:integer;
    winwidth :integer;
    wintop   :integer;
    winleft  :integer;
    OkBtnMode:boolean;


    function  GetFilename(var ASCDateiname:string; var ADFDateiname:string; var SYMDateiname:string):boolean;
    procedure CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
    procedure SetupForBatch;
    procedure SetImpSettings(ImpMode:integer; ImpAdfFile:string; ImpSymFile:string);
    procedure AddImpFile(aFilename:string);
  end;

var
  SelASCFilesWnd: TSelASCFilesWnd;
  StrFileObj    :ClassStrfile;

implementation

uses Maindlg, inifiles;

{$R *.DFM}

procedure TSelASCFilesWnd.FormCreate(Sender: TObject);
var myScale:double;
    oldHeight:integer;
    oldWidth :integer;
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   Self.Caption:=StrFileObj.Assign(1);                // WinGIS PXF-ASC Import 2000
   TabsheetFiles.Caption:=StrFileObj.Assign(2);       // Files
   TabsheetDB.Caption:=StrFileObj.Assign(3);          // Datebase
   SameADFCb.Caption:=StrFileObj.Assign(4);           // same .adf file
   EqualAdfCb.Caption:=StrFileObj.Assign(5);          // .adf file has same name than .asc file
   SameSymCb.Caption:=StrFileObj.Assign(54);          // same .sym file
   EqualSymCb.Caption:=StrFileObj.Assign(55);         // .sym file has same name than .asc file
   OKBtn.Caption:=StrFileObj.Assign(6);               // Ok
   CancelBtn.Caption:=StrFileObj.Assign(7);           // Cancel
   GeoBtn.Caption:=StrFileObj.Assign(21);             // Projection
   DbBtn.Caption:=StrFileObj.Assign(3);               // Database
   IgnoreDuplicatesCb.Caption:=StrFileObj.Assign(56); // ignore already existing objects


   SameAdfLabel.Caption:='';
   SameSymLabel.Caption:='';

   EqualAdfCb.Checked:=TRUE;
   SameAdfCb.Checked:=FALSE;
   SameAdfPanel.Visible:=FALSE;
   EqualSymCb.Checked:=TRUE;
   SameSymCb.Checked:=FALSE;
   SameSymPanel.Visible:=FALSE;

   PageControl.ActivePage:=TabsheetFiles;

   CurrentFileIdx:=0;
   SameAdfFilename:='';
   SameSymFilename:='';

   FirstActivate:=true;
   oldHeight:=Self.Height;
   OldWidth:=Self.Width;

   Self.Height:=1;
   Self.Width:=1;

   myScale:=1;//100000 / Trunc(9600000/(Self.PixelsPerInch));
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);

   // now adapt winheigt and winwidth
   winheight:=round(OldHeight*myScale);
   winwidth:=round(OldWidth*myScale);
end;

procedure TSelASCFilesWnd.sameADFBtnClick(Sender: TObject);
begin
   if OpenDialogSameADF.Execute then
   begin
      SameAdfLabel.Caption:=ExtractFilename(OpenDialogSameADF.Filename);
      SameAdfFilename:=OpenDialogSameADF.Filename;
   end;
   CheckWindow;
end;

procedure TSelASCFilesWnd.sameSYMBtnClick(Sender: TObject);
begin
   if OpenDialogSameSYM.Execute then
   begin
      SameSymLabel.Caption:=Extractfilename(OpenDialogSameSYM.Filename);
      SameSymFilename:=OpenDialogSameSYM.Filename;
   end;
   CheckWindow;
end;

procedure TSelASCFilesWnd.OKBtnClick(Sender: TObject);
var inifile:TIniFile;
begin
   MainWnd.ExitNormal:=true;
   // write current settings to the ConGIS.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir + 'ConGIS.ini');
   inifile.WriteString('ASCIMP','ImpDbMode',inttostr(MainWnd.AXImpExpDbc.ImpDbMode));
   inifile.WriteString('ASCIMP','ImpDatabase',MainWnd.AXImpExpDbc.ImpDatabase);
   inifile.WriteString('ASCIMP','ImpTable',MainWnd.AXImpExpDbc.ImpTable);
   if SameAdfCb.Checked then
   begin
      inifile.WriteString('ASCIMP','USESAMEADF','T');
      inifile.WriteString('ASCIMP','SAMEADFNAME',SameAdfFilename);
   end
   else
      inifile.WriteString('ASCIMP','USESAMEADF','F');

   if SameSymCb.Checked then
   begin
      inifile.WriteString('ASCIMP','USESAMESYM','T');
      inifile.WriteString('ASCIMP','SAMESYMNAME',SameSymFilename);
   end
   else
      inifile.WriteString('ASCIMP','USESAMESYM','F');

   inifile.WriteString('ASCIMP','SourceCoordinateSystem',inttostr(MainWnd.AxGisProjection.SourceCoordinateSystem));
   inifile.WriteString('ASCIMP','SourceProjSettings',MainWnd.AxGisProjection.SourceProjSettings);
   inifile.WriteString('ASCIMP','SourceProjection',MainWnd.AxGisProjection.SourceProjection);
   inifile.WriteString('ASCIMP','SourceDate',MainWnd.AxGisProjection.SourceDate);
   inifile.WriteString('ASCIMP','SourceXOffset',floattostr(MainWnd.AxGisProjection.SourceXOffset));
   inifile.WriteString('ASCIMP','SourceYOffset',floattostr(MainWnd.AxGisProjection.SourceYOffset));
   inifile.WriteString('ASCIMP','SourceScale',floattostr(MainWnd.AxGisProjection.SourceScale));

   inifile.WriteString('ASCIMP','LastUsedPath',FileSelectionPanel.Directory);
   if IgnoreDuplicatesCb.Checked then
      inifile.WriteString('ASCIMP','IgnoreExistingObjects','T')
   else
      inifile.WriteString('ASCIMP','IgnoreExistingObjects','F');
   inifile.Destroy;
   // now the import window can be opened and the import executed
   Self.Close;
   MainWnd.Show;
end;

procedure TSelASCFilesWnd.CancelBtnClick(Sender: TObject);
begin
   MainWnd.ExitNormal:=false;
   MainWnd.CloseTimer.Enabled:=true;
end;



function TSelASCFilesWnd.GetFilename(var ASCDateiname:string; var ADFDateiname:string; var SYMDateiname:string):boolean;
var retVal:boolean;
begin
   retVal:=TRUE;
   if CurrentFileIdx < FileSelectionPanel.NumFilesToConvert then
   begin
      ASCDateiname:=FileSelectionPanel.GetFilename(CurrentFileIdx);
      // get the name of the .adf file
      if SameAdfCb.Checked then
         ADFDateiname:=SameAdfFilename
      else
      begin
         ADFDateiname:=ASCDateiname; ADFDateiname:=Changefileext(ADFDateiname,'.adf');
      end;
      // get the name of the .sym file
      if SameSymCb.Checked then
         SymDateiname:=SameSymFilename
      else
      begin
         SymDateiname:=ASCDateiname; SymDateiname:=Changefileext(SymDateiname,'.sym');
      end;

      if not (Fileexists(ASCDateiname) and Fileexists(ADFDateiname) and Fileexists(SYMDateiname)) then
         retVal:=FALSE;
   end
   else
      retVal:=FALSE;
   result:=retVal;
   CurrentFileIdx:=CurrentFileIdx+1;
end;

procedure TSelASCFilesWnd.CheckWindow;
var allok:boolean;
begin
   allok:=true;
   if FileSelectionPanel.NumFilesToConvert = 0 then allok:=false;

   // check the .adf file
   if (SameAdfCb.Checked) and (SameAdfFilename = '') then
      allok:=FALSE;
   if (not SameAdfCb.Checked) and (not EqualAdfCb.Checked) then
      allok:=FALSE;

   // check the .sym file
   if (SameSymCb.Checked) and (SameSymFilename = '') then
      allok:=FALSE;
   if (not SameSymCb.Checked) and (not EqualSymCb.Checked) then
      allok:=FALSE;

   if allok then
      OkBtn.Enabled:=true
   else
      OkBtn.Enabled:=false;
end;

procedure TSelASCFilesWnd.GeoBtnClick(Sender: TObject);
begin
   MainWnd.AxGisProjection.SetupByDialog;
end;

procedure TSelASCFilesWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TSelASCFilesWnd.SetupWindowTimerTimer(Sender: TObject);
var CurDir:string;
    dummy:string;
    intval, count:integer;
    doubleval:double;
    inifile:TIniFile;
begin
   SetupWindowTimer.Enabled:=false;
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,winleft,wintop,winwidth, winheight,SWP_SHOWWINDOW);
   Self.Height:=winheight;
   Self.Width:=winwidth;
   // Setup the window with the ConGIS.ini settings
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'ConGIS.ini');
   dummy:=MainWnd.DestDocument.Name;
   dummy:=ChangeFileExt(dummy,'.wgi');
   if FileExists(dummy) then
   begin
      MainWnd.AXImpExpDbc.ImpDbMode:=5; // Idb-Database
      MainWnd.AXImpExpDbc.ImpDatabase:='';
      MainWnd.AXImpExpDbc.ImpTable:='';
   end
   else
   begin
      dummy:=inifile.ReadString('ASCIMP','ImpDbMode','0');
      val(dummy, intval, count);
      if intval <> 5 then // Idb-Database
      begin
         MainWnd.AXImpExpDbc.ImpDbMode:=intval;
         dummy:=inifile.ReadString('ASCIMP','ImpDatabase','');
         MainWnd.AXImpExpDbc.ImpDatabase:=dummy;
         dummy:=inifile.ReadString('ASCIMP','ImpTable','');
         MainWnd.AXImpExpDbc.ImpTable:=dummy;
      end;
   end;
   DestDbLabel.Caption:=MainWnd.AXImpExpDbc.ImpDbInfo;

   // setup the ADF-Settings
   dummy:=inifile.ReadString('ASCIMP','USESAMEADF','F');
   if dummy = 'T' then
   begin
      SameAdfCb.Checked:=TRUE;
      dummy:=inifile.ReadString('ASCIMP','SAMEADFNAME','');
      SameAdfLabel.Caption:=ExtractFilename(dummy);
      SameAdfFilename:=dummy;
   end
   else
      SameAdfCb.Checked:=FALSE;
   SameAdfCbClick(nil);

   // setup the SYM-Settings
   dummy:=inifile.ReadString('ASCIMP','USESAMESYM','F');
   if dummy = 'T' then
   begin
      SameSymCb.Checked:=TRUE;
      dummy:=inifile.ReadString('ASCIMP','SAMESYMNAME','');
      SameSymLabel.Caption:=ExtractFilename(dummy);
      SameSymFilename:=dummy;
   end
   else
      SameSymCb.Checked:=FALSE;
   SameSymCbClick(nil);

   // setup the projection
   dummy:=inifile.ReadString('ASCIMP','SourceCoordinateSystem','');
   if dummy <> '' then
   begin
      val(dummy, intval, count);
      MainWnd.AxGisProjection.SourceCoordinateSystem:=intval;
      dummy:=inifile.ReadString('ASCIMP','SourceProjSettings','NONE');
      MainWnd.AxGisProjection.SourceProjSettings:=dummy;
      dummy:=inifile.ReadString('ASCIMP','SourceProjection','NONE');
      MainWnd.AxGisProjection.SourceProjection:=dummy;
      dummy:=inifile.ReadString('ASCIMP','SourceDate','NONE');
      MainWnd.AxGisProjection.SourceDate:=dummy;
      dummy:=inifile.ReadString('ASCIMP','SourceXOffset','0');
      val(dummy, doubleval, count);
      MainWnd.AxGisProjection.SourceXOffset:=doubleval;
      dummy:=inifile.ReadString('ASCIMP','SourceYOffset','0');
      val(dummy, doubleval, count);
      MainWnd.AxGisProjection.SourceYOffset:=doubleval;
      dummy:=inifile.ReadString('ASCIMP','SourceScale','1');
      val(dummy, doubleval, count);
      MainWnd.AxGisProjection.SourceScale:=doubleval;
   end;

   dummy:=inifile.ReadString('ASCIMP','LastUsedPath','');
   if dummy <> '' then
      CurDir:=dummy
   else
      GetDir(0, CurDir);
   FileSelectionPanel.Directory:=CurDir;

   dummy:=inifile.ReadString('ASCIMP','IgnoreExistingObjects','F');
   if dummy = 'T' then IgnoreDuplicatesCb.Checked:=TRUE else IgnoreDuplicatesCb.Checked:=FALSE;
   inifile.Destroy;
   Self.Repaint;
end;

procedure TSelASCFilesWnd.CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
begin
   wintop:=(WHEIGHT div 2) - (winheight div 2);
   winleft:=(WWIDTH div 2) - (winwidth div 2);
end;

// procedure is used to setup the SelAscFilesWnd for batch mode
procedure TSelASCFilesWnd.SetupForBatch;
begin
   if FirstActivate then
   begin
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
   end;
end;

procedure TSelASCFilesWnd.SetImpSettings(ImpMode:integer; ImpAdfFile:string; ImpSymFile:string);
begin
   if ImpMode = 0 then // use same .adf and .sym for all .asc files
   begin
      SameAdfCb.Checked:=TRUE;
      SameAdfFilename:=ImpAdfFile;
      SameSymCb.Checked:=TRUE;
      SameSymFilename:=ImpSymFile;
      EqualAdfCb.Checked:=FALSE;
      EqualSymCb.Checked:=FALSE;
   end;
   if ImpMode = 1 then // .adf and .sym have same name than .asc file
   begin
      SameAdfCb.Checked:=FALSE;
      SameSymCb.Checked:=FALSE;
      EqualAdfCb.Checked:=TRUE;
      EqualSymCb.Checked:=TRUE;
   end;
end;

procedure TSelASCFilesWnd.AddImpFile(aFilename:string);
begin
   FileSelectionPanel.AddFileToConvert(aFilename);
end;

procedure TSelASCFilesWnd.DbBtnClick(Sender: TObject);
begin
   MainWnd.AXImpExpDbc.SetupByDialog(Self.Handle,5,5);
   PageControl.Enabled:=FALSE;
   GeoBtn.Enabled:=FALSE;
   DbBtn.Enabled:=FALSE;
   OkBtnMode:=OkBtn.Enabled;
   OkBtn.Enabled:=FALSE;
   CancelBtn.Enabled:=FALSE;
end;

procedure TSelASCFilesWnd.SameAdfCbClick(Sender: TObject);
begin
   if SameAdfCb.Checked then
   begin
      SameAdfPanel.visible:=TRUE;
      EqualAdfCb.Checked:=FALSE;
   end
   else
   begin
      SameAdfPanel.visible:=FALSE;
   end;
   CheckWindow;
end;

procedure TSelASCFilesWnd.EqualAdfCbClick(Sender: TObject);
begin
   if EqualAdfCb.Checked then
   begin
      SameAdfCb.Checked:=FALSE;
      SameAdfPanel.Visible:=FALSE;
   end;
   CheckWindow;
end;

procedure TSelASCFilesWnd.SameSymCbClick(Sender: TObject);
begin
   if SameSymCb.Checked then
   begin
      SameSymPanel.visible:=TRUE;
      EqualSymCb.Checked:=FALSE;
   end
   else
   begin
      SameSymPanel.visible:=FALSE;
   end;
   CheckWindow;
end;

procedure TSelASCFilesWnd.EqualSymCbClick(Sender: TObject);
begin
   if EqualSymCb.Checked then
   begin
      SameSymCb.Checked:=FALSE;
      SameSymPanel.Visible:=FALSE;
   end;
   CheckWindow;
end;

procedure TSelASCFilesWnd.FileSelectionPanelFilesToConvertChanged( Sender: TObject);
begin
   CheckWindow;
end;

end.
