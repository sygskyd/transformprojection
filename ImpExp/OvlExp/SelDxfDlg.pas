unit SelDxfDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StrFile, StdCtrls, Buttons, FileCtrl, ComCtrls, ExtCtrls, Spin, inifiles,
  WinOSInfo;

type
  TSelDxfWnd = class(TForm)
    PageControl: TPageControl;
    TabSheetSource: TTabSheet;
    LayerLB: TListBox;
    TabSheetDest: TTabSheet;
    LayerCB: TCheckBox;
    OKBtn: TButton;
    CancelBtn: TButton;
    GeoBtn: TButton;
    Panel2: TPanel;
    Label1: TLabel;
    Panel1: TPanel;
    DxfLabel: TLabel;
    DxfBtn: TButton;
    SaveDialogdxf: TSaveDialog;
    SelectedCB: TCheckBox;
    SetupWindowTimer: TTimer;
    PointsCB: TCheckBox;
    PolylinesCB: TCheckBox;
    Label2: TLabel;
    RoutesMaxLengthSE: TSpinEdit;
    Label3: TLabel;
    SplitRB: TRadioButton;
    IgnoreRB: TRadioButton;
    Bevel1: TBevel;
    Label4: TLabel;
    Label5: TLabel;
    WaypointsMaxCountSE: TSpinEdit;
    RoutesMaxCountSE: TSpinEdit;
    Label6: TLabel;
    GPSDevCombo: TComboBox;
    Label7: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure LayerCBClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GeoBtnClick(Sender: TObject);
    procedure DxfBtnClick(Sender: TObject);
    procedure SelectedCBClick(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure LayerLBClick(Sender: TObject);
    procedure PointsCBClick(Sender: TObject);
    procedure PolylinesCBClick(Sender: TObject);
    procedure IgnoreRBClick(Sender: TObject);
    procedure SplitRBClick(Sender: TObject);
    procedure GPSDevComboSelect(Sender: TObject);
  private
    { Private declarations }
    FirstActivate:boolean;
    DestinationDxfFile:string;
    procedure CheckWindow;
  public
    { Public declarations }
    winheight:integer;
    winwidth :integer;
    wintop   :integer;
    winleft  :integer;
    procedure GetDxfFile(var Dxffile:string);
    procedure CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
    procedure SetupForBatch;
    procedure SetExpSettings(DxfFile:string; ExpMode:integer);
    procedure AddExpLayer(ExpLayer:string);
    procedure SaveOVLSettings;
    procedure FillGPSDevCombo;
  end;

var
  SelDxfWnd     : TSelDxfWnd;
  StrFileObj    : ClassStrfile;
  Ini   :TInifile;
implementation

uses Maindlg;

{$R *.DFM}

procedure TSelDxfWnd.FormCreate(Sender: TObject);
var myScale:double;
    dummy  :double;
    dummystr:string;
begin
   Ini := TInifile.Create(OSInfo.LocalAppDataDir+'Congis.ini');

   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   Self.Caption:=StrfileObj.Assign(1);                 { PROGIS OVL Export 2003 }
   TabsheetSource.Caption:=StrfileObj.Assign(2);       { Source Project              }
   TabsheetDest.Caption:=StrfileObj.Assign(3);         { Destination ACAD-DXF        }
   LayerCB.Caption:=StrfileObj.Assign(4);              { Export only selected layers }
   SelectedCB.Caption:=StrfileObj.Assign(38);          { Export only selected objects }

   OkBtn.Caption:=StrfileObj.Assign(5);                { Ok }
   CancelBtn.Caption:=StrfileObj.Assign(6);            { Cancel }
   GeoBtn.Caption:=StrfileObj.Assign(7);               { Projection }
   Label1.Caption:=StrfileObj.Assign(8);               { Dxf-File: }

   pointsCB.Caption := StrfileObj.Assign(43);          {Export points (as Waypoints)}
   polylinesCB.Caption := StrfileObj.Assign(44); 	       {Export polylines (as Routes)}
   label2.Caption := StrfileObj.Assign(45); 		// Routes longer than
   label3.Caption := StrfileObj.Assign(46); 		// points
   splitRB.Caption := StrfileObj.Assign(47);            // split
   ignoreRB.Caption := StrfileObj.Assign(48);           // ignore
   label4.Caption := StrfileObj.Assign(58); 		// waypoints
   label5.Caption := StrfileObj.Assign(59); 		// waypoints
   label6.Caption := StrfileObj.Assign(60); 		// routes
   label7.Caption := StrfileObj.Assign(65); 		// presettings
   LayerLB.enabled:=false;

   DestinationDxfFile:='';
   dxfLabel.Caption:='';
   FirstActivate:=true;

   Self.Height:=1;
   Self.Width:=1;

   myScale:=1; //100000 / Trunc(9600000/(Self.PixelsPerInch));
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
   
   FillGPSDevCombo;

   // now adapt winheigt and winwidth
   dummy:=376*myScale; dummystr:=floattostr(dummy);
   if (Pos('.',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos('.',dummystr)-1);
   if (Pos(',',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos(',',dummystr)-1);
   winheight:=strtoint(dummystr);
   dummy:=462*myScale; dummystr:=floattostr(dummy);
   if (Pos('.',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos('.',dummystr)-1);
   if (Pos(',',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos(',',dummystr)-1);
   winwidth:=strtoint(dummystr);
end;

procedure TSelDxfWnd.SaveOVLSettings;
begin
   Ini.WriteInteger('OVLEXPORT','RoutesMaxLen',SelDxfWnd.RoutesMaxLengthSE.Value);
   Ini.WriteBool('OVLEXPORT','ExportWaypoints',PointsCB.Checked);
   Ini.WriteBool('OVLEXPORT','ExportRoutes',PolylinesCB.Checked);
   Ini.WriteBool('OVLEXPORT','SplitRoutes',SplitRB.Checked);
   Ini.WriteInteger('OVLEXPORT','WaypointsMaxCount',SelDxfWnd.WaypointsMaxCountSE.Value);
   Ini.WriteInteger('OVLEXPORT','RoutesMaxCount',SelDxfWnd.RoutesMaxCountSE.Value);
   Ini.WriteString('OVLEXPORT','OVLExportfile',SelDxfDlg.SelDxfWnd.DxfLabel.Caption);
   Ini.WriteInteger('OVLEXPORT','GPSDevice',OVLSelectedGPSDevice );
end;

procedure TSelDxfWnd.FillGPSDevCombo;
var i : integer;
begin
  i := 0;
  while ini.SectionExists('GPSDevice' + IntToStr(i)) do
  	begin
        GPSDevCombo.Items.Add(ini.ReadString('GPSDevice' + IntToStr(i),'Name','no name given'));
        inc(i);
        end;
end;



procedure TSelDxfWnd.OKBtnClick(Sender: TObject);
begin
   SaveOVLSettings;
   if not MainWnd.CheckProjectionSettings then
   	begin
        SelDXFWnd.GeoBtn.Click;
        exit;
        end;
   MainWnd.ExitNormal:=true;
   Self.Close;
   MainWnd.Show;
end;

procedure TSelDxfWnd.CancelBtnClick(Sender: TObject);
begin
   SaveOVLSettings;
   MainWnd.ExitNormal:=false;
   MainWnd.CloseTimer.Enabled:=true;
end;

procedure TSelDxfWnd.GetDxfFile(var Dxffile:string);
begin
   DxfFile:=DestinationDxfFile;
end;

procedure TSelDxfWnd.LayerCBClick(Sender: TObject);
begin
   if LayerCB.Checked then
   begin
      LayerLB.enabled:=true;
      SelectedCB.Checked:=false;
   end
   else
   begin
      LayerLB.enabled:=false;
   end;
   CheckWindow;
end;

procedure TSelDxfWnd.CheckWindow;
var allok:boolean;
    i:integer;
begin

   { Procedure checks all settings in the window }
   if LayerCb.Checked then
   begin
      allok := FALSE;
      // check if there is a layer selected
      for i:=0 to LayerLb.Items.count-1 do
      begin
         if LayerLb.Selected[i] then
         begin
            allok := (pointsCB.Checked or polylinesCB.Checked);
            break;
         end;
      end;
   end
   else
       if not selectedCB.Checked then allok := (pointsCB.Checked or polylinesCB.Checked);


  if DestinationDxfFile = '' then allok:=false;
  OKBtn.Enabled:=allok;
end;

procedure TSelDxfWnd.FormActivate(Sender: TObject);
var Layername:string;
    layercnt, itemp:integer;
    ALayer:Variant;
begin
   if FirstActivate then
   begin
      FirstActivate := FALSE;	
      // fill layerlist
      LayerLB.Items.Clear;
      for layercnt:=0 to MainWnd.SourceDocument.Layers.Count-1 do
      begin
         ALayer:=MainWnd.SourceDocument.Layers.Items[layercnt];
         Layername:=ALayer.Layername;
         LayerLb.Items.Add(Layername);
      end;
      MainWnd.AxGisProjection.WorkingPath:=OSInfo.WingisDir;
      MainWnd.AxGisProjection.LngCode:=MainWnd.LngCode;
      MainWnd.SetProjectionSettings;
      SetupWindowTimer.Enabled:=true;
      // read ini file
      iTemp := Ini.ReadInteger('OVLEXPORT','RoutesMaxLen',20);
      if iTemp < 1000 then SelDXFWnd.RoutesMaxLengthSE.Value := itemp else SelDXFWnd.RoutesMaxLengthSE.Value := 999;
      PointsCB.Checked := Ini.ReadBool('OVLEXPORT','ExportWaypoints',TRUE);
      PolylinesCB.Checked := Ini.ReadBool('OVLEXPORT','ExportRoutes',TRUE);
      SplitRB.Checked := Ini.ReadBool('OVLEXPORT','SplitRoutes',TRUE);
      WayPointsMaxCountSE.Value := Ini.ReadInteger('OVLEXPORT','WaypointsMaxCount',1000);
      RoutesMaxCountSE.Value := Ini.ReadInteger('OVLEXPORT','RoutesMaxCount',50);
      SelDxfDlg.SelDxfWnd.DxfLabel.Caption := Ini.ReadString('OVLEXPORT','OVLExportfile','');
      DestinationDxfFile := SelDxfDlg.SelDxfWnd.DxfLabel.Caption;
      OVLSelectedGPSDevice := Ini.ReadInteger('OVLEXPORT','GPSDevice',0);
      SelDxfWnd.GPSDevCombo.Text :=  SelDxfWnd.GPSDevCombo.Items[OVLSelectedGPSDevice];

      IgnoreRB.Checked := not SplitRB.Checked;
      CheckWindow;
   end;
end;

procedure TSelDxfWnd.GeoBtnClick(Sender: TObject);
begin
   MainWnd.SetProjectionSettings;
   MainWnd.AxGisProjection.ShowOnlySource:=TRUE;
   MainWnd.AxGisProjection.SetupByDialog;
   MainWnd.SourceDocument.Projection := MainWnd.AxGisProjection.SourceCoordinateSystem;
   MainWnd.SourceDocument.ProjectProjSettings := MainWnd.AxGisProjection.SourceProjSettings;
   MainWnd.SourceDocument.ProjectProjection := MainWnd.AxGisProjection.SourceProjection;
   MainWnd.SourceDocument.Projectdate := MainWnd.AxGisProjection.SourceDate;
   MainWnd.SourceDocument.ProjectionXOffset := MainWnd.AxGisProjection.SourceXOffset;
   MainWnd.SourceDocument.ProjectionYOffset := MainWnd.AxGisProjection.SourceYOffset;
   MainWnd.SourceDocument.ProjectionScale := MainWnd.AxGisProjection.SourceScale;

   MainWnd.SetProjectionSettings;

end;


procedure TSelDxfWnd.DxfBtnClick(Sender: TObject);
begin
   if SaveDialogdxf.Execute then
   begin
      DestinationDxfFile:=SaveDialogdxf.Filename;
      DxfLabel.Caption:=DestinationDxfFile;
   end;
   CheckWindow;
end;

procedure TSelDxfWnd.SelectedCBClick(Sender: TObject);
begin
   if SelectedCB.Checked then
     begin
      LayerCB.Checked:=false;
      PointsCB.Enabled := false;
      PolylinesCB.Enabled := false;
     end
    else
    begin
    PointsCB.Enabled := true;
    PolylinesCB.Enabled := true;
    end;
   CheckWindow;
end;

procedure TSelDxfWnd.CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
begin
   wintop:=(WHEIGHT div 2) - (winheight div 2);
   winleft:=(WWIDTH div 2) - (winwidth div 2);
end;

procedure TSelDxfWnd.SetupWindowTimerTimer(Sender: TObject);
begin
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,winleft,wintop,winwidth, winheight,SWP_SHOWWINDOW);
   Self.Height:=winheight;
   Self.Width:=winwidth;
   if MainWnd.SourceDocument.NrOfSelectedObjects = 0 then
      SelectedCb.Visible:=FALSE;
   Self.Repaint;
   SetupWindowTimer.Enabled:=false;
end;

// procedure is used to setup the SelAscWnd for batch mode
procedure TSelDxfWnd.SetupForBatch;
var Layername:string;
    layercnt:integer;
    ALayer:Variant;
begin
   if FirstActivate then
   begin
      // fill layerlist
      LayerLB.Items.Clear;
      for layercnt:=0 to MainWnd.SourceDocument.Layers.Count-1 do
      begin
         ALayer:=MainWnd.SourceDocument.Layers.Items[layercnt];
         Layername:=ALayer.Layername;
         LayerLb.Items.Add(Layername);
      end;
      MainWnd.AxGisProjection.WorkingPath:=OSInfo.WingisDir;
      MainWnd.AxGisProjection.LngCode:=MainWnd.LngCode;
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
   end;
end;

procedure TSelDxfWnd.SetExpSettings(DxfFile:string; ExpMode:integer);
begin
   DestinationDxfFile:=DxfFile;
   if ExpMode = 2 then // export only selected objects
      SelectedCB.Checked:=true;
   if ExpMode = 1 then // export only selected layers
      LayerCB.Checked:=true;
end;

procedure TSelDxfWnd.AddExpLayer(ExpLayer:string);
var i:integer;
begin
   // find the Layername in the list and select the item
   for i:=0 to LayerLB.Items.Count-1 do
   begin
      if (LayerLB.Items[i] = ExpLayer) then
      begin
         LayerLB.Selected[i]:=true;
      end;
   end;
end;

procedure TSelDxfWnd.LayerLBClick(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSelDxfWnd.PointsCBClick(Sender: TObject);
begin
CheckWindow;
end;

procedure TSelDxfWnd.PolylinesCBClick(Sender: TObject);
begin
CheckWindow;
end;

procedure TSelDxfWnd.IgnoreRBClick(Sender: TObject);
begin
SplitRB.Checked := not IgnoreRB.Checked;
end;

procedure TSelDxfWnd.SplitRBClick(Sender: TObject);
begin
IgnoreRB.Checked := not SplitRB.Checked;
end;

procedure TSelDxfWnd.GPSDevComboSelect(Sender: TObject);
begin
OVLSelectedGPSDevice := GPSDevCombo.ItemIndex;
SelDxfWnd.RoutesMaxLengthSE.Value := ini.ReadInteger('GPSDevice' + IntToStr(OVLSelectedGPSDevice),'RoutesMaxLen',20);
SelDxfWnd.RoutesMaxCountSE.Value := ini.ReadInteger('GPSDevice' + IntToStr(OVLSelectedGPSDevice),'RoutesMaxCount',20);
SelDxfWnd.WaypointsMaxCountSE.Value := ini.ReadInteger('GPSDevice' + IntToStr(OVLSelectedGPSDevice),'WaypointsMaxCount',20);
end;

end.
