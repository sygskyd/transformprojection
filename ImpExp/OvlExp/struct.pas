unit struct;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, db, Math, Seldxfdlg;

type
  { Hier k�nnen Strukturen definiert werden }

   p_Punkt_record=^Punkt_record;
   Punkt_record = record
       X:double;
       Y:double;
       next :p_Punkt_record;
   end;

   p_Layer_record = ^Layer_record;
   Layer_record = record
      Layername     :string;
      Linestyle     :string;
      Linecolor     :string;
      Areastyle     :string;
      Areacolor     :string;
      Objectsonlayer:integer;
      next          :p_Layer_record;
   end;

   p_Point_record = ^Point_record;
   Point_record = record
      X        :double;
      Y        :double;
      Layer    :p_Layer_record;
      next     :p_Point_record;
   end;

   p_Line_record = ^Line_record;
   Line_record = record
      Punktanz :integer;
      Punkte   :p_Punkt_record;
      Layer    :p_Layer_record;
      next     :p_Line_record;
   end;

   p_area_record =^area_record;
   area_record = record
      Punktanz :integer;
      Punkte   :p_Punkt_record;
      Layer    :p_Layer_record;
      next     :p_area_record;
   end;

   p_text_record = ^text_record;
   text_record = record
      X1        :double;
      Y1        :double;
      X2        :double;
      Y2        :double;
      Text      :string;
      Font      :string;
      size      :double;
      angle     :double;
      Layer     :p_Layer_record;
      next      :p_text_record;
   end;

   p_circle_record = ^circle_record;
   circle_record = record
      X         :double;
      Y         :double;
      Radius    :double;
      Layer     :p_Layer_record;
      next      :p_circle_record;
   end;

   p_SymbolRef_record = ^Symbolref_record;
   Symbolref_record = record
      X         :double; { Einsetzpunkt X }
      Y         :double; { Einsetzpunkt Y }
      SymName   :string; { Symbolname     }
      Min_Y     :double; // minimum Y value of Symbol definition
      Max_Y     :double; // maximim Y value of Symbol definition
      Points    :p_Point_record;
      Areas     :p_Area_record;
      Lines     :p_Line_record;
      Circles   :p_Circle_record;
      Texts     :p_Text_record;
      next      :p_Symbolref_record;
   end;

   p_Symbol_record = ^Symbol_record;
   Symbol_record = record
      X          :double;
      Y          :double;
      SymName    :string;
      Size       :double;
      Angle      :double;
      Layer     :p_Layer_record;
      next      :p_Symbol_record;
   end;


ClassSymbol = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X:double;Y:double;SymName:string;Size:double;Angle:double;Layer:p_Layer_record);
      procedure WriteData(Cur_Symbol:p_Symbol_record);
      function GetAnzahl:longint;
      procedure ResetCurrent;
      function GetCurrent:p_Symbol_record;
      procedure SetCurrentNext;
   private
      { private Definitionen }
      Anfang,Ende,Current:p_Symbol_record;
      Anzahl:integer;
end;


ClassArea = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(Layer:p_Layer_record);
      procedure InsertPoint(X:double;Y:double);
      procedure WriteData(Cur_Area:p_Area_record);
      function GetAnzahl:longint;
      procedure ResetCurrent;
      function GetCurrent:p_Area_record;
      procedure SetCurrentNext;
   private
      { private Definitionen }
      Anfang,Ende,Current:p_Area_record;
      Anzahl:integer;
end;

ClassSymbolRef = class
   public
      { public Deklarationen }
      constructor Create;
      destructor Destroy;
      procedure Neu (X:double;Y:double;Symname:string);
      procedure New_Point(X:double;Y:double);
      procedure New_Circle(X:double;Y:double;Radius:integer);
      procedure New_Area;
      procedure InsertPointToArea(X:double;Y:double);
      procedure New_Line;
      procedure InsertPointToLine(X:double;Y:double);
      procedure New_Text(X1:double;Y1:double;X2:double;Y2:double;Text:string;Font:string;size:double;Angle:double);
      function GetAnzahl:integer;
      procedure Resetcurrent;
      procedure SetCurrentnext;
      procedure WriteSymbolDefHeader;
      procedure WriteSymbolDefBottom;
      function GetPoints:p_Point_record;
      function GetLines:p_Line_record;
      function GetAreas:p_Area_record;
      function GetCircles:p_Circle_record;
      function GetTexts:p_Text_record;
      function CheckSymbolIsEmpty:boolean;
      function CheckSymbolRefExists(SymName:string):boolean;
   private
      { private Definitionen }
      Anfang,Ende,Current:p_SymbolRef_record;
      Current_Area:p_Area_record;
      Current_Line:p_Line_record;
      Anzahl:integer;
end;


ClassLine = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(Layer:p_Layer_record);
      procedure InsertPoint(X:double;Y:double);
      procedure WriteData(Cur_Line:p_Line_record);
      function GetAnzahl:longint;
      function GetCurrent:p_Line_record;
      procedure ResetCurrent;
      procedure SetCurrentNext;
   private
      { private Definitionen }
      Anfang,Ende,Current:p_Line_record;
      Anzahl:integer;
end;

ClassPoint = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X:double;Y:double;Layer:p_Layer_record);
      procedure WriteData(Curr_Point:p_Point_record);
      function GetAnzahl:longint;
      procedure ResetCurrent;
      function GetCurrent:p_Point_record;
      procedure SetCurrentNext;
    private
      { private Definitionen }
      Anfang,Ende,Current:p_point_record;
      Anzahl:integer;
end;

ClassText = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X1:double;Y1:double;X2:double;Y2:double;Text:string;Font:string;size:double;angle:double;Layer:p_Layer_record);
      procedure WriteData(Curr_Text:p_Text_record);
      function GetAnzahl:longint;
      procedure ResetCurrent;
      procedure SetCurrentNext;
      function GetCurrent:p_Text_Record;
    private
      { private Definitionen }
      Anfang,Ende,Current:p_text_record;
      Anzahl:integer;
      function  GetAngle(X1:double;Y1:double;X2:double;Y2:double):double;
      function  GetLength(Text:string;Size:double):double;
      function  GetHeight(Size:double):double;
end;

ClassCircle = class
    public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(X:double;Y:double;Radius:integer;Layer:p_Layer_record);
      procedure WriteData(Curr_Circle:p_Circle_record);
      function GetAnzahl:longint;
      function GetCurrent:p_Circle_record;
      procedure ResetCurrent;
      procedure SetCurrentNext;
    private
      { private Definitionen }
      Anfang,Ende,Current:p_circle_record;
      Anzahl:integer;
end;

ClassLayer = class
   public
      { public Definitionen }
      constructor Create;
      destructor Destroy;
      procedure Neu(Layername:string;Line_Type:string;Line_Color:string;Fill_Type:string;Fill_color:string);
      function Checkexists(Layername:string):boolean;
      function GetCurrent:p_Layer_record;
      function Hextoint(Wert:string):longint;
      procedure ResetCurrent;
      function GetAnzahl:longint;
      function GetCurrentLayername:string;
      procedure SetCurrentnext;
      procedure WriteLayerTable;
   private
      { private Definitionen }
      Anfang,Ende,Current:p_Layer_record;
      Anzahl:integer;
      ColumnAnzahl:integer;
end;

implementation
uses files,maindlg;

var FileObj                         :ClassFile;

constructor ClassArea.Create;
begin
   Anfang:=new(p_Area_record); Ende:=new(p_Area_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Area_record); Current:=nil; Anzahl:=0;
end;

constructor ClassSymbol.Create;
begin
   Anfang:=new(p_Symbol_record); Ende:=new(p_Symbol_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Symbol_record); Current:=nil; Anzahl:=0;
end;

constructor ClassSymbolRef.Create;
begin
   Anfang:=new(p_SymbolRef_record); Ende:=new(p_SymbolRef_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_SymbolRef_record); Current:=nil; Anzahl:=0;
   Current_Area:=nil;
   Current_Line:=nil;
end;

constructor ClassText.Create;
begin
   Anfang:=new(p_Text_record); Ende:=new(p_Text_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Text_record); Current:=nil; Anzahl:=0;
end;

constructor ClassCircle.Create;
begin
   Anfang:=new(p_circle_record); Ende:=new(p_circle_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_circle_record); Current:=nil; Anzahl:=0;
end;

constructor ClassLine.Create;
begin
   Anfang:=new(p_Line_record); Ende:=new(p_Line_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Line_record); Current:=nil; Anzahl:=0;
end;

constructor ClassPoint.Create;
begin
   Anfang:=new(p_Point_record); Ende:=new(p_Point_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Point_record); Current:=nil; Anzahl:=0;
end;

constructor ClassLayer.Create;
begin
   Anfang:=new(p_Layer_record); Ende:=new(p_Layer_record); Anfang:=nil; Ende:=nil;
   Current:=new(p_Layer_record); Current:=nil; Anzahl:=0; ColumnAnzahl:=0;
end;

destructor ClassPoint.Destroy;
var Zeiger1,Zeiger2:p_Point_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassSymbol.Destroy;
var Zeiger1,Zeiger2:p_Symbol_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassSymbolRef.Destroy;
var Zeiger1,Zeiger2:p_SymbolRef_record;
    Point1,Point2:p_Point_record;
    Area1,Area2:p_Area_record;
    Punkt1,Punkt2:p_Punkt_record;
    Line1,Line2:p_Line_record;
    Circle1,Circle2:p_Circle_record;
    Text1,Text2:p_Text_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Punktinformation }
      Point1:=Zeiger2^.Points; Point2:=Zeiger2^.Points;
      if Point1 <> nil then
      repeat
         Point2:=Point1;
         Point1:=Point1^.next;
         dispose(Point2);
      until Point1 = nil;

      { L�schen der Area-Information }
      Area1:=Zeiger2^.Areas; Area2:=Zeiger2^.Areas;
      if Area1 <> nil then
      repeat
         Area2:=Area1;
         Area1:=Area1^.next;
         { Nun werden die Eckpunkte gel�scht }
         Punkt1:=Area2^.Punkte; Punkt2:=Area2^.Punkte;
         if Point1 <> nil then
         repeat
            Punkt2:=Punkt1;
            Punkt1:=Punkt1^.next;
            dispose(Punkt2);
         until Punkt1 = nil;
         dispose (Area2);
      until Area1 = nil;

      { L�schen der Line-Information }
      Line1:=Zeiger2^.Lines; Line2:=Zeiger2^.Lines;
      if Area1 <> nil then
      repeat
         Line2:=Line1;
         Line1:=Line1^.next;
         { Nun werden die Eckpunkte gel�scht }
         Punkt1:=Line2^.Punkte; Punkt2:=Line2^.Punkte;
         if Point1 <> nil then
         repeat
            Punkt2:=Punkt1;
            Punkt1:=Punkt1^.next;
            dispose(Punkt2);
         until Punkt1 = nil;
         dispose (Line2);
      until Line1 = nil;

      { L�schen der Circle-Information }
      Circle1:=Zeiger2^.Circles; Circle2:=Zeiger2^.Circles;
      if Circle1 <> nil then
      repeat
         Circle2:=Circle1;
         Circle1:=Circle1^.next;
         dispose(Circle2);
      until Circle1 = nil;

      { L�schen der Text-Information }
      Text1:=Zeiger2^.Texts; Text2:=Zeiger2^.Texts;
      if Text1 <> nil then
      repeat
         Text2:=Text1;
         Text1:=Text1^.next;
         dispose(Text2);
      until Text1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassText.Destroy;
var Zeiger1,Zeiger2:p_Text_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassCircle.Destroy;
var Zeiger1,Zeiger2:p_Circle_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassLayer.Destroy;
var Zeiger1,Zeiger2:p_Layer_record;
begin
   Zeiger1:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassArea.Destroy;
var Zeiger1,Zeiger2:p_Area_record;
    Punkt1,Punkt2: p_Punkt_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Punkte }
      Punkt1:=Zeiger2^.Punkte; Punkt2:=Zeiger2^.Punkte;
      if Punkt1 <> nil then
      repeat
         Punkt2:=Punkt1;
         Punkt1:=Punkt1^.next;
         dispose(Punkt2);
      until Punkt1 = nil;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassLine.Destroy;
var Zeiger1,Zeiger2:p_Line_record;
    Punkt1,Punkt2: p_Punkt_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Punkte }
      Punkt1:=Zeiger2^.Punkte; Punkt2:=Zeiger2^.Punkte;
      if Punkt1 <> nil then
      repeat
         Punkt2:=Punkt1;
         Punkt1:=Punkt1^.next;
         dispose(Punkt2);
      until Punkt1 = nil;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;



{*******************************************************************************
* PROZEDUR : ClassLayer.ResetCurrent                                           *
********************************************************************************
* Proezdur setzt den Current Layerzeiger auf den Anfang.                       *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassLayer.ResetCurrent;
begin
   Current:=Anfang;
end;

procedure ClassSymbol.ResetCurrent;
begin
   Current:=Anfang
end;

procedure ClassSymbol.SetCurrentNext;
begin
   Current:=Current^.next;
end;

function ClassSymbol.GetCurrent:p_Symbol_record;
begin
   result:=Current;
end;

procedure ClassSymbolRef.ResetCurrent;
begin
   Current:=Anfang;
end;

procedure ClassArea.ResetCurrent;
begin
   current:=Anfang;
end;

procedure ClassCircle.ResetCurrent;
begin
   current:=Anfang;
end;

procedure ClassText.ResetCurrent;
begin
   Current:=Anfang;
end;

procedure ClassText.SetCurrentNext;
begin
   Current:=Current^.next;
end;


procedure ClassCircle.SetCurrentNext;
begin
   Current:=Current^.next;
end;

{*******************************************************************************
* PROZEDUR : ClassLayer.SetCurrentnext                                         *
********************************************************************************
* Proezdur setzt den Current Layerzeiger auf den n�chsten Eintrag.             *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
********************************************************************************}
procedure ClassLayer.SetCurrentnext;
begin
   if Current^.next <> nil then Current:=Current^.next;
end;

procedure ClassSymbolRef.SetCurrentnext;
begin
   if Current^.next <> nil then Current:=Current^.next;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.GetAnzahl                                              *
********************************************************************************
* Funktion ermittelt die Anzahl der vorhandenen Layer.                         *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�CKGABEWERT: Anzahl der Layer                                               *
*                                                                              *
********************************************************************************}
function ClassLayer.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassArea.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassLine.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassSymbol.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassPoint.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

procedure ClassPoint.ResetCurrent;
begin
   Current:=Anfang;
end;

function ClassPoint.GetCurrent:p_Point_record;
begin
   result:=Current;
end;

function ClassText.GetCurrent:p_Text_record;
begin
   result:=Current;
end;

function ClassCircle.GetCurrent:p_Circle_record;
begin
   result:=Current;
end;

function ClassArea.GetCurrent:p_Area_record;
begin
   result:=Current;
end;

function ClassLine.GetCurrent:p_Line_record;
begin
   result:=Current;
end;

procedure ClassPoint.SetCurrentNext;
begin
   Current:=Current^.next;
end;

procedure ClassLine.ResetCurrent;
begin
   Current:=Anfang;
end;

procedure ClassLine.SetCurrentNext;
begin
   Current:=Current^.next;
end;

procedure ClassArea.SetCurrentNext;
begin
   Current:=Current^.next;
end;

function ClassCircle.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassText.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

function ClassSymbolRef.GetAnzahl:integer;
begin
   Result:=Anzahl;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.GetCurrentLayername                                    *
********************************************************************************
* Funktion ermittelt den aktuellen Layernamen.                                 *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�CKGABEWERT: Name des Layers.                                               *
*                                                                              *
********************************************************************************}
function ClassLayer.GetCurrentLayername:string;
begin
   Result:=Current^.Layername;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.Checkexists                                            *
********************************************************************************
* Funktion �berpr�ft, ob dieser Layer bereits in der Liste vorkommt.           *
*                                                                              *
* PARAMETER: Layername -> Layername, dessen Vorkommen �berpr�ft werden soll.   *
*                                                                              *
* R�CKGABEWERT: TRUE -> Layer existiert bereits                                *
*               FALSE -> Layer existiert noch nicht                            *
********************************************************************************}
function ClassLayer.CheckExists(Layername:string):boolean;
var Zeiger:p_Layer_record;
    gefunden:boolean;
begin
   Zeiger:=Anfang; gefunden:=false;
   if Zeiger = nil then result:=false
   else
   begin
      repeat
         if Zeiger^.Layername <> Layername then Zeiger:=Zeiger^.next else gefunden:=true;
      until (gefunden = true) or (Zeiger = nil);
   end;
   if (Zeiger = nil) then result:=false else begin result:=true; Current:=Zeiger; end;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.Hextoint                                               *
********************************************************************************
* Prozedur dient zum Auswerten einer zweistelligen Hexadezimalzahl             *
*                                                                              *
* PARAMETER: Wert -> String, der Hexadezimalzahl beinhaltet.                   *
*                                                                              *
* R�CKGABEWERT: Integerwert der Hexzahl                                        *
*                                                                              *
********************************************************************************}
function ClassLayer.Hextoint(Wert:string):longint;
var Zeichen1,Zeichen2:string;
    Wert1,Wert2:longint;
    count:integer;
begin
   Zeichen1:=copy(Wert,1,1); Zeichen2:=copy(Wert,2,1);
   if Zeichen1 = 'A' then Wert1:=10 else
   if Zeichen1 = 'B' then Wert1:=11 else
   if Zeichen1 = 'C' then Wert1:=12 else
   if Zeichen1 = 'D' then Wert1:=13 else
   if Zeichen1 = 'E' then Wert1:=14 else
   if Zeichen1 = 'F' then Wert1:=15 else
      val(Zeichen1,Wert1,count);
   Wert1:=Wert1 * 16;

   if Zeichen2 = 'A' then Wert2:=10 else
   if Zeichen2 = 'B' then Wert2:=11 else
   if Zeichen2 = 'C' then Wert2:=12 else
   if Zeichen2 = 'D' then Wert2:=13 else
   if Zeichen2 = 'E' then Wert2:=14 else
   if Zeichen2 = 'F' then Wert2:=15 else
      val(Zeichen2,Wert2,count);

   Result:=Wert1 + Wert2;
end;

{*******************************************************************************
* FUNKTION : ClassLayer.GetCurrent                                             *
********************************************************************************
* Funktion gibt den Current Layerzeiger zur�ck.                                *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�CKGABEWERT: Zeiger auf den Current Layer.                                  *
********************************************************************************}
function ClassLayer.GetCurrent:p_Layer_record;
begin
   Result:=Current;
end;

{*******************************************************************************
* PROZEDUR : ClassText.Neu                                                     *
********************************************************************************
* Prozedur erzeugt einen neuen Text Eintrag in der Liste.                      *
*                                                                              *
* PARAMETER: X1    -> Erste X-Koordinate des Texts                             *
*            Y1    -> Erste Y-Koordinate des Texts                             *
*            X2    -> Zweite X-Koordinate des Texts                            *
*            Y2    -> Zweite Y-Koordinate des Texts                            *
*            Text  -> Inhalt des Texts                                         *
*            Font  -> Font, der f�r diesen Text verwendet wird                 *
*            Size  -> Gr��e des Texts                                          *
*            Angle -> Winkel                                                   *
*            Layer -> Zeiger auf den Layer auf dem sich diese Area befindet.   *
*                                                                              *
********************************************************************************}
procedure ClassText.Neu(X1:double;Y1:double;X2:double;Y2:double;Text:string;Font:string;size:double;Angle:double;Layer:p_Layer_record);
var neuer_record:p_Text_record;
begin
   new(neuer_record);
   neuer_record^.X1:=X1;
   neuer_record^.Y1:=Y1;
   neuer_record^.X2:=X2;
   neuer_record^.Y2:=Y2;
   neuer_record^.Text:=Text;
   neuer_record^.Font:=Font;
   neuer_record^.Size:=size;
   neuer_record^.Angle:=360 - Angle;
   neuer_record^.Layer:=Layer;

   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

function ClassSymbolRef.CheckSymbolIsEmpty:boolean;
var retVal:boolean;
begin
   retVal:=true;
   if Current^.Points <> nil then retVal:=false;
   if Current^.Areas <> nil then retVal:=false;
   if Current^.Lines <> nil then retVal:=false;
   if Current^.Circles <> nil then retVal:=false;
   if Current^.Texts <> nil then retVal:=false;
   result:=retVal;
end;

function ClassSymbolRef.CheckSymbolRefExists(SymName:string):boolean;
var found:boolean;
    iterate:p_SymbolRef_record;
begin
   found:=false;
   iterate:=Anfang;
   while (iterate <> nil) and (not found) do
   begin
      if (iterate^.SymName = SymName) then
         found:=true
      else
         iterate:=iterate^.next;
   end;
   result:=found;
end;

procedure ClassSymbolRef.WriteSymbolDefBottom;
begin
   // procedure writes bottom of block-definition
   FileObj.WriteDXF('ENDBLK');
   FileObj.WriteDXF('  8');
   FileObj.WriteDXF('0');
   FileObj.WriteDXF('  0');
end;

procedure ClassSymbolRef.WriteSymbolDefHeader;
begin
   { Prozedur schreibt den Definitionskopf einer Symbolreferenz }
   FileObj.WriteDXF('BLOCK');
   FileObj.WriteDXF('  2');
   FileObj.WriteDXF(Current^.SymName); { Schreiben des Blocknamens }
   FileObj.WriteDXF(' 70');
   FileObj.WriteDXF('     2');         { Schreiben des Blocktyps }
   FileObj.WriteDXF(' 10');
   FileObj.WriteDXF(MainWnd.MyFloatToStr(Current^.X)); { Schreiben des Einsetz-X }
   FileObj.WriteDXF(' 20');
   FileObj.WriteDXF(MainWnd.MyFloatToStr(Current^.Y)); { Schreiben des Einsetz-Y }
   FileObj.WriteDXF(' 30');
   FileObj.WriteDXF('0.0');
   FileObj.WriteDXF('  3');
   FileObj.WriteDXF(Current^.SymName); { Schreiben des Blocknamens }
   FileObj.WriteDXF('  0');
end;

function ClassSymbolRef.GetPoints:p_Point_record;
begin
   Result:=Current^.Points;
end;

function ClassSymbolRef.GetLines:p_Line_record;
begin
   Result:=Current^.Lines;
end;

function ClassSymbolRef.GetAreas:p_Area_record;
begin
   Result:=Current^.Areas;
end;

function ClassSymbolRef.GetCircles:p_Circle_record;
begin
   Result:=Current^.Circles;
end;

function ClassSymbolRef.GetTexts:p_Text_record;
begin
   Result:=Current^.Texts;
end;

procedure ClassSymbolRef.New_Circle(X:double; Y:double; Radius:integer);
var neuer_record:p_Circle_record;
    Zeiger:p_Circle_record;
begin
   new (neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.Layer:=nil;
   neuer_record^.Radius:=Radius;
   if Current^.Circles = nil then
   begin
      Current^.Circles:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=Current^.Circles;
      { Der neue Punkt kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=neuer_record;
      neuer_record^.next:=nil;
   end;
end;

procedure ClassSymbolRef.New_Text(X1:double;Y1:double;X2:double;Y2:double;Text:string;Font:string;size:double;Angle:double);
var neuer_record:p_Text_record;
    Zeiger:p_Text_record;
begin
   new(neuer_record);
   neuer_record^.X1:=X1;
   neuer_record^.Y1:=Y1;
   neuer_record^.X2:=X2;
   neuer_record^.Y2:=Y2;
   neuer_record^.Text:=Text;
   neuer_record^.Font:=Font;
   neuer_record^.Size:=size;
   neuer_record^.Angle:=Angle;
   neuer_record^.Layer:=nil;

   if Current^.Texts = NIL then
   begin
      Current^.Texts:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=Current^.Texts;
      { Der neue Punkt kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=neuer_record;
      neuer_record^.next:=nil;
   end;
end;

procedure ClassSymbolRef.New_Area;
var neuer_record:p_Area_record;
    Zeiger:p_Area_record;
begin
   new (neuer_record);
   neuer_record^.Punktanz:=0;
   neuer_record^.Punkte:=nil;
   neuer_record^.Layer:=nil;

   if Current^.Areas = nil then
   begin
      Current^.Areas:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=Current^.Areas;
      { Der neue Punkt kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=neuer_record;
      neuer_record^.next:=nil;
   end;
   Current_Area:=neuer_record;
end;

procedure ClassSymbolRef.New_Line;
var neuer_record:p_Line_record;
    Zeiger:p_Line_record;
begin
   new (neuer_record);
   neuer_record^.Punktanz:=0;
   neuer_record^.Punkte:=nil;
   neuer_record^.Layer:=nil;

   if Current^.Lines = nil then
   begin
      Current^.Lines:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=Current^.Lines;
      { Der neue Punkt kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=neuer_record;
      neuer_record^.next:=nil;
   end;
   Current_Line:=neuer_record;
end;

procedure ClassSymbolRef.New_Point(X:double; Y:double);
var neuer_record:p_Point_record;
    Zeiger:p_Point_record;
begin
   new (neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.Layer:=nil;
   if Current^.Points = nil then
   begin
      Current^.Points:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=Current^.Points;
      { Der neue Punkt kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=neuer_record;
      neuer_record^.next:=nil;
   end;
end;

procedure ClassSymbolRef.Neu (X:double;Y:double;Symname:string);
var neuer_record:p_Symbolref_record;
    count:integer;
begin
   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.SymName:=SymName;
   neuer_record^.Points:=nil;
   neuer_record^.Areas:=nil;
   neuer_record^.Lines:=nil;
   neuer_record^.Circles:=nil;
   neuer_record^.Texts:=nil;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassCircle.Neu                                                   *
********************************************************************************
* Prozedur erzeugt einen neuen Circle Eintrag in der Liste.                    *
*                                                                              *
* PARAMETER: X     -> X-Koordinate des Kreiseinsetzpunktes                     *
*            Y     -> Y-Koordinate des Kreiseinsetzpunktes                     *
*            Radius-> Radius des Kreises                                       *
*            Layer -> Zeiger auf den Layer auf dem sich dieser Kreis befindet. *
*                                                                              *
********************************************************************************}
procedure ClassCircle.Neu(X:double;Y:double;Radius:integer;Layer:p_Layer_record);
var neuer_record:p_Circle_record;
    count:integer;
begin
   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.Layer:=Layer;
   neuer_record^.Radius:=Radius;
   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassArea.Neu                                                     *
********************************************************************************
* Prozedur erzeugt einen neuen Area Eintrag in der Liste.                      *
*                                                                              *
* PARAMETER: Layer -> Zeiger auf den Layer auf dem sich diese Area befindet.   *
*                                                                              *
********************************************************************************}
procedure ClassArea.Neu(Layer:p_Layer_record);
var neuer_record:p_Area_record;
begin
   new(neuer_record);
   neuer_record^.Punkte:=nil;
   neuer_record^.Layer:=Layer;
   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;
   neuer_record^.Punktanz:=0;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassLine.Neu                                                     *
********************************************************************************
* Prozedur erzeugt einen neuen Line Eintrag in der Liste.                      *
*                                                                              *
* PARAMETER: Layer -> Zeiger auf den Layer auf dem sich diese Line befindet.   *
*                                                                              *
********************************************************************************}
procedure ClassLine.Neu(Layer:p_Layer_record);
var neuer_record:p_Line_record;
begin
   new(neuer_record);
   neuer_record^.Punkte:=nil;
   neuer_record^.Layer:=Layer;
   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;
   neuer_record^.Punktanz:=0;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassPoint.Neu                                                    *
********************************************************************************
* Prozedur erzeugt einen neuen Point Eintrag in der Liste.                     *
*                                                                              *
* PARAMETER: X -> X-Koordinate des Punkts                                      *
*            Y -> Y-Koordinate des Punkts                                      *
*            Layer -> Zeiger auf den Layer auf dem sich dieser Point befindet. *
*                                                                              *
********************************************************************************}
procedure ClassPoint.Neu(X:double;Y:double;Layer:p_Layer_record);
var neuer_record:p_Point_record;
begin
   new(neuer_record);
   neuer_record^.Layer:=Layer;
   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

procedure ClassSymbol.Neu(X:double;Y:double;SymName:string;Size:double;Angle:double;Layer:p_Layer_record);
var neuer_record:p_Symbol_record;
begin
   new(neuer_record);
   neuer_record^.Layer:=Layer;
   Layer^.Objectsonlayer:=Layer^.Objectsonlayer + 1;
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.SymName:=SymName;
   neuer_record^.Size:=Size;
   neuer_record^.Angle:=Angle;
   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;



{*******************************************************************************
* PROZEDUR : ClassArea.InsertPoint                                             *
********************************************************************************
* Prozedur f�gt einen Punkt in die Current Area ein.                           *
*                                                                              *
* PARAMETER: X -> X-Koordinate des Punktes                                     *
*            Y -> Y-Koordinate des Punktes                                     *
*                                                                              *
********************************************************************************}
procedure ClassArea.InsertPoint(X:double;Y:double);
var Zeiger:p_Punkt_record;
    new_Zeiger:p_Punkt_record;
begin
   new(new_Zeiger);
   { Kontrolle, ob die Spalten X und Y bereits in der Datenbankliste sind }
   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   Zeiger:=Current^.Punkte;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current^.Punkte:=New_Zeiger;
      Current^.Punkte^.next:=NIL;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
   end;
   Current^.Punktanz:=Current^.Punktanz + 1;
end;

procedure ClassSymbolRef.InsertPointToArea(X:double;Y:double);
var Zeiger:p_Punkt_record;
    new_Zeiger:p_Punkt_record;
begin
   new(new_Zeiger);
   { Kontrolle, ob die Spalten X und Y bereits in der Datenbankliste sind }
   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   Zeiger:=Current_Area^.Punkte;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current_Area^.Punkte:=New_Zeiger;
      Current_Area^.Punkte^.next:=NIL;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
   end;
   Current_Area^.Punktanz:=Current_Area^.Punktanz + 1;
end;

procedure ClassSymbolRef.InsertPointToLine(X:double;Y:double);
var Zeiger:p_Punkt_record;
    new_Zeiger:p_Punkt_record;
begin
   new(new_Zeiger);
   { Kontrolle, ob die Spalten X und Y bereits in der Datenbankliste sind }
   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   Zeiger:=Current_Line^.Punkte;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current_Line^.Punkte:=New_Zeiger;
      Current_Line^.Punkte^.next:=NIL;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
   end;
   Current_Line^.Punktanz:=Current_Line^.Punktanz + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassArea.WriteData                                               *
********************************************************************************
* Prozedur schreibt die Daten der Area, die sich auf einem bestimmten          *
*                                                                              *
* PARAMETER: Layername -> Name des Layers auf dem sich die Objekte befinden    *
*                         sollen.
*                                                                              *
********************************************************************************}
procedure ClassArea.WriteData(Cur_Area:p_area_record);
var Curr_Point:p_punkt_record;
begin
   FileObj.WriteDXF('POLYLINE');
   FileObj.WriteDXF('  8');
   if Cur_Area^.Layer <> nil then FileObj.WriteDXF(Cur_Area^.Layer^.Layername)
   else FileObj.WriteDXF('0');
   FileObj.WriteDXF(' 66');
   FileObj.WriteDXF('     1');
   FileObj.WriteDXF(' 70');
   FileObj.WriteDXF('1');
   FileObj.WriteDXF(' 10');
   FileObj.WriteDXF('0.0');
   FileObj.WriteDXF(' 20');
   FileObj.WriteDXF('0.0');
   FileObj.WriteDXF(' 30');
   FileObj.WriteDXF('0.0');
   FileObj.WriteDXF('  0');

   { Nun werden die Eckpunkte geschrieben }
   Curr_Point:=Cur_Area^.Punkte;
   while (Curr_Point <> nil) do
   begin
      FileObj.WriteDXF('VERTEX');
      FileObj.WriteDXF('  8');
      if Cur_Area^.Layer <> nil then FileObj.WriteDXF(Cur_Area^.Layer^.Layername)
      else FileObj.WriteDXF('0');
      FileObj.WriteDXF(' 10');
      FileObj.WriteDXF(MainWnd.MyFloatToStr(Curr_Point^.X));
      FileObj.WriteDXF(' 20');
      FileObj.WriteDXF(MainWnd.MyFloatToStr(Curr_Point^.Y));
      FileObj.WriteDXF(' 30');
      FileObj.WriteDXF('0.0');
      FileObj.WriteDXF('  0');
      Curr_Point:=Curr_Point^.next;
   end;
   FileObj.WriteDXF('SEQEND');
   FileObj.WriteDXF('  8');
   if Cur_Area^.Layer <> nil then FileObj.WriteDXF(Cur_Area^.Layer^.Layername)
   else FileObj.WriteDXF('0');
   FileObj.WriteDXF('  0');
end;


{*******************************************************************************
* PROZEDUR : ClassLine.WriteData                                               *
********************************************************************************
* Prozedur schreibt die Daten der Line, die sich auf einem bestimmten          *
*                                                                              *
* PARAMETER: Layername -> Name des Layers auf dem sich die Objekte befinden    *
*                         sollen.
*                                                                              *
********************************************************************************}
procedure ClassLine.WriteData(Cur_Line:p_Line_record);
var Curr_Point:p_punkt_record;
    num_points : integer;
    num_routes : integer;
    lastroutelength : integer;
    ftemp : double;
begin
   num_points := 0;
   // Anzahl der Eckpunkte ermitteln
   Curr_Point:=Cur_Line^.Punkte;
   while (Curr_Point <> nil) do
   	begin
        inc(num_points);
        Curr_Point:=Curr_Point^.next;
        end;
   if (SelDXFWnd.IgnoreRB.Checked and (num_points > SelDXFWnd.RoutesMaxLengthSE.Value)) then
   	begin
        inc (Maindlg.OVLRoutesignored);
        exit;
        end;


   if (num_points <= SelDXFWnd.RoutesMaxLengthSE.Value) then                              // Route schreiben
   begin
   // Header f�r eine Route in die OVL Datei schreiben
   FileObj.WriteDXF('[Symbol '+IntToStr(Maindlg.OVLWaypointsworked+Maindlg.OVLRoutesworked)+']');
   FileObj.WriteDXF('Typ=3');
   FileObj.WriteDXF('Group=1');
   FileObj.WriteDXF('Col=1');
   FileObj.WriteDXF('Zoom=1');
   FileObj.WriteDXF('Size=102');
   FileObj.WriteDXF('Punkte='+IntToStr(num_points));
   // Punkte schreiben

   num_points := 0;
   Curr_Point:=Cur_Line^.Punkte;
   while (Curr_Point <> nil) do
   	begin
        FileObj.WriteDXF('XKoord'+IntToStr(num_points)+'='+MainWnd.MyFloatToStr(Curr_Point^.X));
        FileObj.WriteDXF('YKoord'+IntToStr(num_points)+'='+MainWnd.MyFloatToStr(Curr_Point^.Y));
        inc(num_points);
        Curr_Point:=Curr_Point^.next;
        end;

  FileObj.WriteDXF(#13);
  inc(Maindlg.OVLRoutesworked);
  exit;
  end;


  ftemp := num_points/SelDXFWnd.RoutesMaxLengthSE.Value; 				   // Route splitten
  num_routes := Floor(ftemp);
  inc(Maindlg.OVLRoutessplitted);

  if num_routes <> ftemp then
        begin
        lastroutelength := num_points-num_routes*SelDXFWnd.RoutesMaxLengthSE.Value;
        end;

  Curr_Point:=Cur_Line^.Punkte;
  while ((Curr_Point <> nil) and (Maindlg.OVLRoutesworked < SelDXFWnd.RoutesMaxCountSE.Value))  do
  begin
	dec(num_routes);
        if num_routes >=0 then num_points := SeldxfWnd.RoutesMaxLengthSE.Value
    		else num_points := lastroutelength;


   		// Header f�r eine Route in die OVL Datei schreiben
   		FileObj.WriteDXF('[Symbol '+IntToStr(Maindlg.OVLWaypointsworked+Maindlg.OVLRoutesworked)+']');
                FileObj.WriteDXF('Typ=3');
   		FileObj.WriteDXF('Group=1');
   		FileObj.WriteDXF('Col=1');
   		FileObj.WriteDXF('Zoom=1');
   		FileObj.WriteDXF('Size=102');
                FileObj.WriteDXF('Punkte='+IntToStr(num_points));

   		num_points := 0;
   		// Curr_Point:=Cur_Line^.Punkte;
   		while (Curr_Point <> nil)and (num_points < SeldxfWnd.RoutesMaxLengthSE.Value)  do
   			begin
        		FileObj.WriteDXF('XKoord'+IntToStr(num_points)+'='+MainWnd.MyFloatToStr(Curr_Point^.X));
        		FileObj.WriteDXF('YKoord'+IntToStr(num_points)+'='+MainWnd.MyFloatToStr(Curr_Point^.Y));
        		inc(num_points);
        		Curr_Point:=Curr_Point^.next;
        	end;
   	FileObj.WriteDXF(#13);

    inc(Maindlg.OVLRoutesworked);
  end  // endwhile;


   {
   FileObj.WriteDXF('POLYLINE');
   FileObj.WriteDXF('  8');
   if Cur_Line^.Layer <> nil then FileObj.WriteDXF(Cur_Line^.Layer^.Layername)
   else FileObj.WriteDXF('0');
   FileObj.WriteDXF(' 66');
   FileObj.WriteDXF('     1');
   FileObj.WriteDXF(' 70');
   FileObj.WriteDXF('0');
   FileObj.WriteDXF(' 10');
   FileObj.WriteDXF('0.0');
   FileObj.WriteDXF(' 20');
   FileObj.WriteDXF('0.0');
   FileObj.WriteDXF(' 30');
   FileObj.WriteDXF('0.0');
   FileObj.WriteDXF('  0');

   { Nun werden die Eckpunkte geschrieben
   Curr_Point:=Cur_Line^.Punkte;
   while (Curr_Point <> nil) do
   begin
      FileObj.WriteDXF('VERTEX');
      FileObj.WriteDXF('  8');
      if Cur_Line^.Layer <> nil then FileObj.WriteDXF(Cur_Line^.Layer^.Layername)
      else FileObj.WriteDXF('0');
      FileObj.WriteDXF(' 10');
      FileObj.WriteDXF(MainWnd.MyFloatToStr(Curr_Point^.X));
      FileObj.WriteDXF(' 20');
      FileObj.WriteDXF(MainWnd.MyFloatToStr(Curr_Point^.Y));
      FileObj.WriteDXF(' 30');
      FileObj.WriteDXF('0.0');
      FileObj.WriteDXF('  0');
      Curr_Point:=Curr_Point^.next;
   end;
   FileObj.WriteDXF('SEQEND');
   FileObj.WriteDXF('  8');
   if Cur_Line^.Layer <> nil then FileObj.WriteDXF(Cur_Line^.Layer^.Layername)
   else FileObj.WriteDXF('0');
   FileObj.WriteDXF('  0');
   }

end;

procedure ClassText.WriteData(Curr_Text:p_Text_record);
begin
   FileObj.WriteDXF('TEXT');
   FileObj.WriteDXF('  8');
   if Curr_Text^.Layer <> nil then FileObj.WriteDXF(Curr_Text^.Layer^.Layername)
   else FileObj.WriteDXF('0');
   FileObj.WriteDXF(' 10');
   FileObj.WriteDXF(MainWnd.MyFloatToStr(Curr_Text^.X1));
   FileObj.WriteDXF(' 20');
   FileObj.WriteDXF(MainWnd.MyFloatToStr(Curr_Text^.Y1));
   FileObj.WriteDXF(' 30');
   FileObj.WriteDXF('0.0');
   FileObj.WriteDXF(' 40');
   FileObj.WriteDXF(MainWnd.MyFloatToStr(Curr_Text^.size));
   FileObj.WriteDXF('  1');
   FileObj.WriteDXF(Curr_Text^.Text);
   FileObj.WriteDXF(' 50');
   FileObj.WriteDXF(MainWnd.MyFloatToStr(Curr_Text^.angle));
   FileObj.WriteDXF('  7');
   FileObj.WriteDXF('STANDARD');
   FileObj.WriteDXF('  0');
end;

procedure ClassCircle.WriteData(Curr_Circle:p_Circle_record);
begin
   FileObj.WriteDXF('CIRCLE');
   FileObj.WriteDXF('  8');
   if Curr_Circle^.Layer <> nil then FileObj.WriteDXF(Curr_Circle^.Layer^.Layername)
   else FileObj.WriteDXF('0');
   FileObj.WriteDXF(' 10');
   FileObj.WriteDXF(MainWnd.MyFloatToStr(Curr_Circle^.X));
   FileObj.WriteDXF(' 20');
   FileObj.WriteDXF(MainWnd.MyFloatToStr(Curr_Circle^.Y));
   FileObj.WriteDXF(' 30');
   FileObj.WriteDXF('0.0');
   FileObj.WriteDXF(' 40');
   FileObj.WriteDXF(MainWnd.MyFloatToStr(Curr_Circle^.Radius));
   FileObj.WriteDXF(' 0');
end;

{*******************************************************************************
* FUNKTION : ClassText.GetLength                                               *
********************************************************************************
* Funktion ermittelt die L�nge eines Textes in Meter                           *
*                                                                              *
* PARAMETER: Text -> Text, von dem die L�nge ermittelt werden soll.            *
*            Size -> Gr��e, die der Text hat.                                  *
*                                                                              *
* R�CKGABEWERT: L�nge des Texts in Meter.                                      *
*                                                                              *
********************************************************************************}
function ClassText.GetLength(Text:string;Size:double):double;
begin
    Result:=length(Text) * (Size * 4/10);
end;


{*******************************************************************************
* FUNKTION : ClassText.GetHeight                                               *
********************************************************************************
* Funktion ermittelt die H�he eines Textes in Meter                            *
*                                                                              *
* PARAMETER: Size -> Gr��e, die der Text hat.                                  *
*                                                                              *
* R�CKGABEWERT: H�he des Texts in Meter.                                       *
*                                                                              *
********************************************************************************}
function ClassText.GetHeight(Size:double):double;
begin
   Result:=Size * 6 / 10;
end;

{*******************************************************************************
* FUNKTION : ClassText.GetAngle                                                *
********************************************************************************
* Funktion ermittelt den Winkel mit dem ein Text rotiert ist.                  *
*                                                                              *
* PARAMETER: X1 -> Erste X-Koordinate des Texts                                *
*            Y1 -> Erste Y-Koordinate des Texts                                *
*            X2 -> Zweite X-Koordinate des Texts                               *
*            Y2 -> Zweite Y-Koordinate des Texts                               *
*                                                                              *
* R�CKGABEWERT: Winkel mit den dieser Text rotiert ist                         *
*                                                                              *
********************************************************************************}
function ClassText.GetAngle(X1:double;Y1:double;X2:double;Y2:double):double;
var A,B,C:double;
    Alpha:double;
begin
   { Zuwerst werden die Kantenl�ngen des Dreiecks ermittelt }
   A:=ABS(Y2 - Y1); B:=ABS(X2 - X1); C:=SQRT(SQR(A) + SQR(B));

   Alpha:=(A / C);  { Nun ist der Sinus von Alpha bekannt }

   { Nun muss man noch den Invers Sinus von Alpha berechnen }
   Alpha:=ArcSin(Alpha);
   Result:=(Alpha*180) / PI;
end;

procedure ClassPoint.WriteData(Curr_Point:p_Point_record);
var Zeiger:p_Point_record;
    laenge:integer;
    Zeile,hstr:string;
begin

   // Wegpunkt in die OVL Datei schreiben
   FileObj.WriteDXF('[Symbol '+IntToStr(Maindlg.OVLWaypointsworked)+']');
   FileObj.WriteDXF('Typ=6');
   FileObj.WriteDXF('Group=2');
   FileObj.WriteDXF('Dir=100');
   FileObj.WriteDXF('Col=1');
   FileObj.WriteDXF('Zoom=1');
   FileObj.WriteDXF('Size=102');
   FileObj.WriteDXF('Area=2');
   FileObj.WriteDXF('XKoord='+MainWnd.MyFloatToStr(Curr_Point^.X));
   FileObj.WriteDXF('YKoord='+MainWnd.MyFloatToStr(Curr_Point^.Y));
   FileObj.WriteDXF(#13);
   inc(OVLWaypointsworked);

   {
   FileObj.WriteDXF('POINT');
   FileObj.WriteDXF('  8');
   if Curr_Point^.Layer <> nil then FileObj.WriteDXF(Curr_Point^.Layer^.Layername)
   else FileObj.WriteDXF('0');
   FileObj.WriteDXF(' 10');
   FileObj.WriteDXF(MainWnd.MyFloatToStr(Curr_Point^.X));
   FileObj.WriteDXF(' 20');
   FileObj.WriteDXF(MainWnd.MyFloatToStr(Curr_Point^.Y));
   FileObj.WriteDXF(' 30');
   FileObj.WriteDXF('0.0');
   FileObj.WriteDXF('  0');
   }
end;

procedure ClassSymbol.WriteData(Cur_Symbol:p_Symbol_record);
begin
   FileObj.WriteDXF('INSERT');
   FileObj.WriteDXF('  8');
   if Cur_Symbol^.Layer <> nil then FileObj.WriteDXF(Cur_Symbol^.Layer^.Layername)
   else FileObj.WriteDXF('0');
   FileObj.WriteDXF('  2');
   FileObj.WriteDXF(Cur_Symbol^.SymName);
   FileObj.WriteDXF(' 10');
   FileObj.WriteDXF(MainWnd.MyFloatToStr(Cur_Symbol^.X));
   FileObj.WriteDXF(' 20');
   FileObj.WriteDXF(MainWnd.MyFloatToStr(Cur_Symbol^.Y));
   FileObj.WriteDXF(' 30');
   FileObj.WriteDXF('0.0');
   FileObj.WriteDXF(' 50');
   FileObj.WriteDXF(MainWnd.MyFloatToStr(Cur_Symbol^.Angle));
   FileObj.WriteDXF(' 41');
   FileObj.WriteDXF(MainWnd.MyFloatToStr(Cur_Symbol^.Size/MainWnd.SymbolScale));
   FileObj.WriteDXF(' 42');
   FileObj.WriteDXF(MainWnd.MyFloatToStr(Cur_Symbol^.Size/MainWnd.SymbolScale));
   FileObj.WriteDXF('  0');
end;

{*******************************************************************************
* PROZEDUR : ClassLine.InsertPoint                                             *
********************************************************************************
* Prozedur f�gt einen Punkt in die Current Line ein.                           *
*                                                                              *
* PARAMETER: X -> X-Koordinate des Punktes                                     *
*            Y -> Y-Koordinate des Punktes                                     *
*                                                                              *
********************************************************************************}
procedure ClassLine.InsertPoint(X:double;Y:double);
var Zeiger:p_Punkt_record;
    new_Zeiger:p_Punkt_record;
    hstr      :string;
begin
   new(new_Zeiger);
   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   Zeiger:=Current^.Punkte;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current^.Punkte:=New_Zeiger;
      Current^.Punkte^.next:=NIL;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
   end;
   Current^.Punktanz:=Current^.Punktanz + 1;
end;

procedure ClassLayer.Neu(Layername:string;Line_Type:string;Line_Color:string;Fill_Type:string;Fill_Color:string);
var neuer_record,Zeiger:p_Layer_record;
begin
   new(neuer_record);
   neuer_record^.Layername:=Layername;
   neuer_record^.Linestyle:=Line_Type;
   neuer_record^.Linecolor:=Line_Color;
   neuer_record^.Areastyle:=Fill_Type;
   neuer_record^.Areacolor:=Fill_color;
   neuer_record^.ObjectsonLayer:=0;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

procedure ClassLayer.WriteLayerTable;
begin
   { Prozedur schreibt die Layerdeklaration in die Layertabelle }
   FileObj.WriteDXF('LAYER');
   FileObj.WriteDXF('  2');
   FileObj.WriteDXF(Current^.Layername);
   FileObj.WriteDXF(' 70');
   FileObj.WriteDXF('    64');           { Art wie der Layer angezeigt wird }
   FileObj.WriteDXF(' 62');
   FileObj.WriteDXF(Current^.Linecolor); { Linienfarbe }
   FileObj.WriteDXF('  6');
   FileObj.WriteDXF(Current^.Linestyle); { Linienstil }
   FileObj.WriteDXF('  0');
end;

end.

