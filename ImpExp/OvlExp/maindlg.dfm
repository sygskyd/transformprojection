object MainWnd: TMainWnd
  Left = 537
  Top = 298
  BorderStyle = bsDialog
  Caption = 'PROGIS ACAD-DXF Export 2000'
  ClientHeight = 351
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'System'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 441
    Height = 305
    Caption = 'Panel1'
    TabOrder = 0
    object Progresslabel: TLabel
      Left = 8
      Top = 8
      Width = 88
      Height = 13
      Caption = 'Progressmessages'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Prozessbalken: TGauge
      Left = 8
      Top = 256
      Width = 425
      Height = 20
      Progress = 0
    end
    object StatusLB: TListBox
      Left = 8
      Top = 24
      Width = 425
      Height = 193
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
    end
    object SaveReportBtn: TButton
      Left = 152
      Top = 224
      Width = 145
      Height = 25
      Caption = 'Save report'
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = SaveReportBtnClick
    end
  end
  object OKBtn: TButton
    Left = 248
    Top = 320
    Width = 93
    Height = 23
    Caption = 'OK'
    Default = True
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 348
    Top = 320
    Width = 101
    Height = 23
    Cancel = True
    Caption = 'Abbrechen'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 2
    OnClick = CancelBtnClick
  end
  object AxGisProjection: TAxGisProjection
    Left = 72
    Top = 320
    Width = 50
    Height = 25
    ParentFont = False
    TabOrder = 3
    OnWarning = AxGisProjectionWarning
    OnError = AxGisProjectionError
    ControlData = {
      54504630065450616E656C00044C656674024803546F70034001055769647468
      02320648656967687402190743617074696F6E060647697350726F0000}
  end
  object CloseTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = CloseTimerTimer
    Left = 8
    Top = 320
  end
  object SaveDialogRep: TSaveDialog
    DefaultExt = '*.rep'
    FileName = '*.rep'
    Filter = 'Export report file|*.rep'
    Left = 56
    Top = 232
  end
  object SetupWindowTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = SetupWindowTimerTimer
    Left = 40
    Top = 320
  end
end
