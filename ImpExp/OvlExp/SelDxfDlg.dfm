object SelDxfWnd: TSelDxfWnd
  Left = 277
  Top = 223
  BorderStyle = bsDialog
  Caption = 'PROGIS ACAD-DXF Export 2000'
  ClientHeight = 351
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 8
    Top = 8
    Width = 441
    Height = 305
    ActivePage = TabSheetSource
    TabIndex = 0
    TabOrder = 0
    object TabSheetSource: TTabSheet
      Caption = 'Source Project'
      ImageIndex = 1
      object Label2: TLabel
        Left = 192
        Top = 120
        Width = 93
        Height = 13
        Caption = 'Routes longer than '
      end
      object Label3: TLabel
        Left = 352
        Top = 120
        Width = 31
        Height = 13
        Caption = 'points:'
      end
      object Bevel1: TBevel
        Left = 184
        Top = 64
        Width = 244
        Height = 176
      end
      object Label4: TLabel
        Left = 192
        Top = 182
        Width = 79
        Height = 13
        Caption = 'Export maximum:'
      end
      object Label5: TLabel
        Left = 364
        Top = 180
        Width = 50
        Height = 13
        Caption = 'waypoints.'
      end
      object Label6: TLabel
        Left = 364
        Top = 208
        Width = 32
        Height = 13
        Caption = 'routes.'
      end
      object Label7: TLabel
        Left = 192
        Top = 70
        Width = 55
        Height = 13
        Caption = 'Presettings:'
      end
      object LayerLB: TListBox
        Left = 4
        Top = 24
        Width = 169
        Height = 249
        DragMode = dmAutomatic
        ItemHeight = 13
        MultiSelect = True
        TabOrder = 0
        OnClick = LayerLBClick
      end
      object LayerCB: TCheckBox
        Left = 4
        Top = 0
        Width = 169
        Height = 17
        Caption = 'Export only selected layers'
        TabOrder = 1
        OnClick = LayerCBClick
      end
      object SelectedCB: TCheckBox
        Left = 176
        Top = 0
        Width = 249
        Height = 17
        Caption = 'Export only selected objects'
        TabOrder = 2
        OnClick = SelectedCBClick
      end
      object PointsCB: TCheckBox
        Left = 176
        Top = 20
        Width = 249
        Height = 17
        Caption = 'Export points (as Waypoints)'
        Checked = True
        State = cbChecked
        TabOrder = 3
        OnClick = PointsCBClick
      end
      object PolylinesCB: TCheckBox
        Left = 176
        Top = 36
        Width = 249
        Height = 17
        Caption = 'Export polylines (as Routes)'
        Checked = True
        State = cbChecked
        TabOrder = 4
        OnClick = PolylinesCBClick
      end
      object RoutesMaxLengthSE: TSpinEdit
        Left = 300
        Top = 116
        Width = 41
        Height = 22
        MaxValue = 999
        MinValue = 0
        TabOrder = 5
        Value = 20
      end
      object SplitRB: TRadioButton
        Left = 204
        Top = 146
        Width = 113
        Height = 17
        Caption = 'split'
        Checked = True
        TabOrder = 6
        TabStop = True
        OnClick = SplitRBClick
      end
      object IgnoreRB: TRadioButton
        Left = 310
        Top = 146
        Width = 113
        Height = 17
        Caption = 'ignore'
        TabOrder = 7
        OnClick = IgnoreRBClick
      end
      object WaypointsMaxCountSE: TSpinEdit
        Left = 300
        Top = 176
        Width = 53
        Height = 22
        MaxValue = 9999
        MinValue = 0
        TabOrder = 8
        Value = 0
      end
      object RoutesMaxCountSE: TSpinEdit
        Left = 300
        Top = 204
        Width = 53
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 9
        Value = 0
      end
      object GPSDevCombo: TComboBox
        Left = 192
        Top = 88
        Width = 230
        Height = 21
        BevelInner = bvLowered
        BevelOuter = bvRaised
        ItemHeight = 13
        TabOrder = 10
        Text = 'GPSDevCombo'
        OnSelect = GPSDevComboSelect
      end
    end
    object TabSheetDest: TTabSheet
      Caption = 'Destination ACAD-DXF'
      ImageIndex = 1
      object Panel2: TPanel
        Left = 4
        Top = 0
        Width = 426
        Height = 41
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 14
          Width = 35
          Height = 13
          Caption = 'Dxf-file:'
        end
        object Panel1: TPanel
          Left = 72
          Top = 8
          Width = 321
          Height = 25
          TabOrder = 0
          object DxfLabel: TLabel
            Left = 8
            Top = 6
            Width = 42
            Height = 13
            Caption = 'DxfLabel'
          end
        end
        object DxfBtn: TButton
          Left = 395
          Top = 8
          Width = 25
          Height = 25
          Caption = '...'
          TabOrder = 1
          OnClick = DxfBtnClick
        end
      end
    end
  end
  object OKBtn: TButton
    Left = 240
    Top = 320
    Width = 93
    Height = 23
    Caption = 'OK'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 348
    Top = 320
    Width = 101
    Height = 23
    Cancel = True
    Caption = 'Abbrechen'
    ModalResult = 2
    TabOrder = 2
    OnClick = CancelBtnClick
  end
  object GeoBtn: TButton
    Left = 8
    Top = 320
    Width = 93
    Height = 23
    Caption = 'Projection'
    Default = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = GeoBtnClick
  end
  object SaveDialogdxf: TSaveDialog
    DefaultExt = '*.ovl'
    FileName = '*.ovl'
    Filter = 'GPS-Data|*.ovl'
    Left = 404
    Top = 88
  end
  object SetupWindowTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = SetupWindowTimerTimer
    Left = 136
    Top = 312
  end
end
