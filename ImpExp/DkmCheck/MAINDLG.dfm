object MainWnd: TMainWnd
  Left = 250
  Top = 123
  BorderStyle = bsDialog
  Caption = 'PROGIS DKM-Processing'
  ClientHeight = 503
  ClientWidth = 454
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 8
    Top = 16
    Width = 441
    Height = 449
    TabOrder = 0
    object Progresslabel: TLabel
      Left = 8
      Top = 234
      Width = 88
      Height = 13
      Caption = 'Progressmessages'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Prozessbalken: TGauge
      Left = 8
      Top = 394
      Width = 425
      Height = 20
      Progress = 0
    end
    object StatusLB: TListBox
      Left = 8
      Top = 248
      Width = 425
      Height = 142
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
    end
    object Panel2: TPanel
      Left = 8
      Top = 419
      Width = 425
      Height = 25
      TabOrder = 1
      object DbLabel: TLabel
        Left = 8
        Top = 8
        Width = 40
        Height = 13
        Caption = 'DbLabel'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
    end
    object Panel3: TPanel
      Left = 8
      Top = 8
      Width = 425
      Height = 25
      BevelOuter = bvLowered
      TabOrder = 2
      object WholeProjCb: TCheckBox
        Left = 4
        Top = 4
        Width = 189
        Height = 17
        Caption = 'Process whole project'
        TabOrder = 0
        OnClick = WholeProjCbClick
      end
      object SelectedCb: TCheckBox
        Left = 200
        Top = 4
        Width = 201
        Height = 17
        Caption = 'Process selected objects'
        TabOrder = 1
        OnClick = SelectedCbClick
      end
    end
    object Panel5: TPanel
      Left = 8
      Top = 34
      Width = 425
      Height = 31
      BevelOuter = bvLowered
      TabOrder = 3
      object ProcessTypePopupMenuBtn: TPopupMenuBtn
        Left = 8
        Top = 4
        Width = 409
        Caption = 'ProcessTypePopupMenuBtn'
        PopupMenu = ProcessTypePopupMenu
        TabOrder = 0
      end
    end
    object ProcessPageControl: TPageControl
      Left = 8
      Top = 72
      Width = 425
      Height = 161
      ActivePage = BEVTabSheet
      TabIndex = 0
      TabOrder = 4
      object BEVTabSheet: TTabSheet
        Caption = 'BEV'
        object Panel4: TPanel
          Left = 2
          Top = 2
          Width = 412
          Height = 127
          BevelOuter = bvLowered
          TabOrder = 0
          object ParzellenCb: TCheckBox
            Left = 4
            Top = 4
            Width = 277
            Height = 17
            Caption = 'Process Parzellen'
            TabOrder = 0
          end
          object MappenBlattCb: TCheckBox
            Left = 4
            Top = 20
            Width = 229
            Height = 17
            Caption = 'Process Mappenbl'#228'tter'
            TabOrder = 1
          end
          object NutzungCb: TCheckBox
            Left = 4
            Top = 36
            Width = 277
            Height = 17
            Caption = 'Process Nutzungen'
            TabOrder = 2
            OnClick = NutzungCbClick
          end
          object NutzungCreateCb: TCheckBox
            Left = 22
            Top = 52
            Width = 307
            Height = 17
            Caption = 'Create Nutzungsflaechen'
            TabOrder = 3
          end
          object NutzungCombineCb: TCheckBox
            Left = 22
            Top = 68
            Width = 163
            Height = 17
            Caption = 'Combine Nutzungsflaechen'
            TabOrder = 4
          end
          object BEVMergeLayersCb: TCheckBox
            Left = 4
            Top = 88
            Width = 325
            Height = 17
            Caption = 'BEVMergeLayersCb'
            TabOrder = 5
          end
          object BEVDeleteLayersCb: TCheckBox
            Left = 4
            Top = 104
            Width = 293
            Height = 17
            Caption = 'BEVDeleteLayersCb'
            TabOrder = 6
          end
        end
      end
      object MedixTabSheet: TTabSheet
        Caption = 'MEDIX'
        ImageIndex = 1
        object MedixSplitCb: TCheckBox
          Left = 4
          Top = 4
          Width = 357
          Height = 17
          Caption = 'MedixSplitCb'
          TabOrder = 0
        end
        object MedixMergeLayersCb: TCheckBox
          Left = 4
          Top = 20
          Width = 357
          Height = 17
          Caption = 'MedixMergeLayersCb'
          TabOrder = 1
        end
        object MedixDeleteLayersCb: TCheckBox
          Left = 4
          Top = 36
          Width = 373
          Height = 17
          Caption = 'MedixDeleteLayersCb'
          TabOrder = 2
        end
      end
      object DfkTabSheet: TTabSheet
        Caption = 'DFK'
        ImageIndex = 2
        object DfkMergeLayersCb: TCheckBox
          Left = 4
          Top = 4
          Width = 325
          Height = 17
          Caption = 'DfkMergeLayersCb'
          TabOrder = 0
        end
        object DfkDeleteLayersCb: TCheckBox
          Left = 4
          Top = 20
          Width = 97
          Height = 17
          Caption = 'DfkDeleteLayersCb'
          TabOrder = 1
        end
      end
    end
  end
  object OKBtn: TButton
    Left = 336
    Top = 472
    Width = 112
    Height = 23
    Caption = 'OK'
    Default = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object AxGisCombineObj: TAxGisCombine
    Left = 176
    Top = 472
    Width = 41
    Height = 25
    ParentFont = False
    TabOrder = 2
    OnDisplayPass = AxGisCombineObjDisplayPass
    OnDisplayPercent = AxGisCombineObjDisplayPercent
    ControlData = {
      54504630065450616E656C00044C65667403B00003546F7003D8010557696474
      6802290648656967687402190743617074696F6E0606476973436D620000}
  end
  object AXImpExpDbc: TAXImpExpDbc
    Left = 120
    Top = 464
    Width = 50
    Height = 25
    ParentFont = False
    TabOrder = 3
    OnDialogSetuped = AXImpExpDbcDialogSetuped
    OnRequestImpProjectName = AXImpExpDbcRequestImpProjectName
    OnRequestExpProjectName = AXImpExpDbcRequestExpProjectName
    ControlData = {
      54504630065450616E656C00044C656674027803546F7003D001055769647468
      02320648656967687402190743617074696F6E06064769734462630000}
  end
  object CancelBtn: TButton
    Left = 216
    Top = 472
    Width = 112
    Height = 23
    Caption = 'Cancel'
    TabOrder = 4
    OnClick = CancelBtnClick
  end
  object DbBtn: TButton
    Left = 8
    Top = 472
    Width = 112
    Height = 23
    Caption = 'Database'
    TabOrder = 5
    OnClick = DbBtnClick
  end
  object Finishtimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = FinishtimerTimer
    Left = 128
    Top = 456
  end
  object ProcessTypePopupMenu: TPopupMenu
    Left = 192
    Top = 448
    object Hugo1: TMenuItem
      Caption = 'Hugo'
    end
    object Semmel1: TMenuItem
      Caption = 'Semmel'
    end
  end
end
