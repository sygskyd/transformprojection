library DKMCheck;

{ Wichtiger Hinweis zur DLL-Speicherverwaltung: Sharemem mu� die erste
 Unit in der uses-Klausel Ihrer Bibliothek und Ihres Projekts sein,
 wenn Ihre DLL Prozeduren und Funktionen exportiert, die
 Strings als Parameter oder Funktionsergebnisse �bergeben. Dies
 betrifft alle Strings, die an Ihre DLL oder von Ihrer DLL �bergeben werden--selbst diejenigen,
 die sich in Records oder Klassen befinden. Sharemem ist die Schnittstelle zu
 BORLNDMM.DLL, die den gemeinsamen Zugriff auf den Speicher verwaltet; DELPHIMM.DLL
 mu� mit Ihrer DLL weitergegeben werden. Wenn Sie BORLNDMM.DLL nicht verwenden m�chten, m�ssen
 Strings als PChar oder ShortString �bergeben werden.}

uses
  Forms,
  SysUtils,
  Classes,
  Dialogs,
  Maindlg in 'MAINDLG.PAS' {MainWnd},
  Strfile in 'STRFILE.PAS',
  AXDLL_TLB,
  comobj;

{$R *.RES}
{*******************************************************************************
* PROZEDURE * DKMCHECK                                                         *
********************************************************************************
* Prozedure is used to check a project that has been imported by using the     *
* DKM-Import tool.                                                             *
*                                                                              *
* PARAMETERS: DIR      -> WinGIS directory                                     *
*             LNGCODE  -> Current used language code
*             PROJECT  -> Pointer to document that has to be checked           *
*             AXHANDLE -> Pointer to DX                                        *
*                                                                              *
********************************************************************************}
procedure CHECKDKM(DIR:PCHAR;LNGCODE:PCHAR;var PROJECT:VARIANT; var AXHANDLE:VARIANT);stdcall;
var StrfileObj:ClassStrfile;
    aDocument :IDispatch;
begin
   StrFileObj:=ClassStrfile.Create;
   StrFileObj.CreateList(strpas(DIR), strpas(LNGCODE), 'DKMCHECK');
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.App:=AXHANDLE;
   MainWnd.LngCode:=strpas(LNGCODE);
   aDocument:=PROJECT;
   MainWnd.Document:=aDocument as IDocument;
   MainWnd.Showmodal;
   MainWnd.Free;
   StrFileObj.Destroy;
end;

exports CHECKDKM;

begin
end.
