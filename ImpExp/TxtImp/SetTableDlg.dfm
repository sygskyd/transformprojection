object SetTableWnd: TSetTableWnd
  Left = 309
  Top = 253
  BorderStyle = bsDialog
  Caption = 'Select table to import'
  ClientHeight = 174
  ClientWidth = 460
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 2
    Top = 2
    Width = 450
    Height = 140
    BevelOuter = bvLowered
    TabOrder = 0
    object TableLb: TListBox
      Left = 2
      Top = 2
      Width = 446
      Height = 136
      ItemHeight = 13
      TabOrder = 0
    end
  end
  object NextBtn: TButton
    Left = 376
    Top = 146
    Width = 75
    Height = 20
    Caption = 'continue >>'
    TabOrder = 1
    OnClick = NextBtnClick
  end
end
