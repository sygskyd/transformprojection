unit SetTxtFileDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, FileCtrl, ComCtrls, StrFile, Menus, TextFileDef,
  ShellCtrls, FileSelectionPanel, WinOSInfo;

type
  TSetTxtFileWnd = class(TForm)
    PageControl: TPageControl;
    TabSheetFiles: TTabSheet;
    OKBtn: TButton;
    CancelBtn: TButton;
    SetupWindowTimer: TTimer;
    Panel1: TPanel;
    DestDbLabel: TLabel;
    DbBtn: TButton;
    OpenDialogTextDef: TOpenDialog;
    Panel2: TPanel;
    LoadTdfBtn: TButton;
    WizardBtn: TButton;
    TdfLabel: TLabel;
    TdfStatusLabel: TLabel;
    FileSelectionPanel: TFileSelectionPanel;
    ShellTreeView: TShellTreeView;
    FileListBox: TFileListBox;
    ListBox: TListBox;
    TrashPanel: TPanel;
    TrashImage: TImage;
    AddAllBtn: TButton;
    DeleteAllBtn: TButton;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure GeoBtnClick(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure DbBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WizardBtnClick(Sender: TObject);
    procedure LoadTdfBtnClick(Sender: TObject);
    procedure FileSelectionPanelFilesToConvertChanged(Sender: TObject);
    procedure FileListBoxChange(Sender: TObject);
    procedure ListBoxClick(Sender: TObject);
    procedure FileListBoxClick(Sender: TObject);
    procedure FileListBoxDblClick(Sender: TObject);
    procedure FormDockOver(Sender: TObject; Source: TDragDockObject; X,
      Y: Integer; State: TDragState; var Accept: Boolean);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private-Deklarationen}
    CurrentFileIdx :integer;
    StrFileObj :ClassStrfile;
    procedure CheckWindow;
  public
    { Public-Deklarationen}
    winheight:integer;
    winwidth :integer;
    wintop   :integer;
    winleft  :integer;
    OkBtnMode:boolean;
    procedure Reset;
    procedure SetNext;
    function  GetFilename(var aFilename:string):boolean;
    function  GetNumItems:integer;
    procedure CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
  end;

var
  SetTxtFileWnd: TSetTxtFileWnd;

implementation

uses MainDlg, TextWizardDlg, inifiles;

{$R *.DFM}

procedure TSetTxtFileWnd.Reset;
begin
   CurrentFileIdx:=0;
end;

function TSetTxtFileWnd.GetNumItems:integer;
begin
   Result:=FileSelectionPanel.NumFilesToConvert;
end;

procedure TSetTxtFileWnd.SetNext;
begin
   CurrentFileIdx:=CurrentFileIdx+1;
end;

function TSetTxtFileWnd.GetFilename(var aFilename:string):boolean;
begin
   if CurrentFileIdx < FileSelectionPanel.NumFilesToConvert then
   begin
      aFilename:=FileSelectionPanel.GetFilename(CurrentfileIdx);
      Result:=TRUE;
   end
   else
      Result:=FALSE;
end;

procedure TSetTxtFileWnd.FormCreate(Sender: TObject);
var myScale:double;
    dummy  :double;
    dummystr:string;
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   StrFileObj:=ClassStrFile.Create;

   Self.Height:=1;
   Self.Width:=1;

   myScale:=100000 / Trunc(9600000/(Self.PixelsPerInch));

   // now adapt winheigt and winwidth
   dummy:=376*myScale; dummystr:=floattostr(dummy);
   if (Pos('.',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos('.',dummystr)-1);
   if (Pos(',',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos(',',dummystr)-1);
   winheight:=strtoint(dummystr);
   dummy:=462*myScale; dummystr:=floattostr(dummy);
   if (Pos('.',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos('.',dummystr)-1);
   if (Pos(',',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos(',',dummystr)-1);
   winwidth:=strtoint(dummystr);
end;


procedure TSetTxtFileWnd.OKBtnClick(Sender: TObject);
begin
   // now the import window can be opened and the import executed
   Self.Close;
   MainWnd.Show;
end;

procedure TSetTxtFileWnd.CancelBtnClick(Sender: TObject);
begin
   MainWnd.CloseTimer.Enabled:=true;
end;

procedure TSetTxtFileWnd.CheckWindow;
var allok :boolean;

begin
    allok := TRUE;
    WizardBtn.Enabled := allok and (SetTxtFileWnd.ListBox.SelCount>0);

   // ADF-File definition is not allowed for import
   if (MainWnd.CurTextFileDef.GetDelimeterType = delim_None) or (MainWnd.CurTextFileDef.GetObjectType = 99) or (MainWnd.CurTextFileDef.GetObjectType = 98) then allok:=false;

   OkBtn.Enabled:= allok and (GetNumItems <> 0) and (SetTxtFileWnd.ListBox.Count>0);

   end;

procedure TSetTxtFileWnd.GeoBtnClick(Sender: TObject);
begin
   MainWnd.AxGisProjection.SetupByDialog;
end;

procedure TSetTxtFileWnd.CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
begin
   wintop:=(WHEIGHT div 2) - (winheight div 2);
   winleft:=(WWIDTH div 2) - (winwidth div 2);
end;

procedure TSetTxtFileWnd.SetupWindowTimerTimer(Sender: TObject);
begin
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,winleft,wintop,winwidth, winheight,SWP_SHOWWINDOW);
   Self.Height:=winheight;
   Self.Width:=winwidth;
   Self.Repaint;
   FileSelectionPanel.Directory:=MainWnd.LastUsedPath;
   SetupWindowTimer.Enabled:=false;
end;

procedure TSetTxtFileWnd.DbBtnClick(Sender: TObject);
var test, mode : integer;
    inifile:TIniFile;
begin

   MainWnd.AXImpExpDbc.SetupByModalDialog;
   // MainWnd.AXImpExpDbc.SetupByDialog(Self.Handle, 5, 5);
   // PageControl.Enabled:=FALSE;
   // DbBtn.Enabled:=FALSE;
   // OkBtnMode:=OkBtn.Enabled;
   // OkBtn.Enabled:=FALSE;
   // CancelBtn.Enabled:=FALSE;
    if MainWnd.AXImpExpDbc.ImpDbMode > -1 then DestDBLabel.Caption := MainWnd.AXImpExpDbc.ImpDbInfo;
   // test := MainWnd.AXImpExpDbc.ImpDbMode;
   // db with single table = 1;
   // db = 2;
   // Access with single table = 3;
   // access = 4;
   // idb = 5;
   // textfile = 8;

   {
   if (MainWnd.AXImpExpDbc.ImpDbMode > 0) then SetTxtFileWnd.DestDbLabel.Caption := MainWnd.AXImpExpDbc.ImpDbInfo;


     if FileExists(OpenDialogTextDef.Filename)  and (Pos('*',OpenDialogTextDef.Filename)=0)
     then
   	begin
        inifile := TIniFile.Create(OpenDialogTextDef.Filename);

     if MainWnd.CurTextFileDef.HasDatabaseColumns and (MainWnd.AXImpExpDbc.ImpDbMode < 6 ) then
   	begin
   	inifile.WriteInteger('PRESETDATABASE','IMPDBMode',MainWnd.AXImpExpDbc.ImpDbMode);
   	inifile.WriteString('PRESETDATABASE','IMPTable',MainWnd.AXImpExpDbc.ImpTable);
   	inifile.WriteString('PRESETDATABASE','IMPDatabase',MainWnd.AXImpExpDbc.ImpDatabase);
   	end;
    inifile.Free;
    end;
    }
end;

procedure TSetTxtFileWnd.FormShow(Sender: TObject);
begin
   // setup the screen-settings
   StrFileObj.CreateList(OSInfo.WingisDir,MainWnd.LngCode,'TXTIMP');
   Self.Caption:=StrFileObj.Assign(1);                  // PROGIS Text-File Import 2000
   TabSheetFiles.Caption:=StrFileObj.Assign(2);         // Files
   DbBtn.Caption:=StrFileObj.Assign(5);                 // Database
   OKBtn.Caption:=StrFileObj.Assign(6);                 // Ok
   CancelBtn.Caption:=StrFileObj.Assign(7);             // Cancel
   TdfLabel.Caption:=StrFileObj.Assign(8);              // Text definition:
   TdfStatusLabel.Caption:=StrfileObj.Assign(9);        // not set
   WizardBtn.Caption:=StrFileObj.Assign(34);            // Wizard
   WizardBtn.Enabled:=FALSE;
   // setup the projection settings
   MainWnd.SetProjectionSettings;
   // setup the window
   SetupWindowTimer.Enabled:=true;
end;

procedure TSetTxtFileWnd.FormDestroy(Sender: TObject);
begin
   StrFileObj.Free; 
end;

procedure TSetTxtFileWnd.WizardBtnClick(Sender: TObject);
var afilename:string;
    mr:integer;
    i:integer;
    isselected:boolean;
begin
   TextWizardWnd:=TTextWizardWnd.Create(nil);
   windows.setparent (Self.Handle, SetTxtFileWnd.Handle);
   TextWizardWnd.Top :=  SetTxtFileWnd.Top - (TextWizardWnd.Height - SetTxtFileWnd.Height) div 2 ;
   TextWizardWnd.Left :=  SetTxtFileWnd.Left - (TextWizardWnd.Width - SetTxtFileWnd.Width) div 2;

   TextWizardWnd.WorkingDir:=OSInfo.WingisDir;
   TextWizardWnd.LngCode:=MainWnd.LngCode;
   // check if an item is selected
   isselected:=FALSE;
   for i:=0 to Listbox.Items.Count-1 do
   begin
      if Listbox.Selected[i] then
      begin
         isselected:=TRUE;
         break;
      end;
   end;
   // setup the filename for the wizard
   if isselected then
   begin
     aFilename:=FileSelectionPanel.GetFilename(Listbox.ItemIndex);
     TextWizardWnd.CurrTextFileDef:=MainWnd.CurTextFileDef;
     TextWizardWnd.SetFilename(aFilename);
     mr := TextWizardWnd.ShowModal;
     // mr:=TextWizardWnd.GetModalResult; STR ++ --
   end
   else
   begin
      mr:=mrCancel;
      WizardBtn.Enabled:=FALSE;
   end;



   // TextWizardWnd.Free;
   // TextWizardWnd:=nil; STR ++ --

   if mr = mrOk then
   begin
      if (MainWnd.CurTextFileDef.GetDelimeterType <> delim_None) then
         TdfStatuslabel.Caption:=StrFileObj.Assign(74) // set with wizard
      else
         TdfStatuslabel.Caption:=StrFileObj.Assign(9); // none
         // STR ++
         //DbBtn.Enabled:= MainWnd.CurTextFileDef.HasDatabaseColumns;
         // STR --
   end;
   CheckWindow;
   Self.BringToFront;

end;

procedure TSetTxtFileWnd.LoadTdfBtnClick(Sender: TObject);
var CoordSys:integer;
    ProjSet:string;
    Proj:string;
    ProjDate:string;
    XO,YO,Sc:double;
    inifile : TInifile;
    mode : integer;
begin
   OpenDialogTextDef.InitialDir := MainWnd.LastUsedPath;           
   if  not OpenDialogTextDef.Execute then exit;

   if  not MainWnd.CurTextFileDef.LoadFromFile(OpenDialogTextDef.Filename, OSInfo.LocalAppDataDir+'TxtImp.ini') then
         begin
         ShowMessage(StrFileObj.Assign(36)); // Invalid definition file
         exit;
         end;

    // ShowMessage(StrFileObj.Assign(35)) // Defintion file loaded

   if (MainWnd.CurTextFileDef.GetDelimeterType <> delim_None) then
         begin
         TdfStatuslabel.Caption:=ExtractFilename(OpenDialogTextDef.Filename);
         end
   else
         TdfStatuslabel.Caption:=StrFileObj.Assign(9); // none

   // fetch the Sourceprojection from text-file definition
      MainWnd.CurTextFileDef.GetProjection(CoordSys, ProjSet, Proj, ProjDate, XO, YO, Sc);
      MainWnd.AxGisProjection.SourceCoordinateSystem:=CoordSys;
      MainWnd.AxGisProjection.SourceProjSettings:=ProjSet;
      MainWnd.AxGisProjection.SourceProjection:=Proj;
      MainWnd.AxGisProjection.SourceDate:=ProjDate;
      MainWnd.AxGisProjection.SourceXOffset:=XO;
      MainWnd.AxGisProjection.SourceYOffset:=YO;
      MainWnd.AxGisProjection.SourceScale:=Sc;
   // STR ++
   // DbBtn.Enabled:= MainWnd.CurTextFileDef.HasDatabaseColumns;
   // STR --
   if MainWnd.CurTextFileDef.HasDatabaseColumns then
     begin
     if FileExists(OpenDialogTextDef.Filename) then
   	begin
        inifile := TIniFile.Create(OpenDialogTextDef.Filename);
        mode := inifile.ReadInteger('PRESETDATABASE','IMPDBMode',-1);

        if mode > -1 then
                begin
        	MainWnd.AXImpExpDbc.ImpDbMode := mode;
                MainWnd.AXImpExpDbc.ImpTable :=  inifile.ReadString('PRESETDATABASE','IMPTable','');
                MainWnd.AXImpExpDbc.ImpDatabase := inifile.ReadString('PRESETDATABASE','IMPDatabase','');
                SetTxtfileWnd.DestDbLabel.Caption := MainWnd.AXImpExpDbc.ImpDBInfo;
                end;
        inifile.Free;
        end;
      end;
    CheckWindow
end;

procedure TSetTxtFileWnd.FileSelectionPanelFilesToConvertChanged(
  Sender: TObject);
begin
   CheckWindow;
end;

procedure TSetTxtFileWnd.FileListBoxChange(Sender: TObject);
begin
//MainWnd.LastUsedPath :=  ShellTreeView.Path;
end;

procedure TSetTxtFileWnd.ListBoxClick(Sender: TObject);
begin
// STR ++
// DbBtn.Enabled:= MainWnd.CurTextFileDef.HasDatabaseColumns;
// STR --
CheckWindow;
end;

procedure TSetTxtFileWnd.FileListBoxClick(Sender: TObject);
begin
// STR ++
//DbBtn.Enabled:= MainWnd.CurTextFileDef.HasDatabaseColumns;
// STR --
CheckWindow;
end;

procedure TSetTxtFileWnd.FileListBoxDblClick(Sender: TObject);
begin
// STR ++
// DbBtn.Enabled:= MainWnd.CurTextFileDef.HasDatabaseColumns;
// STR --
CheckWindow;
end;

procedure TSetTxtFileWnd.FormDockOver(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer; State: TDragState;
  var Accept: Boolean);
begin
// STR ++
// DbBtn.Enabled:= MainWnd.CurTextFileDef.HasDatabaseColumns;
// STR --
CheckWindow;
end;

procedure TSetTxtFileWnd.FormActivate(Sender: TObject);
begin
SetTxtFileWnd.DestDbLabel.Caption:= MainWnd.AxImpExpDbc.ImpDbInfo;
end;

procedure TSetTxtFileWnd.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
MainWnd.LastUsedPath :=  ShellTreeView.Path;
MainWnd.Writeini;
end;

end.
