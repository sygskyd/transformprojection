object MainWnd: TMainWnd
  Left = 355
  Top = 185
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu, biMinimize, biMaximize, biHelp]
  BorderStyle = bsDialog
  Caption = 'WinGIS  Text-file import 2000'
  ClientHeight = 352
  ClientWidth = 458
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 441
    Height = 305
    Caption = 'Panel1'
    TabOrder = 0
    object Progresslabel: TLabel
      Left = 8
      Top = 8
      Width = 88
      Height = 13
      Caption = 'Progressmessages'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object ProgressBar: TGauge
      Left = 8
      Top = 256
      Width = 425
      Height = 20
      Progress = 0
    end
    object StatusLB: TListBox
      Left = 8
      Top = 24
      Width = 425
      Height = 193
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
    end
    object SaveReportBtn: TButton
      Left = 152
      Top = 224
      Width = 145
      Height = 25
      Caption = 'Save report'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = SaveReportBtnClick
    end
  end
  object OKBtn: TButton
    Left = 248
    Top = 320
    Width = 93
    Height = 23
    Caption = 'OK'
    Default = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 348
    Top = 320
    Width = 101
    Height = 23
    Cancel = True
    Caption = 'Abbrechen'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 2
    OnClick = CancelBtnClick
  end
  object AXImpExpDbc: TAXImpExpDbc
    Left = 8
    Top = 320
    Width = 50
    Height = 25
    ParentFont = False
    TabOrder = 3
    OnDialogSetuped = AXImpExpDbcDialogSetuped
    OnRequestImpLayerIndex = AXImpExpDbcRequestImpLayerIndex
    OnRequestImpProjectName = AXImpExpDbcRequestImpProjectName
    OnRequestImpProjectGUIDs = AXImpExpDbcRequestImpProjectGUIDs
    OnWarning = AXImpExpDbcWarning
    ControlData = {
      54504630065450616E656C00044C656674020803546F70034001055769647468
      02320648656967687402190743617074696F6E06064769734462630000}
  end
  object AxGisProjection: TAxGisProjection
    Left = 64
    Top = 320
    Width = 50
    Height = 25
    ParentFont = False
    TabOrder = 4
    ControlData = {
      54504630065450616E656C00044C656674024003546F70034001055769647468
      02320648656967687402190743617074696F6E060647697350726F0000}
  end
  object AXGisObjMan: TAXGisObjMan
    Left = 120
    Top = 320
    Width = 50
    Height = 25
    TabOrder = 5
    ControlData = {
      54504630065450616E656C00044C656674027803546F70034001055769647468
      02320648656967687402190743617074696F6E06064769734D616E0000}
  end
  object SetupwindowTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = SetupwindowTimerTimer
    Left = 176
    Top = 320
  end
  object CloseTimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = CloseTimerTimer
    Left = 208
    Top = 320
  end
  object SaveDialogRep: TSaveDialog
    DefaultExt = '*.rep'
    FileName = '*.rep'
    Filter = 'Import report file|*.rep'
    Left = 128
    Top = 232
  end
  object ADOConnection: TADOConnection
    LoginPrompt = False
    Mode = cmReadWrite
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 200
    Top = 296
  end
  object ADOTable: TADOTable
    Connection = ADOConnection
    Left = 248
    Top = 296
  end
end
