// ------------------------------------------------------------------------
// NAME: TFileSelectionPanel
// ------------------------------------------------------------------------
// DESCRIPTION: Control is panel that contains components for selection of
//              files from a specified type.
//
// AUTHOR     : Mariacher Bruno
// HISTORY    : 15-08-2002 : Creation of Unit
// ------------------------------------------------------------------------

unit FileSelectionPanel;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, ExtCtrls, ShellCtrls, ComCtrls, FileCtrl, StdCtrls,
  Buttons;

type
  TFileSelectionPanel = class(TPanel)
  private
    { Private declarations }
    FilesToConvertList: array of string;
    FNumFilesToConvert : integer;

    // private variables and get/set methods to store properties
    FMask                   : string;          // Mask that should be used string
    FDirectory              : string;          // Set the directory for the File-Selection
    FDirectoryShellTreeView : TShellTreeView;  // DirectorySelection property
    FFileListbox            : TFileListBox;    // FileSelection property
    FConvertListbox         : TListBox;        // FilesToConvert property
    FAddAllBtn              : TBitBtn;         // AddAll Button property
    FRemoveAllBtn           : TBitBtn;         // RemoveAll Button property
    FTrashImage             : TImage;          // Trash image property
    FTrashPanel             : TPanel;          // Trash panel property

    // event variables
    FOnFilesToConvertChanged :TNotifyEvent; // event that will be executed items of Files to Convert have been changed
    FOnDirectoryChanged      :TNotifyEvent; // event that will be raised if the Directory has been changed in FDirectoryShellTreeView

    function  GetDirectoryShellTreeView:TShellTreeView;
    procedure SetDirectoryShellTreeView(value:TShellTreeView);
    function  GetFileListbox:TFileListBox;
    procedure SetFileListbox(value:TFileListBox);
    function  GetConvertListbox:TListBox;
    procedure SetConvertListbox(value:TListBox);
    function  GetMask:string;
    procedure SetMask(value:string);
    function  GetDirectory:string;
    procedure SetDirectory(value:string);
    procedure SetAddAllBtn(value:TBitBtn);
    function  GetAddAllBtn:TBitBtn;
    procedure SetRemoveAllBtn(value:TBitBtn);
    function  GetRemoveAllBtn:TBitBtn;
    procedure SetTrashImage(value: TImage);
    function  GetTrashImage:TImage;
    procedure SetTrashPanel(value:TPanel);
    function  GetTrashPanel:TPanel;

    function FindTreeNode(Parentnode:TTreeNode; var parentdir:string):TTreeNode;
    function FindChildDirInNode(NodeText:string; ChildDir:string):boolean;

    // methods that handle events that  will be raised from DirectorySelection
    procedure OnDirectoryShellTreeViewClick(Sender: TObject);
    procedure OnDirectoryShellTreeViewChange(Sender: TObject; Node: TTreeNode);
    procedure OnFileListboxDblClick(Sender: TObject);
    procedure OnFileListboxEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure OnConvertListboxDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure OnConvertListboxEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure OnConvertListboxDblClick(Sender: TObject);
    procedure OnConvertListboxMouseMove(Sender: TObject; Shift: TShiftState; X,  Y: Integer);
    procedure OnAddAllBtnClick(Sender: TObject);
    procedure OnRemoveAllBtnClick(Sender: TObject);
    procedure OnTrashImageDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure OnTrashPanelDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
  protected
    { Protected declarations }
    procedure Paint; override;

  public
    { Public declarations }
    constructor Create(owner:TComponent); override;      // constructor
    function    GetFilename(const idx:integer):string;   // method to fetch filename
    procedure   AddFileToConvert(const Filename:string); // method to add filename
    procedure   DisplayFilesToConvert;                   // method to fill list with files that should be converted
  published
    { Published declarations }
    // published properties
    property Mask:string                           read GetMask                   write SetMask;
    property Directory:string                      read GetDirectory              write SetDirectory;
    property DirectoryShellTreeView:TShellTreeView read GetDirectoryShellTreeView write SetDirectoryShellTreeView;
    property FileListbox:TFileListBox              read GetFileListbox            write SetFileListbox;
    property ConvertListbox:TListBox               read GetConvertListbox         write SetConvertListbox;
    property AddAllBtn:TBitBtn                     read GetAddAllBtn              write SetAddAllBtn;
    property RemoveAllBtn:TBitBtn                  read GetRemoveAllBtn           write SetRemoveAllBtn;
    property TrashImage:TImage                     read GetTrashImage             write SetTrashImage;
    property TrashPanel:TPanel                     read GetTrashPanel             write SetTrashPanel;
    property NumFilesToConvert:integer             read FNumFilesToConvert;
    // published events
    property OnFilesToConvertChanged:TNotifyEvent read FOnFilesToConvertChanged write FOnFilesToConvertChanged;
    property OnDirectoryChanged     :TNotifyEvent read FOnDirectoryChanged      write FOnDirectoryChanged;
  end;

procedure Register;

implementation

// ------------------------------------------------------------------------
// NAME: Create
// ------------------------------------------------------------------------
// DESCRIPTION : Constructor of TFileSelectionPanel component
//
// PARAMETERS  : aOwner -> parent component
//
// RETURNVALUE : NONE
// ------------------------------------------------------------------------
constructor TFileSelectionPanel.Create(owner:TComponent);
begin
   inherited Create(owner);            // execute constructor of parent
   // create the parent Panel
   // setup the default size
   Self.Height:=150; // 150 is default height
   Self.Width:=400;  // 400 is default width

   // setup properties
   FMask:='*.*';
   FDirectoryShellTreeView:=nil;
   FFileListbox:=nil;
   FConvertListbox:=nil;
   FAddAllBtn:=nil;
   FRemoveAllBtn:=nil;
   FTrashImage:=nil;

   FNumFilesToConvert:=0;
end;

// ------------------------------------------------------------------------
// NAME: Paint
// ------------------------------------------------------------------------
// DESCRIPTION : Method to overwrite Paint method of panel
//
// PARAMETERS  : NONE
//
// RETURNVALUE : NONE
// ------------------------------------------------------------------------
procedure TFileSelectionPanel.Paint;
begin
   inherited Paint;
   if (FTrashPanel <> nil) and (FTrashImage <> nil) then
      FTrashImage.Visible:=FALSE;
end;

function TFileSelectionPanel.GetFilename(const idx:integer):string;
begin
   result:='';
   if (idx < FNumFilesToConvert) and (idx >= 0) then
      result:=FilesToConvertList[idx];
end;

// ------------------------------------------------------------------------
// NAME: AddFileToConvert
// ------------------------------------------------------------------------
// DESCRIPTION : Method to add file that should be converted
//
// PARAMETERS  : Filename -> Name of the file that should be added
//
// RETURNVALUE : NONE
// ------------------------------------------------------------------------
procedure TFileSelectionPanel.AddFileToConvert(const Filename:string);
var aFilename:string;
    exists   :boolean;
    i        :integer;
begin
   // first check if the filename does not already exist
   exists:=FALSE;
   aFilename:=Filename;
   // aFilename:=UpperCase(aFilename);
   if FNumFilesToConvert > 0 then
   begin
      for i:=0 to FNumFilesToConvert-1 do
      begin
         if FilesToConvertList[i] = aFilename then
         begin
            exists:=TRUE;
            break;
         end;
      end;
   end;
   if not exists then
   begin
      // add new file to list
      FNumFilesToConvert:=FNumFilesToConvert+1;
      SetLength(FilesToConvertList,FNumFilesToConvert);
      FilesToConvertList[FNumFilesToConvert-1]:=aFilename;
   end;
end;

// ------------------------------------------------------------------------
// NAME: DisplayFilesToConvert
// ------------------------------------------------------------------------
// DESCRIPTION : Method to display files that should be converted
//
// PARAMETERS  : NONE
//
// RETURNVALUE : NONE
// ------------------------------------------------------------------------
procedure TFileSelectionPanel.DisplayFilesToConvert;
var i:integer;
    aFilename:string;
begin
   if FConvertListbox <> nil then
   begin
      FConvertListbox.Items.Clear;
      if FNumFilesToConvert < 1 then
         exit;
      for i:=0 to FNumFilesToConvert-1 do
      begin
         aFilename:=FilesToConvertList[i];
         aFilename:=ExtractFilename(aFilename);
         FConvertListbox.Items.Add(aFilename);
      end;
   end;
end;

procedure TFileSelectionPanel.SetTrashImage(value: TImage);
begin
   FTrashImage:=value;
   if value <> nil then
   begin
      FTrashImage.DragMode:=dmAutomatic;
      FTrashImage.OnDragOver:=OnTrashImageDragOver;
   end;
end;

function TFileSelectionPanel.GetTrashImage:TImage;
begin
   result:=FTrashImage;
end;

procedure TFileSelectionPanel.SetTrashPanel(value:TPanel);
begin
   FTrashPanel:=value;
   if value <> nil then
   begin
      FTrashPanel.DragMode:=dmAutomatic;
      FTrashPanel.OnDragOver:=OnTrashPanelDragOver;
   end;
end;

function TFileSelectionPanel.GetTrashPanel:TPanel;
begin
   result:=FTrashPanel;
end;

procedure TFileSelectionPanel.SetRemoveAllBtn(value:TBitBtn);
begin
   FRemoveAllBtn:=value;
   if FRemoveAllBtn <> nil then
   begin
      FRemoveAllBtn.OnClick:=OnRemoveAllBtnClick;
   end;
end;

function TFileSelectionPanel.GetRemoveAllBtn:TBitBtn;
begin
   result:=FRemoveAllBtn;
end;

procedure TFileSelectionPanel.SetAddAllBtn(value:TBitBtn);
begin
   FAddAllBtn:=value;
   if FAddAllBtn <> nil then
   begin
      FAddAllBtn.OnClick:=OnAddAllBtnClick;
   end;
end;


function  TFileSelectionPanel.GetAddAllBtn:TBitBtn;
begin
   result:=FAddAllBtn;
end;

function TFileSelectionPanel.GetDirectoryShellTreeView:TShellTreeView;
begin
   result:=FDirectoryShellTreeView;
end;

procedure TFileSelectionPanel.SetDirectoryShellTreeView(value:TShellTreeView);
begin
   if value <> nil then
   begin
      FDirectoryShellTreeView:=value;
      FDirectoryShellTreeView.OnClick:=OnDirectoryShellTreeViewClick;
      FDirectoryShellTreeView.OnChange:=OnDirectoryShellTreeViewChange;
   end;
end;

function  TFileSelectionPanel.GetFileListbox:TFileListBox;
begin
   result:=FFileListbox;
end;

procedure TFileSelectionPanel.SetFileListbox(value:TFileListBox);
begin
   FFileListbox:=value;
   if value <> nil then
   begin
      FFileListbox.DragMode:=dmAutomatic;
      FFileListbox.Mask:=FMask;
      FFileListbox.MultiSelect:=TRUE;
      FFileListbox.OnDblClick:=OnFileListboxDblClick;
      FFileListbox.OnEndDrag:=OnFileListboxEndDrag;
      FFIleListBox.ShowHint:=True;  
   end;
end;

function TFileSelectionPanel.GetConvertListbox:TListBox;
begin
   result:=FConvertListbox;
end;

procedure TFileSelectionPanel.SetConvertListbox(value:TListBox);
begin
   FConvertListbox:=value;
   if value <> nil then
   begin
      FConvertListbox.DragMode:=dmAutomatic;
      FConvertListbox.OnDragOver:=OnConvertListboxDragOver;
      FConvertListbox.OnEndDrag:=OnConvertListboxEndDrag;
      FConvertListbox.OnDblClick:=OnConvertListboxDblClick;
      FConvertListbox.OnMouseMove:=OnConvertListboxMouseMove;
      FConvertListbox.MultiSelect:=TRUE;
   end;
end;

function TFileSelectionPanel.GetMask:string;
begin
   result:=FMask;
end;

procedure TFileSelectionPanel.SetMask(value:string);
begin
   FMask:=value;
   if FFileListbox <> nil then
      FFileListbox.Mask:=FMask;
end;

function TFileSelectionPanel.GetDirectory:string;
begin
   result:=FDirectory;
end;

// method is used to find the next subdirectory of a startdirectory
// searching starts with parentnode
// return value is the next child-treenode
function TFileSelectionPanel.FindTreeNode(Parentnode:TTreeNode; var parentdir:string):TTreeNode;
var childdir:string;
    childnode:TTreeNode;
    i:integer;
    iterate:TTreeNode;
begin
   // first find the part of the directory - seperator is a \ sign
   childnode:=nil;
   parentnode.Expand(FALSE);
   // extract the first part of the path to find for what it should be searched for
   // find the string till the first \ sign
   if Pos('\',parentdir) = 0 then
   begin
      // it is the last part of the path -> no further recursions have to be done
      childdir:=parentdir;
      parentdir:='';
   end
   else
   begin
      childdir:='';
      while childdir = '' do
      begin
         // find next \ character
         childdir:=copy(parentdir,1,Pos('\',parentdir)-1);
         delete(parentdir,1,Pos('\',parentdir));
      end;
   end;

   iterate:=parentnode.getFirstChild;
   while (iterate <> nil) and (childnode = nil) do
   begin
      if FindChildDirInNode(iterate.Text, childdir) then
      begin
         if parentdir <> '' then
            childnode:=FindTreeNode(iterate, parentdir)
         else
            childnode:=iterate;
      end
      else
         iterate:=parentnode.GetNextChild(iterate);
   end;
   result:=childnode;
end;

function TFileSelectionPanel.FindChildDirInNode(NodeText:string; ChildDir:string):boolean;
var retVal:boolean;
    CmpNodeText,CmpChildDir:string;
    i:integer;
begin
   retVal:=FALSE;
   for i:=1 to length(NodeText) do
   begin
      if NodeText[i] <> #0 then
         CmpNodeText:=CmpNodeText+NodeText[i]
      else
         break;
   end;
   CmpChildDir:=UpperCase(ChildDir);
   CmpNodeText:=UpperCase(CmpNodeText); // compare case independend

   if (pos('(',CmpNodeText) <> 0) and (pos(':',CmpChildDir) <> 0) then
   begin
      // search in high node where drives are set
      if pos(CmpChildDir,CmpNodeText) <> 0 then
         retVal:=TRUE;
   end
   else
   begin
      // if its not the drive part that should be searched for
      // the names must be equal
      if CmpNodeText = CmpChildDir then
         retVal:=TRUE;
   end;
   result:=retVal;
end;

procedure TFileSelectionPanel.SetDirectory(value:string);
var childnode:TTreeNode;
    childdir:string;
    iterate:TTreeNode;
    parentnode:TTreeNode;
begin
   // set a directory for the FDirectoryShellTreeView
   if not DirectoryExists(value) then // if the directory does not exist -> do nothing
      exit;
   if FDirectoryShellTreeView = nil then // if the FDirectoryShellTreeView does not exist -> do nothing
      exit;

   if (FDirectory = value) then // it�s already the same
      exit;

   // Search for the directory that is set
   FDirectoryShellTreeView.Visible:=FALSE;                  // to avoid flickering
   if FFileListbox <> nil then FFileListbox.Visible:=FALSE;
   childnode:=nil;
   parentnode:=FDirectoryShellTreeView.Items[0];
   iterate:=parentnode.getFirstChild;
   while (iterate <> nil) and (childnode = nil) do
   begin
      childdir:=value;
      childnode:=FindTreeNode(iterate,childdir);
      if childnode <> nil then
      begin
         childnode.Selected:=TRUE;
         childnode.Focused:=TRUE;
         FDirectoryShellTreeView.Selected:=childnode;
      end;
      iterate:=parentnode.GetNextChild(iterate);
   end;
   FDirectoryShellTreeView.Visible:=TRUE; // to avoid flickering
   if FFileListbox <> nil then FFileListbox.Visible:=TRUE;
end;

procedure TFileSelectionPanel.OnConvertListboxMouseMove(Sender: TObject; Shift: TShiftState; X,  Y: Integer);
var aPos:TPoint;
    idx:integer;
begin
   aPos.X:=x;
   aPos.Y:=y;
   FConvertListbox.ShowHint:=TRUE;
   idx:=FConvertListbox.ItemAtPos(aPos, TRUE);
   if (idx >= 0) and (idx < FConvertListbox.Items.count) then
   begin
      FConvertListbox.Hint:=FilesToConvertList[idx];
   end
   else
      FConvertListbox.Hint:='';
end;

procedure TFileSelectionPanel.OnConvertListboxDblClick(Sender: TObject);
var i:integer;
    numitems:integer;
    storeitems:array of string;
begin
   // first copy the files that should be converted to a dummy list
   SetLength(storeitems, FNumFilesToConvert); numitems:=FNumFilesToConvert;
   for i:=0 to FNumFilesToConvert-1 do
   begin
      storeitems[i]:=FilesToConvertList[i];
   end;
   // now empty the FilesToConvertList
   FNumFilesToConvert:=0;
   SetLength(FilesToConvertList,0);
   for i:=0 to FConvertListbox.Items.Count-1 do
   begin
      // all items that are not selected can be added again to the FilesToConvertList
      if not FConvertListbox.Selected[i] then
      begin
         FNumFilesToConvert:=FNumFilesToConvert+1;
         SetLength(FilesToConvertList,FNumFilesToConvert);
         FilesToConvertList[FNumFilesToConvert-1]:=storeitems[i];
      end;
   end;
   SetLength(storeitems,0); // remove the store items
   DisplayFilesToConvert;
   if Assigned(OnFilesToConvertChanged) then OnFilesToConvertChanged(Self);
end;

procedure TFileSelectionPanel.OnConvertListboxEndDrag(Sender, Target: TObject; X, Y: Integer);
var i:integer;
    numitems:integer;
    storeitems:array of string;
begin
   if (FTrashPanel <> nil) and (FTrashImage <> nil) then
      FTrashImage.Visible:=FALSE;
   if Target = nil then
      exit;
   if (Target = FTrashImage) or (Target = FTrashPanel) then
   begin
      // first copy the files that should be converted to a dummy list
      SetLength(storeitems, FNumFilesToConvert); numitems:=FNumFilesToConvert;
      for i:=0 to FNumFilesToConvert-1 do
      begin
         storeitems[i]:=FilesToConvertList[i];
      end;
      // now empty the FilesToConvertList
      FNumFilesToConvert:=0;
      SetLength(FilesToConvertList,0);
      for i:=0 to FConvertListbox.Items.Count-1 do
      begin
         // all items that are not selected can be added again to the FilesToConvertList
         if not FConvertListbox.Selected[i] then
         begin
            FNumFilesToConvert:=FNumFilesToConvert+1;
            SetLength(FilesToConvertList,FNumFilesToConvert);
            FilesToConvertList[FNumFilesToConvert-1]:=storeitems[i];
         end;
      end;
      SetLength(storeitems,0); // remove the store items
      DisplayFilesToConvert;
      if Assigned(OnFilesToConvertChanged) then OnFilesToConvertChanged(Self);
   end;
end;

procedure TFileSelectionPanel.OnTrashImageDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
   Accept:=FALSE;
   if Source = FConvertListbox then
      Accept:=TRUE;
end;

procedure TFileSelectionPanel.OnTrashPanelDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
   Accept:=FALSE;
   if Source = FConvertListbox then
   begin
      Accept:=TRUE;
      if (FTrashPanel <> nil) and (FTrashImage <> nil) then
         FTrashImage.Visible:=TRUE;
   end;
end;

procedure TFileSelectionPanel.OnRemoveAllBtnClick(Sender: TObject);
begin
   FNumFilesToConvert:=0;
   SetLength(FilesToConvertList,0);
   DisplayFilesToConvert;
   if Assigned(OnFilesToConvertChanged) then OnFilesToConvertChanged(Self);
end;

procedure TFileSelectionPanel.OnAddAllBtnClick(Sender: TObject);
var i:integer;
    AFilename:string;
begin
   for i:=0 to FFileListbox.Items.Count-1 do
   begin
      aFilename:=FFileListbox.Directory;
      // check if there is a \ at last item in the path
      if aFilename[length(aFilename)] <> '\' then aFilename:=aFilename+'\';
      aFilename:=aFilename+FFileListbox.Items[i];
      AddFileToConvert(aFilename);
   end;
   DisplayFilesToConvert;
   if Assigned(OnFilesToConvertChanged) then OnFilesToConvertChanged(Self);
end;

procedure TFileSelectionPanel.OnConvertListboxDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
   Accept:=FALSE;
   if Source = FFileListbox then
      Accept:=TRUE;
end;

procedure TFileSelectionPanel.OnFileListboxEndDrag(Sender, Target: TObject; X, Y: Integer);
var i:integer;
    aFilename:string;
begin
   if Target = FConvertListbox then
   begin
      for i:=0 to FFileListbox.Items.Count-1 do
      begin
         if FFileListbox.Selected[i] then
         begin
            aFilename:=FFileListbox.Directory;
            // check if there is a \ at last item in the path
            if aFilename[length(aFilename)] <> '\' then aFilename:=aFilename+'\';
            aFilename:=aFilename+FFileListbox.Items[i];
            AddFileToConvert(aFilename);
         end;
      end;
      DisplayFilesToConvert;
      if Assigned(OnFilesToConvertChanged) then OnFilesToConvertChanged(Self);
   end;
end;

procedure TFileSelectionPanel.OnFileListboxDblClick(Sender: TObject);
var aFilename:string;
    i:integer;
begin
   for i:=0 to FFileListbox.Items.Count-1 do
   begin
      if FFileListbox.Selected[i] then
      begin
         aFilename:=FFileListbox.Directory;
         // check if there is a \ at last item in the path
         if aFilename[length(aFilename)] <> '\' then aFilename:=aFilename+'\';
         aFilename:=aFilename+FFileListbox.Items[i];
         AddFileToConvert(aFilename);
      end;
   end;
   DisplayFilesToConvert;
   if Assigned(OnFilesToConvertChanged) then OnFilesToConvertChanged(Self);
end;

procedure TFileSelectionPanel.OnDirectoryShellTreeViewClick(Sender: TObject);
begin
   // handle click event on DirectorySelection
   if FFileListbox <> nil then
   begin
      FFileListbox.Directory:=FDirectoryShellTreeView.Path;
      if FFileListbox.Directory <> FDirectoryShellTreeView.Path then
      begin
         FFileListbox.Mask:='*.dummy';
         FDirectory:='';
      end
      else
      begin
         FFileListbox.Mask:=FMask;
         FDirectory:=FDirectoryShellTreeView.Path;
      end;
   end
   else
   begin
      if DirectoryExists(FDirectoryShellTreeView.Path) then
         FDirectory:=FDirectoryShellTreeView.Path
      else
         FDirectory:='';
   end;
   if Assigned(OnDirectoryChanged) then OnDirectoryChanged(self);
end;

procedure TFileSelectionPanel.OnDirectoryShellTreeViewChange(Sender: TObject; Node: TTreeNode);
begin
   // handle click event on DirectorySelection
   if FFileListbox <> nil then
   begin
      FFileListbox.Directory:=FDirectoryShellTreeView.Path;
      if FFileListbox.Directory <> FDirectoryShellTreeView.Path then
      begin
         FFileListbox.Mask:='*.dummy';
         FDirectory:='';
      end
      else
      begin
         FFileListbox.Mask:=FMask;
         FDirectory:=FDirectoryShellTreeView.Path;
      end;
   end
   else
   begin
      if DirectoryExists(FDirectoryShellTreeView.Path) then
         FDirectory:=FDirectoryShellTreeView.Path
      else
         FDirectory:='';
   end;
   if Assigned(OnDirectoryChanged) then OnDirectoryChanged(self);
end;

procedure Register;
begin
  RegisterComponents('Gis-Controls', [TFileSelectionPanel]);
end;

end.
