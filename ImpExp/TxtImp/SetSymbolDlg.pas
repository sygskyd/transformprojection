unit SetSymbolDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TSetSymbolWnd = class(TForm)
    SymbolCb: TComboBox;
    OkBtn: TButton;
    procedure OkBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    WorkingDir:string;
    LngCode:string;
  end;

var
  SetSymbolWnd: TSetSymbolWnd;

implementation

uses StrFile;

{$R *.DFM}

procedure TSetSymbolWnd.OkBtnClick(Sender: TObject);
begin
   ModalResult:=mrOk;
   Self.Close;
end;

procedure TSetSymbolWnd.FormShow(Sender: TObject);
var StrFileObj:ClassStrFile;
begin
   StrFileObj:=ClassStrfile.Create;
   StrfileObj.CreateList(WorkingDir,LngCode,'TXTIMP');
   Self.Caption:=StrFileObj.Assign(54); // set default symbol
   OkBtn.Caption:=StrFileObj.Assign(6); // Ok
   StrFileObj.Free;
end;

end.
