object TextWizardWnd: TTextWizardWnd
  Left = 209
  Top = 171
  BorderStyle = bsDialog
  Caption = 'Text file definition wizard'
  ClientHeight = 439
  ClientWidth = 713
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 713
    Height = 439
    ActivePage = SaveDefSheet
    Align = alClient
    TabIndex = 4
    TabOrder = 0
    object ColumnDefSheet: TTabSheet
      Caption = 'DataFile'
      object Prev2Btn: TButton
        Left = 544
        Top = 384
        Width = 75
        Height = 20
        Caption = '<< previous'
        TabOrder = 0
        OnClick = Prev2BtnClick
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 700
        Height = 376
        BevelOuter = bvLowered
        TabOrder = 1
        object SepDefScrollBox: TScrollBox
          Left = 1
          Top = 1
          Width = 698
          Height = 374
          VertScrollBar.Tracking = True
          VertScrollBar.Visible = False
          Align = alClient
          AutoSize = True
          TabOrder = 0
          DesignSize = (
            694
            370)
          object SepDefGrid: TStringGrid
            Left = 0
            Top = 0
            Width = 694
            Height = 370
            Align = alClient
            ColCount = 10
            DefaultRowHeight = 16
            FixedCols = 0
            RowCount = 20
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goAlwaysShowEditor]
            ParentFont = False
            ScrollBars = ssHorizontal
            TabOrder = 0
            OnDrawCell = SepDefGridDrawCell
            OnMouseMove = SepDefGridMouseMove
          end
          object SepDefCanvas: TWindow
            Left = 0
            Top = 0
            Width = 546
            Height = 25
            Anchors = [akLeft, akTop, akRight]
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Pitch = fpFixed
            Font.Style = []
            OnMouseDown = SepDefCanvasMouseDown
            OnMouseMove = SepDefCanvasMouseMove
            OnMouseUp = SepDefCanvasMouseUp
            OnPaint = SepDefCanvasPaint
            ParentFont = False
          end
        end
      end
      object Next2Btn: TButton
        Left = 624
        Top = 384
        Width = 75
        Height = 20
        Caption = 'next >>'
        TabOrder = 2
        OnClick = Next2BtnClick
      end
      object Cancel2Btn: TButton
        Left = 448
        Top = 384
        Width = 75
        Height = 20
        Caption = 'Cancel'
        TabOrder = 3
        OnClick = Cancel1BtnClick
      end
    end
    object LabelTextSheet: TTabSheet
      Caption = 'Labeltext'
      ImageIndex = 3
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 697
        Height = 376
        BevelOuter = bvLowered
        TabOrder = 0
        object LabelPosLabel: TLabel
          Left = 4
          Top = 4
          Width = 107
          Height = 13
          Caption = 'Define position of label'
        end
        object LabelFontLabel: TLabel
          Left = 264
          Top = 4
          Width = 139
          Height = 13
          Caption = 'Define font and angle of label'
        end
        object Panel7: TPanel
          Left = 4
          Top = 20
          Width = 258
          Height = 169
          TabOrder = 0
          object TextPosPanel: TTextPosPanel
            Left = 0
            Top = 2
            Width = 254
            Height = 165
            BorderStyle = bsSingle
            Distance = 0
            TextAlign = taUpperLeft
            Text = 'Text'
            OnMouseUp = TextPosPanelMouseUp
          end
        end
        object Panel8: TPanel
          Left = 264
          Top = 20
          Width = 430
          Height = 169
          TabOrder = 1
          object CurrentFontLabel1: TLabel
            Left = 8
            Top = 8
            Width = 61
            Height = 13
            Caption = 'Current Font:'
          end
          object CurrentSizeLabel1: TLabel
            Left = 8
            Top = 25
            Width = 60
            Height = 13
            Caption = 'Current Size:'
          end
          object CurrentFontLabel: TLabel
            Left = 104
            Top = 8
            Width = 88
            Height = 16
            Caption = 'Courier New'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
          end
          object CurrentsizeLabel: TLabel
            Left = 104
            Top = 25
            Width = 12
            Height = 13
            Caption = '10'
          end
          object LabelPosAngleLabel: TLabel
            Left = 8
            Top = 45
            Width = 58
            Height = 13
            Caption = 'Label angle:'
          end
          object AngleUpBtn: TSpeedButton
            Left = 132
            Top = 40
            Width = 16
            Height = 9
            Glyph.Data = {
              96000000424D960000000000000076000000280000000A000000040000000100
              04000000000020000000C40E0000C40E00001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777000007700
              0000777700077700000077777077770000007777777777000000}
            OnClick = AngleUpBtnClick
          end
          object LabelOffsetLabel: TLabel
            Left = 8
            Top = 62
            Width = 58
            Height = 13
            Caption = 'Label offset:'
          end
          object AngleDownBtn: TSpeedButton
            Left = 132
            Top = 49
            Width = 16
            Height = 9
            Glyph.Data = {
              96000000424D960000000000000076000000280000000A000000040000000100
              04000000000020000000C40E0000C40E00001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777770777700
              0000777700077700000077700000770000007777777777000000}
            OnClick = AngleDownBtnClick
          end
          object OffsetUpBtn: TSpeedButton
            Left = 132
            Top = 58
            Width = 16
            Height = 9
            Glyph.Data = {
              96000000424D960000000000000076000000280000000A000000040000000100
              04000000000020000000C40E0000C40E00001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777000007700
              0000777700077700000077777077770000007777777777000000}
            OnClick = OffsetUpBtnClick
          end
          object OffsetDownBtn: TSpeedButton
            Left = 132
            Top = 67
            Width = 16
            Height = 9
            Glyph.Data = {
              96000000424D960000000000000076000000280000000A000000040000000100
              04000000000020000000C40E0000C40E00001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777770777700
              0000777700077700000077700000770000007777777777000000}
            OnClick = OffsetDownBtnClick
          end
          object SetFontBtn: TButton
            Left = 276
            Top = 43
            Width = 148
            Height = 33
            Caption = 'Change Font and size'
            TabOrder = 0
            OnClick = SetFontBtnClick
          end
          object LabelPosPanel: TPanel
            Left = 8
            Top = 138
            Width = 417
            Height = 25
            BevelOuter = bvLowered
            TabOrder = 1
            object LabelPosMarkLabel: TLabel
              Left = 8
              Top = 10
              Width = 117
              Height = 13
              Caption = 'Label positioning marker:'
            end
            object LabelPosMarkCb: TComboBox
              Left = 160
              Top = 2
              Width = 225
              Height = 21
              ItemHeight = 13
              TabOrder = 0
              OnChange = LabelPosMarkCbChange
            end
            object SaveLabelPosBtn: TButton
              Left = 388
              Top = 2
              Width = 25
              Height = 21
              Caption = '...'
              TabOrder = 1
              OnClick = SaveLabelPosBtnClick
            end
          end
          object LabelAngleEdit: TEdit
            Left = 104
            Top = 40
            Width = 28
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            Text = '0'
            OnChange = LabelAngleEditChange
          end
          object LabelPosCb: TCheckBox
            Left = 8
            Top = 120
            Width = 305
            Height = 17
            Caption = 'Setup label positioning markers'
            TabOrder = 3
            OnClick = LabelPosCbClick
          end
          object LabelLayerCb: TCheckBox
            Left = 8
            Top = 76
            Width = 361
            Height = 17
            Caption = 'Create labels on seperate layer'
            TabOrder = 4
            OnClick = LabelLayerCbClick
          end
          object LabelLayerPanel: TPanel
            Left = 8
            Top = 94
            Width = 416
            Height = 25
            BevelOuter = bvLowered
            TabOrder = 5
            object LabelLayerListCb: TComboBox
              Left = 2
              Top = 2
              Width = 410
              Height = 21
              ItemHeight = 13
              TabOrder = 0
              Text = 'LabelLayerListCb'
            end
          end
          object LabelOffsetEdit: TEdit
            Left = 104
            Top = 57
            Width = 28
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
            Text = '0'
            OnChange = LabelOffsetEditChange
          end
        end
      end
      object Cancel4Btn: TButton
        Left = 448
        Top = 384
        Width = 75
        Height = 20
        Caption = 'Cancel'
        TabOrder = 1
        OnClick = Cancel4BtnClick
      end
      object Prev4Btn: TButton
        Left = 544
        Top = 384
        Width = 75
        Height = 20
        Caption = '<< previous'
        TabOrder = 2
        OnClick = Prev4BtnClick
      end
      object Next4Btn: TButton
        Left = 624
        Top = 384
        Width = 75
        Height = 20
        Caption = 'next >>'
        TabOrder = 3
        OnClick = Next4BtnClick
      end
    end
    object SeperatorSheet: TTabSheet
      Caption = 'Column Seperator'
      ImageIndex = 4
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 700
        Height = 376
        BevelOuter = bvLowered
        TabOrder = 0
        object UseSeperatorCb: TCheckBox
          Left = 8
          Top = 8
          Width = 177
          Height = 17
          Caption = 'Use column seperator'
          TabOrder = 0
          OnClick = UseSeperatorCbClick
        end
        object Panel2: TPanel
          Left = 8
          Top = 26
          Width = 689
          Height = 121
          TabOrder = 1
          object TabSepCb: TCheckBox
            Left = 8
            Top = 8
            Width = 129
            Height = 17
            Caption = 'Tabulator'
            TabOrder = 0
            OnClick = TabSepCbClick
          end
          object CommaSepCb: TCheckBox
            Left = 8
            Top = 24
            Width = 145
            Height = 17
            Caption = 'Comma'
            TabOrder = 1
            OnClick = CommaSepCbClick
          end
          object SpaceSepCb: TCheckBox
            Left = 8
            Top = 40
            Width = 129
            Height = 17
            Caption = 'Space'
            TabOrder = 2
            OnClick = SpaceSepCbClick
          end
          object UserSepCb: TCheckBox
            Left = 8
            Top = 72
            Width = 97
            Height = 17
            Caption = 'User defined'
            TabOrder = 3
            OnClick = UserSepCbClick
          end
          object UserSepEdit: TEdit
            Left = 24
            Top = 88
            Width = 25
            Height = 21
            TabOrder = 4
            OnChange = UserSepEditChange
          end
          object SemicolonSepCb: TCheckBox
            Left = 8
            Top = 56
            Width = 97
            Height = 17
            Caption = 'Semicolon'
            TabOrder = 5
            OnClick = SemicolonSepCbClick
          end
        end
        object FixedWideCb: TCheckBox
          Left = 8
          Top = 168
          Width = 209
          Height = 17
          Caption = 'Fixed wide columns'
          TabOrder = 2
          OnClick = FixedWideCbClick
        end
      end
      object Prev1Btn: TButton
        Left = 544
        Top = 384
        Width = 75
        Height = 20
        Caption = '<< previous'
        Enabled = False
        TabOrder = 1
      end
      object Next1Btn: TButton
        Left = 624
        Top = 384
        Width = 75
        Height = 20
        Caption = 'next >>'
        TabOrder = 2
        OnClick = Next1BtnClick
      end
      object Cancel1Btn: TButton
        Left = 448
        Top = 384
        Width = 75
        Height = 20
        Caption = 'Cancel'
        TabOrder = 3
        OnClick = Cancel1BtnClick
      end
      object LoadDefBtn: TButton
        Left = 335
        Top = 384
        Width = 106
        Height = 20
        Caption = 'Load Definition'
        TabOrder = 4
        OnClick = LoadDefBtnClick
      end
    end
    object ColNameSheet: TTabSheet
      Caption = 'Column Assignement'
      ImageIndex = 3
      object DbColnameLabel: TLabel
        Left = 8
        Top = 388
        Width = 81
        Height = 13
        Caption = 'DbColnameLabel'
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 700
        Height = 376
        BevelOuter = bvLowered
        TabOrder = 0
        object ColNameScrollBox: TScrollBox
          Left = 4
          Top = 4
          Width = 485
          Height = 368
          TabOrder = 0
          object ColNameGrid: TStringGrid
            Left = 0
            Top = 0
            Width = 481
            Height = 364
            Align = alClient
            DefaultRowHeight = 16
            DragMode = dmAutomatic
            FixedCols = 0
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goEditing, goAlwaysShowEditor]
            ParentFont = False
            ScrollBars = ssHorizontal
            TabOrder = 0
            OnDragDrop = ColNameGridDragDrop
            OnDragOver = ColNameGridDragOver
            OnDrawCell = ColNameGridDrawCell
            OnEndDrag = ColNameGridEndDrag
            ColWidths = (
              64
              64
              64
              64
              64)
          end
        end
        object Panel5: TPanel
          Left = 492
          Top = 4
          Width = 205
          Height = 368
          BevelOuter = bvLowered
          TabOrder = 1
          object ColnameLabel: TLabel
            Left = 8
            Top = 64
            Width = 92
            Height = 13
            Caption = 'SupportedColumns:'
          end
          object SupportedColsLb: TListBox
            Left = 4
            Top = 80
            Width = 197
            Height = 257
            DragMode = dmAutomatic
            ItemHeight = 13
            TabOrder = 0
            OnDragOver = SupportedColsLbDragOver
            OnEndDrag = SupportedColsLbEndDrag
          end
          object ColTypeGroupBox: TGroupBox
            Left = 4
            Top = 4
            Width = 197
            Height = 49
            Caption = 'Object Type'
            TabOrder = 1
            object PointBtn: TSpeedButton
              Left = 8
              Top = 16
              Width = 23
              Height = 22
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000130B0000130B00001000000010000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
                8888888888888888888888888888888888888888888888888888888888888888
                8888888888888888888888888888888888888888888888888888888888888888
                8888888888888888888888888888888888888888888888888888888888880888
                8888888888808888888888888888088888888888888088888888888888000008
                8888888880000088888888888888088888888888888088888888888888880888
                8888888888808888888888888888888888888888888888888888888888888888
                8888888888888888888888888888888888888888888888888888888888888888
                8888888888888888888888888888888888888888888888888888}
              NumGlyphs = 2
              OnClick = PointBtnClick
            end
            object PolyBtn: TSpeedButton
              Left = 32
              Top = 16
              Width = 23
              Height = 22
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000130B0000130B00001000000010000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
                8888888888888888888888888888888888888888888888888888888888888888
                8888888888888888888888888888888888888888888888888888800000000000
                0888880000000000008880888888888808888808888888888088808888888888
                0888880888888888808880888888888808888808888888888088808888888888
                0888880888888888808880888888888808888808888888888088808888888888
                0888880888888888808880888888888088888808888888880888888888888808
                8888888888888880888888888888808888888888888888088888888888880888
                8888888888888088888888888880888888888888888808888888}
              NumGlyphs = 2
              OnClick = PolyBtnClick
            end
            object CPolyBtn: TSpeedButton
              Left = 56
              Top = 16
              Width = 23
              Height = 22
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000130B0000130B00001000000010000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
                8888888888888888888888888888888888888888888888888888888888888888
                8888888888888888888888800000004088888880000000408888888066666666
                4888888066666666488888806666666660888880666666666088888066666666
                6608888066666666660888806666666666088880666666666608888066666666
                6608888066666666660888806666666666088880666666666608888066666666
                6088888066666666608888880066666608888888006666660888888888066660
                8888888888066660888888888880060888888888888006088888888888888088
                8888888888888088888888888888888888888888888888888888}
              NumGlyphs = 2
              OnClick = CPolyBtnClick
            end
            object AdfBtn: TSpeedButton
              Left = 128
              Top = 16
              Width = 33
              Height = 22
              AllowAllUp = True
              Caption = 'ADF'
              OnClick = AdfBtnClick
            end
            object CircleBtn: TSpeedButton
              Left = 80
              Top = 16
              Width = 23
              Height = 22
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000130B0000130B00001000000010000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
                8888888888888888888888888888888888888888888888888888888888004008
                888888888800400888888888006666E008888888006666E00888888066666666
                6088888066666666608888806666666664888880666666666488880666666666
                6608880666666666660888066666666666088806666666666608880666666666
                6608880666666666660888066666666666088806666666666608880666666666
                6608880666666666660888806666666660888880666666666088888066666666
                6088888066666666608888880066666008888888006666600888888888000008
                8888888888000008888888888888888888888888888888888888}
              NumGlyphs = 2
              OnClick = CircleBtnClick
            end
            object SymbolBtn: TSpeedButton
              Left = 104
              Top = 16
              Width = 23
              Height = 22
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000130B0000130B00001000000010000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
                8888888888888888888888888000008888888888800000888888888806666608
                8888888806666608888888806666666088888880666666608888888066666660
                8888888066666660888888806666666088888880666666608888888066666660
                8888888066666660888888806666666088888880666666608888888806666608
                8888888806666608888888888000008888888888800000888888888888808888
                8888888888808888888888888880888888888888888088888888888888808888
                8888888888808888888888888000008888888888800000888888888888808888
                8888888888808888888888888880888888888888888088888888}
              NumGlyphs = 2
              OnClick = SymbolBtnClick
            end
            object GpsBtn: TSpeedButton
              Left = 160
              Top = 16
              Width = 33
              Height = 22
              Caption = 'GPS'
              OnClick = GpsBtnClick
            end
          end
          object ColLineCb: TCheckBox
            Left = 4
            Top = 344
            Width = 165
            Height = 17
            Caption = 'Columns in first line of file'
            TabOrder = 2
            OnClick = ColLineCbClick
          end
        end
      end
      object Cancel3Btn: TButton
        Left = 448
        Top = 384
        Width = 75
        Height = 20
        Caption = 'Cancel'
        TabOrder = 1
        OnClick = Cancel3BtnClick
      end
      object Prev3Btn: TButton
        Left = 544
        Top = 384
        Width = 75
        Height = 20
        Caption = '<< previous'
        TabOrder = 2
        OnClick = Prev3BtnClick
      end
      object Next3Btn: TButton
        Left = 624
        Top = 384
        Width = 75
        Height = 20
        Caption = 'next >>'
        TabOrder = 3
        OnClick = Next3BtnClick
      end
      object DbColNameEdit: TEdit
        Left = 104
        Top = 384
        Width = 121
        Height = 21
        TabOrder = 4
        Text = 'DbColNameEdit'
        OnExit = DbColnameEditExit
        OnKeyDown = DbColnameEditKeyDown
      end
      object LoadDefBtn2: TButton
        Left = 335
        Top = 384
        Width = 106
        Height = 20
        Caption = 'Load Definition'
        TabOrder = 5
        Visible = False
        OnClick = LoadDefBtn2Click
      end
    end
    object SaveDefSheet: TTabSheet
      Caption = 'Save Definition'
      ImageIndex = 4
      object Cancel5Btn: TButton
        Left = 448
        Top = 384
        Width = 75
        Height = 20
        Caption = 'Cancel'
        TabOrder = 0
        OnClick = Cancel5BtnClick
      end
      object Prev5Btn: TButton
        Left = 544
        Top = 384
        Width = 75
        Height = 20
        Caption = '<< previous'
        TabOrder = 1
        OnClick = Prev5BtnClick
      end
      object Next5Btn: TButton
        Left = 624
        Top = 384
        Width = 75
        Height = 20
        Caption = 'finish'
        ModalResult = 1
        TabOrder = 2
        OnClick = Next5BtnClick
      end
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 700
        Height = 376
        BevelOuter = bvLowered
        TabOrder = 3
        object SaveDefFileLabel: TLabel
          Left = 8
          Top = 14
          Width = 101
          Height = 13
          Caption = 'Save definition to file:'
        end
        object DatabaseLabel: TLabel
          Left = 112
          Top = 346
          Width = 72
          Height = 13
          Caption = 'DatabaseLabel'
          Layout = tlCenter
        end
        object ProjectionLabel: TLabel
          Left = 112
          Top = 314
          Width = 73
          Height = 13
          Caption = 'ProjectionLabel'
          Layout = tlCenter
          Visible = False
        end
        object Panel10: TPanel
          Left = 112
          Top = 10
          Width = 552
          Height = 25
          TabOrder = 0
          object DefFilenameLabel: TLabel
            Left = 8
            Top = 8
            Width = 85
            Height = 13
            Caption = 'DefFilenameLabel'
          end
        end
        object SaveDefFileBtn: TButton
          Left = 668
          Top = 10
          Width = 25
          Height = 25
          Caption = '...'
          TabOrder = 1
          OnClick = SaveDefFileBtnClick
        end
        object SaveDefBtn: TButton
          Left = 592
          Top = 42
          Width = 100
          Height = 25
          Caption = 'Save'
          TabOrder = 2
          Visible = False
          OnClick = SaveDefBtnClick
        end
        object IgnoreDuplicatesCb: TCheckBox
          Left = 8
          Top = 72
          Width = 609
          Height = 17
          Caption = 'Ignore already existing objects'
          TabOrder = 3
        end
        object SetDefaultSymCb: TCheckBox
          Left = 8
          Top = 96
          Width = 161
          Height = 17
          Caption = 'Set default symbol:'
          TabOrder = 4
          OnClick = SetDefaultSymCbClick
        end
        object DefaultSymLb: TComboBox
          Left = 168
          Top = 94
          Width = 169
          Height = 21
          ItemHeight = 13
          TabOrder = 5
          Text = 'DefaultSymLb'
        end
        object SaveDBToTdfCB: TCheckBox
          Left = 8
          Top = 48
          Width = 321
          Height = 17
          Caption = 'Save database settings to definition file '
          TabOrder = 6
          OnClick = SaveDBToTdfCBClick
        end
      end
      object ProjectionBtn: TButton
        Left = 8
        Top = 310
        Width = 93
        Height = 23
        Caption = 'Projection'
        Default = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        Visible = False
        OnClick = ProjectionBtnClick
      end
      object DatabaseBtn: TButton
        Left = 8
        Top = 342
        Width = 93
        Height = 23
        Caption = 'Database'
        TabOrder = 5
        OnClick = DatabaseBtnClick
      end
    end
    object SelTableSheet: TTabSheet
      Caption = 'Select Table'
      ImageIndex = 5
      object Next6Btn: TButton
        Left = 624
        Top = 384
        Width = 75
        Height = 20
        Caption = 'next >>'
        TabOrder = 0
        OnClick = Next6BtnClick
      end
      object Prev6Btn: TButton
        Left = 544
        Top = 384
        Width = 75
        Height = 20
        Caption = '<< previous'
        TabOrder = 1
      end
      object Cancel6Btn: TButton
        Left = 448
        Top = 384
        Width = 75
        Height = 20
        Caption = 'Cancel'
        TabOrder = 2
        OnClick = Cancel6BtnClick
      end
      object Panel11: TPanel
        Left = 0
        Top = 0
        Width = 700
        Height = 376
        BevelOuter = bvLowered
        TabOrder = 3
        object DbTableLb: TListBox
          Left = 4
          Top = 4
          Width = 693
          Height = 368
          ItemHeight = 13
          TabOrder = 0
          OnClick = DbTableLbClick
        end
      end
    end
  end
  object ADOConnection: TADOConnection
    LoginPrompt = False
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 304
    Top = 404
  end
  object ADOTable: TADOTable
    Connection = ADOConnection
    Left = 336
    Top = 404
  end
end
