unit TextWizardDlg;
{$H+}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Grids, ExtCtrls, Menus,
  Textutils, Buttons, TextFileDef, OpenDialogsModule,StrFile, Db,
  ADODB, WCtrls, TextPosPanel, WinOSInfo;

type Delim = array [1..100] of integer;

type
  TTextWizardWnd = class(TForm)
    PageControl1: TPageControl;
    ColumnDefSheet: TTabSheet;
    LabelTextSheet: TTabSheet;
    SeperatorSheet: TTabSheet;
    Panel1: TPanel;
    UseSeperatorCb: TCheckBox;
    Panel2: TPanel;
    TabSepCb: TCheckBox;
    CommaSepCb: TCheckBox;
    SpaceSepCb: TCheckBox;
    UserSepCb: TCheckBox;
    UserSepEdit: TEdit;
    FixedWideCb: TCheckBox;
    Prev2Btn: TButton;
    Prev1Btn: TButton;
    Next1Btn: TButton;
    Panel3: TPanel;
    SepDefScrollBox: TScrollBox;
    SepDefGrid: TStringGrid;
    SepDefCanvas: TWindow;
    Next2Btn: TButton;
    Cancel1Btn: TButton;
    Cancel2Btn: TButton;
    ColNameSheet: TTabSheet;
    Panel4: TPanel;
    Cancel3Btn: TButton;
    Prev3Btn: TButton;
    Next3Btn: TButton;
    ColNameScrollBox: TScrollBox;
    Panel5: TPanel;
    ColnameLabel: TLabel;
    SupportedColsLb: TListBox;
    ColNameGrid: TStringGrid;
    Panel6: TPanel;
    Panel7: TPanel;
    LabelPosLabel: TLabel;
    Panel8: TPanel;
    LabelFontLabel: TLabel;
    CurrentFontLabel1: TLabel;
    CurrentSizeLabel1: TLabel;
    CurrentFontLabel: TLabel;
    CurrentsizeLabel: TLabel;
    SetFontBtn: TButton;
    Cancel4Btn: TButton;
    Prev4Btn: TButton;
    Next4Btn: TButton;
    ColTypeGroupBox: TGroupBox;
    PointBtn: TSpeedButton;
    PolyBtn: TSpeedButton;
    CPolyBtn: TSpeedButton;
    AdfBtn: TSpeedButton;
    CircleBtn: TSpeedButton;
    SymbolBtn: TSpeedButton;
    SaveDefSheet: TTabSheet;
    Cancel5Btn: TButton;
    Prev5Btn: TButton;
    Next5Btn: TButton;
    Panel9: TPanel;
    SaveDefFileLabel: TLabel;
    Panel10: TPanel;
    SaveDefFileBtn: TButton;
    DefFilenameLabel: TLabel;
    SaveDefBtn: TButton;
    ProjectionBtn: TButton;
    DatabaseBtn: TButton;
    DbColNameEdit: TEdit;
    DbColnameLabel: TLabel;
    IgnoreDuplicatesCb: TCheckBox;
    LoadDefBtn: TButton;
    SemicolonSepCb: TCheckBox;
    ColLineCb: TCheckBox;
    ADOConnection: TADOConnection;
    ADOTable: TADOTable;
    SelTableSheet: TTabSheet;
    Next6Btn: TButton;
    Prev6Btn: TButton;
    Cancel6Btn: TButton;
    Panel11: TPanel;
    DbTableLb: TListBox;
    LoadDefBtn2: TButton;
    LabelPosPanel: TPanel;
    LabelPosMarkCb: TComboBox;
    LabelPosMarkLabel: TLabel;
    LabelPosAngleLabel: TLabel;
    LabelAngleEdit: TEdit;
    LabelPosCb: TCheckBox;
    SaveLabelPosBtn: TButton;
    LabelLayerCb: TCheckBox;
    LabelLayerPanel: TPanel;
    LabelLayerListCb: TComboBox;
    SetDefaultSymCb: TCheckBox;
    DefaultSymLb: TComboBox;
    AngleUpBtn: TSpeedButton;
    LabelOffsetLabel: TLabel;
    LabelOffsetEdit: TEdit;
    AngleDownBtn: TSpeedButton;
    OffsetUpBtn: TSpeedButton;
    OffsetDownBtn: TSpeedButton;
    TextPosPanel: TTextPosPanel;
    GpsBtn: TSpeedButton;
    SaveDBToTdfCB: TCheckBox;
    DatabaseLabel: TLabel;
    ProjectionLabel: TLabel;
    procedure FormShow(Sender: TObject);
    procedure SepDefGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure Prev2BtnClick(Sender: TObject);
    procedure SepDefCanvasPaint(Sender: TObject);
    procedure SepDefCanvasMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SepDefCanvasMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure SepDefCanvasMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SepDefGridMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure UseSeperatorCbClick(Sender: TObject);
    procedure TabSepCbClick(Sender: TObject);
    procedure CommaSepCbClick(Sender: TObject);
    procedure SpaceSepCbClick(Sender: TObject);
    procedure Next1BtnClick(Sender: TObject);
    procedure UserSepCbClick(Sender: TObject);
    procedure UserSepEditChange(Sender: TObject);
    procedure FixedWideCbClick(Sender: TObject);
    procedure Cancel1BtnClick(Sender: TObject);
    procedure Next2BtnClick(Sender: TObject);
    procedure ColNameGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure ColNameGridDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure SupportedColsLbEndDrag(Sender, Target: TObject; X,
      Y: Integer);
    procedure SupportedColsLbDragOver(Sender, Source: TObject; X,
      Y: Integer; State: TDragState; var Accept: Boolean);
    procedure ColNameGridEndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure ColNameGridDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure Prev3BtnClick(Sender: TObject);
    procedure Cancel3BtnClick(Sender: TObject);
    procedure Next3BtnClick(Sender: TObject);
    procedure SetFontBtnClick(Sender: TObject);
    procedure Cancel4BtnClick(Sender: TObject);
    procedure Prev4BtnClick(Sender: TObject);
    procedure PointBtnClick(Sender: TObject);
    procedure PolyBtnClick(Sender: TObject);
    procedure CPolyBtnClick(Sender: TObject);
    procedure CircleBtnClick(Sender: TObject);
    procedure SymbolBtnClick(Sender: TObject);
    procedure AdfBtnClick(Sender: TObject);
    procedure Cancel5BtnClick(Sender: TObject);
    procedure Prev5BtnClick(Sender: TObject);
    procedure Next4BtnClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SaveDefFileBtnClick(Sender: TObject);
    procedure SaveDefBtnClick(Sender: TObject);
    procedure Next5BtnClick(Sender: TObject);
    procedure ProjectionBtnClick(Sender: TObject);
    procedure DbColnameEditExit(Sender: TObject);
    procedure DbColnameEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DatabaseBtnClick(Sender: TObject);
    procedure LoadDefBtnClick(Sender: TObject);
    procedure SemicolonSepCbClick(Sender: TObject);
    procedure ColLineCbClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Cancel6BtnClick(Sender: TObject);
    procedure Next6BtnClick(Sender: TObject);
    procedure DbTableLbClick(Sender: TObject);
    procedure LoadDefBtn2Click(Sender: TObject);
    procedure AngleUpBtnClick(Sender: TObject);
    procedure AngleDownBtnClick(Sender: TObject);
    procedure LabelAngleEditChange(Sender: TObject);
    procedure LabelPosCbClick(Sender: TObject);
    procedure LabelPosMarkCbChange(Sender: TObject);
    procedure SaveLabelPosBtnClick(Sender: TObject);
    procedure LabelLayerCbClick(Sender: TObject);
    procedure SetDefaultSymCbClick(Sender: TObject);
    procedure LabelOffsetEditChange(Sender: TObject);
    procedure OffsetUpBtnClick(Sender: TObject);
    procedure OffsetDownBtnClick(Sender: TObject);
    procedure GpsBtnClick(Sender: TObject);
    procedure SaveDBToTdfCBClick(Sender: TObject);
    procedure TextPosPanelMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);

  private
    // private declarations
    TxtFilename    :string;             // name of the textfile that should be processed
    MaxDataLineLen :integer;            // maximum length of a line in the textfile
    CharPixelWidth :integer;            // Number of Pixels a character needs
    OldDelimPos    :integer;            // Old Position of Delimiter (used for move of delimeter)
    SystemDelimeter:string;             // Delimeter that is used if delemiter character should be used
    Delimeters     :Delim;              // array of delimeters if fixed columns are set
    DelimCount     :integer;            // Number of delimeters (fixed column) that are defined
    Currentdelim   :integer;            // Index of the current processed delimeter (used for move of delimter)
    LabelAnglePoint:TPoint;             // Dummy point that is used to setup the base-point of the label
    LabelSpecRect  :TRect;              // Rect that is used to define the label postition
    LabelDeltaX    :integer;            // Dummy variable to define label position
    LabelDeltaY    :integer;            // Dummy variable to define label position
    OrigMouse      :TPoint;             // Store position of mouse (used for move of delimeter)
    DragDropCol    :integer;            // Dummy variable that is used for drag-drop
    ObjectTypeFilter:integer;           // used if objecttype is predefined
    UsedObjectType :integer;            // store the used objecttype
    InputFileType  :TInputFileType;     // type of the input file
    UsedAccessTablename:string;         // name of Access table that has been used to create definition

    OpenDialogsModuleWnd:TOpenDialogsModuleWnd; // window to select filename/fonts
    DataBaseHeaderColumns:TStrings;             // list of database header column

    SuppressAutomaticColAssignment:boolean;
    // modified flags of the pages
    SeperatorSheetModified:boolean;
    ColumnDefSheetModified:boolean;
    LabelTextSheetModified:boolean;
    ColNameSheetModified  :boolean;
    SelTableSheetModified :boolean;
    FirstShow             :boolean;
    DatabaseCol           :integer;
    // names for the supported columns
    LabelColDef           :string;
    LabelMarkerColDef     :string;
    LayerColDef           :string;
    XColDef               :string;
    YColDef               :string;
    RadiusColDef          :string;
    SymbolNameColDef      :string;
    SymbolSizeColDef      :string;
    SymbolRotationColDef  :string;
    SymbolIdxColDef       :string;
    ParamAColDef          :string;
    ParamBColDef          :string;
    ParamCColDef          :string;
    ObjectTypeColDef      :string;
    ObjectNrColDef        :string;
    GrafikColDef          :string;
    DatabaseColDef        :string;
    PolySeperatorColDef   :string;
    ProgisIdColDef        :string;
    GpsSymDirColDef       :string;
    GpsSymLineTypeColDef  :string;
    GpsSymLineColorColDef :string;
    GpsSymFillTypeColDef  :string;
    GpsSymFillColorColDef :string;
    GpsLineTypeColDef     :string;
    GpsLineColorColDef    :string;
    GpsLineWidthColDef    :string;

    StrfileObj            :ClassStrfile;
    CurrModalResult       :integer;

    procedure CheckSeperatorTabPage;   // Check the Separator selection Tab-Page
    procedure DrawSepDefCanvas;        // method to draw the fix column seperator definitions
    procedure DrawDelimiters;          // method to draw the delimiters of fix column definition
    procedure CheckColumnDefTabPage;   // Check the Delimters Tab-Page
    function  GetDelimiterPos(X:Integer):integer; // method to get the position of a delimter
    procedure RedrawColumnDefPage;     // method to redraw the column definition Tab-page
    procedure SetupColNameSheet;       // setup the column-name sheet
    procedure SetupColumnDefSheet;     // setup the column-definition sheet
    procedure CheckColNameTabPage;     // check the column-definition sheet
    procedure SetupSupportedCols;      // setup the supported columns/Objecttype of the column-definition sheet
    procedure SetupSaveDefSheet;       // setup the save-definition sheet
    procedure SetupPagesWithTextFileDef;

    function  ColNameIsSupportedColumnName(aColname:string):boolean;
    function  DatabaseColumnsInColnameSheet:boolean;
    procedure SetupFirstLineOfFile(ColIdx:integer; Token:string);
    procedure OnOpenDialogsModuleWndClose(Sender: TObject;  var Action: TCloseAction);
    procedure SetupSelTableSheet;
    procedure CheckSelTableTabPage;
    procedure SetupLabelTextSheet;
    procedure CheckLabelTextTabPage;
    procedure SetupLabelPosPanel;
  public
    CurrTextFileDef:TTextFileDef;
    WorkingDir     :string;
    LngCode        :string;
    procedure SetupTextFileDefFromWindow(var aTextDef:TTextFileDef);
    procedure SetFilename(const aFilename:string);
    procedure SetObjectTypeFilter(const atype:integer);
    function  GetModalResult:integer;
  end;

const MaxRows = 50;     // max. count of rows of  ColNameGrid

var
  TextWizardWnd: TTextWizardWnd;

implementation

uses Maindlg, SetTxtFileDlg;

{$R *.DFM}

function PointDistance
   (
   const Point1    : TPoint;
   const Point2    : TPoint
   )
   : Double;
  begin
    Result:=Sqrt(Sqr(Point1.X-Point2.X)+Sqr(Point1.Y-Point2.Y));
  end;

function A_AnsiExtractQuotedStr(var Src: PChar; Quote: Char): string;
var
  P, Dest: PChar;
  DropCount: Integer;
begin
  Result := '';
  if (Src = nil) or (Src^ <> Quote) then Exit;
  Inc(Src);
  DropCount := 1;
  P := Src;
  Src := AnsiStrRScan(Src, Quote);
  while Src <> nil do   // count adjacent pairs of quote chars
  begin
    Inc(Src);
    if Src^ <> Quote then Break;
    Inc(Src);
    Inc(DropCount);
    Src := AnsiStrRScan(Src, Quote);
  end;
  if Src = nil then Src := StrEnd(P);
  if ((Src - P) <= 1) then Exit;
  if DropCount = 1 then
    SetString(Result, P, Src - P - 1)
  else
  begin
    SetLength(Result, Src - P - DropCount);
    Dest := PChar(Result);
    Src := AnsiStrRScan(P, Quote);
    while Src <> nil do
    begin
      Inc(Src);
      if Src^ <> Quote then Break;
      Move(P^, Dest^, Src - P);
      Inc(Dest, Src - P);
      Inc(Src);
      P := Src;
      Src := AnsiStrRScan(Src, Quote);
    end;
    if Src = nil then Src := StrEnd(P);
    Move(P^, Dest^, Src - P - 1);
  end;
end;

procedure TTextWizardWnd.SetFilename(const aFilename:string);
var aFileExt:string;
begin
   TxtFilename:=aFilename;
   if Uppercase(ExtractFileExt(TxtFilename)) = '.GPX' then begin
        TxtFilename:=GPXToCSV(TxtFilename);
   end;
   // evaluate the type of the file
   aFileExt:=ExtractFileExt(TxtFilename);
   aFileExt:=UpperCase(aFileExt);
   if aFileExt = '.DBF' then InputFileType:=ft_dBase
   else if aFileExt = '.MDB' then InputFileType:=ft_Access
   else if aFileExt = '.WGI' then InputFileType:=ft_Access
   else InputFileType:=ft_TextFile;
end;

procedure TTextWizardWnd.SetObjectTypeFilter(const atype:integer);
begin
   ObjectTypeFilter:=aType;
end;

procedure TTextWizardWnd.SetupColumnDefSheet;
var DataLine, Substring: ansistring;
    DataFile: textfile;
    ColIdx:integer;
    RowIdx:integer;
    ColWidths:array of integer;
begin
   if not FileExists(TxtFilename) then
   begin;
      ShowMessage('File "' + TxtFilename + '" not found !');
      Exit;
   end;
   // only if the page has been modified it has to be reread
   if not ColumnDefSheetModified then
   begin
      RedrawColumnDefPage;  // the page only has to be redrawed
      Exit;
   end;

   Screen.Cursor:=crHourGlass;
   SepDefGrid.Visible := false;
   // reset the StringGrid that shows the file
   for ColIdx := 0 to SepDefGrid.ColCount - 1 do
      for RowIdx := 0 to SepDefGrid.RowCount - 1 do
         SepDefGrid.Cells[ColIdx, RowIdx] := '';

   // open the file
   {$I-}
   AssignFile(DataFile, TxtFilename);
   Reset(DataFile);
   SepDefGrid.RowCount := 0;
   SepDefGrid.FixedCols := 0;
   SepDefGrid.ColCount := 1;

   try
      if FixedWideCb.Checked then // fixed columns set
      begin;
         RowIdx:=0;
         SepDefGrid.FixedRows:=0;
         SepDefGrid.DefaultColWidth:= SepDefGrid.Width-6;
         MaxDataLineLen:=0;
         SepDefGrid.RowCount:=0; // reset the rows
         while not eof(DataFile) and (RowIdx<= MaxRows)  do // maximum of rows to be displayed
         begin;
            Readln(DataFile, DataLine);
            if (length(DataLine) > MaxDataLineLen) then
               MaxDataLineLen:=length(DataLine); // get maximum length of line
            if SepDefGrid.RowCount-1 < RowIdx then // check if a row has to be added
               SepDefGrid.RowCount:=SepDefGrid.RowCount+1;
            SepDefGrid.Cells[0,RowIdx]:=DataLine;
            RowIdx:=RowIdx+1;
         end;
         // setup the length of the SepDefCanvas and the SepDefGrid
         SepDefGrid.Width:=(MaxDataLineLen * CharPixelWidth) + CharPixelWidth;
         SepDefCanvas.Width:=SepDefGrid.Width;

         SepDefGrid.ColWidths[0] := SepDefGrid.Width;
         // STR ++
         SepDefScrollBox.HorzScrollBar.Range := SepDefCanvas.Width;
         SepDefScrollBox.HorzScrollBar.Visible :=  (SepDefCanvas.Width > SepDefScrollBox.Width);

         if SepDefScrollBox.HorzScrollBar.Visible  then SepDefGrid.ScrollBars := ssNone
         	else SepDefGrid.ScrollBars := ssHorizontal;
          // STR --
         //SepDefGrid.DefaultColWidth:=SepDefGrid.Width;
         RedrawColumnDefPage;
         SepDefGrid.Options:=SepDefGrid.Options-[goFixedVertLine, gofixedHorzLine ];
         CurrTextFileDef.SetMaxLineLen(MaxDataLineLen);
      end
      else if (UseSeperatorCb.Checked) then // use seperator
      begin
         SepDefGrid.DefaultColWidth:=SepDefScrollBox.Width-2;
         RowIdx := 1;
         SepDefGrid.RowCount := 2;  // there is at least one data-row
         SepDefGrid.ColCount := 1;  // there is at least one column
         SepDefGrid.FixedRows := 1; // One Fixed Row to show number of column
         SetLength(ColWidths,0);

         while (not eof(DataFile)) and (RowIdx <= MaxRows ) do // maximum of rows to be displayed
         begin
            Readln(DataFile, DataLine);
            ColIdx:=1;
            if (SpaceSepCb.Checked) then // there can be more spaces as seperator, so compress them
               Q_SpaceCompressInPlace(DataLine);
            while pos(SystemDelimeter, DataLine) <> 0 do  // extract the tokens
            begin
               SubString := Copy(DataLine, 1, pos(SystemDelimeter, DataLine) - 1); // we got the substring
               Delete(DataLine, 1, Length(SubString) + Length(SystemDelimeter));   // delete till next token

               // check if there are enough columns defined
               if SepDefGrid.ColCount < ColIdx then
                  SepDefGrid.ColCount := SepDefGrid.ColCount + 1;
               // check if there are enough rows defined
               if SepDefGrid.RowCount <= RowIdx then
                  SepDefGrid.RowCount := SepDefGrid.RowCount + 1;

               SepDefGrid.Cells[ColIdx-1, 0] := IntToStr(ColIdx); // write the column number to first row
               Trim(SubString);
               if High(ColWidths) < ColIdx-1 then
               begin
                  SetLength(ColWidths,ColIdx);
                  ColWidths[ColIdx-1]:=0;
               end;
               if length(SubString) > ColWidths[ColIdx-1] then
                  ColWidths[ColIdx-1]:=length(SubString);

               SepDefGrid.Cells[ColIdx-1, RowIdx]:=SubString;     // write SubString to Cell
               ColIdx:=ColIdx+1; // add column index
            end;
            // add last token after seperator to Grid
            // check if there are enough columns defined
            if SepDefGrid.ColCount < ColIdx then
               SepDefGrid.ColCount := SepDefGrid.ColCount + 1;
            // check if there are enough rows defined
            if SepDefGrid.RowCount <= RowIdx then
               SepDefGrid.RowCount := SepDefGrid.RowCount + 1;

            SepDefGrid.Cells[ColIdx-1, 0] := IntToStr(ColIdx); // write the column number to first row
            Trim(DataLine);

            if High(ColWidths) < ColIdx-1 then
            begin
               SetLength(ColWidths,ColIdx);
               ColWidths[ColIdx-1]:=0;
            end;

            if length(DataLine) > ColWidths[ColIdx-1] then
               ColWidths[ColIdx-1]:=length(DataLine);
            SepDefGrid.Cells[ColIdx-1, RowIdx]:=DataLine;      // write SubString to Cell
            RowIdx:=RowIdx+1; // add row index
         end;


         // setup the column widths
         SepDefGrid.Width:=0;
         for ColIdx:=0 to High(ColWidths) do
         begin
            SepDefGrid.ColWidths[ColIdx]:=(ColWidths[ColIdx]*CharPixelWidth)+3;
            SepDefGrid.Width:=SepDefGrid.Width+((ColWidths[ColIdx]*CharPixelWidth) + 3);
         end;
         SepDefGrid.Width:=SepDefGrid.Width+CharPixelWidth;
         SetLength(ColWidths,0);
         SepDefGrid.Options:=SepDefGrid.Options+[goFixedVertLine, gofixedHorzLine ];


         if SepDefGrid.Width > SepDefScrollBox.Width then
         begin
            SepDefGrid.RowCount:=10;
            SepDefGrid.Height:=170;
         end
         else
            SepDefGrid.Height:=190;


      end;

   finally
      CloseFile(DataFile);
      SepDefGrid.Update;
      SepDefGrid.Visible := true;
      Screen.Cursor:=crDefault;
   end;
   {$I+}
end;

procedure TTextWizardWnd.FormShow(Sender: TObject);
var i:integer;
    ALayer:Variant;
    aLayername:string;
    ASymbolDef:Variant;
    aSymbolName:string;
begin
   if not FirstShow then
      exit;
   StrfileObj:=ClassStrFile.Create;
   StrfileObj.CreateList(WorkingDir,LngCode,'TXTIMP');
   Self.Caption:=StrfileObj.Assign(10);                  // Text file definition wizard
   PageControl1.Pages[0].Caption:=StrFileObj.Assign(11); // Text file
   PageControl1.Pages[1].Caption:=StrFileObj.Assign(12); // Label
   PageControl1.Pages[2].Caption:=StrFileObj.Assign(13); // Column seperator
   PageControl1.Pages[3].Caption:=StrFileObj.Assign(14); // Column assignement
   PageControl1.Pages[4].Caption:=StrFileObj.Assign(15); // Save definition
   PageControl1.Pages[5].Caption:=StrFileObj.Assign(78); // Table selection
   UseSeperatorCb.Caption:=StrFileObj.Assign(16);        // Use column seperator
   TabSepCb.Caption:=StrFileObj.Assign(17);              // Tabulator
   CommaSepCb.Caption:=StrFileObj.Assign(18);            // Comma
   SpaceSepCb.Caption:=StrFileObj.Assign(19);            // Space
   SemicolonSepCb.Caption:=StrFileObj.Assign(75);        // Semicolon
   UserSepCb.Caption:=StrFileObj.Assign(20);             // User defined
   FixedWideCb.Caption:=StrFileObj.Assign(21);           // Fixed wide column
   Cancel1Btn.Caption:=StrFileObj.Assign(7);             // Cancel
   Prev1Btn.Caption:=StrfileObj.Assign(22);              // << previous
   Next1Btn.Caption:=StrfileObj.Assign(23);              // next >>
   Cancel2Btn.Caption:=StrFileObj.Assign(7);             // Cancel
   Prev2Btn.Caption:=StrfileObj.Assign(22);              // << previous
   Next2Btn.Caption:=StrfileObj.Assign(23);              // next >>
   ColTypeGroupBox.Caption:=StrFileObj.Assign(24);       // Object Type
   ColnameLabel.Caption:=StrFileObj.Assign(25);          // Supported columns
   Cancel3Btn.Caption:=StrFileObj.Assign(7);             // Cancel
   Prev3Btn.Caption:=StrfileObj.Assign(22);              // << previous
   Next3Btn.Caption:=StrfileObj.Assign(23);              // next >>
   LabelPosLabel.Caption:=StrfileObj.Assign(26);         // Define position of label
   LabelFontLabel.Caption:=StrfileObj.Assign(27);        // Define font of label
   CurrentFontLabel1.Caption:=StrFileObj.Assign(28);     // Current font:
   CurrentSizeLabel1.Caption:=StrFileObj.Assign(29);     // Current size:
   SetFontBtn.Caption:=StrFileObj.Assign(30);            // Change font and size
   Cancel4Btn.Caption:=StrFileObj.Assign(7);             // Cancel
   Prev4Btn.Caption:=StrfileObj.Assign(22);              // << previous
   Next4Btn.Caption:=StrfileObj.Assign(23);              // next >>
   SaveDefFileLabel.Caption:=StrFileObj.Assign(31);      // Save definition to file:
   SaveDefBtn.Caption:=StrFileObj.Assign(32);            // Save
   Cancel5Btn.Caption:=StrFileObj.Assign(7);             // Cancel
   Prev5Btn.Caption:=StrfileObj.Assign(22);              // << previous
   Next5Btn.Caption:=StrfileObj.Assign(33);              // Finish
   Cancel6Btn.Caption:=StrFileObj.Assign(7);             // Cancel
   Prev6Btn.Caption:=StrfileObj.Assign(22);              // << previous
   Next6Btn.Caption:=StrfileObj.Assign(23);              // next >>
   ProjectionBtn.Caption:=StrfileObj.Assign(4);          // Projection
   DatabaseBtn.Caption:=StrfileObj.Assign(5);            // Database
   DbColnameLabel.Caption:=StrfileObj.Assign(57);        // Column name:
   IgnoreDuplicatesCb.Caption:=StrfileObj.Assign(3);     // Ignore already existing objects
   LoadDefBtn.Caption:=StrfileObj.Assign(72);            // Load definition
   ColLineCb.Caption:=StrfileObj.Assign(76);             // Columns in first line of file
   LabelPosCb.Caption:=StrFileObj.Assign (85);           // Setup label positioning markers
   LabelPosAngleLabel.Caption:=StrFileObj.Assign (91);   // Label angle:
   LabelOffsetLabel.Caption:=StrFileObj.Assign (92);     // Label offset:

   LabelPosMarkLabel.Caption:=StrFileObj.Assign(86);     // Label positioning marker:
   LabelLayerCb.Caption:=StrFileObj.Assign(87);          // Create labels on seperate layers

   SetDefaultSymCb.Caption:=StrFileObj.Assign(90);       // Set default Symbol:
   SaveDBToTdfCB.Caption := StrfileObj.Assign(110);      // Save database settings to definition file


   // setup the Columnnames
   LabelColDef:=StrfileObj.Assign(58);
   LayerColDef:=StrfileObj.Assign(59);
   XColDef:=StrfileObj.Assign(60);
   YColDef:=StrfileObj.Assign(61);
   RadiusColDef:=StrfileObj.Assign(62);
   SymbolNameColDef:=StrfileObj.Assign(63);
   SymbolSizeColDef:=StrfileObj.Assign(88);
   SymbolRotationColDef:=StrfileObj.Assign(89);
   SymbolIdxColDef:=StrfileObj.Assign(64);
   ParamAColDef:=StrfileObj.Assign(65);
   ParamBColDef:=StrfileObj.Assign(66);
   ParamCColDef:=StrfileObj.Assign(67);
   ObjectTypeColDef:=StrfileObj.Assign(68);
   ObjectNrColDef:=StrfileObj.Assign(69);
   GrafikColDef:=StrfileObj.Assign(70);
   DatabaseColDef:=StrfileObj.Assign(71);
   PolySeperatorColDef:=StrfileObj.Assign(83);
   LabelMarkerColDef:=StrfileObj.Assign(84);
   ProgisIdColDef:=StrfileObj.Assign(95);
   GpsSymDirColDef:=StrfileObj.Assign(96);
   GpsSymLineTypeColDef:=StrfileObj.Assign(97);
   GpsSymLineColorColDef:=StrfileObj.Assign(98);
   GpsSymFillTypeColDef:=StrfileObj.Assign(99);
   GpsSymFillColorColDef:=StrfileObj.Assign(100);
   GpsLineTypeColDef:=StrfileObj.Assign(101);
   GpsLineColorColDef:=StrfileObj.Assign(102);
   GpsLineWidthColDef:=StrfileObj.Assign(103);

   // fill the LabelLayerListCb
   LabelLayerListCb.Items.Clear;
   for i:=0 to MainWnd.Document.Layers.Count-1 do
   begin
      ALayer:=MainWnd.Document.Layers.Items[i];
      aLayername:=ALayer.Layername;
      LabelLayerListCb.Items.Add(aLayername);
      if i = 0 then LabelLayerListCb.Text:=aLayername;
   end;

   // fill the DefaultSymLb
   DefaultSymLb.Items.Clear;
   DefaultSymLb.Text:='';
   for i:=0 to MainWnd.Document.Symbols.Count - 1 do
   begin
      aSymbolDef:=MainWnd.Document.Symbols.Items[i];
      aSymbolName:=aSymbolDef.Name;
      DefaultSymLb.Items.Add(aSymbolName);
      if i = 0 then DefaultSymLb.Text:=aSymbolName;
   end;

   // setup the window
   SepDefCanvas.Width:=SepDefGrid.ClientWidth+2;
   SepDefGrid.Visible := true;
   SaveDefFileBtn.Enabled := TRUE;
   if InputFileType = ft_TextFile then
   begin
      // if input-file is a Textfile setup the SeperatorSheet
      PageControl1.Pages[0].TabVisible:=FALSE;  // ColumnDefSheet will not be shown at begin
      PageControl1.Pages[1].TabVisible:=FALSE;  // LabelTextSheet will not be shown at begin
      PageControl1.Pages[2].TabVisible:=TRUE;   // SeperatorSheet will be shown at begin
      PageControl1.Pages[3].TabVisible:=FALSE;  // ColNameSheet will not be shown
      PageControl1.Pages[4].TabVisible:=FALSE;  // SaveDefSheet will not be shown
      PageControl1.Pages[5].TabVisible:=FALSE;  // SelTableSheet will not be shown
      PageControl1.ActivePage:=SeperatorSheet;  // start with the seperator-sheet if inputfile is a Text-File
      UseSeperatorCb.Checked:=FALSE;
      UseSeperatorCbClick(self);
      Prev1Btn.Enabled:=FALSE;
   end
   else if InputFileType = ft_dBase then
   begin
      PageControl1.Pages[0].TabVisible:=FALSE;  // ColumnDefSheet will not be shown
      PageControl1.Pages[1].TabVisible:=FALSE;  // LabelTextSheet will not be shown
      PageControl1.Pages[2].TabVisible:=FALSE;  // SeperatorSheet will not be shown
      PageControl1.Pages[3].TabVisible:=TRUE;   // ColNameSheet will be shown
      PageControl1.Pages[4].TabVisible:=FALSE;  // SaveDefSheet will not be shown
      PageControl1.Pages[5].TabVisible:=FALSE;  // SelTableSheet will not be shown
      PageControl1.ActivePage:=ColNameSheet;    // show the ColNameSheet
      Prev3Btn.Enabled:=FALSE;                  // in dBase ColNameSheet is first sheet
      ColNameSheetModified:=TRUE;               // set modified that page will be rebuild
   end
   else if InputFileType = ft_Access then
   begin
      PageControl1.Pages[0].TabVisible:=FALSE;  // ColumnDefSheet will not be shown
      PageControl1.Pages[1].TabVisible:=FALSE;  // LabelTextSheet will not be shown
      PageControl1.Pages[2].TabVisible:=FALSE;  // SeperatorSheet will not be shown
      PageControl1.Pages[3].TabVisible:=FALSE;  // ColNameSheet will not be shown
      PageControl1.Pages[4].TabVisible:=FALSE;  // SaveDefSheet will not be shown
      PageControl1.Pages[5].TabVisible:=TRUE;   // SelTableSheet will not be shown
      PageControl1.ActivePage:=SelTableSheet;   // show the ColNameSheet
      Prev6Btn.Enabled:=FALSE;                  // in Access SelTableSheet is first sheet
      SelTableSheetModified:=TRUE;              // set modified that page will be rebuild
   end;

   DefFilenameLabel.Caption:='';

   // evaluate the pixelwidth that is used to setup the grids
   CharPixelWidth:=SepDefGrid.Canvas.TextWidth('w');

   // Setup the choices for the automatic column assignements
   CurrTextFileDef.SetupAutoColumnAssignments(WorkingDir+'TxtImp.ini');
   // Check if there has been already a Text-File Definition done
   if CurrTextFileDef.GetDelimeterType <> delim_None then
   begin
      SetupPagesWithTextFileDef;
   end;

   if InputFileType = ft_TextFile then
   begin
      CheckSeperatorTabPage;
   end
   else if InputFileType = ft_dBase then
   begin
      SetupColNameSheet;
      CheckColNameTabPage;
   end
   else if InputFileType = ft_Access then
   begin
      SetupSelTableSheet;
      CheckSelTableTabPage;
   end;

   if ObjectTypeFilter <> -1 then
   begin
      LoadDefBtn.Visible:=TRUE;
      if (InputFileType = ft_Dbase) or (InputFileType = ft_Access) then
         LoadDefBtn2.Visible:=TRUE;
   end
   else
   begin
      LoadDefBtn.Visible:=FALSE;
   end;
   if InputFileType <> ft_TextFile then
      SupportedColsLb.Height:=SupportedColsLb.Height+20;

   DatabaseLabel.Caption := MainWnd.AXImpExpDbc.ImpDBInfo;
   ProjectionLabel.Caption := MainWnd.AxGisProjection.SourceProjection;
   DatabaseLabel.Enabled :=  SaveDBToTdfCB.Checked;
   DatabaseBtn.Enabled := SaveDBToTdfCB.Checked;

   FirstShow:=FALSE;
end;

procedure TTextWizardWnd.SetupPagesWithTextFileDef;
var i,j,k:integer;
    pos:integer;
    atype:TColumnType;
    ColName:string;
    ColNames:array of string;
    maxlen, oldlen:integer;
    token:string;
    aDbColname:string;
    LabelFont:string;
    LabelSize, LabelAngle, label_horizontal, label_vertical:integer;
    LabelOffset:integer;
    isfound:boolean;
    dummy:string;
begin
   // method is used to setup the pages with the settings of a text file definition

   // first check if the filetype of the textdefinition does fit to the filetype of the current file
   if InputFileType <> CurrTextFileDef.GetInputFileType then
   begin
      Showmessage(StrFileObj.Assign(77));
      exit;
   end;
   // Writing Label for saving
   TextWizardWnd.DefFilenameLabel.Caption := SetTxtFileWnd.OpenDialogTextDef.FileName;
   // first setup the SeperatorTabPage
   if CurrTextFileDef.GetDelimeterType = delim_Character then
   begin
      SystemDelimeter:=CurrTextFileDef.GetDelimeterChar;
      UseSeperatorCb.Checked:=TRUE;
      UseSeperatorCbClick(nil);
      if SystemDelimeter = chr(9) then // Tabulator seperator is used
      begin
         TabSepCb.Checked:=TRUE;
         TabSepCbClick(nil);
      end
      else if SystemDelimeter = ',' then // comma seperator is used
      begin
         CommaSepCb.Checked:=TRUE;
         CommaSepCbClick(nil);
      end
      else if SystemDelimeter = ' ' then // space seperator is used
      begin
         SpaceSepCb.Checked:=TRUE;
         SpaceSepCbClick(nil);
      end
      else if SystemDelimeter = ';' then // semicolon seperator is used
      begin
         SemicolonSepCb.Checked:=TRUE;
         SemicolonSepCbClick(nil);
      end
      else // use user seperator
      begin
         UserSepCb.Checked:=TRUE;
         UserSepCbClick(nil);
         UserSepEdit.Text:=SystemDelimeter;
      end;
      CheckSeperatorTabPage;
      SeperatorSheetModified:=FALSE;
      DelimCount:=0;     // if seperator character is used remove the delimiters
      for i:=1 to 100 do // reset the delemiters
         Delimeters[i]:=10000;
      SepDefCanvas.visible:=FALSE;
      SepDefGrid.Top:=0;
      AdfBtn.Enabled:=FALSE; // allow no ADF-Definition if sepertor is set
   end
   else if CurrTextFileDef.GetDelimeterType = delim_FixColumns then
   begin
      FixedWideCb.Checked:=TRUE;
      FixedWideCbClick(nil);
      CheckSeperatorTabPage;
      SeperatorSheetModified:=FALSE;
      SepDefCanvas.visible:=TRUE;
      SepDefGrid.Top:=24;
      // copy the Delimeters
      for i:=0 to CurrTextFileDef.GetNumFixColDelimeters - 1 do
      begin
         Delimeters[i+1]:=CurrTextFileDef.GetFixColDelimeter(i)*CharPixelWidth;
      end;
      DelimCount:=CurrTextFileDef.GetNumFixColDelimeters;
      AdfBtn.Enabled:=TRUE; // allow ADF-Definition if fixed column seperator is set
   end;
   SeperatorSheetModified:=FALSE;

   // now setup the ColumnDefSheet
   ColumnDefSheetModified:=TRUE;  // to force repaint of the sheet
   // setup the columndef-sheet
   SetupColumnDefSheet;
   CheckColumnDefTabPage;
   ColumnDefSheetModified:=FALSE; // to keep it as it is

   if InputFileType = ft_Access then
   begin
      // now setup the SelTableSheet
      SelTableSheetModified:=TRUE;
      SetupSelTableSheet;
      isfound:=FALSE;
      for i:=0 to DbTableLb.Items.Count-1 do
      begin
         if DbTableLb.Items[i] = CurrTextFileDef.GetDatabaseTablename then
         begin
            isfound:=TRUE;
            DbTableLb.ItemIndex:=i;
            break;
         end;
      end;
      if not isfound then
      begin
         if DbTableLb.Items.Count > 0 then
            DbTableLb.ItemIndex:=0; // use the first table
      end;
      CheckSelTableTabPage;
      SelTableSheetModified:=FALSE;
      GpsBtn.Enabled:=FALSE;
   end;

   // now setup the ColNameSheet
   ColNameSheetModified:=TRUE;
   // check if the first line should be used as column line
   if InputFileType = ft_TextFile then
   begin
      if CurrTextFileDef.GetFirstLineIsColumnDef then
         ColLineCb.Checked:=TRUE
      else
         ColLineCb.Checked:=FALSE;
      GpsBtn.Enabled:=TRUE;
   end;
   // set flag that the automatic evaluation of the columns should be switched off
   // if the definition will be fetched from a file
   SuppressAutomaticColAssignment:=TRUE;
   SetupColNameSheet;

   // setup the buttons to get the column definitions
   case CurrTextFileDef.GetObjectType of
      1 : begin PointBtn.Flat:=TRUE; PointBtnClick(nil); end;
      2 : begin PolyBtn.Flat:=TRUE; PolyBtnClick(nil); end;
      4 : begin CPolyBtn.Flat:=TRUE; CPolyBtnClick(nil); end;
      12: begin CircleBtn.Flat:=TRUE; CircleBtnClick(nil); end;
      8 : begin SymbolBtn.Flat:=TRUE; SymbolBtnClick(nil); end;
      99: begin ADFBtn.Flat:=TRUE; AdfBtnClick(nil); UseSeperatorCb.Enabled:=FALSE; end; // allow no seperator if ADF-File is set
      98: begin GpsBtn.Flat:=TRUE; GpsBtnClick(nil); end;
   end;

   // now iterate through the assigned columns and setup the grid and supported columns
   for i:=0 to CurrTextFileDef.GetNumColumnDefs -1 do
   begin
      CurrTextFileDef.GetColumnDef(i, pos, atype, aDbColname);
      // map the type to the corresponding column-name
      case atype of
         col_ProgisId        : ColName:=ProgisIdColDef;
         col_PolySeperator   : ColName:=PolySeperatorColDef;
         col_Label           : ColName:=LabelColDef;                        
         col_LabelMarker     : ColName:=LabelMarkerColDef;
         col_Layer           : ColName:=LayerColDef;
         col_X               : ColName:=XColDef;
         col_Y               : ColName:=YColDef;
         col_Radius          : ColName:=RadiusColDef;
         col_SymbolName      : ColName:=SymbolNameColDef;
         col_SymbolSize      : ColName:=SymbolSizeColDef;
         col_SymbolRotation  : ColName:=SymbolRotationColDef;
         col_SymbolIndex     : ColName:=SymbolIdxColDef;
         col_ParamA          : ColName:=ParamAColDef;
         col_ParamB          : ColName:=ParamBColDef;
         col_ParamC          : ColName:=ParamCColDef;
         col_ObjectType      : ColName:=ObjectTypeColDef;
         col_ObjectNr        : ColName:=ObjectNrColDef;
         col_Graphic         : ColName:=GrafikColDef;
         col_Database        : ColName:=aDbColname;
         col_GpsSymDir       : ColName:=GpsSymDirColDef;
         col_GpsSymLineType  : ColName:=GpsSymLineTypeColDef;
         col_GpsSymLineColor : ColName:=GpsSymLineColorColDef;
         col_GpsSymFillType  : ColName:=GpsSymFillTypeColDef;
         col_GpsSymFillColor : ColName:=GpsSymFillColorColDef;
         col_GpsLineType     : ColName:=GpsLineTypeColDef;
         col_GpsLineColor    : ColName:=GpsLineColorColDef;
         col_GpsLineWidth    : ColName:=GpsLineWidthColDef;
      end;

      // if pos <= ColNameGrid.RowCount-1 then
      // begin
         // check if the column exists in the supported column list
         if ColNameIsSupportedColumnName(ColName) then
         begin
            // check if the column is supported by the object-type that is set
            for j:=0 to SupportedColsLb.Items.Count-1 do
            begin
               if SupportedColsLb.Items[j] = ColName then
               begin
                  // remove the column out of the list
                  SetLength(ColNames,SupportedColsLb.Items.Count);
                  for k:=0 to SupportedColsLb.Items.Count-1 do
                      ColNames[k]:=SupportedColsLb.Items[k];
                  SupportedColsLb.Items.Clear;
                  for k:=0 to High(ColNames) do
                  begin
                     if ColNames[k] <> ColName then
                        SupportedColsLb.Items.Add(ColNames[k]);
                  end;
                  SetLength(ColNames,0); // delete the ColNames
                  ColNameGrid.Cells[pos, 0]:=ColName;
                  break;
               end;
            end;
         end
         else
         begin
            // it is a database column
            ColNameGrid.Cells[pos, 0]:=ColName;
         end;
         // setup the width of the cell
         maxlen:=0;
         oldlen:=ColNameGrid.ColWidths[pos];
         for k:=0 to ColNameGrid.RowCount - 1 do
         begin
            token:=ColNameGrid.Cells[pos, k];
            if (length(token) > maxlen) then
               maxlen:=length(token);
         end;
         maxlen:=maxlen*CharPixelWidth;
         if maxlen > oldlen then
         begin
            ColNameGrid.ColWidths[pos]:=maxlen;
            ColNameGrid.Width:=ColNameGrid.Width+(maxlen-oldlen);
         end;
      end;
   //end;
   SuppressAutomaticColAssignment:=FALSE;

   // setup the label
   if CurrTextFileDef.UseLabelData then
   begin
      CurrTextFileDef.GetLabelData(LabelFont, LabelSize, LabelAngle, label_horizontal, label_vertical, LabelOffset);
      // write the current label-definition to the text-file-definition
      if label_horizontal=1 then // horizontal alignement is ts_Center
      begin
         if label_vertical = 1 then TextPosPanel.TextAlign:=taCenter;       // center
         if label_vertical = 0 then TextPosPanel.TextAlign:=taUpperCenter;  // bottom
         if label_vertical = 2 then TextPosPanel.TextAlign:=taLowerCenter;  // top
      end;
      if label_horizontal=0 then // horizontal alignement is ts_Left
      begin
         if label_vertical = 1 then TextPosPanel.TextAlign:=taCenterRight; // center
         if label_vertical = 0 then TextPosPanel.TextAlign:=taUpperRight; // bottom
         if label_vertical = 2 then TextPosPanel.TextAlign:=taLowerRight;  // top
      end;
      if label_horizontal = 2 then // horizontal alignement is ts_Right
      begin
         if label_vertical = 1 then TextPosPanel.TextAlign:=taCenterLeft;  // center
         if label_vertical = 0 then TextPosPanel.TextAlign:=taUpperLeft;   // top
         if label_vertical = 2 then TextPosPanel.TextAlign:=taLowerLeft;  // bottom
      end;
      TextPosPanel.Distance:=LabelOffset;

      // get font and size
      CurrentfontLabel.Caption:=LabelFont;
      CurrentSizeLabel.Caption:=inttostr(LabelSize);
      LabelAngleEdit.Text:=inttostr(LabelAngle);
      LabelOffsetEdit.Text:=inttostr(LabelOffset);


      if CurrTextFileDef.GetUseLabelLayer then
      begin
         LabelLayerCb.Checked:=TRUE;
         LabelLayerListCb.Text:=CurrTextFileDef.GetLabelLayer;
      end;

      SetupLabelTextSheet;
      CheckLabelTextTabPage;
   end;
   IgnoreDuplicatesCb.Checked:=CurrTextFileDef.GetIgnoreDuplicates;

   if CurrTextFileDef.GetDefaultSymbolName <> '' then
   begin
      DefaultSymLb.Text:=CurrTextFileDef.GetDefaultSymbolName;
      SetDefaultSymCb.Checked:=TRUE;
   end
   else
      SetDefaultSymCb.Checked:=FALSE;

   if CurrTextFileDef.GetObjectType <> 8 then
   begin
      DefaultSymLb.visible:=FALSE;
      SetDefaultSymCb.Checked:=FALSE;
      SetDefaultSymCb.visible:=FALSE;
   end;
   DefaultSymLb.enabled:=SetDefaultSymCb.Checked;

   CheckColNameTabPage;
   ColNameSheetModified:=FALSE;
end;

procedure TTextWizardWnd.SetupLabelTextSheet;
var colidx:integer;
    islabelmarkerassigned:boolean;
begin
   // method is used to setup the label-text sheet
   // check if one of the columns is a LabelMarker
   islabelmarkerassigned:=FALSE;
   for colidx:=0 to ColNameGrid.ColCount-1 do
   begin
      if ColNameGrid.Cells[ColIdx, 0] = LabelMarkerColDef then islabelmarkerassigned:=TRUE;
   end;

   if islabelmarkerassigned then // show the Panel to setup the label markers
   begin
      LabelPosCb.visible:=TRUE;
      LabelPosCb.Checked:=FALSE;
      LabelPosPanel.visible:=FALSE;
   end
   else
   begin
      LabelPosPanel.visible:=FALSE;
      LabelPosCb.Checked:=FALSE;
      LabelPosCb.Visible:=FALSE;
   end;

   LabelOffsetEdit.Enabled := (TextPosPanel.TextAlign <> taCenter);
   OffsetUpBtn.Enabled := (TextPosPanel.TextAlign <> taCenter);
   OffsetDownBtn.Enabled := (TextPosPanel.TextAlign <> taCenter);

   LabelLayerPanel.visible:=LabelLayerCb.Checked;
end;


procedure TTextWizardWnd.SepDefGridDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
   if FixedWideCb.Checked then
   begin
      SepDefGrid.Canvas.Font.Pitch:=fpFixed;
      SepDefGrid.Font.Pitch:=fpFixed
   end
   else
   begin;
      SepDefGrid.Font.Pitch:=fpVariable;
      SepDefGrid.Canvas.Font.Pitch:=fpVariable;
   end;

   with SepDefGrid do
   begin
      Canvas.FillRect(Rect);
      if ((UseSeperatorCb.Checked) and (SepDefGrid.ColCount>1)) then
      begin
         if Arow>0 then
            DrawText(Canvas.Handle, PChar(Cells[ACol, ARow]),
                     Length(Cells[ACol, ARow]), Rect, (dt_Center) or (dt_VCenter))
         else
            DrawText(Canvas.Handle, PChar(Cells[ACol, ARow]),
                     Length(Cells[ACol, ARow]), Rect, (dt_Center) or (dt_VCenter))
      end
      else
      begin
         DrawText(Canvas.Handle, PChar(Cells[ACol, ARow]),
         Length(Cells[ACol, ARow]), Rect, (dt_Left) or (dt_VCenter))
      end;
   end;
end;

procedure TTextWizardWnd.FormCreate(Sender: TObject);
var i:integer;
begin
  SystemDelimeter := '';
  // Base point of the label is in the middle as default
  TextPosPanel.TextAlign:=taCenter;
  // still no delimeters defined
  DelimCount:=0;
  OldDelimPos:=0;
  for i:=1 to 100 do // reset the delemiters    
      Delimeters[i]:=10000;
  ObjectTypeFilter:=-1;
  UsedObjectType:=-1;
  SeperatorSheetModified:=FALSE;
  ColumnDefSheetModified:=FALSE;
  LabelTextSheetModified:=FALSE;
  ColNameSheetModified:=FALSE;
  SelTableSheetModified:=FALSE;
  SuppressAutomaticColAssignment:=FALSE;
  FirstShow:=TRUE;
  OpenDialogsModuleWnd:=nil;
  StrfileObj:=nil;
  DataBaseHeaderColumns:=nil;
  CurrModalResult:=mrCancel;
  UsedAccessTablename:='';

end;

function TTextWizardWnd.GetModalResult:integer;
begin
   result:=CurrModalResult;
end;

procedure TTextWizardWnd.Prev2BtnClick(Sender: TObject);
begin
   PageControl1.Pages[0].TabVisible:=FALSE;  // ColumnDefSheet will not be shown
   PageControl1.Pages[1].TabVisible:=FALSE;  // LabelTextSheet will not be shown
   PageControl1.Pages[2].TabVisible:=TRUE;   // SeperatorSheet will be shown
   PageControl1.Pages[3].TabVisible:=FALSE;  // ColNameSheet will not be shown
   PageControl1.Pages[4].TabVisible:=FALSE;  // SaveDefSheet will not be shown
   PageControl1.ActivePage:=SeperatorSheet;  // show the SeperatorSheet now
   ColumnDefSheetModified:=FALSE;
end;

procedure TTextWizardWnd.SepDefCanvasPaint(Sender: TObject);
begin
   SepDefGrid.Repaint;
   RedrawColumnDefPage;
end;

procedure TTextWizardWnd.SepDefCanvasMouseDown(Sender: TObject; Button: TMouseButton;  Shift: TShiftState; X, Y: Integer);
var i , J:integer;
    X1:integer;
begin
   for i:= 1 to DelimCount do
   begin
      if ((Delimeters[i]-CharPixelWidth)<=X ) and  ((Delimeters[i]+CharPixelWidth)>=X) then
      begin
         if ssDouble in Shift then //DoubleClick in Shift -> delete delemiter
         begin //Delete It
            for j:=i to DelimCount-1 do
               Delimeters[j]:=Delimeters[j+1];
            Dec(DelimCount);
            CurrentDelim:=0;
            SepDefGrid.Repaint;
            RedrawColumnDefPage; // redraw the column definition page
            CheckColumnDefTabPage;
            ColumnDefSheetModified:=TRUE;
            Exit;
         end
         else
         begin
            if ssLeft in Shift then // existing delemiter selected
            begin
               CurrentDelim:=i;
               SepDefCanvas.Cursor:=crSizeWe; // change cursor to move seperator
               Exit;
            end;
         end;
      end;
   end;
   // new delimiter defined
   X1:=GetDelimiterPos(X); // get the position of the delimter
   Inc(DelimCount);
   Delimeters[DelimCount]:=X1;
   ColumnDefSheetModified:=TRUE;
   SepDefCanvas.Cursor:=crSizeWe;
   CurrentDelim:=DelimCount;
   SepDefGrid.Repaint;
   RedrawColumnDefPage;
   CheckColumnDefTabPage;
end;

procedure TTextWizardWnd.DrawSepDefCanvas;
var i:integer;
    AHeight:integer;
    Pos, Max: Integer;
    ScrollInfo: TScrollInfo;
    Amin,Amax,Apos:integer;
begin
   // method is used to draw the Seperator Definition Canvas ( used to set fixed column widths)
   Apos:= GetScrollPos(SepDefGrid.Handle, SB_HORZ)-5;
   if Apos < 0 then Apos:=0;
   with SepDefCanvas.Canvas do
   begin
      Pen.Color:=clBtnFace;
      Brush.Color:=clBtnFace;
      FillRect(Rect(0,0,SepDefCanvas.Width,SepDefCanvas.Height));

      Pen.Color:=clBlack;
      Pen.Style:=psSolid;
      Pen.Width:=1;
      MoveTo(0,20);
      LineTo(SepDefCanvas.Width,20);
      Pen.Width:=1;

      Pen.Color:=clBlack;
      for i:= 1 to SepDefCanvas.Width do
      begin
         if (frac(i/5)=0) and (frac(i/10)<>0) then
         begin
            Moveto(i*CharPixelWidth,14);
            LineTo(i*CharPixelWidth,20);
         end
         else
         if frac(i/10)=0 then
         begin
            Moveto(i*CharPixelWidth,11);
            LineTo(i*CharPixelWidth,20);
            Font.Color:=clNavy;
            TextOut(i*CharPixelWidth-3,-3,IntToStr(i));
         end
         else
         begin
            Moveto(i*CharPixelWidth,17);
            LineTo(i*CharPixelWidth,20);
         end;
      end;
   end;
end;

Procedure TTextWizardWnd.RedrawColumnDefPage;
begin
   DrawSepDefCanvas; // draw the seperator canvas
   DrawDelimiters;   // draw the delimiters
end;

procedure TTextWizardWnd.SepDefCanvasMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var NewDelimPos,i :integer;
begin
   if not (SSLeft in Shift) then
   begin
      for i:= 1 to DelimCount do
      begin
         if ((Delimeters[i]-1)<=X) and  ((Delimeters[i]+1)>=X) then
         begin
            SepDefCanvas.Cursor:=crHSplit;
            Exit;
         end;
      end;
      SepDefCanvas.Cursor:=crDefault;
      Exit;
   end;

   if Abs(OldDelimPos-x)<CharPixelWidth then Exit;
   if  (SSleft in Shift) then
   begin
      NewDelimPos:=GetDelimiterPos(X); // get the new position of the delemiter
      if ((NewDelimPos div CharPixelWidth) > MaxDataLineLen) then // to avoid moving the delimiter behind the maximum allowed position
         NewDelimPos:=MaxDataLineLen*CharPixelWidth;
      OldDelimPos:=NewDelimPos;
      Delimeters[CurrentDelim]:=NewDelimPos; // set the new delimiter
      ColumnDefSheetModified:=TRUE;
      SepDefGrid.Repaint;
      RedrawColumnDefPage;
   end;
end;


function TTextWizardWnd.GetDelimiterPos(X:Integer):integer;
var i:integer;
begin
   // method is used to calculate the position of the delimiter
   // in the SepDefCanvas
   i:=2;
   while i*CharPixelWidth< X do
     Inc(i);

   if (i*CharPixelWidth)-x > x-(i-1)*CharPixelWidth then
      Result:= (i-1)*CharPixelWidth
   else
      Result:= i*CharPixelWidth;
end;


procedure TTextWizardWnd.DrawDelimiters;
var i, x1:integer;
begin
   // method is used to draw the delimiters if fixed columns are set
   for i:=1 to DelimCount do
   begin
      X1:=Delimeters[i];
      with SepDefCanvas.Canvas do
      begin
         Moveto(X1,1);
         Pen.Width:=1;
         Pen.Style:=psDot;
         Pen.Color:=clRed;
         LineTo(x1,Height-1);
         Pen.Style:=psSolid;
         Pen.Color:=clBlack;
      end;
      with SepDefGrid.Canvas do
      begin
         Moveto(X1,1);
         Pen.Width:=1;
         Pen.Style:=psDot;
         Pen.Color:=clRed;
         LineTo(x1,1000);
      end;
   end;
   with SepDefGrid.Canvas do
   begin
      Pen.Style:=psSolid;
   end;
end;

procedure TTextWizardWnd.SepDefCanvasMouseUp(Sender: TObject; Button: TMouseButton;  Shift: TShiftState; X, Y: Integer);
begin
   SepDefCanvas.Cursor:=crDefault;
end;

procedure TTextWizardWnd.SepDefGridMouseMove(Sender: TObject; Shift: TShiftState;  X, Y: Integer);
begin
   SepDefCanvas.Cursor:=crDefault;
end;

procedure TTextWizardWnd.UseSeperatorCbClick(Sender: TObject);
begin
   if UseSeperatorCb.Checked then // a seperator should be selected
   begin
      // enable the seperator checkboxes
      TabSepCb.Enabled:=TRUE; TabSepCb.Checked:=FALSE;
      CommaSepCb.Enabled:=TRUE; CommaSepCb.Checked:=FALSE;
      SpaceSepCb.Enabled:=TRUE; SpaceSepCb.Checked:=FALSE;
      SemicolonSepCb.Enabled:=TRUE; SemicolonSepCb.Checked:=FALSE;

      UserSepCb.Enabled:=TRUE; UserSepCb.Checked:=FALSE; UserSepEdit.Visible:=FALSE;
      // disable the Fixed Wide Cb
      FixedWideCb.Checked:=FALSE;
   end
   else
   begin
      // disable the seperator checkboxes
      TabSepCb.Enabled:=FALSE; TabSepCb.Checked:=FALSE;
      CommaSepCb.Enabled:=FALSE; CommaSepCb.Checked:=FALSE;
      SpaceSepCb.Enabled:=FALSE; SpaceSepCb.Checked:=FALSE;
      SemicolonSepCb.Enabled:=FALSE; SemicolonSepCb.Checked:=FALSE;
      UserSepCb.Enabled:=FALSE; UserSepCb.Checked:=FALSE; UserSepEdit.Visible:=FALSE;
   end;
   SeperatorSheetModified:=TRUE;
   CheckSeperatorTabPage;
end;

procedure TTextWizardWnd.CheckColNameTabPage;
var allok:boolean;
    xassigned:boolean;
    yassigned:boolean;
    isassigned:boolean;
    colidx:integer;
    labelassigned:boolean;
    labelmarkerassigned:boolean;
begin
   allok:=FALSE;
   // check if there is at least the X and Y column assigned
   xassigned:=FALSE;
   yassigned:=FALSE;
   labelassigned:=FALSE;
   labelmarkerassigned:=FALSE;
   for colidx:=0 to ColNameGrid.ColCount-1 do
   begin
      if ColNameGrid.Cells[ColIdx, 0] = XColDef then xassigned:=TRUE;
      if ColNameGrid.Cells[ColIdx, 0] = YColDef then yassigned:=TRUE;
      if ColNameGrid.Cells[ColIdx, 0] = LabelColDef then labelassigned:=TRUE;
      if ColNameGrid.Cells[ColIdx, 0] = LabelMarkercolDef then labelmarkerassigned:=TRUE;
   end;

   if (labelmarkerassigned and not labelassigned) then
   begin
      // labelmarker can only be set if label is set
      Next3Btn.Enabled:=FALSE;
      exit;
   end;

   if not (xassigned and yassigned) then
   begin
      // X and Y always have to be set
      Next3Btn.Enabled:=FALSE;
      exit;
   end;

   // check depended of the used object-type if the page is correct setuped
   if PointBtn.Flat then
   begin
      // a point only needs X and Y
      allok:=TRUE;
   end
   else if PolyBtn.Flat then
   begin
      // a poly only needs X and Y
      allok:=TRUE;
   end
   else if CPolyBtn.Flat then
   begin
      // a CPoly only needs X and Y
      allok:=TRUE;
   end
   else if CircleBtn.Flat then
   begin
      // the circle needs at least also the radius
      allok:=TRUE;
      isassigned:=FALSE;
      for colidx:=0 to ColNameGrid.ColCount-1 do
      begin
         if ColNameGrid.Cells[ColIdx, 0] = RadiusColDef then begin isassigned:=TRUE; break; end;
      end;
      if allok and not isassigned then
         allok:=FALSE;
   end
   else if SymbolBtn.Flat then
   begin
      // the symbol only needs X and Y
      // if no symbol-name or symbol-index is set the current set symbol will be used
      allok:=TRUE;
   end
   else if AdfBtn.Flat then
   begin
      allok:=TRUE;
      // check if Parameter A is set
      isassigned:=FALSE;
      for colidx:=0 to ColNameGrid.ColCount-1 do
      begin
         if ColNameGrid.Cells[ColIdx, 0] = ParamAColDef then begin isassigned:=TRUE; break; end;
      end;
      if allok and not isassigned then
         allok:=FALSE;

      // check if Parameter B is set
      isassigned:=FALSE;
      for colidx:=0 to ColNameGrid.ColCount-1 do
      begin
         if ColNameGrid.Cells[ColIdx, 0] = ParamBColDef then begin isassigned:=TRUE; break; end;
      end;
      if allok and not isassigned then
         allok:=FALSE;

      // check if Parameter C is set
      isassigned:=FALSE;
      for colidx:=0 to ColNameGrid.ColCount-1 do
      begin
         if ColNameGrid.Cells[ColIdx, 0] = ParamCColDef then begin isassigned:=TRUE; break; end;
      end;
      if allok and not isassigned then
         allok:=FALSE;

      // check if Layer is set
      isassigned:=FALSE;
      for colidx:=0 to ColNameGrid.ColCount-1 do
      begin
         if ColNameGrid.Cells[ColIdx, 0] = LayerColDef then begin isassigned:=TRUE; break; end;
      end;
      if allok and not isassigned then
         allok:=FALSE;

      // check if Object-Type is set
      isassigned:=FALSE;
      for colidx:=0 to ColNameGrid.ColCount-1 do
      begin
         if ColNameGrid.Cells[ColIdx, 0] = ObjectTypeColDef then begin isassigned:=TRUE; break; end;
      end;
      if allok and not isassigned then
         allok:=FALSE;

      // check if Object-Nr is set
      isassigned:=FALSE;
      for colidx:=0 to ColNameGrid.ColCount-1 do
      begin
         if ColNameGrid.Cells[ColIdx, 0] = ObjectNrColDef then begin isassigned:=TRUE; break; end;
      end;
      if allok and not isassigned then
         allok:=FALSE;
   end
   else if GpsBtn.Flat then
   begin
      allok:=TRUE;
   end;

   Next3Btn.Enabled := allok;
   if DbColNameEdit.Visible then Next3Btn.Enabled := FALSE;

end;

procedure TTextWizardWnd.CheckColumnDefTabPage;
var allok:boolean;
begin
   allok:=FALSE;
   if (UseSeperatorCb.Checked) then
   begin
      // check if there are at least two columns defined
      if SepDefGrid.ColCount >= 2 then
         allok:=TRUE;
   end
   else if (FixedWideCb.Checked) then
   begin
      // check if there are at least two delemiters set (X and Y Coordinate)
      if (DelimCount >= 2) then
         allok:=TRUE;
   end;
   if allok then
      Next2Btn.Enabled:=TRUE
   else
      Next2Btn.Enabled:=FALSE;
end;

procedure TTextWizardWnd.CheckSeperatorTabPage;
var allok:boolean;
begin
   allok:=FALSE;
   if (UseSeperatorCb.Checked) then
   begin
      if (TabSepCb.Checked) then
         allok:=TRUE
      else if (CommaSepCb.Checked) then
         allok:=TRUE
      else if (SpaceSepCb.Checked) then
         allok:=TRUE
      else if (SemicolonSepCb.Checked) then
         allok:=TRUE
      else if (UserSepCb.Checked) then
      begin
         if (UserSepEdit.Text <> '') then
            allok:=TRUE;
      end;
   end;

   if FixedWideCb.Checked then
      allok:=TRUE;

   if allok then
      Next1Btn.Enabled:=TRUE
   else
      Next1Btn.Enabled:=FALSE;
end;

procedure TTextWizardWnd.TabSepCbClick(Sender: TObject);
begin
   if TabSepCb.Checked then
   begin
      // setup other checkboxes
      CommaSepCb.Checked:=FALSE;
      SpaceSepCb.Checked:=FALSE;
      SemicolonSepCb.Checked:=FALSE;
      UserSepCb.Checked:=FALSE;
      UserSepEdit.Visible:=FALSE;
      // set the delimiter
      SystemDelimeter := chr(9);
   end;
   SeperatorSheetModified:=TRUE;
   CheckSeperatorTabPage;
end;

procedure TTextWizardWnd.CommaSepCbClick(Sender: TObject);
begin
   if CommaSepCb.Checked then
   begin
      // setup other checkboxes
      TabSepCb.Checked:=FALSE;
      SpaceSepCb.Checked:=FALSE;
      SemicolonSepCb.Checked:=FALSE;
      UserSepCb.Checked:=FALSE;
      UserSepEdit.Visible:=FALSE;
      // set the delimiter
      SystemDelimeter := ',';
   end;
   SeperatorSheetModified:=TRUE;
   CheckSeperatorTabPage;
end;

procedure TTextWizardWnd.SpaceSepCbClick(Sender: TObject);
begin
   if SpaceSepCb.Checked then
   begin
      // setup other checkboxes
      TabSepCb.Checked:=FALSE;
      CommaSepCb.Checked:=FALSE;
      SemicolonSepCb.Checked:=FALSE;
      UserSepCb.Checked:=FALSE;
      UserSepEdit.Visible:=FALSE;
      // set the delimiter
      SystemDelimeter := ' ';
   end;
   SeperatorSheetModified:=TRUE;
   CheckSeperatorTabPage;
end;

procedure TTextWizardWnd.Next1BtnClick(Sender: TObject);
var i:integer;
begin
   SaveDefFileBtn.Enabled := TRUE;
   
   if UseSeperatorCb.Checked then
   begin
      DelimCount:=0; // if seperator character is used remove the delimiters
      for i:=1 to 100 do // reset the delemiters
         Delimeters[i]:=10000;
      SepDefCanvas.visible:=FALSE;
      SepDefGrid.Top:=0;
   end
   else
   begin
      SepDefCanvas.visible:=TRUE;
      SepDefGrid.Top:=24;
   end;

   // if the seperator Sheet has been modified all following pages are modified
   if SeperatorSheetModified then
   begin
      ColumnDefSheetModified:=TRUE;
      ColNameSheetModified:=TRUE;
      LabelTextSheetModified:=TRUE;
   end;

   // setup the columndef-sheet
   SetupColumnDefSheet;

   PageControl1.Pages[0].TabVisible:=TRUE;  // ColumnDefSheet will not be shown
   PageControl1.Pages[1].TabVisible:=FALSE; // LabelTextSheet will not be shown
   PageControl1.Pages[2].TabVisible:=FALSE; // SeperatorSheet will be shown
   PageControl1.Pages[3].TabVisible:=FALSE; // ColNameSheet will not be shown
   PageControl1.Pages[4].TabVisible:=FALSE;  // SaveDefSheet will not be shown
   PageControl1.ActivePage:=ColumnDefSheet; // show the ColumnDefSheet now
   CheckColumnDefTabPage;
   SeperatorSheetModified:=FALSE;
end;

procedure TTextWizardWnd.UserSepCbClick(Sender: TObject);
begin
   if UserSepCb.Checked then
   begin
      // setup other checkboxes
      TabSepCb.Checked:=FALSE;
      CommaSepCb.Checked:=FALSE;
      SpaceSepCb.Checked:=FALSE;
      SemicolonSepCb.Checked:=FALSE;
      UserSepEdit.Visible:=TRUE;
   end;
   SeperatorSheetModified:=TRUE;
   CheckSeperatorTabPage;
end;

procedure TTextWizardWnd.UserSepEditChange(Sender: TObject);
begin
   SystemDelimeter:=UserSepEdit.Text;
   SeperatorSheetModified:=TRUE;
   CheckSeperatorTabPage;
end;

procedure TTextWizardWnd.FixedWideCbClick(Sender: TObject);
begin
   if FixedWideCb.Checked then
   begin
      // disable the seperator checkboxes
      UseSeperatorCb.Checked:=FALSE;
      TabSepCb.Enabled:=FALSE; TabSepCb.Checked:=FALSE;
      CommaSepCb.Enabled:=FALSE; CommaSepCb.Checked:=FALSE;
      SpaceSepCb.Enabled:=FALSE; SpaceSepCb.Checked:=FALSE;
      UserSepCb.Enabled:=FALSE; UserSepCb.Checked:=FALSE; UserSepEdit.Visible:=FALSE;
   end;
   CheckSeperatorTabPage;
end;

procedure TTextWizardWnd.Cancel1BtnClick(Sender: TObject);
begin
   ModalResult:=mrCancel;
   // check if there is a filter set -> in this case reset the file definition
   if ObjectTypeFilter <> -1 then
      CurrTextFileDef.Reset;
   Self.Close;   
end;

procedure TTextWizardWnd.Next2BtnClick(Sender: TObject);
begin
   SaveDefFileBtn.Enabled := TRUE;
   if FixedWideCb.Checked then
      CurrTextFileDef.SortIt(Delimeters);  // fixed wide column seperation has been selected the Delimeters have to be sorted first

   // if the ColumnDef Sheet has been modified all following pages are modified
   if ColumnDefSheetModified then
   begin
      ColNameSheetModified:=TRUE;
      LabelTextSheetModified:=TRUE;
      ColLineCb.Enabled:=FALSE;
      ColLineCb.Checked:=FALSE;
   end;

   // the Column-name Sheet has to be setuped
   SetupColNameSheet;

   PageControl1.Pages[0].TabVisible:=FALSE;  // ColumnDefSheet will not be shown
   PageControl1.Pages[1].TabVisible:=FALSE;  // LabelTextSheet will not be shown
   PageControl1.Pages[2].TabVisible:=FALSE;  // SeperatorSheet will not be shown
   PageControl1.Pages[3].TabVisible:=TRUE;   // ColNameSheet will be shown
   PageControl1.Pages[4].TabVisible:=FALSE;  // SaveDefSheet will not be shown
   PageControl1.ActivePage:=ColNameSheet;    // show the ColNameSheet now
   CheckColNameTabPage;
   ColumnDefSheetModified:=FALSE;
end;

procedure TTextWizardWnd.SetupFirstLineOfFile(ColIdx:integer; Token:string);
var FoundToken:string;
    i,oldidx:integer;
    ColNames:array of string;
begin
   if InputFileType = ft_TextFile then
   begin
      // only if a Textfile will be read it has to be checked if the first line should be
      // checked if it is a reserved column
      if (not ColLineCb.Checked) or (SuppressAutomaticColAssignment) then
         exit;
   end;
   // check if the token could be a column that representates a reserved column
   FoundToken:='';
   if MainWnd.CurTextFileDef.IsAutoAssignementColumn(Token,col_PolySeperator)    then // it is a poly seperator column
      FoundToken:=PolySeperatorColDef
   else if MainWnd.CurTextFileDef.IsAutoAssignementColumn(Token,col_Label)       then // it is a Label column
      FoundToken:=LabelColDef
   else if MainWnd.CurTextFileDef.IsAutoAssignementColumn(Token,col_LabelMarker) then // it is a Label marker column
      FoundToken:=LabelMarkerColDef
   else if MainWnd.CurTextFileDef.IsAutoAssignementColumn(Token,col_Layer)       then // it is a Layer column
      FoundToken:=LayerColDef
   else if MainWnd.CurTextFileDef.IsAutoAssignementColumn(Token,col_X)           then // it is a X column
      FoundToken:=XColDef
   else if MainWnd.CurTextFileDef.IsAutoAssignementColumn(Token,col_Y)           then // it is a Y column
      FoundToken:=YColDef
   else if MainWnd.CurTextFileDef.IsAutoAssignementColumn(Token,col_Radius)      then // it is a Radius column
      FoundToken:=RadiusColDef
   else if MainWnd.CurTextFileDef.IsAutoAssignementColumn(Token,col_SymbolName)  then // it is a Symbol name column
      FoundToken:=SymbolNameColDef
   else if MainWnd.CurTextFileDef.IsAutoAssignementColumn(Token, col_ProgisId)   then // it is a ProgisId column
      FoundToken:=ProgisIdColDef
   else if MainWnd.CurTextFileDef.IsAutoAssignementColumn(Token,col_SymbolSize)  then // it is a Symbol size column
      FoundToken:=SymbolSizeColDef
   else if MainWnd.CurTextFileDef.IsAutoAssignementColumn(Token,col_SymbolRotation)  then // it is a Symbol rotation column
      FoundToken:=SymbolRotationColDef
   else if MainWnd.CurTextFileDef.IsAutoAssignementColumn(Token,col_SymbolIndex) then // it is a Symbol name column
      FoundToken:=SymbolIdxColDef;

   if FoundToken = '' then
   begin
      // check if token is a reserved token
      // if it is not column can be used as database column
      if not ColNameIsSupportedColumnName(Token) then  // it�s a database column
         ColNameGrid.Cells[ColIdx,0]:=Token;
      exit;
   end;

   oldidx:=-1;
   // check if the token is in the current supported columns list
   for i:=0 to SupportedColsLb.Items.Count -1 do
   begin
      if SupportedColsLb.Items[i] = FoundToken then
      begin
         oldidx:=i;
         break;
      end;
   end;

   if oldidx < 0 then
      exit;

   // delete the Foundtoken from the list of the available objects
   ColNameGrid.Cells[ColIdx,0]:=FoundToken;
   SetLength(ColNames,SupportedColsLb.Items.Count);
   for i:=0 to SupportedColsLb.Items.Count-1 do
       ColNames[i]:=SupportedColsLb.Items[i];
   SupportedColsLb.Items.Clear;
   for i:=0 to High(ColNames) do
   begin
      if i <> oldidx then
         SupportedColsLb.Items.Add(ColNames[i]);
   end;
   SetLength(ColNames,0); // delete the ColNames
end;

procedure TTextWizardWnd.SetupColNameSheet;
var DataLine, Substring: ansistring;
    DataFile: textfile;
    ColIdx:integer;
    RowIdx:integer;
    ColWidths:array of integer;
    totoken:integer;
    FirstLineFromFile:boolean;
    ColName:string;
    ColumnName:widestring;
    ColumnInfo:widestring;
    ColumnLength:integer;
    i:integer;
begin
   // only if the page has been modified it has to be reread
   if not ColNameSheetModified then exit;
   DbColNameEdit.Visible:=FALSE;
   DbColNameLabel.Visible:=FALSE;
   Screen.Cursor:=crHourGlass;
   ColNameGrid.Visible := false;

   if InputFileType <> ft_TextFile then // first line auto-column assignement is only supported for Textfiles
   begin
      ColLineCb.visible:=FALSE;
      AdfBtn.visible:=FALSE;
   end;

   // reset the Grid
   if (InputFileType = ft_TextFile) or (DataBaseHeaderColumns = nil) then
   begin
      for ColIdx := 0 to ColNameGrid.ColCount - 1 do
         for RowIdx := 0 to ColNameGrid.RowCount - 1 do
            ColNameGrid.Cells[ColIdx, RowIdx] := '';
   end;

    AdfBtn.Enabled := not UseSeperatorCb.Checked;

   // setup the supported columns and object-types
   if (InputFileType = ft_Textfile) then
   begin
      if (not ColLineCb.Checked) then
      begin
         PointBtn.Flat:=FALSE;
         PolyBtn.Flat:=FALSE;
         CPolyBtn.Flat:=FALSE;
         CircleBtn.Flat:=FALSE;
         SymbolBtn.Flat:=FALSE;
         AdfBtn.Flat:=FALSE;
         GpsBtn.Flat:=FALSE;
         SupportedColsLb.Items.Clear;
         SetupSupportedCols;
      end;
   end;

   // open the file
   if (InputFileType = ft_TextFile) then
   begin
      {$I-}
      AssignFile(DataFile, TxtFilename);
      Reset(DataFile);
      ColNameGrid.RowCount := 0;
      ColNameGrid.FixedCols := 0;
      ColNameGrid.ColCount := 1;
      FirstLineFromFile:=TRUE;
   end
   else if (InputFileType = ft_dBase) then
   begin
      if DataBaseHeaderColumns = nil  then
      begin
         DataBaseHeaderColumns:=TStringList.Create;
         // Open dBase table
         AdoConnection.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False';
         AdoConnection.Properties['Extended Properties'].Value := 'dBASE 5.0';
         AdoConnection.Properties['Data Source'].Value :=ExtractFileDir(TxtFilename);
         AdoConnection.Connected:=True;
         AdoTable.TableName:=ExtractFilename(TxtFilename);
         AdoTable.Open;
         // get the Table-header definition
         for i:= 0 to ADOTable.FieldCount - 1 do // add the columns to the column-list
         begin
            ColName:=ADOTable.Fields[i].FieldName;
            DataBaseHeaderColumns.Add(ColName);
         end;
         ColNameGrid.DefaultColWidth:=ColNameScrollBox.Width-2;
         ColNameGrid.RowCount := 2;  // one row for the header
         ColNameGrid.ColCount := 1;  // there is at least one column
         ColNameGrid.FixedRows := 1; // header row is fixed
         SupportedColsLb.Items.Clear;
         SetupSupportedCols;
      end;
   end
   else if (InputFileType = ft_Access) then
   begin
      if DataBaseHeaderColumns = nil  then
      begin
         DataBaseHeaderColumns:=TStringList.Create;
         // Open Access table
         if not AdoConnection.Connected then
         begin
            AdoConnection.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + TxtFilename;
            AdoConnection.Connected:=True;
         end;
         for i:=0 to DbTableLb.Items.Count-1 do
         begin
            if DbTableLb.Selected[i] then
            begin
               AdoTable.TableName:=DbTableLb.Items[i];
               AdoTable.Open;
            end;
         end;
         // get the Table-header definition
         for i:= 0 to ADOTable.FieldCount - 1 do // add the columns to the column-list
         begin
            ColName:=ADOTable.Fields[i].FieldName;
            DataBaseHeaderColumns.Add(ColName);
         end;
         ColNameGrid.DefaultColWidth:=ColNameScrollBox.Width-2;
         ColNameGrid.RowCount := 2;  // one row for the header
         ColNameGrid.ColCount := 1;  // there is at least one column
         ColNameGrid.FixedRows := 1; // header row is fixed
         SupportedColsLb.Items.Clear;
         SetupSupportedCols;
      end;
   end;
   try
      if (FixedWideCb.Checked) and (InputFileType = ft_TextFile) then // fixed columns set on TextFile  
      begin
         ColNameGrid.DefaultColWidth:=ColNameScrollBox.Width-2;
         RowIdx := 1;
         ColNameGrid.RowCount := 2;  // there is at least one data-row
         ColNameGrid.ColCount := 1;  // there is at least one column
         ColNameGrid.FixedRows := 1; // One Fixed Row to show number of column
         SetLength(ColWidths,0);

         while (not eof(DataFile)) and (RowIdx < 9) do // maximum 9 lines will be displayed
         begin
            Readln(DataFile, DataLine);
            ColIdx:=1;

            while ColIdx <= DelimCount do  // extract the tokens
            begin
               // get to token
               if ColIdx = 1 then
                  totoken:=Delimeters[ColIdx] div CharPixelWidth
               else
                  totoken:=(Delimeters[ColIdx] div CharPixelWidth) - (Delimeters[ColIdx-1] div CharPixelWidth);

               SubString := Copy(DataLine, 1, totoken); // we got the substring
               Delete(DataLine, 1, totoken);   // delete till next token

               // check if there are enough columns defined
               if ColNameGrid.ColCount < ColIdx then
                  ColNameGrid.ColCount := ColNameGrid.ColCount + 1;
               // check if there are enough rows defined
               if ColNameGrid.RowCount <= RowIdx then
                  ColNameGrid.RowCount := ColNameGrid.RowCount + 1;
               if FirstLineFromFile then
                  ColNameGrid.Cells[ColIdx-1, 0] := IntToStr(ColIdx); // write the column number to first row
               if High(ColWidths) < ColIdx-1 then
               begin
                  SetLength(ColWidths,ColIdx);
                  ColWidths[ColIdx-1]:=0;
               end;
               if length(SubString) > ColWidths[ColIdx-1] then
                  ColWidths[ColIdx-1]:=length(SubString);
               ColNameGrid.Cells[ColIdx-1, RowIdx]:=SubString;     // write SubString to Cell
               if FirstLineFromFile then
                  SetupFirstLineOfFile(ColIdx-1,SubString);
               ColIdx:=ColIdx+1; // add column index
            end;
            // add last token after seperator to Grid
            // check if there are enough columns defined
            if ColNameGrid.ColCount < ColIdx  then
               ColNameGrid.ColCount := ColNameGrid.ColCount + 1;
            // check if there are enough rows defined
            if ColNameGrid.RowCount <= RowIdx then
               ColNameGrid.RowCount := ColNameGrid.RowCount + 1;
            if FirstLineFromFile then
               ColNameGrid.Cells[ColIdx-1, 0] := IntToStr(ColIdx); // write the column number to first row
            if High(ColWidths) < ColIdx-1 then
            begin
               SetLength(ColWidths,ColIdx);
               ColWidths[ColIdx-1]:=0;
            end;
            if length(DataLine) > ColWidths[ColIdx-1] then
               ColWidths[ColIdx-1]:=length(DataLine);
            ColNameGrid.Cells[ColIdx-1, RowIdx]:=DataLine;      // write SubString to Cell
            if FirstLineFromFile then
               SetupFirstLineOfFile(ColIdx-1,DataLine);
            if FirstLineFromFile then
            begin
               if not ColLineCb.Checked then
                  Rowidx:=RowIdx+1;
            end
            else
               RowIdx:=RowIdx+1; // add row index
            FirstLineFromFile:=FALSE;
         end;
         // setup the column widths
         ColNameGrid.Width:=0;
         for ColIdx:=0 to High(ColWidths)do
         begin
            ColNameGrid.ColWidths[ColIdx]:=(ColWidths[ColIdx]*CharPixelWidth)+3;
            ColNameGrid.Width:=ColNameGrid.Width+((ColWidths[ColIdx]*CharPixelWidth) + 3);
         end;
         ColNameGrid.Width:=ColNameGrid.Width+CharPixelWidth;
         SetLength(ColWidths,0);
         ColNameGrid.Options:=ColNameGrid.Options+[goFixedVertLine, gofixedHorzLine ];
         if ColNameGrid.Width > ColNameScrollBox.Width then
         begin
            ColNameGrid.RowCount:=9;
            ColNameGrid.Height:=165;
         end
         else
            ColNameGrid.Height:=180;

      end
      else if (UseSeperatorCb.Checked) and (InputFileType = ft_TextFile) then // use seperator on TextFile
      begin
         ColNameGrid.DefaultColWidth:=ColNameScrollBox.Width-2;
         RowIdx := 1;
         ColNameGrid.RowCount := 2;  // there is at least one data-row
         ColNameGrid.ColCount := 1;  // there is at least one column
         ColNameGrid.FixedRows := 1; // One Fixed Row to show number of column
         SetLength(ColWidths,0);

         while (not eof(DataFile)) and (RowIdx <= 24) do // maximum 24 lines will be displayed
         begin
            Readln(DataFile, DataLine);
            ColIdx:=1;
            if (SpaceSepCb.Checked) then // there can be more spaces as seperator, so compress them
               Q_SpaceCompressInPlace(DataLine);
            while pos(SystemDelimeter, DataLine) <> 0 do  // extract the tokens
            begin
               SubString := Copy(DataLine, 1, pos(SystemDelimeter, DataLine) - 1); // we got the substring
               Delete(DataLine, 1, Length(SubString) + Length(SystemDelimeter));   // delete till next token

               // check if there are enough columns defined
               if ColNameGrid.ColCount < ColIdx then
                  ColNameGrid.ColCount := ColNameGrid.ColCount + 1;
               // check if there are enough rows defined
               if ColNameGrid.RowCount <= RowIdx then
                  ColNameGrid.RowCount := ColNameGrid.RowCount + 1;

               if FirstLineFromFile then
                  ColNameGrid.Cells[ColIdx-1, 0] := IntToStr(ColIdx); // write the column number to first row
               Trim(SubString);
               if High(ColWidths) < ColIdx-1 then
               begin
                  SetLength(ColWidths,ColIdx);
                  ColWidths[ColIdx-1]:=0;
               end;
               if length(SubString) > ColWidths[ColIdx-1] then
                  ColWidths[ColIdx-1]:=length(SubString);

               ColNameGrid.Cells[ColIdx-1, RowIdx]:=SubString;     // write SubString to Cell
               if FirstLineFromFile then
                  SetupFirstLineOfFile(ColIdx-1,SubString);
               ColIdx:=ColIdx+1; // add column index
            end;
            // add last token after seperator to Grid
            // check if there are enough columns defined
            if ColNameGrid.ColCount < ColIdx then
               ColNameGrid.ColCount := ColNameGrid.ColCount + 1;
            // check if there are enough rows defined
            if ColNameGrid.RowCount <= RowIdx then
               ColNameGrid.RowCount := ColNameGrid.RowCount + 1;
            if FirstLineFromFile then
               ColNameGrid.Cells[ColIdx-1, 0] := IntToStr(ColIdx); // write the column number to first row
            Trim(DataLine);
            if High(ColWidths) < ColIdx-1 then
            begin
               SetLength(ColWidths,ColIdx);
               ColWidths[ColIdx-1]:=0;
            end;
            if length(DataLine) > ColWidths[ColIdx-1] then
               ColWidths[ColIdx-1]:=length(DataLine);
            ColNameGrid.Cells[ColIdx-1, RowIdx]:=DataLine;      // write SubString to Cell
            if FirstLineFromFile then
               SetupFirstLineOfFile(ColIdx-1,DataLine);
            if FirstLineFromFile then
            begin
               if not ColLineCb.Checked then
                  Rowidx:=RowIdx+1;
            end
            else
               RowIdx:=RowIdx+1; // add row index
            FirstLineFromFile:=FALSE;
         end;
         // setup the column widths
         ColNameGrid.Width:=0;
         for ColIdx:=0 to High(ColWidths) do
         begin
            ColNameGrid.ColWidths[ColIdx]:=(ColWidths[ColIdx]*CharPixelWidth)+3;
            ColNameGrid.Width:=ColNameGrid.Width+((ColWidths[ColIdx]*CharPixelWidth) + 3);
         end;
         ColNameGrid.Width:=ColNameGrid.Width+CharPixelWidth;
         SetLength(ColWidths,0);
         ColNameGrid.Options:=ColNameGrid.Options+[goFixedVertLine, gofixedHorzLine ];
         if ColNameGrid.Width > ColNameScrollBox.Width then
         begin
            ColNameGrid.RowCount:=9;
            ColNameGrid.Height:=170;
         end
         else
            ColNameGrid.Height:=180;
      end
      else if (InputFileType = ft_dBase) or (InputFileType = ft_Access) then // it is a dBase table
      begin
         SetLength(ColWidths,0);
         RowIdx := 1;
         ColIdx:=1;
         // setup the first row by using the table header

         for i:= 0 to DataBaseHeaderColumns.Count - 1 do // add the columns to the column-list
         begin
            ColName:=DataBaseHeaderColumns[i];
            if High(ColWidths) < ColIdx-1 then
            begin
               SetLength(ColWidths,ColIdx);
               ColWidths[ColIdx-1]:=0;
            end;
            if length(Colname) > ColWidths[ColIdx-1] then
               ColWidths[ColIdx-1]:=length(Colname);
            ColNameGrid.Cells[ColIdx-1, 0]:=Colname;
            SetupFirstLineOfFile(ColIdx-1,Colname);
            ColIdx:=ColIdx+1;
         end;

         // check if the database has already been read
         if ADOTable.Active then
         begin
            while (not ADOTable.Eof) and (RowIdx <= 24) do
            begin
               ColIdx:=1;
               for i:= 0 to ADOTable.FieldCount-1 do
               begin
                  SubString:=ADOTable.FieldByName(ADOTable.Fields[i].FieldName).AsString;
                  // check if there are enough columns defined
                  if ColNameGrid.ColCount < ColIdx then
                     ColNameGrid.ColCount := ColNameGrid.ColCount + 1;
                  // check if there are enough rows defined
                  if ColNameGrid.RowCount <= RowIdx then
                     ColNameGrid.RowCount := ColNameGrid.RowCount + 1;
                  Trim(SubString);
                  if High(ColWidths) < ColIdx-1 then
                  begin
                     SetLength(ColWidths,ColIdx);
                     ColWidths[ColIdx-1]:=0;
                  end;
                  if length(SubString) > ColWidths[ColIdx-1] then
                     ColWidths[ColIdx-1]:=length(SubString);
                  ColNameGrid.Cells[ColIdx-1, RowIdx]:=SubString;     // write SubString to Cell
                  ColIdx:=ColIdx+1; // add column index
               end;
               RowIdx:=RowIdx+1; // add row index
               AdoTable.Next;    // get next entry of table
            end;
            // close table
            AdoTable.Close;
            // close connection
            AdoConnection.Close;
            AdoConnection.Connected:=false;
         end
         else
         begin
            // setup the column widths
            for RowIdx:=0 to ColNameGrid.RowCount-1 do
            begin
               for ColIdx:=0 to ColNameGrid.ColCount-1 do
               begin
                  SubString:=ColNameGrid.Cells[ColIdx, RowIdx];
                  if length(SubString) > ColWidths[ColIdx] then
                     ColWidths[ColIdx]:=length(SubString);
               end;
            end;
         end;

         // setup the column widths
         ColNameGrid.Width:=0;
         for ColIdx:=0 to High(ColWidths) do
         begin
            if ColIdx < ColNameGrid.ColCount then
            begin
               ColNameGrid.ColWidths[ColIdx]:=(ColWidths[ColIdx]*CharPixelWidth)+3;
               ColNameGrid.Width:=ColNameGrid.Width+((ColWidths[ColIdx]*CharPixelWidth) + 3);
            end;
         end;
         ColNameGrid.Width:=ColNameGrid.Width+CharPixelWidth;
         SetLength(ColWidths,0);
         ColNameGrid.Options:=ColNameGrid.Options+[goFixedVertLine, gofixedHorzLine ];
         if ColNameGrid.Width > ColNameScrollBox.Width then
         begin
            ColNameGrid.RowCount:=9;
            ColNameGrid.Height:=170;
         end
         else
            ColNameGrid.Height:=180;
      end;
   finally
      if (InputFileType = ft_TextFile) then
      begin
         CloseFile(DataFile);
         {$I+}
      end;
      ColNameGrid.Visible := true;
      Screen.Cursor:=crDefault;
   end;
   UsedObjectType:=-1;
end;

// setup the supported columns/Objecttype of the column-definition sheet
procedure TTextWizardWnd.SetupSupportedCols;
var ColIdx:integer;
begin
   // first check if there is a filter
   PointBtn.visible:=FALSE;
   PolyBtn.visible:=FALSE;
   CPolyBtn.visible:=FALSE;
   CircleBtn.visible:=FALSE;
   SymbolBtn.visible:=FALSE;
   AdfBtn.visible:=FALSE;
   GpsBtn.visible:=FALSE;

   // check if there is a filter set
   // in this case the object-selection buttons will not be shown
   if (ObjectTypeFilter <> -1) then
   begin
      ColTypeGroupBox.visible:=FALSE;
      SupportedColsLb.Top:=4;
      SupportedColsLb.Height:=160;
      ColLineCb.Enabled:=TRUE;
      if InputFileType <> ft_TextFile then
         SupportedColsLb.Height:=181;
   end
   else
   begin
      if InputFileType <> ft_TextFile then
         SupportedColsLb.Height:=101;
   end;

   if ObjectTypeFilter = 1 then // ot_Pixel
   begin
      PointBtn.visible:=TRUE;
      PointBtn.Flat:=TRUE;
      PointBtn.Enabled:=FALSE; // may not be changed
   end
   else if ObjectTypeFilter = 2 then // ot_Poly
   begin
      PolyBtn.visible:=TRUE;
      PolyBtn.Flat:=TRUE;
      PolyBtn.Enabled:=FALSE; // may not be changed
   end
   else if ObjectTypeFilter = 4 then // ot_CPoly
   begin
      CPolyBtn.visible:=TRUE;
      CPolyBtn.Flat:=TRUE;
      CPolyBtn.Enabled:=FALSE; // may not be changed
   end
   else if ObjectTypeFilter = 12 then // ot_Circle
   begin
      CircleBtn.visible:=TRUE;
      CircleBtn.Flat:=TRUE;
      CircleBtn.Enabled:=FALSE; // may not be changed
   end
   else if ObjectTypeFilter = 8 then // ot_Symbol
   begin
      SymbolBtn.visible:=TRUE;
      SymbolBtn.Flat:=TRUE;
      SymbolBtn.Enabled:=FALSE; // may not be changed
   end
   else if ObjectTypeFilter = 99 then // ADF-File
   begin
      AdfBtn.visible:=TRUE;
      AdfBtn.Flat:=TRUE;
      AdfBtn.Enabled:=FALSE; // may not be changed
   end
   else if ObjectTypeFilter = 98 then // GPS-File
   begin
      GpsBtn.Visible:=TRUE;
      GpsBtn.Flat:=TRUE;
      GpsBtn.Enabled:=FALSE; // may not be changed
   end
   else
   begin
      // no filter is set
      PointBtn.visible:=TRUE;
      PolyBtn.visible:=TRUE;
      CPolyBtn.visible:=TRUE;
      CircleBtn.visible:=TRUE;
      SymbolBtn.visible:=TRUE;
      if InputFileType = ft_TextFile then
      begin
         // Adf and Gps type only supported for Textfiles
         AdfBtn.visible:=TRUE;
         // GpsBtn.Visible:=TRUE; // Gps button only visible if filter is set for Gps-File
      end;
   end;

   // if any of the object-types is flat than enable the ColLineCb
   if (PointBtn.Flat or PolyBtn.Flat or CPolyBtn.Flat or CircleBtn.Flat or SymbolBtn.Flat) then
   begin
      ColLineCb.Enabled:=TRUE;
   end;

   // setup the supported columns
   if (PointBtn.Flat) and (UsedObjectType <> 1) then // setup for ot_Pixel
   begin
      // clear the headers of the columns
      for ColIdx:=0 to ColNameGrid.RowCount-1 do
          ColNameGrid.Cells[ColIdx, 0] := IntToStr(ColIdx+1); // write the column number to first row
      SupportedColsLb.Items.Clear;
      SupportedColsLb.Items.Add(XColDef);
      SupportedColsLb.Items.Add(YColDef);
      SupportedColsLb.Items.Add(LayerColDef);
      SupportedColsLb.Items.Add(LabelColDef);
      SupportedColsLb.Items.Add(LabelMarkerColDef);
      if InputFileType = ft_Access then // only for Access tables the progis id can be written back to table
         SupportedColsLb.Items.Add(ProgisIdColDef);
      SupportedColsLb.Items.Add(DatabaseColDef);
      UsedObjectType:=1;
      ColLineCb.Enabled:=TRUE;
   end
   else if (PolyBtn.Flat) and (UsedObjectType <> 2) then // setup of ot_Poly
   begin
      // clear the headers of the columns
      for ColIdx:=0 to ColNameGrid.RowCount-1 do
          ColNameGrid.Cells[ColIdx, 0] := IntToStr(ColIdx+1); // write the column number to first row
      SupportedColsLb.Items.Clear;
      SupportedColsLb.Items.Add(XColDef);
      SupportedColsLb.Items.Add(YColDef);
      SupportedColsLb.Items.Add(LayerColDef);
      SupportedColsLb.Items.Add(PolySeperatorColDef);
      // SupportedColsLb.Items.Add(LabelColDef); // not supported yet
      SupportedColsLb.Items.Add(DatabaseColDef);
      UsedObjectType:=2;
      ColLineCb.Enabled:=TRUE;
   end
   else if (CPolyBtn.Flat) and (UsedObjectType <> 4) then // setup for ot_CPoly
   begin
      // clear the headers of the columns
      for ColIdx:=0 to ColNameGrid.RowCount-1 do
          ColNameGrid.Cells[ColIdx, 0] := IntToStr(ColIdx+1); // write the column number to first row
      SupportedColsLb.Items.Clear;
      SupportedColsLb.Items.Add(XColDef);
      SupportedColsLb.Items.Add(YColDef);
      SupportedColsLb.Items.Add(LayerColDef);
      SupportedColsLb.Items.Add(PolySeperatorColDef);
      // SupportedColsLb.Items.Add(LabelColDef); // not supported yet
      SupportedColsLb.Items.Add(DatabaseColDef);
      UsedObjectType:=4;
      ColLineCb.Enabled:=TRUE;
   end
   else if (CircleBtn.Flat) and (UsedObjectType <> 12) then // setup for ot_Circle
   begin
      // clear the headers of the columns
      for ColIdx:=0 to ColNameGrid.RowCount-1 do
          ColNameGrid.Cells[ColIdx, 0] := IntToStr(ColIdx+1); // write the column number to first row
      SupportedColsLb.Items.Clear;
      SupportedColsLb.Items.Add(XColDef);
      SupportedColsLb.Items.Add(YColDef);
      SupportedColsLb.Items.Add(RadiusColDef);
      SupportedColsLb.Items.Add(LayerColDef);
      // SupportedColsLb.Items.Add(LabelColDef); // not supported yet
      SupportedColsLb.Items.Add(DatabaseColDef);
      UsedObjectType:=12;
      ColLineCb.Enabled:=TRUE;
   end
   else if (SymbolBtn.Flat) and (UsedObjectType <> 8) then // setup for ot_Symbol
   begin
      // clear the headers of the columns
      for ColIdx:=0 to ColNameGrid.RowCount-1 do
          ColNameGrid.Cells[ColIdx, 0] := IntToStr(ColIdx+1); // write the column number to first row
      SupportedColsLb.Items.Clear;
      SupportedColsLb.Items.Add(XColDef);
      SupportedColsLb.Items.Add(YColDef);
      SupportedColsLb.Items.Add(SymbolNameColDef);
      SupportedColsLb.Items.Add(SymbolSizeColDef);
      SupportedColsLb.Items.Add(SymbolRotationColDef);
      SupportedColsLb.Items.Add(SymbolIdxColDef);
      SupportedColsLb.Items.Add(LayerColDef);
      SupportedColsLb.Items.Add(LabelColDef);
      SupportedColsLb.Items.Add(LabelMarkerColDef);
      if InputFileType = ft_Access then // only for Access tables the progis id can be written back to table
         SupportedColsLb.Items.Add(ProgisIdColDef);
      SupportedColsLb.Items.Add(DatabaseColDef);
      UsedObjectType:=8;
      ColLineCb.Enabled:=TRUE;
   end
   else if (AdfBtn.Flat) and (UsedObjectType <> 99) then // setup for ADF-File
   begin
      for ColIdx:=0 to ColNameGrid.RowCount-1 do
          ColNameGrid.Cells[ColIdx, 0] := IntToStr(ColIdx+1); // write the column number to first row
      SupportedColsLb.Items.Clear;
      SupportedColsLb.Items.Add(XColDef);
      SupportedColsLb.Items.Add(YColDef);
      SupportedColsLb.Items.Add(ParamAColDef);
      SupportedColsLb.Items.Add(ParamBColDef);
      SupportedColsLb.Items.Add(ParamCColDef);
      SupportedColsLb.Items.Add(LayerColDef);
      SupportedColsLb.Items.Add(ObjectTypeColDef);
      SupportedColsLb.Items.Add(ObjectNrColDef);
      SupportedColsLb.Items.Add(GrafikColDef);
      SupportedColsLb.Items.Add(DatabaseColDef);
      UsedObjectType:=99;
   end
   else if (GpsBtn.Flat) and (UsedObjectType <> 98) then // setup for GPS-File
   begin
      for ColIdx:=0 to ColNameGrid.RowCount-1 do
          ColNameGrid.Cells[ColIdx, 0] := IntToStr(ColIdx+1); // write the column number to first row
      SupportedColsLb.Items.Clear;
      SupportedColsLb.Items.Add(XColDef);
      SupportedColsLb.Items.Add(YColDef);
      SupportedColsLb.Items.Add(GpsSymDirColDef);
      SupportedColsLb.Items.Add(GpsSymLineTypeColDef);
      SupportedColsLb.Items.Add(GpsSymLineColorColDef);
      SupportedColsLb.Items.Add(GpsSymFillTypeColDef);
      SupportedColsLb.Items.Add(GpsSymFillcolorColDef);
      SupportedColsLb.Items.Add(GpsLineTypeColDef);
      SupportedColsLb.Items.Add(GpsLineColorColDef);
      SupportedColsLb.Items.Add(GpsLineWidthColDef);
      UsedObjectType:=98;
   end;
end;


procedure TTextWizardWnd.ColNameGridDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
   ColNameGrid.Font.Pitch:=fpVariable;
   ColNameGrid.Canvas.Font.Pitch:=fpVariable;

   with ColNameGrid do
   begin
      Canvas.FillRect(Rect);
      if (ColNameGrid.ColCount>1) then
      begin
         if Arow>0 then
            DrawText(Canvas.Handle, PChar(Cells[ACol, ARow]),
                     Length(Cells[ACol, ARow]), Rect, (dt_Center) or (dt_VCenter))
         else
            DrawText(Canvas.Handle, PChar(Cells[ACol, ARow]),
                     Length(Cells[ACol, ARow]), Rect, (dt_Center) or (dt_VCenter))
      end
      else
      begin
         DrawText(Canvas.Handle, PChar(Cells[ACol, ARow]),
         Length(Cells[ACol, ARow]), Rect, (dt_Left) or (dt_VCenter))
      end;
   end;
end;

procedure TTextWizardWnd.ColNameGridDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
  var DragDropRow : integer;
begin
   Accept:=TRUE;
   if (State = dsDragEnter) then  ColNameGrid.MouseToCell(X, Y, DragDropCol, DragDropRow);

end;

procedure TTextWizardWnd.SupportedColsLbEndDrag(Sender, Target: TObject; X,
  Y: Integer);
var CurRow, CurCol:integer;
    i,maxlen      :integer;
    token         :string;
    oldlen        :integer;
    ColNames      :array of string;
    oldidx        :integer;
    oldcolname    :string;
    aRect         :TRect;
    aPoint1       :TPoint;
    aPoint2       :TPoint;
begin
   if (Target = ColNameGrid) then
   begin
      ColNameGrid.MouseToCell(X, Y, CurCol, CurRow);
      if CurCol < 0 then exit;

      ColNameSheetModified:=TRUE;
      oldidx:=SupportedColsLb.ItemIndex;
      if oldidx < 0 then
         exit;
      oldcolname:=ColNameGrid.Cells[CurCol, 0];
      token:=SupportedColsLb.Items[SupportedColsLb.ItemIndex];
      ColNameGrid.Cells[CurCol, 0]:=token;
      if token <> DatabaseColDef then
      begin
         // only if the token is not a database-column definition it has to be removed
         // out of the supported columns
         // remove the selected item that has been draged/dropped out of the SupportedColsLb
         SetLength(ColNames,SupportedColsLb.Items.Count);
         for i:=0 to SupportedColsLb.Items.Count-1 do
            ColNames[i]:=SupportedColsLb.Items[i];
         SupportedColsLb.Items.Clear;
         for i:=0 to High(ColNames) do
         begin
            if i <> oldidx then
               SupportedColsLb.Items.Add(ColNames[i]);
         end;
         SetLength(ColNames,0); // delete the ColNames
      end;
      // check the old-column name, if it is already a column-assignement, put it back to columns
      if ColNameIsSupportedColumnName(oldcolname) then
         SupportedColsLb.Items.Add(oldcolname);
      // setup the width of the cell
      maxlen:=0;
      oldlen:=ColNameGrid.ColWidths[CurCol];
      for i:=0 to ColNameGrid.RowCount - 1 do
      begin
         token:=ColNameGrid.Cells[CurCol, i];
         if (length(token) > maxlen) then
            maxlen:=length(token);
      end;
      maxlen:=maxlen*CharPixelWidth;
      ColNameGrid.ColWidths[CurCol]:=maxlen;
      ColNameGrid.Width:=ColNameGrid.Width+(maxlen-oldlen);
      token:=ColNameGrid.Cells[CurCol, 0];
      if token = DatabaseColDef then
      begin
         DatabaseCol:=CurCol;
         DbColNameLabel.visible:=TRUE;
         DbColNameEdit.visible:=TRUE;
         Next3Btn.Enabled := FALSE;
         DbColNameEdit.Text:='';
         DbColNameEdit.SetFocus;
      end;
   end;
   CheckColNameTabPage;
end;

function TTextWizardWnd.ColNameIsSupportedColumnName(aColname:string):boolean;
var retVal:boolean;
begin
   retVal:=FALSE;
      // check if token is a reserved token
   if (aColname = LabelColDef) or
      (aColname = LabelMarkerColDef) or
      (aColname = LayerColDef) or
      (aColname = XColDef) or
      (aColname = YColDef) or
      (aColname = RadiusColDef) or
      (aColname = SymbolNameColDef) or
      (aColname = ProgisIdColDef) or
      (aColName = SymbolSizeColDef) or
      (aColName = SymbolRotationColDef) or
      (aColname = SymbolIdxColDef) or
      (aColname = ParamAColDef) or
      (aColname = ParamBColDef) or
      (aColname = ParamCColDef) or
      (aColname = ObjectTypeColDef) or
      (aColname = ObjectNrColDef) or
      (aColname = GrafikColDef) or
      (aColname = PolySeperatorColDef) or
      (aColname = GpsSymDirColDef) or
      (aColname = GpsSymLineTypeColDef) or
      (aColname = GpsSymLineColorColDef) or
      (aColname = GpsSymFillTypeColDef) or
      (aColname = GpsSymFillColorColDef) or
      (aColname = GpsLineTypeColDef) or
      (aColname = GpsLineColorColDef) or
      (aColname = GpsLineWidthColDef) or
      (aColname = DatabaseColDef) then
      retVal:=TRUE;
   result:=retVal;
end;

procedure TTextWizardWnd.SupportedColsLbDragOver(Sender, Source: TObject;
  X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
   Accept:=TRUE;
end;

procedure TTextWizardWnd.ColNameGridEndDrag(Sender, Target: TObject; X,
  Y: Integer);
var token         :string;
    dummy, count  :integer;
    oldlen:integer;
    maxlen:integer;
    i:integer;
    DragDropRow:integer;
begin
   if Target = SupportedColsLb then
   begin

   if DragDropCol < 0 then exit;

      ColNameSheetModified:=TRUE;
      // in DragDropCol and DragDropRow is the column that should be reset
      token:=ColNameGrid.Cells[DragDropCol, 0];
      val(token,dummy,count);
      if (count <> 0) then
      begin
         if (ColNameIsSupportedColumnName(token) and (token <> DatabaseColDef)) then
            SupportedColsLb.Items.Add(token);
            SupportedColsLb.Update;
            ColNameGrid.Cells[DragDropCol, 0]:=inttostr(DragDropCol+1);
      end;
    // STR ++
    if token = DatabaseColDef  then DBColNameEdit.Visible := False; 
    // STR --
      // setup the width of the cell
      oldlen:=ColNameGrid.ColWidths[DragDropCol];
      maxlen:=0;
      for i:=0 to ColNameGrid.RowCount - 1 do
      begin
         token:=ColNameGrid.Cells[DragDropCol, i];
         if (length(token) > maxlen) then
            maxlen:=length(token);
      end;
      maxlen:=maxlen*CharPixelWidth;
      ColNameGrid.ColWidths[DragDropCol]:=maxlen; // for seperator columns
      ColNameGrid.Width:=ColNameGrid.Width+(maxlen - oldlen);
   end;
   CheckColNameTabPage;
end;

procedure TTextWizardWnd.ColNameGridDragDrop(Sender, Source: TObject; X,
  Y: Integer);
var DragDropRow:integer;
begin

end;

procedure TTextWizardWnd.Prev3BtnClick(Sender: TObject);
begin
   if InputFileType = ft_TextFile then
   begin
      // setup the columndef-sheet
      SetupColumnDefSheet;
      PageControl1.Pages[0].TabVisible:=TRUE;   // ColumnDefSheet will be shown
      PageControl1.Pages[1].TabVisible:=FALSE;  // LabelTextSheet will not be shown
      PageControl1.Pages[2].TabVisible:=FALSE;  // SeperatorSheet will be shown
      PageControl1.Pages[3].TabVisible:=FALSE;  // ColNameSheet will not be shown
      PageControl1.Pages[4].TabVisible:=FALSE;  // SaveDefSheet will not be shown
      PageControl1.Pages[5].TabVisible:=FALSE;  // SelTableSheet will not be shown
      PageControl1.ActivePage:=ColumnDefSheet;  // show the ColumnDefSheet now
      CheckColumnDefTabPage;
      ColNameSheetModified:=FALSE;
   end
   else if InputFileType = ft_Access then
   begin
      // setup the columndef-sheet
      SetupColumnDefSheet;
      PageControl1.Pages[0].TabVisible:=FALSE;   // ColumnDefSheet will not be shown
      PageControl1.Pages[1].TabVisible:=FALSE;   // LabelTextSheet will not be shown
      PageControl1.Pages[2].TabVisible:=FALSE;   // SeperatorSheet will be shown
      PageControl1.Pages[3].TabVisible:=FALSE;   // ColNameSheet will not be shown
      PageControl1.Pages[4].TabVisible:=FALSE;   // SaveDefSheet will not be shown
      PageControl1.Pages[5].TabVisible:=TRUE;    // SelTableSheet will be shown
      PageControl1.ActivePage:=SelTableSheet;    // show the SelTableSheet now
      CheckSelTableTabPage;
      ColNameSheetModified:=FALSE;
   end;
end;

procedure TTextWizardWnd.Cancel3BtnClick(Sender: TObject);
begin
   ModalResult:=mrCancel;
   // check if there is a filter set -> in this case reset the file definition
   if ObjectTypeFilter <> -1 then
      CurrTextFileDef.Reset;
   Self.Close;
end;

procedure TTextWizardWnd.Next3BtnClick(Sender: TObject);
var colidx:integer;
    islabelassigned:boolean;
begin
   SaveDefFileBtn.Enabled := TRUE;
   // check if there is a label assigned
   islabelassigned:=FALSE;
   for colidx:=0 to ColNameGrid.ColCount-1 do
   begin
      if ColNameGrid.Cells[ColIdx, 0] = LabelColDef then islabelassigned:=TRUE;
   end;

   // if the page has been modified all following pages are modified
   if ColNameSheetModified then
   begin
      LabelTextSheetModified:=TRUE;
   end;

   if islabelassigned then
   begin
      // next page is LabelTextSheet
      SetupLabelTextSheet;
      CheckLabelTextTabPage;
      PageControl1.Pages[0].TabVisible:=FALSE;  // ColumnDefSheet will not be shown
      PageControl1.Pages[1].TabVisible:=TRUE;   // LabelTextSheet will not be shown
      PageControl1.Pages[2].TabVisible:=FALSE;  // SeperatorSheet will not be shown
      PageControl1.Pages[3].TabVisible:=FALSE;  // ColNameSheet will not be shown
      PageControl1.Pages[4].TabVisible:=FALSE;  // SaveDefSheet will not be shown
      PageControl1.ActivePage:=LabelTextSheet;  // show the LabelTextSheet now
   end
   else
   begin
      // next page is SaveDefSheet
      SetupSaveDefSheet;
      PageControl1.Pages[0].TabVisible:=FALSE;  // ColumnDefSheet will not be shown
      PageControl1.Pages[1].TabVisible:=FALSE;  // LabelTextSheet will not be shown
      PageControl1.Pages[2].TabVisible:=FALSE;  // SeperatorSheet will not be shown
      PageControl1.Pages[3].TabVisible:=FALSE;  // ColNameSheet will not be shown
      PageControl1.Pages[4].TabVisible:=TRUE;   // SaveDefSheet will be shown
      PageControl1.ActivePage:=SaveDefSheet;    // show the SaveDefSheet now
   end;
   ColNameSheetModified:=FALSE;
   DatabaseLabel.Caption := MainWnd.AXImpExpDbc.ImpDBInfo;
end;

procedure TTextWizardWnd.SetupSaveDefSheet;
var doshowdefaultsymbol:boolean;
    colidx:integer;
begin
   // ++ STR
   {

   if ObjectTypeFilter <> -1 then
   begin
      // if an object-type filter has been set the projection and database has to be set here
      // in the wizard due to no SetTxtFileWnd is used
      if DatabaseColumnsInColnameSheet then
         DatabaseBtn.Enabled:=TRUE
      else
         DatabaseBtn.Enabled:=FALSE;
         DatabaseLabel.Visible := FALSE;
   end
   else
   begin
      DatabaseBtn.visible:=FALSE;
   end;   

   }
   // Database button should alwas be visible

   DatabaseBtn.visible:=TRUE;

   // -- STR


   // check if there is a label assigned
   doshowdefaultsymbol:=FALSE;
   for colidx:=0 to ColNameGrid.ColCount-1 do
   begin
      if ColNameGrid.Cells[ColIdx, 0] = SymbolNameColDef then doshowdefaultsymbol:=TRUE;
      if ColNameGrid.Cells[ColIdx, 0] = SymbolIdxColDef  then doshowdefaultsymbol:=TRUE;
   end;

   if DefaultSymLb.Text = '' then // if there exist no Symbols in the Project -> do not show
      doshowdefaultsymbol:=FALSE;

   if doshowdefaultsymbol then
   begin
      SetDefaultSymCb.visible:=TRUE;
      DefaultSymLb.visible:=TRUE;
   end
   else
   begin
      SetDefaultSymCb.checked:=FALSE;
      SetDefaultSymCb.visible:=FALSE;
      DefaultSymLb.visible:=FALSE;
   end;
   DefaultSymLb.enabled:=SetDefaultSymCb.Checked;
end;

function TTextWizardWnd.DatabaseColumnsInColnameSheet:boolean;
var i,intval,count:integer;
begin
   for i:=0 to ColNameGrid.ColCount-1 do
   begin
      if ColNameGrid.Cells[i, 0]     = XColDef               then result:=FALSE
      else if ColNameGrid.Cells[i,0] = YColDef               then result:=FALSE
      else if ColNameGrid.Cells[i,0] = LabelColDef           then result:=FALSE
      else if ColNameGrid.Cells[i,0] = LabelMarkerColDef     then result:=FALSE
      else if ColNameGrid.Cells[i,0] = LayerColDef           then result:=FALSE
      else if ColNameGrid.Cells[i,0] = RadiusColDef          then result:=FALSE
      else if ColNameGrid.Cells[i,0] = SymbolNameColDef      then result:=FALSE
      else if ColNameGrid.Cells[i,0] = SymbolSizeColDef      then result:=FALSE
      else if ColNameGrid.Cells[i,0] = SymbolRotationColDef  then result:=FALSE
      else if ColNameGrid.Cells[i,0] = SymbolIdxColDef       then result:=FALSE
      else if ColNameGrid.Cells[i,0] = ParamAColDef          then result:=FALSE
      else if ColNameGrid.Cells[i,0] = ParamBColDef          then result:=FALSE
      else if ColNameGrid.Cells[i,0] = ParamCColDef          then result:=FALSE
      else if ColNameGrid.Cells[i,0] = ObjectTypeColDef      then result:=FALSE
      else if ColNameGrid.Cells[i,0] = ObjectNrColDef        then result:=FALSE
      else if ColNameGrid.Cells[i,0] = GrafikColDef          then result:=FALSE
      else if ColNameGrid.Cells[i,0] = PolySeperatorColDef   then result:=FALSE
      else if ColNameGrid.Cells[i,0] = ProgisIdColDef        then result:=FALSE
      else if ColNameGrid.Cells[i,0] = GpsSymDirColDef       then result:=FALSE
      else if ColNameGrid.Cells[i,0] = GpsSymLineTypeColDef  then result:=FALSE
      else if ColNameGrid.Cells[i,0] = GpsSymLineColorColDef then result:=FALSE
      else if ColNameGrid.Cells[i,0] = GpsSymFillTypeColDef  then result:=FALSE
      else if ColNameGrid.Cells[i,0] = GpsSymFillcolorColDef then result:=FALSE
      else if ColNameGrid.Cells[i,0] = GpsLineTypeColDef     then result:=FALSE
      else if ColNameGrid.Cells[i,0] = GpsLineColorColDef    then result:=FALSE
      else if ColNameGrid.Cells[i,0] = GpsLineWidthColDef    then result:=FALSE
      else
      begin
         // check if it is a database-column
         val(ColNameGrid.Cells[i,0],intval,count);
         if (count <> 0) then
         begin
            result:=TRUE;
            exit;
         end
         else
            result:=FALSE;
      end;
   end;
end;

procedure TTextWizardWnd.SetFontBtnClick(Sender: TObject);
begin
   if OpenDialogsModuleWnd <> nil then
      OpenDialogsModuleWnd.Free;
   OpenDialogsModuleWnd:=TOpenDialogsModuleWnd.Create(nil);
   OpenDialogsModuleWnd.ParentHandle:=Handle;
   OpenDialogsModuleWnd.OpenMode:=font_selection;
   OpenDialogsModuleWnd.OnClose:=OnOpenDialogsModuleWndClose;
   OpenDialogsModuleWnd.Show;
end;

procedure TTextWizardWnd.Cancel4BtnClick(Sender: TObject);
begin
   ModalResult:=mrCancel;
   // check if there is a filter set -> in this case reset the file definition
   if ObjectTypeFilter <> -1 then
      CurrTextFileDef.Reset;
   Self.Close;
end;

procedure TTextWizardWnd.Prev4BtnClick(Sender: TObject);
begin
   PageControl1.Pages[0].TabVisible:=FALSE;  // ColumnDefSheet will not be shown
   PageControl1.Pages[1].TabVisible:=FALSE;  // LabelTextSheet will not be shown
   PageControl1.Pages[2].TabVisible:=FALSE;  // SeperatorSheet will not be shown
   PageControl1.Pages[3].TabVisible:=TRUE;   // ColNameSheet will be shown
   PageControl1.Pages[4].TabVisible:=FALSE;  // SaveDefSheet will not be shown
   PageControl1.ActivePage:=ColNameSheet;    // show the ColNameSheet now
   CheckColNameTabPage;
   LabelTextSheetModified:=FALSE;
end;

procedure TTextWizardWnd.PointBtnClick(Sender: TObject);
begin
   PointBtn.Flat:=TRUE;
   PolyBtn.Flat:=FALSE;
   CPolyBtn.Flat:=FALSE;
   CircleBtn.Flat:=FALSE;
   SymbolBtn.Flat:=FALSE;
   AdfBtn.Flat:=FALSE;
   GpsBtn.Flat:=FALSE;
   SetupSupportedCols;
   ColNameSheetModified:=TRUE;

   if ((ColLineCb.Checked) or (InputFileType <> ft_TextFile)) and (not SuppressAutomaticColAssignment) then
      SetupColNameSheet;
   CheckColNameTabPage;
end;

procedure TTextWizardWnd.PolyBtnClick(Sender: TObject);
begin
   PointBtn.Flat:=FALSE;
   PolyBtn.Flat:=TRUE;
   CPolyBtn.Flat:=FALSE;
   CircleBtn.Flat:=FALSE;
   SymbolBtn.Flat:=FALSE;
   AdfBtn.Flat:=FALSE;
   GpsBtn.Flat:=FALSE;
   SetupSupportedCols;
   ColNameSheetModified:=TRUE;
  if ((ColLineCb.Checked) or (InputFileType <> ft_TextFile)) and (not SuppressAutomaticColAssignment) then
      SetupColNameSheet;
   CheckColNameTabPage;
end;

procedure TTextWizardWnd.CPolyBtnClick(Sender: TObject);
begin
   PointBtn.Flat:=FALSE;
   PolyBtn.Flat:=FALSE;
   CPolyBtn.Flat:=TRUE;
   CircleBtn.Flat:=FALSE;
   SymbolBtn.Flat:=FALSE;
   AdfBtn.Flat:=FALSE;
   GpsBtn.Flat:=FALSE;
   SetupSupportedCols;
   ColNameSheetModified:=TRUE;
   if ((ColLineCb.Checked) or (InputFileType <> ft_TextFile)) and (not SuppressAutomaticColAssignment) then
      SetupColNameSheet;
   CheckColNameTabPage;
end;

procedure TTextWizardWnd.CircleBtnClick(Sender: TObject);
begin
   PointBtn.Flat:=FALSE;
   PolyBtn.Flat:=FALSE;
   CPolyBtn.Flat:=FALSE;
   CircleBtn.Flat:=TRUE;
   SymbolBtn.Flat:=FALSE;
   AdfBtn.Flat:=FALSE;
   GpsBtn.Flat:=FALSE;
   SetupSupportedCols;
   ColNameSheetModified:=TRUE;
   if ((ColLineCb.Checked) or (InputFileType <> ft_TextFile)) and (not SuppressAutomaticColAssignment) then
      SetupColNameSheet;
   CheckColNameTabPage;
end;

procedure TTextWizardWnd.SymbolBtnClick(Sender: TObject);
begin
   PointBtn.Flat:=FALSE;
   PolyBtn.Flat:=FALSE;
   CPolyBtn.Flat:=FALSE;
   CircleBtn.Flat:=FALSE;
   SymbolBtn.Flat:=TRUE;
   AdfBtn.Flat:=FALSE;
   GpsBtn.Flat:=FALSE;
   SetupSupportedCols;
   ColNameSheetModified:=TRUE;
   if ((ColLineCb.Checked) or (InputFileType <> ft_TextFile)) and (not SuppressAutomaticColAssignment) then
      SetupColNameSheet;
   CheckColNameTabPage;
end;

procedure TTextWizardWnd.AdfBtnClick(Sender: TObject);
begin
   PointBtn.Flat:=FALSE;
   PolyBtn.Flat:=FALSE;
   CPolyBtn.Flat:=FALSE;
   CircleBtn.Flat:=FALSE;
   SymbolBtn.Flat:=FALSE;
   AdfBtn.Flat:=TRUE;
   GpsBtn.Flat:=FALSE;
   SetupSupportedCols;
   ColNameSheetModified:=TRUE;
end;

procedure TTextWizardWnd.Cancel5BtnClick(Sender: TObject);
begin
   ModalResult:=mrCancel;
   Self.Close;
end;

procedure TTextWizardWnd.Prev5BtnClick(Sender: TObject);
var colidx:integer;
    islabelassigned:boolean;
begin
   // check if there is a label assigned
   islabelassigned:=FALSE;
   for colidx:=0 to ColNameGrid.ColCount-1 do
   begin
      if ColNameGrid.Cells[ColIdx, 0] = LabelColDef then islabelassigned:=TRUE;
   end;
   if islabelassigned then
   begin
      PageControl1.Pages[0].TabVisible:=FALSE;  // ColumnDefSheet will not be shown
      PageControl1.Pages[1].TabVisible:=TRUE;   // LabelTextSheet will not be shown
      PageControl1.Pages[2].TabVisible:=FALSE;  // SeperatorSheet will not be shown
      PageControl1.Pages[3].TabVisible:=FALSE;  // ColNameSheet will not be shown
      PageControl1.Pages[4].TabVisible:=FALSE;  // SaveDefSheet will not be shown
      PageControl1.ActivePage:=LabelTextSheet;  // show the LabelTextSheet now
      SetupLabelTextSheet;
      CheckLabelTextTabPage;
   end
   else
   begin
      PageControl1.Pages[0].TabVisible:=FALSE;  // ColumnDefSheet will not be shown
      PageControl1.Pages[1].TabVisible:=FALSE;  // LabelTextSheet will not be shown
      PageControl1.Pages[2].TabVisible:=FALSE;  // SeperatorSheet will not be shown
      PageControl1.Pages[3].TabVisible:=TRUE;   // ColNameSheet will be shown
      PageControl1.Pages[4].TabVisible:=FALSE;  // SaveDefSheet will not be shown
      PageControl1.ActivePage:=ColNameSheet;    // show the ColNameSheet now
      CheckColNameTabPage;
   end;
end;

procedure TTextWizardWnd.Next4BtnClick(Sender: TObject);
begin

   // next page is SaveDefSheet
   SetupSaveDefSheet;
   PageControl1.Pages[0].TabVisible:=FALSE;  // ColumnDefSheet will not be shown
   PageControl1.Pages[1].TabVisible:=FALSE;  // LabelTextSheet will not be shown
   PageControl1.Pages[2].TabVisible:=FALSE;  // SeperatorSheet will not be shown
   PageControl1.Pages[3].TabVisible:=FALSE;  // ColNameSheet will not be shown
   PageControl1.Pages[4].TabVisible:=TRUE;   // SaveDefSheet will be shown
   PageControl1.ActivePage:=SaveDefSheet;    // show the SaveDefSheet now
end;

procedure TTextWizardWnd.FormDestroy(Sender: TObject);
begin
   CurrTextFileDef:=nil;
   if OpenDialogsModuleWnd <> nil then
      OpenDialogsModuleWnd.Free;
end;

procedure TTextWizardWnd.SaveDefFileBtnClick(Sender: TObject);
var tmpTxtDef:TTextFileDef;
begin
   if SaveDBToTdfCB.Checked then
   	begin
   	tmpTxtDef := TTextFileDef.Create;
   	SetupTextFileDefFromWindow(tmpTxtDef);
   	if (tmpTxtDef.HasDatabaseColumns  and (MainWnd.AXImpExpDbc.ImpDbMode < 1)) then MainWnd.AXImpExpDbc.SetupByModalDialog;
   	if (tmpTxtDef.HasDatabaseColumns and (MainWnd.AXImpExpDbc.ImpDbMode > 0)) then SetTxtFilewnd.DestDbLabel.Caption := MainWnd.AXImpExpDbc.ImpDbInfo;
   	tmpTxtDef.Destroy;
        end;

   if (Pos('*',DefFilenameLabel.Caption) = 0) then
        begin
   	if FileExists(DefFilenameLabel.Caption) then
        begin
   	SaveDefBtn.Click;
        exit;
        end;
        end;

   if OpenDialogsModuleWnd <> nil then
      OpenDialogsModuleWnd.Free;
   OpenDialogsModuleWnd:=TOpenDialogsModuleWnd.Create(nil);
   OpenDialogsModuleWnd.ParentHandle:=Handle;
   OpenDialogsModuleWnd.OpenMode:=file_save;
   OpenDialogsModuleWnd.OnClose:=OnOpenDialogsModuleWndClose;
   OpenDialogsModuleWnd.SaveDialog.InitialDir := OSInfo.DocDataDir;
   // setup the filter for the open-dialog
   // check if the object-type is an .adf file
   if AdfBtn.Flat then
   begin
      OpenDialogsModuleWnd.SaveDialog.DefaultExt:='*.adf';
      OpenDialogsModuleWnd.SaveDialog.Filename:='*.adf';
      OpenDialogsModuleWnd.SaveDialog.Filter:='PXF-ASCII Definition File|*.adf';
   end
   else
   begin
      OpenDialogsModuleWnd.SaveDialog.FileName:='*.tdf';
      OpenDialogsModuleWnd.SaveDialog.DefaultExt:='*.tdf';
      OpenDialogsModuleWnd.SaveDialog.Filter:='Text Definition File|*.tdf';
   end;
   OpenDialogsModuleWnd.Show;
   SaveDefBtn.Click;
end;

procedure TTextWizardWnd.SaveDefBtnClick(Sender: TObject);
var tmpTxtDef:TTextFileDef;
begin
   // save the current settings to file
   tmpTxtDef:=TTextFileDef.Create;
   // setup the TextFileDef object with the current settings from the window
   SetupTextFileDefFromWindow(tmpTxtDef);
   // now store the temporary text-file definition to file
   tmpTxtDef.SaveToFile(DefFilenameLabel.Caption);
   // the temporary text-file definition can be deleted now
   // STR ++
   tmpTxtDef.Free;
   // STR --
   SaveDefFileBtn.Enabled := FALSE;
end;

procedure TTextWizardWnd.SetupTextFileDefFromWindow(var aTextDef:TTextFileDef);
var i:integer;
    intval,count:integer;
    islabelassigned:boolean;
    label_horizontal, label_vertical:integer;
    LabelFont:string;
    LabelSize, LabelAngle:integer;
    LabelOffset:integer;
begin
   // method is used to setup the text-file definition with the settings from screen

   // set the textfiletype
   aTextDef.SetInputFileType(InputFileType);
   // first setup the delimiter
   if InputFileType = ft_TextFile then
   begin
      if UseSeperatorCb.Checked then
      begin
         aTextDef.SetDelimeterType(delim_Character);
         aTextDef.SetDelimeterChar(SystemDelimeter);
      end
      else if FixedWideCb.Checked then
      begin
         aTextDef.SetDelimeterType(delim_FixColumns);
         // add the delimters
         for i:=1 to DelimCount do
         begin
            aTextDef.AddFixColDelimeter(Delimeters[i] div CharPixelWidth);
         end;
      end;
   end
   else if (InputFileType = ft_dBase) or (InputFileType = ft_Access) then
   begin
      aTextDef.SetDelimeterType(delim_Database);
      aTextDef.SetDatabaseTableName(UsedAccessTablename);
   end;

    // setup the column assignement
   if PointBtn.Flat       then aTextDef.SetObjectType(1)   // ot_Pixel
   else if PolyBtn.Flat   then aTextDef.SetObjectType(2)   // ot_Poly
   else if CPolyBtn.Flat  then aTextDef.SetObjectType(4)   // ot_CPoly
   else if CircleBtn.Flat then aTextDef.SetObjectType(12)  // ot_Circle
   else if SymbolBtn.Flat then aTextDef.SetObjectType(8)   // ot_Symbol
   else if ADFBtn.Flat    then aTextDef.SetObjectType(99)  // ADF-File
   else if GpsBtn.Flat    then aTextDef.SetObjectType(98); // GPS-File

   islabelassigned:=FALSE;
   for i:=0 to ColNameGrid.ColCount-1 do
   begin
      if ColNameGrid.Cells[i, 0]     = XColDef               then aTextDef.AddColumnDef(i, col_X)           // assign X-Coordinate
      else if ColNameGrid.Cells[i,0] = YColDef               then aTextDef.AddColumnDef(i, col_Y)           // assign Y-Coordinate
      else if ColNameGrid.Cells[i,0] = LabelColDef           then begin aTextDef.AddColumnDef(i, col_Label); islabelassigned:=TRUE; end  // assign Label
      else if ColNameGrid.Cells[i,0] = LabelMarkerColDef     then aTextDef.AddColumnDef(i, col_LabelMarker) // assign label-marker
      else if ColNameGrid.Cells[i,0] = PolySeperatorColDef   then aTextDef.AddColumnDef(i, col_PolySeperator) // assign poly-seperator
      else if ColNameGrid.Cells[i,0] = LayerColDef           then aTextDef.AddColumnDef(i, col_Layer)       // assign Layer
      else if ColNameGrid.Cells[i,0] = RadiusColDef          then aTextDef.AddColumnDef(i, col_Radius)      // assign Radius
      else if ColNameGrid.Cells[i,0] = SymbolNameColDef      then aTextDef.AddColumnDef(i, col_SymbolName)  // assign Symbol-name
      else if ColNameGrid.Cells[i,0] = SymbolSizeColDef      then aTextDef.AddColumnDef(i, col_SymbolSize)     // assign Symbol-size
      else if ColNameGrid.Cells[i,0] = SymbolRotationColDef  then aTextDef.AddColumnDef(i, col_SymbolRotation) // assign Symbol-rotation
      else if ColNameGrid.Cells[i,0] = SymbolIdxColDef       then aTextDef.AddColumnDef(i, col_SymbolIndex) // assign Symbol-index
      else if ColNameGrid.Cells[i,0] = ParamAColDef          then aTextDef.AddColumnDef(i, col_ParamA)      // assign Parameter-A
      else if ColNameGrid.Cells[i,0] = ParamBColDef          then aTextDef.AddColumnDef(i, col_ParamB)      // assign Parameter-B
      else if ColNameGrid.Cells[i,0] = ParamCColDef          then aTextDef.AddColumnDef(i, col_ParamC)      // assign Parameter-C
      else if ColNameGrid.Cells[i,0] = ObjectTypeColDef      then aTextDef.AddColumnDef(i, col_ObjectType)  // assign object-type
      else if ColNameGrid.Cells[i,0] = ObjectNrColDef        then aTextDef.AddColumnDef(i, col_ObjectNr)    // assign object-number
      else if ColNameGrid.Cells[i,0] = GrafikColDef          then aTextDef.AddColumnDef(i, col_Graphic)     // assign object-number
      else if ColNameGrid.Cells[i,0] = ProgisIdColDef        then aTextDef.AddColumnDef(i, col_ProgisId)    // assign ProgisId
      else if ColNameGrid.Cells[i,0] = GpsSymDirColDef       then aTextDef.AddColumnDef(i, col_GpsSymDir)       // assign Gps Direction
      else if ColNameGrid.Cells[i,0] = GpsSymLineTypeColDef  then aTextDef.AddColumnDef(i, col_GpsSymLineType)  // assign Gps Linetype
      else if ColNameGrid.Cells[i,0] = GpsSymLineColorColDef then aTextDef.AddColumnDef(i, col_GpsSymLineColor) // assign Gps Linecolor
      else if ColNameGrid.Cells[i,0] = GpsSymFillTypeColDef  then aTextDef.AddColumnDef(i, col_GpsSymFillType)  // assign Gps Filltype
      else if ColNameGrid.Cells[i,0] = GpsSymFillcolorColDef then aTextDef.AddColumnDef(i, col_GpsSymFillcolor) // assign Gps Fillcolor
      else if ColNameGrid.Cells[i,0] = GpsLineTypeColDef     then aTextDef.AddColumnDef(i, col_GpsLineType)     // assign Gps Linetype
      else if ColNameGrid.Cells[i,0] = GpsLineColorColDef    then aTextDef.AddColumnDef(i, col_GpsLineColor)    // assign Gps Line color
      else if ColNameGrid.Cells[i,0] = GpsLineWidthColDef    then aTextDef.AddColumnDef(i, col_GpsLineWidth)    // assign Gps Line width
      else
      begin
         // check if it is a database-column
         val(ColNameGrid.Cells[i,0],intval,count);
         if (count <> 0) then
            aTextDef.AddDatabaseColumnDef(i, ColNameGrid.Cells[i,0]);
      end;
   end;
   aTextDef.SetFirstLineIsColumnDef(ColLineCb.Checked);


   if islabelassigned then
   begin
      // write the current label-definition to the text-file-definition
      if TextPosPanel.TextAlign = taCenter      then begin label_horizontal:=1; label_vertical:=1; end;
      if TextPosPanel.TextAlign = taUpperCenter then begin label_horizontal:=1; label_vertical:=0; end;
      if TextPosPanel.TextAlign = taLowerCenter then begin label_horizontal:=1; label_vertical:=2; end;

      if TextPosPanel.TextAlign = taCenterRight then begin label_horizontal:=0; label_vertical:=1; end;
      if TextPosPanel.TextAlign = taUpperRight  then begin label_horizontal:=0; label_vertical:=0; end;
      if TextPosPanel.TextAlign = taLowerRight  then begin label_horizontal:=0; label_vertical:=2; end;

      if TextPosPanel.TextAlign = taCenterLeft  then begin label_horizontal:=2; label_vertical:=1; end;
      if TextPosPanel.TextAlign = taUpperLeft   then begin label_horizontal:=2; label_vertical:=0; end;
      if TextPosPanel.TextAlign = taLowerLeft   then begin label_horizontal:=2; label_vertical:=2; end;

      // get font and size
      LabelFont:=CurrentfontLabel.Caption;
      val(CurrentSizeLabel.Caption, LabelSize, count);
      val(LabelAngleEdit.Text, LabelAngle, count);
      val(LabelOffsetEdit.Text, LabelOffset, count);
      if not LabelOffsetEdit.Enabled then LabelOffset := 0;
      aTextDef.SetLabelData(LabelFont, Labelsize, LabelAngle, label_horizontal, label_vertical, LabelOffset);
      aTextDef.SetUseLabelLayer(LabelLayerCb.Checked);
      aTextDef.SetLabelLayer(LabelLayerListCb.Text);
   end;
   // check the ignore for duplicates
   aTextDef.SetIgnoreDuplicates(IgnoreDuplicatesCb.Checked);

   // setup the default symbol
   if SetDefaultSymCb.Checked then aTextDef.SetDefaultSymbolName(DefaultSymLb.Text) else aTextDef.SetDefaultSymbolName('');

   // setup the projection
   aTextDef.SetProjection(MainWnd.AxGisProjection.SourceCoordinateSystem,
                          MainWnd.AxGisProjection.SourceProjSettings,
                          MainWnd.AxGisProjection.SourceProjection,
                          MainWnd.AxGisProjection.SourceDate,
                          MainWnd.AxGisProjection.SourceXOffset,
                          MainWnd.AxGisProjection.SourceYOffset,
                          MainWnd.AxGisProjection.SourceScale);

end;

procedure TTextWizardWnd.Next5BtnClick(Sender: TObject);
begin
     if (Pos('*',DefFilenameLabel.Caption) = 0) then
        begin
   	if FileExists(DefFilenameLabel.Caption) then
        begin
        SetTxtFileWnd.TdfStatusLabel.Caption := ExtractFileName(DefFilenameLabel.Caption);
        SetTxtFileWnd.OpenDialogTextDef.Filename := DefFilenameLabel.Caption;
        end;
        end;

   // the text definition object will be setuped with the current window settings
   CurrTextFileDef.Reset;
   SetupTextFileDefFromWindow(CurrTextFileDef);
   if SetTxtFileWnd <> nil then begin
        if SetTxtFileWnd.TdfStatusLabel.Caption = StrfileObj.Assign(9) then SetTxtFileWnd.TdfStatusLabel.Caption := StrfileObj.Assign(74);
   end;
   CurrModalResult:=mrOk;
   Self.Close;
end;

procedure TTextWizardWnd.ProjectionBtnClick(Sender: TObject);
begin
   MainWnd.AxGisProjection.ShowOnlySource := False;
   MainWnd.AxGisProjection.SetupByDialog;
   ProjectionLabel.Caption := MainWnd.AxGisProjection.SourceProjection;
   SaveDefFileBtn.Enabled := TRUE;
end;

procedure TTextWizardWnd.DbColnameEditExit(Sender: TObject);
var token:string;
    maxlen:integer;
    oldlen:integer;
    i:integer;
    intvalue,count:integer;
begin
   DbColnameEdit.visible:=FALSE;
   Next3Btn.Enabled := TRUE;
   DbColnameLabel.visible:=FALSE;
   token:=dbColnameEdit.Text;
   token:=Trim(token);

   // check if token is a reserved token
   val(token, intvalue, count);
   if (ColNameIsSupportedColumnName(token)) or (token = '') or (count = 0) then
      ColNameGrid.Cells[DatabaseCol,0]:=inttostr(DatabaseCol+1)
   else
      ColNameGrid.Cells[DatabaseCol, 0]:=token;
   // setup the width of the cell
   oldlen:=ColNameGrid.ColWidths[DatabaseCol];
   maxlen:=0;
   for i:=0 to ColNameGrid.RowCount - 1 do
   begin
      token:=ColNameGrid.Cells[DatabaseCol, i];
      if (length(token) > maxlen) then
         maxlen:=length(token);
   end;
   maxlen:=maxlen*CharPixelWidth;
   ColNameGrid.ColWidths[DatabaseCol]:=maxlen; // for seperator columns
   ColNameGrid.Width:=ColNameGrid.Width+(maxlen - oldlen);
   SupportedColsLb.SetFocus;
end;

procedure TTextWizardWnd.DbColnameEditKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   if Key = 13 then // leave the field
      DbColnameEditExit(Sender);
end;

procedure TTextWizardWnd.DatabaseBtnClick(Sender: TObject);
begin
   // ++ STR
   {
   SaveDefFileBtn.Enabled:=FALSE;
   SaveDefBtn.Enabled:=FALSE;
   Cancel5Btn.Enabled:=FALSE;
   Next5Btn.Enabled:=FALSE;
   Prev5Btn.Enabled:=FALSE;
   ProjectionBtn.Enabled:=FALSE;
   DatabaseBtn.Enabled:=FALSE;
   Self.Height:=Self.Height+45;
   }
   self.Enabled := FALSE;
   MainWnd.AXImpExpDbc.SetupByModalDialog;
   self.Enabled := TRUE;
   // MainWnd.AXImpExpDbc.SetupByDialog(Self.Handle, 90,1);
   DatabaseLabel.Caption := Mainwnd.AXImpExpDbc.ImpDBInfo;
   SetTxtFileWnd.DestDbLabel.Caption:= MainWnd.AxImpExpDbc.ImpDbInfo;
   SaveDefFileBtn.Enabled := TRUE;
   // -- STR
end;

procedure TTextWizardWnd.LoadDefBtnClick(Sender: TObject);
begin
   if OpenDialogsModuleWnd <> nil then
      OpenDialogsModuleWnd.Free;
   OpenDialogsModuleWnd:=TOpenDialogsModuleWnd.Create(nil);
   OpenDialogsModuleWnd.ParentHandle:=Handle;
   OpenDialogsModuleWnd.OpenMode:=file_open;
   OpenDialogsModuleWnd.OnClose:=OnOpenDialogsModuleWndClose;
   // setup the filter for the open-dialog
   OpenDialogsModuleWnd.OpenDialog.FileName:='*.tdf';
   OpenDialogsModuleWnd.OpenDialog.DefaultExt:='*.tdf';
   OpenDialogsModuleWnd.OpenDialog.Filter:='Text Definition File|*.tdf';
   OpenDialogsModuleWnd.Show;
end;

procedure TTextWizardWnd.SemicolonSepCbClick(Sender: TObject);
begin
   if SemicolonSepCb.Checked then
   begin
      // setup other checkboxes
      TabSepCb.Checked:=FALSE;
      CommaSepCb.Checked:=FALSE;
      SpaceSepCb.Checked:=FALSE;
      UserSepCb.Checked:=FALSE;
      UserSepEdit.Visible:=FALSE;
      // set the delimiter
      SystemDelimeter := ';';
   end;
   SeperatorSheetModified:=TRUE;
   CheckSeperatorTabPage;
end;

procedure TTextWizardWnd.ColLineCbClick(Sender: TObject);
begin
   // the file has to be reread due to the first line of the file should be used
   // as column definition if ColLineCb is checked
   if not ColLineCb.Checked then
   begin
      ColLineCb.Enabled:=FALSE;
   end;
   ColNameSheetModified:=TRUE;
   SetupColNameSheet;
   CheckColNameTabPage;
end;

procedure TTextWizardWnd.OnOpenDialogsModuleWndClose(Sender: TObject;  var Action: TCloseAction);
var tmpTxtDef:TTextFileDef;
    i:integer;
begin
   // check the Tabulator page
   if Pagecontrol1.ActivePage = SeperatorSheet then
   begin
      // a .tdf file has been selected
      if OpenDialogsModuleWnd.Filename <> '' then
      begin
         // a file has been set
         CurrTextFileDef.LoadFromFile(OpenDialogsModuleWnd.Filename, WorkingDir+'TxtImp.ini');
         SetupPagesWithTextFileDef;
      end;
      CheckSeperatorTabPage;
   end
   else if PageControl1.ActivePage = LabelTextSheet then
   begin
      if OpenDialogsModuleWnd.Fontname <> '' then
      begin
         // get new font and
         // CurrentFontLabel.Font:=OpenDialogsModuleWnd.FontDialog.Font;
         CurrentFontLabel.Caption:=OpenDialogsModuleWnd.FontDialog.Font.Name;
         CurrentFontLabel.Font.size:=OpenDialogsModuleWnd.FontDialog.Font.Size;
         CurrentSizeLabel.Caption:=inttostr(CurrentFontLabel.Font.size);
      end;
   end
   else if PageControl1.ActivePage = SaveDefSheet then
   begin
      // set the filename
      if OpenDialogsModuleWnd.Filename <> '' then
      begin
         DefFilenameLabel.Caption:=OpenDialogsModuleWnd.Filename;
         // save the current settings to file
         tmpTxtDef:=TTextFileDef.Create;
         // setup the TextFileDef object with the current settings from the window
         SetupTextFileDefFromWindow(tmpTxtDef);
         // now store the temporary text-file definition to file
         tmpTxtDef.SaveToFile(DefFilenameLabel.Caption);
         // the temporary text-file definition can be deleted now
         tmpTxtDef.Destroy;    
         SaveDefFileBtn.Enabled := FALSE;
      end;
      if DefFilenameLabel.Caption <> '' then SaveDefBtn.Enabled:=TRUE else SaveDefBtn.Enabled:=FALSE;
   end
   else if PageControl1.ActivePage = ColNameSheet then
   begin
      // a .tdf file has been selected
      if OpenDialogsModuleWnd.Filename <> '' then
      begin
         // a file has been set
         CurrTextFileDef.LoadFromFile(OpenDialogsModuleWnd.Filename, WorkingDir+'TxtImp.ini');
         if InputFileType = ft_Access then
         begin
            // setup the database table-name
            CurrTextFileDef.SetDatabaseTableName(DbTableLb.Items[DbTableLb.ItemIndex]);
         end;
         SetupPagesWithTextFileDef;
      end;
      CheckSeperatorTabPage;
   end;
end;

procedure TTextWizardWnd.SetupSelTableSheet;
var TableNames:TStrings;
    i:integer;
    exists:integer;
begin
   if not SelTableSheetModified then
      exit;
   if not AdoConnection.Connected then
   begin
      AdoConnection.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + TxtFilename;
      AdoConnection.Connected:=True;
      DbTableLb.Items.Clear;
      TableNames:=TStringList.Create;
      AdoConnection.GetTableNames(TableNames,False);
      for i:=0 to TableNames.Count-1 do
      begin
         if (Pos('USYS_', TableNames[i]) = 0) then // ignore the system tables
            DbTableLb.Items.Add(TableNames[i]);
      end;
      TableNames.Clear;
   end;
   // check if there is already a table that should be processed set
   // check if this also exists in the current database
   exists:=-1;
   for i:=0 to DbTableLb.Items.Count -1 do
   begin
      if DbTableLb.Items[i] = UsedAccessTablename then
      begin
         exists:=i;
         break;
      end;
   end;
   // check if there is only one table than the current page is the
   if (DbTableLb.Items.Count = 1) or (exists >= 0) then
   begin
      if DbTableLb.Items.Count = 1 then
         DbTableLb.ItemIndex:=0 // it is the first item
      else
      begin
         // select the table that is current processed
         DbTableLb.ItemIndex:=exists;

      end;
      if DataBaseHeaderColumns <> nil then
      begin
         DataBaseHeaderColumns.Free;
         DataBaseHeaderColumns:=nil;
      end;
      // the Column-name Sheet has to be setuped
      ColNameSheetModified:=TRUE;
      SetupColNameSheet;
      ColNameSheetModified:=FALSE;

      PageControl1.Pages[0].TabVisible:=FALSE;  // ColumnDefSheet will not be shown
      PageControl1.Pages[1].TabVisible:=FALSE;  // LabelTextSheet will not be shown
      PageControl1.Pages[2].TabVisible:=FALSE;  // SeperatorSheet will not be shown
      PageControl1.Pages[3].TabVisible:=TRUE;   // ColNameSheet will be shown
      PageControl1.Pages[4].TabVisible:=FALSE;  // SaveDefSheet will not be shown
      PageControl1.Pages[5].TabVisible:=FALSE;  // SelTableSheet will not be shown
      if DbTableLb.Items.Count = 1 then
         Prev3Btn.Enabled:=FALSE
      else
         Prev3Btn.Enabled:=TRUE;

      PageControl1.ActivePage:=ColNameSheet;    // show the ColNameSheet now
      CheckColNameTabPage;
      SelTableSheetModified:=FALSE;
      SupportedColsLb.SetFocus;
   end;
end;

procedure TTextWizardWnd.CheckSelTableTabPage;
var isChecked:boolean;
    i:integer;
begin
   isChecked:=FALSE;
   for i:=0 to DbTableLb.Items.Count-1 do
   begin
      if DbTableLb.Selected[i] then
      begin
         UsedAccessTablename:=DbTableLb.Items[i];
         isChecked:=TRUE;
         break;
      end;
   end;
   if isChecked then Next6Btn.Enabled:=TRUE else Next6Btn.Enabled:=FALSE;
end;

procedure TTextWizardWnd.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
   if StrfileObj <> nil then
      StrfileObj.Free;
   if DataBaseHeaderColumns <> nil then
      DataBaseHeaderColumns.Free;
end;

procedure TTextWizardWnd.Cancel6BtnClick(Sender: TObject);
begin
   if AdoConnection.Connected then
   begin
      AdoConnection.Close;
      AdoConnection.Connected:=FALSE;
   end;
   // check if there is a filter set -> in this case reset the file definition
   if ObjectTypeFilter <> -1 then
      CurrTextFileDef.Reset;
   ModalResult:=mrCancel;
   Self.Close;
end;

procedure TTextWizardWnd.Next6BtnClick(Sender: TObject);
begin
   SaveDefFileBtn.Enabled := TRUE;
   // if the SelTable Sheet has been modified all following pages are modified
   if SelTableSheetModified then
   begin
      if DataBaseHeaderColumns <> nil then
      begin
         DataBaseHeaderColumns.Free;
         DataBaseHeaderColumns:=nil;
      end;
      ColNameSheetModified:=TRUE;
      LabelTextSheetModified:=TRUE;
   end;

   // the Column-name Sheet has to be setuped
   UsedObjectType:=-1;
   SetupColNameSheet;

   PageControl1.Pages[0].TabVisible:=FALSE;  // ColumnDefSheet will not be shown
   PageControl1.Pages[1].TabVisible:=FALSE;  // LabelTextSheet will not be shown
   PageControl1.Pages[2].TabVisible:=FALSE;  // SeperatorSheet will not be shown
   PageControl1.Pages[3].TabVisible:=TRUE;   // ColNameSheet will be shown
   PageControl1.Pages[4].TabVisible:=FALSE;  // SaveDefSheet will not be shown
   PageControl1.Pages[5].TabVisible:=FALSE;  // SelTableSheet will not be shown
   PageControl1.ActivePage:=ColNameSheet;    // show the ColNameSheet now
   CheckColNameTabPage;
   SelTableSheetModified:=FALSE;
   SupportedColsLb.SetFocus;
end;

procedure TTextWizardWnd.DbTableLbClick(Sender: TObject);
begin
   SelTableSheetModified:=TRUE;
   CheckSelTableTabPage;
end;

procedure TTextWizardWnd.LoadDefBtn2Click(Sender: TObject);
begin
   if OpenDialogsModuleWnd <> nil then
      OpenDialogsModuleWnd.Free;
   OpenDialogsModuleWnd:=TOpenDialogsModuleWnd.Create(nil);
   OpenDialogsModuleWnd.ParentHandle:=Handle;
   OpenDialogsModuleWnd.OpenMode:=file_open;
   OpenDialogsModuleWnd.OnClose:=OnOpenDialogsModuleWndClose;
   // setup the filter for the open-dialog
   OpenDialogsModuleWnd.OpenDialog.FileName:='*.tdf';
   OpenDialogsModuleWnd.OpenDialog.DefaultExt:='*.tdf';
   OpenDialogsModuleWnd.OpenDialog.Filter:='Text Definition File|*.tdf';
   OpenDialogsModuleWnd.Show;
end;

procedure TTextWizardWnd.AngleUpBtnClick(Sender: TObject);
var value, count:integer;
begin
   // increment the current angle value
   val (LabelAngleEdit.Text, value, count);
   if (count <> 0) or (value = 360) then exit;
   value:=value+1;
   LabelAngleEdit.Text:=inttostr(value);
end;

procedure TTextWizardWnd.AngleDownBtnClick(Sender: TObject);
var value, count:integer;
begin
   // increment the current angle value
   val (LabelAngleEdit.Text, value, count);
   if (count <> 0) or (value = 0) then exit;
   value:=value-1;
   LabelAngleEdit.Text:=inttostr(value);
end;

procedure TTextWizardWnd.LabelAngleEditChange(Sender: TObject);
var value, count:integer;
begin
   val(LabelAngleEdit.Text, value, count);
   if (count <> 0) or (value > 360) or (value < 0) then
   begin
      LabelAngleEdit.Font.Color:=clRed;
   end
   else
      LabelAngleEdit.Font.Color:=clGreen;

   CheckLabelTextTabPage;
end;

procedure TTextWizardWnd.LabelPosCbClick(Sender: TObject);
var LabelFont:string;
    LabelSize, LabelAngle, label_horizontal, label_vertical:integer;
    LabelOffset:integer;
    dummy:string;
begin
   if LabelPosCb.Checked then
   begin
      LabelPosPanel.Visible:=TRUE;
      SetupLabelPosPanel;
   end
   else
   begin
      LabelPosPanel.Visible:=FALSE;
      // draw the default settings
      CurrTextFileDef.GetLabelData(LabelFont, LabelSize, LabelAngle, label_horizontal, label_vertical, LabelOffset);

      if label_horizontal=1 then // horizontal alignement is ts_Center
      begin
         if label_vertical = 1 then TextPosPanel.TextAlign:=taCenter;       // center
         if label_vertical = 2 then TextPosPanel.TextAlign:=taUpperCenter;  // top
         if label_vertical = 0 then TextPosPanel.TextAlign:=taLowerCenter;  // botton
      end;
      if label_horizontal=2 then // horizontal alignement is ts_Right
      begin
         if label_vertical = 1 then TextPosPanel.TextAlign:=taCenterLeft;   // center
         if label_vertical = 2 then TextPosPanel.TextAlign:=taUpperLeft;    // top
         if label_vertical = 0 then TextPosPanel.TextAlign:=taLowerLeft;   // bottom
      end;
      if label_horizontal=0 then // horizontal alignement is ts_Left
      begin
         if label_vertical = 1 then TextPosPanel.TextAlign:=taCenterRight;  // center
         if label_vertical = 2 then TextPosPanel.TextAlign:=taUpperRight;   // top
         if label_vertical = 0 then TextPosPanel.TextAlign:=taLowerRight;  // bottom
      end;
      // get font and size
      CurrentfontLabel.Caption:=LabelFont;
      CurrentSizeLabel.Caption:=inttostr(LabelSize);
      LabelAngleEdit.Text:=inttostr(LabelAngle);
      LabelOffsetEdit.Text:=inttostr(LabelOffset);
      TextPosPanel.Distance:=LabelOffset;
   end;
end;

procedure TTextWizardWnd.CheckLabelTextTabPage;
var count, value:integer;
    allok:boolean;
begin
   // method is used to check the LabelText sheet
   allok:=TRUE;
   // check the label angle
   val(LabelAngleEdit.Text, value, count);
   if (count <> 0) or (value < 0) or (value > 360) then
   begin
      allok:=FALSE;
      // check if the label markers should be defined
      if LabelPosCb.Checked then
      begin
         SaveLabelPosBtn.Enabled:=FALSE;
      end;
   end;
   // check the label offset
   val(LabelOffsetEdit.Text, value, count);
   if (count <> 0) then
   begin
      allok:=FALSE;
      // check if the label markers should be defined
      if LabelPosCb.Checked then
      begin
         SaveLabelPosBtn.Enabled:=FALSE;
      end;
   end;
   if allok then Next4Btn.Enabled:=TRUE else Next4Btn.Enabled:=FALSE;
end;

procedure TTextWizardWnd.SetupLabelPosPanel;
var i:integer;
    Marker_name, Marker_Font:string;
    Marker_Size, Marker_Angle, Marker_hori, Marker_verti:integer;
    Marker_Offset:integer;
begin
   // method is used to setup the Label positioning panel
   // get the already defined label position markers
   LabelPosMarkCb.Items.Clear;
   LabelPosMarkCb.Text:='';
   if CurrTextFileDef.GetNumLabelPosMarkers > 0 then
   begin
      for i:=0 to CurrTextFileDef.GetNumLabelPosMarkers-1 do
      begin
         CurrTextFileDef.GetLabelPosMarkerByIdx(i, Marker_name, Marker_Font, Marker_Size, Marker_Angle, Marker_hori, Marker_verti, Marker_Offset);
         LabelPosMarkCb.Items.Add(Marker_name);
         if i = 0 then LabelPosMarkCb.Text:=Marker_Name;
      end;
   end;
   LabelPosMarkCbChange(nil);
end;

procedure TTextWizardWnd.LabelPosMarkCbChange(Sender: TObject);
var Marker_Font:string;
    Marker_Size, Marker_Angle, Marker_hori, Marker_verti:integer;
    Marker_Offset:integer;
    value, count:integer;
    dummy:string;
begin
   // check if the current selected item is a label positioning item
   SaveLabelPosBtn.Enabled:=TRUE;
   val (LabelAngleEdit.Text, value, count);
   if (LabelPosMarkCb.Text = '') or (Count <> 0) then
   begin
      SaveLabelPosBtn.Enabled:=FALSE;
      exit;
   end;
   val(LabelOffsetEdit.Text, value, count);
   if (LabelOffsetEdit.Text = '') or (Count <> 0) then
   begin
      SaveLabelPosBtn.Enabled:=FALSE;
      exit;
   end;

   // check if the current LabelPosMark is defined in the labelpos definitions
   if CurrTextFileDef.GetLabelPosMarker(LabelPosMarkCb.Text, Marker_Font, Marker_Size, Marker_Angle, Marker_hori, Marker_verti, Marker_Offset) then
   begin
      // a label-pos marker with this name is already defined, so draw it black
      LabelPosMarkCb.Font.Color:=clBlack;


      if Marker_hori=1 then // horizontal alignement is ts_Center
      begin
         if Marker_verti = 1 then TextPosPanel.TextAlign:=taCenter;       // center             
         if Marker_verti = 2 then TextPosPanel.TextAlign:=taUpperCenter;  // top
         if Marker_verti = 0 then TextPosPanel.TextAlign:=taLowerCenter;  // botton
      end;
      if Marker_hori=2 then // horizontal alignement is ts_Right
      begin
         if Marker_verti = 1 then TextPosPanel.TextAlign:=taCenterLeft; // center
         if Marker_verti = 2 then TextPosPanel.TextAlign:=taUpperLeft; // top
         if Marker_verti = 0 then TextPosPanel.TextAlign:=taLowerLeft;  // bottom
      end;
      if Marker_hori=0 then // horizontal alignement is ts_Left
      begin
         if Marker_verti = 1 then TextPosPanel.TextAlign:=taCenterRight;  // center
         if Marker_verti = 2 then TextPosPanel.TextAlign:=taUpperRight;   // top
         if Marker_verti = 0 then TextPosPanel.TextAlign:=taLowerRight;  // bottom
      end;

      // get font and size
      CurrentfontLabel.Caption:=Marker_Font;
      CurrentSizeLabel.Caption:=inttostr(Marker_Size);
      LabelAngleEdit.Text:=inttostr(Marker_Angle);
      LabelOffsetEdit.Text:=inttostr(Marker_Offset);
      TextPosPanel.Distance:=Marker_Offset;
   end
   else
   begin
      // a label-pos marker with this name is not defined yet so draw it blue
      LabelPosMarkCb.Font.Color:=clBlue;
   end;
end;

procedure TTextWizardWnd.SaveLabelPosBtnClick(Sender: TObject);
var label_horizontal, label_vertical, LabelSize, LabelAngle,count:integer;
    LabelFont:string;
    stillexists:boolean;
    Label_Offset:integer;
begin
   // store the current label positioning marker
   // first check if the item already exists
   stillexists:=CurrTextFileDef.GetLabelPosMarker(LabelPosMarkCb.Text, LabelFont, LabelSize, LabelAngle, label_horizontal, label_vertical, Label_Offset);

   // get the setup from screen
   if TextPosPanel.TextAlign = taCenter      then begin label_horizontal:=1; label_vertical:=1; end;
   if TextPosPanel.TextAlign = taUpperCenter then begin label_horizontal:=1; label_vertical:=0; end;
   if TextPosPanel.TextAlign = taLowerCenter then begin label_horizontal:=1; label_vertical:=2; end;

   if TextPosPanel.TextAlign = taCenterRight then begin label_horizontal:=0; label_vertical:=1; end;
   if TextPosPanel.TextAlign = taUpperRight  then begin label_horizontal:=0; label_vertical:=0; end;
   if TextPosPanel.TextAlign = taLowerRight  then begin label_horizontal:=0; label_vertical:=2; end;

   if TextPosPanel.TextAlign = taCenterLeft  then begin label_horizontal:=2; label_vertical:=1; end;
   if TextPosPanel.TextAlign = taUpperLeft   then begin label_horizontal:=2; label_vertical:=0; end;
   if TextPosPanel.TextAlign = taLowerLeft   then begin label_horizontal:=2; label_vertical:=2; end;

   // get font and size
   LabelFont:=CurrentfontLabel.Caption;
   val(CurrentSizeLabel.Caption, LabelSize, count);
   val(LabelAngleEdit.Text, LabelAngle, count);
   val(LabelOffsetEdit.Text, Label_Offset, count);
   if  not LabelOffsetEdit.Enabled then Label_Offset := 0;
   CurrTextFileDef.SetupLabelPosMarker(LabelPosMarkCb.Text, LabelFont, LabelSize, LabelAngle, label_horizontal, label_vertical,Label_Offset);
   LabelPosMarkCb.Font.Color:=clBlack; // color is black now due it is stored
   // store the settings to the ini-file
   CurrTextFileDef.WriteLabelPosMarkers(WorkingDir+'TxtImp.ini');
   if not stillexists then
   begin
      // Add item to list
      LabelPosMarkCb.Items.Add(LabelPosMarkCb.Text);
   end;
end;

procedure TTextWizardWnd.LabelLayerCbClick(Sender: TObject);
begin
   LabelLayerPanel.visible:=LabelLayerCb.Checked;
end;

procedure TTextWizardWnd.SetDefaultSymCbClick(Sender: TObject);
begin
   DefaultSymLb.Enabled:=SetDefaultSymCb.Checked;
end;

procedure TTextWizardWnd.LabelOffsetEditChange(Sender: TObject);
var value:integer;
    count:integer;
begin
   val(LabelOffsetEdit.Text, value, count);
   if (count <> 0) then
   begin
      LabelOffsetEdit.Font.Color:=clRed;
   end
   else
   begin
      LabelOffsetEdit.Font.Color:=clGreen;
      TextPosPanel.Distance:=value;
   end;
   CheckLabelTextTabPage;
end;

procedure TTextWizardWnd.OffsetUpBtnClick(Sender: TObject);
var value:integer;
    count:integer;
    dummy:string;
begin
   // increment the current N/S offset value
   val (LabelOffsetEdit.Text, value, count);
   if (count <> 0) then exit;
   value:=value+1;
   dummy:=inttostr(value);
   LabelOffsetEdit.Text:=dummy;
   TextPosPanel.Distance:=value;
end;

procedure TTextWizardWnd.OffsetDownBtnClick(Sender: TObject);
var value:integer;
    count:integer;
    dummy:string;
begin
   // decrement the current N/S offset value
   val (LabelOffsetEdit.Text, value, count);
   if (count <> 0) then exit;
   value:=value-1;
   dummy:=inttostr(value);
   LabelOffsetEdit.Text:=dummy;
   TextPosPanel.Distance:=value;
end;


procedure TTextWizardWnd.GpsBtnClick(Sender: TObject);
begin
   PointBtn.Flat:=FALSE;
   PolyBtn.Flat:=FALSE;
   CPolyBtn.Flat:=FALSE;
   CircleBtn.Flat:=FALSE;
   SymbolBtn.Flat:=FALSE;
   AdfBtn.Flat:=FALSE;
   GpsBtn.Flat:=TRUE;
   SetupSupportedCols;
   ColNameSheetModified:=TRUE;
end;




procedure TTextWizardWnd.SaveDBToTdfCBClick(Sender: TObject);
begin
DatabaseLabel.Enabled :=  SaveDBToTdfCB.Checked;
DatabaseBtn.Enabled := SaveDBToTdfCB.Checked;
end;

procedure TTextWizardWnd.TextPosPanelMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
LabelOffsetEdit.Enabled := (TextPosPanel.TextAlign <> taCenter);
OffsetUpBtn.Enabled := (TextPosPanel.TextAlign <> taCenter);
OffsetDownBtn.Enabled := (TextPosPanel.TextAlign <> taCenter);
end;

end.
