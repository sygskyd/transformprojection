unit TextFileDef;

interface
uses Textutils, SysUtils, classes, inifiles;

type TDelimeterType=(delim_None, delim_Character, delim_FixColumns, delim_Database); // set of possible delimeter types
type TColumnType   =(col_Label, col_Layer, col_X, col_Y, col_Radius, col_SymbolName, col_SymbolIndex, col_SymbolSize, col_SymbolRotation,
                     col_PolySeperator, col_LabelMarker, col_ProgisId,
                     col_ParamA, col_ParamB, col_ParamC, col_ObjectType, col_ObjectNr, col_Graphic, col_Database,
                     col_GpsSymDir, col_GpsSymLineType, col_GpsSymLineColor, col_GpsSymFillType, col_GpsSymFillcolor,
                     col_GpsLineType, col_GpsLineColor, col_GpsLineWidth); // set of possible column types
type TInputFileType=(ft_None, ft_TextFile, ft_dBase, ft_Access); // set of possible input file types

     pColDefItem=^ColDefItem;
     ColDefItem=record
        pos    :integer;
        coltype:TColumnType;
        Columnname:string; // for database columns
     end;

     pLabelMarkerItem=^LabelMarkerItem;
     LabelMarkerItem=record
        Name :string;
        LabelFont:string;
        LabelSize:integer;
        LabelAngle:integer;
        Label_horizontal:integer;
        Label_vertical  :integer;
        Label_Offset :integer;
     end;

type TTextFileDef = class
public
   constructor Create;
   destructor Destroy;
   procedure SetDelimeterType(atype:TDelimeterType);
   function  GetDelimeterType:TDelimeterType;
   procedure SetDelimeterChar(adelim:string);
   function  GetDelimeterChar:string;
   procedure AddFixColDelimeter(adelim:integer);
   procedure AddColumnDef(pos:integer; atype:TColumnType);
   procedure AddDatabaseColumnDef(pos:integer; Columnname:string);
   function  ExtractData(DataLine:string; atype:TColumnType; var ResData:string):boolean;
   function  GetNumFixColDelimeters:integer;
   function  GetFixColDelimeter(idx:integer):integer;
   function  GetNumColumnDefs:integer;
   procedure GetColumnDef(idx:integer; var apos:integer; var atype:TColumnType; var aColname:string);
   function  GetPositionOfColumnDef(atype:TColumnType):integer;
   procedure SetObjectType(atype:integer);
   function  GetObjectType:integer;
   procedure Reset;
   procedure SaveToFile(aFilename:string);
   function LoadFromFile(aFilename:string; const inifilename:string):boolean;
   procedure SortIt (var A: array of Integer);
   procedure SetMaxLineLen(alen:integer);
   function HasDatabaseColumns:boolean;
   function ExtractDatabaseData(DataLine:string; aDbColname:string; var ResData:string):boolean;
   procedure SetLabelData(aFont:string; aSize:integer; aAngle:integer; hori:integer; verti:integer;Offset:integer);
   procedure GetLabelData(var aFont:string; var aSize:integer; var aAngle:integer; var hori:integer; var verti:integer;var Offset:integer);
   function UseLabelData:boolean;
   procedure SetIgnoreDuplicates(IgnoreDupl:boolean);
   function GetIgnoreDuplicates:boolean;
   procedure SetProjection(CoordSys:integer; ProjSet:string; Proj:string; ProjDate:string; XO:double; YO:double; Sc:double);
   procedure GetProjection(var CoordSys:integer; var ProjSet:string; var Proj:string; var ProjDate:string; var XO:double; var YO:double; var Sc:double);
   function GetFirstLineIsColumnDef:boolean;
   procedure SetFirstLineIsColumnDef(other:boolean);
   procedure SetupAutoColumnAssignments(inifilename:string);
   function  IsAutoAssignementColumn(ColName:string; ColType:TColumnType):boolean;
   procedure SetInputFileType(aft:TInputFileType);
   function  GetInputFileType:TInputFileType;
   procedure SetDatabaseTableName(dbName:string);
   function GetDatabaseTablename:string;
   procedure ReadLabelPosMarkers(const inifilename:string);
   procedure WriteLabelPosMarkers(const inifilename:string);
   procedure SetupLabelPosMarker(const Marker_Name:string; const Marker_Font:string;
                                 const Marker_Size:integer; const Marker_Angle:integer;
                                 const Marker_hori:integer; const Marker_verti:integer;
                                 const Marker_Offset:integer);
   function  GetLabelPosMarker(const Marker_Name:string; var Marker_Font:string;
                               var Marker_Size:integer; var Marker_Angle:integer;
                               var Marker_hori:integer; var Marker_verti:integer;
                               var Marker_Offset:integer):boolean;
   function  GetNumLabelPosMarkers:integer;
   function  GetLabelPosMarkerByIdx(const Marker_Idx:integer; var Marker_Name:string; var Marker_Font:string;
                               var Marker_Size:integer; var Marker_Angle:integer;
                               var Marker_hori:integer; var Marker_verti:integer;
                               var Marker_Offset:integer):boolean;
   procedure SetUseLabelLayer(other:boolean);
   function  GetUseLabelLayer:boolean;
   procedure SetLabelLayer(const other:string);
   function  GetLabelLayer:string;
   procedure SetDefaultSymbolName(const other:string);
   function  GetDefaultSymbolName:string;
private
   DelimeterType    :TDelimeterType;
   DelimeterChar    :string;
   FixColDelimeters :array of integer;
   FixColDelimetersCount:integer;
   ColumnDefinitions:array of pColDefItem;
   ColumnDefinitionsCount:integer;
   AutoColumns      :array of pColDefItem;
   AutoColumnsCount :integer;
   LabelMarkers     :array of pLabelMarkerItem;
   LabelMarkersCount:integer;
   FirstLineIsColumnDef  :boolean;
   ObjectType       :integer;
   maxlinelen       :integer;
   UseLabel         :boolean;
   LabelFont        :string;
   LabelSize        :integer;
   LabelAngle       :integer;
   Label_horizontal :integer;
   Label_vertical   :integer;
   Label_Offset     :integer;
   IgnoreDuplicates :boolean;
   InputFileType    :TInputFileType;
   DatabaseTablename:string;
   UseLabelLayer    :boolean;
   LabelLayer       :string;
   DefaultSymbolName:string;

   SourceCoordinateSystem:integer;
   SourceProjSettings:string;
   SourceProjection:string;
   SourceDate:string;
   SourceXOffset:double;
   SourceYOffset:double;
   SourceScale:double;

   procedure SaveAdfToFile(aFilename:string);
   procedure SaveTdfToFile(aFilename:string);
   function  LoadAdfFromFile(aFilename:string):boolean;
   function  LoadTdfFromFile(aFilename:string):boolean;
   procedure GetAdfFromToValue(coltype:TColumnType; var fromval:integer; var toval:integer);
   procedure GetAdfDatabaseFromToValue(aDbColname:string; var fromval:integer; var toval:integer);
end;

implementation
uses maindlg, textwizarddlg;

// procedure to sort array of integer values
procedure TTextFileDef.SortIt (var A: array of Integer);
   procedure QuickSort(var A: array of Integer; iLo, iHi: Integer);
   var Lo, Hi, Mid, T: Integer;
   begin
      Lo := iLo;
      Hi := iHi;
      Mid := A[(Lo + Hi) div 2];
      repeat
         while A[Lo] < Mid do Inc(Lo);
         while A[Hi] > Mid do Dec(Hi);
         if Lo <= Hi then
         begin
            T := A[Lo];
            A[Lo] := A[Hi];
            A[Hi] := T;
            Inc(Lo);
            Dec(Hi);
         end;
      until Lo > Hi;
      if Hi > iLo then QuickSort(A, iLo, Hi);
      if Lo < iHi then QuickSort(A, Lo, iHi);
   end;
begin
  QuickSort(A, Low(A), High(A));
end;

constructor TTextFileDef.Create;
begin
   DelimeterType:=delim_None;
   DelimeterChar:='';
   ObjectType:=-1;
   FixColDelimetersCount:=0;
   ColumnDefinitionsCount:=0;
   AutoColumnsCount:=0;
   LabelMarkersCount:=0;

   UseLabel:=FALSE;
   IgnoreDuplicates:=FALSE;
   FirstLineIsColumnDef:=FALSE;

   SourceCoordinateSystem:=0; // cs_carthesic
   SourceProjSettings:='NONE';
   SourceProjection:='NONE';
   SourceDate:='NONE';
   SourceXOffset:=0;
   SourceYOffset:=0;
   SourceScale:=1;

   InputFileType:=ft_none; // no type is default
   DatabaseTableName:='';

   LabelFont:='Courier New'; // default font
   LabelSize:=10;
   LabelAngle:=0;
   Label_horizontal:=0; // left
   Label_vertical:=2;   // top
   Label_Offset:=0;     // default offset is 0

   UseLabelLayer:=FALSE;
   LabelLayer:='';
   DefaultSymbolName:='';
   DatabaseTableName:='';
end;

destructor TTextFileDef.Destroy;
var curlen:integer;
    iterate:pColDefItem;
    markeriterate:pLabelMarkerItem;
    i:integer;
begin
   SetLength(FixColDelimeters, 0);
   curlen:=High(ColumnDefinitions);
   for i:=0 to curlen-1 do
   begin
      iterate:=ColumnDefinitions[i];
      dispose(iterate);
   end;
   SetLength(ColumnDefinitions,0);

   curlen:=High(AutoColumns);
   for i:=0 to curlen-1 do
   begin
      iterate:=AutoColumns[i];
      dispose(iterate);
   end;
   SetLength(AutoColumns,0);

   curlen:=High(LabelMarkers);
   for i:=0 to curlen-1 do
   begin
      markeriterate:=LabelMarkers[i];
      dispose(markeriterate);
   end;
end;

procedure TTextFileDef.Reset;
var curlen:integer;
    iterate:pColDefItem;
    i:integer;
begin
   DelimeterType:=delim_None;
   DelimeterChar:='';
   ObjectType:=-1;
   SetLength(FixColDelimeters, 0);
   curlen:=High(ColumnDefinitions);
   for i:=0 to curlen-1 do
   begin
      iterate:=ColumnDefinitions[i];
      dispose(iterate);
   end;
   SetLength(ColumnDefinitions,0);
   FixColDelimetersCount:=0;
   ColumnDefinitionsCount:=0;
   FirstLineIsColumnDef:=FALSE;
   UseLabel:=FALSE;
   SourceCoordinateSystem:=0; // cs_carthesic
   SourceProjSettings:='NONE';
   SourceProjection:='NONE';
   SourceDate:='NONE';
   SourceXOffset:=0;
   SourceYOffset:=0;
   SourceScale:=1;
   LabelFont:='Courier New'; // default font
   LabelSize:=10;
   LabelAngle:=0;
   Label_horizontal:=0;  // left
   Label_vertical:=2;    // top
   Label_Offset:=0;      // default offset is 0

   InputFileType:=ft_None; // no type is default
   DatabaseTableName:='';

   UseLabelLayer:=FALSE;
   LabelLayer:='';
   DefaultSymbolName:='';
end;

procedure TTextFileDef.SetDefaultSymbolName(const other:string);
begin
   DefaultSymbolName:=other;
end;

function TTextFileDef.GetDefaultSymbolName:string;
begin
   result:=DefaultSymbolName;
end;

procedure TTextFileDef.SetUseLabelLayer(other:boolean);
begin
   UseLabelLayer:=other;
end;

function TTextFileDef.GetUseLabelLayer:boolean;
begin
   result:=UseLabelLayer;
end;

procedure TTextFileDef.SetLabelLayer(const other:string);
begin
   LabelLayer:=other;
end;

function TTextFileDef.GetLabelLayer:string;
begin
   result:=LabelLayer;
end;

procedure TTextFileDef.SetDatabaseTableName(dbName:string);
begin
   DatabaseTableName:=dbName;
end;

function TTextFileDef.GetDatabaseTablename:string;
begin
   result:=DatabaseTableName;
end;

function TTextFileDef.GetFirstLineIsColumnDef:boolean;
begin
   result:=FirstLineIsColumnDef;
end;

procedure TTextFileDef.SetFirstLineIsColumnDef(other:boolean);
begin
   FirstLineIsColumnDef:=other;
end;

procedure TTextFileDef.SetInputFileType(aft:TInputFileType);
begin
   InputFileType:=aft;
end;

function TTextFileDef.GetInputFileType:TInputFileType;
begin
   result:=InputFileType;
end;

procedure TTextFileDef.SetProjection(CoordSys:integer; ProjSet:string; Proj:string; ProjDate:string; XO:double; YO:double; Sc:double);
begin
   SourceCoordinateSystem:=CoordSys;
   SourceProjSettings:=ProjSet;
   SourceProjection:=Proj;
   SourceDate:=ProjDate;
   SourceXOffset:=XO;
   SourceYOffset:=YO;
   SourceScale:=Sc;
end;

procedure TTextFileDef.GetProjection(var CoordSys:integer; var ProjSet:string; var Proj:string; var ProjDate:string; var XO:double; var YO:double; var Sc:double);
begin
   CoordSys:=SourceCoordinateSystem;
   ProjSet:=SourceProjSettings;
   Proj:=SourceProjection;
   ProjDate:=SourceDate;
   XO:=SourceXOffset;
   YO:=SourceYOffset;
   Sc:=SourceScale;
end;

procedure TTextFileDef.SetIgnoreDuplicates(IgnoreDupl:boolean);
begin
   IgnoreDuplicates:=IgnoreDupl;
end;

function TTextFileDef.GetIgnoreDuplicates:boolean;
begin
   result:=IgnoreDuplicates;
end;

procedure TTextFileDef.SetLabelData(aFont:string; aSize:integer; aAngle:integer; hori:integer; verti:integer; Offset:integer);
begin
   LabelFont:=aFont;
   LabelSize:=aSize;
   LabelAngle:=aAngle;
   Label_horizontal:=hori;
   Label_vertical:=verti;
   Label_Offset:=Offset;
end;

procedure TTextFileDef.GetLabelData(var aFont:string; var aSize:integer; var aAngle:integer; var hori:integer; var verti:integer; var Offset:integer);
begin
  aFont:=LabelFont;
  aSize:=LabelSize;
  aAngle:=LabelAngle;
  hori:=Label_horizontal;
  verti:=Label_vertical;
  Offset:=Label_Offset;
end;

function TTextFileDef.UseLabelData:boolean;
begin
   result:=UseLabel;
end;

procedure TTextFileDef.SetMaxLineLen(alen:integer);
begin
  maxlinelen:=alen;
end;

procedure TTextFileDef.SetDelimeterType(atype:TDelimeterType);
begin
   DelimeterType:=atype;
end;

function TTextFileDef.GetDelimeterType:TDelimeterType;
begin
   result:=DelimeterType;
end;

procedure TTextFileDef.SetObjectType(atype:integer);
begin
   ObjectType:=atype;
end;

function TTextFileDef.GetObjectType:integer;
begin
   result:=ObjectType;
end;


procedure TTextFileDef.SetDelimeterChar(adelim:string);
begin
   DelimeterChar:=adelim;
end;

function TTextFileDef.GetDelimeterChar:string;
begin
   result:=DelimeterChar;
end;

procedure TTextFileDef.AddFixColDelimeter(adelim:integer);
begin
   FixColDelimetersCount:=FixColDelimetersCount+1;
   SetLength(FixColDelimeters, FixColDelimetersCount);
   FixColDelimeters[FixColDelimetersCount-1]:=adelim;
end;

function TTextFileDef.GetNumFixColDelimeters:integer;
begin
   result:=FixColDelimetersCount;
end;

function TTextFileDef.GetFixColDelimeter(idx:integer):integer;
begin
   result:=FixColDelimeters[idx];
end;

function TTextFileDef.GetNumColumnDefs:integer;
begin
   result:=ColumnDefinitionsCount;
end;

procedure TTextFileDef.GetColumnDef(idx:integer; var apos:integer; var atype:TColumnType; var aColname:string);
var iterate:pColDefItem;
begin
   iterate:=ColumnDefinitions[idx];
   apos:=iterate^.pos;
   atype:=iterate^.coltype;
   aColname:=iterate^.Columnname;
end;

function TTextFileDef.GetPositionOfColumnDef(atype:TColumnType):integer;
var retVal:integer;
    iterate:pColDefItem;
    i:integer;
begin
   retVal:=-1;    // -1 means not found
   iterate:=nil;
   for i:=0 to ColumnDefinitionsCount-1 do
   begin
      if ColumnDefinitions[i]^.coltype = atype then
      begin
         iterate:=ColumnDefinitions[i];
         break;
      end;
   end;
   if iterate <> nil then
   begin
      retVal:=iterate^.pos;
   end;
   result:=retVal;
end;

procedure TTextFileDef.AddColumnDef(pos:integer; atype:TColumnType);
var newitem:pColDefItem;
begin
   ColumnDefinitionsCount:=ColumnDefinitionsCount+1;
   SetLength(ColumnDefinitions, ColumnDefinitionsCount);
   newitem:=new(pColDefItem);
   newitem^.pos:=pos;
   newitem^.coltype:=atype;
   if atype = col_Label then UseLabel:=TRUE;
   newitem^.Columnname:=''; // empty database-column name
   ColumnDefinitions[ColumnDefinitionsCount-1]:=newitem;
end;

procedure TTextFileDef.AddDatabaseColumnDef(pos:integer; Columnname:string);
var newitem:pColDefItem;
begin
   ColumnDefinitionsCount:=ColumnDefinitionsCount+1;
   SetLength(ColumnDefinitions, ColumnDefinitionsCount);
   newitem:=new(pColDefItem);
   newitem^.pos:=pos;
   newitem^.coltype:=col_Database;
   newitem^.Columnname:=Columnname;
   ColumnDefinitions[ColumnDefinitionsCount-1]:=newitem;
end;



function TTextFileDef.ExtractData(DataLine:string; atype:TColumnType; var ResData:string):boolean;
var retVal:boolean;
    i     :integer;
    iterate:pColDefItem;
    curpos :integer;
    totoken:integer;
    SubString:string;
begin
   retVal:=FALSE; ResData:='';
   // first check if the data of this type has to be extracted
   iterate:=nil;
   for i:=0 to ColumnDefinitionsCount-1 do
   begin
      if ColumnDefinitions[i]^.coltype = atype then
      begin
         iterate:=ColumnDefinitions[i];
         break;
      end;
   end;
   if iterate <> nil then
   begin
      // extract the data on it�s specified position
      if  Delimetertype = delim_FixColumns then
      begin
         curpos:=0;
         while (curpos < FixColDelimetersCount) and (not retVal) do  // extract the tokens
         begin
            // get to token
            if curpos = 0 then
               totoken:=FixColDelimeters[curpos]
            else
               totoken:=FixColDelimeters[curpos]- FixColDelimeters[curpos-1];
             SubString := Copy(DataLine, 1, totoken); // we got the substring
             Delete(DataLine, 1, totoken);   // delete till next token
             if curpos = iterate^.pos then
             begin
                resData:=SubString;
                retVal:=TRUE;
             end;
             curpos:=curpos+1; // add column index
         end;
         if (not retVal) and (curpos = iterate^.pos) then
         begin
            resData:=DataLine;
            retVal:=TRUE;
         end;
      end
      else if DelimeterType = delim_Character then
      begin
         curpos:=0;
         if (DelimeterChar = ' ') then // there can be more spaces as seperator, so compress them
            Q_SpaceCompressInPlace(DataLine);
         while (pos(DelimeterChar, DataLine) <> 0) and (not retVal) do  // extract the tokens
         begin
            SubString := Copy(DataLine, 1, pos(DelimeterChar, DataLine) - 1); // we got the substring
            Delete(DataLine, 1, Length(SubString) + Length(DelimeterChar));   // delete till next token
            Trim(SubString);
            if (curpos = iterate^.pos) then
            begin
               resData:=SubString;
               retVal:=TRUE;
            end;
            curpos:=curpos+1; // add column index
         end;
         if (not retVal) and (curpos = iterate^.pos) then
         begin
            resData:=DataLine;
            retVal:=TRUE;
         end;
      end;
   end;
   resData:=Trim(resData);
   result:=retVal;
end;

function TTextFileDef.ExtractDatabaseData(DataLine:string; aDbColname:string; var ResData:string):boolean;
var retVal:boolean;
    i     :integer;
    iterate:pColDefItem;
    curpos :integer;
    totoken:integer;
    SubString:string;
begin
   retVal:=FALSE; ResData:='';
   // first check if the data of this type has to be extracted
   iterate:=nil;
   for i:=0 to ColumnDefinitionsCount-1 do
   begin
      if (ColumnDefinitions[i]^.coltype = col_Database) and (ColumnDefinitions[i]^.Columnname = aDbColname) then
      begin
         iterate:=ColumnDefinitions[i];
         break;
      end;
   end;
   if iterate <> nil then
   begin
      // extract the data on it�s specified position
      if  Delimetertype = delim_FixColumns then
      begin
         curpos:=0;
         while (curpos < FixColDelimetersCount) and (not retVal) do  // extract the tokens
         begin
            // get to token
            if curpos = 0 then
               totoken:=FixColDelimeters[curpos]
            else
               totoken:=FixColDelimeters[curpos]- FixColDelimeters[curpos-1];
             SubString := Copy(DataLine, 1, totoken); // we got the substring
             Delete(DataLine, 1, totoken);   // delete till next token
             if curpos = iterate^.pos then
             begin
                resData:=SubString;
                retVal:=TRUE;
             end;
             curpos:=curpos+1; // add column index
         end;
         if (not retVal) and (curpos = iterate^.pos) then
         begin
            resData:=DataLine;
            retVal:=TRUE;
         end;
      end
      else if DelimeterType = delim_Character then
      begin
         curpos:=0;
         if (DelimeterChar = ' ') then // there can be more spaces as seperator, so compress them
            Q_SpaceCompressInPlace(DataLine);
         while (pos(DelimeterChar, DataLine) <> 0) and (not retVal) do  // extract the tokens
         begin
            SubString := Copy(DataLine, 1, pos(DelimeterChar, DataLine) - 1); // we got the substring
            Delete(DataLine, 1, Length(SubString) + Length(DelimeterChar));   // delete till next token
            Trim(SubString);
            if (curpos = iterate^.pos) then
            begin
               resData:=SubString;
               retVal:=TRUE;
            end;
            curpos:=curpos+1; // add column index
         end;
         if (not retVal) and (curpos = iterate^.pos) then
         begin
            resData:=DataLine;
            retVal:=TRUE;
         end;
      end;
   end;
   resData:=Trim(resData);
   result:=retVal;
end;

procedure TTextFileDef.SaveToFile(aFilename:string);
var fExt:string;
begin
   if pos('*',aFileName) <> 0 then exit; // to prevent saving of filename '*.tdf'

   // method is used to store current settings to file
   // first check if an .ADF file should be read or an .TDF (Text Definition File)
   fExt:=ExtractFileExt(aFilename);
   fExt:=UpperCase(fExt);
   if fExt = '.ADF' then
      SaveAdfToFile(aFilename)
   else if fExt = '.TDF' then
      SaveTdfToFile(aFilename);
end;

procedure TTextFileDef.SaveAdfToFile(aFilename:string);
var aTextFile:TextFile;
    fromval, toval:integer;
    fromstr, tostr:string;
    outline:string;
    i:integer;
begin
  // method is used to save current settings to .adf file
  {$I-}
  AssignFile(aTextFile,aFilename);
  Rewrite(aTextFile);
  // write the Symoblscale
  writeln(aTextFile,'SYMBOLSCALE');
  writeln(aTextFile,'1.0');

  // write the X-Coordinate
  writeln(aTextFile,'X-COORDINATE');
  GetAdfFromToValue(col_X, fromval, toval);
  fromstr:=inttostr(fromval); tostr:=inttostr(toval);
  // format the string
  while (length(fromstr) < 3) do fromstr:='0'+fromstr;
  while (length(tostr) < 3)   do tostr:='0'+tostr;
  outline:=fromstr+' '+tostr;
  writeln(aTextFile, outline);

  // write the Y-Coordinate
  writeln(aTextFile,'Y-COORDINATE');
  GetAdfFromToValue(col_Y, fromval, toval);
  fromstr:=inttostr(fromval); tostr:=inttostr(toval);
  // format the string
  while (length(fromstr) < 3) do fromstr:='0'+fromstr;
  while (length(tostr) < 3)   do tostr:='0'+tostr;
  outline:=fromstr+' '+tostr;
  writeln(aTextFile, outline);

  // write Parameter-A
  writeln(aTextFile,'PARAMETERA');
  GetAdfFromToValue(col_ParamA, fromval, toval);
  fromstr:=inttostr(fromval); tostr:=inttostr(toval);
  // format the string
  while (length(fromstr) < 3) do fromstr:='0'+fromstr;
  while (length(tostr) < 3)   do tostr:='0'+tostr;
  outline:=fromstr+' '+tostr;
  writeln(aTextFile, outline);

  // write Parameter-B
  writeln(aTextFile,'PARAMETERB');
  GetAdfFromToValue(col_ParamB, fromval, toval);
  fromstr:=inttostr(fromval); tostr:=inttostr(toval);
  // format the string
  while (length(fromstr) < 3) do fromstr:='0'+fromstr;
  while (length(tostr) < 3)   do tostr:='0'+tostr;
  outline:=fromstr+' '+tostr;
  writeln(aTextFile, outline);

  // write Parameter-C
  writeln(aTextFile,'PARAMETERC');
  GetAdfFromToValue(col_ParamC, fromval, toval);
  fromstr:=inttostr(fromval); tostr:=inttostr(toval);
  // format the string
  while (length(fromstr) < 3) do fromstr:='0'+fromstr;
  while (length(tostr) < 3)   do tostr:='0'+tostr;
  outline:=fromstr+' '+tostr;
  writeln(aTextFile, outline);

  // write the layername
  writeln(aTextFile,'LAYERNAME');
  GetAdfFromToValue(col_Layer, fromval, toval);
  fromstr:=inttostr(fromval); tostr:=inttostr(toval);
  // format the string
  while (length(fromstr) < 3) do fromstr:='0'+fromstr;
  while (length(tostr) < 3)   do tostr:='0'+tostr;
  outline:=fromstr+' '+tostr;
  writeln(aTextFile, outline);

  // write the object-type
  writeln(aTextFile,'OBJEKTTYP');
  GetAdfFromToValue(col_ObjectType, fromval, toval);
  fromstr:=inttostr(fromval); tostr:=inttostr(toval);
  // format the string
  while (length(fromstr) < 3) do fromstr:='0'+fromstr;
  while (length(tostr) < 3)   do tostr:='0'+tostr;
  outline:=fromstr+' '+tostr;
  writeln(aTextFile, outline);

  // write the object-number
  writeln(aTextFile,'OBJEKTNR');
  GetAdfFromToValue(col_ObjectNr, fromval, toval);
  fromstr:=inttostr(fromval); tostr:=inttostr(toval);
  // format the string
  while (length(fromstr) < 3) do fromstr:='0'+fromstr;
  while (length(tostr) < 3)   do tostr:='0'+tostr;
  outline:=fromstr+' '+tostr;
  writeln(aTextFile, outline);

  writeln(aTextFile,'');
  // write the graphic column
  writeln(aTextFile,'GRAFIK');
  GetAdfFromToValue(col_Graphic, fromval, toval);
  fromstr:=inttostr(fromval); tostr:=inttostr(toval);
  // format the string
  while (length(fromstr) < 3) do fromstr:='0'+fromstr;
  while (length(tostr) < 3)   do tostr:='0'+tostr;
  outline:=fromstr+' '+tostr;
  writeln(aTextFile, outline);

  // now write the database columns
  if HasDatabaseColumns then
  begin
     for i:=0 to ColumnDefinitionsCount-1 do
     begin
        if ColumnDefinitions[i]^.coltype = col_Database then
        begin
           writeln(aTextFile, ColumnDefinitions[i]^.ColumnName);
           GetAdfDatabaseFromToValue(ColumnDefinitions[i]^.ColumnName, fromval, toval);
           fromstr:=inttostr(fromval); tostr:=inttostr(toval);
           // format the string
           while (length(fromstr) < 3) do fromstr:='0'+fromstr;
           while (length(tostr) < 3)   do tostr:='0'+tostr;
           outline:=fromstr+' '+tostr;
           writeln(aTextFile, outline);
        end;
     end;
  end;
  CloseFile(aTextFile);
  {$I+}
end;

procedure TTextFileDef.GetAdfDatabaseFromToValue(aDbColname:string; var fromval:integer; var toval:integer);
var curfrom, curto:integer;
    i:integer;
    iterate:pColDefItem;
begin
   // method is used to get the from and to column from an .adf column
   curfrom:=1;
   curto:=1;
   iterate:=nil;
   for i:=0 to ColumnDefinitionsCount-1 do
   begin
      if (ColumnDefinitions[i]^.coltype = col_Database) and (ColumnDefinitions[i]^.Columnname = aDbColname)  then
      begin
         iterate:=ColumnDefinitions[i];
         break;
      end;
   end;
   if iterate <> nil then
   begin
      // get the from value
      // check if it is the first position
      if iterate^.pos = 0 then
         fromval:=1  // the from column is 1 then
      else
         fromval:=FixColDelimeters[iterate^.pos-1]+1; // get the from column from the delimiter
      // get the to value
      if iterate^.pos = FixColDelimetersCount then
         toval:=maxlinelen
      else
         toval:=FixColDelimeters[iterate^.pos];
   end
   else
   begin
      fromval:=0;
      toval:=0;
   end;
end;

procedure TTextFileDef.GetAdfFromToValue(coltype:TColumnType; var fromval:integer; var toval:integer);
var curfrom, curto:integer;
    i:integer;
    iterate:pColDefItem;
begin
   // method is used to get the from and to column from an .adf column
   curfrom:=1;
   curto:=1;
   iterate:=nil;
   for i:=0 to ColumnDefinitionsCount-1 do
   begin
      if ColumnDefinitions[i]^.coltype = coltype then
      begin
         iterate:=ColumnDefinitions[i];
         break;
      end;
   end;
   if iterate <> nil then
   begin
      // get the from value
      // check if it is the first position
      if iterate^.pos = 0 then
         fromval:=1  // the from column is 1 then
      else
         fromval:=FixColDelimeters[iterate^.pos-1]+1; // get the from column from the delimiter
      // get the to value
      if iterate^.pos = FixColDelimetersCount then
         toval:=maxlinelen
      else
         toval:=FixColDelimeters[iterate^.pos];
   end
   else
   begin
      fromval:=0;
      toval:=0;
   end;
end;

procedure TTextFileDef.SaveTdfToFile(aFilename:string);
var inifile:TIniFile;
    i:integer;
    itemname:string;
    ColName:string;
    tmpTxtDef : TTextFileDef;
begin
   if FileExists(aFilename) then // if the file already exists it can be deleted
      DeleteFile(aFilename);
   inifile:=TIniFile.Create(aFilename);
   // write the inputfiletype
        if InputFileType = ft_TextFile then inifile.WriteString('SETTINGS','InputFileType','ft_TextFile')
   else if InputFileType = ft_dBase    then inifile.WriteString('SETTINGS','InputFileType','ft_dBase')
   else if InputFileType = ft_Access   then inifile.WriteString('SETTINGS','InputFileType','ft_Access');

   // write the delimter settings
   if InputFileType = ft_TextFile then
   begin
      if DelimeterType = delim_Character then
      begin
         // write the delimter-type
         inifile.WriteString('SETTINGS','DelimeterType','delim_Character');
         // check if the DelimiterChar is space or tab - in this case it will
         // be mapped to the strings SPACE, or TAB
         if DelimeterChar = ' ' then
            ColName:='SPACE'
         else if DelimeterChar = chr(9) then
            ColName:='TAB'
         else
            ColName:=DelimeterChar;
         inifile.WriteString('SETTINGS','DelimeterChar',ColName);
      end
      else if DelimeterType = delim_FixColumns then
      begin
         inifile.WriteString('SETTINGS','DelimeterType','delim_FixColumns');
         // now write the delimters
         for i:=0 to FixColDelimetersCount-1 do
         begin
            ItemName:='ITEM'+inttostr(i);
            inifile.WriteString('FIXCOLDELIMETERS', ItemName, inttostr(FixColDelimeters[i]));
         end;
      end;
   end
   else if (InputfileType = ft_dBase) or (InputfileType = ft_Access) then
   begin
      inifile.WriteString('SETTINGS','DelimeterType','delim_Database');
      inifile.WriteString('SETTINGS','DefaultTable',DatabaseTableName);
   end;

   // write the column name definitions
   for i:=0 to ColumnDefinitionsCount-1 do
   begin
      ItemName:='ITEM'+inttostr(i);
      ColName:='';
      case ColumnDefinitions[i]^.coltype of
         col_Label          : ColName:='col_Label';
         col_LabelMarker    : ColName:='col_LabelMarker';
         col_Layer          : ColName:='col_Layer';
         col_X              : ColName:='col_X';
         col_Y              : ColName:='col_Y';
         col_Radius         : ColName:='col_Radius';
         col_SymbolName     : ColName:='col_SymbolName';
         col_SymbolSize     : ColName:='col_SymbolSize';
         col_SymbolRotation : ColName:='col_SymbolRotation';
         col_SymbolIndex    : ColName:='col_SymbolIndex';
         col_ParamA         : ColName:='col_ParamA';
         col_ParamB         : ColName:='col_ParamB';
         col_ParamC         : ColName:='col_ParamC';
         col_ObjectType     : ColName:='col_ObjectType';
         col_ObjectNr       : ColName:='col_ObjectNr';
         col_Graphic        : ColName:='col_Graphic';
         col_Database       : ColName:='col_Database';
         col_PolySeperator  : ColName:='col_PolySeperator';
         col_ProgisId       : ColName:='col_ProgisId';
         col_GpsSymDir      : ColName:='col_GpsSymDir';
         col_GpsSymLineType : ColName:='col_GpsSymLineType';
         col_GpsSymLineColor: ColName:='col_GpsSymLineColor';
         col_GpsSymFillType : ColName:='col_GpsSymFillType';
         col_GpsSymFillColor: ColName:='col_GpsSymFillColor';
         col_GpsLineType    : ColName:='col_GpsLineType';
         col_GpsLineColor   : ColName:='col_GpsLineColor';
         col_GpsLineWidth   : ColName:='col_GpsLineWidth';
      end;
      inifile.WriteString('COLUMNDEFINITIONS',ItemName,ColName+','+inttostr(ColumnDefinitions[i]^.pos)+','+ColumnDefinitions[i]^.Columnname);
   end;
   if FirstLineIsColumnDef then
      inifile.WriteString('SETTINGS','FirstLineIsColumnDef','TRUE')
   else
      inifile.WriteString('SETTINGS','FirstLineIsColumnDef','FALSE');

   // write object-type settings
   inifile.WriteString('SETTINGS','ObjectType',inttostr(ObjectType));

   // write label settings
   if UseLabel then
   begin
      inifile.WriteString('SETTINGS','UseLabel','TRUE');
      inifile.WriteString('LABEL','LabelFont',LabelFont);
      inifile.WriteString('LABEL','LabelSize',inttostr(LabelSize));
      inifile.WriteString('LABEL','LabelAngle',inttostr(LabelAngle));
      inifile.WriteString('LABEL','Label_horizontal',inttostr(Label_horizontal));
      inifile.WriteString('LABEL','Label_vertical',inttostr(Label_vertical));
      inifile.WriteString('LABEL','Label_Offset',inttostr(Label_Offset));
      if UseLabellayer then
      begin
         inifile.WriteString('LABEL','UseLabelLayer','TRUE');
         inifile.WriteString('LABEL','LabelLayer',LabelLayer);
      end
      else
         inifile.WriteString('LABEL','UseLabelLayer','FALSE');

   end
   else
      inifile.WriteString('SETTINGS','UseLabel','FALSE');

  // write IgnoreDuplicates Flag
   if IgnoreDuplicates then
      inifile.WriteString('SETTINGS','IgnoreDuplicates','TRUE')
   else
      inifile.WriteString('SETTINGS','IgnoreDuplicates','FALSE');

   // write DefaultSymbolName
   inifile.WriteString('SETTINGS','DefaultSymbolName',DefaultSymbolName);

   // write projection settings
   inifile.WriteString('PROJECTION','SourceCoordinateSystem',inttostr(SourceCoordinateSystem));
   inifile.WriteString('PROJECTION','SourceProjSettings',SourceProjSettings);
   inifile.WriteString('PROJECTION','SourceProjection',SourceProjection);
   inifile.WriteString('PROJECTION','SourceDate',SourceDate);
   inifile.WriteString('PROJECTION','SourceXOffset',floattostr(SourceXOffset));
   inifile.WriteString('PROJECTION','SourceYOffset',floattostr(SourceYOffset));
   inifile.WriteString('PROJECTION','SourceScale',floattostr(SourceScale));

   // write database confguration

   inifile.WriteInteger('PRESETDATABASE','IMPDBMode',-1);

   if TextWizardwnd.SaveDBToTdfCB.Checked then
   begin

   tmpTxtDef:=TTextFileDef.Create;
   TextWizardWnd.SetupTextFileDefFromWindow(tmpTxtDef);

   if tmpTxtDef.HasDatabaseColumns and (MainWnd.AXImpExpDbc.ImpDbMode < 6 ) then
   	begin
   	inifile.WriteInteger('PRESETDATABASE','IMPDBMode',MainWnd.AXImpExpDbc.ImpDbMode);
   	inifile.WriteString('PRESETDATABASE','IMPTable',MainWnd.AXImpExpDbc.ImpTable);
   	inifile.WriteString('PRESETDATABASE','IMPDatabase',MainWnd.AXImpExpDbc.ImpDatabase);
   	end;
   tmpTxtDef.Free;
   end;

   inifile.Free;
end;

function TTextFileDef.LoadFromFile(aFilename:string; const inifilename:string):boolean;
var retVal:boolean;
    fExt:string;
begin
   retVal:=FALSE;
   // method is used to load settings from a file
   Reset;
      // method is used to store current settings to file
   // first check if an .ADF file should be read or an .TDF (Text Definition File)
   fExt:=ExtractFileExt(aFilename);
   fExt:=UpperCase(fExt);
   if fExt = '.ADF' then
      retVal:=LoadAdfFromFile(aFilename)
   else if fExt = '.TDF' then
      retVal:=LoadTdfFromFile(aFilename);


   ReadLabelPosMarkers(inifilename); // read also the label position markers
   result:=retVal;
end;

function TTextFileDef.LoadAdfFromFile(aFilename:string):boolean;
var retVal:boolean;
begin
   retVal:=TRUE;
   result:=retVal;
end;

function TTextFileDef.LoadTdfFromFile(aFilename:string):boolean;     
var retVal:boolean;
    inifile:TIniFile;
    dummy:string;
    iniitems:TStrings;
    intval, i,count:integer;
    typestr:string;
    atype  :TColumnType;
begin
   retVal:=TRUE;
   inifile:=TIniFile.Create(aFilename);
   // read the InputFileType
   dummy:=inifile.ReadString('SETTINGS','InputFileType','ft_TextFile');
        if dummy = 'ft_TextFile' then InputFileType:=ft_TextFile
   else if dummy = 'ft_dBase'    then InputFileType:=ft_dBase
   else if dummy = 'ft_Access'   then InputFileType:=ft_Access
   else InputFileType:=ft_TextFile; // default

   // read out the default database table
   DatabaseTableName:=inifile.ReadString('SETTINGS','DefaultTable','');

   // read out the Delimeter Type
   dummy:=inifile.ReadString('SETTINGS','DelimeterType','');
   // map the Delimeter Type
   if dummy = '' then
      retVal:=FALSE;
   if dummy = 'delim_Character' then
   begin
      DelimeterType:=delim_Character;
      dummy:=inifile.ReadString('SETTINGS','DelimeterChar','');
      // map the Delimeter Character
      if dummy = '' then
         retVal:=FALSE
      else if dummy = 'SPACE' then
         DelimeterChar:=' '
      else if dummy = 'TAB' then
         DelimeterChar:=chr(9)
      else
         DelimeterChar:=dummy;
   end
   else if dummy = 'delim_FixColumns' then
   begin
      DelimeterType:=delim_FixColumns;
      IniItems:=TStringList.Create;
      inifile.ReadSection('FIXCOLDELIMETERS',IniItems);
      for i:=0 to IniItems.Count-1 do
      begin
         dummy:=inifile.ReadString('FIXCOLDELIMETERS',IniItems[i],'');
         val(dummy,intval,count);
         AddFixColDelimeter(intval);
      end;
      IniItems.Destroy;
   end
   else if dummy = 'delim_Database' then
      DelimeterType:=delim_Database;

   // read out the column assignements
   IniItems:=TStringList.Create;
   inifile.ReadSection('COLUMNDEFINITIONS',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      dummy:=inifile.ReadString('COLUMNDEFINITIONS',IniItems[i],'');
      // extract type and position out of the string
      typestr:=copy(dummy,1,Pos(',',dummy)-1);
      delete(dummy,1,Pos(',',dummy));
      if      typestr = 'col_Label' then atype:=col_Label
      else if typestr = 'col_LabelMarker' then atype:=col_LabelMarker
      else if typestr = 'col_Layer' then atype:=col_Layer
      else if typestr = 'col_X'     then atype:=col_X
      else if typestr = 'col_Y'     then atype:=col_Y
      else if typestr = 'col_Radius' then atype:=col_Radius
      else if typestr = 'col_SymbolName' then atype:=col_SymbolName
      else if typestr = 'col_SymbolSize' then atype:=col_SymbolSize
      else if typestr = 'col_SymbolRotation' then atype:=col_SymbolRotation
      else if typestr = 'col_SymbolIndex' then atype:=col_SymbolIndex
      else if typestr = 'col_ParamA' then atype:=col_ParamA
      else if typestr = 'col_ParamB' then atype:=col_ParamB
      else if typestr = 'col_ParamC' then atype:=col_ParamC
      else if typestr = 'col_ObjectType' then atype:=col_ObjectType
      else if typestr = 'col_ObjectNr' then atype:=col_ObjectNr
      else if typestr = 'col_Graphic' then atype:=col_Graphic
      else if typestr = 'col_Database' then atype:=col_Database
      else if typestr = 'col_PolySeperator' then atype:=col_PolySeperator
      else if typestr = 'col_ProgisId' then atype:=col_ProgisId
      else if typestr = 'col_GpsSymDir' then atype:=col_GpsSymDir
      else if typestr = 'col_GpsSymLineType' then atype:=col_GpsSymLineType
      else if typestr = 'col_GpsSymLineColor' then atype:=col_GpsSymLineColor
      else if typestr = 'col_GpsSymFillType' then atype:=col_GpsSymFillType
      else if typestr = 'col_GpsSymFillColor' then atype:=col_GpsSymFillColor
      else if typestr = 'col_GpsLineType' then atype:=col_GpsLineType
      else if typestr = 'col_GpsLineColor' then atype:=col_GpsLineColor
      else if typestr = 'col_GpsLineWidth' then atype:=col_GpsLineWidth
      else
         retVal:=FALSE;
      typestr:=copy(dummy,1,Pos(',',dummy)-1);
      delete(dummy,1,Pos(',',dummy));
      val(typestr,intval,count);
      if atype <> col_Database then
         AddColumnDef(intval, atype)
      else
         AddDatabaseColumnDef(intval,dummy);
   end;
   iniitems.Destroy;

   dummy:=inifile.ReadString('SETTINGS','FirstLineIsColumnDef','FALSE');
   if dummy = 'TRUE' then FirstLineIsColumnDef:=TRUE else FirstLineIsColumnDef:=FALSE;

   dummy:=inifile.ReadString('SETTINGS','ObjectType','');
   val (dummy,intval,count);
   ObjectType:=intval;

   // read label settings
   dummy:=inifile.ReadString('SETTINGS','UseLabel','FALSE');
   if dummy = 'TRUE' then UseLabel:=TRUE else UseLabel:=FALSE;
   if UseLabel then
   begin
      LabelFont:=inifile.ReadString('LABEL','LabelFont','Courier New');
      dummy:=inifile.ReadString('LABEL','LabelSize','10');
      val(dummy, LabelSize, count); if count <> 0 then retVal:=FALSE;
      dummy:=inifile.ReadString('LABEL','LabelAngle','0');
      val(dummy, LabelAngle, count); if count <> 0 then retVal:=FALSE;
      dummy:=inifile.ReadString('LABEL','Label_horizontal','1');
      val(dummy, Label_horizontal, count); if count <> 0 then retVal:=FALSE;
      dummy:=inifile.ReadString('LABEL','Label_vertical','1');
      val(dummy, Label_vertical, count); if count <> 0 then retVal:=FALSE;
      dummy:=inifile.ReadString('LABEL','Label_Offset','0');
      val(dummy, Label_Offset, count);if count <> 0 then retVal:=FALSE;
      dummy:=inifile.ReadString('LABEL','UseLabelLayer','FALSE');
      if dummy = 'TRUE' then UseLabelLayer:=TRUE else UseLabelLayer:=FALSE;
      LabelLayer:=inifile.ReadString('LABEL','LabelLayer','');
   end;


   // read IgnoreDuplicates Flag
   dummy:=inifile.ReadString('SETTINGS','IgnoreDuplicates','FALSE');
   if dummy = 'TRUE' then IgnoreDuplicates:=TRUE else IgnoreDuplicates:=FALSE;

   // read DefaultSymbolname
   DefaultSymbolName:=inifile.ReadString('SETTINGS','DefaultSymbolName','');

   // read projection settings
   retval := True;

   dummy:=inifile.ReadString('PROJECTION','SourceCoordinateSystem','0');
   val(dummy,SourceCoordinateSystem,count); if count <> 0 then retVal:=FALSE;

   SourceProjSettings:=inifile.ReadString('PROJECTION','SourceProjSettings','NONE');
   SourceProjection:=inifile.ReadString('PROJECTION','SourceProjection','NONE');
   SourceDate:=inifile.ReadString('PROJECTION','SourceDate','NONE');

   dummy:=inifile.ReadString('PROJECTION','SourceXOffset','0');
   val(dummy,SourceXOffset,count); if count <> 0 then retVal:=FALSE;

   dummy:=inifile.ReadString('PROJECTION','SourceYOffset','0');
   val(dummy,SourceYOffset,count); if count <> 0 then retVal:=FALSE;

   dummy:=inifile.ReadString('PROJECTION','SourceScale','1.0');
   val(dummy,SourceScale,count); if count <> 0 then retVal:=FALSE;

   if not retVal then
      Reset;
   result:=retVal;
end;

function TTextFileDef.HasDatabaseColumns:boolean;
var i:integer;
begin
   // function is used to check if database-columns are used
   result:=FALSE;
   for i:=0 to ColumnDefinitionsCount-1 do
   begin
      if ColumnDefinitions[i]^.coltype = col_Database then
      begin
         result:=TRUE;
         break;
      end;
   end;
end;

function TTextFileDef.IsAutoAssignementColumn(ColName:string; ColType:TColumnType):boolean;
var i:integer;
    retVal:boolean;
    Name1:string;
    Name2:string;
begin
   retVal:=FALSE;
   Name1:=Uppercase(ColName);
   for i:=0 to AutoColumnsCount-1 do
   begin
      Name2:=Uppercase(AutoColumns[i]^.Columnname);
      if (Name1 = Name2) and (AutoColumns[i]^.coltype = ColType) then
      begin
         retVal:=TRUE;
         break;
      end;
   end;
   result:=retVal;
end;

procedure TTextFileDef.SetupAutoColumnAssignments(inifilename:string);
var inifile:TIniFile;
    IniItems:TStrings;
    newitem:pColDefItem;
    colname:string;
    i:integer;
begin

   // avoid reading out the automatic columns assignement choices twice, because they
   // do not change on runtime
   if AutoColumnsCount > 0 then
      exit;

   inifile:=TIniFile.Create(inifilename);
   IniItems:=TStringList.Create;

   // read out the col_Label choices
   inifile.ReadSection('col_Label',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_Label',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_Label;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_Layer choices
   IniItems.Clear;
   inifile.ReadSection('col_Layer',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_Layer',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_Layer;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_X choices
   IniItems.Clear;
   inifile.ReadSection('col_X',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_X',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_X;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_Y choices
   IniItems.Clear;
   inifile.ReadSection('col_Y',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_Y',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_Y;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_Radius choices
   IniItems.Clear;
   inifile.ReadSection('col_Radius',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_Radius',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_Radius;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_SymbolName choices
   IniItems.Clear;
   inifile.ReadSection('col_SymbolName',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_SymbolName',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_SymbolName;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_SymbolIndex choices
   IniItems.Clear;
   inifile.ReadSection('col_SymbolIndex',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_SymbolIndex',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_SymbolIndex;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

      // read out the col_SymbolSize choices
   IniItems.Clear;
   inifile.ReadSection('col_SymbolSize',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_SymbolSize',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_SymbolSize;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_SymbolRotation choices
   IniItems.Clear;
   inifile.ReadSection('col_SymbolRotation',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_SymbolRotation',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_SymbolRotation;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_PolySeperator choices
   IniItems.Clear;
   inifile.ReadSection('col_PolySeperator',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_PolySeperator',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_PolySeperator;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_LabelMarker choices
   IniItems.Clear;
   inifile.ReadSection('col_LabelMarker',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_LabelMarker',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_LabelMarker;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_ProgisId choices
   IniItems.Clear;
   inifile.ReadSection('col_ProgisId',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_ProgisId',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_ProgisId;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_GpsSymDir choices
   IniItems.Clear;
   inifile.ReadSection('col_GpsSymDir',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_GpsSymDir',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_GpsSymDir;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_GpsSymLineType choices
   IniItems.Clear;
   inifile.ReadSection('col_GpsSymLineType',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_GpsSymLineType',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_GpsSymLineType;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_GpsSymLineType choices
   IniItems.Clear;
   inifile.ReadSection('col_GpsSymLineColor',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_GpsSymLineColor',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_GpsSymLineColor;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_GpsSymFillType choices
   IniItems.Clear;
   inifile.ReadSection('col_GpsSymFillType',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_GpsSymFillType',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_GpsSymFillType;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_GpsSymFillcolor choices
   IniItems.Clear;
   inifile.ReadSection('col_GpsSymFillcolor',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_GpsSymFillcolor',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_GpsSymFillcolor;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_GpsLineType choices
   IniItems.Clear;
   inifile.ReadSection('col_GpsLineType',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_GpsLineType',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_GpsLineType;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_GpsLineColor choices
   IniItems.Clear;
   inifile.ReadSection('col_GpsLineColor',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_GpsLineColor',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_GpsLineColor;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   // read out the col_GpsLineWidth choices
   IniItems.Clear;
   inifile.ReadSection('col_GpsLineWidth',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      colname:=inifile.ReadString('col_GpsLineWidth',IniItems[i],'');
      // add item to Auto column assignements
      AutoColumnsCount:=AutoColumnsCount+1;
      SetLength(AutoColumns, AutoColumnsCount);
      newitem:=new(pColDefItem);
      newitem^.pos:=0; // pos is not used here
      newitem^.coltype:=col_GpsLineWidth;      // setup the type
      newitem^.Columnname:=colname; // setup the column-name
      AutoColumns[AutoColumnsCount-1]:=newitem;
   end;

   IniItems.Destroy;
   inifile.Free;

   // read out the Label positioning markers
   ReadLabelPosMarkers(inifilename);
end;

procedure TTextFileDef.WriteLabelPosMarkers(const inifilename:string);
var inifile:TIniFile;
    i:integer;
    iterate: pLabelMarkerItem;
    sectionname:string;
    itemkey:string;
    itemdata:string;
begin
   // method is used to write label positioning markers back to inifile
   if LabelMarkersCount > 0 then
   begin
      inifile:=TIniFile.Create(inifilename);
      for i:=0 to LabelMarkersCount-1 do
      begin
         iterate:=LabelMarkers[i];
         itemkey:='Item'+inttostr(i);
         itemdata:='LABELMARKER'+inttostr(i);
         inifile.WriteString('LABELMARKERS',itemkey, itemdata);
         // write the section of the labelmarker
         sectionname:=itemdata;
         inifile.WriteString(sectionname,'LabelMarker', iterate^.Name);
         inifile.WriteString(sectionname,'LabelFont', iterate^.LabelFont);
         inifile.WriteString(sectionname,'LabelSize', inttostr(iterate^.LabelSize));
         inifile.WriteString(sectionname,'LabelAngle', inttostr(iterate^.LabelAngle));
         inifile.WriteString(sectionname,'Label_horizontal',inttostr(iterate^.Label_horizontal));
         inifile.WriteString(sectionname,'Label_vertical',inttostr(iterate^.Label_vertical));
         inifile.WriteString(sectionname,'Label_Offset', inttostr(iterate^.Label_Offset));
      end;
      inifile.Free;
   end;
end;

procedure TTextFileDef.ReadLabelPosMarkers(const inifilename:string);
var inifile:TIniFile;
    IniItems:TStrings;
    i:integer;
    sectionname:string;
    Marker_name:string;
    Marker_font:string;
    dummy:string;
    Marker_Size,count, Marker_Angle, Marker_hori, Marker_verti:integer;
    iterate:pLabelMarkerItem;
    Marker_Offset:integer;
begin
   // read out the label markers
   // remove the old list of the markers
   if LabelMarkersCount > 0 then
   begin
      for i:=0 to LabelMarkersCount-1 do
      begin
         iterate:=LabelMarkers[i];
         dispose(iterate);
      end;
      SetLength(LabelMarkers, 0);
      LabelMarkersCount:=0;
   end;

   inifile:=TIniFile.Create(inifilename);
   IniItems:=TStringList.Create;
   inifile.ReadSection('LABELMARKERS',IniItems);
   for i:=0 to IniItems.Count-1 do
   begin
      sectionname:=inifile.ReadString('LABELMARKERS', IniItems[i],'');
      if sectionname <> '' then
      begin
         // read out a label-positioning marker
         Marker_Name:=inifile.ReadString(sectionname,'LabelMarker','');
         Marker_Font:=inifile.ReadString(sectionname,'LabelFont','');
         dummy:=inifile.ReadString(sectionname,'LabelSize','');
         val(dummy, Marker_Size, count);
         dummy:=inifile.ReadString(sectionname,'LabelAngle','');
         val(dummy, Marker_Angle, count);
         dummy:=inifile.ReadString(sectionname,'Label_horizontal','');
         val(dummy, Marker_hori, count);
         dummy:=inifile.ReadString(sectionname,'Label_vertical','');
         val(dummy, Marker_verti, count);
         dummy:=inifile.ReadString(sectionname,'Label_Offset','0');
         val(dummy, Marker_Offset, count);
         SetupLabelPosMarker(Marker_Name, Marker_Font, Marker_Size, Marker_Angle, Marker_hori, Marker_verti, Marker_Offset);
      end;
   end;
   iniitems.Destroy;
   inifile.Free;
end;

function TTextFileDef.GetNumLabelPosMarkers:integer;
begin
   result:=LabelMarkersCount;
end;

procedure TTextFileDef.SetupLabelPosMarker(const Marker_Name:string; const Marker_Font:string;
                                           const Marker_Size:integer; const Marker_Angle:integer;
                                           const Marker_hori:integer; const Marker_verti:integer;
                                           const Marker_Offset:integer
                                           );
var iterate:pLabelMarkerItem;
    newitem:pLabelMarkerItem;
    i:integer;
    aName:string;
begin
   // first check if there is already a Marker with this name defined
   iterate:=nil;
   newitem:=nil;
   aName:=Marker_Name;
   aName:=Trim(aName);
   if LabelMarkersCount > 0 then
   begin
      for i:=0 to LabelMarkersCount-1 do
      begin
         iterate:=LabelMarkers[i];
         if iterate^.Name = aName then
         begin
            newitem:=iterate;
            break;
         end;
      end;
   end;
   if newitem <> nil then
   begin
      // item already exists it has to be modified
      newitem^.LabelFont:=Marker_Font;
      newitem^.LabelSize:=Marker_Size;
      newitem^.LabelAngle:=Marker_Angle;
      newitem^.Label_horizontal:=Marker_hori;
      newitem^.Label_vertical:=Marker_verti;
      newitem^.Label_Offset:=Marker_Offset;
   end
   else
   begin
      // add new item to list
      newitem:=new(pLabelMarkerItem);
      newitem^.Name:=aName;
      newitem^.LabelFont:=Marker_Font;
      newitem^.LabelSize:=Marker_Size;
      newitem^.LabelAngle:=Marker_Angle;
      newitem^.Label_horizontal:=Marker_hori;
      newitem^.Label_vertical:=Marker_verti;
      newitem^.Label_Offset:=Marker_Offset;

      LabelMarkersCount:=LabelMarkersCount+1;
      SetLength(LabelMarkers, LabelMarkersCount);
      LabelMarkers[LabelMarkersCount-1]:=newitem;
   end;
end;

function TTextFileDef.GetLabelPosMarker(const Marker_Name:string; var Marker_Font:string;
                                        var Marker_Size:integer; var Marker_Angle:integer;
                                        var Marker_hori:integer; var Marker_verti:integer;
                                        var Marker_Offset:integer):boolean;
var iterate:pLabelMarkerItem;
    modifyitem:pLabelMarkerItem;
    i:integer;
    aName:string;
begin
   // method is used to get the label positioning mark data by the marker name
   iterate:=nil;
   modifyitem:=nil;
   aName:=Marker_Name;
   aName:=Trim(aName); // trim the name
   if LabelMarkersCount > 0 then
   begin
      for i:=0 to LabelMarkersCount-1 do
      begin
         iterate:=LabelMarkers[i];
         if iterate^.Name = aName then
         begin
            modifyitem:=iterate;
            Marker_Font:=modifyitem^.LabelFont;
            Marker_Size:=modifyitem^.LabelSize;
            Marker_Angle:=modifyitem^.LabelAngle;
            Marker_hori:=modifyitem^.Label_horizontal;
            Marker_verti:=modifyitem^.Label_vertical;
            Marker_Offset:=modifyitem^.Label_Offset;
            break;
         end;
      end;
   end;
   if modifyitem <> nil then result:=TRUE else result:=FALSE;
end;

function TTextFileDef.GetLabelPosMarkerByIdx(const Marker_Idx:integer; var Marker_Name:string; var Marker_Font:string;
                                             var Marker_Size:integer; var Marker_Angle:integer;
                                             var Marker_hori:integer; var Marker_verti:integer;
                                             var Marker_Offset:integer):boolean;
var retVal:boolean;
    iterate:pLabelMarkerItem;
begin
   // method is used to get the label positioning data by the index
   retVal:=FALSE;
   if (Marker_Idx >= 0) and (Marker_Idx < LabelMarkersCount) then
   begin
      iterate:=LabelMarkers[Marker_Idx];
      Marker_Name:=iterate^.Name;
      Marker_Font:=iterate^.LabelFont;
      Marker_Size:=iterate^.LabelSize;
      Marker_Angle:=iterate^.LabelAngle;
      Marker_hori:=iterate^.Label_horizontal;
      Marker_verti:=iterate^.Label_vertical;
      Marker_Offset:=iterate^.Label_Offset;
      retVal:=TRUE;
   end;
   result:=retVal;
end;

end.
