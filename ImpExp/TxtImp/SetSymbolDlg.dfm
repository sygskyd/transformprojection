object SetSymbolWnd: TSetSymbolWnd
  Left = 277
  Top = 285
  BorderStyle = bsDialog
  Caption = 'Set default symbol'
  ClientHeight = 63
  ClientWidth = 290
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object SymbolCb: TComboBox
    Left = 4
    Top = 4
    Width = 281
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    Text = 'SymbolCb'
  end
  object OkBtn: TButton
    Left = 96
    Top = 32
    Width = 93
    Height = 23
    Caption = 'Ok'
    TabOrder = 1
    OnClick = OkBtnClick
  end
end
