library TxtImp;

// toDo
{

Fatal Bug: Nach h�ufigen klicken in die fixed column witdh gibts es Accessviolation bei 'Abbruch', nach der Auswahl eines *.tdf und klick auf Abbruch gibt es Accessviolations
           mr := TextWizardWnd.ShowModal;

Bug:  Bei Fix Colum with ist die Voransicht falsch aufgebaut
Bug: Meldung Datei nicht gefunden, wenn in der TreeListview eine Root gew�hlt wird
Bug: ListBox akzeptiert *.tdf Dateien
Bug: Wenn Wingis maximiert wird, bleibt der wizard - dialog  im Hintergrund, wenn er offen ist

New feature: Drag an Drop f�r tdf Dateien
Wenn die Zieldatenbank nicht vorhanden ist, soll deren Pfad rot geschrieben werden, bzw die Zieldatenbank angelegt werden.


Bug: Wenn der Wizard verlassen wird und das *.tdf neu gespeichet wurde, scheint es nicht im Importdialog auf

// done
2005-01-16 /2.1.0.16 Bug: Der Default Symbol Auswahl Dialog kommt poDesigned <-- kommt jetzt in Dialogmitte
2005-01-12 /2.1.0.15 Bug: Bei der Projektionsumrechnung kommen 3 Messageboxen
2005-01-03 /2.1.0.14 Bug: Wenn Quellprojektion aber keine Zielprojektion (oder umgekehrt) angegeben ist, erfolgt keine Warnung
2005-01-02 /2.1.0.14 Bug: Beim Import ist die Textposition falsch, wenn keine Offsetspalte gew�hlt wird
2005-01-02 /2.1.0.14 Bug: Beim Wegwerfen einer Datenbankspalte wird der Spaltennamendialog nicht geschlossen
2005-01-01 /2.1.0.14 Fatal Bug: Beim 2.�ffnen eines tdf Datei kommt 'Accessviolation'.
2005-01-01 /2.1.0.14 Bug: Wenn ein TDF gewechselt wird, wird der Datenbankname im SetTxtFileWnd nicht aktualisiert
2005-01-01 /2.1.0.14 Bug: Wenn ein TDF ausgew�hlt wurde, wird dessen Name nicht in den TextWizardDlg �bernommen
2005-01-01 /2.1.0.14 Bug: DB Button ist nicht aktivierbar
2004-12-19 /2.1.0.12 Bug: Bei breiten  Textfiles und fixed colum with ist die Dateivoransicht zu schmal
2004-12-06 /2.1.0.11 Bug: Text/Label wird nicht lt. TextPosPanel gesetzt (Posiotion und Offset
2004-11-28/ 2.1.0.6  Bug:  Edit Fieldname verschwindet nicht, wenn DATABASE column  verworfen wird
2004-12-01/ 2.1.0.8  Bug: List Index out of bounds beim verwerfen von Columns Definitionen
}


{ Wichtiger Hinweis zur DLL-Speicherverwaltung: Sharemem mu� die erste
 Unit in der uses-Klausel Ihrer Bibliothek und Ihres Projekts sein,
 wenn Ihre DLL Prozeduren und Funktionen exportiert, die
 Strings als Parameter oder Funktionsergebnisse �bergeben. Dies
 betrifft alle Strings, die an Ihre DLL oder von Ihrer DLL �bergeben werden--selbst diejenigen,
 die sich in Records oder Klassen befinden. Sharemem ist die Schnittstelle zu
 BORLNDMM.DLL, die den gemeinsamen Zugriff auf den Speicher verwaltet; DELPHIMM.DLL
 mu� mit Ihrer DLL weitergegeben werden. Wenn Sie BORLNDMM.DLL nicht verwenden m�chten, m�ssen
 Strings als PChar oder ShortString �bergeben werden.}

uses
  SysUtils,
  Classes,
  Dialogs,
  Forms,
  Maindlg in 'MAINDLG.PAS' {MainWnd},
  Strfile in 'STRFILE.PAS',
  comobj,
  wingis_TLB,
  WinOSInfo,
  SetTxtFileDlg in 'SetTxtFileDlg.pas' {SetTxtFileWnd},
  TextWizardDlg in 'TextWizardDlg.pas' {TextWizardWnd},
  TextFileDef in 'TextFileDef.pas',
  SetSymbolDlg in 'SetSymbolDlg.pas' {SetSymbolWnd},
  SetTableDlg in 'SetTableDlg.pas' {SetTableWnd},
  AXImpExpDbcXControl_TLB in '..\..\Program Files\Borland\Delphi6\Imports\AXImpExpDbcXControl_TLB.pas';

{$R *.RES}

//------------------------------------------------------------------------------
// FUNCTION    : EXECMODULE
//------------------------------------------------------------------------------
// DESCRIPTION : Function is used to execute the Text-Import Module
//
// PARAMETERS  : DIR       -> Current working directory
//               AXHANDLE  -> Reference to Ole-Object
//               DOCUMENT  -> Reference to Current used Project in GIS
//               LNGCODE   -> Current language code
//               WINHANDLE -> Window handle of the WinGIS Form
//               WTOP      -> Parent window top value
//               WLEFT     -> Parent window left value
//               WWIDTH    -> Parent window width value
//               WHEIGHT   -> Parent window height value
//
// RETURNVALUE : NONE
//------------------------------------------------------------------------------
procedure EXECMODULE(DIR:PCHAR;var AXHANDLE:VARIANT;var DOCUMENT:VARIANT;LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer;DLLIDX:integer);stdcall;
var aDisp:IDispatch;
begin
   // create the main-window
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.App:=AXHANDLE;
   aDisp:=DOCUMENT;
   MainWnd.Document:=aDisp as IDocument;
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.DllIdx:=DLLIDX;
   MainWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);
   MainWnd.readini();

   // setup the selection window for the text-files
   SetTxtFileWnd:=TSetTxtFileWnd.Create(nil);
   SetTxtFileWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);
   SetTxtFileWnd.Show;
end;

//------------------------------------------------------------------------------
// FUNCTION    : EXECMODULEWITHFILTER
//------------------------------------------------------------------------------
// DESCRIPTION : Function is used to execute the Text-Import Module
//
// PARAMETERS  : DIR       -> Current working directory
//               AXHANDLE  -> Reference to Ole-Object
//               DOCUMENT  -> Reference to Current used Project in GIS
//               LNGCODE   -> Current language code
//               FILTER    -> Object-Type Filter (ot_Pixel, ot_Poly, ot_CPoly,
//                                                ot_Circle, ot_Symbol)
//               TXTNAME   -> Filename of the Text-File that should be read
//               TDFNAME   -> Filename of Text-Definition File to use
//               DBMODE    -> Mode which database should be used
//               DBNAME    -> Name of the database
//               DBTABLE   -> Name of the table
//
// RETURNVALUE : INDEX OF LAST CREATED OBJECT
//------------------------------------------------------------------------------
function EXECMODULEWITHFILTER(DIR:PCHAR;var AXHANDLE:VARIANT;var DOCUMENT:VARIANT;LNGCODE:PCHAR;
                              FILTER:integer; TXTNAME:PCHAR; TDFNAME:PCHAR;
                              DBMODE:integer; DBNAME:PCHAR; DBTABLE:PCHAR):integer;stdcall;
var aStrFileObj:ClassStrFile;
    retVal:integer;
    TextFileDefFailed:boolean;
    aDisp:IDispatch;
begin
   // create the main-window
   SetTxtFileWnd:=nil; // Selection of Text-Files is not necessary here
   MainWnd:=TMainWnd.Create(nil);
   // MainWnd has to be a Modal dialog if it is used as Filter
   MainWnd.FormStyle:=fsNormal;
   MainWnd.Position:=poScreenCenter;
   MainWnd.Visible:=False;
   MainWnd.WindowState:=wsNormal;
   MainWnd.Height:=MainWnd.winheight;
   MainWnd.Width:=MainWnd.winwidth;
   MainWnd.App:=AXHANDLE;
   aDisp:=DOCUMENT;
   MainWnd.Document:=aDisp as IDocument;
   MainWnd.LngCode:=strpas(LNGCODE);

   // do the settings that MainWnd is used with filter
   MainWnd.UsedObjectFilter:=Filter;
   MainWnd.FilterTxtFile:=strpas(TXTNAME);
   // setup the database settings
   MainWnd.AXImpExpDbc.ImpDbMode:=DBMODE;
   MainWnd.AXImpExpDbc.ImpDatabase:=strpas(DBNAME);
   MainWnd.AXImpExpDbc.ImpTable:=strpas(DBTABLE);

   TextFileDefFailed:=TRUE;
   // first check if a TdfFile has been set
   if strpas(TDFNAME) <> '' then
   begin
      aStrFileObj:=ClassStrFile.Create;
      aStrFileObj.CreateList(OSInfo.WingisDir,MainWnd.LngCode,'TXTIMP');
      if not MainWnd.CurTextFileDef.LoadFromFile(strpas(TDFNAME),OSInfo.LocalAppDataDir+'TxtImp.ini') then
      begin
         ShowMessage(aStrFileObj.Assign(36));
         TextFileDefFailed:=TRUE;
      end
      else
      begin
         TextFileDefFailed:=FALSE;
         // check if the Filter is set is the same than the text-file-definition has
         if MainWnd.CurTextFileDef.GetObjectType <> FILTER then
         begin
            Showmessage(aStrFileObj.Assign(55)); // Warning: Different objecttype
         end;
         MainWnd.CurTextFileDef.SetObjectType(FILTER);
      end;
      aStrFileObj.Free;
   end;
   // Display the wizzard only if no text-definition is set
   if TextFileDefFailed then
   begin
      // now the Textfile-Definition Wizard can be executed
      TextWizardWnd:=TTextWizardWnd.Create(nil);
      TextWizardWnd.WorkingDir:=OSInfo.WingisDir;
      TextWizardWnd.LngCode:=MainWnd.LngCode;
      // setup the filename for the wizard
      MainWnd.SetProjectionSettings; // setup the projection
      TextWizardWnd.CurrTextFileDef:=MainWnd.CurTextFileDef;
      TextWizardWnd.SetFilename(MainWnd.FilterTxtFile);
      TextWizardWnd.SetObjectTypeFilter(FILTER);
      TextWizardWnd.ShowModal;
      TextWizardWnd.Free;
      TextWizardWnd:=nil;
   end
   else
      MainWnd.SetProjectionSettings; // setup the projection

   // check if there has been a correct text-file definition set
   // in this case the data will be imported
   if (MainWnd.CurTextFileDef.GetDelimeterType <> delim_None) and
      (MainWnd.CurTextFileDef.GetObjectType < 98) then // after ADF or GPSLOG defintion -> no import
   begin
      MainWnd.ShowModal;
   end;
   retVal:=MainWnd.LastCreatedId;
   MainWnd.Free;
   result:=retVal;


end;

//------------------------------------------------------------------------------
// FUNCTION    : FREEMODULE
//------------------------------------------------------------------------------
// DESCRIPTION : Function is used to close the Module
//
// PARAMETERS  : NONE
//
// RETURNVALUE : NONE
//------------------------------------------------------------------------------
procedure FREEMODULE;stdcall;
begin
   { STR ++
   if SetTxtFileWnd <> nil then
   begin
      SetTxtFileWnd.Destroy;
   end;
   if MainWnd <> nil then
   begin
      MainWnd.Destroy;
   end;
   MainWnd:=nil;
   STR -- }
   if SetTxtFileWnd <> nil then begin
        SetTxtFileWnd.Free;
   end;
   MainWnd.Free;
end;

exports EXECMODULE, FREEMODULE,EXECMODULEWITHFILTER;
begin
end.
