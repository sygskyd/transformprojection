unit SetTableDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TSetTableWnd = class(TForm)
    Panel1: TPanel;
    TableLb: TListBox;
    NextBtn: TButton;
    procedure NextBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SetTableWnd: TSetTableWnd;

implementation

{$R *.DFM}

procedure TSetTableWnd.NextBtnClick(Sender: TObject);
begin
   Self.Close;
end;

end.
