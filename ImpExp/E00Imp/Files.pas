unit Files;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Math, Struct, Db;

const
       Col_Int      = 0;
       Col_SmallInt = 1;
       Col_Float    = 2;
       Col_Date     = 3;
       Col_String   = 4;

type
  { Hier k�nnen Strukturen definiert werden }
  p_double = ^doublewert;
  doublewert = record
     Zahl :double;
  end;

  p_integer = ^integerwert;

  integerwert = record
     Zahl :integer;
  end;

ClassFile = class
   public
      { public Definitionen }
      constructor Create;
      procedure Destroy;
      function GetMaxPass(Dateiname:string):longint;
      function GetMaxPalElements:longint;
      function GetMaxArcElements:longint;
      function GetMaxLabElements:longint;
      procedure OpenE00File(Dateiname:string);
      procedure CloseAllFiles;
      function  ReadElement(Art:string):longint;
      function  GetObjectanz:longint;
      function  WritePalElement(Art:string):longint;
      function  WriteArcElement(Art:string):longint;
      function  WriteLabElement(Art:string):longint;
      procedure CreateDatabase(Layername:string;Typ:string;OriginalLayer:string);
      function  Check_odoa(Dateiname:string):boolean;
      function  to_odoa:longint;
      procedure OpenFile(Dateiname:string);
      procedure Closefiles;
      function CreateElement:longint;
      function SetDBAnfang(Art:string):integer;
      function GetDBcolumnname:string;
      procedure SetToNextDatabaseColumn;
      function GetLength(Bezeichnung:string;Typ:string):integer;
      function CheckPalExistsA:boolean;
      function CheckPalExistsG:boolean;
      function CheckArcExistsA:boolean;
      function CheckArcExistsG:boolean;
      function CheckLabExistsA:boolean;
      function CheckLabExistsG:boolean;
      procedure ResetObjektnr;
      function HasOldDBFTableColumn(Bezeichnung:string;Art:string):boolean;
   private
      { private Definitionen }
      function ReadArc(var Zeile:string):longint;
      function ReadCnt(var Zeile:string):longint;
      function ReadPal(var Zeile:string):longint;
      function ReadLab(var Zeile:string):longint;
      function ReadPrj(var Zeile:string):longint;
      function ReadLog(var Zeile:string):longint;
      function ReadIfo(var Zeile:string;Art:string):longint;
      procedure ValArc_Header(Source:string;var coverage_nr:longint; var coverage_id:longint; var from_node:longint; var to_node:longint;
                              var left_polygon:longint;var right_polygon:longint; var number_of_cor:longint
                             );
      function ParseString(Source:string;Pos:integer):string;
end;
implementation
uses MainDlg,OptionDlg;

var  E00Datei  :Textfile;
     E00Type   :string;
     ArcObj    :ClassArc;
     CntObj    :ClassCnt;
     PalObj    :ClassPal;
     LabObj    :ClassLab;
     ConvertObj:ClassConvert;
     DatenbankObj:ClassDatenbank;
     OldDBFFile  :ClassDatenbank;
     Layername :string;
     Bytesrest_anz  :integer;
     Zeile          :string;
     inFile         :file;
     outFile        :Textfile;
     to_odoa_flag   :boolean;
     to_odoa_filename:string;
     Restbytes      :array [1..5080] of char;
     PalexistsG,PalexistsA:boolean;
     ArcexistsG,ArcexistsA:boolean;
     LabexistsG,LabexistsA:boolean;


constructor ClassFile.Create;
begin
   ArcObj:=ClassArc.Create;
   { CntObj:=ClassCnt.Create; }
   PalObj:=ClassPal.Create;
   LabObj:=ClassLab.Create;
   DatenbankObj:=ClassDatenbank.Create;
   OldDBFFile:=ClassDatenbank.Create;
   ConvertObj:=ClassConvert.Create;
   E00Type:='';
   PalExistsG:=false; ArcExistsG:=false; LabexistsG:=false;
   PalExistsA:=false; ArcExistsA:=false; LabexistsA:=false;
end;

procedure ClassFile.Destroy;
begin
   ArcObj.Destroy;
{   CntObj.Destroy; }
   PalObj.Destroy;
   LabObj.Destroy;
   DatenbankObj.Destroy;
   OldDBFFile.Destroy;
end;

{*******************************************************************************
* PROZEDUR : OpenE00File                                                       *
********************************************************************************
* Prozedur dient zum �ffnen des E00-files.                                     *
*                                                                              *
* PARAMETER: Dateiname -> Dateiname des E00-Files.                             *
*                                                                              *
********************************************************************************}
procedure ClassFile.OpenE00File(Dateiname:string);
begin
   {$i-} Assignfile(E00Datei,Dateiname); Reset(E00Datei); {$i+}
   Layername:=Extractfilename(Dateiname); Layername:=copy(Layername,1,pos('.',Layername)-1);
   MainWnd.CurrLayername:=Layername;
end;

{*******************************************************************************
* PROZEDUR : OpenFile                                                          *
********************************************************************************
* Prozedur dient zum �ffnen eine E00-Files, das sich im im UNIX-Format befindet*
*                                                                              *
* PARAMETER: Dateiname -> Dateiname des E00-Files.                             *
*                                                                              *
********************************************************************************}
procedure ClassFile.OpenFile(Dateiname:string);
begin
   {$i-} Assignfile(InFile,Dateiname); Reset(InFile,1); {$i+}
   {$i-} Assignfile(OutFile,Changefileext(Dateiname,'.tmp')); Rewrite(OutFile); {$i+}
   Zeile:=''; Bytesrest_anz:=0;
end;

{*******************************************************************************
* PROZEDUR : Closefile                                                         *
********************************************************************************
* Prozedur dient zum schliessen der beiden Dateien, die f�r die DOS nach       *
* UNIX Konvertierung ben�tigt wurden.                                          *
*                                                                              *
********************************************************************************}
procedure ClassFile.Closefiles;
begin
   {$i-} Closefile(inFile); Closefile(outFile); {$i+}
end;


{*******************************************************************************
* FUNKTION : Check_odoa                                                        *
********************************************************************************
* Funktion �berpr�ft, ob die Datei vom UNIX ins DOS-Format konvertiert werden  *
* muss.                                                                        *
*                                                                              *
* PARAMETER: Dateiname -> Name der zu pr�fenden Datei                          *
*                                                                              *
* R�CKGABEWERT: TRUE -> wenn konvertiert werden muss, sonst FALSE              *
********************************************************************************}
function ClassFile.Check_odoa(Dateiname:string):boolean;
var Datei:file;
    Actualbytes:array [1..256] of char;
    Bytesread,i:integer;
begin
   Filemode:=0;
   {$i-}
   Assignfile(Datei,Dateiname); Reset(Datei,1);
   {$i+}
   { Einlesen eines 256 Byte Blockes }
   {$i-}Blockread(Datei,Actualbytes,256,Bytesread);{$i+}

   to_odoa_flag:=true;

   if Bytesread > 100 then Bytesread:=100; { Es werden nur

   { Nun wird kontrolliert, ob sich ein <CR> in diesem Buffer befindet }
   for i:=1 to Bytesread do
   begin
      if Actualbytes[i] = CHR(13) then to_odoa_flag:=false;
   end;

   {$i-} Closefile(Datei); {$i+}
   Bytesrest_anz:=0;
   to_odoa_filename:=Changefileext(Dateiname,'.tmp');
   Result:=to_odoa_flag;
end;

{*******************************************************************************
* PROZEDUR : CloseallFiles                                                     *
********************************************************************************
* Prozedur dient zum Schliessen aller Files.                                   *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassFile.CloseallFiles;
var hpchar:pchar;
begin
   hpchar:=stralloc(255);
   {$i-}
   Closefile(E00Datei);
   {$i+}
   strpcopy(hpchar,to_odoa_filename);
   if to_odoa_flag = true then Deletefile(hpchar);
   if MainWnd.ConcatFlag then
   begin
      strpcopy(hpchar,Changefileext(to_odoa_filename,'.cat'));
      Deletefile(hpchar);
   end;
   strdispose(hpchar);
   MainWnd.AXImpExpDbc.CloseImpTables;
end;

{*******************************************************************************
* FUNKTION : GetMaxPass1                                                       *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Bytes des       *
* Shapefiles.                                                                  *
*                                                                              *
* PARAMETER: Dateiname -> Dateiname von der die Bytegr��e ermittelt werden soll*
*                                                                              *
* R�ckgabewert: Anzahl der zu Bearbeitenden Bytes.                             *
********************************************************************************}
function ClassFile.GetMaxPass(Dateiname:string):longint;
var Datei:file of byte;
begin
   {$i-}
   AssignFile(Datei, Dateiname);
   {$i+}
   Filemode:=0;
   {$i-}
   Reset(Datei); Result:=FileSize(Datei); Closefile(Datei);
   {$i+}
end;

{*******************************************************************************
* FUNKTION : GetMaxPalElements                                                 *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Pal-Elemente    *
* des E00-Files.                                                               *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden Pal-Datens�tze.                    *
********************************************************************************}
function ClassFile.GetMaxPalElements:longint;
begin
   PalObj.ResetCurrent;
   Result:=PalObj.GetAnzahl;
end;


{*******************************************************************************
* FUNKTION : GetMaxLabElements                                                 *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Lab-Elemente    *
* des E00-Files.                                                               *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden Lab-Datens�tze.                    *
********************************************************************************}
function ClassFile.GetMaxLabElements:longint;
begin
   LabObj.ResetCurrent;
   Result:=LabObj.Getanzahl;
end;

{*******************************************************************************
* FUNKTION : GetMaxArcElements                                                 *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Arc-Elemente    *
* des E00-Files.                                                               *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden Arc-Datens�tze.                    *
********************************************************************************}
function ClassFile.GetMaxArcElements:longint;
begin
   ArcObj.ResetCurrent;
   Result:=ArcObj.GetAnzahl;
end;

function Classfile.to_odoa:longint;
var Actualbytes    :array [1..5080] of char;
    Hilfsbytes     :array [1..5080] of char;
    Bytesread      :integer;
    counter2       :integer;
    Zeile          :string;
    i,j            :integer;
begin
   Zeile:='';

   { Einlesen eines 4096 Byte Blockes }
   {$i-} Blockread(InFile,Actualbytes,4096,Bytesread); {$i+}

   { Rest des vorigen Blockes mu� ber�cksichtigt werden }
   if (Bytesrest_anz <> 0) then
   begin
      { Zeile mu� vor Actualbytes kopiert werden }
      for i:=1 to Bytesrest_anz do Hilfsbytes[i]:=Restbytes[i];

      for j:=1 to Bytesread do
      begin
         Hilfsbytes[i]:=Actualbytes[j]; i:=i + 1;
      end;
      Bytesread:=Bytesread + Bytesrest_anz;
      for i:=1 to Bytesread do Actualbytes[i]:=Hilfsbytes[i];
      Bytesrest_anz:=0; 
   end;

   counter2:=1;
   { Nun werden die durch Chr-10 getrennten Zeilen, die sich im Buffer befinden ermittelt }
   repeat
      if Actualbytes[counter2] = CHR(10) then
      begin
         {$i-} writeln(OutFile,Zeile); {$i+}
         counter2:=counter2 + 1;
         Bytesrest_anz:=0;
         Zeile:='';
      end
      else
      begin
         Zeile:=Zeile + Actualbytes[counter2];
         Bytesrest_anz:=Bytesrest_anz + 1;
         Restbytes[Bytesrest_anz]:=Actualbytes[counter2];
         counter2:=counter2 + 1;
      end;
   until counter2 = Bytesread + 1;

   Result:=Bytesread;
end;

{*******************************************************************************
* FUNKTION : GetObjectanz                                                      *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu schreibenden Datens�tze im    *
* ASC-File.                                                                    *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden Datens�tze.                        *
********************************************************************************}
function ClassFile.GetObjectanz:longint;
begin
   Result:=0;
end;

{*******************************************************************************
* FUNKTION : WritePalElement                                                   *
********************************************************************************
* Funktion dient zum schreiben eines PAL Eintrages in das ASC-File.            *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Bytes.                                 *
********************************************************************************}
function ClassFile.WritePalElement(Art:string):longint;
begin
   PalObj.WriteCurrent(DatenbankObj.GetAnfang,Art);
   PalObj.SetCurrentNext;
   Result:=1;
end;

{*******************************************************************************
* FUNKTION : WriteLabElement                                                   *
********************************************************************************
* Funktion dient zum schreiben eines Lab Eintrages in das ASC-File.            *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Bytes.                                 *
********************************************************************************}
function ClassFile.WriteLabElement(Art:string):longint;
begin
   LabObj.WriteCurrent(DatenbankObj.GetAnfang,Art);
   LabObj.SetCurrentNext;
   Result:=1;
end;

{*******************************************************************************
* FUNKTION : WriteArcElement                                                   *
********************************************************************************
* Funktion dient zum schreiben eines Eintrages in das ASC-File.                *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Bytes.                                 *
********************************************************************************}
function ClassFile.WriteArcElement(Art:string):longint;
begin
   ArcObj.WriteCurrent(DatenbankObj.GetAnfang,Art);
   ArcObj.SetCurrentNext;
   Result:=1;
end;

function ClassFile.HasOldDBFTableColumn(Bezeichnung:string;Art:string):boolean;
begin
   result:=OldDBFFile.Checkexists(Bezeichnung,Art,OldDBFFile.GetAnfang);
end;

{*******************************************************************************
* PROZEDUR : CreateDatabase                                                    *
********************************************************************************
* Prozedur dient zum Erzeugen der Datenbank                                    *
*                                                                              *
* PARAMETER: Layername-> Name des Layers der in die Datenbank soll             *
*            Typ      -> Art der Datenbank  ALL   ->  DB f�r alle Objekttypen  *
*                                           POLY  ->  DB nur f�r Polygone,     *
*                                           LINE  ->  DB nur f�r Linien,       *
*                                           POINT ->  DB nur f�r Punkte,       *
********************************************************************************}
procedure ClassFile.CreateDatabase(Layername:string;Typ:string;OriginalLayer:string);
var Laenge,i,DBAnz:integer;
    Bezeichnung:string;
    DataType   :integer;
begin
   if Typ = 'NO' then exit;
   if MainWnd.CheckLayerExists(Layername) = FALSE then // create layer before database
      MainWnd.CreateLayer(Layername);
   MainWnd.AXImpExpDbc.CheckAndCreateImpTable(Layername);
   MainWnd.AXImpExpDbc.AddImpIdColumn(Layername);
   { Erhalten der Datenbankspalten }
   DBAnz:=DatenbankObj.SetDBFirst(DatenbankObj.GetAnfang,Typ);
   if DBAnz > 0 then
   begin
      for i := 1 to DBAnz do { Einf�gen der Spaltennamen in die Datenbankliste }
      begin
         Bezeichnung:=DatenbankObj.GetDBColumnname;
         DataType:=DatenbankObj.GetDataType;
         Laenge:=DatenbankObj.GetLength(Bezeichnung,Typ,DatenbankObj.GetAnfang);

         // number and ID have not to be inserted to table
         if (Bezeichnung <> OriginalLayer+'#') and (Bezeichnung <> OriginalLayer + '_ID') then
            MainWnd.AXImpExpDbc.AddImpColumn(Layername,Bezeichnung,DataType,Laenge);
         DatenbankObj.SetCurrentnext;
      end;
   end;
   MainWnd.AXImpExpDbc.CreateImpTable(Layername);
   MainWnd.AXImpExpDbc.OpenImpTable(Layername);
end;

procedure Classfile.ResetObjektnr;
begin
   ConvertObj.ResetObjektnr;
end;

function ClassFile.CheckPalExistsG:boolean;
begin
   Result:=PalExistsG;
end;

function ClassFile.CheckPalExistsA:boolean;
begin
   Result:=PalExistsA;
end;

function ClassFile.CheckLabExistsG:boolean;
begin
   Result:=LabExistsG;
end;

function ClassFile.CheckLabExistsA:boolean;
begin
   Result:=LabExistsA;
end;

function ClassFile.CheckArcExistsG:boolean;
begin
   Result:=ArcExistsG;
end;

function ClassFile.CheckArcExistsA:boolean;
begin
   Result:=ArcExistsA;
end;

function ClassFile.SetDBAnfang(Art:string):integer;
begin
   Result:=DatenbankObj.SetDBFirst(DatenbankObj.GetAnfang,Art);
end;

function ClassFile.GetDBcolumnname:string;
begin
   Result:=DatenbankObj.GetDBColumnname;
end;

procedure ClassFile.SetToNextDatabaseColumn;
begin
   DatenbankObj.SetCurrentnext;
end;

function ClassFile.GetLength(Bezeichnung:string;Typ:string):integer;
begin
   Result:=DatenbankObj.GetLength(Bezeichnung,Typ,DatenbankObj.GetAnfang);
end;

{*******************************************************************************
* FUNKTION : ClassFile.CreateElement                                           *
********************************************************************************
* Funktion dient zum erzeugen eines Polygons aus dem ARC-Elementen.            *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten PAL-elemente.                          *
********************************************************************************}
function ClassFile.CreateElement:longint;
begin
   ConvertObj.CreatePolygon(PalObj.GetCurrent,ArcObj.GetFirst);
   PalObj.Setcurrentnext;
   Result:=1;
end;

{*******************************************************************************
* FUNKTION : ReadElement                                                       *
********************************************************************************
* Funktion dient zum einlesen eines Shapefile-Objekts.                         *
*                                                                              *
* PARAMETER: Art wie die Datei gelesen werden soll (Selektiv, nicht Selektiv)  *
*                                                                              *
* R�ckgabewert: Anzahl der bearbeiteten Bytes.                                 *
********************************************************************************}
function ClassFile.ReadElement(Art:string):longint;
var Zeile:string;
    anz  :longint;
begin
   anz:=0;
   if E00Type = '' then { Wenn der Datentyp ermittelt werden soll }
   begin
      repeat
         {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(Zeile) + 2;
         if copy(Zeile,1,3) = 'ARC' then E00Type:='ARC';
         if copy(Zeile,1,3) = 'CNT' then E00Type:='CNT';
         if copy(Zeile,1,3) = 'LAB' then E00Type:='LAB';
         if copy(Zeile,1,3) = 'LOG' then E00Type:='LOG';
         if copy(Zeile,1,3) = 'PAL' then E00Type:='PAL';
         if copy(Zeile,1,3) = 'PAR' then E00Type:='PAR';
         if copy(Zeile,1,3) = 'PRJ' then E00Type:='PRJ';
         if copy(Zeile,1,3) = 'SIN' then E00Type:='SIN';
         if copy(Zeile,1,3) = 'TOL' then E00Type:='TOL';
         if copy(Zeile,1,3) = 'IFO' then E00Type:='IFO';
      until (E00Type <> '') or ({$i-}EOF(E00Datei){$i+});
   end
   else
   begin
      {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(Zeile) + 2;
      if copy(Zeile,1,3) = 'ARC' then E00Type:='ARC';
      if copy(Zeile,1,3) = 'CNT' then E00Type:='CNT';
      if copy(Zeile,1,3) = 'LAB' then E00Type:='LAB';
      if copy(Zeile,1,3) = 'LOG' then E00Type:='LOG';
      if copy(Zeile,1,3) = 'PAL' then E00Type:='PAL';
      if copy(Zeile,1,3) = 'PAR' then E00Type:='PAR';
      if copy(Zeile,1,3) = 'PRJ' then E00Type:='PRJ';
      if copy(Zeile,1,3) = 'SIN' then E00Type:='SIN';
      if copy(Zeile,1,3) = 'TOL' then E00Type:='TOL';
      if copy(Zeile,1,3) = 'IFO' then E00Type:='IFO';

      if E00Type = 'ARC' then anz:=anz + ReadArc(Zeile); { Einlesen eines E00-Arcs }
      if E00Type = 'LAB' then anz:=anz + ReadLab(Zeile); { Einlesen eines E00-Labs }
      if E00Type = 'LOG' then anz:=anz + ReadLog(Zeile); { Einlesen eines E00-Logs }
      if E00Type = 'PAL' then anz:=anz + ReadPal(Zeile); { Einlesen eines E00-Pals }
      if E00Type = 'IFO' then anz:=anz + ReadIfo(Zeile,Art); { Einlesen eines E00-Ifos}
      if E00Type = 'CNT' then anz:=anz + ReadCnt(Zeile); { Einlesen eines E00-Cnts }
      if E00Type = 'PAR' then begin {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(zeile) +2; end;
      if E00Type = 'PRJ' then begin {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(zeile) +2; end; {anz:=anz + ReadPrj(Zeile); { Einlesen eines E00-Prjs }
      if E00Type = 'SIN' then begin {$i-} readln(E00Datei,Zeile); {$i+}anz:=anz + length(zeile) +2; end;
      if E00Type = 'TOL' then begin {$i-} readln(E00Datei,Zeile); {$i+}anz:=anz + length(zeile) +2; end;
   end;
   Result:=anz;
end;

{*******************************************************************************
* FUNKTION : ClassFile.ReadLab                                                 *
********************************************************************************
* Funktion dient zum Einlesen eines Lab Datensatzes aus dem E00-File.          *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der Bytes, die eingelesen wurden.                       *
********************************************************************************}
function Classfile.ReadLab(var Zeile:string):longint;
var anz,coverage_id,encloses_polygon,count:longint;
    X,Y:double;
begin
   anz:=0;
   { Damit die erste Zeile auch gelesen wird }
   if copy(Zeile,1,3) = 'LAB' then begin {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(Zeile) + 2; end;

   { Ermitteln der Coverage_id }
   val(ParseString(Zeile,1),coverage_id,count); if count <> 0 then coverage_id:=0;

   if coverage_id = -1 then
   begin
      Result:=anz; exit;
   end;

   LabexistsG:=true;

   { Ermitteln des encloses_polygon }
   val(ParseString(Zeile,2),encloses_polygon,count); if count <> 0 then encloses_polygon:=0;

   { Ermitteln der X Koordinate }
   val(ParseString(Zeile,3),X,count); if count <> 0 then x:=0;

   { Ermitteln der Y Koordinate }
   val(ParseString(Zeile,4),Y,count); if count <> 0 then y:=0;
   if (X <> 0) and (Y <> 0) and (coverage_id <> 0) then { Nur wenn der Datensatz OK ist wird er auch engef�gt }
      LabObj.Neu(coverage_id,encloses_polygon,X,Y,DatenbankObj.GetAnfang);

   Result:=anz;
end;

{*******************************************************************************
* FUNKTION : ClassFile.ReadPal                                                 *
********************************************************************************
* Funktion dient zum Einlesen eines Pal Datensatzes aus dem E00-File.          *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der Bytes, die eingelesen wurden.                       *
********************************************************************************}
function ClassFile.ReadPal(var Zeile:string):longint;
var anz,number_of_arcs,count,arc_number,node_number,polygon_number,worked:longint;
    X_min,Y_min,X_max,Y_max:double;
    Insel:boolean;
begin
   anz:=0; Insel:=false;

   { Damit die erste Zeile auch gelesen wird }
   if copy(Zeile,1,3) = 'PAL' then begin {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(Zeile) + 2; end;

   { Ermitteln der number of arcs in polygon }
   val(ParseString(Zeile,1),number_of_arcs,count); if count <> 0 then number_of_arcs:=0;

   if number_of_arcs = -1 then
   begin
      result:=anz; exit;
   end;

   PalexistsG:=true;

   { Ermitteln von X_min }
   val(ParseString(Zeile,2),X_min,count); if count <> 0 then X_min:=0;

   { Ermitteln von Y_min }
   val(ParseString(Zeile,3),Y_min,count); if count <> 0 then Y_min:=0;

   { Ermitteln von X_max }
   val(ParseString(Zeile,4),X_max,count); if count <> 0 then X_max:=0;

   { Ermitteln von Y_max }
   val(ParseString(Zeile,5),Y_max,count); if count <> 0 then Y_max:=0;

   PalObj.Neu(number_of_arcs,X_min,Y_min,X_max,Y_max);

   { Nun m�ssen die Einzelnen Polygone ermittelt werden }
   worked:=0;
   repeat
      if worked < number_of_arcs then
      begin
         {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(Zeile) + 2;
         { Ermitteln von der Arc_number }
         val(ParseString(Zeile,1),arc_number,count); if count <> 0 then arc_number:=0;
         { Ermitteln der node_number }
         val(ParseString(Zeile,2),node_number,count); if count <> 0 then node_number:=0;
         { Ermitteln der polygon_number }
         val(ParseString(Zeile,3),polygon_number,count); if count <> 0 then polygon_number:=0;

         if (Pos('E',Zeile)=0) then
         begin
            if arc_number = 0 then Insel:=true;
            if Insel = false then PalObj.InsertPolygon(arc_number,node_number,polygon_number); worked:=worked + 1;
         end;
      end;

      if worked < number_of_arcs then
      begin
         { Ermitteln von der Arc_number }
         val(ParseString(Zeile,4),arc_number,count); if count <> 0 then arc_number:=0;
         { Ermitteln der node_number }
         val(ParseString(Zeile,5),node_number,count); if count <> 0 then node_number:=0;
         { Ermitteln der polygon_number }
         val(ParseString(Zeile,6),polygon_number,count); if count <> 0 then polygon_number:=0;
         if Pos('E',Zeile) =0 then
         begin
            if arc_number = 0 then Insel:=true;
            if Insel = false then PalObj.InsertPolygon(arc_number,node_number,polygon_number); worked:=worked + 1;
         end;
      end;
   until worked >= number_of_arcs;
   Result:=anz;
end;


{*******************************************************************************
* FUNKTION : ClassFile.ReadCnt                                                 *
********************************************************************************
* Funktion dient zum Einlesen eines Arc Datensatzes aus dem E00-File.          *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der Bytes, die eingelesen wurden.                       *
********************************************************************************}
function Classfile.ReadCnt(var Zeile:string):longint;
var anz,number_of_labels,count,i,worked:longint;
    X,Y:double;
begin
   anz:=0;

   { Damit die erste Zeile auch gelesen wird }
   if copy(Zeile,1,3) = 'CNT' then begin {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(Zeile) + 2; end;

   { Nun wird die Anzahl der Labels im Polygon ermittelt }
   val(ParseString(Zeile,1),number_of_labels,count); if count <> 0 then number_of_labels:=0;

   if number_of_labels = -1 then
   begin
      Result:=anz; exit;
   end;

   { Nun werden die Koordinaten des CNT ermittelt }
   val(ParseString(Zeile,2),X,count); if count <> 0 then X:=0;
   val(ParseString(Zeile,3),Y,count); if count <> 0 then y:=0;

   {CntObj.Neu(number_of_labels,X,Y,DatenbankObj.GetAnfang);}
   worked:=0;
   if number_of_labels <> 0 then
   repeat
      {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(Zeile) + 2;
      for i:=1 to 8 do
      begin
         if ParseString(Zeile,i) <> '' then worked:=worked + 1;
      end;
   until worked = number_of_labels;
   Result:=anz;
end;

{*******************************************************************************
* FUNKTION : ClassFile.ReadIfo                                                 *
********************************************************************************
* Funktion dient zum Einlesen eines Ifo Datensatzes aus dem E00-File.          *
*                                                                              *
* PARAMTER: Art -> Art wie die Informationdatens�tze eingelesen werden sollen. *
*                                                                              *
* R�ckgabewert: Anzahl der Bytes, die eingelesen wurden.                       *
********************************************************************************}
function Classfile.ReadIfo(var Zeile:string;Art:string):longint;
var anz,count,i:longint;
    DBAnz,IfAnz,j,von,bis,Spaltenlaenge,Collength,subbis,aktbis,aktlength:integer;
    Spaltenname,DBInfo,Spaltenart:string;
    SpaltenTyp:integer;
    DBSpalten:array [1..255] of string;
    hdouble:double;
    hpchar:pchar;
begin
   anz:=0; hpchar:=stralloc(255);
   { Nun wird bis zur .PAT bzw. .AAT Information �berlesen }
   repeat
      {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(Zeile) + 2;
   until (Pos('.PAT',Zeile) <> 0) or (Pos('.AAT',Zeile) <> 0) or (copy(Zeile,1,3) = 'EOI') or ({$i-}EOF(E00Datei){$i+});

   if Pos('.PAT',Zeile) <> 0 then { Nun kann die .PAT Information herausgelesen werden }
   begin
      PalexistsA:=false; LabexistsA:=false; von:=1; bis:=1;
      { Zuerst werden die Datenbankspalten herausgelesen }
      val(ParseString(Zeile,4),DbAnz,count); if count <> 0 then DBAnz:=0;
      val(ParseString(Zeile,6),IfAnz,count); if count <> 0 then IfAnz:=0;

      for i:=1 to DBAnz do
      begin
         {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(Zeile) + 2;

         { Um zu vermeiden, dass Bindestriche im Spaltennamen sind werden diese durch _ ersetzt }
         strpcopy(hpchar,Zeile); for count:=1 to length(Zeile) do if hpchar[count] = '-' then hpchar[count]:='_'; Zeile:=strpas(hpchar);

         Spaltenname:=ParseString(Zeile,1);
         SpaltenArt:=ParseString(Zeile,5);
         if (SpaltenArt <> '60_1') and (SpaltenArt <> '50_1') and (SpaltenArt <> '30_1') and (SpaltenArt <> '20_1') then
            SpaltenArt:=ParseString(Zeile,6);
         val(ParseString(Zeile,2),Spaltenlaenge,count);

         if SpaltenArt = '60_1' then begin if Spaltenlaenge = 8 then Collength:=24 else Collength:=14; Spaltentyp:=Col_Float; end;
         if SpaltenArt = '50_1' then begin Collength:=11; Spaltentyp:=Col_Int; end;
         if SpaltenArt = '30_1' then begin Collength:=Spaltenlaenge; Spaltentyp:=Col_String; end;
         if SpaltenArt = '20_1' then begin Collength:=Spaltenlaenge; Spaltentyp:=Col_String; end;

         bis:=von + Collength - 1;

         if Art = 'ALL' then { Die Konvertierung erfolgt nicht selektiv es sind also alle Datenbanks�tze in einer Table }
         begin
            if DatenbankObj.CheckExists(Spaltenname,'ALL',DatenbankObj.GetAnfang) = false then
               DatenbankObj.Insertcolumn(Spaltenname,Collength,Spaltentyp,'','ALL',von,bis)
            else { Wenn die Datenbankspalte bereits existiert mu� der von bis Bereich gesetzt werden }
               DatenbankObj.SetVonBis(Spaltenname,'ALL',von,bis,DatenbankObj.GetAnfang);
            DBSpalten[i]:=Spaltenname;
         end
         else
         begin
            if (IfAnz - 1 = PalObj.Getanzahl) then { Die Datenbankspalten gelten f�r ein Pal-Element }
            begin
               if (DatenbankObj.CheckExists(Spaltenname,'POLY',DatenbankObj.GetAnfang) = false) and (Art = '') then
                  DatenbankObj.Insertcolumn(Spaltenname,Collength,Spaltentyp,'','POLY',von,bis);
            end
            else
            begin { Die DatenbankSpalten gelten f�r ein Lab Element }
               if (DatenbankObj.CheckExists(Spaltenname,'POINT',DatenbankObj.GetAnfang) = false) and (Art = '') then
                  DatenbankObj.Insertcolumn(Spaltenname,Collength,Spaltentyp,'','POINT',von,bis);
            end;
            DBSpalten[i]:=Spaltenname;
         end;
         von:=bis + 1;
      end;

      { Nun wird die Datenbankinformation ausgelesen }
      PalObj.ResetCurrent; LabObj.ResetCurrent;

      for i:=1 to IfAnz do
      begin
         Application.Processmessages;
         MainWnd.ShowProcent(anz);
         {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(Zeile) + 2;

         subbis:=0; Aktlength:=0;
         for j:=1 to DBAnz do
         begin
            DBInfo:='';

            { Wenn die Attribute zu einem Palobjekt geh�ren, dann werden die Attribute mit PAL-Objekten verkn�pft }
            if (IfAnz - 1 = PalObj.Getanzahl) then
            begin
               if Art = '' then DatenbankObj.GetVonBis(DBSpalten[j],'POLY',von,bis,SpaltenTyp,DatenbankObj.GetAnfang);
               if Art = 'ALL' then DatenbankObj.GetVonBis(DBSpalten[j],'ALL',von,bis,SpaltenTyp,DatenbankObj.GetAnfang);
               AktLength:=AktLength + (bis - von + 1);
               if (Aktlength>80) then
               begin
                  DBInfo:=copy(Zeile,von-subbis,length(Zeile));
                  {$i-} readln(E00Datei,Zeile); {$i+}anz:=anz + length(Zeile) + 2;
                  von:=1; subbis:=subbis + 80; bis:=bis - subbis;
                  Aktlength:=bis;
               end
               else
               begin
                  aktbis:=bis; von:=von - subbis; bis:=bis - subbis;
               end;
               DBInfo:=DBInfo + copy(Zeile,von,bis-von + 1);
               { Die Leerzeichen werden nun aus der DBInfo gel�scht }
               DBInfo:=Trimleft(DBInfo); DBInfo:=Trimright(DBInfo);

               if i <> 1 then { Bei PAL - Objekten wird der erste Datensatz ausgelassen }
               begin
                  if Art = '' then
                  begin
                     if SpaltenTyp = Col_Float then begin val(DBInfo,hdouble,count); DBInfo:=floattostr(hdouble); end; { Der Bin�re Wert wird in einen Zahlenwert umgewandelt }
                     PalObj.InsertAttrib(DBSpalten[j],DBInfo); DatenbankObj.SetLength(DBSpalten[j],length(DBInfo),'POLY',DatenbankObj.Getanfang);
                  end;
                  if Art = 'ALL' then
                  begin
                     if SpaltenTyp = Col_Float then begin val(DBInfo,hdouble,count); DBInfo:=floattostr(hdouble); end; { Der Bin�re Wert wird in einen Zahlenwert umgewandelt }
                     PalObj.InsertAttrib(DBSpalten[j],DBInfo); DatenbankObj.SetLength(DBSpalten[j],length(DBInfo),'ALL',DatenbankObj.Getanfang);
                  end;
               end;
            end;

            { Wenn die Attribute zu einem Labobjekt geh�ren, dann werden die Attribute mit LAB-Objekten verkn�pft }
            if (IfAnz = LabObj.Getanzahl) then
            begin
               if Art = '' then DatenbankObj.GetVonBis(DBSpalten[j],'POINT',von,bis,SpaltenTyp,DatenbankObj.GetAnfang);
               if Art = 'ALL' then DatenbankObj.GetVonBis(DBSpalten[j],'ALL',von,bis,SpaltenTyp,DatenbankObj.GetAnfang);
               AktLength:=AktLength + (bis - von + 1);
               if (Aktlength>80) then
               begin
                  DBInfo:=copy(Zeile,von-subbis,length(Zeile));
                  {$i-} readln(E00Datei,Zeile); {$i+}anz:=anz + length(Zeile) + 2;
                  von:=1; subbis:=subbis + 80; bis:=bis - subbis;
                  Aktlength:=bis;
               end
               else
               begin
                  aktbis:=bis; von:=von - subbis; bis:=bis - subbis;
               end;
               DBInfo:=DBInfo + copy(Zeile,von,bis-von + 1);
               { Die Leerzeichen werden nun aus der DBInfo gel�scht }
               DBInfo:=Trimleft(DBInfo); DBInfo:=Trimright(DBInfo);

               if Art = '' then
               begin
                  if SpaltenTyp = Col_Float then begin val(DBInfo,hdouble,count); DBInfo:=floattostr(hdouble); end; { Der Bin�re Wert wird in einen Zahlenwert umgewandelt }
                  LabObj.InsertAttrib(DBSpalten[j],DBInfo); DatenbankObj.SetLength(DBSpalten[j],length(DBInfo),'POINT',DatenbankObj.Getanfang);
               end;
               if Art = 'ALL' then
               begin
                  if SpaltenTyp = Col_Float then begin val(DBInfo,hdouble,count); DBInfo:=floattostr(hdouble); end; { Der Bin�re Wert wird in einen Zahlenwert umgewandelt }
                  LabObj.InsertAttrib(DBSpalten[j],DBInfo); DatenbankObj.SetLength(DBSpalten[j],length(DBInfo),'ALL',DatenbankObj.Getanfang);
               end;
            end;
         end;
         if (IfAnz - 1 = PalObj.Getanzahl) and (i <> 1) then PalObj.Setcurrentnext;
         if (IfAnz = LabObj.Getanzahl) then LabObj.Setcurrentnext;
      end;

      if (IfAnz - 1 = PalObj.Getanzahl) then PalExistsA:=true;
      if (IfAnz = LabObj.Getanzahl) then LabExistsA:=true;
   end;

   if Pos('.AAT',Zeile) <> 0 then { Nun kann die .AAT Information herausgelesen werden }
   begin
      { Zuerst werden die Datenbankspalten herausgelesen }
      val(ParseString(Zeile,4),DbAnz,count); if count <> 0 then DBAnz:=0;
      val(ParseString(Zeile,6),IfAnz,count); if count <> 0 then IfAnz:=0;

      if ArcObj.GetAnzahl = IfAnz then begin ArcExistsA:=true;von:=1; bis:=1; end;
      if LabObj.GetAnzahl = IfAnz then begin LabExistsA:=true;von:=1; bis:=1; end;

      for i:=1 to DBAnz do
      begin
         {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(Zeile) + 2;
         { Um zu vermeiden, dass Bindestriche im Spaltennamen sind werden diese durch _ ersetzt }
         strpcopy(hpchar,Zeile); for count:=1 to length(Zeile) do if hpchar[count] = '-' then hpchar[count]:='_'; Zeile:=strpas(hpchar);

         Spaltenname:=ParseString(Zeile,1);
         SpaltenArt:=ParseString(Zeile,5);
         if (SpaltenArt <> '60_1') and (SpaltenArt <> '50_1') and (SpaltenArt <> '30_1') and (SpaltenArt <> '20_1') then
            SpaltenArt:=ParseString(Zeile,6);
         val(ParseString(Zeile,2),Spaltenlaenge,count);

         if SpaltenArt = '60_1' then begin if Spaltenlaenge = 8 then Collength:=24 else Collength:=14; Spaltentyp:=Col_Float; end;
         if SpaltenArt = '50_1' then begin Collength:=11; Spaltentyp:=Col_Int; end;
         if SpaltenArt = '30_1' then begin Collength:=Spaltenlaenge; Spaltentyp:=Col_String; end;
         if SpaltenArt = '20_1' then begin Collength:=Spaltenlaenge; Spaltentyp:=Col_String; end;

         bis:=von + Collength - 1;

         if Art = 'ALL' then { Die Konvertierung erfolgt nicht selektiv es sind also alle Datenbanks�tze in einer Table }
         begin
            if DatenbankObj.CheckExists(Spaltenname,'ALL',DatenbankObj.GetAnfang) = false then
               DatenbankObj.Insertcolumn(Spaltenname,Collength,Spaltentyp,'','ALL',von,bis)
            else { Wenn die Datenbankspalte bereits existiert mu� der von bis Bereich gesetzt werden }
               DatenbankObj.SetVonBis(Spaltenname,'ALL',von,bis,DatenbankObj.GetAnfang);
            DBSpalten[i]:=Spaltenname;
         end
         else
         begin
            if ArcObj.GetAnzahl = IfAnz then
            begin
               if DatenbankObj.CheckExists(Spaltenname,'LINE',DatenbankObj.GetAnfang) = false then
                  DatenbankObj.Insertcolumn(Spaltenname,Collength,spaltentyp,'','LINE',von,bis);
            end;
            if LabObj.GetAnzahl = IfAnz then
            begin
               if DatenbankObj.CheckExists(Spaltenname,'POINT',DatenbankObj.GetAnfang) = false then
                  DatenbankObj.Insertcolumn(Spaltenname,Collength,spaltentyp,'','POINT',von,bis);
            end;
            DBSpalten[i]:=Spaltenname;
         end;
         von:=bis + 1;
      end;


      { Nun wird die Datenbankinformation selbst ausgelesen }
      if ArcObj.GetAnzahl = IfAnz then ArcObj.ResetCurrent;
      if LabObj.GetAnzahl = IfAnz then LabObj.ResetCurrent;

      for i:=1 to IfAnz do
      begin
         Application.Processmessages;
         MainWnd.ShowProcent(anz);
         {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(Zeile) + 2;

         subbis:=0; Aktlength:=0;
         for j:=1 to DBAnz do
         begin
            if (Art = '') and (LabObj.Getanzahl = IfAnz) then DatenbankObj.GetVonBis(DBSpalten[j],'POINT',von,bis,SpaltenTyp,DatenbankObj.GetAnfang);
            if (Art = '') and (ArcObj.Getanzahl = IfAnz) then DatenbankObj.GetVonBis(DBSpalten[j],'LINE',von,bis,SpaltenTyp,DatenbankObj.GetAnfang);
            if Art = 'ALL' then DatenbankObj.GetVonBis(DBSpalten[j],'ALL',von,bis,SpaltenTyp,DatenbankObj.GetAnfang);

            AktLength:=AktLength + (bis - von + 1); DBInfo:='';
            if (Aktlength > 80 ) then
            begin
               DBInfo:=copy(Zeile,von-subbis,length(Zeile));
               {$i-} readln(E00Datei,Zeile); {$i+}anz:=anz + length(Zeile) + 2;
               von:=1; subbis:=subbis + 80; bis:=bis - subbis;
               Aktlength:=bis;
            end
            else
            begin
               aktbis:=bis; von:=von - subbis; bis:=bis - subbis;
            end;
            DBInfo:=DBInfo + copy(Zeile,von,bis-von + 1);
            { Die Leerzeichen werden aus der DBInfo gel�scht }
            DBInfo:=Trimleft(DBInfo); DBInfo:=Trimright(DBInfo);

            if Art = '' then
            begin
               if SpaltenTyp = Col_Float then begin val(DBInfo,hdouble,count); DBInfo:=floattostr(hdouble); end; { Der Bin�re Wert wird in einen Zahlenwert umgewandelt }
               if LabObj.GetAnzahl = IfAnz then begin LabObj.InsertAttrib(DBSpalten[j],DBInfo); DatenbankObj.SetLength(DBSpalten[j],length(DBInfo),'POINT',DatenbankObj.Getanfang); end;
               if ArcObj.GetAnzahl = IfAnz then begin ArcObj.InsertAttrib(DBSpalten[j],DBInfo); DatenbankObj.SetLength(DBSpalten[j],length(DBInfo),'LINE',DatenbankObj.Getanfang); end;
            end;
            if Art = 'ALL' then
            begin
               if SpaltenTyp = Col_Float then begin val(DBInfo,hdouble,count); DBInfo:=floattostr(hdouble); end; { Der Bin�re Wert wird in einen Zahlenwert umgewandelt }
               if LabObj.GetAnzahl = IfAnz then begin LabObj.InsertAttrib(DBSpalten[j],DBInfo); DatenbankObj.SetLength(DBSpalten[j],length(DBInfo),'ALL',DatenbankObj.Getanfang); end;
               if ArcObj.GetAnzahl = IfAnz then begin ArcObj.InsertAttrib(DBSpalten[j],DBInfo); DatenbankObj.SetLength(DBSpalten[j],length(DBInfo),'ALL',DatenbankObj.Getanfang); end;
            end;
         end;
         if LabObj.GetAnzahl = IfAnz then LabObj.Setcurrentnext;
         if ArcObj.GetAnzahl = IfAnz then ArcObj.Setcurrentnext;
      end;
      if LabObj.GetAnzahl = IfAnz then ArcExistsA:=true;
      if ArcObj.GetAnzahl = IfAnz then PalExistsA:=true;
   end;
   strdispose(hpchar);
   Result:=anz;
end;

{*******************************************************************************
* FUNKTION : ClassFile.ReadPrj                                                 *
********************************************************************************
* Funktion dient zum Einlesen eines Prj Datensatzes aus dem E00-File.          *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der Bytes, die eingelesen wurden.                       *
********************************************************************************}
function Classfile.ReadPrj(var Zeile:string):longint;
var anz:longint;
begin
   { Die Transformation wird einfach �berlesen }
   anz:=0;
   repeat
      {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(Zeile) + 2;
   until Zeile = 'EOP';
   Result:=anz;
end;

{*******************************************************************************
* FUNKTION : ClassFile.ReadLog                                                 *
********************************************************************************
* Funktion dient zum Einlesen eines Log Datensatzes aus dem E00-File.          *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der Bytes, die eingelesen wurden.                       *
********************************************************************************}
function Classfile.ReadLog(var Zeile:string):longint;
var anz:longint;
begin
   { Die Loginformation wird einfach �berlesen }
   anz:=0;
   repeat
      {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(Zeile) + 2;
   until Zeile = 'EOL';
   Result:=anz;
end;

{*******************************************************************************
* FUNKTION : ClassFile.ReadArc                                                 *
********************************************************************************
* Funktion dient zum Einlesen eines Arc Datensatzes aus dem E00-File.          *
*                                                                              *
*                                                                              *
* R�ckgabewert: Anzahl der Bytes, die eingelesen wurden.                       *
********************************************************************************}
function ClassFile.ReadArc(var Zeile:string):longint;
var coverage_nr,coverage_id,from_node,to_node,left_polygon,right_polygon,number_of_cor:longint;
    anz:longint;
    X,Y  :double;
    count,worked:longint;
begin
   anz:=0;

   { Damit die erste Zeile auch gelesen wird }
   if copy(Zeile,1,3) = 'ARC' then begin {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(Zeile) + 2; end;

   { Nun wird der Header von jedem Arc - gelesen }
   valArc_Header(Zeile,coverage_nr,coverage_id,from_node,to_node,left_polygon,right_polygon,number_of_cor);

   if coverage_nr = -1 then
   begin
      result:=anz; exit;
   end;

   ArcExistsG:=true;

   { Das neue Arc-wird nun in die Liste eingef�gt }
   ArcObj.Neu(coverage_nr,coverage_id,from_node,to_node,left_polygon,right_polygon,number_of_cor);

   { Nun werden die dazugeh�rigen Koordinatenpaare eingelesen }
   worked:=0;
   repeat
      {$i-} readln(E00Datei,Zeile); {$i+} anz:=anz + length(Zeile) + 2;

      { Nun wird das erste Koordinatenpaar der Zeile ermittelt }
      if (worked < number_of_cor) then
      begin
         val(ParseString(Zeile,1),X,count); if count <> 0 then x:=0;
         val(ParseString(Zeile,2),Y,count); if count <> 0 then y:=0;
         ArcObj.InsertPoint(X,Y,DatenbankObj.GetAnfang); worked:=worked + 1;
      end;
      { Nun wird das zweite Koordinatenpaar der Zeile ermittelt }
      if (worked < number_of_cor) and (ParseString(Zeile,3) <> '') then
      begin
         val(ParseString(Zeile,3),X,count); if count <> 0 then x:=0;
         val(ParseString(Zeile,4),Y,count); if count <> 0 then y:=0;
         ArcObj.InsertPoint(X,Y,DatenbankObj.GetAnfang); worked:=worked + 1;
      end;
   until worked >= number_of_cor;
   Result:=anz;
end;

{*******************************************************************************
* FUNKTION : ClassFile.ValArc_Header                                           *
********************************************************************************
* Funktion wertet einen Headerdatensatz eines Arc-Objekts aus dem E00-File aus *
*                                                                              *
* PARAMETER: Source      -> Zeile, die ausgewertet wird                        *
*            coverage_nr -> Nummer des Coverage zu dem dieses Arc geh�rt.      *
*            from_node   -> von Knoten dieses Arcs                             *
*            to_node     -> nach Knoten dieses Arcs                            *
*            left_polygon-> linkes Polygon dieses Arcs                         *
*            right_polygon -> rechts Polygon dieses Arcs                       *
*            number_or_cor -> Anzahl der Koordinatenpaare, die zu diesem Arc   *
*                             geh�ren.                                         *
*                                                                              *
* R�ckgabewert: Anzahl der Bytes, die eingelesen wurden.                       *
********************************************************************************}
procedure ClassFile.ValArc_Header
(
   Source:string; var coverage_nr:longint; var coverage_id:longint; var from_node:longint; var to_node:longint;
   var left_polygon:longint;var right_polygon:longint; var number_of_cor:longint
);
var count:integer;
    Zeile:string;
begin
   Zeile:=Source;
   { Nun wird die Coverage Nummer ermittelt }
   val(ParseString(Zeile,1),coverage_nr,count); if count <> 0 then coverage_nr:=0;

   { Nun wird die Coverage ID ermittelt }
   val(ParseString(Zeile,2),coverage_id,count); if count <> 0 then coverage_id:=0;

   { Nun wird der from_node ermittelt }
   val(ParseString(Zeile,3),from_node,count); if count <> 0 then from_node:=0;

   { Nun wird der to_node ermittelt }
   val(ParseString(Zeile,4),to_node,count); if count <> 0 then to_node:=0;

   { Nun wird das left_polygon ermittelt }
   val(ParseString(Zeile,5),left_polygon,count); if count <> 0 then left_polygon:=0;

   { Nun wird das right_polygon ermittelt }
   val(ParseString(Zeile,6),right_polygon,count); if count <> 0 then right_polygon:=0;

   { Nun wird das number_of_cor ermittelt }
   val(ParseString(Zeile,7),number_of_cor,count); if count <> 0 then number_of_cor:=0;
end;


{*******************************************************************************
* FUNKTION : ClassFile.ParseString                                             *
********************************************************************************
* Funktion extrahiert aus der Zeichenkette Source das durch Pos bestimmte      *
* Element.                                                                     *
*                                                                              *
* PARAMETER: Source      -> Text, von dem Ausgegangen wird.                    *
*            Pos         -> Position des Eintrages der erhalten werden soll.   *
*                                                                              *
* R�ckgabewert: Extrahierte Zeichenkette.                                      *
********************************************************************************}
function ClassFile.ParseString(Source:string;Pos:integer):string;
var hpchar:pchar;
    pos1,pos2,stelle:integer;
    hstr:string;
begin
   hpchar:=stralloc(length(Source)+1);
   strpcopy(hpchar,Source);

   pos1:=0; pos2:=0; stelle:=0;
   repeat
      { Zuerst wird bis zum ersten nicht Leerzeichen gegangen }
      repeat
         if hpchar[pos2] = ' ' then pos2:=pos2 + 1;
      until hpchar[pos2] <> ' ';
      pos1:=pos2;
      { Nun wird wieder bis zum ersten Leerzeichen gegangen }
      repeat
         if (hpchar[pos2] <> ' ') and (pos2 <= strlen(hpchar)) then pos2:=pos2 + 1;
      until (hpchar[pos2] = ' ') or (pos2 >= strlen(hpchar)) or ((hpchar[pos2] = '-') and (hpchar[pos2-1] <> 'E'));
      stelle:=stelle + 1;
   until (stelle = pos) or (pos2 >=strlen(hpchar));

   if stelle = pos then hstr:=copy(source,pos1 + 1,pos2 - pos1) else hstr:='';
   result:=hstr;
   strdispose(hpchar);
end;

end.
