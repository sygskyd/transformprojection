unit SetE00fileDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,StrFile,
  StdCtrls, Buttons, ExtCtrls, FileCtrl, ComCtrls, ShellCtrls,
  FileSelectionPanel, WinOSInfo;

type
  TSetE00fileWnd = class(TForm)
    PageControl: TPageControl;
    TabSheetFiles: TTabSheet;
    GeoBtn: TButton;
    OKBtn: TButton;
    CancelBtn: TButton;
    SelectivCB: TCheckBox;
    SetupWindowTimer: TTimer;
    Panel1: TPanel;
    DestDbLabel: TLabel;
    DbBtn: TButton;
    FileSelectionPanel: TFileSelectionPanel;
    ShellTreeView: TShellTreeView;
    FileListBox: TFileListBox;
    ListBox: TListBox;
    TrashPanel: TPanel;
    TrashImage: TImage;
    AddAllBtn: TButton;
    DeleteAllBtn: TButton;
    IgnoreDuplicatesCb: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure GeoBtnClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure DestDBPCChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure DbBtnClick(Sender: TObject);
    procedure FileSelectionPanelFilesToConvertChanged(Sender: TObject);
  private
    { Private-Deklarationen}
    FirstActivate:boolean;
    CurrentFileIdx:integer;
    procedure CheckWindow;
  public
    { Public-Deklarationen}
    winheight:integer;
    winwidth :integer;
    wintop   :integer;
    winleft  :integer;
    OkBtnMode:boolean;
    function  GetFilename(var Dateiname:string):boolean;
    procedure CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
    procedure SetupForBatch;
    procedure SetImpSettings(ImpMode:integer);
    procedure AddImpFile(aFilename:string);
  end;

var
  SetE00fileWnd: TSetE00fileWnd;
  StrFileObj   : ClassStrFile;

implementation

uses Maindlg, inifiles;

{$R *.DFM}

procedure TSetE00FileWnd.FormCreate(Sender: TObject);
var myScale:double;
    OldHeigth:integer;
    OldWidth:integer;
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   Self.Caption:=StrFileObj.Assign(1);             { ARCINFO-E00 Import 2000 }
   OKBtn.Caption:=StrFileObj.Assign(4);            { Ok }
   CancelBtn.Caption:=StrFileObj.Assign(5);        { Cancel }
   TabSheetFiles.Caption:=StrFileObj.Assign(6);    { Files    }
   GeoBtn.Caption:=StrFileObj.Assign(11);          { Projection }
   DbBtn.Caption:=StrFileObj.Assign(7);               // Database
   SelectivCB.Caption:=StrFileObj.Assign(31);         // convert selective
   IgnoreDuplicatesCb.Caption:=StrFileObj.Assign(56); // Ignore already existing objects

   FirstActivate:=true;
   CurrentFileIdx:=0;

   OldHeigth:=Self.Height;
   OldWidth:=Self.Width;

   Self.Height:=1;
   Self.Width:=1;

   myScale:=1; //100000 / Trunc(9600000/(Self.PixelsPerInch));
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
   
   // now adapt winheigt and winwidth
   winheight:=Round(OldHeigth*myScale);
   winwidth:=Round(OldWidth*myScale);
end;

procedure TSetE00FileWnd.GeoBtnClick(Sender: TObject);
begin
   MainWnd.AxGisProjection.SetupByDialog;
end;

procedure TSetE00FileWnd.CancelBtnClick(Sender: TObject);
begin
   MainWnd.ExitNormal:=false;
   MainWnd.CloseTimer.Enabled:=true;
end;

procedure TSetE00FileWnd.CheckWindow;
var allok:boolean;
begin
   allok:=true;
   if FileSelectionPanel.NumFilesToConvert = 0 then allok:=false;
   if allok then OkBtn.Enabled:=true
   else OkBtn.Enabled:=false;
end;

function TSetE00FileWnd.GetFilename(var Dateiname:string):boolean;
var retVal:boolean;
begin
   retVal:=TRUE;
   if CurrentFileIdx < FileSelectionPanel.NumFilesToConvert then
      Dateiname:=FileSelectionPanel.GetFilename(CurrentFileIdx)
   else
      retVal:=FALSE;
   result:=retVal;
   CurrentFileIdx:=CurrentFileIdx+1;
end;


procedure TSetE00fileWnd.OKBtnClick(Sender: TObject);
var inifile:TIniFile;
begin
   MainWnd.ExitNormal:=true;

   // write current settings to the ConGIS.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir + 'ConGIS.ini');
   inifile.WriteString('E00IMP','ImpDbMode',inttostr(MainWnd.AXImpExpDbc.ImpDbMode));
   inifile.WriteString('E00IMP','ImpDatabase',MainWnd.AXImpExpDbc.ImpDatabase);
   inifile.WriteString('E00IMP','ImpTable',MainWnd.AXImpExpDbc.ImpTable);

   inifile.WriteString('E00IMP','SourceCoordinateSystem',inttostr(MainWnd.AxGisProjection.SourceCoordinateSystem));
   inifile.WriteString('E00IMP','SourceProjSettings',MainWnd.AxGisProjection.SourceProjSettings);
   inifile.WriteString('E00IMP','SourceProjection',MainWnd.AxGisProjection.SourceProjection);
   inifile.WriteString('E00IMP','SourceDate',MainWnd.AxGisProjection.SourceDate);
   inifile.WriteString('E00IMP','SourceXOffset',floattostr(MainWnd.AxGisProjection.SourceXOffset));
   inifile.WriteString('E00IMP','SourceYOffset',floattostr(MainWnd.AxGisProjection.SourceYOffset));
   inifile.WriteString('E00IMP','SourceScale',floattostr(MainWnd.AxGisProjection.SourceScale));

   inifile.WriteString('E00IMP','LastUsedPath',FileSelectionPanel.Directory);
   if IgnoreDuplicatesCb.Checked then
      inifile.WriteString('E00IMP','IgnoreExistingObjects','T')
   else
      inifile.WriteString('E00IMP','IgnoreExistingObjects','F');

   inifile.Destroy;
   // now the import window can be opened and the import executed
   Self.Close;
   MainWnd.Show;
end;

procedure TSetE00fileWnd.DestDBPCChange(Sender: TObject);
begin
   CheckWindow;
end;

procedure TSetE00fileWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;


procedure TSetE00fileWnd.CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
begin
   wintop:=(WHEIGHT div 2) - (winheight div 2);
   winleft:=(WWIDTH div 2) - (winwidth div 2);
end;

procedure TSetE00fileWnd.SetupWindowTimerTimer(Sender: TObject);
var dummy:string;
    intval, count:integer;
    doubleval:double;
    inifile:TIniFile;
begin
   SetupWindowTimer.Enabled:=false;
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,winleft,wintop,winwidth, winheight,SWP_SHOWWINDOW);
   Self.Height:=winheight;
   Self.Width:=winwidth;
   // Setup the window from the ConGIS.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'ConGIS.ini');
   dummy:=MainWnd.DestDocument.Name;
   dummy:=ChangeFileExt(dummy,'.wgi');
   if FileExists(dummy) then
   begin
      MainWnd.AXImpExpDbc.ImpDbMode:=5; // Idb-Database
      MainWnd.AXImpExpDbc.ImpDatabase:='';
      MainWnd.AXImpExpDbc.ImpTable:='';
   end
   else
   begin
      dummy:=inifile.ReadString('E00IMP','ImpDbMode','0');
      val(dummy, intval, count);
      if intval <> 5 then // Idb-Database
      begin
         MainWnd.AXImpExpDbc.ImpDbMode:=intval;
         dummy:=inifile.ReadString('E00IMP','ImpDatabase','');
         MainWnd.AXImpExpDbc.ImpDatabase:=dummy;
         dummy:=inifile.ReadString('E00IMP','ImpTable','');
         MainWnd.AXImpExpDbc.ImpTable:=dummy;
      end;
   end;
   DestDbLabel.Caption:=MainWnd.AXImpExpDbc.ImpDbInfo;

   // setup the projection
   dummy:=inifile.ReadString('E00IMP','SourceCoordinateSystem','');
   if dummy <> '' then
   begin
      val(dummy, intval, count);
      MainWnd.AxGisProjection.SourceCoordinateSystem:=intval;
      dummy:=inifile.ReadString('E00IMP','SourceProjSettings','NONE');
      MainWnd.AxGisProjection.SourceProjSettings:=dummy;
      dummy:=inifile.ReadString('E00IMP','SourceProjection','NONE');
      MainWnd.AxGisProjection.SourceProjection:=dummy;
      dummy:=inifile.ReadString('E00IMP','SourceDate','NONE');
      MainWnd.AxGisProjection.SourceDate:=dummy;
      dummy:=inifile.ReadString('E00IMP','SourceXOffset','0');
      val(dummy, doubleval, count);
      MainWnd.AxGisProjection.SourceXOffset:=doubleval;
      dummy:=inifile.ReadString('E00IMP','SourceYOffset','0');
      val(dummy, doubleval, count);
      MainWnd.AxGisProjection.SourceYOffset:=doubleval;
      dummy:=inifile.ReadString('E00IMP','SourceScale','1');
      val(dummy, doubleval, count);
      MainWnd.AxGisProjection.SourceScale:=doubleval;
   end;

   dummy:=inifile.ReadString('E00IMP','LastUsedPath','');
   if dummy = '' then
      GetDir(0, dummy);
   FileSelectionPanel.Directory:=dummy;

   dummy:=inifile.ReadString('E00IMP','IgnoreExistingObjects','F');
   if dummy = 'T' then IgnoreDuplicatesCb.Checked:=TRUE else IgnoreDuplicatesCb.Checked:=FALSE;

   inifile.Destroy;
   Self.Repaint;
end;

// procedure is used to setup the SetE00fileWnd for batch mode
procedure TSetE00fileWnd.SetupForBatch;
begin
   if FirstActivate then
   begin
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
   end;
end;

procedure TSetE00fileWnd.SetImpSettings(ImpMode:integer);
begin
   if (ImpMode = 0) then
      SelectivCB.Checked:=FALSE;
   if (ImpMode = 1) then
      SelectivCB.Checked:=TRUE;
end;

procedure TSetE00fileWnd.AddImpFile(aFilename:string);
begin
   FileSelectionPanel.AddFileToConvert(aFilename);
end;

procedure TSetE00fileWnd.DbBtnClick(Sender: TObject);
begin
   MainWnd.AXImpExpDbc.SetupByDialog(Self.Handle, 5, 5);
   PageControl.Enabled:=FALSE;
   GeoBtn.Enabled:=FALSE;
   DbBtn.Enabled:=FALSE;
   OkBtnMode:=OkBtn.Enabled;
   OkBtn.Enabled:=FALSE;
   CancelBtn.Enabled:=FALSE;
end;

procedure TSetE00fileWnd.FileSelectionPanelFilesToConvertChanged(
  Sender: TObject);
begin
   CheckWindow;
end;

end.

