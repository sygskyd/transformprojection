object OptionWnd: TOptionWnd
  Left = 300
  Top = 200
  BorderStyle = bsDialog
  Caption = 'OptionWnd'
  ClientHeight = 202
  ClientWidth = 352
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 10
    Top = 32
    Width = 303
    Height = 16
    Caption = 'Please set the Objects that should be imported'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Status: TPanel
    Left = 8
    Top = 8
    Width = 337
    Height = 25
    Caption = 'File that currently is worked: TEST.E00'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 8
    Top = 48
    Width = 337
    Height = 116
    TabOrder = 1
    object Label2: TLabel
      Left = 72
      Top = 3
      Width = 37
      Height = 13
      Caption = 'Graphic'
    end
    object Label3: TLabel
      Left = 8
      Top = 24
      Width = 60
      Height = 16
      Caption = 'Polygons:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 51
      Width = 35
      Height = 16
      Caption = 'Lines:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 8
      Top = 88
      Width = 40
      Height = 16
      Caption = 'Points:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 282
      Top = 3
      Width = 44
      Height = 13
      Caption = 'Attributes'
    end
    object GroupBox1: TGroupBox
      Left = 72
      Top = 16
      Width = 193
      Height = 97
      TabOrder = 0
      object Label8: TLabel
        Left = 32
        Top = 56
        Width = 48
        Height = 13
        Caption = 'as Symbol'
      end
      object Label9: TLabel
        Left = 56
        Top = 72
        Width = 31
        Height = 13
        Caption = 'Name:'
      end
      object GPolyCB: TCheckBox
        Left = 9
        Top = 10
        Width = 15
        Height = 17
        TabOrder = 0
        OnClick = GPolyCBClick
      end
      object GLineCB: TCheckBox
        Left = 9
        Top = 37
        Width = 15
        Height = 17
        TabOrder = 1
        OnClick = GLineCBClick
      end
      object GPointCB: TCheckBox
        Left = 9
        Top = 72
        Width = 15
        Height = 17
        TabOrder = 2
        OnClick = GPointCBClick
      end
      object GSymbolCB: TCheckBox
        Left = 32
        Top = 72
        Width = 17
        Height = 17
        TabOrder = 3
        OnClick = GSymbolCBClick
      end
      object SymNameCB: TComboBox
        Left = 88
        Top = 69
        Width = 97
        Height = 21
        ItemHeight = 13
        TabOrder = 4
      end
    end
    object GroupBox2: TGroupBox
      Left = 288
      Top = 16
      Width = 32
      Height = 97
      TabOrder = 1
      object APolyCB: TCheckBox
        Left = 9
        Top = 10
        Width = 15
        Height = 17
        TabOrder = 0
      end
      object ALineCB: TCheckBox
        Left = 9
        Top = 37
        Width = 15
        Height = 17
        TabOrder = 1
      end
      object APointCB: TCheckBox
        Left = 9
        Top = 72
        Width = 15
        Height = 17
        TabOrder = 2
      end
    end
  end
  object OK: TButton
    Left = 252
    Top = 173
    Width = 93
    Height = 23
    Caption = 'Ok'
    Default = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 2
    OnClick = OKClick
  end
end
