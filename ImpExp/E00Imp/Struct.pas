unit struct;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs;

type
  { Hier k�nnen Strukturen definiert werden }
   p_Punkt_record=^Punkt_record;
   Punkt_record = record
       X:double;
       Y:double;
       Insel:boolean;
       next :p_Punkt_record;
       prev :p_Punkt_record;
   end;

   p_Datenbank_record =^Datenbank_record;
   Datenbank_record = record
      ColName     : string;
      ColLength   : integer;
      ColType     : integer;
      ColInfo     : string;
      von         : integer;
      bis         : integer;
      Art         : string;
      next        : p_Datenbank_record;
   end;

   p_arc_record =^arc_record;
   arc_record = record
      coverage_nr  :longint;
      coverage_id  :longint;
      from_node    :longint;
      to_node      :longint;
      left_polygon :longint;
      right_polygon:longint;
      number_of_cor:longint;
      Insel        :boolean;
      FirstPoint   :p_Punkt_record;
      LastPoint    :p_Punkt_record;
      DBInfo       :p_Datenbank_record;
      next         :p_arc_record;
      prev         :p_arc_record;
   end;

   p_cnt_record = ^cnt_record;
   cnt_record = record
      X :double;
      Y :double;
      number_of_labels:longint;
      DBInfo:p_Datenbank_record;
      next :p_cnt_record;
   end;

   p_polygon_record = ^polygon_record;
   polygon_record = record
      arc_number:longint;
      node_number:longint;
      polygon_number:longint;
      next:p_polygon_record;
   end;

   p_pal_record =^pal_record;
   pal_record = record
      number_of_arcs:longint;
      x_min:double;
      y_min:double;
      x_max:double;
      y_max:double;
      Punktanz:longint;
      Polygone:p_polygon_record;
      FirstPoint :p_punkt_record;
      LastPoint  :p_punkt_record;
      DBInfo:p_datenbank_record;
      next:p_pal_record;
   end;

   p_lab_record = ^lab_record;
   lab_record = record
      coverage_id:longint;
      encloses_polygon:longint;
      X:double;
      Y:double;
      DBInfo   :p_Datenbank_record;
      next:p_lab_record;
   end;

ClassLab = class
   public
      constructor Create;
      destructor Destroy;
      procedure Neu(coverage_id:longint;encloses_polygon:longint;X:double;Y:double;DBanfang:p_Datenbank_record);
      function GetAnzahl:longint;
      procedure ResetCurrent;
      procedure SetCurrentNext;
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      procedure WriteCurrent(DBAnfang:p_Datenbank_record;Art:string);
   private
      Anfang,Ende,Current :p_lab_record;
      Anzahl              :longint;
end;
ClassPal = class
   public
      constructor Create;
      destructor Destroy;
      procedure Neu(number_of_arcs:longint;X_min:double;Y_min:double;X_max:double;Y_max:double);
      procedure InsertPolygon(arc_number:longint;node_number:longint;polygon_number:longint);
      function GetAnzahl:longint;
      procedure InsertPoint(var Current_pal:p_pal_record;X:double;Y:double;Insel:boolean);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      function GetCurrent:p_pal_record;
      procedure SetCurrentNext;
      procedure WriteCurrent(DBAnfang:p_Datenbank_record;Art:string);
      procedure ResetCurrent;
   private
      { private Definitionen }
      Anfang,Ende,Current :p_pal_record;
      Anzahl              :longint;
end;

ClassArc = class
   public
      { public Definitionen }
      constructor Create;
      destructor  Destroy;
      procedure Neu(coverage_nr,coverage_id,from_node,to_node,left_polygon,right_polygon,number_of_cor:longint);
      procedure InsertPoint(X:double;Y:double;DBAnfang:p_Datenbank_record);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
      procedure Reset;
      function  GetAnzahl:longint;
      function  GetFirst:p_arc_record;
      function  GetLast :p_arc_record;
      procedure GetArcs(var Current_pal:p_pal_record;Index:longint;ArcAnfang:p_arc_record);
      procedure ResetCurrent;
      procedure SetCurrentNext;
      procedure WriteCurrent(DBAnfang:p_Datenbank_record;Art:string);
   private
      { private Definitionen }
      Anfang,Ende,Current :p_arc_record;
      Anzahl              :longint;
      function FindArc(aIndex:integer):p_arc_record;
end;

ClassConvert = class
   public
      { public Definitionen }
      constructor Create;
      procedure CreatePolygon(Current:p_pal_record;ARCAnfang:p_arc_record);
      procedure ResetObjektnr;
   private
      { private Definitionen }
      Current_Pal :p_pal_record;
      Current_Arc :p_arc_record;
end;

ClassDatenbank = class
   public
      { public Definitionen }
      constructor Create;
      destructor Destroy;
      procedure Neu(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string; Art:string; var MyAnfang:p_datenbank_record; von:integer; bis:integer);
      function Checkexists(Eintrag:string;Art:string;DBAnfang:p_Datenbank_record):boolean;
      procedure SetLength(Bezeichnung:string; Laenge:integer;Art:string;DBAnfang:p_Datenbank_record);
      procedure SetVonBis(Bezeichnung:string;Art:string;von:integer;bis:integer; DBAnfang:P_Datenbank_record);
      procedure GetVonBis(Bezeichnung:string;Art:string;var von:integer;var bis:integer; var Spaltentyp:integer; DBAnfang:P_Datenbank_record);
      function  GetLength(Bezeichnung:string;Art:string;DBAnfang:p_Datenbank_record):integer;
      procedure Insertcolumn(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string; Art:string; von:integer; bis:integer);
      procedure ResetColumn;
      function  SetDBFirst(DBAnfang:p_Datenbank_record;Art:string):integer;
      function GetDBColumnname:string;
      function GetDataType:integer;
      procedure SetCurrentnext;
      function GetAnfang:p_Datenbank_record;
   private
      { private Definitionen }
      Anfang, Current:p_Datenbank_record;
end;

ClassCnt = class
   public
      { public Definitionen }
      constructor Create;
      destructor Destroy;
      procedure Neu(number_of_labels:longint; X:double; Y:double; DBAnfang:p_Datenbank_record);
      function GetAnzahl:longint;
      procedure WriteCurrent(DBAnfang:p_Datenbank_record;Art:string);
      procedure InsertAttrib(Spaltenname:string;Spalteninhalt:string);
   private
      { private Definitionen }
      Anfang,Ende,Current:p_cnt_record;
      Anzahl:longint;
end;

implementation
uses files,OptionDlg,MainDlg;

var FileObj     :ClassFile;
    ArcObj      :ClassArc;
    PalObj      :ClassPal;
    DatenbankObj:ClassDatenbank;
    Objektnummer:integer;
    Insel:boolean;
    IndexList           :array of p_arc_record;
    NumIndexList        :integer;

constructor ClassArc.Create;
begin
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
   NumIndexList:=0;
end;

constructor ClassLab.Create;
begin
   Anfang:=new(p_lab_record); Ende:=new(p_lab_record); Current:=new(p_lab_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassCnt.Create;
begin
   Anfang:=new(p_cnt_record); Ende:=new(p_cnt_record); Current:=new(p_cnt_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassPal.Create;
begin
   Anfang:=new(p_pal_record); Ende:=new(p_pal_record); Current:=new(p_pal_record);
   Anfang:=nil; Current:=nil; Ende:=nil; Anzahl:=0;
end;

constructor ClassDatenbank.Create;
begin
   Anfang:=new(p_Datenbank_record); Current:=new(p_Datenbank_record);
   Anfang:=nil; Current:=nil;
end;

constructor ClassConvert.Create;
begin
   Current_pal:=new(p_pal_record); Current_pal:=nil;
   Current_arc:=new(p_arc_record); Current_arc:=nil;
   Objektnummer:=0; Insel:=false;
end;

destructor ClassDatenbank.Destroy;
var Zeiger1,Zeiger2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;

   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      dispose(Zeiger2);
   until Zeiger1 = nil;

   Anfang:=nil; Current:=nil;
end;


destructor ClassPal.Destroy;
var Zeiger1,Zeiger2:p_pal_record;
    Polygon1,Polygon2: p_polygon_record;
    Punkt1,Punkt2: p_punkt_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;

      { L�schen der Polygone }
      Polygon1:=Zeiger2^.Polygone; Polygon2:=Zeiger2^.Polygone;
      if Polygon1 <> nil then
      repeat
         Polygon2:=Polygon1;
         Polygon1:=Polygon1^.next;
         dispose(Polygon2);
      until Polygon1 = nil;

      { L�schen der Punkte }
      Punkt1:=Zeiger2^.FirstPoint; Punkt2:=Zeiger2^.FirstPoint;
      if Punkt1 <> nil then
      repeat
         Punkt2:=Punkt1;
         Punkt1:=Punkt1^.next;
         dispose(Punkt2);
      until Punkt1 = nil;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;

      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassLab.Destroy;
var Zeiger1,Zeiger2:p_lab_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;

destructor ClassCnt.Destroy;
var Zeiger1,Zeiger2:p_cnt_record;
    DB1,DB2:p_Datenbank_record;
begin
   {Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;
      { L�schen der Datenbankinformation }
    {  DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;
      dispose(Zeiger2);
   until Zeiger1 = nil; }
end;


destructor ClassArc.Destroy;
var Zeiger1,Zeiger2:p_arc_record;
    Punkt1,Punkt2: p_punkt_record;
    DB1,DB2:p_Datenbank_record;
begin
   Zeiger1:=Anfang; Zeiger2:=Anfang;
   if Anfang <> nil then
   repeat
      Zeiger2:=Zeiger1;
      Zeiger1:=Zeiger1^.next;


      { L�schen der Punkte }
      Punkt1:=Zeiger2^.FirstPoint; Punkt2:=Zeiger2^.FirstPoint;
      if Punkt1 <> nil then
      repeat
         Punkt2:=Punkt1;
         Punkt1:=Punkt1^.next;
         dispose(Punkt2);
      until Punkt1 = nil;

      { L�schen der Datenbankinformation }
      DB1:=Zeiger2^.DBInfo; DB2:=Zeiger2^.DBInfo;
      if DB1 <> nil then
      repeat
         DB2:=DB1;
         DB1:=DB1^.next;
         dispose(DB2);
      until DB1 = nil;
      dispose(Zeiger2);
   until Zeiger1 = nil;
end;


{*******************************************************************************
* FUNKTION : ClassDatenbank.Checkexists                                        *
********************************************************************************
* Funktion �berpr�ft, ob der entsprechende Spalteneintrag bereits in der       *
* Datenbankliste ist.                                                          *
*                                                                              *
* R�ckgabewert: TRUE  -> Eintrag ist bereits in Liste.                         *
*               FALSE -> Eintrag ist noch nicht in Liste.                      *
*                                                                              *
********************************************************************************}
function ClassDatenbank.Checkexists(Eintrag:string;Art:string;DBAnfang:p_Datenbank_record):boolean;
var Zeiger:p_Datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=DBAnfang; gefunden:=false;
   if DBAnfang = nil then begin Result:=false; exit; end;
   repeat
      if ((Zeiger^.Colname = Eintrag) and (Zeiger^.Art = Art)) or
         ((Zeiger^.Colname = Eintrag) and (Art = 'ALL')) then
         gefunden:= true;
      Zeiger:=Zeiger^.next;
   until (Zeiger = nil) or (gefunden = true);
   Result:=gefunden;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.GetAnfang                                          *
********************************************************************************
* Funktion liefert den Anfang der Datenbankliste.                              *
*                                                                              *
*                                                                              *
* R�ckgabewert: Zeiger auf den Anfang der Datenbankliste.                      *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetAnfang:p_Datenbank_record;
begin
   Result:=Anfang;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.SetDBFirst                                         *
********************************************************************************
* Funktion setzt den Akt_Datenbank Zeiger auf den ersten Datenbankspalten-     *
* eintrag in der Datenbankliste.                                               *
*                                                                              *
* R�ckgabewert: Anzahl der vorhandenen Datenbankspalten.                       *
*                                                                              *
********************************************************************************}
function ClassDatenbank.SetDBFirst(DBAnfang:p_Datenbank_record;Art:string):integer;
var iterate:p_Datenbank_record;
    retVal:integer;
begin
   if Art = 'NO' then begin Result:=0; exit; end;
   iterate:=DBAnfang; Current:=nil;
   // first find the first column that
   while (iterate <> nil) and (Current = nil) do
   begin
      if (iterate^.Art = 'ALL') or (iterate^.Art = Art) then
         Current:=iterate;
      iterate:=iterate^.next;
   end;
   retVal:=0;
   iterate:=Current;
   while (iterate <> nil) do
   begin
      if (iterate^.Art = 'ALL') or (iterate^.Art = Art) then
         retVal:=retVal+1;
      iterate:=iterate^.next;
   end;
   result:=retVal;
end;


{*******************************************************************************
* FUNKTION : ClassDatenbank.GetDBColumnname                                    *
********************************************************************************
* Funktion gibt den Spaltenname auf dem sich gerade der Akt_Datenbank Zeiger   *
* befindet zur�ck.                                                             *
*                                                                              *
* R�ckgabewert: Spaltenname der sich auf Akt_Datenbank befindet.               *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetDBColumnname:string;
begin
   Result:=Current^.ColName;
end;

function ClassDatenbank.GetDataType:integer;
begin
   Result:=Current^.ColType;
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.SetCurrentnext                                     *
********************************************************************************
* Prozedur setzt den Akt_Datenbank Zeiger um eines weiter.                     *
*                                                                              *
* R�ckgabewert: KEINER                                                         *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.SetCurrentnext;
begin
   Current:=Current^.next;
end;



{*******************************************************************************
* FUNKTION : ClassArc.GetAnzahl                                                *
********************************************************************************
* Funktion gibt die Anzahl der eingelesenen ARC�s als R�ckgabewert zur�ck.     *
*                                                                              *
* R�ckgabewert: Anzahl der eingelesenen Arc Objekte                            *
*                                                                              *
********************************************************************************}
function ClassArc.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

{*******************************************************************************
* FUNKTION : ClassArc.GetAnfang                                                *
********************************************************************************
* Funktion gibt den Zeiger auf den Anfang der Arc - Liste zur�ck               *
*                                                                              *
* R�ckgabewert: Zeiger auf den Anfang der ARC-Liste.                           *
*                                                                              *
********************************************************************************}
function ClassArc.GetFirst:p_arc_record;
begin
   Result:=Anfang;
end;

function ClassArc.GetLast:p_arc_record;
begin
   Result:=Ende;
end;

{*******************************************************************************
* FUNKTION : ClassPunkt.GetPunktanz                                            *
********************************************************************************
* Funktion gibt die Anzahl der eingelesenen Punkte als R�ckgabewert zur�ck.    *
*                                                                              *
* R�ckgabewert: Anzahl der eingelesenen Punkte                                 *
*                                                                              *
********************************************************************************}
function ClassCnt.GetAnzahl:longint;
begin
   Result:=Anzahl;
end;

{*******************************************************************************
* PROZEDUR : ClassArc.InsertAttrib                                             *
********************************************************************************
* Prozedur f�gt einen Datenbankinformationseintrag zum Akt_Polygon ein.        *
*                                                                              *
* PARAMETER: Spaltenname   -> Name der Spalte die eingef�gt werden soll.       *
*            Spalteninhalt -> Inhalt der Datenbankspalte.                      *
*                                                                              *
********************************************************************************}
procedure ClassArc.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,'',Current^.DBInfo,0,0);
end;

{*******************************************************************************
* PROZEDUR : ClassCnt.InsertAttrib                                             *
********************************************************************************
* Prozedur f�gt einen Datenbankinformationseintrag zum Current   ein.          *
*                                                                              *
* PARAMETER: Spaltenname   -> Name der Spalte die eingef�gt werden soll.       *
*            Spalteninhalt -> Inhalt der Datenbankspalte.                      *
*                                                                              *
********************************************************************************}
procedure ClassCnt.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,'',Current^.DBInfo,0,0);
end;

{*******************************************************************************
* PROZEDUR : ClassPal.InsertAttrib                                             *
********************************************************************************
* Prozedur f�gt einen Datenbankinformationseintrag zum Current   ein.          *
*                                                                              *
* PARAMETER: Spaltenname   -> Name der Spalte die eingef�gt werden soll.       *
*            Spalteninhalt -> Inhalt der Datenbankspalte.                      *
*                                                                              *
********************************************************************************}
procedure ClassPal.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,'',Current^.DBInfo,0,0);
end;

{*******************************************************************************
* PROZEDUR : ClassLab.InsertAttrib                                             *
********************************************************************************
* Prozedur f�gt einen Datenbankinformationseintrag zum Current   ein.          *
*                                                                              *
* PARAMETER: Spaltenname   -> Name der Spalte die eingef�gt werden soll.       *
*            Spalteninhalt -> Inhalt der Datenbankspalte.                      *
*                                                                              *
********************************************************************************}
procedure ClassLab.InsertAttrib(Spaltenname:string;Spalteninhalt:string);
begin
   DatenbankObj.Neu(Spaltenname,0,Col_String,Spalteninhalt,'',Current^.DBInfo,0,0);
end;


{*******************************************************************************
* PROZEDUR : ClassDatenbank.InsertColumn                                       *
********************************************************************************
* Prozedur erzeugt einen neuen Spalteneintrag f�r die Datenbank und f�gt diesen*
* in die Datenbank_anfang Liste ein.                                           *
*                                                                              *
* PARAMETER: Bezeichnung -> Spaltenbezeichnung                                 *
*            Laenge      -> L�nge der Spalte                                   *
*            Typ         -> Typ der Spalte                                     *
*            Inhalt      -> Inhalt der Spalte                                  *
*            von         -> Vonspalte f�r das Auslesen aus dem E00-File        *
*            bis         -> Bisspalte f�r das Auslesen aus dem E00-File        *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.Insertcolumn(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string; Art:string; von:integer; bis:integer);
begin
   Neu(Bezeichnung, Laenge, Typ, Inhalt,Art,Anfang, von, bis);
end;

procedure ClassArc.Reset;
begin
   Current:=Anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.ResetColumn                                        *
********************************************************************************
* Prozedur dient dazu den Aktellen Spalteneintragszeiger auf den Anfang zu     *
* setzen.                                                                      *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.ResetColumn;
begin
   Current:=Anfang;
end;

{*******************************************************************************
* PROZEDUR : ClassPal.WriteCurrent                                             *
********************************************************************************
* Prozedur ermittelt die Daten des aktuellen Pal�s     und gibt diese an die   *
* Schreibfunktion in der Unit files weiter.                                    *
*                                                                              *
* PARAMETER: DBAnfang -> Zeiger auf den Anfang der Datenbankliste              *
*            Art      -> Gibt an, welche Datenbankspalten verwendet werden     *
*                                                                              *
********************************************************************************}
procedure ClassPal.WriteCurrent(DBAnfang:p_Datenbank_record;Art:string);
var Zeile,hZeile,Bezeichnung:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i:integer;
    First,gefunden :boolean;
    PunktZeiger:p_Punkt_record;
    Layername:string;
    new_id:integer;
    HasDuplicate:boolean;
begin
   { Schreiben der aktuellen Punktdaten }
   if Current = nil then exit;
   Objektnummer:=Objektnummer + 1; first:=true; PunktZeiger:=Current^.FirstPoint;

   Layername:=MainWnd.CurrLayername;

   if Art <> 'ALL' then
      Layername:=Layername+'_Poly';

   if MainWnd.CheckLayerExists(Layername) = FALSE then
      MainWnd.CreateLayer(Layername)
   else
      MainWnd.SetCurrentLayer(Layername);

   if PunktZeiger = nil then exit;

   MainWnd.CreatePolygon;

   repeat
      { Wenn sich eine Insel innerhalb befindet, so muss die Objektnummer erh�ht werden }
      if PunktZeiger^.Insel = true then Objektnummer:=Objektnummer + 1;

      MainWnd.InsertPointToPolygon(PunktZeiger^.X, PunktZeiger^.Y);


      Punktzeiger:=Punktzeiger^.next;
   until PunktZeiger = nil;
   HasDuplicate:=FALSE;
   new_id:=MainWnd.ClosePolygon(HasDuplicate);

   // write database information
   if (Art <> 'NO') then
   begin
      DBAnz:=FileObj.SetDBAnfang(Art);
      DBZeiger:=Current^.DBInfo;
      if HasDuplicate then
         MainWnd.AXImpExpDbc.DoEditImpInfo(Layername, new_ID)
      else
         MainWnd.AXImpExpDbc.AddImpIdInfo(Layername, new_ID);

      for i:=1 to DBAnz do
      begin
         Bezeichnung:=FileObj.GetDBColumnname; FileObj.SetToNextDatabaseColumn;
         DBZeiger:=Current^.DBInfo;
         if DBZeiger <> nil then
         begin
            { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
            DBZeiger:=Current^.DBInfo; gefunden:=false;
            repeat
               if DBZeiger^.Colname = Bezeichnung then gefunden:=true;
               if gefunden = false then DBZeiger:=DBZeiger^.next;
            until (gefunden = true) or (DBZeiger = nil);
            if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
               hZeile:=DBZeiger^.ColInfo
            else hZeile:='';
         end
         else
            hZeile:='';
         MainWnd.AXImpExpDbc.AddImpInfo(Layername,Bezeichnung,hZeile);
      end;
   end;
end;



{*******************************************************************************
* PROZEDUR : ClassLab.WriteCurrent                                             *
********************************************************************************
* Prozedur ermittelt die Daten des aktuellen Lab�s     und gibt diese an die   *
* Schreibfunktion in der Unit files weiter.                                    *
*                                                                              *
* PARAMETER: DBAnfang -> Zeiger auf den Anfang der Datenbankliste              *
*            Art      -> Gibt an, welche Datenbankspalten verwendet werden     *
*                                                                              *
********************************************************************************}
procedure ClassLab.WriteCurrent(DBAnfang:p_Datenbank_record;Art:string);
var hZeile,Bezeichnung:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i:integer;
    First,gefunden :boolean;
    Layername:string;
    new_id:integer;
    HasDuplicate:boolean;
begin
   { Schreiben der aktuellen Punktdaten }
   if Current = nil then exit;
   Objektnummer:=Objektnummer + 1; first:=true;

   Layername:=MainWnd.CurrLayername;
   if Art <> 'ALL' then
      Layername:=Layername+'_Point';

   if MainWnd.CheckLayerExists(Layername) = FALSE then
      MainWnd.CreateLayer(Layername)
   else
      MainWnd.SetCurrentLayer(Layername);

   HasDuplicate:=FALSE;
   if OptionWnd.DoConvertSymbolGraphic = true then
   begin
      { Schreiben eines Symbols mit dieser Nummer }
      new_id:=MainWnd.CreateSymbol(Current^.X, Current^.Y, OptionWnd.GetSymName, HasDuplicate);
   end
   else
   begin
      new_id:=MainWnd.InsertPoint(Current^.X, Current^.Y, HasDuplicate);
   end;

   if Art = 'NO' then exit;

   // write database information
   if HasDuplicate then
      MainWnd.AXImpExpDbc.DoEditImpInfo(Layername, new_ID)
   else
      MainWnd.AXImpExpDbc.AddImpIdInfo(Layername, new_ID);
   DBAnz:=FileObj.SetDBAnfang(Art);
   DBZeiger:=Current^.DBInfo;
   for i:=1 to DBAnz do // now iterate through the database columns
   begin
      Bezeichnung:=FileObj.GetDBColumnname; FileObj.SetToNextDatabaseColumn;
      DBZeiger:=Current^.DBInfo;
      if DBZeiger <> nil then
      begin
         { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
         DBZeiger:=Current^.DBInfo; gefunden:=false;
         repeat
            if DBZeiger^.ColName = Bezeichnung then gefunden:=true;
            if gefunden = false then DBZeiger:=DBZeiger^.next;
         until (gefunden = true) or (DBZeiger = nil);
         if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
            hZeile:=DBZeiger^.ColInfo
         else
            hZeile:='';
      end
      else
         hZeile:='';
      MainWnd.AXImpExpDbc.AddImpInfo(Layername,Bezeichnung,hZeile);
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassArc.WriteCurrent                                             *
********************************************************************************
* Prozedur ermittelt die Daten des aktuellen Arc�s     und gibt diese an die   *
* Schreibfunktion in der Unit files weiter.                                    *
*                                                                              *
* PARAMETER: DBAnfang -> Zeiger auf den Anfang der Datenbankliste              *
*            Art      -> Gibt an welche Datenbankspalten verwendet werden      *
*                                                                              *
********************************************************************************}
procedure ClassArc.WriteCurrent(DBAnfang:p_Datenbank_record;Art:string);
var Zeile,hZeile,Bezeichnung:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i:integer;
    First,gefunden :boolean;
    PunktZeiger:p_Punkt_record;
    Layername:string;
    new_id:integer;
    HasDuplicate:boolean;
begin
   if Current = nil then exit;

   { Schreiben der aktuellen Punktdaten }
   Objektnummer:=Objektnummer + 1; first:=true; PunktZeiger:=Current^.FirstPoint;

   Layername:=MainWnd.CurrLayername;

   if Art <> 'ALL' then
      Layername:=Layername+'_Line';

   if MainWnd.CheckLayerExists(Layername) = FALSE then
      MainWnd.CreateLayer(Layername)
   else
      MainWnd.SetCurrentLayer(Layername);

   if Punktzeiger = nil then exit;

   { Now a new Polyline Object has to be created }
   MainWnd.CreatePolyline;

   repeat
      MainWnd.InsertPointToPolyLine(PunktZeiger^.X, PunktZeiger^.Y);
      Punktzeiger:=Punktzeiger^.next;
   until PunktZeiger = nil;
   HasDuplicate:=FALSE;
   new_id:=MainWnd.ClosePolyline(HasDuplicate);

   // write database information
   if (Art <> 'NO') then
   begin
      DBAnz:=FileObj.SetDBAnfang(Art);
      DBZeiger:=Current^.DBInfo;
      if HasDuplicate then
         MainWnd.AXImpExpDbc.DoEditImpInfo(Layername, new_ID)
      else
         MainWnd.AXImpExpDbc.AddImpIdInfo(Layername, new_ID);

      for i:=1 to DBAnz do // now iterate through the database columns
      begin
         Bezeichnung:=FileObj.GetDBColumnname; FileObj.SetToNextDatabaseColumn;
         DBZeiger:=Current^.DBInfo;
         if DBZeiger <> nil then
         begin
            { Suchen nach der Bzeichnung in der Datenbankinformation des Objekts }
            DBZeiger:=Current^.DBInfo; gefunden:=false;
            repeat
               if DBZeiger^.Colname = Bezeichnung then gefunden:=true;
               if gefunden = false then DBZeiger:=DBZeiger^.next;
            until (gefunden = true) or (DBZeiger = nil);
            if (gefunden = true) then { Die aktuelle Spalte existiert f�r dieses Objekt }
               hZeile:=DBZeiger^.ColInfo
            else hZeile:='';
         end
         else
            hZeile:='';
         MainWnd.AXImpExpDbc.AddImpInfo(Layername,Bezeichnung,hZeile);
      end;
   end;
end;

procedure ClassCnt.WriteCurrent(DBAnfang:p_Datenbank_record;Art:string);
var Zeile,hZeile,Bezeichnung:string;
    DBZeiger:p_Datenbank_record;
    Laenge,DBAnz,i:integer;
    First :boolean;
begin
   { Schreiben der aktuellen Punktdaten }
   Objektnummer:=Objektnummer + 1; First:=true;
   Zeile:='';
   hZeile:=floattostr(Current^.X);

   { Schreiben der Y-Koordinate }
   hZeile:=floattostr(Current^.Y);

   DBAnz:=FileObj.SetDBAnfang(Art);
   DBZeiger:=Current^.DBInfo; first:=false;

   for i:=1 to DBAnz do // now iterate through the database columns
   begin
      Bezeichnung:=FileObj.GetDBColumnname; FileObj.SetToNextDatabaseColumn;
      Zeile:=Zeile + ' ';
      if DBZeiger <> nil then
      begin
         if Bezeichnung = DBZeiger^.ColName then { Die aktuelle Spalte existiert f�r dieses Objekt }
         begin
            hZeile:=DBZeiger^.ColInfo;
            DBZeiger:=DBZeiger^.next;
         end
         else hZeile:='';
      end
      else hZeile:='';
      Laenge:=FileObj.GetLength(Bezeichnung,Art);
      repeat if length(hZeile) <> Laenge then hZeile:=' '+hZeile; until length(hZeile) >= Laenge;
      Zeile:=Zeile + hZeile + ' ';
   end;
end;


{*******************************************************************************
* PROZEDUR : ClassCnt.Neu                                                      *
********************************************************************************
* Prozedur erzeugt einen neuen Cnt-Eintrag.                                    *
*                                                                              *
* PARAMETER: number_of_labels -> Anzahl der Attribute, die dieser Cnt besitzt. *
*            X -> X-Koordinate des Punkts                                      *
*            Y -> Y-Koordinate des Punkts                                      *
*                                                                              *
********************************************************************************}
procedure ClassCnt.Neu(number_of_labels:longint; X:double; Y:double; DBAnfang:p_Datenbank_record);
var neuer_record:p_cnt_record;
    hstr:string;
begin
   new(neuer_record);
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.number_of_labels:=number_of_labels;
   neuer_record^.DBInfo:=nil;
   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassLab.Neu                                                      *
********************************************************************************
* Prozedur erzeugt einen neuen Lab Eintrag.                                    *
*                                                                              *
* PARAMETER: coverage_id -> ID, des Coverage auf dem sich dieser Lab befindet  *
*            encloses_polygon -> Nummer des Polygon, das diesen Lab umschlie�t *
*            X -> X-Koordinate des Labs                                        *
*            Y -> Y-Koordinate des Labs                                        *
*                                                                              *
********************************************************************************}
procedure ClassLab.Neu(coverage_id:longint;encloses_polygon:longint;X:double;Y:double;DBAnfang:p_datenbank_record);
var neuer_record,Zeiger:p_lab_record;
    hstr:string;
begin
   new(neuer_record);
   neuer_record^.coverage_id:=coverage_id;
   neuer_record^.encloses_polygon:=encloses_polygon;
   neuer_record^.X:=X;
   neuer_record^.Y:=Y;
   neuer_record^.DBInfo:=nil;
   { Setzen des L�ngenfelds f�r die X und Y Koordinaten }
   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* FUNKTION : ClassPal.GetAnzahl                                                *
********************************************************************************
* Funktion liefert die Anzahl der PAL - Elemente.                              *
*                                                                              *
* R�CKGABEWERT: Anzahl der PAL - Elements.                                     *
********************************************************************************}
function ClassPal.GetAnzahl:longint;
begin
   Result:=Anzahl - 1; { Da der erste PAL nie ber�cksichtigt wird }
end;

{*******************************************************************************
* FUNKTION : ClassLab.GetAnzahl                                                *
********************************************************************************
* Funktion liefert die Anzahl der LAB - Elemente.                              *
*                                                                              *
* R�CKGABEWERT: Anzahl der LAB - Elemente.                                     *
********************************************************************************}
function ClassLab.Getanzahl:longint;
begin
   Result:=Anzahl;
end;

procedure ClassPal.ResetCurrent;
begin
   if Anfang <> nil then current:=anfang^.next else current:=nil;
end;

procedure ClassLab.ResetCurrent;
begin
   current:=Anfang;
end;
procedure ClassArc.ResetCurrent;
begin
   current:=anfang;
end;



{*******************************************************************************
* FUNKTION : ClassPal.GetCurrent                                               *
********************************************************************************
* Funktion liefert den Zeiger auf das akutelle PAL-Element                     *
*                                                                              *
* R�CKGABEWERT: Zeiger auf das aktuelle PAL-Element                            *
********************************************************************************}
function ClassPal.GetCurrent:p_pal_record;
begin
   Result:=Current;
end;

{*******************************************************************************
* PROZEDUR : ClassPal.SetCurrentNext                                           *
********************************************************************************
* Prozedur setzt den Aktuellen Pal-Zeiger auf das n�chste Element.             *
*                                                                              *
********************************************************************************}
procedure ClassPal.SetCurrentNext;
begin
   if Current^.next <> nil then Current:=Current^.next;
end;


{*******************************************************************************
* PROZEDUR : ClassLab.SetCurrentNext                                           *
********************************************************************************
* Prozedur setzt den Aktuellen Lab-Zeiger auf das n�chste Element.             *
*                                                                              *
********************************************************************************}
procedure ClassLab.SetCurrentNext;
begin
   if Current^.next <> nil then Current:=Current^.next;
end;


procedure ClassArc.SetCurrentNext;
begin
   if Current <> nil then if Current^.next <> nil then Current:=Current^.next;
end;

{*******************************************************************************
* PROZEDUR : ClassPal.Neu                                                      *
********************************************************************************
* Prozedur erzeugt einen neuen Pal Eintrag.                                    *
*                                                                              *
* PARAMETER: coverage_id -> ID, des Coverage auf dem sich dieser Lab befindet  *
*            encloses_polygon -> Nummer des Polygon, das diesen Lab umschlie�t *
*            X -> X-Koordinate des Labs                                        *
*            Y -> Y-Koordinate des Labs                                        *
*                                                                              *
********************************************************************************}
procedure ClassPal.Neu(number_of_arcs:longint;X_min:double;Y_min:double;X_max:double;Y_max:double);
var neuer_record,Zeiger:p_pal_record;
begin
   new(neuer_record);
   neuer_record^.number_of_arcs:=number_of_arcs;
   neuer_record^.X_min:=x_min;
   neuer_record^.X_max:=x_max;
   neuer_record^.Y_min:=y_min;
   neuer_record^.y_max:=y_max;
   neuer_record^.Polygone:=nil;
   neuer_record^.DBInfo:=nil;
   neuer_record^.FirstPoint:=nil;
   neuer_record^.LastPoint:=nil;
   neuer_record^.Punktanz:=0;
   neuer_record^.next:=nil;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end
   else
   begin
      Ende^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
end;

{*******************************************************************************
* PROZEDUR : ClassArc.Neu                                                      *
********************************************************************************
* Prozedur erzeugt einen neuen Arc Eintrag.    Die Punkte, die zum Arc         *
* geh�ren, werden erst sp�ter mit der Prozedur InsertPoint eingetragen.        *
*                                                                              *
********************************************************************************}
procedure ClassArc.Neu(coverage_nr,coverage_id,from_node,to_node,left_polygon,right_polygon,number_of_cor:longint);
var neuer_record,Zeiger:p_arc_record;
    aIndex:integer;
begin
   new(neuer_record);
   neuer_record^.coverage_nr:=coverage_nr;
   neuer_record^.coverage_id:=coverage_id;
   neuer_record^.from_node:=from_node;
   neuer_record^.to_node:=to_node;
   neuer_record^.left_polygon:=left_polygon;
   neuer_record^.right_polygon:=right_polygon;
   neuer_record^.number_of_cor:=number_of_cor;
   neuer_record^.Insel:=false;
   neuer_record^.FirstPoint:=nil;
   neuer_record^.LastPoint:=nil;
   neuer_record^.DBInfo:=nil;
   neuer_record^.next:=nil;
   neuer_record^.prev:=nil;

   if Anfang = NIL then
   begin
      Anfang:= neuer_record;
      Ende:=neuer_record;
   end
   else
   begin
      neuer_record^.prev:=Ende;
      Ende^.next:=neuer_record;
      Ende:=neuer_record;
   end;
   Current:=neuer_record; Anzahl:=Anzahl + 1;
   // insert the Arc also to the Arc-Index list
   aIndex:=Abs(coverage_nr);
   if aIndex >= NumIndexList then
   begin
      SetLength(IndexList, aIndex+1);
      NumIndexList:=aIndex+1;
   end;
   IndexList[aIndex]:=Current;
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.Neu                                                *
********************************************************************************
* Prozedur erzeugt einen neuen Spalteneintrag f�r die Datenbank.               *
*                                                                              *
* PARAMETER: Bezeichnung -> Spaltenbezeichnung                                 *
*            Laenge      -> L�nge der Spalte                                   *
*            Typ         -> Typ der Spalte                                     *
*            Inhalt      -> Inhalt der Spalte                                  *
*            Art         -> Art der Spalte
*            Anfang      -> Zeiger auf Anfang der Liste in die eingef�gt wird  *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.Neu(Bezeichnung:string; Laenge:integer; Typ:integer; Inhalt:string; Art:string; var MyAnfang:p_datenbank_record; von:integer; bis:integer);
var neuer_record,Zeiger:p_datenbank_record;
begin
   new(neuer_record);
   neuer_record^.ColName:=Bezeichnung;
   neuer_record^.ColLength:=Laenge;
   neuer_record^.ColType:=Typ;
   neuer_record^.ColInfo:=Inhalt;
   neuer_record^.Art:=Art;
   neuer_record^.von:=von;
   neuer_record^.bis:=bis;
   if MyAnfang = NIL then
   begin
      MyAnfang:= neuer_record;
      neuer_record^.next:=NIL;
   end
   else
   begin
      Zeiger:=MyAnfang;
      repeat
         if Zeiger^.next <> nil then Zeiger:=Zeiger^.next;
      until Zeiger^.next = nil;
      Zeiger^.next:=neuer_record;
      Zeiger:=neuer_record;
      neuer_record^.next:=NIL;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.GetVonBis                                          *
********************************************************************************
* Prozedur dient zum Erhalten der Von und Bis Eintr�ge eines Datenbankeintrags *
* sowie des Datentyps f�r diese Spalte.                                        *
*                                                                              *
* PARAMETER:   Bezeichnung -> Name der Spalte                                  *
*              Art         -> Art der Spalte (POLY,LINE,POINT,ALL)             *
*              von         -> neuer von Wert f�r diese Spalte                  *
*              bis         -> neuer bis Wert f�r diese Spalte                  *
*              Spaltentyp  -> Spaltentyp dieser Spalte (C,N,B)                 *
*              DBAnfang    -> Zeiger auf den Anfang der Datenbankliste         *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.GetVonBis(Bezeichnung:string;Art:string;var von:integer;var bis:integer; var Spaltentyp:integer; DBAnfang:P_Datenbank_record);
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=DBAnfang; gefunden:=false;
   repeat
      if ((Zeiger^.Colname = Bezeichnung) and (Zeiger^.Art = Art)) or
         ((Zeiger^.Colname = Bezeichnung) and (Art = 'ALL')) then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then begin SpaltenTyp:=Zeiger^.ColType; von:=Zeiger^.von; bis:=Zeiger^.bis; end else begin von:=0; bis:=0; SpaltenTyp:=Col_String; end;
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.SetVonBis                                          *
********************************************************************************
* Prozedur dient zum �ndern der Bon und Bis Eintr�ge eines Datenbankeintrags.  *
*                                                                              *
* PARAMETER:   Bezeichnung -> Name der Spalte                                  *
*              Art         -> Art der Spalte (POLY,LINE,POINT,ALL)             *
*              von         -> neuer von Wert f�r diese Spalte                  *
*              bis         -> neuer bis Wert f�r diese Spalte                  *
*              DBAnfang    -> Zeiger auf den Anfang der Datenbankliste         *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.SetVonBis(Bezeichnung:string;Art:string;von:integer;bis:integer; DBAnfang:P_Datenbank_record);
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=DBAnfang; gefunden:=false;
   repeat
      if ((Zeiger^.ColName = Bezeichnung) and (Zeiger^.Art = Art)) or
         ((Zeiger^.ColName = Bezeichnung) and (Art = 'ALL')) then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then begin Zeiger^.von:=von; Zeiger^.bis:=bis; end;
end;

{*******************************************************************************
* PROZEDUR : ClassDatenbank.SetLength                                          *
********************************************************************************
* Prozedur dient dazu die erforderliche Spaltenbreite einer Spalte zu setzen.  *
*                                                                              *
* InPARAMETER: Bezeichnung -> Name der Spalte                                  *
*              Laenge      -> Breite der Spalte                                *
*                                                                              *
********************************************************************************}
procedure ClassDatenbank.SetLength(Bezeichnung:string; Laenge:integer;Art:string;DBAnfang:p_Datenbank_record);
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=DBAnfang; gefunden:=false;
   repeat
      if (Zeiger^.Colname = Bezeichnung) and (Zeiger^.Art = Art) then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if Zeiger <> nil then
      if Zeiger^.ColLength < Laenge then Zeiger^.ColLength:=Laenge;
end;

{*******************************************************************************
* FUNKTION : ClassDatenbank.GetLength                                          *
********************************************************************************
* Function is used to get length of column
*                                                                              *
* PARAMETER:   Bezeichnung -> Name der Spalte                                  *
*                                                                              *
* R�ckgabewert: Spaltenbreite f�r die entsprechende Spalte.                    *
*                                                                              *
********************************************************************************}
function ClassDatenbank.GetLength(Bezeichnung:string;Art:string;DBAnfang:P_Datenbank_record):integer;
var Zeiger:p_datenbank_record;
    gefunden:boolean;
begin
   Zeiger:=DBAnfang; gefunden:=false;
   repeat
      if ((Zeiger^.Colname = Bezeichnung) and (Zeiger^.Art = Art)) or
         ((Zeiger^.Colname = Bezeichnung) and (Art = 'ALL')) then gefunden:= true
      else Zeiger:=Zeiger^.next;
   until (gefunden = true) or (Zeiger = nil);
   if gefunden = true then Result:=Zeiger^.ColLength else Result:=0;
end;

{*******************************************************************************
* PROZEDUR : ClassPal.InsertPoint                                              *
********************************************************************************
* Prozedur f�gt einen Punkteintrag zum Current_pal ein.                        *
*                                                                              *
* InPARAMETER: X -> X-Koordinate des neuen Eckpunktes                          *
*              Y -> Y-Koordinate des neuen Eckpunktes                          *
*              Insel -> Gibt an, ob diese Koordianten zu einer Inselfl�che     *
*                       geh�ren.                                               *
********************************************************************************}
procedure ClassPal.InsertPoint(var Current_pal:p_pal_record;X:double;Y:double;Insel:boolean);
var new_Zeiger:p_Punkt_record;
begin
   new(new_Zeiger);
   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y; current_pal^.Punktanz:=current_pal^.Punktanz + 1;
   new_Zeiger^.Insel:=Insel;
   new_Zeiger^.next:=nil;
   new_Zeiger^.prev:=nil;
   if Current_pal^.FirstPoint = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current_pal^.FirstPoint:=New_Zeiger;
      Current_pal^.LastPoint:=New_Zeiger;
   end
   else
   begin
      new_Zeiger^.prev:=Current_pal^.LastPoint;
      Current_pal^.LastPoint^.next:=new_Zeiger;
      Current_pal^.LastPoint:=new_Zeiger;
   end;
end;

{*******************************************************************************
* PROZEDUR : ClassPal.InsertPolygon                                            *
********************************************************************************
* Prozedur f�gt einen Polygoneintrag zum current ein.                          *
*                                                                              *
* InPARAMETER: X -> X-Koordinate des neuen Eckpunktes                          *
*              Y -> Y-Koordinate des neuen Eckpunktes                          *
*                                                                              *
********************************************************************************}
procedure ClassPal.InsertPolygon(arc_number:longint;node_number:longint;polygon_number:longint);
var Zeiger:p_Polygon_record;
    new_Zeiger:p_Polygon_record;
begin
   new(new_Zeiger);
   new_zeiger^.arc_number:=arc_number;
   new_zeiger^.node_number:=node_number;
   new_zeiger^.polygon_number:=polygon_number;

   Zeiger:=Current^.Polygone;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current^.Polygone:=New_Zeiger;
      Current^.Polygone^.next:=NIL;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      while (Zeiger^.next <> nil) do Zeiger:=Zeiger^.next;
      Zeiger^.next:=new_Zeiger;
      new_Zeiger^.next:=nil;
   end;
end;

function ClassArc.FindArc(aIndex:integer):p_arc_record;
var retVal:p_arc_record;
begin
   retVal:=nil;
   if aIndex < NumIndexList then
      retVal:=IndexList[aIndex];
   result:=retVal;
end;

{*******************************************************************************
* PROZEDUR : ClassArc.GetArcs
********************************************************************************
* Prozedur liefert  die Koordinatenpaare des Arcs mit der nummer Index.        *
*                                                                              *
* InPARAMETER: Index -> Nummer des Arcs von dem die Koordinaten gebraucht werde*
*                                                                              *
********************************************************************************}
procedure ClassArc.GetArcs(var Current_pal:p_pal_record;Index:longint;ArcAnfang:p_arc_record);
var Zeiger:p_arc_record;
    Punkte:p_punkt_record;
    i:integer;
    AktAnzahl:longint;
    gefunden:boolean;
begin
   { Wenn der Index des gesuchten ARC�s 0 ist, dann geh�ren die folgenden Daten zu einer Inselfl�che }
   if Index = 0 then begin Insel:=true; exit; end;

   // find the arc
   Zeiger:=FindArc(ABS(Index));

   { Wenn das entsprechende ARC nicht gefunden wurde wird Funktion verlassen }
   if Zeiger = nil then exit;

   { Wenn es sich bei diesem ARC um eine bereits verarbeitete Inselfl�che handelt, so wird Funktion verlassen }
   if (Zeiger^.Insel = true) and (Insel = false) then exit;

   { Wenn es sich bei diesem ARC um eine Inselfl�che handelt, so soll Sie es als solches gekennzeichnet werden }
   if Insel = true then Zeiger^.Insel:=true;

   Punkte:=Zeiger^.FirstPoint;
   { Wenn zum ARC keine Punkte geh�ren wird Funktion verlassen }
   if Punkte = nil then exit;

   { Nun muss die Richtung in der die Koordinaten gebraucht werden �berpr�ft werden }
   if Index < 0 then { Das ARC muss von hinten nach vorn durchgelaufen werden }
   begin
      Punkte:=Zeiger^.LastPoint;
      while (Punkte <> nil) do
      begin
         if Punkte <> nil then PalObj.InsertPoint(Current_Pal,Punkte^.X,Punkte^.Y,Insel);
         Punkte:=Punkte^.prev;
         Insel:=false;
      end;
   end
   else
   begin { Das ARC kann von vorne nach hinten durchlaufen werden }
      for i:=1 to Zeiger^.number_of_cor do
      begin
         if Punkte <> nil then PalObj.InsertPoint(Current_Pal,Punkte^.X,Punkte^.Y,Insel);
         if Punkte <> nil then Punkte:=Punkte^.next;
         Insel:=false;
      end;
   end;
end;


{*******************************************************************************
* PROZEDUR : ClassArc.InsertPoint                                              *
********************************************************************************
* Prozedur f�gt ein Punktpaar in die Punktliste des Current     ein.           *
*                                                                              *
* InPARAMETER: X -> X-Koordinate des neuen Eckpunktes                          *
*              Y -> Y-Koordinate des neuen Eckpunktes                          *
*                                                                              *
********************************************************************************}
procedure ClassArc.InsertPoint(X:double;Y:double;DBAnfang:p_Datenbank_record);
var Zeiger:p_Punkt_record;
    new_Zeiger:p_Punkt_record;
    hstr      :string;
begin
   new(new_Zeiger);
   new_Zeiger^.X:=X; new_Zeiger^.Y:=Y;
   new_Zeiger^.Insel:=FALSE;
   new_Zeiger^.next:=nil;
   new_Zeiger^.prev:=nil;
   Zeiger:=Current^.FirstPoint;
   if Zeiger = nil then
   begin
      { Das neue Punktpaar kommt an den Anfang der Punktliste }
      Current^.FirstPoint:=new_Zeiger;
      Current^.LastPoint:=new_Zeiger;
   end
   else
   begin
      { Das neue Punktpaar kommt ans Ende der Punktliste }
      new_Zeiger^.prev:=Current^.LastPoint;
      Current^.LastPoint^.next:=new_Zeiger;
      Current^.LastPoint:=new_Zeiger;
   end;
end;

procedure ClassConvert.ResetObjektnr;
begin
   Objektnummer:=0; Insel:=false;
end;

{*******************************************************************************
* PROZEDUR : ClassConvert.CreatePolygon                                        *
********************************************************************************
* Prozedur sucht die ARC-Eintr�ge zum Current Eintrag.                         *
*                                                                              *
* PARAMETER: Current -> Zeiger auf den PAL-Eintrag, der verarbeitet werden soll*
*            ARCAnfang -> Zeiger auf den Anfang der ARC-Liste                  *
*                                                                              *
********************************************************************************}
procedure ClassConvert.CreatePolygon(Current:p_pal_record;ARCAnfang:p_arc_record);
var AktPolygon:p_polygon_record;
begin
   { Nun m�ssen die ARC-Eintr�ge zum aktuellen Polygon gesucht werden }
   AktPolygon:=Current^.Polygone;
   if AktPolygon <> nil then
   repeat
      { Im AktPolygon^.arc_number steht die Nummer des ARC�s der zu diesem Polygon geh�rt }
      ArcObj.GetArcs(Current,AktPolygon^.arc_number,ARCAnfang);
      AktPolygon:=AktPolygon^.next;
   until AktPolygon = nil;
end;

end.
