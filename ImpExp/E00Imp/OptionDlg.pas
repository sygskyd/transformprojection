unit OptionDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TOptionWnd = class(TForm)
    Status: TPanel;
    Label1: TLabel;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    GPolyCB: TCheckBox;
    Label4: TLabel;
    GLineCB: TCheckBox;
    Label5: TLabel;
    GPointCB: TCheckBox;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    APolyCB: TCheckBox;
    ALineCB: TCheckBox;
    APointCB: TCheckBox;
    GSymbolCB: TCheckBox;
    Label8: TLabel;
    Label9: TLabel;
    SymNameCB: TComboBox;
    OK: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GPolyCBClick(Sender: TObject);
    procedure GLineCBClick(Sender: TObject);
    procedure GPointCBClick(Sender: TObject);
    procedure GSymbolCBClick(Sender: TObject);
    procedure SymnrEditExit(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private-Deklarationen }
    ConvertPolygonsGraphic:boolean;
    ConvertLinesGraphic:boolean;
    ConvertPointsGraphic:boolean;
    ConvertPolygonsAttribs:boolean;
    ConvertLinesAttribs:boolean;
    ConvertPointsAttribs:boolean;
    SymName:string;

    Polyfilename:string;
    Linefilename:string;
    Pointfilename:string;
  public
    { Public-Deklarationen }
    function DoConvertPolygonsGraphic:boolean;
    function DoConvertPolygonsAttribs:boolean;
    function DoConvertLinesGraphic:boolean;
    function DoConvertLinesAttribs:boolean;
    function DoConvertPointsGraphic:boolean;
    function DoConvertPointsAttribs:boolean;
    function DoConvertSymbolGraphic:boolean;

    function GetPolyfilename:string;
    function GetLinefilename:string;
    function GetPointfilename:string;
    function GetSymName:string;
    procedure FillSymNameCB;
    procedure SetScreenSettings;
  end;

var
  OptionWnd: TOptionWnd;

implementation

{$R *.DFM}

uses StrFile, Maindlg;

var StrFileObj:ClassStrfile;

procedure TOptionWnd.SetScreenSettings;
begin
   Self.Caption:=StrfileObj.Assign(38); { Optionen f�r die Konvertierung }

   Label1.Caption:=StrfileObj.Assign(39); { Bitte w�hlen Sie die zu konvertierenden Objekte }
   Label2.Caption:=StrfileObj.Assign(40); { Grafik }
   Label3.Caption:=StrfileObj.Assign(41); { Polygone: }
   Label4.Caption:=StrfileObj.Assign(42); { Linien: }
   Label5.Caption:=StrfileObj.Assign(43); { Punkte: }
   Label6.Caption:=StrfileObj.Assign(44); { Attribute }
   Label8.Caption:=StrfileObj.Assign(45); { als Symbol }
   Ok.Caption:=StrfileObj.Assign(4);   { Ok }
end;

procedure TOptionWnd.FormCreate(Sender: TObject);
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
end;

function TOptionWnd.DoConvertPolygonsGraphic:boolean;
begin
   Result:=ConvertPolygonsGraphic;
end;

function TOptionWnd.DoConvertSymbolGraphic:boolean;
begin
   Result:=GSymbolCB.checked;
end;

function TOptionWnd.DoConvertPolygonsAttribs:boolean;
begin
   Result:=ConvertPolygonsAttribs;
end;

function TOptionWnd.DoConvertLinesGraphic:boolean;
begin
   Result:=ConvertLinesGraphic;
end;

function TOptionWnd.DoConvertLinesAttribs:boolean;
begin
   Result:=ConvertLinesAttribs;
end;

function TOptionWnd.DoConvertPointsGraphic:boolean;
begin
   Result:=ConvertPointsGraphic;
end;

function TOptionWnd.DoConvertPointsAttribs:boolean;
begin
   Result:=ConvertPointsAttribs;
end;

function TOptionWnd.GetPolyFilename:string;
begin
   Result:=PolyFilename;
end;

function TOptionWnd.GetSymName:string;
begin
   Result:=SymName;
end;

function TOptionWnd.GetLineFilename:string;
begin
   Result:=LineFilename;
end;

function TOptionWnd.GetPointFilename:string;
begin
   Result:=PointFilename;
end;

procedure TOptionWnd.FormActivate(Sender: TObject);
var hstr:string;
    hpchar:pchar;
begin
   GLineCB.checked:=false; GPolyCB.checked:=false; GPointCB.checked:=false;
   GLineCB.Enabled:=true; GPolyCB.Enabled:=true; GPointCB.Enabled:=true;
   ALineCB.checked:=false; APolyCB.checked:=false; APointCB.checked:=false;
   ALineCB.Enabled:=true; APolyCB.Enabled:=true; APointCB.Enabled:=true;

   GSymbolCB.checked:=false; GSymbolCB.enabled:=true; SymNameCB.Enabled:=true;

   { Ermitteln der Dateinamen }
   hpchar:=stralloc(length(PolyFilename)+1);
   strpcopy(hpchar,PolyFilename); hpchar:=strupper(hpchar); PolyFilename:=strpas(hpchar); strdispose(hpchar);
   hstr:=copy(PolyFilename,1,Pos('.ASC',PolyFilename)-1);

   PolyFilename:=hstr;PolyFilename:=PolyFilename + '_POLY.ASC';
   LineFilename:=hstr;LineFilename:=LineFilename + '_LINE.ASC';
   PointFilename:=hstr; PointFilename:=PointFilename + '_POINT.ASC';


   { Damit die Maskeineinstellungen auch korrekt durchgef�hrt werden k�nnen muss zuwert
     der Status geholt werden }

   Status.Caption:=StrfileObj.Assign(28) + ' ' + ExtractFilename(Changefileext(MainWnd.GetE00Filename,'.E00')); { Datei, die aktuell bearbeitet wird: }
   MainWnd.GetConvState(ConvertPolygonsGraphic,ConvertPolygonsAttribs,
                        ConvertLinesGraphic,ConvertLinesAttribs,
                        ConvertPointsGraphic,ConvertPointsAttribs);

   if ConvertPolygonsGraphic = true then { Es sind Polygone vorhanden }
      begin GPolyCB.checked:=true; GPolyCB.Enabled:=true; APolyCB.enabled:=true; end
   else
      begin GPolyCB.checked:=false; GPolyCB.Enabled:=false; APolyCB.checked:=false; APolyCB.enabled:=false; end;

   if ConvertLinesGraphic = true then { Es sind Linien vorhanden }
      begin GLineCB.checked:=true; GLineCB.Enabled:=true; ALineCB.enabled:=true; end
   else
      begin GLineCB.checked:=false; GLineCB.Enabled:=false; ALineCB.enabled:=false; GLineCB.enabled:=false; end;

   if ConvertPointsGraphic = true then { Es sind Punkte vorhanden }
      begin GPointCB.checked:=true; GPointCB.enabled:=true; APointCB.enabled:=true; end
   else
      begin GSymbolCB.checked:=false; GSymbolCB.enabled:=false; GPointCB.checked:=false; GPointCB.enabled:=false; GPointCB.enabled:=false; APointCB.enabled:=false; SymNameCB.Enabled:=false; end;

   if ConvertPolygonsAttribs = true then APolyCB.checked:=true else begin APolyCB.checked:=false; APolyCB.enabled:=false; end;

   if ConvertLinesAttribs = true then ALineCB.checked:=true else begin ALineCB.checked:=false; ALineCB.enabled:=false; end;

   if ConvertPointsAttribs = true then APointCB.checked:=true else begin APointCB.checked:=false; APointCB.enabled:=false; end;

   GSymbolCB.checked:=false; SymNameCB.enabled:=false;
end;

procedure TOptionWnd.GPolyCBClick(Sender: TObject);
begin
   if not GPolyCB.Checked = true then { Die Graphischen Daten von den Polygonen sollen nicht �bernommen werden }
   begin
      APolyCB.checked:=false; APolyCB.enabled:=false;
   end
   else
   begin
      if ConvertPolygonsAttribs = true then APolyCB.enabled:=true else begin APolyCB.checked:=false; APolyCB.enabled:=false; end;
   end;
end;

procedure TOptionWnd.GLineCBClick(Sender: TObject);
begin
   if not GLineCB.Checked = true then { Die Graphischen Daten von den Polygonen sollen nicht �bernommen werden }
   begin
      ALineCB.checked:=false; ALineCB.enabled:=false;
   end
   else
   begin
      if ConvertLinesAttribs = true then ALineCB.enabled:=true else begin ALineCB.checked:=false; ALineCB.enabled:=false; end;
   end;
end;

procedure TOptionWnd.GPointCBClick(Sender: TObject);
begin
   if not GPointCB.Checked = true then { Die Graphischen Daten von den Punkten sollen nicht �bernommen werden }
   begin
      APointCB.checked:=false; APointCB.enabled:=false;
      GSymbolCB.enabled:=false; GSymbolCB.checked:=false;
   end
   else
   begin
      if ConvertPointsAttribs = true then APointCB.enabled:=true else begin APointCB.checked:=false; APointCB.enabled:=false; end;
      GSymbolCB.enabled:=true;
      if MainWnd.DestDocument.Symbols.Count = 0 then GSymbolCB.enabled:=false;
   end;
end;

procedure TOptionWnd.GSymbolCBClick(Sender: TObject);
begin
   if GSymbolCB.checked = true then SymNameCB.enabled:=true else SymNameCB.enabled:=false;
end;

procedure TOptionWnd.SymnrEditExit(Sender: TObject);
begin
   { Kontrolle, ob die Symbolnummer auch Korrekt ist }
   SymName:=SymNameCB.Text;
end;

procedure TOptionWnd.FillSymNameCB;
var i:integer;
    first:boolean;
    ASymbolDef:Variant;
begin
   SymNameCB.Items.Clear;
   SymNameCB.Text:='';
   first:=true;

   for i:=0 to MainWnd.DestDocument.Symbols.Count-1 do
   begin
      ASymboldef:=MainWnd.DestDocument.Symbols.Items[i];
      SymName:=ASymboldef.Name;
      SymNameCB.Items.Add(SymName);
      if first then
      begin
         SymNameCB.Text:=SymName;
         first:=false;
      end;
   end;
end;

procedure TOptionWnd.OKClick(Sender: TObject);
begin
   ConvertPolygonsGraphic:=GPolyCB.checked;
   ConvertLinesGraphic:=GLineCB.checked;
   ConvertPointsGraphic:=GPointCB.checked;

   ConvertPolygonsAttribs:=APolyCB.checked;
   ConvertLinesAttribs:=ALineCB.checked;
   ConvertPointsAttribs:=APointCB.checked;
   OptionWnd.close;
end;

end.
