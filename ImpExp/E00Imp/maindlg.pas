unit Maindlg;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Strfile, StdCtrls, Buttons, ExtCtrls, Gauges, Files, Db,
  DBTables,OptionDlg,SetE00FileDlg, Windows, OleCtrls,
  AXImpExpDbcXControl_TLB, AxGisPro_TLB, AXGisMan_TLB, AXDLL_TLB, WinOSInfo;

const
      //registration modes
      NO_LICENCE        = 0;
      FULL_LICENCE      = 1;
      DEMO_LICENCE      = 2;


type
  TMainWnd = class(TForm)
    Label1: TLabel;
    CloseTimer: TTimer;
    SaveDialogRep: TSaveDialog;
    Panel1: TPanel;
    Label2: TLabel;
    Prozessbalken: TGauge;
    StatusLB: TListBox;
    SaveReportBtn: TButton;
    OK: TButton;
    Cancel: TButton;
    SetupWindowTimer: TTimer;
    AXImpExpDbc: TAXImpExpDbc;
    AxGisProjection: TAxGisProjection;
    AXGisObjMan: TAXGisObjMan;
    procedure FormCreate(Sender: TObject);
    procedure CloseTimerTimer(Sender: TObject);
    procedure CancelClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure SaveReportBtnClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AXImpExpDbcDialogSetuped(Sender: TObject);
    procedure AxGisProjectionWarning(Sender: TObject;
      const Info: WideString);
    procedure AxGisProjectionError(Sender: TObject;
      const Info: WideString);
    procedure AXImpExpDbcRequestImpLayerIndex(Sender: TObject;
      const Layername: WideString; var LayerIndex: Integer);
    procedure AXImpExpDbcRequestImpProjectName(Sender: TObject;
      var ProjectName: WideString);
    procedure AXImpExpDbcRequestImpProjectGUIDs(Sender: TObject;
      var StartGUID, CurrentGUID: WideString);
    procedure AXImpExpDbcWarning(Sender: TObject; const Info: WideString);
  private
    { Private-Deklarationen }
    StrFileObj :ClassStrfile;
    FileObj    :ClassFile;
    Pass       :integer;
    FirstActivate:boolean;
    ObjectCount:integer;
    E00Dateiname:string;
    Bytes2work  :longint;
    Bytesworked :longint;
    Abort       :boolean;
    oldFilename :string;
    ProcessActive:boolean;

    aLayer    :ILayer;
    APolyline :IPolyline;
    APolygon  :IPolygon;

    procedure RunPass(AktPass:integer);
    procedure ExecutePass;
    procedure ConcatFiles(var Filename:string);
    procedure DisplayDemoWarning;
    procedure StartImport;
  public
    { Public-Deklarationen }
    App         :Variant;
    DestDocument:IDocument;

    RegMode     :integer;
    LngCode     :string;
    ExitNormal  :boolean;
    GisHandle   :integer;

    ConcatFlag:boolean;
    CurrLayername:string;

    procedure ShowProcent(Wert:longint);
    function GetE00Filename:string;
    procedure GetConvState(var GPoly:boolean; var APoly:boolean; var GLine:boolean; var ALine:boolean; var GPoint:boolean; var APoint:boolean);

    { Procedures that are used to handle AX-Control }
    procedure CreateLayer(Layer:string);
    function InsertPoint(X:double;Y:double;var HasDuplicate:boolean):integer;
    function  CheckLayerExists(Layer:string):boolean;
    procedure SetCurrentLayer(Layer:string);

    procedure CreatePolyline;
    procedure InsertPointToPolyLine(X:double; Y:double);
    function ClosePolyline(var HasDuplicate:boolean):integer;

    procedure CreatePolygon;
    procedure InsertPointToPolygon(X:double; Y:double);
    function ClosePolygon(var HasDuplicate:boolean):integer;

    function CreateSymbol(X:double;Y:double;Symname:string;var HasDuplicate:boolean):integer;

    procedure SetProjectionSettings;
    procedure CheckProjectionSettings;
    procedure CreateStringFile;
  end;

var
  MainWnd: TMainWnd;

implementation

uses comobj;

{$R *.DFM}

procedure TMainWnd.CreateStringFile;
begin
   StrFileObj:=ClassStrfile.Create;
   StrFileObj.CreateList(OSInfo.WingisDir,LngCode,'E00IMP');
   Self.Caption:=StrFileObj.Assign(1);          { ARCINFO E00 Import 4.0 }
   Label2.Caption:=StrFileObj.Assign(2);        { Processmessages: }
   SaveReportBtn.Caption:=StrFileObj.Assign(3); { save report }
   Ok.Caption:=StrFileObj.Assign(4);            { Ok }
   Cancel.Caption:=StrFileObj.Assign(5);        { Cancel }
   Self.Update;

   // set screen settings
   OptionWnd.SetScreenSettings;
end;

procedure TMainWnd.FormCreate(Sender: TObject);
begin
   OptionWnd:=TOptionWnd.Create(Self);
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];

   Self.Height:=1;
   Self.Width:=1;

   FirstActivate:=true;
   ObjectCount:=0;

   ConcatFlag:=FALSE;
   ProcessActive:=FALSE;

   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

procedure TMainWnd.CheckProjectionSettings;
var SourceProjection:integer;
    hstr:string;
    different:boolean;
begin
   different:=false;
   // function is used to check if the settings in the
   if (AxGisProjection.TargetCoordinateSystem <> DestDocument.Projection) then
      different:=TRUE;

   if (AxGisProjection.TargetXOffset <> DestDocument.ProjectionXOffset) then
      different:=TRUE;

   if (AxGisProjection.TargetYOffset <> DestDocument.ProjectionYOffset) then
      different:=TRUE;

   if (AxGisProjection.TargetScale <> DestDocument.ProjectionScale) then
      different:=TRUE;

   // check if the projections are different
   if DestDocument.ProjectProjSettings <> AxGisProjection.TargetProjSettings then
      different:=TRUE;

   if DestDocument.ProjectProjection <> AxGisProjection.TargetProjection then
      different:=TRUE;

   if DestDocument.Projectdate <> AxGisProjection.TargetDate then
      different:=TRUE;

   if different then
   begin
      Showmessage(StrfileObj.Assign(55)); // Warning: Projection does not fit to project projection
      AxGisProjection.SetupByDialog;
   end;
   // the used output coordinate-system is equal to the
   // target-coordinate-system
   AxGisProjection.UsedInputCoordSystem:=AxGisProjection.SourceCoordinateSystem;
end;

procedure TMainWnd.SetProjectionSettings;
var aApp:OleVariant;
    aDoc:OleVariant;
begin
   // procedure is used to set the projection settings
   // of the current project to the GeoWnd
   AxGisProjection.WorkingPath:=OSInfo.WingisDir;
   AxGisProjection.LngCode:=LngCode;
   AxImpExpDbc.WorkingDir:=OSInfo.WingisDir;
   AxImpExpDbc.LngCode:=LngCode;
   AxImpExpDbc.ShowMode:=2; // Shw_Target
   AXGisObjMan.WorkingDir:=OSInfo.WingisDir;
   AXGisObjMan.LngCode:=LngCode;
   aApp:=App; AxGisObjMan.SetApp(aApp);
   aDoc:=DestDocument; AxGisObjMan.SetDocument(aDoc);

   SetE00fileWnd.DestDbLabel.Caption:=AxImpExpDbc.ImpDbInfo;
   AxGisProjection.SourceCoordinateSystem:=cs_Carthesic;   // ARCINFO data normaly is carthesic
   AxGisProjection.TargetCoordinateSystem:=DestDocument.Projection;
   AxGisProjection.SourceProjSettings:=DestDocument.ProjectProjSettings;
   AxGisProjection.TargetProjSettings:=DestDocument.ProjectProjSettings;
   AxGisProjection.SourceProjection:=DestDocument.ProjectProjection;
   AxGisProjection.TargetProjection:=DestDocument.ProjectProjection;
   AxGisProjection.SourceDate:=DestDocument.Projectdate;
   AxGisProjection.TargetDate:=DestDocument.Projectdate;
   AxGisProjection.SourceXOffset:=DestDocument.ProjectionXOffset;
   AxGisProjection.TargetXOffset:=DestDocument.ProjectionXOffset;
   AxGisProjection.SourceYOffset:=DestDocument.ProjectionYOffset;
   AxGisProjection.TargetYOffset:=DestDocument.ProjectionYOffset;
   AxGisProjection.SourceScale:=DestDocument.ProjectionScale;
   AxGisProjection.TargetScale:=DestDocument.ProjectionScale;
   // if source and target is .amp - file
   // the used input coordinates and used output coordinates
   // for the projection calculation are in carthesic
   AxGisProjection.UsedInputCoordSystem:=AxGisProjection.SourceCoordinateSystem;
   AxGisProjection.UsedOutputCoordSystem:=cs_Carthesic;
end;

function TMainWnd.GetE00Filename:string;
begin
   Result:=E00Dateiname;
end;

procedure TMainWnd.GetConvState(var GPoly:boolean; var APoly:boolean; var GLine:boolean; var ALine:boolean; var GPoint:boolean; var APoint:boolean);
begin
   if FileObj.CheckPalexistsG = true then GPoly:=true else GPoly:=false;
   if FileObj.CheckPalexistsA = true then APoly:=true else APoly:=false;

   if FileObj.CheckArcexistsG = true then GLine:=true else GLine:=false;
   if FileObj.CheckArcexistsA = true then ALine:=true else ALine:=false;

   if FileObj.CheckLabexistsG = true then GPoint:=true else GPoint:=false;
   if FileObj.CheckLabexistsA = true then APoint:=true else APoint:=false;
end;

{*******************************************************************************
* PROZEDUR : RunPass                                                           *
********************************************************************************
* Prozedur leitet den jeweiligen Konvertierungs-Pass ein.                      *
*                                                                              *
* PARAMETER: Pass -> Gibt den Konvertierungs-Pass an.                          *
*                                                                              *
********************************************************************************}
procedure TMainWnd.RunPass(AktPass:integer);
var Ende:boolean;
    Layer:string;
begin
   Layer:=ExtractFilename(E00Dateiname);
   Layer:=copy(Layer,1,pos('.',Layer)-1);
   if AktPass = 1 then { Kontrolle, ob die Datei ins Dos-Format konvertiert werden muss }
   begin
      ProcessActive:=TRUE;
      if not SetE00FileWnd.GetFilename(E00Dateiname) then
      begin
         StatusLb.Items.Add(StrfileObj.Assign(36)); // file correct imported
         StatusLb.Items.Add('--------------------------------------------');
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         Abort:=true;
         ProcessActive:=FALSE;
         Cancel.Enabled:=false;
         Ok.Enabled:=true;
         SaveReportBtn.Enabled:=true;
         exit;
      end;
      FileObj:=ClassFile.Create;
      StatusLb.Items.Add(StrfileObj.Assign(28) + Extractfilename(E00Dateiname));
      // display database information
      StatusLb.Items.Add(AxImpExpDbc.ImpDbInfo);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      CheckProjectionSettings;

      oldFilename:=E00Dateiname;
      { It must be checked if the E00 file contains of one than more file }
      ConcatFiles(E00Dateiname);

      Bytes2Work:=FileObj.GetMaxPass(E00Dateiname); Bytesworked:=0;
      if FileObj.Check_odoa(E00Dateiname) then
      begin
         FileObj.OpenFile(E00Dateiname);

         StatusLb.Items.Add(StrfileObj.Assign(29));
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         Pass:=1;
         ExecutePass;
         exit;
      end
      else
         begin RunPass(2); exit; end;
   end;

   if AktPass = 2 then { �ffnen der Dateien und starten des Einlesens  }
   begin
      { Setzen der Maskeneinstellungen }
      StatusLb.Items.Add(StrfileObj.Assign(30)); // reading E00 data
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      { Die Anzahl der zu bearbeitenden Bytes wird ermittelt }
      Bytes2Work:=FileObj.GetMaxPass(E00Dateiname); Bytesworked:=0;
      { Die Dateien werden ge�ffnet }
      FileObj.OpenE00File(E00Dateiname);
      Pass:=2;
      ExecutePass; exit;
   end;

   if AktPass = 3 then { Erzeugen der Polygone aus dem E00-File  }
   begin
      if SetE00Filewnd.SelectivCB.Checked = true then
      begin
         OptionWnd.FillSymNameCB;
         OptionWnd.ShowModal;
         if OptionWnd.DoConvertPolygonsGraphic = true then // polygons should be converted
         begin
            Bytes2work:=FileObj.GetMaxPalElements; Bytesworked:=0;
            StatusLb.Items.Add(StrfileObj.Assign(32)); // creating PAL objects
            StatusLB.ItemIndex := StatusLB.Items.Count-1;
            StatusLB.Update;

            if Bytes2work > 0 then Pass:=3 else begin Runpass(5); exit; end;
            ExecutePass; exit;
         end
         else
         begin
            Runpass(5); exit;
         end
      end
      else   // if convertation is not selective PAL objects will be created
      begin
         { Setzen der Maskeneinstellungen  }
         Bytes2work:=FileObj.GetMaxPalElements; Bytesworked:=0;

         if Bytes2work > 0 then
         begin
            Pass:=3;
            StatusLb.Items.Add(StrfileObj.Assign(32)); // creating PAL objects
            StatusLB.ItemIndex := StatusLB.Items.Count-1;
            StatusLB.Update;
         end
         else
         begin
            Runpass(5); exit;
         end;
         ExecutePass; exit;
      end;
   end;

   if AktPass = 4 then { Importieren der PAL-Elemente ins GIS }
   begin
      { Wenn Selektiv konvertiert wird und keine Polygone konvertiert werden sollen }
      if ((SetE00Filewnd.SelectivCB.Checked = true) and (OptionWnd.DoConvertPolygonsGraphic = false)) or (FileObj.CheckpalexistsG = false) then
      begin
         Runpass(5);
         exit;
      end;

      if SetE00Filewnd.SelectivCB.Checked = false then { Da nicht selektiv konvertiert wird muss eine Datenbank f�r alle Objekte erzeugt werden }
      begin
         FileObj.CreateDatabase(Layer,'ALL',Layer);
      end
      else // if it will be converted selective an extra database for polygons has to be created
      begin
         if OptionWnd.DoConvertPolygonsAttribs = true then { Wenn Attribute f�r Polygone geschrieben werden sollen }
         begin
            FileObj.CreateDatabase(Layer+'_Poly','POLY',Layer);
         end;
      end;
      { Wenn nicht selektiv konvertiert wird und Polygone vorhanden sind, oder selektiv konvertiert wird und Polygone ausgewew�hlt sind }
      if ((FileObj.CheckPalExistsG) and (SetE00Filewnd.SelectivCB.Checked = false)) or
         ((SetE00Filewnd.SelectivCB.Checked = true) and (OptionWnd.DoConvertPolygonsGraphic = true)) then
      begin
         Bytes2Work:=FileObj.GetMaxPalElements; Bytesworked:=0;

         StatusLb.Items.Add(StrfileObj.Assign(33)); // importing PALs into GIS
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;

         Pass:=4;
         ExecutePass; exit;
      end
      else
      begin
         Runpass(5); { Wenn keine PAL Elemente existieren wird weiter zum n�chsten Pass gegangen }
         exit;
      end;
      exit;
   end;

   if AktPass = 5 then { Schreiben der ARC Elemente }
   begin
      if ((SetE00Filewnd.SelectivCB.Checked = true) and (OptionWnd.DoConvertLinesGraphic = false)) or (FileObj.CheckarcexistsG = false) then
      begin
         Runpass(6);
         exit;
      end;

      { Wenn Selektiv konvertiert wird muss zuerst das ASC-File f�r die Linienelemente �berpr�ft werden }
      if (SetE00Filewnd.SelectivCB.Checked = true) and (OptionWnd.DoConvertLinesGraphic = true) then
      begin
         { Wenn vorher bereits Polygone konvertiert wurden muss die Datei geschlossen werden }
         if OptionWnd.DoConvertPolygonsGraphic = true then
         begin
            FileObj.ResetObjektnr;
         end;

         if OptionWnd.DoConvertLinesAttribs = true then
         begin
            FileObj.CreateDatabase(Layer+'_Line','LINE',Layer);
         end;
      end;

      // if no database has been created yet
      if (SetE00Filewnd.SelectivCB.Checked = false) and (FileObj.CheckPalexistsG = false) and (FileObj.CheckArcexistsG = true) and (FileObj.CheckArcExistsG = true) then
      begin
         FileObj.CreateDatabase(Layer,'ALL',Layer);
      end;

      Bytes2Work:=FileObj.GetMaxArcElements; Bytesworked:=0;

      StatusLb.Items.Add(StrfileObj.Assign(34)); // importing ARCs into GIS
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      Pass:=5;
      ExecutePass; exit;
   end;

   if AktPass = 6 then { Schreiben der LAB Elemente }
   begin
      if ((SetE00Filewnd.SelectivCB.Checked = true) and (OptionWnd.DoConvertPointsGraphic = false)) or (FileObj.ChecklabexistsG = false) then
      begin
         Runpass(7);
         exit;
      end;

      { Wenn Selektiv konvertiert wird muss zuerst das ASC-File f�r die Linienelemente �berpr�ft werden }
      if (SetE00Filewnd.SelectivCB.Checked = true) and (OptionWnd.DoConvertPointsGraphic = true) then
      begin
         { Wenn vorher bereits Polygone oder Linien konvertiert wurden muss die Datei geschlossen werden }
         if (OptionWnd.DoConvertPolygonsGraphic = true) or (OptionWnd.DoConvertLinesGraphic = true) then
         begin
            FileObj.ResetObjektnr;
         end;

         if OptionWnd.DoConvertPointsAttribs = true then
         begin
            FileObj.CreateDatabase(Layer+'_Point','POINT',Layer);
         end;
      end;

      { Wenn nicht selektiv konvertiert wurde und vorher noch keine ASC-Datei erzeugt wurde }
      if (SetE00Filewnd.SelectivCB.Checked = false) and (FileObj.CheckPalexistsG = false) and (FileObj.CheckArcexistsG = false) and (FileObj.CheckLabexistsG = true) then
      begin
         FileObj.CreateDatabase(Layer,'ALL',Layer);
      end;

      Bytes2Work:=FileObj.GetMaxLabElements; Bytesworked:=0;

      StatusLb.Items.Add(StrfileObj.Assign(35)); // importing LABs into GIS
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      Pass:=6;
      ExecutePass; exit;
   end;

   if AktPass = 7 then { Ende der Konvertierung }
   begin
      FileObj.CloseallFiles;
      FileObj.Destroy;
      RunPass(1); // go to next file
      exit;
   end;
end;

procedure TMainWnd.ShowProcent(Wert:longint);
var Prozent:integer;
begin
   if Bytes2work > 0 then Prozent:= Round (((Bytesworked + Wert) / Bytes2work) * 100 ) else Prozent:= 100;
   ProzessBalken.Progress :=Prozent;
   ProzessBalken.Update;
end;

{*******************************************************************************
* PROZEDUR : ExecutePass                                                       *
********************************************************************************
*                                                                              *
*                                                                              *
********************************************************************************}
procedure TMainWnd.ExecutePass;
var Prozent:integer;
begin
   if Pass = 1 then { Konvertieren der E00-Datei aus dem Dos-Format in das Unix-Format }
   begin
      repeat
         Bytesworked:=Bytesworked + FileObj.To_odoa;
         { Anzeigen des Prozessstatus }
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then
      begin
         FileObj.Closefiles;
         E00Dateiname:=Changefileext(E00Dateiname,'.tmp');
         RunPass(2);
         exit;
      end;
      if Abort = true then begin FileObj.Closefiles; exit; end;
   end;

   if Pass = 2 then { Einlesen der Objektdaten aus dem E00-File }
   begin
      repeat
         if SetE00Filewnd.SelectivCB.Checked = false then Bytesworked:=Bytesworked + FileObj.ReadElement('ALL')
         else Bytesworked:=Bytesworked + FileObj.ReadElement('');
         { Anzeigen des Prozessstatus }
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then RunPass(3);
      if Abort = true then exit;
      exit;
   end;

   if Pass = 3 then { Erzeugen der PalObjekte aus dem E00-File }
   begin
      repeat
         Bytesworked:=Bytesworked + FileObj.CreateElement;
         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then RunPass(4);
      if Abort = true then exit;
      exit;
   end;

   if Pass = 4 then { Schreiben der Pal Elemente in das ASC-File }
   begin
      repeat
         if SetE00Filewnd.SelectivCB.Checked = true then
         begin
            if (OptionWnd.DoConvertPolygonsAttribs = true) then
               Bytesworked:=Bytesworked + FileObj.WritePalElement('POLY') { Die Datenbankinformation f�r Polygone wird geschrieben }
            else
               Bytesworked:=Bytesworked + FileObj.WritePalElement('NO') { Es wird keine Datenbankinformation geschrieben }
         end
         else
            Bytesworked:=Bytesworked + FileObj.WritePalElement('ALL');

         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then RunPass(5);
      if Abort = true then exit;
      exit;
   end;

   if Pass = 5 then { Schreiben der Arc Elemente in das ASC-File }
   begin
      repeat
         if SetE00Filewnd.SelectivCB.Checked = true then
         begin
            if (OptionWnd.DoConvertLinesAttribs = true) then
               Bytesworked:=Bytesworked + FileObj.WriteArcElement('LINE') { Die Datenbankinformation f�r Polygone wird geschrieben }
            else
               Bytesworked:=Bytesworked + FileObj.WriteArcElement('NO') { Es wird keine Datenbankinformation geschrieben }
         end
         else
            Bytesworked:=Bytesworked + FileObj.WriteArcElement('ALL');

         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then RunPass(6);
      if Abort = true then exit;
      exit;
   end;

   if Pass = 6 then { Schreiben der Lab Elemente in das ASC-File }
   begin
      repeat
         if SetE00Filewnd.SelectivCB.Checked = true then
         begin
            if (OptionWnd.DoConvertPointsAttribs = true) then
               Bytesworked:=Bytesworked + FileObj.WriteLabElement('POINT') { Die Datenbankinformation f�r Punkte wird geschrieben }
            else
               Bytesworked:=Bytesworked + FileObj.WriteLabElement('NO') { Es wird keine Datenbankinformation geschrieben }
         end
         else
            Bytesworked:=Bytesworked + FileObj.WriteLabElement('ALL');

         if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
         ProzessBalken.Progress :=Prozent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true);
      if (Bytesworked >= Bytes2work) and (Abort = false) then RunPass(7);
      if Abort = true then exit;
   end;
end;

procedure TMainWnd.DisplayDemoWarning;
begin
   StatusLB.Items.Add(StrfileObj.Assign(48)); { 100 Objects created - following objects will be skiped. }
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;
   // also the database must be ignored after this point
   AXImpExpDbc.ImpDbMode:=0; // Db_None
end;

procedure TMainWnd.CreateLayer(Layer:string);
begin
   aLayer:=DestDocument.Layers.InsertLayerByName(Layer,True);
end;

function TMainWnd.CheckLayerExists(Layer:string):boolean;
var dispatch:IDispatch;
begin
   dispatch:=DestDocument.Layers.GetLayerByName(Layer);
   if dispatch <> nil then result:=true else result:=false;
end;

procedure TMainWnd.SetCurrentLayer(Layer:string);
begin
   aLayer:=DestDocument.Layers.GetLayerByName(Layer);
end;

procedure TMainWnd.CreatePolyline;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;

   APolyline:=DestDocument.CreatePolyline;
end;

procedure TMainWnd.InsertPointToPolyLine(X:double; Y:double);
var APoint:IPoint;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;

   AxGisProjection.Calculate(X,Y);
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   APolyLine.InsertPoint(APoint);
end;

function TMainWnd.ClosePolyline(var HasDuplicate:boolean):integer;
var AIndex:integer;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;
   aLayer.InsertObject(APolyline);
   AIndex:=APolyline.Index;
   // check if it is a duplicate
   DuplId:=0;
   HasDuplicate:=FALSE;
   if SetE00FileWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, aLayer.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
      end;
   end;
   result:=AIndex;
end;

procedure TMainWnd.CreatePolygon;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;
   APolygon:=DestDocument.CreatePolygon;
end;

procedure TMainWnd.InsertPointToPolygon(X:double; Y:double);
var APoint:IPoint;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;

   // do the projection
   AxGisProjection.Calculate(X,Y);
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   APolygon.InsertPoint(APoint);
end;

function TMainWnd.ClosePolygon(var HasDuplicate:boolean):integer;
var AIndex:integer;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;
   APolygon.ClosePoly;
   aLayer.InsertObject(APolygon);
   AIndex:=APolygon.Index;
   // check if it is a duplicate
   DuplId:=0;
   HasDuplicate:=FALSE;
   if SetE00FileWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, aLayer.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
      end;
   end;
   result:=AIndex;
end;

function TMainWnd.InsertPoint(X:double;Y:double; var HasDuplicate:boolean):integer;
var APixel:IPixel;
    APoint:IPoint;
    AIndex:integer;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;

   APixel:=DestDocument.CreatePixel;
   APoint:=DestDocument.CreatePoint;
   // do the projection
   AxGisProjection.Calculate(X,Y);

   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   APixel.Position:=APoint;
   aLayer.InsertObject(APixel);
   AIndex:=APixel.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

   // check if it is a duplicate
   if SetE00FileWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, aLayer.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
      end;
   end;
   result:=AIndex;
end;

function TMainWnd.CreateSymbol(X:double;Y:double;Symname:string; var HasDuplicate:boolean):integer;
var ASymbol:ISymbol;
    APoint :IPoint;
    ASymboldef:ISymbolDef;
    i:integer;
    gefunden:boolean;
    Symnr:integer;
    AIndex:integer;
    SymbolSize:double;
    SymbolSizeType:integer;
    DuplId:integer;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;

   { Now the Symbolnr has to be evaluated }
   gefunden:=false;
   for i:=0 to DestDocument.Symbols.Count-1 do
   begin
      ASymboldef:=DestDocument.Symbols.Items[i];
      if ASymboldef.Name = Symname then
      begin
         Symnr:=ASymboldef.Index;
         SymbolSizeType:=ASymboldef.DefaultSizeType;
         SymbolSize:=ASymboldef.DefaultSize;
         gefunden:=true;
      end;
   end;
   if (gefunden) then
   begin
      // do the projection
      AxGisProjection.Calculate(X,Y);

      ASymbol:=DestDocument.CreateSymbol;
      ASymbol.Symbolname:=Symname;
      ASymbol.SymIndex:=SymNr;
      ASymbol.Position.X:=round(X*100);
      ASymbol.Position.Y:=round(Y*100);
      ASymbol.SizeType:=SymbolSizeType;
      ASymbol.Size:=SymbolSize;
      aLayer.InsertObject(ASymbol);
      AIndex:=ASymbol.Index;
      DuplId:=0;
      HasDuplicate:=FALSE;
      // check if it is a duplicate
      if SetE00FileWnd.IgnoreDuplicatesCb.Checked then
      begin
         // check if the object has a duplicate
         DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, aLayer.Layername);
         if DuplId > 0 then
         begin
            // remove the object
            DestDocument.DeleteObjectByIndex(AIndex);
            AIndex:=DuplId;
            HasDuplicate:=TRUE;
         end;
      end;
   end
   else
   begin
      AIndex:=InsertPoint(X,Y,hasduplicate);
   end;
   result:=AIndex;
end;

procedure TMainWnd.ConcatFiles(var Filename:string);
var curfilename:string;
    curextension:string;
    OldFile:file;
    NewFile:file;
    Extnr:integer;
    finishconcat:boolean;
    basefileopened:boolean;
    Actualbytes    :array [1..5080] of char;
    numread      :integer;
    numwritten   :integer;
begin
   { An E00 file could also contain of more files (E00, E01, E02... )
     so all this files have to be concatinated to work only with one file }
   Extnr:=0;
   finishconcat:=FALSE;
   basefileopened:=FALSE;
   while (not finishconcat) do
   begin
      Extnr:=Extnr+1;
      curextension:='.E0'+inttostr(Extnr);
      curfilename:=Filename;
      curfilename:=Changefileext(curfilename,curextension);
      if Fileexists(curfilename) then { An additional E0X file exists and need to be concatinated }
      begin
         if (not basefileopened) then
         begin
           Filemode:=0; Assignfile(OldFile,Filename); Reset(OldFile,1);
           Filemode:=1; Assignfile(NewFile,Changefileext(Filename,'.cat')); Rewrite(NewFile,1);
           Filename:=Changefileext(Filename,'.cat');
           { Now the e00 file will be copied into the .cat file }
           repeat
              BlockRead(OldFile, ActualBytes, SizeOf(ActualBytes), NumRead);
              BlockWrite(NewFile, ActualBytes, NumRead, NumWritten);
           until (NumRead = 0) or (NumWritten <> NumRead);
           CloseFile(OldFile);
           basefileopened:=true;
           ConcatFlag:=TRUE;
         end;
         { now the curfilename file has to be concated to the new .cat file }

         StatusLB.Items.Add(StrfileObj.Assign(37)+curfilename); { Warning: Import canceled! }
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         Application.Processmessages;


         Filemode:=0; Assignfile(OldFile,curfilename); Reset(OldFile,1);
         { Now the e00 file will be copied into the .cat file }
         repeat
              BlockRead(OldFile, ActualBytes, SizeOf(ActualBytes), NumRead);
              BlockWrite(NewFile, ActualBytes, NumRead, NumWritten);
         until (NumRead = 0) or (NumWritten <> NumRead);
         Closefile(OldFile);
      end
      else
      begin
         if (basefileopened) then
         begin
            {$i-} Closefile(NewFile); {$i+}
         end;
         finishconcat:=TRUE;
      end;
   end;
end;

procedure TMainWnd.CloseTimerTimer(Sender: TObject);
begin
   MainWnd.Close;
   SetE00FileWnd.Close;
   DestDocument.FinishImpExpRoutine(ExitNormal,'IMPORTMODULE');
   CloseTimer.Enabled:=false;
end;

procedure TMainWnd.CancelClick(Sender: TObject);
begin
   StatusLB.Items.Add(StrfileObj.Assign(27)); { Warning: Import canceled! }
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;

   FileObj.CloseallFiles;
   Abort:=true;
   ProcessActive:=FALSE;
   FileObj.Destroy;
   Cancel.Enabled:=false;
   Ok.Enabled:=true;
   SaveReportBtn.Enabled:=true;
end;

procedure TMainWnd.StartImport;
begin
   if ExitNormal then
   begin
      AXImpExpDbc.WorkingDir:=OSInfo.WingisDir;
      AXImpExpDbc.LngCode:=Lngcode;
      FirstActivate:=false;
      RunPass(1);
   end;
end;

procedure TMainWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TMainWnd.OKClick(Sender: TObject);
begin
   CloseTimer.Enabled:=true;
end;

procedure TMainWnd.SaveReportBtnClick(Sender: TObject);
var Datei:Textfile;
    Zeile:string;
    i:integer;
begin
   if SaveDialogRep.Execute then
   begin
      {$I-}
      Assignfile(Datei,SaveDialogRep.Filename);
      Rewrite(Datei);
      for i:=0 to StatusLB.Items.Count-1 do
      begin
         Zeile:=StatusLB.items[i];
         Writeln(Datei,Zeile);
      end;
      Closefile(Datei);
      {$I+}
   end;
end;

procedure TMainWnd.FormDestroy(Sender: TObject);
begin
   OptionWnd.Free;;
end;

procedure TMainWnd.SetupWindowTimerTimer(Sender: TObject);
var IdbProcessMask:integer;
begin
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,SetE00FileWnd.winleft,SetE00FileWnd.wintop,SetE00FileWnd.winwidth, SetE00FileWnd.winheight,SWP_SHOWWINDOW);
   MainWnd.Height:=SetE00FileWnd.winheight;
   MainWnd.Width:=SetE00FileWnd.winwidth;
   Self.Repaint;
   SetupWindowTimer.Enabled:=false;
   IdbProcessmask:=DestDocument.IdbProcessMask;
   DestDocument.IdbProcessMask:=0;
   StartImport;
   DestDocument.IdbProcessMask:=IdbProcessMask;
end;

procedure TMainWnd.FormShow(Sender: TObject);
begin
   AXImpExpDbc.visible:=FALSE;
   AxGisProjection.visible:=FALSE;
   AxGisObjMan.Visible:=FALSE;
end;

procedure TMainWnd.AXImpExpDbcDialogSetuped(Sender: TObject);
begin
   SetE00fileWnd.DestDbLabel.Caption:=AxImpExpDbc.ImpDbInfo;
   SetE00fileWnd.PageControl.Enabled:=TRUE;
   SetE00fileWnd.GeoBtn.Enabled:=TRUE;
   SetE00fileWnd.DbBtn.Enabled:=TRUE;
   SetE00fileWnd.OkBtn.Enabled:=SetE00fileWnd.OkBtnMode;
   SetE00fileWnd.CancelBtn.Enabled:=TRUE;
end;

procedure TMainWnd.AxGisProjectionWarning(Sender: TObject;
  const Info: WideString);
begin
   if not ProcessActive then
   begin
      Showmessage(Info);
   end
   else
   begin
      StatusLb.Items.Add(Info);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
   end;
end;

procedure TMainWnd.AxGisProjectionError(Sender: TObject;
  const Info: WideString);
begin
   if not ProcessActive then
   begin
      Showmessage(Info);
   end
   else
   begin
      StatusLb.Items.Add(Info);
      StatusLB.Items.Add(StrfileObj.Assign(27)); { Warning: Import canceled! }
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      FileObj.CloseallFiles;
      Abort:=true;
      ProcessActive:=FALSE;
      FileObj.Destroy;
      Cancel.Enabled:=false;
      Ok.Enabled:=true;
      SaveReportBtn.Enabled:=true;
   end;
end;

procedure TMainWnd.AXImpExpDbcRequestImpLayerIndex(Sender: TObject;
  const Layername: WideString; var LayerIndex: Integer);
var i:integer;
begin
   // the Layerindex of the Layername has to be evaluated
   for i:=0 to DestDocument.Layers.Count-1 do
   begin
      if DestDocument.Layers.Items[i].Layername = Layername then
      begin
         LayerIndex:=DestDocument.Layers.Items[i].Index;
         break;
      end;
   end;
end;

procedure TMainWnd.AXImpExpDbcRequestImpProjectName(Sender: TObject;
  var ProjectName: WideString);
var ProjName:string;
begin
   ProjName:=DestDocument.Name;
   ProjectName:=ProjName;
end;

procedure TMainWnd.AXImpExpDbcRequestImpProjectGUIDs(Sender: TObject;
  var StartGUID, CurrentGUID: WideString);
var aStartGUID, aCurrentGUID:string;
begin
   aStartGUID:=DestDocument.StartProjectGUID;
   aCurrentGUID:=DestDocument.CurrentProjectGUID;
   StartGUID:=aStartGUID;
   CurrentGUID:=aCurrentGUID;
end;

procedure TMainWnd.AXImpExpDbcWarning(Sender: TObject;
  const Info: WideString);
begin
   StatusLb.Items.Add(Info);
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;
end;

end.
