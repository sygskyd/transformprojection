library E00Imp;

{ Wichtiger Hinweis zur DLL-Speicherverwaltung: Sharemem mu� die erste
 Unit in der uses-Klausel Ihrer Bibliothek und Ihres Projekts sein,
 wenn Ihre DLL Prozeduren und Funktionen exportiert, die
 Strings als Parameter oder Funktionsergebnisse �bergeben. Dies
 betrifft alle Strings, die an Ihre DLL oder von Ihrer DLL �bergeben werden--selbst diejenigen,
 die sich in Records oder Klassen befinden. Sharemem ist die Schnittstelle zu
 BORLNDMM.DLL, die den gemeinsamen Zugriff auf den Speicher verwaltet; DELPHIMM.DLL
 mu� mit Ihrer DLL weitergegeben werden. Wenn Sie BORLNDMM.DLL nicht verwenden m�chten, m�ssen
 Strings als PChar oder ShortString �bergeben werden.}

uses
  SysUtils,
  Classes,
  Dialogs,
  Maindlg in 'maindlg.pas' {MainWnd},
  OptionDlg in 'OptionDlg.pas' {OptionWnd},
  SetE00fileDlg in 'SetE00fileDlg.pas' {SetE00fileWnd},
  Files in 'Files.pas',
  comobj,
  AXDLL_TLB,
  Strfile in 'Strfile.pas',
  struct in 'Struct.pas';

{$R *.RES}

{*******************************************************************************
* PROZEDURE * E00IMPORT                                                        *
********************************************************************************
* Procedure is used to import ARCINFO data into GIS                            *
*                                                                              *
********************************************************************************}
procedure E00IMPORT(DIR:PCHAR;var AXHANDLE:VARIANT;var DEST:VARIANT;REGMODE:integer;LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var StrfileObj:ClassStrfile;
    aDisp:IDispatch;
begin
   // creat MainWnd
   MainWnd:=TMainWnd.Create(nil);
   Mainwnd.App:=AXHANDLE;
   aDisp:=DEST;
   MainWnd.DestDocument:=aDisp as IDocument;
   MainWnd.LngCode:=strpas(LngCode);
   MainWnd.RegMode:=REGMODE;
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.CreateStringFile;

   // create SetE00FileWnd
   SetE00fileWnd:=TSetE00fileWnd.Create(nil);
   SetE00fileWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);

   SetE00fileWnd.Show;
end;

{*******************************************************************************
* PROCEDURE * CREATEIMP                                                        *
********************************************************************************
* Procedure is used to prepare an E00-import for Batchmode                     *
*                                                                              *
********************************************************************************}
procedure CREATEIMP(DIR:PCHAR;var AXHANDLE:VARIANT;var DEST:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var aDisp:IDispatch;
begin
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.App:=AXHANDLE;
   aDisp:=DEST;
   MainWnd.DestDocument:=aDisp as IDocument;
   MainWnd.Regmode:=REGMODE;
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.CreateStringFile;

   // create SetE00FileWnd
   SetE00fileWnd:=TSetE00fileWnd.Create(nil);
   SetE00fileWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);
   SetE00fileWnd.SetupForBatch;
end;

{*******************************************************************************
* PROCEDURE * SETE00IMPSETTINGS                                                *
********************************************************************************
* Procedure is used to set type of import                                      *
*                                                                              *
* PARAMETERS: IMPMODE     -> Type if the import should be selective or not     *
*                            0 -> Import should not be selective.              *
*                            1 -> Import should be selective.                  *
*                                                                              *
********************************************************************************}
procedure SETE00IMPSETTINGS(IMPMODE:INTEGER);stdcall;
begin
   SetE00fileWnd.SetImpSettings(IMPMODE);
end;

// procedure is used to add a .E00 file that should be imported
procedure ADDIMPFILE(AFILE:PCHAR);stdcall;
begin
   SetE00fileWnd.AddImpFile(strpas(AFILE));
end;

// procedure is used to set database-settings
procedure SETDBSETTINGS(SOURCEDBMODE:INTEGER; SOURCEDBNAME:PCHAR; SOURCEDBTABLE:PCHAR;
                        DESTDBMODE  :INTEGER; DESTDBNAME  :PCHAR; DESTDBTABLE  :PCHAR);stdcall;
begin
   // source database will be ignored for E00-Import
   MainWnd.AXImpExpDbc.ImpDbMode:=DESTDBMODE;
   MainWnd.AXImpExpDbc.ImpDatabase:=strpas(DESTDBNAME);
   MainWnd.AXImpExpDbc.ImpTable:=strpas(DESTDBTABLE);
end;

// procedure is used to set projection for batch mode
procedure SETPROJECTION(SOURCECOORDINATESYSTEM:INTEGER; // source projection
                        SOURCEPROJECTION      :PCHAR;
                        SOURCEDATE            :PCHAR;
                        SOURCEPROJSETTINGS    :PCHAR;
                        SOURCEXOFFSET         :DOUBLE;
                        SOURCEYOFFSET         :DOUBLE;
                        SOURCESCALE           :DOUBLE;
                        TARGETCOORDINATESYSTEM:INTEGER; // target projection
                        TARGETPROJECTION      :PCHAR;
                        TARGETDATE            :PCHAR;
                        TARGETPROJSETTINGS    :PCHAR;
                        TARGETXOFFSET         :DOUBLE;
                        TARGETYOFFSET         :DOUBLE;
                        TARGETSCALE           :DOUBLE);stdcall;
begin
   // source projection
   MainWnd.AxGisProjection.SourceCoordinateSystem:=SOURCECOORDINATESYSTEM;
   MainWnd.AxGisProjection.SourceProjection:=strpas(SOURCEPROJECTION);
   MainWnd.AxGisProjection.SourceDate:=strpas(SOURCEDATE);
   MainWnd.AxGisProjection.SourceProjSettings:=strpas(SOURCEPROJSETTINGS);
   MainWnd.AxGisProjection.SourceXOffset:=SOURCEXOFFSET;
   MainWnd.AxGisProjection.SourceYOffset:=SOURCEYOFFSET;
   MainWnd.AxGisProjection.SourceScale:=SOURCESCALE;

   // target projection
   MainWnd.AxGisProjection.TargetCoordinateSystem:=TARGETCOORDINATESYSTEM;
   MainWnd.AxGisProjection.TargetProjection:=strpas(TARGETPROJECTION);
   MainWnd.AxGisProjection.TargetDate:=strpas(TARGETDATE);
   MainWnd.AxGisProjection.TargetProjSettings:=strpas(TARGETPROJSETTINGS);
   MainWnd.AxGisProjection.TargetXOffset:=TARGETXOFFSET;
   MainWnd.AxGisProjection.TargetYOffset:=TARGETYOFFSET;
   MainWnd.AxGisProjection.TargetScale:=TARGETSCALE;
end;

// function is used to execute an E00-import
procedure EXECIMP;stdcall;
begin
   MainWnd.ExitNormal:=true;
   MainWnd.Show;
end;

function FREEDLL:boolean; stdcall;
var retVal:boolean;
begin
   retVal:=MainWnd.ExitNormal;
   MainWnd.Free;
   SetE00fileWnd.Free;
   result:=retVal;
end;

exports E00IMPORT, CREATEIMP, SETE00IMPSETTINGS, ADDIMPFILE,
        SETDBSETTINGS, SETPROJECTION, EXECIMP, FREEDLL;

begin
end.
