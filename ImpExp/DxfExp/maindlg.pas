unit Maindlg;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Strfile, StdCtrls, Buttons, ExtCtrls, Gauges, Files,
  windows, OleCtrls, AxGisPro_TLB, WinOSInfo;

const
       //registration modes
       NO_LICENCE        = 0;
       FULL_LICENCE      = 1;
       DEMO_LICENCE      = 2;

       // Object types
       ot_Pixel     = 1;
       ot_Poly      = 2;
       ot_CPoly     = 4;
       ot_Text      = 7;
       ot_Symbol    = 8;
       ot_Spline    = 9;
       ot_Image     = 10;
       ot_MesLine   = 11;
       ot_Circle    = 12;
       ot_Arc       = 13;
       ot_Layer     = 18;

             //TextSTyles
       ts_Left           =  0;
       ts_Center         =  1;
       ts_Right          =  2;
       ts_Italic         =  4;
       ts_Bold           =  8;
       ts_Underl         = 16;
       ts_FixHeight      = 32;
       ts_Transparent    = 64;

       //LineTypes
       lt_Solid     = 0;
       lt_Dash      = 1;
       lt_Dot       = 2;
       lt_DashDot   = 3;
       lt_DashDDot  = 4;
       lt_UserDefined    = 1000;

       //FillTypes
       pt_NoPattern = 0;
       pt_FDiagonal = 1;
       pt_Cross     = 2;
       pt_DiagCross = 3;
       pt_BDiagonal = 4;
       pt_Horizontal= 5;
       pt_Vertical  = 6;
       pt_Solid     = 7;
       pt_UserDefined = 8;

       stDrawingUnits    = 0;
       stScaleInDependend= 1;
       stProjectScale    = 2;
       stPercent         = 3;


type
  TMainWnd = class(TForm)
    Panel1: TPanel;
    Progresslabel: TLabel;
    Prozessbalken: TGauge;
    StatusLB: TListBox;
    SaveReportBtn: TButton;
    CloseTimer: TTimer;
    SaveDialogRep: TSaveDialog;
    OKBtn: TButton;
    CancelBtn: TButton;
    SetupWindowTimer: TTimer;
    AxGisProjection: TAxGisProjection;
    procedure FormCreate(Sender: TObject);
    procedure StartBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure QuittimerTimer(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CloseTimerTimer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure SaveReportBtnClick(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure AxGisProjectionError(Sender: TObject;
      const Info: WideString);
    procedure AxGisProjectionWarning(Sender: TObject;
      const Info: WideString);
    procedure FormShow(Sender: TObject);
  private
    { Private-Deklarationen }
    StrFileObj :ClassStrfile;
    FileObj    :ClassFile;

    Pass       :integer;
    Bytes2work  :longint;
    Bytesworked :longint;
    Aktiv       :boolean;
    Abort       :boolean;
    FirstActivate:boolean;

    DxfFilename :string;
    ObjectsExported:longint;

    function CalculateElementsToExport:integer;
    function CalculateColorStr(color:longint):string;
    function CalculateLineStyle(Style:longint):string;
    function  GetDefaultSymSize(Symname:string):double;
    procedure ExportCPoly(OldObject:Variant;Layername:string;IsSymbol:boolean);
    procedure Showpercent;
    procedure MakeCorrectName(var name:string);
    procedure ConvertMultiLineText(var aText:string);

    procedure RunPass(AktPass:integer);
    procedure ExecutePass;
    procedure StartExport;
  public
    { Public-Deklarationen }
    App             :Variant;
    SourceDocument  :Variant;
    RegMode         :integer;
    ExitNormal      :boolean;
    GisHandle       :integer;
    LngCode         :string;
    SymbolScale     :integer;
    function ReadElement:integer;
    procedure ExportObject(AObject:Variant;IsSymbol:boolean;Layername:string);
    procedure ReadSymbolLibrary;

    procedure SetProjectionSettings;
    procedure CheckProjectionSettings;
    procedure CreateStringFile;
    function  MyFloatToStr(value :double):string;
  end;

var
  MainWnd: TMainWnd;

implementation

uses comobj, SelDxfDlg;

{$R *.DFM}

procedure TMainWnd.CreateStringFile;
begin
   StrFileObj:=ClassStrfile.Create;
   StrFileObj.CreateList(OSInfo.WingisDir,LngCode,'DXFEXP');

   Self.Caption:=StrFileObj.Assign(1);           { PROGIS ACAD-DXF-Export 2000 }
   ProgressLabel.Caption:=StrFileObj.Assign(9);  { Process messages }
   SaveReportBtn.Caption:=StrFileObj.Assign(10); { Save report      }
   OkBtn.Caption:=StrFileObj.Assign(5);          { Ok }
   CancelBtn.Caption:=StrFileObj.Assign(6);      { Cancel }
end;

procedure TMainWnd.FormCreate(Sender: TObject);
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];

   Self.Height:=1;
   Self.Width:=1;
   Self.Update;
   Aktiv:=false;

   FirstActivate:=true;
   ObjectsExported:=0;
   SymbolScale:=100; // Default Symbolscale is 100

   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

procedure TMainWnd.CheckProjectionSettings;
var SourceProjection:integer;
    hstr:string;
    different:boolean;
begin
   different:=false;
   // function is used to check if the settings in the
   if (AxGisProjection.SourceCoordinateSystem <> SourceDocument.Projection) then
      different:=TRUE;

   if (AxGisProjection.SourceXOffset <> SourceDocument.ProjectionXOffset) then
      different:=TRUE;

   if (AxGisProjection.SourceYOffset <> SourceDocument.ProjectionYOffset) then
      different:=TRUE;

   if (AxGisProjection.SourceScale <> SourceDocument.ProjectionScale) then
      different:=TRUE;

   // check if the projections are different
   if SourceDocument.ProjectProjSettings <> AxGisProjection.SourceProjSettings then
      different:=TRUE;

   if SourceDocument.ProjectProjection <> AxGisProjection.SourceProjection then
      different:=TRUE;

   if SourceDocument.Projectdate <> AxGisProjection.SourceDate then
      different:=TRUE;

   if different then
   begin
      Showmessage(StrfileObj.Assign(39)); // Warning: Projection does not fit to project projection
      AxGisProjection.SetupByDialog;
   end;
   // the used output coordinate-system is equal to the
   // target-coordinate-system
   AxGisProjection.UsedOutputCoordSystem:=AxGisProjection.TargetCoordinateSystem;
end;

procedure TMainWnd.SetProjectionSettings;
begin
   // procedure is used to set the projection settings
   // of the current project to the GeoWnd
   AxGisProjection.SourceCoordinateSystem:=SourceDocument.Projection;
   AxGisProjection.TargetCoordinateSystem:=cs_Carthesic;   // ACAD data normaly is in carthesic
   AxGisProjection.SourceProjSettings:=SourceDocument.ProjectProjSettings;
   AxGisProjection.TargetProjSettings:=SourceDocument.ProjectProjSettings;
   AxGisProjection.SourceProjection:=SourceDocument.ProjectProjection;
   AxGisProjection.TargetProjection:=SourceDocument.ProjectProjection;
   AxGisProjection.SourceDate:=SourceDocument.Projectdate;
   AxGisProjection.TargetDate:=SourceDocument.Projectdate;
   AxGisProjection.SourceXOffset:=SourceDocument.ProjectionXOffset;
   AxGisProjection.TargetXOffset:=SourceDocument.ProjectionXOffset;
   AxGisProjection.SourceYOffset:=SourceDocument.ProjectionYOffset;
   AxGisProjection.TargetYOffset:=SourceDocument.ProjectionYOffset;
   AxGisProjection.SourceScale:=SourceDocument.ProjectionScale;
   AxGisProjection.TargetScale:=SourceDocument.ProjectionScale;
   // if source and target is .amp - file
   // the used input coordinates and used output coordinates
   // for the projection calculation are in carthesic
   AxGisProjection.UsedInputCoordSystem:=cs_Carthesic;
   AxGisProjection.UsedOutputCoordSystem:=AxGisProjection.TargetCoordinateSystem;
end;

{*******************************************************************************
* PROZEDUR : StartBtnClick                                                     *
********************************************************************************
* Prozedur wird aufgerufen wenn der Start-Button im MainWnd                    *
* gedr�ckt wurde. Damit wird die Konvertierung gestartet.                      *
*                                                                              *
********************************************************************************}
procedure TMainWnd.StartBtnClick(Sender: TObject);
begin
   RunPass(1);
end;

function TMainWnd.CalculateElementsToExport:integer;
var numelems:integer;
    layercnt:integer;
    ALayer:Variant;
    i:integer;
    Layername:string;
begin
   numelems:=0;
   if SelDxfWnd.SelectedCB.checked then
   begin
       SourceDocument.CreateSelectedList;
       numelems:=SourceDocument.NrOfSelectedObjects;
       result:=numelems;
       exit;
   end;
   for layercnt:=0 to SourceDocument.Layers.Count-1 do
   begin
      ALayer:=SourceDocument.Layers.Items[layercnt];
      { Nun mu� kontrolliert werden ob dieser Layer �berhaupt exportiert werden soll }
      if (SelDxfWnd.LayerCB.checked) then
      begin
         Layername:=ALayer.Layername;
         for i:=0 to SelDxfWnd.LayerLB.Items.Count-1 do
         begin
            if (SelDxfWnd.LayerLB.Items[i] = Layername) and (SelDxfWnd.LayerLB.Selected[i]) then
            begin
               numelems:=numelems+ALayer.count;
               break;
            end;
         end;
      end
      else
         numelems:=numelems+ALayer.count;
   end;
   result:=numelems;
end;

procedure TMainWnd.ReadSymbolLibrary;
var MySymbolDef:Variant;
    SymName:string;
    X:double;
    Y:double;
    i,j:integer;
    AObject:Variant;
    AObjectDisp:IDispatch;
begin
   { Nun wird die Symbolbibliothek ausgelesen }
   for i:=0 to SourceDocument.Symbols.Count-1 do
   begin
      MySymboldef:=SourceDocument.Symbols.Items[i];
      X:=MySymbolDef.RefPoint.X;
      Y:=MySymbolDef.RefPoint.Y;
      Symname:=MySymbolDef.Name;
      MakeCorrectName(SymName);
      // check if a Symbol-Definition with this name already exists
      if FileObj.CheckSymbolRefExists(SymName) then
      begin
         // display a warning that this block can not be exported
         StatusLb.Items.Add(StrFileObj.Assign(40)+' '+MySymbolDef.Name+' '+ StrFileObj.Assign(41)+' '+SymName+' '+StrFileObj.Assign(42));
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         continue;
      end;
      FileObj.New_Symbolref(X/100/SymbolScale,Y/100/SymbolScale,SymName);

      { Nun werden die einzelnen Items ausgelesen }
      for j:=0 to MySymbolDef.Count-1 do
      begin
         AObjectDisp:=MySymbolDef.Items[j];
         if (AObjectDisp = nil) then
            continue;
         AObject:=AObjectDisp;
         ExportObject(AObject,TRUE,SymName);    { Das TRUE bedeutet das Objekt zu einem Symbol geh�rt }
      end;
      // check ob symbolreferenz �berhaupt objekte enth�lt
      // wenn nicht wird warnung ausgegeben und ein punkt
      // in die symbolreferenz eingef�gt, damit sie nicht
      // leer ist
      if FileObj.CheckSymbolIsEmpty then
      begin
         StatusLB.Items.Add(StrfileObj.Assign(33) + SymName + ' - '+StrfileObj.Assign(34));  // Warning: Empty Block: XXX - dummy point will be inserted
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         FileObj.New_Point(0,0,True);
      end;
   end;
end;

procedure TMainWnd.MakeCorrectName(var name:string);
var pdummy:PCHAR;
    pdummy2:PCHAR;
    i,pos:integer;
    res:string;
    canadd:boolean;
begin
   //function is used to set the layername that it can be set as database or controlname
   pdummy:=stralloc(length(name)+1); strpcopy(pdummy,name);
   pdummy2:=stralloc(length(name)+1); strpcopy(pdummy2,name);
   pos:=0;
   for i:=0 to strlen(pdummy)-1 do
   begin
      canadd:=false;
      if (Ord(pdummy[i]) >= Ord ('A')) and (Ord(pdummy[i]) <= Ord ('Z')) then canadd:=true;
      if (Ord(pdummy[i]) >= Ord ('a')) and (Ord(pdummy[i]) <= Ord ('z')) then canadd:=true;
      if (Ord(pdummy[i]) >= Ord ('0')) and (Ord(pdummy[i]) <= Ord ('9')) then canadd:=true;
      if (pdummy[i] = '$') then canadd:=true;
      if (pdummy[i] = '_') then canadd:=true;
      if (pdummy[i] = '-') then canadd:=true;

      if (canadd) then
      begin
         pdummy2[pos]:=pdummy[i];
         pos:=pos+1;
      end;
   end;
   name:=strpas(pdummy2);
   name:=copy(name,1,pos);
   strdispose(pdummy); strdispose(pdummy2);
end;

function TMainWnd.ReadElement:integer;
var layercnt:integer;
    ALayer:Variant;
    AObject:Variant;
    AObjectDisp:IDispatch;
    i,cnt:integer;
    Layername:string;
    found:boolean;
    Linienstil_str:string;
    Linienfarbe_str:string;
    DoexportLayer:boolean;
    MayExportObject:boolean;
    SelectedLayername:string;
begin
   MayExportObject:=true;
   // check if only selected objects should be exported
   if SelDxfWnd.SelectedCB.checked then
   begin
      SelectedLayername:='';
      for cnt:=0 to SourceDocument.NrOfSelectedObjects-1 do
      begin
         if Abort then exit;
         if SourceDocument.NrOfSelectedObjects = 0 then break;
         AObjectDisp:=SourceDocument.SelItems[cnt];
         if AObjectDisp = nil then
         begin
            Bytesworked:=Bytesworked+1;
            ShowPercent;
            continue;
         end;
         AObject:=AObjectDisp;
         if AObject.ObjectType = ot_Layer then // Layer Object received
         begin
            Layername:=AObject.Layername;
            if Layername <> SelectedLayername then
            begin
               SelectedLayername:=Layername;
               // first an entry for the layer has to be created
               Linienfarbe_str:=CalculateColorStr(AObject.Linestyle.Color);
               Linienstil_str:=CalculateLineStyle(AObject.Linestyle.Style);
               { Zuerst wird der Layer erzeugt }
               MakeCorrectName(Layername);
               FileObj.New_Layer(Layername, Linienstil_str,  Linienfarbe_str, '', '');
            end;
            Bytesworked:=Bytesworked+1;
            ShowPercent;
         end
         else
         begin
            ExportObject(AObject,false,Layername);
            ObjectsExported:=ObjectsExported+1;

            // check if in demomode
            if (RegMode = DEMO_LICENCE) and (ObjectsExported = 100) then
            begin
               StatusLB.Items.Add(StrfileObj.Assign(32));  // 100 objects exported, following objects will be ignored
               StatusLB.ItemIndex := StatusLB.Items.Count-1;
               StatusLB.Update;
               MayExportObject:=false;
               Bytesworked:=Bytes2work;
               break;
            end;
         end;
      end;
      if not Abort then
         SourceDocument.DestroySelectedList;
      exit;
   end;

   for layercnt:=0 to SourceDocument.Layers.Count-1 do
   begin
      if Abort then exit;
      if not MayExportObject then exit;
      ALayer:=SourceDocument.Layers.Items[layercnt];
      Layername:=ALayer.Layername;

      // check if layer should be exported
      doexportlayer:=false;
      if (SelDxfWnd.LayerCB.Checked) then
      begin
         for i:=0 to SelDxfWnd.LayerLB.Items.Count-1 do
         begin
            if (SelDxfWnd.LayerLB.Items[i] = Layername) and (SelDxfWnd.LayerLB.Selected[i]) then
            begin
               doexportlayer:=true;
               break;
            end;
         end;
      end
      else
         doexportlayer:=true;

      if (doexportlayer) then
      begin
         Linienfarbe_str:=CalculateColorStr(ALayer.Linestyle.Color);
         Linienstil_str:=CalculateLineStyle(ALayer.Linestyle.Style);
         { Zuerst wird der Layer erzeugt }
         MakeCorrectName(Layername);
         FileObj.New_Layer(Layername, Linienstil_str,  Linienfarbe_str, '', '');
         { Nun werden die einzelnen Layerelemente ausgelesen }
         for cnt:=0 to ALayer.count-1 do
         begin
            if Abort then exit;
            AObjectDisp:=ALayer.Items[cnt];
            if AObjectDisp <> nil then
            begin
               AObject:=AObjectDisp;
               ExportObject(AObject,false,Layername);
            end;
            ObjectsExported:=ObjectsExported+1;

            // check if in demomode
            if (RegMode = DEMO_LICENCE) and (ObjectsExported = 100) then
            begin
               StatusLB.Items.Add(StrfileObj.Assign(32));  // 100 objects exported, following objects will be ignored
               StatusLB.ItemIndex := StatusLB.Items.Count-1;
               StatusLB.Update;
               MayExportObject:=false;
               Bytesworked:=Bytes2work;
               break;
            end;
         end;
      end;
   end;
end;

procedure TMainWnd.ExportObject(AObject:Variant;IsSymbol:boolean;Layername:string);
var AFont:string;
    X,Y:double;
    ASize:double;
    Radius:integer;
    i:integer;
    APoint:Variant;
    Textinhalt:string;
    SymSize,SymAngle,SymDefaultSize:double;
    SymName:string;
begin
   if (AObject.ObjectType = ot_Pixel) then // export point object
   begin
      X:=AObject.Position.X / 100;
      Y:=AObject.Position.Y / 100;
      if not IsSymbol then // do the projection
         AxGisProjection.Calculate(X,Y);
      FileObj.New_Point(X,Y,IsSymbol);
   end;

   if (AObject.ObjectType = ot_Cpoly) then // export area object
   begin
      ExportCPoly(AObject,Layername,IsSymbol);
   end;

   if (AObject.ObjectType = ot_poly) then // export line object
   begin
      FileObj.New_Line(IsSymbol);
      for i:=0 to AObject.Count - 1 do
      begin
         APoint:=AObject.Points[i];
         X:=APoint.X/100; Y:=APoint.Y/100;
         if not IsSymbol then
            AxGisProjection.Calculate(X,Y);
         FileObj.InsertPointToLine(X,Y,IsSymbol);
      end;
   end;

   if (AObject.ObjectType = ot_Text) then { Ein neuer Text wird eingef�gt }
   begin
      AFont:=AObject.Fontname;
      // X:=AObject.Position.X / 100;
      // Y:=AObject.Position.Y / 100;
      X:=AObject.PXFP1_X / 100;
      Y:=AObject.PXFP1_Y / 100;
      Textinhalt:=AObject.Text;
      ConvertMultiLineText(Textinhalt);
      ASize:=AObject.FontHeight/100;

      // calculate projection
      if not IsSymbol then
         AxGisProjection.Calculate(X,Y);
      if not IsSymbol then
         FileObj.New_Text(X,Y,Textinhalt,AFont,Asize,AObject.Angle,IsSymbol)
      else
      begin
         // display warning that a block could not contain text information
         // Blockname is in Layername
         StatusLB.Items.Add(StrfileObj.Assign(35)+' ' + Layername + ' ' + StrfileObj.Assign(36));  // Warning: Block XXX contains text data that will be ignored!
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
      end;
   end;

   if (AObject.ObjectType = ot_Circle) then { Ein neuer Circle wird eingef�gt }
   begin
      APoint:=AObject.Position;
      X:=APoint.X/100;
      Y:=APoint.Y/100;

      // calculate projection
      if not IsSymbol then
         AxGisProjection.Calculate(X,Y);

      Radius:=AObject.PrimaryAxis/100;
      FileObj.New_Circle(X,Y,Radius,IsSymbol);
   end;

   if (AObject.ObjectType = ot_Symbol) then { Ein neues Symbol wird eingef�gt }
   begin
      X:=AObject.Position.X/100;
      Y:=AObject.Position.Y/100;

      // calculate projection
      if not IsSymbol then
         AxGisProjection.Calculate(X,Y);

      // calculate size of symbol
      SymDefaultSize:=GetDefaultSymSize(AObject.Symbolname)/10000; // divide by 10000 to get meters
      if SymDefaultSize <= 0 then SymDefaultSize:=100;
      SymSize:=AObject.Size/100; // divide by 100 to get meters
      if SymSize <= 0 then
         SymSize:=1
      else
         SymSize:=SymSize / SymDefaultSize;

      SymName:=AObject.Symbolname;
      MakeCorrectName(SymName);
      // calculate angle of symbol
      SymAngle:=AObject.Angle;
      if SymAngle < 1 then
         SymAngle:=0
      else
         SymAngle:=SymAngle / (PI / 180);
      FileObj.New_Symbol(X,Y,SymName,SymSize,SymAngle,IsSymbol);
   end;

   if not IsSymbol then
   begin
      Bytesworked:=Bytesworked+1;
      ShowPercent;
   end;
end;

procedure TMainWnd.ExportCPoly(OldObject:Variant;Layername:string;IsSymbol:boolean);
var APoint:Variant;
    PointCount:integer;
    SkipIndex :integer;
    FirstIsland:boolean;
    IslandCount:integer;
    IsIsland:boolean;
    IslandInfoIdx:integer;
    NextIslandIdx:integer;
    X,Y:double;
    Cnt:integer;
begin
   PointCount:=0; SkipIndex:=0; FirstIsland:=TRUE; IsIsland:=FALSE;
   IslandCount:=OldObject.IslandCount;
   if IslandCount > 1 then
   begin
      IsIsland:=true;
      IslandInfoIdx:=0;
      NextIslandIdx:=OldObject.IslandInfo[IslandInfoIdx];
   end;

   FileObj.New_Area(IsSymbol);

   // insert first point to new CPoly
   APoint:=OldObject.Points[0];
   X:=APoint.X/100; Y:=APoint.Y/100;

   // calculate the projection
   if not IsSymbol then
      AxGisProjection.Calculate(X,Y);
   FileObj.InsertPointToArea(X,Y,IsSymbol);

   PointCount:=PointCount+1;

   for Cnt:=1 to OldObject.Count - 1 do
   begin
      if SkipIndex = 0 then
         PointCount:=PointCount + 1;
      if (IsIsland) and (PointCount = NextIslandIdx) then // next island area will be created
      begin
         IslandInfoIdx:=IslandInfoIdx+1;
         if FirstIsland then
            SkipIndex:=1
         else
            SkipIndex:=2;

         FirstIsland:=FALSE;
         if IslandInfoIdx >= IslandCount then
         begin
            // all islands have been inserted
            NextIslandIdx:=0;
            IslandInfoIdx:=IslandCount;
         end
         else
         begin
            // and a new polygon has to be created
            FileObj.New_Area(IsSymbol);

            NextIslandIdx:=OldObject.IslandInfo[IslandInfoIdx];

            // insert point to polygon
            APoint:=OldObject.Points[Cnt+SkipIndex];
            X:=APoint.X/100; Y:=APoint.Y/100;
            // calculate the projection
            if not IsSymbol then
               AxGisProjection.Calculate(X,Y);
            FileObj.InsertPointToArea(X,Y,IsSymbol);

            PointCount:=1;
         end;
      end
      else
      begin
         if (SkipIndex=0) then
         begin
            if (IsIsland) and (Cnt = OldObject.Count -2) then //avoid exporting last connectionline
               continue;

            // insert point to polygon
            APoint:=OldObject.Points[Cnt];
            X:=APoint.X/100; Y:=APoint.Y/100;
            // calculate the projection
            if not IsSymbol then
               AxGisProjection.Calculate(X,Y);
            FileObj.InsertPointToArea(X,Y,IsSymbol);
         end
         else
            SkipIndex:=SkipIndex-1;
      end;
   end;
end;

function TMainWnd.GetDefaultSymSize(Symname:string):double;
var retVal:double;
    i:integer;
    MySymboldef:Variant;
begin
   retVal:=0;
   for i:=0 to SourceDocument.Symbols.Count-1 do
   begin
      MySymboldef:=SourceDocument.Symbols.Items[i];
      if Symname = MySymbolDef.Name then
      begin
         retVal:=MySymbolDef.DefaultSize;
         break;
      end;
   end;
   result:=retVal;
end;

function TMainWnd.CalculateColorStr(color:longint):string;
var Farbstr:string;
begin
   Farbstr:='    7';
   case color of
      1..2  : begin Farbstr:='    1'; end; { Farbe ROT     }
      3..4  : begin Farbstr:='    2'; end; { Farbe YELLOW  }
      5..6  : begin Farbstr:='    3'; end; { Farbe GREEN   }
      7..8  : begin Farbstr:='    4'; end; { Farbe CYAN    }
      9..10 : begin Farbstr:='    5'; end; { Farbe BLUE    }
      11..12: begin Farbstr:='    6'; end; { Farbe MAGENTA }
      13..14: begin Farbstr:='    7'; end; { Farbe BLACK   }
      15..16: begin Farbstr:='  252'; end; { Farbe GRAY    }
      17..18: begin Farbstr:='  255'; end; { Farbe WHITE   }
   end;
   Result:=Farbstr;
end;

function TMainWnd.CalculateLineStyle(Style:longint):string;
var Linestyle:string;
begin
   case Style of
      lt_Dot            :Linestyle:='DOT';
      lt_Dash           :Linestyle:='DASHED';
   else
      Linestyle:='CONTINUOUS';
   end;
   result:=Linestyle;
end;

{*******************************************************************************
* PROZEDUR : RunPass                                                           *
********************************************************************************
* Prozedur leitet den jeweiligen Konvertierungs-Pass ein.                      *
*                                                                              *
* PARAMETER: Pass -> Gibt den Konvertierungs-Pass an.                          *
*                                                                              *
********************************************************************************}
procedure TMainWnd.RunPass(AktPass:integer);
begin
   if AktPass = 1 then // read data from project
   begin
      FileObj:=ClassFile.Create;
      // set mask settings
      StatusLB.Items.Add(StrfileObj.Assign(26));  // reading data from project
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      CheckProjectionSettings;

      Bytes2Work:=CalculateElementsToExport; Bytesworked:=0;
      if Bytes2Work = 0 then
      begin
         StatusLB.Items.Add(StrfileObj.Assign(37));  // Warning: no data to be exported!
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         aktiv:=false;
         FileObj.Destroy;
         Self.Caption:=StrFileObj.Assign(1);           { PROGIS ACAD-DXF Export 2000 }
         OkBtn.Enabled:=true;
         CancelBtn.Enabled:=false;
         SaveReportBtn.Enabled:=true;
      end
      else
      begin
         FileObj.OpenDxfFile(DxfFilename);
         Pass:=1;
         ExecutePass;
      end;
      exit;
   end;

   if AktPass = 2 then // write dxf file
   begin
      // set mask settings
      StatusLB.Items.Add(StrfileObj.Assign(27)+ DxfFilename);  // writing data to file:
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      // write header
      FileObj.WriteDXFHeaderSection(OSInfo.WingisDir + 'Header.inc');
      // write layertable
      FileObj.WriteDXFLayerTable;

      // write style block
      FileObj.WriteDXFHeaderSection(OSInfo.WingisDir + 'Styles.inc');

      // write block section
      FileObj.WriteDXFBlockSection;

      // write entity section
      FileObj.WriteDXFEntitySection;
      Bytes2work:=FileObj.GetMaxEntities; Bytesworked:=0;

      Pass:=2;
      ExecutePass;
      exit;
   end;

   if AktPass = 3 then // end of export
   begin
      // set mask settings
      StatusLB.Items.Add(StrfileObj.Assign(28));  // file has been successful exported
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      aktiv:=false;
      FileObj.CloseAllFiles;
      FileObj.Destroy;
      Self.Caption:=StrFileObj.Assign(1);           { PROGIS ACAD-DXF Export 2000 }
      OkBtn.Enabled:=true;
      CancelBtn.Enabled:=false;
      SaveReportBtn.Enabled:=true;
   end;
end;

procedure TMainWnd.Showpercent;
var Prozent:integer;
begin
   if Bytes2work > 0 then Prozent:= Round (Bytesworked / Bytes2work * 100 ) else Prozent:= 100;
   ProzessBalken.Progress :=Prozent;
   ProzessBalken.Update;
   Application.Processmessages;
end;

{*******************************************************************************
* PROZEDUR : ExecutePass                                                       *
********************************************************************************
* Prozedur die zur Abarbeitung eines Konvertierungsschritts dient              *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure TMainWnd.ExecutePass;
var MaxItems:longint;
    itemcount:longint;
begin
   if Pass = 1 then { Einlesen der Daten  }
   begin
      if Bytes2work > 0 then
      begin
         ReadSymbolLibrary;
         ReadElement;
      end;
      if (Bytesworked >= Bytes2work) and (Abort = false) then begin RunPass(2); exit; end;
      if Abort = true then exit;
   end;

   if Pass = 2 then { Schreiben der DXF-Entities }
   begin
      if Bytes2work > 0 then
      begin
         { Zuerst werden die Punkte geschrieben }
         MaxItems:=FileObj.GetMaxPoints; itemcount:=0;
         if MaxItems > 0 then
         repeat
            FileObj.WritePointEntitie;
            Bytesworked:=Bytesworked + 1;
            itemcount:=itemcount + 1;
            ShowPercent;
         until (itemcount  >= MaxItems) or (Abort = true);

         { Nun werden die Fl�chen geschreiben }
         MaxItems:=FileObj.GetMaxAreas; itemcount:=0;
         if (MaxItems > 0) and (not Abort) then
         repeat
            FileObj.WriteAreaEntitie;
            Bytesworked:=Bytesworked + 1;
            itemcount:=itemcount + 1;
            ShowPercent;
         until (itemcount  >= MaxItems) or (Abort = true);

         { Nun werden die Linien geschreiben }
         MaxItems:=FileObj.GetMaxLines; itemcount:=0;
         if (MaxItems > 0) and (not Abort) then
         repeat
            FileObj.WriteLineEntitie;
            Bytesworked:=Bytesworked + 1;
            itemcount:=itemcount + 1;
            ShowPercent;
         until (itemcount  >= MaxItems) or (Abort = true);

         { Nun werden die Circles geschrieben }
         MaxItems:=FileObj.GetMaxCircles; itemcount:=0;
         if (MaxItems > 0) and (not Abort) then
         repeat
            FileObj.WriteCircleEntitie;
            Bytesworked:=Bytesworked + 1;
            itemcount:=itemcount + 1;
            ShowPercent;
         until (itemcount  >= MaxItems) or (Abort = true);

         { Nun werden die Texte geschrieben }
         MaxItems:=FileObj.GetMaxTexts; itemcount:=0;
         if (MaxItems > 0) and (not Abort) then
         repeat
            FileObj.WriteTextEntitie;
            Bytesworked:=Bytesworked + 1;
            itemcount:=itemcount + 1;
            ShowPercent;
         until (itemcount  >= MaxItems) or (Abort = true);

         { Nun werden die Symbole geschrieben }
         MaxItems:=FileObj.GetMaxSymbols; itemcount:=0;
         if (MaxItems > 0) and (not Abort) then
         repeat
            FileObj.WriteSymbolEntitie;
            Bytesworked:=Bytesworked + 1;
            itemcount:=itemcount + 1;
            ShowPercent;
         until (itemcount  >= MaxItems) or (Abort = true);
         if Abort = false then
            FileObj.WriteEntityEnd;
      end;
      if (Bytesworked >= Bytes2work) and (Abort = false) then begin RunPass(3); exit; end;
      if Abort = true then exit;
   end;
end;

procedure TMainWnd.StartExport;
begin
   if ExitNormal then
   begin
      SelDxfWnd.GetDxfFile(DxfFilename);
      // start export
      RunPass(1);
   end;
end;

procedure TMainWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TMainWnd.QuittimerTimer(Sender: TObject);
begin
   MainWnd.close;
end;

procedure TMainWnd.OKBtnClick(Sender: TObject);
begin
   CloseTimer.Enabled:=true;
end;

procedure TMainWnd.CloseTimerTimer(Sender: TObject);
begin
   MainWnd.Close;
   SelDxfWnd.Close;
   SourceDocument.FinishImpExpRoutine(ExitNormal,'EXPORTMODULE');
   CloseTimer.Enabled:=false;
end;

procedure TMainWnd.FormDestroy(Sender: TObject);
begin
   StrFileObj.Destroy;
end;

procedure TMainWnd.CancelBtnClick(Sender: TObject);
begin
   StatusLB.Items.Add(StrfileObj.Assign(29));  // Warning: Export canceled
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;

   aktiv:=false; Abort:=true;
   CancelBtn.Enabled:=false;
   SaveReportBtn.enabled:=true;
   OkBtn.Enabled:=true;
   FileObj.CloseAllFiles;
   FileObj.Destroy;
   if SelDxfWnd.SelectedCB.checked then
      SourceDocument.DestroySelectedList;
end;

procedure TMainWnd.SaveReportBtnClick(Sender: TObject);
var Datei:Textfile;
    Zeile:string;
    i:integer;
begin
   if SaveDialogRep.Execute then
   begin
      Assignfile(Datei,SaveDialogRep.Filename);
      Rewrite(Datei);
      for i:=0 to StatusLB.Items.Count-1 do
      begin
         Zeile:=StatusLB.items[i];
         Writeln(Datei,Zeile);
      end;
      Closefile(Datei);
   end;
end;

procedure TMainWnd.SetupWindowTimerTimer(Sender: TObject);
var IdbProcessMask:integer;
begin
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,SelDxfWnd.winleft,SelDxfWnd.wintop,SelDxfWnd.winwidth, SelDxfWnd.winheight,SWP_SHOWWINDOW);
   MainWnd.Height:=SelDxfWnd.winheight;
   MainWnd.Width:=SelDxfWnd.winwidth;
   Self.Repaint;
   SetupWindowTimer.Enabled:=false;
   IdbProcessMask:=SourceDocument.IdbProcessMask;
   SourceDocument.IdbProcessMask:=0;
   StartExport;
   SourceDocument.IdbProcessMask:=IdbProcessMask;
end;

function TMainWnd.MyFloatToStr(value :double):string;
var retVal:string;
    dummy :PCHAR;
    i:integer;
begin
   retVal:=floattostr(value);
   if Pos(',',retVal) <> 0 then
   begin
      // now check if there is a comma inside the string value
      dummy:=stralloc(length(retVal)+1); strpcopy(dummy, retVal);
      for i:=0 to strlen(dummy) do
          if (dummy[i] = ',') then dummy[i]:='.';
      retVal:=strpas(dummy);
      strdispose(dummy);
   end;
   result:=retVal;
end;

procedure TMainWnd.ConvertMultiLineText(var aText:string);
var dummy:pchar;
    i:integer;
begin
   // items of multiline texts are seperated with <CR> <LF>
   // these have to be exchanged by space characers
   dummy:=stralloc(length(aText)+1);
   strpcopy(dummy,aText);
   for i:=0 to length(aText)-1 do
   begin
      if (ord(dummy[i]) = 10) or (ord(dummy[i]) = 13) then dummy[i]:=' ';
   end;
   aText:=strpas(dummy);
   strdispose(dummy);
end;

procedure TMainWnd.AxGisProjectionError(Sender: TObject;
  const Info: WideString);
begin
   if Active = FALSE then
   begin
      ShowMessage(Info);
   end
   else
   begin
      StatusLb.Items.Add(Info);
      StatusLB.Items.Add(StrfileObj.Assign(29));  // Warning: Export canceled
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      aktiv:=false; Abort:=true;
      CancelBtn.Enabled:=false;
      SaveReportBtn.enabled:=true;
      OkBtn.Enabled:=true;
      FileObj.CloseAllFiles;
      FileObj.Destroy;
      if SelDxfWnd.SelectedCB.checked then
         SourceDocument.DestroySelectedList;
   end;
end;

procedure TMainWnd.AxGisProjectionWarning(Sender: TObject;
  const Info: WideString);
begin
   if Active=false then
   begin
      ShowMessage(Info);
   end
   else
   begin
      StatusLb.Items.Add(Info);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
   end;
end;

procedure TMainWnd.FormShow(Sender: TObject);
begin
   AxGisProjection.visible:=FALSE;
end;

end.
