unit Files;

interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Math, Struct, Strfile;


type
ClassFile = class
   public
      { public Definitionen }
      constructor Create;
      destructor Destroy;
      procedure OpenDxfFile(Dateiname:string);
      procedure CloseallFiles;
      procedure WriteDXF(Zeile:string);
      procedure WriteDXFHeaderSection(Headerfilename:string);
      procedure WriteDXFLayerTable;
      procedure WriteDXFBlockSection;
      procedure WriteDXFEntitySection;
      procedure WriteEntityEnd;
      procedure WritePointEntitie;
      procedure WriteAreaEntitie;
      procedure WriteLineEntitie;
      procedure WriteCircleEntitie;
      procedure WriteTextEntitie;
      procedure WriteSymbolEntitie;

      function GetMaxLayers:longint;
      function GetMaxPoints:longint;
      function GetMaxAreas:longint;
      function GetMaxLines:longint;
      function GetMaxCircles:longint;
      function GetMaxTexts:longint;
      function GetMaxSymbols:longint;
      function GetMaxEntities:longint;
      function GetCurrentLayername:string;

      procedure New_Layer(Layername:string;Line_Type:string;Line_Color:string;Fill_Type:string;Fill_Color:string);
      procedure New_Point(X:double;Y:double;IsSymbol:boolean);
      procedure New_Line(IsSymbol:boolean);
      procedure InsertPointToLine(X:double;Y:double;IsSymbol:boolean);
      procedure New_Area(IsSymbol:boolean);
      procedure InsertPointToArea(X:double;Y:double;IsSymbol:boolean);
      procedure New_Text(X:double;Y:double;Text:string;Font:string;size:double;Angle:integer;IsSymbol:boolean);
      procedure New_Circle(X:double;Y:double;Radius:integer;IsSymbol:boolean);
      procedure New_SymbolRef(X:double;Y:double;SymName:string);
      procedure New_Symbol(X:double;Y:double;SymName:string;Size:double;Angle:double;IsSymbol:boolean);

      function CheckSymbolIsEmpty:boolean;
      function CheckSymbolRefExists(SymName:string):boolean;
   private
      { private Definitionen }
      AreaObj        :ClassArea;
      PointObj       :ClassPoint;
      LineObj        :ClassLine;
      LayerObj       :ClassLayer;
      TextObj        :ClassText;
      CircleObj      :ClassCircle;
      SymbolRefObj   :ClassSymbolRef;
      SymbolObj      :ClassSymbol;

end;
implementation
uses MainDlg;

var  DxfDatei       :Textfile;

constructor ClassFile.Create;
begin
   AreaObj:=ClassArea.Create;
   PointObj:=ClassPoint.Create;
   LineObj:=ClassLine.Create;
   TextObj:=ClassText.Create;
   CircleObj:=ClassCircle.Create;
   LayerObj:=ClassLayer.Create;
   SymbolRefObj:=ClassSymbolRef.Create;
   SymbolObj:=ClassSymbol.Create;
end;

destructor ClassFile.Destroy;
begin
   AreaObj.Destroy;
   PointObj.Destroy;
   LineObj.Destroy;
   TextObj.Destroy;
   CircleObj.Destroy;
   LayerObj.Destroy;
   SymbolRefObj.Destroy;
   SymbolObj.Destroy;
end;

procedure ClassFile.OpenDxfFile(Dateiname:string);
begin
   Assignfile(DxfDatei,Dateiname);
   Rewrite(DxfDatei);
end;

{*******************************************************************************
* PROZEDUR : CloseallFiles                                                     *
********************************************************************************
* Prozedur dient zum Schliessen aller Files.                                   *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure ClassFile.CloseallFiles;
begin
   {$i-}
   Closefile(DxfDatei);
   {$i+}
end;

procedure ClassFile.WriteDXF(Zeile:string);
begin
   writeln(DxfDatei,Zeile);
end;

procedure ClassFile.WriteDXFHeaderSection(Headerfilename:string);
var Headerdatei:Textfile;
    Zeile :string;
begin
   Assignfile(Headerdatei,Headerfilename);
   Reset(HeaderDatei);
   while (not Eof(Headerdatei)) do
   begin
      readln(Headerdatei,Zeile);
      writeln(DXFDatei,Zeile);
   end;
   Closefile(Headerdatei);
end;

procedure ClassFile.WriteDXFEntitySection;
begin
   writeln(DXFDatei,'SECTION');
   writeln(DXFDatei,'  2');
   writeln(DXFDatei,'ENTITIES');
   writeln(DXFDatei,'  0');
end;

procedure ClassFile.WriteEntityEnd;
begin
   writeln(DXFDatei,'ENDSEC');
   writeln(DXFDatei,'  0');
   writeln(DXFDatei,'EOF');
end;

function ClassFile.GetMaxEntities:longint;
var retVal:longint;
begin
   retVal:=0;
   retVal:=retVal +  AreaObj.GetAnzahl;
   retVal:=retVal +  PointObj.GetAnzahl;
   retVal:=retVal +  LineObj.GetAnzahl;
   retVal:=retVal +  TextObj.GetAnzahl;
   retVal:=retVal +  CircleObj.GetAnzahl;
   retVal:=retVal +  SymbolObj.GetAnzahl;
   result:=retVal;
end;

function ClassFile.GetMaxTexts:longint;
begin
   result:=TextObj.GetAnzahl;
   TextObj.ResetCurrent;
end;

function ClassFile.GetMaxSymbols:longint;
begin
   result:=SymbolObj.GetAnzahl;
   SymbolObj.ResetCurrent;
end;

function ClassFile.GetMaxPoints:longint;
begin
   result:=PointObj.GetAnzahl;
   PointObj.ResetCurrent;
end;

function ClassFile.GetMaxCircles:longint;
begin
   result:=CircleObj.GetAnzahl;
   CircleObj.ResetCurrent;
end;

function ClassFile.GetMaxAreas:longint;
begin
   result:=AreaObj.GetAnzahl;
   AreaObj.ResetCurrent;
end;

function ClassFile.GetMaxLines:longint;
begin
   result:=LineObj.GetAnzahl;
   LineObj.ResetCurrent;
end;

procedure ClassFile.WritePointEntitie;
begin
   PointObj.WriteData(PointObj.GetCurrent);
   PointObj.SetCurrentNext;
end;

procedure ClassFile.WriteAreaEntitie;
begin
   AreaObj.WriteData(AreaObj.GetCurrent);
   AreaObj.SetCurrentNext;
end;

procedure ClassFile.WriteLineEntitie;
begin
   LineObj.WriteData(LineObj.GetCurrent);
   LineObj.SetCurrentNext;
end;

procedure ClassFile.WriteCircleEntitie;
begin
   CircleObj.WriteData(CircleObj.GetCurrent);
   CircleObj.SetCurrentNext;
end;

procedure ClassFile.WriteTextEntitie;
begin
   TextObj.WriteData(TextObj.GetCurrent);
   TextObj.SetCurrentNext;
end;

procedure ClassFile.WriteSymbolEntitie;
begin
   SymbolObj.WriteData(SymbolObj.GetCurrent);
   SymbolObj.SetCurrentNext;
end;

procedure ClassFile.WriteDXFLayerTable;
var i:integer;
begin
   writeln(DXFDatei,'TABLE');
   writeln(DXFDatei,'  2');
   writeln(DXFDatei,'LAYER');
   writeln(DXFDatei,' 70');
   writeln(DXFDatei,'    71');
   writeln(DXFDatei,'  0');
   LayerObj.ResetCurrent;
   for i:=1 to LayerObj.GetAnzahl do
   begin
      LayerObj.WriteLayerTable;
      LayerObj.SetCurrentnext;
   end;
   writeln(DXFDatei,'ENDTAB');
   writeln(DXFDatei,'  0');
end;

procedure ClassFile.WriteDXFBlockSection;
var i:integer;
    Cur_Point:p_Point_record;
    Cur_Line:p_Line_record;
    Cur_Area:p_Area_record;
    Cur_Circle:p_Circle_record;
    Cur_Text:p_Text_record;
begin
   if SymbolRefObj.GetAnzahl = 0 then exit;
   writeln(DXFDatei,'SECTION');
   writeln(DXFDatei,'  2');
   writeln(DXFDatei,'BLOCKS');
   writeln(DXFDatei,'  0');
   SymbolRefObj.Resetcurrent;
   for i:=1 to SymbolRefObj.GetAnzahl do
   begin
      SymbolRefObj.WriteSymbolDefHeader;
      { Nun werden die Punkte die im Symbol vorkommen geschrieben }
      Cur_Point:=SymbolRefObj.GetPoints;
      while (Cur_Point <> nil) do
      begin
         PointObj.WriteData(Cur_Point);
         Cur_Point:=Cur_Point^.next;
      end;

      { Nun werden die Linien die im Symbol vorkommen geschrieben }
      Cur_Line:=SymbolRefObj.GetLines;
      while (Cur_Line <> nil) do
      begin
         LineObj.WriteData(Cur_Line);
         Cur_Line:=Cur_Line^.next;
      end;

      { Nun werden die Fl�chen die im Symbol vorkommen geschrieben }
      Cur_Area:=SymbolRefObj.GetAreas;
      while (Cur_Area <> nil) do
      begin
         AreaObj.WriteData(Cur_Area);
         Cur_Area:=Cur_Area^.next;
      end;

      { Nun werden die Circles die im Symbol vorkommen geschrieben }
      Cur_Circle:=SymbolRefObj.GetCircles;
      while (Cur_Circle <> nil) do
      begin
         CircleObj.WriteData(Cur_Circle);
         Cur_Circle:=Cur_Circle^.next;
      end;
      { Nun werden die Texte die im Symbol vorkommen geschrieben }
      Cur_Text:=SymbolRefObj.GetTexts;
      while (Cur_Text <> nil) do
      begin
         TextObj.WriteData(Cur_Text);
         Cur_Text:=Cur_Text^.next;
      end;

      SymbolRefObj.WriteSymbolDefBottom;
      SymbolRefObj.SetCurrentnext;
   end;

   // write end of section Blocks
   writeln(DXFDatei,'ENDSEC');
   writeln(DXFDatei,'  0');
end;

{*******************************************************************************
* FUNKTION : GetMaxLayers                                                      *
********************************************************************************
* Funktion dient zum Ermitteln der Anzahl der zu bearbeitenden Layer           *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Anzahl der zu bearbeitenden Layer.                             *
********************************************************************************}
function ClassFile.GetMaxLayers:longint;
begin
   LayerObj.ResetCurrent;
   Result:=LayerObj.GetAnzahl;
end;

{*******************************************************************************
* FUNKTION : GetCurrentLayername                                               *
********************************************************************************
* Funktion dient zum Ermitteln des Namens des aktuellen Layers.                *
*                                                                              *
* PARAMETER: keine                                                             *
*                                                                              *
* R�ckgabewert: Name des aktuell bearbeiteten Layers.                          *
********************************************************************************}
function ClassFile.GetCurrentLayername:string;
begin
   Result:=LayerObj.GetCurrentLayername;
end;

procedure ClassFile.New_SymbolRef(X:double;Y:double;SymName:string);
begin
   SymbolRefObj.Neu (X,Y,Symname);
end;

procedure ClassFile.New_Layer(
                                 Layername:string;
                                 Line_Type:string;
                                 Line_Color:string;
                                 Fill_Type:string;
                                 Fill_Color:string
                              );
begin
   LayerObj.Neu(Layername,Line_Type,Line_Color,Fill_Type,Fill_Color);
end;

function ClassFile.CheckSymbolIsEmpty:boolean;
begin
   result:=SymbolRefObj.CheckSymbolIsEmpty;
end;

function ClassFile.CheckSymbolRefExists(SymName:string):boolean;
begin
   result:=SymbolRefObj.CheckSymbolRefExists(SymName);
end;

procedure ClassFile.New_Point(X:double;Y:double;IsSymbol:boolean);
begin
   if IsSymbol then SymbolRefObj.New_Point(X/MainWnd.SymbolScale,Y/MainWnd.SymbolScale)
   else PointObj.Neu(X,Y,LayerObj.GetCurrent);
end;

procedure ClassFile.New_Line(IsSymbol:boolean);
begin
   if IsSymbol then SymbolRefObj.New_Line
   else LineObj.Neu(LayerObj.GetCurrent);
end;

procedure ClassFile.InsertPointToLine(X:double;Y:double;IsSymbol:boolean);
begin
   if IsSymbol then SymbolRefObj.InsertPointToLine(X/MainWnd.SymbolScale,Y/MainWnd.SymbolScale)
   else LineObj.InsertPoint(X,Y);
end;

procedure ClassFile.New_Area(IsSymbol:boolean);
begin
   if IsSymbol then SymbolRefObj.New_Area
   else AreaObj.Neu(LayerObj.GetCurrent);
end;

procedure ClassFile.InsertPointToArea(X:double;Y:double;IsSymbol:boolean);
begin
   if IsSymbol then SymbolRefObj.InsertPointToArea(X/MainWnd.SymbolScale,Y/MainWnd.SymbolScale) else
   AreaObj.InsertPoint(X,Y);
end;

procedure ClassFile.New_Text(X:double;Y:double;Text:string;Font:string;size:double;angle:integer;IsSymbol:boolean);
begin
   if IsSymbol then SymbolRefObj.New_Text(X/MainWnd.SymbolScale,Y/MainWnd.SymbolScale,X/MainWnd.SymbolScale,Y/MainWnd.SymbolScale,Text,Font,size,angle) else
   TextObj.Neu(X,Y,X,Y,Text,Font,size,angle,LayerObj.GetCurrent);
end;

procedure ClassFile.New_Symbol(X:double;Y:double;SymName:string;Size:double;Angle:double;IsSymbol:boolean);
begin
   SymbolObj.Neu(X,Y,SymName,Size,Angle,LayerObj.GetCurrent);
end;

procedure ClassFile.New_Circle(X:double;Y:double;Radius:integer;IsSymbol:boolean);
begin
   if IsSymbol then SymbolRefObj.New_Circle(X/MainWnd.SymbolScale,Y/MainWnd.SymbolScale,Radius div MainWnd.SymbolScale)
   else CircleObj.Neu(X,Y,Radius,LayerObj.GetCurrent);
end;

end.
