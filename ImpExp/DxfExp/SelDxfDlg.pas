unit SelDxfDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StrFile, StdCtrls, Buttons, FileCtrl, ComCtrls, ExtCtrls, WinOSInfo;

type
  TSelDxfWnd = class(TForm)
    PageControl: TPageControl;
    TabSheetSource: TTabSheet;
    LayerLB: TListBox;
    TabSheetDest: TTabSheet;
    LayerCB: TCheckBox;
    OKBtn: TButton;
    CancelBtn: TButton;
    GeoBtn: TButton;
    Panel2: TPanel;
    Label1: TLabel;
    Panel1: TPanel;
    DxfLabel: TLabel;
    DxfBtn: TButton;
    SaveDialogdxf: TSaveDialog;
    SelectedCB: TCheckBox;
    SetupWindowTimer: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure LayerCBClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GeoBtnClick(Sender: TObject);
    procedure DxfBtnClick(Sender: TObject);
    procedure SelectedCBClick(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure LayerLBClick(Sender: TObject);
  private
    { Private declarations }
    FirstActivate:boolean;
    DestinationDxfFile:string;
    procedure CheckWindow;
  public
    { Public declarations }
    winheight:integer;
    winwidth :integer;
    wintop   :integer;
    winleft  :integer;
    procedure GetDxfFile(var Dxffile:string);
    procedure CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
    procedure SetupForBatch;
    procedure SetExpSettings(DxfFile:string; ExpMode:integer);
    procedure AddExpLayer(ExpLayer:string);
  end;

var
  SelDxfWnd     : TSelDxfWnd;
  StrFileObj    : ClassStrfile;

implementation

uses Maindlg;

{$R *.DFM}

procedure TSelDxfWnd.FormCreate(Sender: TObject);
var myScale:double;
    dummy  :double;
    dummystr:string;
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   Self.Caption:=StrfileObj.Assign(1);                 { PROGIS ACAD-DXF Export 2000 }
   TabsheetSource.Caption:=StrfileObj.Assign(2);       { Source Project              }
   TabsheetDest.Caption:=StrfileObj.Assign(3);         { Destination ACAD-DXF        }
   LayerCB.Caption:=StrfileObj.Assign(4);              { Export only selected layers }
   SelectedCB.Caption:=StrfileObj.Assign(38);          { Export only selected objects }

   OkBtn.Caption:=StrfileObj.Assign(5);                { Ok }
   CancelBtn.Caption:=StrfileObj.Assign(6);            { Cancel }
   GeoBtn.Caption:=StrfileObj.Assign(7);               { Projection }
   Label1.Caption:=StrfileObj.Assign(8);               { Dxf-File: }

   LayerLB.enabled:=false;

   DestinationDxfFile:='';
   dxfLabel.Caption:='';
   FirstActivate:=true;

   Self.Height:=1;
   Self.Width:=1;

   myScale:=1; //100000 / Trunc(9600000/(Self.PixelsPerInch));
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
   
   // now adapt winheigt and winwidth
   dummy:=376*myScale; dummystr:=floattostr(dummy);
   if (Pos('.',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos('.',dummystr)-1);
   if (Pos(',',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos(',',dummystr)-1);
   winheight:=strtoint(dummystr);
   dummy:=462*myScale; dummystr:=floattostr(dummy);
   if (Pos('.',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos('.',dummystr)-1);
   if (Pos(',',dummystr) <> 0) then dummystr:=copy(dummystr,1,Pos(',',dummystr)-1);
   winwidth:=strtoint(dummystr);
end;

procedure TSelDxfWnd.OKBtnClick(Sender: TObject);
begin
   MainWnd.ExitNormal:=true;
   Self.Close;
   MainWnd.Show;
end;

procedure TSelDxfWnd.CancelBtnClick(Sender: TObject);
begin
   MainWnd.ExitNormal:=false;
   MainWnd.CloseTimer.Enabled:=true;
end;

procedure TSelDxfWnd.GetDxfFile(var Dxffile:string);
begin
   DxfFile:=DestinationDxfFile;
end;

procedure TSelDxfWnd.LayerCBClick(Sender: TObject);
begin
   if LayerCB.Checked then
   begin
      LayerLB.enabled:=true;
      SelectedCB.Checked:=false;
   end
   else
   begin
      LayerLB.enabled:=false;
   end;
   CheckWindow;
end;

procedure TSelDxfWnd.CheckWindow;
var allok:boolean;
    i:integer;
begin
   allok:=true;
   { Procedure checks all settings in the window }
   if DestinationDxfFile = '' then allok:=false;
   if allok and LayerCb.Checked then
   begin
      // check if there is a layer selected
      allok:=false;
      for i:=0 to LayerLb.Items.count-1 do
      begin
         if LayerLb.Selected[i] then
         begin
            allok:=TRUE;
            break;
         end;
      end;
   end;
   if allok then OKBtn.Enabled:=true
   else OkBtn.Enabled:=false;
end;

procedure TSelDxfWnd.FormActivate(Sender: TObject);
var Layername:string;
    layercnt:integer;
    ALayer:Variant;
begin
   if FirstActivate then
   begin
      // fill layerlist
      LayerLB.Items.Clear;
      for layercnt:=0 to MainWnd.SourceDocument.Layers.Count-1 do
      begin
         ALayer:=MainWnd.SourceDocument.Layers.Items[layercnt];
         Layername:=ALayer.Layername;
         LayerLb.Items.Add(Layername);
      end;
      MainWnd.AxGisProjection.WorkingPath:=OSInfo.WingisDir;
      MainWnd.AxGisProjection.LngCode:=MainWnd.LngCode;
      MainWnd.SetProjectionSettings;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TSelDxfWnd.GeoBtnClick(Sender: TObject);
begin
   MainWnd.AxGisProjection.SetupByDialog;
end;


procedure TSelDxfWnd.DxfBtnClick(Sender: TObject);
begin
   if SaveDialogdxf.Execute then
   begin
      DestinationDxfFile:=SaveDialogdxf.Filename;
      DxfLabel.Caption:=DestinationDxfFile;
   end;
   CheckWindow;
end;

procedure TSelDxfWnd.SelectedCBClick(Sender: TObject);
begin
   if SelectedCB.Checked then
      LayerCB.Checked:=false;
   CheckWindow;
end;

procedure TSelDxfWnd.CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
begin
   wintop:=(WHEIGHT div 2) - (winheight div 2);
   winleft:=(WWIDTH div 2) - (winwidth div 2);
end;

procedure TSelDxfWnd.SetupWindowTimerTimer(Sender: TObject);
begin
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,winleft,wintop,winwidth, winheight,SWP_SHOWWINDOW);
   Self.Height:=winheight;
   Self.Width:=winwidth;
   if MainWnd.SourceDocument.NrOfSelectedObjects = 0 then
      SelectedCb.Visible:=FALSE;
   Self.Repaint;
   SetupWindowTimer.Enabled:=false;
end;

// procedure is used to setup the SelAscWnd for batch mode
procedure TSelDxfWnd.SetupForBatch;
var Layername:string;
    layercnt:integer;
    ALayer:Variant;
begin
   if FirstActivate then
   begin
      // fill layerlist
      LayerLB.Items.Clear;
      for layercnt:=0 to MainWnd.SourceDocument.Layers.Count-1 do
      begin
         ALayer:=MainWnd.SourceDocument.Layers.Items[layercnt];
         Layername:=ALayer.Layername;
         LayerLb.Items.Add(Layername);
      end;
      MainWnd.AxGisProjection.WorkingPath:=OSInfo.WingisDir;
      MainWnd.AxGisProjection.LngCode:=MainWnd.LngCode;
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
   end;
end;

procedure TSelDxfWnd.SetExpSettings(DxfFile:string; ExpMode:integer);
begin
   DestinationDxfFile:=DxfFile;
   if ExpMode = 2 then // export only selected objects
      SelectedCB.Checked:=true;
   if ExpMode = 1 then // export only selected layers
      LayerCB.Checked:=true;
end;

procedure TSelDxfWnd.AddExpLayer(ExpLayer:string);
var i:integer;
begin
   // find the Layername in the list and select the item
   for i:=0 to LayerLB.Items.Count-1 do
   begin
      if (LayerLB.Items[i] = ExpLayer) then
      begin
         LayerLB.Selected[i]:=true;
      end;
   end;
end;

procedure TSelDxfWnd.LayerLBClick(Sender: TObject);
begin
   CheckWindow;
end;

end.
