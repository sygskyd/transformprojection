library dxfexp;

{ Wichtiger Hinweis zur DLL-Speicherverwaltung: Sharemem mu� die erste
 Unit in der uses-Klausel Ihrer Bibliothek und Ihres Projekts sein,
 wenn Ihre DLL Prozeduren und Funktionen exportiert, die
 Strings als Parameter oder Funktionsergebnisse �bergeben. Dies
 betrifft alle Strings, die an Ihre DLL oder von Ihrer DLL �bergeben werden--selbst diejenigen,
 die sich in Records oder Klassen befinden. Sharemem ist die Schnittstelle zu
 BORLNDMM.DLL, die den gemeinsamen Zugriff auf den Speicher verwaltet; DELPHIMM.DLL
 mu� mit Ihrer DLL weitergegeben werden. Wenn Sie BORLNDMM.DLL nicht verwenden m�chten, m�ssen
 Strings als PChar oder ShortString �bergeben werden.}

uses
  Forms,
  SysUtils,
  Classes,
  Dialogs,
  Files in 'Files.pas',
  Maindlg in 'maindlg.pas' {MainWnd},
  Strfile in 'Strfile.pas',
  struct in 'struct.pas',
  comobj,
  SelDxfDlg in 'SelDxfDlg.pas' {SelDxfWnd};

{$R *.RES}

{*******************************************************************************
* PROCEDURE * DXFEXPORT                                                        *
********************************************************************************
* Procedure is used to export GIS data into ACAD format                        *
*                                                                              *
********************************************************************************}
procedure DXFEXPORT(DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var StrfileObj:ClassStrfile;
begin
   // create MainWnd
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.App:=AXHANDLE;
   MainWnd.LngCode:=LNGCODE;
   MainWnd.Regmode:=REGMODE;
   MainWnd.SourceDocument:=SOURCE;
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.CreateStringFile;

   // create SelDxfWnd
   SelDxfWnd:=TSelDxfWnd.Create(nil);
   SelDxfWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);

   if (REGMODE = 2) then // check if in Demo-Mode
      Showmessage(StrfileObj.Assign(31)); // PROGIS ACAD-DXF export in demo mode - only 100 objects will be exported

   if (REGMODE = 1) or (REGMODE = 2) then   // start only when registered or demo-mode
   begin
      SelDxfWnd.Show;
   end
   else
      Showmessage(StrfileObj.Assign(30)); // PROGIS ACAD-DXF export not registered
end;

{*******************************************************************************
* PROCEDURE * CREATEEXP                                                        *
********************************************************************************
* Procedure is used to prepare an dxf-export for Batchmode                     *
*                                                                              *
********************************************************************************}
procedure CREATEEXP(DIR:PCHAR;var AXHANDLE:VARIANT;var SOURCE:VARIANT;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
begin
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.App:=AXHANDLE;
   MainWnd.SourceDocument:=SOURCE;
   MainWnd.RegMode:=REGMODE;
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.CreateStringFile;

   // create SelDxfWnd
   SelDxfWnd:=TSelDxfWnd.Create(nil);
   SelDxfWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);
   SelDxfWnd.SetupForBatch;
end;

{*******************************************************************************
* PROCEDURE * SETEXPSETTINGS                                                   *
********************************************************************************
* Procedure is used to set export project name, export mode and list of layers *
* that should be exported.                                                     *
*                                                                              *
* PARAMETERS: EXPPROJ     -> Name of the file into that should be exported     *
*             EXPPROJMODE -> Mode how it should be exported.                   *
*                            0 -> Export all layers                            *
*                            1 -> Export selected layers                       *
*                            2 -> Export selected objects                      *
*                                                                              *
********************************************************************************}
procedure SETEXPSETTINGS(EXPPROJ:PCHAR; EXPPROJMODE:INTEGER);stdcall;
begin
   SelDxfWnd.SetExpSettings(strpas(EXPPROJ), EXPPROJMODE);
end;

// procedure is used to add a layer that should be exported
// if only selected layers should be exported
procedure ADDEXPLAYER(ALAYER:PCHAR);stdcall;
begin
   SelDxfWnd.AddExpLayer(strpas(ALAYER));
end;

// procedure is used to set database-settings
procedure SETDBSETTINGS(SOURCEDBMODE:INTEGER; SOURCEDBNAME:PCHAR; SOURCEDBTABLE:PCHAR;
                        DESTDBMODE  :INTEGER; DESTDBNAME  :PCHAR; DESTDBTABLE  :PCHAR);stdcall;
begin
   // dxf-export supports no handling of database
end;

// procedure is used to set projection for batch mode
procedure SETPROJECTION(SOURCECOORDINATESYSTEM:INTEGER; // source projection
                        SOURCEPROJECTION      :PCHAR;
                        SOURCEDATE            :PCHAR;
                        SOURCEPROJSETTINGS    :PCHAR;
                        SOURCEXOFFSET         :DOUBLE;
                        SOURCEYOFFSET         :DOUBLE;
                        SOURCESCALE           :DOUBLE;
                        TARGETCOORDINATESYSTEM:INTEGER; // target projection
                        TARGETPROJECTION      :PCHAR;
                        TARGETDATE            :PCHAR;
                        TARGETPROJSETTINGS    :PCHAR;
                        TARGETXOFFSET         :DOUBLE;
                        TARGETYOFFSET         :DOUBLE;
                        TARGETSCALE           :DOUBLE);stdcall;
begin
      // source projection
   MainWnd.AxGisProjection.SourceCoordinateSystem:=SOURCECOORDINATESYSTEM;
   MainWnd.AxGisProjection.SourceProjection:=strpas(SOURCEPROJECTION);
   MainWnd.AxGisProjection.SourceDate:=strpas(SOURCEDATE);
   MainWnd.AxGisProjection.SourceProjSettings:=strpas(SOURCEPROJSETTINGS);
   MainWnd.AxGisProjection.SourceXOffset:=SOURCEXOFFSET;
   MainWnd.AxGisProjection.SourceYOffset:=SOURCEYOFFSET;
   MainWnd.AxGisProjection.SourceScale:=SOURCESCALE;
   // target projection
   MainWnd.AxGisProjection.TargetCoordinateSystem:=TARGETCOORDINATESYSTEM;
   MainWnd.AxGisProjection.TargetProjection:=strpas(TARGETPROJECTION);
   MainWnd.AxGisProjection.TargetDate:=strpas(TARGETDATE);
   MainWnd.AxGisProjection.TargetProjSettings:=strpas(TARGETPROJSETTINGS);
   MainWnd.AxGisProjection.TargetXOffset:=TARGETXOFFSET;
   MainWnd.AxGisProjection.TargetYOffset:=TARGETYOFFSET;
   MainWnd.AxGisProjection.TargetScale:=TARGETSCALE;
end;

// function is used to execute an dxf-export
procedure EXECEXP;stdcall;
begin
   MainWnd.ExitNormal:=true;
   MainWnd.Show;
end;

function FREEDLL:boolean; stdcall;
var retVal:boolean;
begin
   retVal:=MainWnd.ExitNormal;
   MainWnd.Free;
   SelDxfWnd.Free;
   result:=retVal;
end;

exports DXFEXPORT, CREATEEXP, SETEXPSETTINGS, ADDEXPLAYER,
        SETDBSETTINGS, SETPROJECTION, EXECEXP, FREEDLL;

begin
end.
