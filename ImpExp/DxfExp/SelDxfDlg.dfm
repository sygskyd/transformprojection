object SelDxfWnd: TSelDxfWnd
  Left = 218
  Top = 150
  BorderStyle = bsDialog
  Caption = 'PROGIS ACAD-DXF Export 2000'
  ClientHeight = 349
  ClientWidth = 456
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 8
    Top = 8
    Width = 441
    Height = 305
    ActivePage = TabSheetSource
    TabIndex = 0
    TabOrder = 0
    object TabSheetSource: TTabSheet
      Caption = 'Source Project'
      ImageIndex = 1
      object LayerLB: TListBox
        Left = 4
        Top = 24
        Width = 169
        Height = 249
        DragMode = dmAutomatic
        ItemHeight = 13
        MultiSelect = True
        TabOrder = 0
        OnClick = LayerLBClick
      end
      object LayerCB: TCheckBox
        Left = 4
        Top = 0
        Width = 169
        Height = 17
        Caption = 'Export only selected layers'
        TabOrder = 1
        OnClick = LayerCBClick
      end
      object SelectedCB: TCheckBox
        Left = 176
        Top = 0
        Width = 249
        Height = 17
        Caption = 'Export only selected objects'
        TabOrder = 2
        OnClick = SelectedCBClick
      end
    end
    object TabSheetDest: TTabSheet
      Caption = 'Destination ACAD-DXF'
      ImageIndex = 1
      object Panel2: TPanel
        Left = 4
        Top = 0
        Width = 426
        Height = 41
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 14
          Width = 35
          Height = 13
          Caption = 'Dxf-file:'
        end
        object Panel1: TPanel
          Left = 72
          Top = 8
          Width = 321
          Height = 25
          TabOrder = 0
          object DxfLabel: TLabel
            Left = 8
            Top = 6
            Width = 42
            Height = 13
            Caption = 'DxfLabel'
          end
        end
        object DxfBtn: TButton
          Left = 395
          Top = 8
          Width = 25
          Height = 25
          Caption = '...'
          TabOrder = 1
          OnClick = DxfBtnClick
        end
      end
    end
  end
  object OKBtn: TButton
    Left = 240
    Top = 320
    Width = 93
    Height = 23
    Caption = 'OK'
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 348
    Top = 320
    Width = 101
    Height = 23
    Cancel = True
    Caption = 'Abbrechen'
    ModalResult = 2
    TabOrder = 2
    OnClick = CancelBtnClick
  end
  object GeoBtn: TButton
    Left = 8
    Top = 320
    Width = 93
    Height = 23
    Caption = 'Projection'
    Default = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = GeoBtnClick
  end
  object SaveDialogdxf: TSaveDialog
    DefaultExt = '*.dxf'
    FileName = '*.dxf'
    Filter = 'ACAD-DXF|*.dxf'
    Left = 372
    Top = 120
  end
  object SetupWindowTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = SetupWindowTimerTimer
    Left = 136
    Top = 312
  end
end
