 library dfkimp;

{ Wichtiger Hinweis zur DLL-Speicherverwaltung: Sharemem mu� die erste
 Unit in der uses-Klausel Ihrer Bibliothek und Ihres Projekts sein,
 wenn Ihre DLL Prozeduren und Funktionen exportiert, die
 Strings als Parameter oder Funktionsergebnisse �bergeben. Dies
 betrifft alle Strings, die an Ihre DLL oder von Ihrer DLL �bergeben werden--selbst diejenigen,
 die sich in Records oder Klassen befinden. Sharemem ist die Schnittstelle zu
 BORLNDMM.DLL, die den gemeinsamen Zugriff auf den Speicher verwaltet; DELPHIMM.DLL
 mu� mit Ihrer DLL weitergegeben werden. Wenn Sie BORLNDMM.DLL nicht verwenden m�chten, m�ssen
 Strings als PChar oder ShortString �bergeben werden.}

uses
  SysUtils,
  Classes,
  Dialogs,
  Maindlg in 'maindlg.pas' {MainWnd},
  Strfile in 'Strfile.pas',
  SelDfkFileDlg in 'SelDfkFileDlg.pas' {SelDfkFilesWnd},
  comobj,
  AxDll_TLB,
  ImpExpObjects in 'ImpExpObjects.pas',
  DfkReader in 'DfkReader.pas';

{$R *.RES}

{*******************************************************************************
* PROZEDUR * DFKIMPORT                                                         *
********************************************************************************
* Procedure is used to import DFK data into GIS                                *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure DFKIMPORT(DIR:PCHAR;var AXHANDLE:VARIANT;var DEST:VARIANT;REGMODE:integer;LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
var StrfileObj:ClassStrfile;
    aDocument :IDispatch;
begin
   // create MainWnd
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.App:=AXHANDLE;
   aDocument:=DEST;
   MainWnd.DestDocument:=aDocument as IDocument;
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.RegMode:=REGMODE;
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.CreateStringFile;

   // create SelASCFilesWnd
   SelDfkFilesWnd:=TSelDfkFilesWnd.Create(nil);
   SelDfkFilesWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);

   // check if it can be executed
   if (REGMODE = 2) then // check if in Demo-Mode
      Showmessage(StrfileObj.Assign(40)); // DFK import in demo mode - only 100 objects will be imported

   if (REGMODE = 1) or (REGMODE = 2) then // start only when registered or demo-mode
   begin
      // it can be executed
      SelDfkFilesWnd.Show;
   end
   else
   begin
      Showmessage(StrfileObj.Assign(39)); // DFK import not registered
      MainWnd.ExitNormal:=FALSE;
      MainWnd.DestDocument.FinishImpExpRoutine(MainWnd.ExitNormal,'IMPORTMODULE');
   end;
end;

{*******************************************************************************
* PROCEDURE * CREATEIMP                                                        *
********************************************************************************
* Procedure is used to prepare an asc-import for Batchmode                     *
*                                                                              *
********************************************************************************}
procedure CREATEIMP(DIR:PCHAR;var AXHANDLE:IDispatch;var DEST:IDispatch;REGMODE:integer; LNGCODE:PCHAR;WINHANDLE:integer;WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);stdcall;
begin
   MainWnd:=TMainWnd.Create(nil);
   MainWnd.App:=AXHANDLE as ICore;
   MainWnd.DestDocument:=DEST as IDocument;
   MainWnd.Regmode:=REGMODE;
   MainWnd.LngCode:=strpas(LNGCODE);
   MainWnd.GisHandle:=WINHANDLE;
   MainWnd.CreateStringFile;

   // create SelASCFilesWnd
   SelDfkFilesWnd:=TSelDfkFilesWnd.Create(nil);
   SelDfkFilesWnd.CalculatePos(WTOP, WLEFT, WWIDTH, WHEIGHT);
   SelDfkFilesWnd.SetupForBatch;
end;

// procedure is used to add a .asc file that should be imported
procedure ADDIMPFILE(AFILE:PCHAR);stdcall;
begin
   SelDfkFilesWnd.AddImpFile(strpas(AFILE));
end;

// procedure is used to set database-settings
procedure SETDBSETTINGS(SOURCEDBMODE:INTEGER; SOURCEDBNAME:PCHAR; SOURCEDBTABLE:PCHAR;
                        DESTDBMODE  :INTEGER; DESTDBNAME  :PCHAR; DESTDBTABLE  :PCHAR);stdcall;
begin
   // source database will be ignored for ASC-Import
   MainWnd.AXImpExpDbc.ImpDbMode:=DESTDBMODE;
   MainWnd.AxImpExpDbc.ImpDatabase:=strpas(DESTDBNAME);
   MainWnd.AxImpExpDbc.ImpTable:=strpas(DESTDBTABLE);
end;

// procedure is used to set projection for batch mode
procedure SETPROJECTION(SOURCECOORDINATESYSTEM:INTEGER; // source projection
                        SOURCEPROJECTION      :PCHAR;
                        SOURCEDATE            :PCHAR;
                        SOURCEPROJSETTINGS    :PCHAR;
                        SOURCEXOFFSET         :DOUBLE;
                        SOURCEYOFFSET         :DOUBLE;
                        SOURCESCALE           :DOUBLE;
                        TARGETCOORDINATESYSTEM:INTEGER; // target projection
                        TARGETPROJECTION      :PCHAR;
                        TARGETDATE            :PCHAR;
                        TARGETPROJSETTINGS    :PCHAR;
                        TARGETXOFFSET         :DOUBLE;
                        TARGETYOFFSET         :DOUBLE;
                        TARGETSCALE           :DOUBLE);stdcall;
begin
   // source projection
   MainWnd.AxGisProjection.SourceCoordinateSystem:=SOURCECOORDINATESYSTEM;
   MainWnd.AxGisProjection.SourceProjection:=strpas(SOURCEPROJECTION);
   MainWnd.AxGisProjection.SourceDate:=strpas(SOURCEDATE);
   MainWnd.AxGisProjection.SourceProjSettings:=strpas(SOURCEPROJSETTINGS);
   MainWnd.AxGisProjection.SourceXOffset:=SOURCEXOFFSET;
   MainWnd.AxGisProjection.SourceYOffset:=SOURCEYOFFSET;
   MainWnd.AxGisProjection.SourceScale:=SOURCESCALE;
   // target projection
   MainWnd.AxGisProjection.TargetCoordinateSystem:=TARGETCOORDINATESYSTEM;
   MainWnd.AxGisProjection.TargetProjection:=strpas(TARGETPROJECTION);
   MainWnd.AxGisProjection.TargetDate:=strpas(TARGETDATE);
   MainWnd.AxGisProjection.TargetProjSettings:=strpas(TARGETPROJSETTINGS);
   MainWnd.AxGisProjection.TargetXOffset:=TARGETXOFFSET;
   MainWnd.AxGisProjection.TargetYOffset:=TARGETYOFFSET;
   MainWnd.AxGisProjection.TargetScale:=TARGETSCALE;
end;

// function is used to execute an asc-import
procedure EXECIMP;stdcall;
begin
   MainWnd.ExitNormal:=true;
   MainWnd.Show;
end;

function FREEDLL:boolean; stdcall;
var retVal:boolean;
begin
   retVal:=MainWnd.ExitNormal;
   MainWnd.Free;
   SelDfkFilesWnd.Free;
   result:=retVal;
end;

exports DFKIMPORT, CREATEIMP, ADDIMPFILE,
        SETDBSETTINGS, SETPROJECTION, EXECIMP, FREEDLL;

begin
end.
