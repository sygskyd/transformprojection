unit Maindlg;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Strfile, StdCtrls, Buttons, ExtCtrls, Gauges, DfkReader, Db,
  DBTables,Math, windows, OleCtrls, AXImpExpDbcXControl_TLB, AxGisPro_TLB,
  AXGisMan_TLB, AxDll_TLB, WinOSInfo;

const

      //registration modes
      NO_LICENCE        = 0;
      FULL_LICENCE      = 1;
      DEMO_LICENCE      = 2;

      // Object types
      ot_Pixel     = 1;
      ot_Poly      = 2;
      ot_CPoly     = 4;
      ot_Text      = 7;
      ot_Symbol    = 8;
      ot_Spline    = 9;
      ot_Image     = 10;
      ot_MesLine   = 11;
      ot_Circle    = 12;
      ot_Arc       = 13;

      //TextSTyles
      ts_Left           =  0;
      ts_Center         =  1;
      ts_Right          =  2;
      ts_Italic         =  4;
      ts_Bold           =  8;
      ts_Underl         = 16;
      ts_FixHeight      = 32;
      ts_Transparent    = 64;

      //LineTypes
      lt_Solid     = 0;
      lt_Dash      = 1;
      lt_Dot       = 2;
      lt_DashDot   = 3;
      lt_DashDDot  = 4;
      lt_UserDefined    = 1000;

      //FillTypes
      pt_NoPattern = 0;
      pt_FDiagonal = 1;
      pt_Cross     = 2;
      pt_DiagCross = 3;
      pt_BDiagonal = 4;
      pt_Horizontal= 5;
      pt_Vertical  = 6;
      pt_Solid     = 7;
      pt_UserDefined = 8;

      stDrawingUnits    = 0;
      stScaleInDependend= 1;
      stProjectScale    = 2;
      stPercent         = 3;

type
  TMainWnd = class(TForm)
    Label1: TLabel;
    Panel1: TPanel;
    Progresslabel: TLabel;
    Prozessbalken: TGauge;
    StatusLB: TListBox;
    SaveReportBtn: TButton;
    OKBtn: TButton;
    CancelBtn: TButton;
    SaveDialogRep: TSaveDialog;
    CloseTimer: TTimer;
    SetupWindowTimer: TTimer;
    AXImpExpDbc: TAXImpExpDbc;
    AxGisProjection: TAxGisProjection;
    AXGisObjMan: TAXGisObjMan;
    procedure FormCreate(Sender: TObject);
    procedure StopBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure SaveReportBtnClick(Sender: TObject);
    procedure CloseTimerTimer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AXImpExpDbcDialogSetuped(Sender: TObject);
    procedure AXImpExpDbcRequestImpLayerIndex(Sender: TObject;
      const Layername: WideString; var LayerIndex: Integer);
    procedure AXImpExpDbcRequestImpProjectName(Sender: TObject;
      var ProjectName: WideString);
    procedure AXImpExpDbcRequestImpProjectGUIDs(Sender: TObject;
      var StartGUID, CurrentGUID: WideString);
  private
    { Private-Deklarationen }
    StrFileObj :ClassStrfile;
    DfkFile    :TDfkReader;
    Pass       :integer;
    Warnings   :integer;
    IgnoredFiles:integer;

    DfkFilename:string;

    Bytes2work  :longint;
    Bytesworked :longint;
    Aktiv       :boolean;
    ObjectCount :longint;
    FirstActivate  :boolean;
    NumPolys  :integer;
    NumCPolys :integer;
    NumPixels :integer;
    NumTexts  :integer;
    NumArcs   :integer;
    NumSplines:integer;
    NumCircles:integer;
    NumLayers :integer;
    NumSymbols:integer;
    FilesProcessed:integer;
    NumIgnoredObjects:integer;
    FileStartTime   :TDateTime;
    OverAllStartTime:TDateTime;

    procedure RunPass(AktPass:integer);
    procedure ExecutePass;
    procedure DisplayDemoWarning;
    procedure DisplayStatistics;
  public
    { Public-Deklarationen }
    App         :Variant;
    DestDocument:IDocument;
    Layers      :ILayer;
    APolyline   :IPolyline;
    APolygon    :IPolygon;
    ASpline     :ISpline;
    DestDBMode  :integer;
    RegMode     :integer;
    ImagePath   :string;
    IgnoreNotFoundImages:boolean;
    LngCode     :string;
    ExitNormal  :boolean;
    Abort       :boolean;

    GisHandle   :integer;

    { Procedures that are used to handle AX-Control }
    procedure CreateLayer(Layer:string);
    function  InsertPoint(X:double;Y:double; var HasDuplicate:boolean):integer;
    function  CheckLayerExists(Layer:string):boolean;
    procedure CreatePolyline;
    procedure InsertPointToPolyLine(X:double; Y:double);
    function  ClosePolyline(var HasDuplicate:boolean):integer;
    procedure CreatePolygon;
    procedure InsertPointToPolygon(X:double; Y:double);
    function  ClosePolygon(var HasDuplicate:boolean):integer;
    procedure CreateSpline;
    procedure InsertPointToSpline(X:double; Y:double);
    function  CloseSpline(var HasDuplicate:boolean):integer;
    procedure SetLayerPriorities(Linienstil:longint;Linienfarbe:longint;Flaechenstil:longint;Flaechenfarbe:longint);
    function  InsertText(X:double;Y:double;AAngle:integer;ATextInfo:string;AHeight:integer;Afont:string;TextStyle:integer; var HasDuplicate:boolean):integer;
    function  CreateEllipse(X:double;Y:double;Radius1:integer; Radius2:integer; var HasDuplicate:boolean):integer;
    function InsertSymbol(X:double;Y:double;aSize:double;aAngle:double;Symname:string; var HasDuplicate:boolean):integer;
    function InsertArc(X:double;Y:double; PrimAxis:integer; Angle: double; StartAngle: Double; SecAxis:integer; EndAngle: Double; var HasDuplicate:boolean):integer;

    procedure SetProjectionSettings;
    procedure CheckProjectionSettings;

    procedure StartImport;
    procedure CreateStringFile;
    procedure ShowWarning(const aWarning:string);
  end;

var
  MainWnd: TMainWnd;

implementation

uses SelDfkFileDlg, inifiles;

{$R *.DFM}

procedure TMainWnd.CreateStringFile;
begin
   StrFileObj:=ClassStrfile.Create;
   StrFileObj.CreateList(OSInfo.WingisDir,LngCode,'DFKIMP');
   Self.Caption:=StrFileObj.Assign(1);           // WinGIS PFK-Import 2000 }
   OkBtn.Caption:=StrFileObj.Assign(6);          // Ok
   CancelBtn.Caption:=StrFileObj.Assign(7);      // Cancel
   SaveReportBtn.Caption:=StrFileObj.Assign(16); // Save report
   ProgressLabel.Caption:=StrFileObj.Assign(18); // Process messages
end;

procedure TMainWnd.FormCreate(Sender: TObject);
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   FirstActivate:=true;
   Self.Height:=1;
   Self.Width:=1;
   Self.Update;
   Aktiv:=false;

   ImagePath:='';
   IgnoreNotFoundImages:=false;
   ObjectCount:=0;

   NumPolys:=0;
   NumCPolys:=0;
   NumPixels:=0;
   NumTexts:=0;
   NumArcs:=0;
   NumSplines:=0;
   NumCircles:=0;
   FilesProcessed:=0;
   NumSymbols:=0;
   NumIgnoredObjects:=0;
   NumLayers:=0;

   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
end;

procedure TMainWnd.CheckProjectionSettings;
var different:boolean;
begin
   different:=false;
   // function is used to check if the settings in the
   if (AxGisProjection.TargetCoordinateSystem <> DestDocument.Projection) then
      different:=TRUE;

   if (AxGisProjection.TargetXOffset <> DestDocument.ProjectionXOffset) then
      different:=TRUE;

   if (AxGisProjection.TargetYOffset <> DestDocument.ProjectionYOffset) then
      different:=TRUE;

   if (AxGisProjection.TargetScale <> DestDocument.ProjectionScale) then
      different:=TRUE;

   // check if the projections are different
   if DestDocument.ProjectProjSettings <> AxGisProjection.TargetProjSettings then
      different:=TRUE;

   if DestDocument.ProjectProjection <> AxGisProjection.TargetProjection then
      different:=TRUE;

   if DestDocument.Projectdate <> AxGisProjection.TargetDate then
      different:=TRUE;

   if different then
   begin
      Showmessage(StrfileObj.Assign(53)); // Warning: Projection does not fit to project projection
      AxGisProjection.SetupByDialog;
   end;
   AxGisProjection.UsedInputCoordSystem:=cs_Carthesic; // Dfk data is carthesic
end;

procedure TMainWnd.SetProjectionSettings;
var aApp:OleVariant;
    aDoc:OleVariant;
begin
   // procedure is used to set the projection settings
   // of the current project to the GeoWnd
   AxGisProjection.WorkingPath:=OSInfo.WingisDir;
   AxGisProjection.LngCode:=LngCode;
   AXImpExpDbc.WorkingDir:=OSInfo.WingisDir;
   AXImpExpDbc.LngCode:=LngCode;
   AxImpExpDbc.ShowMode:=2; // show only Target
   AXGisObjMan.WorkingDir:=OSInfo.WingisDir;
   AXGisObjMan.LngCode:=LngCode;
   aApp:=App; AxGisObjMan.SetApp(aApp);
   aDoc:=DestDocument; AxGisObjMan.SetDocument(aDoc);

   SelDfkFilesWnd.DestDbLabel.Caption:=AXImpExpDbc.ImpDbInfo;
   AxGisProjection.SourceCoordinateSystem:=DestDocument.Projection;
   AxGisProjection.TargetCoordinateSystem:=DestDocument.Projection;
   AxGisProjection.SourceProjSettings:=DestDocument.ProjectProjSettings;
   AxGisProjection.TargetProjSettings:=DestDocument.ProjectProjSettings;
   AxGisProjection.SourceProjection:=DestDocument.ProjectProjection;
   AxGisProjection.TargetProjection:=DestDocument.ProjectProjection;
   AxGisProjection.SourceDate:=DestDocument.Projectdate;
   AxGisProjection.TargetDate:=DestDocument.Projectdate;
   AxGisProjection.SourceXOffset:=DestDocument.ProjectionXOffset;
   AxGisProjection.TargetXOffset:=DestDocument.ProjectionXOffset;
   AxGisProjection.SourceYOffset:=DestDocument.ProjectionYOffset;
   AxGisProjection.TargetYOffset:=DestDocument.ProjectionYOffset;
   AxGisProjection.SourceScale:=DestDocument.ProjectionScale;
   AxGisProjection.TargetScale:=DestDocument.ProjectionScale;
   // if source and target is .amp - file
   // the used input coordinates and used output coordinates
   // for the projection calculation are in carthesic
   AxGisProjection.UsedInputCoordSystem:=cs_Carthesic; // Dfk data is carthesic
   AxGisProjection.UsedOutputCoordSystem:=cs_Carthesic;
end;


{*******************************************************************************
* PROZEDUR : StopBtnClick                                                      *
********************************************************************************
* Prozedur wird aufgerufen wenn der Stopbutton im MainWnd gedr�ckt wird.       *
* Damit wird entweder der Konvertierung gestoppt oder die Applikation beendet. *
*                                                                              *
********************************************************************************}
procedure TMainWnd.StopBtnClick(Sender: TObject);
begin
   if aktiv = true then
   begin
      DfkFile.CloseFile;
      aktiv:=false; Abort:=true;
   end
   else
      MainWnd.Close;
end;

{*******************************************************************************
* PROZEDUR : RunPass                                                           *
********************************************************************************
* Prozedur leitet den jeweiligen Konvertierungs-Pass ein.                      *
*                                                                              *
* PARAMETER: Pass -> Gibt den Konvertierungs-Pass an.                          *
*                                                                              *
********************************************************************************}
procedure TMainWnd.RunPass(AktPass:integer);
var Status:string;
    aDate, DispDate, aTime, DispTime:string;
    EllapsedTime:TDateTime;
begin
   if AktPass = 1 then // open and read file
   begin
      OkBtn.Enabled:=false;
      SaveReportBtn.Enabled:=false;
      Aktiv:=true;
      Warnings:=0;
      if not SelDfkfilesWnd.GetFilename(DfkFilename) then
      begin
         Aktiv:=false;
         DisplayStatistics;
         CancelBtn.enabled:=false;
         OKBtn.enabled:=true;
         SaveReportBtn.enabled:=true;
         exit;
      end;
      // check if file exists
      if not FileExists(DfkFilename) then
      begin
         // Skip the File
         Runpass(1);
         exit;
      end;
      DfkFile:=TDfkReader.Create;
      // display the starttime
      if (FilesProcessed = 0) then
         OverAllStartTime:=now;
      FilesProcessed:=FilesProcessed+1;
      FileStartTime:=now;
      aDate:=FormatDateTime('ddmmyy',now);
      DispDate:=copy(aDate,1,2)+'.'+copy(aDate,3,2)+'.'+copy(aDate,5,2);
      aTime:=Formatdatetime('hhnnss',Now);
      DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
      StatusLB.Items.Add(StrFileObj.Assign(72) + DispDate+' / '+DispTime); // start time:

      // display Dfk-File that is currently read
      Status:=StrfileObj.Assign(10)+ExtractFilename(DfkFilename); // Dfk-File:
      StatusLB.Items.Add(Status);
      StatusLb.Items.Add(AXImpExpDbc.ImpDbInfo);
      Status:=StrfileObj.Assign(19);
      StatusLB.Items.Add(Status);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;

      // evaluate number of bytes that should be processed
      DfkFile.DfkFilename:=DfkFilename;
      DfkFile.IniFilename:=OSInfo.LocalAppDataDir+'DKMComb.ini';
      Bytes2Work:=DfkFile.GetFileSize; Bytesworked:=0;

      // now the projection will be checked
      CheckProjectionSettings;

      // open the file
      DfkFile.OpenFile;
      Pass:=1;
      ExecutePass;
      exit;
   end;

   if AktPass = 2 then // import data to WinGIS
   begin
       Bytes2work:=DfkFile.ReadObjects; Bytesworked:=0;
       // setup window
       StatusLB.Items.Add(StrfileObj.Assign(15)); // Import DFK data
       StatusLB.ItemIndex := StatusLB.Items.Count-1;
       StatusLB.Update;
       DfkFile.InitImport;
       Pass:=2;
       ExecutePass;
       exit;
   end;

   if AktPass = 3 then // end of import
   begin
      DfkFile.CloseFile;
      if Warnings = 0 then
         StatusLB.Items.Add(StrFileObj.Assign(14)) // File correct imported
      else
         StatusLB.Items.Add(StrFileObj.Assign(42)); // File imported with warnings
      // display end time for this file
      EllapsedTime:=now-FileStartTime;
      aDate:=FormatDateTime('ddmmyy',now);
      DispDate:=copy(aDate,1,2)+'.'+copy(aDate,3,2)+'.'+copy(aDate,5,2);
      aTime:=Formatdatetime('hhnnss',now);
      DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
      StatusLB.Items.Add(StrFileObj.Assign(58) + DispDate+' / '+DispTime); // end time:
      // display ellapsed time for this file
      aTime:=Formatdatetime('hhnnss',EllapsedTime);
      DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
      StatusLB.Items.Add(StrFileObj.Assign(57) + DispTime); // ellapsed time:
      StatusLB.Items.Add('--------------------------------------------------------');
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      Aktiv:=false;
      DfkFile.Destroy;
      Runpass(1); // go to next file
   end;
end;

{*******************************************************************************
* PROZEDUR : ExecutePass                                                       *
********************************************************************************
* Prozedur die zur Abarbeitung eines Konvertierungsschritts dient              *
*                                                                              *
*                                                                              *
********************************************************************************}
procedure TMainWnd.ExecutePass;
var Percent :integer;
    haderror:boolean;
begin
   if Pass = 1 then // read dfk data
   begin
      repeat
         Bytesworked:=Bytesworked + DfkFile.ReadElement;
         haderror:=DfkFile.HadError;
         // Show percent status
         if Bytes2work > 0 then Percent:= Round (Bytesworked / Bytes2work * 100 ) else Percent:= 100;
         ProzessBalken.Progress :=Percent;
         ProzessBalken.Update;
         Application.Processmessages;
      until (Bytesworked >= Bytes2work) or (Abort = true) or (haderror = true);

      if (haderror = TRUE) then
      begin
         StatusLB.Items.Add(StrFileObj.Assign(52));
         IgnoredFiles:=IgnoredFiles+1;
         DfkFile.CloseFile;
         StatusLB.Items.Add('--------------------------------------------------------');
         StatusLB.ItemIndex := StatusLB.Items.Count-1;
         StatusLB.Update;
         Aktiv:=false;
         DfkFile.Destroy;
         Runpass(1);
         exit; // go to next file due to this file has been ignored
      end;
      if (Bytesworked >= Bytes2work) and (Abort = false) then begin RunPass(2); exit; end;
      if Abort = true then exit;
   end;

   if Pass = 2 then // importing into GIS
   begin
      if Bytes2work > 0 then
      begin
         repeat
            DfkFile.ImportObject;
            Bytesworked:=Bytesworked+1;
            // Show percent status
            if Bytes2work > 0 then Percent:= Round (Bytesworked / Bytes2work * 100 ) else Percent:= 100;
            ProzessBalken.Progress :=Percent;
            ProzessBalken.Update;
            Application.Processmessages;
         until (Bytesworked >= Bytes2work) or (Abort = true);
      end;
      if (Abort = false) then begin RunPass(3); exit; end;
      if Abort = true then exit;
   end;
end;

procedure TMainWnd.CreateLayer(Layer:string);
begin
   NumLayers:=NumLayers+1;
   Layers:=DestDocument.Layers.InsertLayerByName(Layer,True);
end;

function TMainWnd.CheckLayerExists(Layer:string):boolean;
var dispatch:IDispatch;
    retVal:boolean;
begin
   dispatch:=DestDocument.Layers.GetLayerByName(Layer);
   if dispatch <> nil then retVal:=true else retVal:=false;
   if retVal then
      Layers:=DestDocument.Layers.GetLayerByName(Layer);
   result:=retVal;
end;

procedure TMainWnd.CreatePolyline;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;
   APolyline:=DestDocument.CreatePolyline;
end;

procedure TMainWnd.DisplayDemoWarning;
begin
   StatusLB.Items.Add(StrfileObj.Assign(41)); { 100 Objects created - following objects will be skiped. }
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;
   // also the database must be ignored after this point
   DestDBMode:=DB_NONE;
   AXImpExpDbc.ImpDbMode:=DestDBMode;
end;

procedure TMainWnd.CreateSpline;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;
   ASpline:=DestDocument.CreateSpline;
end;

procedure TMainWnd.InsertPointToSpline(X:double;Y:double);
var APoint:IPoint;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;

   //do the projection
   AxGisProjection.Calculate(X,Y);

   APoint:=DestDocument.CreatePoint;
   APoint.X:=Round(X*100);
   APoint.Y:=Round(Y*100);
   ASpline.InsertPoint(APoint);
end;

procedure TMainWnd.InsertPointToPolyLine(X:double; Y:double);
var APoint:IPoint;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;

   //do the projection
   AxGisProjection.Calculate(X,Y);

   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   APolyLine.InsertPoint(APoint);
end;

function TMainWnd.ClosePolyline(var HasDuplicate:boolean):integer;
var AIndex:integer;
    DuplId:integer;
begin
   //check licence
   result:=0;
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;

   Layers.InsertObject(APolyline);
   AIndex:=APolyline.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

   // check if it is a duplicate
   if SelDfkFilesWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, Layers.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;
   if (AIndex <> 0) and (DuplId = 0) then
      NumPolys:=NumPolys+1;
   result:=AIndex;
end;

function TMainWnd.CloseSpline(var HasDuplicate:boolean):integer;
var AIndex:integer;
    DuplId:integer;
begin
   //check licence
   result:=0;
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;
   Layers.InsertObject(ASpline);
   AIndex:=ASpline.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;
   // check if it is a duplicate
   if SelDfkFilesWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, Layers.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;

   if (AIndex <> 0) and (DuplId = 0) then
      NumSplines:=NumSplines+1;
   result:=AIndex;
end;

procedure TMainWnd.CreatePolygon;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;
   APolygon:=DestDocument.CreatePolygon;
end;

procedure TMainWnd.InsertPointToPolygon(X:double; Y:double);
var APoint:IPoint;
begin
   //check licence
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;
   //do the projection
   AxGisProjection.Calculate(X,Y);

   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   APolygon.InsertPoint(APoint);
end;

function TMainWnd.ClosePolygon(var HasDuplicate:boolean):integer;
var AIndex:integer;
    Duplid:integer;
begin
   //check licence
   result:=0;
   if Regmode = DEMO_LICENCE then
   begin
      if ObjectCount > 100 then
         exit;
   end;

   APolygon.ClosePoly;
   Layers.InsertObject(APolygon);
   AIndex:=APolygon.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

   // check if it is a duplicate
   if SelDfkFilesWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, Layers.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;
   if (AIndex <> 0) and (DuplId = 0) then
      NumCPolys:=NumCPolys+1;
   result:=AIndex;
end;

function TMainWnd.InsertPoint(X:double;Y:double; var HasDuplicate:boolean):integer;
var APixel:IPixel;
    APoint:IPoint;
    AIndex:integer;
    DuplId:integer;
begin
   //check licence
   result:=0;
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;
   //do the projection
   AxGisProjection.Calculate(X,Y);

   APixel:=DestDocument.CreatePixel;
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   APixel.Position:=APoint;
   Layers.InsertObject(APixel);
   AIndex:=APixel.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

   // check if it is a duplicate
   if SelDfkFilesWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, Layers.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;
   if (AIndex <> 0) and (DuplId = 0) then
      NumPixels:=NumPixels+1;

   result:=AIndex;
end;

procedure TMainWnd.SetLayerPriorities(Linienstil:longint;Linienfarbe:longint;Flaechenstil:longint;Flaechenfarbe:longint);
begin
   Layers.Linestyle.Style:=Linienstil;
   Layers.Linestyle.Color:=Linienfarbe;
   Layers.Linestyle.FillColor:=Linienfarbe;

   Layers.Fillstyle.Pattern:=Flaechenstil;
   Layers.Fillstyle.ForeColor:=Flaechenfarbe;
//   Layers.Fillstyle.BackColor:=Flaechenfarbe;
end;


function TMainWnd.InsertArc(X:double;Y:double; PrimAxis:integer; Angle: double; StartAngle: Double; SecAxis:integer; EndAngle: Double; var HasDuplicate:boolean):integer;
var AArc:IArc;
    APoint:IPoint;
    retVal:integer;
    DuplId:integer;
begin
   //check licence
   result:=0;
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;

   //do the projection
   AxGisProjection.Calculate(X,Y);

   retVal:=0;
   AArc:=DestDocument.CreateArc;
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   AArc.Position:=APoint;
   AArc.PrimaryAxis:=PrimAxis;
   AArc.Angle:=Angle * PI / 180;
   AArc.BeginAngle:=StartAngle * PI / 180;
   AArc.SecondaryAxis:=SecAxis;
   AArc.EndAngle:=EndAngle * PI / 180;
   Layers.InsertObject(AArc);
   retVal:=AArc.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

   // check if it is a duplicate
   if SelDfkFilesWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(retVal, Layers.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(retVal);
         retVal:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;

   if (retVal <> 0) and (DuplId = 0) then
      Numarcs:=Numarcs+1;

   result:=retVal;
end;


function TMainWnd.InsertText(X:double;Y:double; AAngle:integer;ATextinfo:string;AHeight:integer;Afont:string;TextStyle:integer;var HasDuplicate:boolean):integer;
var AText:IText;
    AIndex:integer;
    aTextHeight:integer;
    i:integer;
    fontexists:boolean;
    DuplId:integer;
begin
   //check licence
   result:=0;
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;

   //do the projection
   AxGisProjection.Calculate(X,Y);
   AText:=DestDocument.CreateText;

   AText.FontStyle:=TextStyle;
   // check if font is installed
   fontexists:=FALSE;
   for i:=0 to Screen.Fonts.Count-1 do
   begin
      if Screen.Fonts[i] = AFont then
      begin
         fontexists:=TRUE;
         break;
      end;
   end;

   if not fontexists then
   begin
      StatusLB.Items.Add(StrfileObj.Assign(22) + ' '+ AFont +' '+ StrfileObj.Assign(23)); //Font not known ... use Courier New as default
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      AFont:='Courier New'; //set default font
   end;

   AText.Fontname:=AFont;
   AText.Position.X:=round(X*100);
   AText.Position.Y:=round(Y*100);
   AText.Text:=ATextInfo;
   aTextHeight:=Abs(aHeight); if (aTextHeight < 10) then aTextHeight:=100; // default is one meter
   AText.FontHeight:=aTextHeight;
   AText.Angle:=round(aAngle);
   if aHeight < 0 then
      AText.Align:=0  // positionate from top left
   else
      AText.Align:=2; // positionate from bottom left
   Layers.Insertobject(AText);
   AIndex:=AText.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

   // check if it is a duplicate
   if SelDfkFilesWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, Layers.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;

   if (AIndex <> 0) and (DuplId = 0) then
      NumTexts:=NumTexts+1;
   result:=AIndex;
end;

function TMainWnd.InsertSymbol(X:double;Y:double;aSize:double;aAngle:double;Symname:string; var HasDuplicate:boolean):integer;
var ASymbol:ISymbol;
    APoint :IPoint;
    MySymboldef:ISymbolDef;
    i:integer;
    gefunden:boolean;
    new_id:integer;
    SymSizeType:integer;
    SymIndex   :integer;
    DuplId:integer;
    DefaultSize:double;
begin
   //check licence
   result:=0;
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;

   // Now the Symbolnr has to be evaluated
   gefunden:=false;
   for i:=0 to DestDocument.Symbols.Count-1 do
   begin
      MySymboldef:=DestDocument.Symbols.Items[i];

      if MySymbolDef.Name = SymName then
      begin
         SymSizeType:=MySymbolDef.DefaultSizeType;
         DefaultSize:=MySymbolDef.Defaultsize;
         SymIndex:=MySymbolDef.Index;
         gefunden:=true;
         break;
      end;
   end;
   if (gefunden) then
   begin
      //do the projection
      AxGisProjection.Calculate(X,Y);

      ASymbol:=DestDocument.CreateSymbol;
      APoint:=DestDocument.CreatePoint;
      ASymbol.SymIndex:=SymIndex;
      ASymbol.Angle:=aAngle * PI / 180;
      ASymbol.Symbolname:=Symname;
      ASymbol.Position.X:=round(X*100);
      ASymbol.Position.Y:=round(Y*100);
      ASymbol.SizeType:=SymSizeType;
      ASymbol.Size:=-3E301; // use default size of symbol
      Layers.InsertObject(ASymbol);
      new_id:=ASymbol.Index;
      DuplId:=0;
      HasDuplicate:=FALSE;
      // check if it is a duplicate
      if SelDfkFilesWnd.IgnoreDuplicatesCb.Checked then
      begin
         // check if the object has a duplicate
         DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(new_id, Layers.Layername);
         if DuplId > 0 then
         begin
            // remove the object
            DestDocument.DeleteObjectByIndex(new_id);
            new_id:=DuplId;
            HasDuplicate:=TRUE;
            NumIgnoredObjects:=NumIgnoredObjects+1;
         end;
      end;
      if (new_id <> 0) and (DuplId = 0) then
         NumSymbols:=NumSymbols+1;
   end
   else
   begin
      StatusLB.Items.Add(StrfileObj.Assign(20) + SymName);
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      new_id:=InsertPoint(X,Y, HasDuplicate);
   end;
   result:=new_id;
end;

function TMainWnd.CreateEllipse(X:double;Y:double;Radius1:integer; Radius2:integer; var HasDuplicate:boolean):integer;
var ACircle:ICircle;
    APoint:IPoint;
    AIndex:integer;
    DuplId:integer;
begin
   //check licence
   result:=0;
   if Regmode = DEMO_LICENCE then
   begin
      ObjectCount:=ObjectCount+1;
      if ObjectCount = 100 then
      begin
         DisplayDemoWarning;
      end
      else
      if ObjectCount > 100 then
         exit;
   end;

   //do the projection
   AxGisProjection.Calculate(X,Y);

   ACircle:=DestDocument.CreateCircle;
   APoint:=DestDocument.CreatePoint;
   APoint.X:=round(X*100);
   APoint.Y:=round(Y*100);
   ACircle.PrimaryAxis:=Radius1;
   ACircle.SecondaryAxis:=Radius2;
   ACircle.Position:=APoint;
   Layers.InsertObject(ACircle);
   AIndex:=ACircle.Index;
   DuplId:=0;
   HasDuplicate:=FALSE;

   // check if it is a duplicate
   if SelDfkFilesWnd.IgnoreDuplicatesCb.Checked then
   begin
      // check if the object has a duplicate
      DuplId:=AxGisObjMan.CheckIdHasDuplicateOnLayer(AIndex, Layers.Layername);
      if DuplId > 0 then
      begin
         // remove the object
         DestDocument.DeleteObjectByIndex(AIndex);
         AIndex:=DuplId;
         HasDuplicate:=TRUE;
         NumIgnoredObjects:=NumIgnoredObjects+1;
      end;
   end;
   if (AIndex <> 0) and (DuplId = 0) then
      NumCircles:=NumCircles+1;

   result:=AIndex;
end;

procedure TMainWnd.StartImport;
begin
   if ExitNormal then
   begin
      // init database connection
      AXImpExpDbc.WorkingDir:=OSInfo.WingisDir;
      AXImpExpDbc.LngCode:=LngCode;
      IgnoredFiles:=0;
      RunPass(1);
   end;
end;

procedure TMainWnd.CancelBtnClick(Sender: TObject);
var Status:string;
begin
   if aktiv = true then
   begin
      Status:=StrFileObj.Assign(13); { Import canceled! }
      StatusLB.Items.Add(Status);
      StatusLB.Items.Add('--------------------------------------------------------');
      StatusLB.ItemIndex := StatusLB.Items.Count-1;
      StatusLB.Update;
      DfkFile.CloseFile;
      aktiv:=false; Abort:=true;
      OkBtn.Enabled:=true;
      SaveReportBtn.Enabled:=true;
      CancelBtn.Enabled:=false;
      DisplayStatistics;
   end
end;

procedure TMainWnd.OKBtnClick(Sender: TObject);
begin
   CloseTimer.Enabled:=true;
end;

procedure TMainWnd.SaveReportBtnClick(Sender: TObject);
var Datei:Textfile;
    Zeile:string;
    i:integer;
begin
   if SaveDialogRep.Execute then
   begin
      {$I-}
      Assignfile(Datei,SaveDialogRep.Filename);
      Rewrite(Datei);
      for i:=0 to StatusLB.Items.Count-1 do
      begin
         Zeile:=StatusLB.items[i];
         Writeln(Datei,Zeile);
      end;
      Closefile(Datei);
      {$I+}
   end;
end;

procedure TMainWnd.CloseTimerTimer(Sender: TObject);
begin
   MainWnd.Close;
   SelDfkFilesWnd.Close;
   DestDocument.FinishImpExpRoutine(ExitNormal,'IMPORTMODULE');
   CloseTimer.Enabled:=false;
end;

procedure TMainWnd.FormDestroy(Sender: TObject);
begin
   StrfileObj.Destroy;
end;

procedure TMainWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TMainWnd.SetupWindowTimerTimer(Sender: TObject);
var IdbProcessMask:integer;
begin
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,SelDfkFilesWnd.winleft,SelDfkFilesWnd.wintop,SelDfkFilesWnd.winwidth, SelDfkFilesWnd.winheight,SWP_SHOWWINDOW);
   MainWnd.Height:=SelDfkFilesWnd.winheight;
   MainWnd.Width:=SelDfkFilesWnd.winwidth;
   Self.Repaint;
   SetupWindowTimer.Enabled:=false;
   IdbProcessmask:=DestDocument.IdbProcessMask;
   DestDocument.IdbProcessMask:=0;
   StartImport;
   DestDocument.IdbProcessMask:=IdbProcessMask;
end;

procedure TMainWnd.FormShow(Sender: TObject);
begin
   AXImpExpDbc.Visible:=FALSE;
   AxGisProjection.Visible:=FALSE;
   AxGisObjMan.Visible:=FALSE;
end;

procedure TMainWnd.AXImpExpDbcDialogSetuped(Sender: TObject);
begin
   SelDfkFilesWnd.DestDbLabel.Caption:=AXImpExpDbc.ImpDbInfo;
   SelDfkFilesWnd.PageControl.Enabled:=TRUE;
   SelDfkFilesWnd.GeoBtn.Enabled:=TRUE;
   SelDfkFilesWnd.DbBtn.Enabled:=TRUE;
   SelDfkFilesWnd.OkBtn.Enabled:=SelDfkFilesWnd.OkBtnMode;
   SelDfkFilesWnd.CancelBtn.Enabled:=TRUE;
end;

procedure TMainWnd.AXImpExpDbcRequestImpLayerIndex(Sender: TObject;
  const Layername: WideString; var LayerIndex: Integer);
var i:integer;
begin
   // the Layerindex of the Layername has to be evaluated
   for i:=0 to DestDocument.Layers.Count-1 do
   begin
      if DestDocument.Layers.Items[i].Layername = Layername then
      begin
         LayerIndex:=DestDocument.Layers.Items[i].Index;
         break;
      end;
   end;
end;

procedure TMainWnd.AXImpExpDbcRequestImpProjectName(Sender: TObject;
  var ProjectName: WideString);
var ProjName:string;
begin
   ProjName:=DestDocument.Name;
   ProjectName:=ProjName;
end;

procedure TMainWnd.AXImpExpDbcRequestImpProjectGUIDs(Sender: TObject;
  var StartGUID, CurrentGUID: WideString);
var aStartGUID, aCurrentGUID:string;
begin
   aStartGUID:=DestDocument.StartProjectGUID;
   aCurrentGUID:=DestDocument.CurrentProjectGUID;
   StartGUID:=aStartGUID;
   CurrentGUID:=aCurrentGUID;
end;

procedure TMainWnd.ShowWarning(const aWarning:string);
begin
   // show warning
   Warnings:=Warnings+1;
   StatusLB.Items.Add(aWarning);
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;
end;

procedure TMainWnd.DisplayStatistics;
var aDate, aTime, DispTime, DispDate:string;
    EndTime, UsedTime:TDateTime;
begin
   StatusLB.Items.Add('---------------------------------------------------');
   // display end time for this file
   aDate:=FormatDateTime('ddmmyy',now);
   DispDate:=copy(aDate,1,2)+'.'+copy(aDate,3,2)+'.'+copy(aDate,5,2);
   aTime:=Formatdatetime('hhnnss',now);
   DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
   StatusLB.Items.Add(StrFileObj.Assign(58) + DispDate+' / '+DispTime); // end time:

   // display number of imported files
   StatusLb.Items.Add(StrFileObj.Assign(59)+inttostr(FilesProcessed-IgnoredFiles));
   if IgnoredFiles > 0 then StatusLb.Items.Add(StrFileObj.Assign(44)+inttostr(IgnoredFiles));
   // display ellapsed time
   EndTime:=Now;
   UsedTime:=EndTime-OverallStartTime;
   aTime:=Formatdatetime('hhnnss',UsedTime);
   DispTime:=copy(aTime,1,2)+':'+copy(aTime,3,2)+':'+copy(aTime,5,2);
   StatusLB.Items.Add(StrFileObj.Assign(60) + DispTime); // overall used time:

   // display imported data statistics
   if NumLayers     > 0 then StatusLB.Items.Add(StrFileObj.Assign(61) + inttostr(NumLayers));     // created layers:
   if NumSymbols    > 0 then StatusLB.Items.Add(StrFileObj.Assign(62) + inttostr(NumSymbols));    // created symbols:
   if NumPolys      > 0 then StatusLB.Items.Add(StrFileObj.Assign(63) + inttostr(NumPolys));      // created polylines:
   if NumCPolys     > 0 then StatusLB.Items.Add(StrFileObj.Assign(64) + inttostr(NumCPolys));     // created polygons:
   if NumCircles    > 0 then StatusLB.Items.Add(StrFileObj.Assign(65) + inttostr(NumCircles));    // created circles:
   if Numarcs       > 0 then StatusLB.Items.Add(StrFileObj.Assign(66) + inttostr(NumArcs));       // created arcs:
   if NumSplines    > 0 then StatusLB.Items.Add(StrFileObj.Assign(67) + inttostr(NumSplines));    // created splines:
   if NumTexts      > 0 then StatusLB.Items.Add(StrFileObj.Assign(68) + inttostr(NumTexts));      // created texts:
   if NumPixels     > 0 then StatusLB.Items.Add(StrFileObj.Assign(69) + inttostr(NumPixels));     // created points:
   if NumIgnoredObjects > 0 then StatusLB.Items.Add(StrFileObj.Assign(71) + inttostr(NumIgnoredObjects)); // number of ignored duplicates:
   StatusLB.Items.Add('---------------------------------------------------');
   StatusLB.ItemIndex := StatusLB.Items.Count-1;
   StatusLB.Update;
end;

end.
