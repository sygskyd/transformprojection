unit SelDfkFileDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ComCtrls, FileCtrl, StrFile, Gauges,
  ShellCtrls, FileSelectionPanel, WinOSInfo;

type
  TSelDfkFilesWnd = class(TForm)
    PageControl: TPageControl;
    TabSheetFiles: TTabSheet;
    OKBtn: TButton;
    CancelBtn: TButton;
    GeoBtn: TButton;
    SetupWindowTimer: TTimer;
    DbBtn: TButton;
    IgnoreDuplicatesCb: TCheckBox;
    FileSelectionPanel: TFileSelectionPanel;
    ShellTreeView: TShellTreeView;
    FileListBox: TFileListBox;
    ListBox: TListBox;
    TrashPanel: TPanel;
    AddAllBtn: TButton;
    DeleteAllBtn: TButton;
    TrashImage: TImage;
    DbPanel: TPanel;
    DestDbLabel: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure GeoBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SetupWindowTimerTimer(Sender: TObject);
    procedure DbBtnClick(Sender: TObject);
    procedure FileSelectionPanelFilesToConvertChanged(Sender: TObject);
  private
    { Private-Deklarationen }
    FirstActivate:boolean;
    CurrentFileIdx:integer;

    procedure CheckWindow;
  public
    { Public-Deklarationen }
    winheight:integer;
    winwidth :integer;
    wintop   :integer;
    winleft  :integer;
    OkBtnMode:boolean;


    function  GetFilename(var DfkFilename:string):boolean;
    procedure CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
    procedure SetupForBatch;
    procedure AddImpFile(aFilename:string);
  end;

var
  SelDfkFilesWnd: TSelDfkFilesWnd;
  StrFileObj    :ClassStrfile;

implementation

uses Maindlg, inifiles;

{$R *.DFM}

procedure TSelDfkFilesWnd.FormCreate(Sender: TObject);
var myScale:double;
    oldHeight:integer;
    oldWidth :integer;
begin
   BorderIcons := BorderIcons - [biMaximize] - [biSystemMenu];
   Self.Caption:=StrFileObj.Assign(1);                // WinGIS PXF-ASC Import 2000
   TabsheetFiles.Caption:=StrFileObj.Assign(2);       // Files
   OKBtn.Caption:=StrFileObj.Assign(6);               // Ok
   CancelBtn.Caption:=StrFileObj.Assign(7);           // Cancel
   GeoBtn.Caption:=StrFileObj.Assign(21);             // Projection
   DbBtn.Caption:=StrFileObj.Assign(3);               // Database
   IgnoreDuplicatesCb.Caption:=StrFileObj.Assign(56); // ignore already existing objects

   PageControl.ActivePage:=TabsheetFiles;

   CurrentFileIdx:=0;

   FirstActivate:=true;
   oldHeight:=Self.Height;
   OldWidth:=Self.Width;

   Self.Height:=1;
   Self.Width:=1;

   myScale:=1; //100000 / Trunc(9600000/(Self.PixelsPerInch));
   Scaleby(Trunc(9600000/(PixelsPerInch)),100000);
   
   // now adapt winheigt and winwidth
   winheight:=round(OldHeight*myScale);
   winwidth:=round(OldWidth*myScale);
end;

procedure TSelDfkFilesWnd.OKBtnClick(Sender: TObject);
var inifile:TIniFile;
begin
   MainWnd.ExitNormal:=true;
   // write current settings to the ConGIS.ini file
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir + 'ConGIS.ini');
   inifile.WriteString('DFKIMP','ImpDbMode',inttostr(MainWnd.AXImpExpDbc.ImpDbMode));
   inifile.WriteString('DFKIMP','ImpDatabase',MainWnd.AXImpExpDbc.ImpDatabase);
   inifile.WriteString('DFKIMP','ImpTable',MainWnd.AXImpExpDbc.ImpTable);

   inifile.WriteString('DFKIMP','SourceCoordinateSystem',inttostr(MainWnd.AxGisProjection.SourceCoordinateSystem));
   inifile.WriteString('DFKIMP','SourceProjSettings',MainWnd.AxGisProjection.SourceProjSettings);
   inifile.WriteString('DFKIMP','SourceProjection',MainWnd.AxGisProjection.SourceProjection);
   inifile.WriteString('DFKIMP','SourceDate',MainWnd.AxGisProjection.SourceDate);
   inifile.WriteString('DFKIMP','SourceXOffset',floattostr(MainWnd.AxGisProjection.SourceXOffset));
   inifile.WriteString('DFKIMP','SourceYOffset',floattostr(MainWnd.AxGisProjection.SourceYOffset));
   inifile.WriteString('DFKIMP','SourceScale',floattostr(MainWnd.AxGisProjection.SourceScale));

   inifile.WriteString('DFKIMP','LastUsedPath',FileSelectionPanel.Directory);
   if IgnoreDuplicatesCb.Checked then
      inifile.WriteString('DFKIMP','IgnoreExistingObjects','T')
   else
      inifile.WriteString('DFKIMP','IgnoreExistingObjects','F');
   inifile.Destroy;
   // now the import window can be opened and the import executed
   Self.Close;
   MainWnd.Show;
end;

procedure TSelDfkFilesWnd.CancelBtnClick(Sender: TObject);
begin
   MainWnd.ExitNormal:=false;
   MainWnd.CloseTimer.Enabled:=true;
end;

function TSelDfkFilesWnd.GetFilename(var DfkFilename:string):boolean;
var retVal:boolean;
begin
   retVal:=TRUE;
   if CurrentFileIdx < FileSelectionPanel.NumFilesToConvert then
   begin
      DfkFilename:=FileSelectionPanel.GetFilename(CurrentFileIdx);
      if not Fileexists(DfkFilename) then
         retVal:=FALSE;
   end
   else
      retVal:=FALSE;
   result:=retVal;
   CurrentFileIdx:=CurrentFileIdx+1;
end;

procedure TSelDfkFilesWnd.CheckWindow;
var allok:boolean;
begin
   allok:=true;
   if FileSelectionPanel.NumFilesToConvert = 0 then allok:=false;
   if allok then
      OkBtn.Enabled:=true
   else
      OkBtn.Enabled:=false;
end;

procedure TSelDfkFilesWnd.GeoBtnClick(Sender: TObject);
begin
   MainWnd.AxGisProjection.SetupByDialog;
end;

procedure TSelDfkFilesWnd.FormActivate(Sender: TObject);
begin
   if FirstActivate then
   begin
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
      SetupWindowTimer.Enabled:=true;
   end;
end;

procedure TSelDfkFilesWnd.SetupWindowTimerTimer(Sender: TObject);
var CurDir:string;
    dummy:string;
    intval, count:integer;
    doubleval:double;
    inifile:TIniFile;
begin
   SetupWindowTimer.Enabled:=false;
   windows.setparent (Self.Handle, MainWnd.GisHandle);
   SetWindowPos(Handle,HWND_TOP,winleft,wintop,winwidth, winheight,SWP_SHOWWINDOW);
   Self.Height:=winheight;
   Self.Width:=winwidth;
   // Setup the window with the ConGIS.ini settings
   inifile:=TIniFile.Create(OSInfo.LocalAppDataDir+'ConGIS.ini');
   dummy:=MainWnd.DestDocument.Name;
   dummy:=ChangeFileExt(dummy,'.wgi');
   if FileExists(dummy) then
   begin
      MainWnd.AXImpExpDbc.ImpDbMode:=5; // Idb-Database
      MainWnd.AXImpExpDbc.ImpDatabase:='';
      MainWnd.AXImpExpDbc.ImpTable:='';
   end
   else
   begin
      dummy:=inifile.ReadString('DFKIMP','ImpDbMode','0');
      val(dummy, intval, count);
      if intval <> 5 then // Idb-Database
      begin
         MainWnd.AXImpExpDbc.ImpDbMode:=intval;
         dummy:=inifile.ReadString('DFKIMP','ImpDatabase','');
         MainWnd.AXImpExpDbc.ImpDatabase:=dummy;
         dummy:=inifile.ReadString('DFKIMP','ImpTable','');
         MainWnd.AXImpExpDbc.ImpTable:=dummy;
      end;
   end;
   DestDbLabel.Caption:=MainWnd.AXImpExpDbc.ImpDbInfo;

   // setup the projection
   dummy:=inifile.ReadString('DFKIMP','SourceCoordinateSystem','');
   if dummy <> '' then
   begin
      val(dummy, intval, count);
      MainWnd.AxGisProjection.SourceCoordinateSystem:=intval;
      dummy:=inifile.ReadString('DFKIMP','SourceProjSettings','NONE');
      MainWnd.AxGisProjection.SourceProjSettings:=dummy;
      dummy:=inifile.ReadString('DFKIMP','SourceProjection','NONE');
      MainWnd.AxGisProjection.SourceProjection:=dummy;
      dummy:=inifile.ReadString('DFKIMP','SourceDate','NONE');
      MainWnd.AxGisProjection.SourceDate:=dummy;
      dummy:=inifile.ReadString('DFKIMP','SourceXOffset','0');
      val(dummy, doubleval, count);
      MainWnd.AxGisProjection.SourceXOffset:=doubleval;
      dummy:=inifile.ReadString('DFKIMP','SourceYOffset','0');
      val(dummy, doubleval, count);
      MainWnd.AxGisProjection.SourceYOffset:=doubleval;
      dummy:=inifile.ReadString('DFKIMP','SourceScale','1');
      val(dummy, doubleval, count);
      MainWnd.AxGisProjection.SourceScale:=doubleval;
   end;

   dummy:=inifile.ReadString('DFKIMP','LastUsedPath','');
   if dummy <> '' then
      CurDir:=dummy
   else
      GetDir(0, CurDir);
   FileSelectionPanel.Directory:=CurDir;

   dummy:=inifile.ReadString('DFKIMP','IgnoreExistingObjects','F');
   if dummy = 'T' then IgnoreDuplicatesCb.Checked:=TRUE else IgnoreDuplicatesCb.Checked:=FALSE;
   inifile.Destroy;
   Self.Repaint;
end;

procedure TSelDfkFilesWnd.CalculatePos(WTOP:integer; WLEFT:integer; WWIDTH:integer; WHEIGHT:integer);
begin
   wintop:=(WHEIGHT div 2) - (winheight div 2);
   winleft:=(WWIDTH div 2) - (winwidth div 2);
end;

// procedure is used to setup the SelAscFilesWnd for batch mode
procedure TSelDfkFilesWnd.SetupForBatch;
begin
   if FirstActivate then
   begin
      MainWnd.SetProjectionSettings;
      FirstActivate:=false;
   end;
end;

procedure TSelDfkFilesWnd.AddImpFile(aFilename:string);
begin
   FileSelectionPanel.AddFileToConvert(aFilename);
end;

procedure TSelDfkFilesWnd.DbBtnClick(Sender: TObject);
begin
   MainWnd.AXImpExpDbc.SetupByDialog(Self.Handle,5,5);
   PageControl.Enabled:=FALSE;
   GeoBtn.Enabled:=FALSE;
   DbBtn.Enabled:=FALSE;
   OkBtnMode:=OkBtn.Enabled;
   OkBtn.Enabled:=FALSE;
   CancelBtn.Enabled:=FALSE;
end;

procedure TSelDfkFilesWnd.FileSelectionPanelFilesToConvertChanged( Sender: TObject);
begin
   CheckWindow;
end;

end.
