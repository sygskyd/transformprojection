unit DfkReader;

interface

uses SysUtils, ImpExpObjects,  WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
     Forms, Dialogs, StrFile;

type

CoordinateRef_rec = record
   CoordinateIdx:integer;
   LayerIdx     :integer;
end;

p_KoordinateSatzRef=^KoordinateSatzRef;
KoordinateSatzRef = record
   KoordinateRefs   :array of CoordinateRef_rec;
   NumKoordinateRefs:integer;
   Flurkarte        :string;
end;

type
// base class for import/export objects
TDfkReader = class
   private
      FLayers     :array of TImpExpLayer;
      FNumLayers  :integer;
      FDfkFilename:string;
      FIniFilename:string;
      DfkFile     :TextFile;
      FReadObjects:integer;
      CurrLayerIdx :integer;
      CurrObjectIdx:integer;
      CurrLayer    :TImpExpLayer;

      CurrLineCnt  :integer;
      FHadError    :boolean;
      HadEndCode   :boolean;

      KoordinatenSatzRefs   :array of p_KoordinateSatzRef;
      NumKoordinatenSatzRefs:integer;

      // mapping tables
      SymbolNameTable:TStrings; // mapping table from Symbol number to symbol name
      SymbolDescTable:TStrings; // mapping table for symbol description

      KennungDescription_2000:TStrings;    // description mapping table for 2000er KennungsCodes
      KennungDescription_3000:TStrings;    // description mapping table for 3000er KennungsCodes
      KennungDescription_4000:TStrings;    // description mapping table for 4000er KennungsCodes
      KennungDescription_5000:TStrings;    // description mapping table for 5000er KennungsCodes
      KennungDescription_6000:TStrings;    // description mapping table for 5000er KennungsCodes
      KennungDescription_Various:TStrings; // various descriptions

      LineLayerMap:TStrings;               // layername mapping table for line objects
      SymbolLayerMap:TStrings;             // layername mapping table for symbol objects
      PointLayerMap :TStrings;             // layername mapping table for point objects

      TextLayerMap_2000:TStrings;               // layername mapping table for text objects
      TextLayerMap_3000:TStrings;               // layername mapping table for text objects
      TextLayerMap_4000:TStrings;               // layername mapping table for text objects
      TextLayerMap_5000:TStrings;               // layername mapping table for text objects

      function GetLayers(Index:integer):TImpExpLayer;
      procedure SetDfkFilename(const value:string);
      function GetDfkFilename:string;
      procedure SetIniFilename(const value:string);
      function GetIniFilename:string;
      function GetReadObjects:integer;

      procedure SetupCurrReadLayer(const aLayername:string);
      procedure AddKoordinateSatzRef(const Flurkarte:string; const PunktNummer:integer);
      function  GetCornerIndexByKoordianteRef(const Flurkarte:string; const PunktNummer:integer; var LayerIdx:integer):integer;

      function GonToDegree(const aAngle:double):double; // calculate Gon to Degrees
      procedure DosToWindows(var aText:string);         // procedure to convert Dos characters to Windows characters

      // mapping functions
      function  GetMappedSymbolName(const Symbolname:string; var Description:string):string;  // get name of symbol that should be used + description of symbol
      function  GetMappedLineLayername(const KennungsCode:integer):string;                    // get name of layer that should be used for line KennungsCodes
      function  GetMappedTextLayername(const KennungsCode:integer):string;                    // get name of layer that should be used for text KennungsCodes
      function  GetMappedPointLayername(const KennungsCode:integer):string;                    // get name of layer that should be used for point KennungsCodes
      function  GetMappedSymbolLayername(const aSymbolname:string):string;                    // get name of layer that should be used for symbols
      function  GetKennungsCodeDescription(const KennungsCode:integer):string;                // get description of KennungsCode

      // method to parse file
      function Read_StartSatz(var aLine:string; const aKennungCode:integer):longint;
      function Read_GebietsSatz(var aLine:string; const aKennungCode:integer):longint;
      function Read_Koordinatensatz(var aLine:string; const aKennungCode:integer):longint;
      function Read_LinienBogensatz(var aLine:string; const aKennungCode:integer):longint;
      function Read_SymbolSatz(var aLine:string; const aKennungCode:integer):longint;
      function Read_TextSatz(var aLine:string; const aKennungCode:integer):longint;

      // import methods
      procedure Import_Pixel(const aObject:TImpExpBaseObj);
      procedure Import_Poly(const aObject:TImpExpBaseObj);
      procedure Import_Text(const aObject:TImpExpBaseObj);
      procedure Import_Symbol(const aObject:TImpExpBaseObj);
   public
      constructor Create;
      destructor Destroy;
      property Layers[Index: Integer]:TImpExpLayer read GetLayers;
      property DfkFilename:string read GetDfkFilename write SetDfkFilename;
      property IniFilename:string read GetIniFilename write SetIniFilename;
      property ReadObjects:integer read GetReadObjects;
      property HadError:boolean read FHadError;
      function GetFileSize:longint;
      procedure OpenFile;
      procedure CloseFile;
      function  ReadElement:longint;
      procedure InitImport;
      procedure ImportObject;
end;

var StrFileObj    :ClassStrfile;

implementation
uses MainDlg, inifiles;

constructor TDfkReader.Create;
begin
   FNumLayers:=0;
   FDfkFilename:='';
   FIniFilename:='';
   FReadObjects:=0;
   CurrLayer:=nil;
   CurrLayerIdx:=0;
   NumKoordinatenSatzRefs:=0;
   CurrLineCnt:=0;
   FHadError:=FALSE;
   HadEndCode:=FALSE;
   // reset mapping tables
   SymbolNameTable:=nil;
   SymbolDescTable:=nil;
   KennungDescription_2000:=nil;
   KennungDescription_3000:=nil;
   KennungDescription_4000:=nil;
   KennungDescription_5000:=nil;
   KennungDescription_6000:=nil;
   KennungDescription_Various:=nil;
   LineLayerMap:=nil;
   TextLayerMap_2000:=nil;
   TextLayerMap_3000:=nil;
   TextLayerMap_4000:=nil;
   TextLayerMap_5000:=nil;
   SymbolLayerMap:=nil;
   PointLayerMap:=nil;
end;

destructor TDfkReader.Destroy;
var i:integer;
begin
   if FNumLayers > 0 then
   begin
      for i:=0 to FNumLayers -1 do
         FLayers[i].Destroy;
      SetLength(FLayers,0);
   end;
   CurrLayer:=nil;

   if NumKoordinatenSatzRefs > 0 then
   begin
      for i:=0 to NumKoordinatenSatzRefs-1 do
      begin
         SetLength(KoordinatenSatzRefs[i]^.KoordinateRefs,0);
         dispose (KoordinatenSatzRefs[i]);
      end;
      SetLength(KoordinatenSatzRefs, 0);
   end;
   // destroy mapping tables
   if SymbolNameTable <> nil then SymbolNameTable.Destroy;
   if SymbolDescTable <> nil then SymbolDescTable.Destroy;
   if KennungDescription_2000 <> nil then KennungDescription_2000.Destroy;
   if KennungDescription_3000 <> nil then KennungDescription_3000.Destroy;
   if KennungDescription_4000 <> nil then KennungDescription_4000.Destroy;
   if KennungDescription_5000 <> nil then KennungDescription_5000.Destroy;
   if KennungDescription_6000 <> nil then KennungDescription_6000.Destroy;
   if KennungDescription_Various <> nil then KennungDescription_Various.Destroy;
   if LineLayerMap <> nil then LineLayerMap.Destroy;
   if TextLayerMap_2000 <> nil then TextLayerMap_2000.Destroy;
   if TextLayerMap_3000 <> nil then TextLayerMap_3000.Destroy;
   if TextLayerMap_4000 <> nil then TextLayerMap_4000.Destroy;
   if TextLayerMap_5000 <> nil then TextLayerMap_5000.Destroy;
   if SymbolLayerMap <> nil then SymbolLayerMap.Destroy;
   if PointLayerMap <> nil then PointLayerMap.Destroy;
end;

function TDfkReader.GetLayers(Index:integer):TImpExpLayer;
begin
   result:=nil;
   if (Index < 0) or (Index > FNumLayers) then exit;
   result:=FLayers[Index];
end;

procedure TDfkReader.SetDfkFilename(const value:string);
begin
   FDfkFilename:=value;
end;

function TDfkReader.GetDfkFilename:string;
begin
   result:=FDfkFilename;
end;

procedure TDfkReader.SetIniFilename(const value:string);
begin
   FIniFilename:=value;
end;

function TDfkReader.GetIniFilename:string;
begin
   result:=FIniFilename;
end;

function TDfkReader.GetFileSize:longint;
var aFile:file of byte;
begin
   result:=0;
   if FDfkFilename = '' then exit;
   Filemode:=0;
   {$I-}
   AssignFile(aFile, FDfkFilename);
   Reset(aFile);
   result:=FileSize(aFile);
   Close(aFile);
   {$I+}
end;

procedure TDfkReader.OpenFile;
begin
   {$I-}
   AssignFile(DfkFile, FDfkFilename);
   Reset(DfkFile);
   {$I+}
end;

procedure TDfkReader.CloseFile;
begin
   {$I-}
   Close(DfkFile);
   {$I+}
   MainWnd.AXImpExpDbc.CloseImpTables;
end;

function TDfkReader.GetReadObjects:integer;
begin
   result:=FReadObjects;
end;

function TDfkReader.ReadElement:longint;
var retVal:longint;
    aLine:string;
    aItem:string;
    KennungCode:integer;
    count:integer;
begin
   retVal:=0;
   {$I-}
   if (Eof(DfkFile)) then
   begin
      aLine:=''; retVal:=9999; // return high value to make shure that file reading is finished
   end
   else
   begin
      readln(DfkFile,aLine); retVal:=length(aLine); CurrLineCnt:=CurrLineCnt+1;
   end;
   {$I+}
   // empty lines will not be handeled
   if aLine = '' then begin result:=retVal; exit; end;

   aItem:=copy(aLine,1,6); aItem:=StringReplace(aItem,' ','',[rfReplaceAll]);
   val(aItem, KennungCode, count); if count <> 0 then KennungCode:=-1;
   // now process the item
        if KennungCode = 9999                              then retVal:=retVal+Read_StartSatz(aLine, KennungCode)         // we got a StartSatz
   else if KennungCode = 9998                              then retVal:=retVal+Read_Gebietssatz(aLine, KennungCode)       // we got a GebietsSatz
   else if KennungCode = 9990                              then retVal:=retVal+Read_Koordinatensatz(aLine, KennungCode)   // we got a Koordinatensatz
   else if (KennungCode >= 6000) and (KennungCode <= 9899) then retVal:=retVal+Read_LinienBogensatz(aLine, KennungCode)   // we got a Linien/Bogen Satz
   else if (KennungCode = 4450) or (KennungCode = 5450)    then retVal:=retVal+Read_SymbolSatz(aLine, KennungCode)        // we got a SymbolSatz
   else if (KennungCode = 2450) or (KennungCode = 3450)    then retVal:=retVal+Read_SymbolSatz(aLine, KennungCode)        // we got a SymbolSatz Kartenrand
   else if (KennungCode >= 4000) and (KennungCode <= 5999) then retVal:=retVal+Read_TextSatz(aLine, KennungCode)          // we got a TextSatz
   else if (KennungCode >= 2000) and (KennungCode <= 3999) then retVal:=retVal+Read_TextSatz(aLine, KennungCode)          // we got a TextSatz Kartenrand
   else if KennungCode = 1 then begin HadEndCode:=TRUE; end // do nothing at EndSatz
   else if (aLine <> '') and (not HadEndCode) then // items after endcode will be ignored
   begin
      // we got an invalid line
      MainWnd.StatusLB.Items.Add(StrFileObj.Assign(51)+inttostr(CurrLineCnt));
      MainWnd.StatusLB.ItemIndex := MainWnd.StatusLB.Items.Count-1;
      MainWnd.StatusLB.Update;
      FHadError:=TRUE;
   end;
   // all other KennungCodes will be ignored
   result:=retVal;
end;

function TDfkReader.Read_SymbolSatz(var aLine:string; const aKennungCode:integer):longint;
var retVal:longint;
    FlurKarte:string;
    RechtsWert:double;
    HochWert:double;
    aItem:string;
    count:integer;
    Winkel:double;
    aName:string;
    newSymbol:TImpExpSymbol;
    aSymbolSize:double;
    aSymDescription:string;
    aLayername:string;
    Description:string;
begin
   retVal:=0;
   FlurKarte:=copy(aLine, 8, 15-8+1);
   aItem:=copy(aLine, 17, 25-17+1); val(aItem, RechtsWert, count); RechtsWert:=RechtsWert / 100;
   aItem:=copy(aLine, 27, 35-27+1); val(aItem, HochWert, count); HochWert:=HochWert / 100;
   aItem:=copy(aLine, 44, 49-44+1); val(aItem, aSymbolSize, count); // aSymbolSize:=aSymbolSize / 100;
   aItem:=copy(aLine, 51, 57-51+1); val(aItem, Winkel, count);
   aName:=copy(aLine, 59, 255);

   aLayername:=GetMappedSymbolLayername(aName);

   SetupCurrReadLayer(aLayername);
   newSymbol:=TImpExpSymbol.Create;
   newSymbol.X:=RechtsWert;
   newSymbol.Y:=HochWert;
   newSymbol.Angle:=GonToDegree(Winkel);
   // get mapped name
   aName:=GetMappedSymbolName(aName, aSymDescription);
   newSymbol.Name:=aName;
   newSymbol.Size:=aSymbolSize;

   if MainWnd.AXImpExpDbc.ImpDbMode <> 0 then
   begin
      if CurrLayer.NumSymbols = 0 then
      begin
         // create columns if they do not already exist
         if not CurrLayer.ColExists('Kennzahl')           then CurrLayer.AddColumnDef('Kennzahl',0 {col_Int} , 0);
         if not CurrLayer.ColExists('Beschreibung')       then CurrLayer.AddColumnDef('Beschreibung',4 {col_String} , 255);
         if not CurrLayer.ColExists('FlurKarte')          then CurrLayer.AddColumnDef('FlurKarte',4 {col_String} , 8);
         if not CurrLayer.ColExists('SymbolBeschreibung') then CurrLayer.AddColumnDef('SymbolBeschreibung',4 {col_String} , 255);
      end;
      // add attributes
      newSymbol.AddAttrib('Kennzahl',inttostr(aKennungCode));
      Description:=GetKennungsCodeDescription(aKennungCode);
      newSymbol.AddAttrib('Beschreibung',Description);
      newSymbol.AddAttrib('FlurKarte',FlurKarte);
      newSymbol.AddAttrib('SymbolBeschreibung',aSymDescription);
   end;
   CurrLayer.AddItem(newSymbol);
   FReadObjects:=FReadObjects+1;
   result:=0;
end;

procedure TDfkReader.DosToWindows(var aText:string);
var i:integer;
begin
   // map the characters
   for i:=0 to length(aText) do
   begin
      if aText[i] = CHR(148) then aText[i]:='�' else
      if aText[i] = CHR(153) then aText[i]:='�' else
      if aText[i] = CHR(132) then aText[i]:='�' else
      if aText[i] = CHR(142) then aText[i]:='�' else
      if aText[i] = CHR(129) then aText[i]:='�' else
      if aText[i] = CHR(154) then aText[i]:='�' else
      if aText[i] = CHR(225) then aText[i]:='�';
   end;
end;

function TDfkReader.GonToDegree(const aAngle:double):double;
var retVal:double;
begin
   // Gon circle has 400 Gon and starts with 90 Degrees
   retVal:=aAngle / 400 * 360;
        if (retVal > 0)   and (retVal <= 90)  then retVal:=90-retVal
   else if (retVal > 90)  and (retVal <= 180) then retVal:=360-retVal+90
   else if (retVal > 180) and (retVal <= 270) then retVal:=270-retVal+180
   else if (retVal > 370) and (retVal <  360) then retVal:=360-retVal+90;
   result:=360-retVal;
end;

function TDfkReader.Read_TextSatz(var aLine:string; const aKennungCode:integer):longint;
var retVal:longint;
    FlurKarte:string;
    RechtsWert:double;
    HochWert:double;
    aItem:string;
    count:integer;
    Winkel:double;
    aText:string;
    newText:TImpExpText;
    aTextHeight:double;
    aLayername:string;
    Description:string;
begin
   retVal:=0;
   FlurKarte:=copy(aLine, 8, 15-8+1);
   aItem:=copy(aLine, 17, 25-17+1); val(aItem, RechtsWert, count); RechtsWert:=RechtsWert / 100;
   aItem:=copy(aLine, 27, 35-27+1); val(aItem, HochWert, count); HochWert:=HochWert / 100;
   aItem:=copy(aLine, 44, 49-44+1); val(aItem, aTextHeight, count); aTextHeight:=aTextHeight / 100;
   aItem:=copy(aLine, 51, 57-51+1); val(aItem, Winkel, count);
   aText:=copy(aLine, 59, 255);

   aLayername:=GetMappedTextLayername(aKennungCode); // get mapped text layername

   SetupCurrReadLayer(aLayername);
   newText:=TImpExpText.Create;
   newText.X:=RechtsWert;
   newText.Y:=HochWert;

   newText.Angle:=GonToDegree(Winkel);
   DosToWindows(aText);
   newText.Text:=aText;
   newText.Height:=aTextHeight;
   newText.Font:='PROGIS DKM'; // PROGIS DKM is default font for DFK-Data

   if MainWnd.AXImpExpDbc.ImpDbMode <> 0 then
   begin
      if CurrLayer.NumTexts = 0 then
      begin
         // create columns if they do not already exist
         if not CurrLayer.ColExists('Kennzahl')           then CurrLayer.AddColumnDef('Kennzahl',0 {col_Int} , 0);
         if not CurrLayer.ColExists('Beschreibung')       then CurrLayer.AddColumnDef('Beschreibung',4 {col_String} , 255);
         if not CurrLayer.ColExists('FlurKarte')          then CurrLayer.AddColumnDef('FlurKarte',4 {col_String} , 8);
      end;
      // add attributes
      newText.AddAttrib('Kennzahl',inttostr(aKennungCode));
      Description:=GetKennungsCodeDescription(aKennungCode);
      newText.AddAttrib('Beschreibung',Description);
      newText.AddAttrib('FlurKarte',FlurKarte);
   end;
   CurrLayer.AddItem(newText);
   FReadObjects:=FReadObjects+1;
   result:=0;
end;

// method to read Linien/Bogensatz item
function TDfkReader.Read_LinienBogensatz(var aLine:string; const aKennungCode:integer):longint;
var retVal            :longint;
    FlurkarteAnfang   :string;
    FlurkarteEnde     :string;
    PunktNummerAnfang:integer;
    PunktNummerEnde  :integer;
    aItem             :string;
    count             :integer;
    aPolyline         :TImpExpPoly;
    aPoint            :TImpExpPixel;
    aIndex            :integer;
    StartIdx          :integer;
    EndIdx            :integer;
    LayerIdxAnfang    :integer;
    LayerIdxEnde      :integer;
    aLayername        :string;
    Description       :string;
begin
   retVal:=0;
   FlurKarteAnfang:=copy(aLine, 8, 15-8+1);
   aItem:=copy(aLine, 17, 20); val(aItem, PunktNummerAnfang, count);
   FlurKarteEnde:=copy(aLine, 22, 29-22+1);
   aItem:=copy(aLine, 31, 34-31+1); val(aItem, PunktNummerEnde, count);

   // now create polyline
   aLayername:=GetMappedLineLayername(aKennungCode);

   SetupCurrReadLayer(aLayername);
   StartIdx:=GetCornerIndexByKoordianteRef(FlurKarteAnfang, PunktNummerAnfang, LayerIdxAnfang);
   EndIdx:=GetCornerIndexByKoordianteRef(FlurKarteEnde, PunktNummerEnde, LayerIdxEnde);

   if (StartIdx < 0) or (EndIdx < 0) then
   begin
      // show warning that Linie/Bogen uses Corners that are still not defined
      MainWnd.ShowWarning(StrFileObj.Assign(43)+inttostr(CurrLineCnt));
      result:=retVal; exit;
   end; // could not create line if one of the index is less 0

   aPolyline:=TImpExpPoly.Create;
   aPoint:=FLayers[LayerIdxAnfang].Items[StartIdx] as TImpExpPixel;
   aPolyline.AddCorner(aPoint.X, aPoint.Y);
   aPoint:=FLayers[LayerIdxEnde].Items[EndIdx] as TImpExpPixel;
   aPolyline.AddCorner(aPoint.X, aPoint.Y);

   if MainWnd.AXImpExpDbc.ImpDbMode <> 0 then
   begin
      if CurrLayer.NumPolys = 0 then
      begin
         // create columns if they do not already exist
         if not CurrLayer.ColExists('Kennzahl')           then CurrLayer.AddColumnDef('Kennzahl',0 {col_Int} , 0);
         if not CurrLayer.ColExists('Beschreibung')       then CurrLayer.AddColumnDef('Beschreibung',4 {col_String} , 255);
      end;
      // add attributes
      aPolyline.AddAttrib('Kennzahl',inttostr(aKennungCode));
      Description:=GetKennungsCodeDescription(aKennungCode);
      aPolyline.AddAttrib('Beschreibung',Description);
   end;

   CurrLayer.AddItem(aPolyline);
   FReadObjects:=FReadObjects+1;
   result:=retVal;
end;

// method to read Koordinatensatz item
function TDfkReader.Read_Koordinatensatz(var aLine:string; const aKennungCode:integer):longint;
var retVal       :longint;
    Flurkarte    :string;
    PunktNummer  :integer;
    aItem        :string;
    count        :integer;
    RechtsWert   :double;
    HochWert     :double;
    AbmarkungsArt:string;
    ArtEntstehung:string;
    LetzterZugriff:string;
    aPoint       :TImpExpPixel;
    oldLen       :integer;
    Description  :string;
    aLayername   :string;
begin
   retVal:=0;
   FlurKarte:=copy(aLine, 8, 15-8+1);
   aItem:=copy(aLine, 17, 20-17+1); val(aItem, PunktNummer, count);
   aItem:=copy(aLine, 22, 30-22+1); val(aItem, RechtsWert, count); RechtsWert:=RechtsWert / 100;
   aItem:=copy(aLine, 32, 40-32+1); val(aItem, HochWert, count); HochWert:=HochWert / 100;
   AbmarkungsArt:=copy(aLine, 42, 43-42+1);
   ArtEntstehung:=copy(aLine, 45, 45-45+1);
   LetzterZugriff:=copy(aLine, 47, 56-47+1);

   aLayername:=GetMappedPointLayername(aKennungCode);
   SetupCurrReadLayer(aLayername);
   aPoint:=TImpExpPixel.Create;
   aPoint.X:=RechtsWert;
   aPoint.Y:=HochWert;
   if MainWnd.AXImpExpDbc.ImpDbMode <> 0 then
   begin
      if CurrLayer.NumPixels = 0 then
      begin
         // create columns if they do not already exist
         if not CurrLayer.ColExists('Kennzahl')       then CurrLayer.AddColumnDef('Kennzahl',0 {col_Int} , 0);
         if not CurrLayer.ColExists('Beschreibung')   then CurrLayer.AddColumnDef('Beschreibung',4 {col_String} , 255);
         if not CurrLayer.ColExists('FlurKarte')      then CurrLayer.AddColumnDef('FlurKarte',4 {col_String} , 8);
         if not CurrLayer.ColExists('PunktNummer')    then CurrLayer.AddColumnDef('PunktNummer',0 {col_Int} , 0);
         if not CurrLayer.ColExists('AbmarkungsArt')  then CurrLayer.AddColumnDef('AbmarkungsArt',4 {col_String}, 2);
         if not CurrLayer.ColExists('ArtEntstehung')  then CurrLayer.AddColumnDef('ArtEntstehung',4 {col_String}, 1);
         if not CurrLayer.ColExists('LetzterZugriff') then CurrLayer.AddColumnDef('LetzterZugriff',4 {col_String}, 10);
      end;
      // add attributes
      aPoint.AddAttrib('Kennzahl',inttostr(aKennungCode));
      Description:=GetKennungsCodeDescription(aKennungCode); aPoint.AddAttrib('Beschreibung',Description);
      aPoint.AddAttrib('FlurKarte',FlurKarte);
      aPoint.AddAttrib('PunktNummer',inttostr(PunktNummer));
      aPoint.AddAttrib('AbmarkungsArt',AbmarkungsArt);
      aPoint.AddAttrib('ArtEntstehung',ArtEntstehung);
      aPoint.AddAttrib('LetzterZugriff',LetzterZugriff);
   end;
   CurrLayer.AddItem(aPoint); // add Point to layer
   FReadObjects:=FReadObjects+1;

   // now get the refernce to this Point by using the PunktNummer
   AddKoordinateSatzRef(FlurKarte, Punktnummer);
   result:=retVal;
end;

// method to read Gebietssatz item
function TDfkReader.Read_GebietsSatz(var aLine:string; const aKennungCode:integer):longint;
var retVal:longint;
    aItem:string;
    RechtsWert:double;
    HochWert:double;
    count:integer;
begin
   retVal:=0;
   aItem:=copy(aLine, 8, 16-8+1); aItem:=Trim(aItem);  val(aItem, RechtsWert, count); RechtsWert:=RechtsWert / 100;
   aItem:=copy(aLine,18, 26-18+1); aItem:=Trim(aItem); val(aItem, HochWert, count); HochWert:=HochWert / 100;
   result:=retVal;
end;

// method to read Startsatz item
function TDfkReader.Read_StartSatz(var aLine:string; const aKennungCode:integer):longint;
var retVal                 :longint;
    Abgebestelle           :string;
    Datenaustauschverfahren:string;
    ZeitDatenausspielung   :string;
    aItem                  :string;
    count                  :integer;
    MinRechtsWert          :double;
    MinHochWert            :double;
    MaxRechtsWert          :double;
    MaxHochWert            :double;
    aIntvalue              :integer;
begin
    retVal:=0;
    Abgebestelle:=copy(aLine, 8, 12-8+1);
    Datenaustauschverfahren:=copy(aLine, 14, 15-14+1);
    ZeitDatenausspielung:=copy(aLine, 17, 26-17+1);

    aItem:=copy(aLine, 28, 36-28+1); aItem:=Trim(aItem);
    val(aItem, MinRechtsWert, count);  MinRechtsWert:=MinRechtsWert / 100;

    aItem:=copy(aLine, 38, 46-38+1); aItem:=Trim(aItem);
    val(aItem, aIntvalue, count);   MinHochWert:=MinHochWert / 100;

    aItem:=copy(aLine, 48, 56-48+1); aItem:=Trim(aItem);
    val(aItem, aIntvalue, count); MaxRechtsWert:=MaxRechtsWert / 100;

    aItem:=copy(aLine, 58, 66-58+1); aItem:=Trim(aItem);
    val(aItem, aIntvalue, count);   MaxHochWert:=MaxHochWert / 100;

    result:=retVal;
end;

procedure TDfkReader.InitImport;
begin
  CurrLayerIdx:=0;
  CurrObjectIdx:=0;
end;

procedure TDfkReader.SetupCurrReadLayer(const aLayername:string);
var i:integer;
    found:boolean;
begin
   if (CurrLayer <> nil) and (CurrLayer.Name = aLayername) then // current layer already set
      exit;

   // first check if already a layer with that name exists
   found:=FALSE;
   if FNumLayers > 0 then
   begin
      for i:=0 to FNumLayers-1 do
      begin
         if FLayers[i].Name = aLayername then
         begin
            CurrLayer:=FLayers[i];
            CurrLayerIdx:=i;
            found:=TRUE;
            break;
         end;
      end;
   end;
   if not found then
   begin
      CurrLayer:=TImpExpLayer.Create;
      CurrLayer.Name:=aLayername;
      FNumLayers:=FNumLayers+1;
      SetLength(FLayers, FNumLayers);
      FLayers[FNumlayers-1]:=CurrLayer;
      CurrLayerIdx:=FNumlayers-1;
   end;
end;

procedure TDfkReader.Import_Pixel(const aObject:TImpExpBaseObj);
var aPixel:TImpExpPixel;
    aId:integer;
    hasduplicate:boolean;
    i:integer;
    aAttrib:p_ImpExpAttrib;
begin
   aPixel:=aObject as TImpExpPixel;
   aId:=MainWnd.InsertPoint(aPixel.X, aPixel.Y,hasduplicate);
   if (not hasduplicate) and (aPixel.NumAttribs > 0) then
   begin
      MainWnd.AXImpExpDbc.AddImpIdInfo(CurrLayer.Name, aId);
      for i:=0 to aPixel.NumAttribs - 1 do
      begin
         aAttrib:=aPixel.Attribs[i];
         MainWnd.AXImpExpDbc.AddImpInfo(CurrLayer.Name, aAttrib^.Column, aAttrib^.Info);
      end;
   end;
end;

procedure TDfkReader.Import_Poly(const aObject:TImpExpBaseObj);
var aPoly:TImpExpPoly;
    aId:integer;
    hasduplicate:boolean;
    i:integer;
    aAttrib:p_ImpExpAttrib;
    aCorner:p_ImpExpCorner;
begin
   aPoly:=aObject as TImpExpPoly;
   MainWnd.CreatePolyline;
   for i:=0 to aPoly.NumCorners-1 do
   begin
      aCorner:=aPoly.Corners[i];
      MainWnd.InsertPointToPolyLine(aCorner^.X, aCorner^.Y);
   end;
   aId:=MainWnd.ClosePolyline(hasduplicate);
   if (not hasduplicate) and (aPoly.NumAttribs > 0) then
   begin
      MainWnd.AXImpExpDbc.AddImpIdInfo(CurrLayer.Name, aId);
      for i:=0 to aPoly.NumAttribs - 1 do
      begin
         aAttrib:=aPoly.Attribs[i];
         MainWnd.AXImpExpDbc.AddImpInfo(CurrLayer.Name, aAttrib^.Column, aAttrib^.Info);
      end;
   end;
end;

procedure TDfkReader.Import_Symbol(const aObject:TImpExpBaseObj);
var aSymbol:TImpExpSymbol;
    aId:integer;
    hasduplicate:boolean;
    i:integer;
    aAttrib:p_ImpExpAttrib;
begin
   aSymbol:=aObject as TImpExpSymbol;
   aId:=MainWnd.InsertSymbol(aSymbol.X, aSymbol.Y, aSymbol.Size, aSymbol.Angle, aSymbol.Name, hasduplicate);
   if (not hasduplicate) and (aSymbol.NumAttribs > 0) then
   begin
      MainWnd.AXImpExpDbc.AddImpIdInfo(CurrLayer.Name, aId);
      for i:=0 to aSymbol.NumAttribs - 1 do
      begin
         aAttrib:=aSymbol.Attribs[i];
         MainWnd.AXImpExpDbc.AddImpInfo(CurrLayer.Name, aAttrib^.Column, aAttrib^.Info);
      end;
   end;
end;

procedure TDfkReader.Import_Text(const aObject:TImpExpBaseObj);
var aText:TImpExpText;
    aId:integer;
    hasduplicate:boolean;
    i:integer;
    aAttrib:p_ImpExpAttrib;
begin
   aText:=aObject as TImpExpText;
   aId:=MainWnd.InsertText(aText.X, aText.Y, round(aText.Angle), aText.Text, round(aText.Height), aText.Font, 0, hasduplicate);
   if (not hasduplicate) and (aText.NumAttribs > 0) then
   begin
      MainWnd.AXImpExpDbc.AddImpIdInfo(CurrLayer.Name, aId);
      for i:=0 to aText.NumAttribs - 1 do
      begin
         aAttrib:=aText.Attribs[i];
         MainWnd.AXImpExpDbc.AddImpInfo(CurrLayer.Name, aAttrib^.Column, aAttrib^.Info);
      end;
   end;
end;

procedure TDfkReader.ImportObject;
var aLayername:string;
    i:integer;
    aCol:p_ImpExpDbColumn;
    aObject:TImpExpBaseObj;
begin
   if CurrLayerIdx >= FNumLayers then exit;
   if (CurrObjectIdx = 0) then
   begin
      CurrLayer:=FLayers[CurrLayerIdx];
      aLayername:=FLayers[CurrLayerIdx].Name;
      // create layer itself if it still does not exist
      if not MainWnd.CheckLayerExists(aLayername) then
         MainWnd.CreateLayer(aLayername);
      // create database for layer due to this is first object
      if (MainWnd.AXImpExpDbc.ImpDbMode <> 0) and (FLayers[CurrLayeridx].NumColumnDefs > 0) then
      begin
         MainWnd.AxImpExpDbc.CheckAndCreateImpTable(aLayername);
         MainWnd.AxImpExpDbc.AddImpIdColumn(aLayername);
         for i:=0 to FLayers[CurrLayerIdx].NumColumnDefs-1 do
         begin
            aCol:=FLayers[CurrLayerIdx].ColumnDefs[i];
            MainWnd.AXImpExpDbc.AddImpColumn(aLayername, aCol^.ColName, aCol^.ColType, aCol^.ColLength);
         end;
         MainWnd.AXImpExpDbc.CreateImpTable(aLayername);
         MainWnd.AXImpExpDbc.OpenImpTable(aLayername);
      end;
   end;

   // now import the object itself
   aObject:=FLayers[CurrLayerIdx].Items[CurrObjectIdx];
        if aObject.ObjectType = impexp_Pixel  then Import_Pixel(aObject)
   else if aObject.ObjectType = impexp_Poly   then Import_Poly(aObject)
   else if aObject.ObjectType = impexp_Text   then Import_Text(aObject)
   else if aObject.ObjectType = impexp_Symbol then Import_Symbol(aObject);

   CurrObjectIdx:=CurrObjectIdx+1;
   // check if it should be switched to next layer
   if CurrObjectIdx >= FLayers[CurrLayeridx].NumItems then
   begin
      CurrLayerIdx:=CurrLayerIdx+1;
      CurrObjectIdx:=0;
   end;
end;

function TDfkReader.GetCornerIndexByKoordianteRef(const Flurkarte:string; const PunktNummer:integer; var LayerIdx:integer):integer;
var retVal:integer;
    foundpos:integer;
    i:integer;
begin
   retVal:=-1; foundpos:=-1;
   if NumKoordinatenSatzRefs > 0 then
   begin
      for i:=0 to NumKoordinatenSatzRefs-1 do
      begin
         if KoordinatenSatzRefs[i]^.Flurkarte = Flurkarte then
         begin
            foundpos:=i;
            break;
         end;
      end;
   end;
   if foundpos >= 0 then
   begin
      if PunktNummer < KoordinatenSatzRefs[foundpos]^.NumKoordinateRefs then
      begin
         retVal:=KoordinatenSatzRefs[foundpos]^.KoordinateRefs[PunktNummer].CoordinateIdx;
         LayerIdx:=KoordinatenSatzRefs[foundpos]^.KoordinateRefs[PunktNummer].LayerIdx;
      end;
   end;
   result:=retVal;
end;

procedure TDfkReader.AddKoordinateSatzRef(const Flurkarte:string; const PunktNummer:integer);
var i, oldlen:integer;
    foundpos:integer;
    newitem:p_KoordinateSatzRef;
begin
   // first get the list of the KoordinateSatz references for this Flurkarte
   foundpos:=-1;
   if NumKoordinatenSatzRefs > 0 then
   begin
      for i:=0 to NumKoordinatenSatzRefs-1 do
      begin
         if KoordinatenSatzRefs[i]^.Flurkarte = Flurkarte then
         begin
            foundpos:=i;
            break;
         end;
      end;
   end;

   if foundpos < 0 then
   begin
      // add new reflist for Flurkarte
      newitem:=new(p_KoordinateSatzRef);
      newitem^.Flurkarte:=Flurkarte;
      newitem^.NumKoordinateRefs:=0;
      SetLength(newitem^.KoordinateRefs, 0);
      NumKoordinatenSatzRefs:=NumKoordinatenSatzRefs+1;
      SetLength(KoordinatenSatzRefs, NumKoordinatenSatzRefs);
      KoordinatenSatzRefs[NumKoordinatenSatzRefs-1]:=newitem;
      foundpos:=NumKoordinatenSatzRefs-1;
   end;
   oldLen:=KoordinatenSatzRefs[foundpos]^.NumKoordinateRefs-1;
   if oldLen < PunktNummer then
   begin
      SetLength(KoordinatenSatzRefs[foundpos]^.KoordinateRefs, PunktNummer+1);
      KoordinatenSatzRefs[foundpos]^.NumKoordinateRefs:=PunktNummer+1;
      for i:=oldLen+1 to KoordinatenSatzRefs[foundpos]^.NumKoordinateRefs-1 do // init with -1 to know which refs are used
      begin
         KoordinatenSatzRefs[foundpos]^.KoordinateRefs[i].CoordinateIdx:=-1;
         KoordinatenSatzRefs[foundpos]^.KoordinateRefs[i].LayerIdx:=-1;
      end;
   end;
   KoordinatenSatzRefs[foundpos]^.KoordinateRefs[PunktNummer].CoordinateIdx:=CurrLayer.NumItems-1;
   KoordinatenSatzRefs[foundpos]^.KoordinateRefs[PunktNummer].LayerIdx:=CurrLayerIdx;
end;

// method to get symbol name from number
function TDfkReader.GetMappedSymbolName(const Symbolname:string; var Description:string):string;
var SymNr, count:integer;
    retVal:string;
    inifile:TInifile;
    iniitems:TStrings;
    i:integer;
    aitem:string;
    dummy:string;
    SymName:string;
    SymDesc:string;
begin
   retVal:=Symbolname; Description:='';
   if (SymbolNameTable = nil) then
   begin
      SymbolNameTable:=TStringList.Create;
      SymbolDescTable:=TStringList.Create;
      if FileExists(IniFilename) then
      begin
         inifile:=TIniFile.Create(Inifilename);
         iniitems:=TStringList.Create;
         inifile.ReadSection('DFKSYMBOLMAP',iniitems);
         for i:=0 to iniitems.Count -1 do
         begin
            aitem:=inifile.ReadString('DFKSYMBOLMAP',iniitems[i],'');
            // first get the symbol number
            dummy:=copy(iniitems[i],7,length(iniitems[i]));
            val(dummy, SymNr, count);
            // now get the symbol name
            SymName:=copy(aitem,1,Pos(',',aitem)-1);
            delete(aItem,1,Pos(',',aitem));
            // now get the symbol description
            SymDesc:=aitem;
            if (SymbolNameTable.Count-1 < SymNr) then
            begin
               while (SymbolNameTable.Count <= SymNr) do
               begin
                  SymbolNameTable.Add('');
                  SymbolDescTable.Add('');
               end;
            end;
            SymbolNameTable[SymNr]:=Symname;
            SymbolDescTable[SymNr]:=SymDesc;
         end;
         iniitems.Destroy;
         inifile.Destroy;
      end;
   end;

   // the symbol name is a number in DFK-Format
   val(Symbolname, SymNr, count);

   if (SymNr < SymbolNameTable.Count) and (SymbolNameTable[Symnr] <> '') then
   begin
      retVal:=SymbolNameTable[SymNr];
      Description:=SymbolDescTable[SymNr];
   end;
   result:=retVal;
end;

// method to get symbol name from number
function TDfkReader.GetMappedSymbolLayername(const aSymbolname:string):string;
var SymNr, count:integer;
    retVal:string;
    inifile:TInifile;
    iniitems:TStrings;
    i:integer;
    aitem:string;
    aLayername:string;
    dummy:string;
begin
   retVal:='';
   if (SymbolLayerMap = nil) then
   begin
      SymbolLayerMap:=TStringList.Create;
      if FileExists(IniFilename) then
      begin
         inifile:=TIniFile.Create(Inifilename);
         iniitems:=TStringList.Create;
         inifile.ReadSection('DFKSYMBOLLAYERS',iniitems);
         for i:=0 to iniitems.Count -1 do
         begin
            aitem:=inifile.ReadString('DFKSYMBOLLAYERS',iniitems[i],'');
            // first get the symbol number
            dummy:=copy(iniitems[i],7,length(iniitems[i]));
            val(dummy, SymNr, count);
            // now get the layer name
            aLayername:=aItem;
            if (SymbolLayerMap.Count-1 < SymNr) then
            begin
               while (SymbolLayerMap.Count <= SymNr) do
                  SymbolLayerMap.Add('');
            end;
            SymbolLayerMap[SymNr]:=aLayername;
         end;
         iniitems.Destroy;
         inifile.Destroy;
      end;
   end;

   // the layername
   val(aSymbolname, SymNr, count);

   if (SymNr < SymbolLayerMap.Count) then
      retVal:=SymbolLayerMap[SymNr];

   if retVal = '' then // if layer could not be mapped -> use filename as layername
   begin
      retVal:=ExtractFilename(FDfkFilename);
      retVal:=copy(retVal,1,Pos('.',retVal)-1);
   end;
   result:=retVal;
end;

// get name of text layer that should be used for KennungsCode
function TDfkReader.GetMappedTextLayername(const KennungsCode:integer):string;
var retVal:string;
    aCode:integer;
    inifile    :TIniFile;
    iniitems   :TStrings;
    aItem      :string;
    i,count    :integer;
    aSearchList:TStrings;
begin
   retVal:='';
   if TextLayerMap_2000 = nil then
   begin
      // create KennungsCode mapping lists
      TextLayerMap_2000:=TStringList.Create;
      TextLayerMap_3000:=TStringList.Create;
      TextLayerMap_4000:=TStringList.Create;
      TextLayerMap_5000:=TStringList.Create;
      // read out the ini-items
      inifile:=TIniFile.Create(FIniFilename);
      iniitems:=TStringList.Create;
      inifile.ReadSection('DFKTEXTLAYERS',iniitems);
      for i:=0 to iniitems.Count-1 do
      begin
         aSearchList:=nil;
         aItem:=inifile.ReadString('DFKTEXTLAYERS',iniitems[i],'');
         val( copy(iniitems[i],7,length(iniitems[i])),aCode, count); // get number after Kennz_
              if (aCode >= 2000) and (aCode < 3000) then begin aCode:=aCode-2000; aSearchList:=TextLayerMap_2000; end
         else if (aCode >= 3000) and (aCode < 4000) then begin aCode:=aCode-3000; aSearchList:=TextLayerMap_3000; end
         else if (aCode >= 4000) and (aCode < 5000) then begin aCode:=aCode-4000; aSearchList:=TextLayerMap_4000; end
         else if (aCode >= 5000) and (aCode < 6000) then begin aCode:=aCode-5000; aSearchList:=TextLayerMap_5000; end;
         if aSearchList <> nil then
         begin
            if (aSearchList.Count-1 < aCode) then
            begin
               while (aSearchList.Count <= aCode) do
                  aSearchList.Add('');
            end;
            aSearchList[aCode]:=aItem;
         end;
      end;
      iniitems.Destroy;
      inifile.Destroy;
   end;

   // now get mapped Kennungscode
   aSearchList:=nil;
   aCode:=KennungsCode;
        if (aCode >= 2000) and (aCode < 3000) then begin aCode:=aCode-2000; aSearchList:=TextLayerMap_2000; end
   else if (aCode >= 3000) and (aCode < 4000) then begin aCode:=aCode-3000; aSearchList:=TextLayerMap_3000; end
   else if (aCode >= 4000) and (aCode < 5000) then begin aCode:=aCode-4000; aSearchList:=TextLayerMap_4000; end
   else if (aCode >= 5000) and (aCode < 6000) then begin aCode:=aCode-5000; aSearchList:=TextLayerMap_5000; end;
   if aSearchList <> nil then
   begin
      if aCode < aSearchList.Count then
         retVal:=aSearchList[aCode];
   end;
   if retVal = '' then // if layer could not be mapped -> use filename as layername
   begin
      retVal:=ExtractFilename(FDfkFilename);
      retVal:=copy(retVal,1,Pos('.',retVal)-1);
      MainWnd.ShowWarning(StrFileObj.Assign(54)+inttostr(KennungsCode)+' '+StrFileObj.Assign(55)+inttostr(CurrLineCnt));
   end;
   result:=retVal;
end;


// get name of line layer that should be used for KennungsCode
function TDfkReader.GetMappedLineLayername(const KennungsCode:integer):string;
var retVal:string;
    aCode:integer;
    inifile    :TIniFile;
    iniitems   :TStrings;
    aItem      :string;
    i,count    :integer;
begin
   retVal:='';
   if LineLayerMap = nil then
   begin
      // create KennungsCode mapping lists
      LineLayerMap:=TStringList.Create;
      // read out the ini-items
      inifile:=TIniFile.Create(FIniFilename);
      iniitems:=TStringList.Create;
      inifile.ReadSection('DFKLINELAYERS',iniitems);
      for i:=0 to iniitems.Count-1 do
      begin
         aItem:=inifile.ReadString('DFKLINELAYERS',iniitems[i],'');
         val( copy(iniitems[i],7,length(iniitems[i])),aCode, count); // get number after Kennz_
         aCode:=aCode-6000; // line codes have always code > 6000
         if (LineLayerMap.Count-1 < aCode) then
         begin
            while (LineLayerMap.Count <= aCode) do
               LineLayerMap.Add('');
         end;
         LineLayerMap[aCode]:=aItem;
      end;
      iniitems.Destroy;
      inifile.Destroy;
   end;

   // now get mapped Kennungscode
   aCode:=KennungsCode-6000; // line codes have always code > 6000
   if LineLayerMap <> nil then
   begin
      if aCode < LineLayerMap.Count then
         retVal:=LineLayerMap[aCode];
   end;
   if retVal = '' then // if layer could not be mapped -> use filename as layername
   begin
      retVal:=ExtractFilename(FDfkFilename);
      retVal:=copy(retVal,1,Pos('.',retVal)-1);
      MainWnd.ShowWarning(StrFileObj.Assign(54)+inttostr(KennungsCode)+' '+StrFileObj.Assign(55)+inttostr(CurrLineCnt));
   end;
   result:=retVal;
end;

// get name of point layer that should be used for KennungsCode
function TDfkReader.GetMappedPointLayername(const KennungsCode:integer):string;
var retVal:string;
    aCode:integer;
    inifile    :TIniFile;
    iniitems   :TStrings;
    aItem      :string;
    i,count    :integer;
begin
   retVal:='';
   if PointLayerMap = nil then
   begin
      // create KennungsCode mapping lists
      PointLayerMap:=TStringList.Create;
      // read out the ini-items
      inifile:=TIniFile.Create(FIniFilename);
      iniitems:=TStringList.Create;
      inifile.ReadSection('DFKPOINTLAYERS',iniitems);
      for i:=0 to iniitems.Count-1 do
      begin
         aItem:=inifile.ReadString('DFKPOINTLAYERS',iniitems[i],'');
         val( copy(iniitems[i],7,length(iniitems[i])),aCode, count); // get number after Kennz_
         aCode:=aCode-9990; // line codes have always code > 9990
         if (PointLayerMap.Count-1 < aCode) then
         begin
            while (PointLayerMap.Count <= aCode) do
               PointLayerMap.Add('');
         end;
         PointLayerMap[aCode]:=aItem;
      end;
      iniitems.Destroy;
      inifile.Destroy;
   end;

   // now get mapped Kennungscode
   aCode:=KennungsCode-9990; // point codes have always code > 9990
   if PointLayerMap <> nil then
   begin
      if aCode < PointLayerMap.Count then
         retVal:=PointLayerMap[aCode];
   end;
   if retVal = '' then // if layer could not be mapped -> use filename as layername
   begin
      retVal:=ExtractFilename(FDfkFilename);
      retVal:=copy(retVal,1,Pos('.',retVal)-1);
      MainWnd.ShowWarning(StrFileObj.Assign(54)+inttostr(KennungsCode)+' '+StrFileObj.Assign(55)+inttostr(CurrLineCnt));
   end;
   result:=retVal;
end;

// get Description of KennungsCode
function TDfkReader.GetKennungsCodeDescription(const KennungsCode:integer):string;   // get description of KennungsCode
var retVal:string;
    aCode:integer;
    aSearchList:TStrings;
    inifile    :TIniFile;
    iniitems   :TStrings;
    aItem      :string;
    i,count    :integer;
begin
   retVal:=''; aSearchList:=nil;
   if KennungDescription_2000 = nil then
   begin
      // create KennungsCode mapping lists
      KennungDescription_2000:=TStringList.Create;
      KennungDescription_3000:=TStringList.Create;
      KennungDescription_4000:=TStringList.Create;
      KennungDescription_5000:=TStringList.Create;
      KennungDescription_6000:=TStringList.Create;
      KennungDescription_Various:=TStringList.Create;
      // read out the ini-items
      inifile:=TIniFile.Create(FIniFilename);
      iniitems:=TStringList.Create;
      inifile.ReadSection('DFKDESCRIPTION',iniitems);
      for i:=0 to iniitems.Count-1 do
      begin
         aItem:=inifile.ReadString('DFKDESCRIPTION',iniitems[i],'');
         val( copy(iniitems[i],7,length(iniitems[i])),aCode, count); // get number after Kennz_
         aSearchList:=nil;
              if (aCode >= 2000) and (aCode < 3000) then begin aCode:=aCode-2000; aSearchList:=KennungDescription_2000; end
         else if (aCode >= 3000) and (aCode < 4000) then begin aCode:=aCode-3000; aSearchList:=KennungDescription_3000; end
         else if (aCode >= 4000) and (aCode < 5000) then begin aCode:=aCode-4000; aSearchList:=KennungDescription_4000; end
         else if (aCode >= 5000) and (aCode < 6000) then begin aCode:=aCode-5000; aSearchList:=KennungDescription_5000; end
         else if (aCode >= 6000) and (aCode < 7000) then begin aCode:=aCode-6000; aSearchList:=KennungDescription_6000; end;
         if aSearchList <> nil then
         begin
            if (aSearchList.Count-1 < aCode) then
            begin
               while (aSearchList.Count <= aCode) do
                  aSearchList.Add('');
            end;
            aSearchList[aCode]:=aItem;
         end
         else
         begin
            // add whole line to various items
            KennungDescription_Various.Add(iniitems[i]+','+aItem);
         end;
      end;
      iniitems.Destroy;
      inifile.Destroy;
   end;

   // now get mapped Kennungscode
   aCode:=KennungsCode;
        if (aCode >= 2000) and (aCode < 3000) then begin aCode:=aCode-2000; aSearchList:=KennungDescription_2000; end
   else if (aCode >= 3000) and (aCode < 4000) then begin aCode:=aCode-3000; aSearchList:=KennungDescription_3000; end
   else if (aCode >= 4000) and (aCode < 5000) then begin aCode:=aCode-4000; aSearchList:=KennungDescription_4000; end
   else if (aCode >= 5000) and (aCode < 6000) then begin aCode:=aCode-5000; aSearchList:=KennungDescription_5000; end
   else if (aCode >= 6000) and (aCode < 7000) then begin aCode:=aCode-6000; aSearchList:=KennungDescription_6000; end;
   if aSearchList <> nil then
   begin
      if aCode < aSearchList.Count then
         retVal:=aSearchList[aCode];
   end
   else if KennungDescription_Various <> nil then
   begin
      // search in various items
      for i:=0 to KennungDescription_Various.count-1 do
      begin
         aItem:=KennungDescription_Various[i];
         val(copy(aItem,7,Pos(',',aItem)-1),aCode, count);
         if aCode = KennungsCode then
         begin
            retVal:=copy(aitem,Pos(',',aitem)+1, length(aitem));
            break;
         end;
      end;
   end;
   result:=retVal;
end;

end.
