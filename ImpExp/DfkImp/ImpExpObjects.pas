unit ImpExpObjects;

interface
uses SysUtils;

const
      // Object types
      impexp_Pixel     = 1;
      impexp_Poly      = 2;
      impexp_CPoly     = 4;
      impexp_Text      = 7;
      impexp_Symbol    = 8;
      impexp_Spline    = 9;
      impexp_Image     = 10;
      impexp_MesLine   = 11;
      impexp_Circle    = 12;
      impexp_Arc       = 13;

type

// record to store attributes
p_ImpExpAttrib = ^ImpExpAttrib;
ImpExpAttrib = record
   Column:string;
   Info  :string;
end;

// record to store database column definition
p_ImpExpDbColumn = ^ImpExpDbColumn;
ImpExpDbColumn = record
   ColName   :string;
   ColType   :integer;
   ColLength:integer;
end;

p_ImpExpCorner=^ImpExpCorner;
ImpExpCorner = record
   X:double;
   Y:double;
end;

type
// base class for import/export objects
TImpExpBaseObj = class
   private
      // private definitions
      FObjectType :integer;                 // object type of object
      FAttribs    :array of p_ImpExpAttrib; // reference to attributes
      FNumAttribs :integer;                 // Number of attributes

      // methods to support properties
      procedure SetObjectType(const value:integer);
      function  GetObjectType:integer;
      function  GetNumAttribs:integer;
      function  GetAttrib(Index:integer):p_ImpExpAttrib;
   public
      // public definitions
      constructor Create;
      destructor  Destroy; virtual;

      property    ObjectType:integer read GetObjectType write SetObjectType;
      property    NumAttribs:integer read GetNumAttribs;
      property    Attribs[Index: Integer]:p_ImpExpAttrib read GetAttrib;

      procedure   AddAttrib(const aColumn:string; const aInfo:string);
      function    GetAttribInfoByColumn(const aColumn:string):string;
end;

// class for pixel
TImpExpPixel = class (TImpExpBaseObj)
   private
      // private definitions
      FX,FY:double;
      procedure SetX(const value:double);
      function  GetX:double;
      procedure SetY(const value:double);
      function  GetY:double;
   public
      // public definitions
      constructor Create;
      destructor Destroy;
      property X:double read GetX write SetX;
      property Y:double read GetY write SetY;
end;

// class for polyline
TImpExpPoly = class (TImpExpBaseObj)
   private
      // private definitions
      FCorners    :array of p_ImpExpCorner;
      FNumCorners:integer;
      function GetCorners(Index:integer):p_ImpExpCorner;
      function GetNumCorners:integer;
   public
      // public definitions
      constructor Create;
      destructor Destroy;
      property Corners[Index: Integer]:p_ImpExpCorner read GetCorners;
      property NumCorners:integer read GetNumCorners;
      procedure AddCorner(const X:double; const Y:double);
end;

// class for polygon
TImpExpCPoly = class (TImpExpBaseObj)
   private
      // private definitions
      FCorners    :array of p_ImpExpCorner;
      FNumCorners:integer;
      function GetCorners(Index:integer):p_ImpExpCorner;
      function GetNumCorners:integer;
   public
      // public definitions
      constructor Create;
      destructor Destroy;
      property Corners[Index: Integer]:p_ImpExpCorner read GetCorners;
      property NumCorners:integer read GetNumCorners;
      procedure AddCorner(const X:double; const Y:double);
end;

// class for texts
TImpExpText = class (TImpExpBaseObj)
   private
      // private definitions
      FAngle:double;
      FFont:string;
      FText:string;
      FX,FY:double;
      FHeigth:double;

      function GetX:double;
      procedure SetX(const value:double);
      function GetY:double;
      procedure SetY(const value:double);
      function GetAngle:double;
      procedure SetAngle(const value:double);
      function GetFont:string;
      procedure SetFont(const value:string);
      function GetText:string;
      procedure SetText(const value:string);
      procedure SetHeight(const value:double);
      function GetHeigth:double;
   public
      // public definitions
      constructor Create;
      destructor Destroy;
      property X:double read GetX write SetX;
      property Y:double read GetY write SetY;
      property Height:double read GetHeigth write SetHeight;
      property Angle:double read GetAngle write SetAngle;
      property Font:string read GetFont write SetFont;
      property Text:string read GetText write SetText;
end;

// class for symbol
TImpExpSymbol = class (TImpExpBaseObj)
   private
      // private definitions
      FAngle:double;
      FName:string;
      FX,FY:double;
      FSize:double;

      function GetX:double;
      procedure SetX(const value:double);
      function GetY:double;
      procedure SetY(const value:double);
      function GetAngle:double;
      procedure SetAngle(const value:double);
      function GetName:string;
      procedure SetName(const value:string);
      procedure SetSize(const value:double);
      function GetSize:double;
   public
      // public definitions
      constructor Create;
      destructor Destroy;
      property X:double read GetX write SetX;
      property Y:double read GetY write SetY;
      property Angle:double read GetAngle write SetAngle;
      property Name:string read GetName write SetName;
      property Size:double read GetSize write SetSize;
end;

// class for layer
TImpExpLayer = class
   private
      ColDefs    :array of p_ImpExpDbColumn; // database column definitions
      NumColDefs:integer;

      FName      :string; // name of layer

      Objects    :array of TImpExpBaseObj; // collection of objects
      NumObjects :integer;

      Num_Pixel :integer;
      Num_Poly  :integer;
      Num_CPoly :integer;
      Num_Text  :integer;
      Num_Symbol:integer;

      procedure SetName(const aName:string);
      function  GetName:string;
      function  GetItems(Index:integer):TImpExpBaseObj;
      function  GetNumItems:integer;
      function  GetNumColumnDefs:integer;
      function  GetColumnDef(Index:integer):p_ImpExpDbColumn;
   public
      // public definitions
      constructor Create;
      destructor  Destroy;

      property  Name:string read GetName write SetName;
      property  Items[Index: Integer]:TImpExpBaseObj read GetItems;
      property  NumItems:integer read GetNumItems;
      property  NumPixels:integer  read Num_Pixel;
      property  NumCPolys:integer  read Num_CPoly;
      property  NumPolys:integer   read Num_Poly;
      property  NumTexts:integer   read Num_Text;
      property  NumSymbols:integer read Num_Symbol;
      property  NumColumnDefs:integer read GetNumColumnDefs;
      property  ColumnDefs[Index: Integer]:p_ImpExpDbColumn read GetColumnDef;
      function  ColExists(const aColname:string):boolean;
      procedure  AddColumnDef(const aColname:string; const aColType:integer; const aColLength:integer);
      procedure  AddItem(const aObject:TImpExpBaseObj);
end;


implementation

// ------------------------ IMPEXPBASEOBJ --------------------------------------

constructor TImpExpBaseObj.Create;
begin
   FObjectType:=0;
   FNumAttribs:=0;
end;

destructor TImpExpBaseObj.Destroy;
var i:integer;
begin
   if FNumAttribs > 0 then
   begin
      for i:=0 to FNumAttribs-1 do
         dispose (FAttribs[i]);
   end;
   SetLength(FAttribs, 0);
end;

procedure TImpExpBaseObj.SetObjectType(const value:integer);
begin
   FObjectType:=value;
end;

function TImpExpBaseObj.GetObjectType:integer;
begin
   result:=FObjectType;
end;

procedure TImpExpBaseObj.AddAttrib(const aColumn:string; const aInfo:string);
var new_attrib:p_ImpExpAttrib;
begin
   new_attrib:=new(p_ImpExpAttrib);
   new_attrib^.Column:=aColumn;
   new_attrib^.Info:=aInfo;
   FNumAttribs:=FNumAttribs+1;
   SetLength(FAttribs, NumAttribs);
   FAttribs[NumAttribs-1]:=new_attrib;
end;


function TImpExpBaseObj.GetNumAttribs:integer;
begin
   result:=FNumAttribs;
end;

function TImpExpBaseObj.GetAttrib(Index:integer):p_ImpExpAttrib;
begin
   result:=nil;
   if (Index >= FNumAttribs) or (Index < 0) then exit;
   result:=FAttribs[Index];
end;

function TImpExpBaseObj.GetAttribInfoByColumn(const aColumn:string):string;
var retVal:string;
    i:integer;
begin
   retVal:='';
   for i:=0 to FNumAttribs-1 do
   begin
      if FAttribs[i]^.Column = aColumn then
      begin
         retVal:=FAttribs[i]^.Info;
         break;
      end;
   end;
   result:=retVal;
end;

// ------------------------ IMPEXPPIXEL ----------------------------------------

constructor TImpExpPixel.Create;
begin
   inherited Create;
   ObjectType:=impexp_Pixel;
   X:=0;
   Y:=0;
end;

destructor TImpExpPixel.Destroy;
begin
   inherited Destroy;
end;

procedure TImpExpPixel.SetX(const value:double);
begin
   FX:=value;
end;

function TImpExpPixel.GetX:double;
begin
   result:=FX;
end;

procedure TImpExpPixel.SetY(const value:double);
begin
   FY:=value;
end;

function TImpExpPixel.GetY:double;
begin
   result:=FY;
end;

// ------------------------ IMPEXPPOLY -----------------------------------------
constructor TImpExpPoly.Create;
begin
   inherited Create;
   ObjectType:=impexp_Poly;
   FNumCorners:=0;
end;

destructor TImpExpPoly.Destroy;
var i:integer;
begin
   inherited Destroy;
   if FNumCorners > 0 then
   begin
      for i:=0 to FNumCorners-1 do
         dispose(FCorners[i]);
      Setlength(FCorners,0);
   end;
end;

function TImpExpPoly.GetCorners(Index:integer):p_ImpExpCorner;
var retVal:p_ImpExpCorner;
begin
   result:=nil;
   if (Index < 0) or (Index >= FNumCorners) then exit;
   retVal:=FCorners[Index];
   result:=retVal;
end;

function TImpExpPoly.GetNumCorners:integer;
begin
   result:=FNumCorners;
end;

procedure TImpExpPoly.AddCorner(const X:double; const Y:double);
var newitem:p_ImpExpCorner;
begin
   newitem:=new(p_ImpExpCorner);
   newitem^.X:=X;
   newitem^.Y:=Y;
   FNumCorners:=FNumCorners+1;
   Setlength(FCorners, FNumCorners);
   FCorners[FNumCorners-1]:=newitem;
end;

// ------------------------ IMPEXPCPOLY -----------------------------------------

constructor TImpExpCPoly.Create;
begin
   inherited Create;
   ObjectType:=impexp_CPoly;
   FNumCorners:=0;
end;

destructor TImpExpCPoly.Destroy;
var i:integer;
begin
   inherited Destroy;
   if FNumCorners > 0 then
   begin
      for i:=0 to FNumCorners-1 do
         dispose(FCorners[i]);
      Setlength(FCorners,0);
   end;
end;

function TImpExpCPoly.GetCorners(Index:integer):p_ImpExpCorner;
var retVal:p_ImpExpCorner;
begin
   result:=nil;
   if (Index < 0) or (Index >= FNumCorners) then exit;
   retVal:=FCorners[Index];
   result:=retVal;
end;

function TImpExpCPoly.GetNumCorners:integer;
begin
   result:=FNumCorners;
end;

procedure TImpExpCPoly.AddCorner(const X:double; const Y:double);
var newitem:p_ImpExpCorner;
begin
   newitem:=new(p_ImpExpCorner);
   newitem^.X:=X;
   newitem^.Y:=Y;
   FNumCorners:=FNumCorners+1;
   Setlength(FCorners, FNumCorners);
   FCorners[FNumCorners-1]:=newitem;
end;

// ------------------------ IMPEXPTEXT ----------------------------------------

constructor TImpExpText.Create;
begin
   inherited Create;
   ObjectType:=impexp_Text;
   FX:=0;
   FY:=0;
   FFont:='Courier New';
   FText:='';
end;

destructor TImpExpText.Destroy;
begin
   inherited Destroy;
end;

procedure TImpExpText.SetX(const value:double);
begin
   FX:=value;
end;

function TImpExpText.GetX:double;
begin
   result:=FX;
end;

procedure TImpExpText.SetY(const value:double);
begin
   FY:=value;
end;

function TImpExpText.GetY:double;
begin
   result:=FY;
end;

function TImpExpText.GetAngle:double;
begin
   result:=FAngle;
end;

procedure TImpExpText.SetAngle(const value:double);
begin
   FAngle:=value;
end;

function TImpExpText.GetFont:string;
begin
   result:=FFont;
end;

procedure TImpExpText.SetFont(const value:string);
begin
   FFont:=value;
end;

function TImpExpText.GetText:string;
begin
   result:=FText;
end;

procedure TImpExpText.SetText(const value:string);
begin
   FText:=value;
end;


procedure TImpExpText.SetHeight(const value:double);
begin
   FHeigth:=value;
end;

function TImpExpText.GetHeigth:double;
begin
   result:=FHeigth;
end;

// ------------------------ IMPEXPSYMBOL ---------------------------------------

constructor TImpExpSymbol.Create;
begin
   inherited Create;
   ObjectType:=impexp_Symbol;
   FX:=0;
   FY:=0;
   FName:='';
   FSize:=0;
end;

destructor TImpExpSymbol.Destroy;
begin
   inherited Destroy;
end;

procedure TImpExpSymbol.SetX(const value:double);
begin
   FX:=value;
end;

function TImpExpSymbol.GetX:double;
begin
   result:=FX;
end;

procedure TImpExpSymbol.SetY(const value:double);
begin
   FY:=value;
end;

function TImpExpSymbol.GetY:double;
begin
   result:=FY;
end;

function TImpExpSymbol.GetAngle:double;
begin
   result:=FAngle;
end;

procedure TImpExpSymbol.SetAngle(const value:double);
begin
   FAngle:=value;
end;

function TImpExpSymbol.GetName:string;
begin
   result:=FName;
end;

procedure TImpExpSymbol.SetName(const value:string);
begin
   FName:=value;
end;


procedure TImpExpSymbol.SetSize(const value:double);
begin
   FSize:=value;
end;

function TImpExpSymbol.GetSize:double;
begin
   result:=FSize;
end;

// ------------------------ IMPEXPLAYER ----------------------------------------

constructor TImpExpLayer.Create;
begin
   NumColDefs:=0;
   FName:='';
   NumObjects:=0;
   Num_Pixel:=0;
   Num_Poly:=0;
   Num_CPoly:=0;
   Num_Text:=0;
   Num_Symbol:=0;
end;

destructor TImpExpLayer.Destroy;
var i:integer;
begin
   if NumColDefs > 0 then
   begin
      for i:=0 to NumColDefs-1 do
      begin
         dispose(ColDefs[i]);
      end;
      SetLength(ColDefs, 0);
   end;

   if NumObjects > 0 then
   begin
      for i:=0 to NumObjects-1 do
      begin
         Objects[i].Destroy;
      end;
      SetLength(Objects,0);
   end;
end;

procedure TImpExpLayer.SetName(const aName:string);
begin
   FName:=aName;
end;

function TImpExpLayer.GetName:string;
begin
   result:=FName;
end;

function TImpExpLayer.GetNumColumnDefs:integer;
begin
   result:=NumColDefs;
end;

procedure TImpExpLayer.AddColumnDef(const aColname:string; const aColType:integer; const aColLength:integer);
var new_item:p_ImpExpDbColumn;
begin
   new_item:=new(p_ImpExpDbColumn);
   new_item^.ColName:=aColName;
   new_item^.ColType:=aColType;
   new_item^.ColLength:=aColLength;
   NumColDefs:=NumColDefs+1;
   SetLength(ColDefs, NumColDefs);
   ColDefs[NumColDefs-1]:=new_item;
end;

function TImpExpLayer.GetColumnDef(Index:integer):p_ImpExpDbColumn;
begin
   result:=nil;
   if (Index >= NumColDefs) or (Index < 0) then exit;
   result:=ColDefs[Index];
end;

procedure TImpExpLayer.AddItem(const aObject:TImpExpBaseObj);
begin
   NumObjects:=NumObjects+1;
   SetLength(Objects, NumObjects);
   Objects[NumObjects-1]:=aObject;

   // update counters
   if aObject.ObjectType = impexp_Pixel then Num_Pixel:=Num_Pixel+1
   else if aObject.ObjectType = impexp_Poly then Num_Poly:=Num_Poly+1
   else if aObject.ObjectType = impexp_CPoly then Num_CPoly:=Num_CPoly+1
   else if aObject.ObjectType = impexp_Text then Num_Text:=Num_Text+1
   else if aObject.ObjectType = impexp_Symbol then Num_Symbol:=Num_Symbol+1;
end;

function TImpExpLayer.GetItems(Index:integer):TImpExpBaseObj;
begin
   result:=nil;
   if (Index >= NumObjects) or (Index < 0) then exit;
   result:=Objects[Index];
end;

function TImpExpLayer.GetNumItems:integer;
begin
   result:=NumObjects;
end;

function TImpExpLayer.ColExists(const aColname:string):boolean;
var retVal:boolean;
    i:integer;
begin
   retVal:=FALSE;
   if NumColDefs > 0 then
   begin
      for i:=0 to NumColDefs -1 do
      begin
         if ColDefs[i]^.ColName = aColname then
         begin
            retVal:=TRUE;
            break;
         end;
      end;
   end;
   result:=retVal;
end;

end.
