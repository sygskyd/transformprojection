unit AxGisCombineImpl;

interface

uses
  Windows, ActiveX, Classes, Controls, Graphics, Menus, Forms, StdCtrls,
  ComServ, StdVCL, AXCtrls, AxGisCmb_TLB, ExtCtrls, StrFile, WINGIS_TLB,
  ObjectRefKeeper, ObjectRefFilter;

const
       // Object types
  ot_Pixel = 1;
  ot_Poly = 2;
  ot_CPoly = 4;
  ot_Text = 7;
  ot_Symbol = 8;
  ot_Spline = 9;
  ot_Image = 10;
  ot_MesLine = 11;
  ot_Circle = 12;
  ot_Arc = 13;
  ot_Layer = 18;

       // Processing Modes
  pro_NORMAL = 0;
  pro_DKM_BEV = 1;
  pro_DKM_MEDIX = 2;
  pro_DKM_DFK = 3;

type
  TAxGisCombine = class(TActiveXControl, IAxGisCombine)
  private
    { Private declarations }
    FDelphiControl: TPanel;
    FEvents: IAxGisCombineEvents;

    // private variables
    StrFileObj: ClassStrFile; // String-File object


    App: OleVariant; // Pointer to DX-Cobject
    Document: IDocument; // Pointer to current document
    CurrPercentInfo: integer; // string that contains current percent-info
    FetchRadius: double; // Fetchradius that should be used

    // private variables for properties
    FWorkingDir: string; // current working-dir
    FLngCode: string; // current language-code
    FDBName: string; // database-name
    FDBTable: string; // database-table-name
    FDBMode: integer; // database-mode

    // private variables
    Selected_Stack: array of integer; // Stack that is used for the selected objects
    Selected_StackSize: integer; // Size of the stack for the selected objects
    Selected_StackLayernames: TStrings; // reference to layernames in the selected stack
    Selected_StackNesting: integer; // nesting of stack (to avoid creation and delete of stack in nested functions)

    Database_Nesting: integer; // nesting that is used for database handling (to avoid close still used database)
    Manipulation_Nesting: integer; // nesting that is used for manipulation handlign (to avoid close of still used manipulation object)
    StrFile_Nesting: integer; // nesting that is used for stringfile

    ProcessingMode: integer; // Flag that shows which Processing mode is currently done (Normal, DKM_BEV, DKM_MEDIX)

    // methods to handle selected stack
    procedure Create_SelectedStack;
    procedure Destroy_SelectedStack;

    // methods to handle database
    procedure Create_DatabaseInstance;
    procedure Destroy_DatabaseInstance;

    // methods to handle manipulation object
    procedure Create_ManipulationInstance;
    procedure Destroy_ManipulationInstance;

    // methods to handle string-file
    procedure Create_StringFileInstance;
    procedure Destroy_StringFileInstance;
    procedure RemoveLayerFromProject(const aLayername: string);
    procedure EmptyLayerInProject(const aLayername: string);

    // general methods
    function CheckLayerExists(Layername: string): boolean;
    procedure CreateLayer(Layername: string);
    function GetNumObjectsOfLayer(Layername: string): integer;

    // private methods
    procedure FillObjectKeeper(var aFilter: TObjectRefFilter; var aKeeper: TObjectRefKeeper; DataSource: AxCmbDataSource);
    procedure GenerateObjects(const LayersToProcess: TStrings; const TargetLayer: string; DataSource: AxCmbDataSource);
    procedure CopyNonDuplicatesToLayer(SourceLayer: string; TargetLayer: string; ProcessDatabase: boolean; DataSource: AxCmbDataSource);

    // combine methods
    procedure CombineCPolyRefsWithSymbolRefs(var CPolys: TObjectRefKeeper; var Symbols: TObjectRefKeeper; const Setup: AxCmbSymbolsWithCPolysSetup; DataSource: AxCmbDataSource);
    procedure CombineCPolyRefsWithTextRefs(var CPolys: TObjectRefKeeper; var Texts: TObjectRefKeeper; const Setup: AxCmbTextsWithCPolysSetup; DataSource: AxCmbDataSource);

    // DKM methods
    procedure DKM_Process_LayersToDelete(const Filename: string; const SectionName: string);
    procedure DKM_Process_LayersToMerge(const Filename: string; const SectionName: string);

    // BEV methods
    procedure BEV_Process_Parzellen(const Filename: string; Datasource: AxCmbDataSource);
    procedure BEV_Process_Mappenblatt(const Filename: string; Datasource: AxCmbDataSource);
    procedure BEV_Process_Nutzungen(const Filename: string; Datasource: AxCmbDataSource);
    procedure BEV_Create_Nutzungen(const Filename: string; Datasource: AxCmbDataSource);
    procedure BEV_Combine_Nutzungen(const Filename: WideString; Datasource: AxCmbDataSource);

    procedure ShowPercent(MaxElems: integer; CurrElems: integer);
    function CalculateLength(X1: double; Y1: double; X2: double; Y2: double): double;

    // methods for callbacks of database or manipulation object
    procedure OnReceivedObjManDisplayPass(Sender: TObject; const aPass: WideString);
    procedure OnReceivedObjManDisplayPercent(Sender: TObject; aValue: Integer);
    procedure OnRequestExpLayerIndex(Sender: TObject; const Layername: WideString; var LayerIndex: Integer);
    procedure OnRequestExpProjectName(Sender: TObject; var ProjectName: WideString);
    procedure OnRequestImpLayerIndex(Sender: TObject; const Layername: WideString; var LayerIndex: Integer);
    procedure OnRequestImpProjectName(Sender: TObject; var ProjectName: WideString);
    procedure OnRequestImpProjectGUIDs(Sender: TObject; var StartGUID, CurrentGUID: WideString);

    procedure CanResizeEvent(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure ClickEvent(Sender: TObject);
    procedure ConstrainedResizeEvent(Sender: TObject; var MinWidth, MinHeight,
      MaxWidth, MaxHeight: Integer);
    procedure DblClickEvent(Sender: TObject);
    procedure ResizeEvent(Sender: TObject);
  protected
    { Protected declarations }
    procedure DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage); override;
    procedure EventSinkChanged(const EventSink: IUnknown); override;
    procedure InitializeControl; override;
    function DrawTextBiDiModeFlagsReadingOnly: Integer; safecall;
    function Get_Alignment: TxAlignment; safecall;
    function Get_AutoSize: WordBool; safecall;
    function Get_BevelInner: TxBevelCut; safecall;
    function Get_BevelOuter: TxBevelCut; safecall;
    function Get_BorderStyle: TxBorderStyle; safecall;
    function Get_Caption: WideString; safecall;
    function Get_Color: OLE_COLOR; safecall;
    function Get_Ctl3D: WordBool; safecall;
    function Get_Cursor: Smallint; safecall;
    function Get_DockSite: WordBool; safecall;
    function Get_DoubleBuffered: WordBool; safecall;
    function Get_DragCursor: Smallint; safecall;
    function Get_DragMode: TxDragMode; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Font: IFontDisp; safecall;
    function Get_FullRepaint: WordBool; safecall;
    function Get_Locked: WordBool; safecall;
    function Get_ParentColor: WordBool; safecall;
    function Get_ParentCtl3D: WordBool; safecall;
    function Get_UseDockManager: WordBool; safecall;
    function Get_Visible: WordBool; safecall;
    function Get_VisibleDockClientCount: Integer; safecall;
    function IsRightToLeft: WordBool; safecall;
    function UseRightToLeftReading: WordBool; safecall;
    function UseRightToLeftScrollBar: WordBool; safecall;
    procedure _Set_Font(const Value: IFontDisp); safecall;
    procedure InitiateAction; safecall;
    procedure Set_Alignment(Value: TxAlignment); safecall;
    procedure Set_AutoSize(Value: WordBool); safecall;
    procedure Set_BevelInner(Value: TxBevelCut); safecall;
    procedure Set_BevelOuter(Value: TxBevelCut); safecall;
    procedure Set_BorderStyle(Value: TxBorderStyle); safecall;
    procedure Set_Caption(const Value: WideString); safecall;
    procedure Set_Color(Value: OLE_COLOR); safecall;
    procedure Set_Ctl3D(Value: WordBool); safecall;
    procedure Set_Cursor(Value: Smallint); safecall;
    procedure Set_DockSite(Value: WordBool); safecall;
    procedure Set_DoubleBuffered(Value: WordBool); safecall;
    procedure Set_DragCursor(Value: Smallint); safecall;
    procedure Set_DragMode(Value: TxDragMode); safecall;
    procedure Set_Enabled(Value: WordBool); safecall;
    procedure Set_Font(var Value: IFontDisp); safecall;
    procedure Set_FullRepaint(Value: WordBool); safecall;
    procedure Set_Locked(Value: WordBool); safecall;
    procedure Set_ParentColor(Value: WordBool); safecall;
    procedure Set_ParentCtl3D(Value: WordBool); safecall;
    procedure Set_UseDockManager(Value: WordBool); safecall;
    procedure Set_Visible(Value: WordBool); safecall;

    // set properties
    function Get_WorkingDir: WideString; safecall;
    procedure Set_WorkingDir(const Value: WideString); safecall;
    function Get_LngCode: WideString; safecall;
    procedure Set_LngCode(const Value: WideString); safecall;
    function Get_DBName: WideString; safecall;
    procedure Set_DBName(const Value: WideString); safecall;
    function Get_DBMode: Integer; safecall;
    function Get_DBTable: WideString; safecall;
    procedure Set_DBMode(Value: Integer); safecall;
    procedure Set_DBTable(const Value: WideString); safecall;
    procedure SetApp(var aApp: OleVariant); safecall;
    procedure SetDocument(var aDocument: OleVariant); safecall;
    procedure CombineCPolysWithSymbols(const CPolyList: AxCmbStringList;
      const SymOnLayerList: AxCmbSymOnLayerList;
      const Setup: AxCmbSymbolsWithCPolysSetup; PolygonDataSource,
      SymbolDataSource: AxCmbDataSource); safecall;
    procedure BEV_Combine(const Filename: WideString;
      Datasource: AxCmbDataSource); safecall;
    procedure CombineCPolysByDatabaseAttrib(const SourceLayer,
      TargetLayer: WideString; const DatabaseColumns: AxCmbStringList;
      DataSource: AxCmbDataSource); safecall;
    procedure CombineCPolysWithTexts(const CPolyList,
      TextList: AxCmbStringList; const Setup: AxCmbTextsWithCPolysSetup;
      PolygonDataSource, TextDataSource: AxCmbDataSource); safecall;
    procedure MEDIX_Combine(const Filename: WideString;
      Datasource: AxCmbDataSource); safecall;
    procedure DFK_Combine(const Filename: WideString;
      DataSource: AxCmbDataSource); safecall;
    procedure MergeLayers(const LayerList: AxCmbStringList;
      const Targetlayername, DataSourceCol: WideString); safecall;
    // methods to supported co-classes
    function CreateStringList: AxCmbStringList; safecall;
    function CreateSymbolOnLayerList: AxCmbSymOnLayerList; safecall;
    function CreateCombineSymbolsWithCPolysSetup: AxCmbSymbolsWithCPolysSetup; safecall;
    function CreateCombineTextsWithCPolysSetup: AxCmbTextsWithCPolysSetup;
      safecall; procedure IAxGisCombine._Set_Font = IAxGisCombine__Set_Font;
    procedure IAxGisCombine.Set_Font = IAxGisCombine_Set_Font;

    procedure IAxGisCombine__Set_Font(var Value: IFontDisp); safecall;
    procedure IAxGisCombine_Set_Font(const Value: IFontDisp); safecall;
  end;

implementation

uses ComObj, OleCtrls, AXImpExpDbcXControl_TLB, AXGisMan_TLB,
  SysUtils, inifiles, Dialogs, AxGisCmbStringList, AxGisCmbSymOnLayerList,
  AxGisCmbSymbolsWithCPolysSetup, AxGisCmbTextsWithCPolysSetup;

var AXImpExpDbc: TAXImpExpDbc;
  AxObjectMan: TAXGisObjMan;

{ TAxGisCombine }

procedure TAxGisCombine.DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage);
begin
  {TODO: Define property pages here.  Property pages are defined by calling
    DefinePropertyPage with the class id of the page.  For example,
      DefinePropertyPage(Class_AxGisCombinePage); }
end;

procedure TAxGisCombine.EventSinkChanged(const EventSink: IUnknown);
begin
  FEvents := EventSink as IAxGisCombineEvents;
end;

procedure TAxGisCombine.InitializeControl;
begin
  FDelphiControl := Control as TPanel;
  FDelphiControl.OnCanResize := CanResizeEvent;
  FDelphiControl.OnClick := ClickEvent;
  FDelphiControl.OnConstrainedResize := ConstrainedResizeEvent;
  FDelphiControl.OnDblClick := DblClickEvent;
  FDelphiControl.OnResize := ResizeEvent;
  FDelphiControl.Height := 25;
  FDelphiControl.Width := 50;
  FDelphiControl.Caption := 'GisCmb';
  // initiate the lists

  CurrPercentInfo := 0;
  FDBMode := 0;
  ProcessingMode := pro_NORMAL;

  Database_Nesting := 0;
  AXImpExpDbc := nil;

  Manipulation_Nesting := 0;
  AxObjectMan := nil;

  StrFile_Nesting := 0;
  StrFileObj := nil;

  // init sizes for stack
  Selected_StackSize := 0;
  Selected_StackNesting := 0;
  Selected_StackLayernames := nil;
end;

function TAxGisCombine.DrawTextBiDiModeFlagsReadingOnly: Integer;
begin
  Result := FDelphiControl.DrawTextBiDiModeFlagsReadingOnly;
end;

function TAxGisCombine.Get_Alignment: TxAlignment;
begin
  Result := Ord(FDelphiControl.Alignment);
end;

function TAxGisCombine.Get_AutoSize: WordBool;
begin
  Result := FDelphiControl.AutoSize;
end;

function TAxGisCombine.Get_BevelInner: TxBevelCut;
begin
  Result := Ord(FDelphiControl.BevelInner);
end;

function TAxGisCombine.Get_BevelOuter: TxBevelCut;
begin
  Result := Ord(FDelphiControl.BevelOuter);
end;

function TAxGisCombine.Get_BorderStyle: TxBorderStyle;
begin
  Result := Ord(FDelphiControl.BorderStyle);
end;

function TAxGisCombine.Get_Caption: WideString;
begin
  Result := WideString(FDelphiControl.Caption);
end;

function TAxGisCombine.Get_Color: OLE_COLOR;
begin
  Result := OLE_COLOR(FDelphiControl.Color);
end;

function TAxGisCombine.Get_Ctl3D: WordBool;
begin
  Result := FDelphiControl.Ctl3D;
end;

function TAxGisCombine.Get_Cursor: Smallint;
begin
  Result := Smallint(FDelphiControl.Cursor);
end;

function TAxGisCombine.Get_DockSite: WordBool;
begin
  Result := FDelphiControl.DockSite;
end;

function TAxGisCombine.Get_DoubleBuffered: WordBool;
begin
  Result := FDelphiControl.DoubleBuffered;
end;

function TAxGisCombine.Get_DragCursor: Smallint;
begin
  Result := Smallint(FDelphiControl.DragCursor);
end;

function TAxGisCombine.Get_DragMode: TxDragMode;
begin
  Result := Ord(FDelphiControl.DragMode);
end;

function TAxGisCombine.Get_Enabled: WordBool;
begin
  Result := FDelphiControl.Enabled;
end;

function TAxGisCombine.Get_Font: IFontDisp;
begin
  GetOleFont(FDelphiControl.Font, Result);
end;

function TAxGisCombine.Get_FullRepaint: WordBool;
begin
  Result := FDelphiControl.FullRepaint;
end;

function TAxGisCombine.Get_Locked: WordBool;
begin
  Result := FDelphiControl.Locked;
end;

function TAxGisCombine.Get_ParentColor: WordBool;
begin
  Result := FDelphiControl.ParentColor;
end;

function TAxGisCombine.Get_ParentCtl3D: WordBool;
begin
  Result := FDelphiControl.ParentCtl3D;
end;

function TAxGisCombine.Get_UseDockManager: WordBool;
begin
  Result := FDelphiControl.UseDockManager;
end;

function TAxGisCombine.Get_Visible: WordBool;
begin
  Result := FDelphiControl.Visible;
end;

function TAxGisCombine.Get_VisibleDockClientCount: Integer;
begin
  Result := FDelphiControl.VisibleDockClientCount;
end;

function TAxGisCombine.IsRightToLeft: WordBool;
begin
  Result := FDelphiControl.IsRightToLeft;
end;

function TAxGisCombine.UseRightToLeftReading: WordBool;
begin
  Result := FDelphiControl.UseRightToLeftReading;
end;

function TAxGisCombine.UseRightToLeftScrollBar: WordBool;
begin
  Result := FDelphiControl.UseRightToLeftScrollBar;
end;

procedure TAxGisCombine._Set_Font(const Value: IFontDisp);
begin
  SetOleFont(FDelphiControl.Font, Value);
end;

procedure TAxGisCombine.CanResizeEvent(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
var
  TempNewWidth: Integer;
  TempNewHeight: Integer;
  TempResize: WordBool;
begin
  TempNewWidth := Integer(NewWidth);
  TempNewHeight := Integer(NewHeight);
  TempResize := WordBool(Resize);
  if FEvents <> nil then FEvents.OnCanResize(TempNewWidth, TempNewHeight, TempResize);
  NewWidth := Integer(TempNewWidth);
  NewHeight := Integer(TempNewHeight);
  Resize := Boolean(TempResize);
end;

procedure TAxGisCombine.ClickEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnClick;
end;

procedure TAxGisCombine.ConstrainedResizeEvent(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
var
  TempMinWidth: Integer;
  TempMinHeight: Integer;
  TempMaxWidth: Integer;
  TempMaxHeight: Integer;
begin
  TempMinWidth := Integer(MinWidth);
  TempMinHeight := Integer(MinHeight);
  TempMaxWidth := Integer(MaxWidth);
  TempMaxHeight := Integer(MaxHeight);
  if FEvents <> nil then FEvents.OnConstrainedResize(TempMinWidth, TempMinHeight, TempMaxWidth, TempMaxHeight);
  MinWidth := Integer(TempMinWidth);
  MinHeight := Integer(TempMinHeight);
  MaxWidth := Integer(TempMaxWidth);
  MaxHeight := Integer(TempMaxHeight);
end;

procedure TAxGisCombine.DblClickEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnDblClick;
end;

procedure TAxGisCombine.InitiateAction;
begin
  FDelphiControl.InitiateAction;
end;

procedure TAxGisCombine.ResizeEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnResize;
end;

procedure TAxGisCombine.Set_Alignment(Value: TxAlignment);
begin
  FDelphiControl.Alignment := TAlignment(Value);
end;

procedure TAxGisCombine.Set_AutoSize(Value: WordBool);
begin
  FDelphiControl.AutoSize := Value;
end;

procedure TAxGisCombine.Set_BevelInner(Value: TxBevelCut);
begin
  FDelphiControl.BevelInner := TBevelCut(Value);
end;

procedure TAxGisCombine.Set_BevelOuter(Value: TxBevelCut);
begin
  FDelphiControl.BevelOuter := TBevelCut(Value);
end;

procedure TAxGisCombine.Set_BorderStyle(Value: TxBorderStyle);
begin
  FDelphiControl.BorderStyle := TBorderStyle(Value);
end;

procedure TAxGisCombine.Set_Caption(const Value: WideString);
begin
  FDelphiControl.Caption := TCaption(Value);
end;

procedure TAxGisCombine.Set_Color(Value: OLE_COLOR);
begin
  FDelphiControl.Color := TColor(Value);
end;

procedure TAxGisCombine.Set_Ctl3D(Value: WordBool);
begin
  FDelphiControl.Ctl3D := Value;
end;

procedure TAxGisCombine.Set_Cursor(Value: Smallint);
begin
  FDelphiControl.Cursor := TCursor(Value);
end;

procedure TAxGisCombine.Set_DockSite(Value: WordBool);
begin
  FDelphiControl.DockSite := Value;
end;

procedure TAxGisCombine.Set_DoubleBuffered(Value: WordBool);
begin
  FDelphiControl.DoubleBuffered := Value;
end;

procedure TAxGisCombine.Set_DragCursor(Value: Smallint);
begin
  FDelphiControl.DragCursor := TCursor(Value);
end;

procedure TAxGisCombine.Set_DragMode(Value: TxDragMode);
begin
  FDelphiControl.DragMode := TDragMode(Value);
end;

procedure TAxGisCombine.Set_Enabled(Value: WordBool);
begin
  FDelphiControl.Enabled := Value;
end;

procedure TAxGisCombine.Set_Font(var Value: IFontDisp);
begin
  SetOleFont(FDelphiControl.Font, Value);
end;

procedure TAxGisCombine.Set_FullRepaint(Value: WordBool);
begin
  FDelphiControl.FullRepaint := Value;
end;

procedure TAxGisCombine.Set_Locked(Value: WordBool);
begin
  FDelphiControl.Locked := Value;
end;

procedure TAxGisCombine.Set_ParentColor(Value: WordBool);
begin
  FDelphiControl.ParentColor := Value;
end;

procedure TAxGisCombine.Set_ParentCtl3D(Value: WordBool);
begin
  FDelphiControl.ParentCtl3D := Value;
end;

procedure TAxGisCombine.Set_UseDockManager(Value: WordBool);
begin
  FDelphiControl.UseDockManager := Value;
end;

procedure TAxGisCombine.Set_Visible(Value: WordBool);
begin
  FDelphiControl.Visible := Value;
end;

function TAxGisCombine.Get_WorkingDir: WideString;
begin
  result := FWorkingDir;
end;

procedure TAxGisCombine.Set_WorkingDir(const Value: WideString);
begin
  FWorkingDir := Value;
end;

function TAxGisCombine.Get_LngCode: WideString;
begin
  result := FLngCode;
end;

procedure TAxGisCombine.Set_LngCode(const Value: WideString);
begin
  FLngCode := Value;
end;

function TAxGisCombine.Get_DBName: WideString;
begin
  result := FDBName;
end;

procedure TAxGisCombine.Set_DBName(const Value: WideString);
begin
  FDBName := Value;
end;

function TAxGisCombine.Get_DBMode: Integer;
begin
  result := FDBMode;
end;

function TAxGisCombine.Get_DBTable: WideString;
begin
  result := FDBTable;
end;

procedure TAxGisCombine.Set_DBMode(Value: Integer);
begin
  FDBMode := Value;
end;

procedure TAxGisCombine.Set_DBTable(const Value: WideString);
begin
  FDBTable := Value;
end;

//------------------------------------------------------------------------------
// NAME : Create_SelectedStack
//------------------------------------------------------------------------------
// Method is used to read out the current selected objects and put them into a
// stack.
//
// PARAMETERS : NONE
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.Create_SelectedStack;
var i, j: integer;
  ObjectDisp: IDispatch;
  AObject: IBase;
  ALayer: ILayer;
  Layername: string;
  aIndex: integer;
begin
  Selected_StackNesting := Selected_StackNesting + 1;
  if Selected_StackNesting <> 1 then exit; // stack is already created -> exit

  Create_StringFileInstance;
  FEvents.OnDisplayPass(StrfileObj.Assign(5)); // Evaluate selected objects

   // fill items to selected stack
  Document.CreateSelectedList;
  Selected_StackSize := Document.NrOfSelectedObjects;
  SetLength(Selected_Stack, Selected_StackSize);

  for i := 0 to Selected_StackSize - 1 do
  begin
    ObjectDisp := Document.SelItems[i];
    if ObjectDisp = nil then
      continue;
    AObject := ObjectDisp as IBase;
    if AObject.ObjectType = ot_Layer then // Layer Object received
    begin
         // Index of layer will be added to list
         // Index of layer is: ot_Layer*-1 - Index in Selected_StackLayernames
      ALayer := AObject as ILayer;
      Layername := ALayer.Layername;
      if Selected_StackLayernames = nil then
        Selected_StackLayernames := TStringList.Create;
      aIndex := (ot_Layer * -1) - Selected_StackLayernames.Count;
      Selected_StackLayernames.Add(Layername);
         // add the name to the stack
      Selected_Stack[i] := aIndex;
    end
    else // normal object received
    begin
         // add the Id to the stack
      aIndex := aObject.Index;
      Selected_Stack[i] := aIndex;
    end;
    ShowPercent(i, Selected_StackSize); // show progress
  end;
   // now that the information about the selected objects is in the SelectedObjectsStack the
   // Selected-Objects list may be destroyed
  Document.DestroySelectedList;
  Document.DeselectAll;
  Destroy_StringFileInstance;
end;

//------------------------------------------------------------------------------
// NAME : Destroy_SelectedStack
//------------------------------------------------------------------------------
// Method is used to remove stack of selected objects
//
// PARAMETERS : NONE
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.Destroy_SelectedStack;
begin
  Selected_StackNesting := Selected_StackNesting - 1;
  if Selected_StackNesting <> 0 then exit; // stack is already needed -> may not delete
   // remove stack
  Selected_StackSize := 0;
  SetLength(Selected_Stack, Selected_StackSize);
  Selected_StackLayernames.Destroy;
  Selected_StackLayernames := nil;
end;

//------------------------------------------------------------------------------
// NAME : Create_DatabaseInstance
//------------------------------------------------------------------------------
// Method is used to create instance to handle database
//
// PARAMETERS : NONE
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.Create_DatabaseInstance;
begin
  Database_nesting := Database_nesting + 1;
  if Database_nesting <> 1 then exit; // database is already created

  AxImpExpDbc := TAxImpExpDbc.Create(nil);
  AxImpExpDbc.WorkingDir := FWorkingDir;
  AxImpExpDbc.LngCode := FLngCode;
  AxImpExpDbc.ImpDBMode := FDbMode;
  AxImpExpDbc.ImpDatabase := FDbName;
  AxImpExpDbc.ImpTable := FDbTable;
  AxImpExpDbc.ExpDBMode := FDbMode;
  AxImpExpDbc.ExpDatabase := FDbName;
  AxImpExpDbc.ExpTable := FDbTable;
  AxImpExpDbc.IgnoreNonExistExpTables := TRUE;
  AxImpExpDbc.OnRequestImpLayerIndex := OnRequestImpLayerIndex;
  AxImpExpDbc.OnRequestExpLayerIndex := OnRequestExpLayerIndex;
  AxImpExpDbc.OnRequestImpProjectName := OnRequestImpProjectName;
  AxImpExpDbc.OnRequestExpProjectName := OnRequestExpProjectName;
  AxImpExpDbc.OnRequestImpProjectGUIDs := OnRequestImpProjectGUIDs;
  AxImpExpDbc.RemoveEmptyColumnsOnClose := TRUE;
  if ProcessingMode = pro_DKM_BEV then AxImpExpDbc.RemoveEmptyColumnsOnClose := FALSE;
  if ProcessingMode = pro_DKM_MEDIX then AxImpExpDbc.RemoveEmptyColumnsOnClose := FALSE;
  if ProcessingMode = pro_DKM_DFK then AxImpExpDbc.RemoveEmptyColumnsOnClose := FALSE;
end;

//------------------------------------------------------------------------------
// NAME : Destroy_DatabaseInstance
//------------------------------------------------------------------------------
// Method is used to create instance to handle database
//
// PARAMETERS : NONE
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.Destroy_DatabaseInstance;
begin
  Database_nesting := Database_nesting - 1;
  if Database_nesting > 0 then exit; // database is still used

  if AxImpExpDbc = nil then exit;

  AxImpExpDbc.CloseImpTables;
  AxImpExpDbc.CloseExpTables;
  AxImpExpDbc.Destroy;
  AxImpExpDbc := nil;
end;

//------------------------------------------------------------------------------
// NAME : Create_ManipulationInstance
//------------------------------------------------------------------------------
// Method is used to create instance to handle object manipulations
//
// PARAMETERS : NONE
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.Create_ManipulationInstance;
var aDocument: OleVariant;
begin
  Manipulation_Nesting := Manipulation_Nesting + 1;
  if Manipulation_Nesting <> 1 then exit; // it is already created
  AxObjectMan := TAXGisObjMan.Create(nil);
  AxObjectMan.WorkingDir := FWorkingDir;
  AxObjectMan.LngCode := FLngCode;
  AxObjectMan.SetApp(App);
  aDocument := Document;
  AxObjectMan.SetDocument(aDocument);
  AxObjectMan.SnapRadius := FetchRadius;
  AxObjectMan.OnDisplayPercent := OnReceivedObjManDisplayPercent;
  AxObjectMan.OnDisplayPass := OnReceivedObjManDisplayPass;
end;

//------------------------------------------------------------------------------
// NAME : Destroy_ManipulationInstance
//------------------------------------------------------------------------------
// Method is used to destroy manipulation object
//
// PARAMETERS : NONE
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.Destroy_ManipulationInstance;
begin
  Manipulation_Nesting := Manipulation_Nesting - 1;
  if Manipulation_Nesting > 0 then exit; // still in use
  if AxObjectMan = nil then exit;
  AxObjectMan.Destroy;
  AxObjectMan := nil;
end;

//------------------------------------------------------------------------------
// NAME : Create_StringFileInstance
//------------------------------------------------------------------------------
// Method is used to create instance for string file
//
// PARAMETERS : NONE
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.Create_StringFileInstance;
begin
  StrFile_Nesting := StrFile_Nesting + 1;
  if StrFile_Nesting <> 1 then exit;
  StrFileObj := ClassStrFile.Create;
  StrFileObj.CreateList(FWorkingDir, FLngCode, 'OBJCOMB');
end;

//------------------------------------------------------------------------------
// NAME : Destroy_StringFileInstance
//------------------------------------------------------------------------------
// Method is used to destroy string-file instance
//
// PARAMETERS : NONE
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.Destroy_StringFileInstance;
begin
  StrFile_Nesting := StrFile_Nesting - 1;
  if StrFile_Nesting > 0 then exit;
  if StrFileObj = nil then exit;
  StrFileObj.Destroy;
  StrFileObj := nil;
end;

//------------------------------------------------------------------------------
// NAME : FillObjectKeeper
//------------------------------------------------------------------------------
// Method is used to fill data to objectref keeper
//
// PARAMETERS: aFilter     -> Filter that should be used to write
//             aKeeper     -> Object reference keeper
//             Datasource  -> Data source of objects (layers, selected)
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.FillObjectKeeper(var aFilter: TObjectRefFilter; var aKeeper: TObjectRefKeeper; DataSource: AxCmbDataSource);
var ElemsToWork, ElemsWorked: integer;
  i, j: integer;
  aLayer: ILayer;
  LayerRef: TLayerRef;
  ColName: Widestring;
  ColInfo: Widestring;
  ColType: TOleEnum;
  ColLength: integer;
  aBase: IBase;
  aIndex: integer;

  CPolyRef: TCPolyRef;
  aCPoly: IPolygon;

  SymbolRef: TSymbolRef;
  aSymbol: ISymbol;

  TextRef: TTextRef;
  aText: IText;

  aBaseRef: TBaseRef;
begin
  if aFilter.ReadAttribs then
    Create_DatabaseInstance;

  if DataSource = ds_Layers then
  begin
      // first get elements that should be worked
    ElemsToWork := 0;
    for i := 0 to Document.Layers.Count - 1 do
    begin
      aLayer := Document.Layers.Items[i];
      if aFilter.IsSupportedLayer(aLayer.Layername) then
        ElemsToWork := ElemsToWork + aLayer.Count;
    end;
      // fill objects to object keeper
    ElemsWorked := 0;
    for i := 0 to Document.Layers.Count - 1 do
    begin
      aLayer := Document.Layers.Items[i];
      if not aFilter.IsSupportedLayer(aLayer.Layername) then
        continue; // layer should not be processed
      LayerRef := aKeeper.Layers.Find(aLayer.Layername);
      if LayerRef = nil then
      begin
            // new reference to layer has to be created
        LayerRef := TLayerRef.Create;
        LayerRef.Name := aLayer.LayerName;
        aKeeper.Layers.Add(LayerRef); // add to layer
      end;

      if (aFilter.ReadAttribs) and (LayerRef.DbColumns = nil) then
      begin
            // we need to get the columns
        AxImpExpDbc.CheckAndCreateExpTable(LayerRef.Name);
        while AxImpExpDbc.GetExportColumn(LayerRef.Name, ColName, ColType, ColLength) do
        begin
          if aFilter.IsSupportedDbColumn(Colname) then
          begin
            if LayerRef.DbColumns = nil then
              LayerRef.CreateDbColumnList;
            LayerRef.DbColumns.Add(ColName, ColType, ColLength);
          end;
        end;
        AxImpExpDbc.OpenExpTable(LayerRef.Name);
      end;

         // now read the objects
      for j := 0 to aLayer.Count - 1 do
      begin
        aBase := aLayer.Items[j];
        ElemsWorked := ElemsWorked + 1;
        ShowPercent(ElemsToWork, ElemsWorked);

        aIndex := aBase.Index;
        aBaseRef := nil;

        if aBase.ObjectType = ot_CPoly then
        begin
          if not aFilter.IsSupportedObjectType(Filter_CPoly) then
            continue;
               // create CPoly
          aCPoly := aBase as IPolygon;
          CPolyRef := TCPolyRef.Create;
          CPolyRef.Area := aCPoly.Area;
          aBaseRef := CPolyRef;
        end
        else if aBase.ObjectType = ot_Symbol then
        begin
          if not aFilter.IsSupportedObjectType(Filter_Symbol) then
            continue;
          aSymbol := aBase as ISymbol;
          if not aFilter.IsSupportedLayerContent(aLayer.LayerName, aSymbol.SymbolName) then
            continue;
          aSymbol := aBase as ISymbol;
          SymbolRef := TSymbolRef.Create;
          SymbolRef.X := aSymbol.Position.X / 100;
          SymbolRef.Y := aSymbol.Position.Y / 100;
          SymbolRef.Name := aSymbol.SymbolName;
          aBaseRef := SymbolRef;
        end
        else if aBase.ObjectType = ot_Text then
        begin
          if not aFilter.IsSupportedObjectType(Filter_Text) then
            continue;
          aText := aBase as IText;
          TextRef := TTextRef.Create;
          TextRef.Text := aText.Text;
          TextRef.X := aText.Position.X / 100;
          TextRef.Y := aText.Position.Y / 100;
          aBaseRef := TextRef;
        end;

        if aBaseRef = nil then
          continue;
        aBaseRef.Parent := LayerRef; // apply layer to object
        aBaseRef.ProgisId := aIndex; // apply index to object
        aKeeper.Objects.Add(aBaseRef); // add object to keeper

            // add database attributes
        if (aFilter.ReadAttribs) then
        begin
          if AxImpExpDbc.DoExportData(LayerRef.Name, aIndex) then
          begin
                  // a database entry has been found for this entry
            while AxImpExpDbc.GetExportData(LayerRef.Name, ColName, ColInfo) do
            begin
              if aBaseRef.Attributes = nil then
              begin
                aBaseRef.CreateAttribList;
                aBaseRef.Attributes.NoDuplicates := false; // no need to check here
              end;
              aBaseRef.Attributes.Add(ColName, ColInfo); // add database attribute
            end;
          end;
        end;
      end;
    end;
  end
  else if DataSource = ds_Selected then
  begin
    ElemsToWork := Selected_StackSize;
    ElemsWorked := 0;
    for i := 0 to Selected_StackSize - 1 do
    begin
      ElemsWorked := ElemsWorked + 1;
      ShowPercent(ElemsToWork, ElemsWorked);
      aIndex := Selected_Stack[i];
      if aIndex < 0 then
      begin
            // get layer index
        aIndex := (aIndex + ot_Layer) * -1;
        aLayer := Document.Layers.GetLayerByName(Selected_StackLayernames[aIndex]);
        if not aFilter.IsSupportedLayer(aLayer.Layername) then
        begin
               // ignore layer
          LayerRef := nil;
          continue;
        end;
        LayerRef := aKeeper.Layers.Find(aLayer.Layername);
        if LayerRef = nil then
        begin
               // new reference to layer has to be created
          LayerRef := TLayerRef.Create;
          LayerRef.Name := aLayer.LayerName;
          aKeeper.Layers.Add(LayerRef); // add to layer
        end;

        if (aFilter.ReadAttribs) and (LayerRef.DbColumns = nil) then
        begin
               // we need to get the columns
          AxImpExpDbc.CheckAndCreateExpTable(LayerRef.Name);
          while AxImpExpDbc.GetExportColumn(LayerRef.Name, ColName, ColType, ColLength) do
          begin
            if aFilter.IsSupportedDbColumn(Colname) then
            begin
              if LayerRef.DbColumns = nil then
                LayerRef.CreateDbColumnList;
              LayerRef.DbColumns.Add(ColName, ColType, ColLength);
            end;
          end;
          AxImpExpDbc.OpenExpTable(LayerRef.Name);
        end;
      end
      else if LayerRef <> nil then
      begin
        aBase := Document.GetObjectByIndex(aIndex);
        aIndex := aBase.Index;
        aBaseRef := nil;

        if aBase.ObjectType = ot_CPoly then
        begin
          if not aFilter.IsSupportedObjectType(Filter_CPoly) then
            continue;
               // create CPoly
          aCPoly := aBase as IPolygon;
          CPolyRef := TCPolyRef.Create;
          CPolyRef.Area := aCPoly.Area;
          aBaseRef := CPolyRef;
        end
        else if aBase.ObjectType = ot_Symbol then
        begin
          if not aFilter.IsSupportedObjectType(Filter_Symbol) then
            continue;
          aSymbol := aBase as ISymbol;
          if not aFilter.IsSupportedLayerContent(aLayer.LayerName, aSymbol.SymbolName) then
            continue;
          SymbolRef := TSymbolRef.Create;
          SymbolRef.X := aSymbol.Position.X / 100;
          SymbolRef.Y := aSymbol.Position.Y / 100;
          SymbolRef.Name := aSymbol.SymbolName;
          aBaseRef := SymbolRef;
        end
        else if aBase.ObjectType = ot_Text then
        begin
          if not aFilter.IsSupportedObjectType(Filter_Text) then
            continue;
          aText := aBase as IText;
          TextRef := TTextRef.Create;
          TextRef.Text := aText.Text;
          TextRef.X := aText.Position.X / 100;
          TextRef.Y := aText.Position.Y / 100;
          aBaseRef := TextRef;
        end;

        if aBaseRef = nil then
          continue;
        aBaseRef.Parent := LayerRef; // apply layer to object
        aBaseRef.ProgisId := aIndex; // apply index to object
        aKeeper.Objects.Add(aBaseRef); // add object to keeper

            // add database attributes
        if (aFilter.ReadAttribs) then
        begin
          if AxImpExpDbc.DoExportData(LayerRef.Name, aIndex) then
          begin
                  // a database entry has been found for this entry
            while AxImpExpDbc.GetExportData(LayerRef.Name, ColName, ColInfo) do
            begin
              if aBaseRef.Attributes = nil then
              begin
                aBaseRef.CreateAttribList;
                aBaseRef.Attributes.NoDuplicates := false; // no need to check here
              end;
              aBaseRef.Attributes.Add(ColName, ColInfo); // add database attribute
            end;
          end;
        end;
      end;
    end;
  end;
  if aFilter.ReadAttribs then
    Destroy_DatabaseInstance;
end;

//------------------------------------------------------------------------------
// NAME : ShowPercent
//------------------------------------------------------------------------------
// Method is used to show the current process percent status
//
// PARAMETERS: MaxElems  -> Maximum number of elements
//             CurrElems -> Current number of processed elements
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.ShowPercent(MaxElems: integer; CurrElems: integer);
var Percent: integer;
begin
  if MaxElems > 0 then Percent := Round(CurrElems / MaxElems * 100) else Percent := 100;
  if (Percent <> CurrPercentInfo) then
  begin
    CurrPercentInfo := Percent;
    FEvents.OnDisplayPercent(Percent);
  end;
end;

//------------------------------------------------------------------------------
// NAME : CalculateLength
//------------------------------------------------------------------------------
// Method is used to calculate distance between two points
//
// PARAMETERS: X1  -> X-Coordinate of first point
//             Y1  -> Y-Coordinate of first point
//             X2  -> X-Coordinate of second point
//             Y2  -> Y-Coordinate of second point
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

function TAxGisCombine.CalculateLength(X1: double; Y1: double; X2: double; Y2: double): double;
begin
  result := sqrt(sqr(abs(X2 - X1)) + sqr(abs(Y2 - Y1)));
end;

//------------------------------------------------------------------------------
// NAME : CheckLayerExists
//------------------------------------------------------------------------------
// Method is used to check if a layer already exists
//
// PARAMETERS : Layername -> Name of the layer
//
// RETURNVALUE: TRUE  -> Layer exists
//              FALSE -> Layer does not exist
//------------------------------------------------------------------------------

function TAxGisCombine.CheckLayerExists(Layername: string): boolean;
var dispatch: IDispatch;
begin
  dispatch := Document.Layers.GetLayerByName(Layername);
  if dispatch <> nil then result := true else result := false;
end;

//------------------------------------------------------------------------------
// NAME : GenerateObjects
//------------------------------------------------------------------------------
// Method is used to generate objects
//
// PARAMETERS : LayersToProcess -> List of layers that should be processed
//              TargetLayer     -> Layer on that result should be created
//              DataSource      -> Source of Data (Layers / Selected)
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.GenerateObjects(const LayersToProcess: TStrings; const TargetLayer: string; DataSource: AxCmbDataSource);
var CList: IList;
  i: integer;
  aLayer: ILayer;
  aIndex: integer;
  aDisp: OleVariant;
  FirstObj: boolean;
  CanGenerate: boolean;
  AObject: IGenerateObject;
begin
  cList := Document.CreateList;
  CanGenerate := false;
  Create_StringFileInstance;

  if DataSource = ds_Selected then
  begin
      // if datasource is selected -> copy objects to temporary layers
    FEvents.OnDisplayPass(StrfileObj.Assign(6)); // Prepare selected objects for object generation...
    Create_ManipulationInstance;
    aLayer := nil;
    for i := 0 to Selected_StackSize - 1 do
    begin
         // check if the object from the stack is a layer
      aIndex := Selected_Stack[i];
      if aIndex < 0 then
      begin
            // get layer index
        aIndex := (aIndex + ot_Layer) * -1;
        aLayer := Document.Layers.GetLayerByName(Selected_StackLayernames[aIndex]);
        if LayersToProcess.IndexOf(aLayer.LayerName) < 0 then
          aLayer := nil;
        FirstObj := true;
      end
      else if aLayer <> nil then
      begin
        aDisp := Document.GetObjectByIndex(aIndex);
        AxObjectMan.CopyObjectToLayer(aDisp, 'TMP_' + aLayer.Layername);
        if FirstObj then
        begin
          cList.Add(aLayer);
          CanGenerate := true;
        end;
        FirstObj := false;
      end;
      ShowPercent(Selected_Stacksize, i);
    end;
    Destroy_ManipulationInstance;
  end
  else
  begin
      // process whole layers
    for i := 0 to Document.Layers.Count - 1 do
    begin
      aLayer := Document.Layers.Items[i];
      if LayersToProcess.IndexOf(aLayer.LayerName) >= 0 then
      begin
        cList.Add(aLayer);
        CanGenerate := true;
      end;
    end;
  end;

  if CanGenerate then
  begin
    FEvents.OnDisplayPass(StrfileObj.Assign(7)); // Generate objects
    AObject := Document.CreateGenerateObject;
    i := AObject.Test;
    AObject.GenerateObjects(cList, TargetLayer, round(FetchRadius * 100));
  end;

  if Datasource = ds_Selected then
  begin
      // remove temporary layers that have been used for object generation
    for i := 0 to LayersToProcess.Count - 1 do
      RemoveLayerFromProject('TMP_' + LayersToProcess[i]);
  end;
  Destroy_StringFileInstance;
end;

//------------------------------------------------------------------------------
// NAME : OnReceivedObjManDisplayPass
//------------------------------------------------------------------------------
// Method is used as callback from TAXGisObjMan
//
// PARAMETERS : NONE
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.OnReceivedObjManDisplayPass(Sender: TObject; const aPass: WideString);
begin
  FEvents.OnDisplayPass(aPass);
end;

//------------------------------------------------------------------------------
// NAME : OnReceivedObjManDisplayPercent
//------------------------------------------------------------------------------
// Method is used as callback from TAXGisObjMan
//
// PARAMETERS : NONE
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.OnReceivedObjManDisplayPercent(Sender: TObject; aValue: Integer);
begin
  FEvents.OnDisplayPercent(aValue);
end;

//------------------------------------------------------------------------------
// NAME : CombineCPolyRefsWithSymbolRefs
//------------------------------------------------------------------------------
// Method is used to combine references to polygons with symbols
//
// PARAMETERS:  CPolys    -> Polygons that should be combined
//              Symbols   -> Symbols that should be combined
//              Setup     -> Setup for combine
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.CombineCPolyRefsWithSymbolRefs(var CPolys: TObjectRefKeeper; var Symbols: TObjectRefKeeper; const Setup: AxCmbSymbolsWithCPolysSetup; DataSource: AxCmbDataSource);
var i: integer;
  new_id: integer;

  CombFile: TextFile;
  Line: string;

  Elems2Work: integer;
  ElemsWorked: integer;
  PolyIdx: integer;

  PolyRef: TCPolyRef;
  aCPoly: OleVariant;
  ErrorCPoly: OleVariant;
  ErrorPolyRef: TCPolyRef;

  SymbolRef: TSymbolRef;
  aSymbol: OleVariant;
  SymbolIdx: integer;

  LayerRef: TLayerRef;

  duplId: integer;
begin
   // create a file that contains the combining information if necessary
  if Setup.CombineResultFilename <> '' then
  begin
    AssignFile(CombFile, Setup.CombineResultFilename);
    Rewrite(CombFile);
  end;

  FEvents.OnDisplayPass(StrfileObj.Assign(3)); // combine polygons with symbols...
  Elems2Work := CPolys.Objects.Count; ElemsWorked := 0;

   // create object for object manipulation
  Create_ManipulationInstance;

  for PolyIdx := 0 to CPolys.Objects.Count - 1 do
  begin
    PolyRef := CPolys.Objects.Items[PolyIdx] as TCPolyRef;
      // get object itself
    aCPoly := Document.GetObjectByIndex(PolyRef.ProgisId);
    for SymbolIdx := 0 to Symbols.Objects.Count - 1 do
    begin
      SymbolRef := Symbols.Objects.Items[SymbolIdx] as TSymbolRef;

      if (not Setup.CheckMultiAssignedSymbols) and (SymbolRef.LinkedObjects <> nil) then
        continue; // Symbol is already assigned -> no need to check anymore

      if AxObjectMan.CheckPointInsidePoly(SymbolRef.X, SymbolRef.Y, aCPoly, FALSE) then
      begin
            // check if Symbol is not already combined with a Poly
        if SymbolRef.LinkedObjects = nil then
        begin
          SymbolRef.CreateLinkedObjectList;
          SymbolRef.LinkedObjects.Add(PolyRef);
          if PolyRef.LinkedObjects = nil then
            PolyRef.CreateLinkedObjectList;
          PolyRef.LinkedObjects.Add(SymbolRef);
               // write combining-information to File
          if Setup.CombineResultFilename <> '' then
          begin
            Line := inttostr(PolyRef.ProgisId) + ' ' + inttostr(SymbolRef.ProgisId);
            writeln(CombFile, Line);
          end;
          if (Setup.ProcessCPolyDatabase) and (SymbolRef.Attributes <> nil) then
          begin
                  // write item to database
            LayerRef := PolyRef.Parent as TLayerRef;
            AxImpExpDbc.AddImpIdInfo(LayerRef.Name, PolyRef.ProgisId);
            for i := 0 to SymbolRef.Attributes.Count - 1 do
              AxImpExpDbc.AddImpInfo(LayerRef.Name, SymbolRef.Attributes.Items[i].ColName, SymbolRef.Attributes.Items[i].ColInfo);
            AxImpExpDbc.AddImpInfo(LayerRef.Name, 'AREA', floattostr(PolyRef.Area)); // set area of polygon
          end;

          if not Setup.CheckMultiAssignedSymbols then
            break; // due to there is already a symbol assigned to polygon
        end
        else
        begin
          if PolyRef.LinkedObjects = nil then
            PolyRef.CreateLinkedObjectList;
          PolyRef.LinkedObjects.Add(SymbolRef);
          SymbolRef.LinkedObjects.Add(PolyRef);

               // copy current CPoly and CPoly that is already linked to
               // this Symbol to the CPolyErrorLayer
               // write combining-information to File
          if Setup.CombineResultFilename <> '' then
          begin
            Line := inttostr(PolyRef.ProgisId) + ' ' + inttostr(SymbolRef.ProgisId);
            writeln(CombFile, Line);
          end;

          if (Setup.ProcessCPolyDatabase) and (SymbolRef.Attributes <> nil) then
          begin
                  // write item to database
            LayerRef := PolyRef.Parent as TLayerRef;
            AxImpExpDbc.AddImpIdInfo(LayerRef.Name, PolyRef.ProgisId);
            AxImpExpDbc.AddImpInfo(LayerRef.Name, 'AREA', floattostr(PolyRef.Area));
            for i := 0 to SymbolRef.Attributes.Count - 1 do
              AxImpExpDbc.AddImpInfo(LayerRef.Name, SymbolRef.Attributes.Items[i].ColName, SymbolRef.Attributes.Items[i].ColInfo);
          end;

          if Setup.CPolyErrorLayer <> '' then
          begin
            ErrorPolyRef := SymbolRef.LinkedObjects.Items[SymbolRef.LinkedObjects.Count - 1] as TCPolyRef;
                  // copy current Poly to CPolyErrorLayer
            duplId := 0;
            new_id := AxObjectMan.CopyObjectToLayer(aCPoly, Setup.CPolyErrorLayer);
            if Datasource = ds_Selected then
            begin
              duplId := AxObjectMan.CheckIdHasDuplicateOnLayer(new_id, Setup.CPolyErrorLayer);
              if duplId > 0 then
                Document.DeleteObjectByIndex(new_id);
            end;
            if (duplId = 0) and Setup.ProcessCPolyErrorDatabase then
            begin
                     // add a database entry for the CPoly that could not be combined
              AxImpExpDbc.AddImpIdInfo(Setup.CPolyErrorLayer, new_id);
              AxImpExpDbc.AddImpInfo(Setup.CPolyErrorLayer, 'AREA', floattostr(PolyRef.Area));
            end;
                  // copy Poly that is already linked to this Symbol to CPolyErrorLayer
            ErrorCPoly := Document.GetObjectByIndex(ErrorPolyRef.Progisid);
            duplId := 0;
            new_id := AxObjectMan.CopyObjectToLayer(ErrorCPoly, Setup.CPolyErrorLayer);
            if Datasource = ds_Selected then
            begin
              duplId := AxObjectMan.CheckIdHasDuplicateOnLayer(new_id, Setup.CPolyErrorLayer);
              if duplId > 0 then
                Document.DeleteObjectByIndex(new_id);
            end;
            if (duplId = 0) and Setup.ProcessCPolyErrorDatabase then
            begin
                     // add a database entry for the CPoly that could not be combined
              AxImpExpDbc.AddImpIdInfo(Setup.CPolyErrorLayer, new_id);
              AxImpExpDbc.AddImpInfo(Setup.CPolyErrorLayer, 'AREA', floattostr(ErrorPolyRef.Area));
            end;
          end;
        end;
      end
      else if (PolyIdx = CPolys.Objects.Count - 1) and (SymbolRef.LinkedObjects = nil) then
      begin
            // we just have checked with last polygon and still could not assign
        if Setup.SymbolErrorLayer <> '' then
        begin
               // copy to symbol error layer
          aSymbol := Document.GetObjectByIndex(SymbolRef.ProgisId);
          duplId := 0;
          new_id := AxObjectMan.CopyObjectToLayer(aSymbol, Setup.SymbolErrorLayer);
          if Datasource = ds_Selected then
          begin
            duplId := AxObjectMan.CheckIdHasDuplicateOnLayer(new_id, Setup.SymbolErrorLayer);
            if duplId > 0 then
              Document.DeleteObjectByIndex(new_id);
          end;
          if (DuplId = 0) and Setup.ProcessSymbolErrorDatabase then
          begin
                  // -> copy item to symbol error database
            AxImpExpDbc.AddImpIdInfo(Setup.SymbolErrorLayer, new_id);
            for i := 0 to SymbolRef.Attributes.Count - 1 do
              AxImpExpDbc.AddImpInfo(Setup.SymbolErrorLayer, SymbolRef.Attributes.Items[i].ColName, SymbolRef.Attributes.Items[i].ColInfo);
          end;
        end;
      end;
    end;

      // if polygon could not be assigned to symbols
    if (PolyRef.LinkedObjects = nil) and (Setup.CPolyErrorLayer <> '') then
    begin
         // copy polygon to error layer
      duplId := 0;
      new_id := AxObjectMan.CopyObjectToLayer(aCPoly, Setup.CPolyErrorLayer);
      if Datasource = ds_Selected then
      begin
        duplId := AxObjectMan.CheckIdHasDuplicateOnLayer(new_id, Setup.CPolyErrorLayer);
        if duplId > 0 then
          Document.DeleteObjectByIndex(new_id);
      end;
      if (duplId = 0) and Setup.ProcessCPolyErrorDatabase then
      begin
            // add a database entry for the CPoly that could not be combined
        AxImpExpDbc.AddImpIdInfo(Setup.CPolyErrorLayer, new_id);
        AxImpExpDbc.AddImpInfo(Setup.CPolyErrorLayer, 'AREA', floattostr(PolyRef.Area));
      end;
    end;
    ElemsWorked := ElemsWorked + 1;
    ShowPercent(Elems2Work, ElemsWorked);
  end;

  if Setup.CombineResultFilename <> '' then
    CloseFile(CombFile);

  Destroy_ManipulationInstance;
end;

//------------------------------------------------------------------------------
// NAME : CombineCPolysWithSymbols
//------------------------------------------------------------------------------
// Method is used to do combine Polygons with Symbols
//
// PARAMETERS : CPolyList      -> List of Polygon layers
//              SymOnLayerList -> List of Symbols on layer
//              Setup          -> Setup for combining polygons with symbols
//              DataSource     -> List if datasource is Layers or Selected
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.CombineCPolysWithSymbols(
  const CPolyList: AxCmbStringList;
  const SymOnLayerList: AxCmbSymOnLayerList;
  const Setup: AxCmbSymbolsWithCPolysSetup; PolygonDataSource,
  SymbolDataSource: AxCmbDataSource);
var CPolyObjKeeper: TObjectRefKeeper;
  SymbolObjKeeper: TObjectRefKeeper;
  Filter: TObjectRefFilter;
  i, j: integer;
  aSymbol, aLayer: widestring;
  DbColumns: TDbColumns;
  LayerRef: TLayerRef;
begin
   // initiate the StringFile class
  Create_StringFileInstance;

  if (PolygonDataSource = ds_Selected) or (SymbolDataSource = ds_Selected) then
    Create_SelectedStack;

   // create filter for reading
  Filter := TObjectRefFilter.Create;

   // read polygon information
  FEvents.OnDisplayPass(StrfileObj.Assign(1)); // Read polygon information...
  CPolyObjKeeper := TObjectRefKeeper.Create;
   // prepare filter for reading
  Filter.AddObjectTypeToFilter(Filter_CPoly);
  for i := 0 to CPolyList.Count - 1 do
    Filter.AddSupportedLayer(CPolyList.Items[i]);
  Filter.ReadAttribs := false; // Polygon information has not to be read
  FillObjectKeeper(Filter, CPolyObjKeeper, PolygonDataSource);

   // now read the Symbols from the SymbolLayers
  FEvents.OnDisplayPass(StrfileObj.Assign(2)); // Read symbol information...
  SymbolObjKeeper := TObjectRefKeeper.Create;
   // prepare filter for reading
  Filter.Clear;
  Filter.AddObjectTypeToFilter(Filter_Symbol);
  for i := 0 to SymOnLayerList.Count - 1 do
  begin
    SymOnLayerList.GetItem(i, aSymbol, aLayer);
    Filter.AddSupportedLayerContent(aLayer, aSymbol);
  end;

  if FDbMode = 0 then
  begin
    Setup.ProcessCPolyDatabase := false;
    Setup.ProcessCPolyErrorDatabase := false;
    Setup.ProcessSymbolErrorDatabase := false;
  end;
  if Setup.ProcessCPolyDatabase or Setup.ProcessCPolyErrorDatabase or Setup.ProcessSymbolErrorDatabase then
  begin
    DbColumns := TDbColumns.Create;
    Filter.ReadAttribs := true;
    Create_DatabaseInstance;
  end
  else
  begin
    Dbcolumns := nil;
    Filter.ReadAttribs := false;
  end;
  FillObjectKeeper(Filter, SymbolObjKeeper, SymbolDataSource);

   // prepare database columns for result
  if DbColumns <> nil then
  begin
    for i := 0 to SymbolObjKeeper.Layers.Count - 1 do
    begin
      LayerRef := SymbolObjKeeper.Layers.Items[i];
      if LayerRef.DbColumns = nil then continue;
      for j := 0 to LayerRef.DbColumns.Count - 1 do
        DbColumns.Add(LayerRef.DbColumns.Items[j].ColName, LayerRef.DbColumns.Items[j].ColType, LayerRef.DbColumns.Items[j].ColLength);
    end;
  end;

  if Setup.CPolyErrorLayer <> '' then
    CreateLayer(Setup.CPolyErrorLayer);
  if Setup.SymbolErrorLayer <> '' then
    CreateLayer(Setup.SymbolErrorLayer);

  if (Setup.ProcessCPolyErrorDatabase) and (Setup.CPolyErrorLayer <> '') then
  begin
      // create error table for polygons
    AxImpExpDbc.CheckAndCreateImpTable(Setup.CPolyErrorLayer);
    AxImpExpDbc.AddImpIdColumn(Setup.CPolyErrorLayer);
    AxImpExpDbc.AddImpColumn(Setup.CPolyErrorLayer, 'AREA', col_Float, 0);
    AxImpExpDbc.CreateImpTable(Setup.CPolyErrorLayer);
    AxImpExpDbc.OpenImpTable(Setup.CPolyErrorLayer);
  end;

  if (Setup.ProcessSymbolErrorDatabase) and (Setup.SymbolErrorLayer <> '') then
  begin
      // create error table for symbols
    AxImpExpDbc.CheckAndCreateImpTable(Setup.SymbolErrorLayer);
    AxImpExpDbc.AddImpIdColumn(Setup.SymbolErrorLayer);
    for i := 0 to DbColumns.Count - 1 do
      AxImpExpDbc.AddImpColumn(Setup.SymbolErrorLayer, DbColumns.Items[i].ColName, DbColumns.Items[i].ColType, DbColumns.Items[i].ColLength);
    AxImpExpDbc.CreateImpTable(Setup.SymbolErrorLayer);
    AxImpExpDbc.OpenImpTable(Setup.SymbolErrorLayer);
  end;

  if (Setup.ProcessCPolyDatabase) then
  begin
    for i := 0 to CPolyList.Count - 1 do
    begin
      AxImpExpDbc.CheckAndCreateImpTable(CPolyList.Items[i]);
      AxImpExpDbc.AddImpIdColumn(CPolyList.Items[i]);
      for j := 0 to DbColumns.Count - 1 do
        AxImpExpDbc.AddImpColumn(CPolyList.Items[i], DbColumns.Items[j].ColName, DbColumns.Items[j].ColType, DbColumns.Items[j].ColLength);
      AxImpExpDbc.CreateImpTable(CPolyList.Items[i]);
      AxImpExpDbc.OpenImpTable(CPolyList.Items[i]);
    end;
  end;

   // now combine polygons with symbols
  if (PolygonDataSource = ds_Selected) or (SymbolDatasource = ds_Selected) then
    CombineCPolyRefsWithSymbolRefs(CPolyObjKeeper, SymbolObjKeeper, Setup, ds_Selected)
  else
    CombineCPolyRefsWithSymbolRefs(CPolyObjKeeper, SymbolObjKeeper, Setup, ds_Layers);

   // dispose objects
  CPolyObjKeeper.Destroy;
  SymbolObjKeeper.Destroy;
  Filter.Destroy;

  if (PolygonDataSource = ds_Selected) or (SymbolDataSource = ds_Selected) then
    Destroy_SelectedStack;
  Destroy_StringFileInstance;
  if Setup.ProcessCPolyDatabase or Setup.ProcessCPolyErrorDatabase or Setup.ProcessSymbolErrorDatabase then
    Destroy_DatabaseInstance;
end;

//------------------------------------------------------------------------------
// NAME : CopyNonDuplicatesToLayer
//------------------------------------------------------------------------------
// Method is used to copy all objects from SourceLayer that have no duplicate
// on TargetLayer to TargetLayer and copy also the database-information
//
// PARAMETERS: SourceLayer     -> Name of the SourceLayer
//             TargetLayer     -> Name of the TargetLayer
//             ProcessDatabase -> Flag if also the database should be handeled
//             DataSource      -> Datasource of objects (Layers/Selected)
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.CopyNonDuplicatesToLayer(SourceLayer: string; TargetLayer: string; ProcessDatabase: boolean; DataSource: AxCmbDataSource);
var aLayer: ILayer;
  checkforduplicates: boolean;
  i: integer;
  aBase: IBase;
  aObject: OleVariant;
  old_Id: integer;
  new_Id: integer;
  dup_Id: integer;
  ColName: widestring;
  ColInfo: widestring;
  ColType: TOleEnum;
  ColLength: integer;
  ElemsToWork, ElemsWorked: integer;
begin
  if not CheckLayerExists(SourceLayer) then
    exit;

  if not CheckLayerExists(TargetLayer) then
  begin
    CreateLayer(TargetLayer);
    checkforduplicates := false;
  end
  else
    checkforduplicates := true;

  if ProcessDatabase then
  begin
      // create database instance
    Create_DatabaseInstance;
    AxImpExpDbc.CheckAndCreateImpTable(TargetLayer);
    AxImpExpDbc.CheckAndCreateExpTable(SourceLayer);
      // copy the database structure of the import table to the export table
    AxImpExpDbc.AddImpIdColumn(TargetLayer);
    AxImpExpDbc.AddImpColumn(TargetLayer, 'AREA', col_Float, 0);
    while AxImpExpDbc.GetExportColumn(SourceLayer, colname, coltype, collength) do
      AxImpExpDbc.AddImpColumn(TargetLayer, colname, coltype, collength);

    AxImpExpDbc.CreateImpTable(TargetLayer);
    AxImpExpDbc.OpenExpTable(SourceLayer);
    AxImpExpDbc.OpenImpTable(TargetLayer);
  end;

  Create_ManipulationInstance;
  Create_StringFileInstance;

  FEvents.OnDisplayPass(StrfileObj.Assign(4)); // Copy non duplictes to layer....

  if DataSource = ds_Layers then
  begin
    aLayer := Document.Layers.GetLayerByName(SourceLayer);
    ElemsToWork := aLayer.count - 1; ElemsWorked := 0;
    for i := 0 to aLayer.Count - 1 do
    begin
      ElemsWorked := ElemsWorked + 1;
      ShowPercent(ElemsToWork, ElemsWorked);

      aBase := aLayer.Items[i];
      old_Id := aBase.Index;
      aObject := aBase;
      new_Id := AxObjectMan.CopyObjectToLayer(aObject, TargetLayer);
      dup_Id := 0;
      if checkforduplicates then
        dup_Id := AxObjectMan.CheckIdHasDuplicateOnLayer(new_Id, TargetLayer);
      if (dup_Id > 0) then
      begin
        Document.DeleteObjectByIndex(new_Id);
        continue;
      end;
      if ProcessDatabase then
      begin
        if AxImpExpDbc.DoExportData(SourceLayer, old_Id) then
        begin
          AxImpExpDbc.AddImpIdInfo(TargetLayer, new_Id);
          if aBase.ObjectType = ot_CPoly then
            AxImpExpDbc.AddImpInfo(TargetLayer, 'AREA', floattostr(aObject.Area));
          while AxImpExpDbc.GetExportData(SourceLayer, Colname, ColInfo) do
            AxImpExpDbc.AddImpInfo(TargetLayer, Colname, ColInfo);
        end;
      end;
    end;
  end
  else
  begin
      // process selected objects
    ElemsToWork := Selected_StackSize; ElemsWorked := 0;
    aLayer := nil;
    for i := 0 to ElemsToWork - 1 do
    begin
      ElemsWorked := ElemsWorked + 1;
      ShowPercent(ElemsToWork, ElemsWorked);
      old_Id := Selected_Stack[i];
      if old_Id < 0 then
      begin
            // get layer index
        old_Id := (old_Id + ot_Layer) * -1;
        aLayer := Document.Layers.GetLayerByName(Selected_StackLayernames[old_Id]);
        if aLayer.LayerName <> SourceLayer then
          aLayer := nil;
      end
      else if aLayer <> nil then
      begin
        aBase := Document.GetObjectByIndex(old_Id);
        aObject := aBase;
        new_Id := AxObjectMan.CopyObjectToLayer(aObject, TargetLayer);
        dup_Id := 0;
        if checkforduplicates then
          dup_Id := AxObjectMan.CheckIdHasDuplicateOnLayer(new_Id, TargetLayer);
        if (dup_Id > 0) then
        begin
          Document.DeleteObjectByIndex(new_Id);
          continue;
        end;
        if ProcessDatabase then
        begin
          if AxImpExpDbc.DoExportData(SourceLayer, old_Id) then
          begin
            AxImpExpDbc.AddImpIdInfo(TargetLayer, new_Id);
            if aBase.ObjectType = ot_CPoly then
              AxImpExpDbc.AddImpInfo(TargetLayer, 'AREA', floattostr(aObject.Area));
            while AxImpExpDbc.GetExportData(SourceLayer, Colname, ColInfo) do
              AxImpExpDbc.AddImpInfo(TargetLayer, Colname, ColInfo);
          end;
        end;
      end;
    end;
  end;
  Destroy_ManipulationInstance;
  Destroy_StringFileInstance;

  if ProcessDatabase then
    Destroy_DatabaseInstance;
end;

// ------------------------------------------------------------------------
// NAME: SetApp
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to set the DX-Reference
// PARAMETERS : aApp -> Pointer to DX-Object
// ------------------------------------------------------------------------

procedure TAxGisCombine.SetApp(var aApp: OleVariant);
begin
  App := aApp;
end;

// ------------------------------------------------------------------------
// NAME: SetDocument
// ------------------------------------------------------------------------
// DESCRIPTION: Method is used to set the document reference
// PARAMETERS : aDocument -> Pointer to Document object
// ------------------------------------------------------------------------

procedure TAxGisCombine.SetDocument(var aDocument: OleVariant);
var Doc: IDispatch;
begin
  Doc := aDocument;
  Document := Doc as IDocument;
end;

//------------------------------------------------------------------------------
// NAME : BEV_Process_Mappenblatt
//------------------------------------------------------------------------------
// Method is used to process Mappenblätter for BEV
//
// PARAMETERS : Filename -> File that contains setup for processing
//              DataSource              -> Source of Data (Layers / Selected)
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.BEV_Process_Mappenblatt(const Filename: string; Datasource: AxCmbDataSource);
var SymbolKeeper: TObjectRefKeeper;
  TextKeeper: TObjectRefKeeper;
  Filter: TObjectRefFilter;
  SymbolIdx: integer;
  TextIdx: integer;
  ElemsToWork: integer;
  ElemsWorked: integer;
  SymbolRef: TSymbolRef;
  TextRef: TTextRef;
  aDistance: double;
  aPolygon: IPolygon;
  aPoint: IPoint;
  aLayer: ILayer;
  new_Id: integer;
  DuplId: integer;
  aText: string;
  MBlattNr: string;
  MBlattDate: string;

  MappenBlattLayer: string;
  MappenBlattSymbol: string;
  MappenBlattDest: string;
  MappenBlattHeight: double;
  MappenBlattWidth: Double;
  dummy: string;
  inifile: TInifile;
  count: integer;
begin
  inifile := TIniFile.Create(Filename);
  dummy := inifile.ReadString('SETTINGS', 'ProcessMappenBlatt', 'TRUE');
  if dummy <> 'TRUE' then
  begin
    inifile.Destroy;
    exit;
  end;

   // read settings from file
  MappenBlattLayer := inifile.ReadString('PARZELLENSETTINGS', 'MappenblattLayer', '');
  MappenBlattSymbol := inifile.ReadString('PARZELLENSETTINGS', 'MappenblattSymbol', '');
  MappenBlattDest := inifile.ReadString('PARZELLENSETTINGS', 'MappenblattDest', '');
  dummy := inifile.ReadString('PARZELLENSETTINGS', 'MappenblattHeight', ''); val(dummy, MappenBlattHeight, count);
  dummy := inifile.ReadString('PARZELLENSETTINGS', 'MappenblattWidth', ''); val(dummy, MappenBlattWidth, count);
  inifile.Destroy;

   // initiate the StringFile class
  Create_StringFileInstance;
  FEvents.OnDisplayPass(StrfileObj.Assign(102)); // Process BEV Mappenblätter

   // setup database interface
  Create_DatabaseInstance;

  if DataSource = ds_Selected then
    Create_SelectedStack;

   // create target layer
  if not CheckLayerExists(MappenBlattDest) then
    CreateLayer(MappenBlattDest);
  aLayer := Document.Layers.GetLayerByName(MappenBlattDest);

   // setup database
  AxImpExpDbc.CheckAndCreateImpTable(MappenBlattDest);
  AxImpExpDbc.AddImpIdColumn(MappenBlattDest);
  AxImpExpDbc.AddImpColumn(MappenBlattDest, 'MBNR', Col_String, 20);
  AxImpExpDbc.AddImpColumn(MappenBlattDest, 'DATUM', Col_String, 20);
  AxImpExpDbc.CreateImpTable(MappenBlattDest);
  AxImpExpDbc.OpenImpTable(MappenBlattDest);

   // first read all symbols from layer MappenBlattLayer
  SymbolKeeper := TObjectRefKeeper.Create;
  Filter := TObjectRefFilter.Create;
  Filter.AddObjectTypeToFilter(Filter_Symbol);
  Filter.AddSupportedLayerContent(MappenBlattLayer, MappenBlattSymbol);
  Filter.ReadAttribs := false; // no database information will be read

  FEvents.OnDisplayPass(StrfileObj.Assign(103) + MappenBlattLayer); // Read Mappenblatt symbols from Layer:
  FillObjectKeeper(Filter, SymbolKeeper, DataSource);

   // read texts from layer MappenBlattLayer
  TextKeeper := TObjectRefKeeper.Create;
  Filter.Clear;
  Filter.AddObjectTypeToFilter(Filter_Text);
  Filter.AddSupportedLayer(MappenBlattLayer);
  Filter.ReadAttribs := false;

  FEvents.OnDisplayPass(StrfileObj.Assign(104) + MappenBlattLayer); // Read Mappenblatt texts from Layer:
  FillObjectKeeper(Filter, TextKeeper, DataSource);

   // now process mappenblatt information
  FEvents.OnDisplayPass(StrfileObj.Assign(105)); // Generate Mappenblätter...

  Create_ManipulationInstance;

  ElemsToWork := SymbolKeeper.Objects.Count; ElemsWorked := 0;

  for SymbolIdx := 0 to SymbolKeeper.Objects.Count - 1 do
  begin
    SymbolRef := SymbolKeeper.Objects.Items[SymbolIdx] as TSymbolRef;
    for TextIdx := 0 to TextKeeper.Objects.Count - 1 do
    begin
      TextRef := TextKeeper.Objects.Items[TextIdx] as TTextRef;
      if (TextRef.X >= SymbolRef.X) and
        (TextRef.X <= SymbolRef.X + MappenBlattWidth) and
        (TextRef.Y >= SymbolRef.Y) and
        (TextRef.Y <= SymbolRef.Y + MappenBlattHeight) then
      begin
            // calculate the distance between the text and the symbol
        aDistance := CalculateLength(TextRef.X, TextRef.Y, SymbolRef.X, SymbolRef.Y);
        if (SymbolRef.LinkedObjects = nil) or (aDistance < SymbolRef.DoubleValue) then
        begin
          if SymbolRef.LinkedObjects = nil then
          begin
            SymbolRef.CreateLinkedObjectList;
            SymbolRef.LinkedObjects.Add(TextRef);
          end
          else
            SymbolRef.LinkedObjects.Items[0] := TextRef;
          SymbolRef.DoubleValue := aDistance;
        end;
      end;
    end;

      // now create the Mappenblatt object itself
    APolygon := Document.CreatePolygon;

    APoint := Document.CreatePoint;
    APoint.X := round(SymbolRef.X * 100); // left bottom corner
    APoint.Y := round(SymbolRef.Y * 100);
    APolygon.InsertPoint(APoint);

    APoint := Document.CreatePoint;
    APoint.X := round((SymbolRef.X + MappenBlattWidth) * 100); // right bottom corner
    APoint.Y := round(SymbolRef.Y * 100);
    APolygon.InsertPoint(APoint);

    APoint := Document.CreatePoint;
    APoint.X := round((SymbolRef.X + MappenBlattWidth) * 100); // right upper corner
    APoint.Y := round((SymbolRef.Y + MappenBlattHeight) * 100);
    APolygon.InsertPoint(APoint);

    APoint := Document.CreatePoint;
    APoint.X := round(SymbolRef.X * 100); // left upper corner
    APoint.Y := round((SymbolRef.Y + MappenBlattHeight) * 100);
    APolygon.InsertPoint(APoint);

    APolygon.ClosePoly;
    aLayer.InsertObject(APolygon);
    new_Id := APolygon.Index;

      // check if this Mappenblatt already exists
    Duplid := AxObjectMan.CheckIdHasDuplicateOnLayer(new_Id, MappenBlattDest);

    if (DuplId > 0) then
      Document.DeleteObjectByIndex(new_Id)
    else
    begin
         // now check if also an entry for the database should be done
      if (SymbolRef.LinkedObjects <> nil) then
      begin
        TextRef := SymbolRef.LinkedObjects.Items[0] as TTextRef;
        aText := TextRef.Text;
            // extract number and date
        MBlattNr := copy(aText, 1, Pos('*', aText) - 1);
        delete(aText, 1, Pos('*', aText));
        MBlattDate := copy(aText, 1, Pos('*', aText) - 1);
        AxImpExpDbc.AddImpIdInfo(MappenBlattDest, new_id);
        AxImpExpDbc.AddImpInfo(MappenBlattDest, 'MBNR', MBlattNr);
        AxImpExpDbc.AddImpInfo(MappenBlattDest, 'DATUM', MBlattDate);
      end;
    end;
    ElemsWorked := ElemsWorked + 1;
    ShowPercent(ElemsToWork, ElemsWorked);
  end;

   // remove objects
  SymbolKeeper.Destroy;
  TextKeeper.Destroy;
  Filter.Destroy;

  Destroy_ManipulationInstance;
  Destroy_DatabaseInstance;
  Destroy_StringFileInstance;

  if DataSource = ds_Selected then
    Destroy_SelectedStack;
end;

//------------------------------------------------------------------------------
// NAME : DKM_Process_LayersToDelete
//------------------------------------------------------------------------------
// Method is used to do process the Dkm-Layers that should be deleted
//
// PARAMETERS: Filename -> Filename of the .ini file that contains the
//                         information which layers should be deleted.
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.DKM_Process_LayersToDelete(const Filename: string; const SectionName: string);
var inifile: TIniFile;
  iniitems: TStrings;
  i: integer;
  LayerToDelete: string;
  dummy: string;
begin
  inifile := TIniFile.Create(Filename);
  Create_StringFileInstance;
  Create_DatabaseInstance;
  iniitems := TStringList.Create;
  inifile.ReadSection(SectionName, iniitems);
  for i := 0 to iniitems.Count - 1 do
  begin
    LayerToDelete := inifile.ReadString(SectionName, iniitems[i], '');
      // check if layer exists
    if not CheckLayerExists(LayerToDelete) then
      continue;
    FEvents.OnDisplayPass(StrFileObj.Assign(106) + LayerToDelete); // Delete object and database information for layer:
    AxImpExpDbc.CheckAndCreateExpTable(LayerToDelete);
    AxImpExpDbc.DeleteExpTable(LayerToDelete);
    RemoveLayerFromProject(LayerToDelete);
  end;
  iniitems.Destroy;
  inifile.Free;
  Destroy_DatabaseInstance;
  Destroy_StringFileInstance;
end;

//------------------------------------------------------------------------------
// NAME : BEV_ProcessParzellen
//------------------------------------------------------------------------------
// Method is used to do process the Parzellen for BEV
//
// PARAMETERS: Filename   -> Filename of the .ini file that contains combine
//                           Information
//             DataSource -> Data source of objects (Layers/Selected)
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.BEV_Process_Parzellen(const Filename: string; Datasource: AxCmbDataSource);
var inifile: TiniFile;
  TargetLayerGenerateObjects: string;
  TargetLayerIslands: string;
  dummy: string;
  count: integer;
  ErrorlayerObjects: string;
  ErrorlayerSymbols: string;
  i: integer;
  iniitems: TStrings;
  LayersToProcess: TStrings;
  CPolyList: IAxCmbStringList;
  SymOnLayerList: IAxCmbSymOnLayerList;
  aSymName, aLayername: string;
  Setup: IAxCmbSymolsWithCPolysSetup;
  aObject: OleVariant;
  aIndex: integer;
  newIndex: integer;
  Colname: widestring;
  ColInfo: widestring;
  ColType: TOleEnum;
  ColLength: integer;
begin
  inifile := TIniFile.Create(Filename);
   // check if the parzellen should be processed
  dummy := inifile.ReadString('SETTINGS', 'ProcessParzellen', 'TRUE');
  if (dummy <> 'TRUE') then
  begin
      // parzellen should not be processed -> exit
    inifile.Destroy;
    exit;
  end;

   // if complete layer has to be processed -> remove old results
  if (DataSource <> ds_Selected) then
  begin
    Create_DataBaseInstance;
    dummy := inifile.ReadString('PARZELLENSETTINGS', 'TargetlayerGenerateObjects', '');
    RemoveLayerFromProject(dummy);

    dummy := inifile.ReadString('PARZELLENSETTINGS', 'TargetlayerIslands', '');
    if CheckLayerExists(dummy) then
    begin
      AxImpExpDbc.CheckandCreateExpTable(dummy);
      AxImpExpDbc.EmptyExpTable(dummy); // empty table if it exists
      EmptyLayerInProject(dummy); // empty layer if it exists
    end;
      // check if the error layers exist
    dummy := inifile.ReadString('PARZELLENSETTINGS', 'ErrorlayerObjects', '');
    if CheckLayerExists(dummy) then
    begin
      AxImpExpDbc.CheckandCreateExpTable(dummy);
      AxImpExpDbc.EmptyExpTable(dummy); // empty table if it exists
      EmptyLayerInProject(dummy); // empty layer if it exists
    end;
    dummy := inifile.ReadString('PARZELLENSETTINGS', 'ErrorlayerSymbols', '');
    if CheckLayerExists(dummy) then
    begin
      AxImpExpDbc.CheckandCreateExpTable(dummy);
      AxImpExpDbc.EmptyExpTable(dummy); // empty table if it exists
      EmptyLayerInProject(dummy); // empty layer if it exists
    end;
    Destroy_DatabaseInstance;
  end
  else
    Create_SelectedStack;

  Create_StringFileInstance;
  FEvents.OnDisplayPass(StrFileObj.Assign(101)); // Process Parzellen

   // get the target layer for generate objects
  TargetLayerGenerateObjects := inifile.ReadString('PARZELLENSETTINGS', 'TargetlayerGenerateObjects', '');
  if DataSource = ds_Selected then // if selected objects should be used -> first generate to temporary layer
    TargetLayerGenerateObjects := 'TMP_' + TargetLayerGenerateObjects;

   // get the layers that are used for generation of objects
  iniitems := TStringList.Create;
  LayersToProcess := TStringList.Create;
  inifile.ReadSection('PARZELLENGENLAYERS', iniitems);
  for i := 0 to iniitems.Count - 1 do
    LayersToProcess.Add(inifile.ReadString('PARZELLENGENLAYERS', iniitems[i], ''));

   // get the fetch radius
  dummy := inifile.ReadString('SETTINGS', 'Fetchradius', '');
  val(dummy, FetchRadius, count); if count <> 0 then FetchRadius := 0.01;

   // generate the objects
  GenerateObjects(LayersToProcess, TargetLayerGenerateObjects, DataSource);
  LayersToProcess.Destroy;

   // now the island calculation should be done
  TargetlayerIslands := inifile.ReadString('PARZELLENSETTINGS', 'TargetlayerIslands', '');
  if Datasource = ds_Selected then
    TargetLayerIslands := 'TMP_' + TargetLayerIslands;
  Create_ManipulationInstance;
  AxObjectMan.CreateIslandLayer(TargetLayerGenerateObjects, TargetlayerIslands);
  Destroy_ManipulationInstance;

   // now the Parzellen-Symbols will be combined with the Parzellen
  ErrorlayerObjects := inifile.ReadString('PARZELLENSETTINGS', 'ErrorlayerObjects', '');
  ErrorlayerSymbols := inifile.ReadString('PARZELLENSETTINGS', 'ErrorlayerSymbols', '');

  CPolyList := CreateStringList;
  CPolyList.Add(TargetlayerIslands);
  SymOnLayerList := CreateSymbolOnLayerList;
  inifile.ReadSection('PARZELLENSYMBOLS', iniitems);
  for i := 0 to iniitems.Count - 1 do
  begin
    dummy := inifile.ReadString('PARZELLENSYMBOLS', iniitems[i], '');
    aSymName := copy(dummy, 1, Pos(',', dummy) - 1);
    delete(dummy, 1, Pos(',', dummy));
      // get the layer-name
    aLayername := dummy;
    SymOnLayerList.Add(aSymName, aLayername);
  end;
  iniitems.Destroy;
  Setup := CreateCombineSymbolsWithCPolysSetup;
  Setup.CPolyErrorLayer := ErrorlayerObjects;
  Setup.SymbolErrorLayer := ErrorlayerSymbols;
  Setup.ProcessCPolyDatabase := true;
  Setup.ProcessCPolyErrorDatabase := true;
  Setup.ProcessSymbolErrorDatabase := true;
  Setup.CheckMultiAssignedSymbols := false;

  CombineCPolysWithSymbols(CPolyList, SymOnLayerList, Setup, ds_Layers, DataSource); // Polygons are on layer, and symbols from datasouce

  if Datasource = ds_Selected then
  begin
      // copy result from temporary layer to original layer
    TargetlayerIslands := inifile.ReadString('PARZELLENSETTINGS', 'TargetlayerIslands', '');
    CopyNonDuplicatesToLayer('TMP_' + TargetLayerIslands, TargetLayerIslands, TRUE {use database}, ds_Layers);

    TargetLayerGenerateObjects := inifile.ReadString('PARZELLENSETTINGS', 'TargetlayerGenerateObjects', '');
    CopyNonDuplicatesToLayer('TMP_' + TargetLayerGenerateObjects, TargetLayerGenerateObjects, TRUE {use database}, ds_Layers);

      // delete temporary layers for generate objects and targetlayer islands
    Create_DatabaseInstance;
    AxImpExpDbc.CheckAndCreateExpTable('TMP_' + TargetlayerIslands); AxImpExpDbc.DeleteExpTable('TMP_' + TargetlayerIslands);
    AxImpExpDbc.CheckAndCreateExpTable('TMP_' + TargetLayerGenerateObjects); AxImpExpDbc.DeleteExpTable('TMP_' + TargetLayerGenerateObjects);
    Destroy_DatabaseInstance;
    RemoveLayerFromProject('TMP_' + TargetlayerIslands);
    RemoveLayerFromProject('TMP_' + TargetLayerGenerateObjects);
  end;

   // delete the error layers if no errors occured
  if (GetNumObjectsOfLayer(ErrorlayerObjects) = 0) and (CheckLayerExists(ErrorlayerObjects)) then
    RemoveLayerFromProject(ErrorlayerObjects);
  if (GetNumObjectsOfLayer(ErrorlayerSymbols) = 0) and (CheckLayerExists(ErrorlayerSymbols)) then
    RemoveLayerFromProject(ErrorlayerSymbols);
  iniFile.Free;
  Destroy_StringFileInstance;
  if (DataSource = ds_Selected) then
    Destroy_SelectedStack;
end;

//------------------------------------------------------------------------------
// NAME : BEV_Create_Nutzungen
//------------------------------------------------------------------------------
// Method is used to do create the Nutzungsflächen and combine them with symbols
//
// PARAMETERS: Filename  ->  Filename of the .ini file that contains combine
//                           Information
//             DataSource -> Source of objects (Layers/Selected)
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.BEV_Create_Nutzungen(const Filename: string; Datasource: AxCmbDataSource);
var inifile: TIniFile;
  dummy: string;
  count: integer;
  TargetLayerGenerateObjects: string;
  iniitems: TStrings;
  LayersToProcess: TStrings;
  i: integer;
  NutzungsLayer: string;
  ErrorlayerObjects: string;
  ErrorlayerSymbols: string;
  CPolyList: IAxCmbStringList;
  SymOnLayerList: IAxCmbSymOnLayerList;
  aSymName: string;
  aLayername: string;
  Setup: IAxCmbSymolsWithCPolysSetup;
  NutzungsColumns: TStrings;
  ColName: widestring;
  ColInfo: widestring;
  ColType: string;
  ColLength: integer;
  AssignToParzellen: boolean;
  AssignToParzellenCols: TStrings;
  AssignToParzellenIdCol: string;
  ParzellenLayer: string;
  Bytes2Process: integer;
  BytesProcessed: integer;
  aFile: file of byte;
  CombFile: Textfile;
  Line: string;
  CPolyId: integer;
  aCPoly: OleVariant;
  SymId: integer;
  aSymbol: OleVariant;
  SymbolName: string;
  WidmungsCode: string;
  DuplId: integer;
  AssignedParzellenId: integer;
begin
   // now process the Nutzungsflächen
  inifile := TIniFile.Create(Filename);
  dummy := inifile.ReadString('SETTINGS', 'CreateNutzungen', 'FALSE');
  if dummy <> 'TRUE' then
  begin
    inifile.Destroy;
    exit; // nothing to do
  end;

      // if complete layer has to be processed -> remove old results
  if (DataSource <> ds_Selected) then
  begin
    Create_DataBaseInstance;
    dummy := inifile.ReadString('NUTZUNGSSETTINGS', 'TargetlayerGenerateObjects', '');
    RemoveLayerFromProject(dummy);

    dummy := inifile.ReadString('NUTZUNGSSETTINGS', 'TargetlayerIslands', '');
    if CheckLayerExists(dummy) then
    begin
      AxImpExpDbc.CheckandCreateExpTable(dummy);
      AxImpExpDbc.EmptyExpTable(dummy); // empty table if it exists
      EmptyLayerInProject(dummy); // empty layer if it exists
    end;
      // check if the error layers exist
    dummy := inifile.ReadString('NUTZUNGSSETTINGS', 'ErrorlayerObjects', '');
    if CheckLayerExists(dummy) then
    begin
      AxImpExpDbc.CheckandCreateExpTable(dummy);
      AxImpExpDbc.EmptyExpTable(dummy); // empty table if it exists
      EmptyLayerInProject(dummy); // empty layer if it exists
    end;
    dummy := inifile.ReadString('NUTZUNGSSETTINGS', 'ErrorlayerSymbols', '');
    if CheckLayerExists(dummy) then
    begin
      AxImpExpDbc.CheckandCreateExpTable(dummy);
      AxImpExpDbc.EmptyExpTable(dummy); // empty table if it exists
      EmptyLayerInProject(dummy); // empty layer if it exists
    end;
    Destroy_DatabaseInstance;
  end
  else
    Create_SelectedStack;

  Create_StringFileInstance;
  FEvents.OnDisplayPass(StrFileObj.Assign(107)); // Process Parzellen

   // get the target layer for generate objects
  TargetLayerGenerateObjects := inifile.ReadString('NUTZUNGSSETTINGS', 'TargetlayerGenerateObjects', '');
  if DataSource = ds_Selected then // if selected objects should be used -> first generate to temporary layer
    TargetLayerGenerateObjects := 'TMP_' + TargetLayerGenerateObjects;

   // get the layers that are used for generation of objects
  iniitems := TStringList.Create;
  LayersToProcess := TStringList.Create;
  inifile.ReadSection('NUTZUNGSGENLAYERS', iniitems);
  for i := 0 to iniitems.Count - 1 do
    LayersToProcess.Add(inifile.ReadString('NUTZUNGSGENLAYERS', iniitems[i], ''));

   // get the fetch-radius
  dummy := inifile.ReadString('SETTINGS', 'Fetchradius', '');
  val(dummy, FetchRadius, count); if count <> 0 then FetchRadius := 0.01;

   // generate the objects
  GenerateObjects(LayersToProcess, TargetLayerGenerateObjects, DataSource);
  LayersToProcess.Destroy;

   // now the island calculation should be done
  NutzungsLayer := inifile.ReadString('NUTZUNGSSETTINGS', 'NutzungsLayer', '');
  if Datasource = ds_Selected then
    NutzungsLayer := 'TMP_' + NutzungsLayer;
  Create_ManipulationInstance;
  AxObjectMan.CreateIslandLayer(TargetLayerGenerateObjects, NutzungsLayer);
  Destroy_ManipulationInstance;


   // now the Nutzungs-Symbols will be combined with the Nutzungsareas
  ErrorlayerObjects := inifile.ReadString('NUTZUNGSSETTINGS', 'ErrorlayerObjects', '');
  ErrorlayerSymbols := inifile.ReadString('NUTZUNGSSETTINGS', 'ErrorlayerSymbols', '');

  CPolyList := CreateStringList;
  CPolyList.Add(NutzungsLayer);
  SymOnLayerList := CreateSymbolOnLayerList;
  inifile.ReadSection('NUTZUNGSSYMBOLS', iniitems);
  for i := 0 to iniitems.Count - 1 do
  begin
    dummy := inifile.ReadString('NUTZUNGSSYMBOLS', iniitems[i], '');
    aSymName := copy(dummy, 1, Pos(',', dummy) - 1);
    delete(dummy, 1, Pos(',', dummy));
      // get the layer-name
    aLayername := dummy;
    SymOnLayerList.Add(aSymName, aLayername);
  end;
  iniitems.Destroy;

  Setup := CreateCombineSymbolsWithCPolysSetup;
  Setup.CPolyErrorLayer := ErrorlayerObjects;
  Setup.SymbolErrorLayer := ErrorlayerSymbols;
  Setup.ProcessCPolyDatabase := false;
  Setup.ProcessCPolyErrorDatabase := false;
  Setup.ProcessSymbolErrorDatabase := false;
  Setup.CheckMultiAssignedSymbols := false;
  Setup.CombineResultFilename := FWorkingDir + 'CombCPoly.tab';

  CombineCPolysWithSymbols(CPolyList, SymOnLayerList, Setup, ds_Layers, DataSource); // Polygons are from layer and symbols from datasource

   // now read out the file with the combine result to add database info
  NutzungsLayer := inifile.ReadString('NUTZUNGSSETTINGS', 'NutzungsLayer', '');
  Create_DatabaseInstance;
  Create_ManipulationInstance;

   // create table for the Nutzungs Layer
  AxImpExpDbc.CheckAndCreateImpTable(NutzungsLayer);
   // Id and AREA always will be created
  AxImpExpDbc.AddImpIdColumn(NutzungsLayer);
  AxImpExpDbc.AddImpColumn(NutzungsLayer, 'AREA', Col_Float, 0);

   // get the columns that should be defined for the Nutzungsflächen
  NutzungsColumns := nil;
  iniitems := TStringList.Create;
  inifile.ReadSection('NUTZUNGSCOLUMNS', iniitems);
  if iniitems.Count > 0 then
  begin
    NutzungsColumns := TStringList.Create;
    for i := 0 to iniitems.Count - 1 do
    begin
         // get the column definitions
      dummy := inifile.ReadString('NUTZUNGSCOLUMNS', iniitems[i], '');
         // get the column-name
      ColName := copy(dummy, 1, Pos(',', dummy) - 1); ColName := Trim(ColName);
      delete(dummy, 1, Pos(',', dummy));
         // get the column-type
      ColType := copy(dummy, 1, Pos(',', dummy) - 1); ColType := Trim(ColType);
      delete(dummy, 1, Pos(',', dummy));
      val(dummy, ColLength, count);
      if ColType = 'Col_Int' then AxImpExpDbc.AddImpColumn(NutzungsLayer, ColName, Col_Int, 0);
      if ColType = 'Col_SmallInt' then AxImpExpDbc.AddImpColumn(NutzungsLayer, ColName, Col_SmallInt, 0);
      if ColType = 'Col_Float' then AxImpExpDbc.AddImpColumn(NutzungsLayer, ColName, Col_Float, 0);
      if ColType = 'Col_Date' then AxImpExpDbc.AddImpColumn(NutzungsLayer, ColName, Col_Date, 0);
      if ColType = 'Col_String' then AxImpExpDbc.AddImpColumn(NutzungsLayer, ColName, Col_String, ColLength);
      NutzungsColumns.Add(Colname);
    end;
  end
  else
  begin
      // if no column exist in the NUTZUNGSCOLUMNS create column WIDMUNG
    AxImpExpDbc.AddImpColumn(NutzungsLayer, 'WIDMUNG', Col_String, 255);
  end;
  iniitems.Destroy;

   // Symbol column will always be created
  AxImpExpDbc.AddImpColumn(NutzungsLayer, 'SYMBOL', Col_String, 255);

   // now check if the Nutzungen should be assigned with a reference to the Parzellen
  dummy := inifile.ReadString('NUTZUNGSSETTINGS', 'AssignToParzellen', 'FALSE');
  if dummy = 'TRUE' then AssignToParzellen := true else AssignToParzellen := false;
  AssignToParzellenCols := nil; AssignToParzellenIdCol := '';

  if AssignToParzellen then
  begin
    AssignToParzellenCols := TStringList.Create;
    iniitems := TStringList.Create;
    inifile.ReadSection('ASSIGNNUTZUNGTOPARZELLENCOLUMNS', iniitems);
    for i := 0 to iniitems.Count - 1 do
    begin
      ColName := inifile.ReadString('ASSIGNNUTZUNGTOPARZELLENCOLUMNS', iniitems[i], '');
      if iniitems[i] = 'LINKID' then
      begin
        AxImpExpDbc.AddImpColumn(NutzungsLayer, ColName, Col_Int, 0);
        AssignToParzellenIdCol := ColName;
      end
      else
      begin
            // get columns width from ini-settings
        dummy := inifile.ReadString('DBCOLLENGTH', ColName, '512'); // default length is 512
        val(dummy, ColLength, count);
        AxImpExpDbc.AddImpColumn(NutzungsLayer, ColName, Col_String, ColLength);
        AssignToParzellenCols.Add(ColName);
      end;
    end;
    iniitems.Destroy;
  end;

  AxImpExpDbc.CreateImpTable(NutzungsLayer);
  AxImpExpDbc.OpenImpTable(NutzungsLayer);

  if (AssignToParzellen) then
  begin
      // open also the parzellen database to get database information
    ParzellenLayer := inifile.ReadString('PARZELLENSETTINGS', 'TargetlayerIslands', '');
    if (AssignToParzellenCols <> nil) then
    begin
      AxImpExpDbc.CheckAndCreateExpTable(ParzellenLayer);
      AxImpExpDbc.OpenExpTable(ParzellenLayer);
    end;
  end;

   // now read out the combining result and create the table
  FEvents.OnDisplayPass(StrFileObj.Assign(108)); // Create database Nutzungs Database entries

   // now get size of file to show progress
  Bytes2Process := 0; BytesProcessed := 0;
  AssignFile(aFile, FWorkingDir + 'CombCPoly.tab');
  Filemode := 0;
  Reset(aFile);
  Bytes2Process := FileSize(aFile);
  Closefile(aFile);

  AssignFile(CombFile, FWorkingDir + 'CombCPoly.tab');
  Reset(CombFile);
  while not Eof(CombFile) do
  begin
    readln(CombFile, Line); BytesProcessed := BytesProcessed + length(Line);
    ShowPercent(Bytes2Process, BytesProcessed);
    dummy := copy(Line, 1, Pos(' ', Line) - 1);
    val(dummy, CPolyId, count); if count <> 0 then CPolyId := 0;
    if (CPolyId > 0) then
    begin
      aCPoly := Document.GetObjectByIndex(CPolyId);
      delete(Line, 1, Pos(' ', Line));
      val(Line, SymId, count);
      aSymbol := Document.GetObjectByIndex(SymId);
         // now get the Symbolname of the Symbol
      SymbolName := aSymbol.SymbolName;
         // get the Widmungs code that belongs to this symbol
      WidmungsCode := inifile.ReadString('NUTZUNGSSYMBOLLAYERS', SymbolName, '');

      if DataSource = ds_Selected then
      begin
            // datasource is selected -> check if widmung is not already on Nutzungslayer
        DuplId := AxObjectMan.CheckIdHasDuplicateOnLayer(CPolyId, NutzungsLayer);
        if DuplId > 0 then
          continue; // due to Polygon is already on Nutzungslayer -> continue
            // due to object has not duplicate copy it to the Nutzungslayer
        CPolyId := AxObjectMan.CopyObjectToLayer(aCPoly, NutzungsLayer);
      end;

         // Add Entry to database
      AxImpExpDbc.AddImpIdInfo(NutzungsLayer, CPolyId);
      dummy := floattostr(aCPoly.Area);
      AxImpExpDbc.AddImpInfo(NutzungsLayer, 'AREA', dummy);
      if NutzungsColumns <> nil then
      begin
        for i := 0 to NutzungsColumns.Count - 1 do
        begin
          ColName := NutzungsColumns[i];
               // now get the corresponding info
          if Pos(',', WidmungsCode) <> 0 then
          begin
            dummy := copy(WidmungsCode, 1, Pos(',', WidmungsCode) - 1); dummy := Trim(dummy);
            AxImpExpDbc.AddImpInfo(NutzungsLayer, NutzungsColumns[i], dummy);
            delete(WidmungsCode, 1, Pos(',', WidmungsCode));
          end
          else
            AxImpExpDbc.AddImpInfo(NutzungsLayer, NutzungsColumns[i], WidmungsCode);
        end;
      end
      else // if no NutzungsColumns are set -> write WIDMUNG
        AxImpExpDbc.AddImpInfo(NutzungsLayer, 'WIDMUNG', WidmungsCode);
      AxImpExpDbc.AddImpInfo(NutzungsLayer, 'SYMBOL', SymbolName);
      if AssignToParzellen then
      begin
        AssignedParzellenId := AxObjectMan.CheckIdHasDuplicateOnLayer(CPolyId, ParzellenLayer);
            // it could be that a Nutzungsfläche is inside of a Parzelle (not equal)
            // -> this also has to be checked
        if (AssignedParzellenId < 1) then
        begin
          AssignedParzellenId := AxObjectMan.GetIdOfSurroundingCPolyOnLayer(CPolyId, ParzellenLayer);
        end;

        if (AssignedParzellenId > 0) then
        begin
               // -> Nutzungsfläche has duplicate Parzelle
          if (AssignToParzellenIdCol <> '') then
            AxImpExpDbc.AddImpInfo(NutzungsLayer, AssignToParzellenIdCol, inttostr(AssignedParzellenId));
               // now get database information of other columns that should be assigned
          if (AssignToParzellenCols <> nil) then
          begin
            if AxImpExpDbc.DoExportData(ParzellenLayer, AssignedParzellenId) then
            begin
              while AxImpExpDbc.GetExportData(ParzellenLayer, ColName, ColInfo) do
              begin
                        // check if Column is in Columns that should be assigned to a Parzelle
                if AssignToParzellenCols.IndexOf(ColName) >= 0 then
                  AxImpExpDbc.AddImpInfo(NutzungsLayer, ColName, ColInfo);
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  CloseFile(CombFile);

  inifile.Free;

  Destroy_ManipulationInstance;
  Destroy_DatabaseInstance;
  Destroy_StringFileInstance;

  if NutzungsColumns <> nil then
    NutzungsColumns.Destroy;

  if AssignToParzellenCols <> nil then
    AssignToParzellenCols.Destroy;

   // delete the TargetlayerGenerateObjects
  RemoveLayerFromProject(TargetlayerGenerateObjects);
  if DataSource = ds_Selected then
    RemoveLayerFromProject('TMP_' + NutzungsLayer);

   // delete the Error-Layers
  if (CheckLayerExists(ErrorLayerObjects)) and (GetNumObjectsOfLayer(ErrorLayerObjects) = 0) then
    RemoveLayerFromProject(ErrorLayerObjects);
  if (CheckLayerExists(ErrorLayerSymbols)) and (GetNumObjectsOfLayer(ErrorLayerSymbols) = 0) then
    RemoveLayerFromProject(ErrorLayerSymbols);

  if DataSource = ds_Selected then
    Destroy_SelectedStack;
end;

//------------------------------------------------------------------------------
// NAME : BEV_Combine_Nutzungen
//------------------------------------------------------------------------------
// Method is used to combine nutzungen for BEV
//
// PARAMETERS:  Filename   -> Filename of the .ini file that contains combine
//                            Information
//              Datasource -> Source of objects (layers/selected)
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.BEV_Combine_Nutzungen(const Filename: WideString; Datasource: AxCmbDataSource);
var inifile: TIniFile;
  dummy: string;
  NutzungsLayer, CombinedNutzungen: string;
  AttribList: IAxCmbStringList;
  iniitems: TStrings;
  i: integer;
  ColName: string;
begin
   // check if the Nutzungsflächen should be combined
  inifile := TIniFile.Create(Filename);
  dummy := inifile.ReadString('SETTINGS', 'CombineNutzungen', 'FALSE');
  if dummy <> 'TRUE' then
  begin
    inifile.Destroy;
    exit; // nothing to do -> exit
  end;

   // combine the nutzungen on the Nutzungslayer
  NutzungsLayer := inifile.ReadString('NUTZUNGSSETTINGS', 'NutzungsLayer', '');
  CombinedNutzungen := inifile.ReadString('NUTZUNGSSETTINGS', 'CombinedNutzungen', '');
   // delete the combine result if there is already one
  if DataSource = ds_Layers then
  begin
    if CheckLayerExists(CombinedNutzungen) then
    begin
      Create_DatabaseInstance;
      AxImpExpDbc.CheckandCreateExpTable(CombinedNutzungen);
      AxImpExpDbc.EmptyExpTable(CombinedNutzungen); // empty layer if it already exists
      EmptyLayerInProject(CombinedNutzungen);
      Destroy_DatabaseInstance;
    end;
  end;
   // Add the attributes that should be checked if they are equal
   // get the columns to be combined from the NUTZUNGSCOLUMNS section
  AttribList := CreateStringList;

  iniitems := TStringList.Create;
  inifile.ReadSection('NUTZUNGSCOLUMNS', iniitems);
  if iniitems.Count > 0 then
  begin
    for i := 0 to iniitems.count - 1 do
    begin
      dummy := inifile.ReadString('NUTZUNGSCOLUMNS', iniitems[i], '');
         // get the column name
      ColName := copy(dummy, 1, Pos(',', dummy) - 1); ColName := Trim(ColName);
      AttribList.Add(ColName);
    end;
  end
  else
    AttribList.Add('WIDMUNG'); // if no items exist add column WIDMUNG

  iniitems.Destroy;
   // Symbol will always be used for combining
  AttribList.Add('SYMBOL');
  CombineCPolysByDatabaseAttrib(NutzungsLayer, CombinedNutzungen, AttribList, ds_Layers); // here datasource is always ds-layers
end;

//------------------------------------------------------------------------------
// NAME : BEV_Combine
//------------------------------------------------------------------------------
// Method is used to do DKM processing for BEV
//
// PARAMETERS:  Filename   -> Filename of the .ini file that contains combine
//                            Information
//              Datasource -> Source of objects (layers/selected)
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.BEV_Combine(const Filename: WideString; Datasource: AxCmbDataSource);
var inifile: TInifile;
  dummy: string;
begin
  ProcessingMode := pro_DKM_BEV; // current Processing Mode is for DKM - BEV
  Create_StringfileInstance;

  if DataSource = ds_Selected then
    Create_SelectedStack;

  FEvents.OnDisplayPass('---------------------------------------------------');
  FEvents.OnDisplayPass(StrFileObj.Assign(100)); // Execute BEV Processing

   // Process the parzellen
  BEV_Process_Parzellen(Filename, Datasource);

   // Process the mappenblätter
  BEV_Process_Mappenblatt(Filename, DataSource);

   // Process the Nutzungsflaechen
  BEV_Process_Nutzungen(Filename, DataSource);

   // Process the layers to merge
  inifile := TIniFile.Create(Filename);
  dummy := inifile.ReadString('SETTINGS', 'ProcessMergeLayers', 'FALSE');
  if dummy = 'TRUE' then
    DKM_Process_LayersToMerge(Filename, 'BEV_MERGELAYERS');

   // delete the layers that are set in the LAYERSTODELETE section
  dummy := inifile.ReadString('SETTINGS', 'ProcessDeleteLayers', 'FALSE');
  inifile.Destroy;
  if dummy = 'TRUE' then
    DKM_Process_LayersToDelete(Filename, 'BEV_LAYERSTODELETE');

  FEvents.OnDisplayPass('---------------------------------------------------');
  Destroy_StringFileInstance;

  if DataSource = ds_Selected then
    Destroy_SelectedStack;

  ProcessingMode := pro_NORMAL; // reset the ProcessingMode

  FEvents.OnDisplayPercent(100);
end;

//------------------------------------------------------------------------------
// NAME : BEV_Process_Nutzungen
//------------------------------------------------------------------------------
// Method is used to do process the Nutzungsflächen
//
// PARAMETERS: Filename   -> Filename of the .ini file that contains combine
//                           Information
//             DataSource -> source of objects (Layers/Selected)
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.BEV_Process_Nutzungen(const Filename: string; Datasource: AxCmbDataSource);
var inifile: TIniFile;
  dummy: string;
begin
   // now process the Nutzungsflächen
  inifile := TIniFile.Create(Filename);
   // first check if the Nutzungsflächen should be combined
  dummy := inifile.ReadString('SETTINGS', 'ProcessNutzungen', 'TRUE');
  inifile.Free;
  if (dummy <> 'TRUE') then
    exit;

   // create the nutzungen
  BEV_Create_Nutzungen(Filename, Datasource);

   // combine the nutzungen
  BEV_Combine_Nutzungen(Filename, Datasource);
end;


//------------------------------------------------------------------------------
// NAME : DKM_Process_LayersToMerge
//------------------------------------------------------------------------------
// Method is used to merge DKM layers
//
// PARAMETERS: Filename    -> Filename of the .ini that contains merge information
//             SectionName -> Section that contains merge information
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.DKM_Process_LayersToMerge(const Filename: string; const SectionName: string);
var inifile: TIniFile;
  iniitems: TStrings;
  i: integer;
  LayerToMerge: string;
  TargetLayer: string;
  dummy: string;
  aPos: integer;
  DataSourceColumn: string;
  LayerList: IAxCmbStringList;
begin
  inifile := TIniFile.Create(Filename);
  iniitems := TStringList.Create;
  inifile.ReadSection(SectionName, iniitems);
  LayerList := CreateStringList;
  for i := 0 to iniitems.Count - 1 do
  begin
    LayerList.Clear;
    TargetLayer := iniitems[i];
    dummy := inifile.ReadString(SectionName, iniitems[i], '');
    if (TargetLayer = 'DataSourceCol') then
    begin
      DataSourceColumn := dummy;
      continue;
    end;
    while (dummy <> '') do
    begin
         // the layers to merge are seperated by comma
      aPos := Pos(',', dummy);
      if aPos > 0 then
      begin
        LayerToMerge := copy(dummy, 1, aPos - 1);
        delete(dummy, 1, aPos);
      end
      else
      begin
        LayerToMerge := dummy;
        dummy := '';
      end;
      LayerList.Add(LayerToMerge);
    end;
    MergeLayers(LayerList, Targetlayer, DataSourceColumn);
  end;
  iniitems.Destroy;
  inifile.Free;
end;

//------------------------------------------------------------------------------
// NAME : CreateLayer
//------------------------------------------------------------------------------
// Method is used to create a layer if it is not already existing
//
// PARAMETERS: Layername -> Name of the layer
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.CreateLayer(Layername: string);
var disp: IDispatch;
begin
   // check if layer exists
  disp := Document.Layers.GetLayerByName(Layername);
  if disp = nil then
    Document.Layers.InsertLayerByName(Layername, True);
end;

//------------------------------------------------------------------------------
// NAME : RemoveLayerFromProject
//------------------------------------------------------------------------------
// Method is used to delete a layer form the project
//
// PARAMETERS : aLayername  -> Name of the layer
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.RemoveLayerFromProject(const aLayername: string);
begin
  if CheckLayerExists(aLayername) then
  begin
    Document.DeleteLayerByName(aLayername);
  end;
end;

//------------------------------------------------------------------------------
// NAME : EmptyLayerInProject
//------------------------------------------------------------------------------
// Method is used to delete all items from a layer
//
// PARAMETERS : aLayername  -> Name of the layer
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.EmptyLayerInProject(const aLayername: string);
var dispatch: IDispatch;
  aLayer: ILayer;
  aBase: IBase;
  aIndex: integer;
begin
  dispatch := Document.Layers.GetLayerByName(aLayername);
  if dispatch <> nil then
  begin
      // we got the layer -> now remove all objects from it
    aLayer := dispatch as ILayer;
    while aLayer.Count > 0 do
    begin
      aBase := aLayer.Items[0];
      aIndex := aBase.Index;
      Document.DeleteObjectByIndex(aIndex);
    end;
  end;
end;

//------------------------------------------------------------------------------
// NAME : OnRequestExpLayerIndex
//------------------------------------------------------------------------------
// Method is used to get the index of a layer by its name
//
// PARAMETERS: Layername  -> Name of the layer
//             LayerIndex -> Index of the layer in the project
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.OnRequestExpLayerIndex(Sender: TObject; const Layername: WideString; var LayerIndex: Integer);
var i: integer;
begin
   // the Layerindex of the Layername has to be evaluated
  for i := 0 to Document.Layers.Count - 1 do
  begin
    if UpperCase(Document.Layers.Items[i].Layername) = UpperCase(Layername) then
    begin
      LayerIndex := Document.Layers.Items[i].Index;
      break;
    end;
  end;
end;

//------------------------------------------------------------------------------
// NAME : OnRequestExpProjectName
//------------------------------------------------------------------------------
// Method is used to return the name of the Project
//
// PARAMETERS: ProjectName  -> Name of the Project
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.OnRequestExpProjectName(Sender: TObject; var ProjectName: WideString);
var ProjName: string;
begin
  ProjName := Document.Name;
  ProjectName := ProjName;
end;

//------------------------------------------------------------------------------
// NAME : OnRequestImpLayerIndex
//------------------------------------------------------------------------------
// Method is used to get the index of a layer by its name
//
// PARAMETERS: Layername  -> Name of the layer
//             LayerIndex -> Index of the layer in the project
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.OnRequestImpLayerIndex(Sender: TObject; const Layername: WideString; var LayerIndex: Integer);
var i: integer;
begin
   // the Layerindex of the Layername has to be evaluated
  for i := 0 to Document.Layers.Count - 1 do
  begin
    if UpperCase(Document.Layers.Items[i].Layername) = UpperCase(Layername) then
    begin
      LayerIndex := Document.Layers.Items[i].Index;
      break;
    end;
  end;
end;

//------------------------------------------------------------------------------
// NAME : OnRequestImpProjectName
//------------------------------------------------------------------------------
// Method is used to return the name of the Project
//
// PARAMETERS: ProjectName  -> Name of the Project
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.OnRequestImpProjectName(Sender: TObject; var ProjectName: WideString);
var ProjName: string;
begin
  ProjName := Document.Name;
  ProjectName := ProjName;
end;

//------------------------------------------------------------------------------
// NAME : GetNumObjectsOfLayer
//------------------------------------------------------------------------------
// Method is used to return the number of objects on a layer
//
// PARAMETERS: Layername  -> Name of the Layer
//
// RETURNVALUE: Number of objects
//------------------------------------------------------------------------------

function TAxGisCombine.GetNumObjectsOfLayer(Layername: string): integer;
var retVal: integer;
  i: integer;
begin
  retVal := 0;
  for i := 0 to Document.Layers.Count - 1 do
  begin
    if Document.Layers.Items[i].Layername = Layername then
    begin
      retVal := Document.Layers.Items[i].Count;
      break;
    end;
  end;
  result := retVal;
end;

//------------------------------------------------------------------------------
// NAME : OnRequestImpProjectGUIDs
//------------------------------------------------------------------------------
// Method is used to get the GUID´s for the IDB database
//
// PARAMETERS: StartGUID   -> Start GUID
//             CurrentGUID -> Current GUID
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.OnRequestImpProjectGUIDs(Sender: TObject; var StartGUID, CurrentGUID: WideString);
var aStartGUID, aCurrentGUID: string;
begin
  aStartGUID := Document.StartProjectGUID;
  aCurrentGUID := Document.CurrentProjectGUID;
  StartGUID := aStartGUID;
  CurrentGUID := aCurrentGUID;
end;

//------------------------------------------------------------------------------
// NAME : CombineCPolysByDatabaseAttrib
//------------------------------------------------------------------------------
// Methid is used to combine Polygons in dependence of the database attributes
//
// PARAMETERS : SourceLayer     : Layer that should be used for combining
//              TargetLayer     : Name of layer where combine result should be created
//              DatabaseColumns : List of Database columns that should be used
//              DataSource      : Data Source of the objects (Layers/Selected)
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.CombineCPolysByDatabaseAttrib(const SourceLayer,
  TargetLayer: WideString; const DatabaseColumns: AxCmbStringList;
  DataSource: AxCmbDataSource);
var ColName: widestring;
  ColInfo: widestring;
  ColType: TOleEnum;
  ColLength: integer;
  TmpLayerRefs: TRefLayerList;
  TmpLayerRef: TLayerRef;
  TmpAttribRefs: TDbAttribs;
  i, j, k: integer;
  aLayer: ILayer;
  Elems2Work, ElemsWorked: integer;
  aObject: OleVariant;
  aIndex: integer;
  PostFixIdx: integer;
  MessageString: string;
  DuplId: integer;
  NeedToCheckForDuplicates: boolean;
begin
  NeedToCheckForDuplicates := false;
  if not CheckLayerExists(SourceLayer) then
    exit;
  if not CheckLayerExists(TargetLayer) then
    CreateLayer(TargetLayer)
  else
  begin
    aLayer := Document.Layers.GetLayerByName(TargetLayer);
    if aLayer.Count > 0 then
      NeedToCheckForDuplicates := true;
  end;

   // initiate the StringFile class
  Create_StringFileInstance;

  if DataSource = ds_Selected then
    Create_SelectedStack;

  Create_DatabaseInstance;
  Create_ManipulationInstance;

   // copy the database structure from the SourceLayer to the TargetLayer
  AxImpExpDbc.CheckAndCreateExpTable(SourceLayer);
  AxImpExpDbc.CheckAndCreateImpTable(TargetLayer);
  AxImpExpDbc.AddImpIdColumn(TargetLayer);
  AxImpExpDbc.AddImpColumn(TargetLayer, 'AREA', Col_Float, 0); // add the area column

   // read areas from the Sourcelayer
  FEvents.OnDisplayPass(StrFileObj.Assign(1)); // Read Polygon Information....

   // first read out the database structure of the original table and
   // create new table in dependence of the columns that should be processed
  while AxImpExpDbc.GetExportColumn(SourceLayer, ColName, colType, ColLength) do
  begin
    if DatabaseColumns.IsIn(ColName) >= 0 then
      AxImpExpDbc.AddImpColumn(TargetLayer, ColName, colType, ColLength);
  end;
  AxImpExpDbc.CreateImpTable(TargetLayer);
  AxImpExpDbc.OpenImpTable(TargetLayer);
  AxImpExpDbc.OpenExpTable(SourceLayer);

  TmpLayerRefs := TRefLayerList.Create;
  TmpAttribRefs := TDbAttribs.Create;

  PostFixIdx := 0;

  if Datasource = ds_Layers then
  begin
    aLayer := Document.Layers.GetLayerByName(SourceLayer);
    Elems2Work := aLayer.Count; ElemsWorked := 0;
    for i := 0 to ALayer.count - 1 do
    begin
      ElemsWorked := ElemsWorked + 1;
      ShowPercent(Elems2Work, ElemsWorked);

      aObject := ALayer.Items[i];
      if aObject.ObjectType <> ot_CPoly then
        continue;

         // now read out the database information
         // and get the layer to which the object has to be copied
      aIndex := aObject.Index;
      if AxImpExpDbc.DoExportData(SourceLayer, aIndex) then
      begin
        TmpAttribRefs.Clear;
        while AxImpExpDbc.GetExportData(SourceLayer, ColName, ColInfo) do
        begin
          if DatabaseColumns.IsIn(ColName) >= 0 then
            TmpAttribRefs.Add(ColName, ColInfo);
        end;
            // now get the generic layername
        TmpLayerRef := TmpLayerRefs.Find(TmpAttribRefs);
        if TmpLayerRef = nil then
        begin
               // a new layer reference has to be created
          TmpLayerRef := TLayerRef.Create;
          TmpLayerRef.Name := 'TMP_COMBINE_LAYER_' + inttostr(PostFixIdx);
          PostFixIdx := PostFixIdx + 1;
          TmpLayerRefs.Add(TmpLayerRef);
          TmpLayerRef.CreateAttribList;
          TmpLayerRef.Attributes.Assign(TmpAttribRefs);
        end;
            // now copy to the corresponding layer
        AxObjectMan.CopyObjectToLayer(aObject, TmpLayerRef.Name);
      end;
    end;
  end
  else
  begin
      // iterate through the selected stack and process the objects
    Elems2Work := Selected_StackSize - 1; ElemsWorked := 0;
    aLayer := nil;
    for i := 0 to Selected_StackSize - 1 do
    begin
      ElemsWorked := ElemsWorked + 1;
      ShowPercent(Elems2Work, ElemsWorked);
      aIndex := Selected_Stack[i];
      if aIndex < 0 then
      begin
            // get layer index
        aIndex := (aIndex + ot_Layer) * -1;
        aLayer := Document.Layers.GetLayerByName(Selected_StackLayernames[aIndex]);
        if aLayer.Layername <> SourceLayer then
        begin
               // ignore layer
          aLayer := nil;
          continue;
        end;
      end
      else if aLayer <> nil then
      begin
        aObject := Document.GetObjectByIndex(aIndex);
        if aObject.ObjectType <> ot_CPoly then
          continue;
        if AxImpExpDbc.DoExportData(SourceLayer, aIndex) then
        begin
          TmpAttribRefs.Clear;
          while AxImpExpDbc.GetExportData(SourceLayer, ColName, ColInfo) do
          begin
            if DatabaseColumns.IsIn(ColName) >= 0 then
              TmpAttribRefs.Add(ColName, ColInfo);
          end;
               // now get the generic layername
          TmpLayerRef := TmpLayerRefs.Find(TmpAttribRefs);
          if TmpLayerRef = nil then
          begin
                  // a new layer reference has to be created
            TmpLayerRef := TLayerRef.Create;
            TmpLayerRef.Name := 'TMP_COMBINE_LAYER_' + inttostr(PostFixIdx);
            PostFixIdx := PostFixIdx + 1;
            TmpLayerRefs.Add(TmpLayerRef);
            TmpLayerRef.CreateAttribList;
            TmpLayerRef.Attributes.Assign(TmpAttribRefs);
          end;
               // now copy to the corresponding layer
          AxObjectMan.CopyObjectToLayer(aObject, TmpLayerRef.Name);
        end;
      end;
    end;
  end;

   // now combine the polygons on the temporary layers
  for i := 0 to TmpLayerRefs.Count - 1 do
  begin
    TmpLayerRef := TmpLayerRefs.Items[i];
    AxObjectMan.CombineCPolysOnLayer(TmpLayerRef.Name, 'RES_' + TmpLayerRef.Name, FALSE);
      // layer can be removed now from project
    RemoveLayerFromProject(TmpLayerRef.Name);
  end;

   // now the combined polygons have to be copied to the target layer
   // and database attributes have to be added
  for i := 0 to TmpLayerRefs.Count - 1 do
  begin
    TmpLayerRef := TmpLayerRefs.Items[i];
    if not CheckLayerExists('RES_' + TmpLayerRef.Name) then
      continue;
    MessageString := StrfileObj.Assign(8);
      // create the message string
    for j := 0 to TmpLayerRef.Attributes.Count - 1 do
      MessageString := MessageString + TmpLayerRef.Attributes.Items[j].ColName + '=' + MessageString + TmpLayerRef.Attributes.Items[j].ColInfo + ';';
    FEvents.OnDisplayPass(MessageString); // Copy CPolys to Target layer for attributes:

    aLayer := Document.Layers.GetLayerByName('RES_' + TmpLayerRef.Name);
    Elems2Work := aLayer.Count - 1; ElemsWorked := 0;
    for j := 0 to aLayer.Count - 1 do
    begin
      ElemsWorked := ElemsWorked + 1;
      ShowPercent(Elems2Work, ElemsWorked);
      aObject := aLayer.Items[j];
      aIndex := aObject.Index;
      if (DataSource = ds_Selected) or NeedToCheckForDuplicates then
      begin
        DuplId := AxObjectMan.CheckIdHasDuplicateOnLayer(aIndex, TargetLayer);
        if DuplId > 0 then
        begin
               // object already exists -> do not copy
          continue;
        end;
        DuplId := AxObjectMan.GetIdOfSurroundingCPolyOnLayer(aIndex, TargetLayer);
        if DuplId > 0 then
        begin
               // there is already surrounding polygon -> do not copy
          continue;
        end;
      end;
         // copy object to target layer
      aIndex := AxObjectMan.CopyObjectToLayer(aObject, TargetLayer);
      AxImpExpDbc.AddImpIdInfo(TargetLayer, aIndex);

      AxImpExpDbc.AddImpInfo(TargetLayer, 'AREA', floattostr(aObject.Area));
      for k := 0 to TmpLayerRef.Attributes.Count - 1 do
        AxImpExpDbc.AddImpInfo(TargetLayer, TmpLayerRef.Attributes.Items[k].ColName, TmpLayerRef.Attributes.Items[k].ColInfo);
    end;
      // remove RES Layer from project
    RemoveLayerFromProject('RES_' + TmpLayerRef.Name);
  end;

  TmpLayerRefs.Destroy;
  TmpAttribRefs.Destroy;

  Destroy_ManipulationInstance;
  Destroy_DatabaseInstance;

  if Datasource = ds_Selected then
    Destroy_SelectedStack;
  Destroy_StringFileInstance;
end;

//------------------------------------------------------------------------------
// NAME : CombineCPolyRefsWithTextRefs
//------------------------------------------------------------------------------
// Method is used to combine references to polygons with texts
//
// PARAMETERS:  CPolys    -> Polygons that should be combined
//              Texts     -> Texts that should be combined
//              Setup     -> Setup for combine
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.CombineCPolyRefsWithTextRefs(var CPolys: TObjectRefKeeper; var Texts: TObjectRefKeeper; const Setup: AxCmbTextsWithCPolysSetup; DataSource: AxCmbDataSource);
var i: integer;
  new_id: integer;

  CombFile: TextFile;
  ErrorFile: TextFile;
  Line: string;

  Elems2Work: integer;
  ElemsWorked: integer;
  PolyIdx: integer;

  PolyRef: TCPolyRef;
  aCPoly: OleVariant;
  ErrorCPoly: OleVariant;
  ErrorPolyRef: TCPolyRef;

  TextRef: TTextRef;
  aText: OleVariant;
  myText: IText;
  aBase: IBase;
  TextIdx: integer;

  LayerRef: TLayerRef;

  duplId: integer;
begin
   // create a file that contains the combining information if necessary
  if Setup.ErrorResultFilename <> '' then
  begin
    AssignFile(ErrorFile, Setup.ErrorResultFilename);
    Rewrite(ErrorFile);
  end;
  if Setup.CombineResultFilename <> '' then
  begin
    AssignFile(CombFile, Setup.CombineResultFilename);
    Rewrite(CombFile);
  end;

  FEvents.OnDisplayPass(StrfileObj.Assign(10)); // combine polygons with texts...
  Elems2Work := CPolys.Objects.Count; ElemsWorked := 0;

   // create object for object manipulation
  Create_ManipulationInstance;

  for PolyIdx := 0 to CPolys.Objects.Count - 1 do
  begin
    PolyRef := CPolys.Objects.Items[PolyIdx] as TCPolyRef;
      // get object itself
    aCPoly := Document.GetObjectByIndex(PolyRef.ProgisId);
    for TextIdx := 0 to Texts.Objects.Count - 1 do
    begin
      TextRef := Texts.Objects.Items[TextIdx] as TTextRef;

      if (not Setup.CheckMultiAssignedTexts) and (TextRef.LinkedObjects <> nil) then
        continue; // Text is already assigned -> no need to check anymore

      if AxObjectMan.CheckPointInsidePoly(TextRef.X, TextRef.Y, aCPoly, FALSE) then
      begin
            // check if Symbol is not already combined with a Poly
        if TextRef.LinkedObjects = nil then
        begin
          TextRef.CreateLinkedObjectList;
          TextRef.LinkedObjects.Add(PolyRef);
          if PolyRef.LinkedObjects = nil then
            PolyRef.CreateLinkedObjectList;
          PolyRef.LinkedObjects.Add(TextRef);

          if (Setup.ApplyTextDatabaseInfoToCPoly) or (Setup.TextDatabaseColumn <> '') then
          begin
                  // write item to database
            LayerRef := PolyRef.Parent as TLayerRef;
            AxImpExpDbc.AddImpIdInfo(LayerRef.Name, PolyRef.ProgisId);
            AxImpExpDbc.AddImpInfo(LayerRef.Name, 'AREA', floattostr(PolyRef.Area));
            if TextRef.Attributes <> nil then
            begin
              for i := 0 to TextRef.Attributes.Count - 1 do
                AxImpExpDbc.AddImpInfo(LayerRef.Name, TextRef.Attributes.Items[i].ColName, TextRef.Attributes.Items[i].ColInfo);
            end;
                  // add text to database
            if Setup.TextDatabaseColumn <> '' then
              AxImpExpDbc.AddImpInfo(LayerRef.Name, Setup.TextDatabaseColumn, TextRef.Text);
          end;

          if Setup.CombineResultFilename <> '' then
          begin
            Line := inttostr(PolyRef.ProgisId) + ' ' + inttostr(TextRef.ProgisId);
            writeln(CombFile, Line);
          end;

          if Setup.LinkTextToCPoly then
          begin
                  // link text to polygon
            aBase := Document.GetObjectByIndex(TextRef.ProgisId);
            if aBase <> nil then
            begin
              myText := aBase as IText;
              myText.AttachToObject(PolyRef.ProgisId);
            end;
          end;
          if Setup.RemoveCombinedTexts then
            Document.DeleteObjectByIndex(TextRef.ProgisId);

          if not Setup.CheckMultiAssignedTexts then
            break; // due to there is already a text assigned to polygon
        end
        else
        begin
          if PolyRef.LinkedObjects = nil then
            PolyRef.CreateLinkedObjectList;
          PolyRef.LinkedObjects.Add(TextRef);
          TextRef.LinkedObjects.Add(PolyRef);

               // write error-information to File
          if Setup.ErrorResultFilename <> '' then
          begin
            Line := inttostr(PolyRef.ProgisId);
            writeln(ErrorFile, Line);
            Line := inttostr(TextRef.ProgisId);
            writeln(ErrorFile, Line);
          end;
          if Setup.CombineResultFilename <> '' then
          begin
            Line := inttostr(PolyRef.ProgisId) + ' ' + inttostr(TextRef.ProgisId);
            Writeln(CombFile, Line);
          end;

          if (Setup.ApplyTextDatabaseInfoToCPoly) or (Setup.TextDatabaseColumn <> '') then
          begin
                  // write item to database
            LayerRef := PolyRef.Parent as TLayerRef;
            AxImpExpDbc.AddImpIdInfo(LayerRef.Name, PolyRef.ProgisId);
            AxImpExpDbc.AddImpInfo(LayerRef.Name, 'AREA', floattostr(PolyRef.Area));
            if Setup.TextDatabaseColumn <> '' then
              AxImpExpDbc.AddImpInfo(LayerRef.Name, Setup.TextDatabaseColumn, TextRef.Text);

            if TextRef.Attributes <> nil then
            begin
              for i := 0 to TextRef.Attributes.Count - 1 do
                AxImpExpDbc.AddImpInfo(LayerRef.Name, TextRef.Attributes.Items[i].ColName, TextRef.Attributes.Items[i].ColInfo);
            end;
          end;

          if Setup.CPolyErrorLayer <> '' then
          begin
            ErrorPolyRef := TextRef.LinkedObjects.Items[TextRef.LinkedObjects.Count - 1] as TCPolyRef;
                  // copy current Poly to CPolyErrorLayer
            duplId := 0;
            new_id := AxObjectMan.CopyObjectToLayer(aCPoly, Setup.CPolyErrorLayer);
            if Datasource = ds_Selected then
            begin
              duplId := AxObjectMan.CheckIdHasDuplicateOnLayer(new_id, Setup.CPolyErrorLayer);
              if duplId > 0 then
                Document.DeleteObjectByIndex(new_id);
            end;

                  // copy Poly that is already linked to this Symbol to CPolyErrorLayer
            ErrorCPoly := Document.GetObjectByIndex(ErrorPolyRef.Progisid);
            duplId := 0;
            new_id := AxObjectMan.CopyObjectToLayer(ErrorCPoly, Setup.CPolyErrorLayer);
            if Datasource = ds_Selected then
            begin
              duplId := AxObjectMan.CheckIdHasDuplicateOnLayer(new_id, Setup.CPolyErrorLayer);
              if duplId > 0 then
                Document.DeleteObjectByIndex(new_id);
            end;
          end;

          if Setup.LinkTextToCPoly then
          begin
                  // link text to polygon
            aBase := Document.GetObjectByIndex(TextRef.ProgisId);
            if aBase <> nil then
            begin
              myText := aBase as IText;
              myText.AttachToObject(PolyRef.ProgisId);
            end;
          end;

          if Setup.RemoveCombinedTexts then
            Document.DeleteObjectByIndex(TextRef.ProgisId);
        end;
      end
      else if (PolyIdx = CPolys.Objects.Count - 1) and (TextRef.LinkedObjects = nil) then
      begin
            // we just have checked with last polygon and still could not assign
        if Setup.TextErrorLayer <> '' then
        begin
               // copy to symbol error layer
          aText := Document.GetObjectByIndex(TextRef.ProgisId);
          duplId := 0;
          new_id := AxObjectMan.CopyObjectToLayer(aText, Setup.TextErrorLayer);
          if Datasource = ds_Selected then
          begin
            duplId := AxObjectMan.CheckIdHasDuplicateOnLayer(new_id, Setup.TextErrorLayer);
            if duplId > 0 then
              Document.DeleteObjectByIndex(new_id);
          end;
        end;
        if Setup.ErrorResultFilename <> '' then
        begin
          Line := inttostr(TextRef.ProgisId);
          writeln(ErrorFile, Line);
        end;
      end;
    end;

      // if polygon could not be assigned to symbols
    if (PolyRef.LinkedObjects = nil) and (Setup.CPolyErrorLayer <> '') then
    begin
         // copy polygon to error layer
      duplId := 0;
      new_id := AxObjectMan.CopyObjectToLayer(aCPoly, Setup.CPolyErrorLayer);
      if Datasource = ds_Selected then
      begin
        duplId := AxObjectMan.CheckIdHasDuplicateOnLayer(new_id, Setup.CPolyErrorLayer);
        if duplId > 0 then
          Document.DeleteObjectByIndex(new_id);
      end;
      if Setup.ErrorResultFilename <> '' then
      begin
        Line := inttostr(PolyRef.ProgisId);
        writeln(ErrorFile, Line);
      end;
    end;
    ElemsWorked := ElemsWorked + 1;
    ShowPercent(Elems2Work, ElemsWorked);
  end;

  if Setup.CombineResultFilename <> '' then
    CloseFile(CombFile);

  if Setup.ErrorResultFilename <> '' then
    CloseFile(ErrorFile);

  Destroy_ManipulationInstance;
end;

//------------------------------------------------------------------------------
// NAME : CombineCPolysWithTexts
//------------------------------------------------------------------------------
// Methid is used to combine Polygons with Texts
//
// PARAMETERS : CPolyList   -> List of Polygon Layers
//              TextList    -> List of Text Layers
//              Setup       -> Setup for combining
//              DataSource  -> Data Source of the objects (Layers/Selected)
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.CombineCPolysWithTexts(const CPolyList,
  TextList: AxCmbStringList; const Setup: AxCmbTextsWithCPolysSetup;
  PolygonDataSource, TextDataSource: AxCmbDataSource);
var CPolyObjKeeper: TObjectRefKeeper;
  TextObjKeeper: TObjectRefKeeper;
  Filter: TObjectRefFilter;
  i, j: integer;
  DbColumns: TDbColumns;
  LayerRef: TLayerRef;
begin
   // initiate the StringFile class
  Create_StringFileInstance;

  if (PolygonDataSource = ds_Selected) or (TextDataSource = ds_Selected) then
    Create_SelectedStack;

   // create filter for reading
  Filter := TObjectRefFilter.Create;

   // read polygon information
  FEvents.OnDisplayPass(StrfileObj.Assign(1)); // Read polygon information...
  CPolyObjKeeper := TObjectRefKeeper.Create;
   // prepare filter for reading
  Filter.AddObjectTypeToFilter(Filter_CPoly);
  for i := 0 to CPolyList.Count - 1 do
    Filter.AddSupportedLayer(CPolyList.Items[i]);
  Filter.ReadAttribs := false; // Polygon information has not to be read
  FillObjectKeeper(Filter, CPolyObjKeeper, PolygonDataSource);

   // now read the Texts from the Text-Layers
  FEvents.OnDisplayPass(StrfileObj.Assign(9)); // Read text information...
  TextObjKeeper := TObjectRefKeeper.Create;
   // prepare filter for reading
  Filter.Clear;
  Filter.AddObjectTypeToFilter(Filter_Text);
  for i := 0 to TextList.Count - 1 do
    Filter.AddSupportedLayer(TextList.Items[i]);

  if FDbMode = 0 then
  begin
    Setup.ApplyTextDatabaseInfoToCPoly := false;
    Setup.TextDatabaseColumn := '';
  end;
  if Setup.ApplyTextDatabaseInfoToCPoly or (Setup.TextDatabaseColumn <> '') then
  begin
    DbColumns := TDbColumns.Create;
    if Setup.ApplyTextDatabaseInfoToCPoly then
      Filter.ReadAttribs := true
    else
      Filter.ReadAttribs := false;
    Create_DatabaseInstance;
  end
  else
  begin
    Dbcolumns := nil;
    Filter.ReadAttribs := false;
  end;
  FillObjectKeeper(Filter, TextObjKeeper, TextDataSource);

   // prepare database columns for result
  if DbColumns <> nil then
  begin
    for i := 0 to TextObjKeeper.Layers.Count - 1 do
    begin
      LayerRef := TextObjKeeper.Layers.Items[i];
      if LayerRef.DbColumns = nil then continue;
      for j := 0 to LayerRef.DbColumns.Count - 1 do
        DbColumns.Add(LayerRef.DbColumns.Items[j].ColName, LayerRef.DbColumns.Items[j].ColType, LayerRef.DbColumns.Items[j].ColLength);
    end;
  end;

  if Setup.CPolyErrorLayer <> '' then
    CreateLayer(Setup.CPolyErrorLayer);
  if Setup.TextErrorLayer <> '' then
    CreateLayer(Setup.TextErrorLayer);

  if (Setup.ApplyTextDatabaseInfoToCPoly) or (Setup.TextDatabaseColumn <> '') then
  begin
    for i := 0 to CPolyList.Count - 1 do
    begin
      AxImpExpDbc.CheckAndCreateImpTable(CPolyList.Items[i]);
      AxImpExpDbc.AddImpIdColumn(CPolyList.Items[i]);
      if Setup.TextDatabaseColumn <> '' then
        AxImpExpDbc.AddImpColumn(CPolyList.Items[i], Setup.TextDatabaseColumn, col_String, 255);
      for j := 0 to DbColumns.Count - 1 do
        AxImpExpDbc.AddImpColumn(CPolyList.Items[i], DbColumns.Items[j].ColName, DbColumns.Items[j].ColType, DbColumns.Items[j].ColLength);
      AxImpExpDbc.CreateImpTable(CPolyList.Items[i]);
      AxImpExpDbc.OpenImpTable(CPolyList.Items[i]);
    end;
  end;

   // now combine polygons with symbols
  if (PolygonDataSource = ds_Selected) or (TextDataSource = ds_Selected) then
    CombineCPolyRefsWithTextRefs(CPolyObjKeeper, TextObjKeeper, Setup, ds_Selected)
  else
    CombineCPolyRefsWithTextRefs(CPolyObjKeeper, TextObjKeeper, Setup, ds_Layers);

   // dispose objects
  if DbColumns <> nil then
    DbColumns.Destroy;
  CPolyObjKeeper.Destroy;
  TextObjKeeper.Destroy;
  Filter.Destroy;

  if (PolygonDataSource = ds_Selected) or (TextDataSource = ds_Selected) then
    Destroy_SelectedStack;
  Destroy_StringFileInstance;
  if Setup.ApplyTextDatabaseInfoToCPoly or (Setup.TextDatabaseColumn <> '') then
    Destroy_DatabaseInstance;
end;

//------------------------------------------------------------------------------
// NAME : MEDIX_Combine
//------------------------------------------------------------------------------
// Method is used to do a dkm-combining with Medix Dkm-Data
//
// PARAMETERS: Filename   -> Filename of the .ini file that contains combine
//                           Information
//             Datasource -> Source of objects (Layers/Selected)
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.MEDIX_Combine(const Filename: WideString; Datasource: AxCmbDataSource);
var inifile: TIniFile;
  dummy: string;
  TargetLayerGenerateObjects: string;
  iniitems: TStrings;
  LayersToProcess: TStrings;
  i: integer;
  count: integer;
  TargetlayerIslands: string;
  ColName: string;
  ColLength: integer;
  CPolyList: IAxCmbStringList;
  TextList: IAxCmbStringList;
  Setup: IAxCmbTextsWithCPolysSetup;
  ErrorlayerObjects, ErrorLayerTexts: string;
  TextCombineDbCol: string;
  SplitLayers: string;
  DoSplitLayers: boolean;
begin
  ProcessingMode := pro_DKM_MEDIX;
  inifile := TIniFile.Create(Filename);

   // if complete layer has to be processed -> remove old results
  if (DataSource <> ds_Selected) then
  begin
    Create_DataBaseInstance;
    dummy := inifile.ReadString('MEDIXPARZELLENSETTINGS', 'TargetlayerGenerateObjects', '');
    RemoveLayerFromProject(dummy);

    dummy := inifile.ReadString('MEDIXPARZELLENSETTINGS', 'TargetlayerIslands', '');
    if CheckLayerExists(dummy) then
    begin
      AxImpExpDbc.CheckandCreateExpTable(dummy);
      AxImpExpDbc.EmptyExpTable(dummy); // empty table if it exists
      EmptyLayerInProject(dummy); // empty layer if it exists
    end;
      // check if the error layers exist
    dummy := inifile.ReadString('MEDIXPARZELLENSETTINGS', 'ErrorlayerObjects', '');
    if CheckLayerExists(dummy) then
    begin
      AxImpExpDbc.CheckandCreateExpTable(dummy);
      AxImpExpDbc.EmptyExpTable(dummy); // empty table if it exists
      EmptyLayerInProject(dummy); // empty layer if it exists
    end;
    dummy := inifile.ReadString('MEDIXPARZELLENSETTINGS', 'ErrorlayerTexts', '');
    if CheckLayerExists(dummy) then
    begin
      AxImpExpDbc.CheckandCreateExpTable(dummy);
      AxImpExpDbc.EmptyExpTable(dummy); // empty table if it exists
      EmptyLayerInProject(dummy); // empty layer if it exists
    end;
    Destroy_DatabaseInstance;
  end
  else
    Create_SelectedStack;

  Create_StringFileInstance;
  FEvents.OnDisplayPass('---------------------------------------------------');
  FEvents.OnDisplayPass(StrFileObj.Assign(200)); // Execute MEDIX DKM-Post Processing

  dummy := inifile.ReadString('MEDIXPARZELLENSETTINGS', 'DOSPLITLAYERS', 'FALSE');
  if dummy = 'TRUE' then DoSplitLayers := true else DoSplitLayers := false;

  TargetLayerGenerateObjects := inifile.ReadString('MEDIXPARZELLENSETTINGS', 'TargetlayerGenerateObjects', '');

  if DoSplitLayers then
  begin
    SplitLayers := '';
    iniitems := TStringList.Create;
    inifile.ReadSection('MEDIXPARZELLENGENLAYERS', iniitems);
    for i := 0 to iniitems.Count - 1 do
    begin
      if DataSource = ds_Selected then
      begin
            // copy objects that should be splitted to TMP_TargetLayerGenerateObjects
        SplitLayers := inifile.ReadString('MEDIXPARZELLENGENLAYERS', iniitems[i], '');
        CopyNonDuplicatesToLayer(SplitLayers, 'TMP_' + TargetLayerGenerateObjects, false {no database}, ds_Selected);
      end
      else
      begin
        if SplitLayers <> '' then
          SplitLayers := SplitLayers + #13;
        SplitLayers := SplitLayers + inifile.ReadString('MEDIXPARZELLENGENLAYERS', iniitems[i], '');
      end;
    end;
    iniitems.Destroy;
    Create_ManipulationInstance;
    if DataSource = ds_Selected then
    begin
      AxObjectman.SplitObjectsToLayer('TMP_' + TargetLayerGenerateObjects, 'SPLIT_' + TargetLayerGenerateObjects, smSplitToSingleLines + smIgnoreDuplicates, FALSE);
      RemoveLayerFromProject('TMP_' + TargetLayerGenerateObjects); // temporary layer can be removed now
    end
    else
      AxObjectman.SplitObjectsToLayer(SplitLayers, 'SPLIT_' + TargetLayerGenerateObjects, smSplitToSingleLines + smIgnoreDuplicates, FALSE);
    Destroy_ManipulationInstance;
  end;

   // get the layers that are used for generation of objects
  LayersToProcess := TStringList.Create;
  if DoSplitLayers then
    LayersToProcess.Add('SPLIT_' + TargetLayerGenerateObjects)
  else
  begin
    iniitems := TStringList.Create;
    inifile.ReadSection('MEDIXPARZELLENGENLAYERS', iniitems);
    for i := 0 to iniitems.Count - 1 do
      LayersToProcess.Add(inifile.ReadString('MEDIXPARZELLENGENLAYERS', iniitems[i], ''));
    iniitems.Destroy;
  end;

   // get the fetch radius
  dummy := inifile.ReadString('MEDIXPARZELLENSETTINGS', 'Fetchradius', '');
  val(dummy, FetchRadius, count); if count <> 0 then FetchRadius := 0.01;

   // generate the objects
  if DataSource = ds_Selected then
  begin
    if DoSplitLayers then
      GenerateObjects(LayersToProcess, 'TMP_' + TargetLayerGenerateObjects, ds_Layers) // use Split Layer
    else
      GenerateObjects(LayersToProcess, 'TMP_' + TargetLayerGenerateObjects, ds_Selected); // use original selected data
  end
  else
    GenerateObjects(LayersToProcess, TargetLayerGenerateObjects, ds_Layers);
  LayersToProcess.Destroy;

   // remove the Split-Layer
  RemoveLayerFromProject('SPLIT_' + TargetLayerGenerateObjects);

  if DataSource = ds_Selected then
    TargetLayerGenerateObjects := 'TMP_' + TargetLayerGenerateObjects;

   // now the island calculation should be done
  TargetlayerIslands := inifile.ReadString('MEDIXPARZELLENSETTINGS', 'TargetlayerIslands', '');
  if DataSource = ds_Selected then
    TargetLayerIslands := 'TMP_' + TargetLayerIslands;

  Create_ManipulationInstance;
  AxObjectMan.CreateIslandLayer(TargetLayerGenerateObjects, TargetlayerIslands);
  Destroy_ManipulationInstance;

   // in the next step the texts will be combined with the polygons
   // create a dummy database entry for the TargetLayername to get the database Structure
  iniitems := TStringList.Create;
  inifile.ReadSection('DBCOLLENGTH', iniitems);
  Create_DatabaseInstance;
  AxImpExpDbc.CheckAndCreateImpTable(TargetlayerIslands);
  AxImpExpDbc.AddImpIdColumn(TargetlayerIslands);
  for i := 0 to iniitems.Count - 1 do
  begin
    dummy := inifile.ReadString('DBCOLLENGTH', iniitems[i], '');
    ColName := iniitems[i];
    val(dummy, ColLength, count);

    AxImpExpDbc.AddImpColumn(TargetlayerIslands, Colname, col_String, ColLength);
  end;
  AxImpExpDbc.CreateImpTable(TargetlayerIslands);
  AxImpExpDbc.OpenImpTable(TargetlayerIslands);
  AxImpExpDbc.AddImpIdInfo(TargetlayerIslands, 999);
  Destroy_DatabaseInstance;

   // create polygon layer list
  CPolyList := CreateStringList;
  CPolyList.Add(TargetlayerIslands);

  TextList := CreateStringList;
  inifile.ReadSection('MEDIXTEXTLAYERS', iniitems);
  for i := 0 to iniitems.Count - 1 do
  begin
    dummy := inifile.ReadString('MEDIXTEXTLAYERS', iniitems[i], '');
    TextList.Add(dummy);
  end;
  iniitems.Destroy;

   // create setup for combining texts with polygons
  Setup := CreateCombineTextsWithCPolysSetup;
  ErrorlayerObjects := inifile.ReadString('MEDIXPARZELLENSETTINGS', 'ErrorlayerObjects', '');
  ErrorlayerTexts := inifile.ReadString('MEDIXPARZELLENSETTINGS', 'ErrorlayerTexts', '');
  Setup.CPolyErrorLayer := ErrorLayerObjects;
  Setup.TextErrorLayer := ErrorLayerTexts;
  TextCombineDbCol := inifile.ReadString('MEDIXPARZELLENSETTINGS', 'TextCombineDbCol', '');
  Setup.TextDatabaseColumn := TextCombineDbCol;
  Setup.LinkTextToCPoly := true;
  Setup.ApplyTextDatabaseInfoToCPoly := false;

  CombineCPolysWithTexts(CPolyList, TextList, Setup, ds_Layers, DataSource);

   // now the dummy database entry can be deleted
  Create_DataBaseInstance;
  AxImpExpDbc.CheckAndCreateExpTable(TargetlayerIslands);
  AxImpExpDbc.OpenExpTable(TargetlayerIslands);
  AxImpExpDbc.DeleteExpDBEntry(TargetlayerIslands, 999);
  Destroy_DatabaseInstance;

  if Datasource = ds_Selected then
  begin
      // copy result from temporary layer to original layer
    TargetlayerIslands := inifile.ReadString('MEDIXPARZELLENSETTINGS', 'TargetlayerIslands', '');
    CopyNonDuplicatesToLayer('TMP_' + TargetLayerIslands, TargetLayerIslands, TRUE {use database}, ds_Layers);

    TargetLayerGenerateObjects := inifile.ReadString('MEDIXPARZELLENSETTINGS', 'TargetlayerGenerateObjects', '');
    CopyNonDuplicatesToLayer('TMP_' + TargetLayerGenerateObjects, TargetLayerGenerateObjects, TRUE {use database}, ds_Layers);

      // delete temporary layers for generate objects and targetlayer islands
    Create_DatabaseInstance;
    AxImpExpDbc.CheckAndCreateExpTable('TMP_' + TargetlayerIslands); AxImpExpDbc.DeleteExpTable('TMP_' + TargetlayerIslands);
    Destroy_DatabaseInstance;
    Create_DatabaseInstance;
    AxImpExpDbc.CheckAndCreateExpTable('TMP_' + TargetLayerGenerateObjects); AxImpExpDbc.DeleteExpTable('TMP_' + TargetLayerGenerateObjects);
    Destroy_DatabaseInstance;
    RemoveLayerFromProject('TMP_' + TargetlayerIslands);
    RemoveLayerFromProject('TMP_' + TargetLayerGenerateObjects);
  end;

   // delete the error layers if no errors occured
  if (GetNumObjectsOfLayer(ErrorlayerObjects) = 0) and (CheckLayerExists(ErrorlayerObjects)) then
    RemoveLayerFromProject(ErrorlayerObjects);
  if (GetNumObjectsOfLayer(ErrorLayerTexts) = 0) and (CheckLayerExists(ErrorLayerTexts)) then
    RemoveLayerFromProject(ErrorLayerTexts);

   // process layers that should be merged
  dummy := inifile.ReadString('MEDIXPARZELLENSETTINGS', 'ProcessMergeLayers', '');
  if dummy = 'TRUE' then
    DKM_Process_LayersToMerge(Filename, 'MEDIX_MERGELAYERS');

   // process layers that should be deleted
  dummy := inifile.ReadString('MEDIXPARZELLENSETTINGS', 'ProcessDeleteLayers', '');
  if dummy = 'TRUE' then
    DKM_Process_LayersToDelete(Filename, 'MEDIX_LAYERSTODELETE');

  inifile.Free;

  FEvents.OnDisplayPass('---------------------------------------------------');

  if (DataSource = ds_Selected) then
    Destroy_SelectedStack;

  Destroy_StringfileInstance;
  ProcessingMode := pro_NORMAL;

  FEvents.OnDisplayPercent(100);
end;

//------------------------------------------------------------------------------
// NAME : DFK_Combine
//------------------------------------------------------------------------------
// Method is used to do a dkm-combining with Dfk-Data
//
// PARAMETERS: Filename -> Filename of the .ini file that contains combine
//                         Information
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.DFK_Combine(const Filename: WideString; DataSource: AxCmbDataSource);
var inifile: TIniFile;
  dummy: string;
  count, i: integer;
  LayersToProcess, iniitems: TStrings;
  TargetLayerGebaeude: string;
  CPolyList: IAxCmbStringList;
  TextList: IAxCmbStringList;
  Setup: IAxCmbTextsWithCPolysSetup;
  CPolyErrorGebaeude: string;
  TextErrorGebaeude: string;
  GebaeudeCombineDbCol: string;
  TargetLayerFlurstuecke: string;
  CPolyErrorFlurstuecke: string;
  TextErrorFlurstuecke: string;
  FlurstueckeCombineDbCol: string;
begin
  ProcessingMode := pro_DKM_DFK;
  inifile := TIniFile.Create(Filename);
   // if complete layer has to be processed -> remove old results
  if (DataSource <> ds_Selected) then
  begin
    Create_DataBaseInstance;

    dummy := inifile.ReadString('DFKSETTINGS', 'TargetLayerGebaeude', '');
    if CheckLayerExists(dummy) then
    begin
      AxImpExpDbc.CheckandCreateExpTable(dummy);
      AxImpExpDbc.EmptyExpTable(dummy); // empty table if it exists
      EmptyLayerInProject(dummy); // empty layer if it exists
    end;

    dummy := inifile.ReadString('DFKSETTINGS', 'CPolyErrorGebaeude', '');
    if CheckLayerExists(dummy) then
    begin
      AxImpExpDbc.CheckandCreateExpTable(dummy);
      AxImpExpDbc.EmptyExpTable(dummy); // empty table if it exists
      EmptyLayerInProject(dummy); // empty layer if it exists
    end;

    dummy := inifile.ReadString('DFKSETTINGS', 'TextErrorGebäude', '');
    if CheckLayerExists(dummy) then
    begin
      AxImpExpDbc.CheckandCreateExpTable(dummy);
      AxImpExpDbc.EmptyExpTable(dummy); // empty table if it exists
      EmptyLayerInProject(dummy); // empty layer if it exists
    end;

    dummy := inifile.ReadString('DFKSETTINGS', 'TargetLayerFlurstuecke', '');
    if CheckLayerExists(dummy) then
    begin
      AxImpExpDbc.CheckandCreateExpTable(dummy);
      AxImpExpDbc.EmptyExpTable(dummy); // empty table if it exists
      EmptyLayerInProject(dummy); // empty layer if it exists
    end;

    dummy := inifile.ReadString('DFKSETTINGS', 'CPolyErrorFlurstuecke', '');
    if CheckLayerExists(dummy) then
    begin
      AxImpExpDbc.CheckandCreateExpTable(dummy);
      AxImpExpDbc.EmptyExpTable(dummy); // empty table if it exists
      EmptyLayerInProject(dummy); // empty layer if it exists
    end;

    dummy := inifile.ReadString('DFKSETTINGS', 'TextErrorFlurstuecke', '');
    if CheckLayerExists(dummy) then
    begin
      AxImpExpDbc.CheckandCreateExpTable(dummy);
      AxImpExpDbc.EmptyExpTable(dummy); // empty table if it exists
      EmptyLayerInProject(dummy); // empty layer if it exists
    end;
    Destroy_DatabaseInstance;
  end
  else
    Create_SelectedStack;

   // get the fetch radius
  dummy := inifile.ReadString('DFKSETTINGS', 'Fetchradius', '');
  val(dummy, FetchRadius, count); if count <> 0 then FetchRadius := 1;

  Create_StringFileInstance;
  FEvents.OnDisplayPass('---------------------------------------------------');
  FEvents.OnDisplayPass(StrFileObj.Assign(300)); // Execute DFK processing

   // ------------------------ Process Gebäude information----------------------
  LayersToProcess := TStringList.Create;
  iniitems := TStringList.Create;
  inifile.ReadSection('DFKGEBAEUDEGENLAYERS', iniitems);
  for i := 0 to iniitems.Count - 1 do
  begin
    dummy := inifile.ReadString('DFKGEBAEUDEGENLAYERS', iniitems[i], '');
    LayersToProcess.Add(dummy);
  end;

  TargetLayerGebaeude := inifile.ReadString('DFKSETTINGS', 'TargetLayerGebaeude', '');

   // generate the objects to temporary layer
  GenerateObjects(LayersToProcess, 'OBJGEN_' + TargetLayerGebaeude, DataSource);
  LayersToProcess.Destroy;

   // now calculate the islands
  Create_ManipulationInstance;
  if DataSource = ds_Selected then
    AxObjectMan.CreateIslandLayer('OBJGEN_' + TargetLayerGebaeude, 'TMP_' + TargetLayerGebaeude)
  else
    AxObjectMan.CreateIslandLayer('OBJGEN_' + TargetLayerGebaeude, TargetLayerGebaeude);
  Destroy_ManipulationInstance;

   // remove the object generation layer
  RemoveLayerFromProject('OBJGEN_' + TargetLayerGebaeude);

   // create polygon layer list
  CPolyList := CreateStringList;
  if DataSource = ds_Selected then
    CPolyList.Add('TMP_' + TargetLayerGebaeude)
  else
    CPolyList.Add(TargetLayerGebaeude);

  TextList := CreateStringList;
  inifile.ReadSection('DFKGEBAEUDETEXTLAYERS', iniitems);
  for i := 0 to iniitems.Count - 1 do
  begin
    dummy := inifile.ReadString('DFKGEBAEUDETEXTLAYERS', iniitems[i], '');
    TextList.Add(dummy);
  end;
  iniitems.Destroy;

   // create setup for combining texts with polygons
  Setup := CreateCombineTextsWithCPolysSetup;

  CPolyErrorGebaeude := inifile.ReadString('DFKSETTINGS', 'CPolyErrorGebaeude', '');
  TextErrorGebaeude := inifile.ReadString('DFKSETTINGS', 'TextErrorGebaeude', '');
  GebaeudeCombineDbCol := inifile.ReadString('DFKSETTINGS', 'GebaeudeCombineDbCol', '');

  Setup.CPolyErrorLayer := CPolyErrorGebaeude;
  Setup.TextErrorLayer := TextErrorGebaeude;
  Setup.TextDatabaseColumn := GebaeudeCombineDbCol;
  Setup.LinkTextToCPoly := true;
  Setup.ApplyTextDatabaseInfoToCPoly := false;

  CombineCPolysWithTexts(CPolyList, TextList, Setup, ds_Layers, DataSource);

  if Datasource = ds_Selected then
  begin
      // copy result from temporary layer to original layer
    CopyNonDuplicatesToLayer('TMP_' + TargetLayerGebaeude, TargetLayerGebaeude, TRUE {use database}, ds_Layers);

      // delete temporary layers for generate objects and targetlayer islands
    Create_DatabaseInstance;
    AxImpExpDbc.CheckAndCreateExpTable('TMP_' + TargetLayerGebaeude); AxImpExpDbc.DeleteExpTable('TMP_' + TargetLayerGebaeude);
    Destroy_DatabaseInstance;
    RemoveLayerFromProject('TMP_' + TargetLayerGebaeude);
  end;

   // delete the error layers if no errors occured
  if (GetNumObjectsOfLayer(CPolyErrorGebaeude) = 0) and (CheckLayerExists(CPolyErrorGebaeude)) then
    RemoveLayerFromProject(CPolyErrorGebaeude);
  if (GetNumObjectsOfLayer(TextErrorGebaeude) = 0) and (CheckLayerExists(TextErrorGebaeude)) then
    RemoveLayerFromProject(TextErrorGebaeude);

   // ---------------------- Process Flurstücke information---------------------
  LayersToProcess := TStringList.Create;
  iniitems := TStringList.Create;
  inifile.ReadSection('DFKFLURSTUECKEGENLAYERS', iniitems);
  for i := 0 to iniitems.Count - 1 do
  begin
    dummy := inifile.ReadString('DFKFLURSTUECKEGENLAYERS', iniitems[i], '');
    LayersToProcess.Add(dummy);
  end;
  TargetLayerFlurstuecke := inifile.ReadString('DFKSETTINGS', 'TargetLayerFlurstuecke', '');

   // generate the objects to temporary layer
  GenerateObjects(LayersToProcess, 'OBJGEN_' + TargetLayerFlurstuecke, DataSource);
  LayersToProcess.Destroy;

   // now calculate the islands
  Create_ManipulationInstance;
  if DataSource = ds_Selected then
    AxObjectMan.CreateIslandLayer('OBJGEN_' + TargetLayerFlurstuecke, 'TMP_' + TargetLayerFlurstuecke)
  else
    AxObjectMan.CreateIslandLayer('OBJGEN_' + TargetLayerFlurstuecke, TargetLayerFlurstuecke);
  Destroy_ManipulationInstance;

   // remove the object generation layer
  RemoveLayerFromProject('OBJGEN_' + TargetLayerFlurstuecke);

      // create polygon layer list
  CPolyList := CreateStringList;
  if DataSource = ds_Selected then
    CPolyList.Add('TMP_' + TargetLayerFlurstuecke)
  else
    CPolyList.Add(TargetLayerFlurstuecke);

  TextList := CreateStringList;
  inifile.ReadSection('DFKFLURSTUECKETEXTLAYERS', iniitems);
  for i := 0 to iniitems.Count - 1 do
  begin
    dummy := inifile.ReadString('DFKFLURSTUECKETEXTLAYERS', iniitems[i], '');
    TextList.Add(dummy);
  end;
  iniitems.Destroy;

   // create setup for combining texts with polygons
  Setup := CreateCombineTextsWithCPolysSetup;

  CPolyErrorFlurstuecke := inifile.ReadString('DFKSETTINGS', 'CPolyErrorFlurstuecke', '');
  TextErrorFlurstuecke := inifile.ReadString('DFKSETTINGS', 'TextErrorFlurstuecke', '');
  FlurstueckeCombineDbCol := inifile.ReadString('DFKSETTINGS', 'FlurstueckeCombineDbCol', '');

  Setup.CPolyErrorLayer := CPolyErrorFlurstuecke;
  Setup.TextErrorLayer := TextErrorFlurstuecke;
  Setup.TextDatabaseColumn := FlurstueckeCombineDbCol;
  Setup.LinkTextToCPoly := true;
  Setup.ApplyTextDatabaseInfoToCPoly := false;

  CombineCPolysWithTexts(CPolyList, TextList, Setup, ds_Layers, DataSource);

  if Datasource = ds_Selected then
  begin
      // copy result from temporary layer to original layer
    CopyNonDuplicatesToLayer('TMP_' + TargetLayerFlurstuecke, TargetLayerFlurstuecke, TRUE {use database}, ds_Layers);

      // delete temporary layers for generate objects and targetlayer islands
    Create_DatabaseInstance;
    AxImpExpDbc.CheckAndCreateExpTable('TMP_' + TargetLayerFlurstuecke); AxImpExpDbc.DeleteExpTable('TMP_' + TargetLayerFlurstuecke);
    Destroy_DatabaseInstance;
    RemoveLayerFromProject('TMP_' + TargetLayerFlurstuecke);
  end;

   // delete the error layers if no errors occured
  if (GetNumObjectsOfLayer(CPolyErrorFlurstuecke) = 0) and (CheckLayerExists(CPolyErrorFlurstuecke)) then
    RemoveLayerFromProject(CPolyErrorFlurstuecke);
  if (GetNumObjectsOfLayer(TextErrorFlurstuecke) = 0) and (CheckLayerExists(TextErrorFlurstuecke)) then
    RemoveLayerFromProject(TextErrorFlurstuecke);

  dummy := inifile.ReadString('DFKSETTINGS', 'ProcessMergeLayers', 'FALSE');
  if dummy = 'TRUE' then
    DKM_Process_LayersToMerge(Filename, 'DFK_MERGELAYERS');

   // delete the layers that are set in the LAYERSTODELETE section
  dummy := inifile.ReadString('DFKSETTINGS', 'ProcessDeleteLayers', 'FALSE');
  if dummy = 'TRUE' then
    DKM_Process_LayersToDelete(Filename, 'DFK_LAYERSTODELETE');
  FEvents.OnDisplayPass('---------------------------------------------------');

  iniFile.Free;

  if (DataSource = ds_Selected) then
    Destroy_SelectedStack;

  Destroy_StringfileInstance;

  ProcessingMode := pro_NORMAL;

  FEvents.OnDisplayPercent(100);
end;

//------------------------------------------------------------------------------
// NAME : MergeLayers
//------------------------------------------------------------------------------
// Method is used to merge layers to target layer
//
// PARAMETERS: LayerList       -> List of layers that should be merged
//             TargetLayername -> Name of target layer
//             DataSourceCol   -> Database column to store source layer
//
// RETURNVALUE: NONE
//------------------------------------------------------------------------------

procedure TAxGisCombine.MergeLayers(const LayerList: AxCmbStringList; const Targetlayername, DataSourceCol: WideString);
var Elems2Work, ElemsWorked: integer;
  targetdbcols: TDbColumns;
  ColName: widestring;
  ColType: TOleEnum;
  ColLength: integer;
  ColInfo: widestring;
  i, j: integer;
  TargetLayer: ILayer;
  checkforduplicates: boolean;
  SourceLayer: ILayer;
  aObject: OleVariant;
  oldId, newId: integer;
begin
  Create_StringFileInstance;
  Create_ManipulationInstance;

  Elems2Work := 0;
  ElemsWorked := 0;

   // first create target database
  targetdbcols := TDbcolumns.Create;
  if DataSourceCol <> '' then
    targetdbcols.Add(DataSourceCol, col_string, 255);

  Create_DatabaseInstance;
  for i := 0 to LayerList.Count - 1 do
  begin
    AxImpExpDbc.CheckAndCreateExpTable(LayerList.Items[i]);
    AxImpExpDbc.OpenExpTable(LayerList.Items[i]);
    while (AxImpExpDbc.GetExportColumn(LayerList.Items[i], ColName, ColType, ColLength)) do
      targetdbcols.Add(ColName, ColType, ColLength);
  end;

  if not CheckLayerExists(TargetLayername) then
    CreateLayer(TargetLayername);
   // create target database
  TargetLayer := Document.Layers.GetLayerByName(TargetLayername);
  if targetdbcols.Count > 0 then
  begin
    AxImpExpDbc.CheckAndCreateImpTable(TargetLayername);
    AxImpExpDbc.AddImpIdColumn(TargetLayername);
    for i := 0 to targetdbcols.Count - 1 do
      AxImpExpDbc.AddImpColumn(TargetLayername, targetdbcols.Items[i].ColName, targetdbcols.Items[i].ColType, targetdbcols.Items[i].ColLength);
    AxImpExpDbc.CreateImpTable(TargetLayername);
    AxImpExpDbc.OpenImpTable(TargetLayername);
  end;

  checkforduplicates := false;
  if TargetLayer.Count > 0 then
    checkforduplicates := true;
  for i := 0 to LayerList.Count - 1 do
  begin
    if not CheckLayerExists(LayerList.Items[i]) then
      continue;
    SourceLayer := Document.Layers.GetLayerByName(LayerList.Items[i]);
    FEvents.OnDisplayPass(StrfileObj.Assign(11) + LayerList.Items[i] + '->' + TargetLayername); // Merge data of layer:
    Elems2Work := SourceLayer.Count; ElemsWorked := 0;
    for j := 0 to SourceLayer.Count - 1 do
    begin
      ElemsWorked := ElemsWorked + 1;
      ShowPercent(Elems2Work, ElemsWorked);

      aObject := SourceLayer.Items[j];
      oldId := aObject.Index;
      newId := AxObjectMan.CopyObjectToLayer(aObject, TargetLayername);
      if newId < 1 then
        continue; // could not be copied

      if checkforduplicates then
      begin
        if AxObjectMan.CheckIdHasDuplicateOnLayer(OldId, TargetLayername) > 0 then
        begin
          Document.DeleteObjectByIndex(NewId);
          continue;
        end;
      end;

      if targetdbcols.Count = 0 then
        continue;

      AxImpExpDbc.AddImpIdInfo(TargetLayername, newId);
      if AxImpExpDbc.DoExportData(LayerList.Items[i], oldId) then
      begin
        while AxImpExpDbc.GetExportData(LayerList.Items[i], ColName, ColInfo) do
          AxImpExpDbc.AddImpInfo(TargetLayername, ColName, ColInfo);
      end;
      if DataSourceCol <> '' then
        AxImpExpDbc.AddImpInfo(TargetLayername, DataSourceCol, LayerList.Items[i]);
    end;
  end;
  targetdbcols.Destroy;

  Destroy_StringFileInstance;
  Destroy_ManipulationInstance;
  Destroy_DatabaseInstance;
end;

function TAxGisCombine.CreateStringList: AxCmbStringList;
begin
  result := TAxCmbStringList.Create;
end;

function TAxGisCombine.CreateSymbolOnLayerList: AxCmbSymOnLayerList;
begin
  result := TAxCmbSymOnLayerList.Create;
end;

function TAxGisCombine.CreateCombineSymbolsWithCPolysSetup: AxCmbSymbolsWithCPolysSetup;
begin
  result := TAxCmbSymbolsWithCPolysSetup.Create;
end;

function TAxGisCombine.CreateCombineTextsWithCPolysSetup: AxCmbTextsWithCPolysSetup;
begin
  result := TAxCmbTextsWithCPolysSetup.Create;
end;

procedure TAxGisCombine.IAxGisCombine__Set_Font(var Value: IFontDisp);
begin
  SetOleFont(FDelphiControl.Font, Value);
end;

procedure TAxGisCombine.IAxGisCombine_Set_Font(const Value: IFontDisp);
begin
  SetOleFont(FDelphiControl.Font, Value);
end;

initialization
  TActiveXControlFactory.Create(
    ComServer,
    TAxGisCombine,
    TPanel,
    Class_AxGisCombine,
    1,
    '',
    OLEMISC_SIMPLEFRAME or OLEMISC_ACTSLIKELABEL,
    tmApartment);
end.

