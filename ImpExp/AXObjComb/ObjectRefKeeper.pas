unit ObjectRefKeeper;

interface

uses SysUtils, WinTypes;


const
   ref_CPoly  = 4;
   ref_Symbol = 9;
   ref_Text   = 7;
   ref_Layer  = 18;

   DbCol_Int      = 0;
   DbCol_SmallInt = 1;
   DbCol_Float    = 2;
   DbCol_Date     = 3;
   DbCol_String   = 4;

   Insert_Space_Objects = 1000;
   Insert_Space_Default = 5;

type TDbAttrib=class
   protected
      FColName:string;
      FColInfo:string;
   public
      constructor Create;
      destructor Destroy;
      property ColName:string read FColName write FColName;
      property ColInfo:string read FColInfo write FColInfo;
end;

type TDbAttribs=class
   protected
      FAttribs:array of TDbAttrib;
      FAttribSize   :integer;
      FAttribSpace  :integer;
      FMaxReuseIndex:integer;
      FNoDuplicates :boolean;
   private
      function GetAttrib(Index:integer):TDbAttrib;
   public
      constructor Create;
      destructor Destroy;
      property Count:integer        read FAttribSize;
      property NoDuplicates:boolean read FNoDuplicates write FNoDuplicates;
      property Items[Index: Integer]:TDbAttrib read GetAttrib;
      procedure Add(const ColName:string; const ColInfo:string);
      function FindByColName(const ColName:string):TDbAttrib;
      procedure Clear;
      function IsEqual(const other:TDbAttribs):boolean;
      procedure Assign(const other:TDbAttribs);
end;

type TDbColumn=class
   protected
      FColName:string;
      FColType:integer;
      FColLength:integer;
      procedure SetColType(const value:integer);
      procedure SetColSize(const value:integer);
      function GetColSize:integer;
   public
      constructor Create;
      destructor Destroy;
      property ColName:string read FColName write FColName;
      property ColType:integer read FColType write SetColType;
      property ColLength:integer read GetColSize write SetColSize;
      procedure Init;
end;

type TDbColumns=class
   protected
      FColumns:array of TDbColumn;
      FColumnSize   :integer;
      FColumnSpace  :integer;
      FMaxReuseIndex:integer;
   private
      function GetColumn(Index:integer):TDbColumn;
   public
      constructor Create;
      destructor Destroy;
      property Count:integer read FColumnSize;
      property Items[Index: Integer]:TDbColumn read GetColumn;
      procedure Add(const ColName:string; const ColType:integer; const ColLength:integer);
      function Find(const ColName:string):TDbColumn;
      procedure Clear;
end;

type
   TLinkedObjects=class; // forward declaration
   TBaseRef=class
   protected
      FObjectType   :integer;          // object type
      FProgisId     :integer;          // Progis Id of object
      FDbAttribs    :TDbAttribs;       // list of database attributes
      FLinkedObjects:TLinkedObjects;   // list of linked objects
      FParent       :TBaseRef;         // reference to parent to that object belongs (layer)
   public
      constructor Create;
      destructor Destroy; virtual;
      property ObjectType:integer           read FObjectType;
      property Attributes:TDbAttribs        read FDbAttribs;
      property LinkedObjects:TLinkedObjects read FLinkedObjects;
      property Parent:TBaseRef              read FParent   write FParent;
      property ProgisId:integer             read FProgisId write FProgisId;
      procedure CreateAttribList;
      procedure CreateLinkedObjectList;
   end;

   // class to store linked objects
   TLinkedObjects=class
   protected
      FItems :array of TBaseRef;
      FSize  :integer;
      FSpace :integer;
   private
       function GetItem(Index:integer):TBaseRef;
       procedure SetItem(Index:integer; const aItem:TBaseRef);
   public
       constructor Create;
       destructor Destroy;
       property Count:integer read FSize;
       property Items[Index: Integer]:TBaseRef read GetItem write SetItem;
       procedure Add(const aItem:TBaseRef);
       procedure Clear;
end;

type TLayerRef=class (TBaseRef)
   protected
      FName     :string;
      FDBColumns:TDbColumns;
   public
      constructor Create;
      destructor Destroy;
      property DbColumns:TDbColumns read FDBColumns;
      property Name:string read FName write FName;
      procedure CreateDbColumnList;
end;

type TCPolyRef=class (TBaseRef)
   protected
      FArea:double;
   public
      constructor Create;
      destructor Destroy;
      property Area:double read FArea write FArea;
end;

type TSymbolRef=class (TBaseRef)
   protected
      FName:string;
      FX:double;
      FY:double;
      FDoubleValue:double; // property to store double value
   public
      constructor Create;
      destructor Destroy;
      property Name:string read FName write FName;
      property X:double read FX write FX;
      property Y:double read FY write FY;
      property DoubleValue:double read FDoubleValue write FDoubleValue;
end;

type TTextRef=class (TBaseRef)
   protected
      FText:string;
      FX:double;
      FY:double;
   public
      constructor Create;
      destructor Destroy;
      property Text:string read FText write FText;
      property X:double read FX write FX;
      property Y:double read FY write FY;
end;

type TRefItemList=class
   protected
      FItems :array of TBaseRef;
      FSize  :integer;
      FSpace :integer;
   private
       function GetItem(Index:integer):TBaseRef;
   public
       constructor Create;
       destructor Destroy;
       property Count:integer read FSize;
       property Items[Index: Integer]:TBaseRef read GetItem;
       procedure Add(const aItem:TBaseRef);
       procedure Clear;
end;

type TRefLayerList=class
   protected
       FItems :array of TLayerRef;
       FSize  :integer;
       FSpace :integer;
   private
       FSearchIndex:integer;
       function GetItem(Index:integer):TLayerRef;
   public
       constructor Create;
       destructor Destroy;
       property Count:integer read FSize;
       property Items[Index: Integer]:TLayerRef read GetItem;
       procedure Add(const aItem:TLayerRef);
       procedure Clear;
       function Find(const aName:string):TLayerRef; overload;
       function Find(const attribs:TDbAttribs):TLayerRef; overload;
end;

type TObjectRefKeeper=class
   protected
      FLayers:TRefLayerList;
      FItems :TRefItemList;
   public
      constructor Create;
      destructor Destroy;
      property Layers :TRefLayerList read FLayers;
      property Objects:TRefItemList  read FItems;
end;

implementation

// class TDbAttrib -------------------------------------------------------------
constructor TDbAttrib.Create;
begin
end;

destructor TDbAttrib.Destroy;
begin
end;

// class TDbAttribs -------------------------------------------------------------
constructor TDbAttribs.Create;
begin
   FAttribSize:=0;
   FAttribSpace:=0;
   FNoDuplicates:=true;
   FMaxReuseIndex:=-1; // no index used yet
end;

destructor TDbAttribs.Destroy;
var i:integer;
begin
   if FMaxReuseIndex >= 0 then
   begin
      for i:=0 to FMaxReuseIndex do
         FAttribs[i].Destroy;
   end;
   SetLength(FAttribs,0);
end;
function TDbAttribs.GetAttrib(Index:integer):TDbAttrib;
begin
   result:=nil;
   if (Index < FAttribSize) and (Index >= 0) then
      result:=FAttribs[Index];
end;
procedure TDbAttribs.Add(const ColName:string; const ColInfo:string);
var aDbAttrib:TDbAttrib;
begin
   aDbAttrib:=FindByColName(ColName);
   if (aDbAttrib = nil) then
   begin
      FAttribSize:=FAttribSize+1;
      if FMaxReuseIndex >= FAttribSize-1 then
         aDbAttrib:=FAttribs[FAttribSize-1] // object can be reused
      else
      begin
         // new object has to be created
         aDbAttrib:=TDbAttrib.Create;
         if FAttribSize >= FAttribSpace then
         begin
            FAttribSpace:=FAttribSpace+Insert_Space_Default;
            SetLength(FAttribs, FAttribSpace);
         end;
         FAttribs[FAttribSize-1]:=aDbAttrib;
         FMaxReuseIndex:=FAttribSize-1;
      end;
      aDbAttrib.ColName:=ColName;
      aDbAttrib.ColInfo:=ColInfo;
   end
   else
      aDbAttrib.ColInfo:=ColInfo;
end;
function TDbAttribs.FindByColName(const ColName:string):TDbAttrib;
var i:integer;
begin
   result:=nil;
   if (not FNoDuplicates) or (FAttribSize < 1) then
      exit;
   for i:=0 to FAttribSize-1 do
   begin
      if FAttribs[i].ColName = ColName then
      begin
         result:=FAttribs[i];
         exit;
      end;
   end;
end;
procedure TDbAttribs.Clear;
begin
   FAttribSize:=0;
end;
function TDbAttribs.IsEqual(const other:TDbAttribs):boolean;
var i:integer;
    otherattrib:TDbAttrib;
begin
   result:=true;
   if FAttribSize <> other.FAttribSize then
   begin
      result:=false;
      exit;
   end;
   for i:=0 to FAttribsize-1 do
   begin
      otherattrib:=other.FindByColName(FAttribs[i].ColName);
      if (otherattrib = nil) then
      begin
         result:=false;
         exit;
      end;
      if otherattrib.ColInfo <> FAttribs[i].ColInfo then
      begin
         result:=false;
         exit;
      end;
   end;
end;
procedure TDbAttribs.Assign(const other:TDbAttribs);
var i:integer;
begin
   Clear;
   for i:=0 to other.FAttribSize-1 do
      Add(other.FAttribs[i].ColName, other.FAttribs[i].ColInfo);
end;

// class TDbColumn -------------------------------------------------------------
constructor TDbColumn.Create;
begin
   FColType:=DbCol_SmallInt;
   FColLength:=0;
end;

destructor TDbColumn.Destroy;
begin
end;

procedure TDbColumn.Init;
begin
   FColType:=DbCol_SmallInt;
   FColLength:=0;
end;

procedure TDbColumn.SetColType(const value:integer);
begin
   if value = FColType then
      exit; // no change
   if (FColType = DbCol_SmallInt) and (value <> DbCol_SmallInt) then
      FColType:=value
   else if (FColType = DbCol_Int) then
   begin
      if value = DbCol_Float then
         FColType:=value
      else if value <> DbCol_SmallInt then
         FColType:=DbCol_String;
   end
   else if (FColType = DbCol_Float) then
   begin
      if (value <> DbCol_SmallInt) and (value <> DbCol_Int) then
         FColType:=DbCol_String;
   end
   else
      FColType:=DbCol_String;
end;

procedure TDbColumn.SetColSize(const value:integer);
begin
   if value > FColLength then
      FColLength:=value;
end;
function TDbColumn.GetColSize:integer;
begin
   if (FColType <> DbCol_String) and (FColLength = 0) then
      result:=16 // default size
   else
      result:=FColLength;
end;

// class TDbColumns-------------------------------------------------------------
constructor TDbColumns.Create;
begin
   FColumnSize:=0;
   FColumnSpace:=0;
   FMaxReuseIndex:=-1; // no index used yet
end;

destructor TDbColumns.Destroy;
var i:integer;
begin
   if FMaxReuseIndex >= 0 then
   begin
      for i:=0 to FMaxReuseIndex do
         FColumns[i].Destroy;
   end;
   SetLength(FColumns, 0);
end;

function TDbColumns.GetColumn(Index:integer):TDbColumn;
begin
   result:=nil;
   if (Index < FColumnsize) and (Index >= 0) then
      result:=FColumns[Index];
end;

procedure TDbColumns.Add(const ColName:string; const ColType:integer; const ColLength:integer);
var aColumn:TDbColumn;
begin
   aColumn:=Find(ColName);
   if aColumn = nil then
   begin
      FColumnSize:=FColumnSize+1;
      if FMaxReuseIndex >= FColumnSize-1 then
      begin
         aColumn:=FColumns[FColumnSize-1]; // object can be reused
         aColumn.Init;
      end
      else
      begin
         // new object has to be created
         aColumn:=TDbColumn.Create;
         if FColumnSize >= FColumnSpace then
         begin
            FColumnSpace:=FColumnSpace+Insert_Space_Default;
            SetLength(FColumns, FColumnSpace);
         end;
         FColumns[FColumnSize-1]:=aColumn;
         FMaxReuseIndex:=FColumnSize-1;
      end;
      aColumn.ColName:=ColName;
      aColumn.ColType:=ColType;
      aColumn.ColLength:=ColLength;
   end
   else
   begin
      aColumn.ColType:=ColType;
      aColumn.ColLength:=ColLength;
   end
end;

function TDbColumns.Find(const ColName:string):TDbColumn;
var i:integer;
begin
   result:=nil;
   if FColumnSize = 0 then
      exit;
   for i:=0 to FColumnSize-1 do
   begin
      if FColumns[i].ColName = ColName then
      begin
         result:=FColumns[i];
         break;
      end;
   end;
end;

procedure TDbColumns.Clear;
begin
   FColumnSize:=0;
end;

// class TLinkedObjects --------------------------------------------------------
constructor TLinkedObjects.Create;
begin
   FSize:=0;
   FSpace:=0;
end;

destructor TLinkedObjects.Destroy;
begin
   Clear;
end;

function TLinkedObjects.GetItem(Index:integer):TBaseRef;
begin
   result:=nil;
   if (Index < FSize) and (Index >= 0) then
      result:=FItems[Index];
end;

procedure TLinkedObjects.SetItem(Index:integer; const aItem:TBaseRef);
begin
   if (Index < FSize) and (Index >= 0) then
      FItems[Index]:=aItem;
end;

procedure TLinkedObjects.Add(const aItem:TBaseRef);
begin
   FSize:=FSize+1;
   if FSize >= FSpace then
   begin
      FSpace:=FSpace+Insert_Space_Default;
      SetLength(FItems, FSpace);
   end;
   FItems[FSize-1]:=aItem;
end;

procedure TLinkedObjects.Clear;
begin
   FSize:=0; FSpace:=0;
   SetLength(FItems, FSize);
end;

// class TBaseRef --------------------------------------------------------------
constructor TBaseRef.Create;
begin
   FObjectType:=0;
   FParent:=nil;
   FDbAttribs:=nil;
   FLinkedObjects:=nil;
   FProgisId:=0;
end;

destructor TBaseRef.Destroy;
begin
   FParent:=nil;
   if FDbAttribs <> nil then
      FDbAttribs.Destroy;
   if FLinkedObjects <> nil then
      FLinkedObjects.Destroy;
end;

procedure TBaseRef.CreateAttribList;
begin
   if FDbAttribs = nil then
      FDbAttribs:=TDbAttribs.Create;
   FDbAttribs.Clear;
end;

procedure TBaseRef.CreateLinkedObjectList;
begin
   if FLinkedObjects = nil then
      FLinkedObjects:=TLinkedObjects.Create;
   FLinkedObjects.Clear;
end;

// class TLayerRef -------------------------------------------------------------
constructor TLayerRef.Create;
begin
   inherited Create;
   FDBColumns:=nil;
   FObjectType:=Ref_Layer;
end;

destructor TLayerRef.Destroy;
begin
   if FDbColumns <> nil then
      FDbColumns.Destroy;
   inherited Destroy;
end;

procedure TLayerRef.CreateDbColumnList;
begin
   if FDbColumns = nil then
      FDbColumns:=TDbColumns.Create;
   FDbColumns.Clear;
end;

// class TCPolyRef -------------------------------------------------------------
constructor TCPolyRef.Create;
begin
   inherited Create;
   FObjectType:=ref_CPoly;
   FArea:=0;
end;

destructor TCPolyRef.Destroy;
begin
   inherited Destroy;
end;

// class TSymbolRef ------------------------------------------------------------
constructor TSymbolRef.Create;
begin
   inherited Create;
   FObjectType:=ref_Symbol;
   FX:=0;
   FY:=0;
   FDoubleValue:=0;
end;

destructor TSymbolRef.Destroy;
begin
   inherited Destroy;
end;

// class TTextRef --------------------------------------------------------------
constructor TTextRef.Create;
begin
   inherited Create;
   FObjectType:=ref_Text;
   FX:=0;
   FY:=0;
end;

destructor TTextRef.Destroy;
begin
   inherited Destroy;
end;

// class TRefItemList ----------------------------------------------------------
constructor TRefItemList.Create;
begin
   FSize:=0;
   FSpace:=0;
end;

destructor TRefItemList.Destroy;
begin
   Clear;
end;

procedure TRefItemList.Clear;
var i:integer;
begin
   if FSize >= 0 then
   begin
      for i:=0 to FSize-1 do
         FItems[i].Destroy;
   end;
   FSize:=0;
   FSpace:=0;
   SetLength(FItems, 0);
end;

function TRefItemList.GetItem(Index:integer):TBaseRef;
begin
   result:=nil;
   if (Index < FSize) and (Index >= 0) then
      result:=FItems[Index];
end;

procedure TRefItemList.Add(const aItem:TBaseRef);
begin
   FSize:=FSize+1;
   if FSize >= FSpace then
   begin
      FSpace:=FSpace+Insert_Space_Objects;
      SetLength(FItems, FSpace);
   end;
   FItems[FSize-1]:=aItem;
end;

// class TRefLayerList ---------------------------------------------------------
constructor TRefLayerList.Create;
begin
   FSize:=0;
   FSpace:=0;
   FSearchIndex:=-1;
end;

destructor TRefLayerList.Destroy;
begin
   Clear;
end;

function TRefLayerList.GetItem(Index:integer):TLayerRef;
begin
   result:=nil;
   if (Index < FSize) and (Index >= 0) then
      result:=FItems[Index];
end;

procedure TRefLayerList.Add(const aItem:TLayerRef);
begin
   FSize:=FSize+1;
   if FSize >= FSpace then
   begin
      FSpace:=FSpace+Insert_Space_Default;
      SetLength(FItems, FSpace);
   end;
   FItems[FSize-1]:=aItem;
end;

procedure TRefLayerList.Clear;
var i:integer;
begin
   if FSize >= 0 then
   begin
      for i:=0 to FSize-1 do
         FItems[i].Destroy;
   end;
   FSize:=0; FSpace:=0;
   SetLength(FItems, FSize);
end;


function TRefLayerList.Find(const attribs:TDbAttribs):TLayerRef;
var i:integer;
begin
   result:=nil;
   if (FSearchIndex < FSize) and (FSearchIndex >= 0) then
   begin
      // check with search index
      if (FItems[FSearchIndex].Attributes <> nil) and (FItems[FSearchIndex].Attributes.IsEqual(attribs)) then
      begin
         result:=FItems[FSearchIndex];
         exit;
      end;
   end;

   // normal search
   if FSize > 0 then
   begin
      for i:=0 to FSize-1 do
      begin
         if (FItems[i].Attributes <> nil) and (FItems[i].Attributes.IsEqual(attribs)) then
         begin
            result:=Fitems[i];
            FSearchIndex:=i;
            break;
         end;
      end;
   end;

end;

function TRefLayerList.Find(const aName:string):TLayerRef;
var i:integer;
begin
   result:=nil;
   if (FSearchIndex < FSize) and (FSearchIndex >= 0) then
   begin
      // check with search index
      if FItems[FSearchIndex].Name = aName then
      begin
         result:=FItems[FSearchIndex];
         exit;
      end;
   end;
   // normal search
   if FSize > 0 then
   begin
      for i:=0 to FSize-1 do
      begin
         if FItems[i].Name = aName then
         begin
            result:=Fitems[i];
            FSearchIndex:=i;
            break;
         end;
      end;
   end;
end;

// class TObjectRefKeeper ------------------------------------------------------
constructor TObjectRefKeeper.Create;
begin
   FLayers:=TRefLayerList.Create;
   FItems:=TRefItemList.Create;
end;

destructor TObjectRefKeeper.Destroy;
begin
   FLayers.Destroy;
   FItems.Destroy;
end;

end.
