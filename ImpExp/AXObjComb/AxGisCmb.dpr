library AxGisCmb;

uses
  ComServ,
  AxGisCmb_TLB in 'AxGisCmb_TLB.pas',
  AxGisCombineImpl in 'AxGisCombineImpl.pas' {AxGisCombine: CoClass},
  Strfile in 'Strfile.pas',
  ObjectRefKeeper in 'ObjectRefKeeper.pas',
  AxGisCmbStringList in 'AxGisCmbStringList.pas',
  AxGisCmbSymOnLayerList in 'AxGisCmbSymOnLayerList.pas',
  ObjectRefFilter in 'ObjectRefFilter.pas',
  AxGisCmbSymbolsWithCPolysSetup in 'AxGisCmbSymbolsWithCPolysSetup.pas',
  AxGisCmbTextsWithCPolysSetup in 'AxGisCmbTextsWithCPolysSetup.pas',
  Wingis_TLB in '..\..\Program Files\Borland\Delphi6\Imports\Wingis_TLB.pas';

{$E ocx}
              
exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
