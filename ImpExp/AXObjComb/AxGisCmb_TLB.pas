unit AxGisCmb_TLB;

// ************************************************************************ //
// WARNING                                                                  //
// -------                                                                  //
// The types declared in this file were generated from data read from a     //
// Type Library. If this type library is explicitly or indirectly (via      //
// another type library referring to this type library) re-imported, or the //
// 'Refresh' command of the Type Library Editor activated while editing the //
// Type Library, the contents of this file will be regenerated and all      //
// manual modifications will be lost.                                       //
// ************************************************************************ //

// PASTLWTR : $Revision:   1.11.1.75  $
// File generated on 07.02.2019 19:06:13 from Type Library described below.

// ************************************************************************ //
// Type Lib: D:\Cadmensky\Projects\Progis\ImpExp\AXObjComb\AxGisCmb.tlb
// IID\LCID: {A0E9402E-E635-11D5-9D09-00C0F057241A}\0
// Helpfile: 
// HelpString: AxGisCmb Library
// Version:    1.0
// ************************************************************************ //

interface

uses Windows, ActiveX, Classes, Graphics, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:      //
//   Type Libraries     : LIBID_xxxx                                    //
//   CoClasses          : CLASS_xxxx                                    //
//   DISPInterfaces     : DIID_xxxx                                     //
//   Non-DISP interfaces: IID_xxxx                                      //
// *********************************************************************//
const
  LIBID_AxGisCmb: TGUID = '{A0E9402E-E635-11D5-9D09-00C0F057241A}';
  IID_IAxGisCombine: TGUID = '{A0E9402F-E635-11D5-9D09-00C0F057241A}';
  DIID_IAxGisCombineEvents: TGUID = '{A0E94031-E635-11D5-9D09-00C0F057241A}';
  CLASS_AxGisCombine: TGUID = '{A0E94033-E635-11D5-9D09-00C0F057241A}';
  IID_IAxCmbStringList: TGUID = '{2DC48110-0FBB-46F6-8C02-F98C7B6D4BCF}';
  CLASS_AxCmbStringList: TGUID = '{7BEFB44B-C1CF-4693-90C7-FF02DD2743A9}';
  IID_IAxCmbSymOnLayerList: TGUID = '{6CCC3CF2-F2C6-4091-8987-09E5FD3EE967}';
  CLASS_AxCmbSymOnLayerList: TGUID = '{16C79A34-11B3-421F-B0B5-B8D50708C791}';
  IID_IAxCmbSymolsWithCPolysSetup: TGUID = '{B8A82819-9843-4B5A-AFD7-38CE582D9FC4}';
  CLASS_AxCmbSymbolsWithCPolysSetup: TGUID = '{AFF4CBE3-0BF6-4882-AF23-437E9826E087}';
  IID_IAxCmbTextsWithCPolysSetup: TGUID = '{E07563E8-58E5-4FB9-9683-371EC0BD8CEC}';
  CLASS_AxCmbTextsWithCPolysSetup: TGUID = '{CE3268AD-5C6D-4493-8890-7AE4691BFE74}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                  //
// *********************************************************************//
// TxAlignment constants
type
  TxAlignment = TOleEnum;
const
  taLeftJustify = $00000000;
  taRightJustify = $00000001;
  taCenter = $00000002;

// TxBevelCut constants
type
  TxBevelCut = TOleEnum;
const
  bvNone = $00000000;
  bvLowered = $00000001;
  bvRaised = $00000002;
  bvSpace = $00000003;

// TxBorderStyle constants
type
  TxBorderStyle = TOleEnum;
const
  bsNone = $00000000;
  bsSingle = $00000001;

// TxDragMode constants
type
  TxDragMode = TOleEnum;
const
  dmManual = $00000000;
  dmAutomatic = $00000001;

// TxMouseButton constants
type
  TxMouseButton = TOleEnum;
const
  mbLeft = $00000000;
  mbRight = $00000001;
  mbMiddle = $00000002;

// AxCmbDataSource constants
type
  AxCmbDataSource = TOleEnum;
const
  ds_Layers = $00000000;
  ds_Selected = $00000001;

type

// *********************************************************************//
// Forward declaration of interfaces defined in Type Library            //
// *********************************************************************//
  IAxGisCombine = interface;
  IAxGisCombineDisp = dispinterface;
  IAxGisCombineEvents = dispinterface;
  IAxCmbStringList = interface;
  IAxCmbStringListDisp = dispinterface;
  IAxCmbSymOnLayerList = interface;
  IAxCmbSymOnLayerListDisp = dispinterface;
  IAxCmbSymolsWithCPolysSetup = interface;
  IAxCmbSymolsWithCPolysSetupDisp = dispinterface;
  IAxCmbTextsWithCPolysSetup = interface;
  IAxCmbTextsWithCPolysSetupDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                     //
// (NOTE: Here we map each CoClass to its Default Interface)            //
// *********************************************************************//
  AxGisCombine = IAxGisCombine;
  AxCmbStringList = IAxCmbStringList;
  AxCmbSymOnLayerList = IAxCmbSymOnLayerList;
  AxCmbSymbolsWithCPolysSetup = IAxCmbSymolsWithCPolysSetup;
  AxCmbTextsWithCPolysSetup = IAxCmbTextsWithCPolysSetup;


// *********************************************************************//
// Declaration of structures, unions and aliases.                       //
// *********************************************************************//
  PPUserType1 = ^IFontDisp; {*}
  POleVariant1 = ^OleVariant; {*}


// *********************************************************************//
// Interface: IAxGisCombine
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A0E9402F-E635-11D5-9D09-00C0F057241A}
// *********************************************************************//
  IAxGisCombine = interface(IDispatch)
    ['{A0E9402F-E635-11D5-9D09-00C0F057241A}']
    function Get_Alignment: TxAlignment; safecall;
    procedure Set_Alignment(Value: TxAlignment); safecall;
    function Get_AutoSize: WordBool; safecall;
    procedure Set_AutoSize(Value: WordBool); safecall;
    function Get_BevelInner: TxBevelCut; safecall;
    procedure Set_BevelInner(Value: TxBevelCut); safecall;
    function Get_BevelOuter: TxBevelCut; safecall;
    procedure Set_BevelOuter(Value: TxBevelCut); safecall;
    function Get_BorderStyle: TxBorderStyle; safecall;
    procedure Set_BorderStyle(Value: TxBorderStyle); safecall;
    function Get_Caption: WideString; safecall;
    procedure Set_Caption(const Value: WideString); safecall;
    function Get_Color: OLE_COLOR; safecall;
    procedure Set_Color(Value: OLE_COLOR); safecall;
    function Get_Ctl3D: WordBool; safecall;
    procedure Set_Ctl3D(Value: WordBool); safecall;
    function Get_UseDockManager: WordBool; safecall;
    procedure Set_UseDockManager(Value: WordBool); safecall;
    function Get_DockSite: WordBool; safecall;
    procedure Set_DockSite(Value: WordBool); safecall;
    function Get_DragCursor: Smallint; safecall;
    procedure Set_DragCursor(Value: Smallint); safecall;
    function Get_DragMode: TxDragMode; safecall;
    procedure Set_DragMode(Value: TxDragMode); safecall;
    function Get_Enabled: WordBool; safecall;
    procedure Set_Enabled(Value: WordBool); safecall;
    function Get_FullRepaint: WordBool; safecall;
    procedure Set_FullRepaint(Value: WordBool); safecall;
    function Get_Font: IFontDisp; safecall;
    procedure _Set_Font(const Value: IFontDisp); safecall;
    procedure Set_Font(var Value: IFontDisp); safecall;
    function Get_Locked: WordBool; safecall;
    procedure Set_Locked(Value: WordBool); safecall;
    function Get_ParentColor: WordBool; safecall;
    procedure Set_ParentColor(Value: WordBool); safecall;
    function Get_ParentCtl3D: WordBool; safecall;
    procedure Set_ParentCtl3D(Value: WordBool); safecall;
    function Get_Visible: WordBool; safecall;
    procedure Set_Visible(Value: WordBool); safecall;
    function Get_DoubleBuffered: WordBool; safecall;
    procedure Set_DoubleBuffered(Value: WordBool); safecall;
    function Get_VisibleDockClientCount: Integer; safecall;
    function DrawTextBiDiModeFlagsReadingOnly: Integer; safecall;
    procedure InitiateAction; safecall;
    function IsRightToLeft: WordBool; safecall;
    function UseRightToLeftReading: WordBool; safecall;
    function UseRightToLeftScrollBar: WordBool; safecall;
    function Get_Cursor: Smallint; safecall;
    procedure Set_Cursor(Value: Smallint); safecall;
    function Get_WorkingDir: WideString; safecall;
    procedure Set_WorkingDir(const Value: WideString); safecall;
    function Get_LngCode: WideString; safecall;
    procedure Set_LngCode(const Value: WideString); safecall;
    function Get_DBName: WideString; safecall;
    procedure Set_DBName(const Value: WideString); safecall;
    function Get_DBTable: WideString; safecall;
    procedure Set_DBTable(const Value: WideString); safecall;
    function Get_DBMode: Integer; safecall;
    procedure Set_DBMode(Value: Integer); safecall;
    procedure CombineCPolysWithSymbols(const CPolyList: AxCmbStringList; 
                                       const SymOnLayerList: AxCmbSymOnLayerList; 
                                       const Setup: AxCmbSymbolsWithCPolysSetup; 
                                       PolygonDataSource: AxCmbDataSource; 
                                       SymbolDataSource: AxCmbDataSource); safecall;
    procedure SetApp(var aApp: OleVariant); safecall;
    procedure SetDocument(var aDocument: OleVariant); safecall;
    procedure BEV_Combine(const Filename: WideString; DataSource: AxCmbDataSource); safecall;
    procedure CombineCPolysByDatabaseAttrib(const SourceLayer: WideString; 
                                            const TargetLayer: WideString; 
                                            const DatabaseColumns: AxCmbStringList; 
                                            DataSource: AxCmbDataSource); safecall;
    procedure CombineCPolysWithTexts(const CPolyList: AxCmbStringList; 
                                     const TextList: AxCmbStringList; 
                                     const Setup: AxCmbTextsWithCPolysSetup; 
                                     PolygonDataSource: AxCmbDataSource; 
                                     TextDataSource: AxCmbDataSource); safecall;
    procedure MEDIX_Combine(const Filename: WideString; DataSource: AxCmbDataSource); safecall;
    procedure DFK_Combine(const Filename: WideString; DataSource: AxCmbDataSource); safecall;
    procedure MergeLayers(const LayerList: AxCmbStringList; const Targetlayername: WideString; 
                          const DataSourceCol: WideString); safecall;
    function CreateStringList: AxCmbStringList; safecall;
    function CreateSymbolOnLayerList: AxCmbSymOnLayerList; safecall;
    function CreateCombineSymbolsWithCPolysSetup: AxCmbSymbolsWithCPolysSetup; safecall;
    function CreateCombineTextsWithCPolysSetup: AxCmbTextsWithCPolysSetup; safecall;
    property Alignment: TxAlignment read Get_Alignment write Set_Alignment;
    property AutoSize: WordBool read Get_AutoSize write Set_AutoSize;
    property BevelInner: TxBevelCut read Get_BevelInner write Set_BevelInner;
    property BevelOuter: TxBevelCut read Get_BevelOuter write Set_BevelOuter;
    property BorderStyle: TxBorderStyle read Get_BorderStyle write Set_BorderStyle;
    property Caption: WideString read Get_Caption write Set_Caption;
    property Color: OLE_COLOR read Get_Color write Set_Color;
    property Ctl3D: WordBool read Get_Ctl3D write Set_Ctl3D;
    property UseDockManager: WordBool read Get_UseDockManager write Set_UseDockManager;
    property DockSite: WordBool read Get_DockSite write Set_DockSite;
    property DragCursor: Smallint read Get_DragCursor write Set_DragCursor;
    property DragMode: TxDragMode read Get_DragMode write Set_DragMode;
    property Enabled: WordBool read Get_Enabled write Set_Enabled;
    property FullRepaint: WordBool read Get_FullRepaint write Set_FullRepaint;
    property Font: IFontDisp read Get_Font write _Set_Font;
    property Locked: WordBool read Get_Locked write Set_Locked;
    property ParentColor: WordBool read Get_ParentColor write Set_ParentColor;
    property ParentCtl3D: WordBool read Get_ParentCtl3D write Set_ParentCtl3D;
    property Visible: WordBool read Get_Visible write Set_Visible;
    property DoubleBuffered: WordBool read Get_DoubleBuffered write Set_DoubleBuffered;
    property VisibleDockClientCount: Integer read Get_VisibleDockClientCount;
    property Cursor: Smallint read Get_Cursor write Set_Cursor;
    property WorkingDir: WideString read Get_WorkingDir write Set_WorkingDir;
    property LngCode: WideString read Get_LngCode write Set_LngCode;
    property DBName: WideString read Get_DBName write Set_DBName;
    property DBTable: WideString read Get_DBTable write Set_DBTable;
    property DBMode: Integer read Get_DBMode write Set_DBMode;
  end;

// *********************************************************************//
// DispIntf:  IAxGisCombineDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A0E9402F-E635-11D5-9D09-00C0F057241A}
// *********************************************************************//
  IAxGisCombineDisp = dispinterface
    ['{A0E9402F-E635-11D5-9D09-00C0F057241A}']
    property Alignment: TxAlignment dispid 1;
    property AutoSize: WordBool dispid 2;
    property BevelInner: TxBevelCut dispid 3;
    property BevelOuter: TxBevelCut dispid 4;
    property BorderStyle: TxBorderStyle dispid 5;
    property Caption: WideString dispid -518;
    property Color: OLE_COLOR dispid -501;
    property Ctl3D: WordBool dispid 6;
    property UseDockManager: WordBool dispid 7;
    property DockSite: WordBool dispid 8;
    property DragCursor: Smallint dispid 9;
    property DragMode: TxDragMode dispid 10;
    property Enabled: WordBool dispid -514;
    property FullRepaint: WordBool dispid 11;
    property Font: IFontDisp dispid -512;
    property Locked: WordBool dispid 12;
    property ParentColor: WordBool dispid 13;
    property ParentCtl3D: WordBool dispid 14;
    property Visible: WordBool dispid 15;
    property DoubleBuffered: WordBool dispid 16;
    property VisibleDockClientCount: Integer readonly dispid 17;
    function DrawTextBiDiModeFlagsReadingOnly: Integer; dispid 19;
    procedure InitiateAction; dispid 20;
    function IsRightToLeft: WordBool; dispid 21;
    function UseRightToLeftReading: WordBool; dispid 24;
    function UseRightToLeftScrollBar: WordBool; dispid 25;
    property Cursor: Smallint dispid 26;
    property WorkingDir: WideString dispid 33;
    property LngCode: WideString dispid 34;
    property DBName: WideString dispid 35;
    property DBTable: WideString dispid 36;
    property DBMode: Integer dispid 37;
    procedure CombineCPolysWithSymbols(const CPolyList: AxCmbStringList; 
                                       const SymOnLayerList: AxCmbSymOnLayerList; 
                                       const Setup: AxCmbSymbolsWithCPolysSetup; 
                                       PolygonDataSource: AxCmbDataSource; 
                                       SymbolDataSource: AxCmbDataSource); dispid 22;
    procedure SetApp(var aApp: OleVariant); dispid 23;
    procedure SetDocument(var aDocument: OleVariant); dispid 27;
    procedure BEV_Combine(const Filename: WideString; DataSource: AxCmbDataSource); dispid 29;
    procedure CombineCPolysByDatabaseAttrib(const SourceLayer: WideString; 
                                            const TargetLayer: WideString; 
                                            const DatabaseColumns: AxCmbStringList; 
                                            DataSource: AxCmbDataSource); dispid 38;
    procedure CombineCPolysWithTexts(const CPolyList: AxCmbStringList; 
                                     const TextList: AxCmbStringList; 
                                     const Setup: AxCmbTextsWithCPolysSetup; 
                                     PolygonDataSource: AxCmbDataSource; 
                                     TextDataSource: AxCmbDataSource); dispid 42;
    procedure MEDIX_Combine(const Filename: WideString; DataSource: AxCmbDataSource); dispid 43;
    procedure DFK_Combine(const Filename: WideString; DataSource: AxCmbDataSource); dispid 44;
    procedure MergeLayers(const LayerList: AxCmbStringList; const Targetlayername: WideString; 
                          const DataSourceCol: WideString); dispid 46;
    function CreateStringList: AxCmbStringList; dispid 47;
    function CreateSymbolOnLayerList: AxCmbSymOnLayerList; dispid 48;
    function CreateCombineSymbolsWithCPolysSetup: AxCmbSymbolsWithCPolysSetup; dispid 49;
    function CreateCombineTextsWithCPolysSetup: AxCmbTextsWithCPolysSetup; dispid 18;
  end;

// *********************************************************************//
// DispIntf:  IAxGisCombineEvents
// Flags:     (4096) Dispatchable
// GUID:      {A0E94031-E635-11D5-9D09-00C0F057241A}
// *********************************************************************//
  IAxGisCombineEvents = dispinterface
    ['{A0E94031-E635-11D5-9D09-00C0F057241A}']
    procedure OnCanResize(var NewWidth: Integer; var NewHeight: Integer; var Resize: WordBool); dispid 1;
    procedure OnClick; dispid 2;
    procedure OnConstrainedResize(var MinWidth: Integer; var MinHeight: Integer; 
                                  var MaxWidth: Integer; var MaxHeight: Integer); dispid 3;
    procedure OnDblClick; dispid 7;
    procedure OnResize; dispid 16;
    procedure OnDisplayPass(const aPass: WideString); dispid 4;
    procedure OnDisplayPercent(aValue: Integer); dispid 5;
  end;

// *********************************************************************//
// Interface: IAxCmbStringList
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2DC48110-0FBB-46F6-8C02-F98C7B6D4BCF}
// *********************************************************************//
  IAxCmbStringList = interface(IDispatch)
    ['{2DC48110-0FBB-46F6-8C02-F98C7B6D4BCF}']
    function Get_Count: Integer; safecall;
    procedure Add(const astring: WideString); safecall;
    function Get_Items(Index: Integer): WideString; safecall;
    procedure Clear; safecall;
    function IsIn(const astring: WideString): Integer; safecall;
    property Count: Integer read Get_Count;
    property Items[Index: Integer]: WideString read Get_Items;
  end;

// *********************************************************************//
// DispIntf:  IAxCmbStringListDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2DC48110-0FBB-46F6-8C02-F98C7B6D4BCF}
// *********************************************************************//
  IAxCmbStringListDisp = dispinterface
    ['{2DC48110-0FBB-46F6-8C02-F98C7B6D4BCF}']
    property Count: Integer readonly dispid 3;
    procedure Add(const astring: WideString); dispid 4;
    property Items[Index: Integer]: WideString readonly dispid 5;
    procedure Clear; dispid 6;
    function IsIn(const astring: WideString): Integer; dispid 7;
  end;

// *********************************************************************//
// Interface: IAxCmbSymOnLayerList
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6CCC3CF2-F2C6-4091-8987-09E5FD3EE967}
// *********************************************************************//
  IAxCmbSymOnLayerList = interface(IDispatch)
    ['{6CCC3CF2-F2C6-4091-8987-09E5FD3EE967}']
    function Get_Count: Integer; safecall;
    procedure Add(const aSymbolname: WideString; const aLayername: WideString); safecall;
    procedure Clear; safecall;
    function GetItem(aIndex: Integer; var aSymbolname: WideString; var aLayername: WideString): WordBool; safecall;
    function IsIn(const aSymbolname: WideString; const aLayername: WideString): Integer; safecall;
    property Count: Integer read Get_Count;
  end;

// *********************************************************************//
// DispIntf:  IAxCmbSymOnLayerListDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6CCC3CF2-F2C6-4091-8987-09E5FD3EE967}
// *********************************************************************//
  IAxCmbSymOnLayerListDisp = dispinterface
    ['{6CCC3CF2-F2C6-4091-8987-09E5FD3EE967}']
    property Count: Integer readonly dispid 1;
    procedure Add(const aSymbolname: WideString; const aLayername: WideString); dispid 2;
    procedure Clear; dispid 3;
    function GetItem(aIndex: Integer; var aSymbolname: WideString; var aLayername: WideString): WordBool; dispid 4;
    function IsIn(const aSymbolname: WideString; const aLayername: WideString): Integer; dispid 5;
  end;

// *********************************************************************//
// Interface: IAxCmbSymolsWithCPolysSetup
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B8A82819-9843-4B5A-AFD7-38CE582D9FC4}
// *********************************************************************//
  IAxCmbSymolsWithCPolysSetup = interface(IDispatch)
    ['{B8A82819-9843-4B5A-AFD7-38CE582D9FC4}']
    function Get_CPolyErrorLayer: WideString; safecall;
    procedure Set_CPolyErrorLayer(const Value: WideString); safecall;
    function Get_SymbolErrorLayer: WideString; safecall;
    procedure Set_SymbolErrorLayer(const Value: WideString); safecall;
    function Get_ProcessCPolyDatabase: WordBool; safecall;
    procedure Set_ProcessCPolyDatabase(Value: WordBool); safecall;
    function Get_ProcessCPolyErrorDatabase: WordBool; safecall;
    procedure Set_ProcessCPolyErrorDatabase(Value: WordBool); safecall;
    function Get_ProcessSymbolErrorDatabase: WordBool; safecall;
    procedure Set_ProcessSymbolErrorDatabase(Value: WordBool); safecall;
    function Get_CombineResultFilename: WideString; safecall;
    procedure Set_CombineResultFilename(const Value: WideString); safecall;
    function Get_CheckMultiAssignedSymbols: WordBool; safecall;
    procedure Set_CheckMultiAssignedSymbols(Value: WordBool); safecall;
    property CPolyErrorLayer: WideString read Get_CPolyErrorLayer write Set_CPolyErrorLayer;
    property SymbolErrorLayer: WideString read Get_SymbolErrorLayer write Set_SymbolErrorLayer;
    property ProcessCPolyDatabase: WordBool read Get_ProcessCPolyDatabase write Set_ProcessCPolyDatabase;
    property ProcessCPolyErrorDatabase: WordBool read Get_ProcessCPolyErrorDatabase write Set_ProcessCPolyErrorDatabase;
    property ProcessSymbolErrorDatabase: WordBool read Get_ProcessSymbolErrorDatabase write Set_ProcessSymbolErrorDatabase;
    property CombineResultFilename: WideString read Get_CombineResultFilename write Set_CombineResultFilename;
    property CheckMultiAssignedSymbols: WordBool read Get_CheckMultiAssignedSymbols write Set_CheckMultiAssignedSymbols;
  end;

// *********************************************************************//
// DispIntf:  IAxCmbSymolsWithCPolysSetupDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B8A82819-9843-4B5A-AFD7-38CE582D9FC4}
// *********************************************************************//
  IAxCmbSymolsWithCPolysSetupDisp = dispinterface
    ['{B8A82819-9843-4B5A-AFD7-38CE582D9FC4}']
    property CPolyErrorLayer: WideString dispid 1;
    property SymbolErrorLayer: WideString dispid 2;
    property ProcessCPolyDatabase: WordBool dispid 3;
    property ProcessCPolyErrorDatabase: WordBool dispid 4;
    property ProcessSymbolErrorDatabase: WordBool dispid 5;
    property CombineResultFilename: WideString dispid 6;
    property CheckMultiAssignedSymbols: WordBool dispid 7;
  end;

// *********************************************************************//
// Interface: IAxCmbTextsWithCPolysSetup
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E07563E8-58E5-4FB9-9683-371EC0BD8CEC}
// *********************************************************************//
  IAxCmbTextsWithCPolysSetup = interface(IDispatch)
    ['{E07563E8-58E5-4FB9-9683-371EC0BD8CEC}']
    function Get_CPolyErrorLayer: WideString; safecall;
    procedure Set_CPolyErrorLayer(const Value: WideString); safecall;
    function Get_TextErrorLayer: WideString; safecall;
    procedure Set_TextErrorLayer(const Value: WideString); safecall;
    function Get_LinkTextToCPoly: WordBool; safecall;
    procedure Set_LinkTextToCPoly(Value: WordBool); safecall;
    function Get_RemoveCombinedTexts: WordBool; safecall;
    procedure Set_RemoveCombinedTexts(Value: WordBool); safecall;
    function Get_TextDatabaseColumn: WideString; safecall;
    procedure Set_TextDatabaseColumn(const Value: WideString); safecall;
    function Get_ErrorResultFilename: WideString; safecall;
    procedure Set_ErrorResultFilename(const Value: WideString); safecall;
    function Get_ApplyTextDatabaseInfoToCPoly: WordBool; safecall;
    procedure Set_ApplyTextDatabaseInfoToCPoly(Value: WordBool); safecall;
    function Get_CheckMultiAssignedTexts: WordBool; safecall;
    procedure Set_CheckMultiAssignedTexts(Value: WordBool); safecall;
    function Get_CombineResultFilename: WideString; safecall;
    procedure Set_CombineResultFilename(const Value: WideString); safecall;
    property CPolyErrorLayer: WideString read Get_CPolyErrorLayer write Set_CPolyErrorLayer;
    property TextErrorLayer: WideString read Get_TextErrorLayer write Set_TextErrorLayer;
    property LinkTextToCPoly: WordBool read Get_LinkTextToCPoly write Set_LinkTextToCPoly;
    property RemoveCombinedTexts: WordBool read Get_RemoveCombinedTexts write Set_RemoveCombinedTexts;
    property TextDatabaseColumn: WideString read Get_TextDatabaseColumn write Set_TextDatabaseColumn;
    property ErrorResultFilename: WideString read Get_ErrorResultFilename write Set_ErrorResultFilename;
    property ApplyTextDatabaseInfoToCPoly: WordBool read Get_ApplyTextDatabaseInfoToCPoly write Set_ApplyTextDatabaseInfoToCPoly;
    property CheckMultiAssignedTexts: WordBool read Get_CheckMultiAssignedTexts write Set_CheckMultiAssignedTexts;
    property CombineResultFilename: WideString read Get_CombineResultFilename write Set_CombineResultFilename;
  end;

// *********************************************************************//
// DispIntf:  IAxCmbTextsWithCPolysSetupDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E07563E8-58E5-4FB9-9683-371EC0BD8CEC}
// *********************************************************************//
  IAxCmbTextsWithCPolysSetupDisp = dispinterface
    ['{E07563E8-58E5-4FB9-9683-371EC0BD8CEC}']
    property CPolyErrorLayer: WideString dispid 1;
    property TextErrorLayer: WideString dispid 2;
    property LinkTextToCPoly: WordBool dispid 3;
    property RemoveCombinedTexts: WordBool dispid 4;
    property TextDatabaseColumn: WideString dispid 5;
    property ErrorResultFilename: WideString dispid 6;
    property ApplyTextDatabaseInfoToCPoly: WordBool dispid 7;
    property CheckMultiAssignedTexts: WordBool dispid 8;
    property CombineResultFilename: WideString dispid 9;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TAxGisCombine
// Help String      : AxGisCombine Control
// Default Interface: IAxGisCombine
// Def. Intf. DISP? : No
// Event   Interface: IAxGisCombineEvents
// TypeFlags        : (34) CanCreate Control
// *********************************************************************//
  TAxGisCombineOnCanResize = procedure(Sender: TObject; var NewWidth: Integer; 
                                                        var NewHeight: Integer; var Resize: WordBool) of object;
  TAxGisCombineOnConstrainedResize = procedure(Sender: TObject; var MinWidth: Integer; 
                                                                var MinHeight: Integer; 
                                                                var MaxWidth: Integer; 
                                                                var MaxHeight: Integer) of object;
  TAxGisCombineOnDisplayPass = procedure(Sender: TObject; const aPass: WideString) of object;
  TAxGisCombineOnDisplayPercent = procedure(Sender: TObject; aValue: Integer) of object;

  TAxGisCombine = class(TOleControl)
  private
    FOnCanResize: TAxGisCombineOnCanResize;
    FIAxGisCombineEvents_OnClick: TNotifyEvent;
    FOnConstrainedResize: TAxGisCombineOnConstrainedResize;
    FIAxGisCombineEvents_OnDblClick: TNotifyEvent;
    FOnResize: TNotifyEvent;
    FOnDisplayPass: TAxGisCombineOnDisplayPass;
    FOnDisplayPercent: TAxGisCombineOnDisplayPercent;
    FIntf: IAxGisCombine;
    function  GetControlInterface: IAxGisCombine;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
  public
    function DrawTextBiDiModeFlagsReadingOnly: Integer;
    procedure InitiateAction;
    function IsRightToLeft: WordBool;
    function UseRightToLeftReading: WordBool;
    function UseRightToLeftScrollBar: WordBool;
    procedure CombineCPolysWithSymbols(const CPolyList: AxCmbStringList; 
                                       const SymOnLayerList: AxCmbSymOnLayerList; 
                                       const Setup: AxCmbSymbolsWithCPolysSetup; 
                                       PolygonDataSource: AxCmbDataSource; 
                                       SymbolDataSource: AxCmbDataSource);
    procedure SetApp(var aApp: OleVariant);
    procedure SetDocument(var aDocument: OleVariant);
    procedure BEV_Combine(const Filename: WideString; DataSource: AxCmbDataSource);
    procedure CombineCPolysByDatabaseAttrib(const SourceLayer: WideString; 
                                            const TargetLayer: WideString; 
                                            const DatabaseColumns: AxCmbStringList; 
                                            DataSource: AxCmbDataSource);
    procedure CombineCPolysWithTexts(const CPolyList: AxCmbStringList; 
                                     const TextList: AxCmbStringList; 
                                     const Setup: AxCmbTextsWithCPolysSetup; 
                                     PolygonDataSource: AxCmbDataSource; 
                                     TextDataSource: AxCmbDataSource);
    procedure MEDIX_Combine(const Filename: WideString; DataSource: AxCmbDataSource);
    procedure DFK_Combine(const Filename: WideString; DataSource: AxCmbDataSource);
    procedure MergeLayers(const LayerList: AxCmbStringList; const Targetlayername: WideString; 
                          const DataSourceCol: WideString);
    function CreateStringList: AxCmbStringList;
    function CreateSymbolOnLayerList: AxCmbSymOnLayerList;
    function CreateCombineSymbolsWithCPolysSetup: AxCmbSymbolsWithCPolysSetup;
    function CreateCombineTextsWithCPolysSetup: AxCmbTextsWithCPolysSetup;
    property  ControlInterface: IAxGisCombine read GetControlInterface;
    property DoubleBuffered: WordBool index 16 read GetWordBoolProp write SetWordBoolProp;
    property VisibleDockClientCount: Integer index 17 read GetIntegerProp;
  published
    property  ParentFont;
    property  Align;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property Alignment: TOleEnum index 1 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property AutoSize: WordBool index 2 read GetWordBoolProp write SetWordBoolProp stored False;
    property BevelInner: TOleEnum index 3 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property BevelOuter: TOleEnum index 4 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property BorderStyle: TOleEnum index 5 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property Caption: WideString index -518 read GetWideStringProp write SetWideStringProp stored False;
    property Color: TColor index -501 read GetTColorProp write SetTColorProp stored False;
    property Ctl3D: WordBool index 6 read GetWordBoolProp write SetWordBoolProp stored False;
    property UseDockManager: WordBool index 7 read GetWordBoolProp write SetWordBoolProp stored False;
    property DockSite: WordBool index 8 read GetWordBoolProp write SetWordBoolProp stored False;
    property DragCursor: Smallint index 9 read GetSmallintProp write SetSmallintProp stored False;
    property DragMode: TOleEnum index 10 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property Enabled: WordBool index -514 read GetWordBoolProp write SetWordBoolProp stored False;
    property FullRepaint: WordBool index 11 read GetWordBoolProp write SetWordBoolProp stored False;
    property Font: TFont index -512 read GetTFontProp write SetTFontProp stored False;
    property Locked: WordBool index 12 read GetWordBoolProp write SetWordBoolProp stored False;
    property ParentColor: WordBool index 13 read GetWordBoolProp write SetWordBoolProp stored False;
    property ParentCtl3D: WordBool index 14 read GetWordBoolProp write SetWordBoolProp stored False;
    property Visible: WordBool index 15 read GetWordBoolProp write SetWordBoolProp stored False;
    property Cursor: Smallint index 26 read GetSmallintProp write SetSmallintProp stored False;
    property WorkingDir: WideString index 33 read GetWideStringProp write SetWideStringProp stored False;
    property LngCode: WideString index 34 read GetWideStringProp write SetWideStringProp stored False;
    property DBName: WideString index 35 read GetWideStringProp write SetWideStringProp stored False;
    property DBTable: WideString index 36 read GetWideStringProp write SetWideStringProp stored False;
    property DBMode: Integer index 37 read GetIntegerProp write SetIntegerProp stored False;
    property OnCanResize: TAxGisCombineOnCanResize read FOnCanResize write FOnCanResize;
    property IAxGisCombineEvents_OnClick: TNotifyEvent read FIAxGisCombineEvents_OnClick write FIAxGisCombineEvents_OnClick;
    property OnConstrainedResize: TAxGisCombineOnConstrainedResize read FOnConstrainedResize write FOnConstrainedResize;
    property IAxGisCombineEvents_OnDblClick: TNotifyEvent read FIAxGisCombineEvents_OnDblClick write FIAxGisCombineEvents_OnDblClick;
    property OnResize: TNotifyEvent read FOnResize write FOnResize;
    property OnDisplayPass: TAxGisCombineOnDisplayPass read FOnDisplayPass write FOnDisplayPass;
    property OnDisplayPercent: TAxGisCombineOnDisplayPercent read FOnDisplayPercent write FOnDisplayPercent;
  end;

  CoAxCmbStringList = class
    class function Create: IAxCmbStringList;
    class function CreateRemote(const MachineName: string): IAxCmbStringList;
  end;

  CoAxCmbSymOnLayerList = class
    class function Create: IAxCmbSymOnLayerList;
    class function CreateRemote(const MachineName: string): IAxCmbSymOnLayerList;
  end;

  CoAxCmbSymbolsWithCPolysSetup = class
    class function Create: IAxCmbSymolsWithCPolysSetup;
    class function CreateRemote(const MachineName: string): IAxCmbSymolsWithCPolysSetup;
  end;

  CoAxCmbTextsWithCPolysSetup = class
    class function Create: IAxCmbTextsWithCPolysSetup;
    class function CreateRemote(const MachineName: string): IAxCmbTextsWithCPolysSetup;
  end;

procedure Register;

implementation

uses ComObj;

procedure TAxGisCombine.InitControlData;
const
  CEventDispIDs: array [0..6] of DWORD = (
    $00000001, $00000002, $00000003, $00000007, $00000010, $00000004,
    $00000005);
  CTFontIDs: array [0..0] of DWORD = (
    $FFFFFE00);
  CControlData: TControlData2 = (
    ClassID: '{A0E94033-E635-11D5-9D09-00C0F057241A}';
    EventIID: '{A0E94031-E635-11D5-9D09-00C0F057241A}';
    EventCount: 7;
    EventDispIDs: @CEventDispIDs;
    LicenseKey: nil;
    Flags: $0000001D;
    Version: 401;
    FontCount: 1;
    FontIDs: @CTFontIDs);
begin
  ControlData := @CControlData;
  TControlData2(CControlData).FirstEventOfs := Cardinal(@@FOnCanResize) - Cardinal(Self);
end;

procedure TAxGisCombine.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as IAxGisCombine;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TAxGisCombine.GetControlInterface: IAxGisCombine;
begin
  CreateControl;
  Result := FIntf;
end;

function TAxGisCombine.DrawTextBiDiModeFlagsReadingOnly: Integer;
begin
  Result := ControlInterface.DrawTextBiDiModeFlagsReadingOnly;
end;

procedure TAxGisCombine.InitiateAction;
begin
  ControlInterface.InitiateAction;
end;

function TAxGisCombine.IsRightToLeft: WordBool;
begin
  Result := ControlInterface.IsRightToLeft;
end;

function TAxGisCombine.UseRightToLeftReading: WordBool;
begin
  Result := ControlInterface.UseRightToLeftReading;
end;

function TAxGisCombine.UseRightToLeftScrollBar: WordBool;
begin
  Result := ControlInterface.UseRightToLeftScrollBar;
end;

procedure TAxGisCombine.CombineCPolysWithSymbols(const CPolyList: AxCmbStringList; 
                                                 const SymOnLayerList: AxCmbSymOnLayerList; 
                                                 const Setup: AxCmbSymbolsWithCPolysSetup; 
                                                 PolygonDataSource: AxCmbDataSource; 
                                                 SymbolDataSource: AxCmbDataSource);
begin
  ControlInterface.CombineCPolysWithSymbols(CPolyList, SymOnLayerList, Setup, PolygonDataSource, 
                                            SymbolDataSource);
end;

procedure TAxGisCombine.SetApp(var aApp: OleVariant);
begin
  ControlInterface.SetApp(aApp);
end;

procedure TAxGisCombine.SetDocument(var aDocument: OleVariant);
begin
  ControlInterface.SetDocument(aDocument);
end;

procedure TAxGisCombine.BEV_Combine(const Filename: WideString; DataSource: AxCmbDataSource);
begin
  ControlInterface.BEV_Combine(Filename, DataSource);
end;

procedure TAxGisCombine.CombineCPolysByDatabaseAttrib(const SourceLayer: WideString; 
                                                      const TargetLayer: WideString; 
                                                      const DatabaseColumns: AxCmbStringList; 
                                                      DataSource: AxCmbDataSource);
begin
  ControlInterface.CombineCPolysByDatabaseAttrib(SourceLayer, TargetLayer, DatabaseColumns, 
                                                 DataSource);
end;

procedure TAxGisCombine.CombineCPolysWithTexts(const CPolyList: AxCmbStringList; 
                                               const TextList: AxCmbStringList; 
                                               const Setup: AxCmbTextsWithCPolysSetup; 
                                               PolygonDataSource: AxCmbDataSource; 
                                               TextDataSource: AxCmbDataSource);
begin
  ControlInterface.CombineCPolysWithTexts(CPolyList, TextList, Setup, PolygonDataSource, 
                                          TextDataSource);
end;

procedure TAxGisCombine.MEDIX_Combine(const Filename: WideString; DataSource: AxCmbDataSource);
begin
  ControlInterface.MEDIX_Combine(Filename, DataSource);
end;

procedure TAxGisCombine.DFK_Combine(const Filename: WideString; DataSource: AxCmbDataSource);
begin
  ControlInterface.DFK_Combine(Filename, DataSource);
end;

procedure TAxGisCombine.MergeLayers(const LayerList: AxCmbStringList; 
                                    const Targetlayername: WideString; 
                                    const DataSourceCol: WideString);
begin
  ControlInterface.MergeLayers(LayerList, Targetlayername, DataSourceCol);
end;

function TAxGisCombine.CreateStringList: AxCmbStringList;
begin
  Result := ControlInterface.CreateStringList;
end;

function TAxGisCombine.CreateSymbolOnLayerList: AxCmbSymOnLayerList;
begin
  Result := ControlInterface.CreateSymbolOnLayerList;
end;

function TAxGisCombine.CreateCombineSymbolsWithCPolysSetup: AxCmbSymbolsWithCPolysSetup;
begin
  Result := ControlInterface.CreateCombineSymbolsWithCPolysSetup;
end;

function TAxGisCombine.CreateCombineTextsWithCPolysSetup: AxCmbTextsWithCPolysSetup;
begin
  Result := ControlInterface.CreateCombineTextsWithCPolysSetup;
end;

class function CoAxCmbStringList.Create: IAxCmbStringList;
begin
  Result := CreateComObject(CLASS_AxCmbStringList) as IAxCmbStringList;
end;

class function CoAxCmbStringList.CreateRemote(const MachineName: string): IAxCmbStringList;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AxCmbStringList) as IAxCmbStringList;
end;

class function CoAxCmbSymOnLayerList.Create: IAxCmbSymOnLayerList;
begin
  Result := CreateComObject(CLASS_AxCmbSymOnLayerList) as IAxCmbSymOnLayerList;
end;

class function CoAxCmbSymOnLayerList.CreateRemote(const MachineName: string): IAxCmbSymOnLayerList;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AxCmbSymOnLayerList) as IAxCmbSymOnLayerList;
end;

class function CoAxCmbSymbolsWithCPolysSetup.Create: IAxCmbSymolsWithCPolysSetup;
begin
  Result := CreateComObject(CLASS_AxCmbSymbolsWithCPolysSetup) as IAxCmbSymolsWithCPolysSetup;
end;

class function CoAxCmbSymbolsWithCPolysSetup.CreateRemote(const MachineName: string): IAxCmbSymolsWithCPolysSetup;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AxCmbSymbolsWithCPolysSetup) as IAxCmbSymolsWithCPolysSetup;
end;

class function CoAxCmbTextsWithCPolysSetup.Create: IAxCmbTextsWithCPolysSetup;
begin
  Result := CreateComObject(CLASS_AxCmbTextsWithCPolysSetup) as IAxCmbTextsWithCPolysSetup;
end;

class function CoAxCmbTextsWithCPolysSetup.CreateRemote(const MachineName: string): IAxCmbTextsWithCPolysSetup;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AxCmbTextsWithCPolysSetup) as IAxCmbTextsWithCPolysSetup;
end;

procedure Register;
begin
  RegisterComponents('ActiveX',[TAxGisCombine]);
end;

end.
