unit AxGisCmbSymOnLayerList;

interface

uses
  ComObj, ActiveX, AxGisCmb_TLB, StdVcl, Classes;

type
  TAxCmbSymOnLayerList = class(TAutoObject, IAxCmbSymOnLayerList)
  private
    FSymItems  :TStrings;
    FLayeritems:TStrings;
  protected
    { Protected declarations }
    function  Get_Count: Integer; safecall;
    procedure Add(const aSymbolname: WideString; const aLayername: WideString); safecall;
    procedure Clear; safecall;
    function  GetItem(aIndex: Integer; var aSymbolname: WideString; var aLayername: WideString): WordBool; safecall;
    function  IsIn(const aSymbolname: WideString; const aLayername: WideString): Integer; safecall;
  public
    constructor Create;
    destructor Destroy;
  end;

implementation

uses ComServ;

constructor TAxCmbSymOnLayerList.create;
begin
   FSymItems:=nil;
   FLayerItems:=nil;
end;

destructor TAxCmbSymOnLayerList.Destroy;
begin
   Clear;
end;

function TAxCmbSymOnLayerList.Get_Count: Integer;
begin
   result:=0;
   if FSymItems <> nil then
      result:=FSymItems.Count;
end;

procedure TAxCmbSymOnLayerList.Add(const aSymbolname: WideString; const aLayername: WideString);
begin
   if IsIn(aSymbolname, aLayername) >= 0 then
      exit; // already in list
   if FSymItems=nil then
      FSymItems:=TStringList.Create;
   if FLayeritems=nil then
      FLayeritems:=TStringList.Create;

   FSymItems.Add(aSymbolName);
   FLayeritems.Add(aLayerName);
end;

procedure TAxCmbSymOnLayerList.Clear;
begin
   if FSymItems <> nil then
      FSymItems.Destroy;
   if FLayeritems <> nil then
      FLayeritems.Destroy;
   FSymItems:=nil;
   FLayeritems:=nil;
end;

function TAxCmbSymOnLayerList.GetItem(aIndex: Integer; var aSymbolname: WideString; var aLayername: WideString): WordBool;
begin
   result:=false;
   if FSymItems = nil then
      exit;
   if (aIndex >= 0) and (aIndex < FSymItems.Count) then
   begin
      aSymbolName:=FSymItems[aIndex];
      aLayername:=FLayerItems[aIndex];
      result:=true;
   end;
end;

function TAxCmbSymOnLayerList.IsIn(const aSymbolname: WideString; const aLayername: WideString): Integer;
var i:integer;
begin
   result:=-1;
   if FSymItems = nil then exit;
   for i:=0 to FSymItems.Count-1 do
   begin
      if (FSymItems[i] = aSymbolName) and (FLayerItems[i] = aLayername) then
      begin
         result:=i;
         break;
      end;
   end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TAxCmbSymOnLayerList, Class_AxCmbSymOnLayerList,
    ciInternal, tmApartment);

end.
