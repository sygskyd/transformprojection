unit ObjectRefFilter;

interface

uses SysUtils, WinTypes, Classes;

const Filter_CPoly  = 1;
      Filter_Text   = 2;
      Filter_Symbol = 4;

type TLayerItemFilter=class
   protected
      FLayerName:string;
      FItems    :TStrings;
   public
      constructor Create;
      destructor Destroy;
      property LayerName:string read FLayerName write FLayerName;
      property Items    :TStrings read FItems;
      procedure CreateItems;
end;

type TObjectRefFilter=class
   protected
      FObjectFilter:integer;
      FLayerFilters:array of TLayerItemFilter;
      FLayerFilterSize  :integer;
      FReadAttribs      :boolean;
      FSupportedColumns :TStrings;
   private
      FSearchFilterIndex:integer;
      function FindLayer(const aLayer:string):TLayerItemFilter;
   public
      constructor Create;
      destructor Destroy;
      // methods to set filter
      procedure AddObjectTypeToFilter(const atype:integer);
      procedure AddSupportedLayer(const aLayer:string);
      procedure AddSupportedLayerContent(const aLayer:string; aInfo:string);
      procedure AddSupportedDbColumn(const aCol:string);
      // method to check with filter
      function IsSupportedObjectType(const aObjectType:integer):boolean;
      function IsSupportedLayer(const aName:string):boolean;
      function IsSupportedLayerContent(const aLayer:string; const aInfo:string):boolean;
      function IsSupportedDbColumn(const aCol:string):boolean;
      procedure Clear;
      property  ReadAttribs:boolean read FReadAttribs write FReadAttribs;
end;

implementation

// class TLayerItemFilter-------------------------------------------------------
constructor TLayerItemFilter.Create;
begin
   FItems:=nil;
end;

destructor TLayerItemFilter.Destroy;
begin
   if FItems <> nil then
      FItems.Destroy;
end;

procedure TLayeritemFilter.CreateItems;
begin
   FItems:=TStringList.Create;
end;

// class TObjectRefFilter-------------------------------------------------------

constructor TObjectRefFilter.Create;
begin
   FObjectFilter:=0;
   FLayerFilterSize:=0;
   FSearchFilterIndex:=-1;
   FSupportedColumns:=nil;
end;

destructor TObjectRefFilter.Destroy;
begin
   Clear;
end;

procedure TObjectRefFilter.Clear;
var i:integer;
begin
   if FLayerFilterSize >= 0 then
   begin
      for i:=0 to FLayerFilterSize-1 do
         FLayerFilters[i].Destroy;
   end;
   SetLength(FLayerFilters,0);
   FLayerFilterSize:=0;
   FObjectFilter:=0;
   if FSupportedColumns <> nil then
      FSupportedColumns.Destroy;
end;

procedure TObjectRefFilter.AddObjectTypeToFilter(const atype:integer);
begin
   // first check if type is not already in filter
   if (FObjectFilter and atype) = atype then exit;
   FObjectFilter:=FObjectFilter+atype;
end;

procedure TObjectRefFilter.AddSupportedDbColumn(const aCol:string);
begin
   if FSupportedColumns = nil then
      FSupportedColumns:=TStringList.Create;
   FSupportedColumns.Add(aCol);
end;

function TObjectRefFilter.FindLayer(const aLayer:string):TLayerItemFilter;
var i:integer;
begin
   result:=nil;
   if (FSearchFilterIndex < FLayerFilterSize) and (FSearchFilterIndex >= 0) then
   begin
      // check with search index
      if FLayerFilters[FSearchFilterIndex].LayerName = aLayer then
      begin
         result:=FLayerFilters[FSearchFilterIndex];
         exit;
      end;
   end;
   // normal search
   if FLayerFilterSize > 0 then
   begin
      for i:=0 to FLayerFilterSize-1 do
      begin
         if FLayerFilters[i].Layername = aLayer then
         begin
            result:=FLayerFilters[i];
            FSearchFilterIndex:=i;
            break;
         end;
      end;
   end;
end;

procedure TObjectRefFilter.AddSupportedLayer(const aLayer:string);
var aLayerFilter:TLayerItemFilter;
begin
   aLayerFilter:=FindLayer(aLayer);
   if aLayerFilter <> nil then exit; // already exists in list

   aLayerFilter:=TLayerItemFilter.Create;
   aLayerFilter.LayerName:=aLayer;

   FLayerFilterSize:=FLayerFilterSize+1;
   SetLength(FLayerFilters, FLayerFilterSize);
   FLayerFilters[FLayerFilterSize-1]:=aLayerFilter;
end;

procedure TObjectRefFilter.AddSupportedLayerContent(const aLayer:string; aInfo:string);
var aLayerFilter:TLayerItemFilter;
begin
   aLayerFilter:=FindLayer(aLayer);
   if (aLayerFilter = nil) then
   begin
      aLayerFilter:=TLayerItemFilter.Create;
      aLayerFilter.LayerName:=aLayer;

      FLayerFilterSize:=FLayerFilterSize+1;
      SetLength(FLayerFilters, FLayerFilterSize);
      FLayerFilters[FLayerFilterSize-1]:=aLayerFilter;
   end;
   // now check if content already exists
   if aLayerFilter.Items = nil then
      aLayerFilter.CreateItems;
   if aLayerFilter.Items.IndexOf(aInfo) < 0 then
      aLayerFilter.Items.Add(aInfo);
end;

function TObjectRefFilter.IsSupportedObjectType(const aObjectType:integer):boolean;
begin
   if (FObjectFilter and aObjectType) = aObjectType then result:=true else result:=false;
end;

function TObjectRefFilter.IsSupportedLayer(const aName:string):boolean;
begin
   if FindLayer(aName) <> nil then result:=true else result:=false;
end;

function TObjectRefFilter.IsSupportedLayerContent(const aLayer:string; const aInfo:string):boolean;
var aLayerFilter:TLayerItemFilter;
begin
   aLayerFilter:=FindLayer(aLayer);
   if aLayerFilter = nil then
   begin
      result:=false;
      exit;
   end;

   if aLayerFilter.Items = nil then
   begin
      result:=true; // -> use layer without item filter
      exit;
   end;

   if aLayerFilter.Items.IndexOf(aInfo) < 0 then result:=false else result:=true;
end;

function TObjectRefFilter.IsSupportedDbColumn(const aCol:string):boolean;
begin
   result:=true;
   if FSupportedColumns <> nil then
   begin
      if FSupportedColumns.IndexOf(aCol) >= 0 then
         result:=true;
   end;
end;

end.
