unit AxGisCmbStringList;

interface

uses
  ComObj, ActiveX, AxGisCmb_TLB, StdVcl;

type
  TAxCmbStringList = class(TAutoObject, IAxCmbStringList)
  private
    fitems:array of string;
    fnumitems:integer;
  protected
    function  Get_Count: Integer; safecall;
    procedure Add(const astring: WideString); safecall;
    function  Get_Items(Index: Integer): WideString; safecall;
    procedure Clear; safecall;
    function  IsIn(const astring: WideString): Integer; safecall;
    { Protected declarations }
  public
    constructor create;
    destructor destroy;
  end;

implementation

uses ComServ;

constructor TAxCmbStringList.create;
begin
   fnumitems:=0;
end;

destructor TAxCmbStringList.Destroy;
begin
   Clear;
end;

function TAxCmbStringList.Get_Count: Integer;
begin
   result:=fnumitems;
end;

procedure TAxCmbStringList.Add(const astring:WideString);
begin
   if IsIn(astring) = -1 then
   begin
      fnumitems:=fnumitems+1;
      SetLength(fitems, fnumitems);
      fitems[fnumitems-1]:=astring;
   end;
end;

function TAxCmbStringList.Get_Items(Index: Integer): WideString;
begin
   result:='';
   if (Index < fnumitems) and (Index >= 0) then
      result:=fitems[Index];
end;

procedure TAxCmbStringList.Clear;
begin
   fnumitems:=0;
   SetLength(fitems, fnumitems);
end;

function TAxCmbStringList.IsIn(const astring: WideString): Integer;
var i:integer;
begin
   result:=-1;
   for i:=0 to fnumitems-1 do
   begin
      if fitems[i] = astring then
      begin
         result:=i;
         break;
      end;
   end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TAxCmbStringList, Class_AxCmbStringList,
    ciInternal, tmApartment);

end.
