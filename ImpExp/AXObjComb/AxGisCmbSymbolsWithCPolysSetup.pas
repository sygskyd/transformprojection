unit AxGisCmbSymbolsWithCPolysSetup;

interface

uses
  ComObj, ActiveX, AxGisCmb_TLB, StdVcl;

type
  TAxCmbSymbolsWithCPolysSetup = class(TAutoObject, IAxCmbSymolsWithCPolysSetup)
  private
    // private variables to support properties
    FCPolyErrorLayer:string;
    FSymbolErrorLayer:string;
    FProcessCPolyDatabase      :boolean;
    FProcessCPolyErrorDatabase :boolean;
    FProcessSymbolErrorDatabase:boolean;
    FCombineResultFilename     :string;
    FCheckMultiAssignedSymbols :boolean;
  protected
    { Protected declarations }
    function  Get_CPolyErrorLayer: WideString; safecall;
    procedure Set_CPolyErrorLayer(const Value: WideString); safecall;
    function  Get_SymbolErrorLayer: WideString; safecall;
    procedure Set_SymbolErrorLayer(const Value: WideString); safecall;
    function  Get_ProcessCPolyDatabase: WordBool; safecall;
    procedure Set_ProcessCPolyDatabase(Value: WordBool); safecall;
    function  Get_ProcessCPolyErrorDatabase: WordBool; safecall;
    procedure Set_ProcessCPolyErrorDatabase(Value: WordBool); safecall;
    function  Get_ProcessSymbolErrorDatabase: WordBool; safecall;
    procedure Set_ProcessSymbolErrorDatabase(Value: WordBool); safecall;
    function  Get_CombineResultFilename: WideString; safecall;
    procedure Set_CombineResultFilename(const Value: WideString); safecall;
    function  Get_CheckMultiAssignedSymbols: WordBool; safecall;
    procedure Set_CheckMultiAssignedSymbols(Value: WordBool); safecall;
  public
    constructor create;
    destructor destroy;
  end;

implementation

uses ComServ;

constructor TAxCmbSymbolsWithCPolysSetup.create;
begin
   FCPolyErrorLayer:='';
   FSymbolErrorLayer:='';
   FProcessCPolyDatabase:=false;
   FProcessCPolyErrorDatabase :=false;
   FProcessSymbolErrorDatabase:=false;
   FCombineResultFilename     :='';
   FCheckMultiAssignedSymbols :=false;
end;

destructor TAxCmbSymbolsWithCPolysSetup.destroy;
begin
end;

function TAxCmbSymbolsWithCPolysSetup.Get_CPolyErrorLayer: WideString;
begin
   result:=FCPolyErrorLayer;
end;

procedure TAxCmbSymbolsWithCPolysSetup.Set_CPolyErrorLayer(const Value: WideString);
begin
   FCPolyErrorLayer:=Value;
end;

function TAxCmbSymbolsWithCPolysSetup.Get_SymbolErrorLayer: WideString;
begin
   result:=FSymbolErrorLayer;
end;

procedure TAxCmbSymbolsWithCPolysSetup.Set_SymbolErrorLayer(const Value: WideString);
begin
   FSymbolErrorLayer:=Value;
end;

function TAxCmbSymbolsWithCPolysSetup.Get_ProcessCPolyDatabase: WordBool;
begin
   result:=FProcessCPolyDatabase;
end;

procedure TAxCmbSymbolsWithCPolysSetup.Set_ProcessCPolyDatabase(Value: WordBool);
begin
   FProcessCPolyDatabase:=Value;
end;

function TAxCmbSymbolsWithCPolysSetup.Get_ProcessCPolyErrorDatabase: WordBool;
begin
   result:=FProcessCPolyErrorDatabase;
end;

procedure TAxCmbSymbolsWithCPolysSetup.Set_ProcessCPolyErrorDatabase(Value: WordBool);
begin
   FProcessCPolyErrorDatabase:=Value;
end;

function TAxCmbSymbolsWithCPolysSetup.Get_ProcessSymbolErrorDatabase: WordBool;
begin
   result:=FProcessSymbolErrorDatabase;
end;

procedure TAxCmbSymbolsWithCPolysSetup.Set_ProcessSymbolErrorDatabase(Value: WordBool);
begin
   FProcessSymbolErrorDatabase:=Value;
end;

function TAxCmbSymbolsWithCPolysSetup.Get_CombineResultFilename: WideString;
begin
   result:=FCombineResultFilename;
end;

procedure TAxCmbSymbolsWithCPolysSetup.Set_CombineResultFilename(const Value: WideString);
begin
   FCombineResultFilename:=Value;
end;

function TAxCmbSymbolsWithCPolysSetup.Get_CheckMultiAssignedSymbols: WordBool;
begin
   result:=FCheckMultiAssignedSymbols;
end;

procedure TAxCmbSymbolsWithCPolysSetup.Set_CheckMultiAssignedSymbols(Value: WordBool);
begin
   FCheckMultiAssignedSymbols:=Value;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TAxCmbSymbolsWithCPolysSetup, Class_AxCmbSymbolsWithCPolysSetup,
    ciInternal, tmApartment);

end.
