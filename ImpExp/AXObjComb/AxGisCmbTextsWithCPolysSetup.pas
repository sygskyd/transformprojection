unit AxGisCmbTextsWithCPolysSetup;

interface

uses
  ComObj, ActiveX, AxGisCmb_TLB, StdVcl;

type
  TAxCmbTextsWithCPolysSetup = class(TAutoObject, IAxCmbTextsWithCPolysSetup)
  private
    // private variables to support properties
    FCPolyErrorLayer:string;
    FTextErrorLayer :string;
    FLinkTextToCPoly:boolean;
    FRemoveCombinedTexts:boolean;
    FTextDatabaseColumn:string;
    FErrorResultFilename:string;
    FCombineResultFilename:string;
    FApplyTextDatabaseInfoToCPoly:boolean;
    FCheckMultiAssignedTexts     :boolean;
  protected
    { Protected declarations }
    function  Get_CPolyErrorLayer: WideString; safecall;
    procedure Set_CPolyErrorLayer(const Value: WideString); safecall;
    function  Get_TextErrorLayer: WideString; safecall;
    procedure Set_TextErrorLayer(const Value: WideString); safecall;
    function  Get_LinkTextToCPoly: WordBool; safecall;
    procedure Set_LinkTextToCPoly(Value: WordBool); safecall;
    function  Get_RemoveCombinedTexts: WordBool; safecall;
    procedure Set_RemoveCombinedTexts(Value: WordBool); safecall;
    function  Get_TextDatabaseColumn: WideString; safecall;
    procedure Set_TextDatabaseColumn(const Value: WideString); safecall;
    function  Get_ErrorResultFilename: WideString; safecall;
    procedure Set_ErrorResultFilename(const Value: WideString); safecall;
    function  Get_ApplyTextDatabaseInfoToCPoly: WordBool; safecall;
    procedure Set_ApplyTextDatabaseInfoToCPoly(Value: WordBool); safecall;
    function  Get_CheckMultiAssignedTexts: WordBool; safecall;
    procedure Set_CheckMultiAssignedTexts(Value: WordBool); safecall;
    function  Get_CombineResultFilename: WideString; safecall;
    procedure Set_CombineResultFilename(const Value: WideString); safecall;
  public
    constructor create;
    destructor destroy;
end;

implementation

uses ComServ;

constructor TAxCmbTextsWithCPolysSetup.create;
begin
   FLinkTextToCPoly:=false;
   FRemoveCombinedTexts:=false;
   FApplyTextDatabaseInfoToCPoly:=false;
   FCheckMultiAssignedTexts:=false;
end;

destructor TAxCmbTextsWithCPolysSetup.destroy;
begin

end;

function TAxCmbTextsWithCPolysSetup.Get_CPolyErrorLayer: WideString;
begin
   result:=FCPolyErrorLayer;
end;
procedure TAxCmbTextsWithCPolysSetup.Set_CPolyErrorLayer(const Value: WideString);
begin
   FCPolyErrorLayer:=Value;
end;

function TAxCmbTextsWithCPolysSetup.Get_TextErrorLayer: WideString;
begin
   result:=FTextErrorLayer;
end;
procedure TAxCmbTextsWithCPolysSetup.Set_TextErrorLayer(const Value: WideString);
begin
   FTextErrorLayer:=value;
end;

function TAxCmbTextsWithCPolysSetup.Get_LinkTextToCPoly: WordBool;
begin
   result:=FLinkTextToCPoly;
end;
procedure TAxCmbTextsWithCPolysSetup.Set_LinkTextToCPoly(Value: WordBool);
begin
   FLinkTextToCPoly:=value;
end;

function TAxCmbTextsWithCPolysSetup.Get_RemoveCombinedTexts: WordBool;
begin
   result:=FRemoveCombinedTexts;
end;
procedure TAxCmbTextsWithCPolysSetup.Set_RemoveCombinedTexts(Value: WordBool);
begin
   FRemoveCombinedTexts:=value;
end;

function TAxCmbTextsWithCPolysSetup.Get_TextDatabaseColumn: WideString;
begin
   result:=FTextDatabaseColumn;
end;
procedure TAxCmbTextsWithCPolysSetup.Set_TextDatabaseColumn(const Value: WideString);
begin
   FTextDatabaseColumn:=Value;
end;

function TAxCmbTextsWithCPolysSetup.Get_ErrorResultFilename: WideString;
begin
   result:=FErrorResultFilename;
end;
procedure TAxCmbTextsWithCPolysSetup.Set_ErrorResultFilename(const Value: WideString);
begin
   FErrorResultFilename:=Value;
end;

function TAxCmbTextsWithCPolysSetup.Get_ApplyTextDatabaseInfoToCPoly: WordBool;
begin
   result:=FApplyTextDatabaseInfoToCPoly;
end;
procedure TAxCmbTextsWithCPolysSetup.Set_ApplyTextDatabaseInfoToCPoly(Value: WordBool);
begin
   FApplyTextDatabaseInfoToCPoly:=Value;
end;

function TAxCmbTextsWithCPolysSetup.Get_CheckMultiAssignedTexts: WordBool;
begin
   result:=FCheckMultiAssignedTexts;
end;
procedure TAxCmbTextsWithCPolysSetup.Set_CheckMultiAssignedTexts(Value: WordBool);
begin
   FCheckMultiAssignedTexts:=Value;
end;

function TAxCmbTextsWithCPolysSetup.Get_CombineResultFilename: WideString;
begin
   result:=FCombineResultFilename;
end;
procedure TAxCmbTextsWithCPolysSetup.Set_CombineResultFilename(const Value: WideString);
begin
   FCombineResultFilename:=value;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TAxCmbTextsWithCPolysSetup, Class_AxCmbTextsWithCPolysSetup,
    ciInternal, tmApartment);

end.
